///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AssetURI.h"

namespace KEP {

//!	Parse Asset URI: /path/to/file_name.dae#mesh_name[submeshcount]
//!	URI is composed of following components:
//!	p1) protocol: "file://", "http://"									(planned, optional, default to 'file://')
//!	p2) hostname: "assets.kaneva.com"									(planned, optional, default to 'localhost')
//!	1) path: "/path/to/"												(optional)
//!	2) document: "citywalk.dae"											(path + document = URI Base)
//!	3) node: "#table"				-- subnode inside a document		(optional)
//!	4) submesh count: "[2]"			-- number submeshes under this node	(optional)
//! Note: legacy importers only support path and document name.

enum ParseType {
	PARSE_ALL, // Parse everything
	PARSE_DOC_AND_BEYOND, // Parse subscript, node and document name
	PARSE_NODE_AND_SUBSCRIPT, // Parse subscript and node
	PARSE_SUBSCRIPT_ONLY, // Parse subscript only
};

//! \brief Uniformed parsing logic handler
static void DoParseURIString(const std::string& aURIString, AssetURI& aResult, ParseType aParseType, std::string& unparsed) {
	// Initialize with default values
	aResult.isValid = TRUE;
	aResult.protocol.empty();
	aResult.host.empty();
	aResult.path.empty();
	aResult.document.empty();
	aResult.node.empty();
	aResult.subscript = -1; // -1: unknown/don't care

	unparsed = aURIString;

	// Parse subscript (optional)
	if (!unparsed.empty()) {
		if (unparsed[unparsed.length() - 1] == ']') { // Submesh Count found
			unsigned posLB = unparsed.rfind('[');
			if (posLB != std::string::npos) {
				aResult.subscript = atoi(unparsed.substr(posLB + 1, unparsed.length() - posLB - 2).c_str());
				unparsed = unparsed.substr(0, posLB);
			}
		}
	}

	// Stop parsing if that's all we want
	if (aParseType == PARSE_SUBSCRIPT_ONLY)
		return;

	// Parse node name (optional)
	if (!unparsed.empty()) {
		unsigned posPound = unparsed.rfind('#');
		if (posPound != std::string::npos) {
			aResult.node = unparsed.substr(posPound + 1);
			unparsed = unparsed.substr(0, posPound);
		}
	}

	// Stop parsing if that's all we want
	if (aParseType == PARSE_NODE_AND_SUBSCRIPT)
		return;

	// Parse document name (required)
	if (!unparsed.empty()) {
		unsigned posSlash = unparsed.rfind('/');
		if (posSlash == std::string::npos)
			posSlash = unparsed.rfind('\\');

		if (posSlash != std::string::npos) {
			aResult.document = unparsed.substr(posSlash + 1);
			unparsed = unparsed.substr(0, posSlash);
		} else {
			aResult.document = unparsed;
			unparsed.clear();
		}
	}

	// Stop parsing if that's all we want
	if (aParseType == PARSE_DOC_AND_BEYOND)
		return;

	// Parse protocol
	if (!unparsed.empty()) {
		unsigned posProtDlm = unparsed.find("://");
		if (posProtDlm != std::string::npos) {
			aResult.protocol = unparsed.substr(0, posProtDlm);
			unparsed = unparsed.substr(posProtDlm + 3);
		}
	}

	// Parse host
	if (!unparsed.empty() && !aResult.protocol.empty()) { // host name is assumed not-avail if no protocol is specified
		// Search for first slash, which delimits host name and path
		unsigned posSlash = unparsed.find('/');
		if (posSlash != std::string::npos) {
			aResult.host = unparsed.substr(0, posSlash);
			unparsed = unparsed.substr(posSlash + 1);
		}
	}

	aResult.path = unparsed;
	unparsed.clear();
}

//! \brief Convert from a URI string: prot://host/path/to/document#node[subID]
void AssetURI::ParseFromURIString(const std::string& aURIString) {
	std::string dummy;
	DoParseURIString(aURIString, *this, PARSE_ALL, dummy);
}

//! \brief Convert to a URI string: prot://host/path/to/document#node[subID]
std::string AssetURI::ToURIString() {
	std::stringstream ss;
	ss << GetNodeURI();
	if (subscript != -1)
		ss << "[" << subscript << "]";
	return ss.str();
}

//! \brief Return URI of the asset node: everything in URI string except subID
std::string AssetURI::GetNodeURI() {
	std::string nodeURI = GetDocumentURI();
	if (!node.empty())
		nodeURI += "#" + node;
	return nodeURI;
}

//! \brief Return URI of the document where asset is stored: everything except node/subID
std::string AssetURI::GetDocumentURI() {
	// Short format
	if (protocol.empty() && host.empty() && path.empty())
		return document;

	// Regular format
	if (protocol.empty())
		protocol = "file";
	if (host.empty())
		host = "localhost";

	std::stringstream ss;
	ss << protocol << "://" << host << path << document;
	return ss.str();
}

//! \brief Parse and return URI of the asset node (see GetNodeURI)
std::string AssetURI::GetNodeURIFromURIString(const std::string& aURIString) {
	AssetURI tmpResult;
	std::string nodeURI;
	DoParseURIString(aURIString, tmpResult, PARSE_SUBSCRIPT_ONLY, nodeURI);
	return nodeURI;
}

//! \brief Parse and return URI of the whole document (see GetDocumentURI)
std::string AssetURI::GetDocumentURIFromURIString(const std::string& aURIString) {
	AssetURI tmpResult;
	std::string docURI;
	DoParseURIString(aURIString, tmpResult, PARSE_NODE_AND_SUBSCRIPT, docURI);
	return docURI;
}

//! \brief Parse and return name of the node only
std::string AssetURI::GetNodeNameFromURIString(const std::string& aURIString) {
	AssetURI tmpResult;
	std::string docURI;
	DoParseURIString(aURIString, tmpResult, PARSE_NODE_AND_SUBSCRIPT, docURI);
	return tmpResult.node;
}

// \brief Parse and return sub ID only
int AssetURI::GetSubscriptFromURIString(const std::string& aURIString) {
	AssetURI tmpResult;
	std::string nodeURI;
	DoParseURIString(aURIString, tmpResult, PARSE_SUBSCRIPT_ONLY, nodeURI);
	return tmpResult.subscript;
}

//! \brief Return node name or document file name if node name is empty.
std::string AssetURI::GetAssetName() {
	if (node.empty())
		return document;
	return node;
}

//! \brief Return node name or document file name if node name is empty.
std::string AssetURI::GetAssetNameFromURIString(const std::string& aURIString) {
	AssetURI tmpResult;
	std::string unparsed;
	DoParseURIString(aURIString, tmpResult, PARSE_DOC_AND_BEYOND, unparsed);
	if (tmpResult.node.empty())
		return tmpResult.document;
	return tmpResult.node;
}

} // namespace KEP