///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IAssetsDatabase.h"
#include <string>

namespace KEP {

class TextureImporter : public IAssetImporter {
public:
	TextureImporter();

	void Register() override;

	void Unregister() override;

	bool Import(TextureDef& textureInfo, const std::string& fileName, AssetData* assetDef = NULL, int flags = 0) override;
	bool GetTextureImportInfo(TextureDef& textureInfo, const std::string& importedFileName) override;
};

} // namespace KEP
