///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "AssetsDatabase.h"
#include "AssetNamingSyntax.h"
#include "AssetImporterAdvancedDlg.h"
//#include "AssetImporterNamingDlg.h"
#include "UnicodeMFCHelpers.h"

namespace KEP {

IMPLEMENT_DYNAMIC(AssetImporterAdvancedDlg, CDialog)

AssetImporterAdvancedDlg::AssetImporterAdvancedDlg(const NamingSyntax* namingSyntaxs, UINT numOfSyntaxs, CWnd* pParent /*=NULL*/) :
		CDialog(AssetImporterAdvancedDlg::IDD, pParent), m_bAllowPartial(0), m_bNamingSyntaxChanged(FALSE) {
	SetNamingSyntaxs(namingSyntaxs, numOfSyntaxs);
}

AssetImporterAdvancedDlg::~AssetImporterAdvancedDlg() {
}

void AssetImporterAdvancedDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CK_ALLOW_PARTIAL, m_bAllowPartial);

	if (!pDX->m_bSaveAndValidate) {
		CStringA oldSyntax[2], newSyntax[2];
		GetDlgItemTextUtf8(this, IDC_ED_STNAME, oldSyntax[NAMING_STATIC]);
		GetDlgItemTextUtf8(this, IDC_ED_SKNAME, oldSyntax[NAMING_SKELETAL]);
		newSyntax[NAMING_STATIC] = m_namingSyntaxs[NAMING_STATIC].getSyntaxString().c_str();
		newSyntax[NAMING_SKELETAL] = m_namingSyntaxs[NAMING_SKELETAL].getSyntaxString().c_str();
		if (newSyntax[NAMING_STATIC] != oldSyntax[NAMING_STATIC] || newSyntax[NAMING_SKELETAL] != oldSyntax[NAMING_SKELETAL])
			m_bNamingSyntaxChanged = TRUE;

		DDX_Text(pDX, IDC_ED_STNAME, newSyntax[NAMING_STATIC]);
		DDX_Text(pDX, IDC_ED_SKNAME, newSyntax[NAMING_SKELETAL]);
	}
}

BEGIN_MESSAGE_MAP(AssetImporterAdvancedDlg, CDialog)
ON_BN_CLICKED(IDC_BTN_CHANGE_STNAME, &AssetImporterAdvancedDlg::OnBnClickedBtnChangeStaticNaming)
ON_BN_CLICKED(IDC_BTN_CHANGE_SKNAME, &AssetImporterAdvancedDlg::OnBnClickedBtnChangeSkeletalNaming)
END_MESSAGE_MAP()

// AssetImporterAdvancedDlg message handlers

void AssetImporterAdvancedDlg::OnBnClickedBtnChangeStaticNaming() {
	OnChangeNaming(NAMING_STATIC);
}

void AssetImporterAdvancedDlg::OnBnClickedBtnChangeSkeletalNaming() {
	OnChangeNaming(NAMING_SKELETAL);
}

void AssetImporterAdvancedDlg::OnChangeNaming(int namingType) {
	//	AssetImporterNamingDlg dlg(namingType, m_namingSyntaxs[namingType]);
	//
	//	if (dlg.DoModal() == IDOK) {
	//		UpdateData(FALSE);	// Update control text: not saving control text
	//	}
}

void AssetImporterAdvancedDlg::GetNamingSyntaxs(NamingSyntax* namingSyntaxs, UINT maxNumOfSyntaxs, UINT& numOfSyntaxs) const {
	numOfSyntaxs = min(NUM_NAMING_TYPES, maxNumOfSyntaxs);
	for (UINT i = 0; i < numOfSyntaxs; i++)
		namingSyntaxs[i] = m_namingSyntaxs[i];
}

void AssetImporterAdvancedDlg::SetNamingSyntaxs(const NamingSyntax* namingSyntaxs, UINT numOfSyntaxs) {
	ASSERT(numOfSyntaxs <= NUM_NAMING_TYPES);
	for (UINT i = 0; i < min(numOfSyntaxs, NUM_NAMING_TYPES); i++)
		m_namingSyntaxs[i] = namingSyntaxs[i];
}

} // namespace KEP