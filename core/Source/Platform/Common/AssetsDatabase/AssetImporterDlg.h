///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "AssetTree.h"

#define ERROR_MSG_TOO_MANY_SELECTION 1

namespace KEP {

class CAssetImporterDlg : public CDialog {
	DECLARE_DYNAMIC(CAssetImporterDlg)

private:
	// Reference to data
	AssetTree* m_allAssets;

	// Constants for icons
	int idGroup, idObject, idMesh, idVariation, idLOD, idBone;
	int idGroup_Gray, idObject_Gray, idMesh_Gray, idVariation_Gray, idLOD_Gray, idBone_Gray;

	// Dialog state
	AssetData::Level m_nViewBy, m_nImportLevel;
	KEP_ASSET_TYPE m_nImportType;

	BOOL m_bAllowMultiSel, m_bImportLODs, m_bImportAltCol;
	BOOL m_bShowSubmeshes;

	// Tree view states
	std::map<CStringW, UINT> m_treeItemStates;
	std::map<CStringW, HTREEITEM> m_treeItemKeyMap; // Map from "tree item key" to tree item
	HIMAGELIST m_hImageList;

	BOOL m_bInitialDisplay;
	CStatic m_lblImportType;
	NamingSyntax* m_namingSyntaxs;
	UINT m_numOfSyntaxs;

	BOOL m_bAutoSuppressDuplicatedMessage;
	std::map<int, UINT> m_suppressedMessages, m_suppressedMsgCounter;

public:
	BOOL m_bRebuildAssetTree;

public:
	CAssetImporterDlg(AssetTree* assets,
		NamingSyntax* namingSyntaxs,
		UINT numOfSyntaxs,
		KEP_ASSET_TYPE importType = KEP_UNKNOWN_TYPE,
		AssetData::Level importLevel = AssetData::LVL_UNKNOWN,
		BOOL allowMultiSel = TRUE, BOOL defaultImportLODs = TRUE, BOOL defaultImportCollision = FALSE,
		CWnd* pParent = NULL); // standard constructor
	virtual ~CAssetImporterDlg();

	// Dialog Data
	enum { IDD = IDD_DLG_ASSETIMPORTER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	AssetData::Level CalcViewLevel();
	AssetData::Level CalcImportLevel();
	int CalcViewLevelSel();
	int CalcImportLevelSel();

	//! type: new import type, bFromUI: triggered by UI. Return true if asset tree need to be updated
	BOOL SetImportLevel(AssetData::Level level, BOOL bFromUI = FALSE);
	//! type: new view type, bFromUI: triggered by UI. Return true if asset tree need to be updated
	BOOL SetViewLevel(AssetData::Level level, BOOL bFromUI = FALSE);
	//! bShow: show submeshes, bFromUI: triggered by UI. Return true if asset tree need to be updated
	BOOL SetShowSubmeshes(BOOL bShow, BOOL bFromUI = FALSE);
	//
	BOOL SetAllowMultiSel(BOOL bAllow, BOOL bFromUI = FALSE);
	//
	BOOL SetImportLODs(BOOL bImport, BOOL bFromUI = FALSE);
	//
	BOOL SetImportCollision(BOOL bImport, BOOL bFromUI = FALSE);

	//! Fill in tree control with available assets and update treeItemKeyMap
	void UpdateAssetTreeView();
	//! Clear treeItemKeyMap
	void ClearTreeKeys();
	//! Save expansion and selection state for all tree items for future restoration
	void SaveTreeStates(HTREEITEM item = NULL);
	//! Restore expansion and selection state for all tree items
	void RestoreTreeStates(HTREEITEM item = NULL);
	//! Clear expansion states when dialog is closed
	void ClearTreeStates();
	//! Add flag to tree item states for one single item
	void SetTreeItemState(const CStringW& key, int flag);
	//! Remove flag from tree item states for one single item
	void UnsetTreeItemState(const CStringW& key, int flag);
	//! Add selected asset to import list
	void AddToImportList(HTREEITEM hItem, BOOL bIsDescendent = FALSE);
	//! Remove selected asset from import list
	void RemoveFromImportList(int itemID);
	//! Remove selected asset from import list
	void RemoveFromImportList(HTREEITEM hItem);
	//! Add all asset to import list
	void AddAllToImportList();
	//! Remove all asset from import list
	void RemoveAllFromImportList();
	//! Check if an asset is already in import list
	BOOL IsAssetInImportList(const AssetData* asset);
	//! Restore asset selection list by STA_SELECTED flag
	void RestoreAssetSelections();

	BOOL GetImageIndexByAssetType(KEP_ASSET_TYPE type, AssetData::Level level, int& imageID, int& imageGrayID);
	HTREEITEM InsertTreeItem(HTREEITEM hParent, const wchar_t* itemText, DWORD itemData, int imageID, BOOL bGrayed);

	// Tree View Manipulations
	void CollapseTreeToLevel(AssetData::Level level);
	void ExpandTreeToLevel(AssetData::Level level);
	void CollapseAndExpandToLevel(AssetData::Level level);
	void CollapseNode(HTREEITEM hItem, BOOL bCollapseEverythingBeyond);
	void ExpandNode(HTREEITEM hItem, BOOL bExpandEverythingBeyond);
	void CollapseAll();
	void ExpandAll();

	void ClearSuppressedMessage(int msgID);
	void ClearSuppressedMessageList();
	UINT ShowMessage(int msgID, CStringW message, UINT nType = MB_OK);

	void CheckAndDisplayPreviewWarnings();

	CStylizedTreeCtrl m_assetTree; // MFC automatically subclass tree control for us
	CComboBox m_cbImportType;
	CComboBox m_cbViewBy;
	CButton m_ckShowSubMeshes;
	CListCtrl m_importList;
	CButton m_btnAddToList;
	CButton m_btnRemoveFromList;
	CButton m_btnAddAllToList;
	CButton m_btnRemoveAllFromList;
	CButton m_btnImport;
	CButton m_btnImportAll;
	CButton m_ckImportLODs;
	CButton m_ckImportAltCol;

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnAddtolist();
	afx_msg void OnBnClickedBtnRemovefromlist();
	afx_msg void OnTvnSelchangedTreeAvailasset(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCbnSelchangeCbViewby();
	afx_msg void OnBnClickedCkShowSubmesh();
	afx_msg void OnBnClickedBtnImport();
	afx_msg void OnBnClickedBtnImportall();
	afx_msg void OnClose();
	afx_msg void OnLbnSelchangeListImport();
	afx_msg void OnCbnSelchangeCbImporttype();
	afx_msg void OnBnClickedBtnAddAllToList();
	afx_msg void OnBnClickedBtnRemoveAllFromList();
	afx_msg void OnNMKillfocusTreeAvailasset(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNMSetfocusTreeAvailasset(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNMClickListImport(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedBtnAdvanced();
	afx_msg LRESULT OnInitialDisplay(WPARAM wParam, LPARAM lParam);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnCancel();

	DECLARE_MESSAGE_MAP()
};

} // namespace KEP