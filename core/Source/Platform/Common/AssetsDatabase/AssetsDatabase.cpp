///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "AssetsDatabase.h"

#include "AssetNamingSyntax.h"
#include "CSkeletonObject.h"
#include "RuntimeSkeleton.h"
#include "SkeletonConfiguration.h"
#include "CBoneClass.h"
#include "StylizedTreeCtrl.h"
#include "AssetImporterDlg.h"
#include "CVertexAnimationModule.h"
#include "CEXMeshClass.h"
#include "CDeformableMesh.h"
#include "CHierarchyMesh.h"
#include "CMaterialClass.h"
#include "AssetURI.h"
#include "..\ResourceManagers\ReMeshCreate.h"
#include "CBoneAnimationNaming.h"
#include "UnicodeMFCHelpers.h"
#include "UnicodeWindowsHelpers.h"

#define DEFAULT_ANIMATION_TYPE "USER001"
#include "TextureDef.h"

#include "LogHelper.h"
static LogInstance("Instance");

using namespace KEP;
using namespace tcl;

//! Register the asset importer with the AssetsDatabase, for a particular extension and asset type.
//! Call this function as many times as needed for each extension and asset type supported
void CSimpleImporterFactory::RegisterImporter(IAssetImporter* aAssetImporter, KEP_ASSET_TYPE aAssetType, const std::string& aExtension) {
	// Check if importer already exists
	if (allImporters.find(aAssetImporter->GetName()) == allImporters.end()) {
		// New importer
		allImporters[aAssetImporter->GetName()] = aAssetImporter;
	}
	// otherwise, importer already registered, now go for a new extension rule.

	std::string lowercaseExt = aExtension;
	STLToLower(lowercaseExt);
	IAssetImporter_set& impSet = importerTable[aAssetType][lowercaseExt];
	impSet.insert(aAssetImporter);

	allImportersSet.insert(aAssetImporter);
}

//! Unregister the asset importer with the AssetsDatabase, for a particular extension and asset type.
//! Call this function as many times as needed to discontinue support of a particular format
void CSimpleImporterFactory::UnregisterImporter(IAssetImporter* aAssetImporter, KEP_ASSET_TYPE aAssetType, const std::string& aExtension) {
	std::string lowercaseExt = aExtension;
	STLToLower(lowercaseExt);
	IAssetImporter_set& impSet = importerTable[aAssetType][aExtension];
	impSet.erase(aAssetImporter);

	// now count how many times this importer appears in the importerTable
	// if 0 times, erase it from allImportersSet too
	int numInstances = 0;
	AssetTypeToExts_map::iterator m1It(importerTable.begin()), m1End(importerTable.end());
	for (; m1It != m1End; ++m1It) {
		ExtensionToImporters_map::iterator m2It(m1It->second.begin()), m2End(m1It->second.end());
		for (; m2It != m2End; ++m2It) {
			IAssetImporter_set::iterator impSetIt = m2It->second.find(aAssetImporter);
			if (impSetIt != m2It->second.end())
				numInstances++;
		}
	}

	if (numInstances == 0) {
		allImportersSet.erase(aAssetImporter);
		allImporters.erase(aAssetImporter->GetName());
	}
}

//! Unregister the asset importer with the AssetsDatabase.
//! Removes all the entries that refer to this importer
void CSimpleImporterFactory::UnregisterImporter(IAssetImporter* aAssetImporter) {
	AssetTypeToExts_map::iterator m1It(importerTable.begin()), m1End(importerTable.end());
	for (; m1It != m1End; ++m1It) {
		ExtensionToImporters_map::iterator m2It(m1It->second.begin()), m2End(m1It->second.end());
		for (; m2It != m2End; ++m2It)
			m2It->second.erase(aAssetImporter);
	}

	allImportersSet.erase(aAssetImporter);
}

//! Return, for a particular asset type, a filter to be used in a file open dialog
void CSimpleImporterFactory::GetOpenDialogFilter(KEP_ASSET_TYPE aAssetType, std::string& aExtensionsList) {
	AssetTypeToString_map::iterator filterIt = filterTable.find(aAssetType);
	if (filterIt != filterTable.end()) {
		aExtensionsList.append(filterIt->second);
		return;
	}

	std::set<KEP_ASSET_TYPE> assetTypes;
	assetTypes.insert(aAssetType);

	std::string allFilters;
	GetOpenDialogFilter(assetTypes, allFilters);

	filterTable[aAssetType] = allFilters;
	aExtensionsList.append(allFilters);
}

void CSimpleImporterFactory::GetOpenDialogFilter(std::set<KEP_ASSET_TYPE> aAssetTypes, std::string& aExtensionsList) {
	std::string individualFilters;
	std::string catchAllFilters;

	for (auto itType = aAssetTypes.begin(); itType != aAssetTypes.end(); ++itType) {
		KEP_ASSET_TYPE assetType = *itType;
		for (auto it = importerTable[assetType].begin(); it != importerTable[assetType].end(); ++it) {
			individualFilters.append(it->first);
			individualFilters.append(" Files (*.");
			individualFilters.append(it->first);
			individualFilters.append(")|");
			individualFilters.append("*.");
			individualFilters.append(it->first);
			individualFilters.append("|");

			catchAllFilters.append("*.");
			catchAllFilters.append(it->first);
			catchAllFilters.append(";");
		}
	}

	if (!catchAllFilters.empty()) {
		catchAllFilters.erase(catchAllFilters.size() - 1, 1); // erase trailing ";"
		catchAllFilters = "All Supported Files (" + catchAllFilters + ")|" + catchAllFilters + "|";

		// finally allow loading all files
		individualFilters.append("All Files (*.*)|*.*||");

		std::string allFilters = catchAllFilters + individualFilters;
		aExtensionsList.append(allFilters);
	}
}

std::string getString(const std::string& aString) {
	return aString;
}

std::string getString(const std::vector<std::string>& aStringVector) {
	if (aStringVector.size() > 0)
		return aStringVector[0];
	return "";
}

// checks if a aFilename has aExtension at the end
// assumes aExtension is already in lowercase, as it is the case in our ExtensionToImporters_map
bool hasExtension(const std::string& aFileName, const std::string& aExtension) {
	std::string fileName = aFileName;
	STLToLower(fileName);
	UINT extbegin = 0;
	if (((extbegin = fileName.find(std::string(".") + aExtension)) != std::string::npos) && ((fileName.size() - extbegin) == (aExtension.size() + 1)))
		return true;
	return false;
}

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//! ### Yanfeng Notes - September 2008 ###
//! Guessing importers by file extensions is problematic:
//! E.g. two importers installed, one for .mesh.xml, another one for .xml. User picks a .mesh.xml filter and selects a .mesh.xml
//! file. The file is eventually interpreted by .xml importer which is not what the user wants. (It's possible that .xml importer
//! accepts a .mesh.xml file although it might not be well-formed.) In addition, there might be two .dae importers (collada 1.3
//! and 1.4) performing jobs in totally different ways.
//!
//! Solution 1: do not allow ambiguious importer extensions
//! Solution 2: If user picks a filter while opening file, always use the associated importer to import. For importation without
//! an UI, it's better to know the file format before use, otherwise we can only guess or use defaults.
//! Solution 3: Let importer to decide (current solution) -- try one by one until affirmed
//!
//! Sol#2 sounds great but there is a somewhat serious problem: user cannot use "catch-all" filter to select from a list of supported
//! files. The problem with Sol#3 is already described above. Sol#3 + Sol#1 might be the best solution at this time if we won't support
//! too many formats. In the future if ambiguious file extensions are unavoidable, we can use a work-around for Sol#2 to provide a
//! catch-all filter: when catch-all filter is used and file extension is ambiguous, notify user regarding the possible problem and
//! recommend to use a specific filter, or provide additional selection UI to clarify.

#define INVALID_IMPORTER ((IAssetImporter*)0xFFFFFFFF)

IAssetImporter* CSimpleImporterFactory::GetImporter(KEP_ASSET_TYPE aAssetType, const std::string& aURI, int aFlags) {
	std::vector<IAssetImporter*> compatibleImporters;
	ExtensionToImporters_map m = importerTable[(KEP_ASSET_TYPE)aAssetType];

	AssetURI uri(aURI);
	std::string docUri = uri.GetDocumentURI();

	for (auto it = m.begin(); it != m.end(); ++it) {
		if (hasExtension(docUri, it->first)) {
			IAssetImporter_set& impset = it->second;
			for (auto impsetIt(impset.begin()); impsetIt != impset.end(); ++impsetIt) {
				IAssetImporter* imp = *impsetIt;
				if (imp->SupportAssetURI(docUri, aAssetType)) {
					if ((aFlags & IMP_GENERAL_UIMASK) == IMP_GENERAL_NOUI)
						return imp; // Use first match if no user can be reached through UI.
					compatibleImporters.push_back(imp);
				}
			}
		}
	}

	// Now that's see if we have multiple importers found
	int count = compatibleImporters.size();
	if (count == 0)
		return NULL; // No compatible importers found
	if (count == 1)
		return compatibleImporters[0];

	// Ask user to pick through an UI dialog
	IAssetImporter* imp = compatibleImporters[0];
	CStringA msg = "Multiple importers found, using: ";
	msg += imp->GetDescription().data();
	AfxMessageBoxUtf8(msg);
	return compatibleImporters[0]; // Return first one for now
}

void CSimpleImporterFactory::RegisterImporterImpl(IAssetImporterImpl* aImpl, const std::string& aImporterName) {
	if (allImporters.find(aImporterName) == allImporters.end())
		throw(std::string("RegisterImporterImpl: Importer [" + aImporterName + "] not found"));
	allImpls.insert(std::pair<IAssetImporter*, IAssetImporterImpl*>(allImporters[aImporterName], aImpl));
}

void CSimpleImporterFactory::UnregisterImporterImpl(IAssetImporterImpl* aImpl, const std::string& aImporterName) {
	if (allImporters.find(aImporterName) == allImporters.end())
		throw(std::string("RegisterImporterImpl: Importer [" + aImporterName + "] not found"));
	IAssetImporter* aImporter = allImporters[aImporterName];
	AssetImplMap::iterator it = allImpls.find(aImporter);
	for (; it != allImpls.end() && it->first == aImporter; ++it) {
		if (it->second == aImpl) {
			allImpls.erase(it);
			break;
		}
	}
}

void CSimpleImporterFactory::UnregisterImporterImpl(IAssetImporterImpl* aImpl) {
	AssetImplMap::iterator it = allImpls.begin();
	for (; it != allImpls.end(); ++it) {
		if (it->second == aImpl) {
			allImpls.erase(it);
			break;
		}
	}
}

std::set<IAssetImporterImpl*> CSimpleImporterFactory::GetImporterImpl(IAssetImporter* aImporter) {
	std::set<IAssetImporterImpl*> ret;
	AssetImplMap::iterator it = allImpls.find(aImporter);
	for (; it != allImpls.end() && it->first == aImporter; ++it)
		ret.insert(it->second);
	return ret;
}

CAssetsDatabase::CAssetsDatabase() {
	namingSyntaxs = NULL;
}

CAssetsDatabase::~CAssetsDatabase() {
	if (namingSyntaxs)
		delete[] namingSyntaxs;
	namingSyntaxs = NULL;
}

//! \brief Try importing a single KEP_STATIC_MESH
//! \param aURI Asset URI: path/to/file.ext#assetname[submeshID]
//! \param aFlags Import flag: single/multi mesh/LOD/material
//! \param aImporter Pointer to instance of importer class.
//! \return Imported asset.
AssetData CAssetsDatabase::ImportStaticMesh(
	const std::string& aURI,
	int aFlags,
	IAssetImporter* aImporter) {
	aFlags &= ~(IMP_GENERAL_MESHPARTMASK | IMP_GENERAL_LODMASK); // Reset mesh part and LOD flag
	aFlags |= IMP_GENERAL_SINGLEPART | IMP_GENERAL_SINGLELOD; // Set to single part and single LOD only

	AssetData ret;

	AssetTree* assets = ImportStaticMeshes(aURI, aFlags, NULL, aImporter);
	if (assets == NULL)
		return ret;

	// Return first imported mesh found in tree
	for (auto it = assets->level_order_begin(); it != assets->level_order_end(); ++it) {
		if (it->CheckFlag(AssetData::STA_IMPORTED)) {
			ret = *it;
			break;
		}
	}

	delete assets;

	return ret;
}

//! \brief Try importing selected KEP_STATIC_MESHes (multiple)
//! \param aURI Asset URI: path/to/file.ext#assetname[submeshID]
//! \param aFlags Import flag: single/multi mesh/LOD/material
//! \param aImporter Pointer to instance of importer class.
//! \return All imported asset in a tree hierarchy. Memory is allocated and will be disposed by caller.
AssetTree* CAssetsDatabase::ImportStaticMeshes(
	const std::string& aURI,
	int aFlags,
	IClassifyAssetNode* classifyFunc,
	IAssetImporter* aImporter) {
	IAssetImporter* pImporter = aImporter;
	if (pImporter == NULL)
		pImporter = IAssetImporterFactory::Instance()->GetImporter(KEP_STATIC_MESH, aURI, aFlags);
	if (pImporter == NULL)
		return NULL;

	VERIFY_RETURN(pImporter->BeginSession(aURI, aFlags), NULL);

	AssetTree* pAssets = pImporter->PreviewAssets(aURI, KEP_STATIC_MESH, aFlags, classifyFunc);
	if (pAssets == NULL) {
		// PreviewAssets not supported by importer or,
		//    No assets found in file.

		// Try importing the entire file without definition from AssetData
		CEXMeshObj* newObject = new CEXMeshObj();
		if (pImporter->Import(*newObject, aURI, NULL)) {
			std::string nodeName = AssetURI::GetAssetNameFromURIString(aURI);
			AssetData asset(KEP_STATIC_MESH, AssetData::LVL_SUBMESH, "*", nodeName, aURI, -1);
			asset.AddMesh(newObject);
			asset.SetFlag(AssetData::STA_SELECTED);
			asset.SetFlag(AssetData::STA_IMPORTED);
			pAssets = new AssetTree(asset);

			pImporter->EndSession(aURI);
			return pAssets;
		} else {
			//@@Watch: SafeDelete is not called (just like in legacy code) although I think we're supposed to.
			//I'm leaving it consistent with legacy code for now until it's confirmed to cause memory leak. [Yanfeng 11/2008]
			delete newObject;

			pImporter->EndSession(aURI);
			return NULL;
		}
	}

	if ((aFlags & IMP_GENERAL_UIMASK) == IMP_GENERAL_NOUI) {
		// If no UI available, mark all assets for import
		for (auto it = pAssets->pre_order_begin(); it != pAssets->pre_order_end(); ++it)
			if (it->GetLevel() == AssetData::LVL_LOD)
				it->SetFlag(AssetData::STA_SELECTED);
	} else {
		FOREVER {
			std::auto_ptr<CAssetImporterDlg> dlg(new CAssetImporterDlg(pAssets, namingSyntaxs, NUM_NAMING_TYPES, KEP_STATIC_MESH, AssetData::LVL_LOD));
			if (dlg->DoModal() == IDOK)
				break;

			// Cleanup if failed
			delete pAssets;

			// Return failure if user does not request a re-run
			if (!dlg->m_bRebuildAssetTree) {
				pImporter->EndSession(aURI);
				return NULL;
			}

			// Redo previewing and dialog
			pAssets = pImporter->PreviewAssets(aURI, KEP_STATIC_MESH, aFlags, classifyFunc);
		}
	}

	ImportStaticMeshesByAssetTree(aURI, aFlags, pAssets, pImporter);

	pImporter->EndSession(aURI);
	return pAssets;
}

bool CAssetsDatabase::ImportStaticMeshesByAssetTree(
	const std::string& aRootURI,
	int aFlags,
	AssetTree* pAssets,
	IAssetImporter* apImporter) {
	IAssetImporter* pImporter = apImporter;
	if (pImporter == NULL)
		pImporter = IAssetImporterFactory::Instance()->GetImporter(KEP_STATIC_MESH, aRootURI, aFlags);
	if (pImporter == NULL)
		return false;

	for (auto it = pAssets->post_order_begin(); it != pAssets->post_order_end(); ++it) {
		AssetData& asset = *it;
		if (!asset.IsMeshLevel())
			continue; // Skip no leaf nodes (objects, sections, variations, etc)
		if (!asset.CheckFlag(AssetData::STA_SELECTED))
			continue; // Skip not-selected nodes

		int numSubmeshes = asset.GetNumSubmeshes();

		for (int subID = 0; subID < numSubmeshes || numSubmeshes == -1; subID++) { // If numSubmeshes==-1, keep looping until no more results
			bool res = ImportStaticMeshByAssetDefinition(aRootURI, aFlags, asset, subID, pImporter);
			if (res) {
				if ((aFlags & IMP_GENERAL_SUBMESHMASK) == IMP_GENERAL_SINGLESUBMESH)
					break; // Stop if only one single material submesh is requested
			} else {
				if (numSubmeshes == -1)
					break; // Stop if importation failed when submesh count is not specified
			}
		}
	}

	//	if ( CAssetsDatabase::pGDP ) {
	//		std::string gameBase = PathApp(); //CAssetsDatabase::pGDP->PathBase(); //@@Watch: temporarily solution
	//		SetCurrentDirectoryW( Utf8ToUtf16(gameBase).c_str() );
	//	}

	return true;
}

bool CAssetsDatabase::ImportStaticMeshByAssetDefinition(
	const std::string& aRootURI,
	int aFlags,
	AssetData& aDef,
	int subID,
	IAssetImporter* apImporter) {
	IAssetImporter* pImporter = apImporter;
	if (pImporter == NULL)
		pImporter = IAssetImporterFactory::Instance()->GetImporter(KEP_STATIC_MESH, aRootURI, aFlags);
	if (pImporter == NULL)
		return false;

	bool retVal = true;
	CEXMeshObj* newObject = new CEXMeshObj();

	AssetURI uri(aDef.GetURI()); // Parse URI String
	uri.subscript = subID; // Current sub item ID for import

	if (pImporter->Import(*newObject, uri.ToURIString(), &aDef, aFlags)) {
		aDef.AddMesh(newObject);
		aDef.SetFlag(AssetData::STA_IMPORTED); // called multiple times
	} else {
		//@@Watch: SafeDelete is not called (just like in legacy code) although I think we're supposed to.
		//I'm leaving it consistent with legacy code for now until it's confirmed to cause memory leak. [Yanfeng 11/2008]
		delete newObject;
		if (aDef.GetNumSubmeshes() != -1)
			aDef.AddMesh((CEXMeshObj*)NULL); // Still insert NULL submesh unless the number of total submeshes is indeterminate

		CStringW msg;
		msg.Format(L"Unable to import: %s", Utf8ToUtf16(aDef.GetURI()).c_str());
		MessageBoxW(0, msg, L"TRACE", MB_OK);

		retVal = false;
	}

	return retVal;
}

AssetTree* CAssetsDatabase::OpenAndPreviewURI(const std::string& aURI, KEP_ASSET_TYPE aAssetType, int aFlags, IClassifyAssetNode* classifyFunc /*= NULL*/) {
	IAssetImporter* pImporter = IAssetImporterFactory::Instance()->GetImporter(aAssetType, aURI, aFlags);
	VERIFY_RETURN(pImporter != NULL, NULL);
	VERIFY_RETURN(pImporter->BeginSession(aURI, aFlags), NULL);
	return pImporter->PreviewAssets(aURI, aAssetType, aFlags, classifyFunc);
}

void CAssetsDatabase::CloseURI(const std::string& aURI, KEP_ASSET_TYPE aAssetType, int aFlags) {
	IAssetImporter* pImporter = IAssetImporterFactory::Instance()->GetImporter(aAssetType, aURI, aFlags);
	VERIFY_RETVOID(pImporter != NULL);
	pImporter->EndSession(aURI);
}

BOOL CAssetsDatabase::MergeSkeletalObjects(
	CSkeletonObject& toObject,
	CSkeletonObject& fromObject,
	int aSectionIndex,
	int aConfigIndex,
	unsigned int aLodIndex,
	int aFlags) {
	ImportFlags flags;
	flags.intValue = aFlags;

	if (fromObject.getSkeletonConfiguration()->m_meshSlots != NULL) {
		for (POSITION posSection = fromObject.getSkeletonConfiguration()->m_meshSlots->GetHeadPosition(); posSection != NULL;) {
			CAlterUnitDBObject* pNewSection = dynamic_cast<CAlterUnitDBObject*>(fromObject.getSkeletonConfiguration()->m_meshSlots->GetNext(posSection));
			ASSERT(pNewSection != NULL && pNewSection->m_configurations != NULL);

			CAlterUnitDBObject* pDstSection = IAssetImporter::GetSkeletonSectionPtr(toObject, aSectionIndex, flags, aSectionIndex == -1 ? (const char*)pNewSection->m_name : "");
			ASSERT(pDstSection != NULL && pDstSection->m_configurations != NULL);

			for (POSITION posConfig = pNewSection->m_configurations->GetHeadPosition(); posConfig != NULL;) {
				CDeformableMesh* pNewConfig = dynamic_cast<CDeformableMesh*>(pNewSection->m_configurations->GetNext(posConfig));
				ASSERT(pNewConfig != NULL);

				CDeformableMesh* pDstConfig = IAssetImporter::GetSectionConfigPtr(*pDstSection, aConfigIndex, flags, aConfigIndex == -1 ? (const char*)pNewConfig->m_dimName : "");
				ASSERT(pDstConfig != NULL);

				// Either we allow multi-LOD import or we just imported only one LOD.
				ASSERT(aLodIndex == -1 || pNewConfig->m_lodCount <= 1);

				if (pNewConfig->m_lodCount == 0)
					continue; // skip if no LODs has been imported

				unsigned int oldCount = pDstConfig->m_lodCount, newCount = oldCount, newLODBase = 0;
				UINT64* oldHashes = pDstConfig->m_lodHashes;

				// compute size of new LOD table
				if (flags.lod == SKIMPORT_INSERTAFTER) { // append
					newCount = pDstConfig->m_lodCount + pNewConfig->m_lodCount;
					newLODBase = pDstConfig->m_lodCount;
				} else if (aLodIndex != -1) {
					newCount = std::max(aLodIndex + 1, pDstConfig->m_lodCount); // replace specified LOD
					newLODBase = aLodIndex;
				} else
					newCount = std::max(pDstConfig->m_lodCount, pNewConfig->m_lodCount); // merge LODs by LOD number

				ASSERT(newCount >= oldCount);

				if (newCount > oldCount) { // if grew
					//resize lodhash table
					pDstConfig->m_lodCount = newCount;
					pDstConfig->m_lodHashes = new UINT64[pDstConfig->m_lodCount];
					memset(pDstConfig->m_lodHashes, 0, sizeof(UINT64) * pDstConfig->m_lodCount);
					if (oldHashes != NULL) {
						memcpy(pDstConfig->m_lodHashes, oldHashes, sizeof(UINT64) * oldCount);
						delete[] oldHashes;
					}
				}

				// Converting bone weighing table for new LODs
				std::set<std::string> unmatchedBones;
				if (!RemapDeformableMeshBones(*pNewConfig, *fromObject.getRuntimeSkeleton(), *toObject.getRuntimeSkeleton(), unmatchedBones)) {
					if ((aFlags & IMP_GENERAL_UIMASK) == IMP_GENERAL_WITHUI) {
						CStringA msg = "Warning: not all bones are converted properly into existing skeleton.\n\nFollowing bones are not converted: \n";

						int lineLength = 0;
						for (auto it = unmatchedBones.begin(); it != unmatchedBones.end(); ++it) {
							msg.Append(it->c_str());
							msg.Append(" ");
							lineLength += it->size() + 1;
							if (lineLength > 60) {
								lineLength = 0;
								msg.Append("\n");
							}
						}

						AfxMessageBoxUtf8(msg);
					}
				}

				// now merging LOD hash tables
				if (MeshRM::Instance()) {
					unsigned int newLodCount = pNewConfig->m_lodCount;
					if (aLodIndex != -1)
						newLodCount = 1; // replace single if LOD index is specified

					// As bone assignments are changed we need to rehash it.
					MeshRM::Instance()->SetCurrentBoneList(toObject.getRuntimeSkeleton()->getBoneList());

					POSITION pos = pNewConfig->GetDeformableVisList()->GetHeadPosition();
					for (unsigned int i = 0; i < newLodCount && pos != NULL; i++) {
						CDeformableVisObj* pLod = dynamic_cast<CDeformableVisObj*>(pNewConfig->GetDeformableVisList()->GetNext(pos));
						ASSERT(pLod != NULL);
						if (pLod == NULL)
							continue;

						MeshRM::Instance()->SetCurrentLOD(newLODBase + i);
						ReMesh* pReMesh = pLod->GetReMesh();
						if (pReMesh == NULL) {
							pReMesh = MeshRM::Instance()->CreateReSkinMeshPersistent(pLod);
							pLod->SetReMesh(pReMesh);
						}
						pDstConfig->m_lodHashes[newLODBase + i] = pReMesh->GetHash();
					}
				}

				//@@Watch: copy more fields
				if (pDstConfig->m_supportedMaterials == NULL)
					pDstConfig->m_supportedMaterials = new CObList();
				if (flags.config != SKIMPORT_APPENDTO) {
					// If not appending, cleanup old materials
					for (POSITION pos = pDstConfig->m_supportedMaterials->GetHeadPosition(); pos != NULL;) {
						CMaterialObject* tmp = dynamic_cast<CMaterialObject*>(pDstConfig->m_supportedMaterials->GetNext(pos));
						ASSERT(tmp != NULL);
						if (pDstConfig->m_material == tmp)
							pDstConfig->m_material = NULL; // double-deletion prevention
						delete tmp;
					}
					pDstConfig->m_supportedMaterials->RemoveAll();
					if (pDstConfig->m_material != NULL) {
						delete pDstConfig->m_material;
						pDstConfig->m_material = NULL;
					}
				}

				if ((aFlags & IMP_SKELETAL_DMESHMATMASK) == IMP_SKELETAL_DMESHMAT && pNewConfig->m_supportedMaterials != NULL) {
					pDstConfig->m_supportedMaterials->AddTail(pNewConfig->m_supportedMaterials);
					pNewConfig->m_supportedMaterials->RemoveAll();
				}
				if ((aFlags & IMP_SKELETAL_DMESHMATMASK) == IMP_SKELETAL_DMESHMAT && pNewConfig->m_material != NULL) {
					if (pDstConfig->m_material && pDstConfig->m_supportedMaterials->Find(pDstConfig->m_material) == NULL)
						delete pDstConfig->m_material;
					pDstConfig->m_material = pNewConfig->m_material;
					pNewConfig->m_material = NULL;
				}
			}

			//@@Watch: copy more fields
			pDstSection->m_meshCount = pDstSection->m_configurations->GetCount();
			if (pDstSection->m_meshNames != NULL)
				delete[] pDstSection->m_meshNames;
			pDstSection->m_meshNames = new CStringA[pDstSection->m_meshCount]; //@@Watch: code for mesh name creation should be factored out.
			pDstSection->GenerateMeshNames();
		}
	}

	return TRUE;
}

CSkeletonObject* CAssetsDatabase::ImportSkeletalObjectFromLegacyImporter(
	const std::string& aURI,
	int aFlags,
	CSkeletonObject* pDstObj,
	IAssetImporter* pImporter) {
	// Try importing the entire file without definition from AssetData
	CSkeletonObject* pSkeleton;
	if (pDstObj == NULL)
		pSkeleton = new CSkeletonObject(); // Import to a new skeleton
	else
		pSkeleton = pDstObj; // Import and  merge with an existing skeleton

	if (pImporter->Import(*pSkeleton, aURI, NULL, aFlags))
		return pSkeleton;

	// Import failed, clean up
	if (pDstObj == NULL) {
		delete pSkeleton;
	}

	return NULL;
}

BOOL CAssetsDatabase::ParseAssetName(
	KEP_ASSET_TYPE assetType,
	const std::string& name,
	char& type,
	std::string& stem,
	std::string& slotOrPart,
	std::string& dimVar,
	int& lod) {
	std::string results[NUM_RESULT_STRINGS];
	BOOL ret = FALSE;

	// default values (in case parsing failed)
	type = 0;
	stem = name;
	slotOrPart.clear();
	dimVar.clear();
	lod = -1;

	switch (assetType) {
		case KEP_SKA_SKELETAL_OBJECT:
		case KEP_SKA_SKINNED_MESH:
		case KEP_SKA_RIGID_MESH:
			if (namingSyntaxs) {
				ret = namingSyntaxs[NAMING_SKELETAL].parseString(name, results);
			}
			break;

		case KEP_STATIC_MESH:
			if (namingSyntaxs) {
				ret = namingSyntaxs[NAMING_STATIC].parseString(name, results);
			}
			break;
	}

	if (!ret)
		return FALSE; // parsing failed

	if (!results[RESULT_TYPE].empty())
		type = results[RESULT_TYPE][0];
	else
		type = 0;

	stem = results[RESULT_OBJECT];
	slotOrPart = results[RESULT_PART];
	dimVar = results[RESULT_VARIATION];

	lod = AssetData::LOD0;
	if (!results[RESULT_LOD].empty()) {
		if (STLCompareIgnoreCase(results[RESULT_LOD], "c") == 0)
			lod = AssetData::LOD_COL; // Collision
		else
			lod = atoi(results[RESULT_LOD].c_str());
	}
	return TRUE;
}

BOOL CAssetsDatabase::ParseAnimationName(
	const std::string& animID,
	std::string& animName,
	int& firstFrame,
	int& lastFrame,
	int& fps) {
	animName = animID;
	firstFrame = 0;
	lastFrame = -1; // entire animation
	fps = -1; // use default FPS

	if (animID.substr(0, strlen(ANIM_URI_PREFIX)) != ANIM_URI_PREFIX) // check prefix
		return FALSE;

	animName = animID.substr(strlen(ANIM_URI_PREFIX));

	size_t posAt = animName.rfind('@');
	if (posAt != std::string::npos) {
		std::string fpsStr = animName.substr(posAt + 1);
		animName = animName.substr(0, posAt);
		fps = atoi(fpsStr.c_str());
	}

	size_t posLB = animName.find('{');
	if (posLB != std::string::npos) {
		size_t posRB = animName.find('}');
		if (posRB != animName.length() - 1)
			return FALSE;

		std::string rangeStr = animName.substr(posLB + 1, posRB - posLB - 1);
		animName = animName.substr(0, posLB);

		if (!rangeStr.empty()) {
			size_t posDash = rangeStr.find('-');
			if (posDash != std::string::npos) {
				if (posDash > 0)
					firstFrame = atoi(rangeStr.substr(0, posDash).c_str());
				if (posDash < rangeStr.length() - 1)
					lastFrame = atoi(rangeStr.substr(posDash + 1).c_str());
			} else {
				firstFrame = atoi(rangeStr.c_str());
			}
		}
	}

	return TRUE;
}

std::string CAssetsDatabase::MakeAnimationName(
	const std::string& animName,
	int firstFrame,
	int lastFrame,
	int fps) {
	std::string tmp = animName;
	STLToUpper(tmp);

	std::stringstream ss;
	ss << ANIM_URI_PREFIX << tmp;

	if (firstFrame != 0 || lastFrame != -1) {
		ss << "{";
		if (firstFrame != 0)
			ss << firstFrame;
		if (lastFrame != -1)
			ss << "-" << lastFrame;
		ss << "}";
	}

	if (fps != -1)
		ss << "@" << fps;

	return ss.str();
}

//! \brief Try importing a KEP_SKA_SKELETAL_OBJECT
//! \param aURI Asset URI: path/to/file.ext#assetname[submeshID]
//! \param aFlags Import flag: single/multi mesh/LOD/material
//! \param aImporter Pointer to instance of importer class.
//! \return Imported skeletal object. Memory is allocated and will be disposed by the caller.
CSkeletonObject* CAssetsDatabase::ImportSkeletalObject(
	const std::string& aURI,
	int aFlags,
	CSkeletonObject* pSO_dest,
	int dstSection,
	int dstConfig,
	int dstLOD,
	IClassifyAssetNode* classifyFunc,
	IAssetImporter* aImporter) {
	IAssetImporter* pImporter = aImporter;
	if (pImporter == NULL)
		pImporter = IAssetImporterFactory::Instance()->GetImporter(KEP_SKA_SKELETAL_OBJECT, aURI, aFlags);
	if (pImporter == NULL)
		return NULL;

	VERIFY_RETURN(pImporter->BeginSession(aURI, aFlags), NULL);

	// Enumerate all assets contained at the specified URI
	AssetTree* pAssets = pImporter->PreviewAssets(aURI, KEP_SKA_SKELETAL_OBJECT, aFlags, classifyFunc);
	if (pAssets == NULL) // PreviewAssets not supported by importer or no assets found in file.
		return ImportSkeletalObjectFromLegacyImporter(aURI, aFlags, pSO_dest, pImporter);

	// Instruct user to select asset for import
	if ((aFlags & IMP_GENERAL_UIMASK) == IMP_GENERAL_NOUI) {
		// If no UI available, mark all assets for import
		for (AssetTree::pre_order_iterator it = pAssets->pre_order_begin(); it != pAssets->pre_order_end(); ++it)
			if (it->GetLevel() == AssetData::LVL_LOD)
				it->SetFlag(AssetData::STA_SELECTED);
	} else {
		// Import entire skeletal object by default, multiple selection OK
		AssetData::Level importLevel = AssetData::LVL_OBJECT;
		KEP_ASSET_TYPE importType = KEP_SKA_SKELETAL_OBJECT;
		BOOL bAllowMultiSel = TRUE;

		if (pSO_dest != NULL) {
			// if merging
			importType = KEP_SKA_SKINNED_MESH;
			if (dstSection == -1)
				importLevel = AssetData::LVL_SECTION; // Multi-Section Import
			else if (dstConfig == -1)
				importLevel = AssetData::LVL_VARIATION; // Multi-Config Import
			else {
				importLevel = AssetData::LVL_LOD; // Multi-LOD Import
				if (dstLOD != -1)
					bAllowMultiSel = FALSE; // Single-LOD Import
			}
		}

		FOREVER {
			std::auto_ptr<CAssetImporterDlg> dlg(new CAssetImporterDlg(pAssets, namingSyntaxs, NUM_NAMING_TYPES, importType, importLevel, bAllowMultiSel));
			if (dlg->DoModal() == IDOK)
				break;

			// Cleanup if failed
			delete pAssets;

			// Return failure if user does not request a re-run
			if (!dlg->m_bRebuildAssetTree) {
				pImporter->EndSession(aURI);
				return NULL;
			}

			// Redo previewing and dialog
			pAssets = pImporter->PreviewAssets(aURI, KEP_SKA_SKELETAL_OBJECT, aFlags, classifyFunc);
		}
	}

	//@@Limitation: currently only allow one skeleton in a collada file (or always import first skeleton)
	//@@Todo: Analyze selected assets and decide which skeleton to import
	//@@Watch: there might be more than one skeleton in the file. Without UI we cannot make a selection.
	//Bring in first skeleton for now
	AssetTree* pSkeletonGroup = AssetData::GetAssetGroup(pAssets, KEP_SKA_SKELETAL_OBJECT);
	if (pSkeletonGroup) {
		// First skeleton
		AssetTree::iterator itSkeleton = pSkeletonGroup->begin();
		if (itSkeleton != pSkeletonGroup->end()) {
			for (AssetTree::pre_order_iterator it = itSkeleton.node()->pre_order_begin(); it != itSkeleton.node()->pre_order_end(); ++it)
				it->SetFlag(AssetData::STA_SELECTED); // Mark all bones in first skeleton as selected
		}
	}

	// Import selected assets with skeleton
	CSkeletonObject* pSO_new = new CSkeletonObject();
	if (!pImporter->Import(*pSO_new, aURI, pAssets, aFlags)) {
		delete pAssets;
		delete pSO_new;
		pImporter->EndSession(aURI);
		return NULL;
	}

	//Scan all animations in newly import skeletal object, assign a valid animation type if necessary
	auto anims = pSO_new->getRuntimeSkeleton()->getSkeletonAnimHeaderCount();
	for (size_t i = 0; i < anims; i++) {
		auto pSAH = pSO_new->getRuntimeSkeleton()->getSkeletonAnimHeaderByIndex(i);
		if (pSAH && !IS_VALID_ANIM_TYPE(pSAH->m_animType))
			pSAH->m_animType = CBoneAnimationNaming::AnimNameToType(DEFAULT_ANIMATION_TYPE);
	}

	if (pSO_dest != NULL) { // if merging
		// Now merge two skeletal object
		aFlags &= ~SKIMPORT_SECTIONMASK | ~SKIMPORT_CONFIGMASK | ~SKIMPORT_LODMASK;
		aFlags |= dstSection == -1 ? SKELETAL_INSERTAFTER_SECTION : SKELETAL_APPENDTO_SECTION;
		aFlags |= dstConfig == -1 ? SKELETAL_INSERTAFTER_CONFIG : SKELETAL_APPENDTO_CONFIG;
		aFlags |= dstLOD == -1 ? SKELETAL_INSERTAFTER_LOD : SKELETAL_REPLACE_LOD;
		MergeSkeletalObjects(*pSO_dest, *pSO_new, dstSection, dstConfig, dstLOD, aFlags);

		delete pSO_new;
	}

	// otherwise, return imported object (merged or newly created)
	delete pAssets;
	pImporter->EndSession(aURI);
	return pSO_dest != NULL ? pSO_dest : pSO_new;
}

CSkeletonObject* CAssetsDatabase::ImportSkeletalObjectByAssetTree(
	const std::string& aURI,
	int aFlags,
	AssetTree* assetTree,
	IAssetImporter* apImporter) {
	IAssetImporter* pImporter = apImporter;
	if (pImporter == NULL)
		pImporter = IAssetImporterFactory::Instance()->GetImporter(KEP_SKA_SKELETAL_OBJECT, aURI, aFlags);
	if (pImporter == NULL)
		return NULL;

	CSkeletonObject* newObject = new CSkeletonObject(RuntimeSkeleton::New(), new SkeletonConfiguration());

	// Attempt Object Import
	if (!pImporter->Import(*newObject, aURI, assetTree, aFlags)) {
		delete newObject;
		return NULL;
	}

	return newObject;
}

TextureDef CAssetsDatabase::ImportTexture(const std::string& uri, int flags) {
	TextureDef textureInfo;

	IAssetImporter* pImporter = IAssetImporterFactory::Instance()->GetImporter(KEP_TEXTURE, uri, flags);
	if (!pImporter)
		return textureInfo;

	if (!pImporter->Import(textureInfo, uri, NULL, flags)) {
		LogError("Import() FAILED - '" << uri << "'");
		return textureInfo;
	}

	return textureInfo;
}

bool CAssetsDatabase::GetTextureImportInfo(TextureDef& textureInfo, const std::string& importedFileName) {
	IAssetImporter* pImporter = IAssetImporterFactory::Instance()->GetImporter(KEP_TEXTURE, "dummy.dds", IMP_GENERAL_NOUI);
	return pImporter != nullptr && pImporter->GetTextureImportInfo(textureInfo, importedFileName);
}

//! Imports a deformable mesh in the file into the given position
//! If any *Index is < 0 or not found appending occurs at the appropriate list
bool CAssetsDatabase::ImportSkinnedMesh(
	CSkeletonObject& aSkeletalObject,
	const std::string& aURI,
	int aSectionIndex,
	int aConfigIndex,
	int aLodIndex,
	int aFlags,
	IAssetImporter* aImporter) {
	IAssetImporter* pImporter = aImporter;
	if (!pImporter)
		pImporter = IAssetImporterFactory::Instance()->GetImporter(KEP_SKA_SKINNED_MESH, aURI, aFlags);
	if (!pImporter)
		return false;
	if (pImporter->ImportSkinnedMesh(aSkeletalObject, aURI, aSectionIndex, aConfigIndex, aLodIndex, aFlags, NULL))
		return true;
	return false;
}

//! Imports a rigid mesh in the file and attaches it to the bone at the given index as a
//! rigid mesh at the given LOD. No loading occurs if the bone is not found.
//! If aLodexIndex < 0 or not found the mesh is appended to the LOD list.
bool CAssetsDatabase::ImportRigidMesh(
	RuntimeSkeleton& aSkeletalObject,
	const std::string& aURI,
	int aBoneIndex,
	int aLodIndex,
	int aFlags,
	IAssetImporter* aImporter) {
	IAssetImporter* pImporter = aImporter;
	if (!pImporter)
		pImporter = IAssetImporterFactory::Instance()->GetImporter(KEP_SKA_RIGID_MESH, aURI, aFlags);
	if (!pImporter)
		return false;
	return pImporter->ImportRigidMesh(aSkeletalObject, aURI, aBoneIndex, aLodIndex, aFlags);
}

//! Imports animation data into the given skeletal animated object
//! Data will only be added if all the bones in the file match (by name) those in the given object
//! If data is missing for some of the bones it will be assumed to be equal to the default
//!  transformation for that bone
bool CAssetsDatabase::ImportAnimation(
	CSkeletonObject& aSkeletalObject,
	const std::string& aURI,
	int& aAnimGLID,
	int aFlags,
	const AnimImportOptions* apOptions,
	IAssetImporter* aImporter) {
	bool ret = true;

	IAssetImporter* pImporter = aImporter;
	if (!pImporter)
		pImporter = IAssetImporterFactory::Instance()->GetImporter(KEP_SKA_ANIMATION, aURI, aFlags);
	if (!pImporter)
		return false;

	// Enumerate all animations contained at the specified URI
	AssetTree* pAssets = pImporter->PreviewAssets(aURI, KEP_SKA_ANIMATION, aFlags, NULL);
	if (!pAssets) {
		// PreviewAssets not supported by importer or no assets found in file: bring it in the legacy way
		ret = pImporter->ImportAnimation(aSkeletalObject, aURI, aAnimGLID, aFlags, NULL, apOptions);
	} else {
		// Instruct user to select asset for import
		if ((aFlags & IMP_GENERAL_UIMASK) == IMP_GENERAL_NOUI) {
			// If no UI available, mark all animations for import
			AssetTree* pAnimGroup = AssetData::GetAssetGroup(pAssets, KEP_SKA_ANIMATION);
			if (pAnimGroup) {
				for (auto it = pAnimGroup->pre_order_begin(); it != pAnimGroup->pre_order_end(); ++it)
					if (it->GetLevel() == AssetData::LVL_FLAT)
						it->SetFlag(AssetData::STA_SELECTED);
			}
		} else {
			// Import entire skeletal object by default, multiple selection OK
			AssetData::Level importLevel = AssetData::LVL_FLAT;
			KEP_ASSET_TYPE importType = KEP_SKA_ANIMATION;
			BOOL bAllowMultiSel = (aFlags & SKELETAL_REPLACE_ANIMATION) == SKELETAL_REPLACE_ANIMATION; // Single selection if replacing

			FOREVER { // Loop if user chooses to change naming convention
				std::auto_ptr<CAssetImporterDlg> dlg(new CAssetImporterDlg(pAssets, namingSyntaxs, NUM_NAMING_TYPES, importType, importLevel, bAllowMultiSel));
				if (dlg->DoModal() == IDOK)
					break;

				// Cleanup if failed
				delete pAssets;

				// Return failure if user does not request a re-run
				if (!dlg->m_bRebuildAssetTree)
					return NULL;

				// Redo previewing and dialog
				pAssets = pImporter->PreviewAssets(aURI, KEP_SKA_ANIMATION, aFlags, NULL);
			}
		}

		// Import selected assets with skeleton
		AssetTree* pAnimGroup = AssetData::GetAssetGroup(pAssets, KEP_SKA_ANIMATION);
		if (pAnimGroup) {
			for (auto it = pAnimGroup->pre_order_begin(); it != pAnimGroup->pre_order_end(); ++it) {
				if (it->GetLevel() == AssetData::LVL_FLAT && it->CheckFlag(AssetData::STA_SELECTED)) {
					if (!ImportAnimationByAssetDefinition(aSkeletalObject, it->GetURI(), aAnimGLID, aFlags, *it, apOptions, pImporter))
						ret = false;
				}
			}
		}
	}

	if (!ret) {
		// Import failed
		//		if ( CAssetsDatabase::pGDP ) {
		//			std::string gameBase = CAssetsDatabase::pGDP->PathBase(); //@@Watch: temporarily solution
		//			SetCurrentDirectoryW( Utf8ToUtf16(gameBase).c_str() );
		//		}
	}

	delete pAssets;
	return ret;
}

//! Try importing one single animation using asset definition obtained previously
bool CAssetsDatabase::ImportAnimationByAssetDefinition(
	CSkeletonObject& aSkeletalObject,
	const std::string& aURI,
	int& aAnimGLID,
	int aFlags,
	AssetData& aDef,
	const AnimImportOptions* apOptions,
	IAssetImporter* apImporter) {
	IAssetImporter* pImporter = apImporter;
	if (!pImporter)
		pImporter = IAssetImporterFactory::Instance()->GetImporter(KEP_SKA_ANIMATION, aURI, aFlags);
	if (!pImporter)
		return false;
	return pImporter->ImportAnimation(aSkeletalObject, aURI, aAnimGLID, aFlags, &aDef, apOptions);
}

//! \brief Import a vertex animation from file.
//! \param aMesh The mesh to be animated (return with data of first frame). Should be constructed and disposed by the caller.
//! \param aURI The URI where vertex animation data can be obtained.
//! \param aFlags Import flags.
//! \param aImporter Asset importer instance to be used for importing specified URI.
//! \return Vertex animation data.
CVertexAnimationModule* CAssetsDatabase::ImportVertexAnimation(
	CEXMeshObj& aMesh,
	const std::string& aURI,
	int aFlags,
	IClassifyAssetNode* classifyFunc,
	IAssetImporter* aImporter) {
	IAssetImporter* pImporter = aImporter;
	if (!pImporter)
		pImporter = IAssetImporterFactory::Instance()->GetImporter(KEP_VERTEX_ANIMATION, aURI, aFlags);
	if (!pImporter)
		return NULL;

	VERIFY_RETURN(pImporter->BeginSession(aURI, aFlags), NULL);

	AssetTree* pAssets = pImporter->PreviewAssets(aURI, KEP_VERTEX_ANIMATION, aFlags, classifyFunc);
	if (pAssets == NULL) {
		pImporter->EndSession(aURI);
		return NULL;
	}

	if ((aFlags & IMP_GENERAL_UIMASK) == IMP_GENERAL_NOUI) {
		// If no UI available, import first vertex animation found
		for (auto it = pAssets->pre_order_begin(); it != pAssets->pre_order_end(); ++it)
			if (it->GetLevel() == AssetData::LVL_FLAT && it->GetType() == KEP_VERTEX_ANIMATION)
				it->SetFlag(AssetData::STA_SELECTED);
	} else {
		FOREVER {
			std::auto_ptr<CAssetImporterDlg> dlg(new CAssetImporterDlg(pAssets, namingSyntaxs, NUM_NAMING_TYPES, KEP_VERTEX_ANIMATION, AssetData::LVL_FLAT));
			if (dlg->DoModal() == IDOK)
				break;

			// Cleanup if failed
			delete pAssets;

			// Return failure if user does not request a re-run
			if (!dlg->m_bRebuildAssetTree) {
				pImporter->EndSession(aURI);
				return NULL;
			}

			// Redo previewing and dialog
			pAssets = pImporter->PreviewAssets(aURI, KEP_VERTEX_ANIMATION, aFlags, classifyFunc);
		}
	}

	CVertexAnimationModule* newAnim = new CVertexAnimationModule();
	BOOL ret = FALSE;

	for (auto it = pAssets->post_order_begin(); it != pAssets->post_order_end(); ++it) {
		AssetData& asset = *it;
		if (asset.GetLevel() != AssetData::LVL_FLAT || !asset.CheckFlag(AssetData::STA_SELECTED))
			continue; // Skip not-selected nodes

		//@@Limitation: only import first submesh for now. Need to modify legacy mechanism to support multi-submesh vert-anim.
		int numSubmeshes = asset.GetNumSubmeshes();
		if (numSubmeshes > 0) {
			AssetURI uri(asset.GetURI()); // Parse URI string
			uri.subscript = 0; // Update subscript with 0 (first sub item only)

			ret = pImporter->ImportVertexAnimation(aMesh, *newAnim, uri.ToURIString(), &asset);
			if (!ret) {
				CStringA msg;
				msg.Format("Unable to import: %s", asset.GetURI().c_str());
				MessageBoxUtf8(0, msg, "TRACE", MB_OK);
			}
		}

		break; // Stop after first animation is loaded
	}

	//	if ( CAssetsDatabase::pGDP ) {
	//		std::string gameBase = CAssetsDatabase::pGDP->PathBase(); //@@Watch: temporarily solution
	//		SetCurrentDirectoryW( Utf8ToUtf16(gameBase).c_str() );
	//	}

	if (!ret) {
		delete newAnim;
		newAnim = NULL;
	}

	delete pAssets;
	pImporter->EndSession(aURI);
	return newAnim;
}

//! \brief Default asset naming syntax for static meshes.
std::string defaultStaticTokens[NUM_NAMING_TOKENS] = { "$(Type)", "_", "$(Object)", "_", "$(Part)", "_", "$(LOD)" };

//! \brief Default asset naming syntax for skeletal objects.
std::string defaultSkeletalTokens[NUM_NAMING_TOKENS] = { "$(Type)", "_", "$(Object)", "_", "$(Part)", "_", "$(Variation)", "_", "$(LOD)" };

//! \brief Variables available for asset naming syntax.
std::string NamingSyntax::resultVariables[NUM_RESULT_STRINGS] = { "Type", "Object", "Part", "Variation", "LOD" };

//! \brief Default asset naming syntaxs.
NamingSyntax NamingSyntax::defaultNamingConvention[NUM_NAMING_TYPES] = {
	NamingSyntax(defaultStaticTokens, 3, 4),
	NamingSyntax(defaultSkeletalTokens)
};

//! \brief Convert current asset naming syntax into a string both machine and human readable.
std::string NamingSyntax::getSyntaxString() {
	std::string result;
	for (int i = 0; i < NUM_NAMING_TOKENS; i++) {
		if (i == firstOptional)
			result += "[";
		result += tokens[i];
		if (i == lastOptional)
			result += "]";
	}
	return result;
}

//! \brief Default constructor for a asset naming syntax.
NamingSyntax::NamingSyntax() :
		type(INVALID), firstOptional(-1), lastOptional(-1) {
}

//! \brief Constructor to setup a basic asset naming syntax.
//! \param t Syntax tokens
//! \param firstOpt Index of first optional token
//! \param lastOpt Index of last optional token
NamingSyntax::NamingSyntax(const std::string(&t)[NUM_NAMING_TOKENS], int firstOpt, int lastOpt) :
		type(BASIC), firstOptional(firstOpt), lastOptional(lastOpt) {
	for (int i = 0; i < NUM_NAMING_TOKENS; i++) tokens[i] = t[i];
}

//! \brief Constructor to setup a regex asset naming syntax.
//! \param re Regular expression used for asset name parsing.
//! \param reo Array of names of bindable variables. Regex token \1 -> variable named by reo[0], etc.
NamingSyntax::NamingSyntax(const std::string& re, const std::string(&reo)[NUM_NAMING_REOUTS]) :
		type(REGEX), regex(re), firstOptional(-1), lastOptional(-1) {
	for (int i = 0; i < NUM_NAMING_REOUTS; i++) regexOutputs[i] = reo[i];
}

//! \brief Parse an asset name and return the bound values of all variable.
//! \param input Asset name to be parsed
//! \param result Bound values of all parsed variables.
//! \return TRUE if succeeded. FALSE otherwise.
BOOL NamingSyntax::parseString(const std::string& input, std::string(&result)[NUM_RESULT_STRINGS]) {
	// Initialize Results to be empty
	for (int i = 0; i < NUM_RESULT_STRINGS; i++)
		result[i].clear();

	// Parse string with basic or regex type syntax
	if (type == BASIC ? parseStringBasic(input, result) : parseStringRegex(input, result))
		return TRUE;

	// Parsing Failed
	// Empty Results to clean up all residuals
	for (int i = 0; i < NUM_RESULT_STRINGS; i++)
		result[i].clear();
	return FALSE;
}

//! \brief Parse an asset name with basic naming syntax and return the bound values of all variable.
//! \param input Asset name to be parsed
//! \param result Bound values of all parsed variables.
//! \return TRUE if succeeded. FALSE otherwise.
BOOL NamingSyntax::parseStringBasic(const std::string& input, std::string(&result)[NUM_RESULT_STRINGS]) {
	std::string unparsedString = input;

	int actualTokenCount = NUM_NAMING_TOKENS;
	for (int i = actualTokenCount - 1; i >= 0; i--, actualTokenCount--)
		if (!tokens[i].empty())
			break;

	// Fix for some odd situation
	if (firstOptional >= actualTokenCount)
		firstOptional = -1;
	if (lastOptional >= actualTokenCount)
		lastOptional = -1;

	ASSERT(firstOptional < actualTokenCount && lastOptional < actualTokenCount);

	if (firstOptional == -1 && lastOptional == -1) {
		// Parse entire string with all tokens (FORWARD, MATCH_WHOLE, ALL_TOKENS)
		if (!doParseStringBasic(unparsedString, result, tokens, actualTokenCount, NULL, 0, FALSE, FALSE, FALSE))
			return FALSE;
	} else {
		ASSERT(firstOptional != -1 && lastOptional != -1 && lastOptional >= firstOptional);
		int optionalTokenCount = lastOptional - firstOptional + 1;

		// Parse required prefix (FORWARD, MATCH_PARTIAL, ALL_TOKENS)
		if (firstOptional > 0) {
			// Two optional delimiters to be used for last token in current parsing
			// 1. token at firstOptional
			// 2. token at lastOptional+1
			// Note that either of these two can be non-literal (will be skipped during parsing)
			std::string endingDlmtrs[2];
			int endingDlmtrCnt = 0;
			endingDlmtrs[endingDlmtrCnt++] = tokens[firstOptional];
			if (lastOptional < actualTokenCount - 1)
				endingDlmtrs[endingDlmtrCnt++] = tokens[lastOptional + 1];

			if (!doParseStringBasic(unparsedString, result, tokens, firstOptional, endingDlmtrs, endingDlmtrCnt, FALSE, TRUE, FALSE))
				return FALSE;
		}

		// Parse required postfix (BACKWARD, MATCH_PARTIAL, ALL_TOKENS)
		if (lastOptional < actualTokenCount - 1) {
			// Use all optional tokens as additional delimiters (non-literal tokens will be skipped)
			if (!doParseStringBasic(unparsedString, result, tokens + lastOptional + 1, actualTokenCount - 1 - lastOptional, tokens + firstOptional, optionalTokenCount, TRUE, TRUE, FALSE))
				return FALSE;
		}

		// Parse optional (FORWARD, MATCH_WHOLE, OPTIONAL_TOKENS)
		if (!doParseStringBasic(unparsedString, result, tokens + firstOptional, optionalTokenCount, NULL, 0, FALSE, FALSE, TRUE))
			return FALSE;
	}

	ASSERT(unparsedString.empty());
	return TRUE;
}

//! \brief Parse an asset name with regex naming syntax and return the bound values of all variable.
//! \param input Asset name to be parsed
//! \param result Bound values of all parsed variables.
//! \return TRUE if succeeded. FALSE otherwise.
BOOL NamingSyntax::parseStringRegex(const std::string& input, std::string(&result)[NUM_RESULT_STRINGS]) {
	ASSERT(FALSE);
	//@@Todo: implement regular expression based naming convention
	return FALSE;
}

//! \brief Parse with all tokens, partial string match OK
BOOL NamingSyntax::doParseStringBasic(std::string& unparsedString, std::string(&result)[NUM_RESULT_STRINGS],
	const std::string* tokenArray, int tokenCount,
	const std::string* endingDlmtrs, int endingDlmtrCnt,
	BOOL bBackward, BOOL bPartial, BOOL bOptional) {
	int inc = bBackward ? -1 : 1;

	int tokenLoop = bBackward ? tokenCount - 1 : 0;
	for (; tokenLoop >= 0 && tokenLoop < tokenCount && !unparsedString.empty(); tokenLoop += inc) { // For all tokens or entire string
		BOOL bLastToken = !bBackward && tokenLoop == tokenCount - 1 || bBackward && tokenLoop == 0;
		if (bLastToken)
			ASSERT(!tokenArray[tokenLoop].empty()); // last token must not be empty

		if (tokenArray[tokenLoop].empty())
			continue;

		std::string var;
		int resultID;
		if (NamingSyntax::parseToken(tokenArray[tokenLoop], var, resultID) == TOKEN_LITERAL) {
			// is literal
			if (!NamingSyntax::parseSingleLiteral(unparsedString, tokenArray[tokenLoop], bBackward))
				break; // parsing failed
			continue;
		}

		// is variable
		const std::string* delimiters = NULL;
		int numDelimiters = 0;

		if (!bLastToken) {
			// check to see if we have more tokens
			int delimiterIdx = tokenLoop + inc;
			for (; delimiterIdx >= 0 && delimiterIdx < tokenCount; delimiterIdx += inc)
				if (!tokenArray[delimiterIdx].empty())
					break; // stop if non-empty token found

			if (delimiterIdx >= 0 && delimiterIdx < tokenCount) {
				if (NamingSyntax::parseToken(tokenArray[delimiterIdx]) != TOKEN_LITERAL) {
					// is variable -- unable to parse two consecutive variables: bad syntax
					break; // failed
				}

				delimiters = &tokenArray[delimiterIdx]; // use next delimiter
				numDelimiters = 1;
			}
		} else {
			// last token
			delimiters = endingDlmtrs;
			numDelimiters = endingDlmtrCnt;
		}

		std::string value;
		if (!NamingSyntax::parseSingleVariable(unparsedString, value, delimiters, numDelimiters, bBackward)) {
			// try again for last token without no delimiters
			if (!bLastToken || !NamingSyntax::parseSingleVariable(unparsedString, value, NULL, 0, bBackward))
				break; // Parsing failed
		}

		if (resultID != -1)
			result[resultID] = value;

	} // Loop for all tokens or entire string (whichever ends first)

	// If partial parsing allowed, unparsedString might contain remainings.
	// If optional tokens parsing, tokens need not be matched at its entirety.
	return (bPartial || unparsedString.empty()) && (bOptional || tokenLoop < 0 || tokenLoop == tokenCount);
}

// Parse string by delimiter(s): return entire string if no delimiters.
// It's allowed to have non-literal tokens in delimiter list, which will be skipped during processing.
BOOL NamingSyntax::parseSingleVariable(std::string& unparsedString, std::string& result, const std::string* delimiters, int delimiterCount, BOOL bBackward) {
	if (delimiterCount == 0) {
		// no delimiters, using entire string
		result = unparsedString;
		unparsedString = "";
		return TRUE;
	}

	int inc = bBackward ? -1 : 1;

	// delimiters
	size_t pos = unparsedString.length(); // use entire string if no literal token found
	int dlmLoop = bBackward ? delimiterCount - 1 : 0;
	for (; dlmLoop >= 0 && dlmLoop < delimiterCount; dlmLoop += inc) {
		std::string dlm = delimiters[dlmLoop];
		if (parseToken(dlm) == TOKEN_VARIABLE)
			continue; // skip non-literal tokens

		pos = bBackward ? unparsedString.rfind(dlm) : unparsedString.find(dlm);
		if (pos != std::string::npos) {
			if (bBackward)
				pos += dlm.length(); // if backward matching: skip delimiter itself
			break;
		}
	}

	if (pos == -1) // listed delimiter not present in the string
		return FALSE; // prasing failed

	std::string partA = unparsedString.substr(0, pos);
	std::string partB = unparsedString.substr(pos);
	result = bBackward ? partB : partA;
	unparsedString = bBackward ? partA : partB;
	return TRUE;
}

//! \brief Parse one single literal (usually delimiters)
//! \param unparsedString String to be parsed. Parsed portion will be removed from string when returned.
//! \param literalToken Expected literal token.
//! \param bBackward TRUE if parse from the back of the string. FALSE otherwise.
//! \return TRUE if succeeded. FALSE otherwise.
BOOL NamingSyntax::parseSingleLiteral(std::string& unparsedString, const std::string& literalToken, BOOL bBackward) {
	int tokenLen = literalToken.length();

	std::string tmp = bBackward ? unparsedString.substr(unparsedString.length() - tokenLen) : unparsedString.substr(0, tokenLen);
	if (tmp != literalToken)
		return FALSE; // failed

	// matching OK, move forward (or backward)
	unparsedString = bBackward ? unparsedString.substr(0, unparsedString.length() - tokenLen) : unparsedString.substr(tokenLen);
	return TRUE;
}

//! \brief Parse the specified token and return token type. Also return variable name and ID if it's a variable.
//! \return TOKEN_VARIABLE if variable, TOKEN_LITERAL if literal.
NamingSyntax::TokenType NamingSyntax::parseToken(const std::string& token, std::string& varName, int& resultID) {
	NamingSyntax::TokenType ret = parseToken(token);

	if (ret == TOKEN_VARIABLE) {
		varName = token.substr(2, token.length() - 3);
		resultID = RESULT_UNKNOWN;
		for (int i = 0; i < NUM_RESULT_STRINGS; i++)
			if (STLCompareIgnoreCase(varName, NamingSyntax::resultVariables[i]) == 0)
				resultID = i;
	}

	return ret;
}

//! \brief Parse the specified token and return token type.
//! \return TOKEN_VARIABLE if variable, TOKEN_LITERAL if literal.
NamingSyntax::TokenType NamingSyntax::parseToken(const std::string& token) {
	if (token.length() > 3 && token.substr(0, 2) == "$(" && token.substr(token.length() - 1) == ")")
		return TOKEN_VARIABLE;

	return TOKEN_LITERAL;
}

//! \brief Remap bone indices in vertex assignments from old skeleton to new skeleton.
//! \param dmesh Deformable mesh to be converted
//! \param sourceSkeleton Source skeleton where the deformable mesh is part of.
//! \param targetSkeleton Target skeleton where the deformable mesh is converted into.
//! \param unmatchedBones Returns a list of unmatched bones.
//! \return FALSE if there is unmatched bones. TRUE otherwise.
BOOL CAssetsDatabase::RemapDeformableMeshBones(CDeformableMesh& dmesh, RuntimeSkeleton& sourceSkeleton, RuntimeSkeleton& targetSkeleton, std::set<std::string>& unmatchedBones) {
	BOOL errorFound = FALSE;

	// Pre-build bone mapping table
	std::map<UINT, UINT> src2TgtBoneIdxMap; // map<sourceBoneIndex, targetBoneIndex>
	UINT sourceBoneIndex = 0;
	for (POSITION posBone = sourceSkeleton.getBoneList()->GetHeadPosition(); posBone != NULL; sourceBoneIndex++) {
		auto pBone = static_cast<const CBoneObject*>(sourceSkeleton.getBoneList()->GetNext(posBone));
		if (pBone == NULL || pBone->m_boneName.empty()) {
			// Invalid bone data, unable to convert
			//@@Todo: log more detailed error report
			errorFound = TRUE;
			continue;
		}

		std::string boneName = pBone->m_boneName;
		int targetBoneIndex = targetSkeleton.getBoneIndexByName(boneName, true);
		if (targetBoneIndex != -1)
			src2TgtBoneIdxMap.insert(std::make_pair(sourceBoneIndex, (UINT)targetBoneIndex));
		else {
			// Unmatched bone name, unable to convert
			errorFound = TRUE;
			unmatchedBones.insert(boneName);
		}
	}

	UINT lodNo = 0;
	for (POSITION pos = dmesh.GetDeformableVisList()->GetHeadPosition(); pos != NULL; lodNo++) {
		CDeformableVisObj* pLod = dynamic_cast<CDeformableVisObj*>(dmesh.GetDeformableVisList()->GetNext(pos));
		ASSERT(pLod != NULL);
		if (pLod == NULL)
			continue;

		// Convert bone indices
		switch (pLod->m_blendedType) {
			case SkinVertexBlendType::Disabled:
				for (auto& assignment : pLod->m_vertexAssignments) {
					auto itrBoneMap = src2TgtBoneIdxMap.find(assignment.boneIndex);
					if (itrBoneMap != src2TgtBoneIdxMap.end()) {
						assignment.boneIndex = (byte)itrBoneMap->second;
					}
				}
				break;

			case SkinVertexBlendType::Position_Normal:
				for (auto& assignment : pLod->m_blendedVertexAssignments) {
					for (auto& boneWeight : assignment.vertexBoneWeights) {
						auto itrBoneMap = src2TgtBoneIdxMap.find(boneWeight.boneIndex);
						if (itrBoneMap != src2TgtBoneIdxMap.end()) {
							boneWeight.boneIndex = (byte)itrBoneMap->second;
						}
					}
				}
				break;

			case SkinVertexBlendType::Position_Normal_Tangent:
				assert(false); // deprecated
				break;

			default:
				assert(false);
				break;
		}

		if (pLod->GetReMesh()) {
			// Rebuild and replace remesh if already exists
			MeshRM::Instance()->SetCurrentBoneList(targetSkeleton.getBoneList());
			MeshRM::Instance()->SetCurrentLOD(lodNo);
			ReMesh* newSkinMesh = MeshRM::Instance()->CreateReSkinMeshPersistent(pLod, false);
			pLod->SetReMesh(newSkinMesh);
			if (dmesh.m_lodHashes != NULL)
				dmesh.m_lodHashes[lodNo] = newSkinMesh->GetHash();
		}
	}

	return !errorFound;
}

//! \brief Compare two skeletons: A contains B if both A&B share a same root bone, A contains all bones as in B with same hierarchy, A might contains extra bones at the ends of limbs.
//! \return 1 if first skeleton contains second, -1 if error found, 0 otherwise.
int CAssetsDatabase::CompareSkeletons(RuntimeSkeleton& first, RuntimeSkeleton& second) {
	VERIFY_RETURN(first.getBoneList() != nullptr && second.getBoneList() != nullptr, -1);

	int boneCount1 = first.getBoneList()->GetCount();
	int boneCount2 = second.getBoneList()->GetCount();

	if (boneCount1 < boneCount2)
		return 0;

	const CBoneObject* firstRoot = first.getBoneList()->getBoneByIndex(0);
	const CBoneObject* secondRoot = second.getBoneList()->getBoneByIndex(0);
	VERIFY_RETURN(firstRoot != NULL && secondRoot != NULL, -1);

	if (firstRoot->m_boneName != secondRoot->m_boneName) // Compare root bones (optional?)
		return 0;

	int numBonesCompared = 0;
	return DoCompareSkeletons(first, *firstRoot, second, *secondRoot, numBonesCompared);
}

int CAssetsDatabase::DoCompareSkeletons(const RuntimeSkeleton& first, const CBoneObject& firstRoot, const RuntimeSkeleton& second, const CBoneObject& secondRoot, int& numBonesCompared) {
	for (POSITION pos = second.getBoneList()->GetHeadPosition(); pos != NULL;) {
		auto bone2 = static_cast<const CBoneObject*>(second.getBoneList()->GetNext(pos));
		VERIFY_RETURN(bone2 != NULL, -1);

		if (bone2 != &secondRoot && bone2->m_parentName == secondRoot.m_boneName) {
			numBonesCompared++; // Mandatory recursion termination check, in case there is a loop in the skeleton
			VERIFY_RETURN(numBonesCompared <= second.getBoneList()->GetCount(), -1);

			int index;
			auto bone1 = first.getBoneList()->getBoneObjectByName(bone2->m_boneName, &index);
			if (bone1 == NULL) {
				CStringA tmpName = bone2->m_boneName.c_str();
				tmpName.Replace(' ', '_');
				bone1 = first.getBoneList()->getBoneObjectByName((const char*)tmpName, &index, true);
			}
			if (bone1 == NULL)
				return 0;

			int ret = DoCompareSkeletons(first, *bone1, second, *bone2, numBonesCompared);
			if (ret != 1)
				return ret;
		}
	}

	return 1;
}

AssetData::AssetMesh::~AssetMesh() {
	if (m_pExMesh) {
		m_pExMesh->SafeDelete();
		delete m_pExMesh;
		m_pExMesh = nullptr;
	}
	if (m_pDeMesh) {
		delete m_pDeMesh;
		m_pDeMesh = nullptr;
	}
	if (m_pHiMesh) {
		m_pHiMesh->SafeDelete();
		delete m_pHiMesh;
		m_pHiMesh = nullptr;
	}
}
