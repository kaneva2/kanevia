///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "tcl/tree.h"
#include "IAssetsDatabase.h"
#include "AssetTree.h"
#include "CEXMeshClass.h"
#include "CHierarchyMesh.h"
#include "CDeformableMesh.h"
#include "CSkeletonObject.h"
#include "tcl/tree.h"
#include "SkeletonConfiguration.h"
#include "RuntimeSkeleton.h"

using namespace KEP;
using namespace tcl;

IAssetImporterFactory::IAssetImporterFactory() {
}

IAssetImporterFactory::~IAssetImporterFactory() {
}

IAssetsDatabase::IAssetsDatabase() {
}

IAssetsDatabase::~IAssetsDatabase() {
}

AssetData IAssetsDatabase::ImportStaticMesh(const char* const aFileName, int aFlags /*= 0*/, IAssetImporter* aImporter /*= NULL*/) {
	return ImportStaticMesh(std::string(aFileName), aFlags, aImporter);
}

#define DuplicatedNameSolver(ItemType, NameField, pNewItem, pAllItems)                                                                        \
	{                                                                                                                                         \
		/* Loop thru existing items and look for duplicate names, postfix current name if duplicated */                                       \
		int postFix = 0;                                                                                                                      \
		CStringA nameCandidate = pNewItem->NameField;                                                                                         \
		for (POSITION pos = pAllItems->GetHeadPosition(); pos != NULL;) {                                                                     \
			ItemType* tmp = dynamic_cast<ItemType*>(pAllItems->GetNext(pos));                                                                 \
			ASSERT(tmp != NULL);                                                                                                              \
                                                                                                                                              \
			if (tmp == pNewItem)                                                                                                              \
				continue;                                                                                                                     \
                                                                                                                                              \
			if (tmp->NameField == nameCandidate) {                                                                                            \
				postFix++;                                                                                                                    \
				ASSERT(postFix <= pAllItems->GetCount()); /* pigeonhole principle: not supposed to fail unless something strange happened. */ \
                                                                                                                                              \
				nameCandidate.Format("%s_%d", pNewItem->NameField, postFix);                                                                  \
                                                                                                                                              \
				/* restart entire loop */                                                                                                     \
				pos = pAllItems->GetHeadPosition();                                                                                           \
			}                                                                                                                                 \
		}                                                                                                                                     \
		pNewItem->NameField = nameCandidate;                                                                                                  \
	}

//!*********************************************************************************************
//! \brief Return a pointer to an existing or newly-created section of the skeletal object.
//! \param aSkeletalObject The skeletal object to be modified.
//! \param aSectionIndex Index of the reference section.
//! \param aFlags SKELETAL_REPLACE_SECTION, SKELETAL_INSERTAFTER_SECTION or SKELETAL_APPENDTO_SECTION. Can be combined with other flags with different bit masks.
//! \param sectionName Name of the new section (or sometimes, used to identify an existing section too).
//!
//! This function is called when caller wants to modify an existing skeletal object. Based on the combination of arguments,
//! it will do one of the following:
//! (1) ANY FLAG, aSectionIndex == no-match, sectionName is empty or no-match
//!     A new section will be created and appended to the end of this skeletal object. Section name will be used if not empty.
//!     Otherwise use a default name. In addition, if the flag is not SKELETAL_INSERTAFTER_SECTION, an assertion failure will
//!     be triggered in debug to indicate a bad argument.
//! (2) SKELETAL_REPLACE_SECTION, aSectionIndex == valid index, sectionName == any
//!     An existing section identified by aSectionIndex will be emptied, disposed and recreated.
//!     sectionName will be used to name the newly-created section if not empty. Otherwise, use old seciton name.
//! (3) SKELETAL_REPLACE_SECTION, aSectionIndex == no-match, sectionName matches one existing section.
//!     An existing section identified by section name will be emptied, disposed and recreated. Section name is preserved.
//! (4) SKELETAL_INSERTAFTER_SECTION, aSectionIndex == valid index, sectionName = any
//!     A new section will be created and inserted after the section identified by aSectionIndex. sectionName will be used
//!     for the new sectino if not empty. Otherwise use a default name.
//! (5) SKELETAL_INSERTAFTER_SECTION, aSectionIndex == no-match, sectionName matches one existing section
//!     A new section will be created and inserted after the section identified by sectionName.
//!     NOTE that new section uses default name in this case.
//! (6) SKELETAL_APPENDTO_SECTION, aSectionIndex == valid index, sectionName empty
//!		An existing section identified by aSectionindex will be returned to the caller.
//! (7) SKELETAL_APPENDTO_SECTION, aSectionIndex == no match, sectionName matches one existing section
//!		An existing section identified by sectionName will be returned to the caller.
//! (8) SKELETAL_APPENDTO_SECTION, aSectionIndex == valid index, sectionName not empty
//!     Similar to (7) but with an assertion failure (debug only) to indicate that sectionName is ignored.

CAlterUnitDBObject* IAssetImporter::GetSkeletonSectionPtr(CSkeletonObject& aSkeletalObject, int aSectionIndex, ImportFlags aFlags, const std::string& sectionName) {
	SkeletonConfiguration* skeletalCfg = aSkeletalObject.getSkeletonConfiguration();

	if (skeletalCfg->m_meshSlots == NULL)
		skeletalCfg->m_meshSlots = new CAlterUnitDBObjectList();

	CAlterUnitDBObjectList* sectionListPtr = skeletalCfg->m_meshSlots;

	// get pointer to section
	CAlterUnitDBObject* sectionPtr = 0;
	POSITION sectionPos = sectionListPtr->FindIndex(aSectionIndex);
	if (sectionPos == NULL && !sectionName.empty()) { // No match with sectionIndex, try sectionName
		for (POSITION pos = sectionListPtr->GetHeadPosition(); pos != NULL;) {
			CAlterUnitDBObject* tmp = dynamic_cast<CAlterUnitDBObject*>(sectionListPtr->GetNext(pos));
			ASSERT(tmp != NULL);

			if (tmp->m_name == sectionName.c_str()) {
				sectionPos = pos;
				break;
			}
		}
	}

	BOOL isNewSection = TRUE;
	if (sectionPos != NULL) {
		CAlterUnitDBObject* refSectionPtr = dynamic_cast<CAlterUnitDBObject*>(sectionListPtr->GetAt(sectionPos));
		ASSERT(refSectionPtr != NULL);

		switch (aFlags.section) {
			case SKIMPORT_REPLACE: {
				sectionPtr = refSectionPtr;
				CStringA oldSectionName = sectionPtr->m_name; // keep the old name
				sectionPtr->SafeDelete();
				delete sectionPtr;
				sectionPtr = new CAlterUnitDBObject();
				sectionPtr->m_name = sectionName.empty() ? oldSectionName : sectionName.c_str();
				sectionPtr->m_configurations = new CDeformableMeshList();
				sectionListPtr->SetAt(sectionPos, sectionPtr);

				aSkeletalObject.getRuntimeSkeleton()->setSkinnedMeshCount(aSkeletalObject.getRuntimeSkeleton()->getSkinnedMeshCount() + 1);
				if (skeletalCfg->m_charConfig.getItemCount() == 0)
					skeletalCfg->m_charConfig.initBaseItem(aSkeletalObject.getRuntimeSkeleton()->getSkinnedMeshCount());

				break;
			}
			case SKIMPORT_INSERTAFTER: {
				sectionPtr = new CAlterUnitDBObject();
				sectionPtr->m_name = refSectionPtr->m_name == sectionName.c_str() ? "New_Section" : sectionName.c_str();
				sectionPtr->m_configurations = new CDeformableMeshList();
				sectionListPtr->InsertAfter(sectionPos, sectionPtr);

				aSkeletalObject.getRuntimeSkeleton()->setSkinnedMeshCount(aSkeletalObject.getRuntimeSkeleton()->getSkinnedMeshCount() + 1);
				// allocate or expand aSkeletalObject.m_spawnCfg
				skeletalCfg->m_charConfig.initBaseItem(aSkeletalObject.getRuntimeSkeleton()->getSkinnedMeshCount());
				break;
			}
			case SKIMPORT_APPENDTO:
			default:
				sectionPtr = refSectionPtr;
				isNewSection = FALSE;
				break;
		}
	} else {
		ASSERT(aFlags.section == SKIMPORT_INSERTAFTER);

		sectionPtr = new CAlterUnitDBObject();
		sectionPtr->m_name = sectionName.empty() ? "New_Section" : sectionName.c_str();
		sectionPtr->m_configurations = new CDeformableMeshList();
		sectionListPtr->AddTail(sectionPtr);

		aSkeletalObject.getRuntimeSkeleton()->setSkinnedMeshCount(aSkeletalObject.getRuntimeSkeleton()->getSkinnedMeshCount() + 1);
		// allocate or expand aSkeletalObject.m_spawnCfg
		skeletalCfg->m_charConfig.initBaseItem(aSkeletalObject.getRuntimeSkeleton()->getSkinnedMeshCount());
	}

	// Solve duplicated names if section is newly created.
	if (isNewSection)
		DuplicatedNameSolver(CAlterUnitDBObject, m_name, sectionPtr, sectionListPtr);

	return sectionPtr;
}

//!*********************************************************************************************
//! \brief Return a pointer to an existing or newly-created config of a skinned-mesh section.
//! \param aSectionPtr The section to be modified.
//! \param aConfigIndex Index of the reference configuration.
//! \param aFlags SKELETAL_REPLACE_CONFIG, SKELETAL_INSERTAFTER_CONFIG or SKELETAL_APPENDTO_CONFIG. Can be combined with other flags with different bit masks.
//! \param configName Name of the new configuration (or sometimes, used to identify an existing configuration too).
//!
//! Behavior similar to GetSkeletonSectionPtr(). See GetSkeletonSectionPtr() above.
CDeformableMesh* IAssetImporter::GetSectionConfigPtr(CAlterUnitDBObject& aSectionPtr, int aConfigIndex, ImportFlags aFlags, const std::string& configName) {
	// get pointer to config or append if not found
	CDeformableMesh* configPtr = 0;
	POSITION configPos = aSectionPtr.m_configurations->FindIndex(aConfigIndex);
	if (configPos == NULL && !configName.empty()) { // No match with configIndex, try configName
		int idx = 0;
		for (POSITION pos = aSectionPtr.m_configurations->GetHeadPosition(); pos != NULL; idx++) {
			CDeformableMesh* tmp = dynamic_cast<CDeformableMesh*>(aSectionPtr.m_configurations->GetNext(pos));
			ASSERT(tmp != NULL);

			if (tmp->m_dimName == configName.c_str()) { //@@Watch: maybe aSectionPtr.m_meshNames[idx] ??
				configPos = pos;
				break;
			}
		}
	}

	BOOL isNewConfig = TRUE;
	if (configPos != NULL) {
		CDeformableMesh* refConfigPtr = dynamic_cast<CDeformableMesh*>(aSectionPtr.m_configurations->GetAt(configPos));
		ASSERT(refConfigPtr != NULL);

		switch (aFlags.config) {
			case SKIMPORT_REPLACE: {
				configPtr = refConfigPtr;
				CStringA oldDimName = configPtr->m_dimName; // preserve values
				BOOL oldPublicDim = configPtr->m_publicDim;
				delete configPtr;
				configPtr = new CDeformableMesh();
				configPtr->m_dimName = configName.empty() ? oldDimName : configName.c_str();
				configPtr->m_publicDim = oldPublicDim;
				aSectionPtr.m_configurations->SetAt(configPos, configPtr);
				break;
			}
			case SKIMPORT_INSERTAFTER: {
				configPtr = new CDeformableMesh();
				configPtr->m_dimName = refConfigPtr->m_dimName == configName.c_str() ? "New_Config" : configName.c_str();
				configPtr->m_publicDim = TRUE;
				aSectionPtr.m_configurations->InsertAfter(configPos, configPtr);
				break;
			}
			case SKIMPORT_APPENDTO: // append data to this config
			default:
				configPtr = refConfigPtr;
				isNewConfig = FALSE;
				break;
		}
	} else {
		ASSERT(aFlags.config == SKIMPORT_INSERTAFTER);

		// append a new config
		configPtr = new CDeformableMesh();
		configPtr->m_dimName = configName.empty() ? "New_Config" : configName.c_str();
		configPtr->m_publicDim = TRUE;
		aSectionPtr.m_configurations->AddTail(configPtr);
	}

	// Solve duplicated names if configuration is newly created.
	if (isNewConfig)
		DuplicatedNameSolver(CDeformableMesh, m_dimName, configPtr, aSectionPtr.m_configurations);

	return configPtr;
}

CDeformableMesh* IAssetImporter::GetSectionConfigPtr(CSkeletonObject& aSkeletalObject, int aSectionIndex, int aConfigIndex, ImportFlags aFlags, const std::string& sectionName, const std::string& configName) {
	CAlterUnitDBObject* sectionPtr = GetSkeletonSectionPtr(aSkeletalObject, aSectionIndex, aFlags, sectionName);
	if (sectionPtr == 0)
		return 0;
	return GetSectionConfigPtr(*sectionPtr, aConfigIndex, aFlags, configName);
}

void IAssetImporter::SetConfigLODMeshPtr(CDeformableVisList& aLodList, CDeformableVisObj* aLodMeshPtr, int aLodIndex, ImportFlags aFlags) {
	// get pointer to LOD or append if not found
	POSITION lodPos = aLodList.FindIndex(aLodIndex);
	if (lodPos != NULL) {
		// found that LOD in destination object
		if (aFlags.lod == SKIMPORT_REPLACE) {
			// replace this LOD mesh with a new one
			CDeformableVisObj* oldLodPtr = (CDeformableVisObj*)aLodList.GetAt(lodPos);
			delete oldLodPtr;
			aLodList.SetAt(lodPos, aLodMeshPtr);
		} else {
			// insert a new LOD mesh after this one
			aLodList.InsertAfter(lodPos, aLodMeshPtr);
		}
	} else {
		// append a new LOD mesh
		aLodList.AddTail(aLodMeshPtr);
	}
}

void IAssetImporter::SetConfigLODMeshPtr(CHierarchyVisList& aLodList, CHierarchyVisObj* aLodMeshPtr, int aLodIndex, ImportFlags aFlags) {
	// get pointer to LOD or append if not found
	POSITION lodPos = aLodList.FindIndex(aLodIndex);
	if (lodPos != NULL) {
		// found that LOD in destination object
		if (aFlags.lod == SKIMPORT_REPLACE) {
			// replace this LOD mesh with a new one
			CHierarchyVisObj* oldLodPtr = (CHierarchyVisObj*)aLodList.GetAt(lodPos);
			delete oldLodPtr;
			aLodList.SetAt(lodPos, aLodMeshPtr);
		} else {
			// insert a new LOD mesh after this one
			aLodList.InsertAfter(lodPos, aLodMeshPtr);
		}
	} else {
		// append a new LOD mesh
		aLodList.AddTail(aLodMeshPtr);
	}
}

AssetData::AssetGroupDef AssetData::assetGroupDefs[] = {
	{ KEP_STATIC_MESH, "Static", "All Static Meshes" },
	{ KEP_SKA_SKINNED_MESH, "Skinned", "All Skinned Meshes" },
	{ KEP_SKA_RIGID_MESH, "Rigid", "All Rigid Meshes" },
	{ KEP_SKA_SKELETAL_OBJECT, "Skeleton", "All Skeletons" },
	{ KEP_SKA_ANIMATION, "Animation", "All Animations" },
	{ KEP_VERTEX_ANIMATION, "VertexAnimation", "All Vertex Animations" },
};

//! Return name and description strings for a specific asset type to be used for grouping in asset tree
void AssetData::GetAssetGroupNameFromType(KEP_ASSET_TYPE type, std::string& name, std::string& desc) {
	name = "Unknown";
	desc = "Unknown";

	for (int i = 0; i < sizeof(assetGroupDefs) / sizeof(AssetGroupDef); i++) {
		if (assetGroupDefs[i].m_assetType == type) {
			name = assetGroupDefs[i].m_name;
			desc = assetGroupDefs[i].desc;
			break;
		}
	}
}

//! Return asset type by asset group name
KEP_ASSET_TYPE AssetData::GetAssetTypeByGroupName(const std::string& name) {
	for (int i = 0; i < sizeof(assetGroupDefs) / sizeof(AssetGroupDef); i++) {
		if (assetGroupDefs[i].m_name == name)
			return assetGroupDefs[i].m_assetType;
	}

	return KEP_UNKNOWN_TYPE;
}

//! Return sub-tree based on asset type
AssetTree* AssetData::GetAssetGroup(AssetTree* pRoot, KEP_ASSET_TYPE type) {
	for (AssetTree::iterator itGroup = pRoot->begin(); itGroup != pRoot->end(); ++itGroup) {
		// Look for skinned mesh groups
		KEP_ASSET_TYPE groupType = AssetData::GetAssetTypeByGroupName(itGroup->GetKey());
		if (groupType == type)
			return itGroup.node();
	}

	return NULL;
}

#include "AssetImporterMath.h"
ImpFloatVector3::operator Vector3f() {
	Vector3f ret;
	ret.x = x;
	ret.y = y;
	ret.z = z;
	return ret;
}

//! Check asset compatibility between two level:type pairs
//! \return INT_MAX if incompatible. 0 if compatible. +/-1 if partially compatible (+1:level1 higher than level2 -1:level1 lower than level2)
//!
//! Note that greater level number in means lower level.
AssetData::Compatibility AssetData::CheckAssetCompatibility(AssetData::Level level1, KEP_ASSET_TYPE type1, AssetData::Level level2, KEP_ASSET_TYPE type2) {
	if (level1 == AssetData::LVL_FLAT || level2 == AssetData::LVL_FLAT) {
		// Check type compatibility if level is LVL_FLAT
		if (level1 != level2 || type1 != type2)
			return AC_INCOMPAT; // level1:type1 is incompatible with level2:type2

		return AC_EQUAL; // level1:type1 == level2:type2
	}

	// Other levels can always be compatible
	if (level1 > level2)
		return AC_LOWER; // level1 is lower than level2
	else if (level1 < level2)
		return AC_HIGHER; // level1 is higher than level2
	else
		return AC_EQUAL;
}
