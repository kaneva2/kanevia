///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

class AssetImporterAdvancedDlg : public CDialog {
	DECLARE_DYNAMIC(AssetImporterAdvancedDlg)

public:
	AssetImporterAdvancedDlg(const NamingSyntax* namingSyntaxs, UINT numOfSyntaxs, CWnd* pParent = NULL);
	virtual ~AssetImporterAdvancedDlg();

	// Dialog Data
	enum { IDD = IDD_DLG_ADVANCED_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	void OnChangeNaming(int namingType);
	afx_msg void OnBnClickedBtnChangeStaticNaming();
	afx_msg void OnBnClickedBtnChangeSkeletalNaming();

	DECLARE_MESSAGE_MAP()
public:
	int m_bAllowPartial;
	BOOL m_bNamingSyntaxChanged;
	void GetNamingSyntaxs(NamingSyntax* namingSyntaxs, UINT maxNumOfSyntaxs, UINT& numOfSyntaxs) const;
	void SetNamingSyntaxs(const NamingSyntax* namingSyntaxs, UINT numOfSyntaxs);

private:
	NamingSyntax m_namingSyntaxs[NUM_NAMING_TYPES];
};

} // namespace KEP