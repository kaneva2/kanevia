///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "TextureImporter.h"

#include "KEPCommon.h"
#include "IClientEngine.h"
#include "UrlParser.h"
#include "KEPFileNames.h"
#include <iomanip>
#include "TextureObj.h"
#include "TextureHelper_nvtt.h"
#include "TextureDef.h"
#include "UGC\UGCImport.h"

#include "ImageHelper.h"

#include "LogHelper.h"
static LogInstance("Instance");

#define MAX_TEXTURE_EXTENTS 1024
#define MEDIUM_TEXTURE_EXTENTS 256
#define SMALL_TEXTURE_EXTENTS 128

namespace KEP {

// Supported Texture Extension Formats (must match UGCManager::InitUGCManager())
static std::map<std::string, IMAGE_FMT> s_fmtMap = {
	{ "jpg", IMAGE_FMT_JPEG }, { "jpeg", IMAGE_FMT_JPEG }, { "jpe", IMAGE_FMT_JPEG }, { "jfif", IMAGE_FMT_JPEG }, { "tif", IMAGE_FMT_TIFF }, { "tiff", IMAGE_FMT_TIFF }, { "bmp", IMAGE_FMT_BMP }, { "dib", IMAGE_FMT_BMP }, { "gif", IMAGE_FMT_GIF }, { "png", IMAGE_FMT_PNG } // leave as png to preserve transparency
	,
	{ "tga", IMAGE_FMT_TARGA }, { "dds", IMAGE_FMT_DDS } // leave as dds to preserve transparency
};

TextureImporter::TextureImporter() :
		IAssetImporter("TextureImporter", "Kaneva Universal Texture Importer") {
}

void TextureImporter::Register() {
	// Register All Texture Extensions
	for (const auto& it : s_fmtMap) {
		std::string extStr = it.first;
		IAssetImporterFactory::Instance()->RegisterImporter(this, KEP_TEXTURE, extStr);
	}
}

void TextureImporter::Unregister() {
	// Unregister All Texture Extensions
	for (const auto& it : s_fmtMap) {
		std::string extStr = it.first;
		IAssetImporterFactory::Instance()->UnregisterImporter(this, KEP_TEXTURE, extStr);
	}
}

void pathSplit(const std::string& fullPath, std::string& path, std::string& fileName) {
	size_t ofsSlash = fullPath.find('\\');
	if (ofsSlash == -1)
		ofsSlash = fullPath.find('/');

	if (ofsSlash == -1) {
		fileName = fullPath;
		path.clear();
	} else {
		fileName = fullPath.substr(ofsSlash + 1);
		path = fullPath.substr(0, ofsSlash + 1); // include the trailing "\\" in path
	}
}

#if (DRF_TEXTURE_IMPORT_OPT == 1)
static bool ImageConvertToJpg(const std::string& filePathIn, const std::string& filePathOut) {
	// Supported Texture Format ?
	auto it = s_fmtMap.find(StrToLower(StrStripFileName(filePathIn)));
	if (it == s_fmtMap.end())
		return false;

	// Leave PNG & DDS Untouched To Preserve Transparency
	IMAGE_FMT imgFmt = it->second;
	if ((imgFmt == IMAGE_FMT_PNG) || (imgFmt == IMAGE_FMT_DDS))
		return false;

	// Load Image As Bitmap
	FIBITMAP* pFIB = FreeImage_Load(fiFmt, filePathIn.c_str());
	if (!pFIB) {
		LogError("FreeImage_Load() FAILED - '" << filePathIn << "'");
		return false;
	}

	// Save Bitmap As Jpeg
	int flags = (JPEG_OPTIMIZE | JPEG_BASELINE | JPEG_QUALITYSUPERB);
	if (!FreeImage_Save(IMAGE_FMT_JPEG, pFIB, filePathOut.c_str(), flags)) {
		LogError("FreeImage_Save() FAILED - '" << filePathOut << "'");
		return false;
	}

	return true;
}
#endif

bool TextureImporter::Import(
	TextureDef& textureInfo,
	const std::string& fileName,
	AssetData* assetDef,
	int flags) {
	bool bUrlSupported = false;
	std::string path;
	std::string hashName;
	std::string filePath = fileName;

	// Create Temp Path Imported '<pathApp>\MapsModels\imported\'
	std::string tmpPath = CreatePathMapsModelsImported() + "\\";

#if (DRF_TEXTURE_IMPORT_OPT == 1)
	// Don't Optimize Model Textures For Now, Only UGC Textures
	if (!STLContainsIgnoreCase(filePath, "file:")) {
		// Get MD5 File Hash Name
		hashName = GetFileHash(filePath);

		// Convert To Hash-Named Jpeg In Temp Path (skip if already exists)
		std::string filePathOut = PathAdd(tmpPath, hashName + ".jpg");
		bool alreadyExists = FileHelper::Exists(filePathOut);
		if (alreadyExists || ImageConvertToJpg(filePath, filePathOut))
			filePath = filePathOut;

		textureInfo.origPath = StrStripFileNameExt(filePath);
		textureInfo.origFileName = StrStripFilePath(filePath);

		path = filePath;
	} else
#endif
	{
		// Save source file information to TextureDef
		pathSplit(filePath, textureInfo.origPath, textureInfo.origFileName);

		std::string originalUrl = filePath;

		// Little hack to fix path begin with "driveletter:"
		// otherwise we are not able to distinguish C:\xxx from file:/xxx
		if (originalUrl.size() > 2 && originalUrl[1] == ':')
			originalUrl = "file:///" + originalUrl;

		// Fix UNC-style path "\\host\path\..."
		if (originalUrl.size() > 4 && originalUrl.substr(0, 2) == "\\\\")
			originalUrl = "file://" + originalUrl.substr(2);

		UrlParser urlParser;
		urlParser.parse(originalUrl.c_str());
		std::string scheme(urlParser.scheme() ? urlParser.scheme() : "");
		bool ok = (scheme.empty() || scheme == "file") && urlParser.map().empty();
		if (!ok) {
			textureInfo.valid = false;
			return false;
		}

		// file only and no parameters
		// Parse host name
		std::string trunk = urlParser.leftPart() ? UrlParser::unescapeString(urlParser.leftPart()) : "";
		path = trunk;

		// Valid Url ?
		bool bUrlBadFormed = false;
		if (trunk.substr(0, 2) == "//") {
			size_t nextSlashOfs = trunk.find('/', 2);
			if (nextSlashOfs == std::string::npos) {
				bUrlBadFormed = true;
			}
			if (nextSlashOfs == 2) {
				// "file:///local/path/file" -> remove leading slashes
				path = trunk.substr(nextSlashOfs + 1);
			}
		} else if (trunk.substr(0, 1) == "/") {
			// file:/C:/.../... (URI without hostname segment)
			path = trunk.substr(1);
		}
		replace(path.begin(), path.end(), '/', '\\');
		if (bUrlBadFormed) {
			LogError("Bad Formed Url - '" << path << "'");
			textureInfo.valid = false;
			return false;
		}

		// Get MD5 File Hash Name
		hashName = GetFileHash(path);
	}

	// DDS Conversion File Names
	std::string convertedTexturePath = PathAdd(tmpPath, hashName + ".dds");
	std::string failSafeFileName = PathAdd(tmpPath, hashName + "~INVALID~.dds");

	bool bRequirePowerOf2 = (flags & IMP_TEXTURE_POWEROF2_ROUNDING) == IMP_TEXTURE_POWEROF2_ROUNDING;
	int texLimitOpt = flags & IMP_TEXTURE_SIZE_LIMIT_MASK;
	unsigned int maxTextureExtent = 0;
	switch (texLimitOpt) {
		case IMP_TEXTURE_SIZE_LIMIT_NONE:
			maxTextureExtent = 0;
			break;
		case IMP_TEXTURE_SIZE_LIMIT_SMALL:
			maxTextureExtent = SMALL_TEXTURE_EXTENTS;
			break;
		case IMP_TEXTURE_SIZE_LIMIT_MEDIUM:
			maxTextureExtent = MEDIUM_TEXTURE_EXTENTS;
			break;
		case IMP_TEXTURE_SIZE_LIMIT_LARGE:
		default:
			maxTextureExtent = MAX_TEXTURE_EXTENTS;
			break;
	}

	bool exists = false;
	if (TextureHelper::IsImageAlreadyConverted(path.c_str(), maxTextureExtent, failSafeFileName.c_str(), exists, convertedTexturePath,
			textureInfo.width, textureInfo.height, textureInfo.bytes,
			textureInfo.origWidth, textureInfo.origHeight, textureInfo.origBytes,
			textureInfo.opacity, textureInfo.averageColor)) {
		// existing file used: save information on the old file instead
		pathSplit(convertedTexturePath, textureInfo.path, textureInfo.fileName);
		bUrlSupported = true;
		textureInfo.valid = exists;
	} else {
		textureInfo.fileName = hashName + ".dds";
		textureInfo.path = tmpPath;

		if (TextureHelper::ConvertImageToDDS(
				path.c_str(),
				convertedTexturePath.c_str(),
				WRAP_REPEAT, maxTextureExtent, bRequirePowerOf2, true,
				textureInfo.width, textureInfo.height, textureInfo.bytes,
				textureInfo.origWidth, textureInfo.origHeight, textureInfo.origBytes,
				textureInfo.opacity, textureInfo.averageColor)) {
			bUrlSupported = true;
			textureInfo.valid = true;
			CFileStatus status;
			if (!CFile::GetStatus(Utf8ToUtf16(convertedTexturePath).c_str(), status)) {
				LogError("GetStatus() FAILED - '" << convertedTexturePath << "'");
			}
		} else {
			textureInfo.valid = false;
			LogError("ConvertImageToDDS() FAILED - '" << convertedTexturePath << "'");
		}
	}

	return bUrlSupported;
}

bool TextureImporter::GetTextureImportInfo(TextureDef& textureInfo, const std::string& importedFileName) {
	return TextureHelper::GetConversionInfo(
		importedFileName,
		textureInfo.width, textureInfo.height, textureInfo.bytes,
		textureInfo.origWidth, textureInfo.origHeight, textureInfo.origBytes,
		textureInfo.opacity, textureInfo.averageColor);
}

static IAssetImporter* s_textureImporter = nullptr;

IAssetImporter* createTextureImporter() {
	ASSERT(s_textureImporter == nullptr);
	s_textureImporter = new TextureImporter();
	return s_textureImporter;
}

} // namespace KEP