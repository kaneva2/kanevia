///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "StylizedTreeCtrl.h"

CStylizedTreeCtrl::CStylizedTreeCtrl(void) {
}

CStylizedTreeCtrl::~CStylizedTreeCtrl(void) {
}
BEGIN_MESSAGE_MAP(CStylizedTreeCtrl, CTreeCtrl)
//	ON_WM_PAINT()
END_MESSAGE_MAP()

void CStylizedTreeCtrl::SetItemColor(HTREEITEM hItem, DWORD color) {
	ResetItemColor(hItem);
	allItemColors.insert(std::pair<HTREEITEM, DWORD>(hItem, color));
}

DWORD CStylizedTreeCtrl::GetItemColor(HTREEITEM hItem) {
	std::map<HTREEITEM, DWORD>::iterator it = allItemColors.find(hItem);
	if (it != allItemColors.end())
		return it->second;

	return (DWORD)-1;
}

void CStylizedTreeCtrl::ResetItemColor(HTREEITEM hItem) {
	std::map<HTREEITEM, DWORD>::iterator it = allItemColors.find(hItem);
	if (it != allItemColors.end())
		allItemColors.erase(it);
}

void CStylizedTreeCtrl::DrawTreeItem(HWND hWnd, HDC hDC, LPRECT lpClipRect, HTREEITEM hItem) {
	jsAssert(hDC != NULL);
	jsAssert(hItem != NULL);

	std::map<HTREEITEM, DWORD>::iterator it = allItemColors.find(hItem);
	if (it == allItemColors.end())
		return; // No customizations

	DWORD color = it->second;

	// Get Item Rect
	RECT rectItem;
	ZeroMemory(&rectItem, sizeof(rectItem));
	if (!TreeView_GetItemRect(hWnd, hItem, &rectItem, TRUE))
		return; // Item might not be visible

	// Return if nothing to draw
	if (IsRectEmpty(&rectItem))
		return;

	// Check clipping
	if (lpClipRect) {
		RECT effRect;
		IntersectRect(&effRect, lpClipRect, &rectItem);
		if (IsRectEmpty(&effRect))
			return;
	}

	// Get Item Data
	wchar_t textBuffer[300];
	ZeroMemory(textBuffer, sizeof(textBuffer));
	TVITEM item;
	ZeroMemory(&item, sizeof(item));
	item.hItem = hItem;
	item.mask = TVIF_TEXT | TVIF_STATE;
	item.pszText = textBuffer;
	item.cchTextMax = sizeof(textBuffer) - 1;
	item.stateMask = TVIS_SELECTED | TVIS_BOLD | TVIS_CUT | TVIS_DROPHILITED;
	if (!TreeView_GetItem(hWnd, &item))
		return; // something wrong?

	if (item.state & (TVIS_DROPHILITED))
		return;

	// Clear rectangle (except selection boundary)
	rectItem.left++;
	rectItem.right--;
	rectItem.top++;
	rectItem.bottom--;

	// Get Brush
	HBRUSH hBrush;
	if (item.state & TVIS_SELECTED)
		hBrush = CreateSolidBrush(RGB(240, 240, 240));
	else {
		//COLORREF bkColor = TreeView_GetBkColor(hWnd);
		//hBrush = CreateSolidBrush(bkColor);
		hBrush = (HBRUSH)GetCurrentObject(hDC, OBJ_BRUSH);
	}

	FillRect(hDC, &rectItem, hBrush);

	if (item.state & TVIS_SELECTED)
		DeleteObject(hBrush);

	// Set Font
	HFONT hFont = (HFONT)::SendMessage(hWnd, WM_GETFONT, 0, 0);
	if (hFont) {
		LOGFONT lf;

		::GetObject(hFont, sizeof(lf), &lf);

		// Use bold font
		if (item.state & TVIS_BOLD)
			lf.lfWeight = 800;

		hFont = CreateFontIndirect(&lf);
		SelectObject(hDC, hFont);
	}

	SetBkMode(hDC, TRANSPARENT);

	// Draw Text
	::SetTextColor(hDC, color);
	TextOut(hDC, rectItem.left + 1, rectItem.top, item.pszText, wcslen(item.pszText));

	if (hFont)
		DeleteObject(hFont);
}

LRESULT CStylizedTreeCtrl::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) {
	HWND hWnd = GetSafeHwnd();
	PAINTSTRUCT ps;
	HDC hDC = NULL;
	BOOL bPaint = FALSE;
	RECT rect;

	if (message == WM_PAINT) {
		// Check update region (must call before original WndProc)
		if (::GetUpdateRect(hWnd, &rect, FALSE)) {
			bPaint = TRUE;
			hDC = ::BeginPaint(hWnd, &ps);
			wParam = (WPARAM)hDC; // Pass hDC to system WndProc - secret trick used by Common Controls
		}
	}

	// Call original WndProc
	LRESULT ret = CTreeCtrl::WindowProc(message, wParam, lParam);

	if (ret == S_OK) {
		// Post-call Customization if WndProc successfully returned
		switch (message) {
			case WM_PAINT:
				if (bPaint) {
					HTREEITEM hItem = TreeView_GetFirstVisible(hWnd);
					while (hItem) {
						DrawTreeItem(hWnd, hDC, &rect, hItem);
						hItem = TreeView_GetNextVisible(hWnd, hItem);
					}
				}
				break;
		}
	}

	if (bPaint)
		::EndPaint(hWnd, &ps);

	return ret;
}
