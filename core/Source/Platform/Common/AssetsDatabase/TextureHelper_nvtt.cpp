///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "TextureHelper_nvtt.h"

#include "common\KEPUtil\Helpers.h"

#undef min
#undef max

#include <nvimage/Image.h>
#include <nvimage/DirectDrawSurface.h>
#include <nvtt/nvtt.h>
#include <nvmath/Color.h>
#include <d3d9types.h> // MAKEFOURCC macro
#include "KEP/Images/KEPImages.h"

static LogInstance("Instance");

#define CHECKSUM_BUFFER_SIZE 16 * 1024

namespace KEP {

struct ConversionRecord {
	DWORD m_inputChecksum; // In case image is updated in-place between conversions
	std::string m_outputPath; // Converted image

	unsigned int m_width; // Output image width
	unsigned int m_height; // Output image height
	FileSize m_bytes; // Output image size (bytes)
	unsigned int m_origWidth; // Input image width
	unsigned int m_origHeight; // Input image height
	FileSize m_origBytes; // Input image size (bytes)
	ImageOpacity m_opacity; // Image opacity
	unsigned int m_averageColor; // Average pixel color

	ConversionRecord(DWORD checksum, const std::string& outputPath) :
			m_inputChecksum(checksum), m_outputPath(outputPath), m_width(0), m_height(0), m_bytes(0), m_origWidth(0), m_origHeight(0), m_origBytes(0), m_opacity(ImageOpacity::UNKNOWN), m_averageColor(0) {}

	ConversionRecord(
		DWORD checksum,
		const std::string& outputPath,
		unsigned int width, unsigned int height, FileSize bytes,
		unsigned int origWidth, unsigned int origHeight, FileSize origBytes,
		ImageOpacity opacity, unsigned int averageColor) :
			m_inputChecksum(checksum),
			m_outputPath(outputPath), m_width(width), m_height(height), m_bytes(bytes), m_origWidth(origWidth), m_origHeight(origHeight), m_origBytes(origBytes), m_opacity(opacity), m_averageColor(averageColor) {}

	void get(
		std::string& outputPath,
		unsigned int& width, unsigned int& height, FileSize& bytes,
		unsigned int& origWidth, unsigned int& origHeight, FileSize& origBytes,
		ImageOpacity& opacity, unsigned int& averageColor) const {
		outputPath = m_outputPath;
		width = m_width;
		height = m_height;
		bytes = m_bytes;
		origWidth = m_origWidth;
		origHeight = m_origHeight;
		origBytes = m_origBytes;
		opacity = m_opacity;
		averageColor = m_averageColor;
	}
};

static std::map<std::pair<std::string, unsigned int>, ConversionRecord> conversionLog;

static bool AnalyzeImage(const char* fileName, bool& isDDS, unsigned int& width, unsigned int& height, nvtt::Format& format, ImageOpacity& opacity, unsigned int& averageColor, std::unique_ptr<nv::DirectDrawSurface>& dds, std::unique_ptr<nv::Image>& img);
static bool IsImageValidDDS(const char* fileName, unsigned int& width, unsigned int& height, TEXTURE_TYPE& texType, std::unique_ptr<nv::DirectDrawSurface>& dds, std::unique_ptr<nv::Image>& img);
static bool IsImageFormatSupported(const char* fileName, unsigned int& width, unsigned int& height, std::unique_ptr<nv::Image>& img);

class TextureOutputHandler : public nvtt::OutputHandler, public nvtt::ErrorHandler {
	FileHelper m_file; // drf - file leak fix

public:
	TextureOutputHandler(const char* outputPath) :
			m_file(outputPath, GENERIC_WRITE, CREATE_ALWAYS) {}
	virtual ~TextureOutputHandler() {}

	bool OutputOk() {
		return m_file.IsOpen();
	}

	// NVTT - Indicate the start of a new compressed image that's part of the final texture.
	virtual void beginImage(int size, int width, int height, int depth, int face, int miplevel) {
		ASSERT(OutputOk());
		m_outSize = size;
		m_outWidth = width;
		m_outHeight = height;
		m_outDepth = depth;
		m_outFace = face;
		m_outMipLevel = miplevel;
	}

	// NVTT - Output data. Compressed data is output as soon as it's generated to minimize memory allocations.
	virtual bool writeData(const void* data, int size) {
		return (m_file.Tx(data, size) == size);
	}

	// NVTT - Signal Error
	virtual void error(nvtt::Error e) {
		// Ignore Errors
	}

	int m_outSize, m_outWidth, m_outHeight, m_outDepth, m_outFace, m_outMipLevel;
};

const nvtt::Format nvtt_Format_Others = (nvtt::Format)-1;

inline std::string GetDDSFormatName(nvtt::Format format) {
	switch (format) {
		case nvtt::Format_DXT1: return "DXT1";
		case nvtt::Format_DXT1a: return "DXT1a";
		case nvtt::Format_DXT3: return "DXT3";
		case nvtt::Format_DXT5: return "DXT5";
		case nvtt::Format_DXT5n: return "DXT5n";
		case nvtt::Format_BC4: return "BC4";
		case nvtt::Format_BC5: return "BC5";
		default:
			ASSERT(format == nvtt_Format_Others);
			return std::to_string(format);
	}
}

inline std::string GetOpacityTypeName(ImageOpacity opacity) {
	switch (opacity) {
		case ImageOpacity::FULLY_OPAQUE: return "OPAQUE";
		case ImageOpacity::ONE_BIT_ALPHA: return "1-BIT";
		case ImageOpacity::TRANSLUCENT: return "TRANSLUCENT";
		case ImageOpacity::DISCRETE_ALPHA: return "DISCRETE";
		default: ASSERT(false); return std::to_string((int)opacity);
	}
}

inline bool IsDXT135(nvtt::Format format) {
	switch (format) {
		case nvtt::Format_DXT1:
		case nvtt::Format_DXT1a:
		case nvtt::Format_DXT3:
		case nvtt::Format_DXT5:
		case nvtt::Format_DXT5n:
			return true;
		default:
			return false;
	}
}

inline nvtt::Format GetDDSFormatByFourCC(unsigned fourCC) {
	switch (fourCC) {
		case MAKEFOURCC('D', 'X', 'T', '1'):
			return nvtt::Format_DXT1;
		case MAKEFOURCC('D', 'X', 'T', '3'):
			return nvtt::Format_DXT3;
		case MAKEFOURCC('D', 'X', 'T', '5'):
			return nvtt::Format_DXT5;
		case MAKEFOURCC('B', 'C', '4', 'U'):
		case MAKEFOURCC('B', 'C', '4', 'S'):
			return nvtt::Format_BC4;
		case MAKEFOURCC('A', 'T', 'I', '2'):
		case MAKEFOURCC('B', 'C', '5', 'S'):
			return nvtt::Format_BC5;
		default:
			ASSERT(fourCC == 0); // 0: Non-4CC (uncompressed DDS or other image format). Other values: unsupported/unknown 4CC (assertion).
			return nvtt_Format_Others; // Return a value to trigger conversion
	}
}

inline nvtt::Format GetDDSFormatByOpacityType(ImageOpacity opacity) {
	switch (opacity) {
		case ImageOpacity::FULLY_OPAQUE:
			return nvtt::Format_DXT1;
		case ImageOpacity::ONE_BIT_ALPHA:
			return nvtt::Format_DXT1a;
		case ImageOpacity::TRANSLUCENT:
			return nvtt::Format_DXT5;
		case ImageOpacity::DISCRETE_ALPHA:
			return nvtt::Format_DXT3;
		default:
			ASSERT(false);
			return nvtt::Format_DXT1;
	}
}

bool AnalyzeImage(const char* fileName, bool& isDDS, unsigned int& width, unsigned int& height, nvtt::Format& format, ImageOpacity& opacity, unsigned int& averageColor, std::unique_ptr<nv::DirectDrawSurface>& dds, std::unique_ptr<nv::Image>& img) {
	const uint8 OPACITY_THRESHOLD = 0xfc;
	TEXTURE_TYPE texType = TEXTURE_UNK;

	// Analyze image format
	isDDS = IsImageValidDDS(fileName, width, height, texType, dds, img);
	if (!isDDS && !IsImageFormatSupported(fileName, width, height, img)) {
		return false;
	}

	// HACK: a class mirroring nv::DirectDrawSurface for fetching its internal data
	class NVDirectDrawSurfaceCast {
	public:
		unsigned getFourCC() const { return header.pf.fourcc; }

	private:
		void* stream;
		nv::DDSHeader header;
		nv::DDSHeader10 header10;
	};
	static_assert(sizeof(NVDirectDrawSurfaceCast) == sizeof(nv::DirectDrawSurface), "nvtt version mismatch?");

	unsigned fourCC = isDDS ? ((NVDirectDrawSurfaceCast*)dds.get())->getFourCC() : 0;
	format = GetDDSFormatByFourCC(fourCC);

	// Analyze alpha channel
	opacity = ImageOpacity::FULLY_OPAQUE;

	if (img->format() == nv::Image::Format_ARGB) {
		// Scan pixels until the first translucent one, or until the entire image is scanned
		bool opaque = true, translucent = false;
		for (size_t y = 0; y < img->height() && !translucent; y++) {
			auto scanline = img->scanline(y);
			for (size_t x = 0; x < img->width(); x++) {
				if (scanline[x].a < OPACITY_THRESHOLD) {
					opaque = false;
					if (scanline[x].a > 0) {
						translucent = true;
						break;
					}
				}
			}
		}

		opacity = opaque ? ImageOpacity::FULLY_OPAQUE : translucent ? ImageOpacity::TRANSLUCENT : ImageOpacity::ONE_BIT_ALPHA; // Not handling ALPHA_DISCRETE at this moment
	}

	// Compute average image color
	averageColor = getAverageColor((unsigned char*)img->pixels(), sizeof(nv::Color32) * img->width() * img->height(), img->width(), img->height());
	return true;
}

bool IsImageValidDDS(const char* fileName, unsigned int& width, unsigned int& height, TEXTURE_TYPE& texType, std::unique_ptr<nv::DirectDrawSurface>& dds, std::unique_ptr<nv::Image>& img) {
	width = 0;
	height = 0;
	texType = TEXTURE_UNK;

	dds = std::unique_ptr<nv::DirectDrawSurface>(new nv::DirectDrawSurface(fileName));

	if (!dds->isValid()) { // 2D texture only
		dds = NULL; // unique_ptr automatically deletes old pointer when assigned with NULL
		return false;
	}

	img = std::unique_ptr<nv::Image>(new nv::Image());
	dds->mipmap(img.get(), 0, 0); // first face/ first mipmap

	width = dds->width();
	height = dds->height();

	if (dds->isTexture1D())
		texType = TEXTURE_1D;
	else if (dds->isTexture2D())
		texType = TEXTURE_2D;
	else if (dds->isTexture3D())
		texType = TEXTURE_3D;
	else if (dds->isTextureCube())
		texType = TEXTURE_CUBE;

	return true;
}

bool IsImageFormatSupported(const char* fileName, unsigned int& width, unsigned int& height, std::unique_ptr<nv::Image>& img) {
	width = 0;
	height = 0;

	img = std::unique_ptr<nv::Image>(new nv::Image());

	if (!img->load(fileName)) {
		img = NULL; // unique_ptr automatically deletes old pointer when assigned with NULL
		return false;
	}

	width = img->width();
	height = img->height();
	return true;
}

bool IsPowerOf2(int num) {
	VERIFY_RETURN(num >= 0, false);
	int returnVal = (num != 0) && ((num & (num - 1)) == 0);
	return (returnVal != 0);
}

static DWORD Checksum(const char* inputPath) {
	// Allocate File Buffer
	ASSERT(CHECKSUM_BUFFER_SIZE % sizeof(DWORD) == 0);
	DWORD* buffer = new DWORD[CHECKSUM_BUFFER_SIZE / sizeof(DWORD)];
	std::auto_ptr<DWORD> apBuffer(buffer); // auto disposal

	// Calculate Checksum Of All Bytes
	DWORD checksum = 0;
	DWORD bytesRx = 0;
	FileHelper file(inputPath, GENERIC_READ, OPEN_EXISTING);
	while ((bytesRx = file.Rx(buffer, CHECKSUM_BUFFER_SIZE)) > 0) {
		// Pad Zeros To Fill DWORD
		while (bytesRx % sizeof(DWORD) != 0) {
			((BYTE*)buffer)[bytesRx] = 0;
			bytesRx++;
		}

		// Update Checksum (XOR)
		DWORD dwordsRx = bytesRx / sizeof(DWORD);
		for (DWORD i = 0; i < dwordsRx; ++i)
			checksum ^= buffer[i];
	}

	return checksum;
}

bool TextureHelper::ConvertImageToDDS(
	const char* inputPath, const char* outputPath,
	TEXTURE_WRAP_TYPE wrapType, unsigned int sizeLimit, bool requirePowerOf2, bool fast,
	unsigned int& width, unsigned int& height, FileSize& bytes,
	unsigned int& origWidth, unsigned int& origHeight, FileSize& origBytes,
	ImageOpacity& opacity, unsigned int& averageColor) {
	Timer timer;
	LogInfo("'" << inputPath << "' => '" << outputPath << "' ...");

	std::unique_ptr<nv::DirectDrawSurface> dds;
	std::unique_ptr<nv::Image> img;

	// Initialize out variables to invalid values
	width = 0;
	height = 0;
	bytes = 0;
	origWidth = 0;
	origHeight = 0;
	origBytes = 0;
	opacity = ImageOpacity::FULLY_OPAQUE;
	averageColor = 0;

	// Check if file exists
	if (!FileHelper::Exists(inputPath)) {
		LogError("FAILED - FILE NOT FOUND - '" << inputPath << "'");
		return false;
	}

	// Save file size
	origBytes = FileHelper::Size(inputPath);

	// Analyze original image
	bool isDDS = false;
	nvtt::Format origFormat = nvtt_Format_Others;

	if (!AnalyzeImage(inputPath, isDDS, origWidth, origHeight, origFormat, opacity, averageColor, dds, img)) {
		LogError("FAILED - IMAGE FORMAT NOT SUPPORTED - '" << inputPath << "'");
		return false;
	}

	LogInfo("Input: dds=" << LogBool(isDDS) << " (" << origWidth << "x" << origHeight << ") format=" << GetDDSFormatName(origFormat) << " opacity=" << GetOpacityTypeName(opacity));

	// Conversion required if: 1) not a valid DDS, 2) Not DXT1/3/5, 3) Opaque or 1-bit alpha image but not DXT1, 4) exceeded size limit, or 5) powerof2 rule failed
	bool bConversionRequired = !isDDS ||
							   !IsDXT135(origFormat) ||
							   ((opacity == ImageOpacity::FULLY_OPAQUE || opacity == ImageOpacity::ONE_BIT_ALPHA) && origFormat != nvtt::Format_DXT1) ||
							   (sizeLimit > 0 && (origWidth > sizeLimit || origHeight > sizeLimit)) ||
							   (requirePowerOf2 && (!IsPowerOf2(origWidth) || !IsPowerOf2(origHeight)));

	// Conversion Not Required ?
	if (!bConversionRequired) {
		FileHelper::Copy(inputPath, outputPath);
		// import fails if this is not added to conversionLog.
		conversionLog.insert(std::make_pair(
			std::make_pair(inputPath, sizeLimit),
			ConversionRecord(
				Checksum(inputPath),
				outputPath,
				origWidth,
				origHeight,
				origBytes,
				origWidth,
				origHeight,
				origBytes,
				opacity,
				averageColor)));
		LogInfo("OK - NO CONVERSION REQUIRED - '" << inputPath << "'");
		return true;
	}

	// Valid Image Pixels ?
	if (!img) {
		LogError("FAILED - NULL IMAGE - '" << inputPath << "'");
		return false;
	}

	// DRF - ED-5016 - High Resolution Texture Import Crash
	// Resize Images That Are Too Big Before DDS Conversion
	bool doResize = (sizeLimit > 0) && ((origWidth > sizeLimit) || (origHeight > sizeLimit));
	if (doResize) {
		// Calculate New Image Scaling
		double scale;
		unsigned int newWidth, newHeight;
		if (origWidth > origHeight) {
			scale = (double)origWidth / (double)sizeLimit;
			newWidth = sizeLimit;
			newHeight = (double)origHeight / scale;
		} else {
			scale = (double)origHeight / (double)sizeLimit;
			newHeight = sizeLimit;
			newWidth = (double)origWidth / scale;
		}
		LogWarn("TOO BIG - RESIZING - "
				<< "(" << origWidth << "x" << origHeight << ") --> "
				<< "(" << newWidth << "x" << newHeight << ")");

		// Sub-Sample Image
		auto imgNew = std::unique_ptr<nv::Image>(new nv::Image());
		imgNew->allocate(newWidth, newHeight);
		for (unsigned int y = 0; y < newHeight; ++y)
			for (unsigned int x = 0; x < newWidth; ++x)
				imgNew->pixel(x, y) = img->pixel(x * scale, y * scale);
		img = std::move(imgNew);
		origWidth = newWidth;
		origHeight = newHeight;
	}

	auto outFormat = GetDDSFormatByOpacityType(opacity);
	LogInfo("Conversion: quality=" << (fast ? "fast" : "normal") << " format=" << GetDDSFormatName(outFormat));

	nvtt::CompressionOptions optComp;
	optComp.setQuality(fast ? nvtt::Quality_Fastest : nvtt::Quality_Normal);
	optComp.setFormat(outFormat);

	nvtt::InputOptions optIn;
	optIn.setNormalizeMipmaps(false);
	optIn.setMipmapGeneration(false);
	optIn.setTextureLayout(nvtt::TextureType_2D, origWidth, origHeight);
	optIn.setMipmapData(img->pixels(), origWidth, origHeight);
	if (sizeLimit > 0)
		optIn.setMaxExtents(sizeLimit);
	if (requirePowerOf2)
		optIn.setRoundMode(nvtt::RoundMode_ToPreviousPowerOfTwo);

	switch (wrapType) {
		case WRAP_REPEAT:
			optIn.setWrapMode(nvtt::WrapMode_Repeat);
			break;

		case WRAP_MIRROR:
			optIn.setWrapMode(nvtt::WrapMode_Mirror);
			break;

		case WRAP_CLAMP:
			optIn.setWrapMode(nvtt::WrapMode_Clamp);
			break;

		default:
			LogError("FAILED - UNKNOWN WRAP TYPE " << wrapType);
			return false;
	}

	TextureOutputHandler handler(outputPath);
	if (!handler.OutputOk()) {
		LogError("FAILED - OUTPUT NOT OK");
		return false;
	}

	nvtt::OutputOptions optOut;
	optOut.setOutputHandler(&handler);
	optOut.setErrorHandler(&handler);

	nvtt::Compressor compressor;
	compressor.enableCudaAcceleration(true); // drf - changed
	compressor.process(optIn, optComp, optOut);

	// Save output file attributes
	width = handler.m_outWidth;
	height = handler.m_outHeight;
	bytes = handler.m_outSize;

	LogInfo("Output: (" << width << "x" << height << ") bytes=" << bytes);

	conversionLog.insert(std::make_pair(std::make_pair(inputPath, sizeLimit),
		ConversionRecord(Checksum(inputPath), outputPath, width, height, bytes, origWidth, origHeight, origBytes, opacity, averageColor)));

	LogInfo("OK - timeMs=" << timer.ElapsedMs() << " bytes=" << bytes);

	return true;
}

bool TextureHelper::IsImageAlreadyConverted(
	const char* inputPath, unsigned int sizeLimit, const char* failSafeOutputPath,
	bool& exists, std::string& outputPath,
	unsigned int& width, unsigned int& height, FileSize& bytes,
	unsigned int& origWidth, unsigned int& origHeight, FileSize& origBytes,
	ImageOpacity& opacity, unsigned int& averageColor) {
	jsVerifyReturn(inputPath != NULL, false);

	// Initialize out variables to invalid values
	width = 0;
	height = 0;
	bytes = 0;
	origWidth = 0;
	origHeight = 0;
	origBytes = 0;
	opacity = ImageOpacity::FULLY_OPAQUE;
	averageColor = 0;

	// compute checksum for input file
	DWORD checksum = Checksum(inputPath);
	exists = checksum != 0;

	auto findIt = conversionLog.find(std::make_pair(inputPath, sizeLimit));
	if (findIt != conversionLog.end()) {
		// existing record found, now verify checksum
		if (checksum == findIt->second.m_inputChecksum) {
			// checksum matched: already converted
			findIt->second.get(outputPath, width, height, bytes, origWidth, origHeight, origBytes, opacity, averageColor);
			return true;
		} else {
			// checksum changed, delete old record
			conversionLog.erase(findIt);
		}
	}

	if (checksum == 0) {
		// file not found: bad path?
		if (failSafeOutputPath != NULL) {
			outputPath = failSafeOutputPath;
			conversionLog.insert(std::make_pair(std::make_pair(inputPath, sizeLimit), ConversionRecord(0, failSafeOutputPath)));
			return true;
		}
	}

	// not converted
	return false;
}

bool TextureHelper::GetRawImagePath(const std::string& convertedImagePath, std::string& rawImagePath, bool allowFailSafePath) {
	for (auto it = conversionLog.begin(); it != conversionLog.end(); ++it) {
		auto first = it->first;
		auto second = it->second;
		if (second.m_inputChecksum == 0 && !allowFailSafePath)
			continue; // a failsafe record, skip
		if (second.m_outputPath == convertedImagePath ||
			STLEndWith(second.m_outputPath, "\\" + convertedImagePath) ||
			STLEndWith(second.m_outputPath, "/" + convertedImagePath)) {
			rawImagePath = first.first;
			return true;
		}
	}

	return false;
}

bool TextureHelper::GetConvertedImagePath(const std::string& rawImagePath, unsigned int sizeLimit, std::string& convertedImagePath, bool returnFailSafePath) {
	auto findIt = conversionLog.find(make_pair(rawImagePath, sizeLimit));
	if (findIt == conversionLog.end())
		return false;

	if (findIt->second.m_inputChecksum == 0 && !returnFailSafePath)
		return false;

	convertedImagePath = findIt->second.m_outputPath;
	return true;
}

bool TextureHelper::GetConversionInfo(
	const std::string& convertedImagePath,
	unsigned int& width, unsigned int& height, FileSize& bytes,
	unsigned int& origWidth, unsigned int& origHeight, FileSize& origBytes,
	ImageOpacity& opacity, unsigned int& averageColor) {
	for (const auto pr : conversionLog) {
		if (pr.second.m_inputChecksum == 0) {
			continue; // a failsafe record, skip
		}

		if (pr.second.m_outputPath == convertedImagePath ||
			STLEndWith(pr.second.m_outputPath, "\\" + convertedImagePath) ||
			STLEndWith(pr.second.m_outputPath, "/" + convertedImagePath)) {
			std::string outputPath;
			pr.second.get(outputPath, width, height, bytes, origWidth, origHeight, origBytes, opacity, averageColor);
			return true;
		}
	}

	return false;
}

void TextureHelper::ClearConversionHistory() {
	LogInfo("");
	conversionLog.clear();
}

} // namespace KEP
