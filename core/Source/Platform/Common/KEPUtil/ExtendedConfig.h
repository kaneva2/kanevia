///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// Item internals
// Leaks?
// Tree merging
// Fragments
// This function will only exist in the TinyXML implementation for backward compatibility
// with KEPConfig.  The mechanics of clean/dirty, saveonexit, etc.  are best left to a
// external file agent.  This is especially true considering that there are very few
// instances of a configuration being modified programmatically and then stored to a file.
// In other words, this functionality is not part of the Config interface, but individual
// implementations are free implement it if necessary.
//
// RELATIVE vs ABSOLUTE pathing
//

// TODO: Implement macro expansion for macros.  Currently macros are not expanded and,
// since the attribute() method returns a pointer to a const string directly from TinyXml
// there is not an obvious of handling this.  I propose that attribute() be modified to
// return a string, and if the attribute is not found, an exception be thrown.

//-D"port=1234;gameid=4567"
//<logical name="port">%(port)</port>
//<ENTITY url="file://\\appman1.\\


//
//
// Todo:
//
// Long Term
//  * XPath support
//

// support for plurals is weak, but for that matter so is the plural support in KEPConfig
/*
	Wrapper over an XML config-type file
	Get and Set fns are used for getting and setting values in the XML
	valueName may have nested element separated by /, e.g. A/B/C which 
	translates to 
	<rootNode>
	  <A>
	    <B>
	      <C>

	Most Get/Set fns do one element.  Use append to add more nodes of the
	same name, instead of replacing them.  Use GetFirst/Next to get multiples

	Question 1		Is there any value in deriving from IKEPConfig?  
	
	IKEPConfig declares a small subset of the functionality provided by KEPConfig.  
	It's my belief conclusion that this is because the abstraction was laid down 
	(i.e. KEPConfig -> IKEPConfig) and then as development	progressed, the 
	implementation was expanded and the interface definition was never updated.

	If this is case, there is no value in deriving from IKEPConfig.  At least not
	before IKEPConfig is updated.  
	
	If this is not the case, the value of IKEPConfig is not clear to me.

	...

	Is it possible IKEPConfig is required for API Glue?  If so, it absolutely has value and
	there definitely is value in having Config support the interface, either through inheritance
	or via some kind of adapter/composition architecture.

	Bingo!  The smaller interface is only what is required by the externalized api.
	Consider, XML inherently presents all data as a string.  Also, Lua has more than capable
	of doing any string conversions it needs.  So why bloat the API boundary boundary
	layer by defining methods that can represent the xml data as numeric types?  The only
	thing I see wrong with this now, this relationship between KEPConfig and IKEPConfig
	would much clearer if IKEPConfig were named something like PublicConfigAPI

	That being said, we can put this off until such time, if ever, that KEPConfig is 
	obsolesced.  This class already interacts with KEPConfig pretty well.  Later iterations
	can pursue decoupling of the API from the implementation

    Iteration 1
	     +--------+        +-------------+       +-----------+        +------------+
	     | Config | <----- | TIXMLConfig | ----- | KEPConfig | -----> | IKEPConfig |
	     +--------+        +-------------+       +-----------+        +------------+

	Final Iteration
		 +-------------+      +--------+     +------------------+       +------------+
	     | TIXMLConfig |----->| Config |----+| ConfigAPIAdapter | ----> | IKEPConfig |
		 +-------------+      +--------+     +------------------+       +------------+

 Features:
	* Logical support
  * External references support
  * External logicals support
  * Full compatibility with KEPConfig (via KEPConfigAdapter)
  * Discrete writes on main file
  * Fully concurrent

	Future Features
	* XPath support
  * Discrete writes on all files with optional conflation
  * Array support
  * Support for dynamic reconfiguration via pub/sub model
	* Scoped/Hierarchical logicals.  I.e. logicals are valid only within the scope of where they are defined.

  ENTITY tag is now reserved


 */

#include "TinyXML/tinyxml.h"
#include "js.h"
#include "Core/Util/ChainedException.h"
#include "Common\KEPUtil\ParamString.h"
#include "Common\KEPUtil\URL.h"
#include "Common\include\KEPConfig.h"
#include "Core/Util/fast_mutex.h"

namespace KEP {

// Exceptions specific to the Config object model.
//
// For general configuration errors.
//
CHAINED(ConfigurationException);

/**
 * The generalized interface.
 */
class KEPUTIL_EXPORT Config {
public:
	// An item "reference".  Can exist at any time, but may not have a real
	// backing object.
	class __declspec(dllexport) Item {
	public:
		Item() {}
		Item(const Item& /*other */) {}
		virtual ~Item() {}
	};
};

template <class DestinationT>
class Visitor {
public:
	virtual Visitor& visit(DestinationT& destination) = 0;
	Visitor& visit(DestinationT&& destination) { return visit(destination); } // To allow calling visit on temporaries.
};

/**
 * Implementation of Config based on TinyXml
 */
class KEPUTIL_EXPORT TIXMLConfig : public Config {
public:
	class __declspec(dllexport) Item : public Config::Item {
	protected:
		Item(TiXmlDocument& doc, TiXmlElement* element);

	public:
		Item();
		Item(const std::string& value);
		Item(const Item& other);
		Item(TiXmlElement* element);

		virtual ~Item();

		Item& operator=(const Item& other);
		bool operator==(const Item& other) const;
		bool operator!=(const Item& other) const;

		const std::string& name() const;
		__declspec(dllexport) friend std::ostream& operator<<(std::ostream& out, const TIXMLConfig::Item& item);
		std::string attribute(const std::string& name) const;
		int attribute(const std::string& name, int default) const;

		/**
		 * Acquire the value of a item attribute.
		 * @param   name	const string reference specifying the name of the subject attribute.
		 * @returns A string containing the value of the attribute. 
		 * @throws	ConfigurationException in the event the attribute is not defined or is empty.
		 */
		std::string assertAttribute(const std::string& name) const;

		std::string toString() const;
		TIXMLConfig::Item& appendItem(TIXMLConfig::Item item);
		TIXMLConfig::Item& traverse(Visitor<Item>& visitor);
		TIXMLConfig::Item& traverse(Visitor<Item>&& visitor) { return traverse(visitor); } // To allow calling traverse() on temporaries.

		/**
         * @throws ConfigurationException
         */
		TIXMLConfig::Item firstChild(const std::string& name) const;

		TIXMLConfig::Item nextSibling() const;

		std::string debugString() const;

		/**
		 * Acquire the value of the text node component of the Item.
		 * 
		 * @return The text node component of this item as a string.
		 */
		std::string text() const;

		TiXmlElement* _internal;

		/**
         * Acquire a reference the containing xml config.  
         * @throws ConfigurationException in the event that the item
         * is not contained in a document.
         */
		TIXMLConfig& document() const;

	private:
		BOOL _managed;
		TiXmlDocument* _dom;
		TiXmlElement* _parent;
		mutable fast_recursive_mutex _sync;

		friend class TIXMLConfig;
	};

	static TIXMLConfig::Item NullItem;

	static const Item& nullItem();

	typedef Visitor<TIXMLConfig::Item> ItemVisitor;

	/**
	 * Create an uninitialized configuration object.  Note
	 * that any attempt to exercise the configuration prior
	 * to calling initialize will raise an UninitializedException
	 * 
	 * @throws	UninitializedException in the event that an attemp is made to
	 *			exercise this TIXMLConfig prior to calling TIXMLConfig::initialize()
	 */
	TIXMLConfig();

	/**
	 * Create a TIXMLConfig that is initialized with a root element of "rootName"
	 * 
	 * @param rootName	The name of the root element, as a string reference, with which this TIXMLConfig
	 *					will be initialized.
	 */
	TIXMLConfig(const std::string& rootName);

	/**
	 * Create a TIXMLConfig that is initialized with a root element of "rootName"
	 * 
	 * @param rootName	The name of the root element, as a const char*, with which this TIXMLConfig
	 *					will be initialized.
	 */
	TIXMLConfig(const char* rootName);

	/**
	 * Copy constructor.  Note that this value of this constructor is dubious and is included
	 * as a matter standard practice.  It's dubious value means that it is functionally the same 
	 * as the same as the default destructor.
	 * 
	 * @throws	UninitializedException in the event that an attemp is made to
	 *			exercise this TIXMLConfig prior to calling TIXMLConfig::initialize()
	 */
	TIXMLConfig(const TIXMLConfig& cfg);

	/**
	 * Construct with a TiXmlDocument instance.  Will cause the configuration to be conflated and all logicals
	 * to be loaded.
	 *
	 * Note that TIXMLConfig sets the user data pointer on the TiXmlDocument object to manage logicals,
	 * hence, any user data already stored in the TiXmlDocument will be orphaned.  ALWAYS perform
	 * any memory collection prior to calling this constructor.
	 *
	 * @param kepconfig		Reference to a KEPConfig instance that is wrapping a TiXmlDocument.
	 */
	TIXMLConfig::TIXMLConfig(TiXmlDocument& tixmlconfig);

	/**
	 * Construct with a KEPConfig instance.  Initializes the internal TiXmlDocument reference with the
	 * same instance wrapped by KEPConfig.  Will cause the configuration to be conflated and all logicals
	 * to be loaded.
	 *
	 * Note that TIXMLConfig sets the user data pointer on the TiXmlDocument object to manage logicals,
	 * hence, any user data already stored in the TiXmlDocument will be orphaned.  ALWAYS perform
	 * any memory collection prior to calling this constructor.
	 *
	 * @param kepconfig		Reference to a KEPConfig instance that is wrapping a TiXmlDocument.
	 */
	TIXMLConfig(KEPConfig& kepconfig);

	/**
     * Set the declaration of the xml document.  e.g.
     *
     * 	<?xml version="1.0" encoding="utf-8" standalone="yes"?>
     *
     * This isn't necessary in most circumstances, but it provided for
     * those occasions when it's called for.
     */
	TIXMLConfig& setDeclaration(const std::string& encoding = "", const std::string& standalone = "");
	/**
	 * dtor
	 */
	virtual ~TIXMLConfig();

	/**
	 * Assignment operator
	 */
	TIXMLConfig& operator=(const TIXMLConfig& other);

	/**
	 * Acquire a string representation of the configuration.
	 * Note that string will be generated with logicals applied.
	 * 
	 * @returns String representation of the configuration.
	 */
	virtual std::string toString();

	/**
	 * Returns deflated representation of the tree.  I.e. any data
	 * appended to the tree during conflation will be missing from
	 * this string.
	 *
	 *
	 */
	virtual std::string deflated() const;

	/**
	 * represent as string will all conflations and logicals applied.
	 */
	std::string computeString();

	void showDebug() const;

	/**
	 * Acquire a reference to the tree's root item
	 */
	TIXMLConfig::Item root() const;

	/**
	 * Given a string representing the path to an element, retrieve the element
	 * Note: Currently support for multiple elements is non-existent
	 * pending some xpath support.
	 */
	TIXMLConfig::Item find(const std::string& name) const;

	/**
	 * Initialize this config with an item to be used as the root
	 * element.  Used for low level construction of a configuration.
	 * 
	 * @param	item	Item to be placed in the root of the config.
	 * @return  reference to self.
	 */
	TIXMLConfig& initialize(Item item);

	/**
	 * Initialize this config with name of the root element.
	 * Used for low level construction of a construction.
	 *
	 * @param	rootName	The name of the root element.
	 * @return	reference to self.
	 */
	TIXMLConfig& initialize(const std::string& rootName);

	/**
	 * Given formatted data presented as a string parse the data
	 * into the corresponding document structure.
	 * 
	 * @return reference to self.
	 */
	TIXMLConfig& parse(const std::string& content, const URL& context = URL());

	/**
	 * Obtain a reference to the ParamSet used by this TIXMLConfig.  This is primarily
	 * used for programmatically adding logical definitions to the configuration.
	 */
	ParamSet& paramSet();

	/**
	 * Obtain a reference to this TIXMLConfig instance's backing TiXmlDocument object.
	 */
	TiXmlDocument& impl() const;

	/**
	 * Config can streamed out.
	 */
	__declspec(dllexport) friend std::ostream& operator<<(std::ostream& out, const TIXMLConfig& config);

	TIXMLConfig& setName(const std::string& name) {
		_name = name;
		return *this;
	}

	std::string name() const {
		return _name;
	}

protected:
	/**
     * Initiates full scan of the document in search of "entity items".  When
	 * encountered the contents of the external entity are inserted at the location of the entity
	 * element.
	 */
	TIXMLConfig& conflate(const URL& context);

	/**
	 * Scans tree for any logicals sets <logicals/> and loads them into the configurations
	 * logicals set.
	 */
	TIXMLConfig& loadLogicals();

private:
	TiXmlDocument* _dom;
	bool _mydom; // If it's mine, I allocated it, and I'll free it.
	ParamSet _paramSet;
	mutable fast_recursive_mutex _sync;
	std::string _name;

	friend class ConfigFileLoader;
};

/**
 * As described above, TIXML doesn't know about files.  It does however understand streams.
 * This helper class provides functionality beyond the basic functionality of maintaining
 * and accessing a tree of data.
 */
class KEPUTIL_EXPORT TIXMLConfigHelper {
public:
	TIXMLConfigHelper();
	TIXMLConfigHelper(TIXMLConfig& config);
	bool saveAs(const std::string& fname);
	TIXMLConfig& load(const std::string& fname);

private:
	TIXMLConfig* _config;
};

/**
 * Adapter class to allow for the interoperability of TIXMLConfig and IKEPConfig 
 * derivatives.  While it does not fully implement the KEPConfig API, it does 
 * fully support the IKEPConfig interface, and thus is suitable for exposure
 * to scripting.
 */
class KEPUTIL_EXPORT KEPConfigAdapter : public IKEPConfig {
public:
	KEPConfigAdapter(const char* root);
	KEPConfigAdapter();

	/**
	 * Construct with a reference to a TIXMLConfig instance
     *
	 * @param impl      Reference to the TIXMLConfig instance to be adapted.
     * @param manage    Flag indicating whether or not the TIXMLConfig instance's
     *                  memory is managed, read deleted, by this adapter instance.
	 */
	KEPConfigAdapter(TIXMLConfig& impl, bool manage = false);
	KEPConfigAdapter(KEPConfig& impl, bool manage = false);
	virtual ~KEPConfigAdapter();

	// IKEPConfig Overrides
	virtual bool Print() { impl().impl().Print(); return true; }
	virtual bool New(const char* rootNodeName, const char* rootAttribute = 0, const char* rootAttributeValue = 0) override;
	virtual bool Parse(const char* buffer, const char* rootNodeName = 0, const char* rootAttribute = 0, const char* rootAttributeValue = 0) override;
	virtual bool Load(const std::string& fname, const char* rootNodeName = 0, const char* rootAttribute = 0, const char* rootAttributeValue = 0) override;
	virtual bool Save() override;
	virtual bool SaveAs(const std::string& fname) override;
	virtual bool IsDirty() const override { return _dirty; }
	virtual bool SetDirty(bool d)  override { _dirty = d; return true; }
	virtual bool IsLoaded() const override { return _loaded; }
	virtual bool ValueExists(const char* valueName) const override;
	virtual bool GetValue(const char* valueName, std::string& ret, const char* defaultValue = "") const override;
	virtual bool GetValue(const char* valueName, bool& val, bool defaultValue = false) const override;
	virtual bool GetValue(const char* valueName, int& ret, int defaultValue = 0) const override;
	virtual bool GetValue(const char* valueName, double& ret, double defaultValue = 0.0) const override;
	virtual bool SetValue(const char* valueName, const char* value, bool append = false) override;
	virtual bool SetValue(const char* valueName, int value, bool append = false) override;
	virtual bool SetValue(const char* valueName, bool value, bool append = false) override;
	virtual bool SetValue(const char* valueName, double value, bool append = false) override;

	TIXMLConfig& impl();

private:
	TIXMLConfig* _impl;
	bool _managed;
	bool _dirty;
	bool _loaded;
};

} // namespace KEP
