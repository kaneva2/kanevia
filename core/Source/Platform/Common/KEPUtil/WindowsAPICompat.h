///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// Windows API compatibility helper for API not available on Windows XP

#include "KEPCommon.h"
#include "KEP/Util/KEPUtil.h"

namespace KEP {

class WindowsAPICompat {
public:
	typedef BOOL(WINAPI* GetPhysicallyInstalledSystemMemoryFunc)(PULONGLONG);
	KEPUTIL_EXPORT static GetPhysicallyInstalledSystemMemoryFunc GetPhysicallyInstalledSystemMemory;

private:
	static bool init();
	static bool m_inited;
};

} // namespace KEP
