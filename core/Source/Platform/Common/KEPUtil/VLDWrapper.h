///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


// By default VLD is disaled in all builds.  Developers can enable this through the environment using the CL environment.
// e.g.
//
//     set CL=/DENABLE_VLD
//
#ifdef ENABLE_VLD
#define VLD_ENABLED
#include <vld.h>
#endif