///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <PreciseTime.h>

namespace KEP {
namespace {
int64_t GetPerformanceFrequency() {
	LARGE_INTEGER i;
	QueryPerformanceFrequency(&i);
	return i.QuadPart;
}
} // namespace

int64_t PreciseTimeNative::s_iFrequencySeconds = GetPerformanceFrequency();
double PreciseTimeNative::s_dInverseFrequencySeconds = s_iFrequencySeconds > 0 ? 1.0 / s_iFrequencySeconds : 0.0;

} // namespace KEP