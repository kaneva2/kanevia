///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#include "StdAfx.h"
#include "Broadcaster.h"

namespace KEP {

IListener::IListener() {
} // IListener::IListener

IListener::~IListener() {
} // IListener::~IListener

Broadcaster::Broadcaster() {
} // Broadcaster::Broadcaster

Broadcaster::~Broadcaster() {
} // Broadcaster::~Broadcaster

bool Broadcaster::Attach(IListener* pListener) {
	if (!pListener)
		return false;

	m_listeners.insert(pListener);
	return true;
} // Broadcaster::Attach

bool Broadcaster::Detach(IListener* pListener) {
	if (!pListener)
		return false;
	if (m_listeners.erase(pListener))
		return true;
	return false;
} // Broadcaster::Detach

void Broadcaster::Broadcast() {
	ListenerContainer::iterator listenerIterator;
	ListenerContainer::iterator listenerIteratorEnd;

	listenerIterator = m_listeners.begin();
	listenerIteratorEnd = m_listeners.end();
	for (; listenerIteratorEnd != listenerIterator; ++listenerIterator) {
		Pointer<IListener> pListner = *listenerIterator;
		pListner->Update(this);
	} // for..listenerIteratorEnd != listenerIterator
} // Broadcaster::Broadcast< Event >
} // namespace KEP
