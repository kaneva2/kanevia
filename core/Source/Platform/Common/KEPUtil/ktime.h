/******************************************************************************
 KTime.h

 Copyright (c) 2004-2011 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

/**
 * Lightweight timestamp implementation with second resolution.  Aside from 
 * (_s)afe version of  localtime(), it is platform neutral.
 * 
 * Note that unlike GetSystemTime(), this class makes no assumptions about
 * formatting or presentation as it does not create a formatted string
 * representation.  Formatting is left for users of this class.
 */
#include <time.h>
class KTimeStamp {
public:
	/**
	 * Default constructor
	 */
	KTimeStamp() {
		time_t tt = time(0);
		localtime_s(&_tm, &tt);
	}

	/**
	 * dtor
	 */
	virtual ~KTimeStamp() {}

	/**
	 * Acquire the year represented by the timestamp.
	 */
	int year() { return _tm.tm_year + 1900; }

	/**
	 * Acquire the month of year. (1-12)
	 */
	int month() { return _tm.tm_mon + 1; }

	/**
	 * Acquire the day of the month (1-n)
	 */
	int dayOfMonth() { return _tm.tm_mday; }

	/**
	 * Acquire the day of the week (0-6)
	 */
	int dayOfWeek() { return _tm.tm_wday; }

	/**
	 * Acquire the hour of the day (0-23)
	 */
	int hour() { return _tm.tm_hour; }

	/**
	 * Acquire the minute of the hour (0-59)
	 */
	int minute() { return _tm.tm_min; }

	/**
	 * Acquire the second of the minute (0-59)
	 */
	int second() { return _tm.tm_sec; }

private:
	struct tm _tm;
};
