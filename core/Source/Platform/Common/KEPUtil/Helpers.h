///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define _USE_MATH_DEFINES

// Common C Includes
#include <assert.h>
#include <d3dx9.h>
#include <direct.h>
#include <io.h>
#include <math.h>
#include <stdlib.h>

// Common STL Includes
#include <chrono>
#include <ios>
#include <iostream>
#include <set>
#include <map>
#include <list>
#include <stack>
#include <vector>
#include <queue>
#include <memory>
#include <mutex>
#include <string>
#include <strstream>
#include <fstream>
#include <limits>
#include <algorithm>
#include <thread>

// Common Win32 Includes
#include "WinInet.h"

// Common Tools Includes
#ifndef _NT
#define _NT
#endif

// Common Kaneva Includes
#include "common/include/Glids.h"
#include "common/include/KEPCommon.h"
#include "common/include/KEPConstants.h"
#include "common/include/KEPException.h"
#include "common/include/KEPFileNames.h"
#include "common/include/KEPHelpers.h"
#include "common/include/KEPMacros.h"
#include "common/include/LogHelper.h"
#include "common/include/XmlHelper.h"

// Common Kaneva Utility Includes
#undef DLLExport
#define DLLExport KEPUTIL_EXPORT
#include "common/KEPUtil/Numeric.h"
#include "common/KEPUtil/Algorithms.h"
#include "Core/Util/ChainedException.h"
#include "Core/Util/Monitored.h"
#include "common/KEPUtil/RefPtr.h"

#include "Core/Math/KEPMathUtil.h"
#include "Core/Util/HighPrecisionTime.h"
#include "Core/Util/Mutex.h"

// DRF - Math Conversions
inline double DegNorm(double d) {
	return KEP::FModPositive(d, 360.0);
}
inline double RadNorm(double r) {
	return KEP::FModPositive(r, 2 * M_PI);
}

// DRF - Statistics Macros
#define UPDATE_NUM(num) num++
#define UPDATE_AVG(avg, num, val) avg = (((num - 1) * avg) + val) / num
#define UPDATE_MIN(min, val) min = (min < val) ? min : val
#define UPDATE_MAX(max, val) max = (max > val) ? max : val

/** DRF 
 * Returns if windows is Vista or greater.
 */
DLLExport bool IsWindowsVistaOrGreater();

// DRF - System Memory Info
struct SysMemoryInfo {
	ULONGLONG m_sizeBytes; // system memory installed (bytes)

	SysMemoryInfo() :
			m_sizeBytes(0) {}
};

/** DRF 
 * Returns the current system memory information.
 */
DLLExport bool GetSysMemoryInfo(SysMemoryInfo& sysMem);

// DRF - System Info
struct SysInfo {
	bool m_bInitialized;
	DWORD m_majorVersion;
	DWORD m_minorVersion;
	DWORD m_buildNumber;
	std::string m_sysVersion; // operating system version string 'major.minor.build'
#if defined(KANEVA_ORIG)
	std::string m_sysName; // system computer name 'test-server'
#endif
	SysMemoryInfo m_sysMemoryInfo; // system memory information

	SysInfo() :
			m_bInitialized(false) {}
};

/** DRF 
 * Returns the current system information.
 */
DLLExport bool GetSysInfo(SysInfo& sysInfo);

// DRF - CPU Information
struct CpuInfo {
	size_t m_nPhysicalCores = 0; // Physical cpu cores
	size_t m_nLogicalCores = 0; // Logical cpu cores
	std::string m_cpuVer; // cpu version string eg. '586.1'
	ULONG m_cpuMhz = 0; // cpu maximum frequency (Mhz)
#if defined(KANEVA_ORIG)
	std::string m_strCpuVendor;
	std::string m_strCpuBrand;
	std::string m_strCpuCodeName;
	int m_iCpuFamily = 0; // family + extended_family
	int m_iCpuModel = 0;
	int m_iCpuStepping = 0;
	int m_iSSE3 = 0;
	int m_iSSSE3 = 0;
	int m_iSSE4_1 = 0;
	int m_iSSE4_2 = 0;
	int m_iSSE4_A = 0;
	int m_iSSE5 = 0;
	int m_iAVX = 0;
	int m_iAVX2 = 0;
	int m_iFMA3 = 0;
	int m_iFMA4 = 0;
	int m_iAvx512BW = 0;
	int m_iAvx512CD = 0;
	int m_iAvx512DQ = 0;
	int m_iAvx512ER = 0;
	int m_iAvx512F = 0;
	int m_iAvx512PF = 0;
	int m_iAvx512VL = 0;
#endif
};

/** DRF 
 * Returns the current cpu information.
 */
DLLExport bool GetCpuInfo(CpuInfo& cpuInfo);

// DRF - GPU Information
struct GpuInfo {
	std::string m_gpuName; // gpu name string eg. 'AMD (ATI) FirePro M6000 Graphics'
	std::string m_dxVer; // directx latest version
};

/** DRF 
 * Returns the current gpu information.
 */
DLLExport bool GetGpuInfo(GpuInfo& gpuInfo);

// DRF - Disk Information
struct DiskInfo {
	ULONGLONG m_sizeBytes; // disk size (bytes)
	ULONGLONG m_availBytes; // space available (bytes)

	DiskInfo() :
			m_sizeBytes(0), m_availBytes(0) {}
};

/** DRF 
 * Returns the current disk information.
 */
DLLExport bool GetDiskInfo(DiskInfo& diskInfo);

/** DRF 
 * Gets the full path of the given module (default = current process.exe)
 */
DLLExport std::string GetModulePath(HMODULE hModule = 0);

/** DRF
 * Gets the version string of the given module as 'xx.xx.xx.xx' (default = current process.exe)
 */
DLLExport std::string GetModuleVersion(const std::string& modulePath = "");

/** DRF 
 * Gets the full path with filename.ext of the running application module.
 * NOTE - This does not point to the right folder within the debugger. Application should
 * use PathApp() instead for all base directory path building.
 */
DLLExport std::string GetThisModulePath();

/** DRF 
 * Gets the path only without the filename.ext of the running application module.
 * NOTE - This does not point to the right folder within the debugger. Application should
 * use PathApp() instead for all base directory path building.
 */
DLLExport std::string GetThisModulePathOnly();

/** DRF 
 * Gets the filename.ext of the running application.
 */
DLLExport std::string GetThisModuleFileName();

/** DRF
 * Gets the version string of the running application.
 */
DLLExport std::string GetThisModuleVersion();

/** DRF
 * Gets the version string of the running application.
 */
DLLExport std::string VersionApp(bool log = false);

/** DRF
 * Returns the working path (without filename) of the application at launch (good for debugger).
 */
DLLExport std::string PathApp(bool log = false);

/** DRF
 * Sets the working path (without filename) of the application at launch.
 */
DLLExport bool SetPathApp(const std::string& path, bool log = false);

/** DRF
 * Returns the current working directory.
 */
DLLExport std::string PathCurrent(bool log = false);

/** DRF
 * Sets the current working directory.
 */
DLLExport bool SetPathCurrent(const std::string& path, bool log = false);

/** DRF
 * Returns true if the given file path exists or false otherwise.
 */
DLLExport bool PathExists(const std::string& path, bool log = true);

/** DRF
 * Returns the found path based on the following search order or the given path if not found.
 * - pathSearch
 * - pathCurrent
 * - pathModule
 * - pathApp
 */
DLLExport std::string PathFind(const std::string& pathSearch, const std::string& path, bool log = true);

// DRF - Application Memory Statistics
struct AppMemoryStats {
	ULONGLONG m_num; // number of process memory checks
	ULONGLONG m_avg; // average process memory used (bytes)
	ULONGLONG m_min; // minimum process memory used (bytes)
	ULONGLONG m_max; // maximum process memory used (bytes)
	ULONGLONG m_used; // current process memory used (bytes)

	MEMORYSTATUSEX m_statex;

	AppMemoryStats() :
			m_num(0),
			m_avg(0),
			m_min((ULONGLONG)-1),
			m_max(0),
			m_used(0) {
		m_statex.dwLength = sizeof(m_statex);
	}

	DLLExport void Log();
};

/** DRF 
 * Updates and returns the process memory statistics with optional reset.
 */
DLLExport AppMemoryStats GetAppMemoryStats(bool resetStats = false);

/** DRF 
 * Returns the process cpu total usage time (kernel + user) in milliseconds.
 */
DLLExport ULONGLONG GetAppCpuTime();

// Network Card Information
struct NetInfo {
	DWORD indexBest; // best network local index
	DWORD index; // network local index
	size_t type; // network card type
	std::string desc; // network card identifier
	std::string mac; // network card mac address 'xxxxxxxxxxxxxxxx'
	std::string macHash; // all network cards mac address hash

	NetInfo() :
			type(0) {}

	bool IsValid() { return !mac.empty(); }
};

// Returns network information.
DLLExport bool GetNetInfo(NetInfo& netInfo);

// RPC UUID Wrapper - Returns newly created UUID as string.
DLLExport bool UuidCreate(std::string& uuidStr);

// MsgProc Support
struct DLLExport MsgProcInfo {
	HWND hWnd;
	UINT uMsg;
	WPARAM wParam;
	LPARAM lParam;
	POINT cursorPoint;

	MsgProcInfo();
	MsgProcInfo(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	std::string ToString(bool verbose);

	// Application Messages
	bool IsClose();
	bool IsActivate();

	// Mouse Messages
	bool IsMouse();
	bool IsMouseButtonUp();
	bool IsMouseButtonDown();
	bool IsMouseButtonDblClk();
	bool IsMouseMove();
	bool IsMouseWheel();
	POINT CursorPoint();

	// Keyboard Messages
	bool IsKey();
	bool IsKeyUp();
	bool IsKeyDown();
	bool IsKeyChar();
};
DLLExport void MsgProcLog(bool enable);
DLLExport bool MsgProcPumpUntilEmpty(size_t maxMessages = 0, UINT msgUntil = 0, bool useMFC = true);

DLLExport bool GetCursorEnabled();
DLLExport void SetCursorEnabled(bool enable);

#define PointInsideRect(rect, point) (::PtInRect(&(rect), point) != FALSE)

// DRF - Global D3DDevice GetSet
DLLExport LPDIRECT3DDEVICE9 GlobalD3DDevice(LPDIRECT3DDEVICE9 pd3dDevice = NULL);

// Server does not link to MFC so can't call AfxEnableMemoryLeakDump directly.
// KEPUtil already links to MFC libraries so we wrap it here to be called by server.
KEPUTIL_EXPORT void EnableMFCDebugMemoryDump(bool bEnable);

#define G_DEVICE(...)                   \
	auto gDevice = ::GlobalD3DDevice(); \
	if (!gDevice)                       \
		return __VA_ARGS__;

// Creates a thread using the given worker function, parameter, and priority.
DLLExport HANDLE CreateThread(LPTHREAD_START_ROUTINE pFunc, LPVOID param, int priority);

DLLExport void Helpers_SetDevMode(int devMode);
DLLExport int Helpers_GetDevMode();