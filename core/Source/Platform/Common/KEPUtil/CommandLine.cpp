///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "commandline.h"

namespace KEP {

namespace CommandLineI {

StringParameter::StringParameter(OldCommandLine& cl, const std::string& paramName, const std::string& defaultValue, const std::string& help) :
		GenericParameter(cl, paramName, defaultValue, help) {}

StringParameter::StringParameter(OldCommandLine& cl, const std::string& paramName, const std::string& help) :
		GenericParameter(cl, false, paramName, help) {}

StringParameter::~StringParameter() {}

std::string StringParameter::valueString() const {
	return value();
}

std::string StringParameter::defaultValueString() const {
	return commandLine().parameters().toString(defaultValue());
}

void StringParameter::parseInput(std::vector<std::string>::iterator& iinput) {
	setValue(*(++iinput));
}

// Need to override this so that we can handle use parameterized defaults
//
std::string
StringParameter::value() const {
	OldCommandLine& cl = commandLine();
	ParamSet& pstring = cl.parameters();
	if (_actualValueSet)
		return _actualValue;
	std::string def = pstring.toString(_defaultValue);
	return def;
}

} // namespace CommandLineI

//// Command Line /////////////////////////////////
OldCommandLine::OldCommandLine() :
		_help(*this) {
	// Our command line parameter.  Like a boolean
	// accept during parse it displays help text.
	//
	HelpParameter* param = new HelpParameter(*this);
	addParameter(*param);
}

OldCommandLine::OldCommandLine(const OldCommandLine& toCopy) :
		_help(*this) {
	if (toCopy._paramMap.size() > 0) { /* TODO: Implement map copy */
	}
}

OldCommandLine::~OldCommandLine() {
	for (ParameterMapT::iterator iter = _paramMap.begin(); iter != _paramMap.end(); iter++) {
		delete iter->second;
	}
	_paramMap.clear();
}

OldCommandLine&
OldCommandLine::operator=(const OldCommandLine& rhs) {
	if (rhs._paramMap.size() > 0) {
	}
	return *this;
}

OldCommandLine&
OldCommandLine::addParameter(Parameter& parameter) {
	_paramMap.insert(std::pair<std::string, Parameter*>(parameter.name(), &parameter));
	return *this;
}

OldCommandLine::BooleanParameter&
OldCommandLine::addBoolean(const std::string& paramName, bool defaultValue, const std::string& help) {
	BooleanParameter* param = new BooleanParameter(*this, paramName, defaultValue, help);
	addParameter(*param);
	return *param;
}

OldCommandLine::StringParameter&
OldCommandLine::addString(const std::string& paramName, const std::string& dflt, const std::string& help) {
	StringParameter* param = new StringParameter(*this, paramName, dflt, help);
	addParameter(*param);
	return *param;
}

void OldCommandLine::showHelp() const {
	std::cout << OldCommandLine::helpString() << std::endl;
}

OldCommandLine&
OldCommandLine::parse(size_t argc, char** argv) {
	int keyMode = 1;
	int valMode = 2;
	int mode = keyMode;
	if (mode) {
	}
	if (valMode) {
	}

	if (argc == 0)
		return *this;

	//	_executable.setSubject(argv[0]);				// Store and parse the executable name
	_executable = FileName::parse(argv[0]);
	_params.set("AppName", _executable.fname()); // Set appname ParamString value.
		// Now string params can reference this value.

	std::vector<std::string> svec;
	for (size_t n = 1; n < argc; n++) svec.push_back(std::string(argv[n]));

	for (std::vector<std::string>::iterator iinput = svec.begin(); iinput != svec.end(); iinput++) {
		ParameterMapT::iterator iparm = _paramMap.find(*iinput);
		if (iparm == _paramMap.end()) {
			_orphans.push_back(*iinput);
			continue;
		}
		iparm->second->parseInput(iinput);
	}
	return *this;
}

const FileName&
OldCommandLine::executable() const {
	return _executable;
}

std::vector<std::string>
OldCommandLine::orphans() {
	return _orphans;
}

CommandLineI::Parameter*
OldCommandLine::getParameter(const std::string& paramName) {
	return _paramMap[paramName];
}

std::string
OldCommandLine::helpString() const {
	std::stringstream ss;

	ss << "Parameter        Description      Default" << std::endl;
	for (ParameterMapT::const_iterator iter = _paramMap.begin(); iter != _paramMap.end(); iter++) {
		const std::string t = iter->second->help();
		ss << iter->second->name() << "     "
		   << iter->second->help() << " Default value: "
		   << iter->second->defaultValueString() << std::endl;
	}
	return ss.str();
}

ParamSet&
OldCommandLine::parameters() {
	return _params;
}

void OldCommandLine::HelpParameter::parseInput(std::vector<std::string>::iterator& iinput) {
	setValue(true);
	commandLine().showHelp();
}

OldCommandLine::HelpParameter::HelpParameter(OldCommandLine& cmdLine) :
		//_cmdLine(cmdLine)		,
		CommandLineI::BooleanParameter(cmdLine, "--help", false) {}

} // namespace KEP