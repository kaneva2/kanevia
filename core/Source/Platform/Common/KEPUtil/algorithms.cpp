///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "Algorithms.h"
#include "boost/type_traits.hpp"
#include "boost/token_functions.hpp"
#include "boost/tokenizer.hpp"

namespace KEP {

std::vector<StringPairT>
StringHelpers::parseKeyValuePairs(const std::string &paramString, char pairDelimiter /* = ';' */, char keyValueDelimiter /* = '=' */) {
	std::vector<StringPairT> ret;
	boost::escaped_list_separator<char> sep('`', pairDelimiter, '\'');
	typedef boost::tokenizer<boost::escaped_list_separator<char>> tokenizer;

	tokenizer tokens(paramString, sep);
	for (tokenizer::iterator iter = tokens.begin(); iter != tokens.end(); ++iter)
		ret.push_back(parseKeyValuePair(*iter, keyValueDelimiter));
	return ret;
}

StringPairT
StringHelpers::parseKeyValuePair(const std::string &keyvalpairString, char keyValueDelimiter /* = '=' */) {
	// Use find/substr to extract key and value from token to avoid the need of double escaping.
	std::string::size_type ofs = keyvalpairString.find(keyValueDelimiter);
	if (ofs == std::string::npos) {
		std::stringstream ss;
		ss << "Error parsing property key/val pair [" << keyvalpairString << "]";
		throw ParseException(SOURCELOCATION, ss.str());
	};
	std::string key = keyvalpairString.substr(0, ofs);
	std::string val = keyvalpairString.substr(ofs + 1);

	// Key must not be empty
	if (key.empty()) {
		std::stringstream ss;
		ss << "Error parsing property key/val pair [" << keyvalpairString << "]";
		throw ParseException(SOURCELOCATION, ss.str());
	};
	return StringPairT(key, val);
}

/**
 * Parse parameter overrides. Input must be in following format: [opts]key=value;[opts]key=value...
 *
 * Delimiters can be overridden by caller. Defaults are '=' and ';'. Keys must begin with a letter.
 * Values can be optionally quoted with single quotes. Single quotes are not parts of the value and will be 
 * discarded by tokenizer. If a value itself includes single quotes, they should be escaped by a backtick "`".
 *
 * [opts] Additional options to allow a variable to be used for multiple purposes. 
 *        Currently following options are planned: 
 *          <empty>: no options means it's for configuration logical substitutions only (this is default and always effective); 
 *                $: dollar sign is used to indicate custom environment variables (retrievable by getenv function);  
 *            <TBD>: indicates this is also a database variable. 
 *            <TBD>: indicates this is also a LUA/Python VM global variable. 
 *        Options are not part of a key. Parser should remove options from the return value.
 *
 * Example 1: GAME_ID=12345;GAME_NAME=TestersApp        -> Set GAME_ID to [12345], GAME_NAME to [TestersApp]         (Simple value)
 * Example 2: "GAME_ID=12345;GAME_NAME=Testers App"     -> Set GAME_ID to [12345], GAME_NAME to [Testers App]        (Value with space)
 * Example 3: GAME_ID=12345;GAME_NAME='Testers;App'     -> Set GAME_ID to [12345], GAME_NAME to [Testers;App]        (Value with semicolon)
 * Example 4: GAME_ID=12345;GAME_NAME=Tester`'sApp      -> Set GAME_ID to [12345], GAME_NAME to [Tester'sApp]        (Value with single quote)
 * Example 5: GAME_ID=12345;GAME_NAME=Tester``sApp      -> Set GAME_ID to [12345], GAME_NAME to [Tester`sApp]        (Value with backtick)
 * Example 6: GAME_ID=12345;GAME_NAME='Tester`'s;App'   -> Set GAME_ID to [12345], GAME_NAME to [Tester's;App]       (Value with single quote and semicolon)
 * Example 7: "GAME_ID=12345;GAME_NAME=Tester`'s App"   -> Set GAME_ID to [12345], GAME_NAME to [Tester's App]       (Value with single quote and space)
 * Example 8: $GAME_ID=12345                            -> Set GAME_ID to [12345], 'ALSO' set environment variable GAME_ID to 1234.
 * 
 */
std::vector<StringPairT>
StringHelpers::parseKeyValuePairsWithOption(const std::string &paramString, char optionFilter /* = '\0' */,	char pairDelimiter /* = ';' */,	char keyValueDelimiter /* = '=' */) {
	std::vector<StringPairT> ret;
	boost::escaped_list_separator<char> sep('`', pairDelimiter, '\'');
	typedef boost::tokenizer<boost::escaped_list_separator<char>> tokenizer;

	tokenizer tokens(paramString, sep);
	for (tokenizer::iterator iter = tokens.begin(); iter != tokens.end(); ++iter) {
		std::string token = *iter;

		// Check option filter
		bool matched = false;
		std::string::size_type i;
		for (i = 0; i < token.length(); i++) {
			matched = matched || optionFilter == '\0' || token[i] == optionFilter;

			if (isalpha(token[i])) {
				break;
			}
		}

		if (matched) {
			if (i > 0) {
				// Trim off options prefix
				token = token.substr(i);
			}

			ret.push_back(parseKeyValuePair(token, keyValueDelimiter));
		}
	}
	return ret;
}

} // namespace KEP