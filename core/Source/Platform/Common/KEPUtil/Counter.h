///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

/**
 * Created primarily to aid in Unit Testing of multithreaded constructs,
 * this class provides simple counter capabilities that are thread safe.
 * Note that this class only supports incrementing the counter.  It does
 * not support decrement and arbitrary math ops. If that should become
 * necessary, that would be the time we should consider a synchronized
 * implementation of Numeric<T> (see Numeric.h)
 */
template <typename CountedT>
class Counter {
public:
	/**
	 * Construct with a supplied initial value that defaults to 0
	 * @param initial	The initial value of the counter.
	 */
	Counter(unsigned long initial = 0) :
			_counter(initial) {}

	/**
	 * Destructor
	 */
	virtual ~Counter() {}

	/**
	 * Increment the counter.
	 * @return Reference to self.
	 */
	Counter& operator++() {
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		++_counter;
		return *this;
	}

	/**
	 * Acquire the current value of this counter.  N.B. this 
	 * method is not synchronized.  I.e. the counter's value
	 * could change between the time this method is called
	 * and the value is returned.
	 *
	 * @return The value of this counter.
	 */
	CountedT value() { return _counter; }

private:
	fast_recursive_mutex _sync;
	CountedT _counter;
};