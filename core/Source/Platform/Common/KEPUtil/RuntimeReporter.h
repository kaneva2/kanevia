///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Helpers.h"
#include <Core/Util/Stream.h>

// Install Registry Support
DLLExport bool _RegInstall(const std::string& appName, const std::string& regKey);

// Registry Current User API [HKEY_CURRENT_USER/Software/Kaneva/<appName>/<regKey>]
DLLExport bool _RegCuRxStr(const std::wstring& name, std::string& val);
DLLExport bool _RegCuTxStr(const std::wstring& name, const std::string& val);
DLLExport bool _RegCuRxInt(const std::wstring& name, int& val);
DLLExport bool _RegCuTxInt(const std::wstring& name, const int& val);

// Registry Local Machine API [HKEY_LOCAL_MACHINE/Software/Kaneva/<appName>/<regKey>]
DLLExport bool _RegLmRxStr(const std::wstring& name, std::string& val);
DLLExport bool _RegLmTxStr(const std::wstring& name, const std::string& val);
DLLExport bool _RegLmRxInt(const std::wstring& name, int& val);
DLLExport bool _RegLmTxInt(const std::wstring& name, const int& val);

/// Runtime Reporter API
namespace RuntimeReporter {

/// Runtime Reporter Report Type Enumeration
enum REPORT_TYPE {
	REPORT_TYPE_RUNNING, // report sent on state change to running
	REPORT_TYPE_STOPPED, // report sent on state change to stopped
	REPORT_TYPE_CRASHED, // report sent on state change to crashed
	REPORT_TYPE_CUSTOM, // report sent on application demand
	REPORT_TYPES
};

/// Runtime Reporter Profile Structure
struct Profile {
	std::string version; // application module version 'xx.xx.xx.xx'
	TimeSec secRuntime; // profile runtime in seconds since report
	size_t numRuns; // profile number of crashes since report
	size_t numCrashes; // profile number of crashes since report

	Profile() :
			secRuntime(0), numRuns(0), numCrashes(0) {}
};

/// Runtime Reporter Parameter Selection Bitmap
typedef int PARAM_SELECT;
enum {
	PARAM_SELECT_NONE = 0x0000, // no parameters mask
	PARAM_SELECT_ALL = 0xffff, // all parameters mask
	// Profile Parameters
	PARAM_SELECT_APP = 0x0001, // application parameters (appName, appVersion)
	PARAM_SELECT_RUNTIME = 0x0002, // runtime parameters (runtime, runs, crashes)
	PARAM_SELECT_RUNTIME_ID = 0x0004, // runtime identifier (runtimeId)
	PARAM_SELECT_SYSTEM_ID = 0x0008, // system identifier (systemId)
	// Running Report Type Parameters
	PARAM_SELECT_CPU_INFO = 0x0010, // cpu information (cpus, cpuVer, cpuMhz)
	PARAM_SELECT_SYS_INFO = 0x0020, // system information (sysVer, sysMem)
	PARAM_SELECT_DISK_INFO = 0x0040, // disk information (diskSize, diskAvail)
	PARAM_SELECT_NET_INFO = 0x0080, // network information (net, netMAC, netType)
	// Stopped/Crashed Report Type Parameters
	PARAM_SELECT_APP_INFO = 0x0100, // application information (appCpuTime, appMem*)
};
#define PARAM_SELECTED(ps) ((paramSelect & ps) != 0)

/// Runtime Reporter Callback Struct
struct CallbackStruct {
	REPORT_TYPE type; // report type
	std::string params; // report additional parameters
	bool willSend; // report will be sent
	bool willLog; // report will be logged

	CallbackStruct(const REPORT_TYPE& type) :
			type(type),
			willSend(true),
			willLog(true) {}
};

/** DRF
	* Application Callback allowing report callback struct inspection and  modification before report.
	*/
typedef void (*APP_CALLBACK)(CallbackStruct& cbs);

/** DRF
	* Installs Runtime Reporter by initializing registry key support under .../<appName>/<regKey>
	* where runtime profile data is maintained for the running application. If not provided runtimeId
	* is created as a UUID.
	*/
DLLExport bool Install(const std::string& appName, const std::string& regKey, const std::string& url, APP_CALLBACK pFunc = NULL, const std::string& strRuntimeId = "");

/** DRF
	* Resets the session runtime timer.
	*/
DLLExport bool RuntimeReset();

/** DRF
	* Returns current runtime in seconds.
	*/
DLLExport TimeSec RuntimeSec();

/** DRF
	* Resets all profile accumulators (numRuns, numCrashes, version).
	* Returns true on success or false on failure to reset the registry values.
	*/
DLLExport bool ProfileReset();

/** DRF
	* Call this to update the registry profile with the given profile values:
	*  - version - 'xx.xx.xx.xx' mismatch resets entire registry profile 
	*  - secRuntime - accumulated session runtime in seconds since last report
	*  - numRuns - accumulated number of runs since last report
	*  - numCrashes - accumulated number of crashes since last report
	* Returns true on success or false on failure to update registry profile.
	*/
DLLExport bool ProfileUpdate(Profile* pProfile = NULL);

/** DRF
	* Set profile state to 'Running', update profile numRuns, optionally send 'Running' report.
	* Returns true on success or false on failure to set profile state.
	*/
DLLExport bool ProfileStateRunning(bool sendReport = true);

/** DRF
	* Set profile state to 'Stopped', update profile, optionally send 'Stopped' report.
	* Returns true on success or false on failure to set profile state.
	*/
DLLExport bool ProfileStateStopped(bool sendReport = true);

/** DRF
	* Set profile state to 'Crashed', update profile numCrashes, optionally send 'Crashed' report.
	* Returns true on success or false on failure to set profile state.
	*/
DLLExport bool ProfileStateCrashed(bool sendReport = true);

/** DRF
	* Returns the unique runtime identifier (string GUID) as reported in the response to any 
	* runtime report web call.
	*/
DLLExport std::string RuntimeId();

/** DRF
	* Returns the unique system identifier (string GUID) as reported in the response to any 
	* runtime report web call.
	*/
DLLExport std::string SystemIdReg();

/** DRF
	* Updates the profile and builds the parameter string as selected and ready to be
	* appended to the report webcall.
	*/
DLLExport std::string ParamSelect(PARAM_SELECT paramSelect);

/** DRF
	* Uses the 'RuntimeReporter' web caller to actually send the given report type by first performing
	* the application callback allowing the application to specify parameters that will be appended to 
	* the url in additional to the parameters selected and optionally cancelling sending the report by 
	* setting willSend false. On success of any report sent containing PARAM_SELECT_RUNTIME the runtime 
	* profile is then reset. Returns true on success or false on failure.
	*/
DLLExport bool SendReport(REPORT_TYPE type, PARAM_SELECT paramSelect);
DLLExport bool SendReport(REPORT_TYPE type);

DLLExport void WaitForReportResponseComplete();
}; // namespace RuntimeReporter

// MS - Report Parameter Helpers
class RuntimeReporterParams {
public:
	RuntimeReporterParams(bool bIgnoreEmpty) :
			m_bIgnoreEmpty(bIgnoreEmpty) {}

	// "name" is assumed not to need special encoding.
	// If you need unusual characters in the name string, you should call EncodeString on it before passing it into this function.
	template <typename NameType, typename NumericType, std::enable_if_t<std::is_arithmetic<NumericType>::value, nullptr_t> = nullptr>
	void Add(const NameType& name, NumericType val) {
		if (!m_bIgnoreEmpty || val != 0)
			AddEncodedString(ToCharPtr(name), KEP::StreamToString(val));
	}
	template <typename NameType>
	void Add(const NameType& name, const std::string& val) {
		if (!m_bIgnoreEmpty || !val.empty())
			AddUnencodedString(ToCharPtr(name), val);
	}
	virtual void AddEncodedString(const char* name, const std::string& val) = 0;
	virtual void AddUnencodedString(const char* name, const std::string& val) {
		AddEncodedString(name, EncodeString(val));
	}
	virtual std::string EncodeString(const std::string& val) = 0;

	std::string& GetString() { return m_strm.GetString(); }

private:
	static const char* ToCharPtr(const char* p) { return p; }
	static const char* ToCharPtr(const std::string& str) { return str.c_str(); }

protected:
	bool m_bIgnoreEmpty;
	KEP::OStringStream m_strm;
};

class RuntimeReporterWebParams : public RuntimeReporterParams {
public:
	RuntimeReporterWebParams() :
			RuntimeReporterParams(true) {}
	virtual void AddEncodedString(const char* name, const std::string& val) final {
		m_strm << "&" << name << "=" << val;
	}
	virtual std::string EncodeString(const std::string& val) final {
		return UrlEncode(val, true);
	}
};

class RuntimeReporterJSONParams : public RuntimeReporterParams {
public:
	RuntimeReporterJSONParams() :
			RuntimeReporterParams(false) {}
	virtual void AddEncodedString(const char* name, const std::string& val) final {
		if (m_nAdded != 0)
			m_strm << ", ";
		m_strm << "\"" << name << "\": " << val << "";
		++m_nAdded;
	}
	virtual std::string EncodeString(const std::string& val) final {
		return JSONEncodeString(val, true);
	}

private:
	size_t m_nAdded = 0;
};
