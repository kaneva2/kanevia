///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RefPtr.h"
#include "Helpers.h"
#include "Core/Util/fast_mutex.h"

#if (DRF_REF_PTR == 1)

static LogInstance("Instance");

#define LogQuiet(txt) \
	if (!quiet) {     \
		LogInfo(txt); \
	}

static bool refPtrEnable = DRF_REF_PTR_ENABLE; // reference pointer enable flag

typedef std::map<void*, size_t> REF_PTRS; // map<pointer, identifier index>
static REF_PTRS refPtrs; // container of valid reference pointers
static KEP::fast_recursive_mutex refPtrsMutex; // reference pointers mutex

typedef std::vector<std::string> REF_IDS;
static REF_IDS refIds; // container of valid reference identifiers

/** DRF
* Cross reference an identifier to its collection index, adding the identifier if not in the collection.
*/
static size_t RefId(const std::string& id, bool quiet) {
	// Reference Pointers Enabled ?
	if (!refPtrEnable)
		return 0;

	std::lock_guard<KEP::fast_recursive_mutex> lock(refPtrsMutex);

	// Reference Identifier Already Exists?
	size_t i;
	for (i = 0; i < refIds.size(); ++i)
		if (refIds[i] == id)
			return i;

	// Add New Reference Identifier
	refIds.push_back(id);

	LogQuiet("[" << i << "] -> (" << id << ")");

	return i;
}

void RefPtrEnable(bool enable, bool quiet) {
	refPtrEnable = enable;
}

void RefPtrClear(bool quiet) {
	// Reference Pointers Enabled ?
	if (!refPtrEnable)
		return;

	{
		std::lock_guard<KEP::fast_recursive_mutex> lock(refPtrsMutex);

		// Clear All Reference Pointers
		refPtrs.clear();
	}

	LogQuiet("OK");
}

void RefPtrLog(bool verbose) {
	// Reference Pointers Enabled ?
	if (!refPtrEnable)
		return;

	std::lock_guard<KEP::fast_recursive_mutex> lock(refPtrsMutex);

	// For Each Reference Identifier
	size_t ids = (verbose ? refIds.size() : 1);
	for (size_t id = 0; id < ids; ++id) {
		std::stringstream ss;
		if (verbose)
			ss << "(" << refIds[id] << ") ";

		// Accumulate Reference Pointers
		size_t ptrs = 0;
		const size_t ptrsPerLine = 20;
		for (auto it = refPtrs.begin(); it != refPtrs.end(); ++it) {
			if (verbose) {
				if (it->second == id)
					if (++ptrs <= ptrsPerLine)
						ss << " <" << it->first << ">";
			} else {
				if (++ptrs <= ptrsPerLine)
					ss << " <" << it->first << ">[" << it->second << "]";
			}
		}
		if (ptrs > ptrsPerLine)
			ss << " ...";

		// Log Accumulated Reference Pointers
		if (ptrs) {
			_LogInfo("RefPtr::Log: num=" << ptrs << " " << ss.str());
		}
	}
}

void RefPtrAdd(const std::string& id, void* p, bool quiet) {
	// Reference Pointers Enabled ?
	if (!refPtrEnable)
		return;

	std::lock_guard<KEP::fast_recursive_mutex> lock(refPtrsMutex);

	// Reference Pointer In Container ?
	int idIndex = RefId(id, quiet);
	auto it = refPtrs.find(p);
	auto found = (it != refPtrs.end());
	if (found || !p) {
		// Reference Pointer Already In Container
		LogDebug("<" << p << "> (" << id << ") NO");
	} else {
		// Add Reference Pointer To Container
		refPtrs[p] = idIndex;
		LogQuiet("<" << p << "> (" << id << ") OK");
	}
}

void RefPtrDel(const std::string& id, void* p, bool quiet) {
	// Reference Pointers Enabled ?
	if (!refPtrEnable)
		return;

	std::lock_guard<KEP::fast_recursive_mutex> lock(refPtrsMutex);

	// Reference Pointer In Container ?
	auto it = refPtrs.find(p);
	auto found = (it != refPtrs.end());
	if (found) {
		// Delete Reference Pointer From Container
		std::string refId = refIds[it->second]; // report stored identifier
		LogQuiet("<" << p << "> (" << refId << ") OK");
		refPtrs.erase(p);
	} else {
		// Reference Pointer Not In Container
		LogQuiet("<" << p << "> (" << id << ") NO");
	}
}

bool RefPtrValid(const char* func, const std::string& id, void* p, bool quiet) {
	// Reference Pointers Enabled ?
	if (!refPtrEnable)
		return true;

	std::lock_guard<KEP::fast_recursive_mutex> lock(refPtrsMutex);

	// Reference Pointer In Container ?
	auto it = refPtrs.find(p);
	auto found = (it != refPtrs.end());
	if (found) {
		// Reference Pointer Valid
		std::string refId = refIds[it->second]; // report stored identifier
		LogDebug("<" << p << "> (" << refId << ") OK - " << func);
	} else {
		// Reference Pointer Invalid
		if (!quiet)
			LogWarn("<" << p << "> (" << id << ") NO - DEREFERENCE MAY CRASH - " << func);
	}

	return found;
}

#endif
