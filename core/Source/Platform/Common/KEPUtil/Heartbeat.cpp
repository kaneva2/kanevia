///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <log4cplus/logger.h>
#include "Common/KEPUtil/Heartbeat.h"
#include "Common/KEPUtil/Algorithms.h"

#include "TinyXML/tinyxml.h"

using namespace log4cplus;

namespace KEP {

class MyRunnable : public Runnable {
public:
	MyRunnable(BasicHeartbeatGenerator& bhg) :
			_bhg(bhg) {}

	virtual ~MyRunnable() {}

	RunResultPtr run() override {
		LOG4CPLUS_TRACE(Logger::getInstance(L"Heartbeat"), "BasicHeartbeatGenerator::MyRunnable::run()");
		do {
			LOG4CPLUS_DEBUG(Logger::getInstance(L"Heartbeat"), "BasicHeartbeatGenerator::MyRunnable::run(): Firing event.");
			_bhg.fire();
			LOG4CPLUS_DEBUG(Logger::getInstance(L"Heartbeat"), "BasicHeartbeatGenerator::MyRunnable::run(): Event fired.");
			if (!_bhg._running)
				break;
			LOG4CPLUS_DEBUG(Logger::getInstance(L"Heartbeat"), "BasicHeartbeatGenerator::MyRunnable::run(): Sleeping for [" << _bhg._intervalMillis << "]");
			_bhg._latch.await(_bhg._intervalMillis);
			LOG4CPLUS_DEBUG(Logger::getInstance(L"Heartbeat"), "BasicHeartbeatGenerator::MyRunnable::run(): Sleeping expired.");
		} while (_bhg._running);

		LOG4CPLUS_DEBUG(Logger::getInstance(L"Heartbeat"), "BasicHeartbeatGenerator::MyRunnable::run(): Exited loop.  Running=" << _bhg._running);
		return RunResultPtr();
	}
	BasicHeartbeatGenerator& _bhg;
};

BasicHeartbeatGenerator::BasicHeartbeatGenerator(unsigned long intervalMillis) :
		_intervalMillis(intervalMillis), HeartbeatGenerator(), _runnable(0), _sync(), _latch(), _running(false) {
	_runnable = new MyRunnable(*this);
	_ep = ExecutablePtr(new Executable(RunnablePtr(_runnable)));
}

// Should be disallowed
BasicHeartbeatGenerator::BasicHeartbeatGenerator(const BasicHeartbeatGenerator& other) :
		_intervalMillis(other._intervalMillis), _latch(), _sync(), _running(false) {}

BasicHeartbeatGenerator&
BasicHeartbeatGenerator::operator=(const BasicHeartbeatGenerator& other) {
	return *this;
}

BasicHeartbeatGenerator::~BasicHeartbeatGenerator() {}

HeartbeatGenerator& BasicHeartbeatGenerator::addHeartbeatListener(HeartbeatListener* listener) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Heartbeat"), "BasicHeartbeatGenerator::addHeartbeatListener()");
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	_listeners.insert(listener);
	return *this;
}

HeartbeatGenerator&
BasicHeartbeatGenerator::removeHeartbeatListener(HeartbeatListener* listener) {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	_listeners.erase(listener);
	return *this;
}

HeartbeatGenerator&
BasicHeartbeatGenerator::start() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Heartbeat"), "BasicHeartbeatGenerator::MyRunnable::run(): Start()");
	_running = true;
	_executor.execute(_ep); // queue up our heartbeat task
	_executor.start(); // and fire up the executor
	return *this;
}

HeartbeatGenerator&
BasicHeartbeatGenerator::stop() {
	LOG4CPLUS_DEBUG(Logger::getInstance(L"Heartbeat"), "BasicHeartbeatGenerator::stop()");
	_running = false; // flag to break out of our loop
	_latch.countDown(); // unblock if it's waiting for next poll
	_ep->act()->wait(); // and wait for the task to finish
	_executor.stop(); // and once it's finished, stop the executor
	return *this;
}

void BasicHeartbeatGenerator::fire() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Heartbeat"), "BasicHeartbeatGenerator::fire()");
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	for (BasicHeartbeatGenerator::SetT::iterator iter = _listeners.begin(); iter != _listeners.end(); iter++) {
		try {
			(*iter)->thump();
		}

		// TODO: Pending mods to ChainedException, catch by reference.
		//
		catch (const ChainedException& exc) {
			LOG4CPLUS_ERROR(Logger::getInstance(L"Heartbeat"), exc.str());
		} catch (...) {
			LOG4CPLUS_ERROR(Logger::getInstance(L"Heartbeat"), "Exception raised by HeartbeatListener.");
		}
	}
}

// Plugin support
HeartbeatGeneratorPluginFactory::HeartbeatGeneratorPluginFactory() :
		AbstractPluginFactory("Heartbeat"), _config(0) {
	// cout << "HeartbeatGeneratorPluginFactory::HeartbeatGeneratorPluginFactory()" << endl;
}

HeartbeatGeneratorPluginFactory::HeartbeatGeneratorPluginFactory(KEPConfig* config) :
		AbstractPluginFactory("Heartbeat"), _config(config) {
	// cout << "HeartbeatGeneratorPluginFactory::HeartbeatGeneratorPluginFactory( KEPConfig* )" << endl;
}

HeartbeatGeneratorPluginFactory::~HeartbeatGeneratorPluginFactory() {
	// cout << "HeartbeatGeneratorPluginFactory::HeartbeatGeneratorPluginFactory()" << endl;
}

const std::string& HeartbeatGeneratorPluginFactory::name() const {
	// cout << "HeartbeatGeneratorPluginFactory::name()" << endl;
	return AbstractPluginFactory::name();
}

/**
 *
 */
PluginPtr HeartbeatGeneratorPluginFactory::createPlugin(const TIXMLConfig::Item& configItem) const {
	TiXmlElement* config = configItem._internal;
	LOG4CPLUS_TRACE(Logger::getInstance(L"Heartbeat"), "HeartbeatGeneratorPluginFactory::createPlugin()");

	// Guard clause.  If no configuration provided, we can go with
	// default settings.  We're done here.
	//
	if (config == 0)
		return PluginPtr(new HeartbeatGeneratorPlugin(*(new BasicHeartbeatGenerator(1000))));

	const TiXmlElement* pe = config->FirstChildElement("intervalMillis");
	if (pe == 0)
		return PluginPtr(new HeartbeatGeneratorPlugin(*(new BasicHeartbeatGenerator(1000))));

	std::string s(pe->GetText());
	unsigned long intervalMillis = atoi(s.c_str());
	return PluginPtr(new HeartbeatGeneratorPlugin(*(new BasicHeartbeatGenerator(intervalMillis))));
}

} // namespace KEP