///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#define MONGOOSE_3_0
//#define MONGOOSE_5_6

#ifdef MONGOOSE_3_0
#include "MongooseHTTPServer30.h"
#endif

#ifdef MONGOOSE_5_6
#include "MongooseHTTPServer56.h"
#endif