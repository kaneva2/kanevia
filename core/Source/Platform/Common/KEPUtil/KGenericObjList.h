///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afx.h"

#include "KEPCommon.h"

#include "Traversable.h"

/**
 * An attempt at extending the CObList MFC collection class in a way that
 * that will promote more reuse as opposed to implementing a new
 * new derivative in order to add small amounts of functionality.
 *
 * Note that this class doesn't use CObList via inheritance but rather 
 * accomplishes this via composition.  I.e. the relation KGenericObjList
 * to CObList is a "hasa" relationship rather than a "isa" relationship.
 * This makes it easy to swap out implementations
 */
template <class ContainedT>
class KGenericObjList
		: public Traversable<ContainedT>,
		  public Selectable<ContainedT>,
		  public Deletable<ContainedT> {
public:
	/**
	 * CObList only holds CObjects, so here is a derivative of this class
	 * that holds a reference to the object we want to place in the list.
	 */
	class Object : public CObject {
	public:
		Object(ContainedT& referent) :
				CObject(), _referent(referent) {}
		virtual ~Object(){};
		ContainedT& referent() { return _referent; }

	private:
		ContainedT& _referent;
	};

	typedef Traverser<ContainedT> TraverserT;
	typedef Traverser<ContainedT> Traverser;
	typedef Selector<ContainedT> SelectorT;
	typedef Selector<ContainedT> Selector;
	typedef Deleter<ContainedT> DeleterT;

public:
	KGenericObjList() :
			_impl(){};

	/**
	 * Destructor.  Note that is incumbent of users of this class to insure that any
	 * objects allocated on the heap, that are contained in this collection, are get
	 * deleted/freed
	 */
	virtual ~KGenericObjList() {
		// Now delete all of the CObjects in the list.
		while ((!_impl.IsEmpty())) {
			Object* pObj = (Object*)_impl.GetTail();
			delete pObj;
			_impl.RemoveTail();
		}
	};

	UINT size() const { return _impl.GetSize(); }

	void append(ContainedT& referent) {
		_impl.AddTail(new KGenericObjList<ContainedT>::Object(referent));
	}

	/**
	 * The select function allows users of this class to iterate over the collection
	 * for the purpose of locating a acquiring a reference to a specific contained object.
	 * As this function traverses over the collection, each contained object is passed 
	 * to this selectors's evaluate() method.  The selector, when it finds the desired
	 * contained object, should return true.  This stops the traversal of the collection
	 * returns the object selected.
	 *
	 * Note: this functionality currently differs in no way from traverse().  It is provided
	 * to allow us to address semantic differences as they arise in the future.
	 * @param traverser   Reference to traverser object which will evaluate each object
	 *                    as it is traversed.
	 * @return            The contained object which caused traverser to evaluate to true, else
	 *                    false
	 */
	ContainedT* select(SelectorT& selector) {
		for (POSITION pos = _impl.GetHeadPosition(); pos != NULL; _impl.GetNext(pos)) {
			Object* pObj = (Object*)_impl.GetAt(pos);
			if (selector.evaluate(pObj->referent()))
				return &(pObj->referent());
		}
		return 0;
	}
	// Allow calling with a temporary.
	ContainedT* select(SelectorT&& selector) {
		return select(selector);
	}

	/**
	 * The traverse function allows users of this class to iterate over the collection.
	 * As this function traverses over the collection, each contained object is passed 
	 * to this traverser's evaluate() method.  The traverser can stop the traverse by 
	 * returning true, at which point, traverse returns the item evaluated.
	 *
	 * Note: this functionality currently differs in no way from select().  It is provided
	 * to allow us to address semantic differences as they arise in the future.
	 * @param traverser   Reference to traverser object which will evaluate each object
	 *                    as it is traversed.
	 * @return            The contained object which caused traverser to evaluate to true, else
	 *                    false
	 */
	KGenericObjList<ContainedT>& traverse(TraverserT& traverser) {
		for (POSITION pos = _impl.GetHeadPosition(); pos != NULL; _impl.GetNext(pos)) {
			Object* pObj = (Object*)_impl.GetAt(pos);
			if (traverser.evaluate(pObj->referent()))
				break;
		}
		return *this;
	}
	// Allow calling with a temporary.
	KGenericObjList<ContainedT>& traverse(TraverserT&& traverser) {
		return traverse(traverser);
	}

	/**
	 * The function uses a Selector to traverse over the collection and remove a single 
	 * contained object.
	 * As this function traverses over the collection, each contained object is passed 
	 * to this selectors's evaluate() method.  The selector can stop the traverse by 
	 * returning true, at which point, remove, removes the selected object from the collection
	 * and returns a pointer to the removed object.
	 *
	 * Note: this functionality currently differs in no way from select().  It is provided
	 * to allow us to address semantic differences as they arise in the future.
	 * @param traverser   Reference to traverser object which will evaluate each object
	 *                    as it is traversed.
	 * @return            The contained object which caused traverser to evaluate to true, else
	 *                    false
	 */
	ContainedT* remove(Deleter<ContainedT>& selector) {
		for (POSITION pos = _impl.GetHeadPosition(); pos != NULL; _impl.GetNext(pos)) {
			Object* pObj = (Object*)_impl.GetAt(pos);
			if (selector.evaluate(pObj->referent())) {
				_impl.RemoveAt(pos);
				return &(pObj->referent());
			}
		}
		return 0;
	}
	// Allow calling remove with a temporary.
	ContainedT* remove(Deleter<ContainedT>&& selector) {
		return remove(selector);
	}

	ContainedT* at(int index) const {
		Object* pObj = (Object*)_impl.GetAt(_impl.FindIndex(index));
		if (pObj)
			return &(pObj->referent());
		return 0;
	}

	ContainedT* removeAt(int index) {
		POSITION pos = _impl.FindIndex(index);
		if (!pos)
			return 0;

		Object* pObj = (Object*)_impl.GetAt(pos);
		if (!pObj)
			return 0;

		ContainedT& pRet = pObj->referent();
		_impl.RemoveAt(pos);
		delete pObj;
		return &pRet;
	}

	void clear() {
		while ((!_impl.IsEmpty())) {
			Object* pObj = (Object*)_impl.GetTail();
			delete pObj;
			_impl.RemoveTail();
		}
		//		_impl.RemoveAll();
	}

private:
	CObList _impl;
};
