///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <map>
#include <sstream>
#include "common\include\KEPCommon.h"
#include "Common\KEPUtil\Algorithms.h"

namespace KEP {

/**
 * Class for internationalized/parameterized String/Message.
 *
 * @author Christopher B. Liebman
 * @version $Id: ParamString.java,v 1.7 1999/11/17 21:36:44 tc Exp $
 */
class Properties {
private:
	typedef std::map<std::string, std::string> storeT;

public:
	Properties() :
			_store() {}
	Properties(const Properties& other) :
			_store(other._store) {}
	Properties& operator=(const Properties& rhs) {
		_store = rhs._store;
		return *this;
	}
	virtual ~Properties() {}
	Properties& operator=(const std::vector<std::pair<std::string, std::string>>& props) {
		for (auto iter = props.cbegin(); iter != props.cend(); iter++) {
			set(iter->first, iter->second);
		}
		return *this;
	}

	const std::string& operator[](std::string& key) { return _store[key]; }
	const std::string& operator[](const char* key) { return _store[std::string(key)]; }
	Properties& set(const std::string& key, const std::string& val) {
		_store[key] = val;
		return *this;
	}
	Properties& set(const std::vector<std::pair<std::string, std::string>>& pairVec) {
		for (auto iter = pairVec.cbegin(); iter != pairVec.cend(); iter++) {
			set(iter->first, iter->second);
		}
		return *this;
	}
	std::string get(const std::string& key) const {
		storeT::const_iterator iter = _store.find(key);
		return iter == _store.cend() ? std::string() : iter->second; /*.op return _store[key]; */
	}
	const std::string toString() const {
		std::stringstream ss;
		for (storeT::const_iterator iter = _store.begin(); iter != _store.end(); iter++)
			ss << iter->first << "=" << iter->second << ";";
		return ss.str();
	}

private:
	storeT _store;
};

class KEPUTIL_EXPORT ParamSet { // implements java.io.Serializable
public:
	/**
	 * Default constructor
	 */
	ParamSet();

	/**
     * Construct a parameterized string object.
     *
     * @param	format	String.
	 */
	ParamSet(const std::string& format);

	/**
     * Construct a parameterized string object.
     *
     * @param	values	Defaults.
     * @param	format	String.
	 * @param   See markers in class description.  Defaults to "%()"
     */
	ParamSet(const Properties& params, const std::string& format);

	/**
	 * dtor
	 */
	virtual ~ParamSet();

	/**
     * Set a named parameter.
     *
     * @param	name		String.
     * @param	value		Object.
     * @return current object.
     */
	ParamSet& set(const std::string& name, const std::string& value);

	/**
     *  Set the list of params for this string all at once.
     *  <p>
     *  @param values	the complete list of values to use in
     *  		building the computed string.  All previously
     * 			set values will be lost.
     */
	ParamSet& set(const Properties& params);

	/**
     *  Set the list of params for this string all at once from and array of string pairs
	 *
     *  <p>
     *  @param values	the complete list of values to use in
     *  				building the computed string.  All previously
     * 					set values will be lost.
     */
	ParamSet& set(const std::vector<std::pair<std::string, std::string>>& params);
	/**
	 * Set the characters that are used to mark the parameters in the 
	 * format string.
	 * @param markers	A three character string that defines the markers used to identify
	 *		insertion points.  Where:
	 *
	 *		char0	Indicates the scan char.		Defaults to "%"
	 *		char1	Specifies the opening char.		Defaults to "("
	 *		char2	Specifies the closing char.		Defaults to ")"
	 *
     * Defaults to "%()"
	 */
	ParamSet& setMarkers(const std::string markers = "%()");

	/**
     * Set a named parameter from a string of format key=value.
     *
     * @param	namevalue	String.
     * @return current object.
     */
	ParamSet& set(const std::string& keyvalue);

	/**
     * Compute string using internal format string
     *
     * @return String.
     */
	std::string toString() const;

	/**
	 * Compute string using external format string
	 */
	std::string toString(const std::string format) const;

	/**
	 * Obtain const reference to the parameters configured for this ParamSet
	 */
	const Properties& params();

	/**
	 * Set the internal format string
	 */
	ParamSet& setFormat(const std::string& format);

private:
	/**
     * Compute the string by expanding parameters.
     *
     * @return String
     */
	std::string computeString(const std::string& format, char scanChar, char openChar, char closeChar) const;

	/**
     * Copy of format string.
     */
	std::string _format;

	/**
     * Hash table to contain all of the parameter values.
     */
	Properties _params;
	char _scanChar;
	char _openChar;
	char _closeChar;
};

} // namespace KEP