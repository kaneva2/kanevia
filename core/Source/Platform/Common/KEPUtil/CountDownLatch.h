///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <windows.h>
#include "Core/Util/ChainedException.h"
#include "Core/util/fast_mutex.h"

namespace KEP {

/**
 * Modeled after CountDownLatch in the Java 6 java.util.concurrent package
 *
 * For usage examples see CountDownLatchTests.h
 *
 * @see http://docs.oracle.com/javase/6/docs/api/java/util/concurrent/CountDownLatch.html
 * 
 */
class CountDownLatch {
public:
	CountDownLatch(unsigned long count = 1) :
			_count(count), _sync(), _signal(CreateEvent(0, true, false, 0)) {
		if (_signal != 0)
			return;
		// in error state, subsequent calls will raise an exception
	}

	/**
	 * Note that this is potentially unsafe and should be used with care.
	 * Make no attempt to synchronize access to source latch.
	 * Adds overhead of redundant sem opens.
	 */
	CountDownLatch(const CountDownLatch& other) :
			_count(other._count), _sync(), _signal(::CreateEvent(0, true, false, 0)) {
		if (_signal != 0)
			return;
		// in error state, subsequent calls will raise an exception
	}

	/**
	 * Note that this is potentially unsafe as it makes no attempt
	 * to determine if it's has any existing waiters that may be
	 * impacted by resetting the counter.
	 */
	CountDownLatch& operator=(const CountDownLatch& other) {
		_count = other._count;
		return *this;
	}

	/**
	 * dtor
	 */
	virtual ~CountDownLatch() {
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		if (_signal == 0)
			return;
		CloseHandle(_signal);
		_signal = 0;
	}

	/**
	 * Set the countdown amount.  Note that this method makes no attempt
	 * to synchronize or determine if a countdown is in progress.  It 
	 * is incumbent on the caller to insure that the countdown has not yet
	 * started.
	 */
	CountDownLatch& setCount(unsigned long count) {
		_count = count;
		return *this;
	}

	/**
	 * Decrements the counter.  When the counter reaches zero, any threads blocking on this latch
	 * use await() will unblock.
	 */
	unsigned long countDown() {
		if (_signal == 0)
			throw UninitializedException(TRACETHROW, SOURCELOCATION);
		if (_count == 0)
			return _count;
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		--_count;
		if (_count == 0)
			::SetEvent(_signal);
		return _count;
	}

	/**
	 * Wait indefinitely for this CountDownLatch to become unlatched.
	 */
	void await() {
		if (_signal == 0)
			throw UninitializedException(TRACETHROW, SOURCELOCATION);
		WaitForSingleObject(_signal, INFINITE);
	}

	/**
	 * Wait timeoutMillis for this CountDownLatch to become unlatched.
	 *
	 * @param timeoutMillis		Time in milliseconds to wait for this CountDownLatch to become unlatched.
	 *                          A value of 0 returns immediately with the latches state.
	 * @return				    Return the results of the wait, true if unlatched, else false.
	 */
	bool await(unsigned long timeoutMillis) {
		if (_signal == 0)
			throw UninitializedException(TRACETHROW, SOURCELOCATION);
		return WaitForSingleObject(_signal, timeoutMillis) == WAIT_OBJECT_0;
	}

	/**
	 * Return the current count.
	 */
	unsigned long count() {
		if (_signal == 0)
			throw UninitializedException(TRACETHROW, SOURCELOCATION);
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		return _count;
	}

	/**
	 * Obtain a string representation of this CountDownLatch
	 */
	std::string toString() {
		if (_signal == 0)
			throw UninitializedException(TRACETHROW, SOURCELOCATION);
		return "";
	}

private:
	fast_recursive_mutex _sync;
	unsigned long _count;
	HANDLE _signal;
};

} // namespace KEP