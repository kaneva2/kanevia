///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PluginHostConfigurator.h"

namespace KEP {

PluginHostConfigurator&
PluginHostConfigurator::configure(PluginHost& pluginHost, const TIXMLConfig::Item& cfg) {
	LOG4CPLUS_TRACE(log4cplus::Logger::getInstance(L"Plugin"), "PluginHostConfigurator::configure()");

	// Pass this to the configurator.
	//
	const TIXMLConfig::Item pluginHostConfig = cfg.firstChild(TAG_PLUGINHOST);

	// Guard Clause.  It's okay to not have a configuration.
	//
	if (pluginHostConfig == TIXMLConfig::nullItem()) {
		LOG4CPLUS_DEBUG(log4cplus::Logger::getInstance(L"STARTUP"), "PluginHostConfigurator::configure(): No configuration available!");
		return *this;
	}

	loadExternalPluginLibraries(pluginHost, pluginHostConfig.firstChild(TAG_LIBRARIES));
	cycleUp(pluginHost, pluginHostConfig.firstChild(TAG_PLUGINS));
	return *this;
}

PluginHostConfigurator&
PluginHostConfigurator::loadExternalPluginLibraries(PluginHost& pluginHost, const TIXMLConfig::Item& libraries) {
	// Guard Clause
	if (libraries == 0)
		return *this;

	// Load any external libraries.
	//
	for (TIXMLConfig::Item element = libraries.firstChild(TAG_LIBRARY); element != TIXMLConfig::nullItem(); element = element.nextSibling()) {
		std::string pcPluginLibraryPath = element.attribute(TAG_LIBRARY_PATH);
		if (pcPluginLibraryPath.length() == 0) {
			LOG4CPLUS_WARN(log4cplus::Logger::getInstance(L"STARTUP"), "PluginHostConfigurator::configure(): Missing library path specification!");
			continue;
		}

#if defined(_DEBUG)
		std::string strDebugLibraryPath;
		size_t len = pcPluginLibraryPath.size();
		if (len > 4) {
			strDebugLibraryPath = pcPluginLibraryPath.substr(0, len - 4) + "d" + pcPluginLibraryPath.substr(len - 4, 4);

			bool bFoundInCurrentDir = false;
			wchar_t awModuleFileName[MAX_PATH];
			if (GetModuleFileName(NULL, awModuleFileName, _countof(awModuleFileName))) {
				std::wstring str(awModuleFileName);
				size_t pos = str.find_last_of(L'\\', std::wstring::npos);
				if (pos != std::wstring::npos) {
					str.erase(str.begin() + (pos + 1), str.end());
					str += Utf8ToUtf16(strDebugLibraryPath);
					if (PathFileExists(str.c_str())) {
						bFoundInCurrentDir = true;
						strDebugLibraryPath = Utf16ToUtf8(str);
						pcPluginLibraryPath = strDebugLibraryPath;
					}
				}
			}
			if (!bFoundInCurrentDir && PathFileExists(Utf8ToUtf16(strDebugLibraryPath).c_str()))
				pcPluginLibraryPath = strDebugLibraryPath;
		}
#endif

		try {
			pluginHost.loadPluginLibrary(pcPluginLibraryPath);
		} catch (const PluginException& pe) {
			LOG4CPLUS_WARN(log4cplus::Logger::getInstance(L"STARTUP"), "PluginHostConfigurator::configure(): Failed to load plugin library [" << pcPluginLibraryPath << "] reason: " << pe.str());
		}
	} // Load libraries
	return *this;
}

PluginHostConfigurator&
PluginHostConfigurator::cycleUp(PluginHost& pluginHost, const TIXMLConfig::Item& plugins) {
	// Guard clause
	if (plugins == TIXMLConfig::nullItem())
		return *this;

	ParamSet& paramSet = plugins.document().paramSet();
	for (TIXMLConfig::Item element = plugins.firstChild(TAG_PLUGIN); element != TIXMLConfig::nullItem(); element = element.nextSibling()) {
		std::string pcPluginFactoryName = element.attribute(TAG_FACTORY_NAME);
		if (pcPluginFactoryName.length() == 0) {
			LOG4CPLUS_WARN(log4cplus::Logger::getInstance(L"STARTUP"), "PluginHostConfigurator::configure(): Missing factory specification!");
			continue;
		}

		// default the instance name to the factory name
		std::string pcPluginInstanceName = element.attribute(TAG_INSTANCE_NAME);
		if (pcPluginInstanceName.length() == 0)
			pcPluginInstanceName = pcPluginFactoryName;

		try {
			std::string pcPluginDisabled = element.attribute(TAG_PLUGIN_DISABLED);
			// Configuration doesn't, but should, apply logicals to attributes.  Until this is
			// properly resolved, apply it explicitly
			//
			if (pcPluginDisabled.length() != 0) {
				std::string pluginDisabled = paramSet.toString(pcPluginDisabled);

				if (pluginDisabled == "true") {
					LOG4CPLUS_WARN(log4cplus::Logger::getInstance(L"STARTUP"), "PluginHostConfigurator::configure(): Plugin " << pcPluginInstanceName << " is disabled");
					continue;
				}
			}

			const TIXMLConfig::Item configNode = element.firstChild(TAG_PLUGIN_CONFIG); //->ToElement();
			LOG4CPLUS_DEBUG(log4cplus::Logger::getInstance(L"STARTUP"), "PluginHostConfigurator::configure(): Mounting plugin. Factory[" << pcPluginFactoryName.c_str() << "] instance name[" << pcPluginInstanceName.c_str() << "]");
			pluginHost.mountPlugin(pcPluginFactoryName // This is where I get uncomfortable with this.
				,
				pcPluginInstanceName // Binding the plugin class to a third party
				,
				&configNode); // class.  But this is preferable to being bound
			// to KEPConfig.
			LOG4CPLUS_DEBUG(log4cplus::Logger::getInstance(L"STARTUP"), "PluginHostConfigurator::configure(): Setting up plugin. Factory[" << pcPluginFactoryName.c_str() << "] instance name[" << pcPluginInstanceName.c_str() << "]");
			pluginHost.setUpPlugin(pcPluginFactoryName);

			LOG4CPLUS_DEBUG(log4cplus::Logger::getInstance(L"STARTUP"), "PluginHostConfigurator::configure(): Starting plugin.  Factory[" << pcPluginFactoryName.c_str() << "] instance name[" << pcPluginInstanceName.c_str() << "]");
			pluginHost.startPlugin(pcPluginFactoryName);
		} catch (const ChainedException ce) {
			std::stringstream ss;
			ss << "Exception raised during cycle up for plugin [" << pcPluginInstanceName << "]\n";
			throw ChainedException(SOURCELOCATION, ss.str()).chain(ce);
		}
	}
	return *this;
}

PluginHostConfigurator& PluginHostConfigurator::configure(PluginHost& pluginHost, const TIXMLConfig& config) {
	return configure(pluginHost, config.root());
}

} // namespace KEP