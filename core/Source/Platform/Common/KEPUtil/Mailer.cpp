///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <sstream>
#include "Core/Util/ErrorSpec.h"
#include "Common\KEPUtil\SocketSubsystem.h"
#include "mailer.h"
#include "wsaerrors.h"

using namespace log4cplus;
// todo: move me
// At some point will likely end up implementing a wrapper for socket resources.
// move this define there.
//
#define ERR_SIZE 512

namespace KEP {

Logger mailLogger = Logger::getInstance(L"Mailer");

static SOCKET connectServerSocket(const std::string& server, const unsigned short port) {
	int nConnect;
	short nProtocolPort;
	LPHOSTENT lpHostEnt = 0;
	LPSERVENT lpServEnt;
	SOCKADDR_IN sockAddr;
	SOCKET hServerSocket = INVALID_SOCKET;

	lpHostEnt = gethostbyname(server.c_str());

	if (!lpHostEnt) {
		std::stringstream msg;
		msg << "Invalid host name [" << server << "]";
		// Get rid of this catchall user of id "1"
		//
		throw MailException(SOURCELOCATION, ErrorSpec(1, msg.str()));
	}

	hServerSocket = socket(PF_INET, SOCK_STREAM, DEFAULT_PROTOCOL);
	if (hServerSocket == INVALID_SOCKET)
		throw MailException(SOURCELOCATION, getError());

	int timeout = 250;
	setsockopt(hServerSocket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof(int));

	if (port != NULL) {
		nProtocolPort = port;
	} else {
		lpServEnt = getservbyname("mail", DEFAULT_PROTOCOL);
		if (lpServEnt == NULL)
			nProtocolPort = htons(IPPORT_SMTP);
		else
			nProtocolPort = lpServEnt->s_port;
	}

	sockAddr.sin_family = AF_INET;
	sockAddr.sin_port = nProtocolPort;
	sockAddr.sin_addr = *((LPIN_ADDR)*lpHostEnt->h_addr_list);
	nConnect = connect(hServerSocket, (PSOCKADDR)&sockAddr, sizeof(sockAddr)); // returns 0 on success
	if (nConnect == SOCKET_ERROR) {
		// The fact that nConnect represents a bad socket, it doesn't necessarily mean that GetLastError() is going to
		//
		ErrorSpec es = getError();
		if (es.error())
			throw MailException(SOURCELOCATION, es);
		else {
			std::stringstream ss;
			ss << "Connection to server [" << server << ":" << nProtocolPort << "1] failed.";
			throw MailException(SOURCELOCATION, ErrorSpec(ULONG_MAX, ss.str()));
		}
	}
	LOG4CPLUS_TRACE(mailLogger, "Mailer: Successfully connected to mail server [" << server << ":" << ntohs(nProtocolPort) << "]");
	return (hServerSocket);
}

class MailerInternalsT {
private:
	typedef LightWeightMailer::RecipientsVector RecipientsVector;
	typedef LightWeightMailer::MAIL_ERRORS MailErrors;

public:
	MailerInternalsT() :
			hSocket(INVALID_SOCKET) {}
	MailerInternalsT(const MailerInternalsT& source) { hSocket = source.hSocket; }
	virtual ~MailerInternalsT() {
		if (connected())
			disconnect();
	}
	MailerInternalsT& operator=(const MailerInternalsT& rhs) {
		hSocket = rhs.hSocket;
		return *this;
	}

	bool connected() { return hSocket != INVALID_SOCKET; }
	ErrorSpec connectToServer(const std::string& localHostName, const std::string& server, const unsigned short port = NULL);
	ErrorSpec disconnect();
	std::string response();

	// todo: change to errorspec
	bool sendRecipients(const LightWeightMailer::RecipientsVector& recipients);
	std::string recipientsString(const RecipientsVector& recipients);

	std::string formatHeader(const std::string& senderName, const RecipientsVector& recipients, const RecipientsVector& ccRecipients, const RecipientsVector& bccRecipients, const std::string subject, const std::string xMailer, const std::string replyTo);

	/**
	 * Transmit the configured EMail.
	 * @returns		ErrorSpec.  first == 0 if no error, else first != 0
	 */
	ErrorSpec transmit(const std::string& fromEmail, const std::string& fromName, const LightWeightMailer::RecipientsVector& recipients, const LightWeightMailer::RecipientsVector& ccRecipients, const LightWeightMailer::RecipientsVector& bccRecipients, const std::string& subject, const std::string& body, const std::string& xMailer, const std::string& replyTo);

private:
	SOCKET hSocket;
};

std::string MailerInternalsT::recipientsString(const RecipientsVector& recipients) {
	char* delim = "";
	std::stringstream recipientsStream;
	for (RecipientsVector::const_iterator recipient = recipients.begin(); recipient != recipients.end(); recipient++) {
		recipientsStream << delim << recipient->email();
		delim = ",";
	}
	return recipientsStream.str();
}

std::string MailerInternalsT::formatHeader(const std::string& senderName, const RecipientsVector& recipients, const RecipientsVector& ccRecipients, const RecipientsVector& bccRecipients, const std::string subject, const std::string xMailer, const std::string replyTo) {
	// Guard clause.
	// check for at least one recipient
	if (recipients.size() <= 0)
		return NULL;

	std::stringstream msgHeader;

	char szDate[500];
	char sztTime[500];

	// create the recipient string, cc string, and bcc string
	std::string to = recipientsString(recipients);
	std::string cc = recipientsString(ccRecipients);
	std::string bcc = recipientsString(bccRecipients);

	// get the current date and time
	SYSTEMTIME st = { 0 };
	::GetSystemTime(&st);
	// Use English/US locale for date/time in message header.
	LCID localeID = MAKELCID(MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), SORT_DEFAULT);
	::GetDateFormatA(localeID, 0, &st, "ddd',' dd MMM yyyy", szDate, sizeof(szDate));
	::GetTimeFormatA(localeID, TIME_FORCE24HOURFORMAT, &st, "HH':'mm':'ss tt", sztTime, sizeof(sztTime));

	// here it is...the main data of the message
	// _wsprintf(msgHeader,"DATE: %s %s\r\n", szDate, sztTime);
	msgHeader << "DATE: " << szDate << " " << sztTime << "\r\n";

	if (!senderName.empty())
		msgHeader << "FROM: " << senderName << "\r\n";

	msgHeader << "To: " << to << "\r\n"
			  << "Cc: " << cc << "\r\n";

	if (!subject.empty())
		msgHeader << "Subject: " << subject << "\r\n";
	if (!xMailer.empty())
		msgHeader << "X-Mailer: " << xMailer << "\r\n";
	if (!replyTo.empty())
		msgHeader << "Reply-To: " << replyTo << "\r\n";

	// start MIME versions
	msgHeader << "MIME-Version: 1.0\r\nContent-type: text/plain; charset=US-ASCII\r\n";

	// send header finish command
	msgHeader << "\r\n";

	// done
	return msgHeader.str();
}

bool MailerInternalsT::sendRecipients(const LightWeightMailer::RecipientsVector& recipients) {
	std::stringstream ss;

	for (LightWeightMailer::RecipientsVector::const_iterator i = recipients.begin(); i != recipients.end(); i++) {
		ss << "RCPT TO: " << i->email() << "\r\n";

		std::string s = ss.str();
		// std::cout << ">> " << s;
		if (::send(hSocket, s.c_str(), s.length(), NO_FLAGS) == SOCKET_ERROR) {
			// printf("Socket send error: %d\r\n", WSAGetLastError());
			return false;
		}

		std::string r = response();
		LOG4CPLUS_TRACE(mailLogger, "Mailer: Transmitted RECIPIENT with response [" << StringHelpers::trim(r) << "]");
	}
	return true;
}

std::string MailerInternalsT::response() {
	BYTE sReceiveBuffer[4096];
	int iLength = 0;
	int iEnd = 0;
	iLength = recv(hSocket, (LPSTR)sReceiveBuffer + iEnd, sizeof(sReceiveBuffer) - iEnd, NO_FLAGS);
	if (iLength == SOCKET_ERROR) {
		return "";
	}
	iEnd += iLength;
	sReceiveBuffer[iEnd] = '\0';
	std::stringstream ss;
	ss << sReceiveBuffer;
	return ss.str();
}

ErrorSpec
MailerInternalsT::connectToServer(const std::string& localHostName, const std::string& server, const unsigned short port) {
	if (connected())
		disconnect(); // disconnect from any existing connection

	try {
		hSocket = connectServerSocket(server, port);
		if (hSocket == INVALID_SOCKET) {
			ErrorSpec es(2, "Unexpected error! Invalid Socket!");
			LOG4CPLUS_TRACE(mailLogger, es.toString());
			return es;
		}

		std::string r = response();
		LOG4CPLUS_TRACE(mailLogger, "Mailer: Connection response [" << StringHelpers::trim(r) << "]");
		std::stringstream buf;
		buf << "HELO " << localHostName << "\r\n";
		if (::send(hSocket, buf.str().c_str(), buf.str().length(), NO_FLAGS) == SOCKET_ERROR) {
			return getError();
		}
		r = response();
		LOG4CPLUS_TRACE(mailLogger, "Mailer: Transmitted handshake [" << StringHelpers::trim(r) << "]");
	} catch (CONST MailException& me) {
		return me.errorSpec();
	}
	return ErrorSpec(0, "");
}

ErrorSpec MailerInternalsT::transmit(const std::string& fromEmail, const std::string& fromName, const LightWeightMailer::RecipientsVector& recipients, const LightWeightMailer::RecipientsVector& ccRecipients, const LightWeightMailer::RecipientsVector& bccRecipients, const std::string& subject, const std::string& body, const std::string& xMailer, const std::string& replyTo) {
	// verify sender email
	// If no from or to.  bug out with a "no address" type of message
	//
	if (fromEmail.length() == 0)
		return ErrorSpec(LightWeightMailer::errorNoSender, LightWeightMailer::errorString(LightWeightMailer::errorNoSender));
	if (recipients.size() == 0)
		return ErrorSpec(LightWeightMailer::errorNoRecipient, LightWeightMailer::errorString(LightWeightMailer::errorNoRecipient));

	std::stringstream buf;
	// get proper header
	std::string msgHeader(formatHeader(fromName, recipients, ccRecipients, bccRecipients, subject, xMailer, replyTo));

	if (msgHeader.empty())
		return getError();

	// start
	buf << "MAIL FROM: <" << fromEmail << ">\r\n";
	std::string mfrom(buf.str());
	if (::send(hSocket, mfrom.c_str(), mfrom.length(), NO_FLAGS) == SOCKET_ERROR) {
		printf("Socket send error: %d\r\n", WSAGetLastError());
		return getError();
	}
	std::string r = response();
	LOG4CPLUS_TRACE(mailLogger, "Mailer: Transmitted SENDER with response [" << StringHelpers::trim(r) << "]");

	// create message receipts
	sendRecipients(recipients);
	sendRecipients(ccRecipients);
	sendRecipients(bccRecipients);

	// init data
	if (::send(hSocket, "DATA\r\n", strlen("DATA\r\n"), NO_FLAGS) == SOCKET_ERROR)
		return getError();

	// send header
	if (::send(hSocket, msgHeader.c_str(), msgHeader.length(), NO_FLAGS) == SOCKET_ERROR)
		return getError();

	// send body
	if (::send(hSocket, body.c_str(), body.length(), NO_FLAGS) == SOCKET_ERROR) {
		return getError();
	}

	::send(hSocket, (LPSTR) "\r\n.\r\n", strlen("\r\n.\r\n"), NO_FLAGS);
	ErrorSpec es = getError();
	r = response();
	LOG4CPLUS_TRACE(mailLogger, "Mailer: Transmitted BODY with reponse [" << StringHelpers::trim(r) << "]");
	return es;
}

ErrorSpec MailerInternalsT::disconnect() {
	if (!connected())
		return ErrorSpec(WSAENOTCONN, "Mailer not connected!");

	if (::send(hSocket, (LPSTR) "QUIT\r\n", strlen("QUIT\r\n"), NO_FLAGS) == SOCKET_ERROR)
		return getError();

	hSocket = INVALID_SOCKET;
	return ErrorSpec(0, "");
}

LightWeightMailer::LightWeightMailer() :
		_fromEmail(), _fromName(), _subject(), _msgBody(), _xMailer(), _replyTo(), _ipAddr(), _hiddenInternals(new MailerInternalsT()) {
}

std::string LightWeightMailer::errorString(int ErrorCode, HINSTANCE hInst) {
	int err_len = 0;
	HWND hwnd;
	wchar_t acBuf[512];

	if (!hInst) {
		hwnd = GetActiveWindow();
#ifndef WIN32
		hInst = GetWindowWord(hwnd, GWW_HINSTANCE);
#else
		hInst = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
#ifdef _DEBUG
		hInst = GetModuleHandleW(L"keputild.dll");
#else
		hInst = GetModuleHandleW(L"keputil.dll");
#endif
#endif
	}
	err_len = LoadStringW(hInst, ErrorCode, acBuf, ERR_SIZE / 2);
	return Utf16ToUtf8(acBuf);
}

LightWeightMailer::LightWeightMailer(const LightWeightMailer& source) :
		_fromEmail(source._fromEmail), _fromName(source._fromName), _subject(source._subject), _msgBody(source._msgBody), _xMailer(source._xMailer), _replyTo(source._replyTo), _ipAddr(source._ipAddr), _hiddenInternals(new MailerInternalsT(*(source._hiddenInternals))), _localHostName(source._localHostName), _mailHostName(source._mailHostName), _recipients(source._recipients), _ccRecipients(source._ccRecipients), _bccRecipients(source._bccRecipients) {
}

LightWeightMailer::~LightWeightMailer() {
	delete _hiddenInternals;
}

LightWeightMailer& LightWeightMailer::operator=(const LightWeightMailer& source) {
	_fromEmail = source._fromEmail;
	_fromName = source._fromName;
	_subject = source._subject;
	_msgBody = source._msgBody;
	_xMailer = source._xMailer;
	_replyTo = source._replyTo;
	_ipAddr = source._ipAddr;
	_localHostName = source._localHostName;
	_mailHostName = source._mailHostName;
	_recipients = source._recipients;
	_ccRecipients = source._ccRecipients;
	_bccRecipients = source._bccRecipients;
	(*_hiddenInternals) = *(source._hiddenInternals);
	return *this;
}

std::string formatAddress(const std::string& name, const std::string& email) {
	std::stringstream ss;
	ss << name << "<" << email << ">";
	return ss.str();
}

LightWeightMailer& LightWeightMailer::addRecipient(const std::string& email, const std::string& name) {
	if (!email.empty())
		_recipients.push_back(MailRecipient(formatAddress(name, email)));
	return *this;
}

LightWeightMailer& LightWeightMailer::addCCRecipient(const std::string& email, const std::string& name) {
	if (!email.empty())
		_ccRecipients.push_back(MailRecipient(formatAddress(name, email)));
	return *this;
}

LightWeightMailer& LightWeightMailer::addBCCRecipient(const std::string& email, const std::string& name) {
	if (!email.empty())
		_bccRecipients.push_back(MailRecipient(formatAddress(name, email)));
	return *this;
}

ErrorSpec LightWeightMailer::send(const std::string& mailHost, const std::string& fromAddress, const std::string& toAddress, const std::string& subject, const std::string& body) {
	if (fromAddress.length() == 0)
		return ErrorSpec(errorNoSender, errorString(errorNoSender));
	if (toAddress.length() == 0)
		return ErrorSpec(errorNoRecipient, errorString(errorNoRecipient));

	return LightWeightMailer::create(mailHost, fromAddress, toAddress, subject, body)
		.send();
}

LightWeightMailer LightWeightMailer::create(const std::string& mailHost, const std::string& fromAddress, const std::string& toAddress, const std::string& subject, const std::string& body) {
	return LightWeightMailer().setMailHost(mailHost).setSenderEmail(fromAddress).addRecipient(toAddress).setSubject(subject).setMessageBody(body);
}

class RunnableMail : public Runnable {
public:
	RunnableMail(LightWeightMailer mailer) :
			_mailer(mailer) {}
	RunResultPtr run() override {
		ErrorSpec es = _mailer.send();
		LOG4CPLUS_TRACE(Logger::getInstance(L"Mailer"), "Mailer: Transmission complete. " << es.toString());
		return RunResultPtr(new RunResult(es.first));
	}

private:
	LightWeightMailer _mailer;
};

RunnablePtr LightWeightMailer::createRunnable(const std::string& mailHost, const std::string& fromAddress, const std::string& toAddress, const std::string& subject, const std::string& body) {
	RunnableMail* pRunnable = new RunnableMail(create(mailHost, fromAddress, toAddress, subject, body));
	return RunnablePtr(pRunnable);
}

std::string LightWeightMailer::str() {
	std::stringstream ss;
	ss << "from=\"" << this->_fromEmail << "\";"
	   << "to=\"" << toString(this->_recipients) << "\";"
	   << "cc=\"" << toString(this->_ccRecipients) << "\";"
	   << "bcc=\"" << toString(this->_bccRecipients) << "\";"
	   << "mailhost=\"" << this->_mailHostName << "\";";
	return ss.str();
}

std::string LightWeightMailer::toString(const RecipientsVector& recipients) {
	bool delim = false;
	std::stringstream ss;
	for (RecipientsVector::const_iterator iter = recipients.begin(); iter != recipients.end(); iter++) {
		ss << (delim ? "," : "") << iter->email();
		delim = true;
	}
	return ss.str();
}

ErrorSpec LightWeightMailer::send() {
	return send(_subject, _msgBody);
}

ErrorSpec LightWeightMailer::send(const std::string& subject, const std::string& body) {
	SocketSubSystem ss;
	LOG4CPLUS_TRACE(Logger::getInstance(L"Mailer"), "LightWeightMailer::send()");
	if (_fromEmail.length() == 0) {
		LOG4CPLUS_ERROR(Logger::getInstance(L"Mailer"), "LightWeightMailer::send(): No sender address specifiied!");
		return ErrorSpec(errorNoSender, errorString(errorNoSender));
	}

	if (_recipients.size() == 0) {
		LOG4CPLUS_ERROR(Logger::getInstance(L"Mailer"), "LightWeightMailer::send(): No recipient addressess specifiied!");
		return ErrorSpec(errorNoRecipient, errorString(errorNoSender));
	}

	ErrorSpec results = _hiddenInternals->connectToServer(ss.hostName(), _mailHostName);
	if (results.error()) {
		LOG4CPLUS_ERROR(Logger::getInstance(L"Mailer"), "LightWeightMailer::send(): Failed to connect to mail host at [" << _mailHostName << "]");
		return results;
	}
	if (!_hiddenInternals->connected()) {
		LOG4CPLUS_ERROR(Logger::getInstance(L"Mailer"), "LightWeightMailer::send(): Not connected!");
		return ErrorSpec(IDS_WSAENOTCONN, WSAErrStr(IDS_WSAENOTCONN));
	}

	ErrorSpec transmitError = _hiddenInternals->transmit(_fromEmail, _fromName, _recipients, _ccRecipients, _bccRecipients, subject, body, _xMailer, _replyTo);
	LOG4CPLUS_DEBUG(Logger::getInstance(L"Mailer"), "LightWeightMailer::send(): Transmission completed with results [" << transmitError.toString() << "]");
	_hiddenInternals->disconnect();
	LOG4CPLUS_DEBUG(Logger::getInstance(L"Mailer"), "LightWeightMailer::send(): disconnected!");
	return transmitError;
}

const std::string& LightWeightMailer::localHostIp() {
	in_addr* iaHost;

	char acHostName[255];
	if (gethostname(acHostName, 255) == SOCKET_ERROR) {
		/*DWORD dErr =*/WSAGetLastError(); // printf("Failed to get the local ip address\r\n");
		return _ipAddr;
	}

	const std::string t1(acHostName);
	_localHostName = t1;
	HOSTENT* pHe = NULL;
	pHe = gethostbyname(acHostName);
	if (pHe == NULL)
		return _ipAddr;

	for (int i = 0; pHe->h_addr_list[i] != 0; i++) {
		iaHost = (LPIN_ADDR)pHe->h_addr_list[i];
		const std::string t2(inet_ntoa(*iaHost));
		_ipAddr = t2;
	}
	return _ipAddr;
}

unsigned const int LightWeightMailer::bccRecipientCount() {
	return _bccRecipients.size();
}
unsigned const int LightWeightMailer::ccRecipientCount() {
	return _ccRecipients.size();
}
unsigned const int LightWeightMailer::recipientCount() {
	return _recipients.size();
}
const std::string& LightWeightMailer::messageBody() const {
	return _msgBody;
}
const std::string& LightWeightMailer::replyTo() const {
	return _replyTo;
}
const std::string& LightWeightMailer::senderEmail() const {
	return _fromEmail;
}
const std::string& LightWeightMailer::senderName() const {
	return _fromName;
}
const std::string& LightWeightMailer::subject() const {
	return _subject;
}
const std::string& LightWeightMailer::xMailer() const {
	return _xMailer;
}
LightWeightMailer& LightWeightMailer::setMessageBody(const std::string& body) {
	_msgBody = body;
	return *this;
}
LightWeightMailer& LightWeightMailer::setReplyTo(const std::string& replyto) {
	_replyTo = replyto;
	return *this;
}
LightWeightMailer& LightWeightMailer::setSenderEmail(const std::string& email) {
	_fromEmail = email;
	return *this;
}
LightWeightMailer& LightWeightMailer::setSenderName(const std::string& name) {
	_fromName = name;
	return *this;
}
LightWeightMailer& LightWeightMailer::setSubject(const std::string& subject) {
	_subject = subject;
	return *this;
}
LightWeightMailer& LightWeightMailer::setXMailer(const std::string& xmailer) {
	_xMailer = xmailer;
	return *this;
}

/**
 * A "runnable" implementation of Mailer
 */
class SendAgent : public Runnable {
private:
	LightWeightMailer _mailer;

public:
	SendAgent(LightWeightMailer mailer) :
			_mailer(mailer) {}

	RunResultPtr run() override {
		_mailer.send();
		return RunResultPtr();
	}
};

const LightWeightMailer::RecipientsVector& LightWeightMailer::recipients() {
	return this->_recipients;
}
const LightWeightMailer::RecipientsVector& LightWeightMailer::ccRecipients() {
	return this->_ccRecipients;
}
const LightWeightMailer::RecipientsVector& LightWeightMailer::bccRecipients() {
	return this->_bccRecipients;
}

} // namespace KEP