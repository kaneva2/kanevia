///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Helpers.h"

namespace KEP {

// Verbose Enumeration
enum {
	WC_VERBOSE_NONE = 0x00, /// only warnings and errors
	WC_VERBOSE_STATUS = 0x01, /// add status variables
	WC_VERBOSE_URL = 0x02, /// add url string
	WC_VERBOSE_RESPONSE = 0x04, /// add response string
	WC_VERBOSE_FULL = 0xff /// add everything
};

// GetBrowserPage instance
#define WEB_CALLER_GET_BROWSER_PAGE "GetBrowserPage"

/// Default Web Caller Parameters
#ifndef WEB_CALLER_ENABLED
#define WEB_CALLER_ENABLED true
#endif
#ifndef WEB_CALLER_DISABLE_TIMEOUT_MS
#define WEB_CALLER_DISABLE_TIMEOUT_MS (30 * MS_PER_SEC)
#endif
#ifndef WEB_CALLER_WAIT_TIMEOUT_MS
#define WEB_CALLER_WAIT_TIMEOUT_MS (30 * MS_PER_SEC)
#endif
#ifndef WEB_CALLER_THREADS
#define WEB_CALLER_THREADS 1
#endif
#ifndef WEB_CALLER_PRIORITY
#define WEB_CALLER_PRIORITY THREAD_PRIORITY_NORMAL
#endif
#ifndef WEB_CALLER_VERBOSE
#define WEB_CALLER_VERBOSE (WC_VERBOSE_STATUS | WC_VERBOSE_URL)
#endif

/// Default Web Call Options
#ifndef WEB_CALL_OPT_SECURE
#define WEB_CALL_OPT_SECURE false
#endif
#ifndef WEB_CALL_OPT_RESPONSE
#define WEB_CALL_OPT_RESPONSE false
#endif
#ifndef WEB_CALL_OPT_VERBOSE
#define WEB_CALL_OPT_VERBOSE WEB_CALLER_VERBOSE
#endif
#ifndef WEB_CALL_OPT_TIMEOUT_MS
#define WEB_CALL_OPT_TIMEOUT_MS (30 * MS_PER_SEC)
#endif
#ifndef WEB_CALL_OPT_URL_ENCODE
#define WEB_CALL_OPT_URL_ENCODE true
#endif
#ifndef WEB_CALL_OPT_CB_FUNC
#define WEB_CALL_OPT_CB_FUNC nullptr
#endif
#ifndef WEB_CALL_OPT_CB_PRIORITY
#define WEB_CALL_OPT_CB_PRIORITY THREAD_PRIORITY_NORMAL
#endif
#ifndef WEB_CALL_OPT_GZIP
#define WEB_CALL_OPT_GZIP true
#endif

#define HTTP_STATUS_KANEVA_AUTH 490 // Kaneva needs authenticated

inline std::string UrlSeparator(const std::string& url) {
	return (STLContains(url, "?") ? "&" : "?");
}

#define UrlAddParam(url, key, val) StrAppend(url, UrlSeparator(url) << (key) << "=" << (val));

/// Mutex Declarations
using wcMutex = fast_recursive_mutex;
using wcMutexGuardLock = std::unique_lock<wcMutex>;

/// Web Call Callback
class WebCallAct;
typedef std::function<void(WebCallAct* pWCA)> WebCallCallbackFunc;

/// Web Call Options
struct /*KEPUTIL_EXPORT*/ WebCallOpt {
	std::string url; ///< web call url string
	bool response; ///< web call requires response
	WebCallCallbackFunc cbFunc; ///< web call callback function
	TimeMs timeoutMs; ///< web call timeout (ms)
	bool urlEncode; ///< web call url needs encoded
	size_t verbose; ///< web call log verbosity
	bool gzip; ///< web call gzip compression
	bool secure; ///< web call url use secure (https)
	std::string responseFilePath; ///< optional file path for response
	void* pAddData; ///< optional user data pointer

	WebCallOpt(
		const std::string& url = "",
		bool response = WEB_CALL_OPT_RESPONSE,
		WebCallCallbackFunc cbFunc = WEB_CALL_OPT_CB_FUNC,
		TimeMs timeoutMs = WEB_CALL_OPT_TIMEOUT_MS,
		bool urlEncode = WEB_CALL_OPT_URL_ENCODE,
		size_t verbose = WEB_CALL_OPT_VERBOSE,
		bool gzip = WEB_CALL_OPT_GZIP,
		bool secure = WEB_CALL_OPT_SECURE) :
			url(url),
			response(response),
			cbFunc(cbFunc),
			timeoutMs(timeoutMs),
			urlEncode(urlEncode),
			verbose(verbose),
			gzip(gzip),
			secure(secure),
			pAddData(NULL) {}

	virtual ~WebCallOpt() {}

	// DRF - Gets gzip flag from url parameter and strips url parameter.
	bool UseParamGzip() {
		if (STLContainsIgnoreCase(url, "gzip=true")) {
			gzip = true;
			STLStringSubstitute(url, "?gzip=true&", "?");
			STLStringSubstitute(url, "?gzip=true", "");
			STLStringSubstitute(url, "&gzip=true", "");
		}
		return gzip;
	}

	// DRF - Adds parameter to url using proper separator.
	void AddParam(const std::string& key, const std::string& val) {
		UrlAddParam(url, key, UrlEncode(val, false));
	}
	void AddParam(const std::string& key, int val) {
		UrlAddParam(url, key, val);
	}
};

/// Web Call ACT (Asyncronous Completion Token) Interface
class KEPUTIL_EXPORT WebCallAct {
	// Do Not Allow Copy!
	WebCallAct(const WebCallAct& wca);
	void operator=(const WebCallAct& wca);

public:
	/// Web Call State Enumeration
	enum WebCallState {
		STATE_IDLE, /// no url defined or waiting for thread to open url
		STATE_FILE_ERROR, /// response file error, web call complete
		STATE_URL_OPENING, /// url opening, waiting on server ok
		STATE_URL_ERROR, /// server responded url error, web call complete
		STATE_URL_OK, /// server responded url ok, if mode RESPONSE_NO web call complete
		STATE_RESPONSE_READING, /// reading server response html one line at a time
		STATE_RESPONSE_ERROR, /// server response html error, web call complete
		STATE_RESPONSE_OK, /// server response html ok, web call complete
		STATES
	};

	WebCallAct(const WebCallOpt& wco);

	virtual ~WebCallAct();

	/**
	* Returns web call entry as string formatted according to wco.verbose setting.
	*/
	std::string ToStr() const;

	/**
	* Logs web call entry formatted according to wco.verbose setting.
	*/
	void Log(const std::string& funcStr);

	/**
	* Returns web call state as string.
	*/
	const char* StateStr() const {
		static const char* stateStrs[STATES] = {
			"IDLE",
			"FILE_ERROR",
			"URL_OPENING",
			"URL_ERROR",
			"URL_OK",
			"RESPONSE_READING",
			"RESPONSE_ERROR",
			"RESPONSE_OK"
		};
		return stateStrs[state];
	}

	/** 
	* Returns true if web call state is idle, in queue and awaiting processing.
	*/
	bool IsIdle() const {
		return (state == STATE_IDLE);
	}

	/** 
	* Returns true if web call url is errored or does not exist.
	*/
	bool IsUrlError() const {
		return (state == STATE_URL_ERROR);
	}

	/** 
	* Returns true if web call response was not requested and the url exists -or-
	* response was requested and the url exists but the HTTP response has not yet been received.
	*/
	bool IsUrlOk() const {
		return (state >= STATE_URL_OK);
	}

	/** 
	* Returns true if web call HTTP response is errored or the response data is invalid.
	*/
	bool IsResponseError() const {
		return (state == STATE_RESPONSE_ERROR);
	}

	/** 
	* Returns true if web call HTTP response is ok and the response data is valid.
	*/
	bool IsResponseOk() const {
		return (state >= STATE_RESPONSE_OK);
	}

	/** 
	* Returns true if web call response was not requested and the url exists -or-
	* response was requested and the HTTP response is ok and the response data is valid.
	*/
	bool IsOk() const {
		return (wco.response ? IsResponseOk() : IsUrlOk());
	}

	/** 
	* Returns true if web call is complete.
	*/
	bool IsComplete() const {
		return m_bComplete.load(std::memory_order_relaxed);
	}

	void SetComplete();

	/**
	* Returns web call HTTP status code (eg. [200], [404], ...).
	*/
	DWORD StatusCode() const {
		return statusCode;
	}

	/**
	* Returns web call HTTP status string .
	*/
	std::string StatusStr() const {
		return statusStr;
	}

	/**
	* Returns true if web call HTTP status is ok ([200]).
	*/
	bool IsStatusOk() const {
		return (statusCode == HTTP_STATUS_OK);
	}

	/**
	* Returns web call result windows error code.
	*/
	DWORD ErrorCode() const {
		return errCode;
	}

	/**
	* Returns web call result windows error string.
	*/
	std::string ErrorStr() const {
		return errStr;
	}

	/**
	* Returns true if web call result is errored.
	*/
	bool IsError() const {
		return (errCode != 0);
	}

	/**
	* Returns web call time in milliseconds from construction through call completion.
	*/
	TimeMs CallTimeMs() const {
		return callTimer.ElapsedMs();
	}

	/**
	* Returns web call time as a string from construction through call completion.
	*/
	std::string CallTimeStr() const;

	/**
	* Returns web call response time in milliseconds from call through call completion.
	*/
	TimeMs ResponseTimeMs() const {
		return responseTimer.ElapsedMs();
	}

	/**
	* Returns true if web call response time exceeds the web call option timeout.
	*/
	bool IsSlow() const {
		return (ResponseTimeMs() > wco.timeoutMs);
	}

	/**
	* Returns true if web call timeout time exceeds the web call option timeout.
	*/
	bool IsTimeout() const {
		return ErrorCode() == ERROR_INTERNET_TIMEOUT;
	}

	/**
	* Returns web call response size in bytes.
	*/
	size_t ResponseSize() const {
		return responseSize;
	}

	/**
	* Returns web call response character buffer.
	* WARNING - Buffer is not necessarily NULL terminated!
	*/
	const char* ResponseChars() const {
		return ResponseSize() ? responseChars.data() : "";
	}

	/**
	* Returns web call response string.
	*/
	std::string ResponseStr() const {
		return std::string(responseChars.begin(), responseChars.end());
	}

	/**
	* Blocking web call until completion. Returns true on success or false on error or timeout.
	*/
	bool Call();

	/**
	* Blocking wait for async web call completion. Returns true on success or false on error or timeout.
	*/
	bool Wait() const;

public:
	/// Web Call Options
	WebCallOpt wco; ///< web call options

	/// Web Call State
	WebCallState state; ///< enumerated web call state

	/// Web Call Complete Flag (either error or url/response ok)
	std::atomic<bool> m_bComplete; ///< web call complete flag

	/// Web Call Http Status Information
	DWORD statusCode; ///< http status code (eg. 200)
	std::string statusStr; ///< http status string (eg. 'HTTP_STATUS_OK')

	/// Web Call Error Information
	DWORD errCode; ///< windows internet error code
	std::string errStr; ///< windows internet error string

	/// Web Call Timers
	Timer callTimer; ///< web call timer (constructor -> InternetCloseHandle())
	Timer responseTimer; ///< web call response timer (InternetOpenUrl() -> InternetCloseHandle())

	/// Web Call Response
	size_t responseSize; ///< web call response size (bytes not counting null termination)
	std::vector<char> responseChars; ///< web call response characters (null terminated)
	FileHelper responseFile; ///< web call response file (optional)

	// Completion event
	HANDLE hCompletionEvent;
};

/// Web Call Act Shortcuts
typedef std::shared_ptr<WebCallAct> WebCallActPtr;
#define WebCallActPtrNew(pWCA, ...) WebCallActPtr pWCA(new WebCallAct(__VA_ARGS__))
#define WebCallActLog(pWCA, ...) \
	if (pWCA)                    \
	(pWCA)->Log(__FUNCTION__, __VA_ARGS__)

/// Web Caller Interface
class KEPUTIL_EXPORT WebCaller {
public:
	WebCaller(const std::string& id, bool enable = WEB_CALLER_ENABLED, size_t threads = WEB_CALLER_THREADS, int priority = WEB_CALLER_PRIORITY);

	virtual ~WebCaller();

	/**
	* Returns web caller as string.
	*/
	std::string ToStr();

	/**
	* Logs web caller state and web call request queue.
	*/
	void Log(const std::string& funcStr);

	/**
	* Resets the web call queue without affecting enabled or web calls currently in process.
	* If resetStats is true all web call statistics are also reset.
	*/
	void Reset(bool resetStats = true);

	/**
	* If enabled all worker threads are spawned and web calls may be queued for immediate processing
	* through Call(url).  If disabled and (timeoutMs > 0) an attempt is made to first flush active calls
	* from the queue followed by all worker threads gracefully exiting. The web call queue is then reset
	* but the web call statistics are left alone. Follow by a call to Reset() with resetStats true to
	* reset the web call statistics if desired.
	*/
	bool Enable(bool enable = WEB_CALLER_ENABLED, TimeMs timeoutMs = WEB_CALLER_DISABLE_TIMEOUT_MS);

	/**
	* Returns true if the webcaller is enabled false otherwise.
	*/
	bool Enabled() const {
		return enabled;
	}

	/**
	* Returns the number of threads actively processing web calls.
	*/
	size_t ThreadsActive() {
		wcMutexGuardLock lockTM(threadsMutex);
		return threadsActive;
	}

	/**
	* Returns the number of web calls requested since the web caller was constructed.
	*/
	size_t CallsRequested() {
		wcMutexGuardLock lockWCM(webCallsMutex);
		return webCallsRequested;
	}

	/**
	* Returns the number of web calls in the queue awaiting processing.
	*/
	size_t CallsInQueue() {
		wcMutexGuardLock lockWCM(webCallsMutex);
		return webCalls.size();
	}

	/**
	* Returns the number of web calls in process of a call.
	*/
	size_t CallsInProcess() {
		wcMutexGuardLock lockWCM(webCallsMutex);
		return webCallsInProcess;
	}

	/**
	* Returns the number of web calls in the queue awaiting processing or in process.
	*/
	size_t CallsActive() {
		wcMutexGuardLock lockWCM(webCallsMutex);
		return webCalls.size() + webCallsInProcess;
	}

	/**
	* Returns the number of web calls completed.
	*/
	size_t CallsCompleted() {
		wcMutexGuardLock lockWCM(webCallsMutex);
		return webCallsCompleted;
	}

	/**
	* Returns the number of web calls errored.
	*/
	size_t CallsErrored() {
		wcMutexGuardLock lockWCM(webCallsMutex);
		return webCallsErrored;
	}

	/**
	* Returns the total call time in milliseconds of all web calls.
	*/
	TimeMs CallTimeMs() {
		wcMutexGuardLock lockWCM(webCallsMutex);
		return webCallsCallTimeMs;
	}

	/**
	* Returns the total response time in milliseconds of all web calls.
	*/
	TimeMs ResponseTimeMs() {
		wcMutexGuardLock lockWCM(webCallsMutex);
		return webCallsResponseTimeMs;
	}

	/**
	* Returns the total response bytes of all web calls.
	*/
	ULONGLONG ResponseBytes() {
		wcMutexGuardLock lockWCM(webCallsMutex);
		return webCallsResponseBytes;
	}

	/**
	* If enabled the given WebCallAct request queues for immediate processing by the web
	* caller worker threads. Depending on the wait flag it will either return immediately
	* (asynchronous) or wait for web call completion or timeout (synchronous).
	*/
	WebCallActPtr Call(WebCallActPtr& pWCA, bool wait = false);

	/**
	* Creates a new WebCallAct request and queues for immediate processing by the web
	* caller worker threads. Returns immediately.
	*/
	WebCallActPtr Call(const WebCallOpt& wco);

	/**
	* Creates a new WebCallAct request and queues for immediate processing by the web
	* caller worker threads. Waits for web call to complete or timeout to return.
	*/
	WebCallActPtr CallAndWait(const WebCallOpt& wco);

	/**
	* Wait for all web calls to complete. Returns true on success or false on timeout.
	*/
	bool Wait(TimeMs timeoutMs = WEB_CALLER_WAIT_TIMEOUT_MS);

public: // Static Functions
	/// Static Instance Management Functions
	static bool AddInstance(WebCaller* pWC);
	static bool DelInstance(const std::string& id);
	static WebCaller* GetInstance(const std::string& id, bool orCreate = false, size_t threads = WEB_CALLER_THREADS, int priority = WEB_CALLER_PRIORITY);
	static void LogInstances(const std::string& funcStr);
	static bool DisableInstances(TimeMs timeoutMs = WEB_CALLER_DISABLE_TIMEOUT_MS);

	/// Static Instance WebCall Functions
	static WebCallActPtr WebCall(const std::string& id, const WebCallOpt& wco);
	static WebCallActPtr WebCallAndWait(const std::string& id, const WebCallOpt& wco);
	static bool WebCallAndWaitOk(const std::string& id, const WebCallOpt& wco);
	static bool WebCallAndWaitOk(const std::string& id, const WebCallOpt& wco, std::string& responseStr, DWORD& errorCode, DWORD& statusCode);

	/// Static web caller test, returning true on success and false on failure.
	static bool Test();

	/// Static web caller experimental blocking POST call.
	static WebCallActPtr PostAndWait(const WebCallOpt& wco, const std::string& header, const std::string& data);

public:
	std::string idStr; ///< web caller identifier string

	bool enabled; ///< web caller enabled

	/// Web Caller Worker Threads
	wcMutex threadsMutex; // web caller threads mutex
	size_t threadsActive; ///< web caller worker threads active
	size_t threadsRequested; ///< web caller worker threads requested
	int threadPriority; ///< web caller worker thread priority

	/// Web Caller Iterable Queue
	template <typename T, typename Container = std::deque<T>>
	struct iterable_queue : public std::queue<T, Container> {
		typedef typename Container::iterator iterator;
		typedef typename Container::const_iterator const_iterator;
		iterator begin() {
			return this->c.begin();
		}
		iterator end() {
			return this->c.end();
		}
		const_iterator begin() const {
			return this->c.begin();
		}
		const_iterator end() const {
			return this->c.end();
		}
		void clear() {
			this->c.clear();
		}
	};
	typedef iterable_queue<std::shared_ptr<WebCallAct>> WEB_CALLS;

	/// Web Caller Calls Queue (accessed by worker threads)
	WEB_CALLS webCalls; ///< web caller calls queue
	wcMutex webCallsMutex; ///< web caller calls mutex
	HANDLE webCallsAvailEvent; // signaled when queue is *NOT* empty
	volatile size_t webCallsRequested; ///< web caller calls requested total
	volatile size_t webCallsInProcess; ///< web caller calls currently in process
	volatile size_t webCallsCompleted; ///< web caller calls completed total
	volatile size_t webCallsErrored; ///< web caller calls errored total

	/// Web Caller Statistics
	volatile TimeMs webCallsCallTimeMs; ///< web caller calls average call time (ms)
	volatile TimeMs webCallsResponseTimeMs; ///< web caller calls average response time (ms)
	volatile ULONGLONG webCallsResponseBytes; ///< web caller calls total response bytes
};

/// Web Caller Shortcuts
#define WebCallerLog(pWC, ...) \
	if (pWC)                   \
	(pWC)->Log(__FUNCTION__, __VA_ARGS__)
#define WebCallerLogInstances(...) WebCaller::LogInstances(__FUNCTION__, __VA_ARGS__)

/// GetBrowserPage Functions
#ifndef WEBCALLER_GBP_TIMEOUT
#define WEBCALLER_GBP_TIMEOUT WEB_CALL_OPT_TIMEOUT_MS
#endif
struct /*KEPUTIL_EXPORT*/ GBPO {
	std::string url;
	std::string resultText;
	std::string resultFile;
	size_t resultSize;
	TimeMs resultTimeMs;
	DWORD httpStatusCode;
	std::string httpStatusDesc;
	TimeMs timeoutMs;

	GBPO(const std::string& url = "") :
			url(url),
			resultSize(0),
			resultTimeMs(0.0),
			httpStatusCode(0),
			timeoutMs(WEBCALLER_GBP_TIMEOUT) {}

	virtual ~GBPO() {}
};
DLLExport bool GetBrowserPage(GBPO& gbpo);
DLLExport bool GetBrowserPageToFile(const std::string& url, const std::string& filePath, size_t& resultSize, TimeMs& resultTimeMs, DWORD& httpStatusCode, std::string& httpStatusDesc, TimeMs timeoutMs = WEBCALLER_GBP_TIMEOUT);
DLLExport bool GetBrowserPageToFile(const std::string& url, const std::string& filePath, size_t& resultSize, DWORD& httpStatusCode, std::string& httpStatusDesc, TimeMs timeoutMs = WEBCALLER_GBP_TIMEOUT);
DLLExport bool GetBrowserPage(const std::string& url, std::string& resultText, size_t& resultSize, TimeMs& resultTimeMs, DWORD& httpStatusCode, std::string& httpStatusDesc, TimeMs timeoutMs = WEBCALLER_GBP_TIMEOUT);
DLLExport bool GetBrowserPage(const std::string& url, std::string& resultText, size_t& resultSize, DWORD& httpStatusCode, std::string& httpStatusDesc, TimeMs timeoutMs = WEBCALLER_GBP_TIMEOUT);

} // namespace KEP