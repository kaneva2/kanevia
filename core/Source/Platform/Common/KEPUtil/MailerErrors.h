///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define MOD_MAILER 0x06000000

#define IDS_MAILERR_NOSENDER MOD_MAILER | 0x01
#define IDS_MAILERR_NORECIPIENT MOD_MAILER | 0x02
#define IDS_MAILERR_INCOMPATIBLE_WINSOCK_VERSION MOD_MAILER | 0x03
