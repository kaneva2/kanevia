///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

/**
 * This class defines the interface for "Traverser" implementations.  A traverser
 * can be used by various methods of this collection class to to iterate or
 * travers accross a collection
 */
template <class TraversedT>
class Traversable;

template <class TraversedT>
class Traverser {
public:
	virtual bool evaluate(TraversedT& obj) = 0;

	Traverser<TraversedT>& traverse(Traversable<TraversedT>& traversable) {
		traversable.traverse(*this);
		return *this;
	}
};

template <class TraversedT>
class Traversable {
public:
	typedef Traverser<TraversedT> TraverserT;
	virtual Traversable<TraversedT>& traverse(Traverser<TraversedT>& traversed) = 0;
};

/**
 * This class defines the interface for "Selector" implementations.  A selector can be
 * used by various methods of this collection class to acquire a pointer to a contained
 * object.  See select() for a description of how instances of this class are used.
 * Adds support of Inversion of Control (IoC) operations
 */
template <class SelectedT>
class Selectable;

template <class SelectedT>
class Selector {
public:
	virtual bool evaluate(SelectedT& obj) = 0;
	SelectedT* select(Selectable<SelectedT>& selectable) { return selectable.select(*this); }
};

template <class SelectedT>
class Selectable {
public:
	typedef Selector<SelectedT> SelectorT;
	virtual SelectedT* select(SelectorT& selector) = 0;
};

// delete operations
template <class DeleteT>
class Deletable;

template <class DeleteT>
class Deleter {
public:
	virtual bool evaluate(DeleteT& obj) = 0;
	DeleteT* remove(Deletable<DeleteT>& deletable) { return deletable.remove(*this); }
};

template <class DeleteT>
class Deletable {
public:
	Deletable<DeleteT>() {}
	virtual ~Deletable<DeleteT>() {}
	typedef Deletable<DeleteT> DeletableT;
	typedef Deleter<DeleteT> DeleterT;
	virtual DeleteT* remove(DeleterT& deleter) = 0;
};
