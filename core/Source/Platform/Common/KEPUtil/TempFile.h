///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

class TempFile : public fstream {
public:
	TempFile() :
			fstream(), _fileName() {}

	TempFile(const FileName& fileName, ios_base::openmode mode = ios_base::in | ios_base::out) :
			fstream(fileName.toString(), mode) {
		assertUnassociated();
		_fileName = fileName;
	}

	TempFile(const std::string& fileName, ios_base::openmode mode = ios_base::in | ios_base::out) :
			fstream(fileName, mode) {
		assertUnassociated();
		_fileName = fileName;
	}

	TempFile(const char* fileName, ios_base::openmode mode = ios_base::in | ios_base::out) :
			fstream(fileName, mode), _fileName(fileName) {
		assertUnassociated();
		_fileName = fileName;
	}

	TempFile(const TempFile&) { throw UnsupportedException(); }
	TempFile(fstream&) { throw UnsupportedException(); }

	virtual ~TempFile() {
		if (_fileName.toString().length() == 0)
			return;
		if (!exists(_fileName))
			return;
		if (fstream::is_open())
			fstream::close();
		::remove(_fileName.toString().c_str());
	}

	static bool exists(const FileName& fileName) {
		return (0xffffffff != GetFileAttributesW(Utf8ToUtf16(fileName.toString()).c_str()));
	}

	const FileName& name() const {
		return _fileName;
	}

	TempFile& open(const std::string& fileName, ios_base::openmode mode = ios_base::in | ios_base::out) {
		return open(FileName(fileName), mode);
	}

	TempFile& open(const FileName& fileName, ios_base::openmode mode = ios_base::in | ios_base::out) {
		_fileName = fileName;
		try {
			fstream::exceptions(std::ofstream::failbit | std::ofstream::badbit);
			fstream::open(_fileName.toString(), mode); // ios_base::trunc | ios_base::out );
		} catch (ofstream::failure e) {
			stringstream ss;
			ss << "Failed to open file (truncate|output) [" << _fileName.toString() << "][" << e.code() << "][" << e.what() << "]";
			throw ChainedException(SOURCELOCATION, ss.str());
		}
		return *this;
	}

	TempFile& close() {
		fstream::close();
		return *this;
	}

private:
	void assertUnassociated() {
		std::string s = _fileName.toString();
		if (_fileName.toString().length() > 0) {
			if (fstream::is_open()) {
				fstream::close();
				::remove(_fileName.toString().c_str());
			}
			throw UnsupportedException();
		}
	}

	FileName _fileName;
};
