///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "WebCall.h"

#include "SetThreadName.h"
#include <Core/Util/CallbackSystem.h>

/// Static Web Caller Default Configuration
#ifndef DRF_WEB_CALLER_RXBUF_CHARS
#define DRF_WEB_CALLER_RXBUF_CHARS (128 * 1024)
#endif

// debug logging call response ok (0=off 1=on)
#ifdef _DEBUG
#define DRF_WEB_CALLER_LOG_RESPONSE_OK 1
#else
#define DRF_WEB_CALLER_LOG_RESPONSE_OK 0
#endif

#define WC_FMT_TIME FMT_TIME

namespace KEP {

static LogInstance("Instance");
static std::map<std::string, WebCaller*> wcInstances; // web caller instances

///////////////////////////////////////////////////////////////////////////////
// Static Functions
///////////////////////////////////////////////////////////////////////////////

/** DRF
 * This function wraps HttpQueryInfo() for getting http status codes and
 * converting to string.
 */
static void WebCallGetHttpStatus(HINTERNET hInternetUrl, DWORD& statusCode, std::string& statusStr) {
	// Get HTTP Status Code (as char string eg. '200')
	CHAR statusCodeStr[16] = "";
	DWORD dwSize = sizeof(statusCodeStr);
	if (!::HttpQueryInfoA(hInternetUrl, HTTP_QUERY_STATUS_CODE, &statusCodeStr, &dwSize, NULL)) {
		statusCode = 0;
		statusStr = "HTTP_QUERY_INFO_FAILED";
	} else {
		statusCode = atoi(statusCodeStr);
		switch (statusCode) {
			case HTTP_STATUS_CONTINUE:
				statusStr = "CONTINUE";
				break;
			case HTTP_STATUS_SWITCH_PROTOCOLS:
				statusStr = "SWITCH_PROTOCOLS";
				break;
			case HTTP_STATUS_OK:
				statusStr = "OK";
				break;
			case HTTP_STATUS_CREATED:
				statusStr = "CREATED";
				break;
			case HTTP_STATUS_ACCEPTED:
				statusStr = "ACCEPTED";
				break;
			case HTTP_STATUS_PARTIAL:
				statusStr = "PARTIAL";
				break;
			case HTTP_STATUS_NO_CONTENT:
				statusStr = "NO_CONTENT";
				break;
			case HTTP_STATUS_RESET_CONTENT:
				statusStr = "RESET_CONTENT";
				break;
			case HTTP_STATUS_PARTIAL_CONTENT:
				statusStr = "PARTIAL_CONTENT";
				break;
			case HTTP_STATUS_AMBIGUOUS:
				statusStr = "AMBIGUOUS";
				break;
			case HTTP_STATUS_MOVED:
				statusStr = "MOVED";
				break;
			case HTTP_STATUS_REDIRECT:
				statusStr = "REDIRECT";
				break;
			case HTTP_STATUS_REDIRECT_METHOD:
				statusStr = "REDIRECT_METHOD";
				break;
			case HTTP_STATUS_NOT_MODIFIED:
				statusStr = "NOT_MODIFIED";
				break;
			case HTTP_STATUS_USE_PROXY:
				statusStr = "USE_PROXY";
				break;
			case HTTP_STATUS_REDIRECT_KEEP_VERB:
				statusStr = "REDIRECT_KEEP_VERB";
				break;
			case HTTP_STATUS_BAD_REQUEST:
				statusStr = "BAD_REQUEST";
				break;
			case HTTP_STATUS_DENIED:
				statusStr = "DENIED";
				break;
			case HTTP_STATUS_PAYMENT_REQ:
				statusStr = "PAYMENT_REQ";
				break;
			case HTTP_STATUS_FORBIDDEN:
				statusStr = "FORBIDDEN";
				break;
			case HTTP_STATUS_NOT_FOUND:
				statusStr = "NOT_FOUND";
				break;
			case HTTP_STATUS_BAD_METHOD:
				statusStr = "BAD_METHOD";
				break;
			case HTTP_STATUS_NONE_ACCEPTABLE:
				statusStr = "NONE_ACCEPTABLE";
				break;
			case HTTP_STATUS_PROXY_AUTH_REQ:
				statusStr = "PROXY_AUTH_REQ";
				break;
			case HTTP_STATUS_REQUEST_TIMEOUT:
				statusStr = "REQUEST_TIMEOUT";
				break;
			case HTTP_STATUS_CONFLICT:
				statusStr = "CONFLICT";
				break;
			case HTTP_STATUS_GONE:
				statusStr = "GONE";
				break;
			case HTTP_STATUS_LENGTH_REQUIRED:
				statusStr = "LENGTH_REQUIRED";
				break;
			case HTTP_STATUS_PRECOND_FAILED:
				statusStr = "PRECOND_FAILED";
				break;
			case HTTP_STATUS_REQUEST_TOO_LARGE:
				statusStr = "REQUEST_TOO_LARGE";
				break;
			case HTTP_STATUS_URI_TOO_LONG:
				statusStr = "URI_TOO_LONG";
				break;
			case HTTP_STATUS_UNSUPPORTED_MEDIA:
				statusStr = "UNSUPPORTED_MEDIA";
				break;
			case HTTP_STATUS_RETRY_WITH:
				statusStr = "RETRY_WITH";
				break;
			case HTTP_STATUS_SERVER_ERROR:
				statusStr = "SERVER_ERROR";
				break;
			case HTTP_STATUS_NOT_SUPPORTED:
				statusStr = "NOT_SUPPORTED";
				break;
			case HTTP_STATUS_BAD_GATEWAY:
				statusStr = "BAD_GATEWAY";
				break;
			case HTTP_STATUS_SERVICE_UNAVAIL:
				statusStr = "SERVICE_UNAVAIL";
				break;
			case HTTP_STATUS_GATEWAY_TIMEOUT:
				statusStr = "GATEWAY_TIMEOUT";
				break;
			case HTTP_STATUS_VERSION_NOT_SUP:
				statusStr = "VERSION_NOT_SUP";
				break;
			case HTTP_STATUS_KANEVA_AUTH:
				statusStr = "KANEVA_AUTH";
				break;
			default:
				statusStr = "STATUS_UNKNOWN";
				break;
		}
	}
}

/** DRF
 * This function wraps GetLastError() and InternetGetLastResponseInfo() for
 * getting internet error codes and converting to string.
 */
static void WebCallInternetGetLastError(DWORD& errCode, std::string& errStr) {
	errCode = ::GetLastError();
	CHAR errChar[1024] = "";
	DWORD errSize = sizeof(errChar);
	DWORD errCodeInt = 0;
	::InternetGetLastResponseInfoA(&errCodeInt, errChar, &errSize);
	if (!errCode)
		errCode = errCodeInt;
	switch (errCode) {
		case ERROR_INTERNET_OUT_OF_HANDLES:
			errStr = "OUT_OF_HANDLES";
			break;
		case ERROR_INTERNET_TIMEOUT:
			errStr = "TIMEOUT";
			break;
		case ERROR_INTERNET_EXTENDED_ERROR:
			errStr = "EXTENDED_ERROR";
			break;
		case ERROR_INTERNET_INTERNAL_ERROR:
			errStr = "INTERNAL_ERROR";
			break;
		case ERROR_INTERNET_INVALID_URL:
			errStr = "INVALID_URL";
			break;
		case ERROR_INTERNET_UNRECOGNIZED_SCHEME:
			errStr = "UNRECOGNIZED_SCHEME";
			break;
		case ERROR_INTERNET_NAME_NOT_RESOLVED:
			errStr = "NAME_NOT_RESOLVED";
			break;
		case ERROR_INTERNET_PROTOCOL_NOT_FOUND:
			errStr = "PROTOCOL_NOT_FOUND";
			break;
		case ERROR_INTERNET_INVALID_OPTION:
			errStr = "INVALID_OPTION";
			break;
		case ERROR_INTERNET_BAD_OPTION_LENGTH:
			errStr = "BAD_OPTION_LENGTH";
			break;
		case ERROR_INTERNET_OPTION_NOT_SETTABLE:
			errStr = "OPTION_NOT_SETTABLE";
			break;
		case ERROR_INTERNET_SHUTDOWN:
			errStr = "SHUTDOWN";
			break;
		case ERROR_INTERNET_INCORRECT_USER_NAME:
			errStr = "INCORRECT_USER_NAME";
			break;
		case ERROR_INTERNET_INCORRECT_PASSWORD:
			errStr = "INCORRECT_PASSWORD";
			break;
		case ERROR_INTERNET_LOGIN_FAILURE:
			errStr = "LOGIN_FAILURE";
			break;
		case ERROR_INTERNET_INVALID_OPERATION:
			errStr = "INVALID_OPERATION";
			break;
		case ERROR_INTERNET_OPERATION_CANCELLED:
			errStr = "OPERATION_CANCELLED";
			break;
		case ERROR_INTERNET_INCORRECT_HANDLE_TYPE:
			errStr = "INCORRECT_HANDLE_TYPE";
			break;
		case ERROR_INTERNET_INCORRECT_HANDLE_STATE:
			errStr = "INCORRECT_HANDLE_STATE";
			break;
		case ERROR_INTERNET_NOT_PROXY_REQUEST:
			errStr = "NOT_PROXY_REQUEST";
			break;
		case ERROR_INTERNET_REGISTRY_VALUE_NOT_FOUND:
			errStr = "REGISTRY_VALUE_NOT_FOUND";
			break;
		case ERROR_INTERNET_BAD_REGISTRY_PARAMETER:
			errStr = "BAD_REGISTRY_PARAMETER";
			break;
		case ERROR_INTERNET_NO_DIRECT_ACCESS:
			errStr = "NO_DIRECT_ACCESS";
			break;
		case ERROR_INTERNET_NO_CONTEXT:
			errStr = "NO_CONTEXT";
			break;
		case ERROR_INTERNET_NO_CALLBACK:
			errStr = "NO_CALLBACK";
			break;
		case ERROR_INTERNET_REQUEST_PENDING:
			errStr = "REQUEST_PENDING";
			break;
		case ERROR_INTERNET_INCORRECT_FORMAT:
			errStr = "INCORRECT_FORMAT";
			break;
		case ERROR_INTERNET_ITEM_NOT_FOUND:
			errStr = "ITEM_NOT_FOUND";
			break;
		case ERROR_INTERNET_CANNOT_CONNECT:
			errStr = "CANNOT_CONNECT";
			break;
		case ERROR_INTERNET_CONNECTION_ABORTED:
			errStr = "CONNECTION_ABORTED";
			break;
		case ERROR_INTERNET_CONNECTION_RESET:
			errStr = "CONNECTION_RESET";
			break;
		case ERROR_INTERNET_FORCE_RETRY:
			errStr = "FORCE_RETRY";
			break;
		case ERROR_INTERNET_INVALID_PROXY_REQUEST:
			errStr = "INVALID_PROXY_REQUEST";
			break;
		case ERROR_INTERNET_NEED_UI:
			errStr = "NEED_UI";
			break;
		default:
			errStr = errChar;
			break;
	}
}

/** DRF
 * This function wraps InternetSetOption() for setting internet connection options.
 */
static bool WebCallInternetSetOptions(HINTERNET hInternet, TimeMs timeoutMs, bool gzip) {
	// Set Internet Timeouts
	unsigned long timeout = timeoutMs;
	if (::InternetSetOptionA(hInternet, INTERNET_OPTION_CONNECT_TIMEOUT, &timeout, sizeof(timeout)) == FALSE) {
		LogError("InternetSetOption(INTERNET_OPTION_CONNECT_TIMEOUT) FAILED");
		return false;
	}
	if (::InternetSetOptionA(hInternet, INTERNET_OPTION_RECEIVE_TIMEOUT, &timeout, sizeof(timeout)) == FALSE) {
		LogError("InternetSetOption(INTERNET_OPTION_RECEIVE_TIMEOUT) FAILED");
		return false;
	}

	// Set Internet Decoding (also requires 'Accept-Encoding=gzip,deflate' header)
	BOOL decode = (gzip ? TRUE : FALSE);
	if (::InternetSetOptionA(hInternet, INTERNET_OPTION_HTTP_DECODING, &decode, sizeof(decode)) == FALSE) {
		LogError("InternetSetOption(INTERNET_OPTION_HTTP_DECODING) FAILED");
		return false;
	}

	return true;
}

/** DRF
 * This function opens the internet connection on the first call and returns
 * the shared singleton HINTERNET handle on success or NULL on failure.
 */
static HINTERNET WebCallInternetOpen() {
	// Shared Singleton Internet Connection Already Open ?
	static HINTERNET hInternet = NULL;
	if (hInternet)
		return hInternet;

	// Open Shared Singleton Internet Connection
	hInternet = ::InternetOpenA("WebCaller", INTERNET_OPEN_TYPE_PRECONFIG, 0, 0, 0);
	if (!hInternet) {
		LogError("InternetOpen() FAILED");
		return NULL;
	}

	// Set Default Internet Connection Options
	WebCallInternetSetOptions(hInternet, WEB_CALL_OPT_TIMEOUT_MS, WEB_CALL_OPT_GZIP);

	return hInternet;
}

///////////////////////////////////////////////////////////////////////////////
// WebCallAct Functions
///////////////////////////////////////////////////////////////////////////////

WebCallAct::WebCallAct(const WebCallOpt& wco) :
		wco(wco), state(STATE_IDLE), m_bComplete(false), statusCode(0), errCode(0), responseSize(0), responseTimer(Timer::State::Paused), hCompletionEvent(::CreateEvent(NULL, TRUE, FALSE, NULL)) // Use manual reset flag - once the event is signaled, it stays signaled. This allows multiple Wait calls on a same WebCallAct.
{
	// Assure Internet Is Open
	WebCallInternetOpen();
}

// NOTE - Automatically Closes responseFile !
WebCallAct::~WebCallAct() {
	// Close handle
	::CloseHandle(hCompletionEvent);
}

void WebCallAct::SetComplete() {
	m_bComplete.store(true, std::memory_order_relaxed);
	::SetEvent(hCompletionEvent);
}

bool WebCallAct::Call() {
	bool ok = false;
	void* hInternet = NULL;
	void* hInternetUrl = NULL;

	// Reset Response, Error & Status Info
	responseSize = 0;
	responseChars.clear();
	responseTimer.Reset();
	errCode = 0;
	errStr = "";
	statusCode = 0;
	statusStr = "";

	// Use Parameter Gzip ? (also strips from url)
	const char* header = NULL;
	wco.UseParamGzip();
	if (wco.gzip)
		header = "Accept-Encoding: gzip,deflate";

	// Encode Url ?
	std::string url = wco.urlEncode ? UrlEncode(wco.url, true) : wco.url;

	// Secure Url ?
	if (wco.secure)
		STLStringSubstitute(url, "http:", "https:", false);

	// Using Response File ?
	std::string responseFilePath = wco.responseFilePath;
	bool useResponseFile = !responseFilePath.empty();
	if (useResponseFile) {
		wco.response = true; // needs response
		StrAppend(responseFilePath, ".tmp"); // use <filePath>.tmp until success
		if (!responseFile.Open(responseFilePath, GENERIC_WRITE, CREATE_ALWAYS)) {
			state = STATE_FILE_ERROR;
			LogError("responseFile.Open('" << responseFilePath << "') FAILED - " << ToStr());
			goto CallComplete;
		}
	}

	// Start Response Timer
	responseTimer.Start();

	// Internet Open ?
	state = STATE_URL_OPENING;
	hInternet = WebCallInternetOpen();
	if (!hInternet) {
		// STATE_URL_ERROR - Get Windows Error Information & Complete Failure
		WebCallInternetGetLastError(errCode, errStr);
		state = STATE_URL_ERROR;
		LogError("WebCallInternetOpen() FAILED - " << ToStr());
		goto CallComplete;
	}

	// Open Url To Begin Web Call
	const DWORD internetFlags = (INTERNET_FLAG_RELOAD); // | INTERNET_FLAG_IGNORE_REDIRECT_TO_HTTP | INTERNET_FLAG_NO_AUTO_REDIRECT);
	hInternetUrl = ::InternetOpenUrlA(hInternet, url.c_str(), header, -1L, internetFlags, 0);
	if (!hInternetUrl) {
		// STATE_URL_ERROR - Get Windows Error Information & Complete Failure
		DWORD errCodeTemp = 0;
		std::string errStrTemp;
		WebCallInternetGetLastError(errCodeTemp, errStrTemp);
		LogError("InternetOpenUrl() FAILED - " << ToStr());

		// Https Fallback To Http Instead ?
		bool httpsFallback = STLStringSubstitute(url, "https:", "http:", false);
		if (!httpsFallback) {
			errCode = errCodeTemp;
			errStr = errStrTemp;
			state = STATE_URL_ERROR;
			goto CallComplete;
		}

		// Try Again with Http Instead
		hInternetUrl = ::InternetOpenUrlA(hInternet, url.c_str(), header, -1L, internetFlags, 0);
		if (!hInternetUrl) {
			WebCallInternetGetLastError(errCode, errStr);
			LogError("InternetOpenUrl(HTTP_FALLBACK) FAILED - " << ToStr());
			state = STATE_URL_ERROR;
			goto CallComplete;
		} else {
			state = STATE_URL_OK;
		}
	}

	// Get HTTP Status Result (eg. 200 HTTP_STATUS_OK)
	WebCallGetHttpStatus(hInternetUrl, statusCode, statusStr);

	// DRF - Added - Is Status Ok ?
	if (!IsStatusOk()) {
		// STATE_URL_ERROR - Get Windows Error Information & Complete Failure
		WebCallInternetGetLastError(errCode, errStr);
		state = STATE_URL_ERROR;
		LogError("WebCallGetHttpStatus() FAILED - " << ToStr());
		goto CallComplete;
	}

	// STATE_URL_OK
	ok = true;
	state = STATE_URL_OK;

	// Get HTTP Response Data ?
	if (wco.response) {
		// Set Internet Options
		WebCallInternetSetOptions(hInternetUrl, wco.timeoutMs, wco.gzip);

		// Read Response Html Character Chunks Until Complete
		char rxBuf[DRF_WEB_CALLER_RXBUF_CHARS] = "";
		DWORD rxChars = 0;
		state = STATE_RESPONSE_READING;
		FOREVER {
			// Read Next Chunk Of Characters
			ok = (::InternetReadFile(hInternetUrl, rxBuf, sizeof(rxBuf) - 1, &rxChars) == TRUE);
			if (!ok) {
				// STATE_RESPONSE_ERROR - Get Windows Error Information & Complete Failure
				WebCallInternetGetLastError(errCode, errStr);
				state = STATE_RESPONSE_ERROR;
				LogError("InternetReadFile() FAILED - " << ToStr());
				goto CallComplete;

			} else if (!rxChars) {
				// STATE_RESPONSE_OK - Response Data Is Complete OK
				state = STATE_RESPONSE_OK;
				goto CallComplete;
			}

			// STATE_RESPONSE_READING - Accumulate Response & Reset Timeout Timer
			responseSize += rxChars;
			if (useResponseFile)
				responseFile.Tx(rxBuf, rxChars);
			else
				responseChars.insert(responseChars.end(), rxBuf, rxBuf + rxChars);

#if 0
			// Null Terminate Response Characters Incase Used Directly As String
			if (!useResponseFile)
				responseChars.push_back(NULL);
#endif
		}
	}

CallComplete:

	// Close Internet Url
	if (hInternetUrl)
		::InternetCloseHandle(hInternetUrl);

	// Stop All Timers
	callTimer.Pause();
	responseTimer.Pause();

	// Finalize Response File ?
	if (responseFile.IsOpen()) {
		responseFile.Close();
		if (ok && IsStatusOk())
			FileHelper::Move(responseFilePath, StrStripFileExt(responseFilePath));
		else
			FileHelper::Delete(responseFilePath);
	}

	// Web Call Complete
	SetComplete();

	// Log OK (without response HTML)
	bool isIssue = (IsSlow() || !IsStatusOk());
	bool logResponseOk = (DRF_WEB_CALLER_LOG_RESPONSE_OK == 1);
	if (ok && (isIssue || (!isIssue && logResponseOk)))
		WebCallActLog(this);

	// Application Callback ?
	if (wco.cbFunc)
		wco.cbFunc(this);

	return ok;
}

bool WebCallAct::Wait() const {
	// Wait Until All Web Call Completes Or Timeout
	auto rc = ::WaitForSingleObject(hCompletionEvent, INFINITE);
	if (rc == WAIT_TIMEOUT) {
		// Note that WaitForSingleObject does not have the same time precision as our HP timer
		LogError("WAIT TIMEOUT - " << ToStr());
		return false;
	} else if (rc != WAIT_OBJECT_0) {
		ASSERT(false);
		LogError("WAIT rc = " << rc << ", " << ToStr());
		// Unexpected state, continue to check if web call completed.
	}

	// Web Call Ok ?
	bool ok = IsOk();
	if (!ok) {
		LogError("FAILED - " << ToStr());
	}

	return ok;
}

std::string WebCallAct::CallTimeStr() const {
	// Build Time String (queueTime + responseTime)
	std::string str;
	TimeMs callTimeMs = CallTimeMs();
	TimeMs responseTimeMs = ResponseTimeMs();
	TimeMs timeDiff = callTimeMs - responseTimeMs;
	if (timeDiff < 1) {
		StrBuild(str, WC_FMT_TIME << responseTimeMs);
	} else {
		StrBuild(str, WC_FMT_TIME << timeDiff << "+" << responseTimeMs);
	}
	if (IsTimeout()) {
		StrBuild(str, "(TIMEOUT " << str << "ms)");
	} else if (IsSlow()) {
		StrBuild(str, "(SLOW " << str << "ms)");
	} else {
		StrBuild(str, "(" << str << "ms)");
	}
	return str;
}

std::string WebCallAct::ToStr() const {
	// Build This WCA String
	std::string str;
	StrBuild(str, "WCA<" << this << "> ");

	// Add Status Variables
	if (wco.verbose & WC_VERBOSE_STATUS) {
		if (IsError()) {
			StrAppend(str, "[" << ErrorCode() << "] " << ErrorStr() << " ");
		} else {
			if (StatusCode() && !IsStatusOk()) {
				StrAppend(str, "[" << StatusCode() << "] " << StatusStr() << " ");
			}
			StrAppend(str, StateStr() << " ");
		}
		StrAppend(str, CallTimeStr());
	}

	// Add URL String and Response File Path
	if ((wco.verbose & WC_VERBOSE_URL) && !wco.url.empty()) {
		StrAppend(str, " '" << wco.url << "'");
		if (!wco.responseFilePath.empty()) {
			StrAppend(str, " -> '" << LogPath(wco.responseFilePath) << "'");
		}
	}

	// Add Response File or Chars (as string)
	if ((wco.verbose & WC_VERBOSE_RESPONSE) && wco.responseFilePath.empty() && ResponseSize()) {
		StrAppend(str, "\n" << ResponseStr());
	}

	return str;
}

void WebCallAct::Log(const std::string& funcStr) {
	std::string funcStrDeep = funcStr;
	if (IsError() || !IsStatusOk()) {
		_LogError(funcStrDeep << ": " << ToStr());
	} else if (IsSlow()) {
		_LogWarn(funcStrDeep << ": " << ToStr());
	} else if (wco.verbose) {
		_LogInfo(funcStrDeep << ": " << ToStr());
	}
}

///////////////////////////////////////////////////////////////////////////////
// WebCaller Static Instance Management Functions
///////////////////////////////////////////////////////////////////////////////

bool WebCaller::AddInstance(WebCaller* pWC) {
	if (!pWC) {
		LogError("<NULL> FAILED");
		return false;
	}
	wcInstances[pWC->idStr] = pWC;
	return true;
}

bool WebCaller::DelInstance(const std::string& id) {
	auto it = wcInstances.find(id);
	auto found = (it != wcInstances.end());
	if (!found) {
		LogError("'" << id << "' NOT FOUND");
		return false;
	}
	wcInstances.erase(id);
	return true;
}

WebCaller* WebCaller::GetInstance(const std::string& id, bool orCreate, size_t threads, int priority) {
	auto it = wcInstances.find(id);
	auto found = (it != wcInstances.end());
	if (!found && orCreate) {
		LogInfo("'" << id << "' NOT FOUND - Creating...");
		auto pWC = new WebCaller(id, true, threads, priority);
		if (!pWC) {
			LogError("FAILED - '" << id << "'");
		}
		return pWC;
	}
	if (!found) {
		LogError("'" << id << "' NOT FOUND");
		return NULL;
	}
	return wcInstances[id];
}

void WebCaller::LogInstances(const std::string& funcStr) {
	std::string funcStrDeep = funcStr + "." __FUNCTION__;
	_LogInfo(funcStrDeep << ": instances=" << wcInstances.size());
	for (auto& it : wcInstances) {
		auto pWC = it.second;
		if (!pWC)
			continue;
		_LogInfo(funcStrDeep << ": ... " << pWC->ToStr());
	}
}

bool WebCaller::DisableInstances(TimeMs timeoutMs) {
	bool ok = true;
	for (auto& it : wcInstances) {
		auto pWC = it.second;
		if (!pWC)
			continue;
		ok &= pWC->Enable(false, timeoutMs);
	}
	if (timeoutMs)
		fTime::SleepMs(100); // give threads some extra time to exit
	WebCallerLogInstances();
	return ok;
}

///////////////////////////////////////////////////////////////////////////////
// WebCaller Static Instance WebCall Functions
///////////////////////////////////////////////////////////////////////////////

WebCallActPtr WebCaller::WebCall(const std::string& id, const WebCallOpt& wco) {
	// Get Identified Web Caller (or create if not found)
	auto pWC = WebCaller::GetInstance(id, true);
	if (!pWC)
		return NULL;

	// Make Web Call, Don't Wait For Complete & Return WebCallAct
	return pWC->Call(wco);
}

WebCallActPtr WebCaller::WebCallAndWait(const std::string& id, const WebCallOpt& wco) {
	// Get Identified Web Caller (or create if not found)
	auto pWC = WebCaller::GetInstance(id, true);
	if (!pWC)
		return NULL;

	// Make Web Call, Wait For Complete & Return WebCallAct
	auto pWCA = pWC->CallAndWait(wco);
	return pWCA;
}

bool WebCaller::WebCallAndWaitOk(const std::string& id, const WebCallOpt& wco) {
	// Make Web Call, Wait For Complete & Return WebCallAct Is Ok
	auto pWCA = WebCallAndWait(id, wco);
	return pWCA ? pWCA->IsOk() : false;
}

bool WebCaller::WebCallAndWaitOk(const std::string& id, const WebCallOpt& wco, std::string& responseStr, DWORD& errorCode, DWORD& statusCode) {
	// Make Web Call, Wait For Complete & Return WebCallAct Is Ok
	auto pWCA = WebCallAndWait(id, wco);
	if (pWCA) {
		responseStr = pWCA->ResponseStr();
		errorCode = pWCA->ErrorCode();
		statusCode = pWCA->StatusCode();
	}
	return pWCA ? pWCA->IsOk() : false;
}

///////////////////////////////////////////////////////////////////////////////
// WebCaller Worker Thread Function
///////////////////////////////////////////////////////////////////////////////

/** DRF
 * This function is the web caller worker thread.  It pops ACTs off the singleton webCalls queue
 * and performs webcalls to the ACT.url with ACT.mode option to either ignore the server response
 * html string in which case it immediately closes the url connection or stays open and buffers
 * the response html string. In either case if the application callback ACT.cbFunc is defined then
 * it creates a thread for the callback and passes it a copy of the finalized ACT including server
 * response if requested or any error codes and messages that may have occured.
 */
static DWORD WINAPI WebCallerWorkerThread(LPVOID vThreadParm) {
	// Cast Thread Param To Web Caller Pointer
	WebCaller* pWC(static_cast<WebCaller*>(vThreadParm));
	if (!pWC) {
		LogError("static_cast<WebCaller*>(" << vThreadParm << ") FAILED");
		return 0;
	}

	// Set Thread Name
	std::string str;
	StrBuild(str, "WebCaller::" << pWC->idStr);
	::SetThreadName(str.c_str());

	// Web Caller Worker Loops While Enabled
	while (pWC->Enabled()) {
		// Wait until something happens to the queue.
		auto rc = ::WaitForSingleObject(pWC->webCallsAvailEvent, INFINITE);
		ASSERT(rc == WAIT_OBJECT_0); // Assertion only. We will use additional checks to make sure the queue is actually available.

		// Check if disabled while waiting
		if (!pWC->Enabled()) {
			break;
		}

		// Web Calls Waiting In Queue ?
		wcMutexGuardLock lockWCM(pWC->webCallsMutex);
		if (pWC->webCalls.empty()) {
			::ResetEvent(pWC->webCallsAvailEvent); // Queue is empty - manually reset the event
			lockWCM.unlock();
			fTime::SleepMs(1); // check again in 1ms
			continue;
		}

		// Pop Web Call From Queue & Increment Processing Count
		auto pWCA = pWC->webCalls.front();
		pWC->webCalls.pop();
		if (pWC->webCalls.empty()) {
			::ResetEvent(pWC->webCallsAvailEvent); // Queue is empty - manually reset the event
		}
		pWC->webCallsInProcess++;
		lockWCM.unlock();

		// Process Web Call Until Completion (blocking timeout)
		bool ok = pWCA->Call();

		// Decrement Processing Count & Update Statistics
		lockWCM.lock();
		pWC->webCallsInProcess--;
		pWC->webCallsCompleted++;
		pWC->webCallsResponseBytes += pWCA->ResponseSize();
		UPDATE_AVG(pWC->webCallsCallTimeMs, pWC->webCallsCompleted, pWCA->CallTimeMs());
		UPDATE_AVG(pWC->webCallsResponseTimeMs, pWC->webCallsCompleted, pWCA->ResponseTimeMs());
		if (!ok) {
			pWC->webCallsErrored++;
			LogError("Call() FAILED - " << pWCA->ToStr());
		}
		lockWCM.unlock();
	}

	// Decrement Threads Active
	wcMutexGuardLock lockTM(pWC->threadsMutex);
	pWC->threadsActive--;

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// WebCaller Public Functions
///////////////////////////////////////////////////////////////////////////////

WebCaller::WebCaller(const std::string& id, bool enable, size_t threads, int priority) :
		idStr(id),
		enabled(false),
		threadsRequested(threads),
		threadsActive(0),
		threadPriority(priority),
		webCallsRequested(0),
		webCallsInProcess(0),
		webCallsCompleted(0),
		webCallsErrored(0),
		webCallsCallTimeMs(0),
		webCallsResponseTimeMs(0),
		webCallsResponseBytes(0),
		webCallsAvailEvent(::CreateEvent(NULL, TRUE, FALSE, NULL)) // queue is initially empty, set when new jobs queued, manual-reset when found empty again
{
	if (enable)
		Enable(true);
	AddInstance(this);
}

WebCaller::~WebCaller() {
	DelInstance(idStr);
	if (Enabled())
		Enable(false);
	CloseHandle(webCallsAvailEvent);
}

bool WebCaller::Enable(bool enable, TimeMs timeoutMs) {
	// Enable/Disable Web Calls
	bool ok = false;
	enabled = enable;
	if (enable) {
		// Create Web Calls Worker Threads With This Web Caller
		wcMutexGuardLock lockTM(threadsMutex);
		while (threadsActive < threadsRequested) {
			HANDLE handle = ::CreateThread(WebCallerWorkerThread, this, threadPriority);
			if (!handle) {
				LogError("::CreateThread() FAILED - " << ToStr());
				return false;
			}
			CloseHandle(handle);
			threadsActive++;
		}
		ok = (threadsActive == threadsRequested);

	} else {
		// Notify worker thread to exit
		::SetEvent(webCallsAvailEvent);

		// Wait For Web Calls & Threads To Complete Or Timeout ?
		if (timeoutMs) {
			// Wait For Web Calls To Complete Or Timeout
			ok = Wait(timeoutMs);

			// Wait For Threads To Complete Or Timeout
			Timer timer;
			while (ThreadsActive()) {
				if (timer.ElapsedMs() >= timeoutMs) {
					LogError("TIMEOUT - " << ToStr());
					ok = false;
					break;
				}
				fTime::SleepMs(1); // check again in 1ms
			}
		} else {
			ok = true;
		}

		// Reset Web Call Queue (not statistics)
		Reset(false);
	}

	return ok;
}

void WebCaller::Reset(bool resetStats) {
	// Reset Web Call Queue
	wcMutexGuardLock lockWCM(webCallsMutex);
	webCalls.clear();
	::ResetEvent(webCallsAvailEvent); // Queue is empty - manually reset the event

	// Reset Web Call Statistics
	if (resetStats) {
		webCallsRequested = 0;
		webCallsCompleted = 0;
		webCallsErrored = 0;
		webCallsCallTimeMs = 0;
		webCallsResponseTimeMs = 0;
		webCallsResponseBytes = 0;
	}
}

WebCallActPtr WebCaller::Call(WebCallActPtr& pWCA, bool wait) {
	// Don't Queue While Disabled
	if (!Enabled())
		return pWCA;

	// Push Url To Web Call Queue
	wcMutexGuardLock lockWCM(webCallsMutex);
	webCalls.push(pWCA);
	::SetEvent(webCallsAvailEvent); // Signal that new job is available
	webCallsRequested++;
	lockWCM.unlock();

	// Wait For Call Complete?
	if (wait)
		pWCA->Wait();

	return pWCA;
}

WebCallActPtr WebCaller::Call(const WebCallOpt& wco) {
	// Create New WebCallAct And Call It Asynchronously (return immediately)
	WebCallActPtrNew(pWCA, wco);
	return Call(pWCA, false);
}

WebCallActPtr WebCaller::CallAndWait(const WebCallOpt& wco) {
	// Create New WebCallAct And Call It Synchronously (return on complete or timeout)
	WebCallActPtrNew(pWCA, wco);
	return Call(pWCA, true);
}

bool WebCaller::Wait(TimeMs timeoutMs) {
	// Calls Active?
	if (!CallsActive())
		return true;

	// Wait Until All Active Web Calls Complete Or Timeout
	Timer timer;
	while (CallsActive()) {
		if (timer.ElapsedMs() >= timeoutMs) {
			LogError("TIMEOUT - " << ToStr());
			return false;
		}
		fTime::SleepMs(1); // check again in 1ms
	}
	return true;
}

std::string WebCaller::ToStr() {
	// Build String For Logging
	wcMutexGuardLock lockWCM(webCallsMutex);
	std::string str;
	StrBuild(str, "WC<" << this << "> '" << idStr << "' " << (Enabled() ? "ENABLED" : "DISABLED"));
	if (ThreadsActive())
		StrAppend(str, " threads=" << ThreadsActive());
	if (CallsActive()) {
		StrAppend(str, " calls=" << CallsCompleted() << "+" << CallsActive());
	} else if (CallsCompleted()) {
		StrAppend(str, " calls=" << CallsCompleted());
	}
	if (CallsErrored())
		StrAppend(str, " errors=" << CallsErrored());
	if (CallTimeMs() || ResponseTimeMs()) {
		TimeMs timeDiff = CallTimeMs() - ResponseTimeMs();
		if (timeDiff < 1) {
			StrAppend(str, " timeMs=" << WC_FMT_TIME << ResponseTimeMs());
		} else {
			StrAppend(str, " timeMs=" << WC_FMT_TIME << timeDiff << "+" << ResponseTimeMs());
		}
	}
	if (ResponseBytes())
		StrAppend(str, " bytes=" << ResponseBytes());

	return str;
}

void WebCaller::Log(const std::string& funcStr) {
	// Log Urls In Web Call Queue
	wcMutexGuardLock lockWCM(webCallsMutex);
	std::string funcStrDeep = funcStr + "." __FUNCTION__;
	_LogInfo(funcStrDeep << ": " << ToStr());
	for (auto& pWCA : webCalls) {
		_LogInfo(funcStrDeep << ": ... " << pWCA->ToStr());
	}
}

///////////////////////////////////////////////////////////////////////////////
// WebCaller Test Functions
///////////////////////////////////////////////////////////////////////////////
#define DRF_WEB_CALLER_TEST_CALLS 5

/** DRF
 * WebCaller::Test() Callback
 */
static size_t testCallbacks = 0;
static DWORD WebCallerTestCallback(void* vThreadParm) {
	// Cast WebCallAct*
	auto pWCA = static_cast<WebCallAct*>(vThreadParm);
	if (!pWCA) {
		LogError("static_cast<WebCallAct*>(" << vThreadParm << ") FAILED");
		return 0;
	}
	testCallbacks++;

	// Log WebCallAct (verbose FULL)
	pWCA->wco.verbose = WC_VERBOSE_FULL; // & (~WC_VERBOSE_RESPONSE);
	WebCallActLog(pWCA);

	return 0;
}

bool WebCaller::Test() {
	LogInfo("START");

	// Test Timer Is Running
	Timer timer;

	// Instance 'Test' Web Caller
	WebCaller wc1("Test1", false, 5);
	WebCaller wc2("Test2", true, 1);
	WebCaller wc3("Test3", false, 100);
	WebCallerLogInstances();

	// Get 'DoesNotExist' Web Caller Instance
	WebCaller* pWC;
	pWC = WebCaller::GetInstance("DoesNotExist");
	if (pWC) {
		LogError("WebCaller::GetInstance('DoesNotExist') FAILED");
		return false;
	}

	// Get 'Test1' Web Caller & Enable
	pWC = WebCaller::GetInstance("Test1");
	if (!pWC) {
		LogError("WebCaller::GetInstance('Test1') FAILED");
		return false;
	}
	pWC->Enable(true);

	// Test Asynchronous Calls On 'Test1' (yes response verbose none)
	testCallbacks = 0;
	WebCallActPtr pWCA[DRF_WEB_CALLER_TEST_CALLS];
	for (size_t i = 0; i < DRF_WEB_CALLER_TEST_CALLS; ++i) {
		WebCallOpt wcoGoogle;
		wcoGoogle.url = "http://www.google.com";
		wcoGoogle.response = true;
		wcoGoogle.verbose = WC_VERBOSE_NONE; // logged in callback
		wcoGoogle.cbFunc = WebCallerTestCallback;

		// Do Web Call (no blocking + response + WebCallerTestCallback)
		pWCA[i] = pWC->Call(wcoGoogle);
	}

	// Wait For All Web Calls To Complete
	if (!pWC->Wait()) {
		LogError("WebCaller::Wait() FAILED");
		return false;
	}

	// Check For No Calls Active
	size_t calls = pWC->CallsActive();
	if (calls != 0) {
		LogError("WebCaller::CallsActive() " << calls << " != 0");
		return false;
	}

	// Check For All Calls Ok
	for (size_t i = 0; i < DRF_WEB_CALLER_TEST_CALLS; ++i) {
		if (!pWCA[i]->IsOk()) {
			LogError("WebCaller::Call() FAILED");
			return false;
		}
	}

	// Get 'Test2' Web Caller
	pWC = WebCaller::GetInstance("Test2");
	if (!pWC) {
		LogError("WebCaller::GetInstance('Test2') FAILED");
		return false;
	}

	// Test Synchronous Call On 'Test2' (no response)
	WebCallOpt wcoYahoo;
	wcoYahoo.url = "http://www.yahoo.com";
	wcoYahoo.response = false;

	// Do Web Call (blocking + no response + response log)
	WebCallActPtr pWCA2 = pWC->CallAndWait(wcoYahoo);
	if (!pWCA2->IsOk()) {
		LogError("WebCaller::CallAndWait() FAILED");
		return false;
	}

	// Check All Asynchronous Callbacks Actually Executed
	if (testCallbacks != DRF_WEB_CALLER_TEST_CALLS) {
		LogError("testCallbacks " << testCallbacks << " != " << DRF_WEB_CALLER_TEST_CALLS);
		return false;
	}

	// Disable All Instances
	if (!WebCaller::DisableInstances()) {
		LogError("WebCaller::DisableInstances() FAILED");
		return false;
	}

	LogInfo("END - PASSED (" << WC_FMT_TIME << timer.ElapsedMs() << "ms)");
	return true;
}

// TODO - Testing Post To Translate Service
// https://cxc-languagecloud.sdl.com/#app=languagecloud&entry=home
WebCallActPtr WebCaller::PostAndWait(const WebCallOpt& wco, const std::string& header, const std::string& data) {
	static char* accept[2] = { "*/*", NULL };

	// Parse Params
	bool secure = wco.secure;
	std::string http = StrStripRightFirst(wco.url, ":");
	if (StrSameIgnoreCase(http, "https"))
		secure = true;
	INTERNET_PORT port = secure ? INTERNET_DEFAULT_HTTPS_PORT : INTERNET_DEFAULT_HTTP_PORT;
	DWORD flags = secure ? INTERNET_FLAG_SECURE : 0;
	std::string action = StrStripLeftLast(wco.url, "/");
	std::string server = StrStripRightLast(wco.url, "/");
	server = StrStripLeftLast(server, "/");

	// New Web Call ACT
	WebCallActPtrNew(pWCA, wco);

	// for clarity, error-checking has been removed
	HINTERNET hSession = ::InternetOpenA("WebCaller",
		INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
	HINTERNET hConnect = ::InternetConnectA(hSession, server.c_str(),
		port, NULL, NULL, INTERNET_SERVICE_HTTP, 0, 1);
	HINTERNET hRequest = ::HttpOpenRequestA(hConnect, "POST",
		action.c_str(), NULL, NULL, (LPCSTR*)accept, flags, 1);
	if (!::HttpSendRequestA(hRequest, header.c_str(), header.size(), (void*)data.c_str(), data.size())) {
		pWCA->state = WebCallAct::STATE_RESPONSE_ERROR;
		WebCallInternetGetLastError(pWCA->errCode, pWCA->errStr);
		LogError("HttpSendRequest() FAILED - errCode=" << pWCA->errCode << " '" << pWCA->errStr << "'");
	} else {
		char rxBuf[1025] = "";
		DWORD rxChars = 0;
		while (::InternetReadFile(hRequest, rxBuf, sizeof(rxBuf) - 1, &rxChars) && rxChars) {
			rxBuf[rxChars] = 0;
			pWCA->responseChars.insert(pWCA->responseChars.end(), rxBuf, rxBuf + rxChars);
			rxChars = 0;
		}
		pWCA->state = WebCallAct::STATE_RESPONSE_OK;
		//		LogWarn("OK - '" << pWCA->ResponseStr() << "'");
	}
	pWCA->SetComplete();

	// close any valid internet-handles
	::InternetCloseHandle(hRequest);
	::InternetCloseHandle(hConnect);
	::InternetCloseHandle(hSession);

	return pWCA;
}

///////////////////////////////////////////////////////////////////////////////
// GetBrowserPage Functions
///////////////////////////////////////////////////////////////////////////////

static bool WC_GetBrowserPage(
	const std::string& url,
	const std::string& resultFilePath,
	std::string* pResultText,
	size_t& resultSize,
	TimeMs& resultTimeMs,
	DWORD& httpStatusCode,
	std::string& httpStatusDesc,
	TimeMs timeoutMs) {
	// Clear Return Values
	resultSize = 0;
	resultTimeMs = 0.0;
	httpStatusCode = 0;
	httpStatusDesc.clear();
	if (pResultText)
		pResultText->clear();

	// Set Web Call Options (blocking + response)
	WebCallOpt wco;
	wco.url = url;
	wco.response = true;
	wco.responseFilePath = resultFilePath;
	wco.timeoutMs = timeoutMs;

	// Retry Web Call On Failure
	const size_t retries = 3;
	for (size_t i = 0; i < retries; ++i) {
		// Non-Blocking Web Call
		auto pWCA = WebCaller::WebCall(WEB_CALLER_GET_BROWSER_PAGE, wco);
		if (!pWCA)
			continue;

		// Legacy Timeout & Windows Message Loop
		FOREVER {
			// Web Call Complete Or Timeout ?
			if (pWCA->IsComplete() || pWCA->IsTimeout())
				break;

			// Pump Messages Until Empty (1 messages max)
			if (!::MsgProcPumpUntilEmpty(1)) {
				// Quit App
				httpStatusCode = 500;
				httpStatusDesc = "MsgProcPumpUntilEmpty() FAILED - Posting Quit Message!";
				LogFatal(httpStatusDesc);
				::PostQuitMessage(0);
				return false;
			}

			// Allow Thread To Pre-empt
			fTime::SleepMs(1);
		}

		// Web Call Complete ?
		if (pWCA->IsComplete()) {
			// Get Return Values
			DWORD status = pWCA->StatusCode();
			httpStatusCode = status;
			httpStatusDesc = pWCA->StatusStr();
			resultSize = pWCA->ResponseSize();
			resultTimeMs = pWCA->ResponseTimeMs();

			// Success ?
			if (status == HTTP_STATUS_OK) {
				// Success - Get Result Text & Return OK
				if (pResultText)
					*pResultText = pWCA->ResponseStr();

				return true;
			}
		}
	}

	return false;
}

bool GetBrowserPage(GBPO& gbpo) {
	return (gbpo.resultFile.empty() ?
				GetBrowserPage(gbpo.url, gbpo.resultText, gbpo.resultSize, gbpo.resultTimeMs, gbpo.httpStatusCode, gbpo.httpStatusDesc, gbpo.timeoutMs) :
				GetBrowserPageToFile(gbpo.url, gbpo.resultFile, gbpo.resultSize, gbpo.resultTimeMs, gbpo.httpStatusCode, gbpo.httpStatusDesc, gbpo.timeoutMs));
}

bool GetBrowserPageToFile(const std::string& url, const std::string& filePath, size_t& resultSize, TimeMs& resultTimeMs, DWORD& httpStatusCode, std::string& httpStatusDesc, TimeMs timeoutMs) {
	if (filePath.empty())
		return false;
	return WC_GetBrowserPage(url, filePath, NULL, resultSize, resultTimeMs, httpStatusCode, httpStatusDesc, timeoutMs);
}

bool GetBrowserPageToFile(const std::string& url, const std::string& filePath, size_t& resultSize, DWORD& httpStatusCode, std::string& httpStatusDesc, TimeMs timeoutMs) {
	TimeMs resultTimeMs;
	return GetBrowserPageToFile(url, filePath, resultSize, resultTimeMs, httpStatusCode, httpStatusDesc, timeoutMs);
}

bool GetBrowserPage(const std::string& url, std::string& resultText, size_t& resultSize, TimeMs& resultTimeMs, DWORD& httpStatusCode, std::string& httpStatusDesc, TimeMs timeoutMs) {
	return WC_GetBrowserPage(url, "", &resultText, resultSize, resultTimeMs, httpStatusCode, httpStatusDesc, timeoutMs);
}

bool GetBrowserPage(const std::string& url, std::string& resultText, size_t& resultSize, DWORD& httpStatusCode, std::string& httpStatusDesc, TimeMs timeoutMs) {
	TimeMs resultTimeMs;
	return GetBrowserPage(url, resultText, resultSize, resultTimeMs, httpStatusCode, httpStatusDesc, timeoutMs);
}

} // namespace KEP