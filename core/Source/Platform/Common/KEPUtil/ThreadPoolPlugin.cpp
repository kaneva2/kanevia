
///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Common/KEPUtil/ThreadPoolPlugin.h"

using namespace log4cplus;

namespace KEP {

// Plugin support
ThreadPoolPluginFactory::ThreadPoolPluginFactory() :
		AbstractPluginFactory("ThreadPool"), _config(0) {
}

ThreadPoolPluginFactory::ThreadPoolPluginFactory(KEPConfig* config) :
		AbstractPluginFactory("ThreadPool"), _config(config) {
}

ThreadPoolPluginFactory::~ThreadPoolPluginFactory() {}

const std::string& ThreadPoolPluginFactory::name() const {
	return AbstractPluginFactory::name();
}

/**
 *
 */
PluginPtr ThreadPoolPluginFactory::createPlugin(const TIXMLConfig::Item& configItem) const {
	LOG4CPLUS_TRACE(Logger::getInstance(L"ThreadPool"), "ThreadPoolPluginFactory::createPlugin()");
	TiXmlElement* config = configItem._internal;
	// Guard clause.  If no configuration provided, we can go with
	// default settings.  We're done here.
	//
	if (config == 0)
		return PluginPtr(new ThreadPoolPlugin(*(new ConcreteThreadPool(5, std::string("unnamed")))));

	int i;
	const char* p = config->Attribute("name");
	std::string name(p == 0 ? "unnamed" : p);

	p = config->Attribute("size");
	size_t size = (p == 0) ? 5 : atoi(p);

	// Thread pool attriibutes
	//
	// number of threads
	// pool name
	// thread priority
	//		enum jsThdPriority
	//		{
	//			PR_LOWEST,
	//			PR_LOW,
	//			PR_NORMAL,
	//			PR_HIGH,
	//			PR_HIGHEST
	//		};
	p = config->Attribute("priority");
	jsThdPriority priority = PR_NORMAL;
	i = (p == 0) ? PR_NORMAL : atoi(p); // there is a hole here.  if non-digit string passed, will get low priority thread
		// fix rename parseNumber<> routines to be makeString<>
		// implement new parseNumber<> that actuall parses a string and changes it to an numeric.
	switch (i) {
		case 0: priority = PR_LOWEST; break;
		case 1: priority = PR_LOW; break;
		case 2: priority = PR_NORMAL; break;
		case 3: priority = PR_HIGH; break;
		case 4: priority = PR_HIGHEST; break;
		default: priority = PR_NORMAL;
	};

	p = config->Attribute("threadtype");
	jsThreadType type = TT_NORMAL;
	i = (p == 0) ? 0 : atoi(p);
	switch (i) {
		case 0: type = TT_NORMAL; break;
		case 1: type = TT_COOPERATIVE; break;
		default: type = TT_NORMAL; break;
	};

	p = config->Attribute("shutdownmode");
	i = (p == 0) ? ThreadPool::hurried : atoi(p);
	ThreadPool::ShutdownMode mode = ThreadPool::hurried;
	switch (i) {
		case 0: mode = ThreadPool::hurried; break;
		case 1: mode = ThreadPool::ponderous; break;
		case 2: mode = ThreadPool::patient; break;
		case 3: mode = ThreadPool::hurried; break;
		case 4: mode = ThreadPool::murderous; break;
		default: mode = ThreadPool::hurried; break;
	};

	return PluginPtr(new ThreadPoolPlugin(*(new ConcreteThreadPool(size, name, priority, type))));
}

} // namespace KEP