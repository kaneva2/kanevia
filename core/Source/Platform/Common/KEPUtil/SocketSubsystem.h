///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Windows.h>
#include "Common/include/KEPCommon.h"

#include "Core/Util/ChainedException.h"
#include "Core\Util\ErrorSpec.h"

#define ERR_SIZE 512
#define IDS_SOCKSYS_INCOMPATIBLE_WINSOCK_VERSION 3

namespace KEP {

/**
 * Much of the socket API is premised on the pattern of:
 *   1) Perform Socket operation.
 *   2) Call WSAGetLastError() to find out if the operation was successful
 *      and if not, why?
 *   3) If error occurred, dig up error texgt for the error.
 *
 * This class simply allows these two return values (error code and error message) to 
 * be passed back as a single return value (as opposed to using output parameters.
 *
 * Note that generally an error code is interpreted as "no error" though this can
 * very depending the application.
 */
__declspec(dllexport) std::string WSAErrStr(int WSAErr, HINSTANCE hInst = 0);

__declspec(dllexport) const ErrorSpec getError(unsigned long id);
_declspec(dllexport) const ErrorSpec getError();

CHAINED(NetworkException);

/**
 * This class exists almost exclusively for the purpose of unit testing.
 * Prior to this class's existence, management of the socket subsystem
 * was the purview of the Mailer's constructor and destructor.
 *
 * Further refactoring and testing reveals that the start/stop of the socket 
 * subsystem cannot be left to the Mailer as we don't want the subsystem being 
 * shutdown prematurely when operating within a larger context (e.g. wok).
 *
 * Never the less, this system must be managed when performing unit testing.
 * Simply place an instance of this class on the stack prior to testing any
 * mail functions.  When it goes off the stack, the subsystem will be 
 * shutdown.
 */
class __declspec(dllexport) SocketSubSystem {
public:
	SocketSubSystem();
	virtual ~SocketSubSystem();
	static std::string SocketSubSystem::hostName();

private:
	static std::string _hostName;
};

} // namespace KEP