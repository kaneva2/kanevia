///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "plugin.h"
#include "common/include/KEPConfig.h"

using namespace log4cplus;

namespace KEP {

/////////////// Plugin Host Implementation ////////////////////////////////////////////

PluginLibrary::PluginLibrary() :
		_name("<unnamed>"), _handle(0), _factoryMap() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginLibrary::PluginLibrary(" << _name << ")");
}

PluginLibrary::PluginLibrary(const std::string& name) :
		_name(name), _handle(0), _factoryMap() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginLibrary::PluginLibrary(" << _name << ")");
}

PluginLibrary::~PluginLibrary() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginLibrary::~PluginLibrary(" << _name << ")");
	unload();
}

PluginLibrary&
PluginLibrary::map(PluginFactoryMap& acquisition) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginLibrary::map(" << _name << ")");
	for (PluginFactoryMap::iterator i = _factoryMap.begin(); i != _factoryMap.end(); i++) {
#ifdef USE_FACTORYHOLDER
		acquisition[i->first] = new FactoryRefHolder(*(i->second));
#else
		acquisition[i->first] = i->second;
#endif
	}
	return *this;
}

PluginLibrary&
PluginLibrary::unmap(PluginFactoryMap& acquisition) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginLibrary::unmap(" << _name << ")");
	for (PluginFactoryMap::iterator i = _factoryMap.begin(); i != _factoryMap.end(); i++) {
		acquisition.erase(i->first);
	}
	return *this;
}

PluginLibrary&
PluginLibrary::load(const std::string& filename) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginLibrary::load(" << filename << "):" << this);
	_handle = ::LoadLibraryW(Utf8ToUtf16(filename).c_str());
	if (0 == _handle) {
		DWORD eno = ::GetLastError();
		throw PluginException(SOURCELOCATION,
			ErrorSpec(eno) << "Error: " << filename << " doesn't load [" << eno << "][" << OS::errorNumToString(eno) << "]");
	}

	typedef void (*PluginEnumFunc)(PluginFactoryVecPtr);
	PluginEnumFunc enumFunc = (PluginEnumFunc)::GetProcAddress(_handle, "EnumeratePlugins");

	if (!enumFunc) {
		// Doesn't implement Create()
		char zstr[255];
		sprintf_s(zstr, _countof(zstr), "Error: %s doesn't export FactoryVecPtr EnumeratePlugins().  This plugin library will not be loaded.", filename.c_str());
		::FreeLibrary(_handle);
		_handle = 0;
		throw PluginException(SOURCELOCATION, zstr);
	}
	PluginFactoryVecPtr factoryVec(new PluginFactoryVec());
	enumFunc(factoryVec);
	// Add references to the map
	//
	for (PluginFactoryVec::iterator iter = factoryVec->begin(); iter != factoryVec->end(); iter++) {
#ifdef USE_FACTORYHOLDER
		addPluginFactory((*iter)->getRef());
#else
		addPluginFactory(*iter);
#endif
	}
	_name = filename;
	return *this;
}

PluginLibrary&
PluginLibrary::unload() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginLibrary::unload(" << _name << ")");
	if (_factoryMap.size() > 0)
		_factoryMap.clear();
	if (_handle == 0)
		return *this;
	::FreeLibrary(_handle);
	_handle = 0;
	return *this;
}

PluginLibrary&
PluginLibrary::addPluginFactory(const PluginFactoryPtr& factory) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginLibrary::addPluginFactory(" << factory->name() << ")");
	const std::string& n = factory->name();
#ifdef USE_FACTORYHOLDER
	_factoryMap.insert(std::pair<std::string, FactoryRefHolder*>(n, new FactoryRefHolder(factory)));
#else
	_factoryMap.insert(std::pair<std::string, PluginFactoryPtr>(n, factory));
#endif
	return *this;
}

PluginFactoryPtr
PluginLibrary::getPluginFactory(const std::string& name) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginLibrary::getPluginFactory(" << name << ")");
	PluginFactoryMap::iterator iter = _factoryMap.find(name);
#ifdef USE_FACTORYHOLDER
	if (iter != _factoryMap.end())
		return iter->second->getRef().operator->();
#else
	if (iter != _factoryMap.end())
		return iter->second;
#endif
	return PluginFactoryPtr((PluginFactory*)0);
}

const std::string& PluginLibrary::name() const {
	return _name;
}

PluginLibrary&
PluginLibrary::removePluginFactory(const std::string& name) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginLibrary::removePluginFactory(" << name << ")");
	_factoryMap.erase(name);
	return *this;
}

/////////////// Plugin Host Implementation ////////////////////////////////////////////
Singleton<PluginHost> PluginHost::_singleton; //(/*"PluginHost"*/);
PluginHost& PluginHost::singleton() {
	return *_singleton;
}

PluginHost::PluginHost() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginHost::PluginHost()");
}

PluginHost::~PluginHost() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginHost::~PluginHost()");
	shutdown();
}

PluginHost&
PluginHost::loadLibrary(const std::string& path) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginHost::loadLibrary(" << path << ")");
	// todo: implement helper function to PluginHost consolidate logic required to clean up
	// this allocation should the load fail...
	// ref count ptr please.
	PluginLibraryPtr plib(new PluginLibrary());

	//	try {
	plib->load(path); // Load it.
	addPluginLibrary(plib);
	//	}
	//	catch( const PluginException& pe ) {
	//		delete plib;
	//		throw pe;
	//	}
	return *this;
}

ACTPtr PluginHost::shutdown() {
	if (_pluginLibraries.size() == 0) {
		ACT* p = new ACT();
		p->complete();
		return ACTPtr(p);
	}

	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginHost::shutdown()");

	// Move this logic out to a shutdown strategy class.
	// A companion to the logic contained in the configurator
	// So we can vary the behavior.
	//
	// Shut down in lifo fashion
	LOG4CPLUS_DEBUG(Logger::getInstance(L"Plugin"), "PluginHost::shutdown(): Unloading plugins");
	while (_shutdown.size() > 0) {
		// N.B.:  The use of the PluginHost in dynamic fashion will break this in the event that the client does what it should
		// and cleans up.
		//
		// For example, the HostLeaseManager plugin that is contained in the AppIncubator plugin library dynamically
		// loads an AppController proxy plugin whenever a new AppHost comes up.  If and when the AppHost goes away
		// the AppHost's lease to an entry in the registry times out and the proxy plugin is removed.  This absolutely
		// valid usage and Plugin architecture was intended support the ability to extend the server.
		//
		// However, the initial implementation was based on a static usage where the clients of the PluginHost didn't need
		// to manage how plugins were loaded and unloaded while respecting Plugin dependencies. Specifically, PluginHost
		// elects to NOT evaluate Plugin dependencies and assume that the configuration order was correct, then on shutdown
		// unload the Plugins in reverse order.  Hence the _shutdown vector.
		//
		// Dynamic usage corrupts this leaving references in the shut down vector.  The final solution to this problem
		// is still under consideration, but, we can least insure that we don't crash by simply warning about missing
		// plugins referenced in the shutdown list.  Hence the following try catch
		//
		PluginPtr plugin = _shutdown.top();
		LOG4CPLUS_DEBUG(Logger::getInstance(L"Plugin"), "PluginHost::shutdown(): Unloading plugin [" << plugin->name() << "]");
		try {
			stopPlugin(plugin->name());
			tearDownPlugin(plugin->name());
			unmountPlugin(plugin->name());
		} catch (const PluginException& pe) {
			LOG4CPLUS_ERROR(Logger::getInstance(L"Plugin"), pe.str(););
		}
		_shutdown.pop();
	}

	LOG4CPLUS_DEBUG(Logger::getInstance(L"Plugin"), "PluginHost::shutdown(): Clearing shutdown plan");

	// Unload all of our dynamic libraries
	//
	LOG4CPLUS_DEBUG(Logger::getInstance(L"Plugin"), "PluginHost::shutdown(): Unloading libraries");
	for (PluginLibraryMap::iterator iter = _pluginLibraries.begin(); iter != _pluginLibraries.end(); iter++) {
		LOG4CPLUS_DEBUG(Logger::getInstance(L"Plugin"), "PluginHost::shutdown(): Unmapping [" << iter->second->name() << "]");

		// Tell the library to remove it's entries from the list of available plugins
		// Then unload the library
		//
		iter->second->unmap(_availablePlugins)
			.unload();
	}
	_pluginLibraries.clear();

	// need to feed this off of the ACTs that come from each serice stop
	ACT* p = new ACT();
	p->complete();
	return ACTPtr(p);
}

PluginHost&
PluginHost::mountPlugin(const std::string& factoryName, const std::string& instanceName, const TIXMLConfig::Item* config) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginHost::mountPlugin(" << factoryName << "):" << this);

	std::string linstanceName = (instanceName.length() == 0) ? factoryName : instanceName;

	PluginMap::iterator mountediter = _mountedPlugins.find(linstanceName);
	if (mountediter != _mountedPlugins.end())
		return *this; // already mounted...do nothing

	PluginFactoryMap::iterator iter = _availablePlugins.find(factoryName);
	if (iter == _availablePlugins.end()) {
		std::stringstream ss;
		ss << "PluginHost::mountPlugin(): Plugin factory [" << factoryName << "] not found!";
		throw PluginException(SOURCELOCATION, ss.str().c_str());
	}

#ifdef USE_FACTORYHOLDER
	PluginFactoryPtr pf = iter->second->getRef().operator->();
#else
	PluginFactoryPtr pf = iter->second;
#endif
	PluginPtr pp = pf->createPlugin(*config);

	// Need to create a wrapper for the plugin that aggregates
	// metadata about the plugin:
	//		1)  The plugin instance
	//		2)	The plugin interface name
	//		3)  The plugin instance name
	//		4)  Plugin Runtime Id (RId)
	// Place this in the map of mounted plugins.
	//
	_mountedPlugins[linstanceName] = pp;

	// Push this on to our shutdown list.
	// Primitive mechanism for avoiding prereq collisions when shutting down.
	//
	_shutdown.push(pp);

	// Finally, bind the plugin to this host.
	pp->bind(*this);

	return *this;
	}

PluginHost&
PluginHost::unmountPlugin(const std::string& name) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginHost::unmountPlugin(" << name << ")");
	LOG4CPLUS_DEBUG(Logger::getInstance(L"Plugin"), "PluginHost::unmountPlugin(" << name << "): [" << _mountedPlugins.size() << "] plugin(s) remaining!");

	PluginMap::iterator iter = _mountedPlugins.find(name);
	if (iter == _mountedPlugins.end()) {
		LOG4CPLUS_WARN(Logger::getInstance(L"Plugin"), "PluginHost::unmountPluin(" << name << "): Plugin Not Found!");
		return *this;
	}
	iter->second->unbind(*this);
	_mountedPlugins.erase(iter);
	LOG4CPLUS_DEBUG(Logger::getInstance(L"Plugin"), "PluginHost::unmountPlugin(" << name << "): Complete. [" << _mountedPlugins.size() << "] plugin(s) remaining!");
	return *this;
}

PluginHost&
PluginHost::setUpPlugin(const std::string& name) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginHost::setUpPlugin(" << name << ")");
	PluginMap::iterator iter = _mountedPlugins.find(name);
	if (iter == _mountedPlugins.end())
		return *this;
	iter->second->setUp();
	return *this;
}

PluginHost&
PluginHost::startPlugin(const std::string& name) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginHost::startPlugin(" << name << ")");
	PluginMap::iterator iter = _mountedPlugins.find(name);
	if (iter == _mountedPlugins.end())
		return *this;
	iter->second->startPlugin();
	return *this;
}

ACTPtr
PluginHost::stopPlugin(const std::string& name) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginHost::stopPlugin(" << name << ")");
	PluginMap::iterator iter = _mountedPlugins.find(name);
	if (iter == _mountedPlugins.end()) {
		std::stringstream ss;
		ss << "Plugin not found [" << name << "]";
		throw PluginException(SOURCELOCATION, ss.str());
	}
	return iter->second->stopPlugin();
}

PluginHost&
PluginHost::tearDownPlugin(const std::string& name) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginHost::tearDownPlugin(" << name << ")");
	PluginMap::iterator iter = _mountedPlugins.find(name);
	if (iter == _mountedPlugins.end())
		return *this;
	iter->second->tearDown();
	return *this;
}

PluginHost&
PluginHost::addPluginLibrary(PluginLibraryPtr library) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginHost::addPluginLibrary(" << library->name() << ")");
	_pluginLibraries[library->name()] = library;
	library->map(_availablePlugins);
	return *this;
}

PluginHost&
PluginHost::loadPluginLibrary(const std::string& pluginPath) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginHost::loadPluginLibrary(" << pluginPath << ")");
	PluginLibraryPtr plib(new PluginLibrary());

	// throws PluginException
	plib->load(pluginPath);
	addPluginLibrary(plib);
	return *this;
}

PluginHost& PluginHost::addListener(PluginHostListener& listener) { /* todo: implement */
	return *this;
}
PluginHost& PluginHost::removeListener(PluginHostListener& listener) { /* todo: implement */
	return *this;
}
PluginPtr PluginHost::getPlugin(const std::string& name, unsigned int version) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginHost::getPlugin(" << name << "):" << this);

	PluginMap::iterator iter = _mountedPlugins.find(name);
	if (iter != _mountedPlugins.end())
		return iter->second;
	return PluginPtr((Plugin*)0);
}

PluginPtr PluginHost::getPluginByInterfaceName(const std::string& interfaceName) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Plugin"), "PluginHost::getPluginByInterface(" << interfaceName << "):" << this);

	PluginMap::iterator iter = _mountedPlugins.begin();
	while (iter != _mountedPlugins.end()) {
		if (iter->second->supportsInterface(interfaceName))
			return iter->second;

		++iter;
	}
	return PluginPtr((Plugin*)0);
}

PluginHost& PluginHost::firePluginLoaded() { /* todo: implement */
	return *this;
}
PluginHost& PluginHost::firePluginUnloaded() { /* todo: implement */
	return *this;
}

Monitored::DataPtr
PluginHost::monitor() const {
	BasicMonitorData* p = new BasicMonitorData();
	p->ss << "<PluginHost>";
	for (PluginMap::const_iterator iter = _mountedPlugins.begin(); iter != _mountedPlugins.end(); iter++) {
		p->ss << iter->second->monitor()->toString();
	}
	p->ss << "</PluginHost>";
	return Monitored::DataPtr(p);
}

// AbstractPlugin
//
AbstractPlugin::AbstractPlugin(const std::string& name) :
	_name(name), _host(0) {}

AbstractPlugin::AbstractPlugin(const AbstractPlugin& tocopy) :
	_name(tocopy._name), _host(tocopy._host) {}

AbstractPlugin::~AbstractPlugin() {}

AbstractPlugin& AbstractPlugin::operator=(const AbstractPlugin& rhs) {
	Plugin::operator=(rhs);
	_name = rhs._name;
	_host = rhs._host;
	return *this;
}

const std::string& AbstractPlugin::name() const {
	return _name;
}
unsigned int AbstractPlugin::version() const {
	return 0;
}

/**
 * Acquire any resources required to run the plugin.
 * Any configuration of the plugin should have already occurred
 */
Plugin& AbstractPlugin::setUp() {
	return *this;
}
Plugin& AbstractPlugin::tearDown() {
	return *this;
}
Plugin& AbstractPlugin::startPlugin() {
	return *this;
}
ACTPtr AbstractPlugin::stopPlugin() {
	ACT* act = new ACT();
	act->complete();
	return ACTPtr(act);
}

Plugin& AbstractPlugin::addListener(PluginListener& listener) { /* todo:  implement me. */
	return *this;
}
Plugin& AbstractPlugin::removeListener(PluginListener& listener) { /* todo: implement me. */
	return *this;
}
Plugin& AbstractPlugin::bind(PluginHost& pluginHost) {
	_host = &pluginHost;
	firePluginBound(); // Alert any interested parties that this plugin has been bound to the plugin stack.
	return *this;
}

Plugin& AbstractPlugin::unbind(PluginHost& pluginHost) {
	_host = 0;
	firePluginUnbound(); // Alert any interested parties that this plugin has been unbound from the plugin stack.
	return *this;
}

/**
 * Obtain the host of this plugin.
 * @return pointer to PluginHost if bound, else null.
 */
PluginHost* AbstractPlugin::host() const {
	return _host;
}

Plugin& AbstractPlugin::firePluginStartComplete() { /* todo: implement */
	return *this;
}
Plugin& AbstractPlugin::firePluginSetupComplete() { /* todo: implement */
	return *this;
}
Plugin& AbstractPlugin::firePluginBound() { /* todo: implement */
	return *this;
}
Plugin& AbstractPlugin::firePluginUnbound() { /* todo: implement */
	return *this;
}

Monitored::DataPtr
AbstractPlugin::monitor() const {
	BasicMonitorData* p = new BasicMonitorData();
	p->ss << "<Plugin name=\"" << name() << "\"/>";
	return Monitored::DataPtr(p);
}

std::ostream&
PluginConfig::streamTo(std::ostream& out) const {
	out << "<Plugin factory_name=\"" << _factoryName << "\"";
	if (_pluginName.length() > 0)
		out << " name=\"" << _pluginName << "\"";
	out << ">";
	out << "<PluginConfig";
	if (_attributes.size() > 0) {
		for (auto i = _attributes.cbegin(); i != _attributes.cend(); i++) {
			out << " " << i->first << "=\"" << i->second << "\"";
		}
	}
	std::string b = _blob1.str();
	if (b.length() > 0)
		out << ">" << b << "</PluginConfig></Plugin>";
	else
		out << "/></Plugin>";

	return out;
}

std::ostream& operator<<(std::ostream& out, const PluginConfig& config) {
	return config.streamTo(out);
}

} // namespace KEP