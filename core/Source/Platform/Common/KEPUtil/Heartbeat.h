///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <set>
#include "Core/Util/Runnable.h"
#include "Core/Util/Executor.h"
#include "Common/KEPUtil/Heartbeat.h"
#include "Common/KEPUtil/Plugin.h"
#include "Common/include/kepconfig.h"
#include "Common/KEPUtil/CountDownLatch.h"

class TiXmlElement;

namespace KEP {

/**
 * Interface definition for consumers of heartbeats.
 * 
 * The business end of an HeartbeatGenerator is the HeartbeatListener.  The
 * point of the HeartbeatListener is to encapsulate the logic required to 
 * generate an heartbeat on a schedule basis and dispatch that heartbeat
 * to consumers of the heartbeat.  Once that algorithm exists, there is 
 * little need to mess with the generator.
 */
class __declspec(dllexport) HeartbeatListener {
public:
	/**
	 * Default destuctor
	 */
	HeartbeatListener() {}

	/**
	 * Copy constructor
	 */
	HeartbeatListener(const HeartbeatListener& /* other */) {}

	/**
	 * dtor
	 */
	virtual ~HeartbeatListener() {}

	/**
	 * Assignment operator
	 */
	HeartbeatListener& operator=(const HeartbeatListener& /* rhs */) { return *this; }

	/**
	 * Consumers of heartbeats must implement this interface and of particular
	 * importance is this method.
	 */
	virtual void thump() = 0;
};

/**
 * Interface of an HeartbeatGenerator.  
 *
 * An HeartbeatGenerator is a mechanism that allows for a client to receive an event
 * at some arbitrary interval.  This interface defines methods for adding and
 * removing listeners only.
 */
class __declspec(dllexport) HeartbeatGenerator {
public:
	HeartbeatGenerator() {}

	virtual HeartbeatGenerator& start() = 0;
	virtual HeartbeatGenerator& stop() = 0;

	/**
	 * Add a listener.
	 * @param listener   Pointer to an implementation of an HeartbeatListener.
	 * @returns reference to self
	 */
	virtual HeartbeatGenerator& addHeartbeatListener(HeartbeatListener* listener) = 0;

	/**
	 * Add a listener.
	 * @param listener   Pointer to an implementation of an HeartbeatListener.
	 * @returns reference to self
	 */
	virtual HeartbeatGenerator& removeHeartbeatListener(HeartbeatListener* listener) = 0;

private:
};

/**
 * Very simple heartbeat that make no warrantee as to the timeliness of the heartbeat.
 *
 * If the frequency of the heartbeat is low enough, the listeners overhead is low enough
 * and the number of listeners is reasonable, this implementation will work just fine.
 * 
 * This implementation runs on a single dedicated thread, but note that instantiation and destruction, 
 * start and stop processing.  Until we have a motivation to make start() and stop()
 * explicit operations, let's not clutter up the interface.  Feel free to implement as a stop
 * and start to facilitate this potential change.
 */
class __declspec(dllexport) BasicHeartbeatGenerator : public HeartbeatGenerator {
public:
	/**
	 * @param	intervalMillis	Heartbeat frequency specified as the interval time,
	 *							in milliseconds, between heartbeats.
	 */
	BasicHeartbeatGenerator(unsigned long intervalMillis);

	/**
	 * Create an HeartbeatGenerator from an existing heartbeat generator
	 * Creates completely new generator with the same interval as the other
	 * HeartbeatGenerator.
	 */
	BasicHeartbeatGenerator(const BasicHeartbeatGenerator& other);

	/**
	 * dtor
	 */
	virtual ~BasicHeartbeatGenerator();

	/**
	 * Add a listener to this generator.
	 */
	HeartbeatGenerator& addHeartbeatListener(HeartbeatListener* listener);

	/**
	 * Remove a listener from this generator.
	 */
	HeartbeatGenerator& removeHeartbeatListener(HeartbeatListener* listener);

	/**
	 * Start the generator
	 */
	HeartbeatGenerator& start();

	/**
	 * Stop the generator.
	 */
	HeartbeatGenerator& stop();

private:
	/**
	 * Assigment operator is disabled
	 */
	BasicHeartbeatGenerator& operator=(const BasicHeartbeatGenerator& other);

	typedef std::set<HeartbeatListener*> SetT; // short hand typedef
	friend class MyRunnable; // Let this classes specialized Runnable instance
		// access the generators internals

	/**
	 * Calls thump() method of all current listeners of this generator.
	 */
	void fire();

	unsigned long _intervalMillis;
	MyRunnable* _runnable;
	bool _running;
	jsThreadExecutor _executor;
	SetT _listeners;
	fast_recursive_mutex _sync;
	ExecutablePtr _ep;
	CountDownLatch _latch;
};

/**
 * More complicated heartbeat that makes limited warranty as to the timeliness of the
 * heartbeat.  Specifically:
 *
 * 1) The actual frequency fluctuage due to the overhead of firing the listener event for
 * one or more listeners.  In most cases this fluctuation should be negligible.
 *
 * 2) If the heartbeat processing cost is higher than the frequency of the heartbeat,
 *	  your listeners actions will start to backlog.  Implementation is such that the
 *    subsequent heartbeat will arrive immediately after you listener has finished
 *    running.
 */
class AsyncronousHeartbeateGenerator : public HeartbeatGenerator {
};

class __declspec(dllexport) HeartbeatGeneratorPlugin
		: public HeartbeatGenerator
		,
		  public AbstractPlugin {
public:
	HeartbeatGeneratorPlugin(HeartbeatGenerator& impl) :
			_impl(impl), AbstractPlugin("Heartbeat") {}

	HeartbeatGeneratorPlugin(const HeartbeatGeneratorPlugin& other) :
			_impl(other._impl), AbstractPlugin("Heartbeat"), HeartbeatGenerator(other._impl) {
	}

	HeartbeatGenerator& operator=(const HeartbeatGeneratorPlugin& /* rhs */) {
		return *this;
	}

	virtual ~HeartbeatGeneratorPlugin() { delete (&_impl); }

	virtual HeartbeatGenerator& start() { return _impl.start(); }

	virtual HeartbeatGenerator& stop() { return _impl.stop(); }

	// Plugin support
	//
	const std::string& name() { return AbstractPlugin::name(); }

	unsigned int version() { return AbstractPlugin::version(); }

	Plugin& startPlugin() {
		_impl.start();
		return AbstractPlugin::startPlugin();
	}

	ACTPtr stopPlugin() {
		_impl.stop();
		return AbstractPlugin::stopPlugin();
	}

	HeartbeatGenerator& addHeartbeatListener(HeartbeatListener* hbl) {
		_impl.addHeartbeatListener(hbl);
		return *this;
	}

	HeartbeatGenerator& removeHeartbeatListener(HeartbeatListener* hbl) {
		_impl.removeHeartbeatListener(hbl);
		return *this;
	}

private:
	HeartbeatGenerator& _impl;
};

class __declspec(dllexport) HeartbeatGeneratorPluginFactory : public AbstractPluginFactory {
public:
	HeartbeatGeneratorPluginFactory();
	HeartbeatGeneratorPluginFactory(KEPConfig* config);
	virtual ~HeartbeatGeneratorPluginFactory();

	const std::string& name() const;
	PluginPtr createPlugin(const TIXMLConfig::Item& config) const;

private:
	KEPConfig* _config;
};

} // namespace KEP