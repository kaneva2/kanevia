///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

/******************************************************************************
 This file defines a framework for dynamically extending an application.  
 Extended functionality is bundled in Plugins.  These plugins can,
 dynamically, be instantiated, and placed into a PluginHost.  Once placed 
 in a PluginHost they can accessed by other plugins or any other
 component that has access to the PluginHost. The rest of this overview
 is organized by the dominant components of the framework.

 Plugins
 
 Plugins can provide an interface to components already statically linked
 into the application (e.g. HTTPServer, Mailer etc.)  Such plugins are
 considered to be static as, ultimately, their backing logic is always
 available.

 Plugins can also be implemented in external DLLs and loaded/unloaded
 dynamically.  These DLLs can contain 0 or more Plugin implementations.
 They need only implement the function EnumeratePlugins whose prototype
 can be found in this file. These plugins are considered to be dynamic 
 since their backing logic can disappear, i.e. is volatile.

 Plugin Libraries

 This concept of static v dynamic plugins impacts how plugins are
 packaged.  Because an external DLL can contain multiple Plugin
 implementations, we need to maintain a mapping between the DLL and the
 plugins contained in the dll.  In this way, we can insure that any plugins
 contained in a dll are removed from the system.  This functionality
 is required for proper shutdown of the application but also lends itselft
 to the ability to load/unload/reload plugins on demand.

 Note that a library of plugins, whether static or dynamic is actually
 a collection of plugin factories.  This allows us to defer instantiation
 of a plugin until it is actually needed.  It also allows for future 
 support for multiple instances of the same plugin.  For example, consider 
 a plugin that manages connections to a database.  It may configured to manage
 connections to database A while another plugin might be configured to
 maintain connections to database B.  Another example might be a Plugin
 that provides thread pooling services.  Once plugin instance might
 be configured for high priority threads, whilst another would be
 configured for low priority threads.

 PluginHost

 An instance of PluginHost maintains a collection of Plugin libraries 
 from which it can instantiate Plugin instances to be "mounted" to the host

 It provides methods for explicitly managing it's collection of libraries,
 managing actually plugin instances (e.g. setup/start/stop/teardown) and
 for acquiring references to the plugin instances being managed by the host.

 Typically there will be only one PluginHost per application, but this is
 not strictly enforced.

 Future Enhancements
 * Allow for multiple instances of a plugin to be loaded, with different
   configuration and identified by an instance name.
 

******************************************************************************/

#include <map>
#include <queue>
#include <stack>
#include <set>
#include "Core/Util/Monitored.h"
#include "Core/Util/ChainedException.h"
#include "Core/Util/Singleton.h"
#include "common/include/kepcommon.h"
#include "Core/Util/AsynchronousCompletionToken.h"
#include "Common/KEPUtil/ExtendedConfig.h"

#include <log4cplus/logger.h>

// forward references
class TiXmlElement;

namespace KEP {

// forward references
class PluginListener;
class PluginHostListener;
class PluginHost;

/**
 * Error handling is accomplished primarily through the use of exceptions.
 * All exceptions thrown by this package are of the following type.
 * As situations are identified that warrant a specialized PluginException
 * it will be derived from this class.
 *
 */
CHAINED(PluginException);

CHAINED_DERIVATIVE(PluginException, PluginNotFoundException);

/**
 * The stock and trade of this package is the management of Plugins.  Plugins 
 * can be thought of as logical services that are hosted with Kaneva applications.
 * The can be enabled/disabled, started/stopped in arbitrary fashion.
 * This allows for a common mechanism for dynamically extending the functionality 
 * of any application.
 *
 * This class sets forth the interface to be implemented by Plugins.
 *
 * The lifecycle of a Plugin is typically:
 *
 * <ol>
 *  <li>Startup
 *   <ol>
 *    <li>Instantiate</li>
 *    <li>Setup</li>
 *    <li>Start</li>
 *   </ol>
 *  </li>
 *  <li>Shutdown
 *   <ol>
 *    <li>Stop</li>
 *    <li>Teardown</li>
 *    <li>Destruct</li>
 *   </ol>
 *  </li>
 * </ol>
 */
class __declspec(dllexport) Plugin : public Monitored {
public:
	Plugin() {}
	Plugin(const Plugin& /* tocopy */) {}

	virtual ~Plugin() {}

	Plugin& operator=(const Plugin& /* tocopy */) { return *this; }

	/**
	 * Every plugin has a name by which it is identified.  Use this method to 
	 * query the plugin's name.
	 * @returns The name of the Plugin.
	 */
	virtual const std::string& name() const = 0;

	/**
	 * Every plugin has a version.  By default the version is 0.  Currently
	 * version() is not interpreted, i.e. it has no effect on the framework.
	 * @returns the version of the Plugin.
	 */
	virtual unsigned int version() const = 0;

	/**
	 * Return true if given interface name is supported by this plugin
	 */
	virtual bool supportsInterface(const std::string& interfaceName) const = 0;

	/**
	 * Called after the Plugin has been instantiated.  This is an opportunity for
	 * the plugin to perform any necessary setup, typically any resource acquisition,
	 * prior to being started.  For example, a Plugin that implements a datamodel
	 * may need to acquire a database connection.  This would be done in this
	 * method.
     *
     * Note that depending on the application, the existence of a plugin may or 
     * may not be critical, however, this framework does make this distinction.
	 * If your plugin requires a resource external to the plugin, e.g. another plugin
     * or a file etc, you should throw a ChainedException (or some derivative thereof)
     * Conversely, if your plugin uses an optional resource, external to the plugin,
     * you should not throw an exception.
     * 
	 * @returns		Reference to self.
     * @throws      ChainedException in the event that the plugin can be setUp, e.g.
     *              cannot acquire an external resource.
	 */
	virtual Plugin& setUp() = 0;

	/**
	 * Called after the Plugin has been stopped.  This is an opportunity for
	 * the plugin to perform any necessary teardown, typically any resource cleanup,
	 * prior to being destroyed.  For example, a Plugin that implements a datamodel
	 * may acquire a database connection during setup.  This connection would be 
	 * released in this method.
	 *
	 * @returns		Reference to self.
	 */
	virtual Plugin& tearDown() = 0;

	/**
	 * Start the plugin.  Many plugins are passive in that they don't actively do
	 * anything.  A Mailer would be a good example of a a passive plugin.  Until
	 * a client acquires the plugin and uses it to send email, the plugin is never
	 * on the stack.  Other plugins are active in that they may maintain a presence
	 * on the stack.  HTTPServer would be an example of an active Plugin.  Internally
	 * HTTPServer starts at least one thread to poll a configured port for incoming
	 * HTTP Requests.  HeartBeat would be another example of a passive plugin.  
	 * It starts a thread and then sleeps, periodically waking up just long enough
	 * to publish a heartbeat event.  
	 *
	 * Called after the Plugin has been setup, this method is where active plugins
	 * would start any asynchronous processing.
	 *
	 * @returns		Reference to self.
	 */
	virtual Plugin& startPlugin() = 0;

	/**
	 * The first call of the shutdown sequence of a Plugin, this method is used to
	 * stop, if applicable, any asynchronous processing being performed by the 
	 * Plugin.
	 */
	virtual ACTPtr stopPlugin() = 0;

	/**
	 * Subscribe to receive published Plugin events.
	 *
	 * @returns		Reference to self.
	 */
	virtual Plugin& addListener(PluginListener& listener) = 0;

	/**
	 * unsubscribe from receiving published Plugin events.
	 *
	 * @return		Reference to self.
	 */
	virtual Plugin& removeListener(PluginListener& listener) = 0;

	/**
	 * When an instance of a plugin is enabled, it is bound to the host.
	 * This allows it to inspect and listen to host.
	 */
	virtual Plugin& bind(PluginHost& pluginHost) = 0;

	/**
	 * When an instance of a plugin is disabled, it is unbound from the host.
	 */
	virtual Plugin& unbind(PluginHost& pluginHost) = 0;

	/**
     * @see Monitored
     */
	virtual Monitored::DataPtr monitor() const = 0;
};

typedef std::shared_ptr<Plugin> PluginPtr;
typedef std::map<std::string, PluginPtr> PluginMap;
typedef std::shared_ptr<PluginMap> PluginMapPtr;
typedef std::stack<PluginPtr> PluginStack;

/**
 * Like other classes in this package, state change in various component
 * instances (i.e. plugins, libraries, plugin hosts) are handled via a 
 * subscribe/unsubscribe model similar to Javas event model.
 *
 * This class sets forth the interface for listening to Plugin state
 * change events.
 */
class PluginListener {
public:
	/**
	 * Called when the plugin has completed it's setup
	 * 
	 * @param	plugin			The plugin that was setup.
	 */
	virtual PluginListener& pluginSetupComplete(Plugin& plugin) = 0;

	/**
	 * Called when the plugin has completed it's startup.
	 * 
	 * @param	plugin			The plugin that was started
	 */
	virtual PluginListener& pluginStartUpComplete(Plugin& plugin) = 0;

	/**
	 * Called with the plugin has been stopped.
	 */
	virtual PluginListener& pluginStopComplete(Plugin& plugin) = 0;

	/**
	 * Called when the plugin has completed it's teardown
	 * 
	 * @param	plugin			The plugin that was torn down
	 */
	virtual PluginListener& pluginTearDownComplete(Plugin& plugin) = 0;

	/**
	 * Called when the plugin is bound to the PluginHost.
	 * 
	 * @param	pluginHost		The host from which the plugin was bound.
	 * @param	plugin			The plugin that was unbound.
	 */
	virtual PluginListener& pluginBound(PluginHost& pluginHost, Plugin& plugin) = 0;

	/**
	 * Called when the plugin is unbound from the PluginHost.
	 *
	 *  @param	pluginHost		The host from which the plugin was unbound.
	 *  @param  plugin          The plugin that was unbound.
	 */
	virtual PluginListener& pluginUnbound(PluginHost& pluginHost, Plugin& plugin) = 0;
};

/**
 * Provides common Plugin logic (e.g. listener management) and 
 * default functionality (e.g. noop functions) for ease of use.
 * @see Plugin interface for detailed info.
 *
 * Allows implementers of Plugin to implement only those 
 * methods of interest.  For example, passive Plugin
 * might have no interest in the start() message.  Deriving
 * from this class allows the developer to ignore this
 * event.
 */
class __declspec(dllexport) AbstractPlugin : public Plugin {
	//	friend class PluginConfigurator;
public:
	AbstractPlugin(const std::string& name);
	AbstractPlugin(const AbstractPlugin& tocopy);
	virtual ~AbstractPlugin();

	AbstractPlugin& operator=(const AbstractPlugin& rhs);

	/**
	 * The name of any Plugin derived from AbstractPlugin is
	 * maintained by AbstractPlugin.  Acquire this name via
	 * this method
	 *
	 * @returns		The name of this plugin.
	 */
	virtual const std::string& name() const;

	/**
	 * The version of any plugin can defaults to 0.
	 * This method returns that default value.  
	 * Plugin implementations should override this
	 * method to indicate a plugin version other
	 * than 0.
	 */
	virtual unsigned int version() const;

	/**
	 * Return true if given interface name is supported by this plugin
	 */
	bool supportsInterface(const std::string& interfaceName) const {
		return _supportedInterfaces.find(interfaceName) != _supportedInterfaces.end();
	}

	/**
	 * Noop to facilitate Plugin development.
	 * 
	 * @returns		Reference to self
	 */
	virtual Plugin& setUp();

	/**
	 * Noop to facilitate Plugin development.
	 *
	 * @returns		Reference to self
	 */
	virtual Plugin& tearDown();

	/**
	 * Noop to facilitate Plugin development.
	 *
	 * @returns		Reference to self
	 */
	virtual Plugin& startPlugin();

	/**
	 * Noop to facilitate Plugin development.
	 *
	 * @returns		Reference to self
	 */
	virtual ACTPtr stopPlugin();

	/**
	 * AbstractPlugin provides functionality for 
	 * <ol>
	 *  <li>managing listeners that have subscribed 
	 *   to listent to plugin events.</li>
	 *  <li>dispatching Plugin events to PluginEventListeners.</li>
	 * </ol>
	 */
	virtual Plugin& addListener(PluginListener& listener);

	/**
	 * AbstractPlugin provides functionality for 
	 * <ol>
	 *  <li>managing listeners that have subscribed 
	 *   to listent to plugin events.</li>
	 *  <li>dispatching Plugin events to PluginEventListeners.</li>
	 * </ol>
	 *
	 * @return		Reference to self.
	 */
	virtual Plugin& removeListener(PluginListener& listener);

	/**
	 * When a Plugin has completed the startup sequence, it is bound to the
	 * PluginHost.  This allows the plugin to inspect the PluginHost
	 * that is holding it.
	 *
	 * @returns		Reference to self.
	 */
	virtual Plugin& bind(PluginHost& pluginHost);

	/**
	 * When a Plugin has completed the shutdown sequence, it is unbound 
	 * from the PluginHost
	 * 
	 * @returns		Reference to self.
	 */
	virtual Plugin& unbind(PluginHost& pluginHost);

	// TODO: Rename to getPluginImpl() for consistency
	//
	template <typename InterfaceT>
	InterfaceT* getServiceImpl(const std::string& serviceName) {
		PluginPtr pp = AbstractPlugin::host()->getPlugin(serviceName);
		if (pp.operator->() == 0) {
			std::stringstream ss;
			ss << "No " << serviceName;
			throw PluginException(SOURCELOCATION, ss.str());
		}
		return dynamic_cast<InterfaceT*>(&*AbstractPlugin::host()->getPlugin(serviceName));
	}

	template <typename InterfaceT>
	bool hasPluginImpl(const std::string& serviceName) {
		PluginPtr pp = AbstractPlugin::host()->getPlugin(serviceName);
		if (pp.operator->() == 0)
			return false;
		InterfaceT* p = dynamic_cast<InterfaceT*>(&*AbstractPlugin::host()->getPlugin(serviceName));
		return p > 0;
	}

	virtual Monitored::DataPtr monitor() const;

protected:
	/**
	 * AbstractPlugin assumes responsibility for managing the plugins binding
	 * to a host.  Use this method in derivatives of AbstractPlugin to acquire
	 * the PluginHost if bound.
	 * @return  Pointer PluginHost if bound, else null
	 */
	PluginHost* host() const;

	/**
	 * Helper function for dispatching PluginStartComplete event to listeners
	 * 
	 * @returns reference to self.
	 */
	Plugin& firePluginStartComplete();

	/**
	 * Helper function for dispatching PluginSetupComplete event to listeners
	 * 
	 * @returns reference to self.
	 */
	Plugin& firePluginSetupComplete();

	/**
	 * Helper function for dispatching PluginStartBound event to listeners
	 * 
	 * @returns reference to self.
	 */
	Plugin& firePluginBound();

	/**
	 * Helper function for dispatching PluginUnbound event to listeners
	 * 
	 * @returns reference to self.
	 */
	Plugin& firePluginUnbound();

	/**
	 * Register a supported interface name for this plugin so the host can look it up by such name.
	 * 
	 * @returns reference to self.
	 */
	Plugin& addSupportedInterface(const std::string& interfaceName) {
		_supportedInterfaces.insert(interfaceName);
		return *this;
	}

private:
	std::string _name;
	mutable PluginHost* _host;

	std::set<std::string> _supportedInterfaces;
};

/**
 * PluginLibraries are collections of PluginFactory implementations.
 * PluginFactory allows us to defer the construction of a Plugin
 * until it is actually bound to PluginHost.
 *
 * This class sets for the contract to implemented PluginFactories.
 */
class PluginFactory {
public:
	virtual const std::string& name() const = 0;
	virtual PluginPtr createPlugin(const TIXMLConfig::Item& config = TIXMLConfig::nullItem()) const = 0;
};

/**
 * Helper class to aid in the development of the PluginFactories.
 * Supplies naming logic.  Concrete PluginFactory implementations
 * must still implement createPlugin().
 */
class __declspec(dllexport) AbstractPluginFactory : public PluginFactory {
public:
	AbstractPluginFactory(const std::string& name) :
			_name(name) {
		// cout << "AbstractPluginFactory::AbstractPluginPluginFactory()" << endl;
	}

	virtual ~AbstractPluginFactory() {
		// cout << "AbstractPluginFactory::~AbstractPluginPluginFactory()" << endl;
	}

	virtual const std::string& name() const {
		// cout << "AbstractPluginFactory::name()" << endl;
		return _name;
	}

private:
	std::string _name;
};

typedef std::shared_ptr<PluginFactory> PluginFactoryPtr; // Plugin Factory Pointer

//#define USE_FACTORYHOLDER
#ifdef USE_FACTORYHOLDER
class FactoryRefHolder {
public:
	FactoryRefHolder(PluginFactoryPtr factoryRef) :
			_factoryRef(factoryRef) {}

	FactoryRefHolder(FactoryRefHolder& other) :
			_factoryRef(other._factoryRef) {}

	~FactoryRefHolder() {}

	PluginFactoryPtr getRef() {
		return _factoryRef;
	}

private:
	PluginFactoryPtr _factoryRef;
};
#endif

#ifdef USE_FACTORYHOLDER
typedef std::map<std::string, FactoryRefHolder*> PluginFactoryMap; // A map of factories keyed by name
#else
typedef std::map<std::string, PluginFactoryPtr> PluginFactoryMap; // A map of factories keyed by name
#endif

typedef std::shared_ptr<PluginFactoryMap> PluginFactoryMapPtr; // A pointer to a map of factories

#ifdef USE_FACTORYHOLDER
typedef std::vector<FactoryRefHolder*> PluginFactoryVec; // A vector of factories
#else
typedef std::vector<PluginFactoryPtr> PluginFactoryVec; // A vector of factories
#endif
typedef std::shared_ptr<PluginFactoryVec> PluginFactoryVecPtr; // A pointer to a vector of factories

/**
 * Static v Dynamic Libraries.  Plugins that are statically linked into the binary obj 
 * differ from Plugins loaded via a dll (dynamic) in that they don't have an associated
 * file handle.
 *
 * Typically, there will be only one static library loaded, though this singularity
 * is not enforced for testing purposes.
 */
class __declspec(dllexport) PluginLibrary {
public:
	/**
	 * Default constructor
	 */
	PluginLibrary();

	/**
	 * Construct with a name.  
	 */
	PluginLibrary(const std::string& name);

	/**
	 * dtor
	 */
	virtual ~PluginLibrary();

	/**
	 * PluginLibraries are named.  
	 *
	 * @returns The name of this PluginLibrary
	 */
	const std::string& name() const;

	/**
	 * Add a Plugin factory to the library.
	 * @param		factory	Plugin factory to be added to the library
	 * @returns	reference to self
	 */
	PluginLibrary& addPluginFactory(const PluginFactoryPtr& factory);

	/**
	 * Given the name of a plugin, retrieve the factory for that
	 * plugin.
	 *
	 * @param		name	The name of the plugin whose factory is to
	 *						be returned.
	 * @returns		The plugin factory for plugin with name.  0 if
	 *				not found
	 */
	PluginFactoryPtr getPluginFactory(const std::string& name);

	/**
	 * Given the name of a plugin, remove it's factory from the library.
	 * returns		reference to self
	 */
	PluginLibrary& removePluginFactory(const std::string& name);

	/**
	 * Adds libraries plugin factories to a map of
	 * factories keyed by factory name.
	 *
	 * To facilitate acquisition of a plugin factory, 
	 * a map of all the PluginFactories is maintained by
	 * the PluginHost.
     *
	 * This function is called whenever a plugin library 
	 * is loaded to register this library's plugins.
	 * @parm	acquisitionMap		Map of plugin factories 
     *								keyed by name
	 * @returns	reference to self
	 */
	PluginLibrary& map(PluginFactoryMap& mappingTarget);

	/**
	 * Removes libraries plugin factories from a map of
	 * factories keyed by factory name.
	 *
	 * To facilitate acquisition of a PluginFactory, 
	 * a map of all the PluginFactories is maintained by 
	 * the PluginHost.
     *
	 * This function is called whenever a plugin library 
	 * is unloaded to deregister this library's plugins.
	 *
	 * @parm	acquisitionMap		Map of plugin factories keyed by name
	 * @returns	reference to self
	 */
	PluginLibrary& unmap(PluginFactoryMap& mappingTarget);

	/**
	 * Makes system call to opens a DLL containing one or more plugins.  
	 * This DLL must export the function.  
	 *
	 *   FactoryVectPtr EnumeratePlugins()
	 * 
	 * When the DLL is opened it's handled is stored, along with the results of a call to
	 * EnumeratePlugins()
	 *
	 * @param	fileName			path and name of the DLL to be loaded.
	 * @returns	reference to self
	 * @throws	PluginException   	In the event file cannot be opened, in which case the error message
	 *								will contain the OS system error.
	 *								In the event that the DLL does not export 
	 *								FactoryVecPtr EnumeratePlugins()
	 */
	PluginLibrary& load(const std::string& filename);

	/**
 	 * Cleans up internal plugin map and closes the external library 
	 * Note, if the library has been mapped into a larger flat collection
	 * use map(),  It is imperative that unmap() be callsed prior to unloading
	 * library
	 */
	PluginLibrary& unload();

private:
	HMODULE _handle;
	std::string _name;

	// When plugin libraries are loaded, their factories are mapped
	// into a plugin domain.
	//
	// Specifically a plugin domain, is a collection of plugin
	// libraries that are mapped into a single flat digest (implemented
	// as a PluginFactoryMap) in order to facilitate unsegmented
	// searches for plugin factories (i.e. search of a single list
	// rather than a list of lists).
	//
	// When the library is destroyed, it has no knowledge of this mapping
	// and hence, references in the plugin domain become invalidated.
	//
	// It is incumbent upon this Host to insure that the a libraries
	// are unloaded in the properly.  That is to say, destroying plugin
	// host will reliably release all related plugins, factories and
	// libraries.  Destroying a library, while is bound referenced in
	// the plugin domain makes no such guarantee.
	//
	PluginFactoryMap _factoryMap;
};
typedef std::shared_ptr<PluginLibrary> PluginLibraryPtr;
typedef std::map<std::string, PluginLibraryPtr> PluginLibraryMap;

/**
 * Ultimately, there will be a need to run and use our plugins.
 * This class is what makes that happen.
 * 
 * This class manages three key collections:
 *
 * <ul>
 *  <li>A map all loaded PluginLibraries keyed by name.  This is used 
 *    for library management.</li>
 *  <li>A map of all loaded PluginFactories.  This collection represents 
 *    all of the loaded libraries mapped into a single map.  As libraries
 *    are loaded/removed, the factories are added/removed from this 
 *    collection</li>
 *  <li>A collection of all Plugins that are currently hosted by this
 *    this PluginHost</li>
 * </ul>
 * 
 * Typical work session would follow the folloing pseudo code:
 * <ol>
 *  <li>Startup
 *   <ol>
 *    <li>Create PluginHost</li>
 *	  <li>Create/Load libraries and add them to the PluginHost</li>
 *	  <li>Create, mount, setup, start desired plugins</li>
 *	 </ol>
 *  </li>
 *  <li>Shutdown
 *   <ol>
 *    <li>stop, teardown, dismount, destroy plugins</li>
 *    <li>unload libraries</li>
 *    <li>destroy the PluginHost</li>
 *   </ol>
 *  </li>
 * </ol>
 */
class __declspec(dllexport) PluginHost : public Monitored {
public:
	static PluginHost& singleton();

	/**
	 * Default constructor
	 */
	PluginHost();

	/**
	 * dtor
	 */
	virtual ~PluginHost();

	/**
	 * Helper function consolidate necessary exception handling 
	 * when loading a library.
	 *
	 * @param path		Full path and file name of the library to 
	 *					be loaded.
	 * @return			Reference to self
	 * @throws			PluginException
	 */
	PluginHost& loadLibrary(const std::string& path);

	// The current implementation of PluginHost has no support
	// for resolving dependencies between Plugins.  Rather it provides
	// the following methods that allow an external entity to manage
	// the startup and shutdown sequencing of a plugin.
	//

	/**
	 * Mount an instance of Plugin with name.  Mounting a plugin
	 * entails instantiating the plugin and adding the plugin to
	 * the host collection of hosted Plugins.
	 *
	 * @returns reference to self.
	 */
	PluginHost& mountPlugin(const std::string& factoryName, const std::string& instanceName = std::string(""), const TIXMLConfig::Item* config = 0);

	/**
	 * Unmount the instance of Plugin with name.  Unmounting
	 * entails removing the Plugin from the Hosts collection of 
	 * host Plugins.
	 *
	 * @returns		Reference to self.
	 */
	PluginHost& unmountPlugin(const std::string& name);

	/**
	 * Setup the the plugin with name.  After a plugin has been
	 * mounted, it Plugin::setUp() would be called to allow the 
	 * plugin to acquire any necessary resources.
	 *
	 * @returns		Reference to self.
	 */
	PluginHost& setUpPlugin(const std::string& name);

	/**
	 * Start a mounted, setup plugin.  After a plugin has been setup
	 * it is started.  Many plugins need to nothing during this phase
	 * of startup, but this is where the plugin would initiate
	 * any asynchronous processing.
	 *
	 * @returns		Reference to self.
	 */
	PluginHost& startPlugin(const std::string& name);

	/**
	 * Stop the plugin.  Eventually, a plugin must be shutdown.  Most
	 * Plugins do nothing in the phase of shutting down a plugin, but
	 * for those plugins that employ any asynchronous processing, this
	 * is where that processing is stopped.
	 *
	 * @returns		Reference to self.
	 */
	ACTPtr stopPlugin(const std::string& name);

	/**
	 * Tear down the plugin.  Called after the plugin has been stopped,
	 * this function allows the plugin to release any resources acquired
	 * during setup.
	 *
	 * @returns		Reference to self.
	 */
	PluginHost& tearDownPlugin(const std::string& name);

	/**
	 * Add a PluginLibrary to the PluginHost.  This could be a
	 * static library or dynamic library.
	 *
	 * @returns		Reference to self.
	 */
	PluginHost& addPluginLibrary(PluginLibraryPtr library);

	/**
	 * Add a dynamic PluginLibrary implemented in the DLL named
	 * in pluginPath.
	 *
	 * @param	pluginPath		The fullpath and name of the dll to be
	 *							loaded.
	 * @throws @see Plugin::load()
	 */
	PluginHost& loadPluginLibrary(const std::string& pluginPath);

	/**
	 * Subscribe a PluginHostListener
	 */
	PluginHost& addListener(PluginHostListener& listener);

	/**
	 * Unsubscribe a PluginHostListener
	 */
	PluginHost& removeListener(PluginHostListener& listener);

	/**
	 * Acquire a plugin by name.
	 * @ returns	Pointer to Plugin if found, else null
     * @throws PluginException if it fails to acquire the plugin.
	 */
	PluginPtr getPlugin(const std::string& name, unsigned int version = 0);

	/**
	 * Acquire a plugin by one of the interfaces it supports.
	 * @ returns	Pointer to Plugin if found, else null
     * @throws PluginException if it fails to acquire the plugin.
	 */
	PluginPtr getPluginByInterfaceName(const std::string& interfaceName);

	template <typename InterfaceT>
	std::shared_ptr<InterfaceT> getInterface(const std::string& serviceName, unsigned int /*version*/ = 0) {
		PluginPtr pp = getPlugin(serviceName);
		if (pp.operator->() == 0) {
			std::stringstream ss;
			ss << "Plugin with name [" << serviceName << "] not found!";
			throw PluginNotFoundException(SOURCELOCATION, ss.str());
		}

		std::shared_ptr<InterfaceT> ret = std::dynamic_pointer_cast<InterfaceT>(pp);
		if (ret.operator->() == 0)
			throw PluginNotFoundException(SOURCELOCATION, ErrorSpec(1) << "Failed to acquire plugin [" << serviceName << "] Type conflict!");
		return ret;
	}

	template <typename InterfaceT>
	std::shared_ptr<InterfaceT> getInterfaceNoThrow(const std::string& serviceName, unsigned int version = 0) {
		PluginPtr pp = getPlugin(serviceName);
		return std::dynamic_pointer_cast<InterfaceT>(pp);
	}

	template <typename InterfaceT>
	InterfaceT* DEPRECATED_getInterface(const std::string& serviceName, unsigned int /*version*/ = 0) {
		PluginPtr pp = getPlugin(serviceName);
		if (pp.operator->() == 0) {
			std::stringstream ss;
			ss << "Plugin with name [" << serviceName << "] not found!";
			throw PluginNotFoundException(SOURCELOCATION, ss.str());
		}
		InterfaceT* ret = dynamic_cast<InterfaceT*>(&*(pp));
		if (ret == 0) {
			std::stringstream ss;
			ss << "Failed to acquire plugin [" << serviceName << "] Type conflict!";
			throw PluginNotFoundException(SOURCELOCATION, ss.str());
		}
		return ret;
	}

	template <typename InterfaceT>
	InterfaceT* DEPRECATED_getInterfaceByName(const std::string& interfaceName) {
		PluginPtr pp = getPluginByInterfaceName(interfaceName);
		if (pp.operator->() == 0) {
			std::stringstream ss;
			ss << "Plugin with name [" << interfaceName << "] not found!";
			throw PluginNotFoundException(SOURCELOCATION, ss.str());
		}
		InterfaceT* ret = dynamic_cast<InterfaceT*>(&*(pp));
		if (ret == 0) {
			std::stringstream ss;
			ss << "Failed to acquire plugin [" << interfaceName << "] Type conflict!";
			throw PluginNotFoundException(SOURCELOCATION, ss.str());
		}
		return ret;
	}

	template <typename InterfaceT>
	InterfaceT* DEPRECATED_getInterfaceNoThrow(const std::string& serviceName, unsigned int version = 0) {
		try {
			return DEPRECATED_getInterface<InterfaceT>(serviceName, version);
		} catch (const PluginException& /* e */) {
			return 0;
		}
	}

	template <typename InterfaceT>
	InterfaceT* DEPRECATED_getInterfaceByNameNoThrow(const std::string& interfaceName) {
		try {
			return DEPRECATED_getInterfaceByName<InterfaceT>(interfaceName);
		} catch (const PluginException& /* e */) {
			return 0;
		}
	}

	ACTPtr shutdown();

	/**
     * @see Monitored
     */
	virtual DataPtr monitor() const;

private:
	/**
	 * Helper function to dispatch PluginLoaded events to classes that have subscribed 
	 * to PluginHost events.
	 *
	 * @returns		Reference to self.
	 */
	PluginHost& firePluginLoaded();

	/**
	 * Helper function to dispatch PluginUnloaded events to classes that have subscribed
	 * to PluginHost events.
	 *
	 * @returns		Reference to self.
	 */
	PluginHost& firePluginUnloaded();

	// A map of all PluginLibraries held by this PluginHost
	// Each of these libraries are mapped into
	// _availablePlugins
	//
	PluginLibraryMap _pluginLibraries;

	// A flat map of all plugin factories from all plugin
	// libraries.  Allows access to all factories, regardless
	// of hosting library.
	//
	PluginFactoryMap _availablePlugins;

	// A map of PluginPtr.  Used to provide access to
	// mounted plugins
	//
	PluginMap _mountedPlugins;

	// A vector PluginPtr.  Used to enforce shutdown
	// on running plugins.  Parallel to mountedPlugins
	// but it records the order in which plugins were
	// cycled up and shuts them down in reverse order.
	//
	PluginStack _shutdown;

	// Our well known singleton instance
	//
	static Singleton<PluginHost> _singleton;
};

/**
 * Some objects will want to subscribe to receive published PluginHost events.
 * E.g. a dependent plugin can listen for an event indicating that a required
 * plugin was loaded.
 *
 * This class sets forth the interface for such classes.
 */
class PluginHostListener {
public:
	/**
	 * Fired when a plugin has been added to the PluginHost.
	 */
	virtual PluginHostListener& pluginLoaded(PluginHost& pluginHost, Plugin& plugin) = 0;

	/**
	 * Fired when a plugin has been removed from the PluginHost.
	 */
	virtual PluginHostListener& pluginUnloaded(PluginHost& pluginHost, Plugin& plugin) = 0;
};

class __declspec(dllexport) PluginConfig {
public:
	PluginConfig(const std::string& factoryName, const std::string pluginName = "") :
			_factoryName(factoryName), _pluginName(pluginName) {}

	PluginConfig(const PluginConfig& other) :
			_factoryName(other._factoryName), _pluginName(other._pluginName), _attributes(other._attributes), _blob1() {
		_blob1 << other._blob1.str();
	}

	virtual ~PluginConfig() {}

	PluginConfig& operator=(const PluginConfig& rhs) {
		_factoryName = rhs._factoryName;
		_pluginName = rhs._pluginName;
		_attributes = rhs._attributes;
		_blob1 << rhs._blob1.str();
		return *this;
	}

	PluginConfig& addAttribute(const std::string& key, const std::string& value) {
		_attributes.push_back(std::pair<std::string, std::string>(key, value));
		return *this;
	}

	PluginConfig& operator<<(const std::string& blob) {
		_blob1 << blob;
		return *this;
	}

	__declspec(dllexport) friend std::ostream& operator<<(std::ostream& out, const PluginConfig& item);

	virtual std::string toXML() const {
		std::stringstream ss;
		ss << *this;
		return ss.str();
	}

protected:
	virtual std::ostream& streamTo(std::ostream& out) const;

private:
	std::string _pluginName;
	std::string _factoryName;
	std::vector<std::pair<std::string, std::string>> _attributes;
	std::stringstream _blob1;
};

class PluginHostConfiguration {
private:
	typedef std::vector<PluginConfig> PluginVecT;

public:
	PluginHostConfiguration() :
			_libs(), _plugins() {
		//        addPluginLibrary("CorePluginLibrary.dll");
		//        addPluginLibrary("IncubatorPlugins.dll");
	}

	virtual ~PluginHostConfiguration() {}

	PluginHostConfiguration& addPluginLibrary(const std::string& libName) {
		_libs.push_back(libName);
		return *this;
	}

	PluginHostConfiguration& addPluginConfig(const PluginConfig& config) {
		_plugins.push_back(config);
		return *this;
	}

	std::string toXML() const {
		std::stringstream ss;
		ss << "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
		   << "<Config>"
		   << "<PluginHost>"
		   << "<Libraries>";
		for (std::vector<std::string>::const_iterator i = _libs.begin(); i != _libs.end(); i++) {
			ss << "<Library path=\"" << *i << "\" />";
		}

		ss << "</Libraries>"
		   << "<Plugins>";

		for (std::vector<PluginConfig>::const_iterator pi = _plugins.begin(); pi != _plugins.end(); pi++) {
			ss << *pi;
		}

		ss << "</Plugins></PluginHost></Config>";
		return ss.str();
	}

private:
	std::vector<std::string> _libs;
	PluginVecT _plugins;
};

} // namespace KEP