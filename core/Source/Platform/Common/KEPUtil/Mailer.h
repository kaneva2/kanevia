///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <windows.h>
#include <assert.h>
#include <vector>
#include <utility>
#include <Log4CPlus/Logger.h>
#include "Core\Util\runnable.h"
#include "Core\Util\ChainedException.h"
#include "common\include\kepcommon.h"
#include "common\keputil\MailerErrors.h"
#include "common\keputil\WSAErrors.h"
#include "common\include\kepexception.h"
#include "SocketSubsystem.h"
#include "Common\KEPUtil\Plugin.h"
#include "TinyXML/TinyXML.h"

namespace KEP {

const int DEFAULT_PROTOCOL = 0;
const int NO_FLAGS = 0;
/**
 * This is a specialized class of KEPException.  All exceptions thrown from the class of type MailException.
 * If specialized exceptions are needed, they will be derived from this class.
 */
CHAINED(MailException);

class MailerInternalsT;

/**
 * This class provides very basic capabilities to send email via SMTP.
 */
class __declspec(dllexport) LightWeightMailer {
private:
	// forward reference.
	//
	class MailRecipient;

	// instances of type RecipientsVector are used collect lists of email addresses.
	//
	typedef std::vector<MailRecipient> RecipientsVector;

public:
	/**
	 * Helper function that returns the string representation of a list of email addresses
	 */
	static std::string toString(const RecipientsVector& recipients);
	std::string LightWeightMailer::str();

	/**
	 * Factory method for constructing a LightWeightMailer instance with minimal 
	 * configuration
	 */
	static LightWeightMailer create(const std::string& mailHost, const std::string& fromAddress, const std::string& toAddress, const std::string& subject, const std::string& body);

	/**
	 * Factory method that creates a runnable instance of a mailer.  The mailer is 
	 * preconfigured, but transmission is deferred to the run() method.  Helps us 
	 * run mailing tasks on another thread.
	 */
	static RunnablePtr createRunnable(const std::string& mailHost, const std::string& fromAddress, const std::string& toAddress, const std::string& subject, const std::string& body);
	/**
	 * Helper function to simplify use of this class.
	 * @throws MailException
	 */
	static ErrorSpec send(const std::string& mailHost, const std::string& fromAddress, const std::string& toAddress, const std::string& subject, const std::string& body);

	/**
	 * Transmit a configured email.  
	 */
	ErrorSpec send();

	/**
	 * Transmit a partially configured email.  Presumes that host, sender, recipients are 
	 * already configured.
	 */
	ErrorSpec send(const std::string& subject, const std::string& body);

	/**
	 * Obtain name of mailhost to be used by this Mailer
	 */
	std::string mailHost() { return _mailHostName; }

	static std::string errorString(int ErrorCode, HINSTANCE hInst = 0);

	LightWeightMailer();
	LightWeightMailer(const LightWeightMailer& mailer);
	LightWeightMailer& operator=(const LightWeightMailer& mailer);

	virtual ~LightWeightMailer();

	LightWeightMailer& setMailHost(const std::string& mailHost) {
		_mailHostName = mailHost;
		return *this;
	}

	/**
	 * Add a recipient to the email.
	 */
	LightWeightMailer& addRecipient(const std::string& email, const std::string& name = "");

	enum MAIL_ERRORS {
		errorNoSender = IDS_MAILERR_NOSENDER,
		errorNoRecipient = IDS_MAILERR_NORECIPIENT
	};

	/**
	 * Obtain the this mailers senders email address
	 */
	const std::string& senderEmail() const;
	LightWeightMailer& setSenderEmail(const std::string& email);

	/**
	 * Obtain a list of recipients of this mailer
	 */
	const RecipientsVector& recipients();

	/**
	 * Obtain a list of cc recipients of this mailer
	 */
	const RecipientsVector& ccRecipients();

	/**
	 * Obtain a list of bcc recipients of this mailer
	 */
	const RecipientsVector& bccRecipients();

private: // Make everything private and expose only what we need to expose.
	// Addressing operations
	//

	/**
	 * Add a bcc recipient to the email.
	 */
	LightWeightMailer& addBCCRecipient(const std::string& email, const std::string& name = "");

	/**
	 * Add a cc recipient to the email.
	 */
	LightWeightMailer& addCCRecipient(const std::string& email, const std::string& name = "");

	/**
	 * Determine number of configured bcc recipients 
	 */
	const unsigned int bccRecipientCount();

	/**
	 * Determine number of configured cc recipients
	 */
	const unsigned int ccRecipientCount();

	/**
	 * Determine number of recipients
	 */
	const unsigned int recipientCount();

	// Socket operations
	//
	bool connectStatus();
	const unsigned int getSocket();

	// Get/Set attributes.
	//
	const std::string& localHostIp();
	const std::string& messageBody() const;
	LightWeightMailer& setMessageBody(const std::string& body);
	const std::string& replyTo() const;
	LightWeightMailer& setReplyTo(const std::string& replyto);
	const std::string& senderName() const;
	LightWeightMailer& setSenderName(const std::string& name);
	const std::string& subject() const;
	LightWeightMailer& setSubject(const std::string& subject);
	const std::string& xMailer() const;
	LightWeightMailer& setXMailer(const std::string& xmailer);

	char* allocateRecipientsString(RecipientsVector& recipients);

	static std::string recipientsString(const RecipientsVector& recipients);

	/**
	 * Wrapper of a string containing an email address.  Provides appropriate
	 * comparison/assignment operations
	 */
	class MailRecipient {
	public:
		MailRecipient(const std::string& email) :
				_email(email) {}

		virtual ~MailRecipient() {}

		bool operator==(const MailRecipient& src) {
			if (this == &src)
				return true;
			return _email.compare(src._email) == 0;
		}

		MailRecipient& operator=(const MailRecipient& src) {
			if (!operator==(src))
				setEMail(src._email);
			return *this;
		}

		const std::string email() const { return _email; }
		MailRecipient& setEMail(const std::string& email) {
			_email = email;
			return *this;
		}

	private:
		std::string _email;
	};

	//bool				_bConnected;
	std::string _localHostName;
	std::string _mailHostName;
	std::string _fromEmail;
	std::string _fromName;
	std::string _subject;
	std::string _msgBody;
	std::string _xMailer;
	std::string _replyTo;
	std::string _ipAddr;
	RecipientsVector _recipients;
	RecipientsVector _ccRecipients;
	RecipientsVector _bccRecipients;

	// Need to hide these from files including this file as any one that use stdafx/precompiled headers is not likely to build.
	// By the time that we get to the include of windows.h above, stdafx has already loaded and precluded winsock.
	// We can do this by storing this data in a secondary structure, and maintaining a pointer to that structure here.
	// Note that it's also important that the size of the instance is consisttent regardless of perspective.
	//
	//    WSADATA				wsaData;
	//    SOCKET				hSocket;
	//
	MailerInternalsT* _hiddenInternals;

	friend class MailerInternalsT;
};

/**
 * Very light wrapper to adapt the mailer to a plugin.
 */
class MailerPlugin : public AbstractPlugin {
public:
	MailerPlugin() :
			AbstractPlugin("Mailer"), _enabled(true) {}

	void send(const std::string& mailHost, const std::string& from, const std::string& to, const std::string& subject, const std::string& body) {
		if (_enabled)
			LightWeightMailer::send(mailHost, from, to, subject, body);
		_mailer.setSenderEmail("");
	}

	void send(const std::string& subject, const std::string& body) {
		if (_enabled)
			_mailer.send(subject, body);
	}

	MailerPlugin& setEnabled(bool enabled) {
		_enabled = enabled;
		return *this;
	}
	bool enabled() const { return _enabled; }

	void setMailHost(const std::string& mailHost) { _mailer.setMailHost(mailHost); }
	void setSenderEmail(const std::string& sender) { _mailer.setSenderEmail(sender); }
	void addRecipient(const std::string& recipient) { _mailer.addRecipient(recipient); }

private:
	LightWeightMailer _mailer;
	bool _enabled;
};

class MailerPluginFactory : public AbstractPluginFactory {
public:
	MailerPluginFactory() :
			AbstractPluginFactory("Mailer") {}

	virtual ~MailerPluginFactory() {}
	PluginPtr createPlugin(const TIXMLConfig::Item& configItem) const {
		TiXmlElement* config = configItem._internal;
		if (!config) {
			// throw exception?
			return PluginPtr((Plugin*)0);
		}

		MailerPlugin* p = new MailerPlugin();

		const char* pc = config->Attribute("enabled");
		if (pc)
			p->setEnabled(strcmp("0", pc) != 0);

		const TiXmlElement* n = config->FirstChildElement("defaults");

		pc = n->Attribute("mail_host");
		if (pc)
			p->setMailHost(pc);
		pc = n->Attribute("from");
		if (pc)
			p->setSenderEmail(pc);
		pc = n->Attribute("to");
		if (pc)
			p->addRecipient(pc);

		return PluginPtr(p);
	}
};

} // namespace KEP