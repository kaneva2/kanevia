///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <iostream>

// forward references for streaming support
template <class T>
class Numeric;

template <class T>
std::ostream& operator<<(std::ostream& out_stream, Numeric<T>& Obj);

/**
 * This class exists primarily for the purpose precluding autocasting of numeric types.
 * Officially, this can be handle via pragmas, however, that would require a global pragma
 * and it appears to be kind of hit or miss as to whether the compiler actually reports
 * it as an error.
 *
 * Note, that there is nothing that can prevent a developer from explicitly casting 
 * numeric types of different size/signage.  This class is simply forcing the developer
 * to make explicit casts.
 */
template <typename T>
class Numeric {
public:
	// ctors/dtor
	//
	Numeric(const T& value) :
			_value(value) {}
	Numeric<T>(const Numeric<T>& toCopy) :
			_value(toCopy._value) {}
	virtual ~Numeric() {}

	// Assignment operators
	Numeric<T>& operator=(const Numeric<T>& rhs) {
		_value = rhs._value;
		return *this;
	}
	Numeric<T>& operator=(const T& rhs) {
		_value = rhs;
		return *this;
	}

	// Comparison operators
	bool operator==(const T& rhs) const { return _value == rhs; }
	bool operator==(const Numeric<T>& rhs) const { return _value == rhs._value; }
	bool operator<(const T& rhs) const { return _value < rhs; }
	bool operator<(const Numeric<T>& rhs) const { return _value < rhs._value; }
	bool operator<=(const T& rhs) const { return _value <= rhs; }
	bool operator<=(const Numeric<T>& rhs) const { return _value <= rhs._value; }
	bool operator>(const T& rhs) const { return _value > rhs; }
	bool operator>(const Numeric<T>& rhs) const { return _value > rhs._value; }
	bool operator>=(const T& rhs) const { return _value >= rhs; }
	bool operator>=(const Numeric<T>& rhs) const { return _value >= rhs._value; }
	bool operator!=(const T& rhs) const { return _value != rhs; }
	bool operator!=(const Numeric<T>& rhs) const { return _value != rhs._value; }

	// Additive operators
	Numeric<T>& operator+=(const T& rhs) {
		_value += rhs;
		return *this;
	}
	Numeric<T>& operator+=(const Numeric<T>& rhs) {
		_value += rhs._value;
		return *this;
	}
	Numeric<T> operator+(const T& rhs) const { return Numeric<T>(_value += rhs); }
	Numeric<T> operator+(const Numeric<T>& rhs) const { return Numeric<T>(_value + rhs._value); }

	// Subtractive operators
	Numeric<T>& operator-=(const T& rhs) {
		_value -= rhs;
		return *this;
	}
	Numeric<T>& operator-=(const Numeric<T>& rhs) {
		_value -= rhs._value;
		return *this;
	}
	Numeric<T> operator-(const T& rhs) const { return Numeric(_value - rhs); }
	Numeric<T> operator-(const Numeric<T>& rhs) const { return Numeric(_value - rhs._value); }

	// If you can cast it to the correct type....you can get the internal value
	//
	operator T() const { return _value; }

	friend std::ostream& operator<<<>(std::ostream& out_stream, Numeric<T>& Obj);

private:
	T _value;
};

template <class T>
std::ostream& operator<<(std::ostream& out_stream, Numeric<T>& Obj) {
	out_stream << Obj._value;
	return out_stream;
}
