///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <iostream>
#include <sstream>

#include "Common/KEPUtil/ParamString.h"
#include "Common/KEPUtil/Algorithms.h"

#include "Core/Util/ChainedException.h"

namespace KEP {

class OldCommandLine;

/**
 * @file
 *
 * 
 */
// pure interface
namespace CommandLineI {

//class CommandLineI::CommandLine;

class __declspec(dllexport) Parameter {
public:
	Parameter() {}
	virtual ~Parameter() {}

	virtual const std::string& name() const = 0;
	virtual const std::string help() const = 0;
	virtual Parameter& setHelp(const std::string& help) = 0;
	virtual bool hasBeenSet() const = 0;
	virtual void parseInput(std::vector<std::string>::iterator& iinput) = 0;
	virtual std::string valueString() const = 0;
	virtual std::string defaultValueString() const = 0;
};

/**
 * Common implementation
 *      Help
 *      Name
 *      Value Value
 *		Default Value
 *		Init flag
 */
template <typename ParamT>
class __declspec(dllexport) GenericParameter : public Parameter {
public:
	GenericParameter(OldCommandLine& cmdLine, const std::string& name, ParamT defaultValue, const std::string& help = std::string("No help available.")) :
			_cmdLine(cmdLine), _name(name), _defaultValue(defaultValue), _defaultValueSet(true), _help(help), _actualValueSet(false) {
	}

	GenericParameter(OldCommandLine& cmdLine, const std::string& name, const std::string& help = std::string("No help available.")) :
			_cmdLine(cmdLine), _name(name), _help(help), _defaultValueSet(false), _actualValueSet(false) {}

	GenericParameter(OldCommandLine& cmdLine, bool diff, const std::string& name, const std::string& help) :
			_cmdLine(cmdLine), _name(name), _help(help), _defaultValueSet(false), _actualValueSet(false) {}

	virtual ~GenericParameter() {}
	const std::string& name() const { return _name; }
	const std::string help() const { return _cmdLine.parameters().toString(_help); }
	Parameter& setHelp(const std::string& help) {
		_help = help;
		return *this;
	}
	virtual bool hasBeenSet() const { return _actualValueSet; }
	operator ParamT() { return value(); }
	virtual ParamT value() const { return _actualValueSet ? _actualValue : _defaultValue; }
	const ParamT& defaultValue() const { return _defaultValue; }

	virtual void parseInput(std::vector<std::string>::iterator& iinput) = 0;

protected:
	Parameter& setValue(ParamT value) {
		_actualValueSet = true;
		_actualValue = value;
		return *this;
	}

	OldCommandLine& commandLine() const { return _cmdLine; }

protected:
	ParamT _defaultValue;
	ParamT _actualValue;
	bool _actualValueSet;
	bool _defaultValueSet;

private:
	OldCommandLine& _cmdLine;
	std::string _name;
	std::string _help;
};

class __declspec(dllexport) BooleanParameter : public GenericParameter<bool> {
public:
	BooleanParameter(OldCommandLine& cmdLine, const std::string& paramName, bool defaultValue, const std::string& paramHelp = std::string("No help available")) :
			GenericParameter(cmdLine, paramName, defaultValue, paramHelp) {}

	BooleanParameter(OldCommandLine& cmdLine, const std::string& paramName, const std::string& paramHelp = std::string("No help available")) :
			GenericParameter(cmdLine, paramName, paramHelp) {}

	BooleanParameter(OldCommandLine& cmdLine, const std::string& paramName) :
			GenericParameter(cmdLine, paramName, false) {}

	virtual ~BooleanParameter() {}
	virtual std::string valueString() const { return std::string(value() ? "true" : "false"); }
	virtual std::string defaultValueString() const { return std::string(defaultValue() ? "true" : "false"); }
	virtual void parseInput(std::vector<std::string>::iterator& /*iinput*/) { setValue(true); }
};

class __declspec(dllexport) StringParameter : public GenericParameter<std::string> {
public:
	StringParameter(OldCommandLine& cl, const std::string& paramName, const std::string& defaultValue, const std::string& help = std::string("No help available."));

	StringParameter(OldCommandLine& cl, const std::string& paramName, const std::string& help = std::string("No help available."));

	virtual ~StringParameter();

	std::string valueString() const;
	std::string defaultValueString() const;
	void parseInput(std::vector<std::string>::iterator& iinput);

	virtual std::string value() const;
};
} // namespace CommandLineI

/**
	CommandLine is a utility class to facilitate 
	Usage:

	// Create a command line object on the stack or heap.  If you place it on the heap
	// you can refer back to it later to evaluate command line params.
	//
	//
	CommandLine cl;

	// Add an optional boolean parameter
	// It's mere presence trips something, the question is, does it's presence represent 
	// a true or false?
	//
	CommandLine::BooleanParameter	optionalBoolean = cl.addBoolean( "optionalFlag", false );	
					
	// Add a required boolean parameter
	// Parsing will fail if this parameter is not found.  It's a show stopper.
	//
	CommandLine::BooleanParameter	requiredFlag = cl.addBoolean( "requiredFlag" );

	// Add an optional string parameter
	// default value, hence optional
	CommandLine::StringParameter	optionalString = cl.addString( "optionalString","defaultValue" );

	// Well, you can prolly figure out what this one does.
	// no default value, hence required.
	//
	CommandLine::StringParameter    requiredString = cl.addString( "requiredString" );

	// Note that we don't really have to acquire references to the returned parameters.
	// We can alway retrieve them later with getParameter(string)
	//

	// parsing will throw an exception if any required parameters are not supplied
	// as well as for any other parsing error
	//
	try {
		cl.parse( argv, argc );
	}
	catch( CommandLineException cle ) {
	    cout << "Error parsing command line [" << cle.m_message << "]" endl;
		exit(1);
	}

	bool bRequired			= requiredFlag.value();
	bool bOptional			= optionalFlag.value();
	std::string sRequired	= requiredString.value();
	std::string sOptional	= optionalString.value();
 */
class __declspec(dllexport) OldCommandLine {
public:
	typedef CommandLineI::Parameter Parameter;
	typedef CommandLineI::BooleanParameter BooleanParameter;
	typedef CommandLineI::StringParameter StringParameter;
	typedef std::pair<std::string, Parameter*> ParameterMapItemT;
	typedef std::map<std::string, Parameter*> ParameterMapT;

	OldCommandLine();
	OldCommandLine(const OldCommandLine& toCopy);
	virtual ~OldCommandLine();
	OldCommandLine& operator=(const OldCommandLine& rhs);
	OldCommandLine& addParameter(Parameter& parameter);
	BooleanParameter& addBoolean(const std::string& paramName, bool defaultValue, const std::string& help = std::string("No help available"));
	StringParameter& addString(const std::string& paramName, const std::string& dflt, const std::string& help = std::string("No help available"));

	/**
	 * Prints the command line help to stdout.  Each executable has help related to the
	 * application.  
	 */
	void showHelp() const;

	/**
	 * Parse the command line parameters.
	 *
	 * @param argc		The number of parameters contained in the command line as passed
	 *					to main().
	 * @param argv      The values of each parameter passed from the console as passed
	 *					to main().
	 * @return			reference to self
	 */
	OldCommandLine& parse(size_t argc, char** argv);

	/**
	 * In c, as in many other languages, the main function is passed an array of strings
	 * containing all of the parameters specified on the command line.  This array always
	 * has at least one value, argv[0] which contains the full path and file name of the 
	 * executable.  This method returns this value.
	 *
	 * @return reference to self
	 */
	const FileName& executable() const;

	/**
	 * CommandLine does not limit the parameters to those defined in 
	 * the command line.  Any parameters encountered that it doesn't
	 * know what to with are collected in a vector of "orphaned" parameters
	 * This method allows access to those parameters.
	 *
	 * @return vector of strings where each element is an unhandled command line
	 * param.
	 */
	std::vector<std::string> orphans();

	/**
	 * Acquire a pointer to a parameter with specified name.
	 *
	 * @return	pointer to the parameter with the specified name, null if not found.
	 */
	Parameter* getParameter(const std::string& paramName);

	/**
	 * Acquire a copy of this parameter's help string.
	 */
	std::string helpString() const;

	/**
	 * Acquire a reference to this command lines ParamSet
	 */
	ParamSet& parameters();

private:
	class HelpParameter : public BooleanParameter {
	public:
		virtual void parseInput(std::vector<std::string>::iterator& iinput);
		HelpParameter(OldCommandLine& cmdLine);
	};

	HelpParameter _help;
	ParamSet _params;
	ParameterMapT _paramMap;
	std::vector<std::string> _orphans;
	FileName _executable;
};

} // namespace KEP