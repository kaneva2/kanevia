///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <map>
#include "Common/KEPUtil/SynchronizedLock.h"
#include "Core/Util/ChainedException.h"

namespace KEP {

/**
 * A thread safe map mechanism.
 * There are essentially two modes of operation:
 * 
 * Implicit where all calls automatically 
 * synchronize.
 *
 * Explicit, where an explicit lock is performed before
 * any operations.
 *
 * Below is an example of implicit synchronization:
 *
 *      SynchronizedMap<int,int> m;
 *      m.set()                     // synchronized across this call
 *      m.get(key)                  // synchronized across this call
 *      m.exists(key)               // synchronized across this call
 *      m.find(key)                 // synchronized across this call
 *      m.erase(key)                // synchronized across this call
 *
 * Below is an example of explicit synchronization:
 *  
 *      SynchronizedMap<int,int> m;
 *      SyncLock lock = m.lock();   // synchronize across all calls 
 *                                  // until explicitly unlocked
 *      m.set()       
 *      for ( SynchronizedMap<int,int>::interator iter = m.begin()
 *            ; iter != m.end()
 *            ; iter ++ )
 *          iter->second = 0;
 *      lock.unlock()
 *
 * See SynchronizedMapTests for usage
 */
template <typename KeyT, typename ValueT>
class SynchronizedMap {
public:
	typedef std::map<KeyT, ValueT> ImplT;
	typedef typename std::map<KeyT, ValueT>::iterator iterator;
	typedef typename std::map<KeyT, ValueT>::const_iterator const_iterator;
	typedef SynchronizedMap<KeyT, ValueT> MyT;
	typedef std::pair<KeyT, ValueT> PairT;

	/**
	 * Default constructor.
	 */
	SynchronizedMap<KeyT, ValueT>() :
			_impl(), _sync() {}

	virtual ~SynchronizedMap<KeyT, ValueT>() {}

	/**
	 * Push a new instance of T on to the map.
     */
	typename std::pair<typename std::map<KeyT, ValueT>::iterator, bool>
	insert(const typename std::pair<const KeyT, ValueT>& keyValPair) {
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		return _impl.insert(keyValPair);
	}

	iterator begin() { return _impl.begin(); }
	iterator end() { return _impl.end(); }
	const_iterator cbegin() const { return _impl.cbegin(); }
	const_iterator cend() const { return _impl.cend(); }
	iterator find(const KeyT& key) { return _impl.find(key); }
	const_iterator find(const KeyT& key) const { return _impl.find(key); }

	MyT& set(const std::pair<KeyT, ValueT>& keyValPair) {
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		_impl[keyValPair.first] = keyValPair.second;
		return *this;
	}

	/**
	 * Obtain an external lock to this SynchronizedQueue.  Will
	 * block until locks from any other threads are released.
	 */
	SyncLock lock() const {
		return SyncLock(_sync);
	}

	/**
	 * Pop element from the back of the map.
     * NOTE: We cannot risk handing out a reference to the value 
     * as the referent could be invalidated (i.e. destroyed).
     * Hence we return the value...by value.  Bottom line, be sure
     * to implement copy constructor and assignment operator
     * if you are storing an object in the map.  In the event
     * that we must return a reference, e.g. to address performance
     * concerns, we can implement support for std::shared_ptr and
     * std::weak_ptr.
     *
     * @throws KeyNotFoundException in the event that the key does
     * not exist in the map.  
     */
	ValueT get(const KeyT& key) {
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		ImplT::const_iterator i = _impl.find(key);
		if (i == _impl.end())
			throw KeyNotFoundException(SOURCELOCATION);
		return i->second;
	}

	/**
	 * Retrieve element from back of the map.
     */
	size_t erase(const KeyT& key) {
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		return _impl.erase(key);
	}

	MyT& clear() {
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		_impl.clear();
		return *this;
	}

	/**
	 * Determine if the map is empty.
	 */
	bool empty() const { return _impl.empty(); }

	/**
	 * Determine the number of elements in the map
	 */
	size_t size() const { return _impl.size(); }

private:
	mutable fast_recursive_mutex _sync;
	ImplT _impl;
};

} // namespace KEP