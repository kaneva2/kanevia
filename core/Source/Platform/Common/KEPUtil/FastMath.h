///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define _USE_MATH_DEFINES
#include <math.h>

// DRF - Lookup Table - 70% faster than sin()/cos()
inline void FastSinCos(int deg, float& rotSin, float& rotCos) {
	// DRF - Fast Sin/Cos Lookup Table [0..720]
	struct SinCos {
		float m_sin;
		float m_cos;
	};
	static SinCos s_fastSinCos[721];
	static bool s_init = false;
	if (!s_init) {
		s_init = true;
		for (size_t d = 0; d < 721; ++d) {
			double rad = (M_PI * (double)d) / 180.0;
			s_fastSinCos[d].m_sin = (float)sin(rad);
			s_fastSinCos[d].m_cos = (float)cos(rad);
		}
	}

	int degMod = (deg % 360) + 360;
	rotSin = s_fastSinCos[degMod].m_sin;
	rotCos = s_fastSinCos[degMod].m_cos;
}

// DRF - Marsaglia's XORSHF Generator - 600% faster than rand()
inline unsigned long FastRand() {
	static unsigned long x = 123456789, y = 362436069, z = 521288629;
	unsigned long t;
	x ^= x << 16;
	x ^= x >> 5;
	x ^= x << 1;
	t = x;
	x = y;
	y = z;
	z = t ^ x ^ y;
	return z;
}

inline float FastRandFloat() {
	return (float)FastRand() / (float)ULONG_MAX;
}

inline bool FastRandBool() {
	return FastRand() & 0x01;
}
