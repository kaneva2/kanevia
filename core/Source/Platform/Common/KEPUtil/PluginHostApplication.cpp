///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <winsvc.h>
#include "TinyXML/tinyxml.h"
#include "PluginHostApplication.h"
#include "Commandline.h"
#include "Plugin.h"
#include "PluginHostConfigurator.h"
#include "Common/KEPUtil/commandline.h"
#include "Common/KEPUtil/CrashReporter.h"
#include "js.h"
#include "jsEnumFiles.h"

using namespace log4cplus;

namespace KEP {

PluginHostApplication* PluginHostApplication::ms_instance = NULL;

PluginHostApplication::PluginHostApplication(const char* name, const char* description, const char* defAppCfgFile, const char* defLogCfgFile) :
		CNTService(name, description), m_defaultAppConfigFile(defAppCfgFile == NULL ? "%(AppName).cfg" : defAppCfgFile), m_defaultLogConfigFile(defLogCfgFile == NULL ? "%(AppName)Log.cfg" : defLogCfgFile), m_configXml(NULL), m_interactive(false), m_loggerInited(false) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Application"), "PluginHostApplication::PluginHostApplication() name = " << CNTService::ServiceName());
	ms_instance = this;
}

PluginHostApplication::~PluginHostApplication() {
	ms_instance = NULL;
}

int PluginHostApplication::Main(int argc, char** argv) {
	FileName moduleFile = getProcessFileName();
	if (moduleFile.drive().length() > 0) {
		LOG4CPLUS_DEBUG(Logger::getInstance(L"STARTUP"), "Changing directory to [" << moduleFile.toString(FileName::Drive | FileName::Path).c_str() << "]");
		SetCurrentDirectoryW(Utf8ToUtf16(moduleFile.toString(FileName::Drive | FileName::Path)).c_str());
	}

	OldCommandLine cmdLine;
	OldCommandLine::BooleanParameter& interactiveFlag = cmdLine.addBoolean("-r", false, "Run application interactively in a console window (run as Windows Service if not specified)");
	OldCommandLine::StringParameter& logCfgFile = cmdLine.addString("--logcfg", m_defaultLogConfigFile, "Logging configuration file");
	OldCommandLine::BooleanParameter& pauseOnExit = cmdLine.addBoolean("--pause", false, "Pause on exit?");
	cmdLine.parse(argc, argv);

	// Install Crash Reporter (must be before any other threads)
	std::string appName = cmdLine.parameters().params().get("AppName");
	CrashReporter::Install(appName);

	// Add Crash Report Log Files
	CrashReporter::AddLogFiles(logCfgFile);

	// Initialize Logger
	InitLogger(logCfgFile);
	Logger logger = Logger::getInstance(L"Application");

	LOG4CPLUS_DEBUG(logger, "PluginHostApplication::Main, pid=" << GetCurrentProcessId());

	// are we to run on command line?
	if ((bool)interactiveFlag) {
		LOG4CPLUS_INFO(logger, "Start " << ServiceName() << " in interactive mode");
		m_interactive = true;

		// add Ctrl+C handler
		jsVerify(::SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE) != FALSE);

		if (Initialize((DWORD)argc, argv)) {
			SetIsRunning(true);
			Run();
		}

		// remove Ctrl+C handler
		jsVerify(::SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, FALSE) != FALSE);
	} else {
		// Parse for standard arguments (install, uninstall, version etc.)
		if (!ParseStandardArgs(argc, argv)) {
			if (Initialize((DWORD)argc, argv)) {
				// Didn't find any standard args so start the service
				StartService();
			}
		}
	}

	LOG4CPLUS_DEBUG(Logger::getInstance(L"STARTUP"), "Current directory [" << moduleFile.toString(FileName::Drive | FileName::Path).c_str() << "]");
	if (jsFileExists("Crash", 0)) {
		LOG4CPLUS_DEBUG(Logger::getInstance(L"STARTUP"), "Performing controlled crash!");
		CrashReporter::ForcedCrash(1);
	}

	if (pauseOnExit) {
		std::cout << "Press any key to continue." << std::endl;
		Algorithms::getCh();
	}

	// When we get here, the service has been stopped
	return GetServiceStatus().dwWin32ExitCode;
}

BOOL PluginHostApplication::OnInit(DWORD dwArgc, char** lpszArgv) {
	OldCommandLine cmdLine;
	OldCommandLine::StringParameter& appCfgFile = cmdLine.addString("--cfg", m_defaultAppConfigFile, "%(AppName) configuration file");
	cmdLine.parse(dwArgc, lpszArgv);

	Logger logger = Logger::getInstance(L"Application");

	LOG4CPLUS_DEBUG(logger, "PluginHostApplication::OnInit, pid=" << GetCurrentProcessId());
	LOG4CPLUS_INFO(logger, "Loading configurations from [" << appCfgFile.value() << "]...");
	TIXMLConfigHelper helper(m_configXml);

	try {
		helper.load(appCfgFile);
	} catch (const ConfigurationException& ce) {
		LOG4CPLUS_ERROR(logger, "Unable to load configuration from " << appCfgFile.value() << "[" << ce.str() << "]");
		return FALSE;
	}
	LOG4CPLUS_INFO(logger, "Configurations loaded");
	return TRUE;
}

void PluginHostApplication::OnStop() {
	Logger logger = Logger::getInstance(L"Application");
	LOG4CPLUS_INFO(logger, "Got shutdown signal");
	SetIsRunning(false);
}

void PluginHostApplication::OnShutdown() {
	OnStop();
}

void PluginHostApplication::Run() {
	Logger logger = Logger::getInstance(L"Application");

	try {
		LOG4CPLUS_INFO(logger, "Initializing...");
		PluginHostConfigurator configurator;
		configurator.configure(PluginHost::singleton(), m_configXml);
		LOG4CPLUS_INFO(logger, "Initialization succeeded");
	} catch (const ChainedException& e) {
		LOG4CPLUS_ERROR(logger, "Exception occurred during initialization: " << e.str());
		return;
	} catch (KEPException* e) {
		LOG4CPLUS_ERROR(logger, "Exception occurred during initialization: " << e->ToString());
		delete e;
		return;
	}

	LOG4CPLUS_INFO(logger, "Enter main loop");

	while (IsRunning()) {
		Sleep(1000);
	}

	LOG4CPLUS_INFO(logger, "Exit main loop");

	try {
		LOG4CPLUS_INFO(logger, "Shuting down...");
		PluginHost::singleton().shutdown();
		LOG4CPLUS_INFO(logger, "Shutdown completed");
	} catch (KEPException* e) {
		LOG4CPLUS_ERROR(logger, "Exception occurred during shutdown: " << e->ToString());
		delete e;
		return;
	}
}

void PluginHostApplication::InitLogger(const std::string& cfgPath) {
	if (m_loggerInited) {
		return;
	}

	char baseDir[_MAX_PATH + 1];
	baseDir[0] = '\0';
	_getcwd(baseDir, _MAX_PATH);

	std::string propFile = baseDir;
	propFile += "\\";
	propFile += cfgPath;
	std::string logDir = baseDir;
	logDir += "\\logs";

	LoggingSubsystem(propFile, logDir).start();

	m_loggerInited = true;
}

BOOL WINAPI PluginHostApplication::CtrlHandler(DWORD fdwCtrlType) {
	if (ms_instance) {
		ms_instance->OnStop();

		//Consume this event
		return TRUE;
	}

	return FALSE;
}

FileName PluginHostApplication::getProcessFileName() {
	wchar_t path[MAX_PATH + 1];
	memset(path, 0, sizeof(path));
	if (GetModuleFileNameW(GetModuleHandle(NULL), path, MAX_PATH))
		return FileName::parse(Utf16ToUtf8(path));
	return FileName();
}

} // namespace KEP

KEPUTIL_EXPORT int RunPluginHostApplication(int argc, char** argv, const char* name, const char* description, const char* defAppCfgFile, const char* defLogCfgFile) {
	// Short term quick fix for using actual executable filename as service name instead of compile value.
	KEP::OldCommandLine cl;
	KEP::PluginHostApplication theApp(name == 0 ? cl.parse(argc, argv).executable().fname().c_str() : name, description, defAppCfgFile, defLogCfgFile);
	return theApp.Main(argc, argv);
}
