///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common/include/kepcommon.h"
#include <string>
#include <map>
#include "HTTPServer.h"

namespace KEP {

/**
 * This class is intended to provide services common to all 
 * concrete implemenations and hence ease deployment of 
 * different HTTP Server implemenations.
 */
class __declspec(dllexport) AbstractHTTPServer : public HTTPServer {
public:
	typedef std::map<std::string, std::string> HeadersT;

	/**
	 * ctor.  Initially, the configuration is fixed on:
	 *		port				8080
	 *		directory access	disabled
	 *		thread count		10 
	 *
	 * Subsequent iterations will provide a configuration model.
	 */
	AbstractHTTPServer();

	/** 
	 * dtor
	 */
	virtual ~AbstractHTTPServer();

	/**
	 * Configure for handler to be called when specific uri is encountered.
	 *
	 * Where's the counterpart to deregister?  This will show up in the leak tests
	 * at the end.
	 */
	virtual HTTPServer& registerHandler(RequestHandler& handler, const std::string& uri);
	virtual RequestHandler* deregisterHandler(const std::string& uri);
	virtual HTTPServer::RequestHandler& getDefaultURIHandler();

	/**
	 * Given a spefic uri path e.g. "/info" | "/", obtain a handler
	 * to be executed.  If no registered handler is found, the default handler
	 * is returned.
	 */
	virtual RequestHandler& getHandler(const std::string& uri);
	virtual HTTPServer& setThreadCount(unsigned long threadCount);
	virtual unsigned long threadCount() const { return _threadCount; }
	virtual HTTPServer& enableDirectories(bool dirEnabled = false);
	virtual bool directoriesEnabled() const { return _dirEnabled; }

	/**
	 * Instruct HTTPServer to listen on a port (bind to port).
	 *
	 * @param bindPort		The port to which to bind.
	 * @param secure        Flags communications on this port as using SSL (secure)
	 *						Note that enabling ssl on a port requires that a 
	 *						certificate be specified via setCertificate.
	 * @param pcHost        For use in multi-homed machines.  Specifies the interface
	 *						to bind to.
	 */
	virtual HTTPServer& addPortBinding(unsigned short bindPort, bool secure = false, const char* pcHost = 0);

	virtual unsigned short bindPort() const { return _bindPort; }
	virtual HTTPServer& setDocumentRoot(const std::string& docRoot) {
		_docRoot = docRoot;
		return *this;
	}
	virtual std::string documentRoot() const { return _docRoot; }
	virtual HTTPServer& enableFileServing(bool enabled = false);
	virtual bool fileServingEnabled() const { return _fileServingEnabled; }
	virtual HTTPServer& start() { return *this; }
	virtual HTTPServer& stop() { return *this; }

	virtual HTTPServer& setACL(const std::string& acl) {
		_acl = acl;
		return *this;
	}
	virtual const std::string& acl() const { return _acl; }

	virtual const std::string& lastError() const { return _lastError; }

protected:
	virtual HTTPServer& setLastError(const std::string lastError) {
		_lastError = lastError;
		return *this;
	}

private:
	/**
	 * mongoose specific start up routine.  When composite model is put in place
	 * this implementation will be moved to the Mongoose implementation of the
	 * HTTPServer interface.
	 */
	bool _fileServingEnabled;
	std::string _docRoot;
	typedef std::map<std::string, HTTPServer::RequestHandler*> HandlerMapT;
	HandlerMapT _handlerMap;
	unsigned long _threadCount;
	bool _dirEnabled;
	unsigned short _bindPort;
	bool _secure;
	std::string _acl;
	RequestHandler* _endpointEnumerationHandler;
	std::string _lastError;
};

} // namespace KEP