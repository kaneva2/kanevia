///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include "common/keputil/SDFHelper.h"
#include "Core/Util/ChainedException.h"

using namespace std;
bool SDFHelper::readDataFile(string file_name, vector<vector<string>>& out_vector, int count /*= -1*/
	,
	int offset /*= 0*/) {
	// Guard clause.
	//
	if (count == 0)
		return true;

	ifstream config_to_read;
	config_to_read.open(file_name.c_str());

	if (!config_to_read.is_open()) {
		stringstream ss;
		ss << "Error reading \"" << file_name << "\"";
		throw FileException(SOURCELOCATION, ss.str());
	}

	int lineCount = 0;

	while (!config_to_read.eof() && out_vector.size() < (unsigned int)count) {
		string current_line;
		getline(config_to_read, current_line);
		vector<string> current_info = SDFHelper::parseDataLine(current_line);

		// if there are no elements (i.e. an empty or commented line)
		// don't bother...just get the next line.
		//
		if (current_info.size() <= 0)
			continue;

		// Test and increment our line count to see if we are in
		// the offset window.  If not just the get the next line.
		//
		if (lineCount++ < offset)
			continue;

		out_vector.push_back(current_info);
	}
	config_to_read.close();

	return true;
}

vector<string> SDFHelper::parseDataLine(const string& dataLine) {
	vector<string> returnVector;

	// If the string is empty...don't bother.
	//
	if (dataLine.size() <= 0)
		return returnVector;

	// If the line is a comment...don't bother.
	//
	if (dataLine.at(0) == ';')
		return returnVector;

	size_t start_pos = 0;
	size_t found_pos;

	do {
		found_pos = dataLine.find("|", start_pos);
		if (found_pos != string::npos) {
			returnVector.push_back(dataLine.substr(start_pos, found_pos - start_pos));
			start_pos = found_pos + 1;
		}
	} while (found_pos != string::npos);

	returnVector.push_back(dataLine.substr(start_pos));
	return returnVector;
}
