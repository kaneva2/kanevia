///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/fast_mutex.h"

namespace KEP {

/**
 * Class for externalizing the synchronizing element of the
 * SynchronizedQueue.
 *
 * See SynchronizedMap and SynchronizedMapTests for usage examples
 */
class SyncLock {
public:
	/**
	 * Construct a lock with an instance of a SynchronizedQueue.
	 */
	SyncLock(fast_recursive_mutex& sync) :
			_lock(new std::lock_guard<fast_recursive_mutex>(sync)), _sync(&sync) {}

	SyncLock() :
			_lock(0), _sync(0) {
	}

	SyncLock(const SyncLock& other) :
			_lock(0), _sync(other._sync) {
		if (_sync)
			_lock = new std::lock_guard<fast_recursive_mutex>(*_sync);
	}

	SyncLock& operator=(const SyncLock& rhs) {
		if (!rhs._sync)
			return *this;
		if (_lock) {
			delete _lock;
		}
		_sync = rhs._sync;
		_lock = new std::lock_guard<fast_recursive_mutex>(*_sync);
		return *this;
	}

	/**
		* Destructor releases the lock.
		*/
	virtual ~SyncLock() {
		if (_lock)
			delete _lock;
	}

	/**
     * Explicitly unlock
     */
	SyncLock& unlock() {
		if (_lock)
			delete _lock;
		_sync = 0;
		_lock = 0;
		return *this;
	}

private:
	fast_recursive_mutex* _sync;
	std::lock_guard<fast_recursive_mutex>* _lock;
};

} // namespace KEP