///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"

class CKEPUtilApp : public CWinApp {
public:
	CKEPUtilApp();

	virtual BOOL InitInstance() override;

	/** DRF - Overrides CWinApp::ProcessWndProcException()
	* This is where we land with some MFC exceptions. If we needed to show a message or something, we 
	* could do that here. However, in most cases we just want to cause MFC to throw the exception out 
	* to CrashRpt.
	*/
	virtual LRESULT ProcessWndProcException(CException* e, const MSG* pMsg) override { THROW_LAST(); }

	DECLARE_MESSAGE_MAP()
};
