///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common/keputil/plugin.h"
#include "Common/keputil/ExtendedConfig.h"
#include "TinyXML/tinyxml.h"

namespace KEP {

const char* const TAG_PLUGINHOST = "PluginHost";
const char* const TAG_PLUGINS = "Plugins";
const char* const TAG_PLUGIN = "Plugin";
const char* const TAG_PLUGIN_CONFIG = "PluginConfig";
const char* const TAG_INSTANCE_NAME = "instance_name";
const char* const TAG_FACTORY_NAME = "factory_name";
const char* const TAG_LIBRARIES = "Libraries";
const char* const TAG_LIBRARY = "Library";
const char* const TAG_LIBRARY_PATH = "path";
const char* const TAG_PLUGIN_DISABLED = "disabled";

class __declspec(dllexport) PluginHostConfigurator {
public:
	PluginHostConfigurator() {}

	/**
	 * NOTE: Correct this.  This method is misnamed.  It does indeed configure
	 * the PluginHost.  But it also mounts, sets up and starts each plugin.
	 * Assumes cfg is root element
	 */
	PluginHostConfigurator& configure(PluginHost& pluginHost, const TIXMLConfig::Item& cfg);

	PluginHostConfigurator& configure(PluginHost& pluginHost, const TIXMLConfig& config);

private:
	PluginHostConfigurator& loadExternalPluginLibraries(PluginHost& pluginHost, const TIXMLConfig::Item& libraries);

	/**
	 * Cycle up.  Current implementation is to cycle plugins into a running state in		
	 * a serial fashion to avoid dealing with race conditions.  Later iterations can
	 * improve on this logic by adding dependency support via the PluginHostListener
	 * interface.
	 */
	PluginHostConfigurator& cycleUp(PluginHost& pluginHost, const TIXMLConfig::Item& plugins);
};

} // namespace KEP