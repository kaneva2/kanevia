///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Common/KEPUtil/CommLogger.h"
#include "Core/Util/StackTrace.h"
#include "Common/KEPUtil/Algorithms.h"

namespace KEP {

// singleton semantics
CommLogger CommLogger::_singleton;
CommLogger& CommLogger::singleton() {
	return _singleton;
}

// network operation enumeration and descriptions
static const char* opstr[] = { "SEND", "RECV" };

// Compute functions
//

/**
 * Given a buffer of data, format the first leaderLength bytes
 * as a hexadecimal string
 */
std::stringstream& leader(std::stringstream& ss, const unsigned char* buf, size_t buflen) {
	if (buflen < 4) {
		ss << "err";
		return ss;
	}

	char* outbuf = new char[buflen * 2 + 1];
	sprintf(outbuf, "0x%x%x%x%x", buf[0], buf[1], buf[2], buf[3]);
	ss << outbuf;
	delete[] outbuf;
	return ss;
}

std::stringstream& computeInfo(std::stringstream& ss, ops op, NETID netId, unsigned long flags, const unsigned char* data, size_t size, std::string context /*= "" */) {
	if (context.length() > 0)
		ss << context;
	ss << " "
	   << opstr[op]
	   << " [netid=" << netId
	   << ";flags=" << flags
	   << ";dword=";
#if 1
	leader(ss, data, size);
#else
	char outbuf[11]; //  sample: 0xffffffff\0
	sprintf(outbuf, "0x%x%x%x%x", data[0], data[1], data[2], data[3]);
	ss << outbuf;
#endif
	ss << "]";

	return ss;
}

std::stringstream& computeHeader(std::stringstream& ss, ops op, NETID recipient, int flags) {
	char outbuf[256];
	sprintf(outbuf, "%s [%d][%d]", opstr[op], recipient, flags);
	ss << outbuf;
	return ss;
}

std::stringstream& remainder(std::stringstream& ss, log4cplus::LogLevel loggingLevel, ops op, NETID netId, int flags, const std::string& context, const unsigned char* data, size_t size) {
	if (context.length() > 0)
		ss << context << std::endl;

	// ss << header << endl; // "SEND [" << recipient << "][" << flags << "]" << endl;

	computeHeader(ss, op, netId, flags) << std::endl;

	// Higher overhead so use sparingly
	if (log4cplus::DEBUG_LOG_LEVEL >= loggingLevel && data) {
		ByteBufRuler::formatTo(ss, data, size);
		ss << std::endl;
	}

	// Highest overhead so use even more sparingly
	if (loggingLevel == log4cplus::TRACE_LOG_LEVEL) {
		StackTrace st;
		ss << st;
	}
	return ss; // .str();
}

std::string
CommLogger::computeLogText(ops op, log4cplus::LogLevel loggingLevel, NETID netId, unsigned long flags, const unsigned char* data, size_t size, std::string context /*= "" */) const {
	if (loggingLevel < log4cplus::TRACE_LOG_LEVEL || loggingLevel > log4cplus::INFO_LOG_LEVEL)
		return std::string();

	std::stringstream ss;

	// info level is a one liner.  The most efficient
	if (loggingLevel == log4cplus::INFO_LOG_LEVEL) {
		return computeInfo(ss, op, netId, flags, data, size, context).str();
	}

	remainder(ss, loggingLevel, op, netId, flags, context, data, size);

	return ss.str();
}

CommLogger::CommLogger() :
		_commTraceLogger(log4cplus::Logger::getInstance(L"CommTrace")) {}

void CommLogger::logSend(NETID recipient, unsigned long flags, const unsigned char* data, size_t size, std::string context /*= "" */) {
	int loggingLevel = _commTraceLogger.getLogLevel();
	if (loggingLevel < log4cplus::TRACE_LOG_LEVEL || loggingLevel > log4cplus::INFO_LOG_LEVEL)
		return;

	std::string sendmsg = computeLogText(opsend, loggingLevel, recipient, flags, data, size, context);

	_commTraceLogger.log(loggingLevel, KanevaLog4CPlus_WidenString(sendmsg));
}

void CommLogger::logReceive(NETID sender, const unsigned char* data, size_t size, std::string context /*= ""*/) {
	log4cplus::LogLevel loggingLevel = _commTraceLogger.getLogLevel();

	if (loggingLevel < log4cplus::TRACE_LOG_LEVEL || loggingLevel > log4cplus::INFO_LOG_LEVEL)
		return;

	std::string msg = computeLogText(oprecv, loggingLevel, sender, 0, data, size, context);

	_commTraceLogger.log(loggingLevel, KanevaLog4CPlus_WidenString(msg));
}

void CommLogger::log(NETID id, const std::string& context) {
	std::stringstream ss;
	if (context.length() > 0)
		ss << context << std::endl;
	ss << "[" << id << "]" << std::endl;
	_commTraceLogger.log(_commTraceLogger.getLogLevel(), KanevaLog4CPlus_WidenString(ss.str()));
}

} // namespace KEP