///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <map>
#include <vector>
#include <string>
#include <jssync.h>
#include "Core\Util\Accumulator.h"
#include "common\include\KEPException.h"

// defines for streaming support
//
template <class T>
class AccumulatorSet;

template <class T>
std::ostream& operator<<(std::ostream& out_stream, AccumulatorSet<T>& Obj);

/**
 * AccumulatorSet provides the ability to manage an homogenous set of accumulators.
 */
template <typename T>
class AccumulatorSet {
public:
	typedef std::map<std::string, int> AccumulatorMapT;
	typedef std::pair<std::string, int> AccumulatorPairT;
	typedef std::vector<Accumulator<T>*> AccumulatorVecT;

	/**
	 * Default constructor
	 */
	AccumulatorSet() :
			m_map(), m_vector(), m_enabled(true) {}

	/**
	 * Destroy this AccumulatorSet.  Note that the current implementation
	 * frees resources for all accumulators regardless of whether or not 
	 * they were added explicitly (allocated by caller) or implicitly 
	 * (allocated by AccumulatorSet).
	 */
	virtual ~AccumulatorSet() {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		setEnabled(false);
		m_vector.clear();
	}

	/**
	 * Accumulate to accumulator at index.
	 */
	AccumulatorSet& accumulate(unsigned int k, T value) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		accumulator(k).accumulate(value);
		return *this;
	}

	/**
	 * Accumulate to accumulator with key
	 * Note, if no accumulator is found for the specified key, one
	 * will implicitly created and named for that key.
	 */
	AccumulatorSet& accumulate(const std::string& k, T value) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		accumulator(k).accumulate(value);
		return *this;
	}

	/**
	 * Acquire a Accumulator based on the accumulator's name.
	 * If an accumulator with the given name is not found
	 * then one is created and added to the set.
	 *
	 * @param k		The name of the accumulator being acquired.
	 */
	Accumulator<T>& accumulator(const std::string& k) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		std::map<std::string, int>::iterator iter = m_map.find(k);
		if (iter != m_map.end())
			return *(m_vector[iter->second]);

		// The collection is empty.. create a new accumlator with this name and add it
		//
		Accumulator<T>* newAccumulator = new Accumulator<T>(k);
		add(*newAccumulator);
		return *newAccumulator;
	}

	/**
	 * Determine if this AccumulatorSet is enabled/disabled
	 * @return true if enabled, else false.
	 */
	bool isEnabled() { return _enabled; }

	/**
	 * Set this accumulator set to disabled.  Causes all accumulators managed
	 * by this set to be disabled as well.
	 */
	AccumulatorSet& setEnabled(bool enable) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		m_enabled = enable;
		for (std::vector<Accumulator<T>*>::iterator iter = m_vector.begin(); iter != m_vector.end(); iter++) {
			(*iter)->setEnabled(enable);
		}
		return *this;
	}

	/**
	 * Acquire accumulator at index.
	 */
	Accumulator<T>& accumulator(unsigned int k) {
		if ((k >= 0) && (k < m_vector.size()))
			return *(m_vector[k]);
		throw new KEPException("Invalid array index");
	}

	/**
	 * Determine how many accumulators are in the set.
	 */
	unsigned int size() { return m_vector.size(); }

	/**
	 * Add an accumulator to the set.
	 */
	AccumulatorSet<T>& add(Accumulator<T>& accumulator) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		size_t n = m_vector.size();
		m_vector.push_back(&accumulator);
		m_map.insert(AccumulatorPairT(accumulator.name(), (int)n));
		return *this;
	}

	friend std::ostream& operator<<<>(std::ostream& out_stream, AccumulatorSet<T>& Obj);

	std::ostream& streamOut(std::ostream& out_stream) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		out_stream << "<accumulators size=\"" << m_vector.size() << "\">";
		bool delim = false;
		for (unsigned int n = 0; n < m_vector.size(); n++) {
			out_stream << *(m_vector[n]);
		}
		out_stream << "</accumulators>";
		return out_stream;
	}

private:
	AccumulatorVecT m_vector;
	AccumulatorMapT m_map;
	fast_recursive_mutex m_sync;
	bool m_enabled;

	//	Accumulator<T>::Factory<T>*		_factory;
};

template <class T>
std::ostream& operator<<(std::ostream& out_stream, AccumulatorSet<T>& Obj) {
	return Obj.streamOut(out_stream);
}
