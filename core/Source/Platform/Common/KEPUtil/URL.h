///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <utility>
#include "common/include/KEPCommon.h"
#include "Common/KEPUtil/Algorithms.h"
#include "Core/Util/ChainedException.h"

namespace KEP {
class FileName;

/**
 * URL Parsing can and do throw exceptions when parsing issues arise.  
 * You should always be prepared to catch this exception when parsing
 * a URL.
 */
//class URLParseException : public ChainedException {};
CHAINED(URLParseException);

/**
 * Utility class to facilitate parsing, generation and manipulation of
 * a Universal Resource Locator(s) (URL).
 */
class __declspec(dllexport) URL {
public:
	/**
	 * Utility to performs lions share of parsing of a url into 
	 * its constituent components.  Note that path/file name component
	 * as well as query information is not decomposed.
	 * see URLTests.h for usage examples.
	 */
	class __declspec(dllexport) Parser {
	public:
		enum URLComponent {
			scheme,
			host,
			port,
			path,
			params,
			reference
		};

		Parser();
		virtual ~Parser();
		bool parse(const char* pcUrl);

		int _workspacePos;
		char* _workspace;
		int _workspaceSize;
		char* _pcScheme;
		char* _pcHost;
		char* _pcPort;
		char* _pcPath;
		char* _pcQuery;
		char* _pcRef;
	};

	static URL parse(const std::string& url);

	/**
	 * Default destructor
	 */
	URL();

	/**
	 * Copy constructor
	 */
	URL(const URL& toCopy);

	/**
     * Construct from FileName instance
     */
	URL(const FileName& fname);

	URL(const std::string& url);

	URL(const char* url);

	/**
	 * Destructor
	 */
	virtual ~URL();

	/**
	 * Assignment operator
	 */
	URL& operator=(const URL& rhs);

	/**
	 * Equality operator
	 */
	bool operator==(const URL& rhs) const;

	/**
	 * Inequality operator
	 */
	bool operator!=(const URL& rhs) const;

	/**
	 * Inequality opertor
	 */
	bool operator!=(const URL& rhs);

	/**
	* assign from a string
	*/
	URL& operator=(const std::string& rhs);

	/** 
	 * assign from a const char*
	 */
	URL& operator=(const char* url);

	/**
     * assign from a FileNameInstance.
     */
	URL& operator=(const FileName& fname);

	/**
	 * Append a path component.  Currently path management is minimal.  Only allows
	 * for appending elements to the path and acquiring string representation of the
	 * URL path.  To build a multilevel path, multiple calls must be made to this function.
	 * E.g. to build up the path /dir1/dir2/dir3/file1 you will need to peform the following
	 *
	 * URL url;
	 * url.addPath( "dir1" ).addPath( "dir2" ).addPath( "dir3" ).addPath( "file1" );
	 * 
	 * @param	pathElement		Unadorned portion of path component (e.g. "dir" not "/dir")
	 * @return	reference to self.
	 */
	URL& addPath(const std::string& pathElement);

	/**
	 * Set the value of key value pair.  If the key/val pair is not already present
	 * then the new key is appended to the end of the parameters collection.
	 * Otherwise, the existing key/value pair is updated.
	 * @param  Key/Value pair to be set in the parameter collection.
	 * @return reference to self.
	 */
	URL& setParam(const std::pair<std::string, std::string>& keyValPair);

	/**
	 * Set the value of key value pair.  If the key/val pair is not already present
	 * then the new key is appended to the end of the parameters collection.
	 * Otherwise, the existing key/value pair is updated.
	 * @param  key		key to be updated.
	 * @param  value	value with which to update the kep/value pair.
	 * @return reference to self.
	 */
	URL& setParam(const std::string& key, const std::string& value);

	/**
	 * Set the value of all key value pairs.  Performs clearParams()
	 * @return reference to self
	 */
	URL& setParams(const std::vector<std::pair<std::string, std::string>>& params);

	/**
	 * Clears the parameters referenced in this url
	 */
	URL& clearParams();

	/**
	 * Remove parameter identified by key from the parameters key/value collection
	 * If the key is not found no operation is performed and no errors are reported.
	 * If the key is found the entire entry is removed from the collection.
	 * @param  key		key of parameter to be unset.  
	 * @return reference to self.
	 */
	URL& unsetParam(const std::string& key);

	/**
	 * Return string representation of this URL.
	 * @return String representation of this URL.
	 * @deprecated use str()
	 */
	std::string toString() const;

	/**
  	 * Return string representation of this URL.
	 * @return String representation of this URL.
	 */
	std::string str() const;

	// TODO: document
	std::string encodedString();

	// TODO: document
	std::string decodedString();

	/**
	 * Return the scheme component of this URL.
	 * @return Scheme component of this URL.  Empty string if not present.
	 */
	const std::string& scheme();

	/**
	 * Set the scheme component of this URL
	 * @param scheme		String representation of the url scheme	(e.g. http, ftp, 
	 *						https etc.). Note, do not include any other decoration
	 *						(e.g. "http" not "http:"
	 * @return reference to self.
	 */
	URL& setScheme(const std::string& scheme);

	/**
	 * Return the host component of this URL.
	 * @return Host component of this URL.  Empty string if not present.
	 */
	const std::string& host();

	/**
	 * Set the host component of this URL.
	 * @return Reference to self.
	 */
	URL& setHost(const std::string& host);

	/**
	 * Return port component of this URL.  Zero if not set.
	 */
	unsigned short port();

	/**
	 * Set port component of this URL.
	 * @return Reference to self.
	 */
	URL& setPort(unsigned short port);

	/**
	 * Return reference component of this URL.  Empty string if not present.
	 */
	const std::string& reference();

	/**
	 * Set the reference component of this URL.
	 * @param	reference		Reference component of this URL.  Note
	 *							that this should not include any decoration
	 *							(e.g. "TOC" not "#TOC")
	 * @return	reference to self.
	 */
	URL& setReference(const std::string& reference);

	/**
	 * Return path component (including file name) of this URL.  
	 * Empty string if not set.
	 */
	std::string pathString() const;

	/**
	 * Set the path component of this url using a string.
	 * @return reference to self
	 */
	URL& URL::setPath(const std::string& path);

	/**
     * Append a path to the existing path component of this url
     * @param path      The path to be appended.
     * @return reference to self.
     */
	URL& URL::appendPath(const std::string& path);

	/**
	 * Return query component of this URL.  Empty string if not present.
	 */
	std::string queryString() const;

	/**
	 * Obtain a const reference to the parameters currently defined for this url.
	 */
	const std::vector<std::pair<std::string, std::string>>& params() const;

	/**
	 * Obtain a const reference to a map of parameters currently defined for this url.
	 */
	const std::map<std::string, std::string>& paramMap() const;

	/**
	 * Short cut to UrlParser::unescapeString()
	 */
	static std::string unescapeString(const std::string& s);

	/** 
	 * Short hand to UrlParser::escapeString()
	 */
	static std::string escapeString(const std::string& s);

private:
	std::string _scheme;
	std::string _host;
	unsigned short _port;
	std::string _reference;
	std::vector<std::pair<std::string, std::string>> _params;
	std::map<std::string, std::string> _paramMap;
	std::vector<std::string> _pathElements;
};

}
