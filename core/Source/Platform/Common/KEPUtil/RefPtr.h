///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


/******************************************************************************
 RefPtr was developed to help assure reference pointers are validated before
 being dereferenced.  Historically, the platform passed direct pointers to
 various userdata objects in script.  The script retained these references
 even after the pointed to objects were destroyed in the platform.  Crashes
 would occur the next time the script hands the pointer back and the platform
 tries to dereference it.

 Enable reference pointers by calling RefPtrEnable(true), disabled by default.
 Call RefPtrAdd() from the object constructor to insert the pointer into the
 list of valid reference pointers.  Call RefPtrDel() from the destructor to
 remove the pointer from the list.  Call RefPtrValid() before dereferencing
 the pointer.
******************************************************************************/
#pragma once

#include "Common/include/KEPCommon.h"
#include "KEP/Util/KEPUtil.h"
#include <string>

#ifdef NSCONFIG_EXPORTS
#define DRF_REF_PTR 0 // disable RefPtr
#else
#define DRF_REF_PTR 1 // enable RefPtr
#endif

#ifndef DRF_REF_PTR_ENABLE
#define DRF_REF_PTR_ENABLE false // default enable flag
#endif

#ifndef DRF_REF_PTR_VERBOSE
#define DRF_REF_PTR_VERBOSE false // default verbose flag
#endif

#ifndef DRF_REF_PTR_QUIET
#define DRF_REF_PTR_QUIET true // default quiet flag
#endif

#if (DRF_REF_PTR == 0)
#define RefPtrEnable(...)
#define RefPtrClear(...)
#define RefPtrLog(...)
#define RefPtrAdd(...)
#define RefPtrDel(...)
#define RefPtrValid(...) true
#define REF_PTR_ADD(...)
#define REF_PTR_ADD_QUIET(...)
#define REF_PTR_DEL(...)
#define REF_PTR_DEL_QUIET(...)
#define REF_PTR_VALID(...) true
#define REF_PTR_VALID_QUIET(...) true
#elif (DRF_REF_PTR == 1)

/** DRF
* Enables/disables reference pointers (disabled by default).
*/
KEPUTIL_EXPORT void RefPtrEnable(bool enable = DRF_REF_PTR_ENABLE, bool quiet = DRF_REF_PTR_QUIET);

/** DRF
* Clears set of valid reference pointers.
*/
KEPUTIL_EXPORT void RefPtrClear(bool quiet = DRF_REF_PTR_QUIET);

/** DRF
* Logs set of valid reference pointers.
*/
KEPUTIL_EXPORT void RefPtrLog(bool verbose = DRF_REF_PTR_VERBOSE);

/** DRF
* Adds a pointer to the set of valid reference pointers.
*/
KEPUTIL_EXPORT void RefPtrAdd(const std::string& id, void* p, bool quiet = DRF_REF_PTR_QUIET);

/** DRF
* Deletes a pointer from the set of valid reference pointers.
*/
KEPUTIL_EXPORT void RefPtrDel(const std::string& id, void* p, bool quiet = DRF_REF_PTR_QUIET);

/** DRF
* Check a pointer is in the set of valid reference pointers.
*/
KEPUTIL_EXPORT bool RefPtrValid(const char* func, const std::string& id, void* p, bool quiet = DRF_REF_PTR_QUIET);

// Convenience Macros
#define REF_PTR_ADD(p) ::RefPtrAdd(typeid(p).name(), (void*)p, DRF_REF_PTR_QUIET)
#define REF_PTR_ADD_QUIET(p) ::RefPtrAdd(typeid(p).name(), (void*)p, true)
#define REF_PTR_DEL(p) ::RefPtrDel(typeid(p).name(), (void*)p, DRF_REF_PTR_QUIET)
#define REF_PTR_DEL_QUIET(p) ::RefPtrDel(typeid(p).name(), (void*)p, true)
#define REF_PTR_VALID(p) ::RefPtrValid(__FUNCTION__, typeid(p).name(), (void*)p, DRF_REF_PTR_QUIET)
#define REF_PTR_VALID_QUIET(p) ::RefPtrValid(__FUNCTION__, typeid(p).name(), (void*)p, true)

#endif // DRF_REF_PTR