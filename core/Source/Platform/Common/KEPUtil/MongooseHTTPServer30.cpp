///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

// Based on Mongoose console application implementation at
//		http://code.google.com/p/mongoose/
//
// Copyright (c) 2004-2009 Sergey Lyubka
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OF OR CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// Initial iteration of this unit, it is still kinda messy.
//TODO: Move httpserver and mongoose files to keputil...for now.  We'll move them
//		 when we move this to a blade.
#if defined(_WIN32)
//#define _CRT_SECURE_NO_WARNINGS  // Disable deprecation warning in VS2005
#else
#define _XOPEN_SOURCE 600 // For PATH_MAX on linux
#endif

#include <sstream>
#include "mongoosehttpserver.h"
#ifdef MONGOOSE_3_0
#include "common/include/kepconfig.h"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdarg.h>

#include <log4cplus/logger.h>
using namespace log4cplus;
#include "mongoose3.0/mongoose.h"
#include "Common/include/UrlParser.h"
#include <boost/tokenizer.hpp>

#include "TinyXML/tinyxml.h"

#ifdef _WIN32
#else
#include <sys/wait.h>
#include <unistd.h>
#define DIRSEP '/'
#define WINCDECL
#endif // _WIN32

namespace KEP {

static Logger _logger = Logger::getInstance(L"HTTPServer");
class MongooseEndPoint : public HTTPServer::EndPoint {
public:
	MongooseEndPoint(mg_connection& conn) :
			_conn(conn) {}

	virtual ~MongooseEndPoint() {}

	EndPoint& operator<<(const BaseResponse& response) {
		return write(response);
	}

	EndPoint& write(const BaseResponse& response) {
		LOG4CPLUS_TRACE(Logger::getInstance(L"HTTPServer"), "MongooseEndPoint::write()");
		const void* content = response.buffer();
		unsigned long contentLength = response.bufferSize();

		// write managed headers...headers that the server controls...not the user.
		// The distinction between managed and unmanaged headers will change
		// as needs demand, but now, the return code, content length, and connection
		// closure are not some of them.
		//
		mg_printf(&_conn, "HTTP/1.1 200 OK\r\n");
		mg_printf(&_conn, "Content-Length: %d\r\n", contentLength);
		mg_printf(&_conn, "Connection: close\r\n");

		// write headers
		const HTTPServer::HeadersT& headers = response.headers();
		for (auto iter = headers.cbegin(); iter != headers.cend(); iter++) {
			mg_printf(&_conn, "%s: %s\r\n", iter->first.c_str(), iter->second.c_str());
		}
		mg_printf(&_conn, "\r\n"); // write the content delimiter

		// Don't use mg_printf() as it has some size limitations
		//
		// mg_printf(&_conn, "%s", payload.c_str() );
		mg_write(&_conn, content, contentLength);

		LOG4CPLUS_DEBUG(Logger::getInstance(L"HTTPServer"), "MongooseEndPoint::write(): Call complete");
		return *this;
	}

	EndPoint& sendError(int status, const std::string& msg) {
		mg_send_http_error(&_conn, status, msg.c_str(), "%s", msg.c_str());

		return *this;
	}

private:
	mg_connection& _conn;
};

void CopyMongooseRequestToHTTPServerRequest(const mg_request_info& source, HTTPServer::Request& target) {
	target._userData = source.user_data; // User-defined pointer passed to mg_start()
	target._requestMethod = source.request_method; // "GET", "POST", etc
	target._uri = source.uri; // URL-decoded URI
	target._httpVersion = source.http_version; // E.g. "1.0", "1.1"
	if (source.query_string)
		target._queryString = source.query_string; // \0 - terminated
	if (source.remote_user)
		target._remoteUser = source.remote_user; // Authenticated user
	if (source.log_message)
		target._logMessage = source.log_message; // Mongoose error log message
	target._remoteIP = source.remote_ip; // Client's IP address
	target._remotePort = source.remote_port; // Client's port
	target._statusCode = source.status_code; // HTTP reply status code
	target._isSSL = source.is_ssl; // 1 if SSL-ed, 0 if not

	// Copy the query string parameters into a map
	//
	typedef boost::char_separator<char> separatorT;
	typedef boost::tokenizer<separatorT> tokenizerT;
	tokenizerT t(target._queryString, separatorT("&"));
	for (tokenizerT::iterator ti = t.begin(); ti != t.end(); ti++) {
		tokenizerT t2(*ti, separatorT("="));
		tokenizerT::iterator ti2 = t2.begin();
		std::string key(UrlParser::unescapeString(*ti2));
		ti2++;
		std::string val(UrlParser::unescapeString(*ti2));
		target._paramMap.insert(std::pair<std::string, std::string>(key, val));
	}

	for (int n = 0; n < source.num_headers; n++) {
		target._headerMap.insert(std::pair<std::string, std::string>(source.http_headers[n].name, source.http_headers[n].value));
	}
}

void* my_mg_callback(enum mg_event event,
	struct mg_connection* conn,
	const struct mg_request_info* request_info) {
	MongooseHTTPServer& wrapper = *((MongooseHTTPServer*)request_info->user_data);

	LOG4CPLUS_TRACE(_logger, "my_mg_callback()");

	// For some reason every request is handled twice.  Once on reciept and once on handle new connection.
	//  This is because the browser is implicitly requesting /favicon.ico
	switch (event) {
		case MG_NEW_REQUEST: //,   // New HTTP request has arrived from the client
		{
			// Our initial integration is crude at best.  Here we are deferring
			// much of the implementation details of configuring, identifying,
			// instantiating  uri handlers.  We still go the HTTPServer to do this,
			// however only to get a "default" handler.
			//
			// Migrate mongoose request structure to HTTPServer request structure
			//
			HTTPServer::Request request;

			// Initial implementation of HTTPServer::Request is simply a wrapper around mongoose's request type.
			//
			CopyMongooseRequestToHTTPServerRequest(*request_info, request);
			MongooseEndPoint endPoint(*conn);
			HTTPServer::RequestHandler& rh = wrapper.getHandler(request._uri);
			LOG4CPLUS_DEBUG(_logger, "Dispatching to handler for uri [" << request._uri << "] to handler [" << rh.name() << "]");
			void* pvoid = (void*)rh.handle(request, endPoint);
			if (pvoid > 0) {
				LOG4CPLUS_DEBUG(_logger, "Request [" << request._uri << "] handled by [" << rh.name() << "]");
			} else {
				LOG4CPLUS_WARN(_logger, "Request [" << request._uri << "] NOT handled BY  [" << rh.name() << "]");
			}
			return pvoid;
		}
		case MG_HTTP_ERROR: // HTTP error must be returned to the client
		{
			if (request_info->log_message == 0)
				return (void*)0;
			LOG4CPLUS_ERROR(_logger, "Mongoose Log Event [" << request_info->log_message << "]");
			return (void*)1;
		} break;
		case MG_EVENT_LOG: // Mongoose logs an event, request_info.log_message
		{
			if (request_info->log_message == 0)
				return (void*)0;

			wrapper.setLastError(request_info->log_message);
			LOG4CPLUS_ERROR(_logger, "Mongoose Log Event [" << request_info->log_message << "]");
			return (void*)1;
		} break;
		case MG_INIT_SSL: // Mongoose initializes SSL. Instead of mg_connection *,
			// SSL context is passed to the callback function.
			break;
	};
	return (void*)0; // not-zero indicates that the request was handled here...not further processing
		// necessary.
}

static char* sdup(const char* str) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"HTTPServer"), "mongooselib: sdup(\"" << str << "\")");
	char* p;
	int bufsize = strlen(str) + 1;
	if ((p = (char*)malloc(bufsize)) != NULL) {
		//	  printf( "Allocated [%x][%s]\n", p, str );
		LOG4CPLUS_DEBUG(Logger::getInstance(L"HTTPServer"), "mongooselib: sdup(\"" << str << "\") Calling strcpy_s(\"" << (void*)p << "\"," << strlen(p) << ",\"" << str << "\") bufsize=" << bufsize);
		strcpy_s(p, bufsize, str);
	}
	return p;
}

static void set_option(char** options, const char* name, const char* value) {
	int i;
	LOG4CPLUS_TRACE(Logger::getInstance(L"HTTPServer"), "mongooselib: set_option(" << options << ",\"" << name << "\",\"" << value << "\") options=" << options);

	// 3xN
	for (i = 0; i < MAX_OPTIONS - 3; i++) {
		LOG4CPLUS_DEBUG(Logger::getInstance(L"HTTPServer"), "mongooselib: set_option() options=" << options);

		// Don't allow us to set more than once
		if (options[i] != NULL)
			continue;
		options[i] = sdup(name);
		options[i + 1] = sdup(value);
		options[i + 2] = NULL;
		break;
	}

	if (i == MAX_OPTIONS - 3) {
	}
}

static void process_command_line_arguments(char* argv[], char** options) {
	options[0] = NULL;
	// Now handle command line flags. They override config file settings.
	for (size_t i = 1; argv[i] != NULL; i += 2) {
		if (argv[i][0] != '-' || argv[i + 1] == NULL) {
		}
		set_option(options, &argv[i][1], argv[i + 1]);
	}
}

HTTPServer& MongooseHTTPServer::start() {
	// Mongoose configuration options
	// Option   Description					Default Value
	// ======   =========================== ================
	//  "C"		cgi_extensions				".cgi,.pl,.php"
	//  "E"		cgi_environment				NULL
	//  "G"		put_delete_passwords_file	NULL
	//  "I"		cgi_interpreter"			NULL
	//  "P"		protect_uri					NULL
	//  "R"		authentication_domain		"mydomain.com"
	//  "S"		ssi_extensions				".shtml,.shtm"
	//  "a"		access_log_file				NULL
	//  "c"		ssl_chain_file				NULL
	//  "d"		enable_directory_listing	"no"
	//  "e"		error_log_file				NULL
	//  "g"		global_passwords_file		NULL
	//  "i"		index_files					"index.html,index.htm,index.cgi"
	//  "k"		enable_keep_alive			"no"
	//  "l"		access_control_list			NULL
	//  "M"		max_request_size			"16384",
	//  "m"		extra_mime_types			NULL,
	//  "p"		listening_ports				"8080"
	//  "r"		document_root				"."
	//  "s"		ssl_certificate				NULL
	//, "t"		num_threads					"10"
	//  "u"		run_as_user					NULL
	//
	// Mongoose takes a single call to set_options to set the bindings
	// i.e. multiple calls do not accumulate, they overwrite
	// Bindings will have been collected in _bindings.
	//
	std::stringstream ss;
	std::string delim;
	for (std::vector<PortBinding>::const_iterator i = _bindings.begin(); i != _bindings.end(); i++) {
		ss << delim << i->toString();
		delim = ",";
	}
	LOG4CPLUS_INFO(_logger, " Binding to: " << ss.str().c_str());
	set_option(_options, "p", ss.str().c_str());

	_ctx = mg_start(my_mg_callback, this, (const char**)_options /*_config_options */);
	return *this;
}

MongooseHTTPServer::MongooseHTTPServer() :
		AbstractHTTPServer(), _ctx(0) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"HTTPServer"), "MongooseHTTPServer::MongooseHTTPServer() options=[" << _options << "]");

	// Here we intialize as if the array is a 1xN.
	//
	memset(_options, 0, MAX_OPTIONS * sizeof(char*) * 3);
}

MongooseHTTPServer::~MongooseHTTPServer() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"HTTPServer"), "MongooseHTTPServer::~MongooseHTTPServer()");
	stop();
	LOG4CPLUS_TRACE(Logger::getInstance(L"HTTPServer"), "MongooseHTTPServer::~MongooseHTTPServer() : MongooseHTTPServer::stop() complete!");

	// todo:  dereg handlers?

	// Free options.  This isn't obvious, but we call mongoose set_option with this collection.
	// mongoose.set_option() calls sdup.  Normally this would be managed by Mongoose, but since
	// we've option to manage an external options array, we have to clean this up ourselves.
	//
	LOG4CPLUS_DEBUG(Logger::getInstance(L"HTTPServer"), "MongooseHTTPServer::~MongooseHTTPServer(): cleaning up options array");

	// Here we accessing as 3xN
	//
	int m = MAX_OPTIONS * 3;
	for (int i = 0; i < m; i++) {
		if (_options[i] == 0)
			continue;
		free(_options[i]);
		_options[i] = 0;
	}
}

HTTPServer&
MongooseHTTPServer::stop() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"HTTPServer"), "MongooseHTTPServer::stop()");
	if (_ctx) {
		mg_stop(_ctx);
		_ctx = 0;
	}
	LOG4CPLUS_DEBUG(Logger::getInstance(L"HTTPServer"), "MongooseHTTPServer::stop(): mg_stop() complete!");
	return AbstractHTTPServer::stop();
}

HTTPServer&
MongooseHTTPServer::setThreadCount(unsigned long threadCount) {
	char ac[256];
	_itoa_s(threadCount, ac, 10);
	set_option(_options, "t", ac);
	return AbstractHTTPServer::setThreadCount(threadCount);
}

HTTPServer&
MongooseHTTPServer::setDocumentRoot(const std::string& docRoot) {
	set_option(_options, "r", docRoot.c_str());
	return AbstractHTTPServer::setDocumentRoot(docRoot);
}

std::string
MongooseHTTPServer::documentRoot() const {
	//mg_get_option( _ctx, "r" );   // Just use our stored version.
	return AbstractHTTPServer::documentRoot();
}

HTTPServer&
MongooseHTTPServer::enableDirectories(bool dirEnabled) {
	set_option(_options, "d", dirEnabled ? "yes" : "no");
	return AbstractHTTPServer::enableDirectories(dirEnabled);
}

HTTPServer&
MongooseHTTPServer::setCertificate(const std::string& cert) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"HTTPServer"), "MongooseHTTPServer::setCertificate( " << cert << " )");
	set_option(_options, "s", cert.c_str());
	return *this;
}

HTTPServer&
MongooseHTTPServer::setACL(const std::string& acl) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"HTTPServer"), "MongooseHTTPServer::setACL( " << acl << " )");
	AbstractHTTPServer::setACL(acl);
	set_option(_options, "l", acl.c_str());
	return *this;
}

const std::string&
MongooseHTTPServer::acl() const {
	return AbstractHTTPServer::acl();
}

HTTPServer&
MongooseHTTPServer::addPortBinding(unsigned short bindPort, bool secure, const char* pcHost) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"HTTPServer"), "MongooseHTTPServer::addsetBindPort("
															<< bindPort
															<< (secure ? ", secure " : "") << ") options=" << _options);

	_bindings.push_back(PortBinding(bindPort, secure, (pcHost == 0 ? "" : pcHost)));
	return *this;
	// return AbstractHTTPServer::setBindPort( bindPort, secure );
}

MongooseHTTPPluginFactory::MongooseHTTPPluginFactory() :
		AbstractPluginFactory("HTTPServer"), _config(0) {
}

MongooseHTTPPluginFactory::MongooseHTTPPluginFactory(KEPConfig* config) :
		AbstractPluginFactory("HTTPServer"), _config(config) {
}

MongooseHTTPPluginFactory::~MongooseHTTPPluginFactory() {
}

const std::string& MongooseHTTPPluginFactory::name() const {
	return AbstractPluginFactory::name();
}

/**
 *
 */
PluginPtr MongooseHTTPPluginFactory::createPlugin(const TIXMLConfig::Item& configItem) const {
	TiXmlElement* config = configItem._internal;
	LOG4CPLUS_TRACE(Logger::getInstance(L"HTTPServer"), "MongooseHTTPPluginFactory::createPlugin()");

	LOG4CPLUS_DEBUG(Logger::getInstance(L"HTTPServer"), (configItem == TIXMLConfig::nullItem() ? "null" : configItem.toString().c_str()));

	MongooseHTTPServer* pserv = new MongooseHTTPServer();

	//const char* const TAG_PORT					= "Port";
	const char* const TAG_THREAD_COUNT = "ThreadCount";
	const char* const TAG_DOCROOT = "DocumentRoot";
	const char* const TAG_DIR_ENABLED = "EnableDirectories";
	const char* const TAG_FILE_SERVING_ENABLED = "EnableFileServing";

	// Guard clause.  If no configuration provided, we can go with
	// default settings.  We're done here.
	//
	if (config == 0)
		return PluginPtr(new HTTPServerPlugin(*pserv));

	const char* pc = config->Attribute(TAG_THREAD_COUNT);
	if (pc != 0)
		pserv->setThreadCount(atoi(pc));

	pc = config->Attribute(TAG_DOCROOT);
	if (pc != 0)
		pserv->setDocumentRoot(pc);

	pc = config->Attribute(TAG_DIR_ENABLED);
	if (pc != 0)
		pserv->enableDirectories(_strcmpi("true", pc) == 0);
	else
		pserv->enableDirectories(false);

	pc = config->Attribute(TAG_FILE_SERVING_ENABLED);
	if (pc != 0)
		pserv->enableFileServing(_strcmpi("true", pc) == 0);
	else
		pserv->enableFileServing(false);

	pc = config->Attribute("certificate");
	if (pc != 0)
		pserv->setCertificate(pc);

	pc = config->Attribute("acl");
	if (pc != 0)
		pserv->setACL(pc);

	// Iterate over the the port binding configurations
	//
	for (TIXMLConfig::Item ci = configItem.firstChild("binding"); ci != TIXMLConfig::NullItem; ci = ci.nextSibling()) {
		// Cast the Node back to an element
		if (StringHelpers::lower(ci.name()) != "binding")
			continue;

		int port = ci.attribute("port", 0);
		if (port == 0) {
			LOG4CPLUS_WARN(Logger::getInstance(L"HTTPServer"), "Missing port specification!");
			continue;
		}

		const std::string pcHost = ci.attribute("host");
		const std::string pcSecure = ci.attribute("secure");

		pserv->addPortBinding(port, _strcmpi(pcSecure.c_str(), "true") == 0, pcHost.c_str());
	}

	return PluginPtr(new HTTPServerPlugin(*pserv));
}

} // namespace KEP

#endif // ifdef MONGOOSE_3_0