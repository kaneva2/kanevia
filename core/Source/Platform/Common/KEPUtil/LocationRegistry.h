///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/KEPUtil/SynchronizedMap.h"
#include "Core/Util/ChainedException.h"
#include "Common/include/CoreStatic/InstanceId.h"
#include "Core/Math/Vector.h"
#include "Log4CPlus/logger.h"

namespace KEP {

class __declspec(dllexport) LocationRegistry {
public:
	class Entry {
		friend bool operator==(const Entry& lhs, const Entry& rhs) {
			return lhs._netId == rhs._netId && lhs._zoneInstanceId == rhs._zoneInstanceId && lhs._zoneChannelId == rhs._zoneChannelId && lhs._zoneType == rhs._zoneType && lhs._coords == rhs._coords;
		}

		friend bool operator!=(const Entry& lhs, const Entry& rhs) {
			return !(lhs == rhs);
		}

		friend std::ostream& operator<<(std::ostream& os, const Entry& obj) {
			return os
				   << "_netId: " << obj._netId
				   << " _zoneInstanceId: " << obj._zoneInstanceId
				   << " _zoneChannelId: " << obj._zoneChannelId
				   << " _zoneType: " << obj._zoneType;
			//				<< " _coords: " << obj._coords;
		}

	public:
		Entry(unsigned long net_id, unsigned long zoneInstanceId, unsigned long zoneChannelId, unsigned long zoneType, const std::pair<Vector3f, Vector3f>& coords) :
				_netId(net_id), _zoneInstanceId(zoneInstanceId), _zoneChannelId(zoneChannelId), _zoneType(zoneType), _coords(coords) {}

		Entry() {}

		// setget
		unsigned long netId() const {
			return _netId;
		}

		Entry(const Entry& other) :
				_netId(other._netId), _coords(other._coords), _zoneInstanceId(other._zoneInstanceId), _zoneChannelId(other._zoneChannelId), _zoneType(other._zoneType) {}

		Entry& operator=(const Entry& other) {
			if (this == &other)
				return *this;
			_netId = other._netId;
			_zoneInstanceId = other._zoneInstanceId;
			_zoneChannelId = other._zoneChannelId;
			_zoneType = other._zoneType;
			_coords = other._coords;
			return *this;
		}

		Entry& setNetId(unsigned long net_id) {
			_netId = net_id;
			return *this;
		}

		const std::pair<Vector3f, Vector3f>& coords() const {
			return _coords;
		}

		Entry& setCoords(const std::pair<Vector3f, Vector3f>& coords) {
			_coords = coords;
			return *this;
		}

		unsigned long zoneType() const {
			return _zoneType;
		}

		unsigned long zoneInstanceId() const {
			return _zoneInstanceId;
		}

		unsigned long zoneChannelId() const {
			return _zoneChannelId;
		}

		Entry& setZoneIndex(const ZoneIndex& zone_index) {
			_zoneInstanceId = zone_index.GetInstanceId();
			_zoneChannelId = zone_index.GetChannelId();
			_zoneType = zone_index.GetType();
			return *this;
		}

	private:
		unsigned long _netId;
		unsigned long _zoneInstanceId;
		unsigned long _zoneChannelId;
		unsigned long _zoneType;
		std::pair<Vector3f, Vector3f> _coords;
	};

public:
	typedef SynchronizedMap<unsigned long, std::pair<Vector3f, Vector3f>> MapT;
	typedef SynchronizedMap<unsigned long, Entry> MapT1;

	static LocationRegistry& singleton();

	LocationRegistry() {}

	virtual ~LocationRegistry() {}

	LocationRegistry& registerPlayerLocation(
		unsigned long netID,
		const ZoneIndex& zoneIndex,
		const std::pair<Vector3f, Vector3f>& posrot) {
		_map1.set(MapT1::PairT(netID, Entry(netID, zoneIndex.GetInstanceId(), zoneIndex.GetChannelId(), zoneIndex.GetType(), posrot)));
		return *this;
	}

	LocationRegistry& unregister(unsigned long netID) {
		try {
			_mapPrev1.set(MapT1::PairT(netID, _map1.get(netID))); // throws KeyNotFoundException
			_map1.erase(netID);
		} catch (KeyNotFoundException&) {
		} // Do nothing if not found

		return *this;
	}

	std::pair<Vector3f, Vector3f> getPlayerLocation(unsigned long netID) {
		return _map1.get(netID).coords();
	}

	LocationRegistry& entries(std::vector<Entry>& entries) {
		auto lock = _map1.lock();
		for (MapT1::const_iterator iter = _map1.cbegin(); iter != _map1.cend(); iter++)
			entries.push_back(Entry(iter->second));
		return *this;
	}

	LocationRegistry& csv(std::vector<std::string>& entries) {
		auto lock = _map1.lock();

		std::stringstream ss;
		for (MapT1::const_iterator iter = _map1.cbegin(); iter != _map1.cend(); iter++) {
			auto netId = iter->second.netId();
			auto coords = iter->second.coords();
			ss << netId
			   << "," << iter->second.zoneInstanceId()
			   << "," << iter->second.zoneChannelId()
			   << "," << iter->second.zoneType()
			   << "," << coords.first.X()
			   << "," << coords.first.Y()
			   << "," << coords.first.Z();
			entries.push_back(ss.str());
			ss.clear();
		}

		return *this;
	}

	// Read and erase last know player location in previous zone
	std::pair<Vector3f, Vector3f> fetchPlayerLocationInPreviousZone(unsigned long netID) {
		LOG4CPLUS_TRACE(log4cplus::Logger::getInstance(L"LocationRegistry"), "LocationRegistry(" << this << ")::getPlayerLocation()");
		;
		try {
			auto lock = _mapPrev1.lock();
			auto res = _mapPrev1.get(netID);
			_mapPrev1.erase(netID);
			return res.coords();
		} catch (KeyNotFoundException&) {
			// LocationRegistry::unregister should have already been called at this moment
			// Fall back to current location map anyway
			return getPlayerLocation(netID);
		}
	}

private:
	MapT1 _map1;
	MapT1 _mapPrev1; // Location map to backup last-know location in previous zone

	static LocationRegistry* _singleton;
	static std::mutex _initSync;
};

} // namespace KEP