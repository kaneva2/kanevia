///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "URL.h"
#include "Common/KEPUtil/Algorithms.h"
#include "Common/include/UrlParser.h"

namespace KEP {

URL::URL() :
		_reference(), _params(), _paramMap(), _port(0), _host() {}

URL::URL(const URL& toCopy) :
		_reference(toCopy._reference), _scheme(toCopy._scheme), _params(toCopy._params), _paramMap(toCopy._paramMap), _pathElements(toCopy._pathElements), _port(toCopy._port), _host(toCopy._host) {}

URL::URL(const std::string& url) {
	*this = parse(url);
};

URL::URL(const char* url) {
	*this = parse(std::string(url));
}

URL::URL(const FileName& fname) :
		_scheme("file"), _host(), _port(0), _reference(), _pathElements() {
	StringHelpers::explode(_pathElements, fname.dir(), "/");
	_pathElements.push_back(fname.fname());
}

URL::~URL() {}

URL& URL::operator=(const URL& rhs) {
	_reference = rhs._reference;
	_paramMap = rhs._paramMap;
	_params = rhs._params;
	_pathElements = rhs._pathElements;
	_port = rhs._port;
	_host = rhs._host;
	_scheme = rhs._scheme;
	return *this;
}

URL& URL::operator=(const std::string& rhs) {
	*this = parse(rhs);
	return *this;
};

URL& URL::operator=(const FileName& fname) {
	_reference = "";
	_paramMap.clear();
	_params.clear();
	_pathElements.clear();
	if (fname.drive().length() > 0)
		_pathElements.push_back(fname.drive());
	StringHelpers::explode(_pathElements, StringHelpers::ltrim(fname.dir(), "/"), ":/");
	_pathElements.push_back(fname.toString(FileName::Name | FileName::Ext));
	_port = 0;
	_host = "";
	_scheme = "file";
	return *this;
}

URL& URL::operator=(const char* url) {
	*this = parse(std::string(url));
	return *this;
}

bool URL::operator==(const URL& rhs) const {
	if (&rhs == this)
		return true;
	if (_scheme != rhs._scheme)
		return false;
	if (_host != rhs._host)
		return false;
	if (_port != rhs._port)
		return false;
	if (_reference != rhs._reference)
		return false;
	if ((_pathElements.size() > 0) && (pathString() != rhs.pathString()))
		return false;
	std::string lhs = queryString();
	std::string rhss = rhs.queryString();
	if ((_params.size() > 0) && (lhs != rhss))
		return false;
	if ((_paramMap.size() > 0) && (lhs != rhss))
		return false;
	return true;
}

bool URL::operator!=(const URL& rhs) const {
	return !operator==(rhs);
}

bool URL::operator!=(const URL& rhs) {
	return !operator==(rhs);
}
URL& URL::setScheme(const std::string& scheme) {
	_scheme = scheme;
	return *this;
}
URL& URL::setHost(const std::string& host) {
	_host = host;
	return *this;
}
URL& URL::setPort(unsigned short port) {
	_port = port;
	return *this;
}
URL& URL::setReference(const std::string& reference) {
	_reference = reference;
	return *this;
}
URL& URL::addPath(const std::string& pathElement) {
	_pathElements.push_back(pathElement);
	return *this;
}
URL& URL::clearParams() {
	_params.clear();
	_paramMap.clear();
	return *this;
}

URL& URL::setParams(const std::vector<std::pair<std::string, std::string>>& params) {
	clearParams();

	for (auto iter = params.begin(); iter != params.end(); iter++) {
		_params.push_back(std::make_pair(iter->first, iter->second));
		_paramMap.insert(std::make_pair(iter->first, iter->second));
	}
	return *this;
}

URL& URL::setParam(const std::pair<std::string, std::string>& keyValPair) {
	bool found = false;
	for (auto iter = _params.begin(); iter != _params.end(); iter++) {
		if (iter->first != keyValPair.first)
			continue;
		iter->second = keyValPair.second;
		found = true;
		break;
	}
	if (!found) {
		_params.push_back(keyValPair);
		_paramMap.insert(keyValPair);
	}

	return *this;
}

URL& URL::setParam(const std::string& key, const std::string& value) {
	return setParam(std::make_pair(key, value));
}

URL& URL::unsetParam(const std::string& key) {
	for (auto iter = _params.begin(); iter != _params.end(); iter++) {
		if (iter->first != key)
			continue;
		_params.erase(iter);
		auto i = _paramMap.find(key);
		if (i != _paramMap.end())
			_paramMap.erase(i);
		break;
	}
	return *this;
}

const std::string& URL::scheme() {
	return _scheme;
}
const std::string& URL::host() {
	return _host;
}
unsigned short URL::port() {
	return _port;
}
const std::string& URL::reference() {
	return _reference;
}

std::string URL::pathString() const {
	std::stringstream ss;
	std::string delim;
	for (auto iter = _pathElements.begin(); iter != _pathElements.end(); iter++) {
		if (StringHelpers::startsWith("/", *iter))
			delim = "";

		ss << delim << (*iter);
		delim = "/";
	}
	std::string t = ss.str();
	return ss.str();
}

const std::vector<std::pair<std::string, std::string>>& URL::params() const {
	return _params;
}
const std::map<std::string, std::string>& URL::paramMap() const {
	return _paramMap;
}

std::string URL::unescapeString(const std::string& s) {
	return UrlParser::unescapeString(s);
}

std::string URL::escapeString(const std::string& s) {
	return UrlParser::escapeString(s);
}

std::string URL::queryString() const {
	std::stringstream ss;
	std::string delim;

	for (auto iter = _params.cbegin(); iter != _params.cend(); iter++) {
		ss << delim << (*iter).first << "=" << UrlParser::escapeString((*iter).second);
		delim = "&";
	}
	return ss.str();
}

std::string URL::str() const {
	return toString();
}

std::string URL::toString() const {
	std::stringstream ss;
	if (_scheme.length() > 0)
		ss << _scheme << "://";
	ss << _host;
	if (_port > 0)
		ss << ":" << _port;

	if (_pathElements.size() > 0)
		if (StringHelpers::lower(_scheme) == "file")
			ss << pathString();
		else
			ss << "/" << pathString();
	if (_params.size() > 0)
		ss << "?" << queryString();
	if (_reference.length() > 0)
		ss << "#" << _reference;
	return ss.str();
}

std::string URL::encodedString() {
	return toString();
}

std::string URL::decodedString() {
	return UrlParser::unescapeString(toString());
}

URL& URL::setPath(const std::string& path) {
	_pathElements.clear();
	StringHelpers::explode(_pathElements, path, "\\/");
	return *this;
}

URL& URL::appendPath(const std::string& path) {
	StringHelpers::explode(_pathElements, path, "\\/");
	return *this;
}

URL URL::parse(const std::string& url) {
	Parser parser;
	if (!parser.parse(url.c_str()))
		throw URLParseException(SOURCELOCATION);

	URL ret;
	if (parser._pcScheme > 0)
		ret.setScheme(parser._pcScheme);
	if (parser._pcHost > 0)
		ret.setHost(parser._pcHost);
	if (parser._pcPort > 0)
		ret.setPort(atoi(parser._pcPort));
	if (parser._pcRef > 0)
		ret.setReference(parser._pcRef);
	if (parser._pcPath > 0)
		StringHelpers::explode(ret._pathElements, std::string(parser._pcPath), "\\/");

	if (parser._pcQuery == 0)
		return ret; // Done if no query data parsed.

	std::vector<std::string> params;
	StringHelpers::explode(params, parser._pcQuery, "&");

	for (auto iter = params.begin(); iter != params.end(); iter++) {
		const std::string& subject = *iter;
		std::string::size_type i = subject.find('=');
		if (i == std::string::npos)
			continue;
		std::string key = subject.substr(0, i);
		std::string value = subject.substr(i + 1);
		ret._params.push_back(std::make_pair(key, value));
		ret._paramMap.insert(std::make_pair(key, value));
	}

	return ret;
}

URL::Parser::Parser() :
		_workspaceSize(0), _workspace(0), _pcScheme(0), _pcHost(0), _pcPort(0), _pcPath(0), _pcQuery(0), _pcRef(0), _workspacePos(0) {}

URL::Parser::~Parser() {
	if (_workspace != 0)
		delete[] _workspace;
}

bool URL::Parser::parse(const char* pcUrl) {
	if (pcUrl == 0)
		throw URLParseException(SOURCELOCATION, "Attempted to parse null url.");
	int urlLen = strlen(pcUrl);
	if (urlLen == 0)
		throw URLParseException(SOURCELOCATION, "Attempted to parse empty url.");
	_workspaceSize = strlen(pcUrl) + 5;
	_workspace = new char[_workspaceSize];

	int urlPos = 0;
	char currChar = 0;
	URLComponent component = scheme;

	for (urlPos = 0; urlPos < urlLen; urlPos++) {
		currChar = pcUrl[urlPos];

		switch (currChar) {
			case '#':
				switch (component) {
					case scheme: _pcHost = _workspace;
					case host:
					case port:
					case path:
					case params:
						component = reference;
						currChar = 0;
						_pcRef = _workspace + _workspacePos + 1;
						break;
					default:
						return false;
				};
				break;
			case '?':
				switch (component) {
					case scheme: _pcHost = _workspace;
					case host:
					case port:
					case path:
						currChar = 0;
						component = params;
						_pcQuery = _workspace + _workspacePos + 1;
						break;
					default:
						return false;
				};
				break;
			case '/':
			case '\\':
				switch (component) {
					case params:
					case path: break;
					case scheme:
						if (_pcHost == 0)
							_pcHost = _workspace;
					case host:
					case port:
						_pcPath = _workspace + _workspacePos + 1;
						currChar = 0;
						component = path;
						break;
					default:
						return false;
				};
				break;
			case ':': {
				switch (component) {
					case scheme: {
						// We think we are scanning the scheme, but it's possible no scheme was supplied.
						// if the next two chars are '//' then we are indeed in the scheme, elese we were
						// in the host component
						//
						if ((pcUrl[urlPos + 1] == '/') && (pcUrl[urlPos + 2] == '/')) {
							// confirmed we were in the scheme
							currChar = 0;
							_pcScheme = _workspace;
							_pcHost = _pcScheme + urlPos + 1;
							if (_strnicmp(_pcScheme, "FILE", 4) == 0) {
								_pcPath = _pcHost;
								_pcHost = 0;
								*(_pcScheme + urlPos) = 0;
								component = path;
							} else {
								component = host;
							}
							urlPos += 2;
						} else if (strchr("0123456789", pcUrl[urlPos + 1])) {
							// we were in the host
							_pcHost = _workspace;
							_pcPort = _workspace + _workspacePos + 1;
							currChar = 0;
							component = port;
						} else {
							return false;
						}
					} break;
					// We were processing the host key
					//
					case host: {
						if (!strchr("0123456789", pcUrl[urlPos + 1]))
							return false;
						currChar = 0;
						component = port;
						_pcPort = _workspace + _workspacePos + 1;
					} break;
				};
				break;
			}
			default:
				switch (component) {
					case port:
						if (!strchr("0123456789", pcUrl[urlPos]))
							return false;
					default: break;
				};
		};
		_workspace[_workspacePos++] = currChar;
	}
	_workspace[_workspacePos] = 0;
	if ((_pcHost == 0) && (component == scheme) && (urlPos > 0)) {
		_pcHost = _workspace;
	}
	return true;
}

} // namespace KEP