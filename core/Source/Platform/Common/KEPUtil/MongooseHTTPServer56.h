///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#pragma comment(lib, "Mongoose5.6.lib")

#include <string>
#include <map>

#include "common/include/kepcommon.h"
#include "common/include/kepconfig.h"
#include "AbstractHTTPServer.h"
#include "Plugin.h"

struct mg_connection;
struct mg_request_info;
struct mg_context;
enum mg_event;

class TiXmlElement;

//namespace mg56 {
// Mongoose specific forward declaration

#define MAX_OPTIONS 40

/**
 * This class serves as a very thin veneer over the Mongoose HTTP
 * server.  For the most part it exists to localize all compile
 * dependencies on Mongoose.  This includes a veneers over the
 * end point, request and response objects.
 * Next steps in the evolution of this class is to set out
 * interface/implementation composition model.
 * 
 * Note: This class is built on top of a modified version 3.0 of the 
 * Mongoose embedded http server. See:
 *
 *       void mg_send_http_error(	struct mg_connection *conn
 *								    , int status 
 * 									, const char *reason
 * 									, const char *fmt, ...)
 * in mongoose.c/mongoose.h
 * If the decision is made to upgrade to a newer version of Mongoose,
 * we will need to be sure that this type of functionality exists
 */

class __declspec(dllexport) MongooseHTTPServer : public AbstractHTTPServer {
public:
	/**
	 * ctor.  Initially, the configuration is fixed on:
	 *		port				8080
	 *		directory access	disabled
	 *		thread count		10 
	 *
	 * Subsequent iterations will provide a configuration model.
	 */
	MongooseHTTPServer();

	/** 
	 * dtor
	 */
	virtual ~MongooseHTTPServer();

	/**
	 * Start the server's internal processing.  This call is synchronous.  I.e.
	 * when this call returns all internals of the server are running.
	 */
	virtual HTTPServer& start();

	/**
	 * Stop the server's internal processing.  This call is synchronous.  I.e.
	 * when this call returns all internals of the server are stopped.
	 */
	virtual HTTPServer& stop();

	virtual HTTPServer& setThreadCount(unsigned long threadCount);
	virtual HTTPServer& enableDirectories(bool dirEnabled = false);

	/**
	 * @see HTTPServer
	 */
	virtual HTTPServer& setCertificate(const std::string& cert);

	/**
	 * @see HTTPServer
	 */
	virtual HTTPServer& setACL(const std::string& acl);
	virtual const std::string& acl() const;

	/**
	 * @see HTTPServer
	 */
	virtual HTTPServer& addPortBinding(unsigned short bindPort, bool secure = false, const char* pcHost = 0);
	virtual HTTPServer& setDocumentRoot(const string& docRoot);
	virtual string documentRoot() const;

private:
	static class PortBinding {
	public:
		PortBinding(unsigned short port, bool secure = false, const char* host = 0) :
				_port(port), _secure(secure), _host(host) {}

		PortBinding(const PortBinding& other) :
				_port(other._port), _secure(other._secure), _host(other._host) {}

		virtual ~PortBinding() {}

		std::string toString() const {
			stringstream ss;
			if (_host.length() > 0 && _host != "*")
				ss << _host << ":";
			ss << _port;
			if (_secure)
				ss << "s";
			return ss.str();
		}

		unsigned short _port;
		std::string _host;
		bool _secure;
	};

	std::vector<PortBinding> _bindings;

	/**
	 * mongoose specific start up routine.  When composite model is put in place
	 * this implementation will be moved to the Mongoose implementation of the
	 * HTTPServer interface.
	 */
	mg_context* _ctx;
	unsigned long _threadCount;
	char* _options[MAX_OPTIONS * 3];

	friend void* my_mg_callback(mg_event event,
		mg_connection* conn,
		const mg_request_info* request_info);
};

class __declspec(dllexport) MongooseHTTPPluginFactory : public AbstractPluginFactory {
public:
	MongooseHTTPPluginFactory();
	MongooseHTTPPluginFactory(KEPConfig* config);
	virtual ~MongooseHTTPPluginFactory();

	const std::string& name() const;
	PluginPtr createPlugin(const TIXMLConfig::Item& config) const;

private:
	KEPConfig* _config;
};
