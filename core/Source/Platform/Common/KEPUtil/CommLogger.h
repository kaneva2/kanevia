///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <log4cplus/Logger.h>
#include "Common/Include/KEPCommon.h"
#include "Tools/NetworkV2/NetworkTypes.h"
#include "KEP/Util/KEPUtil.h"

namespace KEP {

// network operation enumeration and descriptions
//static const char* opstr[]; //  = { "SEND","RECV" };
enum ops { opsend,
	oprecv };

/**
 * Helper class for logging all network IO in a ByteRuled format with optional
 * support for logging the call stack for where io operation (read send/receive)
 * occured.
 * 
 * This class is completely self sufficient and can readily be declared as a 
 * local static.
 *
 * Configuration occurs solely via the log configuration file.  
 * To use this class:
 *  <ol>
 *    <li>Create an instance of the class.</li>
 *    <li>Call the logSend() or logReceive() method as appropriate.</li>
 *    <li>Configure the logger in the log configuration file (e.g. 
 *    KEPLog.cfg/ServerLog.cfg).</li>
 *  </ol>
 * Failing to define the logger, or 
 * or assign a priority lower than debug will effectively disable
 * Comm Trace Logging.  Setting the priority level to DEBUG will 
 * cause the subject IO buffer to be logged in Byte Ruled format.
 * Setting the priority level to TRACE will cause the subject IO buffer
 * to be output in Byte Ruled format as well as the complete call stack 
 * for where the logging is initiated. 
 * 
 * The intent is to allow users to place a call to CommLogger immediately
 * after a send or receive and determine what was sent, even if
 * the content was in binary form AND provide context for the call.
 *
 * Note that depending on where CommLogger calls are made, this
 * generated a HUGE amount of logging data, and hence should be
 * enabled judiciously.
 */
class CommLogger {
	// because we need to be able to defer initialization until after
	static CommLogger _singleton;

public:
	/**
     *  Default constructor
     */
	KEPUTIL_EXPORT CommLogger();

	/**
	 * Singleton semantics.  Acquire a reference to a 
	 * well know CommLogger instance.
	 */
	KEPUTIL_EXPORT static CommLogger& singleton();

	/**
     * Log data sent.
     *
     * @param   recipient   NETID of the intended recipient.
     * @param   flags       Flags applied to send the data.
     * @param   data        Pointer to the data sent.
     * @param   size        The amount of data sent, to insure that only the data
     *                      transmitted is logged and more importantly to
     *                      no buffer overrun occurs.
     */
	void KEPUTIL_EXPORT logSend(NETID recipient, unsigned long flags, const unsigned char* data, size_t size, std::string context = "");

	/**
     * Log data received.
     *
     * @param   sender      NETID of the sender.
     * @param   data        Pointer to the data received.
     * @param   size        The amount of data received, to insure that only the data
     *                      received is logged and more importantly that
     *                      no buffer overrun occurs.
     */
	void KEPUTIL_EXPORT logReceive(NETID sender, const unsigned char* data, size_t size, std::string context = "");

	/**
	 * General purpose logging (DEBUG logging level).
	 * @param semder	NET_ID for receiver/sender
	 * @param msg		The message to be logged.
	 */
	void KEPUTIL_EXPORT log(NETID sender, const std::string& context);

	/**
	 * Obtain a reference to this CommLoggers internal log4cplus logger
	 */
	log4cplus::Logger& logger() { return _commTraceLogger; }

	/**
	 * Generate the output content without actually logging the message.
	 * Handy for unit testing.
	 */
	std::string KEPUTIL_EXPORT computeLogText(ops op, log4cplus::LogLevel loggingLevel, NETID netId, unsigned long flags, const unsigned char* data, size_t size, std::string context /*= "" */) const;

private:
	log4cplus::Logger _commTraceLogger;
};

} // namespace KEP