///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "Reference.h"

namespace KEP {

IReference::IReference() :
		m_count(0) {
} // IReference::IReference

unsigned int
IReference::AddReference() {
	return ++m_count;
} // IReference::AddReference

unsigned int
IReference::Release() {
	if (!(--m_count)) {
		delete this;
		return 0; // m_count == 0 but no longer accessible
	}
	return m_count;
} // IReference::Release
} // namespace KEP
