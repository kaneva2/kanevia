///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
#include <string>
/**
 * Initial implementation of a class for parsing standard delimited files
 * where each line in the file represents a tuple and delimited piece in
 * the line is a field.  
 *
 * This class does not require a square structure, but your usage might.
 * I.e. This class does enforce any aspect of the file, number of records
 * number of fields in a record, nada!  It simply create a vector of 
 * vectors of strings.
 */
class __declspec(dllexport) SDFHelper {
public:
	static std::vector<std::string> parseDataLine(const std::string& dataLine);
	static bool readDataFile(std::string file_name, std::vector<std::vector<std::string>>& out_vector, int count = -1, int offset = 0);
};
