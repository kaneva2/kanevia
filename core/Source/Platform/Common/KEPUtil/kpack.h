///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>
#include <functional>

namespace KEP {
namespace KPack {

// Unpack Files Within .pak
void UnpackFiles(const std::string& pakFilePath, const std::string& unpackPath, size_t& numFilesUnpacked, std::function<void(const std::string&)> progressUpdate = nullptr);

// Pack files (new signature incompatible with references from Distribution.cpp)
void PackFiles(const std::string& pakFilePath, const std::vector<std::string> filesToPack, std::function<void(const std::string&)> progressUpdate = nullptr);

}
}
