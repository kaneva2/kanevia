///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <map>
#include "common/keputil/plugin.h"

namespace KEP {

typedef std::pair<std::string, std::string> Header;
typedef std::map<std::string, std::string> HeaderMapT;

/**
 * This class serves to formalize how an HTTPServer handler responds
 * to a calling client and provide support for common operations
 * like writing automatic headers (e.g. content-type), allow for 
 * arbitrary headers.  Also, provides mechanism for seamlessly
 * dispatching wide v. narrow content. 
 */
class BaseResponse {
public:
	const std::string& header(const std::string& name) const {
		// map<>::operator[] is not const
		// So we need to drop the constness of "this" before we access by index
		// if we intend to advertise that this function is const...i.e. does not
		// modify this instance.
		//
		return const_cast<BaseResponse*>(this)->_headers[name];
	}

	virtual BaseResponse& setHeader(const Header& header) { return setHeader(header.first, header.second); }

	virtual BaseResponse& setHeader(const std::string& name, const std::string& value) {
		_headers[name] = value;
		return *this;
	}
	const std::map<std::string, std::string>& headers() const { return _headers; }
	virtual const void* buffer() const = 0;
	virtual size_t bufferSize() const = 0;

private:
	std::map<std::string, std::string> _headers;
};

template <class ContentT>
class TypedResponse : public BaseResponse {
public:
	std::basic_stringstream<ContentT>& outputStream() { return _content; }
	size_t bufferSize() const { return _content.str().length() * sizeof(ContentT); }
	const void* buffer() const {
		_contents = _content.str();
		return (void*)_contents.c_str();
	}

	virtual TypedResponse<ContentT>& setHeader(const std::string& name, const std::string& value) {
		BaseResponse::setHeader(name, value);
		return *this;
	}

	virtual TypedResponse<ContentT>& operator<<(const std::basic_string<ContentT> content) {
		_content << content;
		return *this;
	}

	virtual TypedResponse<ContentT>& operator<<(unsigned int content) {
		_content << content;
		return *this;
	}

	virtual TypedResponse<ContentT>& operator<<(const Header& h) {
		BaseResponse::setHeader(h);
		return *this;
	}

private:
	std::basic_stringstream<ContentT> _content;
	mutable std::basic_string<ContentT> _contents;
};

/**
 * This class sets forth a consistent interface for interacting
 * with an embedded HTTPServer independent of the actual HTTP
 * implementation being used.  Currently we are using Mongoose HTTP
 * as our implementation.  
 * 
 * This includes defining interfaces for:
 * <ul>
 *  <li>end point</li>
 *  <li>request objects</li>
 *  <li>response objects</li>
 * </ul>
 * 
 * Limitations:
 *
 * 	Currently HTTPServer has no multi-homing support.  I.e. no ability to
 *  bind to specific NIC.
 */
class HTTPServer {
public:
	typedef std::shared_ptr<HTTPServer> ptr_t;
	typedef std::map<std::string, std::string> HeadersT;

	typedef TypedResponse<wchar_t> ResponseW;
	typedef TypedResponse<char> Response;

	/**
	 * This class encapsulates the mechanism for responding to
	 * a request and should be thought of as the client's
	 * proxy on the web server.  In a call/response model such 
	 * as HTTP, a request handler, more often than not, needs 
	 * to send data back to the caller.   This is down by 
	 * writing data to the EndPoint.  
	 *
	 * The intent is to hide the underlying implementation
	 * (currently Mongoose), but this may not always be the 
	 * case.  If and when we do, we will be able to localize 
	 * most if not all the necessary mods to this class.
	 */
	class EndPoint {
	public:
		/**
		 * Write a Response object to the output stream.  E.g. 
		 * result code, headers, content etc...
		 */
		virtual EndPoint& write(const BaseResponse& response) = 0;
		virtual EndPoint& operator<<(const BaseResponse& response) = 0;

		/**
		 * Dispatch an error to the caller.
		 * e.g. 
		 *      sendError( 404, "File not found" );
		 */
		virtual EndPoint& sendError(int errNo, const std::string& msg) = 0;
	};

	/**
	 * This class sets forth a format for describing a request
	 * from a client.  It is modeled on the request structure 
	 * used by Mongoose.
	 *
	 * The intent is to hide the underlying implementation.  
	 * We are currently using Mongoose as our HTTP server 
	 * implementation, but may not always be the case.  If 
	 * and when we do opt for a different implementation, we 
	 * will be able to minimize, if not completely preclude, 
	 * the need to modify users of this class.  I.e. we won't 
	 * have to go through our entire codebase looking for 
	 * references to the Mongoose request structure.  We'll 
	 * be able to localize any required modifications to 
	 * this class.
	 */
	class Request {
	public:
		void* _userData; // User-defined pointer passed to mg_start()
		std::string _requestMethod; // "GET", "POST", etc
		std::string _uri; // URL-decoded URI
		std::string _httpVersion; // E.g. "1.0", "1.1"
		std::string _queryString; // \0 - terminated
		std::string _remoteUser; // Authenticated user
		std::string _logMessage; // Mongoose error log message
		long _remoteIP; // Client's IP address
		int _remotePort; // Client's port
		int _statusCode; // HTTP reply status code
		int _isSSL; // 1 if SSL-ed, 0 if not

		std::map<std::string, std::string> _paramMap;
		std::map<std::string, std::string> _headerMap;
	};

	/**
	 * This Abstract Base Class sets forth the contract to be met by "handler" of
	 * incoming HTTP requests.
	 */
	class RequestHandler {
	public:
		/**
		 * Construct with a name.  This name can be use to dynamically
		 * bind the handler to an endpoint.
		 */
		RequestHandler(const std::string& name) :
				_name(name) {}

		/**
		 * Construct with a name.  This name can be use to dynamically
		 * bind the handler to an endpoint.
		 */
		RequestHandler(const char* name) :
				_name(std::string(name)) {}

		/**
		 * Reports the name of the handler
		 *
		 * @returns the name of the handler
		 */
		const std::string& name() const { return _name; }

		/**
		 * Reports the version of the handler.
		 */
		virtual unsigned int version() const { return 0; }

		/**
		 * Handle the request.
		 * 
		 * @param	request		The incoming request
		 * @param   endpoint    to which the response is written
		 * @returns  0 to handling to continue, 1 to indicate that the
		 *			 request was handled
		 */
		virtual int handle(const Request& request, EndPoint& endPoint) = 0;

	private:
		std::string _name;
	};

	/**
	 * A handler to be used when a handler cannot be identified.  In this
	 * case, we do nothing, or play dead.  But this could be used to 
	 * generate 404 errors.  
	 */
	class DefaultRequestHandler : public HTTPServer::RequestHandler {
	public:
		DefaultRequestHandler() :
				HTTPServer::RequestHandler("DefaultHandler") {}
		int handle(const HTTPServer::Request& /*request*/
			,
			HTTPServer::EndPoint& /*ep*/) {
			return 0;
		}
	};

	/**
	 * This handler is installed when file serving is disabled.  
	 * Generates a 404 and sends it to the client
	 */
	class NoFileSupportHandler : public RequestHandler {
	public:
		NoFileSupportHandler() :
				HTTPServer::RequestHandler("NoFileSupportHandler") {}
		int handle(const HTTPServer::Request& /*request*/
			,
			HTTPServer::EndPoint& ep) {
			ep.sendError(404, "File not found!");
			return 1;
		}
	};

	// Handlers should always be stateless, so we can keep a single
	// default handler instance right here
	//
	static DefaultRequestHandler defaultRequestHandler;
	static NoFileSupportHandler noFileSupportHandler;

	/**
	 * ctor.  Initially, the configuration is fixed on:
	 *		port				8080
	 *		directory access	disabled
	 *		thread count		10 
	 *
	 * Subsequent iterations will provide a configuration model.
	 */
	HTTPServer();

	/** 
	 * dtor
	 */
	virtual __declspec(dllexport) ~HTTPServer();

	/**
	 * Configure for handler to be called when specific uri is encountered.
	 *
	 * Where's the counterpart to deregister?  This will show up in the leak tests
	 * at the end.
	 */
	virtual HTTPServer& registerHandler(RequestHandler& handler, const std::string& uri) = 0;

	/**
	 * Remove handler and uri mapping.
	 *
	 * @param uri		The uri to which the handler is mapped.
	 * @return			Pointer to the newly removed handler if it exists, else returns null.
	 */
	virtual RequestHandler* deregisterHandler(const std::string& uri) = 0;

	/**
	 * Start the server's internal processing.  This call is synchronous.  I.e.
	 * when this call returns all internals of the server are running.
	 */
	virtual HTTPServer& start() = 0;

	/**
	 * Stop the server's internal processing.  This call is synchronous.  I.e.
	 * when this call returns all internals of the server are stopped.
	 */
	virtual HTTPServer& stop() = 0;

	/**
	 * Obtain a reference to the default URI handler.  The server always has
	 * a handler that is prepared to handle the unexpected.  If you don't 
	 * provide one, HTTPServer will provide for you.
	 */
	virtual RequestHandler& getDefaultURIHandler() = 0;

	/**
	 * Given a spefic uri path e.g. "/info" | "/", obtain a handler
	 * to be executed.  If no registered handler is found, the default handler
	 * is returned.
	 */
	virtual RequestHandler& getHandler(const std::string& uri) = 0;
	virtual HTTPServer& setThreadCount(unsigned long threadCount) = 0;
	virtual unsigned long threadCount() const = 0;
	virtual HTTPServer& enableDirectories(bool dirEnabled = false) = 0;
	virtual bool directoriesEnabled() const = 0;

	/**
	 * Specify path and name of cerficate file to be used for ssl
	 * 
	 * @param cert		Path and name of certificate file.
	 */
	virtual HTTPServer& setCertificate(const std::string& cert) = 0;

	/**
	 * Set the Access Control List
	 *
	 * From Mongoose Documentation:
	 *
	 *		ACL Support
	 *		An Access Control List (ACL) allows restrictions to be put on the 
	 *		list of IP addresses which have access to the web server. In the 
	 *		case of the Mongoose web server, the ACL is a comma separated list 
	 *		of IP subnets, where each subnet is prepended by either a ?-? or 
	 *		a ?+? sign. A plus sign means allow, where a minus sign means deny. 
	 *		If a subnet mask is omitted, such as ?-1.2.3.4?, this means to deny
	 *		only that single IP address.
	 *
	 *		Subnet masks may vary from 0 to 32, inclusive. The default setting 
	 *		is to allow all, and on each request the full list is traversed - 
	 *		where the last match wins.
	 *
	 *		The ACL can be specified either at runtime, using the -l option, or
	 *		by using ?l" in the config file. For example, to allow only 
	 *		the 192.168.0.0/16 subnet to connect, you would run the following 
	 *		command:
	 *
	 *				./mongoose -l -0.0.0.0/0,+192.168.0.0/16
	 *
	 *		The ACL can also be set in the web server config file. Using the 
	 *		example above, the config file line would be:
	 *
	 *				l       -0.0.0.0/0,+192.168.0.0/16
	 *
	 *      See HTTPServerTestSuite for usage example.
	 *
	 * @param acl			String of format
	 *						<-|+><ip>/[<mask>],,,
	 */
	virtual HTTPServer& setACL(const std::string& acl) = 0;
	virtual const std::string& acl() const = 0;

	// Questionable whether this belongs here as the method of attaining this
	// info is implementation specific, but this is the way Mongoose does it.
	// So let's do it.
	virtual const std::string& lastError() const = 0;

	/**
	 * Instruct HTTPServer to listen on a port (bind to port). Note that currently
	 * there is no mechanism for querying port bindings.  Implement when need arises.
	 *
	 * @param bindPort		The port to which to bind.
	 * @param secure        Flags communications on this port as using SSL (secure)
	 *						Note that enabling ssl on a port requires that a 
	 *						certificate be specified via setCertificate.
	 * @param pcHost        For use in multi-homed machines.  Specifies the interface
	 *						to bind to.
	 * @return				Reference to self.
	 */
	virtual HTTPServer& addPortBinding(unsigned short bindPort, bool secure = false, const char* pcHost = 0) = 0;

	virtual HTTPServer& setDocumentRoot(const std::string& docRoot) = 0;
	virtual std::string documentRoot() const = 0;
	virtual HTTPServer& enableFileServing(bool enabled = false) = 0;
	virtual bool fileServingEnabled() const = 0;
};

///// Plugin Adapter for HTTPServer
class HTTPServerPlugin
		: public HTTPServer,
		  public AbstractPlugin {
public:
	HTTPServerPlugin(HTTPServer& impl) :
			_impl(impl), AbstractPlugin("HTTPServer") {
		addSupportedInterface("HTTPServer");
	}

	HTTPServerPlugin(const HTTPServerPlugin& other) :
			_impl(other._impl), AbstractPlugin(other) {
		assert(false);
	}

	virtual ~HTTPServerPlugin() { delete (&_impl); }

	HTTPServerPlugin& operator=(const HTTPServerPlugin&) {
		assert(false);
		return *this;
	}

	virtual HTTPServer& start() { return _impl.start(); }

	virtual HTTPServer& stop() { return _impl.stop(); }

	virtual HTTPServer::RequestHandler& getDefaultURIHandler() { return _impl.getDefaultURIHandler(); }

	virtual HTTPServer::RequestHandler& getHandler(const std::string& path) { return _impl.getHandler(path); }

	virtual HTTPServer& setThreadCount(unsigned long count) { return _impl.setThreadCount(count); }

	virtual unsigned long threadCount() const { return _impl.threadCount(); }

	virtual HTTPServer& enableDirectories(bool dirEnabled = false) { return _impl.enableDirectories(dirEnabled); }

	virtual bool directoriesEnabled() const { return _impl.directoriesEnabled(); }

	virtual HTTPServer& setCertificate(const std::string& cert) { return _impl.setCertificate(cert); }

	virtual HTTPServer& setACL(const std::string& acl) { return _impl.setACL(acl); }
	virtual const std::string& acl() const { return _impl.acl(); }

	virtual const std::string& lastError() const { return _impl.lastError(); }

	virtual HTTPServer& addPortBinding(unsigned short port, bool secure = false, const char* host = 0) { return _impl.addPortBinding(port, secure, host); }

	virtual HTTPServer& setDocumentRoot(const std::string& docRoot) { return _impl.setDocumentRoot(docRoot); }

	virtual std::string documentRoot() const { return _impl.documentRoot(); }

	virtual HTTPServer& enableFileServing(bool enabled = false) { return _impl.enableFileServing(enabled); }

	virtual bool fileServingEnabled() const { return _impl.fileServingEnabled(); }

	virtual HTTPServer& registerHandler(HTTPServer::RequestHandler& handler, const std::string& path) { return _impl.registerHandler(handler, path); }

	virtual HTTPServer::RequestHandler* deregisterHandler(const std::string& path) { return _impl.deregisterHandler(path); }

	// Plugin support
	//
	const std::string& name() { return AbstractPlugin::name(); }

	unsigned int version() { return AbstractPlugin::version(); }

	Plugin& startPlugin() {
		_impl.start();
		return AbstractPlugin::startPlugin();
	}

	ACTPtr stopPlugin() {
		_impl.stop();
		return AbstractPlugin::stopPlugin();
	}

private:
	HTTPServer& _impl;
};

} // namespace KEP