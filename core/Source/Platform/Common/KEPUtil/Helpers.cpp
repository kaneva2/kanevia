///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Helpers.h"
#include "WindowsAPICompat.h"
#include "libcpuid/libcpuid.h"
#include "common\include\NetworkCfg.h"

static LogInstance("Instance");

// VersionHelpers.h is only available through Windows SDK 8.1
bool IsWindowsVistaOrGreater() {
	OSVERSIONINFOEXW ovi;
	ZeroMemory(&ovi, sizeof(ovi));
	ovi.dwOSVersionInfoSize = sizeof(ovi);
	ovi.dwMajorVersion = 6;
	ovi.dwMinorVersion = 0;
	ovi.wServicePackMajor = 0;

	ULONGLONG mask = 0;
	VER_SET_CONDITION(mask, VER_MAJORVERSION, VER_GREATER_EQUAL);
	VER_SET_CONDITION(mask, VER_MINORVERSION, VER_GREATER_EQUAL);
	VER_SET_CONDITION(mask, VER_SERVICEPACKMAJOR, VER_GREATER_EQUAL);
	return VerifyVersionInfoW(&ovi, VER_MAJORVERSION | VER_MINORVERSION | VER_SERVICEPACKMAJOR, mask) != FALSE;
}

#include "Winver.h"
#pragma comment(lib, "Version.lib")
bool GetSysInfo(SysInfo& sysInfo) {
	// Get This Info Once
	static SysInfo s_sysInfo;
	bool ok = true;
	if (!s_sysInfo.m_bInitialized) {
		s_sysInfo.m_bInitialized = true;

		// Get Windows Version Hack
		typedef LONG(WINAPI * tRtlGetVersion)(RTL_OSVERSIONINFOEXW*);
		RTL_OSVERSIONINFOEXW ovi;
		memset(&ovi, 0, sizeof(RTL_OSVERSIONINFOEXW));
		ovi.dwOSVersionInfoSize = sizeof(RTL_OSVERSIONINFOEXW);
		HMODULE h_NtDll = GetModuleHandleW(L"ntdll.dll");
		tRtlGetVersion f_RtlGetVersion = (tRtlGetVersion)GetProcAddress(h_NtDll, "RtlGetVersion");
		ok = (f_RtlGetVersion ? (f_RtlGetVersion(&ovi) == 0) : false);
		if (!ok) {
			LogError("ntdll.dll::RtlGetVersion() FAILED");
		} else {
			s_sysInfo.m_majorVersion = ovi.dwMajorVersion;
			s_sysInfo.m_minorVersion = ovi.dwMinorVersion;
			s_sysInfo.m_buildNumber = ovi.dwBuildNumber;
			StrBuild(s_sysInfo.m_sysVersion, ovi.dwMajorVersion << "." << ovi.dwMinorVersion << "." << ovi.dwBuildNumber);
		}

#if defined(KANEVA_ORIG)
		// Get System Name
		wchar_t sysName[MAX_COMPUTERNAME_LENGTH + 1] = L"";
		DWORD sysNameLen = (MAX_COMPUTERNAME_LENGTH + 1);
		ok = (::GetComputerName(sysName, &sysNameLen) != FALSE);
		if (!ok) {
			LogError("::GetComputerName() FAILED");
		} else {
			s_sysInfo.m_sysName = Utf16ToUtf8(sysName);
		}
#endif

		// Get System Memory Info
		GetSysMemoryInfo(s_sysInfo.m_sysMemoryInfo);
	}
	sysInfo = s_sysInfo;
	return ok;
}

bool GetSysMemoryInfo(SysMemoryInfo& sysMemInfo) {
	// Get This Info Once
	static SysMemoryInfo s_sysMemInfo;
	bool ok = true;
	if (!s_sysMemInfo.m_sizeBytes) {
		// Get Physically Installed Memory
		ok = false;

		// Try GetPhysicallyInstalledSystemMemory API (Vista SP1 or later)
		if (KEP::WindowsAPICompat::GetPhysicallyInstalledSystemMemory != nullptr) {
			unsigned long long totalMemoryInKilobytes = 0;
			ok = (*KEP::WindowsAPICompat::GetPhysicallyInstalledSystemMemory)(&totalMemoryInKilobytes) != FALSE;
			if (!ok) {
				LogError("::GetPhysicallyInstalledSystemMemory() FAILED");
			} else {
				s_sysMemInfo.m_sizeBytes = totalMemoryInKilobytes * 1024;
			}
		}

		if (!ok) {
			// Use GlobalMemoryStatusEx (XP compatible)
			MEMORYSTATUSEX ms = { 0 };
			ms.dwLength = sizeof(ms);
			ok = (::GlobalMemoryStatusEx(&ms) != FALSE);
			if (!ok) {
				LogError("::GlobalMemoryStatusEx() FAILED");
				s_sysMemInfo.m_sizeBytes = 0;
			} else {
				s_sysMemInfo.m_sizeBytes = ms.ullTotalPhys;
			}
		}
	}
	sysMemInfo = s_sysMemInfo;
	return ok;
}

// Microsoft Forgot This Declaration
#include "PowrProf.h"
#pragma comment(lib, "PowrProf.lib")
struct PPI {
	ULONG Number;
	ULONG MaxMhz;
	ULONG CurrentMhz;
	ULONG MhzLimit;
	ULONG MaxIdleState;
	ULONG CurrentIdleState;
	PPI() :
			Number(0), MaxMhz(0), CurrentMhz(0), MhzLimit(0), MaxIdleState(0), CurrentIdleState(0) {}
};

bool GetCpuInfo(CpuInfo& cpuInfo) {
	// Get This Info Once
	static CpuInfo s_cpuInfo;
	bool ok = true;
	if (s_cpuInfo.m_nLogicalCores == 0) {
		// Get CPU Information
		SYSTEM_INFO sysInfo;
		::GetSystemInfo(&sysInfo);
		s_cpuInfo.m_nLogicalCores = sysInfo.dwNumberOfProcessors;
		StrBuild(s_cpuInfo.m_cpuVer, sysInfo.dwProcessorType << "." << sysInfo.wProcessorLevel);

		// Get CPU Frequency
		typedef std::vector<PPI> PPIVector;
		PPIVector ppi;
		ppi.resize(2 * sysInfo.dwNumberOfProcessors);
		ok = (::CallNtPowerInformation(ProcessorInformation, NULL, 0, &ppi[0], sizeof(PPI) * ppi.size()) == 0);
		if (!ok) {
			LogError("::CallNtPowerInformation() FAILED");
			s_cpuInfo.m_cpuMhz = 0;
		} else {
			s_cpuInfo.m_cpuMhz = ppi[0].MaxMhz;
		}

#if defined(KANEVA_ORIG)
		cpu_id_t cpuIdData;
		if (cpu_identify(nullptr, &cpuIdData) == 0) {
			s_cpuInfo.m_strCpuVendor = cpuIdData.vendor_str;
			s_cpuInfo.m_strCpuBrand = cpuIdData.brand_str;
			s_cpuInfo.m_strCpuCodeName = cpuIdData.cpu_codename;
			s_cpuInfo.m_iCpuFamily = cpuIdData.ext_family;
			s_cpuInfo.m_iCpuModel = cpuIdData.ext_model;
			s_cpuInfo.m_iCpuStepping = cpuIdData.stepping;
			s_cpuInfo.m_iSSE3 = cpuIdData.flags[CPU_FEATURE_PNI];
			s_cpuInfo.m_iSSSE3 = cpuIdData.flags[CPU_FEATURE_SSSE3];
			s_cpuInfo.m_iSSE4_1 = cpuIdData.flags[CPU_FEATURE_SSE4_1];
			s_cpuInfo.m_iSSE4_2 = cpuIdData.flags[CPU_FEATURE_SSE4_2];
			s_cpuInfo.m_iSSE4_A = cpuIdData.flags[CPU_FEATURE_SSE4A];
			s_cpuInfo.m_iSSE5 = cpuIdData.flags[CPU_FEATURE_SSE5];
			s_cpuInfo.m_iAVX = cpuIdData.flags[CPU_FEATURE_AVX];
			s_cpuInfo.m_iAVX2 = cpuIdData.flags[CPU_FEATURE_AVX2];
			s_cpuInfo.m_iFMA3 = cpuIdData.flags[CPU_FEATURE_FMA3];
			s_cpuInfo.m_iFMA4 = cpuIdData.flags[CPU_FEATURE_FMA4];

			s_cpuInfo.m_iAvx512BW = cpuIdData.flags[CPU_FEATURE_AVX512BW];
			s_cpuInfo.m_iAvx512CD = cpuIdData.flags[CPU_FEATURE_AVX512CD];
			s_cpuInfo.m_iAvx512DQ = cpuIdData.flags[CPU_FEATURE_AVX512DQ];
			s_cpuInfo.m_iAvx512ER = cpuIdData.flags[CPU_FEATURE_AVX512ER];
			s_cpuInfo.m_iAvx512F = cpuIdData.flags[CPU_FEATURE_AVX512F];
			s_cpuInfo.m_iAvx512PF = cpuIdData.flags[CPU_FEATURE_AVX512PF];
			s_cpuInfo.m_iAvx512VL = cpuIdData.flags[CPU_FEATURE_AVX512VL];

			s_cpuInfo.m_nLogicalCores = cpuIdData.num_logical_cpus;
			s_cpuInfo.m_nPhysicalCores = cpuIdData.num_cores;
		} else {
			LogError("Could not get CPU info");
			ok = false;
		}
#endif
	}
	cpuInfo = s_cpuInfo;
	return ok;
}

bool GetGpuInfo(GpuInfo& gpuInfo) {
	// Get This Info Once
	static GpuInfo s_gpuInfo;
	bool ok = true;
	if (s_gpuInfo.m_gpuName.empty()) {
		// Get GPU Information
		DISPLAY_DEVICE displayDevice;
		::ZeroMemory(&displayDevice, sizeof(displayDevice));
		displayDevice.cb = sizeof(displayDevice);
		ok = (::EnumDisplayDevices(NULL, 0, &displayDevice, 0) != FALSE);
		if (!ok) {
			LogError("::EnumDisplayDevices() FAILED");
		} else {
			s_gpuInfo.m_gpuName = KEP::Utf16ToUtf8(displayDevice.DeviceString);
		}

		// Get Latest DirectX Version
		std::string dxVer;
		size_t i = 15;
		for (; i; --i) {
			std::string dxDll;
			StrBuild(dxDll, "d3d" << i << ".dll");
			dxVer = GetModuleVersion(dxDll);
			if (!dxVer.empty())
				break;
		}
		if (!dxVer.empty())
			StrBuild(s_gpuInfo.m_dxVer, i << "." << dxVer);
	}
	gpuInfo = s_gpuInfo;
	return ok;
}

bool GetDiskInfo(DiskInfo& diskInfo) {
	// Get Disk Information
	ULARGE_INTEGER availBytes = { 0 };
	ULARGE_INTEGER sizeBytes = { 0 };
	ULARGE_INTEGER freeBytes = { 0 };
	bool ok = (::GetDiskFreeSpaceEx(NULL, &availBytes, &sizeBytes, &freeBytes) != FALSE);
	if (!ok) {
		LogError("::GetDiskFreeSpaceEx() FAILED");
		diskInfo.m_availBytes = 0;
		diskInfo.m_sizeBytes = 0;
	} else {
		diskInfo.m_availBytes = availBytes.QuadPart;
		diskInfo.m_sizeBytes = sizeBytes.QuadPart;
	}
	return ok;
}

std::string GetModulePath(HMODULE hModule) {
	wchar_t filePath[MAX_PATH] = L"";
	::GetModuleFileNameW(hModule, filePath, MAX_PATH);
	return KEP::Utf16ToUtf8(filePath);
}

std::string GetModuleVersion(const std::string& modulePath) {
	// Default Is Current Process
	std::string filePath = modulePath;
	if (filePath.empty())
		filePath = GetModulePath();
	std::wstring filePathW = KEP::Utf8ToUtf16(filePath);

	DWORD dummy;
	DWORD size = ::GetFileVersionInfoSizeW(filePathW.c_str(), &dummy);
	if (::GetFileVersionInfoSizeW(filePathW.c_str(), &dummy) == 0)
		return std::string();

	char* buffer = new char[size];
	if (buffer == NULL)
		return std::string();

	if (::GetFileVersionInfoW(filePathW.c_str(), 0, size, (void*)buffer) == 0) {
		delete[] buffer;
		return std::string();
	}

	VS_FIXEDFILEINFO* data = NULL;
	unsigned int len;
	bool bResult = ::VerQueryValue(buffer, L"\\", (void**)&data, &len) != 0;
	if (!bResult || data == NULL || len != sizeof(VS_FIXEDFILEINFO)) {
		delete[] buffer;
		return std::string();
	}

	std::stringstream ss;
	ss << HIWORD(data->dwFileVersionMS)
	   << "." << LOWORD(data->dwFileVersionMS)
	   << "." << HIWORD(data->dwFileVersionLS)
	   << "." << LOWORD(data->dwFileVersionLS);
	delete[] buffer;
	return ss.str();
}

// Returns Full Module Path (c:\module\path\filename.exe)
std::string GetThisModulePath() {
	static std::string pathModule;
	if (pathModule.empty()) {
		pathModule = GetModulePath();
	}
	return pathModule;
}

// Returns Module Path Only (c:\module\path)
std::string GetThisModulePathOnly() {
	return StrStripFileNameExt(GetThisModulePath());
}

// Returns Module FileName Only (filename.exe)
std::string GetThisModuleFileName() {
	return StrStripFilePath(GetThisModulePath());
}

std::string GetThisModuleVersion() {
	static std::string versionModule;
	if (versionModule.empty()) {
		versionModule = GetModuleVersion();
	}
	return versionModule;
}

#include "Psapi.h"
#pragma comment(lib, "Psapi.lib")
AppMemoryStats GetAppMemoryStats(bool resetStats) {
	static AppMemoryStats ams; // static singleton

	// Reset Statistics ?
	if (resetStats)
		ams = AppMemoryStats();

	// Get Process Memory Information
	PROCESS_MEMORY_COUNTERS pmc;
	bool ok = (::GetProcessMemoryInfo(::GetCurrentProcess(), &pmc, sizeof(pmc)) != FALSE);
	if (!ok) {
		LogError("::GetProcessMemoryInfo() FAILED");
	} else {
		ams.m_used = pmc.WorkingSetSize;
		UPDATE_NUM(ams.m_num);
		UPDATE_AVG(ams.m_avg, ams.m_num, ams.m_used);
		UPDATE_MIN(ams.m_min, ams.m_used);
		UPDATE_MAX(ams.m_max, pmc.PeakWorkingSetSize);
	}

	// Get Memory Status
	ok = (::GlobalMemoryStatusEx(&ams.m_statex) != FALSE);
	if (!ok) {
		LogError("::GlobalMemoryStatusEx() FAILED");
	}

	return ams;
}

void AppMemoryStats::Log() {
	LogInfo("appMemUsageMB=" << m_used / BYTES_PER_MB << " [" << m_min / BYTES_PER_MB << " " << m_avg / BYTES_PER_MB << " " << m_max / BYTES_PER_MB << "]");
}

ULONGLONG GetAppCpuTime() {
	// Get Process Cpu Time
	FILETIME ft[4] = { 0 };
	bool ok = (::GetProcessTimes(::GetCurrentProcess(), &ft[0], &ft[1], &ft[2], &ft[3]) != FALSE);
	if (!ok) {
		LogError("::GetProcessTimes() FAILED");
		return 0;
	}

	// Return (Kernel + User) Time In Milliseconds (1 interval = 100ns)
	ULONGLONG ftKernel = (((ULONGLONG)ft[2].dwHighDateTime) << 32 | ft[2].dwLowDateTime) / 10000;
	ULONGLONG ftUser = (((ULONGLONG)ft[3].dwHighDateTime) << 32 | ft[3].dwLowDateTime) / 10000;
	return (ftKernel + ftUser);
}

#include <iphlpapi.h>
#pragma comment(lib, "IPHLPAPI.lib")
static std::string MacToStr(BYTE pb[8]) {
	CHAR mac[64] = "";
	sprintf_s(mac, "%02x%02x%02x%02x%02x%02x%02x%02x", pb[0], pb[1], pb[2], pb[3], pb[4], pb[5], pb[6], pb[7]);
	return mac;
}

bool GetNetInfo(NetInfo& netInfo) {
	// Get This Info Once
	static NetInfo s_netInfo;
	if (s_netInfo.IsValid()) {
		netInfo = s_netInfo;
		return true;
	}

	// Reset Network Info
	netInfo.indexBest = 0;
	netInfo.index = 0;
	netInfo.type = 0;
	netInfo.desc = "";
	netInfo.mac = "";

	// Get Best Network Adapter Interface
	IPAddr ipAddr = NET_DOMAIN_IP;
	bool indexOk = (::GetBestInterface(ipAddr, &s_netInfo.indexBest) == NO_ERROR);
	if (!indexOk) {
		LogWarn("::GetBestInterface() FAILED");
	} else {
		LogInfo("indexBest=" << s_netInfo.indexBest);
	}

	// Get All Adapters Info
	IP_ADAPTER_INFO adapterInfo[16];
	DWORD dwBufLen = sizeof(adapterInfo);
	bool ok = (::GetAdaptersInfo(adapterInfo, &dwBufLen) == ERROR_SUCCESS);
	if (!ok) {
		LogError("::GetAdaptersInfo() FAILED");
		return false;
	}

	// For All Network Adapters
	BYTE macHash[8] = { 0 };
	PIP_ADAPTER_INFO pAdapterInfo = adapterInfo;
	for (; pAdapterInfo; pAdapterInfo = pAdapterInfo->Next) {
		// Find MAC As String
		BYTE* mac = pAdapterInfo->Address;
		std::string macStr = MacToStr(mac);

		// Hash All MACs
		for (size_t i = 0; i < 8; ++i)
			macHash[i] += mac[i];

		// Parse Adapter Info
		std::string dhcpServerIp = pAdapterInfo->DhcpServer.IpAddress.String;
		std::string desc = pAdapterInfo->Description;
		size_t type = pAdapterInfo->Type;
		DWORD index = pAdapterInfo->Index;
		bool dhcp = !dhcpServerIp.empty();
		bool lastAdapter = (pAdapterInfo->Next == NULL);

		// DHCP Adapter Is The One We Want Or The Last In Any Case
		bool usingThis = (!s_netInfo.IsValid() && (dhcp || lastAdapter));
		//		bool usingThis = (!s_netInfo.IsValid() && ((indexOk ? (index == indexBest) : dhcp) || lastAdapter));
		if (usingThis) {
			s_netInfo.index = index;
			s_netInfo.type = type;
			s_netInfo.desc = desc;
			s_netInfo.mac = macStr;
		}

		LogInfo(" ... index=" << index
							  << " '" << desc << "'"
							  << " mac=" << macStr
							  << (dhcp ? " DHCP" : "")
							  << (usingThis ? " <==USING_THIS==" : ""));
	}

	// Store MAC Hash As String
	std::string macHashStr = MacToStr(macHash);
	s_netInfo.macHash = macHashStr;
	LogInfo("macHash=" << macHashStr);

	netInfo = s_netInfo;

	return s_netInfo.IsValid();
}

#include <rpc.h>
#pragma comment(lib, "Rpcrt4.lib")
bool UuidCreate(std::string& uuidStr) {
	// Create UUID String (RPC)
	UUID uuid = { 0 };
	RPC_CSTR uuidRpcStr = NULL;
	UuidCreate(&uuid);
	UuidToStringA(&uuid, &uuidRpcStr);
	if (!uuidRpcStr) {
		LogError("::UuidCreate() FAILED");
		return false;
	}

	// Copy UUID String
	uuidStr = (char*)uuidRpcStr;
	RpcStringFreeA(&uuidRpcStr);

	return true;
}

std::string VersionApp(bool log) {
	std::string versionApp = GetThisModuleVersion();
	if (log) {
		LogInfo(versionApp);
	}
	return versionApp;
}

std::string PathCurrent(bool log) {
	std::string pathCurrent = FileHelper::GetCurrentDirectory();
	if (log) {
		LogInfo("'" << pathCurrent << "'");
	}
	return pathCurrent;
}

bool SetPathCurrent(const std::string& path, bool log) {
	bool ok = FileHelper::SetCurrentDirectory(path);
	if (log) {
		LogInfo("'" << path << "' " << (ok ? "OK" : "FAILED"));
	}
	return ok;
}

static std::string s_pathApp; // current application path

bool SetPathApp(const std::string& path, bool log) {
	if (!SetPathCurrent(path, log)) {
		LogError("SetPathCurrent() FAILED - '" << path << "'");
		return false;
	}
	s_pathApp = path;
	if (log) {
		LogInfo("'" << s_pathApp << "'");
	}
	return true;
}

std::string PathApp(bool log) {
	if (s_pathApp.empty()) {
		s_pathApp = PathCurrent(log); // prevent PathFind() recursion
		s_pathApp = StrStripFileNameExt(PathFind(s_pathApp, GetThisModuleFileName(), log));
		SetPathCurrent(s_pathApp, log); // make it the current working directory on launch
	}
	if (log) {
		LogInfo("'" << s_pathApp << "'");
	}
	return s_pathApp;
}

bool PathExists(const std::string& path, bool log) {
	bool pathExists = FileHelper::Exists(path);
	if (log) {
		if (pathExists) {
			LogInfo("'" << LogPath(path) << "' YES");
		} else {
			LogWarn("'" << LogPath(path) << "' NO");
		}
	}
	return pathExists;
}

std::string PathFind(const std::string& pathSearch, const std::string& path, bool log) {
	std::string pathFind;
	if (log) {
		LogInfo("'" << LogPath(pathSearch) << "' -> '" << LogPath(path) << "'?");
	}

	// pathSearch
	if (!pathSearch.empty()) {
		pathFind = PathAdd(pathSearch, path);
		if (PathExists(pathFind, log))
			return pathFind;
	}

	// pathCurrent
	std::string pathCurrent = PathCurrent(log);
	if (!pathCurrent.empty() && !StrSameFilePath(pathCurrent, pathSearch)) {
		pathFind = PathAdd(pathCurrent, path);
		if (PathExists(pathFind, log))
			return pathFind;
	}

	// pathModule
	std::string pathModule = GetThisModulePathOnly();
	if (!pathModule.empty() && !StrSameFilePath(pathModule, pathCurrent)) {
		pathFind = PathAdd(pathModule, path);
		if (PathExists(pathFind, log))
			return pathFind;
	}

	// pathApp
	std::string pathApp = PathApp(log);
	if (!pathApp.empty() && !StrSameFilePath(pathApp, pathModule)) {
		pathFind = PathAdd(pathApp, path);
		if (PathExists(pathFind, log))
			return pathFind;
	}

	if (log) {
		LogError("'" << LogPath(path) << "' NOT FOUND");
	}

	// Return Assumed Search Path
	pathFind = PathAdd(pathSearch, path);
	return pathFind;
}

bool s_msgProcLog = false;
void MsgProcLog(bool enable) {
	s_msgProcLog = enable;
}

MsgProcInfo s_msgProcInfo; // current MsgProcInfo copy

MsgProcInfo::MsgProcInfo() :
		hWnd(0),
		uMsg(0),
		wParam(0),
		lParam(0) {
	cursorPoint.x = cursorPoint.y = 0;

	// Copy From Static
	*this = s_msgProcInfo;
}

MsgProcInfo::MsgProcInfo(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) :
		hWnd(hWnd),
		uMsg(uMsg),
		wParam(wParam),
		lParam(lParam) {
	// Determine Cursor Point
	if (IsMouse()) {
		cursorPoint.x = short(LOWORD(lParam));
		cursorPoint.y = short(HIWORD(lParam));
	} else {
		cursorPoint = s_msgProcInfo.cursorPoint;
	}

	// Copy To Static
	s_msgProcInfo = *this;

	// Log Message ?
	if (s_msgProcLog) {
		LogWarn(ToString(true));
	}
}

bool MsgProcInfo::IsClose() {
	return (uMsg == WM_CLOSE);
}

bool MsgProcInfo::IsActivate() {
	return (uMsg == WM_ACTIVATEAPP);
}

bool MsgProcInfo::IsMouse() {
	return IsMouseButtonUp() || IsMouseButtonDown() || IsMouseButtonDblClk() || IsMouseMove() || IsMouseWheel();
}

bool MsgProcInfo::IsMouseButtonUp() {
	return (uMsg == WM_LBUTTONUP) || (uMsg == WM_RBUTTONUP) || (uMsg == WM_MBUTTONUP) || (uMsg == WM_XBUTTONUP);
}

bool MsgProcInfo::IsMouseButtonDown() {
	return (uMsg == WM_LBUTTONDOWN) || (uMsg == WM_RBUTTONDOWN) || (uMsg == WM_MBUTTONDOWN) || (uMsg == WM_XBUTTONDOWN);
}

bool MsgProcInfo::IsMouseButtonDblClk() {
	return (uMsg == WM_LBUTTONDBLCLK) || (uMsg == WM_RBUTTONDBLCLK) || (uMsg == WM_MBUTTONDBLCLK) || (uMsg == WM_XBUTTONDBLCLK);
}

bool MsgProcInfo::IsMouseMove() {
	return (uMsg == WM_MOUSEMOVE);
}

bool MsgProcInfo::IsMouseWheel() {
	return (uMsg == WM_MOUSEWHEEL);
}

bool MsgProcInfo::IsKey() {
	return IsKeyUp() || IsKeyDown() || IsKeyChar();
}

bool MsgProcInfo::IsKeyUp() {
	return (uMsg == WM_KEYUP) || (uMsg == WM_SYSKEYUP);
}

bool MsgProcInfo::IsKeyDown() {
	return (uMsg == WM_KEYDOWN) || (uMsg == WM_SYSKEYDOWN);
}

bool MsgProcInfo::IsKeyChar() {
	return (uMsg == WM_CHAR);
}

POINT MsgProcInfo::CursorPoint() {
	return cursorPoint;
}

std::string MsgProcInfo::ToString(bool verbose) {
	const char* pMsg = NULL;

	const char* uMsg0055[] = {
		"WM_NULL", // = 0x00
		"WM_CREATE", // = 0x01
		"WM_DESTROY", // 0x02
		"WM_MOVE", // 0x03
		NULL,
		"WM_SIZE", // 0x05
		"WM_ACTIVATE", // 0x06
		"WM_SETFOCUS", // 0x07
		"WM_KILLFOCUS", // 0x08
		NULL,
		"WM_ENABLE", // 0x0A
		"WM_SETREDRAW", // 0x0B
		"WM_SETTEXT", // 0x0C
		"WM_GETTEXT", // 0x0D
		"WM_GETTEXTLENGTH", // 0x0E
		"WM_PAINT", // 0x0F
		"WM_CLOSE", // 0x10
		"WM_QUERYENDSESSION", // 0x11
		"WM_QUIT", // 0x12
		"WM_QUERYOPEN", // 0x13
		"WM_ERASEBKGND", // 0x14
		"WM_SYSCOLORCHANGE", // 0x15
		"WM_ENDSESSION", // 0x16
		"WM_SYSTEMERROR", // 0x17
		"WM_SHOWWINDOW", // 0x18
		"WM_CTLCOLOR", // 0x19
		"WM_SETTINGCHANGE", // 0x1A
		"WM_DEVMODECHANGE", // 0x1B
		"WM_ACTIVATEAPP", // 0x1C
		"WM_FONTCHANGE", // 0x1D
		"WM_TIMECHANGE", // 0x1E
		"WM_CANCELMODE", // 0x1F
		"WM_SETCURSOR", // 0x20
		"WM_MOUSEACTIVATE", // 0x21
		"WM_CHILDACTIVATE", // 0x22
		"WM_QUEUESYNC", // 0x23
		"WM_GETMINMAXINFO", // 0x24
		NULL,
		"WM_PAINTICON", // 0x26
		"WM_ICONERASEBKGND", // 0x27
		"WM_NEXTDLGCTL", // 0x28
		NULL,
		"WM_SPOOLERSTATUS", // 0x2A
		"WM_DRAWITEM", // 0x2B
		"WM_MEASUREITEM", // 0x2C
		"WM_DELETEITEM", // 0x2D
		"WM_VKEYTOITEM", // 0x2E
		"WM_CHARTOITEM", // 0x2F
		"WM_SETFONT", // 0x30
		"WM_GETFONT", // 0x31
		"WM_SETHOTKEY", // 0x32
		"WM_GETHOTKEY", // 0x33
		NULL,
		NULL,
		NULL,
		"WM_QUERYDRAGICON", // 0x37
		NULL,
		"WM_COMPAREITEM", // 0x39
		NULL,
		"WM_COMPACTING", // 0x41
		NULL,
		NULL,
		NULL,
		NULL,
		"WM_WINDOWPOSCHANGING", // 0x46
		"WM_WINDOWPOSCHANGED", // 0x47
		"WM_POWER", // 0x48
		NULL,
		"WM_COPYDATA", // 0x4A
		"WM_CANCELJOURNAL", // 0x4B
		NULL,
		NULL,
		"WM_NOTIFY", // 0x4E
		NULL,
		"WM_INPUTLANGCHANGEREQUEST", // 0x50
		"WM_INPUTLANGCHANGE", // 0x51
		"WM_TCARD", // 0x52
		"WM_HELP", // 0x53
		"WM_USERCHANGED", // 0x54
		"WM_NOTIFYFORMAT", // 0x55
	};
	if (uMsg >= 0x00 && uMsg <= 0x55)
		pMsg = uMsg0055[uMsg - 0x00];

	const char* uMsg7B87[] = {
		"WM_CONTEXTMENU", // 0x7B
		"WM_STYLECHANGING", // 0x7C
		"WM_STYLECHANGED", // 0x7D
		"WM_DISPLAYCHANGE", // 0x7E
		"WM_GETICON", // 0x7F
		"WM_SETICON", // 0x80
		"WM_NCCREATE", // 0x81
		"WM_NCDESTROY", // 0x82
		"WM_NCCALCSIZE", // 0x83
		"WM_NCHITTEST", // 0x84
		"WM_NCPAINT", // 0x85
		"WM_NCACTIVATE", // 0x86
		"WM_GETDLGCODE", // 0x87
	};
	if (uMsg >= 0x7B && uMsg <= 0x87)
		pMsg = uMsg7B87[uMsg - 0x7B];

	const char* uMsgA0AD[] = {
		"WM_NCMOUSEMOVE", // 0xA0
		"WM_NCLBUTTONDOWN", // 0xA1
		"WM_NCLBUTTONUP", // 0xA2
		"WM_NCLBUTTONDBLCLK", // 0xA3
		"WM_NCRBUTTONDOWN", // 0xA4
		"WM_NCRBUTTONUP", // 0xA5
		"WM_NCRBUTTONDBLCLK", // 0xA6
		"WM_NCMBUTTONDOWN", // 0xA7
		"WM_NCMBUTTONUP", // 0xA8
		"WM_NCMBUTTONDBLCLK", // 0xA9
		NULL,
		"WM_NCXBUTTONDOWN", // 0xAB
		"WM_NCXBUTTONUP", // 0xAC
		"WM_NCXBUTTONDBLCLK", // 0xAD
	};
	if (uMsg >= 0xA0 && uMsg <= 0xAD)
		pMsg = uMsgA0AD[uMsg - 0xA0];

	const char* uMsg100138[] = {
		"WM_KEYDOWN", // 0x100
		"WM_KEYUP", // 0x101
		"WM_CHAR", // 0x102
		"WM_DEADCHAR", // 0x103
		"WM_SYSKEYDOWN", // 0x104
		"WM_SYSKEYUP", // 0x105
		"WM_SYSCHAR", // 0x106
		"WM_SYSDEADCHAR", // 0x107
		"WM_KEYLAST", // 0x108
		NULL,
		NULL,
		NULL,
		NULL,
		"WM_IME_STARTCOMPOSITION", // 0x10D
		"WM_IME_ENDCOMPOSITION", // 0x10E
		"WM_IME_COMPOSITION", // 0x10F
		"WM_IME_KEYLAST", // 0x10F
		"WM_INITDIALOG", // 0x110
		"WM_COMMAND", // 0x111
		"WM_SYSCOMMAND", // 0x112
		"WM_TIMER", // 0x113
		"WM_HSCROLL", // 0x114
		"WM_VSCROLL", // 0x115
		"WM_INITMENU", // 0x116
		"WM_INITMENUPOPUP", // 0x117
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		"WM_MENUSELECT", // 0x11F
		"WM_MENUCHAR", // 0x120
		"WM_ENTERIDLE", // 0x121
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		"WM_CTLCOLORMSGBOX", // 0x132
		"WM_CTLCOLOREDIT", // 0x133
		"WM_CTLCOLORLISTBOX", // 0x134
		"WM_CTLCOLORBTN", // 0x135
		"WM_CTLCOLORDLG", // 0x136
		"WM_CTLCOLORSCROLLBAR", // 0x137
		"WM_CTLCOLORSTATIC", // 0x138
	};
	if (uMsg >= 0x100 && uMsg <= 0x138)
		pMsg = uMsg100138[uMsg - 0x100];

	const char* uMsg200234[] = {
		"WM_MOUSEMOVE", // 0x200
		"WM_LBUTTONDOWN", // 0x201
		"WM_LBUTTONUP", // 0x202
		"WM_LBUTTONDBLCLK", // 0x203
		"WM_RBUTTONDOWN", // 0x204
		"WM_RBUTTONUP", // 0x205
		"WM_RBUTTONDBLCLK", // 0x206
		"WM_MBUTTONDOWN", // 0x207
		"WM_MBUTTONUP", // 0x208
		"WM_MBUTTONDBLCLK", // 0x209
		"WM_MOUSEWHEEL", // 0x20A
		NULL,
		NULL,
		NULL,
		"WM_MOUSEHWHEEL", // 0x20E
		NULL,
		"WM_PARENTNOTIFY", // 0x210
		"WM_ENTERMENULOOP", // 0x211
		"WM_EXITMENULOOP", // 0x212
		"WM_NEXTMENU", // 0x213
		"WM_SIZING", // 0x214
		"WM_CAPTURECHANGED", // 0x215
		"WM_MOVING", // 0x216
		NULL,
		"WM_POWERBROADCAST", // 0x218
		"WM_DEVICECHANGE", // 0x219
		"WM_MDICREATE", // 0x220
		"WM_MDIDESTROY", // 0x221
		"WM_MDIACTIVATE", // 0x222
		"WM_MDIRESTORE", // 0x223
		"WM_MDINEXT", // 0x224
		"WM_MDIMAXIMIZE", // 0x225
		"WM_MDITILE", // 0x226
		"WM_MDICASCADE", // 0x227
		"WM_MDIICONARRANGE", // 0x228
		"WM_MDIGETACTIVE", // 0x229
		"WM_MDISETMENU", // 0x230
		"WM_ENTERSIZEMOVE", // 0x231
		"WM_EXITSIZEMOVE", // 0x232
		"WM_DROPFILES", // 0x233
		"WM_MDIREFRESHMENU", // 0x234
	};
	if (uMsg >= 0x200 && uMsg <= 0x234)
		pMsg = uMsg200234[uMsg - 0x200];

	const char* uMsg281286[] = {
		"WM_IME_SETCONTEXT", // 0x281
		"WM_IME_NOTIFY", // 0x282
		"WM_IME_CONTROL", // 0x283
		"WM_IME_COMPOSITIONFULL", // 0x284
		"WM_IME_SELECT", // 0x285
		"WM_IME_CHAR", // 0x286
	};
	if (uMsg >= 0x281 && uMsg <= 0x286)
		pMsg = uMsg281286[uMsg - 0x281];

	const char* uMsg290291[] = {
		"WM_IME_KEYDOWN", // 0x290
		"WM_IME_KEYUP", // 0x291
	};
	if (uMsg >= 0x290 && uMsg <= 0x291)
		pMsg = uMsg290291[uMsg - 0x290];

	const char* uMsg2A12A3[] = {
		"WM_MOUSEHOVER", // 0x2A1
		"WM_NCMOUSELEAVE", // 0x2A2
		"WM_MOUSELEAVE", // 0x2A3
	};
	if (uMsg >= 0x2A1 && uMsg <= 0x2A3)
		pMsg = uMsg2A12A3[uMsg - 0x2A1];

	const char* uMsg300312[] = {
		"WM_CUT", // 0x300
		"WM_COPY", // 0x301
		"WM_PASTE", // 0x302
		"WM_CLEAR", // 0x303
		"WM_UNDO", // 0x304
		"WM_RENDERFORMAT", // 0x305
		"WM_RENDERALLFORMATS", // 0x306
		"WM_DESTROYCLIPBOARD", // 0x307
		"WM_DRAWCLIPBOARD", // 0x308
		"WM_PAINTCLIPBOARD", // 0x309
		"WM_VSCROLLCLIPBOARD", // 0x30A
		"WM_SIZECLIPBOARD", // 0x30B
		"WM_ASKCBFORMATNAME", // 0x30C
		"WM_CHANGECBCHAIN", // 0x30D
		"WM_HSCROLLCLIPBOARD", // 0x30E
		"WM_QUERYNEWPALETTE", // 0x30F
		"WM_PALETTEISCHANGING", // 0x310
		"WM_PALETTECHANGED", // 0x311
		"WM_HOTKEY", // 0x312
	};
	if (uMsg >= 0x300 && uMsg <= 0x312)
		pMsg = uMsg300312[uMsg - 0x300];

	const char* uMsg317318[] = {
		"WM_PRINT", // 0x317
		"WM_PRINTCLIENT", // 0x318
	};
	if (uMsg >= 0x317 && uMsg <= 0x318)
		pMsg = uMsg317318[uMsg - 0x317];

	if (uMsg == 0x358)
		pMsg = (const char*)"WM_HANDHELDFIRST"; // 0x358

	if (uMsg == 0x35F)
		pMsg = (const char*)"WM_HANDHELDLAST"; // 0x35F

	if (uMsg == 0x380)
		pMsg = (const char*)"WM_PENWINFIRST"; // 0x380

	if (uMsg == 0x38F)
		pMsg = (const char*)"WM_PENWINLAST"; // 0x38F

	if (uMsg == 0x390)
		pMsg = (const char*)"WM_COALESCE_FIRST"; // 0x390

	if (uMsg == 0x39F)
		pMsg = (const char*)"WM_COALESCE_LAST"; // 0x39F

	const char* uMsg3E03E8[] = {
		"WM_DDE_INITIATE", // 0x3E0
		"WM_DDE_TERMINATE", // 0x3E1
		"WM_DDE_ADVISE", // 0x3E2
		"WM_DDE_UNADVISE", // 0x3E3
		"WM_DDE_ACK", // 0x3E4
		"WM_DDE_DATA", // 0x3E5
		"WM_DDE_REQUEST", // 0x3E6
		"WM_DDE_POKE", // 0x3E7
		"WM_DDE_EXECUTE", // 0x3E8
	};
	if (uMsg >= 0x3E0 && uMsg <= 0x3E8)
		pMsg = uMsg3E03E8[uMsg - 0x3E0];

	if (uMsg == 0x400)
		pMsg = (const char*)"WM_USER"; // 0x400

	if (uMsg == 0x8000)
		pMsg = (const char*)"WM_APP"; // 0x8000

	// uMsg Not Found?
	if (!pMsg) {
		static char uMsgNo[64];
		sprintf_s(uMsgNo, "<0x%X>", uMsg);
		pMsg = uMsgNo;
	}

	// Return String
	std::string msgStr;
	StrBuild(msgStr, pMsg);
	if (verbose) {
		StrAppend(msgStr, " - "
							  << " msg=[0x" << std::hex << uMsg
							  << " 0x" << std::hex << wParam
							  << " 0x" << std::hex << lParam << "]"
							  << " cursor=(" << std::dec << cursorPoint.x << "," << cursorPoint.y << ")");
	}
	return msgStr;
}

bool MsgProcPumpUntilEmpty(size_t maxMessages, UINT msgUntil, bool useMFC) {
	auto pApp = useMFC ? AfxGetApp() : nullptr;

	// Process Messages Until Empty or Max Messages
	for (size_t i = 0; (maxMessages == 0) || (i < maxMessages); ++i) {
		MSG msg = { 0 };

		// Call PeekMessage first if we want to exit when message queue is empty
		// No message queue check if this is a infinite loop until certain message is received
		if (maxMessages != 0 || msgUntil == 0) {
			// Message Waiting ?
			BOOL bRet = ::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE);
			if (bRet == -1) {
				LogError("::PeekMessage() FAILED");
				return false;
			}

			if (bRet == 0) {
				// No messages in the queue
				if (msgUntil != 0)
					continue; // continue to loop until we receive msgUntil
				else
					break; // exit loop
			}
		}

		// Read and process message
		if (pApp != nullptr) {
			// MFC message loop
			pApp->PumpMessage();
		} else {
			// Standard Win32 message loop
			if (!::GetMessage(&msg, NULL, 0, 0)) {
				// WM_QUIT received
				break;
			}
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}

		// Received msgUntil?
		if (msgUntil != 0 && msg.message == msgUntil)
			break;
	}

	return true;
}

static int cursorEnableCounter = 0;
bool GetCursorEnabled() {
	return (cursorEnableCounter >= 0);
}
void SetCursorEnabled(bool enable) {
	while (GetCursorEnabled() != enable) {
		cursorEnableCounter = ::ShowCursor(enable ? TRUE : FALSE);
		LogWarn("enable=" << LogBool(enable));
	}
}

LPDIRECT3DDEVICE9 GlobalD3DDevice(LPDIRECT3DDEVICE9 pd3dDevice) {
	static LPDIRECT3DDEVICE9 s_pd3dDevice = nullptr;
	if (pd3dDevice)
		s_pd3dDevice = pd3dDevice;
	return s_pd3dDevice;
}

HANDLE CreateThread(LPTHREAD_START_ROUTINE pFunc, LPVOID param, int priority) {
	// Valid Thread Function ?
	if (!pFunc)
		return NULL;

	// Create Thread
	HANDLE threadHandle;
	DWORD threadId;
	threadHandle = ::CreateThread(NULL, 0, pFunc, param, 0, &threadId);
	if (threadHandle) {
		if (!::SetThreadPriority(threadHandle, priority))
			LogError("::SetThreadPriority() FAILED - [" << threadId << "]");
	} else {
		LogError("::CreateThread() FAILED");
	}

	return threadHandle;
}

void EnableMFCDebugMemoryDump(bool bEnable) {
#if defined(_DEBUG)
	AfxEnableMemoryLeakDump(bEnable ? TRUE : FALSE);
#endif
}

static int s_devMode;
void Helpers_SetDevMode(int devMode) {
	s_devMode = devMode;
}
int Helpers_GetDevMode() {
	return s_devMode;
}