///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

//
// Performance note [YC 09/2013]
//
// Based on recent load test experiments, I found that Windows mutex performance seems to be
// linear to the total number of mutex (locked or not) in the operation system.
//
// I observed following stats on my box (i7 2720qm 2.2GHz, 8GB RAM, Windows 7 64-bit)
//
// Each mutex operation = CreateMutex + WaitForSingleObject (not locked by other) + ReleaseMutex + CloseHandle
//
// (*) When the total number of mutexes in the system is ~10k			(== 50 concurrent operations/app * 200 apps/server)
// ==> Average time per mutex operation ~= 0.01 millisecond
//
// (*) When the total number of mutexes in the system is ~20k			(== 100 concurrent operations/app * 200 apps/server)
// ==> Average time per mutex operation ~= 0.02 millisecond
//
// (*) When the total number of mutexes in the system is ~50k			(== 200 concurrent operations/app * 200 apps/server)
// ==> Average time per mutex operation ~= 0.05 millisecond
//
// (*) When the total number of mutexes in the system is ~100k			(== 500 concurrent operations/app * 200 apps/server)
// ==> Average time per mutex operation ~= 0.1 millisecond
//
// jsFastSync on the other hand is quite constant on performance. Each jsFastSync construction/locking/unlocking
//  cycle takes less than 1 microsecond (0.001 millisecond) on average with same testing environment.
//

namespace KEP {

class NamedMutexHelper {
public:
	NamedMutexHelper(const std::string& name) :
			m_locked(false), m_disposeHandle(true) {
		m_hMutex = CreateMutexW(NULL, FALSE, Utf8ToUtf16(name).c_str());
	}

	NamedMutexHelper(HANDLE hMutex) :
			m_locked(false), m_disposeHandle(false) {
		m_hMutex = hMutex;
	}

	~NamedMutexHelper() {
		if (m_hMutex) {
			unlock();
			if (m_disposeHandle) {
				CloseHandle(m_hMutex);
			}
		}
	}

	DWORD lock(DWORD waitTimeMs = INFINITE) {
		DWORD res = WaitForSingleObject(m_hMutex, waitTimeMs);
		m_locked = res == WAIT_OBJECT_0;
		return res;
	}

	void unlock() {
		if (m_locked) {
			ReleaseMutex(m_hMutex);
			m_locked = false;
		}
	}

	bool isValid() const { return m_hMutex != NULL; }

private:
	HANDLE m_hMutex;
	bool m_locked;
	bool m_disposeHandle;
};

} // namespace KEP