///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "KEPException.h"
#include "ExtendedConfig.h"
#include "Application.h"
#include "commandline.h"

using namespace std;

namespace KEP {

static void AppendSlash(string& path);

const char* const TAG_ROOT = "config";
const char* const TAG_STD_SERVER_DIR = "server_directory";
const char* const TAG_STD_SOURCE_DIR = "source_directory";
const char* const TAG_STD_PATCH_DIR = "patch_directory";
const char* const TAG_APP_SERVER_DIR = "app_server_directory";
const char* const TAG_APP_SOURCE_DIR = "app_source_directory";
const char* const TAG_APP_PATCH_DIR = "app_patch_directory";
const char* const TAG_APP_PATCH_URL = "app_patch_url";
const char* const TAG_ADD_SERVER_URL = "add_server_url";

__declspec(dllexport)
	BOOL WriteDistributionInfo(const string& distroInfoPath, const string& serverDir, const string& sourceDir, const string& patchDir,
		const string& appServerDir, const string& appSourceDir, const string& appPatchDir, const string& appPatchUrl) {
	KEPConfig config;
	config.New(TAG_ROOT);
	config.SetValue(TAG_STD_SERVER_DIR, serverDir.c_str());
	config.SetValue(TAG_STD_SOURCE_DIR, sourceDir.c_str());
	config.SetValue(TAG_STD_PATCH_DIR, patchDir.c_str());
	config.SetValue(TAG_APP_SERVER_DIR, appServerDir.c_str());
	config.SetValue(TAG_APP_SOURCE_DIR, appSourceDir.c_str());
	config.SetValue(TAG_APP_PATCH_DIR, appPatchDir.c_str());
	config.SetValue(TAG_APP_PATCH_URL, appPatchUrl.c_str());
	config.SetValue(TAG_ADD_SERVER_URL, ""); // Set per hosting environment
	return config.SaveAs(distroInfoPath.c_str()) ? TRUE : FALSE;
}

__declspec(dllexport)
	BOOL ReadDistributionInfo(const string& distroInfoPath, string& serverDir, string& sourceDir, string& patchDir,
		string& appServerDir, string& appSourceDir, string& appPatchDir, string& appPatchUrl, string& addServerUrl) {
	// Use command line parameter -D to specify logical overrides. E.g. Incubator can define \\inc-fs\Server\ServerFiles-%(GAME_ID)
	// in distributioninfo.xml for "source directory", which will be overridden by KGPServer to \\inc-fs\Server\ServerFiles-12345 if
	// -D GAME_ID=12345 is provided in KGPServer command line.

	OldCommandLine& cmdLine = Application::singleton().commandLine();
	OldCommandLine::Parameter* p = cmdLine.getParameter("-D");

	KEPConfigAdapter config;
	try {
		config.Load(distroInfoPath);
	} catch (KEPException&) {
		//Catch ConfigurationException thrown by ConfigFileLoader::load
		return FALSE;
	}

	config.impl().paramSet().set(StringHelpers::parseKeyValuePairsWithOption(p->valueString()));
	serverDir = config.Get(TAG_STD_SERVER_DIR, "");
	sourceDir = config.Get(TAG_STD_SOURCE_DIR, "");
	patchDir = config.Get(TAG_STD_PATCH_DIR, "");
	appServerDir = config.Get(TAG_APP_SERVER_DIR, "");
	appSourceDir = config.Get(TAG_APP_SOURCE_DIR, "");
	appPatchDir = config.Get(TAG_APP_PATCH_DIR, "");
	appPatchUrl = config.Get(TAG_APP_PATCH_URL, "");
	addServerUrl = config.Get(TAG_ADD_SERVER_URL, "");

	// Make sure all paths end with backslash if not empty
	AppendSlash(serverDir);
	AppendSlash(sourceDir);
	AppendSlash(patchDir);
	AppendSlash(appServerDir);
	AppendSlash(appSourceDir);
	AppendSlash(appPatchDir);
	return TRUE;
}

static void AppendSlash(string& path) {
	if (!path.empty()) {
		if (path[path.length() - 1] != '\\') {
			path.append("\\");
		}
	}
}

} // namespace KEP