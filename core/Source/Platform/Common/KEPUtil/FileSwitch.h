///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/FileName.h"
#include "Core/Util/HighPrecisionTime.h"
#include "jsEnumFiles.h"

namespace KEP {

/**
 * Class that provides an easy way to drop in a run time switch.  This 
 * switch can be throttled such that it can be placed in high frequency 
 * logic without impacting performance.
 *
 * Typically run time switches have been implemented using configuration
 * files.  This has two draw backs:
 * 
 * <ul>
 *   <li>Access to high level configuration data from low level generic
 *   logic can be problematic</li>
 *   <li>Until we add dynamic support to our confguration classes, i.e.
 *    until configuration files can be reloaded with mechanisms to update
 *    the volatile configuration data, effecting a change in the volatile
 *    configuration requires an application restart.</li>
 * </ul>
 * This initial implementation supports a binary on/off switch with a
 * configurable timeout that is used to throttle testing of the underlying
 * file.  This is crucial as over frequent calls to check for the existence
 * of a file, let alone actually opening the file and reading it's contents
 * has relatively high overhead.
 */
class FileSwitch {
public:
	FileSwitch(const std::string switchName, unsigned long throttleMS = 10000) :
			_switchName(FileName::parse(switchName)),
			_throttle(throttleMS, Timer::State::Running),
			_state(false),
			_initialized(false) {}

	FileSwitch(const FileName& switchName, unsigned long throttleMS = 10000) :
			_switchName(switchName),
			_throttle(throttleMS, Timer::State::Running),
			_state(false),
			_initialized(false) {}

	bool isOn() {
		liveTest();
		return _state;
	}

	bool isOff() {
		liveTest();
		return !_state;
	}

private:
	void liveTest() {
		if (!_initialized || _throttle.elapsed()) {
			bool isdir = 0;
			_state = jsFileExists(_switchName.toString().c_str(), &isdir);
			_initialized = true;
		}
	}
	FileName _switchName;
	TimeDelay _throttle;
	bool _state;
	bool _initialized;
};

} // namespace KEP