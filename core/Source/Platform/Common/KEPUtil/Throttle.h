///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

/**
 * Interface that defines the basic API of a throttling mechanism.  The throttle is
 * queried for being in a throttled state,  indicating that some logic should or s
 * should not be executed.  e.g.:
 *
 *			...
 *			Throttle myThrottle;
 *			if ( !myThrottle.throttled ) myFunc();
 *			...
 */
class Throttle {
public:
	/**
	 * Determine if throttle is in a throttled state.
	 * @returns If the throttle is in a throttled state returns true, else returns false (clear)
	 */
	virtual bool isThrottled() = 0;

	/**
	 * Determine if the throttle is clear.
	 */
	bool isCleared() { return !isThrottled(); }
};

/**
 * Concrete implementation of the Throttle interface.  Throttles based on a counter that
 * is incremented when the throttle is queried.  E.g.:
 *
 *		CountThrottle throttle(5)     // Clear on every fifth query
 *      while( true )
 *           if (!throttle.isThrottled()) cout << "The throttle is clear!" << endl;
 */
class CountThrottle : public Throttle {
public:
	/**
	 * Construct with a frequency on which the throttle should be
	 * in a cleared state.
	 */
	CountThrottle(unsigned long frequency) :
			m_frequency(frequency), m_counter(0) {}

	/**
	 * @return true if caller should throttle, else faluse
	 */
	virtual bool isThrottled() {
		m_counter++;
		if (m_frequency == 0)
			return false; // protect against divide by zero.
		return (m_counter % m_frequency) != 0;
	}

	/**
	 * Set the frequency with this throttle will be in a cleared state.
	 */
	CountThrottle& setFrequency(unsigned long frequency) {
		m_frequency = frequency;
		return *this;
	}

	/**
	 * Acquire the current throttling frequency of this 
	 * throttle.
	 */
	unsigned long frequency() { return m_frequency; }

private:
	unsigned long m_frequency;
	unsigned long m_counter;
};
