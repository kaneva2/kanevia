///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <windows.h>
#include <winsock.h>
#include "Core/Util/Unicode.h"
#include "SocketSubsystem.h"
#include "WSAErrors.h"

namespace KEP {

const ErrorSpec getError(unsigned long id) {
	return ErrorSpec(id, WSAErrStr(id));
}

const ErrorSpec getError() {
	DWORD dErr = WSAGetLastError();
	if (dErr == 0)
		return ErrorSpec(0, "");
	return ErrorSpec(MOD_WSAERRORS | dErr, WSAErrStr(dErr));
}
/*-----------------------------------------------------------
 * Function: WSAErrStr()
 *
 * Description: Given a WinSock error value, return error string
 *  NOTE: This function requires an active window to work.
 */
std::string WSAErrStr(int WSAErr, HINSTANCE hInst) {
	int err_len = 0;
	HWND hwnd;

	wchar_t acBuf[512];
	acBuf[0] = 0;

	if (!hInst) {
		hwnd = GetActiveWindow();
#ifndef WIN32 // I guess this is to stay compatible with 16 bit windows.
		hInst = GetWindowWord(hwnd, GWW_HINSTANCE);
#else
		hInst = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
#ifdef _DEBUG
		hInst = GetModuleHandleW(L"keputild.dll");
#else
		hInst = GetModuleHandleW(L"keputil.dll");
#endif
#endif
	}

	if (WSAErr == 0) /* If error passed is 0, use the */
		WSAErr = WSABASEERR; /*  base resource file number */

	if (WSAErr >= WSABASEERR) /* Valid Error code? */ {
		/* get error string from the table in the Resource file */
		unsigned int idd = (WSAErr | MOD_WSAERRORS);
		err_len = LoadStringW(hInst, idd, acBuf, ERR_SIZE / 2);
	}
	return Utf16ToUtf8(acBuf);
} /* end GetWSAErrStr() */ // end extract class

SocketSubSystem::SocketSubSystem() {
	WORD wVer = MAKEWORD(2, 2);
	WSAData wsaData;
	if (WSAStartup(wVer, &wsaData) != NO_ERROR) {
		throw NetworkException(SOURCELOCATION, getError());
	}

	if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2) {
		WSACleanup();
		throw NetworkException(SOURCELOCATION, getError(IDS_SOCKSYS_INCOMPATIBLE_WINSOCK_VERSION));
	}
}

SocketSubSystem::~SocketSubSystem() {
	WSACleanup();
}

std::string SocketSubSystem::_hostName;

std::string SocketSubSystem::hostName() {
	if (_hostName.length() > 0)
		return _hostName;

	char achost[512];
	if (gethostname(achost, 512) != 0)
		throw ChainedException(SOURCELOCATION);

	_hostName = achost;
	return _hostName;
}

} // namespace KEP