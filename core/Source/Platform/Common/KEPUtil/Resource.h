///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#define IDS_MAILERR_NOSENDER            1
#define IDS_MAILERR_NORECIPIENT         2
#define IDS_STRING3                     3
#define IDS_MAILERR_INCOMPATIBLE_WINSOCK_VERSION 3

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NEXT_RESOURCE_VALUE	4000
#define _APS_NEXT_CONTROL_VALUE		4000
#define _APS_NEXT_SYMED_VALUE		4000
#define _APS_NEXT_COMMAND_VALUE		32771
#endif
#endif
