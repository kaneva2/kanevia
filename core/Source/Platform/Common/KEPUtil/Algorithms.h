///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <atlenc.h>
#include <iomanip>
#include <string>
#include <sstream>
#include <vector>
#include <iterator>
#include <random>
#include <algorithm>

#include "Core/Util/ByteBufRuler.h"
#include "Core/Util/Unicode.h"
#include "Core/Util/ChainedException.h"
#include "Core/Util/OS.h"
#include "Core/Util/StringHelpers.h"
#include "Core/Util/FileName.h"
#include <Log4CPlus/configurator.h>

#include "Common\include\KEPHelpers.h"

/**
 * Class used to aggregate various non-class specific algorithms.
 */
namespace KEP {

class OS;

class Algorithms {
public:
	/**
	 * Does absolutely nothing.  Handing for sidestepping strict warning levels during development.
	 */
	static void Noop() {}

	/** 
	 * Given a module handle determine the module version as identified by
	 * GetFileVersionInfo()
	 * @returns String representation of version
	 */
	static std::string GetModuleVersion(HMODULE hModule = 0) {
		wchar_t filename[_MAX_PATH];
		GetModuleFileNameW(hModule, filename, _MAX_PATH);
		return Algorithms::GetModuleVersion(Utf16ToUtf8(filename));
	}

	/**
	 * Give a module file name, determine the module version as identified by
	 * GetFileVersionInfo()
	 * @returns String reprentation of version
	 */
	static std::string GetModuleVersion(std::string filename) {
		std::wstring filenameW = Utf8ToUtf16(filename);
		DWORD dummy;
		DWORD size = GetFileVersionInfoSizeW(filenameW.c_str(), &dummy);
		if (GetFileVersionInfoSizeW(filenameW.c_str(), &dummy) == 0)
			return std::string();

		char* buffer = new char[size];
		if (buffer == NULL)
			return std::string();

		if (GetFileVersionInfo(filenameW.c_str(), 0, size, (void*)buffer) == 0) {
			delete[] buffer;
			return std::string();
		}

		VS_FIXEDFILEINFO* data = NULL;
		unsigned int len;
		bool bResult = VerQueryValueW(buffer, L"\\", (void**)&data, &len) != 0;
		if (!bResult || data == NULL || len != sizeof(VS_FIXEDFILEINFO)) {
			delete[] buffer;
			return std::string();
		}

		std::stringstream ss;
		ss << HIWORD(data->dwFileVersionMS)
		   << "." << LOWORD(data->dwFileVersionMS)
		   << "." << HIWORD(data->dwFileVersionLS)
		   << "." << LOWORD(data->dwFileVersionLS);
		delete[] buffer;
		return ss.str();
	}

	/**
	 * Given a wide string containing xml data, escape an xml control 
	 * characters (e.g. & v &amp;)
	 */
	static std::wstring xmlEscape(const std::wstring& content, unsigned long bufSize = 512) {
		wchar_t* p = new wchar_t[bufSize];
		memset(p, 0, bufSize * sizeof(wchar_t));
		/*unsigned long l =*/EscapeXML(content.c_str(), content.length(), p, bufSize, ATL_ESC_FLAG_ATTR);
		std::wstring ret(p);
		delete p;
		return ret;
	}

	/**
	 * Given a narrow string containing xml data, escape an xml control 
	 * characters (e.g. & v &amp;)
	 */
	static std::string xmlEscape(const std::string& content) {
		return Utf16ToUtf8(xmlEscape(Utf8ToUtf16(content)));
	}

	static wchar_t getCh(VOID) {
		HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
		INPUT_RECORD irInputRecord;
		DWORD dwEventsRead;
		wchar_t cChar;

		while (ReadConsoleInputW(hStdin, &irInputRecord, 1, &dwEventsRead)) /* Read key press */
			if (irInputRecord.EventType == KEY_EVENT && irInputRecord.Event.KeyEvent.wVirtualKeyCode != VK_SHIFT && irInputRecord.Event.KeyEvent.wVirtualKeyCode != VK_MENU && irInputRecord.Event.KeyEvent.wVirtualKeyCode != VK_CONTROL) {
				cChar = irInputRecord.Event.KeyEvent.uChar.UnicodeChar;
				ReadConsoleInputW(hStdin, &irInputRecord, 1, &dwEventsRead); /* Read key release */
				return cChar;
			}
		return EOF;
	}

	static std::string getLocalHostName() {
		wchar_t buffer[1024];
		DWORD nameLen = _countof(buffer);

		if (GetComputerNameExW(ComputerNameDnsHostname, buffer, &nameLen)) {
			return Utf16ToUtf8(buffer);
		} else {
			return "localhost";
		}
	}
};

//// Traits about Numbers which are used insure that parsing when Numbers to string
//// we get the expected representation.
////
//enum NumberFormat {
//	Signed				// i.e. range short is -32768 to 32767
//	, Unsigned			// i.e. range short is 0 to 65535
//	, FloatingPoint
//};

//// Internally we use sprintf to format the numeric.  Thus far in most cases
//// %d will get the disired effect.  However for unsigned long/int (32bit) we
//// need to adjust the format string to %u.
////
//// Partial.  What is used in cases where we haven't declared specific implementations
////
//template<typename T > struct NumberTraits		{ static const NumberFormat format = Signed;		};
//
//// Traits specific to unsigned int
//template<> struct NumberTraits<unsigned int>	{ static const NumberFormat format = Unsigned;		};
//
//// Traits specific to unsigned long.
//template<> struct NumberTraits<unsigned long>	{ static const NumberFormat format = Unsigned;		};
//
//// Traits specific to double
//template<> struct NumberTraits<double>			{ static const NumberFormat format = FloatingPoint;	};

class LoggingSubsystem {
private:
	log4cplus::ConfigureAndWatchThread* _configureThread;
	std::string _logCfg;
	std::string _logDir;

public:
	LoggingSubsystem(const std::string& logCfg, const std::string& logDir = "") :
			_configureThread(0), _logCfg(logCfg), _logDir(logDir) {
	}

	~LoggingSubsystem() {
		if (_configureThread > 0) {
			delete _configureThread;
			_configureThread = 0;
		}
	}

	void start(const std::string& preamble = "") {
		if (_configureThread != 0)
			return;
		if (_logDir.length() > 0)
			_mkdir(_logDir.c_str());

		if (!FileHelper::Exists(_logCfg)) {
			fprintf(stderr, "ERROR log4cplus config file doesn't exist! \"%s\"\n", _logCfg.c_str());
			fprintf(stderr, "No logging will occur\n");
			fflush(stderr);
		} else
			fprintf(stderr, "Opening log4cplus config file \"%s\"\n", _logCfg.c_str());

		_configureThread = new log4cplus::ConfigureAndWatchThread(Utf8ToUtf16(_logCfg).c_str(), 30 * 1000);
		::Sleep(10); // Give the logging thread a chance to startup.

		if (preamble.length() > 0) {
			LOG4CPLUS_INFO(log4cplus::Logger::getInstance(L"STARTUP"), preamble);
			LOG4CPLUS_INFO(log4cplus::Logger::getInstance(L"STARTUP"), preamble);
			LOG4CPLUS_INFO(log4cplus::Logger::getInstance(L"STARTUP"), preamble);
		}
	}
};

// This implementation is NOT reentrant
//
class WeightedDistribution {
public:
	WeightedDistribution() :
			_sum(0) { srand((unsigned int)time(0)); }

	WeightedDistribution& addWeight(int key, int weight) {
		_distribution.push_back(SegMappingT(key, RangeT(_sum, _sum + weight - 1)));
		_sum += weight;
		return *this;
	}

	int generate() {
		// Using the weights we generate a vector of ranges where the
		// magnitude of the range reflects the weight
		//
		int index = (rand() % _sum);
		for (SegMappingVecT::iterator iter = _distribution.begin(); iter != _distribution.end(); iter++) {
			if (index < iter->second.first)
				continue;
			if (index > iter->second.second)
				continue;
			return iter->first;
		}
		return 0; // should never happen.
	}

private:
	typedef std::pair<int, int> WeightT;
	typedef std::vector<WeightT> WeightTableT;
	// A pair containing the lo and hi bounds of the range
	typedef std::pair<int, int> RangeT;

	// A pair that maps an aribitrary value to a range
	//
	typedef std::pair<int, RangeT> SegMappingT;

	// A vector of range mappings
	//
	typedef std::vector<SegMappingT> SegMappingVecT;

	std::vector<WeightT> _weights;
	SegMappingVecT _distribution;
	int _sum;
};

} // namespace KEP