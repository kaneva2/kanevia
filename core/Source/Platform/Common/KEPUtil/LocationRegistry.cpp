///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LocationRegistry.h"

namespace KEP {

LocationRegistry* LocationRegistry::_singleton = 0;
std::mutex LocationRegistry::_initSync;

LocationRegistry& LocationRegistry::singleton() {
	if (_singleton != 0)
		return *_singleton;
	std::lock_guard<std::mutex> lock(_initSync);
	if (_singleton != 0)
		return *_singleton;
	_singleton = new LocationRegistry();
	return *_singleton;
}

} // namespace KEP