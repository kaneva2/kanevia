///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "SDKCommon.h"
#include "log4cplus/logger.h"
#include "NTService.h"
#include "Core/Util/Singleton.h"
#include <string>
#include "Common/KEPUtil/ExtendedConfig.h"

class TiXmlDocument;

namespace KEP {

class PluginHostApplication : public CNTService {
public:
	PluginHostApplication(const char* name, const char* description = "", const char* defAppCfgFile = NULL, const char* defLogCfgFile = NULL);
	virtual ~PluginHostApplication();

	int Main(int argc, char** argv);

protected:
	virtual BOOL OnInit(DWORD dwArgc, char** lpszArgv);
	virtual void OnStop();
	virtual void OnShutdown();
	virtual void Run();

	void InitLogger(const std::string& cfgPath);

	static BOOL WINAPI CtrlHandler(DWORD fdwCtrlType);

	static FileName getProcessFileName();

private:
	std::string m_defaultAppConfigFile;
	std::string m_defaultLogConfigFile;
	bool m_interactive;
	TIXMLConfig m_configXml;
	bool m_loggerInited;

	static PluginHostApplication* ms_instance;
};

} // namespace KEP

KEPUTIL_EXPORT int RunPluginHostApplication(int argc, char** argv, const char* name, const char* description = "", const char* defAppCfgFile = NULL, const char* defLogCfgFile = NULL);
