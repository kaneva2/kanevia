///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "httpserver.h"

#include <log4cplus/logger.h>
using namespace log4cplus;

namespace KEP {

// Handlers should always be stateless, so we can keep a single
// default handler instance right here
//
HTTPServer::DefaultRequestHandler HTTPServer::defaultRequestHandler;
HTTPServer::NoFileSupportHandler HTTPServer::noFileSupportHandler;

HTTPServer::HTTPServer() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"HTTPServer"), "HTTPServer::HTTPServer()");
}

HTTPServer::~HTTPServer() {}

} // namespace KEP