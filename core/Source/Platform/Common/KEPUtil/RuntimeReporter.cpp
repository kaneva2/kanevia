///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RuntimeReporter.h"
#include "core\crypto\Crypto_MD5.h"
#include "WebCall.h"

using namespace RuntimeReporter;
using namespace KEP;

#define RR_WEB_CALLER "RuntimeReporter"

static LogInstance("Exception");

/// Registry Support
static bool regInstalled = false; // registry support is installed

// Registry Current User Software Keys (HKEY_CURRENT_USER/Software/Kaneva/<appName>/<regKey>)
static HKEY hCuSwKey; // 'Software' registry key
static HKEY hCuCompanyKey; // 'Kaneva' registry key
static HKEY hCuAppKey; // <appName> registry key
static HKEY hCuKey; // <regKey> registry key

// Registry Local Machine Software Keys (HKEY_LOCAL_MACHINE/Software/Kaneva/<appName>/<regKey>)
static HKEY hLmSwKey; // 'Hardware' registry key
static HKEY hLmCompanyKey; // 'Kaneva' registry key
static HKEY hLmAppKey; // <appName> registry key
static HKEY hLmKey; // <regKey> registry key

// Report Type Running Parameter Selections
static const PARAM_SELECT paramSelectTypeRunning = (PARAM_SELECT_APP | PARAM_SELECT_RUNTIME | PARAM_SELECT_RUNTIME_ID | PARAM_SELECT_SYSTEM_ID
#if defined(KANEVA_ORIG)
													| PARAM_SELECT_CPU_INFO | PARAM_SELECT_SYS_INFO | PARAM_SELECT_DISK_INFO | PARAM_SELECT_NET_INFO
#endif
);

// Report Type Stopped Parameter Selections
static const PARAM_SELECT paramSelectTypeStopped = (PARAM_SELECT_APP | PARAM_SELECT_RUNTIME | PARAM_SELECT_RUNTIME_ID | PARAM_SELECT_APP_INFO);

// Report Type Crashed Parameter Selections (same as stopped)
static const PARAM_SELECT paramSelectTypeCrashed = paramSelectTypeStopped;

// Report Type Custom Parameter Selections
static const PARAM_SELECT paramSelectTypeCustom = (PARAM_SELECT_APP | PARAM_SELECT_RUNTIME_ID);

// Report Type Parameter Selections
static const PARAM_SELECT paramSelectTypes[REPORT_TYPES] = {
	paramSelectTypeRunning, // REPORT_TYPE_RUNNING
	paramSelectTypeStopped, // REPORT_TYPE_STOPPED
	paramSelectTypeCrashed, // REPORT_TYPE_CRASHED
	paramSelectTypeCustom // REPORT_TYPE_CUSTOM
};

bool _RegInstall(const std::string& appName, const std::string& regKey) {
	// Already Installed ?
	if (regInstalled)
		return true;

	std::wstring wappName = Utf8ToUtf16(appName);
	std::wstring wregKey = Utf8ToUtf16(regKey);

	// Open Current User Registry Keys [HKEY_CURRENT_USER/Software/Kanevia/<appName>/<regKey>]
	DWORD dw;
	bool okCu = (::RegOpenKeyExW(HKEY_CURRENT_USER, L"software", 0, KEY_WRITE | KEY_READ, &hCuSwKey) == ERROR_SUCCESS);
	okCu &= (::RegCreateKeyExW(hCuSwKey, K_NAME_REGISTRY, 0, REG_NONE, REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL, &hCuCompanyKey, &dw) == ERROR_SUCCESS);
	okCu &= (::RegCreateKeyExW(hCuCompanyKey, wappName.c_str(), 0, REG_NONE, REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL, &hCuAppKey, &dw) == ERROR_SUCCESS);
	okCu &= (::RegCreateKeyExW(hCuAppKey, wregKey.c_str(), 0, REG_NONE, REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL, &hCuKey, &dw) == ERROR_SUCCESS);
	if (!okCu) {
		LogError("RegOpenKeyEx(CURRENT_USER) FAILED");
	}

	// Open Local Machine Registry Keys [HKEY_LOCAL_MACHINE/Software/Kanevia/<appName>/<regKey>]
	bool okLm = (::RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"software", 0, KEY_READ, &hLmSwKey) == ERROR_SUCCESS);
	okLm &= (::RegCreateKeyExW(hLmSwKey, K_NAME_REGISTRY, 0, REG_NONE, REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL, &hLmCompanyKey, &dw) == ERROR_SUCCESS);
	okLm &= (::RegCreateKeyExW(hLmCompanyKey, wappName.c_str(), 0, REG_NONE, REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL, &hLmAppKey, &dw) == ERROR_SUCCESS);
	okLm &= (::RegCreateKeyExW(hLmAppKey, wregKey.c_str(), 0, REG_NONE, REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL, &hLmKey, &dw) == ERROR_SUCCESS);
	if (!okLm) {
		LogError("RegOpenKeyEx(LOCAL_MACHINE) FAILED");
	}

	regInstalled = true; //(okCu && okLm);

	return regInstalled;
}

bool _RegRxStr(HKEY hKey, const std::wstring& name, std::string& val) {
	wchar_t v[257] = L"";
	DWORD vSize = sizeof(v) - sizeof(wchar_t);
	bool ok = (::RegQueryValueEx(hKey, name.c_str(), 0, NULL, reinterpret_cast<BYTE*>(v), &vSize) == ERROR_SUCCESS);
	if (ok)
		val = Utf16ToUtf8(v);
	return ok;
}

bool _RegTxStr(HKEY hKey, const std::wstring& name, const std::string& valUtf8) {
	std::wstring valW = Utf8ToUtf16(valUtf8);
	bool ok = (::RegSetValueEx(hKey, name.c_str(), 0, REG_SZ, (BYTE*)valW.c_str(), sizeof(wchar_t) * (valW.size() + 1)) == ERROR_SUCCESS);
	return ok;
}

bool _RegRxInt(HKEY hKey, const std::wstring& name, int& val) {
	DWORD v = 0;
	DWORD vSize = sizeof(v);
	bool ok = (::RegQueryValueEx(hKey, name.c_str(), 0, NULL, (BYTE*)&v, &vSize) == ERROR_SUCCESS);
	if (ok)
		val = v;
	return ok;
}

bool _RegTxInt(HKEY hKey, const std::wstring& name, const int& val) {
	bool ok = (::RegSetValueEx(hKey, name.c_str(), 0, REG_DWORD, (const BYTE*)&val, 4) == ERROR_SUCCESS);
	return ok;
}

// Registry Current User Rx/Tx Helpers
bool _RegCuRxStr(const std::wstring& name, std::string& val) {
	return _RegRxStr(hCuKey, name, val);
}
bool _RegCuTxStr(const std::wstring& name, const std::string& val) {
	return _RegTxStr(hCuKey, name, val);
}
bool _RegCuRxInt(const std::wstring& name, int& val) {
	return _RegRxInt(hCuKey, name, val);
}
bool _RegCuTxInt(const std::wstring& name, const int& val) {
	return _RegTxInt(hCuKey, name, val);
}
#define RegTxVersion(val) _RegCuTxStr(L"version", val)
#define RegRxVersion(val) _RegCuRxStr(L"version", val)
#define RegTxState(val) _RegCuTxStr(L"state", val)
#define RegRxState(val) _RegCuRxStr(L"state", val)
#define RegTxSecRuntime(val) _RegCuTxInt(L"secRuntime", (int)val)
#define RegRxSecRuntime(val) _RegCuRxInt(L"secRuntime", (int&)val)
#define RegTxNumRuns(val) _RegCuTxInt(L"numRuns", (int)val)
#define RegRxNumRuns(val) _RegCuRxInt(L"numRuns", (int&)val)
#define RegTxNumCrashes(val) _RegCuTxInt(L"numCrashes", (int)val)
#define RegRxNumCrashes(val) _RegCuRxInt(L"numCrashes", (int&)val)

// Registry Local Machine Rx/Tx Helpers
bool _RegLmRxStr(const std::wstring& name, std::string& val) {
	return _RegRxStr(hLmKey, name, val);
}
bool _RegLmTxStr(const std::wstring& name, const std::string& val) {
	return _RegTxStr(hLmKey, name, val);
}
bool _RegLmRxInt(const std::wstring& name, int& val) {
	return _RegRxInt(hLmKey, name, val);
}
bool _RegLmTxInt(const std::wstring& name, const int& val) {
	return _RegTxInt(hLmKey, name, val);
}
#define RegTxSystemId(val) _RegLmTxStr(L"systemId", val);
#define RegRxSystemId(val) _RegLmRxStr(L"systemId", val)

static std::atomic<bool> rrInstalled = false; ///< runtime reporting is installed flag
static std::string rrUrl = "runtimeReport.aspx?";
static std::string rrAppName = "appName";
static APP_CALLBACK rrAppCallback = NULL; ///< application installed callback
static Timer rrRuntime; ///< session runtime timer (ms since install)
static std::string rrRuntimeId;

static std::string strHash(const std::string& hash, const std::string& str) {
}

static std::string getSystemId() {
	static std::string s_systemId;
	if (s_systemId.empty()) {
		std::string str;
		CpuInfo cpuInfo;
		if (GetCpuInfo(cpuInfo)) {
			str += cpuInfo.m_cpuVer;
		}
		SysInfo sysInfo;
		if (GetSysInfo(sysInfo)) {
			str += sysInfo.m_sysVersion;
		}
		NetInfo netInfo;
		if (GetNetInfo(netInfo)) {
			str += netInfo.macHash;
		}
		MD5_DIGEST_STR hash;
		hashString(str.c_str(), hash);
		s_systemId = hash.s;
	}
	return s_systemId;
}

class RRMutexProtectedVariables {
public:
	std::string GetSystemId() {
		if (m_strSystemId.empty())
			m_strSystemId = getSystemId();
		return m_strSystemId;
	}

	void SetSystemIdReg(const std::string& str) {
		std::lock_guard<fast_mutex> lock(m_mutex);
		m_strSystemIdReg = str;
	}

	std::string GetSystemIdReg(bool bWait = false) {
		if (bWait)
			WaitForResponseComplete();
		std::lock_guard<fast_mutex> lock(m_mutex);
		return m_strSystemIdReg;
	}

	bool ResponseIsPending() {
		std::lock_guard<fast_mutex> lock(m_mutex);
		return m_pWcaRequest != nullptr;
	}

	void ResponsePending(std::shared_ptr<WebCallAct> pWCA) {
		std::lock_guard<fast_mutex> lock(m_mutex);
		m_pWcaRequest = pWCA;
		m_responseOk = false;
	}

	void ResponseComplete(WebCallAct* pWCA, bool ok) {
		std::lock_guard<fast_mutex> lock(m_mutex);
		if (pWCA == m_pWcaRequest.get()) {
			m_responseOk = ok;
			m_pWcaRequest.reset();
		}
	}

	void WaitForResponseComplete(TimeMs timeoutSec = 10.0) {
		Timer timer;
		while (ResponseIsPending() && (timer.ElapsedSec() < timeoutSec))
			fTime::SleepMs(1);
	}

private:
	fast_mutex m_mutex;
	std::string m_strSystemId;
	std::string m_strSystemIdReg;
	std::shared_ptr<WebCallAct> m_pWcaRequest; // If non-null, must wait for this before getting systemId is needed.
	bool m_responseOk;
};

static RRMutexProtectedVariables s_RRMutexProtectedVariables;

bool RuntimeReporter::Install(const std::string& appName, const std::string& regKey, const std::string& url, RuntimeReporter::APP_CALLBACK pFunc, const std::string& strRuntimeId) {
	// Already Installed ?
	if (rrInstalled)
		return true;

	// Install Registry Support
	rrInstalled = _RegInstall(appName, regKey);
	if (!rrInstalled) {
		LogError("_RegInstall() FAILED - appName=" << appName << " regKey=" << regKey);
		return false;
	}

	// Copy App Name
	rrAppName = appName;

	// Copy Url
	rrUrl = url;

	// Attach Application Callback
	rrAppCallback = pFunc;

	// Update Profile
	ProfileUpdate();

	// ED-8454 - RuntimeReporter should optionally create it's own runtimeId on Install
	if (!strRuntimeId.empty())
		rrRuntimeId = strRuntimeId;

	// Get Registry SystemId
	std::string strSystemIdReg;
	RegRxSystemId(strSystemIdReg);
	s_RRMutexProtectedVariables.SetSystemIdReg(strSystemIdReg);

	// Prime Application Memory Statistics
	auto ams = GetAppMemoryStats();
	ams.Log();

	return true;
}

bool RuntimeReporter::RuntimeReset() {
	// Reset Session Runtime Timer (not synced with profile)
	rrRuntime.Reset();
	return true;
}

TimeSec RuntimeReporter::RuntimeSec() {
	// Return Runtime In Seconds (profile + session)
	int regSecRuntime = 0;
	RegRxSecRuntime(regSecRuntime);
	TimeSec secRuntime = rrRuntime.ElapsedSec();
	secRuntime += regSecRuntime;
	return secRuntime;
}

bool RuntimeReporter::ProfileReset() {
	// Not Installed?
	if (!rrInstalled)
		return false;

	// Reset Profile Accumulators
	bool ok = RegTxVersion(VersionApp());
	ok &= RegTxSecRuntime(0);
	ok &= RegTxNumRuns(0);
	ok &= RegTxNumCrashes(0);
	return ok;
}

bool RuntimeReporter::ProfileUpdate(Profile* pProfile) {
	// Not Installed?
	if (!rrInstalled)
		return false;

	// Use Default Profile ?
	Profile profileDefault;
	if (!pProfile)
		pProfile = &profileDefault;

	// Update Profile Version (reset profile on version mismatch)
	std::string regVersion;
	RegRxVersion(regVersion);
	pProfile->version = VersionApp();
	bool versionOk = (pProfile->version == regVersion) ? true : ProfileReset();

	// Update Profile Runs
	size_t regNumRuns = 0;
	RegRxNumRuns(regNumRuns);
	pProfile->numRuns += regNumRuns;
	bool runsOk = RegTxNumRuns(pProfile->numRuns);

	// Update Profile Crashes
	size_t regNumCrashes = 0;
	RegRxNumCrashes(regNumCrashes);
	pProfile->numCrashes += regNumCrashes;
	bool crashesOk = RegTxNumCrashes(pProfile->numCrashes);

	// Update Profile Runtime In Seconds
	pProfile->secRuntime = RuntimeSec();
	bool runtimeOk = RegTxSecRuntime((int)pProfile->secRuntime);

	// Reset Session Runtime If Ok
	if (runtimeOk)
		RuntimeReset();

	return (versionOk && runsOk && crashesOk && runtimeOk);
}

// ED-8454 - RuntimeReporter should optionally create it's own runtimeId on Install
std::string RuntimeReporter::RuntimeId() {
	if (rrRuntimeId.empty())
		UuidCreate(rrRuntimeId);
	return rrRuntimeId;
}

std::string RuntimeReporter::SystemIdReg() {
	return s_RRMutexProtectedVariables.GetSystemIdReg(true); // wait for web response systemId while locked
}

std::string RuntimeReporter::ParamSelect(PARAM_SELECT paramSelect) {
	bool bJSONString = false;
	RuntimeReporterWebParams webParams;
	RuntimeReporterJSONParams jsonParams;
	RuntimeReporterParams& params = bJSONString ? static_cast<RuntimeReporterParams&>(jsonParams) : webParams;

	// Update Profile
	Profile profile;
	ProfileUpdate(&profile);

	// Append Application Parameters ?
	if (PARAM_SELECTED(PARAM_SELECT_APP)) {
		params.Add("appName", rrAppName);
		params.Add("appVer", profile.version);
	}

	// Append Runtime Identifier ?
	if (PARAM_SELECTED(PARAM_SELECT_RUNTIME_ID)) {
		params.Add("runtimeId", RuntimeId());
	}

	// Append System Identifier ?
	if (PARAM_SELECTED(PARAM_SELECT_SYSTEM_ID)) {
#if defined(KANEVA_ORIG)
		params.Add("systemId", SystemIdReg());
#else
		auto systemId = s_RRMutexProtectedVariables.GetSystemId();
		params.Add("systemId", systemId);

		auto systemIdReg = s_RRMutexProtectedVariables.GetSystemIdReg();
		if (systemId != systemIdReg)
			params.Add("systemIdReg", systemIdReg);
#endif
	}

	// Append Runtime Parameters ?
	if (PARAM_SELECTED(PARAM_SELECT_RUNTIME)) {
		params.Add("runtime", profile.secRuntime);
		params.Add("runs", profile.numRuns);
		params.Add("crashes", profile.numCrashes);
	}

	// Append CPU Info ?
	if (PARAM_SELECTED(PARAM_SELECT_CPU_INFO)) {
		CpuInfo cpuInfo;
		if (GetCpuInfo(cpuInfo)) {
			params.Add("cpus", cpuInfo.m_nLogicalCores);
			params.Add("cpuPhysCore", cpuInfo.m_nPhysicalCores);
			params.Add("cpuVer", cpuInfo.m_cpuVer);
			params.Add("cpuMhz", cpuInfo.m_cpuMhz);
#if defined(KANEVA_ORIG)
			params.Add("cpuVendor", cpuInfo.m_strCpuVendor);
			params.Add("cpuBrand", cpuInfo.m_strCpuBrand);
			params.Add("cpuCodeName", cpuInfo.m_strCpuCodeName);
			params.Add("cpuFamily", cpuInfo.m_iCpuFamily);
			params.Add("cpuModel", cpuInfo.m_iCpuModel);
			params.Add("cpuStepping", cpuInfo.m_iCpuStepping);
			params.Add("cpuSSE3", cpuInfo.m_iSSE3);
			params.Add("cpuSSSE3", cpuInfo.m_iSSSE3);
			params.Add("cpuSSE4_1", cpuInfo.m_iSSE4_1);
			params.Add("cpuSSE4_2", cpuInfo.m_iSSE4_2);
			params.Add("cpuSSE4_A", cpuInfo.m_iSSE4_A);
			params.Add("cpuSSE5", cpuInfo.m_iSSE5);
			params.Add("cpuAVX", cpuInfo.m_iAVX);
			params.Add("cpuAVX2", cpuInfo.m_iAVX2);
			params.Add("cpuFMA3", cpuInfo.m_iFMA3);
			params.Add("cpuFMA4", cpuInfo.m_iFMA4);
			/* Very few CPUs support the following features at this time
			params.Add("cpuAVX512BW", cpuInfo.m_iAvx512BW);
			params.Add("cpuAVX512CD", cpuInfo.m_iAvx512CD);
			params.Add("cpuAVX512DQ", cpuInfo.m_iAvx512DQ);
			params.Add("cpuAVX512ER", cpuInfo.m_iAvx512ER);
			params.Add("cpuAVX512F" , cpuInfo.m_iAvx512F);
			params.Add("cpuAVX512PF", cpuInfo.m_iAvx512PF);
			params.Add("cpuAVX512VL", cpuInfo.m_iAvx512VL);
			*/
#endif
		}
	}

	// Append System Info ?
	if (PARAM_SELECTED(PARAM_SELECT_SYS_INFO)) {
		SysInfo sysInfo;
		if (GetSysInfo(sysInfo)) {
			params.Add("sysVer", sysInfo.m_sysVersion);
#if defined(KANEVA_ORIG)
			params.Add("sysName", sysInfo.m_sysName);
#endif
			params.Add("sysMem", sysInfo.m_sysMemoryInfo.m_sizeBytes);
		}
	}

	// Append Disk Info ?
	if (PARAM_SELECTED(PARAM_SELECT_DISK_INFO)) {
		DiskInfo diskInfo;
		if (GetDiskInfo(diskInfo)) {
			params.Add("diskSize", diskInfo.m_sizeBytes);
			params.Add("diskAvail", diskInfo.m_availBytes);
		}
	}

	// Append Network Info ?
	if (PARAM_SELECTED(PARAM_SELECT_NET_INFO)) {
		NetInfo netInfo;
		if (GetNetInfo(netInfo)) {
			params.Add("net", netInfo.desc);
			params.Add("netMAC", netInfo.mac);
			params.Add("netType", netInfo.type);
			params.Add("netHash", netInfo.macHash);
		}
	}

	// Append Application Info ?
	if (PARAM_SELECTED(PARAM_SELECT_APP_INFO)) {
		// Append Application CPU Time
		auto cpuTime = GetAppCpuTime();
		params.Add("appCpuTime", cpuTime);

		// Append Application Memory Statistics
		auto ams = GetAppMemoryStats();
		params.Add("appMemNum", ams.m_num);
		params.Add("appMemAvg", ams.m_avg);
		params.Add("appMemMin", ams.m_min);
		params.Add("appMemMax", ams.m_max);
	}

	return std::move(params.GetString());
}

bool RuntimeReporter::ProfileStateRunning(bool sendReport) {
	// Not Installed?
	if (!rrInstalled)
		return false;

	// Set Profile State 'Running'
	bool ok = RegTxState("Running");
	if (!ok)
		LogError("RegTxState(Running) FAILED");

	// Update Profile (numRuns + 1)
	Profile profile;
	profile.numRuns = 1;
	ProfileUpdate(&profile);

	LogInfo("===== RUNNING (numRuns=" << profile.numRuns << ") =====");

	// Send 'Running' Runtime Report ?
	if (sendReport)
		SendReport(REPORT_TYPE_RUNNING);

	return ok;
}

bool RuntimeReporter::ProfileStateStopped(bool sendReport) {
	// Not Installed?
	if (!rrInstalled)
		return false;

	// Profile State Already 'Stopped' ?
	std::string regState;
	RegRxState(regState);
	if (regState == "Stopped")
		return true;

	// Set Profile State 'Stopped'
	bool ok = RegTxState("Stopped");
	if (!ok)
		LogError("RegTxState(Stopped) FAILED");

	// Update Profile
	Profile profile;
	ProfileUpdate(&profile);

	LogInfo("===== STOPPED (secRuntime=" << FMT_TIME << profile.secRuntime << ") =====");

	// Send 'Stopped' Runtime Report ?
	if (sendReport)
		SendReport(REPORT_TYPE_STOPPED);

	return ok;
}

bool RuntimeReporter::ProfileStateCrashed(bool sendReport) {
	// Not Installed?
	if (!rrInstalled)
		return false;

	// Set Profile State 'Crashed'
	bool ok = RegTxState("Crashed");
	if (!ok)
		LogError("RegTxState(Crashed) FAILED");

	// Update Profile (numCrashes + 1)
	Profile profile;
	profile.numCrashes = 1;
	ProfileUpdate(&profile);

#if (DRF_LOGS_ON_CRASH == 1)
	LogFatal("===== CRASHED (secRuntime=" << FMT_TIME << profile.secRuntime << " numCrashes=" << profile.numCrashes << ") =====");
#endif

	// Send 'Crashed' Runtime Report ?
	if (sendReport)
		SendReport(REPORT_TYPE_CRASHED);

	return ok;
}

bool RuntimeReporter::SendReport(REPORT_TYPE type, PARAM_SELECT paramSelect) {
	// Not Installed?
	if (!rrInstalled)
		return false;

	// App Callback (update CallbackStruct to set parameters for webcall)
	CallbackStruct cbs(type);
	if (rrAppCallback)
		rrAppCallback(cbs);

	// Send Report ?
	if (!cbs.willSend)
		return false;

	// Get Selected Parameters
	std::string selParams = ParamSelect(paramSelect);

	// Build Url '<rrUrl>type=<type><selParams><appParams>'
	std::string url;
	StrBuild(url, rrUrl << "type=" << cbs.type << selParams << cbs.params);

#if (DRF_RUNTIME_REPORTER_SEND_REPORT == 0)
	LogWarn("DISABLED - " << url);
	return true;
#else		

	// Set Web Call Options (secure + response + hide url for jeff)
	WebCallOpt wco;
	wco.url = url;
	wco.secure = true;
	wco.response = true;
#ifndef _DEBUG
	if (!cbs.willLog)
		wco.verbose &= ~WC_VERBOSE_URL; // jeff's request
#endif

	// Assign Response Handler ?
	wco.cbFunc = [paramSelect, cbs](WebCallAct* wca) {
		bool ok = wca->IsOk();

		// Parse Running Report Response Only
		if (cbs.type == REPORT_TYPE_RUNNING) {
			auto response = wca->ResponseStr();
			if (ok && !response.empty()) {
				// Log Response ?
				if (cbs.willLog)
					LogInfo("response=...\n"
							<< response);

#if defined(KANEVA_ORIG)
				// Parse <SystemId> Return String
				if (STLContainsIgnoreCase(response, "SystemId")) {
					std::string systemId = XmlGetFirstChildTextAsStr(response, "SystemId");
					if (!systemId.empty()) {
						std::string strCurrentSystemId = s_RRMutexProtectedVariables.GetSystemIdReg(false);
						if (!strCurrentSystemId.empty() && (systemId != strCurrentSystemId))
							LogWarn("Mismatched SystemId - (" << systemId << " != " << strCurrentSystemId << ")"); // happens when switching stars
						s_RRMutexProtectedVariables.SetSystemIdReg(systemId);

						// Set Registry SystemId
						RegTxSystemId(systemId);
					}
				}
#else
				// Set Registry SystemId
				std::string systemId = s_RRMutexProtectedVariables.GetSystemId();
				std::string systemIdReg = s_RRMutexProtectedVariables.GetSystemIdReg();
				if (systemId != systemIdReg)
					RegTxSystemId(systemId);
#endif
			}
		}

		// Always Reset Profile After Sending Runtime Successfully
		bool runtimeSentOk = (ok && PARAM_SELECTED(PARAM_SELECT_RUNTIME));
		if (runtimeSentOk)
			ProfileReset();

		// Request Complete
		s_RRMutexProtectedVariables.ResponseComplete(wca, ok);
	};

	// Perform Async Webcall
	std::shared_ptr<WebCallAct> wca = WebCaller::WebCall(RR_WEB_CALLER, wco);

	// Race condition. If web call completes before we call SetRuntimeIdRequest, ReceivedRuntimeIdRequest() won't be
	// called. The result of this will be unnecessary calls to Wait() when retrieving the runtimeId or systemId.
	s_RRMutexProtectedVariables.ResponsePending(wca);

	return wca != nullptr;
#endif // DRF_RUNTIME_REPORTER_SEND_REPORT
}

bool RuntimeReporter::SendReport(REPORT_TYPE type) {
	// Get Default Report Type Parameter Selection
	if (type >= REPORT_TYPES)
		return false;
	PARAM_SELECT paramSelect = paramSelectTypes[type];

	// Send Report
	return SendReport(type, paramSelect);
}

void RuntimeReporter::WaitForReportResponseComplete() {
	s_RRMutexProtectedVariables.WaitForResponseComplete();
}