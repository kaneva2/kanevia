///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Helpers.h"

/// Crash Report Error Data API
class DLLExport ErrorData {
	typedef std::map<std::string, std::string> MAP_KVP;

public:
	std::string reason; ///< error data reason string

	/// Error Data Types
	enum Type {
		TYPE_NONE = 0, ///< no error type
		TYPE_FORCED = 1, ///< app forced a report, not necessarily a crash
		TYPE_CRASH = 2, ///< app crashed, send a crash report
		TYPES
	} type; ///< error data enumerated type

	MAP_KVP kvp; ///< error data key/value pair map

	ErrorData(const char* r = NULL, Type t = TYPE_NONE);

	void Reset();
	bool SetValue(const std::string& key, const std::string& value);
	bool SetValue(const std::string& key, const long& value);
	std::string GetValue(const std::string& key);
};

/// Crash Type Enumeration
enum {
	CR_CRASH_TYPE_REPORT_ONLY = 0 // report only, don't terminate process
};

/// Crash Reporter Minidump Enumeration
enum CR_MINIDUMP {
	CR_MINIDUMP_RAND, // randomize minidump level (LOW, MED, HIGH)
	CR_MINIDUMP_LOW, // MiniDumpNormal | MiniDumpWithIndirectlyReferencedMemory | MiniDumpWithHandleData
	CR_MINIDUMP_MED, // CR_MINIDUMP_LOW | MiniDumpWithDataSegs
	CR_MINIDUMP_HIGH, // CR_MINIDUMP_MED | MiniDumpWithPrivateReadWriteMemory
	CR_MINIDUMPS
};

/// Crash Reporter Screenshot Enumeration
enum CR_SCREENSHOT {
	CR_SCREENSHOT_NONE, // no screen shot
	CR_SCREENSHOT_PROCESS, // screen shot of process windows only
	CR_SCREENSHOT_DESKTOP, // screen shot of entire desktop
	CR_SCREENSHOTS
};

/// Crash Reporter API
namespace CrashReporter {

// Crash Reporter Callback Structure
struct CallbackStruct {
	bool willSend; // report will be sent (false to cancel)
	bool isNotCrash; // report is not real crash

	CallbackStruct(bool isNotCrash) :
			willSend(true),
			isNotCrash(isNotCrash) {}
};

typedef void (*APP_CALLBACK)(CallbackStruct& cbs);

// Crash Reporter Version
DLLExport int Version();

// Crash Reporter Process Install/UnInstall Functions
DLLExport bool Install(
	const std::string& appNameStr,
	const std::string& appVersionStr = "",
	APP_CALLBACK pFunc = NULL,
	const CR_MINIDUMP& crMinidump = CR_MINIDUMP_LOW,
	bool showGUI = false,
	bool dontSend = false);
DLLExport bool UnInstall();

// Crash Reporter Enable Functions
DLLExport void Enable(bool enable);
DLLExport bool IsEnabled();

// Crash Reporter Attachment Functions
DLLExport bool AddFile(const std::string& filePath, const std::string& fileDesc = "File");
DLLExport bool AddLogFile(const std::string& filePathLog, const std::string& fileDesc = "Log");
DLLExport bool AddLogFiles(const std::string& filePathLogCfg, const std::string& fileDesc = "Log");
DLLExport bool AddScreenshot(const CR_SCREENSHOT& screenshot);
DLLExport bool AddProperty(const std::string& propName, const std::string& propValue);
DLLExport bool AddProperty(const std::string& propName, const long& propValue);
DLLExport bool AddErrorData(const ErrorData& errorData);
DLLExport bool AddRegistryKey(const std::string& keyName);

// Crash Reporter Error Data Functions
DLLExport void LogErrorData();
DLLExport ErrorData* GetErrorData();
DLLExport std::string GetErrorData(const std::string& key);
DLLExport bool SetErrorData(const std::string& key, const std::string& value);
DLLExport bool SetErrorData(const std::string& key, const long& value);

// Crash Reporter Forced Report Functions
DLLExport bool ReportError(const ErrorData& errorData, bool isNotCrash = true);
DLLExport bool ForcedCrash(unsigned int crashType, const std::string& reason = "") throw(...);
}; // namespace CrashReporter
