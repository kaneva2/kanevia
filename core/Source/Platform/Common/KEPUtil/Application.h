///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/Singleton.h"
#include "common/keputil/CommandLine.h"
#include "common/include/KEPConfig.h"

namespace KEP {

/**
 * Class, typically used by as a singleton, serves as single access point
 * for standard application wide resources, like the application config
 * and command line.
 */
class __declspec(dllexport) Application {
public:
	Application() :
			_sessionUUID(OS::makeGUID()) {}

	virtual ~Application() {}

	// Singleton support
	static Application& singleton();

	OldCommandLine& commandLine() { return _commandLine; }
	const KEPConfig& configuration() { return _config; }
	void loadConfiguration(const char* path, const char* rootTag) { _config.Load(path, rootTag); }

	/**
	 * Acquire the value of the PrivateBytes performance counter.  This counter represents
	 * the memory used by the application. 
	 */
	size_t privateBytes();

	/**
	 * Acquire the version of this application.
	 */
	std::string version();

	/**
     * Every Applicaition instance generates and stores a uuid() at start up.
     * This method gets that value.
     */
	std::string sessionUUId();

private:
	// Singleton support
	static Singleton<Application> _singleton;

	OldCommandLine _commandLine;

	std::string _sessionUUID;

	// Configuration may or may not be needed or desired, furthermore
	// if support for configuration files is needed, we need to be
	// prepared for the possibility that it will need to be reloaded
	//
	// declare as a
	KEPConfig _config;
};

} // namespace KEP