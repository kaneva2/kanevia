///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <log4cplus/logger.h>

#include "Core/Util/HighPrecisionTime.h"
#include "common/include/kepexception.h"
#include "Core/Util/fast_mutex.h"

/**
 * This file describes the frame work of the leasing subsystem.  Many components will be described and
 * explained using a real world implementation:  A vacation property rental.  Here are the involved 
 * parties and components (with their corresponding nomenclature in parens):
 *
 * 1) A Ski Chalet      (Resource)
 * 2) Ski Chalet Owner  (Resource Provider)
 * 3) Property Manager  (Lease Manager)
 * 4) The Tourist       (Lessor/Holder)
 * 5) Lease(Lease)      An artifact that asserts the right of the Lessor to use the resource.
 * 5a) Lease Terms      Specifies various, arbitrary properties of the lease.  E.g.  Can the lease be renewed? Can the lease
 *                      be vacated?  How long can the resource be used?  How much of the resource can be used etc..
 * 
 * Resource.  The reason a leasing system exists is to insure that resources are 1) released appropriately
 * and 2) users of resources can be arbitrarily evicted from from a resource.  Leasing::Resource serves
 * a base class for resources to be managed via leases.
 */

namespace Leasing {
// The building blocks

// naming interface

/**
 * Primarily a naming interface, Leasing::Resource is the base class for any and all resources to be managed 
 * via this leasing framework.
 */
class Resource {
public:
	static Resource& null();

private:
	static Resource* _null;
};

/**
 * In the Leasing framework, Resources are acquired by submitting a lease request to a Lessor, or resource
 * provider.  This class, though primarily used as a naming interface, serves as the base class for any
 * all LeaseRequest implementations.
 */
class LeaseRequest {
private:
	static LeaseRequest* _null;

public:
	static LeaseRequest& null();
};

class LeaseTerms {
private:
	static LeaseTerms* _null;

public:
	static LeaseTerms& null();
	LeaseTerms();
	virtual ~LeaseTerms();

	virtual boolean renew() = 0;
	virtual boolean expired() = 0;
};

class Lease;
class Lessor;
class Lessee;

/**
 * Defines the interface of a Resource provider.  Analogous
 * to the 
 */
class Lessor {
	static Lessor* _null;

public:
	static Lessor& null();

	/**
     * Given a request for a lease, grant or deny the lease.
     * @param request
     * @return
     * @throws LeaseException
     */
	virtual Lease& lease(Lessee& lessee, LeaseRequest& request, LeaseTerms& terms) = 0;

	virtual void leaseInvalidated(Lease& lease) = 0;
};

/**
 * If you want to hold a lease or be a Lessee, you need to provide a method whereby
 * the lessee can be notified of the lease becoming invalidated.
 */
class Lessee {
private:
	static Lessee* _null;

public:
	static Lessee& null();
	virtual void leaseInvalidated(Lease& lease) = 0;
};

class Lease {
private:
	static log4cplus::Logger LOG;

	// For now, the expiry of a lease is based solely on elapsed time.  We may in the future
	// allow for an expiry policy to be supplied so that the expiry policy can be developed
	// independant of the lease itself.
	//
	static int seq;

	Lessee& _lessee;
	Lessor& _lessor;
	Resource& _resource;
	LeaseTerms& _terms;
	int _leaseId;
	boolean _isValid;
	KEP::fast_recursive_mutex _self;
	static Lease* _null;

	// IS THIS EVEN OF VALUE?
	// Object      userObject = null;
	//public void setUserObject( Object userObject )  { this.userObject = userObject; }
	//public Object getUserObject()   { return userObject; }
public:
	Lease(Lessee& lessee, Resource& resource, Lessor& lessor, LeaseTerms& terms);

	static Lease& null();

	/**
     * Invalidate this lease.
     */
	void invalidate();

	boolean isValid();

	/**
     * Obtain a reference to the Lessor of this lease.
     * @return a reference to the Lessor of this lease.
     */
	Lessor& lessor();

	Lessee& lessee();

	/**
     * Determine the duration, or in loan industry speak, the terms, of the lease.
     * @return The terms of the lease.
     * :todo: need to abstract the concept of a lease terms.  Because this need not always be
     * :todo: based on time.   Until need for this identified.  Remove from this interface.
     */
	LeaseTerms& terms();

	/**
     * Obtain a reference to the object being leased.  i.e. The Resource
     * @return reference to the object being leased.
     */
	Resource& resource();

	/**
     * Obtain the unique id of this lease..
     * @return leaseId
     */
	int id();
};

/**
 * For resources that are created dynamically due to non-deterministic events, i.e. a client connecting to
 * an prototerra.pneII.Acceptor, we need a method to insure that some party is willing to take responsibility
 * for the lease.  For this scenario use LeaseAcceptor.  LeaseAcceptor presents an interface that allows
 * a Lessor to offer a lease to a party
 */
class LeaseAcceptor {
public:
	LeaseAcceptor() {}
	virtual ~LeaseAcceptor() {}
	virtual boolean accept(Lease& lease) = 0;
};

class LeaseFactory {
public:
	virtual Lease& newLease(Lessee& lessee, Resource& resource, Lessor& lessor, LeaseTerms& terms) = 0;
};

/**
 * Being aware of a goings with a lease is optional.  For lease Holders that do not care to know or that 
 */
class LeaseListener {
public:
	virtual void invalidated(Lease& lease) = 0;
};

class LeaseOffer {
};

/**
 * Helper implementation for managing leases
 */
class LeasingAgent {
private:
	typedef std::map<int, Lease&> LeaseMapT;
	LeaseMapT _leases;
	LeaseFactory* _leaseFactory;

public:
	class DefaultLeaseFactory : public LeaseFactory {
	public:
		Lease& newLease(Lessee& lessee, Resource& resource, Lessor& lessor, LeaseTerms& terms) {
			return *(new Lease(lessee, resource, lessor, terms));
		}
	};

	LeasingAgent();
	LeasingAgent(LeaseFactory& leaseFactory);

	unsigned int leaseCount();

	/**
	 * Invalidate all leases held by this agent.
	 */
	void invalidate();

	void recoup();

	// :todo:  Add abstraction here like "LeaseFactory" that allows us to abstract away the actual creation
	// of the lease object.
	//
	// How do we handle leases on implicit connections
	//
	Lease& finalizeLease(Lessee& lessee, Resource& resource, Lessor& lessor, LeaseTerms& terms);
};

class RenewalPolicy {
};

/**
 * Helper implementations for the 99%
 */
class LeaseException : public KEP::KEPException {
private:
	//    LeaseException()    {
	//        super();
	//    }
public:
	LeaseException(const std::string information) :
			KEPException(information) {
	}
};

class ResourceUnavailableException : public LeaseException {
	ResourceUnavailableException(const std::string& msg) :
			LeaseException(msg) {
	}
};

class IndefiniteTerm : public LeaseTerms {
	IndefiniteTerm() {}
	virtual ~IndefiniteTerm() {}

	/**
     * Request a renewal of the lease.
     * @return true if the renewal succeeded...i.e the lease and the leased object are still valid.  Else  false.
     */
	boolean renew() { return true; }

	/**
     * Determine if this lease is expired.
     *
     * @return true if this lease has expired, else false.
     */
	boolean expired() { return false; }

	/**
     * Allows user manager of lease to specify the time stamp to be used in the
     * term test.  This allows for a single call to System.currentTimeMillis()
     * @param currentTimeMillis
     * @return True if this lease has expired, else false.
     */
	std::string toString() { return "Indefinite"; }
};

class DurationLeaseTerms : public LeaseTerms {
public:
	DurationLeaseTerms(TimeMs term);
	virtual ~DurationLeaseTerms();

	/**
     * Request a renewal of the lease.
     * @return true if the renewal succeeded...i.e the lease and the leased object are still valid.  Else  false.
     */
	boolean renew();

	/**
     * Determine if this lease is expired.
     *
     * @return true if this lease has expired, else false.
     */
	boolean expired();

private:
	TimeDelay _expires;
	TimeMs _term;
};

/**
 * A very basic implementation of Lessor.
 */
class BasicLessor : public Lessor {
public:
	Lease& lease(Lessee& lessee, LeaseRequest& request, LeaseTerms& terms);
	void leaseInvalidated(Lease& lease);
};

} // namespace Leasing
