///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <common/keputil/CommLogger.h>
#include <Core/Util/StringHelpers.h>
#include <Core/Util/HighPrecisionTime.h>

TEST(CommLoggerTests, CommTestInfo) {
	std::string testtxt("The quick brown fox");
	CommLogger commLogger;
	log4cplus::Logger logger = commLogger.logger();

	log4cplus::LogLevel loggingLevel = logger.getLogLevel();
	if (loggingLevel > log4cplus::INFO_LOG_LEVEL)
		return;

	ASSERT_STREQ(
		"context RECV [netid=1;flags=2;dword=0x54686520]", commLogger.computeLogText(oprecv, log4cplus::INFO_LOG_LEVEL, 1, 2, (const unsigned char*)testtxt.c_str(), testtxt.length(), "context").c_str());
	ASSERT_STREQ(
		"context SEND [netid=1;flags=2;dword=0x54686520]", commLogger.computeLogText(opsend, log4cplus::INFO_LOG_LEVEL, 1, 2, (const unsigned char*)testtxt.c_str(), testtxt.length(), "context").c_str());

	// Testing the next logging levels are a bit more problematic as there is to
	// much opportunity for the content to change and break the test.  For example
	// the address of the buffer being logged will change constantly.
	// It will have to be sufficient to look for appropriate markers in the output.
	// E.g. the ruled output itself will remain static.
	// The leader should not change unless we explicitly change the output format,
	// in which case the test failure would be appropriate.
	//
	// send DEBUG_LOG_LEVEL
	string actual;
	actual = commLogger.computeLogText(opsend, log4cplus::DEBUG_LOG_LEVEL, 1, 2, (const unsigned char*)testtxt.c_str(), testtxt.length(), "context");
	ASSERT_TRUE(StringHelpers::startsWith(
		"context\nSEND [1][2]", actual));
	ASSERT_NE(
		string::npos, actual.find("0x0000|54 68 65 20 71 75 69 63  6b 20 62 72 6f 77 6e 20   The quick brown"));

	// recv DEBUG_LOG_LEVEL
	actual = commLogger.computeLogText(oprecv, log4cplus::DEBUG_LOG_LEVEL, 1, 2, (const unsigned char*)testtxt.c_str(), testtxt.length(), "context");
	ASSERT_TRUE(StringHelpers::startsWith(
		"context\nRECV [1][2]", actual));
	ASSERT_NE(
		string::npos, actual.find("0x0000|54 68 65 20 71 75 69 63  6b 20 62 72 6f 77 6e 20   The quick brown"));
}

/*

Initial timings 1000 iterations (Debug build)

	Info total time(ms) :46.6664
	Info mean time(ms) : 0.0476369
	Debug total time(ms) : 162.056
	Debug mean time(ms) : 0.163014
	Trace total time(ms) : 2593.5
	Trace mean time(ms) : 2.59455

Initial timings 100 iterations (Release build)
	Info total time (ms):9.34737
	Info mean time (ms):0.0105843
	Debug total time (ms):36.8314
	Debug mean time (ms):0.037737
	Trace total time (ms):544.59
	Trace mean time (ms):0.545624
*/
TEST(CommLoggerTests, DISCO_PerfTest01) {
	// Let's use the same test as before.
	// the send/receive aspects should be identical.
	//
	std::string testtxt("The quick brown fox");
	CommLogger commLogger;
	int iterations = 1000;

	// Out of range.  Verify that disabling or failing
	// to configure the commlogger has near 0 overhead.
	//
	{
		Timer t;
		for (int n = 0; n < iterations; n++) {
			string actual = commLogger.computeLogText(opsend, log4cplus::WARN_LOG_LEVEL, 1, 2, (const unsigned char*)testtxt.c_str(), testtxt.length(), "context");
		}
		cout << "Warn total time (ms):" << t.ElapsedMs() << endl;
		cout << " Warn mean time (ms):" << (t.ElapsedMs() / iterations) << endl;
	}

	// So let's do a big loop and determine what our
	// overhead is like.
	//
	// Test info/debug/trace
	{
		Timer t;
		for (int n = 0; n < iterations; n++) {
			string actual = commLogger.computeLogText(opsend, log4cplus::INFO_LOG_LEVEL, 1, 2, (const unsigned char*)testtxt.c_str(), testtxt.length(), "context");
		}
		cout << "Info total time (ms):" << t.ElapsedMs() << endl;
		cout << "Info mean time (ms):" << (t.ElapsedMs() / iterations) << endl;
	}

	{
		Timer t;
		for (int n = 0; n < iterations; n++) {
			string actual = commLogger.computeLogText(opsend, log4cplus::DEBUG_LOG_LEVEL, 1, 2, (const unsigned char*)testtxt.c_str(), testtxt.length(), "context");
		}
		cout << "Debug total time (ms):" << t.ElapsedMs() << endl;
		cout << "Debug mean time (ms):" << (t.ElapsedMs() / iterations) << endl;
	}

	{
		Timer t;
		for (int n = 0; n < iterations; n++) {
			string actual = commLogger.computeLogText(opsend, log4cplus::TRACE_LOG_LEVEL, 1, 2, (const unsigned char*)testtxt.c_str(), testtxt.length(), "context");
		}
		cout << "Trace total time (ms):" << t.ElapsedMs() << endl;
		cout << "Trace mean time (ms):" << (t.ElapsedMs() / iterations) << endl;
	}
}