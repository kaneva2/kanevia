///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

TEST(AccumulatorTest, AccumulatorTest2) {
	// Test basic instantiation operations
	//
	Accumulator<unsigned long> accumulator1;
	accumulator1.accumulate(100ul);

	Accumulator<unsigned long> accumulator2(accumulator1);
	EXPECT_TRUE(accumulator1 == accumulator2);

	accumulator2.accumulate(100ul);
	accumulator1 = accumulator2;
	EXPECT_EQ(accumulator1.accumulation(), 200ul);
	EXPECT_EQ(accumulator1.count(), 2);

	EXPECT_EQ(accumulator1.accumulation(), accumulator2.accumulation());
	EXPECT_EQ(accumulator1.count(), accumulator2.count());
}

// The gtest templates are somehow stomping on the .add() method of AccumulatorSet
// However, this has been tested in situ.  So simply disable this test.
//
TEST(AccumulatorTest, AccumulatorTest3) {
	AccumulatorSet<Numeric<unsigned long>> set1;

	// Before we cause any accumulators to be added to the set,
	// let's verify that looking for a bad index raises and exception
	try {
		set1.accumulator(0);

		// If we get here...something ain't right.
		//
		FAIL();
	} catch (...) {
	}

	// before we start accumulating anything, let's test the enable/disable switch
	//
	AccumulatorSet<unsigned long> set2;
	Accumulator<unsigned long>& acc1 = set2.accumulator("test");
	Accumulator<unsigned long>& acc2 = set2.accumulator("test");

	EXPECT_TRUE(acc1 == acc2);

	//	void* ptr1 = &acc1;

	set2.setEnabled(false);
	acc1.accumulate(1ul);

	EXPECT_EQ(0ul, acc1.accumulation());

	set2.setEnabled(true);
	acc1.accumulate(1ul);
	EXPECT_EQ(1ul, acc1.accumulation());
}

TEST(AccumulatorTest, AccumulatorTest4) {
	AccumulatorSet<Numeric<unsigned long>> accumulator;

	try {
		// Try accumulating by index and verify that an exception is raised.
		//
		accumulator.accumulate(0, Numeric<unsigned long>(1000));
		FAIL();
	} catch (KEPException* ex) {
		if (ex) {
		}
	}

	accumulator.accumulate("test1", Numeric<unsigned long>(1000));
	accumulator.accumulate("test2", Numeric<unsigned long>(1000));

	// Now we should be able to accumulate using an index.
	accumulator.accumulate(0, Numeric<unsigned long>(1000));
	accumulator.accumulate(1, Numeric<unsigned long>(1000));

	EXPECT_TRUE(accumulator.accumulator(0).accumulation() == 2000ul);
	EXPECT_TRUE(accumulator.accumulator(0).count() == 2);
	EXPECT_TRUE(accumulator.accumulator(1).accumulation() == Numeric<unsigned long>(2000));
	EXPECT_TRUE(accumulator.accumulator(1).count() == 2);

	std::stringstream ss;
	ss << accumulator;
	//	const char* s = ss.str().c_str();
	EXPECT_STREQ(ss.str().c_str(), "Accumulators[2]{{name=\"test1\";count=2;cummulative=2000;mean=1000;},{name=\"test2\";count=2;cummulative=2000;mean=1000;}}");
}

TEST(AccumulatorTest, AccumulatorTest1) {
	//	typedef	Numeric<unsigned long>		atomT;
	typedef Accumulator<Numeric<unsigned long>> accumulatorT;

	Accumulator<unsigned long> accumulator("Unit Test Accumulator #1");
	accumulator.accumulate(100);
	accumulator.accumulate(100);
	accumulator.accumulate(100);
	accumulator.accumulate(100);
	EXPECT_TRUE(accumulator.accumulation() == 400);
	EXPECT_TRUE(accumulator.count() == 4);

	accumulatorT accum2("Unit Test Accumulator #2");
	;
	accum2.accumulate(1);
	accum2.accumulate(1);
	accum2.accumulate(1);
	accum2.accumulate(1);
	EXPECT_TRUE(accum2.accumulation() == 4ul);
	EXPECT_TRUE(accum2.count() == 4);

	// Test the stream support
	//
	std::stringstream ss;
	ss << accum2;
	EXPECT_STREQ(ss.str().c_str(), "name=\"Unit Test Accumulator #2\";count=4;cummulative=4;mean=1;");

	// Test divide by zero
	//
	std::stringstream ss2;
	ss2 << accumulatorT("UnitTest Accumulator");
	EXPECT_STREQ(ss2.str().c_str(), "name=\"UnitTest Accumulator\";count=0;cummulative=0;mean=0;");
}

TEST(AccumulatingTimerTest, AccumlatingTimerTest01) {
	Accumulator<Numeric<unsigned long>> accumulator("testaccumulator");
	{
		AccumulatingTimer<Numeric<unsigned long>> timer(accumulator);
		Sleep(2000);
	}

	// We really can't test with a string value as the actual elapsed time is indeterminate
	// and approximate.
	//
	EXPECT_EQ(1, accumulator.count());
	Numeric<unsigned long> acc = accumulator.accumulation();
	unsigned long l = (unsigned long)acc;

	// Unfortunately, sleep can be pretty exact, been observed being as much as 3ms off.
	// for the purposes of this test fudge the control value by 5ms.
	//
	EXPECT_GE(l, 1995ul);
	EXPECT_STREQ("testaccumulator", accumulator.name().c_str());
}

