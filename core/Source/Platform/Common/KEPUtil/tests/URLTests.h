///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Common/KEPUtil/URL.h"
#include "Common/KEPUtil/Algorithms.h"

// Defect replication.  Assigning a url from a
// string is broke.
//
TEST(URLParserTests, AssignFromStringTest) {
	string control = "http://dev-auth.kaneva.com/kepauth/kepauthv6.aspx";
	URL url = control;
	string test = url.str();
	ASSERT_STREQ(control.c_str(), test.c_str());
}
TEST(URLParserTests, AssignFromConstChar) {
	string control = "http://dev-auth.kaneva.com/kepauth/kepauthv6.aspx";
	URL url = "http://dev-auth.kaneva.com/kepauth/kepauthv6.aspx";
	;
	string test = url.str();
	ASSERT_STREQ(control.c_str(), test.c_str());
}

#if 0
TEST( URLParserTests, t1 ) {
	URL::Parser parser;
	ASSERT_TRUE(parser.parse( "scheme://host:8080/dir1/dir2/dir3/file?arg1=value1&arg2=value2#ref"));
	EXPECT_STREQ( "scheme"					, parser._pcScheme	);
	EXPECT_STREQ( "host"					, parser._pcHost	);
	EXPECT_STREQ( "8080"					, parser._pcPort	);
	EXPECT_STREQ( "dir1/dir2/dir3/file"	, parser._pcPath	);
	EXPECT_STREQ( "arg1=value1&arg2=value2"	, parser._pcQuery	);
	EXPECT_STREQ( "ref"						, parser._pcRef		);
}

TEST( URLParserTests, t2 ) {
	URL::Parser parser;
	ASSERT_TRUE(parser.parse( "host:8080/dir1/dir2/dir3/file?arg1=value1&arg2=value2#ref"));
	EXPECT_EQ(		0, parser._pcScheme	);
	EXPECT_STREQ( "host"					, parser._pcHost	);
	EXPECT_STREQ( "8080"					, parser._pcPort	);
	EXPECT_STREQ( "dir1/dir2/dir3/file"	, parser._pcPath	);
	EXPECT_STREQ( "arg1=value1&arg2=value2"	, parser._pcQuery	);
	EXPECT_STREQ( "ref"						, parser._pcRef		);
}

TEST( URLParserTests, t3 ) {
	URL::Parser parser;
	ASSERT_TRUE(parser.parse( "host/dir1/dir2/dir3/file?arg1=value1&arg2=value2#ref"));
	EXPECT_EQ(		0							, parser._pcScheme	);
	EXPECT_STREQ(	"host"						, parser._pcHost	);
	EXPECT_EQ(		0							, parser._pcPort	);
	EXPECT_STREQ(	"dir1/dir2/dir3/file"		, parser._pcPath	);
	EXPECT_STREQ(	"arg1=value1&arg2=value2"	, parser._pcQuery	);
	EXPECT_STREQ(	"ref"						, parser._pcRef		);
}

TEST( URLParserTests, t4 ) {
	URL::Parser parser;
	ASSERT_TRUE(parser.parse( "host?arg1=value1&arg2=value2#ref"));
	EXPECT_EQ(		0							, parser._pcScheme	);
	EXPECT_STREQ(	"host"						, parser._pcHost	);
	EXPECT_EQ(		0							, parser._pcPort	);
	EXPECT_EQ(		0							, parser._pcPath	);
	EXPECT_STREQ(	"arg1=value1&arg2=value2"	, parser._pcQuery	);
	EXPECT_STREQ(	"ref"						, parser._pcRef		);
}

TEST( URLParserTests, t5 ) {
	URL::Parser parser;
	ASSERT_TRUE(parser.parse( "host#ref"));
	EXPECT_EQ(		0							, parser._pcScheme	);
	EXPECT_STREQ(	"host"						, parser._pcHost	);
	EXPECT_EQ(		0							, parser._pcPort	);
	EXPECT_EQ(		0							, parser._pcPath	);
	EXPECT_EQ(		0							, parser._pcQuery	);
	EXPECT_STREQ(	"ref"						, parser._pcRef		);
}

// should be replicated in the ubertests below
TEST( URLParserTests, t6 ) {
	URL::Parser parser;
	ASSERT_TRUE(parser.parse( "host"));
	EXPECT_EQ(		0							, parser._pcScheme	);
	EXPECT_STREQ(	"host"						, parser._pcHost	);
	EXPECT_EQ(		0							, parser._pcPort	);
	EXPECT_EQ(		0							, parser._pcPath	);
	EXPECT_EQ(		0							, parser._pcQuery	);
	EXPECT_EQ(		0							, parser._pcRef		);
}
#endif

TEST(URLTests, InequalityTest01) {
	URL url1 = URL::parse("www.test.com");
	URL url2 = URL::parse("ftp://www.test.com");
	EXPECT_FALSE(url1 == url2);
}

TEST(URLTests, EmptySourceTest) {
	EXPECT_THROW(URL::parse(""), URLParseException);
}

TEST(URLTests, InvalidPortTest01) {
	EXPECT_THROW(URL::parse(":8a80"), URLParseException);
}

TEST(URLTests, InvalidPortTest02) {
	EXPECT_THROW(URL::parse("scheme:8a80"), URLParseException);
}

TEST(URLTests, InvalidPortTest03) {
	EXPECT_THROW(URL::parse("scheme:aa80"), URLParseException);
}

TEST(URLParserTests, NullSourceTest) {
	// URL doesn't accept a c_str as argument, rather it uses a const
	// string reference.
	// But the URLParser class does.
	EXPECT_THROW(URL::Parser().parse(0), URLParseException);
}

// SCHEME breakdown
//
// Covered in uber test
TEST(URLParserTests, DISABLED_t9) {
	URL::Parser parser;
	ASSERT_TRUE(parser.parse("scheme://host/dir1/dir2/dir3/file?arg1=value1&arg2=value2#ref"));
	EXPECT_STREQ("scheme", parser._pcScheme);
	EXPECT_STREQ("host", parser._pcHost);
	EXPECT_EQ(0, parser._pcPort);
	EXPECT_STREQ("dir1/dir2/dir3/file", parser._pcPath);
	EXPECT_STREQ("arg1=value1&arg2=value2", parser._pcQuery);
	EXPECT_STREQ("ref", parser._pcRef);
}

enum TestOptions {
	schemeOpt = 1,
	portOpt = 2,
	pathOpt = 4,
	queryOpt = 8,
	refOpt = 16
};

char* TestScheme = "http";
char* TestHost = "www.simpleqa.com";
char* TestPort = "8080";
char* TestPath = "dir1/dir2/dir3/file";
char* TestQuery = "arg1=value1&arg2=value2";
char* TestRef = "ref";

void describeURL(char** description, int testNum) {
	description[0] = ((testNum & schemeOpt) > 0) ? TestScheme : 0;
	description[1] = ((testNum & portOpt) > 0) ? TestPort : 0;
	description[2] = ((testNum & pathOpt) > 0) ? TestPath : 0;
	description[3] = ((testNum & queryOpt) > 0) ? TestQuery : 0;
	description[4] = ((testNum & refOpt) > 0) ? TestRef : 0;
}

std::string buildURL(char** description) {
	std::stringstream ss;
	if (description[0] != 0)
		ss << TestScheme << "://";
	ss << TestHost;
	if (description[1] != 0)
		ss << ":" << TestPort;
	if (description[2] != 0)
		ss << "/" << TestPath;
	if (description[3] != 0)
		ss << "?" << TestQuery;
	if (description[4] != 0)
		ss << "#" << TestRef;
	return ss.str();
}

TEST(URLParserTests, ParserUberTest) {
	char* testDesc[5];
	for (int testNum = 0; testNum < 32; testNum++) {
		describeURL(testDesc, testNum);
		std::string url = buildURL(testDesc);
		{
			URL::Parser parser;
			EXPECT_TRUE(parser.parse(url.c_str())) << " test[" << testNum << "][" << url.c_str() << "]";

			EXPECT_STREQ(TestHost, parser._pcHost) << " test[" << testNum << "][" << url.c_str() << "]";

			if (testDesc[0] == 0)
				EXPECT_EQ(testDesc[0], parser._pcScheme) << " test[" << testNum << "][" << url.c_str() << "]";
			else
				EXPECT_STREQ(testDesc[0], parser._pcScheme) << " test[" << testNum << "][" << url.c_str() << "]";

			if (testDesc[1] == 0)
				EXPECT_EQ(testDesc[1], parser._pcPort) << " test[" << testNum << "][" << url.c_str() << "]";
			else
				EXPECT_STREQ(testDesc[1], parser._pcPort) << " test[" << testNum << "][" << url.c_str() << "]";

			if (testDesc[2] == 0)
				EXPECT_EQ(testDesc[2], parser._pcPath) << " test[" << testNum << "][" << url.c_str() << "]";
			else
				EXPECT_STREQ(testDesc[2], parser._pcPath) << " test[" << testNum << "][" << url.c_str() << "]";

			if (testDesc[3] == 0)
				EXPECT_EQ(testDesc[3], parser._pcQuery) << " test[" << testNum << "][" << url.c_str() << "]";
			else
				EXPECT_STREQ(testDesc[3], parser._pcQuery) << " test[" << testNum << "][" << url.c_str() << "]";

			if (testDesc[4] == 0)
				EXPECT_EQ(testDesc[4], parser._pcRef) << " test[" << testNum << "][" << url.c_str() << "]";
			else
				EXPECT_STREQ(testDesc[4], parser._pcRef) << " test[" << testNum << "][" << url.c_str() << "]";
		}
	}
}

TEST(URLTests, SpecificTest01) {
	URL url = URL::parse("http://www.simpleqa.com");
	EXPECT_STREQ("http", url.scheme().c_str());
}

TEST(URLTests, UberTest) {
	char* testDesc[5];
	for (int testNum = 0; testNum < 32; testNum++) {
		describeURL(testDesc, testNum);
		std::string urlString = buildURL(testDesc);
		try {
			URL url = URL::parse(urlString);

			EXPECT_STREQ(TestHost, url.host().c_str());

			if (testDesc[0] == 0)
				EXPECT_STREQ("", url.scheme().c_str());
			else
				EXPECT_STREQ(testDesc[0], url.scheme().c_str());

			if (testDesc[1] == 0)
				EXPECT_EQ(0, url.port());
			else
				EXPECT_EQ(atoi(testDesc[1]), url.port());

			if (testDesc[2] == 0)
				EXPECT_STREQ("", url.pathString().c_str());
			else
				EXPECT_STREQ(testDesc[2], url.pathString().c_str());

			if (testDesc[3] == 0)
				EXPECT_STREQ("", url.queryString().c_str());
			else
				EXPECT_STREQ(testDesc[3], url.queryString().c_str());

			if (testDesc[4] == 0)
				EXPECT_STREQ("", url.reference().c_str());
			else
				EXPECT_STREQ(testDesc[4], url.reference().c_str());

			// Verify the copy constructor
			URL copy(url);
			if (testDesc[0] == 0)
				EXPECT_STREQ("", copy.scheme().c_str());
			else
				EXPECT_STREQ(testDesc[0], copy.scheme().c_str());

			if (testDesc[1] == 0)
				EXPECT_EQ(0, copy.port());
			else
				EXPECT_EQ(atoi(testDesc[1]), copy.port());

			if (testDesc[2] == 0)
				EXPECT_STREQ("", copy.pathString().c_str());
			else
				EXPECT_STREQ(testDesc[2], copy.pathString().c_str());

			if (testDesc[3] == 0)
				EXPECT_STREQ("", copy.queryString().c_str());
			else
				EXPECT_STREQ(testDesc[3], copy.queryString().c_str());

			if (testDesc[4] == 0)
				EXPECT_STREQ("", copy.reference().c_str());
			else
				EXPECT_STREQ(testDesc[4], copy.reference().c_str());

			// Verify assignment operator
			//
			copy = url;
			if (testDesc[0] == 0)
				EXPECT_STREQ("", copy.scheme().c_str());
			else
				EXPECT_STREQ(testDesc[0], copy.scheme().c_str());

			if (testDesc[1] == 0)
				EXPECT_EQ(0, copy.port());
			else
				EXPECT_EQ(atoi(testDesc[1]), copy.port());

			if (testDesc[2] == 0)
				EXPECT_STREQ("", copy.pathString().c_str());
			else
				EXPECT_STREQ(testDesc[2], copy.pathString().c_str());

			if (testDesc[3] == 0)
				EXPECT_STREQ("", copy.queryString().c_str());
			else
				EXPECT_STREQ(testDesc[3], copy.queryString().c_str());

			if (testDesc[4] == 0)
				EXPECT_STREQ("", copy.reference().c_str());
			else
				EXPECT_STREQ(testDesc[4], copy.reference().c_str());

			// Verify toString()
			//
			EXPECT_STREQ(urlString.c_str(), copy.toString().c_str());

			EXPECT_TRUE(copy == url);
		} catch (URLParseException&) {
			FAIL() << "Undexpected exception!";
		}
	}
}

TEST(URLTests, FailureTest01) {
	//	"pv-wok.kaneva.com/kgp/validateuser.aspx"	std::basic_string<char,std::char_traits<char>,std::allocator<char> >
	try {
		URL url = URL::parse("pv-wok.kaneva.com/kgp/validateuser.aspx");
		EXPECT_STREQ("pv-wok.kaneva.com", url.host().c_str());
	} catch (URLParseException&) {
		FAIL();
	}
}

TEST(URLTests, SetParamsTest) {
	URL::ParamVector params;
	params.push_back(URL::KeyValuePair("Key1", "Value1"));
	params.push_back(URL::KeyValuePair("Key2", "Value2"));

	URL url;
	url.setScheme("http:")
		.setHost("host")
		.setPort(8080)
		.setReference("TestReference")
		.setParams(params)
		.addPath("dir1")
		.addPath("dir2")
		.addPath("index.html");

	EXPECT_STREQ("http:://host:8080/dir1/dir2/index.html?Key1=Value1&Key2=Value2#TestReference", url.toString().c_str());
}

TEST(URLTests, ParamMapTest01) {
	// Instantiate from parse
	//
	URL url = URL::parse("http://www.test.com?a=b&c=d");
	ASSERT_TRUE(url.paramMap().find("a") != url.paramMap().end());

	// Verify that the parsed param is in the param map
	//
	url.unsetParam("a");
	ASSERT_TRUE(url.paramMap().find("a") == url.paramMap().end());

	// Put the parameter back and verify that it was added successfully
	//
	url.setParam("a", "b");
	ASSERT_TRUE(url.paramMap().find("a") != url.paramMap().end());

	// Does the URL copy constructor correctly pick up params from the source url?
	//
	URL urlCopy(url);
	ASSERT_TRUE(urlCopy.paramMap().find("a") != urlCopy.paramMap().end());

	// Verify that the data structures between the two URLs are unlinked
	// Remove from the copy and verify it is still in the original
	//
	urlCopy.unsetParam("a");
	ASSERT_TRUE(url.paramMap().find("a") != url.paramMap().end());

	// Does the assigment operator copy the param map successfully?
	//
	urlCopy = url;
	ASSERT_TRUE(urlCopy.paramMap().find("a") != urlCopy.paramMap().end());

	// Verify that the data structure between the two URLs are unlinked.
	// Remove from the original and verify that it is still in the copy.
	//
	url.unsetParam("a");
	ASSERT_TRUE(urlCopy.paramMap().find("a") != urlCopy.paramMap().end());

	// Does the comparison operator account for the map correctly?
	//
	url.setParam("a", "b");
	ASSERT_TRUE(url == urlCopy);

	// Does clear rest the param map correctly?
	//
	urlCopy.clearParams();
	ASSERT_TRUE(urlCopy.paramMap().find("a") == urlCopy.paramMap().end());

	// Does the bulk set work correctly?
	vector<pair<string, string>> params;
	params.push_back(pair<string, string>("a", "b"));
	params.push_back(pair<string, string>("c", "d"));
	urlCopy.setParams(params);
	ASSERT_TRUE(urlCopy.paramMap().find("a") != urlCopy.paramMap().end());
}
