///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/KEPUtil/ExtendedConfig.h"
#include "Common/KEPUtil/URL.h"
#include "TinyXML/tinyxml.h"
#include <log4cplus/logger.h>
#include "Common/include/KEPConfig.h"
#include "KEPConfig.h"
#include "Common/KEPUtil/Tempfile.h"

#include <memory>

TEST(TIXMLConfigTests, InitializationTest004) {
	TIXMLConfig config(0);
	config.initialize("test"); //.setDeclaration();
	EXPECT_STREQ("<test />", config.toString().c_str());
}

TEST(TIXMLConfigTests, InitializationTest001) {
	TIXMLConfig config;
	config.initialize("test");
	EXPECT_STREQ("<test />", config.toString().c_str());
}

// Test initialization of a config by specifying the name of the root element
// to the constructor
//
TEST(TIXMLConfigTests, InitializationTest002) {
	TIXMLConfig config("test");
	config.showDebug();
	EXPECT_STREQ("<test />", config.toString().c_str());
}

// Verify that if we are messing with an uninitialized Item when attempting
// to initialize a configuration, that an UninitializedException thrown
//
TEST(TIXMLConfigTests, InitializationTest003) {
	TIXMLConfig config;

	// We can create a blank configuration item...
	//
	TIXMLConfig::Item i;
	try {
		// but we can't do anything with it...until it is initialized.
		//
		config.initialize(i);
		FAIL() << "UninitializedException expected";
	} catch (UninitializedException& /* ue */) {
	} catch (...) {
		FAIL() << "UninitializedException expected";
	}
}

TEST(TIXMLConfigTests, ConfigStreamTest001) {
	TIXMLConfig config("test");
	stringstream ss;
	ss << config;
	EXPECT_STREQ("<test />", ss.str().c_str());
	config.showDebug();
}

// Test handling of detecting a nil item.
// Item is simply a light weight wrapper around a TiXmlElement (possibly should be node level)
// Because the wrapper is light weight,  We throw around instances on the stack, and do not have
// to worry about memory management.
//
// But, the wrapper may reference a nil implementation pointer.
//
TEST(TIXMLConfigTests, ConfigNilTest000) {
	// When querying the root of an un initialized document...
	//
	TIXMLConfig config;

	// ...we should get nil item, which is by definition
	// any uninitialized Item
	//
	EXPECT_TRUE(TIXMLConfig::Item() == config.root());
}

// Conversely, if we initialize the configuration by supplying
// the name of a root node on construction, the root should
// not be nil.
//
TEST(TIXMLConfigTests, ConfigNilTest001) {
	// When querying the root of an initialized document...
	//
	TIXMLConfig config("test");

	// ...we should NOT get a nil item
	//
	EXPECT_TRUE(TIXMLConfig::Item() != config.root());
}

// When doing a find on a non-existent path,
// should get nil item
//
TEST(TIXMLConfigTests, ConfigFindTest000) {
	TIXMLConfig config("test");
	EXPECT_TRUE(TIXMLConfig::Item() == config.find("test\\test1\\test11"));
}

// When doing a find on a non-existent path,
// should get nil item
//
TEST(TIXMLConfigTests, ConfigFindTest001) {
	TIXMLConfig config;
	//	config.parse("<root></root>");
	config.parse("<root><test/></root>");
	EXPECT_TRUE(TIXMLConfig::Item() != config.find("test"));
	EXPECT_STREQ("<test />", config.find("test").toString().c_str());
}

// When doing a find on a non-existent path,
// should get nil item
//
TEST(TIXMLConfigTests, ConfigFindTest002) {
	TIXMLConfig config;
	config.parse("<root><test1/></root>");
	EXPECT_STREQ("<test1 />", config.find("test1").toString().c_str());
}

TEST(TIXMLConfigTests, CloneTest001) {
	//
	try {
		TIXMLConfig config1;
		config1.parse("<root><level1><level2><level3/></level2></level1></root>").setName("config1");
		config1.showDebug();
		TIXMLConfig config2;
		config2.parse("<root><level1><level2><level3/></level2></level1></root>").setName("config2");
		config2.showDebug();

		{
			TIXMLConfig::Item i = config2.find("level1");
			TIXMLConfig::Item rootItem = config1.root();
			rootItem.appendItem(i);

			config1.showDebug();
			config2.showDebug();
			//cout << config2 << endl;
			EXPECT_STREQ("<root><level1><level2><level3 /></level2></level1><level1><level2><level3 /></level2></level1></root>", config1.toString().c_str());
		}
	} catch (const ChainedException& exc) {
		FAIL() << "Unexpected Exception [" << exc.str() << "]";
	}
}

// --gtest_filter=TIXMLConfigTests.ConfigBuildMergeTest001
TEST(TIXMLConfigTests, ConfigBuildMergeTest001) {
	//
	try {
		TIXMLConfig config1;
		config1.parse("<root><item1></item1></root>").setName("config1");
		TIXMLConfig config2;
		config2.parse("<root><item2><item3/></item2></root>").setName("config2");
		config1.root().appendItem(config2.find("item2"));
		config1.showDebug();
		config2.showDebug();
		//cout << config2 << endl;
		EXPECT_STREQ("<root><item1 /><item2><item3 /></item2></root>", config1.toString().c_str());
	} catch (const ChainedException& exc) {
		FAIL() << "Unexpected Exception [" << exc.str() << "]";
	}
}

// Build a complex document and query it.
//
TEST(TIXMLConfigTests, ConfigTest010) {
	try {
		TIXMLConfig config("root");
		config.root().appendItem(TIXMLConfig::Item("sub1"));
		TIXMLConfig::Item initialize = config.find("sub1");
		config.find("sub1").appendItem(TIXMLConfig::Item("sub2"));
		char* p = "<root><sub1><sub2 /></sub1></root>";
		EXPECT_STREQ(p, config.toString().c_str());
	} catch (const ChainedException& exc) {
		FAIL() << "Unexpected Exception [" << exc.str() << "]";
	}
}

// Should fail under current implementation because item destructor will delete underlying elment
//
TEST(TIXMLConfigTests, ConfigTest0101) {
	try {
		TIXMLConfig config("root");
		config.root().appendItem(TIXMLConfig::Item("sub1"));
		config.find("/sub1").appendItem(TIXMLConfig::Item("sub2"));

		{
			TIXMLConfig::Item i1 = config.find("/sub1");
			TIXMLConfig::Item i2 = config.find("/sub1/sub2");
		}
		EXPECT_STREQ("<root><sub1><sub2 /></sub1></root>", config.toString().c_str());
	} catch (const ChainedException& exc) {
		FAIL() << "Unexpected Exception [" << exc.str() << "]";
	}
}

//////// TRAVERSAL TESTS ////////
//////// TRAVERSAL TESTS ////////
//////// TRAVERSAL TESTS ////////

/**
 * TUTORIAL: Iterating the config.
 * For any given operation, TIXMLConfig is thread safe.  This includes iteration.
 * This is accomplished via the Visitor pattern coupled with IoC/DI
 * 
 * To traverse a configuration, create an instance of TIXMLConfig::ItemVisitor as
 * the following class illustrates.  This implementation simply counts the number
 * of items visited. See the the subsequent unit tests for what
 * happens next.
 */
class BasicTestVisitor : public TIXMLConfig::ItemVisitor {
public:
	int _count;
	BasicTestVisitor() :
			TIXMLConfig::ItemVisitor(), _count(0) {}

	/**
     * This is the business end of an ItemVisitor
     * 
     * @see TIXMLConfig::ItemVisitor
     */
	BasicTestVisitor& visit(TIXMLConfig::Item& /* destination */) {
		_count++;
		return *this;
	}
};

/**
 * In the following test we use the BasicTestVisitor declared above
 * to count the number of items in the root of the configuration.
 *
 * Note that while this configuration has more than level (excluding the root)
 * we only traverse the first level.
 */
TEST(TIXMLConfigTests, ConfigTraversalTest001) {
	TIXMLConfig config1;
	config1.parse("<root><item1><subitem1/></item1><item2><subitem2/></item2><item3><subitem3/></item3></root>");

	BasicTestVisitor visitor;
	config1.root().traverse(visitor);
	EXPECT_EQ(3, visitor._count);
}

/**
 * The following implementation of ItemVisitor illustrates how to traverse sublevels, in this
 * case, it counts all items in a multilevel configuration.  See the visit() method
 * of this class for the details.
 */
class RecursiveTestVisitor : public TIXMLConfig::ItemVisitor {
public:
	int _count;
	RecursiveTestVisitor() :
			TIXMLConfig::ItemVisitor(), _count(0) {}

	/**
     * This is the business end of an ItemVisitor
     * 
     * @see TIXMLConfig::ItemVisitor
     */
	RecursiveTestVisitor& visit(TIXMLConfig::Item& destination) {
		_count++;

		// Note that TIXMLConfig::traverse() is simply shorthand
		// for TIXMLConfig::root().traverse(myvisitor)
		//
		// Essentially we recurse into the lower levels by calling
		// the Items traverse() method as follows:
		//
		destination.traverse(*this);

		return *this;
	}
};

/**
 * And here we traverse the entire configuration.
 *     --gtest_filter=TIXMLConfigTests.ConfigTraversalTest002
 */
TEST(TIXMLConfigTests, ConfigTraversalTest002) {
	TIXMLConfig config1;
	config1.parse("<root><level1><level2><level3/></level2></level1></root>").setName("config1");

	TIXMLConfig config2;
	config2.parse("<root><level1><level2><level3/></level2></level1></root>").setName("config2");

	config1.root().appendItem(config2.find("level1"));

	//cout << config1;
	RecursiveTestVisitor visitor;
	config1.root().traverse(visitor);
	EXPECT_EQ(6, visitor._count);
}

TEST(TIXMLConfigTests, AppendItemTest001) {
	TIXMLConfig config1;
	config1.parse("<root><level1><level2><level3/></level2></level1></root>");

	TIXMLConfig config2;
	config2.parse("<root><level1><level2><level3/></level2></level1></root>");
	config1.root().appendItem(config2.find("level1"));

	stringstream actual;
	actual << config1;

	stringstream expected;
	expected << "<root>"
			 << "<level1>"
			 << "<level2>"
			 << "<level3 />"
			 << "</level2>"
			 << "</level1>"
			 << "<level1>"
			 << "<level2>"
			 << "<level3 />"
			 << "</level2>"
			 << "</level1>"
			 << "</root>";
	EXPECT_STREQ(expected.str().c_str(), actual.str().c_str());
}

//////// LOGICALS TESTS ////////
//////// LOGICALS TESTS ////////
//////// LOGICALS TESTS ////////
//
// Test the use of logical translation.
//

/**
 *
 */
TEST(TIXMLConfigTests, LogicalsTest001) {
	stringstream ss;
	ss << "<root>"
	   << "<logicals name=\"test\" delims=\"$()\">" // We
	   << "<logical pattern=\"TEST_LOGICAL\">" // define
	   << "Now is the time for all good men to come to the aid of their nation" // the
	   << "</logical>" // logicals
	   << "</logicals>" // here.
	   << "<test>%(TEST_LOGICAL)</test>" // We should expect to find them applied here
	   << "</root>";

	TIXMLConfig config1;

	config1.parse(ss.str());
	TIXMLConfig::Item i = config1.find("test");
	string s = i.text();
	ASSERT_STREQ("Now is the time for all good men to come to the aid of their nation", i.text().c_str());
}

// Insure that:
//
//	TIXMLConfig::Item.attributeText(std::string)  supports returns logicals
//  TIXMLConfig::Item.attribute(std::string,int)
//
// Note that the attribute() methods in this class are intended to be straight passthroughs
// to TiXmlElement thus allowing for seemless use of the TiXMLConfigAdapter.
// However,
//
//	TiXmlElement.Attribute(const char*) returns a const char* which does not
// play nice with our need to compute logicals.  To that end I've introduced
// a new method TIXMLItem::attributeText)
// Both support logicals
//
TEST(TIXMLConfigTests, LogicalsTest002) {
	stringstream ss;
	ss << "<root>"
	   << "<logicals name=\"test\" delims=\"$()\">" // We
	   << "<logical pattern=\"TEST_LOGICAL\">" // define
	   << "Now is the time for all good men to come to the aid of their nation" // the
	   << "</logical>" // logicals
	   << "<logical pattern=\"TEST_ATTRIBUTE\">" // define
	   << "attrvalue"
	   << "</logical>" // logicals
	   << "</logicals>" // here.
	   << "<test attr=\"%(TEST_ATTRIBUTE)\">%(TEST_LOGICAL)</test>" // We should expect to find them applied here
	   << "</root>";

	TIXMLConfig config1;

	config1.parse(ss.str());
	TIXMLConfig::Item i = config1.find("test");
	string s = i.text();
	ASSERT_STREQ("Now is the time for all good men to come to the aid of their nation", i.text().c_str());
	ASSERT_STREQ("attrvalue", i.attribute("attr").c_str());
}

//
TEST(TIXMLConfigTests, DISABLED_ConfigConflationTest01) {
	TempFile tempFile(FileName::createTemp("xml", "KEPConfigAdapterTests_SaveTest01")); // Create a temporary file

	TIXMLConfig logicalsConfig; // Create a configuration object
	{ // and populate it by parsing
		stringstream ss; // string
		ss << "<logicals name=\"test\" delims=\"%()\">" //
		   << "<logical pattern=\"WOKHOST\">pv-wok.kaneva.com:8080</logical>" //
		   << "<logical pattern=\"AUTHHOST\">pv-auth.kaneva.com</logical>" //
		   << "</logicals>"; //
		logicalsConfig.parse(ss.str()); //
	}
	TIXMLConfigHelper(logicalsConfig).saveAs(tempFile.name().toString()); // Save this configuration object to
		// the temporary file

	TIXMLConfig config; // Create a second config that references
	{ // the configuration above.
		stringstream ss; //
		ss << "<root>" //
		   << "<ENTITY url=\"file://" << tempFile.name().toString() << "\" />" //
		   << "</root>"; //
		config.parse(ss.str()); //
	} //

	stringstream expected;
	expected << "<root>"
			 << "<ENTITY url=\"file://" << tempFile.name().toString() << "\" />"
			 << "<logicals name=\"test\" delims=\"%()\" ENTITY_URL=\"file://" << tempFile.name().toString() << "\">"
			 << "<logical pattern=\"WOKHOST\">pv-wok.kaneva.com:8080</logical>"
			 << "<logical pattern=\"AUTHHOST\">pv-auth.kaneva.com</logical>"
			 << "</logicals>"
			 << "</root>";

	stringstream actual;
	actual << config;
	EXPECT_STREQ(expected.str().c_str(), actual.str().c_str());
	//	EXPECT_TRUE(DeleteFile( tempFile.name().toString().c_str() ) == TRUE);
}

// Test include of external file in the same directory as the referencing file
// as specified by a relative path.
//
TEST(TIXMLConfigTests, DISABLED_ConfigConflationTest02) {
	TempFile logicalsFile(FileName::createTemp("xml", "KEPConfigAdapterTests_SaveTest01_logicals")); // Create a temporary file
	TIXMLConfig logicalsConfig; // Create a configuration object
	{ // and populate it by parsing
		stringstream ss; // string
		ss << "<logicals name=\"test\" delims=\"%()\">" //
		   << "<logical pattern=\"WOKHOST\">pv-wok.kaneva.com:8080</logical>" //
		   << "<logical pattern=\"AUTHHOST\">pv-auth.kaneva.com</logical>" //
		   << "</logicals>"; //
		logicalsConfig.parse(ss.str()); //
	}
	TIXMLConfigHelper(logicalsConfig).saveAs(logicalsFile.name().toString()); // Save this configuration object to
		// the temporary file

	TempFile mainFile(FileName::createTemp("xml", "KEPConfigAdapterTests_SaveTest01_mainfile"));
	TIXMLConfig config; // Create a second config that references
	{ // the configuration above.
		stringstream ss; //
		ss << "<root>" //
		   << "<ENTITY url=\"file://" << logicalsFile.name().toString() << "\" />" //
		   << "</root>"; //
		config.parse(ss.str()); //
	} //
	TIXMLConfigHelper(logicalsConfig).saveAs(logicalsFile.name().toString()); // Save this configuration object to

	stringstream expected;
	expected << "<root>"
			 << "<ENTITY url=\"file://" << logicalsFile.name().toString() << "\" />"
			 << "<logicals name=\"test\" delims=\"%()\" ENTITY_URL=\"file://" << logicalsFile.name().toString() << "\">"
			 << "<logical pattern=\"WOKHOST\">pv-wok.kaneva.com:8080</logical>"
			 << "<logical pattern=\"AUTHHOST\">pv-auth.kaneva.com</logical>"
			 << "</logicals>"
			 << "</root>";

	stringstream actual;
	actual << config;
	EXPECT_STREQ(expected.str().c_str(), actual.str().c_str());
	//	EXPECT_TRUE(DeleteFile( tempFile.name().toString().c_str() ) == TRUE);
}
//#endif
class TIXMLConfigTests01 : public ::testing::Test {
public:
	void SetUp() {
		logicalsFile.open(FileName::createTemp("xml", "KEPConfigAdapterTests_SaveTest01_logicals-"), ios_base::trunc | ios_base::out); // Create a temporary file
		logicalsFile << "<logicals name=\"test\" delims=\"%()\">" //
					 << "<logical pattern=\"WOKHOST\">pv-wok.kaneva.com:8080</logical>" //
					 << "<logical pattern=\"AUTHHOST\">pv-auth.kaneva.com</logical>" //
					 << "</logicals>"; //
		logicalsFile.close();

		mainFile.open(FileName::createTemp("xml", "KEPConfigAdapterTests_SaveTest01_mainfile-"), ios_base::trunc | ios_base::out);
		mainFile << "<root>" //
				 << "<ENTITY url=\"file://" << logicalsFile.name().toString((FileName::Name | FileName::Ext)) << "\" />" //
				 << "</root>"; //
		mainFile.close();
	}

	TempFile logicalsFile;
	TempFile mainFile;
};

// Test include of external file in the same directory as the referencing file
// as specified by a relative path.
//
TEST_F(TIXMLConfigTests01, BROKEN_ConfigConflationTest03) {
	try {
		stringstream expected;
		URL url = URL::parse("file://" + TIXMLConfigTests01::logicalsFile.name().toString());
		string t3 = url.toString();
		expected << "<root>"
				 << "<ENTITY url=\"file://" << TIXMLConfigTests01::logicalsFile.name().toString(FileName::Name | FileName::Ext) << "\" />"
				 // Temporary??? why???
				 //					<< "<logicals name=\"test\" delims=\"%()\" ENTITY_URL=\"file://" << TIXMLConfigTests01::logicalsFile.name().toString() << "\">"
				 << "<logicals name=\"test\" delims=\"%()\" ENTITY_URL=\"" << url.toString() << "\">"
				 << "<logical pattern=\"WOKHOST\">pv-wok.kaneva.com:8080</logical>"
				 << "<logical pattern=\"AUTHHOST\">pv-auth.kaneva.com</logical>"
				 << "</logicals>"
				 << "</root>";
		// cout << "Expected: " << expected.str() << endl;
		stringstream actual;
		TIXMLConfig config;
		string t2 = TIXMLConfigTests01::mainFile.name().toString();
		TIXMLConfigHelper(config).load(t2); // Save this configuration object to
		config.showDebug();
		actual << config;
		string t(actual.str());
		//    cout << "Actual: " << actual.str() << endl;
		EXPECT_STREQ(expected.str().c_str(), actual.str().c_str());
	} catch (const ConfigurationException& cexc) {
		FAIL() << cexc.str().c_str();
	}
}

// This is how we'll save the file without writing externs to the wrong file.
// If the element contains external data, write to the external file not, this one.
//
class MyDiscoveryVisitor : public TiXmlVisitor {
private:
	unsigned int _level;

public:
	MyDiscoveryVisitor() :
			TiXmlVisitor(), _level(0) {}
	MyDiscoveryVisitor(const MyDiscoveryVisitor& other) :
			TiXmlVisitor(other), _level(0) {}
	virtual ~MyDiscoveryVisitor() {}
	virtual bool Visit(const TiXmlComment& comment) {
		_level++;

		_level--;
		return TiXmlVisitor::Visit(comment);
	}

	void indent() {
		for (unsigned int i = 0; i < _level; i++) cout << " ";
	}

	virtual bool Visit(const TiXmlDeclaration& /*declaration*/) {
		return true;
	}
	virtual bool Visit(const TiXmlUnknown& /*unknown*/) { return true; }
	virtual bool Visit(const TiXmlText& /*text*/) { return true; }
	virtual bool VisitEnter(const TiXmlDocument& /*document*/) { return true; }
	virtual bool VisitExit(const TiXmlDocument& /*document*/) { return true; }
	virtual bool VisitEnter(const TiXmlElement& element, const TiXmlAttribute* /*attribute*/) {
		// returing false on enter indicates "do not enter the element"
		if (element.ValueStr() == "item1")
			return false;

		indent();
		//cout << "<" << element.ValueStr() << ">" << endl;
		_level++;
		return true;
	}

	virtual bool VisitExit(const TiXmlElement& element) {
		// returning false on exit indicates "do not visit my siblings"
		if (element.ValueStr() == "item1")
			return true;
		_level--;
		indent();
		//cout << "</" << element.ValueStr() << ">" << endl;
		return true;
	}
};

// The following tests validate the KEPConfigAdapter class.  This class provides
// compatibility with the legacy KEPConfig class.
//

// Reasons for not adding file save/load operations support to Config
// 1)	In a decoupled system, it easy enough to gain streaming capabilities
//		that allow us to store/load from resources other than a file.  This
//		means there is little value in maintaining explicit support for
//		these operations.
// 2)   Config is an abstraction to aid in the dessemination and management
//		of configuration data.  While Config makes efforts to hide the
//		implementing XML libraries (e.g. xerces v tinyxml) it really makes
//		no attempt to hide the fact that the implementation leverages XML
//		heavily.  Having said that, it is not my belief that all XML
//		implementations support read/write caps and by simply adding
//		streaming support, we get a lot more value.
// 3)   The most basic reason is this: Separation of Concerns.  Let Config
//		concern on providing robust, reliable "config" operations.  Let an
//		io centric class consern worry about the io issue.

// test setter/getters
// test Load()
// test Save()
// test Save() with external entities.
// The actual unit tests.  Not discovery.
TEST(KEPConfigAdapterTests, ValueExistsTest01) {
	stringstream ss;
	ss << "<KGPServer>"
	   << "</KGPServer>";
	TIXMLConfig impl;
	impl.parse(ss.str());
	KEPConfigAdapter cfg(impl, false);
	EXPECT_FALSE(cfg.ValueExists("TickCounterTareOffset"));
}

TEST(KEPConfigAdapterTests, ValueExistsTest02) {
	stringstream ss;
	ss << "<KGPServer>"
	   << "<!-- TickCounterTareOffset>0x80000001</TickCounterTareOffset -->"
	   << "</KGPServer>";
	TIXMLConfig impl;
	impl.parse(ss.str());
	KEPConfigAdapter cfg(impl, false);
	EXPECT_FALSE(cfg.ValueExists("TickCounterTareOffset"));
}

TEST(KEPConfigAdapterTests, GetValueTest01) {
	TIXMLConfig impl;
	impl.parse("<root>test</root>");

	EXPECT_STREQ("test", impl.root().text().c_str());

	string s;
	KEPConfigAdapter cfg(impl, false);
	EXPECT_FALSE(cfg.GetValue("root", s));
	EXPECT_TRUE(cfg.GetValue("", s));
	EXPECT_STREQ("test", s.c_str());
}

// Test GetString for non-root element
//
TEST(KEPConfigAdapterTests, GetValueTest02) {
	KEPConfigAdapter cfg;
	string s;
	cfg.Parse("<root><sub>test</sub></root>");
	EXPECT_FALSE(cfg.GetValue("root/sub", s));
	EXPECT_TRUE(cfg.GetValue("sub", s));
	EXPECT_STREQ("test", s.c_str());
}

// Get int from root and non-root
TEST(KEPConfigAdapterTests, GetIntValueTest01) {
	KEPConfigAdapter cfg;
	int n;
	cfg.Parse("<root>99</root>");
	EXPECT_FALSE(cfg.GetValue("root", n));
	EXPECT_TRUE(cfg.GetValue("", n));
	EXPECT_EQ(99, n);
}

TEST(KEPConfigAdapterTests, GetIntValueTest02) {
	KEPConfigAdapter cfg;
	int n;
	cfg.Parse("<root><sub>99</sub></root>");
	EXPECT_FALSE(cfg.GetValue("root/sub", n));
	EXPECT_TRUE(cfg.GetValue("sub", n));
	EXPECT_EQ(99, n);
}

// non-root
TEST(KEPConfigAdapterTests, GetBooleanValueTest01) {
	KEPConfigAdapter cfg;
	bool b = false;
	cfg.Parse("<root><sub>99</sub></root>", 0);
	EXPECT_FALSE(cfg.GetValue("root/sub", b));
	EXPECT_TRUE(cfg.GetValue("sub", b));
	EXPECT_TRUE(b);
}

TEST(KEPConfigAdapterTests, GetBooleanValueTest02) {
	KEPConfigAdapter cfg;
	bool b = true;
	cfg.Parse("<root><sub>0</sub></root>");
	EXPECT_FALSE(cfg.GetValue("root/sub", b));
	EXPECT_TRUE(cfg.GetValue("sub", b));
	EXPECT_FALSE(b);
}

TEST(KEPConfigAdapterTests, GetDoubleValueTest01) {
	KEPConfigAdapter cfg;
	double d = 0.0f;
	cfg.Parse("<root><sub>1.0</sub></root>");
	EXPECT_FALSE(cfg.GetValue("root/sub", d));
	EXPECT_TRUE(cfg.GetValue("sub", d));
	EXPECT_TRUE(d == 1.0f);
}

// Test all of the short hands gets
//
TEST(KEPConfigAdapterTests, GetStringTest01) {
	KEPConfigAdapter cfg;
	cfg.Parse("<root><sub>test</sub></root>");
	EXPECT_STREQ("test", cfg.Get("sub", "").c_str());
}

TEST(KEPConfigAdapterTests, GetStringTest02) {
	KEPConfigAdapter cfg;
	cfg.Parse("<root>test</root>");
	EXPECT_STREQ("test", cfg.Get("", "").c_str());
}

TEST(KEPConfigAdapterTests, GetIntTest01) {
	KEPConfigAdapter cfg;
	cfg.Parse("<root>1</root>");
	EXPECT_EQ(1, cfg.Get("", 0));
}

TEST(KEPConfigAdapterTests, GetIntTest02) {
	KEPConfigAdapter cfg;
	cfg.Parse("<root><sub>1</sub></root>");
	EXPECT_EQ(1, cfg.Get("sub", 0));
}

TEST(KEPConfigAdapterTests, GetBoolTest01) {
	KEPConfigAdapter cfg;
	cfg.Parse("<root>1</root>");
	EXPECT_TRUE(cfg.Get("", false));
}

TEST(KEPConfigAdapterTests, GetBoolTest02) {
	KEPConfigAdapter cfg;
	cfg.Parse("<root><sub>1</sub></root>");
	EXPECT_TRUE(cfg.Get("sub", false));
}

// Some setter tests
TEST(KEPConfigAdapterTests, SetStringTest01) {
	KEPConfigAdapter cfg;
	cfg.Parse("<root><sub>1</sub></root>");
	EXPECT_STREQ("1", cfg.Get("sub", "1").c_str());

	cfg.SetValue("sub", "0");
	EXPECT_STREQ("0", cfg.Get("sub", "").c_str());
}

TEST(KEPConfigAdapterTests, SetStringTest02) {
	KEPConfigAdapter cfg("root");
	cfg.SetValue("sub", "0");
	EXPECT_STREQ("0", cfg.Get("sub", "").c_str());
}

TEST(KEPConfigAdapterTests, SetBoolTest01) {
	KEPConfigAdapter cfg;
	cfg.Parse("<root><sub>1</sub></root>");
	EXPECT_TRUE(cfg.Get("sub", false));

	cfg.SetValue("sub", false);
	EXPECT_FALSE(cfg.Get("sub", false));
}

/**
 * Per YC.  KepConfigAdapter.load() does not resolve logicals
 * This test replicates and, once fixed, verifies the fix.
  */

TEST(KEPConfigAdapterTests, LoadTest01) {
	TempFile tempFile(FileName::createTemp("xml", "KEPConfigAdapterTests_LoadTest01"));
	TIXMLConfig logicalsConfig;
	{
		stringstream ss;
		ss << "<database>"
		   << "<logicals name=\"metaconfig\" delims=\"%()\">"
		   << "<logical pattern=\"FULL_INVENTORY\">1</logical>"
		   << "</logicals>"
		   << "<FullInventory>%(FULL_INVENTORY)</FullInventory>"
		   << "</database>";
		logicalsConfig.parse(ss.str());
	}
	TIXMLConfigHelper(logicalsConfig).saveAs(tempFile.name().toString());

	KEPConfigAdapter config;
	config.Load(tempFile.name().toString().c_str(), "database");
	ASSERT_EQ(1, config.Get("FullInventory", 4));
}

// Test file ops
// Working out the mechanics of saving a file that has external
// references.
//
// Should not write the external data.
// future iterations can take flag to indicate that conflated
// data should be written to it's respective file and/or that it
// should be merged into the master file.
//
// Disabled to support general test execution.  I.e. cannot
// assume ability to write to root of any drive.  Furthermore
// doesn't actually test that the output file is correctly
// generated, making this more of a dev test
//
TEST(KEPConfigAdapterTests, DISABLED_SaveTest01) {
	KEPConfigAdapter cfg;

	try {
		stringstream ss;
		ss << "<root>"
		   << "<sub1>sub1</sub1>"
		   << "<ENTITY url=\"FILE://c:\\stage\\included.xml\" />"
		   << "<sub2>sub2</sub2>"
		   << "</root>";

		cfg.Parse(ss.str().c_str());
		cfg.SetValue("sub1", "modified");
		cfg.SaveAs("c:\\test.out");
	} catch (const ConfigurationException& exc) {
		FAIL() << exc.str();
	}
}

TEST(KEPConfigAdapterTests, SetDoubleTest) {
	KEPConfigAdapter cfg;

	try {
		stringstream ss;
		ss << "<root>"
		   << "<sub1>1.1</sub1>"
		   << "</root>";

		cfg.Parse(ss.str().c_str());
		cfg.SetValue("sub1", (double)2.2); // get the value
		cfg.SetValue("sub1", (double)3.3, true); // get the value
		EXPECT_EQ(cfg.Get("sub1", 4.4), 2.2); // use supplied default
	} catch (const ConfigurationException& exc) {
		FAIL() << exc.str();
	}
}

TEST(KEPConfigAdapterTests, GetIntTest) {
	KEPConfigAdapter cfg;

	try {
		stringstream ss;
		ss << "<root>"
		   << "<sub1>2</sub1>"
		   << "</root>";

		cfg.Parse(ss.str().c_str());
		EXPECT_EQ(cfg.Get("sub1", 1), 2); // get the value
		EXPECT_EQ(cfg.Get("sub2", 3), 3); // use supplied default
	} catch (const ConfigurationException& exc) {
		FAIL() << exc.str();
	}
}

TEST(KEPConfigAdapterTests, GetStringTest) {
	KEPConfigAdapter cfg;

	try {
		stringstream ss;
		ss << "<root>"
		   << "<sub1>dog</sub1>"
		   << "</root>";

		cfg.Parse(ss.str().c_str());
		EXPECT_STREQ(cfg.Get("sub1", "cat").c_str(), "dog"); // get the value
		EXPECT_STREQ(cfg.Get("sub2", "cat").c_str(), "cat"); // use supplied default
	} catch (const ConfigurationException& exc) {
		FAIL() << exc.str();
	}
}

TEST(KEPConfigAdapterTests, GetBoolTest) {
	KEPConfigAdapter cfg;

	try {
		stringstream ss;
		ss << "<root>"
		   << "<sub1>1</sub1>"
		   << "</root>";

		cfg.Parse(ss.str().c_str());
		EXPECT_TRUE(cfg.Get("sub1", false)); // get the value
		EXPECT_TRUE(cfg.Get("sub2", true)); // use supplied default
	} catch (const ConfigurationException& exc) {
		FAIL() << exc.str();
	}
}

TEST(KEPConfigAdapterTests, GetDoubleTest) {
	KEPConfigAdapter cfg;

	try {
		stringstream ss;
		ss << "<root>"
		   << "<sub1>2.2</sub1>"
		   << "</root>";

		cfg.Parse(ss.str().c_str());
		EXPECT_EQ(cfg.Get("sub1", 1.1), 2.2l); // get the value
		EXPECT_EQ(cfg.Get("sub2", 1.1), 1.1l); // use supplied default
		//		EXPECT_EQ( cfg.Get( "sub2" ), 0.0l );			// should use default default
	} catch (const ConfigurationException& exc) {
		FAIL() << exc.str();
	}
}

// DISABLED TESTS FOLLOW

// Discovery test.
// DISABLED
TEST(ConfigDiscovery, DISABLED_VisitorTest01) {
	stringstream ss;
	ss << "<root>"
	   << "<item1>"
	   << "<item11>"
	   << "</item11>"
	   << "<item12>"
	   << "</item12>"
	   << "<item13>"
	   << "</item13>"
	   << "</item1>"
	   << "<item2 />"
	   << "<item3 />"
	   << "</root>";
	TiXmlDocument config;
	config.Parse(ss.str().c_str());
	MyDiscoveryVisitor v;
	config.Accept(&v);
}

// Discovery test to investigate pathing in KEPConfig
// DISABLED
TEST(ConfigDiscovery, DISABLED_GetTest01) {
	KEPConfig cfg;
	cfg.Parse("<root>test</root>", "root");

	string s;
	EXPECT_FALSE(cfg.GetValue("root", s));
	EXPECT_TRUE(cfg.GetValue("", s));
	EXPECT_STREQ("test", s.c_str());
}

// one benefit of using the light weight wrapper is it allows us
// to simply wrap an existing implementation in our wrapper
// class, and free of cost, get the Config functionality.
// This is important for simplifying the migration path to the
// new config.  The following tests discover the limits of this.
// For exmple, one known limitation is that Config does maintain change
// state or manage persistence (read load/save file operations.
// This responsibilty is the domain of helper classes.  Hence,
//
//
TEST(ConfigDiscovery, DISABLED_ConfigIntegrationTest001) {
	try {
		KEPConfig kepconf;
		kepconf.Load("c:\\stage\\kep_prod.pv\\impltest.xml", "NetworkConfig");

		string fname = OS::makeTempFileName("xml", "KEPConfigAdapterTests_SaveTest01");
		TIXMLConfig logicalsConfig;
		{
			stringstream ss;
			ss << "<logicals name=\"test\" delims=\"%()\">"
			   << "<logical pattern=\"WOKHOST\">pv-wok.kaneva.com:8080</logical>"
			   << "<logical pattern=\"AUTHHOST\">pv-auth.kaneva.com</logical>"
			   << "</logicals>";

			logicalsConfig.parse(ss.str());
		}
		TIXMLConfigHelper(logicalsConfig).saveAs(fname);

		TIXMLConfig newconf(kepconf);
		cout << newconf.toString() << endl
			 << endl;
		cout << newconf.computeString() << endl
			 << endl;
	} catch (KEPException* pexc) {
		FAIL() << "Unexpected exception [" << pexc->ToString() << "]";
		delete pexc;
	} catch (ChainedException& exc) {
		FAIL() << "Unexpected exception [" << exc.str() << "]";
	}
}

class XMLBuilder {
public:
	stringstream _ss;
};

class XMLLogical {
public:
	XMLLogical(const std::string& pattern, const std::string& value) :
			_pattern(pattern), _value(value) {
	}

	string str() {
		return "<logical pattern=\"" + _pattern + "\">" + _value + "</logical>";
	}

	friend std::ostream& operator<<(std::ostream& os, const XMLLogical& obj) {
		return os << "<logical pattern=\"" << obj._pattern << "\">" << obj._value << "</logical>";
	}

	string _pattern;
	string _value;
};

class XMLLogicalSet {
public:
	XMLLogicalSet(const std::string& name, const std::string& delims) :
			_name(name), _delims(delims) {}

	XMLLogicalSet& addLogical(const XMLLogical& logical) {
		_logicals.push_back(logical);
		return *this;
	}

	std::string str() {
		stringstream ss;
		ss << *this;
		return ss.str();
	}
	friend std::ostream& operator<<(std::ostream& os, const XMLLogicalSet& set) {
		os << "<logicals name=\""
		   << set._name
		   << "\" delims=\""
		   << set._delims
		   << "\">";
		for (std::vector<XMLLogical>::const_iterator i = set._logicals.cbegin(); i != set._logicals.cend(); i++) {
			os << *i;
		}
		os << "</logicals>";
		return os;
	}
	std::vector<XMLLogical> _logicals;
	std::string _name;
	std::string _delims;
};

// <logicals name = "default" delims = "%()">
//<!--dev-->
//<logical pattern = "KANEVA_DEV_HOME">C:\dev< / logical>
//<logical pattern = "KD_CONTEXT">mainline< / logical>
//<logical pattern = "KD_PROJECT_ROOT">mainline< / logical>
//< / logicals>

class XMLFile {
public:
	XMLFile() {}

	std::string str() {
		stringstream ss;
		ss << *this;
		return ss.str();
	}

	friend ostream& operator<<(ostream& os, const XMLFile& obj) {
		return os << "<?xml version=\"1.0\" encoding=\"utf-8\" ? >" << obj._ss.str();
	}

	XMLFile& operator<<(const std::string& st) {
		_ss << st;
		return *this;
	}

	stringstream _ss;
};

typedef std::shared_ptr<TempFile> TempFilePtr;

TempFilePtr temporize(XMLLogicalSet& ls) {
	TempFilePtr tempFile(new TempFile(FileName::createTemp("xml", "TIXMLConfigTests_ConflationTest014")));

	TIXMLConfig logicalsConfig; // Create a configuration object
	{ // and populate it by parsing
		//XMLLogicalSet ls("default", "%()");
		//ls.addLogical(XMLLogical("KANEVA_DEV_HOME", "C:\\dev"))
		//	.addLogical(XMLLogical("KD_CONTEXT", "mainline"))
		//	.addLogical(XMLLogical("KD_PROJECT_ROOT", "mainline"));
		stringstream ss;
		ss << ls;
		logicalsConfig.parse(ss.str());
	}
	TIXMLConfigHelper(logicalsConfig).saveAs(tempFile->name().toString()); // Save this configuration object to
	return tempFile;
}

class XMLEntity {
public:
	XMLEntity(const FileName fname) :
			_fname(fname) {}

	std::string str() {
		stringstream ss;
		ss << *this;
		return ss.str();
	}

	friend ostream& operator<<(ostream& os, const XMLEntity& obj) {
		return os << "<ENTITY url=\"file://" << obj._fname.str() << "\"/>";
	}
	FileName _fname;
};

class TempFileCleaner {
public:
	TempFileCleaner(const std::string& extension = "", const std::string& prefix = "", const std::string& suffix = "") {
		std::remove(FileName::tempMask(extension, prefix, suffix).c_str());
	}
};

TEST(DiscoBuilderTests, TempFileMaskTest) {
	FileName temp = FileName::createTemp("tmp", "TempFileMaskTest");
	string mask = FileName::tempMask("tmp", "TempFileMaskTest");
	string stemp = temp.str();
	ASSERT_TRUE(true);
}

TEST(BuilderTests, XMLLogicalTest) {
	ASSERT_STREQ("<logical pattern=\"pattern\">value</logical>", XMLLogical("pattern", "value").str().c_str());
}

TEST(BuilderTests, XMLLogicalSetTest) {
	XMLLogicalSet slset("default", "%()");
	slset.addLogical(XMLLogical("pattern", "value"));
	ASSERT_STREQ("<logicals name=\"default\" delims=\"%()\"><logical pattern=\"pattern\">value</logical></logicals>", slset.str().c_str());
}

TEST(BuilderTests, ConflationTest04) {
	TempFilePtr devenv = temporize(XMLLogicalSet("default", "%()").addLogical(XMLLogical("PATTERN1", "value1")));
	TempFilePtr machine = temporize(XMLLogicalSet("default", "%()").addLogical(XMLLogical("PATTERN2", "value2")));

	XMLFile mainfile;
	mainfile << "<TestConfig>"
			 << XMLEntity(devenv->name()).str()
			 << XMLEntity(machine->name()).str()
			 << "<VALUE2>%(PATTERN2)</VALUE2>"
			 << "</TestConfig>";

	TIXMLConfig config;
	config.parse(mainfile.str());
	//	cout << endl << config.computeString() << endl;

	KEPConfigAdapter adapter(config);
	string ipAddress = adapter.Get("VALUE2", "");
	ASSERT_TRUE(ipAddress.length() != 0);
}

#if 0
// some quick exploration into the ABC's of ABCs
// The problem I'm trying trying to resolve is
// returning actual objects which are defined
// to be of an abstract class.
//
// This is becoming tedious.  The advantages offered
// by Reference pattern are real, and to my mind
// outweigh the value of a fully abstracted hierarchy
// when in fact, it is not likely that we are
// going to need to turn to a new xml parser
// (e.g. xerces)
// 
// Go back to working on the adapter class.
//
class ComponentRef;
typedef std::shared_ptr<Component> ComponentRef;


class Component {
public:
	virtual Component foo()  = 0;
};

template <class Impl>
class ComponentRef : public Component {
public:
	ComponentRef<Impl> foo() { return *this; }
private:
	Impl*	_impl;
};

template <class ComponentImpl>
class Collection {
public:
    virtual Collection&		add( ComponentRef<ComponentImpl> ni ) = 0;
	virtual ComponentRef<ComponentImpl>	get() = 0;
};



class ConcreteComponent : public Component {
public:
	ConcreteComponent() : Component()										{}
	ConcreteComponent( const ConcreteComponent& other ) : Component(other)	{}
	virtual ~ConcreteComponent()											{}

	virtual ConcreteComponent foo()											{ return *this; }
	ConcreteComponent specialization()										{ return *this; }
};

class ConcreteCollection : public Collection<ConcreteComponent> {
public:
	ConcreteCollection() {}	
	virtual ComponentRef<ConcreteComponent> get()		{ return ComponentRef<ConcreteComponent>();	}

	virtual ConcreteCollection& add(	ComponentRef<ConcreteComponent> component ) { return *this; }

	virtual ConcreteCollection& xadd( Component& component ) { 
		component.foo();
		// can't do this because we are reference in the context of it's interface
		//
		// component.specialization();
		// So how do we do this?  Why cast it.  but how?
		ConcreteComponent& concreteComponent = (ConcreteComponent&)component;
		concreteComponent.foo();
		concreteComponent.specialization();
		return *this; 
	}
};

//typedef CompenentRef<ConcreteCollection::Component> ConcreteComponentRef;

TEST( quicksuite, quicktest ) {
	ConcreteCollection collection;
//    collection.add( ConcreteComponentRef() );
//    ConcreteCollection::Component component;

	
}
#endif
