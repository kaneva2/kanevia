///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Common/include/kepconfig.h"
#include "Common/include/KEPException.h"
#include "Tools/NetworkV2/inetwork.h"

TEST(KEPConfigTests, DISABLED_NoContentTest) {
	int serverId = 0;
	string gender, birthDate, country;
	bool showMature = false;
	ULONG roleId = 0;
	int ret = -1;
	char* userInfoXML = "";
	KEPConfig cfg;
	const char* userName = 0; // DRF - potentially uninitialized variable
	try {
		cfg.Parse(userInfoXML, "user");
		serverId = cfg.Get("u", 0);
		gender = cfg.Get("g", "U");
		birthDate = cfg.Get("b", "1900-1-1");
		country = cfg.Get("c", "US");
		showMature = cfg.Get("m", false);
		roleId = cfg.Get("p", 0);
		if (gender.empty())
			gender = "U";
		if (birthDate.empty())
			birthDate = "1900-1-1";
	} catch (KEPException* e) {
		//		LOG4CPLUS_WARN( m_logger, "Error parsing userinfo for " << context->username << " " << e->ToString() );
		//		LOG4CPLUS_DEBUG( m_logger, " Userinfo " << userInfoXML );
		e->Delete();
		ret = LR_USER_INFO;
		goto VL_Exit;
	}

	userName = "set";
	//const char* userName = "set";    // <============= important\
    //const char* password = "set";    // <<<<<<<<<<<<<< When exception is thrown, the variables are not initialized
	//BYTE clientType = 55;

VL_Exit:

#if 0 // DRF - Senseless Code
	if ( ret == LR_OK )
	{
		int n = 1;
	}
       if ( serverId != 0 )
       {
           // <<<<<<<<<<<<< NOTE the usage of userName despite it never having been initialized.
           // VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
//           LOG4CPLUS_INFO( m_logger, "Cleaning up user before login complete. " << userName );
//           m_serverNetwork->LogoffCleanup( serverId, DISC_FORCE );
//           return ret;
		   int n = 1;
       }
       // usually more informative error message logged before this
//       if ( m_serverNetwork->GetPingerGameId() != 0 )
           // <<<<<<<<<<<<< NOTE the usage of userName despite it never having been initialized.
           // <<<<<<<<<<<<< NOTE this is where our log entry got generated             
           // VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
#endif
	assert(userName != 0); // DRF - potentially uninitialized variable
	LOG4CPLUS_WARN(Logger::getInstance(L"PlatformTest"), userName);
	//       else
	// <<<<<<<<<<<<< NOTE the usage of userName despite it never having been initialized.
	// VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
	//          LOG4CPLUS_DEBUG( m_logger, "Server hasn't pinged in yet.  User id not parsed. " << userName );
}
