///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/KEPUtil/WebCall.h"
#include "Core/Util/threadpool.h"

#include "common\include\NetworkCfg.h"

TEST(WebCallTests, DISABLED_Test01) {
	// Create Web Caller
	WebCaller caller("WebCallTests");

	// Do Web Call (blocking + response)
	WebCallOpt wco;
	wco.url = NET_KGP "/serverlist.aspx?id=5316&v=2";
	wco.response = true;
	WebCallActPtr p = caller.CallAndWait(wco);

	ASSERT_TRUE(p->IsOk());
	ASSERT_TRUE(p->ResponseStr().length() > 0);
}

class WebCallTestEndPoint01 : public HTTPServer::RequestHandler {
public:
	WebCallTestEndPoint01() :
			HTTPServer::RequestHandler("TestEndPoint01") {}
	virtual int handle(const HTTPServer::Request& /*request*/, HTTPServer::EndPoint& endPoint) {
		// Okay...we have two problems...
		// 1. Regardless of the media type (e.g. UTF-16, UTF-8, etc.), we always assume a wide charset.
		//    SEE http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.7
		// 2. GetBrowserPage assumes narrow string
		// The fix is to make sure that HTTP Server is capable of dispatching narrow charsets as well
		// so to that end parse the media-type component of the content type header and do a lookup
		// to determine the width of a character set.  And write it accordingly.
		//
		//		Sleep(MAXDWORD);
		//		endPoint <<  (HTTPServer::Response() << Header( "Content-Type", "text/html;charset=UTF-8" ) << "OK" ) ;

		///		endPoint <<  (HTTPServer::Response() << Header( "Content-Type", "text/html;charset=UTF-8" ) ) ;
		//		while( true ) {
		endPoint << (HTTPServer::Response() << "OK");
		//			Sleep(10000);
		//		}
		//		endPoint << "OK";
		return 1;
	}
};

class WebCallerTestClient : public KEP::Runnable {
	CountDownLatch& _latch;

public:
	WebCallerTestClient(CountDownLatch& latch) :
			_latch(latch), KEP::Runnable() {
	}

	KEP::RunResultPtr run() override {
		// Create Web Caller
		WebCaller caller("WebCallTests");

		// Do Web Call (blocking + response)
		WebCallOpt wco;
		wco.url = "http://localhost:8900/testendpoint";
		wco.response = true;
		wco.timeoutMs = MAXDWORD;

		WebCaller::WebCallAndWait("LockupTest", wco);

		//		WebCallActPtr p = caller.CallAndWait( wco );
		_latch.countDown();
		return RunResultPtr();
	}
};

TEST(WebCallerTests, DISABLED_LockupTest) {
	WebCallTestEndPoint01 ep02;
	MongooseHTTPServer AppHostController;
	AppHostController.addPortBinding(8900)
		.enableFileServing(false)
		.enableDirectories(false)
		.registerHandler(ep02, "/testendpoint")
		.start();

	std::string resultText;
	std::string httpStatusDescription;

	ConcreteThreadPool tp(10, std::string("WebCallTesters"));

	int numClients = 1;

	CountDownLatch latch(numClients);

	for (int n = 0; n < numClients; n++) {
		tp.enqueue(RunnablePtr(new WebCallerTestClient(latch)));
	}
	tp.start();
	latch.await();
}
