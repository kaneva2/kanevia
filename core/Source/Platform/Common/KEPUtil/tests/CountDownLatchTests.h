///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/Executor.h"
#include "Core/Util/ThreadPool.h"
#include "Common/KEPUtil/CountDownLatch.h"
#include "Common/KEPUtil/Counter.h"

//////////////////////
//  CDL was previously implemented using a mutex, which meant that
// we would deadlock with multiple waiters.  This tests the subsequent
// fix of implementing use Event.
//
class CDLDiscoRunnable1 : public KEP::Runnable {
public:
	CDLDiscoRunnable1(CountDownLatch* latch1, CountDownLatch* latch2) :
			_latch1(latch1), _latch2(latch2) {}

	RunResultPtr run() override {
		//        ::Sleep(10000);
		_latch1->await(); // wait for the subjects completion state to be set.
		_latch2->countDown(); // and notify external that we are done.
		return RunResultPtr();
	}

private:
	CDLDiscoRunnable1() :
			_latch1(0), _latch2(0) {}
	CDLDiscoRunnable1(const CDLDiscoRunnable1& /* other */) :
			_latch1(0), _latch2(0) {}
	CDLDiscoRunnable1& operator=(const CDLDiscoRunnable1& /*rhs*/) { return *this; }
	CountDownLatch* _latch1;
	CountDownLatch* _latch2;
};

TEST(CDLDiscoveryTests, DISABLED_USAGE_CDLDisco01) {
	jsThreadExecutor exec1("Executor31");
	exec1.start();
	jsThreadExecutor exec2("Executor32");
	exec2.start();

	for (int lcv1 = 0; lcv1 < 20; lcv1++) {
		try {
			CountDownLatch latch1(1);
			CountDownLatch latch2(2);

			CDLDiscoRunnable1* runnable1 = new CDLDiscoRunnable1(&latch1, &latch2);
			RunnablePtr runptr1(runnable1);
			ExecutablePtr executable1(new Executable(runptr1));

			CDLDiscoRunnable1* runnable2 = new CDLDiscoRunnable1(&latch1, &latch2);
			RunnablePtr runptr2(runnable2);
			ExecutablePtr executable2(new Executable(runptr2));

			exec1.execute(executable1);
			exec2.execute(executable2);

			//            Sleep(4000);
			latch1.countDown();
			if (!latch2.await(4000))
				FAIL();
		} catch (const ChainedException& e) {
			FAIL() << "UnexpectedException [" << e.str().c_str() << "]";
		}
	}
	exec1.stop();
	exec2.stop();
}

/**
 * Lauches multiple threads that block on a single CountDownLatch.  The main thread 
 * unblocks the threads by calling CountDownLatch::countDown()
 * Exhibits refcountptr race condition.
 */
TEST(CountDownLatchTests, CountDownLatchTest01) {
	class myRunnable : public KEP::Runnable {
	public:
		myRunnable(CountDownLatch& latch, int& counter) :
				_latch(latch), _counter(counter) {}

		RunResultPtr run() override {
			while (!_latch.await(1000))
				;
			_counter++;
			return RunResultPtr();
		}

		CountDownLatch& _latch;
		int& _counter;
	};

	int counter = 0;
	CountDownLatch latch;
	ConcreteThreadPool tp(1);

	tp.start();
	RunnablePtr runnable(new myRunnable(latch, counter));
	tp.enqueue(runnable);
	Sleep(10000);
	latch.countDown(); // created with countdown value of 1  this should release it.
	tp.shutdown()->wait();
	ASSERT_EQ(1, counter);
}

/**
 * Launches multiple threads and blocks on a single CountDownLatch.  Each thread, as it completes
 * execution, decrements the latch, thus allowing the main thread to continue after all
 * spawned threads have executed.
 */
TEST(CountDownLatchTests, SemaphoreTest02) {
	class myRunnable : public KEP::Runnable {
	public:
		myRunnable(CountDownLatch& latch, Counter<unsigned long>& counter) :
				_latch(latch), _counter(counter) {}

		RunResultPtr run() override {
			Sleep(1000);
			++_counter;
			_latch.countDown();
			return RunResultPtr();
		}
		CountDownLatch& _latch;
		Counter<unsigned long>& _counter;
	};

	CountDownLatch latch(30); // create our test subject.  A latch with a count of 30
	ConcreteThreadPool tp(5);
	tp.start();
	Counter<unsigned long> counter(0);
	for (int n = 0; n < 30; n++) {
		RunnablePtr runnable(new myRunnable(latch, counter));
		tp.enqueue(runnable);
	}
	if (!latch.await(30000))
		FAIL() << "Bad Timing";
	else {
		ASSERT_EQ(0, latch.count());
		ASSERT_EQ(30, counter.value());
	}
	tp.shutdown()->wait();
}