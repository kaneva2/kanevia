///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#if 0
TEST( ExecutorTest, ExecutorTest1 ) {

	class runtest : public KEP::Runnable {
	public:
		void run() {
			cout << "Run Test I am here.  Doing some really busy things that might take awhile!!!!!" << endl;
			Sleep(25);
			cout << "done.  Phew am I tired!!!!!" << endl;
		}
	};


	Executor executor("Test Executor");

	cout << "TEST: Starting exector" << endl;
	executor.start();

	for ( int n = 0; n < 1000; n++ ) {
		cout << "TEST: Queueing executable" << endl;
		KEP::ExecutablePtr ptr( new Executable( RunnablePtr( new runtest() ) ) );
		executor.execute(ptr);

		cout << "Acquiring results" << endl;
		ptr->act()->getResults();
	}

	executor.stop();
	executor.join();

	cout << "Results successfully acquired!" << endl;
	cout << "Ta" << endl;
}
#endif
