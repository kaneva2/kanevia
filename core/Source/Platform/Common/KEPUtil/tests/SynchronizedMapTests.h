///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/Runnable.h"
#include "Common/KEPUtil/SynchronizedMap.h"
#include "Common/KEPUtil/Sequence.h"
#include "Core/Util/ThreadPool.h"

typedef SynchronizedMap<unsigned long, unsigned long> TestMapT;

// Test basic operations.
//
TEST(SynchronizedMapTests, Test01) {
	TestMapT syncedMap;
	syncedMap.insert(std::pair<int, int>(0, 0));
	EXPECT_EQ(1, syncedMap.size()); // insert worked

	EXPECT_EQ(0, syncedMap.get(0)); // get worked
	EXPECT_THROW(syncedMap.get(1), KeyNotFoundException); // get failed

	EXPECT_EQ(0, syncedMap.erase(1)); // erase failed
	EXPECT_FALSE(syncedMap.empty()); // works when not empty

	EXPECT_EQ(1, syncedMap.erase(0)); // erase worked
	EXPECT_EQ(0, syncedMap.size()); // works
	EXPECT_TRUE(syncedMap.empty()); // works when empty

	syncedMap.insert(std::pair<int, int>(0, 0));
	EXPECT_FALSE(syncedMap.empty());

	syncedMap.clear();
	EXPECT_TRUE(syncedMap.empty());
}

class SMTR : public KEP::Runnable {
public:
	static const unsigned long Explicit = 0x00000001;
	static const unsigned long Dominate = 0x00000002;
	static const unsigned long Sleep = 0x00000004;
	static const unsigned long Cleanup = 0x00000008;
	static const unsigned long Announce = 0x80000000;

	std::string flagString(const std::string pre = "run") {
		stringstream ss;
		ss << pre << "(" << _key << ")"
		   << ((_mask & Cleanup) ? "+" : "-")
		   << ((_mask & Sleep) ? "+" : "-")
		   << ((_mask & Dominate) ? "+" : "-")
		   << ((_mask & Explicit) ? "+" : "-")
		   << ((masked(Sleep | Dominate | Explicit)) ? "long" : "")
		   << endl;
		return ss.str();
	}

	SMTR(unsigned long moves, unsigned long key, unsigned long mask, TestMapT& subject, const Sequence& seq = Sequence()) :
			_moves(moves), _key(key), _subject(subject), _seq(seq), _mask(mask) {
		if (_key == 0)
			throw ChainedException();
	}

	RunResultPtr run() override {
		srand((unsigned int)fTime::TimeMs());

		_subject.set(pair<int, int>(_key, _seq.nextVal()));
		SyncLock lock;
		if (masked(Explicit | Dominate))
			lock = _subject.lock();

		if (masked(Announce))
			cout << flagString();

		for (unsigned long n = 0; n < _moves; n++) {
			if (masked(Sleep))
				::Sleep((rand() % _key) + 1);

			if (!masked(Explicit)) {
				_subject.set(pair<int, int>(_key, _seq.nextVal()));
				continue;
			}

			if (!masked(Dominate))
				lock = _subject.lock();

			TestMapT::iterator i = _subject.find(_key);
			if (i == _subject.end())
				throw ChainedException();
			i->second = _seq.nextVal();
		}

		if (masked(Cleanup))
			_subject.erase(_key);

		if (masked(Announce))
			cout << flagString("end");

		return RunResultPtr();
	}
	bool masked(unsigned long mask) {
		unsigned long interim = _mask & mask;
		bool ret = (interim == mask);
		return ret;
	}
	unsigned long _mask;
	unsigned long _key;
	TestMapT& _subject;
	Sequence _seq;
	unsigned long _moves;
};

// Now test synchronization
//
TEST(SynchronizedMapTests, Test02) {
	unsigned long moves = 10;
	unsigned long players = 1000;
	unsigned long workers = 10;

	ConcreteThreadPool tp(workers);
	TestMapT subject;
	Sequence seq;

	for (unsigned long n = 1; n < players + 1; n++)
		tp.enqueue(RunnablePtr(new SMTR(moves, n, SMTR::Cleanup, subject)));

	tp.start();
	tp.shutdown(ThreadPool::patient)->wait();
	ASSERT_EQ(0, subject.size());
}

TEST(SynchronizedMapTests, Test03) {
	unsigned long moves = 10;
	unsigned long players = 100;
	unsigned long workers = 10;

	ConcreteThreadPool tp(workers);
	TestMapT subject;
	Sequence seq;

	for (unsigned long n = 1; n < players + 1; n++)
		tp.enqueue(RunnablePtr(new SMTR(moves, n, n, subject)));

	tp.start();
	tp.shutdown(ThreadPool::patient)->wait();

	{
		SyncLock lock = subject.lock();
		for (TestMapT::iterator i = subject.begin(); i != subject.end(); i++)

			ASSERT_EQ(moves, i->second);
		// We shouldn't need to explicitly unlocked as the lock is about to roll off the stack
		// anyway.  But it shouldn't matter if we do or don't.
		//
		lock.unlock();
	}
}