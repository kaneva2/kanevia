///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common/keputil/ExceptionHelper.h"

TEST(ExceptionHelpertests, DISABLED_NOTATEST_ReportFatalExceptionTest) {
	ChainedException e(SOURCELOCATION, "Test");

	// First exercise ChainedException's ability to report.
	//
	// Report the exception to stderr
	//
	e.report();

	// Report the exception to stdout
	//
	e.report(cout);

	// Reports the exception to stderr
	//
	e.report(cerr);

	// Report the exception to the PlatformTest logger at fatal level and to
	// stderr
	//
	ExceptionHelper::ReportFatalException(ChainedException(SOURCELOCATION, "Testing"), Logger::getInstance(L"PlatformTest"));

	// Report the exception to the PlatformTest logger at error level and to
	// stderr
	//
	ExceptionHelper::ReportErrorException(ChainedException(SOURCELOCATION, "Testing"), Logger::getInstance(L"PlatformTest"));

	// Report the exception to the PlatformTest logger at warning level and to
	// stderr
	//
	ExceptionHelper::ReportWarningException(ChainedException(SOURCELOCATION, "Testing"), Logger::getInstance(L"PlatformTest"));

	// Report the exception to the PlatformTest logger at fatal level and to
	// stdout
	//
	ExceptionHelper::ReportFatalException(ChainedException(SOURCELOCATION, "Testing"), Logger::getInstance(L"PlatformTest"), cout);

	// Report the exception to the PlatformTest logger at error level and to
	// stdout
	//
	ExceptionHelper::ReportErrorException(ChainedException(SOURCELOCATION, "Testing"), Logger::getInstance(L"PlatformTest"), cout);

	// Report the exception to the PlatformTest logger at Warning level and to
	// stdout
	//
	ExceptionHelper::ReportWarningException(ChainedException(SOURCELOCATION, "Testing"), Logger::getInstance(L"PlatformTest"), cout);
}
