///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Common/KEPUtil/LocationRegistry.h"

TEST(LocationRegistryTest, Test01) {
#if 1
	LocationRegistry locationRegistry;

	pair<Vector3f, Vector3f> coords1(Vector3f(1, 2, 3), Vector3f(4, 5, 6));

	locationRegistry.registerPlayerLocation(7, 8, coords1);
	pair<Vector3f, Vector3f> coords2 = locationRegistry.getPlayerLocation(7);

	ASSERT_TRUE(coords1 == coords2);

	locationRegistry.unregister(7);
	try {
		pair<Vector3f, Vector3f> coords3 = locationRegistry.getPlayerLocation(7);
		FAIL() << "Failed to remove entry";
	} catch (KeyNotFoundException) {
	} catch (...) {
		FAIL() << "Unexpected exception raised!";
	}

#endif
}