///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Lease.h"
using namespace Leasing;
using namespace log4cplus;

static Logger LeasingLog = Logger::getInstance(L"Leasing");

class NullResource : public Resource {
public:
	NullResource() :
			Resource() {}
	virtual ~NullResource() {}
};
Resource* Resource::_null = new NullResource();
Resource& Resource::null() {
	return *_null;
}

class NullLessee : public Lessee {
public:
	NullLessee() :
			Lessee() {}
	virtual ~NullLessee() {}
	virtual void leaseInvalidated(Lease& lease) {}
};
Lessee* Lessee::_null = new NullLessee();
Lessee& Lessee::null() {
	return *_null;
}

class NullLessor : public Lessor {
public:
	virtual void leaseInvalidated(Lease& lease) {}

	virtual Lease& lease(Lessee& lessee, LeaseRequest& request, LeaseTerms& terms) { return Lease::null(); }
};
Lessor* Lessor::_null = new NullLessor();
Lessor& Lessor::null() {
	return *_null;
}

class NullLeaseTerms : public LeaseTerms {
public:
	NullLeaseTerms() {}
	virtual ~NullLeaseTerms() {}
	virtual boolean renew() { return false; }
	virtual boolean expired() { return true; }
};

LeaseTerms* LeaseTerms::_null = new NullLeaseTerms();
LeaseTerms& LeaseTerms::null() {
	return *_null;
}

class NullLease : public Lease {
public:
	NullLease() :
			Lease(Lessee::null(), Resource::null(), Lessor::null(), LeaseTerms::null()) {}
	virtual ~NullLease() {}
};

Lease* Lease::_null = new NullLease();
Lease& Lease::null() {
	return *_null;
}

class NullLeaseRequest : public LeaseRequest {
public:
	NullLeaseRequest() {}
	virtual ~NullLeaseRequest() {}
};

LeaseRequest*
	LeaseRequest::_null = new NullLeaseRequest();

LeaseRequest&
LeaseRequest::null() {
	return *_null;
}

LeaseTerms::LeaseTerms() {}
LeaseTerms::~LeaseTerms() {}

Logger
	Lease::LOG = Logger::getInstance(L"Lease");

int
	Lease::seq = 1;

// For now, the expiry of a lease is based solely on elapsed time.  We may in the future
// allow for an expiry policy to be supplied so that the expiry policy can be developed
// independant of the lease itself.
//

// IS THIS EVEN OF VALUE?
// Object      userObject = null;
//public void setUserObject( Object userObject )  { this.userObject = userObject; }
//public Object getUserObject()   { return userObject; }
Lease::Lease(Lessee& lessee, Resource& resource, Lessor& lessor, LeaseTerms& terms) :
		_lessee(lessee), _lessor(lessor), _resource(resource), _terms(terms), _isValid(true), _leaseId(seq++) {}

/**
 * Invalidate this lease.
 */
void Lease::invalidate() {
	std::lock_guard<KEP::fast_recursive_mutex> lock(_self);
	if (!_isValid)
		return;
	_isValid = false;

	LOG4CPLUS_DEBUG(LeasingLog, "Invalidating lease [" << id() << "]");
	_lessor.leaseInvalidated(*this);
	_lessee.leaseInvalidated(*this);
}

boolean
Lease::isValid() {
	return _isValid;
}

/**
 * Obtain a reference to the Lessor of this lease.
 * @return a reference to the Lessor of this lease.
 */
Lessor&
Lease::lessor() {
	return _lessor;
}

Lessee&
Lease::lessee() {
	return _lessee;
}

/**
 * Determine the duration, or in loan industry speak, the terms, of the lease.
 * @return The terms of the lease.
 * :todo: need to abstract the concept of a lease terms.  Because this need not always be
 * :todo: based on time.   Until need for this identified.  Remove from this interface.
 */
LeaseTerms&
Lease::terms() {
	return _terms;
}

/**
  * Obtain a reference to the object being leased.  i.e. The Resource
  * @return reference to the object being leased.
  */
Resource&
Lease::resource() {
	return _resource;
}

/**
 * Obtain the unique id of this lease..
 * @return leaseId
 */
int Lease::id() {
	return _leaseId;
}

DurationLeaseTerms::DurationLeaseTerms(TimeMs term) :
		_term(term), _expires(term) {}

DurationLeaseTerms::~DurationLeaseTerms() {}

/**
 * Request a renewal of the lease.
 * @return true if the renewal succeeded...i.e the lease and the leased object are still valid.  Else  false.
 */
boolean DurationLeaseTerms::renew() {
	_expires.setDelayTime(_term);
	return true;
}

/**
 * Determine if this lease is expired.
 *
 * @return true if this lease has expired, else false.
 */
boolean DurationLeaseTerms::expired() {
	boolean ret = _expires.elapsed();
	LOG4CPLUS_DEBUG(LeasingLog, "expired() returns " << ret);
	return ret;
}

Lease& BasicLessor::lease(Lessee& lessee, LeaseRequest& request, LeaseTerms& terms) {
	return Lease::null();
}
void BasicLessor::leaseInvalidated(Lease& lease) {}

void LeasingAgent::invalidate() {
	LOG4CPLUS_TRACE(LeasingLog, "Recouping leases [" << _leases.size() << "]");
	for (LeaseMapT::iterator iter = _leases.begin(); iter != _leases.end(); iter++) {
		try {
			LOG4CPLUS_DEBUG(LeasingLog, "invalidating lease [" << iter->first << "]");
			iter->second.invalidate();
			_leases.erase(iter);
		} catch (std::exception& /*t*/) {
			// Would be nice if kepexception supported nesting of exceptions.
			LOG4CPLUS_ERROR(LeasingLog, "ExceptionRaised while invalidating lease [" << iter->first << "]");
		}
	}
}

unsigned int
LeasingAgent::leaseCount() {
	return _leases.size();
}

void LeasingAgent::recoup() {
	LOG4CPLUS_TRACE(LeasingLog, "Recouping leases [" << _leases.size() << "]");
	for (LeaseMapT::iterator iter = _leases.begin(); iter != _leases.end(); iter++) {
		if (!iter->second.terms().expired()) {
			LOG4CPLUS_DEBUG(LeasingLog, "validated lease [" << iter->first << "]");
			continue;
		}
		try {
			LOG4CPLUS_DEBUG(LeasingLog, "invalidating lease [" << iter->first << "]");
			iter->second.invalidate();
			_leases.erase(iter);
		} catch (std::exception& /*t*/) {
			LOG4CPLUS_ERROR(LeasingLog, "ExceptionRaised while invalidating lease [" << iter->first << "]");
		}
	}
}

// LeasingAgent
//
LeasingAgent::LeasingAgent() :
		_leaseFactory(new DefaultLeaseFactory()) {}

LeasingAgent::LeasingAgent(LeaseFactory& leaseFactory) :
		_leaseFactory(&leaseFactory) {}

// :todo:  Add abstraction here like "LeaseFactory" that allows us to abstract away the actual creation
// of the lease object.
//
// How do we handle leases on implicit connections...answer LeaseOffer
//
Lease&
LeasingAgent::finalizeLease(Lessee& lessee, Resource& resource, Lessor& lessor, LeaseTerms& terms) {
	LOG4CPLUS_TRACE(LeasingLog, "LeasingAgent::finalizeLease()");
	Lease& lease = _leaseFactory->newLease(lessee, resource, lessor, terms);

	LOG4CPLUS_DEBUG(LeasingLog, "Posting service lease!");
	_leases.insert(std::pair<int, Lease&>(lease.id(), lease));

	LOG4CPLUS_DEBUG(LeasingLog, "Finalized lease [" << lease.id() << "]");
	return lease;
}
