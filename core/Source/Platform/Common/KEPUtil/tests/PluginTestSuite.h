///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "common/keputil/Plugin.h"
#include "common/keputil/PluginHostConfigurator.h"
#include "common/keputil/MongooseHTTPServer.h"
#include "common/keputil/algorithms.h"

class TestPlugin : public AbstractPlugin {
public:
	TestPlugin() :
			AbstractPlugin("TestPlugin") {}
	virtual ~TestPlugin() {}
	virtual Plugin& setUp() { return AbstractPlugin::setUp(); }
	virtual const std::string& name() const { return AbstractPlugin::name(); }
	virtual unsigned int version() { return 0; }
};

// A plugin library is a collection of PluginFactoryPtr that are used to
// instantiate new instances of Plugin.  The class below, implements the
// PluginFactory interface with some help from AbstractPluginFactory.
//
class TestPluginFactory : public AbstractPluginFactory {
public:
	TestPluginFactory() :
			AbstractPluginFactory("TestPlugin") {}
	const std::string& name() const { return AbstractPluginFactory::name(); }
	virtual PluginPtr createPlugin(const TIXMLConfig::Item& /*config*/) const { return PluginPtr(new TestPlugin()); }
};

// There was a problem instantiating a PluginException.
// Had issue due to the fact that <errno.h> was being included
// which defined a macro named, "errno".  That was costly.
//
//
TEST(PluginTestSuite, PluginExceptionTest) {
	PluginException pe(SOURCELOCATION, "test");
}

/**
 * Verify exception handling when we attempt to load a plugin library that doesn't exist.
 */
TEST(PluginTestSuite, MissingDynamicLibaryFileTest) {
	ASSERT_THROW(PluginLibrary().load("c:\\dev\\kep_prod\\source\\platform\\debug\\Provisionerxxx.dll"), PluginException);
}

/**
 * Verify exception handling when we attempt to load a plugin library that does export the
 * enumeratePlugins() entry point
 */
TEST(PluginTestSuite, InvalidDynamicLibaryFileTest) {
	ASSERT_THROW(PluginLibrary().load("lua5.1.dll"), PluginException);
}

TEST(PluginTestSuite, DISABLED_FIXME_DynamicLibaryFileTest01) {
	PluginLibrary plib1;

	ASSERT_NO_THROW(plib1.load("CorePluginLibrary.dll"));

	PluginFactoryPtr pfp(plib1.getPluginFactory("HTTPServer"));
	EXPECT_TRUE((bool)pfp);

	// Create an instance of the plugin and exercise it's interface
	//
	PluginPtr pp = pfp->createPlugin();
	EXPECT_EQ(0, pp->version());
}

/**
 * Run through the missing/invalid/valid sequence manifested by the
 * tests above against the same Library object to verify that
 * reloading DynamicLibrary is reentrant.
 */
TEST(PluginTestSuite, DISABLED_FIXME_DynamicLibaryReentrantLoadTest) {
	PluginLibrary plib1;
	ASSERT_THROW(plib1.load("asdfaeadf3414wrtw.dll"), PluginException);
	ASSERT_THROW(plib1.load("lua5.1.dll"), PluginException);
	ASSERT_NO_THROW(plib1.load("CorePluginLibrary.dll"));

	PluginFactoryPtr pfp(plib1.getPluginFactory("HTTPServer"));
	ASSERT_TRUE((bool)pfp);

	// Create an instance of the plugin and exercise it's interface
	//
	PluginPtr pp = pfp->createPlugin();
	ASSERT_EQ(0, pp->version());
}

// In this test, we create two libraries one dynamic, and one static.
// The next test will be to actually execute both of the plugins
// DISABLED until we have a dynamic plugin lib that we know will
// be available
TEST(PluginTestSuite, DISABLED_PluginTest00) {
	LOG4CPLUS_INFO(Logger::getInstance(L"Testing"), "***************** PluginTest00 ***************");
	LOG4CPLUS_INFO(Logger::getInstance(L"Testing"), "***************** PluginTest00 ***************");
	LOG4CPLUS_INFO(Logger::getInstance(L"Testing"), "***************** PluginTest00 ***************");

	// Here the hierarchy of the librarian is exemplified
	// Library is collection of PluginFactories
	// Domain is a collection of libraries
	//

	// Load an external/dynamic library
	//
	PluginLibraryPtr dynLib(new PluginLibrary());

	try {
		dynLib->load("CorePluginLibrary.dll");
	} catch (const PluginException& /*pe*/) {
		FAIL() << "Failed to open library";
	} // Call to load() should succeed
	// Fabricate a static library
	//
	//	PluginLibraryPtr			staticLib(new PluginLibrary());
	//	staticLib->addPluginFactory( PluginFactoryPtr(new TestPluginFactory()) )
	//		      .addPluginFactory( PluginFactoryPtr(new MongooseHTTPPluginFactory()) );

	PluginHost pd; // A domain of available plugins contained in zero or more libraries
	pd.addPluginLibrary(dynLib);
	//      .addPluginLibrary( staticLib );

	// Test should be that we can query all factories of interest.
	// Current implementation provides no dependency resolution.
	// Rather Plugin Host requires the user to sequence the startup
	//
	// So plugins should be brought up explicitly in serial fashion
	// such that prerequisites are resolved.
	//
	try {
		pd
			//		.mountPlugin( "Provisioner"	)
			//	  .setUpPlugin( "Provisioner"	)
			//	  .startPlugin( "Provisioner"	)
			.mountPlugin("HTTPServer")
			.setUpPlugin("HTTPServer")
			.startPlugin("HTTPServer");
	} catch (const PluginException& /*  pe */) {
		FAIL();
	}

	pd.stopPlugin("HTTPServer")->wait();
	pd.tearDownPlugin("HTTPServer")
		.unmountPlugin("HTTPServer");
	//	  .stopPlugin( "Provisioner" )
	//	  .tearDownPlugin( "Provisioner" )
	//	  .unmountPlugin( "Provisioner" );
}

TEST(PluginTestSuite, PluginTest02) {
	TestPlugin tpi;
	TestPluginFactory* tf = new TestPluginFactory();

	PluginLibrary plib;
	plib.addPluginFactory(PluginFactoryPtr(tf));
	PluginFactoryPtr pf = plib.getPluginFactory(std::string("TestPlugin"));
	EXPECT_STREQ("TestPlugin", pf->name().c_str());
	plib.removePluginFactory(std::string("TestPlugin"));
	pf = plib.getPluginFactory(std::string("TestPlugin"));
	EXPECT_TRUE(!pf);
}

TEST(PluginTestSuite, CrashTest01) {
	LOG4CPLUS_INFO(Logger::getInstance(L"Testing"), "************ CrashTest01");
	std::stringstream ss;

	ss << "<KGPController>"
	   << "<PluginHost>"
	   << "<Libraries>"
	   << "<Library Path=\"c:\\dev\\kep_prod\\source\\platform\\debug\\Provisioner.dll\" />"
	   << "</Libraries>"
	   << "<Plugins>"
	   << "<Plugin FactoryName=\"HTTPServer\" InstanceName=\"HTTPServer\" Enabled=\"true\">"
	   << "<PluginConfig Port=\"8080\" ThreadCount=\"5\" />"
	   << "</Plugin>"
	   << "<Plugin FactoryName=\"Provisioner\" InstanceName=\"Provisioner\" Enabled=\"true\"/>"
	   << "</Plugins>"
	   << "</PluginHost>"
	   << "</KGPController>";

	KEPConfig conf;
	conf.Parse(ss.str().c_str(), "KGPController");

	PluginHost host;
	PluginLibraryPtr staticLib(new PluginLibrary("Static")); // This guy goes off the stack first, then we clean it up explicitly after
	staticLib->addPluginFactory(PluginFactoryPtr(new MongooseHTTPPluginFactory()));
	host.addPluginLibrary(staticLib);
	PluginHostConfigurator().configure(host, conf);
	host.shutdown();
}

TEST(PluginHostConfigTests, Test01) {
	PluginHostConfiguration config;
	config.addPluginLibrary("CorePluginLibrary.dll")
		.addPluginLibrary("IncubatorPlugins.dll")
		.addPluginConfig(PluginConfig("HTTPServer") << "<binding port=\"8081\"/>")
		.addPluginConfig(PluginConfig("RESTClient"))
		.addPluginConfig(PluginConfig("Notifier")
						 << "<remote_notification>"
						 << "<receiving>0</receiving>"
						 << "<forwarding>server:8080</forwarding>"
						 << "</remote_notification>")
		.addPluginConfig(PluginConfig("AppController").addAttribute("local_port", "8081")
						 << "<kgpserver>"
						 << "<path>C:\\Program Files (x86)\\Kaneva\\Kaneva Platform\\bin</path>"
						 << "<binary>KGPServer.exe</binary>"
						 << "<stopping_timeout>30000</stopping_timeout>"
						 << "</kgpserver>"
						 << "<request_processor_threads>4</request_processor_threads>"
						 << "<reporting_interval>15000</reporting_interval>")
		.addPluginConfig(PluginConfig("Mailer ").addAttribute("enabled", "1")
						 << "<defaults mail_host=\"mail.kanevia.com\" to=\"recipient@kanevia.com\" from=\"mailer@kanevia.com\" />")
		.addPluginConfig(PluginConfig("WOKProvisioner").addAttribute("remote", "1").addAttribute("remote_host", "server:8081"));
	string s = config.toXML();
	ASSERT_STREQ("<?xml version=\"1.0\" encoding=\"utf-8\" ?><Config><PluginHost><Libraries><Library path=\"CorePluginLibrary.dll\" /><Library path=\"IncubatorPlugins.dll\" /></Libraries><Plugins><Plugin factory_name=\"HTTPServer\"><PluginConfig><binding port=\"8081\"/></PluginConfig></Plugin><Plugin factory_name=\"RESTClient\"><PluginConfig/></Plugin><Plugin factory_name=\"Notifier\"><PluginConfig><remote_notification><receiving>0</receiving><forwarding>server:8080</forwarding></remote_notification></PluginConfig></Plugin><Plugin factory_name=\"AppController\"><PluginConfig local_port=\"8081\"><kgpserver><path>C:\\Program Files (x86)\\Kaneva\\Kaneva Platform\\bin</path><binary>KGPServer.exe</binary><stopping_timeout>30000</stopping_timeout></kgpserver><request_processor_threads>4</request_processor_threads><reporting_interval>15000</reporting_interval></PluginConfig></Plugin><Plugin factory_name=\"Mailer \"><PluginConfig enabled=\"1\"><defaults mail_host=\"mail.kanevia.com\" to=\"recipient@kanevia.com\" from=\"mailer@kanevia.com\" /></PluginConfig></Plugin><Plugin factory_name=\"WOKProvisioner\"><PluginConfig remote=\"1\" remote_host=\"server:8081\"/></Plugin></Plugins></PluginHost></Config>", s.c_str());

	// Or, when formatted, toXML() yields:
	//
	// <?xml version="1.0" encoding="utf-8" ?>
	// <Config>
	//    <PluginHost>
	//        <Libraries>
	//            <Library path="CorePluginLibrary.dll" />
	//            <Library path="IncubatorPlugins.dll" />
	//        </Libraries>
	//        <Plugins>
	//            <Plugin factory_name="HTTPServer">
	//                <PluginConfig>
	//                    <binding port="8081"/>
	//                </PluginConfig>
	//            </Plugin>
	//            <Plugin factory_name="RESTClient">
	//                <PluginConfig/>
	//            </Plugin>
	//            <Plugin factory_name="Notifier">
	//                <PluginConfig>
	//                    <remote_notification>
	//                        <receiving>0</receiving>
	//                        <forwarding>server:8080</forwarding>
	//                    </remote_notification>
	//                </PluginConfig>
	//            </Plugin>
	//            <Plugin factory_name="AppController">
	//                <PluginConfig local_port="8081">
	//                    <kgpserver>
	//                        <path>C:\Program Files (x86)\Kaneva\Kaneva Platform\bin</path>
	//                        <binary>KGPServer.exe</binary>
	//                        <stopping_timeout>30000</stopping_timeout>
	//                    </kgpserver>
	//                    <request_processor_threads>4</request_processor_threads>
	//                    <reporting_interval>15000</reporting_interval>
	//                </PluginConfig>
	//            </Plugin>
	//            <Plugin factory_name="Mailer ">
	//                <PluginConfig enabled="1">
	//                    <defaults mail_host="mail.kanevia.com" to="recipient@kanevia.com" from="mailer@kanevia.com" />
	//                </PluginConfig>
	//            </Plugin>
	//            <Plugin factory_name="WOKProvisioner">
	//                <PluginConfig remote="1" remote_host="server:8081"/>
	//            </Plugin>
	//        </Plugins>
	//    </PluginHost>
	// </Config>
}

// You will need to implement your own runnable
//
// In your case, your runnable will like need to hold a
// db connector
class ThreadPoolDemoRunnable : public KEP::Runnable {
public:
	ThreadPoolDemoRunnable() {}

	virtual ~ThreadPoolDemoRunnable() {}

	RunResultPtr run() override {
		cout << "Hello world!" << endl;

		// And right here is where you'd execute your sql.
		//
		// In truth, the plan is to move all sql access behind a model
		// layer.  When this happens, you'd simply call that model
		// directly from your handler.  It would be incumbent on
		// the model to run this Runnable instance.
		// This is a decent middle step.
		//
		return RunResultPtr();
	}
};

TEST(PluginTestSuite, ThreadpoolDemo) {
	std::stringstream ss;
	ss << "<KGPServer>"
	   << "<PluginHost>"
	   << "<Libraries>"
	   << "<Library path=\"CorePluginLibrary.dll\" />"
	   << "<Library path=\"KEPPluginLibrary.dll\" />"
	   << "</Libraries>"
	   << "<Plugins>"
	   << "<Plugin factory_name=\"ThreadPool\">"
	   << "<PluginConfig name=\"GeneralUse\" size=\"2\" priority=\"3\"/>"
	   << "</Plugin>"
	   << "</Plugins>"
	   << "</PluginHost>"
	   << "</KGPServer>";

	try {
		KEPConfig conf;
		conf.Parse(ss.str().c_str(), "KGPServer");

		// Typically you would acquire the well-known host singleton
		// and hence would not need to see any of the following set
		// code.
		//
		// e.g.:
		// PluginHost& host = PluginHost::singleton();
		// But for this demonstration, we will put one on the stack
		// and start it up.
		PluginHost host;
		PluginHostConfigurator().configure(host, conf);

		// In a running server, this line should be all it takes to put something
		// in the thread pool.
		//
		host.getInterface<ThreadPool>("ThreadPool")->enqueue(RunnablePtr(new ThreadPoolDemoRunnable()));

		// Nor would need to see this.
		//
		host.shutdown()->wait();
	} catch (const KEPException* e) {
		FAIL() << e->ToString().c_str();
	} catch (const ChainedException& e) {
		FAIL() << e.str().c_str();
	} catch (...) {
		FAIL() << "Unknown exception raised";
	}
}

