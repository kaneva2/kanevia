#pragma once

#include "Common/KEPUtil/PluginHostConfigurator.h"

class PluginHostTests : public ::testing::Test {
public:
	typedef pair<std::string, PluginHost> HostSetupPair;
	typedef std::map<string, HostSetupPair> HostMapT;

	PluginHostTests() :
			::testing::Test() {}

	virtual void SetUp() {
		try {
			initializeAll();
		} catch (const ChainedException& ce) {
			FAIL() << "Exception raised while configuring tests" << endl
				   << ce.str() << endl;
		}
	}

	virtual void TearDown() {
		for (HostMapT::iterator iter = hosts.begin(); iter != hosts.end(); iter++) {
			iter->second.second.shutdown();
		}
		hosts.clear();
	}

	void addConfig(const std::string& name, const std::string& xml) {
		hosts.insert(pair<string, HostSetupPair>(name, pair<string, PluginHost>(xml, PluginHost())));
	}

	PluginHost* getPluginHost(const std::string& hostName) {
		HostMapT::iterator i = hosts.find(hostName);
		if (i == hosts.end())
			return (PluginHost*)0;
		return &(i->second.second);
	}

	void initializeAll() {
		for (HostMapT::iterator iter = hosts.begin(); iter != hosts.end(); iter++) {
			HostSetupPair& p = iter->second;

			std::string& configString = p.first;
			PluginHost& host = p.second;

			TIXMLConfig config;
			config.parse(configString);
			PluginHostConfigurator().configure(host, config);
		}
	}

	HostMapT hosts;
};
