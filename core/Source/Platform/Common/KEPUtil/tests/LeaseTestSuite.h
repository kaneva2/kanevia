///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <map>
#include "Core/Util/Runnable.h"
#include "Core/Util/ChainedException.h"
#include "Core/Util/HighPrecisionTime.h"
#include "lease.h"
#include "common/include/kepexception.h"
#include "Core/Util/ThreadPool.h"
#include "Test/testhelpers.h"

/**
 * Okay, back to the our attempt at a generalized solution to resource leasing as a
 * complement to LifeCycle management.
 */
template <typename PooledT>
class Pool {
public:
	Pool() {}
	virtual ~Pool() {}

	virtual PooledT checkout() = 0;
	virtual void checkin(PooledT value) = 0;
};

/**
 * No guarantee of order or contiguity.
 */
template <typename OrdinalT>
class RangedPool : public Pool<OrdinalT> {
public:
	RangedPool(OrdinalT rangeFloor, OrdinalT rangeCeiling) :
			_floor(rangeFloor), _next(rangeFloor), _ceiling(rangeCeiling) {}

	OrdinalT checkout() {
		OrdinalT ret = 0;

		// First determine if an allocated port is available
		//
		if (_next <= _ceiling) {
			ret = _next++;
			_inUse.insert(ret);
			return ret;
		}
		if (_available.size() > 0) {
			ret = _available.back();
			_available.pop_back();
			_inUse.insert(ret);
			return ret;
		}
		// throw an exception
		throw RangeException(SOURCELOCATION, ErrorSpec(1, "Error in test"));
	}

	void checkin(OrdinalT ordinal) {
		_inUse.erase(_inUse.find(ordinal));
		_available.push_back(ordinal);
	}

	virtual ~RangedPool() {
	}

private:
	typedef vector<unsigned short> PortVecT;
	typedef set<unsigned short> PortMapT;
	unsigned short _next;
	unsigned short _ceiling;
	unsigned short _floor;

	PortVecT _available;
	PortMapT _inUse;
};

/**
 * Simple test traversal of a ranged pool of unsigned shorts
 */
TEST(LeaseTestSuite, RangedPoolTest00) {
	// Create a ranged pool of unsigned shorts with a minimum value of  100 and a size of 100
	// In this context, I see that referring to range as floor/ceil as opposed to min/size
	// makes more sense.
	//
	unsigned short floor = 100;
	unsigned short ceil = 105;

	RangedPool<unsigned short> rp(100, ceil);
	for (unsigned short n = floor; n <= ceil; n++)
		EXPECT_EQ(n, rp.checkout());
}

/**
 * Verify that value of 0 is returned when the pool is exhausted.
 * This actually isn't very good stuff, as 0 could concievably be
 * in a range, hence an exception is really more appropriate here.
 */
TEST(LeaseTestSuite, RangedPoolTest01) {
	try {
		RangedPool<unsigned short> rp(1, 3);
		for (unsigned short n = 0; n < 5; n++)
			rp.checkout();
		FAIL() << "Pool failure never occurred";
	} catch (const RangeException& /*pe*/) {
	}
}

/**
 * Same as test00, but range includes 0
 */
TEST(LeaseTestSuite, RangedPoolTest02) {
	short floor = -100;
	short ceil = 100;

	RangedPool<unsigned short> rp(100, ceil);
	for (unsigned short n = floor; n <= ceil; n++)
		EXPECT_EQ(n, rp.checkout());
}

/**
 * Test to verify that checkin works.  We should be able to set up a sceneario
 * where we can perform infinite checkouts by checking things back in. But we
 * need only run the test of multiple iterations.
 */
TEST(LeaseTestSuite, RangedPoolTest03) {
	RangedPool<unsigned short> rp(100, 200);

	//unsigned short floor = 100;
	//unsigned short ceiling = 199;

	vector<unsigned short> vec;

	try {
		for (int iteration = 0; iteration < 40; iteration++) {
			// Need to code a loop
			for (int ncheckout = 0; ncheckout < 25; ncheckout++) {
				vec.push_back(rp.checkout());
				if (vec.size() > 20) {
					vec.front();
					rp.checkin(vec.front());
					vec.erase(vec.begin());
				}
			}
		}
	} catch (const RangeException pe) {
		FAIL() << "Range exception";
	}
	//	int nn=1;
}

/**
 * Same as RangedPool, but thread safe.
 */
template <typename OrdinalT>
class ConcurrentNumberPool : public Pool<OrdinalT> {
public:
	ConcurrentNumberPool(OrdinalT floor, OrdinalT ceiling) :
			_pool(floor, ceiling) {}

	virtual ~ConcurrentNumberPool() {}

	OrdinalT checkout() {
		std::lock_guard<fast_recursive_mutex> lock(_self);
		return _pool.checkout();
	}

	void checkin(OrdinalT val) {
		std::lock_guard<fast_recursive_mutex> lock(_self);
		_pool.checkin(val);
	}

private:
	fast_recursive_mutex _self;
	RangedPool<OrdinalT> _pool;
};

/**
 * A singled threaded exercise of the a pool that supports multiple
 * threads.  To actually exercise this class we will need to setup 
 * a thread pool
 */
TEST(LeaseTestSuite, ConcurrentPoolTest03) {
	ConcurrentNumberPool<unsigned short> cp(100, 199);
	vector<unsigned short> vec;

	try {
		for (int iteration = 0; iteration < 40; iteration++) {
			// Need to code a loop
			for (int ncheckout = 0; ncheckout < 25; ncheckout++) {
				vec.push_back(cp.checkout());
				if (vec.size() > 20) {
					vec.front();
					cp.checkin(vec.front());
					vec.erase(vec.begin());
				}
			}
		}
	} catch (const RangeException& /*pe*/) {
		FAIL() << "Range exception";
	}
	//	int nn=1;
}

/**
 * Implementation of Runnable to facilitate manipulation
 * of a RangedPool with concurrency support.
 */
template <typename PooledT>
class ConcurrentPoolTestRunnable : public KEP::Runnable {
public:
	ConcurrentPoolTestRunnable(ConcurrentNumberPool<PooledT>& pool, unsigned long timeout) :
			_pool(pool), _timeout(timeout) {
	}

	virtual ~ConcurrentPoolTestRunnable() {}

	RunResultPtr run() override {
		/*PooledT n =*/_pool.checkout();
		Sleep(_timeout);
		return RunResultPtr();
	}

private:
	ConcurrentNumberPool<PooledT>& _pool;
	unsigned long _timeout;
};

typedef ConcurrentPoolTestRunnable<unsigned short> LeaseTestRunnable;
TEST(LeaseTestSuite, ConcurrentPoolTest04) {
	ConcreteThreadPool tp(5);
	ConcurrentNumberPool<unsigned short> cp(100, 199);

	try {
		for (int factor = 0; factor < 600; factor++) {
			for (int vvar = 0; vvar < 10; vvar++) {
				VarModFactorComplement<unsigned long> p(factor);
				LeaseTestRunnable* runnable = new LeaseTestRunnable(cp, p.calc(vvar));
				KEP::RunnablePtr pRunnable(runnable);
				tp.enqueue(pRunnable);
			}
		}
		tp.start();
		tp.shutdown(ThreadPool::patient)->wait();
	} catch (const RangeException& /*pe*/) {
		FAIL() << "Range exception";
	}
}

/**
 * Class that wraps an unsigned short representing a socket port.  On first glance, this
 * would appear to be a bit of overkill, but network ports are system resources that need
 * to be managed via a leasing system.
class Port {
public:
	Port( unsigned short port ) : _port(port) {}
	virtual ~Port() {}
	unsigned short _port;
};
 */

/*
template < typename LeasedT >
class LeasedResource : public Lease {
public:
	LeasedResource( LeasedT leased ) {
	}
private:
};
 */

/**
 * This class encapsulates a ConcurrentNumberPool<unsigned short> for the purpose of maintaining
 * a pool of port numbers.  Furthermore, it forces lease semantics on the use of these ports.
 * While a port is in use, the lease must be maintained.  Note that there is no correlation
 * between the logical ports provided by this and the physical ports that they serve to address.
class PortPool {
public:
	LeasedResource<Port>& checkout() {
		// Create a lease and submit it to the lease manager
		//
		LeasedResource* p = new LeasedResource< Port >( new Port( _pool.checkout() );
		LeaseManager.add( *p );
	}

private:
	ConcurrentNumberPool< unsigned short > _pool;
};
 */

///////// Test framework
/////
//class TestResource {
//};

/////////////// Frame Work ////////////////////
/////////////// At some point extract out to their own file
// An example of a lease term.
//
#if 0
class LeaseExpiryPolicy {
public:
	virtual bool expired() = 0;
};
#endif
#if 0
class ElapsedTimeExpiryPolicy : public LeaseExpiryPolicy {
public:
	// For elapsed time policy, simply supply a timer 
	// This implementation assumes that the timer is in a 
	// running state.
	//
	ElapsedTimeExpiryPolicy(const TimeDelay timer ) : _timer(timer) {}

	bool expired()					{ return _timer.elapsed(); }
	LeaseExpiryPolicy& renewed()	{ _timer.timer().reset(); }
	TimeDelay _timer;
};
#endif

/**
 * A selection of definitions of the work "lease" found on the tubes
 *		
 *		A contract or instrument conveying property to another for a specified 
 *		period or for a period determinable at the will of either lessor or 
 *		lessee  in consideration of rent or other compensation. 
 *		------------------
 *		To grant the temporary possession or use of (lands, tenements, etc.) 
 *		to another, usually for compensation at a fixed rate; let: She plans 
 *		to lease her apartment to a friend.
 *		------------------
 *		To take or hold by lease e.g. He leased the farm from the sheriff.
 *
 * 
 * The passage of time is the primary but certainly not only reason becomes
 * invalidated.  In practice, other conditions are often usually applied to a 
 * lease.
 *
 * While 99% of our expirations are time based, we will almost certainly
 * need to be able apply other policy types.
 *
 * Grantor At Will
 * Lessor At Will
 * Time
 * 
 * With this in mind, the lease declares the terms whereby:
 *
 *		The Grantor may evict the Grantee from the resource.  An example 
 *		of this would be the case where the Resource Provider 
 *		is running out of Resource and must evict an idle tennant.
 * 
 *		The Grantee may vacate the resource. An example of this would be
 *		the case where Resource is no longer needed or used by a Grantee.
 *		Consider leasing a file handle, when the file.
 *
 *		Arbitrary and possibly external conditions can invalidate a lease, 
 *		e.g. the consumption of some quota (e.g. time, bytes read, operations 
 *		performed etc.).  The existence of arbitrary conditions,
 *		e.g. disk space, memory. etc.
 *
 * In order, to control access to the resource, it is accessed via the lease. 
 * Both the grantee and the grantor monitor changes in the leases state.  When the
 * resource becomes invalid, the grantee vacates the resource, the grantor
 * performs any appropriate operations (e.g. recover/recycle)
 *
 * Implement a single term.  We can create an "aggregate" term when necessary.
 * Implement it as a 
 */
#if 0
class Lease {
public:
	Lease() {}
	virtual ~Lease() {}
    virtual bool expired() = 0;
	virtual std::string key() const = 0;
};
typedef std::shared_ptr< Lease > LeasePtr;
#endif

//template <class ResourceT>
//class GenericLease : public Lease {
//public:
//GenericLease( ResourceT resource ) : _resource(resource) 				{}
//virtual ~GenericLease()		{}
//ResourceT& resource()		{ return _resource;	}
//
//virtual bool		expired() = 0;
//virtual std::string key() const = 0;
//private:
//ResourceT& _resource;
//};

//class KeyGenerator {
//};

// Initial implementation based on elapsed time
// however, the ExpiryPolicy could be based other things.
//
// For example, the ExpiryPolicy could indicate that a lease
// has expired when an object is serialized to file.
//
//
// class ConcreteLease : public Lease {
// public:
// 	ConcreteLease() {	}
// 	virtual ~ConcreteLease() {}
// 	bool expired() { return true; }
//	std::string key() const { return "t"; }
//};

/**
 * The Resource Grantor/Lessor can delegate management of the
 * lease to a Lease Manager.
 */
#if 0
/**
 * Naming interface.  Just for collecting the resource
 * So no methods defined.
 */
class Resource{
public:
	Resource() {}
	virtual ~Resource() {}
};
class Lessor{
public:
	/**
	 * 
	 */
	virtual Lessor& possess( Resource& resource ) = 0;
};

class TestLessor : public Lessor {
public:
	TestLessor(bool possesses=true ) : _possesses(possesses){}
	virtual ~TestLessor() {}
	Lessor& possess( Resource& resource ) {
		_possesses = true;
		return *this;
	}
	bool possesses() { return _possesses; }
private:
	bool _possesses;
};

class Lessee{
public:
	/**
	 *
	 */
	virtual Lessee& vacate( Resource& resource )  = 0;
};

class TestLessee : public Lessee {
public:
	TestLessee(bool vacated = false ) : _vacated(vacated){}
	virtual ~TestLessee() {}
	Lessee& vacate( Resource& resource ){
		_vacated = true;
		return *this;
	}
	bool vacated() { return _vacated; }
private:
	bool _vacated;
};

class Term{
public:
	Term(){}
	virtual ~Term(){}

	/**
	 * Is this term satisfied?
	 */
	virtual bool satisfied() = 0;
};

class BasicExpiryTerm : public Term {
public:
	BasicExpiryTerm( unsigned long millis ) 
		: _elapsed(millis)
	{}
	virtual ~BasicExpiryTerm() 
	{}
	bool satisfied() {
		return !_elapsed.elapsed();
	}
	BasicExpiryTerm& start() {
		_elapsed.timer().start();
		return *this;
	}
private:
	TimeDelay _elapsed;
};
class Lease{
public:
//	Lease( Resource& resource, Lessor& lessor, Lessee& lessee, Term )
	Lease(){}
	virtual ~Lease(){}
	virtual Lease& invalidate() = 0;
	virtual Lease& validate() = 0;
};

/**
 * Support for extending Lease
 */
class AbstractLease : public Lease {
public:
	AbstractLease( Resource& resource, Lessor& lessor, Lessee& lessee, Term& term )
		:	_resource( resource )
			, _lessor( lessor )
			, _lessee( lessee )
			, _term( term )
	{}

	virtual ~AbstractLease()
	{}
protected:
	Resource&	_resource;
	Lessor&		_lessor;
	Lessee&		_lessee;
	Term&		_term;
};

class ConcreteLease : public AbstractLease {
	typedef AbstractLease super;
public:
	ConcreteLease( Resource& resource, Lessor& lessor, Lessee& lessee, Term& term ) 
		: AbstractLease( resource, lessor, lessee, term )
	{}

	Lease& validate()	{
		if ( !super::_term.satisfied() ) invalidate();
		return *this;
	}

	Lease& invalidate() {
		super::_lessee.vacate( super::_resource );
		super::_lessor.possess( super::_resource );
		return *this;
	}
};

typedef std::shared_ptr< Lease > LeasePtr;

class LeaseSponsor {
public:
	LeaseSponsor(){}
	virtual ~LeaseSponsor(){}
	virtual LeaseSponsor& leaseInvalidate( Lease& lease ) = 0;
};

#if 0
class ActiveLeaseManager {
public:
	ActiveLeaseManager& add( LeasePtr  lease ){
		_leaseMap.insert( std::pair< std::string, LeasePtr > ( lease->key(), lease ) );
		return *this;
	}
private:
	// A collection of leases
	std::map< std::string, LeasePtr > _leaseMap;
};
#endif
#if 0
// Types of Expiry
// Time based
// How do we abstract


/**
 * Test Eviction
 * Test Expiration
 * 
 */
TEST( LeaseTestSuite, LeaseTest00 ) {
	Resource resource;
	TestLessor lessor(false);
	TestLessee lessee;
	BasicExpiryTerm term(3000);
	ConcreteLease lease( resource, lessor, lessee, term.start() );
	Sleep( 1000 );
	lease.validate();
	EXPECT_FALSE( lessor.possesses() );
	EXPECT_FALSE( lessee.vacated() );
	Sleep( 3000 );
	lease.validate();
	EXPECT_TRUE( lessor.possesses() );
	EXPECT_TRUE( lessee.vacated() );
}

/**
 * Test Eviction
 * Test Expiration
 * 
 */
TEST( LeaseTestSuite, LeaseTest01 ) {
	Resource resource;
	TestLessor lessor(false);
	TestLessee lessee;
	BasicExpiryTerm term(3000);
	ConcreteLease lease( resource, lessor, lessee, term.start() );
	Sleep( 1000 );
	lease.validate();
	EXPECT_FALSE( lessor.possesses() );
	EXPECT_FALSE( lessee.vacated() );
	Sleep( 3000 );
	lease.validate();
	EXPECT_TRUE( lessor.possesses() );
	EXPECT_TRUE( lessee.vacated() );
#if 0
	ActiveLeaseManager alm;

//    ConcreteLease* cl = new ConcreteLease();
	LeasePtr lptr(cl);
	alm.add( lptr );
#endif

//	ConcreteLease cl;
#endif
}
#endif
