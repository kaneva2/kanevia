///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common/keputil/Traversable.h"

/**
 * Naming interface.
 * To be moved out when implementation is complete.
 */
class Listener {
public:
	Listener() {}
	virtual ~Listener() {}
};

// test fixture

class MyListener : public Listener {
public:
	MyListener() :
			Listener() {}
	virtual ~MyListener() {}

	void eventOccurred() {}
};

/**
 * Naming interface.
 * To be moved out when implementatino is complete.
 * Collects listeners and fires events.  The specific
 * events being fired are implementation specific.
 */
class EventDispatcher {
public:
	EventDispatcher() {}
	virtual ~EventDispatcher() {}
	virtual EventDispatcher& addListener(Listener& listener) = 0;
};

class MyEventDispatcher : public EventDispatcher {
public:
	MyEventDispatcher() :
			EventDispatcher() {}
	virtual ~MyEventDispatcher() {}
	virtual MyEventDispatcher& addListener(Listener& listener) {
		_listeners.push_back(listener);
		return *this;
	}

	MyEventDispatcher& fireEventOccurred() {
		for (std::vector<Listener>::iterator iter = _listeners.begin(); iter != _listeners.end(); iter++) {
			//			iter->eventOccurred();
		}
		return *this;
	}

private:
	std::vector<Listener> _listeners;
};

// We are after thread safety here, so vector is not going to cut it.
// This should be a TraversableVector.
//typedef std::vector<Listener> ListenerVec;

/*
template <typename SpecificT>
class EventDispatcher : public Traverser< SpecificT >{
private:
	class EventOccured : public Traverser< MyListener > {
		void traverse( MyListener& ml ) {
			
		}
	};
public:
	

private:
};
*/

TEST(EventModelTestSuite, Test00) {
	// Create a listener
	//
	MyListener listener;

	// Create an event dispatcher
	//
	MyEventDispatcher dispatcher;

	dispatcher.addListener(listener);
}

