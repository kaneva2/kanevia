///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common/KEPUtil/VLDWrapper.h"
#include <iostream>
#include <stdio.h>
#include <io.h>

#define snprintf _snprintf
#include "common/keputil/mailer.h"
#include "Core/Util/ThreadPool.h"

// Test Fixture
class LightWeightMailerTestSuite : public ::testing::Test {
public:
	SocketSubSystem ss;
};

#if 1
// This sendMail() call will "likely" fail, because passing an empty string
// as a value for host causes the host to resolve to localhost.  If no mail server
// is running this will result in a WSAECONREFUSED
//
TEST_F(LightWeightMailerTestSuite, HardinessTest01) {
	try {
		ErrorSpec es = LightWeightMailer::send("", "", "", "", "");
		EXPECT_EQ(LightWeightMailer::errorNoSender, es.first);
		EXPECT_STREQ(LightWeightMailer::errorString(LightWeightMailer::errorNoSender).c_str(), es.second.c_str());
	} catch (...) {
		FAIL();
	}
}
#endif
#if 1
TEST_F(LightWeightMailerTestSuite, HardinessTest02) {
	try {
		ErrorSpec es = LightWeightMailer::send("mail.kanevia.com", "", "", "", "");
		EXPECT_EQ(LightWeightMailer::errorNoSender, es.first);
		EXPECT_STREQ(LightWeightMailer::errorString(LightWeightMailer::errorNoSender).c_str(), es.second.c_str());
	} catch (...) {
		FAIL();
	}
}
#endif
#if 1
TEST_F(LightWeightMailerTestSuite, HardinessTest03) {
	try {
		ErrorSpec es = LightWeightMailer::send("localhost", "mailer@kanevia.com", "", "", "");
		EXPECT_EQ(LightWeightMailer::errorNoRecipient, es.first);
		EXPECT_STREQ(LightWeightMailer::errorString(LightWeightMailer::errorNoRecipient).c_str(), es.second.c_str());
	} catch (...) {
		FAIL();
	}
}
#endif
#if 1
// this test is having an issue because the WS Subsystem is being shutdown inadvertently.
// The shut down is occurring because the .create() method creates a mailer on the stack
// and then returns the instance (i.e. not a reference to the instance).  Since the mailer
// will be destroyed as it comes off the stack, this is what we want.  However,
// 1) We don't have a copy constructor defined in Mailer (shallow copy).  We could create
// this copy constructor, indeed we should create these methods.
// 2) and this is the kicker...the Mailer desctructor shutsdown the WS subsystem...so either
// way this has to be resolved.
//
TEST_F(LightWeightMailerTestSuite, HardinessTest04) {
	try {
		ErrorSpec es = LightWeightMailer::send("", "mailer@kanevia.com", "recipient@kanevia.com", "", "");
		EXPECT_EQ(IDS_WSAECONNREFUSED, es.first);
		EXPECT_STREQ(WSAErrStr(IDS_WSAECONNREFUSED).c_str(), es.second.c_str());
	} catch (...) {
		FAIL();
	}
}
#endif
#if 1
TEST_F(LightWeightMailerTestSuite, InvalidHostTest) {
	EXPECT_TRUE(LightWeightMailer::send("mail.kaneviaasdfasf.com", "mailer@kanevia.com", "recipient@kanevia.com", "LightWeightMailer v1.1 Release", "Here is another test of LightWeightMailer SMTP class!").error());
}
#endif
#if 1
TEST_F(LightWeightMailerTestSuite, SendMailTest03) {
	EXPECT_TRUE(LightWeightMailer::send("mail.kanevia.com", "mailer@kanevia.com", "recipient@kanevia.com", "LightWeightMailer v1.1 Release", "Here is another test of LightWeightMailer SMTP class!").noError());
}
#endif

#if 1
TEST_F(LightWeightMailerTestSuite, PooledMailTest) {
	// Okay, we can go no further without digging into the "RunResults" abstraction.
	// Then we can check against the ErrorSpec that Mailer returns.
	//
	ThreadPool tp(5);
	tp.start();
	tp.enqueue(LightWeightMailer::createRunnable("mail.kanevia.com", "mailer@kanevia.com", "recipient@kanevia.com", "LightWeightMailer v1.1 Release", "Here is another test of LightWeightMailer SMTP class!"))->getResults();
	tp.shutdown()->wait();
}
#endif

