///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common/keputil/Heartbeat.h"
#include "Common/KEPUtil/Algorithms.h"

#include "Tools\HttpClient\GetBrowserPage.h"

#include <log4cplus/logger.h>

// A note on the following discovery
// Internally, wininet appears to connect to a server, though of course that is not possible.  It doesn't actually
// fail until send request.  At which point GetLastError() returns 12017 (ERROR_WINHTTP_OPERATION_CANCELLED).
// It also returns an implementation specific error code of 406...i.e. 406 is NOT associated with the standard
// HTTP error codes and hence should not be interpreted as HTTP result code 406 "not Acceptable" error.
//
TEST(DiscoveryTest, DISABLED_GetBrowserPage500_validHost) {
	std::string resultText;
	DWORD httpStatus;
	std::string httpStatusMessage;
	size_t resultSize = 0;

	EXPECT_FALSE(GetBrowserPage("localhost:8081", resultText, resultSize, httpStatus, httpStatusMessage, 30));
	EXPECT_EQ(500, httpStatus);
	EXPECT_STREQ("::HttpSendRequest failed. Make sure you have internet connection, and you are not behind an firewall", httpStatusMessage.substr(0, httpStatusMessage.length() - 1).c_str());
}

TEST(DiscoveryTest, DISABLED_GetBrowserPage500_validHost2) {
	std::string resultText;
	DWORD httpStatus;
	std::string httpStatusMessage;
	size_t resultSize = 0;

	EXPECT_FALSE(GetBrowserPage("adfadfa.kaneva.com:8081", resultText, resultSize, httpStatus, httpStatusMessage, 30));
	EXPECT_EQ(500, httpStatus);
	EXPECT_STREQ("::HttpSendRequest failed. Make sure you have internet connection, and you are not behind an firewall", httpStatusMessage.substr(0, httpStatusMessage.length() - 1).c_str());
}

TEST(DiscoveryTest, DISABLED_GetBrowserPage500_invalidHost) {
	std::string resultText;
	DWORD httpStatus;
	std::string httpStatusMessage;
	size_t resultSize = 0;

	EXPECT_FALSE(GetBrowserPage("addfasdfadfddfdosidfsdfaodfadfdoodskqcda", resultText, resultSize, httpStatus, httpStatusMessage, 30));
	EXPECT_EQ(500, httpStatus);
	EXPECT_STREQ("::HttpSendRequest failed. Make sure you have internet connection, and you are not behind an firewall", httpStatusMessage.substr(0, httpStatusMessage.length() - 1).c_str());
}

class MyHeartBeatListener : public HeartbeatListener {
public:
	void thump() {
		//		cout << "Thump" << endl;
	}
};

TEST(HeartbeatTests, DISABLED_HeartbeatTest01) {
	BasicHeartbeatGenerator g(3000);
	g.start();

	MyHeartBeatListener l;

	g.addHeartbeatListener(&l);
	Sleep(5000);

	//	 cout << "Removing listener." << endl;
	g.removeHeartbeatListener(&l);
	Sleep(5000);

	//	 cout << "Adding listener." << endl;
	g.addHeartbeatListener(&l);
	Sleep(5000);
	g.stop();
}
