///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/Tests/SimpleSetTests.h"
#include "Common/KEPUtil/Tests/CommTraceTests.h"
#include "Common/KEPUtil/Tests/LocationRegistryTests.h"
#include "Common/KEPUtil/Tests/ExceptionHelperTests.h"
#include "common/keputil/tests/FileNameTestSuite.h"
#include "common/KEPUtil/Tests/TempFileTests.h"
#include "common/KEPUtil/tests/FileSwitchTests.h"
#include "Common/KEPUtil/tests/ExtendedConfigTests.h"
#include "Common/KEPUtil/tests/URLTests.h"
#include "common/KEPUtil/tests/SynchronizedMapTests.h"
#include "common/keputil/tests/plugintestsuite.h"
#include "common/keputil/tests/StringHelpersTests.h"
#include "Common/KEPUtil/tests/CountDownLatchTests.h"
#include "Common/KEPUtil/tests/WebCallTests.h"

#include "Common/KEPUtil/tests/HeartbeatTests.h"
#include "Common/KEPUtil/tests/ACTTests.h"
#include "Common/KEPUtil/tests/ParamSetTestSuite.h"
#include "common/keputil/tests/httpservertestsuite.h"
#include "Common/KEPUtil/tests/KEPConfigTests.h"
#include "common/keputil/tests/CommandLineTestSuite.h"
#include "common/keputil/tests/EventModelTestSuite.h"
#include "common/keputil/tests/LeaseTestSuite.h"
#include "common/keputil/tests/ThreadPoolTestSuite.h"
#include "common/keputil/tests/timertestsuite.h"
//#include "Common/KEPUtil/tests/StackTraceTests.h"
