///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "core/Util/Executor.h"
#include "common/keputil/CountDownLatch.h"

class ACTDiscoRunnable1 : public KEP::Runnable {
public:
	ACTDiscoRunnable1(ACTPtr actp, CountDownLatch* latch) :
			_latch(latch), _subject(actp) {}

	RunResultPtr run() override {
		//        ::Sleep(10000);
		_subject->wait(); // wait for the subjects completion state to be set.
		_latch->countDown(); // and notify external that we are done.
		return RunResultPtr();
	}

private:
	ACTDiscoRunnable1() :
			_latch(0), _subject() {}
	ACTDiscoRunnable1(const ACTDiscoRunnable1& /* other */) :
			_latch(0), _subject() {}
	ACTDiscoRunnable1& operator=(const ACTDiscoRunnable1& /*rhs*/) { return *this; }
	ACTPtr _subject;
	CountDownLatch* _latch;
};

TEST(ACTDiscoveryTests, ACTDisco02) {
	jsThreadExecutor exec1("Executor21");
	exec1.start();
	jsThreadExecutor exec2("Executor22");
	exec2.start();

	for (int lcv1 = 0; lcv1 < 100; lcv1++) {
		try {
			CountDownLatch latch(2);
			ACTPtr actp(new ACT());

			ACTDiscoRunnable1* runnable1 = new ACTDiscoRunnable1(actp, &latch);
			RunnablePtr runptr1(runnable1);
			ExecutablePtr executable1(new Executable(runptr1));

			ACTDiscoRunnable1* runnable2 = new ACTDiscoRunnable1(actp, &latch);
			RunnablePtr runptr2(runnable2);
			ExecutablePtr executable2(new Executable(runptr2));

			exec1.execute(executable1);
			exec2.execute(executable2);
			actp->complete();
			if (!latch.await(4000))
				FAIL();
		} catch (const ChainedException& e) {
			FAIL() << "UnexpectedException [" << e.str().c_str() << "]";
		}
	}
	exec1.stop();
	exec2.stop();
}
