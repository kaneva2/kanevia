///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common\KEPUtil\Helpers.h"

TEST(TimerTest, Test1) {
	Timer timer;
	while (timer.ElapsedSec() < 3.0)
		;
	EXPECT_TRUE(timer.ElapsedSec() >= 3.0);
}

TEST(TimerTest, ResetTest1) {
	Timer timer;
	for (int n = 0; n < 3; n++) {
		while (timer.ElapsedMs() < 1000ul)
			;
		unsigned long elapsed = timer.ElapsedMs();
		EXPECT_TRUE(elapsed >= 1000ul);
		EXPECT_TRUE(elapsed < 1100ul);
		timer.Reset();
	}
}

TEST(TimerTest, ResetTest2) {
	Timer timer;
	fTime::SleepMs(1000.0);
	EXPECT_TRUE(timer.ElapsedMs() > 0.0);
	timer.Pause();
	timer.Reset();
	EXPECT_TRUE(timer.ElapsedMs() < 1000.0);
}

TEST(TimerTest, ResetTest3) {
	Timer timer;
	fTime::SleepMs(1000.0);
	EXPECT_TRUE(timer.ElapsedMs() > 0.0);
	timer.Reset();
	EXPECT_TRUE(timer.ElapsedMs() < 1000.0);
}

TEST(TimerTest, PauseTest1) {
	Timer timer(Timer::State::Paused);
	fTime::SleepMs(1000.0);
	EXPECT_TRUE(timer.ElapsedMs() == 0.0);
}

TEST(TimerTest, PauseTest2) {
	Timer timer;
	fTime::SleepMs(1000.0);
	auto elapsed = timer.Pause().ElapsedMs();
	fTime::SleepMs(1000.0);
	EXPECT_TRUE(timer.ElapsedMs() == elapsed);
}
