///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/threadpool.h"
#include "Test/testhelpers.h"

#define THREAD_POOL_TEST_SUITE_QUARANTINE

//#define do_longrunning
//#define do_broken

bool verbose = false;

typedef SleepFunctor<unsigned long> ULSF;

/**
 * SleepFunctor that returns the calculation of a supplied
 * SleepFunctor implementation or, if the functor input
 * should be a multiple of a specified value, throws
 * exception.
 */
class ExceptionOnMultiple : public ULSF {
public:
	unsigned long _multiple;
	SleepFunctor* _functor;

	/**
	 * 
	 */
	ExceptionOnMultiple(unsigned long multiple, SleepFunctor* functor) :
			_multiple(multiple), _functor(functor) {}

	virtual ~ExceptionOnMultiple() { delete _functor; }

	virtual unsigned long calc(unsigned long var) {
		if ((var % _multiple) == 0) {
			throw new KEPException("");
		}
		return _functor->calc(var);
	}
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////// Runnable implementations ////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// ThreadPool is actually kind of a misnomer, it is a pool of threads but
// users of this class don't ever actually see a thread.  It does however
// manage a pool of threads and dispatch work to those threads.  "work" is
// is packaged into implementations of KEP::Runnable.  ThreadPool accepts
// Runnable instances, queues them and when a thread resource becomes available
// dispatches a runnable to that thread.
//
// The following class are implemenations of KEP::Runnable that exist
// solely for the purpose of exercising ThreadPool.
//
// Many of the tests attempt to overwhelm the Thread pool both when
// queueing jobs and also when waiting for those jobs to complete.
// To Accomplish this, we utilize a pipeline consisting of three
// ThreadPools.
// The first ThreadPool is filled with runnables that generate
// more runnables and queue them into the second stage ThreadPool.

/**
 * A simple Runnable implementation...for the purposes of this test suite.  Each
 * instance has an id, and a sleep value, both assigned at instantiation.
 */
class SubjectRunnable : public KEP::Runnable {
public:
	unsigned long _id;
	SleepFunctor<unsigned long>* _functor;
	RunResult _result;

	SubjectRunnable(unsigned long id, ULSF* functor, RunResult result = RunResult()) :
			_id(id), _functor(functor), _result(result) {}

	virtual ~SubjectRunnable() { delete _functor; }

	RunResultPtr run() override {
		// Shortcoming?  calc on each run?  This is likely not too bad as the task is at least
		// having to do something.
		//
		// Look at using single instance of the functor.  e.g. create a single instance of a
		// factory class. This factory creates single functor, multiple tasks.  Cut construction
		// time and localize to context switch time.
		//
		unsigned long w = _functor->calc(_id);
		if (verbose)
			cout << "running ID[" << _id << "][" << w << "]" << endl;
		if (w > 0)
			Sleep(w);
		if (verbose)
			cout << "ID[" << _id << "] done." << endl;
		return RunResultPtr(new RunResult(_result));
	}
};

class RunnableFactory {
public:
	virtual RunnablePtr create() = 0;
};

class SubjectRunnable2 : public KEP::Runnable {
public:
	unsigned long _id;
	ULSF& _functor;
	RunResult _result;

	SubjectRunnable2(unsigned long id, ULSF& functor, RunResult result = RunResult()) :
			_id(id), _functor(functor), _result(result) {}

	virtual ~SubjectRunnable2() {}

	RunResultPtr run() override {
		// Shortcoming?  calc on each run?  This is likely not too bad as the task is at least
		// having to do something.
		//
		// Look at using single instance of the functor.  e.g. create a single instance of a
		// factory class. This factory creates single functor, multiple tasks.  Cut construction
		// time and localize to context switch time.
		//
		unsigned long w = _functor.calc(_id);
		if (verbose)
			cout << "running ID[" << _id << "][" << w << "]" << endl;
		if (w > 0)
			Sleep(w);
		if (verbose)
			cout << "ID[" << _id << "] done." << endl;
		return RunResultPtr(new RunResult(_result));
	}
};

class SubjectFactory : public RunnableFactory {
public:
	ULSF& _taskDuration;
	unsigned long _counter;

	SubjectFactory(ULSF& taskDuration) :
			_taskDuration(taskDuration), _counter(1) {}

	RunnablePtr create() {
		return RunnablePtr(new SubjectRunnable2(_counter++, _taskDuration));
	}
};

class PollObjectHandler : public KEP::Runnable {
public:
	ACTPtr _act;
	unsigned long _wait;

	PollObjectHandler(ACTPtr act, unsigned long wait = 0) :
			_act(act), _wait(wait) {}
	RunResultPtr run() override {
		if (&(*_act) == 0) {
			//int n1 = 1;
		}
		_act->wait(_wait);
		return RunResultPtr();
	}
};

class ProducerRunnable : public KEP::Runnable {
public:
	ThreadPool& _tpool;
	ULSF* _sleepSpawn;
	bool _verbose;

	ProducerRunnable(ThreadPool& tpool, ULSF* spawnFunctor, bool verbose = false) :
			_tpool(tpool), _sleepSpawn(spawnFunctor), _verbose(verbose) {}

	virtual ~ProducerRunnable() { delete _sleepSpawn; }
};

/**
 * Generator runs for specific amount of time.
 * during that time it 
 */
// The task generator used by the Producer pool.  We place one of these
// for each thread in the producer pool, generates tasks that
//
// Variable spawn rate
// Variable task duration
class ProducerRunnable1 : public ProducerRunnable {
public:
	//	SleepFunctor*  _sleepDuration;
	unsigned long _runDurationMS;
	RunnableFactory& _runnableFactory;

	/**
	 * @param runDurationMS			The time in milliseconds that this generator
	 *								is to run.
	 *
	 * @param taskSpawnFactor		Factor to be used when calculating the amount
	 *								of time to wait before spawning the next runnable
	 *								Calculated as follows:
	 *
	 *									MSToNextSpawn = taskSpawnFactor - (Id % taskSpawnFactor)
	 *
	 * @param taskDurationFactor	Factor used to calculated the amount of time the 
	 *								generated task is to run. Calculated as follows:
	 *
	 *									MSForTaskToRun = Id % taskDurationFactor
	 *
	 */
	ProducerRunnable1(ThreadPool& tpool, unsigned long runDurationMS, unsigned long taskSpawnFactor, RunnableFactory& runnableFactory, bool verbose = false) :
			ProducerRunnable(tpool, new VarModFactorComplement<unsigned long>(taskSpawnFactor), verbose), _runDurationMS(runDurationMS), _runnableFactory(runnableFactory) {}

	virtual ~ProducerRunnable1() {}

	RunResultPtr run() override {
		if (verbose)
			cout << "TaskGenerator::run()" << endl;
		Timer timer;
		int n = 0;
		//unsigned long dur = _runDurationMS;
		while (timer.ElapsedMs() < _runDurationMS) {
			n++;
			//			_tpool.enqueue( RunnablePtr( new SubjectRunnable( n, new VarModFactorFunctor(n) ) ) );
			//			_tpool.enqueue( RunnablePtr( new SubjectRunnable( n, new VarModFactorComplement(100) ) ) );
			_tpool.enqueue(_runnableFactory.create());
			// Calculate the amount of time to wait before spawning the next instance.
			//
			unsigned long spawnDelay = _sleepSpawn->calc(n);

			// And wait for the next iteration
			//
			if (spawnDelay > 0)
				Sleep(spawnDelay);
		}
		return RunResultPtr();
	}
};

class ProducerRunnable2 : public ProducerRunnable {
public:
	ThreadPool& _hpool;

	unsigned long _taskDurationFactor;
	unsigned long _taskSpawnFactor;
	unsigned long _runDurationMS;
	unsigned long _genCount;

	ProducerRunnable2(ThreadPool& tpool, ThreadPool& hpool, unsigned long genCount, unsigned long runDurationMS, unsigned long taskSpawnFactor, unsigned long taskDurationFactor) :
			ProducerRunnable(tpool, new VarModFactorComplement<unsigned long>(taskSpawnFactor)), _genCount(genCount), _hpool(hpool), _taskSpawnFactor(taskSpawnFactor), _taskDurationFactor(taskDurationFactor), _runDurationMS(runDurationMS) {}

	RunResultPtr run() override {
		if (verbose)
			cout << "TaskGenerator::run()" << endl;
		for (unsigned long n = 0; n < _genCount; n++) {
			ACTPtr act = _tpool.enqueue(RunnablePtr(new SubjectRunnable(n, new VarModFactorFunctor<unsigned long>(_taskDurationFactor), RunResult(1))));
			_hpool.enqueue(RunnablePtr(new PollObjectHandler(act)));

			unsigned long spawnDelay = _sleepSpawn->calc(n);
			if (spawnDelay > 0)
				Sleep(spawnDelay);
		}
		return RunResultPtr();
	}
};

class ProducerRunnable4 : public ProducerRunnable {
public:
	ThreadPool& _hpool;

	unsigned long _taskDurationFactor;
	unsigned long _runDurationMS;
	unsigned long _genCount;

	ProducerRunnable4(ThreadPool& tpool, ThreadPool& hpool, unsigned long genCount, unsigned long runDurationMS, unsigned long taskSpawnFactor, unsigned long taskDurationFactor) :
			ProducerRunnable(tpool, new VarModFactorComplement<unsigned long>(taskSpawnFactor)), _genCount(genCount), _hpool(hpool), _taskDurationFactor(taskDurationFactor), _runDurationMS(runDurationMS) {}

	RunResultPtr run() override {
		if (verbose)
			cout << "TaskGenerator::run()" << endl;
		for (unsigned long n = 0; n < _genCount; n++) {
			ACTPtr act = _tpool.enqueue(RunnablePtr(new SubjectRunnable(n, new VarModFactorFunctor<unsigned long>(_taskDurationFactor), RunResult(1))));
			_hpool.enqueue(RunnablePtr(new PollObjectHandler(act, 5000)));
			unsigned long spawnDelay = _sleepSpawn->calc(n);
			if (spawnDelay > 0)
				Sleep(spawnDelay);
		}
		return RunResultPtr();
	}
};

class ProducerRunnable3 : public ProducerRunnable {
public:
	ThreadPool& _hpool;
	unsigned long _taskDurationFactor;
	unsigned long _runDurationMS;

	ProducerRunnable3(ThreadPool& tpool, ThreadPool& hpool, unsigned long runDurationMS, unsigned long taskSpawnFactor, unsigned long taskDurationFactor) :
			ProducerRunnable(tpool, new VarModFactorComplement<unsigned long>(taskSpawnFactor)), _hpool(hpool), _taskDurationFactor(taskDurationFactor), _runDurationMS(runDurationMS) {}

	RunResultPtr run() override {
		if (verbose)
			cout << "TaskGenerator::run()" << endl;
		Timer timer;
		int n = 0;
		while (timer.ElapsedMs() < _runDurationMS) {
			ACTPtr act = _tpool.enqueue(RunnablePtr(new SubjectRunnable(n++, new VarModFactorFunctor<unsigned long>(_taskDurationFactor), RunResult(1))));
			_hpool.enqueue(RunnablePtr(new PollObjectHandler(act)));
			unsigned long spawnDelay = _sleepSpawn->calc(n);
			if (spawnDelay > 0)
				Sleep(spawnDelay);
		}
		return RunResultPtr();
	}
};

class ProducerRunnable5 : public ProducerRunnable {
public:
	ThreadPool& _hpool;

	unsigned long _taskDurationFactor;
	unsigned long _taskSpawnFactor;
	unsigned long _runDurationMS;

	ProducerRunnable5(ThreadPool& tpool, ThreadPool& hpool, unsigned long runDurationMS, unsigned long taskSpawnFactor, unsigned long taskDurationFactor)
			// todo: NOTE THAT THIS MODEL IS A FIXED TIME DURATION
			:
			ProducerRunnable(tpool, new VarModFactorComplement<unsigned long>(taskSpawnFactor)),
			_hpool(hpool), _taskSpawnFactor(taskSpawnFactor), _taskDurationFactor(taskDurationFactor), _runDurationMS(runDurationMS) {}

	virtual ~ProducerRunnable5() {}

	RunResultPtr run() override {
		if (verbose)
			cout << "TaskGenerator::run()" << endl;
		Timer timer;
		int n = 0;
		while (timer.ElapsedMs() < _runDurationMS) {
			ACTPtr act = _tpool.enqueue(RunnablePtr(new SubjectRunnable(n++, new VarModFactorFunctor<unsigned long>(_taskDurationFactor), RunResult(1))));
		}
		return RunResultPtr();
	}
};

class ProducerRunnable6 : public ProducerRunnable {
public:
	unsigned long _taskDurationFactor;
	unsigned long _taskCount;

	ProducerRunnable6(ThreadPool& tpool, unsigned long taskCount, unsigned long taskSpawnFactor, unsigned long taskDurationFactor) :
			ProducerRunnable(tpool, new VarModFactorComplement<unsigned long>(taskSpawnFactor)), _taskCount(taskCount), _taskDurationFactor(taskDurationFactor) {}

	RunResultPtr run() override {
		if (verbose)
			cout << "TaskGenerator::run()" << endl;
		unsigned long mmax = 0;
		for (unsigned long n = 0; n < _taskCount; n++) {
			_tpool.enqueue(RunnablePtr(new SubjectRunnable(n, new VarModFactorFunctor<unsigned long>(_taskDurationFactor))));

			unsigned long spawnDelay = _sleepSpawn->calc(n);
			mmax = max(mmax, spawnDelay);

			if (spawnDelay > 0)
				Sleep(spawnDelay);
		}
		if (verbose)
			cout << "*********** maxdelay=" << mmax << endl;
		return RunResultPtr();
	}
};

/**
 * While TestRunnable may have a single implementation, the methods we use to generate them do not
 */
// Note that these ThreadPool tests generally fall into two categories:
//		1) Unit test
//		2) Load Test
//			These test attempt to apply appropriate loads to test the threadpools hardiness and
//			don't necessarily do any test assertions (e.g. EXPECT_EQ etc). These tests typically
//			construct three separate ThreadPools.
//
//				1)	Producer Pool.  Thread pool one is used to generate Executors in a
//					non-deterministic manner.  Aside from being a general test of the thread pool,
//					it also tests the thread safeness of the ThreadPools input queue.
//				2)  Subject Pool.  This pool is, generally speaking, the real focus of any test as
//					it is executing targeted tasks
//				3)  Monitor Pool.  This pool contains tasks that are tracking the completion of the
//					tasks in the Subject Pool.  Used to test shutdown and result return methods.
//
#if 1
// Recipe:
//		Producer Pool:
//				11 threads.
//				Generates 10000 tasks that run for 30000ms.
//		Subject Pool:
//				15 threads
//		Monitor Pool:
//				None
//
// This a long running stability test, not necessarily a unit test.
// Enable as need but keep DISABLED in archive to facilitate automated
// testing by the build system
//
TEST(ThreadPoolTest, DISABLED_ThreadPoolMultipleInputTestVariant3) {
	const unsigned long producerPoolThreadCount = 2;
	const unsigned long consumerPoolThreadCount = 5; //30;
	const unsigned long runDurationMS = 3000;
	//const unsigned long taskCount					= 10000;
	const unsigned long taskSpawnFactor = 0;
	const unsigned long taskDurationFactor = 4;

	ConcreteThreadPool producerPool(producerPoolThreadCount, std::string("Producer"));
	ConcreteThreadPool subjectPool(consumerPoolThreadCount, std::string("Subject"));
	VarModFactorFunctor<unsigned long> taskDurationFunctor(taskDurationFactor);
	SubjectFactory factory(taskDurationFunctor);

	// Queue a producer on each thread.
	//
	for (int n = 0; n < producerPoolThreadCount; n++)
		producerPool.enqueue(RunnablePtr(new ProducerRunnable1(subjectPool, runDurationMS, taskSpawnFactor, factory, true)));

	producerPool.start()->wait(); // Start it and wait for the startup to complete.

	// Shut down in hurried mode.

	producerPool.shutdown(ThreadPool::patient)->wait(); // Shut it down and wait for the shutdown to complete.
	// This has the effect of producing jobs for the next phase on all threads (thus achieving
	// indeterminence) until all jobs are produced.

	subjectPool.start(); // Now fire up our consumer pool.  This should
	// cause each of our randomized tasks will be executed on all threads.

	// ThreadPool.shutdown() returns an ACT which allows
	// the caller optionally block or not block until
	// the pool shutdown has been completed.
	//

	// Is the problem that we have already shut the pool down?  In fact when we
	// shutit dowo
	//	producerPool.shutdown(ThreadPool::patient)->wait();

	if (verbose)
		cout << "Producer shutdown complete!" << endl;

	unsigned long ul1 = subjectPool.pending();
	unsigned long ul2 = ul1;
	while (ul1 > 0) {
		Sleep(1000);
		ul2 = subjectPool.pending();

		if (verbose)
			cout << ul2 << "(" << (ul1 - ul2) << ")" << endl;
		ul1 = ul2;
	}
	if (verbose)
		cout << subjectPool.toString() << endl;
	subjectPool.shutdown(ThreadPool::patient)->wait();
	if (verbose)
		cout << "Consumer shutdown complete!" << endl;
}
#endif

/**
 * A very simple test.  Generates 1000 tasks, each with a
 * unique id.  The unique ids are generated from a 
 * sequence.  An instance of this task simply sleeps
 * for a duration defined as Id % 100.
 * 
 * Any task id where Id % 9 == 0 throws an exception.  Hence
 * tests the hardiness of the pool.
 */
TEST(ThreadPoolTest, ThreadPoolTest1) {
	// Create a thread pool with 5 threads named test.
	// This has the effect of causing each of these threads
	// to be named test0, test1, etc...
	//
	ConcreteThreadPool pool(5, std::string(::testing::UnitTest::GetInstance()->current_test_info()->name()));

	// Start the pool
	//
	pool.start();

	for (int n = 0; n < 1000; n++) {
		// Create a functor to facilitate (Id % 100)
		//
		ULSF* pf1 = new VarModFactorFunctor<unsigned long>(100);

		// Create a functor to encapsulates pf1 but
		// throws an exception when the Task id is a
		// multiple of 9
		//
		ULSF* pf2 = new ExceptionOnMultiple(9, pf1);

		// Now create a runnable which, when run, will
		// sleep for (Id % 100) and throw an exception
		// whenever the Id is a multiple of 9.
		//
		KEP::Runnable* pr = new SubjectRunnable(n, pf2);

		// Finally enqueue the Runnable.
		//
		pool.enqueue(RunnablePtr(pr));
	}

	// Perform a patient shutdown the thread pool and wait for
	// the shutdown to complete.
	//
	LOG4CPLUS_INFO(Logger::getInstance(L"Testing"), "ThreadPoolTest1:: Shutting down pool");
	pool.shutdown(ThreadPool::patient)->wait();
	LOG4CPLUS_INFO(Logger::getInstance(L"Testing"), "ThreadPoolTest1:: Poll shutdown complete");
}

TEST(ThreadPoolTest, DISABLED_ThreadPoolLongRunTest) {
	const unsigned long threadCount = 5;
	const unsigned long taskCount = 10000;
	const unsigned long taskSpawnFactor = 0;
	const unsigned long taskDurationFactor = 1;

	ConcreteThreadPool pool(threadCount, std::string("test"));
	if (verbose)
		cout << "Starting pool" << endl;
	pool.start();
	ULSF* _sleepSpawn = new VarModFactorComplement<unsigned long>(taskSpawnFactor);
	for (int n = 0; n < taskCount; n++) {
		ULSF* pf = new VarModFactorFunctor<unsigned long>(taskDurationFactor);
		KEP::Runnable* pr = new SubjectRunnable(n, pf);
		pool.enqueue(RunnablePtr(pr));
		unsigned long spawnDelay = _sleepSpawn->calc(n);
		if (spawnDelay > 0)
			Sleep(spawnDelay);
	}
	if (verbose)
		cout << "Shutting down pool" << endl;
	pool.shutdown(ThreadPool::patient)->wait();
	if (verbose)
		cout << "pool kill complete" << endl;
	delete _sleepSpawn;
}

TEST(ThreadPoolTest, PatientShutdownTest1) {
	ConcreteThreadPool pool(5, std::string("ThreadPoolTest::PatientShutdownTest1"));
	EXPECT_EQ(5, pool.capacity());
	EXPECT_EQ(0, pool.idle());
	pool.start()->wait();

	// All executors have started, but they may not have entered
	// idle state yet...and beyond this test, there is no real value
	// in making this state waitable.
	// so simply give them some time to get to idle
	//
	Sleep(50);

	EXPECT_EQ(5, pool.idle());
	pool.shutdown(ThreadPool::patient)->wait();
	EXPECT_EQ(0, pool.idle());
}

#if 1
TEST(ThreadPoolTest, HurriedShutdownTest1) {
	ConcreteThreadPool pool(5, std::string("ThreadPoolTest::PatientShutdownTest1"));
	EXPECT_EQ(5, pool.capacity());
	EXPECT_EQ(0, pool.idle());
	pool.start()->wait();

	// All executors have started, but they may not have entered
	// idle state yet...and beyond this test, there is no real value
	// in making this state waitable.
	// so simply give them some time to get to idle
	//
	Sleep(50);

	EXPECT_EQ(5, pool.idle());
	pool.shutdown(ThreadPool::hurried)->wait();

	EXPECT_EQ(0, pool.idle());
}
#endif
#if 1
TEST(ThreadPoolTest, PatientShutdownTest2) {
	const unsigned long producerPoolThreadCount = 1;
	const unsigned long consumerPoolThreadCount = 15;
	const unsigned long handlersPoolThreadCount = 15;
	const unsigned long runDurationMS = 1000;
	const unsigned long taskSpawnFactor = 0;
	const unsigned long taskDurationFactor = 4;
	const unsigned long genCount = 1000;

	ConcreteThreadPool producerPool(producerPoolThreadCount, std::string("Producer"));
	ConcreteThreadPool consumerPool(consumerPoolThreadCount, std::string("Consumer"));
	ConcreteThreadPool handlersPool(handlersPoolThreadCount, std::string("Handler"));

	for (int n = 0; n < producerPoolThreadCount; n++)
		producerPool.enqueue(RunnablePtr(new ProducerRunnable2(consumerPool, handlersPool, genCount, runDurationMS, taskSpawnFactor, taskDurationFactor)));

	handlersPool.start();
	consumerPool.start();
	producerPool.start();

	// ThreadPool.shutdown() returns an ACT which allows
	// the caller optionally block or not block until
	// the pool shutdown has been completed.
	//
	producerPool.shutdown(ThreadPool::patient)->wait();
	if (verbose)
		cout << "Producer shutdown complete!" << endl;
	consumerPool.shutdown(ThreadPool::patient)->wait();
	if (verbose)
		cout << "Consumer shutdown complete!" << endl;
	handlersPool.shutdown(ThreadPool::patient)->wait();

	EXPECT_EQ((producerPoolThreadCount * genCount) + consumerPoolThreadCount, consumerPool.processed());
}
#endif

TEST(ThreadPoolTest, ThreadPoolMultipleInputTestVariantx) {
	const unsigned long producerPoolThreadCount = 1;
	const unsigned long consumerPoolThreadCount = 15;
	const unsigned long handlersPoolThreadCount = 15;
	const unsigned long runDurationMS = 1000;
	const unsigned long taskSpawnFactor = 0;
	const unsigned long taskDurationFactor = 4;

	ConcreteThreadPool producerPool(producerPoolThreadCount, std::string("Producer"));
	ConcreteThreadPool consumerPool(consumerPoolThreadCount, std::string("Consumer"));
	ConcreteThreadPool handlersPool(handlersPoolThreadCount, std::string("Handler"));

	for (int n = 0; n < producerPoolThreadCount; n++)
		producerPool.enqueue(RunnablePtr(new ProducerRunnable3(consumerPool, handlersPool, runDurationMS, taskSpawnFactor, taskDurationFactor)));

	handlersPool.start();
	consumerPool.start();
	producerPool.start();

	// ThreadPool.shutdown() returns an ACT which allows
	// the caller optionally block or not block until
	// the pool shutdown has been completed.
	//
	producerPool.shutdown(ThreadPool::patient)->wait();
	if (verbose)
		cout << "Producer shutdown complete!" << endl;
	consumerPool.shutdown(ThreadPool::patient)->wait();
	if (verbose)
		cout << "Consumer shutdown complete!" << endl;
	handlersPool.shutdown(ThreadPool::patient)->wait();
}

// Test that insures that the thread pool can accept
// inputs from multiple threads safely.
// This is the first of a series of tests that will use
// multiple thread pools.  In this case, One Thread pool
// that will take a task that generates other tasks
// and puts them into the second queue.
//
TEST(ThreadPoolTest, ThreadPoolMultipleInputTestVariant1) {
	const unsigned long producerPoolThreadCount = 4;
	const unsigned long consumerPoolThreadCount = 5;
	const unsigned long taskCount = 10000;
	const unsigned long taskSpawnFactor = 0;
	const unsigned long taskDurationFactor = 0;

	class Consumer : public KEP::Runnable {
	public:
		Consumer(unsigned long taskDurationFactor = 0, int id = 0) :
				_id(id), _durationSleep(new VarModFactorFunctor<unsigned long>(taskDurationFactor)) {}

		virtual ~Consumer() { delete _durationSleep; }
		RunResultPtr run() override {
			unsigned long w = _durationSleep->calc(_id);
			if (w > 0)
				Sleep(w);
			return RunResultPtr();
		}

		unsigned long _id;
		ULSF* _durationSleep;
	};

	class Producer1 : public KEP::Runnable {
	public:
		ThreadPool& _tpool;
		unsigned long _taskDurationFactor;
		unsigned long _genCount;
		ULSF* _spawnSleep;

		Producer1(ThreadPool& tpool, unsigned long genCount, unsigned long taskSpawnFactor, unsigned long taskDurationFactor) :
				_tpool(tpool), _genCount(genCount), _spawnSleep(new VarModFactorComplement<unsigned long>(taskSpawnFactor)), _taskDurationFactor(taskDurationFactor) {}

		virtual ~Producer1() { delete _spawnSleep; }

		RunResultPtr run() override {
			if (verbose)
				cout << "Producer::run()" << endl;
			for (unsigned long n = 0; n < _genCount; n++) {
				_tpool.enqueue(RunnablePtr(new Consumer(_taskDurationFactor, n)));
				unsigned long spawnDelay = _spawnSleep->calc(n);
				if (spawnDelay > 0)
					Sleep(spawnDelay);
			}
			return RunResultPtr();
		}
	};

	ConcreteThreadPool producerPool(producerPoolThreadCount, std::string("Producer"));
	ConcreteThreadPool consumerPool(consumerPoolThreadCount, std::string("Consumer"));

	for (int n = 0; n < producerPoolThreadCount; n++)
		producerPool.enqueue(RunnablePtr(new Producer1(consumerPool, taskCount, taskSpawnFactor, taskDurationFactor)));

	producerPool.start();
	producerPool.shutdown(ThreadPool::patient)->wait();
	if (verbose)
		cout << "Producer shutdown complete!" << endl;

	consumerPool.start();
	consumerPool.shutdown(ThreadPool::patient)->wait();
	if (verbose)
		cout << "Consumer shutdown complete!" << endl;
}

#if 0
TEST( ThreadPoolTest, ThreadPoolKillTest ) {
	ThreadPool pool(5, std::string("test" ));

	for ( int n = 0
			; n < 1000
			; n++ )  {
		pool.enqueue( RunnablePtr( new SubjectRunnable(n, new VarModFactorFunctor(100)) ) );
	}
	if ( verbose ) cout << "Starting pool" << endl;
	pool.start();
	if ( verbose ) cout << "Shutting down pool" << endl;
	pool.shutdown( ThreadPool::murderous );
	if ( verbose ) cout << "pool kill complete" << endl;
}
#endif

#ifdef do_broken
TEST(ThreadPoolTest, HurriedShutdownTest2) {
	const unsigned long producerPoolThreadCount = 1;
	const unsigned long consumerPoolThreadCount = 15;
	const unsigned long handlersPoolThreadCount = 15;
	const unsigned long runDurationMS = 1000;
	const unsigned long taskSpawnFactor = 0;
	const unsigned long taskDurationFactor = 4;
	const unsigned long genCount = 1000;

	ThreadPool producerPool(producerPoolThreadCount, std::string("Producer"));
	ThreadPool consumerPool(consumerPoolThreadCount, std::string("Consumer"));
	ThreadPool handlersPool(handlersPoolThreadCount, std::string("Handler"));

	for (int n = 0; n < producerPoolThreadCount; n++)
		producerPool.enqueue(RunnablePtr(new ProducerRunnable4(consumerPool, handlersPool, genCount, runDurationMS, taskSpawnFactor, taskDurationFactor)));

	handlersPool.start()->wait();
	consumerPool.start()->wait();
	producerPool.start()->wait();

	// ThreadPool.shutdown() returns an ACT which allows
	// the caller optionally block or not block until
	// the pool shutdown has been completed.
	//
	producerPool.shutdown(ThreadPool::patient)->wait();
	if (verbose)
		cout << "Producer shutdown complete!" << endl;
	consumerPool.shutdown(ThreadPool::hurried)->wait();
	if (verbose)
		cout << "Consumer shutdown complete!" << endl;
	handlersPool.shutdown(ThreadPool::hurried)->wait();
	if (verbose)
		cout << "Handlers shutdown complete!" << endl;

	// Test - Assert that the number of tasks processed by the
	// the consumer pool is equal to:
	//		(number of producer threads * the per thread gen count)
	//			+ the number of arrestors used to stop the consumer pool.
	//
	EXPECT_EQ((producerPoolThreadCount * genCount) + consumerPoolThreadCount, consumerPool.processed());
}
#endif
#ifdef do_broken
TEST(ThreadPoolTest, ThreadPoolMultipleInputTestVariantz) {
	const unsigned long producerPoolThreadCount = 11;
	const unsigned long consumerPoolThreadCount = 11;
	const unsigned long handlersPoolThreadCount = 15;
	const unsigned long runDurationMS = 30000000;
	//	const unsigned long taskCount					= 1000;
	const unsigned long taskSpawnFactor = 0;
	const unsigned long taskDurationFactor = 4;

	ThreadPool producerPool(producerPoolThreadCount, std::string("Producer"));
	ThreadPool consumerPool(consumerPoolThreadCount, std::string("Consumer"));
	ThreadPool handlersPool(handlersPoolThreadCount, std::string("Handler"));

	for (int n = 0; n < producerPoolThreadCount; n++)
		producerPool.enqueue(RunnablePtr(new ProducerRunnable5(consumerPool, handlersPool, runDurationMS, taskSpawnFactor, taskDurationFactor)));

	consumerPool.start();
	producerPool.start();

	// ThreadPool.shutdown() returns an ACT which allows
	// the caller optionally block or not block until
	// the pool shutdown has been completed.
	//
	producerPool.shutdown(ThreadPool::patient)->wait();
	if (verbose)
		cout << "Producer shutdown complete!" << endl;
	consumerPool.shutdown(ThreadPool::patient)->wait();
	if (verbose)
		cout << "Consumer shutdown complete!" << endl;
}
#endif

#ifdef do_longrunning
// Test that insures that the thread pool can accept
// inputs from multiple threads safely.
// This is the first of a series of tests that will use
// multiple thread pools.  In this case, One Thread pool
// that will take a task that generates other tasks
// and puts them into the second queue.
//
TEST(ThreadPoolTest, ThreadPoolMultipleInputTestVariant2) {
	const unsigned long producerPoolThreadCount = 4;
	const unsigned long consumerPoolThreadCount = 5;
	const unsigned long taskCount = 100000; // 100000
	const unsigned long taskSpawnFactor = 0;
	const unsigned long taskDurationFactor = 0;

	ThreadPool producerPool(producerPoolThreadCount, std::string("Producer"));
	ThreadPool consumerPool(consumerPoolThreadCount, std::string("Consumer"));

	for (int n = 0; n < producerPoolThreadCount; n++)
		producerPool.enqueue(RunnablePtr(new ProducerRunnable6(consumerPool, taskCount, taskSpawnFactor, taskDurationFactor)));

	consumerPool.start();
	producerPool.start();

	// ThreadPool.shutdown() returns an ACT which allows
	// the caller optionally block or not block until
	// the pool shutdown has been completed.
	//
	producerPool.shutdown(ThreadPool::patient)->wait();
	if (verbose)
		cout << "Producer shutdown complete!" << endl;

	if (verbose)
		cout << "Preparing to shutdown consumer pool with Id [" << consumerPool.pending() << "]" << endl;
	consumerPool.shutdown(ThreadPool::patient)->wait();
	if (verbose)
		cout << "Consumer shutdown complete!" << endl;
}
#endif
#endif // if included

// detritis

// Sleep Functors.  Classes that create values to
// to be fed into ::Sleep().  The tasks executed in this
// test suite, don't actually do any work.  Rather
// they simply sleep for a specified amount of time.
// These classes generate those sleep times.
//
#if 0
/**
 * Sleep functor is used supply an unsigned long value
 * to be used as an argument to ::Sleep().
 */
class SleepFunctor {
public:
	/**
	 * Calculate an unsigned long.
	 * @param	val		unsigned long value to be used
	 *					as a generalized input to the 
	 *					calculation.  Usage can and does
	 *					vary with the implementation.
	 * @returns	An unsigned long
	 *
	 */
	virtual unsigned long calc( unsigned long val ) = 0;
	virtual ~SleepFunctor() {}
};

/**
 * Implementation of SleepFunctor that simply returns the 
 * functor input.
 */
class ConstantFunctor : public SleepFunctor {
public:
	unsigned long _constant;

	/**
	 * @see		SleepFunctor
	 * @param	Constant		This SleepFunctor implementation simply returns
	 *							this value for calc.
	 */
	ConstantFunctor( unsigned long Constant ) : _constant(Constant)	{}

	/**
	 * @see		SleepFunctor
	 * @param   var				This Sleepfunctor implementation ignores this
	 *							parameter.
	 */
	unsigned long calc( unsigned long var )							{ return _constant;	}
};

/**
 * SleepFunctor implementation that mods the calculation input with a 
 * supplied divisor.  It's name is derived from logical description
 *
 *       var % factor
 * 
 * Usage:
 *		VarModFactorFunctor vmf(10);// define a VarModFactorFunctor devides
 *									// the calculation by 10 and 
 *									// returns the remainder
 *      vmf.calc(43)				// Should return (43 % 10) or 3.
 */
class VarModFactorFunctor : public SleepFunctor {
public:
	unsigned long _factor;
	/**
	 * Construct with a factor to be used as the divisor in a modulus operation
	 */
	VarModFactorFunctor( unsigned long factor ) : _factor( factor )	{}
	unsigned long calc( unsigned long var )						{ return _factor > 0 ? var % _factor	: 0; }
};

/**
 * @see VarModFactorFunctor
 * Extends the functionality of VarModFactorFunctor by taking the extra step
 * of subtracting the VarModFactor result from the factor, thus calculating
 * a complement.
 */
class VarModFactorComplement : public SleepFunctor {
public:
	unsigned long _factor;
	VarModFactorComplement( unsigned long factor ) : _factor( factor ) 	{}
	unsigned long calc( unsigned long var )			{ return _factor > 0 ? _factor - ( var % _factor ) : 0;	}
};
#endif

// sample from web.
//
class AddressBook {
public:
	AddressBook() {
		_addresses.push_back("www.kaneva.com");
		_addresses.push_back("www.wikipedia.org");
	}
	// using a template allows us to ignore the differences between functors, function pointers
	// and lambda
	template <typename Func>
	std::vector<std::string> findMatchingAddresses(Func func) {
		std::vector<std::string> results;
		for (auto itr = _addresses.begin(), end = _addresses.end(); itr != end; ++itr) {
			// call the function passed into findMatchingAddresses and see if it matches
			if (func(*itr)) {
				results.push_back(*itr);
			}
		}
		return results;
	}

	vector<string> findAddressesFromOrgs() {
		return findMatchingAddresses(
			// we're declaring a lambda here; the [] signals the start
			[](const string& addr) { return addr.find(".org") != string::npos; });
	}

private:
	std::vector<std::string> _addresses;
};

#if 0 // lambda DISCO
TEST( ThreadPoolTest, LambdaTest01 )
{
	AddressBook global_address_book;

	int c = 0;

	auto l = global_address_book.findMatchingAddresses(
		// we're declaring a lambda here; the [] signals the start
		[&](const string& addr) mutable
	{
		c++;
		return addr.find(".org") != string::npos;
	});


	ASSERT_EQ(2, c);
	ASSERT_EQ(1, l.size());
	ASSERT_STREQ("www.wikipedia.org", l[0].c_str())	;// _addresses.push_back("www.wikipedia.org");

}

TEST(ThreadPoolTest, LambdaTest02)
{
	AddressBook global_address_book;

	int c = 0;

	auto l = global_address_book.findMatchingAddresses(
		// we're declaring a lambda here; the [] signals the start
		[=](const string& addr) mutable
	{
		c++;
		return addr.find(".org") != string::npos;
	});


	ASSERT_EQ(0, c);
	ASSERT_EQ(1, l.size());
	ASSERT_STREQ("www.wikipedia.org", l[0].c_str());// _addresses.push_back("www.wikipedia.org");

}
TEST(ThreadPoolTest, LambdaTest03) {
	ConcreteThreadPool tp(5);
	tp.start()->wait();
	CountDownLatch l(1);

	tp.enqueue(RunnablePtr(new FunctionRunner([&]() {
		  l.countDown();
		  return RunResultPtr();
	  })))
		->wait();
	l.await();

	tp.shutdown()->wait();
	ASSERT_EQ(0, l.count());
}

TEST(ThreadPoolTest, LambdaTest04) {
	ConcreteThreadPool tp(5);
	tp.start()->wait();
	CountDownLatch l(1);

	tp.enqueue([&]() {
		  l.countDown();
		  return RunResultPtr();
	  })->wait();
	l.await();
	tp.shutdown()->wait();
	ASSERT_EQ(0, l.count());
}
