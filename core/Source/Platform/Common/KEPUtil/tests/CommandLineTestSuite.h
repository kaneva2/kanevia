///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common/keputil/commandline.h"

// Initial CommandLine implementation is now deprecated in favor of CommandLineII::CommandLine.
// This updated CommandLine object is based on the boost::program_options library which gives
// much greater functionality.
#if 0
// Unit tests for the CommandLine class
//
/**
 * Simple test.  Define a command line parser with one of each of our command line
 * parameter types.  Supply a command line the specifies each of these parameters
 * and verify that the we get the expected values back.
 */
TEST( CommandLineTestSuite, CommandlineTest00 ) {

	// Define a commandline parser with one boolean and one string parameter
	//
	CommandLine cl;
	CommandLine::BooleanParameter& bp = cl.addBoolean( "-b", false );
	CommandLine::StringParameter&  sp = cl.addString( "-s", "defaultstring" );

	char* argv[] = {"myexecutablewithpath", "-b", "-s", "string" };

	// Parse the command args defined above.
	//
	cl.parse( sizeof(argv)/4, argv  );

	// Test that the parser picked up the correct values
	//
	EXPECT_TRUE((bool)bp);
	EXPECT_STREQ( "string", ((string)sp).c_str() );
//	EXPECT_STREQ( "myexecutablewithpath", (const char*)cl.executable().subject().c_str() );
}

/**
 * Test that when a string param is not referenced in a command line that
 * the supplied default value is returned
 */
TEST( CommandLineTestSuite, DefaultedStringParamTest ) {
	CommandLine cl;
	CommandLine::BooleanParameter& bp = cl.addBoolean( "-r", false );
	CommandLine::StringParameter&  sp = cl.addString( "-c", "c:\\stage\\kep_prod.pv\\test.conf" );

	char* argv[] = {"myexecutablewithpath", "-r" };
	cl.parse( sizeof(argv)/4, argv  );
	EXPECT_FALSE( sp.hasBeenSet() );
	EXPECT_TRUE((bool)bp);
	EXPECT_STREQ( "c:\\stage\\kep_prod.pv\\test.conf", ((string)sp).c_str() );
//	EXPECT_STREQ( "myexecutablewithpath", cl.executable().subject().c_str() );
}

/**
 * Test anonymous parameters.  I.e. parameters to not have any indicator
 * these would be parameters that identify something for an application to
 * work on.  E.g.   notepad <filename>
 * Notepad exists to edit text files, so it is perfectly reasonable to not
 * have to preface the parameter.
 */ 
TEST( CommandLineTestSuite, AnonymousParameterTest ) {
	CommandLine cl;
	CommandLine::BooleanParameter& bp = cl.addBoolean(	"-r", false );
	CommandLine::StringParameter&  sp = cl.addString(	"-c", "c:\\stage\\kep_prod.pv\\test.conf");

	char* argv[] = {"myexecutablewithpath", "-r", "anonymous1", "anonymous2"  };
	cl.parse( sizeof(argv)/4, argv  );
	EXPECT_FALSE( sp.hasBeenSet() );
	EXPECT_TRUE((bool)bp);
	EXPECT_STREQ( "c:\\stage\\kep_prod.pv\\test.conf", ((string)sp).c_str() );
//	EXPECT_STREQ( "myexecutablewithpath", cl.executable().subject().c_str() );
	EXPECT_EQ( (size_t)2, cl.orphans().size() );
	EXPECT_STREQ( "anonymous1", cl.orphans()[0].c_str() );
	EXPECT_STREQ( "anonymous2", cl.orphans()[1].c_str() );
}

// Currently only supported for string parameters
// Test Parameterized defaults
TEST( CommandLineTestSuite, ParameterizedDefaultsTest00 ) {
	CommandLine cl;
	CommandLine::StringParameter&  sp = cl.addString(	"-c", "%(AppName)");

	char* argv[] = {"c:\\somepath\\someexecutable" };
	cl.parse( 1, argv  );
	EXPECT_STREQ( "someexecutable", sp.value().c_str() );
}

// Test execution of help resolves the defaults
// Requires visual inspection and hence is not appropriate for
// automated testing.
TEST( CommandLineTestSuite, DISABLED_ParemeterizedDefaultsTest01 ) {
	CommandLine cl;
	CommandLine::StringParameter sp = cl.addString( "-c", "%(AppName)", "Defaults to %(AppName)" );

	// Create a sample commandline
	char* argv[] = {"c:\\somepath\\someexecutable", "--help" };

	// parse it
	cl.parse(2, argv );
}
#endif
