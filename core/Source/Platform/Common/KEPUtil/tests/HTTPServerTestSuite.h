///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// #define BREAKIT__

#pragma comment(lib, "httpclient.lib")

#include <log4cplus/logger.h>

#include "Core/Util/ObjectRegistry.h"
#include "Core/Util/Singleton.h"
#include "common/keputil/httpserver.h"
#include "common/keputil/mongoosehttpserver.h"
#include "common/include/kepconfig.h"
#include "Common/include/UrlParser.h"
#include "Common/KEPUtil/SocketSubsystem.h"
#include "Common/KEPUtil/URL.h"
#include "tools/httpclient/GetBrowserPage.h"

#include "common/keputil/pluginhostconfigurator.h"

static short portNo = 8900;

class TestEndPoint01 : public HTTPServer::RequestHandler {
public:
	TestEndPoint01() :
			HTTPServer::RequestHandler("TestEndPoint01") {}
	virtual int handle(const HTTPServer::Request& /*request*/, HTTPServer::EndPoint& endPoint) {
		// Okay...we have two problems...
		// 1. Regardless of the media type (e.g. UTF-16, UTF-8, etc.), we always assume a wide charset.
		//    SEE http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.7
		// 2. GetBrowserPage assumes narrow string
		// The fix is to make sure that HTTP Server is capable of dispatching narrow charsets as well
		// so to that end parse the media-type component of the content type header and do a lookup
		// to determine the width of a character set.  And write it accordingly.
		//
		endPoint << (HTTPServer::Response() << Header("Content-Type", "text/html;charset=UTF-8") << "OK");
		return 1;
	}
};

class TestEndPoint02 : public HTTPServer::RequestHandler {
public:
	bool called;
	TestEndPoint02() :
			HTTPServer::RequestHandler("TestEndPoint02"), called(false) {}
	virtual int handle(const HTTPServer::Request& /*request*/, HTTPServer::EndPoint& endPoint) {
		// Okay...we have two problems...
		// 1. Regardless of the media type (e.g. UTF-16, UTF-8, etc.), we always assume a wide charset.
		//    SEE http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.7
		// 2. GetBrowserPage assumes narrow string
		// The fix is to make sure that HTTP Server is capable of dispatching narrow charsets as well
		// so to that end parse the media-type component of the content type header and do a lookup
		// to determine the width of a character set.  And write it accordingly.
		//
		endPoint << (HTTPServer::Response()
					 << Header("Content-Type", "text/html;charset=UTF-8")
					 << "OK");
		called = true;
		return 1;
	}
};

const string TestParam03("And so you see, here we are");
class TestEndPoint03 : public HTTPServer::RequestHandler {
public:
	TestEndPoint03() :
			HTTPServer::RequestHandler("TestEndPoint02"), _passed(false) {}
	virtual int handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint) {
		// Okay...we have two problems...
		// 1. Regardless of the media type (e.g. UTF-16, UTF-8, etc.), we always assume a wide charset.
		//    SEE http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.7
		// 2. GetBrowserPage assumes narrow string
		// The fix is to make sure that HTTP Server is capable of dispatching narrow charsets as well
		// so to that end parse the media-type component of the content type header and do a lookup
		// to determine the width of a character set.  And write it accordingly.
		//
		endPoint << (HTTPServer::Response()
					 << Header("Content-Type", "text/html;charset=UTF-8")
					 << "OK");
		map<string, string>::const_iterator i = request._paramMap.find("parm");
		_passed = i == request._paramMap.end() ? false : true;
		if (!_passed)
			return 1;
		EXPECT_STREQ(TestParam03.c_str(), i->second.c_str());
		return 1;
	}
	bool _passed;
};

TEST(HTTPServerTestSuite, UrlEscapeTest) {
	TestEndPoint03 ep02;
	MongooseHTTPServer AppHostController;
	AppHostController.addPortBinding(8900)
		.enableFileServing(false)
		.enableDirectories(false)
		.registerHandler(ep02, "/testendpoint")
		.start();

	std::string resultText;
	DWORD httpStatusCode;
	std::string httpStatusDescription;

	// Issue #1.  GetBrowserPage creates a new HttpClient with every call.
	// Issue #2.  GetBrowserPage is dependent on Log4cplus.
	//
	std::string parmValue = UrlParser::escapeString(TestParam03);
	stringstream ss;
	ss << "http://localhost:8900/testendpoint?parm=" << parmValue;
	size_t resultSize = 0;
	/*bool b =*/GetBrowserPage(
		ss.str(), resultText, resultSize, httpStatusCode, httpStatusDescription);
	ASSERT_TRUE(ep02._passed);
}

static URL MainURL;

TEST(HTTPServerTestSuite, P2PTest01) {
	TestEndPoint01 ep01;
	MongooseHTTPServer AppHostController;
	AppHostController.addPortBinding(portNo)
		.enableFileServing(false)
		.enableDirectories(false)
		.registerHandler(ep01, "/testendpoint")
		.start();

	std::string resultText;
	DWORD httpStatusCode;
	std::string httpStatusDescription;

	// Issue #1.  GetBrowserPage creates a new HttpClient with every call.
	// Issue #2.  GetBrowserPage is dependent on Log4cplus.
	//
	MainURL.setScheme("http")
		.setHost("127.0.0.1")
		.setPort(portNo)
		.addPath("testendpoint");

	size_t resultSize = 0;
	bool b = GetBrowserPage(
		MainURL.toString(), resultText, resultSize, httpStatusCode, httpStatusDescription);

	EXPECT_TRUE(b);
	EXPECT_EQ(200, httpStatusCode);
	EXPECT_STREQ("OK", httpStatusDescription.c_str());
	EXPECT_STREQ("OK", resultText.c_str());
	MongooseHTTPServer AppHost;
	AppHost.addPortBinding(portNo)
		.enableFileServing(false)
		.enableDirectories(false)
		.start();

	AppHost.stop();
	AppHostController.stop();
}

/**
 * Let's play with the HTTPServerPlugin
 * Disabled while I investigate KEPConfig capabilities.
 */
TEST(HTTPServerTestSuite, HTTPServerPluginConfiguratorTest00) {
	// Create a static library and add it to the plugin host
	//
	PluginLibraryPtr staticLib(new PluginLibrary());
	staticLib->addPluginFactory(PluginFactoryPtr(new MongooseHTTPPluginFactory()));

	// PluginHost supports some singleton semantics, though the singularity is not
	// enforced.  I.e. the singleton class's constructor is not hidden.  You can
	// have more than one PluginHost, but only one "singleton" PluginHost.
	//
#ifdef BREAKIT__
	PluginHost ph;
#else
	PluginHost& ph = PluginHost::singleton();
#endif
	ph.addPluginLibrary(staticLib);

	// Note, no libraries section yet as we don't have any dynamic libraries....may need to
	// light up provisioner project to make test
	//
	std::stringstream ss;
	ss << "<config>"
	   << "<" << TAG_PLUGINS << ">"
	   << "<" << TAG_PLUGIN << " " << TAG_FACTORY_NAME << "=\"HTTPServer\" " << TAG_INSTANCE_NAME << "=\"HTTPServer\">"
	   << "<PluginConfig Port=\"" << portNo << "\" acl=\"-127.0.0.1\" DocumentRoot=\"c:\\\" EnableFileServing=\"true\" EnableDirectories=\"true\" ThreadCount=\"5\"/>"
	   << "</" << TAG_PLUGIN << ">"
	   << "</" << TAG_PLUGINS << ">"
	   << "</config>";

	KEPConfig c;
	std::string s = ss.str();
	c.Parse(ss.str().c_str(), "config");
	PluginHostConfigurator().configure(ph, c);
	ph.shutdown();
}

TEST(HTTPServerTestSuite, BROKEN_HTTPServerPluginConfiguratorTest01) {
	// A Couple of things of note...we should NOT be using the singleton
	// What is the this scope block for?
	{
		// Create a static library and add it to the plugin host
		//
		PluginLibraryPtr staticLib(new PluginLibrary());
		staticLib->addPluginFactory(PluginFactoryPtr(new MongooseHTTPPluginFactory()));

		// PluginHost supports some singleton semantics, though the singularity is not
		// enforced.  I.e. the singleton class's constructor is not hidden.  You can
		// have more than one PluginHost, but only one "singleton" PluginHost.
		//
#ifdef BREAKIT__
		PluginHost ph;
#else
		PluginHost& ph = PluginHost::singleton();
#endif
		ph.addPluginLibrary(staticLib);

		// Note, no libraries section yet as we don't have any dynamic libraries....may need to
		// light up provisioner project to make test
		//
		std::stringstream ss;
		ss << "<config>"
		   << "<" << TAG_PLUGINHOST << ">"
		   << "<" << TAG_PLUGINS << ">"
		   << "<" << TAG_PLUGIN << " " << TAG_FACTORY_NAME << "=\"HTTPServer\" " << TAG_INSTANCE_NAME << "=\"HTTPServer\">"
		   << "<PluginConfig Port=\"" << portNo << "\" DocumentRoot=\"c:\\\" acl=\"-127.0.0.1\" EnableFileServing=\"true\" EnableDirectories=\"true\" ThreadCount=\"5\"/>"
		   << "</" << TAG_PLUGIN << ">"
		   << "</" << TAG_PLUGINS << ">"
		   << "</" << TAG_PLUGINHOST << ">"
		   << "</config>";

		KEPConfig c;
		std::string s = ss.str();
		c.Parse(ss.str().c_str(), "config");

		//	PluginHost& host	= PluginHost::singleton();
		PluginHostConfigurator().configure(ph, c);

		// Add an endpoint
		PluginPtr plugin = ph.getPlugin("HTTPServer");
		HTTPServerPlugin& serv = (HTTPServerPlugin&)(*plugin);

		EXPECT_STREQ("-127.0.0.1", serv.acl().c_str());

		TestEndPoint02* ep02 = new TestEndPoint02();
		serv.registerHandler(*ep02, "/testendpoint");

		std::string resultText;
		DWORD httpStatusCode;
		std::string httpStatusDescription;
		size_t resultSize = 0;

		// Issue #1.  GetBrowserPage creates a new HttpClient with every call.
		// Issue #2.  GetBrowserPage is dependent on Log4cplus.
		//
		MainURL.pathString() = "/testendpoint";
		bool b = GetBrowserPage(
			MainURL.toString() // "http://127.0.0.1:8900/testendpoint"
			,
			resultText, resultSize, httpStatusCode, httpStatusDescription);

		EXPECT_FALSE(b);
		EXPECT_EQ(500, httpStatusCode);
		EXPECT_STREQ("::HttpSendRequest failed. Make sure you have internet connection, and you are not behind an firewall", httpStatusDescription.c_str());
		EXPECT_STREQ("", resultText.c_str());
		EXPECT_FALSE(ep02->called);

		ph.shutdown(); // Is this really necessary?  Or would it get shutdown via destruction?
	}
}

TEST(HTTPServerTestSuite, BROKEN_ACLTest00) {
	SocketSubSystem socksys;
	TestEndPoint02 ep02;

	// Create an HTTPServer.  Test against MongooseHTTP specifically
	//
	MongooseHTTPServer webserver;
	// Configure to block calls from
	//
	webserver.addPortBinding(portNo, false)
		.enableFileServing(false)
		.enableDirectories(false)
		//			 .setACL( "-127.0.0.1,+10.1.201.56" )  // Deny from local, allow from 201.56
		.setACL("-127.0.0.1")
		.registerHandler(ep02, "/testendpoint");
	webserver.start();

	//	Sleep( 99999999999);

	std::string resultText;
	DWORD httpStatusCode;
	std::string httpStatusDescription;

	// Issue #1.  GetBrowserPage creates a new HttpClient with every call.
	// Issue #2.  GetBrowserPage is dependent on Log4cplus.
	//
	// Not sure what's up with this.  A change to GetBrowserPage
	// perhaps.  But calling this url while the server is running, works just fine
	//
	URL url = URL::parse("http://127.0.0.1:8900/testendpoint");

	//	MainURL.pathString() = "";

	size_t resultSize = 0;
	bool b = GetBrowserPage(
		url.toString() // "http://127.0.0.1:8900/testendpoint"
		,
		resultText, resultSize, httpStatusCode, httpStatusDescription);

	EXPECT_FALSE(b);

	// It would appear that web caller mods broke the expected response.
	// However, the connection is indeed rejected.  So for now, let's
	// mark the test as broken
	//
	EXPECT_EQ(500, httpStatusCode);
	EXPECT_STREQ("::HttpSendRequest failed. Make sure you have internet connection, and you are not behind an firewall\n", httpStatusDescription.c_str());
	EXPECT_STREQ("accept_new_connection: 127.0.0.1 is not allowed to connect", webserver.lastError().c_str());
	EXPECT_STREQ("", resultText.c_str());
	EXPECT_FALSE(ep02.called);

	webserver.stop();
}

// It would appear that using a self signed certificate may be mucking with GetBrowserPage as the
// request times out.
// Do so through a browser however, as expected, prompts to trust the connection.
// In any case, this test will fail...disabled until GetBrowserPage is fixed.  Until it a very long
// sleep is performed, thus allowing time for a call from an external browser.
//
TEST(HTTPServerTestSuite, DISABLED_SSLTest00) {
	TestEndPoint01 ep01;

	// Create an HTTPServer.  Test against MongooseHTTP specifically
	//
	MongooseHTTPServer AppHostController;
	// If the start fails (as missing the certificate), that might be an inssue.
	//
	AppHostController.addPortBinding(443, true)
		.enableFileServing(false)
		.enableDirectories(false)
		.setCertificate("platformtest.pem")
		.registerHandler(ep01, "/testendpoint");
	AppHostController.start();
	EXPECT_STREQ("", AppHostController.lastError().c_str());

	//	Sleep( 99999999999);

	std::string resultText;
	DWORD httpStatusCode;
	std::string httpStatusDescription;

	// Issue #1.  GetBrowserPage creates a new HttpClient with every call.
	// Issue #2.  GetBrowserPage is dependent on Log4cplus.
	// Not sure what's up with this.  A change to GetBrowserPage
	// perhaps.  But calling this url while the server is running, works just fine
	//
	MainURL.setScheme("https")
		.addPath("testendpoint")
		.setPort(443);
	std::string ts = MainURL.toString();
	size_t resultSize = 0;
	bool b = GetBrowserPage(
		MainURL.toString() // "https://localhost:443/testendpoint"
		,
		resultText, resultSize, httpStatusCode, httpStatusDescription);

	EXPECT_TRUE(b);
	EXPECT_EQ(200, httpStatusCode);
	EXPECT_STREQ("OK", httpStatusDescription.c_str());
	EXPECT_STREQ("OK", resultText.c_str());
	EXPECT_STREQ("", AppHostController.lastError().c_str());
	AppHostController.stop();
}

