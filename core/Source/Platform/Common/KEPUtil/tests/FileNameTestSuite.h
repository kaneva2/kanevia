///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "Common/KEPUtil/Algorithms.h"

TEST(FileNameTestSuite, getShortNamesTest01) {
	string pathSource("data");
	string pathDest;
	// STRING SUB
	// string pathDestSub
	FileName::GetShortNames(pathSource, pathDest);
	ASSERT_STREQ("data", pathDest.c_str());
}

TEST(FileNameTestSuite, getShortNamesTest02) {
	string pathSource("c:\\test\\data\\filename.txt");
	string pathDest;
	// STRING SUB
	// string pathDestSub
	FileName::GetShortNames(pathSource, pathDest);
	ASSERT_STREQ("filename.txt", pathDest.c_str());
}

TEST(FileNameTestSuite, getShortNamesTest03) {
	string pathSource("c:\\test\\data\\filename.txt");
	string pathDest;
	// STRING SUB
	string pathDestSub;
	FileName::GetShortNames(pathSource, pathDest, "\\test\\data\\test.RLB", &pathDestSub);
	ASSERT_STREQ("filename.txt", pathDest.c_str());
}

TEST(FileNameTestSuite, toNameExt01) {
	FileName f1("\\dir1\\dir2\\test.txt");
	ASSERT_STREQ("test.txt", f1.toNameExt().c_str());
}

TEST(FileNameTestSuite, SetExtensionTest01) {
	FileName f1("\\dir1\\dir2\\test.txt");
	f1.setExt("tmp");
	ASSERT_STREQ("tmp", f1.ext().c_str());
	ASSERT_STREQ(".tmp", f1.extWithDelim().c_str());
}

TEST(FileNameTestSuite, SetExtensionTest02) {
	FileName f1("\\dir1\\dir2\\test.txt");
	f1.setExt(".tmp");
	ASSERT_STREQ("tmp", f1.ext().c_str());
	ASSERT_STREQ(".tmp", f1.extWithDelim().c_str());
}

TEST(FileNameTestSuite, FileNameTest00) {
	FileName f1("c:\\dir1\\dir2\\test.txt");
	EXPECT_STREQ("c:", f1.drive().c_str());
	EXPECT_STREQ("\\dir1\\dir2\\", f1.dir().c_str());
	EXPECT_STREQ("test", f1.fname().c_str());
	EXPECT_STREQ("txt", f1.ext().c_str());
	EXPECT_STREQ(".txt", f1.extWithDelim().c_str());
	EXPECT_STREQ("c:\\dir1\\dir2\\test.txt", f1.toString().c_str());
}

TEST(FileNameTestSuite, FileNameTest01) {
	FileName f1("\\dir1\\dir2\\test.txt");
	EXPECT_STREQ("", f1.drive().c_str());
	EXPECT_STREQ("\\dir1\\dir2\\", f1.dir().c_str());
	EXPECT_STREQ("test", f1.fname().c_str());
	EXPECT_STREQ("txt", f1.ext().c_str());
	EXPECT_STREQ(".txt", f1.extWithDelim().c_str());
	EXPECT_STREQ("\\dir1\\dir2\\test.txt", f1.toString().c_str());
}

TEST(FileNameTestSuite, FileNameTest02) {
	FileName f1("\\dir1\\dir2\\test");
	EXPECT_STREQ("", f1.drive().c_str());
	EXPECT_STREQ("\\dir1\\dir2\\", f1.dir().c_str());
	EXPECT_STREQ("test", f1.fname().c_str());
	EXPECT_STREQ("", f1.ext().c_str());
	EXPECT_STREQ("\\dir1\\dir2\\test", f1.toString().c_str());
}

TEST(FileNameTestSuite, FileNameTest03) {
	FileName f1("\\dir1\\dir2\\");
	f1.appendDir("test");
	EXPECT_STREQ("", f1.drive().c_str());
	EXPECT_STREQ("\\dir1\\dir2\\test\\", f1.dir().c_str());
	EXPECT_STREQ("", f1.fname().c_str());
	EXPECT_STREQ("", f1.ext().c_str());
	EXPECT_STREQ("\\dir1\\dir2\\test\\", f1.toString().c_str());
}

/**
 * the leading slash is eaten as they are trimmed and singular put pack when building the path
 */
TEST(FileNameTestSuite, FileNameTest04) {
	FileName f1("\\\\kaneva.com\\dir1\\somefile.xml");
	EXPECT_STREQ("\\\\kaneva.com", f1.drive().c_str());
	EXPECT_STREQ("\\kaneva\\dir1\\", f1.dir().c_str());
	EXPECT_STREQ("somefile", f1.fname().c_str());
	EXPECT_STREQ("xml", f1.ext().c_str());
	string s(f1.toString());
	EXPECT_STREQ("\\\\kaneva.com\\dir1\\somefile.xml", f1.toString().c_str());
}

