///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/KEPUtil/FileSwitch.h"
#include "Common/KEPUtil/TempFile.h"

TEST(FileSwitchTests, FileSwitchTest01) {
	TempFile t(FileName::createTemp("tmp"), ios_base::trunc | ios_base::out);
	t << 0;

	FileSwitch f(t.name());
	EXPECT_FALSE(f.isOff());
	EXPECT_TRUE(f.isOn());
	::Sleep(15000);
	EXPECT_FALSE(f.isOff());
	EXPECT_TRUE(f.isOn());
}

TEST(FileSwitchTests, FileSwitchTest02) {
	FileName tfilename = FileName::createTemp("tmp"); // Create a temp file name
	FileSwitch f(tfilename, 0); // Create a switch based on this filename

	EXPECT_TRUE(f.isOff()); // At this point the file does not
	EXPECT_FALSE(f.isOn()); // exist so switch is off

	{
		TempFile t(tfilename, ios_base::trunc | ios_base::out);
		t << 0; // A temp file doesn't actually exist until
			// we write something into it.

		EXPECT_FALSE(f.isOff()); // Switch should be on now.
		EXPECT_TRUE(f.isOn()); //
	}
	EXPECT_TRUE(f.isOff()); // Temp file is now off the stack
	EXPECT_FALSE(f.isOn()); // and should be gone and the switch off
}