///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "common/KEPUtil/TempFile.h"

TEST(TempFileTests, Test01) {
	FileName fname;
	{
		TempFile tempFile(FileName::createTemp("tmp"), ios_base::trunc | ios_base::out);
		fname = tempFile.name();
#if 0
        tempFile <<  "This is only a test" << endl << "But it's only good if the previous line is present" ;
        string fname = tempFile.name().toString();
        tempFile.close();
        cout << fname << endl;
#endif
		ASSERT_TRUE(TempFile::exists(fname));
	}
	ASSERT_FALSE(TempFile::exists(fname));
}