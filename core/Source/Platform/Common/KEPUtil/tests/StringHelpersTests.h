///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/StringHelpers.h"

TEST(StringHelpersTests, FromShortTest01) {
	EXPECT_STREQ("32765", StringHelpers::parseNumber<short>(32765).c_str());
	EXPECT_STREQ("32766", StringHelpers::parseNumber<short>(32766).c_str());
	EXPECT_STREQ("32767", StringHelpers::parseNumber<short>(32767).c_str());
	EXPECT_STREQ("-32768", StringHelpers::parseNumber<short>((unsigned short)32768).c_str());
	EXPECT_STREQ("-32767", StringHelpers::parseNumber<short>((unsigned short)32769).c_str());
	EXPECT_STREQ("-32766", StringHelpers::parseNumber<short>((unsigned short)32770).c_str());
}

TEST(StringHelpersTests, FromShortTest02) {
	short s = 32765;
	EXPECT_STREQ("32765", StringHelpers::parseNumber<short>(s++).c_str());
	EXPECT_STREQ("32766", StringHelpers::parseNumber<short>(s++).c_str());
	EXPECT_STREQ("32767", StringHelpers::parseNumber<short>(s++).c_str());
	EXPECT_STREQ("-32768", StringHelpers::parseNumber<short>(s++).c_str());
	EXPECT_STREQ("-32767", StringHelpers::parseNumber<short>(s++).c_str());
	EXPECT_STREQ("-32766", StringHelpers::parseNumber<short>(s++).c_str());
}

TEST(StringHelpersTests, FromUnsignedShortTest01) {
	EXPECT_STREQ("32765", StringHelpers::parseNumber<unsigned short>(32765).c_str());
	EXPECT_STREQ("32766", StringHelpers::parseNumber<unsigned short>(32766).c_str());
	EXPECT_STREQ("32767", StringHelpers::parseNumber<unsigned short>(32767).c_str());
	EXPECT_STREQ("32768", StringHelpers::parseNumber<unsigned short>(32768).c_str());
	EXPECT_STREQ("32769", StringHelpers::parseNumber<unsigned short>(32769).c_str());
	EXPECT_STREQ("32770", StringHelpers::parseNumber<unsigned short>(32770).c_str());
}

TEST(StringHelpersTests, FromUnsignedShortTest02) {
	unsigned short s = 32765;
	EXPECT_STREQ("32765", StringHelpers::parseNumber<unsigned short>(s++).c_str());
	EXPECT_STREQ("32766", StringHelpers::parseNumber<unsigned short>(s++).c_str());
	EXPECT_STREQ("32767", StringHelpers::parseNumber<unsigned short>(s++).c_str());
	EXPECT_STREQ("32768", StringHelpers::parseNumber<unsigned short>(s++).c_str());
	EXPECT_STREQ("32769", StringHelpers::parseNumber<unsigned short>(s++).c_str());
	EXPECT_STREQ("32770", StringHelpers::parseNumber<unsigned short>(s++).c_str());
}

TEST(StringHelpersTests, FromIntTest01) {
	EXPECT_STREQ("2147483645", StringHelpers::parseNumber<int>(2147483645).c_str());
	EXPECT_STREQ("2147483646", StringHelpers::parseNumber<int>(2147483646).c_str());
	EXPECT_STREQ("2147483647", StringHelpers::parseNumber<int>(2147483647).c_str());
	EXPECT_STREQ("-2147483648", StringHelpers::parseNumber<int>(2147483648).c_str());
	EXPECT_STREQ("-2147483647", StringHelpers::parseNumber<int>(2147483649).c_str());
	EXPECT_STREQ("-2147483646", StringHelpers::parseNumber<int>(2147483650).c_str());
}

TEST(StringHelpersTests, FromIntTest02) {
	int i = 2147483645;
	EXPECT_STREQ("2147483645", StringHelpers::parseNumber<int>(i++).c_str());
	EXPECT_STREQ("2147483646", StringHelpers::parseNumber<int>(i++).c_str());
	EXPECT_STREQ("2147483647", StringHelpers::parseNumber<int>(i++).c_str());
	EXPECT_STREQ("-2147483648", StringHelpers::parseNumber<int>(i++).c_str());
	EXPECT_STREQ("-2147483647", StringHelpers::parseNumber<int>(i++).c_str());
	EXPECT_STREQ("-2147483646", StringHelpers::parseNumber<int>(i++).c_str());
}

TEST(StringHelpersTests, FromUnsignedIntTest01) {
	EXPECT_STREQ("2147483645", StringHelpers::parseNumber<unsigned int>(2147483645).c_str());
	EXPECT_STREQ("2147483646", StringHelpers::parseNumber<unsigned int>(2147483646).c_str());
	EXPECT_STREQ("2147483647", StringHelpers::parseNumber<unsigned int>(2147483647).c_str());
	EXPECT_STREQ("2147483648", StringHelpers::parseNumber<unsigned int>(2147483648).c_str());
	EXPECT_STREQ("2147483649", StringHelpers::parseNumber<unsigned int>(2147483649).c_str());
	EXPECT_STREQ("2147483650", StringHelpers::parseNumber<unsigned int>(2147483650).c_str());
}

TEST(StringHelpersTests, FromUnsignedIntTest02) {
	unsigned int i = 2147483645;
	EXPECT_STREQ("2147483645", StringHelpers::parseNumber<unsigned int>(i++).c_str());
	EXPECT_STREQ("2147483646", StringHelpers::parseNumber<unsigned int>(i++).c_str());
	EXPECT_STREQ("2147483647", StringHelpers::parseNumber<unsigned int>(i++).c_str());
	EXPECT_STREQ("2147483648", StringHelpers::parseNumber<unsigned int>(i++).c_str());
	EXPECT_STREQ("2147483649", StringHelpers::parseNumber<unsigned int>(i++).c_str());
	EXPECT_STREQ("2147483650", StringHelpers::parseNumber<unsigned int>(i++).c_str());
}

TEST(StringHelpersTests, FromLongTest01) {
	EXPECT_STREQ("2147483645", StringHelpers::parseNumber<long>(2147483645).c_str());
	EXPECT_STREQ("2147483646", StringHelpers::parseNumber<long>(2147483646).c_str());
	EXPECT_STREQ("2147483647", StringHelpers::parseNumber<long>(2147483647).c_str());
	EXPECT_STREQ("-2147483648", StringHelpers::parseNumber<long>(2147483648).c_str());
	EXPECT_STREQ("-2147483647", StringHelpers::parseNumber<long>(2147483649).c_str());
	EXPECT_STREQ("-2147483646", StringHelpers::parseNumber<long>(2147483650).c_str());
}

TEST(StringHelpersTests, FromLongTest02) {
	long i = 2147483645;
	EXPECT_STREQ("2147483645", StringHelpers::parseNumber<long>(i++).c_str());
	EXPECT_STREQ("2147483646", StringHelpers::parseNumber<long>(i++).c_str());
	EXPECT_STREQ("2147483647", StringHelpers::parseNumber<long>(i++).c_str());
	EXPECT_STREQ("-2147483648", StringHelpers::parseNumber<long>(i++).c_str());
	EXPECT_STREQ("-2147483647", StringHelpers::parseNumber<long>(i++).c_str());
	EXPECT_STREQ("-2147483646", StringHelpers::parseNumber<long>(i++).c_str());
}

TEST(StringHelpersTests, FromUnsignedLongTest01) {
	EXPECT_STREQ("2147483645", StringHelpers::parseNumber<unsigned long>(2147483645).c_str());
	EXPECT_STREQ("2147483646", StringHelpers::parseNumber<unsigned long>(2147483646).c_str());
	EXPECT_STREQ("2147483647", StringHelpers::parseNumber<unsigned long>(2147483647).c_str());
	EXPECT_STREQ("2147483648", StringHelpers::parseNumber<unsigned long>(2147483648).c_str());
	EXPECT_STREQ("2147483649", StringHelpers::parseNumber<unsigned long>(2147483649).c_str());
	EXPECT_STREQ("2147483650", StringHelpers::parseNumber<unsigned long>(2147483650).c_str());
}

TEST(StringHelpersTests, FromUnsignedLongTest02) {
	unsigned long i = 2147483645;
	EXPECT_STREQ("2147483645", StringHelpers::parseNumber<unsigned long>(i++).c_str());
	EXPECT_STREQ("2147483646", StringHelpers::parseNumber<unsigned long>(i++).c_str());
	EXPECT_STREQ("2147483647", StringHelpers::parseNumber<unsigned long>(i++).c_str());
	EXPECT_STREQ("2147483648", StringHelpers::parseNumber<unsigned long>(i++).c_str());
	EXPECT_STREQ("2147483649", StringHelpers::parseNumber<unsigned long>(i++).c_str());
	EXPECT_STREQ("2147483650", StringHelpers::parseNumber<unsigned long>(i++).c_str());
}

TEST(StringHelpersTests, FromUnsignedDoubleTest02) {
	EXPECT_STREQ("36112.000000", StringHelpers::parseNumber<double>((double)36112).c_str());
	EXPECT_STREQ("36112.000000", StringHelpers::parseNumber<double>(36112.000000000000).c_str());
}

TEST(StringHelpersTests, ToLower01) {
	string s("qWeRtY");
	EXPECT_STREQ("qwerty", StringHelpers::lower(s).c_str());
}

TEST(StringHelpersTests, KeyValPairParserTest01) {
	std::pair<std::string, std::string> kvpair = StringHelpers::parseKeyValuePair("key=val");
	EXPECT_STREQ("key", kvpair.first.c_str());
	EXPECT_STREQ("val", kvpair.second.c_str());
}

TEST(StringHelpersTests, KeyValPairParserMissingKeyTest01) {
	try {
		std::pair<std::string, std::string> kvpair = StringHelpers::parseKeyValuePair("=value");
		FAIL() << "ParseException expected";
	} catch (ParseException e) {
	}
}

TEST(StringHelpersTests, KeyValPairParserMissingDelimTest01) {
	try {
		std::pair<std::string, std::string> kvpair = StringHelpers::parseKeyValuePair("key");
		FAIL() << "ParseException expected";
	} catch (ParseException e) {
	}
}

TEST(StringHelpersTests, KeyValPairParserNonDefaultParserTest) {
	std::pair<std::string, std::string> kvpair = StringHelpers::parseKeyValuePair("key-val", '-');
	EXPECT_STREQ("key", kvpair.first.c_str());
	EXPECT_STREQ("val", kvpair.second.c_str());
}

TEST(StringHelpersTests, KeyValPairParserMissingKeyTest02) {
	try {
		std::pair<std::string, std::string> kvpair = StringHelpers::parseKeyValuePair("-value", '-');
		FAIL() << "ParseException expected";
	} catch (ParseException e) {
	}
}

TEST(StringHelpersTests, KeyValPairParserMissingDelimTest02) {
	try {
		std::pair<std::string, std::string> kvpair = StringHelpers::parseKeyValuePair("key=", '-');
		FAIL() << "ParseException expected";
	} catch (ParseException e) {
	}
}

TEST(StringHelpersTests, KeyValPairsTest01) {
	try {
		vector<std::pair<std::string, std::string>> kvpairs = StringHelpers::parseKeyValuePairs("key0=val0;key1=val1;key2=val2");
		ASSERT_EQ(3, kvpairs.size());
		EXPECT_STREQ("key0", kvpairs[0].first.c_str());
		EXPECT_STREQ("val0", kvpairs[0].second.c_str());
		EXPECT_STREQ("key1", kvpairs[1].first.c_str());
		EXPECT_STREQ("val1", kvpairs[1].second.c_str());
		EXPECT_STREQ("key2", kvpairs[2].first.c_str());
		EXPECT_STREQ("val2", kvpairs[2].second.c_str());

	} catch (ParseException e) {
		FAIL() << "Unexpected ParseException";
	}
}

TEST(StringHelpersTests, KeyValPairsTest02) {
	try {
		vector<std::pair<std::string, std::string>> kvpairs = StringHelpers::parseKeyValuePairs("=val0;key1=val1;key2=val2");
		//		ASSERT_EQ( 2, kvpairs.size() );
		//		EXPECT_STREQ( "key1", kvpairs[0].first.c_str() );
		//		EXPECT_STREQ( "val1", kvpairs[0].second.c_str() );
		//		EXPECT_STREQ( "key2", kvpairs[1].first.c_str() );
		//		EXPECT_STREQ( "val2", kvpairs[1].second.c_str() );
		FAIL() << "Unexpected ParseException";

	} catch (ParseException e) {
	}
}

TEST(StringHelpersTests, KeyValPairsTest03) {
	try {
		vector<std::pair<std::string, std::string>> kvpairs = StringHelpers::parseKeyValuePairs("=val0;key1=val1;key2=val2");
		FAIL() << "Expected ParseException";
	} catch (ParseException e) {
		int n = 0;
		if (n == 1) {
		}
	}
}

TEST(StringHelpersTests, EndsWithTest01) {
	EXPECT_TRUE(StringHelpers::endsWith(".pak", "test.pak"));
	EXPECT_FALSE(StringHelpers::endsWith(".pak", "test.txt"));
}

// Bug detected in StringHelpers::endsWith()
// endsWith("\\", "C:\\Users\\dev\\AppData\\Roaming" ) returns true
//
//TEST(StringHelpersTests, EndsWithTest02) {
//	EXPECT_TRUE(StringHelpers::endsWith("\\", "C:\\Users\\dev\\AppData\\Roaming\\"));
//	EXPECT_FALSE(StringHelpers::endsWith("\\", "C:\\Users\\dev\\AppData\\Roaming"));
//}

// Issue is that leading char of every needle was never being
// tested, hence the previous test.  This manifests as 1)
// single character needles always indicating true.
TEST(StringHelpersTests, EndsWithTest02) {
	EXPECT_TRUE(StringHelpers::endsWith("d", "abcd"));
	EXPECT_FALSE(StringHelpers::endsWith("d", "abce"));
}

// And it manifests as 2, a mismatch in the first character of the
// needle is not detected hence generating a false positive.
TEST(StringHelpersTests, EndsWithTest03) {
	EXPECT_TRUE(StringHelpers::endsWith("cd", "abcd"));
	EXPECT_FALSE(StringHelpers::endsWith("_d", "abcd"));
}

TEST(StringHelpersTests, EndsWithTest04) {
	EXPECT_TRUE(StringHelpers::endsWith("abcd", "abcd"));
	EXPECT_FALSE(StringHelpers::endsWith("abce", "abcd"));
}

TEST(StringHelpersTests, TestExplosion01) {
	// test possible issue with explode
	string t("1;;;");
	vector<string> v;
	StringHelpers::explode(v, t, ";");
	ASSERT_EQ(3, v.size());
}

TEST(StringHelpersTests, TestExplosion02) {
	// test possible issue with explode
	string t(";;;");
	vector<string> v;
	StringHelpers::explode(v, t, ";");
	ASSERT_EQ(3, v.size());
	ASSERT_STREQ("", v[0].c_str());
	ASSERT_STREQ("", v[1].c_str());
	ASSERT_STREQ("", v[2].c_str());
}

TEST(StringHelpersTests, TestExplosion03) {
	// test possible issue with explode
	string t(";;;1");
	vector<string> v;
	StringHelpers::explode(v, t, ";");
	ASSERT_EQ(4, v.size());
	ASSERT_STREQ("", v[0].c_str());
	ASSERT_STREQ("", v[1].c_str());
	ASSERT_STREQ("", v[2].c_str());
	ASSERT_STREQ("1", v[3].c_str());
}

TEST(StringHelpersTests, TestImplosion01) {
	std::vector<string> v;
	v.push_back("one");
	v.push_back("two");
	v.push_back("three");
	v.push_back("four");
	std::string imploded = StringHelpers::implode(v);
	ASSERT_STREQ("one,two,three,four", imploded.c_str());
}

TEST(StringHelpersTests, TestImplosion02) {
	std::vector<string> v;
	v.push_back("one");
	v.push_back("two");
	v.push_back("three");
	v.push_back("four");
	std::string target;
	StringHelpers::implode(v, target);
	ASSERT_STREQ("one,two,three,four", target.c_str());
}

