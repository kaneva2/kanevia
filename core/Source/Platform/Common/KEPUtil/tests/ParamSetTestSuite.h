///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
// todo:
// Test that PluginHost::stopPlugin(name) throws/doesn't throw exception correctly.
// Test the use ACTPtr to optionally block until pluginhost shutdown is complete.
//

#include "common/keputil/ParamString.h"

/**
 * Test 
 */
TEST(ParamSetTestSuite, ParamSetTest01) {
	EXPECT_STREQ("Here is value1 and here is value2", ParamSet(Properties().set("key1", "value1").set(std::string("key2"), std::string("value2")), "Here is %(key1) and here is %(key2)").toString().c_str());
}

/**
 * Test the ability to change the markers after construction.
 */
TEST(ParamSetTestSuite, ParamSetTest02) {
	EXPECT_STREQ("Here is value1 and here is value2", ParamSet(Properties().set("key1", "value1").set(std::string("key2"), std::string("value2")), "Here is ${key1} and here is ${key2}").setMarkers("${}").toString().c_str());
}

/**
 * Test scenario with variable parameters and constant format.
 */
TEST(ParamSetTestSuite, ParamSetTest03) {
	Properties props;
	props.set("key1", "value1")
		.set(std::string("key2"), std::string("value2"));

	ParamSet ps(props, "Here is %(key1) and here is %(key2)");
	EXPECT_STREQ("Here is value1 and here is value2", ps.toString().c_str());

	ps.set("key1", "...hey this isn't value1")
		.set("key2", "...and this isn't value2");

	EXPECT_STREQ("Here is ...hey this isn't value1 and here is ...and this isn't value2", ps.toString().c_str());
}

TEST(ParamSetTestSuite, ParamSetTest04) {
	Properties props;
	props.set(string("key1"), string("value1"))
		.set(std::string("key2"), std::string("value2"));
	ParamSet ps(props, "Here is %(key1) and here is %(key2)");
	EXPECT_STREQ("Here is value1 and here is value2", ps.toString().c_str());

	ps.setFormat("Here is %(key2) and here is %(key1)");
	EXPECT_STREQ("Here is value2 and here is value1", ps.toString().c_str());
}

TEST(ParamSetTestSuite, ParamEscapeTest) {
	ParamSet ps;
	ps.set("parm", "notparm");
	string s1 = ps.toString("%%(parm)");
	EXPECT_STREQ("%(parm)", ps.toString("%%(parm)").c_str());
}

/**
 * Test scenario with key val pairs as single string.
 */
TEST(ParamSetTestSuite, ParamSetTest05) {
	ParamSet ps("Here is %(key1) and here is %(key2)");
	EXPECT_STREQ("Here is value1 and here is value2", ParamSet("Here is %(key1) and here is %(key2)").set("key1=value1").set("key2 = value2").toString().c_str());
}

TEST(PropertiesTest, SetTest01) {
	Properties props;
	props.set(StringHelpers::parseKeyValuePairs("key0=val0;key1=val1;key2=val2"));
	EXPECT_STREQ("val0", props["key0"].c_str());
	EXPECT_STREQ("val0", props["key0"].c_str());
	EXPECT_STREQ("val0", props["key0"].c_str());
}

