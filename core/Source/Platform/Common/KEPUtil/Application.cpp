///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Application.h"
#include "Algorithms.h"

namespace KEP {

Singleton<Application> Application::_singleton;
Application& Application::singleton() {
	return *_singleton;
}

size_t Application::privateBytes() {
	return OS::privateBytes();
}

std::string Application::version() {
	return Algorithms::GetModuleVersion();
}

std::string Application::sessionUUId() {
	return _sessionUUID;
}

} // namespace KEP