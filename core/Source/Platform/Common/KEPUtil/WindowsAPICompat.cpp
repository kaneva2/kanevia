///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "WindowsAPICompat.h"

namespace KEP {

WindowsAPICompat::GetPhysicallyInstalledSystemMemoryFunc WindowsAPICompat::GetPhysicallyInstalledSystemMemory = nullptr;

// Cheap and dirty static init - API function pointers are *NOT* guaranteed to be available during static initialization phase.
bool WindowsAPICompat::m_inited = WindowsAPICompat::init();

bool WindowsAPICompat::init() {
	GetPhysicallyInstalledSystemMemory = reinterpret_cast<GetPhysicallyInstalledSystemMemoryFunc>(::GetProcAddress(::GetModuleHandleA("kernel32.dll"), "GetPhysicallyInstalledSystemMemory"));
	return true;
}

} // namespace KEP
