///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

class Sequence {
public:
	Sequence(unsigned long initial = 0,
		unsigned long max = ULONG_MAX) :
			_resource(),
			_count(initial), _initial(initial), _max(max) {}

	Sequence(const Sequence& other) :
			_resource(), _count(other._initial), _initial(other._initial), _max(other._max) {}

	unsigned long nextVal() {
		std::lock_guard<fast_recursive_mutex> lock(_resource);
		return _count++;
	}

	unsigned long currVal() {
		std::lock_guard<fast_recursive_mutex> lock(_resource);
		return _count;
	}

private:
	fast_recursive_mutex _resource;
	unsigned long _count;
	unsigned long _max;
	unsigned long _initial;
};
