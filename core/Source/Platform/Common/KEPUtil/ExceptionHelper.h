///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "Core/Util/ChainedException.h"

/**
 * Class for performing standard operations with logging.  This class exists primarily to
 * avoid coupling of ChainedException with Log4CPlus.
 * @see ExceptionHelperTests for usage examples.
 */
class ExceptionHelper {
public:
	/**
	 * Report exception to the logging system as an ERROR level log entry and
	 * additionally output to the provided ostream reference.
	 * 
	 * @param	exc			Reference to the exception to be reported.
	 * @param	logger		Reference to target logger
	 * @param   outStream	Reference to ostream to which exception is streamed.
	 *						Defaults to cerr
	 */
	static void ReportErrorException(const ChainedException& exc, const log4cplus::Logger& logger, ostream& outStream = cerr) {
		std::string excStr(exc.str());
		outStream << excStr << endl;
		LOG4CPLUS_ERROR(logger, exc.str());
	}

	/**
	 * Report exception to the logging system as a FATAL level log entry and
	 * additionally output to the provided ostream reference.
	 * 
	 * @param	exc			Reference to the exception to be reported.
	 * @param	logger		Reference to target logger
	 * @param   outStream	Reference to ostream to which exception is streamed.
	 *						Defaults to cerr
	 */
	static void ReportFatalException(const ChainedException& exc, const log4cplus::Logger& logger, ostream& outStream = cerr) {
		std::string excStr(exc.str());
		outStream << excStr << endl;
		LOG4CPLUS_FATAL(logger, exc.str());
	}

	/**
	 * Report exception to the logging system as an WARN level log entry and
	 * additionally output to the provided ostream reference.
	 * 
	 * @param	exc			Reference to the exception to be reported.
	 * @param	logger		Reference to target logger
	 * @param   outStream	Reference to ostream to which exception is streamed.
	 *						Defaults to cerr
	 */
	static void ReportWarningException(const ChainedException& exc, const log4cplus::Logger& logger, ostream& outStream = cerr) {
		std::string excStr(exc.str());
		outStream << excStr << endl;
		LOG4CPLUS_WARN(logger, exc.str());
	}
};
