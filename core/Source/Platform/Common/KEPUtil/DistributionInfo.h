///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

__declspec(dllimport)
	BOOL WriteDistributionInfo(const std::string& distroInfoPath, const std::string& serverDir, const std::string& sourceDir, const std::string& patchDir,
		const std::string& appServerDir, const std::string& appSourceDir, const std::string& appPatchDir, const std::string& appPatchUrl);

__declspec(dllimport)
	BOOL ReadDistributionInfo(const std::string& distroInfoPath, std::string& serverDir, std::string& sourceDir, std::string& patchDir,
		std::string& appServerDir, std::string& appSourceDir, std::string& appPatchDir, std::string& appPatchUrl, std::string& addServerUrl);

#define DISTRO_INFO_FILENAME "DistributionInfo.xml"

} // namespace KEP