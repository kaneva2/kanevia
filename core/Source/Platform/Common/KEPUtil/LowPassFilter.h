///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

// YC 01/2015
// Simple LT1 low-pass filter class to suppress data jittery such as collision states.

#pragma once

#include <queue>
#include <list>

// Forward declarations
template <typename SampleType, typename CoeffType, int NumInSamples, int NumOutSamples>
class LowPassFilter;

template <typename SampleType, typename CoeffType, int NumInSamples, int NumOutSamples>
SampleType DifferenceEquationCalculator(const LowPassFilter<SampleType, CoeffType, NumInSamples, NumOutSamples>& filter, const list<SampleType>& inSeries);

// Low pass filter class
template <typename SampleType, typename CoeffType, int NumInSamples, int NumOutSamples>
class LowPassFilter {
public:
	struct Coefficients {
		CoeffType data[NumInSamples];
	};

	class State {
		typedef LowPassFilter<SampleType, CoeffType, NumInSamples, NumOutSamples> FilterType;

	public:
		State(const FilterType& filter) :
				m_filter(filter) {
			for (int i = 0; i < NumInSamples; i++) {
				m_inSeries.push_front(filter.zeroValue());
			}
			for (int i = 0; i < NumOutSamples; i++) {
				m_outSeries.push_front(filter.zeroValue());
			}
		}

		// Push a new value into in series
		SampleType push(SampleType val) {
			m_inSeries.pop_back();
			m_inSeries.push_front(val);
			m_outSeries.pop_back();
			SampleType outVal = DifferenceEquationCalculator(m_filter, m_inSeries);
			m_outSeries.push_front(outVal);
			return outVal;
		}

		// Return current or past values (index 0: current value, 1: last value, 2: ... )
		SampleType get(int index) const {
			int i = 0;
			for (auto it = m_outSeries.begin(); it != m_outSeries.end(); ++it, ++i) {
				if (i == index)
					return *it;
			}

			return m_filter.zeroValue();
		}

		// Return current or past raw values (index 0: current value, 1: last value, 2: ... )
		SampleType getRaw(int index) const {
			int i = 0;
			for (auto it = m_inSeries.begin(); it != m_inSeries.end(); ++it, ++i) {
				if (i == index)
					return *it;
			}

			return m_filter.zeroValue();
		}

	private:
		list<SampleType> m_inSeries;
		list<SampleType> m_outSeries;
		const FilterType& m_filter;
	};

	LowPassFilter(const Coefficients& coeff, const CoeffType& zeroCoeff, const SampleType& zeroValue) :
			m_coefficients(coeff),
			m_zeroCoeff(zeroCoeff),
			m_zeroValue(zeroValue) {
	}

	State newState() const {
		return State(*this);
	}

	SampleType zeroValue() const { return m_zeroValue; }
	CoeffType getCoefficient(int index) const {
		if (index < NumInSamples)
			return m_coefficients.data[index];
		return m_zeroCoeff;
	}

private:
	Coefficients m_coefficients;
	CoeffType m_zeroCoeff;
	SampleType m_zeroValue;
};

// Generic equation calculator
template <typename SampleType, typename CoeffType, int NumInSamples, int NumOutSamples>
inline SampleType DifferenceEquationCalculator(const LowPassFilter<SampleType, CoeffType, NumInSamples, NumOutSamples>& filter, const list<SampleType>& inSeries) {
	auto it = inSeries.begin();
	SampleType outVal = filter.zeroValue();
	for (int i = 0; i < NumInSamples && it != inSeries.end(); ++i, ++it) {
		SampleType inVal = *it;
		outVal += filter.getCoefficient(i) * inVal;
	}

	return outVal;
}

// Calculator specialized for bool values
template <int NumInSamples, int NumOutSamples>
inline bool DifferenceEquationCalculator(const LowPassFilter<bool, bool, NumInSamples, NumOutSamples>& filter, const list<bool>& inSeries) {
	auto it = inSeries.begin();
	bool outVal = filter.zeroValue();
	for (int i = 0; i < NumInSamples && it != inSeries.end(); ++i, ++it) {
		bool inVal = *it;
		outVal = outVal || (filter.getCoefficient(i) && inVal);
	}

	return outVal;
}
