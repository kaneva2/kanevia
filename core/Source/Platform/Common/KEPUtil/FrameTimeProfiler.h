///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "KEP/Util/KEPUtil.h"
#include "Core/Util/HighPrecisionTime.h"
#include <assert.h>
#include <map>
#include <string>
#include <stack>
#include <vector>

namespace KEP {

class FrameTimeProfiler {
public:
	enum { INVALID_CONTEXT_ID = 0xffffffff };

	FrameTimeProfiler() :
			m_threadId(0), m_frameStartTime(0) { reset(); }

	// Push profiling context and return a timing context ID for current context
	size_t pushContext(const std::string& context) {
		assert(m_threadId != 0 && GetCurrentThreadId() == m_threadId);
		if (m_threadId == 0 || GetCurrentThreadId() != m_threadId) {
			// Frame not initialized, or calling from a different thread
			return INVALID_CONTEXT_ID;
		}

		// Nested context
		std::string currContext;
		if (!m_contextStack.empty()) {
			currContext = m_contextStack.top() + "/" + context;
		} else {
			currContext = context;
		}

		m_contextStack.push(currContext);

		// Get context ID
		size_t contextId;
		auto itr = m_contextIDs.find(currContext);
		if (itr == m_contextIDs.end()) {
			// New context
			m_contexts.push_back(currContext);
			contextId = m_contexts.size() - 1;
			m_contextIDs.insert(std::make_pair(currContext, contextId));
			m_durationsByContext.resize(m_contexts.size());
			m_reports.back().resize(m_contexts.size());
		} else {
			// Known context
			contextId = itr->second;
		}

		return contextId;
	}

	// Pop profiling context
	void popContext() {
		assert(m_threadId != 0 && GetCurrentThreadId() == m_threadId);
		if (m_threadId == 0 || GetCurrentThreadId() != m_threadId) {
			// Frame not initialized, or calling from a different thread
			return;
		}

		assert(!m_contextStack.empty());
		if (!m_contextStack.empty()) {
			m_contextStack.pop();
		}
	}

	// Call to reset all reports
	void reset() {
		// Remove all existing reports
		m_reports.clear();
		// Create an empty new report and reset pending states
		rollover();
	}

	// Call to roll over existing report and append a new report
	void rollover() {
		m_reports.push_back(Report());
		m_reports.back().resize(m_contexts.size());

		// Reset pending frame
		m_frameStartTime = 0;
		m_durationsByContext.clear();
		m_durationsByContext.resize(m_contexts.size());
	}

	// Call to log time
	void time(size_t contextId, TimeMs msec) {
		assert(m_threadId != 0 && GetCurrentThreadId() == m_threadId);
		if (m_threadId == 0 || GetCurrentThreadId() != m_threadId) {
			// Frame not initialized, or calling from a different thread
			return;
		}

		assert(m_durationsByContext.size() > contextId);
		m_durationsByContext[contextId] += msec; // Accumulate for multiple logs under same context ID
	}

	// Call to mark frame boundary
	void frame() {
		if (m_threadId == 0) {
			m_threadId = GetCurrentThreadId();
		}

		assert(GetCurrentThreadId() == m_threadId);

		if (m_frameStartTime != 0) {
			// Save statistics from last frame to current report
			m_reports.back().update(fTime::ElapsedMs(m_frameStartTime), m_durationsByContext);
		}

		// New frame
		m_frameStartTime = fTime::TimeMs();
		m_durationsByContext.clear();
		m_durationsByContext.resize(m_contexts.size());
	}

	class Report {
	private:
		time_t m_timestamp;
		size_t m_numFrames;
		TimeMs m_totalDuration;
		std::vector<TimeMs> m_accumulatedDurationsByContext;

	public:
		Report() :
				m_timestamp(0), m_numFrames(0), m_totalDuration(0) {
		}

		// Data logging
		void resize(size_t newSize) { m_accumulatedDurationsByContext.resize(newSize); }

		void update(TimeMs frameDuration, const std::vector<TimeMs>& durationByContexts) {
			if (m_timestamp == 0) {
				// Report timestamp
				::time(&m_timestamp);
			}

			assert(durationByContexts.size() == m_accumulatedDurationsByContext.size());

			// Accumulate
			m_numFrames++;
			m_totalDuration += frameDuration;
			for (size_t contextId = 0; contextId < durationByContexts.size(); contextId++) {
				m_accumulatedDurationsByContext[contextId] += durationByContexts[contextId];
			}
		}

		// For Reporting
		size_t getFrameCount() const { return m_numFrames; }

		TimeMs getTotalDuration() const { return m_totalDuration; }

		const std::vector<TimeMs>& getAccumulatedDurationsByContext() const { return m_accumulatedDurationsByContext; }

		const time_t& getTimestamp() const { return m_timestamp; }
	};

	// Call to get current report
	const std::vector<Report>& reports() const { return m_reports; }

	// Array of contexts
	const std::vector<std::string>& contexts() const { return m_contexts; }

private:
	std::vector<std::string> m_contexts;
	std::map<std::string, size_t> m_contextIDs;

	unsigned m_threadId;
	std::vector<Report> m_reports;
	std::stack<std::string> m_contextStack;

	// Pending statistics from current frame
	TimeMs m_frameStartTime;
	std::vector<TimeMs> m_durationsByContext;
};

// Establish a nested profiling context
class FrameTimeNestedContext {
	template <typename StringType>
	void PushContext(const StringType& context) {
		m_startTime = fTime::TimeMs();
		m_contextId = m_profiler->pushContext(context);
	}
	void PopContext() {
		if (m_contextId != FrameTimeProfiler::INVALID_CONTEXT_ID) {
			// Log elapsed time
			m_profiler->time(m_contextId, fTime::ElapsedMs(m_startTime));
			m_profiler->popContext();
		}
	}

public:
	template <typename StringType>
	FrameTimeNestedContext(FrameTimeProfiler* profiler, const StringType& context) {
		m_profiler = profiler;
		if (m_profiler) {
			// Profiler is enabled: track time
			PushContext(context);
		}
	}

	~FrameTimeNestedContext() {
		if (m_profiler)
			PopContext();
	}

private:
	FrameTimeProfiler* m_profiler;
	size_t m_contextId;
	TimeMs m_startTime;
};

KEPUTIL_EXPORT FrameTimeProfiler* GetFrameTimeProfiler(); // Singleton instance

} // namespace KEP
