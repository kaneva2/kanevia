///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <fstream>
#include "ExtendedConfig.h"
#include <TinyXML/tinyxml.h>
#include "Core/Util/ChainedException.h"
#include "Common/KEPUtil/URL.h"
#include "Common/KEPUtil/Algorithms.h"
#include "Tools/HttpClient/GetBrowserPage.h"

using namespace std;

#include "common/include/LogHelper.h"
static LogInstance("Instance");

// TODO: Get declaration straightened out.  currently the declaration is set in the constructor.  However, if we parse
// a document we end up with two declarations

namespace KEP {

TIXMLConfig::Item TIXMLConfig::NullItem;
const TIXMLConfig::Item& TIXMLConfig::nullItem() {
	return NullItem;
}

/**
 * The decision was made to not implement the collection mechanisms at the adapter layer, but rather forward such calls to the TiXml 
 * implementation.  What this means is that TIXMLConfig does NOT manage the linkages between the various element of the document
 * tree.  TIXMLConfig maintain collections of child elements or references to parents etc.  In fact, TIXMLConfig::Item objects aren't
 * even collected.
 *
 * With the addition of support of external entities, we need to be able to associate a context with each of the document items.
 * Specifically,  elements of a tree need to know where the file was located.  This will be used to resolve relative URIs to
 * subsequent external entities.
 *
 * Note that this infers coupling with some of the file operations.  This mod must leave TIXMLConfig decoupled from the file system.
 * 
 *    TiXmlElement 
 *
 * 1) Add fixtures to the TiXml components using the userData mechanism insuring that the memory associated with the fixtures is
 *    properly collected.
 * 
 */
class Fixture {
private:
	// Only the factory function can construct a fixture
	Fixture() :
			_url() {
	}

	Fixture(const Fixture& other) :
			_url(other._url) {
	}

public:
	static std::shared_ptr<Fixture> CreateFixtureRef(const URL& url);

	Fixture(URL context) :
			_url(context) {}

	Fixture& operator=(const Fixture& rhs) {
		_url = rhs._url;
		return *this;
	}

	bool operator==(const Fixture& rhs) {
		if (this == &rhs)
			return true;
		return false;
	}

	virtual ~Fixture() {
	}

	URL _url;
};

typedef std::shared_ptr<Fixture> FixtureRef;

void ClearUserData(TiXmlNode* item);
void ClearAndSetUserData(TiXmlNode* item, FixtureRef reference);
string NodeDebugString(TiXmlNode* node);

class FixtureRefHolder {
public:
	FixtureRefHolder(const FixtureRef& fixture) :
			_fixtureRef(fixture) {
	}

	virtual ~FixtureRefHolder() {
	}

	FixtureRef ref() const { return _fixtureRef; }

	FixtureRefHolder& setRef(FixtureRef ref) {
		_fixtureRef = ref;
		return *this;
	}

	string debugString() {
		stringstream ss;
		ss << _fixtureRef.operator->();
		return ss.str();
	}

private:
	FixtureRef _fixtureRef;
};

/**
 * Implementation of ItemVisitor that is used to load external references.
 * Modify to manage a stack of Fixtures.
 * 
 */
class FixtureCleaner : public TIXMLConfig::ItemVisitor {
private:
public:
	FixtureCleaner() {}
	FixtureCleaner& visit(TIXMLConfig::Item& i) {
		ClearUserData(i._internal); // Free any user data
		i.traverse(*this); // And recurse
		return *this;
	}
};

/**
 * To be used whenever we append an item from one list into another list.  append()
 * performs a clone of the node being appended.  The TiXmlBase::Clone() method clones
 * the entire tree, however, it brings user data over wholesale, meaning that after
 * the clone we have two pointers to the same FixtureRefHolder.  When either the source
 * or target document is destructed it will delete the FixtureRefHolder (and consequently
 * the FixtureRef) leaving the other document holding an invalid pointer in the user data.
 *
 * This visitor, unlike the Cleaner visitor, does not attempt to free the holder, but
 * rather simply sets the userdata to 0.
 */
class UnlinkFixture : public TIXMLConfig::ItemVisitor {
	UnlinkFixture& visit(TIXMLConfig::Item& i) {
		i._internal->SetUserData(0);
		i.traverse(*this); // And recurse
		return *this;
	}
};

// TiXmlVisitor implementation that that associates each TiXmlElement with
// a TIXMLElementFixture
//
class FixtureFixer : public TIXMLConfig::ItemVisitor {
public:
	FixtureFixer() :
			_reference(Fixture::CreateFixtureRef(URL())) {
	}

	FixtureFixer(FixtureRef fixture) :
			_reference(fixture) {
	}

	~FixtureFixer() {
	}

	FixtureFixer& visit(TIXMLConfig::Item& item) {
		ClearAndSetUserData(item._internal, _reference);
		item.traverse(*this);
		return *this;
	}

	FixtureRef _reference;
};

class DebugVisitor : public TIXMLConfig::ItemVisitor {
private:
	int level;

public:
	DebugVisitor() :
			level(0) {
	}

	DebugVisitor& visit(TIXMLConfig::Item& i) {
		i.traverse(*this); // And recurse
		return *this;
	}
};

void SetContext(TIXMLConfig::Item& ele, FixtureRef fixtureref) {
	ClearAndSetUserData(ele._internal, fixtureref);
	ele.traverse(FixtureFixer(fixtureref));
}

// allow calling SetContext with temporaries.
void SetContext(TIXMLConfig::Item&& ele, FixtureRef fixtureref) {
	return SetContext(ele, fixtureref);
}

/**
 * Our version of a TiXmlVisitor implemented as a TiXmlPrinter derivative.
 */
class _declspec(dllexport) KTiXmlPrinter : public TiXmlPrinter {
public:
	KTiXmlPrinter();
	virtual bool VisitEnter(const TiXmlDocument& doc);
	virtual bool VisitExit(const TiXmlDocument& doc);
	virtual bool VisitEnter(const TiXmlElement& element, const TiXmlAttribute* firstAttribute);
	virtual bool VisitExit(const TiXmlElement& element);
	virtual bool Visit(const TiXmlDeclaration& declaration);
	virtual bool Visit(const TiXmlText& text);
	virtual bool Visit(const TiXmlComment& comment);
	virtual bool Visit(const TiXmlUnknown& unknown);

	/** Set the indent characters for printing. By default 4 spaces
		but tab (\t) is also useful, or null/empty string for no indentation.
	*/
	void SetIndent(const char* _indent);

	/// Query the indention string.
	const char* Indent();

	/** Set the line breaking string. By default set to newline (\n). 
		Some operating systems prefer other characters, or can be
		set to the null/empty string for no indenation.
	*/
	void SetLineBreak(const char* _lineBreak);

	/// Query the current line breaking string.
	const char* LineBreak();

	/** Switch over to "stream printing" which is the most dense formatting without 
		linebreaks. Common when the XML is needed for network transmission.
	*/
	void SetStreamPrinting();

	/// Return the result.
	const char* CStr();

	/// Return the length of the result string.
	size_t Size();

#ifdef TIXML_USE_STL
	/// Return the result.
	const string& Str();
#endif
private:
	vector<int> _ignoreStack;
};

// Our own specialized printer.
// So that we can get the same save formatting as TiXML, but with the
// ability to not print external references, or more accurately, save
// external entities to the external file from which they were loaded.
//
KTiXmlPrinter::KTiXmlPrinter() :
		TiXmlPrinter() {}

bool KTiXmlPrinter::VisitEnter(const TiXmlDocument& doc) {
	return TiXmlPrinter::VisitEnter(doc);
}

bool KTiXmlPrinter::VisitExit(const TiXmlDocument& doc) {
	return TiXmlPrinter::VisitExit(doc);
}

bool KTiXmlPrinter::VisitEnter(const TiXmlElement& element, const TiXmlAttribute* firstAttribute) {
	if (element.Attribute("ENTITY_URL") != 0) {
		stringstream ss;
		ss << "Ignoring external entity [" << element.ValueStr() << "]";
		// Push it on to our ignore stack.
		_ignoreStack.push_back((int)const_cast<TiXmlElement*>(&element));
		return false;
	}

	return TiXmlPrinter::VisitEnter(element, firstAttribute);
}

bool KTiXmlPrinter::VisitExit(const TiXmlElement& element) {
	if (_ignoreStack.size() <= 0)
		return TiXmlPrinter::VisitExit(element);
	if (_ignoreStack[0] != (int)&element)
		return TiXmlPrinter::VisitExit(element);

	// if we are ignoring this return true (we still want to keep processing subsequent siblings)
	// but don't call the default printer logic (we don't want it streamed to our output
	_ignoreStack.pop_back();
	return true;
}

bool KTiXmlPrinter::Visit(const TiXmlDeclaration& declaration) {
	return TiXmlPrinter::Visit(declaration);
}

bool KTiXmlPrinter::Visit(const TiXmlText& text) {
	return TiXmlPrinter::Visit(text);
}

bool KTiXmlPrinter::Visit(const TiXmlComment& comment) {
	return TiXmlPrinter::Visit(comment);
}

bool KTiXmlPrinter::Visit(const TiXmlUnknown& unknown) {
	return TiXmlPrinter::Visit(unknown);
}

/** Set the indent characters for printing. By default 4 spaces
	but tab (\t) is also useful, or null/empty string for no indentation.
*/
void KTiXmlPrinter::SetIndent(const char* _indent) {
	TiXmlPrinter::SetIndent(_indent);
}

/// Query the indention string.
const char* KTiXmlPrinter::Indent() {
	return TiXmlPrinter::Indent();
}

/** Set the line breaking string. By default set to newline (\n). 
	Some operating systems prefer other characters, or can be
	set to the null/empty string for no indenation.
*/
void KTiXmlPrinter::SetLineBreak(const char* _lineBreak) {
	TiXmlPrinter::SetLineBreak(_lineBreak);
}

/// Query the current line breaking string.
const char* KTiXmlPrinter::LineBreak() {
	return TiXmlPrinter::LineBreak();
}

/** Switch over to "stream printing" which is the most dense formatting without 
	linebreaks. Common when the XML is needed for network transmission.
*/
void KTiXmlPrinter::SetStreamPrinting() {
	TiXmlPrinter::SetStreamPrinting();
}

/// Return the result.
const char* KTiXmlPrinter::CStr() {
	return TiXmlPrinter::CStr();
}

/// Return the length of the result string.
size_t KTiXmlPrinter::Size() {
	return TiXmlPrinter::Size();
}

#ifdef TIXML_USE_STL
/// Return the result.
const string& KTiXmlPrinter::Str() {
	return TiXmlPrinter::Str();
}
#endif

/**
 * Abstraction to facilitate loading configuration data from disparate 
 * sources (e.g. file, http, database etc...)
 *
 * Architectural: This abstraction needs to be extracted out to be used
 * for every case of loading a config file, not just the case of 
 * conflating a configuration.
 */
class ConfigLoader {
public:
	virtual TIXMLConfig& load(const URL url, TIXMLConfig& container) const = 0;
	virtual TiXmlDocument& load(const URL url, TiXmlDocument& container) const = 0;
};

/**
 * Implementation for loading a configuration from a file
 */
class ConfigFileLoader : public ConfigLoader {
public:
	TIXMLConfig& load(const URL url, TIXMLConfig& container) const {
		load(url, container.impl());

		container.conflate(url).loadLogicals();
		return container;
	}

	TiXmlDocument& load(const URL url, TiXmlDocument& container) const {
		container.LoadFile(url.pathString());
		if (container.Error()) {
			stringstream ss;
			ss << "Error loading file "
			   << "[" << url.pathString() << "]. "
			   << "Error [" << container.ErrorId() << "]"
			   << " @ (" << container.ErrorRow() << "," << container.ErrorCol() << ")"
			   << " [" << container.ErrorDesc() << "]";
			throw ConfigurationException(SOURCELOCATION, ss.str());
		}
		return container;
	}
};

TIXMLConfig::Item::Item(TiXmlElement* element) :
		_internal(element), _dom(0), _parent(0) {}

TIXMLConfig::Item::Item(TiXmlDocument& dom, TiXmlElement* element) :
		_internal(element), _dom(&dom), _parent(0) {}

TIXMLConfig::Item::Item() :
		_internal(0), _dom(0), _parent(0) {}

TIXMLConfig::Item::Item(const string& value) :
		_internal(new TiXmlElement(value)), _dom(0), _parent(0) {}

TIXMLConfig::Item::Item(const Item& other) :
		_internal((other._internal > 0) ? new TiXmlElement(*(other._internal)) : 0), _dom(other._dom), _parent(0) {}

TIXMLConfig::Item::~Item() {
	if (_internal == 0)
		return;
	if (_internal->GetDocument() != 0)
		return;
	delete _internal;
}

TIXMLConfig::Item& TIXMLConfig::Item::operator=(const Item& other) {
	_internal = other._internal;
	_dom = other._dom;
	_parent = other._parent;
	return *this;
}

bool TIXMLConfig::Item::operator==(const Item& other) const {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	if (other._internal == _internal)
		return true;

	DWORD dw1 = (DWORD)other._internal;
	DWORD dw2 = (DWORD)_internal;
	DWORD dw3 = dw1 ^ dw2;

	if (dw3 == (DWORD)other._internal)
		return false;
	if (dw3 == (DWORD)_internal)
		return false;

	if (other._internal->ValueStr() == _internal->ValueStr())
		return true;
	return false;
}

string TIXMLConfig::Item::text() const {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	if (_internal == 0)
		throw UninitializedException(SOURCELOCATION, "Item not initialized!");

	// Be prepared for the tag to exist, but contain no data.
	//
	const char* c = _internal->GetText();
	if (c == 0)
		return string();
	string s(c); // = _internal->GetText();

	// Get the ParamSet and reinterpret
	//
	if (_dom == 0)
		return s;
	TIXMLConfig* pconf = (TIXMLConfig*)_dom->GetUserData();
	return (pconf == 0) ? s : pconf->_paramSet.toString(s);
}

bool TIXMLConfig::Item::operator!=(const Item& other) const {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	return !(operator==(other));
}

const string& TIXMLConfig::Item::name() const {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	return _internal->ValueStr();
}

TIXMLConfig::Item TIXMLConfig::Item::firstChild(const string& name) const {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	return TIXMLConfig::Item(_internal->FirstChildElement(name.c_str()));
}

TIXMLConfig::Item TIXMLConfig::Item::nextSibling() const {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	return TIXMLConfig::Item(_internal->NextSiblingElement());
}

string TIXMLConfig::Item::toString() const {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	if (_internal == 0)
		throw UninitializedException(SOURCELOCATION, "Item not initialized!");
	TiXmlDocument* doc = _internal->GetDocument();
	if (doc == 0) {
		// No document.   Simply return the value
		stringstream ss;
		ss << *this;
		return ss.str();
	}

	TIXMLConfig* conf = (TIXMLConfig*)doc->GetUserData();
	if (conf == 0) {
		// hmm....no wrapper.  Perhaps we log and/or throw?
		// No document.   Simply return the value
		stringstream ss;
		ss << *this;
		return ss.str();
	}

	ParamSet params = conf->paramSet();

	stringstream ss;
	ss << *this;
	return params.toString(ss.str());
}

TIXMLConfig::Item& TIXMLConfig::Item::appendItem(TIXMLConfig::Item item) {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	if (_internal == 0)
		throw UninitializedException(SOURCELOCATION, "Item not initialized!");
	if (item._internal == 0)
		throw UninitializedException(SOURCELOCATION, "Item not initialized!");

	TiXmlNode* ele = item._internal->Clone();

	// Okay, we now have a copy of the tree, but unfortunately, they also copied the user data pointers.
	// We have to inject our own fixtures.  For now we simply copy the fixture.  Note that ultimately
	// it might not be a good idea to simply copy it.
	// To Do.  When we actually have context, we'll need to limit out far down we
	// Use the ref ripple ref downward.
	//
	_internal->LinkEndChild(ele);

	// Ripple this item's context through tree
	// First get my context
	//
	Item cloned(ele->ToElement());
	cloned.traverse(UnlinkFixture());
	FixtureRefHolder* holder = (FixtureRefHolder*)_internal->GetUserData();
	cloned.traverse(FixtureFixer(holder->ref()));
	cloned._internal->SetUserData(new FixtureRefHolder(holder->ref()));
	return *this;
}

ostream& operator<<(ostream& out, const TIXMLConfig::Item& item) {
	if (item._internal != 0)
		out << *(item._internal);
	return out;
}

TIXMLConfig&
TIXMLConfig::Item::document() const {
	if (_internal == 0)
		throw ConfigurationException(SOURCELOCATION, "Uninitialized configuration Item!");
	TiXmlDocument* pdoc = _internal->GetDocument();
	if (pdoc == 0)
		throw ConfigurationException(SOURCELOCATION, "Internal structure error!");
	void* udata = pdoc->GetUserData();
	if (pdoc == 0)
		throw ConfigurationException(SOURCELOCATION, "Orphaned item!");
	return *((TIXMLConfig*)udata);
}

string
TIXMLConfig::Item::attribute(const string& name) const {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	const string* ps = _internal->Attribute(name);
	if (ps == 0)
		return "";

	string s(*ps); // = _internal->GetText();

	TiXmlDocument* doc = _internal->GetDocument();
	if (doc == 0)
		return s;

	// Get the ParamSet and reinterpret
	//
	TIXMLConfig* pconf = (TIXMLConfig*)doc->GetUserData();
	return (pconf == 0) ? s : pconf->_paramSet.toString(s);
}

int TIXMLConfig::Item::attribute(const string& name, int dflt) const {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	std::string val = attribute(name);
	if (val.length() == 0)
		return dflt;
	return atoi(val.c_str());
}

std::string
TIXMLConfig::Item::assertAttribute(const string& name) const {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	const std::string* pret = _internal->Attribute(name);
	if (pret == 0 || pret->empty())
		throw ConfigurationException(ErrorSpec(1) << name << " not specified in configuration");
	return std::string(*pret);
}

TIXMLConfig::TIXMLConfig() :
		_dom(new TiXmlDocument()), _mydom(true), _name("unnamed") {
	_dom->SetUserData((void*)this);
}

TIXMLConfig::TIXMLConfig(const string& rootName) :
		_dom(new TiXmlDocument()), _mydom(true), _name("unnamed") {
	_dom->SetUserData((void*)this);

	TiXmlElement* root = new TiXmlElement(rootName);
	FixtureRef fixtureref = Fixture::CreateFixtureRef(URL());
	FixtureRefHolder* refholder = new FixtureRefHolder(fixtureref);
	root->SetUserData(refholder);
	_dom->LinkEndChild(root);
}

TIXMLConfig::TIXMLConfig(const char* rootName) :
		_dom(new TiXmlDocument()), _mydom(true), _name("unnamed") {
	_dom->SetUserData((void*)this);
	if (rootName == 0)
		return;

	_dom->SetUserData((void*)this);
	if (rootName == 0)
		return;

	FixtureRef fixtureref = Fixture::CreateFixtureRef(URL());
	FixtureRefHolder* refholder = new FixtureRefHolder(fixtureref);
	TiXmlElement* root = new TiXmlElement(rootName);
	root->SetUserData(refholder);
	_dom->LinkEndChild(root);
}

TIXMLConfig::TIXMLConfig(TiXmlDocument& kepconfig) :
		_dom(&kepconfig), _mydom(false), _name("unnamed") {
	_dom->SetUserData((void*)this);
	conflate(URL());
	loadLogicals();
}

TIXMLConfig::TIXMLConfig(KEPConfig& kepconfig) :
		_dom(&(kepconfig.impl())), _mydom(false), _name("unnamed") {
	_dom->SetUserData((void*)this);
	conflate(URL());
	loadLogicals();
}

TIXMLConfig::~TIXMLConfig() {
	{
		TIXMLConfig::Item rootItem = root();
		if (rootItem._internal > 0) {
			rootItem.traverse(FixtureCleaner());
			ClearUserData(rootItem._internal);
		}
	}

	// It's absolutely critical that we not have any references to the DOM
	// around when we delete the model, e.g. such that is held by the root
	// TIXMLConfig::Item.  This is because when the item destructs it will
	// attempt to reference the newly deleted dom.  We could use a reference counter
	// but it's easier to just not leave one around at this point.
	// Hence the use of the temporary variable on the traverse() call.
	//
	if (_mydom)
		delete _dom;
}

TIXMLConfig& TIXMLConfig::operator=(const TIXMLConfig& /*other*/) {
	throw UnsupportedException();
}

string
TIXMLConfig::Item::debugString() const {
	return NodeDebugString(_internal);
}

void TIXMLConfig::showDebug() const {
	root().traverse(DebugVisitor());
}

TIXMLConfig::Item&
TIXMLConfig::Item::traverse(Visitor<Item>& visitor) {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	if (_internal == 0)
		return *this;
	for (TiXmlNode* io = _internal->IterateChildren(0); io != 0; io = _internal->IterateChildren(io)) {
		if (io->Type() != TiXmlNode::ELEMENT)
			continue;
		visitor.visit(TIXMLConfig::Item(io->ToElement()));
	}
	return *this;
}

class Conflator : public TIXMLConfig::ItemVisitor {
public:
	Conflator(const URL& context, TIXMLConfig& dom) :
			_context(context), _dom(dom) {
	}

	Conflator& loadEntity(TIXMLConfig::Item& i) {
		const string sURL = i.attribute("url");
		if (sURL.length() == 0) {
			LogWarn("Invalid entity tag.  No url specified");
			return *this;
		}

		try {
			URL url = URL::parse(sURL);
			string t = url.pathString();

			// Is this relative?  If so make it relative to context of parent element
			//
			FileName fname = FileName::parse(url.pathString());
			if (!StringHelpers::startsWith("\\", fname.dir())) {
				// Need to merge the path from our context with the
				// path/filename of the external entity
				//
				// Make this absolute.
				FileName ContextFile = FileName::parse(_context.pathString());
				string PathString = ContextFile.toString(FileName::Drive | FileName::Path);
				url = FileName::parse(PathString + fname.toString());
			}

			if (StringHelpers::lower(url.scheme()) == "file") {
				TiXmlDocument tdoc;
				ConfigFileLoader().load(url, tdoc);
				TiXmlElement* e = tdoc.RootElement();
				e->SetAttribute("ENTITY_URL", url.toString().c_str());

				SetContext(TIXMLConfig::Item(e), Fixture::CreateFixtureRef(url));

				// It's important that add this AFTER to ENTITY ref.  This way it will
				// in turn be conflated and, the ENTITY ref does not get in the way
				// of any queries.
				((TiXmlNode*)i._internal)->Parent()->InsertAfterChild(i._internal, *e);
			} 
		} catch (const URLParseException& /*parseE*/) {
			LogError( "Failed to parse entity url [" << sURL.c_str() << "]");
		} catch (const ConfigurationException configE) {
			LogError("Failed to load external entity at [" << sURL.c_str() << "][" << configE.str() << "]");
		} catch (...) {
			// TODO: log something else went wrong, but heck if I know what it was.
		}

		return *this;
	}

	Conflator& visit(TIXMLConfig::Item& i) {
		if (i._internal->Type() != TiXmlNode::ELEMENT)
			return *this;

		// set the userData of the internal implementation
		// to point to a new fixture
		if (_fixture.operator->() > 0)
			ClearAndSetUserData(i._internal, _fixture);
		if (i.name() == "ENTITY")
			loadEntity(i);
		i.traverse(*this);
		return *this;
	}

private:
	FixtureRef _fixture;
	URL _context;
	TIXMLConfig& _dom;
};

TIXMLConfig&
TIXMLConfig::initialize(TIXMLConfig::Item item) {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	if (item._internal == 0)
		throw UninitializedException();
	_dom->LinkEndChild(item._internal);
	return *this;
}

TIXMLConfig&
TIXMLConfig::initialize(const string& rootName) {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	_dom->SetUserData((void*)this);
	_dom->LinkEndChild(new TiXmlElement(rootName));
	return *this;
}

TIXMLConfig::Item
TIXMLConfig::root() const {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	TiXmlElement* pEle = _dom->RootElement();
	return TIXMLConfig::Item(*_dom, const_cast<TiXmlElement*>(pEle));
}

TIXMLConfig&
TIXMLConfig::parse(const string& content, const URL& context) {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	//    cout << "TIXMLConfig::parse(const string&):" << this << endl;
	_dom->Parse(content.c_str());
	if (_dom->Error()) {
		stringstream ss;
		ss << "Error parsing XML Content [" << _dom->ErrorRow() << "][" << _dom->ErrorCol() << "][" << _dom->ErrorDesc() << "]";
		throw ParseException(SOURCELOCATION, ss.str());
	}

	// Set the context to "." before we conflate
	//
	SetContext(root(), Fixture::CreateFixtureRef(context));
	conflate(context);
	loadLogicals();
	return *this;
}

// Not a big fan of this, but will do for now.  Assume that
// an element on the root called "Logicals" contains
// a definition for a ParamSet
//
TIXMLConfig&
TIXMLConfig::loadLogicals() {
	std::lock_guard<fast_recursive_mutex> lock(_sync);

	//TIXMLConfig::Item logicalsRoot = find("logicals");
	//if (logicalsRoot == TIXMLConfig::Item()) return *this;

	for (TiXmlNode* logicalSet = root()._internal->IterateChildren("logicals", 0); logicalSet != 0; logicalSet = root()._internal->IterateChildren("logicals", logicalSet)) {
		for (TiXmlNode* node = logicalSet->IterateChildren("logical", 0); node != 0; node = logicalSet->IterateChildren("logical", node)) {
			if (node->Type() != TiXmlNode::ELEMENT)
				continue;

			const char* pattern = node->ToElement()->Attribute("pattern");

			string val;
			TiXmlNode* textNode = node->FirstChild();
			if (textNode > 0) {
				TiXmlText* text = textNode->ToText();
				if (text > 0) {
					val = text->ValueStr();
				}
			}
			this->_paramSet.set(pattern, val);
		}
	}
	return *this;
}

//TIXMLConfig&
//TIXMLConfig::conflate( const FileName& context) {
//	std::lock_guard<fast_recursive_mutex> lock(_sync);
//	Conflator conflator(context);
//	root().traverse( conflator );
//	return *this;
//}
TIXMLConfig&
TIXMLConfig::conflate(const URL& context) {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	Conflator conflator(context, *this);
	root().traverse(conflator);
	return *this;
}

TIXMLConfig::Item
TIXMLConfig::find(const string& name) const {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	// allow setting multilevels
	vector<string> tokens;
	STLTokenize(name, tokens, "/");
	TiXmlElement* e = root()._internal;

	// for now, try handling edge case of root
	//
	for (size_t i = 0; i < tokens.size(); i++) {
		if (!e)
			return e;
		e = e->FirstChildElement(tokens[i]);
	}
	return TIXMLConfig::Item(*_dom, e);
}

TiXmlDocument& TIXMLConfig::impl() const {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	if (_dom == 0)
		throw UninitializedException(SOURCELOCATION);
	return *_dom;
}

string TIXMLConfig::deflated() const {
	KTiXmlPrinter p;
	impl().Accept(&p);
	return p.Str();
}

ostream& operator<<(ostream& out, const TIXMLConfig& config) {
	out << config.impl();
	return out;
}

string TIXMLConfig::toString() {
	stringstream ss;
	ss << *this;
	return ss.str();
}

string TIXMLConfig::computeString() {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	if (_dom == 0) {
		// No document.   Simply return the value
		stringstream ss;
		ss << *this;
		return ss.str();
	}

	stringstream ss;
	ss << *this;
	return _paramSet.toString(ss.str());
}

ParamSet& TIXMLConfig::paramSet() {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	return _paramSet;
}

TIXMLConfigHelper::TIXMLConfigHelper() :
		_config(0) {}

TIXMLConfigHelper::TIXMLConfigHelper(TIXMLConfig& config) :
		_config(&config) {}

bool TIXMLConfigHelper::saveAs(const string& fname) {
	if (_config == 0)
		throw UninitializedException(SOURCELOCATION);
	//	std::lock_guard<fast_recursive_mutex> lock(_sync);

	// open ofstream
	ofstream os;
	try {
		os.exceptions(ofstream::failbit | ofstream::badbit);
		os.open(fname, ios_base::trunc | ios_base::out);
		os << _config->deflated();
		os.close();
	} catch (ofstream::failure e) {
		stringstream ss;
		ss << "Failed to open file (truncate|output) [" << fname << "][" << e.code() << "][" << e.what() << "]";
		throw ConfigurationException(SOURCELOCATION, ss.str());
	}
	return true;
}

TIXMLConfig& TIXMLConfigHelper::load(const string& fname) {
	if (_config == 0)
		throw UninitializedException(SOURCELOCATION, "Configuration helper not initialized!");
	ifstream is;
	try {
		is.exceptions(ifstream::failbit | ifstream::badbit);
		is.open(fname, ios_base::in);
		stringstream ss;
		ss << is.rdbuf();
		is.close();
		URL url;
		url.setScheme("file")
			.setPath(fname);
		return _config->parse(ss.str(), url);
	} catch (const ParseException& pexc) {
		ConfigurationException cexc(SOURCELOCATION);
		cexc.chain(pexc);
		throw ConfigurationException(SOURCELOCATION);
	} catch (ifstream::failure e) {
		stringstream ss;
		ss << "Failed to open file (truncate|output) [" << fname << "][" << e.code() << "][" << e.what() << "]";
		throw ConfigurationException(SOURCELOCATION, ss.str());
	}
}

//////////////////// KEPConfigAdapter implementation //////////////////////
//class _declspec(dllexport) KEPConfigAdapter : public IKEPConfig {
//public:
KEPConfigAdapter::KEPConfigAdapter(const char* root) :
		_impl(new TIXMLConfig(root)), _managed(true), _dirty(true), _loaded(false) {}

KEPConfigAdapter::KEPConfigAdapter() :
		_impl(new TIXMLConfig()), _managed(true), _dirty(false), _loaded(false) {}

KEPConfigAdapter::KEPConfigAdapter(TIXMLConfig& impl, bool manage) :
		_impl(&impl), _managed(manage), _dirty(false), _loaded(false) {}

KEPConfigAdapter::KEPConfigAdapter(KEPConfig& impl, bool manage) :
		_impl(new TIXMLConfig(impl)), _managed(manage), _dirty(false), _loaded(false) {}

KEPConfigAdapter::~KEPConfigAdapter() {
	if (_managed)
		delete _impl;
}

bool KEPConfigAdapter::New(const char* rootNodeName,
	const char* rootAttribute /*= 0*/,
	const char* rootAttributeValue /*= 0 */) {
	string s = "<";
	s += rootNodeName;
	s += " />";
	return Parse(s.c_str(), rootNodeName, rootAttribute, rootAttributeValue);
}

/** 
 * NOTE: TIXMLConfig doesn't care about the root name, Hence rootNodeName is ignore and not required.  It sufficient
 * to pass 0.
 */
bool KEPConfigAdapter::Parse(const char* buffer, const char* /* rootNodeName = 0 */
	,
	const char* /* rootAttribute = 0 */
	,
	const char* /* rootAttributeValue = 0 */) {
	_loaded = false;
	try {
		_impl->parse(buffer);
	} catch (...) {
		LogError("FAILED dom.parse()");
		return false;
	}
	_loaded = true;
	return true;
}

bool KEPConfigAdapter::Load(const string& fname, const char* /* rootNodeName			*/
	,
	const char* /* rootAttribute		*/
	,
	const char* /* rootAttributeValue	*/) {
	_loaded = false;
	// We are expecting a filename...
	// Load expects a url.
	// This needs to be reconciled at some point
	stringstream ss;
	ss << "file://" << fname;
	URL url = URL::parse(ss.str());
	try {
		ConfigFileLoader().load(url, *_impl);
	} catch (...) {
		LogError("FAILED ConfigFileLoader.load()");
		return false;
	}
	_loaded = true;
	return true;
}

bool KEPConfigAdapter::Save() {
	_dirty = false;
	return true;
}

bool KEPConfigAdapter::SaveAs(const string& fname) {
	ofstream os;
	try {
		os.exceptions(ofstream::failbit | ofstream::badbit);
		os.open(fname, ios_base::trunc | ios_base::out);
		os << _impl->deflated();
		os.close();
	} catch (ofstream::failure e) {
		stringstream ss;
		ss << "Failed to open file (truncate|output) [" << fname << "][" << e.code() << "][" << e.what() << "]";
		LogError(ss.str());
		return false;
	}
	return true;
}

TIXMLConfig& KEPConfigAdapter::impl() {
	return *_impl;
}

bool KEPConfigAdapter::GetValue(const char* valueName, string& ret, const char* defaultValue) const {
	// Nope, not being initialized is not acceptable.  Returning the default value
	// under these conditions is hiding a problem.
	TIXMLConfig::Item i = _impl->find(valueName); // iXmlHandle node = findNode( m_root, valueName );
	if (i == TIXMLConfig::Item()) {
		ret = defaultValue;
		return false;
	}
	ret = i.text();
	return true;
}

bool KEPConfigAdapter::GetValue(const char* valueName, bool& val, bool defaultValue) const {
	int i;
	if (!GetValue(valueName, i, defaultValue ? 1 : 0)) {
		val = defaultValue;
		return false;
	}
	val = (i != 0);
	return true;
}

bool KEPConfigAdapter::GetValue(const char* valueName, int& ret, int defaultValue) const {
	TIXMLConfig::Item i = _impl->find(valueName);
	if (i == TIXMLConfig::Item()) {
		ret = defaultValue;
		return false;
	}
	string s = i.text();
	if (s.length() == 0) {
		ret = defaultValue;
		return false;
	}

	ret = atoi(s.c_str());
	return true;
}

bool KEPConfigAdapter::GetValue(const char* valueName, double& ret, double defaultValue) const {
	string s;
	bool found = GetValue(valueName, s, "0.0");
	if (found)
		ret = atof(s.c_str());
	else
		ret = defaultValue;
	return found;
}

bool KEPConfigAdapter::ValueExists(const char* valueName) const {
	return _impl->find(valueName) != TIXMLConfig::NullItem;
}

bool KEPConfigAdapter::SetValue(const char* valueName, const char* value, bool append) {
	_dirty = true;

	// allow setting multilevels
	vector<string> tokens;
	STLTokenize(string(valueName), tokens, "/");
	if (tokens.size() == 0)
		return false;

	TiXmlElement* pElement = _impl->impl().RootElement();
	for (size_t i = 0; i < tokens.size(); i++) {
		TiXmlElement* pChild = pElement->FirstChildElement(tokens[i]);
		if (pChild == 0 || (i == tokens.size() - 1 && append)) 
			pChild = pElement->LinkEndChild(new TiXmlElement(tokens[i]))->ToElement();
		pElement = pChild;
	}
	// now add the text node
	TiXmlText* pText = TiXmlHandle(pElement).FirstChild().Text();
	if (!pText)
		pText = pElement->LinkEndChild(new TiXmlText(value))->ToText();
	else
		pText->SetValue(value);
	return pText != nullptr;
}

bool KEPConfigAdapter::SetValue(const char* valueName, int value, bool append) {
	char s[34];
	_itoa_s(value, s, _countof(s), 10);
	return SetValue(valueName, s, append);
}

bool KEPConfigAdapter::SetValue(const char* valueName, bool value, bool append) {
	return SetValue(valueName, value ? "1" : "0", append);
}

bool KEPConfigAdapter::SetValue(const char* valueName, double value, bool append) {
	char s[50];
	_snprintf_s(s, _countof(s), _TRUNCATE, "%f", value);
	return SetValue(valueName, s, append);
}

void ClearUserData(TiXmlNode* item) {
	if (item == 0)
		return; // guard clause.  Can occur when document hasn't been initialized.  I.e. doesn't have a root element.
	void* udata = item->GetUserData();
	if (udata == 0)
		return;
	item->SetUserData(0);
	FixtureRefHolder* pfixture = (FixtureRefHolder*)udata;
	delete pfixture;
}

void ClearAndSetUserData(TiXmlNode* item, FixtureRef reference) {
	ClearUserData(item);
	item->SetUserData(new FixtureRefHolder(reference));
}

string NodeDebugString(TiXmlNode* node) {
	stringstream ss;
	void* puserdata = node->GetUserData();
	FixtureRefHolder* pholder = 0;
	Fixture* pfixture = 0;
	pholder = (FixtureRefHolder*)puserdata;
	pfixture = pholder->ref().operator->();

	ss << "TIXMLConfig::Item(address=" << node << ";userdata=" << puserdata << ";holder=" << pholder << ";fixture=" << pfixture << ";context=" << pfixture->_url.toString() << ";name=" << node->ValueStr();
	return ss.str();
}

FixtureRef Fixture::CreateFixtureRef(const URL& url) {
	return FixtureRef(std::make_shared<Fixture>(url));
}

} // namespace KEP
