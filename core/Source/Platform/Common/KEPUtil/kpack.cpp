///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "kpack.h"
#include <stdexcept>
#include <assert.h>
#include <fstream>
#include <algorithm>
#include <experimental/filesystem>
#undef min
#undef max

#if !defined(KPACK_APP)
#include "Helpers.h"
#endif

using namespace std;
namespace filesystem = std::experimental::filesystem::v1;

#define MAX_PATH 260

#ifndef DRF_KPACK_BUF_CHARS
#define DRF_KPACK_BUF_CHARS (128 * 1024)
#endif

namespace KEP {

#if !defined(KPACK_APP)
static LogInstance("Instance");
#endif

namespace KPack {

static void UnpackFiles(const string& pakFilePath, const string& unpackPath, size_t& numFilesUnpacked, bool purgeOnly, function<void(const string&)> progressUpdate) {
	numFilesUnpacked = 0;

#if !defined(KPACK_APP)
	if (!purgeOnly)
		LogInfo(pakFilePath << " ...");
#endif

	// Valid File ?
	if (pakFilePath.substr(pakFilePath.length() - 4) != ".pak") {
		throw invalid_argument("Invalid pak file path - '" + pakFilePath + "', expecting .pak extension");
	}

	ifstream pakFile(filesystem::u8path(pakFilePath), ios::binary | ios::ate);
	if (!pakFile.good()) {
		throw runtime_error("Error opening pak file for read - '" + pakFilePath + "', error: " + to_string(errno));
	}

	// Empty File ?
	int pakFileSize = pakFile.tellg();
	pakFile.seekg(0);
	if (pakFileSize == 0) {
		throw runtime_error("Pak file is empty - '" + pakFilePath + "'");
	}

	error_code error;

	// Unpack Files
	while (pakFile.tellg() < pakFileSize) {

		// Read File Header (<MAX_PATH:filePath> <int:fileSize>)
		char fileName[MAX_PATH + 1] = "";
		if (!pakFile.read(fileName, sizeof(char) * MAX_PATH)) {
			throw runtime_error("pakFile.read(fileName) FAILED - '" + pakFilePath + "' at offset " + to_string(pakFile.tellg()));
		}
		fileName[MAX_PATH] = NULL;
		if (*fileName == '\0') {
			throw runtime_error("File name is EMPTY - '" + pakFilePath + "' at offset " + to_string(pakFile.tellg()));
		}

		unsigned int fileSize = 0;
		if (!pakFile.read((char *)&fileSize, sizeof(unsigned int))) {
			throw runtime_error("pakFile.read(fileSize) FAILED - '" + pakFilePath + "' at offset " + to_string(pakFile.tellg()));
		}

		// Build Output File Path
		filesystem::path outFilePath = filesystem::u8path(fileName);
		outFilePath = outFilePath.filename(); // Strip directory from path, if any. (e.g. old pak file has leading slash in the file name).
		if (!unpackPath.empty()) {
			outFilePath = filesystem::u8path(unpackPath) / outFilePath;
		}

		// Delete Target File (just in case). Ignore error.
		filesystem::remove(outFilePath, error);

		if (purgeOnly) {
			pakFile.seekg(fileSize, ios::cur);
		}
		else {

#if !defined(KPACK_APP)
			LogInfo(string(" ---> ") + outFilePath.string() + " (" + to_string(fileSize) + " bytes)");
#endif

			// Open Output File For Writing
			ofstream outFile(outFilePath, ios::binary | ios::trunc);
			if (!outFile.good()) {
				throw runtime_error("outFile.open() FAILED - '" + outFilePath.u8string() + "', error: " + to_string(errno));
			}

			// Copy Pak File Contents To Output File
			char buf[DRF_KPACK_BUF_CHARS] = "";
			unsigned int bytesLeft = fileSize;
			while (bytesLeft > 0) {
				unsigned int bytesToTransfer = min(sizeof(buf), bytesLeft);
				pakFile.read(buf, bytesToTransfer);
				if (!pakFile.good()) {
					throw runtime_error("pakFile.read() FAILED - '" + pakFilePath + "', error: " + to_string(errno));
				}

				outFile.write(buf, bytesToTransfer);
				if (!outFile.good()) {
					throw runtime_error("outFile.write() FAILED - '" + outFilePath.u8string() + "', error: " + to_string(errno));
				}

				bytesLeft -= bytesToTransfer;
			}
		}

		++numFilesUnpacked;
		if (progressUpdate) {
			progressUpdate(outFilePath.u8string());
		}
	}
}

void UnpackFiles(const string& pakFilePath, const string& unpackPath, size_t& numFilesUnpacked, function<void(const string&)> progressUpdate) {
	// Unpack Files Purge Only
	UnpackFiles(pakFilePath, unpackPath, numFilesUnpacked, true, nullptr);

	// Unpack Files For Real
	UnpackFiles(pakFilePath, unpackPath, numFilesUnpacked, false, progressUpdate);
}

void PackFiles(const string& pakFilePath, const vector<string> filesToPack, function<void(const string&)> progressUpdate) {
	assert(!pakFilePath.empty());

	for (string filePathUtf8 : filesToPack) {
		if (filePathUtf8.find_first_of("<>|?*") != string::npos) {
			throw runtime_error("PackFiles: invalid file name '" + filePathUtf8 + "'");
		}
	}

	ofstream out(filesystem::u8path(pakFilePath), ios::binary | ios::trunc);
	if (!out.good()) {
		throw runtime_error("PackFiles: error opening file '" + pakFilePath + "' for write, error: " + to_string(errno));
	}

	vector<char> buffer;
	buffer.resize(DRF_KPACK_BUF_CHARS);

	vector<char> fileNameBuffer;
	fileNameBuffer.resize(MAX_PATH);

	for (string filePathUtf8 : filesToPack) {
		filesystem::path filePath = filesystem::u8path(filePathUtf8);
		string fileName = filePath.filename().u8string();
		if (fileName.length() > fileNameBuffer.size()) {
			throw runtime_error("PackFiles: file name too long '" + fileName + "'");
		}

		ifstream in(filePath, ios::binary | ios::ate);
		if (!in.good()) {
			throw runtime_error("PackFiles: error opening file '" + filePath.u8string() + "' for read, error: " + to_string(errno));
		}

		// Write file name (MAX_PATH bytes)
		fill(fileNameBuffer.begin(), fileNameBuffer.end(), 0);
		copy(fileName.cbegin(), fileName.cend(), fileNameBuffer.begin());
		out.write(&fileNameBuffer[0], fileNameBuffer.size());

		// Write file size
		unsigned int fileSize = in.tellg();
		in.seekg(0);
		out.write((const char *)&fileSize, sizeof(fileSize));

		// Write file data
		unsigned int bytesLeft = fileSize;
		while (bytesLeft > 0) {
			unsigned int bytesToTransfer = min(buffer.size(), bytesLeft);
			in.read(&buffer[0], bytesToTransfer);
			out.write(&buffer[0], bytesToTransfer);
			bytesLeft -= bytesToTransfer;
		}

		if (progressUpdate) {
			progressUpdate(filePath.u8string());
		}
	}
}

} // namespace KPack
} // namespace KEP