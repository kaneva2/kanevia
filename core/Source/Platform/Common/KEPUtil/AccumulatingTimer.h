///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/Accumulator.h"
#include "Numeric.h"

/**
 * A utility class for capturing execution times.  The usage pattern is the same
 * as jsFastLock.  An instance is instantiated on the stack.  When the instance
 * is rolled off of the stack, it is destructed and here is when it performs
 * it's work.  This insures that the time is accumulated regardless of 
 * the exit path.
 */
template <typename T>
class AccumulatingTimer : public Timer {
public:
	/**
	 * Construct an AccumulatingTimer with an Accumulator and a boolean specifying 
	 * whether or not the timer is enabled.   
	 * @param accumulator	A reference to the accumulator that the time will be 
	 *						accumulated to.
	 * @param enabled		boolean indicating whether the timer initial state is enabled or
	 *						disabled.  Defaults to true.
	 */
	AccumulatingTimer(Accumulator<T>& accumulator, bool paused = false) :
			Timer(paused), _accumulator(accumulator) {}

	/**
	 * Destructor.  The business end of the class.  When the instance is destroyed
	 * we accumulate the time.
	 */
	virtual ~AccumulatingTimer() { _accumulator.accumulate(ElapsedMs()); }

private:
	Accumulator<T>& _accumulator;
};
