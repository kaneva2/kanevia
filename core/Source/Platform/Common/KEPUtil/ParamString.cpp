///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ParamString.h"
#include "Common\KEPUtil\Algorithms.h"
#include <boost/algorithm/string.hpp>

namespace KEP {

ParamSet::ParamSet() :
		_format(), _params() {
	setMarkers();
}

ParamSet::ParamSet(const std::string& format) :
		_format(format), _params() {
	setMarkers();
}

ParamSet::ParamSet(const Properties& params, const std::string& format) :
		_format(format), _params(params) {
	setMarkers();
}

ParamSet::~ParamSet() {}

ParamSet&
ParamSet::set(const std::string& name, const std::string& value) {
	_params.set(name, value);
	return *this;
}

ParamSet&
ParamSet::set(const Properties& params) {
	_params = params;
	return *this;
}

ParamSet&
ParamSet::set(const std::vector<StringPairT>& params) {
	for (std::vector<StringPairT>::const_iterator iter = params.begin(); iter != params.end(); iter++)
		set(iter->first, iter->second);

	return *this;
}

ParamSet&
ParamSet::setMarkers(const std::string markers /* = "%()" */) {
	_scanChar = markers[0];
	_openChar = markers[1];
	_closeChar = markers[2];
	return *this;
}

std::string
ParamSet::toString() const {
	return computeString(_format, _scanChar, _openChar, _closeChar);
}

std::string
ParamSet::toString(const std::string format) const {
	return computeString(format, _scanChar, _openChar, _closeChar);
}

const Properties&
ParamSet::params() {
	return _params;
}

ParamSet&
ParamSet::setFormat(const std::string& format) {
	_format = format;
	return *this;
}

ParamSet&
ParamSet::set(const std::string& keyvalue) {
	int index = keyvalue.find('=');

	if (index < 0)
		return *this;

	std::string key = keyvalue.substr(0, index);
	boost::trim(key);
	std::string val = keyvalue.substr(index + 1);
	boost::trim(val);
	_params.set(key, val);
	return *this;
}

std::string
ParamSet::computeString(const std::string& format, char scanChar, char openChar, char closeChar) const {
	std::stringstream result;

	unsigned int last = 0;
	int index = 0;

	for (index = format.find(scanChar);
		 index >= 0;
		 index = format.find(scanChar, index + 1)) {
		// First copy from last till index.
		//
		result << format.substr(last, index - last);

		// Find out what the next char is.
		//
		int nextChar = format[index + 1];

		// just another % so point last at it and continue.
		//
		if (nextChar == scanChar) {
			index += 1;
			last = index;
			continue;
		}

		// If it is not the open char then just point last at the %
		// and continue.
		//
		if (nextChar != openChar) {
			last = index;
			continue;
		}

		// Find the close char.
		//
		last = format.find(closeChar, index);

		if (last < 0) {
			last = index;
			continue;
		}

		// Get the parameter name.
		//
		std::string name = format.substr(index + 2, last - index - 2);

		// Get the expanded value.
		//
		std::string value = _params.get(name);

		// Add the value to the result.
		//
		if (value.length())
			result << value;
		//		else                result << scanChar << openChar << name << closeChar;

		// Point last past the close char.
		//
		last += 1;
	}

	if (last < format.length())
		result << format.substr(last);

	return result.str();
}

} // namespace KEP