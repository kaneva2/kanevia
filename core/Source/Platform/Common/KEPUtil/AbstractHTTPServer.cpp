///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AbstractHTTPServer.h"
#include <log4cplus/logger.h>
using namespace log4cplus;

namespace KEP {

class EnumerateEndPoints : public HTTPServer::RequestHandler {
	typedef std::map<std::string, HTTPServer::RequestHandler*> HandlerMapT;
	const HandlerMapT& _handlerMap;

public:
	EnumerateEndPoints(const HandlerMapT& handlerMap) :
			HTTPServer::RequestHandler("endpoints"), _handlerMap(handlerMap) {}

	int RequestHandler::handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endpoint) {
		// Generate an xml string that enumerates the endpoints and their handler names
		//

		HTTPServer::Response r;
		r << Header("Content-Type", "text/xml")
		  << "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
		  << "<endpoints>";
		for (HandlerMapT::const_iterator iter = _handlerMap.begin(); iter != _handlerMap.end(); iter++) {
			r << "<endpoint path=\""
			  << iter->first << "\" "
			  << "name=\"" << iter->second->name() << "\" "
			  << "version=\"" << iter->second->version() << "\""
			  << "/>";
		}

		r << "</endpoints>";
		endpoint << r;
		return 1;
	}
};

AbstractHTTPServer::AbstractHTTPServer() :
		_dirEnabled(false), _bindPort(8080), _threadCount(10), _fileServingEnabled(false), HTTPServer(), _secure(false) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"HTTPServer"), "AbstractHTTPServer::AbstractHTTPServer()");
	_endpointEnumerationHandler = new EnumerateEndPoints(_handlerMap);
	// Add our enumeration handler
	registerHandler(*_endpointEnumerationHandler, "/endpoints");
}

AbstractHTTPServer::~AbstractHTTPServer() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"HTTPServer"), "AbstractHTTPServer::~AbstractHTTPServer()");
	delete _endpointEnumerationHandler;
}

HTTPServer& AbstractHTTPServer::enableFileServing(bool enable) {
	_fileServingEnabled = enable;
	return *this;
}

HTTPServer& AbstractHTTPServer::setThreadCount(unsigned long threadCount) {
	_threadCount = threadCount;
	return *this;
}

HTTPServer& AbstractHTTPServer::enableDirectories(bool dirEnabled) {
	_dirEnabled = dirEnabled;
	return *this;
}

HTTPServer&
AbstractHTTPServer::addPortBinding(unsigned short bindPort, bool secure, const char* pcHost) {
	return *this;
}

HTTPServer::RequestHandler&
AbstractHTTPServer::getDefaultURIHandler() {
	if (!_fileServingEnabled)
		return noFileSupportHandler;
	return HTTPServer::defaultRequestHandler;
}

HTTPServer::RequestHandler& AbstractHTTPServer::getHandler(const std::string& uri) {
	HTTPServer::RequestHandler* ret = _handlerMap[uri];
	if (ret != 0)
		return *ret;
	return getDefaultURIHandler();
}

HTTPServer& AbstractHTTPServer::registerHandler(RequestHandler& handler, const std::string& uri) {
	_handlerMap[uri] = &handler;
	return *this;
}

HTTPServer::RequestHandler* AbstractHTTPServer::deregisterHandler(const std::string& uri) {
	HandlerMapT::iterator iter = _handlerMap.find(uri);
	if (iter == _handlerMap.end())
		return 0;

	HTTPServer::RequestHandler* ret = iter->second;
	_handlerMap.erase(iter);
	return ret;
}

} // namespace KEP