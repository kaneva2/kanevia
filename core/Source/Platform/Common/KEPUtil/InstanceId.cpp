///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Common/include/CoreStatic/InstanceId.h"

namespace KEP {

// index is the 16 bit instanceId (USHORT)
// value is the 32 bit id for player
#ifndef REFACTOR_INSTANCEID
class IntInstanceIdMap {
private:
	std::vector<int> m_map;
	fast_mutex m_sync;

public:
	int get(USHORT shortInstanceId) {
		assert(shortInstanceId > 0 && shortInstanceId <= USHORT_MAX);

		std::lock_guard<fast_mutex> lock(m_sync);
		if (shortInstanceId > 0 && shortInstanceId <= m_map.size()) {
			int intInstanceId = m_map.at(shortInstanceId - 1); // we started at 1 to avoid zero instanceId
			//TRACE("InstanceId demapping instanceId: %d -> %d\n", intInstanceId, shortInstanceId);
			return intInstanceId;
		}

		assert(false);
		return 0;
	}

	USHORT insert(int intInstanceId) {
		std::lock_guard<fast_mutex> lock(m_sync);
		for (size_t i = 0; i < m_map.size(); i++) {
			if (m_map.at(i) == intInstanceId) {
				assert(i < USHORT_MAX);
				return (USHORT)(i + 1); // start at 1 to avoid 0 instanceId
			}
		}

		// TODO: what about when count > USHORT??
		assert(m_map.size() < USHORT_MAX);
		USHORT shortInstanceId = (USHORT)(m_map.size() + 1); // start at 1 to avoid 0 instanceId
		m_map.push_back(intInstanceId);
		TRACE("InstanceId mapping instanceId: %d -> %d\n", intInstanceId, shortInstanceId);
		return shortInstanceId;
	}
};

static IntInstanceIdMap s_instanceIdMap;

LONG getMappedInstanceId(USHORT shortInstanceId) {
	return s_instanceIdMap.get(shortInstanceId);
}

USHORT getShortInstanceId(LONG intInstanceId) {
	return s_instanceIdMap.insert(intInstanceId);
}

#else
#endif

} // namespace KEP
