///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "imodel.h"

namespace KEP {

class CDynamicPlacementObj;
class CReferenceObjList;

class DynamicPlacementModelProxy : public IModel {
public:
	DynamicPlacementModelProxy(CDynamicPlacementObj* mesh, IDirect3DDevice9* d, CReferenceObjList* libraryReferenceDB);

	virtual void GetName(std::string& aName);
	virtual bool Intersect(Vector3f& from, Vector3f& to, BOOL testAlpha = TRUE);

	virtual Matrix44f GetTransform();

	virtual void Move(float x, float y, float z);
	virtual void Rotate(float sx, float sy, float sz);
	virtual void Scale(float sx, float sy, float sz);

	virtual void GetBoundingBox(Vector4f& min, Vector4f& max);

	CDynamicPlacementObj* GetDynamicPlacementObj() {
		return m_dynamic_obj_placement;
	}

	virtual void Invalidate() {
		;
	}

protected:
	CDynamicPlacementObj* m_dynamic_obj_placement;

private:
	IDirect3DDevice9* m_device;
	CReferenceObjList* m_libraryReferenceDB;
};

} // namespace KEP
