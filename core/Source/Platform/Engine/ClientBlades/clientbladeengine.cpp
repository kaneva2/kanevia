///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <vector>

#include "bladeexports.h"
#include "assetfactory.h"
#include "wkgtypes.h"
#include "legacymodelproxy.h"
#include "clientbladefactory.h"
#include "clientbladeengine.h"

namespace KEP {

void ClientBladeEngine::GetViewMatrix(Matrix44f* out) const {
	(*out) = m_view_matrix;
}

void ClientBladeEngine::GetProjectionMatrix(Matrix44f* out) const {
	(*out) = m_projection_matrix;
}

IDirect3DDevice9*
ClientBladeEngine::GetD3DDevice() {
	return m_device;
}

IAssetFactory*
ClientBladeEngine::GetAssetFactory() {
	return AssetFactory::Instance();
}

IClientBladeFactory*
ClientBladeEngine::GetBladeFactory() {
	return ClientBladeFactory::Instance();
}

const IClientBladeFactory*
ClientBladeEngine::GetBladeFactory() const {
	return ClientBladeFactory::Instance();
}

}; // namespace KEP
