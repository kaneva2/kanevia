///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "clientbladefactory.h"
#include "iclientblade.h"
#include "iclientassetblade.h"
#include "imeshrenderblade.h"
#include "iclientengine.h"
#include "BladeStrings.h"
#include <string>
#include <typeinfo.h>
#include <algorithm>
#include <unordered_set>
#include "assetfactory.h"
#include "iasset.h"
#include "Common/include/UnicodeWindowsHelpers.h"
#include "Common/KEPUtil/Helpers.h"
#include "Common/KEPUtil/FrameTimeProfiler.h"

namespace KEP {

ClientBladeFactory::ClientBladeFactory() :
		wkg::Singleton<ClientBladeFactory>(),
		m_logger(LoggerGetInstance("Blade")) {
}

ClientBladeFactory::~ClientBladeFactory() {
}

void ClientBladeFactory::UnloadBlades(bool shutdown) {
	delete AssetFactory::Instance();

	std::vector<HINSTANCE> dll_handles;
	typedef std::vector<HINSTANCE>::iterator dll_handles_iterator;

	// first try and remove the blades loaded from within DLLs
	auto itor = m_blades.begin();
	while (itor != m_blades.end()) {
		if (find(dll_handles.begin(), dll_handles.end(), itor->m_dll_handle) == dll_handles.end()) {
			if (shutdown) {
				// for now, don't add KEP Menu to list of DLLs -- we don't want to free it here
				// it needs to be freed when the app closes (DXUT framework was intended to close
				// when the app closed)
				if (itor->m_blade->GetName() != "KEP Menu")
					dll_handles.push_back(itor->m_dll_handle);
			} else
				dll_handles.push_back(itor->m_dll_handle);
		}

		typedef void (*DestroyFunc)(IClientBlade*);
		DestroyFunc destroy_func = (DestroyFunc)::GetProcAddress(itor->m_dll_handle, "Destroy");
		if (destroy_func) {
			destroy_func(itor->m_blade);
			itor->m_blade = 0;
		}

		itor++;
	}

	// now free the DLLs
	dll_handles_iterator dll_itor = dll_handles.begin();
	while (dll_itor != dll_handles.end()) {
		::FreeLibrary(*dll_itor);

		dll_itor++;
	}

	m_blades.clear();
	dll_handles.clear();
}

void ClientBladeFactory::DetermineBladeInterfaces(IClientBlade* blade) {
	// iterate over each of the types we know of
#define TRY_TYPE(T)                                                                                                  \
	try {                                                                                                            \
		T* obj = dynamic_cast<T*>(blade);                                                                            \
		if (obj) {                                                                                                   \
			m_blademap.insert(                                                                                       \
				ClientBladeMap::value_type(                                                                          \
					typeid(T).name(), blade));                                                                       \
		}                                                                                                            \
	} catch (std::bad_cast) {                                                                                             \
	} catch (std::__non_rtti_object) {                                                                                    \
		auto pICE = IClientEngine::Instance();                                                                  \
		if (pICE)                                                                                                    \
			::MessageBoxW(pICE->GetWindowHandle(), L"You must compile plugins with RTTI enabled.", L"Error", MB_OK); \
	}

	// when we add any interfaces, we have to put them in here
	//  this avoids a lot of messy rtti issues
	TRY_TYPE(IClientRenderBlade);
	TRY_TYPE(IClientParticleBlade);
	TRY_TYPE(IClientAssetBlade);
	TRY_TYPE(IMeshRenderBlade);
	TRY_TYPE(IInputSubscriber);
	TRY_TYPE(IMenuBlade);

#undef TRY_TYPE
}

// loads the dll and asks for a new instance of the encapsulated class
//  loading a dll and asking for the interface should never have any dependencies
bool ClientBladeFactory::LoadBlade(const std::string& filename) {
	// Load Blade DLL
	HINSTANCE handle = ::LoadLibraryW(Utf8ToUtf16(filename).c_str());
	if (!handle) {
		DWORD err = ::GetLastError();
		LogError("LoadLibrary() FAILED - '" << LogPath(filename) << "' err=" << err);
		return false;
	}

	// Find Blade Create & GetNumInterfaces Functions
	typedef IClientBlade* (*CreateFunc)(size_t idx);
	typedef size_t (*GetNumFunc)();
	CreateFunc create_func = (CreateFunc)::GetProcAddress(handle, "Create");
	GetNumFunc getnum_func = (GetNumFunc)::GetProcAddress(handle, "GetNumInterfaces");
	if (!create_func || !getnum_func) {
		LogError("LoadLibrary() FAILED - '" << LogPath(filename) << "' Missing Create()");
		::FreeLibrary(handle);
		return false;
	}

	// Enumerate Blade Interfaces
	size_t num = getnum_func();
	LogInfo("OK - '" << LogPath(filename) << "' (" << num << " funcs)");
	for (size_t i = 0; i < num; i++) {
		ClientBladeContainer cont;
		cont.m_blade = create_func(i);
		cont.m_dll_handle = handle;

		// DRF - Skip Blades We Don't Use
		const std::unordered_set<std::string> unused({ "WKGMeshBlade",
			"WKGMeshRenderBlade",
			"MoveWidget",
			"ScaleWidget",
			"RotateWidget",
			"SelectWidget",
			"SceneGraphBlade" });
		if (unused.find(cont.m_blade->GetName()) != unused.end())
			continue;

		// Update blades & bladesMap
		m_blades.push_back(cont);
		DetermineBladeInterfaces(cont.m_blade);
	}

	return true;
}

// should be in <pathApp>/ClientBlades or ClientBladesd
std::string ClientBladeFactory::PathClientBlades() {
	if (m_pathClientBlades.empty()) {
#ifdef _DEBUG
		static const std::string pathClientBlades = "ClientBladesd";
#else
		static const std::string pathClientBlades = "ClientBlades";
#endif
		m_pathClientBlades = PathFind(PathApp(), pathClientBlades, true);
	}
	return m_pathClientBlades;
}

bool ClientBladeFactory::FindBlades(const std::string& /*csbase_dir*/) {
	// Find All ClientBlades/*.dll
	std::string pathClientBlades = PathClientBlades();
	std::string file_mask = PathAdd(pathClientBlades, "*.dll");

	bool done = false;
	WIN32_FIND_DATA find_data;
	HANDLE handle;
	handle = ::FindFirstFileW(Utf8ToUtf16(file_mask).c_str(), &find_data);
	if (INVALID_HANDLE_VALUE == handle)
		done = true;

	// Load Blade
	while (!done) {
		std::string pathname = PathAdd(pathClientBlades, Utf16ToUtf8(find_data.cFileName));
		LoadBlade(pathname);
		BOOL res = ::FindNextFile(handle, &find_data);
		if (!res)
			done = true;
	}

	::FindClose(handle);

#if DEAD_CODE
	new AssetFactory();

	// ok - after locating all the client plugins, let's find all those that implement an asset creator,
	//  and tell them all to register themselves
	size_t blades = GetNumBladesOfType(typeid(IClientAssetBlade));
	for (size_t i = 0; i < blades; i++) {
		IClientBlade* blade = GetBladeOfType(typeid(IAsset), i);
		IClientAssetBlade* ass_creator = (IClientAssetBlade*)blade;
		ass_creator->RegisterAsset(AssetFactory::Instance());
	}
#endif

	return true;
}

std::vector<IClientBlade*> ClientBladeFactory::GetBladesOfType(const std::string& typeIdName) const {
	std::vector<IClientBlade*> vecBlades;
	std::string typeNameFind = typeIdName;
	for (const auto& it : m_blademap) {
		std::string typeName = it.first;
		if ((typeName == typeNameFind) && it.second)
			vecBlades.push_back(it.second);
	}
	return vecBlades;
}

std::vector<IClientRenderBlade*> ClientBladeFactory::GetClientRenderBlades() {
	static std::vector<IClientRenderBlade*> s_CBs;
	if (!s_CBs.size()) {
		auto vecBlades = Instance()->GetBladesOfType(typeid(IClientRenderBlade).name());
		for (const auto& pCB : vecBlades)
			s_CBs.push_back(dynamic_cast<IClientRenderBlade*>(pCB));
	}
	return s_CBs;
}

std::vector<IInputSubscriber*> ClientBladeFactory::GetInputSubscribers() {
	static std::vector<IInputSubscriber*> s_CBs;
	if (!s_CBs.size()) {
		auto vecBlades = Instance()->GetBladesOfType(typeid(IInputSubscriber).name());
		for (const auto& pCB : vecBlades)
			s_CBs.push_back(dynamic_cast<IInputSubscriber*>(pCB));
	}
	return s_CBs;
}

IClientParticleBlade* ClientBladeFactory::GetClientParticleBlade() {
	static IClientParticleBlade* s_pCB = nullptr;
	if (!s_pCB) {
		auto vecBlades = Instance()->GetBladesOfType(typeid(IClientParticleBlade).name());
		s_pCB = vecBlades.size() ? dynamic_cast<IClientParticleBlade*>(vecBlades[0]) : nullptr;
	}
	return s_pCB;
}

template <typename FactoryPtrType, typename BladePtrType>
BladePtrType GetMenuBlade(FactoryPtrType pFactory) {
	static BladePtrType s_pCB = nullptr;
	if (!s_pCB) {
		auto vecBlades = pFactory->GetBladesOfType(typeid(IMenuBlade).name());
		s_pCB = vecBlades.size() ? dynamic_cast<BladePtrType>(vecBlades[0]) : nullptr;
	}
	return s_pCB;
}

IMenuBlade* ClientBladeFactory::GetMenuBlade() {
	return ::KEP::GetMenuBlade<ClientBladeFactory*, IMenuBlade*>(this);
}

const IMenuBlade* ClientBladeFactory::GetMenuBlade() const {
	return ::KEP::GetMenuBlade<const ClientBladeFactory*, const IMenuBlade*>(this);
}

size_t ClientBladeFactory::GetNumBlades() const {
	return m_blades.size();
}

IClientBlade* ClientBladeFactory::GetBlade(size_t idx) const {
	return m_blades[idx].m_blade;
}

// Client Window Out Of View (minimized)
void ClientBladeFactory::OnLostDevice() {
	for (const auto& blade : m_blades) {
		auto pCB = blade.m_blade;
		if (pCB)
			pCB->OnLostDevice();
	}
}

// Client Window Returns To View (un-minimized)
void ClientBladeFactory::OnResetDevice() {
	for (const auto& blade : m_blades) {
		auto pCB = blade.m_blade;
		if (pCB)
			pCB->OnResetDevice();
	}
}

void ClientBladeFactory::OnUpdateBlades() {
	for (const auto& blade : m_blades) {
		IClientBlade* pCB = blade.m_blade;
		if (!pCB)
			continue;
		pCB->OnUpdate();
	}
}

void ClientBladeFactory::RenderBlades(RenderBladeDepth depthBufferUse) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	auto pFTP = pICE->GetFrameTimeProfiler();
	FrameTimeNestedContext ftnc(pFTP, "RenderBlades");

	// Update & Render All Client Render Blades
	auto vecBlades = GetClientRenderBlades();
	for (const auto& pCRB : vecBlades) {
		if (!pCRB || (pCRB->m_bladeDepth != depthBufferUse))
			continue;
		auto name = pCRB->GetName();
		{
			FrameTimeNestedContext ftnc2(pFTP, "Update-" + name);
			pCRB->Update();
		}
		{
			FrameTimeNestedContext ftnc2(pFTP, "Render-" + name);
			pCRB->Render();
		}
	}
}

void ClientBladeFactory::OnLButtonDown(int x, int y) {
	auto vecBlades = GetInputSubscribers();
	for (const auto& pISB : vecBlades) {
		if (!pISB)
			continue;
		if (pISB->IsInputEnabled())
			pISB->OnLButtonDown(x, y);
	}
}

void ClientBladeFactory::OnLButtonUp(int x, int y) {
	auto vecBlades = GetInputSubscribers();
	for (const auto& pISB : vecBlades) {
		if (!pISB)
			continue;
		if (pISB->IsInputEnabled())
			pISB->OnLButtonUp(x, y);
	}
}

void ClientBladeFactory::OnRButtonDown(int x, int y) {
	auto vecBlades = GetInputSubscribers();
	for (const auto& pISB : vecBlades) {
		if (!pISB)
			continue;
		if (pISB->IsInputEnabled())
			pISB->OnRButtonDown(x, y);
	}
}

void ClientBladeFactory::OnRButtonUp(int x, int y) {
	auto vecBlades = GetInputSubscribers();
	for (const auto& pISB : vecBlades) {
		if (!pISB)
			continue;
		if (pISB->IsInputEnabled())
			pISB->OnRButtonUp(x, y);
	}
}

void ClientBladeFactory::OnMButtonDown(int x, int y) {
	auto vecBlades = GetInputSubscribers();
	for (const auto& pISB : vecBlades) {
		if (!pISB)
			continue;
		if (pISB->IsInputEnabled())
			pISB->OnMButtonDown(x, y);
	}
}

void ClientBladeFactory::OnMButtonUp(int x, int y) {
	auto vecBlades = GetInputSubscribers();
	for (const auto& pISB : vecBlades) {
		if (!pISB)
			continue;
		if (pISB->IsInputEnabled())
			pISB->OnMButtonUp(x, y);
	}
}

void ClientBladeFactory::OnUnLoadScene() {
	for (const auto& blade : m_blades) {
		IClientBlade* pCB = blade.m_blade;
		if (pCB)
			pCB->OnUnLoadScene();
	}
}

void ClientBladeFactory::OnLoadScene() {
	for (const auto& blade : m_blades) {
		IClientBlade* pCB = blade.m_blade;
		if (pCB)
			pCB->OnLoadScene();
	}
}

void ClientBladeFactory::OnMouseMove(int x, int y) {
	auto vecBlades = GetInputSubscribers();
	for (const auto& pISB : vecBlades) {
		if (!pISB)
			continue;
		if (pISB->IsInputEnabled())
			pISB->OnMouseMove(x, y);
	}
}

bool ClientBladeFactory::HitTest(int x, int y) {
	auto vecBlades = GetInputSubscribers();
	for (const auto& pISB : vecBlades) {
		if (!pISB)
			continue;
		if (pISB->HitTest(x, y))
			return true;
	}
	return false;
}

bool ClientBladeFactory::InitClientBlades() {
	bool retval = true;

	// we now have a d3d device, so tell our blades to initialize themselves
	// initialize a map with all the blade interfaces
	std::map<std::string, blade_init_struct> bladeMap;

	for (const auto& blade : m_blades) {
		IClientBlade* pCB = blade.m_blade;
		if (!pCB)
			continue;
		blade_init_struct bis;
		bis.blade = pCB;
		bis.initialized = false;
		bladeMap.insert(std::map<std::string, blade_init_struct>::value_type(typeid(*pCB).name(), bis));
	}

	bool all_done = false;
	int pass = 0;
	while (!all_done) {
		bool initialized_one = false;
		all_done = true;
		++pass;
		LogDebug("Dependency Pass " << pass << " ...");

		for (const auto& blade : m_blades) {
			auto pCB = blade.m_blade;
			if (!pCB)
				continue;

			std::string name = typeid(*pCB).name();
			LogDebug(name << " ...");

			// are we initialized yet?
			if (!bladeMap[name].initialized) {
				bool can_initialize = true;

				// check to see if we have the necessary dependencies initialized
				size_t dependencies = pCB->GetNumDependencies();
				for (size_t d = 0; d < dependencies; d++) {
					std::string dep = pCB->GetDependency(d);
					auto itor = bladeMap.find(dep);
					if (itor != bladeMap.end()) {
						// if this blade is asking for a dependency that doesn't exist, then
						//  don't allow it to say that we're not done... otherwise, we'll
						//  never be all_done
						all_done = false;
						if (!itor->second.initialized) {
							// we have it, but it's not initialized yet
							can_initialize = false;
							break;
						}
					}
				}

				if (can_initialize) {
					if (pCB->Initialize()) {
						bladeMap[name].initialized = true;
						initialized_one = true;
					}
				}
			}
		}
	}

	std::vector<std::string> uninitialized_blades;

	auto itor = m_blades.begin();
	while (itor != m_blades.end()) {
		// if this blade wasn't initialized
		std::string name = typeid(*(itor->m_blade)).name();

		// are we initialized yet?
		if (!bladeMap[name].initialized) {
			uninitialized_blades.push_back(name);

			LogError(name << " FAILED");

			// since it failed to init, remove from blademap all that have the same blade
			auto blademap_itor = m_blademap.begin();
			while (blademap_itor != m_blademap.end()) {
				if (blademap_itor->second == itor->m_blade)
					blademap_itor = m_blademap.erase(blademap_itor);
				else
					blademap_itor++;
			}

			typedef void (*DestroyFunc)(IClientBlade*);
			DestroyFunc destroy_func = (DestroyFunc)::GetProcAddress(itor->m_dll_handle, "Destroy");
			if (destroy_func) {
				destroy_func(itor->m_blade);
				itor->m_blade = 0;
			}

			::FreeLibrary(itor->m_dll_handle);
			itor->m_dll_handle = 0;

			itor = m_blades.erase(itor);
		} else {
			itor++;
		}
	}

	// show message box if there are any uninitialized blades
	auto uninit_itor = uninitialized_blades.begin();
	std::string msg = loadStr(IDS_ERROR_BLADES_NOT_INITIALIZED);
	while (uninit_itor != uninitialized_blades.end()) {
		if (uninit_itor != uninitialized_blades.begin())
			msg += "\r\n";
		msg += *uninit_itor;
		uninit_itor++;
	}

	if (!uninitialized_blades.empty()) {
		retval = false;
		auto pICE = IClientEngine::Instance();
		if (pICE)
			MessageBoxUtf8(pICE->GetWindowHandle(), msg.c_str(), loadStr(IDS_ERROR_INITIALIZING_BLADES).c_str(), MB_OK | MB_ICONERROR);
	}
	uninitialized_blades.clear();

	return retval;
}

bool ClientBladeFactory::PreInitClientBlades() {
	bool retval = true;
	for (const auto& blade : m_blades) {
		IClientBlade* pCB = blade.m_blade;
		if (!pCB)
			continue;
		std::string name = typeid(*(pCB)).name();
		if (!pCB->PreInitialize()) {
			LogError(name << " FAILED");
			retval = false;
		}
	}
	return retval;
}

bool ClientBladeFactory::PostInitClientBlades() {
	bool retval = true;
	for (const auto& blade : m_blades) {
		IClientBlade* pCB = blade.m_blade;
		if (!pCB)
			continue;
		std::string name = typeid(*(pCB)).name();
		if (!pCB->PostInitialize()) {
			LogError(name << " FAILED");
			retval = false;
		}
	}
	return retval;
}

} // namespace KEP
