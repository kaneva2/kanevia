///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "kephelpers.h"

#include "iclientblade.h"
#include "tools\wkglib\singleton.h"
#include "iclientbladefactory.h"
#include "iclientrenderblade.h"
#include "log4cplus/logger.h"
#include <string>
#include <map>
#include <vector>

namespace KEP {

class ClientBladeFactory : public IClientBladeFactory, public wkg::Singleton<ClientBladeFactory> {
public:
	ClientBladeFactory();
	virtual ~ClientBladeFactory();

	virtual std::string PathClientBlades() override;

	virtual size_t GetNumBlades() const override;
	virtual IClientBlade* GetBlade(size_t idx) const override;

	virtual std::vector<IClientBlade*> GetBladesOfType(const std::string& typeIdName) const override;

	virtual std::vector<IClientRenderBlade*> GetClientRenderBlades() override;
	virtual std::vector<IInputSubscriber*> GetInputSubscribers() override;
	virtual IClientParticleBlade* GetClientParticleBlade() override;
	virtual IMenuBlade* GetMenuBlade() override;
	virtual const IMenuBlade* GetMenuBlade() const override;

	void OnLButtonDown(int x, int y);
	void OnLButtonUp(int x, int y);

	void OnRButtonDown(int x, int y);
	void OnRButtonUp(int x, int y);

	void OnMButtonDown(int x, int y);
	void OnMButtonUp(int x, int y);

	void OnMouseMove(int x, int y);

	bool HitTest(int x, int y);

	virtual void OnLostDevice();
	virtual void OnResetDevice();

	bool FindBlades(const std::string& base_dir);

	void UnloadBlades(bool shutdown = false);

	/** DRF
	* This function pre-initializes all client blades.  Call only after all script and client blades are loaded.
	*/
	bool PreInitClientBlades();

	/** DRF
	* This function initializes all client blades by loading them and registering events and handlers.
	*/
	bool InitClientBlades();

	/** DRF
	* This function post-initializes all client blades.  Call only after all script and client blades are loaded.
	*/
	bool PostInitClientBlades();

	void OnUpdateBlades(); // drf - added

	void RenderBlades(RenderBladeDepth depthBufferUse);

	virtual void OnUnLoadScene();
	virtual void OnLoadScene();

protected:
	bool LoadBlade(const std::string& filename);

	void DetermineBladeInterfaces(IClientBlade* blade);

protected:
	struct blade_init_struct {
		IClientBlade* blade;
		bool initialized;
	};

	// a map from interface types to client blades
	typedef std::multimap<std::string, IClientBlade*> ClientBladeMap;
	ClientBladeMap m_blademap;

	class ClientBladeContainer {
	public:
		IClientBlade* m_blade;
		HINSTANCE m_dll_handle;
	};

	// just a vector containing those that have been created and their dll interface
	typedef std::vector<ClientBladeContainer> ClientBladeVector;
	ClientBladeVector m_blades;

	// DRF - Added
	std::string m_pathClientBlades;
	std::string m_pathClientBladesMeshes;
	std::string m_pathClientBladesEffects;

	log4cplus::Logger m_logger;
};

} // namespace KEP
