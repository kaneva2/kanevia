/******************************************************************************
 imeshrenderblade.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "iclientblade.h"

struct D3DXMATRIX;

namespace KEP {

class IMesh;

class IMeshRenderBlade : public IClientBlade {
public:
	virtual void RenderMesh(D3DXMATRIX& m, IMesh* mesh, const char* effect = 0) = 0;

protected:
};

} // namespace KEP
