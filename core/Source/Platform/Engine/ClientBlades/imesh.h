/******************************************************************************
 imesh.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "wkgtypes.h"
#include "iasset.h"
#include <Core/Math/Matrix.h>

namespace KEP {

class IMesh : public IAsset {
public:
	virtual bool Intersect(Matrix44f& /*world*/, Vector3f& /*from*/, Vector3f& /*to*/) {
		return false;
	}
};

} // namespace KEP
