///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "imodel.h"

namespace KEP {

class CMovementObj;
class CMovementObjList;

class PlayerModelProxy : public IModel {
public:
	PlayerModelProxy(CMovementObj* mesh, IDirect3DDevice9* d, CMovementObjList* libraryReferenceDB);

	~PlayerModelProxy();

	virtual void GetName(std::string& aName);
	virtual bool Intersect(Vector3f& from, Vector3f& to, BOOL testAlpha = TRUE);

	virtual Matrix44f GetTransform();

	virtual void Move(float x, float y, float z);
	virtual void Rotate(float sx, float sy, float sz);
	virtual void Scale(float sx, float sy, float sz);

	virtual void GetBoundingBox(Vector4f& min, Vector4f& max);

	CMovementObj* GetPlayerObj() {
		return m_movementObj;
	}

	virtual void Invalidate() {
		m_movementObj = NULL;
	}

protected:
	CMovementObj* m_movementObj;

private:
	// PlayerModelProxy();		// NOT USED

	IDirect3DDevice9* m_device;
	CMovementObjList* m_libraryReferenceDB;

	static const UINT m_maxVerts = 16384;

	bool m_cached_box;
	Vector3f m_min, m_max;
};

} // namespace KEP
