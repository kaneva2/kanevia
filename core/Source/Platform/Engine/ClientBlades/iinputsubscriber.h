#pragma once

namespace KEP {

class IInputSubscriber {
public:
	IInputSubscriber() :
			m_input_enabled(true) {}

	virtual void OnLButtonDown(int x, int y) = 0;
	virtual void OnLButtonUp(int x, int y) = 0;

	virtual void OnRButtonDown(int x, int y) = 0;
	virtual void OnRButtonUp(int x, int y) = 0;

	virtual void OnMButtonDown(int x, int y) = 0;
	virtual void OnMButtonUp(int x, int y) = 0;

	virtual void OnMouseMove(int x, int y) = 0;
	//virtual void OnKey(unsigned vkey) = 0;

	virtual bool HitTest(int /*x*/, int /*y*/) const {
		return false;
	}

	enum CursorType { GameCursor,
		WindowsCursor };
	// returns the type of cursor we want to know the
	//  posiiton of.  An input subscriber can only choose one.
	virtual CursorType GetCursorType() {
		return WindowsCursor;
	}

	virtual void EnableInput(bool enable = true) {
		m_input_enabled = enable;
	}
	virtual bool IsInputEnabled() {
		return m_input_enabled;
	}

protected:
	bool m_input_enabled;
};

} // namespace KEP
