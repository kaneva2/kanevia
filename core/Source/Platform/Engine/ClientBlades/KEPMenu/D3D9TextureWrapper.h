///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <d3d9.h>

namespace KEP {

class D3D9DeviceWrapper;

class D3D9TextureWrapper : public IDirect3DTexture9 {
public:
	D3D9TextureWrapper(D3D9DeviceWrapper* device, IDirect3DTexture9* pd3dTexture) :
			m_pd3dDeviceWrapper(device), m_pd3dTexture(pd3dTexture), m_revision(0) {}

	IDirect3DTexture9* getD3DTexture() const { return m_pd3dTexture; }

	// Transparent pass-through
	virtual STDMETHODIMP QueryInterface(THIS_ REFIID iid, LPVOID* ppv) override { return m_pd3dTexture->QueryInterface(iid, ppv); }
	virtual STDMETHODIMP_(ULONG) AddRef(THIS) override { return m_pd3dTexture->AddRef(); }
	virtual STDMETHODIMP SetPrivateData(THIS_ REFGUID refguid, CONST void* pData, DWORD SizeOfData, DWORD Flags) override { return m_pd3dTexture->SetPrivateData(refguid, pData, SizeOfData, Flags); }
	virtual STDMETHODIMP GetPrivateData(THIS_ REFGUID refguid, void* pData, DWORD* pSizeOfData) override { return m_pd3dTexture->GetPrivateData(refguid, pData, pSizeOfData); }
	virtual STDMETHODIMP FreePrivateData(THIS_ REFGUID refguid) override { return m_pd3dTexture->FreePrivateData(refguid); }
	virtual STDMETHODIMP_(DWORD) SetPriority(THIS_ DWORD PriorityNew) override { return m_pd3dTexture->SetPriority(PriorityNew); }
	virtual STDMETHODIMP_(DWORD) GetPriority(THIS) override { return m_pd3dTexture->GetPriority(); }
	virtual STDMETHODIMP_(void) PreLoad(THIS) override { return m_pd3dTexture->PreLoad(); }
	virtual STDMETHODIMP_(D3DRESOURCETYPE) GetType(THIS) override { return m_pd3dTexture->GetType(); }
	virtual STDMETHODIMP_(DWORD) SetLOD(THIS_ DWORD LODNew) override { return m_pd3dTexture->SetLOD(LODNew); }
	virtual STDMETHODIMP_(DWORD) GetLOD(THIS) override { return m_pd3dTexture->GetLOD(); }
	virtual STDMETHODIMP_(DWORD) GetLevelCount(THIS) override { return m_pd3dTexture->GetLevelCount(); }
	virtual STDMETHODIMP SetAutoGenFilterType(THIS_ D3DTEXTUREFILTERTYPE FilterType_) override { return m_pd3dTexture->SetAutoGenFilterType(FilterType_); }
	virtual STDMETHODIMP_(D3DTEXTUREFILTERTYPE) GetAutoGenFilterType(THIS) override { return m_pd3dTexture->GetAutoGenFilterType(); }
	virtual STDMETHODIMP_(void) GenerateMipSubLevels(THIS) override { return m_pd3dTexture->GenerateMipSubLevels(); }
	virtual STDMETHODIMP GetLevelDesc(THIS_ UINT Level, D3DSURFACE_DESC* pDesc) override { return m_pd3dTexture->GetLevelDesc(Level, pDesc); }
	virtual STDMETHODIMP GetSurfaceLevel(THIS_ UINT Level, IDirect3DSurface9** ppSurfaceLevel) override { return m_pd3dTexture->GetSurfaceLevel(Level, ppSurfaceLevel); }
	virtual STDMETHODIMP UnlockRect(THIS_ UINT Level) override { return m_pd3dTexture->UnlockRect(Level); }
	virtual STDMETHODIMP AddDirtyRect(THIS_ CONST RECT* pDirtyRect) override { return m_pd3dTexture->AddDirtyRect(pDirtyRect); } // Is forced dirty region worth monitoring?

	// Overrides
	virtual STDMETHODIMP_(ULONG) Release(THIS) override;
	virtual STDMETHODIMP GetDevice(THIS_ LPDIRECT3DDEVICE9* ppDevice) override;
	virtual STDMETHODIMP LockRect(THIS_ UINT Level, D3DLOCKED_RECT* pLockedRect, CONST RECT* pRect, DWORD Flags) override;

	int getRevision() const { return m_revision; }
	void incRevision() { m_revision++; }

private:
	D3D9DeviceWrapper* m_pd3dDeviceWrapper;
	IDirect3DTexture9* m_pd3dTexture;
	int m_revision; // Use revision to track texture changes. Revision -1 is used by sprites to indicate that the texture is not yet loaded to atlas.
};

} // namespace KEP
