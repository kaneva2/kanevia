///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

typedef int BOOL;
#define TRUE 1
#define FALSE 0

#pragma pack(push, 1)

// DRF - List/Combo/User Data Type (must be signed!)
#define tDATA int
#define DataToPtr(a) IntToPtr(a)
#define PtrToData(a) PtrToInt(a)

struct COLOR {
	unsigned char b, g, r, a;
};

#pragma pack(pop)

typedef unsigned long RAWCOLOR;

inline COLOR ToCOLOR(DWORD src) {
	return *(COLOR*)&src;
}

inline DWORD FromCOLOR(COLOR src) {
	return *(DWORD*)&src;
}

namespace KEP {
class IMenuDialog;
class IMenuControl;
class IMenuStatic;
class IMenuImage;
class IMenuButton;
class IMenuCheckBox;
struct IMenuBlendColor;
class IMenuElement;
}

int Dialog_NumOpen();

BOOL Dialog_Log();

BOOL Dialog_MsgBox(const char* msg);

const char* Dialog_MenuId(KEP::IMenuDialog* pDialog);
const char* Dialog_MenuName(KEP::IMenuDialog* pDialog);
const char* Dialog_ScriptName(KEP::IMenuDialog* pDialog);

KEP::IMenuDialog* Dialog_GetDialogByName(const char* menuName);
KEP::IMenuDialog* Menu_GetDialog(const char* menuName);

BOOL Dialog_IsDialogOpenByName(const char* menuName);
bool Menu_IsOpen(const char* menuName);

KEP::IMenuDialog* Dialog_New();

KEP::IMenuDialog* Dialog_CreateAs(const char* menuName, const char* menuId);
bool Menu_OpenAs(const char* menuName, const char* menuId);

KEP::IMenuDialog* Dialog_Create(const char* menuName);
bool Menu_Open(const char* menuName);

KEP::IMenuDialog* Dialog_CreateTrustedAs(const char* menuName, const char* menuId);
bool Menu_OpenTrustedAs(const char* menuName, const char* menuId);

KEP::IMenuDialog* Dialog_CreateTrusted(const char* menuName);
bool Menu_OpenTrusted(const char* menuName);

KEP::IMenuDialog* Dialog_CreateAndEdit(const char* menuName, BOOL edit);
bool Menu_OpenForEdit(const char* menuName);

KEP::IMenuDialog* Dialog_CreateAtCoordinates(const char* menuName, int x, int y);
bool Menu_OpenAtCoordinates(const char* menuName, int x, int y);

BOOL Dialog_Destroy(KEP::IMenuDialog* dh);
bool Menu_Close(const char* menuName);

BOOL Dialog_DestroyAll(BOOL ok);
bool Menu_CloseAll();

BOOL Dialog_DestroyAllExcept(KEP::IMenuDialog* dh);
bool Menu_CloseAllExcept(const char* menuName);

BOOL Dialog_DestroyAllNonEssentialExcept(KEP::IMenuDialog* dh, BOOL includeFramework = false);
bool Menu_CloseAllNonEssentialExcept(const char* menuName, BOOL includeFramework = false);

BOOL Dialog_Save(KEP::IMenuDialog* dh, const char* fileName);
bool Menu_Save(const char* menuName, const char* fileName);

BOOL Dialog_CopyTemplate(KEP::IMenuDialog* pDialog);
bool Menu_CopyTemplate(const char* menuName);

BOOL Dialog_ApplyInputSettings(KEP::IMenuDialog* dh);
bool Menu_ApplyInputSettings(const char* menuName);

BOOL Dialog_SendToBack(KEP::IMenuDialog* dh);
bool Menu_SendToBack(const char* menuName);

BOOL Dialog_BringToFrontEnable(KEP::IMenuDialog* dh, BOOL enable);
bool Menu_BringToFrontEnable(const char* menuName, BOOL enable);

BOOL Dialog_BringToFront(KEP::IMenuDialog* dh);
bool Menu_BringToFront(const char* menuName);

BOOL Dialog_AlwaysBringToFront(KEP::IMenuDialog* dh);
bool Menu_AlwaysBringToFront(const char* menuName);

BOOL Dialog_AlwaysBringToFrontDisable();
bool Menu_AlwaysBringToFrontDisable();

BOOL Dialog_ClearFocus(KEP::IMenuDialog* dh);

BOOL Dialog_SetEnabled(KEP::IMenuDialog* dh, BOOL enable);

// DEPRECATED - Use MakeWebCall() Instead
BOOL Dialog_GetBrowserPage(KEP::IMenuDialog* dh, const char* url, int filter, int startIndex, int maxItems);
BOOL Dialog_MakeWebCall(const char* url, int filter, int startIndex, int maxItems);

BOOL Dialog_MoveControlForward(KEP::IMenuDialog* dh, KEP::IMenuControl* pControl);
BOOL Dialog_MoveControlBackward(KEP::IMenuDialog* dh, KEP::IMenuControl* pControl);
BOOL Dialog_MoveControlToFront(KEP::IMenuDialog* dh, KEP::IMenuControl* pControl);
BOOL Dialog_MoveControlToBack(KEP::IMenuDialog* dh, KEP::IMenuControl* pControl);
BOOL Dialog_MoveControlToIndex(KEP::IMenuDialog* dh, KEP::IMenuControl* pControl, unsigned int index);
BOOL Dialog_SetSelected(KEP::IMenuDialog* dh, BOOL selected);

KEP::IMenuControl* Dialog_LoadControlByXml(KEP::IMenuDialog* dh, const char* xml);
const char* Dialog_GetControlXml(KEP::IMenuDialog* dh, KEP::IMenuControl* ch);

// dialog generic events
BOOL Dialog_RegisterGenericEvents(KEP::IMenuDialog* dh);
BOOL Dialog_UnRegisterGenericEvents(KEP::IMenuDialog* dh);

// dialog controls accessors
int Dialog_GetId(KEP::IMenuDialog* dh);

KEP::IMenuControl* Dialog_GetControl(KEP::IMenuDialog*, const char* name);
KEP::IMenuControl* Dialog_GetControlByIndex(KEP::IMenuDialog*, int index);

int Dialog_GetNumControls(KEP::IMenuDialog*);

KEP::IMenuStatic* Dialog_GetStatic(KEP::IMenuDialog* dh, const char* name);
KEP::IMenuImage* Dialog_GetImage(KEP::IMenuDialog* dh, const char* name);
KEP::IMenuButton* Dialog_GetButton(KEP::IMenuDialog* dh, const char* name);
KEP::IMenuCheckBox* Dialog_GetCheckBox(KEP::IMenuDialog* dh, const char* name);
KEP::IMenuRadioButton* Dialog_GetRadioButton(KEP::IMenuDialog* dh, const char* name);
KEP::IMenuComboBox* Dialog_GetComboBox(KEP::IMenuDialog* dh, const char* name);
KEP::IMenuSlider* Dialog_GetSlider(KEP::IMenuDialog* dh, const char* name);
KEP::IMenuEditBox* Dialog_GetEditBox(KEP::IMenuDialog* dh, const char* name);
KEP::IMenuListBox* Dialog_GetListBox(KEP::IMenuDialog* dh, const char* name);

// dialog control methods
HRESULT Dialog_AddControl(KEP::IMenuDialog* dh, KEP::IMenuControl* ch);
BOOL Dialog_RemoveControl(KEP::IMenuDialog* dh, const char* controlName);
BOOL Dialog_FocusDefaultControl(KEP::IMenuDialog* dh);

// dialog controls accessors
KEP::IMenuStatic* Dialog_AddStatic(KEP::IMenuDialog* dh, const char* name, const char* text, int x, int y, int width, int height);
KEP::IMenuImage* Dialog_AddImage(KEP::IMenuDialog* dh, const char* name, int x, int y, const char* textureName, int left, int top, int right, int bottom);
KEP::IMenuImage* Dialog_AddImageToBack(KEP::IMenuDialog* dh, const char* name, int x, int y, const char* textureName, int left, int top, int right, int bottom);
KEP::IMenuButton* Dialog_AddButton(KEP::IMenuDialog* dh, const char* name, const char* text, int x, int y, int width, int height);
KEP::IMenuCheckBox* Dialog_AddCheckBox(KEP::IMenuDialog* dh, const char* name, const char* text, int x, int y, int width, int height, BOOL bChecked);
KEP::IMenuRadioButton* Dialog_AddRadioButton(KEP::IMenuDialog* dh, const char* name, unsigned int buttonGroup, const char* text, int x, int y, int width, int height, BOOL bChecked);
KEP::IMenuComboBox* Dialog_AddComboBox(KEP::IMenuDialog* dh, const char* name, int x, int y, int width, int height);
KEP::IMenuSlider* Dialog_AddSlider(KEP::IMenuDialog* dh, const char* name, int x, int y, int width, int height);

KEP::IMenuEditBox* Dialog_AddEditBox(KEP::IMenuDialog* dh, const char* name, const char* text, int x, int y, int width, int height);
KEP::IMenuEditBox* Dialog_AddSimpleEditBox(KEP::IMenuDialog* dh, const char* name, const char* text, int x, int y, int width, int height);

KEP::IMenuListBox* Dialog_AddListBox(KEP::IMenuDialog* dh, const char* name, int x, int y, int width, int height);

BOOL Dialog_CopyControl(KEP::IMenuDialog* dh, KEP::IMenuControl* ch);
BOOL Dialog_PasteControl(KEP::IMenuDialog* dh);

bool Dialog_IsSomethingInClipboard();

// accessors
KEP::IMenuElement* Dialog_GetDisplayElement(KEP::IMenuDialog* dh, const char* elementName);
KEP::IMenuElement* Dialog_GetCaptionElement(KEP::IMenuDialog* dh);
KEP::IMenuElement* Dialog_GetBodyElement(KEP::IMenuDialog* dh);
BOOL Dialog_GetElementTagUsesFontState(KEP::IMenuDialog* dh, const char* pElementTag, unsigned int iState);
BOOL Dialog_GetElementTagUsesTextureState(KEP::IMenuDialog* dh, const char* pElementTag, unsigned int iState);
BOOL Dialog_GetMinimized(KEP::IMenuDialog* dh);
BOOL Dialog_GetEdit(KEP::IMenuDialog* dh);
BOOL Dialog_GetCaptionEnabled(KEP::IMenuDialog* dh);
const char* Dialog_GetCaptionText(KEP::IMenuDialog* dh);
int Dialog_GetCaptionHeight(KEP::IMenuDialog* dh);
BOOL Dialog_GetCaptionShadow(KEP::IMenuDialog* dh);
COLOR Dialog_GetColorTopLeft(KEP::IMenuDialog* dh);
COLOR Dialog_GetColorTopRight(KEP::IMenuDialog* dh);
COLOR Dialog_GetColorBottomLeft(KEP::IMenuDialog* dh);
COLOR Dialog_GetColorBottomRight(KEP::IMenuDialog* dh);
int Dialog_GetCursorLocationX(KEP::IMenuDialog* dh);
int Dialog_GetCursorLocationY(KEP::IMenuDialog* dh);
int Dialog_GetLocationX(KEP::IMenuDialog* dh);
int Dialog_GetLocationY(KEP::IMenuDialog* dh);
int Dialog_GetWidth(KEP::IMenuDialog* dh);
int Dialog_GetHeight(KEP::IMenuDialog* dh);
int Dialog_GetScreenWidth(KEP::IMenuDialog* dh);
int Dialog_GetScreenHeight(KEP::IMenuDialog* dh);
BOOL Dialog_IsLMouseDown(KEP::IMenuDialog* dh);
BOOL Dialog_IsRMouseDown(KEP::IMenuDialog* dh);
BOOL Dialog_GetSnapToEdges(KEP::IMenuDialog* dh);
BOOL Dialog_GetModal(KEP::IMenuDialog* dh);
BOOL Dialog_GetPassthrough(KEP::IMenuDialog* dh);
BOOL Dialog_GetMovable(KEP::IMenuDialog* dh);
BOOL Dialog_GetEnableMouseInput(KEP::IMenuDialog* dh);
BOOL Dialog_GetEnableKeyboardInput(KEP::IMenuDialog* dh);
const char* Dialog_GetScriptName(KEP::IMenuDialog* dh);
KEP::IMenuDialog* Dialog_GetNextDialog(KEP::IMenuDialog* dh);
KEP::IMenuDialog* Dialog_GetPreviousDialog(KEP::IMenuDialog* dh);
unsigned int Dialog_GetLocationFormat(KEP::IMenuDialog* dh);
int Dialog_GetNumTextures();
const char* Dialog_GetTextureByIndex(int index);
const char* Dialog_GetXmlName(KEP::IMenuDialog* dh);

BOOL Dialog_SetMinimized(KEP::IMenuDialog* dh, BOOL minimized);
BOOL Dialog_SetAllControls(KEP::IMenuDialog* dh, BOOL visible, BOOL enabled);
BOOL Dialog_SetEdit(KEP::IMenuDialog* dh, BOOL editEnabled);
BOOL Dialog_SetBackgroundColor(KEP::IMenuDialog* dh, COLOR colorAllCorners);
BOOL Dialog_SetBackgroundColorRaw(KEP::IMenuDialog* dh, RAWCOLOR colorAllCorners);
BOOL Dialog_SetBackgroundColors(KEP::IMenuDialog* dh, COLOR colorTopLeft, COLOR colorTopRight, COLOR colorBottomLeft, COLOR colorBottomRight);
BOOL Dialog_SetBackgroundColorsRaw(KEP::IMenuDialog* dh, RAWCOLOR colorTopLeft, RAWCOLOR colorTopRight, RAWCOLOR colorBottomLeft, RAWCOLOR colorBottomRight);
BOOL Dialog_SetCaptionEnabled(KEP::IMenuDialog* dh, BOOL enable);
BOOL Dialog_SetCaptionHeight(KEP::IMenuDialog* dh, int height);
BOOL Dialog_SetCaptionShadow(KEP::IMenuDialog* dh, BOOL shadow);
BOOL Dialog_SetCaptionText(KEP::IMenuDialog* dh, const char* text);
BOOL Dialog_SetId(KEP::IMenuDialog* dh, int id);
BOOL Dialog_SetLocation(KEP::IMenuDialog* dh, int x, int y);
BOOL Dialog_SetLocationX(KEP::IMenuDialog* dh, int x);
BOOL Dialog_SetLocationY(KEP::IMenuDialog* dh, int y);
BOOL Dialog_SetHeight(KEP::IMenuDialog* dh, int height);
BOOL Dialog_SetWidth(KEP::IMenuDialog* dh, int width);
BOOL Dialog_SetSize(KEP::IMenuDialog* dh, int width, int height);
BOOL Dialog_SetSnapToEdges(KEP::IMenuDialog* dh, BOOL snapToEdges);
BOOL Dialog_SetModal(KEP::IMenuDialog* dh, BOOL modal);
BOOL Dialog_SetCaptureKeys(KEP::IMenuDialog* dh, BOOL capture);
BOOL Dialog_SetPassthrough(KEP::IMenuDialog* dh, BOOL passthrough);
BOOL Dialog_SetMovable(KEP::IMenuDialog* dh, BOOL movable);
BOOL Dialog_SetKeyboardInputEnabled(KEP::IMenuDialog* dh, BOOL enable);
BOOL Dialog_SetMouseInputEnabled(KEP::IMenuDialog* dh, BOOL enable);

BOOL Dialog_SetCloseOnESC(KEP::IMenuDialog* dh, BOOL enable);
BOOL Dialog_GetCloseOnESC(KEP::IMenuDialog* dh);

BOOL Dialog_SetNextDialog(KEP::IMenuDialog* dh, KEP::IMenuDialog* next_dh);
BOOL Dialog_SetPreviousDialog(KEP::IMenuDialog* dh, KEP::IMenuDialog* prev_dh);
BOOL Dialog_SetScriptName(KEP::IMenuDialog* dh, const char* scriptName);
BOOL Dialog_SetLocationFormat(KEP::IMenuDialog* dh, unsigned int format);
BOOL Dialog_SetXmlName(KEP::IMenuDialog* dh, const char* fileName);

/////////////////////////////
// blend color

void BlendColor_Init(KEP::IMenuBlendColor*, COLOR defaultColor, COLOR disabledColor, COLOR hiddenColor);
void BlendColor_InitRaw(KEP::IMenuBlendColor*, RAWCOLOR defaultColor, RAWCOLOR disabledColor, RAWCOLOR hiddenColor);

// accessors
COLOR BlendColor_GetColor(KEP::IMenuBlendColor*, int state);
unsigned long BlendColor_GetColorRaw(KEP::IMenuBlendColor*, int state);

// modifiers
void BlendColor_SetColor(KEP::IMenuBlendColor*, int state, COLOR color);
void BlendColor_SetColorRaw(KEP::IMenuBlendColor*, int state, RAWCOLOR color);

/////////////////////////////
// element

// accessors
KEP::IMenuBlendColor* Element_GetTextureColor(KEP::IMenuElement*);
KEP::IMenuBlendColor* Element_GetFontColor(KEP::IMenuElement*);
const char* Element_GetFontFace(KEP::IMenuElement*, KEP::IMenuDialog*);
int Element_GetFontHeight(KEP::IMenuElement*, KEP::IMenuDialog*);
int Element_GetFontWeight(KEP::IMenuElement*, KEP::IMenuDialog*);
BOOL Element_GetFontItalic(KEP::IMenuElement*, KEP::IMenuDialog*);
BOOL Element_GetFontUnderline(KEP::IMenuElement*, KEP::IMenuDialog*);

const char* Element_GetTextureName(KEP::IMenuElement*, KEP::IMenuDialog*);
unsigned int Element_GetTextFormat(KEP::IMenuElement*);
int Element_GetCoordLeft(KEP::IMenuElement*);
int Element_GetCoordRight(KEP::IMenuElement*);
int Element_GetCoordTop(KEP::IMenuElement*);
int Element_GetCoordBottom(KEP::IMenuElement*);
int Element_GetSliceLeft(KEP::IMenuElement*);
int Element_GetSliceRight(KEP::IMenuElement*);
int Element_GetSliceTop(KEP::IMenuElement*);
int Element_GetSliceBottom(KEP::IMenuElement*);
int Element_GetTextureWidth(KEP::IMenuElement*, KEP::IMenuDialog*);
int Element_GetTextureHeight(KEP::IMenuElement*, KEP::IMenuDialog*);
RECT Element_GetCoords(KEP::IMenuElement*); // currently not registered through scripting due to RECT

// modifers
void Element_SetTextFormat(KEP::IMenuElement*, unsigned long textFormat);
void Element_SetCoords(KEP::IMenuElement*, int left, int top, int right, int bottom);
void Element_SetSlices(KEP::IMenuElement*, int left, int top, int right, int bottom);

// methods
void Element_AddTexture(KEP::IMenuElement*, KEP::IMenuDialog* dh, const char* textureName);
void Element_AddExternalTexture(KEP::IMenuElement*, KEP::IMenuDialog* dh, const char* textureName, const char* progressTextureName);
void Element_AddDynamicTexture(KEP::IMenuElement*, KEP::IMenuDialog* dh, unsigned dynTextureId);
void Element_AddFont(KEP::IMenuElement*, KEP::IMenuDialog* dh, const char* faceName, long height, long weight, BOOL italic);

/////////////////////////////
// control

BOOL Control_SetPassthrough(KEP::IMenuControl* pControl, BOOL passthrough);

KEP::IMenuDialog* Control_GetDialog(KEP::IMenuControl*);
const char* Control_GetName(KEP::IMenuControl*);
BOOL Control_GetEnabled(KEP::IMenuControl*);
BOOL Control_GetMouseOver(KEP::IMenuControl*);
BOOL Control_GetVisible(KEP::IMenuControl*);
BOOL Control_GetFocus(KEP::IMenuControl*);
int Control_GetLocationX(KEP::IMenuControl*);
int Control_GetLocationY(KEP::IMenuControl*);
int Control_GetWidth(KEP::IMenuControl*);
int Control_GetHeight(KEP::IMenuControl*);
unsigned int Control_GetHotkey(KEP::IMenuControl*);
tDATA Control_GetUserData(KEP::IMenuControl*);
KEP::IMenuElement* Control_GetDisplayElement(KEP::IMenuControl* pControl, const char* elementName);
int Control_GetType(KEP::IMenuControl*);
const char* Control_GetComment(KEP::IMenuControl*);
const char* Control_GetToolTip(KEP::IMenuControl*);
int Control_GetGroupId(KEP::IMenuControl*);
const char* Control_GetLinkClicked(KEP::IMenuControl*);
BOOL Control_GetElementTagUsesFont(KEP::IMenuControl* pControl, const char* pElementTag);
BOOL Control_GetElementTagUsesFontState(KEP::IMenuControl* pControl, const char* pElementTag, unsigned int iState);
BOOL Control_GetElementTagUsesTexture(KEP::IMenuControl* pControl, const char* pElementTag);
BOOL Control_GetElementTagUsesTextureState(KEP::IMenuControl* pControl, const char* pElementTag, unsigned int iState);

// control accessors
KEP::IMenuStatic* Control_GetStatic(KEP::IMenuControl*);
KEP::IMenuImage* Control_GetImage(KEP::IMenuControl*);
KEP::IMenuButton* Control_GetButton(KEP::IMenuControl*);
KEP::IMenuCheckBox* Control_GetCheckBox(KEP::IMenuControl*);
KEP::IMenuRadioButton* Control_GetRadioButton(KEP::IMenuControl*);
KEP::IMenuComboBox* Control_GetComboBox(KEP::IMenuControl*);
KEP::IMenuSlider* Control_GetSlider(KEP::IMenuControl*);
KEP::IMenuEditBox* Control_GetEditBox(KEP::IMenuControl*);
KEP::IMenuListBox* Control_GetListBox(KEP::IMenuControl*);

// modifiers
void Control_SetName(KEP::IMenuControl*, const char* name);
void Control_SetNameUnconflicting(KEP::IMenuControl*, const char* name);
void Control_SetEnabled(KEP::IMenuControl*, BOOL enable);
void Control_SetVisible(KEP::IMenuControl*, BOOL visible);
void Control_SetFocus(KEP::IMenuControl*);
void Control_SetLocation(KEP::IMenuControl*, int x, int y);
void Control_SetLocationX(KEP::IMenuControl*, int x);
void Control_SetLocationY(KEP::IMenuControl*, int y);
void Control_SetSize(KEP::IMenuControl*, int width, int height);
void Control_SetHotkey(KEP::IMenuControl*, unsigned int hotkey);
void Control_SetUserData(KEP::IMenuControl*, tDATA data);
void Control_SetEditing(KEP::IMenuControl*, BOOL enable);
void Control_SetComment(KEP::IMenuControl*, const char* text);
void Control_SetToolTip(KEP::IMenuControl*, const char* text);
void Control_SetGroupId(KEP::IMenuControl*, int groupId);
void Control_SetClipRect(KEP::IMenuControl*, int left, int top, int right, int bottom);

// methods
BOOL Control_ContainsPoint(KEP::IMenuControl*, int x, int y);

/////////////////////////////
// static control

// accessors
KEP::IMenuControl* Static_GetControl(KEP::IMenuStatic*);
KEP::IMenuElement* Static_GetDisplayElement(KEP::IMenuStatic*);
const char* Static_GetText(KEP::IMenuStatic*);
BOOL Static_GetShadow(KEP::IMenuStatic*);
int Static_GetStringWidth(KEP::IMenuStatic*, const char* text);
int Static_GetStringHeight(KEP::IMenuStatic*, const char* text);
void Static_GetStringDims(KEP::IMenuStatic*, const char* text, int& width, int& height);
BOOL Static_GetUseHTML(KEP::IMenuStatic* pStatic);

// modifiers
void Static_SetText(KEP::IMenuStatic*, const char* text);
void Static_SetShadow(KEP::IMenuStatic*, BOOL shadow);
void Static_SetUseHTML(KEP::IMenuStatic* pStatic, BOOL useHTML);

/////////////////////////////
// image control

// accessors
KEP::IMenuControl* Image_GetControl(KEP::IMenuImage*);
KEP::IMenuElement* Image_GetDisplayElement(KEP::IMenuImage*);

// methods
void Image_SetColor(KEP::IMenuImage*, COLOR textureColor);
void Image_SetColorRaw(KEP::IMenuImage*, RAWCOLOR textureColor);

/////////////////////////////
// button control

// accessors
KEP::IMenuControl* Button_GetControl(KEP::IMenuButton*);
KEP::IMenuStatic* Button_GetStatic(KEP::IMenuButton*);
KEP::IMenuElement* Button_GetNormalDisplayElement(KEP::IMenuButton*);
KEP::IMenuElement* Button_GetMouseOverDisplayElement(KEP::IMenuButton*);
KEP::IMenuElement* Button_GetPressedDisplayElement(KEP::IMenuButton*);
KEP::IMenuElement* Button_GetFocusedDisplayElement(KEP::IMenuButton*);
KEP::IMenuElement* Button_GetDisabledDisplayElement(KEP::IMenuButton*);

void Button_SetSimpleMode(KEP::IMenuButton*, BOOL enabled);
BOOL Button_GetSimpleMode(KEP::IMenuButton*);

/////////////////////////////
// checkbox control

// accessors
KEP::IMenuControl* CheckBox_GetControl(KEP::IMenuCheckBox*);
KEP::IMenuButton* CheckBox_GetButton(KEP::IMenuCheckBox*);
KEP::IMenuElement* CheckBox_GetBoxDisplayElement(KEP::IMenuCheckBox*);
KEP::IMenuElement* CheckBox_GetCheckDisplayElement(KEP::IMenuCheckBox*);
BOOL CheckBox_GetChecked(KEP::IMenuCheckBox*);
LONG CheckBox_GetIconWidth(KEP::IMenuCheckBox*);

// modifiers
void CheckBox_SetChecked(KEP::IMenuCheckBox*, BOOL checked);
void CheckBox_SetIconWidth(KEP::IMenuCheckBox*, LONG iconWidth);

/////////////////////////////
// radiobutton control

// accessors
KEP::IMenuControl* RadioButton_GetControl(KEP::IMenuRadioButton*);
KEP::IMenuCheckBox* RadioButton_GetCheckBox(KEP::IMenuRadioButton*);
unsigned int RadioButton_GetButtonGroup(KEP::IMenuRadioButton*);

// modifiers
void RadioButton_SetChecked(KEP::IMenuRadioButton*, BOOL checked);
void RadioButton_SetButtonGroup(KEP::IMenuRadioButton*, unsigned int group);

/////////////////////////////
// combobox control

// accessors
KEP::IMenuControl* ComboBox_GetControl(KEP::IMenuComboBox*);
KEP::IMenuScrollBar* ComboBox_GetScrollBar(KEP::IMenuComboBox*);
KEP::IMenuElement* ComboBox_GetMainDisplayElement(KEP::IMenuComboBox*);
KEP::IMenuElement* ComboBox_GetButtonDisplayElement(KEP::IMenuComboBox*);
KEP::IMenuElement* ComboBox_GetDropDownDisplayElement(KEP::IMenuComboBox*);
KEP::IMenuElement* ComboBox_GetSelectionDisplayElement(KEP::IMenuComboBox*);
int ComboBox_GetDropHeight(KEP::IMenuComboBox*);
int ComboBox_GetScrollBarWidth(KEP::IMenuComboBox*);
const char* ComboBox_GetItemText(KEP::IMenuComboBox*, int index);
tDATA ComboBox_GetItemData(KEP::IMenuComboBox*, const char* text);
tDATA ComboBox_GetItemDataByIndex(KEP::IMenuComboBox*, unsigned int index);
tDATA ComboBox_GetSelectedData(KEP::IMenuComboBox*);
const char* ComboBox_GetSelectedText(KEP::IMenuComboBox*);
int ComboBox_GetSelectedIndex(KEP::IMenuComboBox*);
unsigned int ComboBox_GetNumItems(KEP::IMenuComboBox*);

// modifiers
void ComboBox_SetTextColor(KEP::IMenuComboBox*, COLOR color);
void ComboBox_SetTextColorRaw(KEP::IMenuComboBox*, RAWCOLOR color);
void ComboBox_SetDropHeight(KEP::IMenuComboBox*, int height);
void ComboBox_SetScrollBarWidth(KEP::IMenuComboBox*, int width);
BOOL ComboBox_SetSelectedByIndex(KEP::IMenuComboBox*, unsigned int index);
BOOL ComboBox_SetSelectedByText(KEP::IMenuComboBox*, const char* text);
BOOL ComboBox_SetSelectedByData(KEP::IMenuComboBox*, tDATA data);

// methods
BOOL ComboBox_AddItem(KEP::IMenuComboBox*, const char* text, tDATA data);
void ComboBox_RemoveAllItems(KEP::IMenuComboBox*);
void ComboBox_RemoveItem(KEP::IMenuComboBox*, unsigned int index);
BOOL ComboBox_ContainsItem(KEP::IMenuComboBox*, const char* text);
int ComboBox_FindItem(KEP::IMenuComboBox*, const char* text);

/////////////////////////////
// slider control

// accessors
KEP::IMenuControl* Slider_GetControl(KEP::IMenuSlider*);
KEP::IMenuElement* Slider_GetTrackDisplayElement(KEP::IMenuSlider*);
KEP::IMenuElement* Slider_GetButtonDisplayElement(KEP::IMenuSlider*);
int Slider_GetValue(KEP::IMenuSlider*);
int Slider_GetRangeMin(KEP::IMenuSlider*);
int Slider_GetRangeMax(KEP::IMenuSlider*);

// modifiers
void Slider_SetValue(KEP::IMenuSlider*, int value);
void Slider_SetRange(KEP::IMenuSlider*, int min, int max);

/////////////////////////////
// edit box control

// accessors
KEP::IMenuControl* EditBox_GetControl(KEP::IMenuEditBox* pEditBox);
KEP::IMenuScrollBar* EditBox_GetScrollBar(KEP::IMenuEditBox*);
int EditBox_GetScrollBarWidth(KEP::IMenuEditBox*);
KEP::IMenuElement* EditBox_GetTextAreaDisplayElement(KEP::IMenuEditBox* pEditBox);
KEP::IMenuElement* EditBox_GetTopLeftBorderDisplayElement(KEP::IMenuEditBox* pEditBox);
KEP::IMenuElement* EditBox_GetTopBorderDisplayElement(KEP::IMenuEditBox* pEditBox);
KEP::IMenuElement* EditBox_GetTopRightBorderDisplayElement(KEP::IMenuEditBox* pEditBox);
KEP::IMenuElement* EditBox_GetLeftBorderDisplayElement(KEP::IMenuEditBox* pEditBox);
KEP::IMenuElement* EditBox_GetRightBorderDisplayElement(KEP::IMenuEditBox* pEditBox);
KEP::IMenuElement* EditBox_GetLowerLeftBorderDisplayElement(KEP::IMenuEditBox* pEditBox);
KEP::IMenuElement* EditBox_GetLowerBorderDisplayElement(KEP::IMenuEditBox* pEditBox);
KEP::IMenuElement* EditBox_GetLowerRightBorderDisplayElement(KEP::IMenuEditBox* pEditBox);
const char* EditBox_GetText(KEP::IMenuEditBox* pEditBox);
int EditBox_GetTextLength(KEP::IMenuEditBox* pEditBox);
COLOR EditBox_GetTextColor(KEP::IMenuEditBox* pEditBox);
RAWCOLOR EditBox_GetTextColorRaw(KEP::IMenuEditBox* pEditBox);
COLOR EditBox_GetSelectedTextColor(KEP::IMenuEditBox* pEditBox);
RAWCOLOR EditBox_GetSelectedTextColorRaw(KEP::IMenuEditBox* pEditBox);
COLOR EditBox_GetSelectedBackColor(KEP::IMenuEditBox* pEditBox);
RAWCOLOR EditBox_GetSelectedBackColorRaw(KEP::IMenuEditBox* pEditBox);
COLOR EditBox_GetCaretColor(KEP::IMenuEditBox* pEditBox);
RAWCOLOR EditBox_GetCaretColorRaw(KEP::IMenuEditBox* pEditBox);
int EditBox_GetBorderWidth(KEP::IMenuEditBox* pEditBox);
int EditBox_GetSpacing(KEP::IMenuEditBox* pEditBox);
int EditBox_GetFont(KEP::IMenuEditBox* pEditBox);
BOOL EditBox_GetEditable(KEP::IMenuEditBox* pEditBox);
BOOL EditBox_GetPassword(KEP::IMenuEditBox* pEditBox);

// DRF - DEPRECATED
BOOL EditBox_GetMultiline(KEP::IMenuEditBox* pEditBox);

void EditBox_SetFont(KEP::IMenuEditBox* pEditBox, int fontindex);
void EditBox_SetText(KEP::IMenuEditBox* pEditBox, const char* text, BOOL selected);
void EditBox_SetTextColor(KEP::IMenuEditBox* pEditBox, COLOR color);
void EditBox_SetTextColorRaw(KEP::IMenuEditBox* pEditBox, RAWCOLOR color);
bool EditBox_SetSelected(KEP::IMenuEditBox* pEditBox, int startIndex, int endIndex);
bool EditBox_SetSelectedAll(KEP::IMenuEditBox* pEditBox);
void EditBox_SetSelectedTextColor(KEP::IMenuEditBox* pEditBox, COLOR color);
void EditBox_SetSelectedTextColorRaw(KEP::IMenuEditBox* pEditBox, RAWCOLOR color);
void EditBox_SetSelectedBackColor(KEP::IMenuEditBox* pEditBox, COLOR color);
void EditBox_SetSelectedBackColorRaw(KEP::IMenuEditBox* pEditBox, RAWCOLOR color);
void EditBox_SetCaretColor(KEP::IMenuEditBox* pEditBox, COLOR color);
void EditBox_SetCaretColorRaw(KEP::IMenuEditBox* pEditBox, RAWCOLOR color);
void EditBox_SetBorderWidth(KEP::IMenuEditBox* pEditBox, int width);
void EditBox_SetSpacing(KEP::IMenuEditBox* pEditBox, int spacing);
void EditBox_SetEditable(KEP::IMenuEditBox* pEditBox, BOOL editable);
void EditBox_SetPassword(KEP::IMenuEditBox* pEditBox, BOOL password);

// DRF - DEPRECATED
void EditBox_SetMultiline(KEP::IMenuEditBox* pEditBox, BOOL multiline);

// methods
void EditBox_ClearText(KEP::IMenuEditBox* pEditBox);
void EditBox_SetMaxCharCount(KEP::IMenuEditBox* pEditBox, int maxCount);

/////////////////////////////
// listbox control

// accessors
KEP::IMenuControl* ListBox_GetControl(KEP::IMenuListBox*);
KEP::IMenuScrollBar* ListBox_GetScrollBar(KEP::IMenuListBox*);
int ListBox_GetScrollBarWidth(KEP::IMenuListBox*);
KEP::IMenuElement* ListBox_GetMainDisplayElement(KEP::IMenuListBox*);
KEP::IMenuElement* ListBox_GetSelectionDisplayElement(KEP::IMenuListBox*);
BOOL ListBox_GetMultiSelection(KEP::IMenuListBox*);
BOOL ListBox_GetDragSelection(KEP::IMenuListBox*);
int ListBox_GetBorder(KEP::IMenuListBox*);
int ListBox_GetLeftMargin(KEP::IMenuListBox*);
int ListBox_GetRightMargin(KEP::IMenuListBox*);
const char* ListBox_GetItemText(KEP::IMenuListBox*, int index);
const char* ListBox_GetSelectedItemText(KEP::IMenuListBox*);
tDATA ListBox_GetItemData(KEP::IMenuListBox*, int index);
tDATA ListBox_GetSelectedItemData(KEP::IMenuListBox*);
int ListBox_GetSelectedItemIndex(KEP::IMenuListBox*);
int ListBox_GetMultiSelectedItemIndex(KEP::IMenuListBox*, int index);
int ListBox_GetRowHeight(KEP::IMenuListBox*);
int ListBox_GetSize(KEP::IMenuListBox*);

// modifiers
void ListBox_SetMultiSelection(KEP::IMenuListBox*, BOOL multiSelection);
void ListBox_SetHTMLEnabled(KEP::IMenuListBox*, BOOL htmlsupport);
void ListBox_SetDragSelection(KEP::IMenuListBox*, BOOL dragSelection);
void ListBox_SetScrollBarWidth(KEP::IMenuListBox*, int width);
void ListBox_SetBorder(KEP::IMenuListBox*, int border);
void ListBox_SetLeftMargin(KEP::IMenuListBox*, int margin);
void ListBox_SetRightMargin(KEP::IMenuListBox*, int margin);
void ListBox_SetMargins(KEP::IMenuListBox*, int margin);
void ListBox_SetRowHeight(KEP::IMenuListBox*, int height);
void ListBox_SetItemText(KEP::IMenuListBox*, int index, const char* text);

// methods
BOOL ListBox_AddItem(KEP::IMenuListBox*, const char* text, tDATA data);
BOOL ListBox_InsertItem(KEP::IMenuListBox*, int index, const char* text, tDATA data);
void ListBox_RemoveAllItems(KEP::IMenuListBox*);
void ListBox_RemoveItem(KEP::IMenuListBox*, unsigned int index);
void ListBox_SelectItem(KEP::IMenuListBox*, int index);
int ListBox_FindItem(KEP::IMenuListBox*, tDATA data);
void ListBox_ClearSelection(KEP::IMenuListBox*);

/////////////////////////////
// scroll bar control

// accessors
KEP::IMenuControl* ScrollBar_GetControl(KEP::IMenuScrollBar*);
int ScrollBar_GetTrackStart(KEP::IMenuScrollBar*);
int ScrollBar_GetTrackEnd(KEP::IMenuScrollBar*);
int ScrollBar_GetTrackPos(KEP::IMenuScrollBar*);
int ScrollBar_GetPageSize(KEP::IMenuScrollBar*);
BOOL ScrollBar_GetShowThumb(KEP::IMenuScrollBar*);
KEP::IMenuElement* ScrollBar_GetTrackDisplayElement(KEP::IMenuScrollBar*);
KEP::IMenuElement* ScrollBar_GetUpArrowDisplayElement(KEP::IMenuScrollBar*);
KEP::IMenuElement* ScrollBar_GetDownArrowDisplayElement(KEP::IMenuScrollBar*);
KEP::IMenuElement* ScrollBar_GetButtonDisplayElement(KEP::IMenuScrollBar*);
KEP::IMenuControl* ScrollBar_GetParent(KEP::IMenuScrollBar*);

// modifiers
void ScrollBar_SetTrackRange(KEP::IMenuScrollBar*, int nStart, int nEnd);
void ScrollBar_SetTrackPos(KEP::IMenuScrollBar*, int nPosition);
void ScrollBar_SetPageSize(KEP::IMenuScrollBar*, int nPageSize);

/** DRF
 * Enables/disables the mouse cursor.
 */
BOOL Cursor_SetEnabled(BOOL enable);

/** DRF
 * Returns the current mouse cursor location (x, y).
 */
int Cursor_GetLocationX();
int Cursor_GetLocationY();

#ifdef __cplusplus
}
#endif
