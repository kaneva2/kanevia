///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <crtdbg.h>

struct WeakRef {
	WeakRef(class WeakRefObject *object) :
			_target(object),
			_nref(0) {
	}

	class WeakRefObject *_target;

	int _nref; // This is the number of script VMs which reference this WeakRef
		// It is here so we know when it is safe to dispose of this WeakRef
		// It is NOT # of references to _target!
};

class WeakRefObject {
public:
	WeakRefObject() :
			_weakRef(NULL) {
	}

	virtual ~WeakRefObject() {
		if (_weakRef) {
			_ASSERTE(_weakRef->_target == this);
			_weakRef->_target = NULL;
		}
	}

	WeakRef *GetWeakRef() {
		if (!_weakRef)
			_weakRef = new WeakRef(this);

		return _weakRef;
	}

	void ClearWeakRef() {
		_weakRef = NULL;
	}

private:
	WeakRef *_weakRef;
};
