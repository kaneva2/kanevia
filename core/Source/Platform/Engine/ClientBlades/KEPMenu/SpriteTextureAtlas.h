///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <map>
#include <vector>
#include <set>
#include <assert.h>

struct IDirect3DTexture9;

namespace KEP {

class D3D9DeviceWrapper;

#define SPRITE_DEFAULT_FTL 50 // See SpriteInfo::m_FTL

struct SpriteInfo {
public:
	IDirect3DTexture9* m_texture; // Original sprite texture
	unsigned m_texWidth, m_texHeight; // Original sprite texture dimension in pixels
	RECT m_layoutRect; // rect used for texture layout, measured in number of cells
	int m_texRevision; // See D3D9TextureWrapper::m_revision

private:
	unsigned m_FTL; // "Frames-to-live": sprites not rendered during the past several frames will be dropped from master sheet

public:
	SpriteInfo(IDirect3DTexture9* pSpriteTexture = nullptr, unsigned width = 0, unsigned height = 0, unsigned CELL_SIZE = 1, unsigned CELL_PADDING = 0) :
			m_texture(pSpriteTexture), m_texWidth(width), m_texHeight(height), m_texRevision(-1), m_FTL(SPRITE_DEFAULT_FTL) {
		m_layoutRect.left = 0;
		m_layoutRect.top = 0;
		m_layoutRect.right = (m_texWidth + CELL_PADDING + CELL_SIZE - 1) / CELL_SIZE;
		m_layoutRect.bottom = (m_texHeight + CELL_PADDING + CELL_SIZE - 1) / CELL_SIZE;
	}

	void invalidate() {
		// Reset m_texRevision so that it will be blt into master sheet next time
		m_texRevision = -1;
	}

	void resetFTL() {
		m_FTL = SPRITE_DEFAULT_FTL;
	}

	void decFTL() {
		if (m_FTL > 0) {
			m_FTL--;
		}
	}

	bool isDead() {
		return m_FTL == 0;
	}
};

class SpriteTextureAtlas {
private:
	// Disable copy constructor and assign operator
	SpriteTextureAtlas(const SpriteTextureAtlas& rhs);
	void operator=(const SpriteTextureAtlas& rhs);

public:
	SpriteTextureAtlas() :
			m_cells(nullptr), m_maxWidth(0), m_maxHeight(0), m_masterWidth(0), m_masterHeight(0), m_masterTexture(nullptr), m_layoutNeeded(false) {}
	~SpriteTextureAtlas() {
		if (m_masterTexture) {
			m_masterTexture->Release();
			m_masterTexture = nullptr;
		}
		if (m_cells) {
			delete[] m_cells;
			m_cells = nullptr;
		}
	}

	void beginFrame(); // Begin-of-frame update
	void endFrame(); // End-of-frame update

	void addSprite(IDirect3DTexture9* pSpriteTexture);
	const SpriteInfo* getSpriteInfo(IDirect3DTexture9* pSpriteTexture) const;

	bool updateMasterSheet(D3D9DeviceWrapper* device);
	void updateMasterSheetSprite(D3D9DeviceWrapper* device, const D3DLOCKED_RECT& masterLockedRect, const SpriteInfo& spriteInfo);

	IDirect3DTexture9* getMasterTexture() const { return m_masterTexture; }
	RECT getMasterTextureRect() const { return RECT{ 0, 0, LONG(m_masterWidth * CELL_SIZE), LONG(m_masterHeight * CELL_SIZE) }; }

	unsigned getCellSize() const { return CELL_SIZE; }

protected:
	const static unsigned CELL_SIZE = 40;
	const static unsigned CELL_PADDING = 8;

	struct CellInfo {
		bool m_occupied;
		unsigned m_nextX;
		unsigned m_nextY;
	};

	void allocateCells(size_t width, size_t height) {
		if (m_cells) {
			delete[] m_cells;
		}
		m_maxWidth = width;
		m_maxHeight = height;
		m_cells = new CellInfo[m_maxWidth * m_maxHeight];
		memset(m_cells, 0, sizeof(CellInfo) * m_maxWidth * m_maxHeight);
	}

	void markCell(size_t x, size_t y, unsigned nextX, unsigned nextY) {
		assert(x < m_maxWidth && y < m_maxHeight);
		m_cells[x * m_maxHeight + y].m_occupied = true;
		m_cells[x * m_maxHeight + y].m_nextX = nextX;
		m_cells[x * m_maxHeight + y].m_nextY = nextY;
	}

	const CellInfo& getCell(size_t x, size_t y) {
		assert(x < m_maxWidth && y < m_maxHeight);
		return m_cells[x * m_maxHeight + y];
	}

	bool relayoutMasterSheet();
	bool layoutConstrained(unsigned candWidth, unsigned candHeight, std::vector<SpriteInfo>& sprites, unsigned& actualW, unsigned& actualH);

private:
	std::string m_menuName; // name of the owner, for logging purpose
	CellInfo* m_cells;
	std::map<IDirect3DTexture9*, SpriteInfo> m_sprites;
	std::set<IDirect3DTexture9*> m_unsupportedSprites;

	size_t m_maxWidth, m_maxHeight;
	size_t m_masterWidth, m_masterHeight;
	IDirect3DTexture9* m_masterTexture;

	bool m_layoutNeeded;
};

} // namespace KEP
