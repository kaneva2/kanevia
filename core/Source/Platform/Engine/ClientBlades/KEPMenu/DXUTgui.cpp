///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "dxstdafx.h"
#include <TinyXML/tinyxml.h>
#include "DXUTgui.h"
#include "DXUTFlashPlayer.h"
#include "tinyxmlUtil.h"
#include "Handler.h"
#include "KEPMenu.h"
#include "D3DXSpriteWrapper.h"
#include "D3D9DeviceWrapper.h"
#include "SpriteTextureAtlas.h"
#include "Event/EventSystem/dispatcher.h"
#include "UnicodeWindowsHelpers.h"
#include <Log4CPlus\loggingmacros.h>
#include "common\KEPUTIL\Helpers.h"
#include "common\include\Rect.h"
#include "js.h"
#include "jsEnumFiles.h"
#include "common\KEPUtil\WebCall.h"

namespace KEP {

static LogInstance("MenuSystem");

// Minimum scroll bar thumb size
#define SCROLLBAR_MINTHUMBSIZE 8

// Delay and repeat period when clicking on the scroll bar arrows
#define SCROLLBAR_ARROWCLICK_DELAY 0.33
#define SCROLLBAR_ARROWCLICK_REPEAT 0.05

#define SELECTION_HANDLE_RECT \
	{ 0, 0, 6, 6 }

// DXUT_MAX_EDITBOXLENGTH is the maximum string length allowed in edit boxes,
// including the NULL terminator.
//
// Uniscribe does not support strings having bigger-than-16-bits length.
// This means that the string must be less than 65536 characters long,
// including the NULL terminator.
#define DXUT_MAX_EDITBOXLENGTH 0xFFFF

static const LPCSTR DEFAULT_FONT_FACE_NAME = "Arial";
static const long DEFAULT_FONT_HEIGHT = 14;
static const long DEFAULT_FONT_WEIGHT = FW_NORMAL;

static const LPCSTR ELEMENTS_TAG = "elements";

static const LPCSTR DIALOG_TAG = "dialog";
static const LPCSTR SCRIPT_TAG = "script";
static const LPCSTR TITLEBAR_TAG = "titlebar";
static const LPCSTR TOPMOST_TAG = "topmost";
static const LPCSTR TEXT_TAG = "text";
static const LPCSTR COMMENT_TAG = "comment";
static const LPCSTR TOOLTIP_TAG = "tooltip";
static const LPCSTR XMLCSS_TAG = "xml_css";
static const LPCSTR ENABLED_TAG = "enabled";
static const LPCSTR HEIGHT_TAG = "height";
static const LPCSTR DEFAULT_TEXTURE_TAG = "defaultTexture";
static const LPCSTR DEFAULT_FONT_TAG = "defaultFont";
static const LPCSTR ID_TAG = "ID";
static const LPCSTR MOVABLE_TAG = "movable";
static const LPCSTR MINIMIZED_TAG = "minimized";
static const LPCSTR MODAL_TAG = "modal";
static const LPCSTR SNAP_TO_EDGES_TAG = "snapToEdges";
static const LPCSTR LOCATION_TAG = "location";
static const LPCSTR FORMAT_TAG = "format";
static const LPCSTR X_TAG = "x";
static const LPCSTR Y_TAG = "y";
static const LPCSTR X_OFFSET_TAG = "xOffset";
static const LPCSTR Y_OFFSET_TAG = "yOffset";
static const LPCSTR SIZE_TAG = "size";
static const LPCSTR WIDTH_TAG = "width";
static const LPCSTR ENABLE_MOUSE_TAG = "enableMouse";
static const LPCSTR ENABLE_KEYBOARD_TAG = "enableKeyboard";
static const LPCSTR CLOSE_ON_ESC_TAG = "closeOnESC";
static const LPCSTR BACKGROUND_COLORS_TAG = "backgroundColors";
static const LPCSTR ALL_CORNERS_TAG = "allCorners";
static const LPCSTR TOP_LEFT_TAG = "topLeft";
static const LPCSTR TOP_RIGHT_TAG = "topRight";
static const LPCSTR BOTTOM_LEFT_TAG = "bottomLeft";
static const LPCSTR BOTTOM_RIGHT_TAG = "bottomRight";

static const LPCSTR STATIC_TAG = "static";
static const LPCSTR IMAGE_TAG = "image";
static const LPCSTR BUTTON_TAG = "button";
static const LPCSTR CHECK_BOX_TAG = "checkBox";
static const LPCSTR RADIO_BUTTON_TAG = "radioButton";
static const LPCSTR COMBO_BOX_TAG = "comboBox";
static const LPCSTR SLIDER_TAG = "slider";
static const LPCSTR SCROLL_BAR_TAG = "scrollBar";
static const LPCSTR EDIT_BOX_TAG = "editBox";
static const LPCSTR SIMPLE_EDIT_BOX_TAG = "simpleEditBox";
static const LPCSTR LIST_BOX_TAG = "listBox";

static const LPCSTR NAME_TAG = "name";
static const LPCSTR SHADOW_TAG = "shadow";
static const LPCSTR USE_HTML_TAG = "useHTML";
static const LPCSTR VISIBLE_TAG = "visible";
static const LPCSTR DEFAULT_TAG = "default";
static const LPCSTR FOCUS_TAG = "focus";

static const LPCSTR TEXT_FORMAT_TAG = "textFormat";
static const LPCSTR TEXT_OFFSET_X_TAG = "textOffsetX";
static const LPCSTR TEXT_OFFSET_Y_TAG = "textOffsetY";
static const LPCSTR FONT_COLOR_TAG = "fontColor";
static const LPCSTR TEXTURE_COLOR_TAG = "textureColor";
static const LPCSTR NORMAL_TAG = "normal";
static const LPCSTR DISABLED_TAG = "disabled";
static const LPCSTR HIDDEN_TAG = "hidden";
static const LPCSTR MOUSEOVER_TAG = "mouseover";
static const LPCSTR PRESSED_TAG = "pressed";
static const LPCSTR FONT_TAG = "font";
static const LPCSTR WEIGHT_TAG = "weight";
static const LPCSTR FACENAME_TAG = "faceName";
static const LPCSTR ITALIC_TAG = "italic";
static const LPCSTR UNDERLINE_TAG = "underline";

static const LPCSTR TEXTURE_TAG = "texture";
static const LPCSTR COORDS_TAG = "coords";
static const LPCSTR SLICES_TAG = "slices";
static const LPCSTR XLEFT_TAG = "xLeft";
static const LPCSTR YTOP_TAG = "yTop";
static const LPCSTR XRIGHT_TAG = "xRight";
static const LPCSTR YBOTTOM_TAG = "yBottom";

static const LPCSTR CHECKED_TAG = "checked";
static const LPCSTR ICONWIDTH_TAG = "iconWidth";
static const LPCSTR HOTKEY_TAG = "hotkey";
static const LPCSTR GROUP_TAG = "group";

static const LPCSTR DROP_HEIGHT_TAG = "dropHeight";
static const LPCSTR SCROLL_BAR_WIDTH_TAG = "scrollBarWidth";
static const LPCSTR ITEMS_TAG = "items";
static const LPCSTR ITEM_TAG = "item";
static const LPCSTR DATA_TAG = "data";

static const LPCSTR MIN_TAG = "min";
static const LPCSTR MAX_TAG = "max";
static const LPCSTR VALUE_TAG = "value";

static const LPCSTR START_TAG = "start";
static const LPCSTR END_TAG = "end";
static const LPCSTR POSITION_TAG = "position";
static const LPCSTR PAGE_SIZE_TAG = "pageSize";

static const LPCSTR PASSWORD_TAG = "password";
static const LPCSTR HIDE_CARET_TAG = "hideCaret";
static const LPCSTR BORDER_WIDTH_TAG = "borderWidth";
static const LPCSTR SPACING_TAG = "spacing";
static const LPCSTR TEXT_COLOR_TAG = "textColor";
static const LPCSTR SELECTED_TEXT_COLOR_TAG = "selectedTextColor";
static const LPCSTR SELECTED_BACK_COLOR_TAG = "selectedBackColor";
static const LPCSTR CARET_COLOR_TAG = "caretColor";

static const LPCSTR EDITABLE_TAG = "editable";
static const LPCSTR MULTI_LINE_TAG = "multiLine";

static const LPCSTR MULTI_SELECTION_TAG = "multiSelection";
static const LPCSTR BORDER_TAG = "border";
static const LPCSTR MARGIN_TAG = "margin";
static const LPCSTR MARGINS_TAG = "margins";
static const LPCSTR LEFT_TAG = "left";
static const LPCSTR RIGHT_TAG = "right";
static const LPCSTR ROW_HEIGHT_TAG = "rowHeight";
static const LPCSTR DRAG_SELECTION_TAG = "dragSelection";

static const LPCSTR NEXT_CONTROL_VALUE_TAG = "nextControlValue";

// default control name/text
static const LPCSTR BUTTON_DEFAULT_NAME = "Button";
static const LPCSTR BUTTON_DEFAULT_TEXT = "Button";

static const LPCSTR STATIC_DEFAULT_NAME = "Static";
static const LPCSTR STATIC_DEFAULT_TEXT = "Static";

static const LPCSTR CHECK_BOX_DEFAULT_NAME = "CheckBox";
static const LPCSTR CHECK_BOX_DEFAULT_TEXT = "CheckBox";

static const LPCSTR RADIO_BUTTON_DEFAULT_NAME = "RadioButton";
static const LPCSTR RADIO_BUTTON_DEFAULT_TEXT = "Radio";

static const LPCSTR COMBO_BOX_DEFAULT_NAME = "ComboBox";

static const LPCSTR SLIDER_DEFAULT_NAME = "Slider";

static const LPCSTR EDIT_BOX_DEFAULT_NAME = "EditBox";
static const LPCSTR EDIT_BOX_DEFAULT_TEXT = "Sample edit box";

static const LPCSTR IME_EDIT_BOX_DEFAULT_TEXT = "Sample edit box";

static const LPCSTR LIST_BOX_DEFAULT_NAME = "ListBox";

static const LPCSTR SCROLL_BAR_DEFAULT_NAME = "ScrollBar";

static const LPCSTR IMAGE_DEFAULT_NAME = "Image";

static const LPCSTR DEFAULT_FLASH_TEXT = "KanevaIdle.swf";

static std::list<IDirect3DTexture9*> g_refedTexture;

std::string IMenuDialog::s_sDefaultMenuDir = "";

CDXUTDialogResourceManager* DXUTGetGlobalDialogResourceManager() {
	static CDXUTDialogResourceManager manager;
	return &manager;
}

#define ASSERT_GDRM(...)                               \
	auto pGDRM = DXUTGetGlobalDialogResourceManager(); \
	if (!pGDRM)                                        \
		return __VA_ARGS__;

// DRF - Added Failed Texture Handling
#define TIME_REFRESH_MS 1000
static TimeMs timeRefresh = 0;
bool NeedsWindowRefresh() {
	return (timeRefresh && ((fTime::TimeMs() - timeRefresh) > TIME_REFRESH_MS));
}
void FlagWindowRefresh() {
	timeRefresh = fTime::TimeMs();
}
void FlagWindowRefreshed() {
	timeRefresh = 0;
}

double CDXUTDialog::s_fTimeRefresh = 0.0f;
IMenuControl* CDXUTDialog::s_pControlFocus = NULL; // The control which has focus
IMenuControl* CDXUTDialog::s_pControlPressed = NULL; // The control currently pressed
IMenuControl* CDXUTDialog::s_pControlSelected = NULL; // The control currently selected (for editing)
bool CDXUTDialog::m_LMouseDown = false;
bool CDXUTDialog::m_RMouseDown = false;

DWORD DXUT_SCREEN_VERTEX::FVF = D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1;

static bool _VALID_DIALOG(const IMenuDialog* pDialog) {
	return (REF_PTR_VALID(pDialog) && pDialog->IsFullyConstructed());
}

static bool VALID_DIALOG(const IMenuDialog* pDialog) {
	return (_VALID_DIALOG(pDialog) && !pDialog->IsPendingDeletion());
}

///////////////////////////////////////////////////////////////////////////////
// DRF - Experminetal TranslateTo Functions
///////////////////////////////////////////////////////////////////////////////

typedef std::map<std::string, std::string> TT_MAP;
static std::string s_ttLang;
static TT_MAP s_ttMap;
bool DXUT_TranslateToSetLang(const std::string& ttLang) {
	s_ttLang = ttLang;
	return true;
}
static std::string TranslateToCacheFind(const std::string& fromStr) {
	if (fromStr.empty() || fromStr[0] < 'A')
		return fromStr;
	auto it = s_ttMap.find(fromStr);
	return (it == s_ttMap.end()) ? "" : it->second;
}
static void TranslateToCacheAdd(const std::string& fromStr, const std::string& toStr) {
	s_ttMap[fromStr] = toStr;
}
static std::string TranslateTo(const std::string& fromStr, const std::string& lang) {
	std::string toStr = TranslateToCacheFind(fromStr);
	if (!toStr.empty())
		return toStr;
	WebCallOpt wco;
	wco.url = "https://lc-api.sdl.com/translate";
	wco.response = true;
	static std::string header = "Content-type: application/json\r\nAuthorization: LC apiKey=P1NB100TsRqh6eXN983ptA%3D%3D";
	static std::string data;
	StrBuild(data, "{\"text\":\"" << fromStr << "\", \"from\":\"eng\", \"to\":\"" << lang << "\"}");
	auto pWCA = WebCaller::PostAndWait(wco, header, data);
	if (!pWCA->IsOk()) {
		LogError("WebCaller::PostAndWait() FAILED - fromStr=" << fromStr << " lang=" << lang << " '" << pWCA->ErrorStr() << "'");
		return fromStr;
	}
	std::string responseStr = pWCA->ResponseStr();
	toStr = StrStripRightLast(responseStr, "\"");
	toStr = StrStripLeftLast(toStr, "\"");
	TranslateToCacheAdd(fromStr, toStr);
	return toStr;
}

///////////////////////////////////////////////////////////////////////////////

CDXUTDialog::CDXUTDialog(const std::string& menuFilePath, const std::string& menuId, bool trustedMenu) :
		m_magicNumber(MN_CONSTRUCTING), m_trustedMenu(trustedMenu), m_textureAtlas(nullptr) {
	// DRF - Add Reference Pointer (as IMenuDialog*)
	REF_PTR_ADD(dynamic_cast<IMenuDialog*>(this));

	SetMenuId(menuId);
	SetMenuFilePath(menuFilePath);

	// Initialize Members
	Init();

	// Initialize Default Elements
	InitDefaultElements();

	m_magicNumber = MN_CONSTRUCTED;
}

void CDXUTDialog::Delete() {
	// DRF - Assure Object Valid
	assert(m_magicNumber == MN_CONSTRUCTED);
	if (m_magicNumber != MN_CONSTRUCTED) {
		LogError("Magic Number (" << std::hex << m_magicNumber << "!=" << std::hex << MN_CONSTRUCTED << ") FAILED");
		return;
	}

	// Delete Dialog
	LogInfo(ToStr() << " OK");
	delete this;
}

CDXUTDialog::~CDXUTDialog() {
	m_magicNumber = MN_DESTRUCTING;

	int i = 0;

	RemoveAllControls();

	m_Fonts.RemoveAll();
	m_Textures.RemoveAll();

	for (i = 0; i < m_DefaultElements.GetSize(); i++) {
		DXUTElementHolder* pElementHolder = m_DefaultElements.GetAt(i);
		SAFE_DELETE(pElementHolder);
	}

	m_DefaultElements.RemoveAll();

	delete m_pCapElement;
	delete m_pBodyElement;
	delete m_pIconElement;
	m_pCapElement = 0;
	m_pBodyElement = 0;
	m_pIconElement = 0;

	if (m_textureAtlas != nullptr) {
		delete m_textureAtlas;
		m_textureAtlas = nullptr;
	}

	m_magicNumber = MN_DESTRUCTED;

	// DRF - Invalidate Reference Pointer (as IMenuDialog*)
	REF_PTR_DEL(dynamic_cast<IMenuDialog*>(this));
}

void CDXUTDialog::Init() {
	m_ID = -1;

	m_x = 0;
	m_y = 0;
	m_xOffset = 0;
	m_yOffset = 0;
	m_width = 200;
	m_height = 200;
	m_dwLocationFormat = IMDL_NONE;

	m_pCapElement = 0;
	m_pBodyElement = 0;
	m_pIconElement = 0;
	m_bCaption = false;
	m_bCaptionShadow = false;
	m_bMinimized = false;
	m_btopmost = false;
	m_bSnapToEdges = true;
	m_szCaption[0] = '\0';
	m_nCaptionHeight = 18;
	m_bDelete = false;
	m_bEdit = false;

	m_bCaptureKeys = false; // drf - default dialogs do not capture keys while in focus
	m_bPassthrough = false; // drf - default dialogs do not passthrough clicks while in focus

	m_colorTopLeft = D3DCOLOR_ARGB(255, 255, 255, 255);
	m_colorTopRight = D3DCOLOR_ARGB(255, 255, 255, 255);
	m_colorBottomLeft = D3DCOLOR_ARGB(255, 255, 255, 255);
	m_colorBottomRight = D3DCOLOR_ARGB(255, 255, 255, 255);

	m_pDialogCallbackEvent = NULL;
	m_pControlCallbackEvent = NULL;

	m_fTimeLastRefresh = 0;

	m_pControlMouseOver = NULL;

	m_pNextDialog = this;
	m_pPrevDialog = this;

	m_bNonUserEvents = false;

	m_bKeyboardInput = true;
	m_bMouseInput = true;

	m_bCloseOnESC = true; // drf - added

	SetMovable(false);
	SetResizable(false);

	m_bIsSelected = false;
	m_bModal = false;
	m_bBringToFront = false;

	m_bBringToFrontEnable = true;

	m_nextControlValue = 0;

	m_isScriptLoaded = false; // drf - added

	// used in MsgProc
	m_bDragCaption = false;
	m_prevMousePointCaption.x = m_prevMousePointCaption.y = 0;
	m_initialMousePointCaption.x = m_initialMousePointCaption.y = 0;
}

std::string CDXUTDialog::ToStr() const {
	const IMenuDialog* pIMD = dynamic_cast<const IMenuDialog*>(this);
	std::string str;
	if (!REF_PTR_VALID(pIMD)) {
		StrBuild(str, "Dialog<" << pIMD << "> REF_PTR_INVALID");
	} else {
		//		std::string nameStr = StrFileName(GetMenuFileName());
		std::string idStr = StrFileName(GetMenuId());
		std::string validStr = VALID_DIALOG(pIMD) ? "" : (m_bDelete ? " DELETING" : " INVALID");
		std::string editStr = GetEdit() ? " EDITING" : "";
		std::string modalStr = GetModal() ? " MODAL" : "";
		std::string captureStr = GetCaptureKeys() ? " CAPTURING" : "";
		std::string trustStr = GetTrusted() ? "" : " UNTRUSTED";
		StrBuild(str, "Dialog<" << pIMD << "> "
								<< "'" << idStr << "'"
								<< validStr
								<< editStr
								<< modalStr
								<< captureStr
								<< trustStr);
	}
	return str;
}

bool CDXUTDialog::LoadDefaultElements() {
	TiXmlDocument xml_doc;

	// should be at 'GameFiles\Menus\default_elements.xml'
	static const std::string resourceFile = "default_elements.xml";
	static const std::string textureFile = "DXUTControls.dds";
	std::string pathResources = PathAdd(IMenuDialog::GetDefaultMenuDir(), resourceFile);

	if (xml_doc.LoadFile(pathResources.c_str())) {
		SetFont(0, DEFAULT_FONT_FACE_NAME, DEFAULT_FONT_HEIGHT, DEFAULT_FONT_WEIGHT, false, false);

		int idx = SetTexture(0, textureFile);
		if (IDX_ERROR(idx)) {
			std::string sError = "Error loading texture file: ";
			sError += textureFile;
			MessageBoxUtf8(DXUTGetHWND(), sError.c_str(), "Menu Error", MB_OK);
			throw(sError.c_str());
		}

		TiXmlElement* root = xml_doc.FirstChildElement(DIALOG_TAG);

		// load elements by way of creating controls
		LoadMenuXmlResources(root);

		// remove the controls, we only keep the elements
		RemoveAllControls();

		return true;
	} else {
		std::string sError = xml_doc.ErrorDesc();
		sError += ": ";
		sError += resourceFile;
		MessageBoxUtf8(DXUTGetHWND(), sError.c_str(), "Menu Error", MB_OK);
		throw(sError.c_str());
		return false;
	}

	return false;
}

bool CDXUTDialog::LoadMenuXmlResources(TiXmlNode* dialog_root) {
	if (!dialog_root) {
		LogError("dialog_root is null");
		return false;
	}

	// Load Menu Default Texture
	TiXmlNode* child = dialog_root->FirstChild(DEFAULT_TEXTURE_TAG);
	if (child) {
		LoadMenuXmlDefaultTexture(child);
	}

	// Load Menu Default Font
	child = dialog_root->FirstChild(DEFAULT_FONT_TAG);
	if (child) {
		LoadMenuXmlDefaultFont(child);
	}

	// Load Remaining Menu Resources
	child = 0;
	for (child = dialog_root->FirstChild(); child; child = child->NextSibling()) {
		const char* val = child->Value();
		if (!_stricmp(val, TOPMOST_TAG)) {
			int ISTOPMOST = 0;
			if (GetIntFromXMLNode(child, ISTOPMOST))
				SetAsTopMost(!!ISTOPMOST);
		} else if (!_stricmp(val, TITLEBAR_TAG)) {
			LoadMenuXmlTitlebar(child);
		} else if (!_stricmp(val, SCRIPT_TAG)) {
			if (!GetEdit()) {
				if (!LoadMenuXmlScript(child)) {
					LogError("LoadMenuXmlScript() FAILED");
					return false;
				}
			} else {
				std::string scriptFileName = "";
				if (GetStringFromXMLNode(child, scriptFileName))
					SetScriptFileName(scriptFileName);
			}
		} else if (!_stricmp(val, ID_TAG)) {
			int ID = -1;
			if (GetIntFromXMLNode(child, ID))
				SetID(ID);
		} else if (!_stricmp(val, MOVABLE_TAG)) {
			bool bMovable = false;
			if (GetBoolFromXMLNode(child, bMovable))
				SetMovable(bMovable);
		} else if (!_stricmp(val, MINIMIZED_TAG)) {
			bool bMinimized = false;
			if (GetBoolFromXMLNode(child, bMinimized))
				SetMinimized(bMinimized);
		} else if (!_stricmp(val, MODAL_TAG)) {
			bool bModal = false;
			if (GetBoolFromXMLNode(child, bModal))
				SetModal(bModal);
		} else if (!_stricmp(val, SNAP_TO_EDGES_TAG)) {
			bool bSnapToEdges = false;
			if (GetBoolFromXMLNode(child, bSnapToEdges))
				SetSnapToEdges(bSnapToEdges);
		} else if (!_stricmp(val, ENABLE_KEYBOARD_TAG)) {
			bool bEnableKeyboard = false;
			if (GetBoolFromXMLNode(child, bEnableKeyboard))
				EnableKeyboardInput(bEnableKeyboard);
		} else if (!_stricmp(val, CLOSE_ON_ESC_TAG)) {
			bool bCloseOnESC = true;
			if (GetBoolFromXMLNode(child, bCloseOnESC))
				SetCloseOnESC(bCloseOnESC);
		} else if (!_stricmp(val, LOCATION_TAG)) {
			LoadMenuXmlLocation(child);
		} else if (!_stricmp(val, SIZE_TAG)) {
			LoadMenuXmlSize(child);
		} else if (!_stricmp(val, BACKGROUND_COLORS_TAG)) {
			LoadMenuXmlBackgroundColors(child);
		} else if (!_stricmp(val, NEXT_CONTROL_VALUE_TAG)) {
			int nextControlValue = 0;
			if (GetIntFromXMLNode(child, nextControlValue))
				SetNextControlValue(nextControlValue);
		} else {
			LoadControl(child);
		}
	}

	return true;
}

bool CDXUTDialog::SetLocationInScreen(int x, int y) {
	RECT rect = DXUTGetWindowClientRect();
	if (x + GetWidth() > rect.right) {
		x = rect.right - GetWidth();
	}
	if (y + GetHeight() > rect.bottom) {
		y = rect.bottom - GetHeight();
	}
	return this->SetLocation(x, y);
}

int CDXUTDialog::GetScreenWidth() {
	RECT rect = DXUTGetWindowClientRect();
	int width = rect.right - rect.left;
	return width;
}

int CDXUTDialog::GetScreenHeight() {
	RECT rect = DXUTGetWindowClientRect();
	int height = rect.bottom - rect.top;
	return height;
}

bool CDXUTDialog::IsLMouseDown() {
	return CDXUTDialog::m_LMouseDown;
}

bool CDXUTDialog::IsRMouseDown() {
	return CDXUTDialog::m_RMouseDown;
}

bool CDXUTDialog::ApplyLocationFormatting() {
	// DRF - Moved From LoadMenuXmlScript()
	if (GetEdit())
		return false;

	RECT rect = DXUTGetWindowClientRect();
	int width = rect.right - rect.left;
	int height = rect.bottom - rect.top;

	int x = m_x;
	int y = m_y;

	// first do horizontal formatting
	if (m_dwLocationFormat & IMDL_LEFT)
		x = 0;
	else if (m_dwLocationFormat & IMDL_CENTER)
		x = width / 2 - GetWidth() / 2;
	else if (m_dwLocationFormat & IMDL_RIGHT)
		x = width - GetWidth();

	// then do vertical formatting
	if (m_dwLocationFormat & IMDL_TOP)
		y = 0;
	else if (m_dwLocationFormat & IMDL_VCENTER)
		y = height / 2 - GetHeight() / 2;
	else if (m_dwLocationFormat & IMDL_BOTTOM)
		y = height - GetHeight();

	SetLocation(x, y);

	// Call Lua Script OnMoved()
	bool ok = OnMoved();

	return ok;
}

bool CDXUTDialog::LoadMenuXmlTitlebar(TiXmlNode* titlebar_root) {
	bool bSuccess = false;

	if (titlebar_root) {
		TiXmlNode* child = titlebar_root->FirstChild(TEXT_TAG);
		if (child) {
			std::string sText = "";
			if (GetStringFromXMLNode(child, sText)) {
				SetCaptionText(sText.c_str());
				bSuccess = true;
			}
		}

		if (bSuccess) {
			bSuccess = false;

			child = titlebar_root->FirstChild(ENABLED_TAG);
			if (child) {
				bool bEnableCaption = false;
				if (GetBoolFromXMLNode(child, bEnableCaption)) {
					EnableCaption(bEnableCaption);
					bSuccess = true;
				}
			}
		}

		if (bSuccess) {
			bSuccess = false;

			child = titlebar_root->FirstChild(HEIGHT_TAG);
			if (child) {
				int height = 0;
				if (GetIntFromXMLNode(child, height)) {
					SetCaptionHeight(height);
					bSuccess = true;
				}
			}
		}

		child = titlebar_root->FirstChild(SHADOW_TAG);
		if (child) {
			bool bShadow = false;
			if (GetBoolFromXMLNode(child, bShadow)) {
				SetCaptionShadow(bShadow);
			}
		}

		child = titlebar_root->FirstChild(ELEMENTS_TAG);
		if (child) {
			bSuccess = LoadElements(child);
		}
	}

	return bSuccess;
}

bool CDXUTDialog::LoadMenuXmlScript(TiXmlNode* scriptRootNode) {
	// Valid Root Node ?
	if (!scriptRootNode) {
		LogError("scriptRootNode is null");
		return false;
	}

	// Get Dialog Script Name From XML
	std::string scriptFileName = "";
	if (!GetStringFromXMLNode(scriptRootNode, scriptFileName)) {
		LogError("GetStringFromXMLNode(scriptFileName) FAILED");
		return false;
	}

	// Set Script File Name of Dialog
	SetScriptFileName(scriptFileName);
	if (scriptFileName.empty()) {
		LogError("scriptFileName is empty");
		return false;
	}

	// Look in all the various places the script could be, starting with localDev.
	std::string fullPath;
	if (!KEPMenu::Instance()->SearchFile(scriptFileName, true, false, fullPath)) {
		LogError("SearchFile(" << scriptFileName << ") FAILED");
		return false;
	}

	// Dialogs Created For Edit Do Not Load Scripts
	if (GetEdit()) {
		LogDebug("Dialog Created For Edit - Skipping Script " << scriptFileName);
		return true;
	}

	// Load The Menu Script
	bool isDir = false;
	if (!jsFileExists(fullPath.c_str(), &isDir) || isDir) {
		LogError("Script File " << scriptFileName << " Does Not Exist");
		return false;
	}

	m_isScriptLoaded = (LoadMenuScript(this, fullPath.c_str()) == 0);
	if (!m_isScriptLoaded) {
		LogError("LoadMenuScript(" << scriptFileName << ") FAILED");
		return false;
	}

	return true;
}

bool CDXUTDialog::LoadElements(TiXmlNode* elements_root) {
	bool bSuccess = false;

	if (elements_root) {
		TiXmlNode* first_child = elements_root->FirstChild(DISPLAY_TAG);
		if (first_child) {
			if (!m_pCapElement) {
				m_pCapElement = new CDXUTElement();
			}

			// if there was an error creating the element, then exit
			if (!m_pCapElement)
				return false;

			bSuccess = m_pCapElement->Load(first_child, this, DXUT_STATE_FLAG_NORMAL, DXUT_STATE_FLAG_NORMAL);

			// Pre-blend as we don't need to transition the state
			(*(m_pCapElement->GetTextureColor())).Blend(DXUT_STATE_NORMAL, 10.0f);
			(*(m_pCapElement->GetFontColor())).Blend(DXUT_STATE_NORMAL, 10.0f);
		}

		first_child = elements_root->FirstChild(BACKGROUND_DISPLAY_TAG);
		if (first_child) {
			if (!m_pBodyElement) {
				m_pBodyElement = new CDXUTElement();
			}

			// if there was an error creating the element, then exit
			if (!m_pBodyElement)
				return false;

			bSuccess = m_pBodyElement->Load(first_child, this, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NORMAL);

			// Pre-blend as we don't need to transition the state
			(*(m_pBodyElement->GetTextureColor())).Blend(DXUT_STATE_NORMAL, 10.0f);
		}

		first_child = elements_root->FirstChild(ICON_DISPLAY_TAG);
		if (first_child) {
			if (!m_pIconElement) {
				m_pIconElement = new CDXUTElement();
			}

			// if there was an error creating the element, then exit
			if (!m_pIconElement)
				return false;

			bSuccess = m_pIconElement->Load(first_child, this, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NORMAL);

			// Pre-blend as we don't need to transition the state
			(*(m_pIconElement->GetTextureColor())).Blend(DXUT_STATE_NORMAL, 10.0f);
		}
	}

	return bSuccess;
}

bool CDXUTDialog::LoadMenuXmlDefaultTexture(TiXmlNode* default_texture_root) {
	ASSERT_GDRM(false);

	if (!default_texture_root) {
		LogError("defaultTextureRoot is null");
		return false;
	}

	std::string sText;
	if (!GetStringFromXMLNode(default_texture_root, sText)) {
		LogError("GetStringFromXMLNode() FAILED");
		return false;
	}

	int textureCacheIndex = m_Textures[0];
	SetTexture(0, sText);
	pGDRM->ReleaseTextureNode(textureCacheIndex);

	return true;
}

bool CDXUTDialog::LoadMenuXmlDefaultFont(TiXmlNode* default_font_root) {
	ASSERT_GDRM(false);

	if (!default_font_root) {
		LogError("defaultFontRoot is null");
		return false;
	}

	std::string sFaceName = "";
	long height = -1;
	long weight = -1;

	TiXmlNode* child = 0;
	for (child = default_font_root->FirstChild(); child; child = child->NextSibling()) {
		const char* val = child->Value();

		if (!_stricmp(val, FACENAME_TAG)) {
			GetStringFromXMLNode(child, sFaceName);
		} else if (!_stricmp(val, HEIGHT_TAG)) {
			GetLongFromXMLNode(child, height);
		} else if (!_stricmp(val, WEIGHT_TAG)) {
			GetLongFromXMLNode(child, weight);
		}
	}

	if (!sFaceName.empty() && (height != -1) && (weight != -1)) {
		int fontCacheIndex = m_Fonts[0];
		SetFont(0, sFaceName, height, weight, false, false);
		pGDRM->ReleaseFontNode(fontCacheIndex);
		return true;
	}

	return false;
}

bool CDXUTDialog::LoadMenuXmlLocation(TiXmlNode* location_root) {
	bool bSuccess = false;

	if (location_root) {
		int x = 0;
		int y = 0;

		TiXmlNode* child = location_root->FirstChild(X_TAG);
		if (child)
			bSuccess = GetIntFromXMLNode(child, x);

		if (bSuccess) {
			bSuccess = false;
			child = location_root->FirstChild(Y_TAG);
			if (child)
				bSuccess = GetIntFromXMLNode(child, y);
		}

		if (bSuccess)
			SetLocation(x, y);

		child = location_root->FirstChild(X_OFFSET_TAG);
		if (child) {
			int xOffset;
			if (GetIntFromXMLNode(child, xOffset))
				SetOffsetX(xOffset);
		}

		child = location_root->FirstChild(Y_OFFSET_TAG);
		if (child) {
			int yOffset;
			if (GetIntFromXMLNode(child, yOffset))
				SetOffsetY(yOffset);
		}

		// get location formatting
		child = location_root->FirstChild(FORMAT_TAG);
		if (child) {
			DWORD dwLocationFormat;
			if (GetDWORDFromXMLNode(child, dwLocationFormat))
				SetLocationFormat(dwLocationFormat);
		}
	}

	return bSuccess;
}

bool CDXUTDialog::LoadMenuXmlSize(TiXmlNode* size_root) {
	bool bSuccess = false;

	if (size_root) {
		int width = 0;
		int height = 0;

		TiXmlNode* child = size_root->FirstChild(WIDTH_TAG);

		if (child) {
			if (GetIntFromXMLNode(child, width))
				bSuccess = true;
		}

		if (bSuccess) {
			bSuccess = false;

			child = size_root->FirstChild(HEIGHT_TAG);
			if (child) {
				if (GetIntFromXMLNode(child, height))
					bSuccess = true;
			}
		}

		if (bSuccess)
			SetSize(width, height);
	}

	return bSuccess;
}

bool CDXUTDialog::LoadMenuXmlBackgroundColors(TiXmlNode* background_colors_root) {
	bool bSuccess = false;

	if (background_colors_root) {
		D3DCOLOR color;

		TiXmlNode* child = background_colors_root->FirstChild(ALL_CORNERS_TAG);

		if (child) {
			if (GetDWORDFromXMLNode(child, color)) {
				SetBackgroundColors(color);
				bSuccess = true;
			}
		}

		child = background_colors_root->FirstChild(TOP_LEFT_TAG);
		if (child) {
			if (GetDWORDFromXMLNode(child, color)) {
				m_colorTopLeft = color;
				bSuccess = true;
			} else
				return false;
		}

		child = background_colors_root->FirstChild(TOP_RIGHT_TAG);
		if (child) {
			if (GetDWORDFromXMLNode(child, color)) {
				m_colorTopRight = color;
				bSuccess = true;
			} else
				return false;
		}

		child = background_colors_root->FirstChild(BOTTOM_LEFT_TAG);
		if (child) {
			if (GetDWORDFromXMLNode(child, color)) {
				m_colorBottomLeft = color;
				bSuccess = true;
			} else
				return false;
		}

		child = background_colors_root->FirstChild(BOTTOM_RIGHT_TAG);
		if (child) {
			if (GetDWORDFromXMLNode(child, color)) {
				m_colorBottomRight = color;
				bSuccess = true;
			} else
				return false;
		}
	}

	return bSuccess;
}

IMenuControl* CDXUTDialog::LoadStatic(TiXmlNode* static_root) {
	bool bSuccess = false;

	if (static_root) {
		CDXUTStatic* pStatic = new CDXUTStatic(this);

		if (pStatic == NULL)
			return NULL;

		HRESULT hr = AddControl(pStatic);
		if (FAILED(hr))
			return NULL;

		bSuccess = pStatic->Load(static_root);
		return pStatic;
	}

	return NULL;
}

IMenuControl* CDXUTDialog::LoadImage(TiXmlNode* image_root) {
	bool bSuccess = false;

	if (image_root) {
		CDXUTImage* pImage = new CDXUTImage(this);

		if (pImage == NULL)
			return NULL;

		HRESULT hr = AddControl(pImage);
		if (FAILED(hr))
			return NULL;

		bSuccess = pImage->Load(image_root);
		return pImage;
	}

	return NULL;
}

IMenuControl* CDXUTDialog::LoadFlash(TiXmlNode* image_root) {
	bool bSuccess = false;

	if (image_root) {
		CDXUTFlashPlayer* pFlash = new CDXUTFlashPlayer(this);

		if (pFlash == NULL)
			return NULL;

		HRESULT hr = AddControl(pFlash);
		if (FAILED(hr))
			return NULL;

		bSuccess = pFlash->Load(image_root);
		return pFlash;
	}

	return NULL;
}

IMenuControl* CDXUTDialog::LoadButton(TiXmlNode* button_root) {
	bool bSuccess = false;

	if (button_root) {
		CDXUTButton* pButton = new CDXUTButton(this);

		if (pButton == NULL)
			return NULL;

		HRESULT hr = AddControl(pButton);
		if (FAILED(hr))
			return NULL;

		bSuccess = pButton->Load(button_root);
		return pButton;
	}

	return NULL;
}

IMenuControl* CDXUTDialog::LoadCheckBox(TiXmlNode* check_box_root) {
	bool bSuccess = false;

	if (check_box_root) {
		CDXUTCheckBox* pCheckBox = new CDXUTCheckBox(this);

		if (pCheckBox == NULL)
			return NULL;

		HRESULT hr = AddControl(pCheckBox);
		if (FAILED(hr))
			return NULL;

		bSuccess = pCheckBox->Load(check_box_root);
		return pCheckBox;
	}

	return NULL;
}

IMenuControl* CDXUTDialog::LoadRadioButton(TiXmlNode* radio_button_root) {
	bool bSuccess = false;

	if (radio_button_root) {
		CDXUTRadioButton* pRadioButton = new CDXUTRadioButton(this);

		if (pRadioButton == NULL)
			return NULL;

		HRESULT hr = AddControl(pRadioButton);
		if (FAILED(hr))
			return NULL;

		bSuccess = pRadioButton->Load(radio_button_root);
		return pRadioButton;
	}

	return NULL;
}

IMenuControl* CDXUTDialog::LoadComboBox(TiXmlNode* combo_box_root) {
	bool bSuccess = false;

	if (combo_box_root) {
		CDXUTComboBox* pComboBox = new CDXUTComboBox(this);

		if (pComboBox == NULL)
			return NULL;

		HRESULT hr = AddControl(pComboBox);
		if (FAILED(hr))
			return NULL;

		bSuccess = pComboBox->Load(combo_box_root);
		return pComboBox;
	}

	return NULL;
}

IMenuControl* CDXUTDialog::LoadSlider(TiXmlNode* slider_root) {
	bool bSuccess = false;

	if (slider_root) {
		CDXUTSlider* pSlider = new CDXUTSlider(this);

		if (pSlider == NULL)
			return NULL;

		HRESULT hr = AddControl(pSlider);
		if (FAILED(hr))
			return NULL;

		bSuccess = pSlider->Load(slider_root);
		return pSlider;
	}

	return NULL;
}

IMenuControl* CDXUTDialog::LoadScrollBar(TiXmlNode* scroll_bar_root) {
	bool bSuccess = false;

	if (scroll_bar_root) {
		CDXUTScrollBar* pScrollBar = new CDXUTScrollBar(this);

		if (pScrollBar == NULL)
			return NULL;

		HRESULT hr = AddControl(pScrollBar);
		if (FAILED(hr))
			return NULL;

		bSuccess = pScrollBar->Load(scroll_bar_root);
		return pScrollBar;
	}

	return NULL;
}

IMenuControl* CDXUTDialog::LoadEditBox(TiXmlNode* edit_box_root, bool typeSimple) {
	bool bSuccess = false;

	if (edit_box_root) {
		CDXUTEditBox* pEditBox = new CDXUTEditBox(this, typeSimple);

		if (pEditBox == NULL)
			return NULL;

		HRESULT hr = AddControl(pEditBox);
		if (FAILED(hr))
			return NULL;

		bSuccess = pEditBox->Load(edit_box_root);
		return pEditBox;
	}

	return NULL;
}

IMenuControl* CDXUTDialog::LoadListBox(TiXmlNode* list_box_root) {
	bool bSuccess = false;

	if (list_box_root) {
		CDXUTListBox* pListBox = new CDXUTListBox(this);

		if (pListBox == NULL)
			return NULL;

		HRESULT hr = AddControl(pListBox);
		if (FAILED(hr))
			return NULL;

		bSuccess = pListBox->Load(list_box_root);
		return pListBox;
	}

	return NULL;
}

IMenuControl* CDXUTDialog::LoadComment(TiXmlNode* comment_root) {
	bool bSuccess = false;

	if (comment_root) {
		CDXUTComment* pComment = new CDXUTComment(this);

		if (pComment == NULL)
			return NULL;

		HRESULT hr = AddControl(pComment);
		if (FAILED(hr))
			return NULL;

		bSuccess = pComment->Load(comment_root);
		return pComment;
	}

	return NULL;
}

// DRF - Only MenuEditor Uses This
IMenuControl* CDXUTDialog::LoadControlByXml(const std::string& xml) {
	if (xml.empty())
		return NULL;
	TiXmlDocument xml_doc;
	xml_doc.Parse(xml.c_str());
	TiXmlElement* root = xml_doc.RootElement();
	return root ? LoadControl(root) : NULL;
}

IMenuControl* CDXUTDialog::LoadControl(TiXmlNode* controlXml) {
	const char* val = controlXml->Value();
	IMenuControl* ctl = NULL;
	if (!_stricmp(val, STATIC_TAG)) {
		ctl = LoadStatic(controlXml);
	} else if (!_stricmp(val, IMAGE_TAG)) {
		ctl = LoadImage(controlXml);
	} else if (!_stricmp(val, FLASH_TAG)) {
		ctl = LoadFlash(controlXml);
	} else if (!_stricmp(val, BUTTON_TAG)) {
		ctl = LoadButton(controlXml);
	} else if (!_stricmp(val, CHECK_BOX_TAG)) {
		ctl = LoadCheckBox(controlXml);
	} else if (!_stricmp(val, RADIO_BUTTON_TAG)) {
		ctl = LoadRadioButton(controlXml);
	} else if (!_stricmp(val, COMBO_BOX_TAG)) {
		ctl = LoadComboBox(controlXml);
	} else if (!_stricmp(val, SLIDER_TAG)) {
		ctl = LoadSlider(controlXml);
	} else if (!_stricmp(val, SCROLL_BAR_TAG)) {
		ctl = LoadScrollBar(controlXml);
	} else if (!_stricmp(val, EDIT_BOX_TAG)) {
		ctl = LoadEditBox(controlXml, false);
	} else if (!_stricmp(val, SIMPLE_EDIT_BOX_TAG)) {
		ctl = LoadEditBox(controlXml, true);
	} else if (!_stricmp(val, LIST_BOX_TAG)) {
		ctl = LoadListBox(controlXml);
	} else if (!_stricmp(val, COMMENT_TAG)) {
		ctl = LoadComment(controlXml);
	}
	return ctl;
}

bool CDXUTDialog::MoveControlForward(const std::string& ctlName) {
	if (ctlName.empty())
		return false;
	for (MenuControlVector::iterator it = m_Controls.begin(); it != m_Controls.end(); ++it) {
		IMenuControl* ctl = *it;
		if (strcmp(ctl->GetName(), ctlName.c_str()) == 0) {
			if (it + 1 != m_Controls.end()) {
				//Actually we need to move this control towards the end of the list to make it appear to be "forward"
				*it = *(it + 1);
				*(it + 1) = ctl;

				//Send dummy notification
				POINT pos;
				ctl->GetLocation(pos);
				Handlers::OnControlMoved(ctl->GetDialog(), ctl, pos);
				return true;
			}
		}
	}
	return false;
}

bool CDXUTDialog::MoveControlBackward(const std::string& ctlName) {
	if (ctlName.empty())
		return false;
	for (MenuControlVector::iterator it = m_Controls.begin(); it != m_Controls.end(); ++it) {
		IMenuControl* ctl = *it;
		if (strcmp(ctl->GetName(), ctlName.c_str()) == 0) {
			if (it != m_Controls.begin()) {
				//The opposite, see comments in MoveControlForward
				*it = *(it - 1);
				*(it - 1) = ctl;

				//Send dummy notification
				POINT pos;
				ctl->GetLocation(pos);
				Handlers::OnControlMoved(ctl->GetDialog(), ctl, pos);
				return true;
			}
		}
	}
	return false;
}

bool CDXUTDialog::MoveControlToFront(const std::string& ctlName) {
	if (ctlName.empty())
		return false;
	for (MenuControlVector::iterator it = m_Controls.begin(); it != m_Controls.end(); ++it) {
		IMenuControl* ctl = *it;
		if (strcmp(ctl->GetName(), ctlName.c_str()) == 0) {
			//Actually we need to move this control towards the end of the list to make it appear to be "forward"
			m_Controls.erase(it);
			m_Controls.push_back(ctl);

			//Send dummy notification
			POINT pos;
			ctl->GetLocation(pos);
			Handlers::OnControlMoved(ctl->GetDialog(), ctl, pos);
			return true;
		}
	}
	return false;
}

bool CDXUTDialog::MoveControlToBack(const std::string& ctlName) {
	if (ctlName.empty())
		return false;
	for (MenuControlVector::iterator it = m_Controls.begin(); it != m_Controls.end(); ++it) {
		IMenuControl* ctl = *it;
		if (strcmp(ctl->GetName(), ctlName.c_str()) == 0) {
			//Actually we need to move this control towards the front of the list to make it appear to be "backward"
			m_Controls.erase(it);
			m_Controls.insert(m_Controls.begin(), ctl);

			//Send dummy notification
			POINT pos;
			ctl->GetLocation(pos);
			Handlers::OnControlMoved(ctl->GetDialog(), ctl, pos);
			return true;
		}
	}
	return false;
}

bool CDXUTDialog::MoveControlToIndex(const std::string& ctlName, unsigned int index) {
	if (ctlName.empty())
		return false;
	if (index >= m_Controls.size())
		return false;
	for (MenuControlVector::iterator it = m_Controls.begin(); it != m_Controls.end(); ++it) {
		IMenuControl* ctl = *it;
		if (strcmp(ctl->GetName(), ctlName.c_str()) == 0) {
			m_Controls.erase(it);
			m_Controls.insert(m_Controls.begin() + index, ctl);

			//Send dummy notification
			POINT pos;
			ctl->GetLocation(pos);
			Handlers::OnControlMoved(ctl->GetDialog(), ctl, pos);
			return true;
		}
	}
	return false;
}

SpriteTextureAtlas& CDXUTDialog::GetTextureAtlas() {
	if (m_textureAtlas == nullptr) {
		// Created on demand for now as this feature is switchable
		m_textureAtlas = new SpriteTextureAtlas();
	}

	return *m_textureAtlas;
}

bool CDXUTDialog::Save(TiXmlDocument* xml_doc) {
	if (!xml_doc)
		return false;

	TiXmlNode* dialog_root = xml_doc->InsertEndChild(TiXmlElement(DIALOG_TAG));
	if (!dialog_root)
		return false;

	SaveMenuXmlDefaultTexture(dialog_root);

	SaveMenuXmlDefaultFont(dialog_root);

	SaveMenuXmlTitlebar(dialog_root);

	TiXmlNode* child = dialog_root->InsertEndChild(TiXmlElement(ID_TAG));
	if (child)
		PutIntToXMLNode(child, GetID());

	child = dialog_root->InsertEndChild(TiXmlElement(MOVABLE_TAG));
	if (child)
		PutBoolToXMLNode(child, GetMovable());

	child = dialog_root->InsertEndChild(TiXmlElement(MINIMIZED_TAG));
	if (child)
		PutBoolToXMLNode(child, GetMinimized());

	child = dialog_root->InsertEndChild(TiXmlElement(MODAL_TAG));
	if (child)
		PutBoolToXMLNode(child, GetModal());

	child = dialog_root->InsertEndChild(TiXmlElement(SNAP_TO_EDGES_TAG));
	if (child)
		PutBoolToXMLNode(child, GetSnapToEdges());

	child = dialog_root->InsertEndChild(TiXmlElement(CLOSE_ON_ESC_TAG));
	if (child)
		PutBoolToXMLNode(child, GetCloseOnESC());

	child = dialog_root->InsertEndChild(TiXmlElement(ENABLE_KEYBOARD_TAG));
	if (child)
		PutBoolToXMLNode(child, GetEnableKeyboardInput());

	SaveMenuXmlLocation(dialog_root);

	SaveMenuXmlSize(dialog_root);

	SaveMenuXmlScript(dialog_root);

	SaveMenuXmlBackgroundColors(dialog_root);

	child = dialog_root->InsertEndChild(TiXmlElement(NEXT_CONTROL_VALUE_TAG));
	if (child)
		PutIntToXMLNode(child, GetNextControlValue());

	SaveControls(dialog_root);

	return true;
}

bool CDXUTDialog::SaveMenuXmlDefaultTexture(TiXmlNode* dialog_root) {
	if (!dialog_root)
		return false;

	DXUTTextureNode* pTextureNode = GetTexture(0);
	if (!pTextureNode)
		return false;

	std::string texFileName = StrStripFilePath(pTextureNode->GetFileName());
	if (!StrSameIgnoreCase(texFileName, "DXUTControls.dds")) {
		TiXmlNode* default_texture_root = dialog_root->InsertEndChild(TiXmlElement(DEFAULT_TEXTURE_TAG));
		if (default_texture_root)
			PutStringToXMLNode(default_texture_root, texFileName);
	}

	return true;
}

bool CDXUTDialog::SaveMenuXmlDefaultFont(TiXmlNode* dialog_root) {
	bool bSuccess = false;

	if (dialog_root) {
		CDXUTFontNode* pFontNode = GetFont(0);
		std::string sFaceName = pFontNode->strFace;
		long height = pFontNode->nHeight;
		long weight = pFontNode->nWeight;

		if (sFaceName != DEFAULT_FONT_FACE_NAME ||
			height != DEFAULT_FONT_HEIGHT ||
			weight != DEFAULT_FONT_WEIGHT) {
			TiXmlNode* default_font_root = dialog_root->InsertEndChild(TiXmlElement(DEFAULT_FONT_TAG));
			if (default_font_root) {
				TiXmlNode* child = default_font_root->InsertEndChild(TiXmlElement(FACENAME_TAG));
				if (child) {
					PutStringToXMLNode(child, sFaceName);
				}

				child = default_font_root->InsertEndChild(TiXmlElement(HEIGHT_TAG));
				if (child) {
					PutLongToXMLNode(child, height);
				}

				child = default_font_root->InsertEndChild(TiXmlElement(WEIGHT_TAG));
				if (child) {
					PutLongToXMLNode(child, weight);
				}
			}
		}
	}

	return bSuccess;
}

bool CDXUTDialog::SaveMenuXmlTitlebar(TiXmlNode* dialog_root) {
	bool bSuccess = false;

	if (dialog_root) {
		TiXmlNode* titlebar_root = dialog_root->InsertEndChild(TiXmlElement(TITLEBAR_TAG));
		if (titlebar_root) {
			TiXmlNode* child = titlebar_root->InsertEndChild(TiXmlElement(TEXT_TAG));
			if (child) {
				std::string str = m_szCaption;
				PutStringToXMLNode(child, str);
			}

			child = titlebar_root->InsertEndChild(TiXmlElement(ENABLED_TAG));
			if (child) {
				PutBoolToXMLNode(child, GetCaptionEnabled());
			}

			child = titlebar_root->InsertEndChild(TiXmlElement(HEIGHT_TAG));
			if (child) {
				PutIntToXMLNode(child, GetCaptionHeight());
			}

			child = titlebar_root->InsertEndChild(TiXmlElement(SHADOW_TAG));
			if (child) {
				PutBoolToXMLNode(child, GetCaptionShadow());
			}

			SaveMenuXmlElements(titlebar_root);
		}
	}

	return bSuccess;
}

bool CDXUTDialog::SaveMenuXmlScript(TiXmlNode* dialog_root) {
	bool bSuccess = false;

	if (dialog_root) {
		TiXmlNode* script_root = dialog_root->InsertEndChild(TiXmlElement(SCRIPT_TAG));
		if (script_root) {
			std::string scriptFileName = GetScriptFileName();
			PutStringToXMLNode(script_root, scriptFileName);
		}
	}

	return bSuccess;
}

bool CDXUTDialog::SaveMenuXmlLocation(TiXmlNode* dialog_root) {
	bool bSuccess = false;

	if (dialog_root) {
		TiXmlNode* location_root = dialog_root->InsertEndChild(TiXmlElement(LOCATION_TAG));
		if (location_root) {
			POINT pt;
			GetLocation(pt, false);

			TiXmlNode* child = location_root->InsertEndChild(TiXmlElement(X_TAG));
			if (child) {
				PutIntToXMLNode(child, pt.x);
			}

			child = location_root->InsertEndChild(TiXmlElement(X_OFFSET_TAG));
			if (child) {
				PutIntToXMLNode(child, GetOffsetX());
			}

			child = location_root->InsertEndChild(TiXmlElement(Y_TAG));
			if (child) {
				PutIntToXMLNode(child, pt.y);
			}

			child = location_root->InsertEndChild(TiXmlElement(Y_OFFSET_TAG));
			if (child) {
				PutIntToXMLNode(child, GetOffsetY());
			}

			child = location_root->InsertEndChild(TiXmlElement(FORMAT_TAG));
			if (child) {
				PutDWORDToXMLNode(child, GetLocationFormat());
			}
		}
	}

	return bSuccess;
}

bool CDXUTDialog::SaveMenuXmlSize(TiXmlNode* dialog_root) {
	bool bSuccess = false;

	if (dialog_root) {
		TiXmlNode* size_root = dialog_root->InsertEndChild(TiXmlElement(SIZE_TAG));
		if (size_root) {
			TiXmlNode* child = size_root->InsertEndChild(TiXmlElement(WIDTH_TAG));
			if (child) {
				PutIntToXMLNode(child, GetWidth());
			}

			child = size_root->InsertEndChild(TiXmlElement(HEIGHT_TAG));
			if (child) {
				PutIntToXMLNode(child, GetHeight());
			}
		}
	}

	return bSuccess;
}

bool CDXUTDialog::SaveMenuXmlBackgroundColors(TiXmlNode* dialog_root) {
	bool bSuccess = false;

	if (dialog_root) {
		TiXmlNode* background_colors_root = dialog_root->InsertEndChild(TiXmlElement(BACKGROUND_COLORS_TAG));
		if (background_colors_root) {
			if ((m_colorTopLeft == m_colorTopRight) &&
				(m_colorTopLeft == m_colorBottomLeft) &&
				(m_colorTopLeft == m_colorBottomRight)) {
				TiXmlNode* child = background_colors_root->InsertEndChild(TiXmlElement(ALL_CORNERS_TAG));
				if (child) {
					PutDWORDToXMLNode(child, m_colorTopLeft);
				}
			} else {
				TiXmlNode* child = background_colors_root->InsertEndChild(TiXmlElement(TOP_LEFT_TAG));
				if (child) {
					PutDWORDToXMLNode(child, m_colorTopLeft);
				}

				child = background_colors_root->InsertEndChild(TiXmlElement(TOP_RIGHT_TAG));
				if (child) {
					PutDWORDToXMLNode(child, m_colorTopRight);
				}

				child = background_colors_root->InsertEndChild(TiXmlElement(BOTTOM_LEFT_TAG));
				if (child) {
					PutDWORDToXMLNode(child, m_colorBottomLeft);
				}

				child = background_colors_root->InsertEndChild(TiXmlElement(BOTTOM_RIGHT_TAG));
				if (child) {
					PutDWORDToXMLNode(child, m_colorBottomRight);
				}
			}
		}
	}

	return bSuccess;
}

bool CDXUTDialog::SaveMenuXmlElements(TiXmlNode* dialog_root) {
	if (!dialog_root)
		return false;

	TiXmlNode* elements_root = dialog_root->InsertEndChild(TiXmlElement(ELEMENTS_TAG));
	if (!elements_root)
		return false;

	TiXmlNode* child = elements_root->InsertEndChild(TiXmlElement(DISPLAY_TAG));
	if (child) {
		if (m_pCapElement)
			m_pCapElement->Save(child, this, DXUT_STATE_FLAG_NORMAL, DXUT_STATE_FLAG_NORMAL);
	}

	child = elements_root->InsertEndChild(TiXmlElement(BACKGROUND_DISPLAY_TAG));
	if (child) {
		if (m_pBodyElement)
			m_pBodyElement->Save(child, this, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NORMAL);
	}

	child = elements_root->InsertEndChild(TiXmlElement(ICON_DISPLAY_TAG));
	if (child) {
		if (m_pIconElement)
			m_pIconElement->Save(child, this, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NORMAL);
	}

	return true;
}

bool CDXUTDialog::SaveControls(TiXmlNode* dialog_root) {
	if (!dialog_root)
		return false;

	for (const auto& pMC : m_Controls) {
		if (!pMC)
			continue;

		TiXmlNode* pXML = dialog_root->InsertEndChild(TiXmlElement(pMC->GetTag()));
		if (pXML)
			pMC->Save(pXML);
	}
	return true;
}

bool CDXUTDialog::RemoveControl(int ID) {
	for (MenuControlIter iter = m_Controls.begin(); iter != m_Controls.end(); iter++) {
		IMenuControl* pControl = *iter;
		if (pControl->GetID() == ID) {
			// Clean focus first
			_ClearFocus();

			// Clear Selection If Editing Menu
			if (GetEdit())
				_ClearSelection();

			// Clear references to this control
			if (s_pControlFocus == pControl)
				s_pControlFocus = NULL;
			if (s_pControlPressed == pControl)
				s_pControlPressed = NULL;
			if (m_pControlMouseOver == pControl)
				m_pControlMouseOver = NULL;
			if (s_pControlSelected == pControl)
				s_pControlSelected = NULL;

			CDXUTControl* pCControl = (CDXUTControl*)pControl;
			SAFE_DELETE(pCControl);
			m_Controls.erase(iter);

			return true;
		}
	}
	return false;
}

bool CDXUTDialog::RemoveControl(const char* strName) {
	if (!strName)
		return false;
	for (MenuControlIter iter = m_Controls.begin(); iter != m_Controls.end(); iter++) {
		IMenuControl* pControl = *iter;
		if (!strcmp(pControl->GetName(), strName)) {
			// Clean focus first
			_ClearFocus();

			// Clear Selection If Editing Menu
			if (GetEdit())
				_ClearSelection();

			// Clear references to this control
			if (s_pControlFocus == pControl)
				s_pControlFocus = NULL;
			if (s_pControlPressed == pControl)
				s_pControlPressed = NULL;
			if (m_pControlMouseOver == pControl)
				m_pControlMouseOver = NULL;
			if (s_pControlSelected == pControl)
				s_pControlSelected = NULL;

			CDXUTControl* pCControl = (CDXUTControl*)pControl;
			SAFE_DELETE(pCControl);
			m_Controls.erase(iter);

			return true;
		}
	}
	return false;
}

bool CDXUTDialog::RemoveAllControls() {
	// Clear Static Controls For This Dialog
	ClearStaticControlsForThisDialog();

	// Delete All Controls Of This Dialog
	for (MenuControlIter iter = m_Controls.begin(); iter != m_Controls.end(); iter++) {
		IMenuControl* pControl = *iter;
		CDXUTControl* pCControl = (CDXUTControl*)pControl;
		SAFE_DELETE(pCControl);
	}
	m_Controls.clear();

	return true;
}

UINT CDXUTDialogResourceManager::TextureMemoryAvail() {
	return (m_pd3dDevice ? m_pd3dDevice->GetAvailableTextureMem() : 0);
}

bool CDXUTDialogResourceManager::IsInView() {
	// Is Client Window In View ?
	bool tclOk = (m_pd3dDevice ? (m_pd3dDevice->TestCooperativeLevel() == D3D_OK) : false);
	bool isInView = tclOk && TextureMemoryAvail();
	return isInView;
}

bool CDXUTDialogResourceManager::HResultFailed(HRESULT hr, const std::string& msg) {
	// Result Failed ?
	bool failed = FAILED(hr);
	if (!failed)
		return false;

	// In View - Log As Error
	bool isInView = IsInView();
	if (isInView) {
		LogError(msg << " - " << DXGetErrorDescription9A(hr));
		return true;
	}

	// Out Of View - Log As Warning
	//	LogWarn(msg << " - Out Of View"); // expected
	return true;
}

// DRF - This is only called once during client init when both FontCache and TextureCache
// are empty therefore the font and texture loops are dead code.
HRESULT CDXUTDialogResourceManager::OnCreateDevice(LPDIRECT3DDEVICE9 pd3dDevice) {
	HRESULT hr = S_OK;
	HRESULT hrReturn = S_OK;

	m_pd3dDevice = pd3dDevice;

	assert(m_pd3dDeviceWrapper == nullptr);
	if (m_pd3dDeviceWrapper == nullptr) {
		m_pd3dDeviceWrapper = new D3D9DeviceWrapper(pd3dDevice);
	}

	// Create Sprite
	m_pSprite = new D3DXSpriteWrapper(m_pd3dDeviceWrapper);
	hr = m_pSprite->Create();

	if (HResultFailed(hr, "OnCreateDevice: D3DXCreateSprite()"))
		hrReturn = hr;

	return hrReturn;
}

HRESULT CDXUTDialogResourceManager::OnResetDevice() {
	for (size_t i = 0; i < FontNodes(); i++) {
		CDXUTFontNode* pFontNode = m_FontCache.GetAt(i);
		if (pFontNode->pFont)
			pFontNode->pFont->OnResetDevice();
	}

	if (m_pSprite)
		m_pSprite->OnResetDevice();

	IDirect3DDevice9* pd3dDevice = DXUTGetD3DDevice();

	HRESULT hr = pd3dDevice->CreateStateBlock(D3DSBT_ALL, &m_pStateBlock);
	if (HResultFailed(hr, "OnResetDevice: CreateStateBlock()"))
		return hr;

	return hr;
}

void CDXUTDialogResourceManager::OnLostDevice() {
	for (size_t i = 0; i < FontNodes(); i++) {
		CDXUTFontNode* pFontNode = GetFontNode(i);
		if (pFontNode && pFontNode->pFont)
			pFontNode->pFont->OnLostDevice();
	}

	if (m_pSprite)
		m_pSprite->OnLostDevice();

	// calling Release() sometimes causes a run-time exception when this method is called
	// in response to a lost device -- removing this call may or may not introduce memory leaks
	SAFE_RELEASE(m_pStateBlock);
}

void CDXUTDialogResourceManager::OnDestroyDevice() {
	m_pd3dDevice = NULL;

	// Release the resources but don't clear the cache, as these will need to be recreated if the device is recreated
	for (size_t i = 0; i < FontNodes(); i++)
		ReleaseFontNode(i);

	for (size_t i = 0; i < TextureNodes(); i++)
		ReleaseTextureNode(i);

	SAFE_RELEASE(m_pSprite);
}

ID3DXSprite* CDXUTDialogResourceManager::GetSprite() {
	// Valid DirectX Device ?
	IDirect3DDevice9* pD3DDevice;
	HRESULT hr = m_pSprite->GetDevice(&pD3DDevice);
	if (HResultFailed(hr, "GetSprite: GetDevice()"))
		return 0;

	// Window Out of View ?
	hr = pD3DDevice->TestCooperativeLevel();
	if (HResultFailed(hr, "GetSprite: TestCooperativeLevel()"))
		return 0;

	return m_pSprite;
}

int CDXUTDialogResourceManager::AddFont(const std::string& strFaceName, LONG height, LONG weight, bool italic, bool underline) {
	// Exact Same Font Already Exists?
	int idx = IDX_NOT_IN_ARRAY;
	for (size_t i = 0; i < FontNodes(); i++) {
		CDXUTFontNode* pFontNode = GetFontNode(i);
		if (!pFontNode)
			continue;
		if ((0 == STLCompareIgnoreCase(pFontNode->GetFace(), strFaceName)) && (pFontNode->GetHeight() == height) && (pFontNode->GetWeight() == weight) && (pFontNode->GetItalic() == italic) && (pFontNode->GetUnderline() == underline)) {
			// DRF - Added Font Error Handling
			if (!pFontNode->pFont) {
				idx = i;
				break; // do retry
			}

			return (int)i; // already exists
		}
	}

	// Add New Font To End Of Cache
	if (idx == IDX_NOT_IN_ARRAY) {
		CDXUTFontNode* pNewFontNode = new CDXUTFontNode();
		if (!pNewFontNode) {
			LogError("OF MEMORY");
			return IDX_OUT_OF_MEMORY;
		}
		pNewFontNode->strFace = strFaceName;
		pNewFontNode->nHeight = height;
		pNewFontNode->nWeight = weight;
		pNewFontNode->bItalic = italic;
		pNewFontNode->bUnderline = underline;
		m_FontCache.Add(pNewFontNode);

		// New Font Indexed At End Of Cache
		size_t nodes = FontNodes();
		if (nodes <= 0) {
			LogError("NOT ARRAY");
			return IDX_NOT_IN_ARRAY;
		}
		idx = nodes - 1;
	}

	// Attempt DirectX Create Font (fails if out of view)
	idx = DxCreateFont(idx);

	return idx;
}

// DRF - This function attempts to create the font resource on the DirectX device.
// This will fail if the client window is out of view and so pFont will remain NULL
// and OnRender() will have to try again once the client is in view.
int CDXUTDialogResourceManager::DxCreateFont(size_t index) {
	// Get Font Resource By Index
	CDXUTFontNode* pFontNode = GetFontNode(index);
	if (!pFontNode) {
		LogError("NOT ARRAY index=" << index);
		return IDX_NOT_IN_ARRAY;
	}

	// Valid Font Face Name ?
	if (pFontNode->GetFace().empty()) {
		LogError("INVALID NAME ''");
		return IDX_INVALID_NAME;
	}

	// Valid DirectX Device?
	if (!m_pd3dDevice) {
		LogError("FAILED CREATE - Invalid d3dDevice");
		return IDX_FAILED_CREATE;
	}

	// Release Old Font Resource
	ReleaseFontNode(index);

	// Create New Font Resource On DirectX Device (fails if out of view - pFont=NULL)
	std::string msg;
	StrBuild(msg, "DxCreateFont: D3DXCreateFont() FontCache[" << index << "] '" << pFontNode->GetFace() << "'");
	BOOL italic = (pFontNode->GetItalic() ? TRUE : FALSE);
	// BOOL underline = (pFontNode->GetUnderline() ? TRUE : FALSE); // TODO
	HRESULT hr = D3DXCreateFontW(
		m_pd3dDeviceWrapper,
		pFontNode->GetHeight(),
		0,
		pFontNode->GetWeight(),
		1,
		italic,
		DEFAULT_CHARSET,
		OUT_TT_PRECIS,
		CLEARTYPE_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		Utf8ToUtf16(pFontNode->GetFacePtr()).c_str(),
		&pFontNode->pFont);
	if (HResultFailed(hr, msg)) {
		if (IsInView())
			return IDX_FAILED_CREATE;
		else
			return IDX_CLIENT_OUT_OF_VIEW;
	}

	return index;
}

bool CDXUTDialogResourceManager::ReleaseFontNode(size_t index) {
	// Free Font Resource (drf - todo - should also remove from directx device)
	CDXUTFontNode* pFontNode = GetFontNode(index);
	if (!pFontNode)
		return false;
	SAFE_RELEASE(pFontNode->pFont);
	return true;
}

int CDXUTDialogResourceManager::AddTexture(const std::string& filePath, const std::string& fileName, D3DXIMAGE_INFO* texInfoToBeReturned) {
	// Exact Same Texture Already Exists ?
	size_t textureNodes = TextureNodes();
	for (size_t i = 0; i < textureNodes; ++i) {
		DXUTTextureNode* pTextureNode = GetTextureNode(i);
		if (!pTextureNode || !pTextureNode->TypeInternal())
			continue;
		if (StrSameIgnoreCase(pTextureNode->GetFileName(), fileName))
			return i; // already exists
	}

	// Add New Texture To End Of Cache
	DXUTTextureNode* pNewTextureNode = new DXUTTextureNode(DXUTTextureNode::TYPE_INTERNAL, fileName, filePath);
	if (!pNewTextureNode) {
		LogError("OF MEMORY");
		return IDX_OUT_OF_MEMORY;
	}
	m_TextureCache.Add(pNewTextureNode);

	// New Texture Indexed At End Of Cache
	size_t nodes = TextureNodes();
	if (nodes <= 0) {
		LogError("NOT ARRAY");
		return IDX_NOT_IN_ARRAY;
	}
	size_t index = nodes - 1;

	// Attempt DirectX Create Texture (fails if out of view)
	int idx = DxCreateTexture(index, texInfoToBeReturned);
	if (IDX_ERROR(idx)) {
		m_TextureCache.Remove(index);
		delete pNewTextureNode;
		return idx;
	}

	return index;
}

int CDXUTDialogResourceManager::AddExternalTexture(const std::string& fileName) {
	// Exact Same External Texture Already Exists ?
	size_t textureNodes = TextureNodes();
	for (size_t i = 0; i < textureNodes; ++i) {
		DXUTTextureNode* pTextureNode = GetTextureNode(i);
		if (!pTextureNode || !pTextureNode->TypeExternal())
			continue;
		if (StrSameIgnoreCase(pTextureNode->GetFileName(), fileName))
			return i; // already exists
	}

	// Add New Texture To End Of Cache Marked As 'External'
	DXUTTextureNode* pNewTextureNode = new DXUTTextureNode(DXUTTextureNode::TYPE_EXTERNAL, fileName);
	if (!pNewTextureNode) {
		LogError("OF MEMORY");
		return IDX_OUT_OF_MEMORY;
	}
	m_TextureCache.Add(pNewTextureNode);

	// New Texture Indexed At End Of Cache
	textureNodes = TextureNodes();
	if (textureNodes <= 0) {
		LogError("NOT ARRAY");
		return IDX_NOT_IN_ARRAY;
	}
	size_t index = textureNodes - 1;

	return index;
}

int CDXUTDialogResourceManager::AddDynamicTexture(unsigned dynTextureId) {
	// Already Exists ?
	for (size_t i = 0; i < TextureNodes(); ++i) {
		DXUTTextureNode* pTextureNode = GetTextureNode(i);
		if (!pTextureNode || pTextureNode->GetType() != DXUTTextureNode::TYPE_DYNAMIC)
			continue;
		if (pTextureNode->GetDynamicTextureId() == dynTextureId)
			return i; // already exists
	}

	// Add New Texture To End Of Cache
	DXUTTextureNode* pNewTextureNode = new DXUTTextureNode(DXUTTextureNode::TYPE_DYNAMIC, "");
	if (!pNewTextureNode) {
		LogError("OF MEMORY");
		return IDX_OUT_OF_MEMORY;
	}
	pNewTextureNode->SetDynamicTextureId(dynTextureId);
	m_TextureCache.Add(pNewTextureNode);

	// New Texture Indexed At End Of Cache
	if (TextureNodes() <= 0) {
		LogError("NOT ARRAY");
		return IDX_NOT_IN_ARRAY;
	}

	return TextureNodes() - 1;
}

// DRF - This function attempts to create the texture resource on the DirectX device.
// This will fail if the client window is out of view and so pTexture will remain NULL
// and OnRender() will have to try again once the client is in view.
int CDXUTDialogResourceManager::DxCreateTexture(size_t index, D3DXIMAGE_INFO* texInfoToBeReturned) {
	// Get Texture Resource By Index
	DXUTTextureNode* pTextureNode = GetTextureNode(index);
	if (!pTextureNode) {
		LogError("NOT ARRAY index=" << index);
		return IDX_NOT_IN_ARRAY;
	}

	// Not Internal Texture ?
	if (!pTextureNode->TypeInternal())
		return index;

	// Valid Texture Filename ?
	std::string fileName = pTextureNode->GetFileName();
	if (fileName.empty()) {
		LogError("INVALID NAME - ''");
		return IDX_INVALID_NAME;
	}

	// Build Full Texture File Path\Name
	std::string texPathName = PathAdd(pTextureNode->GetFilePath(), fileName);

	// Valid DirectX Device?
	if (!m_pd3dDevice) {
		LogError("FAILED CREATE - Invalid d3dDevice");
		return IDX_FAILED_CREATE;
	}

	// Find Texture File
	std::string msg;
	StrBuild(msg, "DxCreateTexture: DXUTFindDXSDKMediaFileCch() '" << texPathName << "'");
	TCHAR strPath[MAX_PATH] = L"";
	HRESULT hr = DXUTFindDXSDKMediaFileCch(strPath, MAX_PATH, Utf8ToUtf16(texPathName).c_str());
	if (HResultFailed(hr, msg))
		return IDX_FAILED_CREATE;

	// Release Old Texture Resource
	ReleaseTextureNode(index);

	// Create New Texture Resource On DirectX Device (fails if out of view - pTexture=NULL)
	StrBuild(msg, "DxCreateTexture: D3DXCreateTextureFromFileEx() '" << strPath << "'");
	D3DXIMAGE_INFO info;

	IDirect3DTexture9* pTexture;
	hr = D3DXCreateTextureFromFileEx(m_pd3dDevice, strPath, D3DX_DEFAULT, D3DX_DEFAULT,
		D3DX_DEFAULT, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED,
		D3DX_DEFAULT, D3DX_DEFAULT, 0,
		&info, NULL, &pTexture);

	if (texInfoToBeReturned != nullptr) {
		*texInfoToBeReturned = info;
	}

	//Ankit ----- Check if failed to create resource because client out of view
	if (HResultFailed(hr, msg)) {
		if (IsInView())
			return IDX_FAILED_CREATE;
		else
			return IDX_CLIENT_OUT_OF_VIEW;
	}

	pTextureNode->SetTexture(pTexture);

	// Store dimensions
	pTextureNode->SetWidth(info.Width);
	pTextureNode->SetHeight(info.Height);

	return index;
}

bool CDXUTDialogResourceManager::ReleaseTextureNode(size_t index) {
	// Free Texture Resource (drf - todo - should also remove from directx device)
	DXUTTextureNode* pTextureNode = GetTextureNode(index);
	if (!pTextureNode)
		return false;
	pTextureNode->SetTexture(nullptr);
	return true;
}

// DRF - ED-7035 - Investigate texture memory impact
bool CDXUTDialogResourceManager::ReleaseTextureNode(IDirect3DTexture9* pTex) {
	for (size_t i = 0; i < m_TextureCache.GetSize(); ++i) {
		auto pTextureNode = m_TextureCache[i];
		if (pTextureNode->GetTexture() == pTex) {
			pTextureNode->SetTexture(nullptr);
			return true;
		}
	}
	return false;
}

void DXUTTextureNode::InitDimension(IDirect3DTexture9* pD3DTexture) {
	assert(!IsDimensionSet());
	// update dimension
	if (D3D_OK == pD3DTexture->GetLevelDesc(0, &m_surfaceDesc)) { // get the description of mipmap 0
		SetWidth(m_surfaceDesc.Width);
		SetHeight(m_surfaceDesc.Height);
	} else {
		// Unable to obtain texture dimension - set to zero anyway so we won't query again for the same texture
		SetWidth(0);
		SetHeight(0);
	}
}

IDirect3DTexture9* DXUTTextureNode::GetTexture() const {
	if (GetType() == TYPE_DYNAMIC) {
		assert(m_pTexture == nullptr);
		return KEPMenu::Instance()->GetDynamicTexture(GetDynamicTextureId());
	}

	return m_pTexture;
}

//Ankit ----- Update the set texture function retrieve the surface desc and store it in the DXUTTextureNode
void DXUTTextureNode::SetTexture(IDirect3DTexture9* pTexture) {
	// Do not update if texture is not changed
	if (m_pTexture == pTexture)
		return;

	// Release old texture reference
	if (m_pTexture)
		m_pTexture->Release();

	m_pTexture = pTexture;
	if (m_pTexture) {
		m_pTexture->AddRef();
		m_pTexture->GetLevelDesc(0, &m_surfaceDesc);
	} else
		m_surfaceDesc = {};

	// Mark dimension as dirty (pending SetWidth/Height or InitDimension)
	m_dimensionSet = false;
}

void CDXUTDialog::Refresh() {
	if (s_pControlFocus)
		s_pControlFocus->OnFocusOut();

	if (m_pControlMouseOver)
		m_pControlMouseOver->OnMouseLeave();

	s_pControlFocus = NULL;
	s_pControlPressed = NULL;
	m_pControlMouseOver = NULL;

	for (const auto& pMC : m_Controls) {
		if (pMC)
			pMC->Refresh();
	}

	if (m_bKeyboardInput)
		FocusDefaultControl();
}

void CDXUTDialog::RenderControl(IDirect3DDevice9* pd3dDevice, ID3DXSprite* sprite, IMenuControl* pControl, float fElapsedTime) {
	assert(pd3dDevice != nullptr);
	assert(sprite != nullptr);

	// Check clipping
	if (!GetEdit() && pControl->IsCulled()) {
		// Completely culled, nothing to render
		return;
	}

	bool clipped = false;
	if (!GetEdit() && pControl->IsClipped()) {
		sprite->Flush();

		RECT clipRect = pControl->GetClipRect();
		::OffsetRect(&clipRect, GetLocationX(), GetCaptionHeight() + GetLocationY());
		pd3dDevice->SetScissorRect(&clipRect);
		pd3dDevice->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
		clipped = true;
	}

	pControl->Render(fElapsedTime);

	// Restore from clipping
	if (clipped) {
		sprite->Flush();
		pd3dDevice->SetRenderState(D3DRS_SCISSORTESTENABLE, 0);
	}
}

void CDXUTDialog::OnBeginFrame() {
	if (m_textureAtlas) {
		m_textureAtlas->beginFrame();
	}
}

void CDXUTDialog::OnEndFrame() {
	if (m_textureAtlas) {
		m_textureAtlas->endFrame();
	}
}

HRESULT CDXUTDialog::OnRender(float elapsedSec) {
	ASSERT_GDRM(S_OK);

	// See if the dialog needs to be refreshed
	if (m_fTimeLastRefresh < s_fTimeRefresh) {
		m_fTimeLastRefresh = DXUTGetTime();
		Refresh();
	}

	IDirect3DDevice9* pd3dDevice = pGDRM->GetD3DDevice();
	if (!pd3dDevice)
		return S_OK;

	// Set up a state block here and restore it when finished drawing all the controls
	pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	pd3dDevice->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);

	pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG2);
	pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

	pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_DIFFUSE);

	pd3dDevice->SetVertexShader(NULL);
	pd3dDevice->SetPixelShader(NULL);

	pd3dDevice->SetRenderState(D3DRS_ZENABLE, FALSE);

	POINT myPos;
	GetLocation(myPos);

	if (!GetMinimized()) {
		if (!m_pBodyElement || ((*(m_pBodyElement->GetTextureColor())).GetColor(DXUT_STATE_NORMAL) & 0xff000000) == 0) { // if the background element doesn't exist or it's color is transparent
			// draw the simple background rectangle using corner colors
			DXUT_SCREEN_VERTEX vertices[4] = {
				(float)myPos.x,
				(float)myPos.y,
				0.5f,
				1.0f,
				m_colorTopLeft,
				0.0f,
				0.5f,
				(float)myPos.x + GetWidth(),
				(float)myPos.y,
				0.5f,
				1.0f,
				m_colorTopRight,
				1.0f,
				0.5f,
				(float)myPos.x + GetWidth(),
				(float)myPos.y + GetHeight(),
				0.5f,
				1.0f,
				m_colorBottomRight,
				1.0f,
				1.0f,
				(float)myPos.x,
				(float)myPos.y + GetHeight(),
				0.5f,
				1.0f,
				m_colorBottomLeft,
				0.0f,
				1.0f,
			};
			pd3dDevice->SetFVF(DXUT_SCREEN_VERTEX::FVF);
			pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN, 2, vertices, sizeof(DXUT_SCREEN_VERTEX));
		}
	}

	pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

	pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

	pd3dDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);

	DXUTTextureNode* pTextureNode = GetTexture(0);
	if (!pTextureNode)
		return S_OK;

	pd3dDevice->SetTexture(0, pTextureNode->GetTexture());

	ID3DXSprite* sprite = pGDRM->GetSprite();
	if (!sprite)
		return S_OK;

	D3DXSpriteWrapper* spriteWrapper = dynamic_cast<D3DXSpriteWrapper*>(sprite);
	assert(spriteWrapper != nullptr);
	if (spriteWrapper != nullptr) {
		spriteWrapper->SetCurrentMenu(this);
	}

	sprite->Begin(D3DXSPRITE_DONOTSAVESTATE);

	// Render the caption if it's enabled.
	if (GetCaptionEnabled()) {
		// DrawSprite will offset the rect down by
		// m_nCaptionHeight, so adjust the rect higher
		// here to negate the effect.
		RECT rc = { 0, -GetCaptionHeight(), GetWidth(), 0 };

		(*(m_pCapElement->GetFontColor())).Blend(DXUT_STATE_NORMAL, elapsedSec, 0.0f);
		(*(m_pCapElement->GetTextureColor())).Blend(DXUT_STATE_NORMAL, elapsedSec, 0.0f);

		DrawSprite(m_pCapElement, &rc);

		// Render the icon
		if (m_pIconElement && ((*(m_pIconElement->GetTextureColor())).GetColor(DXUT_STATE_NORMAL) & 0xff000000) != 0) { // if the icon element's color is not transparent, draw it.
			// The icon will always be drawn as a square that's the same size as the titlebar.
			RECT rcIcon = { 0, -GetCaptionHeight(), GetCaptionHeight(), 0 };
			DrawSprite(m_pIconElement, &rcIcon);
			rc.left += GetCaptionHeight();
		}

		rc.left += 5; // Make a left margin
		TCHAR wszOutput[256];
		_tcsncpy(wszOutput, Utf8ToUtf16(m_szCaption).c_str(), 256 - 1);
		wszOutput[256 - 1] = _T('\0');
		if (GetMinimized())
			_tcsncat(wszOutput, _T(" (Minimized)"), 256 - lstrlen(wszOutput));
		DrawText(wszOutput, m_pCapElement, &rc, m_bCaptionShadow);
	}

	// If the dialog is minimized, skip rendering its controls.
	if (!GetMinimized()) {
		if (m_pBodyElement && ((*(m_pBodyElement->GetTextureColor())).GetColor(DXUT_STATE_NORMAL) & 0xff000000) != 0) { // if the background element's color is not transparent, draw it.
			RECT rc = { 0, 0, GetWidth(), GetHeight() - (m_bCaption ? (GetCaptionHeight()) : 0) };
			(*(m_pBodyElement->GetTextureColor())).Blend(DXUT_STATE_NORMAL, elapsedSec, 0.0f);
			DrawSprite(m_pBodyElement, &rc);
		}

		for (MenuControlIter iter = m_Controls.begin(); iter != m_Controls.end(); iter++) {
			IMenuControl* pControl = *iter;
			if (!pControl)
				continue;

			// Focused control is drawn last
			if (pControl == s_pControlFocus)
				continue;

			RenderControl(pd3dDevice, sprite, pControl, elapsedSec);
		}

		if (s_pControlFocus && (s_pControlFocus->GetDialog() == this)) {
			RenderControl(pd3dDevice, sprite, s_pControlFocus, elapsedSec);
		}

		if (GetEdit() && GetIsSelected())
			m_selectionHandles.Render(myPos.x, myPos.y, GetWidth(), GetHeight());
	}

	// Sprite->End() calls sprite->Flush() which actually does the work
	// that all the previous code has prepaired, so the textures must
	// still be valid during this function.
	sprite->End();

	if (spriteWrapper != nullptr)
		spriteWrapper->SetCurrentMenu(nullptr);

	// All these textures have had their ref count increased, so go through and release them.
	for (const auto& pTexRef : g_refedTexture)
		if (pTexRef)
			pTexRef->Release();
	g_refedTexture.clear();

	return S_OK;
}

bool CDXUTDialog::SendEvent(GUI_EVENT nEvent, bool bTriggeredByUser, IMenuDialog* pDialog, POINT pt) {
	// If no callback has been registered there's nowhere to send the event to
	if (m_pDialogCallbackEvent == NULL)
		return false;

	// Discard events triggered programatically if these types of events haven't been
	// enabled
	if (!bTriggeredByUser && !m_bNonUserEvents)
		return false;

	return m_pDialogCallbackEvent(nEvent, pDialog, pt);
}

bool CDXUTDialog::SendEvent(GUI_EVENT nEvent, bool bTriggeredByUser, IMenuDialog* pDialog) {
	const POINT pt = { 0, 0 };
	return SendEvent(nEvent, bTriggeredByUser, pDialog, pt);
}

bool CDXUTDialog::SendEvent(GUI_EVENT nEvent, bool bTriggeredByUser, IMenuControl* pControl, POINT pt) {
	// If no callback has been registered there's nowhere to send the event to
	if (!m_pControlCallbackEvent || !pControl)
		return false;

	// Discard events triggered programatically if these types of events haven't been enabled
	if (!bTriggeredByUser && !m_bNonUserEvents)
		return false;

	return m_pControlCallbackEvent(nEvent, pControl->GetID(), pControl, pt);
}

bool CDXUTDialog::SendEvent(GUI_EVENT nEvent, bool bTriggeredByUser, IMenuControl* pControl) {
	const POINT pt = { 0, 0 };
	return SendEvent(nEvent, bTriggeredByUser, pControl, pt);
}

int CDXUTDialog::IdxFontRemap(int idxRM) {
	if (IDX_ERROR(idxRM))
		return idxRM;

	// Remap To My Font Index
	int idxMe = m_Fonts.IndexOf(idxRM);
	if (idxMe < 0) {
		m_Fonts.Add(idxRM);
		idxMe = m_Fonts.GetSize() - 1;
	}
	return idxMe;
}

int CDXUTDialog::AddFont(const std::string& strFaceName, LONG height, LONG weight, bool italic, bool underline) {
	ASSERT_GDRM(IDX_GDRM_INVALID);

	// Create Font ?
	int idxRM = pGDRM->AddFont(strFaceName, height, weight, italic, underline);
	if (IDX_ERROR(idxRM))
		return idxRM;

	// Return Resource Manager Font Index Remapped
	return IdxFontRemap(idxRM);
}

int CDXUTDialog::IdxTextureRemap(int idxRM) {
	if (IDX_ERROR(idxRM))
		return idxRM;

	// Remap To My Texture Index
	int idxMe = m_Textures.IndexOf(idxRM);
	if (idxMe < 0) {
		m_Textures.Add(idxRM);
		idxMe = m_Textures.GetSize() - 1;
	}
	return idxMe;
}

int CDXUTDialog::AddTexture(const std::string& strFilename, D3DXIMAGE_INFO* textInfoToBeReturned) {
	ASSERT_GDRM(IDX_GDRM_INVALID);

	// Valid Texture File ?
	if (strFilename.empty()) {
		LogError("INVALID NAME - ''");
		return IDX_INVALID_NAME;
	}

	// Find Texture File ?
	std::string dir, fileName;
	bool foundFile = KEPMenu::Instance()->SearchFile(strFilename, false, false, dir, fileName);
	if (!foundFile) {
		// Texture Will Be Downloaded Asyncronously
		// LogWarn("FILE NOT FOUND '" << strFilename << "'");
		return IDX_FILE_NOT_FOUND;
	}

	// Create Texture ?
	int idxRM = pGDRM->AddTexture(dir, fileName, textInfoToBeReturned);
	if (IDX_ERROR(idxRM))
		return idxRM;

	// Return Resource Manager Texture Index Remapped
	return IdxTextureRemap(idxRM);
}

int CDXUTDialog::AddExternalTexture(const std::string& strFilename) {
	ASSERT_GDRM(IDX_GDRM_INVALID);

	// Valid Texture File ?
	if (strFilename.empty()) {
		LogError("INVALID NAME - ''");
		return IDX_INVALID_NAME;
	}

	// Create External Texture ?
	int idxRM = pGDRM->AddExternalTexture(strFilename);
	if (IDX_ERROR(idxRM))
		return idxRM;

	// Return Resource Manager Texture Index Remapped
	return IdxTextureRemap(idxRM);
}

int CDXUTDialog::AddDynamicTexture(unsigned dynTextureId) {
	ASSERT_GDRM(IDX_GDRM_INVALID);

	// Create External Texture ?
	int idxRM = pGDRM->AddDynamicTexture(dynTextureId);
	if (IDX_ERROR(idxRM))
		return idxRM;

	// Return Resource Manager Texture Index Remapped
	return IdxTextureRemap(idxRM);
}

int CDXUTDialog::SetTexture(size_t index, const std::string& strFilename, D3DXIMAGE_INFO* textInfoToBeReturned) {
	ASSERT_GDRM(IDX_GDRM_INVALID);

	// Make sure the list is at least as large as the index being set
	for (size_t i = (size_t)m_Textures.GetSize(); i <= index; i++)
		m_Textures.Add(-1);

	// Find Texture File ?
	std::string dir, fileName;
	bool foundFile = KEPMenu::Instance()->SearchFile(strFilename, false, false, dir, fileName);
	if (!foundFile) {
		LogError("FILE NOT FOUND '" << strFilename << "'");
		return IDX_FILE_NOT_FOUND;
	}

	// Attempt To Create Texture (fails if out of view)
	int idx = pGDRM->AddTexture(dir, fileName, textInfoToBeReturned);
	if (IDX_ERROR(idx))
		return idx;

	LogDebug("OK '" << strFilename << "'");

	m_Textures.SetAt(index, idx);

	return index;
}

DXUTTextureNode* CDXUTDialog::GetTexture(size_t index) {
	ASSERT_GDRM(NULL);
	if ((int)index >= m_Textures.GetSize())
		return NULL;
	return pGDRM->GetTextureNode(m_Textures.GetAt(index));
}

int CDXUTDialog::SetFont(size_t index, const std::string& strFaceName, LONG height, LONG weight, bool italic, bool underline) {
	ASSERT_GDRM(IDX_GDRM_INVALID);

	// Make sure the list is at least as large as the index being set
	for (size_t i = (size_t)m_Fonts.GetSize(); i <= index; i++)
		m_Fonts.Add(-1);

	// Attempt To Create Font (fails if out of view)
	int idx = pGDRM->AddFont(strFaceName, height, weight, italic, underline);
	if (IDX_ERROR(idx))
		return idx;

	LogDebug("OK - '" << strFaceName << "'");

	m_Fonts.SetAt(index, idx);

	return index;
}

CDXUTFontNode* CDXUTDialog::GetFont(size_t index) {
	ASSERT_GDRM(NULL);

	// Valid Font Index ?
	auto fonts = m_Fonts.GetSize();
	if ((int)index >= fonts) {
		LogError("index=" << index << " > fonts=" << fonts);
		return NULL;
	}

	// Get Font
	auto pFN = pGDRM->GetFontNode(m_Fonts.GetAt(index));
	if (!pFN) {
		LogError("GetFontNode() FAILED - index=" << index);
		return NULL;
	}

	return pFN;
}

int CDXUTDialog::GetResourceManagerFontIndex(size_t index) {
	if ((int)index < m_Fonts.GetSize())
		return m_Fonts.GetAt(index);
	return IDX_NOT_IN_ARRAY;
}

bool CDXUTDialog::CheckForDuplicateControlName(const char* strName, bool bShowMsg) {
	IMenuControl* ctrl = GetControl(strName);
	if (ctrl) {
		if (bShowMsg) {
			char error[_MAX_PATH];
			_snprintf(error, _MAX_PATH, "A control with the name '%s' already exists.", strName);
			error[_MAX_PATH - 1] = 0;

			MessageBoxUtf8(DXUTGetHWND(), error, "Duplicate Control Name", MB_OK | MB_ICONWARNING);
		}
		return true;
	}
	return false;
}

// not an elegant solution (n^2) but it works
bool CDXUTDialog::CheckForDuplicateControlNames(bool bShowMsg) {
	for (MenuControlIter iter = m_Controls.begin(); iter != m_Controls.end(); iter++) {
		IMenuControl* pControl = *iter;
		if (pControl) {
			int count = 0;
			const char* strName = (*iter)->GetName();

			for (MenuControlIter iter2 = m_Controls.begin(); iter2 != m_Controls.end(); iter2++) {
				if (_strnicmp(strName, (*iter2)->GetName(), MAX_PATH) == 0)
					count++;
			}

			// there should only be 1 control with this name
			if (count > 1) {
				if (bShowMsg) {
					char error[_MAX_PATH];
					_snprintf(error, _MAX_PATH, "A control with the name '%s' already exists.", strName);
					error[_MAX_PATH - 1] = 0;

					MessageBoxUtf8(DXUTGetHWND(), error, "Duplicate Control Name", MB_OK | MB_ICONWARNING);
				}

				return true;
			}
		}
	}

	return false;
}

CDXUTDialog* CDXUTDialog::GetDialogInFocus() {
	if (!s_pControlFocus || !s_pControlFocus->GetEnabled())
		return NULL;
	return dynamic_cast<CDXUTDialog*>(s_pControlFocus->GetDialog());
}

// DRF - Returns mouse point relative to menu origin
POINT CDXUTDialog::GetCursorPointMenu(const POINT& cursorPoint) {
	POINT cursorPointMenu = cursorPoint;
	POINT myPos;
	GetLocation(myPos);
	cursorPointMenu.x -= myPos.x;
	cursorPointMenu.y -= (myPos.y + (m_bCaption ? GetCaptionHeight() : 0));
	return cursorPointMenu;
}

bool CDXUTDialog::PointIsInside(const POINT& pt) {
	if (m_bMinimized)
		return false;
	POINT myPos;
	GetLocation(myPos);
	RECT rect = { myPos.x, myPos.y, myPos.x + GetWidth(), myPos.y + GetHeight() };
	return PointInsideRect(rect, pt);
}

// Legacy Interface
bool CDXUTDialog::ContainsPoint(POINT pt) {
	return PointIsInside(pt);
}

bool CDXUTDialog::PointIsInsideCaption(const POINT& pt) {
	POINT myPos;
	GetLocation(myPos);

	return (pt.x >= myPos.x && pt.x < (myPos.x + GetWidth()) && pt.y >= myPos.y && pt.y < (myPos.y + GetCaptionHeight()));
}

bool CDXUTDialog::PointIsInsideNotCaption(const POINT& pt) {
	POINT myPos;
	GetLocation(myPos);

	return (pt.x >= myPos.x && pt.x < (myPos.x + GetWidth()) && pt.y >= myPos.y && pt.y < (myPos.y + GetHeight()));
}

static void SetMenuCursorType(const CURSOR_TYPE& cursorType) {
	KEPMenu* kep_menu = KEPMenu::Instance();
	if (kep_menu)
		kep_menu->SetMenuCursorType(cursorType);
}

#define MSG_KEY_ARROW(wParam) (wParam == VK_RIGHT || wParam == VK_UP || wParam == VK_LEFT || wParam == VK_DOWN || wParam == VK_TAB)

bool CDXUTDialog::MsgProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	bool bHandled = false;

	static bool bResizing;

	// Get Mouse Info
	MsgProcInfo mpi;
	POINT mousePoint = mpi.CursorPoint();
	POINT mousePointMenu = GetCursorPointMenu(mousePoint);
	bool mousePointIsInside = PointIsInside(mousePoint);
	bool mousePointIsInsideCaption = PointIsInsideCaption(mousePoint);
	bool mousePointIsInsideNotCaption = PointIsInsideNotCaption(mousePoint);
	bool mouseMsg = mpi.IsMouse();
	bool mouseMsgButtonDown = mpi.IsMouseButtonDown();

	// Set Static LMouseDown State
	if (mouseMsg) {
		if (uMsg == WM_LBUTTONDOWN)
			CDXUTDialog::m_LMouseDown = true;
		if (uMsg == WM_LBUTTONUP)
			CDXUTDialog::m_LMouseDown = false;

		if (uMsg == WM_RBUTTONDOWN)
			CDXUTDialog::m_RMouseDown = true;
		if (uMsg == WM_RBUTTONUP)
			CDXUTDialog::m_RMouseDown = false;
	}

	// CJW: Detect Dialog changes only on mouse move events, otherwise we get weird artifacts on key presses and such...
	if (uMsg == WM_MOUSEMOVE) {
		// If Cursor Is Inside Menu
		if (mousePointIsInside) {
			//Ankit ----- if current dialog is not the same as the previous dialog which had the cursor, fire leave event for the focused control in the previous dialog
			CDXUTDialog* pMenu = KEPMenu::Instance()->m_cursorMenu;
			if (pMenu != NULL && pMenu != this) {
				pMenu->OnMouseMove(mousePoint);
			}

			// Set Cursor Menu To This
			KEPMenu::Instance()->SetCursorMenu(this);

			// Bring To Front On Mouse Button Down
			if (mouseMsgButtonDown)
				m_bBringToFront = true;
		}
	}

	// Mouse Message And Menu Is Draggable ?
	bool isDraggable = (GetCaptionEnabled() && GetMovable() && !GetEdit());
	if (mouseMsg && isDraggable) {
		// Mouse Is Inside Draggable Menu Caption ?
		if (mousePointIsInsideCaption) {
			// Mouse Left Button Double Click - Do Nothing
			if (uMsg == WM_LBUTTONDBLCLK)
				return true;

			// Mouse Left Button Down - Start Dragging
			if (uMsg == WM_LBUTTONDOWN) {
				// Clicked Button Inside Caption - Cancel Drag
				IMenuControl* pControl = GetControlAtPoint(mousePointMenu);
				if (pControl != NULL && pControl->GetEnabled()) {
					if (dynamic_cast<CDXUTButton*>(pControl) != NULL) {
						pControl->MsgProc(uMsg, wParam, lParam);
						if (pControl->HandleMouse(uMsg, mousePointMenu, wParam, lParam)) {
							CancelDraggingByCaption(mousePointMenu);
							return true;
						}
					}
				}

				// Start Left Button Drag If Draggable And Not Editing Menu
				SetCapture(DXUTGetHWND());
				m_prevMousePointCaption = mousePoint;
				StartDraggingByCaption(mousePointMenu);
			}
		} // if (mousePointIsInsideCaption)

		// Mouse Button Up
		if (uMsg == WM_LBUTTONUP && GetIsDraggingByCaption()) {
			// End Left Button Drag
			bool dialogMovedByDragging = EndDraggingByCaption(mousePoint);
			ReleaseCapture();
			dialogMovedByDragging;
			OnMoved();
			return true;
		}

		// Mouse Move And We Are Dragging This Menu
		if (uMsg == WM_MOUSEMOVE && GetIsDraggingByCaption())
			HandleDraggingByCaption(mousePoint);
	}

	// Menu Never Handles Messages While Minimized
	if (GetMinimized())
		return false;

	// Menu Is Open For Editing In Menu Editor
	if (GetEdit()) {
		static bool s_bDrag;
		static POINT s_prevMousePoint;

		// If a control is selected, it belongs to this dialog, then give
		// it the first chance at handling the message.
		if (s_pControlSelected && s_pControlSelected->GetDialog() == this) {
			if (s_pControlSelected->HandleMouse(uMsg, mousePointMenu, wParam, lParam))
				return true;
		}

		if (uMsg == WM_LBUTTONDBLCLK) {
			return true;
		} else if (uMsg == WM_LBUTTONDOWN) {
			if (mousePointIsInsideNotCaption) {
				// Not yet handled, see if the mouse is over any controls
				IMenuControl* pControl = GetControlAtPoint(mousePointMenu);
				if (pControl != NULL && (m_bEdit || pControl->GetEnabled())) {
					if (pControl->HandleMouse(uMsg, mousePointMenu, wParam, lParam))
						return true;
				}

				s_prevMousePoint = mousePoint;

				SetCapture(DXUTGetHWND());

				SetMovable(true);
				SetResizable(true);

				SetIsSelected(true);

				// now that dialog is selected, remove selection from any previously
				// selected control
				_ClearSelection();

				return true;
			}
		} else if (uMsg == WM_LBUTTONUP && s_bDrag) {
			// End Menu Drag
			s_bDrag = false;
			if (mousePointIsInsideNotCaption) {
				ReleaseCapture();
				return true;
			}

		} else if (uMsg == WM_MOUSEMOVE) {
			if (GetIsSelected()) {
				// Handle Dialog Drag Position
				if (s_bDrag) {
					// Due to snapping math, we must include offsets before we set the new X/Y position values. They'll
					// have to be subtracted out prior to the actual SetLocationX/Y calls.
					POINT myPos;
					GetLocation(myPos);

					myPos.x += mousePoint.x - s_prevMousePoint.x;
					myPos.y += mousePoint.y - s_prevMousePoint.y;
					s_prevMousePoint = mousePoint;

					if (m_bSnapToEdges) {
						// top, left
						if (myPos.x < SNAP_THRESHOLD && myPos.x > -SNAP_THRESHOLD)
							myPos.x = 0;
						if (myPos.y < SNAP_THRESHOLD && myPos.y > -SNAP_THRESHOLD)
							myPos.y = 0;

						// bottom, right
						RECT rect = DXUTGetWindowClientRect();
						if ((myPos.x + GetWidth() > rect.right - SNAP_THRESHOLD) && (myPos.x + GetWidth() < rect.right + SNAP_THRESHOLD))
							myPos.x = rect.right - GetWidth();
						if ((myPos.y + GetHeight() > rect.bottom - SNAP_THRESHOLD) && (myPos.y + GetHeight() < rect.bottom + SNAP_THRESHOLD))
							myPos.y = rect.bottom - GetHeight();
					}

					// Now, subtract out the offsets and set the new position values.
					SetLocationX(myPos.x - GetOffsetX());
					SetLocationY(myPos.y - GetOffsetY());

					// Call Lua Dialog_OnMoved()
					OnMoved();

					return true;

				} else {
					// if the mouse is within the dialog area
					if (!bResizing && mousePointIsInsideNotCaption) {
						return true;
					}
				}
			}
		}
	} // if (m_bEdit)

	// Dialog Is Being Resized In Menu Editor
	if (GetResizable()) {
		static POINT prevMousePoint;
		static CDXUTSelectionHandles::SelectionHandle selectionHandle;

		if (uMsg == WM_LBUTTONDOWN) {
			POINT myPos;
			GetLocation(myPos);

			// determine which selection handle (if any) was selected
			m_selectionHandles.SetSelectionHandles(myPos.x, myPos.y, GetWidth(), GetHeight());
			selectionHandle = m_selectionHandles.GetSelectionHandleAtPoint(mousePoint);
			if (selectionHandle != CDXUTSelectionHandles::NOT_FOUND) {
				// Start Resize Menu
				prevMousePoint = mousePoint;
				bResizing = true;
				SetCapture(DXUTGetHWND());
				return true;
			}
		} else if (uMsg == WM_LBUTTONUP && bResizing) {
			// End Resize Menu
			bResizing = false;
			selectionHandle = CDXUTSelectionHandles::NOT_FOUND;
			ReleaseCapture();
			return true;

		} else if (uMsg == WM_MOUSEMOVE) {
			// Do Resize Menu
			if (bResizing) {
				POINT myPos;
				GetLocation(myPos, false);

				switch (selectionHandle) {
					case CDXUTSelectionHandles::TOP_LEFT:
						SetWidth(GetWidth() - (mousePoint.x - prevMousePoint.x));
						SetHeight(GetHeight() - (mousePoint.y - prevMousePoint.y));
						SetLocationX(myPos.x + (mousePoint.x - prevMousePoint.x));
						SetLocationY(myPos.y + (mousePoint.y - prevMousePoint.y));
						break;

					case CDXUTSelectionHandles::TOP_MIDDLE:
						SetHeight(GetHeight() - (mousePoint.y - prevMousePoint.y));
						SetLocationY(myPos.y + (mousePoint.y - prevMousePoint.y));
						break;

					case CDXUTSelectionHandles::TOP_RIGHT:
						SetWidth(GetWidth() + (mousePoint.x - prevMousePoint.x));
						SetHeight(GetHeight() - (mousePoint.y - prevMousePoint.y));
						SetLocationY(myPos.y + (mousePoint.y - prevMousePoint.y));
						break;

					case CDXUTSelectionHandles::BOTTOM_LEFT:
						SetWidth(GetWidth() - (mousePoint.x - prevMousePoint.x));
						SetHeight(GetHeight() + (mousePoint.y - prevMousePoint.y));
						SetLocationX(myPos.x + (mousePoint.x - prevMousePoint.x));
						break;

					case CDXUTSelectionHandles::BOTTOM_MIDDLE:
						SetHeight(GetHeight() + (mousePoint.y - prevMousePoint.y));
						break;

					case CDXUTSelectionHandles::BOTTOM_RIGHT:
						SetWidth(GetWidth() + (mousePoint.x - prevMousePoint.x));
						SetHeight(GetHeight() + (mousePoint.y - prevMousePoint.y));
						break;

					case CDXUTSelectionHandles::LEFT_MIDDLE:
						SetWidth(GetWidth() - (mousePoint.x - prevMousePoint.x));
						SetLocationX(myPos.x + (mousePoint.x - prevMousePoint.x));
						break;

					case CDXUTSelectionHandles::RIGHT_MIDDLE:
						SetWidth(GetWidth() + (mousePoint.x - prevMousePoint.x));
						break;
				}

				prevMousePoint = mousePoint;

				// Call Lua Dialog_OnMoved()
				OnMoved();

				// Call Lua Dialog_OnResized()
				OnResized();

			} else {
				POINT myPos;
				GetLocation(myPos);
				m_selectionHandles.SetSelectionHandles(myPos.x, myPos.y, GetWidth(), GetHeight());
				selectionHandle = m_selectionHandles.GetSelectionHandleAtPoint(mousePoint);
			}

			// this will prevent the sub controls from receiving any
			// events -- while editing we don't want the controls to
			// behave as if the game was running
			return true;
		} // if (WM_MOUSEMOVE)
	} // if(m_bResize)

	// If a control is in focus, it belongs to this dialog, and it's enabled, then give
	// it the first chance at handling the message.
	if (IsInFocus()) {
		// If the control MsgProc handles it, then we don't.
		if (s_pControlFocus->MsgProc(uMsg, wParam, lParam))
			return true;
	}

	switch (uMsg) {
		case WM_ACTIVATEAPP:
			// Call OnFocusIn()/OnFocusOut() of the control that currently has the focus
			// as the application is activated/deactivated.  This matches the Windows
			// behavior.
			if (s_pControlFocus &&
				s_pControlFocus->GetDialog() == this &&
				s_pControlFocus->GetEnabled()) {
				if (wParam)
					s_pControlFocus->OnFocusIn();
				else
					s_pControlFocus->OnFocusOut();
			}
			break;

			// Keyboard messages
		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
		case WM_KEYUP:
		case WM_SYSKEYUP: {
			// Ignore keys if this is not the focus dialog and the focus dialog is capturing keys.
			CDXUTDialog* pFocusDialog = GetDialogInFocus();
			if (pFocusDialog && pFocusDialog != this && pFocusDialog->GetCaptureKeys())
				return false;

			// If keyboard input is not enabled and not editing, this message should be ignored
			if (!m_bKeyboardInput && !m_bEdit)
				return false;

			// If a control is in focus, it belongs to this dialog, and it's enabled, then give
			// it the first chance at handling the message.
			if (s_pControlFocus &&
				s_pControlFocus->GetDialog() == this &&
				s_pControlFocus->GetEnabled()) {
				if (s_pControlFocus->HandleKeyboard(uMsg, wParam, lParam))
					return true;
			}

			// If not editing and
			// Not yet handled, see if this matches a control's hotkey
			// Activate the hotkey if the focus doesn't belong to an
			// edit box.
			if (uMsg == WM_KEYUP && !GetEdit() && (!s_pControlFocus || (s_pControlFocus->GetType() != DXUT_CONTROL_EDITBOX && s_pControlFocus->GetType() != DXUT_CONTROL_IMEEDITBOX))) {
				for (MenuControlIter iter = m_Controls.begin(); iter != m_Controls.end(); iter++) {
					IMenuControl* pControl = *iter;
					if (pControl->GetHotkey() == wParam) {
						pControl->OnHotkey();
						return true;
					}
				}
			}

			POINT keyInfo; // using this struct pass key info
				// x is used for the key value
				// y is used for the state of the SHIFT, CTRL, or ALT keys
			keyInfo.x = (LONG)wParam;

			bool bShiftDown = ((GetAsyncKeyState(VK_SHIFT) & 0x8000) != 0);
			bool bControlDown = ((GetAsyncKeyState(VK_CONTROL) & 0x8000) != 0);
			bool bAltDown = ((GetAsyncKeyState(VK_MENU) & 0x8000) != 0);
			keyInfo.y = bShiftDown ? 0x0001 : 0x0000;
			keyInfo.y += bControlDown ? 0x0010 : 0x0000;
			keyInfo.y += bAltDown ? 0x0100 : 0x0000;

			// Not yet handled, check for focus/selection messages
			if (uMsg == WM_KEYDOWN) {
				// Call Dialog_OnKeyDown() Event Handler
				SendEvent(EVENT_DIALOG_KEYDOWN, true, this, keyInfo);

				if (GetEdit()) {
					switch (wParam) {
						case VK_RIGHT: {
							if (s_pControlSelected != NULL) {
								// resize
								if (bShiftDown) {
									s_pControlSelected->SetSize(s_pControlSelected->GetWidth() + 1, s_pControlSelected->GetHeight());
									s_pControlSelected->OnResized();
								}
								// move
								else {
									POINT pt;
									s_pControlSelected->GetLocation(pt, false);
									s_pControlSelected->SetLocation(pt.x + 1, pt.y);
									s_pControlSelected->OnMoved();
								}

								return true;
							}

							// if the dialog is selected, move it
							if (GetIsSelected()) {
								// resize
								if (bShiftDown) {
									SetWidth(GetWidth() + 1);
									OnResized();
								}
								// move
								else {
									SetLocationX(GetLocationX(false) + 1);
									OnMoved();
								}
								return true;
							}

							break;
						}

						case VK_DOWN:
							if (s_pControlSelected != NULL) {
								// resize
								if (bShiftDown) {
									s_pControlSelected->SetSize(s_pControlSelected->GetWidth(), s_pControlSelected->GetHeight() + 1);
									s_pControlSelected->OnResized();
								}
								// move
								else {
									POINT pt;
									s_pControlSelected->GetLocation(pt, false);
									s_pControlSelected->SetLocation(pt.x, pt.y + 1);
									s_pControlSelected->OnMoved();
								}

								return true;
							}

							// if the dialog is selected, move it
							if (GetIsSelected()) {
								// resize
								if (bShiftDown) {
									SetHeight(GetHeight() + 1);
									OnResized();
								}
								// move
								else {
									SetLocationY(GetLocationY(false) + 1);
									OnMoved();
								}
								return true;
							}
							break;

						case VK_LEFT:
							if (s_pControlSelected != NULL) {
								// resize
								if (bShiftDown) {
									s_pControlSelected->SetSize(s_pControlSelected->GetWidth() - 1, s_pControlSelected->GetHeight());
									s_pControlSelected->OnResized();
								}
								// move
								else {
									POINT pt;
									s_pControlSelected->GetLocation(pt, false);
									s_pControlSelected->SetLocation(pt.x - 1, pt.y);
									s_pControlSelected->OnMoved();
								}

								return true;
							}

							// if the dialog is selected, move it
							if (GetIsSelected()) {
								// resize
								if (bShiftDown) {
									SetWidth(GetWidth() - 1);
									OnResized();
								}
								// move
								else {
									SetLocationX(GetLocationX(false) - 1);
									OnMoved();
								}
								return true;
							}

							break;

						case VK_UP:
							if (s_pControlSelected != NULL) {
								// resize
								if (bShiftDown) {
									s_pControlSelected->SetSize(s_pControlSelected->GetWidth(), s_pControlSelected->GetHeight() - 1);
									s_pControlSelected->OnResized();
								}
								// move
								else {
									POINT pt;
									s_pControlSelected->GetLocation(pt, false);
									s_pControlSelected->SetLocation(pt.x, pt.y - 1);
									s_pControlSelected->OnMoved();
								}

								return true;
							}

							// if the dialog is selected, move it
							if (GetIsSelected()) {
								// resize
								if (bShiftDown) {
									SetHeight(GetHeight() - 1);
									OnResized();
								}
								// move
								else {
									SetLocationY(GetLocationY(false) - 1);
									;
									OnMoved();
								}
								return true;
							}
							break;

						case VK_TAB:
							if (s_pControlSelected == NULL) {
								// for now, do nothing
							} else {
								return OnCycleSelection(!bShiftDown);
							}
							break;
					}

				} else {
					switch (wParam) {
						case VK_TAB:
							if (s_pControlFocus == NULL) {
								// Topmost dialog shouldn't be grabbing focus on tab key press.
								// This seems of little value and breaks other uses of the tab key.
								// For instance, this causes problems interacting with vendors in Kaneva City. [ED-4448]
								// return FocusDefaultControl();
							} else {
								return OnCycleFocus(!bShiftDown);
							}
							break;
					} // end switch wParam
				} // end if m_bedit
			} // end if WM_KEYDOWN
			else if (uMsg == WM_SYSKEYDOWN) {
				SendEvent(EVENT_DIALOG_SYSKEYDOWN, true, this, keyInfo);
			} else if (uMsg == WM_KEYUP) {
				SendEvent(EVENT_DIALOG_KEYUP, true, this, keyInfo);
			} else if (uMsg == WM_SYSKEYUP) {
				SendEvent(EVENT_DIALOG_SYSKEYUP, true, this, keyInfo);
			}

			// Is Modal And Not Being Edited ?
			if (IsActuallyModal())
				return true;

			break;
		}

			// Handle Mouse Messages - Part B
		case WM_MOUSEMOVE:
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_MBUTTONDOWN:
		case WM_MBUTTONUP:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
		case WM_XBUTTONDOWN:
		case WM_XBUTTONUP:
		case WM_LBUTTONDBLCLK:
		case WM_MBUTTONDBLCLK:
		case WM_RBUTTONDBLCLK:
		case WM_XBUTTONDBLCLK:
		case WM_MOUSEWHEEL: {
			// If not accepting mouse input, return false to indicate the message should still
			// be handled by the application (usually to move the camera).
			if (!GetEnableMouseInput())
				return false;

			if (uMsg == WM_LBUTTONDOWN && mousePointIsInside)
				SendEvent(EVENT_DIALOG_LBUTTONDOWN_INSIDE, true, this, mousePointMenu);

			// If a control is in focus, it belongs to this dialog, and it's enabled, then give
			// it the first chance at handling the message.
			if (s_pControlFocus && s_pControlFocus->GetDialog() == this && s_pControlFocus->GetEnabled())
				bHandled = s_pControlFocus->HandleMouse(uMsg, mousePointMenu, wParam, lParam);

			// Not yet handled, see if the mouse is over any controls
			if (!bHandled) {
				IMenuControl* pControl = GetControlAtPoint(mousePointMenu);

				if (pControl != NULL && (GetEdit() || pControl->GetEnabled())) {
					if (pControl != s_pControlFocus) {
						// s_pControlFocus already got to run its HandleMouse(), so don't do it again.
						bHandled = pControl->HandleMouse(uMsg, mousePointMenu, wParam, lParam);
					}
				} else {
					// Mouse not over any controls in this dialog, if there was a control
					// which had focus it just lost it
					if (uMsg == WM_LBUTTONDOWN && s_pControlFocus && s_pControlFocus->GetDialog() == this) {
						s_pControlFocus->OnFocusOut();
						s_pControlFocus = NULL;
					}

					if (uMsg == WM_LBUTTONDOWN) {
						// Is Modal And Not Being Edited ?
						if (IsActuallyModal()) {
							bHandled = true;
						} else {
							//	if the mouse is within the dialog consume the message
							//	NOTE: This will be true if someone clicks the mouse
							//	anywhere inside the app window.
							if (mousePointIsInside)
								SendEvent(EVENT_DIALOG_LBUTTONDOWN, true, this, mousePointMenu);
						}
					}
				}
			}

			// Stop Now If Message Handled
			if (bHandled) {
				KEPMenu::Instance()->SetCursorMenu(this);
				return true;
			}

			// Send Menu Mouse Event
			switch (uMsg) {
				case WM_MOUSEMOVE:
					OnMouseMove(mousePointMenu);
					break;

				case WM_LBUTTONDBLCLK:
					if (mousePointIsInside)
						SendEvent(EVENT_DIALOG_LBUTTONDBLCLK, true, this, mousePointMenu);
					break;

				case WM_RBUTTONDOWN:
					if (mousePointIsInside)
						SendEvent(EVENT_DIALOG_RBUTTONDOWN, true, this, mousePointMenu);
					break;

				case WM_LBUTTONUP:
					SendEvent(EVENT_DIALOG_LBUTTONUP, true, this, mousePointMenu);
					break;

				case WM_RBUTTONUP:
					SendEvent(EVENT_DIALOG_RBUTTONUP, true, this, mousePointMenu);
					break;
			}

			// DRF - These Conditions Always Handle Mouse Input
			// - Dragging Menus By Caption
			// - Modal Menus Not Being Edited
			// - Non-Passthrough Menus With Mouse Point Inside
			if (GetIsDraggingByCaption() || IsActuallyModal() || (mousePointIsInside && !GetPassthrough())) {
				KEPMenu::Instance()->SetCursorMenu(this);
				return true;
			}

			break;
		} // case MOUSE_ANYTHING

	} // switch(uMsg)

	return false;
}

bool CDXUTDialog::OnMoved() {
	POINT pt;
	GetLocation(pt);
	return SendEvent(EVENT_DIALOG_MOVED, true, this, pt);
}

bool CDXUTDialog::OnResized() {
	POINT pt = { GetWidth(), GetHeight() };
	return SendEvent(EVENT_DIALOG_RESIZED, true, this, pt);
}

const IMenuControl* CDXUTDialog::GetControlAtPoint(POINT pt) const {
	return const_cast<CDXUTDialog*>(this)->GetControlAtPoint(pt);
}

IMenuControl* CDXUTDialog::GetControlAtPoint(POINT pt) {
	// DRF - Dialog Passthrough All Clicks ? (Mouse Invisible Menus eg. tooltips)
	if (GetPassthrough())
		return NULL;

	// Search through all child controls for the first one which
	// contains the mouse point (last control is highest in z-order)
	for (int i = static_cast<int>(m_Controls.size()) - 1; i >= 0; i--) {
		IMenuControl* pControl = m_Controls[i];
		if (!pControl)
			continue;

		// Does Control Contain Point?
		if (pControl->ContainsPoint(pt)) {
			// DRF - Control Passthrough All Clicks ? (Mouse Invisible Menus eg. tooltips)
			if (pControl->GetPassthrough())
				continue;

			// We only return the current control if it is visible -- with two exceptions:
			// during edit mode we allow not-visible or disabled
			// Because GetControlAtPoint() is used to do mouse
			// hittest, it makes sense to perform this filtering.
			if (GetEdit() || pControl->GetVisible())
				return pControl;
		}
	}

	return NULL;
}

bool CDXUTDialog::GetControlEnabled(int ID) {
	IMenuControl* pControl = GetControl(ID);
	if (!pControl)
		return false;
	return pControl->GetEnabled();
}

bool CDXUTDialog::SetModal(bool modal) {
	if (m_bModal == modal)
		return true;
	m_bModal = modal;
	LogInfo(LogBool(modal) << " - " << ToStr());
	return true;
}

bool CDXUTDialog::SetCaptureKeys(bool capture) {
	if (m_bCaptureKeys == capture)
		return true;
	m_bCaptureKeys = capture;
	LogInfo(LogBool(capture) << " - " << ToStr());
	return true;
}

bool CDXUTDialog::SetAllControls(bool bVisible, bool bEnabled) {
	// Try to find the control with the given name
	for (MenuControlIter iter = m_Controls.begin(); iter != m_Controls.end(); iter++) {
		IMenuControl* pControl = *iter;

		pControl->SetVisible(bVisible);
		pControl->SetEnabled(bEnabled);
	}
	return true;
}

bool CDXUTDialog::SetControlEnabled(int ID, bool bEnabled) {
	IMenuControl* pControl = GetControl(ID);
	if (!pControl)
		return false;
	return pControl->SetEnabled(bEnabled);
}

void CDXUTDialog::OnMouseUp(POINT pt) {
	s_pControlPressed = NULL;
	m_pControlMouseOver = NULL;
}

static bool ControlIsClickable(IMenuControl* pControl) {
	if (!pControl || !pControl->GetEnabled() || !pControl->GetVisible())
		return false;
	UINT type = pControl->GetType();
	return type == DXUT_CONTROL_CHECKBOX || type == DXUT_CONTROL_RADIOBUTTON || type == DXUT_CONTROL_BUTTON;
}

void CDXUTDialog::OnMouseMove(POINT pt) {
	KEPMenu* kep_menu = KEPMenu::Instance();
	if (!kep_menu)
		return;

	// Send OnMouseMove Event
	SendEvent(EVENT_DIALOG_MOUSE_MOVE, true, this, pt);

	// Get Drag State
	bool isDragging = GetIsDraggingByCaption();
	bool isDraggable = (GetCaptionEnabled() && GetMovable());
	bool isInsideDraggableCaption = (isDraggable && ((pt.x >= 0) && (pt.x < GetWidth()) && (pt.y >= (0 - GetCaptionHeight())) && (pt.y < 0)));

	// Set Cursor Clickable If Cursor Not Set And Not Dragging Menu And Over Clickable Control
	IMenuControl* pControl = GetControlAtPoint(pt);
	if (!isDragging && ControlIsClickable(pControl))
		SetMenuCursorType(CURSOR_TYPE_CLICKABLE);

	// Set Cursor Draggable If Cursor Not Set And Dragging Menu Or Over Draggable Caption
	if (isDragging || isInsideDraggableCaption)
		SetMenuCursorType(CURSOR_TYPE_DRAGGABLE);

	// If the mouse is still over the same control, nothing needs to be done
	if (pControl == m_pControlMouseOver)
		return;

	// Handle mouse leaving the old control
	if (m_pControlMouseOver)
		m_pControlMouseOver->OnMouseLeave();

	// Handle mouse entering the new control
	m_pControlMouseOver = pControl;
	if (pControl != NULL)
		m_pControlMouseOver->OnMouseEnter(pt);
}

HRESULT CDXUTDialog::SetDefaultElement(UINT nControlType, UINT iElement, CDXUTElement* pElement) {
	// If this Element type already exist in the list, simply update the stored Element
	for (int i = 0; i < m_DefaultElements.GetSize(); i++) {
		DXUTElementHolder* pElementHolder = m_DefaultElements.GetAt(i);

		if (pElementHolder->nControlType == nControlType &&
			pElementHolder->iElement == iElement) {
			pElementHolder->Element = *pElement;
			return S_OK;
		}
	}

	// Otherwise, add a new entry
	DXUTElementHolder* pNewHolder;
	pNewHolder = new DXUTElementHolder;
	if (pNewHolder == NULL)
		return E_OUTOFMEMORY;

	pNewHolder->nControlType = nControlType;
	pNewHolder->iElement = iElement;
	pNewHolder->Element = *pElement;

	m_DefaultElements.Add(pNewHolder);
	return S_OK;
}

CDXUTElement* CDXUTDialog::GetDefaultElement(UINT nControlType, UINT iElement) {
	for (int i = 0; i < m_DefaultElements.GetSize(); i++) {
		DXUTElementHolder* pElementHolder = m_DefaultElements.GetAt(i);

		if (pElementHolder->nControlType == nControlType &&
			pElementHolder->iElement == iElement) {
			return &pElementHolder->Element;
		}
	}

	return NULL;
}

HRESULT CDXUTDialog::AddStatic(int ID, const char* strText, int x, int y, int width, int height, bool bShadow, bool bIsDefault, CDXUTStatic** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTStatic* pStatic = new CDXUTStatic(this, bShadow);

	if (ppCreated != NULL)
		*ppCreated = pStatic;

	if (pStatic == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pStatic);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;

	// Set the ID and list index
	pStatic->SetID(ID);
	pStatic->SetText(strText);
	pStatic->SetLocation(x, y);
	pStatic->SetSize(width, height);
	pStatic->SetIsDefault(bIsDefault);

	return S_OK;
}

HRESULT CDXUTDialog::AddImage(int ID, int x, int y, int width, int height, UINT iTexture, RECT* prcTexture, D3DCOLOR defaultTextureColor, CDXUTImage** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTImage* pImage = new CDXUTImage(this);

	if (ppCreated != NULL)
		*ppCreated = pImage;

	if (pImage == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pImage);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;

	// Set the ID and list index
	pImage->SetID(ID);
	pImage->SetLocation(x, y);
	pImage->SetSize(width, height);

	// Set the texture and its color
	CDXUTElement element;
	element.SetTexture(iTexture, prcTexture, defaultTextureColor);
	pImage->SetElement(0, &element);

	return S_OK;
}

HRESULT CDXUTDialog::AddImageToBack(const char* strName, int x, int y, int width, int height, const std::string& textureName, RECT coords, D3DCOLOR defaultTextureColor, IMenuControl** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTImage* pImage = new CDXUTImage(this);

	if (ppCreated != NULL)
		*ppCreated = pImage;

	if (pImage == NULL)
		return E_OUTOFMEMORY;

	hr = AddControlToBack(pImage);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;
	pImage->SetID(m_nextControlValue);

	// Set the name and list index
	pImage->SetName(strName);

	pImage->SetLocation(x, y);
	pImage->SetSize(width, height);

	// Set the texture and its color
	CDXUTElement element;
	RECT texSlices = element.GetTextureSlices(); // todo - should be passed parameter?
	bool ok = element.AddTexture(this, textureName, coords, texSlices);
	if (ok && !textureName.empty()) { // Local texture
		// only if texture was successfully added (and a texture name was given)
		// use the new texture coords
		element.SetTextureCoords(coords);
		//TODO: Something with rcSlices
	} else {
		// (otherwise, the default values will be used)
		CDXUTElement* defaultElement = GetDefaultElement(pImage->GetType(), 0);
		element.SetTextureCoords(defaultElement->GetTextureCoords());
		element.SetTextureSlices(defaultElement->GetTextureSlices());
	}

	// use the texture color provided
	(*(element.GetTextureColor())).Init(defaultTextureColor);

	pImage->SetElement(0, &element);

	return S_OK;
}

HRESULT CDXUTDialog::AddButton(int ID, const char* strText, int x, int y, int width, int height, UINT nHotkey, bool bIsDefault, CDXUTButton** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTButton* pButton = new CDXUTButton(this);

	if (ppCreated != NULL)
		*ppCreated = pButton;

	if (pButton == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pButton);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;

	// Set the ID and list index
	pButton->SetID(ID);
	pButton->SetText(strText);
	pButton->SetLocation(x, y);
	pButton->SetSize(width, height);
	pButton->SetHotkey(nHotkey);
	pButton->SetIsDefault(bIsDefault);

	return S_OK;
}

HRESULT CDXUTDialog::AddCheckBox(int ID, const char* strText, int x, int y, int width, int height, bool bChecked, UINT nHotkey, bool bIsDefault, CDXUTCheckBox** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTCheckBox* pCheckBox = new CDXUTCheckBox(this);

	if (ppCreated != NULL)
		*ppCreated = pCheckBox;

	if (pCheckBox == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pCheckBox);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;

	// Set the ID and list index
	pCheckBox->SetID(ID);
	pCheckBox->SetText(strText);
	pCheckBox->SetLocation(x, y);
	pCheckBox->SetSize(width, height);
	pCheckBox->SetHotkey(nHotkey);
	pCheckBox->SetIsDefault(bIsDefault);
	pCheckBox->SetChecked(bChecked);

	return S_OK;
}

HRESULT CDXUTDialog::AddRadioButton(int ID, UINT nButtonGroup, const char* strText, int x, int y, int width, int height, bool bChecked, UINT nHotkey, bool bIsDefault, CDXUTRadioButton** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTRadioButton* pRadioButton = new CDXUTRadioButton(this);

	if (ppCreated != NULL)
		*ppCreated = pRadioButton;

	if (pRadioButton == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pRadioButton);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;

	// Set the ID and list index
	pRadioButton->SetID(ID);
	pRadioButton->SetText(strText);
	pRadioButton->SetButtonGroup(nButtonGroup);
	pRadioButton->SetLocation(x, y);
	pRadioButton->SetSize(width, height);
	pRadioButton->SetHotkey(nHotkey);
	pRadioButton->SetChecked(bChecked);
	pRadioButton->SetIsDefault(bIsDefault);
	pRadioButton->SetChecked(bChecked);

	return S_OK;
}

HRESULT CDXUTDialog::AddComboBox(int ID, int x, int y, int width, int height, UINT nHotkey, bool bIsDefault, CDXUTComboBox** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTComboBox* pComboBox = new CDXUTComboBox(this);

	if (ppCreated != NULL)
		*ppCreated = pComboBox;

	if (pComboBox == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pComboBox);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;

	// Set the ID and list index
	pComboBox->SetID(ID);
	pComboBox->SetLocation(x, y);
	pComboBox->SetSize(width, height);
	pComboBox->SetHotkey(nHotkey);
	pComboBox->SetIsDefault(bIsDefault);

	return S_OK;
}

HRESULT CDXUTDialog::AddSlider(int ID, int x, int y, int width, int height, int min, int max, int value, bool bIsDefault, CDXUTSlider** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTSlider* pSlider = new CDXUTSlider(this);

	if (ppCreated != NULL)
		*ppCreated = pSlider;

	if (pSlider == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pSlider);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;

	// Set the ID and list index
	pSlider->SetID(ID);
	pSlider->SetLocation(x, y);
	pSlider->SetSize(width, height);
	pSlider->SetIsDefault(bIsDefault);
	pSlider->SetRange(min, max);
	pSlider->SetValue(value);

	return S_OK;
}

HRESULT CDXUTDialog::AddEditBox(int ID, const char* strText, int x, int y, int width, int height, bool typeSimple, bool bIsDefault, CDXUTEditBox** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTEditBox* pEditBox = new CDXUTEditBox(this, typeSimple);

	if (ppCreated != NULL)
		*ppCreated = pEditBox;

	if (pEditBox == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pEditBox);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;

	// Set the ID and position
	pEditBox->SetID(ID);
	pEditBox->SetLocation(x, y);
	pEditBox->SetSize(width, height);
	pEditBox->SetIsDefault(bIsDefault);

	if (strText)
		pEditBox->SetText(strText);

	return S_OK;
}

HRESULT CDXUTDialog::AddListBox(int ID, int x, int y, int width, int height, DWORD dwStyle, CDXUTListBox** ppCreated) {
	HRESULT hr = S_OK;
	CDXUTListBox* pListBox = new CDXUTListBox(this);

	if (ppCreated != NULL)
		*ppCreated = pListBox;

	if (pListBox == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pListBox);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;

	// Set the ID and position
	pListBox->SetID(ID);
	pListBox->SetLocation(x, y);
	pListBox->SetSize(width, height);
	pListBox->SetStyle(dwStyle);

	return S_OK;
}

HRESULT CDXUTDialog::AddStatic(const char* strName, const char* strText, int x, int y, int width, int height, bool bShadow, bool bIsDefault, IMenuControl** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTStatic* pStatic = new CDXUTStatic(this, bShadow);

	if (ppCreated != NULL)
		*ppCreated = pStatic;

	if (pStatic == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pStatic);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;
	pStatic->SetID(m_nextControlValue);

	// Set the name and list index
	pStatic->SetName(strName);

	if (strlen(strText))
		pStatic->SetText(strText);
	else
		pStatic->SetTextWithDefault();

	pStatic->SetLocation(x, y);
	pStatic->SetSize(width, height);
	pStatic->SetIsDefault(bIsDefault);

	return S_OK;
}

HRESULT CDXUTDialog::AddImage(const char* strName, int x, int y, int width, int height, const std::string& textureName, RECT coords, D3DCOLOR defaultTextureColor, IMenuControl** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTImage* pImage = new CDXUTImage(this);

	if (ppCreated != NULL)
		*ppCreated = pImage;

	if (pImage == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pImage);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;
	pImage->SetID(m_nextControlValue);

	// Set the name and list index
	pImage->SetName(strName);

	pImage->SetLocation(x, y);
	pImage->SetSize(width, height);

	// Set the texture and its color
	CDXUTElement element;
	RECT texSlices = element.GetTextureSlices(); // todo - should be passed parameter?
	bool ok = element.AddTexture(this, textureName, coords, texSlices);
	if (ok && !textureName.empty()) { // Local Texture
		// only if texture was successfully added (and a texture name was given)
		// use the new texture coords
		element.SetTextureCoords(coords);
		//TODO: Something with rcSlices
	} else {
		// (otherwise, the default values will be used)
		CDXUTElement* defaultElement = GetDefaultElement(pImage->GetType(), 0);
		element.SetTextureCoords(defaultElement->GetTextureCoords());
		element.SetTextureSlices(defaultElement->GetTextureSlices());
	}

	// use the texture color provided
	(*(element.GetTextureColor())).Init(defaultTextureColor);

	pImage->SetElement(0, &element);

	return S_OK;
}

HRESULT CDXUTDialog::AddButton(const char* strName, const char* strText, int x, int y, int width, int height, UINT nHotkey, bool bIsDefault, IMenuControl** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTButton* pButton = new CDXUTButton(this);

	if (ppCreated != NULL)
		*ppCreated = pButton;

	if (pButton == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pButton);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;
	pButton->SetID(m_nextControlValue);

	// Set the name and list index
	pButton->SetName(strName);

	if (strlen(strText))
		pButton->SetText(strText);
	else
		pButton->SetTextWithDefault();

	pButton->SetLocation(x, y);
	pButton->SetSize(width, height);
	pButton->SetHotkey(nHotkey);
	pButton->SetIsDefault(bIsDefault);

	return S_OK;
}

HRESULT CDXUTDialog::AddCheckBox(const char* strName, const char* strText, int x, int y, int width, int height, bool bChecked, UINT nHotkey, bool bIsDefault, IMenuControl** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTCheckBox* pCheckBox = new CDXUTCheckBox(this);

	if (ppCreated != NULL)
		*ppCreated = pCheckBox;

	if (pCheckBox == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pCheckBox);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;
	pCheckBox->SetID(m_nextControlValue);

	// Set the name and list index
	pCheckBox->SetName(strName);

	if (strlen(strText))
		pCheckBox->SetText(strText);
	else
		pCheckBox->SetTextWithDefault();

	pCheckBox->SetLocation(x, y);
	pCheckBox->SetSize(width, height);
	pCheckBox->SetHotkey(nHotkey);
	pCheckBox->SetIsDefault(bIsDefault);
	pCheckBox->SetChecked(bChecked);

	return S_OK;
}

HRESULT CDXUTDialog::AddRadioButton(const char* strName, UINT nButtonGroup, const char* strText, int x, int y, int width, int height, bool bChecked, UINT nHotkey, bool bIsDefault, IMenuControl** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTRadioButton* pRadioButton = new CDXUTRadioButton(this);

	if (ppCreated != NULL)
		*ppCreated = pRadioButton;

	if (pRadioButton == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pRadioButton);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;
	pRadioButton->SetID(m_nextControlValue);

	// Set the name and list index
	pRadioButton->SetName(strName);

	if (strlen(strText))
		pRadioButton->SetText(strText);
	else
		pRadioButton->SetTextWithDefault();

	pRadioButton->SetButtonGroup(nButtonGroup);
	pRadioButton->SetLocation(x, y);
	pRadioButton->SetSize(width, height);
	pRadioButton->SetHotkey(nHotkey);
	pRadioButton->SetChecked(bChecked);
	pRadioButton->SetIsDefault(bIsDefault);
	pRadioButton->SetChecked(bChecked);

	return S_OK;
}

HRESULT CDXUTDialog::AddComboBox(const char* strName, int x, int y, int width, int height, UINT nHotkey, bool bIsDefault, IMenuControl** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTComboBox* pComboBox = new CDXUTComboBox(this);

	if (ppCreated != NULL)
		*ppCreated = pComboBox;

	if (pComboBox == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pComboBox);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;
	pComboBox->SetID(m_nextControlValue);

	// Set the name and list index
	pComboBox->SetName(strName);
	pComboBox->SetLocation(x, y);
	pComboBox->SetSize(width, height);
	pComboBox->SetHotkey(nHotkey);
	pComboBox->SetIsDefault(bIsDefault);

	return S_OK;
}

HRESULT CDXUTDialog::AddSlider(const char* strName, int x, int y, int width, int height, int min, int max, int value, bool bIsDefault, IMenuControl** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTSlider* pSlider = new CDXUTSlider(this);

	if (ppCreated != NULL)
		*ppCreated = pSlider;

	if (pSlider == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pSlider);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;
	pSlider->SetID(m_nextControlValue);

	// Set the name and list index
	pSlider->SetName(strName);
	pSlider->SetLocation(x, y);
	pSlider->SetSize(width, height);
	pSlider->SetIsDefault(bIsDefault);
	pSlider->SetRange(min, max);
	pSlider->SetValue(value);

	return S_OK;
}

HRESULT CDXUTDialog::AddEditBox(const char* strName, const char* strText, int x, int y, int width, int height, bool typeSimple, bool bIsDefault, IMenuControl** ppCreated) {
	HRESULT hr = S_OK;

	CDXUTEditBox* pEditBox = new CDXUTEditBox(this, typeSimple);

	if (ppCreated != NULL)
		*ppCreated = pEditBox;

	if (pEditBox == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pEditBox);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;
	pEditBox->SetID(m_nextControlValue);

	// Set the name and position
	pEditBox->SetName(strName);

	pEditBox->SetLocation(x, y);
	pEditBox->SetSize(width, height);
	pEditBox->SetIsDefault(bIsDefault);

	if (strText)
		pEditBox->SetText(strText);

	return S_OK;
}

HRESULT CDXUTDialog::AddListBox(const char* strName, int x, int y, int width, int height, DWORD dwStyle, IMenuControl** ppCreated) {
	HRESULT hr = S_OK;
	CDXUTListBox* pListBox = new CDXUTListBox(this);

	if (ppCreated != NULL)
		*ppCreated = pListBox;

	if (pListBox == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pListBox);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;
	pListBox->SetID(m_nextControlValue);

	// Set the name and position
	pListBox->SetName(strName);
	pListBox->SetLocation(x, y);
	pListBox->SetSize(width, height);
	pListBox->SetStyle(dwStyle);

	return S_OK;
}

HRESULT CDXUTDialog::AddFlash(const char* strName, int x, int y, int width, int height,
	bool wantInput, bool sizeToFit, bool loop, const char* params, IMenuControl** ppCreated) {
	HRESULT hr = S_OK;
	CDXUTFlashPlayer* pFlash = new CDXUTFlashPlayer(this);

	if (ppCreated != NULL)
		*ppCreated = pFlash;

	if (pFlash == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pFlash);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;
	pFlash->SetID(m_nextControlValue);

	// Set the name and position
	pFlash->SetText(DEFAULT_FLASH_TEXT);
	pFlash->SetName(strName);
	pFlash->SetLocation(x, y);
	pFlash->SetSize(width, height);
	pFlash->SetWantInput(wantInput);
	pFlash->SetLoopSwf(loop);
	pFlash->SetSizeToFit(sizeToFit);
	pFlash->SetSwfParameters(params);

	return S_OK;
}

HRESULT CDXUTDialog::AddFlash(int ID, int x, int y, int width, int height,
	bool wantInput, bool sizeToFit, bool loop, const char* params, CDXUTFlashPlayer** ppCreated) {
	HRESULT hr = S_OK;
	CDXUTFlashPlayer* pFlash = new CDXUTFlashPlayer(this);

	if (ppCreated != NULL)
		*ppCreated = pFlash;

	if (pFlash == NULL)
		return E_OUTOFMEMORY;

	hr = AddControl(pFlash);
	if (FAILED(hr))
		return hr;

	m_nextControlValue++;

	// Set the ID and position
	pFlash->SetID(ID);
	pFlash->SetLocation(x, y);
	pFlash->SetSize(width, height);
	pFlash->SetWantInput(wantInput);
	pFlash->SetLoopSwf(loop);
	pFlash->SetSizeToFit(sizeToFit);
	pFlash->SetSwfParameters(params);

	return S_OK;
}

HRESULT CDXUTDialog::InitControl(IMenuControl* pControl) {
	HRESULT hr;

	if (pControl == NULL)
		return E_INVALIDARG;

	// Look for a default Element entries
	for (int i = 0; i < m_DefaultElements.GetSize(); i++) {
		DXUTElementHolder* pElementHolder = m_DefaultElements.GetAt(i);
		if (pElementHolder->nControlType == pControl->GetType())
			pControl->SetElement(pElementHolder->iElement, &pElementHolder->Element);
	}

	V_RETURN(pControl->OnInit());

	return S_OK;
}

HRESULT CDXUTDialog::AddControl(IMenuControl* pControl) {
	HRESULT hr = S_OK;

	hr = InitControl(pControl);
	if (FAILED(hr))
		return DXTRACE_ERR(_T("CDXUTDialog::InitControl"), hr);

	// Add to the list
	m_Controls.push_back(pControl);

	return S_OK;
}

//Adds Control to begin() of vector so it will be drawn first (behind all other elements in the dialog)
HRESULT CDXUTDialog::AddControlToBack(IMenuControl* pControl) {
	HRESULT hr = S_OK;

	hr = InitControl(pControl);
	if (FAILED(hr))
		return DXTRACE_ERR(_T("CDXUTDialog::InitControl"), hr);

	// Add to the list
	m_Controls.insert(m_Controls.begin(), pControl);

	return S_OK;
}

IMenuControl* CDXUTDialog::GetControl(const char* strName) {
	if (!strName)
		return NULL;
	for (MenuControlIter iter = m_Controls.begin(); iter != m_Controls.end(); iter++) {
		IMenuControl* pControl = *iter;
		if (!strcmp(pControl->GetName(), strName))
			return pControl;
	}
	return NULL;
}

IMenuControl* CDXUTDialog::GetControl(int ID) {
	for (MenuControlIter iter = m_Controls.begin(); iter != m_Controls.end(); iter++) {
		IMenuControl* pControl = *iter;
		if (pControl->GetID() == ID)
			return pControl;
	}
	return NULL;
}

IMenuControl* CDXUTDialog::GetControl(int ID, UINT nControlType) {
	for (MenuControlIter iter = m_Controls.begin(); iter != m_Controls.end(); iter++) {
		IMenuControl* pControl = *iter;
		if (pControl->GetID() == ID && pControl->GetType() == nControlType)
			return pControl;
	}
	return NULL;
}

IMenuControl* CDXUTDialog::GetControlByIndex(int index) {
	assert(index >= 0 && index < GetNumControls());
	return m_Controls[index];
}

int CDXUTDialog::GetControlIndex(IMenuControl* pControl) {
	int index = 0;
	int num_controls = GetNumControls();
	while (index < num_controls) {
		IMenuControl* pNewControl = m_Controls[index];
		if (pNewControl == pControl)
			return index;

		index++;
	}

	return -1;
}

IMenuControl* CDXUTDialog::_GetNextControl(IMenuControl* pControl) {
	IMenuDialog* pDialog = pControl->GetDialog();
	int index = pDialog->GetControlIndex(pControl) + 1;

	// Cycle through dialogs in the loop to find the next control. Note
	// that if only one control exists in all looped dialogs it will
	// be the returned 'next' control.
	while (index >= pControl->GetDialog()->GetNumControls()) {
		pDialog = pDialog->GetNextDialog();
		index = 0;
	}

	return pDialog->GetControlByIndex(index);
}

IMenuControl* CDXUTDialog::_GetPrevControl(IMenuControl* pControl) {
	IMenuDialog* pDialog = pControl->GetDialog();
	int index = pDialog->GetControlIndex(pControl) - 1;

	// Cycle through dialogs in the loop to find the next control. Note
	// that if only one control exists in all looped dialogs it will
	// be the returned 'previous' control.
	while (index < 0) {
		pDialog = pDialog->GetPrevDialog();
		if (pDialog == NULL)
			pDialog = pControl->GetDialog();

		index = pDialog->GetNumControls() - 1;
	}

	return pDialog->GetControlByIndex(index);
}

const char* CDXUTDialog::GetControlXml(IMenuControl* pControl) {
	if (pControl) {
		//create an xml doc and save the root tag of the control .. <static>
		TiXmlDocument xml_doc;
		TiXmlNode* root = xml_doc.InsertEndChild(TiXmlElement(pControl->GetTag()));
		if (root) {
			//save off the rest of the xml
			pControl->Save(root);

			TiXmlPrinter printer;
			xml_doc.Accept(&printer);
			//store the xml as a string member variable
			pControl->SetXml(printer.CStr());
			return pControl->GetXml();
		}
	}
	return "";
}

bool CDXUTDialog::ClearRadioButtonGroup(UINT nButtonGroup) {
	// Find all radio buttons with the given group number
	for (MenuControlIter iter = m_Controls.begin(); iter != m_Controls.end(); iter++) {
		IMenuControl* pControl = *iter;
		if (pControl->GetType() == DXUT_CONTROL_RADIOBUTTON) {
			CDXUTRadioButton* pRadioButton = (CDXUTRadioButton*)pControl;

			if (pRadioButton->GetButtonGroup() == nButtonGroup)
				pRadioButton->SetChecked(false, false);
		}
	}
	return true;
}

bool CDXUTDialog::ClearComboBox(int ID) {
	CDXUTComboBox* pComboBox = GetComboBox(ID);
	if (!pComboBox)
		return false;
	pComboBox->RemoveAllItems();
	return true;
}

bool CDXUTDialog::RequestSelection(IMenuControl* pControl, bool bEnable) {
	if (!pControl)
		return false;

	// if the dialog was previously selected, de-select it
	if (m_bIsSelected)
		m_bIsSelected = false;

	if (s_pControlSelected)
		s_pControlSelected->OnSelectionOff();

	if (bEnable)
		pControl->OnSelectionOn();
	else
		pControl->OnSelectionOff();

	s_pControlSelected = pControl;

	return true;
}

bool CDXUTDialog::RequestFocus(IMenuControl* pControl) {
	// Already In Focus ?
	if (s_pControlFocus == pControl)
		return true;

	// Allowed Focus ?
	if (!pControl || !pControl->CanHaveFocus())
		return false;

	// don't alter focus when in edit mode
	if (m_bEdit)
		return false;

	if (s_pControlFocus)
		s_pControlFocus->OnFocusOut();

	pControl->OnFocusIn();
	s_pControlFocus = pControl;

	return true;
}

HRESULT CDXUTDialog::DrawRect(RECT* pRect, D3DCOLOR color) {
	ASSERT_GDRM(S_OK);

	if (!pRect)
		return S_OK;

	RECT rcScreen = *pRect;
	POINT myPos;
	GetLocation(myPos);
	OffsetRect(&rcScreen, myPos.x, myPos.y);

	// If caption is enabled, offset the Y position by its height.
	if (m_bCaption)
		OffsetRect(&rcScreen, 0, GetCaptionHeight());

	DXUT_SCREEN_VERTEX vertices[4] = {
		(float)rcScreen.left - 0.5f,
		(float)rcScreen.top - 0.5f,
		0.5f,
		1.0f,
		color,
		0,
		0,
		(float)rcScreen.right - 0.5f,
		(float)rcScreen.top - 0.5f,
		0.5f,
		1.0f,
		color,
		0,
		0,
		(float)rcScreen.right - 0.5f,
		(float)rcScreen.bottom - 0.5f,
		0.5f,
		1.0f,
		color,
		0,
		0,
		(float)rcScreen.left - 0.5f,
		(float)rcScreen.bottom - 0.5f,
		0.5f,
		1.0f,
		color,
		0,
		0,
	};

	IDirect3DDevice9* pd3dDevice = pGDRM->GetD3DDevice();
	if (!pd3dDevice)
		return S_OK;

	// Since we're doing our own drawing here we need to flush the sprites
	ID3DXSprite* sprite = pGDRM->GetSprite();
	if (sprite)
		sprite->Flush();

	IDirect3DVertexDeclaration9* pDecl = NULL;
	pd3dDevice->GetVertexDeclaration(&pDecl); // Preserve the sprite's current vertex decl
	if (!pDecl)
		return S_OK;

	pd3dDevice->SetFVF(DXUT_SCREEN_VERTEX::FVF);

	pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG2);
	pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG2);

	pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN, 2, vertices, sizeof(DXUT_SCREEN_VERTEX));

	pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);

	// Restore the vertex decl
	pd3dDevice->SetVertexDeclaration(pDecl);
	pDecl->Release();

	return S_OK;
}

HRESULT CDXUTDialog::DrawPolyLine(POINT* apPoints, UINT nNumPoints, D3DCOLOR color) {
	ASSERT_GDRM(S_OK);

	DXUT_SCREEN_VERTEX* vertices = new DXUT_SCREEN_VERTEX[nNumPoints];
	if (!vertices)
		return E_OUTOFMEMORY;

	if (!apPoints)
		return S_OK;
	DXUT_SCREEN_VERTEX* pVertex = vertices;
	POINT* pt = apPoints;
	for (UINT i = 0; i < nNumPoints; i++) {
		POINT myPos;
		GetLocation(myPos);

		pVertex->x = myPos.x + (float)pt->x;
		pVertex->y = myPos.y + (float)pt->y;
		pVertex->z = 0.5f;
		pVertex->h = 1.0f;
		pVertex->color = color;
		pVertex->tu = 0.0f;
		pVertex->tv = 0.0f;

		pVertex++;
		pt++;
	}

	IDirect3DDevice9* pd3dDevice = pGDRM->GetD3DDevice();
	if (!pd3dDevice)
		return S_OK;

	// Since we're doing our own drawing here we need to flush the sprites
	ID3DXSprite* sprite = DXUTGetGlobalDialogResourceManager()->GetSprite();
	if (sprite)
		sprite->Flush();

	IDirect3DVertexDeclaration9* pDecl = NULL;
	pd3dDevice->GetVertexDeclaration(&pDecl); // Preserve the sprite's current vertex decl
	if (!pDecl)
		return S_OK;

	pd3dDevice->SetFVF(DXUT_SCREEN_VERTEX::FVF);

	pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG2);
	pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG2);

	pd3dDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, nNumPoints - 1, vertices, sizeof(DXUT_SCREEN_VERTEX));

	pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);

	// Restore the vertex decl
	pd3dDevice->SetVertexDeclaration(pDecl);
	pDecl->Release();

	SAFE_DELETE_ARRAY(vertices);
	return S_OK;
}

HRESULT DrawToRect(CDXUTElement* pElement, IDirect3DTexture9* pD3DTexture, RECT& rcScreen, RECT& rcTexture, const DXUTTextureNode* pTextureNode) {
	if (RectWidth(rcTexture) == 0 || RectHeight(rcTexture) == 0)
		return S_OK;

	// Clamp rect within texture
	rcTexture.left = std::min(rcTexture.left, (long) pTextureNode->GetSurfaceWidth());
	rcTexture.right = std::min(rcTexture.right, (long) pTextureNode->GetSurfaceWidth());
	rcTexture.top = std::min(rcTexture.top, (long) pTextureNode->GetSurfaceHeight());
	rcTexture.bottom = std::min(rcTexture.bottom, (long) pTextureNode->GetSurfaceHeight());

	float fScaleX = (float)RectWidth(rcScreen) / RectWidth(rcTexture);
	float fScaleY = (float)RectHeight(rcScreen) / RectHeight(rcTexture);
	if (fScaleX == 0.0f || fScaleY == 0.0f)
		return S_OK;

	ASSERT_GDRM(S_OK);

	D3DXMATRIXA16 matTransform;
	D3DXMatrixScaling(&matTransform, fScaleX, fScaleY, 1.0f);

	ID3DXSprite* sprite = pGDRM->GetSprite();
	if (!sprite)
		return S_OK;

	sprite->SetTransform(&matTransform);

	D3DXVECTOR3 vPos((float)rcScreen.left, (float)rcScreen.top, 0.0f);

	vPos.x /= fScaleX;
	vPos.y /= fScaleY;

	HRESULT ret = S_OK;
	if (pD3DTexture && pElement) {
		pGDRM->GetSpriteWrapper()->SetCurrentTextureType(pTextureNode->GetType());
		ret = sprite->Draw(pD3DTexture, &rcTexture, NULL, &vPos, (*(pElement->GetTextureColor())).GetCurrentColor());
		pGDRM->GetSpriteWrapper()->SetCurrentTextureType(DXUTTextureNode::TYPE_NONE);
	}

	return ret;
}

HRESULT CDXUTDialog::DrawSprite(CDXUTElement* pElement, RECT* prcDest) {
	// No need to draw fully transparent layers
	if ((*(pElement->GetTextureColor())).GetCurrentColor().a == 0)
		return S_OK;

	// DRF - Retry Failed Textures (true=ok false=failure)
	if (!pElement->RetryFailedTexture(this))
		return S_OK;

	DXUTTextureNode* pTextureNode = GetTexture(pElement->GetTextureIndex());
	if (!pTextureNode)
		return S_OK;

	// Get Texture Node Texture
	IDirect3DTexture9* pD3DTexture = pTextureNode->GetTexture();

	if (!pD3DTexture && pTextureNode->TypeExternal()) {
		// external texture
		if (pTextureNode->TypeExternal()) {
			std::string texFileName = pTextureNode->GetFileName();
			pD3DTexture = KEPMenu::Instance()->GetD3DTextureIfLoadedOrQueueToLoad(texFileName);
		}

		if (pD3DTexture) {
			pTextureNode->SetTexture(pD3DTexture);
		} else {
			// external texture not available - use progress texture if defined
			if (pElement->GetProgressTextureIndex() >= 0) {
				DXUTTextureNode* pProgTextureNode = GetTexture(pElement->GetProgressTextureIndex());
				if (pProgTextureNode) {
					pTextureNode = pProgTextureNode;
					pD3DTexture = pTextureNode->GetTexture();
				}
			}
		}
	}

	if (!pD3DTexture)
		return S_OK; // TODO: Return something reasonable

	if (!pTextureNode->IsDimensionSet()) {
		assert(pTextureNode->GetType() != DXUTTextureNode::TYPE_INTERNAL); // Internal texture is created in DxCreateTexture() and should have been fully inited
		pTextureNode->InitDimension(pD3DTexture);
	}

	RECT rcTexture = pElement->GetTextureCoords();
	RECT rcSlices = pElement->GetTextureSlices();

	RECT rcScreen = *prcDest;
	POINT myPos;
	GetLocation(myPos);
	OffsetRect(&rcScreen, myPos.x, myPos.y);

	// allow script to use a texture without knowing its dimension
	if (rcTexture.right == -1)
		rcTexture.right = pTextureNode->GetWidth();
	if (rcTexture.bottom == -1)
		rcTexture.bottom = pTextureNode->GetHeight();

	// If caption is enabled, offset the Y position by its height.
	if (m_bCaption)
		OffsetRect(&rcScreen, 0, GetCaptionHeight());

	HRESULT ret = S_OK; // This value is pretty much worthless: each 9-slices draw call overwrites it; if pD3DTexture is NULL, it's always S_OK.
	if ((rcSlices.left == 0) && (rcSlices.top == 0) && (rcSlices.right == 0) && (rcSlices.bottom == 0)) {
		// No 9-slice nonsense
		ret = DrawToRect(pElement, pD3DTexture, rcScreen, rcTexture, pTextureNode);
	} else {
		// If the texture coords are flipped, flip the slices too
		// But NOT the screen-space slices
		RECT rcSlicesScr = rcSlices;
		if (rcTexture.left > rcTexture.right) {
			rcSlices.left *= -1;
			rcSlices.right *= -1;
		}
		if (rcTexture.top > rcTexture.bottom) {
			rcSlices.top *= -1;
			rcSlices.bottom *= -1;
		}

		// Draw the top row if needed
		if (rcSlices.top != 0) {
			// Adjust the texture coords for this row
			RECT rcRow = { rcTexture.left, rcTexture.top, rcTexture.right, rcTexture.top + rcSlices.top };
			RECT rcRowScr = { rcScreen.left, rcScreen.top, rcScreen.right, rcScreen.top + rcSlicesScr.top };

			// Draw the left if needed
			if (rcSlices.left != 0) {
				RECT rcElem = { rcRow.left, rcRow.top, rcRow.left + rcSlices.left, rcRow.bottom };
				RECT rcElemScr = { rcRowScr.left, rcRowScr.top, rcRowScr.left + rcSlicesScr.left, rcRowScr.bottom };
				ret = DrawToRect(pElement, pD3DTexture, rcElemScr, rcElem, pTextureNode);
			}

			// Draw the middle
			{
				RECT rcElem = { rcRow.left + rcSlices.left, rcRow.top, rcRow.right - rcSlices.right, rcRow.bottom };
				RECT rcElemScr = { rcRowScr.left + rcSlicesScr.left, rcRowScr.top, rcRowScr.right - rcSlicesScr.right, rcRowScr.bottom };
				ret = DrawToRect(pElement, pD3DTexture, rcElemScr, rcElem, pTextureNode);
			}

			// Draw the right if needed
			if (rcSlices.right != 0) {
				RECT rcElem = { rcRow.right - rcSlices.right, rcRow.top, rcRow.right, rcRow.bottom };
				RECT rcElemScr = { rcRowScr.right - rcSlicesScr.right, rcRowScr.top, rcRowScr.right, rcRowScr.bottom };
				ret = DrawToRect(pElement, pD3DTexture, rcElemScr, rcElem, pTextureNode);
			}
		}

		// Draw the middle row
		{
			// Adjust the texture coords for this row
			RECT rcRow = { rcTexture.left, rcTexture.top + rcSlices.top, rcTexture.right, rcTexture.bottom - rcSlices.bottom };
			RECT rcRowScr = { rcScreen.left, rcScreen.top + rcSlicesScr.top, rcScreen.right, rcScreen.bottom - rcSlicesScr.bottom };

			// Draw the left if needed
			if (rcSlices.left != 0) {
				RECT rcElem = { rcRow.left, rcRow.top, rcRow.left + rcSlices.left, rcRow.bottom };
				RECT rcElemScr = { rcRowScr.left, rcRowScr.top, rcRowScr.left + rcSlicesScr.left, rcRowScr.bottom };
				ret = DrawToRect(pElement, pD3DTexture, rcElemScr, rcElem, pTextureNode);
			}

			// Draw the middle
			{
				RECT rcElem = { rcRow.left + rcSlices.left, rcRow.top, rcRow.right - rcSlices.right, rcRow.bottom };
				RECT rcElemScr = { rcRowScr.left + rcSlicesScr.left, rcRowScr.top, rcRowScr.right - rcSlicesScr.right, rcRowScr.bottom };
				ret = DrawToRect(pElement, pD3DTexture, rcElemScr, rcElem, pTextureNode);
			}

			// Draw the right if needed
			if (rcSlices.right != 0) {
				RECT rcElem = { rcRow.right - rcSlices.right, rcRow.top, rcRow.right, rcRow.bottom };
				RECT rcElemScr = { rcRowScr.right - rcSlicesScr.right, rcRowScr.top, rcRowScr.right, rcRowScr.bottom };
				ret = DrawToRect(pElement, pD3DTexture, rcElemScr, rcElem, pTextureNode);
			}
		}

		// Draw the bottom row if needed
		if (rcSlices.bottom != 0) {
			// Adjust the texture coords for this row
			RECT rcRow = { rcTexture.left, rcTexture.bottom - rcSlices.bottom, rcTexture.right, rcTexture.bottom };
			RECT rcRowScr = { rcScreen.left, rcScreen.bottom - rcSlicesScr.bottom, rcScreen.right, rcScreen.bottom };

			// Draw the left if needed
			if (rcSlices.left != 0) {
				RECT rcElem = { rcRow.left, rcRow.top, rcRow.left + rcSlices.left, rcRow.bottom };
				RECT rcElemScr = { rcRowScr.left, rcRowScr.top, rcRowScr.left + rcSlicesScr.left, rcRowScr.bottom };
				ret = DrawToRect(pElement, pD3DTexture, rcElemScr, rcElem, pTextureNode);
			}

			// Draw the middle
			{
				RECT rcElem = { rcRow.left + rcSlices.left, rcRow.top, rcRow.right - rcSlices.right, rcRow.bottom };
				RECT rcElemScr = { rcRowScr.left + rcSlicesScr.left, rcRowScr.top, rcRowScr.right - rcSlicesScr.right, rcRowScr.bottom };
				ret = DrawToRect(pElement, pD3DTexture, rcElemScr, rcElem, pTextureNode);
			}

			// Draw the right if needed
			if (rcSlices.right != 0) {
				RECT rcElem = { rcRow.right - rcSlices.right, rcRow.top, rcRow.right, rcRow.bottom };
				RECT rcElemScr = { rcRowScr.right - rcSlicesScr.right, rcRowScr.top, rcRowScr.right, rcRowScr.bottom };
				ret = DrawToRect(pElement, pD3DTexture, rcElemScr, rcElem, pTextureNode);
			}
		}
	}

	return ret;
}

std::wstring FilterBadChars(const wchar_t* strText) {
	// List of characters that are known to crash ID3DXFont::DrawText function.
	// List is generated by scanning all 16-bit unicode characters with font "Verdana", height -14 and all styles.
	// More details: https://kaneva.atlassian.net/browse/ED-4459
	static std::set<WCHAR> BAD_UNICODE_CHARS = { 0x0300, 0x0301, 0x0303, 0x0309, 0x0323, 0x20f0 };
	static WCHAR BAD_CHAR_SUBSTITUTE = 0x0020; // space for now

	assert(strText != nullptr);
	std::wstring res(strText);
	for (wchar_t& ch : res) {
		if (ch >= 0x100) { // shortcut common characters
			if (BAD_UNICODE_CHARS.find(ch) != BAD_UNICODE_CHARS.end()) {
				ch = BAD_CHAR_SUBSTITUTE;
			}
		}
	}

	return res;
}

int CDXUTDialog::DrawText(const wchar_t* strText, CDXUTElement* pElement, RECT* prcDest, bool bShadow, int nCount) {
	ASSERT_GDRM(0);

	if (!strText || !pElement || !prcDest)
		return 0;

	// No need to draw fully transparent layers
	if (!(*(pElement->GetFontColor())).GetCurrentColor().a)
		return 0;

	std::wstring sTextFiltered = FilterBadChars(strText);

	RECT rcScreen = *prcDest;
	POINT myPos;
	GetLocation(myPos);
	OffsetRect(&rcScreen, myPos.x, myPos.y);

	// If caption is enabled, offset the Y position by its height.
	if (m_bCaption)
		OffsetRect(&rcScreen, 0, GetCaptionHeight());

	D3DXMATRIXA16 matTransform;
	D3DXMatrixIdentity(&matTransform);

	ID3DXSprite* sprite = pGDRM->GetSprite();
	if (!sprite)
		return 0;

	sprite->SetTransform(&matTransform);

	CDXUTFontNode* pFontNode = GetFont(pElement->GetFontIndex());
	if (!pFontNode || !pFontNode->pFont)
		return 0;

	RECT rcText = pElement->GetTextRect(&rcScreen);

	int ret_height = 0;
	if (bShadow) {
		RECT rcShadow = rcText;
		OffsetRect(&rcShadow, 1, 1);
		ret_height = pFontNode->pFont->DrawTextW(sprite, sTextFiltered.c_str(), nCount, &rcShadow, pElement->GetTextFormat() | DT_EXPANDTABS, D3DCOLOR_ARGB(DWORD((*(pElement->GetFontColor())).GetCurrentColor().a * 255), 0, 0, 0));
		if (ret_height == 0)
			return ret_height;
	}

	ret_height = pFontNode->pFont->DrawTextW(sprite, sTextFiltered.c_str(), nCount, &rcText, pElement->GetTextFormat(), (*(pElement->GetFontColor())).GetCurrentColor());

	return ret_height;
}

IMenuElement* CDXUTDialog::GetElement(std::string elementTag) {
	if (elementTag == DISPLAY_TAG)
		return m_pCapElement;
	else if (elementTag == BACKGROUND_DISPLAY_TAG)
		return m_pBodyElement;
	else if (elementTag == ICON_DISPLAY_TAG)
		return m_pIconElement;

	return 0;
}

unsigned int CDXUTDialog::GetElementTagUsesFont(std::string elementTag) {
	unsigned int result = 0;
	if (elementTag == DISPLAY_TAG)
		result = DXUT_STATE_FLAG_NORMAL;
	else if (elementTag == BACKGROUND_DISPLAY_TAG)
		result = DXUT_STATE_FLAG_NONE;
	else if (elementTag == ICON_DISPLAY_TAG)
		result = DXUT_STATE_FLAG_NONE;
	return result;
}

unsigned int CDXUTDialog::GetElementTagUsesTexture(std::string elementTag) {
	unsigned int result = 0;
	if (elementTag == DISPLAY_TAG)
		result = DXUT_STATE_FLAG_NORMAL;
	else if (elementTag == BACKGROUND_DISPLAY_TAG)
		result = DXUT_STATE_FLAG_NORMAL;
	else if (elementTag == ICON_DISPLAY_TAG)
		result = DXUT_STATE_FLAG_NORMAL;

	return result;
}

bool CDXUTDialog::SetBackgroundColors(D3DCOLOR colorTopLeft, D3DCOLOR colorTopRight, D3DCOLOR colorBottomLeft, D3DCOLOR colorBottomRight) {
	m_colorTopLeft = colorTopLeft;
	m_colorTopRight = colorTopRight;
	m_colorBottomLeft = colorBottomLeft;
	m_colorBottomRight = colorBottomRight;
	return true;
}

bool CDXUTDialog::SetNextDialog(IMenuDialog* pNextDialog) {
	if (pNextDialog == NULL)
		pNextDialog = this;
	m_pNextDialog = pNextDialog;
	m_pNextDialog->SetPrevDialog(this);
	return true;
}

bool CDXUTDialog::ClearStaticControlsForThisDialog() {
	// Clear Static Control Selected
	if (s_pControlSelected && (s_pControlSelected->GetDialog() == this))
		s_pControlSelected = NULL;

	// Clear Static Control In Focus
	if (s_pControlFocus && (s_pControlFocus->GetDialog() == this))
		s_pControlFocus = NULL;

	// Clear Static Control Pressed
	if (s_pControlPressed && (s_pControlPressed->GetDialog() == this))
		s_pControlPressed = NULL;

	// Clear Mouse Over Control
	m_pControlMouseOver = NULL;

	return true;
}

void CDXUTDialog::_ClearSelection() {
	if (s_pControlSelected) {
		s_pControlSelected->OnSelectionOff();
		s_pControlSelected = NULL;
	}
}

void CDXUTDialog::_ClearFocus() {
	if (s_pControlFocus) {
		s_pControlFocus->OnFocusOut();
		s_pControlFocus = NULL;
	}
}

bool CDXUTDialog::FocusDefaultControl() {
	// Check for default control in this dialog
	for (MenuControlIter iter = m_Controls.begin(); iter != m_Controls.end(); iter++) {
		IMenuControl* pControl = *iter;
		if (pControl->GetIsDefault()) {
			// Remove focus from the current control
			_ClearFocus();

			// Give focus to the default control
			s_pControlFocus = pControl;
			s_pControlFocus->OnFocusIn();
			return true;
		}
	}

	return false;
}

bool CDXUTDialog::OnCycleFocus(bool bForward) {
	// This should only be handled by the dialog which owns the focused control, and
	// only if a control currently has focus
	if (s_pControlFocus == NULL || s_pControlFocus->GetDialog() != this)
		return false;

	IMenuControl* pControl = s_pControlFocus;
	for (int i = 0; i < 0xffff; i++) {
		pControl = (bForward) ? _GetNextControl(pControl) : _GetPrevControl(pControl);

		// If we've gone in a full circle then focus doesn't change
		if (pControl == s_pControlFocus)
			return true;

		// If the dialog accepts keybord input and the control can have focus then
		// move focus
		if (pControl->GetDialog()->GetEnableKeyboardInput() && pControl->CanHaveFocus()) {
			s_pControlFocus->OnFocusOut();
			s_pControlFocus = pControl;
			s_pControlFocus->OnFocusIn();
			return true;
		}
	}

	// If we reached this point, the chain of dialogs didn't form a complete loop
	DXTRACE_ERR(_T("CDXUTDialog: Multiple dialogs are improperly chained together"), E_FAIL);

	return false;
}

bool CDXUTDialog::OnCycleSelection(bool bForward) {
	// This should only be handled by the dialog which owns the selected control, and
	// only if a control currently is selected, and the dialog is being edited
	if (s_pControlSelected == NULL || s_pControlSelected->GetDialog() != this || !m_bEdit)
		return false;

	IMenuControl* pControl = s_pControlSelected;
	pControl = (bForward) ? _GetNextControl(pControl) : _GetPrevControl(pControl);

	// If we've gone in a full circle then selection doesn't change
	if (pControl != s_pControlSelected)
		RequestSelection(pControl, true);

	return true;
}

bool CDXUTDialog::SetEnabled(bool enable) {
	int index = (int)m_Controls.size() - 1;
	while (index >= 0) {
		IMenuControl* pControl = m_Controls[index];
		if (pControl != NULL)
			pControl->SetEnabled(enable);
		index--;
	}
	return true;
}

bool CDXUTDialog::HitTest(int x, int y) const {
	if (m_bMinimized)
		return false;

	// Menu Controls Use Point Relative To Menu
	POINT myPos;
	GetLocation(myPos);
	POINT ptMenu = { x - myPos.x, y - myPos.y };

	// DRF - Added - Is Point Within Menu And Not Passthrough?
	RECT rectMenu = { 0, 0, GetWidth(), GetHeight() }; // menu size rectangle
	bool insideRectMenu = PointInsideRect(rectMenu, ptMenu);
	if (insideRectMenu && !GetPassthrough())
		return true;

	// Caption Enabled ? (never passthrough)
	if (GetCaptionEnabled()) {
		// Is Point Within Caption?
		RECT rectCaption = { 0, 0, GetWidth(), GetCaptionHeight() };
		bool insideRectCaption = PointInsideRect(rectCaption, ptMenu);
		if (insideRectCaption)
			return true;

		// When Caption Is Showing Controls Are Offset By Caption Height
		ptMenu.y -= GetCaptionHeight();
	}

	// Is Point Within Control ? (GetControlAtPoint() applies control passthroughs)
	bool insideControl = (GetControlAtPoint(ptMenu) != NULL);
	if (insideControl)
		return true;

	return false;
}

void CDXUTDialog::OnMouseEnter(CDXUTControl* pControl) {
	if (pControl == NULL)
		return;
}

void CDXUTDialog::OnMouseLeave(CDXUTControl* pControl) {
	if (pControl == NULL)
		return;
}

void CDXUTDialog::StartDraggingByCaption(const POINT& mousePoint) {
	SetIsDraggingByCaption(true);
	m_initialMousePointCaption = mousePoint;
	SetMenuCursorType(CURSOR_TYPE_DRAGGABLE);
}

bool CDXUTDialog::EndDraggingByCaption(const POINT& mousePoint) {
	if (!GetIsDraggingByCaption())
		return false;
	SetIsDraggingByCaption(false);
	SetMenuCursorType(CURSOR_TYPE_DRAGGABLE);
	return ((m_initialMousePointCaption.x != mousePoint.x) || (m_initialMousePointCaption.y != mousePoint.y));
}

void CDXUTDialog::CancelDraggingByCaption(const POINT& /*mousePoint*/) {
	SetIsDraggingByCaption(false);
	m_initialMousePointCaption.x = 0;
	m_initialMousePointCaption.y = 0;
	SetMenuCursorType(CURSOR_TYPE_DRAGGABLE);
}

void CDXUTDialog::HandleDraggingByCaption(const POINT& mousePoint) {
	if (GetIsDraggingByCaption()) {
		// Due to snapping math, we must include offsets before we set the new X/Y position values. They'll
		// have to be subtracted out prior to the actual SetLocationX/Y calls.
		POINT myPos;
		GetLocation(myPos);

		myPos.x += mousePoint.x - m_prevMousePointCaption.x;
		myPos.y += mousePoint.y - m_prevMousePointCaption.y;

		m_prevMousePointCaption = mousePoint;

		if (GetSnapToEdges()) {
			if (myPos.x < SNAP_THRESHOLD && myPos.x > -SNAP_THRESHOLD)
				myPos.x = 0;
			if (myPos.y < SNAP_THRESHOLD && myPos.y > -SNAP_THRESHOLD)
				myPos.y = 0;

			RECT rect = DXUTGetWindowClientRect();
			int width = GetWidth();
			int height = GetHeight();
			if ((myPos.x + width > rect.right - SNAP_THRESHOLD) && (myPos.x + width < rect.right + SNAP_THRESHOLD))
				myPos.x = rect.right - width;
			if ((myPos.y + height > rect.bottom - SNAP_THRESHOLD) && (myPos.y + height < rect.bottom + SNAP_THRESHOLD))
				myPos.y = rect.bottom - height;
		}

		// Set the new position and eliminate the offset from our previous calculations above.
		SetLocationX(myPos.x - GetOffsetX());
		SetLocationY(myPos.y - GetOffsetY());
	}
}

void CDXUTDialog::InitDefaultElements() {
	LoadDefaultElements();
}

void CDXUTDialog::InitDefaultElementsHelper() {
	SetFont(0, "Arial", 14, FW_NORMAL, false, false);

	CDXUTElement Element;
	RECT rcTexture;

	//-------------------------------------
	// Element for the caption
	//-------------------------------------
	m_pCapElement = new CDXUTElement();
	m_pCapElement->SetFont(0);
	SetRect(&rcTexture, 17, 269, 241, 287);
	m_pCapElement->SetTexture(0, &rcTexture);
	(*(m_pCapElement->GetTextureColor())).SetColor(DXUT_STATE_NORMAL, D3DCOLOR_ARGB(255, 255, 255, 255));
	(*(m_pCapElement->GetFontColor())).SetColor(DXUT_STATE_NORMAL, D3DCOLOR_ARGB(255, 255, 255, 255));
	m_pCapElement->SetFont(0, D3DCOLOR_ARGB(255, 255, 255, 255), DT_LEFT | DT_VCENTER);
	// Pre-blend as we don't need to transition the state
	(*(m_pCapElement->GetTextureColor())).Blend(DXUT_STATE_NORMAL, 10.0f);
	(*(m_pCapElement->GetFontColor())).Blend(DXUT_STATE_NORMAL, 10.0f);

	m_pBodyElement = new CDXUTElement();
	m_pBodyElement->SetFont(0);
	SetRect(&rcTexture, 17, 269, 241, 287);
	m_pBodyElement->SetTexture(0, &rcTexture);
	(*(m_pBodyElement->GetTextureColor())).SetColor(DXUT_STATE_NORMAL, D3DCOLOR_ARGB(255, 255, 255, 255));
	(*(m_pBodyElement->GetFontColor())).SetColor(DXUT_STATE_NORMAL, D3DCOLOR_ARGB(255, 255, 255, 255));
	m_pBodyElement->SetFont(0, D3DCOLOR_ARGB(255, 255, 255, 255), DT_LEFT | DT_VCENTER);
	// Pre-blend as we don't need to transition the state
	(*(m_pBodyElement->GetTextureColor())).Blend(DXUT_STATE_NORMAL, 10.0f);
	(*(m_pBodyElement->GetFontColor())).Blend(DXUT_STATE_NORMAL, 10.0f);

	m_pIconElement = new CDXUTElement();
	m_pIconElement->SetFont(0);
	SetRect(&rcTexture, 17, 269, 241, 287);
	m_pIconElement->SetTexture(0, &rcTexture);
	(*(m_pIconElement->GetTextureColor())).SetColor(DXUT_STATE_NORMAL, D3DCOLOR_ARGB(255, 255, 255, 255));
	(*(m_pIconElement->GetFontColor())).SetColor(DXUT_STATE_NORMAL, D3DCOLOR_ARGB(255, 255, 255, 255));
	m_pIconElement->SetFont(0, D3DCOLOR_ARGB(255, 255, 255, 255), DT_LEFT | DT_VCENTER);
	// Pre-blend as we don't need to transition the state
	(*(m_pIconElement->GetTextureColor())).Blend(DXUT_STATE_NORMAL, 10.0f);
	(*(m_pIconElement->GetFontColor())).Blend(DXUT_STATE_NORMAL, 10.0f);

	//-------------------------------------
	// CDXUTStatic
	//-------------------------------------
	Element.SetFont(0);
	(*(Element.GetFontColor())).SetColor(DXUT_STATE_DISABLED, D3DCOLOR_ARGB(200, 200, 200, 200));

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_STATIC, 0, &Element);

	//-------------------------------------
	// CDXUTImage
	//-------------------------------------
	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_IMAGE, 0, &Element);

	//-------------------------------------
	// CDXUTButton - Button
	//-------------------------------------
	SetRect(&rcTexture, 0, 0, 136, 54);
	Element.SetTexture(0, &rcTexture);
	Element.SetFont(0);
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_NORMAL, D3DCOLOR_ARGB(150, 255, 255, 255));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_PRESSED, D3DCOLOR_ARGB(200, 255, 255, 255));
	(*(Element.GetFontColor())).SetColor(DXUT_STATE_MOUSEOVER, D3DCOLOR_ARGB(255, 0, 0, 0));

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_BUTTON, 0, &Element);

	//-------------------------------------
	// CDXUTButton - Fill layer
	//-------------------------------------
	SetRect(&rcTexture, 136, 0, 272, 54);
	Element.SetTexture(0, &rcTexture, D3DCOLOR_ARGB(0, 255, 255, 255));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_MOUSEOVER, D3DCOLOR_ARGB(160, 255, 255, 255));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_PRESSED, D3DCOLOR_ARGB(60, 0, 0, 0));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_FOCUS, D3DCOLOR_ARGB(30, 255, 255, 255));

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_BUTTON, 1, &Element);

	//-------------------------------------
	// CDXUTCheckBox - Box
	//-------------------------------------
	SetRect(&rcTexture, 0, 54, 27, 81);
	Element.SetTexture(0, &rcTexture);
	Element.SetFont(0, D3DCOLOR_ARGB(255, 255, 255, 255), DT_LEFT | DT_VCENTER);
	(*(Element.GetFontColor())).SetColor(DXUT_STATE_DISABLED, D3DCOLOR_ARGB(200, 200, 200, 200));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_NORMAL, D3DCOLOR_ARGB(150, 255, 255, 255));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_FOCUS, D3DCOLOR_ARGB(200, 255, 255, 255));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_PRESSED, D3DCOLOR_ARGB(255, 255, 255, 255));

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_CHECKBOX, 0, &Element);

	//-------------------------------------
	// CDXUTCheckBox - Check
	//-------------------------------------
	SetRect(&rcTexture, 27, 54, 54, 81);
	Element.SetTexture(0, &rcTexture);

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_CHECKBOX, 1, &Element);

	//-------------------------------------
	// CDXUTRadioButton - Box
	//-------------------------------------
	SetRect(&rcTexture, 54, 54, 81, 81);
	Element.SetTexture(0, &rcTexture);
	Element.SetFont(0, D3DCOLOR_ARGB(255, 255, 255, 255), DT_LEFT | DT_VCENTER);
	(*(Element.GetFontColor())).SetColor(DXUT_STATE_DISABLED, D3DCOLOR_ARGB(200, 200, 200, 200));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_NORMAL, D3DCOLOR_ARGB(150, 255, 255, 255));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_FOCUS, D3DCOLOR_ARGB(200, 255, 255, 255));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_PRESSED, D3DCOLOR_ARGB(255, 255, 255, 255));

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_RADIOBUTTON, 0, &Element);

	//-------------------------------------
	// CDXUTRadioButton - Check
	//-------------------------------------
	SetRect(&rcTexture, 81, 54, 108, 81);
	Element.SetTexture(0, &rcTexture);

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_RADIOBUTTON, 1, &Element);

	//-------------------------------------
	// CDXUTComboBox - Main
	//-------------------------------------
	SetRect(&rcTexture, 7, 81, 247, 123);
	Element.SetTexture(0, &rcTexture);
	Element.SetFont(0);
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_NORMAL, D3DCOLOR_ARGB(150, 200, 200, 200));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_FOCUS, D3DCOLOR_ARGB(170, 230, 230, 230));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_DISABLED, D3DCOLOR_ARGB(70, 200, 200, 200));
	(*(Element.GetFontColor())).SetColor(DXUT_STATE_MOUSEOVER, D3DCOLOR_ARGB(255, 0, 0, 0));
	(*(Element.GetFontColor())).SetColor(DXUT_STATE_PRESSED, D3DCOLOR_ARGB(255, 0, 0, 0));
	(*(Element.GetFontColor())).SetColor(DXUT_STATE_DISABLED, D3DCOLOR_ARGB(200, 200, 200, 200));

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_COMBOBOX, 0, &Element);

	//-------------------------------------
	// CDXUTComboBox - Button
	//-------------------------------------
	SetRect(&rcTexture, 272, 0, 325, 49);
	Element.SetTexture(0, &rcTexture);
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_NORMAL, D3DCOLOR_ARGB(150, 255, 255, 255));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_PRESSED, D3DCOLOR_ARGB(255, 150, 150, 150));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_FOCUS, D3DCOLOR_ARGB(200, 255, 255, 255));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_DISABLED, D3DCOLOR_ARGB(70, 255, 255, 255));

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_COMBOBOX, 1, &Element);

	//-------------------------------------
	// CDXUTComboBox - Dropdown
	//-------------------------------------
	SetRect(&rcTexture, 7, 123, 241, 265);
	Element.SetTexture(0, &rcTexture);
	Element.SetFont(0, D3DCOLOR_ARGB(255, 0, 0, 0), DT_LEFT | DT_TOP);

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_COMBOBOX, 2, &Element);

	//-------------------------------------
	// CDXUTComboBox - Selection
	//-------------------------------------
	SetRect(&rcTexture, 7, 266, 241, 289);
	Element.SetTexture(0, &rcTexture);
	Element.SetFont(0, D3DCOLOR_ARGB(255, 255, 255, 255), DT_LEFT | DT_TOP);

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_COMBOBOX, 3, &Element);

	//-------------------------------------
	// CDXUTSlider - Track
	//-------------------------------------
	SetRect(&rcTexture, 1, 290, 280, 331);
	Element.SetTexture(0, &rcTexture);
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_NORMAL, D3DCOLOR_ARGB(150, 255, 255, 255));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_FOCUS, D3DCOLOR_ARGB(200, 255, 255, 255));
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_DISABLED, D3DCOLOR_ARGB(70, 255, 255, 255));

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_SLIDER, 0, &Element);

	//-------------------------------------
	// CDXUTSlider - Button
	//-------------------------------------
	SetRect(&rcTexture, 248, 55, 289, 96);
	Element.SetTexture(0, &rcTexture);

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_SLIDER, 1, &Element);

	//-------------------------------------
	// CDXUTScrollBar - Track
	//-------------------------------------
	SetRect(&rcTexture, 243, 144, 265, 155);
	Element.SetTexture(0, &rcTexture);
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_DISABLED, D3DCOLOR_ARGB(255, 200, 200, 200));

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_SCROLLBAR, 0, &Element);

	//-------------------------------------
	// CDXUTScrollBar - Up Arrow
	//-------------------------------------
	SetRect(&rcTexture, 243, 124, 265, 144);
	Element.SetTexture(0, &rcTexture);
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_DISABLED, D3DCOLOR_ARGB(255, 200, 200, 200));

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_SCROLLBAR, 1, &Element);

	//-------------------------------------
	// CDXUTScrollBar - Down Arrow
	//-------------------------------------
	SetRect(&rcTexture, 243, 155, 265, 176);
	Element.SetTexture(0, &rcTexture);
	(*(Element.GetTextureColor())).SetColor(DXUT_STATE_DISABLED, D3DCOLOR_ARGB(255, 200, 200, 200));

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_SCROLLBAR, 2, &Element);

	//-------------------------------------
	// CDXUTScrollBar - Button
	//-------------------------------------
	SetRect(&rcTexture, 266, 123, 286, 167);
	Element.SetTexture(0, &rcTexture);

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_SCROLLBAR, 3, &Element);

	//-------------------------------------
	// CDXUTEditBox
	//-------------------------------------
	// Element assignment:
	//   0 - text area
	//   1 - top left border
	//   2 - top border
	//   3 - top right border
	//   4 - left border
	//   5 - right border
	//   6 - lower left border
	//   7 - lower border
	//   8 - lower right border

	Element.SetFont(0, D3DCOLOR_ARGB(255, 0, 0, 0), DT_LEFT | DT_TOP);

	// Assign the style
	SetRect(&rcTexture, 14, 90, 241, 113);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_EDITBOX, 0, &Element);
	SetRect(&rcTexture, 8, 82, 14, 90);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_EDITBOX, 1, &Element);
	SetRect(&rcTexture, 14, 82, 241, 90);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_EDITBOX, 2, &Element);
	SetRect(&rcTexture, 241, 82, 246, 90);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_EDITBOX, 3, &Element);
	SetRect(&rcTexture, 8, 90, 14, 113);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_EDITBOX, 4, &Element);
	SetRect(&rcTexture, 241, 90, 246, 113);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_EDITBOX, 5, &Element);
	SetRect(&rcTexture, 8, 113, 14, 121);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_EDITBOX, 6, &Element);
	SetRect(&rcTexture, 14, 113, 241, 121);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_EDITBOX, 7, &Element);
	SetRect(&rcTexture, 241, 113, 246, 121);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_EDITBOX, 8, &Element);

	//-------------------------------------
	// CDXUTIMEEditBox
	//-------------------------------------

	Element.SetFont(0, D3DCOLOR_ARGB(255, 0, 0, 0), DT_LEFT | DT_TOP);

	// Assign the style
	SetRect(&rcTexture, 14, 90, 241, 113);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_IMEEDITBOX, 0, &Element);
	SetRect(&rcTexture, 8, 82, 14, 90);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_IMEEDITBOX, 1, &Element);
	SetRect(&rcTexture, 14, 82, 241, 90);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_IMEEDITBOX, 2, &Element);
	SetRect(&rcTexture, 241, 82, 246, 90);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_IMEEDITBOX, 3, &Element);
	SetRect(&rcTexture, 8, 90, 14, 113);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_IMEEDITBOX, 4, &Element);
	SetRect(&rcTexture, 241, 90, 246, 113);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_IMEEDITBOX, 5, &Element);
	SetRect(&rcTexture, 8, 113, 14, 121);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_IMEEDITBOX, 6, &Element);
	SetRect(&rcTexture, 14, 113, 241, 121);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_IMEEDITBOX, 7, &Element);
	SetRect(&rcTexture, 241, 113, 246, 121);
	Element.SetTexture(0, &rcTexture);
	SetDefaultElement(DXUT_CONTROL_IMEEDITBOX, 8, &Element);
	// Element 9 for IME text, and indicator button
	SetRect(&rcTexture, 0, 0, 136, 54);
	Element.SetTexture(0, &rcTexture);
	Element.SetFont(0, D3DCOLOR_ARGB(255, 0, 0, 0), DT_CENTER | DT_VCENTER);
	SetDefaultElement(DXUT_CONTROL_IMEEDITBOX, 9, &Element);

	//-------------------------------------
	// CDXUTSimpleEditBox
	//-------------------------------------
	// Element assignment:
	//   0 - text area

	Element.SetFont(0, D3DCOLOR_ARGB(255, 0, 0, 0), DT_LEFT | DT_TOP);

	// Assign the style
	SetRect(&rcTexture, 14, 90, 242, 114);
	Element.SetTexture(0, &rcTexture);
	SetRect(&rcTexture, 6, 6, 6, 6);
	Element.SetTextureSlices(rcTexture);
	SetDefaultElement(DXUT_CONTROL_SIMPLEEDITBOX, 0, &Element);

	//-------------------------------------
	// CDXUTListBox - Main
	//-------------------------------------

	SetRect(&rcTexture, 13, 124, 241, 265);
	Element.SetTexture(0, &rcTexture);
	Element.SetFont(0, D3DCOLOR_ARGB(255, 0, 0, 0), DT_LEFT | DT_TOP);

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_LISTBOX, 0, &Element);

	//-------------------------------------
	// CDXUTListBox - Selection
	//-------------------------------------

	SetRect(&rcTexture, 17, 269, 241, 287);
	Element.SetTexture(0, &rcTexture);
	Element.SetFont(0, D3DCOLOR_ARGB(255, 255, 255, 255), DT_LEFT | DT_TOP);

	// Assign the Element
	SetDefaultElement(DXUT_CONTROL_LISTBOX, 1, &Element);
}

bool CDXUTDialog::SetMenuFilePath(const std::string& menuFilePath) {
	m_menuFilePath = menuFilePath; // menu's full path
	m_menuFileName = StrFileNameExt(m_menuFilePath); // menu's 'name'
	return true;
}

bool CDXUTDialog::GetCursorLocation(POINT& pt) const {
	MsgProcInfo mpi;
	pt.x = mpi.CursorPoint().x - m_x;
	pt.y = mpi.CursorPoint().y - m_y;
	return true;
}

int CDXUTDialog::GetCursorLocationX() const {
	MsgProcInfo mpi;
	return mpi.CursorPoint().x - m_x;
}

int CDXUTDialog::GetCursorLocationY() const {
	MsgProcInfo mpi;
	return mpi.CursorPoint().y - m_y;
}

CDXUTFlashPlayer* CDXUTDialog::GetFlash(int ID) {
	return dynamic_cast<CDXUTFlashPlayer*>(GetControl(ID, DXUT_CONTROL_FLASH));
}

CDXUTFlashPlayer* CDXUTDialog::GetFlash(const char* strName) {
	return dynamic_cast<CDXUTFlashPlayer*>(GetControl(strName));
}

// CDXUTControl class
CDXUTControl::CDXUTControl(IMenuDialog* pDialog) {
	m_Type = DXUT_CONTROL_BUTTON;
	m_Tag = BUTTON_TAG;
	m_pDialog = pDialog;
	m_ID = 0;
	m_Name[0] = 0;
	m_pUserData = NULL;
	m_groupID = 0;

	m_defaultName = "";
	m_sLinkClicked = "";

	ZeroMemory(&m_comment, sizeof(m_comment));
	ZeroMemory(&m_tooltip, sizeof(m_tooltip));
	ZeroMemory(&m_xmlcss, sizeof(m_xmlcss));

	m_bEnabled = true;
	m_bVisible = true;
	m_bMouseOver = false;
	m_bIsInFocus = false;
	m_bIsDefault = false;

	m_pDialog = NULL;

	m_x = 0;
	m_y = 0;
	m_xOffset = 0;
	m_yOffset = 0;
	m_width = 0;
	m_height = 0;

	SetMovable(false);
	SetResizable(false);

	m_bIsSelected = false;

	m_nHotkey = 0;

	ZeroMemory(&m_rcBoundingBox, sizeof(m_rcBoundingBox));

	m_bToolTipShown = false;

	m_clipRect.left = m_clipRect.top = m_clipRect.right = m_clipRect.bottom = 0;
	m_clipRectSet = false;
	m_clipped = false;
	m_culled = false;
}

CDXUTControl::HTMLTag CDXUTControl::s_tags[] = {
	{ NULL, tNONE, 0, 0 },
	{ _T("b"), tB, 0, 0 },
	{ _T("br"), tBR, 0, 1 },
	{ _T("em"), tI, 0, 0 },
	{ _T("font"), tFONT, 1, 0 },
	{ _T("i"), tI, 0, 0 },
	{ _T("p"), tP, 0, 1 },
	{ _T("strong"), tB, 0, 0 },
	{ _T("sub"), tSUB, 0, 0 },
	{ _T("sup"), tSUP, 0, 0 },
	{ _T("u"), tU, 0, 0 },
	{ _T("a"), tA, 1, 0 },
	{ _T("t"), tT, 0, 1 }, // drf - added
	{ _T("ul"), tUL, 0, 0 }, // drf - added
	{ _T("li"), tLI, 0, 0 }, // drf - added
};

int CDXUTControl::m_stacktop = 0;

D3DCOLOR CDXUTControl::m_stack[STACKSIZE] = {
	D3DCOLOR_ARGB(255, 255, 255, 255),
	D3DCOLOR_ARGB(255, 255, 255, 255),
	D3DCOLOR_ARGB(255, 255, 255, 255),
	D3DCOLOR_ARGB(255, 255, 255, 255),
	D3DCOLOR_ARGB(255, 255, 255, 255),
	D3DCOLOR_ARGB(255, 255, 255, 255),
	D3DCOLOR_ARGB(255, 255, 255, 255),
	D3DCOLOR_ARGB(255, 255, 255, 255)
};

CDXUTControl::~CDXUTControl() {
	HideToolTip();

	for (ElementIterator itor = m_Elements.begin(); itor != m_Elements.end(); itor++) {
		delete itor->second;
	}
	m_Elements.clear();

	m_elementTags.clear();
}

bool CDXUTControl::Load(TiXmlNode* control_root) {
	bool bSuccess = false;

	if (control_root) {
		// given the control root, iterate over the children.
		TiXmlNode* child = 0;
		for (child = control_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, NAME_TAG)) {
				std::string sName;
				if (GetStringFromXMLNode(child, sName))
					SetName(sName.c_str());
			} else if (!_stricmp(val, LOCATION_TAG)) {
				LoadLocation(child);
			} else if (!_stricmp(val, SIZE_TAG)) {
				LoadSize(child);
			} else if (!_stricmp(val, ENABLED_TAG)) {
				bool bEnabled = false;
				if (GetBoolFromXMLNode(child, bEnabled))
					SetEnabled(bEnabled);
			} else if (!_stricmp(val, VISIBLE_TAG)) {
				bool bVisible = false;
				if (GetBoolFromXMLNode(child, bVisible))
					SetVisible(bVisible);
			} else if (!_stricmp(val, DEFAULT_TAG)) {
				bool bDefault = false;
				if (GetBoolFromXMLNode(child, bDefault))
					m_bIsDefault = bDefault;
			} else if (!_stricmp(val, HOTKEY_TAG)) {
				std::string sHotkey = "";
				if (GetStringFromXMLNode(child, sHotkey)) {
					SetHotkey(sHotkey.c_str()[0]);
				}
			} else if (!_stricmp(val, ELEMENTS_TAG)) {
				bSuccess = LoadElements(child);
			} else if (!_stricmp(val, COMMENT_TAG)) {
				std::string sComment = "";
				if (GetStringFromXMLNode(child, sComment))
					SetComment(sComment.c_str());

			} else if (!_stricmp(val, TOOLTIP_TAG)) {
				std::string sToolTip = "";
				if (GetStringFromXMLNode(child, sToolTip))
					SetToolTip(sToolTip.c_str());

			} else if (!_stricmp(val, XMLCSS_TAG)) {
				std::string sXMLCSS = "";
				if (GetStringFromXMLNode(child, sXMLCSS))
					SetXMLCSS(sXMLCSS.c_str());
			}
		}
	}

	return bSuccess;
}

bool CDXUTControl::Save(TiXmlNode* parent) {
	bool bSuccess = false;

	if (parent) {
		TiXmlNode* child;
		std::string sText;

		sText = GetName();
		if (!sText.empty()) {
			child = parent->InsertEndChild(TiXmlElement(NAME_TAG));
			if (child) {
				PutStringToXMLNode(child, sText);
			}
		}

		SaveLocation(parent);

		SaveSize(parent);

		child = parent->InsertEndChild(TiXmlElement(ENABLED_TAG));
		if (child) {
			PutBoolToXMLNode(child, GetEnabled());
		}

		child = parent->InsertEndChild(TiXmlElement(VISIBLE_TAG));
		if (child) {
			PutBoolToXMLNode(child, GetVisible());
		}

		sText = GetComment();
		if (!sText.empty()) {
			child = parent->InsertEndChild(TiXmlElement(COMMENT_TAG));
			if (child) {
				PutStringToXMLNode(child, sText);
			}
		}

		sText = GetToolTip();
		if (!sText.empty()) {
			child = parent->InsertEndChild(TiXmlElement(TOOLTIP_TAG));
			if (child) {
				PutStringToXMLNode(child, sText);
			}
		}

		if (m_bIsDefault) {
			child = parent->InsertEndChild(TiXmlElement(DEFAULT_TAG));
			if (child) {
				PutBoolToXMLNode(child, m_bIsDefault);
			}
		}

		if (GetHotkey()) {
			child = parent->InsertEndChild(TiXmlElement(HOTKEY_TAG));
			if (child) {
				char hotkey[2];
				sprintf(hotkey, "%c", GetHotkey());
				PutStringToXMLNode(child, hotkey);
			}
		}

		sText = GetXMLCSS();
		if (!sText.empty()) {
			child = parent->InsertEndChild(TiXmlElement(XMLCSS_TAG));
			if (child) {
				PutStringToXMLNode(child, sText);
			}
		}

		SaveElements(parent);
	}

	return bSuccess;
}

bool CDXUTControl::SaveLocation(TiXmlNode* parent) {
	bool bSuccess = false;

	if (parent) {
		TiXmlNode* location_root = parent->InsertEndChild(TiXmlElement(LOCATION_TAG));
		if (location_root) {
			POINT pt;
			GetLocation(pt, false);

			TiXmlNode* child = location_root->InsertEndChild(TiXmlElement(X_TAG));
			if (child) {
				PutIntToXMLNode(child, pt.x);
			}

			child = location_root->InsertEndChild(TiXmlElement(X_OFFSET_TAG));
			if (child) {
				PutIntToXMLNode(child, GetOffsetX());
			}

			child = location_root->InsertEndChild(TiXmlElement(Y_TAG));
			if (child) {
				PutIntToXMLNode(child, pt.y);
			}

			child = location_root->InsertEndChild(TiXmlElement(Y_OFFSET_TAG));
			if (child) {
				PutIntToXMLNode(child, GetOffsetY());
			}
		}
	}

	return bSuccess;
}

bool CDXUTControl::SaveSize(TiXmlNode* parent) {
	bool bSuccess = false;

	if (parent) {
		TiXmlNode* size_root = parent->InsertEndChild(TiXmlElement(SIZE_TAG));
		if (size_root) {
			TiXmlNode* child = size_root->InsertEndChild(TiXmlElement(WIDTH_TAG));
			if (child) {
				PutIntToXMLNode(child, GetWidth());
			}

			child = size_root->InsertEndChild(TiXmlElement(HEIGHT_TAG));
			if (child) {
				PutIntToXMLNode(child, GetHeight());
			}
		}
	}

	return bSuccess;
}

void CDXUTControl::HideToolTip() {
	// Hide the tooltip if we showed it
	if (m_bToolTipShown) {
		IDispatcher* dispatcher = KEPMenu::Instance()->GetEngineInterface()->GetDispatcher();
		if (dispatcher) {
			IEvent* e = dispatcher->MakeEvent(dispatcher->GetEventId("ShowToolTipEvent"));
			(*e->OutBuffer()) << ""
							  << "" << (double)0 << (double)0;
			dispatcher->QueueEvent(e);
		}
		m_bToolTipShown = false;
	}
}

bool CDXUTControl::SaveElements(TiXmlNode* parent) {
	bool bSuccess = false;

	if (parent) {
		TiXmlNode* elements_root = parent->InsertEndChild(TiXmlElement(ELEMENTS_TAG));
		if (elements_root) {
			for (ElementIterator itor = m_Elements.begin(); itor != m_Elements.end(); itor++) {
				std::string xmlTag = "";
				unsigned int iUsesFont = DXUT_STATE_FLAG_ALL;
				unsigned int iUsesTexture = DXUT_STATE_FLAG_ALL;
				for (ElementTagIterator tagItor = m_elementTags.begin(); tagItor != m_elementTags.end(); tagItor++) {
					if (tagItor->second.ElementIndex == itor->first) {
						xmlTag = tagItor->first;
						iUsesFont = tagItor->second.UsesFont;
						iUsesTexture = tagItor->second.UsesTexture;
						break;
					}
				}

				if (!xmlTag.empty()) {
					TiXmlNode* child = elements_root->InsertEndChild(TiXmlElement(xmlTag));
					if (child && m_pDialog) {
						itor->second->Save(child, m_pDialog, iUsesFont, iUsesTexture);
					}
				}
			}
		}
	}

	return bSuccess;
}

int CDXUTControl::DrawHTML(LPCTSTR strText, CDXUTElement* pElement, RECT* prcDest, int nCount) {
	CDXUTFontNode* pBaseFontNode = m_pDialog->GetFont(pElement->GetFontIndex());
	if (!pBaseFontNode || !pBaseFontNode->pFont) {
		assert(false);
		return 0;
	}

	if (strText == NULL) {
		assert(false);
		return 0;
	}

	LPCTSTR cur_string = strText;

	// array of ids for special fonts
	struct FontSpecial {
		int font_id;
		int spaceWidth;
		int lineHeight;
		FontSpecial() :
				font_id(-1), spaceWidth(0), lineHeight(0) {}
	};

	// Additional fonts created on-demand to accommodate HTML font style changes
	FontSpecial font_special[FV_NUMBER];

	int left, top, max_width;
	left = top = max_width = 0;
	int height = 0;
	BOOL white_space = FALSE;

	//Ankit ------ adding an int to account for multiple spaces together
	int noOfWhiteSpaces = 0;

	int xpos = 0;
	int min_width = 0;

	// DRF - Added Unordered List Support
	int ulPos = 0;
	bool bullet = false;

	// for parsing
	int tag, token_length;
	int prev_tag = 0; // DRF - potentially uninitialized variable

	int styles = 0;
	int cur_styles = -1; // force a select of the proper style

	SIZE size;
	RECT rc;

	if (nCount < 0)
		nCount = (int)_tcslen(strText);

	if (prcDest != NULL) {
		left = prcDest->left;
		top = prcDest->top;
		max_width = prcDest->right - prcDest->left;
	}

	if (max_width < 0)
		max_width = 0;

	DWORD uFormat = pElement->GetTextFormat();

	// toggle flags we do not support
	uFormat &= ~(DT_CENTER | DT_RIGHT | DT_TABSTOP);
	uFormat |= DT_LEFT;

	// get the base (default) font
	int base_font_id = pElement->GetFontIndex();

	// Set Base Font
	auto pCurrFontNode = pBaseFontNode;

	font_special[0].font_id = base_font_id;
	GetTextExtentPoint32(pCurrFontNode->pFont->GetDC(), _T("?y"), 2, &size);
	font_special[0].lineHeight = size.cy;
	GetTextExtentPoint32(pCurrFontNode->pFont->GetDC(), _T(" "), 1, &size);
	font_special[0].spaceWidth = size.cx;
	int spaceWidth = font_special[0].spaceWidth;
	int lineHeight = font_special[0].lineHeight;

	// init color stack
	CDXUTControl::m_stacktop = 0;

	// run through the string, word for word
	FOREVER {
		tag = CDXUTStatic::GetToken(&cur_string, &nCount, &token_length, &white_space, &noOfWhiteSpaces);
		if (tag < 0)
			break;

		const int tab_size = 10; // drf - added
		bool startTag = ((tag & ENDFLAG) == 0);
		bool endTag = !startTag;
		switch (tag & ~ENDFLAG) {
			case tP: {
				if (startTag && (uFormat & DT_SINGLELINE) == 0) {
					if (cur_string != strText)
						height += lineHeight;
					xpos = 0;
				}
			} break;

			case tBR: {
				if (startTag && (uFormat & DT_SINGLELINE) == 0) {
					height += lineHeight;
					xpos = 0;
				}
			} break;

			case tT: {
				xpos += (startTag ? tab_size : 0);
			} break;

			case tUL: {
				ulPos += (startTag ? 1 : -1);
				if (ulPos < 0)
					ulPos = 0;
				height += lineHeight;
				xpos = (ulPos * tab_size);
			} break;

			case tLI: {
				if (startTag) {
					height += lineHeight;
					xpos = (ulPos * tab_size);
					bullet = true;
				}
			} break;

			case tB: {
				styles = endTag ? (styles & ~FV_BOLD) : (styles | FV_BOLD);
			} break;

			case tI: {
				styles = endTag ? (styles & ~FV_ITALIC) : (styles | FV_ITALIC);
			} break;

			case tU: {
				styles = endTag ? (styles & ~FV_UNDERLINE) : (styles | FV_UNDERLINE);
			} break;

			case tSUB: {
				styles = endTag ? (styles & ~FV_SUBSCRIPT) : (styles | FV_SUBSCRIPT);
			} break;

			case tSUP: {
				styles = endTag ? (styles & ~FV_SUPERSCRIPT) : (styles | FV_SUPERSCRIPT);
			} break;

			case tFONT: {
				if (startTag) {
					std::basic_string<TCHAR> attrStr = cur_string;
					std::transform(attrStr.begin(), attrStr.end(), attrStr.begin(), ::tolower);
					size_t endPos = attrStr.find(_T(">"));
					if (endPos != std::string::npos) {
						attrStr = attrStr.substr(0, endPos);

						// Push Font Foreground Color Change ?
						size_t colorPos = attrStr.find(_T("color="));
						if (colorPos != std::string::npos) {
							D3DCOLOR color = CDXUTControl::ParseColor(cur_string + colorPos + 6);
							CDXUTControl::PushColor((*(pElement->GetFontColor())), color);
							(pElement->GetHTMLFontColorStack()).push(color);
						}

						// Push Font Background Color Change ?
						size_t colorBGPos = attrStr.find(_T("colorbg="));
						if (colorBGPos != std::string::npos) {
							D3DCOLOR color = CDXUTControl::ParseColor(cur_string + colorBGPos + 8);
							CDXUTControl::PushColor((*(pElement->GetFontColorBG())), color);
							(pElement->GetHTMLFontColorBGStack()).push(color);
						}
					}
				} else {
					// Pop Font Foreground Color Change ?
					if (!(pElement->GetHTMLFontColorStack()).empty()) {
						CDXUTControl::PopColor(*(pElement->GetFontColor()));
						(pElement->GetHTMLFontColorStack()).pop();
					}

					// Pop Font Background Color Change?
					if (!(pElement->GetHTMLFontColorBGStack()).empty()) {
						CDXUTControl::PopColor(*(pElement->GetFontColorBG()));
						(pElement->GetHTMLFontColorBGStack()).pop();
					}
				}
			} break;

			case tA: {
				if (startTag) {
					//grab the url from href=
					std::wstring sTemp;
					sTemp.append(cur_string);
					size_t i = sTemp.find(L"href=");
					size_t i2 = sTemp.find(L">");
					if (i != std::string::npos && i2 != std::string::npos) {
						//Start after href="
						i = i + 6;
						//length of link
						size_t len = i2 - 1 - i;
						m_sLinkClicked = Utf16ToUtf8(sTemp.substr(i, len));
					}
					if (white_space)
						xpos += spaceWidth;

					CDXUTControl::PushColor((*(pElement->GetFontColor())), CDXUTControl::ParseColor(L"#FF0000FF"));
				} else {
					CDXUTControl::PopColor(*(pElement->GetFontColor()));
				}
			} break;

			default: {
				if (tag == (tNONE | ENDFLAG))
					break;

				// Font Style Changed ?
				if (cur_styles != styles) {
					cur_styles = styles;

					// Update Font
					if (font_special[styles].font_id == -1) {
						int variant_font_id = CDXUTControl::GetFontVariant(base_font_id, styles);
						assert(variant_font_id != -1);
						if (variant_font_id != -1) {
							pCurrFontNode = m_pDialog->GetFont(variant_font_id);
							assert(pCurrFontNode != nullptr);
							font_special[styles].font_id = variant_font_id;
							GetTextExtentPoint32(pCurrFontNode->pFont->GetDC(), _T("?y"), 2, &size);
							font_special[styles].lineHeight = size.cy;
							GetTextExtentPoint32(pCurrFontNode->pFont->GetDC(), _T(" "), 1, &size);
							font_special[styles].spaceWidth = size.cx;
						}
					} else {
						pCurrFontNode = m_pDialog->GetFont(font_special[styles].font_id);
					}

					if (font_special[styles].font_id != -1) {
						// Use font by current styles
						spaceWidth = font_special[styles].spaceWidth;
						lineHeight = std::max(font_special[styles].lineHeight, lineHeight);
						pElement->SetFontIndex(font_special[styles].font_id);
					} else {
						// Fall back to base font
						spaceWidth = font_special[0].spaceWidth;
						lineHeight = std::max(font_special[0].lineHeight, lineHeight);
						pElement->SetFontIndex(font_special[0].font_id);
					}
				}

				// DRF - Extract Current Word
				std::wstring curWord = std::wstring((const wchar_t*)cur_string, (size_t)token_length);

				// DRF - ED-4027 - Convert HTML Special Characters (&amp; &lt; &gt; &#XXXX;)
				if (!curWord.empty() && curWord[0] == L'&') {
					if (curWord == L"&lt;")
						curWord = L"<";
					else if (curWord == L"&gt;")
						curWord = L">";
					else if (curWord == L"&amp;")
						curWord = L"&";
					else if (curWord.size() > 3 && curWord[1] == L'#') {
						int x = 0;
						sscanf(Utf16ToUtf8(curWord).c_str() + 2, "%x", &x);
						curWord[0] = (wchar_t)x;
						curWord = curWord.substr(0, 1);
					}
				}

				// check word length, check whether to wrap around
				GetTextExtentPoint32(pCurrFontNode->pFont->GetDC(), curWord.c_str(), curWord.size(), &size);

				//Ankit ----- updating the conditional to check for the number of whitespaces if any and incrementing the xpos by that many spaceWidth
				if (white_space)
					xpos += (spaceWidth * noOfWhiteSpaces);

				if (xpos + size.cx > max_width && white_space) {
					// Word Wrap ?
					if ((uFormat & DT_WORDBREAK) != 0) {
						height += lineHeight;
						xpos = 0;
					}
				}

				// output text (unless DT_CALCRECT is set)
				if ((uFormat & DT_CALCRECT) == 0) {
					// Build Rectangle From Text To End Of Line
					int xs = left + xpos;
					int xe = left + max_width;
					int ys = top + height;
					int ye = ys + lineHeight;
					SetRect(&rc, xs, ys, xe, ye);

					// if the text is within the original boundaries (for the bottom)
					if (rc.top <= prcDest->bottom) {
						// ensure we clip the text within the original boundaries (for the bottom)
						if (rc.bottom > prcDest->bottom)
							rc.bottom = prcDest->bottom;

						// DRF - Added - Draw Font Background Color ?
						if (!(pElement->GetHTMLFontColorBGStack()).empty()) {
							D3DCOLOR color = (pElement->GetHTMLFontColorBGStack()).top();

							// Build Rectangle Around Padded Text
							RECT rcText;
							int xsText = std::min(rc.left - (spaceWidth / 2), rc.right);
							int xeText = std::min(xsText + size.cx + spaceWidth, rc.right);
							int ysText = rc.top;
							int yeText = rc.top + lineHeight;
							SetRect(&rcText, xsText, ysText, xeText, yeText);

							m_pDialog->DrawRect(&rcText, color);
						}

						// DRF - Added - Draw Bullet ?
						if (bullet) {
							D3DCOLOR color = (*(pElement->GetFontColor())).GetCurrentColor();
							int ym = ys + lineHeight / 2;
							RECT rcBullet = { xs - 10, ym - 2, xs - 6, ym + 2 };
							m_pDialog->DrawRect(&rcBullet, color);
						}
						bullet = false;

						// Draw Font
						DWORD old_format = pElement->GetTextFormat();
						DWORD new_format = uFormat | ((styles & FV_SUBSCRIPT) ? DT_BOTTOM | DT_SINGLELINE : 0);
						pElement->SetTextFormat(new_format);
						m_pDialog->DrawText(curWord.c_str(), pElement, &rc, false, curWord.size());
						pElement->SetTextFormat(old_format);

						// check word length, check whether to wrap around
						std::basic_string<TCHAR> sTemp;
						sTemp.append(cur_string);
						size_t i = sTemp.find(_T("</a>"));
						i--;

						//recalc the width of the phrase in between <a> </a> tags
						if (i > 0) {
							SIZE sizeLink;
							GetTextExtentPoint32(pCurrFontNode->pFont->GetDC(), cur_string, (int)i, &sizeLink);
							rc.right = rc.left + sizeLink.cx;
						}

						if ((m_LastTouch.x != 0) && (m_LastTouch.y != 0) && ((prev_tag & ~ENDFLAG) == tA) && PointInsideRect(rc, m_LastTouch)) {
							m_LastTouch.x = 0;
							m_LastTouch.y = 0;

							//TODO send event to lua that a link was touched
							m_pDialog->SendEvent(EVENT_LINK_PRESSED, true, this);
						}
					}
				}

				// update current position
				xpos += size.cx;
				if (xpos > min_width)
					min_width = xpos;

				white_space = FALSE;
			} break;
		}
		prev_tag = tag;
		cur_string += token_length;
	}

	// Restore font index
	pElement->SetFontIndex(base_font_id);

	// store width and height back into the prcDest structure
	if ((uFormat & DT_CALCRECT) != 0 && prcDest != NULL) {
		prcDest->right = prcDest->left + min_width;
		prcDest->bottom = prcDest->top + height + lineHeight;
	}

	return height;
}

IMenuElement* CDXUTControl::GetElement(UINT iElement) {
	CDXUTElement* pElement = 0;

	ElementIterator itor = m_Elements.find(iElement);
	if (itor != m_Elements.end())
		pElement = itor->second;

	return (IMenuElement*)pElement;
}

IMenuElement* CDXUTControl::GetElement(std::string elementTag) {
	CDXUTElement* pElement = 0;

	ElementTagIterator iter = m_elementTags.find(elementTag);
	if (iter != m_elementTags.end())
		pElement = (CDXUTElement*)GetElement(iter->second.ElementIndex);

	return (IMenuElement*)pElement;
}

const char* CDXUTControl::GetElementTag(int index) {
	int num_tags = GetNumElementTags();
	if (index >= 0 && index < num_tags) {
		for (ElementTagIterator iter = m_elementTags.begin(); iter != m_elementTags.end(); iter++) {
			if (iter->second.ElementIndex == index)
				return iter->first.c_str();
		}
	}

	return 0;
}

unsigned int CDXUTControl::GetElementTagUsesFont(std::string elementTag) {
	unsigned int result = 0;
	ElementTagIterator it = m_elementTags.find(elementTag);
	if (it != m_elementTags.end())
		result = it->second.UsesFont;
	return result;
}

unsigned int CDXUTControl::GetElementTagUsesTexture(std::string elementTag) {
	unsigned int result = 0;
	ElementTagIterator it = m_elementTags.find(elementTag);
	if (it != m_elementTags.end())
		result = it->second.UsesTexture;
	return result;
}

CDXUTControl::ElementTagIterator CDXUTControl::FindElementTag(std::string tag) {
	return m_elementTags.find(tag.c_str());
}

int CDXUTControl::GetFontVariant(int base_font_id, int styles) {
	// Get Base Font
	auto pFontNode = m_pDialog->GetFont(base_font_id);
	if (!pFontNode) {
		LogError("GetFont() FAILED - baseFontId=" << base_font_id);
		return -1;
	}

	LONG height = pFontNode->nHeight;
	if (styles & (FV_SUPERSCRIPT | FV_SUBSCRIPT))
		height = height * 7 / 10;

	LONG weight = (styles & FV_BOLD) ? FW_BOLD : FW_NORMAL;
	bool italic = ((styles & FV_ITALIC) != 0);
	bool underline = ((styles & FV_UNDERLINE) != 0);

	int idx = m_pDialog->AddFont(pFontNode->strFace, height, weight, italic, underline);
	if (IDX_ERROR(idx)) {
		LogError("AddFont() FAILED - '" << pFontNode->strFace << "'"
										<< " height=" << height
										<< " weight=" << weight
										<< (italic ? " ITALIC" : "")
										<< (underline ? " UNDERLINE" : ""));
		return -1;
	}

	return idx;
}

bool CDXUTControl::LoadElements(TiXmlNode* elements_root) {
	bool bSuccess = false;

	if (elements_root) {
		TiXmlNode* child = 0;
		for (child = elements_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			ElementTagIterator iter = FindElementTag(val);
			if (iter != m_elementTags.end()) {
				CDXUTElement* pElement = (CDXUTElement*)GetElement(iter->second.ElementIndex);
				if (pElement)
					bSuccess = pElement->Load(child, m_pDialog, iter->second.UsesFont, iter->second.UsesTexture);
				else {
					CDXUTElement defaultElement;
					if (m_pDialog && defaultElement.Load(child, m_pDialog, iter->second.UsesFont, iter->second.UsesTexture))
						bSuccess = m_pDialog->SetDefaultElement(m_Type, iter->second.ElementIndex, &defaultElement) == S_OK;
				}
			}
		}
	}

	return bSuccess;
}

bool CDXUTControl::LoadLocation(TiXmlNode* location_root) {
	bool bSuccess = false;

	if (location_root) {
		int x = 0;
		int y = 0;

		TiXmlNode* child = location_root->FirstChild(X_TAG);

		if (child) {
			bSuccess = GetIntFromXMLNode(child, x);
		}

		if (bSuccess) {
			bSuccess = false;

			child = location_root->FirstChild(Y_TAG);
			if (child) {
				bSuccess = GetIntFromXMLNode(child, y);
			}
		}

		if (bSuccess)
			SetLocation(x, y);

		child = location_root->FirstChild(X_OFFSET_TAG);
		if (child) {
			int xOffset;
			if (GetIntFromXMLNode(child, xOffset)) {
				SetOffsetX(xOffset);
			}
		}

		child = location_root->FirstChild(Y_OFFSET_TAG);
		if (child) {
			int yOffset;
			if (GetIntFromXMLNode(child, yOffset)) {
				SetOffsetY(yOffset);
			}
		}
	}

	return bSuccess;
}

bool CDXUTControl::LoadSize(TiXmlNode* size_root) {
	bool bSuccess = false;

	if (size_root) {
		int width = 0;
		int height = 0;

		TiXmlNode* child = size_root->FirstChild(WIDTH_TAG);

		if (child) {
			if (GetIntFromXMLNode(child, width))
				bSuccess = true;
		}

		if (bSuccess) {
			bSuccess = false;

			child = size_root->FirstChild(HEIGHT_TAG);
			if (child) {
				if (GetIntFromXMLNode(child, height))
					bSuccess = true;
			}
		}

		if (bSuccess)
			SetSize(width, height);
	}

	return bSuccess;
}

bool CDXUTControl::HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam) {
	if (m_pDialog->GetEdit()) {
		static bool s_bDrag = false;
		static bool s_bResizing = false;
		static POINT s_prevMousePoint;
		static HCURSOR s_hCursor;

		switch (uMsg) {
			case WM_LBUTTONDBLCLK: {
				return true;
			}

			case WM_LBUTTONDOWN: {
				// Pressed while inside the control ?
				if (ContainsPoint(pt)) {
					SetMovable(true);
					SetResizable(true);

					if (!m_bIsSelected)
						m_pDialog->RequestSelection(this, true);

					s_prevMousePoint = pt;

					s_bDrag = true;
					SetCapture(DXUTGetHWND());

					// Added by YC for Undo support
					m_pDialog->SendEvent(EVENT_CONTROL_DRAGSTART, true, this, pt);
					return true;
				}

				break;
			}

			case WM_LBUTTONUP: {
				if (s_bDrag) {
					s_bDrag = false;

					// Added by YC for Undo support
					m_pDialog->SendEvent(EVENT_CONTROL_DRAGEND, true, this, pt);

					if (ContainsPoint(pt)) {
						ReleaseCapture();
						return true;
					}
				}

				break;
			}

			case WM_MOUSEMOVE: {
				if (GetMovable()) {
					if (s_bDrag) {
						POINT myPos;
						GetLocation(myPos, false);
						myPos.x += pt.x - s_prevMousePoint.x;
						myPos.y += pt.y - s_prevMousePoint.y;

						SetLocation(myPos.x, myPos.y);

						s_prevMousePoint = pt;

						OnMoved();

						return true;
					} else {
						// if the mouse is within the control area
						if (!s_bResizing && ContainsPoint(pt))
							return true;
					}
				}

				break;
			}
		};

		// If the control is allowed to be resized
		if (GetResizable()) {
			static POINT prevMousePoint;
			static CDXUTSelectionHandles::SelectionHandle selectionHandle;

			if (uMsg == WM_LBUTTONDOWN) {
				// determine which selection handle (if any) was selected

				POINT myPos;
				GetLocation(myPos);

				m_selectionHandles.SetSelectionHandles(myPos.x, myPos.y, GetWidth(), GetHeight());
				selectionHandle = m_selectionHandles.GetSelectionHandleAtPoint(pt);

				if (selectionHandle != CDXUTSelectionHandles::NOT_FOUND) {
					prevMousePoint = pt;

					s_bResizing = true;
					SetCapture(DXUTGetHWND());

					// Added by YC for Undo support
					m_pDialog->SendEvent(EVENT_CONTROL_DRAGSTART, true, this, pt);
					return true;
				}
			} else if (uMsg == WM_LBUTTONUP && s_bResizing) {
				s_bResizing = false;
				selectionHandle = CDXUTSelectionHandles::NOT_FOUND;

				ReleaseCapture();

				// Added by YC for Undo support
				m_pDialog->SendEvent(EVENT_CONTROL_DRAGEND, true, this, pt);
				return true;

			} else if (uMsg == WM_MOUSEMOVE) {
				if (s_bResizing) {
					POINT myPos;
					GetLocation(myPos, false);
					switch (selectionHandle) {
						case CDXUTSelectionHandles::TOP_LEFT:
							SetWidth(GetWidth() - (pt.x - prevMousePoint.x));
							SetHeight(GetHeight() - (pt.y - prevMousePoint.y));
							SetLocation(myPos.x + (pt.x - prevMousePoint.x), myPos.y + (pt.y - prevMousePoint.y));
							break;

						case CDXUTSelectionHandles::TOP_MIDDLE:
							SetHeight(GetHeight() - (pt.y - prevMousePoint.y));
							SetLocationY(myPos.y + (pt.y - prevMousePoint.y));
							break;

						case CDXUTSelectionHandles::TOP_RIGHT:
							SetWidth(GetWidth() + (pt.x - prevMousePoint.x));
							SetHeight(GetHeight() - (pt.y - prevMousePoint.y));
							SetLocationY(myPos.y + (pt.y - prevMousePoint.y));
							break;

						case CDXUTSelectionHandles::BOTTOM_LEFT:
							SetWidth(GetWidth() - (pt.x - prevMousePoint.x));
							SetHeight(GetHeight() + (pt.y - prevMousePoint.y));
							SetLocationX(myPos.x + (pt.x - prevMousePoint.x));
							break;

						case CDXUTSelectionHandles::BOTTOM_MIDDLE:
							SetHeight(GetHeight() + (pt.y - prevMousePoint.y));
							break;

						case CDXUTSelectionHandles::BOTTOM_RIGHT:
							SetWidth(GetWidth() + (pt.x - prevMousePoint.x));
							SetHeight(GetHeight() + (pt.y - prevMousePoint.y));
							break;

						case CDXUTSelectionHandles::LEFT_MIDDLE:
							SetWidth(GetWidth() - (pt.x - prevMousePoint.x));
							SetLocationX(myPos.x + (pt.x - prevMousePoint.x));
							break;

						case CDXUTSelectionHandles::RIGHT_MIDDLE:
							SetWidth(GetWidth() + (pt.x - prevMousePoint.x));
							break;
					}

					prevMousePoint = pt;
					UpdateRects();

					OnResized();
				} else {
					POINT myPos;
					GetLocation(myPos);

					m_selectionHandles.SetSelectionHandles(myPos.x, myPos.y, GetWidth(), GetHeight());
					selectionHandle = m_selectionHandles.GetSelectionHandleAtPoint(pt);
				}

				// this will prevent any other controls from receiving any
				// events -- while editing we don't want the controls to
				// behave as if the game was running
				return true;
			}
		}
	} else {
		// Dialog Not In Edit Mode
		// Handle Enabled & Visible Dialog Control Mouse Clicks
		bool bHandled = false;
		if (m_bEnabled && m_bVisible) {
			switch (uMsg) {
				case WM_LBUTTONDOWN:
					if (m_pDialog && ContainsPoint(pt))
						bHandled = m_pDialog->SendEvent(EVENT_CONTROL_LBUTTONDOWN, true, this, pt);
					HideToolTip();
					break;

				case WM_RBUTTONDOWN:
					if (m_pDialog && ContainsPoint(pt))
						bHandled = m_pDialog->SendEvent(EVENT_CONTROL_RBUTTONDOWN, true, this, pt);
					HideToolTip();
					break;

				case WM_LBUTTONDBLCLK:
					if (m_pDialog && ContainsPoint(pt))
						bHandled = m_pDialog->SendEvent(EVENT_CONTROL_LBUTTONDBLCLK, true, this, pt);
					HideToolTip();
					break;

				case WM_LBUTTONUP:
					if (m_pDialog && ContainsPoint(pt))
						bHandled = m_pDialog->SendEvent(EVENT_CONTROL_LBUTTONUP, true, this, pt);
					break;

				case WM_RBUTTONUP:
					if (m_pDialog && ContainsPoint(pt))
						bHandled = m_pDialog->SendEvent(EVENT_CONTROL_RBUTTONUP, true, this, pt);
					break;
			}
		}

		return bHandled;
	}

	return false;
}

void CDXUTControl::OnMouseEnter(POINT pt) {
	m_bMouseOver = true;

	if (m_pDialog)
		m_pDialog->SendEvent(EVENT_CONTROL_MOUSE_ENTER, true, this);

	if (m_tooltip[0] != _T('\0') && GetEnabled() && GetVisible()) { //
		IDispatcher* dispatcher = KEPMenu::Instance()->GetEngineInterface()->GetDispatcher();
		if (dispatcher) {
			IEvent* e = dispatcher->MakeEvent(dispatcher->GetEventId("ShowToolTipEvent"));
			POINT dialogPt = { 0, 0 };
			if (m_pDialog)
				m_pDialog->GetLocation(dialogPt);

			// Look for the magic string "-!-" to separate title from body of the tooltip
			char* pTitle = "";
			char* pBody = &(m_tooltip[0]);
			char* pDivider = strstr(m_tooltip, "-!-");
			if (pDivider) {
				pTitle = &(m_tooltip[0]);
				*pDivider = _T('\0');
				pBody = (pDivider + strlen("-!-"));
			}

			(*e->OutBuffer()) << pTitle << pBody << (double)(dialogPt.x + pt.x) << (double)(dialogPt.y + pt.y);
			dispatcher->QueueEvent(e);
			m_bToolTipShown = true;

			if (pDivider) {
				*pDivider = '-';
			}
		}
	}
}

void CDXUTControl::OnMouseLeave() {
	m_bMouseOver = false;

	if (m_pDialog)
		m_pDialog->SendEvent(EVENT_CONTROL_MOUSE_LEAVE, true, this);

	HideToolTip();
}

void CDXUTControl::OnFocusIn() {
	m_bIsInFocus = true;

	if (m_pDialog)
		m_pDialog->SendEvent(EVENT_CONTROL_FOCUS_IN, true, this);
}

void CDXUTControl::OnFocusOut() {
	m_bIsInFocus = FALSE;

	if (m_pDialog)
		m_pDialog->SendEvent(EVENT_CONTROL_FOCUS_OUT, true, this);

	HideToolTip();
}

void CDXUTControl::OnSelectionOn() {
	m_bIsSelected = true;
	if (m_pDialog)
		m_pDialog->SendEvent(EVENT_CONTROL_SELECTED, true, this);
}

void CDXUTControl::OnMoved() {
	POINT pt;
	GetLocation(pt);
	if (m_pDialog)
		m_pDialog->SendEvent(EVENT_CONTROL_MOVED, true, this, pt);
}

void CDXUTControl::OnResized() {
	POINT size = { GetWidth(), GetHeight() };
	if (m_pDialog)
		m_pDialog->SendEvent(EVENT_CONTROL_RESIZED, true, this, size);
}

int CDXUTControl::HexDigit(TCHAR ch) {
	if (ch >= _T('0') && ch <= _T('9'))
		return ch - _T('0');
	if (ch >= _T('A') && ch <= _T('F'))
		return ch - _T('A') + 10;
	if (ch >= _T('a') && ch <= _T('f'))
		return ch - _T('a') + 10;

	return 0;
}

D3DCOLOR CDXUTControl::ParseColor(LPCTSTR color_string) {
	int a, r, g, b;

	if (*color_string == _T('\'') || *color_string == _T('"'))
		color_string++;
	if (*color_string == _T('#'))
		color_string++;

	a = (HexDigit(color_string[0]) << 4) | HexDigit(color_string[1]);
	r = (HexDigit(color_string[2]) << 4) | HexDigit(color_string[3]);
	g = (HexDigit(color_string[4]) << 4) | HexDigit(color_string[5]);
	b = (HexDigit(color_string[6]) << 4) | HexDigit(color_string[7]);

	return D3DCOLOR_ARGB(a, r, g, b);
}

bool CDXUTControl::PushColor(IMenuBlendColor& cur_color, D3DCOLOR color) {
	if (m_stacktop < STACKSIZE)
		m_stack[m_stacktop++] = cur_color.GetCurrentColor();

	cur_color.SetCurrentColor(color);

	return true;
}

bool CDXUTControl::PopColor(IMenuBlendColor& cur_color) {
	bool okay = (m_stacktop > 0);

	cur_color.SetCurrentColor(okay ? m_stack[--m_stacktop] : m_stack[0]);

	return okay;
}

bool CDXUTControl::SetName(const char* strName) {
	if (strName && strlen(strName)) {
		strncpy(m_Name, strName, MAX_PATH);
		m_Name[MAX_PATH - 1] = _T('\0');
	} else {
		_snprintf(m_Name, MAX_PATH - 1, "%s%d", m_defaultName.c_str(), GetID());
		m_Name[MAX_PATH - 1] = _T('\0');
	}
	return true;
}

bool CDXUTControl::SetComment(const char* strText) {
	if (strText) {
		strncpy(m_comment, strText, MAX_COMMENT - 1);
		m_comment[MAX_COMMENT - 1] = _T('\0');
	} else {
		m_comment[0] = _T('\0');
	}
	return true;
}

bool CDXUTControl::SetToolTip(const char* strText) {
	if (strText) {
		strncpy(m_tooltip, strText, MAX_TOOLTIP - 1);
		m_tooltip[MAX_TOOLTIP - 1] = _T('\0');
	} else {
		m_tooltip[0] = _T('\0');
	}
	return true;
}

bool CDXUTControl::SetXMLCSS(const char* strText) {
	if (strText == NULL) {
		strncpy(m_xmlcss, strText, MAX_XMLCSS - 1);
		m_xmlcss[MAX_XMLCSS - 1] = _T('\0');
	} else {
		m_xmlcss[0] = _T('\0');
	}
	return true;
}

bool CDXUTControl::SetEnabled(bool bEnabled) {
	if (!bEnabled)
		HideToolTip();
	m_bEnabled = bEnabled;
	return true;
}

bool CDXUTControl::SetVisible(bool bVisible) {
	if (!bVisible)
		HideToolTip();
	m_bVisible = bVisible;
	return true;
}

bool CDXUTControl::SetTextColor(D3DCOLOR Color) {
	CDXUTElement* pElement = m_Elements[0];
	if (!pElement)
		return false;
	(*(pElement->GetFontColor())).SetColor(DXUT_STATE_NORMAL, Color);
	return true;
}

HRESULT CDXUTControl::SetElement(UINT iElement, CDXUTElement* pElement) {
	if (pElement == NULL)
		return E_INVALIDARG;

	if (m_Elements.find(iElement) == m_Elements.end()) {
		CDXUTElement* pNewElement = new CDXUTElement();
		if (pNewElement == NULL)
			return E_OUTOFMEMORY;

		m_Elements.insert(ElementMap::value_type(iElement, pNewElement));
	}

	// Update the data
	CDXUTElement* pCurElement = m_Elements[iElement];
	*pCurElement = *pElement;

	return S_OK;
}

void CDXUTControl::Refresh() {
	m_bMouseOver = false;
	m_bIsInFocus = false;

	for (ElementIterator itor = m_Elements.begin(); itor != m_Elements.end(); itor++) {
		itor->second->Refresh();
	}
}

void CDXUTControl::UpdateRects() {
	POINT myPos;
	GetLocation(myPos);
	SetRect(&m_rcBoundingBox, myPos.x, myPos.y, myPos.x + GetWidth(), myPos.y + GetHeight());
	UpdateClipping();
}

void CDXUTControl::UpdateClipping() {
	if (m_clipRectSet) {
		RECT overlapped;
		m_culled = !::IntersectRect(&overlapped, &m_rcBoundingBox, &m_clipRect); // If IntersectRect returns false, there is no intersection, hence the control is culled.
		m_clipped = !::EqualRect(&overlapped, &m_rcBoundingBox); // If intersection is the not same as the control rect, the control is either partly or completely clipped.
	} else {
		m_clipped = false;
		m_culled = false;
	}
}

void CDXUTControl::Render(float fElapsedTime) {
	if (m_pDialog->GetEdit() && m_bIsSelected) {
		POINT pt;
		m_pDialog->GetLocation(pt);

		POINT myPos;
		GetLocation(myPos);

		int x = myPos.x + pt.x;
		int y = myPos.y + pt.y;

		if (m_pDialog->GetCaptionEnabled()) {
			y += m_pDialog->GetCaptionHeight();
		}

		m_selectionHandles.Render(x, y, GetWidth(), GetHeight());
	}
}

BOOL CDXUTControl::ContainsPoint(POINT pt) {
	return (PointInsideRect(m_rcBoundingBox, pt) && ClipRectContainsPoint(pt)) ? TRUE : FALSE;
}

BOOL CDXUTControl::ClipRectContainsPoint(POINT pt) {
	return (!IsClipped() || PointInsideRect(m_clipRect, pt)) ? TRUE : FALSE;
}

bool CDXUTControl::GetCursorLocation(POINT& pt) const {
	MsgProcInfo mpi;
	pt.x = mpi.CursorPoint().x - m_x;
	pt.y = mpi.CursorPoint().y - m_y;
	return true;
}

int CDXUTControl::GetCursorLocationX() const {
	MsgProcInfo mpi;
	return mpi.CursorPoint().x - m_x;
}

int CDXUTControl::GetCursorLocationY() const {
	MsgProcInfo mpi;
	return mpi.CursorPoint().y - m_y;
}

bool CDXUTControl::SetClipRect(int left, int top, int right, int bottom) {
	if (right < left || bottom < top) {
		return false;
	}

	::SetRect(&m_clipRect, left, top, right, bottom);
	m_clipRectSet = true;
	UpdateClipping();
	return true;
}

CDXUTComment::CDXUTComment(IMenuDialog* pDialog) {
	m_Tag = COMMENT_TAG;
	m_pDialog = pDialog;

	m_Name[0] = _T('\0');
	m_comment = "";
	m_xml = "";
}

bool CDXUTComment::Load(TiXmlNode* root) {
	bool bSuccess = false;

	if (root) {
		std::string val = "";
		GetStringFromXMLNode(root, val);
		SetComment(val.c_str());
	}

	return bSuccess;
}

bool CDXUTComment::Save(TiXmlNode* parent) {
	bool bSuccess = false;

	if (parent) {
		std::string sText = GetComment();
		if (!sText.empty()) {
			PutStringToXMLNode(parent, sText);
		}
	}
	return bSuccess;
}

const char* CDXUTComment::GetName() {
	return GetComment();
}

bool CDXUTComment::SetName(const char* strName) {
	return SetComment(strName);
}

bool CDXUTComment::SetComment(const char* strText) {
	if (strText)
		m_comment = strText;
	else
		m_comment = "";
	return true;
}

CDXUTStatic::CDXUTStatic(CDXUTDialog* pDialog, bool bShadow) :
		CDXUTControl(pDialog),
		m_bShadow(bShadow),
		m_bUseHTML(false) {
	m_Type = DXUT_CONTROL_STATIC;
	m_Tag = STATIC_TAG;
	m_pDialog = pDialog;

	m_defaultName = STATIC_DEFAULT_NAME;
	m_strTextDefault = STATIC_DEFAULT_TEXT;

	SetText("");

	for (ElementIterator itor = m_Elements.begin(); itor != m_Elements.end(); itor++)
		SAFE_DELETE(itor->second);
	m_Elements.clear();

	m_elementTags.clear();
	m_elementTags[DISPLAY_TAG] = ElementTagMapValue(0, (DXUT_STATE_FLAG_NORMAL | DXUT_STATE_FLAG_DISABLED), DXUT_STATE_FLAG_NONE);

	//Ankit ----- setting up the word-wrapper object
	m_StaticWordWrapper.Setm_lastCachedShadowFlag(m_bShadow);
	m_StaticWordWrapper.Setm_lastCachedString(m_strTextUtf16.c_str());
	m_StaticWordWrapper.SetRecalcFlag(true);
}

bool CDXUTStatic::Load(TiXmlNode* static_root) {
	bool bSuccess = false;

	if (static_root) {
		// given the static root, iterate over the children.
		TiXmlNode* child = 0;
		for (child = static_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, TEXT_TAG)) {
				std::string sText = "";
				if (GetStringFromXMLNode(child, sText))
					SetText(sText.c_str());
			} else if (!_stricmp(val, SHADOW_TAG)) {
				bool bShadow = false;
				if (GetBoolFromXMLNode(child, bShadow))
					SetShadow(bShadow);
			} else if (!_stricmp(val, USE_HTML_TAG)) {
				bool bUseHTML = false;
				if (GetBoolFromXMLNode(child, bUseHTML))
					SetUseHTML(bUseHTML);
			}
		}

		bSuccess = CDXUTControl::Load(static_root);
	}

	return bSuccess;
}

bool CDXUTStatic::Save(TiXmlNode* static_root) {
	bool bSuccess = false;

	if (static_root) {
		TiXmlNode* child = static_root->InsertEndChild(TiXmlElement(TEXT_TAG));
		if (child) {
			std::string sText = GetText();
			if (sText != "") {
				PutStringToXMLNode(child, sText);
			}
		}

		child = static_root->InsertEndChild(TiXmlElement(SHADOW_TAG));
		if (child) {
			PutBoolToXMLNode(child, GetShadow());
		}

		child = static_root->InsertEndChild(TiXmlElement(USE_HTML_TAG));
		if (child) {
			PutBoolToXMLNode(child, GetUseHTML());
		}

		bSuccess = CDXUTControl::Save(static_root);
	}

	return bSuccess;
}

void CDXUTStatic::Render(float fElapsedTime) {
	DXUT_CONTROL_STATE iState = DXUT_STATE_NORMAL;

	// if editing, show control as 'normal'
	if (!GetVisible() && !m_pDialog->GetEdit())
		iState = DXUT_STATE_HIDDEN;
	else if (m_bEnabled == false)
		iState = DXUT_STATE_DISABLED;

	if (iState != DXUT_STATE_HIDDEN) {
		CDXUTElement* pElement = m_Elements[0];

		(*(pElement->GetFontColor())).Blend(iState, fElapsedTime);

		//Ankit ----- replacing the call to DrawText, if not using HTML, to route through word-wrapper's draw text
		if (m_bUseHTML)
			DrawText(m_strTextUtf16.c_str(), pElement, &m_rcBoundingBox, m_bShadow);
		else
			m_StaticWordWrapper.DrawWordWrappedText(m_pDialog, m_strTextUtf16.c_str(), pElement, &m_rcBoundingBox, m_bShadow);
	}

	CDXUTControl::Render(fElapsedTime);
}

bool CDXUTStatic::HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam) {
	if (!CDXUTControl::HandleMouse(uMsg, pt, wParam, lParam)) {
		if (!m_bEnabled || !GetVisible() || m_pDialog->GetEdit())
			return false;

		switch (uMsg) {
			case WM_LBUTTONDOWN:
			case WM_LBUTTONDBLCLK: {
				if (!ContainsPoint(pt))
					return false;
				m_LastTouch = pt;
				return true;
			}
			case WM_LBUTTONUP: {
				return true;
			}
		};

		return false;
	}

	return true;
}

HRESULT CDXUTStatic::GetTextCopy(char* strDest, UINT bufferCount) {
	// Validate incoming parameters
	if (strDest == NULL || bufferCount == 0) {
		return E_INVALIDARG;
	}

	// Copy the window text
	strncpy(strDest, m_strText.c_str(), bufferCount - 1);
	strDest[bufferCount - 1] = 0;

	return S_OK;
}

HRESULT CDXUTStatic::SetText(const char* strText) {
	// Valid Text ?
	if (strText == NULL || strText[0] == NULL) {
		m_strText = "";
		m_strTextOrig = "";
		m_strTextUtf16 = L"";
		return S_OK;
	}

	// Same Text ?
	if (strText == m_strTextOrig)
		return S_OK;

	m_strTextOrig = strText;

	// Translate Text ?
	if (!s_ttLang.empty())
		m_strText = TranslateTo(strText, s_ttLang);
	else
		m_strText = strText;

	// Make Utf16 Copy (for render performance)
	m_strTextUtf16 = Utf8ToUtf16(m_strText);

	//Ankit ----- text reset...change needToRecalcWordWrapFlag to true
	m_StaticWordWrapper.SetRecalcFlag(true);

	return S_OK;
}

HRESULT CDXUTStatic::SetTextWithDefault() {
	std::stringstream strm;
	strm << m_strTextDefault << GetID();
	return SetText(strm.str().c_str());
}

//Ankit ----- updating function signature to contain another parameter used to return the number of whitespace before the token
int CDXUTStatic::GetToken(LPCTSTR* String, int* Size, int* TokenLength, BOOL* WhiteSpace, int* noOfWhiteSpaces) {
	LPCTSTR Start, EndToken;
	int Length, EntryWhiteSpace, Index, IsEndTag;

	assert(String != NULL && *String != NULL);
	assert(Size != NULL);
	Start = *String;

	//Ankit ----- zeroing the count
	*noOfWhiteSpaces = 0;

	// check for leading white space, then skip it
	if (WhiteSpace != NULL) {
		EntryWhiteSpace = *WhiteSpace;
		*WhiteSpace = EntryWhiteSpace || _istspace(*Start);
	} else
		EntryWhiteSpace = FALSE;

	while (*Size > 0 && _istspace(*Start)) {
		Start++;
		*Size -= 1;
		//incrementing the no of white spaces found
		*noOfWhiteSpaces += 1;
	}

	if (*Size <= 0)
		return -1; // no printable text left

	EndToken = Start;
	Length = 0;
	IsEndTag = 0;

	if (*EndToken == _T('<')) {
		// might be a HTML tag, check
		EndToken++;
		Length++;

		if (Length < *Size && *EndToken == _T('/')) {
			IsEndTag = ENDFLAG;
			EndToken++;
			Length++;
		}

		while (Length < *Size && !_istspace(*EndToken) && *EndToken != _T('<') && *EndToken != _T('>')) {
			EndToken++;
			Length++;
		}

		for (Index = sizeof s_tags / sizeof s_tags[0] - 1; Index > 0; Index--) {
			if (!_tcsnicmp(Start + (IsEndTag ? 2 : 1), s_tags[Index].mnemonic, _tcslen(s_tags[Index].mnemonic)))
				break;
		}

		if (Index > 0) {
			// so it is a tag, see whether to accept parameters
			if (s_tags[Index].param && !IsEndTag) {
				while (Length < *Size && *EndToken != _T('<') && *EndToken != _T('>')) {
					EndToken++;
					Length++;
				}
			} else if (*EndToken != _T('>')) {
				// no parameters, then '>' must follow the tag
				Index = 0;
			}

			//Ankit ----- zeroing the number of white spaces found as the flag is changed to false
			if (WhiteSpace != NULL && s_tags[Index].block) {
				*WhiteSpace = FALSE;
				*noOfWhiteSpaces = 0;
			}
		}

		if (*EndToken == _T('>')) {
			EndToken++;
			Length++;
		}

		// skip trailing white space in some circumstances
		if (Index > 0 && (s_tags[Index].block || EntryWhiteSpace)) {
			while (Length < *Size && _istspace(*EndToken)) {
				EndToken++;
				Length++;
			}
		}
	} else {
		// normal word (no tag)
		Index = 0;
		while (Length < *Size && !_istspace(*EndToken) && *EndToken != _T('<')) {
			// DRF - ED-4027 - Break HTML Special Characters (&amp; &lt; &gt;)
			wchar_t c = *EndToken;
			if (Length && (c == L'&'))
				break;
			if (c == L';') {
				EndToken++;
				Length++;
				break;
			}

			EndToken++;
			Length++;
		}
	}

	if (TokenLength != NULL)
		*TokenLength = Length;

	*Size -= Length;
	*String = Start;
	return s_tags[Index].token | IsEndTag;
}

int CDXUTStatic::DrawText(const wchar_t* strText, CDXUTElement* pElement, RECT* prcDest, bool bShadow, int nCount) {
	if (m_bUseHTML)
		return DrawHTML(strText, pElement, prcDest, nCount);
	else
		return m_pDialog->DrawText(strText, pElement, prcDest, bShadow, nCount);
}

int CDXUTStatic::GetStringWidth(const char* strText) {
	int width = 0, height = 0;
	GetStringDims(strText, width, height);
	return width;
}

int CDXUTStatic::GetStringHeight(const char* strText) {
	int width = 0, height = 0;
	GetStringDims(strText, width, height);
	return height;
}

void CDXUTStatic::GetStringDims(const char* strText, int& width, int& height) {
	width = height = 0;

	ASSERT_GDRM();

	if (!strText)
		return;

	CDXUTElement* pElement = m_Elements[0];
	if (!pElement)
		return;

	std::wstring strTextW = Utf8ToUtf16(strText);
	RECT rcScreen = pElement->GetTextRect(&m_rcBoundingBox);
	POINT myPos;
	GetLocation(myPos);
	OffsetRect(&rcScreen, myPos.x, myPos.y);

	if (m_bUseHTML) {
		DWORD oldFormat = pElement->GetTextFormat();
		pElement->SetTextFormat(pElement->GetTextFormat() | DT_CALCRECT);
		CDXUTControl::DrawHTML(strTextW.c_str(), pElement, &rcScreen, -1);
		width = (rcScreen.right - rcScreen.left);
		height = (rcScreen.bottom - rcScreen.top);
		pElement->SetTextFormat(oldFormat);
	} else {
		D3DXMATRIXA16 matTransform;
		D3DXMatrixIdentity(&matTransform);
		ID3DXSprite* sprite = pGDRM->GetSprite();
		if (!sprite)
			return;

		sprite->SetTransform(&matTransform);

		CDXUTFontNode* pFontNode = m_pDialog->GetFont(pElement->GetFontIndex());
		if (pFontNode && pFontNode->pFont) {
			height = pFontNode->pFont->DrawText(sprite, strTextW.c_str(), -1, &rcScreen, pElement->GetTextFormat() | DT_CALCRECT, (*(pElement->GetFontColor())).GetCurrentColor());
			width = (rcScreen.right - rcScreen.left);
		}
	}
}

CDXUTImage::CDXUTImage(CDXUTDialog* pDialog) :
		CDXUTStatic(pDialog) {
	m_Type = DXUT_CONTROL_IMAGE;
	m_Tag = IMAGE_TAG;
	m_pDialog = pDialog;

	m_defaultName = IMAGE_DEFAULT_NAME;

	m_elementTags.clear();
	m_elementTags[DISPLAY_TAG] = ElementTagMapValue(0, DXUT_STATE_FLAG_NONE, (DXUT_STATE_FLAG_NORMAL | DXUT_STATE_FLAG_DISABLED));
}

void CDXUTImage::Render(float fElapsedTime) {
	DXUT_CONTROL_STATE iState = DXUT_STATE_NORMAL;

	// if editing, show control as 'normal'
	if (!GetVisible() && !m_pDialog->GetEdit()) {
		iState = DXUT_STATE_HIDDEN;
	} else if (m_bEnabled == false) {
		iState = DXUT_STATE_DISABLED;
	}

	if (iState != DXUT_STATE_HIDDEN) {
		CDXUTElement* pElement = m_Elements[0];

		RECT rcWindow = m_rcBoundingBox;

		// Blend current color
		(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime, 0.0f);

		m_pDialog->DrawSprite(pElement, &rcWindow);
	}

	CDXUTControl::Render(fElapsedTime);
}

HRESULT CDXUTImage::SetColor(D3DCOLOR textureColor) {
	CDXUTElement* pElement = m_Elements[0];
	if (pElement) {
		(*(pElement->GetTextureColor())).Init(textureColor);
	} else
		return S_FALSE;

	return S_OK;
}

CDXUTButton::CDXUTButton(CDXUTDialog* pDialog) :
		CDXUTStatic(pDialog) {
	m_Type = DXUT_CONTROL_BUTTON;
	m_Tag = BUTTON_TAG;
	m_pDialog = pDialog;

	m_defaultName = BUTTON_DEFAULT_NAME;
	m_strTextDefault = BUTTON_DEFAULT_TEXT;

	m_bPressed = false;
	m_nHotkey = 0;

	m_bSimpleMode = false;

	m_elementTags.clear();

	m_elementTags[NORMAL_DISPLAY_TAG] = ElementTagMapValue(0, DXUT_STATE_FLAG_NOT_HIDDEN, DXUT_STATE_FLAG_NOT_HIDDEN);
	m_elementTags[MOUSE_OVER_DISPLAY_TAG] = ElementTagMapValue(1, DXUT_STATE_FLAG_NOT_HIDDEN, DXUT_STATE_FLAG_NOT_HIDDEN);
	m_elementTags[FOCUSED_DISPLAY_TAG] = ElementTagMapValue(2, DXUT_STATE_FLAG_NOT_HIDDEN, DXUT_STATE_FLAG_NOT_HIDDEN);
	m_elementTags[PRESSED_DISPLAY_TAG] = ElementTagMapValue(3, DXUT_STATE_FLAG_NOT_HIDDEN, DXUT_STATE_FLAG_NOT_HIDDEN);
	m_elementTags[DISABLED_DISPLAY_TAG] = ElementTagMapValue(4, DXUT_STATE_FLAG_NOT_HIDDEN, DXUT_STATE_FLAG_NOT_HIDDEN);

	SetSimpleMode(m_bSimpleMode);
}

bool CDXUTButton::Load(TiXmlNode* button_root) {
	bool bSuccess = false;

	if (button_root) {
		bSuccess = CDXUTStatic::Load(button_root);
	}

	return bSuccess;
}

IMenuElement* CDXUTButton::GetElement(UINT id) {
	return CDXUTControl::GetElement(id);
}

CDXUTControl::ElementTagIterator CDXUTButton::FindElementTag(std::string tag) {
	bool bLegacy = false;

	if (DXUT_CONTROL_BUTTON == m_Type) {
		// legacy tags that need to be treated as new ones
		if (MAIN_DISPLAY_TAG == tag) {
			tag = MOUSE_OVER_DISPLAY_TAG;
			bLegacy = true;
		} else if (BACKGROUND_DISPLAY_TAG == tag) {
			tag = NORMAL_DISPLAY_TAG;
			bLegacy = true;
		}
	}

	if (bLegacy) {
		// legacy button only had two display elements -- clear out the others
		for (ElementTagIterator itor = m_elementTags.begin(); itor != m_elementTags.end(); itor++) {
			if (itor->first != MOUSE_OVER_DISPLAY_TAG && itor->first != NORMAL_DISPLAY_TAG) {
				CDXUTElement* pElement = m_Elements[itor->second.ElementIndex];
				if (pElement) {
					(*(pElement->GetFontColor())).Init(0, 0, 0);
					(*(pElement->GetTextureColor())).Init(0, 0, 0);
				}
			}
		}
	}

	return CDXUTControl::FindElementTag(tag);
}

bool CDXUTButton::HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if (!m_bEnabled || !GetVisible())
		return false;

	switch (uMsg) {
		case WM_KEYDOWN: {
			switch (wParam) {
				//				case VK_SPACE: // DRF - You gotta be fucking kidding me! SPACE == JUMP != BUTTON CLICK
				case VK_RETURN:
					m_bPressed = true;
					return true;
			}
		}

		case WM_KEYUP: {
			switch (wParam) {
				//				case VK_SPACE: // DRF - You gotta be fucking kidding me! SPACE == JUMP != BUTTON CLICK
				case VK_RETURN:
					if (m_bPressed == true) {
						m_bPressed = false;
						m_pDialog->SendEvent(EVENT_BUTTON_CLICKED, true, this);
					}
					return true;
			}
		}
	}
	return false;
}

bool CDXUTButton::HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam) {
	if (!CDXUTControl::HandleMouse(uMsg, pt, wParam, lParam)) {
		if (!m_bEnabled || !GetVisible())
			return false;

		switch (uMsg) {
			case WM_LBUTTONDOWN:
			case WM_LBUTTONDBLCLK: {
				if (ContainsPoint(pt)) {
					// Pressed while inside the control
					m_bPressed = true;
					SetCapture(DXUTGetHWND());

					if (!GetIsInFocus())
						m_pDialog->RequestFocus(this);

					return true;
				}

				break;
			}

			case WM_LBUTTONUP: {
				if (m_bPressed) {
					m_bPressed = false;
					ReleaseCapture();

					if (!m_pDialog->GetEnableKeyboardInput())
						m_pDialog->ClearFocus();

					// Button click
					if (ContainsPoint(pt)) {
						m_pDialog->SendEvent(EVENT_BUTTON_CLICKED, true, this);
					} else
						m_pDialog->SendEvent(EVENT_CONTROL_LBUTTONUP, true, this, pt);

					return true;
				}

				break;
			}
		};

		return false;
	}

	return true;
}

void CDXUTButton::Render(float fElapsedTime) {
	int nOffsetX = 0;
	int nOffsetY = 0;

	DXUT_CONTROL_STATE iState = DXUT_STATE_NORMAL;

	// if editing, show control as 'normal'
	if (!GetVisible() && !m_pDialog->GetEdit()) {
		iState = DXUT_STATE_HIDDEN;
	} else if (m_bEnabled == false) {
		iState = DXUT_STATE_DISABLED;
	} else if (m_bPressed) {
		iState = DXUT_STATE_PRESSED;

		nOffsetX = 1;
		nOffsetY = 2;
	} else if (GetMouseOver()) {
		iState = DXUT_STATE_MOUSEOVER;
	} else if (GetIsInFocus()) {
		iState = DXUT_STATE_FOCUS;
	}

	float fBlendRate = (iState == DXUT_STATE_PRESSED) ? 0.0f : 0.8f;
	RECT rcWindow = m_rcBoundingBox;
	OffsetRect(&rcWindow, nOffsetX, nOffsetY);

	if (iState != DXUT_STATE_HIDDEN) {
		for (ElementIterator itor = m_Elements.begin(); itor != m_Elements.end(); itor++) {
			CDXUTElement* pElement = itor->second;

			// Blend current color
			(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime, fBlendRate);
			(*(pElement->GetFontColor())).Blend(iState, fElapsedTime, fBlendRate);

			m_pDialog->DrawSprite(pElement, &rcWindow);
			DrawText(m_strTextUtf16.c_str(), pElement, &rcWindow);
		}
	}

	CDXUTControl::Render(fElapsedTime);
}

void CDXUTButton::SetSimpleMode(bool bSimpleMode) {
	m_bSimpleMode = bSimpleMode;

	if (m_bSimpleMode) {
		m_elementTags[NORMAL_DISPLAY_TAG] = ElementTagMapValue(0, DXUT_STATE_FLAG_NORMAL, DXUT_STATE_FLAG_NORMAL);
		m_elementTags[MOUSE_OVER_DISPLAY_TAG] = ElementTagMapValue(1, DXUT_STATE_FLAG_MOUSEOVER, DXUT_STATE_FLAG_MOUSEOVER);
		m_elementTags[FOCUSED_DISPLAY_TAG] = ElementTagMapValue(2, DXUT_STATE_FLAG_FOCUS, DXUT_STATE_FLAG_FOCUS);
		m_elementTags[PRESSED_DISPLAY_TAG] = ElementTagMapValue(3, DXUT_STATE_FLAG_PRESSED, DXUT_STATE_FLAG_PRESSED);
		m_elementTags[DISABLED_DISPLAY_TAG] = ElementTagMapValue(4, DXUT_STATE_FLAG_DISABLED, DXUT_STATE_FLAG_DISABLED);
	} else {
		m_elementTags[NORMAL_DISPLAY_TAG] = ElementTagMapValue(0, DXUT_STATE_FLAG_NOT_HIDDEN, DXUT_STATE_FLAG_NOT_HIDDEN);
		m_elementTags[MOUSE_OVER_DISPLAY_TAG] = ElementTagMapValue(1, DXUT_STATE_FLAG_NOT_HIDDEN, DXUT_STATE_FLAG_NOT_HIDDEN);
		m_elementTags[FOCUSED_DISPLAY_TAG] = ElementTagMapValue(2, DXUT_STATE_FLAG_NOT_HIDDEN, DXUT_STATE_FLAG_NOT_HIDDEN);
		m_elementTags[PRESSED_DISPLAY_TAG] = ElementTagMapValue(3, DXUT_STATE_FLAG_NOT_HIDDEN, DXUT_STATE_FLAG_NOT_HIDDEN);
		m_elementTags[DISABLED_DISPLAY_TAG] = ElementTagMapValue(4, DXUT_STATE_FLAG_NOT_HIDDEN, DXUT_STATE_FLAG_NOT_HIDDEN);
	}
}

CDXUTCheckBox::CDXUTCheckBox(CDXUTDialog* pDialog) :
		CDXUTButton(pDialog),
		m_bChecked(false),
		m_iIconWidth(-1) {
	m_Type = DXUT_CONTROL_CHECKBOX;
	m_Tag = CHECK_BOX_TAG;
	m_pDialog = pDialog;

	m_defaultName = CHECK_BOX_DEFAULT_NAME;
	m_strTextDefault = CHECK_BOX_DEFAULT_TEXT;

	m_elementTags.clear();
	m_elementTags[BOX_DISPLAY_TAG] = ElementTagMapValue(0, DXUT_STATE_FLAG_NOT_HIDDEN, DXUT_STATE_FLAG_NOT_HIDDEN);
	m_elementTags[CHECK_DISPLAY_TAG] = ElementTagMapValue(1, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NOT_HIDDEN);
}

bool CDXUTCheckBox::Load(TiXmlNode* check_box_root) {
	bool bSuccess = false;

	if (check_box_root) {
		// given the checkbox root, iterate over the children.
		TiXmlNode* child = 0;
		for (child = check_box_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, CHECKED_TAG)) {
				bool bChecked = false;
				if (GetBoolFromXMLNode(child, bChecked))
					SetChecked(bChecked);
			} else if (!_stricmp(val, ICONWIDTH_TAG)) {
				LONG iIconWidth = -1;
				if (GetLongFromXMLNode(child, iIconWidth))
					SetIconWidth(iIconWidth);
			}
		}

		bSuccess = CDXUTButton::Load(check_box_root);
	}

	return bSuccess;
}

bool CDXUTCheckBox::Save(TiXmlNode* check_box_root) {
	bool bSuccess = false;

	if (check_box_root) {
		TiXmlNode* child = check_box_root->InsertEndChild(TiXmlElement(CHECKED_TAG));
		if (child) {
			PutBoolToXMLNode(child, GetChecked());
		}

		// Only write the iconWidth if it's non-default.
		if (GetIconWidth() >= 0) {
			child = check_box_root->InsertEndChild(TiXmlElement(ICONWIDTH_TAG));
			if (child) {
				PutLongToXMLNode(child, GetIconWidth());
			}
		}

		bSuccess = CDXUTButton::Save(check_box_root);
	}

	return bSuccess;
}

bool CDXUTCheckBox::HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam) {
	return false;
}

bool CDXUTCheckBox::HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam) {
	if (!CDXUTControl::HandleMouse(uMsg, pt, wParam, lParam)) {
		if (!m_bEnabled || !GetVisible())
			return false;

		switch (uMsg) {
			case WM_LBUTTONDOWN:
			case WM_LBUTTONDBLCLK: {
				if (ContainsPoint(pt)) {
					// Pressed while inside the control
					m_bPressed = true;
					SetCapture(DXUTGetHWND());

					if (!GetIsInFocus() && m_pDialog->GetEnableKeyboardInput())
						m_pDialog->RequestFocus(this);

					return true;
				}

				break;
			}

			case WM_LBUTTONUP: {
				if (m_bPressed) {
					m_bPressed = false;
					ReleaseCapture();

					// Button click
					if (ContainsPoint(pt))
						SetCheckedInternal(!m_bChecked, true);

					return true;
				}

				break;
			}
		};

		return false;
	}

	return true;
}

void CDXUTCheckBox::SetCheckedInternal(bool bChecked, bool bFromInput) {
	m_bChecked = bChecked;

	m_pDialog->SendEvent(EVENT_CHECKBOX_CHANGED, bFromInput, this);
}

BOOL CDXUTCheckBox::ContainsPoint(POINT pt) {
	return ((PointInsideRect(m_rcBoundingBox, pt) || PointInsideRect(m_rcButton, pt)) && ClipRectContainsPoint(pt)) ? TRUE : FALSE;
}

void CDXUTCheckBox::UpdateRects() {
	CDXUTButton::UpdateRects();

	m_rcButton = m_rcBoundingBox;
	m_rcButton.right = m_rcButton.left;
	if (m_iIconWidth < 0) {
		m_rcButton.right += RectHeight(m_rcButton);
	} else {
		m_rcButton.right += m_iIconWidth;
	}

	m_rcText = m_rcBoundingBox;
	m_rcText.left += (int)(1.25f * RectWidth(m_rcButton));
}

void CDXUTCheckBox::Render(float fElapsedTime) {
	DXUT_CONTROL_STATE iState = DXUT_STATE_NORMAL;

	// if editing, show control as 'normal'
	if (!GetVisible() && !m_pDialog->GetEdit())
		iState = DXUT_STATE_HIDDEN;
	else if (m_bEnabled == false)
		iState = DXUT_STATE_DISABLED;
	else if (m_bPressed)
		iState = DXUT_STATE_PRESSED;
	else if (GetMouseOver())
		iState = DXUT_STATE_MOUSEOVER;
	else if (GetIsInFocus())
		iState = DXUT_STATE_FOCUS;

	if (iState != DXUT_STATE_HIDDEN) {
		//TODO: remove magic numbers
		CDXUTElement* pElement = m_Elements[0];

		float fBlendRate = (iState == DXUT_STATE_PRESSED) ? 0.0f : 0.8f;

		(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime, fBlendRate);
		(*(pElement->GetFontColor())).Blend(iState, fElapsedTime, fBlendRate);

		if (RectWidth(m_rcButton) > 0) {
			m_pDialog->DrawSprite(pElement, &m_rcButton);
		}

		DrawText(m_strTextUtf16.c_str(), pElement, &m_rcText, m_bShadow);

		if (!m_bChecked)
			iState = DXUT_STATE_HIDDEN;

		if (iState != DXUT_STATE_HIDDEN) {
			pElement = m_Elements[1];

			(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime, fBlendRate);
			if (RectWidth(m_rcButton) > 0) {
				m_pDialog->DrawSprite(pElement, &m_rcButton);
			}
		}
	}

	CDXUTControl::Render(fElapsedTime);
}

CDXUTRadioButton::CDXUTRadioButton(CDXUTDialog* pDialog) :
		CDXUTCheckBox(pDialog) {
	m_Type = DXUT_CONTROL_RADIOBUTTON;
	m_Tag = RADIO_BUTTON_TAG;
	m_pDialog = pDialog;

	m_defaultName = RADIO_BUTTON_DEFAULT_NAME;
	m_strTextDefault = RADIO_BUTTON_DEFAULT_TEXT;

	m_nButtonGroup = 0;
}

bool CDXUTRadioButton::Load(TiXmlNode* radio_button_root) {
	bool bSuccess = false;

	if (radio_button_root) {
		// given the radiobutton root, iterate over the children.
		TiXmlNode* child = 0;
		for (child = radio_button_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, GROUP_TAG)) {
				long group = -1;
				if (GetLongFromXMLNode(child, group))
					SetButtonGroup(group);
			}
		}

		bSuccess = CDXUTCheckBox::Load(radio_button_root);
	}

	return bSuccess;
}

bool CDXUTRadioButton::Save(TiXmlNode* radio_button_root) {
	bool bSuccess = false;

	if (radio_button_root) {
		TiXmlNode* child = radio_button_root->InsertEndChild(TiXmlElement(GROUP_TAG));
		if (child) {
			PutLongToXMLNode(child, GetButtonGroup());
		}

		bSuccess = CDXUTCheckBox::Save(radio_button_root);
	}

	return bSuccess;
}

bool CDXUTRadioButton::HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam) {
	return false;
}

bool CDXUTRadioButton::HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam) {
	if (!CDXUTControl::HandleMouse(uMsg, pt, wParam, lParam)) {
		if (!m_bEnabled || !GetVisible())
			return false;

		switch (uMsg) {
			case WM_LBUTTONDOWN:
			case WM_LBUTTONDBLCLK: {
				if (ContainsPoint(pt)) {
					// Pressed while inside the control
					m_bPressed = true;
					SetCapture(DXUTGetHWND());

					if (!GetIsInFocus() && m_pDialog->GetEnableKeyboardInput())
						m_pDialog->RequestFocus(this);

					return true;
				}

				break;
			}

			case WM_LBUTTONUP: {
				if (m_bPressed) {
					m_bPressed = false;
					ReleaseCapture();

					// Button click
					if (ContainsPoint(pt)) {
						m_pDialog->ClearRadioButtonGroup(m_nButtonGroup);
						m_bChecked = !m_bChecked;

						m_pDialog->SendEvent(EVENT_RADIOBUTTON_CHANGED, true, this);
					}

					return true;
				}

				break;
			}
		};

		return false;
	}

	return true;
}

void CDXUTRadioButton::SetCheckedInternal(bool bChecked, bool bClearGroup, bool bFromInput) {
	if (bChecked && bClearGroup)
		m_pDialog->ClearRadioButtonGroup(m_nButtonGroup);

	m_bChecked = bChecked;
	m_pDialog->SendEvent(EVENT_RADIOBUTTON_CHANGED, bFromInput, this);
}

CDXUTComboBox::CDXUTComboBox(CDXUTDialog* pDialog) :
		CDXUTButton(pDialog),
		m_ScrollBar(pDialog) {
	m_ScrollBar.SetParent(this);

	m_Type = DXUT_CONTROL_COMBOBOX;
	m_Tag = COMBO_BOX_TAG;
	m_pDialog = pDialog;

	m_defaultName = COMBO_BOX_DEFAULT_NAME;

	m_nDropHeight = 100;

	m_nSBWidth = 16;
	m_bOpened = false;
	m_iSelected = -1;
	m_iFocused = -1;

	m_elementTags.clear();
	m_elementTags[MAIN_DISPLAY_TAG] = ElementTagMapValue(0, DXUT_STATE_FLAG_NOT_HIDDEN, DXUT_STATE_FLAG_NOT_HIDDEN);
	m_elementTags[BUTTON_DISPLAY_TAG] = ElementTagMapValue(1, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NOT_HIDDEN);
	m_elementTags[DROP_DOWN_DISPLAY_TAG] = ElementTagMapValue(2, DXUT_STATE_FLAG_NORMAL, DXUT_STATE_FLAG_NORMAL);
	m_elementTags[SELECTION_DISPLAY_TAG] = ElementTagMapValue(3, DXUT_STATE_FLAG_NORMAL, DXUT_STATE_FLAG_NORMAL);
}

CDXUTComboBox::~CDXUTComboBox() {
	RemoveAllItems();
}

bool CDXUTComboBox::Load(TiXmlNode* combo_box_root) {
	bool bSuccess = false;

	if (combo_box_root) {
		// given the combobox root, iterate over the children.
		TiXmlNode* child = 0;
		for (child = combo_box_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, DROP_HEIGHT_TAG)) {
				int dropHeight = -1;
				if (GetIntFromXMLNode(child, dropHeight))
					SetDropHeight(dropHeight);
			} else if (!_stricmp(val, SCROLL_BAR_WIDTH_TAG)) {
				int sbWidth = -1;
				if (GetIntFromXMLNode(child, sbWidth))
					SetScrollBarWidth(sbWidth);
			} else if (!_stricmp(val, ITEMS_TAG)) {
				LoadItems(child);
			} else if (!_stricmp(val, SCROLL_BAR_TAG)) {
				m_ScrollBar.Load(child);
			}
		}

		bSuccess = CDXUTButton::Load(combo_box_root);
	}

	return bSuccess;
}

bool CDXUTComboBox::LoadItems(TiXmlNode* items_root) {
	bool bSuccess = false;

	if (items_root) {
		// given the items root, iterate over the children.
		TiXmlNode* child = 0;
		for (child = items_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, ITEM_TAG)) {
				bSuccess = LoadItem(child);
			}
		}
	}

	return bSuccess;
}

bool CDXUTComboBox::LoadItem(TiXmlNode* item_root) {
	bool bSuccess = false;

	if (item_root) {
		void* data = 0;
		std::string sText = "";

		// given the item root, iterate over the children.
		TiXmlNode* child = 0;
		for (child = item_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, TEXT_TAG)) {
				GetStringFromXMLNode(child, sText);
			}
		}

		if (!sText.empty()) {
			AddItem(sText.c_str(), data);
		}
	}

	return bSuccess;
}

bool CDXUTComboBox::Save(TiXmlNode* combo_box_root) {
	bool bSuccess = false;

	if (combo_box_root) {
		TiXmlNode* child = combo_box_root->InsertEndChild(TiXmlElement(DROP_HEIGHT_TAG));
		if (child) {
			PutIntToXMLNode(child, GetDropHeight());
		}

		child = combo_box_root->InsertEndChild(TiXmlElement(SCROLL_BAR_WIDTH_TAG));
		if (child) {
			PutIntToXMLNode(child, GetScrollBarWidth());
		}

		SaveItems(combo_box_root);

		child = combo_box_root->InsertEndChild(TiXmlElement(SCROLL_BAR_TAG));
		if (child) {
			m_ScrollBar.Save(child);
		}

		bSuccess = CDXUTButton::Save(combo_box_root);
	}

	return bSuccess;
}

bool CDXUTComboBox::SaveItems(TiXmlNode* combo_box_root) {
	bool bSuccess = false;

	if (combo_box_root) {
		TiXmlNode* items_root = combo_box_root->InsertEndChild(TiXmlElement(ITEMS_TAG));
		if (items_root) {
			for (int i = 0; i < m_Items.GetSize(); ++i) {
				DXUTComboBoxItem* pItem = m_Items.GetAt(i);
				SaveItem(items_root, pItem);
			}
		}
	}

	return bSuccess;
}

bool CDXUTComboBox::SaveItem(TiXmlNode* items_root, DXUTComboBoxItem* pItem) {
	bool bSuccess = false;

	if (items_root && pItem) {
		TiXmlNode* item_root = items_root->InsertEndChild(TiXmlElement(ITEM_TAG));
		if (item_root) {
			TiXmlNode* child = item_root->InsertEndChild(TiXmlElement(TEXT_TAG));
			if (child) {
				std::string sText = pItem->strText;
				PutStringToXMLNode(child, sText);
			}
		}
	}

	return bSuccess;
}

bool CDXUTComboBox::SetTextColor(D3DCOLOR Color) {
	CDXUTElement* pElement = m_Elements[0];

	if (pElement)
		(*(pElement->GetFontColor())).SetColor(DXUT_STATE_NORMAL, Color);

	pElement = m_Elements[2];

	if (pElement)
		(*(pElement->GetFontColor())).SetColor(DXUT_STATE_NORMAL, Color);

	return true;
}

void CDXUTComboBox::UpdateRects() {
	ASSERT_GDRM();

	CDXUTButton::UpdateRects();

	// if the scrollbar's thumb is not showing, then no scrolling
	// so don't show the scrollbar at all
	int nSBWidth = m_ScrollBar.GetShowThumb() ? m_nSBWidth : 0;

	m_rcButton = m_rcBoundingBox;
	m_rcButton.left = m_rcButton.right - RectHeight(m_rcButton);

	m_rcText = m_rcBoundingBox;
	m_rcText.right = m_rcButton.left;

	m_rcDropdown = m_rcText;
	OffsetRect(&m_rcDropdown, 0, (int)(0.90f * RectHeight(m_rcText)));
	m_rcDropdown.bottom += m_nDropHeight;
	m_rcDropdown.right -= nSBWidth;

	m_rcDropdownText = m_rcDropdown;
	m_rcDropdownText.left += (int)(0.1f * RectWidth(m_rcDropdown));
	m_rcDropdownText.right -= (int)(0.1f * RectWidth(m_rcDropdown));
	m_rcDropdownText.top += (int)(0.1f * RectHeight(m_rcDropdown));
	m_rcDropdownText.bottom -= (int)(0.1f * RectHeight(m_rcDropdown));

	// Update the scrollbar's rects
	m_ScrollBar.SetLocation(m_rcDropdown.right, m_rcDropdown.top + 2);
	m_ScrollBar.SetSize(nSBWidth, RectHeight(m_rcDropdown) - 2);

	CDXUTFontNode* pFontNode = pGDRM->GetFontNode(m_Elements[2]->GetFontIndex());
	if (pFontNode && pFontNode->nHeight) {
		m_ScrollBar.SetPageSize(RectHeight(m_rcDropdownText) / pFontNode->nHeight);

		// The selected item may have been scrolled off the page.
		// Ensure that it is in page again.
		m_ScrollBar.ShowItem(m_iSelected);
	}
}

void CDXUTComboBox::OnFocusOut() {
	CDXUTButton::OnFocusOut();
	m_bOpened = false;
}

bool CDXUTComboBox::HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam) {
	const DWORD REPEAT_MASK = (0x40000000);

	if (!m_bEnabled || !GetVisible())
		return false;

	// Let the scroll bar have a chance to handle it first
	if (m_ScrollBar.HandleKeyboard(uMsg, wParam, lParam))
		return true;

	switch (uMsg) {
		case WM_KEYDOWN: {
			switch (wParam) {
				case VK_RETURN:
					if (m_bOpened) {
						if (m_iSelected != m_iFocused) {
							m_iSelected = m_iFocused;
							m_pDialog->SendEvent(EVENT_COMBOBOX_SELECTION_CHANGED, true, this);
						}
						m_bOpened = false;

						if (!m_pDialog->GetEnableKeyboardInput())
							m_pDialog->ClearFocus();

						return true;
					}
					break;

				case VK_F4:
					// Filter out auto-repeats
					if (lParam & REPEAT_MASK)
						return true;

					m_bOpened = !m_bOpened;

					if (!m_bOpened) {
						m_pDialog->SendEvent(EVENT_COMBOBOX_SELECTION_CHANGED, true, this);

						if (!m_pDialog->GetEnableKeyboardInput())
							m_pDialog->ClearFocus();
					}

					return true;

				case VK_LEFT:
				case VK_UP:
					if (m_iFocused > 0) {
						m_iFocused--;
						m_iSelected = m_iFocused;

						if (!m_bOpened)
							m_pDialog->SendEvent(EVENT_COMBOBOX_SELECTION_CHANGED, true, this);
					}

					return true;

				case VK_RIGHT:
				case VK_DOWN:
					if (m_iFocused + 1 < (int)GetNumItems()) {
						m_iFocused++;
						m_iSelected = m_iFocused;

						if (!m_bOpened)
							m_pDialog->SendEvent(EVENT_COMBOBOX_SELECTION_CHANGED, true, this);
					}

					return true;
			}
			break;
		}
	}

	return false;
}

bool CDXUTComboBox::HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam) {
	if (!CDXUTControl::HandleMouse(uMsg, pt, wParam, lParam)) {
		if (!m_bEnabled || !GetVisible())
			return false;

		// Let the scroll bar handle it first.
		if (m_ScrollBar.HandleMouse(uMsg, pt, wParam, lParam))
			return true;

		switch (uMsg) {
			case WM_MOUSEMOVE: {
				if (m_bOpened && PointInsideRect(m_rcDropdown, pt)) {
					// Determine which item has been selected
					for (int i = 0; i < m_Items.GetSize(); i++) {
						DXUTComboBoxItem* pItem = m_Items.GetAt(i);
						if (pItem->bVisible && PointInsideRect(pItem->rcActive, pt))
							m_iFocused = i;
					}
					return true;
				}
				break;
			}

			case WM_LBUTTONDOWN:
			case WM_LBUTTONDBLCLK: {
				if (ContainsPoint(pt)) {
					// Pressed while inside the control
					m_bPressed = true;
					SetCapture(DXUTGetHWND());

					if (!GetIsInFocus())
						m_pDialog->RequestFocus(this);

					// Toggle dropdown
					if (GetIsInFocus()) {
						m_bOpened = !m_bOpened;

						if (!m_bOpened) {
							if (!m_pDialog->GetEnableKeyboardInput())
								m_pDialog->ClearFocus();
						}
					}

					return true;
				}

				// Perhaps this click is within the dropdown
				if (m_bOpened && PointInsideRect(m_rcDropdown, pt)) {
					// Determine which item has been selected
					for (int i = m_ScrollBar.GetTrackPos(); i < m_Items.GetSize(); i++) {
						DXUTComboBoxItem* pItem = m_Items.GetAt(i);
						if (pItem->bVisible && PointInsideRect(pItem->rcActive, pt)) {
							m_iFocused = m_iSelected = i;
							m_pDialog->SendEvent(EVENT_COMBOBOX_SELECTION_CHANGED, true, this);
							m_bOpened = false;

							if (!m_pDialog->GetEnableKeyboardInput())
								m_pDialog->ClearFocus();

							break;
						}
					}

					return true;
				}

				// Mouse click not on main control or in dropdown, fire an event if needed
				if (m_bOpened) {
					m_iFocused = m_iSelected;

					m_pDialog->SendEvent(EVENT_COMBOBOX_SELECTION_CHANGED, true, this);
					m_bOpened = false;
				}

				// Make sure the control is no longer in a pressed state
				m_bPressed = false;

				// Release focus if appropriate
				if (!m_pDialog->GetEnableKeyboardInput()) {
					m_pDialog->ClearFocus();
				}

				break;
			}

			case WM_LBUTTONUP: {
				if (m_bPressed && ContainsPoint(pt)) {
					// Button click
					m_bPressed = false;
					ReleaseCapture();
					return true;
				}

				break;
			}

			case WM_MOUSEWHEEL: {
				int zDelta = (short)HIWORD(wParam) / WHEEL_DELTA;
				if (m_bOpened) {
					UINT uLines;
					SystemParametersInfo(SPI_GETWHEELSCROLLLINES, 0, &uLines, 0);
					m_ScrollBar.Scroll(-zDelta * uLines);
				} else {
					if (zDelta > 0) {
						if (m_iFocused > 0) {
							m_iFocused--;
							m_iSelected = m_iFocused;

							if (!m_bOpened)
								m_pDialog->SendEvent(EVENT_COMBOBOX_SELECTION_CHANGED, true, this);
						}
					} else {
						if (m_iFocused + 1 < (int)GetNumItems()) {
							m_iFocused++;
							m_iSelected = m_iFocused;

							if (!m_bOpened)
								m_pDialog->SendEvent(EVENT_COMBOBOX_SELECTION_CHANGED, true, this);
						}
					}
				}
				return true;
			}
		};

		return false;
	}

	return true;
}

void CDXUTComboBox::OnHotkey() {
	if (m_bOpened)
		return;

	if (m_iSelected == -1)
		return;

	m_iSelected++;

	if (m_iSelected >= (int)m_Items.GetSize())
		m_iSelected = 0;

	m_iFocused = m_iSelected;
	m_pDialog->SendEvent(EVENT_COMBOBOX_SELECTION_CHANGED, true, this);
}

void CDXUTComboBox::Render(float fElapsedTime) {
	DXUT_CONTROL_STATE iState = DXUT_STATE_NORMAL;

	if (!m_bOpened)
		iState = DXUT_STATE_HIDDEN;

	if (iState != DXUT_STATE_HIDDEN) {
		// Dropdown box
		CDXUTElement* pElement = m_Elements[2];
		CDXUTFontNode* pFont = m_pDialog->GetFont(pElement->GetFontIndex());
		LONG rowHeight = abs(pFont->nHeight);

		// If we have not initialized the scroll bar page size,
		// do that now.
		static bool bSBInit;
		if (!bSBInit) {
			// Update the page size of the scroll bar
			if (rowHeight == 0)
				rowHeight = 1;

			m_ScrollBar.SetPageSize(RectHeight(m_rcDropdownText) / rowHeight);
			bSBInit = true;
		}

		// Scroll bar
		// if the scrollbar's thumb is not showing, then no scrolling
		// so don't show the scrollbar at all
		if (m_bOpened && m_ScrollBar.GetShowThumb())
			m_ScrollBar.Render(fElapsedTime);

		// Blend current color
		(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime);
		(*(pElement->GetFontColor())).Blend(iState, fElapsedTime);

		m_pDialog->DrawSprite(pElement, &m_rcDropdown);

		// Selection outline
		CDXUTElement* pSelectionElement = m_Elements[3];
		(*(pSelectionElement->GetTextureColor())).SetCurrentColor((*(pSelectionElement->GetTextureColor())).GetCurrentColor());
		(*(pSelectionElement->GetFontColor())).SetCurrentColor((*(pSelectionElement->GetFontColor())).GetColor(DXUT_STATE_NORMAL));

		int curY = m_rcDropdownText.top;
		int nRemainingHeight = RectHeight(m_rcDropdownText);

		for (int i = m_ScrollBar.GetTrackPos(); i < m_Items.GetSize(); i++) {
			DXUTComboBoxItem* pItem = m_Items.GetAt(i);

			// Make sure there's room left in the dropdown
			nRemainingHeight -= rowHeight;
			if (nRemainingHeight < 0) {
				pItem->bVisible = false;
				continue;
			}

			SetRect(&pItem->rcActive, m_rcDropdownText.left, curY, m_rcDropdownText.right, curY + rowHeight);
			curY += rowHeight;

			pItem->bVisible = true;

			if (m_bOpened) {
				if ((int)i == m_iFocused) {
					RECT rc;
					SetRect(&rc, m_rcDropdown.left, pItem->rcActive.top - 2, m_rcDropdown.right, pItem->rcActive.bottom + 2);
					m_pDialog->DrawSprite(pSelectionElement, &rc);
					//Ankit ----- replacing the draw text to instead route through the word-wrapper object if not m_bUseHTML
					if (m_bUseHTML)
						DrawText(Utf8ToUtf16(pItem->strText).c_str(), pSelectionElement, &pItem->rcActive);
					else
						(pItem->m_ComboBoxItemWordWrapper).DrawWordWrappedText(m_pDialog, Utf8ToUtf16(pItem->strText).c_str(), pElement, &pItem->rcActive);
				} else {
					//Ankit ----- replacing the draw text to instead route through the word-wrapper object if not m_bUseHTML
					if (m_bUseHTML)
						DrawText(Utf8ToUtf16(pItem->strText).c_str(), pElement, &pItem->rcActive);
					else
						(pItem->m_ComboBoxItemWordWrapper).DrawWordWrappedText(m_pDialog, Utf8ToUtf16(pItem->strText).c_str(), pElement, &pItem->rcActive);
				}
			}
		}
	}

	int nOffsetX = 0;
	int nOffsetY = 0;

	iState = DXUT_STATE_NORMAL;

	// if editing, show control as 'normal'
	if (!GetVisible() && !m_pDialog->GetEdit())
		iState = DXUT_STATE_HIDDEN;
	else if (m_bEnabled == false)
		iState = DXUT_STATE_DISABLED;
	else if (m_bPressed) {
		iState = DXUT_STATE_PRESSED;

		nOffsetX = 1;
		nOffsetY = 2;
	} else if (GetMouseOver()) {
		iState = DXUT_STATE_MOUSEOVER;

		nOffsetX = -1;
		nOffsetY = -2;
	} else if (GetIsInFocus())
		iState = DXUT_STATE_FOCUS;

	if (iState != DXUT_STATE_HIDDEN) {
		float fBlendRate = (iState == DXUT_STATE_PRESSED) ? 0.0f : 0.8f;

		// Button
		CDXUTElement* pElement = m_Elements[1];

		// Blend current color
		(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime, fBlendRate);

		RECT rcWindow = m_rcButton;
		OffsetRect(&rcWindow, nOffsetX, nOffsetY);
		m_pDialog->DrawSprite(pElement, &rcWindow);

		if (m_bOpened)
			iState = DXUT_STATE_PRESSED;

		// Main text box
		//TODO: remove magic numbers
		pElement = m_Elements[0];

		// Blend current color
		(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime, fBlendRate);
		(*(pElement->GetFontColor())).Blend(iState, fElapsedTime, fBlendRate);

		m_pDialog->DrawSprite(pElement, &m_rcText);

		if (m_iSelected >= 0 && m_iSelected < (int)m_Items.GetSize()) {
			DXUTComboBoxItem* pItem = m_Items.GetAt(m_iSelected);
			if (pItem != NULL) {
				//Ankit ----- replacing the draw text to instead route through the word-wrapper object if not m_bUseHTML
				if (m_bUseHTML)
					DrawText(Utf8ToUtf16(pItem->strText).c_str(), pElement, &m_rcText);
				else
					(pItem->m_ComboBoxItemWordWrapper).DrawWordWrappedText(m_pDialog, Utf8ToUtf16(pItem->strText).c_str(), pElement, &m_rcText);
			}
		}
	}

	CDXUTControl::Render(fElapsedTime);
}

HRESULT CDXUTComboBox::AddItem(const char* strText, void* pData) {
	// Validate parameters
	if (strText == NULL) {
		return E_INVALIDARG;
	}

	// Create a new item and set the data
	DXUTComboBoxItem* pItem = new DXUTComboBoxItem;
	if (pItem == NULL) {
		return DXTRACE_ERR_MSGBOX(_T("new"), E_OUTOFMEMORY);
	}

	ZeroMemory(pItem, sizeof(DXUTComboBoxItem));
	strncpy(pItem->strText, strText, 256 - 1);
	pItem->strText[256 - 1] = _T('\0');
	pItem->pData = pData;

	m_Items.Add(pItem);

	// Update the scroll bar with new range
	m_ScrollBar.SetTrackRange(0, m_Items.GetSize());

	// If this is the only item in the list, it's selected
	if (GetNumItems() == 1) {
		m_iSelected = 0;
		m_iFocused = 0;
		m_pDialog->SendEvent(EVENT_COMBOBOX_SELECTION_CHANGED, false, this);
	}

	//Ankit ----- set up the word wrapper object
	(pItem->m_ComboBoxItemWordWrapper).SetRecalcFlag(true);

	return S_OK;
}

void CDXUTComboBox::RemoveItem(UINT index) {
	DXUTComboBoxItem* pItem = m_Items.GetAt(index);
	SAFE_DELETE(pItem);
	m_Items.Remove(index);
	m_ScrollBar.SetTrackRange(0, m_Items.GetSize());
	if (m_iSelected >= m_Items.GetSize())
		m_iSelected = m_Items.GetSize() - 1;
}

void CDXUTComboBox::RemoveAllItems() {
	for (int i = 0; i < m_Items.GetSize(); i++) {
		DXUTComboBoxItem* pItem = m_Items.GetAt(i);
		SAFE_DELETE(pItem);
	}

	m_Items.RemoveAll();
	m_ScrollBar.SetTrackRange(0, 1);
	m_iFocused = m_iSelected = -1;
}

bool CDXUTComboBox::ContainsItem(const char* strText, UINT iStart) {
	return (-1 != FindItem(strText, iStart));
}

int CDXUTComboBox::FindItem(const char* strText, UINT iStart) {
	if (strText == NULL)
		return -1;

	for (int i = iStart; i < m_Items.GetSize(); i++) {
		DXUTComboBoxItem* pItem = m_Items.GetAt(i);

		if (0 == strcmp(pItem->strText, strText)) {
			return i;
		}
	}

	return -1;
}

void* CDXUTComboBox::GetSelectedData() {
	if (m_iSelected < 0)
		return NULL;

	DXUTComboBoxItem* pItem = m_Items.GetAt(m_iSelected);
	return pItem->pData;
}

const char* CDXUTComboBox::GetSelectedText() {
	if (m_iSelected < 0)
		return NULL;

	DXUTComboBoxItem* pItem = m_Items.GetAt(m_iSelected);
	return pItem->strText;
}

DXUTComboBoxItem* CDXUTComboBox::GetSelectedItem() {
	if (m_iSelected < 0)
		return NULL;

	return m_Items.GetAt(m_iSelected);
}

const char* CDXUTComboBox::GetItemText(UINT nIndex) {
	DXUTComboBoxItem* pItem = GetItem(nIndex);
	if (pItem) {
		return pItem->strText;
	}

	return NULL;
}

void* CDXUTComboBox::GetItemData(const char* strText) {
	int index = FindItem(strText);
	if (index == -1) {
		return NULL;
	}

	DXUTComboBoxItem* pItem = m_Items.GetAt(index);
	if (pItem == NULL) {
		DXTRACE_ERR(_T("CGrowableArray::GetAt"), E_FAIL);
		return NULL;
	}

	return pItem->pData;
}

void* CDXUTComboBox::GetItemData(UINT index) {
	if (index >= GetNumItems()) {
		DXTRACE_ERR(_T("CGrowableArray::GetAt"), E_FAIL);
		return NULL;
	}

	DXUTComboBoxItem* pItem = m_Items.GetAt(index);
	if (pItem == NULL) {
		DXTRACE_ERR(_T("CGrowableArray::GetAt"), E_FAIL);
		return NULL;
	}

	return pItem->pData;
}

HRESULT CDXUTComboBox::SetSelectedByIndex(UINT index) {
	if (index >= GetNumItems())
		return E_INVALIDARG;

	m_iFocused = m_iSelected = index;
	m_pDialog->SendEvent(EVENT_COMBOBOX_SELECTION_CHANGED, false, this);

	return S_OK;
}

HRESULT CDXUTComboBox::SetSelectedByText(const char* strText) {
	if (strText == NULL)
		return E_INVALIDARG;

	int index = FindItem(strText);
	if (index == -1)
		return E_FAIL;

	m_iFocused = m_iSelected = index;
	m_pDialog->SendEvent(EVENT_COMBOBOX_SELECTION_CHANGED, false, this);

	return S_OK;
}

HRESULT CDXUTComboBox::SetSelectedByData(void* pData) {
	for (int i = 0; i < m_Items.GetSize(); i++) {
		DXUTComboBoxItem* pItem = m_Items.GetAt(i);

		if (pItem->pData == pData) {
			m_iFocused = m_iSelected = i;
			m_pDialog->SendEvent(EVENT_COMBOBOX_SELECTION_CHANGED, false, this);
			return S_OK;
		}
	}

	return E_FAIL;
}

CDXUTSlider::CDXUTSlider(CDXUTDialog* pDialog) :
		CDXUTControl(pDialog) {
	m_Type = DXUT_CONTROL_SLIDER;
	m_Tag = SLIDER_TAG;
	m_pDialog = pDialog;

	m_defaultName = SLIDER_DEFAULT_NAME;

	m_nMin = 0;
	m_nMax = 100;
	m_nValue = 50;

	m_bPressed = false;

	m_elementTags.clear();
	m_elementTags[TRACK_DISPLAY_TAG] = ElementTagMapValue(0, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NOT_HIDDEN);
	m_elementTags[BUTTON_DISPLAY_TAG] = ElementTagMapValue(1, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NOT_HIDDEN);
}

bool CDXUTSlider::Load(TiXmlNode* slider_root) {
	bool bSuccess = false;

	if (slider_root) {
		int min = GetRangeMin();
		int max = GetRangeMax();
		int value = GetValue();

		// given the slider root, iterate over the children.
		TiXmlNode* child = 0;
		for (child = slider_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, MIN_TAG)) {
				GetIntFromXMLNode(child, min);
			} else if (!_stricmp(val, MAX_TAG)) {
				GetIntFromXMLNode(child, max);
			} else if (!_stricmp(val, VALUE_TAG)) {
				GetIntFromXMLNode(child, value);
			}
		}

		SetRange(min, max);
		SetValue(value);

		bSuccess = CDXUTControl::Load(slider_root);
	}

	return bSuccess;
}

bool CDXUTSlider::Save(TiXmlNode* slider_root) {
	bool bSuccess = false;

	if (slider_root) {
		TiXmlNode* child = slider_root->InsertEndChild(TiXmlElement(MIN_TAG));
		if (child) {
			PutIntToXMLNode(child, GetRangeMin());
		}

		child = slider_root->InsertEndChild(TiXmlElement(MAX_TAG));
		if (child) {
			PutIntToXMLNode(child, GetRangeMax());
		}

		child = slider_root->InsertEndChild(TiXmlElement(VALUE_TAG));
		if (child) {
			PutIntToXMLNode(child, GetValue());
		}

		bSuccess = CDXUTControl::Save(slider_root);
	}

	return bSuccess;
}

BOOL CDXUTSlider::ContainsPoint(POINT pt) {
	return ((PointInsideRect(m_rcBoundingBox, pt) || PointInsideRect(m_rcButton, pt)) && ClipRectContainsPoint(pt)) ? TRUE : FALSE;
}

void CDXUTSlider::UpdateRects() {
	CDXUTControl::UpdateRects();

	m_rcButton = m_rcBoundingBox;
	m_rcButton.right = m_rcButton.left + RectHeight(m_rcButton);
	OffsetRect(&m_rcButton, -RectWidth(m_rcButton) / 2, 0);

	m_nButtonX = (int)((m_nValue - m_nMin) * (float)RectWidth(m_rcBoundingBox) / (m_nMax - m_nMin));
	OffsetRect(&m_rcButton, m_nButtonX, 0);
}

int CDXUTSlider::ValueFromPos(int x) {
	float fValuePerPixel = (float)(m_nMax - m_nMin) / RectWidth(m_rcBoundingBox);
	return (int)(0.5f + m_nMin + fValuePerPixel * (x - m_rcBoundingBox.left));
}

bool CDXUTSlider::HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if (!m_bEnabled || !GetVisible())
		return false;

	switch (uMsg) {
		case WM_KEYDOWN: {
			switch (wParam) {
				case VK_HOME:
					SetValueInternal(m_nMin, true);
					return true;

				case VK_END:
					SetValueInternal(m_nMax, true);
					return true;

				case VK_PRIOR:
				case VK_LEFT:
				case VK_UP:
					SetValueInternal(m_nValue - 1, true);
					return true;

				case VK_NEXT:
				case VK_RIGHT:
				case VK_DOWN:
					SetValueInternal(m_nValue + 1, true);
					return true;
			}
			break;
		}
	}

	return false;
}

bool CDXUTSlider::HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam) {
	if (!CDXUTControl::HandleMouse(uMsg, pt, wParam, lParam)) {
		if (!m_bEnabled || !GetVisible())
			return false;

		switch (uMsg) {
			case WM_LBUTTONDOWN:
			case WM_LBUTTONDBLCLK: {
				if (PointInsideRect(m_rcButton, pt)) {
					// Pressed while inside the control
					m_bPressed = true;
					SetCapture(DXUTGetHWND());

					m_nDragX = pt.x;
					//m_nDragY = pt.y;
					m_nDragOffset = m_nButtonX - m_nDragX;

					//m_nDragValue = m_nValue;

					if (!GetIsInFocus())
						m_pDialog->RequestFocus(this);

					return true;
				}

				if (PointInsideRect(m_rcBoundingBox, pt)) {
					POINT myPos;
					GetLocation(myPos);

					if (pt.x > m_nButtonX + myPos.x) {
						SetValueInternal(m_nValue + 1, true);
						return true;
					}

					if (pt.x < m_nButtonX + myPos.y) {
						SetValueInternal(m_nValue - 1, true);
						return true;
					}
				}

				break;
			}

			case WM_LBUTTONUP: {
				if (m_bPressed) {
					m_bPressed = false;
					ReleaseCapture();
					m_pDialog->ClearFocus();
					m_pDialog->SendEvent(EVENT_SLIDER_VALUE_CHANGED, true, this);

					return true;
				}

				break;
			}

			case WM_MOUSEMOVE: {
				if (m_bPressed) {
					POINT myPos;
					GetLocation(myPos);
					SetValueInternal(ValueFromPos(myPos.x + pt.x + m_nDragOffset), true);
					return true;
				}

				break;
			}
		};

		return false;
	}

	return true;
}

void CDXUTSlider::SetRange(int nMin, int nMax) {
	m_nMin = nMin;
	m_nMax = nMax;

	SetValueInternal(m_nValue, false);
}

void CDXUTSlider::SetValueInternal(int nValue, bool bFromInput) {
	// Clamp to range
	nValue = std::max(m_nMin, nValue);
	nValue = std::min(m_nMax, nValue);

	if (nValue == m_nValue)
		return;

	m_nValue = nValue;
	UpdateRects();

	m_pDialog->SendEvent(EVENT_SLIDER_VALUE_CHANGED, bFromInput, this);
}

void CDXUTSlider::Render(float fElapsedTime) {
	int nOffsetX = 0;
	int nOffsetY = 0;

	DXUT_CONTROL_STATE iState = DXUT_STATE_NORMAL;

	// if editing, show control as 'normal'
	if (!GetVisible() && !m_pDialog->GetEdit()) {
		iState = DXUT_STATE_HIDDEN;
	} else if (m_bEnabled == false) {
		iState = DXUT_STATE_DISABLED;
	} else if (m_bPressed) {
		iState = DXUT_STATE_PRESSED;

		nOffsetX = 1;
		nOffsetY = 2;
	} else if (GetMouseOver()) {
		iState = DXUT_STATE_MOUSEOVER;

		nOffsetX = -1;
		nOffsetY = -2;
	} else if (GetIsInFocus()) {
		iState = DXUT_STATE_FOCUS;
	}

	if (iState != DXUT_STATE_HIDDEN) {
		float fBlendRate = (iState == DXUT_STATE_PRESSED) ? 0.0f : 0.8f;

		CDXUTElement* pElement = m_Elements[0];

		// Blend current color
		(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime, fBlendRate);
		m_pDialog->DrawSprite(pElement, &m_rcBoundingBox);

		// TODO: remove magic numbers
		pElement = m_Elements[1];

		// Blend current color
		(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime, fBlendRate);
		m_pDialog->DrawSprite(pElement, &m_rcButton);
	}

	CDXUTControl::Render(fElapsedTime);
}

CDXUTScrollBar::CDXUTScrollBar(CDXUTDialog* pDialog, CDXUTControl* pParent) :
		CDXUTControl(pDialog) {
	m_Type = DXUT_CONTROL_SCROLLBAR;
	m_Tag = SCROLL_BAR_TAG;
	m_pDialog = pDialog;
	m_pParent = pParent;

	m_defaultName = SCROLL_BAR_DEFAULT_NAME;

	m_bShowThumb = false;

	SetRect(&m_rcUpButton, 0, 0, 0, 0);
	SetRect(&m_rcDownButton, 0, 0, 0, 0);
	SetRect(&m_rcTrack, 0, 0, 0, 0);
	SetRect(&m_rcThumb, 0, 0, 0, 0);

	m_nPosition = 0;
	m_nPageSize = 1;
	m_nStart = 0;
	m_nEnd = 1;
	m_Arrow = CLEAR;
	m_dArrowTS = 0.0;

	m_elementTags.clear();
	// These don't actually use the PRESSED state in Render(), but they should.
	m_elementTags[TRACK_DISPLAY_TAG] = ElementTagMapValue(0, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NOT_HIDDEN);
	m_elementTags[UP_ARROW_DISPLAY_TAG] = ElementTagMapValue(1, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NOT_HIDDEN);
	m_elementTags[DOWN_ARROW_DISPLAY_TAG] = ElementTagMapValue(2, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NOT_HIDDEN);
	m_elementTags[BUTTON_DISPLAY_TAG] = ElementTagMapValue(3, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NOT_HIDDEN);
}

CDXUTScrollBar::~CDXUTScrollBar() {}

bool CDXUTScrollBar::Load(TiXmlNode* scroll_bar_root) {
	bool bSuccess = false;

	if (scroll_bar_root) {
		int start = GetTrackStart();
		int end = GetTrackEnd();
		int position = GetTrackPos();
		int pageSize = GetPageSize();

		// given the scrollbar root, iterate over the children.
		TiXmlNode* child = 0;
		for (child = scroll_bar_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, START_TAG)) {
				GetIntFromXMLNode(child, start);
			} else if (!_stricmp(val, END_TAG)) {
				GetIntFromXMLNode(child, end);
			} else if (!_stricmp(val, POSITION_TAG)) {
				GetIntFromXMLNode(child, position);
			} else if (!_stricmp(val, PAGE_SIZE_TAG)) {
				GetIntFromXMLNode(child, pageSize);
			}
		}

		SetTrackRange(start, end);
		SetPageSize(pageSize);
		SetTrackPos(position);

		bSuccess = CDXUTControl::Load(scroll_bar_root);
	}

	return bSuccess;
}

bool CDXUTScrollBar::Save(TiXmlNode* scroll_bar_root) {
	bool bSuccess = false;

	if (scroll_bar_root) {
		TiXmlNode* child = scroll_bar_root->InsertEndChild(TiXmlElement(START_TAG));
		if (child) {
			PutIntToXMLNode(child, GetTrackStart());
		}

		child = scroll_bar_root->InsertEndChild(TiXmlElement(END_TAG));
		if (child) {
			PutIntToXMLNode(child, GetTrackEnd());
		}

		child = scroll_bar_root->InsertEndChild(TiXmlElement(POSITION_TAG));
		if (child) {
			PutLongToXMLNode(child, GetTrackPos());
		}

		child = scroll_bar_root->InsertEndChild(TiXmlElement(PAGE_SIZE_TAG));
		if (child) {
			PutLongToXMLNode(child, GetPageSize());
		}

		bSuccess = CDXUTControl::Save(scroll_bar_root);
	}

	return bSuccess;
}

void CDXUTScrollBar::UpdateRects() {
	CDXUTControl::UpdateRects();

	// Make the buttons square

	SetRect(&m_rcUpButton, m_rcBoundingBox.left, m_rcBoundingBox.top,
		m_rcBoundingBox.right, m_rcBoundingBox.top + RectWidth(m_rcBoundingBox));
	SetRect(&m_rcDownButton, m_rcBoundingBox.left, m_rcBoundingBox.bottom - RectWidth(m_rcBoundingBox),
		m_rcBoundingBox.right, m_rcBoundingBox.bottom);
	SetRect(&m_rcTrack, m_rcUpButton.left, m_rcUpButton.bottom,
		m_rcDownButton.right, m_rcDownButton.top);
	m_rcThumb.left = m_rcUpButton.left;
	m_rcThumb.right = m_rcUpButton.right;

	UpdateThumbRect();
}

// Compute the dimension of the scroll thumb
void CDXUTScrollBar::UpdateThumbRect() {
	if (m_nEnd - m_nStart > m_nPageSize) {
		int nThumbHeight = std::max(RectHeight(m_rcTrack) * m_nPageSize / (m_nEnd - m_nStart), SCROLLBAR_MINTHUMBSIZE);
		int nMaxPosition = m_nEnd - m_nStart - m_nPageSize;
		m_rcThumb.top = m_rcTrack.top + (m_nPosition - m_nStart) * (RectHeight(m_rcTrack) - nThumbHeight) / nMaxPosition;
		m_rcThumb.bottom = m_rcThumb.top + nThumbHeight;
		m_bShowThumb = true;

	} else {
		// No content to scroll
		m_rcThumb.bottom = m_rcThumb.top;
		m_bShowThumb = false;
	}

	// DRF - This should not be happening for invalid dialogs but it does!
	if (!REF_PTR_VALID(m_pDialog)) {
		LogWarn("Invalid IMenuDialog " << m_pDialog);
		return;
	}
	m_pDialog->SendEvent(EVENT_SCROLLBAR_CHANGED, true, this);
}

// Scroll() scrolls by nDelta items.  A positive value scrolls down, while a negative value scrolls up.
void CDXUTScrollBar::Scroll(int nDelta) {
	// Perform scroll
	m_nPosition += nDelta;

	// Cap position
	Cap();

	// Update thumb position
	UpdateThumbRect();
}

void CDXUTScrollBar::ShowItem(int nIndex) {
	// Cap the index

	if (nIndex < 0)
		nIndex = 0;

	if (nIndex >= m_nEnd)
		nIndex = m_nEnd - 1;

	// Adjust position

	if (m_nPosition > nIndex)
		m_nPosition = nIndex;
	else if (m_nPosition + m_nPageSize <= nIndex)
		m_nPosition = nIndex - m_nPageSize + 1;

	UpdateThumbRect();
}

bool CDXUTScrollBar::HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam) {
	return false;
}

bool CDXUTScrollBar::HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam) {
	static int ThumbOffsetY;
	static bool bDrag;

	m_LastMouse = pt;
	switch (uMsg) {
		case WM_LBUTTONDOWN:
		case WM_LBUTTONDBLCLK: {
			if (!GetShowThumb())
				return false;

			// Check for click on up button

			if (PointInsideRect(m_rcUpButton, pt)) {
				if (m_nPosition > m_nStart)
					--m_nPosition;
				UpdateThumbRect();
				m_Arrow = CLICKED_UP;
				m_dArrowTS = DXUTGetTime();
				return true;
			}

			// Check for click on down button

			if (PointInsideRect(m_rcDownButton, pt)) {
				if (m_nPosition + m_nPageSize < m_nEnd)
					++m_nPosition;
				UpdateThumbRect();
				m_Arrow = CLICKED_DOWN;
				m_dArrowTS = DXUTGetTime();
				return true;
			}

			// Check for click on thumb

			if (PointInsideRect(m_rcThumb, pt)) {
				bDrag = true;
				ThumbOffsetY = pt.y - m_rcThumb.top;
				return true;
			}

			// Check for click on track

			if (m_rcThumb.left <= pt.x &&
				m_rcThumb.right > pt.x) {
				SetCapture(DXUTGetHWND());

				if (m_rcThumb.top > pt.y &&
					m_rcTrack.top <= pt.y) {
					Scroll(-(m_nPageSize - 1));
					return true;
				} else if (m_rcThumb.bottom <= pt.y &&
						   m_rcTrack.bottom > pt.y) {
					Scroll(m_nPageSize - 1);
					return true;
				}
			}

			break;
		}

		case WM_LBUTTONUP: {
			bDrag = false;
			ReleaseCapture();
			UpdateThumbRect();
			m_Arrow = CLEAR;
			break;
		}

		case WM_MOUSEMOVE: {
			if (bDrag) {
				m_rcThumb.bottom += pt.y - ThumbOffsetY - m_rcThumb.top;
				m_rcThumb.top = pt.y - ThumbOffsetY;
				if (m_rcThumb.top < m_rcTrack.top)
					OffsetRect(&m_rcThumb, 0, m_rcTrack.top - m_rcThumb.top);
				else if (m_rcThumb.bottom > m_rcTrack.bottom)
					OffsetRect(&m_rcThumb, 0, m_rcTrack.bottom - m_rcThumb.bottom);

				// Compute first item index based on thumb position

				int nMaxFirstItem = m_nEnd - m_nStart - m_nPageSize; // Largest possible index for first item
				int nMaxThumb = RectHeight(m_rcTrack) - RectHeight(m_rcThumb); // Largest possible thumb position from the top

				if (nMaxThumb == 0 || nMaxFirstItem == 0)
					return true;

				m_nPosition = m_nStart +
							  (m_rcThumb.top - m_rcTrack.top +
								  nMaxThumb / (nMaxFirstItem * 2)) * // Shift by half a row to avoid last row covered by only one pixel
								  nMaxFirstItem /
								  nMaxThumb;

				if (m_pDialog)
					m_pDialog->SendEvent(EVENT_SCROLLBAR_CHANGED, true, this);

				return true;
			}

			break;
		}

		case WM_MOUSEWHEEL: // DRF - Added

			bool dirDown = ((wParam & 0x80000000) != 0);
			if (dirDown) {
				if (m_nPosition + m_nPageSize < m_nEnd)
					++m_nPosition;
				UpdateThumbRect();
				m_Arrow = CLICKED_DOWN;
				return true;
			} else {
				if (m_nPosition > m_nStart)
					--m_nPosition;
				UpdateThumbRect();
				m_Arrow = CLICKED_UP;
				return true;
			}
			break;
	}

	return false;
}

void CDXUTScrollBar::Render(float fElapsedTime) {
	// Check if the arrow button has been held for a while.
	// If so, update the thumb position to simulate repeated
	// scroll.
	if (m_Arrow != CLEAR) {
		double dCurrTime = DXUTGetTime();
		if (PointInsideRect(m_rcUpButton, m_LastMouse)) {
			switch (m_Arrow) {
				case CLICKED_UP:
					if (SCROLLBAR_ARROWCLICK_DELAY < dCurrTime - m_dArrowTS) {
						Scroll(-1);
						m_Arrow = HELD_UP;
						m_dArrowTS = dCurrTime;
					}
					break;
				case HELD_UP:
					if (SCROLLBAR_ARROWCLICK_REPEAT < dCurrTime - m_dArrowTS) {
						Scroll(-1);
						m_dArrowTS = dCurrTime;
					}
					break;
			}
		} else if (PointInsideRect(m_rcDownButton, m_LastMouse)) {
			switch (m_Arrow) {
				case CLICKED_DOWN:
					if (SCROLLBAR_ARROWCLICK_DELAY < dCurrTime - m_dArrowTS) {
						Scroll(1);
						m_Arrow = HELD_DOWN;
						m_dArrowTS = dCurrTime;
					}
					break;
				case HELD_DOWN:
					if (SCROLLBAR_ARROWCLICK_REPEAT < dCurrTime - m_dArrowTS) {
						Scroll(1);
						m_dArrowTS = dCurrTime;
					}
					break;
			}
		}
	}

	DXUT_CONTROL_STATE iState = DXUT_STATE_NORMAL;

	// if editing, show control as 'normal'
	if (!GetVisible() && !m_pDialog->GetEdit())
		iState = DXUT_STATE_HIDDEN;
	else if (m_bEnabled == false || m_bShowThumb == false)
		iState = DXUT_STATE_DISABLED;
	else if (GetMouseOver())
		iState = DXUT_STATE_MOUSEOVER;
	else if (GetIsInFocus())
		iState = DXUT_STATE_FOCUS;

	if (iState != DXUT_STATE_HIDDEN) {
		float fBlendRate = (iState == DXUT_STATE_PRESSED) ? 0.0f : 0.8f;

		// Background track layer
		CDXUTElement* pElement = m_Elements[0];

		// Blend current color
		(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime, fBlendRate);
		m_pDialog->DrawSprite(pElement, &m_rcTrack);

		// Up Arrow
		pElement = m_Elements[1];

		// Blend current color
		(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime, fBlendRate);
		m_pDialog->DrawSprite(pElement, &m_rcUpButton);

		// Down Arrow
		pElement = m_Elements[2];

		// Blend current color
		(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime, fBlendRate);
		m_pDialog->DrawSprite(pElement, &m_rcDownButton);

		// Thumb button
		pElement = m_Elements[3];

		// Blend current color
		(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime, fBlendRate);
		m_pDialog->DrawSprite(pElement, &m_rcThumb);
	}

	CDXUTControl::Render(fElapsedTime);
}

void CDXUTScrollBar::SetTrackRange(int nStart, int nEnd) {
	m_nStart = nStart;
	m_nEnd = nEnd;
	Cap();
	UpdateThumbRect();
}

void CDXUTScrollBar::Cap() { // Clips position at boundaries. Ensures it stays within legal range.
	if ((m_nPosition < m_nStart) || ((m_nEnd - m_nStart) <= m_nPageSize)) {
		m_nPosition = m_nStart;
	} else if ((m_nPosition + m_nPageSize) > m_nEnd) {
		m_nPosition = m_nEnd - m_nPageSize;
	}
}

CDXUTListBox::CDXUTListBox(CDXUTDialog* pDialog) :
		CDXUTControl(pDialog),
		m_ScrollBar(pDialog) {
	m_ScrollBar.SetParent(this);

	m_Type = DXUT_CONTROL_LISTBOX;
	m_Tag = LIST_BOX_TAG;
	m_pDialog = pDialog;

	m_defaultName = LIST_BOX_DEFAULT_NAME;

	m_dwStyle = 0;
	m_nSBWidth = 16;
	m_nSelected = -1;
	m_nSelStart = 0;
	m_bDrag = false;
	m_nBorder = 6;
	m_nLeftMargin = 5;
	m_nRightMargin = 5;
	m_nTextHeight = 0;
	m_nRowHeight = 0;
	m_bDragSelection = true;
	m_bHTMLSupport = false;

	m_elementTags.clear();
	m_elementTags[MAIN_DISPLAY_TAG] = ElementTagMapValue(0, DXUT_STATE_FLAG_NORMAL, DXUT_STATE_FLAG_NORMAL);
	m_elementTags[SELECTION_DISPLAY_TAG] = ElementTagMapValue(1, DXUT_STATE_FLAG_NORMAL, DXUT_STATE_FLAG_NORMAL);
}

CDXUTListBox::~CDXUTListBox() {
	RemoveAllItems();
}

bool CDXUTListBox::Load(TiXmlNode* list_box_root) {
	bool bSuccess = false;

	if (list_box_root) {
		// given the listbox root, iterate over the children.
		TiXmlNode* child = 0;
		for (child = list_box_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, MULTI_SELECTION_TAG)) {
				bool bMultiSelection = false;
				if (GetBoolFromXMLNode(child, bMultiSelection)) {
					SetStyle(bMultiSelection ? MULTISELECTION : 0);
				}
			} else if (!_stricmp(val, BORDER_TAG)) {
				int width = -1;
				if (GetIntFromXMLNode(child, width))
					m_nBorder = width;
			}
			// legacy tag for backwards compatibility
			else if (!_stricmp(val, MARGIN_TAG)) {
				int margin = -1;
				if (GetIntFromXMLNode(child, margin)) {
					SetMargins(margin);
				}
			} else if (!_stricmp(val, MARGINS_TAG)) {
				LoadMargins(child);
			} else if (!_stricmp(val, SCROLL_BAR_WIDTH_TAG)) {
				int width = -1;
				if (GetIntFromXMLNode(child, width))
					SetScrollBarWidth(width);
			} else if (!_stricmp(val, ITEMS_TAG)) {
				LoadItems(child);
			} else if (!_stricmp(val, ROW_HEIGHT_TAG)) {
				int height = 0;
				if (GetIntFromXMLNode(child, height))
					SetRowHeight(height);
			} else if (!_stricmp(val, DRAG_SELECTION_TAG)) {
				bool bDragSelection = 0;
				if (GetBoolFromXMLNode(child, bDragSelection))
					SetDragSelection(bDragSelection);
			} else if (!_stricmp(val, SCROLL_BAR_TAG)) {
				m_ScrollBar.Load(child);
			}
		}

		bSuccess = CDXUTControl::Load(list_box_root);
	}

	return bSuccess;
}

bool CDXUTListBox::LoadItems(TiXmlNode* items_root) {
	bool bSuccess = false;

	if (items_root) {
		// given the items root, iterate over the children.
		TiXmlNode* child = 0;
		for (child = items_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, ITEM_TAG)) {
				bSuccess = LoadItem(child);
			}
		}
	}

	return bSuccess;
}

bool CDXUTListBox::LoadItem(TiXmlNode* item_root) {
	bool bSuccess = false;

	if (item_root) {
		void* data = 0;
		std::string sText = "";

		// given the item root, iterate over the children.
		TiXmlNode* child = 0;
		for (child = item_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, TEXT_TAG)) {
				GetStringFromXMLNode(child, sText);
			}
		}

		if (!sText.empty()) {
			AddItem(sText.c_str(), data);
		}
	}

	return bSuccess;
}

bool CDXUTListBox::LoadMargins(TiXmlNode* margins_root) {
	bool bSuccess = false;

	if (margins_root) {
		int nMargin = -1;

		TiXmlNode* child = 0;
		for (child = margins_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, LEFT_TAG)) {
				nMargin = -1;
				if (GetIntFromXMLNode(child, nMargin)) {
					SetLeftMargin(nMargin);
					bSuccess = true;
				}
			} else if (!_stricmp(val, RIGHT_TAG)) {
				nMargin = -1;
				if (GetIntFromXMLNode(child, nMargin)) {
					SetRightMargin(nMargin);
					bSuccess = true;
				}
			}
		}
	}

	return bSuccess;
}

bool CDXUTListBox::Save(TiXmlNode* list_box_root) {
	bool bSuccess = false;

	if (list_box_root) {
		TiXmlNode* child = list_box_root->InsertEndChild(TiXmlElement(MULTI_SELECTION_TAG));
		if (child) {
			PutBoolToXMLNode(child, GetStyle() == MULTISELECTION);
		}

		child = list_box_root->InsertEndChild(TiXmlElement(BORDER_TAG));
		if (child) {
			PutIntToXMLNode(child, GetBorder());
		}

		SaveMargins(list_box_root);

		child = list_box_root->InsertEndChild(TiXmlElement(SCROLL_BAR_WIDTH_TAG));
		if (child) {
			PutIntToXMLNode(child, GetScrollBarWidth());
		}

		child = list_box_root->InsertEndChild(TiXmlElement(ROW_HEIGHT_TAG));
		if (child) {
			PutIntToXMLNode(child, GetRowHeight());
		}

		child = list_box_root->InsertEndChild(TiXmlElement(DRAG_SELECTION_TAG));
		if (child) {
			PutBoolToXMLNode(child, GetDragSelection());
		}

		child = list_box_root->InsertEndChild(TiXmlElement(SCROLL_BAR_TAG));
		if (child) {
			m_ScrollBar.Save(child);
		}

		SaveItems(list_box_root);

		bSuccess = CDXUTControl::Save(list_box_root);
	}

	return bSuccess;
}

bool CDXUTListBox::SaveItems(TiXmlNode* list_box_root) {
	bool bSuccess = false;

	if (list_box_root) {
		TiXmlNode* items_root = list_box_root->InsertEndChild(TiXmlElement(ITEMS_TAG));
		if (items_root) {
			for (int i = 0; i < GetSize(); ++i) {
				DXUTListBoxItem* pItem = m_Items[i];
				SaveItem(items_root, pItem);
			}
		}
	}

	return bSuccess;
}

bool CDXUTListBox::SaveItem(TiXmlNode* items_root, DXUTListBoxItem* pItem) {
	bool bSuccess = false;

	if (items_root && pItem) {
		TiXmlNode* item_root = items_root->InsertEndChild(TiXmlElement(ITEM_TAG));
		if (item_root) {
			TiXmlNode* child = item_root->InsertEndChild(TiXmlElement(TEXT_TAG));
			if (child) {
				std::string sText = pItem->strText;
				PutStringToXMLNode(child, sText);
			}
		}
	}

	return bSuccess;
}

bool CDXUTListBox::SaveMargins(TiXmlNode* list_box_root) {
	bool bSuccess = false;

	if (list_box_root) {
		TiXmlNode* margins_root = list_box_root->InsertEndChild(TiXmlElement(MARGINS_TAG));
		if (margins_root) {
			TiXmlNode* child = margins_root->InsertEndChild(TiXmlElement(LEFT_TAG));
			if (child) {
				if (PutIntToXMLNode(child, GetLeftMargin()))
					bSuccess = true;
			}

			child = margins_root->InsertEndChild(TiXmlElement(RIGHT_TAG));
			if (child) {
				if (PutIntToXMLNode(child, GetRightMargin()))
					bSuccess = true;
			}
		}
	}

	return bSuccess;
}

void CDXUTListBox::UpdateRects() {
	CDXUTControl::UpdateRects();

	// if the scrollbar's thumb is not showing, then no scrolling
	// so don't show the scrollbar at all
	int nSBWidth = m_ScrollBar.GetShowThumb() ? m_nSBWidth : 0;

	m_rcSelection = m_rcBoundingBox;
	m_rcSelection.right -= nSBWidth;
	InflateRect(&m_rcSelection, -m_nBorder, -m_nBorder);
	m_rcText = m_rcSelection;

	// adjust for margins
	m_rcText.left += m_nLeftMargin;
	m_rcText.right -= m_nRightMargin;

	// Update the scrollbar's rects
	m_ScrollBar.SetLocation(m_rcBoundingBox.right - m_nSBWidth, m_rcBoundingBox.top);
	m_ScrollBar.SetSize(nSBWidth, GetHeight());

	if (m_Elements.size() > 0) {
		ASSERT_GDRM();

		CDXUTFontNode* pFontNode = pGDRM->GetFontNode(m_Elements[0]->GetFontIndex());
		if (pFontNode && pFontNode->nHeight) {
			m_ScrollBar.SetPageSize(RectHeight(m_rcText) / (m_nRowHeight > 0 ? m_nRowHeight : pFontNode->nHeight));

			// just switched from showing thumb, to not showing thumb
			// recalculate the rects
			if (!m_ScrollBar.GetShowThumb() && nSBWidth != 0) {
				nSBWidth = 0;

				m_rcSelection = m_rcBoundingBox;
				m_rcSelection.right -= nSBWidth;
				InflateRect(&m_rcSelection, -m_nBorder, -m_nBorder);
				m_rcText = m_rcSelection;
				// adjust for margins
				m_rcText.left += m_nLeftMargin;
				m_rcText.right -= m_nRightMargin;

				// Update the scrollbar's rects
				m_ScrollBar.SetLocation(m_rcBoundingBox.right - m_nSBWidth, m_rcBoundingBox.top);
				m_ScrollBar.SetSize(nSBWidth, GetHeight());
			}

			// The selected item may have been scrolled off the page.
			// Ensure that it is in page again.
			m_ScrollBar.ShowItem(m_nSelected);
		}
	}
}

HRESULT CDXUTListBox::AddItem(const char* wszText, void* pData) {
	DXUTListBoxItem* pNewItem = new DXUTListBoxItem;
	if (!pNewItem)
		return E_OUTOFMEMORY;

	pNewItem->strText = wszText;
	pNewItem->pData = pData;
	SetRect(&pNewItem->rcActive, 0, 0, 0, 0);
	pNewItem->bSelected = false;

	m_Items.push_back(pNewItem);

	m_ScrollBar.SetTrackRange(0, GetSize());

	// scrollbar needs to update too
	UpdateRects();

	//Ankit ----- set up the word wrapper object
	(pNewItem->m_ListBoxItemWordWrapper).SetRecalcFlag(true);

	return S_OK;
}

HRESULT CDXUTListBox::InsertItem(int nIndex, const char* wszText, void* pData) {
	if (nIndex < 0 || nIndex > GetSize())
		return E_INVALIDARG;

	DXUTListBoxItem* pNewItem = new DXUTListBoxItem;
	if (!pNewItem)
		return E_OUTOFMEMORY;

	pNewItem->strText = wszText;
	pNewItem->pData = pData;
	SetRect(&pNewItem->rcActive, 0, 0, 0, 0);
	pNewItem->bSelected = false;

	m_Items.insert(m_Items.begin() + nIndex, pNewItem);

	m_ScrollBar.SetTrackRange(0, GetSize());

	// scrollbar needs to update too
	UpdateRects();

	//Ankit ----- set up the word wrapper object
	(pNewItem->m_ListBoxItemWordWrapper).SetRecalcFlag(true);

	return S_OK;
}

void CDXUTListBox::RemoveItem(int nIndex) {
	if (nIndex < 0 || nIndex >= GetSize())
		return;

	DXUTListBoxItem* pItem = m_Items[nIndex];

	delete pItem;

	m_Items.erase(m_Items.begin() + nIndex);

	m_ScrollBar.SetTrackRange(0, GetSize());

	if (m_nSelected >= GetSize())
		m_nSelected = GetSize() - 1;

	if (m_nSelStart >= GetSize())
		m_nSelStart = GetSize() - 1;

	// scrollbar needs to update too
	UpdateRects();

	m_pDialog->SendEvent(EVENT_LISTBOX_SELECTION, true, this);
}

void CDXUTListBox::RemoveItemByText(char* wszText) {}

void CDXUTListBox::RemoveItemByData(void* pData) {}

int CDXUTListBox::FindItem(void* pData) {
	for (int i = 0; i < GetSize(); ++i) {
		DXUTListBoxItem* pItem = m_Items[i];

		if (pItem->pData == pData)
			return i;
	}

	return -1;
}

void CDXUTListBox::ClearSelection() {
	if (m_dwStyle & MULTISELECTION) {
		for (int i = 0; i < GetSize(); i++) {
			DXUTListBoxItem* pItem = m_Items[i];
			pItem->bSelected = false;
		}
	} else {
		int nBegin = std::min(m_nSelStart, m_nSelected);
		int nEnd = std::max(m_nSelStart, m_nSelected);

		for (int i = nBegin; i <= nEnd; ++i) {
			if (i > -1 && i < GetSize()) { //crash prevention
				DXUTListBoxItem* pItem = m_Items[i];
				pItem->bSelected = false;
			}
		}
	}
	m_nSelected = -1;
	m_nSelStart = 0;
}

void CDXUTListBox::RemoveAllItems() {
	// Clear Selection
	ClearSelection();

	// Delete All Items
	for (int i = 0; i < GetSize(); ++i) {
		DXUTListBoxItem* pItem = m_Items[i];
		delete pItem;
		pItem = NULL;
	}
	m_Items.clear();

	// Reset Scrollbar
	m_ScrollBar.SetTrackRange(0, 1);
	UpdateRects();
}

DXUTListBoxItem* CDXUTListBox::GetItem(int nIndex) {
	return (nIndex < 0 || nIndex >= GetSize()) ? NULL : m_Items[nIndex];
}

const char* CDXUTListBox::GetItemText(int nIndex) {
	DXUTListBoxItem* pItem = GetItem(nIndex);
	return pItem ? pItem->strText.c_str() : NULL;
}

const char* CDXUTListBox::GetSelectedItemText() {
	DXUTListBoxItem* pItem = GetSelectedItem();
	return pItem ? pItem->strText.c_str() : NULL;
}

void* CDXUTListBox::GetItemData(int nIndex) {
	DXUTListBoxItem* pItem = GetItem(nIndex);
	return pItem ? pItem->pData : NULL;
}

void* CDXUTListBox::GetSelectedItemData() {
	DXUTListBoxItem* pItem = GetSelectedItem();
	return pItem ? pItem->pData : NULL;
}

// For single-selection listbox, returns the index of the selected item.
// For multi-selection, returns the first selected item after the nPreviousSelected position.
// To search for the first selected item, the app passes -1 for nPreviousSelected.  For
// subsequent searches, the app passes the returned index back to GetSelectedIndex as.
// nPreviousSelected.
// Returns -1 on error or if no item is selected.
int CDXUTListBox::GetSelectedIndex(int nPreviousSelected) {
	if (nPreviousSelected < -1)
		return -1;

	if (m_dwStyle & MULTISELECTION) {
		// Multiple selection enabled. Search for the next item with the selected flag.
		for (int i = nPreviousSelected + 1; i < GetSize(); ++i) {
			DXUTListBoxItem* pItem = m_Items[i];
			if (pItem && pItem->bSelected)
				return i;
		}
		return -1;
	} else {
		// Single selection
		return m_nSelected;
	}
}

void CDXUTListBox::SelectItem(int nNewIndex) {
	// If no item exists, do nothing.
	if (GetSize() == 0)
		return;

	int nOldSelected = m_nSelected;

	// Adjust m_nSelected
	m_nSelected = nNewIndex;

	// Perform capping
	if (m_nSelected < 0)
		m_nSelected = 0;
	if (m_nSelected >= GetSize())
		m_nSelected = GetSize() - 1;

	if (nOldSelected != m_nSelected) {
		if (m_dwStyle & MULTISELECTION) {
			if (m_nSelected > -1 && m_nSelected < GetSize())
				m_Items[m_nSelected]->bSelected = true;

			if (m_nSelStart > m_nSelected)
				m_nSelStart = m_nSelected;
		} else {
			// Update selection start
			m_nSelStart = m_nSelected;
		}

		// Adjust scroll bar
		m_ScrollBar.ShowItem(m_nSelected);
	}

	m_pDialog->SendEvent(EVENT_LISTBOX_SELECTION, true, this);
}

void CDXUTListBox::SetItemText(int nIndex, const char* text) {
	if (text) {
		if (nIndex >= 0 && nIndex < GetSize()) {
			DXUTListBoxItem* pItem = m_Items[nIndex];

			if (pItem) {
				pItem->strText = text;

				//Ankit ----- set the needToRecalcWordWrapFlag to true;
				(pItem->m_ListBoxItemWordWrapper).SetRecalcFlag(true);
			}
		}
	}
}

bool CDXUTListBox::HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if (!m_bEnabled || !GetVisible())
		return false;

	// Let the scroll bar have a chance to handle it first
	if (m_ScrollBar.HandleKeyboard(uMsg, wParam, lParam))
		return true;

	switch (uMsg) {
		case WM_KEYDOWN:
			switch (wParam) {
				case VK_UP:
				case VK_DOWN:
				case VK_NEXT:
				case VK_PRIOR:
				case VK_HOME:
				case VK_END:

					// If no item exists, do nothing.
					if (GetSize() == 0)
						return true;

					int nOldSelected = m_nSelected;

					// Adjust m_nSelected
					switch (wParam) {
						case VK_UP:
							--m_nSelected;
							break;
						case VK_DOWN:
							++m_nSelected;
							break;
						case VK_NEXT:
							m_nSelected += m_ScrollBar.GetPageSize() - 1;
							break;
						case VK_PRIOR:
							m_nSelected -= m_ScrollBar.GetPageSize() - 1;
							break;
						case VK_HOME:
							m_nSelected = 0;
							break;
						case VK_END:
							m_nSelected = GetSize() - 1;
							break;
					}

					// Perform capping
					if (m_nSelected < 0)
						m_nSelected = 0;
					if (m_nSelected >= GetSize())
						m_nSelected = GetSize() - 1;

					if (nOldSelected != m_nSelected) {
						if (m_dwStyle & MULTISELECTION) {
							// Multiple selection

							// Clear all selection
							for (int i = 0; i < GetSize(); ++i) {
								DXUTListBoxItem* pItem = m_Items[i];
								pItem->bSelected = false;
							}

							if (GetKeyState(VK_SHIFT) < 0) {
								// Select all items from m_nSelStart to
								// m_nSelected
								int nEnd = std::max(m_nSelStart, m_nSelected);

								for (int n = std::min(m_nSelStart, m_nSelected); n <= nEnd; ++n) {
									if (n > -1 && n < GetSize())
										m_Items[n]->bSelected = true;
								}
							} else {
								if (m_nSelected > -1 && m_nSelected < GetSize())
									m_Items[m_nSelected]->bSelected = true;

								// Update selection start
								m_nSelStart = m_nSelected;
							}
						} else {
							m_nSelStart = m_nSelected;
						}

						// Adjust scroll bar

						m_ScrollBar.ShowItem(m_nSelected);

						// Send notification

						// the first number, 1, signifies this was from a keyboard
						// the second number, 0, is for mouse down
						POINT pt = { 1, 0 };
						m_pDialog->SendEvent(EVENT_LISTBOX_SELECTION, true, this, pt);
					}
					return true;
			}
			break;
	}

	return false;
}

bool CDXUTListBox::HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam) {
	if (!CDXUTControl::HandleMouse(uMsg, pt, wParam, lParam)) {
		if (!m_bEnabled || !GetVisible())
			return false;

		// First acquire focus
		if (WM_LBUTTONDOWN == uMsg)
			if (!GetIsInFocus())
				m_pDialog->RequestFocus(this);

		// Let the scroll bar handle it first.
		if (m_ScrollBar.HandleMouse(uMsg, pt, wParam, lParam))
			return true;

		switch (uMsg) {
			case WM_LBUTTONDOWN:
			case WM_LBUTTONDBLCLK:
				// Check for clicks in the text area
				if (PointInsideRect(m_rcSelection, pt)) {
					if (GetSize() > 0) {
						// Compute the index of the clicked item

						int nClicked;
						if (m_nTextHeight)
							nClicked = m_ScrollBar.GetTrackPos() + (pt.y - m_rcText.top) / m_nTextHeight;
						else
							nClicked = -1;

						// Only proceed if the click falls on top of an item.

						if (nClicked >= m_ScrollBar.GetTrackPos() &&
							nClicked < GetSize() &&
							nClicked < m_ScrollBar.GetTrackPos() + m_ScrollBar.GetPageSize()) {
							if (m_bDragSelection) {
								SetCapture(DXUTGetHWND());
								m_bDrag = true;
							}

							// If this is a double click, fire off an event and exit
							// since the first click would have taken care of the selection
							// updating.
							if (uMsg == WM_LBUTTONDBLCLK) {
								m_pDialog->SendEvent(EVENT_LISTBOX_ITEM_DBLCLK, true, this);
								return true;
							}

							m_nSelected = nClicked;
							if (!(wParam & MK_SHIFT))
								m_nSelStart = m_nSelected;

							m_LastTouch = pt;

							// If this is a multi-selection listbox, update per-item
							// selection data.

							if (m_dwStyle & MULTISELECTION) {
								// Determine behavior based on the state of Shift and Ctrl

								if (m_nSelected > -1 && m_nSelected < GetSize()) {
									DXUTListBoxItem* pSelItem = m_Items[m_nSelected];
									if ((wParam & (MK_SHIFT | MK_CONTROL)) == MK_CONTROL) {
										// Control click. Reverse the selection of this item.

										pSelItem->bSelected = !pSelItem->bSelected;
									} else if ((wParam & (MK_SHIFT | MK_CONTROL)) == MK_SHIFT) {
										// Shift click. Set the selection for all items
										// from last selected item to the current item.
										// Clear everything else.

										int nBegin = std::min(m_nSelStart, m_nSelected);
										int nEnd = std::max(m_nSelStart, m_nSelected);

										for (int i = 0; i < nBegin; ++i) {
											if (i < GetSize()) {
												DXUTListBoxItem* pItem = m_Items[i];
												pItem->bSelected = false;
											}
										}

										for (int i = nEnd + 1; i < GetSize(); ++i) {
											if (i > -1 && i < GetSize()) {
												DXUTListBoxItem* pItem = m_Items[i];
												pItem->bSelected = false;
											}
										}

										for (int i = nBegin; i <= nEnd; ++i) {
											if (i > -1 && i < GetSize()) {
												DXUTListBoxItem* pItem = m_Items[i];
												pItem->bSelected = true;
											}
										}
									} else if ((wParam & (MK_SHIFT | MK_CONTROL)) == (MK_SHIFT | MK_CONTROL)) {
										// Control-Shift-click.

										// The behavior is:
										//   Set all items from m_nSelStart to m_nSelected to
										//     the same state as m_nSelStart, not including m_nSelected.
										//   Set m_nSelected to selected.

										int nBegin = std::min(m_nSelStart, m_nSelected);
										int nEnd = std::max(m_nSelStart, m_nSelected);

										// The two ends do not need to be set here.

										if (m_nSelStart > -1 && m_nSelStart < GetSize()) {
											bool bLastSelected = m_Items[m_nSelStart]->bSelected;

											for (int i = nBegin + 1; i < nEnd; ++i) {
												if (i > -1 && i < GetSize()) {
													DXUTListBoxItem* pItem = m_Items[i];
													pItem->bSelected = bLastSelected;
												}
											}
										}

										pSelItem->bSelected = true;

										// Restore m_nSelected to the previous value
										// This matches the Windows behavior

										m_nSelected = m_nSelStart;
									} else {
										// Simple click.  Clear all items and select the clicked
										// item.

										for (int i = 0; i < GetSize(); ++i) {
											DXUTListBoxItem* pItem = m_Items[i];
											pItem->bSelected = false;
										}

										pSelItem->bSelected = true;
									}
								}
							} // End of multi-selection case

							POINT listBoxPoint = { 0, 1 };
							m_pDialog->SendEvent(EVENT_LISTBOX_SELECTION, true, this, listBoxPoint);
						}

						return true;
					}
					// there are no items in the list
					else {
						// but we still want to consume the click so it won't fall through
						return true;
					}
				}

				break;

			case WM_LBUTTONUP: {
				ReleaseCapture();

				if (m_nSelected != -1) {
					// Set all items between m_nSelStart and m_nSelected to
					// the same state as m_nSelStart
					int nEnd = std::max(m_nSelStart, m_nSelected);

					if (GetSize() > 0) {
						for (int n = std::min(m_nSelStart, m_nSelected) + 1; n < nEnd; ++n) {
							if (n > -1 && n < GetSize() && m_nSelStart > -1 && m_nSelStart < GetSize())
								m_Items[n]->bSelected = m_Items[m_nSelStart]->bSelected;
						}

						//check bounds ... script shouldnt be able to explode us
						if (m_nSelected > -1 && m_nSelected < GetSize() && m_nSelStart > -1 && m_nSelStart < GetSize()) {
							m_Items[m_nSelected]->bSelected = m_Items[m_nSelStart]->bSelected;
						}
					}

					// if the user has dragged the mouse to make a selection.
					// Notify the application of this.
					if (m_bDrag && m_bDragSelection)
						m_pDialog->SendEvent(EVENT_LISTBOX_SELECTION, true, this);
				}

				m_bDrag = false;

				return false;
			}

			case WM_MOUSEMOVE:
				if (m_bDrag && m_bDragSelection) {
					// Compute the index of the item below cursor

					int nItem;
					if (m_nTextHeight)
						nItem = m_ScrollBar.GetTrackPos() + (pt.y - m_rcText.top) / m_nTextHeight;
					else
						nItem = -1;

					// Only proceed if the cursor is on top of an item.

					if (nItem >= (int)m_ScrollBar.GetTrackPos() &&
						nItem < GetSize() &&
						nItem < m_ScrollBar.GetTrackPos() + m_ScrollBar.GetPageSize()) {
						m_nSelected = nItem;
						POINT listboxPoint = { 0, 1 };
						m_pDialog->SendEvent(EVENT_LISTBOX_SELECTION, true, this, listboxPoint);
					} else if (nItem < (int)m_ScrollBar.GetTrackPos()) {
						// User drags the mouse above window top
						m_ScrollBar.Scroll(-1);
						m_nSelected = m_ScrollBar.GetTrackPos();
						POINT listboxPoint = { 0, 1 };
						m_pDialog->SendEvent(EVENT_LISTBOX_SELECTION, true, this, listboxPoint);
					} else if (nItem >= m_ScrollBar.GetTrackPos() + m_ScrollBar.GetPageSize()) {
						// User drags the mouse below window bottom
						m_ScrollBar.Scroll(1);
						m_nSelected = std::min(GetSize(), m_ScrollBar.GetTrackPos() + m_ScrollBar.GetPageSize()) - 1;
						POINT listboxPoint = { 0, 1 };
						m_pDialog->SendEvent(EVENT_LISTBOX_SELECTION, true, this, listboxPoint);
					}
				}
				break;

			case WM_MOUSEWHEEL: {
				UINT uLines;
				SystemParametersInfo(SPI_GETWHEELSCROLLLINES, 0, &uLines, 0);
				int nScrollAmount = int((short)HIWORD(wParam)) / WHEEL_DELTA * uLines;
				m_ScrollBar.Scroll(-nScrollAmount);
				return true;
			}
		}

		return false;
	}

	return true;
}

void CDXUTListBox::Render(float fElapsedTime) {
	DXUT_CONTROL_STATE iState = DXUT_STATE_NORMAL;

	// if editing, show control as 'normal'
	if (!GetVisible() && !m_pDialog->GetEdit())
		iState = DXUT_STATE_HIDDEN;

	if (iState != DXUT_STATE_HIDDEN) {
		CDXUTElement* pElement = m_Elements[0];
		(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime);
		(*(pElement->GetFontColor())).Blend(iState, fElapsedTime);

		CDXUTElement* pSelElement = m_Elements[1];
		(*(pSelElement->GetTextureColor())).Blend(iState, fElapsedTime);
		(*(pSelElement->GetFontColor())).Blend(iState, fElapsedTime);

		m_pDialog->DrawSprite(pElement, &m_rcBoundingBox);

		// Render the text
		if (GetSize() > 0) {
			ASSERT_GDRM();

			// Find out the height of a single line of text
			RECT rc = m_rcText;
			RECT rcSel = m_rcSelection;
			rc.bottom = rc.top + (m_nRowHeight > 0 ? m_nRowHeight : abs(pGDRM->GetFontNode(pElement->GetFontIndex())->nHeight));

			// Update the line height formation
			m_nTextHeight = rc.bottom - rc.top;

			static bool bSBInit;
			if (!bSBInit) {
				// Update the page size of the scroll bar
				if (m_nTextHeight)
					m_ScrollBar.SetPageSize(RectHeight(m_rcText) / m_nTextHeight);
				else
					m_ScrollBar.SetPageSize(RectHeight(m_rcText));
				bSBInit = true;
			}

			rc.right = m_rcText.right;
			for (int i = m_ScrollBar.GetTrackPos(); i < GetSize(); ++i) {
				if (rc.bottom > m_rcText.bottom)
					break;

				if (i > -1 && i < GetSize()) {
					DXUTListBoxItem* pItem = m_Items[i];

					// Determine if we need to render this item with the
					// selected element.
					bool bSelectedStyle = false;

					if (!(m_dwStyle & MULTISELECTION) && i == m_nSelected) {
						bSelectedStyle = true;
					} else if (m_dwStyle & MULTISELECTION) {
						if (m_bDrag && ((i >= m_nSelected && i < m_nSelStart) || (i <= m_nSelected && i > m_nSelStart))) {
							if (m_nSelStart > -1 && m_nSelStart < GetSize())
								bSelectedStyle = m_Items[m_nSelStart]->bSelected;
						} else if (pItem->bSelected) {
							bSelectedStyle = true;
						}
					}

					if (bSelectedStyle) {
						rcSel.top = rc.top;
						rcSel.bottom = rc.bottom;
						m_pDialog->DrawSprite(pSelElement, &rcSel);
						if (m_bHTMLSupport) {
							DrawHTML(Utf8ToUtf16(pItem->strText).c_str(), pSelElement, &rc, -1);
						} else {
							//Ankit ----- replacing the draw text to instead route through the word-wrapper object
							(pItem->m_ListBoxItemWordWrapper).DrawWordWrappedText(m_pDialog, Utf8ToUtf16(pItem->strText).c_str(), pSelElement, &rc);
						}

					} else {
						if (m_bHTMLSupport) {
							DrawHTML(Utf8ToUtf16(pItem->strText).c_str(), pElement, &rc, -1);
						} else {
							//Ankit ----- replacing the draw text to instead route through the word-wrapper object
							(pItem->m_ListBoxItemWordWrapper).DrawWordWrappedText(m_pDialog, Utf8ToUtf16(pItem->strText).c_str(), pElement, &rc);
						}
					}
					OffsetRect(&rc, 0, m_nTextHeight);
				}
			}
		}

		// Render the scroll bar
		// (if it needs to be shown)
		if (m_ScrollBar.GetShowThumb())
			m_ScrollBar.Render(fElapsedTime);
	}

	CDXUTControl::Render(fElapsedTime);
}

void CUniBuffer::InitializeUniscribe() {
	TCHAR wszPath[MAX_PATH + 1];
	if (!::GetSystemDirectory(wszPath, MAX_PATH + 1))
		return;
	lstrcat(wszPath, UNISCRIBE_DLLNAME);
	s_hDll = LoadLibrary(wszPath);
	if (s_hDll) {
		FARPROC Temp;
		GETPROCADDRESS(s_hDll, ScriptApplyDigitSubstitution, Temp);
		GETPROCADDRESS(s_hDll, ScriptStringAnalyse, Temp);
		GETPROCADDRESS(s_hDll, ScriptStringCPtoX, Temp);
		GETPROCADDRESS(s_hDll, ScriptStringXtoCP, Temp);
		GETPROCADDRESS(s_hDll, ScriptStringFree, Temp);
		GETPROCADDRESS(s_hDll, ScriptString_pLogAttr, Temp);
		GETPROCADDRESS(s_hDll, ScriptString_pcOutChars, Temp);
	}
}

void CUniBuffer::UninitializeUniscribe() {
	if (s_hDll) {
		PLACEHOLDERPROC(ScriptApplyDigitSubstitution);
		PLACEHOLDERPROC(ScriptStringAnalyse);
		PLACEHOLDERPROC(ScriptStringCPtoX);
		PLACEHOLDERPROC(ScriptStringXtoCP);
		PLACEHOLDERPROC(ScriptStringFree);
		PLACEHOLDERPROC(ScriptString_pLogAttr);
		PLACEHOLDERPROC(ScriptString_pcOutChars);

		FreeLibrary(s_hDll);
		s_hDll = NULL;
	}
}

// Uniscribe -- Analyse() analyses the string in the buffer
HRESULT CUniBuffer::Analyse() {
	ASSERT_GDRM(S_OK);

	if (m_Analysis)
		_ScriptStringFree(&m_Analysis);

	SCRIPT_CONTROL ScriptControl; // For uniscribe
	SCRIPT_STATE ScriptState; // For uniscribe
	ZeroMemory(&ScriptControl, sizeof(ScriptControl));
	ZeroMemory(&ScriptState, sizeof(ScriptState));
	_ScriptApplyDigitSubstitution(NULL, &ScriptControl, &ScriptState);

	CDXUTFontNode* pFontNode = pGDRM->GetFontNode(m_iFont);
	if (!pFontNode)
		return S_OK;

	HRESULT hr = _ScriptStringAnalyse(pFontNode->pFont ? pFontNode->pFont->GetDC() : NULL,
		m_strBuffer.c_str(),
		m_strBuffer.size() + 1, // NULL is also analyzed.
		m_strBuffer.size() * 3 / 2 + 16,
		-1, // Used to be ANSI_CHARSET. Now use -1 for UTF-16 (there is no constant given to us for UTF-16).
		SSA_BREAK | SSA_GLYPHS | SSA_FALLBACK | SSA_LINK,
		0,
		&ScriptControl,
		&ScriptState,
		NULL,
		NULL,
		NULL,
		&m_Analysis);
	if (SUCCEEDED(hr) && (m_iFont != 0))
		m_bAnalyseRequired = false; // Analysis is up-to-date
	return hr;
}

CUniBuffer::CUniBuffer(const wchar_t* str) {
	m_bAnalyseRequired = true;
	m_Analysis = NULL;
	m_iFont = 0;

	// DRF - Added
	SetText(str);
}

CUniBuffer::~CUniBuffer() {
	if (m_Analysis)
		_ScriptStringFree(&m_Analysis);
}

TCHAR& CUniBuffer::operator[](size_t n) {
	// This version of operator[] is called only
	// if we are asking for write access, so
	// re-analysis is required.
	m_bAnalyseRequired = true;
	return m_strBuffer[n];
}

void CUniBuffer::Clear() {
	m_strBuffer.clear();
	m_bAnalyseRequired = true;
}

bool CUniBuffer::InsertChar(size_t nIndex, TCHAR wChar) {
	// Valid Index ?
	if (nIndex > m_strBuffer.size()) {
		LogError("Invalid Index");
		return false;
	}

	// Check for maximum length allowed
	if (GetTextSize() + 1 >= DXUT_MAX_EDITBOXLENGTH)
		return false;

	m_strBuffer.insert(nIndex, 1, wChar);

	m_bAnalyseRequired = true;

	return true;
}

bool CUniBuffer::RemoveChar(size_t nIndex, size_t count) {
	// Valid Index ?
	if (nIndex >= m_strBuffer.size()) {
		LogError("Invalid Index");
		return false;
	}

	m_strBuffer.erase(nIndex, count);
	m_bAnalyseRequired = true;

	return true;
}

bool CUniBuffer::InsertString(size_t nIndex, const wchar_t* str, int nCount_) {
	// Valid Index ?
	if (nIndex > m_strBuffer.size()) {
		LogError("Invalid Index");
		return false;
	}

	size_t nCount = nCount_ < 0 ? wcslen(str) : nCount_;

	// Check for maximum length allowed
	if (m_strBuffer.size() + nCount >= DXUT_MAX_EDITBOXLENGTH)
		return false;

	m_strBuffer.insert(nIndex, str, nCount);
	m_bAnalyseRequired = true;

	return true;
}

bool CUniBuffer::SetText(const wchar_t* str) {
	size_t nCount = wcslen(str);

	// Check for maximum length allowed
	if (nCount >= DXUT_MAX_EDITBOXLENGTH)
		return false;

	m_strBuffer = str;
	m_bAnalyseRequired = true;

	return true;
}

HRESULT CUniBuffer::CPtoX(int nCP, BOOL bTrail, int* pX) {
	assert(pX);
	*pX = 0; // Default

	// ScriptStringCPtoX can crash if called with out-of-range arguments
	// so make sure that nCP is within [0, m_nTextSize]

	// Negative character pos doesn't make sense
	if (nCP < 0)
		return E_INVALIDARG;

	// Clamp nCP to m_nTextSize
	if (nCP > (int)m_strBuffer.size())
		nCP = (int)m_strBuffer.size();

	HRESULT hr = S_OK;
	if (m_bAnalyseRequired)
		hr = Analyse();

	if (SUCCEEDED(hr))
		hr = _ScriptStringCPtoX(m_Analysis, nCP, bTrail, pX);

	return hr;
}

HRESULT CUniBuffer::XtoCP(int nX, int* pCP, int* pnTrail) {
	assert(pCP && pnTrail);
	*pCP = 0;
	*pnTrail = FALSE; // Default

	HRESULT hr = S_OK;
	if (m_bAnalyseRequired)
		hr = Analyse();

	if (SUCCEEDED(hr))
		hr = _ScriptStringXtoCP(m_Analysis, nX, pCP, pnTrail);

	// If the coordinate falls outside the text region, we
	// can get character positions that don't exist.  We must
	// filter them here and convert them to those that do exist.
	if (*pCP == -1 && *pnTrail == TRUE) {
		*pCP = 0;
		*pnTrail = FALSE;
	} else if ((*pCP > (int)m_strBuffer.size()) && (*pnTrail == FALSE)) {
		*pCP = (int)m_strBuffer.size();
		*pnTrail = TRUE;
	}
	return hr;
}

void CUniBuffer::GetPriorItemPos(int nCP, int* pPrior) {
	*pPrior = nCP; // Default is the char itself

	if (m_bAnalyseRequired)
		if (FAILED(Analyse()))
			return;

	const SCRIPT_LOGATTR* pLogAttr = _ScriptString_pLogAttr(m_Analysis);
	if (!pLogAttr)
		return;

	if (!_ScriptString_pcOutChars(m_Analysis))
		return;
	int nInitial = *_ScriptString_pcOutChars(m_Analysis);
	if (nCP - 1 < nInitial)
		nInitial = nCP - 1;
	for (int i = nInitial; i > 0; --i) {
		if (pLogAttr[i].fWordStop || // Either the fWordStop flag is set
			(!pLogAttr[i].fWhiteSpace && // Or the previous char is whitespace but this isn't.
				pLogAttr[i - 1].fWhiteSpace)) {
			*pPrior = i;
			return;
		}
	}

	// We have reached index 0.  0 is always a break point, so simply return it.
	*pPrior = 0;
}

void CUniBuffer::GetNextItemPos(int nCP, int* pPrior) {
	*pPrior = nCP; // Default is the char itself

	HRESULT hr = S_OK;
	if (m_bAnalyseRequired)
		hr = Analyse();
	if (FAILED(hr))
		return;

	const SCRIPT_LOGATTR* pLogAttr = _ScriptString_pLogAttr(m_Analysis);
	if (!pLogAttr)
		return;

	if (!_ScriptString_pcOutChars(m_Analysis))
		return;
	int nInitial = *_ScriptString_pcOutChars(m_Analysis);
	if (nCP + 1 < nInitial)
		nInitial = nCP + 1;
	for (int i = nInitial; i < *_ScriptString_pcOutChars(m_Analysis) - 1; ++i) {
		if (pLogAttr[i].fWordStop) { // Either the fWordStop flag is set
			*pPrior = i;
			return;
		} else if (pLogAttr[i].fWhiteSpace && // Or this whitespace but the next char isn't.
				   !pLogAttr[i + 1].fWhiteSpace) {
			*pPrior = i + 1; // The next char is a word stop
			return;
		}
	}
	// We have reached the end. It's always a word stop, so simply return it.
	*pPrior = *_ScriptString_pcOutChars(m_Analysis) - 1;
}

// Helper class that help us automatically initialize and uninitialize external API.
// Important: C++ does not guaranteed the order global and static objects are
//            initialized in.  Therefore, do not use edit controls inside
//            a constructor.
class CExternalApiInitializer {
public:
	CExternalApiInitializer() {
		CUniBuffer::InitializeUniscribe();
	}

	~CExternalApiInitializer() {
		CUniBuffer::UninitializeUniscribe();
	}
} EXTERNAL_API_INITIALIZER;

// Static member initialization
HINSTANCE CUniBuffer::s_hDll = NULL;
HRESULT(WINAPI* CUniBuffer::_ScriptApplyDigitSubstitution)
(const SCRIPT_DIGITSUBSTITUTE*, SCRIPT_CONTROL*, SCRIPT_STATE*) = Dummy_ScriptApplyDigitSubstitution;
HRESULT(WINAPI* CUniBuffer::_ScriptStringAnalyse)
(HDC, const void*, int, int, int, DWORD, int, SCRIPT_CONTROL*, SCRIPT_STATE*,
	const int*, SCRIPT_TABDEF*, const BYTE*, SCRIPT_STRING_ANALYSIS*) = Dummy_ScriptStringAnalyse;
HRESULT(WINAPI* CUniBuffer::_ScriptStringCPtoX)
(SCRIPT_STRING_ANALYSIS, int, BOOL, int*) = Dummy_ScriptStringCPtoX;
HRESULT(WINAPI* CUniBuffer::_ScriptStringXtoCP)
(SCRIPT_STRING_ANALYSIS, int, int*, int*) = Dummy_ScriptStringXtoCP;
HRESULT(WINAPI* CUniBuffer::_ScriptStringFree)
(SCRIPT_STRING_ANALYSIS*) = Dummy_ScriptStringFree;
const SCRIPT_LOGATTR*(WINAPI* CUniBuffer::_ScriptString_pLogAttr)(SCRIPT_STRING_ANALYSIS) = Dummy_ScriptString_pLogAttr;
const int*(WINAPI* CUniBuffer::_ScriptString_pcOutChars)(SCRIPT_STRING_ANALYSIS) = Dummy_ScriptString_pcOutChars;

// When scrolling, EDITBOX_SCROLLEXTENT is reciprocal of the amount to scroll.
// If EDITBOX_SCROLLEXTENT = 4, then we scroll 1/4 of the control each time.
#define EDITBOX_SCROLLEXTENT 4

#define EDITBOX_TEXT_ROWS                   \
	TEXT_ROWS& textRows = m_textRowsRender; \
	if (GetEditable())                      \
		textRows = m_textRows;              \
	auto rows = textRows.size();            \
	rows;

CDXUTEditBox::CDXUTEditBox(CDXUTDialog* pDialog, bool typeSimple) :
		CDXUTControl(pDialog), m_typeSimple(typeSimple), m_ScrollBar(pDialog) {
	m_ScrollBar.SetParent(this);

	m_pDialog = pDialog;

	if (typeSimple) {
		m_Type = DXUT_CONTROL_SIMPLEEDITBOX;
		m_Tag = SIMPLE_EDIT_BOX_TAG;
		m_nBorder = 0;
		m_elementTags.clear();
		m_elementTags[TEXT_AREA_DISPLAY_TAG] = ElementTagMapValue(0, DXUT_STATE_FLAG_NORMAL, DXUT_STATE_FLAG_NORMAL | DXUT_STATE_FLAG_DISABLED);
	} else {
		m_Type = DXUT_CONTROL_EDITBOX;
		m_Tag = EDIT_BOX_TAG;
		m_nBorder = 5;
		m_elementTags.clear();
		m_elementTags[TEXT_AREA_DISPLAY_TAG] = ElementTagMapValue(0, DXUT_STATE_FLAG_NORMAL, DXUT_STATE_FLAG_NORMAL | DXUT_STATE_FLAG_DISABLED);
		m_elementTags[TOP_LEFT_BORDER_DISPLAY_TAG] = ElementTagMapValue(1, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NORMAL | DXUT_STATE_FLAG_DISABLED);
		m_elementTags[TOP_BORDER_DISPLAY_TAG] = ElementTagMapValue(2, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NORMAL | DXUT_STATE_FLAG_DISABLED);
		m_elementTags[TOP_RIGHT_BORDER_DISPLAY_TAG] = ElementTagMapValue(3, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NORMAL | DXUT_STATE_FLAG_DISABLED);
		m_elementTags[LEFT_BORDER_DISPLAY_TAG] = ElementTagMapValue(4, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NORMAL | DXUT_STATE_FLAG_DISABLED);
		m_elementTags[RIGHT_BORDER_DISPLAY_TAG] = ElementTagMapValue(5, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NORMAL | DXUT_STATE_FLAG_DISABLED);
		m_elementTags[LOWER_LEFT_BORDER_DISPLAY_TAG] = ElementTagMapValue(6, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NORMAL | DXUT_STATE_FLAG_DISABLED);
		m_elementTags[LOWER_BORDER_DISPLAY_TAG] = ElementTagMapValue(7, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NORMAL | DXUT_STATE_FLAG_DISABLED);
		m_elementTags[LOWER_RIGHT_BORDER_DISPLAY_TAG] = ElementTagMapValue(8, DXUT_STATE_FLAG_NONE, DXUT_STATE_FLAG_NORMAL | DXUT_STATE_FLAG_DISABLED);
	}

	m_defaultName = EDIT_BOX_DEFAULT_NAME;
	m_defaultText = L""; // for now, don't set default text EDIT_BOX_DEFAULT_TEXT;

	m_nSpacing = 4; // Default spacing
	m_nSBWidth = 16;
	m_nRowHeight = 0;

	m_bCaretOn = true;
	m_dfBlink = GetCaretBlinkTime() * 0.001f;
	m_dfLastBlink = DXUTGetGlobalTimer()->GetAbsoluteTime();
	m_nFirstVisible = 0;
	m_TextColor = D3DCOLOR_ARGB(255, 16, 16, 16);
	m_SelTextColor = D3DCOLOR_ARGB(255, 255, 255, 255);
	m_SelBkColor = D3DCOLOR_ARGB(255, 40, 50, 92);
	m_CaretColor = D3DCOLOR_ARGB(255, 0, 0, 0);
	m_bInsertMode = true;
	m_bMouseDrag = false;

	SetMaxCharCount(0);

	SetPassword(false);

	SetEditable(true);

	SetHideCaret(false);

	m_strSetText = L"";
	m_sCurrentRenderedRow = L"";

	m_nMaxRows = 1;

	// Without This We Crash Switching Between Single & Multi-line
	m_textRows.push_back(L"");
	m_textRowsRender.push_back(L"");

	SetCaretPosAbs(0);
	SetSelectPosAbs(0);
}

CDXUTEditBox::~CDXUTEditBox() {}

bool CDXUTEditBox::Load(TiXmlNode* edit_box_root) {
	bool bSuccess = false;

	if (edit_box_root) {
		//Load the Super Class Controls properties so that all of the EditBoxes dimensions have been loaded
		//for proper bounds checking
		bSuccess = CDXUTControl::Load(edit_box_root);

		//Store the text for reloading after all the other properties have been loaded
		std::string sText = "";
		// given the editbox root, iterate over the children.
		TiXmlNode* child = 0;
		for (child = edit_box_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, TEXT_TAG)) {
				if (GetStringFromXMLNode(child, sText))
					SetText(sText.c_str());
			} else if (!_stricmp(val, PASSWORD_TAG)) {
				bool bPassword = false;
				if (GetBoolFromXMLNode(child, bPassword))
					SetPassword(bPassword);
			} else if (!_stricmp(val, HIDE_CARET_TAG)) {
				bool bHideCaret = false;
				if (GetBoolFromXMLNode(child, bHideCaret))
					SetHideCaret(bHideCaret);
			} else if (!_stricmp(val, BORDER_WIDTH_TAG)) {
				int width = -1;
				if (GetIntFromXMLNode(child, width))
					SetBorderWidth(width);
			} else if (!_stricmp(val, SPACING_TAG)) {
				int spacing = -1;
				if (GetIntFromXMLNode(child, spacing))
					SetSpacing(spacing);
			} else if (!_stricmp(val, TEXT_COLOR_TAG)) {
				D3DCOLOR color;
				if (GetDWORDFromXMLNode(child, color))
					SetTextColor(color);
			} else if (!_stricmp(val, SELECTED_TEXT_COLOR_TAG)) {
				D3DCOLOR color;
				if (GetDWORDFromXMLNode(child, color))
					SetSelectedTextColor(color);
			} else if (!_stricmp(val, SELECTED_BACK_COLOR_TAG)) {
				D3DCOLOR color;
				if (GetDWORDFromXMLNode(child, color))
					SetSelectedBackColor(color);
			} else if (!_stricmp(val, CARET_COLOR_TAG)) {
				D3DCOLOR color;
				if (GetDWORDFromXMLNode(child, color))
					SetCaretColor(color);
			} else if (!_stricmp(val, EDITABLE_TAG)) {
				bool bEditable = false;
				if (GetBoolFromXMLNode(child, bEditable))
					SetEditable(bEditable);
			} else if (!_stricmp(val, SCROLL_BAR_TAG)) {
				bool enable = false;
				if (GetBoolFromXMLNode(child, enable))
					SetScrollBar(enable);
			}
		}
		//reset the text now that all the properties (multiline/editable) have been set
		SetText(sText.c_str());
	}

	return bSuccess;
}

bool CDXUTEditBox::Save(TiXmlNode* edit_box_root) {
	bool bSuccess = false;

	if (edit_box_root) {
		TiXmlNode* child = edit_box_root->InsertEndChild(TiXmlElement(TEXT_TAG));
		if (child) {
			std::string sText = GetText();
			PutStringToXMLNode(child, sText);
		}

		child = edit_box_root->InsertEndChild(TiXmlElement(PASSWORD_TAG));
		if (child) {
			PutBoolToXMLNode(child, GetPassword());
		}

		child = edit_box_root->InsertEndChild(TiXmlElement(HIDE_CARET_TAG));
		if (child) {
			PutBoolToXMLNode(child, GetHideCaret());
		}

		child = edit_box_root->InsertEndChild(TiXmlElement(EDITABLE_TAG));
		if (child) {
			PutBoolToXMLNode(child, GetEditable());
		}

		child = edit_box_root->InsertEndChild(TiXmlElement(BORDER_WIDTH_TAG));
		if (child) {
			PutIntToXMLNode(child, GetBorderWidth());
		}

		child = edit_box_root->InsertEndChild(TiXmlElement(SPACING_TAG));
		if (child) {
			PutIntToXMLNode(child, GetSpacing());
		}

		child = edit_box_root->InsertEndChild(TiXmlElement(TEXT_COLOR_TAG));
		if (child) {
			PutDWORDToXMLNode(child, GetTextColor());
		}

		child = edit_box_root->InsertEndChild(TiXmlElement(SELECTED_TEXT_COLOR_TAG));
		if (child) {
			PutDWORDToXMLNode(child, GetSelectedTextColor());
		}

		child = edit_box_root->InsertEndChild(TiXmlElement(SELECTED_BACK_COLOR_TAG));
		if (child) {
			PutDWORDToXMLNode(child, GetSelectedBackColor());
		}

		child = edit_box_root->InsertEndChild(TiXmlElement(CARET_COLOR_TAG));
		if (child) {
			PutDWORDToXMLNode(child, GetCaretColor());
		}

		bSuccess = CDXUTControl::Save(edit_box_root);
	}

	return bSuccess;
}

// Scrolls the text string so that the last character is in view ... does not move
// caret.  If the string is smaller than the edit box, has no effect.
void CDXUTEditBox::ScrollToEnd() {
	int nXLast = 0;

	// Find the x-position of the leading edge of the null terminator
	HRESULT hr = m_Buffer.CPtoX(m_Buffer.GetTextSize(), FALSE, &nXLast);

	if (SUCCEEDED(hr)) {
		int nCPFirst = 0, // character index of new first char to show
			nIgnored = 0;

		// x-coordinate of character at text rect left boundary ...
		// don't pass a negative number to XtoCP, who knows what will happen
		int nXFirst = std::max(nXLast - RectWidth(m_rcText), 0);

		// Find the character position of the character at text rect left boundary
		hr = m_Buffer.XtoCP(nXFirst, &nCPFirst, &nIgnored);

		if (SUCCEEDED(hr)) {
			int nXTemp = 0;
			hr = m_Buffer.CPtoX(nCPFirst, FALSE, &nXTemp);

			if (SUCCEEDED(hr)) {
				// if character at nXFirst would span text rect left boundary, move forward one character
				if (nXTemp < nXFirst)
					nCPFirst++;
			}

			m_nFirstVisible = nCPFirst;
		}
	}
}

bool CDXUTEditBox::ClearText() {
	// Reset Caret & Selection
	SetCaretPosAbs(0);
	SetSelectPosAbs(0);

	// DRF - Added - Multi-line Reset
	m_textRows.clear();
	m_textRowsRender.clear();
	m_textRows.push_back(L"");
	m_textRowsRender.push_back(L"");

	m_Buffer.Clear();
	m_strOriginalText.clear();

	m_nFirstVisible = 0;
	m_ScrollBar.SetVisible(false);

	// DRF - Added - Multi-line Refresh
	if (IsMultiline()) {
		EDITBOX_TEXT_ROWS;
		TextRowsRefresh(textRows);
	}

	return true;
}

bool CDXUTEditBox::SetSelected(int startIndex, int endIndex) {
	// Default Selects All
	int numChars = m_Buffer.GetTextSize();
	if (startIndex < 0)
		startIndex = 0;
	if (startIndex > numChars)
		startIndex = numChars;
	if (endIndex < 0)
		endIndex = numChars;
	if (endIndex > numChars)
		endIndex = numChars;

#ifdef DRF_POS_ABS
	SetSelectPosAbs((size_t)startIndex);
	SetCaretPosAbs((size_t)endIndex);
#else
	// Set Selection (caret at the end)
	m_selectPosX = startIndex;
	m_caretPosX = endIndex;
#endif

	return true;
}

bool CDXUTEditBox::SetText(const char* szText, bool bSelected) {
	if (!szText)
		return false;

	std::wstring strTextW = Utf8ToUtf16(szText);
	if (m_bPassword)
		m_Buffer.SetText(std::wstring(strTextW.size(), PASSWORD_CHAR).c_str());
	else
		m_Buffer.SetText(strTextW.c_str());

	m_strOriginalText = strTextW;

	// Move the caret to the end of the text, and scroll the end of the text into view
#ifdef DRF_POS_ABS
	// DRF - TODO
	SetCaretPosX
#else
	m_caretPosX = m_Buffer.GetTextSize();
	if (!IsMultiline())
		ScrollToEnd();
	m_selectPosX = bSelected ? 0 : m_caretPosX;
#endif

		//New Multiline Code
		if (IsMultiline()) {
		std::wstring copyOfText = L"";
		copyOfText.append(strTextW);

		m_nFirstVisible = 0;

		//if we aren't editable we want to suck out the links to use
		//strings of the right length for selection
		if (!GetEditable()) {
			m_textRowsRender.clear();
			m_vLinks.clear();
			int nCount, token_length = 0;
			int tag = 0;
			BOOL white_space = FALSE;

			//Ankit ------ adding an int to account for multiple spaces together..... not used inside this function
			int noOfWhiteSpaces = 0;

			nCount = strTextW.size();
			m_strSetText.clear();
			std::wstring link = L"";
			size_t absolutePosition = 0;

			const wchar_t* wszText = strTextW.c_str();
			for (;;) {
				tag = CDXUTStatic::GetToken(&wszText, &nCount, &token_length, &white_space, &noOfWhiteSpaces);
				if (tag < 0)
					break;

				if (tag == tA) {
					LinkItem lItem;

					//grab the url from href=
					std::wstring sTemp;
					sTemp.append(wszText);
					size_t i = sTemp.find(L">");
					size_t i2 = 0;
					size_t start = 0;
					size_t len = 0;
					if (i != std::string::npos) {
						//Start after >
						start = i + 1;
						//find the matching </a>
						i2 = sTemp.find(L"</a>");
						if (i2 != std::string::npos)
							len = i2 - start;

						link = sTemp.substr(start, len);

						lItem.startText = absolutePosition + 1;
						lItem.lenText = len;
						lItem.linkText = link;

						lItem.fullUrl = sTemp.substr(0, i2 + 4);

						m_vLinks.push_back(lItem);
					}

					if (white_space == 1) {
						m_strSetText.append(L" ");
						absolutePosition += len + 1;
					} else {
						absolutePosition += len;
					}

					m_strSetText.append(link);
					wszText += token_length + len + 4;
					nCount = (int)_tcslen(wszText);
					white_space = FALSE;
					continue;
				}
				if (white_space == 1) {
					std::wstring temp = L" ";
					temp.append(wszText);
					m_strSetText.append(temp.c_str(), token_length + 1);
					absolutePosition += token_length + 1;

				} else {
					m_strSetText.append(wszText, token_length);
					absolutePosition += token_length;
				}
				wszText += token_length;
				white_space = FALSE;
			}

#ifdef DRF_POS_ABS
			SetCaretPosAbs(0);
			SetSelectPosAbs(0);
#else
			m_selectPosY = 0;
			SetCaretPosY(0);
			SetCaretPosX(0);
#endif

			// DRF - Add String To Rendered Rows
			m_textRowsRender.clear();
			TextRowsAddAtCaret(m_textRowsRender, m_strSetText);

#ifdef DRF_POS_ABS
			// Set Position At End
			auto posAbsMax = GetPosAbsMax();
			SetCaretPosAbs(posAbsMax);
			SetSelectPosAbs(posAbsMax);
#else
			//Set which rows are currently active based on the number of rows that can be shown
			if (m_textRowsRender.size() > m_nMaxRows) {
				m_caretPosY = m_nMaxRows - 1;
				m_selectPosY = m_nMaxRows - 1;
				m_caretPosX = (int)m_textRowsRender[m_caretPosY].length();
				m_selectPosX = m_caretPosX;
			} else {
				m_caretPosY = m_textRowsRender.size() - 1;
				m_selectPosY = m_textRowsRender.size() - 1;
				m_caretPosX = (int)m_textRowsRender[m_textRowsRender.size() - 1].length();
				m_selectPosX = m_caretPosX;
			}
#endif
		}

		m_strSetText = copyOfText;

#ifdef DRF_POS_ABS
		// Set Position At Start
		SetCaretPosAbs(0);
		SetSelectPosAbs(0);
#else
		if (GetEditable())
			m_selectPosY = 0;
		SetCaretPosY(0);
		SetCaretPosX(0);
#endif

		// DRF - Add String To Editable Rows (automatically splits)
		m_textRows.clear();
		TextRowsAddAtCaret(m_textRows, m_strSetText);

#ifdef DRF_POS_ABS
		// TODO ?
#else
		//Set which rows are currently active based on the number of rows that can be shown
		if (GetEditable()) {
			if (m_textRows.size() > m_nMaxRows) {
				if (m_nMaxRows != 0) {
					m_caretPosY = m_nMaxRows - 1;
					m_selectPosY = m_nMaxRows - 1;
				} else {
					m_caretPosY = 0;
					m_selectPosY = 0;
				}
				m_caretPosX = (int)m_textRows[m_caretPosY].length();
				m_selectPosX = m_caretPosX;
			} else {
				m_caretPosY = m_textRows.size() - 1;
				m_selectPosY = m_textRows.size() - 1;
				m_caretPosX = (int)m_textRows[m_textRows.size() - 1].length();
				m_selectPosX = m_caretPosX;
			}

			m_Buffer.SetText(m_textRows[GetCaretTextRow()].c_str());
		} else {
			m_Buffer.SetText(m_textRowsRender[GetCaretTextRow()].c_str());
		}
#endif

		ScrollBarUpdate();
	}

	return true;
}

const char* CDXUTEditBox::GetText() {
	if (IsMultiline()) {
		m_sBuilderString = Utf16ToUtf8(TextRowsGetStrAbs(m_textRows));
		return m_sBuilderString.c_str();
	} else {
		m_sBuilderString = Utf16ToUtf8(m_Buffer.GetString());
		return m_sBuilderString.c_str();
	}
}

const char* CDXUTEditBox::GetTextOrig() {
	m_sBuilderString = Utf16ToUtf8(m_strOriginalText);
	return m_sBuilderString.c_str();
}

bool CDXUTEditBox::SetTextWithDefault() {
	// for now, don't set any default text
	return true;
}

HRESULT CDXUTEditBox::GetTextCopy(char* strDest, UINT bufferCount) {
	if (strDest) {
		std::string txt = Utf16ToUtf8(m_Buffer.GetString());
		strncpy(strDest, txt.c_str(), bufferCount - 1);
		strDest[bufferCount - 1] = '\0';
	}
	return S_OK;
}

HRESULT CDXUTEditBox::GetTextOrigCopy(char* strDest, UINT bufferCount) {
	if (strDest) {
		std::string txt = Utf16ToUtf8(m_strOriginalText);
		strncpy(strDest, txt.c_str(), bufferCount - 1);
		strDest[bufferCount - 1] = '\0';
	}
	return S_OK;
}

bool CDXUTEditBox::DeleteSelected(bool sendEvent) {
	if (!GetEditable())
		return false;

	// Valid Selection ?
	if (!ValidSelection())
		return false;

	// DRF - Added - Multiline
	if (!IsMultiline()) {
		size_t nFirst = std::min(m_caretPosX, m_selectPosX);
		size_t nLast = std::max(m_caretPosX, m_selectPosX);

		// Update caret and selection
		SetCaretPosX(nFirst);
		m_selectPosX = m_caretPosX;

		// Remove the characters
		m_Buffer.RemoveChar(nFirst, nLast - nFirst);
		m_strOriginalText.erase(nFirst, nLast - nFirst);

	} else { // DRF - Added - Multiline

		// Delete Selection ?
		size_t selPosAbs = 0;
		size_t selNumAbs = 0;
		if (!GetSelectPosAbs(selPosAbs, selNumAbs))
			return false;

		// Delete Absolutely
		TextRowsDelAbs(m_textRows, selPosAbs, selNumAbs);
	}

	// Send EditBoxChange Event ?
	if (sendEvent)
		m_pDialog->SendEvent(EVENT_EDITBOX_CHANGE, true, this);

	return true;
}

void CDXUTEditBox::UpdateRects() {
	CDXUTControl::UpdateRects();

	// Update the text rectangle
	m_rcText = m_rcBoundingBox;
	// First inflate by m_nBorder to compute render rects
	InflateRect(&m_rcText, -m_nBorder, -m_nBorder);

	// Update the render rectangles
	m_rcRender[0] = m_rcText;
	SetRect(&m_rcRender[1], m_rcBoundingBox.left, m_rcBoundingBox.top, m_rcText.left, m_rcText.top);
	SetRect(&m_rcRender[2], m_rcText.left, m_rcBoundingBox.top, m_rcText.right, m_rcText.top);
	SetRect(&m_rcRender[3], m_rcText.right, m_rcBoundingBox.top, m_rcBoundingBox.right, m_rcText.top);
	SetRect(&m_rcRender[4], m_rcBoundingBox.left, m_rcText.top, m_rcText.left, m_rcText.bottom);
	SetRect(&m_rcRender[5], m_rcText.right, m_rcText.top, m_rcBoundingBox.right, m_rcText.bottom);
	SetRect(&m_rcRender[6], m_rcBoundingBox.left, m_rcText.bottom, m_rcText.left, m_rcBoundingBox.bottom);
	SetRect(&m_rcRender[7], m_rcText.left, m_rcText.bottom, m_rcText.right, m_rcBoundingBox.bottom);
	SetRect(&m_rcRender[8], m_rcText.right, m_rcText.bottom, m_rcBoundingBox.right, m_rcBoundingBox.bottom);

	// Inflate further by m_nSpacing
	InflateRect(&m_rcText, -m_nSpacing, -m_nSpacing);

	assert(m_pDialog);

	if (m_pDialog != NULL) {
		CDXUTFontNode* pFontNode = m_pDialog->GetFont(m_Elements[0]->GetFontIndex());
		if (pFontNode && pFontNode->pFont && pFontNode->pFont->GetDC()) {
			TEXTMETRIC metric;
			GetTextMetrics(pFontNode->pFont->GetDC(), &metric);

			int heightOfLine = metric.tmHeight + metric.tmExternalLeading;

			//determine pagesize based on font height and drawable region
			int rows = RectHeight(m_rcText) / heightOfLine > 0 ? RectHeight(m_rcText) / heightOfLine : 1;

			m_nRowHeight = metric.tmHeight;
			m_nMaxRows = rows;

			m_ScrollBar.SetPageSize(rows);
		}
	}

	// Update the scrollbar's rects
	m_ScrollBar.SetLocation(m_rcText.right - m_nSBWidth, m_rcText.top);
	m_ScrollBar.SetSize(m_nSBWidth, RectHeight(m_rcText));

	if (m_ScrollBar.GetShowThumb())
		m_rcText.right -= m_nSBWidth;

	m_rcBoundary = m_rcText;
}

// does not copy m_strOriginalText to clipboard
void CDXUTEditBox::CopySelection() {
	if (!ValidSelection()) {
		LogError("ValidSelected() FAILED");
		return;
	}

	// DRF - Added - Multi-line
	if (IsMultiline()) {
		EDITBOX_TEXT_ROWS;
		size_t posAbsS = GetSelectPosAbs();
		size_t posAbsC = GetCaretPosAbs();
		size_t posAbsMin = std::min(posAbsS, posAbsC);
		size_t posAbsMax = std::max(posAbsS, posAbsC);
		size_t numAbs = posAbsMax - posAbsMin;
		std::wstring str = TextRowsGetStrAbs(textRows, posAbsMin, numAbs);
		CUniBuffer buffer(str.c_str());
		CopyToClipboard(0, 0, buffer);
	} else {
		CopyToClipboard(m_selectPosX, m_caretPosX, m_Buffer);
	}
}

bool CDXUTEditBox::CopyToClipboard(size_t startIndex, size_t endIndex, CUniBuffer& cub) {
	// Valid Text To Copy ?
	const TCHAR* cubChar = cub.GetString();
	size_t cubSize = cub.GetTextSize();
	if (!cubChar || !cubSize) {
		LogError("NOTHING TO COPY");
		return false;
	}
	std::wstring str = cubChar;
	size_t strSize = str.size();

	// DRF - ED-497 - External Line Breaks Broken
	// Export Line Breaks
	STLStringSubstitute(str, LINE_BREAK_STR, EXT_LINE_BREAK_STR);

	// DRF - Copy Bug Fix
	// NOTE: startIndex is always the character you clicked down on and endIndex is the up click.
	// If you select text backwards these indexes will be backwards so we must swap them.
	size_t nFirst = std::min(startIndex, endIndex);
	size_t nLast = std::max(startIndex, endIndex);
	if (nFirst < 0)
		nFirst = 0;
	if (nFirst > strSize)
		nFirst = strSize;
	if (nLast < 0)
		nLast = 0;
	if ((nLast > strSize) || (nLast == 0))
		nLast = strSize;

	strSize = nLast - nFirst;
	if (!strSize)
		return false;

	if (!OpenClipboard(NULL)) {
		LogError("OpenClipboard() FAILED");
		return false;
	}

	EmptyClipboard();
	HGLOBAL hBlock = GlobalAlloc(GMEM_MOVEABLE, (strSize + 1) * sizeof(wchar_t));
	if (hBlock) {
		wchar_t* pwszText = (wchar_t*)GlobalLock(hBlock);
		if (pwszText) {
			std::copy(str.begin() + nFirst, str.begin() + (nFirst + strSize), pwszText);
			pwszText[strSize] = L'\0'; // Terminate it
			GlobalUnlock(hBlock);
		}
		SetClipboardData(CF_UNICODETEXT, hBlock);
	}
	CloseClipboard();
	if (hBlock)
		GlobalFree(hBlock);

	return true;
}

bool CDXUTEditBox::PasteFromClipboard() {
	if (!GetEditable())
		return false;

	if (!OpenClipboard(NULL)) {
		LogError("OpenClipboard() FAILED");
		return false;
	}

	HANDLE handle = GetClipboardData(CF_UNICODETEXT);
	if (!handle) {
		LogError("GetClipboardData() FAILED");
		CloseClipboard();
		return false;
	}

	// Convert the ANSI string to Unicode, then insert to our buffer.
	wchar_t* pwszText = (wchar_t*)GlobalLock(handle);
	if (!pwszText) {
		LogError("GlobalLock() FAILED");
		CloseClipboard();
		return false;
	}
	std::wstring str = pwszText;

	// DRF - ED-497 - External Line Breaks Broken
	// Import Line Breaks
	STLStringSubstitute(str, EXT_LINE_BREAK_STR, LINE_BREAK_STR);

	// Too Big ?
	size_t strSize = str.size();
	if (strSize > MAX_PASTE_SIZE) {
		LogError("MAX_PASTE_SIZE");
		CloseClipboard();
		return false;
	}

	// Delete Possible Selection (send edit box change event)
	DeleteSelected();

	// Paste Text In
	if (!IsMultiline()) {
		// Check and see if the clipboard text + the text already in the edit box
		// is greater than the MaxCharCount variable.
		size_t nAddedChars = strSize;
		size_t nOldSize = m_strOriginalText.size();
		size_t nMaxSize = GetMaxCharCount();
		if (nOldSize < nMaxSize)
			nAddedChars = std::min<size_t>(nMaxSize - nOldSize, nAddedChars);
		else
			nAddedChars = 0;

		if (nAddedChars > 0 && m_Buffer.InsertString(m_caretPosX, str.c_str(), nAddedChars)) {
			m_strOriginalText.insert(m_caretPosX, str.c_str(), nAddedChars);

			// Password '*' Replace ?
			if (m_bPassword)
				m_Buffer.SetText(std::wstring(m_strOriginalText.size(), PASSWORD_CHAR).c_str());

			SetCaretPosX(m_caretPosX + nAddedChars);
		}

		m_selectPosX = m_caretPosX;

	} else {
		TextRowsAddAtCaret(m_textRows, str);
	}

	GlobalUnlock(handle);

	CloseClipboard();

	return true;
}

bool CDXUTEditBox::HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if (!m_bEnabled || !GetVisible())
		return false;

	// Let the scroll bar have a chance to handle it first
	if (m_ScrollBar.HandleKeyboard(uMsg, wParam, lParam))
		return true;

	bool bHandled = false;

	switch (uMsg) {
		case WM_KEYDOWN: {
			// x is used for the key value
			// y is used for the state of the SHIFT, CTRL, or ALT keys
			POINT keyInfo = { (LONG)wParam, 0 };
			bool bShiftDown = ((GetAsyncKeyState(VK_SHIFT) & 0x8000) != 0);
			bool bControlDown = ((GetAsyncKeyState(VK_CONTROL) & 0x8000) != 0);
			bool bAltDown = ((GetAsyncKeyState(VK_MENU) & 0x8000) != 0);
			keyInfo.y |= bShiftDown ? 0x0001 : 0x0000;
			keyInfo.y |= bControlDown ? 0x0010 : 0x0000;
			keyInfo.y |= bAltDown ? 0x0100 : 0x0000;

			m_pDialog->SendEvent(EVENT_EDITBOX_KEYDOWN, true, this, keyInfo);

			HRESULT hr = S_OK;
			int nXFirst = 0;
			int nCPFirst = 0;
			int nNewTrail = 0;

			switch (wParam) {
				case VK_HOME: {
					SetCaretPosX(0);

					if (!bShiftDown)
						ResetSelection();

					ResetCaretBlink();
					bHandled = true;
				} break;

				case VK_END: {
					SetCaretPosX(m_Buffer.GetTextSize());

					if (!bShiftDown)
						ResetSelection();

					ResetCaretBlink();
					bHandled = true;
				} break;

				case VK_INSERT: {
					if (bControlDown)
						CopySelection();
					else if (bShiftDown)
						PasteFromClipboard();
					else
						m_bInsertMode = !m_bInsertMode;
				} break;

				case VK_DELETE: {
					// Shift + Delete Means Copy Before Delete
					if (bShiftDown)
						CopySelection();

					// DRF - Added - Multiline
					if (!IsMultiline()) {
						// Delete Selected Text Or Single Char
						if (!DeleteSelected()) {
							if (m_caretPosX < m_Buffer.GetTextSize()) {
								if (m_Buffer.RemoveChar(m_caretPosX, 1)) {
									m_strOriginalText.erase(m_caretPosX, 1);
									m_pDialog->SendEvent(EVENT_EDITBOX_CHANGE, true, this);
								}
							}
						}

					} else { // DRF - Added - Multiline

						// Assume Delete Caret Single Char Forward
						size_t caretPosAbs = GetCaretPosAbs();
						size_t delPosAbs = caretPosAbs;
						size_t delNumAbs = 1;

						// Delete Selection ?
						size_t selPosAbs = 0;
						size_t selNumAbs = 0;
						if (GetSelectPosAbs(selPosAbs, selNumAbs)) {
							delPosAbs = selPosAbs;
							delNumAbs = selNumAbs;
						}

						// Delete Absolutely
						TextRowsDelAbs(m_textRows, delPosAbs, delNumAbs);

						m_pDialog->SendEvent(EVENT_EDITBOX_CHANGE, true, this);
					}

					ResetCaretBlink();
					bHandled = true;
				} break;

				case VK_LEFT: {
					// Move Caret Backward
					if (!IsMultiline()) {
						if (m_caretPosX > 0)
							SetCaretPosX(m_caretPosX - 1);
					} else { // DRF - Added - Multiline
						auto posAbs = GetCaretPosAbs();
						posAbs = posAbs ? (posAbs - 1) : 0;
						SetCaretPosAbs(posAbs);
					}

					if (!bShiftDown)
						ResetSelection();

					ResetCaretBlink();
					bHandled = true;
				} break;

				case VK_RIGHT: {
					// Move Caret Forward
					if (!IsMultiline()) {
						if ((int)m_caretPosX < m_Buffer.GetTextSize())
							SetCaretPosX(m_caretPosX + 1);
					} else { // DRF - Added - Multiline
						auto posAbs = GetCaretPosAbs() + 1;
						auto posAbsMax = GetPosAbsMax();
						posAbs = std::min(posAbs, posAbsMax);
						SetCaretPosAbs(posAbs);
					}

					if (!bShiftDown)
						ResetSelection();

					ResetCaretBlink();
					bHandled = true;
				} break;

				case VK_UP: {
					if (IsMultiline() && GetEditable()) {
						if (GetCaretTextRow() - 1 >= 0 && GetCaretTextRow() != 0) {
							if (m_caretPosY == 0) {
								m_ScrollBar.Scroll(-1);
							} else {
								m_caretPosY--;
							}

							// Update ScrollBar
							ScrollBarUpdate();

							//get previous x position of caret
							hr = m_Buffer.CPtoX(m_caretPosX, FALSE, &nXFirst);
							m_Buffer.SetText(m_textRows[GetCaretTextRow()].c_str());
							if (SUCCEEDED(hr)) {
								hr = m_Buffer.XtoCP(nXFirst, &nCPFirst, &nNewTrail);
								if (SUCCEEDED(hr)) {
									m_caretPosX = nCPFirst;

									// DRF - Allow Up Arrow Through Line Breaks
									if (m_caretPosX && (m_Buffer[m_caretPosX - 1] == LINE_BREAK_CHAR)) {
										--m_caretPosX;
									}
								}
							}
						}
					}

					if (!bShiftDown)
						ResetSelection();

					ResetCaretBlink();
					bHandled = true;
				} break;

				case VK_DOWN: {
					if (IsMultiline() && GetEditable()) {
						if (GetCaretTextRow() + 1 < m_textRows.size()) {
							if (m_caretPosY + 1 >= m_nMaxRows) {
								m_ScrollBar.Scroll(1);
							} else {
								m_caretPosY++;
							}

							// Update ScrollBar
							ScrollBarUpdate();

							//get previous x position of caret
							hr = m_Buffer.CPtoX(m_caretPosX, FALSE, &nXFirst);
							m_Buffer.SetText(m_textRows[GetCaretTextRow()].c_str());
							if (SUCCEEDED(hr)) {
								hr = m_Buffer.XtoCP(nXFirst, &nCPFirst, &nNewTrail);
								if (SUCCEEDED(hr)) {
									m_caretPosX = nCPFirst;

									// DRF - Allow Down Arrow Through Line Breaks
									if (m_caretPosX && (m_Buffer[m_caretPosX - 1] == LINE_BREAK_CHAR)) {
										--m_caretPosX;
									}
								}
							}
						}
					}

					if (!bShiftDown)
						ResetSelection();

					ResetCaretBlink();
					bHandled = true;
				} break;

				default:

					// Allow Dialog To Handle ESC & TAB
					bHandled = (wParam != VK_ESCAPE) && (wParam != VK_TAB);
					break;
			}
		}
	}

	return bHandled;
}

bool CDXUTEditBox::HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam) {
	if (!CDXUTControl::HandleMouse(uMsg, pt, wParam, lParam)) {
		if (!m_bEnabled || !GetVisible())
			return false;

		// Let the scroll bar handle it first.
		if (m_ScrollBar.HandleMouse(uMsg, pt, wParam, lParam))
			return true;

		switch (uMsg) {
			case WM_LBUTTONDOWN: {
				if (!GetIsInFocus())
					m_pDialog->RequestFocus(this);

				if (!ContainsPoint(pt))
					return false;

				//mark mouse click
				m_LastTouch = pt;

				if (IsMultiline()) {
					//which visible row was clicked on
					m_nRowHeight != 0 ? (m_caretPosY = (pt.y - m_rcText.top) / m_nRowHeight) : m_caretPosY = 0;

					//double check that we arent out of bounds
					if (m_caretPosY > m_nMaxRows - 1)
						m_caretPosY = m_nMaxRows - 1;

					size_t index = GetCaretTextRow();

					if (GetEditable()) {
						//make sure there is actual text on the row selected
						if (m_caretPosY > m_textRows.size() - 1)
							m_caretPosY = m_textRows.size() - 1;

						if (index <= m_textRows.size() - 1)
							m_Buffer.SetText(m_textRows[index].c_str());
						else
							m_Buffer.SetText(L"");
					} else {
						//make sure there is actual text on the row selected
						if (m_caretPosY > m_textRowsRender.size() - 1)
							m_caretPosY = m_textRowsRender.size() - 1;

						if (index <= m_textRowsRender.size() - 1)
							m_Buffer.SetText(m_textRowsRender[index].c_str());
						else
							m_Buffer.SetText(L"");
					}
				}

				m_bMouseDrag = true;
				SetCapture(DXUTGetHWND());
				// Determine the character corresponding to the coordinates.
				int nCP, nTrail, nX1st;
				m_Buffer.CPtoX(m_nFirstVisible, FALSE, &nX1st); // X offset of the 1st visible char
				if (SUCCEEDED(m_Buffer.XtoCP(pt.x - m_rcText.left + nX1st, &nCP, &nTrail))) {
					// Cap at the NULL character.
					if (nTrail && (nCP < (int)m_Buffer.GetTextSize()))
						SetCaretPosX(nCP + 1);
					else
						SetCaretPosX(nCP);

#ifdef DRF_POS_ABS
					SetSelectPosAbs(GetCaretPosAbs());
#else
					m_selectPosX = m_caretPosX;
					m_selectPosY = m_caretPosY;
#endif

					ResetCaretBlink();
				}
				return true;
			}

			case WM_LBUTTONUP:
				ReleaseCapture();
				m_bMouseDrag = false;
				break;

			case WM_LBUTTONDBLCLK: // DRF - Added
				if (!GetIsInFocus())
					m_pDialog->RequestFocus(this);
				if (!ContainsPoint(pt))
					return false;
				SetSelected();
				break;

			case WM_MOUSEMOVE:

				if (m_bMouseDrag) {
					//what row did we end on?
					if (IsMultiline()) {
						//did we scroll past what is visible?
						size_t past = m_ScrollBar.GetTrackPos() + m_nMaxRows;

						if (m_caretPosY > past) {
							//we need to move the scrollbar now
							m_ScrollBar.Scroll(1);
						}

						//which visible row was clicked on
						m_nRowHeight != 0 ? (m_caretPosY = (pt.y - m_rcText.top) / m_nRowHeight) : m_caretPosY = 0;

						//double check that we arent out of bounds
						if (m_caretPosY > m_nMaxRows - 1)
							m_caretPosY = m_nMaxRows - 1;

						size_t index = GetCaretTextRow();

						if (GetEditable()) {
							//make sure there is actual text on the row selected
							if (m_caretPosY > m_textRows.size() - 1)
								m_caretPosY = m_textRows.size() - 1;

							if (index <= m_textRows.size() - 1)
								m_Buffer.SetText(m_textRows[index].c_str());
							else
								m_Buffer.SetText(L"");
						} else {
							//make sure there is actual text on the row selected
							if (m_caretPosY > m_textRowsRender.size() - 1)
								m_caretPosY = m_textRowsRender.size() - 1;

							if (index <= m_textRowsRender.size() - 1)
								m_Buffer.SetText(m_textRowsRender[index].c_str());
							else
								m_Buffer.SetText(L"");
						}
					}

					// Determine the character corresponding to the coordinates.
					int nCP, nTrail, nX1st;
					m_Buffer.CPtoX(m_nFirstVisible, FALSE, &nX1st); // X offset of the 1st visible char
					if (SUCCEEDED(m_Buffer.XtoCP(pt.x - m_rcText.left + nX1st, &nCP, &nTrail))) {
						// Cap at the NULL character.
						if (nTrail && (nCP < (int)m_Buffer.GetTextSize()))
							SetCaretPosX(nCP + 1);
						else
							SetCaretPosX(nCP);
					}
				}
				break;
		}

		return false;
	}

	return true;
}

void CDXUTEditBox::OnFocusIn() {
	CDXUTControl::OnFocusIn();
	ResetCaretBlink();

	// DRF - Added
	if (m_pDialog)
		m_pDialog->SetCaptureKeys(true);
}

void CDXUTEditBox::OnFocusOut() {
	CDXUTControl::OnFocusOut();

	// DRF - Added
	if (m_pDialog)
		m_pDialog->SetCaptureKeys(false);
}

bool CDXUTEditBox::MsgProc(UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if (!m_bEnabled || !GetVisible() || !m_pDialog->GetEnableKeyboardInput())
		return false;

	switch (uMsg) {
		case WM_CHAR: {
			wchar_t charValue = wParam;

			switch (charValue) {
				// Backspace
				case VK_BACK: {
					if (!GetEditable())
						break;

					// DRF - Added - Multiline
					if (!IsMultiline()) {
						// Delete Selected Text Or Single Char
						if (!DeleteSelected() && (m_caretPosX > 0)) {
							// Move the caret, then delete the char.
							SetCaretPosX(m_caretPosX - 1);
							m_selectPosX = m_caretPosX;
							m_Buffer.RemoveChar(m_caretPosX, 1);
							m_strOriginalText.erase(m_caretPosX, 1);
							m_pDialog->SendEvent(EVENT_EDITBOX_CHANGE, true, this);
						}

					} else { // DRF - Added - Multiline

						// Assume Delete Caret Single Char Backwards
						size_t caretPosAbs = GetCaretPosAbs();
						size_t delPosAbs = (caretPosAbs ? (caretPosAbs - 1) : 0);
						size_t delNumAbs = (caretPosAbs ? 1 : 0);

						// Delete Selection ?
						size_t selPosAbs = 0;
						size_t selNumAbs = 0;
						if (GetSelectPosAbs(selPosAbs, selNumAbs)) {
							delPosAbs = selPosAbs;
							delNumAbs = selNumAbs;
						}

						// Delete Absolutely
						TextRowsDelAbs(m_textRows, delPosAbs, delNumAbs);

						m_pDialog->SendEvent(EVENT_EDITBOX_CHANGE, true, this);
					}

					ResetCaretBlink();
				} break;

				case 24: { // Ctrl-X Cut
					CopySelection();
					if (!GetEditable())
						break;
					DeleteSelected();
				} break;

				case VK_CANCEL: { // Ctrl-C Copy
					CopySelection();
				} break;

					// Ctrl-V Paste
				case 22: {
					if (!GetEditable())
						break;
					PasteFromClipboard();
					m_pDialog->SendEvent(EVENT_EDITBOX_CHANGE, true, this);
				} break;

					// Ctrl-A Select All
				case 1: {
					// DRF - Added - Multiline
					if (!IsMultiline()) {
						m_selectPosX = 0;
						SetCaretPosX(m_Buffer.GetTextSize());
					} else { // DRF - Added - Multiline
						SetSelectPosAbs(0);
						SetCaretPosAbs(GetPosAbsMax());
					}
				} break;

					// Junk characters we don't want in the string
				case 26: // Ctrl Z
				case 2: // Ctrl B
				case 14: // Ctrl N
				case 19: // Ctrl S
				case 4: // Ctrl D
				case 6: // Ctrl F
				case 7: // Ctrl G
				case 10: // Ctrl J
				case 11: // Ctrl K
				case 12: // Ctrl L
				case 17: // Ctrl Q
				case 23: // Ctrl W
				case 5: // Ctrl E
				case 18: // Ctrl R
				case 20: // Ctrl T
				case 25: // Ctrl Y
				case 21: // Ctrl U
				case 9: // Ctrl I
				case 15: // Ctrl O
				case 16: // Ctrl P
				case 27: // Ctrl [
				case 29: // Ctrl ]
				case 28: // Ctrl \ (extra characters to prevent line continuation)
					break;

				default: {
					if (charValue == VK_RETURN) {
						// Non Multi-line Enter Means Do It
						if (!IsMultiline()) {
							m_pDialog->SendEvent(EVENT_EDITBOX_STRING, true, this);
							break;
						}

						// Use Line Break Character
						charValue = LINE_BREAK_CHAR;
					}

					if (!GetEditable())
						break;

					if (!IsMultiline()) {
						// Delete Possible Selection (send edit box change event)
						DeleteSelected();

						// If we are in overwrite mode and there is already
						// a char at the caret's position, simply replace it.
						// Otherwise, we insert the char as normal.
						if (!m_bInsertMode && m_caretPosX < m_Buffer.GetTextSize()) {
							m_strOriginalText[m_caretPosX] = charValue;

							if (m_bPassword)
								m_Buffer[m_caretPosX] = PASSWORD_CHAR;
							else
								m_Buffer[m_caretPosX] = charValue;

							SetCaretPosX(m_caretPosX + 1);
							m_selectPosX = m_caretPosX;
						} else {
							// Insert the char if the index is less than the MaxCharCount variable.
							if (m_caretPosX < GetMaxCharCount()) {
								bool inserted = m_Buffer.InsertChar(m_caretPosX, m_bPassword ? PASSWORD_CHAR : charValue);
								if (inserted) {
									m_strOriginalText.insert(m_caretPosX, 1, charValue);
									SetCaretPosX(m_caretPosX + 1);
									m_selectPosX = m_caretPosX;
								}
							}
						}
					} else {
						// Delete Possible Selection (send edit box change event)
						DeleteSelected();

						// Overwrite Mode ?
						if (!m_bInsertMode) {
							// Assume Delete Caret Single Char Forward
							size_t caretPosAbs = GetCaretPosAbs();
							size_t delPosAbs = (caretPosAbs ? caretPosAbs : 0);
							size_t delNumAbs = (caretPosAbs ? 1 : 0);

							// Delete Absolutely
							TextRowsDelAbs(m_textRows, delPosAbs, delNumAbs);
						}

						// Add Typed Character Into Text Row At Caret
						std::wstring str;
						str += charValue;
						TextRowsAddAtCaret(m_textRows, str);
					}

					ResetCaretBlink();

					m_pDialog->SendEvent(EVENT_EDITBOX_CHANGE, true, this);
				}
			}
			return true;
		}
	}
	return false;
}

void CDXUTEditBox::Render(float fElapsedTime) {
	DXUT_CONTROL_STATE iState = DXUT_STATE_NORMAL;

	// if editing, show control as 'normal'
	if (!GetVisible() && !m_pDialog->GetEdit())
		iState = DXUT_STATE_HIDDEN;
	else if (!m_bEnabled)
		iState = DXUT_STATE_DISABLED;

	if (iState == DXUT_STATE_HIDDEN) {
		CDXUTControl::Render(fElapsedTime);
		return;
	}

	size_t caretPosX = GetCaretPosX();
	size_t caretPosY = GetCaretPosY();
	size_t selPosX = GetSelectPosX();
	size_t selPosY = GetSelectPosY();

	size_t bufSize;
	if (IsMultiline()) {
		// Set Buffer Used For Caret Placement & Rendering
		SetBuffer(GetCaretTextRow());
	}
	bufSize = m_Buffer.GetTextSize();
	if (caretPosX > bufSize)
		caretPosX = bufSize;

	auto pFirstElement = (CDXUTElement*)GetElement(0);
	if (pFirstElement) {
		// Set Edit Box Font
		int idx = 0; // Assume Default Font 0
		if (m_pDialog)
			idx = m_pDialog->GetResourceManagerFontIndex(pFirstElement->GetFontIndex());
		if (IDX_ERROR(idx))
			idx = 0;
		m_Buffer.SetFontIndex(idx);

		// Call PlaceCaret now that we have the DC so scrolling can be handled.
		SetCaretPosX(caretPosX);
	}

	// Render the control graphics
	auto elementsSize = m_Elements.size();
	for (size_t e = 0; e < elementsSize; ++e) {
		CDXUTElement* pElement = m_Elements[e];
		(*(pElement->GetTextureColor())).Blend(iState, fElapsedTime);
		m_pDialog->DrawSprite(pElement, &m_rcRender[e]);
	}

	//Render the Selection Rectangle
	HRESULT hr;
	int nSelStartX = 0, nCaretX = 0; // Left and right X cordinates of the selection region

	//X coordinate of caret position on CurrentRow()
	hr = m_Buffer.CPtoX(caretPosX, FALSE, &nCaretX);

	// Compute the X coordinates of the first visible character. single line usage
	int nXFirst;
	m_Buffer.CPtoX(m_nFirstVisible, FALSE, &nXFirst);

	// Compute the X coordinates of the selection rectangle
	// x coordinate of starting caret position on SelectionStartedRow
	if (IsMultiline()) {
		// Set Buffer Used For Caret Placement & Rendering
		SetBuffer(GetSelectStartTextRow());

		//always find the x Coord of selection start caret with multiline
		hr = m_Buffer.CPtoX(selPosX, FALSE, &nSelStartX);

	} else {
		if (caretPosX != selPosX)
			hr = m_Buffer.CPtoX(selPosX, FALSE, &nSelStartX);
		else
			nSelStartX = nCaretX;
	}

	// Render the selection rectangle
	RECT rcSelection;
	if (GetIsInFocus() && ValidSelection()) {
		int nSelLeftX = nSelStartX, nSelRightX = nCaretX;

		if (IsMultiline()) {
			//swap if the selection started from the ground up or the selection is on the same line
			if (selPosY > caretPosY || (selPosY == caretPosY && nSelLeftX > nSelRightX)) {
				int nTemp = nSelLeftX;
				nSelLeftX = nSelRightX;
				nSelRightX = nTemp;
			}
		} else {
			// Swap if left is bigger than right
			if (nSelLeftX > nSelRightX) {
				int nTemp = nSelLeftX;
				nSelLeftX = nSelRightX;
				nSelRightX = nTemp;
			}
		}

		if (IsMultiline()) {
			size_t nCaretRow = caretPosY;
			size_t nStartRow = selPosY;
			int nCaret = caretPosX;
			int nStartCaret = selPosX;
			bool reswap = false;

			//swap if on same row but highlighted backwards
			if (selPosX > caretPosX && selPosY == caretPosY) {
				nCaret = selPosX;
				nStartCaret = caretPosX;
			}

			//swap these if the starting row is below the row of the caret
			if (selPosY > caretPosY) {
				nCaret = selPosX;
				nStartCaret = caretPosX;
				nCaretRow = selPosY;
				nStartRow = caretPosY;

				selPosY = caretPosY;
				caretPosY = nCaretRow;
				reswap = true;
			}

			if (selPosY == caretPosY) {
				size_t top = m_rcText.top + nCaretRow * m_nRowHeight;
				size_t bottom = m_rcText.top + (nCaretRow + 1) * m_nRowHeight;

				SetRect(&rcSelection, nSelLeftX, (int)top, nSelRightX, (int)bottom);
				OffsetRect(&rcSelection, m_rcText.left - nXFirst, 0);

				m_pDialog->DrawRect(&rcSelection, m_SelBkColor);
			} else {
				//Render at least the top row
				size_t startRow = nStartRow;
				size_t top = m_rcText.top + startRow * m_nRowHeight;
				size_t bottom = m_rcText.top + (startRow + 1) * m_nRowHeight;

				SetRect(&rcSelection, m_rcText.left + nSelLeftX, (int)top, m_rcText.right, (int)bottom);
				m_pDialog->DrawRect(&rcSelection, m_SelBkColor);

				//Render the middle rows we have scrolled past a whole row
				startRow++;
				while (startRow != nCaretRow) {
					top = m_rcText.top + startRow * m_nRowHeight;
					bottom = m_rcText.top + (startRow + 1) * m_nRowHeight;

					SetRect(&rcSelection, m_rcText.left, (int)top, m_rcText.right, (int)bottom);
					m_pDialog->DrawRect(&rcSelection, m_SelBkColor);

					startRow++;
				}

				//Render the bottom row where the caret is actually been placed
				top = m_rcText.top + startRow * m_nRowHeight;
				bottom = m_rcText.top + (startRow + 1) * m_nRowHeight;

				SetRect(&rcSelection, m_rcText.left, (int)top, m_rcText.left + nSelRightX, (int)bottom);
				m_pDialog->DrawRect(&rcSelection, m_SelBkColor);
			}

			if (reswap) {
				selPosY = nCaretRow;
				caretPosY = nStartRow;
			}
		} else {
			SetRect(&rcSelection, nSelLeftX, m_rcText.top, nSelRightX, m_rcText.bottom);
			OffsetRect(&rcSelection, m_rcText.left - nXFirst, 0);
			IntersectRect(&rcSelection, &m_rcText, &rcSelection);
			m_pDialog->DrawRect(&rcSelection, m_SelBkColor);
		}
	}

	// Render the text
	// Element 0 for text
	(*(m_Elements[0]->GetFontColor())).SetCurrentColor(m_TextColor);

	if (IsMultiline()) {
		m_Elements[0]->SetTextFormat(DT_WORDBREAK); // TODO: This might be the reason for v align problem

		std::wstring renderCopy = L"";

		//separate the rendering of editable and non-editable b/c  m_vRows is broken up by spaces on wordwrap
		//this alters links and they won't be rendered properly
		size_t count = 0;
		size_t row = m_ScrollBar.GetTrackPos();
		if (GetEditable()) {
			// Editable
			size_t rows = m_textRows.size();
			for (size_t i = row; i < rows; ++i) {
				if (count == m_nMaxRows)
					break;
				renderCopy.append(m_textRows[i]);
				RECT temp = {
					LONG(m_rcText.left),
					LONG((m_rcText.top + (count * m_nRowHeight))),
					LONG(m_rcText.right),
					LONG((m_rcText.top + ((count + 1) * m_nRowHeight)))
				};
				m_pDialog->DrawText(m_textRows[i].c_str(), m_Elements[0], &temp);
				++count;
			}

		} else {
			// Not Editable
			size_t rows = m_textRowsRender.size();
			for (size_t i = row; i < rows; ++i) {
				if (count == m_nMaxRows)
					break;
				renderCopy.append(m_textRowsRender[i]);
				++count;
			}

			//replace dummy links with full links
			for (auto it = m_vLinks.begin(); it != m_vLinks.end(); ++it) {
				renderCopy.erase(it->startText, it->lenText);
				renderCopy.insert(it->startText, it->fullUrl);
			}

			RECT temp = {
				m_rcText.left,
				m_rcText.top,
				m_rcText.right,
				m_rcText.bottom
			};
			DrawHTML(renderCopy.c_str(), m_Elements[0], &temp);
		}

		// Set Buffer Used For Caret Placement & Rendering
		SetBuffer(GetCaretTextRow());

	} else {
		// DRF - ED-497 - Single Line Spaces Broken
		if (GetEditable()) {
			(*(m_Elements[0]->GetFontColor())).SetCurrentColor(m_TextColor);
			m_pDialog->DrawText(m_Buffer.GetString() + m_nFirstVisible, m_Elements[0], &m_rcText, false);
		} else {
			DrawHTML(m_Buffer.GetString() + m_nFirstVisible, m_Elements[0], &m_rcText);
		}

		// Render the inverse color selected text if this control has the focus
		if (GetIsInFocus() && caretPosX != selPosX) {
			size_t nFirstToRender = std::max((size_t)m_nFirstVisible, std::min(selPosX, caretPosX));
			size_t nNumCharToRender = std::max(selPosX, caretPosX) - nFirstToRender;
			(*(m_Elements[0]->GetFontColor())).SetCurrentColor(m_SelTextColor);
			m_pDialog->DrawText(m_Buffer.GetString() + nFirstToRender, m_Elements[0], &rcSelection, false, (int)nNumCharToRender);
		}
	}

	// Blink the caret
	if (DXUTGetGlobalTimer()->GetAbsoluteTime() - m_dfLastBlink >= m_dfBlink) {
		m_bCaretOn = !m_bCaretOn;
		m_dfLastBlink = DXUTGetGlobalTimer()->GetAbsoluteTime();
	}

	// Render the caret if this control has the focus
	if (GetIsInFocus() && m_bCaretOn && !GetHideCaret() && GetEditable()) {
		//calculate the dimensions of the caret
		RECT rcCaret = {
			m_rcText.left - nXFirst + nCaretX - 1,
			m_rcText.top + ((int)caretPosY * m_nRowHeight),
			m_rcText.left - nXFirst + nCaretX + 1,
			m_rcText.top + (((int)caretPosY + 1) * m_nRowHeight)
		};

		// If we are in overwrite mode, adjust the caret rectangle
		// to fill the entire character.
		if (!m_bInsertMode) {
			// Obtain the right edge X coord of the current character
			int nRightEdgeX;
			m_Buffer.CPtoX(caretPosX, TRUE, &nRightEdgeX);
			rcCaret.right = m_rcText.left - nXFirst + nRightEdgeX;
		}

		m_pDialog->DrawRect(&rcCaret, m_CaretColor);
	}

	// Render the scroll bar
	// (if it needs to be shown)
	if (m_ScrollBar.GetShowThumb())
		m_ScrollBar.Render(fElapsedTime);

	CDXUTControl::Render(fElapsedTime);
}

#define IN_FLOAT_CHARSET(c) \
	((c) == _T('-') || (c) == _T('.') || ((c) >= _T('0') && (c) <= _T('9')))

bool CDXUTEditBox::ParseFloatArray(float* pNumbers, int nCount) {
	int nWritten = 0; // Number of floats written
	const TCHAR *pToken, *pEnd;
	TCHAR wszToken[60] = L"";

	pToken = m_Buffer.GetString();
	while (nWritten < nCount && *pToken != _T('\0')) {
		// Skip leading spaces
		while (*pToken == _T(' '))
			++pToken;

		if (*pToken == _T('\0'))
			break;

		// Locate the end of number
		pEnd = pToken;
		while (IN_FLOAT_CHARSET(*pEnd))
			++pEnd;

		// Copy the token to our buffer
		int nTokenLen = std::min((int) (sizeof(wszToken) / sizeof(wszToken[0])) - 1, int(pEnd - pToken));
		_tcsncpy(wszToken, pToken, nTokenLen);
		wszToken[nTokenLen] = _T('\0');
		*pNumbers = (float)_tcstod(wszToken, NULL);
		++nWritten;
		++pNumbers;
		pToken = pEnd;
	}

	return true;
}

bool CDXUTEditBox::SetTextFloatArray(const float* pNumbers, int nCount) {
	char szBuffer[512] = "";
	char* pNext = szBuffer;

	for (int i = 0; i < nCount; ++i)
		pNext += _snprintf(pNext, 512 - (pNext - szBuffer), "%.4f ", pNumbers[i]);

	// Don't want the last space
	if (nCount > 0)
		*(pNext - 1) = _T('\0');
	else
		*pNext = _T('\0');

	SetText(szBuffer);

	return true;
}

void CDXUTEditBox::ResetCaretBlink() {
	m_bCaretOn = true;
	m_dfLastBlink = DXUTGetGlobalTimer()->GetAbsoluteTime();
}

//Fetchs the length of the strings before nStartRow, this is used for link extraction
size_t CDXUTEditBox::GetCaretLocation(size_t nStartRow, size_t nCaret) {
	size_t start = 0;
	size_t numChars = 0;
	while (start != nStartRow) {
		if (GetEditable()) {
			if (start >= m_textRows.size())
				break;
			else
				numChars += m_textRows[start].length();
		} else {
			if (start >= m_textRowsRender.size())
				break;
			else
				numChars += m_textRowsRender[start].length();
		}

		start++;
	}
	numChars += nCaret;
	return numChars;
}

// PlaceCaret: Set the caret to a character position, and adjust the scrolling if necessary.
size_t CDXUTEditBox::SetCaretPosX(const size_t& posX) {
	// Set Buffer Used for Caret Placement & Rendering Just Incase
	if (IsMultiline())
		SetBuffer(GetCaretTextRow());

	// Validate Buffer Caret Placement
	int bufSize = m_Buffer.GetTextSize();
	if ((int)posX > bufSize) {
		LogError("INVALID CARET PLACEMENT - nCP=" << posX << " bufSize=" << bufSize << " '" << m_Buffer.GetString() << "'");
		return GetCaretPosX();
	}

	m_caretPosX = posX;

	// DRF - Advance Caret Following Line Break (maintain selected state)
	if (IsMultiline()) {
		if (posX && (m_Buffer[posX - 1] == LINE_BREAK_CHAR)) {
			bool selected = ValidSelection();
			auto rowNext = GetCaretTextRow() + 1;
			while (rowNext >= m_textRows.size())
				m_textRows.push_back(L"");
			SetCaretTextRow(rowNext);
			SetCaretPosX(0);
			if (!selected)
				ResetSelection();
		}
	}

	// Obtain the X offset of the character.
	int nX1st, nX, nX2;

	HRESULT hr = m_Buffer.CPtoX(m_nFirstVisible, FALSE, &nX1st); // 1st visible char
	if (FAILED(hr)) {
		LogError("FAILED CPtoX() - nCP=" << m_nFirstVisible);
	}

	hr = m_Buffer.CPtoX(posX, FALSE, &nX); // LEAD
	if (FAILED(hr)) {
		LogError("FAILED CPtoX() - nCP=" << posX);
	}

	if ((int)posX == bufSize) {
		// nCP is the index of NULL terminator, so trailing edge = leading edge
		nX2 = nX;
	} else {
		// Get the trailing edge x-pos of char at nCP
		hr = m_Buffer.CPtoX(posX, TRUE, &nX2); // TRAIL
		if (FAILED(hr)) {
			LogError("FAILED CPtoX() - nCP=" << posX);
		}
	}

	// If the left edge of the char is smaller than the left edge of the 1st visible char,
	// we need to scroll left until this char is visible.
	if (nX < nX1st) {
		// Simply make the first visible character the char at the new caret position.
		m_nFirstVisible = posX;
	} else if (nX2 > nX1st + RectWidth(m_rcText)) {
		// If the right of the character is bigger than the offset of the control's
		// right edge, we need to scroll right to this character.

		// Compute the X of the new left-most pixel
		int nXNewLeft = nX2 - RectWidth(m_rcText);

		// Compute the char position of this character
		int nCPNew1st, nNewTrail;
		hr = m_Buffer.XtoCP(nXNewLeft, &nCPNew1st, &nNewTrail);
		if (FAILED(hr)) {
			LogError("FAILED XtoCP() - nX=" << nXNewLeft);
		}

		// If this coordinate is not on a character border,
		// start from the next character so that the caret
		// position does not fall outside the text rectangle.
		int nXNew1st;
		hr = m_Buffer.CPtoX(nCPNew1st, FALSE, &nXNew1st);
		if (FAILED(hr)) {
			LogError("FAILED CPtoX() - nCP=" << nCPNew1st);
		}

		if (nXNew1st < nXNewLeft)
			++nCPNew1st;

		m_nFirstVisible = nCPNew1st;
	}

	return GetCaretPosX();
}

///////////////////////////////////////////////////////////////////////////////
// DRF - ED-29 - Edit Box Fixes
///////////////////////////////////////////////////////////////////////////////

#define VALID_INDEX(i, n) (((i) < 0) ? 0 : (((i) < (n)) ? (i) : ((n) ? (n - 1) : 0)))

size_t CDXUTEditBox::GetNumTextRows() {
	EDITBOX_TEXT_ROWS;
	return rows;
}

bool CDXUTEditBox::SetBuffer(const size_t& row) {
	EDITBOX_TEXT_ROWS;

	// Update Buffer With Row Text
	if (row >= textRows.size()) {
		m_Buffer.SetText(L"");
		return false;
	}
	m_Buffer.SetText(textRows[row].c_str());

	return true;
}

size_t CDXUTEditBox::SetCaretPosY(const size_t& posY) {
	EDITBOX_TEXT_ROWS;

	// Update Caret Position
	m_caretPosY = posY;

	// Update Buffer With Current Caret Text Row
	SetBuffer(GetCaretTextRow());

	return posY;
}

size_t CDXUTEditBox::SetCaretTextRow(const size_t& row) {
	EDITBOX_TEXT_ROWS;

	// Valid Row ?
	auto rowSet = VALID_INDEX(row, rows);

	// Update ScrollBar (guarantees rowSet visible)
	ScrollBarUpdate(rowSet);

	// Update Caret Position Y
	auto posY = rowSet - GetScrollBarTextRow();
	SetCaretPosY(posY);

	return rowSet;
}

size_t CDXUTEditBox::GetCaretTextRow() {
	EDITBOX_TEXT_ROWS;

	// Valid Row ?
	auto row = GetScrollBarTextRow() + GetCaretPosY();
	if (row < 0)
		row = SetCaretTextRow(0);
	else if (row && (row >= rows))
		row = SetCaretTextRow(rows ? (rows - 1) : 0);

	return row;
}

size_t CDXUTEditBox::GetSelectStartTextRow() {
	EDITBOX_TEXT_ROWS;

	// Valid Row ?
	auto rowSB = GetScrollBarTextRow();
	auto posY = GetSelectPosY();
	auto row = VALID_INDEX(rowSB + posY, rows);
	return row;
}

bool CDXUTEditBox::ColRow2PosAbs(size_t& posAbs, const size_t& col, const size_t& row) {
	EDITBOX_TEXT_ROWS;

	// Reset Return Value
	posAbs = 0;

	// Valid (Col, Row) ?
	auto rowV = VALID_INDEX(row, rows);
	auto colV = VALID_INDEX(col, textRows[rowV].size() + 1);

	// Accumulate Absolute Position (characters) Up To (col, row)
	for (size_t i = 0; i < rowV; ++i)
		posAbs += textRows[i].size();
	posAbs += colV;

	return true;
}

bool CDXUTEditBox::PosAbs2ColRow(const size_t& posAbs, size_t& col, size_t& row) {
	EDITBOX_TEXT_ROWS;

	// Reset Return Values
	col = row = 0;

	// Valid Absolute Position ?
	auto posAbsV = VALID_INDEX(posAbs, GetPosAbsMax() + 1);

	// Discount All Characters Until Absolutely Positioned
	for (size_t i = 0; i < rows; ++i) {
		auto rowSize = textRows[i].size();
		if (rowSize >= posAbsV || (i == rows - 1)) {
			// Set (Col, Row)
			col = posAbsV;
			row = i;
			return true;
		}
		posAbsV -= rowSize;
	}

	return false;
}

size_t CDXUTEditBox::GetPosAbsMax() {
	EDITBOX_TEXT_ROWS;

	// Accumulate Text Row Characters Up To End
	size_t posAbs = 0;
	for (size_t i = 0; i < rows; ++i)
		posAbs += textRows[i].size();
	return posAbs;
}

bool CDXUTEditBox::SetSelectPosAbs(const size_t& posAbs) {
	EDITBOX_TEXT_ROWS;

	// Convert posAbs To (col, row)
	size_t col = 0;
	size_t row = 0;
	PosAbs2ColRow(posAbs, col, row);

	// Set Selected
	m_selectPosY = row;
	m_selectPosX = col;

	return true;
}

bool CDXUTEditBox::GetSelectPosAbs(size_t& posAbs, size_t& numAbs) {
	EDITBOX_TEXT_ROWS;

	// Reset Return Values
	posAbs = numAbs = 0;

	// Valid Selection ?
	if (!ValidSelection())
		return false;

	// Get Selection Absolute Position & Number
	auto caretPosAbs = GetCaretPosAbs();
	auto selPosAbs = GetSelectPosAbs();
	auto posAbsS = std::min(caretPosAbs, selPosAbs);
	auto posAbsE = std::max(caretPosAbs, selPosAbs);
	posAbs = posAbsS;
	numAbs = (posAbsE - posAbsS);

	return true;
}

bool CDXUTEditBox::SetCaretPosAbs(const size_t& posAbs) {
	EDITBOX_TEXT_ROWS;

	// Convert posAbs To (col, row)
	size_t col = 0;
	size_t row = 0;
	PosAbs2ColRow(posAbs, col, row);

	// Set Caret (updates scrollbar)
	SetCaretTextRow(row);
	SetCaretPosX(col);

	return true;
}

// DRF - Returns position of first line break in string not at end of line.
static size_t FirstLineBreakPos(const std::wstring& str) {
	auto strSize = str.size();
	if (strSize < 2)
		return std::string::npos;
	auto strNoEol = str.substr(0, strSize - 1);
	return strNoEol.find(LINE_BREAK_CHAR);
}

// DRF - Returns position of last line space in string not at end of line.
static size_t LastLineSpacePos(const std::wstring& str) {
	auto strSize = str.size();
	if (strSize < 2)
		return std::string::npos;
	auto strNoEol = str.substr(0, strSize - 1);
	return strNoEol.rfind(' ');
}

// DRF - Returns character index where given text using given font overflows the given rectangle.
// (0=Error, string::npos=NoOverflow)
static size_t LineOverflowsRect(const std::wstring& str, CDXUTFontNode* pFontNode, const RECT& rect) {
	// Valid String ?
	size_t strSize = str.size();
	if (!strSize)
		return std::string::npos;

	// Valid Rectangle ?
	if (rect.bottom <= 0 && rect.top <= 0 && rect.left <= 0 && rect.right <= 0)
		return std::string::npos;
	if (rect.left <= 0 || rect.right <= 0)
		return std::string::npos;

	// Valid Font ?
	if (!pFontNode || !pFontNode->pFont)
		return std::string::npos;

	// Return Last Character Of Overflow (string::npos = no overflow)
	auto hdc = pFontNode->pFont->GetDC();
	SIZE size;
	bool overflow = false;
	int width = RectWidth(rect);
	for (; strSize; --strSize) {
		GetTextExtentPoint32(hdc, str.c_str(), (int)strSize, &size);
		if (size.cx < width)
			return overflow ? (strSize - 1) : std::string::npos;
		overflow = true;
	}

	LogError("FAILED - FIX THIS !!!");
	return 0;
}

size_t CDXUTEditBox::LineSplitPos(const std::wstring& str) {
	// Priority 1 - First Line Break Split
	auto pos = FirstLineBreakPos(str);
	if (pos != std::string::npos)
		return pos;

	// Priority 2 - Line Overflow Split
	auto strSub = str;
	auto pFontNode = m_pDialog->GetFont(m_Elements[0]->GetFontIndex());
	FOREVER {
		size_t overflowPos = LineOverflowsRect(strSub, pFontNode, m_rcText);
		if (overflowPos == std::string::npos)
			break;

		// Priority 2.1 - Split At Last Space
		pos = LastLineSpacePos(strSub);
		if (pos == std::string::npos)
			return overflowPos; // Priority 2.2 - Split At Overflow

		// Strip At Last Space And Repeat Until No Overflow
		strSub = strSub.substr(0, pos + 1);
	}

	return pos;
}

std::wstring CDXUTEditBox::TextRowsGetStrAbs(const TEXT_ROWS& textRows, const size_t& posAbs, const size_t& numAbs) {
	// Append Absolute String
	std::wstring strAbs;
	auto rows = textRows.size();
	for (size_t i = 0; i < rows; ++i)
		strAbs += textRows[i];

	//	LogInfo("'"<<strAbs<<"'");

	// Return Substring
	if (posAbs && numAbs) {
		strAbs = strAbs.substr(posAbs, numAbs ? numAbs : std::string::npos);
		//		LogInfo("sub '"<<strAbs<<"'");
	}

	return strAbs;
}

bool CDXUTEditBox::TextRowsSetStrAbs(TEXT_ROWS& textRows, const std::wstring& strAbs) {
	textRows.clear();
	textRows.push_back(strAbs);
	//	LogInfo("'"<<strAbs<<"'");
	return true;
}

bool CDXUTEditBox::TextRowsLineSplit(TEXT_ROWS& textRows, const size_t& row) {
	// Valid Row ?
	auto rows = textRows.size();
	if (row >= rows)
		return false;

	// Line Needs Split ?
	auto line = textRows[row];
	auto pos = LineSplitPos(line);
	if (pos == std::string::npos)
		return false;

	// Split This Line
	textRows[row] = line.substr(0, pos + 1);

	// Insert New Text Row ?
	auto rowNext = row + 1;
	if (rowNext >= rows)
		textRows.push_back(L"");

	// Prepend Line Remainder To Next Row
	textRows[rowNext] = line.substr(pos + 1) + textRows[rowNext];

	return true;
}

bool CDXUTEditBox::TextRowsRefresh(TEXT_ROWS& textRows) {
	// Update All Rectangles
	UpdateRects();

	// Get Caret Position
	auto caretPosAbs = GetCaretPosAbs();

	// Reset Text Rows String Absolutely
	TextRowsSetStrAbs(textRows, TextRowsGetStrAbs(textRows));

	// Re-Split Line Into All Text Rows (textRows will grow)
	for (size_t i = 0; i < textRows.size(); ++i) {
		// Split Each Line Possibly Twice
		if (TextRowsLineSplit(textRows, i))
			TextRowsLineSplit(textRows, i);
	}

	// Set New Caret Position
	SetCaretPosAbs(caretPosAbs);

	return true;
}

bool CDXUTEditBox::TextRowsAddAtCaret(TEXT_ROWS& textRows, const std::wstring& str) {
	// Get Current Text Row
	auto row = GetCaretTextRow();

	// Add Rows ?
	while (row >= textRows.size())
		textRows.push_back(L"");

	// Get Line At Text Row
	std::wstring line = textRows[row];

	// Get Current Caret Position
	auto posX = GetCaretPosX();
	auto lineSize = line.size();
	if (posX > lineSize) {
		LogError("posX=" << posX << " > lineSize=" << lineSize);
		posX = SetCaretPosX(lineSize);
	}

	// Get Caret Position
	auto caretPosAbs = GetCaretPosAbs();

	// Insert String Into Line At Caret
	line.insert(posX, str);
	textRows[row] = line;

	// Re-Split Lines For All Rows
	TextRowsRefresh(textRows);

	// Set New Caret Position
	SetCaretPosAbs(caretPosAbs + str.size());

	// Reset Selection
	ResetSelection();

	return true;
}

bool CDXUTEditBox::TextRowsDelAbs(TEXT_ROWS& textRows, const size_t& posAbs, const size_t& numAbs) {
	// Nothing To Delete ?
	if (!numAbs)
		return false;

	// Get Text Rows Absolute String
	auto strAbs = TextRowsGetStrAbs(textRows);

	// Erase Absolute Characters
	strAbs.erase(posAbs, numAbs);

	// Set Text Rows Absolute String
	TextRowsSetStrAbs(textRows, strAbs);

	// Refresh Text Rows
	TextRowsRefresh(textRows);

	// Set New Caret Position
	SetCaretPosAbs(posAbs);

	// Reset Selection
	ResetSelection();

	return true;
}

///////////////////////////////////////////////////////////////////////////////

void DXUTBlendColor::Init(D3DCOLOR defaultColor, D3DCOLOR disabledColor, D3DCOLOR hiddenColor) {
	for (int i = 0; i < DXUT_CONTROL_STATES_MAX; i++) {
		m_States[i] = defaultColor;
	}

	m_States[DXUT_STATE_DISABLED] = disabledColor;
	m_States[DXUT_STATE_HIDDEN] = hiddenColor;
	m_Current = hiddenColor;
}

// [MS] Visual Studio profiler was telling me that DXUTBlendColor::Blend was taking 2.8% of the render time.
// Optimizing it and reprofiling gave nonsensical results. (Other unrelated functions sped up by a factor of 10, even after multiple retest.)
// Measuring with the Windows performance counters suggest that optimizing this function can only save 38 microseconds per frame.
// Now I know not to trust the Visual Studio profiler.
// I'm keeping this code disabled for now because it requires a fixed transition rate which is probably not worth enforcing for the little gain.
// Also, this DXUT stuff should disappear with the new graphics engine.
#if 0
namespace {
static const float kfBlendLUTSampleFreq = 400.0f; // Samples per second.
class BlendLUT {
public:
	BlendLUT(float fRate) {
		m_fRate = fRate;
		for (size_t i = 0; i < NumValues(); ++i)
			m_values[i] = Calculate((i + 0.5f) / kfBlendLUTSampleFreq);
	}
	float ValueForTime(float fTime) const {
		if (fTime <= 0.0f)
			return 0.0f;
		float fIndex = kfBlendLUTSampleFreq * fTime;
		if (fIndex < NumValues()) // Compare before converting to int to avoid overflow issues.
			return  m_values[static_cast<int>(fIndex)];

		return Calculate(fTime); // Fall back to slow path for long time intervals.
	}
private:
	float Calculate(float fTime) const {
		return 1.0f - powf(m_fRate, 30 * fTime);
	}
	size_t NumValues() const {
		return sizeof(m_values) / sizeof(m_values[0]);
	}
	float m_values[100];
	float m_fRate;
};
static const BlendLUT s_BlendLUT(.8f);
}
static const int g_BlendVersion = 0;

void DXUTBlendColor::Blend(DXUT_CONTROL_STATE iState, float fElapsedTime, bool bImmediateTransition) {
	D3DXCOLOR destColor = GetColor(iState);
	if (g_BlendVersion == 0) {
		m_Current = destColor;
	} else if (g_BlendVersion == 1) {
		float fInterpFactor = bImmediateTransition ? 1.0f : s_BlendLUT.ValueForTime(fElapsedTime);
		D3DXColorLerp(&m_Current, &m_Current, &destColor, fInterpFactor);
	} else {
		D3DXColorLerp(&m_Current, &m_Current, &destColor, 1.0f - powf(.8f, 30 * fElapsedTime));
	}
}
#endif

void DXUTBlendColor::Blend(DXUT_CONTROL_STATE iState, float elapsedSec, float fRate) {
	D3DXCOLOR destColor = GetColor(iState);
	D3DXColorLerp(&m_Current, &m_Current, &destColor, 1.0f - powf(fRate, 30 * elapsedSec));
}

bool DXUTBlendColor::Load(TiXmlNode* blend_color_root, unsigned int iStatesToLoad) {
	bool bSuccess = true;

	if (blend_color_root) {
		TiXmlNode* child = 0;
		for (child = blend_color_root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();

			if (!_stricmp(val, NORMAL_TAG)) {
				D3DCOLOR color;
				if ((iStatesToLoad & DXUT_STATE_FLAG_NORMAL) && GetDWORDFromXMLNode(child, color))
					SetColor(DXUT_STATE_NORMAL, color);
			} else if (!_stricmp(val, DISABLED_TAG)) {
				D3DCOLOR color;
				if ((iStatesToLoad & DXUT_STATE_FLAG_DISABLED) && GetDWORDFromXMLNode(child, color))
					SetColor(DXUT_STATE_DISABLED, color);
			} else if (!_stricmp(val, HIDDEN_TAG)) {
				D3DCOLOR color;
				if ((iStatesToLoad & DXUT_STATE_FLAG_HIDDEN) && GetDWORDFromXMLNode(child, color))
					SetColor(DXUT_STATE_HIDDEN, color);
			} else if (!_stricmp(val, FOCUS_TAG)) {
				D3DCOLOR color;
				if ((iStatesToLoad & DXUT_STATE_FLAG_FOCUS) && GetDWORDFromXMLNode(child, color))
					SetColor(DXUT_STATE_FOCUS, color);
			} else if (!_stricmp(val, MOUSEOVER_TAG)) {
				D3DCOLOR color;
				if ((iStatesToLoad & DXUT_STATE_FLAG_MOUSEOVER) && GetDWORDFromXMLNode(child, color))
					SetColor(DXUT_STATE_MOUSEOVER, color);
			} else if (!_stricmp(val, PRESSED_TAG)) {
				D3DCOLOR color;
				if ((iStatesToLoad & DXUT_STATE_FLAG_PRESSED) && GetDWORDFromXMLNode(child, color))
					SetColor(DXUT_STATE_PRESSED, color);
			}
		}
	}

	return bSuccess;
}

bool DXUTBlendColor::Save(TiXmlNode* parent, unsigned int iStatesToSave) {
	bool bSuccess = true;

	if (parent) {
		TiXmlNode* child;

		if (iStatesToSave & DXUT_STATE_FLAG_NORMAL) {
			child = parent->InsertEndChild(TiXmlElement(NORMAL_TAG));
			if (child) {
				PutDWORDToXMLNode(child, GetColor(DXUT_STATE_NORMAL));
			}
		}

		if (iStatesToSave & DXUT_STATE_FLAG_DISABLED) {
			child = parent->InsertEndChild(TiXmlElement(DISABLED_TAG));
			if (child) {
				PutDWORDToXMLNode(child, GetColor(DXUT_STATE_DISABLED));
			}
		}

		if (iStatesToSave & DXUT_STATE_FLAG_HIDDEN) {
			child = parent->InsertEndChild(TiXmlElement(HIDDEN_TAG));
			if (child) {
				PutDWORDToXMLNode(child, GetColor(DXUT_STATE_HIDDEN));
			}
		}

		if (iStatesToSave & DXUT_STATE_FLAG_FOCUS) {
			child = parent->InsertEndChild(TiXmlElement(FOCUS_TAG));
			if (child) {
				PutDWORDToXMLNode(child, GetColor(DXUT_STATE_FOCUS));
			}
		}

		if (iStatesToSave & DXUT_STATE_FLAG_MOUSEOVER) {
			child = parent->InsertEndChild(TiXmlElement(MOUSEOVER_TAG));
			if (child) {
				PutDWORDToXMLNode(child, GetColor(DXUT_STATE_MOUSEOVER));
			}
		}

		if (iStatesToSave & DXUT_STATE_FLAG_PRESSED) {
			child = parent->InsertEndChild(TiXmlElement(PRESSED_TAG));
			if (child) {
				PutDWORDToXMLNode(child, GetColor(DXUT_STATE_PRESSED));
			}
		}
	}

	return bSuccess;
}

bool CDXUTElement::Load(TiXmlNode* root, IMenuDialog* pDialog, unsigned int iUsesFontFlags, unsigned int iUsesTextureFlags) {
	bool bSuccess = true;

	if (root) {
		TiXmlNode* child = 0;
		for (child = root->FirstChild(); child; child = child->NextSibling()) {
			const char* val = child->Value();
			bool bParsed = false;

			if (iUsesFontFlags && !bParsed) {
				if (!_stricmp(val, TEXT_FORMAT_TAG)) {
					bParsed = true;
					DWORD dwTextFormat;
					if (GetDWORDFromXMLNode(child, dwTextFormat))
						this->SetTextFormat(dwTextFormat);
				} else if (!_stricmp(val, FONT_COLOR_TAG)) {
					bParsed = true;
					bSuccess = this->fontColor.Load(child, iUsesFontFlags);
				} else if (!_stricmp(val, FONT_TAG)) {
					bParsed = true;
					bSuccess = LoadFont(child, pDialog);
				} else if (!_stricmp(val, TEXT_OFFSET_X_TAG)) {
					bParsed = true;
					int offsetX = 0;
					if (GetIntFromXMLNode(child, offsetX)) {
						SetTextOffsetX(offsetX);
					}
				} else if (!_stricmp(val, TEXT_OFFSET_Y_TAG)) {
					bParsed = true;
					int offsetY = 0;
					if (GetIntFromXMLNode(child, offsetY)) {
						SetTextOffsetY(offsetY);
					}
				}
			}

			if (iUsesTextureFlags && !bParsed) {
				if (!_stricmp(val, TEXTURE_COLOR_TAG)) {
					bParsed = true;
					bSuccess = this->textureColor.Load(child, iUsesTextureFlags);
				} else if (!_stricmp(val, TEXTURE_TAG)) {
					bParsed = true;
					bSuccess = LoadTexture(child, pDialog);
				}
			}
		}
	}

	return bSuccess;
}

void LogIdx(size_t idx, const std::string& func, const std::string& msg) {
	ASSERT_GDRM();

	// Log DX Errors
	switch (idx) {
		case IDX_NOT_IN_ARRAY: // Will Ignore Resource
			_LogError(func << ": NOT ARRAY '" << msg << "'");
			break;

		case IDX_FILE_NOT_FOUND: // Will Retry Download Resource
			// _LogWarn(func << ": FILE NOT FOUND '" << msg << "'");
			break;

		case IDX_INVALID_NAME: // Will Ignore Resource
			_LogError(func << ": INVALID NAME '" << msg << "'");
			break;

		case IDX_FAILED_CREATE: // Will Retry Create Resource
			if (pGDRM->IsInView()) {
				_LogError(func << ": FAILED CREATE '" << msg << "' - VIEW");
			} else {
				_LogWarn(func << ": FAILED CREATE '" << msg << "' - OF VIEW"); // expected
			}
			break;

		case IDX_OUT_OF_MEMORY: // Will Ignore Resource
			_LogError(func << ": OF MEMORY '" << msg << "'");
			break;

		default:
			_LogDebug(func << ": OK '" << msg << "'");
			break;
	}
}

bool CDXUTElement::AddFont(IMenuDialog* pDialog, const std::string& sFaceName, long height, long weight, bool italic, bool underline) {
	// Valid Dialog ?
	if (!pDialog)
		return false;

	// Valid Font Spec ?
	if (sFaceName.empty() || (height == -1) || (weight == -1)) {
		//		LogDebug("Invalid Font Spec"); // happens all the time
		return false;
	}

	// Attempt To Create Font
	int idx = pDialog->AddFont(sFaceName, height, weight, italic, underline);
	LogIdx(idx, __FUNCTION__, sFaceName);

	//Ankit ----- if the font loading failed because client was in minized state, store the values to create resource once again when client active
	if (IDX_ERROR(idx)) {
		if (idx == IDX_CLIENT_OUT_OF_VIEW) {
			failedFontOnClientMinimize = true;
			failedFontInDialog = pDialog;
			failedFontName = sFaceName;
			failedFontHeight = height;
			failedFontWeight = weight;
			failedFontItalic = italic;
			failedFontUnderline = underline;
		}
		return false;
	}

	// Success
	this->SetFontIndex(idx);

	return true;
}

/** DRF
* This function will attempt to retry creation of textures that have previously failed
* to be created within DirectX.  It returns true if the texture has been created successfully
* otherwise it returns false and the texture should not be used.
*/
#define FAILED_TEXTURE_RETRIES 5
bool CDXUTElement::RetryFailedTexture(IMenuDialog* pDialog) {
	// Texture Failed ?
	if (!failedTexture)
		return true;

	// Increment Failed Retries
	if (++failedTextureRetries > FAILED_TEXTURE_RETRIES)
		return false;

	// Retry Create Texture
	bool ok = AddTexture(pDialog, failedTextureFilename, failedTextureCoords, failedTextureSlices);
	if (ok) {
		LogWarn("<" << this << "> OK '" << failedTextureFilename << "'");
	} else {
		LogError("<" << this << "> FAILED retry=" << failedTextureRetries << " '" << failedTextureFilename << "'");
	}

	// Needs Window Refresh On Success
	if (ok)
		FlagWindowRefresh();

	return ok;
}

bool CDXUTElement::AddTexture(IMenuDialog* pDialog, const std::string& texturePath, RECT coords, RECT slices) {
	// Valid Dialog ?
	if (!pDialog)
		return false;

	// Use Default Texture Path ?
	std::string texPath = texturePath;
	if (StrFileName(texPath).empty()) {
		// LogError("Invalid Texture Path '" << texPath << "' - Ignoring");
		return true;
	}

	// Attempt To Create Texture
	D3DXIMAGE_INFO texInfo;
	texInfo.Width = 0;
	texInfo.Height = 0;
	int idx = pDialog->AddTexture(texPath, &texInfo);
	LogIdx(idx, __FUNCTION__, texPath);

	if (idx >= 0) {
		textureWidth = (pDialog->GetTexture(idx))->GetWidth();
		textureHeight = (pDialog->GetTexture(idx))->GetHeight();
		texInfoNeedsUpdate = false;
	} else if ((texInfo.Height > 0) && (texInfo.Width > 0)) {
		textureWidth = texInfo.Width;
		textureHeight = texInfo.Height;
		texInfoNeedsUpdate = false;
	}

	// Flag Failed Create Textures For Retry
	if ((idx == IDX_FAILED_CREATE) || (idx == IDX_CLIENT_OUT_OF_VIEW)) {
		failedTexture = true;
		failedTextureFilename = texPath;
		failedTextureCoords = coords;
		failedTextureSlices = slices;
	}

	//Ankit ----- Set the textureCoords and Slices even if texture loading fails since they are loaded directly from menu xml and doesn't depend on successful texture creation
	SetTextureCoords(coords);
	SetTextureSlices(slices);

	if (IDX_ERROR(idx))
		return false;

	// Success
	failedTexture = false;
	this->iTexture = idx;

	return true;
}

bool CDXUTElement::AddExternalTexture(IMenuDialog* pDialog, const std::string& texturePath) {
	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog))
		return false;

	// Valid Texture Spec ?
	std::string textureExt = StrStripLeftLast(texturePath, ".");
	if (texturePath.empty() || textureExt.empty()) {
		LogDebug("Invalid External Texture Spec"); // happens all the time
		return true;
	}

	// Attempt To Create Texture
	int idx = pDialog->AddExternalTexture(texturePath);
	LogIdx(idx, __FUNCTION__, texturePath);

	if (IDX_ERROR(idx))
		return false;

	// Success
	textureWidth = (pDialog->GetTexture(idx))->GetWidth();
	textureHeight = (pDialog->GetTexture(idx))->GetHeight();

	this->iTexture = idx;

	return true;
}

bool CDXUTElement::AddProgressTexture(IMenuDialog* pDialog, const std::string& texturePath) {
	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog))
		return false;

	if (texturePath.empty())
		return false;

	D3DXIMAGE_INFO texInfo;
	int idx = pDialog->AddTexture(texturePath, &texInfo);
	LogIdx(idx, __FUNCTION__, texturePath);

	if (IDX_ERROR(idx))
		return false;

	progressTextureWidth = texInfo.Width;
	progressTextureHeight = texInfo.Height;

	// Success
	this->iProgressTexture = idx;

	return true;
}

bool CDXUTElement::AddDynamicTexture(IMenuDialog* pDialog, unsigned dynTextureId) {
	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog))
		return false;

	// Attempt To Create Texture
	int idx = pDialog->AddDynamicTexture(dynTextureId);
	LogIdx(idx, __FUNCTION__, std::to_string(dynTextureId));

	if (IDX_ERROR(idx))
		return false;

	// Success
	textureWidth = (pDialog->GetTexture(idx))->GetWidth();
	textureHeight = (pDialog->GetTexture(idx))->GetHeight();

	this->iTexture = idx;

	return true;
}

IMenuFont* CDXUTElement::GetFont(IMenuDialog* pDialog) {
	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog))
		return NULL;

	CDXUTFontNode* pFont = pDialog->GetFont(this->GetFontIndex());
	return (IMenuFont*)pFont;
}

//Ankit ----- Made the function not inline and to account for failed creation of font resource for the element
size_t CDXUTElement::GetFontIndex() {
	if (failedFontOnClientMinimize) {
		if (AddFont(failedFontInDialog, failedFontName, failedFontHeight, failedFontWeight, failedFontItalic, failedFontUnderline)) {
			failedFontOnClientMinimize = false;
			failedFontInDialog = NULL;
		}
	}
	return this->m_iFont;
}

const char* CDXUTElement::GetTextureNamePtr(IMenuDialog* pDialog) {
	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog))
		return NULL;

	// Get Texture Node
	DXUTTextureNode* pTexture = pDialog->GetTexture(this->iTexture);
	if (!pTexture)
		return NULL;

	return pTexture->GetFileNamePtr();
}

std::string CDXUTElement::GetTextureName(IMenuDialog* pDialog) {
	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog))
		return "";

	// Get Texture Node
	DXUTTextureNode* pTexture = pDialog->GetTexture(this->iTexture);
	if (!pTexture)
		return "";

	return pTexture->GetFileName();
}

UINT CDXUTElement::GetTextureWidth(IMenuDialog* pDialog) {
	int texWidth = 0;
	if (texInfoNeedsUpdate) {
		DXUTTextureNode* tempTextureNode = pDialog->GetTexture(iTexture);
		if (tempTextureNode != NULL) {
			int tempWidth = (pDialog->GetTexture(iTexture))->GetWidth();
			int tempHeight = (pDialog->GetTexture(iTexture))->GetHeight();
			if ((tempWidth > 0) && (tempHeight > 0)) {
				textureWidth = tempWidth;
				textureHeight = tempHeight;
				texInfoNeedsUpdate = false;
				texWidth = textureWidth;
			} else
				texInfoNeedsUpdate = true;
		} else
			texInfoNeedsUpdate = true;
	} else
		texWidth = textureWidth;

	return texWidth;
}

UINT CDXUTElement::GetTextureHeight(IMenuDialog* pDialog) {
	int texHeight = 0;
	if (texInfoNeedsUpdate) {
		DXUTTextureNode* tempTextureNode = pDialog->GetTexture(iTexture);
		if (tempTextureNode != NULL) {
			int tempWidth = (pDialog->GetTexture(iTexture))->GetWidth();
			int tempHeight = (pDialog->GetTexture(iTexture))->GetHeight();
			if ((tempWidth > 0) && (tempHeight > 0)) {
				textureWidth = tempWidth;
				textureHeight = tempHeight;
				texInfoNeedsUpdate = false;
				texHeight = textureHeight;
			} else
				texInfoNeedsUpdate = true;
		} else
			texInfoNeedsUpdate = true;
	} else
		texHeight = textureHeight;

	return texHeight;
}

RECT CDXUTElement::GetTextRect(RECT* prcDest) {
	RECT rcText = *prcDest;
	OffsetRect(&rcText, GetTextOffsetX(), GetTextOffsetY());
	return rcText;
}

bool CDXUTElement::LoadFont(TiXmlNode* font_root, IMenuDialog* pDialog) {
	if (!font_root)
		return false;

	std::string sFaceName = "";
	long height = -1;
	long weight = -1;
	long italic = 0;
	long underline = 0;

	TiXmlNode* child = 0;
	for (child = font_root->FirstChild(); child; child = child->NextSibling()) {
		const char* val = child->Value();

		if (!_stricmp(val, FACENAME_TAG)) {
			GetStringFromXMLNode(child, sFaceName);
		} else if (!_stricmp(val, HEIGHT_TAG)) {
			GetLongFromXMLNode(child, height);
		} else if (!_stricmp(val, WEIGHT_TAG)) {
			GetLongFromXMLNode(child, weight);
		} else if (!_stricmp(val, ITALIC_TAG)) {
			GetLongFromXMLNode(child, italic);
		} else if (!_stricmp(val, UNDERLINE_TAG)) {
			GetLongFromXMLNode(child, underline);
		}
	}

	// Create Font
	return AddFont(pDialog, sFaceName, height, weight, italic != 0, underline != 0);
}

bool CDXUTElement::LoadTexture(TiXmlNode* texture_root, IMenuDialog* pDialog) {
	if (!texture_root)
		return false;

	this->iTexture = 0;

	// Assume 'DXUTcontrols.dds' If No Name Specified
	std::string sTextureName = "DXUTControls.dds";
	RECT coords = this->GetTextureCoords();
	RECT slices = this->GetTextureSlices();

	TiXmlNode* child = 0;
	for (child = texture_root->FirstChild(); child; child = child->NextSibling()) {
		const char* val = child->Value();

		if (!_stricmp(val, NAME_TAG)) {
			GetStringFromXMLNode(child, sTextureName);
		} else if (!_stricmp(val, COORDS_TAG)) {
			SetRect(&coords, 0, 0, -1, -1); // If we have a <coords> block, change the defaults to the whole texture, instead of what's in default_elements.xml

			TiXmlNode* child_coord = child->FirstChild(XLEFT_TAG);
			if (child_coord)
				GetLongFromXMLNode(child_coord, coords.left);

			child_coord = child->FirstChild(YTOP_TAG);
			if (child_coord)
				GetLongFromXMLNode(child_coord, coords.top);

			child_coord = child->FirstChild(XRIGHT_TAG);
			if (child_coord)
				GetLongFromXMLNode(child_coord, coords.right);

			child_coord = child->FirstChild(YBOTTOM_TAG);
			if (child_coord)
				GetLongFromXMLNode(child_coord, coords.bottom);
		} else if (!_stricmp(val, SLICES_TAG)) {
			SetRect(&slices, 0, 0, 0, 0); // If we have a <slices> block, change the defaults to the whole texture, instead of what's in default_elements.xml

			TiXmlNode* child_coord = child->FirstChild(XLEFT_TAG);
			if (child_coord)
				GetLongFromXMLNode(child_coord, slices.left);

			child_coord = child->FirstChild(YTOP_TAG);
			if (child_coord)
				GetLongFromXMLNode(child_coord, slices.top);

			child_coord = child->FirstChild(XRIGHT_TAG);
			if (child_coord)
				GetLongFromXMLNode(child_coord, slices.right);

			child_coord = child->FirstChild(YBOTTOM_TAG);
			if (child_coord)
				GetLongFromXMLNode(child_coord, slices.bottom);
		}
	}

	// DRF - Added Coords & Slices
	// Create Texture
	return AddTexture(pDialog, sTextureName, coords, slices);
}

bool CDXUTElement::SaveFont(TiXmlNode* parent, IMenuDialog* pDialog) {
	bool bSuccess = true;

	//Ankit ----- Save font info even if the font creation had failed using failed font info stored
	if (failedFontOnClientMinimize) {
		TiXmlNode* font_node = parent->InsertEndChild(TiXmlElement(FONT_TAG));
		if (font_node) {
			TiXmlNode* child = font_node->InsertEndChild(TiXmlElement(FACENAME_TAG));
			if (child) {
				PutStringToXMLNode(child, failedFontName);
			}

			child = font_node->InsertEndChild(TiXmlElement(HEIGHT_TAG));
			if (child) {
				PutLongToXMLNode(child, failedFontHeight);
			}

			child = font_node->InsertEndChild(TiXmlElement(WEIGHT_TAG));
			if (child) {
				PutLongToXMLNode(child, failedFontWeight);
			}

			// Only write the italic tag if it's enabled, to save spam in the .xml file
			if (failedFontItalic) {
				child = font_node->InsertEndChild(TiXmlElement(ITALIC_TAG));
				if (child) {
					PutLongToXMLNode(child, failedFontItalic ? 1 : 0);
				}
			}
		}
	}

	if (parent && pDialog && this->GetFontIndex() != -1) {
		CDXUTFontNode* fontNode = pDialog->GetFont(this->GetFontIndex());
		if (fontNode) {
			std::string sFacename = fontNode->strFace;
			TiXmlNode* font_node = parent->InsertEndChild(TiXmlElement(FONT_TAG));
			if (font_node) {
				TiXmlNode* child = font_node->InsertEndChild(TiXmlElement(FACENAME_TAG));
				if (child) {
					PutStringToXMLNode(child, sFacename);
				}

				child = font_node->InsertEndChild(TiXmlElement(HEIGHT_TAG));
				if (child) {
					PutLongToXMLNode(child, fontNode->nHeight);
				}

				child = font_node->InsertEndChild(TiXmlElement(WEIGHT_TAG));
				if (child) {
					PutLongToXMLNode(child, fontNode->nWeight);
				}

				// Only write the italic tag if it's enabled, to save spam in the .xml file
				if (fontNode->bItalic) {
					child = font_node->InsertEndChild(TiXmlElement(ITALIC_TAG));
					if (child) {
						PutLongToXMLNode(child, fontNode->bItalic ? 1 : 0);
					}
				}
			}
		}
	}

	return bSuccess;
}

bool CDXUTElement::SaveTexture(TiXmlNode* parent, IMenuDialog* pDialog) {
	//Ankit ----- Save texture info even if the texture creation had failed using failed tex info stored
	if (failedTexture) {
		TiXmlNode* texture_node = parent->InsertEndChild(TiXmlElement(TEXTURE_TAG));
		if (!texture_node) {
			LogError("InsertEndChild(TEXTURE_TAG) FAILED");
			return false;
		}

		TiXmlNode* name_node = texture_node->InsertEndChild(TiXmlElement(NAME_TAG));
		if (name_node)
			PutStringToXMLNode(name_node, failedTextureFilename);

		TiXmlNode* coords_node = texture_node->InsertEndChild(TiXmlElement(COORDS_TAG));
		if (!coords_node) {
			LogError("InsertEndChild(COORDS_TAG) FAILD");
			return false;
		}

		TiXmlNode* child = coords_node->InsertEndChild(TiXmlElement(XLEFT_TAG));
		if (child)
			PutLongToXMLNode(child, failedTextureCoords.left);

		child = coords_node->InsertEndChild(TiXmlElement(YTOP_TAG));
		if (child)
			PutLongToXMLNode(child, failedTextureCoords.top);

		child = coords_node->InsertEndChild(TiXmlElement(XRIGHT_TAG));
		if (child)
			PutLongToXMLNode(child, failedTextureCoords.right);

		child = coords_node->InsertEndChild(TiXmlElement(YBOTTOM_TAG));
		if (child)
			PutLongToXMLNode(child, failedTextureCoords.bottom);

		if ((failedTextureSlices.left != 0) || (failedTextureSlices.top != 0) || (failedTextureSlices.right != 0) || (failedTextureSlices.bottom != 0)) {
			TiXmlNode* slices_node = texture_node->InsertEndChild(TiXmlElement(SLICES_TAG));
			if (slices_node) {
				child = slices_node->InsertEndChild(TiXmlElement(XLEFT_TAG));
				if (child) {
					PutLongToXMLNode(child, failedTextureSlices.left);
				}

				child = slices_node->InsertEndChild(TiXmlElement(YTOP_TAG));
				if (child) {
					PutLongToXMLNode(child, failedTextureSlices.top);
				}

				child = slices_node->InsertEndChild(TiXmlElement(XRIGHT_TAG));
				if (child) {
					PutLongToXMLNode(child, failedTextureSlices.right);
				}

				child = slices_node->InsertEndChild(TiXmlElement(YBOTTOM_TAG));
				if (child) {
					PutLongToXMLNode(child, failedTextureSlices.bottom);
				}
			}
		}

		return true;
	}

	if (!parent || !pDialog || (this->iTexture == -1)) {
		LogError("Invalida Texture");
		return false;
	}

	TiXmlNode* texture_node = parent->InsertEndChild(TiXmlElement(TEXTURE_TAG));
	if (!texture_node) {
		LogError("InsertEndChild(TEXTURE_TAG) FAILED");
		return false;
	}

	// Get Texture Node
	DXUTTextureNode* textureNode = pDialog->GetTexture(this->iTexture);
	if (!textureNode) {
		LogError("textureNode is null");
		return false;
	}
	std::string texFileName = StrStripFilePath(textureNode->GetFileName());

	// Get Default Texture Node
	DXUTTextureNode* defaultTextureNode = pDialog->GetTexture(0);
	if (!defaultTextureNode) {
		LogError("defaultTextureNode is null");
		return false;
	}
	std::string texFileNameDefault = StrStripFilePath(defaultTextureNode->GetFileName());

	//				int i = (int) texFileName.find_last_of('\\');
	//				if (i >= 0) {
	//					i++;
	//					texFileName = texFileName.substr(i, texFileName.size() - i);
	//				}

	//				i = (int) texFileNameDefault.find_last_of('\\');
	//				if (i >= 0) {
	//					i++;
	//					texFileNameDefault = texFileNameDefault.substr(i, texFileNameDefault.size() - i);
	//				}

	if (!StrSameIgnoreCase(texFileName, texFileNameDefault)) {
		TiXmlNode* name_node = texture_node->InsertEndChild(TiXmlElement(NAME_TAG));
		if (name_node)
			PutStringToXMLNode(name_node, texFileName);
	}

	TiXmlNode* coords_node = texture_node->InsertEndChild(TiXmlElement(COORDS_TAG));
	if (!coords_node) {
		LogError("InsertEndChild(COORDS_TAG) FAILD");
		return false;
	}

	TiXmlNode* child = coords_node->InsertEndChild(TiXmlElement(XLEFT_TAG));
	if (child)
		PutLongToXMLNode(child, this->GetTextureCoords().left);

	child = coords_node->InsertEndChild(TiXmlElement(YTOP_TAG));
	if (child)
		PutLongToXMLNode(child, this->GetTextureCoords().top);

	child = coords_node->InsertEndChild(TiXmlElement(XRIGHT_TAG));
	if (child)
		PutLongToXMLNode(child, this->GetTextureCoords().right);

	child = coords_node->InsertEndChild(TiXmlElement(YBOTTOM_TAG));
	if (child)
		PutLongToXMLNode(child, this->GetTextureCoords().bottom);

	if ((this->textureSlices.left != 0) || (this->textureSlices.top != 0) || (this->textureSlices.right != 0) || (this->textureSlices.bottom != 0)) {
		TiXmlNode* slices_node = texture_node->InsertEndChild(TiXmlElement(SLICES_TAG));
		if (slices_node) {
			child = slices_node->InsertEndChild(TiXmlElement(XLEFT_TAG));
			if (child) {
				PutLongToXMLNode(child, this->GetTextureSlices().left);
			}

			child = slices_node->InsertEndChild(TiXmlElement(YTOP_TAG));
			if (child) {
				PutLongToXMLNode(child, this->GetTextureSlices().top);
			}

			child = slices_node->InsertEndChild(TiXmlElement(XRIGHT_TAG));
			if (child) {
				PutLongToXMLNode(child, this->GetTextureSlices().right);
			}

			child = slices_node->InsertEndChild(TiXmlElement(YBOTTOM_TAG));
			if (child) {
				PutLongToXMLNode(child, this->GetTextureSlices().bottom);
			}
		}
	}

	return true;
}

bool CDXUTElement::Save(TiXmlNode* parent, IMenuDialog* pDialog, unsigned int iUsesFontFlags, unsigned int iUsesTextureFlags) {
	bool bSuccess = false;

	if (parent) {
		TiXmlNode* child;

		if (iUsesFontFlags) {
			child = parent->InsertEndChild(TiXmlElement(TEXT_FORMAT_TAG));
			if (child) {
				PutDWORDToXMLNode(child, this->GetTextFormat());
			}

			child = parent->InsertEndChild(TiXmlElement(TEXT_OFFSET_X_TAG));
			if (child) {
				PutIntToXMLNode(child, this->GetTextOffsetX());
			}

			child = parent->InsertEndChild(TiXmlElement(TEXT_OFFSET_Y_TAG));
			if (child) {
				PutIntToXMLNode(child, this->GetTextOffsetY());
			}

			SaveFont(parent, pDialog);

			if (this->GetFontIndex() != -1) {
				child = parent->InsertEndChild(TiXmlElement(FONT_COLOR_TAG));
				if (child) {
					this->fontColor.Save(child, iUsesFontFlags);
				}
			}
		}

		if (iUsesTextureFlags) {
			SaveTexture(parent, pDialog);

			if (this->iTexture != -1) {
				child = parent->InsertEndChild(TiXmlElement(TEXTURE_COLOR_TAG));
				if (child) {
					this->textureColor.Save(child, iUsesTextureFlags);
				}
			}
		}
	}

	return bSuccess;
}

void CDXUTElement::SetTexture(size_t iTexture_, RECT* ptextureCoords, D3DCOLOR defaultTextureColor) {
	this->iTexture = iTexture_;

	if (ptextureCoords)
		SetTextureCoords(*ptextureCoords);
	else
		EmptyTextureCoords();

	textureColor.Init(defaultTextureColor);

	texInfoNeedsUpdate = true;
}

void CDXUTElement::SetFont(size_t iFont, D3DCOLOR defaultFontColor, DWORD dwTextFormat) {
	this->SetFontIndex(iFont);
	this->SetTextFormat(dwTextFormat);

	fontColor.Init(defaultFontColor);
}

void CDXUTElement::Refresh() {
	textureColor.SetCurrentColor(textureColor.GetColor(DXUT_STATE_HIDDEN));
	fontColor.SetCurrentColor(fontColor.GetColor(DXUT_STATE_HIDDEN));
}

//Ankit ----- WordWrapper Class function  implementations
int WordWrapper::DrawWordWrappedText(IMenuDialog* m_pDialog, const wchar_t* strText, CDXUTElement* pElement, RECT* prcDest, bool bShadow, int nCount) {
	if (!m_needToRecalcWordWrapping) {
		m_needToRecalcWordWrapping |= (m_lastCachedElement != pElement);
		m_needToRecalcWordWrapping |= ((prcDest->bottom != m_lastCachedRect.bottom) || (prcDest->top != m_lastCachedRect.top) || (prcDest->left != m_lastCachedRect.left) || (prcDest->right != m_lastCachedRect.right));
		m_needToRecalcWordWrapping |= (m_lastCachedShadowFlag != bShadow);
		m_needToRecalcWordWrapping |= (wcscmp(strText, m_lastCachedString.c_str()) != 0);
	}

	if (m_needToRecalcWordWrapping) {
		if (CalculateWordWrappedStrings(m_pDialog, strText, pElement, prcDest, bShadow))
			return m_pDialog->DrawTextW(m_formattedString.c_str(), (CDXUTElement*)m_lastCachedElement, &m_lastCachedRect, m_lastCachedShadowFlag);
		else
			return m_pDialog->DrawTextW(strText, pElement, prcDest, bShadow, nCount);
	} else {
		int ret_height = m_pDialog->DrawTextW(m_formattedString.c_str(), (CDXUTElement*)m_lastCachedElement, &m_lastCachedRect, m_lastCachedShadowFlag);
		if (ret_height == 0)
			return m_pDialog->DrawTextW(strText, pElement, prcDest, bShadow, nCount);
		else
			return ret_height;
	}
}

bool WordWrapper::CalculateWordWrappedStrings(IMenuDialog* m_pDialog, const wchar_t* strText, CDXUTElement* pElement, RECT* prcDest, bool bShadow) {
	auto pGDRM = DXUTGetGlobalDialogResourceManager();
	if (!pGDRM) {
		return false;
	}

	ID3DXSprite* sprite = pGDRM->GetSprite();
	if (!sprite) {
		return false;
	}
	D3DXMATRIXA16 matTransform;
	D3DXMatrixIdentity(&matTransform);
	sprite->SetTransform(&matTransform);

	RECT rcScreen = pElement->GetTextRect(prcDest);

	m_formattedString.clear();

	m_needToRecalcWordWrapping = false;

	m_lastCachedShadowFlag = bShadow;

	m_lastCachedRect.bottom = prcDest->bottom;
	m_lastCachedRect.top = prcDest->top;
	m_lastCachedRect.right = prcDest->right;
	m_lastCachedRect.left = prcDest->left;

	m_lastCachedElement = pElement;

	m_lastCachedString.clear();
	m_lastCachedString.append(strText);

	std::wstring temp(m_lastCachedString);
	if (temp.empty()) {
		m_formattedString = m_lastCachedString;
		return true;
	}

	//Ankit ----- sequence of string seperated by newline present in the actual string
	std::vector<std::wstring> seqOfStrings;

	//Ankit ----- break the actual string into array of strings on newline character
	while (!temp.empty()) {
		int indexOfNewlineChar = temp.find(L"\n");
		if (indexOfNewlineChar != std::wstring::npos) {
			seqOfStrings.push_back(temp.substr(0, indexOfNewlineChar));
			temp.erase(0, indexOfNewlineChar + 1);
		} else {
			seqOfStrings.push_back(temp);
			temp.clear();
		}
	}

	CDXUTFontNode* pFontNode = m_pDialog->GetFont(pElement->GetFontIndex());
	if (!pFontNode || !pFontNode->pFont) {
		m_needToRecalcWordWrapping = true;
		return false;
	}

	int rcWidth = m_lastCachedRect.right - m_lastCachedRect.left;

	//Ankit ----- sequence of formatted strings to seperated by newline
	std::vector<std::wstring> seqOfFormattedStrings;
	for (auto it = seqOfStrings.begin(); it != seqOfStrings.end(); it++) {
		std::wstring tempFormattedString;
		std::wstring tempHolderOfCrntString(*it);

		int compStringHeight = pFontNode->pFont->DrawText(sprite, tempHolderOfCrntString.c_str(), -1, &rcScreen, (pElement->GetTextFormat() | DT_CALCRECT) & (~DT_WORDBREAK), (*(pElement->GetFontColor())).GetCurrentColor());
		if (compStringHeight == 0) {
			m_needToRecalcWordWrapping = true;
			return false;
		}
		int compStringWidth = (rcScreen.right - rcScreen.left);

		if (compStringWidth <= rcWidth) {
			tempFormattedString = tempHolderOfCrntString;
			seqOfFormattedStrings.push_back(tempFormattedString);
			continue;
		}

		while (!tempHolderOfCrntString.empty()) {
			int wordWrapIndex = tempHolderOfCrntString.size();

			int stringDimsWidth, stringDimsHeight;
			do {
				stringDimsHeight = pFontNode->pFont->DrawText(sprite, tempHolderOfCrntString.c_str(), wordWrapIndex, &rcScreen, (pElement->GetTextFormat() | DT_CALCRECT) & (~DT_WORDBREAK), (*(pElement->GetFontColor())).GetCurrentColor());
				if (stringDimsHeight == 0) {
					m_needToRecalcWordWrapping = true;
					return false;
				}
				stringDimsWidth = (rcScreen.right - rcScreen.left);

				if ((wordWrapIndex > 1) && (wordWrapIndex % 2))
					wordWrapIndex = (wordWrapIndex / 2) + 1;
				else
					wordWrapIndex /= 2;
			} while ((stringDimsWidth > rcWidth) && (wordWrapIndex > 0));

			if (wordWrapIndex == 0) {
				++wordWrapIndex;
			} else {
				wordWrapIndex *= 2;
			}
			int upperBound = wordWrapIndex * 2;
			upperBound = (upperBound > tempHolderOfCrntString.size()) ? tempHolderOfCrntString.size() : upperBound;

			bool needToFindWhiteSpace = true;
			do {
				if (++wordWrapIndex > upperBound) {
					--wordWrapIndex;
					needToFindWhiteSpace = false;
					break;
				}

				stringDimsHeight = pFontNode->pFont->DrawText(sprite, tempHolderOfCrntString.c_str(), wordWrapIndex, &rcScreen, (pElement->GetTextFormat() | DT_CALCRECT) & (~DT_WORDBREAK), (*(pElement->GetFontColor())).GetCurrentColor());
				if (stringDimsHeight == 0) {
					m_needToRecalcWordWrapping = true;
					return false;
				}
				stringDimsWidth = (rcScreen.right - rcScreen.left);
			} while (stringDimsWidth <= rcWidth);

			if (needToFindWhiteSpace) {
				--wordWrapIndex;
				int largeWordFailBound = wordWrapIndex;
				while (!iswspace(tempHolderOfCrntString[wordWrapIndex])) {
					if ((--wordWrapIndex) == -1)
						break;
				}

				if (wordWrapIndex == -1) {
					wordWrapIndex = largeWordFailBound;
				}

				if (wordWrapIndex < largeWordFailBound) {
					++wordWrapIndex;
				}
			}

			//Ankit ----- Microsoft's DrawText cannot handle spaces at end of a particular line
			tempFormattedString.append(tempHolderOfCrntString.substr(0, wordWrapIndex));
			while (!tempFormattedString.empty() && tempFormattedString.back() == ' ') {
				tempFormattedString.pop_back();
			}
			tempFormattedString += L"\n";
			tempHolderOfCrntString.erase(0, wordWrapIndex);
		}

		tempFormattedString.erase((tempFormattedString.size() - 1), 1);
		seqOfFormattedStrings.push_back(tempFormattedString);
		continue;
	}

	for (auto it = seqOfFormattedStrings.begin(); (it + 1) != seqOfFormattedStrings.end(); it++) {
		m_formattedString.append(*it);
		m_formattedString += L"\n";
	}
	m_formattedString.append(seqOfFormattedStrings.back());

	return true;
}

void WordWrapper::Setm_lastCachedElement(CDXUTElement* pElement) {
	m_lastCachedElement = pElement;
}

void WordWrapper::Setm_lastCachedRect(RECT* prcDest) {
	m_lastCachedRect.bottom = prcDest->bottom;
	m_lastCachedRect.top = prcDest->top;
	m_lastCachedRect.right = prcDest->right;
	m_lastCachedRect.left = prcDest->left;
}

void WordWrapper::Setm_lastCachedShadowFlag(bool bShadow) {
	m_lastCachedShadowFlag = bShadow;
}

void WordWrapper::Setm_lastCachedString(const wchar_t* strText) {
	m_lastCachedString.clear();
	m_lastCachedString.append(strText);
}

void WordWrapper::SetRecalcFlag(bool recalcFlag) {
	m_needToRecalcWordWrapping = recalcFlag;
}

} // namespace KEP