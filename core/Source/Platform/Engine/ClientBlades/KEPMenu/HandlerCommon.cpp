///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <Windows.h>
#include "Handler.h"
#include "MenuCInterface.h"
#include "LuaHandler.h"
#include "scriptVM.h"
#include "../imenugui.h"
#include <js.h>

#include "LogHelper.h"

namespace KEP {

namespace Handlers {
	bool OnDialogEvent(IMenuDialog *pDialog, const char *eventName, POINT pt);
	bool OnDialogEvent(IMenuDialog *pDialog, const char *eventName, unsigned int key, BOOL bShiftDown, BOOL bControlDown, BOOL bAltDown);
	bool OnControlEvent(IMenuControl *pControl, const char *eventName);
	bool OnControlEvent(IMenuControl *pControl, const char *eventName, POINT pt);
}

std::string GetMenuScriptFilePathFromState(IScriptVM *state) {
	int slot = FindSlotByState(state);
	if (slot >= 0)
		return g_map[slot].scriptFilePath;
	return "<null>";
}

std::string GetMenuScriptFilePathFromDialog(IMenuDialog *pDialog) {
	int slot = FindSlotByDialog(pDialog);
	if (slot >= 0)
		return g_map[slot].scriptFilePath;
	return "<null>";
}

IScriptVM *GetScriptVMFromDialog(IMenuDialog *pDialog) {
	int slot = FindSlotByDialog(pDialog);
	if (slot >= 0)
		return MAKE_SCRIPT_VM(g_map[slot].state->GetNativeHandle());
	return 0;
}

void LogMenuScripts() {
	LogInstance("Script");
	for (int i = 0; i < MAX_NODES; i++) {
		if (g_map[i].pDialog) {
			lua_State *state = g_map[i].state->GetNativeHandle();
			IMenuDialog *pDialog = g_map[i].pDialog;
			LogInfo("g_map[" << i << "]: Script <" << pDialog << ":" << state << "> path=" << g_map[i].scriptFilePath);
		}
	}
}

int FindOpenSlot() {
	for (int i = 0; i < MAX_NODES; i++)
		if (!g_map[i].pDialog)
			return i;
	return -1;
}

int FindSlotByState(IScriptVM *state) {
	for (int i = 0; i < MAX_NODES; i++)
		if ((void *)g_map[i].state == (void *)state)
			return i;
	return -1;
}

int FindSlotByDialog(IMenuDialog *pDialog) {
	for (int i = 0; i < MAX_NODES; i++)
		if (g_map[i].pDialog == pDialog)
			return i;
	return -1;
}

void RegisterScriptFunctions(STATE_TYPE *state) {
	///////////////////////////////////////////////////////////////////////////
	// Cursor Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<BOOL, BOOL>("Cursor", "SetEnabled", Cursor_SetEnabled);

	state->RegisterNativeFunction<int>("Cursor", "GetLocationX", Cursor_GetLocationX);

	state->RegisterNativeFunction<int>("Cursor", "GetLocationY", Cursor_GetLocationY);

	///////////////////////////////////////////////////////////////////////////
	// Dialog/Menu Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<BOOL, const char *>("Dialog", "MsgBox", Dialog_MsgBox);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "RegisterGenericEvents", Dialog_RegisterGenericEvents);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "UnRegisterGenericEvents", Dialog_UnRegisterGenericEvents);

	state->RegisterNativeFunction<int>("Dialog", "NumOpen", Dialog_NumOpen);

	state->RegisterNativeFunction<BOOL>("Dialog", "Log", Dialog_Log);

	state->RegisterNativeFunction<const char *, IMenuDialog *>("Dialog", "MenuId", Dialog_MenuId);

	state->RegisterNativeFunction<const char *, IMenuDialog *>("Dialog", "MenuName", Dialog_MenuName);

	state->RegisterNativeFunction<const char *, IMenuDialog *>("Dialog", "ScriptName", Dialog_ScriptName);

	state->RegisterNativeFunction<IMenuDialog *>("Dialog", "New", Dialog_New);

	state->RegisterNativeFunction<IMenuDialog *, const char *, const char *>("Dialog", "CreateAs", Dialog_CreateAs);
	state->RegisterNativeFunction<bool, const char *, const char *>("Menu", "OpenAs", Menu_OpenAs);

	state->RegisterNativeFunction<IMenuDialog *, const char *>("Dialog", "Create", Dialog_Create);
	state->RegisterNativeFunction<bool, const char *>("Menu", "Open", Menu_Open);

	state->RegisterNativeFunction<IMenuDialog *, const char *, const char *>("Dialog", "CreateTrustedAs", Dialog_CreateTrustedAs);
	state->RegisterNativeFunction<bool, const char *, const char *>("Menu", "OpenTrustedAs", Menu_OpenTrustedAs);

	state->RegisterNativeFunction<IMenuDialog *, const char *>("Dialog", "CreateTrusted", Dialog_CreateTrusted);
	state->RegisterNativeFunction<bool, const char *>("Menu", "OpenTrusted", Menu_OpenTrusted);

	state->RegisterNativeFunction<IMenuDialog *, const char *, BOOL>("Dialog", "CreateAndEdit", Dialog_CreateAndEdit);
	state->RegisterNativeFunction<bool, const char *>("Menu", "OpenForEdit", Menu_OpenForEdit);

	state->RegisterNativeFunction<IMenuDialog *, const char *, int, int>("Dialog", "CreateAtCoordinates", Dialog_CreateAtCoordinates);
	state->RegisterNativeFunction<bool, const char *, int, int>("Menu", "OpenAtCoordinates", Menu_OpenAtCoordinates);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "Destroy", Dialog_Destroy);
	state->RegisterNativeFunction<bool, const char *>("Menu", "Close", Menu_Close);

	state->RegisterNativeFunction<BOOL, BOOL>("Dialog", "DestroyAll", Dialog_DestroyAll);
	state->RegisterNativeFunction<bool>("Menu", "CloseAll", Menu_CloseAll);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "DestroyAllExcept", Dialog_DestroyAllExcept);
	state->RegisterNativeFunction<bool, const char *>("Menu", "CloseAllExcept", Menu_CloseAllExcept);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "DestroyAllNonEssentialExcept", Dialog_DestroyAllNonEssentialExcept);
	state->RegisterNativeFunction<bool, const char *, BOOL>("Menu", "CloseAllNonEssentialExcept", Menu_CloseAllNonEssentialExcept);

	state->RegisterNativeFunction<BOOL, const char *>("Dialog", "IsDialogOpenByName", Dialog_IsDialogOpenByName);
	state->RegisterNativeFunction<bool, const char *>("Menu", "IsOpen", Menu_IsOpen);

	state->RegisterNativeFunction<IMenuDialog *, const char *>("Dialog", "GetDialogByName", Dialog_GetDialogByName);
	state->RegisterNativeFunction<IMenuDialog *, const char *>("Menu", "GetDialog", Menu_GetDialog);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, const char *>("Dialog", "Save", Dialog_Save);
	state->RegisterNativeFunction<bool, const char *, const char *>("Menu", "Save", Menu_Save);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "CopyTemplate", Dialog_CopyTemplate);
	state->RegisterNativeFunction<bool, const char *>("Menu", "CopyTemplate", Menu_CopyTemplate);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "ApplyInputSettings", Dialog_ApplyInputSettings);
	state->RegisterNativeFunction<bool, const char *>("Menu", "ApplyInputSettings", Menu_ApplyInputSettings);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "SendToBack", Dialog_SendToBack);
	state->RegisterNativeFunction<bool, const char *>("Menu", "SendToBack", Menu_SendToBack);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "BringToFrontEnable", Dialog_BringToFrontEnable);
	state->RegisterNativeFunction<bool, const char *, BOOL>("Menu", "BringToFrontEnable", Menu_BringToFrontEnable);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "BringToFront", Dialog_BringToFront);
	state->RegisterNativeFunction<bool, const char *>("Menu", "BringToFront", Menu_BringToFront);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "AlwaysBringToFront", Dialog_AlwaysBringToFront);
	state->RegisterNativeFunction<bool, const char *>("Menu", "AlwaysBringToFront", Menu_AlwaysBringToFront);

	state->RegisterNativeFunction<BOOL>("Dialog", "AlwaysBringToFrontDisable", Dialog_AlwaysBringToFrontDisable);
	state->RegisterNativeFunction<bool>("Menu", "AlwaysBringToFrontDisable", Menu_AlwaysBringToFrontDisable);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "ClearFocus", Dialog_ClearFocus);

	state->RegisterNativeFunction<IMenuControl *, IMenuDialog *, const char *>("Dialog", "GetControl", Dialog_GetControl);

	state->RegisterNativeFunction<int, IMenuDialog *>("Dialog", "GetNumControls", Dialog_GetNumControls);

	state->RegisterNativeFunction<IMenuControl *, IMenuDialog *, int>("Dialog", "GetControlByIndex", Dialog_GetControlByIndex);

	state->RegisterNativeFunction<IMenuStatic *, IMenuDialog *, const char *>("Dialog", "GetStatic", Dialog_GetStatic);

	state->RegisterNativeFunction<IMenuImage *, IMenuDialog *, const char *>("Dialog", "GetImage", Dialog_GetImage);

	state->RegisterNativeFunction<IMenuButton *, IMenuDialog *, const char *>("Dialog", "GetButton", Dialog_GetButton);

	state->RegisterNativeFunction<IMenuCheckBox *, IMenuDialog *, const char *>("Dialog", "GetCheckBox", Dialog_GetCheckBox);

	state->RegisterNativeFunction<IMenuRadioButton *, IMenuDialog *, const char *>("Dialog", "GetRadioButton", Dialog_GetRadioButton);

	state->RegisterNativeFunction<IMenuComboBox *, IMenuDialog *, const char *>("Dialog", "GetComboBox", Dialog_GetComboBox);

	state->RegisterNativeFunction<IMenuSlider *, IMenuDialog *, const char *>("Dialog", "GetSlider", Dialog_GetSlider);

	state->RegisterNativeFunction<IMenuEditBox *, IMenuDialog *, const char *>("Dialog", "GetEditBox", Dialog_GetEditBox);

	state->RegisterNativeFunction<IMenuEditBox *, IMenuDialog *, const char *>("Dialog", "GetSimpleEditBox", Dialog_GetEditBox);

	state->RegisterNativeFunction<IMenuListBox *, IMenuDialog *, const char *>("Dialog", "GetListBox", Dialog_GetListBox);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, const char *, int, int, int>("Dialog", "GetBrowserPage", Dialog_GetBrowserPage);

	state->RegisterNativeFunction<BOOL, const char *, int, int, int>("Dialog", "MakeWebCall", Dialog_MakeWebCall);

	state->RegisterNativeFunction<int>("Dialog", "GetNumTextures", Dialog_GetNumTextures);

	state->RegisterNativeFunction<const char *, int>("Dialog", "GetTextureByIndex", Dialog_GetTextureByIndex);

	state->RegisterNativeFunction<IMenuStatic *, IMenuDialog *, const char *, const char *, int, int, int, int>("Dialog", "AddStatic", Dialog_AddStatic);

	state->RegisterNativeFunction<HRESULT, IMenuDialog *, IMenuControl *>("Dialog", "AddControl", Dialog_AddControl);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, const char *>("Dialog", "RemoveControl", Dialog_RemoveControl);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "FocusDefaultControl", Dialog_FocusDefaultControl);

	state->RegisterNativeFunction<IMenuImage *, IMenuDialog *, const char *, int, int, const char *, int, int, int, int>("Dialog", "AddImage", Dialog_AddImage);

	state->RegisterNativeFunction<IMenuImage *, IMenuDialog *, const char *, int, int, const char *, int, int, int, int>("Dialog", "AddImageToBack", Dialog_AddImageToBack);

	state->RegisterNativeFunction<IMenuButton *, IMenuDialog *, const char *, const char *, int, int, int, int>("Dialog", "AddButton", Dialog_AddButton);

	state->RegisterNativeFunction<IMenuCheckBox *, IMenuDialog *, const char *, const char *, int, int, int, int, BOOL>("Dialog", "AddCheckBox", Dialog_AddCheckBox);

	state->RegisterNativeFunction<IMenuRadioButton *, IMenuDialog *, const char *, unsigned int, const char *, int, int, int, int, BOOL>("Dialog", "AddRadioButton", Dialog_AddRadioButton);

	state->RegisterNativeFunction<IMenuComboBox *, IMenuDialog *, const char *, int, int, int, int>("Dialog", "AddComboBox", Dialog_AddComboBox);

	state->RegisterNativeFunction<IMenuSlider *, IMenuDialog *, const char *, int, int, int, int>("Dialog", "AddSlider", Dialog_AddSlider);

	state->RegisterNativeFunction<IMenuEditBox *, IMenuDialog *, const char *, const char *, int, int, int, int>("Dialog", "AddEditBox", Dialog_AddEditBox);

	state->RegisterNativeFunction<IMenuEditBox *, IMenuDialog *, const char *, const char *, int, int, int, int>("Dialog", "AddSimpleEditBox", Dialog_AddSimpleEditBox);

	state->RegisterNativeFunction<IMenuListBox *, IMenuDialog *, const char *, int, int, int, int>("Dialog", "AddListBox", Dialog_AddListBox);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, const char *>("Dialog", "RemoveControl", Dialog_RemoveControl);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, IMenuControl *>("Dialog", "CopyControl", Dialog_CopyControl);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "PasteControl", Dialog_PasteControl);

	state->RegisterNativeFunction<bool>("Dialog", "IsSomethingInClipboard", Dialog_IsSomethingInClipboard);

	state->RegisterNativeFunction<const char *, IMenuDialog *, IMenuControl *>("Dialog", "GetControlXml", Dialog_GetControlXml);

	state->RegisterNativeFunction<IMenuControl *, IMenuDialog *, const char *>("Dialog", "LoadControlByXml", Dialog_LoadControlByXml);

	state->RegisterNativeFunction<const char *, IMenuDialog *>("Dialog", "GetXmlName", Dialog_GetXmlName);

	state->RegisterNativeFunction<IMenuElement *, IMenuDialog *, const char *>("Dialog", "GetDisplayElement", Dialog_GetDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuDialog *>("Dialog", "GetCaptionElement", Dialog_GetCaptionElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuDialog *>("Dialog", "GetBodyElement", Dialog_GetBodyElement);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, const char *, unsigned int>("Dialog", "GetElementTagUsesFontState", Dialog_GetElementTagUsesFontState);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, const char *, unsigned int>("Dialog", "GetElementTagUsesTextureState", Dialog_GetElementTagUsesTextureState);

	state->RegisterNativeFunction<const char *, IMenuDialog *>("Dialog", "GetCaptionText", Dialog_GetCaptionText);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "GetMinimized", Dialog_GetMinimized);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "GetEdit", Dialog_GetEdit);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "GetCaptionEnabled", Dialog_GetCaptionEnabled);

	state->RegisterNativeFunction<int, IMenuDialog *>("Dialog", "GetCaptionHeight", Dialog_GetCaptionHeight);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "GetCaptionShadow", Dialog_GetCaptionShadow);

	state->RegisterNativeFunction<COLOR, IMenuDialog *>("Dialog", "GetColorTopLeft", Dialog_GetColorTopLeft);

	state->RegisterNativeFunction<COLOR, IMenuDialog *>("Dialog", "GetColorTopRight", Dialog_GetColorTopRight);

	state->RegisterNativeFunction<COLOR, IMenuDialog *>("Dialog", "GetColorBottomLeft", Dialog_GetColorBottomLeft);

	state->RegisterNativeFunction<COLOR, IMenuDialog *>("Dialog", "GetColorBottomRight", Dialog_GetColorBottomRight);

	state->RegisterNativeFunction<int, IMenuDialog *>("Dialog", "GetCursorLocationX", Dialog_GetCursorLocationX);

	state->RegisterNativeFunction<int, IMenuDialog *>("Dialog", "GetCursorLocationY", Dialog_GetCursorLocationY);

	state->RegisterNativeFunction<int, IMenuDialog *>("Dialog", "GetLocationX", Dialog_GetLocationX);

	state->RegisterNativeFunction<int, IMenuDialog *>("Dialog", "GetLocationY", Dialog_GetLocationY);

	state->RegisterNativeFunction<int, IMenuDialog *>("Dialog", "GetWidth", Dialog_GetWidth);

	state->RegisterNativeFunction<int, IMenuDialog *>("Dialog", "GetHeight", Dialog_GetHeight);

	state->RegisterNativeFunction<int, IMenuDialog *>("Dialog", "GetScreenHeight", Dialog_GetScreenHeight);

	state->RegisterNativeFunction<int, IMenuDialog *>("Dialog", "GetScreenWidth", Dialog_GetScreenWidth);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "IsLMouseDown", Dialog_IsLMouseDown);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "IsRMouseDown", Dialog_IsRMouseDown);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "GetSnapToEdges", Dialog_GetSnapToEdges);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "GetModal", Dialog_GetModal);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "GetPassthrough", Dialog_GetPassthrough);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "GetMovable", Dialog_GetMovable);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "GetEnableMouseInput", Dialog_GetEnableMouseInput);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "GetEnableKeyboardInput", Dialog_GetEnableKeyboardInput);

	state->RegisterNativeFunction<const char *, IMenuDialog *>("Dialog", "GetScriptName", Dialog_GetScriptName);

	state->RegisterNativeFunction<IMenuDialog *, IMenuDialog *>("Dialog", "GetNextDialog", Dialog_GetNextDialog);

	state->RegisterNativeFunction<IMenuDialog *, IMenuDialog *>("Dialog", "GetPreviousDialog", Dialog_GetPreviousDialog);

	state->RegisterNativeFunction<unsigned int, IMenuDialog *>("Dialog", "GetLocationFormat", Dialog_GetLocationFormat);

	state->RegisterNativeFunction<int, IMenuDialog *>("Dialog", "GetId", Dialog_GetId);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "SetMinimized", Dialog_SetMinimized);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL, BOOL>("Dialog", "SetAllControls", Dialog_SetAllControls);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "SetEdit", Dialog_SetEdit);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, COLOR>("Dialog", "SetBackgroundColor", Dialog_SetBackgroundColor);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, RAWCOLOR>("Dialog", "SetBackgroundColorRaw", Dialog_SetBackgroundColorRaw);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, COLOR, COLOR, COLOR, COLOR>("Dialog", "SetBackgroundColors", Dialog_SetBackgroundColors);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, RAWCOLOR, RAWCOLOR, RAWCOLOR, RAWCOLOR>("Dialog", "SetBackgroundColorsRaw", Dialog_SetBackgroundColorsRaw);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "SetCaptionEnabled", Dialog_SetCaptionEnabled);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, int>("Dialog", "SetCaptionHeight", Dialog_SetCaptionHeight);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "SetCaptionShadow", Dialog_SetCaptionShadow);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, const char *>("Dialog", "SetCaptionText", Dialog_SetCaptionText);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, int>("Dialog", "SetId", Dialog_SetId);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, int, int>("Dialog", "SetLocation", Dialog_SetLocation);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, int>("Dialog", "SetLocationX", Dialog_SetLocationX);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, int>("Dialog", "SetLocationY", Dialog_SetLocationY);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, int>("Dialog", "SetHeight", Dialog_SetHeight);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, int>("Dialog", "SetWidth", Dialog_SetWidth);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, int, int>("Dialog", "SetSize", Dialog_SetSize);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "SetSnapToEdges", Dialog_SetSnapToEdges);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "SetModal", Dialog_SetModal);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "SetCaptureKeys", Dialog_SetCaptureKeys);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "SetPassthrough", Dialog_SetPassthrough);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "SetMovable", Dialog_SetMovable);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "SetKeyboardInputEnabled", Dialog_SetKeyboardInputEnabled);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "SetMouseInputEnabled", Dialog_SetMouseInputEnabled);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "SetCloseOnESC", Dialog_SetCloseOnESC);

	state->RegisterNativeFunction<BOOL, IMenuDialog *>("Dialog", "GetCloseOnESC", Dialog_GetCloseOnESC);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, IMenuDialog *>("Dialog", "SetNextDialog", Dialog_SetNextDialog);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, IMenuDialog *>("Dialog", "SetPreviousDialog", Dialog_SetPreviousDialog);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, const char *>("Dialog", "SetScriptName", Dialog_SetScriptName);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, unsigned int>("Dialog", "SetLocationFormat", Dialog_SetLocationFormat);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "SetEnabled", Dialog_SetEnabled);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, const char *>("Dialog", "SetXmlName", Dialog_SetXmlName);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, IMenuControl *>("Dialog", "MoveControlForward", Dialog_MoveControlForward);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, IMenuControl *>("Dialog", "MoveControlBackward", Dialog_MoveControlBackward);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, IMenuControl *>("Dialog", "MoveControlToFront", Dialog_MoveControlToFront);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, IMenuControl *>("Dialog", "MoveControlToBack", Dialog_MoveControlToBack);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, IMenuControl *, unsigned int>("Dialog", "MoveControlToIndex", Dialog_MoveControlToIndex);

	state->RegisterNativeFunction<BOOL, IMenuDialog *, BOOL>("Dialog", "SetSelected", Dialog_SetSelected);

	///////////////////////////////////////////////////////////////////////////
	// BlendColor Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<void, IMenuBlendColor *, COLOR, COLOR, COLOR>("BlendColor", "Init", BlendColor_Init);

	state->RegisterNativeFunction<void, IMenuBlendColor *, RAWCOLOR, RAWCOLOR, RAWCOLOR>("BlendColor", "InitRaw", BlendColor_InitRaw);

	state->RegisterNativeFunction<COLOR, IMenuBlendColor *, int>("BlendColor", "GetColor", BlendColor_GetColor);

	state->RegisterNativeFunction<RAWCOLOR, IMenuBlendColor *, int>("BlendColor", "GetColorRaw", BlendColor_GetColorRaw);

	state->RegisterNativeFunction<void, IMenuBlendColor *, int, COLOR>("BlendColor", "SetColor", BlendColor_SetColor);

	state->RegisterNativeFunction<void, IMenuBlendColor *, int, RAWCOLOR>("BlendColor", "SetColorRaw", BlendColor_SetColorRaw);

	///////////////////////////////////////////////////////////////////////////
	// Element Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<IMenuBlendColor *, IMenuElement *>("Element", "GetTextureColor", Element_GetTextureColor);

	state->RegisterNativeFunction<IMenuBlendColor *, IMenuElement *>("Element", "GetFontColor", Element_GetFontColor);

	state->RegisterNativeFunction<const char *, IMenuElement *, IMenuDialog *>("Element", "GetFontFace", Element_GetFontFace);

	state->RegisterNativeFunction<int, IMenuElement *, IMenuDialog *>("Element", "GetFontHeight", Element_GetFontHeight);

	state->RegisterNativeFunction<int, IMenuElement *, IMenuDialog *>("Element", "GetFontWeight", Element_GetFontWeight);

	state->RegisterNativeFunction<BOOL, IMenuElement *, IMenuDialog *>("Element", "GetFontItalic", Element_GetFontItalic);

	state->RegisterNativeFunction<BOOL, IMenuElement *, IMenuDialog *>("Element", "GetFontUnderline", Element_GetFontUnderline);

	state->RegisterNativeFunction<unsigned int, IMenuElement *>("Element", "GetTextFormat", Element_GetTextFormat);

	state->RegisterNativeFunction<const char *, IMenuElement *, IMenuDialog *>("Element", "GetTextureName", Element_GetTextureName);

	state->RegisterNativeFunction<int, IMenuElement *, IMenuDialog *>("Element", "GetTextureWidth", Element_GetTextureWidth);

	state->RegisterNativeFunction<int, IMenuElement *, IMenuDialog *>("Element", "GetTextureHeight", Element_GetTextureHeight);

	state->RegisterNativeFunction<int, IMenuElement *>("Element", "GetCoordLeft", Element_GetCoordLeft);

	state->RegisterNativeFunction<int, IMenuElement *>("Element", "GetCoordRight", Element_GetCoordRight);

	state->RegisterNativeFunction<int, IMenuElement *>("Element", "GetCoordTop", Element_GetCoordTop);

	state->RegisterNativeFunction<int, IMenuElement *>("Element", "GetCoordBottom", Element_GetCoordBottom);

	state->RegisterNativeFunction<int, IMenuElement *>("Element", "GetSliceLeft", Element_GetSliceLeft);

	state->RegisterNativeFunction<int, IMenuElement *>("Element", "GetSliceRight", Element_GetSliceRight);

	state->RegisterNativeFunction<int, IMenuElement *>("Element", "GetSliceTop", Element_GetSliceTop);

	state->RegisterNativeFunction<int, IMenuElement *>("Element", "GetSliceBottom", Element_GetSliceBottom);

	state->RegisterNativeFunction<void, IMenuElement *, unsigned long>("Element", "SetTextFormat", Element_SetTextFormat);

	state->RegisterNativeFunction<void, IMenuElement *, int, int, int, int>("Element", "SetCoords", Element_SetCoords);

	state->RegisterNativeFunction<void, IMenuElement *, int, int, int, int>("Element", "SetSlices", Element_SetSlices);

	state->RegisterNativeFunction<void, IMenuElement *, IMenuDialog *, const char *>("Element", "AddTexture", Element_AddTexture);

	state->RegisterNativeFunction<void, IMenuElement *, IMenuDialog *, const char *, const char *>("Element", "AddExternalTexture", Element_AddExternalTexture);

	state->RegisterNativeFunction<void, IMenuElement *, IMenuDialog *, unsigned>("Element", "AddDynamicTexture", Element_AddDynamicTexture);

	state->RegisterNativeFunction<void, IMenuElement *, IMenuDialog *, const char *, long, long, BOOL>("Element", "AddFont", Element_AddFont);

	///////////////////////////////////////////////////////////////////////////
	// Control Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<BOOL, IMenuControl *, BOOL>("Control", "SetPassthrough", Control_SetPassthrough);

	state->RegisterNativeFunction<IMenuDialog *, IMenuControl *>("Control", "GetDialog", Control_GetDialog);

	state->RegisterNativeFunction<const char *, IMenuControl *>("Control", "GetName", Control_GetName);

	state->RegisterNativeFunction<BOOL, IMenuControl *>("Control", "GetEnabled", Control_GetEnabled);

	state->RegisterNativeFunction<BOOL, IMenuControl *>("Control", "GetVisible", Control_GetVisible);

	state->RegisterNativeFunction<BOOL, IMenuControl *>("Control", "GetMouseOver", Control_GetMouseOver);

	state->RegisterNativeFunction<int, IMenuControl *>("Control", "GetLocationX", Control_GetLocationX);

	state->RegisterNativeFunction<int, IMenuControl *>("Control", "GetLocationY", Control_GetLocationY);

	state->RegisterNativeFunction<int, IMenuControl *>("Control", "GetWidth", Control_GetWidth);

	state->RegisterNativeFunction<int, IMenuControl *>("Control", "GetHeight", Control_GetHeight);

	state->RegisterNativeFunction<unsigned int, IMenuControl *>("Control", "GetHotkey", Control_GetHotkey);

	state->RegisterNativeFunction<tDATA, IMenuControl *>("Control", "GetUserData", Control_GetUserData);

	state->RegisterNativeFunction<BOOL, IMenuControl *>("Control", "GetFocus", Control_GetFocus);

	state->RegisterNativeFunction<BOOL, IMenuControl *, const char *>("Control", "GetElementTagUsesFont", Control_GetElementTagUsesFont);

	state->RegisterNativeFunction<BOOL, IMenuControl *, const char *, unsigned int>("Control", "GetElementTagUsesFontState", Control_GetElementTagUsesFontState);

	state->RegisterNativeFunction<BOOL, IMenuControl *, const char *>("Control", "GetElementTagUsesTexture", Control_GetElementTagUsesTexture);

	state->RegisterNativeFunction<BOOL, IMenuControl *, const char *, unsigned int>("Control", "GetElementTagUsesTextureState", Control_GetElementTagUsesTextureState);

	state->RegisterNativeFunction<IMenuStatic *, IMenuControl *>("Control", "GetStatic", Control_GetStatic);

	state->RegisterNativeFunction<IMenuImage *, IMenuControl *>("Control", "GetImage", Control_GetImage);

	state->RegisterNativeFunction<IMenuButton *, IMenuControl *>("Control", "GetButton", Control_GetButton);

	state->RegisterNativeFunction<IMenuCheckBox *, IMenuControl *>("Control", "GetCheckBox", Control_GetCheckBox);

	state->RegisterNativeFunction<IMenuRadioButton *, IMenuControl *>("Control", "GetRadioButton", Control_GetRadioButton);

	state->RegisterNativeFunction<IMenuComboBox *, IMenuControl *>("Control", "GetComboBox", Control_GetComboBox);

	state->RegisterNativeFunction<IMenuSlider *, IMenuControl *>("Control", "GetSlider", Control_GetSlider);

	state->RegisterNativeFunction<IMenuEditBox *, IMenuControl *>("Control", "GetEditBox", Control_GetEditBox);

	state->RegisterNativeFunction<IMenuEditBox *, IMenuControl *>("Control", "GetSimpleEditBox", Control_GetEditBox);

	state->RegisterNativeFunction<IMenuListBox *, IMenuControl *>("Control", "GetListBox", Control_GetListBox);

	state->RegisterNativeFunction<int, IMenuControl *>("Control", "GetType", Control_GetType);

	state->RegisterNativeFunction<const char *, IMenuControl *>("Control", "GetComment", Control_GetComment);

	state->RegisterNativeFunction<const char *, IMenuControl *>("Control", "GetToolTip", Control_GetToolTip);

	state->RegisterNativeFunction<int, IMenuControl *>("Control", "GetGroupId", Control_GetGroupId);

	state->RegisterNativeFunction<IMenuElement *, IMenuControl *, const char *>("Control", "GetDisplayElement", Control_GetDisplayElement);

	state->RegisterNativeFunction<void, IMenuControl *, const char *>("Control", "SetName", Control_SetName);

	state->RegisterNativeFunction<void, IMenuControl *, const char *>("Control", "SetNameUnconflicting", Control_SetNameUnconflicting);

	state->RegisterNativeFunction<void, IMenuControl *, BOOL>("Control", "SetEnabled", Control_SetEnabled);

	state->RegisterNativeFunction<void, IMenuControl *, BOOL>("Control", "SetVisible", Control_SetVisible);

	state->RegisterNativeFunction<void, IMenuControl *, int, int>("Control", "SetLocation", Control_SetLocation);

	state->RegisterNativeFunction<void, IMenuControl *, int>("Control", "SetLocationX", Control_SetLocationX);

	state->RegisterNativeFunction<void, IMenuControl *, int>("Control", "SetLocationY", Control_SetLocationY);

	state->RegisterNativeFunction<void, IMenuControl *, int, int>("Control", "SetSize", Control_SetSize);

	state->RegisterNativeFunction<void, IMenuControl *, unsigned int>("Control", "SetHotkey", Control_SetHotkey);

	state->RegisterNativeFunction<void, IMenuControl *, tDATA>("Control", "SetUserData", Control_SetUserData);

	state->RegisterNativeFunction<void, IMenuControl *>("Control", "SetFocus", Control_SetFocus);

	state->RegisterNativeFunction<void, IMenuControl *, BOOL>("Control", "SetEditing", Control_SetEditing);

	state->RegisterNativeFunction<void, IMenuControl *, const char *>("Control", "SetComment", Control_SetComment);

	state->RegisterNativeFunction<void, IMenuControl *, const char *>("Control", "SetToolTip", Control_SetToolTip);

	state->RegisterNativeFunction<void, IMenuControl *, int>("Control", "SetGroupId", Control_SetGroupId);

	state->RegisterNativeFunction<void, IMenuControl *, int, int, int, int>("Control", "SetClipRect", Control_SetClipRect);

	state->RegisterNativeFunction<BOOL, IMenuControl *, int, int>("Control", "ContainsPoint", Control_ContainsPoint);

	///////////////////////////////////////////////////////////////////////////
	// Static Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<IMenuControl *, IMenuStatic *>("Static", "GetControl", Static_GetControl);

	state->RegisterNativeFunction<IMenuElement *, IMenuStatic *>("Static", "GetDisplayElement", Static_GetDisplayElement);

	state->RegisterNativeFunction<const char *, IMenuStatic *>("Static", "GetText", Static_GetText);

	state->RegisterNativeFunction<BOOL, IMenuStatic *>("Static", "GetShadow", Static_GetShadow);

	state->RegisterNativeFunction<BOOL, IMenuStatic *>("Static", "GetUseHTML", Static_GetUseHTML);

	state->RegisterNativeFunction<void, IMenuStatic *, const char *>("Static", "SetText", Static_SetText);

	state->RegisterNativeFunction<int, IMenuStatic *, const char *>("Static", "GetStringWidth", Static_GetStringWidth);

	state->RegisterNativeFunction<int, IMenuStatic *, const char *>("Static", "GetStringHeight", Static_GetStringHeight);

	state->RegisterNativeFunction<void, IMenuStatic *, BOOL>("Static", "SetShadow", Static_SetShadow);

	state->RegisterNativeFunction<void, IMenuStatic *, BOOL>("Static", "SetUseHTML", Static_SetUseHTML);

	///////////////////////////////////////////////////////////////////////////
	// Image Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<IMenuControl *, IMenuImage *>("Image", "GetControl", Image_GetControl);

	state->RegisterNativeFunction<IMenuElement *, IMenuImage *>("Image", "GetDisplayElement", Image_GetDisplayElement);

	state->RegisterNativeFunction<void, IMenuImage *, COLOR>("Image", "SetColor", Image_SetColor);

	state->RegisterNativeFunction<void, IMenuImage *, RAWCOLOR>("Image", "SetColorRaw", Image_SetColorRaw);

	///////////////////////////////////////////////////////////////////////////
	// Button Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<IMenuControl *, IMenuButton *>("Button", "GetControl", Button_GetControl);

	state->RegisterNativeFunction<IMenuStatic *, IMenuButton *>("Button", "GetStatic", Button_GetStatic);

	state->RegisterNativeFunction<IMenuElement *, IMenuButton *>("Button", "GetNormalDisplayElement", Button_GetNormalDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuButton *>("Button", "GetMouseOverDisplayElement", Button_GetMouseOverDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuButton *>("Button", "GetPressedDisplayElement", Button_GetPressedDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuButton *>("Button", "GetFocusedDisplayElement", Button_GetFocusedDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuButton *>("Button", "GetDisabledDisplayElement", Button_GetDisabledDisplayElement);

	state->RegisterNativeFunction<void, IMenuButton *, BOOL>("Button", "SetSimpleMode", Button_SetSimpleMode);

	state->RegisterNativeFunction<BOOL, IMenuButton *>("Button", "GetSimpleMode", Button_GetSimpleMode);

	///////////////////////////////////////////////////////////////////////////
	// CheckBox Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<IMenuControl *, IMenuCheckBox *>("CheckBox", "GetControl", CheckBox_GetControl);

	state->RegisterNativeFunction<IMenuButton *, IMenuCheckBox *>("CheckBox", "GetButton", CheckBox_GetButton);

	state->RegisterNativeFunction<IMenuElement *, IMenuCheckBox *>("CheckBox", "GetBoxDisplayElement", CheckBox_GetBoxDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuCheckBox *>("CheckBox", "GetCheckDisplayElement", CheckBox_GetCheckDisplayElement);

	state->RegisterNativeFunction<BOOL, IMenuCheckBox *>("CheckBox", "GetChecked", CheckBox_GetChecked);

	state->RegisterNativeFunction<LONG, IMenuCheckBox *>("CheckBox", "GetIconWidth", CheckBox_GetIconWidth);

	state->RegisterNativeFunction<void, IMenuCheckBox *, BOOL>("CheckBox", "SetChecked", CheckBox_SetChecked);

	state->RegisterNativeFunction<void, IMenuCheckBox *, LONG>("CheckBox", "SetIconWidth", CheckBox_SetIconWidth);

	///////////////////////////////////////////////////////////////////////////
	// RadioButton Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<IMenuControl *, IMenuRadioButton *>("RadioButton", "GetControl", RadioButton_GetControl);

	state->RegisterNativeFunction<IMenuCheckBox *, IMenuRadioButton *>("RadioButton", "GetCheckBox", RadioButton_GetCheckBox);

	state->RegisterNativeFunction<unsigned int, IMenuRadioButton *>("RadioButton", "GetButtonGroup", RadioButton_GetButtonGroup);

	state->RegisterNativeFunction<void, IMenuRadioButton *, BOOL>("RadioButton", "SetChecked", RadioButton_SetChecked);

	state->RegisterNativeFunction<void, IMenuRadioButton *, unsigned int>("RadioButton", "SetButtonGroup", RadioButton_SetButtonGroup);

	///////////////////////////////////////////////////////////////////////////
	// ComboBox Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<IMenuControl *, IMenuComboBox *>("ComboBox", "GetControl", ComboBox_GetControl);

	state->RegisterNativeFunction<IMenuScrollBar *, IMenuComboBox *>("ComboBox", "GetScrollBar", ComboBox_GetScrollBar);

	state->RegisterNativeFunction<IMenuElement *, IMenuComboBox *>("ComboBox", "GetMainDisplayElement", ComboBox_GetMainDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuComboBox *>("ComboBox", "GetButtonDisplayElement", ComboBox_GetButtonDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuComboBox *>("ComboBox", "GetDropDownDisplayElement", ComboBox_GetDropDownDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuComboBox *>("ComboBox", "GetSelectionDisplayElement", ComboBox_GetSelectionDisplayElement);

	state->RegisterNativeFunction<int, IMenuComboBox *>("ComboBox", "GetDropHeight", ComboBox_GetDropHeight);

	state->RegisterNativeFunction<int, IMenuComboBox *>("ComboBox", "GetScrollBarWidth", ComboBox_GetScrollBarWidth);

	state->RegisterNativeFunction<const char *, IMenuComboBox *, int>("ComboBox", "GetItemText", ComboBox_GetItemText);

	state->RegisterNativeFunction<tDATA, IMenuComboBox *, const char *>("ComboBox", "GetItemData", ComboBox_GetItemData);

	state->RegisterNativeFunction<tDATA, IMenuComboBox *, unsigned int>("ComboBox", "GetItemDataByIndex", ComboBox_GetItemDataByIndex);

	state->RegisterNativeFunction<tDATA, IMenuComboBox *>("ComboBox", "GetSelectedData", ComboBox_GetSelectedData);

	state->RegisterNativeFunction<const char *, IMenuComboBox *>("ComboBox", "GetSelectedText", ComboBox_GetSelectedText);

	state->RegisterNativeFunction<unsigned int, IMenuComboBox *>("ComboBox", "GetNumItems", ComboBox_GetNumItems);

	state->RegisterNativeFunction<void, IMenuComboBox *, COLOR>("ComboBox", "SetTextColor", ComboBox_SetTextColor);

	state->RegisterNativeFunction<void, IMenuComboBox *, RAWCOLOR>("ComboBox", "SetTextColorRaw", ComboBox_SetTextColorRaw);

	state->RegisterNativeFunction<void, IMenuComboBox *, int>("ComboBox", "SetDropHeight", ComboBox_SetDropHeight);

	state->RegisterNativeFunction<void, IMenuComboBox *, int>("ComboBox", "SetScrollBarWidth", ComboBox_SetScrollBarWidth);

	state->RegisterNativeFunction<BOOL, IMenuComboBox *, unsigned int>("ComboBox", "SetSelectedByIndex", ComboBox_SetSelectedByIndex);

	state->RegisterNativeFunction<BOOL, IMenuComboBox *, const char *>("ComboBox", "SetSelectedByText", ComboBox_SetSelectedByText);

	state->RegisterNativeFunction<BOOL, IMenuComboBox *, tDATA>("ComboBox", "SetSelectedByData", ComboBox_SetSelectedByData);

	state->RegisterNativeFunction<BOOL, IMenuComboBox *, const char *, tDATA>("ComboBox", "AddItem", ComboBox_AddItem);

	state->RegisterNativeFunction<void, IMenuComboBox *>("ComboBox", "RemoveAllItems", ComboBox_RemoveAllItems);

	state->RegisterNativeFunction<void, IMenuComboBox *, unsigned int>("ComboBox", "RemoveItem", ComboBox_RemoveItem);

	state->RegisterNativeFunction<BOOL, IMenuComboBox *, const char *>("ComboBox", "ContainsItem", ComboBox_ContainsItem);

	state->RegisterNativeFunction<int, IMenuComboBox *, const char *>("ComboBox", "FindItem", ComboBox_FindItem);

	///////////////////////////////////////////////////////////////////////////
	// Slider Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<IMenuControl *, IMenuSlider *>("Slider", "GetControl", Slider_GetControl);

	state->RegisterNativeFunction<IMenuElement *, IMenuSlider *>("Slider", "GetTrackDisplayElement", Slider_GetTrackDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuSlider *>("Slider", "GetButtonDisplayElement", Slider_GetButtonDisplayElement);

	state->RegisterNativeFunction<int, IMenuSlider *>("Slider", "GetValue", Slider_GetValue);

	state->RegisterNativeFunction<int, IMenuSlider *>("Slider", "GetRangeMin", Slider_GetRangeMin);

	state->RegisterNativeFunction<int, IMenuSlider *>("Slider", "GetRangeMax", Slider_GetRangeMax);

	state->RegisterNativeFunction<void, IMenuSlider *, int>("Slider", "SetValue", Slider_SetValue);

	state->RegisterNativeFunction<void, IMenuSlider *, int, int>("Slider", "SetRange", Slider_SetRange);

	///////////////////////////////////////////////////////////////////////////
	// EditBox Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<IMenuControl *, IMenuEditBox *>("EditBox", "GetControl", EditBox_GetControl);

	state->RegisterNativeFunction<IMenuScrollBar *, IMenuEditBox *>("EditBox", "GetScrollBar", EditBox_GetScrollBar);

	state->RegisterNativeFunction<int, IMenuEditBox *>("EditBox", "GetScrollBarWidth", EditBox_GetScrollBarWidth);

	state->RegisterNativeFunction<IMenuElement *, IMenuEditBox *>("EditBox", "GetTextAreaDisplayElement", EditBox_GetTextAreaDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuEditBox *>("EditBox", "GetTopLeftBorderDisplayElement", EditBox_GetTopLeftBorderDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuEditBox *>("EditBox", "GetTopBorderDisplayElement", EditBox_GetTopBorderDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuEditBox *>("EditBox", "GetTopRightBorderDisplayElement", EditBox_GetTopRightBorderDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuEditBox *>("EditBox", "GetLeftBorderDisplayElement", EditBox_GetLeftBorderDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuEditBox *>("EditBox", "GetRightBorderDisplayElement", EditBox_GetRightBorderDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuEditBox *>("EditBox", "GetLowerLeftBorderDisplayElement", EditBox_GetLowerLeftBorderDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuEditBox *>("EditBox", "GetLowerBorderDisplayElement", EditBox_GetLowerBorderDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuEditBox *>("EditBox", "GetLowerRightBorderDisplayElement", EditBox_GetLowerRightBorderDisplayElement);

	state->RegisterNativeFunction<const char *, IMenuEditBox *>("EditBox", "GetText", EditBox_GetText);

	state->RegisterNativeFunction<int, IMenuEditBox *>("EditBox", "GetTextLength", EditBox_GetTextLength);

	state->RegisterNativeFunction<COLOR, IMenuEditBox *>("EditBox", "GetTextColor", EditBox_GetTextColor);

	state->RegisterNativeFunction<RAWCOLOR, IMenuEditBox *>("EditBox", "GetTextColorRaw", EditBox_GetTextColorRaw);

	state->RegisterNativeFunction<COLOR, IMenuEditBox *>("EditBox", "GetSelectedTextColor", EditBox_GetSelectedTextColor);

	state->RegisterNativeFunction<RAWCOLOR, IMenuEditBox *>("EditBox", "GetSelectedTextColorRaw", EditBox_GetSelectedTextColorRaw);

	state->RegisterNativeFunction<COLOR, IMenuEditBox *>("EditBox", "GetSelectedBackColor", EditBox_GetSelectedBackColor);

	state->RegisterNativeFunction<RAWCOLOR, IMenuEditBox *>("EditBox", "GetSelectedBackColorRaw", EditBox_GetSelectedBackColorRaw);

	state->RegisterNativeFunction<COLOR, IMenuEditBox *>("EditBox", "GetCaretColor", EditBox_GetCaretColor);

	state->RegisterNativeFunction<RAWCOLOR, IMenuEditBox *>("EditBox", "GetCaretColorRaw", EditBox_GetCaretColorRaw);

	state->RegisterNativeFunction<int, IMenuEditBox *>("EditBox", "GetBorderWidth", EditBox_GetBorderWidth);

	state->RegisterNativeFunction<int, IMenuEditBox *>("EditBox", "GetSpacing", EditBox_GetSpacing);

	state->RegisterNativeFunction<BOOL, IMenuEditBox *>("EditBox", "GetEditable", EditBox_GetEditable);

	state->RegisterNativeFunction<BOOL, IMenuEditBox *>("EditBox", "GetPassword", EditBox_GetPassword);

	state->RegisterNativeFunction<int, IMenuEditBox *>("EditBox", "GetFont", EditBox_GetFont);

	// DRF - DEPRECATED
	state->RegisterNativeFunction<BOOL, IMenuEditBox *>("EditBox", "GetMultiline", EditBox_GetMultiline);

	state->RegisterNativeFunction<void, IMenuEditBox *, int>("EditBox", "SetFont", EditBox_SetFont);

	state->RegisterNativeFunction<void, IMenuEditBox *, const char *, BOOL>("EditBox", "SetText", EditBox_SetText);

	state->RegisterNativeFunction<void, IMenuEditBox *, COLOR>("EditBox", "SetTextColor", EditBox_SetTextColor);

	state->RegisterNativeFunction<void, IMenuEditBox *, RAWCOLOR>("EditBox", "SetTextColorRaw", EditBox_SetTextColorRaw);

	state->RegisterNativeFunction<bool, IMenuEditBox *, int, int>("EditBox", "SetSelected", EditBox_SetSelected);

	state->RegisterNativeFunction<bool, IMenuEditBox *>("EditBox", "SetSelectedAll", EditBox_SetSelectedAll);

	state->RegisterNativeFunction<void, IMenuEditBox *, COLOR>("EditBox", "SetSelectedTextColor", EditBox_SetSelectedTextColor);

	state->RegisterNativeFunction<void, IMenuEditBox *, RAWCOLOR>("EditBox", "SetSelectedTextColorRaw", EditBox_SetSelectedTextColorRaw);

	state->RegisterNativeFunction<void, IMenuEditBox *, COLOR>("EditBox", "SetSelectedBackColor", EditBox_SetSelectedBackColor);

	state->RegisterNativeFunction<void, IMenuEditBox *, RAWCOLOR>("EditBox", "SetSelectedBackColorRaw", EditBox_SetSelectedBackColorRaw);

	state->RegisterNativeFunction<void, IMenuEditBox *, COLOR>("EditBox", "SetCaretColor", EditBox_SetCaretColor);

	state->RegisterNativeFunction<void, IMenuEditBox *, RAWCOLOR>("EditBox", "SetCaretColorRaw", EditBox_SetCaretColorRaw);

	state->RegisterNativeFunction<void, IMenuEditBox *, int>("EditBox", "SetBorderWidth", EditBox_SetBorderWidth);

	state->RegisterNativeFunction<void, IMenuEditBox *, int>("EditBox", "SetSpacing", EditBox_SetSpacing);

	state->RegisterNativeFunction<void, IMenuEditBox *, BOOL>("EditBox", "SetEditable", EditBox_SetEditable);

	state->RegisterNativeFunction<void, IMenuEditBox *, BOOL>("EditBox", "SetPassword", EditBox_SetPassword);

	// DRF - DEPRECATED
	state->RegisterNativeFunction<void, IMenuEditBox *, BOOL>("EditBox", "SetMultiline", EditBox_SetMultiline);

	state->RegisterNativeFunction<void, IMenuEditBox *>("EditBox", "ClearText", EditBox_ClearText);

	state->RegisterNativeFunction<void, IMenuEditBox *, int>("EditBox", "SetMaxCharCount", EditBox_SetMaxCharCount);

	///////////////////////////////////////////////////////////////////////////
	// ListBox Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<IMenuControl *, IMenuListBox *>("ListBox", "GetControl", ListBox_GetControl);

	state->RegisterNativeFunction<IMenuScrollBar *, IMenuListBox *>("ListBox", "GetScrollBar", ListBox_GetScrollBar);

	state->RegisterNativeFunction<int, IMenuListBox *>("ListBox", "GetScrollBarWidth", ListBox_GetScrollBarWidth);

	state->RegisterNativeFunction<IMenuElement *, IMenuListBox *>("ListBox", "GetMainDisplayElement", ListBox_GetMainDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuListBox *>("ListBox", "GetSelectionDisplayElement", ListBox_GetSelectionDisplayElement);

	state->RegisterNativeFunction<BOOL, IMenuListBox *>("ListBox", "GetMultiSelection", ListBox_GetMultiSelection);

	state->RegisterNativeFunction<BOOL, IMenuListBox *>("ListBox", "GetDragSelection", ListBox_GetDragSelection);

	state->RegisterNativeFunction<int, IMenuListBox *>("ListBox", "GetBorder", ListBox_GetBorder);

	state->RegisterNativeFunction<int, IMenuListBox *>("ListBox", "GetLeftMargin", ListBox_GetLeftMargin);

	state->RegisterNativeFunction<int, IMenuListBox *>("ListBox", "GetRightMargin", ListBox_GetRightMargin);

	state->RegisterNativeFunction<int, IMenuListBox *>("ListBox", "GetRowHeight", ListBox_GetRowHeight);

	state->RegisterNativeFunction<const char *, IMenuListBox *, int>("ListBox", "GetItemText", ListBox_GetItemText);

	state->RegisterNativeFunction<const char *, IMenuListBox *>("ListBox", "GetSelectedItemText", ListBox_GetSelectedItemText);

	state->RegisterNativeFunction<int, IMenuListBox *>("ListBox", "GetSelectedItemIndex", ListBox_GetSelectedItemIndex);

	state->RegisterNativeFunction<int, IMenuListBox *, int>("ListBox", "GetMultiSelectedItemIndex", ListBox_GetMultiSelectedItemIndex);

	state->RegisterNativeFunction<tDATA, IMenuListBox *, int>("ListBox", "GetItemData", ListBox_GetItemData);

	state->RegisterNativeFunction<tDATA, IMenuListBox *>("ListBox", "GetSelectedItemData", ListBox_GetSelectedItemData);

	state->RegisterNativeFunction<int, IMenuListBox *>("ListBox", "GetSize", ListBox_GetSize);

	state->RegisterNativeFunction<void, IMenuListBox *, BOOL>("ListBox", "SetMultiSelection", ListBox_SetMultiSelection);

	state->RegisterNativeFunction<void, IMenuListBox *, BOOL>("ListBox", "SetHTMLEnabled", ListBox_SetHTMLEnabled);

	state->RegisterNativeFunction<void, IMenuListBox *, BOOL>("ListBox", "SetDragSelection", ListBox_SetDragSelection);

	state->RegisterNativeFunction<void, IMenuListBox *, int>("ListBox", "SetScrollBarWidth", ListBox_SetScrollBarWidth);

	state->RegisterNativeFunction<void, IMenuListBox *, int>("ListBox", "SetBorder", ListBox_SetBorder);

	state->RegisterNativeFunction<void, IMenuListBox *, int>("ListBox", "SetMargins", ListBox_SetMargins);

	state->RegisterNativeFunction<void, IMenuListBox *, int>("ListBox", "SetLeftMargin", ListBox_SetLeftMargin);

	state->RegisterNativeFunction<void, IMenuListBox *, int>("ListBox", "SetRightMargin", ListBox_SetRightMargin);

	state->RegisterNativeFunction<void, IMenuListBox *, int>("ListBox", "SetRowHeight", ListBox_SetRowHeight);

	state->RegisterNativeFunction<void, IMenuListBox *, int, const char *>("ListBox", "SetItemText", ListBox_SetItemText);

	state->RegisterNativeFunction<BOOL, IMenuListBox *, const char *, tDATA>("ListBox", "AddItem", ListBox_AddItem);

	state->RegisterNativeFunction<BOOL, IMenuListBox *, int, const char *, tDATA>("ListBox", "InsertItem", ListBox_InsertItem);

	state->RegisterNativeFunction<void, IMenuListBox *>("ListBox", "RemoveAllItems", ListBox_RemoveAllItems);

	state->RegisterNativeFunction<void, IMenuListBox *, unsigned int>("ListBox", "RemoveItem", ListBox_RemoveItem);

	state->RegisterNativeFunction<void, IMenuListBox *, int>("ListBox", "SelectItem", ListBox_SelectItem);

	state->RegisterNativeFunction<int, IMenuListBox *, tDATA>("ListBox", "FindItem", ListBox_FindItem);

	state->RegisterNativeFunction<void, IMenuListBox *>("ListBox", "ClearSelection", ListBox_ClearSelection);

	///////////////////////////////////////////////////////////////////////////
	// ScrollBar Functions
	///////////////////////////////////////////////////////////////////////////

	state->RegisterNativeFunction<IMenuControl *, IMenuScrollBar *>("ScrollBar", "GetControl", ScrollBar_GetControl);

	state->RegisterNativeFunction<IMenuControl *, IMenuScrollBar *>("ScrollBar", "GetParent", ScrollBar_GetParent);

	state->RegisterNativeFunction<int, IMenuScrollBar *>("ScrollBar", "GetTrackStart", ScrollBar_GetTrackStart);

	state->RegisterNativeFunction<int, IMenuScrollBar *>("ScrollBar", "GetTrackEnd", ScrollBar_GetTrackEnd);

	state->RegisterNativeFunction<int, IMenuScrollBar *>("ScrollBar", "GetTrackPos", ScrollBar_GetTrackPos);

	state->RegisterNativeFunction<int, IMenuScrollBar *>("ScrollBar", "GetPageSize", ScrollBar_GetPageSize);

	state->RegisterNativeFunction<BOOL, IMenuScrollBar *>("ScrollBar", "GetShowThumb", ScrollBar_GetShowThumb);

	state->RegisterNativeFunction<IMenuElement *, IMenuScrollBar *>("ScrollBar", "GetTrackDisplayElement", ScrollBar_GetTrackDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuScrollBar *>("ScrollBar", "GetUpArrowDisplayElement", ScrollBar_GetUpArrowDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuScrollBar *>("ScrollBar", "GetDownArrowDisplayElement", ScrollBar_GetDownArrowDisplayElement);

	state->RegisterNativeFunction<IMenuElement *, IMenuScrollBar *>("ScrollBar", "GetButtonDisplayElement", ScrollBar_GetButtonDisplayElement);

	state->RegisterNativeFunction<void, IMenuScrollBar *, int, int>("ScrollBar", "SetTrackRange", ScrollBar_SetTrackRange);

	state->RegisterNativeFunction<void, IMenuScrollBar *, int>("ScrollBar", "SetTrackPos", ScrollBar_SetTrackPos);

	state->RegisterNativeFunction<void, IMenuScrollBar *, int>("ScrollBar", "SetPageSize", ScrollBar_SetPageSize);
}

namespace Handlers {

void OnInit() {
	// DRF - Globals Always 0 Initialized
	//memset(g_map, 0, MAX_NODES*sizeof(MapNode));
}

bool OnDialogKeyDown(IMenuDialog *pDialog, unsigned int key, BOOL bShiftDown, BOOL bControlDown, BOOL bAltDown) {
	return OnDialogEvent(pDialog, "OnKeyDown", key, bShiftDown, bControlDown, bAltDown);
}

bool OnDialogSysKeyDown(IMenuDialog *pDialog, unsigned int key, BOOL bShiftDown, BOOL bControlDown, BOOL bAltDown) {
	return OnDialogEvent(pDialog, "OnSysKeyDown", key, bShiftDown, bControlDown, bAltDown);
}

bool OnDialogKeyUp(IMenuDialog *pDialog, unsigned int key, BOOL bShiftDown, BOOL bControlDown, BOOL bAltDown) {
	return OnDialogEvent(pDialog, "OnKeyUp", key, bShiftDown, bControlDown, bAltDown);
}

bool OnDialogSysKeyUp(IMenuDialog *pDialog, unsigned int key, BOOL bShiftDown, BOOL bControlDown, BOOL bAltDown) {
	return OnDialogEvent(pDialog, "OnSysKeyUp", key, bShiftDown, bControlDown, bAltDown);
}

bool OnDialogMouseMove(IMenuDialog *pDialog, POINT pt) {
	return OnDialogEvent(pDialog, "OnMouseMove", pt);
}

bool OnDialogLButtonDown(IMenuDialog *pDialog, POINT pt) {
	return OnDialogEvent(pDialog, "OnLButtonDown", pt);
}

bool OnDialogRButtonDown(IMenuDialog *pDialog, POINT pt) {
	return OnDialogEvent(pDialog, "OnRButtonDown", pt);
}

bool OnDialogLButtonDblClk(IMenuDialog *pDialog, POINT pt) {
	return OnDialogEvent(pDialog, "OnLButtonDblClk", pt);
}

bool OnDialogLButtonUp(IMenuDialog *pDialog, POINT pt) {
	return OnDialogEvent(pDialog, "OnLButtonUp", pt);
}

bool OnDialogRButtonUp(IMenuDialog *pDialog, POINT pt) {
	return OnDialogEvent(pDialog, "OnRButtonUp", pt);
}

bool OnDialogLButtonDownInside(IMenuDialog *pDialog, POINT pt) {
	return OnDialogEvent(pDialog, "OnLButtonDownInside", pt);
}

bool OnControlMouseEnter(IMenuControl *pControl) {
	return OnControlEvent(pControl, "OnMouseEnter");
}

bool OnControlMouseLeave(IMenuControl *pControl) {
	return OnControlEvent(pControl, "OnMouseLeave");
}

bool OnControlFocusIn(IMenuControl *pControl) {
	return OnControlEvent(pControl, "OnFocusIn");
}

bool OnControlFocusOut(IMenuControl *pControl) {
	return OnControlEvent(pControl, "OnFocusOut");
}

bool OnControlLButtonDown(IMenuControl *pControl, POINT pt) {
	return OnControlEvent(pControl, "OnLButtonDown", pt);
}

bool OnControlRButtonDown(IMenuControl *pControl, POINT pt) {
	return OnControlEvent(pControl, "OnRButtonDown", pt);
}

bool OnControlLButtonDblClk(IMenuControl *pControl, POINT pt) {
	return OnControlEvent(pControl, "OnLButtonDblClk", pt);
}

bool OnControlLButtonUp(IMenuControl *pControl, POINT pt) {
	return OnControlEvent(pControl, "OnLButtonUp", pt);
}

bool OnControlRButtonUp(IMenuControl *pControl, POINT pt) {
	return OnControlEvent(pControl, "OnRButtonUp", pt);
}

} // namespace Handlers

} // namespace KEP