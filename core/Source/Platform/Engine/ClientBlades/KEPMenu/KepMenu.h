///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../IClientBlade.h"
#include "../IClientRenderBlade.h"
#include "../BladeExports.h"
#include "../IInputSubscriber.h"
#include "../IMenuBlade.h"
#include "../IMenuGui.h"
#include "Handler.h"
#include "DXUTGui.h"
#include "IClientEngine.h"
#include "js.h"
#include "Core/Util/fast_mutex.h"
#include "log4cplus/Logger.h"
#include <string>
#include <set>
#include <list>

class TiXmlNode;

namespace KEP {

class KEPMenu : public IMenuBlade, public IInputSubscriber {
public:
	typedef std::list<IMenuDialog*> dialogs_t;
	typedef std::list<IMenuDialog*>::iterator dialog_iter_t;
	typedef std::list<IMenuDialog*>::const_iterator dialog_citer_t;
	typedef std::list<IMenuDialog*>::reverse_iterator dialog_riter_t;

	static CDXUTDialog* GetDialogInFocus() {
		return CDXUTDialog::GetDialogInFocus();
	}

	static bool IsDialogInFocusCapturingKeys() {
		return CDXUTDialog::IsDialogInFocusCapturingKeys();
	}

	static bool IsDialogInFocusMouseEnabled() {
		return CDXUTDialog::IsDialogInFocusMouseEnabled();
	}

	static bool IsDialogInFocusKeyboardEnabled() {
		return CDXUTDialog::IsDialogInFocusKeyboardEnabled();
	}

public:
	KEPMenu();

	virtual ~KEPMenu();

	static KEPMenu* Instance() {
		return m_pThis;
	}

	void SetTestGroup(int testGroup);

	// from IClientBlade
	virtual bool Initialize();
	virtual bool Destroy();
	virtual ULONG Version() const {
		return 0x100;
	}
	virtual const std::string& GetName() const {
		return m_sName;
	}

	virtual void OnUpdate() override;

	// from IInputSubscriber
	virtual void OnLButtonDown(int x, int y);
	virtual void OnLButtonUp(int x, int y);

	virtual void OnRButtonDown(int x, int y);
	virtual void OnRButtonUp(int x, int y);

	virtual void OnMButtonDown(int x, int y);
	virtual void OnMButtonUp(int x, int y);

	virtual void OnMouseMove(int x, int y);

	virtual bool HitTest(int x, int y) const;

	virtual void Render();

	virtual void OnLostDevice();
	virtual void OnResetDevice();

	// DRF - Returns if the dialog currently frontmost is mouse enabled.
	bool IsDialogFrontmostMouseEnabled();

	// DRF - Returns if the dialog currently frontmost is keyboard enabled.
	bool IsDialogFrontmostKeyboardEnabled();

	// DRF - Return dialog as a string for consistent logging.
	std::string DialogToStr(IMenuDialog* pDialog) const;

	// DRF - Returns number of open dialogs.
	size_t DialogsOpen() const;

	// DRF - Logs all dialogs for debugging the menu system.
	bool LogAllDialogs(bool verbose = true);

	// DRF - Returns the dialog menu identifier.
	const char* DialogMenuId(IMenuDialog* pDialog);

	// DRF - Returns the normalized dialog menu name (without '.xml').
	const char* DialogMenuName(IMenuDialog* pDialog);

	// DRF - Returns the normalized dialog script name (without '.lua').
	const char* DialogScriptName(IMenuDialog* pDialog);

	// DRF - Returns the dialog identifier (specified by <DialogID> in the xml).
	int GetDialogID(const std::string& menuName);

	// DRF - Returns a pointer to the named menu dialog (or variant) if open or a null if not.
	IMenuDialog* GetDialog(const std::string& menuName);

	// DRF - Creates new empty dialog.
	IMenuDialog* DialogNew();

	// DRF - Loads given menu from search path as a new dialog with assigned menu identifier.
	IMenuDialog* DialogCreateAs(const std::string& menuName, const std::string& menuId, bool editNow = false, bool trustedOnly = false);

	// DRF - Loads given menu from search path as a new dialog using menu name as menu identifier.
	IMenuDialog* DialogCreate(const std::string& menuName, bool editNow = false, bool trustedOnly = false);

	// DRF - Calls all dialogs OnExit() function.
	bool DialogExitAll();

	// DRF - Destroys the given dialog by removing it from m_dialogs (invalidating the list iterator)
	// and calling dialog OnDestroy. Dialog is flagged for deletion and moved to m_dialogsDeleted
	// which is flushed in ClearDeletedDialogs() between script event handler calls.
	bool DialogDestroy(IMenuDialog* pDialog);

	// DRF - Destroys all dialogs except the given dialog, NULL for destroy all.
	bool DialogDestroyAllExcept(IMenuDialog* pDialog = NULL);

	// DRF - Destroys all non-essential dialogs except the given dialog, NULL for destroy all non-essential.
	bool DialogDestroyAllNonEssentialExcept(IMenuDialog* pDialog = NULL, BOOL includeFramework = false);

	// DRF - Saves the given dialog to the given fileName.
	bool DialogSave(IMenuDialog* pDialog, const std::string& fileName);

	bool DialogCopyTemplate(IMenuDialog* pDialog);

	bool DialogRegisterGenericEvents(IMenuDialog* pDialog);

	bool DialogUnRegisterGenericEvents(IMenuDialog* pDialog);

	bool ApplyInputSettings();

	void DialogsSetEdit(bool bEdit);

	/** DRF
	* Finds the given dialog in the m_dialogs list, returning true on success
	* or false on failure. This only succeeds on valid dialogs not flagged for deletion.
	*/
	bool DialogFind(IMenuDialog* pDialog);

	/** DRF
	* Removes dialog from m_dialogs list and marks the list as dirty. Do not call this to close a dialog,
	* instead call DialogDestroy() which will call this after calling the dialog OnDestroy() to remove it
	* from the list. Optionally add it to the m_deletedDialogs list and flag it as deleted.
	* Returns true on success or false on failure.
	*/
	bool DialogRemove(IMenuDialog* pDialog, bool addToDeleted);

	/** DRF
	* Returns the frontmost dialog in z-order. (actually at back of list)
	*/
	IMenuDialog* GetDialogFrontmost() {
		return (m_dialogs.empty() ? NULL : m_dialogs.back());
	}

	/** DRF
	* Enable/Disable specified dialog to always be brought to front of all others.  This feature may only
	* apply to one dialog at a time and will remain in effect until disabled.
	*/
	bool DialogAlwaysBringToFront(IMenuDialog* pDialog, bool enable);

	/** DRF
	* Moves dialog to front of all other dialogs, marks the list as dirty, and updates the front dialog input settings.
	*/
	bool DialogBringToFrontEnable(IMenuDialog* pDialog, bool enable);
	bool DialogBringToFront(IMenuDialog* pDialog);

	/** DRF
	* Returns the backmost dialog in z-order. (actually at front of list)
	*/
	IMenuDialog* GetDialogBackmost() {
		return (m_dialogs.empty() ? NULL : m_dialogs.front());
	}

	/** DRF
	* Moves dialog to back of all other dialogs, marks the list as dirty, and updates the front dialog input settings.
	*/
	bool DialogSendToBack(IMenuDialog* pDialog);

	IDirect3DTexture9* GetD3DTextureIfLoadedOrQueueToLoad(const std::string& filename);
	IDirect3DTexture9* GetDynamicTexture(unsigned dynTextureId);

	/** DRF
	* Sets the cursor mode types.
	*/
	CURSOR_TYPE m_cursorModeType[CURSOR_MODES]; // current cursor modes types
	bool ResetCursorModesTypes();
	bool SetCursorModeType(const CURSOR_MODE& cursorMode, const CURSOR_TYPE& cursorType);

	/** DRF
	* Sets the current cursor mode.
	*/
	CURSOR_MODE m_cursorMode; // current cursor mode
	bool SetCursorMode(const CURSOR_MODE& cursorMode);

	/** DRF
	* Sets the current cursor type.
	*/
	CURSOR_TYPE m_cursorType; // current cursor type
	bool SetCursorType(const CURSOR_TYPE& cursorType);

	/** DRF
	* The current menu that the cursor is inside.
	*/
	CDXUTDialog* m_cursorMenu; // current cursor menu
	CDXUTDialog* m_cursorMenuTemp; // current cursor menu
	void SetCursorMenuBegin();
	void SetCursorMenu(CDXUTDialog* cursorMenu);
	void SetCursorMenuEnd();

	/** DRF
	* SetMenuCursorType() only suceeds once between SetMenuCursorTypeBegin() and SetMenuCursorTypeEnd().
	* If the menu cursor type has not been set by SetMenuCursorTypeEnd() it is set to CURSOR_TYPE_DEFAULT.
	*/
	bool m_cursorTypeSet;
	void SetMenuCursorTypeBegin();
	void SetMenuCursorType(const CURSOR_TYPE& cursorType);
	void SetMenuCursorTypeEnd(UINT uMsg);

	bool CopyControlToClipboard(IMenuDialog* pDialog, const std::string& ctlName);
	bool PasteControlFromClipboard(IMenuDialog* pDialog, bool bRelocate, int newX, int newY);
	bool IsSomethingInClipboard() const {
		return m_clipboardItem.controlType != (DXUT_CONTROL_TYPE)-1;
	}

	bool DialogHandleSelected(IMenuDialog* pDialog, POINT pt);
	bool DialogHandleMoved(IMenuDialog* pDialog, POINT pt);
	bool DialogHandleResized(IMenuDialog* pDialog, POINT pt);
	bool ControlHandleSelected(int nControlID, IMenuControl* pControl, POINT pt);
	bool ControlHandleMoved(IMenuControl* pControl, POINT pt);
	bool ControlHandleResized(IMenuControl* pControl, POINT pt);
	bool ControlHandleDragStart(int nControlID, IMenuControl* pControl, POINT pt);
	bool ControlHandleDragEnd(int nControlID, IMenuControl* pControl, POINT pt);
	bool DialogUndo(IMenuDialog* pDialog);

	bool DialogControlMoveForward(IMenuDialog* pDialog, const std::string& ctlName);
	bool DialogControlMoveBackward(IMenuDialog* pDialog, const std::string& ctlName);

	// DEPRECATED - Use MakeWebCall() Instead
	bool GetBrowserPage(IMenuDialog* pDialog, const char* url, int filter, int startIndex, int maxItems);
	bool MakeWebCall(const char* url, int filter, int startIndex, int maxItems);

	/** DRF
	* Clears the script search m_paths list.
	*/
	void ClearMenuPaths();

	/** DRF
	* Adds a pair of menu/script paths to the start of the script search m_paths list.
	* @@param pathMenu		Path to Menus folder
	* @@param pathScript	Path to matching MenuScripts folder
	* @@param trustedPath	If true, the paths provided are considered as trusted source for menus and scripts (WOK for now, probably also templates in the future)
	* @@param addTextures	If true, search all textures in menuPath and adds them to m_textures.
	*/
	void AddMenuPaths(const std::string& pathMenu, const std::string& pathScript, bool trustedPaths, bool addTextures);

	// DRF - ED-7035 - Investigate texture memory impact
	virtual void ReleaseTextureNode(IDirect3DTexture9* pTex) override;

	/** DRF
	* Clears the m_textures list.
	*/
	void ClearTextures();

	/** DRF
	* Adds named texture to the m_textures list.
	*/
	void AddTexture(const std::string& fileName);

	/** DRF
	* Locates all textures in the menu_dir and adds them to the m_textures list.
	*/
	void AddTextures(const std::string& menu_dir);

	/** DRF
	* Returns the number of textures in the m_textures list.
	*/
	int GetNumTextures();

	/** DRF
	* Returns the indexed texture in the m_textures list.
	*/
	const char* GetTextureByIndex(int index);

	void ReInit(bool isChild, bool clearMenuPaths, bool clearTextures);

	std::string PathBase() const {
		return m_pathBase;
	}

	bool IsModalDialogOpen();

	bool SearchFile(const std::string& relPath, bool isScript, bool trustedOnly, std::string& fullPath, bool* pTrusted = NULL);

	bool SearchFile(const std::string& relPath, bool isScript, bool trustedOnly, std::string& directory, std::string& fileName, bool* pTrusted = NULL);

protected:
	/** DRF
	* Load the given Menu.xml into a new Dialog and return the dialog.
	*/
	IMenuDialog* LoadMenuXml(const std::string& xmlFilePath, bool editable, bool trustedMenu);

private:
	/** DRF
	* Inserts dialog to back of all other dialogs and marks the list as dirty.
	* The backmost dialog on the screen is actually the dialog at m_dialogs.begin().
	*/
	void DialogInsertToBack(IMenuDialog* pDialog);

	/** DRF
	* Inserts dialog to front of all other dialogs except the 'AlwaysFront' dialog and marks the list as dirty.
	* The frontmost dialog on the screen is actually the dialog at m_dialogs.back().
	*/
	void DialogInsertToFront(IMenuDialog* pDialog);

	/** DRF
	* Returns true if the given message is WM_CLOSE.
	*/
	static bool MsgProcIsClose();

	/** DRF
	* Returns true if the given message is a valid MsgProc() message we process.
	*/
	static bool MsgProcValidMsg();

	static LRESULT CALLBACK MsgProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, bool* pbNoFurtherProcessing);
	static void CALLBACK KeyboardProc(UINT nChar, bool bKeyDown, bool bAltDown);
	static bool CALLBACK OnDialogGUIEvent(GUI_EVENT nEvent, IMenuDialog* pDialog, POINT pt);
	static bool CALLBACK OnControlGUIEvent(GUI_EVENT nEvent, int nControlID, IMenuControl* pControl, POINT pt);

private:
	static const std::string m_sName;

	struct path_pair_t {
		std::string menuPath;
		std::string scriptPath;
		bool trusted;

		path_pair_t(std::string menuDir, std::string scriptDir, bool t) :
				menuPath(menuDir), scriptPath(scriptDir), trusted(t) {}
	};

	// m_paths List Iterators
	typedef std::vector<path_pair_t> paths_t;
	typedef std::vector<path_pair_t>::iterator paths_iter_t;
	typedef std::vector<path_pair_t>::const_iterator paths_citer_t;
	typedef std::vector<path_pair_t>::reverse_iterator paths_riter_t;

private:
	paths_t m_paths; // DRF - Script dofile() Search Paths (multi-threaded access during rezone)
public:
	fast_recursive_mutex m_pathsMutex; // DRF - Added - Rezone Crash Fix

private:
	// m_textures List Iterators
	typedef std::set<std::string> textures_t;
	typedef std::set<std::string>::iterator textures_iter_t;

	// Based on currently implementation, if user is in WOK, m_textures holds a list of names for textures found under
	// GameFiles\Menus folder. Otherwise, it's a list of textures found under the Menus folders for a 3dapp, including
	// both Template menus and custom app menu. It does not contain any textures from LocalDev. MenuEditor/TexturePicker
	// uses m_textures to display a list of available menu textures. TexturePicker will append localDev texture by calling
	// ClientEngine_Count3DAppUnpublishedAssets and ClientEngine_Get3DAppUnpublishedAsset.
private:
	textures_t m_textures; // DRF - Texture List (multi-threaded access during rezone)
public:
	fast_recursive_mutex m_texturesMutex; // DRF - TODO - Does This Require Mutex Same As m_paths?

private:
	IMenuDialog* m_alwaysBringDialogToFront;

	// Dialogs are managed in Z Order Using The Following 3 Lists:
	// - m_dialogs
	//      Unstable list of all opened dialogs during the frame. Iterators to this list will
	//      be invalidated so use m_dialogsStable if the dialogs are able to change during a
	//      loop via script event handlers as this list is added to and removed from as dialogs are
	//      deleted and created during script event handling.
	// - dialogsStable
	//      A stable copy of m_dialogs at the beginning of this frame. We promise not to add
	//      or remove anything from this list during a frame, so your iterators will never be
	//      invalidated. The only place that will manipulate m_dialogsStable is at the beginning
	//      of OnUpdate().
	// - dialogsDeleted
	//      Dialogs are moved from m_dialogs to here during deletion and periodically removed
	//      from memory using the ClearDeletedDialogs() 'garbage collection' type process.
	dialogs_t m_dialogs;
	dialogs_t m_dialogsStable;
	dialogs_t m_dialogsDeleted;

	bool m_dialogsDirty; // true whenever m_dialogs list has changed during a frame

	/** DRF
	* Deletes all dialogs in deleted list and frees all menu Xmls, Lua VMs, & resources.
	* WARNING - Do not call this through glue from inside a Lua script if the script belongs
	* to a dialog in this list or you will lose your VM state and crash! This is called
	* periodically during the render loop OnUpdate() and so dialogs will get cleared in
	* the next render loop outside of the context of a Lua script.
	*/
	void DialogsClearDeleted();

	Timer m_timerOnRender;
	TimeSec m_timeOnRenderSec;

	Timer m_timerOnRenderScript;

	static KEPMenu* m_pThis;

	//Added by YC for limited Clipboard support
	struct ClipboardItem {
		UINT controlType;
		TiXmlDocument* controlXml;

		ClipboardItem();
		~ClipboardItem();
	};

	ClipboardItem m_clipboardItem;

	//Added by YC for limited UNDO support
	enum {
		UNDO_NONE,
		UNDO_PLACEMENT
	};

	struct UndoInfo {
		int op, dlgID;
		std::string ctlName;
		int ctlX, ctlY, ctlW, ctlH;

		UndoInfo() {
			op = UNDO_NONE;
			dlgID = -1;
		}
	};

	UndoInfo m_undoInfo, m_undoCandidate;

	std::string m_selectedDlgId; // drf - selected menu identifier
	std::string m_selectedCtlName;
	std::string m_pathBase;
};

KEPMenu* GetKepMenu();

} // namespace KEP