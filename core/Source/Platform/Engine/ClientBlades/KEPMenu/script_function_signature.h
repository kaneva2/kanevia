/******************************************************************************
 script_function_signature.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

template <
	typename out_val_type
#if (NUM_PARAMS > 0)
	,
	typename in_val_type1
#endif
#if (NUM_PARAMS > 1)
	,
	typename in_val_type2
#endif
#if (NUM_PARAMS > 2)
	,
	typename in_val_type3
#endif
#if (NUM_PARAMS > 3)
	,
	typename in_val_type4
#endif
#if (NUM_PARAMS > 4)
	,
	typename in_val_type5
#endif
#if (NUM_PARAMS > 5)
	,
	typename in_val_type6
#endif
#if (NUM_PARAMS > 6)
	,
	typename in_val_type7
#endif
#if (NUM_PARAMS > 7)
	,
	typename in_val_type8
#endif
#if (NUM_PARAMS > 8)
	,
	typename in_val_type9
#endif
#if (NUM_PARAMS > 9)
	,
	typename in_val_type10
#endif
#if (NUM_PARAMS > 10)
	,
	typename in_val_type11
#endif
#if (NUM_PARAMS > 11)
	,
	typename in_val_type12
#endif
#if (NUM_PARAMS > 12)
	,
	typename in_val_type13
#endif
#if (NUM_PARAMS > 13)
	,
	typename in_val_type14
#endif
	>
class function_signature<
	typename out_val_type
#if (NUM_PARAMS > 0)
	,
	typename in_val_type1
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 1)
	,
	typename in_val_type2
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 2)
	,
	typename in_val_type3
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 3)
	,
	typename in_val_type4
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 4)
	,
	typename in_val_type5
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 5)
	,
	typename in_val_type6
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 6)
	,
	typename in_val_type7
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 7)
	,
	typename in_val_type8
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 8)
	,
	typename in_val_type9
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 9)
	,
	typename in_val_type10
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 10)
	,
	typename in_val_type11
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 11)
	,
	typename in_val_type12
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 12)
	,
	typename in_val_type13
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 13)
	,
	typename in_val_type14
#else
	,
	null_parameter
#endif
	,
	null_parameter> {
public:
	typedef out_val_type (*FunctionType)(
#if (NUM_PARAMS > 0)
		in_val_type1
#endif
#if (NUM_PARAMS > 1)
		,
		in_val_type2
#endif
#if (NUM_PARAMS > 2)
		,
		in_val_type3
#endif
#if (NUM_PARAMS > 3)
		,
		in_val_type4
#endif
#if (NUM_PARAMS > 4)
		,
		in_val_type5
#endif
#if (NUM_PARAMS > 5)
		,
		in_val_type6
#endif
#if (NUM_PARAMS > 6)
		,
		in_val_type7
#endif
#if (NUM_PARAMS > 7)
		,
		in_val_type8
#endif
#if (NUM_PARAMS > 8)
		,
		in_val_type9
#endif
#if (NUM_PARAMS > 9)
		,
		in_val_type10
#endif
#if (NUM_PARAMS > 10)
		,
		in_val_type11
#endif
#if (NUM_PARAMS > 11)
		,
		in_val_type12
#endif
#if (NUM_PARAMS > 12)
		,
		in_val_type13
#endif
#if (NUM_PARAMS > 13)
		,
		in_val_type14
#endif
	);

	static out_val_type make_call(
#if (NUM_PARAMS > 0)
		in_val_type1
#else
		null_parameter
#endif
			iv1,
#if (NUM_PARAMS > 1)
		in_val_type2
#else
		null_parameter
#endif
			iv2,
#if (NUM_PARAMS > 2)
		in_val_type3
#else
		null_parameter
#endif
			iv3,
#if (NUM_PARAMS > 3)
		in_val_type4
#else
		null_parameter
#endif
			iv4,
#if (NUM_PARAMS > 4)
		in_val_type5
#else
		null_parameter
#endif
			iv5,
#if (NUM_PARAMS > 5)
		in_val_type6
#else
		null_parameter
#endif
			iv6,
#if (NUM_PARAMS > 6)
		in_val_type7
#else
		null_parameter
#endif
			iv7,
#if (NUM_PARAMS > 7)
		in_val_type8
#else
		null_parameter
#endif
			iv8,
#if (NUM_PARAMS > 8)
		in_val_type9
#else
		null_parameter
#endif
			iv9,
#if (NUM_PARAMS > 9)
		in_val_type10
#else
		null_parameter
#endif
			iv10,
#if (NUM_PARAMS > 10)
		in_val_type11
#else
		null_parameter
#endif
			iv11,
#if (NUM_PARAMS > 11)
		in_val_type12
#else
		null_parameter
#endif
			iv12,
#if (NUM_PARAMS > 12)
		in_val_type13
#else
		null_parameter
#endif
			iv13,
#if (NUM_PARAMS > 13)
		in_val_type14
#else
		null_parameter
#endif
			iv14,
		null_parameter iv15,
		FunctionType f) {
		return f(
#if (NUM_PARAMS > 0)
			iv1
#endif
#if (NUM_PARAMS > 1)
			,
			iv2
#endif
#if (NUM_PARAMS > 2)
			,
			iv3
#endif
#if (NUM_PARAMS > 3)
			,
			iv4
#endif
#if (NUM_PARAMS > 4)
			,
			iv5
#endif
#if (NUM_PARAMS > 5)
			,
			iv6
#endif
#if (NUM_PARAMS > 6)
			,
			iv7
#endif
#if (NUM_PARAMS > 7)
			,
			iv8
#endif
#if (NUM_PARAMS > 8)
			,
			iv9
#endif
#if (NUM_PARAMS > 9)
			,
			iv10
#endif
#if (NUM_PARAMS > 10)
			,
			iv11
#endif
#if (NUM_PARAMS > 11)
			,
			iv12
#endif
#if (NUM_PARAMS > 12)
			,
			iv13
#endif
#if (NUM_PARAMS > 13)
			,
			iv14
#endif
		);
	}
};
