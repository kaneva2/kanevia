///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <d3d9.h>
#include <mutex>
#include <set>

namespace KEP {

class D3D9TextureWrapper;

class D3D9DeviceWrapper : public IDirect3DDevice9 {
public:
	D3D9DeviceWrapper(IDirect3DDevice9* pd3dDevice) :
			m_pd3dDevice(pd3dDevice) {}

	// Transparent pass-through
	virtual STDMETHODIMP QueryInterface(THIS_ REFIID riid, void** ppvObj) override { return m_pd3dDevice->QueryInterface(riid, ppvObj); }
	virtual STDMETHODIMP_(ULONG) AddRef(THIS) override { return m_pd3dDevice->AddRef(); }
	virtual STDMETHODIMP TestCooperativeLevel(THIS) override { return m_pd3dDevice->TestCooperativeLevel(); }
	virtual STDMETHODIMP_(UINT) GetAvailableTextureMem(THIS) override { return m_pd3dDevice->GetAvailableTextureMem(); }
	virtual STDMETHODIMP EvictManagedResources(THIS) override { return m_pd3dDevice->EvictManagedResources(); }
	virtual STDMETHODIMP GetDirect3D(THIS_ IDirect3D9** ppD3D9) override { return m_pd3dDevice->GetDirect3D(ppD3D9); }
	virtual STDMETHODIMP GetDeviceCaps(THIS_ D3DCAPS9* pCaps) override { return m_pd3dDevice->GetDeviceCaps(pCaps); }
	virtual STDMETHODIMP GetDisplayMode(THIS_ UINT iSwapChain, D3DDISPLAYMODE* pMode) override { return m_pd3dDevice->GetDisplayMode(iSwapChain, pMode); }
	virtual STDMETHODIMP GetCreationParameters(THIS_ D3DDEVICE_CREATION_PARAMETERS* pParameters) override { return m_pd3dDevice->GetCreationParameters(pParameters); }
	virtual STDMETHODIMP SetCursorProperties(THIS_ UINT XHotSpot, UINT YHotSpot, IDirect3DSurface9* pCursorBitmap) override { return m_pd3dDevice->SetCursorProperties(XHotSpot, YHotSpot, pCursorBitmap); }
	virtual STDMETHODIMP_(void) SetCursorPosition(THIS_ int X, int Y, DWORD Flags) override { m_pd3dDevice->SetCursorPosition(X, Y, Flags); }
	virtual STDMETHODIMP_(BOOL) ShowCursor(THIS_ BOOL bShow) override { return m_pd3dDevice->ShowCursor(bShow); }
	virtual STDMETHODIMP CreateAdditionalSwapChain(THIS_ D3DPRESENT_PARAMETERS* pPresentationParameters, IDirect3DSwapChain9** pSwapChain) override { return m_pd3dDevice->CreateAdditionalSwapChain(pPresentationParameters, pSwapChain); }
	virtual STDMETHODIMP GetSwapChain(THIS_ UINT iSwapChain, IDirect3DSwapChain9** pSwapChain) override { return m_pd3dDevice->GetSwapChain(iSwapChain, pSwapChain); }
	virtual STDMETHODIMP_(UINT) GetNumberOfSwapChains(THIS) override { return m_pd3dDevice->GetNumberOfSwapChains(); }
	virtual STDMETHODIMP Reset(THIS_ D3DPRESENT_PARAMETERS* pPresentationParameters) override { return m_pd3dDevice->Reset(pPresentationParameters); }
	virtual STDMETHODIMP Present(THIS_ CONST RECT* pSourceRect, CONST RECT* pDestRect, HWND hDestWindowOverride, CONST RGNDATA* pDirtyRegion) override { return m_pd3dDevice->Present(pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion); }
	virtual STDMETHODIMP GetBackBuffer(THIS_ UINT iSwapChain, UINT iBackBuffer, D3DBACKBUFFER_TYPE Type, IDirect3DSurface9** ppBackBuffer) override { return m_pd3dDevice->GetBackBuffer(iSwapChain, iBackBuffer, Type, ppBackBuffer); }
	virtual STDMETHODIMP GetRasterStatus(THIS_ UINT iSwapChain, D3DRASTER_STATUS* pRasterStatus) override { return m_pd3dDevice->GetRasterStatus(iSwapChain, pRasterStatus); }
	virtual STDMETHODIMP SetDialogBoxMode(THIS_ BOOL bEnableDialogs) override { return m_pd3dDevice->SetDialogBoxMode(bEnableDialogs); }
	virtual STDMETHODIMP_(void) SetGammaRamp(THIS_ UINT iSwapChain, DWORD Flags, CONST D3DGAMMARAMP* pRamp) override { m_pd3dDevice->SetGammaRamp(iSwapChain, Flags, pRamp); }
	virtual STDMETHODIMP_(void) GetGammaRamp(THIS_ UINT iSwapChain, D3DGAMMARAMP* pRamp) override { m_pd3dDevice->GetGammaRamp(iSwapChain, pRamp); }
	virtual STDMETHODIMP BeginScene(THIS) override { return m_pd3dDevice->BeginScene(); }
	virtual STDMETHODIMP EndScene(THIS) override { return m_pd3dDevice->EndScene(); }
	virtual STDMETHODIMP Clear(THIS_ DWORD Count, CONST D3DRECT* pRects, DWORD Flags, D3DCOLOR Color, float Z, DWORD Stencil) override { return m_pd3dDevice->Clear(Count, pRects, Flags, Color, Z, Stencil); }
	virtual STDMETHODIMP SetTransform(THIS_ D3DTRANSFORMSTATETYPE State, CONST D3DMATRIX* pMatrix) override { return m_pd3dDevice->SetTransform(State, pMatrix); }
	virtual STDMETHODIMP GetTransform(THIS_ D3DTRANSFORMSTATETYPE State, D3DMATRIX* pMatrix) override { return m_pd3dDevice->GetTransform(State, pMatrix); }
	virtual STDMETHODIMP MultiplyTransform(THIS_ D3DTRANSFORMSTATETYPE State, CONST D3DMATRIX* pMatrix) override { return m_pd3dDevice->MultiplyTransform(State, pMatrix); }
	virtual STDMETHODIMP SetRenderState(THIS_ D3DRENDERSTATETYPE State, DWORD Value) override { return m_pd3dDevice->SetRenderState(State, Value); }
	virtual STDMETHODIMP GetRenderState(THIS_ D3DRENDERSTATETYPE State, DWORD* pValue) override { return m_pd3dDevice->GetRenderState(State, pValue); }
	virtual STDMETHODIMP CreateStateBlock(THIS_ D3DSTATEBLOCKTYPE Type, IDirect3DStateBlock9** ppSB) override { return m_pd3dDevice->CreateStateBlock(Type, ppSB); }
	virtual STDMETHODIMP BeginStateBlock(THIS) override { return m_pd3dDevice->BeginStateBlock(); }
	virtual STDMETHODIMP EndStateBlock(THIS_ IDirect3DStateBlock9** ppSB) override { return m_pd3dDevice->EndStateBlock(ppSB); }
	virtual STDMETHODIMP SetScissorRect(THIS_ CONST RECT* pRect) override { return m_pd3dDevice->SetScissorRect(pRect); }
	virtual STDMETHODIMP GetScissorRect(THIS_ RECT* pRect) override { return m_pd3dDevice->GetScissorRect(pRect); }
	virtual STDMETHODIMP GetTextureStageState(THIS_ DWORD Stage, D3DTEXTURESTAGESTATETYPE Type, DWORD* pValue) override { return m_pd3dDevice->GetTextureStageState(Stage, Type, pValue); }
	virtual STDMETHODIMP SetTextureStageState(THIS_ DWORD Stage, D3DTEXTURESTAGESTATETYPE Type, DWORD Value) override { return m_pd3dDevice->SetTextureStageState(Stage, Type, Value); }
	virtual STDMETHODIMP DrawPrimitive(THIS_ D3DPRIMITIVETYPE PrimitiveType, UINT StartVertex, UINT PrimitiveCount) override { return m_pd3dDevice->DrawPrimitive(PrimitiveType, StartVertex, PrimitiveCount); }
	virtual STDMETHODIMP DrawIndexedPrimitive(THIS_ D3DPRIMITIVETYPE PrimitiveType, INT BaseVertexIndex, UINT MinVertexIndex, UINT NumVertices, UINT startIndex, UINT primCount) override { return m_pd3dDevice->DrawIndexedPrimitive(PrimitiveType, BaseVertexIndex, MinVertexIndex, NumVertices, startIndex, primCount); }
	virtual STDMETHODIMP DrawPrimitiveUP(THIS_ D3DPRIMITIVETYPE PrimitiveType, UINT PrimitiveCount, CONST void* pVertexStreamZeroData, UINT VertexStreamZeroStride) override { return m_pd3dDevice->DrawPrimitiveUP(PrimitiveType, PrimitiveCount, pVertexStreamZeroData, VertexStreamZeroStride); }
	virtual STDMETHODIMP DrawIndexedPrimitiveUP(THIS_ D3DPRIMITIVETYPE PrimitiveType, UINT MinVertexIndex, UINT NumVertices, UINT PrimitiveCount, CONST void* pIndexData, D3DFORMAT IndexDataFormat, CONST void* pVertexStreamZeroData, UINT VertexStreamZeroStride) override { return m_pd3dDevice->DrawIndexedPrimitiveUP(PrimitiveType, MinVertexIndex, NumVertices, PrimitiveCount, pIndexData, IndexDataFormat, pVertexStreamZeroData, VertexStreamZeroStride); }
	virtual STDMETHODIMP SetViewport(THIS_ CONST D3DVIEWPORT9* pViewport) override { return m_pd3dDevice->SetViewport(pViewport); }
	virtual STDMETHODIMP GetViewport(THIS_ D3DVIEWPORT9* pViewport) override { return m_pd3dDevice->GetViewport(pViewport); }
	virtual STDMETHODIMP SetMaterial(THIS_ CONST D3DMATERIAL9* pMaterial) override { return m_pd3dDevice->SetMaterial(pMaterial); }
	virtual STDMETHODIMP GetMaterial(THIS_ D3DMATERIAL9* pMaterial) override { return m_pd3dDevice->GetMaterial(pMaterial); }
	virtual STDMETHODIMP SetLight(THIS_ DWORD Index, CONST D3DLIGHT9* pLight) override { return m_pd3dDevice->SetLight(Index, pLight); }
	virtual STDMETHODIMP GetLight(THIS_ DWORD Index, D3DLIGHT9* pLight) override { return m_pd3dDevice->GetLight(Index, pLight); }
	virtual STDMETHODIMP LightEnable(THIS_ DWORD Index, BOOL Enable) override { return m_pd3dDevice->LightEnable(Index, Enable); }
	virtual STDMETHODIMP GetLightEnable(THIS_ DWORD Index, BOOL* pEnable) override { return m_pd3dDevice->GetLightEnable(Index, pEnable); }
	virtual STDMETHODIMP SetClipPlane(THIS_ DWORD Index, CONST float* pPlane) override { return m_pd3dDevice->SetClipPlane(Index, pPlane); }
	virtual STDMETHODIMP GetClipPlane(THIS_ DWORD Index, float* pPlane) override { return m_pd3dDevice->GetClipPlane(Index, pPlane); }
	virtual STDMETHODIMP SetClipStatus(THIS_ CONST D3DCLIPSTATUS9* pClipStatus) override { return m_pd3dDevice->SetClipStatus(pClipStatus); }
	virtual STDMETHODIMP GetClipStatus(THIS_ D3DCLIPSTATUS9* pClipStatus) override { return m_pd3dDevice->GetClipStatus(pClipStatus); }
	virtual STDMETHODIMP GetSamplerState(THIS_ DWORD Sampler, D3DSAMPLERSTATETYPE Type, DWORD* pValue) override { return m_pd3dDevice->GetSamplerState(Sampler, Type, pValue); }
	virtual STDMETHODIMP SetSamplerState(THIS_ DWORD Sampler, D3DSAMPLERSTATETYPE Type, DWORD Value) override { return m_pd3dDevice->SetSamplerState(Sampler, Type, Value); }
	virtual STDMETHODIMP ValidateDevice(THIS_ DWORD* pNumPasses) override { return m_pd3dDevice->ValidateDevice(pNumPasses); }
	virtual STDMETHODIMP SetPaletteEntries(THIS_ UINT PaletteNumber, CONST PALETTEENTRY* pEntries) override { return m_pd3dDevice->SetPaletteEntries(PaletteNumber, pEntries); }
	virtual STDMETHODIMP GetPaletteEntries(THIS_ UINT PaletteNumber, PALETTEENTRY* pEntries) override { return m_pd3dDevice->GetPaletteEntries(PaletteNumber, pEntries); }
	virtual STDMETHODIMP SetCurrentTexturePalette(THIS_ UINT PaletteNumber) override { return m_pd3dDevice->SetCurrentTexturePalette(PaletteNumber); }
	virtual STDMETHODIMP GetCurrentTexturePalette(THIS_ UINT* PaletteNumber) override { return m_pd3dDevice->GetCurrentTexturePalette(PaletteNumber); }
	virtual STDMETHODIMP SetSoftwareVertexProcessing(THIS_ BOOL bSoftware) override { return m_pd3dDevice->SetSoftwareVertexProcessing(bSoftware); }
	virtual STDMETHODIMP_(BOOL) GetSoftwareVertexProcessing(THIS) override { return m_pd3dDevice->GetSoftwareVertexProcessing(); }
	virtual STDMETHODIMP SetNPatchMode(THIS_ float nSegments) override { return m_pd3dDevice->SetNPatchMode(nSegments); }
	virtual STDMETHODIMP_(float) GetNPatchMode(THIS) override { return m_pd3dDevice->GetNPatchMode(); }
	virtual STDMETHODIMP ProcessVertices(THIS_ UINT SrcStartIndex, UINT DestIndex, UINT VertexCount, IDirect3DVertexBuffer9* pDestBuffer, IDirect3DVertexDeclaration9* pVertexDecl, DWORD Flags) override { return m_pd3dDevice->ProcessVertices(SrcStartIndex, DestIndex, VertexCount, pDestBuffer, pVertexDecl, Flags); }
	virtual STDMETHODIMP CreateVertexDeclaration(THIS_ CONST D3DVERTEXELEMENT9* pVertexElements, IDirect3DVertexDeclaration9** ppDecl) override { return m_pd3dDevice->CreateVertexDeclaration(pVertexElements, ppDecl); }
	virtual STDMETHODIMP SetVertexDeclaration(THIS_ IDirect3DVertexDeclaration9* pDecl) override { return m_pd3dDevice->SetVertexDeclaration(pDecl); }
	virtual STDMETHODIMP GetVertexDeclaration(THIS_ IDirect3DVertexDeclaration9** ppDecl) override { return m_pd3dDevice->GetVertexDeclaration(ppDecl); }
	virtual STDMETHODIMP SetFVF(THIS_ DWORD FVF_) override { return m_pd3dDevice->SetFVF(FVF_); }
	virtual STDMETHODIMP GetFVF(THIS_ DWORD* pFVF) override { return m_pd3dDevice->GetFVF(pFVF); }
	virtual STDMETHODIMP CreateVertexShader(THIS_ CONST DWORD* pFunction, IDirect3DVertexShader9** ppShader) override { return m_pd3dDevice->CreateVertexShader(pFunction, ppShader); }
	virtual STDMETHODIMP SetVertexShader(THIS_ IDirect3DVertexShader9* pShader) override { return m_pd3dDevice->SetVertexShader(pShader); }
	virtual STDMETHODIMP GetVertexShader(THIS_ IDirect3DVertexShader9** ppShader) override { return m_pd3dDevice->GetVertexShader(ppShader); }
	virtual STDMETHODIMP SetVertexShaderConstantF(THIS_ UINT StartRegister, CONST float* pConstantData, UINT Vector4fCount) override { return m_pd3dDevice->SetVertexShaderConstantF(StartRegister, pConstantData, Vector4fCount); }
	virtual STDMETHODIMP GetVertexShaderConstantF(THIS_ UINT StartRegister, float* pConstantData, UINT Vector4fCount) override { return m_pd3dDevice->GetVertexShaderConstantF(StartRegister, pConstantData, Vector4fCount); }
	virtual STDMETHODIMP SetVertexShaderConstantI(THIS_ UINT StartRegister, CONST int* pConstantData, UINT Vector4iCount) override { return m_pd3dDevice->SetVertexShaderConstantI(StartRegister, pConstantData, Vector4iCount); }
	virtual STDMETHODIMP GetVertexShaderConstantI(THIS_ UINT StartRegister, int* pConstantData, UINT Vector4iCount) override { return m_pd3dDevice->GetVertexShaderConstantI(StartRegister, pConstantData, Vector4iCount); }
	virtual STDMETHODIMP SetVertexShaderConstantB(THIS_ UINT StartRegister, CONST BOOL* pConstantData, UINT BoolCount) override { return m_pd3dDevice->SetVertexShaderConstantB(StartRegister, pConstantData, BoolCount); }
	virtual STDMETHODIMP GetVertexShaderConstantB(THIS_ UINT StartRegister, BOOL* pConstantData, UINT BoolCount) override { return m_pd3dDevice->GetVertexShaderConstantB(StartRegister, pConstantData, BoolCount); }
	virtual STDMETHODIMP SetStreamSource(THIS_ UINT StreamNumber, IDirect3DVertexBuffer9* pStreamData, UINT OffsetInBytes, UINT Stride) override { return m_pd3dDevice->SetStreamSource(StreamNumber, pStreamData, OffsetInBytes, Stride); }
	virtual STDMETHODIMP GetStreamSource(THIS_ UINT StreamNumber, IDirect3DVertexBuffer9** ppStreamData, UINT* pOffsetInBytes, UINT* pStride) override { return m_pd3dDevice->GetStreamSource(StreamNumber, ppStreamData, pOffsetInBytes, pStride); }
	virtual STDMETHODIMP SetStreamSourceFreq(THIS_ UINT StreamNumber, UINT Setting) override { return m_pd3dDevice->SetStreamSourceFreq(StreamNumber, Setting); }
	virtual STDMETHODIMP GetStreamSourceFreq(THIS_ UINT StreamNumber, UINT* pSetting) override { return m_pd3dDevice->GetStreamSourceFreq(StreamNumber, pSetting); }
	virtual STDMETHODIMP SetIndices(THIS_ IDirect3DIndexBuffer9* pIndexData) override { return m_pd3dDevice->SetIndices(pIndexData); }
	virtual STDMETHODIMP GetIndices(THIS_ IDirect3DIndexBuffer9** ppIndexData) override { return m_pd3dDevice->GetIndices(ppIndexData); }
	virtual STDMETHODIMP CreatePixelShader(THIS_ CONST DWORD* pFunction, IDirect3DPixelShader9** ppShader) override { return m_pd3dDevice->CreatePixelShader(pFunction, ppShader); }
	virtual STDMETHODIMP SetPixelShader(THIS_ IDirect3DPixelShader9* pShader) override { return m_pd3dDevice->SetPixelShader(pShader); }
	virtual STDMETHODIMP GetPixelShader(THIS_ IDirect3DPixelShader9** ppShader) override { return m_pd3dDevice->GetPixelShader(ppShader); }
	virtual STDMETHODIMP SetPixelShaderConstantF(THIS_ UINT StartRegister, CONST float* pConstantData, UINT Vector4fCount) override { return m_pd3dDevice->SetPixelShaderConstantF(StartRegister, pConstantData, Vector4fCount); }
	virtual STDMETHODIMP GetPixelShaderConstantF(THIS_ UINT StartRegister, float* pConstantData, UINT Vector4fCount) override { return m_pd3dDevice->GetPixelShaderConstantF(StartRegister, pConstantData, Vector4fCount); }
	virtual STDMETHODIMP SetPixelShaderConstantI(THIS_ UINT StartRegister, CONST int* pConstantData, UINT Vector4iCount) override { return m_pd3dDevice->SetPixelShaderConstantI(StartRegister, pConstantData, Vector4iCount); }
	virtual STDMETHODIMP GetPixelShaderConstantI(THIS_ UINT StartRegister, int* pConstantData, UINT Vector4iCount) override { return m_pd3dDevice->GetPixelShaderConstantI(StartRegister, pConstantData, Vector4iCount); }
	virtual STDMETHODIMP SetPixelShaderConstantB(THIS_ UINT StartRegister, CONST BOOL* pConstantData, UINT BoolCount) override { return m_pd3dDevice->SetPixelShaderConstantB(StartRegister, pConstantData, BoolCount); }
	virtual STDMETHODIMP GetPixelShaderConstantB(THIS_ UINT StartRegister, BOOL* pConstantData, UINT BoolCount) override { return m_pd3dDevice->GetPixelShaderConstantB(StartRegister, pConstantData, BoolCount); }
	virtual STDMETHODIMP DrawRectPatch(THIS_ UINT Handle, CONST float* pNumSegs, CONST D3DRECTPATCH_INFO* pRectPatchInfo) override { return m_pd3dDevice->DrawRectPatch(Handle, pNumSegs, pRectPatchInfo); }
	virtual STDMETHODIMP DrawTriPatch(THIS_ UINT Handle, CONST float* pNumSegs, CONST D3DTRIPATCH_INFO* pTriPatchInfo) override { return m_pd3dDevice->DrawTriPatch(Handle, pNumSegs, pTriPatchInfo); }
	virtual STDMETHODIMP DeletePatch(THIS_ UINT Handle) override { return m_pd3dDevice->DeletePatch(Handle); }
	virtual STDMETHODIMP CreateQuery(THIS_ D3DQUERYTYPE Type, IDirect3DQuery9** ppQuery) override { return m_pd3dDevice->CreateQuery(Type, ppQuery); }
	virtual STDMETHODIMP CreateVolumeTexture(THIS_ UINT Width, UINT Height, UINT Depth, UINT Levels, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DVolumeTexture9** ppVolumeTexture, HANDLE* pSharedHandle) override { return m_pd3dDevice->CreateVolumeTexture(Width, Height, Depth, Levels, Usage, Format, Pool, ppVolumeTexture, pSharedHandle); }
	virtual STDMETHODIMP CreateCubeTexture(THIS_ UINT EdgeLength, UINT Levels, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DCubeTexture9** ppCubeTexture, HANDLE* pSharedHandle) override { return m_pd3dDevice->CreateCubeTexture(EdgeLength, Levels, Usage, Format, Pool, ppCubeTexture, pSharedHandle); }
	virtual STDMETHODIMP CreateVertexBuffer(THIS_ UINT Length, DWORD Usage, DWORD FVF_, D3DPOOL Pool, IDirect3DVertexBuffer9** ppVertexBuffer, HANDLE* pSharedHandle) override { return m_pd3dDevice->CreateVertexBuffer(Length, Usage, FVF_, Pool, ppVertexBuffer, pSharedHandle); }
	virtual STDMETHODIMP CreateIndexBuffer(THIS_ UINT Length, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DIndexBuffer9** ppIndexBuffer, HANDLE* pSharedHandle) override { return m_pd3dDevice->CreateIndexBuffer(Length, Usage, Format, Pool, ppIndexBuffer, pSharedHandle); }
	virtual STDMETHODIMP CreateRenderTarget(THIS_ UINT Width, UINT Height, D3DFORMAT Format, D3DMULTISAMPLE_TYPE MultiSample, DWORD MultisampleQuality, BOOL Lockable, IDirect3DSurface9** ppSurface, HANDLE* pSharedHandle) override { return m_pd3dDevice->CreateRenderTarget(Width, Height, Format, MultiSample, MultisampleQuality, Lockable, ppSurface, pSharedHandle); }
	virtual STDMETHODIMP CreateDepthStencilSurface(THIS_ UINT Width, UINT Height, D3DFORMAT Format, D3DMULTISAMPLE_TYPE MultiSample, DWORD MultisampleQuality, BOOL Discard, IDirect3DSurface9** ppSurface, HANDLE* pSharedHandle) override { return m_pd3dDevice->CreateDepthStencilSurface(Width, Height, Format, MultiSample, MultisampleQuality, Discard, ppSurface, pSharedHandle); }
	virtual STDMETHODIMP GetRenderTargetData(THIS_ IDirect3DSurface9* pRenderTarget, IDirect3DSurface9* pDestSurface) override { return m_pd3dDevice->GetRenderTargetData(pRenderTarget, pDestSurface); }
	virtual STDMETHODIMP GetFrontBufferData(THIS_ UINT iSwapChain, IDirect3DSurface9* pDestSurface) override { return m_pd3dDevice->GetFrontBufferData(iSwapChain, pDestSurface); }
	virtual STDMETHODIMP ColorFill(THIS_ IDirect3DSurface9* pSurface, CONST RECT* pRect, D3DCOLOR color) override { return m_pd3dDevice->ColorFill(pSurface, pRect, color); }
	virtual STDMETHODIMP CreateOffscreenPlainSurface(THIS_ UINT Width, UINT Height, D3DFORMAT Format, D3DPOOL Pool, IDirect3DSurface9** ppSurface, HANDLE* pSharedHandle) override { return m_pd3dDevice->CreateOffscreenPlainSurface(Width, Height, Format, Pool, ppSurface, pSharedHandle); }
	virtual STDMETHODIMP SetRenderTarget(THIS_ DWORD RenderTargetIndex, IDirect3DSurface9* pRenderTarget) override { return m_pd3dDevice->SetRenderTarget(RenderTargetIndex, pRenderTarget); }
	virtual STDMETHODIMP GetRenderTarget(THIS_ DWORD RenderTargetIndex, IDirect3DSurface9** ppRenderTarget) override { return m_pd3dDevice->GetRenderTarget(RenderTargetIndex, ppRenderTarget); }
	virtual STDMETHODIMP SetDepthStencilSurface(THIS_ IDirect3DSurface9* pNewZStencil) override { return m_pd3dDevice->SetDepthStencilSurface(pNewZStencil); }
	virtual STDMETHODIMP GetDepthStencilSurface(THIS_ IDirect3DSurface9** ppZStencilSurface) override { return m_pd3dDevice->GetDepthStencilSurface(ppZStencilSurface); }
	virtual STDMETHODIMP UpdateSurface(THIS_ IDirect3DSurface9* pSourceSurface, CONST RECT* pSourceRect, IDirect3DSurface9* pDestinationSurface, CONST POINT* pDestPoint) override { return m_pd3dDevice->UpdateSurface(pSourceSurface, pSourceRect, pDestinationSurface, pDestPoint); }
	virtual STDMETHODIMP StretchRect(THIS_ IDirect3DSurface9* pSourceSurface, CONST RECT* pSourceRect, IDirect3DSurface9* pDestSurface, CONST RECT* pDestRect, D3DTEXTUREFILTERTYPE Filter) override { return m_pd3dDevice->StretchRect(pSourceSurface, pSourceRect, pDestSurface, pDestRect, Filter); }

	// Overrides
	virtual STDMETHODIMP_(ULONG) Release(THIS) override;
	virtual STDMETHODIMP CreateTexture(THIS_ UINT Width, UINT Height, UINT Levels, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DTexture9** ppTexture, HANDLE* pSharedHandle) override;
	virtual STDMETHODIMP UpdateTexture(THIS_ IDirect3DBaseTexture9* pSourceTexture, IDirect3DBaseTexture9* pDestinationTexture) override;
	virtual STDMETHODIMP GetTexture(THIS_ DWORD Stage, IDirect3DBaseTexture9** ppTexture) override;
	virtual STDMETHODIMP SetTexture(THIS_ DWORD Stage, IDirect3DBaseTexture9* pTexture) override;

	// Return a wrapper from the list once its reference count hits zero
	void removeTextureWrapper(IDirect3DBaseTexture9* pTexture);

	// Return true if input is a known texture wrapper.
	bool isTextureWrapper(IDirect3DBaseTexture9* pTextureOrWrapper);

	// Map input to a D3D texture pointer. If input is a wrapper, return its underlying D3D texture pointer. Otherwise return itself.
	IDirect3DBaseTexture9* getD3DTexture(IDirect3DBaseTexture9* pTextureOrWrapper);
	IDirect3DTexture9* getD3DTexture(IDirect3DTexture9* pTextureOrWrapper);

private:
	IDirect3DDevice9* m_pd3dDevice;
	std::mutex m_textureWrapperMutex;
	std::set<IDirect3DBaseTexture9*> m_textureWrappers; // Set of known texture wrappers - each element is an instance of D3DTextureWrapper class
};

} // namespace KEP
