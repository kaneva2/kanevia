///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "dxstdafx.h"
#include "D3D9TextureWrapper.h"
#include "D3D9DeviceWrapper.h"

namespace KEP {

STDMETHODIMP_(ULONG)
D3D9TextureWrapper::Release(THIS) {
	auto rc = m_pd3dTexture->Release();
	if (rc == 0) {
		m_pd3dDeviceWrapper->removeTextureWrapper(static_cast<LPDIRECT3DTEXTURE9>(this));
		delete this;
	}
	return rc;
}

STDMETHODIMP D3D9TextureWrapper::GetDevice(THIS_ LPDIRECT3DDEVICE9* ppDevice) {
	if (ppDevice != nullptr) {
		*ppDevice = m_pd3dDeviceWrapper;
		return D3D_OK;
	}
	return D3DERR_INVALIDCALL;
}

STDMETHODIMP D3D9TextureWrapper::LockRect(THIS_ UINT Level, D3DLOCKED_RECT* pLockedRect, CONST RECT* pRect, DWORD Flags) {
	incRevision();
	return m_pd3dTexture->LockRect(Level, pLockedRect, pRect, Flags);
}

} // namespace KEP
