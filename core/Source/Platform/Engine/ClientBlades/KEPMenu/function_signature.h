///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace script_glue {

class null_parameter {
};

///////////////////////////////////////////////////////////////////////////
// templates to define function signatures for various number of params
///////////////////////////////////////////////////////////////////////////

// this is the non-specialized version

template <
	typename out_val_type,
	typename in_val_type1,
	typename in_val_type2,
	typename in_val_type3,
	typename in_val_type4,
	typename in_val_type5,
	typename in_val_type6,
	typename in_val_type7,
	typename in_val_type8,
	typename in_val_type9,
	typename in_val_type10>
class function_signature {
public:
	typedef out_val_type (*FunctionType)(
		in_val_type1,
		in_val_type2,
		in_val_type3,
		in_val_type4,
		in_val_type5,
		in_val_type6,
		in_val_type7,
		in_val_type8,
		in_val_type9,
		in_val_type10);

	static out_val_type make_call(
		in_val_type1 iv1,
		in_val_type2 iv2,
		in_val_type3 iv3,
		in_val_type3 iv4,
		in_val_type3 iv5,
		in_val_type3 iv6,
		in_val_type3 iv7,
		in_val_type3 iv8,
		in_val_type3 iv9,
		in_val_type3 iv10,
		FunctionType f) {
		return f(iv1, iv2, iv3, iv4, iv5, iv6, iv7, iv8, iv9, iv10);
	}

	template <int I>
	struct get_param_type {
	};

	template <>
	struct get_param_type<0> {
		typedef in_val_type1 param_type;
	};

	template <>
	struct get_param_type<1> {
		typedef in_val_type2 param_type;
	};

	template <>
	struct get_param_type<2> {
		typedef in_val_type3 param_type;
	};

	template <>
	struct get_param_type<3> {
		typedef in_val_type4 param_type;
	};

	template <>
	struct get_param_type<4> {
		typedef in_val_type5 param_type;
	};

	template <>
	struct get_param_type<5> {
		typedef in_val_type6 param_type;
	};

	template <>
	struct get_param_type<6> {
		typedef in_val_type7 param_type;
	};

	template <>
	struct get_param_type<7> {
		typedef in_val_type8 param_type;
	};

	template <>
	struct get_param_type<8> {
		typedef in_val_type9 param_type;
	};

	template <>
	struct get_param_type<9> {
		typedef in_val_type10 param_type;
	};
};

// now specialize it for different number of params

#define NUM_PARAMS 9
#include "function_signature_specialization.h"
#undef NUM_PARAMS

#define NUM_PARAMS 8
#include "function_signature_specialization.h"
#undef NUM_PARAMS

#define NUM_PARAMS 7
#include "function_signature_specialization.h"
#undef NUM_PARAMS

#define NUM_PARAMS 6
#include "function_signature_specialization.h"
#undef NUM_PARAMS

#define NUM_PARAMS 5
#include "function_signature_specialization.h"
#undef NUM_PARAMS

#define NUM_PARAMS 4
#include "function_signature_specialization.h"
#undef NUM_PARAMS

#define NUM_PARAMS 3
#include "function_signature_specialization.h"
#undef NUM_PARAMS

#define NUM_PARAMS 2
#include "function_signature_specialization.h"
#undef NUM_PARAMS

#define NUM_PARAMS 1
#include "function_signature_specialization.h"
#undef NUM_PARAMS

#define NUM_PARAMS 0
#include "function_signature_specialization.h"
#undef NUM_PARAMS

} // namespace script_glue