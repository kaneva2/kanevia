///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "dxstdafx.h"
#include <TinyXML/tinyxml.h>
#include "Handler.h"

static const LPCSTR BROWSER_DEFAULT_NAME = "Browser";

#include "DXUTExternal.h"
#include "../../common/include/corestatic/ExternalMesh.h"
using namespace KEP;

CDXUTExternal::CDXUTExternal(DXUT_CONTROL_TYPE type, const char* tag, const char* defaultName, CDXUTDialog* pDialog) :
		m_externalMesh(0),
		m_bPressed(false),
		m_prevX(0),
		m_prevY(0),
		m_prevWidth(0),
		m_prevHeight(0),
		m_loaded(false) {
	m_Type = type;
	m_Tag = tag;
	m_pDialog = pDialog;

	m_defaultName = defaultName;

	// must add one display tag, even if do nothing with it.
	m_elementTags.clear();
	m_elementTags[DISPLAY_TAG] = ElementTagMapValue(0, false, false);
	m_Elements[0] = new CDXUTElement();
}

CDXUTExternal::~CDXUTExternal() {
	if (m_externalMesh) {
		m_externalMesh->Delete();
	}
	m_externalMesh = NULL;
}

HRESULT CDXUTExternal::SetText(const char* strText) {
	HRESULT ret = CDXUTStatic::SetText(strText);

	if (SUCCEEDED(ret) && m_loaded)
		renderExternalFromText();

	return ret;
}

bool CDXUTExternal::Load(TiXmlNode* Browser_root) {
	bool ret = false;

	if (Browser_root) {
		ret = CDXUTStatic::Load(Browser_root);
	}

	m_loaded = ret;

	if (ret && GetText() && ::strlen(GetText()) != 0)
		ret = renderExternalFromText();

	return ret;
}

void CDXUTExternal::Render(float fElapsedTime) {
	DXUT_CONTROL_STATE iState = DXUT_STATE_NORMAL;

	// if editing, show control as 'normal'
	if (!GetVisible() && !m_pDialog->GetEdit()) {
		iState = DXUT_STATE_HIDDEN;
	} else if (m_bEnabled == false) {
		iState = DXUT_STATE_DISABLED;
	}

	if (iState == DXUT_STATE_NORMAL && m_externalMesh != 0) {
		LPDIRECT3DTEXTURE9 pTexture = 0;
		if (m_externalMesh != 0) {
			m_externalMesh->UpdateTexture();

			pTexture = m_externalMesh->GetTexture();

			POINT pt;
			m_pDialog->GetLocation(pt);

			POINT myPos;
			GetLocation(myPos);

			if (m_prevX != myPos.x + pt.x ||
				m_prevY != myPos.y + pt.y ||
				m_prevWidth != GetWidth() ||
				m_prevHeight != GetHeight())

			{
				m_externalMesh->UpdateRect(pt.x + myPos.x, GetWidth(), pt.y + myPos.y + (m_pDialog->GetCaptionEnabled() ? m_pDialog->GetCaptionHeight() : 0), GetHeight());

				m_prevX = myPos.x + pt.x;
				m_prevY = myPos.y + pt.y;
				m_prevWidth = GetWidth();
				m_prevHeight = GetHeight();
			}
		}

		// No need to draw fully transparent layers
		// get the rects
		RECT myRect;
		myRect.left = myRect.top = 0;
		myRect.bottom = GetHeight();
		myRect.right = GetWidth();

		RECT rcScreen;
		POINT pt;
		m_pDialog->GetLocation(pt);
		POINT myPos;
		GetLocation(myPos);
		rcScreen.top = myPos.y;
		rcScreen.bottom = myPos.y + GetHeight();
		rcScreen.left = myPos.x;
		rcScreen.right = myPos.x + GetWidth();

		OffsetRect(&rcScreen, pt.x, pt.y);

		// If caption is enabled, offset the Y position by its height.
		if (m_pDialog->GetCaptionEnabled())
			OffsetRect(&rcScreen, 0, m_pDialog->GetCaptionHeight());

		D3DXVECTOR3 vPos((float)rcScreen.left, (float)rcScreen.top, 0.0f);

		D3DCOLOR color = D3DCOLOR_XRGB(255, 255, 255);
		ID3DXSprite* sprite = DXUTGetGlobalDialogResourceManager()->GetSprite();
		if (sprite)
			sprite->Draw(pTexture, &myRect, NULL, &vPos, color);
	}

	CDXUTControl::Render(fElapsedTime);
}

bool CDXUTExternal::HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam) {
	bool ret = CDXUTControl::HandleKeyboard(uMsg, wParam, lParam);
	if (m_externalMesh == 0)
		return ret;

	if (!m_bEnabled || !GetVisible())
		return false;

	switch (uMsg) {
		case WM_KEYDOWN: {
			m_externalMesh->SendInputMessage(uMsg, wParam, lParam);
			m_bPressed = true;
			return true;
		}

		case WM_KEYUP: {
			if (m_bPressed == true) {
				m_bPressed = false;
				m_externalMesh->SendInputMessage(uMsg, wParam, lParam);
			}
			return true;
		}
	}
	return false;
}

bool CDXUTExternal::HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam) {
	bool ret = CDXUTControl::HandleMouse(uMsg, pt, wParam, lParam);

	if (m_externalMesh == 0)
		return ret;

	if (!ret) {
		if (!m_bEnabled || !GetVisible())
			return false;

		POINT myPos;
		GetLocation(myPos);

		switch (uMsg) {
			case WM_MOUSEMOVE: {
				if (ContainsPoint(pt)) {
					m_externalMesh->SendInputMessage(uMsg, wParam, MAKELPARAM(pt.x - myPos.x, pt.y - myPos.y));
					return true;
				}
				break;
			}

			case WM_LBUTTONDOWN:
			case WM_LBUTTONDBLCLK: {
				if (ContainsPoint(pt)) {
					m_externalMesh->SendInputMessage(uMsg, wParam, MAKELPARAM(pt.x - myPos.x, pt.y - myPos.y));
					// Pressed while inside the control
					m_bPressed = true;
					if (!GetIsInFocus())
						m_pDialog->RequestFocus(this);

					return true;
				}

				break;
			}

			case WM_LBUTTONUP: {
				if (m_bPressed) {
					m_bPressed = false;
					m_externalMesh->SendInputMessage(uMsg, wParam, MAKELPARAM(pt.x - myPos.x, pt.y - myPos.y));

					if (!m_pDialog->GetEnableKeyboardInput())
						m_pDialog->ClearFocus();

					return true;
				}

				break;
			}
		};

		return false;
	}

	return true;
}
