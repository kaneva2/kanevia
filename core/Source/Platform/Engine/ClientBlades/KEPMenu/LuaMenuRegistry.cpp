#include <d3d9.h>
#include "LuaMenuRegistry.h"
#include "..\imenugui.h"
#include "MenuCInterface.h"
#include "script_glue.h"
#include "LuaHelper.h"
#include <map>

using namespace std;
using namespace script_glue;

namespace KEP {

static map<lua_State*, registration_vector*>* registry_dictionary = 0;

// NOT THREAD SAFE!!!! (but that should be ok)
// To make thread safe, probably can simply use TLS for registry_dictionary since
// probably always add/remove on same thread.
void add_registry(lua_State* native, registration_vector* native_registry) { // for cleanup later
	if (registry_dictionary == 0)
		registry_dictionary = new map<lua_State*, registration_vector*>();

	assert(registry_dictionary->find(native) == registry_dictionary->end());
	(*registry_dictionary)[native] = native_registry;
}

void remove_all_registries() {
	if (registry_dictionary == 0)
		return;

	map<lua_State*, registration_vector*>::iterator i;

	// clean it up
	for (i = registry_dictionary->begin(); i != registry_dictionary->end(); i++) {
		registration_vector::iterator itor = i->second->begin();
		while (itor != i->second->end()) {
			delete (*itor);
			itor++;
		}

		i->second->clear();
		delete i->second;
	}

	registry_dictionary->clear();
	delete registry_dictionary;
	registry_dictionary = 0;
}

void remove_registry(void* vm) {
	lua_State* native = (lua_State*)vm;

	assert(registry_dictionary != 0);
	if (registry_dictionary == 0)
		return;

	map<lua_State*, registration_vector*>::iterator i = registry_dictionary->find(native);
	// ok to try to cleanup on not there assert( i != registry_dictionary->end() );

	// clean it up
	if (i != registry_dictionary->end()) {
		registration_vector::iterator itor = i->second->begin();
		while (itor != i->second->end()) {
			delete (*itor);
			itor++;
		}

		i->second->clear();
		delete i->second;
		registry_dictionary->erase(i);
	}

	if (registry_dictionary->size() == 0) {
		delete registry_dictionary;
		registry_dictionary = 0;
	}
}

} // namespace KEP