///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "dxstdafx.h"
#include <TinyXML/tinyxml.h>
#include "DXUTFlashPlayer.h"
#include "Handler.h"

#include "tinyxmlutil.h"
#include "KepMenu.h"
#include "../../common/include/corestatic/ExternalMesh.h"

#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

static const LPCSTR FLASH_DEFAULT_NAME = "FlashPlayer";

// flash parameters
static const LPCSTR LOOP_TAG = "loop";
static const LPCSTR WANT_INPUT_TAG = "wantInput";
static const LPCSTR PARAMS_TAG = "params";
static const LPCSTR SIZE_TO_FIT_TAG = "sizeToFit";

void CDXUTFlashPlayer::OnFocusIn() {
	m_externalMesh->SetFocus();
}
void CDXUTFlashPlayer::OnFocusOut() {
	// TODO: how to get focus back to menu
}

//--------------------------------------------------------------------------------------
// CDXUTFlashPlayer class
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
CDXUTFlashPlayer::CDXUTFlashPlayer(CDXUTDialog *pDialog) :
		CDXUTExternal(DXUT_CONTROL_FLASH, FLASH_TAG, FLASH_DEFAULT_NAME, pDialog),
		m_sizeToFit(true),
		m_wantInput(true),
		m_loopSwf(false) {
}

bool CDXUTFlashPlayer::Load(TiXmlNode *flash_root) {
	bool ret = false;

	if (flash_root) {
		// given the checkbox root, iterate over the children.
		TiXmlNode *child = 0;
		for (child = flash_root->FirstChild(); child; child = child->NextSibling()) {
			const char *val = child->Value();

			if (!_stricmp(val, LOOP_TAG)) {
				bool bChecked = false;
				if (GetBoolFromXMLNode(child, bChecked))
					m_loopSwf = bChecked;
			} else if (!_stricmp(val, WANT_INPUT_TAG)) {
				bool bChecked = true;
				if (GetBoolFromXMLNode(child, bChecked))
					m_wantInput = bChecked;
			} else if (!_stricmp(val, PARAMS_TAG)) {
				std::string s;
				if (GetStringFromXMLNode(child, s))
					m_parameters = s;
			} else if (!_stricmp(val, SIZE_TO_FIT_TAG)) {
				bool bChecked = false;
				if (GetBoolFromXMLNode(child, bChecked))
					m_sizeToFit = bChecked;
			}
		}

		ret = CDXUTExternal::Load(flash_root);
	}

	return ret;
}

//--------------------------------------------------------------------------------------
bool CDXUTFlashPlayer::Save(TiXmlNode *flash_root) {
	bool bSuccess = false;

	if (flash_root) {
		TiXmlNode *child = flash_root->InsertEndChild(TiXmlElement(WANT_INPUT_TAG));
		if (child) {
			PutBoolToXMLNode(child, GetWantInput());
		}
		child = flash_root->InsertEndChild(TiXmlElement(SIZE_TO_FIT_TAG));
		if (child) {
			PutBoolToXMLNode(child, GetSizeToFit());
		}
		child = flash_root->InsertEndChild(TiXmlElement(LOOP_TAG));
		if (child) {
			PutBoolToXMLNode(child, GetLoopSwf());
		}
		child = flash_root->InsertEndChild(TiXmlElement(PARAMS_TAG));
		if (child) {
			PutStringToXMLNode(child, GetSwfParameters());
		}

		bSuccess = CDXUTExternal::Save(flash_root);
	}

	return bSuccess;
}

// DRF - Used For Flash Games Only!
bool CDXUTFlashPlayer::renderExternalFromText() {
	// Don't Play Flash Game While Editing Menu
	if (m_pDialog->GetEdit())
		return true;

	// Get Flash Frame Position Within Dialog
	POINT myPos;
	GetLocation(myPos, false);

	// Set Media Url From Static Text Field
	std::string mUrl = GetText() ? GetText() : "";
	if (!STLContains(mUrl, "http")) {
		// Local Path - Check ScriptData Folder
		mUrl = PathAdd(PathAdd(IMenuDialog::GetDefaultMenuDir(), "..\\ScriptData"), mUrl);

		// for now only resize local files, not remote ones
		if (m_sizeToFit) {
			uint32_t cx, cy;
			if (KEPMenu::Instance()->GetEngineInterface()->GetFlashMovieSize(mUrl, cx, cy)) {
				// keep it centered
				LONG xDelta = (GetWidth() - (int)cx) / 2;
				LONG yDelta = (GetHeight() - (int)cy) / 2;
				SetLocationX(myPos.x + xDelta);
				SetLocationY(myPos.y + yDelta);
				SetSize((int)cx, (int)cy);
			} else {
				LogError("GetMovieSize() FAILED");
			}
		}
	}

	// Get Dialog Position
	POINT pt;
	m_pDialog->GetLocation(pt);
	GetLocation(myPos, false);

	// Build Media Params
	MediaParams mediaParams(mUrl, m_parameters);
	LogInfo("FLASH GAME - " << mediaParams.ToStr());

	// Create New Flash Player Window
	if (m_externalMesh) {
		m_externalMesh->Delete();
		m_externalMesh = nullptr;
	}

	m_externalMesh = KEPMenu::Instance()->GetEngineInterface()->CreateFlashMesh(
		DXUTGetHWND(),
		mediaParams,
		GetWidth(), GetHeight(),
		m_loopSwf, m_wantInput,
		pt.x + myPos.x, pt.y + myPos.y + (m_pDialog->GetCaptionEnabled() ? m_pDialog->GetCaptionHeight() : 0));
	return true;
}

} // namespace KEP