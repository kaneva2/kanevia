///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

class CD3DEnumeration;

struct DXUTDeviceSettings {
	UINT AdapterOrdinal;
	D3DDEVTYPE DeviceType;
	D3DFORMAT AdapterFormat;
	DWORD BehaviorFlags;
	D3DPRESENT_PARAMETERS pp;
};

#define DXUTERR_NODIRECT3D MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0901)
#define DXUTERR_NOCOMPATIBLEDEVICES MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0902)
#define DXUTERR_MEDIANOTFOUND MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0903)
#define DXUTERR_NONZEROREFCOUNT MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0904)
#define DXUTERR_CREATINGDEVICE MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0905)
#define DXUTERR_RESETTINGDEVICE MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0906)
#define DXUTERR_CREATINGDEVICEOBJECTS MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0907)
#define DXUTERR_RESETTINGDEVICEOBJECTS MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0908)
#define DXUTERR_INCORRECTVERSION MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0909)

typedef bool(CALLBACK* LPDXUTCALLBACKISDEVICEACCEPTABLE)(D3DCAPS9* pCaps, D3DFORMAT AdapterFormat, D3DFORMAT BackBufferFormat, bool bWindowed);
typedef void(CALLBACK* LPDXUTCALLBACKMODIFYDEVICESETTINGS)(DXUTDeviceSettings* pDeviceSettings, const D3DCAPS9* pCaps);
typedef HRESULT(CALLBACK* LPDXUTCALLBACKDEVICECREATED)(IDirect3DDevice9* pd3dDevice, const D3DSURFACE_DESC* pBackBufferSurfaceDesc);
typedef HRESULT(CALLBACK* LPDXUTCALLBACKDEVICERESET)(IDirect3DDevice9* pd3dDevice, const D3DSURFACE_DESC* pBackBufferSurfaceDesc);
typedef void(CALLBACK* LPDXUTCALLBACKDEVICEDESTROYED)();
typedef void(CALLBACK* LPDXUTCALLBACKDEVICELOST)();
typedef void(CALLBACK* LPDXUTCALLBACKFRAMEMOVE)(IDirect3DDevice9* pd3dDevice, double fTime, float fElapsedTime);
typedef void(CALLBACK* LPDXUTCALLBACKFRAMERENDER)(IDirect3DDevice9* pd3dDevice, double fTime, float fElapsedTime);
typedef void(CALLBACK* LPDXUTCALLBACKKEYBOARD)(UINT nChar, bool bKeyDown, bool bAltDown);
typedef void(CALLBACK* LPDXUTCALLBACKMOUSE)(bool bLeftButtonDown, bool bRightButtonDown, bool bMiddleButtonDown, bool bSideButton1Down, bool bSideButton2Down, int nMouseWheelDelta, int xPos, int yPos);
typedef LRESULT(CALLBACK* LPDXUTCALLBACKMSGPROC)(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, bool* pbNoFurtherProcessing);
typedef void(CALLBACK* LPDXUTCALLBACKTIMER)(UINT idEvent);

// Device callbacks
void DXUTSetCallbackDeviceCreated(LPDXUTCALLBACKDEVICECREATED pCallbackDeviceCreated);
void DXUTSetCallbackDeviceReset(LPDXUTCALLBACKDEVICERESET pCallbackDeviceReset);
void DXUTSetCallbackDeviceLost(LPDXUTCALLBACKDEVICELOST pCallbackDeviceLost);
void DXUTSetCallbackDeviceDestroyed(LPDXUTCALLBACKDEVICEDESTROYED pCallbackDeviceDestroyed);

// Frame callbacks
void DXUTSetCallbackFrameMove(LPDXUTCALLBACKFRAMEMOVE pCallbackFrameMove);
void DXUTSetCallbackFrameRender(LPDXUTCALLBACKFRAMERENDER pCallbackFrameRender);

// Message callbacks
void DXUTSetCallbackKeyboard(LPDXUTCALLBACKKEYBOARD pCallbackKeyboard);
void DXUTSetCallbackMouse(LPDXUTCALLBACKMOUSE pCallbackMouse, bool bIncludeMouseMove = false);
void DXUTSetCallbackMsgProc(LPDXUTCALLBACKMSGPROC pCallbackMsgProc);

HRESULT DXUTInit();

bool DXUTSyncWindowRect(bool adjStyle); // drf - added

HRESULT DXUTSetWindow(HWND hWnd, bool bHandleMessages = true);

LRESULT CALLBACK DXUTStaticWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

HRESULT DXUTSetDeviceWithoutCreatingWindow(IDirect3DDevice9* pd3dDevice);

enum DXUT_MATCH_TYPE {
	DXUTMT_IGNORE_INPUT = 0, // Use the closest valid value to a default
	DXUTMT_PRESERVE_INPUT, // Use input without change, but may cause no valid device to be found
	DXUTMT_CLOSEST_TO_INPUT // Use the closest valid value to the input
};

struct DXUTMatchOptions {
	DXUT_MATCH_TYPE eAdapterOrdinal;
	DXUT_MATCH_TYPE eDeviceType;
	DXUT_MATCH_TYPE eWindowed;
	DXUT_MATCH_TYPE eAdapterFormat;
	DXUT_MATCH_TYPE eVertexProcessing;
	DXUT_MATCH_TYPE eResolution;
	DXUT_MATCH_TYPE eBackBufferFormat;
	DXUT_MATCH_TYPE eBackBufferCount;
	DXUT_MATCH_TYPE eMultiSample;
	DXUT_MATCH_TYPE eSwapEffect;
	DXUT_MATCH_TYPE eDepthFormat;
	DXUT_MATCH_TYPE eStencilFormat;
	DXUT_MATCH_TYPE ePresentFlags;
	DXUT_MATCH_TYPE eRefreshRate;
	DXUT_MATCH_TYPE ePresentInterval;
};

void DXUTSetCursorSettings(bool bShowCursor, bool bClipCursorWhenFullScreen, bool bUseWindowsCursor);
void DXUTSetCursorTexture(const char* sCursorFilename, POINT hotSpot);

void DXUTPause(bool bPauseTime, bool bPauseRendering);
void DXUTShutdown();

IDirect3D9* DXUTGetD3DObject(); // Does not addref unlike typical Get* APIs
IDirect3DDevice9* DXUTGetD3DDevice(); // Does not addref unlike typical Get* APIs
DXUTDeviceSettings DXUTGetDeviceSettings();
D3DPRESENT_PARAMETERS DXUTGetPresentParameters();
const D3DSURFACE_DESC* DXUTGetBackBufferSurfaceDesc();
const D3DCAPS9* DXUTGetDeviceCaps();
HWND DXUTGetHWND();
HWND DXUTGetHWNDFocus();
HWND DXUTGetHWNDDeviceFullScreen();
HWND DXUTGetHWNDDeviceWindowed();
const RECT& DXUTGetWindowClientRect();
double DXUTGetTime();
float DXUTGetElapsedTime();
bool DXUTIsWindowed();
float DXUTGetFPS();
LPCTSTR DXUTGetWindowTitle();
bool DXUTIsRenderingPaused();

} // namespace KEP