///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "dxstdafx.h"
#include "DXUTGui.h"
#include "DXUTSelectionHandles.h"

namespace KEP {

//--------------------------------------------------------------------------------------
HRESULT CDXUTSelectionHandles::SetSelectionHandles(int x, int y, int nWidth, int nHeight) {
	// init the handles
	m_handles[TOP_LEFT] = m_rcHandle;
	for (int i = 1; i < NUM_HANDLES; i++) {
		m_handles[i] = m_handles[TOP_LEFT];
	}

	int width = (m_handles[TOP_LEFT]).right - (m_handles[TOP_LEFT]).left;
	int height = (m_handles[TOP_LEFT]).bottom - (m_handles[TOP_LEFT]).top;
	int halfWidth = width >> 1;
	int halfHeight = height >> 1;

	int halfControlWidth = nWidth >> 1;
	int halfControlHeight = nHeight >> 1;

	// 8 selection 'points'

	// top-left
	OffsetRect(&(m_handles[TOP_LEFT]), x, y);
	OffsetRect(&(m_handles[TOP_LEFT]), -1 * width, -1 * height);

	// top-middle
	OffsetRect(&(m_handles[TOP_MIDDLE]), x + halfControlWidth, y);
	OffsetRect(&(m_handles[TOP_MIDDLE]), -1 * halfWidth, -1 * height);

	// top-right
	OffsetRect(&(m_handles[TOP_RIGHT]), x + nWidth, y);
	OffsetRect(&(m_handles[TOP_RIGHT]), 0, -1 * height);

	// bottom-left
	OffsetRect(&(m_handles[BOTTOM_LEFT]), x, y + nHeight);
	OffsetRect(&(m_handles[BOTTOM_LEFT]), -1 * width, 0);

	// bottom-middle
	OffsetRect(&(m_handles[BOTTOM_MIDDLE]), x + halfControlWidth, y + nHeight);
	OffsetRect(&(m_handles[BOTTOM_MIDDLE]), -1 * halfWidth, 0);

	// bottom-right
	OffsetRect(&(m_handles[BOTTOM_RIGHT]), x + nWidth, y + nHeight);

	// left-middle
	OffsetRect(&(m_handles[LEFT_MIDDLE]), x, y + halfControlHeight);
	OffsetRect(&(m_handles[LEFT_MIDDLE]), -1 * width, -1 * halfHeight);

	// right-middle
	OffsetRect(&(m_handles[RIGHT_MIDDLE]), x + nWidth, y + halfControlHeight);
	OffsetRect(&(m_handles[RIGHT_MIDDLE]), 0, -1 * halfHeight);

	return S_OK;
}

//--------------------------------------------------------------------------------------
CDXUTSelectionHandles::SelectionHandle CDXUTSelectionHandles::GetSelectionHandleAtPoint(POINT pt) {
	// Search through all selection handles for the first one which
	// contains the mouse point
	for (int i = TOP_LEFT; i < NUM_HANDLES; i++) {
		if (PtInRect(&m_handles[i], pt))
			return (SelectionHandle)i;
	}

	return NOT_FOUND;
}

//--------------------------------------------------------------------------------------
HRESULT CDXUTSelectionHandles::Render(int x, int y, int width, int height) {
	SetSelectionHandles(x, y, width, height);

	IDirect3DDevice9* pd3dDevice = DXUTGetGlobalDialogResourceManager()->GetD3DDevice();

	// Since we're doing our own drawing here we need to flush the sprites
	ID3DXSprite* sprite = DXUTGetGlobalDialogResourceManager()->GetSprite();
	if (sprite) {
		sprite->Flush();
	}
	IDirect3DVertexDeclaration9* pDecl = NULL;
	pd3dDevice->GetVertexDeclaration(&pDecl); // Preserve the sprite's current vertex decl
	pd3dDevice->SetFVF(DXUT_SCREEN_VERTEX::FVF);

	pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG2);
	pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG2);

	for (int j = 0; j < NUM_HANDLES; j++) {
		DXUT_SCREEN_VERTEX vertices[4] = {
			(float)m_handles[j].left - 0.5f,
			(float)m_handles[j].top - 0.5f,
			0.5f,
			1.0f,
			m_color,
			0,
			0,
			(float)m_handles[j].right - 0.5f,
			(float)m_handles[j].top - 0.5f,
			0.5f,
			1.0f,
			m_color,
			0,
			0,
			(float)m_handles[j].right - 0.5f,
			(float)m_handles[j].bottom - 0.5f,
			0.5f,
			1.0f,
			m_color,
			0,
			0,
			(float)m_handles[j].left - 0.5f,
			(float)m_handles[j].bottom - 0.5f,
			0.5f,
			1.0f,
			m_color,
			0,
			0,
		};

		pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN, 2, vertices, sizeof(DXUT_SCREEN_VERTEX));
	}

	pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);

	// Restore the vertex decl
	pd3dDevice->SetVertexDeclaration(pDecl);
	pDecl->Release();

	return S_OK;
}

//--------------------------------------------------------------------------------------
LPTSTR CDXUTSelectionHandles::GetResizingCursor(CDXUTSelectionHandles::SelectionHandle selectionHandle) {
	LPTSTR cursor = NULL;

	switch (selectionHandle) {
		case CDXUTSelectionHandles::TOP_LEFT:
		case CDXUTSelectionHandles::BOTTOM_RIGHT:
			cursor = IDC_SIZENWSE;
			break;

		case CDXUTSelectionHandles::TOP_MIDDLE:
		case CDXUTSelectionHandles::BOTTOM_MIDDLE:
			cursor = IDC_SIZENS;
			break;

		case CDXUTSelectionHandles::TOP_RIGHT:
		case CDXUTSelectionHandles::BOTTOM_LEFT:
			cursor = IDC_SIZENESW;
			break;

		case CDXUTSelectionHandles::LEFT_MIDDLE:
		case CDXUTSelectionHandles::RIGHT_MIDDLE:
			cursor = IDC_SIZEWE;
			break;
	}

	return cursor;
}

} // namespace KEP