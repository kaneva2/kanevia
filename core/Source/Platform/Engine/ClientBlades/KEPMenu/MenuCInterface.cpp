///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "dxstdafx.h"
#include "DXUTGui.h"
#include "DXUTFlashPlayer.h"
#include "KEPMenu.h"
#include "MenuCInterface.h"
#include "Common/KEPUtil/Helpers.h"
#include <fstream>
#include "LogHelper.h"
#include "..\KEPUtil\RefPtr.h"

static LogInstance("ClientEngine");

using namespace std;
using namespace KEP;

#define pKM KEPMenu::Instance()

#define FromBOOL(b) ((b) != FALSE)
#define ToBOOL(b) ((b) ? TRUE : FALSE)

// DRF - Safe Dialog Macros
static bool VALID_DIALOG(IMenuDialog* pDialog) {
	bool ok = (REF_PTR_VALID((CDXUTDialog*)pDialog) && !((CDXUTDialog*)pDialog)->m_bDelete && (((CDXUTDialog*)pDialog)->m_magicNumber == MN_CONSTRUCTED));
	if (!ok) {
		LogError("Dialog <" << pDialog << "> INVALID");
	}
	return ok;
}

#define VALID_DIALOG_FUNC_VOID(dialogFunc) \
	(VALID_DIALOG(pDialog) ? pDialog->dialogFunc : NULL)

#define FAILURE_BOOL FALSE
#define VALID_DIALOG_FUNC_BOOL(dialogFunc) \
	(VALID_DIALOG(pDialog) ? ToBOOL(pDialog->dialogFunc) : FAILURE_BOOL)

#define FAILURE_INT 0
#define VALID_DIALOG_FUNC_INT(dialogFunc) \
	(VALID_DIALOG(pDialog) ? pDialog->dialogFunc : FAILURE_INT)

#define FAILURE_HRESULT (~S_OK)
#define VALID_DIALOG_FUNC_HRESULT(dialogFunc) \
	(VALID_DIALOG(pDialog) ? pDialog->dialogFunc : FAILURE_HRESULT)

#define FAILURE_PTR NULL
#define VALID_DIALOG_FUNC_PTR(dialogFunc) \
	(VALID_DIALOG(pDialog) ? pDialog->dialogFunc : FAILURE_PTR)

#define FAILURE_COLOR ToCOLOR(0)
#define VALID_DIALOG_FUNC_COLOR(dialogFunc) \
	(VALID_DIALOG(pDialog) ? ToCOLOR(pDialog->dialogFunc) : FAILURE_COLOR)

#define FAILURE_STR ""
#define VALID_DIALOG_FUNC_STR(dialogFunc) \
	(VALID_DIALOG(pDialog) ? pDialog->dialogFunc : FAILURE_STR)

#ifdef __cplusplus
extern "C" {
#endif

BOOL Dialog_MsgBox(const char* msg) {
#ifdef _DEBUG
	HWND hWnd = DXUTGetHWND();
	::MessageBoxW(hWnd, Utf8ToUtf16(msg).c_str(), L"Script", MB_OK);
	return TRUE;
#else
	printf_s("Suppressed MsgBox, text: %s\n", msg);
	return FALSE;
#endif
}

int Dialog_NumOpen() {
	return (int)pKM->DialogsOpen();
}

BOOL Dialog_Log() {
	return ToBOOL(pKM->LogAllDialogs());
}

const char* Dialog_MenuId(IMenuDialog* pDialog) {
	return pKM->DialogMenuId(pDialog);
}

const char* Dialog_MenuName(IMenuDialog* pDialog) {
	return pKM->DialogMenuName(pDialog);
}

const char* Dialog_ScriptName(IMenuDialog* pDialog) {
	return pKM->DialogScriptName(pDialog);
}

IMenuDialog* Dialog_GetDialogByName(const char* menuName) {
	return menuName ? pKM->GetDialog(menuName) : FAILURE_PTR;
}
IMenuDialog* Menu_GetDialog(const char* menuName) {
	return menuName ? pKM->GetDialog(menuName) : FAILURE_PTR;
}

BOOL Dialog_IsDialogOpenByName(const char* menuName) {
	return menuName ? ToBOOL(pKM->GetDialog(menuName) != NULL) : FAILURE_BOOL;
}
bool Menu_IsOpen(const char* menuName) {
	return menuName ? pKM->MenuIsOpen(menuName) : false;
}

IMenuDialog* Dialog_New() {
	return pKM->DialogNew();
}

IMenuDialog* Dialog_CreateAs(const char* menuName, const char* menuId) {
	return (menuName && menuId) ? pKM->DialogCreateAs(menuName, menuId) : FAILURE_PTR;
}
bool Menu_OpenAs(const char* menuName, const char* menuId) {
	return (menuName && menuId) ? pKM->MenuOpenAs(menuName, menuId) : false;
}

IMenuDialog* Dialog_Create(const char* menuName) {
	return menuName ? pKM->DialogCreate(menuName) : FAILURE_PTR;
}
bool Menu_Open(const char* menuName) {
	return menuName ? pKM->MenuOpen(menuName) : false;
}

IMenuDialog* Dialog_CreateTrustedAs(const char* menuName, const char* menuId) {
	return (menuName && menuId) ? pKM->DialogCreateAs(menuName, menuId, false, true) : FAILURE_PTR;
}
bool Menu_OpenTrustedAs(const char* menuName, const char* menuId) {
	return (menuName && menuId) ? pKM->MenuOpenAs(menuName, menuId, false, true) : false;
}

IMenuDialog* Dialog_CreateTrusted(const char* menuName) {
	return menuName ? pKM->DialogCreate(menuName, false, true) : FAILURE_PTR;
}
bool Menu_OpenTrusted(const char* menuName) {
	return menuName ? pKM->MenuOpen(menuName, false, true) : false;
}

IMenuDialog* Dialog_CreateAndEdit(const char* menuName, BOOL edit) {
	return menuName ? pKM->DialogCreate(menuName, FromBOOL(edit)) : FAILURE_PTR;
}
bool Menu_OpenForEdit(const char* menuName) {
	return menuName ? pKM->MenuOpen(menuName, true) : false;
}

IMenuDialog* Dialog_CreateAtCoordinates(const char* menuName, int x, int y) {
	if (!menuName)
		return FAILURE_PTR; // DialogCreate() takes string
	IMenuDialog* pDialog = pKM->DialogCreate(menuName);
	if (pDialog)
		pDialog->SetLocationInScreen(x, y);
	return pDialog;
}
bool Menu_OpenAtCoordinates(const char* menuName, int x, int y) {
	return (Dialog_CreateAtCoordinates(menuName, x, y) != FAILURE_PTR);
}

BOOL Dialog_Destroy(IMenuDialog* pDialog) {
	return ToBOOL(pKM->DialogDestroy(pDialog));
}
bool Menu_Close(const char* menuName) {
	return menuName ? pKM->MenuClose(menuName) : false;
}

BOOL Dialog_DestroyAll(BOOL /*ok*/) {
	return ToBOOL(pKM->DialogDestroyAll());
}
bool Menu_CloseAll() {
	return pKM->MenuCloseAll();
}

BOOL Dialog_DestroyAllExcept(IMenuDialog* pDialog) {
	return ToBOOL(pKM->DialogDestroyAllExcept(pDialog));
}
bool Menu_CloseAllExcept(const char* menuName) {
	return menuName ? pKM->MenuCloseAllExcept(menuName) : false;
}

BOOL Dialog_DestroyAllNonEssentialExcept(IMenuDialog* pDialog, BOOL includeFramework) {
	return ToBOOL(pKM->DialogDestroyAllNonEssentialExcept(pDialog, includeFramework));
}
bool Menu_CloseAllNonEssentialExcept(const char* menuName, BOOL includeFramework) {
	return menuName ? pKM->MenuCloseAllNonEssentialExcept(menuName, includeFramework) : false;
}

BOOL Dialog_Save(IMenuDialog* pDialog, const char* fileName) {
	return fileName ? ToBOOL(pKM->DialogSave(pDialog, fileName)) : FAILURE_BOOL;
}
bool Menu_Save(const char* menuName, const char* fileName) {
	if (!fileName)
		return false;
	return menuName ? pKM->MenuSave(menuName, fileName) : false;
}

BOOL Dialog_CopyTemplate(IMenuDialog* pDialog) {
	return ToBOOL(pKM->DialogCopyTemplate(pDialog));
}
bool Menu_CopyTemplate(const char* menuName) {
	return menuName ? pKM->MenuCopyTemplate(menuName) : false;
}

BOOL Dialog_ApplyInputSettings(IMenuDialog* /*pDialog*/) {
	return ToBOOL(pKM->ApplyInputSettings());
}
bool Menu_ApplyInputSettings(const char* menuName) {
	return menuName ? pKM->MenuApplyInputSettings(menuName) : false;
}

BOOL Dialog_SendToBack(IMenuDialog* pDialog) {
	return ToBOOL(pKM->DialogSendToBack(pDialog));
}
bool Menu_SendToBack(const char* menuName) {
	return menuName ? pKM->MenuSendToBack(menuName) : false;
}

BOOL Dialog_BringToFront(IMenuDialog* pDialog) {
	return ToBOOL(pKM->DialogBringToFront(pDialog));
}
bool Menu_BringToFront(const char* menuName) {
	return menuName ? pKM->MenuBringToFront(menuName) : false;
}

BOOL Dialog_BringToFrontEnable(IMenuDialog* pDialog, BOOL enable) {
	return ToBOOL(pKM->DialogBringToFrontEnable(pDialog, FromBOOL(enable)));
}
bool Menu_BringToFrontEnable(const char* menuName, BOOL enable) {
	return menuName ? pKM->MenuBringToFrontEnable(menuName, FromBOOL(enable)) : false;
}

BOOL Dialog_AlwaysBringToFront(IMenuDialog* pDialog) {
	return ToBOOL(pKM->DialogAlwaysBringToFront(pDialog, true));
}
bool Menu_AlwaysBringToFront(const char* menuName) {
	return menuName ? pKM->MenuAlwaysBringToFront(menuName) : false;
}

BOOL Dialog_AlwaysBringToFrontDisable() {
	return ToBOOL(pKM->AlwaysBringDialogToFrontDisable());
}
bool Menu_AlwaysBringToFrontDisable() {
	return pKM->MenuAlwaysBringToFrontDisable();
}

BOOL Dialog_RegisterGenericEvents(IMenuDialog* pDialog) {
	return ToBOOL(pKM->DialogRegisterGenericEvents(pDialog));
}

BOOL Dialog_UnRegisterGenericEvents(IMenuDialog* pDialog) {
	return ToBOOL(pKM->DialogUnRegisterGenericEvents(pDialog));
}

int Dialog_GetId(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_INT(GetID());
}

IMenuControl* Dialog_GetControl(IMenuDialog* pDialog, const char* name) {
	return VALID_DIALOG_FUNC_PTR(GetControl(name));
}

IMenuControl* Dialog_GetControlByIndex(IMenuDialog* pDialog, int index) {
	if (!VALID_DIALOG(pDialog))
		return FAILURE_PTR;
	if (index < 0 || index >= pDialog->GetNumControls())
		return FAILURE_PTR;
	return pDialog->GetControlByIndex(index);
}

int Dialog_GetNumControls(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_INT(GetNumControls());
}

IMenuStatic* Dialog_GetStatic(IMenuDialog* pDialog, const char* name) {
	return VALID_DIALOG_FUNC_PTR(GetStatic(name));
}

IMenuImage* Dialog_GetImage(IMenuDialog* pDialog, const char* name) {
	return VALID_DIALOG_FUNC_PTR(GetImage(name));
}

IMenuButton* Dialog_GetButton(IMenuDialog* pDialog, const char* name) {
	return VALID_DIALOG_FUNC_PTR(GetButton(name));
}

IMenuCheckBox* Dialog_GetCheckBox(IMenuDialog* pDialog, const char* name) {
	return VALID_DIALOG_FUNC_PTR(GetCheckBox(name));
}

IMenuRadioButton* Dialog_GetRadioButton(IMenuDialog* pDialog, const char* name) {
	return VALID_DIALOG_FUNC_PTR(GetRadioButton(name));
}

IMenuComboBox* Dialog_GetComboBox(IMenuDialog* pDialog, const char* name) {
	return VALID_DIALOG_FUNC_PTR(GetComboBox(name));
}

IMenuSlider* Dialog_GetSlider(IMenuDialog* pDialog, const char* name) {
	return VALID_DIALOG_FUNC_PTR(GetSlider(name));
}

IMenuEditBox* Dialog_GetEditBox(IMenuDialog* pDialog, const char* name) {
	return VALID_DIALOG_FUNC_PTR(GetEditBox(name));
}

IMenuListBox* Dialog_GetListBox(IMenuDialog* pDialog, const char* name) {
	return VALID_DIALOG_FUNC_PTR(GetListBox(name));
}

BOOL Dialog_RemoveControl(IMenuDialog* pDialog, const char* controlName) {
	return VALID_DIALOG_FUNC_BOOL(RemoveControl(controlName));
}

BOOL Dialog_FocusDefaultControl(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_BOOL(FocusDefaultControl());
}

IMenuStatic* Dialog_AddStatic(IMenuDialog* pDialog, const char* name, const char* text, int x, int y, int width, int height) {
	if (!VALID_DIALOG(pDialog))
		return FAILURE_PTR;
	if (pDialog->AddStatic(name, text, x, y, width, height) != S_OK)
		return FAILURE_PTR;
	return pDialog->GetStatic(name);
}

IMenuImage* Dialog_AddImage(IMenuDialog* pDialog, const char* name, int x, int y, const char* textureName, int left, int top, int right, int bottom) {
	if (!VALID_DIALOG(pDialog))
		return FAILURE_PTR;
	RECT coords = { left, top, right, bottom };
	int width = abs(right - left);
	int height = abs(top - bottom);
	if (pDialog->AddImage(name, x, y, width, height, textureName, coords) != S_OK)
		return FAILURE_PTR;
	return pDialog->GetImage(name);
}

// Adds the image to the the front of the vector so that it will draw behind all elements in the dialog
IMenuImage* Dialog_AddImageToBack(IMenuDialog* pDialog, const char* name, int x, int y, const char* textureName, int left, int top, int right, int bottom) {
	if (!VALID_DIALOG(pDialog))
		return FAILURE_PTR;
	RECT coords = { left, top, right, bottom };
	int width = abs(right - left);
	int height = abs(top - bottom);
	if (pDialog->AddImageToBack(name, x, y, width, height, textureName, coords) != S_OK)
		return FAILURE_PTR;
	return pDialog->GetImage(name);
}

IMenuButton* Dialog_AddButton(IMenuDialog* pDialog, const char* name, const char* text, int x, int y, int width, int height) {
	if (!VALID_DIALOG(pDialog))
		return FAILURE_PTR;
	if (pDialog->AddButton(name, text, x, y, width, height) != S_OK)
		return FAILURE_PTR;
	return pDialog->GetButton(name);
}

IMenuCheckBox* Dialog_AddCheckBox(IMenuDialog* pDialog, const char* name, const char* text, int x, int y, int width, int height, BOOL bChecked) {
	if (!VALID_DIALOG(pDialog))
		return FAILURE_PTR;
	if (pDialog->AddCheckBox(name, text, x, y, width, height, FromBOOL(bChecked)) != S_OK)
		return FAILURE_PTR;
	return pDialog->GetCheckBox(name);
}

IMenuRadioButton* Dialog_AddRadioButton(IMenuDialog* pDialog, const char* name, unsigned int buttonGroup, const char* text, int x, int y, int width, int height, BOOL bChecked) {
	if (!VALID_DIALOG(pDialog))
		return FAILURE_PTR;
	if (pDialog->AddRadioButton(name, buttonGroup, text, x, y, width, height, FromBOOL(bChecked)) != S_OK)
		return FAILURE_PTR;
	return pDialog->GetRadioButton(name);
}

IMenuComboBox* Dialog_AddComboBox(IMenuDialog* pDialog, const char* name, int x, int y, int width, int height) {
	if (!VALID_DIALOG(pDialog))
		return FAILURE_PTR;
	if (pDialog->AddComboBox(name, x, y, width, height) != S_OK)
		return FAILURE_PTR;
	return pDialog->GetComboBox(name);
}

IMenuSlider* Dialog_AddSlider(IMenuDialog* pDialog, const char* name, int x, int y, int width, int height) {
	if (!VALID_DIALOG(pDialog))
		return FAILURE_PTR;
	if (pDialog->AddSlider(name, x, y, width, height) != S_OK)
		return FAILURE_PTR;
	return pDialog->GetSlider(name);
}

IMenuEditBox* Dialog_AddEditBox(IMenuDialog* pDialog, const char* name, const char* text, int x, int y, int width, int height) {
	if (!VALID_DIALOG(pDialog))
		return FAILURE_PTR;
	if (pDialog->AddEditBox(name, text, x, y, width, height, false) != S_OK)
		return FAILURE_PTR;
	return pDialog->GetEditBox(name);
}

IMenuEditBox* Dialog_AddSimpleEditBox(IMenuDialog* pDialog, const char* name, const char* text, int x, int y, int width, int height) {
	if (!VALID_DIALOG(pDialog))
		return FAILURE_PTR;
	if (pDialog->AddEditBox(name, text, x, y, width, height, true) != S_OK)
		return FAILURE_PTR;
	return pDialog->GetEditBox(name);
}

IMenuListBox* Dialog_AddListBox(IMenuDialog* pDialog, const char* name, int x, int y, int width, int height) {
	if (!VALID_DIALOG(pDialog))
		return FAILURE_PTR;
	if (pDialog->AddListBox(name, x, y, width, height) != S_OK)
		return FAILURE_PTR;
	return pDialog->GetListBox(name);
}

IFlashPlayerControl* Dialog_AddFlash(IMenuDialog* pDialog, const char* name, int x, int y, int width, int height) {
	if (!VALID_DIALOG(pDialog))
		return FAILURE_PTR;
	if (pDialog->AddFlash(name, x, y, width, height, true, true, false, "") != S_OK)
		return FAILURE_PTR;
	return pDialog->GetFlash(name);
}

BOOL Dialog_CopyControl(IMenuDialog* pDialog, IMenuControl* pControl) {
	if (!pControl || !pControl->GetName())
		return FAILURE_BOOL; // CopyControlToClipboard() takes string
	return ToBOOL(pKM->CopyControlToClipboard(pDialog, pControl->GetName()));
}

BOOL Dialog_PasteControl(IMenuDialog* pDialog) {
	return ToBOOL(pKM->PasteControlFromClipboard(pDialog, false, 0, 0));
}

// DRF - TODO - Should this be BOOL or should the rest be bool ?
bool Dialog_IsSomethingInClipboard() {
	return pKM->IsSomethingInClipboard();
}

IMenuElement* Dialog_GetDisplayElement(IMenuDialog* pDialog, const char* elementName) {
	if (!elementName)
		return FAILURE_PTR; // GetElement() takes string
	return VALID_DIALOG_FUNC_PTR(GetElement(elementName));
}

IMenuElement* Dialog_GetCaptionElement(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_PTR(GetCaptionElement());
}

IMenuElement* Dialog_GetBodyElement(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_PTR(GetBodyElement());
}

BOOL Dialog_GetElementTagUsesFontState(IMenuDialog* pDialog, const char* pElementTag, /*DXUT_CONTROL_STATE*/ unsigned int iState) {
	if (!VALID_DIALOG(pDialog) || !pElementTag)
		return FAILURE_BOOL; // GetElementTagUsesFont() takes string
	return ToBOOL((pDialog->GetElementTagUsesFont(pElementTag) & (1 << iState)) != 0);
}

BOOL Dialog_GetElementTagUsesTextureState(IMenuDialog* pDialog, const char* pElementTag, /*DXUT_CONTROL_STATE*/ unsigned int iState) {
	if (!VALID_DIALOG(pDialog) || !pElementTag)
		return FAILURE_BOOL; // GetElementTagUsesTexture() takes string
	return ToBOOL((pDialog->GetElementTagUsesTexture(pElementTag) & (1 << iState)) != 0);
}

BOOL Dialog_GetMinimized(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_BOOL(GetMinimized());
}

BOOL Dialog_GetEdit(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_BOOL(GetEdit());
}

BOOL Dialog_GetCaptionEnabled(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_BOOL(GetCaptionEnabled());
}

const char* Dialog_GetCaptionText(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_STR(GetCaptionText());
}

int Dialog_GetCaptionHeight(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_INT(GetCaptionHeight());
}

BOOL Dialog_GetCaptionShadow(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_BOOL(GetCaptionShadow());
}

COLOR Dialog_GetColorTopLeft(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_COLOR(GetColorTopLeft());
}

COLOR Dialog_GetColorTopRight(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_COLOR(GetColorTopRight());
}

COLOR Dialog_GetColorBottomLeft(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_COLOR(GetColorBottomLeft());
}

COLOR Dialog_GetColorBottomRight(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_COLOR(GetColorBottomRight());
}

int Dialog_GetCursorLocationX(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_INT(GetCursorLocationX());
}

int Dialog_GetCursorLocationY(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_INT(GetCursorLocationY());
}

int Dialog_GetLocationX(IMenuDialog* pDialog) {
	if (!VALID_DIALOG(pDialog))
		return -1; // failure value exception (legacy code)
	return VALID_DIALOG_FUNC_INT(GetLocationX());
}

int Dialog_GetLocationY(IMenuDialog* pDialog) {
	if (!VALID_DIALOG(pDialog))
		return -1; // failure value excpetion (legacy code)
	return VALID_DIALOG_FUNC_INT(GetLocationY());
}

int Dialog_GetWidth(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_INT(GetWidth());
}

int Dialog_GetHeight(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_INT(GetHeight());
}

int Dialog_GetScreenWidth(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_INT(GetScreenWidth());
}

int Dialog_GetScreenHeight(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_INT(GetScreenHeight());
}

BOOL Dialog_IsLMouseDown(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_BOOL(IsLMouseDown());
}

BOOL Dialog_IsRMouseDown(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_BOOL(IsRMouseDown());
}

BOOL Dialog_GetSnapToEdges(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_BOOL(GetSnapToEdges());
}

BOOL Dialog_GetModal(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_BOOL(GetModal());
}

BOOL Dialog_GetPassthrough(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_BOOL(GetPassthrough());
}

BOOL Dialog_GetMovable(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_BOOL(GetMovable());
}

BOOL Dialog_GetEnableMouseInput(IMenuDialog* pDialog) {
	if (!VALID_DIALOG(pDialog))
		return TRUE; // failure value exception (always enable mouse)
	return VALID_DIALOG_FUNC_BOOL(GetEnableMouseInput());
}

BOOL Dialog_GetEnableKeyboardInput(IMenuDialog* pDialog) {
	if (!VALID_DIALOG(pDialog))
		return TRUE; // failure value exception (always enable keyboard)
	return VALID_DIALOG_FUNC_BOOL(GetEnableKeyboardInput());
}

const char* Dialog_GetScriptName(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_STR(GetScriptFileNamePtr());
}

IMenuDialog* Dialog_GetNextDialog(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_PTR(GetNextDialog());
}

IMenuDialog* Dialog_GetPreviousDialog(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_PTR(GetPrevDialog());
}

unsigned int Dialog_GetLocationFormat(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_INT(GetLocationFormat());
}

int Dialog_GetNumTextures() {
	return pKM->GetNumTextures();
}

const char* Dialog_GetTextureByIndex(int index) {
	return pKM->GetTextureByIndex(index);
}

const char* Dialog_GetXmlName(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_STR(GetMenuFilePathPtr());
}

BOOL Dialog_SetMinimized(IMenuDialog* pDialog, BOOL minimized) {
	return VALID_DIALOG_FUNC_BOOL(SetMinimized(FromBOOL(minimized)));
}

BOOL Dialog_SetAllControls(IMenuDialog* pDialog, BOOL minimized, BOOL enabled) {
	return VALID_DIALOG_FUNC_BOOL(SetAllControls(FromBOOL(minimized), FromBOOL(enabled)));
}

BOOL Dialog_SetEdit(IMenuDialog* pDialog, BOOL editEnabled) {
	return VALID_DIALOG_FUNC_BOOL(SetEdit(FromBOOL(editEnabled)));
}

BOOL Dialog_SetBackgroundColor(IMenuDialog* pDialog, COLOR colorAllCorners) {
	return VALID_DIALOG_FUNC_BOOL(SetBackgroundColors(FromCOLOR(colorAllCorners)));
}

BOOL Dialog_SetBackgroundColorRaw(IMenuDialog* pDialog, RAWCOLOR colorAllCorners) {
	return VALID_DIALOG_FUNC_BOOL(SetBackgroundColors(colorAllCorners));
}

BOOL Dialog_SetBackgroundColors(IMenuDialog* pDialog, COLOR colorTopLeft, COLOR colorTopRight, COLOR colorBottomLeft, COLOR colorBottomRight) {
	return VALID_DIALOG_FUNC_BOOL(SetBackgroundColors(FromCOLOR(colorTopLeft), FromCOLOR(colorTopRight), FromCOLOR(colorBottomLeft), FromCOLOR(colorBottomRight)));
}

BOOL Dialog_SetBackgroundColorsRaw(IMenuDialog* pDialog, RAWCOLOR colorTopLeft, RAWCOLOR colorTopRight, RAWCOLOR colorBottomLeft, RAWCOLOR colorBottomRight) {
	return VALID_DIALOG_FUNC_BOOL(SetBackgroundColors(colorTopLeft, colorTopRight, colorBottomLeft, colorBottomRight));
}

BOOL Dialog_SetCaptionEnabled(IMenuDialog* pDialog, BOOL enable) {
	return VALID_DIALOG_FUNC_BOOL(EnableCaption(FromBOOL(enable)));
}

BOOL Dialog_SetCaptionHeight(IMenuDialog* pDialog, int height) {
	return VALID_DIALOG_FUNC_BOOL(SetCaptionHeight(height));
}

BOOL Dialog_SetCaptionShadow(IMenuDialog* pDialog, BOOL shadow) {
	return VALID_DIALOG_FUNC_BOOL(SetCaptionShadow(FromBOOL(shadow)));
}

BOOL Dialog_SetCaptionText(IMenuDialog* pDialog, const char* text) {
	return VALID_DIALOG_FUNC_BOOL(SetCaptionText(text));
}

BOOL Dialog_SetId(IMenuDialog* pDialog, int id) {
	return VALID_DIALOG_FUNC_BOOL(SetID(id));
}

BOOL Dialog_SetLocation(IMenuDialog* pDialog, int x, int y) {
	return VALID_DIALOG_FUNC_BOOL(SetLocation(x, y));
}

BOOL Dialog_SetLocationX(IMenuDialog* pDialog, int x) {
	return VALID_DIALOG_FUNC_BOOL(SetLocationX(x));
}

BOOL Dialog_SetLocationY(IMenuDialog* pDialog, int y) {
	return VALID_DIALOG_FUNC_BOOL(SetLocationY(y));
}

BOOL Dialog_SetWidth(IMenuDialog* pDialog, int width) {
	return VALID_DIALOG_FUNC_BOOL(SetWidth(width));
}

BOOL Dialog_SetHeight(IMenuDialog* pDialog, int height) {
	return VALID_DIALOG_FUNC_BOOL(SetHeight(height));
}

BOOL Dialog_SetSize(IMenuDialog* pDialog, int width, int height) {
	return VALID_DIALOG_FUNC_BOOL(SetSize(width, height));
}

BOOL Dialog_SetSnapToEdges(IMenuDialog* pDialog, BOOL snapToEdges) {
	return VALID_DIALOG_FUNC_BOOL(SetSnapToEdges(FromBOOL(snapToEdges)));
}

BOOL Dialog_SetModal(IMenuDialog* pDialog, BOOL modal) {
	BOOL ok = VALID_DIALOG_FUNC_BOOL(SetModal(FromBOOL(modal)));
	ok &= Dialog_ApplyInputSettings(pDialog);
	return ok;
}

BOOL Dialog_SetCaptureKeys(IMenuDialog* pDialog, BOOL capture) {
	return VALID_DIALOG_FUNC_BOOL(SetCaptureKeys(FromBOOL(capture)));
}

BOOL Dialog_SetPassthrough(IMenuDialog* pDialog, BOOL passthrough) {
	return VALID_DIALOG_FUNC_BOOL(SetPassthrough(FromBOOL(passthrough)));
}

BOOL Dialog_SetMovable(IMenuDialog* pDialog, BOOL movable) {
	return VALID_DIALOG_FUNC_BOOL(SetMovable(FromBOOL(movable)));
}

BOOL Dialog_SetKeyboardInputEnabled(IMenuDialog* pDialog, BOOL enable) {
	return VALID_DIALOG_FUNC_BOOL(EnableKeyboardInput(FromBOOL(enable)));
}

BOOL Dialog_SetMouseInputEnabled(IMenuDialog* pDialog, BOOL enable) {
	return VALID_DIALOG_FUNC_BOOL(EnableMouseInput(FromBOOL(enable)));
}

BOOL Dialog_SetCloseOnESC(IMenuDialog* pDialog, BOOL enable) {
	return VALID_DIALOG_FUNC_BOOL(SetCloseOnESC(FromBOOL(enable)));
}

BOOL Dialog_GetCloseOnESC(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_BOOL(GetCloseOnESC());
}

BOOL Dialog_SetNextDialog(IMenuDialog* pDialog, IMenuDialog* pNextDialog) {
	return VALID_DIALOG_FUNC_BOOL(SetNextDialog(pNextDialog));
}

BOOL Dialog_SetPreviousDialog(IMenuDialog* pDialog, IMenuDialog* pPrevDialog) {
	return VALID_DIALOG_FUNC_BOOL(SetPrevDialog(pPrevDialog));
}

BOOL Dialog_SetScriptName(IMenuDialog* pDialog, const char* scriptName) {
	if (!scriptName)
		return FAILURE_BOOL; // SetScriptFileName() takes a string
	return VALID_DIALOG_FUNC_BOOL(SetScriptFileName(scriptName));
}

BOOL Dialog_SetLocationFormat(IMenuDialog* pDialog, unsigned int format) {
	BOOL ok = VALID_DIALOG_FUNC_BOOL(SetLocationFormat(format));
	ok &= ToBOOL(pDialog->ApplyLocationFormatting());
	return ok;
}

BOOL Dialog_SetXmlName(IMenuDialog* pDialog, const char* filePath) {
	if (!filePath)
		return FAILURE_BOOL; // SetMenuFilePath() takes a string
	return VALID_DIALOG_FUNC_BOOL(SetMenuFilePath(filePath));
}

BOOL Dialog_SetEnabled(IMenuDialog* pDialog, BOOL enable) {
	return VALID_DIALOG_FUNC_BOOL(SetEnabled(FromBOOL(enable)));
}

BOOL Dialog_ClearFocus(IMenuDialog* pDialog) {
	return VALID_DIALOG_FUNC_BOOL(ClearFocus());
}

BOOL Dialog_GetBrowserPage(IMenuDialog* pDialog, const char* url, int filter, int startIndex, int maxItems) {
	return ToBOOL(pKM->GetBrowserPage(pDialog, url, filter, startIndex, maxItems));
}

BOOL Dialog_MakeWebCall(const char* url, int filter, int startIndex, int maxItems) {
	return ToBOOL(pKM->MakeWebCall(url, filter, startIndex, maxItems));
}

BOOL Dialog_MoveControlForward(IMenuDialog* pDialog, IMenuControl* pControl) {
	if (!pControl)
		return FAILURE_BOOL;
	return VALID_DIALOG_FUNC_BOOL(MoveControlForward(pControl->GetName()));
}

BOOL Dialog_MoveControlBackward(IMenuDialog* pDialog, IMenuControl* pControl) {
	if (!pControl)
		return FAILURE_BOOL;
	return VALID_DIALOG_FUNC_BOOL(MoveControlBackward(pControl->GetName()));
}

BOOL Dialog_MoveControlToFront(IMenuDialog* pDialog, IMenuControl* pControl) {
	if (!pControl)
		return FAILURE_BOOL;
	return VALID_DIALOG_FUNC_BOOL(MoveControlToFront(pControl->GetName()));
}

BOOL Dialog_MoveControlToBack(IMenuDialog* pDialog, IMenuControl* pControl) {
	if (!pControl)
		return FAILURE_BOOL;
	return VALID_DIALOG_FUNC_BOOL(MoveControlToBack(pControl->GetName()));
}

BOOL Dialog_MoveControlToIndex(IMenuDialog* pDialog, IMenuControl* pControl, unsigned int index) {
	if (!pControl)
		return FAILURE_BOOL;
	return VALID_DIALOG_FUNC_BOOL(MoveControlToIndex(pControl->GetName(), index));
}

BOOL Dialog_SetSelected(IMenuDialog* pDialog, BOOL selected) {
	return VALID_DIALOG_FUNC_BOOL(SetIsSelected(FromBOOL(selected)));
}

HRESULT Dialog_AddControl(IMenuDialog* pDialog, IMenuControl* pControl) {
	if (!pControl)
		return FAILURE_HRESULT;
	return VALID_DIALOG_FUNC_HRESULT(AddControl(pControl));
}

IMenuControl* Dialog_LoadControlByXml(IMenuDialog* pDialog, const char* xml) {
	return VALID_DIALOG_FUNC_PTR(LoadControlByXml(xml));
}

const char* Dialog_GetControlXml(IMenuDialog* pDialog, IMenuControl* pControl) {
	if (!pControl)
		return FAILURE_STR;
	return VALID_DIALOG_FUNC_STR(GetControlXml(pControl));
}

int Control_GetType(IMenuControl* pControl) {
	return pControl ? pControl->GetType() : FAILURE_INT;
}

const char* Control_GetComment(IMenuControl* pControl) {
	return pControl ? pControl->GetComment() : FAILURE_STR;
}

const char* Control_GetToolTip(IMenuControl* pControl) {
	return pControl ? pControl->GetToolTip() : FAILURE_STR;
}

int Control_GetGroupId(IMenuControl* pControl) {
	return pControl ? pControl->GetGroupId() : FAILURE_INT;
}

void BlendColor_Init(IMenuBlendColor* pBlendColor, COLOR defaultColor, COLOR disabledColor, COLOR hiddenColor) {
	if (pBlendColor)
		pBlendColor->Init(FromCOLOR(defaultColor), FromCOLOR(disabledColor), FromCOLOR(hiddenColor));
}

void BlendColor_InitRaw(IMenuBlendColor* pBlendColor, RAWCOLOR defaultColor, RAWCOLOR disabledColor, RAWCOLOR hiddenColor) {
	if (pBlendColor)
		pBlendColor->Init(defaultColor, disabledColor, hiddenColor);
}

COLOR BlendColor_GetColor(IMenuBlendColor* pBlendColor, int state) {
	return pBlendColor ? ToCOLOR(pBlendColor->GetColor((DXUT_CONTROL_STATE)state)) : ToCOLOR(0);
}

RAWCOLOR BlendColor_GetColorRaw(IMenuBlendColor* pBlendColor, int state) {
	return pBlendColor ? pBlendColor->GetColor((DXUT_CONTROL_STATE)state) : 0;
}

void BlendColor_SetColor(IMenuBlendColor* pBlendColor, int state, COLOR color) {
	if (pBlendColor)
		pBlendColor->SetColor((DXUT_CONTROL_STATE)state, FromCOLOR(color));
}

void BlendColor_SetColorRaw(IMenuBlendColor* pBlendColor, int state, RAWCOLOR color) {
	if (pBlendColor)
		pBlendColor->SetColor((DXUT_CONTROL_STATE)state, color);
}

IMenuBlendColor* Element_GetTextureColor(IMenuElement* pElement) {
	return pElement ? pElement->GetTextureColor() : NULL;
}

IMenuBlendColor* Element_GetFontColor(IMenuElement* pElement) {
	return pElement ? pElement->GetFontColor() : NULL;
}

const char* Element_GetFontFace(IMenuElement* pElement, IMenuDialog* pDialog) {
	return (pElement && pDialog) ? pElement->GetFont(pDialog)->GetFacePtr() : NULL;
}

int Element_GetFontHeight(IMenuElement* pElement, IMenuDialog* pDialog) {
	return (pElement && pDialog) ? pElement->GetFont(pDialog)->GetHeight() : 0;
}

int Element_GetFontWeight(IMenuElement* pElement, IMenuDialog* pDialog) {
	return (pElement && pDialog) ? pElement->GetFont(pDialog)->GetWeight() : 0;
}

BOOL Element_GetFontItalic(IMenuElement* pElement, IMenuDialog* pDialog) {
	return (pElement && pDialog) ? pElement->GetFont(pDialog)->GetItalic() : FALSE;
}

BOOL Element_GetFontUnderline(IMenuElement* pElement, IMenuDialog* pDialog) {
	return (pElement && pDialog) ? pElement->GetFont(pDialog)->GetUnderline() : FALSE;
}

const char* Element_GetTextureName(IMenuElement* pElement, IMenuDialog* pDialog) {
	return (pElement && pDialog) ? pElement->GetTextureNamePtr(pDialog) : NULL;
}

int Element_GetTextureWidth(IMenuElement* pElement, IMenuDialog* pDialog) {
	return (pElement && pDialog) ? pElement->GetTextureWidth(pDialog) : 0;
}

int Element_GetTextureHeight(IMenuElement* pElement, IMenuDialog* pDialog) {
	return (pElement && pDialog) ? pElement->GetTextureHeight(pDialog) : 0;
}

unsigned int Element_GetTextFormat(IMenuElement* pElement) {
	return pElement ? pElement->GetTextFormat() : 0;
}

RECT Element_GetCoords(IMenuElement* pElement) {
	static RECT rc = { 0, 0, 0, 0 };
	return pElement ? pElement->GetTextureCoords() : rc;
}

int Element_GetCoordLeft(IMenuElement* pElement) {
	return Element_GetCoords(pElement).left;
}

int Element_GetCoordRight(IMenuElement* pElement) {
	return Element_GetCoords(pElement).right;
}

int Element_GetCoordTop(IMenuElement* pElement) {
	return Element_GetCoords(pElement).top;
}

int Element_GetCoordBottom(IMenuElement* pElement) {
	return Element_GetCoords(pElement).bottom;
}

RECT Element_GetSlices(IMenuElement* pElement) {
	static RECT rc = { 0, 0, 0, 0 };
	return pElement ? pElement->GetTextureSlices() : rc;
}

int Element_GetSliceLeft(IMenuElement* pElement) {
	return Element_GetSlices(pElement).left;
}

int Element_GetSliceRight(IMenuElement* pElement) {
	return Element_GetSlices(pElement).right;
}

int Element_GetSliceTop(IMenuElement* pElement) {
	return Element_GetSlices(pElement).top;
}

int Element_GetSliceBottom(IMenuElement* pElement) {
	return Element_GetSlices(pElement).bottom;
}

void Element_SetCoords(IMenuElement* pElement, int left, int top, int right, int bottom) {
	if (!pElement)
		return;

	RECT coords = { left, top, right, bottom };
	pElement->SetTextureCoords(coords);
}

void Element_SetSlices(IMenuElement* pElement, int left, int top, int right, int bottom) {
	if (!pElement)
		return;

	RECT slices = { left, top, right, bottom };
	pElement->SetTextureSlices(slices);
}

void Element_SetTextFormat(IMenuElement* pElement, unsigned long textFormat) {
	if (pElement)
		pElement->SetTextFormat(textFormat);
}

void Element_AddTexture(IMenuElement* pElement, IMenuDialog* pDialog, const char* textureName) {
	if (!pElement || !pDialog)
		return;

	// DRF - Added Coords & Slices
	RECT texCoords = pElement->GetTextureCoords();
	RECT texSlices = pElement->GetTextureSlices();
	pElement->AddTexture(pDialog, textureName, texCoords, texSlices);
}

void Element_AddExternalTexture(IMenuElement* pElement, IMenuDialog* pDialog, const char* textureName, const char* progressTextureName) {
	if (!pElement || !pDialog || !textureName)
		return;

	pElement->AddExternalTexture(pDialog, textureName);
	if (progressTextureName)
		pElement->AddProgressTexture(pDialog, progressTextureName);
}

void Element_AddDynamicTexture(IMenuElement* pElement, IMenuDialog* pDialog, unsigned dynTextureId) {
	if (!pElement || !pDialog)
		return;

	pElement->AddDynamicTexture(pDialog, dynTextureId);
}

void Element_AddFont(IMenuElement* pElement, IMenuDialog* pDialog, const char* faceName, long height, long weight, BOOL italic) {
	if (!pElement || !pDialog)
		return;

	pElement->AddFont(pDialog, faceName, height, weight, FromBOOL(italic), false);
}

BOOL Control_SetPassthrough(IMenuControl* pControl, BOOL passthrough) {
	return pControl ? ToBOOL(pControl->SetPassthrough(FromBOOL(passthrough))) : FALSE;
}

IMenuElement* Control_GetDisplayElement(IMenuControl* pControl, const char* elementName) {
	return pControl ? pControl->GetElement(elementName) : NULL;
}

IMenuDialog* Control_GetDialog(IMenuControl* pControl) {
	return pControl ? pControl->GetDialog() : NULL;
}

const char* Control_GetName(IMenuControl* pControl) {
	return pControl ? pControl->GetName() : NULL;
}

BOOL Control_GetEnabled(IMenuControl* pControl) {
	return pControl ? ToBOOL(pControl->GetEnabled()) : FALSE;
}

BOOL Control_GetVisible(IMenuControl* pControl) {
	return pControl ? ToBOOL(pControl->GetVisible()) : FALSE;
}

BOOL Control_GetMouseOver(IMenuControl* pControl) {
	return pControl ? ToBOOL(pControl->GetMouseOver()) : FALSE;
}

BOOL Control_GetFocus(IMenuControl* pControl) {
	return pControl ? ToBOOL(pControl->GetIsInFocus()) : FALSE;
}

int Control_GetLocationX(IMenuControl* pControl) {
	if (!pControl)
		return -1;

	POINT pt;
	pControl->GetLocation(pt);
	return pt.x;
}

int Control_GetLocationY(IMenuControl* pControl) {
	if (!pControl)
		return -1;

	POINT pt;
	pControl->GetLocation(pt);
	return pt.y;
}

int Control_GetWidth(IMenuControl* pControl) {
	return pControl ? pControl->GetWidth() : 0;
}

int Control_GetHeight(IMenuControl* pControl) {
	return pControl ? pControl->GetHeight() : 0;
}

unsigned int Control_GetHotkey(IMenuControl* pControl) {
	return pControl ? pControl->GetHotkey() : 0;
}

tDATA Control_GetUserData(IMenuControl* pControl) {
	return pControl ? PtrToData(pControl->GetUserData()) : 0;
}

const char* Control_GetLinkClicked(IMenuControl* pControl) {
	return pControl ? pControl->GetLinkClicked() : NULL;
}

BOOL Control_GetElementTagUsesFont(IMenuControl* pControl, const char* pElementTag) {
	if (!pElementTag)
		return FALSE;
	return pControl ? ToBOOL(pControl->GetElementTagUsesFont(pElementTag) != 0) : FALSE;
}

BOOL Control_GetElementTagUsesFontState(IMenuControl* pControl, const char* pElementTag, unsigned int iState) {
	if (!pElementTag)
		return FALSE;
	DXUT_CONTROL_STATE s = (DXUT_CONTROL_STATE)iState;
	return pControl ? ToBOOL((pControl->GetElementTagUsesFont(pElementTag) & (1 << s)) != 0) : FALSE;
}

BOOL Control_GetElementTagUsesTexture(IMenuControl* pControl, const char* pElementTag) {
	if (!pElementTag)
		return FALSE;
	return pControl ? ToBOOL(pControl->GetElementTagUsesTexture(pElementTag) != 0) : FALSE;
}

BOOL Control_GetElementTagUsesTextureState(IMenuControl* pControl, const char* pElementTag, unsigned int iState) {
	if (!pElementTag)
		return FALSE;
	DXUT_CONTROL_STATE s = (DXUT_CONTROL_STATE)iState;
	return pControl ? ToBOOL((pControl->GetElementTagUsesTexture(pElementTag) & (1 << s)) != 0) : FALSE;
}

IMenuStatic* Control_GetStatic(IMenuControl* pControl) {
	return dynamic_cast<CDXUTStatic*>(pControl);
}

IMenuImage* Control_GetImage(IMenuControl* pControl) {
	return dynamic_cast<CDXUTImage*>(pControl);
}

IMenuButton* Control_GetButton(IMenuControl* pControl) {
	return dynamic_cast<CDXUTButton*>(pControl);
}

IMenuCheckBox* Control_GetCheckBox(IMenuControl* pControl) {
	return dynamic_cast<CDXUTCheckBox*>(pControl);
}

IMenuRadioButton* Control_GetRadioButton(IMenuControl* pControl) {
	return dynamic_cast<CDXUTRadioButton*>(pControl);
}

IMenuComboBox* Control_GetComboBox(IMenuControl* pControl) {
	return dynamic_cast<CDXUTComboBox*>(pControl);
}

IMenuSlider* Control_GetSlider(IMenuControl* pControl) {
	return dynamic_cast<CDXUTSlider*>(pControl);
}

IMenuEditBox* Control_GetEditBox(IMenuControl* pControl) {
	return dynamic_cast<CDXUTEditBox*>(pControl);
}

IMenuListBox* Control_GetListBox(IMenuControl* pControl) {
	return dynamic_cast<CDXUTListBox*>(pControl);
}

void Control_SetName(IMenuControl* pControl, const char* name) {
	if (pControl)
		pControl->SetName(name);
}

void Control_SetNameUnconflicting(IMenuControl* pControl, const char* newName) {
	if (!pControl)
		return;

	string name(newName);
	IMenuDialog* pDialog = pControl->GetDialog();
	int postfix = 0;
	bool bConflictFound = false;
	do {
		bConflictFound = false;
		for (int i = 0; i < pDialog->GetNumControls(); i++) {
			IMenuControl* pTmp = pDialog->GetControlByIndex(i);
			if (pControl != pTmp && name == pTmp->GetName()) {
				bConflictFound = true;
				postfix++;
				stringstream ss;
				ss << newName;
				ss << postfix;
				name = ss.str();
				break;
			}
		}
	} while (bConflictFound);

	pControl->SetName(name.c_str());
}

void Control_SetEnabled(IMenuControl* pControl, BOOL enable) {
	if (pControl)
		pControl->SetEnabled(FromBOOL(enable));
}

void Control_SetVisible(IMenuControl* pControl, BOOL visible) {
	if (pControl)
		pControl->SetVisible(FromBOOL(visible));
}

void Control_SetFocus(IMenuControl* pControl) {
	if (pControl)
		pControl->GetDialog()->RequestFocus(pControl);
}

void Control_SetLocation(IMenuControl* pControl, int x, int y) {
	if (pControl)
		pControl->SetLocation(x, y);
}

void Control_SetLocationX(IMenuControl* pControl, int x) {
	if (!pControl)
		return;

	POINT pt;
	pControl->GetLocation(pt);
	pControl->SetLocation(x, pt.y);
}

void Control_SetLocationY(IMenuControl* pControl, int y) {
	if (!pControl)
		return;

	POINT pt;
	pControl->GetLocation(pt);
	pControl->SetLocation(pt.x, y);
}

void Control_SetSize(IMenuControl* pControl, int width, int height) {
	if (pControl)
		pControl->SetSize(width, height);
}

void Control_SetHotkey(IMenuControl* pControl, unsigned int hotkey) {
	if (pControl)
		pControl->SetHotkey(hotkey);
}

void Control_SetUserData(IMenuControl* pControl, tDATA data) {
	void* pData = DataToPtr(data);
	if (pControl)
		pControl->SetUserData(pData);
}

BOOL Control_ContainsPoint(IMenuControl* pControl, int x, int y) {
	if (!pControl)
		return FALSE;

	POINT pt;
	pt.x = x;
	pt.y = y;
	return pControl->ContainsPoint(pt);
}

void Control_SetEditing(IMenuControl* pControl, BOOL enable) {
	if (!pControl)
		return;

	IMenuDialog* tempDialog = pControl->GetDialog();
	if (tempDialog)
		tempDialog->RequestSelection(pControl, FromBOOL(enable));
}

void Control_SetComment(IMenuControl* pControl, const char* text) {
	if (pControl)
		pControl->SetComment(text);
}

void Control_SetToolTip(IMenuControl* pControl, const char* text) {
	if (pControl)
		pControl->SetToolTip(text);
}

void Control_SetGroupId(IMenuControl* pControl, int groupId) {
	if (pControl)
		pControl->SetGroupId(groupId);
}

void Control_SetClipRect(IMenuControl* pControl, int left, int top, int right, int bottom) {
	if (pControl)
		pControl->SetClipRect(left, top, right, bottom);
}

void Static_SetText(IMenuStatic* pStatic, const char* text) {
	if (pStatic)
		pStatic->SetText(text);
}

int Static_GetStringWidth(IMenuStatic* pStatic, const char* text) {
	if (!pStatic)
		return 0;

	CDXUTStatic* pDXUTStatic = dynamic_cast<CDXUTStatic*>(pStatic);
	return pDXUTStatic ? pDXUTStatic->GetStringWidth(text) : 0;
}

int Static_GetStringHeight(IMenuStatic* pStatic, const char* text) {
	if (!pStatic)
		return 0;

	CDXUTStatic* pDXUTStatic = dynamic_cast<CDXUTStatic*>(pStatic);
	return pDXUTStatic ? pDXUTStatic->GetStringHeight(text) : 0;
}

void Static_GetStringDims(IMenuStatic* pStatic, const char* text, int& width, int& height) {
	width = height = 0;
	if (pStatic)
		return;

	CDXUTStatic* pDXUTStatic = dynamic_cast<CDXUTStatic*>(pStatic);
	if (pDXUTStatic)
		pDXUTStatic->GetStringDims(text, width, height);
}

void Static_SetShadow(IMenuStatic* pStatic, BOOL shadow) {
	if (pStatic)
		pStatic->SetShadow(FromBOOL(shadow));
}

IMenuControl* Static_GetControl(IMenuStatic* pStatic) {
	return dynamic_cast<CDXUTStatic*>(pStatic);
}

IMenuElement* Static_GetDisplayElement(IMenuStatic* pStatic) {
	return Control_GetDisplayElement(dynamic_cast<CDXUTStatic*>(pStatic), DISPLAY_TAG);
}

const char* Static_GetText(IMenuStatic* pStatic) {
	return pStatic ? pStatic->GetText() : NULL;
}

BOOL Static_GetShadow(IMenuStatic* pStatic) {
	return pStatic ? ToBOOL(pStatic->GetShadow()) : FALSE;
}

void Static_SetUseHTML(IMenuStatic* pStatic, BOOL useHTML) {
	if (pStatic)
		pStatic->SetUseHTML(FromBOOL(useHTML));
}

BOOL Static_GetUseHTML(IMenuStatic* pStatic) {
	return pStatic ? ToBOOL(pStatic->GetUseHTML()) : FALSE;
}

IMenuControl* Image_GetControl(IMenuImage* pImage) {
	return dynamic_cast<CDXUTImage*>(pImage);
}

IMenuElement* Image_GetDisplayElement(IMenuImage* pImage) {
	return Control_GetDisplayElement(Image_GetControl(pImage), DISPLAY_TAG);
}

void Image_SetColor(IMenuImage* pImage, COLOR textureColor) {
	if (pImage)
		pImage->SetColor(FromCOLOR(textureColor));
}

void Image_SetColorRaw(IMenuImage* pImage, RAWCOLOR textureColor) {
	if (pImage)
		pImage->SetColor(textureColor);
}

IMenuControl* Button_GetControl(IMenuButton* pButton) {
	return dynamic_cast<CDXUTButton*>(pButton);
}

IMenuStatic* Button_GetStatic(IMenuButton* pButton) {
	return dynamic_cast<CDXUTButton*>(pButton);
}

IMenuElement* Button_GetNormalDisplayElement(IMenuButton* pButton) {
	return Control_GetDisplayElement(Button_GetControl(pButton), NORMAL_DISPLAY_TAG);
}

IMenuElement* Button_GetMouseOverDisplayElement(IMenuButton* pButton) {
	return Control_GetDisplayElement(Button_GetControl(pButton), MOUSE_OVER_DISPLAY_TAG);
}

IMenuElement* Button_GetPressedDisplayElement(IMenuButton* pButton) {
	return Control_GetDisplayElement(Button_GetControl(pButton), PRESSED_DISPLAY_TAG);
}

IMenuElement* Button_GetFocusedDisplayElement(IMenuButton* pButton) {
	return Control_GetDisplayElement(Button_GetControl(pButton), FOCUSED_DISPLAY_TAG);
}

IMenuElement* Button_GetDisabledDisplayElement(IMenuButton* pButton) {
	return Control_GetDisplayElement(Button_GetControl(pButton), DISABLED_DISPLAY_TAG);
}

void Button_SetSimpleMode(IMenuButton* pButton, BOOL enabled) {
	if (pButton)
		pButton->SetSimpleMode(FromBOOL(enabled));
}

BOOL Button_GetSimpleMode(IMenuButton* pButton) {
	return pButton ? ToBOOL(pButton->GetSimpleMode()) : FALSE;
}

IMenuControl* CheckBox_GetControl(IMenuCheckBox* pCheckBox) {
	return dynamic_cast<CDXUTCheckBox*>(pCheckBox);
}

IMenuButton* CheckBox_GetButton(IMenuCheckBox* pCheckBox) {
	return dynamic_cast<CDXUTCheckBox*>(pCheckBox);
}

IMenuElement* CheckBox_GetBoxDisplayElement(IMenuCheckBox* pCheckBox) {
	return Control_GetDisplayElement(CheckBox_GetControl(pCheckBox), BOX_DISPLAY_TAG);
}

IMenuElement* CheckBox_GetCheckDisplayElement(IMenuCheckBox* pCheckBox) {
	return Control_GetDisplayElement(CheckBox_GetControl(pCheckBox), CHECK_DISPLAY_TAG);
}

BOOL CheckBox_GetChecked(IMenuCheckBox* pCheckBox) {
	return pCheckBox ? ToBOOL(pCheckBox->GetChecked()) : FALSE;
}

LONG CheckBox_GetIconWidth(IMenuCheckBox* pCheckBox) {
	return pCheckBox ? pCheckBox->GetIconWidth() : -1;
}

void CheckBox_SetChecked(IMenuCheckBox* pCheckBox, BOOL checked) {
	if (pCheckBox)
		pCheckBox->SetChecked(FromBOOL(checked));
}

void CheckBox_SetIconWidth(IMenuCheckBox* pCheckBox, LONG iconWidth) {
	if (pCheckBox)
		pCheckBox->SetIconWidth(iconWidth);
}

IMenuControl* RadioButton_GetControl(IMenuRadioButton* pRadioButton) {
	return dynamic_cast<CDXUTRadioButton*>(pRadioButton);
}

IMenuCheckBox* RadioButton_GetCheckBox(IMenuRadioButton* pRadioButton) {
	return dynamic_cast<CDXUTRadioButton*>(pRadioButton);
}

unsigned int RadioButton_GetButtonGroup(IMenuRadioButton* pRadioButton) {
	return pRadioButton ? pRadioButton->GetButtonGroup() : 0;
}

void RadioButton_SetChecked(IMenuRadioButton* pRadioButton, BOOL checked) {
	CDXUTRadioButton* pDXUTRadioButton = dynamic_cast<CDXUTRadioButton*>(pRadioButton);
	if (pDXUTRadioButton)
		pDXUTRadioButton->SetChecked(FromBOOL(checked));
}

void RadioButton_SetButtonGroup(IMenuRadioButton* pRadioButton, unsigned int group) {
	if (pRadioButton)
		pRadioButton->SetButtonGroup(group);
}

IMenuControl* ComboBox_GetControl(IMenuComboBox* pComboBox) {
	return dynamic_cast<CDXUTComboBox*>(pComboBox);
}

IMenuScrollBar* ComboBox_GetScrollBar(IMenuComboBox* pComboBox) {
	return pComboBox ? pComboBox->GetScrollBar() : NULL;
}

IMenuElement* ComboBox_GetMainDisplayElement(IMenuComboBox* pComboBox) {
	return Control_GetDisplayElement(ComboBox_GetControl(pComboBox), MAIN_DISPLAY_TAG);
}

IMenuElement* ComboBox_GetButtonDisplayElement(IMenuComboBox* pComboBox) {
	return Control_GetDisplayElement(ComboBox_GetControl(pComboBox), BUTTON_DISPLAY_TAG);
}

IMenuElement* ComboBox_GetDropDownDisplayElement(IMenuComboBox* pComboBox) {
	return Control_GetDisplayElement(ComboBox_GetControl(pComboBox), DROP_DOWN_DISPLAY_TAG);
}

IMenuElement* ComboBox_GetSelectionDisplayElement(IMenuComboBox* pComboBox) {
	return Control_GetDisplayElement(ComboBox_GetControl(pComboBox), SELECTION_DISPLAY_TAG);
}

int ComboBox_GetDropHeight(IMenuComboBox* pComboBox) {
	return pComboBox ? pComboBox->GetDropHeight() : 0;
}

int ComboBox_GetScrollBarWidth(IMenuComboBox* pComboBox) {
	return pComboBox ? pComboBox->GetScrollBarWidth() : 0;
}

void ComboBox_SetTextColor(IMenuComboBox* pComboBox, COLOR color) {
	if (pComboBox)
		pComboBox->SetTextColor(FromCOLOR(color));
}

void ComboBox_SetTextColorRaw(IMenuComboBox* pComboBox, RAWCOLOR color) {
	if (pComboBox)
		pComboBox->SetTextColor(color);
}

void ComboBox_SetDropHeight(IMenuComboBox* pComboBox, int height) {
	if (pComboBox)
		pComboBox->SetDropHeight(height);
}

void ComboBox_SetScrollBarWidth(IMenuComboBox* pComboBox, int width) {
	if (pComboBox)
		pComboBox->SetScrollBarWidth(width);
}

BOOL ComboBox_AddItem(IMenuComboBox* pComboBox, const char* text, tDATA data) {
	void* pData = DataToPtr(data);
	return pComboBox ? ToBOOL(pComboBox->AddItem(text, pData) == S_OK) : FALSE;
}

void ComboBox_RemoveAllItems(IMenuComboBox* pComboBox) {
	if (pComboBox)
		pComboBox->RemoveAllItems();
}

void ComboBox_RemoveItem(IMenuComboBox* pComboBox, unsigned int index) {
	if (pComboBox)
		pComboBox->RemoveItem(index);
}

BOOL ComboBox_ContainsItem(IMenuComboBox* pComboBox, const char* text) {
	return pComboBox ? ToBOOL(pComboBox->ContainsItem(text)) : FALSE;
}

int ComboBox_FindItem(IMenuComboBox* pComboBox, const char* text) {
	return pComboBox ? pComboBox->FindItem(text) : -1;
}

const char* ComboBox_GetItemText(IMenuComboBox* pComboBox, int index) {
	return pComboBox ? pComboBox->GetItemText(index) : NULL;
}

tDATA ComboBox_GetItemData(IMenuComboBox* pComboBox, const char* text) {
	return pComboBox ? PtrToData(pComboBox->GetItemData(text)) : 0;
}

tDATA ComboBox_GetItemDataByIndex(IMenuComboBox* pComboBox, unsigned int index) {
	return pComboBox ? PtrToData(pComboBox->GetItemData(index)) : 0;
}

tDATA ComboBox_GetSelectedData(IMenuComboBox* pComboBox) {
	return pComboBox ? PtrToData(pComboBox->GetSelectedData()) : 0;
}

const char* ComboBox_GetSelectedText(IMenuComboBox* pComboBox) {
	return pComboBox ? pComboBox->GetSelectedText() : 0;
}

int ComboBox_GetSelectedIndex(IMenuComboBox* pComboBox) {
	return pComboBox ? pComboBox->GetSelectedIndex() : -1;
}

unsigned int ComboBox_GetNumItems(IMenuComboBox* pComboBox) {
	return pComboBox ? pComboBox->GetNumItems() : 0;
}

BOOL ComboBox_SetSelectedByIndex(IMenuComboBox* pComboBox, unsigned int index) {
	return pComboBox ? ToBOOL(pComboBox->SetSelectedByIndex(index) == S_OK) : FALSE;
}

BOOL ComboBox_SetSelectedByText(IMenuComboBox* pComboBox, const char* text) {
	return pComboBox ? ToBOOL(pComboBox->SetSelectedByText(text) == S_OK) : FALSE;
}

BOOL ComboBox_SetSelectedByData(IMenuComboBox* pComboBox, tDATA data) {
	void* pData = DataToPtr(data);
	return pComboBox ? ToBOOL(pComboBox->SetSelectedByData(pData) == S_OK) : FALSE;
}

IMenuControl* Slider_GetControl(IMenuSlider* pSlider) {
	return dynamic_cast<CDXUTSlider*>(pSlider);
}

IMenuElement* Slider_GetTrackDisplayElement(IMenuSlider* pSlider) {
	return Control_GetDisplayElement(Slider_GetControl(pSlider), TRACK_DISPLAY_TAG);
}

IMenuElement* Slider_GetButtonDisplayElement(IMenuSlider* pSlider) {
	return Control_GetDisplayElement(Slider_GetControl(pSlider), BUTTON_DISPLAY_TAG);
}

int Slider_GetValue(IMenuSlider* pSlider) {
	return pSlider ? pSlider->GetValue() : 0;
}

int Slider_GetRangeMin(IMenuSlider* pSlider) {
	return pSlider ? pSlider->GetRangeMin() : 0;
}

int Slider_GetRangeMax(IMenuSlider* pSlider) {
	return pSlider ? pSlider->GetRangeMax() : 0;
}

void Slider_SetValue(IMenuSlider* pSlider, int value) {
	if (pSlider)
		pSlider->SetValue(value);
}

void Slider_SetRange(IMenuSlider* pSlider, int min, int max) {
	if (pSlider)
		pSlider->SetRange(min, max);
}

IMenuControl* EditBox_GetControl(IMenuEditBox* pEditBox) {
	return dynamic_cast<CDXUTEditBox*>(pEditBox);
}

IMenuScrollBar* EditBox_GetScrollBar(IMenuEditBox* pEditBox) {
	return pEditBox ? pEditBox->GetScrollBar() : NULL;
}

int EditBox_GetScrollBarWidth(IMenuEditBox* pEditBox) {
	return pEditBox ? pEditBox->GetScrollBarWidth() : 0;
}

IMenuElement* EditBox_GetTextAreaDisplayElement(IMenuEditBox* pEditBox) {
	return Control_GetDisplayElement(EditBox_GetControl(pEditBox), TEXT_AREA_DISPLAY_TAG);
}

IMenuElement* EditBox_GetTopLeftBorderDisplayElement(IMenuEditBox* pEditBox) {
	return Control_GetDisplayElement(EditBox_GetControl(pEditBox), TOP_LEFT_BORDER_DISPLAY_TAG);
}

IMenuElement* EditBox_GetTopBorderDisplayElement(IMenuEditBox* pEditBox) {
	return Control_GetDisplayElement(EditBox_GetControl(pEditBox), TOP_BORDER_DISPLAY_TAG);
}

IMenuElement* EditBox_GetTopRightBorderDisplayElement(IMenuEditBox* pEditBox) {
	return Control_GetDisplayElement(EditBox_GetControl(pEditBox), TOP_RIGHT_BORDER_DISPLAY_TAG);
}

IMenuElement* EditBox_GetLeftBorderDisplayElement(IMenuEditBox* pEditBox) {
	return Control_GetDisplayElement(EditBox_GetControl(pEditBox), LEFT_BORDER_DISPLAY_TAG);
}

IMenuElement* EditBox_GetRightBorderDisplayElement(IMenuEditBox* pEditBox) {
	return Control_GetDisplayElement(EditBox_GetControl(pEditBox), RIGHT_BORDER_DISPLAY_TAG);
}

IMenuElement* EditBox_GetLowerLeftBorderDisplayElement(IMenuEditBox* pEditBox) {
	return Control_GetDisplayElement(EditBox_GetControl(pEditBox), LOWER_LEFT_BORDER_DISPLAY_TAG);
}

IMenuElement* EditBox_GetLowerBorderDisplayElement(IMenuEditBox* pEditBox) {
	return Control_GetDisplayElement(EditBox_GetControl(pEditBox), LOWER_BORDER_DISPLAY_TAG);
}

IMenuElement* EditBox_GetLowerRightBorderDisplayElement(IMenuEditBox* pEditBox) {
	return Control_GetDisplayElement(EditBox_GetControl(pEditBox), LOWER_RIGHT_BORDER_DISPLAY_TAG);
}

int EditBox_GetFont(IMenuEditBox* pEditBox) {
	if (!pEditBox)
		return 0;

	IMenuControl* pControl = EditBox_GetControl(pEditBox);
	if (!pControl)
		return 0;

	CDXUTElement* pDXUTElement = dynamic_cast<CDXUTElement*>(pControl->GetElement(0));
	if (!pDXUTElement)
		return 0;

	return pDXUTElement->GetFontIndex();
}

int EditBox_GetTextLength(IMenuEditBox* pEditBox) {
	return pEditBox ? pEditBox->GetTextLength() : 0;
}

COLOR EditBox_GetTextColor(IMenuEditBox* pEditBox) {
	return pEditBox ? ToCOLOR(pEditBox->GetTextColor()) : ToCOLOR(0);
}

RAWCOLOR EditBox_GetTextColorRaw(IMenuEditBox* pEditBox) {
	return pEditBox ? pEditBox->GetTextColor() : 0;
}

COLOR EditBox_GetSelectedTextColor(IMenuEditBox* pEditBox) {
	return pEditBox ? ToCOLOR(pEditBox->GetSelectedTextColor()) : ToCOLOR(0);
}

RAWCOLOR EditBox_GetSelectedTextColorRaw(IMenuEditBox* pEditBox) {
	return pEditBox ? pEditBox->GetSelectedTextColor() : 0;
}

COLOR EditBox_GetSelectedBackColor(IMenuEditBox* pEditBox) {
	return pEditBox ? ToCOLOR(pEditBox->GetSelectedBackColor()) : ToCOLOR(0);
}

RAWCOLOR EditBox_GetSelectedBackColorRaw(IMenuEditBox* pEditBox) {
	return pEditBox ? pEditBox->GetSelectedBackColor() : 0;
}

COLOR EditBox_GetCaretColor(IMenuEditBox* pEditBox) {
	return pEditBox ? ToCOLOR(pEditBox->GetCaretColor()) : ToCOLOR(0);
}

RAWCOLOR EditBox_GetCaretColorRaw(IMenuEditBox* pEditBox) {
	return pEditBox ? pEditBox->GetCaretColor() : 0;
}

const char* EditBox_GetText(IMenuEditBox* pEditBox) {
	return pEditBox ? (pEditBox->GetPassword() ? pEditBox->GetTextOrig() : pEditBox->GetText()) : NULL;
}

int EditBox_GetBorderWidth(IMenuEditBox* pEditBox) {
	return pEditBox ? pEditBox->GetBorderWidth() : 0;
}

int EditBox_GetSpacing(IMenuEditBox* pEditBox) {
	return pEditBox ? pEditBox->GetSpacing() : 0;
}

BOOL EditBox_GetEditable(IMenuEditBox* pEditBox) {
	return pEditBox ? (ToBOOL(pEditBox->GetEditable())) : FALSE;
}

BOOL EditBox_GetPassword(IMenuEditBox* pEditBox) {
	return pEditBox ? (ToBOOL(pEditBox->GetPassword())) : FALSE;
}

BOOL EditBox_GetMultiline(IMenuEditBox* pEditBox) {
	return pEditBox ? ToBOOL(pEditBox->IsMultiline()) : FALSE;
}

void EditBox_SetFont(IMenuEditBox* pEditBox, int fontindex) {
	if (!pEditBox)
		return;

	IMenuControl* pControl = EditBox_GetControl(pEditBox);
	if (!pControl)
		return;

	CDXUTElement* pElement = dynamic_cast<CDXUTElement*>(pControl->GetElement(0));
	if (!pElement)
		return;

	pElement->SetFontIndex(fontindex);
}

void EditBox_SetText(IMenuEditBox* pEditBox, const char* text, BOOL selected) {
	if (pEditBox)
		pEditBox->SetText(text, FromBOOL(selected));
}

void EditBox_SetTextColor(IMenuEditBox* pEditBox, COLOR color) {
	if (pEditBox)
		pEditBox->SetTextColor(FromCOLOR(color));
}

void EditBox_SetTextColorRaw(IMenuEditBox* pEditBox, RAWCOLOR color) {
	if (pEditBox)
		pEditBox->SetTextColor(color);
}

bool EditBox_SetSelected(IMenuEditBox* pEditBox, int startIndex, int endIndex) {
	return (pEditBox ? pEditBox->SetSelected(startIndex, endIndex) : false);
}

bool EditBox_SetSelectedAll(IMenuEditBox* pEditBox) {
	return (pEditBox ? pEditBox->SetSelected() : false);
}

void EditBox_SetSelectedTextColor(IMenuEditBox* pEditBox, COLOR color) {
	if (pEditBox)
		pEditBox->SetSelectedTextColor(FromCOLOR(color));
}

void EditBox_SetSelectedTextColorRaw(IMenuEditBox* pEditBox, RAWCOLOR color) {
	if (pEditBox)
		pEditBox->SetSelectedTextColor(color);
}

void EditBox_SetSelectedBackColor(IMenuEditBox* pEditBox, COLOR color) {
	if (pEditBox)
		pEditBox->SetSelectedBackColor(FromCOLOR(color));
}

void EditBox_SetSelectedBackColorRaw(IMenuEditBox* pEditBox, RAWCOLOR color) {
	if (pEditBox)
		pEditBox->SetSelectedBackColor(color);
}

void EditBox_SetCaretColor(IMenuEditBox* pEditBox, COLOR color) {
	if (pEditBox)
		pEditBox->SetCaretColor(FromCOLOR(color));
}

void EditBox_SetCaretColorRaw(IMenuEditBox* pEditBox, RAWCOLOR color) {
	if (pEditBox)
		pEditBox->SetCaretColor(color);
}

void EditBox_SetBorderWidth(IMenuEditBox* pEditBox, int width) {
	if (pEditBox)
		pEditBox->SetBorderWidth(width);
}

void EditBox_SetSpacing(IMenuEditBox* pEditBox, int spacing) {
	if (pEditBox)
		pEditBox->SetSpacing(spacing);
}

void EditBox_SetEditable(IMenuEditBox* pEditBox, BOOL editable) {
	if (pEditBox)
		pEditBox->SetEditable(FromBOOL(editable));
}

void EditBox_SetPassword(IMenuEditBox* pEditBox, BOOL password) {
	if (pEditBox)
		pEditBox->SetPassword(FromBOOL(password));
}

void EditBox_ClearText(IMenuEditBox* pEditBox) {
	if (pEditBox)
		pEditBox->ClearText();
}

void EditBox_SetMaxCharCount(IMenuEditBox* pEditBox, int maxCount) {
	if (pEditBox)
		pEditBox->SetMaxCharCount(maxCount > 0 ? maxCount : 0);
}

// DRF - DEPRECATED - Automatically Multi-line If More Then 1 Line
void EditBox_SetMultiline(IMenuEditBox* pEditBox, BOOL multiline) {
	return;
}

IMenuControl* ListBox_GetControl(IMenuListBox* pListBox) {
	return dynamic_cast<CDXUTListBox*>(pListBox);
}

IMenuScrollBar* ListBox_GetScrollBar(IMenuListBox* pListBox) {
	return pListBox ? pListBox->GetScrollBar() : NULL;
}

int ListBox_GetScrollBarWidth(IMenuListBox* pListBox) {
	return pListBox ? pListBox->GetScrollBarWidth() : 0;
}

IMenuElement* ListBox_GetMainDisplayElement(IMenuListBox* pListBox) {
	return Control_GetDisplayElement(ListBox_GetControl(pListBox), MAIN_DISPLAY_TAG);
}

IMenuElement* ListBox_GetSelectionDisplayElement(IMenuListBox* pListBox) {
	return Control_GetDisplayElement(ListBox_GetControl(pListBox), SELECTION_DISPLAY_TAG);
}

BOOL ListBox_GetMultiSelection(IMenuListBox* pListBox) {
	return pListBox ? ToBOOL(pListBox->GetStyle() == CDXUTListBox::MULTISELECTION) : FALSE;
}

BOOL ListBox_GetDragSelection(IMenuListBox* pListBox) {
	return pListBox ? ToBOOL(pListBox->GetDragSelection()) : FALSE;
}

int ListBox_GetBorder(IMenuListBox* pListBox) {
	return pListBox ? pListBox->GetBorder() : 0;
}

int ListBox_GetLeftMargin(IMenuListBox* pListBox) {
	return pListBox ? pListBox->GetLeftMargin() : 0;
}

int ListBox_GetRightMargin(IMenuListBox* pListBox) {
	return pListBox ? pListBox->GetRightMargin() : 0;
}

const char* ListBox_GetItemText(IMenuListBox* pListBox, int index) {
	return pListBox ? pListBox->GetItemText(index) : NULL;
}

const char* ListBox_GetSelectedItemText(IMenuListBox* pListBox) {
	return pListBox ? pListBox->GetSelectedItemText() : NULL;
}

tDATA ListBox_GetItemData(IMenuListBox* pListBox, int index) {
	return pListBox ? PtrToData(pListBox->GetItemData(index)) : 0;
}

tDATA ListBox_GetSelectedItemData(IMenuListBox* pListBox) {
	return pListBox ? PtrToData(pListBox->GetSelectedItemData()) : 0;
}

int ListBox_GetSelectedItemIndex(IMenuListBox* pListBox) {
	return pListBox ? pListBox->GetSelectedIndex() : -1;
}

int ListBox_GetMultiSelectedItemIndex(IMenuListBox* pListBox, int index) {
	return pListBox ? pListBox->GetSelectedIndex(index) : -1;
}

int ListBox_GetRowHeight(IMenuListBox* pListBox) {
	return pListBox ? pListBox->GetRowHeight() : -1;
}

int ListBox_GetSize(IMenuListBox* pListBox) {
	return pListBox ? pListBox->GetSize() : -1;
}

void ListBox_SetMultiSelection(IMenuListBox* pListBox, BOOL multiSelection) {
	if (pListBox)
		pListBox->SetStyle(FromBOOL(multiSelection) ? CDXUTListBox::MULTISELECTION : 0);
}

void ListBox_SetHTMLEnabled(IMenuListBox* pListBox, BOOL htmlsupport) {
	if (pListBox)
		pListBox->SetHTMLEnabled(FromBOOL(htmlsupport));
}

void ListBox_SetDragSelection(IMenuListBox* pListBox, BOOL dragSelection) {
	if (pListBox)
		pListBox->SetDragSelection(FromBOOL(dragSelection));
}

void ListBox_SetScrollBarWidth(IMenuListBox* pListBox, int width) {
	if (pListBox)
		pListBox->SetScrollBarWidth(width);
}

void ListBox_SetBorder(IMenuListBox* pListBox, int border) {
	if (pListBox)
		pListBox->SetBorder(border);
}

void ListBox_SetMargins(IMenuListBox* pListBox, int margin) {
	if (pListBox)
		pListBox->SetMargins(margin);
}

void ListBox_SetLeftMargin(IMenuListBox* pListBox, int margin) {
	if (pListBox)
		pListBox->SetLeftMargin(margin);
}

void ListBox_SetRightMargin(IMenuListBox* pListBox, int margin) {
	if (pListBox)
		pListBox->SetRightMargin(margin);
}

void ListBox_SetRowHeight(IMenuListBox* pListBox, int height) {
	if (pListBox)
		pListBox->SetRowHeight(height);
}

void ListBox_SetItemText(IMenuListBox* pListBox, int index, const char* text) {
	if (pListBox && text)
		pListBox->SetItemText(index, text);
}

BOOL ListBox_AddItem(IMenuListBox* pListBox, const char* text, tDATA data) {
	void* pData = DataToPtr(data);
	return pListBox ? ToBOOL(pListBox->AddItem(text, pData) == S_OK) : FALSE;
}

BOOL ListBox_InsertItem(IMenuListBox* pListBox, int index, const char* text, tDATA data) {
	void* pData = DataToPtr(data);
	return pListBox ? ToBOOL(pListBox->InsertItem(index, text, pData) == S_OK) : FALSE;
}

void ListBox_RemoveAllItems(IMenuListBox* pListBox) {
	if (pListBox)
		pListBox->RemoveAllItems();
}

void ListBox_RemoveItem(IMenuListBox* pListBox, unsigned int index) {
	if (pListBox)
		pListBox->RemoveItem(index);
}

void ListBox_SelectItem(IMenuListBox* pListBox, int index) {
	if (pListBox)
		pListBox->SelectItem(index);
}

int ListBox_FindItem(IMenuListBox* pListBox, tDATA data) {
	void* pData = DataToPtr(data);
	return pListBox ? pListBox->FindItem(pData) : -1;
}

void ListBox_ClearSelection(IMenuListBox* pListBox) {
	if (pListBox)
		pListBox->ClearSelection();
}

IMenuControl* ScrollBar_GetControl(IMenuScrollBar* pScrollBar) {
	return dynamic_cast<CDXUTScrollBar*>(pScrollBar);
}

int ScrollBar_GetTrackStart(IMenuScrollBar* pScrollBar) {
	return pScrollBar ? pScrollBar->GetTrackStart() : -1;
}

int ScrollBar_GetTrackEnd(IMenuScrollBar* pScrollBar) {
	return pScrollBar ? pScrollBar->GetTrackEnd() : -1;
}

int ScrollBar_GetTrackPos(IMenuScrollBar* pScrollBar) {
	return pScrollBar ? pScrollBar->GetTrackPos() : -1;
}

int ScrollBar_GetPageSize(IMenuScrollBar* pScrollBar) {
	return pScrollBar ? pScrollBar->GetPageSize() : -1;
}

BOOL ScrollBar_GetShowThumb(IMenuScrollBar* pScrollBar) {
	return pScrollBar ? ToBOOL(pScrollBar->GetShowThumb()) : FALSE;
}

IMenuElement* ScrollBar_GetTrackDisplayElement(IMenuScrollBar* pScrollBar) {
	return Control_GetDisplayElement(ScrollBar_GetControl(pScrollBar), TRACK_DISPLAY_TAG);
}

IMenuElement* ScrollBar_GetUpArrowDisplayElement(IMenuScrollBar* pScrollBar) {
	return Control_GetDisplayElement(ScrollBar_GetControl(pScrollBar), UP_ARROW_DISPLAY_TAG);
}

IMenuElement* ScrollBar_GetDownArrowDisplayElement(IMenuScrollBar* pScrollBar) {
	return Control_GetDisplayElement(ScrollBar_GetControl(pScrollBar), DOWN_ARROW_DISPLAY_TAG);
}

IMenuElement* ScrollBar_GetButtonDisplayElement(IMenuScrollBar* pScrollBar) {
	return Control_GetDisplayElement(ScrollBar_GetControl(pScrollBar), BUTTON_DISPLAY_TAG);
}

IMenuControl* ScrollBar_GetParent(IMenuScrollBar* pScrollBar) {
	return pScrollBar ? pScrollBar->GetParent() : NULL;
}

void ScrollBar_SetTrackRange(IMenuScrollBar* pScrollBar, int nStart, int nEnd) {
	if (pScrollBar)
		pScrollBar->SetTrackRange(nStart, nEnd);
}

void ScrollBar_SetTrackPos(IMenuScrollBar* pScrollBar, int nPosition) {
	if (pScrollBar)
		pScrollBar->SetTrackPos(nPosition);
}

void ScrollBar_SetPageSize(IMenuScrollBar* pScrollBar, int nPageSize) {
	if (pScrollBar)
		pScrollBar->SetPageSize(nPageSize);
}

BOOL Cursor_SetEnabled(BOOL enable) {
	LogInfo(LogBool(FromBOOL(enable)));
	::SetCursorEnabled(FromBOOL(enable));
	return TRUE;
}

int Cursor_GetLocationX() {
	MsgProcInfo mpi;
	return mpi.CursorPoint().x;
}

int Cursor_GetLocationY() {
	MsgProcInfo mpi;
	return mpi.CursorPoint().y;
}

#ifdef __cplusplus
}
#endif
