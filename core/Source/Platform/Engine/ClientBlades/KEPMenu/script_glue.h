///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "function_signature.h"
#include "WeakRef.h"

#ifdef _DEBUG
#include <windows.h>
#endif

#include <string>
#include <vector>
#include <log4cplus/logger.h>

#include "..\KEPUtil\RefPtr.h"

#ifndef ASSERT
#ifdef _DEBUG
#define ASSERT(x)          \
	{                      \
		if (!x) {          \
			_asm { int 3 } \
		}                  \
	}
#else
#define ASSERT(x) \
	{}
#endif
#endif

namespace script_glue {

///////////////////////////////////////////////////////////
// a few base classes
///////////////////////////////////////////////////////////

struct script_base_registration {
};

typedef std::vector<script_base_registration *> registration_vector;

/////////////////////////////////////////////////////////////////
// generic template functions for creation/destruction/utility
/////////////////////////////////////////////////////////////////

template <typename T>
T *create_vm() {
	// in case we want to support other virtual machines
	ASSERT(0);
	return 0;
};

template <typename T>
void destroy_vm(T *t) {
	ASSERT(0);
	return 0;
};

template <typename T>
bool load_file(T *t, const std::string &filename) {
	ASSERT(0);
	return 0;
}

template <typename T>
bool call_function(T *t, const std::string &fnName) {
	ASSERT(0);
	return 0;
}

/////////////////////////////////////////////////////////////////////////
// generic script template functions to get data off the vm stack
/////////////////////////////////////////////////////////////////////////

//template <typename T, typename S> T get_parameter(S *L, int pos)
//{
//	// default implementation
//#ifdef _DEBUG
//	OutputDebugStringA("get_parameter called on unknown type\n");
//#endif
//	ASSERT(0);
//}

/////////////////////////////////////////////////////////////////////////
// generic script template functions to push data to the vm stack
/////////////////////////////////////////////////////////////////////////

template <typename T, typename S>
void push_parameter(S * /*L*/, T /*t*/) {
	// default implementation
#ifdef _DEBUG
	OutputDebugStringA("push_parameter called on unknown type\n");
#endif
	ASSERT(false);
}

} // namespace script_glue

#ifdef LUA
#include "lua_script_glue.h"
#endif
#ifdef PYTHON
#include "python_script_glue.h"
#endif

namespace script_glue {

// base template definition
template <
	typename script_vm_type,
	typename out_val_type,
	typename in_val_type1 = null_parameter,
	typename in_val_type2 = null_parameter,
	typename in_val_type3 = null_parameter,
	typename in_val_type4 = null_parameter,
	typename in_val_type5 = null_parameter,
	typename in_val_type6 = null_parameter,
	typename in_val_type7 = null_parameter,
	typename in_val_type8 = null_parameter,
	typename in_val_type9 = null_parameter,
	typename in_val_type10 = null_parameter>
class register_native_function : public script_base_registration {
};

} // namespace script_glue

// specialize it for a non-void return

#undef SCRIPT_GLUE_VOID_RET
#ifdef LUA
#include "lua_register_function.h"
#endif
#ifdef PYTHON
#include "python_register_function.h"
#endif
// specialize it for a void return

#define SCRIPT_GLUE_VOID_RET
#ifdef LUA
#include "lua_register_function.h"
#endif
#ifdef PYTHON
#include "python_register_function.h"
#endif

#undef SCRIPT_GLUE_VOID_RET
