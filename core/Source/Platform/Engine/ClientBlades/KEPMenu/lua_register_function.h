///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "LuaHelper.h"

namespace script_glue {

/////////////////////////////////////////////////////////////////
// lua specialization script registry to glue it all together
/////////////////////////////////////////////////////////////////

#ifdef SCRIPT_GLUE_VOID_RET
#define out_val_type void
#endif

template <
#ifndef SCRIPT_GLUE_VOID_RET
	typename out_val_type,
#endif
	typename in_val_type1,
	typename in_val_type2,
	typename in_val_type3,
	typename in_val_type4,
	typename in_val_type5,
	typename in_val_type6,
	typename in_val_type7,
	typename in_val_type8,
	typename in_val_type9,
	typename in_val_type10>
class register_native_function<
	lua_State,
	out_val_type,
	in_val_type1,
	in_val_type2,
	in_val_type3,
	in_val_type4,
	in_val_type5,
	in_val_type6,
	in_val_type7,
	in_val_type8,
	in_val_type9,
	in_val_type10> : public script_base_registration {
public:
	static int main_entry(lua_State *L) {
		// this is the entry point from lua
		//  pop the parameters off the stack

		// get our 'this' pointer
		typedef register_native_function<
			lua_State,
			out_val_type,
			in_val_type1,
			in_val_type2,
			in_val_type3,
			in_val_type4,
			in_val_type5,
			in_val_type6,
			in_val_type7,
			in_val_type8,
			in_val_type9,
			in_val_type10>
			this_type;

		typename typedef function_signature<out_val_type,
			in_val_type1,
			in_val_type2,
			in_val_type3,
			in_val_type4,
			in_val_type5,
			in_val_type6,
			in_val_type7,
			in_val_type8,
			in_val_type9,
			in_val_type10>
			OurFunctionType;

		this_type *mythis = (this_type *)lua_touserdata(L, lua_upvalueindex(1));

		// decided to do it this way; easier to read and understand than
		//  templat metafunction recrusion

		int our_ret_val = 0;

		in_val_type1 in_val1;
		in_val_type2 in_val2;
		in_val_type3 in_val3;
		in_val_type4 in_val4;
		in_val_type5 in_val5;
		in_val_type6 in_val6;
		in_val_type7 in_val7;
		in_val_type8 in_val8;
		in_val_type9 in_val9;
		in_val_type10 in_val10;

		if (get_parameter(&in_val1, L, 1) &&
			get_parameter(&in_val2, L, 2) &&
			get_parameter(&in_val3, L, 3) &&
			get_parameter(&in_val4, L, 4) &&
			get_parameter(&in_val5, L, 5) &&
			get_parameter(&in_val6, L, 6) &&
			get_parameter(&in_val7, L, 7) &&
			get_parameter(&in_val8, L, 8) &&
			get_parameter(&in_val9, L, 9) &&
			get_parameter(&in_val10, L, 10)) {
			// now actually call the function
#ifndef SCRIPT_GLUE_VOID_RET
			out_val_type ret_val =
#endif
				function_signature<
					out_val_type,
					in_val_type1,
					in_val_type2,
					in_val_type3,
					in_val_type4,
					in_val_type5,
					in_val_type6,
					in_val_type7,
					in_val_type8,
					in_val_type9,
					in_val_type10>::make_call(in_val1,
					in_val2,
					in_val3,
					in_val4,
					in_val5,
					in_val6,
					in_val7,
					in_val8,
					in_val9,
					in_val10,
					mythis->m_f);

#ifndef SCRIPT_GLUE_VOID_RET
			// now push the results back onto the stack
			push_parameter<out_val_type, lua_State>(L, ret_val);
			our_ret_val = 1;
#endif
		}

		return our_ret_val;
	}

	typename function_signature<out_val_type,
		in_val_type1,
		in_val_type2,
		in_val_type3,
		in_val_type4,
		in_val_type5,
		in_val_type6,
		in_val_type7,
		in_val_type8,
		in_val_type9,
		in_val_type10>::FunctionType m_f;

	register_native_function(lua_State *L, std::string class_name, std::string script_name,
		typename function_signature<out_val_type,
			in_val_type1,
			in_val_type2,
			in_val_type3,
			in_val_type4,
			in_val_type5,
			in_val_type6,
			in_val_type7,
			in_val_type8,
			in_val_type9,
			in_val_type10>::FunctionType f) {
		m_f = f;

		// register our main entry point
		std::string functionName = class_name + "_" + script_name;
		lua_pushlightuserdata(L, this);
		lua_pushcclosure(L, main_entry, 1);
		lua_setglobal(L, functionName.c_str());
	}
};

#undef out_val_type

} // namespace script_glue