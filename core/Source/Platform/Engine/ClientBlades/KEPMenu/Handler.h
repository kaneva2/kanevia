///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

namespace KEP {

class IScriptVM;
class IMenuDialog;
class IMenuControl;
class IMenuButton;
class IMenuRadioButton;
class IMenuCheckBox;
class IMenuListBox;
class IMenuScrollBar;
class IMenuComboBox;
class IMenuSlider;
class IMenuEditBox;
class IFlashPlayerControl;

///////////////////////////////////////////////////
// Helpers

void LogMenuScripts();

int FindOpenSlot();
int FindSlotByDialog(IMenuDialog *pDialog);
int FindSlotByState(IScriptVM *state);

std::string GetMenuScriptFilePathFromState(IScriptVM *state);
std::string GetMenuScriptFilePathFromDialog(IMenuDialog *);

IScriptVM *GetScriptVMFromDialog(IMenuDialog *);

int LoadMenuScript(IMenuDialog *pDialog, const std::string &scriptFilePath);

///////////////////////////////////////////////////
// Handlers

namespace Handlers {

	void OnInit();

	bool OnExit(IMenuDialog *pDialog);

	bool OnError(IMenuDialog *pDialog, const char *errMsg);
	bool OnCreate(IMenuDialog *pDialog);
	bool OnDestroy(IMenuDialog *pDialog);
	bool OnDelete(IMenuDialog *pDialog);
	bool OnRender(IMenuDialog *pDialog, float fElapsedTime);

	bool OnDialogMouseMove(IMenuDialog *pDialog, POINT pt);
	bool OnDialogLButtonDown(IMenuDialog *pDialog, POINT pt);
	bool OnDialogRButtonDown(IMenuDialog *pDialog, POINT pt);
	bool OnDialogLButtonDblClk(IMenuDialog *pDialog, POINT pt);
	bool OnDialogLButtonUp(IMenuDialog *pDialog, POINT pt);
	bool OnDialogRButtonUp(IMenuDialog *pDialog, POINT pt);
	bool OnDialogLButtonDownInside(IMenuDialog *pDialog, POINT pt);
	bool OnDialogKeyDown(IMenuDialog *pDialog, unsigned int key, BOOL bShiftDown, BOOL bControlDown, BOOL bAltDown);
	bool OnDialogSysKeyDown(IMenuDialog *pDialog, unsigned int key, BOOL bShiftDown, BOOL bControlDown, BOOL bAltDown);
	bool OnDialogKeyUp(IMenuDialog *pDialog, unsigned int key, BOOL bShiftDown, BOOL bControlDown, BOOL bAltDown);
	bool OnDialogSysKeyUp(IMenuDialog *pDialog, unsigned int key, BOOL bShiftDown, BOOL bControlDown, BOOL bAltDown);
	bool OnDialogSelected(IMenuDialog *targetDialog, IMenuDialog *pDialog);
	bool OnDialogMoved(IMenuDialog *targetDialog, IMenuDialog *pDialog, POINT pt);
	bool OnDialogResized(IMenuDialog *targetDialog, IMenuDialog *pDialog, POINT pt);

	bool OnControlMouseEnter(IMenuControl *);
	bool OnControlMouseLeave(IMenuControl *);
	bool OnControlFocusIn(IMenuControl *);
	bool OnControlFocusOut(IMenuControl *);
	bool OnControlLButtonDown(IMenuControl *, POINT pt);
	bool OnControlRButtonDown(IMenuControl *, POINT pt);
	bool OnControlLButtonDblClk(IMenuControl *, POINT pt);
	bool OnControlLButtonUp(IMenuControl *, POINT pt);
	bool OnControlRButtonUp(IMenuControl *, POINT pt);
	bool OnLinkClicked(IMenuControl *);
	// edit related events
	bool OnControlSelected(IMenuDialog *pDialog, IMenuControl *);
	bool OnControlMoved(IMenuDialog *pDialog, IMenuControl *, POINT pt);
	bool OnControlResized(IMenuDialog *pDialog, IMenuControl *, POINT pt);

	bool OnButtonEvent(IMenuButton *, const char *eventName);
	bool OnButtonClicked(IMenuButton *);

	bool OnCheckBoxChanged(IMenuCheckBox *);

	bool OnRadioButtonChanged(IMenuRadioButton *);

	bool OnComboBoxSelectionChanged(IMenuComboBox *);

	bool OnSliderValueChanged(IMenuSlider *);

	bool OnEditBoxString(IMenuEditBox *);
	bool OnEditBoxChange(IMenuEditBox *);
	bool OnEditBoxKeyDown(IMenuEditBox *, unsigned int key, BOOL bShiftDown, BOOL bControlDown, BOOL bAltDown);

	bool OnListBoxItemDblClick(IMenuListBox *pListBox);
	bool OnListBoxSelection(IMenuListBox *pListBox, POINT pt);

	bool OnScrollBarChanged(IMenuScrollBar *pScrollBar);

} // namespace Handlers

} // namespace KEP