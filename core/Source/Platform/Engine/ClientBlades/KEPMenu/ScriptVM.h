///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>
#include "script_glue.h"

namespace script_glue {

struct create_failed {
};

template <typename T>
class ScriptVM {
public:
	typedef T *ScriptVMHandle;

	// constructs the virtual machine
	ScriptVM(T *native) {
		m_vm = native;
		m_externalVM = true;
		m_native_registry = new registration_vector();
		KEP::add_registry(native, m_native_registry); // for cleanup later
	}

	ScriptVM() {
		m_vm = create_vm<T>();
		m_externalVM = false;
		m_native_registry = new std::vector<script_base_registration *>;
	}

	virtual ~ScriptVM() {
		if (!m_externalVM) {
			std::vector<script_base_registration *>::iterator itor = m_native_registry->begin();
			while (itor != m_native_registry->end()) {
				delete (*itor);
				itor++;
			}

			m_native_registry->clear();
			delete m_native_registry;

			destroy_vm<T>(m_vm);
		}
	}

	bool
	LoadFile(const std::string &filename) {
		return load_file<T>(m_vm, filename);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// ok; kinda frustrating.  template functions can't have default params, so to get it going,
	//  here are ten versions.  this will probably become a functor to help.
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	template <typename return_type>
	void RegisterNativeFunction(
		std::string class_name,
		std::string script_name,
		typename function_signature<
			return_type,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter>::FunctionType f) {
		m_native_registry->push_back(new register_native_function<T, return_type>(m_vm, class_name, script_name, f));
	}

	template <typename return_type,
		typename in_param1>
	void RegisterNativeFunction(
		std::string class_name,
		std::string script_name,
		typename function_signature<
			return_type,
			in_param1,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter>::FunctionType f) {
		m_native_registry->push_back(
			new register_native_function<T, return_type, in_param1>(m_vm, class_name, script_name, f));
	}

	template <typename return_type,
		typename in_param1,
		typename in_param2>
	void RegisterNativeFunction(
		std::string class_name,
		std::string script_name,
		typename function_signature<
			return_type,
			in_param1,
			in_param2,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter>::FunctionType f) {
		m_native_registry->push_back(
			new register_native_function<T, return_type, in_param1, in_param2>(m_vm, class_name, script_name, f));
	}

	template <typename return_type,
		typename in_param1,
		typename in_param2,
		typename in_param3>
	void RegisterNativeFunction(
		std::string class_name,
		std::string script_name,
		typename function_signature<
			return_type,
			in_param1,
			in_param2,
			in_param3,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter>::FunctionType f) {
		m_native_registry->push_back(
			new register_native_function<T, return_type,
				in_param1,
				in_param2,
				in_param3>(m_vm, class_name, script_name, f));
	}

	template <typename return_type,
		typename in_param1,
		typename in_param2,
		typename in_param3,
		typename in_param4>
	void RegisterNativeFunction(
		std::string class_name,
		std::string script_name,
		typename function_signature<
			return_type,
			in_param1,
			in_param2,
			in_param3,
			in_param4,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter>::FunctionType f) {
		m_native_registry->push_back(
			new register_native_function<T, return_type,
				in_param1,
				in_param2,
				in_param3,
				in_param4>(m_vm, class_name, script_name, f));
	}

	template <typename return_type,
		typename in_param1,
		typename in_param2,
		typename in_param3,
		typename in_param4,
		typename in_param5>
	void RegisterNativeFunction(
		std::string class_name,
		std::string script_name,
		typename function_signature<
			return_type,
			in_param1,
			in_param2,
			in_param3,
			in_param4,
			in_param5,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter>::FunctionType f) {
		m_native_registry->push_back(
			new register_native_function<T, return_type,
				in_param1,
				in_param2,
				in_param3,
				in_param4,
				in_param5>(m_vm, class_name, script_name, f));
	}

	template <typename return_type,
		typename in_param1,
		typename in_param2,
		typename in_param3,
		typename in_param4,
		typename in_param5,
		typename in_param6>
	void RegisterNativeFunction(
		std::string class_name,
		std::string script_name,
		typename function_signature<
			return_type,
			in_param1,
			in_param2,
			in_param3,
			in_param4,
			in_param5,
			in_param6,
			null_parameter,
			null_parameter,
			null_parameter,
			null_parameter>::FunctionType f) {
		m_native_registry->push_back(
			new register_native_function<T, return_type,
				in_param1,
				in_param2,
				in_param3,
				in_param4,
				in_param5,
				in_param6>(m_vm, class_name, script_name, f));
	}

	template <typename return_type,
		typename in_param1,
		typename in_param2,
		typename in_param3,
		typename in_param4,
		typename in_param5,
		typename in_param6,
		typename in_param7>
	void RegisterNativeFunction(
		std::string class_name,
		std::string script_name,
		typename function_signature<
			return_type,
			in_param1,
			in_param2,
			in_param3,
			in_param4,
			in_param5,
			in_param6,
			in_param7,
			null_parameter,
			null_parameter,
			null_parameter>::FunctionType f) {
		m_native_registry->push_back(
			new register_native_function<T, return_type,
				in_param1,
				in_param2,
				in_param3,
				in_param4,
				in_param5,
				in_param6,
				in_param7>(m_vm, class_name, script_name, f));
	}

	template <typename return_type,
		typename in_param1,
		typename in_param2,
		typename in_param3,
		typename in_param4,
		typename in_param5,
		typename in_param6,
		typename in_param7,
		typename in_param8>
	void RegisterNativeFunction(
		std::string class_name,
		std::string script_name,
		typename function_signature<
			return_type,
			in_param1,
			in_param2,
			in_param3,
			in_param4,
			in_param5,
			in_param6,
			in_param7,
			in_param8,
			null_parameter,
			null_parameter>::FunctionType f) {
		m_native_registry->push_back(
			new register_native_function<T, return_type,
				in_param1,
				in_param2,
				in_param3,
				in_param4,
				in_param5,
				in_param6,
				in_param7,
				in_param8>(m_vm, class_name, script_name, f));
	}

	template <typename return_type,
		typename in_param1,
		typename in_param2,
		typename in_param3,
		typename in_param4,
		typename in_param5,
		typename in_param6,
		typename in_param7,
		typename in_param8,
		typename in_param9>
	void RegisterNativeFunction(
		std::string class_name,
		std::string script_name,
		typename function_signature<
			return_type,
			in_param1,
			in_param2,
			in_param3,
			in_param4,
			in_param5,
			in_param6,
			in_param7,
			in_param8,
			in_param9,
			null_parameter>::FunctionType f) {
		m_native_registry->push_back(
			new register_native_function<T, return_type,
				in_param1,
				in_param2,
				in_param3,
				in_param4,
				in_param5,
				in_param6,
				in_param7,
				in_param8,
				in_param9>(m_vm, class_name, script_name, f));
	}

	template <typename return_type,
		typename in_param1,
		typename in_param2,
		typename in_param3,
		typename in_param4,
		typename in_param5,
		typename in_param6,
		typename in_param7,
		typename in_param8,
		typename in_param9,
		typename in_param10>
	void RegisterNativeFunction(
		std::string class_name,
		std::string script_name,
		typename function_signature<
			return_type,
			in_param1,
			in_param2,
			in_param3,
			in_param4,
			in_param5,
			in_param6,
			in_param7,
			in_param8,
			in_param9,
			in_param10>::FunctionType f) {
		m_native_registry->push_back(
			new register_native_function<T, return_type,
				in_param1,
				in_param2,
				in_param3,
				in_param4,
				in_param5,
				in_param6,
				in_param7,
				in_param8,
				in_param9,
				in_param10>(m_vm, class_name, script_name, f));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// any functions registered after this call will be placed inside a namespace
	//  if a BeginNamespace call is made before and EndNamespace call, it ends
	//  the previous namespace and begins putting subsequent functions in the new
	//  namespace.  functions registered outside of a Begin/End Namespace call are
	//  placed in the global namespace.  If a particular script vm doesn't support
	//  namespaces, then these functions are essentially ignored.
	//  BeginNamespace("") or BeginNamespace(0) are essentially the same as EndNamespace
	void BeginNamespace(std::string curr_namespace) {
		if (curr_namespace) {
			m_namespace = curr_namespace;
		} else
			m_namespace = "";
	}

	// this marks the end of the namespace declaration
	//  this actually registers the functions with the vm at this point
	void EndNamespace() {
		BeginNamespace("");
	}

	// just in case
	T *GetNativeHandle() { return m_vm; }

protected:
	std::string m_current_namespace;

	registration_vector *m_native_registry;

	T *m_vm;
	bool m_externalVM;
};

} // namespace script_glue
