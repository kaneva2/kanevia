///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

class CDXUTSelectionHandles {
public:
	CDXUTSelectionHandles() {
		RECT rect = { 0, 0, HandleSize, HandleSize };
		m_rcHandle = rect;
		m_color = D3DCOLOR_ARGB(255, 0, 0, 255);
		memset(m_handles, 0, sizeof(RECT) * NUM_HANDLES);
	}
	virtual ~CDXUTSelectionHandles() {}

	void SetColor(const D3DCOLOR color) { m_color = color; }
	void SetSelectionHandleRect(const RECT rect) { m_rcHandle = rect; }

	// selection handles
	typedef enum {
		NOT_FOUND = -1,
		TOP_LEFT = 0,
		TOP_MIDDLE,
		TOP_RIGHT,
		BOTTOM_LEFT,
		BOTTOM_MIDDLE,
		BOTTOM_RIGHT,
		LEFT_MIDDLE,
		RIGHT_MIDDLE,
		NUM_HANDLES
	} SelectionHandle;

	static const int HandleSize = 6;

	SelectionHandle GetSelectionHandleAtPoint(POINT pt);
	HRESULT SetSelectionHandles(const int x, const int y, const int nWidth, const int nHeight);
	HRESULT Render(const int x, const int y, const int width, const int height);
	LPTSTR GetResizingCursor(CDXUTSelectionHandles::SelectionHandle selectionHandle);

private:
	RECT m_rcHandle; // should be in the form {0, 0, 6, 6}
	D3DCOLOR m_color;
	RECT m_handles[NUM_HANDLES];
};

} // namespace KEP