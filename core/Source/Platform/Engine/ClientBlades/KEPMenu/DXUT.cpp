///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "dxstdafx.h"

#include "common/include/KEPCommon.h"
#include "Common/KEPUtil/Helpers.h"

#include "common\include\Rect.h"

#define MIN_WINDOW_SIZE_X 200
#define MIN_WINDOW_SIZE_Y 200
#define DXUTERR_SWITCHEDTOREF MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x1001)

namespace KEP {

static CRITICAL_SECTION g_cs;
static bool g_bThreadSafe = true;

class DXUTLock {
public:
	inline DXUTLock() {
		if (g_bThreadSafe)
			EnterCriticalSection(&g_cs);
	}
	inline ~DXUTLock() {
		if (g_bThreadSafe)
			LeaveCriticalSection(&g_cs);
	}
};

#define SET_ACCESSOR(x, y)    \
	inline void Set##y(x t) { \
		DXUTLock l;           \
		m_state.m_##y = t;    \
	};
#define GET_ACCESSOR(x, y)    \
	inline x Get##y() {       \
		DXUTLock l;           \
		return m_state.m_##y; \
	};
#define GET_SET_ACCESSOR(x, y) \
	SET_ACCESSOR(x, y)         \
	GET_ACCESSOR(x, y)

#define SETP_ACCESSOR(x, y)    \
	inline void Set##y(x* t) { \
		DXUTLock l;            \
		m_state.m_##y = *t;    \
	};
#define GETP_ACCESSOR(x, y)    \
	inline x* Get##y() {       \
		DXUTLock l;            \
		return &m_state.m_##y; \
	};
#define GETP_SETP_ACCESSOR(x, y) \
	SETP_ACCESSOR(x, y)          \
	GETP_ACCESSOR(x, y)

LONG_PTR g_oldWinProc = 0;

struct DXUT_TIMER {
	LPDXUTCALLBACKTIMER pCallbackTimer;
	float fTimeoutInSecs;
	float fCountdown;
	bool bEnabled;
};

class DXUTState {
protected:
	struct STATE {
		IDirect3D9* m_D3D; // the main D3D object

		IDirect3DDevice9* m_D3DDevice; // the D3D rendering device
		CD3DEnumeration* m_D3DEnumeration; // CD3DEnumeration object

		DXUTDeviceSettings* m_CurrentDeviceSettings; // current device settings
		D3DSURFACE_DESC m_BackBufferSurfaceDesc; // back buffer surface description
		D3DCAPS9 m_Caps; // D3D caps for current device

		HWND m_HWNDFocus; // the main app focus window
		HWND m_HWNDDeviceFullScreen; // the main app device window in fullscreen mode
		HWND m_HWNDDeviceWindowed; // the main app device window in windowed mode
		HMONITOR m_AdapterMonitor; // the monitor of the adapter
		double m_Time; // current time in seconds
		float m_ElapsedTime; // time elapsed since last frame

		DWORD m_WinStyle; // window style
		RECT m_WindowClientRect; // client rect of HWND
		RECT m_FullScreenClientRect; // client rect of HWND when fullscreen
		RECT m_WindowBoundsRect; // window rect of HWND
		HMENU m_Menu; // handle to menu
		double m_LastStatsUpdateTime; // last time the stats were updated
		DWORD m_LastStatsUpdateFrames; // frames count since last time the stats were updated
		float m_FPS; // frames per second
		int m_CurrentFrameNumber; // the current frame number

		bool m_HandleDefaultHotkeys; // if true, the sample framework will handle some default hotkeys
		bool m_ShowMsgBoxOnError; // if true, then msgboxes are displayed upon errors
		bool m_ClipCursorWhenFullScreen; // if true, then the sample framework will keep the cursor from going outside the window when full screen
		bool m_ShowCursor; // if true, then the sample framework will show a cursor
		bool m_UseWindowsCursor; // if true, then the Windows cursor will be used
		POINT m_CursorHotSpotPoint; // location of cursor hot spot
		bool m_ConstantFrameTime; // if true, then elapsed frame time will always be 0.05f seconds which is good for debugging or automated capture
		float m_TimePerFrame; // the constant time per frame in seconds, only valid if m_ConstantFrameTime==true
		bool m_WireframeMode; // if true, then D3DRS_FILLMODE==D3DFILL_WIREFRAME else D3DRS_FILLMODE==D3DFILL_SOLID
		bool m_AutoChangeAdapter; // if true, then the adapter will automatically change if the window is different monitor
		bool m_WindowCreatedWithDefaultPositions; // if true, then CW_USEDEFAULT was used and the window should be moved to the right adapter
		int m_ExitCode; // the exit code to be returned to the command line

		bool m_DXUTInited; // if true, then DXUTInit() has succeeded
		bool m_WindowCreated; // if true, then DXUTCreateWindow() or DXUTSetWindow() has succeeded
		bool m_DeviceCreated; // if true, then DXUTCreateDevice*() or DXUTSetDevice() has succeeded

		bool m_DXUTInitCalled; // if true, then DXUTInit() was called
		bool m_WindowCreateCalled; // if true, then DXUTCreateWindow() or DXUTSetWindow() was called
		bool m_DeviceCreateCalled; // if true, then DXUTCreateDevice*() or DXUTSetDevice() was called

		bool m_DeviceObjectsCreated; // if true, then DeviceCreated callback has been called (if non-NULL)
		bool m_DeviceObjectsReset; // if true, then DeviceReset callback has been called (if non-NULL)
		bool m_InsideDeviceCallback; // if true, then the framework is inside an app device callback
		bool m_InsideMainloop; // if true, then the framework is inside the main loop
		bool m_Active; // if true, then the app is the active top level window
		bool m_TimePaused; // if true, then time is paused
		bool m_RenderingPaused; // if true, then rendering is paused
		int m_PauseRenderingCount; // pause rendering ref count
		int m_PauseTimeCount; // pause time ref count
		bool m_DeviceLost; // if true, then the device is lost and needs to be reset
		bool m_Minimized; // if true, then the HWND is minimized
		bool m_Maximized; // if true, then the HWND is maximized
		bool m_IgnoreSizeChange; // if true, the sample framework won't reset the device upon HWND size change (for internal use only)
		bool m_NotifyOnMouseMove; // if true, include WM_MOUSEMOVE in mousecallback

		int m_OverrideAdapterOrdinal; // if != -1, then override to use this adapter ordinal
		bool m_OverrideWindowed; // if true, then force to start windowed
		bool m_OverrideFullScreen; // if true, then force to start full screen
		int m_OverrideStartX; // if != -1, then override to this X position of the window
		int m_OverrideStartY; // if != -1, then override to this Y position of the window
		int m_OverrideWidth; // if != 0, then override to this width
		int m_OverrideHeight; // if != 0, then override to this height
		bool m_OverrideForceHAL; // if true, then force to HAL device (failing if one doesn't exist)
		bool m_OverrideForceREF; // if true, then force to REF device (failing if one doesn't exist)
		bool m_OverrideForcePureHWVP; // if true, then force to use pure HWVP (failing if device doesn't support it)
		bool m_OverrideForceHWVP; // if true, then force to use HWVP (failing if device doesn't support it)
		bool m_OverrideForceSWVP; // if true, then force to use SWVP
		bool m_OverrideConstantFrameTime; // if true, then force to constant frame time
		float m_OverrideConstantTimePerFrame; // the constant time per frame in seconds if m_OverrideConstantFrameTime==true
		int m_OverrideQuitAfterFrame; // if != 0, then it will force the app to quit after that frame

		LPDXUTCALLBACKISDEVICEACCEPTABLE m_IsDeviceAcceptableFunc; // is device acceptable callback
		LPDXUTCALLBACKMODIFYDEVICESETTINGS m_ModifyDeviceSettingsFunc; // modify device settings callback
		LPDXUTCALLBACKDEVICECREATED m_DeviceCreatedFunc; // device created callback
		LPDXUTCALLBACKDEVICERESET m_DeviceResetFunc; // device reset callback
		LPDXUTCALLBACKDEVICELOST m_DeviceLostFunc; // device lost callback
		LPDXUTCALLBACKDEVICEDESTROYED m_DeviceDestroyedFunc; // device destroyed callback
		LPDXUTCALLBACKFRAMEMOVE m_FrameMoveFunc; // frame move callback
		LPDXUTCALLBACKFRAMERENDER m_FrameRenderFunc; // frame render callback
		LPDXUTCALLBACKKEYBOARD m_KeyboardFunc; // keyboard callback
		LPDXUTCALLBACKMOUSE m_MouseFunc; // mouse callback
		LPDXUTCALLBACKMSGPROC m_WindowMsgFunc; // window messages callback

		bool m_ShowD3DSettingsDlg; // if true, then show the D3DSettingsDlg
		bool m_Keys[256]; // array of key state
		bool m_MouseButtons[5]; // array of mouse states

		CGrowableArray<DXUT_TIMER>* m_TimerList; // list of DXUT_TIMER structs
		wchar_t m_StaticFrameStats[256]; // static part of frames stats
		wchar_t m_FrameStats[256]; // frame stats (fps, width, etc)
		wchar_t m_DeviceStats[256]; // device stats (description, device type, etc)
		wchar_t m_WindowTitle[256]; // window title
		wchar_t m_CursorFilename[256]; // path to cursor texture
	};

	STATE m_state;

public:
	DXUTState() {
		Create();
	}

	virtual ~DXUTState() {}

	void Create() {
		// Make sure these are created before DXUTState so they
		// destoryed last because DXUTState cleanup needs them
		DXUTGetGlobalDialogResourceManager();
		DXUTGetGlobalResourceCache();

		ZeroMemory(&m_state, sizeof(STATE));

		g_bThreadSafe = true;
		InitializeCriticalSection(&g_cs);
		m_state.m_OverrideStartX = -1;
		m_state.m_OverrideStartY = -1;
		m_state.m_OverrideAdapterOrdinal = -1;
		m_state.m_AutoChangeAdapter = true;
		m_state.m_ShowMsgBoxOnError = true;
		m_state.m_Active = true;
	}

	void Destroy() {
		DXUTShutdown();
		DeleteCriticalSection(&g_cs);
	}

	// Macros to define access functions for thread safe access into m_state
	GET_SET_ACCESSOR(IDirect3D9*, D3D);

	GET_SET_ACCESSOR(IDirect3DDevice9*, D3DDevice);
	GET_SET_ACCESSOR(CD3DEnumeration*, D3DEnumeration);
	GET_SET_ACCESSOR(DXUTDeviceSettings*, CurrentDeviceSettings);
	GETP_SETP_ACCESSOR(D3DSURFACE_DESC, BackBufferSurfaceDesc);
	GETP_SETP_ACCESSOR(D3DCAPS9, Caps);

	GET_SET_ACCESSOR(HWND, HWNDFocus);
	GET_SET_ACCESSOR(HWND, HWNDDeviceFullScreen);
	GET_SET_ACCESSOR(HWND, HWNDDeviceWindowed);
	GET_SET_ACCESSOR(HMONITOR, AdapterMonitor);
	GET_SET_ACCESSOR(double, Time);
	GET_SET_ACCESSOR(float, ElapsedTime);

	GET_SET_ACCESSOR(DWORD, WinStyle);
	GET_SET_ACCESSOR(const RECT&, WindowClientRect);
	GET_SET_ACCESSOR(const RECT&, FullScreenClientRect);
	GET_SET_ACCESSOR(const RECT&, WindowBoundsRect);
	GET_SET_ACCESSOR(HMENU, Menu);
	GET_SET_ACCESSOR(double, LastStatsUpdateTime);
	GET_SET_ACCESSOR(DWORD, LastStatsUpdateFrames);
	GET_SET_ACCESSOR(float, FPS);
	GET_SET_ACCESSOR(int, CurrentFrameNumber);

	GET_SET_ACCESSOR(bool, HandleDefaultHotkeys);
	GET_SET_ACCESSOR(bool, ShowMsgBoxOnError);
	GET_SET_ACCESSOR(bool, ClipCursorWhenFullScreen);

	GET_SET_ACCESSOR(bool, ShowCursor);

	GET_SET_ACCESSOR(bool, UseWindowsCursor);
	GET_SET_ACCESSOR(const POINT&, CursorHotSpotPoint);
	GET_SET_ACCESSOR(bool, ConstantFrameTime);
	GET_SET_ACCESSOR(float, TimePerFrame);
	GET_SET_ACCESSOR(bool, WireframeMode);
	GET_SET_ACCESSOR(bool, AutoChangeAdapter);
	GET_SET_ACCESSOR(bool, WindowCreatedWithDefaultPositions);
	GET_SET_ACCESSOR(int, ExitCode);

	GET_SET_ACCESSOR(bool, DXUTInited);
	GET_SET_ACCESSOR(bool, WindowCreated);
	GET_SET_ACCESSOR(bool, DeviceCreated);
	GET_SET_ACCESSOR(bool, DXUTInitCalled);
	GET_SET_ACCESSOR(bool, WindowCreateCalled);
	GET_SET_ACCESSOR(bool, DeviceCreateCalled);
	GET_SET_ACCESSOR(bool, InsideDeviceCallback);
	GET_SET_ACCESSOR(bool, InsideMainloop);
	GET_SET_ACCESSOR(bool, DeviceObjectsCreated);
	GET_SET_ACCESSOR(bool, DeviceObjectsReset);
	GET_SET_ACCESSOR(bool, Active);
	GET_SET_ACCESSOR(bool, RenderingPaused);
	GET_SET_ACCESSOR(bool, TimePaused);
	GET_SET_ACCESSOR(int, PauseRenderingCount);
	GET_SET_ACCESSOR(int, PauseTimeCount);
	GET_SET_ACCESSOR(bool, DeviceLost);
	GET_SET_ACCESSOR(bool, Minimized);
	GET_SET_ACCESSOR(bool, Maximized);
	GET_SET_ACCESSOR(bool, IgnoreSizeChange);
	GET_SET_ACCESSOR(bool, NotifyOnMouseMove);

	GET_SET_ACCESSOR(int, OverrideAdapterOrdinal);
	GET_SET_ACCESSOR(bool, OverrideWindowed);
	GET_SET_ACCESSOR(bool, OverrideFullScreen);
	GET_SET_ACCESSOR(int, OverrideStartX);
	GET_SET_ACCESSOR(int, OverrideStartY);
	GET_SET_ACCESSOR(int, OverrideWidth);
	GET_SET_ACCESSOR(int, OverrideHeight);
	GET_SET_ACCESSOR(bool, OverrideForceHAL);
	GET_SET_ACCESSOR(bool, OverrideForceREF);
	GET_SET_ACCESSOR(bool, OverrideForcePureHWVP);
	GET_SET_ACCESSOR(bool, OverrideForceHWVP);
	GET_SET_ACCESSOR(bool, OverrideForceSWVP);
	GET_SET_ACCESSOR(bool, OverrideConstantFrameTime);
	GET_SET_ACCESSOR(float, OverrideConstantTimePerFrame);
	GET_SET_ACCESSOR(int, OverrideQuitAfterFrame);

	GET_SET_ACCESSOR(LPDXUTCALLBACKISDEVICEACCEPTABLE, IsDeviceAcceptableFunc);
	GET_SET_ACCESSOR(LPDXUTCALLBACKMODIFYDEVICESETTINGS, ModifyDeviceSettingsFunc);
	GET_SET_ACCESSOR(LPDXUTCALLBACKDEVICECREATED, DeviceCreatedFunc);
	GET_SET_ACCESSOR(LPDXUTCALLBACKDEVICERESET, DeviceResetFunc);
	GET_SET_ACCESSOR(LPDXUTCALLBACKDEVICELOST, DeviceLostFunc);
	GET_SET_ACCESSOR(LPDXUTCALLBACKDEVICEDESTROYED, DeviceDestroyedFunc);
	GET_SET_ACCESSOR(LPDXUTCALLBACKFRAMEMOVE, FrameMoveFunc);
	GET_SET_ACCESSOR(LPDXUTCALLBACKFRAMERENDER, FrameRenderFunc);
	GET_SET_ACCESSOR(LPDXUTCALLBACKKEYBOARD, KeyboardFunc);
	GET_SET_ACCESSOR(LPDXUTCALLBACKMOUSE, MouseFunc);
	GET_SET_ACCESSOR(LPDXUTCALLBACKMSGPROC, WindowMsgFunc);

	GET_SET_ACCESSOR(bool, ShowD3DSettingsDlg);

	GET_SET_ACCESSOR(CGrowableArray<DXUT_TIMER>*, TimerList);
	GET_ACCESSOR(bool*, Keys);
	GET_ACCESSOR(bool*, MouseButtons);
	GET_ACCESSOR(wchar_t*, StaticFrameStats);
	GET_ACCESSOR(wchar_t*, FrameStats);
	GET_ACCESSOR(wchar_t*, DeviceStats);
	GET_ACCESSOR(wchar_t*, WindowTitle);
	GET_ACCESSOR(wchar_t*, CursorFilename);
};

DXUTState& GetDXUTState() {
	// Using an accessor function gives control of the construction order
	static DXUTState state;
	return state;
}

typedef IDirect3D9*(WINAPI* LPDIRECT3DCREATE9)(UINT SDKVersion);
typedef DECLSPEC_IMPORT UINT(WINAPI* LPTIMEBEGINPERIOD)(UINT uPeriod);

CD3DEnumeration* DXUTPrepareEnumerationObject(bool bEnumerate = false);
HRESULT DXUTFindAdapterFormat(UINT AdapterOrdinal, D3DDEVTYPE DeviceType, D3DFORMAT BackBufferFormat, BOOL Windowed, D3DFORMAT* pAdapterFormat);
HRESULT DXUTChangeDevice(DXUTDeviceSettings* pNewDeviceSettings, IDirect3DDevice9* pd3dDeviceFromApp, bool bForceRecreate);
HRESULT DXUTInitialize3DEnvironment();
void DXUTPrepareDevice(IDirect3DDevice9* pd3dDevice);
HRESULT DXUTReset3DEnvironment();
void DXUTCleanup3DEnvironment(bool bReleaseSettings = true);
void DXUTHandlePossibleSizeChangeWithoutResetDevice();
void DXUTAdjustWindowStyle(HWND hWnd, bool bWindowed);
void DXUTDisplayErrorMessage(HRESULT hr);
LRESULT CALLBACK DXUTStaticWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

void DXUTSetCallbackDeviceCreated(LPDXUTCALLBACKDEVICECREATED pCallbackDeviceCreated) {
	GetDXUTState().SetDeviceCreatedFunc(pCallbackDeviceCreated);
}
void DXUTSetCallbackDeviceReset(LPDXUTCALLBACKDEVICERESET pCallbackDeviceReset) {
	GetDXUTState().SetDeviceResetFunc(pCallbackDeviceReset);
}
void DXUTSetCallbackDeviceLost(LPDXUTCALLBACKDEVICELOST pCallbackDeviceLost) {
	GetDXUTState().SetDeviceLostFunc(pCallbackDeviceLost);
}
void DXUTSetCallbackDeviceDestroyed(LPDXUTCALLBACKDEVICEDESTROYED pCallbackDeviceDestroyed) {
	GetDXUTState().SetDeviceDestroyedFunc(pCallbackDeviceDestroyed);
}
void DXUTSetCallbackFrameMove(LPDXUTCALLBACKFRAMEMOVE pCallbackFrameMove) {
	GetDXUTState().SetFrameMoveFunc(pCallbackFrameMove);
}
void DXUTSetCallbackFrameRender(LPDXUTCALLBACKFRAMERENDER pCallbackFrameRender) {
	GetDXUTState().SetFrameRenderFunc(pCallbackFrameRender);
}
void DXUTSetCallbackKeyboard(LPDXUTCALLBACKKEYBOARD pCallbackKeyboard) {
	GetDXUTState().SetKeyboardFunc(pCallbackKeyboard);
}
void DXUTSetCallbackMouse(LPDXUTCALLBACKMOUSE pCallbackMouse, bool bIncludeMouseMove) {
	GetDXUTState().SetMouseFunc(pCallbackMouse);
	GetDXUTState().SetNotifyOnMouseMove(bIncludeMouseMove);
}
void DXUTSetCallbackMsgProc(LPDXUTCALLBACKMSGPROC pCallbackMsgProc) {
	GetDXUTState().SetWindowMsgFunc(pCallbackMsgProc);
}

HRESULT DXUTInit() {
	GetDXUTState().SetDXUTInitCalled(true);

	// Not always needed, but lets the app create GDI dialogs
	InitCommonControls();

	// Increase the accuracy of Sleep() without needing to link to winmm.lib
	wchar_t wszPath[MAX_PATH + 1];
	if (!::GetSystemDirectory(wszPath, MAX_PATH + 1))
		return E_FAIL;
	lstrcat(wszPath, _T("\\winmm.dll"));
	HINSTANCE hInstWinMM = LoadLibrary(wszPath);
	if (hInstWinMM != NULL) {
		LPTIMEBEGINPERIOD pTimeBeginPeriod = (LPTIMEBEGINPERIOD)GetProcAddress(hInstWinMM, "timeBeginPeriod");
		if (NULL != pTimeBeginPeriod)
			pTimeBeginPeriod(1);
	}
	FreeLibrary(hInstWinMM);

	GetDXUTState().SetShowMsgBoxOnError(false);
	GetDXUTState().SetHandleDefaultHotkeys(false);

	// Verify D3DX version
	if (!D3DXCheckVersion(D3D_SDK_VERSION, D3DX_SDK_VERSION)) {
		DXUTDisplayErrorMessage(DXUTERR_INCORRECTVERSION);
		return DXUT_ERR(_T("D3DXCheckVersion"), DXUTERR_INCORRECTVERSION);
	}

	// Create a Direct3D object if one has not already been created
	IDirect3D9* pD3D = DXUTGetD3DObject();
	if (pD3D == NULL) {
		// This may fail if DirectX 9 isn't installed
		// This may fail if the DirectX headers are out of sync with the installed DirectX DLLs
		pD3D = DXUT_Dynamic_Direct3DCreate9(D3D_SDK_VERSION);
		GetDXUTState().SetD3D(pD3D);
	}

	if (pD3D == NULL) {
		// If still NULL, then something went wrong
		DXUTDisplayErrorMessage(DXUTERR_NODIRECT3D);
		return DXUT_ERR(_T("Direct3DCreate9"), DXUTERR_NODIRECT3D);
	}

	// Reset the timer
	DXUTGetGlobalTimer()->Reset();

	GetDXUTState().SetDXUTInited(true);

	return S_OK;
}

bool DXUTSyncWindowRect(bool adjStyle) {
	HWND hWnd = DXUTGetHWNDFocus();
	if (!hWnd)
		return false;

	RECT rect;
	if (::GetWindowRect(hWnd, &rect) == FALSE)
		return false;

	// Adjust For Window Style ? (add [16 x 39] to match ClientEngineApp::GetWindowCoords())
	if (adjStyle) {
		RECT rectAdj = RECT({ 0, 0, 0, 0 });
		::AdjustWindowRect(&rectAdj, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN, false);
		rect.right += RectWidth(rectAdj) + 1;
		rect.bottom += RectHeight(rectAdj) + 1;
	}

	GetDXUTState().SetWindowClientRect(RECT({ 0, 0, RectWidth(rect), RectHeight(rect) }));
	GetDXUTState().SetWindowBoundsRect(rect);

	return true;
}

HRESULT DXUTSetWindow(HWND hWnd, bool bHandleMessages) {
	HWND hWndFocus = hWnd;
	HWND hWndDeviceFullScreen = hWnd;
	HWND hWndDeviceWindowed = hWnd;

	// Not allowed to call this from inside the device callbacks
	if (GetDXUTState().GetInsideDeviceCallback())
		return DXUT_ERR_MSGBOX(_T("DXUTCreateWindow"), E_FAIL);

	GetDXUTState().SetWindowCreateCalled(true);

	// To avoid confusion, we do not allow any HWND to be NULL here.  The
	// caller must pass in valid HWND for all three parameters.  The same
	// HWND may be used for more than one parameter.
	if (hWndFocus == NULL || hWndDeviceFullScreen == NULL || hWndDeviceWindowed == NULL)
		return DXUT_ERR_MSGBOX(_T("DXUTSetWindow"), E_INVALIDARG);

	// If subclassing the window, set the pointer to the local window procedure
	if (bHandleMessages) {
		// Switch window procedures
#ifdef _WIN64
		LONG_PTR nResult = SetWindowLongPtr(hWndFocus, GWLP_WNDPROC, (LONG_PTR)DXUTStaticWndProc);
#else
		LONG_PTR nResult = SetWindowLongPtr(hWndFocus, GWLP_WNDPROC, (LONG)(LONG_PTR)DXUTStaticWndProc);
#endif

		g_oldWinProc = nResult;

		DWORD dwError = GetLastError();
		if (nResult == 0)
			return DXUT_ERR_MSGBOX(_T("SetWindowLongPtr"), HRESULT_FROM_WIN32(dwError));
	}

	wchar_t* strCachedWindowTitle = GetDXUTState().GetWindowTitle();
	GetWindowText(hWndFocus, strCachedWindowTitle, 255);
	strCachedWindowTitle[255] = 0;

	// Get the window's initial style
	DWORD dwWindowStyle = GetWindowLong(hWndDeviceWindowed, GWL_STYLE);
	GetDXUTState().SetWinStyle(dwWindowStyle);
	GetDXUTState().SetWindowCreatedWithDefaultPositions(false);

	// Store the client and window rects of the windowed-mode device window
	RECT rcClient;
	GetClientRect(hWndDeviceWindowed, &rcClient);
	GetDXUTState().SetWindowClientRect(rcClient);

	RECT rcWindow;
	GetWindowRect(hWndDeviceWindowed, &rcWindow);
	GetDXUTState().SetWindowBoundsRect(rcWindow);

	GetDXUTState().SetWindowCreated(true);
	GetDXUTState().SetHWNDFocus(hWndFocus);
	GetDXUTState().SetHWNDDeviceFullScreen(hWndDeviceFullScreen);
	GetDXUTState().SetHWNDDeviceWindowed(hWndDeviceWindowed);

	return S_OK;
}

HRESULT DXUTSetDeviceWithoutCreatingWindow(IDirect3DDevice9* pd3dDevice) {
	HRESULT hr;

	if (pd3dDevice == NULL)
		return DXUT_ERR_MSGBOX(_T("DXUTSetDevice"), E_INVALIDARG);

	// Not allowed to call this from inside the device callbacks
	if (GetDXUTState().GetInsideDeviceCallback())
		return DXUT_ERR_MSGBOX(_T("DXUTCreateWindow"), E_FAIL);

	DXUTDeviceSettings* pDeviceSettings = new DXUTDeviceSettings;
	if (pDeviceSettings == NULL)
		return E_OUTOFMEMORY;
	ZeroMemory(pDeviceSettings, sizeof(DXUTDeviceSettings));

	// Get the present params from the swap chain
	IDirect3DSurface9* pBackBuffer = NULL;
	hr = pd3dDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer);
	if (SUCCEEDED(hr)) {
		IDirect3DSwapChain9* pSwapChain = NULL;
		hr = pBackBuffer->GetContainer(IID_IDirect3DSwapChain9, (void**)&pSwapChain);
		if (SUCCEEDED(hr)) {
			pSwapChain->GetPresentParameters(&pDeviceSettings->pp);
			SAFE_RELEASE(pSwapChain);
		}

		SAFE_RELEASE(pBackBuffer);
	}

	D3DDEVICE_CREATION_PARAMETERS d3dCreationParams;
	pd3dDevice->GetCreationParameters(&d3dCreationParams);

	// Fill out the rest of the device settings struct
	pDeviceSettings->AdapterOrdinal = d3dCreationParams.AdapterOrdinal;
	pDeviceSettings->DeviceType = d3dCreationParams.DeviceType;
	DXUTFindAdapterFormat(pDeviceSettings->AdapterOrdinal, pDeviceSettings->DeviceType,
		pDeviceSettings->pp.BackBufferFormat, pDeviceSettings->pp.Windowed,
		&pDeviceSettings->AdapterFormat);
	pDeviceSettings->BehaviorFlags = d3dCreationParams.BehaviorFlags;

	// DRF - Added - Sync Window Rectangles
	DXUTSyncWindowRect(true);

	//// Change to the Direct3D device passed in
	hr = DXUTChangeDevice(pDeviceSettings, pd3dDevice, false);
	if (FAILED(hr))
		return hr;

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Internal helper function to return the adapter format from the first device settings
// combo that matches the passed adapter ordinal, device type, backbuffer format, and windowed.
//--------------------------------------------------------------------------------------
HRESULT DXUTFindAdapterFormat(UINT AdapterOrdinal, D3DDEVTYPE DeviceType, D3DFORMAT BackBufferFormat,
	BOOL Windowed, D3DFORMAT* pAdapterFormat) {
	CD3DEnumeration* pd3dEnum = DXUTPrepareEnumerationObject();
	CD3DEnumDeviceInfo* pDeviceInfo = pd3dEnum->GetDeviceInfo(AdapterOrdinal, DeviceType);
	if (pDeviceInfo) {
		for (int iDeviceCombo = 0; iDeviceCombo < pDeviceInfo->deviceSettingsComboList.GetSize(); iDeviceCombo++) {
			CD3DEnumDeviceSettingsCombo* pDeviceSettingsCombo = pDeviceInfo->deviceSettingsComboList.GetAt(iDeviceCombo);
			if (pDeviceSettingsCombo->BackBufferFormat == BackBufferFormat &&
				pDeviceSettingsCombo->Windowed == Windowed) {
				// Return the adapter format from the first match
				*pAdapterFormat = pDeviceSettingsCombo->AdapterFormat;
				return S_OK;
			}
		}
	}

	*pAdapterFormat = BackBufferFormat;
	return E_FAIL;
}

//--------------------------------------------------------------------------------------
// Change to a Direct3D device created from the device settings or passed in.
// The framework will only reset if the device is similar to the previous device
// otherwise it will cleanup the previous device (if there is one) and recreate the
// scene using the app's device callbacks.
//--------------------------------------------------------------------------------------
HRESULT DXUTChangeDevice(DXUTDeviceSettings* pNewDeviceSettings, IDirect3DDevice9* pd3dDeviceFromApp, bool bForceRecreate) {
	HRESULT hr;

	if (!pNewDeviceSettings || !DXUTGetD3DObject())
		return S_FALSE;

	// This Returns NULL While DirectX Is Not Yet Initialized
	DXUTDeviceSettings* pOldDeviceSettings = GetDXUTState().GetCurrentDeviceSettings();

	// Make a copy of the pNewDeviceSettings on the heap
	DXUTDeviceSettings* pNewDeviceSettingsOnHeap = new DXUTDeviceSettings;
	if (!pNewDeviceSettingsOnHeap)
		return E_OUTOFMEMORY;
	memcpy(pNewDeviceSettingsOnHeap, pNewDeviceSettings, sizeof(DXUTDeviceSettings));
	pNewDeviceSettings = pNewDeviceSettingsOnHeap;

	GetDXUTState().SetCurrentDeviceSettings(pNewDeviceSettings);

	DXUTPause(true, true);

	// When a WM_SIZE message is received, it calls DXUTHandlePossibleSizeChange().
	// A WM_SIZE message might be sent when adjusting the window, so tell
	// DXUTHandlePossibleSizeChange() to ignore size changes temporarily
	GetDXUTState().SetIgnoreSizeChange(true);

	// Update thread safety on/off depending on Direct3D device's thread safety
	g_bThreadSafe = ((pNewDeviceSettings->BehaviorFlags & D3DCREATE_MULTITHREADED) != 0);

	// Don't allow smaller than what's used in WM_GETMINMAXINFO
	// otherwise the window size will be different than the backbuffer size
	if (pNewDeviceSettings->pp.BackBufferWidth < MIN_WINDOW_SIZE_X)
		pNewDeviceSettings->pp.BackBufferWidth = MIN_WINDOW_SIZE_X;
	if (pNewDeviceSettings->pp.BackBufferHeight < MIN_WINDOW_SIZE_Y)
		pNewDeviceSettings->pp.BackBufferHeight = MIN_WINDOW_SIZE_Y;

	pd3dDeviceFromApp->AddRef();

	GetDXUTState().SetD3DDevice(pd3dDeviceFromApp);

	// Now that the device is created, update the window and misc settings and
	// call the app's DeviceCreated and DeviceReset callbacks.
	hr = DXUTInitialize3DEnvironment();
	if (FAILED(hr)) {
		DXUTDisplayErrorMessage(hr);
		DXUTPause(false, false);
		return hr;
	}

	SAFE_DELETE(pOldDeviceSettings);

	IDirect3D9* pD3D = DXUTGetD3DObject();
	HMONITOR hAdapterMonitor = pD3D->GetAdapterMonitor(pNewDeviceSettings->AdapterOrdinal);
	GetDXUTState().SetAdapterMonitor(hAdapterMonitor);

	// Resize the window
	RECT rcWindowBounds = GetDXUTState().GetWindowBoundsRect();
	POINT ptClient = { rcWindowBounds.left, rcWindowBounds.top };
	ScreenToClient(GetParent(DXUTGetHWNDDeviceWindowed()), &ptClient);
	SetWindowPos(
		DXUTGetHWND(),
		HWND_NOTOPMOST,
		ptClient.x,
		ptClient.y,
		RectWidth(rcWindowBounds),
		RectHeight(rcWindowBounds),
		SWP_SHOWWINDOW // | SWP_NOACTIVATE
	);

	// Update the cache of the window style
	GetDXUTState().SetWinStyle(GetDXUTState().GetWinStyle() | WS_VISIBLE);

	GetDXUTState().SetIgnoreSizeChange(false);
	DXUTPause(false, false);
	GetDXUTState().SetDeviceCreated(true);

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Internal helper function to prepare the enumeration object by creating it if it
// didn't already exist and enumerating if desired.
//--------------------------------------------------------------------------------------
CD3DEnumeration* DXUTPrepareEnumerationObject(bool bEnumerate) {
	// Create a new CD3DEnumeration object and enumerate all devices unless its already been done
	CD3DEnumeration* pd3dEnum = GetDXUTState().GetD3DEnumeration();
	if (pd3dEnum == NULL) {
		pd3dEnum = DXUTGetEnumeration();
		GetDXUTState().SetD3DEnumeration(pd3dEnum);

		bEnumerate = true;
	}

	if (bEnumerate) {
		// Enumerate for each adapter all of the supported display modes,
		// device types, adapter formats, back buffer formats, window/full screen support,
		// depth stencil formats, multisampling types/qualities, and presentations intervals.
		//
		// For each combination of device type (HAL/REF), adapter format, back buffer format, and
		// IsWindowed it will call the app's ConfirmDevice callback.  This allows the app
		// to reject or allow that combination based on its caps/etc.  It also allows the
		// app to change the BehaviorFlags.  The BehaviorFlags defaults non-pure HWVP
		// if supported otherwise it will default to SWVP, however the app can change this
		// through the ConfirmDevice callback.
		IDirect3D9* pD3D = DXUTGetD3DObject();
		pd3dEnum->Enumerate(pD3D, GetDXUTState().GetIsDeviceAcceptableFunc());
	}

	return pd3dEnum;
}

//--------------------------------------------------------------------------------------
// Initializes the 3D environment by:
//       - Adjusts the window size, style, and menu
//       - Stores the back buffer description
//       - Sets up the full screen Direct3D cursor if requested
//       - Calls the device created callback
//       - Calls the device reset callback
//       - If both callbacks succeed it unpauses the app
//--------------------------------------------------------------------------------------
HRESULT DXUTInitialize3DEnvironment() {
	HRESULT hr = S_OK;

	IDirect3DDevice9* pd3dDevice = DXUTGetD3DDevice();
	if (!pd3dDevice)
		return S_FALSE;

	GetDXUTState().SetDeviceObjectsCreated(false);
	GetDXUTState().SetDeviceObjectsReset(false);

	// Prepare the window for a possible change between windowed mode
	// and full screen mode by adjusting the window style and its menu.
	DXUTAdjustWindowStyle(DXUTGetHWND(), DXUTIsWindowed());

	// Prepare the device with cursor info and
	// store backbuffer desc and caps from the device
	DXUTPrepareDevice(pd3dDevice);

	// Call the GUI resource device created function
	hr = DXUTGetGlobalDialogResourceManager()->OnCreateDevice(pd3dDevice);
	if (FAILED(hr)) {
		if (hr == DXUTERR_MEDIANOTFOUND)
			return DXUT_ERR(_T("OnCreateDevice"), DXUTERR_MEDIANOTFOUND);
		else
			return DXUT_ERR(_T("OnCreateDevice"), DXUTERR_CREATINGDEVICEOBJECTS);
	}

	// Call the resource cache created function
	hr = DXUTGetGlobalResourceCache().OnCreateDevice(pd3dDevice);
	if (FAILED(hr)) {
		if (hr == DXUTERR_MEDIANOTFOUND)
			return DXUT_ERR(_T("OnCreateDevice"), DXUTERR_MEDIANOTFOUND);
		else
			return DXUT_ERR(_T("OnCreateDevice"), DXUTERR_CREATINGDEVICEOBJECTS);
	}

	// Call the app's device created callback if set
	const D3DSURFACE_DESC* pbackBufferSurfaceDesc = DXUTGetBackBufferSurfaceDesc();
	GetDXUTState().SetInsideDeviceCallback(true);
	LPDXUTCALLBACKDEVICECREATED pCallbackDeviceCreated = GetDXUTState().GetDeviceCreatedFunc();
	hr = S_OK;
	if (pCallbackDeviceCreated != NULL)
		hr = pCallbackDeviceCreated(pd3dDevice, pbackBufferSurfaceDesc);
	GetDXUTState().SetInsideDeviceCallback(false);
	if (FAILED(hr)) {
		// Cleanup upon failure
		DXUTCleanup3DEnvironment();

		DXUT_ERR(_T("DeviceCreated callback"), hr);
		if (hr == DXUTERR_MEDIANOTFOUND)
			return DXUT_ERR(_T("DeviceCreatedCallback"), DXUTERR_MEDIANOTFOUND);
		else
			return DXUT_ERR(_T("DeviceCreatedCallback"), DXUTERR_CREATINGDEVICEOBJECTS);
	} else {
		// Call the GUI resource device reset function
		hr = DXUTGetGlobalDialogResourceManager()->OnResetDevice();
		if (FAILED(hr))
			return DXUT_ERR(_T("OnResetDevice"), DXUTERR_RESETTINGDEVICEOBJECTS);

		// Call the resource cache device reset function
		hr = DXUTGetGlobalResourceCache().OnResetDevice(pd3dDevice);
		if (FAILED(hr))
			return DXUT_ERR(_T("OnResetDevice"), DXUTERR_RESETTINGDEVICEOBJECTS);

		// Call the app's device reset callback if set
		GetDXUTState().SetDeviceObjectsCreated(true);
		GetDXUTState().SetInsideDeviceCallback(true);
		LPDXUTCALLBACKDEVICERESET pCallbackDeviceReset = GetDXUTState().GetDeviceResetFunc();
		hr = S_OK;
		if (pCallbackDeviceReset != NULL)
			hr = pCallbackDeviceReset(pd3dDevice, pbackBufferSurfaceDesc);
		GetDXUTState().SetInsideDeviceCallback(false);
		if (FAILED(hr)) {
			DXUT_ERR(_T("DeviceReset callback"), hr);
			if (hr == DXUTERR_MEDIANOTFOUND)
				return DXUTERR_MEDIANOTFOUND;
			else
				return DXUTERR_RESETTINGDEVICEOBJECTS;
		} else {
			GetDXUTState().SetDeviceObjectsReset(true);
			return S_OK;
		}
	}
}

//--------------------------------------------------------------------------------------
// Resets the 3D environment by:
//      - Calls the device lost callback
//      - Stores the back buffer description
//      - Sets up the full screen Direct3D cursor if requested
//      - Calls the device reset callback
//--------------------------------------------------------------------------------------
HRESULT DXUTReset3DEnvironment() {
	HRESULT hr;

	IDirect3DDevice9* pd3dDevice = DXUTGetD3DDevice();
	if (!pd3dDevice)
		return S_FALSE;

	// Release all vidmem objects
	if (GetDXUTState().GetDeviceObjectsReset()) {
		GetDXUTState().SetInsideDeviceCallback(true);

		DXUTGetGlobalDialogResourceManager()->OnLostDevice();
		DXUTGetGlobalResourceCache().OnLostDevice();

		LPDXUTCALLBACKDEVICELOST pCallbackDeviceLost = GetDXUTState().GetDeviceLostFunc();
		if (pCallbackDeviceLost != NULL)
			pCallbackDeviceLost();
		GetDXUTState().SetDeviceObjectsReset(false);
		GetDXUTState().SetInsideDeviceCallback(false);
	}

	hr = DXUTGetGlobalDialogResourceManager()->OnResetDevice();
	if (FAILED(hr))
		return DXUT_ERR(_T("OnResetDevice"), DXUTERR_RESETTINGDEVICEOBJECTS);

	hr = DXUTGetGlobalResourceCache().OnResetDevice(pd3dDevice);
	if (FAILED(hr))
		return DXUT_ERR(_T("OnResetDevice"), DXUTERR_RESETTINGDEVICEOBJECTS);

	// Initialize the app's device-dependent objects
	GetDXUTState().SetInsideDeviceCallback(true);
	const D3DSURFACE_DESC* pbackBufferSurfaceDesc = DXUTGetBackBufferSurfaceDesc();
	LPDXUTCALLBACKDEVICERESET pCallbackDeviceReset = GetDXUTState().GetDeviceResetFunc();
	hr = S_OK;
	if (pCallbackDeviceReset != NULL)
		hr = pCallbackDeviceReset(pd3dDevice, pbackBufferSurfaceDesc);
	GetDXUTState().SetInsideDeviceCallback(false);
	if (FAILED(hr)) {
		DXUT_ERR(_T("DeviceResetCallback"), hr);
		if (hr != DXUTERR_MEDIANOTFOUND)
			hr = DXUTERR_RESETTINGDEVICEOBJECTS;

		DXUTGetGlobalDialogResourceManager()->OnLostDevice();
		DXUTGetGlobalResourceCache().OnLostDevice();

		LPDXUTCALLBACKDEVICELOST pCallbackDeviceLost = GetDXUTState().GetDeviceLostFunc();
		if (pCallbackDeviceLost != NULL)
			pCallbackDeviceLost();
	} else {
		// Success
		GetDXUTState().SetDeviceObjectsReset(true);
	}

	return hr;
}

//--------------------------------------------------------------------------------------
// Prepares a new or resetting device by with cursor info and
// store backbuffer desc and caps from the device
//--------------------------------------------------------------------------------------
void DXUTPrepareDevice(IDirect3DDevice9* pd3dDevice) {
	HRESULT hr;

	// Store render target surface desc
	IDirect3DSurface9* pBackBuffer;
	hr = pd3dDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer);
	D3DSURFACE_DESC* pbackBufferSurfaceDesc = GetDXUTState().GetBackBufferSurfaceDesc();
	ZeroMemory(pbackBufferSurfaceDesc, sizeof(D3DSURFACE_DESC));
	if (SUCCEEDED(hr)) {
		pBackBuffer->GetDesc(pbackBufferSurfaceDesc);
		SAFE_RELEASE(pBackBuffer);
	}

	// Update GetDXUTState()'s copy of D3D caps
	D3DCAPS9* pd3dCaps = GetDXUTState().GetCaps();
	pd3dDevice->GetDeviceCaps(pd3dCaps);

	// Set up the cursor
	bool showCursor = true; //GetDXUTState().GetShowCursor();
	if (showCursor) {
		// if the windows cursor should be used, or if there was a problem setting the cursor from a texture
		if (GetDXUTState().GetUseWindowsCursor() ||
			FAILED(DXUTSetDeviceCursor(pd3dDevice, GetDXUTState().GetCursorFilename(), GetDXUTState().GetCursorHotSpotPoint()))) {
			HCURSOR hCursor;
#ifdef _WIN64
			hCursor = (HCURSOR)GetClassLongPtr(DXUTGetHWND(), GCLP_HCURSOR);
#else
			hCursor = (HCURSOR)ULongToHandle(GetClassLong(DXUTGetHWND(), GCL_HCURSOR));
#endif
			DXUTSetDeviceCursor(pd3dDevice, hCursor, false);
		}
	}

	// Confine cursor to full screen window
	if (GetDXUTState().GetClipCursorWhenFullScreen()) {
		if (!DXUTIsWindowed()) {
			RECT rcWindow;
			GetWindowRect(DXUTGetHWND(), &rcWindow);
			ClipCursor(&rcWindow);
		} else {
			ClipCursor(NULL);
		}
	}
}

//--------------------------------------------------------------------------------------
// Pauses time or rendering.  Keeps a ref count so pausing can be layered
//--------------------------------------------------------------------------------------
void DXUTPause(bool bPauseTime, bool bPauseRendering) {
	int nPauseTimeCount = GetDXUTState().GetPauseTimeCount();
	nPauseTimeCount += (bPauseTime ? +1 : -1);
	if (nPauseTimeCount < 0)
		nPauseTimeCount = 0;
	GetDXUTState().SetPauseTimeCount(nPauseTimeCount);

	int nPauseRenderingCount = GetDXUTState().GetPauseRenderingCount();
	nPauseRenderingCount += (bPauseRendering ? +1 : -1);
	if (nPauseRenderingCount < 0)
		nPauseRenderingCount = 0;
	GetDXUTState().SetPauseRenderingCount(nPauseRenderingCount);

	if (nPauseTimeCount > 0) {
		// Stop the scene from animating
		DXUTGetGlobalTimer()->Stop();
	} else {
		// Restart the timer
		DXUTGetGlobalTimer()->Start();
	}

	GetDXUTState().SetRenderingPaused(nPauseRenderingCount > 0);
	GetDXUTState().SetTimePaused(nPauseTimeCount > 0);
}

#if 0
//--------------------------------------------------------------------------------------
// Checks if the window client rect has changed and if it has, then reset the device
//--------------------------------------------------------------------------------------
void DXUTHandlePossibleSizeChange()
{
    if( !GetDXUTState().GetDeviceCreated() || GetDXUTState().GetIgnoreSizeChange() )
        return;

    DXUTDeviceSettings* pDeviceSettings = GetDXUTState().GetCurrentDeviceSettings();
    if( false == pDeviceSettings->pp.Windowed )
        return;

    HRESULT hr = S_OK;
    RECT rcClientOld = GetDXUTState().GetWindowClientRect();

    // Update window properties
    RECT rcWindowClient;
    GetClientRect( DXUTGetHWNDDeviceWindowed(), &rcWindowClient );
    GetDXUTState().SetWindowClientRect( rcWindowClient );

    RECT rcWindowBounds;
    GetWindowRect( DXUTGetHWNDDeviceWindowed(), &rcWindowBounds );
    GetDXUTState().SetWindowBoundsRect( rcWindowBounds );

    // Check if the window client rect has changed
    if( rcClientOld.right - rcClientOld.left != rcWindowClient.right - rcWindowClient.left ||
        rcClientOld.bottom - rcClientOld.top != rcWindowClient.bottom - rcWindowClient.top )
    {
        // A new window size will require a new backbuffer
        // size, so the 3D structures must be changed accordingly.
        DXUTPause( true, true );

        pDeviceSettings = GetDXUTState().GetCurrentDeviceSettings();
        pDeviceSettings->pp.BackBufferWidth  = rcWindowClient.right - rcWindowClient.left;
        pDeviceSettings->pp.BackBufferHeight = rcWindowClient.bottom - rcWindowClient.top;

        // Reset the 3D environment
        IDirect3DDevice9* pd3dDevice = DXUTGetD3DDevice();
        if( pd3dDevice )
        {
            if( FAILED( hr = DXUTReset3DEnvironment() ) )
            {
                if( D3DERR_DEVICELOST == hr )
                {
                    // The device is lost, so wait until it can be reset
                    GetDXUTState().SetDeviceLost( true );
                }
                else if( DXUTERR_RESETTINGDEVICEOBJECTS == hr || 
                         DXUTERR_MEDIANOTFOUND == hr )
                {
                    DXUTDisplayErrorMessage( hr );
                    DXUTShutdown();
                    return;
                }
                else // DXUTERR_RESETTINGDEVICE
                {
                    // Reset failed, but the device wasn't lost so something bad happened, 
                    // so recreate the device to try to recover
                    pDeviceSettings = GetDXUTState().GetCurrentDeviceSettings();
                    if( FAILED( DXUTChangeDevice( pDeviceSettings, NULL, true ) ) )
                    {
                        DXUTShutdown();
                        return;
                    }
                }
            }
        }

        DXUTPause( false, false );
    }

    DXUTCheckForWindowMonitorChange();
}
#endif

//--------------------------------------------------------------------------------------
// Checks if the window client rect has changed but does not reset the device
//--------------------------------------------------------------------------------------
void DXUTHandlePossibleSizeChangeWithoutResetDevice() {
	if (!GetDXUTState().GetDeviceCreated() || GetDXUTState().GetIgnoreSizeChange())
		return;

	DXUTDeviceSettings* pDeviceSettings = GetDXUTState().GetCurrentDeviceSettings();
	if (false == pDeviceSettings->pp.Windowed)
		return;

	//HRESULT hr = S_OK;
	RECT rcClientOld = GetDXUTState().GetWindowClientRect();

	// Update window properties
	RECT rcWindowClient;
	GetClientRect(DXUTGetHWNDDeviceWindowed(), &rcWindowClient);
	GetDXUTState().SetWindowClientRect(rcWindowClient);

	RECT rcWindowBounds;
	GetWindowRect(DXUTGetHWNDDeviceWindowed(), &rcWindowBounds);
	GetDXUTState().SetWindowBoundsRect(rcWindowBounds);

	// Check if the window client rect has changed
	if (rcClientOld.right - rcClientOld.left != rcWindowClient.right - rcWindowClient.left ||
		rcClientOld.bottom - rcClientOld.top != rcWindowClient.bottom - rcWindowClient.top) {
		// A new window size will require a new backbuffer
		// size, so the 3D structures must be changed accordingly.
		DXUTPause(true, true);

		pDeviceSettings = GetDXUTState().GetCurrentDeviceSettings();
		pDeviceSettings->pp.BackBufferWidth = rcWindowClient.right - rcWindowClient.left;
		pDeviceSettings->pp.BackBufferHeight = rcWindowClient.bottom - rcWindowClient.top;

		DXUTPause(false, false);
	}
}

//--------------------------------------------------------------------------------------
// Prepare the window for a possible change between windowed mode and full screen mode
// by adjusting the window style and its menu.
//--------------------------------------------------------------------------------------
void DXUTAdjustWindowStyle(HWND hWnd, bool bWindowed) {
	if (bWindowed) {
		// If different device windows are used for windowed mode and fullscreen mode,
		// hide the fullscreen window so that it doesn't obscure the screen.
		if (GetDXUTState().GetHWNDDeviceFullScreen() != GetDXUTState().GetHWNDDeviceWindowed()) {
			::ShowWindow(GetDXUTState().GetHWNDDeviceFullScreen(), SW_HIDE);
		}

		// Set windowed-mode style
		SetWindowLong(hWnd, GWL_STYLE, GetDXUTState().GetWinStyle());
		if (GetDXUTState().GetMenu() != NULL) {
			SetMenu(hWnd, GetDXUTState().GetMenu());
		}
	} else {
		// If different device windows are used for windowed mode and fullscreen mode,
		// restore and show the fullscreen device window.
		if (GetDXUTState().GetHWNDDeviceFullScreen() != GetDXUTState().GetHWNDDeviceWindowed()) {
			if (::IsIconic(GetDXUTState().GetHWNDDeviceFullScreen()))
				::ShowWindow(GetDXUTState().GetHWNDDeviceFullScreen(), SW_RESTORE);
			::ShowWindow(GetDXUTState().GetHWNDDeviceFullScreen(), SW_SHOW);
		}

		// Set full screen mode style
		SetWindowLong(hWnd, GWL_STYLE, WS_POPUP | WS_SYSMENU | WS_VISIBLE);
		if (GetDXUTState().GetMenu()) {
			HMENU hMenu = GetMenu(hWnd);
			GetDXUTState().SetMenu(hMenu);
			SetMenu(hWnd, NULL);
		}
	}
}

LRESULT CALLBACK DXUTStaticWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// DRF - Set Static MsgProcInfo
	MsgProcInfo mpi(hWnd, uMsg, wParam, lParam);

	// Consolidate the keyboard messages and pass them to the app's keyboard callback
	bool isKeyDown = mpi.IsKeyDown();
	bool isKeyUp = mpi.IsKeyUp();
	if (isKeyDown || isKeyUp) {
		DWORD dwMask = (1 << 29);
		bool bAltDown = ((lParam & dwMask) != 0);

		bool* bKeys = GetDXUTState().GetKeys();
		bKeys[(BYTE)(wParam & 0xFF)] = isKeyDown;

		LPDXUTCALLBACKKEYBOARD pCallbackKeyboard = GetDXUTState().GetKeyboardFunc();
		if (pCallbackKeyboard)
			pCallbackKeyboard((UINT)wParam, isKeyDown, bAltDown);
	}

	// Consolidate the mouse button messages and pass them to the app's mouse callback
	bool isMouseMove = mpi.IsMouseMove();
	bool isMouseWheel = mpi.IsMouseWheel();
	bool isMouse = (mpi.IsMouse() && !(isMouseMove && !GetDXUTState().GetNotifyOnMouseMove()));
	if (isMouse) {
		int xPos = mpi.CursorPoint().x;
		int yPos = mpi.CursorPoint().y;

		if (isMouseWheel) {
			// WM_MOUSEWHEEL passes screen mouse coords
			// so convert them to client coords
			POINT pt;
			pt.x = xPos;
			pt.y = yPos;
			ScreenToClient(hWnd, &pt);
			xPos = pt.x;
			yPos = pt.y;
		}

		int nMouseWheelDelta = 0;
		if (isMouseWheel)
			nMouseWheelDelta = (short)HIWORD(wParam);

		int nMouseButtonState = LOWORD(wParam);
		bool bLeftButton = ((nMouseButtonState & MK_LBUTTON) != 0);
		bool bRightButton = ((nMouseButtonState & MK_RBUTTON) != 0);
		bool bMiddleButton = ((nMouseButtonState & MK_MBUTTON) != 0);
		bool bSideButton1 = ((nMouseButtonState & MK_XBUTTON1) != 0);
		bool bSideButton2 = ((nMouseButtonState & MK_XBUTTON2) != 0);

		bool* bMouseButtons = GetDXUTState().GetMouseButtons();
		bMouseButtons[0] = bLeftButton;
		bMouseButtons[1] = bMiddleButton;
		bMouseButtons[2] = bRightButton;
		bMouseButtons[3] = bSideButton1;
		bMouseButtons[4] = bSideButton2;

		LPDXUTCALLBACKMOUSE pCallbackMouse = GetDXUTState().GetMouseFunc();
		if (pCallbackMouse)
			pCallbackMouse(bLeftButton, bRightButton, bMiddleButton, bSideButton1, bSideButton2, nMouseWheelDelta, xPos, yPos);
	}

	// Pass all messages to the app's MsgProc callback, and don't
	// process further messages if the apps says not to.
	LPDXUTCALLBACKMSGPROC pCallbackMsgProc = GetDXUTState().GetWindowMsgFunc();
	if (pCallbackMsgProc) {
		bool bNoFurtherProcessing = false;
		LRESULT nResult = pCallbackMsgProc(hWnd, uMsg, wParam, lParam, &bNoFurtherProcessing);
		if (bNoFurtherProcessing)
			return nResult;
	}

	switch (uMsg) {
		case WM_SIZE:
			// Pick up possible changes to window style due to maximize, etc.
			if (DXUTIsWindowed() && DXUTGetHWND() != NULL)
				GetDXUTState().SetWinStyle(GetWindowLong(DXUTGetHWND(), GWL_STYLE));

			DXUTHandlePossibleSizeChangeWithoutResetDevice();

			if (SIZE_MINIMIZED == wParam) {
				if (GetDXUTState().GetClipCursorWhenFullScreen() && !DXUTIsWindowed())
					ClipCursor(NULL);
				DXUTPause(true, true); // Pause while we're minimized
				GetDXUTState().SetMinimized(true);
				GetDXUTState().SetMaximized(false);
			} else if (SIZE_MAXIMIZED == wParam) {
				if (GetDXUTState().GetMinimized())
					DXUTPause(false, false); // Unpause since we're no longer minimized
				GetDXUTState().SetMinimized(false);
				GetDXUTState().SetMaximized(true);
				DXUTHandlePossibleSizeChangeWithoutResetDevice();
			} else if (SIZE_RESTORED == wParam) {
				if (GetDXUTState().GetMaximized()) {
					GetDXUTState().SetMaximized(false);
					DXUTHandlePossibleSizeChangeWithoutResetDevice();
				} else if (GetDXUTState().GetMinimized()) {
					DXUTPause(false, false); // Unpause since we're no longer minimized
					GetDXUTState().SetMinimized(false);
					DXUTHandlePossibleSizeChangeWithoutResetDevice();
				} else {
					// If we're neither maximized nor minimized, the window size
					// is changing by the user dragging the window edges.  In this
					// case, we don't reset the device yet -- we wait until the
					// user stops dragging, and a WM_EXITSIZEMOVE message comes.
				}
			}
			break;

		case WM_GETMINMAXINFO:
			((MINMAXINFO*)lParam)->ptMinTrackSize.x = MIN_WINDOW_SIZE_X;
			((MINMAXINFO*)lParam)->ptMinTrackSize.y = MIN_WINDOW_SIZE_Y;
			break;

		case WM_ENTERSIZEMOVE:
			// Halt frame movement while the app is sizing or moving
			DXUTPause(true, true);
			break;

		case WM_EXITSIZEMOVE:
			DXUTPause(false, false);
			DXUTHandlePossibleSizeChangeWithoutResetDevice();
			break;

		case WM_SETCURSOR:
			// Turn off Windows cursor
			if (!DXUTIsRenderingPaused() && !GetDXUTState().GetUseWindowsCursor() && DXUTGetDeviceCaps()->CursorCaps != 0) {
				// if fullscreen, or if the cursor is within the client area of the window
				if (!DXUTIsWindowed() || ((short)LOWORD(lParam) == HTCLIENT)) {
					SetCursor(NULL);
					IDirect3DDevice9* pd3dDevice = DXUTGetD3DDevice();
					if (pd3dDevice && GetDXUTState().GetShowCursor())
						pd3dDevice->ShowCursor(true);
					return true; // prevent Windows from setting cursor to window class cursor
				}
			}
			break;

		case WM_ACTIVATEAPP:
			if (wParam == TRUE)
				GetDXUTState().SetActive(true);
			else
				GetDXUTState().SetActive(false);
			break;
	}

	return CallWindowProc((WNDPROC)g_oldWinProc, hWnd, uMsg, wParam, lParam);
}

void DXUTShutdown() {
	HWND hWnd = DXUTGetHWND();
	if (hWnd != NULL)
		SetWindowLongPtr(hWnd, GWLP_WNDPROC, (LONG)g_oldWinProc);

	DXUTCleanup3DEnvironment(true);

	GetDXUTState().SetD3DEnumeration(NULL);

	GetDXUTState().SetD3D(NULL);
}

//--------------------------------------------------------------------------------------
// Cleans up the 3D environment by:
//      - Calls the device lost callback
//      - Calls the device destroyed callback
//      - Releases the D3D device
// MJS 6/10/05 - Changed to only call OnLostDevice() nothing more
//--------------------------------------------------------------------------------------
void DXUTCleanup3DEnvironment(bool bReleaseSettings) {
	// Stop crash on close after freeing this elsewhere.  --Jonny
	DXUTGetGlobalDialogResourceManager()->OnLostDevice();
	DXUTGetGlobalResourceCache().OnLostDevice();

	if (bReleaseSettings) {
		DXUTGetGlobalDialogResourceManager()->OnDestroyDevice();
		DXUTGetGlobalResourceCache().OnDestroyDevice();
	}
}

//--------------------------------------------------------------------------------------
// External state access functions
//--------------------------------------------------------------------------------------
IDirect3D9* DXUTGetD3DObject() {
	return GetDXUTState().GetD3D();
}
IDirect3DDevice9* DXUTGetD3DDevice() {
	return GetDXUTState().GetD3DDevice();
}
const D3DSURFACE_DESC* DXUTGetBackBufferSurfaceDesc() {
	return GetDXUTState().GetBackBufferSurfaceDesc();
}
const D3DCAPS9* DXUTGetDeviceCaps() {
	return GetDXUTState().GetCaps();
}
HWND DXUTGetHWND() {
	return DXUTIsWindowed() ? GetDXUTState().GetHWNDDeviceWindowed() : GetDXUTState().GetHWNDDeviceFullScreen();
}
HWND DXUTGetHWNDFocus() {
	return GetDXUTState().GetHWNDFocus();
}
HWND DXUTGetHWNDDeviceFullScreen() {
	return GetDXUTState().GetHWNDDeviceFullScreen();
}
HWND DXUTGetHWNDDeviceWindowed() {
	return GetDXUTState().GetHWNDDeviceWindowed();
}
const RECT& DXUTGetWindowClientRect() {
	return GetDXUTState().GetWindowClientRect();
}
double DXUTGetTime() {
	return GetDXUTState().GetTime();
}
float DXUTGetElapsedTime() {
	return GetDXUTState().GetElapsedTime();
}
float DXUTGetFPS() {
	return GetDXUTState().GetFPS();
}
LPCTSTR DXUTGetWindowTitle() {
	return GetDXUTState().GetWindowTitle();
}
bool DXUTIsRenderingPaused() {
	return GetDXUTState().GetPauseRenderingCount() > 0;
}

void DXUTSetCursorSettings(bool bShowCursor, bool bClipCursorWhenFullScreen, bool bUseWindowsCursor) {
	GetDXUTState().SetClipCursorWhenFullScreen(bClipCursorWhenFullScreen);
	GetDXUTState().SetShowCursor(bShowCursor);
	GetDXUTState().SetUseWindowsCursor(bUseWindowsCursor);
}

void DXUTSetCursorTexture(const char* sCursorFilename, POINT hotSpot) {
	wchar_t* pstrCursorFilename = GetDXUTState().GetCursorFilename();
	wcsncpy(pstrCursorFilename, Utf8ToUtf16(sCursorFilename).c_str(), 256);
	pstrCursorFilename[255] = 0;

	GetDXUTState().SetCursorHotSpotPoint(hotSpot);

	IDirect3DDevice9* pd3dDevice = DXUTGetD3DDevice();

	if (pd3dDevice != NULL)
		DXUTPrepareDevice(pd3dDevice);
}

bool DXUTIsWindowed() {
	DXUTDeviceSettings* pDeviceSettings = GetDXUTState().GetCurrentDeviceSettings();
	if (pDeviceSettings)
		return (pDeviceSettings->pp.Windowed != 0);
	else
		return false;
}

//--------------------------------------------------------------------------------------
// Return the present params of the current device.  If no device exists yet, then
// return blank present params
//--------------------------------------------------------------------------------------
D3DPRESENT_PARAMETERS DXUTGetPresentParameters() {
	DXUTDeviceSettings* pDS = GetDXUTState().GetCurrentDeviceSettings();
	if (pDS) {
		return pDS->pp;
	} else {
		D3DPRESENT_PARAMETERS pp;
		ZeroMemory(&pp, sizeof(D3DPRESENT_PARAMETERS));
		return pp;
	}
}

//--------------------------------------------------------------------------------------
// Return the device settings of the current device.  If no device exists yet, then
// return blank device settings
//--------------------------------------------------------------------------------------
DXUTDeviceSettings DXUTGetDeviceSettings() {
	DXUTDeviceSettings* pDS = GetDXUTState().GetCurrentDeviceSettings();
	if (pDS) {
		return *pDS;
	} else {
		DXUTDeviceSettings ds;
		ZeroMemory(&ds, sizeof(DXUTDeviceSettings));
		return ds;
	}
}

//--------------------------------------------------------------------------------------
// Display an custom error msg box
//--------------------------------------------------------------------------------------
void DXUTDisplayErrorMessage(HRESULT hr) {
	wchar_t strBuffer[512];

	int nExitCode;
	bool bFound = true;
	switch (hr) {
		case DXUTERR_NODIRECT3D:
			nExitCode = 2;
			_tcsncpy(strBuffer, _T("Could not initialize Direct3D. You may want to check that the latest version of DirectX is correctly installed on your system.  Also make sure that this program was compiled with header files that match the installed DirectX DLLs."), 512);
			break;
		case DXUTERR_INCORRECTVERSION:
			nExitCode = 10;
			_tcsncpy(strBuffer, _T("Incorrect version of Direct3D and/or D3DX."), 512);
			break;
		case DXUTERR_MEDIANOTFOUND:
			nExitCode = 4;
			_tcsncpy(strBuffer, _T("Could not find required media. Ensure that the DirectX SDK is correctly installed."), 512);
			break;
		case DXUTERR_NONZEROREFCOUNT:
			nExitCode = 5;
			_tcsncpy(strBuffer, _T("The D3D device has a non-zero reference count, meaning some objects were not released."), 512);
			break;
		case DXUTERR_CREATINGDEVICE:
			nExitCode = 6;
			_tcsncpy(strBuffer, _T("Failed creating the Direct3D device."), 512);
			break;
		case DXUTERR_RESETTINGDEVICE:
			nExitCode = 7;
			_tcsncpy(strBuffer, _T("Failed resetting the Direct3D device."), 512);
			break;
		case DXUTERR_CREATINGDEVICEOBJECTS:
			nExitCode = 8;
			_tcsncpy(strBuffer, _T("Failed creating Direct3D device objects."), 512);
			break;
		case DXUTERR_RESETTINGDEVICEOBJECTS:
			nExitCode = 9;
			_tcsncpy(strBuffer, _T("Failed resetting Direct3D device objects."), 512);
			break;
		case DXUTERR_SWITCHEDTOREF:
			nExitCode = 0;
			_tcsncpy(strBuffer, _T("Switching to the reference rasterizer,\na software device that implements the entire\nDirect3D feature set, but runs very slowly."), 512);
			break;
		case DXUTERR_NOCOMPATIBLEDEVICES:
			nExitCode = 3;
			if (GetSystemMetrics(SM_REMOTESESSION) != 0)
				_tcsncpy(strBuffer, _T("Direct3D does not work over a remote session."), 512);
			else
				_tcsncpy(strBuffer, _T("Could not find any compatible Direct3D devices."), 512);
			break;
		default:
			bFound = false;
			nExitCode = 1;
			break;
	}
	strBuffer[511] = 0;

	GetDXUTState().SetExitCode(nExitCode);

	bool bShowMsgBoxOnError = GetDXUTState().GetShowMsgBoxOnError();
	if (bFound && bShowMsgBoxOnError) {
		if (DXUTGetWindowTitle()[0] == 0)
			MessageBox(DXUTGetHWND(), strBuffer, _T("DirectX Application"), MB_ICONERROR | MB_OK);
		else
			MessageBox(DXUTGetHWND(), strBuffer, DXUTGetWindowTitle(), MB_ICONERROR | MB_OK);
	}
}

//--------------------------------------------------------------------------------------
// Display error msg box to help debug
//--------------------------------------------------------------------------------------
HRESULT WINAPI DXUTTrace(const char* strFile, DWORD dwLine, HRESULT hr,
	LPCTSTR strMsg, bool bPopMsgBox) {
	bool bShowMsgBoxOnError = GetDXUTState().GetShowMsgBoxOnError();
	if (bPopMsgBox && bShowMsgBoxOnError == false)
		bPopMsgBox = false;

	return DXTrace(strFile, dwLine, hr, strMsg, bPopMsgBox);
}

} // namespace KEP