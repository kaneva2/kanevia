///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../imenugui.h"
#include "MenuCInterface.h"
#include "LuaHelper.h"
#include "LogHelper.h"

namespace script_glue {

/******************************************************************************
 * create_vm<lua_State>
 *   Initializes the Lua interpreter
 ******************************************************************************/
template <>
inline lua_State *create_vm<lua_State>() {
	return LUA_OPEN();
}

/******************************************************************************
 * destroy_vm<lua_State>
 *   Closes the Lua interpreter
 ******************************************************************************/
template <>
inline void destroy_vm<lua_State>(lua_State *L) {
	lua_close(L);
}

/******************************************************************************
 * my_dofile
 *   Helper function to load a Lua script file and run it
 ******************************************************************************/
int my_dofile(lua_State *L, int nresults, const char *name);

/******************************************************************************
 * load_file<lua_State>
 *   Loads a Lua script file and runs it
 ******************************************************************************/
template <>
inline bool load_file<lua_State>(lua_State *L, const std::string &filename) {
	return my_dofile(L, 1, filename.c_str()) == 0;
}

/** DRF
 * Returns lua stack trace as a string.
 */
std::string Lua_GetErrorStr(lua_State *L);

/******************************************************************************
 * error_null_parameter
 *   Called when there is a null object where the glue code expects a valid object.
 ******************************************************************************/
void error_null_parameter(lua_State *L, int pos, const char *expectedTypeStr);

/******************************************************************************
 * error_type_mismatch
 *   Called when runtime type of parameter coming in from Lua doesn't match
 *   what the glue code expects to find.
 ******************************************************************************/
void error_type_mismatch(lua_State *L, int pos, const char *expectedTypeStr);

/******************************************************************************
 * get_parameter<T,S>
 *   Base template for get_parameter functions.  Functions should return
 *   false if for whatever reason they cannot get the parameter.
 ******************************************************************************/
template <typename T, typename S>
bool get_parameter(T *, S *, int);

/******************************************************************************
 * get_parameter<null_parameter, lua_State>
 ******************************************************************************/
template <>
inline bool get_parameter<null_parameter, lua_State>(null_parameter * /*param*/, lua_State * /*L*/, int /*pos*/) {
	// getting null_parameter always succeeds
	return true;
}

/******************************************************************************
 * get_parameter<int, lua_State>
 ******************************************************************************/
template <>
inline bool get_parameter<int, lua_State>(int *param, lua_State *L, int pos) {
	if (lua_isnumber(L, pos)) {
		*param = (int)lua_tonumber(L, pos);
		return true;
	} else if (lua_isboolean(L, pos)) {
		// to support 'C' style booleans
		*param = lua_toboolean(L, pos) ? 1 : 0;
		return true;
	} else {
		error_type_mismatch(L, pos, "int");
		return false;
	}
}

/******************************************************************************
 * get_parameter<float, lua_State>
 ******************************************************************************/
template <>
inline bool get_parameter<float, lua_State>(float *param, lua_State *L, int pos) {
	if (lua_isnumber(L, pos)) {
		*param = (float)lua_tonumber(L, pos);
		return true;
	} else {
		error_type_mismatch(L, pos, "float");
		return false;
	}
}

/******************************************************************************
 * get_parameter<const char *, lua_State>
 ******************************************************************************/
template <>
inline bool get_parameter<const char *, lua_State>(const char **param, lua_State *L, int pos) {
	if (lua_isstring(L, pos)) {
		*param = lua_tostring(L, pos);
		return true;
	} else {
		int t = lua_type(L, pos);

		if (t != LUA_TNIL) {
			error_type_mismatch(L, pos, "string");
			return false;
		} else {
			*param = "<nil>";
			return true;
		}
	}
}

/******************************************************************************
 * get_parameter<unsigned int, lua_State>
 ******************************************************************************/
template <>
inline bool get_parameter<unsigned int, lua_State>(unsigned int *param, lua_State *L, int pos) {
	if (lua_isnumber(L, pos)) {
		*param = (unsigned int)lua_tonumber(L, pos);
		return true;
	} else {
		error_type_mismatch(L, pos, "unsigned int");
		return false;
	}
}

/******************************************************************************
 * get_parameter<unsigned long, lua_State>
 ******************************************************************************/
template <>
inline bool get_parameter<unsigned long, lua_State>(unsigned long *param, lua_State *L, int pos) {
	if (lua_isnumber(L, pos)) {
		*param = (unsigned long)lua_tonumber(L, pos);
		return true;
	} else {
		error_type_mismatch(L, pos, "unsigned long");
		return false;
	}
}

/******************************************************************************
 * get_parameter<long, lua_State>
 ******************************************************************************/
template <>
inline bool get_parameter<long, lua_State>(long *param, lua_State *L, int pos) {
	if (lua_isnumber(L, pos)) {
		*param = (long)lua_tonumber(L, pos);
		return true;
	} else {
		error_type_mismatch(L, pos, "long");
		return false;
	}
}

/******************************************************************************
 * get_object_parameter
 ******************************************************************************/
template <typename T>
inline bool get_object_parameter(T **param, lua_State *L, int pos) {
	void **fud = static_cast<void **>(lua_touserdata(L, pos));

	if (fud && *fud) {
		WeakRef *weakRef = *reinterpret_cast<WeakRef **>(fud);

		if (weakRef) {
			T *object = dynamic_cast<T *>(weakRef->_target);

			if (object) {
				*param = object;
				return true;
			} else {
				error_null_parameter(L, pos, typeid(T).name());
				return false;
			}
		} else {
			error_null_parameter(L, pos, typeid(T).name());
			return false;
		}
	} else {
		error_type_mismatch(L, pos, typeid(T).name());
		return false;
	}
}

/******************************************************************************
 * get_parameter<IMenuDialog, lua_State>
 ******************************************************************************/
template <>
inline bool get_parameter<KEP::IMenuDialog *, lua_State>(KEP::IMenuDialog **param, lua_State *L, int pos) {
	// DRF - Validate Reference Pointer (IMenuDialog*)
	bool ok = get_object_parameter(param, L, pos);
	if (ok)
		ok &= REF_PTR_VALID(*param);
	if (!ok) {
		LogInstance("Script");
		LogError("Lua Script Error...\n"
				 << "Invalid Parameter IMenuDialog " << *param);
	}
	return ok;
}

/******************************************************************************
 * get_parameter<IMenuControl, lua_State>
  ******************************************************************************/
template <>
inline bool get_parameter<KEP::IMenuControl *, lua_State>(KEP::IMenuControl **param, lua_State *L, int pos) {
	return get_object_parameter(param, L, pos);
}

/******************************************************************************
 * get_parameter<IMenuStatic, lua_State>
 ******************************************************************************/
template <>
inline bool get_parameter<KEP::IMenuStatic *, lua_State>(KEP::IMenuStatic **param, lua_State *L, int pos) {
	return get_object_parameter(param, L, pos);
}

/******************************************************************************
 * get_parameter<IMenuRadioButton, lua_State>
 ******************************************************************************/
template <>
inline bool get_parameter<KEP::IMenuRadioButton *, lua_State>(KEP::IMenuRadioButton **param, lua_State *L, int pos) {
	return get_object_parameter(param, L, pos);
}

/******************************************************************************
 * get_parameter<IMenuImage, lua_State>
 ******************************************************************************/
template <>
inline bool get_parameter<KEP::IMenuImage *, lua_State>(KEP::IMenuImage **param, lua_State *L, int pos) {
	return get_object_parameter(param, L, pos);
}

/******************************************************************************
 * get_parameter<IMenuButton, lua_State>
  ******************************************************************************/
template <>
inline bool get_parameter<KEP::IMenuButton *, lua_State>(KEP::IMenuButton **param, lua_State *L, int pos) {
	return get_object_parameter(param, L, pos);
}

/******************************************************************************
 * get_parameter<IMenuCheckBox, lua_State>
  ******************************************************************************/
template <>
inline bool get_parameter<KEP::IMenuCheckBox *, lua_State>(KEP::IMenuCheckBox **param, lua_State *L, int pos) {
	return get_object_parameter(param, L, pos);
}

/******************************************************************************
 * get_parameter<IMenuListBox, lua_State>
  ******************************************************************************/
template <>
inline bool get_parameter<KEP::IMenuListBox *, lua_State>(KEP::IMenuListBox **param, lua_State *L, int pos) {
	return get_object_parameter(param, L, pos);
}

/******************************************************************************
 * get_parameter<IMenuScrollBar, lua_State>
  ******************************************************************************/
template <>
inline bool get_parameter<KEP::IMenuScrollBar *, lua_State>(KEP::IMenuScrollBar **param, lua_State *L, int pos) {
	return get_object_parameter(param, L, pos);
}

/******************************************************************************
 * get_parameter<IMenuComboBox, lua_State>
  ******************************************************************************/
template <>
inline bool get_parameter<KEP::IMenuComboBox *, lua_State>(KEP::IMenuComboBox **param, lua_State *L, int pos) {
	return get_object_parameter(param, L, pos);
}

/******************************************************************************
 * get_parameter<IMenuSlider, lua_State>
  ******************************************************************************/
template <>
inline bool get_parameter<KEP::IMenuSlider *, lua_State>(KEP::IMenuSlider **param, lua_State *L, int pos) {
	return get_object_parameter(param, L, pos);
}

/******************************************************************************
 * get_parameter<IMenuEditBox, lua_State>
  ******************************************************************************/
template <>
inline bool get_parameter<KEP::IMenuEditBox *, lua_State>(KEP::IMenuEditBox **param, lua_State *L, int pos) {
	return get_object_parameter(param, L, pos);
}

/******************************************************************************
 * get_parameter<IFlashPlayerControl, lua_State>
  ******************************************************************************/
template <>
inline bool get_parameter<KEP::IFlashPlayerControl *, lua_State>(KEP::IFlashPlayerControl **param, lua_State *L, int pos) {
	return get_object_parameter(param, L, pos);
}

/******************************************************************************
 * get_parameter<IMenuBlendColor, lua_State>
  ******************************************************************************/
template <>
inline bool get_parameter<KEP::IMenuBlendColor *, lua_State>(KEP::IMenuBlendColor **param, lua_State *L, int pos) {
	return get_object_parameter(param, L, pos);
}

/******************************************************************************
 * get_parameter<IMenuElement, lua_State>
  ******************************************************************************/
template <>
inline bool get_parameter<KEP::IMenuElement *, lua_State>(KEP::IMenuElement **param, lua_State *L, int pos) {
	return get_object_parameter(param, L, pos);
}

/******************************************************************************
 * get_parameter<COLOR, lua_State>
  ******************************************************************************/
template <>
inline bool get_parameter<COLOR, lua_State>(COLOR *param, lua_State *L, int pos) {
	int r = 0, g = 0, b = 0, a = 0xff;

	// Validate existence of table on stack at pos
	if (!lua_istable(L, pos)) {
		error_type_mismatch(L, pos, "color");
		return false;
	}

	// Get values of table r, g, b, and a elements

	lua_pushstring(L, "r");
	lua_gettable(L, pos);
	r = (int)lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_pushstring(L, "g");
	lua_gettable(L, pos);
	g = (int)lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_pushstring(L, "b");
	lua_gettable(L, pos);
	b = (int)lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_pushstring(L, "a");
	lua_gettable(L, pos);
	a = (int)lua_tonumber(L, -1);
	lua_pop(L, 1);

	// Unpack table members into fields of 'param'
	if (r < 0)
		r = 0;
	if (r > 255)
		r = 255;
	param->r = (unsigned char)r;
	if (g < 0)
		g = 0;
	if (g > 255)
		g = 255;
	param->g = (unsigned char)g;
	if (b < 0)
		b = 0;
	if (b > 255)
		b = 255;
	param->b = (unsigned char)b;
	if (a < 0)
		a = 0;
	if (a > 255)
		a = 255;
	param->a = (unsigned char)a;

	return true;
}

/******************************************************************************
 * push_parameter<int, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<int, lua_State>(lua_State *L, int t) {
	lua_pushnumber(L, t);
}

/******************************************************************************
 * push_parameter<const char *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<const char *, lua_State>(lua_State *L, const char *t) {
	lua_pushstring(L, t);
}

/******************************************************************************
 * push_parameter<float, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<float, lua_State>(lua_State *L, float t) {
	lua_pushnumber(L, t);
}

/******************************************************************************
 * push_parameter<unsigned int, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<unsigned int, lua_State>(lua_State *L, unsigned int t) {
	lua_pushnumber(L, t);
}

/******************************************************************************
 * push_parameter<unsigned long, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<unsigned long, lua_State>(lua_State *L, unsigned long t) {
	lua_pushnumber(L, (lua_Number)t);
}

/******************************************************************************
 * push_parameter<bool, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<bool, lua_State>(lua_State *L, bool b) {
	lua_pushboolean(L, b);
}

/******************************************************************************
 * push_parameter<null_parameter, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<null_parameter, lua_State>(lua_State * /*L*/, null_parameter /*p*/) {
	// pushing a null_parameter is a no-op
}

/******************************************************************************
 * WeakRefGC
 ******************************************************************************/
int WeakRefGC(lua_State *L);

/******************************************************************************
 * WeakRefToString
 ******************************************************************************/
int WeakRefToString(lua_State *L);

/******************************************************************************
 * WeakRefEq
 ******************************************************************************/
int WeakRefEq(lua_State *L);

/******************************************************************************
 * push_object_parameter
 ******************************************************************************/
template <typename T>
inline void push_object_parameter(lua_State *L, T *obj) {
	if (obj) {
		// get registry[weak ref address]
		lua_pushlightuserdata(L, obj->GetWeakRef());
		lua_gettable(L, LUA_REGISTRYINDEX);

		if (lua_touserdata(L, -1) != NULL) {
			// FUD for this object already exists in registry
			// and is on top of stack now so nothing else needs to be done here

#ifdef _DEBUG
			// Do some sanity checking on the WeakRef value
			WeakRef *weakRef = *static_cast<WeakRef **>(lua_touserdata(L, -1));

			_ASSERTE(weakRef != NULL);

			if (weakRef)
				_ASSERTE(weakRef->_target == obj);
#endif
		} else {
			lua_pop(L, 1); // pop nil from the stack

			// FUD for this object doesn't exist, create a new one and store it in registry
			WeakRef **pWeakRef = static_cast<WeakRef **>(lua_newuserdata(L, sizeof(WeakRef *)));

			if (pWeakRef) {
				// Get or create the metatable named 'weakref'
				if (luaL_newmetatable(L, "weakref") != 0) {
					// Table didn't exist before, need to initialize it

					// metatable["__gc"] = WeakRefGC
					lua_pushstring(L, "__gc");
					lua_pushcfunction(L, WeakRefGC);
					lua_settable(L, -3);

					// metatable["__tostring"] = WeakRefToString
					lua_pushstring(L, "__tostring");
					lua_pushcfunction(L, WeakRefToString);
					lua_settable(L, -3);

					// metatable["__eq"] = WeakRefEq
					lua_pushstring(L, "__eq");
					lua_pushcfunction(L, WeakRefEq);
					lua_settable(L, -3);
				}

				// FUD.metatable = weak ref metatable
				lua_setmetatable(L, -2);

				// Store pointer to weak ref in the FUD
				*pWeakRef = obj->GetWeakRef();

				if (*pWeakRef)
					(*pWeakRef)->_nref++; // Increment weak ref VM reference count
			}

			// registry[weak ref address] = FUD
			lua_pushlightuserdata(L, obj->GetWeakRef());
			lua_pushvalue(L, -2);
			lua_settable(L, LUA_REGISTRYINDEX);
		}
	} else {
		// 'obj' parameter was NULL
		lua_pushnil(L);
	}
}

/******************************************************************************
 * push_parameter<IMenuDialog *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IMenuDialog *, lua_State>(lua_State *L, KEP::IMenuDialog *pDialog) {
	push_object_parameter<KEP::IMenuDialog>(L, pDialog);
}

/******************************************************************************
 * push_parameter<IMenuControl *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IMenuControl *, lua_State>(lua_State *L, KEP::IMenuControl *pControl) {
	push_object_parameter<KEP::IMenuControl>(L, pControl);
}

/******************************************************************************
 * push_parameter<IMenuStatic *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IMenuStatic *, lua_State>(lua_State *L, KEP::IMenuStatic *pStatic) {
	push_object_parameter<KEP::IMenuStatic>(L, pStatic);
}

/******************************************************************************
 * push_parameter<IMenuRadioButton *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IMenuRadioButton *, lua_State>(lua_State *L, KEP::IMenuRadioButton *pRadioButton) {
	push_object_parameter<KEP::IMenuRadioButton>(L, pRadioButton);
}

/******************************************************************************
 * push_parameter<IMenuImage *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IMenuImage *, lua_State>(lua_State *L, KEP::IMenuImage *pImage) {
	push_object_parameter<KEP::IMenuImage>(L, pImage);
}

/******************************************************************************
 * push_parameter<IMenuButton *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IMenuButton *, lua_State>(lua_State *L, KEP::IMenuButton *pButton) {
	push_object_parameter<KEP::IMenuButton>(L, pButton);
}

/******************************************************************************
 * push_parameter<IMenuCheckBox *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IMenuCheckBox *, lua_State>(lua_State *L, KEP::IMenuCheckBox *pCheckBox) {
	push_object_parameter<KEP::IMenuCheckBox>(L, pCheckBox);
}

/******************************************************************************
 * push_parameter<IMenuListBox *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IMenuListBox *, lua_State>(lua_State *L, KEP::IMenuListBox *pListBox) {
	push_object_parameter<KEP::IMenuListBox>(L, pListBox);
}

/******************************************************************************
 * push_parameter<IMenuScrollBar *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IMenuScrollBar *, lua_State>(lua_State *L, KEP::IMenuScrollBar *pScrollBar) {
	push_object_parameter<KEP::IMenuScrollBar>(L, pScrollBar);
}

/******************************************************************************
 * push_parameter<IMenuComboBox *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IMenuComboBox *, lua_State>(lua_State *L, KEP::IMenuComboBox *pComboBox) {
	push_object_parameter<KEP::IMenuComboBox>(L, pComboBox);
}

/******************************************************************************
 * push_parameter<IMenuSlider *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IMenuSlider *, lua_State>(lua_State *L, KEP::IMenuSlider *pSlider) {
	push_object_parameter<KEP::IMenuSlider>(L, pSlider);
}

/******************************************************************************
 * push_parameter<IMenuEditBox *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IMenuEditBox *, lua_State>(lua_State *L, KEP::IMenuEditBox *pEditBox) {
	push_object_parameter<KEP::IMenuEditBox>(L, pEditBox);
}

/******************************************************************************
 * push_parameter<IFlashPlayerControl *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IFlashPlayerControl *, lua_State>(lua_State *L, KEP::IFlashPlayerControl *pFlash) {
	push_object_parameter<KEP::IFlashPlayerControl>(L, pFlash);
}

/******************************************************************************
 * push_parameter<IMenuBlendColor *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IMenuBlendColor *, lua_State>(lua_State *L, KEP::IMenuBlendColor *pBlendColor) {
	push_object_parameter<KEP::IMenuBlendColor>(L, pBlendColor);
}

/******************************************************************************
 * push_parameter<IMenuElement *, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<KEP::IMenuElement *, lua_State>(lua_State *L, KEP::IMenuElement *pElement) {
	push_object_parameter<KEP::IMenuElement>(L, pElement);
}

/******************************************************************************
 * push_parameter<COLOR, lua_State>
 ******************************************************************************/
template <>
inline void push_parameter<COLOR, lua_State>(lua_State *L, COLOR color) {
	// Create new table on Lua stack
	lua_newtable(L);

	// Set r element
	lua_pushstring(L, "r");
	lua_pushnumber(L, color.r);
	lua_settable(L, -3);

	// Set g element
	lua_pushstring(L, "g");
	lua_pushnumber(L, color.g);
	lua_settable(L, -3);

	// Set b element
	lua_pushstring(L, "b");
	lua_pushnumber(L, color.b);
	lua_settable(L, -3);

	// Set a element
	lua_pushstring(L, "a");
	lua_pushnumber(L, color.a);
	lua_settable(L, -3);
}

} // namespace script_glue
