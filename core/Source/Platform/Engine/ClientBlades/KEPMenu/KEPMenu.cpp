///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "dxstdafx.h"

#include "js.h"
#include "jsEnumFiles.h"
#include <js.h>

#include "LogHelper.h"

#include "KEPMenu.h"
#include "KEPFileNames.h"

#include "common/include/KEPCommon.h"

#include "..\..\Event\ClientEvents.h"

#include "Common/KEPUtil/Helpers.h"
#include <TinyXML/tinyxml.h>
#include "Common/include/LuaHelper.h"

#define GET_MENU_FN_EVENT "MenuFunctions"

namespace KEP {

extern "C" void RegisterScriptFunctionsForVM(IScriptVM* state);

static LogInstance("Instance");
static const LPCSTR DIALOG_TAG = "dialog";
static KEPMenu* s_pKepMenu = nullptr; // KEPMenu singleton instance
static bool g_cursorInit = false;

KEPMenu* GetKepMenu() {
	if (s_pKepMenu)
		return s_pKepMenu;
	s_pKepMenu = new KEPMenu();
	return s_pKepMenu;
}

static bool _VALID_DIALOG(const IMenuDialog* pDialog) {
	return (pDialog && REF_PTR_VALID(pDialog) && pDialog->IsFullyConstructed());
}

static bool VALID_DIALOG(const IMenuDialog* pDialog) {
	return (_VALID_DIALOG(pDialog) && !pDialog->IsPendingDeletion());
}

static bool VALID_DIALOG_STABLE(const IMenuDialog* pDialog) {
	return (_VALID_DIALOG(pDialog) && !pDialog->IsPendingDeletion());
}

static bool VALID_DIALOG_DELETED(const IMenuDialog* pDialog) {
	return (_VALID_DIALOG(pDialog) && pDialog->IsPendingDeletion());
}

#define FOR_EACH_DIALOG(pDialog)            \
	for (const auto& pDialog : m_dialogs) { \
		if (!VALID_DIALOG(pDialog))         \
			continue;

#define FOR_EACH_DIALOG_STABLE(pDialog)           \
	for (const auto& pDialog : m_dialogsStable) { \
		if (!VALID_DIALOG_STABLE(pDialog))        \
			continue;

#define FOR_EACH_DIALOG_DELETED(pDialog)           \
	for (const auto& pDialog : m_dialogsDeleted) { \
		if (!VALID_DIALOG_DELETED(pDialog))        \
			continue;

#define FOR_EACH_DIALOG_REVERSE(pDialog, dialogList)                           \
	for (auto iter = dialogList.rbegin(); iter != dialogList.rend(); ++iter) { \
		auto pDialog = *iter;                                                  \
		if (!VALID_DIALOG(pDialog))                                            \
			continue;

#define FOR_EACH_DIALOG_HIT(pDialog, dialogList) \
	for (const auto& pDialog : dialogList) {     \
		if (!_VALID_DIALOG(pDialog))             \
			continue;

#define FOR_EACH_DIALOG_END }

const std::string KEPMenu::m_sName = "KEP Menu";
KEPMenu* KEPMenu::m_pThis = 0;

#define TestGroupKepMenu() (g_testGroup & DRF_TEST_GROUP_KEP_MENU)
static int g_testGroup = DRF_TEST_GROUP_OFF;
void KEPMenu::SetTestGroup(int testGroup) {
	g_testGroup = testGroup;
}

/** DRF
* Return Path ...\Star\<gameId>\MapsModels\Common
*/
static std::string GetPathMapsModelsCommon() {
	// Only Get This Once
	static std::string mmcPath;
	if (!mmcPath.empty())
		return mmcPath;

	// Get Path ...\Star\<gameId>\MapsModels\Common
	wchar_t path[MAX_PATH] = L"";
	GetCurrentDirectoryW(MAX_PATH, path);
	mmcPath = PathAdd(Utf16ToUtf8(path), "MapsModels\\Common");
	return mmcPath;
}

EVENT_PROC_RC GetMenuFunctionHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	GetFunctionEvent* gfe = dynamic_cast<GetFunctionEvent*>(e);
	if (!gfe)
		return EVENT_PROC_RC::NOT_PROCESSED;

	std::string name, vmName;
	ULONG vm;
	(*gfe->InBuffer()) >> name >> vmName >> vm;
	if (name == GET_MENU_FN_EVENT && vm != 0) {
		IScriptVM* state = (IScriptVM*)UintToPtr(vm);
		RegisterScriptFunctionsForVM(state);
		return EVENT_PROC_RC::CONSUMED;
	}

	return EVENT_PROC_RC::NOT_PROCESSED;
}

KEPMenu::KEPMenu() :
		m_timeOnRenderSec(0),
		m_dialogsDirty(true),
		m_alwaysBringDialogToFront(nullptr),
		m_cursorTypeSet(false),
		m_cursorMenu(nullptr) {
	m_pThis = this;
	IClientRenderBlade::m_bladeDepth = ClearsZ;
}

KEPMenu::~KEPMenu() {
	DXUTShutdown();
}

bool KEPMenu::Initialize() {
	TiXmlBase::SetCondenseWhiteSpace(false);

	IDirect3DDevice9* pd3dDevice = m_engine->GetD3DDevice();
	HWND hWnd = m_engine->GetWindowHandle();

	IClientEngine* clientEngine = dynamic_cast<IClientEngine*>(m_engine);
	jsAssert(clientEngine != NULL);
	m_pathBase = clientEngine->PathBase();

	std::string pathMenu = PathFind(m_pathBase, "GameFiles\\Menus");
	LogInfo("pathMenu='" << LogPath(pathMenu) << "'");

	std::string pathScript = PathFind(m_pathBase, "GameFiles\\MenuScripts");
	LogInfo("pathScript='" << LogPath(pathScript) << "'");

	// Load script checksums
	LuaLoadScriptInfo(pathScript);
	LuaLoadScriptInfo(PathAdd(PathAdd(pathScript, ".."), SCRIPTDIR));
	LuaLoadScriptInfo(PathAdd(PathAdd(pathScript, ".."), SCRIPTDATADIR));

	// Temporarily allow menus to include a script under ClientScripts folder
	LuaLoadScriptInfo(PathAdd(PathAdd(pathScript, ".."), CLIENTSCRIPTSDIR));

	// Add Menu Paths (also add textures)
	AddMenuPaths(pathMenu, pathScript, true, true);

	// Default menu directory (WOK menus)
	IMenuDialog::SetDefaultMenuDir(pathMenu);

	if (FAILED(DXUTInit()))
		return false;

	if (FAILED(DXUTSetWindow(hWnd, true)))
		return false;

	// Initialize DirectX Cursor Only Once
	if (!g_cursorInit) {
		DXUTSetCursorSettings(true, true, false);
		g_cursorInit = true;
	}

	// Reset Cursor Mode And Types
	ResetCursorModesTypes();

	if (FAILED(DXUTSetDeviceWithoutCreatingWindow(pd3dDevice)))
		return false;

	// scripting interface
	Handlers::OnInit();

	DXUTSetCallbackMsgProc(MsgProc);
	DXUTSetCallbackKeyboard(KeyboardProc);

	// add our events and handler
	assert(m_engine != 0);
	if (m_engine) {
		IDispatcher* dispatcher = m_engine->GetDispatcher();
		assert(dispatcher != 0);
		if (dispatcher) {
			IEvent::SetEncodingFactory(dispatcher->EncodingFactory());
			dispatcher->RegisterEvent(GET_MENU_FN_EVENT, GetFunctionEvent::ClassVersion(), GetFunctionEvent::CreateEvent, 0, GetFunctionEvent::ClassPriority());
			dispatcher->RegisterHandler(GetMenuFunctionHandler, (ULONG)PtrToUint(this), "GetMenuFunctions", GetFunctionEvent::ClassVersion(), dispatcher->GetEventId(GET_MENU_FN_EVENT));
		}
	}

	// DRF - Added - Set Translate To Language
	auto ttLang = clientEngine->AppTTLang();
	DXUT_TranslateToSetLang(ttLang);

	return true;
}

bool KEPMenu::Destroy() {
	return false;
}

void KEPMenu::DialogsSetEdit(bool bEdit) {
	FOR_EACH_DIALOG_REVERSE(pDialog, m_dialogs)
	pDialog->SetEdit(bEdit);
	FOR_EACH_DIALOG_END
}

std::string KEPMenu::DialogToStr(IMenuDialog* pDialog) const {
	std::string str;
	if (!REF_PTR_VALID(pDialog)) {
		StrBuild(str, "Dialog<" << pDialog << "> REF_PTR_INVALID");
	} else {
		std::string dialogStr = pDialog->ToStr();
		std::string frontStr = (pDialog == m_alwaysBringDialogToFront) ? " FRONT" : "";
		StrBuild(str, dialogStr << frontStr);
	}
	return str;
}

size_t KEPMenu::DialogsOpen() const {
	return m_dialogs.size();
}

bool KEPMenu::LogAllDialogs(bool verbose) {
	// Log All Dialogs (active, stable, and deleted)
	auto dialogs = m_dialogs.size();
	auto dialogsStable = m_dialogsStable.size();
	auto dialogsDeleted = m_dialogsDeleted.size();
	LogInfo("dialogs=" << dialogs << " dialogsStable=" << dialogsStable << " dialogsDeleted=" << dialogsDeleted);
	if (verbose && dialogs) {
		for (const auto& pDialog : m_dialogs) {
			LogInfo(" ... " << DialogToStr(pDialog));
		}
	}
	if (verbose && dialogsStable && !StlIsEqual(m_dialogsStable, m_dialogs)) {
		LogInfo("=== dialogsStable ===");
		for (const auto& pDialog : m_dialogsStable) {
			LogInfo(" ... " << DialogToStr(pDialog));
		}
	}
	if (verbose && dialogsDeleted && !StlIsEqual(m_dialogsDeleted, m_dialogs)) {
		LogInfo("=== dialogsDeleted ===");
		for (const auto& pDialog : m_dialogsDeleted) {
			LogInfo(" ... " << DialogToStr(pDialog));
		}
	}
	return true;
}

const char* KEPMenu::DialogMenuId(IMenuDialog* pDialog) {
	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog))
		return "DIALOG_MENU_ID_INVALID";

	// Return Menu Identifier
	return pDialog->GetMenuIdPtr();
}

const char* KEPMenu::DialogMenuName(IMenuDialog* pDialog) {
	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog))
		return "DIALOG_MENU_NAME_INVALID";

	// Return Normalized Menu Name (without '.xml')
	return pDialog->GetMenuFileNamePtr();
}

const char* KEPMenu::DialogScriptName(IMenuDialog* pDialog) {
	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog))
		return "DIALOG_SCRIPT_NAME_INVALID";

	// Return Normalized Script Name (without '.lua')
	return pDialog->GetScriptFileNamePtr();
}

int KEPMenu::GetDialogID(const std::string& menuName) {
	// just read the file for the ID for performance
	std::string filepath;
	if (SearchFile(menuName, false, false, filepath)) {
		std::ifstream f;
		f.open(filepath.c_str());
		if (f.is_open()) {
			while (!f.eof()) {
				char s[4096];
				f.getline(s, 4096);
				char* p = 0;
				if ((p = strstr(s, "<ID>")) != 0)
					return atoi(p + 4);
			}
			return -1;
		}
	}
	return -1;
}

// DRF - Now Returns AB Variant Matches Without Looping From CommonFunctions::GetDialogOrVariantOpenByName().
IMenuDialog* KEPMenu::GetDialog(const std::string& menuName) {
	IMenuDialog* pDialogMatch = NULL;

	// Valid Dialogs To Search ?
	if (m_dialogs.empty())
		return NULL;

	// Valid Menu Name ?
	if (menuName.empty())
		return NULL;

	// Match Only File Name (without path or '_ab#' or extension)
	std::string menuNameToFind = StrFileNameNoAB(menuName);

	// Fast - Find Easy Single Match By Id
	size_t matches = 0;
	FOR_EACH_DIALOG(pDialog)
	std::string dialogName = StrFileNameNoAB(pDialog->GetMenuId());
	if (StrSameFilePath(menuNameToFind, dialogName)) {
		++matches;
		if (matches >= 2)
			break;
		pDialogMatch = pDialog;
	}
	FOR_EACH_DIALOG_END

	if (matches < 2)
		return pDialogMatch;

	// Slow - Find Search Path Match By Full Path
	std::lock_guard<fast_recursive_mutex> pathsLock(m_pathsMutex);
	for (const auto& it : m_paths) {
		std::string menuPathToFind = PathAdd(it.menuPath, menuNameToFind);
		FOR_EACH_DIALOG(pDialog)
		std::string dialogFilePath = StrStripFileExtAB(pDialog->GetMenuFilePath());
		if (StrSameFilePath(menuPathToFind, dialogFilePath))
			return pDialog;
		FOR_EACH_DIALOG_END
	}

	return pDialogMatch; // Path Orphaned Dialog
}

IMenuDialog* KEPMenu::DialogNew() {
	// Create New Empty Dialog
	try {
		CDXUTDialog* pDialog = new CDXUTDialog("<NewDialog>", false);
		DialogInsertToFront(pDialog);
		pDialog->SetCallback(OnDialogGUIEvent);
		pDialog->SetCallback(OnControlGUIEvent);
		return pDialog;
	} catch (...) {
		LogError("Caught Exception(...)");
		return 0;
	}
}

IMenuDialog* KEPMenu::DialogCreateAs(const std::string& menuName, const std::string& menuId, bool editNow, bool trustedOnly) {
	// Valid Menu Xml File Name?
	if (menuId.empty() || menuName.empty() || !StrSameFileExt(menuName, "xml")) {
		LogError("Invalid Dialog Filename '" << menuName << "'");
		return 0;
	}

	// Search For Menu File Path With Matching Menu Name
	std::string menuFilePath;
	bool trustedMenu = false;
	if (!SearchFile(menuName, false, trustedOnly, menuFilePath, &trustedMenu)) {
		// Only Log Error For Non AB Test Menus
		if (!STLContainsIgnoreCase(menuName, "_ab"))
			LogError("SearchFile(" << menuName << ") FAILED");
		return 0;
	}

	// Load Menu Xml Into New Dialog
	IMenuDialog* pDialog = LoadMenuXml(menuFilePath, editNow, trustedMenu);
	if (!pDialog) {
		LogError("LoadMenuXml(" << LogPath(menuFilePath) << ") FAILED");
		return 0;
	}

	// Assign Menu Id
	pDialog->SetMenuId(menuId);

	// Assign Event Callbacks
	pDialog->SetCallback(OnDialogGUIEvent);
	pDialog->SetCallback(OnControlGUIEvent);

	// Set If Menu Being Edited
	pDialog->SetEdit(editNow);

	// Call Script::OnCreate()
	if (!editNow)
		Handlers::OnCreate(pDialog);

	// Apply Input Settings
	ApplyInputSettings();

	// Log Newly Created Dialog OK
	LogInfo(DialogToStr(pDialog) << " OK");

	return pDialog;
}

IMenuDialog* KEPMenu::DialogCreate(const std::string& menuName, bool editNow, bool trustedOnly) {
	return DialogCreateAs(menuName, menuName, editNow, trustedOnly);
}

bool KEPMenu::DialogExitAll() {
	// For All Dialogs Call Lua Script OnExit()
	FOR_EACH_DIALOG(pDialog)
	Handlers::OnExit(pDialog);
	FOR_EACH_DIALOG_END

	return true;
}

bool KEPMenu::DialogDestroy(IMenuDialog* pIMenuDialog) {
	// Valid Dialog ?
	if (!pIMenuDialog || !VALID_DIALOG(pIMenuDialog))
		return false;

	// Disable If Always To Front Dialog
	if (pIMenuDialog == m_alwaysBringDialogToFront)
		DialogAlwaysBringToFront(pIMenuDialog, false);

	// Already Being Deleted ?
	// DRF - TODO - Remove This (Should Never Succeed)
	FOR_EACH_DIALOG_DELETED(pDialog)
	if (pDialog == pIMenuDialog) {
		// Remove From m_dialogs[] (do not add to m_deletedDialogs[])
		DialogRemove(pIMenuDialog, false);
		LogError("FIX THIS - Already Deleting - Ignoring!");
		return false; // duplicate; don't add!
	}
	FOR_EACH_DIALOG_END

	// Call Lua Script OnDestroy()
	Handlers::OnDestroy(pIMenuDialog);

	// Log Newly Destroyed Dialog OK
	LogInfo(DialogToStr(pIMenuDialog) << " OK");

	// Remove From m_dialogs[] (add to m_deletedDialogs[])
	DialogRemove(pIMenuDialog, true);

	// Update Input Settings
	ApplyInputSettings();

	return true;
}

bool KEPMenu::DialogDestroyAllExcept(IMenuDialog* pDialog) {
	// Find Excepted Dialog
	if (!pDialog) {
		LogInfo("Dialog <NULL> [All Dialogs]");
	} else if (DialogFind(pDialog)) {
		LogInfo(DialogToStr(pDialog));
	} else {
		LogInfo("Dialog <" << pDialog << "> [Dialog Not Open]");
	}

	// Destroy All Dialogs (one at a time skipping the excepted)
	while (!m_dialogs.empty()) {
		dialog_iter_t iterFirst = m_dialogs.begin();
		auto pDialogFirst = *iterFirst;
		if (pDialogFirst != pDialog)
			DialogDestroy(pDialogFirst);
		else {
			dialog_iter_t iterSecond = ++iterFirst;
			if (iterSecond != m_dialogs.end()) {
				auto pDialogSecond = *iterSecond;
				DialogDestroy(pDialogSecond);
			} else
				break; // all done
		}
	}

	return true;
}

// TODO - PJD - the optional includeFramework parameter here is intended to be temporary - ED-7184
bool KEPMenu::DialogDestroyAllNonEssentialExcept(IMenuDialog* pDialogEx, BOOL includeFramework) {
	// Find Excepted Dialog
	if (!pDialogEx) {
		LogInfo("Dialog <NULL> [All Non-Essential Dialogs]");
	} else if (DialogFind(pDialogEx)) {
		LogInfo(DialogToStr(pDialogEx));
	} else {
		LogInfo("Dialog <" << pDialogEx << "> [Dialog Not Open]");
	}

	// Destroy All Non-Essential Dialogs (one at a time skipping the excepted)
	static const std::string essentialMenus[] = {
		//		"DevConsole",
		//		"ChatMenu",
		"NotificationsBar",
		"HUDKaneva"
	};

	// This should be moved elsewhere
	static const std::string frameworkMenus[] = {
		"Framework_PlayerHandler",
		"Framework_BattleHandler",
		"Framework_BottomHitBox",
		"Framework_BottomRightHUD",
		"Framework_ContextualHelp",
		"Framework_Cursor_HighlightHandler",
		"Framework_DragDropHandler",
		"Framework_GameItemEditor",
		"Framework_Indicators",
		"Framework_InventoryHandler",
		"Framework_MapDataHandler",
		"Framework_MapIcon",
		"Framework_MediaFlagHandler",
		"Framework_PlayerBuild",
		"Framework_QuestCompass",
		"Framework_StatusIcons",
		"Framework_Toolbar",
		"Framework_Tooltip",
		"Framework_TopRightHUD",
		"HUDBuild"
	};

	IMenuDialog* pDialogDestroy;
	do {
		pDialogDestroy = NULL;
		FOR_EACH_DIALOG(pDialog)

		// Excepted Dialog ?
		if (pDialog == pDialogEx)
			continue;

		// Valid FileName?
		std::string fileName = StrFileNameNoAB(pDialog->GetMenuFileName());
		if (fileName.empty())
			continue;

		// Essential Dialog ?
		pDialogDestroy = pDialog;
		for (size_t i = 0; i < _countof(essentialMenus); ++i) {
			if (StrSameFilePath(fileName, essentialMenus[i])) {
				pDialogDestroy = NULL;
				break;
			}
		}

		if (includeFramework) {
			for (size_t i = 0; i < _countof(frameworkMenus); ++i) {
				if (StrSameFilePath(fileName, frameworkMenus[i])) {
					pDialogDestroy = NULL;
					break;
				}
			}
		}

		if (pDialogDestroy)
			break;
		FOR_EACH_DIALOG_END

		// Destroy Dialog
		if (pDialogDestroy)
			DialogDestroy(pDialogDestroy);

	} while (pDialogDestroy);

	return true;
}

void KEPMenu::DialogsClearDeleted() {
	// Delete Dialogs In Delete List
	while (!m_dialogsDeleted.empty()) {
		auto pDialog = *(m_dialogsDeleted.begin());
		m_dialogsDeleted.pop_front();

		// Valid Dialog Deleted ?
		if (!VALID_DIALOG_DELETED(pDialog))
			continue;

		// Remove From g_map[]
		Handlers::OnDelete(pDialog);

		// Delete Menu Xml, Lua VM, & Resources
		pDialog->Delete();

		m_dialogsDirty = true;
	}

	//Update the stable list of dialogs
	if (m_dialogsDirty) {
		m_dialogsStable.clear();
		m_dialogsStable.insert(m_dialogsStable.begin(), m_dialogs.begin(), m_dialogs.end());
		m_dialogsDirty = false;
	}
}

// DEPRECATED - Use MakeWebCall() Instead
bool KEPMenu::GetBrowserPage(IMenuDialog* pDialog, const char* url, int filter, int startIndex, int maxItems) {
	// Valid Dialog & File ?
	if (!VALID_DIALOG(pDialog))
		return false;
	if (!url || !url[0])
		return false;

	LogWarn("DEPRECATED - Use KEPMenu::MakeWebCall() Instead!");

	// if this menu is associated with a child game then certain web calls are not allowed
	if (!pDialog->IsTrustedMenu()) {
		// if the url contains a page in the blacklist, do not make the call
		if (m_engine->ChildGameWebCallAllowed(url))
			m_engine->GetBrowserPageAsync(url, NULL, filter, startIndex, maxItems);
		else {
			LogError("Web call not allowed from this menu: " << url);
			return false;
		}
	} else {
		// not from child game menu, make the call
		m_engine->GetBrowserPageAsync(url, NULL, filter, startIndex, maxItems);
	}

	return true;
}

bool KEPMenu::MakeWebCall(const char* url, int filter, int startIndex, int maxItems) {
	if (!url || !url[0] || !m_engine)
		return false;
	m_engine->GetBrowserPageAsync(url, NULL, filter, startIndex, maxItems);
	return true;
}

bool KEPMenu::IsModalDialogOpen() {
	FOR_EACH_DIALOG(pDialog)
	if (pDialog->IsActuallyModal())
		return true;
	FOR_EACH_DIALOG_END
	return false;
}

IMenuDialog* KEPMenu::LoadMenuXml(const std::string& xmlFilePath, bool editable, bool trustedMenu) {
	// Load Menu XML
	TiXmlDocument xml_doc;
	if (!xml_doc.LoadFile(xmlFilePath, TIXML_ENCODING_LEGACY)) {
		LogError("FAILED LoadFile() " << xml_doc.ErrorDesc() << " '" << LogPath(xmlFilePath) << "'");
		return NULL;
	}

	// First <dialog> Tag Is Root Node
	TiXmlElement* root = xml_doc.FirstChildElement(DIALOG_TAG);
	if (!root) {
		LogError("FAILED FirstChildElement(dialog) '" << LogPath(xmlFilePath) << "'");
		return NULL;
	}

	// Try Loading Menu XML Resources
	IMenuDialog* pDialog = NULL;
	try {
		// Create New Menu Dialog
		pDialog = new CDXUTDialog(xmlFilePath, trustedMenu);

		pDialog->SetEdit(editable);

		// Load All Menu Resources (lua script, textures, etc)
		// NOTE: This only returns false if the lua script fails to load (if there is no lua file, it won't fail)
		if (!pDialog->LoadMenuXmlResources(root)) {
			LogError("FAILED LoadXmlMenuResources() '" << LogPath(xmlFilePath) << "'");
			pDialog->Delete();
			return NULL;
		}

		// Set XML File Path
		if (root->GetDocument())
			pDialog->SetMenuFilePath(root->GetDocument()->Value());

		// Modal Dialogs Go In Front All Others In Back
		if (pDialog->IsActuallyModal())
			DialogInsertToFront(pDialog);
		else
			DialogInsertToBack(pDialog);

		// Reformat Dialog And Apply Focus To Default Control
		pDialog->ApplyLocationFormatting();
		pDialog->FocusDefaultControl();

		return pDialog;
	} catch (...) {
		LogError("FAILED Uncaught Exception '" << LogPath(xmlFilePath) << "'");
		if (pDialog)
			pDialog->Delete();
		return NULL;
	}
}

bool KEPMenu::DialogSave(IMenuDialog* pDialog, const std::string& filename) {
	// Valid Dialog & File ?
	if (!VALID_DIALOG(pDialog) || filename.empty())
		return false;

	TiXmlDocument xml_doc;
	xml_doc.InsertEndChild(TiXmlDeclaration("1.0", "UTF-8", "yes"));
	pDialog->Save(&xml_doc);
	std::string filepath = m_engine->getDevToolAssetPath(ScriptServerEvent::MenuXml);
	filepath += filename;
	return xml_doc.SaveFile(filepath);
}

bool KEPMenu::DialogCopyTemplate(IMenuDialog* pDialog) {
	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog))
		return false;

	std::string filepath = m_engine->getDevToolAssetPath(ScriptServerEvent::MenuScript);
	filepath += pDialog->GetScriptFileName();

	//s should be at 'GameFiles\Menus\..\Scriptdata\Menu.lua'
	std::string pathMenuLua = PathAdd(IMenuDialog::GetDefaultMenuDir(), "..\\ScriptData\\menu.lua");

	return (CopyFileW(Utf8ToUtf16(pathMenuLua).c_str(), Utf8ToUtf16(filepath).c_str(), TRUE) != FALSE);
}

bool KEPMenu::DialogRegisterGenericEvents(IMenuDialog* pDialog) {
	// Valid Dialog?
	if (!VALID_DIALOG(pDialog))
		return false;

	// Get Menu Script VM
	IScriptVM* vm = GetScriptVMFromDialog(pDialog);
	if (!vm)
		return false;

	// Get Menu Script File Path
	std::string scriptFilePath = GetMenuScriptFilePathFromDialog(pDialog);
	if (vm->GetVM() != 0)
		m_engine->RegisterVMsEvents(vm, scriptFilePath.c_str());
	delete vm;

	return true;
}

bool KEPMenu::DialogUnRegisterGenericEvents(IMenuDialog* pDialog) {
	// Valid Dialog?
	if (!VALID_DIALOG(pDialog))
		return false;

	// call Handler
	IScriptVM* vm = GetScriptVMFromDialog(pDialog);
	if (!vm)
		return false;

	if (vm->GetVM() != 0)
		m_engine->UnRegisterVMsEvents(vm);
	delete vm;

	return true;
}

bool KEPMenu::ApplyInputSettings() {
	// Restrict All Movement While Modal Menu Is Open
	bool dialogIsModal = IsModalDialogOpen();
	m_engine->SetMenuSelectMode(dialogIsModal);
	m_engine->setMovementRestriction(IClientEngine::MR_SRC_MENU, IClientEngine::RestrictMovement_All, dialogIsModal);

	return true;
}

void KEPMenu::OnLostDevice() {
	DXUTCleanup3DEnvironment(false);
}

void KEPMenu::OnResetDevice() {
	if (DXUTReset3DEnvironment() != S_OK)
		return;

	LogInfo("START");

	HWND hWnd = m_engine->GetWindowHandle();
	DXUTSetWindow(hWnd, false);
	DXUTPrepareDevice(m_engine->GetD3DDevice());

	// DRF - Bug Fix
	// ClearDeletedDialogs() so we are using fresh stable dialogs.  OnResetDevice() happens when
	// the client window is maximized from being minimized and we don't want to ApplyLocationFormatting()
	// to the dialogs that were open when the user minimized the client but to the dialogs that are now
	// open.
	DialogsClearDeleted();

	// Log All Dialogs (not verbose, just the counts)
	LogAllDialogs(false);

	// Apply Location Formatting To Stable Dialogs Not Being Edited
	FOR_EACH_DIALOG_STABLE(pDialog)
	if (pDialog->GetEdit())
		continue;
	bool ok = pDialog->ApplyLocationFormatting();
	LogInfo("ApplyLocationFormatting(" << LogBool(ok) << ") " << DialogToStr(pDialog));
	FOR_EACH_DIALOG_END

	LogInfo("END");
}

void KEPMenu::Render() {
	// Update OnRender Time
	// DRF - Moved From OnUpdate()
	m_timeOnRenderSec = m_timerOnRender.ElapsedSec();
	m_timerOnRender.Reset();

	// Call OnBeginFrame for all stable dialogs
	FOR_EACH_DIALOG_STABLE(pDialog)
	pDialog->OnBeginFrame();
	FOR_EACH_DIALOG_END

	// Limit Script::OnRender Calls (under 60hz)
	bool scriptOnRender = false;
	TimeMs timeOnRenderScriptMs = m_timerOnRenderScript.ElapsedMs();
	TimeSec timeOnRenderScriptSec = timeOnRenderScriptMs / MS_PER_SEC;
	const TimeMs timeOnRenderScriptMinMs = (TimeMs)15.0;
	if (timeOnRenderScriptMs >= (m_renderOptions.m_onRenderTimeMs + timeOnRenderScriptMinMs)) {
		m_timerOnRenderScript.Reset();
		scriptOnRender = true;
	}

	// First Call OnRender() For All Non Top-Most Stable Dialogs
	IMenuDialog* pTopmostDialog = NULL;
	FOR_EACH_DIALOG_STABLE(pDialog)
	if (!pDialog->IsTopmost()) {
		pDialog->OnRender(m_timeOnRenderSec);
		if (scriptOnRender)
			Handlers::OnRender(pDialog, timeOnRenderScriptSec);
	} else {
		pTopmostDialog = pDialog;
	}
	FOR_EACH_DIALOG_END

	// Then Call OnRender() For Top-Most Dialog
	if (pTopmostDialog) {
		pTopmostDialog->OnRender(m_timeOnRenderSec);
		if (scriptOnRender)
			Handlers::OnRender(pTopmostDialog, timeOnRenderScriptSec);
	}

	// Call OnEndFrame for all stable dialogs
	FOR_EACH_DIALOG_STABLE(pDialog)
	pDialog->OnEndFrame();
	FOR_EACH_DIALOG_END
}

void KEPMenu::OnUpdate() {
	//	// Update OnRender Time
	//	m_timeOnRenderSec = m_timerOnRender.ElapsedSec();
	//	m_timerOnRender.Reset();

	// Clear Dialogs In Deleted List (frees menu xmls, lua vms, & resources)
	DialogsClearDeleted();

	// Window Refresh Is Needed ?
	if (NeedsWindowRefresh()) {
		LogInfo("NeedsWindowRefresh - START");
		OnLostDevice();
		OnResetDevice();
		FlagWindowRefreshed();
		LogInfo("NeedsWindowRefresh - END");
	}
}

void KEPMenu::OnLButtonDown(int x, int y) {}

void KEPMenu::OnLButtonUp(int x, int y) {}

void KEPMenu::OnRButtonDown(int x, int y) {}

void KEPMenu::OnRButtonUp(int x, int y) {}

void KEPMenu::OnMButtonDown(int x, int y) {}

void KEPMenu::OnMButtonUp(int x, int y) {}

void KEPMenu::OnMouseMove(int x, int y) {}

bool KEPMenu::HitTest(int x, int y) const {
	// DRF - Bug Fix - World Object Selected On Close Menu Mouse Up
	// We must include menus being deleted in this test otherwise the client
	// thinks the left click mouse up event that occurs while closing a menu
	// is meant to be an object selection event.
	FOR_EACH_DIALOG_HIT(pDialog, m_dialogsStable)
	if (pDialog->HitTest(x, y))
		return true;
	FOR_EACH_DIALOG_END

	return false;
}

// DRF - Only succeeds for valid dialogs not being deleted.
bool KEPMenu::DialogFind(IMenuDialog* pIMenuDialog) {
	// Valid Dialog ?
	if (!VALID_DIALOG(pIMenuDialog)) {
		LogError("Dialog <" << pIMenuDialog << "> INVALID");
		return false;
	}

	// Search Dialog List For Match
	FOR_EACH_DIALOG(pDialog)
	if (pDialog == pIMenuDialog)
		return true;
	FOR_EACH_DIALOG_END

	LogError("Dialog <" << pIMenuDialog << "> NOT FOUND");
	LogAllDialogs();
	return false;
}

bool KEPMenu::DialogRemove(IMenuDialog* pDialog, bool addToDeleted) {
	// Invalid Dialog Or No Dialogs Open ?
	if (!VALID_DIALOG(pDialog) || m_dialogs.empty())
		return false;

	// Erase Dialog From m_dialogs[] (flag as dirty)
	dialog_iter_t iter;
	if (!DialogFind(pDialog)) {
		LogError("FindDialog(" << pDialog << ") FAILED");
		return false;
	}
	m_dialogs.remove(pDialog);
	m_dialogsDirty = true;

	// Add To m_deletedDialogs[] ?
	if (addToDeleted) {
		// Flag For Deletion
		pDialog->SetPendingDeletion(true);

		// Add To m_dialogsDeleted[]
		m_dialogsDeleted.push_back(pDialog);

		// Clear Cursor Menu If It Was This
		if (m_cursorMenu == pDialog) {
			m_cursorMenu = NULL;
			SetMenuCursorType(CURSOR_TYPE_DEFAULT);
		}

		// DRF - Bug Fix - Clear Static Controls For This
		pDialog->ClearStaticControlsForThisDialog();
	}

	return true;
}

void KEPMenu::DialogInsertToFront(IMenuDialog* pDialog) {
	// Insert Dialog To Front Of m_dialogs[] (flag as dirty)
	m_dialogs.push_back(pDialog);
	m_dialogsDirty = true;

	// Move 'AlwaysFront' Dialog To Front
	if (m_alwaysBringDialogToFront && (pDialog != m_alwaysBringDialogToFront)) {
		if (DialogRemove(m_alwaysBringDialogToFront, false))
			m_dialogs.push_back(m_alwaysBringDialogToFront);
	}
}

bool KEPMenu::DialogBringToFront(IMenuDialog* pDialog) {
	// Invalid Dialog Or No Dialogs Open ?
	if (!VALID_DIALOG(pDialog) || m_dialogs.empty())
		return false;

	// Clear BringToFront Flag
	pDialog->SetBringToFront(false);

	// BringToFront Enabled >
	if (!pDialog->BringToFrontEnabled())
		return false;

	// Already Frontmost ?
	IMenuDialog* dlgFrontWas = GetDialogFrontmost();
	if (dlgFrontWas == pDialog)
		return true;

	// Move Dialog To Front
	if (DialogRemove(pDialog, false)) {
		DialogInsertToFront(pDialog);
	} else {
		LogError("RemoveDialog(" << pDialog << ") FAILED");
	}

	return true;
}

bool KEPMenu::DialogBringToFrontEnable(IMenuDialog* pDialog, bool enable) {
	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog) || m_dialogs.empty())
		return false;

	// Enable/Disable Dialog Bring To Front
	return pDialog->BringToFrontEnable(enable);
}

bool KEPMenu::DialogAlwaysBringToFront(IMenuDialog* pDialog, bool enable) {
	// Disable ?
	if (!enable) {
		m_alwaysBringDialogToFront = NULL;
		LogInfo("DISABLED");
		return true;
	}

	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog) || m_dialogs.empty())
		return false;

	// Update Always Bring To Front Dialog
	m_alwaysBringDialogToFront = pDialog;

	// And Bring It To The Front Now
	DialogBringToFront(pDialog);
	LogInfo(DialogToStr(pDialog));

	return true;
}

void KEPMenu::DialogInsertToBack(IMenuDialog* pDialog) {
	// Insert Dialog To Back (flag as dirty)
	m_dialogs.insert(m_dialogs.begin(), pDialog);
	m_dialogsDirty = true;
}

bool KEPMenu::DialogSendToBack(IMenuDialog* pDialog) {
	// Invalid Dialog Or No Dialogs Open ?
	if (!VALID_DIALOG(pDialog) || m_dialogs.empty())
		return false;

	// DRF - Disable If Always To Front Dialog
	if (pDialog == m_alwaysBringDialogToFront)
		DialogAlwaysBringToFront(pDialog, false);

	// Already Backmost ?
	IMenuDialog* dlgBackWas = GetDialogBackmost();
	if (dlgBackWas == pDialog)
		return true;

	// Move Dialog To Back
	if (DialogRemove(pDialog, false)) {
		DialogInsertToBack(pDialog);
	} else {
		LogError("RemoveDialog(" << pDialog << ") FAILED");
	}

	return true;
}

IDirect3DTexture9* KEPMenu::GetD3DTextureIfLoadedOrQueueToLoad(const std::string& filename) {
	return m_engine->GetD3DTextureIfLoadedOrQueueToLoad(filename.c_str());
}

IDirect3DTexture9* KEPMenu::GetDynamicTexture(unsigned dynTextureId) {
	return m_engine->GetDynamicTexture(dynTextureId);
}

// Handles the Dialog GUI events
bool CALLBACK KEPMenu::OnDialogGUIEvent(GUI_EVENT nEvent, IMenuDialog* pDialog, POINT pt) {
	bool ok = true;

	// do not handle any events if in edit mode
	if (pDialog->GetEdit()) {
		switch (nEvent) {
			//all these events should be broadcast to all dialogs, this is for menu editor purposes: tyler
			case EVENT_DIALOG_SELECTED:
				if (KEPMenu::Instance())
					KEPMenu::Instance()->DialogHandleSelected(pDialog, pt);
				break;

			case EVENT_DIALOG_MOVED:
				if (KEPMenu::Instance())
					KEPMenu::Instance()->DialogHandleMoved(pDialog, pt);
				break;

			case EVENT_DIALOG_RESIZED:
				if (KEPMenu::Instance())
					KEPMenu::Instance()->DialogHandleResized(pDialog, pt);
				break;
		}
	} else {
		switch (nEvent) {
			case EVENT_DIALOG_MOUSE_MOVE:
				ok = Handlers::OnDialogMouseMove(pDialog, pt);
				break;

			case EVENT_DIALOG_LBUTTONDOWN:
				ok = Handlers::OnDialogLButtonDown(pDialog, pt);
				break;

			case EVENT_DIALOG_RBUTTONDOWN:
				ok = Handlers::OnDialogRButtonDown(pDialog, pt);
				break;

			case EVENT_DIALOG_LBUTTONDBLCLK:
				ok = Handlers::OnDialogLButtonDblClk(pDialog, pt);
				break;

			case EVENT_DIALOG_LBUTTONUP:
				ok = Handlers::OnDialogLButtonUp(pDialog, pt);
				break;

			case EVENT_DIALOG_RBUTTONUP:
				ok = Handlers::OnDialogRButtonUp(pDialog, pt);
				break;

			case EVENT_DIALOG_LBUTTONDOWN_INSIDE:
				ok = Handlers::OnDialogLButtonDownInside(pDialog, pt);
				break;

			case EVENT_DIALOG_KEYDOWN: {
				bool bShiftDown = (pt.y & 0x0001) != 0;
				bool bControlDown = (pt.y & 0x0010) != 0;
				bool bAltDown = (pt.y & 0x0100) != 0;
				ok = Handlers::OnDialogKeyDown(pDialog, (unsigned int)pt.x, bShiftDown, bControlDown, bAltDown);
				break;
			}

			case EVENT_DIALOG_SYSKEYDOWN: {
				bool bShiftDown = (pt.y & 0x0001) != 0;
				bool bControlDown = (pt.y & 0x0010) != 0;
				bool bAltDown = (pt.y & 0x0100) != 0;
				ok = Handlers::OnDialogSysKeyDown(pDialog, (unsigned int)pt.x, bShiftDown, bControlDown, bAltDown);
				break;
			}

			case EVENT_DIALOG_KEYUP: {
				bool bShiftDown = (pt.y & 0x0001) != 0;
				bool bControlDown = (pt.y & 0x0010) != 0;
				bool bAltDown = (pt.y & 0x0100) != 0;
				ok = Handlers::OnDialogKeyUp(pDialog, (unsigned int)pt.x, bShiftDown, bControlDown, bAltDown);
				break;
			}

			case EVENT_DIALOG_SYSKEYUP: {
				bool bShiftDown = (pt.y & 0x0001) != 0;
				bool bControlDown = (pt.y & 0x0010) != 0;
				bool bAltDown = (pt.y & 0x0100) != 0;
				ok = Handlers::OnDialogSysKeyUp(pDialog, (unsigned int)pt.x, bShiftDown, bControlDown, bAltDown);
				break;
			}

			case EVENT_DIALOG_MOVED: {
				ok = Handlers::OnDialogMoved(pDialog, pDialog, pt);
				break;
			}
		}
	}

	return ok;
}

// Handles the Control GUI events
bool CALLBACK KEPMenu::OnControlGUIEvent(GUI_EVENT nEvent, int nControlID, IMenuControl* pControl, POINT pt) {
	bool bHandled = false;

	IMenuDialog* pDialog = pControl->GetDialog();
	if (!REF_PTR_VALID(pDialog)) {
		LogError("event=" << std::hex << nEvent << " Dialog<" << pDialog << "> REF_PTR_INVALID");
		return true; // stop trying to handle this event
	}

	// if edit mode
	if (pDialog->GetEdit()) {
		switch (nEvent) {
			case EVENT_CONTROL_SELECTED:
				if (KEPMenu::Instance())
					bHandled = KEPMenu::Instance()->ControlHandleSelected(nControlID, pControl, pt);
				break;

			case EVENT_CONTROL_MOVED:
				bHandled = Handlers::OnControlMoved(pControl->GetDialog(), pControl, pt);
				break;

			case EVENT_CONTROL_RESIZED:
				if (KEPMenu::Instance())
					bHandled = KEPMenu::Instance()->ControlHandleResized(pControl, pt);
				break;

			case EVENT_CONTROL_DRAGSTART:
				if (KEPMenu::Instance())
					bHandled = KEPMenu::Instance()->ControlHandleDragStart(nControlID, pControl, pt);
				break;

			case EVENT_CONTROL_DRAGEND:
				if (KEPMenu::Instance())
					bHandled = KEPMenu::Instance()->ControlHandleDragEnd(nControlID, pControl, pt);
				break;
		}
	} else {
		switch (nEvent) {
			case EVENT_BUTTON_CLICKED:
				bHandled = Handlers::OnButtonClicked(dynamic_cast<CDXUTButton*>(pControl));
				break;

			case EVENT_COMBOBOX_SELECTION_CHANGED:
				bHandled = Handlers::OnComboBoxSelectionChanged(dynamic_cast<CDXUTComboBox*>(pControl));
				break;

			case EVENT_RADIOBUTTON_CHANGED:
				bHandled = Handlers::OnRadioButtonChanged(dynamic_cast<CDXUTRadioButton*>(pControl));
				break;

			case EVENT_CHECKBOX_CHANGED:
				bHandled = Handlers::OnCheckBoxChanged(dynamic_cast<CDXUTCheckBox*>(pControl));
				break;

			case EVENT_SLIDER_VALUE_CHANGED:
				bHandled = Handlers::OnSliderValueChanged(dynamic_cast<CDXUTSlider*>(pControl));
				break;

			case EVENT_EDITBOX_STRING:
				bHandled = Handlers::OnEditBoxString(dynamic_cast<CDXUTEditBox*>(pControl));
				break;

			case EVENT_EDITBOX_CHANGE:
				bHandled = Handlers::OnEditBoxChange(dynamic_cast<CDXUTEditBox*>(pControl));
				break;

			case EVENT_EDITBOX_KEYDOWN: {
				bool bShiftDown = (pt.y & 0x0001) != 0;
				bool bControlDown = (pt.y & 0x0010) != 0;
				bool bAltDown = (pt.y & 0x0100) != 0;
				bHandled = Handlers::OnEditBoxKeyDown(dynamic_cast<CDXUTEditBox*>(pControl), (unsigned int)pt.x, bShiftDown, bControlDown, bAltDown);
				break;
			}

			case EVENT_LISTBOX_ITEM_DBLCLK:
				bHandled = Handlers::OnListBoxItemDblClick(dynamic_cast<CDXUTListBox*>(pControl));
				break;

			case EVENT_LISTBOX_SELECTION:
				bHandled = Handlers::OnListBoxSelection(dynamic_cast<CDXUTListBox*>(pControl), pt);
				break;

			case EVENT_SCROLLBAR_CHANGED:
				bHandled = Handlers::OnScrollBarChanged(dynamic_cast<CDXUTScrollBar*>(pControl));
				break;

			case EVENT_CONTROL_MOUSE_ENTER:
				bHandled = Handlers::OnControlMouseEnter(pControl);
				break;

			case EVENT_CONTROL_MOUSE_LEAVE:
				bHandled = Handlers::OnControlMouseLeave(pControl);
				break;

			case EVENT_CONTROL_FOCUS_IN:
				bHandled = Handlers::OnControlFocusIn(pControl);
				break;

			case EVENT_CONTROL_FOCUS_OUT:
				bHandled = Handlers::OnControlFocusOut(pControl);
				break;

			case EVENT_CONTROL_LBUTTONDOWN:
				bHandled = Handlers::OnControlLButtonDown(pControl, pt);
				break;

			case EVENT_CONTROL_RBUTTONDOWN:
				bHandled = Handlers::OnControlRButtonDown(pControl, pt);
				break;

			case EVENT_CONTROL_LBUTTONDBLCLK:
				bHandled = Handlers::OnControlLButtonDblClk(pControl, pt);
				break;

			case EVENT_CONTROL_LBUTTONUP:
				bHandled = Handlers::OnControlLButtonUp(pControl, pt);
				break;

			case EVENT_CONTROL_RBUTTONUP:
				bHandled = Handlers::OnControlRButtonUp(pControl, pt);
				break;

			case EVENT_LINK_PRESSED:
				bHandled = Handlers::OnLinkClicked(pControl);
				break;
		}
	}

	return bHandled;
}

bool KEPMenu::IsDialogFrontmostMouseEnabled() {
	IMenuDialog* pDialog = GetDialogFrontmost();
	return (!pDialog || pDialog->GetEnableMouseInput()); // default true
}

bool KEPMenu::IsDialogFrontmostKeyboardEnabled() {
	IMenuDialog* pDialog = GetDialogFrontmost();
	return (!pDialog || pDialog->GetEnableKeyboardInput()); // default true
}

bool KEPMenu::MsgProcIsClose() {
	MsgProcInfo mpi;
	return mpi.IsClose();
}

bool KEPMenu::MsgProcValidMsg() {
	MsgProcInfo mpi;
	return (mpi.IsActivate() || mpi.IsMouse() || mpi.IsKey());
}

// Before handling window messages, the sample framework passes incoming windows
// messages to the application through this callback function. If the application sets
// *pbNoFurtherProcessing to TRUE, then the sample framework will not process this message.
LRESULT CALLBACK KEPMenu::MsgProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, bool* pbNoFurtherProcessing) {
	*pbNoFurtherProcessing = false; // continue processing message if no menus handle message

	// Valid KEPMenu & IClientEngine ?
	if (!m_pThis || !m_pThis->m_engine)
		return 0;

	// Valid Message ?
	if (!MsgProcValidMsg())
		return 0;

	// Is Dialog In Focus Capturing Keys?
	auto capturingKeys = IsDialogInFocusCapturingKeys();
	m_pThis->m_engine->KeyCapturedTriggered(capturingKeys);

	// Beginning Of Menu Cursor Type Setting
	m_pThis->SetMenuCursorTypeBegin();

	// Beginning Of Menu Cursor Setting
	m_pThis->SetCursorMenuBegin();

	// DRF - Drag Bug Fix - Once Set with WM_MOUSEMOVE We Remain Set Until Button Up?!
	m_pThis->m_engine->MenuEventTriggered(false);

	// For Each Stable Dialog (reverse order)
	FOR_EACH_DIALOG_REVERSE(pDialog, m_pThis->m_dialogsStable)

	// DRF - Added - Close On ESC?
	bool keyESC = ((uMsg == WM_KEYDOWN) && (wParam == 27));
	bool closeOnESC = (keyESC && pDialog->GetCloseOnESC() && !pDialog->GetEdit());
	if (closeOnESC) {
		LogWarn("CloseOnESC - " << pDialog->ToStr());
		m_pThis->DialogDestroy(pDialog);
		continue;
	}

	// Call Dialog Message Process
	*pbNoFurtherProcessing = pDialog->MsgProc(hWnd, uMsg, wParam, lParam);

	// ED-7373:
	// * Stop propagating keyboard/mouse events below first modal dialog found in Z-order (except for closeOnESC messages)
	// * Also allow non-modal dialogs on top of modal ones to process and consume messages (e.g. keyboard / mouse inputs)
	if (pDialog->IsActuallyModal()) {
		*pbNoFurtherProcessing = true;
	}

	// If This Dialog Handled Message Then...
	if (*pbNoFurtherProcessing) {
		// Prevent ClickAndDrag Camera Movement Until Mouse Button Release
		// DRF - Drag Bug Fix - TODO - Need to be smarter about this.  This gets set for WM_MOUSEMOVE
		// over any menu and then won't be unset until original WM_LBUTTONUP logic below.  This causes
		// camera drags to not work sometimes until the second click to drag attempt.  For now the fix
		// is to set this false at the start of every message so it will get reset on the first message
		// not handled by a menu as well as menu WM_LBUTTONUP as it originally was. The bug fix is above.
		// This needs re-evaluated.
		m_pThis->m_engine->MenuEventTriggered(true);

		// Bring Dialog To Front If Flagged By MsgProc
		// DRF - Bug Fix - MsgProc calls lua event handler which may have closed
		// this menu so re-validate it before bringing it to front.
		if (VALID_DIALOG_STABLE(pDialog) && pDialog->GetBringToFront())
			m_pThis->DialogBringToFront(pDialog);

		// Exit Dialog Loop
		break;
	}
	FOR_EACH_DIALOG_END

	// End Of Menu Cursor Setting
	m_pThis->SetCursorMenuEnd();

	// End Of Menu Cursor Type Setting
	m_pThis->SetMenuCursorTypeEnd(uMsg);

	return 0;
}

void KEPMenu::SetCursorMenuBegin() {
	m_cursorMenuTemp = NULL;
}

void KEPMenu::SetCursorMenu(CDXUTDialog* pDialog) {
	m_cursorMenuTemp = pDialog;
}

void KEPMenu::SetCursorMenuEnd() {
	// Cursor Menu Unchanged ?
	if (m_cursorMenu == m_cursorMenuTemp)
		return;

	// Re-Validate Cursor Menu Just Incase It Was Deleted
	if (m_cursorMenuTemp && !VALID_DIALOG(m_cursorMenuTemp))
		m_cursorMenuTemp = NULL;

	// Update Cursor Menu
	m_cursorMenu = m_cursorMenuTemp;
	std::string cursorMenu = (m_cursorMenu ? StrFileName(m_cursorMenu->GetScriptFileName()) : "NONE");

	LogDebug("cursorMenu=" << cursorMenu);
}

void KEPMenu::SetMenuCursorTypeBegin() {
	m_cursorTypeSet = false;
}

void KEPMenu::SetMenuCursorType(const CURSOR_TYPE& cursorType) {
	if (!m_cursorTypeSet)
		SetCursorModeType(CURSOR_MODE_MENU, cursorType);
	m_cursorTypeSet = true;
}

void KEPMenu::SetMenuCursorTypeEnd(UINT uMsg) {
	if (uMsg != WM_MOUSEMOVE)
		return;
	if (!m_cursorTypeSet)
		SetMenuCursorType(CURSOR_TYPE_DEFAULT);
}

bool KEPMenu::ResetCursorModesTypes() {
	// Reset All Cursor Modes And Types To Defaults
	LogInfo("OK");
	SetCursorModeType(CURSOR_MODE_NONE, CURSOR_TYPE_NONE);
	SetCursorModeType(CURSOR_MODE_MENU, CURSOR_TYPE_DEFAULT);
	SetCursorModeType(CURSOR_MODE_WIDGET, CURSOR_TYPE_DEFAULT);
	SetCursorModeType(CURSOR_MODE_OBJECT, CURSOR_TYPE_DEFAULT);
	SetCursorModeType(CURSOR_MODE_WORLD, CURSOR_TYPE_DEFAULT);
	SetCursorMode(CURSOR_MODE_NONE);

	return true;
}

bool KEPMenu::SetCursorModeType(const CURSOR_MODE& cursorMode, const CURSOR_TYPE& cursorType) {
	// Valid Cursor Mode ?
	if (cursorMode >= CURSOR_MODES) {
		LogError("Invalid cursorMode=" << (int)cursorMode);
		return false;
	}

	// Valid Cursor Type ?
	if (cursorType >= CURSOR_TYPES) {
		LogError("Invalid cursorType=" << (int)cursorType);
		return false;
	}

	// Cursor Mode Type Unchanged ?
	if (m_cursorModeType[cursorMode] == cursorType)
		return true;

	// Update Cursor Mode Type
	m_cursorModeType[cursorMode] = cursorType;

	// If Current Cursor Mode Set Current Cursor Type
	if (cursorMode == m_cursorMode)
		return SetCursorType(cursorType);

	return true;
}

bool KEPMenu::SetCursorMode(const CURSOR_MODE& cursorMode) {
	// Valid Cursor Mode ?
	if (cursorMode >= CURSOR_MODES) {
		LogError("Invalid cursorMode=" << (int)cursorMode);
		return false;
	}

	// Cursor Mode Unchanged ?
	if (m_cursorMode == cursorMode)
		return true;

	// Get Cursor Mode Type
	CURSOR_TYPE cursorType = m_cursorModeType[cursorMode];

	// Set Cursor Type
	if (!SetCursorType(cursorType)) {
		LogError("SetCursorType() FAILED - " << CursorModeStr(cursorMode));
		return false;
	}

	// Update Current Cursor Mode
	m_cursorMode = cursorMode;

	return true;
}

bool KEPMenu::SetCursorType(const CURSOR_TYPE& cursorType) {
	// Cursor Initialized?
	if (!g_cursorInit) {
		LogError("Cursor Not Initialized");
		return false;
	}

	// Valid Cursor Type ?
	if (cursorType >= CURSOR_TYPES) {
		LogError("Invalid cursorType=" << (int)cursorType);
		return false;
	}

	// Cursor Type Unchanged ?
	if (m_cursorType == cursorType)
		return true;

	// Get Cursor Texture File
	std::string cursorFile = PathAdd(GetPathMapsModelsCommon(), m_engine->GetCursorFileName(cursorType));

	// Set DirectX Cursor Texture
	DXUTSetCursorTexture(cursorFile.c_str(), CURSOR_HOTSPOT[cursorType]);

	// Update Current Cursor Type
	m_cursorType = cursorType;

	return true;
}

// As a convenience, the framework inspects the incoming windows messages for
// keystroke messages and decodes the message parameters to pass relevant keyboard
// messages to the application.  The framework does not remove the underlying keystroke
// messages, which are still passed to the application's MsgProc callback.
void CALLBACK KEPMenu::KeyboardProc(UINT nChar, bool bKeyDown, bool bAltDown) {
	if (KEPMenu::Instance() && GetKeyState(VK_CONTROL)) {
		if (!bKeyDown) {
			//Somehow these events are for key-up only
			//Controlled from MenuEditor.lua now
			switch (nChar) {
				case 'C':
					//CTRL-C
					//KEPMenu::Instance()->CopyControlToClipboard( NULL, "" );
					break;
				case 'V':
					//CTRL-V
					//KEPMenu::Instance()->PasteControlFromClipboard( NULL, true, 10, 10 );
					break;
				case 'Z':
					//CTRL-Z
					//KEPMenu::Instance()->Undo( NULL );
					break;
			}
		} else {
			//Following events will trigger on key-down or both
			switch (nChar) {
				case VK_OEM_4:
					//CTRL-[
					KEPMenu::Instance()->DialogControlMoveBackward(NULL, "");
					break;
				case VK_OEM_6:
					//CTRL-]
					KEPMenu::Instance()->DialogControlMoveForward(NULL, "");
					break;
			}
		}
	}
}

bool KEPMenu::CopyControlToClipboard(IMenuDialog* pDialog, const std::string& ctlName) {
	// Use Last Known Selected Dialog ?
	if (!pDialog) {
		if (!m_selectedDlgId.empty())
			pDialog = GetDialog(m_selectedDlgId);
	}

	// Valid Dialog?
	if (!VALID_DIALOG(pDialog))
		return false;

	std::string controlName = ctlName;
	if (controlName.empty()) {
		if (!m_selectedCtlName.empty()) {
			auto pMC = pDialog->GetControl(m_selectedCtlName.c_str());
			if (pMC)
				controlName = m_selectedCtlName;
		}
	}
	if (controlName.empty())
		return false;

	auto pMC = pDialog->GetControl(controlName.c_str());
	if (!pMC)
		return false;

	m_clipboardItem.controlType = pMC->GetType();
	m_clipboardItem.controlXml->Clear();
	pMC->Save(m_clipboardItem.controlXml);
	m_clipboardItem.controlXml->SetValue(pMC->GetTag());
	return true;
}

bool KEPMenu::PasteControlFromClipboard(IMenuDialog* pDialog, bool bRelocate, int newX, int newY) {
	// Use Last Known Selected Dialog ?
	if (!pDialog && !m_selectedDlgId.empty())
		pDialog = GetDialog(m_selectedDlgId);

	// Valid Dialog?
	if (!VALID_DIALOG(pDialog))
		return false;

	auto pMC = pDialog->LoadControl(m_clipboardItem.controlXml);
	if (!pMC)
		return false;

	std::string name = pMC->GetName();
	int postfix = 0;
	FOREVER {
		bool bConflictFound = false;
		for (int i = 0; i < pDialog->GetNumControls(); i++) {
			IMenuControl* pTmp = pDialog->GetControlByIndex(i);
			if (pMC != pTmp && name == pTmp->GetName()) {
				bConflictFound = true;
				break;
			}
		}

		if (bConflictFound) {
			postfix++;
			std::stringstream ss;
			ss << pMC->GetName();
			ss << postfix;
			name = ss.str();
		} else {
			break;
		}
	}

	pMC->SetName(name.c_str());
	pDialog->RequestSelection(pMC, true);

	//Send a dummy event to notify UI that dialog has changed
	POINT pt;
	pMC->GetLocation(pt);
	Handlers::OnControlMoved(pDialog, pMC, pt);

	return true;
}

bool KEPMenu::DialogHandleSelected(IMenuDialog* pIMenuDialog, POINT pt) {
	// Clear Selected Dialog & Control
	m_selectedDlgId.clear();
	m_selectedCtlName.clear();

	// Valid Dialog ?
	if (VALID_DIALOG(pIMenuDialog))
		m_selectedDlgId = pIMenuDialog->GetMenuId();

	bool ok = true;
	FOR_EACH_DIALOG_STABLE(pDialog)
	ok &= Handlers::OnDialogSelected(pDialog, pIMenuDialog);
	FOR_EACH_DIALOG_END
	return ok;
}

bool KEPMenu::DialogHandleMoved(IMenuDialog* pIMenuDialog, POINT pt) {
	// Valid Dialog ?
	if (!VALID_DIALOG(pIMenuDialog))
		return false;

	bool ok = true;
	FOR_EACH_DIALOG_STABLE(pDialog)
	ok &= Handlers::OnDialogMoved(pDialog, pIMenuDialog, pt);
	FOR_EACH_DIALOG_END
	return ok;
}

bool KEPMenu::DialogHandleResized(IMenuDialog* pIMenuDialog, POINT pt) {
	// Valid Dialog ?
	if (!VALID_DIALOG(pIMenuDialog))
		return false;

	bool ok = true;
	FOR_EACH_DIALOG_STABLE(pDialog)
	ok &= Handlers::OnDialogResized(pDialog, pIMenuDialog, pt);
	FOR_EACH_DIALOG_END
	return ok;
}

bool KEPMenu::ControlHandleSelected(int nControlID, IMenuControl* pControl, POINT pt) {
	// Clear Selected Dialog & Control
	m_selectedDlgId.clear();
	m_selectedCtlName.clear();

	if (!pControl || !pControl->GetDialog())
		return false;

	m_selectedDlgId = pControl->GetDialog()->GetMenuId();
	m_selectedCtlName = pControl->GetName();

	bool bHandled = false;
	FOR_EACH_DIALOG_STABLE(pDialog)
	bHandled = Handlers::OnControlSelected(pDialog, pControl);
	FOR_EACH_DIALOG_END
	return bHandled;
}

bool KEPMenu::ControlHandleMoved(IMenuControl* pControl, POINT pt) {
	if (!pControl)
		return false;

	bool bHandled = false;
	FOR_EACH_DIALOG_STABLE(pDialog)
	bHandled = Handlers::OnControlMoved(pDialog, pControl, pt);
	FOR_EACH_DIALOG_END
	return bHandled;
}

bool KEPMenu::ControlHandleResized(IMenuControl* pControl, POINT pt) {
	if (!pControl)
		return false;

	bool bHandled = false;
	FOR_EACH_DIALOG_STABLE(pDialog)
	bHandled = Handlers::OnControlResized(pDialog, pControl, pt);
	FOR_EACH_DIALOG_END
	return bHandled;
}

bool KEPMenu::ControlHandleDragStart(int nControlID, IMenuControl* pControl, POINT pt) {
	POINT pos;
	pControl->GetLocation(pos);
	m_undoCandidate.op = UNDO_PLACEMENT;
	m_undoCandidate.dlgID = pControl->GetDialog()->GetID();
	m_undoCandidate.ctlName = pControl->GetName();
	m_undoCandidate.ctlX = pos.x;
	m_undoCandidate.ctlY = pos.y;
	m_undoCandidate.ctlW = pControl->GetWidth();
	m_undoCandidate.ctlH = pControl->GetHeight();
	return true;
}

bool KEPMenu::ControlHandleDragEnd(int nControlID, IMenuControl* pControl, POINT pt) {
	// Valid Dialog ?
	IMenuDialog* pIMenuDialog = pControl->GetDialog();
	if (!VALID_DIALOG(pIMenuDialog))
		return false;

	if ((pIMenuDialog->GetID() != m_undoCandidate.dlgID) || (pControl->GetName() != m_undoCandidate.ctlName)) {
		m_undoCandidate.op = UNDO_NONE;
		m_undoCandidate.dlgID = -1;
		m_undoCandidate.ctlName.clear();
		return false;
	}

	if (m_undoCandidate.op == UNDO_PLACEMENT) {
		POINT pos;
		pControl->GetLocation(pos);
		if ((pos.x != m_undoCandidate.ctlX) || (pos.y != m_undoCandidate.ctlY) || (pControl->GetWidth() != m_undoCandidate.ctlW) || (pControl->GetHeight() != m_undoCandidate.ctlH)) {
			m_undoInfo = m_undoCandidate;
		}

		m_undoCandidate.op = UNDO_NONE;
		m_undoCandidate.dlgID = -1;
		m_undoCandidate.ctlName.clear();
	}

	bool bHandled = false;
	FOR_EACH_DIALOG_STABLE(pDialog)
	bHandled = Handlers::OnControlMoved(pDialog, pControl, pt) || bHandled;
	FOR_EACH_DIALOG_END
	return bHandled;
}

bool KEPMenu::DialogUndo(IMenuDialog* pDialog) {
	// Use Last Known Selected Dialog ?
	if (!pDialog) {
		if (!m_selectedDlgId.empty())
			pDialog = GetDialog(m_selectedDlgId);
	}

	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog))
		return false;

	if (m_undoInfo.op == UNDO_NONE || pDialog->GetID() != m_undoInfo.dlgID)
		return false;

	switch (m_undoInfo.op) {
		case UNDO_PLACEMENT: {
			auto pMC = pDialog->GetControl(m_undoInfo.ctlName.c_str());
			if (!pMC)
				return false;

			POINT pos;
			pMC->GetLocation(pos);

			int tmp = m_undoInfo.ctlX;
			m_undoInfo.ctlX = pos.x;
			pos.x = tmp;

			tmp = m_undoInfo.ctlY;
			m_undoInfo.ctlY = pos.y;
			pos.y = tmp;

			POINT size;
			size.x = m_undoInfo.ctlW;
			m_undoInfo.ctlW = pMC->GetWidth();
			size.y = m_undoInfo.ctlH;
			m_undoInfo.ctlH = pMC->GetHeight();

			pMC->SetLocation(pos.x, pos.y);
			pMC->SetSize(size.x, size.y);

			Handlers::OnControlMoved(pDialog, pMC, pos);
			Handlers::OnControlResized(pDialog, pMC, size);
			return true;
		} break;
	}

	return false;
}

bool KEPMenu::DialogControlMoveForward(IMenuDialog* pDialog, const std::string& ctlName) {
	// Use Last Known Selected Dialog ?
	if (!pDialog) {
		if (!m_selectedDlgId.empty())
			pDialog = GetDialog(m_selectedDlgId);
	}

	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog))
		return false;

	std::string controlName = ctlName;
	if (controlName.empty()) {
		if (!m_selectedCtlName.empty()) {
			IMenuControl* ctl = pDialog->GetControl(m_selectedCtlName.c_str());
			if (ctl)
				controlName = m_selectedCtlName;
		}
	}

	return pDialog->MoveControlForward(controlName);
}

bool KEPMenu::DialogControlMoveBackward(IMenuDialog* pDialog, const std::string& ctlName) {
	// Use Last Known Selected Dialog ?
	if (!pDialog) {
		if (!m_selectedDlgId.empty())
			pDialog = GetDialog(m_selectedDlgId);
	}

	// Valid Dialog ?
	if (!VALID_DIALOG(pDialog))
		return false;

	std::string controlName = ctlName;
	if (controlName.empty()) {
		if (!m_selectedCtlName.empty()) {
			IMenuControl* ctl = pDialog->GetControl(m_selectedCtlName.c_str());
			if (ctl)
				controlName = m_selectedCtlName;
		}
	}

	return pDialog->MoveControlBackward(controlName);
}

// DRF - ED-7035 - Investigate texture memory impact
void KEPMenu::ReleaseTextureNode(IDirect3DTexture9* pTex) {
	auto pRM = DXUTGetGlobalDialogResourceManager();
	if (pRM)
		pRM->ReleaseTextureNode(pTex);
}

void KEPMenu::ClearTextures() {
	// Clear Textures List
	std::lock_guard<fast_recursive_mutex> texturesLock(m_texturesMutex); // drf - added
	m_textures.clear();
	LogInfo("OK (0 textures)");
}

int KEPMenu::GetNumTextures() {
	std::lock_guard<fast_recursive_mutex> texturesLock(m_texturesMutex);
	return (int)m_textures.size();
}

const char* KEPMenu::GetTextureByIndex(int index) {
	// Return Indexed Texture File Name
	std::lock_guard<fast_recursive_mutex> texturesLock(m_texturesMutex);
	int count = 0;
	for (const auto& it : m_textures) {
		if (count == index)
			return it.c_str();
		count++;
	}
	return "";
}

void KEPMenu::AddTexture(const std::string& fileName) {
	// Add To Textures List
	std::lock_guard<fast_recursive_mutex> texturesLock(m_texturesMutex);
	m_textures.insert(fileName);
}

void KEPMenu::AddTextures(const std::string& menu_dir) {
	LogInfo("START");

	// find all the textures in the 'Menus' directory
	WIN32_FIND_DATA find_data;
	HANDLE handle;

	std::string file_mask = PathAdd(menu_dir, "*.*");
	std::string validExtensions = ":dds:tga:png:bmp:jpg";

	bool done = false;
	handle = ::FindFirstFileW(Utf8ToUtf16(file_mask).c_str(), &find_data);

	if (INVALID_HANDLE_VALUE == handle)
		done = true;

	while (!done) {
		std::string filename = Utf16ToUtf8(find_data.cFileName);
		std::string ext = filename.substr(filename.rfind(".") + 1, std::string::npos);

		if (ext.length() != 0) { //just to kill the ".." directory
			ext = ":" + ext; //a delimeter that can never be in a file name

			if (validExtensions.find(ext) != std::string::npos)
				AddTexture(filename);
		}

		done = (::FindNextFile(handle, &find_data) == FALSE);
	}
	::FindClose(handle);

	LogInfo("END (" << m_textures.size() << " textures)");
}

void KEPMenu::ClearMenuPaths() {
	// Clear Menu Paths List
	std::lock_guard<fast_recursive_mutex> pathsLock(m_pathsMutex); // drf - paths mutex also locked in CMenuList_Depricated::Load()
	m_paths.clear();
	LogInfo("OK (0 menuPaths)");
}

void KEPMenu::AddMenuPaths(const std::string& pathMenu, const std::string& pathScript, bool trustedPaths, bool addTextures) {
	// Get proper textures each time a new menuPath is loaded.
	if (addTextures)
		AddTextures(pathMenu);

	// Ignore Duplicate Paths
	std::lock_guard<fast_recursive_mutex> pathsLock(m_pathsMutex); // drf - paths mutex also locked in CMenuList_Depricated::Load()
	for (const auto& it : m_paths)
		if (it.menuPath == pathMenu)
			return;

	// Add To Start Of Menu Paths List
	LogInfo("pathMenu='" << LogPath(pathMenu) << "' pathScript='" << LogPath(pathScript) << "' trusted=" << LogBool(trustedPaths) << " addTextures=" << LogBool(addTextures));
	m_paths.insert(m_paths.begin(), path_pair_t(pathMenu, pathScript, trustedPaths));
}

void KEPMenu::ReInit(bool /*isChild*/, bool clearMenuPaths, bool clearTextures) {
	// Clear Textures List?
	if (clearTextures)
		ClearTextures();

	// Clear Menu Paths List?
	if (clearMenuPaths)
		ClearMenuPaths();
}

bool KEPMenu::SearchFile(const std::string& relPath, bool isScript, bool trustedOnly, std::string& fullPath, bool* pTrusted) {
	LogTrace("relPath=" << relPath << " isScript=" << LogBool(isScript));

	// Absolute path is not allowed
	if (IsAbsolutePath(relPath)) {
		LogError("Path not relative: " << relPath);
		return false;
	}

	jsVerifyReturn(!m_pathBase.empty(), false);
	IClientEngine* pClientEngine = dynamic_cast<IClientEngine*>(m_engine);
	jsVerifyReturn(pClientEngine != NULL, false);

	// All assets must be under BASEDIR or LocalDev\<GameId> (I'd like to use BASEDIR\GameFiles but unfortunately menu sometimes refer to files under CustomTextures)
	std::string gameAssetPrefix = m_pathBase;

	// Call ClientEngine for the path to localDev directory. NOTE: All non-script files are represented by MenuXml so that I don't have to pass in actual type. Might become a problem if we move images somewhere else.
	std::string localDevAssetPrefix = m_engine->getDevToolAssetPath(isScript ? ScriptServerEvent::MenuScript : ScriptServerEvent::MenuXml);

	// Deal with other LocalDev paths (e.g. CustomTexture, Script) - treat LocalDev\<GameID>\OtherDir as a bad path but do not report it in the log
	std::string localDevAppDir = pClientEngine->LocalDevAppDir();

	// DRF - Added Mutex
	std::lock_guard<fast_recursive_mutex> pathsLock(m_pathsMutex);
	for (const auto& it : m_paths) {
		if (trustedOnly && !it.trusted)
			continue;

		// Get canonical path
		std::string testFullPath = GetAbsolutePath(relPath, isScript ? it.scriptPath : it.menuPath);

		// Check if under valid parent directories
		if (!StrSameFileSubPath(testFullPath, gameAssetPrefix) && !StrSameFileSubPath(testFullPath, localDevAssetPrefix)) {
			//Invalid path - report it unless it's under LocalDev\<GameID>
			if (!StrSameFileSubPath(testFullPath, localDevAppDir)) {
				LogDebug("Invalid relative path: " << relPath << " full path: " << testFullPath);
			}
			continue;
		}

		bool isDir;
		if (jsFileExists(testFullPath.c_str(), &isDir) && !isDir) {
			fullPath = testFullPath;
			if (pTrusted)
				*pTrusted = it.trusted;
			return true;
		}
	}

	return false;
}

bool KEPMenu::SearchFile(const std::string& relPath, bool isScript, bool trustedOnly, std::string& directory, std::string& fileName, bool* pTrusted /*=NULL*/) {
	std::string fullPath;
	if (SearchFile(relPath, isScript, trustedOnly, fullPath, pTrusted)) {
		size_t ofsSlash = fullPath.rfind("\\");
		size_t ofsSlash2 = fullPath.rfind("/");
		if (ofsSlash == std::string::npos || ofsSlash2 != std::string::npos && ofsSlash2 > ofsSlash)
			ofsSlash = ofsSlash2;

		if (ofsSlash != std::string::npos) {
			directory = fullPath.substr(0, ofsSlash + 1); // Preserve "\\" in directory
			fileName = fullPath.substr(ofsSlash + 1);
		} else {
			directory = "";
			fileName = fullPath;
		}

		if (fileName.empty()) {
			LogError("fileName.empty() - relPath=" << relPath);
			return false;
		}

		return true;
	}

	return false;
}

extern "C" BLADE_EXPORT
	size_t
	GetNumInterfaces() {
	return 1;
}

extern "C" BLADE_EXPORT
	IClientBlade*
	Create(size_t idx) {
	if (0 == idx)
		return dynamic_cast<IClientBlade*>(GetKepMenu());
	else
		return 0;
}

extern "C" BLADE_EXPORT void Destroy(IClientBlade* blade) {
	delete blade;
}

KEPMenu::ClipboardItem::ClipboardItem() {
	controlType = UINT_MAX; // DRF - ?!?! (DXUT_CONTROL_TYPE)-1;
	controlXml = new TiXmlDocument();
}

KEPMenu::ClipboardItem::~ClipboardItem() {
	delete controlXml;
}

} // namespace KEP