///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

template <
	typename out_val_type
#if (NUM_PARAMS > 0)
	,
	typename in_val_type1
#endif
#if (NUM_PARAMS > 1)
	,
	typename in_val_type2
#endif
#if (NUM_PARAMS > 2)
	,
	typename in_val_type3
#endif
#if (NUM_PARAMS > 3)
	,
	typename in_val_type4
#endif
#if (NUM_PARAMS > 4)
	,
	typename in_val_type5
#endif
#if (NUM_PARAMS > 5)
	,
	typename in_val_type6
#endif
#if (NUM_PARAMS > 6)
	,
	typename in_val_type7
#endif
#if (NUM_PARAMS > 7)
	,
	typename in_val_type8
#endif
#if (NUM_PARAMS > 8)
	,
	typename in_val_type9
#endif
	>
class function_signature<
	typename out_val_type
#if (NUM_PARAMS > 0)
	,
	typename in_val_type1
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 1)
	,
	typename in_val_type2
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 2)
	,
	typename in_val_type3
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 3)
	,
	typename in_val_type4
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 4)
	,
	typename in_val_type5
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 5)
	,
	typename in_val_type6
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 6)
	,
	typename in_val_type7
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 7)
	,
	typename in_val_type8
#else
	,
	null_parameter
#endif
#if (NUM_PARAMS > 8)
	,
	typename in_val_type9
#else
	,
	null_parameter
#endif
	,
	null_parameter> {
public:
	typedef out_val_type (*FunctionType)(
#if (NUM_PARAMS > 0)
		in_val_type1
#endif
#if (NUM_PARAMS > 1)
		,
		in_val_type2
#endif
#if (NUM_PARAMS > 2)
		,
		in_val_type3
#endif
#if (NUM_PARAMS > 3)
		,
		in_val_type4
#endif
#if (NUM_PARAMS > 4)
		,
		in_val_type5
#endif
#if (NUM_PARAMS > 5)
		,
		in_val_type6
#endif
#if (NUM_PARAMS > 6)
		,
		in_val_type7
#endif
#if (NUM_PARAMS > 7)
		,
		in_val_type8
#endif
#if (NUM_PARAMS > 8)
		,
		in_val_type9
#endif
	);

	static out_val_type make_call(
#if (NUM_PARAMS > 0)
		in_val_type1
#else
		null_parameter
#endif
			iv1,
#if (NUM_PARAMS > 1)
		in_val_type2
#else
		null_parameter
#endif
			iv2,
#if (NUM_PARAMS > 2)
		in_val_type3
#else
		null_parameter
#endif
			iv3,
#if (NUM_PARAMS > 3)
		in_val_type4
#else
		null_parameter
#endif
			iv4,
#if (NUM_PARAMS > 4)
		in_val_type5
#else
		null_parameter
#endif
			iv5,
#if (NUM_PARAMS > 5)
		in_val_type6
#else
		null_parameter
#endif
			iv6,
#if (NUM_PARAMS > 6)
		in_val_type7
#else
		null_parameter
#endif
			iv7,
#if (NUM_PARAMS > 7)
		in_val_type8
#else
		null_parameter
#endif
			iv8,
#if (NUM_PARAMS > 8)
		in_val_type9
#else
		null_parameter
#endif
			iv9,
		null_parameter iv10,
		FunctionType f) {
		iv1; // DRF - unreferenced formal parameters
		iv2;
		iv3;
		iv4;
		iv5;
		iv6;
		iv7;
		iv8;
		iv9;
		iv10;
		return f(
#if (NUM_PARAMS > 0)
			iv1
#endif
#if (NUM_PARAMS > 1)
			,
			iv2
#endif
#if (NUM_PARAMS > 2)
			,
			iv3
#endif
#if (NUM_PARAMS > 3)
			,
			iv4
#endif
#if (NUM_PARAMS > 4)
			,
			iv5
#endif
#if (NUM_PARAMS > 5)
			,
			iv6
#endif
#if (NUM_PARAMS > 6)
			,
			iv7
#endif
#if (NUM_PARAMS > 7)
			,
			iv8
#endif
#if (NUM_PARAMS > 8)
			,
			iv9
#endif
		);
	}
};
