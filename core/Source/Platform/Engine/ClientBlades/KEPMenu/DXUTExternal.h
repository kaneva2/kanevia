///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

class IExternalMesh;

class CDXUTExternal : public CDXUTStatic {
public:
	virtual ~CDXUTExternal();

	virtual bool CanHaveFocus() { return true; }

	// IMenuStatic override
	HRESULT SetText(const char* strText);

	virtual bool Load(TiXmlNode* Browser_root);

	virtual void Render(float fElapsedTime);

	virtual bool HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual bool HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam);

protected:
	CDXUTExternal(DXUT_CONTROL_TYPE type, const char* tag, const char* defaltName, CDXUTDialog* pDialog);

	virtual bool renderExternalFromText() = 0;

	bool m_loaded;
	IExternalMesh* m_externalMesh; // flash renderer
	bool m_bPressed;
	int m_prevX, m_prevY, m_prevWidth, m_prevHeight;
};

} // namespace KEP