///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "DXUTExternal.h"

namespace KEP {

const LPCSTR FLASH_TAG = "flash";

//-----------------------------------------------------------------------------
// Flash control
//-----------------------------------------------------------------------------
class CDXUTFlashPlayer : public CDXUTExternal, public IFlashPlayerControl {
public:
	CDXUTFlashPlayer(CDXUTDialog* pDialog = NULL);

	// override from CDXUTStatic or above
	virtual bool Load(TiXmlNode* root);
	virtual bool Save(TiXmlNode* root);

	// overrides from IFlashPlayerControl
	virtual const char* GetSwfName() const { return m_strText.c_str(); }
	virtual const char* GetSwfParameters() const { return m_parameters.c_str(); }
	virtual bool GetSizeToFit() const { return m_sizeToFit; }
	virtual bool GetLoopSwf() const { return m_loopSwf; }
	virtual bool GetWantInput() const { return m_wantInput; }

	virtual void SetSwfName(const char* name) { SetText(name); }
	virtual void SetSwfParameters(const char* params) { m_parameters = params; }
	virtual void SetSizeToFit(bool fit) { m_sizeToFit = fit; }
	virtual void SetLoopSwf(bool loop) { m_loopSwf = loop; }
	virtual void SetWantInput(bool wantIt) { m_wantInput = wantIt; }

	virtual bool CanHaveFocus() { return m_wantInput; }
	virtual void OnFocusIn();
	virtual void OnFocusOut();

private:
	bool renderExternalFromText();

	bool m_sizeToFit, // size the control to fit the movie
		m_loopSwf,
		m_wantInput; // do we want mouse/keyboard inputs?
	std::string m_swfName,
		m_parameters; // parameters to pass to swf
};

} // namespace KEP