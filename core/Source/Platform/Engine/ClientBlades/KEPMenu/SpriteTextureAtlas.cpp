///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "dxstdafx.h"
#include "SpriteTextureAtlas.h"
#include "D3D9DeviceWrapper.h"
#include "D3D9TextureWrapper.h"
#include <algorithm>

namespace KEP {

void SpriteTextureAtlas::beginFrame() {
	// Remove dead sprites
	for (auto itr = m_sprites.begin(); itr != m_sprites.end();) {
		if (itr->second.isDead()) {
			itr = m_sprites.erase(itr);
			m_layoutNeeded = true;
		} else {
			itr->second.decFTL();
			itr++;
		}
	}
}

void SpriteTextureAtlas::endFrame() {
	// No-op at this moment
}

void SpriteTextureAtlas::addSprite(IDirect3DTexture9* pSpriteTexture) {
	//assert(pSpriteTexture != nullptr);
	if (pSpriteTexture == nullptr || m_unsupportedSprites.find(pSpriteTexture) != m_unsupportedSprites.end()) {
		return;
	}

	auto itr = m_sprites.find(pSpriteTexture);
	if (itr == m_sprites.end()) {
		D3DSURFACE_DESC desc;
		pSpriteTexture->GetLevelDesc(0, &desc);
		if (desc.Format != D3DFMT_X8R8G8B8 && desc.Format != D3DFMT_A8R8G8B8 || desc.Pool != D3DPOOL_MANAGED) {
			m_unsupportedSprites.insert(pSpriteTexture);
		} else {
			m_sprites.insert(std::make_pair(pSpriteTexture, SpriteInfo(pSpriteTexture, desc.Width, desc.Height, CELL_SIZE, CELL_PADDING)));
			m_layoutNeeded = true;
		}
	} else {
		// Refresh sprite life
		itr->second.resetFTL();
	}
}

const SpriteInfo* SpriteTextureAtlas::getSpriteInfo(IDirect3DTexture9* pSpriteTexture) const {
	if (m_masterTexture != nullptr) {
		auto itr = m_sprites.find(pSpriteTexture);
		if (itr != m_sprites.end()) {
			return &itr->second;
		}
	}
	return nullptr;
}

bool SpriteTextureAtlas::updateMasterSheet(D3D9DeviceWrapper* device) {
	assert(device != nullptr);

	unsigned oldWidth = m_masterWidth;
	unsigned oldHeight = m_masterHeight;
	bool clearSpriteSheet = false; // clear sprite sheet before updating sprites
	bool updateAllSprites = false; // update all sprites, regardless of individual dirty flags

	if (m_layoutNeeded) {
		relayoutMasterSheet();
		updateAllSprites = m_masterTexture != nullptr; // refresh all sprites if layout changed
		m_layoutNeeded = false;
	}

	// Re-create master texture if dimension changed
	if (oldWidth != m_masterWidth || oldHeight != m_masterHeight) {
		if (m_masterTexture != nullptr) {
			m_masterTexture->Release();
			m_masterTexture = nullptr;

			// Invalid m_texRevision for all sprites so that they will be updated properly when master texture is reconstructed
			for (auto& pr : m_sprites) {
				pr.second.invalidate();
			}
		}

		if (m_masterWidth != 0 && m_masterHeight != 0) {
			if (D3D_OK != device->CreateTexture(m_masterWidth * CELL_SIZE, m_masterHeight * CELL_SIZE, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, &m_masterTexture, nullptr)) {
				assert(false);
				return false;
			}
		}
	} else {
		// Clear sprite sheet if layout changed and same texture reused?
		clearSpriteSheet = updateAllSprites;
	}

	if (m_masterWidth != 0 && m_masterHeight != 0) {
		if (m_masterTexture == nullptr) {
			assert(false);
			return false;
		}

		// Update pixels
		D3DLOCKED_RECT lockedRect;
		if (D3D_OK != m_masterTexture->LockRect(0, &lockedRect, NULL, 0)) {
			assert(false);
		} else {
			if (clearSpriteSheet) {
				// clear master texture
				memset(lockedRect.pBits, 0, lockedRect.Pitch * m_masterHeight * CELL_SIZE);
			}

			for (auto& pr : m_sprites) {
				SpriteInfo& spriteInfo = pr.second;

				// Check revision of the underlying texture
				int currTexRevision = 0;
				if (device->isTextureWrapper(spriteInfo.m_texture)) {
					auto pTextureWrapper = static_cast<D3D9TextureWrapper*>(pr.second.m_texture); // Safe to down cast
					currTexRevision = pTextureWrapper->getRevision();
				}

				// If all sprites are flagged for update, or current sprite older than the underlying texture
				if (updateAllSprites || spriteInfo.m_texRevision < currTexRevision) {
					updateMasterSheetSprite(device, lockedRect, spriteInfo);
					spriteInfo.m_texRevision = currTexRevision;
				}
			}

			m_masterTexture->UnlockRect(0);
		}
	}

	return true;
}

void SpriteTextureAtlas::updateMasterSheetSprite(D3D9DeviceWrapper* device, const D3DLOCKED_RECT& masterLockedRect, const SpriteInfo& spriteInfo) {
	D3DSURFACE_DESC desc;
	spriteInfo.m_texture->GetLevelDesc(0, &desc);
	assert(desc.Format == D3DFMT_A8R8G8B8 || desc.Format == D3DFMT_X8R8G8B8);
	if (desc.Format != D3DFMT_A8R8G8B8 && desc.Format != D3DFMT_X8R8G8B8) {
		return;
	}

	// Try obtaining underlying texture if it's wrapped, otherwise LockRect will mark the texture dirty again
	IDirect3DTexture9* pTexture = device->getD3DTexture(spriteInfo.m_texture);

	// Copy sprite into master sheet
	D3DLOCKED_RECT lockedRect;
	if (D3D_OK != pTexture->LockRect(0, &lockedRect, NULL, 0)) {
		assert(false);
	} else {
		D3DSURFACE_DESC masterDesc;
		m_masterTexture->GetLevelDesc(0, &masterDesc);

		RECT dstRect{
			LONG(spriteInfo.m_layoutRect.left * CELL_SIZE), // LEFT
			LONG(spriteInfo.m_layoutRect.top * CELL_SIZE), // TOP
			LONG(spriteInfo.m_layoutRect.left * CELL_SIZE + spriteInfo.m_texWidth), // RIGHT
			LONG(spriteInfo.m_layoutRect.top * CELL_SIZE + spriteInfo.m_texHeight), // BOTTOM
		};

		unsigned char* dstBuf = (unsigned char*)masterLockedRect.pBits;
		unsigned char* dstPtr = dstBuf + masterLockedRect.Pitch * dstRect.top + dstRect.left * sizeof(unsigned);
		unsigned char* srcBuf = (unsigned char*)lockedRect.pBits;
		unsigned char* srcPtr = srcBuf;

		for (size_t i = 0; i < spriteInfo.m_texHeight; i++) {
			assert(dstPtr < dstBuf + masterDesc.Height * masterLockedRect.Pitch);
			assert(dstPtr + sizeof(unsigned) * (spriteInfo.m_texWidth) <= dstBuf + masterDesc.Height * masterLockedRect.Pitch);
			if (desc.Format == D3DFMT_X8R8G8B8) {
				// X8R8G8B8: update alpha before copy
				unsigned* dstPix = (unsigned*)dstPtr;
				unsigned* srcPix = (unsigned*)srcPtr;
				for (size_t j = 0; j < spriteInfo.m_texWidth; j++) {
					dstPix[j] = srcPix[j] | 0xFF000000;
				}
			} else {
				// A8R8G8B8: straight copy
				memcpy(dstPtr, srcPtr, sizeof(unsigned) * (spriteInfo.m_texWidth));
			}
			dstPtr += masterLockedRect.Pitch;
			srcPtr += lockedRect.Pitch;
		}

#define HALF_PIXEL_BLEEDING_WORKAROUND

#ifdef HALF_PIXEL_BLEEDING_WORKAROUND
		// duplicate one extra row of pixels before first row and after last row, if the sprite is *NOT* placed at the very top / bottom of the master sheet
		// duplicate one extra column of pixels before first column, if the sprite is *NOT* placed at the very left of the master sheet
		// NOTE: we use a cell padding is big enough to prevent extra row and column from overwriting other sprites.

		for (int dstX = dstRect.left - 1; dstX <= dstRect.right; dstX++) {
			int srcX = std::min<int>(std::max<int>(dstX, dstRect.left), dstRect.right - 1);
			if (dstX < 0 || dstX >= m_masterWidth * CELL_SIZE) {
				// Dest coordinates outside master rect
				continue;
			}

			for (int dstY = dstRect.top - 1; dstY <= dstRect.bottom; dstY++) {
				if (dstX == dstRect.left - 1 || dstX == dstRect.right || dstY == dstRect.top - 1 || dstY == dstRect.bottom) {
					// Border only
					int srcY = std::min<int>(std::max<int>(dstY, dstRect.top), dstRect.bottom - 1);
					if (dstY < 0 || dstY >= m_masterHeight * CELL_SIZE) {
						// Dest coordinates outside master rect
						continue;
					}

					unsigned* dstPix = (unsigned*)(dstBuf + masterLockedRect.Pitch * dstY + dstX * sizeof(unsigned));
					unsigned* srcPix = (unsigned*)(dstBuf + masterLockedRect.Pitch * srcY + srcX * sizeof(unsigned));
					*dstPix = *srcPix;
				}
			}
		}
#endif

		spriteInfo.m_texture->UnlockRect(0);
	}
}

bool SpriteTextureAtlas::relayoutMasterSheet() {
	if (m_sprites.empty()) {
		return true;
	}

#if 0
	static LogInstance("MenuSystem");
	LogInfo(m_menuName << ", texture count: " << m_sprites.size());
#endif

	// Sort rectangles by height then by width
	std::vector<SpriteInfo> sorted, finalLayout;
	sorted.reserve(m_sprites.size());

	unsigned totalArea = 0;
	unsigned maxSpriteWidth = 0, maxSpriteHeight = 0;

	for (const auto& pr : m_sprites) {
		sorted.push_back(pr.second);
		auto& s = sorted.back();
		s.m_layoutRect.right -= s.m_layoutRect.left;
		s.m_layoutRect.left = 0;
		s.m_layoutRect.bottom -= s.m_layoutRect.top;
		s.m_layoutRect.top = 0;
		totalArea += s.m_layoutRect.right * s.m_layoutRect.bottom;
		maxSpriteWidth = std::max<unsigned int>(maxSpriteWidth, s.m_layoutRect.right);
		maxSpriteHeight = std::max<unsigned int>(maxSpriteHeight, s.m_layoutRect.bottom);
	}

	std::sort(sorted.begin(), sorted.end(), [this](const SpriteInfo& s1, const SpriteInfo& s2) {
		return s1.m_layoutRect.bottom > s2.m_layoutRect.bottom || s1.m_layoutRect.bottom == s2.m_layoutRect.bottom && s1.m_layoutRect.right > s2.m_layoutRect.right;
	});

	// Sprite sheet should be at least as big as the tallest and widest rectangles
	// Also constraint heights to allow proper aspect ratio
	unsigned minSheetWidth = maxSpriteWidth;
	unsigned minSheetHeight = std::max<unsigned int>(maxSpriteHeight, (unsigned)sqrt(totalArea) * 2 / 3);
	unsigned maxSheetHeight = std::max<unsigned int>(maxSpriteHeight, (unsigned)sqrt(totalArea));

	// A naive implementation for rectangle packing
	// Candidate heights for the sprite sheet is listed in following way:
	// Sheet height #N: Sum of heights of the first N tallest sprite
	// Use candidate heights to layout
	// Calculate the candidate width the following way:
	// ...
	// Record the most recently succeeded layout and total area.
	// If a new candidate sheet has an area greater than the known best layout, don't try it at all.

	unsigned bestH = 65535, bestW = 65535;
	unsigned candH = 0;
	bool succeeded = false;
	size_t i = 0;
	for (; i < sorted.size() && candH < minSheetHeight; i++) {
		candH += sorted[i].m_layoutRect.bottom;
	}

	for (; i < sorted.size() - 1; i++) {
		//candH += sorted[i++].m_layoutRect.bottom;
		candH += sorted[i].m_layoutRect.bottom;

		if (candH > maxSheetHeight && succeeded) {
			break;
		}

		unsigned candW = 0;
		unsigned sectW = 0;
		for (size_t j = 0; j < sorted.size(); j++) {
			if (j % (i + 1) == 0) {
				candW += sectW;
				sectW = 0;
			}
			sectW = std::max(sectW, (unsigned)sorted[i].m_layoutRect.right);
		}
		candW += sectW;
		candW = std::max(minSheetWidth, candW);

		// try laying out with new candidate sheet dimension
		auto tempLayout = sorted;
		unsigned actualW = 0, actualH = 0;
		if (layoutConstrained(candW, candH, tempLayout, actualW, actualH) && actualW * actualH < bestW * bestH) {
			bestW = actualW;
			bestH = actualH;
			finalLayout = tempLayout;
			succeeded = true;
		}
	}

	if (succeeded) {
		for (const auto& s : finalLayout) {
			auto itr = m_sprites.find(s.m_texture);
			assert(itr != m_sprites.end());
			if (itr != m_sprites.end()) {
				itr->second.m_layoutRect = s.m_layoutRect;
			}
		}

		m_masterWidth = bestW;
		m_masterHeight = bestH;
	} else {
		m_masterWidth = 0;
		m_masterHeight = 0;
	}

	return succeeded;
}

bool SpriteTextureAtlas::layoutConstrained(unsigned candWidth, unsigned candHeight, std::vector<SpriteInfo>& sprites, unsigned& outWidth, unsigned& outHeight) {
	allocateCells(candWidth, candHeight);
	outWidth = outHeight = 0;

	for (auto& spriteInfo : sprites) {
		bool placed = false;
		assert(spriteInfo.m_layoutRect.top == 0);
		assert(spriteInfo.m_layoutRect.left == 0);

		unsigned spriteWidth = spriteInfo.m_layoutRect.right;
		unsigned spriteHeight = spriteInfo.m_layoutRect.bottom;

		assert(candWidth >= spriteWidth);
		assert(candHeight >= spriteHeight);

		for (size_t i = 0; i < candWidth + 1 - spriteWidth; i++) {
			for (size_t j = 0; j < candHeight + 1 - spriteHeight; j++) {
				bool vacant = true;
				for (size_t u = 0; u < spriteWidth; u++) {
					for (size_t v = 0; v < spriteHeight; v++) {
						const auto& cellInfo = getCell(i + u, j + v);
						if (cellInfo.m_occupied) {
							vacant = false;
							j = std::max(j, cellInfo.m_nextY - 1);
							break;
						}
					}
					if (!vacant) {
						break;
					}
				}

				if (vacant) {
					for (size_t u = 0; u < spriteWidth; u++) {
						for (size_t v = 0; v < spriteHeight; v++) {
							markCell(i + u, j + v, i + spriteWidth, j + spriteHeight);
						}
					}

					placed = true;
					::OffsetRect(&spriteInfo.m_layoutRect, i, j);
					outWidth = std::max(outWidth, i + spriteWidth);
					outHeight = std::max(outHeight, j + spriteHeight);
					break;
				}
			}

			if (placed) {
				break;
			}
		}

		if (!placed) {
			return false;
		}
	}

	return true;
}

} // namespace KEP
