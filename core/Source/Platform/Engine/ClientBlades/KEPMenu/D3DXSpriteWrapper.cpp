///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "dxstdafx.h"
#include "D3DXSpriteWrapper.h"
#include "D3D9DeviceWrapper.h"
#include "KepMenu.h"
#include "SpriteTextureAtlas.h"

namespace KEP {

HRESULT D3DXSpriteWrapper::Create() {
	return D3DXCreateSprite(m_pd3dDeviceWrapper, &m_pd3dSprite);
}

STDMETHODIMP D3DXSpriteWrapper::Begin(THIS_ DWORD Flags) {
	m_currScaleX = m_currScaleY = 1.0f;
	return m_pd3dSprite->Begin(Flags);
}

STDMETHODIMP D3DXSpriteWrapper::End(THIS) {
	FlushDrawQueue(true);
	m_currScaleX = m_currScaleY = 1.0f;
	return m_pd3dSprite->End();
}

STDMETHODIMP D3DXSpriteWrapper::Flush(THIS) {
	FlushDrawQueue(false);
	m_currScaleX = m_currScaleY = 1.0f;
	return m_pd3dSprite->Flush();
}

STDMETHODIMP D3DXSpriteWrapper::SetTransform(THIS_ CONST D3DXMATRIX *pTransform) {
	// Only handles X/Y scaling as this is only thing we do in DXUT.
	// Additional work would be needed if we do more fancy things such as rotation.

	if (KEPMenu::Instance()->IsRenderOptionSet(MenuRenderOption::USE_DRAW_QUEUE)) {
		// Bug detection
		assert(pTransform->_12 == 0 && pTransform->_13 == 0 &&
			   pTransform->_21 == 0 && pTransform->_23 == 0 &&
			   pTransform->_31 == 0 && pTransform->_32 == 0 && pTransform->_33 == 1.0f &&
			   pTransform->_41 == 0 && pTransform->_42 == 0 && pTransform->_43 == 0);

		// Save current transform
		m_currScaleX = pTransform->_11;
		m_currScaleY = pTransform->_22;
		return S_OK;
	} else {
		return m_pd3dSprite->SetTransform(pTransform);
	}
}

STDMETHODIMP D3DXSpriteWrapper::Draw(THIS_ LPDIRECT3DTEXTURE9 pTexture, CONST RECT *pSrcRect, CONST D3DXVECTOR3 *pCenter, CONST D3DXVECTOR3 *pPosition, D3DCOLOR Color) {
	if (KEPMenu::Instance()->IsRenderOptionSet(MenuRenderOption::USE_DRAW_QUEUE)) {
		// Use draw command queue
		m_drawQueue.push_back(DrawInfo(pTexture, pSrcRect, pCenter, pPosition, Color, m_currScaleX, m_currScaleY, m_currTextureType));
		return S_OK;
	} else {
		// Send to ID3DXSprite directly
		return m_pd3dSprite->Draw(pTexture, pSrcRect, pCenter, pPosition, Color);
	}
}

void D3DXSpriteWrapper::FlushDrawQueue(bool end) {
	// If draw queue is not enabled, simply do nothing
	if (m_drawQueue.empty()) {
		return;
	}

	assert(m_currentMenu != nullptr);
	if (m_currentMenu == nullptr) {
		return;
	}

	SpriteTextureAtlas &atlas = m_currentMenu->GetTextureAtlas();

	// Flush queued draw commands
	// NOTE: submit each draw commands to ID3DXSprite with updated texture rect
	// if master texture sheet is ready for the desired loose texture. Otherwise
	// send original parameters. If all loose textures are updated in the master
	// sheet, D3DXSprite will combine all draw commands into one D3D draw call.
	LPDIRECT3DTEXTURE9 lastTexture = nullptr;
	for (const DrawInfo &drawInfo : m_drawQueue) {
		if (drawInfo.m_texType == DXUTTextureNode::TYPE_DYNAMIC) {
			// Skip dynamic textures
			continue;
		}
		if (lastTexture != drawInfo.m_pTexture) {
			lastTexture = drawInfo.m_pTexture;
			atlas.addSprite(lastTexture);
		}
	}

	// Update master texture sheet before submitting draw commands to sprite
	// NOTE: ID3DXFont::DrawText function has already been called at this moment. All loose textures are ready.
	atlas.updateMasterSheet(m_pd3dDeviceWrapper);

	for (const DrawInfo &drawInfo : m_drawQueue) {
		auto spriteInfo = atlas.getSpriteInfo(drawInfo.m_pTexture);
		// Set transform
		D3DXMATRIX matTransform;
		D3DXMatrixScaling(&matTransform, drawInfo.m_scaleX, drawInfo.m_scaleY, 1.0f);
		m_pd3dSprite->SetTransform(&matTransform);

		if (spriteInfo != nullptr) {
			// Use master sheet
			auto pTexRect = drawInfo.getRect();
			RECT texRect;
			if (pTexRect) {
				texRect = *pTexRect;
			} else {
				texRect.left = texRect.top = 0;
				texRect.right = spriteInfo->m_texWidth;
				texRect.bottom = spriteInfo->m_texHeight;
			}

			// Map original texture rect into master sheet
			::OffsetRect(&texRect, spriteInfo->m_layoutRect.left * atlas.getCellSize(), spriteInfo->m_layoutRect.top * atlas.getCellSize());

			// Call D3DX to draw the sprite
			m_pd3dSprite->Draw(atlas.getMasterTexture(), &texRect, &drawInfo.m_center, &drawInfo.m_position, drawInfo.m_color);
		} else {
			// Use loose sprites
			m_pd3dSprite->Draw(drawInfo.m_pTexture, drawInfo.getRect(), &drawInfo.m_center, &drawInfo.m_position, drawInfo.m_color);
		}
	}

	// Clear draw queue
	m_drawQueue.clear();
	m_drawQueue.reserve(DRAW_QUEUE_RESERVE_SIZE);
}

} // namespace KEP
