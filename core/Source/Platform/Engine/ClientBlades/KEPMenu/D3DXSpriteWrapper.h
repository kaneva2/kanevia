///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <d3dx9.h>
#include <string>
#include <vector>
#include <set>

namespace log4cplus {
class Logger;
}

namespace KEP {

class CDXUTDialog;
class D3D9DeviceWrapper;
class SpriteTextureAtlas;

class D3DXSpriteWrapper : public ID3DXSprite {
private:
	struct DrawInfo;
	const size_t DRAW_QUEUE_RESERVE_SIZE = 100;

public:
	D3DXSpriteWrapper(D3D9DeviceWrapper *pDeviceWrapper) :
			m_pd3dSprite(nullptr),
			m_pd3dDeviceWrapper(pDeviceWrapper),
			m_currScaleX(1.0f),
			m_currScaleY(1.0f),
			m_currentMenu(nullptr),
			m_currTextureType(0) {
		m_drawQueue.reserve(DRAW_QUEUE_RESERVE_SIZE);
	}

	HRESULT Create();

	void SetCurrentMenu(CDXUTDialog *menu) { m_currentMenu = menu; }
	void SetCurrentTextureType(int type) { m_currTextureType = type; }

	// Transparent pass-through
	virtual STDMETHODIMP QueryInterface(THIS_ REFIID iid, LPVOID *ppv) override { return m_pd3dSprite->QueryInterface(iid, ppv); }
	virtual STDMETHODIMP_(ULONG) AddRef(THIS) override { return m_pd3dSprite->AddRef(); }
	virtual STDMETHODIMP_(ULONG) Release(THIS) override { return m_pd3dSprite->Release(); } // Ideally we should set m_sprite to null and delete self if ref count hits 0. This works for now.
	virtual STDMETHODIMP GetDevice(THIS_ LPDIRECT3DDEVICE9 *ppDevice) override { return m_pd3dSprite->GetDevice(ppDevice); }
	virtual STDMETHODIMP GetTransform(THIS_ D3DXMATRIX *pTransform) override { return m_pd3dSprite->GetTransform(pTransform); }
	virtual STDMETHODIMP SetWorldViewRH(THIS_ CONST D3DXMATRIX *pWorld, CONST D3DXMATRIX *pView) override { return m_pd3dSprite->SetWorldViewRH(pWorld, pView); }
	virtual STDMETHODIMP SetWorldViewLH(THIS_ CONST D3DXMATRIX *pWorld, CONST D3DXMATRIX *pView) override { return m_pd3dSprite->SetWorldViewLH(pWorld, pView); }
	virtual STDMETHODIMP OnLostDevice(THIS) override { return m_pd3dSprite->OnLostDevice(); }
	virtual STDMETHODIMP OnResetDevice(THIS) override { return m_pd3dSprite->OnResetDevice(); }

	// Overrides
	virtual STDMETHODIMP Begin(THIS_ DWORD Flags) override;
	virtual STDMETHODIMP End(THIS) override;
	virtual STDMETHODIMP Flush(THIS) override;
	virtual STDMETHODIMP SetTransform(THIS_ CONST D3DXMATRIX *pTransform) override;
	virtual STDMETHODIMP Draw(THIS_ LPDIRECT3DTEXTURE9 pTexture, CONST RECT *pSrcRect, CONST D3DXVECTOR3 *pCenter, CONST D3DXVECTOR3 *pPosition, D3DCOLOR Color) override;

private:
	void FlushDrawQueue(bool end);

private:
	struct DrawInfo {
		LPDIRECT3DTEXTURE9 m_pTexture;
		bool m_texRectIsNull;
		RECT m_texRect;
		D3DXVECTOR3 m_position;
		D3DXVECTOR3 m_center;
		D3DCOLOR m_color;
		float m_scaleX, m_scaleY;
		int m_texType;

		DrawInfo(LPDIRECT3DTEXTURE9 pTexture, CONST RECT *pSrcRect, CONST D3DXVECTOR3 *pCenter, CONST D3DXVECTOR3 *pPosition, D3DCOLOR Color, float scaleX, float scaleY, int texType) :
				m_pTexture(pTexture),
				m_texRectIsNull(true),
				m_position(0.0f, 0.0f, 0.0f),
				m_center(0.0f, 0.0f, 0.0f),
				m_color(Color),
				m_scaleX(scaleX),
				m_scaleY(scaleY),
				m_texType(texType) {
			if (pSrcRect) {
				m_texRectIsNull = false;
				m_texRect = *pSrcRect;
			}
			if (pCenter) {
				m_center = *pCenter; // if pCenter is null, use (0, 0, 0) according to ID3DXSprite::Draw()
			}
			if (pPosition) {
				m_position = *pPosition; // if pPosition is null, use (0, 0, 0) according to ID3DXSprite::Draw()
			}
		}

		const RECT *getRect() const {
			return m_texRectIsNull ? nullptr : &m_texRect;
		}
	};

	typedef std::vector<DrawInfo> DrawQueue;
	typedef std::set<LPDIRECT3DTEXTURE9> TexSet;

private:
	ID3DXSprite *m_pd3dSprite; // Pointer to the underlying D3DX sprite
	D3D9DeviceWrapper *m_pd3dDeviceWrapper;
	DrawQueue m_drawQueue;
	float m_currScaleX, m_currScaleY;
	int m_currTextureType; // DXUTTextureNode::TYPE
	CDXUTDialog *m_currentMenu;
};

} // namespace KEP
