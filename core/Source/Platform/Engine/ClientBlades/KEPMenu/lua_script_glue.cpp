#include "WeakRef.h"
#include "LuaHelper.h"

namespace script_glue {

int my_dofile(lua_State *L, int nresults, const char *name) {
	// load the script
	int rc = LuaLoadFileProtected(L, name, true);
	if (rc)
		return rc; // there was a compile-time error. it's on the top of the stack

				   // there were no compilation errors. run the script
				   // if there is a runtime error it is on the top of the stack
	return lua_pcall(L, 0, nresults, 0);
}

std::string Lua_GetErrorStr(lua_State *L) {
	std::string errStr;

	// Call Lua debug.traceback()
	lua_getglobal(L, "debug");
	lua_pushstring(L, "traceback");
	lua_gettable(L, -2);
	if (lua_isfunction(L, -1)) {
		if (lua_pcall(L, 0, 1, 0) == 0) {
			errStr = lua_tostring(L, -1);
		}
		else {
			errStr = __FUNCTION__ ": Failed lua_pcall(debug.traceback)";
		}
	}
	else {
		errStr = __FUNCTION__ ": Failed lua_isfunction(debug.traceback)";
	}
	lua_pop(L, 3);

	return errStr;
}

void error_null_parameter(lua_State *L, int pos, const char *expectedTypeStr) {
	LogInstance("Script");
	std::string errStr = Lua_GetErrorStr(L);
	LogError("Lua Script Error...\n"
		<< "Can't convert parameter " << pos << " from type <null> to expected " << expectedTypeStr << "\n"
		<< errStr);
}

void error_type_mismatch(lua_State *L, int pos, const char *expectedTypeStr) {
	LogInstance("Script");
	int t = lua_type(L, pos);
	const char *typeStr = lua_typename(L, t);
	std::string errStr = Lua_GetErrorStr(L);
	LogError("Lua Script Error...\n"
		<< "Can't convert parameter " << pos << " from type " << typeStr << " to expected " << expectedTypeStr << "\n"
		<< errStr);
}

int WeakRefGC(lua_State *L) {
	void *ud = lua_touserdata(L, -1);

	_ASSERTE(ud);

	if (ud) {
		WeakRef *weakRef = *static_cast<WeakRef **>(ud);

		_ASSERTE(weakRef);

		if (weakRef) {
			/* registry[weakRef] = nil */
			lua_pushlightuserdata(L, weakRef);
			lua_pushnil(L);
			lua_settable(L, LUA_REGISTRYINDEX);

			/* See if this is the last VM to release this weak reference */
			if (--weakRef->_nref == 0) {
				/* Update target to no longer have a weak reference site */
				if (weakRef->_target)
					weakRef->_target->ClearWeakRef();

				/* Dispose of the weak reference */
				delete weakRef;
			}
		}
	}

	return 0;
}

int WeakRefToString(lua_State *L) {
	void *ud = lua_touserdata(L, -1);

	_ASSERTE(ud);

	if (ud) {
		WeakRef *weakRef = *static_cast<WeakRef **>(ud);

		_ASSERTE(weakRef);

		if (weakRef) {
			if (weakRef->_target)
				lua_pushfstring(L, "[handle:%d]", weakRef->_target);
			else
				lua_pushstring(L, "[handle:null]");

			return 1;
		}
	}

	/* Error unpacking args */
	return 0;
}

int WeakRefEq(lua_State *L) {
	void *ud0 = lua_touserdata(L, -1);
	void *ud1 = lua_touserdata(L, -2);

	_ASSERTE(ud0 && ud1);

	if (ud0 && ud1) {
		WeakRef *weakRef0 = *static_cast<WeakRef **>(ud0);
		WeakRef *weakRef1 = *static_cast<WeakRef **>(ud1);

		_ASSERTE(weakRef0);
		_ASSERTE(weakRef1);

		if (weakRef0 && weakRef1) {
			bool result = (weakRef0->_target == weakRef1->_target);
			lua_pushboolean(L, result);
		}
	}

	/* Error unpacking args */
	return 0;
}

} // namespace script_glue