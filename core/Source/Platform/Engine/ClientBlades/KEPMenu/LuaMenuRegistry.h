#pragma once

#include "LuaHelper.h"
#include "script_glue.h"

namespace KEP {

void add_registry(lua_State* native, script_glue::registration_vector* native_registry);
void remove_registry(void* vm);
void remove_all_registries();

} // namespace KEP