///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "dxstdafx.h"
#include "D3D9DeviceWrapper.h"
#include "D3D9TextureWrapper.h"

namespace KEP {

STDMETHODIMP_(ULONG)
D3D9DeviceWrapper::Release(THIS) {
	auto rc = m_pd3dDevice->Release();
	if (rc == 0) {
		delete this;
	}
	return rc;
}

STDMETHODIMP D3D9DeviceWrapper::CreateTexture(THIS_ UINT Width, UINT Height, UINT Levels, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DTexture9** ppTexture, HANDLE* pSharedHandle) {
	IDirect3DTexture9* pD3DTexture = nullptr;
	auto rc = m_pd3dDevice->CreateTexture(Width, Height, Levels, Usage, Format, Pool, &pD3DTexture, pSharedHandle);

	if (rc == D3D_OK && pD3DTexture != nullptr) {
		auto pTextureWrapper = new D3D9TextureWrapper(this, pD3DTexture);
		std::lock_guard<std::mutex> lock(m_textureWrapperMutex);
		m_textureWrappers.insert(static_cast<IDirect3DBaseTexture9*>(pTextureWrapper));
		*ppTexture = pTextureWrapper;
	} else {
		*ppTexture = pD3DTexture;
	}
	return rc;
}

STDMETHODIMP D3D9DeviceWrapper::UpdateTexture(THIS_ IDirect3DBaseTexture9* pSourceTexture, IDirect3DBaseTexture9* pDestinationTexture) {
	assert(false); // *NOT* tested. It's probably never called by a sprite or font.
	return m_pd3dDevice->UpdateTexture(getD3DTexture(pSourceTexture), getD3DTexture(pDestinationTexture));
}

STDMETHODIMP D3D9DeviceWrapper::GetTexture(THIS_ DWORD Stage, IDirect3DBaseTexture9** ppTexture) {
	assert(false); // *NOT* tested. It's probably never called by a sprite or font.
	return m_pd3dDevice->GetTexture(Stage, ppTexture);
}

STDMETHODIMP D3D9DeviceWrapper::SetTexture(THIS_ DWORD Stage, IDirect3DBaseTexture9* pTexture) {
	return m_pd3dDevice->SetTexture(Stage, getD3DTexture(pTexture)); // Translate wrapper into underlying texture, if any
}

void D3D9DeviceWrapper::removeTextureWrapper(IDirect3DBaseTexture9* pTexture) {
	std::lock_guard<std::mutex> lock(m_textureWrapperMutex);
	m_textureWrappers.erase(pTexture);
}

bool D3D9DeviceWrapper::isTextureWrapper(IDirect3DBaseTexture9* pTextureOrWrapper) {
	std::lock_guard<std::mutex> lock(m_textureWrapperMutex);
	auto itr = m_textureWrappers.find(pTextureOrWrapper);
	return itr != m_textureWrappers.end();
}

IDirect3DBaseTexture9* D3D9DeviceWrapper::getD3DTexture(IDirect3DBaseTexture9* pTextureOrWrapper) {
	std::lock_guard<std::mutex> lock(m_textureWrapperMutex);
	auto itr = m_textureWrappers.find(pTextureOrWrapper);
	if (itr != m_textureWrappers.end()) {
		// Input is a wrapper
		auto pTextureWrapper = static_cast<D3D9TextureWrapper*>(*itr); // Safe to down cast
		return pTextureWrapper->getD3DTexture();
	}

	// Input is already an D3D9 texture
	return pTextureOrWrapper;
}

IDirect3DTexture9* D3D9DeviceWrapper::getD3DTexture(IDirect3DTexture9* pTextureOrWrapper) {
	std::lock_guard<std::mutex> lock(m_textureWrapperMutex);
	auto itr = m_textureWrappers.find(pTextureOrWrapper);
	if (itr != m_textureWrappers.end()) {
		// Input is a wrapper
		auto pTextureWrapper = static_cast<D3D9TextureWrapper*>(*itr); // Safe to down cast
		return pTextureWrapper->getD3DTexture();
	}

	// Input is already an D3D9 texture
	return pTextureOrWrapper;
}

} // namespace KEP
