///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPCommon.h"

#include <usp10.h>
#include <dimm.h>
#include <d3d9.h>
#include <d3dx9.h>

#include "..\imenugui.h"
#include "DXUTMisc.h"
#include "DXUTSelectionHandles.h"
#include "..\gui_events.h"
#include <string>
#include <map>
#include <stack>
#include <vector>

class TiXmlNode;
class TiXmlDocument;

namespace KEP {

static const char* const DISPLAY_TAG = "display";
static const char* const BACKGROUND_DISPLAY_TAG = "backgroundDisplay"; // legacy CDXUTButton
static const char* const NORMAL_DISPLAY_TAG = "normalDisplay"; // for CDXUTButton
static const char* const MOUSE_OVER_DISPLAY_TAG = "mouseOverDisplay"; // for CDXUTButton
static const char* const FOCUSED_DISPLAY_TAG = "focusedDisplay"; // for CDXUTButton
static const char* const PRESSED_DISPLAY_TAG = "pressedDisplay"; // for CDXUTButton
static const char* const DISABLED_DISPLAY_TAG = "disabledDisplay"; // for CDXUTButton
static const char* const MAIN_DISPLAY_TAG = "mainDisplay";
static const char* const BOX_DISPLAY_TAG = "boxDisplay";
static const char* const CHECK_DISPLAY_TAG = "checkDisplay";
static const char* const BUTTON_DISPLAY_TAG = "buttonDisplay";
static const char* const DROP_DOWN_DISPLAY_TAG = "dropdownDisplay";
static const char* const SELECTION_DISPLAY_TAG = "selectionDisplay";
static const char* const TRACK_DISPLAY_TAG = "trackDisplay";
static const char* const TEXT_AREA_DISPLAY_TAG = "textAreaDisplay";
static const char* const TOP_LEFT_BORDER_DISPLAY_TAG = "topLeftBorderDisplay";
static const char* const TOP_BORDER_DISPLAY_TAG = "topBorderDisplay";
static const char* const TOP_RIGHT_BORDER_DISPLAY_TAG = "topRightBorderDisplay";
static const char* const LEFT_BORDER_DISPLAY_TAG = "leftBorderDisplay";
static const char* const RIGHT_BORDER_DISPLAY_TAG = "rightBorderDisplay";
static const char* const LOWER_LEFT_BORDER_DISPLAY_TAG = "lowerLeftBorderDisplay";
static const char* const LOWER_BORDER_DISPLAY_TAG = "lowerBorderDisplay";
static const char* const LOWER_RIGHT_BORDER_DISPLAY_TAG = "lowerRightBorderDisplay";
static const char* const INDICATOR_DISPLAY_TAG = "indicatorDisplay";
static const char* const UP_ARROW_DISPLAY_TAG = "upArrowDisplay";
static const char* const DOWN_ARROW_DISPLAY_TAG = "downArrowDisplay";
static const char* const ICON_DISPLAY_TAG = "iconDisplay";

//--------------------------------------------------------------------------------------
// Enums for pre-defined control types
//--------------------------------------------------------------------------------------
enum DXUT_CONTROL_TYPE {
	DXUT_CONTROL_BUTTON,
	DXUT_CONTROL_STATIC,
	DXUT_CONTROL_CHECKBOX,
	DXUT_CONTROL_RADIOBUTTON,
	DXUT_CONTROL_COMBOBOX,
	DXUT_CONTROL_SLIDER,
	DXUT_CONTROL_EDITBOX,
	DXUT_CONTROL_IMEEDITBOX,
	DXUT_CONTROL_LISTBOX,
	DXUT_CONTROL_SCROLLBAR,
	DXUT_CONTROL_IMAGE,
	DXUT_CONTROL_FLASH,
	DXUT_CONTROL_BROWSER,
	DXUT_CONTROL_COMMENT,
	DXUT_CONTROL_SIMPLEEDITBOX,
};

// this represents the 'bullet' character
#if UNICODE
#define PASSWORD_CHAR 183
#else
#define PASSWORD_CHAR -73
#endif

// DRF - Added
bool DXUT_TranslateToSetLang(const std::string& ttLang);

//--------------------------------------------------------------------------------------
// Forward declarations
//--------------------------------------------------------------------------------------
class D3D9DeviceWrapper;
class D3DXSpriteWrapper;
class SpriteTextureAtlas;
class CDXUTDialog;
class CDXUTControl;
class CDXUTButton;
class CDXUTStatic;
class CDXUTImage;
class CDXUTCheckBox;
class CDXUTRadioButton;
class CDXUTComboBox;
class CDXUTSlider;
class CDXUTEditBox;
class CDXUTIMEEditBox;
class CDXUTSimpleEditBox;
class CDXUTListBox;
class CDXUTScrollBar;
class CDXUTElement;
struct DXUTElementHolder;
struct DXUTTextureNode;
class CDXUTFontNode;
class CDXUTFlashPlayer;

//Ankit ----- word-wrapping class
class WordWrapper;

class WordWrapper {
private:
	std::wstring m_formattedString;
	std::wstring m_lastCachedString;
	const CDXUTElement* m_lastCachedElement;
	RECT m_lastCachedRect;
	bool m_lastCachedShadowFlag;

	//flag to check if any of the values were changed requiring to redo word-wrapping
	bool m_needToRecalcWordWrapping;

	bool CalculateWordWrappedStrings(IMenuDialog* m_pDialog, const wchar_t* strText, CDXUTElement* pElement, RECT* prcDest, bool bShadow);

public:
	int DrawWordWrappedText(IMenuDialog* m_pDialog, const wchar_t* strText, CDXUTElement* pElement, RECT* prcDest, bool bShadow = false, int nCount = -1);

	//getters and setters
	void Setm_lastCachedString(const wchar_t* strText);
	void Setm_lastCachedElement(CDXUTElement* pElement);
	void Setm_lastCachedRect(RECT* prcDest);
	void Setm_lastCachedShadowFlag(bool bShadow);
	void SetRecalcFlag(bool needToRecalc);

	std::wstring Getm_lastCachedString() {
		return m_lastCachedString;
	}
	const CDXUTElement* Getm_lastCachedElement() {
		return m_lastCachedElement;
	}
	RECT Getm_lastCachedRect() {
		return m_lastCachedRect;
	}
	bool Getm_lastCachedShadowFlag() {
		return m_lastCachedShadowFlag;
	}
	bool GetNeedToRecalc() {
		return m_needToRecalcWordWrapping;
	}
};

//typedef bool (CALLBACK *PCALLBACKDXUTDIALOGGUIEVENT) ( GUI_EVENT nEvent, CDXUTDialog* pDialog, POINT pt );
//typedef bool (CALLBACK *PCALLBACKDXUTCONTROLGUIEVENT) ( GUI_EVENT nEvent, int nControlID, IMenuControl* pControl, POINT pt );

struct DXUTBlendColor : public IMenuBlendColor {
	DXUTBlendColor() {
		Init(D3DCOLOR_ARGB(255, 255, 255, 255), D3DCOLOR_ARGB(255, 255, 255, 255), D3DCOLOR_ARGB(255, 255, 255, 255));
	}
	virtual void Init(D3DCOLOR defaultColor, D3DCOLOR disabledColor = D3DCOLOR_ARGB(0, 128, 128, 128), D3DCOLOR hiddenColor = 0);
	virtual void Blend(DXUT_CONTROL_STATE iState, float fElapsedTime, float fRate = 0.7f);

	virtual void SetColor(DXUT_CONTROL_STATE state, D3DCOLOR color) {
		assert(state >= 0 && state < DXUT_CONTROL_STATES_MAX);
		m_States[state] = color;
	}
	virtual D3DCOLOR GetColor(DXUT_CONTROL_STATE state) {
		assert(state >= 0 && state < DXUT_CONTROL_STATES_MAX);
		return m_States[state];
	}

	virtual void SetCurrentColor(D3DCOLOR color) {
		m_Current = color;
	}
	virtual D3DXCOLOR GetCurrentColor() const {
		return m_Current;
	}

	virtual bool Load(TiXmlNode* root, unsigned int iStatesToLoad = DXUT_STATE_FLAG_ALL);
	virtual bool Save(TiXmlNode* parent, unsigned int iStatesToSave = DXUT_STATE_FLAG_ALL);

protected:
	D3DCOLOR m_States[DXUT_CONTROL_STATES_MAX]; // Modulate colors for all possible control states
	D3DXCOLOR m_Current;
};

struct DXUT_SCREEN_VERTEX {
	float x, y, z, h;
	D3DCOLOR color;
	float tu, tv;

	static DWORD FVF;
};

//-----------------------------------------------------------------------------
// Contains all the display tweakables for a sub-control
//-----------------------------------------------------------------------------
class CDXUTElement : public IMenuElement {
public:
	CDXUTElement() :
			iTexture(0), iProgressTexture(-1), m_iFont(0), m_dwTextFormat(DT_LEFT | DT_TOP | DT_SINGLELINE), m_textOffsetX(0), m_textOffsetY(0), failedTextureRetries(0), failedTexture(false), failedFontOnClientMinimize(false), texInfoNeedsUpdate(false), textureWidth(0), textureHeight(0) {
		EmptyTextureCoords();
		EmptyTextureSlices();
	}

	virtual IMenuFont* GetFont(IMenuDialog* pDialog);
	virtual size_t GetFontIndex();
	virtual UINT GetTextFormat() {
		return m_dwTextFormat;
	}
	virtual IMenuBlendColor* GetTextureColor() {
		return (IMenuBlendColor*)&textureColor;
	}
	virtual IMenuBlendColor* GetFontColor() {
		return (IMenuBlendColor*)&fontColor;
	}
	virtual IMenuBlendColor* GetFontColorBG() {
		return (IMenuBlendColor*)&fontColorBG;
	}
	virtual std::stack<D3DCOLOR> GetHTMLFontColorStack() {
		return htmlFontColor;
	}
	virtual std::stack<D3DCOLOR> GetHTMLFontColorBGStack() {
		return htmlFontColorBG;
	}

	virtual RECT GetTextureCoords() const {
		return this->textureCoords;
	}
	virtual RECT GetTextureSlices() const {
		return this->textureSlices;
	}

	virtual void SetTextureCoords(RECT coords) {
		this->textureCoords = coords;
	}
	virtual void SetTextureSlices(RECT slices) {
		this->textureSlices = slices;
	}

	virtual void EmptyTextureCoords() {
		SetRectEmpty(&(this->textureCoords));
	}
	virtual void EmptyTextureSlices() {
		SetRectEmpty(&(this->textureSlices));
	}

	virtual size_t GetTextureIndex() {
		return iTexture;
	}
	virtual int GetProgressTextureIndex() {
		return iProgressTexture;
	}
	virtual const char* GetTextureNamePtr(IMenuDialog* pDialog);
	virtual std::string GetTextureName(IMenuDialog* pDialog);
	virtual UINT GetTextureWidth(IMenuDialog* pDialog);
	virtual UINT GetTextureHeight(IMenuDialog* pDialog);
	virtual UINT GetProgressTextureWidth(IMenuDialog* pDialog) {
		return (UINT)progressTextureWidth;
	}
	virtual UINT GetProgressTextureHeight(IMenuDialog* pDialog) {
		return (UINT)progressTextureHeight;
	}

	void SetTexture(size_t iTexture, RECT* prcTexture, D3DCOLOR defaultTextureColor = D3DCOLOR_ARGB(255, 255, 255, 255));
	void SetFont(size_t iFont, D3DCOLOR defaultFontColor = D3DCOLOR_ARGB(255, 255, 255, 255), DWORD dwTextFormat = DT_CENTER | DT_VCENTER);
	virtual void SetFontIndex(size_t iFont) {
		this->m_iFont = iFont;
	}
	virtual void SetTextFormat(UINT textFormat) {
		m_dwTextFormat = textFormat;
	}

	RECT GetTextRect(RECT* prcDest);

	void SetTextOffsetX(int _offsetX) {
		m_textOffsetX = _offsetX;
	}
	void SetTextOffsetY(int _offsetY) {
		m_textOffsetY = _offsetY;
	}

	int GetTextOffsetX() {
		return m_textOffsetX;
	}
	int GetTextOffsetY() {
		return m_textOffsetY;
	}

	bool AddTexture(IMenuDialog* pDialog, const std::string& sTextureName, RECT coords, RECT slices);
	bool AddExternalTexture(IMenuDialog* pDialog, const std::string& sTextureName);
	bool AddProgressTexture(IMenuDialog* pDialog, const std::string& sTextureName);
	bool AddDynamicTexture(IMenuDialog* pDialog, unsigned dynTextureId);

	bool AddFont(IMenuDialog* pDialog, const std::string& sFaceName, long height, long weight, bool italic, bool underline);

	bool Load(TiXmlNode* root, IMenuDialog* pDialog, unsigned int iUsesFontFlags = DXUT_STATE_FLAG_ALL, unsigned int iUsesTextureFlags = DXUT_STATE_FLAG_ALL);
	bool Save(TiXmlNode* parent, IMenuDialog* pDialog, unsigned int iUsesFontFlags = DXUT_STATE_FLAG_ALL, unsigned int iUsesTextureFlags = DXUT_STATE_FLAG_ALL);

	void Refresh();

	bool RetryFailedTexture(IMenuDialog* pDialog);

private:
	size_t iTexture; // Index of the texture for this Element
	int iProgressTexture; // Index of the 'loading' texture when the main texture is pending

	size_t m_iFont; // Index of the font for this Element

	RECT textureCoords; // Bounding rect of this element on the composite texture
	RECT textureSlices; // Pixel offsets of the slices from each edge

	DXUTBlendColor textureColor;

	DXUTBlendColor fontColor;
	DXUTBlendColor fontColorBG;

	// DRF - Added - Enable html colorBG
	std::stack<D3DCOLOR> htmlFontColor;
	std::stack<D3DCOLOR> htmlFontColorBG;

private:
	// DRF - Added Failed Texture Handling
	bool failedTexture;
	size_t failedTextureRetries;
	std::string failedTextureFilename;
	RECT failedTextureCoords;
	RECT failedTextureSlices;

	//Ankit ----- flag to indicate if the font for this element failed on creation
	bool failedFontOnClientMinimize;
	IMenuDialog* failedFontInDialog;
	std::string failedFontName;
	long failedFontWeight;
	long failedFontHeight;
	bool failedFontUnderline;
	bool failedFontItalic;

private:
	DWORD m_dwTextFormat; // The format argument to DrawText
	int m_textOffsetX; // The X Offset for the text on this element.
	int m_textOffsetY; // The Y Offset for the text on this element.

	//Ankit ----- storing the texture width and height info in the Element itself
	bool texInfoNeedsUpdate;

	DWORD textureWidth;
	DWORD textureHeight;

	DWORD progressTextureWidth;
	DWORD progressTextureHeight;

protected:
	bool LoadFont(TiXmlNode* font_root, IMenuDialog* pDialog);
	bool LoadTexture(TiXmlNode* texture_root, IMenuDialog* pDialog);

	bool SaveFont(TiXmlNode* parent, IMenuDialog* pDialog);
	bool SaveTexture(TiXmlNode* parent, IMenuDialog* pDialog);
};

// DRF - Added Failed Texture Handling
bool NeedsWindowRefresh();
void FlagNeedsWindowRefresh();
void FlagWindowRefreshed();

//-----------------------------------------------------------------------------
// Base class for controls
//-----------------------------------------------------------------------------
#define MAX_COMMENT 128
#define MAX_TOOLTIP 256
#define MAX_XMLCSS 64

class CDXUTControl : public IMenuControl {
public:
	CDXUTControl(IMenuDialog* pDialog = NULL);
	virtual ~CDXUTControl();

	bool Load(TiXmlNode* root);
	virtual bool LoadElements(TiXmlNode* elements_root);
	virtual bool Save(TiXmlNode* parent);
	bool SaveElements(TiXmlNode* parent);

	virtual HRESULT OnInit() {
		return S_OK;
	}
	virtual void Refresh();
	virtual void Render(float fElapsedTime);

	// Windows message handler
	virtual bool MsgProc(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/) {
		return false;
	}

	virtual bool HandleKeyboard(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/) {
		return false;
	}
	virtual bool HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam);

	virtual bool CanHaveFocus() {
		return false;
	}
	virtual void OnFocusIn();
	virtual void OnFocusOut();
	virtual void OnSelectionOn();
	virtual void OnSelectionOff() {
		m_bIsSelected = false;
	}
	virtual void OnMouseEnter(POINT pt);
	virtual void OnMouseLeave();
	virtual void OnHotkey() {}
	virtual void OnMoved();
	virtual void OnResized();

	virtual BOOL ContainsPoint(POINT pt);
	virtual BOOL ClipRectContainsPoint(POINT pt);

	IMenuDialog* GetDialog() {
		return m_pDialog;
	}

	virtual bool GetEnabled() {
		return m_bEnabled;
	}

	virtual bool GetVisible() {
		return m_bVisible;
	}

	virtual bool GetMouseOver() {
		return m_bMouseOver;
	}

	virtual bool GetIsInFocus() {
		return m_bIsInFocus;
	}

	UINT GetType() const {
		return m_Type;
	}

	std::string GetTag() const {
		return m_Tag;
	}

	int GetID() const {
		return m_ID;
	}

	const char* GetName() {
		return &m_Name[0];
	}

	bool GetCursorLocation(POINT& pt) const;
	int GetCursorLocationX() const;
	int GetCursorLocationY() const;

	bool GetLocation(POINT& pt, bool useOffset = true) const {
		pt.x = (useOffset) ? m_x + m_xOffset : m_x;
		pt.y = (useOffset) ? m_y + m_yOffset : m_y;
		return true;
	}
	int GetLocationX(bool useOffset = true) const {
		return (useOffset) ? m_x + m_xOffset : m_x;
	}
	int GetLocationY(bool useOffset = true) const {
		return (useOffset) ? m_y + m_yOffset : m_x;
	}

	bool GetOffset(POINT& pt) const {
		pt.x = m_xOffset;
		pt.y = m_yOffset;
		return true;
	}
	int GetOffsetX() const {
		return m_xOffset;
	}
	int GetOffsetY() const {
		return m_yOffset;
	}

	int GetWidth() const {
		return m_width;
	}
	int GetHeight() const {
		return m_height;
	}
	UINT GetHotkey() const {
		return m_nHotkey;
	}
	void* GetUserData() const {
		return m_pUserData;
	}
	IMenuElement* GetElement(UINT iElement);
	virtual IMenuElement* GetElement(std::string elementTag);
	int GetFontVariant(int base_font_id, int Styles);
	bool GetIsDefault() const {
		return m_bIsDefault;
	}
	int GetNumElementTags() const {
		return (int)m_elementTags.size();
	}
	const char* GetElementTag(int index);
	int GetGroupId() const {
		return m_groupID;
	}
	const char* GetLinkClicked() const {
		return m_sLinkClicked.c_str();
	}
	unsigned int GetElementTagUsesFont(std::string elementTag);
	unsigned int GetElementTagUsesTexture(std::string elementTag);

	bool GetMovable() const {
		return m_bMovable;
	}
	bool GetResizable() const {
		return m_bResizable;
	}
	RECT GetClipRect() const override {
		return m_clipRect;
	}
	bool IsClipped() const override {
		return m_clipped;
	}
	bool IsCulled() const override {
		return m_culled;
	}

	virtual bool SetEnabled(bool bEnabled);

	virtual bool SetVisible(bool bVisible);

	bool SetID(int ID) {
		m_ID = ID;
		return true;
	}

	bool SetName(const char* strName);

	bool SetLocation(int x, int y) {
		m_x = x;
		m_y = y;
		UpdateRects();
		return true;
	}
	bool SetLocationX(int x) {
		m_x = x;
		UpdateRects();
		return true;
	}
	bool SetLocationY(int y) {
		m_y = y;
		UpdateRects();
		return true;
	}

	bool SetOffset(int _xOffset, int _yOffset) {
		m_xOffset = _xOffset;
		m_yOffset = _yOffset;
		UpdateRects();
		return true;
	}
	bool SetOffsetX(int _xOffset) {
		m_xOffset = _xOffset;
		UpdateRects();
		return true;
	}
	bool SetOffsetY(int _yOffset) {
		m_yOffset = _yOffset;
		UpdateRects();
		return true;
	}

	bool SetWidth(int width) {
		m_width = width;
		UpdateRects();
		return true;
	}
	bool SetHeight(int height) {
		m_height = height;
		UpdateRects();
		return true;
	}
	bool SetSize(int width, int height) {
		m_width = width;
		m_height = height;
		UpdateRects();
		return true;
	}
	bool SetHotkey(UINT nHotkey) {
		m_nHotkey = nHotkey;
		return true;
	}
	bool SetUserData(void* pUserData) {
		m_pUserData = pUserData;
		return true;
	}
	virtual bool SetTextColor(D3DCOLOR Color);
	HRESULT SetElement(UINT iElement, CDXUTElement* pElement);
	bool SetIsDefault(bool bIsDefault) {
		m_bIsDefault = bIsDefault;
		return true;
	}
	bool SetGroupId(int ID) {
		m_groupID = ID;
		return true;
	}

	bool SetMovable(bool enable) {
		m_bMovable = enable;
		return true;
	}
	bool SetResizable(bool enable) {
		m_bResizable = enable;
		return true;
	}

	const char* GetComment() {
		return m_comment;
	}
	bool SetComment(const char* strText);

	const char* GetToolTip() {
		return m_tooltip;
	}
	bool SetToolTip(const char* strText);

	const char* GetXMLCSS() {
		return m_xmlcss;
	}
	bool SetXMLCSS(const char* strText);

	bool SetXml(const char* xml) {
		if (xml)
			m_xml = xml;
		return true;
	}
	const char* GetXml() {
		return m_xml.c_str();
	}

	bool SetClipRect(int left, int top, int right, int bottom) override;

	static bool PushColor(IMenuBlendColor& cur_color, D3DCOLOR color);
	static bool PopColor(IMenuBlendColor& cur_color);

	static int HexDigit(wchar_t ch);
	static D3DCOLOR ParseColor(const wchar_t* color_string);

private:
	bool m_bVisible; // Shown/hidden flag
	bool m_bMouseOver; // Mouse pointer is above control
	bool m_bIsInFocus; // Control has input focus
	bool m_bIsDefault; // Is the default control

	bool m_bMovable;
	bool m_bResizable;

	bool m_bIsSelected; // Control has been selected for editing

	// Size, scale, and positioning members
	int m_x, m_y;
	int m_xOffset, m_yOffset;
	int m_width, m_height;

	// These members are set by the container
public:
	IMenuDialog* m_pDialog; // DRF - This is entirely unsafe without refptr validation

public:
	typedef std::map<int, CDXUTElement*> ElementMap;
	typedef ElementMap::iterator ElementIterator;
	ElementMap m_Elements; // All display elements

protected:
	virtual void UpdateRects();
	virtual void UpdateClipping();

	int DrawHTML(const wchar_t* strText, CDXUTElement* pElement, RECT* prcDest, int nCount = -1);

	bool LoadLocation(TiXmlNode* location_root);
	bool LoadSize(TiXmlNode* size_root);

	bool SaveLocation(TiXmlNode* parent);
	bool SaveSize(TiXmlNode* parent);

	void HideToolTip();

protected:
	int m_ID; // ID number
	char m_Name[MAX_PATH]; // Name
	DXUT_CONTROL_TYPE m_Type; // Control type, set once in constructor
	int m_groupID; // group this control belongs to
	std::string m_Tag; // XML tag for this control
	UINT m_nHotkey; // Virtual key code for this control's hotkey
	void* m_pUserData; // Data associated with this control that is set by user.
	bool m_bEnabled; // Enabled/disabled flag
	RECT m_rcBoundingBox; // Rectangle defining the active region of the control
	CDXUTSelectionHandles m_selectionHandles;

	struct ElementTagMapValue {
		int ElementIndex;
		unsigned int UsesFont;
		unsigned int UsesTexture;
		ElementTagMapValue(int iIndex = 0, unsigned int iUsesFont = DXUT_STATE_FLAG_NONE, unsigned int iUsesTexture = DXUT_STATE_FLAG_NONE) :
				ElementIndex(iIndex), UsesFont(iUsesFont), UsesTexture(iUsesTexture) {}
	};
	typedef std::map<std::string, ElementTagMapValue> ElementTagMap;
	typedef ElementTagMap::iterator ElementTagIterator;
	ElementTagMap m_elementTags; // XML tags for display elements

	std::string m_defaultName; // Default name used if no name given when added

	char m_comment[MAX_COMMENT]; // comment
	char m_tooltip[MAX_TOOLTIP]; // tooltip
	bool m_bToolTipShown;
	char m_xmlcss[MAX_XMLCSS]; // a special comment for Eric's script to style controls
	std::string m_xml; //used to store the xml of the object whenever a Save occurs

	POINT m_LastTouch;
	std::string m_sLinkClicked;
	RECT m_clipRect;
	bool m_clipRectSet;
	bool m_clipped; // Part or all of the control is clipped
	bool m_culled; // Control outside clip rect

	virtual ElementTagIterator FindElementTag(std::string tag);

	// for HTML rendering used by Editbox/Static/ListBox
	enum {
		tNONE,
		tB,
		tBR,
		tFONT,
		tI,
		tP,
		tSUB,
		tSUP,
		tU,
		tA,
		tT, // drf - added - tab
		tUL, // drf - added - unordered list
		tLI, // drf - added - list item
		tNUMTAGS
	};

	struct HTMLTag {
		wchar_t* mnemonic;
		short token, param, block;
	};

	static HTMLTag s_tags[];

	static const int STACKSIZE = 8;
	static int m_stacktop;
	static D3DCOLOR m_stack[STACKSIZE];
};

//-----------------------------------------------------------------------------
// Simple Comment control
//-----------------------------------------------------------------------------

class CDXUTComment : public IMenuControl {
public:
	CDXUTComment(IMenuDialog* pDialog = NULL);
	virtual ~CDXUTComment() {}

	virtual bool Load(TiXmlNode* root);
	virtual bool LoadElements(TiXmlNode* elements_root) {
		return false;
	}
	virtual bool Save(TiXmlNode* parent);
	virtual bool SaveElements(TiXmlNode* parent) {
		return false;
	}

	virtual HRESULT OnInit() {
		return S_OK;
	}
	virtual void Refresh() {}
	virtual void Render(float fElapsedTime) {}

	// Windows message handler
	virtual bool MsgProc(UINT uMsg, WPARAM wParam, LPARAM lParam) {
		return false;
	}

	virtual bool HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam) {
		return false;
	}
	virtual bool HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam) {
		return false;
	}

	virtual bool CanHaveFocus() {
		return false;
	}

	virtual void OnFocusIn() {}
	virtual void OnFocusOut() {}

	virtual void OnSelectionOn() {}
	virtual void OnSelectionOff() {}

	virtual void OnMouseEnter(POINT pt) {}
	virtual void OnMouseLeave() {}

	virtual void OnHotkey() {}

	virtual void OnMoved() {}

	virtual void OnResized() {}

	virtual BOOL ContainsPoint(POINT pt) {
		return false;
	}

	virtual IMenuDialog* GetDialog() {
		return m_pDialog;
	}

	virtual bool GetEnabled() {
		return false;
	}

	virtual bool GetVisible() {
		return false;
	}

	virtual bool GetMouseOver() {
		return false;
	}

	virtual bool GetIsInFocus() {
		return false;
	}

	virtual UINT GetType() const {
		return DXUT_CONTROL_COMMENT;
	}

	virtual std::string GetTag() const {
		return m_Tag;
	}

	virtual int GetID() const {
		return 0;
	}

	virtual const char* GetName();

	virtual bool GetCursorLocation(POINT& /*pt*/) const {
		return false;
	}
	virtual int GetCursorLocationX() const {
		return 0;
	}
	virtual int GetCursorLocationY() const {
		return 0;
	}

	virtual bool GetLocation(POINT& /*pt*/, bool useOffset = true) const {
		return false;
	}
	virtual int GetLocationX(bool useOffset = true) const {
		return 0;
	}
	virtual int GetLocationY(bool useOffset = true) const {
		return 0;
	}

	virtual int GetWidth() const {
		return 0;
	}
	virtual int GetHeight() const {
		return 0;
	}
	virtual UINT GetHotkey() const {
		return 0;
	}
	virtual void* GetUserData() const {
		return NULL;
	}
	virtual IMenuElement* GetElement(UINT iElement) {
		return NULL;
	}
	virtual IMenuElement* GetElement(std::string elementTag) {
		return NULL;
	}
	virtual bool GetIsDefault() const {
		return false;
	}
	virtual int GetNumElementTags() const {
		return 0;
	}
	virtual const char* GetElementTag(int index) {
		return "";
	}
	virtual const char* GetComment() {
		return m_comment.c_str();
	}
	virtual const char* GetToolTip() {
		return "";
	}
	virtual const char* GetXMLCSS() {
		return "";
	}
	virtual int GetGroupId() const {
		return 0;
	}
	virtual const char* GetLinkClicked() const {
		return "";
	}
	unsigned int GetElementTagUsesFont(std::string elementTag) {
		return 0;
	}
	unsigned int GetElementTagUsesTexture(std::string elementTag) {
		return 0;
	}

	virtual bool GetResizable() const {
		return true;
	}
	virtual bool GetMovable() const {
		return true;
	}
	RECT GetClipRect() const override {
		return RECT();
	}
	bool IsClipped() const override {
		return false;
	}
	bool IsCulled() const override {
		return false;
	}

	virtual bool SetEnabled(bool bEnabled) {
		return true;
	}
	virtual bool SetVisible(bool bVisible) {
		return true;
	}
	virtual bool SetID(int ID) {
		return true;
	}
	virtual bool SetName(const char* strName);

	virtual bool SetLocation(int x, int y) {
		return true;
	}
	virtual bool SetLocationX(int x) {
		return true;
	}
	virtual bool SetLocationY(int y) {
		return true;
	}

	virtual bool SetSize(int width, int height) {
		return true;
	}
	virtual bool SetHotkey(UINT nHotkey) {
		return true;
	}
	virtual bool SetUserData(void* pUserData) {
		return true;
	}
	virtual bool SetTextColor(D3DCOLOR Color) {
		return true;
	}
	virtual HRESULT SetElement(UINT iElement, CDXUTElement* pElement) {
		return S_OK;
	}
	virtual bool SetIsDefault(bool bIsDefault) {
		return true;
	}
	virtual bool SetComment(const char* strText);
	virtual bool SetToolTip(const char* strText) {
		return true;
	}
	virtual bool SetXMLCSS(const char* strText) {
		return true;
	}

	virtual bool SetResizable(bool enable) {
		return true;
	}
	virtual bool SetMovable(bool enable) {
		return true;
	}

	virtual bool SetGroupId(int id) {
		return true;
	}
	virtual bool SetClipRect(int left, int top, int right, int bottom) {
		return true;
	}

	virtual bool SetXml(const char* xml) {
		if (xml)
			m_xml = xml;
		return true;
	}
	virtual const char* GetXml() {
		return m_xml.c_str();
	}

protected:
	IMenuDialog* m_pDialog; // Parent container
	char m_Name[MAX_PATH]; // Name
	std::string m_Tag; // XML tag for this control
	std::string m_comment; // comment
	std::string m_xml; //used to store the xml of the object whenever a Save occurs
};

//-----------------------------------------------------------------------------
// Contains all the display information for a given control type
//-----------------------------------------------------------------------------
struct DXUTElementHolder {
	UINT nControlType;
	UINT iElement;

	CDXUTElement Element;
};

//-----------------------------------------------------------------------------
// Static control
//-----------------------------------------------------------------------------
#define MAX_STATIC 8192 //8K

#define ENDFLAG 0x100

#define FV_BOLD 0x01
#define FV_ITALIC (FV_BOLD << 1)
#define FV_UNDERLINE (FV_ITALIC << 1)
#define FV_SUPERSCRIPT (FV_UNDERLINE << 1)
#define FV_SUBSCRIPT (FV_SUPERSCRIPT << 1)
#define FV_NUMBER (FV_SUBSCRIPT << 1)

class CDXUTStatic : public CDXUTControl, public IMenuStatic {
public:
	CDXUTStatic(CDXUTDialog* pDialog = NULL, bool bShadow = false);

	bool Load(TiXmlNode* root);

	virtual bool Save(TiXmlNode* static_root);

	virtual bool HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam);

	virtual void Render(float fElapsedTime);

	HRESULT GetTextCopy(char* strDest, UINT bufferCount);

	const char* GetText() {
		return m_strText.c_str();
	}

	bool GetShadow() const {
		return m_bShadow;
	}

	bool GetUseHTML() const {
		return m_bUseHTML;
	}

	HRESULT SetText(const char* strText);

	HRESULT SetTextWithDefault();

	HRESULT SetShadow(bool bShadow) {
		m_bShadow = bShadow; /*Ankit ----- set needToRecalcWordWrap to true*/
		m_StaticWordWrapper.SetRecalcFlag(true);
		return S_OK;
	}

	HRESULT SetUseHTML(bool bUseHTML) {
		m_bUseHTML = bUseHTML;
		return S_OK;
	}

	int DrawText(const wchar_t* strText, CDXUTElement* pElement, RECT* prcDest, bool bShadow = false, int nCount = -1);

	int GetStringWidth(const char* strText);

	int GetStringHeight(const char* strText);

	void GetStringDims(const char* strText, int& width, int& height);

	//Ankit ----- updating function signature to contain another parameter used to return the number of whitespace before the token
	static int GetToken(const wchar_t** String, int* Size, int* TokenLength, BOOL* WhiteSpace, int* noOfWhiteSpaces);

protected:
	//Ankit ----- adding a word-wrapper object to enclose m_strText
	WordWrapper m_StaticWordWrapper;

	std::string m_strTextDefault; // default window text

	std::string m_strText; // window text current (post translation)
	std::string m_strTextOrig; // window text original (pre translation)
	std::wstring m_strTextUtf16; // window text current in Utf-16 (for render performance)

	bool m_bShadow;
	bool m_bUseHTML; // whether to render text from HTML
};

//-----------------------------------------------------------------------------
// Image control
//-----------------------------------------------------------------------------
class CDXUTImage : public CDXUTStatic, public IMenuImage {
public:
	CDXUTImage(CDXUTDialog* pDialog = NULL);

	virtual void Render(float fElapsedTime);

	HRESULT SetColor(D3DCOLOR textureColor);
};

//-----------------------------------------------------------------------------
// Button control
//-----------------------------------------------------------------------------
class CDXUTButton : public CDXUTStatic, public IMenuButton {
public:
	CDXUTButton(CDXUTDialog* pDialog = NULL);

	bool Load(TiXmlNode* root);

	virtual IMenuElement* GetElement(UINT iElement);
	virtual CDXUTControl::ElementTagIterator FindElementTag(std::string tag);

	virtual bool HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual bool HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam);
	virtual void OnHotkey() {
		m_pDialog->SendEvent(EVENT_BUTTON_CLICKED, true, this);
	}

	virtual bool CanHaveFocus() {
		return (GetVisible() && m_bEnabled);
	}

	virtual void Render(float fElapsedTime);

	virtual void SetSimpleMode(bool bSimpleMode);
	virtual bool GetSimpleMode() const {
		return m_bSimpleMode;
	}

protected:
	bool m_bPressed;
	bool m_bSimpleMode;
};

//-----------------------------------------------------------------------------
// CheckBox control
//-----------------------------------------------------------------------------
class CDXUTCheckBox : public CDXUTButton, public IMenuCheckBox {
public:
	CDXUTCheckBox(CDXUTDialog* pDialog = NULL);

	bool Load(TiXmlNode* root);
	virtual bool Save(TiXmlNode* check_box_root);

	virtual bool HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual bool HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam);
	virtual void OnHotkey() {
		SetCheckedInternal(!m_bChecked, true);
	}

	virtual BOOL ContainsPoint(POINT pt);

	virtual void UpdateRects();

	virtual void Render(float fElapsedTime);

	bool GetChecked() const {
		return m_bChecked;
	}
	void SetChecked(bool bChecked) {
		SetCheckedInternal(bChecked, false);
	}

	LONG GetIconWidth() const {
		return m_iIconWidth;
	}
	void SetIconWidth(LONG iIconWidth) {
		m_iIconWidth = iIconWidth;
		UpdateRects();
	}

protected:
	virtual void SetCheckedInternal(bool bChecked, bool bFromInput);

	bool m_bChecked;
	LONG m_iIconWidth; // <0 means autosize it to the bounding box height
	RECT m_rcButton;
	RECT m_rcText;
};

//-----------------------------------------------------------------------------
// RadioButton control
//-----------------------------------------------------------------------------
class CDXUTRadioButton : public CDXUTCheckBox, public IMenuRadioButton {
public:
	CDXUTRadioButton(CDXUTDialog* pDialog = NULL);

	bool Load(TiXmlNode* radio_button_root);
	virtual bool Save(TiXmlNode* radio_button_root);

	virtual bool HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual bool HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam);
	virtual void OnHotkey() {
		SetCheckedInternal(true, true, true);
	}

	UINT GetButtonGroup() {
		return m_nButtonGroup;
	}

	void SetChecked(bool bChecked, bool bClearGroup = true) {
		SetCheckedInternal(bChecked, bClearGroup, false);
	}
	void SetButtonGroup(UINT nButtonGroup) {
		m_nButtonGroup = nButtonGroup;
	}

protected:
	virtual void SetCheckedInternal(bool bChecked, bool bClearGroup, bool bFromInput);
	UINT m_nButtonGroup;
};

//-----------------------------------------------------------------------------
// Scrollbar control
//-----------------------------------------------------------------------------
class CDXUTScrollBar : public CDXUTControl, public IMenuScrollBar {
public:
	CDXUTScrollBar(CDXUTDialog* pDialog = NULL, CDXUTControl* pParent = NULL);
	virtual ~CDXUTScrollBar();

	bool Load(TiXmlNode* root);
	virtual bool Save(TiXmlNode* scroll_bar_root);

	virtual bool HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual bool HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam);

	virtual void Render(float fElapsedTime);
	virtual void UpdateRects();

	int GetTrackStart() const {
		return m_nStart;
	}
	int GetTrackEnd() const {
		return m_nEnd;
	}
	int GetTrackPos() const {
		return m_nPosition;
	}
	int GetPageSize() const {
		return m_nPageSize;
	}
	bool GetShowThumb() const {
		return m_bShowThumb;
	}
	IMenuControl* GetParent() {
		return m_pParent;
	}

	void SetTrackRange(int nStart, int nEnd);
	void SetTrackPos(int nPosition) {
		m_nPosition = nPosition;
		Cap();
		UpdateThumbRect();
	}
	void SetPageSize(int nPageSize) {
		m_nPageSize = abs(nPageSize);
		Cap();
		UpdateThumbRect();
	}
	void SetParent(IMenuControl* pParent) {
		m_pParent = dynamic_cast<CDXUTControl*>(pParent);
	}

	void Scroll(int nDelta); // Scroll by nDelta items (plus or minus)
	void ShowItem(int nIndex); // Ensure that item nIndex is displayed, scroll if necessary

	void Update(int start, int end, int show) {
		m_nStart = start;
		m_nEnd = end;
		ShowItem(show);
	}

protected:
	// ARROWSTATE indicates the state of the arrow buttons.
	// CLEAR            No arrow is down.
	// CLICKED_UP       Up arrow is clicked.
	// CLICKED_DOWN     Down arrow is clicked.
	// HELD_UP          Up arrow is held down for sustained period.
	// HELD_DOWN        Down arrow is held down for sustained period.
	enum ARROWSTATE {
		CLEAR,
		CLICKED_UP,
		CLICKED_DOWN,
		HELD_UP,
		HELD_DOWN
	};

	void UpdateThumbRect();
	void Cap(); // Clips position at boundaries. Ensures it stays within legal range.

	bool m_bShowThumb;
	RECT m_rcUpButton;
	RECT m_rcDownButton;
	RECT m_rcTrack;
	RECT m_rcThumb;
	int m_nPosition; // Position of the first displayed item
	int m_nPageSize; // How many items are displayable in one page
	int m_nStart; // First item
	int m_nEnd; // The index after the last item
	POINT m_LastMouse; // Last mouse position
	ARROWSTATE m_Arrow; // State of the arrows
	double m_dArrowTS; // Timestamp of last arrow event.

	CDXUTControl* m_pParent;
};

//-----------------------------------------------------------------------------
// ListBox control
//-----------------------------------------------------------------------------
struct DXUTListBoxItem {
	std::string strText;
	void* pData;

	RECT rcActive;
	bool bSelected;

	//Ankit ----- word-wrapper object to correspond to each DXUTListBoxItem
	WordWrapper m_ListBoxItemWordWrapper;
};

class CDXUTListBox : public CDXUTControl, public IMenuListBox {
public:
	CDXUTListBox(CDXUTDialog* pDialog = NULL);
	virtual ~CDXUTListBox();

	bool Load(TiXmlNode* root);
	virtual bool Save(TiXmlNode* list_box_root);

	virtual HRESULT OnInit() {
		return m_pDialog->InitControl(&m_ScrollBar);
	}
	virtual bool CanHaveFocus() {
		return (GetVisible() && m_bEnabled);
	}
	virtual bool HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual bool HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam);

	virtual void Render(float fElapsedTime);
	virtual void UpdateRects();

	IMenuScrollBar* GetScrollBar() {
		return &m_ScrollBar;
	}
	int GetScrollBarWidth() const {
		return m_nSBWidth;
	}

	DWORD GetStyle() const {
		return m_dwStyle;
	}
	bool GetMultiSelection() const {
		return GetStyle() == MULTISELECTION;
	}
	int GetSize() const {
		return static_cast<int>(m_Items.size());
	}
	int GetBorder() const {
		return m_nBorder;
	}
	int GetLeftMargin() const {
		return m_nLeftMargin;
	}
	int GetRightMargin() const {
		return m_nRightMargin;
	}
	int GetRowHeight() const {
		return m_nRowHeight;
	}
	bool GetDragSelection() const {
		return m_bDragSelection;
	}
	DXUTListBoxItem* GetItem(int nIndex);
	const char* GetItemText(int nIndex);
	void* GetItemData(int nIndex);
	int GetSelectedIndex(int nPreviousSelected = -1);
	DXUTListBoxItem* GetSelectedItem(int nPreviousSelected = -1) {
		return GetItem(GetSelectedIndex(nPreviousSelected));
	}
	const char* GetSelectedItemText();
	void* GetSelectedItemData();
	void SetStyle(DWORD dwStyle) {
		m_dwStyle = dwStyle;
	}
	void SetHTMLEnabled(bool htmlsupport) {
		m_bHTMLSupport = htmlsupport;
	}
	void SetMultiSelection(bool bMultiSelection) {
		SetStyle(bMultiSelection ? MULTISELECTION : 0);
	}
	void SetScrollBarWidth(int nWidth) {
		m_nSBWidth = nWidth;
		UpdateRects();
	}
	void SetBorder(int nBorder) {
		m_nBorder = nBorder;
		UpdateRects();
	}
	void SetMargins(int nMargin) {
		m_nLeftMargin = m_nRightMargin = nMargin;
		UpdateRects();
	}
	void SetLeftMargin(int nMargin) {
		m_nLeftMargin = nMargin;
		UpdateRects();
	}
	void SetRightMargin(int nMargin) {
		m_nRightMargin = nMargin;
		UpdateRects();
	}
	void SetRowHeight(int nHeight) {
		m_nRowHeight = nHeight;
		UpdateRects();
	}
	void SetItemText(int nIndex, const char* text);
	void SetDragSelection(bool bDragSelection) {
		m_bDragSelection = bDragSelection;
	}
	bool SetEnabled(bool bEnabled) {
		m_bEnabled = bEnabled;
		m_ScrollBar.SetVisible(bEnabled);
		m_bDrag = false;
		return true;
	}
	bool SetVisible(bool bVisible) {
		CDXUTControl::SetVisible(bVisible);
		m_bDrag = false;
		return true;
	}
	HRESULT AddItem(const char* szText, void* pData);
	HRESULT InsertItem(int nIndex, const char* szText, void* pData);
	void RemoveItem(int nIndex);
	void RemoveItemByText(char* szText);
	void RemoveItemByData(void* pData);
	void RemoveAllItems();
	int FindItem(void* pData);
	void ClearSelection();

	void SelectItem(int nNewIndex);

	enum STYLE {
		MULTISELECTION = 1
	};

protected:
	bool LoadItems(TiXmlNode* items_root);
	bool LoadItem(TiXmlNode* item_root);
	bool LoadMargins(TiXmlNode* margins_root);

	bool SaveItems(TiXmlNode* list_box_root);
	bool SaveItem(TiXmlNode* items_root, DXUTListBoxItem* pItem);
	bool SaveMargins(TiXmlNode* list_box_root);

protected:
	RECT m_rcText; // Text rendering bound
	RECT m_rcSelection; // Selection box bound
	CDXUTScrollBar m_ScrollBar;
	int m_nSBWidth;
	int m_nBorder;
	int m_nLeftMargin;
	int m_nRightMargin;
	int m_nTextHeight; // Height of a single line of text
	int m_nRowHeight; // Height of row  -- used to display text instead of text height
	DWORD m_dwStyle; // List box style
	int m_nSelected; // Index of the selected item for single selection list box
	int m_nSelStart; // Index of the item where selection starts (for handling multi-selection)
	bool m_bDrag; // Whether the user is dragging the mouse to select
	bool m_bDragSelection; // Whether the user can use the mouse to drag the
	bool m_bHTMLSupport; // Added by Jonny whether or not to render this listbox with the HTML

	std::vector<DXUTListBoxItem*> m_Items;
};

//-----------------------------------------------------------------------------
// ComboBox control
//-----------------------------------------------------------------------------
struct DXUTComboBoxItem {
	char strText[256];
	void* pData;

	RECT rcActive;
	bool bVisible;

	//Ankit ----- word-wrapper object to correspond to each DXUTListBoxItem
	WordWrapper m_ComboBoxItemWordWrapper;
};

class CDXUTComboBox : public CDXUTButton, public IMenuComboBox {
public:
	CDXUTComboBox(CDXUTDialog* pDialog = NULL);
	virtual ~CDXUTComboBox();

	bool Load(TiXmlNode* root);
	virtual bool Save(TiXmlNode* combo_box_root);

	virtual bool SetTextColor(D3DCOLOR Color);
	virtual HRESULT OnInit() {
		return m_pDialog->InitControl(&m_ScrollBar);
	}

	virtual bool HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual bool HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam);
	virtual void OnHotkey();

	virtual bool CanHaveFocus() {
		return (GetVisible() && m_bEnabled);
	}
	virtual void OnFocusOut();
	virtual void Render(float fElapsedTime);

	virtual void UpdateRects();

	HRESULT AddItem(const char* strText, void* pData);
	void RemoveAllItems();
	void RemoveItem(UINT index);
	bool ContainsItem(const char* strText, UINT iStart = 0);
	int FindItem(const char* strText, UINT iStart = 0);

	const char* GetItemText(UINT nIndex);
	void* GetItemData(const char* strText);
	void* GetItemData(UINT index);
	int GetDropHeight() const {
		return m_nDropHeight;
	}
	int GetScrollBarWidth() const {
		return m_nSBWidth;
	}
	CDXUTScrollBar* GetScrollBar() {
		return &m_ScrollBar;
	}
	void* GetSelectedData();
	const char* GetSelectedText();
	DXUTComboBoxItem* GetSelectedItem();
	int GetSelectedIndex() {
		return m_iSelected;
	}
	UINT GetNumItems() {
		return m_Items.GetSize();
	}
	DXUTComboBoxItem* GetItem(UINT index) {
		return m_Items.GetAt(index);
	}

	void SetDropHeight(UINT nHeight) {
		m_nDropHeight = nHeight;
		UpdateRects();
	}
	void SetScrollBarWidth(int nWidth) {
		m_nSBWidth = nWidth;
		UpdateRects();
	}
	HRESULT SetSelectedByIndex(UINT index);
	HRESULT SetSelectedByText(const char* strText);
	HRESULT SetSelectedByData(void* pData);

protected:
	bool LoadItems(TiXmlNode* items_root);
	bool LoadItem(TiXmlNode* item_root);

	bool SaveItems(TiXmlNode* combo_box_root);
	bool SaveItem(TiXmlNode* items_root, DXUTComboBoxItem* pItem);

protected:
	int m_iSelected;
	int m_iFocused;
	int m_nDropHeight;
	CDXUTScrollBar m_ScrollBar;
	int m_nSBWidth;

	bool m_bOpened;

	RECT m_rcText;
	RECT m_rcButton;
	RECT m_rcDropdown;
	RECT m_rcDropdownText;

	CGrowableArray<DXUTComboBoxItem*> m_Items;
};

//-----------------------------------------------------------------------------
// Slider control
//-----------------------------------------------------------------------------
class CDXUTSlider : public CDXUTControl, public IMenuSlider {
public:
	CDXUTSlider(CDXUTDialog* pDialog = NULL);

	bool Load(TiXmlNode* root);
	virtual bool Save(TiXmlNode* slider_root);

	virtual BOOL ContainsPoint(POINT pt);

	virtual bool CanHaveFocus() {
		return (GetVisible() && m_bEnabled);
	}
	virtual bool HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual bool HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam);

	virtual void UpdateRects();

	virtual void Render(float fElapsedTime);

	int GetRangeMin() const {
		return m_nMin;
	}
	int GetRangeMax() const {
		return m_nMax;
	}
	int GetValue() {
		return m_nValue;
	};

	void SetValue(int nValue) {
		SetValueInternal(nValue, false);
	}
	void SetRange(int nMin, int nMax);

protected:
	void SetValueInternal(int nValue, bool bFromInput);
	int ValueFromPos(int x);

	int m_nValue;

	int m_nMin;
	int m_nMax;

	int m_nDragX; // Mouse position at start of drag
	int m_nDragOffset; // Drag offset from the center of the button
	int m_nButtonX;

	bool m_bPressed;
	RECT m_rcButton;
};

class CUniBuffer {
	friend class CExternalApiInitializer;

	// static member

	// Empty implementation of the Uniscribe API
	static HRESULT WINAPI Dummy_ScriptApplyDigitSubstitution(const SCRIPT_DIGITSUBSTITUTE*, SCRIPT_CONTROL*, SCRIPT_STATE*) {
		return E_NOTIMPL;
	}
	static HRESULT WINAPI Dummy_ScriptStringAnalyse(HDC, const void*, int, int, int, DWORD, int, SCRIPT_CONTROL*, SCRIPT_STATE*, const int*, SCRIPT_TABDEF*, const BYTE*, SCRIPT_STRING_ANALYSIS*) {
		return E_NOTIMPL;
	}
	static HRESULT WINAPI Dummy_ScriptStringCPtoX(SCRIPT_STRING_ANALYSIS, int, BOOL, int*) {
		return E_NOTIMPL;
	}
	static HRESULT WINAPI Dummy_ScriptStringXtoCP(SCRIPT_STRING_ANALYSIS, int, int*, int*) {
		return E_NOTIMPL;
	}
	static HRESULT WINAPI Dummy_ScriptStringFree(SCRIPT_STRING_ANALYSIS*) {
		return E_NOTIMPL;
	}
	static const SCRIPT_LOGATTR* WINAPI Dummy_ScriptString_pLogAttr(SCRIPT_STRING_ANALYSIS) {
		return NULL;
	}
	static const int* WINAPI Dummy_ScriptString_pcOutChars(SCRIPT_STRING_ANALYSIS) {
		return NULL;
	}

	// Function pointers
	static HRESULT(WINAPI* _ScriptApplyDigitSubstitution)(const SCRIPT_DIGITSUBSTITUTE*, SCRIPT_CONTROL*, SCRIPT_STATE*);
	static HRESULT(WINAPI* _ScriptStringAnalyse)(HDC, const void*, int, int, int, DWORD, int, SCRIPT_CONTROL*, SCRIPT_STATE*, const int*, SCRIPT_TABDEF*, const BYTE*, SCRIPT_STRING_ANALYSIS*);
	static HRESULT(WINAPI* _ScriptStringCPtoX)(SCRIPT_STRING_ANALYSIS, int, BOOL, int*);
	static HRESULT(WINAPI* _ScriptStringXtoCP)(SCRIPT_STRING_ANALYSIS, int, int*, int*);
	static HRESULT(WINAPI* _ScriptStringFree)(SCRIPT_STRING_ANALYSIS*);
	static const SCRIPT_LOGATTR*(WINAPI* _ScriptString_pLogAttr)(SCRIPT_STRING_ANALYSIS);
	static const int*(WINAPI* _ScriptString_pcOutChars)(SCRIPT_STRING_ANALYSIS);

	static void InitializeUniscribe();
	static void UninitializeUniscribe();

	static HINSTANCE s_hDll; // Uniscribe DLL handle

private:
	std::wstring m_strBuffer;

	//   NULL terminator, in characters
	// Uniscribe-specific
	int m_iFont; // Font index of this buffer

	bool m_bAnalyseRequired; // True if the string has changed since last analysis.
	SCRIPT_STRING_ANALYSIS m_Analysis; // Analysis for the current string

	// Uniscribe -- Analyse() analyses the string in the buffer
	HRESULT Analyse();

public:
	CUniBuffer(const wchar_t* text = L"");

	~CUniBuffer();

	size_t GetTextSize() const {
		return m_strBuffer.size();
	}

	const wchar_t* GetString() const {
		return m_strBuffer.c_str();
	}

	const wchar_t& operator[](size_t n) const {
		return m_strBuffer[n];
	}
	wchar_t& operator[](size_t n);

	int GetFontIndex() const {
		return m_iFont;
	}

	bool SetText(const wchar_t* str);

	void SetFontIndex(int iFont) {
		m_iFont = iFont;
	}

	void Clear();

	bool InsertChar(size_t nIndex, wchar_t wChar);

	bool RemoveChar(size_t nIndex, size_t count);

	// Inserts the first nCount characters of the string pStr at specified index.
	// If nCount == -1, the entire string is inserted.
	bool InsertString(size_t nIndex, const wchar_t* str, int nCount = -1);

	// Uniscribe
	HRESULT CPtoX(int nCP, BOOL bTrail, int* pX);
	HRESULT XtoCP(int nX, int* pCP, int* pnTrail);
	void GetPriorItemPos(int nCP, int* pPrior);
	void GetNextItemPos(int nCP, int* pPrior);
};

//-----------------------------------------------------------------------------
// EditBox control
//-----------------------------------------------------------------------------
#undef DRF_POS_ABS // drf - todo - use only absolute positions

#define LINE_BREAK_CHAR '\n'
#define LINE_BREAK_STR L"\n"
#define EXT_LINE_BREAK_STR L"\r\n"

#define UNISCRIBE_DLLNAME _T("\\usp10.dll")

#define GETPROCADDRESS(Module, APIName, Temp) \
	Temp = GetProcAddress(Module, #APIName);  \
	if (Temp)                                 \
	*(FARPROC*)&_##APIName = Temp

#define PLACEHOLDERPROC(APIName) \
	_##APIName = Dummy_##APIName

class CDXUTEditBox : public CDXUTControl, public IMenuEditBox {
	friend class CExternalApiInitializer;

	typedef std::vector<std::wstring> TEXT_ROWS;

	static const int MAX_PASTE_SIZE = 1000;

public:
	CDXUTEditBox(CDXUTDialog* pDialog = NULL, bool typeSimple = false);

	virtual ~CDXUTEditBox();

	bool Load(TiXmlNode* root);
	bool Save(TiXmlNode* edit_box_root);

	HRESULT OnInit() {
		return m_pDialog->InitControl(&m_ScrollBar);
	}

	bool MsgProc(UINT uMsg, WPARAM wParam, LPARAM lParam);

	bool HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam);
	bool HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam);

	void UpdateRects();

	bool CanHaveFocus() {
		return (GetVisible() && m_bEnabled);
	}

	void Render(float fElapsedTime);

	void OnFocusIn();
	void OnFocusOut();

	IMenuScrollBar* GetScrollBar() {
		return &m_ScrollBar;
	}
	int GetScrollBarWidth() const {
		return m_nSBWidth;
	}

	const char* GetText();

	const char* GetTextOrig();

	size_t GetTextLength() const {
		return m_Buffer.GetTextSize();
	}

	HRESULT GetTextCopy(char* strDest, UINT bufferCount);
	HRESULT GetTextOrigCopy(char* strDest, UINT bufferCount);
	D3DCOLOR GetTextColor() const {
		return m_TextColor;
	}
	D3DCOLOR GetSelectedTextColor() const {
		return m_SelTextColor;
	}
	D3DCOLOR GetSelectedBackColor() const {
		return m_SelBkColor;
	}
	D3DCOLOR GetCaretColor() const {
		return m_CaretColor;
	}
	int GetBorderWidth() const {
		return m_nBorder;
	}
	int GetSpacing() const {
		return m_nSpacing;
	}
	bool GetPassword() const {
		return m_bPassword;
	}

	size_t GetNumTextRows();

	/** DRF
	* Sets m_Buffer with line at current caret text row, used for caret placement and rendering.
	*/
	bool SetBuffer(const size_t& row);

	void ScrollBarUpdate(const size_t& row) {
		m_ScrollBar.Update(0, GetNumTextRows(), row);
	}
	void ScrollBarUpdate() {
		return ScrollBarUpdate(GetCaretTextRow());
	}

	/** DRF
	* Gets the text row index of the first line currently displayed.
	*/
	int GetScrollBarTextRow() const {
		return m_ScrollBar.GetTrackPos();
	}

	/** DRF
	* Gets the X position the caret is currently on directly.
	*/
	size_t GetCaretPosX() const {
#ifdef DRF_POS_ABS
		size_t col = 0;
		size_t row = 0;
		PosAbs2ColRow(m_caretPosAbs, col, row);
		return col;
#else
		return m_caretPosX;
#endif
	}

	/** DRF
	* Sets the X position the caret is currently on directly.
	*/
	size_t SetCaretPosX(const size_t& posX);

	/** DRF
	* Gets the Y position the caret is currently on directly.
	*/
	size_t GetCaretPosY() const {
#ifdef DRF_POS_ABS
		size_t col = 0;
		size_t row = 0;
		PosAbs2ColRow(m_caretPosAbs, col, row);
		return (int)row - (int)GetScrollBarTextRow();
#else
		return m_caretPosY;
#endif
	}

	/** DRF
	* Sets the Y position the caret is currently on directly.
	*/
	size_t SetCaretPosY(const size_t& posY);

	/** DRF
	* Sets the caret position via the given text row index.
	* The textRow = scrollBar.GetTrackPos() + caretPos
	*/
	size_t SetCaretTextRow(const size_t& row);

	/** DRF
	* Gets the text row index of the display row the caret is currently on.
	* The textRow = scrollBar.GetTrackPos() + caretPos
	*/
	size_t GetCaretTextRow();

	/** DRF
	* Gets the X position the selection is currently on directly.
	*/
	size_t GetSelectPosX() const {
#ifdef DRF_POS_ABS
		size_t col = 0;
		size_t row = 0;
		PosAbs2ColRow(m_selectPosAbs, col, row);
		return col;
#else
		return m_selectPosX;
#endif
	}

	/** DRF
	* Gets the Y position the selection is currently on directly.
	*/
	size_t GetSelectPosY() const {
#ifdef DRF_POS_ABS
		size_t col = 0;
		size_t row = 0;
		PosAbs2ColRow(m_selectPosAbs, col, row);
		return (int)row - (int)GetScrollBarTextRow();
#else
		return m_selectPosY;
#endif
	}

	/** DRF
	* Gets the text row index of the display row the selection started on.
	*/
	size_t GetSelectStartTextRow();

	/** DRF
	* Gets text rows absolute position (character count) given (col, row).
	*/
	bool ColRow2PosAbs(size_t& posAbs, const size_t& col, const size_t& row);

	/** DRF
	* Gets (col, row) given text rows absolute position (character count).
	*/
	bool PosAbs2ColRow(const size_t& posAbs, size_t& col, size_t& row);

	/** DRF
	* Gets text rows absolute position (character count) max.
	*/
	size_t GetPosAbsMax();

	/** DRF
	* Gets text rows absolute position (character count) up to caret.
	*/
	size_t GetCaretPosAbs() {
#ifdef DRF_POS_ABS
		return m_caretPosAbs;
#else
		size_t posAbs = 0;
		ColRow2PosAbs(posAbs, GetCaretPosX(), GetCaretTextRow());
		return posAbs;
#endif
	}

	/** DRF
	* Gets text rows absolute position (character count) up to selection start.
	*/
	size_t GetSelectPosAbs() {
#ifdef DRF_POS_ABS
		return m_selectPosAbs;
#else
		size_t posAbs = 0;
		ColRow2PosAbs(posAbs, m_selectPosX, GetSelectStartTextRow());
		return posAbs;
#endif
	}

	/** DRF
	* Sets text rows absolute position of selection.
	*/
	bool SetSelectPosAbs(const size_t& posAbs);

	/** DRF
	* Gets text rows absolute position (character count) up to selection
	* and the number of characters selected if a selection is made, otherwise
	* it returns false.
	*/
	bool GetSelectPosAbs(size_t& posAbs, size_t& numAbs);

	/** DRF
	* Sets text rows absolute caret position.
	*/
	bool SetCaretPosAbs(const size_t& caretPos);

	/** DRF
	* Gets text rows absolute string (all rows combined).
	*/
	std::wstring TextRowsGetStrAbs(const TEXT_ROWS& textRows, const size_t& posAbs = 0, const size_t& numAbs = 0);

	/** DRF
	* Sets text rows absolute string (all rows combined).
	* Existing text rows are cleared first.
	*/
	bool TextRowsSetStrAbs(TEXT_ROWS& textRows, const std::wstring& strAbs);

	/** DRF
	* Gets the position of the prioritized line split:
	*  1. First Line Break Split - FirstLineBreakPos()
	*  2. Line Overflow Split - LineOverflowsRect()
	*    2.1 Split At Last Space - LastLineSpacePos()
	*    2.2 Split At Overflow
	*/
	size_t LineSplitPos(const std::wstring& str);

	/** DRF
	* Performs single text row line split. Rows might need split twice
	* since there are 2 priorities. For example first we will split on
	* the first line break but that doesn't mean we don't then need to
	* overflow split that.
	*/
	bool TextRowsLineSplit(TEXT_ROWS& textRows, const size_t& row);

	/** DRF
	* Refreshes formatting on all rows updating rectangles and scrollbars.
	*/
	bool TextRowsRefresh(TEXT_ROWS& textRows);

	/** DRF
	* Adds given string to current caret position and refreshes all rows.
	*/
	bool TextRowsAddAtCaret(TEXT_ROWS& textRows, const std::wstring& str);

	bool TextRowsDelAbs(TEXT_ROWS& textRows, const size_t& posAbs, const size_t& numAbs);

	// Deletes selected text and optionally sends edit box changed event.
	bool DeleteSelected(bool sendEvent = true);

	// Resets selected text.
	bool ResetSelection() {
#ifdef DRF_POS_ABS
		SetSelectPosAbs(GetCaretPosAbs());
#else
		m_selectPosX = GetCaretPosX();
		m_selectPosY = GetCaretPosY();
#endif
		return true;
	}

	// Returns true if there is valid selected text.
	bool ValidSelection() {
#ifdef DRF_POS_ABS
		return (GetSelectPosAbs() != GetCaretPosAbs());
#else
		return (m_selectPosX != GetCaretPosX()) || (m_selectPosY != GetCaretPosY());
#endif
	}

	// Sets edit box selection from start to end character indexes. (default selects all)
	bool SetSelected(int startIndex = 0, int endIndex = -1);

	bool SetText(const char* szText, bool bSelected = false);

	bool SetTextWithDefault();

	bool SetTextColor(D3DCOLOR Color) {
		m_TextColor = Color;
		return true;
	}

	bool SetSelectedTextColor(D3DCOLOR Color) {
		m_SelTextColor = Color;
		return true;
	}

	bool SetSelectedBackColor(D3DCOLOR Color) {
		m_SelBkColor = Color;
		return true;
	}

	bool SetCaretColor(D3DCOLOR Color) {
		m_CaretColor = Color;
		return true;
	}

	bool SetBorderWidth(int nBorder) {
		m_nBorder = nBorder;
		UpdateRects();
		return true;
	}

	bool SetSpacing(int nSpacing) {
		m_nSpacing = nSpacing;
		UpdateRects();
		return true;
	}

	bool SetTextFloatArray(const float* pNumbers, int nCount);

	bool SetPassword(bool bPassword) {
		m_bPassword = bPassword;
		return true;
	}

	bool SetScrollBarWidth(int nWidth) {
		m_nSBWidth = nWidth;
		UpdateRects();
		return true;
	}

	bool ParseFloatArray(float* pNumbers, int nCount);

	bool ClearText();

	size_t GetMaxCharCount() const {
		return m_nMaxCharCount == 0 ? SIZE_MAX : m_nMaxCharCount;
	}
	bool SetMaxCharCount(size_t maxCount) {
		m_nMaxCharCount = maxCount;
		return true;
	}

	// DRF - ED-29 - Edit Box Fixes
	// Deprecated XML <multiline>
	// Edit box is now automatically multi-line if it has more then 1 row
	bool IsMultiline() const {
		return (m_ScrollBar.GetPageSize() > 1);
	}

	bool GetEditable() const {
		return m_bEditable;
	}
	bool SetEditable(bool editable) {
		m_bEditable = editable;
		return true;
	}

	bool GetHideCaret() const {
		return m_bHideCaret;
	}
	bool SetHideCaret(bool bHideCaret) {
		m_bHideCaret = bHideCaret;
		return true;
	}

	// DRF - Doesn't Work
	bool SetScrollBar(bool enable) {
		return m_ScrollBar.SetVisible(enable);
	}

protected:
	void ScrollToEnd();
	void ResetCaretBlink();
	void CopySelection();

	/** DRF
	* Copys characters from given UniBuffer between startIndex and endIndex into clipboard.
	* If endIndex is 0 it copies all characters to the end of the buffer.
	*/
	bool CopyToClipboard(size_t startIndex, size_t endIndex, CUniBuffer& cub);

	/** DRF
	* Pastes characters in clipboard into editbox at current caret position.
	*/
	bool PasteFromClipboard();

	size_t GetCaretLocation(size_t nStartRow, size_t nCaret);

protected:
	bool m_typeSimple; // drf - added to replace SimpleEditBox

	bool m_bEditable; // Can the text be altered by a user
	bool m_bHideCaret; // If true, we don't render the caret.
	bool m_bMouseDrag; // True to indicate drag in progress
	bool m_bInsertMode; // If true, control is in insert mode. Else, overwrite mode.
	bool m_bCaretOn; // Flag to indicate whether caret is currently visible
	bool m_bPassword; // If true, control displays '*' but has copy of original chars

	double m_dfBlink; // Caret blink time in milliseconds
	double m_dfLastBlink; // Last timestamp of caret blink

	size_t m_nMaxCharCount; // Maximum characters allowed in the edit box, 0 if unlimited

	int m_nFirstVisible; // First visible character in the edit control

	int m_nBorder; // Border of the window
	int m_nSpacing; // Spacing between the text and the edge of border

	int m_nSBWidth; // Scroll Bar Width

	int m_nRowHeight; //The height of each row so we can draw the caret the correct size

	size_t m_nMaxRows; //The maximum number of rows text can fit into

#ifdef DRF_POS_ABS
	size_t m_selectPosAbs;
#else
	size_t m_selectPosX; // Starting position of the selection. The caret marks the end.
	size_t m_selectPosY; // Which row the selection has started on
#endif

#ifdef DRF_POS_ABS
	size_t m_caretPosAbs;
#else
	size_t m_caretPosX; // Caret X position (display column)
	size_t m_caretPosY; // caret y position (display row)
#endif

	TEXT_ROWS m_textRows; // editable multi-line rows of text
	TEXT_ROWS m_textRowsRender; // not editable multi-line rows of text

	std::wstring m_defaultText; // Default text used if no text given when added

	CUniBuffer m_Buffer; // Buffer to hold text ( could be converted to '*'s )
	std::wstring m_strOriginalText; // Buffer to hold text ( unchanged )

	RECT m_rcBoundary; // Bounding rectangle used for text, stays unchanged throughout draws
	RECT m_rcText; // Bounding rectangle for the text
	RECT m_rcRender[9]; // Convenient rectangles for rendering elements

	D3DCOLOR m_TextColor; // Text color
	D3DCOLOR m_SelTextColor; // Selected text color
	D3DCOLOR m_SelBkColor; // Selected background color
	D3DCOLOR m_CaretColor; // Caret color

	CDXUTScrollBar m_ScrollBar;

	std::wstring m_strSetText; //String to hold the row that is currently being typed into
	std::string m_sBuilderString; //String to concat from m_vRows when calling EditBox_GetText
	std::wstring m_sCurrentRenderedRow;

	//Added by tbreisch for Link Tracking
	struct LinkItem {
		std::wstring linkText;
		std::wstring fullUrl;

		size_t startText;
		size_t lenText;
	};
	std::vector<LinkItem> m_vLinks;
};

// DRF - Magic Number Enumeration
enum MAGIC_NUMBER {
	MN_NONE = 0,
	MN_CONSTRUCTING = 1,
	MN_CONSTRUCTED = 2,
	MN_DESTRUCTING = 3,
	MN_DESTRUCTED = 4,
	MAGIC_NUMBERS
};

// DRF - Index Error Enumeration
#define IDX_OK(i) (i >= 0)
#define IDX_ERROR(i) (i < 0)
enum {
	IDX_NOT_IN_ARRAY = -1, // CGrowableArray Error
	IDX_FAILED_CREATE = -2, // do retries
	IDX_FILE_NOT_FOUND = -3,
	IDX_INVALID_NAME = -4,
	IDX_OUT_OF_MEMORY = -5,
	IDX_GDRM_INVALID = -6,
	IDX_CLIENT_OUT_OF_VIEW = -7
};

//-----------------------------------------------------------------------------
// All controls must be assigned to a dialog, which handles
// input and rendering for the controls.
//-----------------------------------------------------------------------------
class CDXUTDialog : public IMenuDialog {
private:
	// DRF - Variables Ordered To See In Debugger
	std::string m_menuId; // menu identifier (menuFileName unless overridden by DialogCreateAs())
	std::string m_menuFilePath; // menu file path '...\gamefiles\menus\menu.xml'
	std::string m_menuFileName; // menu file name 'menu.xml'
	std::string m_scriptFileName; // menu script normailized file name without path 'menu.lua'

private:
	// Disable copy constructor and assign operator
	CDXUTDialog(const CDXUTDialog& rhs);
	void operator=(const CDXUTDialog& rhs);

public:
	CDXUTDialog(const std::string& menuFilePath, const std::string& menuId, bool trustedMenu = false);

	CDXUTDialog(const std::string& menuFilePath, bool trustedMenu = false) :
			CDXUTDialog(menuFilePath, menuFilePath, trustedMenu) {}

	~CDXUTDialog();

	// This is the assigned menu identifier for Logging & IsOpen comparison.
	// It is normally set to match menuFileName name unless overridden by DialogCreateAs().
	std::string GetMenuId() const {
		return m_menuId;
	}
	const char* GetMenuIdPtr() {
		return m_menuId.c_str();
	}
	void SetMenuId(const std::string& menuId) {
		m_menuId = menuId;
	}

	// This is the original menu.xml full file path
	std::string GetMenuFilePath() const {
		return m_menuFilePath;
	}
	const char* GetMenuFilePathPtr() {
		return m_menuFilePath.c_str();
	}
	bool SetMenuFilePath(const std::string& menuFilePath);

	// This is the original menu.xml file name
	std::string GetMenuFileName() const {
		return m_menuFileName;
	}
	const char* GetMenuFileNamePtr() {
		return m_menuFileName.c_str();
	}

	// This is used as the MenuHelper logging prefix
	std::string GetScriptFileName() const {
		return m_scriptFileName;
	}
	const char* GetScriptFileNamePtr() {
		return m_scriptFileName.c_str();
	}
	bool SetScriptFileName(const std::string& scriptFileName) {
		m_scriptFileName = scriptFileName;
		return true;
	}

	/** DRF
	* Return Dialog as a string for consistent logging.
	*/
	std::string ToStr() const;

	/** DRF
	* Return if this dialog is fully constructed. It is dangerous to use dialogs not fully constructed.
	*/
	MAGIC_NUMBER m_magicNumber; // enumerated magic number
	bool IsFullyConstructed() const {
		return (m_magicNumber == MN_CONSTRUCTED);
	}

	/** DRF
	* Return if this dialog is currently pending deletion. It is dangerous to use dialogs pending deletion.
	*/
	bool m_bDelete; // invalid dialog - moved to m_deletedDialogs[] pending deletion
	void SetPendingDeletion(bool pendingDelete) {
		m_bDelete = pendingDelete;
	}
	bool IsPendingDeletion() const {
		return m_bDelete;
	}

	bool m_isScriptLoaded; // script is loaded (dialogs created for edit do not load scripts)

	bool m_btopmost; // Make this dialog the top most dialog.
	bool m_bNonUserEvents;

	bool m_bKeyboardInput;
	bool m_bMouseInput;

	bool m_bCloseOnESC; // drf - added - close menu on ESC key press

	bool m_bCaptureKeys; // capture keys while in focus

	bool m_bPassthrough; // passthrough clicks while in focus

	virtual void Delete();

	/** DRF
	* Returns true if associated menu script is successfully loaded otherwise false.
	* Dialogs created for editing (Dialog_CreateAndEdit(true)) do not load scripts.
	*/
	bool IsScriptLoaded() {
		return m_isScriptLoaded;
	}

	bool IsTrustedMenu() const {
		return m_trustedMenu;
	}

	bool LoadMenuXmlResources(TiXmlNode* root);
	bool LoadElements(TiXmlNode* elements_root);
	bool Save(TiXmlDocument* xml_doc);

	// Windows message handler
	bool MsgProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	/** DRF
	* Gets menu relative cursor point given client window relative cursor point.
	*/
	POINT GetCursorPointMenu(const POINT& cursorPoint);

	// DRF - Returns if given point is inside menu area.
	bool PointIsInside(const POINT& pt);
	bool PointIsInsideCaption(const POINT& pt);
	bool PointIsInsideNotCaption(const POINT& pt);

	// Control creation
	HRESULT AddStatic(int ID, const char* strText, int x, int y, int width, int height, bool bShadow = false, bool bIsDefault = false, CDXUTStatic** ppCreated = NULL);
	HRESULT AddImage(int ID, int x, int y, int width, int height, UINT iTexture, RECT* prcTexture, D3DCOLOR defaultTextureColor = D3DCOLOR_ARGB(255, 255, 255, 255), CDXUTImage** ppCreated = NULL);
	HRESULT AddButton(int ID, const char* strText, int x, int y, int width, int height, UINT nHotkey = 0, bool bIsDefault = false, CDXUTButton** ppCreated = NULL);
	HRESULT AddCheckBox(int ID, const char* strText, int x, int y, int width, int height, bool bChecked = false, UINT nHotkey = 0, bool bIsDefault = false, CDXUTCheckBox** ppCreated = NULL);
	HRESULT AddRadioButton(int ID, UINT nButtonGroup, const char* strText, int x, int y, int width, int height, bool bChecked = false, UINT nHotkey = 0, bool bIsDefault = false, CDXUTRadioButton** ppCreated = NULL);
	HRESULT AddComboBox(int ID, int x, int y, int width, int height, UINT nHotKey = 0, bool bIsDefault = false, CDXUTComboBox** ppCreated = NULL);
	HRESULT AddSlider(int ID, int x, int y, int width, int height, int min = 0, int max = 100, int value = 50, bool bIsDefault = false, CDXUTSlider** ppCreated = NULL);
	HRESULT AddEditBox(int ID, const char* strText, int x, int y, int width, int height, bool typeSimple = false, bool bIsDefault = false, CDXUTEditBox** ppCreated = NULL);
	HRESULT AddListBox(int ID, int x, int y, int width, int height, DWORD dwStyle = 0, CDXUTListBox** ppCreated = NULL);
	HRESULT AddFlash(int ID, int x, int y, int width, int height, bool wantInput, bool sizeToFit, bool loop, const char* params, CDXUTFlashPlayer** ppCreated = NULL);

	// Control creation by name
	HRESULT AddStatic(const char* strName, const char* strText, int x, int y, int width, int height, bool bShadow = false, bool bIsDefault = false, IMenuControl** ppCreated = NULL);
	HRESULT AddImage(const char* strName, int x, int y, int width, int height, const std::string& textureName, RECT coords, D3DCOLOR defaultTextureColor = D3DCOLOR_ARGB(255, 255, 255, 255), IMenuControl** ppCreated = NULL);
	HRESULT AddImageToBack(const char* strName, int x, int y, int width, int height, const std::string& textureName, RECT coords, D3DCOLOR defaultTextureColor = D3DCOLOR_ARGB(255, 255, 255, 255), IMenuControl** ppCreated = NULL);
	HRESULT AddButton(const char* strName, const char* strText, int x, int y, int width, int height, UINT nHotkey = 0, bool bIsDefault = false, IMenuControl** ppCreated = NULL);
	HRESULT AddCheckBox(const char* strName, const char* strText, int x, int y, int width, int height, bool bChecked = false, UINT nHotkey = 0, bool bIsDefault = false, IMenuControl** ppCreated = NULL);
	HRESULT AddRadioButton(const char* strName, UINT nButtonGroup, const char* strText, int x, int y, int width, int height, bool bChecked = false, UINT nHotkey = 0, bool bIsDefault = false, IMenuControl** ppCreated = NULL);
	HRESULT AddComboBox(const char* strName, int x, int y, int width, int height, UINT nHotKey = 0, bool bIsDefault = false, IMenuControl** ppCreated = NULL);
	HRESULT AddSlider(const char* strName, int x, int y, int width, int height, int min = 0, int max = 100, int value = 50, bool bIsDefault = false, IMenuControl** ppCreated = NULL);
	HRESULT AddEditBox(const char* strName, const char* strText, int x, int y, int width, int height, bool typeSimple = false, bool bIsDefault = false, IMenuControl** ppCreated = NULL);
	HRESULT AddListBox(const char* strName, int x, int y, int width, int height, DWORD dwStyle = 0, IMenuControl** ppCreated = NULL);
	HRESULT AddFlash(const char* strName, int x, int y, int width, int height, bool wantInput, bool sizeToFit, bool loop, const char* params, IMenuControl** ppCreated = NULL);

	HRESULT AddControl(IMenuControl* pControl);
	HRESULT AddControlToBack(IMenuControl* pControl);
	HRESULT InitControl(IMenuControl* pControl);

	// Control retrieval
	CDXUTStatic* GetStatic(int ID) {
		return dynamic_cast<CDXUTStatic*>(GetControl(ID, DXUT_CONTROL_STATIC));
	}
	CDXUTImage* GetImage(int ID) {
		return dynamic_cast<CDXUTImage*>(GetControl(ID, DXUT_CONTROL_IMAGE));
	}
	CDXUTButton* GetButton(int ID) {
		return dynamic_cast<CDXUTButton*>(GetControl(ID, DXUT_CONTROL_BUTTON));
	}
	CDXUTCheckBox* GetCheckBox(int ID) {
		return dynamic_cast<CDXUTCheckBox*>(GetControl(ID, DXUT_CONTROL_CHECKBOX));
	}
	CDXUTRadioButton* GetRadioButton(int ID) {
		return dynamic_cast<CDXUTRadioButton*>(GetControl(ID, DXUT_CONTROL_RADIOBUTTON));
	}
	CDXUTComboBox* GetComboBox(int ID) {
		return dynamic_cast<CDXUTComboBox*>(GetControl(ID, DXUT_CONTROL_COMBOBOX));
	}
	CDXUTSlider* GetSlider(int ID) {
		return dynamic_cast<CDXUTSlider*>(GetControl(ID, DXUT_CONTROL_SLIDER));
	}
	CDXUTEditBox* GetEditBox(int ID) {
		return dynamic_cast<CDXUTEditBox*>(GetControl(ID, DXUT_CONTROL_EDITBOX));
	}
	CDXUTListBox* GetListBox(int ID) {
		return dynamic_cast<CDXUTListBox*>(GetControl(ID, DXUT_CONTROL_LISTBOX));
	}
	CDXUTFlashPlayer* GetFlash(int ID);

	// Control retrieval by name
	CDXUTStatic* GetStatic(const char* strName) {
		return dynamic_cast<CDXUTStatic*>(GetControl(strName));
	}
	CDXUTImage* GetImage(const char* strName) {
		return dynamic_cast<CDXUTImage*>(GetControl(strName));
	}
	CDXUTButton* GetButton(const char* strName) {
		return dynamic_cast<CDXUTButton*>(GetControl(strName));
	}
	CDXUTCheckBox* GetCheckBox(const char* strName) {
		return dynamic_cast<CDXUTCheckBox*>(GetControl(strName));
	}
	CDXUTRadioButton* GetRadioButton(const char* strName) {
		return dynamic_cast<CDXUTRadioButton*>(GetControl(strName));
	}
	CDXUTComboBox* GetComboBox(const char* strName) {
		return dynamic_cast<CDXUTComboBox*>(GetControl(strName));
	}
	CDXUTSlider* GetSlider(const char* strName) {
		return dynamic_cast<CDXUTSlider*>(GetControl(strName));
	}
	CDXUTEditBox* GetEditBox(const char* strName) {
		return dynamic_cast<CDXUTEditBox*>(GetControl(strName));
	}
	CDXUTListBox* GetListBox(const char* strName) {
		return dynamic_cast<CDXUTListBox*>(GetControl(strName));
	}
	CDXUTFlashPlayer* GetFlash(const char* strName);

	//Ankit ----- return IMenu equivalent interface objects to everything outside KepMenu
	IMenuListBox* GetIMenuListBox(const char* strName) {
		return dynamic_cast<IMenuListBox*>(GetControl(strName));
	}
	IMenuEditBox* GetIMenuEditBox(const char* strName) {
		return dynamic_cast<IMenuEditBox*>(GetControl(strName));
	}
	IMenuComboBox* GetIMenuComboBox(const char* strName) {
		return dynamic_cast<IMenuComboBox*>(GetControl(strName));
	}
	IMenuStatic* GetIMenuStatic(const char* strName) {
		return dynamic_cast<IMenuStatic*>(GetControl(strName));
	}

	IMenuControl* GetControl(const char* strName);
	IMenuControl* GetControl(int ID);
	IMenuControl* GetControl(int ID, UINT nControlType);
	IMenuControl* GetControlByIndex(int index);
	IMenuControl* GetControlAtPoint(POINT pt);
	const IMenuControl* GetControlAtPoint(POINT pt) const;
	IMenuControl* GetNextControl(IMenuControl* pControl) {
		return _GetNextControl(pControl);
	}
	IMenuControl* GetPrevControl(IMenuControl* pControl) {
		return _GetPrevControl(pControl);
	}
	const char* GetControlXml(IMenuControl* pControl);

	bool RemoveControl(int ID);
	bool RemoveControl(const char* strName);
	bool RemoveAllControls();

	bool GetControlEnabled(int ID);
	bool SetControlEnabled(int ID, bool bEnabled);

	bool ClearRadioButtonGroup(UINT nGroup);
	bool ClearComboBox(int ID);

	// DRF - Added - HUDInventory::OnMouseMove::Dialog_ClearFocus Log Errors Fix
	bool ClearStaticControlsForThisDialog();

	bool ClearFocus() {
		_ClearFocus();
		return true;
	}
	bool FocusDefaultControl();
	bool ClearSelection() {
		_ClearSelection();
		return true;
	}

	bool SetAsTopMost(bool btop) {
		m_btopmost = btop;
		return true;
	}
	bool IsTopmost() const {
		return m_btopmost;
	}

	// Access the default display Elements used when adding new controls
	HRESULT SetDefaultElement(UINT nControlType, UINT iElement, CDXUTElement* pElement);
	CDXUTElement* GetDefaultElement(UINT nControlType, UINT iElement);

	bool SendEvent(GUI_EVENT nEvent, bool bTriggeredByUser, IMenuDialog* pDialog, POINT pt);
	bool SendEvent(GUI_EVENT nEvent, bool bTriggeredByUser, IMenuDialog* pDialog);

	// Methods called by controls
	bool SendEvent(GUI_EVENT nEvent, bool bTriggeredByUser, IMenuControl* pControl, POINT pt);
	bool SendEvent(GUI_EVENT nEvent, bool bTriggeredByUser, IMenuControl* pControl);

	bool RequestFocus(IMenuControl* pControl);
	bool RequestSelection(IMenuControl* pControl, bool bEnable);

	// Render helpers
	HRESULT DrawRect(RECT* pRect, D3DCOLOR color);
	HRESULT DrawPolyLine(POINT* apPoints, UINT nNumPoints, D3DCOLOR color);
	HRESULT DrawSprite(CDXUTElement* pElement, RECT* prcDest);
	int DrawText(const wchar_t* strText, CDXUTElement* pElement, RECT* prcDest, bool bShadow = false, int nCount = -1);

	int GetID() const {
		return m_ID;
	}
	bool GetCaptionEnabled() const {
		return m_bCaption;
	}
	const char* GetCaptionText() {
		return &m_szCaption[0];
	}
	int GetCaptionHeight() const {
		return m_nCaptionHeight;
	}
	bool GetCaptionShadow() const {
		return m_bCaptionShadow;
	}

	bool GetIsDraggingByCaption() const {
		return m_bDragCaption;
	}
	bool SetIsDraggingByCaption(bool enable) {
		m_bDragCaption = enable;
		return true;
	}

	bool GetCursorLocation(POINT& pt) const;
	int GetCursorLocationX() const;
	int GetCursorLocationY() const;

	bool GetLocation(POINT& pt, bool useOffset = true) const {
		pt.x = (useOffset) ? m_x + m_xOffset : m_x;
		pt.y = (useOffset) ? m_y + m_yOffset : m_y;
		return true;
	}
	int GetLocationX(bool useOffset = true) const {
		return (useOffset) ? m_x + m_xOffset : m_x;
	}
	int GetLocationY(bool useOffset = true) const {
		return (useOffset) ? m_y + m_yOffset : m_y;
	}

	bool GetOffset(POINT& pt) const {
		pt.x = m_xOffset;
		pt.y = m_yOffset;
		return true;
	}
	int GetOffsetX() const {
		return m_xOffset;
	}
	int GetOffsetY() const {
		return m_yOffset;
	}

	DWORD GetLocationFormat() const {
		return m_dwLocationFormat;
	}

	int GetWidth() const {
		return m_width;
	}
	int GetHeight() const {
		return m_height;
	}

	bool GetMinimized() const {
		return m_bMinimized;
	}
	bool GetSnapToEdges() const {
		return m_bSnapToEdges;
	}
	bool GetIsSelected() const {
		return m_bIsSelected;
	}
	bool GetModal() const {
		return m_bModal;
	}

	bool GetMovable() const {
		return m_bMovable;
	}
	bool GetResizable() const {
		return m_bResizable;
	}

	bool GetEdit() const {
		return m_bEdit;
	}
	bool GetTrusted() const {
		return m_trustedMenu;
	}

	bool GetPassthrough() const {
		return m_bPassthrough;
	}
	bool SetPassthrough(bool passthrough) {
		m_bPassthrough = passthrough;
		return true;
	}

	// DRF - Added - Menus Open For Edit Cannot Be Actually 'modal'
	bool IsActuallyModal() const {
		return GetModal() && !GetEdit();
	}

	bool GetBringToFront() const {
		return m_bBringToFront;
	}
	IMenuElement* GetElement(std::string elementTag);
	IMenuElement* GetCaptionElement() {
		return m_pCapElement;
	}
	IMenuElement* GetBodyElement() {
		return m_pBodyElement;
	}
	IMenuElement* GetIconElement() {
		return m_pIconElement;
	}
	unsigned int GetElementTagUsesFont(std::string elementTag);
	unsigned int GetElementTagUsesTexture(std::string elementTag);

	bool GetCaptureKeys() const {
		return m_bCaptureKeys;
	}

	bool GetEnableMouseInput() const {
		return m_bMouseInput;
	}
	bool GetEnableKeyboardInput() const {
		return m_bKeyboardInput;
	}

	int GetNumControls() const {
		return (int)m_Controls.size();
	}
	int GetControlIndex(IMenuControl* pControl);
	IMenuDialog* GetNextDialog() {
		return m_pNextDialog;
	}
	IMenuDialog* GetPrevDialog() {
		return m_pPrevDialog;
	}
	int GetNextControlValue() const {
		return m_nextControlValue;
	}
	D3DCOLOR GetColorTopLeft() const {
		return m_colorTopLeft;
	}
	D3DCOLOR GetColorTopRight() const {
		return m_colorTopRight;
	}
	D3DCOLOR GetColorBottomLeft() const {
		return m_colorBottomLeft;
	}
	D3DCOLOR GetColorBottomRight() const {
		return m_colorBottomRight;
	}

	bool SetID(int ID) {
		m_ID = ID;
		return true;
	}
	bool SetMinimized(bool bMinimized) {
		m_bMinimized = bMinimized;
		return true;
	}
	bool SetBackgroundColors(D3DCOLOR colorAllCorners) {
		SetBackgroundColors(colorAllCorners, colorAllCorners, colorAllCorners, colorAllCorners);
		return true;
	}
	bool SetBackgroundColors(D3DCOLOR colorTopLeft, D3DCOLOR colorTopRight, D3DCOLOR colorBottomLeft, D3DCOLOR colorBottomRight);
	bool EnableCaption(bool bEnable) {
		m_bCaption = bEnable;
		return true;
	}
	bool SetCaptionHeight(int nHeight) {
		m_nCaptionHeight = nHeight;
		return true;
	}
	bool SetCaptionText(const char* pszText) {
		if (!pszText)
			return false;
		strncpy_s(m_szCaption, _countof(m_szCaption), pszText, _TRUNCATE);
		return true;
	}
	bool SetCaptionShadow(const bool bShadow) {
		m_bCaptionShadow = bShadow;
		return true;
	}

	bool SetLocation(int x, int y) {
		m_x = x;
		m_y = y;
		return true;
	}
	bool SetLocationX(int x) {
		m_x = x;
		return true;
	}
	bool SetLocationY(int y) {
		m_y = y;
		return true;
	}

	bool SetOffset(int _xOffset, int _yOffset) {
		m_xOffset = _xOffset;
		m_yOffset = _yOffset;
		return true;
	}
	bool SetOffsetX(int _xOffset) {
		m_xOffset = _xOffset;
		return true;
	}
	bool SetOffsetY(int _yOffset) {
		m_yOffset = _yOffset;
		return true;
	}

	bool SetLocationInScreen(int x, int y); //this guarantees that the dialog will show inside the screen

	bool SetWidth(int width) {
		m_width = width;
		return true;
	}
	bool SetHeight(int height) {
		m_height = height;
		return true;
	}
	bool SetSize(int width, int height) {
		m_width = width;
		m_height = height;
		return true;
	}

	bool SetLocationFormat(DWORD dwFormat) {
		m_dwLocationFormat = dwFormat;
		return true;
	}
	bool SetSnapToEdges(bool bSnap) {
		m_bSnapToEdges = bSnap;
		return true;
	}
	bool SetIsSelected(bool bIsSelected) {
		m_bIsSelected = bIsSelected;
		SendEvent(EVENT_DIALOG_SELECTED, true, this);
		return true;
	}

	bool SetMovable(bool enable) {
		m_bMovable = enable;
		return true;
	}
	bool SetResizable(bool enable) {
		m_bResizable = enable;
		return true;
	}

	bool BringToFrontEnable(bool enable) {
		m_bBringToFrontEnable = enable;
		return true;
	}
	bool BringToFrontEnabled() const {
		return m_bBringToFrontEnable;
	}

	bool SetBringToFront(bool bFront) {
		m_bBringToFront = bFront;
		return true;
	}

	bool SetNextDialog(IMenuDialog* pNextDialog);
	bool SetPrevDialog(IMenuDialog* pPrevDialog) {
		m_pPrevDialog = pPrevDialog;
		return true;
	}
	bool SetRefreshTime(float fTime) {
		_SetRefreshTime(fTime);
		return true;
	}

	bool SetNextControlValue(int value) {
		m_nextControlValue = value;
		return true;
	}
	bool SetColorTopLeft(D3DCOLOR color) {
		m_colorTopLeft = color;
		return true;
	}
	bool SetColorTopRight(D3DCOLOR color) {
		m_colorTopRight = color;
		return true;
	}
	bool SetColorBottomLeft(D3DCOLOR color) {
		m_colorBottomLeft = color;
		return true;
	}
	bool SetColorBottomRight(D3DCOLOR color) {
		m_colorBottomRight = color;
		return true;
	}

	bool EnableNonUserEvents(bool bEnable) {
		m_bNonUserEvents = bEnable;
		return true;
	}

	bool SetModal(bool modal);

	bool SetCaptureKeys(bool capture);

	bool EnableKeyboardInput(bool bEnable) {
		m_bKeyboardInput = bEnable;
		return true;
	}
	bool EnableMouseInput(bool bEnable) {
		m_bMouseInput = bEnable;
		return true;
	}

	bool SetCloseOnESC(bool bEnable) {
		m_bCloseOnESC = bEnable;
		return true;
	}
	bool GetCloseOnESC() {
		return m_bCloseOnESC;
	}

	bool SetEdit(bool bEnable) {
		m_bEdit = bEnable;
		return true;
	}

	bool OnMoved();
	bool OnResized();

	bool SetAllControls(bool bVisible, bool bEnabled);

	bool CheckForDuplicateControlName(const char* strName, bool bShowMsg);
	bool CheckForDuplicateControlNames(bool bShowMsg);

	bool ContainsPoint(POINT pt);

	bool ApplyLocationFormatting();

	int GetScreenHeight();
	int GetScreenWidth();

	bool IsLMouseDown();
	bool IsRMouseDown();

	// Sets the callback used to notify the app of dialog events
	bool SetCallback(PCALLBACKDXUTDIALOGGUIEVENT pCallback) {
		m_pDialogCallbackEvent = pCallback;
		return true;
	}

	// Sets the callback used to notify the app of control events
	bool SetCallback(PCALLBACKDXUTCONTROLGUIEVENT pCallback) {
		m_pControlCallbackEvent = pCallback;
		return true;
	}

	void Refresh();

	void OnBeginFrame() override;
	void OnEndFrame() override;

	HRESULT OnRender(float fElapsedTime);

	// Shared resource access. Indexed fonts and textures are shared among all the controls.
	int SetFont(size_t index, const std::string& strFaceName, LONG height, LONG weight, bool italic, bool underline);

	CDXUTFontNode* GetFont(size_t index);

	int AddFont(const std::string& strFaceName, LONG height, LONG weight, bool italic, bool underline);

	// helper to get the 'global' (across all dialogs) font index
	int GetResourceManagerFontIndex(size_t index);

	int SetTexture(size_t index, const std::string& strFilename, D3DXIMAGE_INFO* textInfoToBeReturned = nullptr) override;
	DXUTTextureNode* GetTexture(size_t index) override;
	int AddTexture(const std::string& strFilename, D3DXIMAGE_INFO* textInfoToBeReturned = nullptr) override;
	int AddExternalTexture(const std::string& strFilename) override;
	int AddDynamicTexture(unsigned dynTextureId) override;

	bool SetEnabled(bool enable);

	/** DRF
	* Returns whether or not the given point is within this menu.
	*/
	bool HitTest(int x, int y) const;

	IMenuControl* LoadControl(TiXmlNode* controlXml);
	IMenuControl* LoadControlByXml(const std::string& xml);

	bool MoveControlForward(const std::string& ctlName);
	bool MoveControlBackward(const std::string& ctlName);
	bool MoveControlToFront(const std::string& ctlName);
	bool MoveControlToBack(const std::string& ctlName);
	bool MoveControlToIndex(const std::string& ctlName, unsigned int index);

	SpriteTextureAtlas& GetTextureAtlas();

protected:
	int IdxFontRemap(int idxRM);
	int IdxTextureRemap(int idxRM);

	bool LoadDefaultElements();

	// Menu XML Resources Load Functions
	bool LoadMenuXmlTitlebar(TiXmlNode* titlebar_root);
	bool LoadMenuXmlScript(TiXmlNode* script_root);
	bool LoadMenuXmlDefaultTexture(TiXmlNode* default_texture_root);
	bool LoadMenuXmlDefaultFont(TiXmlNode* default_font_root);
	bool LoadMenuXmlLocation(TiXmlNode* location_root);
	bool LoadMenuXmlSize(TiXmlNode* size_root);
	bool LoadMenuXmlBackgroundColors(TiXmlNode* background_colors_root);

	// Menu XML Resources Save Functions
	bool SaveMenuXmlTitlebar(TiXmlNode* dialog_root);
	bool SaveMenuXmlLocation(TiXmlNode* dialog_root);
	bool SaveMenuXmlSize(TiXmlNode* dialog_root);
	bool SaveMenuXmlBackgroundColors(TiXmlNode* dialog_root);
	bool SaveMenuXmlElements(TiXmlNode* dialog_root);
	bool SaveMenuXmlDefaultFont(TiXmlNode* dialog_root);
	bool SaveMenuXmlDefaultTexture(TiXmlNode* dialog_root);
	bool SaveMenuXmlScript(TiXmlNode* dialog_root);

	// controls
	IMenuControl* LoadStatic(TiXmlNode* static_root);
	IMenuControl* LoadImage(TiXmlNode* image_root);
	IMenuControl* LoadFlash(TiXmlNode* image_root);
	IMenuControl* LoadButton(TiXmlNode* button_root);
	IMenuControl* LoadCheckBox(TiXmlNode* check_box_root);
	IMenuControl* LoadRadioButton(TiXmlNode* radio_button_root);
	IMenuControl* LoadComboBox(TiXmlNode* combo_box_root);
	IMenuControl* LoadSlider(TiXmlNode* slider_root);
	IMenuControl* LoadScrollBar(TiXmlNode* scroll_bar_root);
	IMenuControl* LoadEditBox(TiXmlNode* edit_box_root, bool typeSimple);
	IMenuControl* LoadListBox(TiXmlNode* list_box_root);
	IMenuControl* LoadComment(TiXmlNode* comment_root);

	bool SaveControls(TiXmlNode* dialog_root);
	void RenderControl(IDirect3DDevice9* pd3dDevice, ID3DXSprite* sprite, IMenuControl* pControl, float fElapsedTime);

private:
	static const int SNAP_THRESHOLD = 5; // pixels from edge

	static double s_fTimeRefresh;
	static bool m_LMouseDown;
	static bool m_RMouseDown;
	double m_fTimeLastRefresh;

	void Init();

	// Initialize default Elements
	void InitDefaultElements();
	void InitDefaultElementsHelper();

	// Windows message handlers
	void OnMouseMove(POINT pt);
	void OnMouseUp(POINT pt);

	// Control events
	bool OnCycleFocus(bool bForward);
	bool OnCycleSelection(bool bForward);
	void OnMouseEnter(CDXUTControl* pControl);
	void OnMouseLeave(CDXUTControl* pControl);

	bool RemoveControl(IMenuControl* pControl) {
		return pControl ? RemoveControl(pControl->GetName()) : false;
	}

	void StartDraggingByCaption(const POINT& mousePoint);
	bool EndDraggingByCaption(const POINT& mousePoint);
	void CancelDraggingByCaption(const POINT& mousePoint);
	void HandleDraggingByCaption(const POINT& mousePoint);

	static void _SetRefreshTime(float fTime) {
		s_fTimeRefresh = fTime;
	}

	static void _ClearFocus();
	static void _ClearSelection();

	static IMenuControl* _GetNextControl(IMenuControl* pControl);
	static IMenuControl* _GetPrevControl(IMenuControl* pControl);

	static IMenuControl* s_pControlFocus; // The control which has focus
	static IMenuControl* s_pControlPressed; // The control currently pressed
	static IMenuControl* s_pControlSelected; // The control currently selected (for editing)

public:
	/** DRF
	* Returns the dialog currently in focus or NULL if no dialog is currently in focus.
	*/
	static CDXUTDialog* GetDialogInFocus();

	static bool IsDialogInFocusCapturingKeys() {
		auto pDialog = GetDialogInFocus();
		return (pDialog && pDialog->GetCaptureKeys()); // default false
	}

	static bool IsDialogInFocusMouseEnabled() {
		auto pDialog = GetDialogInFocus();
		return (!pDialog || pDialog->GetEnableMouseInput()); // default true
	}

	static bool IsDialogInFocusKeyboardEnabled() {
		auto pDialog = GetDialogInFocus();
		return (!pDialog || pDialog->GetEnableKeyboardInput()); // default true
	}

	/** DRF
	* Returns if this dialog is currently in focus.
	*/
	bool IsInFocus() {
		return (GetDialogInFocus() == this);
	}

private:
	IMenuControl* m_pControlMouseOver; // The control which is hovered over

	int m_ID;

	bool m_bCaption;
	bool m_bCaptionShadow;
	bool m_bMinimized;
	bool m_bSnapToEdges;
	bool m_bEdit;

	bool m_bMovable;
	bool m_bResizable;

	bool m_bIsSelected; // dialog has been selected for editing
	bool m_bModal;

	bool m_bBringToFrontEnable;
	bool m_bBringToFront;

	char m_szCaption[256];

	int m_x;
	int m_y;
	int m_xOffset;
	int m_yOffset;
	int m_width;
	int m_height;
	DWORD m_dwLocationFormat;
	int m_nCaptionHeight;
	D3DCOLOR m_colorTopLeft;
	D3DCOLOR m_colorTopRight;
	D3DCOLOR m_colorBottomLeft;
	D3DCOLOR m_colorBottomRight;

	PCALLBACKDXUTDIALOGGUIEVENT m_pDialogCallbackEvent;
	PCALLBACKDXUTCONTROLGUIEVENT m_pControlCallbackEvent;

	CGrowableArray<int> m_Textures; // Index into m_TextureCache;
	CGrowableArray<int> m_Fonts; // Index into m_FontCache;

	int m_nextControlValue; // used for default ID when adding new controls

	typedef std::vector<IMenuControl*> MenuControlVector;
	typedef MenuControlVector::iterator MenuControlIter;
	MenuControlVector m_Controls;

	CGrowableArray<DXUTElementHolder*> m_DefaultElements;

	CDXUTElement* m_pCapElement; // Element for the caption
	CDXUTElement* m_pBodyElement; // Element for the body background
	CDXUTElement* m_pIconElement; // Element for the titlebar icon

	IMenuDialog* m_pNextDialog;
	IMenuDialog* m_pPrevDialog;

	CDXUTSelectionHandles m_selectionHandles;

	bool m_bDragCaption;
	POINT m_prevMousePointCaption;
	POINT m_initialMousePointCaption;

	static std::string s_sDefaultMenuDir;

	bool m_trustedMenu;

	SpriteTextureAtlas* m_textureAtlas;
};

// DRF - NOTE - Shares CTextureEX Resources With TextureDatabase
struct DXUTTextureNode {
public:
	enum TYPE {
		TYPE_NONE,
		TYPE_INTERNAL, // texture source is internal
		TYPE_EXTERNAL, // texture source is external (pTexture shared with TextureDatabase)
		TYPE_DYNAMIC, // dynamic, procedurally generated textures
	};

	DXUTTextureNode(const TYPE& type, const std::string& fileName = "", const std::string& filePath = "") :
			m_type(type), m_fileName(fileName), m_filePath(filePath), m_dynTextureId(0), m_width(0), m_height(0), m_dimensionSet(false), m_pTexture(NULL), m_surfaceDesc{} {}

	~DXUTTextureNode() {
		SetTexture(nullptr);
	}

	TYPE GetType() const {
		return m_type;
	}
	void SetType(const TYPE& type) {
		m_type = type;
	}
	bool TypeNone() const {
		return (m_type == TYPE_NONE);
	}
	bool TypeInternal() const {
		return (m_type == TYPE_INTERNAL);
	}
	bool TypeExternal() const {
		return (m_type == TYPE_EXTERNAL);
	}

	const char* GetFilePathPtr() const {
		return m_filePath.c_str();
	}
	std::string GetFilePath() const {
		return m_filePath;
	}

	const char* GetFileNamePtr() const {
		return m_fileName.c_str();
	}
	std::string GetFileName() const {
		return m_fileName;
	}

	unsigned GetDynamicTextureId() const {
		assert(m_type == TYPE_DYNAMIC);
		return m_dynTextureId;
	}
	void SetDynamicTextureId(unsigned dynTextureId) {
		assert(m_type == TYPE_DYNAMIC);
		m_dynTextureId = dynTextureId;
	}

	DWORD GetWidth() const {
		return m_width;
	}
	void SetWidth(const DWORD& width) {
		m_width = width;
		m_dimensionSet = true;
	}

	DWORD GetHeight() const {
		return m_height;
	}
	void SetHeight(const DWORD& height) {
		m_height = height;
		m_dimensionSet = true;
	}

	void InitDimension(IDirect3DTexture9* pD3DTexture);
	bool IsDimensionSet() const {
		return m_dimensionSet;
	}

	IDirect3DTexture9* GetTexture() const;
	void SetTexture(IDirect3DTexture9* pTexture);

	const D3DSURFACE_DESC& GetSurfaceDesc() const {
		return m_surfaceDesc;
	}
	UINT GetSurfaceWidth() const {
		return m_surfaceDesc.Width;
	}
	UINT GetSurfaceHeight() const {
		return m_surfaceDesc.Height;
	}

private:
	TYPE m_type;

	std::string m_fileName;
	std::string m_filePath;

	unsigned m_dynTextureId; // For dynamic textures only

	DWORD m_width;
	DWORD m_height;
	bool m_dimensionSet;

	//Ankit ----- will always be the level 0 surface desc
	D3DSURFACE_DESC m_surfaceDesc;

	IDirect3DTexture9* m_pTexture;
};

class CDXUTFontNode : public IMenuFont {
public:
	std::string strFace;
	LONG nHeight;
	LONG nWeight;
	bool bItalic;
	bool bUnderline;
	ID3DXFont* pFont;

	CDXUTFontNode() :
			pFont(0), nHeight(0), nWeight(0), bItalic(false), bUnderline(false) {}
	~CDXUTFontNode() {}

	const char* GetFacePtr() const {
		return strFace.c_str();
	}
	std::string GetFace() const {
		return strFace;
	}
	LONG GetHeight() const {
		return nHeight;
	}
	LONG GetWeight() const {
		return nWeight;
	}
	bool GetItalic() const {
		return bItalic;
	}
	bool GetUnderline() const {
		return bUnderline;
	}
	ID3DXFont* GetFont() const {
		return pFont;
	}
};

//-----------------------------------------------------------------------------
// Manages shared resources of dialogs
// Use DXUTGetGlobalDialogResourceManager() to get access to the global instance
//-----------------------------------------------------------------------------
class CDXUTDialogResourceManager {
public:
	virtual ~CDXUTDialogResourceManager() {}

	/** DRF
	* Returns the available texture memory in bytes.
	*/
	UINT TextureMemoryAvail();

	/** DRF
	* Returns if the client window is currently in view.
	*/
	bool IsInView();

	/** DRF
	* Most DirectX functions fail while the graphics device is out of view or minimized.
	* This function checks if the last call failed and logs a warning if out of view or
	* an error if in view.
	*/
	bool HResultFailed(HRESULT hr, const std::string& msg);

	IDirect3DDevice9* GetD3DDevice() {
		return m_pd3dDevice;
	}

	size_t FontNodes() {
		return (size_t)m_FontCache.GetSize();
	};
	CDXUTFontNode* GetFontNode(size_t index) {
		return m_FontCache.GetAt(index);
	};
	bool ReleaseFontNode(size_t index);

	int AddFont(const std::string& strFaceName, LONG height, LONG weight, bool italic, bool underline);

	size_t TextureNodes() {
		return (size_t)m_TextureCache.GetSize();
	};
	DXUTTextureNode* GetTextureNode(size_t index) {
		return m_TextureCache.GetAt(index);
	};
	bool ReleaseTextureNode(size_t index);

	// DRF - ED-7035 - Investigate texture memory impact
	bool ReleaseTextureNode(IDirect3DTexture9* pTex);

	int AddTexture(const std::string& strDirectoryName, const std::string& strFilename, D3DXIMAGE_INFO* texInfoToBeReturned = nullptr);
	int AddExternalTexture(const std::string& strFilename);
	int AddDynamicTexture(unsigned dynTextureId);

	// Shared between all dialogs
	IDirect3DStateBlock9* m_pStateBlock;
	ID3DXSprite* GetSprite(); // This can return a null ptr.
	D3DXSpriteWrapper* GetSpriteWrapper() const {
		return m_pSprite;
	} // This should only be used to access wrapper specific functions. D3D device might not be available.

	void OnLostDevice();
	HRESULT OnResetDevice();

protected:
	friend CDXUTDialogResourceManager* DXUTGetGlobalDialogResourceManager();
	friend HRESULT DXUTInitialize3DEnvironment();
	friend HRESULT DXUTReset3DEnvironment();
	friend void DXUTCleanup3DEnvironment(bool bReleaseSettings);
	friend void DXUTPrepareDevice(IDirect3DDevice9* pd3dDevice);

	// Use DXUTGetGlobalDialogResourceManager() to get access to the global instance
	CDXUTDialogResourceManager() :
			m_pd3dDevice(nullptr),
			m_pd3dDeviceWrapper(nullptr),
			m_pStateBlock(nullptr),
			m_pSprite(nullptr) {}

	// The sample framework uses the global instance and automatically calls the device events
	HRESULT OnCreateDevice(LPDIRECT3DDEVICE9 pd3dDevice);

	void OnDestroyDevice();

	CGrowableArray<DXUTTextureNode*> m_TextureCache; // Shared textures
	CGrowableArray<CDXUTFontNode*> m_FontCache; // Shared fonts

	IDirect3DDevice9* m_pd3dDevice;
	D3D9DeviceWrapper* m_pd3dDeviceWrapper; // Device wrapper to hook into D3D API calls from ID3DXSprite and ID3DXFont
	D3DXSpriteWrapper* m_pSprite; // Sprite used for drawing. Shared between all dialogs

	// Resource creation helpers
	int DxCreateFont(size_t index); // returns enum IDX_xxx
	int DxCreateTexture(size_t index, D3DXIMAGE_INFO* texInfoToBeReturned = nullptr); // returns enum IDX_xxx
};

CDXUTDialogResourceManager* DXUTGetGlobalDialogResourceManager();

} // namespace KEP