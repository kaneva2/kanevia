/******************************************************************************
 script_register_function.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#ifndef SCRIPT_GLUE_VOID_RET

template <typename P1, typename P2>
class tuple {
public:
	P1 p1;
	P2 p2;

	template <int I, typename P>
	void set_param(P p) {
	}

	template <int I, typename P>
	P get_param() {
	}

	template <>
	void set_param<0, P1>(P1 p) {
		p1 = p;
	}

	template <>
	P1 get_param<0, P1>() {
		return p1;
	}

	template <>
	void set_param<1, P2>(P2 p) {
		p2 = p;
	}

	template <>
	P2 get_param<1, P2>() {
		return p2;
	}
};

template <int I, typename T, typename U>
class enum_params {
private:
	enum { go = (I - 1) != 0 };

public:
	static inline void f(lua_State *L, U u) {
		T::get_param_type<I>::param_type in_val =
			get_parameter<T::get_param_type<I>::param_type>(L, I);
		u.set_param<I, T::get_param_type<I>::param_type>(in_val);

		enum_params<go ? (I - 1) : 0, T, U>::f(L, u);
	}
};

template <typename T, typename U>
class enum_params<0, T, U> {
public:
	static inline void f(lua_State *L, U u) {
		T::get_param_type<0>::param_type in_val =
			get_parameter<T::get_param_type<0>::param_type>(L, 0);
		u.set_param<0, T::get_param_type<0>::param_type>(in_val);
	}
};

#endif

/////////////////////////////////////////////////////////////////
// script registry to glue it all together for three params
/////////////////////////////////////////////////////////////////

#ifdef SCRIPT_GLUE_VOID_RET
#define out_val_type void

template <
	typename in_val_type1,
	typename in_val_type2,
	typename in_val_type3,
	typename in_val_type4,
	typename in_val_type5,
	typename in_val_type6,
	typename in_val_type7,
	typename in_val_type8,
	typename in_val_type9,
	typename in_val_type10,
	typename in_val_type11,
	typename in_val_type12,
	typename in_val_type13,
	typename in_val_type14,
	typename in_val_type15>
class register_native_function<
	void,
	in_val_type1,
	in_val_type2,
	in_val_type3,
	in_val_type4,
	in_val_type5,
	in_val_type6,
	in_val_type7,
	in_val_type8,
	in_val_type9,
	in_val_type10,
	in_val_type11,
	in_val_type12,
	in_val_type13,
	in_val_type14,
	in_val_type15>
#else

template <
	typename out_val_type,
	typename in_val_type1 = null_parameter,
	typename in_val_type2 = null_parameter,
	typename in_val_type3 = null_parameter,
	typename in_val_type4 = null_parameter,
	typename in_val_type5 = null_parameter,
	typename in_val_type6 = null_parameter,
	typename in_val_type7 = null_parameter,
	typename in_val_type8 = null_parameter,
	typename in_val_type9 = null_parameter,
	typename in_val_type10 = null_parameter,
	typename in_val_type11 = null_parameter,
	typename in_val_type12 = null_parameter,
	typename in_val_type13 = null_parameter,
	typename in_val_type14 = null_parameter,
	typename in_val_type15 = null_parameter>
class register_native_function

#endif
{
public:
	static int main_entry(lua_State *L) {
		// this is the entry point from lua
		//  pop the parameters off the stack

		// get our 'this' pointer
		typedef register_native_function<
			out_val_type,
			in_val_type1,
			in_val_type2,
			in_val_type3,
			in_val_type4,
			in_val_type5,
			in_val_type6,
			in_val_type7,
			in_val_type8,
			in_val_type9,
			in_val_type10,
			in_val_type11,
			in_val_type12,
			in_val_type13,
			in_val_type14,
			in_val_type15>
			this_type;

		typename typedef function_signature<out_val_type,
			in_val_type1,
			in_val_type2,
			in_val_type3,
			in_val_type4,
			in_val_type5,
			in_val_type6,
			in_val_type7,
			in_val_type8,
			in_val_type9,
			in_val_type10,
			in_val_type11,
			in_val_type12,
			in_val_type13,
			in_val_type14,
			in_val_type15>
			OurFunctionType;

		this_type *mythis = (this_type *)lua_touserdata(L, lua_upvalueindex(1));

		in_val_type1 in_val1 = get_parameter<in_val_type1>(L, 1);
		in_val_type2 in_val2 = get_parameter<in_val_type2>(L, 2);
		in_val_type3 in_val3 = get_parameter<in_val_type3>(L, 3);
		in_val_type4 in_val4 = get_parameter<in_val_type4>(L, 4);
		in_val_type5 in_val5 = get_parameter<in_val_type5>(L, 5);
		in_val_type6 in_val6 = get_parameter<in_val_type6>(L, 6);
		in_val_type7 in_val7 = get_parameter<in_val_type7>(L, 7);
		in_val_type8 in_val8 = get_parameter<in_val_type8>(L, 8);
		in_val_type9 in_val9 = get_parameter<in_val_type9>(L, 9);
		in_val_type10 in_val10 = get_parameter<in_val_type10>(L, 10);
		in_val_type11 in_val11 = get_parameter<in_val_type11>(L, 11);
		in_val_type12 in_val12 = get_parameter<in_val_type12>(L, 12);
		in_val_type13 in_val13 = get_parameter<in_val_type13>(L, 13);
		in_val_type14 in_val14 = get_parameter<in_val_type14>(L, 14);
		in_val_type15 in_val15 = get_parameter<in_val_type15>(L, 15);

		int our_ret_val = 0;

		// now actually call the function

#ifndef SCRIPT_GLUE_VOID_RET
		out_val_type ret_val =
#endif
			function_signature<
				out_val_type,
				in_val_type1,
				in_val_type2,
				in_val_type3,
				in_val_type4,
				in_val_type5,
				in_val_type6,
				in_val_type7,
				in_val_type8,
				in_val_type9,
				in_val_type10,
				in_val_type11,
				in_val_type12,
				in_val_type13,
				in_val_type14,
				in_val_type15>::make_call(in_val1,
				in_val2,
				in_val3,
				in_val4,
				in_val5,
				in_val6,
				in_val7,
				in_val8,
				in_val9,
				in_val10,
				in_val11,
				in_val12,
				in_val13,
				in_val14,
				in_val15,
				mythis->m_f);

#ifndef SCRIPT_GLUE_VOID_RET
		// now push the results back onto the stack
		push_parameter<out_val_type>(L, ret_val);
		our_ret_val = 1;
#endif

		return our_ret_val;
	}

	typename function_signature<out_val_type,
		in_val_type1,
		in_val_type2,
		in_val_type3,
		in_val_type4,
		in_val_type5,
		in_val_type6,
		in_val_type7,
		in_val_type8,
		in_val_type9,
		in_val_type10,
		in_val_type11,
		in_val_type12,
		in_val_type13,
		in_val_type14,
		in_val_type15>::FunctionType m_f;

	register_native_function(lua_State *L, std::string luaenum_params,
		typename function_signature<out_val_type,
			in_val_type1,
			in_val_type2,
			in_val_type3,
			in_val_type4,
			in_val_type5,
			in_val_type6,
			in_val_type7,
			in_val_type8,
			in_val_type9,
			in_val_type10,
			in_val_type11,
			in_val_type12,
			in_val_type13,
			in_val_type14,
			in_val_type15>::FunctionType f) {
		m_f = f;

		// register our main entry point
		lua_pushlightuserdata(L, this);
		lua_pushcclosure(L, main_entry, 1);
		lua_setglobal(L, luaenum_params.c_str());
	}
};

#undef out_val_type
