#pragma once

#include "LuaHelper.h"
#include "ScriptVM.h"
#include "..\..\Event\LuaEventHandler\LuaScriptVM.h"
#include "../imenugui.h"
#include <string>

#define STATE_TYPE script_glue::ScriptVM<lua_State>
#define MAX_NODES (128) // drf - maximum number of loaded menu scripts

namespace KEP {

typedef struct
{
	STATE_TYPE *state; // menu script vm state
	IMenuDialog *pDialog; // menu script render dialog
	std::string scriptFilePath; // full\path\to\menuScript.lua
} MapNode;

extern MapNode g_map[];

// this define controls Handler Common
inline LuaScriptVM* MAKE_SCRIPT_VM(lua_State* s) {
	return new LuaScriptVM(s);
}

} // namespace KEP