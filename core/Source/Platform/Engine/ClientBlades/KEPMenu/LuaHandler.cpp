///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <Windows.h>
#include "LuaHelper.h"
#include "IClientEngine.h"
#include "..\..\Event\Base\EventHeader.h"
#include "..\..\Event\Base\IDispatcher.h"
#include "..\..\Event\Events\DialogEvents.h"
#include "Handler.h"
#include "MenuCInterface.h"
#include "scriptVM.h"
#include "LuaMenuRegistry.h"
#include "LuaHandler.h"
#include "..\..\Event\LuaEventHandler\LuaErrorHandler.h"
#include "..\..\Event\LuaEventHandler\LuaScriptVM.h"
#include "KEPMenu.h"
#include "common\KEPUtil\Helpers.h"
#include "Common/include/KEPHelpers.h"
#define DRF_ERROR_DATA_LUA_CALL 0
#if (DRF_ERROR_DATA_LUA_CALL == 1)
#include "Common\KEPUtil\CrashReporter.h"
#endif
#include "..\KEPUtil\RefPtr.h"
#include "LogHelper.h"
#include "function_signature.h"
#include <assert.h>
#include <map>

using namespace script_glue;
using namespace std;

namespace KEP {

typedef unsigned int LuaVMHandle;

void RegisterScriptFunctions(STATE_TYPE *state);

static LogInstance("Script");
MapNode g_map[MAX_NODES]; // drf - list of all loaded menu scripts

template <typename P1 = null_parameter, typename P2 = null_parameter, typename P3 = null_parameter, typename P4 = null_parameter, typename P5 = null_parameter>
struct arg_info {
	enum { how_many = 5 };
};

template <typename P1, typename P2, typename P3, typename P4>
struct arg_info<P1, P2, P3, P4, null_parameter> {
	enum { how_many = 4 };
};

template <typename P1, typename P2, typename P3>
struct arg_info<P1, P2, P3, null_parameter, null_parameter> {
	enum { how_many = 3 };
};

template <typename P1, typename P2>
struct arg_info<P1, P2, null_parameter, null_parameter, null_parameter> {
	enum { how_many = 2 };
};

template <typename P1>
struct arg_info<P1, null_parameter, null_parameter, null_parameter, null_parameter> {
	enum { how_many = 1 };
};

template <>
struct arg_info<null_parameter, null_parameter, null_parameter, null_parameter, null_parameter> {
	enum { how_many = 0 };
};

/******************************************************************************
 * call_lua_dialog_function
 *   The only reason this is a struct is so that we can default template args
 ******************************************************************************/
template <typename P1 = null_parameter, typename P2 = null_parameter, typename P3 = null_parameter, typename P4 = null_parameter, typename P5 = null_parameter>
struct call_lua_dialog_function {
	static bool do_it(
		IMenuDialog* pDialog,
		const char* fnName,
		P1 p1 = null_parameter(),
		P2 p2 = null_parameter(),
		P3 p3 = null_parameter(),
		P4 p4 = null_parameter(),
		P5 p5 = null_parameter()) {
		// Valid Dialog ?
		if (!pDialog) {
			_LogError("LuaHandler::call_lua_dialog_function: Null Dialog Pointer");
			return false;
		}

		// Find Dialog VM State Map Slot
		int slot = FindSlotByDialog(pDialog);
		if (slot < 0) {
			// Dynamically Cast Dialog
			CDXUTDialog* cdxutDialog = dynamic_cast<CDXUTDialog*>(pDialog);
			if (!cdxutDialog) {
				_LogError("LuaHandler::call_lua_dialog_function: FindSlotByDialog(" << pDialog << ") FAILED");
				_LogWarn("... <dynamic_cast_failure>::" << fnName << "()");
				return false;
			}

			// Is Dialog Script Even Loaded ? (dialogs open for edit never have script loaded)
			if (!cdxutDialog->IsScriptLoaded()) {
				//_LogDebug("LuaHandler::call_lua_dialog_function: IsScriptLoaded("<<pDialog<<" FAILED");
				return false;
			}

			// DRF - This should no longer happen.  Fix something if it does.
			_LogError("LuaHandler::call_lua_dialog_function: FindSlotByDialog(" << pDialog << ") FAILED");
			_LogWarn("... " << cdxutDialog->GetScriptFileName() << "::" << fnName << "()");
			return false;
		}

		// Get Script Full Name
		std::string funcName = fnName ? fnName : "<null>";
		std::string fileName = StrFileNameExt(g_map[slot].scriptFilePath);
		std::string fullName = fileName + "::" + funcName + "()";
		LogDebug(fullName);

		// Get Dialog VM State
		lua_State* L = g_map[slot].state->GetNativeHandle();
		if (!L) {
			_LogError("LuaHandler::call_lua_dialog_function: lua_State->GetNativeHandle() FAILED");
			_LogError("... " << fullName);
			return false;
		}

		// Valid Function ?
		if (!fnName) {
			_LogError("LuaHandler::call_lua_dialog_function: Null Function");
			_LogError("... " << fullName);
			return false;
		}

		// DRF - Find Dialog In KEPMenu Dialog List (just incase)
		auto pKepMenu = GetKepMenu();
		bool found = (pKepMenu ? pKepMenu->DialogFind(pDialog) : false);
		if (!found) {
			_LogError("LuaHandler::call_lua_dialog_function: FindDialog(" << pDialog << ") FAILED");
			_LogError("... " << fullName);
			return false;
		}

		// Push Error Handler Function
		lua_pushcfunction(L, LuaErrorHandler);
		int argErrorHandler = lua_gettop(L);

		// Get Function To Call From VM (internally pushes the function)
		lua_getglobal(L, fnName);
		if (!lua_isfunction(L, -1)) {
			lua_pop(L, 1); // remove whatever getglobal pushed
			lua_pop(L, 1); // remove error handler function
			return false;
		}

		// Push Function Parameters
		push_parameter(L, p1);
		push_parameter(L, p2);
		push_parameter(L, p3);
		push_parameter(L, p4);
		push_parameter(L, p5);

		int nargs = arg_info<P1, P2, P3, P4, P5>::how_many;

#if (DRF_ERROR_DATA_LUA_CALL == 1)
		// DRF - Set 'luaCall' Crash Report Error Data
		CrashReporter::SetErrorData("luaCall", fullName);
#endif

		// Try Lua Function Call
		bool ok = false;
		try {
			ok = (lua_pcall(L, nargs, 0, argErrorHandler) == 0);
		}
		catch (...) {
			// Catch Everything As Failure
			_LogError("LuaHandler::call_lua_dialog_function: Caught Exception in lua_pcall()");
			goto LogError; // drf - lua_remove(argErrorHandler) may crash
		}

#if (DRF_ERROR_DATA_LUA_CALL == 1)
		// DRF - Clear 'luaCall' Crash Report Error Data
		CrashReporter::SetErrorData("luaCall", "");
#endif

		// Pop(remove) Error Handler Function
		lua_remove(L, argErrorHandler);

		// Return Ok If No Error
		if (ok)
			return true;

	LogError:
		// Read & Pop Error Message
		stringstream ss;
		const char* tempMsg = lua_tostring(L, -1);

		if (tempMsg == nullptr) {
			ss << "LuaHandler: Unknown error in [" << fullName << "]";
		}
		else {
			ss << "LuaHandler: Error in " << fullName << "...\n"
				<< tempMsg;
			lua_pop(L, 1);
		}

		std::string errMsg = ss.str();

		_LogError("LuaHandler: Error in " << fullName << "...\n"
			<< errMsg);

		// No Errors Inside Errors
		if (STLContainsIgnoreCase(fnName, "OnError")) {
			_LogError("LuaHandler: Error inside OnError - Ignoring");
			return false;
		}

		// Call Dialog_OnError() (logs error message including call stack)
		if (REF_PTR_VALID(pDialog))
			Handlers::OnError(pDialog, errMsg.c_str());

		return false;
		}
	};

// added to allow register to be called from outside for just a VM
extern "C" void RegisterScriptFunctionsForVM(IScriptVM* state) {
	LuaScriptVM* luaVM = dynamic_cast<LuaScriptVM*>(state);
	script_glue::ScriptVM<lua_State> svm(luaVM->GetState());
	RegisterScriptFunctions(&svm);
}

// Works exactly like LUA's dofile, but will try multiple directories before giving up.
static int multipath_dofile(lua_State* L) {
	// The actual dofile code (as opposed to the code to check different folders)
	// comes from lbaselib.c luaB_dofile(), which is the original LUA implementation.
	const char* fname = luaL_optstring(L, 1, NULL);

	// Check to see if the file exists in the various places such things can
	// exist.  If it doesn't exist anywhere, go ahead and allow loaL_loadfile()
	// to fail and lua_error() to be called, so an error is returned.
	std::string fullPath;
	if (!KEPMenu::Instance()->SearchFile(fname, true, false, fullPath)) {
		fullPath = fname;
		LogError("SearchFile() FAILED - fname='" << fname << "'");
	}

	int n = lua_gettop(L);
	int status = LuaLoadFileProtected(L, fullPath.c_str(), true);
	if (status != 0)
		lua_error(L);
	lua_call(L, 0, LUA_MULTRET);
	return lua_gettop(L) - n;
}

int LoadMenuScript(IMenuDialog* pDialog, const std::string& scriptFilePath) {
	// as a sanity check, only load files with ".lua" in the name (.luac ok)
	if (!StrSameFileExt(scriptFilePath, "lua"))
		return -1;

	// Find Slot in g_map[]
	int slot = FindOpenSlot();
	if (slot >= 0) {
		script_glue::ScriptVM<lua_State>* script_vm = new script_glue::ScriptVM<lua_State>();

		// Open Lua Libraries (restricted)
		LUA_OPENLIBS(script_vm->GetNativeHandle(), true);

		// Define global variable __ID__ for current menu identifier
		lua_pushstring(script_vm->GetNativeHandle(), pDialog->GetMenuIdPtr());
		lua_setglobal(script_vm->GetNativeHandle(), "__ID__");

		// Define global variable __MENU__ for current menu file name
		lua_pushstring(script_vm->GetNativeHandle(), pDialog->GetMenuFileNamePtr());
		lua_setglobal(script_vm->GetNativeHandle(), "__MENU__");

		// Define global variable __SCRIPT__ for current script file name
		lua_pushstring(script_vm->GetNativeHandle(), pDialog->GetScriptFileNamePtr());
		lua_setglobal(script_vm->GetNativeHandle(), "__SCRIPT__");

		// Define global variable __LUA_COMPAT_LEVEL__
		// TODO: use a better mechanism to determine a script's API level
		lua_pushnumber(script_vm->GetNativeHandle(), pDialog->IsTrustedMenu() ? CURRENT_SCRIPT_LUA_COMPAT_LEVEL : LEGACY_SCRIPT_LUA_COMPAT_LEVEL);
		lua_setglobal(script_vm->GetNativeHandle(), "__LUA_COMPAT_LEVEL__");

		// Define global variable __LUA_VERSION__
		lua_pushnumber(script_vm->GetNativeHandle(), LUA_VERSION_NUM);
		lua_setglobal(script_vm->GetNativeHandle(), "__LUA_VERSION__");

		lua_pushnumber(script_vm->GetNativeHandle(), Helpers_GetDevMode());
		lua_setglobal(script_vm->GetNativeHandle(), "__DEV_MODE__");

		// replace the base library's dofile with our version
		lua_pushcfunction(script_vm->GetNativeHandle(), multipath_dofile);
		lua_setglobal(script_vm->GetNativeHandle(), "dofile");

		// Assign g_map[slot]
		g_map[slot].state = script_vm;
		g_map[slot].pDialog = pDialog;
		g_map[slot].scriptFilePath = scriptFilePath;

		RegisterScriptFunctions(g_map[slot].state);

		if (script_vm->LoadFile(scriptFilePath)) {
			lua_State* state = g_map[slot].state->GetNativeHandle();
			LogInfo("Script <" << pDialog << ":" << state << "> '" << StrFileNameExt(scriptFilePath) << "' OK");
			return 0;
		}
		else {
			const char* err = lua_tostring(g_map[slot].state->GetNativeHandle(), -1);
			LogError("'" << LogPath(scriptFilePath) << "' FAILED");
			LogError("... " << (err == nullptr ? "(null)" : err));

			// Make sure the slot can be re-used by FindOpenSlot.
			delete g_map[slot].state;
			g_map[slot].state = 0;
			g_map[slot].pDialog = 0;
			g_map[slot].scriptFilePath.clear();
		}
	}
	return -1;
}

namespace Handlers {

bool OnError(IMenuDialog* pDialog, const char* errMsg) {
	// DRF - Validate Reference Pointer To Avoid Errors Inside Errors
	if (!REF_PTR_VALID(pDialog))
		return false;

	return call_lua_dialog_function<IMenuDialog*, const char*>::do_it(
		pDialog,
		"Dialog_OnError",
		pDialog,
		errMsg);
}

bool OnExit(IMenuDialog* pDialog) {
	if (!pDialog)
		return false;

	// Call Dialog On_Exit -> lua::Helper_Dialog_OnExit()
	return call_lua_dialog_function<IMenuDialog*>::do_it(
		pDialog,
		"Dialog_OnExit",
		pDialog);
}

bool OnCreate(IMenuDialog* pDialog) {
	if (!pDialog)
		return false;

	// Broadcast 'DialogEvent' (filter=DIALOG_OPEN)
	EVENT_ID dialogEventId = KEPMenu::Instance()->GetEngineInterface()->GetDialogEventId();
	IEvent* e = KEPMenu::Instance()->GetEngineInterface()->GetDispatcher()->MakeEvent(dialogEventId);
	e->SetFilter(DIALOG_OPEN);
	*e->OutBuffer() << pDialog->GetMenuFilePath().c_str();
	KEPMenu::Instance()->GetEngineInterface()->GetDispatcher()->QueueEvent(e);

	// Call Dialog On_Create -> lua::Helper_Dialog_OnCreate() -> ... -> lua::KEP_PreInitialize->InitializeKEPEvents/Handlers->KEP_PostInitialize
	return call_lua_dialog_function<IMenuDialog*>::do_it(
		pDialog,
		"Dialog_OnCreate",
		pDialog);
}

bool OnDestroy(IMenuDialog* pDialog) {
	if (!pDialog)
		return false;

	// Broadcast 'DialogEvent' (filter=DIALOG_CLOSE)
	EVENT_ID dialogEventId = KEPMenu::Instance()->GetEngineInterface()->GetDialogEventId();
	IEvent* e = KEPMenu::Instance()->GetEngineInterface()->GetDispatcher()->MakeEvent(dialogEventId);
	e->SetFilter(DIALOG_CLOSED);
	*e->OutBuffer() << pDialog->GetMenuFilePath().c_str();
	KEPMenu::Instance()->GetEngineInterface()->GetDispatcher()->QueueEvent(e);

	bool bHandled = call_lua_dialog_function<IMenuDialog*>::do_it(
		pDialog,
		"Dialog_OnDestroy",
		pDialog);

	//Ensure we unregister our events or an errant handler could be called
	KEPMenu::Instance()->DialogUnRegisterGenericEvents(pDialog);

	return bHandled;
}

bool OnDelete(IMenuDialog* pDialog) {
	int slot = FindSlotByDialog(pDialog);
	if (slot >= 0 && slot < _countof(g_map)) {
		remove_registry(g_map[slot].state->GetNativeHandle());

		delete g_map[slot].state;
		g_map[slot].pDialog = 0;
		g_map[slot].state = 0;
		g_map[slot].scriptFilePath.clear();
	}

	return true;
}

bool OnRender(IMenuDialog* pDialog, float elapsedSec) {
	if (!pDialog)
		return false;

	// Time Limit Script OnRender Calls
	TimeMs timeMs = pDialog->m_timerScriptOnRender.ElapsedMs();
	if (timeMs < pDialog->m_timeScriptOnRender)
		return false;
	pDialog->m_timerScriptOnRender.Reset();

	// Call Script::OnRender()
	return call_lua_dialog_function<IMenuDialog*, float>::do_it(
		pDialog,
		"Dialog_OnRender",
		pDialog,
		timeMs / MS_PER_SEC);
}

bool OnControlEvent(IMenuControl* pControl, const char* eventName) {
	if (!pControl || !eventName)
		return false;

	IMenuDialog* pDialog = Control_GetDialog(pControl);
	if (!pDialog)
		return false;

	// Get Control Name
	const char* ctlName = Control_GetName(pControl);
	if (!ctlName)
		return false;

	// Call Lua <ctlName>_<eventName>()
	std::string funcName = std::string(ctlName) + "_" + std::string(eventName);
	return call_lua_dialog_function<IMenuControl*>::do_it(
		pDialog,
		funcName.c_str(),
		pControl);
}

bool OnControlEvent(IMenuControl* pControl, const char* eventName, POINT pt) {
	if (!pControl || !eventName)
		return false;

	IMenuDialog* pDialog = Control_GetDialog(pControl);
	if (!pDialog)
		return false;

	// Get Control Name
	const char* ctlName = Control_GetName(pControl);
	if (!ctlName)
		return false;

	// Call Lua <ctlName>_<eventName>()
	std::string funcName = std::string(ctlName) + "_" + std::string(eventName);
	return call_lua_dialog_function<IMenuControl*, int, int>::do_it(
		pDialog,
		funcName.c_str(),
		pControl,
		pt.x,
		pt.y);
}

bool OnDialogEvent(IMenuDialog* pDialog, const char* eventName, POINT pt) {
	if (!pDialog || !eventName)
		return false;

	// Call Lua Dialog_<eventName>()
	std::string funcName = std::string("Dialog_") + std::string(eventName);
	return call_lua_dialog_function<IMenuDialog*, int, int>::do_it(
		pDialog,
		funcName.c_str(),
		pDialog,
		pt.x,
		pt.y);
}

bool OnDialogEvent(IMenuDialog* pDialog, const char* eventName, unsigned int key, BOOL bShiftDown, BOOL bControlDown, BOOL bAltDown) {
	if (!pDialog || !eventName)
		return false;

	// Call Lua Dialog_<eventName>()
	std::string funcName = std::string("Dialog_") + std::string(eventName);
	return call_lua_dialog_function<IMenuDialog*, unsigned int, bool, bool, bool>::do_it(
		pDialog,
		funcName.c_str(),
		pDialog,
		key,
		(bShiftDown == 1),
		(bControlDown == 1),
		(bAltDown == 1));
}

bool OnDialogSelected(IMenuDialog* targetDialog, IMenuDialog* pDialog) {
	if (!targetDialog || !pDialog)
		return false;

	return call_lua_dialog_function<IMenuDialog*>::do_it(
		targetDialog,
		"Dialog_OnSelected",
		pDialog);
}

bool OnDialogMoved(IMenuDialog* targetDialog, IMenuDialog* pDialog, POINT pt) {
	if (!targetDialog || !pDialog)
		return false;

	return call_lua_dialog_function<IMenuDialog*, int, int>::do_it(
		targetDialog,
		"Dialog_OnMoved",
		pDialog,
		pt.x,
		pt.y);
}

bool OnDialogResized(IMenuDialog* targetDialog, IMenuDialog* pDialog, POINT pt) {
	if (!targetDialog || !pDialog)
		return false;

	return call_lua_dialog_function<IMenuDialog*, int, int>::do_it(
		targetDialog,
		"Dialog_OnResized",
		pDialog,
		pt.x,
		pt.y);
}

bool OnControlSelected(IMenuDialog* pDialog, IMenuControl* pControl) {
	if (!pDialog || !pControl)
		return false;

	return call_lua_dialog_function<IMenuControl*>::do_it(
		pDialog,
		"OnControlSelected",
		pControl);
}

bool OnControlMoved(IMenuDialog* pDialog, IMenuControl* pControl, POINT pt) {
	if (!pDialog || !pControl)
		return false;

	return call_lua_dialog_function<IMenuControl*, int, int>::do_it(
		pDialog,
		"OnControlMoved",
		pControl,
		pt.x,
		pt.y);
}

bool OnControlResized(IMenuDialog* pDialog, IMenuControl* pControl, POINT pt) {
	if (!pDialog || !pControl)
		return false;

	return call_lua_dialog_function<IMenuControl*, int, int>::do_it(
		pDialog,
		"OnControlResized",
		pControl,
		pt.x,
		pt.y);
}

bool OnButtonClicked(IMenuButton* pButton) {
	if (!pButton)
		return false;

	IMenuControl* pControl = Button_GetControl(pButton);
	if (!pControl)
		return false;

	IMenuDialog* pDialog = Control_GetDialog(pControl);
	if (!pDialog)
		return false;

	EVENT_ID dialogEventId = KEPMenu::Instance()->GetEngineInterface()->GetDialogEventId();
	IEvent* e = KEPMenu::Instance()->GetEngineInterface()->GetDispatcher()->MakeEvent(dialogEventId);
	e->SetFilter(DIALOG_BUTTON_CLICKED);
	*e->OutBuffer() << pDialog->GetMenuFilePath().c_str() << pControl->GetName();
	KEPMenu::Instance()->GetEngineInterface()->GetDispatcher()->QueueEvent(e);

	// Get Control Name
	const char* ctlName = Control_GetName(pControl);
	if (!ctlName)
		return false;

	// Call Lua <btnName>_OnButtonClicked()
	std::string funcName = std::string(ctlName) + "_OnButtonClicked";
	return call_lua_dialog_function<IMenuButton*>::do_it(
		pDialog,
		funcName.c_str(),
		pButton);
}

bool OnCheckBoxChanged(IMenuCheckBox* pCheckBox) {
	if (!pCheckBox)
		return false;

	IMenuControl* pControl = CheckBox_GetControl(pCheckBox);
	if (!pControl)
		return false;

	IMenuDialog* pDialog = Control_GetDialog(pControl);
	if (!pDialog)
		return false;

	// Get Control Name
	const char* ctlName = Control_GetName(pControl);
	if (!ctlName)
		return false;

	// Call Lua <ctlName>_OnCheckBoxChanged()
	std::string funcName = std::string(ctlName) + "_OnCheckBoxChanged";
	return call_lua_dialog_function<IMenuCheckBox*, BOOL>::do_it(
		pDialog,
		funcName.c_str(),
		pCheckBox,
		CheckBox_GetChecked(pCheckBox));
}

bool OnRadioButtonChanged(IMenuRadioButton* pRadioButton) {
	if (!pRadioButton)
		return false;

	IMenuControl* pControl = RadioButton_GetControl(pRadioButton);
	if (!pControl)
		return false;

	IMenuDialog* pDialog = Control_GetDialog(pControl);
	if (!pDialog)
		return false;

	// Get Control Name
	const char* ctlName = Control_GetName(pControl);
	if (!ctlName)
		return false;

	// Call Lua <ctlName>_OnRadioButtonChanged()
	std::string funcName = std::string(ctlName) + "_OnRadioButtonChanged";
	return call_lua_dialog_function<IMenuRadioButton*>::do_it(
		pDialog,
		funcName.c_str(),
		pRadioButton);
}

bool OnComboBoxSelectionChanged(IMenuComboBox* pComboBox) {
	if (!pComboBox)
		return false;

	IMenuControl* pControl = ComboBox_GetControl(pComboBox);
	IMenuDialog* pDialog = Control_GetDialog(pControl);
	if (!pControl || !pDialog)
		return false;

	// Get Control Name
	const char* ctlName = Control_GetName(pControl);
	if (!ctlName)
		return false;

	// Call Lua <ctlName>_OnComboBoxSelectionChanged()
	std::string funcName = std::string(ctlName) + "_OnComboBoxSelectionChanged";
	return call_lua_dialog_function<IMenuComboBox*, int, const char*>::do_it(
		pDialog,
		funcName.c_str(),
		pComboBox,
		ComboBox_GetSelectedIndex(pComboBox),
		ComboBox_GetSelectedText(pComboBox));
}

bool OnSliderValueChanged(IMenuSlider* pSlider) {
	if (!pSlider)
		return false;

	IMenuControl* pControl = Slider_GetControl(pSlider);
	IMenuDialog* pDialog = Control_GetDialog(pControl);
	if (!pControl || !pDialog)
		return false;

	// Get Control Name
	const char* ctlName = Control_GetName(pControl);
	if (!ctlName)
		return false;

	// Call Lua <ctlName>_OnSliderValueChanged()
	std::string funcName = std::string(ctlName) + "_OnSliderValueChanged";
	return call_lua_dialog_function<IMenuSlider*, int>::do_it(
		pDialog,
		funcName.c_str(),
		pSlider,
		Slider_GetValue(pSlider));
}

bool OnLinkClicked(IMenuControl* pControl) {
	if (!pControl)
		return false;

	IMenuDialog* pDialog = Control_GetDialog(pControl);
	if (!pDialog)
		return false;

	// Get Control Name
	const char* ctlName = Control_GetName(pControl);
	if (!ctlName)
		return false;

	// Call Lua <ctlName>_OnLinkClicked()
	std::string funcName = std::string(ctlName) + "_OnLinkClicked";
	return call_lua_dialog_function<IMenuControl*, const char*>::do_it(
		pDialog,
		funcName.c_str(),
		pControl,
		Control_GetLinkClicked(pControl));
}

bool OnEditBoxString(IMenuEditBox* pEditBox) {
	if (!pEditBox)
		return false;

	IMenuControl* pControl = EditBox_GetControl(pEditBox);
	IMenuDialog* pDialog = Control_GetDialog(pControl);
	if (!pControl || !pDialog)
		return false;

	// Get Control Name
	const char* ctlName = Control_GetName(pControl);
	if (!ctlName)
		return false;

	// Call Lua <ctlName>_OnEditBoxString()
	std::string funcName = std::string(ctlName) + "_OnEditBoxString";
	return call_lua_dialog_function<IMenuEditBox*, const char*>::do_it(
		pDialog,
		funcName.c_str(),
		pEditBox,
		EditBox_GetText(pEditBox));
}

bool OnEditBoxChange(IMenuEditBox* pEditBox) {
	if (!pEditBox)
		return false;

	IMenuControl* pControl = EditBox_GetControl(pEditBox);
	IMenuDialog* pDialog = Control_GetDialog(pControl);
	if (!pControl || !pDialog)
		return false;

	// Get Control Name
	const char* ctlName = Control_GetName(pControl);
	if (!ctlName)
		return false;

	// Call Lua <ctlName>_OnEditBoxChange()
	std::string funcName = std::string(ctlName) + "_OnEditBoxChange";
	return call_lua_dialog_function<IMenuEditBox*, const char*>::do_it(
		pDialog,
		funcName.c_str(),
		pEditBox,
		EditBox_GetText(pEditBox));
}

bool OnEditBoxKeyDown(IMenuEditBox* pEditBox, unsigned int key, BOOL bShiftDown, BOOL bControlDown, BOOL bAltDown) {
	if (!pEditBox)
		return false;

	IMenuControl* pControl = EditBox_GetControl(pEditBox);
	IMenuDialog* pDialog = Control_GetDialog(pControl);
	if (!pControl || !pDialog)
		return false;

	// Get Control Name
	const char* ctlName = Control_GetName(pControl);
	if (!ctlName)
		return false;

	// Call Lua <ctlName>_OnEditBoxKeyDown()
	std::string funcName = std::string(ctlName) + "_OnEditBoxKeyDown";
	return call_lua_dialog_function<IMenuEditBox*, int, bool, bool, bool>::do_it(
		pDialog,
		funcName.c_str(),
		pEditBox,
		key,
		(bShiftDown == 1),
		(bControlDown == 1),
		(bAltDown == 1));
}

bool OnListBoxItemDblClick(IMenuListBox* pListBox) {
	if (!pListBox)
		return false;

	IMenuControl* pControl = ListBox_GetControl(pListBox);
	IMenuDialog* pDialog = Control_GetDialog(pControl);
	if (!pControl || !pDialog)
		return false;

	// Get Control Name
	const char* ctlName = Control_GetName(pControl);
	if (!ctlName)
		return false;

	// Call Lua <ctlName>_OnListBoxItemDblClick()
	std::string funcName = std::string(ctlName) + "_OnListBoxItemDblClick";
	return call_lua_dialog_function<IMenuListBox*, int, const char*>::do_it(
		pDialog,
		funcName.c_str(),
		pListBox,
		ListBox_GetSelectedItemIndex(pListBox),
		ListBox_GetSelectedItemText(pListBox));
}

bool OnListBoxSelection(IMenuListBox* pListBox, POINT pt) {
	if (!pListBox)
		return false;

	IMenuControl* pControl = ListBox_GetControl(pListBox);
	IMenuDialog* pDialog = Control_GetDialog(pControl);
	if (!pControl || !pDialog)
		return false;

	// Get Control Name
	const char* ctlName = Control_GetName(pControl);
	if (!ctlName)
		return false;

	// Call Lua <ctlName>_OnListBoxSelection()
	std::string funcName = std::string(ctlName) + "_OnListBoxSelection";
	return call_lua_dialog_function<IMenuListBox*, int, int, int, const char*>::do_it(
		pDialog,
		funcName.c_str(),
		pListBox,
		pt.x,
		pt.y,
		ListBox_GetSelectedItemIndex(pListBox),
		ListBox_GetSelectedItemText(pListBox));
}

bool OnScrollBarChanged(IMenuScrollBar* pScrollBar) {
	if (!pScrollBar)
		return false;

	IMenuControl* pControl = ScrollBar_GetControl(pScrollBar);
	if (!pControl)
		return false;

	IMenuDialog* pDialog = Control_GetDialog(pControl);
	if (FindSlotByDialog(pDialog) < 0)
		return false;

	IMenuControl* pParentControl = ScrollBar_GetParent(pScrollBar);
	char funcName[_MAX_PATH] = "OnScrollBarChanged";
	if (pParentControl)
		sprintf_s(funcName, _countof(funcName), "%s_OnScrollBarChanged", Control_GetName(pParentControl));

	return call_lua_dialog_function<IMenuScrollBar*, IMenuControl*, int, int>::do_it(
		pDialog,
		funcName,
		pScrollBar,
		pParentControl,
		ScrollBar_GetPageSize(pScrollBar),
		ScrollBar_GetTrackPos(pScrollBar));
}

} // namespace Handlers

} // namespace KEP