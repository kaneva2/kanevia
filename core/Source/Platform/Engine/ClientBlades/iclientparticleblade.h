///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "iclientrenderblade.h"

struct EffectDef;

namespace KEP {

typedef int EffectHandle;
const EffectHandle INVALID_EFFECT_HANDLE = -1;

class IWorldObject;

struct ParticleRenderOptions {
	bool m_enabled = true;
	double m_fps = 0.0;
	int m_bias = 5;
	bool m_biasAuto = true;
	int m_DOCullBias = 5;
	bool m_DOCullBiasAuto = true;
};

class IClientParticleBlade : public IClientRenderBlade {
public:
	virtual bool InitializeParticleBlade(const char* filename) = 0;

	ParticleRenderOptions m_renderOptions;
	void SetRenderOptions(const ParticleRenderOptions& renderOptions) {
		m_renderOptions = renderOptions;
	}

	virtual size_t GetNumEffects() const = 0;
	virtual const char* GetEffectName(size_t idx) const = 0;

	virtual EffectHandle CreateEffect(const char* effect_name,
		float x, float y, float z, float qx, float qy, float qz, float qw) = 0;

	virtual void RemoveEffect(EffectHandle handle) = 0;
	virtual void ClearRuntimeEffects() = 0;
	virtual void RemoveAllEffects() = 0;
	virtual void FreeTextures() = 0;

	virtual void SetEffectPosition(EffectHandle effect_handle, float x, float y, float z) = 0;
	virtual void SetEffectOrientation(EffectHandle effect_handle, float qx, float qy, float qz, float qw) = 0;
	virtual void SetEffectWaitingToDie(EffectHandle effect_handle, bool waitingToDie) = 0;

	virtual const char* GetProviderDescription() = 0;
	virtual const char* GetDefinitionExtension() = 0;

	virtual bool LoadParticleDefinitions(const char* filename) = 0;

	// Import/Export function
	virtual bool LoadKEPDefinitionsFromBuffer(const char* effect_name, EffectDef* pDef) = 0;

	virtual bool Destroy() = 0;
};

} // namespace KEP
