///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {
public:
virtual void GetPosition(float* x, float* y, float* z) = 0;
virtual void GetOrientation(float* qx, float* qy, float* qz, float* qw) = 0;

protected:
}; // namespace KEP
