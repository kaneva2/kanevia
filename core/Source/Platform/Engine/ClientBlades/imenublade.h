#pragma once

#include "KEPConstants.h"

#include "iclientrenderblade.h"

#include <d3d9.h>
#include <d3dx9.h>

namespace KEP {

class IMenuDialog;

enum class MenuRenderOption {
	PASS_THROUGH = 0,
	USE_DRAW_QUEUE = 1
};

struct MenuRenderOptions {
	MenuRenderOption m_renderOption = MenuRenderOption::USE_DRAW_QUEUE;
	TimeMs m_onRenderTimeMs = 0.0;
};

class IMenuBlade : public IClientRenderBlade {
public:
	virtual ~IMenuBlade() {}

	virtual void SetTestGroup(int testGroup) = 0;

	MenuRenderOptions m_renderOptions;
	MenuRenderOptions GetRenderOptions() const {
		return m_renderOptions;
	}
	void SetRenderOptions(const MenuRenderOptions& renderOptions) {
		m_renderOptions = renderOptions;
	}
	bool IsRenderOptionSet(const MenuRenderOption& renderOption) const {
		return ((int)m_renderOptions.m_renderOption & (int)renderOption) != 0;
	}

	virtual void ReInit(bool isChild, bool resetMenuPaths, bool clearTextureList) = 0;

	virtual size_t DialogsOpen() const = 0;

	virtual bool LogAllDialogs(bool verbose = true) = 0;

	virtual void AddMenuPaths(const std::string& pathMenu, const std::string& pathScript, bool trustedPaths, bool addTextures) = 0;

	virtual const char* DialogMenuName(IMenuDialog* pDialog) = 0;
	virtual const char* DialogScriptName(IMenuDialog* pDialog) = 0;

	virtual int GetDialogID(const std::string& menuName) = 0;
	virtual IMenuDialog* GetDialog(const std::string& menuName) = 0;

	virtual bool MenuIsOpen(const std::string& menuName) {
		return (GetDialog(menuName) != NULL);
	}

	virtual IMenuDialog* DialogNew() = 0;

	virtual IMenuDialog* DialogCreateAs(const std::string& menuName, const std::string& menuId, bool editNow = false, bool trustedOnly = false) = 0;
	virtual bool MenuOpenAs(const std::string& menuName, const std::string& menuId, bool editNow = false, bool trustedOnly = false) {
		return (DialogCreateAs(menuName, menuId, editNow, trustedOnly) != NULL);
	}

	virtual IMenuDialog* DialogCreate(const std::string& menuName, bool editNow = false, bool trustedOnly = false) = 0;
	virtual bool MenuOpen(const std::string& menuName, bool editNow = false, bool trustedOnly = false) {
		return (DialogCreate(menuName, editNow, trustedOnly) != NULL);
	}

	virtual bool DialogDestroy(IMenuDialog* pDialog) = 0;
	virtual bool MenuClose(const std::string& menuName) {
		return DialogDestroy(GetDialog(menuName));
	}

	virtual bool DialogDestroyAll() {
		return DialogDestroyAllExcept(NULL);
	}
	virtual bool MenuCloseAll() {
		return DialogDestroyAll();
	}

	virtual bool DialogDestroyAllExcept(IMenuDialog* pDialog = NULL) = 0;
	virtual bool MenuCloseAllExcept(const std::string& menuName) {
		return DialogDestroyAllExcept(GetDialog(menuName));
	}

	virtual bool DialogDestroyAllNonEssentialExcept(IMenuDialog* pDialog = NULL, BOOL includeFramework = false) = 0;
	virtual bool MenuCloseAllNonEssentialExcept(const std::string& menuName, BOOL includeFramework = false) {
		return DialogDestroyAllNonEssentialExcept(GetDialog(menuName), includeFramework);
	}

	virtual bool DialogExitAll() = 0;
	virtual bool MenuExitAll() {
		return DialogExitAll();
	}

	virtual bool DialogSave(IMenuDialog* pDialog, const std::string& fileName) = 0;
	virtual bool MenuSave(const std::string& menuName, const std::string& fileName) {
		return DialogSave(GetDialog(menuName), fileName);
	}

	virtual bool DialogCopyTemplate(IMenuDialog* pDialog) = 0;
	virtual bool MenuCopyTemplate(const std::string& menuName) {
		return DialogCopyTemplate(GetDialog(menuName));
	}

	virtual bool ApplyInputSettings() = 0;
	virtual bool MenuApplyInputSettings(const std::string& /*menuName*/) {
		return ApplyInputSettings();
	}

	virtual bool DialogSendToBack(IMenuDialog* pDialog) = 0;
	virtual bool MenuSendToBack(const std::string& menuName) {
		return DialogSendToBack(GetDialog(menuName));
	}

	virtual bool DialogBringToFrontEnable(IMenuDialog* pDialog, bool enable) = 0;
	virtual bool MenuBringToFrontEnable(const std::string& menuName, bool enable) {
		return DialogBringToFrontEnable(GetDialog(menuName), enable);
	}

	virtual bool DialogBringToFront(IMenuDialog* pDialog) = 0;
	virtual bool MenuBringToFront(const std::string& menuName) {
		return DialogBringToFront(GetDialog(menuName));
	}

	virtual bool DialogAlwaysBringToFront(IMenuDialog* pDialog, bool enable) = 0;
	virtual bool MenuAlwaysBringToFront(const std::string& menuName) {
		return DialogAlwaysBringToFront(GetDialog(menuName), true);
	}

	virtual bool AlwaysBringDialogToFrontDisable() {
		return DialogAlwaysBringToFront(NULL, false);
	}
	virtual bool MenuAlwaysBringToFrontDisable() {
		return DialogAlwaysBringToFront(NULL, false);
	}

	virtual void DialogsSetEdit(bool bEdit) = 0;

	virtual std::string PathBase() const = 0;

	virtual bool ResetCursorModesTypes() = 0;
	virtual bool SetCursorMode(const CURSOR_MODE& cursorMode) = 0;
	virtual bool SetCursorModeType(const CURSOR_MODE& cursorMode, const CURSOR_TYPE& cursorType) = 0;

	virtual bool IsModalDialogOpen() = 0;

	virtual bool IsDialogFrontmostMouseEnabled() = 0;
	virtual bool IsDialogFrontmostKeyboardEnabled() = 0;

	// DRF - ED-7035 - Investigate texture memory impact
	virtual void ReleaseTextureNode(IDirect3DTexture9* pTex) = 0;
};

} // namespace KEP
