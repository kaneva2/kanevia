///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "wkgtypes.h"
#include "iasset.h"
#include "iclientassetblade.h"
#include "assetfactory.h"

namespace KEP {

AssetFactory::AssetFactory() {
}

AssetFactory::~AssetFactory() {
	AssetTypeMap::iterator itor = m_assets.begin();
	while (itor != m_assets.end()) {
		delete &(*itor);
		itor++;
	}
}

IAsset*
AssetFactory::GetAsset(const std::string& name, const type_info& ti) {
	IAsset* found = 0;

	// see if we have any assets of this type and name
	AssetTypeMap::iterator itor = m_assets.find(ti.name());
	if (itor != m_assets.end()) {
		AssetMap::iterator asset_itor = itor->second.find(name);
		if (asset_itor != itor->second.end()) {
			found = asset_itor->second;
		}
	}

	if (0 == found) {
		// couldn't find it, so create it
		AssetCreatorMap::iterator creator_itor = m_asset_creators.find(ti.name());
		if (creator_itor != m_asset_creators.end()) {
			// construct this asset
			IAsset* a = (*creator_itor->second)(name);
			return a;
		} else {
			// don't know how to construt this asset
			return 0;
		}
	}

	return found;
}

} // namespace KEP
