///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "wkgtypes.h"
#include "PSEmitter.h"
#include "PSParticle.h"
#include "PSEffect.h"
#include "PSUtils.h"
#include "FileStream.h"
#include "Units.h"
#include "Device.h"

#include "Common\KEPUtil\Helpers.h"
#include <Core\Math\Random.h>
#include <Core\Math\TransformUtil.h>
#include <Core\Math\Units.h>

static LogInstance("Instance");

using namespace KEP;

namespace wkg {

ParticleSystem::EmitterType::EmitterType() :
		baseLifetime(10000),
		randExtraLifetime(0),
		bLoopCycle(false),
		bEmitLight(false),
		lightOffset(0.0f, 0.0f, 0.0f),
		shapeType(EMITTER_CUBE) {
	memset(&shapeData, 0, sizeof(shapeData));
	//shapeData.square.m_min.Fill(0);
	//shapeData.square.m_max.Fill(0);
}

ParticleSystem::EmitterType::~EmitterType() {
}

bool ParticleSystem::EmitterType::Read(FileStream& f, uint32 /*fileVersion*/) {
	uint8 uint8Val;
	float floatVal; // don't change this to real

	uint32 baseLifetime32 = 0;
	if (!f.Read(&(baseLifetime32)))
		goto fileError;
	baseLifetime = baseLifetime32;
	if (!f.Read(&(randExtraLifetime)))
		goto fileError;
	READBOOL(bLoopCycle);

	if (!emitFunc.Read(f))
		goto fileError;
	if (!xVelFunc.Read(f))
		goto fileError;
	if (!yVelFunc.Read(f))
		goto fileError;
	if (!zVelFunc.Read(f))
		goto fileError;
	if (!lifeFunc.Read(f))
		goto fileError;
	if (!sizeMultFunc.Read(f))
		goto fileError;
	if (!rotMultFunc.Read(f))
		goto fileError;

	READBOOL(bEmitLight);
	if (bEmitLight) {
		if (!lightRFunc.Read(f))
			goto fileError;
		if (!lightGFunc.Read(f))
			goto fileError;
		if (!lightBFunc.Read(f))
			goto fileError;
		if (!lightFalloffFunc.Read(f))
			goto fileError;
		READREAL(lightOffset.x);
		READREAL(lightOffset.y);
		READREAL(lightOffset.z);
	}

	if (!f.Read(&uint8Val))
		goto fileError;

	if (0 == uint8Val) {
		shapeType = EMITTER_CUBE;
		READREAL(shapeData.square.m_min.X());
		READREAL(shapeData.square.m_max.X());
		READREAL(shapeData.square.m_min.Y());
		READREAL(shapeData.square.m_max.Y());
		READREAL(shapeData.square.m_min.Z());
		READREAL(shapeData.square.m_max.Z());
	} else if (1 == uint8Val) {
		shapeType = EMITTER_SPHERE;
		shapeData.sphere.m_center.Set(0, 0, 0);
		READREAL(shapeData.sphere.hotspot);
		READREAL(shapeData.sphere.falloff);
	} else if (2 == uint8Val) { // DRF - Added Sphere With Center
		shapeType = EMITTER_SPHERE;
		READREAL(shapeData.sphere.m_center.X());
		READREAL(shapeData.sphere.m_center.Y());
		READREAL(shapeData.sphere.m_center.Z());
		READREAL(shapeData.sphere.hotspot);
		READREAL(shapeData.sphere.falloff);
	} else {
		goto fileError; // undefined particle emitter shape type
	}

	return true;

fileError:
	ASSERT(!"Error reading file in ParticleSystem::EmitterType::Read()");
	return false;
}

ParticleSystem::Emitter* ParticleSystem::EmitterType::CreateEmitter(Effect* pEffect, EmitterType* pEmitterType, ParticleType* pParticleType, const Vector3f& offset) {
	return new Emitter(pEffect, pEmitterType, pParticleType, offset);
}

ParticleSystem::Emitter::Emitter(Effect* pEff, EmitterType* pET, ParticleType* pPT, const Vector3f& Offset) :
		m_pEffect(pEff),
		m_pEmitterType(pET),
		m_pParticleType(pPT),
		m_offset(Offset) {
	m_waitingToDie = false;
	m_lifetimeMs = 0;
	m_lifespanMs = m_pEmitterType->baseLifetime;
	if (m_pEmitterType->randExtraLifetime)
		m_lifespanMs += RandomFloat() * m_pEmitterType->randExtraLifetime;
	m_reciprocalLifeSpanMs = m_lifespanMs <= 0 ? 0 : 1.0f / m_lifespanMs;
	m_fFractionalParticles = 0;

	ConvertRotation(m_pEffect->GetSysOrient(), out(m_orientation));
	m_OrientedOffset = TransformVector(m_orientation, Vector4f(m_offset, 1.0f));
}

ParticleSystem::Emitter::~Emitter() {
#if 0
	for (const auto& pParticle : m_particles)
		if (pParticle)
			delete pParticle;
#else
#endif
	m_particles.clear();
}

KEP::Vector3f ParticleSystem::Emitter::Orient(const KEP::Vector3f& orig) const {
	KEP::Vector3f vec;
	vec = KEP::TransformVector(m_orientation, orig);
	return vec;
}

void ParticleSystem::Emitter::CreateParticle(real EmitterSampleTime) {
	Particle particle;
	Particle* p = &particle;
	p->SetEmitter(this);

	p->SetLifeTimeMs(0);

	p->SetLifeSpanMs((uint32)(m_pEmitterType->lifeFunc.Sample(EmitterSampleTime, false, false) * 1000)); // use value in milliseconds;

	p->SetSizeMult(m_pEmitterType->sizeMultFunc.Sample(EmitterSampleTime));

	// re-orient the particle relative to the emitter's current orientation
	Vector3f vel(
		m_pEmitterType->xVelFunc.Sample(EmitterSampleTime),
		m_pEmitterType->yVelFunc.Sample(EmitterSampleTime),
		m_pEmitterType->zVelFunc.Sample(EmitterSampleTime));

	// Particle effects in dance game shoot out to the side instead of upward if we call Orient()
	if (m_pEffect->InObjectSpace())
		p->SetVel(Orient(vel));
	else
		p->SetVel(vel);

	// if using sprite list, set texture index for particle
	bool bNegRotDir = false;
	bool bMirrorX = false;
	bool bMirrorY = false;

	// 255 texture index means invalid texture, since 0-254 index the texture* array
	uint8_t iStartBitmap = 255;
	if (m_pParticleType->m_papTextures && m_pParticleType->m_nTextures > 0) {
		if (m_pParticleType->bRandomStartFrame)
			iStartBitmap = (uint8)(Random32() % m_pParticleType->m_nTextures);
		else
			iStartBitmap = 0;

		uint32_t r32 = Random32();
		if (m_pParticleType->bRandomXMirror)
			bMirrorX = (r32 & 0x80000000) != 0;
		if (m_pParticleType->bRandomYMirror)
			bMirrorY = (r32 & 0x40000000) != 0;
		if (m_pParticleType->bRandomRotDir)
			bNegRotDir = (r32 & 0x20000000) != 0;
	}
	p->InitBitmap(iStartBitmap, bMirrorX, bMirrorY);

	// Rotation
	p->SetRotStartDegrees(CalcRandFloat(m_pParticleType->startRotMin, m_pParticleType->startRotMax));
	float fRotMult = m_pEmitterType->rotMultFunc.Sample(EmitterSampleTime);
	if (bNegRotDir)
		fRotMult = -fRotMult;
	p->SetRotMult(fRotMult);

	// determine particle position based on emitter shape
	Vector3f pos;
	switch (m_pEmitterType->shapeType) {
		case EmitterType::EMITTER_CUBE: {
			Simd4f mn(m_pEmitterType->shapeData.square.m_min);
			Simd4f mx(m_pEmitterType->shapeData.square.m_max);
			Simd4f pos_simd = MetersToFeet(mn + RandomFloatSimd() * (mx - mn));

			// The result of the Orient() call used to be discarded.
			// Fixing the problem puts the "Perfect" particle for the dance game in the wrong location.
			// We will continue to ignore
			// align emitter shape to the emitter's orientation (esp important if the "cube" is really a line)
			if (m_pEffect->InObjectSpace())
				pos = Orient(Vector4f(pos_simd).SubVector<3>());
			else
				pos = pos_simd.AsVector3f();
			break;
		}

		case EmitterType::EMITTER_SPHERE: {
			// todo: this is not correct...just picks a point within the hotspot sphere
			//		 ...falloff is not used
			//		 ...this probably shouldn't use spherical coords, either (expensive)
			real rho = MetersToFeet(m_pEmitterType->shapeData.sphere.hotspot * RandomFloat());
			float phiCycles = RandomFloat() * 0.5f;
			float thetaCycles = RandomFloat();

			Vector3f center = MetersToFeet(m_pEmitterType->shapeData.sphere.m_center);

			float sinPhi, cosPhi;
			float sinTheta, cosTheta;
			SinCosCycles_Approx(phiCycles, out(sinPhi), out(cosPhi));
			SinCosCycles_Approx(thetaCycles, out(sinTheta), out(cosTheta));

			pos.x = rho * sinPhi * cosTheta + center.X();
			pos.y = rho * sinPhi * sinTheta + center.Y();
			pos.z = rho * cosPhi + center.Z();

			// don't need to rotate spherical emitter
			break;
		}

		default:
			return;
	}

	if (!m_pEffect->InObjectSpace()) {
		pos += m_pEffect->GetSysPos();
		pos += m_offset;
	}

	p->InitPos(pos);

	m_particles.push_back(particle);
}

bool ParticleSystem::Emitter::GenerateParticles(float elapsedMs) {
	if (m_waitingToDie)
		return !m_particles.empty();

	real sampleTime = m_lifetimeMs * m_reciprocalLifeSpanMs;

	// calculate number of particles to emit since last frame & store integer remainder
	real emissionRate = m_pEmitterType->emitFunc.Sample(sampleTime);
	if (emissionRate < 0)
		emissionRate = 0;
	float fElapsedSec = float(elapsedMs) * .001f;
	float fToEmit = emissionRate * fElapsedSec + m_fFractionalParticles;
	float fToEmitFloor = floor(fToEmit);
	uint32 iToEmit = NearestInt(fToEmitFloor);
	m_fFractionalParticles = fToEmit - fToEmitFloor;

	for (uint32 i = 0; i < iToEmit; i++)
		CreateParticle(sampleTime);

	// check for emitter death
	if (m_lifespanMs > 0 && m_lifetimeMs >= m_lifespanMs) {
		if (m_pEmitterType->bLoopCycle) {
			m_lifetimeMs = 0;
			m_lifespanMs = m_pEmitterType->baseLifetime;
			if (m_pEmitterType->randExtraLifetime)
				m_lifespanMs += RandomFloat() * m_pEmitterType->randExtraLifetime;
		} else {
			m_waitingToDie = true;
		}
	} else {
		m_lifetimeMs += elapsedMs; // update here to give emitter a chance to use 0 lifetime values (otherwise it may miss all of the particles that it should have emitted)
	}

	return true;
}

void ParticleSystem::Emitter::UpdateParticles(float elapsedMs) {
#if 1
	int32_t iElapsedMs = elapsedMs;
	ParticleList::iterator itr = m_particles.begin();
	size_t nParticles = 0;
	Particle* apParticles[4];
	bool bAllParticlesStillAlive = true;
	while (itr != m_particles.end()) {
		bool bStillAlive = itr->StillAlive();
		bAllParticlesStillAlive &= bStillAlive;
		apParticles[nParticles] = &*itr;
		nParticles += bStillAlive;
		if (nParticles == 4) {
			Particle::Update4Alive(this->m_pParticleType, iElapsedMs, apParticles);
			nParticles = 0;
		}
		++itr;
	}

	// Handle remaining non multiple of four particles
	if (nParticles > 0) {
		if (nParticles == 1) {
			// One particle remaining
			apParticles[0]->Update(this->m_pParticleType, iElapsedMs);
		} else {
			// More than one particle remaining.
			// We will redundantly update particle zero multiple times, which should be harmless.
			for (size_t i = nParticles; i < 4; ++i)
				apParticles[i] = apParticles[0];
			Particle::Update4Alive(this->m_pParticleType, iElapsedMs, apParticles);
		}
	}

	// Remove dead particles.
	if (!bAllParticlesStillAlive) {
		itr = m_particles.begin();
		while (itr != m_particles.end()) {
			if (!itr->StillAlive())
				break;
			++itr;
		}

		ParticleList::iterator itrEnd = m_particles.end();
		while (itr != itrEnd) {
			if (itr->StillAlive()) {
				++itr;
			} else {
				--itrEnd;
				*itr = *itrEnd;
			}
		}
		m_particles.erase(itrEnd, m_particles.end());
	}
#elif 1
	ParticleList::iterator itr = m_particles.begin();
	bool bAllParticlesStillAlive = true;
	while (itr != m_particles.end()) {
		bAllParticlesStillAlive &= itr->Update(this->m_pParticleType, elapsedMs);
		++itr;
	}

	// Remove dead particles.
	if (!bAllParticlesStillAlive) {
		itr = m_particles.begin();
		while (itr != m_particles.end()) {
			if (!itr->StillAlive())
				break;
			++itr;
		}

		ParticleList::iterator itrEnd = m_particles.end();
		while (itr != itrEnd) {
			if (itr->StillAlive()) {
				++itr;
			} else {
				--itrEnd;
				*itr = *itrEnd;
			}
		}
		m_particles.erase(itrEnd, m_particles.end());
	}
#elif 1
	ParticleList::iterator itr = m_particles.begin();
	while (itr != m_particles.end()) {
		if (itr->Update(this->m_pParticleType, elapsedMs)) {
			++itr;
		} else {
			*itr = m_particles.back();
			m_particles.pop_back();
		}
	}
#else
	ParticleList::iterator iParticle = m_particles.begin();
	while (iParticle != m_particles.end()) {
		Particle& particle = *iParticle;
		++iParticle;
		if (!particle.Update(this->m_pParticleType, elapsedMs))
			break;
	}

	if (iParticle != m_particles.end()) {
		ParticleList::iterator iWriteParticle = iParticle;
		while (iParticle != m_particles.end()) {
			Particle& particle = *iParticle;
			++iParticle;
			if (particle.Update(this->m_pParticleType, elapsedMs)) {
				*iWriteParticle = particle;
				++iWriteParticle;
			}
		}

		m_particles.erase(iWriteParticle, m_particles.end());
	}
#endif
}

} // namespace wkg