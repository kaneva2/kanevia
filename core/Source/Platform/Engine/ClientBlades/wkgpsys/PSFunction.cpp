///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "wkgtypes.h"
#include "PSfunction.h"
#include "FileStream.h"
#include "LogHelper.h"
static LogInstance("Instance");

namespace wkg {

ParticleFunction::FPIterator ParticleFunction::AddFunctionPointToGraph(FunctionPointVector& graph, float time, float value) {
	FunctionPoint p{ time, value };
	auto itr = graph.begin();
	while (itr < graph.end() && time > itr->time)
		++itr;

	itr = graph.insert(itr, p);
	return itr;
}

void ParticleFunction::LogFunctionPoints(bool bRandom) {
	const auto& graph = bRandom ? m_minBoundGraph : m_graph;
	if (bRandom)
		LogInfo("Random function points:" << m_minBoundGraph.size());
	else
		LogInfo("Function points:" << m_graph.size());
	for (const FunctionPoint& fp : graph) {
		LogInfo(fp.time << ", " << fp.value);
	}
}

bool ParticleFunction::Read(FileStream& f) {
	uint8 numFunctionPoints;
	real time, value;
	uint8 usingRandomization;

	if (!f.Read(&numFunctionPoints))
		return false;

	for (uint8 i = 0; i < numFunctionPoints; ++i) {
		if (!f.Read(&time))
			return false;

		if (!f.Read(&value))
			return false;

		AddFunctionPoint(time, value);
	}
	//LogFunctionPoints();

	if (!f.Read(&usingRandomization))
		return false;

	SetUseRandomization(0 != usingRandomization);

	if (0 != usingRandomization) {
		if (!f.Read(&numFunctionPoints))
			return false;

		for (uint8 i = 0; i < numFunctionPoints; ++i) {
			if (!f.Read(&time))
				return false;

			if (!f.Read(&value))
				return false;

			AddRandomFunctionPoint(time, value);
		}
	}
	//LogFunctionPoints(true);

	this->Finalize();

	return true;
}

} // namespace wkg