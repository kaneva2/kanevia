///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ParticleSystem.h"
#include "PSFunction.h"
#include <Core/Util/Memory.h>
#include <Core/Math/Vector.h>

#include "PSEffect.h"
#include "PSEmitter.h"
namespace KEP {
}

namespace wkg {

class FileStream;
class Texture;

class ParticleSystem::ParticleType : public KEP::AlignedOperatorNewBase<16> {
public:
	ParticleType();
	~ParticleType();

	bool ReserveTextures(int32 numToReserve);
	void LoadTexture(int32 index, char* szName);
	void ClearTextures();
	void CreateFunctionTables();

	bool Read(FileStream&, uint32);

	static void TextureRelease(Texture* pTexture);

public:
	enum MultiplierType {
		NONE = 0,
		LINEAR,
		QUADRATIC
	};

	struct TextureReleaser {
		void operator()(Texture* pTexture) const { TextureRelease(pTexture); }
	};

public:
	ParticleFunction rFunc, gFunc, bFunc, aFunc; // 0-255 each
	ParticleFunction sizeFunc; // meters
	ParticleFunction rotFunc; // degrees
	ParticleFunctionTable<KEP::Simd4f> m_rgbaFunctionTable;
	ParticleFunctionTable<float> m_sizeFunctionTable;
	ParticleFunctionTable<float> m_rotFunctionTable;
	uint32 msPerFrame;
	float m_fTexturesPerMS; // Reciprocal of msPerFrame
	std::unique_ptr<std::unique_ptr<Texture, TextureReleaser>[]> m_papTextures;
	float m_fReciprocalNumTextures; // Reciprocal of m_nTextures
	uint8 m_nTextures;
	bool bLooping;
	bool bRandomStartFrame;
	bool bRandomRotDir;
	bool bRandomXMirror;
	bool bRandomYMirror;
	bool bMotionBlur;
	bool bEnableLighting;
	bool bAdditiveAlpha; // instead of modulating
	bool m_bConstantVelocity; // gravityCoef == 0 && resistanceCoef == 0
	MultiplierType velResistance;
	MultiplierType sizeResistance;
	// Blur isn't used, so disable variables.
	//uint8 numBlurSteps, blurAlphaFadeTo;
	//real blurTrailLength;
	real gravityCoef;
	real resistanceCoef; // unitless. Stored as a positive value, but negated when read in.
	real startRotMin;
	real startRotMax; // degrees
};

// Align to a cache line.
class ParticleSystem::Particle : public KEP::AlignedOperatorNewBase<64> {
public:
	bool Update(ParticleType* pParticleType, const int32 msElapsed);
	static void Update4Alive(ParticleType* pParticleType, const int32 msElapsed, Particle* aParticles[4]); // Equivalent to calling Update on 4 particles that are alive.

	void SetEmitter(Emitter* pEmitter) {
		m_pEmitter = pEmitter;
	}
	Emitter* GetEmitter() const { return m_pEmitter; }

	__forceinline void InitPos(const KEP::Vector3f& pos) {
		m_Pos.Set(pos.x, pos.y, pos.z, 0);
	}

	void SetLifeSpanMs(uint32_t lifespanMs) {
		if (lifespanMs != 0) {
			m_iLifeSpan = lifespanMs;
			m_fReciprocalLifeSpan = 1.0f / lifespanMs;
		} else {
			m_iLifeSpan = (std::numeric_limits<decltype(m_iLifeSpan)>::max)();
			m_fReciprocalLifeSpan = 0;
		}
	}
	void SetLifeTimeMs(uint32_t lifeTime) { m_iLifeTime = lifeTime; }
	uint32_t GetLifeTimeMs() const { return m_iLifeTime; }
	float CalcSampleTime() const { return static_cast<int32_t>(m_iLifeTime) * m_fReciprocalLifeSpan; }
	bool StillAlive() const { return m_iLifeTime < m_iLifeSpan; }

	void SetVel(const KEP::Vector3f& vel) {
		m_Vel = _mm_setr_ps(vel.x, vel.y, vel.z, 0);
	}
	const KEP::Vector3f& GetVel() const {
		return reinterpret_cast<const KEP::Vector3f&>(m_Vel);
	}

	uint8_t GetMirrorX() const { return m_MirrorFlags & 1; }
	uint8_t GetMirrorY() const { return m_MirrorFlags >> 1; }
	//bool GetMirrorX() const { return m_bMirrorX; }
	//bool GetMirrorY() const { return m_bMirrorY; }

	void InitBitmap(uint8_t iStartBitmapIndex, bool bMirrorX, bool bMirrorY) {
		//m_bMirrorX = bMirrorX;
		//m_bMirrorY = bMirrorY;
		m_MirrorFlags = uint8_t(bMirrorX) | (uint8_t(bMirrorY) << 1);
		m_bmpIndex = iStartBitmapIndex;
		m_bmpIndexStart = iStartBitmapIndex;
		m_msSinceFrameChange = 0;
	}
	uint8_t GetCurrentBitmapIndex() const { return m_bmpIndex; }

	void SetRotMult(float fRotMult) { m_fRotMult = fRotMult; } // This should include bNegRotDir.
	float GetRotMult() const { return m_fRotMult; }
	//void SetRotStart(float fRotStart) { m_fRotStart = fRotStart; }
	//float GetRotStart() const { return m_fRotStart; }
	void SetRotStartDegrees(float fRotStart) { m_iRotStart = floor(fRotStart * (256.0f / 360.0f)); }
	float GetRotStartDegrees() const { return m_iRotStart * (360.0f / 256.0f); }
	void SetSizeMult(float fSizeMult) { m_fSizeMult = fSizeMult; }
	float GetSizeMult() const { return m_fSizeMult; }

	__m128 GetPositionSSE() const { return m_Pos; }

private:
	const KEP::Vector3f& GetPos() const {
		return reinterpret_cast<const KEP::Vector3f&>(m_Pos);
	}
	__forceinline void SetPos(const KEP::Simd4f& pos) {
		m_Pos = pos;
		m_pEmitter->UpdateBounds(pos);
	}

	// Slower than UpdateTextureIndex, but does not use m_iLifeSpan, allowing for less memory usage.
	__forceinline void UpdateTextureIndex_NoLifeSpan(ParticleType* pParticleType) {
		if (m_bmpIndex >= 255)
			return; // No texture

		// Casting m_iLifeTime to signed int for faster conversion to floating point. Overflow should never occur.
		double dTextureCyclesPerMS;
		if (pParticleType->bLooping)
			dTextureCyclesPerMS = pParticleType->m_fTexturesPerMS * pParticleType->m_fReciprocalNumTextures;
		else
			dTextureCyclesPerMS = m_fReciprocalLifeSpan;
		double fNumCycles = m_bmpIndexStart + int32_t(m_iLifeTime) * dTextureCyclesPerMS;
		double fCompleteCycles = floor(fNumCycles);
		double fPartialCycles = fNumCycles - fCompleteCycles;
		m_bmpIndex = KEP::NearestInt(floor(fPartialCycles * pParticleType->m_nTextures));
	}

	__forceinline void UpdateTextureIndex(ParticleType* pParticleType, uint32_t msElapsed) {
		//if( m_bmpIndex >= 255 )
		//	return; // No texture
		if (pParticleType->m_nTextures < 1)
			return;

		m_msSinceFrameChange += msElapsed;

		if (pParticleType->bLooping) {
			while (m_msSinceFrameChange >= pParticleType->msPerFrame) {
				++m_bmpIndex;
				m_msSinceFrameChange -= pParticleType->msPerFrame;
				if (m_bmpIndex == pParticleType->m_nTextures)
					m_bmpIndex = 0;
			}
		} else {
			// if not looping, spread image animation over particles lifetime
			if (m_msSinceFrameChange * pParticleType->m_nTextures >= m_iLifeSpan) {
				// check if cycle not finished
				uint8_t iNextBmpIndex = m_bmpIndex + 1;
				if (iNextBmpIndex == pParticleType->m_nTextures)
					iNextBmpIndex = 0;
				if (iNextBmpIndex != m_bmpIndexStart)
					m_bmpIndex = iNextBmpIndex;
				m_msSinceFrameChange = 0;
			}
		}
	}

	__forceinline void UpdateTextureIndex2(ParticleType* pParticleType, uint32_t msElapsed) {
		if (m_bmpIndex >= 255)
			return; // No texture

		m_msSinceFrameChange += msElapsed;

		if (pParticleType->bLooping) {
			while (m_msSinceFrameChange >= pParticleType->msPerFrame) {
				++m_bmpIndex;
				m_msSinceFrameChange -= pParticleType->msPerFrame;
				if (m_bmpIndex == pParticleType->m_nTextures)
					m_bmpIndex = 0;
			}
		} else {
			// if not looping, spread image animation over particles lifetime
			if (m_msSinceFrameChange * pParticleType->m_nTextures >= m_iLifeSpan) {
				// check if cycle not finished
				uint8_t iNextBmpIndex = m_bmpIndex + 1;
				if (iNextBmpIndex == pParticleType->m_nTextures)
					iNextBmpIndex = 0;
				if (iNextBmpIndex != m_bmpIndexStart)
					m_bmpIndex = iNextBmpIndex;
				m_msSinceFrameChange = 0;
			}
		}
	}

	// Fastest version. B is almost as fast and would probably be faster if we could avoid packing/unpacking SIMD vectors
	// This could be done by storing 4 particles per struct instead of one.
	__forceinline static void UpdateTextureIndex4_A(ParticleType* pParticleType, uint32_t msElapsed, Particle* aParticles[4]) {
		if (pParticleType->m_nTextures <= 1)
			return;

		if (pParticleType->bLooping) {
			for (size_t i = 0; i < 4; ++i) {
				aParticles[i]->m_msSinceFrameChange += msElapsed;

				while (aParticles[i]->m_msSinceFrameChange >= pParticleType->msPerFrame) {
					++aParticles[i]->m_bmpIndex;
					aParticles[i]->m_msSinceFrameChange -= pParticleType->msPerFrame;
					if (aParticles[i]->m_bmpIndex == pParticleType->m_nTextures)
						aParticles[i]->m_bmpIndex = 0;
				}
			}
		} else {
			// if not looping, spread image animation over particles lifetime
			for (size_t i = 0; i < 4; ++i) {
				aParticles[i]->m_msSinceFrameChange += msElapsed;

				if (aParticles[i]->m_msSinceFrameChange * pParticleType->m_nTextures >= aParticles[i]->m_iLifeSpan) {
					// check if cycle not finished
					uint8_t iNextBmpIndex = aParticles[i]->m_bmpIndex + 1;
					if (iNextBmpIndex == pParticleType->m_nTextures)
						iNextBmpIndex = 0;
					if (iNextBmpIndex != aParticles[i]->m_bmpIndexStart)
						aParticles[i]->m_bmpIndex = iNextBmpIndex;
					aParticles[i]->m_msSinceFrameChange = 0;
				}
			}
		}
	}
	__forceinline static void UpdateTextureIndex4_B(ParticleType* pParticleType, uint32_t msElapsed, Particle* aParticles[4]) {
		uint8_t nTextures = pParticleType->m_nTextures;
		if (nTextures <= 1)
			return;

		KEP::Simd4i32 msSinceFrameChange(aParticles[0]->m_msSinceFrameChange, aParticles[1]->m_msSinceFrameChange, aParticles[2]->m_msSinceFrameChange, aParticles[3]->m_msSinceFrameChange);
		msSinceFrameChange += KEP::Simd4i32(msElapsed);

		if (pParticleType->bLooping) {
			KEP::Simd4i32 msPerFrame(pParticleType->msPerFrame);
			KEP::Simd4i32 keepCurrentTexture = msSinceFrameChange < msPerFrame;
			if (keepCurrentTexture.AnyFalse()) {
				// One or more bitmap indexes needs updating
				KEP::Simd4i32 bmpIndex(aParticles[0]->m_bmpIndex, aParticles[1]->m_bmpIndex, aParticles[2]->m_bmpIndex, aParticles[3]->m_bmpIndex);
				KEP::Simd4i32 nTextures4(nTextures);
				do {
					msSinceFrameChange -= msPerFrame & ~keepCurrentTexture;
					bmpIndex += KEP::Simd4i32(1) & ~keepCurrentTexture;
					KEP::Simd4i32 pastMaxTexture = bmpIndex == nTextures4;
					bmpIndex &= ~pastMaxTexture;
					keepCurrentTexture = msSinceFrameChange < msPerFrame;
				} while (keepCurrentTexture.AnyFalse());
				aParticles[0]->m_bmpIndex = bmpIndex.AtIndex<0>();
				aParticles[1]->m_bmpIndex = bmpIndex.AtIndex<1>();
				aParticles[2]->m_bmpIndex = bmpIndex.AtIndex<2>();
				aParticles[3]->m_bmpIndex = bmpIndex.AtIndex<3>();
			}
		} else {
			// if not looping, spread image animation over particles lifetime
			KEP::Simd4i32 nTextures4(nTextures);
			KEP::Simd4i32 lifeSpan(aParticles[0]->m_iLifeSpan, aParticles[1]->m_iLifeSpan, aParticles[2]->m_iLifeSpan, aParticles[3]->m_iLifeSpan);
			KEP::Simd4i32 keepCurrentTexture = msSinceFrameChange * nTextures4 < lifeSpan;
			if (keepCurrentTexture.AnyFalse()) {
				KEP::Simd4i32 currentBmpIndex(aParticles[0]->m_bmpIndex, aParticles[1]->m_bmpIndex, aParticles[2]->m_bmpIndex, aParticles[3]->m_bmpIndex);
				KEP::Simd4i32 bmpIncrement = (KEP::Simd4i32(1) & ~keepCurrentTexture);
				KEP::Simd4i32 nextBmpIndex = currentBmpIndex + bmpIncrement;
				KEP::Simd4i32 equalsNumTextures = nextBmpIndex == nTextures4;
				nextBmpIndex = nextBmpIndex & ~equalsNumTextures;
				KEP::Simd4i32 bmpIndexStart(aParticles[0]->m_bmpIndexStart, aParticles[1]->m_bmpIndexStart, aParticles[2]->m_bmpIndexStart, aParticles[3]->m_bmpIndexStart);
				KEP::Simd4i32 equalsStart = nextBmpIndex == bmpIndexStart;
				nextBmpIndex = IfThenElse(equalsStart, currentBmpIndex, nextBmpIndex);
				msSinceFrameChange &= keepCurrentTexture;
				aParticles[0]->m_bmpIndex = nextBmpIndex.AtIndex<0>();
				aParticles[1]->m_bmpIndex = nextBmpIndex.AtIndex<1>();
				aParticles[2]->m_bmpIndex = nextBmpIndex.AtIndex<2>();
				aParticles[3]->m_bmpIndex = nextBmpIndex.AtIndex<3>();
			}
		}
		aParticles[0]->m_msSinceFrameChange = msSinceFrameChange.AtIndex<0>();
		aParticles[1]->m_msSinceFrameChange = msSinceFrameChange.AtIndex<1>();
		aParticles[2]->m_msSinceFrameChange = msSinceFrameChange.AtIndex<2>();
		aParticles[3]->m_msSinceFrameChange = msSinceFrameChange.AtIndex<3>();
	}
	// This version is a little slower than the others, probably because it does more work when particles don't need to be updated.
	// It may be faster when bitmaps change rapidly.
	__forceinline static void UpdateTextureIndex4_C(ParticleType* pParticleType, uint32_t msElapsed, Particle* aParticles[4]) {
		if (pParticleType->m_nTextures <= 1)
			return;

		KEP::Simd4f textureCyclesPerMS;
		if (pParticleType->bLooping)
			textureCyclesPerMS = KEP::Simd4f(pParticleType->m_fTexturesPerMS * pParticleType->m_fReciprocalNumTextures);
		else
			textureCyclesPerMS = KEP::Simd4f(aParticles[0]->m_fReciprocalLifeSpan, aParticles[1]->m_fReciprocalLifeSpan, aParticles[2]->m_fReciprocalLifeSpan, aParticles[3]->m_fReciprocalLifeSpan);
		KEP::Simd4f numCycles =
			KEP::Simd4f(int32_t(aParticles[0]->m_bmpIndexStart), int32_t(aParticles[1]->m_bmpIndexStart), int32_t(aParticles[2]->m_bmpIndexStart), int32_t(aParticles[3]->m_bmpIndexStart)) + KEP::Simd4f(KEP::Simd4i32(aParticles[0]->m_iLifeTime, aParticles[1]->m_iLifeTime, aParticles[2]->m_iLifeTime, aParticles[3]->m_iLifeTime)) * textureCyclesPerMS;
		KEP::Simd4i32 completeCycles = numCycles.TruncatedI32();
		KEP::Simd4f partialCycles = numCycles - KEP::Simd4f(completeCycles);
		KEP::Simd4i32 bmpIndex = (partialCycles * KEP::Simd4f(pParticleType->m_nTextures)).TruncatedI32();
		aParticles[0]->m_bmpIndex = bmpIndex.AtIndex<0>();
		aParticles[1]->m_bmpIndex = bmpIndex.AtIndex<1>();
		aParticles[2]->m_bmpIndex = bmpIndex.AtIndex<2>();
		aParticles[3]->m_bmpIndex = bmpIndex.AtIndex<3>();
	}

private:
	KEP::Simd4f m_Pos;
	// 16 bytes

	KEP::Simd4f m_Vel;
	// 32 bytes

	float m_fSizeMult;
	float m_fReciprocalLifeSpan;

	uint32_t m_iLifeTime; // milliseconds in existance
	uint32_t m_iLifeSpan; // max # of milliseconds particle will live. 0 means forever

	// 48 bytes

	uint32 m_msSinceFrameChange; // # of milliseconds since texture frame changed

	uint8 m_bmpIndex; // which current bitmap to use for the particle out of the list
	uint8 m_bmpIndexStart; // beginning bmp, to check if anim done (when image anim is spread over particle's lifetime)

	uint8 m_MirrorFlags; // 0x1 : MirrorX, 0x2: MirrorY

	uint8_t m_iRotStart; // 0 = 0 degrees, 256 = 360 degrees. i.e. #degrees = m_iRotStart * (360.0 / 256.0).
	float m_fRotMult; // unitless

	Emitter* m_pEmitter;
	// 64 bytes
};

} // namespace wkg