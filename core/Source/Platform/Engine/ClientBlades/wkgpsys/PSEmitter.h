///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ParticleSystem.h"
#include "PSFunction.h"
#include "AABB.h"
#include <Core/Math/Matrix.h>
#include <Core/Math/Vector.h>
#include <Core/Util/Memory.h>

namespace wkg {

class FileStream;

//
// EmitterType -- static data for a particular kind of emitter (also creates emitters)
//
class ParticleSystem::EmitterType {
public:
	EmitterType();
	~EmitterType();

	bool Read(FileStream&, uint32);

	Emitter* CreateEmitter(Effect*, EmitterType*, ParticleType*, const KEP::Vector3f&);

public:
	enum EmitterShape {
		EMITTER_CUBE,
		EMITTER_SPHERE,
	};

	TimeMs baseLifetime;

	uint32 randExtraLifetime; // milliseconds

	bool bLoopCycle, bEmitLight;
	ParticleFunction emitFunc; // particles/second
	ParticleFunction lifeFunc; // seconds
	ParticleFunction xVelFunc, yVelFunc, zVelFunc; // meters/second
	ParticleFunction lightRFunc, lightGFunc, lightBFunc;
	ParticleFunction lightFalloffFunc; // meters
	ParticleFunction sizeMultFunc, rotMultFunc; // unitless
	KEP::Vector3f lightOffset; // meters

	EmitterShape shapeType; // meters

	struct SquareShapeData {
		// all in meters
		KEP::Vector4f m_min = KEP::Vector4f(0);
		KEP::Vector4f m_max = KEP::Vector4f(0);
	};

	struct SphereShapeData {
		real hotspot; // all in meters
		real falloff;
		KEP::Vector3f m_center;
	};

	union ShapeData {
		ShapeData() {}
		SquareShapeData square;
		SphereShapeData sphere;
	};

	ShapeData shapeData;
};

//
// Emitter -- dynamic data representing an active emitter in the world
//
class ParticleSystem::Emitter : public KEP::AlignedOperatorNewBase<16> {
public:
	Emitter(Effect*, EmitterType*, ParticleType*, const KEP::Vector3f&);
	~Emitter();

	void CreateParticle(real);

	bool GenerateParticles(float elapsedMs);

	void UpdateParticles(float elapsedMs);

	bool HasActiveParticles() const { return !m_particles.empty(); }

	KEP::Vector3f Orient(const KEP::Vector3f&) const;

	void ResetBounds() { m_PointBounds.SetEmpty(); }
	void UpdateBounds(const __m128& pt) { m_PointBounds.Add(pt); }
	void UpdateBounds(const KEP::Vector4f& pt) { m_PointBounds.Add(pt); }
	void UpdateBounds(const KEP::Vector3f& pt) { m_PointBounds.Add(pt); }
	const AABB& GetBBox() const { return m_PointBounds; }

	void SetWaitingToDie(bool bWaitingToDie) { m_waitingToDie = bWaitingToDie; }
	bool GetWaitingToDie() const { return m_waitingToDie; }

	const KEP::Vector4f& GetOrientedOffset() const { return m_OrientedOffset; }
	const KEP::Vector3f& GetOffset() const { return m_offset; }
	const ParticleType* GetParticleType() const { return m_pParticleType; }

	ParticleList::iterator begin() { return m_particles.begin(); }
	ParticleList::iterator end() { return m_particles.end(); }

private:
	AABB m_PointBounds;

	Effect* m_pEffect;
	EmitterType* m_pEmitterType;
	ParticleType* m_pParticleType;
	KEP::Vector3f m_offset; // wkg::Units
	KEP::Vector4f m_OrientedOffset; // offset transformed by orientation.
	alignas(16) KEP::Matrix44f m_orientation; // only changed in ParticleSystem::GenerateParticles
	float m_fFractionalParticles; // fractional particles to emit

	float m_lifetimeMs; // milliseconds in existance
	float m_lifespanMs; // milliseconds to live
	float m_reciprocalLifeSpanMs;

	ParticleList m_particles; // particles are stored with their emitters to allow controllable layering of particles from multiple emitters
	bool m_waitingToDie; // after last particle dies
};

} // namespace wkg