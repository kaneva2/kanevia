///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "wkgtypes.h"
#include "PSEffect.h"
#include "PSEmitter.h"
#include "PSParticle.h"
#include "PSUtils.h"
#include "FileStream.h"
#include "Units.h"
#include <Core/Math/Units.h>

using namespace KEP;

namespace wkg {

ParticleSystem::EffectType::EffectType() :
		numEmitters(0),
		particleTypes(0),
		emitterTypes(0),
		posOffsets(0),
		emitIntoObjectspace(false) {
}

ParticleSystem::EffectType::~EffectType() {
	delete[] particleTypes;
	delete[] emitterTypes;
	delete[] posOffsets;

	particleTypes = 0;
	emitterTypes = 0;
	posOffsets = 0;
}

ParticleSystem::Effect* ParticleSystem::EffectType::CreateEffect(const Vector3f& pos, const Quaternionf& orientation) {
	return new Effect(this, pos, orientation);
}

float ParticleSystem::EffectType::CalculateMaximumParticleSize() const {
	float mx = 0;
	for (size_t i = 0; i < numEmitters; ++i) {
		const EmitterType* pEmitterType = emitterTypes[i];
		const ParticleType* pParticleType = particleTypes[i];
		float fEmitterSize = pEmitterType->sizeMultFunc.MaxValue();
		float fPartSize = pParticleType->sizeFunc.MaxValue();
		float fCombinedSize = fEmitterSize * fPartSize;
		mx = (std::max)(fCombinedSize, mx);
	}
	return mx;
}

bool ParticleSystem::EffectType::Read(FileStream& f, uint32 /*fileVersion*/, ParticleSystem* pPartiSys) {
	char szName[257];
	uint8 uint8Val;
	float floatVal; // don't change this to real
	size_t len;

	if (!f.Read(&(numEmitters)))
		goto fileError;

	if (numEmitters > 0) {
		emitterTypes = new EmitterType*[numEmitters];
		particleTypes = new ParticleType*[numEmitters];
		posOffsets = new Vector3f[numEmitters];
		memzero(emitterTypes, sizeof(EmitterType*) * numEmitters);
		memzero(particleTypes, sizeof(ParticleType*) * numEmitters);

		for (uint8 i = 0; i < numEmitters; i++) {
			if (!ReadString(f, szName, &len))
				goto fileError;
			emitterTypes[i] = pPartiSys->FindEmitterType(szName);

			if (!ReadString(f, szName, &len))
				goto fileError;
			particleTypes[i] = pPartiSys->FindParticleType(szName);

			READREAL(posOffsets[i].x);
			READREAL(posOffsets[i].y);
			READREAL(posOffsets[i].z);
			READBOOL(emitIntoObjectspace);

			// Only Kaneva objects are read with this function, and they all have "emitIntoObjectspace
			// set to true. I don't think any of them should be true (at least the two dance game
			// particles - "Perfect" and stars - need to be false).
			// I'll force them all false here and if any particles become broken, then we know only
			// those dance particles should be edited.
			emitIntoObjectspace = false;
		}
	}

	return true;

fileError:
	ASSERT(!"Error reading file in ParticleSystem::EffectType::Read()");
	return false;
}

ParticleSystem::Effect::Effect(EffectType* pEffectType, const Vector3f& Pos, const Quaternionf& Orient) :
		fixedRef(Pos, Orient), /*refObj(fixedRef),*/ m_pEffectType(pEffectType) {
	Init();
}
/*
ParticleSystem::Effect::Effect(EffectType* pEffectType, WorldObject& AttachToMe)
	:	refObj(AttachToMe), m_pEffectType(pEffectType) {
	Init();
}
*/
ParticleSystem::Effect::~Effect() {
	Clear();
}

void ParticleSystem::Effect::Init() {
	m_bShouldDelete = false;

	ResetBoundingBox();

	for (uint8 i = 0; i < m_pEffectType->numEmitters; i++) {
		if (m_pEffectType->particleTypes[i] &&
			m_pEffectType->emitterTypes[i]) {
			std::unique_ptr<Emitter> pEmitter(m_pEffectType->emitterTypes[i]->CreateEmitter(
				this,
				m_pEffectType->emitterTypes[i],
				m_pEffectType->particleTypes[i],
				MetersToFeet(m_pEffectType->posOffsets[i])));

			m_apEmitters.push_back(std::move(pEmitter));
		}
	}
	m_fMaxParticleSize = m_pEffectType->CalculateMaximumParticleSize();
}

void ParticleSystem::Effect::Clear() {
	m_apEmitters.clear();
}

bool ParticleSystem::Effect::HasActiveParticles() {
	for (auto& pEmitter : m_apEmitters) {
		if (pEmitter->HasActiveParticles())
			return true;
	}
	return false;
}

bool ParticleSystem::Effect::GenerateParticles(TimeMs elapsedMs) {
	bool stillActive = false;

	// Start With Fresh Bounding Box
	ResetBoundingBox();
	for (size_t i = 0; i < m_apEmitters.size(); i) {
		size_t iNext = i + 1;
		Emitter* pEmitter = m_apEmitters[i].get();
		if (pEmitter->GetWaitingToDie()) {
			if (pEmitter->HasActiveParticles()) {
				stillActive = true;
			} else {
				m_apEmitters.erase(m_apEmitters.begin() + i);
				--iNext;
			}
		} else {
			pEmitter->GenerateParticles(elapsedMs);
			stillActive = true;
		}
		i = iNext;
	}

	return stillActive;
}

void ParticleSystem::Effect::UpdateParticles(TimeMs elapsedMs) {
	for (auto& pEmitter : m_apEmitters)
		pEmitter->UpdateParticles(elapsedMs);
}

void ParticleSystem::Effect::SetWaitingToDie(bool waitingToDie) {
	for (auto& pEmitter : m_apEmitters)
		pEmitter->SetWaitingToDie(waitingToDie);
}

void ParticleSystem::Effect::ResetBoundingBox() {
	for (auto& pEmitter : m_apEmitters)
		pEmitter->ResetBounds();
}

void ParticleSystem::Effect::UpdateAccumulatedBox(double dCurrentTimeSeconds) {
	bool bObjectSpace = m_pEffectType->emitIntoObjectspace;
	m_bboxFrame.SetEmpty();
	for (auto& pEmitter : m_apEmitters) {
		if (bObjectSpace) {
			const AABB& emitterBox = pEmitter->GetBBox();
			if (!emitterBox.IsEmpty()) {
				Simd4f emitterOffset(pEmitter->GetOrientedOffset());
				Simd4f totalOffset = Simd4f(GetSysPos(), 0) + emitterOffset;
				Simd4f center = emitterBox.GetCenter();
				Simd4f halfSize = emitterBox.GetExtent();
				Simd4f vHalfDiagLength = halfSize.Length_Approx();
				Simd4f ptOffsetCenter = center + totalOffset;
				m_bboxFrame.Add(ptOffsetCenter + vHalfDiagLength);
				m_bboxFrame.Add(ptOffsetCenter - vHalfDiagLength);
			}
		} else {
			m_bboxFrame.Add(pEmitter->GetBBox());
		}
	}
	m_bboxFrame.Expand(Simd4f(0.5f * m_fMaxParticleSize));

	m_bboxCalculating.Add(m_bboxFrame);
	if (m_dBBoxCalcStartTime + kdBoxRecalcInterval < dCurrentTimeSeconds) {
		m_dBBoxCalcStartTime = dCurrentTimeSeconds;
		m_bboxSustained = m_bboxCalculating;
		m_bboxCalculating.SetEmpty();
	} else {
		m_bboxSustained.Add(m_bboxCalculating);
	}
}
#if 0
ParticleSystem::EffectParticleIterator ParticleSystem::Effect::GetParticleIterator() {
	return EffectParticleIterator(*this);
}

ParticleSystem::EffectParticleIterator::EffectParticleIterator(Effect& toIter)
	: effect(toIter) {
	iEmit = effect.emitters.begin();

	if (iEmit != effect.emitters.end()) {
		iParticle = (*iEmit)->begin();

		while (iParticle == (*iEmit)->end()) {
			++iEmit;
			if (iEmit != effect.emitters.end())
				iParticle = (*iEmit)->begin();
			else
				break;
		}
	}
}

ParticleSystem::Particle* ParticleSystem::EffectParticleIterator::operator*() {
	if (iEmit != effect.emitters.end()) {
		if (iParticle != (*iEmit)->end())
			return &*iParticle;
	}
	return 0;
}

ParticleSystem::Emitter* ParticleSystem::EffectParticleIterator::GetEmitter() {
	if (iEmit != effect.emitters.end()) {
		return *iEmit;
	}
	return 0;
}

void ParticleSystem::EffectParticleIterator::operator++() {
	if (iEmit != effect.emitters.end()) {
		ASSERT(iParticle != (*iEmit)->end());

		++iParticle;
		while (iParticle == (*iEmit)->end()) {
			++iEmit;
			if (iEmit != effect.emitters.end())
				iParticle = (*iEmit)->begin();
			else
				break;
		}
	}
}
#endif
} // namespace wkg