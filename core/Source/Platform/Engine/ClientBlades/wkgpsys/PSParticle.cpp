///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "wkgtypes.h"
#include "PSParticle.h"
#include "PSEmitter.h"
#include "PSEffect.h"
#include "PSUtils.h"
#include "FileStream.h"
#include "Units.h"
#include "texture.h"
#include "TextureFactory.h"
#include <Core/Math/Units.h>
#include "LogHelper.h"
static LogInstance("Instance");

using namespace KEP;

namespace wkg {

ParticleSystem::ParticleType::ParticleType() :
		/*textures(0),*/ m_nTextures(0), m_fReciprocalNumTextures(0), msPerFrame(0), m_fTexturesPerMS(0), bLooping(false), bRandomStartFrame(false), bRandomRotDir(false), bRandomXMirror(false), bRandomYMirror(false), bMotionBlur(false), bEnableLighting(false), bAdditiveAlpha(false),
		//numBlurSteps(4), blurAlphaFadeTo(0), blurTrailLength(0.0f),
		gravityCoef(1.0f),
		resistanceCoef(0.0f),
		startRotMin(0.0f),
		startRotMax(0.0f),
		velResistance(LINEAR),
		sizeResistance(LINEAR) {
}

ParticleSystem::ParticleType::~ParticleType() {
	ClearTextures();
}

void ParticleSystem::ParticleType::TextureRelease(Texture* pTexture) {
	if (pTexture)
		pTexture->Release();
}

bool ParticleSystem::ParticleType::ReserveTextures(int32 numToReserve) {
	bool bSucceeded = false;
	if (numToReserve > 0 && numToReserve < 255) { // 255 special sentinel (see UpdateParticles)
		m_papTextures = std::make_unique<std::unique_ptr<Texture, TextureReleaser>[]>(numToReserve);
		if (m_papTextures) {
			m_nTextures = uint8(numToReserve & 0xff);
			m_fReciprocalNumTextures = m_nTextures ? 1.0f / m_nTextures : 0.0f;
			bSucceeded = true;
		}
	}
	return bSucceeded;
}

void ParticleSystem::ParticleType::LoadTexture(int32 index, char* szName) {
	m_papTextures[index].reset(TextureFactory::Instance()->Get(szName));
}

void ParticleSystem::ParticleType::ClearTextures() {
	m_papTextures.reset();
	m_nTextures = 0;
	m_fReciprocalNumTextures = 0;
}

bool ParticleSystem::ParticleType::Read(FileStream& f, uint32 fileVersion) {
	char szName[257];
	uint8 uint8Val;
	float floatVal; // don't change this to real
	size_t len;

	if (!rFunc.Read(f))
		goto fileError;
	if (!gFunc.Read(f))
		goto fileError;
	if (!bFunc.Read(f))
		goto fileError;
	if (!aFunc.Read(f))
		goto fileError;
	if (!sizeFunc.Read(f))
		goto fileError;
	READREAL(gravityCoef);
	READREAL(resistanceCoef);
	resistanceCoef = -resistanceCoef;
	m_bConstantVelocity = gravityCoef == 0 && resistanceCoef == 0;

	if (!f.Read(&uint8Val))
		goto fileError;
	switch (uint8Val) {
		case LINEAR:
		case QUADRATIC:
			velResistance = MultiplierType(uint8Val);
			break;

		default:
			velResistance = LINEAR;
			break;
	}

	if (!f.Read(&uint8Val))
		goto fileError;
	switch (uint8Val) {
		case NONE:
		case LINEAR:
		case QUADRATIC:
			sizeResistance = MultiplierType(uint8Val);
			break;

		default:
			sizeResistance = LINEAR;
	}

	READBOOL(bEnableLighting);
	READBOOL(bMotionBlur);

	if (bMotionBlur) {
		// disabled member variables
		uint8 numBlurSteps, blurAlphaFadeTo;
		real blurTrailLength;

		if (!f.Read(&numBlurSteps))
			goto fileError;
		if (!f.Read(&blurTrailLength))
			goto fileError;
		if (!f.Read(&blurAlphaFadeTo))
			goto fileError;
	}

	// read in texture/sprite animation info
	if (!f.Read(&m_nTextures))
		goto fileError;

	if (m_nTextures > 0) {
		if (!ReserveTextures(m_nTextures)) {
			m_nTextures = 0;
			goto fileError;
		}

		if (fileVersion >= 0x10001) {
			READBOOL(bAdditiveAlpha);
		}

		// whether or not to loop sprite list in animation
		READBOOL(bLooping);

		if (bLooping) {
			if (!f.Read(&msPerFrame))
				goto fileError;
		}

		READBOOL(bRandomStartFrame);
		READBOOL(bRandomXMirror);
		READBOOL(bRandomYMirror);
		READBOOL(bRandomRotDir);
		READREAL(startRotMin);
		READREAL(startRotMax);

		if (!rotFunc.Read(f))
			goto fileError;

		// read in filenames and create texture
		for (uint8 i = 0; i < m_nTextures; i++) {
			if (!ReadString(f, szName, &len))
				goto fileError;
			LoadTexture(i, szName);
		}
	}
	m_fReciprocalNumTextures = m_nTextures > 0 ? 1.0f / m_nTextures : 0.0f;

	CreateFunctionTables();

	return true;

fileError:
	ASSERT(!"Error reading file in ParticleSystem::ParticleType::Read()");
	return false;
}

void ParticleSystem::ParticleType::CreateFunctionTables() {
	m_rotFunctionTable = rotFunc.CreateTable();
	m_sizeFunctionTable = sizeFunc.CreateTable();
	m_rgbaFunctionTable = ParticleFunction::CreateTable(&bFunc, &gFunc, &rFunc, &aFunc);
#if 0
	{
		Simd4f fMax( 0 );
		Simd4f fMaxError( 0 );
		for( size_t i = 0; i <= 1024; ++i ) {
			float fIn = i / 1024.0f;
			Simd4f f1(
				rFunc.Sample(fIn),
				gFunc.Sample(fIn),
				bFunc.Sample(fIn),
				aFunc.Sample(fIn));
			Simd4f f2(m_rgbaFunctionTable.LookUp(fIn));
			Simd4f fError = Abs(f1 - f2);
			fMaxError = Max(fError, fMaxError);
			fMax = Max(Abs(f1), fMax);
			fMax = Max(Abs(f2), fMax);
		}
		Simd4f fRelError = fMaxError / fMax;
		Vector4f vRelError( fRelError );
		Vector4f vMaxError( fMaxError );
		for( size_t i = 0; i < 4; ++i )
			LogInfo("RelError: " << vRelError[i] << "MaxError: " << vMaxError[i]);
	}
#endif
}

bool ParticleSystem::Particle::Update(ParticleType* pParticleType, const int32 msElapsed) {
	if (!StillAlive())
		return false; // particle is dead

	Simd4f dt(msElapsed * .001f);

	Simd4f vDeltaPosMeters;
	if (pParticleType->m_bConstantVelocity) {
		vDeltaPosMeters = dt * m_Vel;
	} else {
		Simd4f vAccel(0, -9.80665f * pParticleType->gravityCoef, 0, 0);

		// air resistance (surface area is spherical)
		if (pParticleType->resistanceCoef != 0.0f) {
			Simd4f drag;
			if (pParticleType->velResistance == ParticleSystem::ParticleType::QUADRATIC)
				drag = m_Vel * Abs(m_Vel);
			else
				drag = m_Vel;

			float area_times_resist;
			if (pParticleType->sizeResistance == ParticleSystem::ParticleType::NONE) {
				area_times_resist = pParticleType->resistanceCoef;
			} else {
				//size = pParticleType->sizeFunc.Sample(sampleTime) * m_sizeMult;
				real sampleTime = CalcSampleTime();
				float size = pParticleType->m_sizeFunctionTable.LookUp(sampleTime) * GetSizeMult();
				float area = 0.5f * WKG_PI * SQR(size);

				if (pParticleType->sizeResistance == ParticleSystem::ParticleType::QUADRATIC)
					area *= area;
				area_times_resist = area * pParticleType->resistanceCoef;
			}

			vAccel += drag * Simd4f(area_times_resist);
		}

		Simd4f vDeltaVel = vAccel * dt;
		vDeltaPosMeters = dt * (m_Vel + vDeltaVel * Simd4f(0.5f));
		m_Vel += vDeltaVel;
	}
	SetPos(m_Pos + MetersToFeet(vDeltaPosMeters));

	UpdateTextureIndex(pParticleType, msElapsed);

	m_iLifeTime += msElapsed; // update here to give particle a chance to use 0 lifetime values (otherwise it may die immediately)

	return true;
}

void ParticleSystem::Particle::Update4Alive(ParticleType* pParticleType, const int32 msElapsed, Particle* aParticles[4]) {
	Simd4f dt(msElapsed * .001f);

	Simd4f forceX(0);
	Simd4f forceY(-9.80665f * pParticleType->gravityCoef);
	Simd4f forceZ(0);

	Simd4f velX, velY, velZ;
	Transpose(aParticles[0]->m_Vel, aParticles[1]->m_Vel, aParticles[2]->m_Vel, aParticles[3]->m_Vel, out(velX), out(velY), out(velZ));

	Simd4f vDeltaPosMetersX;
	Simd4f vDeltaPosMetersY;
	Simd4f vDeltaPosMetersZ;
	if (pParticleType->m_bConstantVelocity) {
		vDeltaPosMetersX = dt * velX;
		vDeltaPosMetersY = dt * velY;
		vDeltaPosMetersZ = dt * velZ;
	} else {
		Simd4f vAccelY(-9.80665f * pParticleType->gravityCoef);
		Simd4f velNewX;
		Simd4f velNewY;
		Simd4f velNewZ;

		// air resistance (surface area is spherical)
		if (pParticleType->resistanceCoef == 0.0f) {
			Simd4f deltaVelY = vAccelY * dt;
			Simd4f half_dt = Simd4f(0.5f) * dt;
			velNewX = velX;
			velNewY = velY + deltaVelY;
			velNewZ = velZ;
			vDeltaPosMetersX = dt * velNewX;
			vDeltaPosMetersY = half_dt * (velY + velNewY);
			vDeltaPosMetersZ = dt * velNewZ;
		} else {
			Simd4f dragX = velX;
			Simd4f dragY = velY;
			Simd4f dragZ = velZ;
			if (pParticleType->velResistance == ParticleSystem::ParticleType::QUADRATIC) {
				dragX = velX * Abs(velX);
				dragY = velY * Abs(velY);
				dragZ = velZ * Abs(velZ);
			}

			Simd4f area_times_resist;
			Simd4f resistanceCoef(pParticleType->resistanceCoef);
			if (pParticleType->sizeResistance == ParticleSystem::ParticleType::NONE) {
				area_times_resist = resistanceCoef;
			} else {
				Simd4f size = pParticleType->m_sizeFunctionTable.LookUp4(aParticles[0]->CalcSampleTime(), aParticles[1]->CalcSampleTime(), aParticles[2]->CalcSampleTime(), aParticles[3]->CalcSampleTime()) * Simd4f(aParticles[0]->GetSizeMult(), aParticles[1]->GetSizeMult(), aParticles[2]->GetSizeMult(), aParticles[3]->GetSizeMult());

				Simd4f area = Simd4f(0.5f * WKG_PI) * size * size;

				if (pParticleType->sizeResistance == ParticleSystem::ParticleType::QUADRATIC)
					area *= area;
				area_times_resist = area * resistanceCoef;
			}

			Simd4f vAccelX = dragX * area_times_resist;
			vAccelY += dragY * area_times_resist;
			Simd4f vAccelZ = dragZ * area_times_resist;
			Simd4f deltaVelX = vAccelX * dt;
			Simd4f deltaVelY = vAccelY * dt;
			Simd4f deltaVelZ = vAccelZ * dt;
			velNewX = velX + deltaVelX;
			velNewY = velY + deltaVelY;
			velNewZ = velZ + deltaVelZ;
			vDeltaPosMetersX = dt * (velX + deltaVelX * Simd4f(0.5f));
			vDeltaPosMetersY = dt * (velY + deltaVelY * Simd4f(0.5f));
			vDeltaPosMetersZ = dt * (velZ + deltaVelZ * Simd4f(0.5f));
		}

		Transpose(velNewX, velNewY, velNewZ, Simd4f(0), out(aParticles[0]->m_Vel), out(aParticles[1]->m_Vel), out(aParticles[2]->m_Vel), out(aParticles[3]->m_Vel));
	}
	Simd4f vDeltaPosFeetX = MetersToFeet(vDeltaPosMetersX);
	Simd4f vDeltaPosFeetY = MetersToFeet(vDeltaPosMetersY);
	Simd4f vDeltaPosFeetZ = MetersToFeet(vDeltaPosMetersZ);
	Simd4f deltaPos0, deltaPos1, deltaPos2, deltaPos3;
	Transpose(vDeltaPosFeetX, vDeltaPosFeetY, vDeltaPosFeetZ, Simd4f(0), out(deltaPos0), out(deltaPos1), out(deltaPos2), out(deltaPos3));

	Emitter* pEmitter = aParticles[0]->m_pEmitter;
	Simd4f newPos0 = aParticles[0]->m_Pos + deltaPos0;
	Simd4f newPos1 = aParticles[1]->m_Pos + deltaPos1;
	Simd4f newPos2 = aParticles[2]->m_Pos + deltaPos2;
	Simd4f newPos3 = aParticles[3]->m_Pos + deltaPos3;

	// Calculate all new positions prior to updating so duplicates in aParticles[] are handled correctly.
	aParticles[0]->m_Pos = newPos0;
	pEmitter->UpdateBounds(aParticles[0]->m_Pos);

	aParticles[1]->m_Pos = newPos1;
	pEmitter->UpdateBounds(aParticles[1]->m_Pos);

	aParticles[2]->m_Pos = newPos2;
	pEmitter->UpdateBounds(aParticles[2]->m_Pos);

	aParticles[3]->m_Pos = newPos3;
	pEmitter->UpdateBounds(aParticles[3]->m_Pos);

	UpdateTextureIndex4_A(pParticleType, msElapsed, aParticles);

	// update here to give particle a chance to use 0 lifetime values (otherwise it may die immediately)
	aParticles[0]->m_iLifeTime += msElapsed;
	aParticles[1]->m_iLifeTime += msElapsed;
	aParticles[2]->m_iLifeTime += msElapsed;
	aParticles[3]->m_iLifeTime += msElapsed;
}

} // namespace wkg