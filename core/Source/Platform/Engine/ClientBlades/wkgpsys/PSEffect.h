///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ParticleSystem.h"
#include "AABB.h"
#include <Core/Math/Quaternion.h>
#include <Core/Util/Memory.h>

namespace wkg {

class FileStream;

class WorldObject {
public:
	const KEP::Vector3f& GetPosition() const {
		return m_position;
	}
	const KEP::Quaternionf& GetOrientation() const {
		return m_orientation;
	}

	void SetPosition(KEP::Vector3f& v) {
		m_position = v;
	}

	void SetOrientation(KEP::Quaternionf& q) {
		m_orientation = q;
	}

protected:
	KEP::Vector3f m_position;
	KEP::Quaternionf m_orientation;
	float m_mass;
};

//
// EffectType -- template to build an a kind of effect (also static data for the effect)
//
class ParticleSystem::EffectType {
public:
	EffectType();
	~EffectType();

	bool Read(FileStream&, uint32, ParticleSystem*);

	Effect* CreateEffect(const KEP::Vector3f& pos, const KEP::Quaternionf& orientation);

	float CalculateMaximumParticleSize() const;

public:
	uint8 numEmitters;
	ParticleType** particleTypes;
	EmitterType** emitterTypes;
	KEP::Vector3f* posOffsets; // meters
	bool emitIntoObjectspace;
};
#if 0
//
// EffectParticleIterator -- iterate through all particles in all emitters of an effect
//
// This allows access to all particles in the system from a single function (e.g. used when rendering)
// or it allows global operators across all particles.
//
class ParticleSystem::EffectParticleIterator {
  public:
	Particle* operator*();
	void operator++();

	Emitter* GetEmitter();

  protected:
	EffectParticleIterator(Effect&);
	friend class Effect;

  protected:
	EmitterIterator iEmit;
	ParticleIterator iParticle;
	Effect& effect;
};
#endif
//
// Effect -- an active effect in the world (with dynamic data)
//
class ParticleSystem::Effect : public KEP::AlignedOperatorNewBase<16> {
public:
	Effect(EffectType*, const KEP::Vector3f& Pos, const KEP::Quaternionf& Orient);
	//Effect(EffectType*, WorldObject& AttachToMe);
	~Effect();

	void Clear();

	bool HasActiveParticles();

	bool GenerateParticles(TimeMs elapsedMs);

	void UpdateParticles(TimeMs elapsedMs);

	EffectParticleIterator GetParticleIterator();

	const KEP::Vector3f& GetSysPos() const {
		//return refObj.GetPosition();
		return fixedRef.GetPosition();
	}

	const KEP::Quaternionf& GetSysOrient() const {
		//return refObj.GetOrientation();
		return fixedRef.GetOrientation();
	}

	void SetPosition(KEP::Vector3f& v) {
		//refObj.SetPosition(v);
		fixedRef.SetPosition(v);
	}

	void SetOrientation(KEP::Quaternionf& q) {
		//refObj.SetOrientation(q);
		fixedRef.SetOrientation(q);
	}

	bool InObjectSpace() {
		return m_pEffectType->emitIntoObjectspace;
	}

	void SetWaitingToDie(bool waitingToDie);

	void ResetBoundingBox();
	const KEP::Simd4f& GetBBoxMin() { return m_bboxSustained.GetMin(); }
	const KEP::Simd4f& GetBBoxMax() { return m_bboxSustained.GetMax(); }
	void UpdateAccumulatedBox(double dCurrentTimeSeconds);

	size_t GetEmitterCount() const { return m_apEmitters.size(); }
	Emitter* GetEmitter(size_t i) { return m_apEmitters[i].get(); }

	bool GetShouldDelete() const { return m_bShouldDelete; }
	void SetShouldDelete() { m_bShouldDelete = true; }

	EffectType* GetEffectType() { return m_pEffectType; }

protected:
	void Init();

protected:
	// obviates need to distinguish between attached effects and
	//  non-attached effects (this stores non-attached effect info
	//  in the same way as attached info)
	class FixedEffect : public WorldObject {
	public:
		FixedEffect(const KEP::Vector3f& p, const KEP::Quaternionf& o) {
			m_position = p;
			m_orientation = o;
			m_mass = 0.0f;
		}

		// all undefined
		FixedEffect() {
		}
	};

private:
	EffectType* m_pEffectType;
	std::vector<std::unique_ptr<Emitter>> m_apEmitters;
	bool m_bShouldDelete;
	float m_fMaxParticleSize = 0;

	static constexpr double kdBoxRecalcInterval = 15; // Seconds
	double m_dBBoxCalcStartTime = -DBL_MAX; // When we started calculating m_bboxCalculating, in seconds.
	AABB m_bboxFrame; // Bounding box that encompasses all particles of a single frame.
	AABB m_bboxCalculating; // Bounding box that is accumulating since m_dBBoxCalcStartTime.
	AABB m_bboxSustained; // Bounding box that has been accumulated for at least kdBoxRecalcInterval.

	FixedEffect fixedRef;
	//WorldObject& refObj;
};

} // namespace wkg
