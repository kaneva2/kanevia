///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "wkgtypes.h"
#include "singleton.h"

#include "Core/Util/HighPrecisionTime.h"
#include <Core/Math/Quaternion.h>
#include <Core/Math/Vector.h>
#include <memory>

namespace KEP {
class WKGParticleSystem;
}

namespace wkg {

class FileStream;
class Texture;
class WorldObject;

static constexpr int FVF_PARTICLEVERTEX = (D3DFVF_XYZ | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0) | D3DFVF_DIFFUSE /*| D3DFVF_PSIZE*/);

class ParticleVertex {
public:
	KEP::Vector3f pos; // wkg::Units
	uint32 diffuse;
	real tu, tv;
};

class ParticleSystem : public Singleton<ParticleSystem> {
public:
	ParticleSystem() :
			m_VB(0), m_IB(0) {}

	~ParticleSystem() {
		ClearDefinitions();
	}

	void SetVBIB(PDIRECT3DVERTEXBUFFER9 vb, PDIRECT3DINDEXBUFFER9 ib) {
		m_VB = vb;
		m_IB = ib;
		m_nMaxVertices = 0;
		m_nMaxIndices = 0;
		if (m_VB) {
			HRESULT hrv, hri;
			D3DVERTEXBUFFER_DESC vdesc;
			hrv = m_VB->GetDesc(&vdesc);
			D3DINDEXBUFFER_DESC idesc;
			hri = m_IB->GetDesc(&idesc);
			if (SUCCEEDED(hrv) && SUCCEEDED(hri)) {
				m_nMaxVertices = vdesc.Size / sizeof(ParticleVertex);
				m_nMaxIndices = idesc.Size / sizeof(uint16_t);
			}
		}
	}

	bool LoadDefinitions(const char* szFile);

	void ClearDefinitions();

	void ClearEffects();

	bool Update(TimeMs elapsedMs);

	void Render(bool doSort, int iSizeCullBias);

	uint32 GetNumActiveParticles();
	bool HasActiveParticles();

	class Effect;

protected:
	class ParticleType;
	class EmitterType;
	class EffectType;
	class Particle;
	class Emitter;
	class EffectParticleIterator;
	friend class ParticleType;
	friend class EmitterType;
	friend class EffectType;
	friend class Particle;
	friend class Emitter;
	friend class Effect;
	friend class EffectParticleIterator;

	typedef std::map<std::string, ParticleType*> ParticleTypeMap;
	typedef ParticleTypeMap::iterator ParticleTypeIterator;

	typedef std::map<std::string, EmitterType*> EmitterTypeMap;
	typedef EmitterTypeMap::iterator EmitterTypeIterator;

	typedef std::map<std::string, EffectType*> EffectTypeMap;
	typedef EffectTypeMap::iterator EffectTypeIterator;

	template <typename T, size_t Alignment>
	struct AlignedAllocator {
		//typedef AlignedAllocator <T, Alignment> MyType;
		typedef T value_type;

		template <typename U>
		struct rebind {
			typedef AlignedAllocator<U, Alignment> other;
		};

		template <typename U>
		AlignedAllocator(const AlignedAllocator<U, Alignment>&) {}
		AlignedAllocator() = default;

		void deallocate(T* p, size_t count) { _aligned_free(p); }

		T* allocate(size_t count) { return static_cast<T*>(_aligned_malloc(sizeof(T) * count, Alignment)); }

		T* allocate(size_t count, const void*) { return allocate(count); }
	};
	typedef std::vector<Particle, AlignedAllocator<Particle, 64>> ParticleList;
	typedef ParticleList::iterator ParticleIterator;

	std::vector<Particle*> m_particlesSort[2][256];

	std::vector<Particle*> m_particlesSorted;

	void SortParticles(Effect* pEffect, bool doSort);

public:
	typedef Effect* EffectHandle;

	void SetEffectPosition(EffectHandle effect_handle, float x, float y, float z);
	void SetEffectOrientation(EffectHandle effect_handle, float qx, float qy, float qz, float qw);

	size_t GetNumEffects() { return m_effectTypeMap.size(); }

	const char* GetEffectName(size_t idx) const {
		EffectTypeMap::const_iterator itor = m_effectTypeMap.begin();
		while (idx) {
			itor++;
			idx--;
		}

		return itor->first.c_str();
	}

	EffectType* FindEffectType(const char*);

	// pos is in wkg::Units (not meters or feet, etc)
	bool CreateEffect(const char* szEffectName, const KEP::Vector3f& pos,
		const KEP::Quaternionf& orient, EffectHandle* pHandle = NULL);
	void RemoveEffect(EffectHandle& handle);

	void FreeTextures();

	KEP::Vector3f GetCameraPosition() const {
		return m_camPosition;
	}

	friend class KEP::WKGParticleSystem;

protected:
	ParticleType* FindParticleType(char*);
	EmitterType* FindEmitterType(char*);
	bool GetEmitterTypeName(EmitterType*, char*, size_t*);
	bool GetParticleTypeName(ParticleType*, char*, size_t*);

protected:
	void UpdateCameraPosition(const KEP::Vector3f& newPos) {
		m_camPosition = newPos;
	}

	EffectTypeMap m_effectTypeMap; // all effect types defined in the system
	EmitterTypeMap m_emitterTypeMap; // all emitter types in the system
	ParticleTypeMap m_particleTypeMap; // all particle types in the system

	std::vector<std::unique_ptr<Effect>> m_apEffects; // all active effects

	size_t m_nMaxVertices = 0;
	size_t m_nMaxIndices = 0;
	PDIRECT3DVERTEXBUFFER9 m_VB = 0;
	PDIRECT3DINDEXBUFFER9 m_IB = 0;
	KEP::Vector3f m_camPosition;
};

} // namespace wkg
