///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ParticleSystem.h"

#include "Vertexshader.h"
#include "VSFactory.h"
#include "PSEffect.h"
#include "PSEmitter.h"
#include "PSParticle.h"
#include "PSUtils.h"
#include "FileStream.h"
#include "Units.h"
#include "Device.h"
#include "RenderState.h"
#include "DynIVB.h"

#include "Core/Math/Interpolate.h"
#include "Core/Math/KEPMathUtil.h"
#include "Core/Math/Matrix.h"
#include "Core/Math/Plane.h"
#include "Core/Math/TransformUtil.h"
#include "Core/Math/Units.h"
#include "Common/KEPUtil/Helpers.h"
#include <Common/include/CoreStatic/ITexture.h>

static LogInstance("Instance");

using namespace KEP;

namespace wkg {

#define PARTSYS_FILEVERSION_MAJOR 1
#define PARTSYS_FILEVERSION_MINOR 1

void ParticleSystem::ClearDefinitions() {
	ClearEffects();

	ParticleTypeIterator iPType;
	ParticleTypeIterator endPType = m_particleTypeMap.end();

	for (iPType = m_particleTypeMap.begin(); iPType != endPType; iPType++) {
		delete iPType->second;
	}

	EmitterTypeIterator iEType;
	EmitterTypeIterator endEType = m_emitterTypeMap.end();

	for (iEType = m_emitterTypeMap.begin(); iEType != endEType; iEType++) {
		delete iEType->second;
	}

	EffectTypeIterator iEffect;
	EffectTypeIterator endEffect = m_effectTypeMap.end();

	for (iEffect = m_effectTypeMap.begin(); iEffect != endEffect; iEffect++) {
		delete iEffect->second;
	}

	m_particleTypeMap.clear();
	m_emitterTypeMap.clear();
	m_effectTypeMap.clear();
}

void ParticleSystem::ClearEffects() {
	m_apEffects.clear();
}

ParticleSystem::ParticleType* ParticleSystem::FindParticleType(char* name) {
	ParticleTypeIterator pt = m_particleTypeMap.find(name);
	return pt != m_particleTypeMap.end() ? pt->second : nullptr;
}

ParticleSystem::EmitterType* ParticleSystem::FindEmitterType(char* name) {
	EmitterTypeIterator et = m_emitterTypeMap.find(name);
	return et != m_emitterTypeMap.end() ? et->second : nullptr;
}

bool ParticleSystem::GetEmitterTypeName(EmitterType* pET, char* name, size_t* nameLen) {
	EmitterTypeIterator iET = m_emitterTypeMap.begin();
	EmitterTypeIterator endET = m_emitterTypeMap.end();

	while (iET != endET) {
		if (iET->second == pET) {
			if (iET->first.length() < *nameLen) {
				strcpy_s(name, *nameLen, iET->first.c_str());
				*nameLen = iET->first.length();
			} else {
				--(*nameLen);
				strncpy_s(name, *nameLen, iET->first.c_str(), (*nameLen) - 1);
				name[*nameLen] = '\0';
			}
			return true;
		}

		iET++;
	}
	return false;
}

bool ParticleSystem::GetParticleTypeName(ParticleType* pPT, char* name, size_t* nameLen) {
	ParticleTypeIterator iPT = m_particleTypeMap.begin();
	ParticleTypeIterator endPT = m_particleTypeMap.end();

	while (iPT != endPT) {
		if (iPT->second == pPT) {
			if (iPT->first.length() < *nameLen) {
				strcpy_s(name, *nameLen, iPT->first.c_str());
				*nameLen = iPT->first.length();
			} else {
				--(*nameLen);
				strncpy_s(name, *nameLen, iPT->first.c_str(), (*nameLen) - 1);
				name[*nameLen] = '\0';
			}
			return true;
		}

		iPT++;
	}
	return false;
}

bool ParticleSystem::LoadDefinitions(const char* szFile) {
	ClearDefinitions();

	FileStream f;
	uint32 fileVersion;
	uint8 data;
	char szName[257];
	size_t len;

	if (!f.Open(szFile, FileStream::ReadOnly))
		goto fileError;

	if (!f.Read(&fileVersion))
		goto fileError;

	while (f.Read(&data)) {
		// Read in Particle Type
		if (0 == data) {
			if (!ReadString(f, szName, &len))
				goto fileError;

			ParticleType* pParticleType = new ParticleType;
			m_particleTypeMap.insert(ParticleTypeMap::value_type(szName, pParticleType));

			if (!pParticleType->Read(f, fileVersion))
				goto fileError;
		}
		// Read in Emitter Type
		else if (1 == data) {
			if (!ReadString(f, szName, &len))
				goto fileError;

			EmitterType* pEmitterType = new EmitterType;
			m_emitterTypeMap.insert(EmitterTypeMap::value_type(szName, pEmitterType));

			if (!pEmitterType->Read(f, fileVersion))
				goto fileError;
		}
		// Read in Effect definition
		else if (2 == data) {
			if (!ReadString(f, szName, &len))
				goto fileError;

			EffectType* pEffectType = new EffectType;
			m_effectTypeMap.insert(EffectTypeMap::value_type(szName, pEffectType));

			if (!pEffectType->Read(f, fileVersion, this))
				goto fileError;
		} else {
			goto fileError; // bad data type
		}
	}

	f.Close();
	return true;

fileError:
	LogError("FAILED - '" << szFile << "'");
	f.Close();
	return false;
}

bool ParticleSystem::CreateEffect(
	const char* szEffectName,
	const Vector3f& pos,
	const Quaternionf& orient,
	EffectHandle* pHandle) {
	EffectTypeIterator foundEffect = m_effectTypeMap.find(szEffectName);

	if (foundEffect == m_effectTypeMap.end()) {
		LogError("EFFECT NOT FOUND - '" << szEffectName << "'");
		return false;
	}

	if (foundEffect->second->numEmitters == 0 ||
		NULL == foundEffect->second->emitterTypes ||
		NULL == foundEffect->second->particleTypes) {
		LogError("EFFECT NO EMITTERS - '" << szEffectName << "'");
		return false;
	}

	Effect* pEffect = foundEffect->second->CreateEffect(pos, orient);
	if (pHandle)
		*pHandle = pEffect;
	m_apEffects.push_back(std::unique_ptr<Effect>(pEffect));

	return true;
}

void ParticleSystem::SetEffectPosition(EffectHandle effect_handle, float x, float y, float z) {
	Effect* p = (effect_handle);
	Vector3f vec3(x, y, z);
	p->SetPosition(vec3);
}

void ParticleSystem::SetEffectOrientation(EffectHandle effect_handle, float qx, float qy, float qz, float qw) {
	Quaternionf quat(qx, qy, qz, qw);
	(effect_handle)->SetOrientation(quat);
}

bool ParticleSystem::HasActiveParticles() {
	for (const auto& pE : m_apEffects) {
		if (pE->HasActiveParticles())
			return true;
	}
	return false;
}

void ParticleSystem::RemoveEffect(EffectHandle& handle) {
	for (auto itr = m_apEffects.begin(); itr != m_apEffects.end(); ++itr) {
		if (itr->get() == handle) {
			m_apEffects.erase(itr);
			return;
		}
	}
}

void ParticleSystem::FreeTextures() {
	for (auto& mapEntry : m_effectTypeMap) {
		size_t nEmitters = mapEntry.second->numEmitters;
		for (size_t i = 0; i < nEmitters; ++i) {
			ParticleType* pParticleType = mapEntry.second->particleTypes[i];
			if (!pParticleType)
				continue;
			for (size_t j = 0; j < pParticleType->m_nTextures; ++j) {
				Texture* pTexture = pParticleType->m_papTextures[j].get();
				if (!pTexture || pTexture->GetExternalKey() == 0)
					continue;
				pTexture->SetExternalD3DTexture(nullptr);
			}
		}
	}
}

// Sorts Particle Effects Far To Near For Render

bool ParticleSystem::Update(TimeMs elapsedMs) {
	bool bEffectsDeleted = false;
	double dCurrentTime = fTime::TimeSec();

	// Generate & Update All Particle Effects
	for (auto& pEffect : m_apEffects) {
		// Generate Effect New Particles
		if (!pEffect->GenerateParticles(elapsedMs)) {
			bEffectsDeleted = true;
			pEffect->SetShouldDelete();
		}

		// Update Effect Particle Positions
		pEffect->UpdateParticles(elapsedMs);
		pEffect->UpdateAccumulatedBox(dCurrentTime);
	}

	// Sort Particle Effects Far To Near
	//m_effectList.sort(CompareFarToNear);
	Vector3f camPosition = m_camPosition;
	std::sort(m_apEffects.begin(), m_apEffects.end(), [camPosition](std::unique_ptr<ParticleSystem::Effect>& e1, std::unique_ptr<ParticleSystem::Effect>& e2) {
		Vector3f myVec = e1->GetSysPos();
		Vector3f theirVec = e2->GetSysPos();
		theirVec = theirVec - camPosition;
		myVec = myVec - camPosition;
		float myDist = myVec.LengthSquared();
		float theirDist = theirVec.LengthSquared();
		return (myDist > theirDist);
	});

	return bEffectsDeleted;
}

void ParticleSystem::SortParticles(Effect* pEffect, bool doSort) {
	m_particlesSorted.clear();
	size_t nEmitters = pEffect->GetEmitterCount();
	if (doSort) {
		size_t bmpIndexMax = 0;
		bool abAdditiveAlpha[2] = { false, false };

		for (size_t i = 0; i < nEmitters; ++i) {
			Emitter& emitter = *pEffect->GetEmitter(i);
			for (Particle& particle : emitter) {
				Particle* pParticle = &particle;

				size_t bmpIndex = pParticle->GetCurrentBitmapIndex();
				const ParticleType* pParticleType = emitter.GetParticleType();
				size_t alpha = pParticleType->bAdditiveAlpha;
				abAdditiveAlpha[alpha] = true;
				if (bmpIndex > bmpIndexMax)
					bmpIndexMax = bmpIndex;
				m_particlesSort[alpha][bmpIndex].push_back(pParticle);
			}
		}

		for (size_t i = 0; i < 2; ++i) {
			if (!abAdditiveAlpha[i])
				continue; // No particles with this additive-alpha setting.
			for (size_t j = 0; j <= bmpIndexMax; ++j) {
				std::vector<Particle*>& aParticlesSort = m_particlesSort[i][j];
				if (!aParticlesSort.empty()) {
					m_particlesSorted.insert(m_particlesSorted.end(), aParticlesSort.begin(), aParticlesSort.end());
					aParticlesSort.clear();
				}
			}
		}
	} else {
		for (size_t i = 0; i < nEmitters; ++i) {
			for (Particle& particle : *pEffect->GetEmitter(i)) {
				Particle* pParticle = &particle;
				m_particlesSorted.push_back(pParticle);
			}
		}
	}
}

__forceinline void SCREENALIGN_WORLDSPACE_QUAD3(float halfSize, float rotCos, float rotSin, const Simd4f mtxBboard[], const Simd4f& vParticlePos, Simd4f& pos0, Simd4f& pos1, Simd4f& pos2, Simd4f& pos3) {
	float fOffset1 = (rotCos + rotSin) * halfSize;
	float fOffset2 = (rotCos - rotSin) * halfSize;
	Simd4f vOffset1(fOffset1);
	Simd4f vOffset2(fOffset2);
	Simd4f vDirX = mtxBboard[0];
	Simd4f vDirY = mtxBboard[1];
	Simd4f vOffset1X = vOffset1 * vDirX;
	Simd4f vOffset1Y = vOffset1 * vDirY;
	Simd4f vOffset2X = vOffset2 * vDirX;
	Simd4f vOffset2Y = vOffset2 * vDirY;
	Simd4f vOffset1Xminus2Y = vOffset1X - vOffset2Y;
	Simd4f vOffset2Xplus1Y = vOffset2X + vOffset1Y;
	pos0 = vParticlePos - vOffset1Xminus2Y;
	pos1 = vParticlePos + vOffset2Xplus1Y;
	pos2 = vParticlePos - vOffset2Xplus1Y;
	pos3 = vParticlePos + vOffset1Xminus2Y;
};

__forceinline void SCREENALIGN_OBJECTSPACE_QUAD2(float halfSize, float rotCos, float rotSin, const Simd4f& emitterOffset, const Simd4f& sysPos, const Simd4f& vParticlePos, const Simd4f mtxBboard[], ParticleVertex quad[4], Simd4f& pos0, Simd4f& pos1, Simd4f& pos2, Simd4f& pos3) {
	SCREENALIGN_WORLDSPACE_QUAD3(halfSize, rotCos, rotSin, mtxBboard, vParticlePos + sysPos + emitterOffset, pos0, pos1, pos2, pos3);
}

void ParticleSystem::Render(bool doSort, int iSizeCullBias) {
	IDirect3DDevice9* pDevice = Device::Instance();
	if (!pDevice)
		return;

	real rotCos, rotSin;
	bool curAdditiveAlpha;
	RenderState rs;
	ParticleVertex* pVert;

	if (!HasActiveParticles())
		return;

	VSFactory::Instance()->GetShader("ParticleSysFVF")->Activate();

	pDevice->SetPixelShader(0);

	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	pDevice->SetRenderState(D3DRS_ZENABLE, WKG_ZENABLE);
	pDevice->SetRenderState(D3DRS_COLORVERTEX, TRUE);
	pDevice->SetRenderState(D3DRS_FOGENABLE, FALSE);
	pDevice->SetRenderState(D3DRS_SPECULARENABLE, FALSE);
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	//pDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	pDevice->SetRenderState(D3DRS_AMBIENT, 0xFFFFFFFF);
	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	pDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP); // CLAMP is necessary with min/max filtering to prevent anti-alias wrap
	pDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
	pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
	pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE);

	pDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	pDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_DIFFUSE);
	pDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TEXTURE);

	pDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	pDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_DIFFUSE);
	pDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_TEXTURE);

	for (DWORD i = 1; i < 8; ++i) {
		pDevice->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_DISABLE);
		pDevice->SetTextureStageState(i, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
		pDevice->SetTexture(i, nullptr);
	}

	pDevice->SetStreamSource(0, m_VB, 0, sizeof(ParticleVertex));
	pDevice->SetTexture(0, (IDirect3DTexture9*)0);
	pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
	curAdditiveAlpha = false;

	Matrix44f mtxView;
	pDevice->GetTransform(D3DTS_VIEW, reinterpret_cast<D3DMATRIX*>(&mtxView));
	pDevice->SetTransform(D3DTS_WORLD, reinterpret_cast<const D3DMATRIX*>(&Matrix44f::GetIdentity()));
	//const Matrix44f& mtxWorld = Matrix44f::GetIdentity();
	Matrix44f mtxProj;
	pDevice->GetTransform(D3DTS_PROJECTION, reinterpret_cast<D3DMATRIX*>(&mtxProj));
	Matrix44f mtxWVP = /*mtxWorld * */ mtxView * mtxProj;

	Simd4f mtxBBoard[4];
	Transpose(Simd4f(mtxView[0]), Simd4f(mtxView[1]), Simd4f(mtxView[2]), Simd4f(mtxView[3]),
		out(mtxBBoard[0]), out(mtxBBoard[1]), out(mtxBBoard[2]), out(mtxBBoard[3]));

	// Culled vertices are those that are outside a frustum plane
	Plane3f aViewVolumePlanes[5];
	CreateD3DViewVolumePlanes(mtxWVP, aViewVolumePlanes, std::extent<decltype(aViewVolumePlanes)>::value);
	D3DVIEWPORT9 viewport;
	pDevice->GetViewport(&viewport);
	Vector3f ptCameraD3D = GetCameraPosition();
	Simd4f ptCamera(ptCameraD3D.x, ptCameraD3D.y, ptCameraD3D.z, 0);

	constexpr float kfMinCullDistance = 25.0f; // Objects closer than this aren't culled.
	constexpr float kfCullSizeAtSetting1 = .10f; // 10% of viewport height
	constexpr float kfCullSizeAtSetting10 = .002f; // 0.2% of viewport height
	float fLargestEffectToCull = viewport.Height * InterpolateLinearClamped<float, float>(kfCullSizeAtSetting1, kfCullSizeAtSetting10, iSizeCullBias, 1.0f, 10.0f);

	struct ParticleGroup {
		size_t GetParticleCount() const { return m_iEndParticleIndex - m_iBeginParticleIndex; }
		bool IsSameGroup(Texture* pTexture, bool bAdditiveAlpha) const { return pTexture == m_pTexture && bAdditiveAlpha == m_bAdditiveAlpha; }
		void Init(size_t iParticleIndex, Texture* pTexture, bool bAdditiveAlpha) {
			m_iBeginParticleIndex = iParticleIndex;
			m_iEndParticleIndex = m_iBeginParticleIndex;
			m_pTexture = pTexture;
			m_bAdditiveAlpha = bAdditiveAlpha;
		}

		size_t m_iBeginParticleIndex;
		size_t m_iEndParticleIndex;
		Texture* m_pTexture;
		bool m_bAdditiveAlpha;
	};

	static constexpr size_t knMaxParticleGroups = 64;
	class ParticleGroupList {
	public:
		ParticleGroupList(IDirect3DDevice9* pDevice, IDirect3DVertexBuffer9* pVB, size_t nMaxParticles) :
				m_pDevice(pDevice), m_pVB(pVB), m_nMaxParticles(nMaxParticles) {}
		~ParticleGroupList() {
			m_pVB->Unlock();
		}
		void Flush() {
			if (m_nParticles == 0)
				return;
			m_pVB->Unlock();
			for (size_t i = 0; i < m_nParticleGroups; ++i) {
				const ParticleGroup& grp = m_aParticleGroups[i];
				Texture* pTexture = grp.m_pTexture;
				if (pTexture != m_pLastTexture) {
					IDirect3DTexture9* pD3DTexture = nullptr;
					if (pTexture) {
						pD3DTexture = pTexture->Interface();
						if (!pD3DTexture) {
							ITexture* pITexture = pTexture->GetExternalTexture();
							if (pITexture)
								pD3DTexture = pITexture->GetOrQueueTexture();
						}
					}
					m_pDevice->SetTexture(0, pD3DTexture);
					m_pLastTexture = pTexture;
				}

				bool bAdditiveAlpha = grp.m_bAdditiveAlpha;
				if (int(bAdditiveAlpha) != m_iLastAdditiveAlpha) {
					static_cast<IDirect3DDevice9*>(m_pDevice)->SetRenderState(D3DRS_DESTBLEND, bAdditiveAlpha ? D3DBLEND_ONE : D3DBLEND_INVSRCALPHA);
					m_iLastAdditiveAlpha = bAdditiveAlpha;
				}

				size_t nParticles = grp.GetParticleCount();
				m_pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, grp.m_iBeginParticleIndex * 4, 0, nParticles * 4, 0, nParticles * 2);
			}
			m_nParticleGroups = 0;
			m_nParticles = 0;
		}
		bool AddParticle(Texture* pTexture, bool bAdditiveAlpha, ParticleVertex** ppVert) {
			if (m_nParticles >= m_nMaxParticles) {
				if (m_nMaxParticles == 0)
					return false;
				Flush();
			}

			if (m_nParticleGroups == 0 || !m_aParticleGroups[m_nParticleGroups - 1].IsSameGroup(pTexture, bAdditiveAlpha)) {
				// Need new group
				if (m_nParticleGroups == knMaxParticleGroups)
					Flush(); // Can't fit any more groups.

				if (m_nParticleGroups == 0) {
					HRESULT hr = m_pVB->Lock(0, m_nMaxParticles * 4 * sizeof(ParticleVertex), reinterpret_cast<void**>(&m_pNextVertex), D3DLOCK_DISCARD);
					if (FAILED(hr))
						return false;
				}
				m_aParticleGroups[m_nParticleGroups].Init(m_nParticles, pTexture, bAdditiveAlpha);
				++m_nParticleGroups;
			}
			++m_nParticles;
			m_aParticleGroups[m_nParticleGroups - 1].m_iEndParticleIndex = m_nParticles;
			*ppVert = m_pNextVertex;
			m_pNextVertex += 4;
			return true;
		}

	private:
		IDirect3DDevice9* m_pDevice;
		IDirect3DVertexBuffer9* m_pVB = nullptr;
		ParticleVertex* m_pNextVertex = nullptr;
		Texture* m_pLastTexture = reinterpret_cast<Texture*>(-1);
		uint8_t m_iLastAdditiveAlpha = 2;
		size_t m_nMaxParticles = 0;
		size_t m_nParticleGroups = 0;
		size_t m_nParticles = 0;
		ParticleGroup m_aParticleGroups[knMaxParticleGroups];
	};

	size_t nMaxParticles = std::min<size_t>(m_nMaxVertices / 4, m_nMaxIndices / 6);
	ParticleGroupList particleGroupList(pDevice, m_VB, nMaxParticles);

	pDevice->SetIndices(m_IB);

	// All Particle Effects
	for (const auto& pEffect : m_apEffects) {
		if (!pEffect)
			continue;

		// Size culling
		//pEffect->UpdateAccumulatedBox(dCurrentTime);
		const Simd4f& ptBoxMin = pEffect->GetBBoxMin();
		const Simd4f& ptBoxMax = pEffect->GetBBoxMax();
		Simd4f ptCenter = (ptBoxMin + ptBoxMax) * Simd4f(0.5f);
		Simd4f vBoxSize = ptBoxMax - ptBoxMin;
		Simd4f vBoxHalfSize = vBoxSize * Simd4f(0.5f);
		Simd4f vCameraToBoxCenter = ptCenter - ptCamera;
		Simd4f vCameraToBoxCenterDir = Normalize_Approx(vCameraToBoxCenter);
		Simd4f vClosestBoxCornerAdjustment = Abs(vBoxHalfSize * vCameraToBoxCenterDir);
		Simd4f vCameraToClosestBoxPoint = vCameraToBoxCenter - vClosestBoxCornerAdjustment;
		float fDistanceToBox;
		if (Dot3(vCameraToClosestBoxPoint, vCameraToBoxCenter).GetScalar() > 0)
			fDistanceToBox = vCameraToClosestBoxPoint.Length3_Approx().GetScalar();
		else
			fDistanceToBox = 0;

		float fBoxDiameter = vBoxSize.Length3().GetScalar();
		// Approximate screen size in pixels: fBoxDiameter * mtxProj[1][1] * viewport.Height / (2 * fDistanceToBox)
		if (fDistanceToBox > kfMinCullDistance && fBoxDiameter * mtxProj[1][1] * viewport.Height < 2 * fDistanceToBox * fLargestEffectToCull)
			continue;

		// Frustum culling compares bounding box with each frustum plane.
		// The particle effect is culled if the box is entirely outside any one plane.
		// Future optimization:
		//  Test whether the particle effect is culled by a combination of frustum
		//  planes. E.g. the effect could be outside the top and bottom together,
		//  but not outside either one individually. This is more complicated and is
		//  not a common occurrence so is not worth the effort at this time.
		bool bCulled = false;
		for (const Plane3f& plane : aViewVolumePlanes) {
			if (IsBoxCenterHalfSizeOutsideOfPlane(reinterpret_cast<const Vector3f&>(ptCenter), reinterpret_cast<const Vector3f&>(vBoxHalfSize), plane)) {
				bCulled = true;
				break;
			}
		}
		if (bCulled)
			continue;

#if 0 // Log whether effect is culled for debugging. Remove this code once culling is tested.
static std::map<Effect*,bool> s_mapLastCulled;
bool& bLastCulled = s_mapLastCulled[pEffect];
if( bCulled != bLastCulled ) {
	LogInfo("Effect " << pEffect << " is now " << (bCulled?"":"NOT ") << "culled");
	bLastCulled = bCulled;
}
#endif

		// Sort Particles (updates m_particlesSorted)
		SortParticles(pEffect.get(), doSort);

		// All Sorted Particles
		for (const Particle* pParticle : m_particlesSorted) {
			const Emitter* pEmitter = pParticle->GetEmitter();
			const ParticleType* pParticleType = pEmitter->GetParticleType();
			Texture* pTexture = pParticle->GetCurrentBitmapIndex() < 255 ? pParticleType->m_papTextures[pParticle->GetCurrentBitmapIndex()].get() : 0;
			bool bAdditiveAlpha = pParticleType->bAdditiveAlpha;

			if (!particleGroupList.AddParticle(pTexture, bAdditiveAlpha, &pVert))
				break; // Error. Probably buffer could not be locked.

			float sampleTime = pParticle->CalcSampleTime();
			float halfSize = MetersToFeet(pParticleType->m_sizeFunctionTable.LookUp(sampleTime)) * pParticle->GetSizeMult() * 0.5f;
			float rotation = pParticleType->m_rotFunctionTable.LookUp(sampleTime) * pParticle->GetRotMult();

			SinCosDegrees_Approx(pParticle->GetRotStartDegrees() + rotation, out(rotSin), out(rotCos));

			Simd4f pos0, pos1, pos2, pos3;
			if (pEffect->InObjectSpace()) {
				const Vector4f& emitterOffset = pEmitter->GetOrientedOffset();
				SCREENALIGN_OBJECTSPACE_QUAD2(halfSize, rotCos, rotSin, Simd4f(emitterOffset), Simd4f(pEffect->GetSysPos(), 0), pParticle->GetPositionSSE(), mtxBBoard, pVert, pos0, pos1, pos2, pos3);
			} else {
				SCREENALIGN_WORLDSPACE_QUAD3(halfSize, rotCos, rotSin, mtxBBoard, pParticle->GetPositionSSE(), pos0, pos1, pos2, pos3);
			}

			// Store vertex data using SSE instructions.
			// Store in order of increasing address because this is potentially uncached memory.
			// Use unaligned stores because we can't be 100% sure that base vertex is aligned, although it probably is.
			Simd4f fColor = pParticleType->m_rgbaFunctionTable.LookUp(sampleTime);
			Simd4f fPackedColor = ReinterpretCast<Simd4f>(fColor.PackSatu8());
			static constexpr uint32_t aiColorMask alignas(16)[4] = { 0, 0, 0, 0xffffffff };
			Simd4f colorMask = *reinterpret_cast<const Simd4f*>(aiColorMask);
			Simd4f poscolor0 = IfThenElse(colorMask, fPackedColor, pos0);
			Simd4f poscolor1 = IfThenElse(colorMask, fPackedColor, pos1);
			Simd4f poscolor2 = IfThenElse(colorMask, fPackedColor, pos2);
			Simd4f poscolor3 = IfThenElse(colorMask, fPackedColor, pos3);

			int iXMirror = pParticle->GetMirrorX(); // 1 if mirroring X, else 0
			int iYMirror = pParticle->GetMirrorY(); // 1 if mirroring Y, else 0

			// Duplicates of each UV in high and low halves of vector.
			static constexpr float s_afUV alignas(16)[4][4] = {
				{ 0, 0, 0, 0 },
				{ 0, 1, 0, 1 },
				{ 1, 0, 1, 0 },
				{ 1, 1, 1, 1 }
			};
			int idxUV0 = iXMirror * 2 + iYMirror;
			int idxUV1 = idxUV0 ^ 2;
			int idxUV2 = idxUV0 ^ 1;
			int idxUV3 = idxUV0 ^ 3;
			const Simd4f* pUV = reinterpret_cast<const Simd4f*>(s_afUV);

			_mm_storeu_ps(reinterpret_cast<float*>(pVert) + 0, poscolor0);
			_mm_storeu_ps(reinterpret_cast<float*>(pVert) + 4, MergeLowHalves(pUV[idxUV0], poscolor1));
			_mm_storeu_ps(reinterpret_cast<float*>(pVert) + 8, MergeHighHalves(poscolor1, pUV[idxUV1]));
			_mm_storeu_ps(reinterpret_cast<float*>(pVert) + 12, poscolor2);
			_mm_storeu_ps(reinterpret_cast<float*>(pVert) + 16, MergeLowHalves(pUV[idxUV2], poscolor3));
			_mm_storeu_ps(reinterpret_cast<float*>(pVert) + 20, MergeHighHalves(poscolor3, pUV[idxUV3]));
		}
	}

	particleGroupList.Flush();
	pDevice->SetStreamSource(0, nullptr, 0, 0);
}

ParticleSystem::EffectType* ParticleSystem::FindEffectType(const char* szEffectName) {
	EffectTypeIterator foundEffect = m_effectTypeMap.find(szEffectName);
	return (foundEffect != m_effectTypeMap.end()) ? foundEffect->second : nullptr;
}

} // namespace wkg