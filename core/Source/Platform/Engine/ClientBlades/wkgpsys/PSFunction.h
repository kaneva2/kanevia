///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
#include "Common\KEPUtil\FastMath.h"
#include <Core/Math/KEPMathUtil.h>
#include <Core/Math/Random.h>
#include <Core/Math/Simd.h>
#include <Core/Math/Vector.h>
#include <algorithm>

namespace wkg {

class FileStream;

template <typename T>
class ParticleFunctionTable {
public:
	ParticleFunctionTable() :
			m_iTablePower(0), m_aEntries{ T(0), T(0) } {}

	static const size_t knMaxTableIntervalsPower = 4;
	static const size_t knMaxTableIntervals = 1 << knMaxTableIntervalsPower;

	template <typename U>
	static bool AllZero(U u) { return u == 0; }
	static bool AllZero(const KEP::Simd4f& u) { return u.BooleanMask() == 0; }

	bool Init(unsigned int iTablePower, const T aEntries[]) {
		if (iTablePower > knMaxTableIntervalsPower)
			return false;
		m_iTablePower = iTablePower;
		m_fIntervals = 1 << m_iTablePower;
		size_t nTableEntries = (1 << iTablePower) + 1;
		for (size_t i = 0; i < nTableEntries; ++i)
			m_aEntries[i] = aEntries[i];
		m_aEntries[nTableEntries] = m_aEntries[nTableEntries - 1];
		m_bConstant = true;
		for (size_t i = 1; i < nTableEntries; ++i)
			m_bConstant &= AllZero(m_aEntries[i] - m_aEntries[i - 1]);
		m_bLinear = nTableEntries == 2;
		return true;
	}

	__forceinline T LookUp(float fAt) const {
		ASSERT(fAt >= 0);
		if (m_bConstant)
			return m_aEntries[0];
		//float fIntervals = 1 << m_iTablePower;
		float fIntervals = m_fIntervals;
		//float fi = Clamp(fAt * fIntervals, 0, fIntervals);
		float fi = KEP::Min(fAt * fIntervals, fIntervals);
		float fi_floor = floor(fi);
		int i = KEP::NearestInt(fi_floor);
		float fFraction = fi - fi_floor;
		T fValue = m_aEntries[i] + T(fFraction) * (m_aEntries[i + 1] - m_aEntries[i]);
		return fValue;
	}
	__forceinline KEP::Simd4f LookUp4(float fAt0, float fAt1, float fAt2, float fAt3) const {
		if (m_bConstant)
			return KEP::Simd4f(m_aEntries[0]);

		KEP::Simd4f fAt(fAt0, fAt1, fAt2, fAt3);
		if (m_bLinear) {
			KEP::Simd4f a(m_aEntries[0]);
			KEP::Simd4f b(m_aEntries[1]);
			return a + fAt * (b - a);
		}
		KEP::Simd4f fIntervals(m_fIntervals);
		//float fi = Clamp(fAt * fIntervals, 0, fIntervals);
		KEP::Simd4f fi = Min(fAt * fIntervals, fIntervals);
		KEP::Simd4i32 iFloor = fi.TruncatedI32();
		KEP::Simd4f fFraction = fi - KEP::Simd4f(iFloor);
		//T fValue = m_aEntries[i] + T(fFraction) * (m_aEntries[i+1] - m_aEntries[i]);
		int32_t i0 = reinterpret_cast<int32_t*>(&iFloor)[0];
		int32_t i1 = reinterpret_cast<int32_t*>(&iFloor)[1];
		int32_t i2 = reinterpret_cast<int32_t*>(&iFloor)[2];
		int32_t i3 = reinterpret_cast<int32_t*>(&iFloor)[3];
		KEP::Simd4f fA(m_aEntries[i0], m_aEntries[i1], m_aEntries[i2], m_aEntries[i3]);
		KEP::Simd4f fB(m_aEntries[i0 + 1], m_aEntries[i1 + 1], m_aEntries[i2 + 1], m_aEntries[i3 + 1]);
		return fA + fFraction * (fB - fA);
	}
#if 0
	static std::pair<int,float> CalculateIndex(float fAt) {
		float fi = Clamp(fAt * float(knMaxTableIntervals), 0, knMaxTableIntervals);
		float fi_floor = floor(fi);
		int i = NearestInt(fi_floor);
		float fFraction = fi - fi_floor;
		return std::make_pair(i, fFraction);
	}
	float LookUp(std::pair<int,float> index) {
		int i = index.first >> m_iTablePowerReduction;
		float fFraction = index.second + (index.first - i) * (1 << m_iTablePowerReduction);
		T fValue = m_aEntries[i] + fFraction * (m_aEntries[i+1] - m_aEntries[i]);
		return fValue
	}
#endif
	unsigned int GetTablePower() const { return m_iTablePower; }

private:
	unsigned int m_iTablePower; // Number of intervals in table is 1 << m_iTablePower.
	float m_fIntervals;
	bool m_bConstant;
	bool m_bLinear;
	T m_aEntries[knMaxTableIntervals + 2]; // nIntervals+1 endpoints, plus additional at the end so we can use half-open ranges for each possible input value.
};

template <typename FunctionPointVector>
inline void InitFunctionTable(ParticleFunctionTable<float>& tbl, const FunctionPointVector& functionPoints) {
	static const unsigned int knMaxTableIntervalsPower = ParticleFunctionTable<float>::knMaxTableIntervalsPower;
	int nIntervalsPower = 1;
	for (size_t i = 0; i < functionPoints.size(); ++i) {
		float time = functionPoints[i].time;

		static const int iFixedPointBits = 16;
		float fFixedPoint = time * float(1 << iFixedPointBits);
		uint32_t iFixedPoint = KEP::NearestInt(fFixedPoint);
		if ((iFixedPoint & ((1 << iFixedPointBits) - 1)) == 0)
			continue;
		if (iFixedPoint & ((1 << (iFixedPointBits - knMaxTableIntervalsPower)) - 1)) {
			nIntervalsPower = knMaxTableIntervalsPower;
			break;
		}
		iFixedPoint >>= iFixedPointBits - knMaxTableIntervalsPower;
		for (int j = knMaxTableIntervalsPower; j > nIntervalsPower; --j) {
			if ((1 << (knMaxTableIntervalsPower - j)) & iFixedPoint) {
				nIntervalsPower = j;
				break;
			}
		}
	}
	size_t nIntervals = 1 << nIntervalsPower;
	unsigned int iTablePower = nIntervalsPower;

	float aEntries[(1 << knMaxTableIntervalsPower) + 1];
	for (size_t i = 0; i <= nIntervals; ++i) {
		float fTime = float(i) / nIntervals;
		auto itr = functionPoints.begin();
		for (; itr != functionPoints.end(); ++itr) {
			if (fTime < itr->time)
				break;
		}
		float fValue;
		if (itr == functionPoints.begin()) {
			if (itr != functionPoints.end())
				fValue = itr->value;
			else
				fValue = 0;
		} else if (itr == functionPoints.end()) {
			fValue = (itr - 1)->value;
		} else {
			float fFraction = (fTime - (itr - 1)->time) / (itr->time - (itr - 1)->time);
			fValue = (itr - 1)->value + fFraction * (itr->value - (itr - 1)->value);
		}
		aEntries[i] = fValue;
	}
	tbl.Init(iTablePower, aEntries);
}

inline bool InitFunctionTable(ParticleFunctionTable<KEP::Simd4f>& table, const ParticleFunctionTable<float>* pTables, size_t nTables) {
	if (nTables > 4) {
		// Use only first four tables and return false.
		InitFunctionTable(table, pTables, 4);
		return false;
	}

	unsigned int iTablePower = 0;
	for (size_t i = 0; i < nTables; ++i)
		iTablePower = std::max<unsigned int>(iTablePower, pTables[i].GetTablePower());

	KEP::Simd4f aEntries[ParticleFunctionTable<KEP::Simd4f>::knMaxTableIntervals + 1];
	KEP::Vector4f v(0);
	unsigned int nIntervals = 1 << iTablePower;
	for (size_t i = 0; i <= nIntervals; ++i) {
		for (size_t j = 0; j < nTables; ++j)
			v[j] = pTables[j].LookUp(float(i) / nIntervals);
		aEntries[i].Set(v);
	}

	table.Init(iTablePower, aEntries);
	return true;
}

class ParticleFunction {
public:
	class FunctionPoint {
	public:
		float time;
		float value;
		//float reciprocal_time_diff;
	};

	typedef std::vector<FunctionPoint> FunctionPointVector;
	typedef FunctionPointVector::iterator FPIterator;
	typedef FunctionPointVector::const_iterator FPConstIterator;
	typedef FunctionPointVector::reverse_iterator FPReverseIterator;

	ParticleFunction() :
			m_bUseRandomization(false) {}

	ParticleFunction::FPIterator AddFunctionPoint(float time, float value) {
		return AddFunctionPointToGraph(m_graph, time, value);
	}
	ParticleFunction::FPIterator AddRandomFunctionPoint(float time, float value) {
		return AddFunctionPointToGraph(m_minBoundGraph, time, value);
	}
	void LogFunctionPoints(bool bRandomFunction = false);
	void SetUseRandomization(bool bUse) {
		m_bUseRandomization = bUse;
	}
	bool IsUsingRandomization() const {
		return m_bUseRandomization;
	}
	float Sample(float time, bool allowRandomZero = true, bool allowRandomNegative = true) const;

	bool Read(FileStream&);

	ParticleFunctionTable<float> CreateTable() const {
		ParticleFunctionTable<float> tbl;
		InitFunctionTable(tbl, m_graph);
		return tbl;
	}
	static ParticleFunctionTable<KEP::Simd4f> CreateTable(const ParticleFunction* pf1, const ParticleFunction* pf2, const ParticleFunction* pf3, const ParticleFunction* pf4) {
		const ParticleFunction* apf[4] = { pf1, pf2, pf3, pf4 };
		ParticleFunctionTable<float> aTables[4];
		size_t nTables = 0;
		for (; nTables < 4; ++nTables) {
			if (!apf[nTables])
				break;
			aTables[nTables] = apf[nTables]->CreateTable();
		}

		ParticleFunctionTable<KEP::Simd4f> tbl;
		InitFunctionTable(tbl, aTables, nTables);
		return tbl;
	}

	// Make sure graphs aren't empty. Set m_bConstant and m_bMinConstant as appropriate.
	void Finalize() {
		if (m_graph.empty())
			m_graph.push_back(FunctionPoint{ 0, 0 });
		auto IsConstant = [](FunctionPointVector& graph) {
			for (size_t i = 1; i < graph.size(); ++i) {
				if (graph[i].value != graph[i - 1].value)
					return false;
			}
			return true;
		};
		m_bConstant = IsConstant(m_graph);

		if (m_bUseRandomization) {
			if (m_minBoundGraph.empty())
				m_minBoundGraph.push_back(FunctionPoint{ 0, 0 });
			m_bMinConstant = IsConstant(m_minBoundGraph);
		}
	}

	float MaxValue() const {
		float mx = -FLT_MAX;
		for (const FunctionPoint& fp : m_graph)
			mx = fp.value > mx ? fp.value : mx;
		return mx;
	}

private:
	static ParticleFunction::FPIterator AddFunctionPointToGraph(FunctionPointVector& fpv, float time, float value);
	static float SampleGraph(const FunctionPointVector& graph, float time, bool bConstant);

	FunctionPointVector m_graph;

	bool m_bUseRandomization;
	bool m_bConstant;
	bool m_bMinConstant;
	FunctionPointVector m_minBoundGraph; //for use in randomization. m_graph holds the max bound.
};

__forceinline float ParticleFunction::SampleGraph(const FunctionPointVector& graph, float time, bool bConstant) {
	if (bConstant)
		return graph.front().value;

	size_t idxLow = 0;
	size_t szRange = graph.size() - 1;
	while (szRange > 1) {
		size_t szHalf = szRange / 2;
		size_t idxMid = idxLow + szHalf;
		if (graph[idxMid].time < time) {
			idxLow = idxMid;
			szRange = szRange - szHalf;
		} else {
			szRange = szHalf;
		}
	}
	const FunctionPoint& fpA = graph[idxLow];
	const FunctionPoint& fpB = graph[idxLow + 1];
	float timeDiff = time - fpA.time;
	float timeDiffBA = fpB.time - fpA.time;
	float valueDiffBA = fpB.value - fpA.value;
	float fraction = timeDiff * KEP::Reciprocal_Approx(timeDiffBA);
	return fpA.value + fraction * valueDiffBA;
}

__forceinline float ParticleFunction::Sample(float time, bool allowRandomZero /* = true */, bool allowRandomNegative /* = true */) const {
	FPConstIterator iFP = m_graph.begin();
	FPConstIterator endFP = m_graph.end();
	FPConstIterator prevFP = endFP;
	float retVal = SampleGraph(m_graph, time, m_bConstant);
	if (m_bUseRandomization) {
		float minVal = SampleGraph(m_minBoundGraph, time, m_bMinConstant);
		float intervalSize = retVal - minVal;
		if (intervalSize != 0) {
			retVal = minVal + KEP::RandomFloatClosed() * intervalSize;

			if (!allowRandomNegative && retVal < 0)
				retVal = 0.f;
			if (!allowRandomZero && retVal == 0)
				retVal = .001f;
		}
	}

	return retVal;
}

} // namespace wkg
