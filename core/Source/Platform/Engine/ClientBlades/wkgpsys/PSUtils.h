///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include <Core/Math/Random.h>

namespace wkg {

class FileStream;

inline real CalcRandFloat(real minVal, real maxVal) {
	real interval = maxVal - minVal;
	return minVal + interval * KEP::RandomFloatClosed();
}

#define WRITEREAL(fval) \
	floatVal = (fval);  \
	ar.Write(&floatVal, sizeof(float))
#define WRITEBOOL(bval) \
	uint8Val = (bval);  \
	ar.Write(&uint8Val, sizeof(uint8))
#define READREAL(rval)     \
	if (f.Read(&floatVal)) \
		rval = floatVal;   \
	else                   \
		goto fileError
#define READBOOL(bval)        \
	if (f.Read(&uint8Val))    \
		bval = uint8Val != 0; \
	else                      \
		goto fileError

bool ReadString(FileStream& f, char* str, size_t* len);

} // namespace wkg
