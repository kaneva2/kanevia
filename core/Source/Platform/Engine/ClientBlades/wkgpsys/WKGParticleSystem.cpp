///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#define BUILD_BLADE

#include "KEPConstants.h"
#include "wkgtypes.h"
#include "device.h"
#include "renderstate.h"
#include "particlesystem.h"
#include "pseffect.h"
#include "texturefactory.h"
#include "vertexshader.h"
#include "vsfactory.h"
#include "iclientparticleblade.h"
#include "IClientEngine.h"
#include "IDispatcher.h"
#include "PSParticle.h"
#include "PSEmitter.h"
#include "../../Common/include/CoreStatic/ParticleDefinition.h"
#include "LogHelper.h"
#include <Common/include/CoreStatic/ITexture.h>

#include <TinyXML/tinyxml.h>

using namespace wkg;

namespace KEP {

static LogInstance("Instance");

class WKGParticleSystem : public KEP::IClientParticleBlade {
public:
	WKGParticleSystem();
	~WKGParticleSystem();

	virtual bool Initialize();
	virtual bool InitializeParticleBlade(const char* filename);

	virtual bool Destroy();

	virtual ULONG Version() const {
		return 0x1000;
	}
	virtual const std::string& GetName() const {
		return m_name;
	}

	void Render() override;

	virtual size_t GetNumEffects() const;
	const char* GetEffectName(size_t idx) const;

	virtual KEP::EffectHandle CreateEffect(const char* effect_name,
		real x, real y, real z, real qx, real qy, real qz, real qw);

	virtual void RemoveEffect(KEP::EffectHandle handle);
	virtual void ClearRuntimeEffects();
	virtual void RemoveAllEffects();

	virtual void SetEffectPosition(KEP::EffectHandle effect_handle, real x, real y, real z);
	virtual void SetEffectOrientation(KEP::EffectHandle effect_handle, real qx, real qy, real qz, real qw);
	virtual void SetEffectWaitingToDie(KEP::EffectHandle effect_handle, bool regenerate);

	virtual const char* GetDefinitionExtension() {
		return m_file_extension.c_str();
	}
	virtual const char* GetProviderDescription() {
		return m_provider_description.c_str();
	}

	bool LoadParticleDefinitions(const char* filename);

	virtual bool LoadKEPDefinitionsFromBuffer(const char* effect_name, EffectDef* pDef);

	virtual void OnLostDevice();
	virtual void OnResetDevice();
	virtual void CleanupEffects();

	virtual void FreeTextures();

protected:
	virtual bool Initialize(TiXmlNode* particle_root);

	bool LoadFunctionFromKEPDefinition(ParticleFunction& particleFunc, const AnimatedFloatValue* pDef, const AnimatedFloatValue* pVarDef = NULL);
	void ResolveExternalTextures(wkg::ParticleSystem::EffectHandle handle);
	void CreateVertexAndIndexBuffers();

protected:
	std::string m_name;

	wkg::Device* m_device;

	wkg::ParticleSystem* m_particle_system;
	CComPtr<IDirect3DVertexBuffer9> m_dynamic_vb;
	CComPtr<IDirect3DIndexBuffer9> m_dynamic_ib;

	IDirect3DStateBlock9* m_state_block;

	std::map<int, wkg::ParticleSystem::EffectHandle> m_external_handle_map;
	int m_external_counter;

	std::string m_file_extension;
	std::string m_provider_description;
};

WKGParticleSystem::WKGParticleSystem() :
		m_name("WKGParticleSystem"),
		m_device(0),
		m_particle_system(0),
		m_dynamic_vb(0),
		m_dynamic_ib(0),
		m_state_block(0),
		m_external_counter(0),
		m_file_extension("wkp"),
		m_provider_description("WKG Particles") {
	IClientRenderBlade::m_bladeDepth = RequiresZUse;
}

WKGParticleSystem::~WKGParticleSystem() {
}

bool WKGParticleSystem::LoadParticleDefinitions(const char* filename) {
	return m_particle_system->LoadDefinitions(filename);
}

bool WKGParticleSystem::InitializeParticleBlade(const char* filename) {
	TiXmlDocument xml_doc;

	if (xml_doc.LoadFile(filename)) {
		TiXmlElement* root = xml_doc.FirstChildElement("ParticleSystems");
		return Initialize(root);
	}

	return false;
}

bool WKGParticleSystem::Initialize(TiXmlNode* particle_root) {
	if (!particle_root)
		return false;

	// given the paricle root, iterate over the children.
	//  they generally should be particle instances.
	TiXmlNode* child = 0;
	for (child = particle_root->FirstChild(); child; child = child->NextSibling()) {
		const char* val = child->Value();
		if (!_strcmpi(val, "ParticleSystem")) {
			TiXmlElement* elem = child->ToElement();

			const char* x_str = elem->Attribute("x");
			const char* y_str = elem->Attribute("y");
			const char* z_str = elem->Attribute("z");

			const char* name = elem->Attribute("name");

			real x = wkg::atof(x_str);
			real y = wkg::atof(y_str);
			real z = wkg::atof(z_str);

			bool didCreate = m_particle_system->CreateEffect(name, Vector3f(x, y, z), Quaternionf(0.0f, 0.0f, 0.0f, 1.0f));

			if (didCreate) {
				//send an event here
				IDispatcher* dispatcher = m_engine->GetDispatcher();
				if (dispatcher) {
					EVENT_ID eventId = dispatcher->GetEventId("ParticleEffectCreatedEvent");
					if (eventId != BAD_EVENT_ID) {
						if (dispatcher->HasHandler(eventId)) {
							IEvent* e = dispatcher->MakeEvent(eventId);
							(*e->OutBuffer()) << name;
							dispatcher->QueueEvent(e);
						}
					}
				}
			}
		} else if (!_strcmpi(val, "ParticleLibrary")) {
			TiXmlElement* e = child->ToElement();
			const char* lib_name = e->Attribute("name");
			m_particle_system->LoadDefinitions(lib_name);
		}
	}

	return true;
}

bool WKGParticleSystem::Destroy() {
	return true;
}

size_t WKGParticleSystem::GetNumEffects() const {
	return m_particle_system->GetNumEffects();
}

const char* WKGParticleSystem::GetEffectName(size_t idx) const {
	return m_particle_system->GetEffectName(idx);
}

KEP::EffectHandle WKGParticleSystem::CreateEffect(
	const char* effect_name,
	real x, real y, real z,
	real qx, real qy, real qz, real qw) {
	wkg::ParticleSystem::EffectHandle handle;
	bool didCreate = m_particle_system->CreateEffect(effect_name,
		Vector3f(x, y, z),
		Quaternionf(qx, qy, qz, qw),
		&handle);

	//send an event here
	IDispatcher* dispatcher = m_engine->GetDispatcher();
	if (didCreate && dispatcher) {
		EVENT_ID eventId = dispatcher->GetEventId("ParticleEffectCreatedEvent");
		if (eventId != BAD_EVENT_ID) {
			if (dispatcher->HasHandler(eventId)) {
				IEvent* e = dispatcher->MakeEvent(eventId);
				(*e->OutBuffer()) << effect_name;
				dispatcher->QueueEvent(e);
			}
		}
	}

	if (didCreate) {
		m_external_handle_map[m_external_counter] = handle;
		ResolveExternalTextures(handle);
		m_external_counter++;
	}

	return m_external_counter - 1;
}

// Method removes the given effect from the particle system. All currently
// rendered particles associated with the given effect will also be destroyed.
// Parameters:
//  handle  - Handle to the effect to be removed.
void WKGParticleSystem::RemoveEffect(KEP::EffectHandle handle) {
	// * NOTE * //
	// The given handle is an iterator to an list< Effect* >. This
	// is not a good way to do things. It is very possible that the
	// iterator or the handle may not be valid and there is no way to
	// check the validity of the iterator.

	// Take the given handle and attempt to find it the internal handle
	// that it is mapped to.
	auto it = m_external_handle_map.find(handle);
	if (it == m_external_handle_map.end())
		return;

	wkg::ParticleSystem::EffectHandle internal_handle;
	internal_handle = it->second;
	m_particle_system->RemoveEffect(internal_handle);
	m_external_handle_map.erase(it);
}

void WKGParticleSystem::ClearRuntimeEffects() {
	m_particle_system->ClearEffects();
	m_external_handle_map.clear();
}

void WKGParticleSystem::RemoveAllEffects() {
	m_particle_system->ClearDefinitions();
	m_particle_system->ClearEffects();
	m_external_handle_map.clear();
}

void WKGParticleSystem::SetEffectPosition(KEP::EffectHandle effect_handle, real x, real y, real z) {
	auto it = m_external_handle_map.find(effect_handle);
	if (it == m_external_handle_map.end())
		return;

	wkg::ParticleSystem::EffectHandle handle = it->second;
	m_particle_system->SetEffectPosition(handle, x, y, z);
}

void WKGParticleSystem::SetEffectOrientation(KEP::EffectHandle effect_handle, real qx, real qy, real qz, real qw) {
	auto it = m_external_handle_map.find(effect_handle);
	if (it == m_external_handle_map.end())
		return;

	wkg::ParticleSystem::EffectHandle handle = it->second;
	m_particle_system->SetEffectOrientation(handle, qx, qy, qz, qw);
}

////////////////////////////////////////////////////////////////////////////////
//
// Method sets the flag, for the given effect, that tells the effect whether
// or not it should destroy itself once it is finished.
//
// Parameters:
//  effect_handle   - Handle to the effect that is to have its destroy flag set.
//  waitingToDie    - If 'true' then the given effect will stop emitting
//                    particles and once all of the effects current 'living'
//                    particles 'die' then the effect will be removed.
//
void WKGParticleSystem::SetEffectWaitingToDie(KEP::EffectHandle effect_handle, bool waitingToDie) {
	// * NOTE * //
	// The given handle is an iterator to an list< Effect* >. This
	// is not a good way to do things. It is very possible that the
	// iterator or the handle may not be valid and there is no way to
	// check the validity of the iterator.
	auto it = m_external_handle_map.find(effect_handle);
	if (it == m_external_handle_map.end())
		return;

	wkg::ParticleSystem::EffectHandle handle = it->second;
	(handle)->SetWaitingToDie(waitingToDie);
}

void WKGParticleSystem::CleanupEffects() {
	//	This function deletes the effect memory, and removes the effect from the list
	for (auto iter = m_external_handle_map.begin(); iter != m_external_handle_map.end();) {
		wkg::ParticleSystem::EffectHandle handle = iter->second;
		wkg::ParticleSystem::Effect* p = (handle);
		if (p->GetShouldDelete()) {
			m_particle_system->RemoveEffect(handle);
			iter = m_external_handle_map.erase(iter);
			continue;
		}
		iter++;
	}
}

void WKGParticleSystem::FreeTextures() {
	m_particle_system->FreeTextures();
}

void WKGParticleSystem::ResolveExternalTextures(wkg::ParticleSystem::EffectHandle handle) {
	wkg::ParticleSystem::Effect* p = (handle);
	if (!p)
		return;

	wkg::ParticleSystem::EffectType* pType = p->GetEffectType();
	if (!pType)
		return;

	for (uint8 i = 0; i < pType->numEmitters; i++) {
		if (!pType->particleTypes[i])
			continue;

		for (uint8 texLoop = 0; texLoop < pType->particleTypes[i]->m_nTextures; texLoop++) {
			wkg::ParticleSystem::ParticleType* pParticleType = pType->particleTypes[i];
			wkg::Texture* pTexture = pParticleType->m_papTextures[texLoop].get();
			if (!pTexture)
				continue;

			if (pTexture->Interface())
				continue;

			uint32 externalTextureKey = pTexture->GetExternalKey();
			if (externalTextureKey == 0)
				continue;

			KEP::ITexture* pITexture = pTexture->GetExternalTexture();
			if (!pITexture) {
				pITexture = m_engine->GetTextureIfLoadedOrQueueToLoad(externalTextureKey);
				pTexture->SetExternalTexture(pITexture);
			}

			pTexture->SetExternalD3DTexture(pITexture->GetOrQueueTexture());
		}
	}
}

bool WKGParticleSystem::Initialize() {
	IDirect3DDevice9* d = m_engine->GetD3DDevice();

	wkg::Device::Init(&m_device, 0, false, 800, 600, d);

	//new wkg::PSFactory();
	new wkg::VSFactory();

	wkg::VertexShader* vs;
	vs = new wkg::VertexShader();
	vs->Init(FVF_PARTICLEVERTEX);

	wkg::VSFactory::Instance()->RegisterShader(vs, "ParticleSysFVF");

	new wkg::TextureFactory();
	m_particle_system = new wkg::ParticleSystem();

	CreateVertexAndIndexBuffers();

	//
	IDispatcher* p = m_engine->GetDispatcher();
	IEvent::SetEncodingFactory(p->EncodingFactory());

	return true;
}

void WKGParticleSystem::OnLostDevice() {
	if (!wkg::Device::Instance())
		return;

	if (!m_particle_system)
		return;

	m_dynamic_vb = 0;
	m_dynamic_ib = 0;
	m_particle_system->SetVBIB(0, 0);
}

void WKGParticleSystem::OnResetDevice() {
	if (!wkg::Device::Instance())
		return;

	if (!m_particle_system)
		return;

	m_dynamic_vb = 0;
	m_dynamic_ib = 0;
	CreateVertexAndIndexBuffers();
}

void WKGParticleSystem::CreateVertexAndIndexBuffers() {
	HRESULT hr;
	static const size_t knMaxParticles = (128 * 1024) / sizeof(ParticleVertex); // sizeof(ParticleVertex) == 24. So 5461 particles
	static const size_t knMaxVertices = 4 * knMaxParticles;
	static const size_t knMaxIndices = knMaxVertices * 6;

	hr = wkg::Device::Instance()->CreateVertexBuffer(
		knMaxVertices * sizeof(ParticleVertex), D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, &m_dynamic_vb, 0);

	hr = wkg::Device::Instance()->CreateIndexBuffer(
		knMaxIndices * sizeof(uint16_t), D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &m_dynamic_ib, 0);

	if (m_dynamic_ib) {
		uint16_t* pIndices = nullptr;
		hr = m_dynamic_ib->Lock(0, knMaxIndices, reinterpret_cast<void**>(&pIndices), D3DLOCK_DISCARD);
		if (SUCCEEDED(hr)) {
			for (size_t i = 0; i < knMaxParticles; ++i) {
				pIndices[i * 6 + 0] = static_cast<uint16_t>(i * 4 + 0);
				pIndices[i * 6 + 1] = static_cast<uint16_t>(i * 4 + 1);
				pIndices[i * 6 + 2] = static_cast<uint16_t>(i * 4 + 2);
				pIndices[i * 6 + 3] = static_cast<uint16_t>(i * 4 + 3);
				pIndices[i * 6 + 4] = static_cast<uint16_t>(i * 4 + 2);
				pIndices[i * 6 + 5] = static_cast<uint16_t>(i * 4 + 1);
			}
			m_dynamic_ib->Unlock();
		}
	}

	m_particle_system->SetVBIB(m_dynamic_vb, m_dynamic_ib);
}

void WKGParticleSystem::Render() {
	// Particles Enabled ?
	if (!m_renderOptions.m_enabled)
		return;

	// Get Render Options
	auto fps = m_renderOptions.m_fps;
	auto bias = m_renderOptions.m_bias;
	auto biasAuto = m_renderOptions.m_biasAuto;

	// ED-5427 - Auto Graphics Options
	// Automatically adjust graphics options based on FPS (2hz max)
	// Priorities:
	// 1. Particle Bias (10 -> 1)
	static int s_bias = 10;
	static Timer s_timerAuto;
	bool tooSlow = (fps < 10.0);
	bool tooFast = (fps > 15.0);
	bool tooSoon = (s_timerAuto.ElapsedMs() < 500.0);
	if (!tooSoon && tooSlow) {
		// Too Slow - Decrease Quality
		if (biasAuto && (s_bias > 1)) {
			--s_bias;
			LogInfo("SLOW - fps=" << fps << " bias=" << s_bias);
			s_timerAuto.Reset();
		}
	} else if (!tooSoon && tooFast) {
		// Too Fast - Increase Quality
		if (biasAuto && (s_bias < 10)) {
			++s_bias;
			LogInfo("FAST - fps=" << fps << " bias=" << s_bias);
			s_timerAuto.Reset();
		}
	}
	if (biasAuto)
		bias = s_bias;
	else
		s_bias = bias;

	// Determine Particle Render Delay
	static Timer s_timerRender;
	TimeMs timeUpdateMs = c_frameTimeTargetMs * ((10 - bias) + 1);
	bool update = (s_timerRender.ElapsedMs() >= timeUpdateMs);
	if (update)
		s_timerRender.Reset();

	D3DXMATRIX view;
	m_engine->GetViewMatrix(reinterpret_cast<Matrix44f*>(&view));
	wkg::Device::Instance()->SetTransform(D3DTS_VIEW, &view);

	D3DXMATRIX proj;
	m_engine->GetProjectionMatrix(reinterpret_cast<Matrix44f*>(&proj));

	wkg::Device::Instance()->SetTransform(D3DTS_WORLD, &Matrix44f::GetIdentity());

	Vector3f camPos;
	D3DXMATRIX invview;
	D3DXMatrixInverse(&invview, NULL, &view);
	camPos.x = invview._41;
	camPos.y = invview._42;
	camPos.z = invview._43;

	m_particle_system->UpdateCameraPosition(camPos);

	if (update) {
		if (m_particle_system->Update(c_frameTimeTargetMs))
			CleanupEffects();
	}

	bool doSort = true; // drf - doubles frame rates in k-town for integrated graphics
	m_particle_system->Render(doSort, m_renderOptions.m_DOCullBias);
}

bool WKGParticleSystem::LoadKEPDefinitionsFromBuffer(const char* effect_name, EffectDef* pDef) {
	if (effect_name == NULL || pDef == NULL)
		return false;

	// Already Loaded ?
	bool isNew = false;
	ParticleSystem::EffectType* pEffectType = m_particle_system->FindEffectType(effect_name);
	if (!pEffectType) {
		pEffectType = new ParticleSystem::EffectType();
		isNew = true;
	} else {
		// currently don't support importing into an existing effect with more than one emitters/particle types
		// UGC PS should only carries one pair of emitter + particle type
		if (pEffectType->numEmitters > 1)
			return false;
	}

	std::string sEffectName(effect_name);

	ParticleSystem::ParticleType* pParticleType = NULL;
	ParticleSystem::EmitterType* pEmitterType = NULL;

	if (pEffectType->numEmitters == 0) {
		// empty effect, creating a new particle type and effect type
		pParticleType = new ParticleSystem::ParticleType();
		m_particle_system->m_particleTypeMap.insert(make_pair(sEffectName + "_Particle0", pParticleType));
		pEmitterType = new ParticleSystem::EmitterType();
		m_particle_system->m_emitterTypeMap.insert(make_pair(sEffectName + "_Emitter0", pEmitterType));

		pEffectType->numEmitters = 1;
		pEffectType->particleTypes = new ParticleSystem::ParticleType*[1];
		pEffectType->particleTypes[0] = pParticleType;
		pEffectType->emitterTypes = new ParticleSystem::EmitterType*[1];
		pEffectType->emitterTypes[0] = pEmitterType;
		pEffectType->posOffsets = new Vector3f[1];
		pEffectType->posOffsets[0].x = 0;
		pEffectType->posOffsets[0].y = 0;
		pEffectType->posOffsets[0].z = 0;
	} else {
		pParticleType = pEffectType->particleTypes[0];
		pEmitterType = pEffectType->emitterTypes[0];
	}

	LoadFunctionFromKEPDefinition(pParticleType->rFunc, &pDef->particle.colorFunc.r);
	LoadFunctionFromKEPDefinition(pParticleType->gFunc, &pDef->particle.colorFunc.g);
	LoadFunctionFromKEPDefinition(pParticleType->bFunc, &pDef->particle.colorFunc.b);
	LoadFunctionFromKEPDefinition(pParticleType->aFunc, &pDef->particle.colorFunc.a);
	LoadFunctionFromKEPDefinition(pParticleType->sizeFunc, &pDef->particle.sizeFunc);
	LoadFunctionFromKEPDefinition(pParticleType->rotFunc, &pDef->particle.rotation.angleFunc);

	pParticleType->CreateFunctionTables();

	pParticleType->m_nTextures = (uint8)std::min(pDef->particle.textures.fileNames.size(), 255U);
	pParticleType->m_fReciprocalNumTextures = pParticleType->m_nTextures > 0 ? 1.0f / pParticleType->m_nTextures : 0.0f;
	pParticleType->m_papTextures = std::make_unique<std::unique_ptr<Texture, ParticleSystem::ParticleType::TextureReleaser>[]>(pParticleType->m_nTextures);

	for (size_t texLoop = 0; texLoop < pDef->particle.textures.fileNames.size() && texLoop < pDef->particle.textures.handles.size(); texLoop++) {
		pParticleType->m_papTextures[texLoop].reset(new wkg::Texture());
		pParticleType->m_papTextures[texLoop]->LoadD3DExternal(
			pDef->particle.textures.fileNames[texLoop].c_str(),
			strtoul(pDef->particle.textures.handles[texLoop].c_str(), NULL, 10));
	}

	if (pDef->particle.textures.customFrameRate != 0) {
		pParticleType->msPerFrame = MS_PER_SEC / pDef->particle.textures.customFrameRate;
		pParticleType->m_fTexturesPerMS = 1.0f / pParticleType->msPerFrame;
		pParticleType->bLooping = true;
	} else {
		pParticleType->msPerFrame = 0;
		pParticleType->m_fTexturesPerMS = 0;
		pParticleType->bLooping = false;
	}

	pParticleType->bRandomStartFrame = pDef->particle.textures.randomInitFrame;
	pParticleType->bRandomXMirror = pDef->particle.textures.randomXMirroring;
	pParticleType->bRandomYMirror = pDef->particle.textures.randomYMirroring;
	pParticleType->bAdditiveAlpha = pDef->particle.textures.additiveAlpha;
	pParticleType->gravityCoef = pDef->particle.gravityCoeff;
	pParticleType->resistanceCoef = pDef->particle.dampingCoeff;
	pParticleType->resistanceCoef = -pParticleType->resistanceCoef;
	pParticleType->m_bConstantVelocity = pParticleType->gravityCoef == 0 && pParticleType->resistanceCoef == 0;
	pParticleType->startRotMin = (real)(pDef->particle.rotation.initAngle - pDef->particle.rotation.initAngleVar);
	pParticleType->startRotMax = (real)(pDef->particle.rotation.initAngle + pDef->particle.rotation.initAngleVar);

	switch (pDef->particle.dampingByVelocity) {
		case QUADRATIC:
			pParticleType->velResistance = ParticleSystem::ParticleType::QUADRATIC;
			break;

		default:
			pParticleType->velResistance = ParticleSystem::ParticleType::LINEAR;
			break;
	}

	switch (pDef->particle.dampingBySize) {
		case QUADRATIC:
			pParticleType->sizeResistance = ParticleSystem::ParticleType::QUADRATIC;
			break;

		default:
			pParticleType->sizeResistance = ParticleSystem::ParticleType::LINEAR;
			break;
	}

	pEmitterType->baseLifetime = (TimeMs)(pDef->emitterLife * MS_PER_SEC);
	pEmitterType->randExtraLifetime = (uint32)(pDef->emitterLifeVar * MS_PER_SEC);
	pEmitterType->bLoopCycle = pDef->loopEmitter;
	LoadFunctionFromKEPDefinition(pEmitterType->emitFunc, &pDef->emitter.birthRateFunc, &pDef->emitter.birthRateVarFunc);
	LoadFunctionFromKEPDefinition(pEmitterType->lifeFunc, &pDef->emitter.particleLifeFunc, &pDef->emitter.particleLifeVarFunc);
	LoadFunctionFromKEPDefinition(pEmitterType->xVelFunc, &pDef->emitter.initVelFunc.x, &pDef->emitter.initVelVarFunc.x);
	LoadFunctionFromKEPDefinition(pEmitterType->yVelFunc, &pDef->emitter.initVelFunc.y, &pDef->emitter.initVelVarFunc.y);
	LoadFunctionFromKEPDefinition(pEmitterType->zVelFunc, &pDef->emitter.initVelFunc.z, &pDef->emitter.initVelVarFunc.z);
	LoadFunctionFromKEPDefinition(pEmitterType->sizeMultFunc, &pDef->emitter.ptcSizeCoeffFunc, &pDef->emitter.ptcSizeCoeffVarFunc);
	LoadFunctionFromKEPDefinition(pEmitterType->rotMultFunc, &pDef->emitter.ptcRotCoeffFunc, &pDef->emitter.ptcRotCoeffVarFunc);

	switch (pDef->emitter.shape) {
		case SPHERE:
			pEmitterType->shapeType = ParticleSystem::EmitterType::EMITTER_SPHERE;
			pEmitterType->shapeData.sphere.m_center = pDef->emitter.m_center;
			pEmitterType->shapeData.sphere.hotspot = pDef->emitter.m_size.x;
			pEmitterType->shapeData.sphere.falloff = 0.0f;
			break;

		default:
			pEmitterType->shapeType = ParticleSystem::EmitterType::EMITTER_CUBE;
			pEmitterType->shapeData.square.m_min.SubVector<3>() = pDef->emitter.m_center - pDef->emitter.m_size;
			pEmitterType->shapeData.square.m_max.SubVector<3>() = pDef->emitter.m_center + pDef->emitter.m_size;
			break;
	}

	pEffectType->emitIntoObjectspace = pDef->emitIntoObjectSpace;

	if (isNew)
		m_particle_system->m_effectTypeMap.insert(make_pair(sEffectName, pEffectType));

	return true;
}

bool WKGParticleSystem::LoadFunctionFromKEPDefinition(ParticleFunction& particleFunc, const AnimatedFloatValue* pDef, const AnimatedFloatValue* pVarDef /*=NULL*/) {
	if (!pDef || pVarDef && !pVarDef->empty() && pDef->size() != pVarDef->size())
		return false;

	ParticleFunction tmp;
	if (pVarDef && !pVarDef->empty()) {
		tmp.SetUseRandomization(true);
		for (AnimatedFloatValue::const_iterator it = pDef->begin(), itVar = pVarDef->begin(); it != pDef->end() && itVar != pVarDef->end(); ++it, ++itVar) {
			float time = it->first / MS_PER_SEC;
			float timeVar = itVar->first / MS_PER_SEC;
			timeVar; // DRF - potententially unreferenced
			ASSERT(time == timeVar);

			float value = it->second;
			float valueVar = itVar->second;

			tmp.AddFunctionPoint(time, value + valueVar);
			tmp.AddRandomFunctionPoint(time, value - valueVar);
		}
	} else {
		tmp.SetUseRandomization(false);
		for (AnimatedFloatValue::const_iterator it = pDef->begin(); it != pDef->end(); ++it) {
			float time = it->first / MS_PER_SEC;
			float value = it->second;
			tmp.AddFunctionPoint(time, value);
		}
	}
	tmp.Finalize();

	particleFunc = tmp;
	return true;
}

extern "C" BLADE_EXPORT
	size_t
	GetNumInterfaces() {
	return 1;
}

extern "C" BLADE_EXPORT
	IClientBlade*
	Create(size_t idx) {
	if (0 == idx)
		return new WKGParticleSystem();
	else
		return 0;
}

extern "C" BLADE_EXPORT void Destroy(IClientBlade* blade) {
	delete blade;
}

} // namespace KEP

BOOL APIENTRY DllMain(HANDLE /*hModule*/,
	DWORD /*ul_reason_for_call*/,
	LPVOID /*lpReserved*/
) {
	return TRUE;
}
