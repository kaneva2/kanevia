///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "wkgtypes.h"
#include "PSUtils.h"
#include "FileStream.h"

namespace wkg {

bool ReadString(FileStream& f, char* str, size_t* len) {
	uint8 uint8Val;

	if (!f.Read(&uint8Val) ||
		f.Read((uint8*)str, uint8Val) != sizeof(uint8) * uint8Val)
		return false;

	*len = uint8Val;
	str[*len] = 0;

	return true;
}

} // namespace wkg