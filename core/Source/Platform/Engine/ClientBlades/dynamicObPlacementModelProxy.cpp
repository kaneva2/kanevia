///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include <d3d9.h>
#include "cexmeshclass.h"
#include "creferencelibrary.h"
#include "CWldObject.h"
#include "RuntimeSkeleton.h"
#include "compileopts.h"
#include "wkgtypes.h"

#include <vector>

#include "DynamicObj.h"
#include "DynamicObPlacementModelProxy.h"
#include "CSkeletonObject.h"
#include "ReMesh.h"

namespace KEP {

DynamicPlacementModelProxy::DynamicPlacementModelProxy(CDynamicPlacementObj* obj,
	IDirect3DDevice9* d,
	CReferenceObjList* libraryReferenceDB) :
		m_dynamic_obj_placement(obj),
		m_device(d),
		m_libraryReferenceDB(libraryReferenceDB) {
	type = DYNAMIC_PLACEMENT_PROXY;
}

void DynamicPlacementModelProxy::Scale(float sx, float sy, float sz) {
}

void DynamicPlacementModelProxy::Move(float x, float y, float z) {
}

void DynamicPlacementModelProxy::Rotate(float rx, float ry, float rz) {
}

void DynamicPlacementModelProxy::GetName(std::string& aName) {
	// Note if the placements have the DO name from the server, we should return the name
	// off the placement here.
	if (m_dynamic_obj_placement && m_dynamic_obj_placement->GetBaseObj()) {
		aName = m_dynamic_obj_placement->GetBaseObj()->getName();
	}
}

bool DynamicPlacementModelProxy::Intersect(Vector3f& from, Vector3f& to, BOOL testAlpha) {
	if (m_dynamic_obj_placement && m_dynamic_obj_placement->GetBaseObj()) {
		Vector3f origin(from), direction((to - from).Normalize()), normal;
		float distance = FLT_MAX;

		return m_dynamic_obj_placement->IntersectRay(origin, direction, &distance, &to, &normal, testAlpha == TRUE);
	}

	return false;
}

Matrix44f
DynamicPlacementModelProxy::GetTransform() {
	return m_dynamic_obj_placement->getSkeleton()->getWorldMatrix();
}

void DynamicPlacementModelProxy::GetBoundingBox(Vector4f& min, Vector4f& max) {
}

} // namespace KEP