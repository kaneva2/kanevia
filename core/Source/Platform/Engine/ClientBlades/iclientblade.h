///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "bladeexports.h"
#include <string>

#include "Core/Util/HighPrecisionTime.h"

namespace KEP {

class IClientEngine;

class IClientBlade {
public:
	virtual ~IClientBlade() {}

	// this is called after the environment has been set up, primarily d3d
	virtual bool PreInitialize() { return true; }

	virtual bool Initialize() = 0;

	virtual bool PostInitialize() { return true; }

	virtual unsigned long Version() const = 0;

	virtual const std::string& GetName() const = 0;

	virtual void SetEngineInterface(IClientEngine* e) { m_engine = e; }
	virtual IClientEngine* GetEngineInterface() { return m_engine; }

	virtual size_t GetNumDependencies() const { return 0; }
	virtual std::string GetDependency(size_t /*idx*/) const { return ""; }

	virtual void OnLostDevice() {}

	virtual void OnResetDevice() {}

	virtual void OnUpdate() {}

	virtual void OnUnLoadScene() {}
	virtual void OnLoadScene() {}

protected:
	IClientEngine* m_engine;
};

} // namespace KEP