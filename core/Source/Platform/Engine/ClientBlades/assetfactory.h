///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <map>
#include <typeinfo.h>

#include "iassetfactory.h"
#include "singleton.h"
#include <assert.h>

namespace KEP {

class IAsset;
class IClientAssetBlade;

class AssetFactory : public IAssetFactory, public wkg::Singleton<AssetFactory> {
public:
	AssetFactory();
	~AssetFactory();

	// requests a particular asset of a given type
	//  between textures and meshses, it's possible to determine the type
	//  from teh file extension (presumed to be the name), but when opening
	//  this up to other developers, there's not telling what will happen.
	//  also, you don't have to reference assets by filename, you can give
	//  then just a name if desired, as long as the asset knows enough to
	//  create itself by the name
	virtual IAsset* GetAsset(const std::string& name, const type_info& ti);

	virtual bool RemoveAsset(const type_info& ti, IAsset* a) {
		return RemoveAssetByName(ti.name(), a);
	}
	virtual bool RemoveAssetByName(const std::string& /*name*/, IAsset* /*a*/) {
		assert(false);
		return true;
	}

	// doesn't delete them, just removes them from the map
	virtual void RemoveAllAssets() {
		m_assets.clear();
	}

	// doesn't delete them, just removes them
	virtual void RemoveAllAssetCreators() {
		m_asset_creators.clear();
	}

	virtual void RegisterAssetCreator(const type_info& ti, IClientAssetBlade* c) {
		RegisterAssetCreatorByName(ti.name(), c);
	}

	virtual void RegisterAssetCreatorByName(const std::string& name, IClientAssetBlade* c) {
		m_asset_creators[name] = c;
	}

protected:
	typedef std::map<std::string, IAsset*> AssetMap;
	typedef std::map<std::string, AssetMap> AssetTypeMap;

	AssetTypeMap m_assets;

	typedef std::map<std::string, IClientAssetBlade*> AssetCreatorMap;
	AssetCreatorMap m_asset_creators;
};

} // namespace KEP
