/******************************************************************************
 imodel.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "Core/Math/Matrix.h"

namespace wkg {
class Vector3;
}

namespace KEP {

class IMemSizeGadget;

enum IModelType {
	UNKNOWN = 0,
	LEGACY_PROXY = 1,
	DYNAMIC_PLACEMENT_PROXY = 2,
	PLAYER_PROXY = 3
};

class IModel {
public:
	virtual ~IModel() {}
	virtual void GetName(OUT std::string& aName) = 0;
	virtual Matrix44f GetTransform() = 0;
	virtual bool Intersect(wkg::Vector3& /*from*/, wkg::Vector3& /*to*/, BOOL /*testAlpha*/ = TRUE) {
		return false;
	}
	virtual bool Intersect(wkg::Vector3& /*from*/, wkg::Vector3& /*to*/, wkg::Vector3& /*normal*/) {
		return false;
	}

	virtual void Move(float x, float y, float z) = 0;
	virtual void Rotate(float sx, float sy, float sz) = 0;
	virtual void Scale(float sx, float sy, float sz) = 0;

	virtual void GetBoundingBox(Vector4f& min, Vector4f& max) = 0;

	virtual void Invalidate() = 0;

	IModelType type;
	inline IModelType getType(void) {
		return type;
	}

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const = 0;
};

} // namespace KEP
