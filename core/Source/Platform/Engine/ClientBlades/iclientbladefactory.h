#pragma once

#include "iclientrenderblade.h"
#include "iinputsubscriber.h"
#include "iclientparticleblade.h"
#include "imenublade.h"

namespace KEP {

class IClientBladeFactory {
public:
	virtual std::string PathClientBlades() = 0;

	virtual size_t GetNumBlades() const = 0;
	virtual IClientBlade* GetBlade(size_t idx) const = 0;

	virtual std::vector<IClientBlade*> GetBladesOfType(const std::string& className) const = 0;

	virtual std::vector<IClientRenderBlade*> GetClientRenderBlades() = 0;
	virtual std::vector<IInputSubscriber*> GetInputSubscribers() = 0;
	virtual IClientParticleBlade* GetClientParticleBlade() = 0;
	virtual IMenuBlade* GetMenuBlade() = 0;
	virtual const IMenuBlade* GetMenuBlade() const = 0;
};

} // namespace KEP
