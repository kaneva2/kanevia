/******************************************************************************
 iassetfactory.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <typeinfo.h>

namespace KEP {

class IAsset;
class IClientAssetBlade;

class IAssetFactory {
public:
	virtual IAsset* GetAsset(const std::string& name, const type_info& ti) = 0;

	virtual bool RemoveAsset(const type_info& ti, IAsset* a) = 0;
	virtual bool RemoveAssetByName(const std::string& name, IAsset* a) = 0;

	virtual void RemoveAllAssets() = 0;
	virtual void RemoveAllAssetCreators() = 0;

	virtual void RegisterAssetCreator(const type_info& ti, IClientAssetBlade* c) = 0;
	virtual void RegisterAssetCreatorByName(const std::string& name, IClientAssetBlade* c) = 0;
};

} // namespace KEP
