#if DEPRECATED
#include "wkgtypes.h"

// we use these types
#include "wkgmesh.h"
#include "wkgmeshrender.h"
#include "clientbladefactory.h"
#include "iassetfactory.h"
#include "selectwidget.h"
#include "imodel.h"

#include "device.h"

#include "matrix44.h"

#include "IClientEngine.h"
#include "rotatewidget.h"

#include "common\KEPutil\helpers.h"

RotateWidget::RotateWidget() :
		EditorWidget(),
		m_wkg_viz_meshx(0),
		m_wkg_viz_meshy(0),
		m_wkg_viz_meshz(0)

{
	m_name = "RotateWidget";
	EnableInput(false);
}

RotateWidget::~RotateWidget() {
	SAFE_DELETE(m_wkg_viz_meshx);
	SAFE_DELETE(m_wkg_viz_meshy);
	SAFE_DELETE(m_wkg_viz_meshz);
}

bool RotateWidget::Initialize() {
	return true;
}

void RotateWidget::Render(IDirect3DDevice9 *d) {
}

void RotateWidget::RotateAxis(int32 /*dx*/, int32 /*dy*/,
	float axis_x, float axis_y, float axis_z) {
}

void RotateWidget::MoveX(int32 dx, int32 dy) {
	RotateAxis(dx, dy, (real)dy * 0.2f, 0.0f, 0.0f);
}

void RotateWidget::MoveY(int32 dx, int32 dy) {
	RotateAxis(dx, dy, 0.0f, (real)dx * 0.2f, 0.0f);
}

void RotateWidget::MoveZ(int32 dx, int32 dy) {
	RotateAxis(dx, dy, 0.0f, 0.0f, (real)dy * 0.2f);
}

#endif