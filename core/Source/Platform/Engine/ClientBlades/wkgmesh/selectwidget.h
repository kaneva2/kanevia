/******************************************************************************
 selectwidget.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "iinputsubscriber.h"
#include "iclientrenderblade.h"

namespace wkg {
class Vector3;
}

namespace KEP {
class IModel;
class IMesh;
class IMeshRenderBlade;
} // namespace KEP

struct JONNYVERTEX {
	float x, y, z;
};

class SelectWidget : public KEP::IInputSubscriber, public IClientRenderBlade {
public:
	SelectWidget();
	virtual ~SelectWidget();

	//flags that indicate type of objects to test
	static const ULONG WORLD_OBJECTS = 0x01; //test scene objects
	static const ULONG DYNAMIC_OBJECTS = 0x02; //test dyanmic objects
	static const ULONG PLAYERS = 0x04; //test players

	bool m_renderSelWidget;
	// from IClientBlade
	virtual bool Initialize();
	virtual uint32 Version();
	virtual const string &GetName() const;

	virtual size_t GetNumDependencies() const { return 0; }
	virtual string GetDependency(size_t /*idx*/) const { return ""; }

	// from IInputSubscriber
	virtual void OnLButtonDown(int32 x, int32 y);
	virtual void OnLButtonUp(int32 x, int32 y);

	virtual void OnRButtonDown(int32 x, int32 y);
	virtual void OnRButtonUp(int32 x, int32 y);

	virtual void OnMButtonDown(int32 /*x*/, int32 /*y*/) {}
	virtual void OnMButtonUp(int32 /*x*/, int32 /*y*/) {}

	virtual void OnMouseMove(int32 x, int32 y);
	virtual void OnKey(uint32 vkey);

	// from IClientRenderBlade
	virtual void Render(IDirect3DDevice9 *d);

	virtual uint32 Version() const;

	KEP::IModel *GetSelected() { return m_selected; }
	virtual void SetSelected(KEP::IModel * /*s*/) {}

	virtual void DeselectObj(void *oldObj);

	void DoMove();
	void DoRotate();
	void DoScale();

	virtual void DisableRender();
	virtual void EnableRender();

	void DisableAll();

	virtual void OnUnLoadScene();

	virtual void EnableInput(bool enable = true);

	ULONG GetObjectsToTest() const { return m_objectsToTest; }
	void SetObjectsToTest(ULONG flag) { m_objectsToTest = flag; }

protected:
	void GetPickRayFromPoint(int32 in_x, int32 in_y,
		wkg::Vector3 &out_p1, wkg::Vector3 &out_p2);

	bool SelectModel(int32 x, int32 y, bool leftClick = false, bool rightClick = false, bool doTriangleTest = true); //select model where mouse is

	ULONG m_objectsToTest;

protected:
	string m_name;
	int32 m_x, m_y;

	KEP::IMesh *m_wkg_mesh;
	KEP::IMeshRenderBlade *m_wkg_mesh_render;

	KEP::IModel *m_selected;
	static int m_boundingBoxIndex[36];
	IDirect3DVertexBuffer9 *m_jonnyBoundingVertexBuffer;
	IDirect3DIndexBuffer9 *m_jonnyBoundingIndexBuffer;
	IDirect3DVertexDeclaration9 *m_jonnysWidgetHackDeclaration;
};

#ifndef SAFE_DELETE
#define SAFE_DELETE(p) \
	if (p) {           \
		delete p;      \
		p = 0;         \
	}
#endif
