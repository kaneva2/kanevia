/******************************************************************************
 SceneGraphBlade.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <TinyXML/tinyxml.h>

#include "iinputsubscriber.h"
#include "iclientrenderblade.h"

class WKGMesh;
class WKGMeshRenderBlade;

namespace wkg {
class SceneGraph;
class Node;
} // namespace wkg

class SceneGraphBlade : public IClientRenderBlade {
public:
	SceneGraphBlade();
	virtual ~SceneGraphBlade();

	virtual bool Initialize();
	virtual uint32 Version() const;
	virtual const string &GetName() const;

	virtual void Render(IDirect3DDevice9 *d);

	virtual size_t GetNumDependencies() const;
	virtual string GetDependency(size_t idx) const;

	virtual void AddMesh(KEP::IMesh *mesh);
	virtual void RemoveMesh(KEP::IMesh *mesh);

	virtual bool Load(const char *filename);

protected:
	bool LoadNode(TiXmlElement *node, wkg::Node *parent_node);

protected:
	string m_name;

	WKGMeshRenderBlade *m_wkg_mesh_render;
	wkg::SceneGraph *m_sg;
};
