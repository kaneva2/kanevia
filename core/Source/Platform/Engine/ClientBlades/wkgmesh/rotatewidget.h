#pragma once
/******************************************************************************
 rotatewidget.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "editorwidget.h"

class WKGMesh;
class WKGMeshRenderBlade;

class RotateWidget : public EditorWidget {
public:
	RotateWidget();
	virtual ~RotateWidget();

	virtual bool Initialize();

	virtual void Render(IDirect3DDevice9 *d);

protected:
	void RotateAxis(int32 dx, int32 dy, float x, float y, float z);

	virtual void MoveX(int32 dx, int32 dy);
	virtual void MoveY(int32 dx, int32 dy);
	virtual void MoveZ(int32 dx, int32 dy);

	KEP::IMesh *m_wkg_viz_meshx;
	KEP::IMesh *m_wkg_viz_meshy;
	KEP::IMesh *m_wkg_viz_meshz;
};
