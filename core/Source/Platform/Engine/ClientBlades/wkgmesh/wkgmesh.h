/******************************************************************************
 wkgmesh.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "iasset.h"
#include "iclientassetblade.h"
#include "imesh.h"

namespace wkg {
class Mesh;
class Device;
} // namespace wkg

class WKGMesh : public KEP::IMesh {
public:
	WKGMesh();
	virtual ~WKGMesh();

	bool Load(const string &name);
	wkg::Mesh *GetWKGMesh() { return m_mesh; }

	virtual bool Intersect(wkg::Matrix44 &world, wkg::Vector3 &from, wkg::Vector3 &to);

protected:
	wkg::Mesh *m_mesh;
};

class WKGMeshAssetBlade : public KEP::IClientAssetBlade {
public:
	WKGMeshAssetBlade();

	virtual bool Initialize();

	KEP::IAsset *operator()(const string &name) {
		WKGMesh *mesh = new WKGMesh();
		mesh->Load(name);

		return mesh;
	}

	// if a client blade derives from IAssetCreator, it must implement the following
	//  to regsiter the asset class with the pluggable factory
	void RegisterAsset(KEP::IAssetFactory *factory);

	virtual uint32 Version() const { return 0x100; }
	virtual const string &GetName() const { return m_name; }

	virtual size_t GetNumExtensions() { return 1; }
	virtual const char *GetExtension(size_t /*i*/) { return m_extension.c_str(); }

	virtual const char *GetAssetType();

protected:
	string m_name;
	string m_extension;

	wkg::Device *m_device;
};
