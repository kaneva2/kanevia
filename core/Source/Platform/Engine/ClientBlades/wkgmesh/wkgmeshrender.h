/******************************************************************************
 wkgmeshrender.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <string>
#include "imeshrenderblade.h"

namespace wkg {
class EffectShader;
class EffectMeshRenderer;
class Device;
} // namespace wkg

// This class is exported from the wkgmeshrender.dll
class WKGMeshRenderBlade : public KEP::IMeshRenderBlade {
public:
	WKGMeshRenderBlade();

	virtual bool Initialize();

	enum Options { Render_Wireframe = 0 };
	virtual void RenderMesh(D3DXMATRIX &m, KEP::IMesh *mesh, const char *effect = 0);

	virtual uint32 Version() const { return 0x100; }
	virtual const string &GetName() const { return m_name; }

	virtual void OnLostDevice();
	virtual void OnResetDevice();

	wkg::EffectMeshRenderer *GetEffect(const string &name);

protected:
protected:
	string m_name;

	wkg::Device *m_device;

	map<string, wkg::EffectMeshRenderer *> m_effects;
};
