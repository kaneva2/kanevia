#if DEPRECATED
#include "wkgtypes.h"

// we use these types
#include "wkgmesh.h"
#include "wkgmeshrender.h"
#include "clientbladefactory.h"
#include "iassetfactory.h"
#include "imodel.h"
#include "selectwidget.h"

#include "matrix44.h"

#include "IClientEngine.h"
#include "scalewidget.h"

#include "common\KEPutil\helpers.h"

ScaleWidget::ScaleWidget() :
		EditorWidget() {
	m_name = "ScaleWidget";
	EnableInput(false);
}

bool ScaleWidget::Initialize() {
	return true;
}

uint32
ScaleWidget::Version() const {
	return 0x100;
}

void ScaleWidget::ScaleAxis(int32 dx, int32 dy,
	float axis_x, float axis_y, float axis_z) {
}

void ScaleWidget::MoveX(int32 dx, int32 dy) {
	ScaleAxis(dx, dy, m_matrix._11, m_matrix._12, m_matrix._13);
}

void ScaleWidget::MoveY(int32 dx, int32 dy) {
	ScaleAxis(dx, dy, m_matrix._21, m_matrix._22, m_matrix._23);
}

void ScaleWidget::MoveZ(int32 dx, int32 dy) {
	ScaleAxis(dx, dy, m_matrix._31, m_matrix._32, m_matrix._33);
}

#endif