#include "wkgtypes.h"

// we use these types
#include "wkgmesh.h"
#include "wkgmeshrender.h"
#include "wkgcolor.h"
#include "wkgmaterial.h"
#include "clientbladefactory.h"
#include "iassetfactory.h"
#include "mesh.h"

#include "imodel.h"
#include "selectwidget.h"
#include "matrix44.h"

#include "device.h"

#include "IClientEngine.h"
#include "editorwidget.h"
#include "iclientrenderblade.h"

EditorWidget::EditorWidget() :
		IInputSubscriber(),
		m_name("EditorWidget"),
		m_wkg_meshx(0),
		m_wkg_meshy(0),
		m_wkg_meshz(0),
		m_wkg_mesh_render(0),
		m_x(0),
		m_y(0),
		m_state(State_Start) {
	IClientRenderBlade::m_bladeDepth = ClearsZ;
}

EditorWidget::~EditorWidget() {
	SAFE_DELETE(m_wkg_meshx);
	SAFE_DELETE(m_wkg_meshy);
	SAFE_DELETE(m_wkg_meshz);
}

bool EditorWidget::Initialize() {
	return true;
}

uint32
EditorWidget::Version() const {
	return 0x100;
}

const string &
EditorWidget::GetName() const {
	return m_name;
}

void EditorWidget::GetPickRayFromPoint(int32 in_x, int32 in_y,
	wkg::Vector3 &out_p1, wkg::Vector3 &out_p2) {
	D3DXMATRIX proj;
	m_engine->GetProjectionMatrix(reinterpret_cast<Matrix44f *>(&proj));

	D3DXMATRIX view;
	m_engine->GetViewMatrix(reinterpret_cast<Matrix44f *>(&view));

	D3DXMATRIX view_i;
	D3DXMatrixInverse(&view_i, 0, &view);

	D3DXMATRIX proj_i;
	D3DXMatrixInverse(&proj_i, 0, &proj);

	D3DVIEWPORT9 viewport;
	m_engine->GetD3DDevice()->GetViewport(&viewport);

	//	float aspect = (float)viewport.Width / (float)viewport.Height;

	float x = (float)in_x / (viewport.Width / 2.0f) - 1.0f;
	float y = 1.0f - (float)in_y / (viewport.Height / 2.0f);

	float dx = (1.0f / proj._11) * x;
	float dy = (1.0f / proj._22) * y;

	real hither = 0.1f;
	real yon = 100.0f;

	D3DXVECTOR4 p1(dx * hither, dy * hither, hither, 1.0f);
	D3DXVECTOR4 p2(dx * yon, dy * yon, yon, 1.0f);

	D3DXVec4Transform(&p1, &p1, &view_i);
	D3DXVec4Transform(&p2, &p2, &view_i);

	out_p1.x = p1.x;
	out_p1.y = p1.y;
	out_p1.z = p1.z;

	out_p2.x = p2.x;
	out_p2.y = p2.y;
	out_p2.z = p2.z;

	//	D3DXMatrixIdentity(&m_matrix);
	D3DXMatrixIdentity(&m_matrix_mod);
}

void EditorWidget::OnLButtonDown(int32 /*x*/, int32 /*y*/) {
	// test to see if we intersect any of our widgets
	//  test x, y, z, early out if one is found

	// if we intersect one, then we're dragging it
	//  along that axis until we receive a mouseup

	wkg::Vector3 p1;
	wkg::Vector3 p2;
	GetPickRayFromPoint(m_x, m_y, p1, p2);

	D3DXMATRIX final(m_matrix);
	D3DXMatrixMultiply(&final, &final, &m_matrix_mod);

	wkg::Matrix44 wkgfinal(final);

	if (m_wkg_meshx->Intersect(wkgfinal, p1, p2)) {
		// the user clicked down on the x axis widget, so now if they
		//  move the mouse, we should translate in x
		m_state = State_MoveX;
	} else if (m_wkg_meshy->Intersect(wkgfinal, p1, p2)) {
		m_state = State_MoveY;
	} else if (m_wkg_meshz->Intersect(wkgfinal, p1, p2)) {
		m_state = State_MoveZ;
	} else {
		// the user clicked down, but not on any widget
		m_state = State_Start;
	}
}

void EditorWidget::OnLButtonUp(int32 /*x*/, int32 /*y*/) {
	// we're back to the start state
	m_state = State_Start;
}

void EditorWidget::OnRButtonDown(int32 /*x*/, int32 /*y*/) {
	// don't yet care about this
}

void EditorWidget::OnRButtonUp(int32 /*x*/, int32 /*y*/) {
	// don't yet care about this
}

void EditorWidget::OnMButtonDown(int32 /*x*/, int32 /*y*/) {
	// don't yet care about this
}

void EditorWidget::OnMButtonUp(int32 /*x*/, int32 /*y*/) {
	// don't yet care about this
}

void EditorWidget::MoveX(int32 /*dx*/, int32 /*dy*/) {
}

void EditorWidget::MoveY(int32 /*dx*/, int32 /*dy*/) {
}

void EditorWidget::MoveZ(int32 /*dx*/, int32 /*dy*/) {
}

void EditorWidget::OnMouseMove(int32 x, int32 y) {
	int32 dx = x - m_x;
	int32 dy = y - m_y;

	switch (m_state) {
		case State_MoveX:
			MoveX(dx, dy);
			break;

		case State_MoveY:
			MoveY(dx, dy);
			break;

		case State_MoveZ:
			MoveZ(dx, dy);
			break;

		default:
			// think about enlarging the axis here for ease of use
			break;
	}

	m_x = x;
	m_y = y;
}

void EditorWidget::OnKey(uint32 /*vkey*/) {
	// don't yet care about this
}

void EditorWidget::Render(IDirect3DDevice9 * /*d*/) {
}

string EditorWidget::GetDependency(size_t idx) const {
	switch (idx) {
		case 0:
			return typeid(WKGMeshAssetBlade).name();
			break;

		case 1:
			return typeid(WKGMeshRenderBlade).name();
			break;

		default:
			return "";
			break;
	}
}

void EditorWidget::RecalcMatrix() {
}
