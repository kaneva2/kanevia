#if DEPRECATED
#include "wkgtypes.h"

#include "IClientEngine.h"
#include "iassetfactory.h"
#include "wkgmesh.h"
#include "wkgmeshrender.h"
#include "clientbladefactory.h"
#include "renderstate.h"
#include "device.h"
#include "movewidget.h"
#include "rotatewidget.h"
#include "scalewidget.h"

#include "vector3.h"
#include "imodel.h"
#include "selectwidget.h"
#include "dynamicObPlacementModelProxy.h"
#include "playerModelProxy.h"
#include "legacyModelProxy.h"

#include "common\KEPutil\helpers.h"

SelectWidget::SelectWidget() :
		IInputSubscriber(),
		IClientRenderBlade(),
		m_name("SelectWidget"),
		m_x(0),
		m_y(0),
		m_wkg_mesh(0),
		m_wkg_mesh_render(0),
		m_selected(0),
		m_objectsToTest(WORLD_OBJECTS),
		m_renderSelWidget(true) {
	EnableInput(false);
}

int SelectWidget::m_boundingBoxIndex[36] = {
	0, 1, 0, 4,
	5, 1, 5, 4,
	3, 2, 3, 7,
	6, 2, 6, 7,
	1, 2, 0, 3,
	5, 6, 4, 7,
	1, 3, 0, 7,
	4, 6, 5, 2,
	4, 1, 6, 3
};

SelectWidget::~SelectWidget() {
	SAFE_DELETE(m_selected);
	SAFE_DELETE(m_wkg_mesh);
}

void SelectWidget::DisableRender() {
	m_renderSelWidget = false;
}
void SelectWidget::EnableRender() {
	m_renderSelWidget = true;
}

bool SelectWidget::Initialize() {
	return true;
}

uint32
SelectWidget::Version() {
	return 0x100;
}

const string &
SelectWidget::GetName() const {
	return m_name;
}

void SelectWidget::DeselectObj(void *oldObj) {
	if (!m_selected)
		return;

	auto type = m_selected->getType();
	switch (type) {
		case DYNAMIC_PLACEMENT_PROXY: {
			DynamicPlacementModelProxy *dpmp = (DynamicPlacementModelProxy *)m_selected;
			if (oldObj == (void *)dpmp->GetDynamicPlacementObj())
				SetSelected(NULL);
		} break;

		case PLAYER_PROXY: {
			PlayerModelProxy *pmp = (PlayerModelProxy *)m_selected;
			if (oldObj == (void *)pmp->GetPlayerObj())
				SetSelected(NULL);
		} break;

		case LEGACY_PROXY: {
			LegacyModelProxy *lmp = (LegacyModelProxy *)m_selected;
			if (oldObj == (void *)lmp->GetLegacyWorldObject())
				SetSelected(NULL);
		} break;
	}
}

void SelectWidget::OnLButtonDown(int32 /*x*/, int32 /*y*/) {
	//SelectModel(x,y, true, false);
}

void SelectWidget::OnRButtonDown(int32 /*x*/, int32 /*y*/) {
	//select and fire event
	//SelectModel(x,y, false, true);
}

bool SelectWidget::SelectModel(int32 x, int32 y, bool leftClick, bool rightClick, bool doTriangleTest) {
	return true;
}

void SelectWidget::OnLButtonUp(int32 /*x*/, int32 /*y*/) {
}

void SelectWidget::OnRButtonUp(int32 /*x*/, int32 /*y*/) {
}

void SelectWidget::OnMouseMove(int32 /*x*/, int32 /*y*/) {
}

void SelectWidget::Render(IDirect3DDevice9 * /*d*/) {
}

void SelectWidget::GetPickRayFromPoint(int32 in_x, int32 in_y,
	wkg::Vector3 &out_p1, wkg::Vector3 &out_p2) {
	D3DXMATRIX proj;
	m_engine->GetProjectionMatrix(reinterpret_cast<Matrix44f *>(&proj));

	D3DXMATRIX view;
	m_engine->GetViewMatrix(reinterpret_cast<Matrix44f *>(&view));

	D3DXMATRIX view_i;
	D3DXMatrixInverse(&view_i, 0, &view);

	D3DXMATRIX proj_i;
	D3DXMatrixInverse(&proj_i, 0, &proj);

	D3DVIEWPORT9 viewport;
	m_engine->GetD3DDevice()->GetViewport(&viewport);

	float x = (float)in_x / (viewport.Width / 2.0f) - 1.0f;
	float y = 1.0f - (float)in_y / (viewport.Height / 2.0f);

	float dx = (1.0f / proj._11) * x;
	float dy = (1.0f / proj._22) * y;

	real hither = 0.01f;
	real yon = 10.0f;

	D3DXVECTOR4 p1(dx * hither, dy * hither, hither, 1.0f);
	D3DXVECTOR4 p2(dx * yon, dy * yon, yon, 1.0f);

	D3DXVec4Transform(&p1, &p1, &view_i);
	D3DXVec4Transform(&p2, &p2, &view_i);

	out_p1.x = p1.x;
	out_p1.y = p1.y;
	out_p1.z = p1.z;

	out_p2.x = p2.x;
	out_p2.y = p2.y;
	out_p2.z = p2.z;
}

uint32
SelectWidget::Version() const {
	return 0x100;
}

void SelectWidget::OnKey(uint32 vkey) {
	switch (vkey) {
		case 0:
			DoMove();
			break;

		case 1:
			DoRotate();
			break;

		case 2:
			DoScale();
			break;

		case 3:
			DisableAll();
			break;

		default:
			break;
	}
}

void SelectWidget::DoMove() {
}

void SelectWidget::DoRotate() {
}

void SelectWidget::DoScale() {
}

void SelectWidget::DisableAll() {
}

void SelectWidget::OnUnLoadScene() {
	DisableAll();
}

void SelectWidget::EnableInput(bool enable) {
	m_input_enabled = enable;
}

#endif
