/******************************************************************************
 wkgmesh.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "bladeexports.h"

#include "device.h"
#include "texturefactory.h"
#include "texture.h"
#include "psfactory.h"
#include "vsfactory.h"
#include "pixelshader.h"
#include "vertexshader.h"

#include "iassetfactory.h"
#include "IClientEngine.h"

#include "wkgmeshrender.h"
#include "movewidget.h"
#include "rotatewidget.h"
#include "scalewidget.h"
#include "selectwidget.h"
#include "SceneGraphBlade.h"
#include "ray.h"
#include "triangle.h"
#include "intersect.h"

#include "wkgmesh.h"

#include "mesh.h"

// after loading in a plugin that implements an asset, this is called to
//  register the asset creators

WKGMesh::WKGMesh() {
	m_mesh = new wkg::Mesh();
}

WKGMesh::~WKGMesh() {
	delete m_mesh;
}

bool WKGMesh::Load(const string &name) {
	return m_mesh->Load(name.c_str());
}

bool WKGMesh::Intersect(wkg::Matrix44 &world, wkg::Vector3 &from, wkg::Vector3 &to) {
	vector<wkg::Vector3> verts;
	m_mesh->VecsFromVerts(verts, world);

	// for each tri, intersect this line with it

	wkg::Ray r;
	r.Set(from, (to - from).Normalize());

	for (size_t i = 0; i < verts.size(); i += 3) {
		float t = 0.0f;

		wkg::Triangle tri;
		tri.Set(verts[i], verts[i + 1], verts[i + 2]);

		bool b = wkg::Intersect::TriRay(tri, r, &t);
		if (b && t >= 0.0f) {
			//OutputDebugString("HIT\n");
			return true;
		}
	}

	//OutputDebugString("NOT\n");
	return false;
}

const char *
WKGMeshAssetBlade::GetAssetType() {
	return typeid(WKGMesh).name();
}

void WKGMeshAssetBlade::RegisterAsset(KEP::IAssetFactory *factory) {
	// register ourselves for this type
	factory->RegisterAssetCreator(typeid(WKGMesh), this);
}

WKGMeshAssetBlade::WKGMeshAssetBlade() :
		m_name("WKGMeshBlade"), m_extension("wbm") {
}

bool WKGMeshAssetBlade::Initialize() {
	IDirect3DDevice9 *d = m_engine->GetD3DDevice();

	wkg::Device::Init(&m_device, 0, false, 800, 600, d);
	wkg::Device::Instance()->ForceDefaultRenderState();

	new wkg::TextureFactory();

	return true;
}

extern "C" BLADE_EXPORT
	size_t
	GetNumInterfaces() {
	return 7;
}

extern "C" BLADE_EXPORT
	IClientBlade *
	Create(size_t idx) {
	switch (idx) {
		case 0:
			return new WKGMeshAssetBlade();
			break;

		case 1:
			return new WKGMeshRenderBlade();
			break;

		case 2:
			return new MoveWidget();
			break;

		case 3:
			return new RotateWidget();
			break;

		case 4:
			return new ScaleWidget();
			break;

		case 5:
			return new SelectWidget();
			break;

		case 6:
			return new SceneGraphBlade();
			break;

		default:
			return 0;
			break;
	}
}

extern "C" BLADE_EXPORT void Destroy(IClientBlade *c) {
	delete c;
}

BOOL APIENTRY DllMain(HANDLE /*hModule*/, DWORD /*ul_reason_for_call*/, LPVOID /*lpReserved*/) {
	return TRUE;
}
