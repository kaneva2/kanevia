/******************************************************************************
 editorwidget.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "iinputsubscriber.h"
#include "iclientrenderblade.h"

class WKGMesh;
class WKGMeshRenderBlade;

class EditorWidget : public KEP::IInputSubscriber, public IClientRenderBlade {
public:
	EditorWidget();
	virtual ~EditorWidget();

	virtual bool Initialize();
	virtual uint32 Version() const;
	virtual const string &GetName() const;

	virtual void OnLButtonDown(int32 x, int32 y);
	virtual void OnLButtonUp(int32 x, int32 y);

	virtual void OnRButtonDown(int32 x, int32 y);
	virtual void OnRButtonUp(int32 x, int32 y);

	virtual void OnMButtonDown(int32 x, int32 y);
	virtual void OnMButtonUp(int32 x, int32 y);

	virtual void OnMouseMove(int32 x, int32 y);
	virtual void OnKey(uint32 vkey);
	virtual void Render(IDirect3DDevice9 *d);

	virtual void SetMatrix(const D3DXMATRIX &m) {
		m_matrix = m;
	}

	virtual size_t GetNumDependencies() const { return 2; }
	virtual string GetDependency(size_t idx) const;

	virtual void RecalcMatrix();

protected:
	void GetPickRayFromPoint(int32 x, int32 y, wkg::Vector3 &p1, wkg::Vector3 &p2);

	virtual void MoveX(int32 dx, int32 dy);
	virtual void MoveY(int32 dx, int32 dy);
	virtual void MoveZ(int32 dx, int32 dy);

protected:
	enum State {
		State_Start = 0,
		State_MoveX,
		State_MoveY,
		State_MoveZ
	};

	State m_state;

	// cached position of the mouse
	int32 m_x, m_y;

	KEP::IMesh *m_wkg_meshx;
	KEP::IMesh *m_wkg_meshy;
	KEP::IMesh *m_wkg_meshz;

	KEP::IMeshRenderBlade *m_wkg_mesh_render;

	// this is the matrix this widget keeps track of
	D3DXMATRIX m_matrix;
	D3DXMATRIX m_matrix_mod;

	string m_name;

	friend class SelectWidget;
};
