/******************************************************************************
 SceneGraphBlade.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "wkgmesh.h"
#include "wkgmeshrender.h"
#include "clientbladefactory.h"
#include "iassetfactory.h"
#include "imodel.h"
#include "matrix44.h"
#include "device.h"
#include "sgraph/scenegraph.h"
#include "WKGMeshRender.h"
#include "engine/meshnode.h"
#include "effectshader.h"
#include "effectmeshrenderer.h"

#include "IClientEngine.h"

#include "SceneGraphBlade.h"

struct render_pair {
	KEP::IMesh *mesh;
	KEP::IMeshRenderBlade *mesh_render;
};

vector<render_pair> g_to_render;

SceneGraphBlade::SceneGraphBlade() :
		IClientRenderBlade(), m_sg(0), m_name("SceneGraphBlade") {
}

SceneGraphBlade::~SceneGraphBlade() {
}

bool SceneGraphBlade::Initialize() {
	m_sg = new wkg::SceneGraph();

	m_wkg_mesh_render = new WKGMeshRenderBlade();
	m_wkg_mesh_render->Initialize();
	m_wkg_mesh_render->SetEngineInterface(m_engine);

	return true;
}

uint32
SceneGraphBlade::Version() const {
	return 0x100;
}

const string &
SceneGraphBlade::GetName() const {
	return m_name;
}

void SceneGraphBlade::Render(IDirect3DDevice9 *d) {
	D3DXMATRIX view, proj;

	m_engine->GetViewMatrix(reinterpret_cast<Matrix44f *>(&view));
	m_engine->GetProjectionMatrix(reinterpret_cast<Matrix44f *>(&proj));

	d->SetTransform(D3DTS_VIEW, &view);
	d->SetTransform(D3DTS_PROJECTION, &proj);

	m_sg->Update();
	m_sg->Render();
}

size_t SceneGraphBlade::GetNumDependencies() const {
	return 2;
}

string
SceneGraphBlade::GetDependency(size_t idx) const {
	switch (idx) {
		case 0:
			return typeid(WKGMeshAssetBlade).name();
			break;

		case 1:
			return typeid(WKGMeshRenderBlade).name();
			break;

		default:
			return "";
			break;
	}
}

void SceneGraphBlade::AddMesh(KEP::IMesh *mesh) {
	wkg::MeshNode *mn = new wkg::MeshNode();

	WKGMesh *wkgm = (WKGMesh *)mesh;

	mn->SetMesh(wkgm->GetWKGMesh());
	mn->AddRenderer(m_wkg_mesh_render->GetEffect("selection.fx"));

	m_sg->GetRoot()->AddChild(mn);

	render_pair rp;
	rp.mesh = mesh;
	rp.mesh_render = m_wkg_mesh_render;
	g_to_render.push_back(rp);
}

void SceneGraphBlade::RemoveMesh(KEP::IMesh * /*mesh*/) {
}

bool SceneGraphBlade::LoadNode(TiXmlElement *node, wkg::Node *parent_node) {
	TiXmlNode *child = 0;
	for (child = node->FirstChild(); child; child = child->NextSibling()) {
		const char *val = child->Value();
		if (!_stricmp(val, "Node")) {
			TiXmlElement *e = child->ToElement();

			const char *name = e->Attribute("name");

			const char *m11 = e->Attribute("matrix11");
			const char *m12 = e->Attribute("matrix12");
			const char *m13 = e->Attribute("matrix13");

			const char *m21 = e->Attribute("matrix21");
			const char *m22 = e->Attribute("matrix22");
			const char *m23 = e->Attribute("matrix23");

			const char *m31 = e->Attribute("matrix31");
			const char *m32 = e->Attribute("matrix32");
			const char *m33 = e->Attribute("matrix33");

			const char *m41 = e->Attribute("matrix41");
			const char *m42 = e->Attribute("matrix42");
			const char *m43 = e->Attribute("matrix43");

			wkg::Matrix44 load_mat;
			load_mat.Identity();

			load_mat._11 = wkg::atof(m11);
			load_mat._12 = wkg::atof(m12);
			load_mat._13 = wkg::atof(m13);

			load_mat._21 = wkg::atof(m21);
			load_mat._22 = wkg::atof(m22);
			load_mat._23 = wkg::atof(m23);

			load_mat._31 = wkg::atof(m31);
			load_mat._32 = wkg::atof(m32);
			load_mat._33 = wkg::atof(m33);

			load_mat._41 = wkg::atof(m41);
			load_mat._42 = wkg::atof(m42);
			load_mat._43 = wkg::atof(m43);

			wkg::XFormNode *n = new wkg::XFormNode();
			n->SetMatrix(load_mat);
			n->SetName(name);

			parent_node->AddChild(n);

			if (node->FirstChild())
				LoadNode(node->FirstChild()->ToElement(), n);
		} else if (!_stricmp(val, "Object")) {
			TiXmlElement *e = child->ToElement();
			const char *name = e->Attribute("name");

			KEP::IAsset *asset = m_engine->GetAssetFactory()->GetAsset(name, typeid(WKGMesh));

			wkg::Mesh *wkg_mesh = ((WKGMesh *)asset)->GetWKGMesh();

			wkg::MeshNode *mn = new wkg::MeshNode();
			mn->SetMesh(wkg_mesh);
			mn->AddRenderer(m_wkg_mesh_render->GetEffect("selection.fx"));

			parent_node->AddChild(mn);
		}
	}

	return false;
}

bool SceneGraphBlade::Load(const char *filename) {
	TiXmlDocument xml_doc;

	if (xml_doc.LoadFile(filename)) {
		TiXmlElement *root = xml_doc.FirstChildElement("SceneGraph");
		return LoadNode(root, m_sg->GetRoot());
	}

	return true;
}
