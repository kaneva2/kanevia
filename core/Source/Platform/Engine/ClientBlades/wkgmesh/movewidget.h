/******************************************************************************
 movewidget.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "editorwidget.h"

class WKGMesh;
class WKGMeshRenderBlade;

class MoveWidget : public EditorWidget {
public:
	MoveWidget();

	virtual bool Initialize();

protected:
	void MoveAxis(int32 dx, int32 dy, float x, float y, float z);

	virtual void MoveX(int32 dx, int32 dy);
	virtual void MoveY(int32 dx, int32 dy);
	virtual void MoveZ(int32 dx, int32 dy);
};
