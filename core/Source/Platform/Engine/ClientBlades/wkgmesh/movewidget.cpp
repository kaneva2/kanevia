#if DEPRECATED
#include "wkgtypes.h"

// we use these types
#include "wkgmesh.h"
#include "wkgmeshrender.h"
#include "clientbladefactory.h"
#include "iassetfactory.h"
#include "selectwidget.h"
#include "imodel.h"

#include "vector3.h"
#include "matrix44.h"

#include "IClientEngine.h"
#include "movewidget.h"

#include "common\KEPutil\helpers.h"

MoveWidget::MoveWidget() :
		EditorWidget() {
	m_name = "MoveWidget";
	EnableInput(false);
}

bool MoveWidget::Initialize() {
	return true;
}

void MoveWidget::MoveAxis(int32 dx, int32 dy, float axis_x, float axis_y, float axis_z) {
}

void MoveWidget::MoveX(int32 dx, int32 dy) {
	MoveAxis(dx, dy, m_matrix._11, m_matrix._12, m_matrix._13);
}

void MoveWidget::MoveY(int32 dx, int32 dy) {
	MoveAxis(dx, dy, m_matrix._21, m_matrix._22, m_matrix._23);
}

void MoveWidget::MoveZ(int32 dx, int32 dy) {
	MoveAxis(dx, dy, m_matrix._31, m_matrix._32, m_matrix._33);
}
#endif