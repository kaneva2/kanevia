#pragma once
/******************************************************************************
 scalewidget.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "editorwidget.h"

class WKGMesh;
class WKGMeshRenderBlade;

class ScaleWidget : public EditorWidget {
public:
	ScaleWidget();

	virtual bool Initialize();
	virtual uint32 Version() const;

protected:
	void ScaleAxis(int32 dx, int32 dy, float x, float y, float z);

	virtual void MoveX(int32 dx, int32 dy);
	virtual void MoveY(int32 dx, int32 dy);
	virtual void MoveZ(int32 dx, int32 dy);
};
