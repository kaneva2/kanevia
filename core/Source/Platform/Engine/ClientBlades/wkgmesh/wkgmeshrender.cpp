/******************************************************************************
 wkgmeshrender.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "iclientblade.h"
#include "IClientEngine.h"
#include "device.h"
#include "matrix44.h"
#include "texturefactory.h"
#include "../wkgmesh/wkgmesh.h"
#include "renderstate.h"

#include "iclientbladefactory.h"
#include "effectshader.h"
#include "effectmeshrenderer.h"

#include "wkgmeshrender.h"
#include <string>
#include <d3dx9math.h>

#include "common\KEPutil\helpers.h"

#define DEFAULT_EFFECT "material.fx"

using namespace std;

WKGMeshRenderBlade::WKGMeshRenderBlade() :
		m_name("WKGMeshRenderBlade") {
}

wkg::EffectMeshRenderer *
WKGMeshRenderBlade::GetEffect(const string &name) {
	auto pCBF = m_engine->GetBladeFactory();
	if (!pCBF) {
		return NULL;
	}

	string effectname = PathAdd(PathAdd(pCBF->PathClientBlades(), "effects"), name);

	map<string, wkg::EffectMeshRenderer *>::iterator itor = m_effects.find(effectname);
	if (itor == m_effects.end()) {
		wkg::EffectShader *es = new wkg::EffectShader();
		es->Create(effectname.c_str());

		wkg::EffectMeshRenderer *em = new wkg::EffectMeshRenderer(es);
		m_effects[effectname] = em;

		itor = m_effects.find(effectname);
	}

	return itor->second;
}

void WKGMeshRenderBlade::RenderMesh(D3DXMATRIX &world, KEP::IMesh *mesh, const char *effect) {
	HRESULT hr;
	wkg::RenderState rs;
	wkg::Device::Instance()->CaptureRenderState(rs);

	IDirect3DPixelShader9 *ps = 0;
	hr = wkg::Device::Instance()->GetPixelShader(&ps);

	IDirect3DVertexShader9 *vs = 0;
	hr = wkg::Device::Instance()->GetVertexShader(&vs);

	IDirect3DVertexDeclaration9 *vd = 0;
	hr = wkg::Device::Instance()->GetVertexDeclaration(&vd);

	// illamas: added this defaultrs, before it would use rs in the SetRenderState call...
	// this is as inefficient but at least it resets the state to some known default...
	// we should do this before rendering every "RenderBlade" blade, not every mesh I guess.
	wkg::RenderState defaultrs;
	wkg::Device::Instance()->SetRenderState(defaultrs);

	D3DXMATRIX view, proj;
	D3DXMATERIAL m;

	m.MatD3D.Diffuse.a = 255.0f;
	m.MatD3D.Diffuse.r = 255.0f;
	m.MatD3D.Diffuse.g = 255.0f;
	m.MatD3D.Diffuse.b = 255.0f;

	IDirect3DDevice9 *d = m_engine->GetD3DDevice();

	d->SetMaterial(&m.MatD3D);
	m_engine->GetViewMatrix(reinterpret_cast<Matrix44f *>(&view));
	m_engine->GetProjectionMatrix(reinterpret_cast<Matrix44f *>(&proj));
	d->SetTransform(D3DTS_WORLD, &world);
	d->SetTransform(D3DTS_VIEW, &view);
	d->SetTransform(D3DTS_PROJECTION, &proj);

	wkg::EffectMeshRenderer *emr = 0;
	if (effect) {
		emr = GetEffect(effect);
	} else {
		emr = GetEffect(DEFAULT_EFFECT);
	}

	WKGMesh *wkgmesh = reinterpret_cast<WKGMesh *>(mesh);
	if (wkgmesh) {
		emr->Render(wkgmesh->GetWKGMesh());
	}

	wkg::Device::Instance()->SetVertexDeclaration(vd);
	wkg::Device::Instance()->SetVertexShader(vs);
	wkg::Device::Instance()->SetPixelShader(ps);
	wkg::Device::Instance()->RestoreRenderState(rs);
}

void WKGMeshRenderBlade::OnLostDevice() {
	map<string, wkg::EffectMeshRenderer *>::iterator itor;
	for (itor = m_effects.begin(); itor != m_effects.end(); itor++) {
		if (itor->second)
			itor->second->OnLostDevice();
	}
}

void WKGMeshRenderBlade::OnResetDevice() {
	map<string, wkg::EffectMeshRenderer *>::iterator itor;
	for (itor = m_effects.begin(); itor != m_effects.end(); itor++) {
		if (itor->second)
			itor->second->OnResetDevice();
	}
}

bool WKGMeshRenderBlade::Initialize() {
	return true;
}
