///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include <d3d9.h>
#include "cexmeshclass.h"
#include "creferencelibrary.h"
#include "CWldObject.h"
#include "CNodeObject.h"
#include "compileopts.h"
#include "wkgtypes.h"

#include <vector>

#include "legacymodelproxy.h"
#include "ReMesh.h"

namespace KEP {

LegacyModelProxy::LegacyModelProxy(CWldObject* obj, IDirect3DDevice9* d, CReferenceObjList* libraryReferenceDB) :
		m_world_obj(obj),
		m_device(d),
		m_libraryReferenceDB(libraryReferenceDB)
//	m_world_obj->m_cached_box(false),			//12_27_06 CPITZER....persistent AABB support...moved to CWldObject
//	m_min(0.0f, 0.0f, 0.0f),		//12_27_06 CPITZER....persistent AABB support...moved to CWldObject
//	m_max(0.0f, 0.0f, 0.0f)			//12_27_06 CPITZER....persistent AABB support...moved to CWldObject
{
	type = LEGACY_PROXY;
}

void LegacyModelProxy::Scale(float sx, float sy, float sz) {
	Vector3f scale(sx * m_world_obj->GetScale().x, sy * m_world_obj->GetScale().y, sz * m_world_obj->GetScale().z);
	Matrix44f worldTransform(m_world_obj->GetWorldTransform());
	Vector3f translate = worldTransform.GetTranslation();
	m_world_obj->Transform(scale, m_world_obj->GetRotation(), translate);
}

void LegacyModelProxy::Move(float x, float y, float z) {
	Matrix44f worldTransform(m_world_obj->GetWorldTransform());
	Vector3f translate(x + worldTransform(3, 0),
		y + worldTransform(3, 1),
		z + worldTransform(3, 2));
	m_world_obj->Transform(m_world_obj->GetScale(), m_world_obj->GetRotation(), /*m_world_obj->GetTranslation() +*/ translate);
}

void LegacyModelProxy::Rotate(float rx, float ry, float rz) {
	Vector3f rotate(rx, ry, rz);
	Matrix44f worldTransform(m_world_obj->GetWorldTransform());
	Vector3f translate(worldTransform.GetTranslation());
	m_world_obj->Transform(m_world_obj->GetScale(), m_world_obj->GetRotation() + rotate, translate);
}

void LegacyModelProxy::GetName(std::string& aName) {
	if (m_world_obj && m_world_obj->m_meshObject)
		aName = m_world_obj->m_meshObject->m_fileName;
}

bool LegacyModelProxy::Intersect(Vector3f& from, Vector3f& to, BOOL testAlpha) {
	Vector3f normal;
	return Intersect(from, to, normal);
}

bool LegacyModelProxy::Intersect(Vector3f& from, Vector3f& to, Vector3f& normal) {
	if (m_world_obj) {
		Vector3f origin(from), direction((to - from).Normalize()), result;

		return m_world_obj->IntersectRay(
			*reinterpret_cast<const Vector3f*>(&origin),
			*reinterpret_cast<const Vector3f*>(&direction),
			NULL,
			reinterpret_cast<Vector3f*>(&to),
			reinterpret_cast<Vector3f*>(&normal));
	}

	return false;
}

Matrix44f
LegacyModelProxy::GetTransform() {
	CEXMeshObj* mesh_reference = 0;

	Matrix44f adj_matrix;
	adj_matrix.MakeIdentity();

	Matrix44f identity;
	identity.MakeIdentity();

	if (m_world_obj->m_meshObject) {
		mesh_reference = m_world_obj->m_meshObject;
		identity.MakeTranslation(mesh_reference->GetMeshCenter());
	} else if (m_world_obj->m_meshObject == 0 && m_world_obj->m_node != 0) {
		if (m_libraryReferenceDB->GetCount()) {
			CReferenceObj* refObj = m_libraryReferenceDB->GetByIndex(m_world_obj->m_node->m_libraryReference);
			if (refObj) {
				if (refObj->m_collisionModel) {
					mesh_reference = refObj->m_collisionModel;
					identity = adj_matrix = m_world_obj->m_node->GetWorldTransform();
				}
			}
		}
	}

	return identity;
}

void LegacyModelProxy::GetBoundingBox(Vector4f& min, Vector4f& max) {
	if (m_world_obj->m_node) {
		ABBOX& bndBox = m_world_obj->m_node->m_clipBox;

		min.x = bndBox.minX;
		min.y = bndBox.minY;
		min.z = bndBox.minZ;
		min.w = 0.f;

		max.x = bndBox.maxX;
		max.y = bndBox.maxY;
		max.z = bndBox.maxZ;
		max.w = 0.f;

		return;
	} else if (m_world_obj->m_meshObject) {
		const ABBOX& bndBox = m_world_obj->m_meshObject->GetBoundingBox();

		min.x = bndBox.minX;
		min.y = bndBox.minY;
		min.z = bndBox.minZ;
		min.w = 0.f;

		max.x = bndBox.maxX;
		max.y = bndBox.maxY;
		max.z = bndBox.maxZ;
		max.w = 0.f;

		return;
	}
}

} //namespace KEP