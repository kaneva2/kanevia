///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "iclientblade.h"

struct IDirect3DDevice9;

namespace KEP {

enum RenderBladeDepth {
	UndefinedZUse = 2,
	RequiresZUse = 1,
	ClearsZ = 3
};

class IClientRenderBlade : public IClientBlade {
public:
	RenderBladeDepth m_bladeDepth;

	virtual ~IClientRenderBlade() {}

	virtual void Render() = 0;

	virtual void Update() {}
};

} // namespace KEP