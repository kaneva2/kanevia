///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#ifdef BUILD_BLADE
#define BLADE_EXPORT __declspec(dllexport)
#define ENGINE_EXPORT __declspec(dllimport)
#elif defined(USE_BLADE_DLL)
#define BLADE_EXPORT __declspec(dllimport)
#define ENGINE_EXPORT __declspec(dllexport)
#else
#define BLADE_EXPORT
#define ENGINE_EXPORT
#endif
