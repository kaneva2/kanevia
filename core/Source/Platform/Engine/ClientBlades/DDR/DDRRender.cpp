///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "DDR.h"
#include "Event\eventsystem\dispatcher.h"
#include "IClientEngine.h"

using namespace KEP;

#define DIRECTINPUT_VERSION 0x0800
#define DIKEY_PRESSED 0x80
#define DIKEY_RELEASED 0x80
#define SMALL_SIZE_MOD 0.75f
#define INTRO_DELAY_TIME 0.5f
#define MAX_PAUSE_TIME 170.0f
#define SUCCESS_MESSAGE_XSIZE 256
#define SUCCESS_MESSAGE_YSIZE 64
#define PERFECT_BONUS_XSIZE 89
#define PERFECT_BONUS_YSIZE 10
#define ICON_SIZE 8

#define FAIL_ANIM_BASE 142
#define FAIL_ANIM_COUNT 2
#define GAMEOVER_ANIM_BASE 139
#define GAMEOVER_ANIM_COUNT 3
#define PERFECT_ANIM_INDEX 0
#define SUCCESS_ANIM_INDEX 1
#define FAIL_ANIM_INDEX 2
#define MAGIC_ANIM_OFFSET_NUMBER 19

#define START_ARROW_NUM 4

int IDEAL_X_SIZE = 47;
int IDEAL_Y_SIZE = 43;

#define MAX_SCORE_SLOTS 7

int ConvertToArray(int *result, DWORD value) {
	int digits = 0;
	bool valHit = false;

	for (int i = MAX_SCORE_SLOTS; i > 0; i--) {
		DWORD topVal = (DWORD)pow((double)10, i - 1);

		result[i - 1] = (value / topVal);

		if (result[i - 1] > 0 || valHit) {
			valHit = true;
			digits++;
		}

		value -= (topVal * result[i - 1]);
	}

	if (valHit)
		return digits;
	else
		return 1;
}

enum NUMBER {
	ZERO = 0,
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	COMMA,
	MAX_NUMBERS
};

struct NUMBER_QUAD {
	QUAD_UV lowerLeftUV;
	QUAD_UV upperRightUV;

	NUMBER number;

	//	Each number has a different size
	float sizeX;

	static std::string texName;
	static float numberHeight;
};

std::string NUMBER_QUAD::texName("DDRNumbers");
float NUMBER_QUAD::numberHeight = 30.0f;

NUMBER_QUAD displayNumbers[MAX_NUMBERS];

float AnimTimes[200] = {
	0.0f,
};
float AnimTimesM[200] = {
	0.0f,
};

#include "dinput.h"

namespace KEP {

void DDR::InitNumbers() {
	displayNumbers[0].sizeX = 24.0f;
	displayNumbers[0].lowerLeftUV.u = 0.862f;
	displayNumbers[0].lowerLeftUV.v = 1.0f;
	displayNumbers[0].upperRightUV.u = 0.959f;
	displayNumbers[0].upperRightUV.v = 0.0f;

	displayNumbers[1].sizeX = 18.0f;
	displayNumbers[1].lowerLeftUV.u = 0.0f;
	displayNumbers[1].lowerLeftUV.v = 1.0f;
	displayNumbers[1].upperRightUV.u = 0.072f;
	displayNumbers[1].upperRightUV.v = 0.0f;

	displayNumbers[2].sizeX = 22.0f;
	displayNumbers[2].lowerLeftUV.u = 0.076f;
	displayNumbers[2].lowerLeftUV.v = 1.0f;
	displayNumbers[2].upperRightUV.u = 0.165f;
	displayNumbers[2].upperRightUV.v = 0.0f;

	displayNumbers[3].sizeX = 23.0f;
	displayNumbers[3].lowerLeftUV.u = 0.17f;
	displayNumbers[3].lowerLeftUV.v = 1.0f;
	displayNumbers[3].upperRightUV.u = 0.263f;
	displayNumbers[3].upperRightUV.v = 0.0f;

	displayNumbers[4].sizeX = 25.0f;
	displayNumbers[4].lowerLeftUV.u = 0.267f;
	displayNumbers[4].lowerLeftUV.v = 1.0f;
	displayNumbers[4].upperRightUV.u = 0.368f;
	displayNumbers[4].upperRightUV.v = 0.0f;

	displayNumbers[5].sizeX = 23.0f;
	displayNumbers[5].lowerLeftUV.u = 0.372f;
	displayNumbers[5].lowerLeftUV.v = 1.0f;
	displayNumbers[5].upperRightUV.u = 0.465f;
	displayNumbers[5].upperRightUV.v = 0.0f;

	displayNumbers[6].sizeX = 23.0f;
	displayNumbers[6].lowerLeftUV.u = 0.469f;
	displayNumbers[6].lowerLeftUV.v = 1.0f;
	displayNumbers[6].upperRightUV.u = 0.562f;
	displayNumbers[6].upperRightUV.v = 0.0f;

	displayNumbers[7].sizeX = 23.0f;
	displayNumbers[7].lowerLeftUV.u = 0.566f;
	displayNumbers[7].lowerLeftUV.v = 1.0f;
	displayNumbers[7].upperRightUV.u = 0.659f;
	displayNumbers[7].upperRightUV.v = 0.0f;

	displayNumbers[8].sizeX = 24.0f;
	displayNumbers[8].lowerLeftUV.u = 0.663f;
	displayNumbers[8].lowerLeftUV.v = 1.0f;
	displayNumbers[8].upperRightUV.u = 0.761f;
	displayNumbers[8].upperRightUV.v = 0.0f;

	displayNumbers[9].sizeX = 23.0f;
	displayNumbers[9].lowerLeftUV.u = 0.765f;
	displayNumbers[9].lowerLeftUV.v = 1.0f;
	displayNumbers[9].upperRightUV.u = 0.858f;
	displayNumbers[9].upperRightUV.v = 0.0f;

	displayNumbers[10].sizeX = 8.0f;
	displayNumbers[10].lowerLeftUV.u = 0.963f;
	displayNumbers[10].lowerLeftUV.v = 1.0f;
	displayNumbers[10].upperRightUV.u = 0.995f;
	displayNumbers[10].upperRightUV.v = 0.0f;
}

void DDR::RenderNumbers() {
	//	for each of the score slots, render a number
	float numberOffsetX = 248.0f;
	float numberOffsetY = -26.0f;
	float levelOffsetX = -142.0f;
	float levelOffsetY = -25.0f;
	float currentSizeOffset = 0.0f;
	float defaultNumberGap = 2.0f;

	int scores[MAX_SCORE_SLOTS];

	int numDigits = ConvertToArray(scores, mCurrentFameAwarded);

	for (int i = 0; i < numDigits; i++) {
		mRenderer->DrawQuad(QUAD_POINT(mStartX + numberOffsetX - displayNumbers[scores[i]].sizeX - currentSizeOffset - (defaultNumberGap * i),
								mStartY + numberOffsetY),
			QUAD_POINT(mStartX + numberOffsetX - currentSizeOffset - (defaultNumberGap * i),
				mStartY + numberOffsetY + NUMBER_QUAD::numberHeight),

			NUMBER_QUAD::texName, D3DTEXF_POINT,
			displayNumbers[scores[i]].lowerLeftUV,
			displayNumbers[scores[i]].upperRightUV);

		currentSizeOffset += displayNumbers[scores[i]].sizeX;
	}

	if (mCurrentLevel == 10) {
		mRenderer->DrawQuad(QUAD_POINT(mStartX + levelOffsetX, mStartY + numberOffsetY),
			QUAD_POINT(mStartX + levelOffsetX + displayNumbers[1].sizeX, mStartY + numberOffsetY + NUMBER_QUAD::numberHeight),

			NUMBER_QUAD::texName, D3DTEXF_POINT,
			displayNumbers[1].lowerLeftUV,
			displayNumbers[1].upperRightUV);

		mRenderer->DrawQuad(QUAD_POINT(mStartX + levelOffsetX + displayNumbers[1].sizeX + defaultNumberGap,
								mStartY + numberOffsetY),
			QUAD_POINT(mStartX + levelOffsetX + displayNumbers[0].sizeX + displayNumbers[1].sizeX + defaultNumberGap,
				mStartY + numberOffsetY + NUMBER_QUAD::numberHeight),

			NUMBER_QUAD::texName, D3DTEXF_POINT,
			displayNumbers[0].lowerLeftUV,
			displayNumbers[0].upperRightUV);

	} else {
		mRenderer->DrawQuad(QUAD_POINT(mStartX + levelOffsetX, mStartY + levelOffsetY),
			QUAD_POINT(mStartX + levelOffsetX + displayNumbers[mCurrentLevel].sizeX, mStartY + levelOffsetY + NUMBER_QUAD::numberHeight),

			NUMBER_QUAD::texName, D3DTEXF_POINT,
			displayNumbers[mCurrentLevel].lowerLeftUV,
			displayNumbers[mCurrentLevel].upperRightUV);
	}
}

bool DDR::Initialize() {
	//	Get the sound manager
	mEngine = m_engine;

	assert(mEngine);

	//	Get Renderer
	mRenderer = mEngine->GetDynamicGeom();

	assert(mRenderer);

	IDispatcher *p = mEngine->GetDispatcher();

	assert(p);

	mLevelToStart = 0;
	mTimeAddSec = 0.0f;
	mNumMoves = 0;
	mbWaitingForScore = false;
	mTimePaused = 0.0f;
	mbHasResized = false;
	mScoreToRender = 0;
	mCurrentFrameTimeSec = 0.0f;
	numPerfectsInARow = 0;
	mCurrentNumberTime = 0.0f;
	mCurrentNumberToDisplay = 0;
	mWhichSuccessMessage = 0;
	mCurrentFameEarned = 0;

	//	Init events
	RegisterEvents(p);
	RegisterEventHandlers(p);

	mSequenceToStart = 0;
	mReInitLevelsOnFail = false;
	mGameState = NOTRUNNING;
	mAnimState = NOTPLAYING;
	mPauseState = NOTPAUSED;

	//	Init success textures
	mSuccessMessages[0].textureName = "awesome";
	mSuccessMessages[1].textureName = "excellent";
	mSuccessMessages[2].textureName = "fantastic";
	mSuccessMessages[3].textureName = "great_job";
	mSuccessMessages[4].textureName = "rock_on";
	mSuccessMessages[5].textureName = "dance_fame_level_up";

	mFameNumbers[0].textureName = "dance_fame_0";
	mFameNumbers[0].charStartX = 10;
	mFameNumbers[0].charWidthX = 21;
	mFameNumbers[1].textureName = "dance_fame_1";
	mFameNumbers[1].charStartX = 13;
	mFameNumbers[1].charWidthX = 15;
	mFameNumbers[2].textureName = "dance_fame_2";
	mFameNumbers[2].charStartX = 11;
	mFameNumbers[2].charWidthX = 19;
	mFameNumbers[3].textureName = "dance_fame_3";
	mFameNumbers[3].charStartX = 11;
	mFameNumbers[3].charWidthX = 19;
	mFameNumbers[4].textureName = "dance_fame_4";
	mFameNumbers[4].charStartX = 10;
	mFameNumbers[4].charWidthX = 21;
	mFameNumbers[5].textureName = "dance_fame_5";
	mFameNumbers[5].charStartX = 11;
	mFameNumbers[5].charWidthX = 20;
	mFameNumbers[6].textureName = "dance_fame_6";
	mFameNumbers[6].charStartX = 11;
	mFameNumbers[6].charWidthX = 19;
	mFameNumbers[7].textureName = "dance_fame_7";
	mFameNumbers[7].charStartX = 11;
	mFameNumbers[7].charWidthX = 19;
	mFameNumbers[8].textureName = "dance_fame_8";
	mFameNumbers[8].charStartX = 11;
	mFameNumbers[8].charWidthX = 20;
	mFameNumbers[9].textureName = "dance_fame_9";
	mFameNumbers[9].charStartX = 11;
	mFameNumbers[9].charWidthX = 19;

	return true;
}

void DDR::HandleSlotUpdate(ArrowSlot *pSlot) {
	//	They pressed an arrow and it did not match the current arrow key
	mEngine->PlayBySoundID(8, false);

	Sequence *pSequence = &mLevels[mCurrentLevelIndex].sequences[mLevels[mCurrentLevelIndex].currentSequence];

	pSlot->bSelected = true;

	pSequence->currentSlot++;
}

void DDR::InitializeSequence(Sequence *pSequence, int numArrows, bool reInit) {
	pSequence->numArrows = numArrows;

	int startx;
	int starty = mStartY;

	int arrowOffsetX = -1;
	int arrowOffsetY = -94;

	if (!reInit)
		pSequence->currentSlot = 0;

	for (int i = 0; i < numArrows; i++) {
		//	Get the slot
		ArrowSlot *pSlot = &pSequence->Slots[i];

		//	Randomly choose between 0-3 for an arrow
		//	If we're re-initing, don't redo this
		if (!reInit)
			pSlot->arrows = (DDR::ARROW)(rand() % 4);

		startx = mStartX - ((numArrows * IDEAL_X_SIZE) / 2);
		pSlot->lowerLeft.x = (float)(startx + (i * IDEAL_X_SIZE) + arrowOffsetX);
		pSlot->upperRight.x = (float)(startx + ((i + 1) * IDEAL_X_SIZE) + arrowOffsetX);

		pSlot->lowerLeft.y = (float)(starty + arrowOffsetY);
		pSlot->upperRight.y = (float)(starty + IDEAL_Y_SIZE + arrowOffsetY);

		//	Assign positions for a smaller version
		startx = (int)(mStartX - ((numArrows * (IDEAL_X_SIZE * SMALL_SIZE_MOD)) / 2));
		pSlot->halfLowerLeft.x = (float)(startx + (i * (IDEAL_X_SIZE * SMALL_SIZE_MOD)));
		pSlot->halfUpperRight.x = (float)(startx + ((i + 1) * (IDEAL_X_SIZE * SMALL_SIZE_MOD)));

		pSlot->halfLowerLeft.y = (float)(starty);
		pSlot->halfUpperRight.y = (float)(starty + (IDEAL_Y_SIZE * SMALL_SIZE_MOD));

		if (!reInit)
			pSlot->bSelected = false;

		//	Build UV's
		switch (pSlot->arrows) {
			case Up: {
				pSlot->lowerLeftUV.u = 0.0f;
				pSlot->lowerLeftUV.v = 0.25f;

				pSlot->upperRightUV.u = 0.33f;
				pSlot->upperRightUV.v = 0.0f;
			} break;

			case Down: {
				pSlot->lowerLeftUV.u = 0.0f;
				pSlot->lowerLeftUV.v = 0.5f;

				pSlot->upperRightUV.u = 0.33f;
				pSlot->upperRightUV.v = 0.25;
			} break;

			case Left: {
				pSlot->lowerLeftUV.u = 0.0f;
				pSlot->lowerLeftUV.v = 0.75f;

				pSlot->upperRightUV.u = 0.33f;
				pSlot->upperRightUV.v = 0.5f;
			} break;

			case Right: {
				pSlot->lowerLeftUV.u = 0.0f;
				pSlot->lowerLeftUV.v = 1.0f;

				pSlot->upperRightUV.u = 0.33f;
				pSlot->upperRightUV.v = 0.75f;
			} break;

			default:
				break;
		}
	}
}

void DDR::ReSizeLevel(ULONG xVal, ULONG yVal) {
	DWORD width, height;

	mbHasResized = true;

	mRenderer->GetViewportSize(width, height);

	int XOFFSET = -124;
	int YOFFSET = 28;

	mStartY = (height - yVal) + YOFFSET;
	mStartX = (xVal + ((mEvent.m_ctrlWidth + XOFFSET) / 2));

	InitializeLevel(mCurrentLevelIndex, mNumMoves, true);

	if (mCurrentLevel < 10)
		InitializeLevel(mNextLevelIndex, mNumMoves + 1, true);
}

void DDR::InitializeLevel(int level, ULONG numArrows, bool reInit) {
	Sequence *pSequence;

	//	Initialize sequences
	for (int j = 0; j < mLevels[level].numSequences; j++) {
		pSequence = &mLevels[level].sequences[j];

		//	Initialize the sequence set for this level
		InitializeSequence(pSequence, numArrows, reInit);
	}
}

void DDR::HandleLevelFail() {
	if (mCurrentLevel > 1)
		mLevelToStart = --mCurrentLevel;
	else
		mLevelToStart = 1;

	//	Don't show a victory
	mWhichSuccessMessage = -1;
	numPerfectsInARow = 0;

	mTimeAddSec = 0.0f;

	//	If they don't have any tries left, it's over
	if (mNumFailAttempts == 1) {
		mGameState = GAMEOVER;

		//	Set the failure animation to be a final flare

		mAnimData.animationflares[FAIL_ANIM_INDEX] = GAMEOVER_ANIM_BASE + (rand() % GAMEOVER_ANIM_COUNT);

		//	Play final flare sound
		mEngine->PlayBySoundID(7, false);

		RecordScore(mEvent.m_gameId, mCurrentLevel, 0, false, true, false, false, false);

		//	Start Playing the animation
		PlayAnimSequence(true);

		mLevelToStart = 0;
	} else {
		//	Play level fail sound
		mEngine->PlayBySoundID(4, false);

		PlayAnimSequence(true);
		mNumFailAttempts--;
		StartLevelData(mLevelToStart);
		mGameState = WAITINGONRELOAD;
	}

	//	Stop timer, handle fail
	mReInitLevelsOnFail = true;
	mDidFail = true;
}

void DDR::HandleKeyPressFail() {
	if (mFailDelaySec <= 0.0f) {
		//	Add 1 second
		mFailDelaySec += 0.3f;

		//	They pressed an arrow and it did not match the current arrow key
		mLevelCompletedSec = mCurrentGameTimeSec;

		HandleLevelFail();
	}
}

void DDR::HandleGameExit() {
	mbGameRunning = false;
	mPauseState = NOTPAUSED;
	mbWaitingForScore = false;
	mbHasResized = false;
	mScoreToRender = 0;
	mSequenceToStart = 0;
	numPerfectsInARow = 0;
	mTimeAddSec = 0.0f;

	mEngine->setMovementRestriction(IClientEngine::MR_SRC_DDR, IClientEngine::RestrictMovement_All, false);

	mGameState = NOTRUNNING;

	//	Reset Animations
	mAnimState = NOTPLAYING;

	//	Set idle
	mEngine->AssignCurrentAnimation(eAnimType::Stand);

	mbWaitingForUser = false;
	mbHasAccepted = false;

	CloseGameDialog();
}

EVENT_PROC_RC DDR::ProcessKeys(ULONG data) {
	//	If we're waiting on the user to start

	//	This test is here to avoid the deserialization (which is costly)
	//	if the game is not running
	if (mGameState == WAITINGONUSERTOSTART || mGameState == RUNNING || mGameState == WAITINGONANIMATIONS) {
		//	Test the keys we're interested in
		BYTE *keyData = (BYTE *)data;
		static DWORD lastKey = 0;

		if (lastKey != 0) {
			//	They are still holding down the key
			if (keyData[lastKey] & DIKEY_PRESSED)
				return EVENT_PROC_RC::CONSUMED;
			else {
				//	Key released, reset
				lastKey = 0;
				return EVENT_PROC_RC::CONSUMED;
			}
		}

		//	Collect user data in pregame
		if (keyData[DIK_TAB] & DIKEY_PRESSED && mGameState == WAITINGONUSERTOSTART) {
			lastKey = DIK_TAB;
			mbHasAccepted = true;
		}

		//	Don't collect game input if game isnt running
		//	Or if animations are playing
		//	Or if paused!
		if (!(mGameState == RUNNING) || mAnimState == PLAYING || mPauseState == PAUSED)
			return EVENT_PROC_RC::CONSUMED;

		Sequence *pSequence = &mLevels[mCurrentLevelIndex].sequences[mLevels[mCurrentLevelIndex].currentSequence];
		ArrowSlot *pSlot = &pSequence->Slots[pSequence->currentSlot];

		if (keyData[DIK_UP] & DIKEY_PRESSED) {
			lastKey = DIK_UP;
			if (pSlot->arrows == DDR::Up)
				HandleSlotUpdate(pSlot);
			else
				HandleKeyPressFail();
		}

		if (keyData[DIK_DOWN] & DIKEY_PRESSED) {
			lastKey = DIK_DOWN;
			if (pSlot->arrows == DDR::Down)
				HandleSlotUpdate(pSlot);
			else
				HandleKeyPressFail();
		}

		if (keyData[DIK_LEFT] & DIKEY_PRESSED) {
			lastKey = DIK_LEFT;
			if (pSlot->arrows == DDR::Left)
				HandleSlotUpdate(pSlot);
			else
				HandleKeyPressFail();
		}

		if (keyData[DIK_RIGHT] & DIKEY_PRESSED) {
			lastKey = DIK_RIGHT;
			if (pSlot->arrows == DDR::Right)
				HandleSlotUpdate(pSlot);
			else
				HandleKeyPressFail();
		}

		if (keyData[DIK_ESCAPE] & DIKEY_PRESSED) {
			lastKey = DIK_ESCAPE;
			HandleGameExit();
		}

		return EVENT_PROC_RC::CONSUMED;
	} else {
		return EVENT_PROC_RC::NOT_PROCESSED;
	}
}

EVENT_PROC_RC DDR::DDRInputHandler(ULONG lparam, IDispatcher * /*disp*/, IEvent *e) {
	ULONG keyData = 0; // DRF - Reasonable Value If Missing InBuffer?

	DDR *pBlade = (DDR *)lparam;

	if (e->InBuffer()) {
		e->InBuffer()->Rewind();
		(*e->InBuffer()) >> keyData;
	} else {
		assert(false); // DRF - keyData Not Initialized
	}

	if (pBlade)
		pBlade->ProcessKeys(keyData);

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC DDR::DDRMenuHandler(ULONG lparam, IDispatcher * /*disp*/, IEvent *e) {
	DDR *pBlade = (DDR *)lparam;
	ULONG KdpID;
	ULONG xVal = 0; // DRF - Reasonable Value If Missing InBuffer?
	ULONG yVal = 0; // DRF - Reasonable Value If Missing InBuffer?

	if (e->InBuffer()) {
		e->InBuffer()->Rewind();

		(*e->InBuffer()) >> KdpID;
		(*e->InBuffer()) >> xVal;
		(*e->InBuffer()) >> yVal;
	} else {
		assert(false); // DRF - xVal & yVal Not Initialized
	}

	pBlade->ReSizeLevel(xVal, yVal);

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC DDR::DDRPauseEvent(ULONG lparam, IDispatcher *disp, IEvent * /*e*/) {
	DDR *pBlade = (DDR *)lparam;

	if (pBlade->mAnimState == PLAYING || pBlade->mPauseState == PAUSED) {
		pBlade->mTimePaused = 0.0f;

		DDREvent *DDRPaused = new DDREvent();

		if (pBlade->mPauseState == PAUSED) {
			DDRPaused->SetFilter(DDREvent::DDR_CLI_PAUSED_SENT);
			(*DDRPaused->OutBuffer()) << 0;
			disp->QueueEvent(DDRPaused);

			pBlade->mPauseState = NOTPAUSED;
			pBlade->PauseGame(pBlade->m_gameId, false);
		} else {
			DDRPaused->SetFilter(DDREvent::DDR_CLI_PAUSED_SENT);
			(*DDRPaused->OutBuffer()) << 1;
			disp->QueueEvent(DDRPaused);

			pBlade->mPauseState = PAUSED;
			pBlade->PauseGame(pBlade->m_gameId, true);
		}
	}

	return EVENT_PROC_RC::OK;
}

void DDR::StartGameState(DDREvent *e) {
	mEvent = (*e);

	if (IsNewGame()) {
		mGameState = WAITINGONUSERTOSTART;
	} else {
		//	Starting a new level, not a new game, so don't go into wait mode
		StartGame(e);
	}
}

bool DDR::HandlePaused() {
	if (mPauseState == PAUSED) {
		mTimePaused += mCurrentFrameTimeSec;

		if (mTimePaused > MAX_PAUSE_TIME) {
			mTimePaused = 0.0f;
			mPauseState = NOTPAUSED;

			PauseGame(m_gameId, false);

			return false;
		}

		return true;
	}

	return false;
}

void DDR::StartGame(DDREvent *e) {
	//	If we're waiting on animations, don't start yet
	if (mAnimState == PLAYING) {
		mGameState = WAITINGONANIMATIONS;
		return;
	}

	//	Don't start a new game if we're paused
	if (HandlePaused())
		return;

	mbGameRunning = true;
	mLevels[mCurrentLevelIndex].currentSequence = 0;
	mLevels[mNextLevelIndex].currentSequence = 0;

	mGameState = RUNNING;
	mEngine->setMovementRestriction(IClientEngine::MR_SRC_DDR, IClientEngine::RestrictMovement_All, true);

	if (mCurrentLevel == 10 && e->m_levelId == 10)
		mTimeAddSec += 0.5f;

	mMaxGameTimeSec = ((float)e->m_num_seconds_to_play) - mTimeAddSec;

	mFirstFrame = true;
	mNumMoves = e->m_num_moves;
	mCurrentGameTimeSec = 0.0f;
	mLevelCompletedSec = 0.0f;
	mCurrentAnimTimeSec = 0.0f;

	DWORD width, height;

	mRenderer->GetViewportSize(width, height);

	//	Change actor camera
	//mEngine->SetActorsCamera( NULL, 3 );

	int XOFFSET = -124;
	int YOFFSET = 28;

	//	The co-ords for the y val are relative to the top left side of the screen
	//	Since we're currently using co-ords from the bottom left, we'll offset them
	if (!mbHasResized) {
		mStartY = (height - e->m_ctrly) + YOFFSET;
		mStartX = (e->m_ctrlx + ((e->m_ctrlWidth + XOFFSET) / 2));
	}

	//	Setup numbers
	InitNumbers();

	//	Set up animations
	mAnimData.animationIdle = (eAnimType)e->m_animation_idles[0].first;
	mAnimData.currentAnimIndex = -1;

	mTotalFlareCount = 0;

	for (auto ii = e->m_animation_flares.cbegin(); ii != e->m_animation_flares.cend(); ii++) {
		mAllFlares[mTotalFlareCount] = ii->first;
		mTotalFlareCount++;
	}

	if (mTotalFlareCount > 0 && mTotalFlareCount < 11) {
		int randNum = (rand() % mTotalFlareCount);
		mAnimData.animationflares[SUCCESS_ANIM_INDEX] = mAllFlares[randNum];

		//	Count is 3, index 0 is perfect/levelup, 1 is the flare to play
		//	and 2 is the failure animation
		mAnimData.flareCount = 3;

		//	Set the final flare to be the failure flare
		mAnimData.animationflares[FAIL_ANIM_INDEX] = FAIL_ANIM_BASE + (rand() % FAIL_ANIM_COUNT);
	}

	// Preload all gameplay animations
	AssignCurrentAnimation(mAnimData.animationIdle);

	for (int i = 0; i < mTotalFlareCount; i++)
		AssignCurrentAnimation((eAnimType)mAllFlares[i]);

	// Set the current animation to idle
	AssignCurrentAnimation(mAnimData.animationIdle);

	mCurrentLevel = e->m_levelId;
	mGameId = e->m_gameId;

	mbInAnimState = false;

	//	This is previous level ( currently unused )
	mLevels[2].numSequences = 1;

	//	If it's the first level, or we failed, then we need to init the current level and
	//	the next level
	if (mCurrentLevel == 1 || mReInitLevelsOnFail) {
		//	First level, reset the number of fail attempts
		if (IsNewGame())
			mNumFailAttempts = e->m_num_misses;

		//	First level, indexes set to display current level and next level
		mCurrentLevelIndex = 0;
		mNextLevelIndex = 1;
		mPreviousLevelIndex = -1;

		mLevels[mCurrentLevelIndex].numSequences = e->m_levelId;
		mLevels[mNextLevelIndex].numSequences = e->m_levelId + 1;

		InitializeLevel(mCurrentLevelIndex, e->m_num_moves);
		InitializeLevel(mNextLevelIndex, e->m_num_moves + 1);
	} else if (mCurrentLevel == 10) {
		int temp = mCurrentLevelIndex;
		mCurrentLevelIndex = mNextLevelIndex;
		mNextLevelIndex = temp;

		mLevels[mNextLevelIndex].numSequences = e->m_levelId;
		InitializeLevel(mNextLevelIndex, 11);
	} else {
		//	Normal level. Set the current level to be the level we displayed on the previous level
		int temp = mCurrentLevelIndex;
		mCurrentLevelIndex = mNextLevelIndex;
		mNextLevelIndex = temp;

		mLevels[mNextLevelIndex].numSequences = e->m_levelId + 1;
		InitializeLevel(mNextLevelIndex, e->m_num_moves + 1);
	}

	//	Reset failure
	mReInitLevelsOnFail = false;

	//	Save instance ID
	mPrevInstanceId = GetGameInstanceId();

	mDisplayNextLevel = false;
}

float DDR::GetAnimTime(int index) {
	//	If the index is > 12, the character is male
	//	else female
	if (mEngine->GetMyRefModelIndex() > 12) {
		return AnimTimesM[index];
	} else {
		return AnimTimes[index];
	}
}

void DDR::PlayAnimSequence(bool didFail, int IndexToStart) {
	if (mAnimData.flareCount > 0) {
		int index = didFail ? (mAnimData.flareCount - 1) : IndexToStart;

		mbInAnimState = true;
		mAnimState = PLAYING;

		//	Start of anim sequence
		mAnimData.currentAnimIndex = index;
		mCurrentAnimTimeSec = 0.0f;

		mAnimData.animTimeSec = 2.4 * AssignCurrentAnimation((eAnimType)mAnimData.animationflares[mAnimData.currentAnimIndex]);

#ifdef DEBUG
		char buff[255];
		sprintf_s(
			buff,
			_countof(buff),
			"Played Anim:(Initial %d): %d With time: %.2f\n",
			mAnimData.currentAnimIndex,
			mAnimData.animationflares[mAnimData.currentAnimIndex] - MAGIC_ANIM_OFFSET_NUMBER,
			mAnimData.animTimeSec);

		OutputDebugStringA(buff);
#endif
	}
}

bool DDR::UpdateAnims() {
	//	Check to see if a sequence is playing
	if (mAnimData.currentAnimIndex == -1) {
		//	Start idle, if not in an animstate
		AssignCurrentAnimation(mAnimData.animationIdle);
		return false;
	}

	mCurrentAnimTimeSec += mCurrentFrameTimeSec;

	//	Has this animation been playing for the alloted amount of time ?
	if (mCurrentAnimTimeSec >= mAnimData.animTimeSec) {
		mAnimData.currentAnimIndex++;

		//	Next anim. Check to see if there's more anims to this flare.
		//	If there is, increase index, reset timer
		//	If there isnt, reset index and timers. The next game will determine the idle dance.
		if (mAnimData.currentAnimIndex >= mAnimData.flareCount - 1) {
			//	Done with playing, reset
			mAnimState = NOTPLAYING;
			mAnimData.currentAnimIndex = -1;
			mCurrentAnimTimeSec = 0.0;
			mAnimData.animTimeSec = 0.0;

			AssignCurrentAnimation(mAnimData.animationIdle);
		} else {
			//	Play the next animation, we only do this if there is 3 more more total flares
			//	in the case of 3 total flares, there would be 2 success flares and 1 fail flare.
			mAnimData.animTimeSec = 2.4 * AssignCurrentAnimation((eAnimType)mAnimData.animationflares[mAnimData.currentAnimIndex]);
			mCurrentAnimTimeSec = 0.0;

#ifdef DEBUG
			char buff[255];
			sprintf_s(
				buff,
				_countof(buff),
				"Played Anim:(Sequence %d) %d With time: %.2f\n",
				mAnimData.currentAnimIndex,
				mAnimData.animationflares[mAnimData.currentAnimIndex] - MAGIC_ANIM_OFFSET_NUMBER,
				mAnimData.animTimeSec);

			OutputDebugStringA(buff);
#endif
		}
	}

	return true;
}

TimeSec DDR::AssignCurrentAnimation(eAnimType animType, bool repeat) {
	if (!mEngine)
		return 0.0;

	const int animVer = 500;
	TimeSec animTimeSec = mEngine->AssignCurrentAnimation(animType, animVer, repeat);

	// is the animation resident?
	if (animTimeSec != 0.0)
		return animTimeSec;

	// play the dance idle animation for 1 sec instead
	mEngine->AssignCurrentAnimation(mAnimData.animationIdle, animVer, repeat);
	return 1.0;
}

void DDR::UpdateTime() {
	LARGE_INTEGER currTime, clockFreq;
	float diffTime;

	QueryPerformanceCounter(&currTime);

	if (mFirstFrame) {
		mFirstFrame = false;
		mLastTime = currTime.LowPart;

		return;
	}

	QueryPerformanceFrequency(&clockFreq);
	diffTime = (float)(currTime.LowPart - mLastTime);
	mLastTime = currTime.LowPart;

	mCurrentFrameTimeSec = diffTime / clockFreq.LowPart;

	if (mFailDelaySec > 0.0f)
		mFailDelaySec -= mCurrentFrameTimeSec;
}

void DDR::UpdateIntro() {
	mCurrentNumberTime += mCurrentFrameTimeSec;

	if (mCurrentNumberTime > INTRO_DELAY_TIME) {
		mCurrentNumberTime = 0.0f;
		mCurrentNumberToDisplay++;
	}

	if (mCurrentNumberToDisplay > 3) {
		mCurrentNumberTime = 0.0f;
		mCurrentNumberToDisplay = 0;
		mGameState = RUNNING;
	}
}

void DDR::Update() {
	UpdateTime();

	//	Check to see if the games is over ( and we're waiting on the final animations )
	if (mGameState == GAMEOVER) {
		UpdateAnims();

		if (mAnimState == NOTPLAYING) {
			mGameState = NOTRUNNING;

			//	Set the animation back to idle
			mEngine->AssignCurrentAnimation(eAnimType::Stand);

			GameOverDialog(mGameId, mCurrentFameAwarded, mPercentNextFameLevel, mRewardsNextFameLevel, mGainMessage);
		}
		return;
	}

	//	Check to see if we were waiting on anims to
	//	finish and start a new game
	if (mGameState == WAITINGONANIMATIONS) {
		StartGame(&mEvent);
		UpdateAnims();
		return;
	}

	if (mGameState == WAITINGONUSERTOSTART) {
		mbWaitingForUser = false;
		mbHasAccepted = false;

		mCurrentFameAwarded = 0;
		mPercentNextFameLevel = 0;
		mRewardsNextFameLevel = "";
		mGainMessage = "";

		ReadyToPlay();

		StartGame(&mEvent);

		mGameState = READYSETGO;

		return;
	}

	if (mGameState == READYSETGO) {
		UpdateAnims();
		UpdateIntro();

		return;
	}

	if (!mbGameRunning)
		return;

	Sequence *pSequence = &mLevels[mCurrentLevelIndex].sequences[mLevels[mCurrentLevelIndex].currentSequence];

	static bool startGame = false;

	//	Increase game time
	mCurrentGameTimeSec += mCurrentFrameTimeSec;

	//	If we are in an animation sequence, then don't update the game
	//	or start a new one
	if (UpdateAnims() || mbWaitingForScore || (mGameState >= WAITINGONRELOAD))
		return;

	//	Should we start a new sequence?
	if (mSequenceToStart > 0) {
		//	Don't start a new sequence if we're paused
		if (HandlePaused())
			return;

		mLevels[mCurrentLevelIndex].currentSequence = mSequenceToStart;
		mSequenceToStart = 0;
		mbInAnimState = false;

		//	Get a new flare animation
		int randNum = (rand() % mTotalFlareCount);
		mAnimData.animationflares[SUCCESS_ANIM_INDEX] = mAllFlares[randNum];

		mGameState = RUNNING;

		//	Reset Timers
		mCurrentGameTimeSec = 0.0f;
		mLevelCompletedSec = 0.0f;
		mCurrentAnimTimeSec = 0.0;
		return;
	}

	bool sequenceComplete = false;
	bool levelUp = false;

	//	First update the arrow slot
	if (pSequence->currentSlot >= pSequence->numArrows) {
		pSequence->currentSlot = 0;

		sequenceComplete = true;

		//	Set victory data
		mCurrentSuccessPosition = 0.0f;
		mWhichSuccessMessage = rand() % 5;

		//	Finished all the arrows on this slot, increase sequence
		//	Set up to start a new sequence
		mSequenceToStart = mLevels[mCurrentLevelIndex].currentSequence + 1;

		mLevelCompletedSec = mCurrentGameTimeSec;

		levelUp = (mSequenceToStart == mLevels[mCurrentLevelIndex].numSequences);

		float numSecondsForPerfect = (float)mEvent.m_perfect_score_ceiling;

		bool perfectAchieved = (mLevelCompletedSec <= numSecondsForPerfect);

		if (perfectAchieved) {
			numPerfectsInARow++;

			//	Set the animation for perfect, if they finished before perfect cutoff
			//	Set index 0 to be perfect
			mAnimData.animationflares[PERFECT_ANIM_INDEX] = mEvent.m_animation_perfects[0].first;

			//	Start the animation at index 0
			PlayAnimSequence(false, 0);
		} else {
			numPerfectsInARow = 0;

			//	Just play the flare without perfect at index 0
			PlayAnimSequence(false);
		}

		//	Record a score for completed sequence or level
		bool ApplyPerfectBonus = numPerfectsInARow >= 3; //mEvent.m_level_bonus_trigger_num;

		bool useAlternate = false;

		if (mEngine->GetMyRefModelIndex() <= 12) {
			useAlternate = true;
		}

		RecordScore(mGameId, mCurrentLevel, (int)(mLevelCompletedSec * 1000), levelUp, false, ApplyPerfectBonus, perfectAchieved, useAlternate);
	}

	//	Check to see if we should level up
	//	if the sequence to start is == max sequence for this level, then it's a level up
	if (levelUp) {
		//	Set state to wait on level load
		mGameState = WAITINGONRELOAD;

		mDisplayNextLevel = true;

		//	Don't start a new sequence, since we're starting a new level
		mSequenceToStart = 0;

		//	Set animation to level up
		mAnimData.animationflares[PERFECT_ANIM_INDEX] = mEvent.m_animation_levelups[0].first;

		//	Start the animation at index 0
		PlayAnimSequence(false, 0);

		//	Play level up sound
		mEngine->PlayBySoundID(6, false);

		//	Finished all the sequences on this level, increase level
		//	Play level complete sound
		mEngine->PlayBySoundID(3, false);
		mDidFail = false;

		if (mCurrentLevel < 10)
			mLevelToStart = mCurrentLevel + 1;
		else {
			mLevelToStart = 10;
		}

		StartLevelData(mLevelToStart);
	}

	//	Check time
	if (mCurrentGameTimeSec >= mMaxGameTimeSec && mLevelCompletedSec == 0.0f) {
		//	Level failed based on time
		mLevelCompletedSec = mMaxGameTimeSec;
		mDidFail = true;
		HandleLevelFail();
	}
}

void DDR::RenderLivesRemaining() {
#define LIFE_QUAD_X 15
#define LIFE_QUAD_Y 25

	struct LifeQuad {
		QUAD_POINT LowerLeft;
		QUAD_POINT UpperRight;

		QUAD_UV lowerLeftUV;
		QUAD_UV upperRightUV;
	};

	//	Assemble a structure of 5 life quads
	LifeQuad lifeQuads[5];

	float LIFE_X_OFFSET = 165.0f;
	float LIFE_Y_OFFSET = -126.0f;

	float LIFE_U_OFFSET = 0.18f;
	float LIFE_V_OFFSET = 0.30f;

	float LIFE_U_SIZE = 0.118f;
	float LIFE_V_SIZE = 0.39f;

	static std::string texName("DDRLives");
	//DWORD color = D3DCOLOR_RGBA(255, 255, 255, 255);

	for (int i = 0; i < 5; i++) {
		lifeQuads[i].LowerLeft.x = mStartX + LIFE_X_OFFSET + (i * LIFE_QUAD_X);
		lifeQuads[i].LowerLeft.y = mStartY + LIFE_Y_OFFSET;

		lifeQuads[i].UpperRight.x = mStartX + LIFE_X_OFFSET + LIFE_QUAD_X + (i * LIFE_QUAD_X);
		lifeQuads[i].UpperRight.y = mStartY + LIFE_Y_OFFSET + LIFE_QUAD_Y;

		lifeQuads[i].lowerLeftUV.u = LIFE_U_OFFSET + (i * LIFE_U_SIZE);
		lifeQuads[i].lowerLeftUV.v = LIFE_V_OFFSET + LIFE_V_SIZE;

		lifeQuads[i].upperRightUV.u = LIFE_U_OFFSET + (i * LIFE_U_SIZE) + LIFE_U_SIZE;
		lifeQuads[i].upperRightUV.v = LIFE_V_OFFSET;
	}

	for (int i = 4 - (mNumFailAttempts - 1); i < 5; i++) {
		mRenderer->DrawQuad(lifeQuads[i].LowerLeft, lifeQuads[i].UpperRight, texName, D3DTEXF_POINT,
			lifeQuads[i].lowerLeftUV, lifeQuads[i].upperRightUV);
	}
}

void DDR::Render() {
	if (!mbGameRunning)
		return;

	if (mCurrentLevelIndex == -1)
		return;

	static std::string texName("danceArrows");
	DWORD color;

	Sequence *pSequence = &mLevels[mCurrentLevelIndex].sequences[mLevels[mCurrentLevelIndex].currentSequence];
	int numArrowSlots = pSequence->numArrows;
	int currentArrowSlot = pSequence->currentSlot;

	ArrowSlot *pSlot;

	RenderLivesRemaining();
	RenderNumbers();

	//	Render time remaining bar
	{
		QUAD_POINT timeBarBL;
		QUAD_POINT timeBarTR;

		color = D3DCOLOR_RGBA(255, 255, 0, 100);

		float beginPointX = mStartX - 93.0f;
		float beginPointY = mStartY - 36.0f;
		float barWidth = 186.0f;
		float barHeight = 9.0f;

		float endPointX;

		//	If we're animating, ignore game time
		if (!mbInAnimState) {
			if (mLevelCompletedSec > 0.0f)
				endPointX = LERP(beginPointX, beginPointX + barWidth, mMaxGameTimeSec, mLevelCompletedSec);
			else
				endPointX = LERP(beginPointX, beginPointX + barWidth, mMaxGameTimeSec, mCurrentGameTimeSec);
		} else
			endPointX = beginPointX;

		mRenderer->DrawQuad(QUAD_POINT(beginPointX, beginPointY), QUAD_POINT(endPointX, beginPointY + barHeight), color);
	}

	static bool frameDelay = false;

	//	Render Current Level sequence or victory message
	if (mAnimState == PLAYING && mWhichSuccessMessage > -1) {
		//	Render the current success message
		QUAD_POINT lowerLeft;
		QUAD_POINT upperRight;
		frameDelay = true;

#define XVICTORYOFFSET -145.0f
#define YVICTORYOFFSET -73.0f

#define XLEVELUPOFFSET -20.0f
#define YLEVELUPOFFSET -73.0f

		if (mWhichSuccessMessage == 5) {
			float xOffset = OSSCILATING_LERP(0.0f, 156.0f, 1.5f, mCurrentSuccessPosition);

			mCurrentSuccessPosition += mCurrentFrameTimeSec;

			lowerLeft.x = (float)(mStartX - 512 / 2 + xOffset + XLEVELUPOFFSET);
			lowerLeft.y = (float)(mStartY - 48 / 2 + YLEVELUPOFFSET);

			upperRight.x = (float)(mStartX + 512 / 2 + xOffset + XLEVELUPOFFSET);
			upperRight.y = (float)(mStartY + 48 / 2 + YLEVELUPOFFSET);

			mRenderer->DrawQuad(lowerLeft, upperRight, mSuccessMessages[mWhichSuccessMessage].textureName);
		} else if (mWhichSuccessMessage < 5) {
			int displayNumbers[MAX_SCORE_SLOTS];
			int numDigits = ConvertToArray(displayNumbers, mCurrentFameEarned);
			float xOffsetMax = 516.0f - SUCCESS_MESSAGE_XSIZE - numDigits * 20;

			float xOffset = OSSCILATING_LERP(0.0f, xOffsetMax, 1.5f, mCurrentSuccessPosition);

			mCurrentSuccessPosition += mCurrentFrameTimeSec;

			lowerLeft.x = (float)(mStartX - (SUCCESS_MESSAGE_XSIZE / 2) + xOffset + XVICTORYOFFSET);
			lowerLeft.y = (float)(mStartY - (SUCCESS_MESSAGE_YSIZE / 2)) + YVICTORYOFFSET;

			upperRight.x = (float)(mStartX + (SUCCESS_MESSAGE_XSIZE / 2) + xOffset + XVICTORYOFFSET);
			upperRight.y = (float)(mStartY + (SUCCESS_MESSAGE_YSIZE / 2)) + YVICTORYOFFSET;

			static std::string levelUpString = "dance_fame_plus";
			mRenderer->DrawQuad(lowerLeft, upperRight, levelUpString);

			lowerLeft.x += SUCCESS_MESSAGE_XSIZE;
			upperRight.x += 40;
			for (int i = numDigits - 1; i >= 0; i--) {
				lowerLeft.x -= mFameNumbers[displayNumbers[i]].charStartX; // move the texture back to get the character to start at the right location
				mRenderer->DrawQuad(lowerLeft, upperRight, mFameNumbers[displayNumbers[i]].textureName);
				lowerLeft.x += mFameNumbers[displayNumbers[i]].charStartX + mFameNumbers[displayNumbers[i]].charWidthX + 10; // set up the next character to start at the right location
				upperRight.x = lowerLeft.x + 40;
			}
		}
	} else {
		if (!frameDelay) {
			for (int i = 0; i < numArrowSlots; i++) {
				pSlot = &pSequence->Slots[i];
				float fSelectedOffset = 0.0f;

				//	First case, the arrow has been successfully completed
				if (pSlot->bSelected) {
					fSelectedOffset = 0.002f;
				}

				//	Second case, we're currently on this arrow
				else if (currentArrowSlot == i && mReInitLevelsOnFail) {
					//	Offset to red
					fSelectedOffset = 0.335f;

				}

				//	Else, we've yet to arrive at this arrow
				else {
					fSelectedOffset = 0.668f;
				}

				QUAD_UV finalUV1(pSlot->lowerLeftUV.u + fSelectedOffset, pSlot->lowerLeftUV.v);
				QUAD_UV finalUV2(pSlot->upperRightUV.u + fSelectedOffset, pSlot->upperRightUV.v);

				color = D3DCOLOR_RGBA(255, 255, 255, 255);

				mRenderer->DrawQuad(pSlot->lowerLeft, pSlot->upperRight,
					texName, D3DTEXF_LINEAR, finalUV1, finalUV2);
			}

		} else {
			frameDelay = false;
		}
	}

	//	Render intro sequence, if we're in an intro state
	if (mGameState == READYSETGO) {
#define NUMBER_SIZEX 128
#define NUMBER_SIZEY 128

		QUAD_POINT lowerLeft;
		QUAD_POINT upperRight;

		lowerLeft.x = (float)(mStartX - (NUMBER_SIZEX / 2));
		lowerLeft.y = (float)(mStartY - (NUMBER_SIZEY / 2));

		upperRight.x = (float)(mStartX + (NUMBER_SIZEX / 2));
		upperRight.y = (float)(mStartY + (NUMBER_SIZEY / 2));

		std::string numToRender;

		switch (mCurrentNumberToDisplay) {
			case 0:
				numToRender = "3";
				break;
			case 1:
				numToRender = "2";
				break;
			case 2:
				numToRender = "1";
				break;
			case 3:
				numToRender = "Go";
				break;
			default:
				numToRender = "3";
				break;
		}

#define SIZE_TO_SCALE_BY (NUMBER_SIZEX * 0.3f)

		int alpha = (int)NLERP(0.0f, 255.0f, INTRO_DELAY_TIME, mCurrentNumberTime);

		DWORD IntroAlpha = D3DCOLOR_ARGB(alpha, 255, 255, 255);

		mRenderer->DrawQuadEx(lowerLeft, upperRight, numToRender, IntroAlpha, SIZE_TO_SCALE_BY, mCurrentNumberTime, INTRO_DELAY_TIME);
	}
}

} // namespace KEP
