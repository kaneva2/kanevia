///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEventBlade.h"
#include "Event\Base\IEventHandler.h"
#include "..\IClientRenderBlade.h"
#include "Event\Events\DDREvent.h"
#include "..\..\..\Platform\RenderEngine\ReDynamicGeom.h"

#include "common\include\CoreStatic\CBoneAnimationNaming.h"

#define MAX_SLOTS 15
#define MAX_LEVELS 10

namespace KEP {
class EnvParticleSystem;

// Event Handling class
class DDR : public IEventBlade, public IClientRenderBlade {
public:
	static const LONG DDR_OBJECT_1 = 103; // Global object id

	DDR();

	// set the blade interface version
	static ULONG BladeVersion() { return PackVersion(1, 0, 0, 0); }
	virtual Monitored::DataPtr monitor() const { return Monitored::DataPtr(new Monitored::Data()); }

	// IBlade overrides
	virtual ULONG Version() const { return PackVersion(1, 0, 0, 0); }

	// BaseDir is scriptPath For Event Blades
	virtual std::string PathBase() const { return m_scriptDir; }
	virtual void SetPathBase(const std::string &baseDir, bool readonly) {
		m_scriptDir = baseDir;
		m_scriptDirReadOnly = readonly;
	}

	virtual void Delete() { delete this; }
	virtual void FreeAll();
	virtual const char *Name() const { return m_name.c_str(); }

	virtual void StartLevel(DDREvent *e);

	virtual LONG GetNetAssignedId() { return m_netAssignedId; }
	virtual void SetNetAssignedId(LONG netAssignedId) { m_netAssignedId = netAssignedId; }

	virtual void SetGameId(ULONG gameId) { m_gameId = gameId; }
	virtual ULONG GetGameId() { return m_gameId; }

	virtual void SetObjectId(int objectId) { m_objectId = objectId; }
	virtual int GetObjectId() { return m_objectId; }
	inline virtual void SetGameCtrlPosition(int ctrlx, int ctrly, int ctrlWidth, int ctrlHeight) {
		m_ctrlx = ctrlx;
		m_ctrly = ctrly;
		m_ctrlWidth = ctrlWidth;
		m_ctrlHeight = ctrlHeight;
	}
	inline virtual void GetGameCtrlPosition(int &ctrlx, int &ctrly, int &ctrlWidth, int &ctrlHeight) {
		ctrlx = m_ctrlx;
		ctrly = m_ctrly;
		ctrlWidth = m_ctrlWidth;
		ctrlHeight = m_ctrlHeight;
	}

	virtual void StartLevelData(ULONG levelId);

	// IEventBlade overrides
	bool RegisterEvents(IDispatcher *dispatcher, IEncodingFactory *factory);
	bool RegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory);
	bool ReRegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory);

	virtual int GetGameInstanceId() { return m_gameInstanceId; }
	virtual void SetGameInstanceId(int gameInstanceId) { m_gameInstanceId = gameInstanceId; }

	virtual ULONG GetPreviousHighLevelId() { return m_prevHighLevlId; }
	virtual void SetPreviousHighLevelId(ULONG prevHighLevelId) { m_prevHighLevlId = prevHighLevelId; }

	virtual bool RecordScore(ULONG gameId, ULONG levelId, ULONG numMilliSecondsExpired, bool levelComplete, bool gameOver, bool perfectBonus, bool perfectAchieved, bool useAlternate);
	virtual void PostedScore(ULONG gameId, ULONG levelId, int gameInstanceId, int playerId, ULONG score, ULONG highScore, ULONG famePointsEarned, int newFameLevel, int percentToNextLevel, std::string rewardsAtNextLevel, std::string gainMessage);

	virtual void CloseGameDialog();
	virtual void GameOverDialog(ULONG gameId, int famePointsAwarded, int percentToNext, std::string rewardsAtNext, std::string gainMessage);

	virtual void ReadyToPlay();

	virtual bool IsNewGame() { return m_newGame; }
	virtual void SetNewGame(bool newGame) { m_newGame = newGame; }

	virtual bool IsLeavingFloor() { return m_isLeavingFloor; }
	virtual void IsLeavingFloor(bool leavingFloor) { m_isLeavingFloor = leavingFloor; }

	virtual void PauseGame(ULONG gameId, bool isPaused);

	log4cplus::Logger m_logger;

	enum ARROW {
		Left = 0,
		Right,
		Up,
		Down,
	};

	enum GAMEPAUSE {
		PAUSED = 0,
		NOTPAUSED,
	};

	enum ANIMSTATE {
		NOTPLAYING = 0,
		PLAYING,
	};

	enum GAMESTATE {
		NOTRUNNING = 0,
		WAITINGONUSERTOSTART,
		READYSETGO,
		RUNNING,
		WAITINGONRELOAD,
		WAITINGONANIMATIONS,
		GAMEOVER,
	};

	struct SUCCESSMESSAGE {
		std::string textureName;
	};

	struct FAMENUMBERS {
		std::string textureName;

		int charStartX;
		int charWidthX;
	};

	struct ArrowSlot {
		QUAD_POINT lowerLeft;
		QUAD_POINT upperRight;

		QUAD_POINT halfLowerLeft;
		QUAD_POINT halfUpperRight;

		QUAD_UV lowerLeftUV;
		QUAD_UV upperRightUV;

		//	For animation of the square
		QUAD_POINT animOffsets;

		//	Arrow type
		ARROW arrows;

		//	The key this slot requires to trigger
		int key;

		//	Has this slot been highlighted?
		bool bSelected;
	};

	struct Sequence {
		ArrowSlot Slots[50];

		//	The number of arrows in this sequence
		int numArrows;
		int currentSlot;
	};

	struct LEVEL {
		Sequence sequences[MAX_LEVELS];

		//	Number of sequence in this level
		int numSequences;
		int currentSequence;
	};

	struct AnimData {
		TimeSec animTimeSec;
		int currentAnimIndex;

		eAnimType animationIdle;
		ULONG animationFail;

		int flareCount;
		ULONG animationflares[10];

		AnimData() {
			currentAnimIndex = -1;
			animTimeSec = 0.0;
			flareCount = 0;
		}
	};

	static EVENT_PROC_RC DDRInputHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC DDRMenuHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC DDRPauseEvent(ULONG lparam, IDispatcher *disp, IEvent *e);

	EVENT_PROC_RC ProcessKeys(ULONG data);
	bool RegisterEvents(IDispatcher *dispatcher);
	bool RegisterEventHandlers(IDispatcher *dispatcher);

	void PlayAnimSequence(bool didFail, int IndexToStart = 1);
	TimeSec AssignCurrentAnimation(eAnimType animType, bool repeat = true);
	float GetAnimTime(int index);
	bool UpdateAnims();

	virtual bool Initialize();
	virtual const std::string &GetName() const { return mBladeName; }

	void InitializeSequence(Sequence *pSequence, int numArrows, bool reInit = false);
	void UpdateTime();
	void HandleSlotUpdate(ArrowSlot *pSlot);
	void HandleGameExit();
	void HandleLevelFail();
	void HandleKeyPressFail();
	void InitializeLevel(int level, ULONG numArrows, bool reInit = false);
	void StartGameState(DDREvent *e);
	void RenderLivesRemaining();
	void ReSizeLevel(ULONG xVal, ULONG yVal);
	void StartGame(DDREvent *e);
	void InitNumbers();
	bool HandlePaused();
	void RenderNumbers();
	void UpdateIntro();
	virtual void Update();
	virtual void Render() override;

	void SetWaitingOnScoreUpdate(bool var) { mbWaitingForScore = var; }
	int MapGlobalIdToLegacyGameId(int globalId) const;

protected:
	virtual bool IsBaseDirReadOnly() const { return m_scriptDirReadOnly; }

private:
	// for IBlade support
	std::string m_scriptDir;
	bool m_scriptDirReadOnly;
	std::string m_name;
	IDispatcher *m_dispatcher;

	ULONG m_gameId;
	int m_objectId;
	LONG m_netAssignedId;
	ULONG m_prevHighLevlId;

	int m_gameInstanceId;
	int m_ctrlx;
	int m_ctrly;
	int m_ctrlWidth;
	int m_ctrlHeight;
	bool m_newGame;
	bool m_isLeavingFloor;

	int mCurrentLevel;
	int mGameId;

	//	Managers
	IClientEngine *mEngine;
	std::string mBladeName;
	ReDynamicGeom *mRenderer;
	DDREvent mEvent;

	//	Particles
	EnvParticleSystem *mParticleSystem;

	//	Anim
	AnimData mAnimData;

	//	Current Y position
	int mStartY;
	int mStartX;

	//	Sound ID's
	int mFailSound;

	//	Timings
	bool mFirstFrame;
	int mWhichSuccessMessage;
	float mCurrentNumberTime;
	float mCurrentSuccessPosition;
	float mTimePaused;
	bool mbHasResized;
	bool mbWaitingForScore;
	bool mbWaitingForUser;
	bool mbHasAccepted;
	int mNumFailAttempts;
	int mPrevInstanceId;
	ULONG mLastTime;
	ULONG numPerfectsInARow;
	bool mDidFail;
	int mScoreToRender;
	bool mReInitLevelsOnFail;

	TimeSec mLevelCompletedSec;
	TimeSec mTimeAddSec;
	TimeSec mFailDelaySec;
	TimeSec mMaxGameTimeSec;
	TimeSec mCurrentFrameTimeSec;
	TimeSec mCurrentGameTimeSec;
	TimeSec mCurrentAnimTimeSec;

	//	3 Different Level Slots
	LEVEL mLevels[3];
	ULONG mAllFlares[10];

	int mTotalFlareCount;
	int mNumMoves;
	int mCurrentNumberToDisplay;
	int mCurrentLevelIndex;
	bool mDisplayNextLevel;
	bool mbInAnimState;
	int mLevelToStart;
	int mSequenceToStart;
	int mNextLevelIndex;
	int mPreviousLevelIndex;

	ULONG mKeyboardData;
	int mCurrentSlot;
	bool mbGameRunning;

	//	Success Messages
	SUCCESSMESSAGE mSuccessMessages[15];

	//   Fame Numbers
	FAMENUMBERS mFameNumbers[10];

	//	Current game state
	GAMESTATE mGameState;

	//	Current anim state
	ANIMSTATE mAnimState;

	//	Current pause state
	GAMEPAUSE mPauseState;

	// current fame points awarded
	int mCurrentFameAwarded;

	// percentage to the next fame level
	int mPercentNextFameLevel;

	// rewards available on next fame level
	std::string mRewardsNextFameLevel;

	// message sent to the user concerning rewards giving at level up
	std::string mGainMessage;

	int mCurrentFameEarned;
};

} // namespace KEP
