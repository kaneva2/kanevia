///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "DDR.h"
#include "resource.h"

#include "Event\Events\TriggerEvent.h"
#include "Event\Events\DDREvent.h"
#include "Event\Base\IDispatcher.h"
#include "Event\Base\IEncodingFactory.h"
#include "Common\include\KEPHelpers.h"

#include <assert.h>

using namespace std;
using namespace log4cplus;

static bool g_ftt = false;

//----------------------------------------------------------------------
// NOTEs for ED-3065
//----------------------------------------------------------------------
// Since object ID is obsolete and replaced by global ID in trigger event, we will need to either
// remap existing records to use global IDs in the DB, or remap global IDs to legacy game IDs in
// the code.
// There are tons of existing DB records out there in both WOK and incubator. Hence I'll remap from
// code for now until we completely retire dev worlds with this g_globalIdToLegacyGameIdMap.
//----------------------------------------------------------------------
static std::map<int, int> g_globalIdToLegacyGameIdMap{
	{ 0, 0 }, // Sometimes 0 is used to indicate that game is turned off
	{ 1784, 369 },
	{ 1835, 370 },
	{ 1836, 371 },
	{ 3001, 883 },
	{ 3323, 1101 },
	{ 3324, 1102 },
	{ 3564, 1321 },
	{ 4933, 2474 },
	{ 4934, 2473 },
	{ 4935, 2475 },
};

namespace KEP {
int DDR::MapGlobalIdToLegacyGameId(int globalId) const {
	auto itr = g_globalIdToLegacyGameIdMap.find(globalId);
	if (itr == g_globalIdToLegacyGameIdMap.end()) {
		// Something is wrong - just report it
		assert(false);
		LOG4CPLUS_ERROR(m_logger, "DDRGameTriggerFn: Invalid DDR game object global ID: " << globalId);
		return 0;
	} else {
		return itr->second;
	}
}

EVENT_PROC_RC DDRGameStartFn(ULONG lparam, IDispatcher * /*disp*/, IEvent *e) {
	DDREvent *ddre = dynamic_cast<DDREvent *>(e);
	jsVerifyReturn(ddre != 0, EVENT_PROC_RC::NOT_PROCESSED);
	DDR *me = (DDR *)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	ddre->ExtractInfo();

	me->SetObjectId(ddre->m_objectId);
	me->SetGameId(me->MapGlobalIdToLegacyGameId(ddre->m_gameId));
	me->SetNetAssignedId(ddre->m_networkAssignedId);
	me->SetGameCtrlPosition(ddre->m_ctrlx, ddre->m_ctrly, ddre->m_ctrlWidth, ddre->m_ctrlHeight);
	me->SetNewGame(true);
	g_ftt = true;
	me->StartLevelData(1);
	me->SetGameInstanceId(0);
	me->IsLeavingFloor(false);

	return ret;
};

EVENT_PROC_RC DDRGameEndFn(ULONG lparam, IDispatcher * /*disp*/, IEvent *e) {
	DDREvent *ddre = dynamic_cast<DDREvent *>(e);
	jsVerifyReturn(ddre != 0, EVENT_PROC_RC::NOT_PROCESSED);
	DDR *me = (DDR *)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	me->IsLeavingFloor(true);

	me->HandleGameExit();

	return ret;
};

EVENT_PROC_RC DDRGameTriggerFn(ULONG lparam, IDispatcher * /*disp*/, IEvent *e) {
	DDR *me = (DDR *)lparam;

	TriggerEvent *tre = dynamic_cast<TriggerEvent *>(e);
	if (tre) {
		LONG netAssignedId;
		LONG objectPlacementId;
		LONG globalId;
		string miscStr;
		eTriggerEvent triggerEvent;
		int miscInt;
		float radius = 0.0;
		double x, y, z;
		double xCenter, yCenter, zCenter;
		int triggerId;

		tre->ExtractInfo(netAssignedId, objectPlacementId, globalId, triggerEvent, radius, miscInt, miscStr, x, y, z, xCenter, yCenter, zCenter, triggerId);

		if (triggerEvent == eTriggerEvent::Enter) {
			me->SetGameId(me->MapGlobalIdToLegacyGameId(globalId));
			me->SetNetAssignedId(netAssignedId);
			me->StartLevelData(1);
		} else if (triggerEvent == eTriggerEvent::Exit) {
			me->SetGameId(0);
			me->SetNetAssignedId(0);
			me->SetGameInstanceId(0);
			me->HandleGameExit();
		}

		return EVENT_PROC_RC::CONSUMED;
	}

	return EVENT_PROC_RC::OK;
};

EVENT_PROC_RC DDRGameLevelDataFn(ULONG lparam, IDispatcher * /*disp*/, IEvent *e) {
	DDREvent *ddre = dynamic_cast<DDREvent *>(e);
	jsVerifyReturn(ddre != 0, EVENT_PROC_RC::NOT_PROCESSED);
	DDR *me = (DDR *)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	ddre->ExtractInfo();

	me->GetGameCtrlPosition(ddre->m_ctrlx, ddre->m_ctrly, ddre->m_ctrlWidth, ddre->m_ctrlHeight);

	me->StartLevel(ddre);

	return ret;
};

EVENT_PROC_RC DDRPostedScoreFn(ULONG lparam, IDispatcher * /*disp*/, IEvent *e) {
	DDREvent *ddre = dynamic_cast<DDREvent *>(e);
	jsVerifyReturn(ddre != 0, EVENT_PROC_RC::NOT_PROCESSED);
	DDR *me = (DDR *)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	ddre->ExtractInfo();

	me->PostedScore(ddre->GetGameId(), ddre->GetGameLevel(), ddre->m_gameInstanceId, ddre->m_playerId, ddre->m_score, ddre->m_highScore, ddre->m_famePointsEarned, ddre->m_newFameLevel, ddre->m_percentToNextLevel, ddre->m_rewardsAtNextLevel, ddre->m_gainMessage);

	return ret;
};

} // namespace KEP

using namespace KEP;

DDR::DDR() :
		m_logger(Logger::getInstance(L"Blade")), m_scriptDirReadOnly(false) {
	// set the name of this handler to the RTTI name ("class KEP::DDR", in this case)
	m_name = typeid(*this).name();

	m_gameId = 0;
	mFirstFrame = true;
	m_dispatcher = NULL;
	m_gameInstanceId = 0;
	m_newGame = false;

	mFailDelaySec = 0.0f;
	mbWaitingForUser = false;
	mbHasAccepted = false;
	mDidFail = false;
	mCurrentLevelIndex = 0;
	mNextLevelIndex = 0;
	mPreviousLevelIndex = 0;
	mLevelCompletedSec = 0.0f;
	mBladeName = "DDRRender";
	mCurrentSlot = 0;
	mLastTime = 0;
	mbGameRunning = false;
	IClientRenderBlade::m_bladeDepth = ClearsZ;
}

bool DDR::RegisterEvents(IDispatcher *dispatcher, IEncodingFactory *ef) {
	if (dispatcher == 0 || ef == 0) {
		assert(false);
		return false;
	}

	// init static factory in this DLL
	IEvent::SetEncodingFactory(ef);

	IGame *game = dispatcher->Game();
	if (game != 0 && game->EngineType() == eEngineType::Client) {
		RegisterEvent<DDREvent>(*dispatcher);
	}

	return true;
}

bool DDR::RegisterEvents(IDispatcher *dispatcher) {
	if (dispatcher == 0) {
		assert(false);
		return false;
	}

	// init static factory in this DLL
	IEvent::SetEncodingFactory(dispatcher->EncodingFactory());

	IGame *game = dispatcher->Game();
	if (game != 0 && game->EngineType() == eEngineType::Client) {
		RegisterEvent<DDREvent>(*dispatcher);
	}

	return true;
}

bool DDR::RegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *ef) {
	if (dispatcher == 0 || ef == 0) {
		assert(false);
		return false;
	}

	// init static factory in this DLL
	IEvent::SetEncodingFactory(ef);

	IGame *game = dispatcher->Game();
	if (game != 0 && game->EngineType() == eEngineType::Client) {
		// now register handlers
		EVENT_ID ddr_id = dispatcher->GetEventId("DDREvent");
		dispatcher->RegisterFilteredHandler(DDRGameStartFn, (ULONG)this, "DDRGameStartFn", DDREvent::ClassVersion(), ddr_id, DDREvent::DDR_START_GAME);
		dispatcher->RegisterFilteredHandler(DDRGameLevelDataFn, (ULONG)this, "DDRGameLevelDataFn", DDREvent::ClassVersion(), ddr_id, DDREvent::DDR_START_LEVEL_DATA);
		dispatcher->RegisterFilteredHandler(DDRPostedScoreFn, (ULONG)this, "DDRPostedScoreFn", DDREvent::ClassVersion(), ddr_id, DDREvent::DDR_POST_SCORE);
		dispatcher->RegisterFilteredHandler(DDRGameEndFn, (ULONG)this, "DDRGameEndFn", DDREvent::ClassVersion(), ddr_id, DDREvent::DDR_END_GAME);

		//	Register input handler
		EVENT_ID DDRInputEvent = dispatcher->GetEventId("Keyboard");
		dispatcher->RegisterHandler(DDRInputHandler, (ULONG)this, "DDRInputHandler", PackVersion(1, 0, 0, 0), DDRInputEvent);

		m_dispatcher = dispatcher;
	}

	return true;
}

bool DDR::RegisterEventHandlers(IDispatcher *dispatcher) {
	if (dispatcher == 0) {
		assert(false);
		return false;
	}

	// init static factory in this DLL
	IEvent::SetEncodingFactory(dispatcher->EncodingFactory());

	IGame *game = dispatcher->Game();
	if (game != 0 && game->EngineType() == eEngineType::Client) {
		// now register handlers
		EVENT_ID ddr_id = dispatcher->GetEventId("DDREvent");
		dispatcher->RegisterFilteredHandler(DDRGameStartFn, (ULONG)this, "DDRGameStartFn", DDREvent::ClassVersion(), ddr_id, DDREvent::DDR_START_GAME);
		dispatcher->RegisterFilteredHandler(DDRGameLevelDataFn, (ULONG)this, "DDRGameLevelDataFn", DDREvent::ClassVersion(), ddr_id, DDREvent::DDR_START_LEVEL_DATA);
		dispatcher->RegisterFilteredHandler(DDRPostedScoreFn, (ULONG)this, "DDRPostedScoreFn", DDREvent::ClassVersion(), ddr_id, DDREvent::DDR_POST_SCORE);
		dispatcher->RegisterFilteredHandler(DDRGameEndFn, (ULONG)this, "DDRGameEndFn", DDREvent::ClassVersion(), ddr_id, DDREvent::DDR_END_GAME);
		dispatcher->RegisterFilteredHandler(DDRPauseEvent, (ULONG)this, "DDRPauseEvent", DDREvent::ClassVersion(), ddr_id, DDREvent::DDR_CLI_PAUSED_RECEIVED);

		//	Register input handler
		EVENT_ID DDRInputEvent = dispatcher->GetEventId("Keyboard");
		dispatcher->RegisterHandler(DDRInputHandler, (ULONG)this, "DDRInputHandler", PackVersion(1, 0, 0, 0), DDRInputEvent);

		EVENT_ID DDRMenuEvent = dispatcher->GetEventId("DDRMenuEvent");
		dispatcher->RegisterHandler(DDRMenuHandler, (ULONG)this, "DDRMenuHandler", PackVersion(1, 0, 0, 0), DDRMenuEvent);

		m_dispatcher = dispatcher;
	}

	return true;
}

bool DDR::ReRegisterEventHandlers(IDispatcher * /*dispatcher*/, IEncodingFactory * /*factory*/) {
	bool ret = true;
	// do anything here you need to do if re-registering handlers
	return ret;
}

void DDR::StartLevelData(ULONG levelId) {
	if (g_ftt) {
		SetNewGame(true);
		g_ftt = false;
	} else {
		SetNewGame(false);
	}

	// Request meta information about the game from the server
	ULONG gameId = GetGameId();
	assert(gameId);
	DDREvent *ddre = new DDREvent();
	ddre->StartLevel(gameId, levelId, IsNewGame(), GetObjectId());
	ddre->AddTo(SERVER_NETID);
	m_dispatcher->QueueEvent(ddre);
}

void DDR::FreeAll() {
	if (m_dispatcher != NULL) {
		IGame *game = dynamic_cast<IGame *>(m_dispatcher->Game());
		if (game != 0 && game->EngineType() == eEngineType::Client) {
			m_dispatcher->RemoveHandler(DDRGameStartFn, (ULONG)this, "DDRGameStartFn", DDREvent::DDR_START_GAME);
			m_dispatcher->RemoveHandler(DDRGameLevelDataFn, (ULONG)this, "DDRGameLevelDataFn", DDREvent::DDR_START_LEVEL_DATA);
			m_dispatcher->RemoveHandler(DDRPostedScoreFn, (ULONG)this, "DDRPostedScoreFn", DDREvent::DDR_POST_SCORE);
			m_dispatcher->RemoveHandler(DDRGameEndFn, (ULONG)this, "DDRGameEndFn", DDREvent::DDR_END_GAME);
		}
	}
}

extern "C" BOOL APIENTRY DllMain(HANDLE hModule,
	DWORD ul_reason_for_call,
	LPVOID /*lpReserved*/
) {
	if (ul_reason_for_call == DLL_PROCESS_ATTACH) {
		StringResource = (HINSTANCE)hModule;
	}
	return TRUE;
}

extern "C" BLADE_EXPORT
	size_t
	GetNumInterfaces() {
	return 1;
}

extern "C" BLADE_EXPORT
	IClientBlade *
	Create(size_t idx) {
	if (0 == idx)
		return new DDR();
	else
		return 0;
}

bool DDR::RecordScore(ULONG gameId, ULONG levelId, ULONG numMilliSecondsExpired, bool levelComplete, bool gameOver, bool perfectBonus, bool perfectAchieved, bool useAlternate) {
	int instanceId = GetGameInstanceId();

	if ((instanceId == 0) && (levelId != 1)) {
		LOG4CPLUS_ERROR(m_logger, loadStrPrintf(IDS_INVALID_INSTANCE_AND_LEVEL_1, levelId).c_str());
		return false;
	}

	DDREvent *ddre = new DDREvent();
	ddre->RecordScore(gameId, GetObjectId(), levelId, instanceId, numMilliSecondsExpired, levelComplete, gameOver, perfectBonus, perfectAchieved, useAlternate);
	ddre->AddTo(SERVER_NETID);
	m_dispatcher->QueueEvent(ddre);

	return true;
}

void DDR::PauseGame(ULONG gameId, bool isPaused) {
	DDREvent *ddre = new DDREvent();
	ddre->PlayerPaused(gameId, GetObjectId(), isPaused);
	ddre->AddTo(SERVER_NETID);
	m_dispatcher->QueueEvent(ddre);
}

void DDR::PostedScore(ULONG /*gameId*/, ULONG /*levelId*/, int gameInstanceId, int /*playerId*/, ULONG score, ULONG /*highScore*/, ULONG famePointsEarned, int newFameLevel, int percentToNextLevel, string rewardsAtNextLevel, string gainMessage) {
	if (newFameLevel > 0) {
		mGainMessage = gainMessage;

		// set the success message to dds that corresponds to the new fame level message
		mWhichSuccessMessage = 5;
	}

	mScoreToRender = score;
	SetGameInstanceId(gameInstanceId);

	// current fame points awarded
	mCurrentFameEarned = famePointsEarned;

	mCurrentFameAwarded = mCurrentFameAwarded + famePointsEarned;

	if (percentToNextLevel != 0) {
		mPercentNextFameLevel = percentToNextLevel;
	}

	if (rewardsAtNextLevel != "") {
		mRewardsNextFameLevel = rewardsAtNextLevel;
	}

	SetWaitingOnScoreUpdate(false);
}

void DDR::GameOverDialog(ULONG gameId, int famePointsAwarded, int percentToNext, string rewardsAtNext, string gainMessage) {
	HandleGameExit();
	DDREvent *ddre = new DDREvent();
	ddre->GameOverDialog(gameId, famePointsAwarded, percentToNext, rewardsAtNext, gainMessage);
	m_dispatcher->QueueEvent(ddre);
}

void DDR::ReadyToPlay() {
	// Clear the Press TAB to play text in the gameplay control
	DDREvent *ddre = new DDREvent();
	ULONG gameId = GetGameId();
	ddre->ReadyToPlay(gameId);
	m_dispatcher->QueueEvent(ddre);
}

void DDR::CloseGameDialog() {
	DDREvent *ddr_exit = new DDREvent();
	ULONG gameId = GetGameId();
	ddr_exit->PlayerExit(gameId, GetObjectId(), IsLeavingFloor());
	ddr_exit->AddTo(SERVER_NETID);
	m_dispatcher->QueueEvent(ddr_exit);

	DDREvent *ddre = new DDREvent();
	ddre->CloseGameDialog(gameId);
	m_dispatcher->QueueEvent(ddre);
}

void DDR::StartLevel(DDREvent *e) {
	LOG4CPLUS_INFO(m_logger, loadStrPrintf(IDS_START_DDR, e->GetGameLevel()).c_str());

	// Play game
	StartGameState(e);
}
