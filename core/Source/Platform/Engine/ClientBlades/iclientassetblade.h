/******************************************************************************
 iclientassetblade.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "iclientblade.h"

namespace KEP {

class IAssetFactory;
class IAsset;

class IClientAssetBlade : public IClientBlade {
public:
	// creates an asset it knows how to create given a name (generally a filename)
	virtual IAsset* operator()(const std::string& name) = 0;

	virtual void RegisterAsset(IAssetFactory* factory) = 0;

	virtual size_t GetNumExtensions() = 0;
	virtual const char* GetExtension(size_t i) = 0;

	virtual const char* GetAssetType() = 0;
};

} // namespace KEP
