///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "imodel.h"

namespace KEP {

class CReferenceObjList;
class CWldObject;

class LegacyModelProxy : public IModel {
public:
	LegacyModelProxy(CWldObject* mesh, IDirect3DDevice9* d, CReferenceObjList* libraryReferenceDB);

	virtual void GetName(std::string& aName);
	virtual bool Intersect(Vector3f& from, Vector3f& to, BOOL testAlpha);
	virtual bool Intersect(Vector3f& from, Vector3f& to, Vector3f& normal);

	virtual Matrix44f GetTransform();

	virtual void Move(float x, float y, float z);
	virtual void Rotate(float sx, float sy, float sz);
	virtual void Scale(float sx, float sy, float sz);

	virtual void GetBoundingBox(Vector4f& min, Vector4f& max);

	CWldObject* GetLegacyWorldObject() {
		return m_world_obj;
	}

	virtual void Invalidate() {
		;
	}

protected:
	CWldObject* m_world_obj;

private:
	// LegacyModelProxy();	// NOT USED

	IDirect3DDevice9* m_device;
	CReferenceObjList* m_libraryReferenceDB;

	//	bool m_cached_box;					//12_27_06 CPITZER....persistent AABB support...moved to CWldObject
	//	wkg::Vector3 m_min, m_max;			//12_27_06 CPITZER....persistent AABB support...moved to CWldObject
};

} // namespace KEP
