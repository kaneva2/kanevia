///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <d3d9.h>
#include "cexmeshclass.h"
#include "creferencelibrary.h"
#include "CWldObject.h"

#include "compileopts.h"
#include "wkgtypes.h"

#include <vector>

#include "CMovementObj.h"
#include "playerModelProxy.h"
#include "ReSkinMesh.h"

#include "ReMeshCreate.h"
#include "CSkeletonObject.h"
#include "RuntimeSkeleton.h"

namespace KEP {

PlayerModelProxy::PlayerModelProxy(CMovementObj* obj,
	IDirect3DDevice9* d,
	CMovementObjList* libraryReferenceDB) :
		m_movementObj(obj),
		m_device(d),
		m_libraryReferenceDB(libraryReferenceDB),
		m_cached_box(false),
		m_min(0.0f, 0.0f, 0.0f),
		m_max(0.0f, 0.0f, 0.0f) {
	type = PLAYER_PROXY;

	if (m_movementObj) {
		m_movementObj->setModel((IModel*)this);
	}
}

PlayerModelProxy::~PlayerModelProxy() {
	if (m_movementObj)
		m_movementObj->setModel(NULL);
}

void PlayerModelProxy::Scale(float sx, float sy, float sz) {
	//NO-OP
}

void PlayerModelProxy::Move(float x, float y, float z) {
	//NO-OP
}

void PlayerModelProxy::Rotate(float rx, float ry, float rz) {
	//NO-OP
}

void PlayerModelProxy::GetName(std::string& aName) {
	if (m_movementObj)
		aName = m_movementObj->getName();
}

bool PlayerModelProxy::Intersect(Vector3f& from, Vector3f& to, BOOL testAlpha) {
	if (m_movementObj && m_movementObj->getSkeleton()) {
		Vector3f origin(from), direction((to - from).Normalize()), result;

		return m_movementObj->getSkeleton()->intersectRay(origin, direction, &to);
	}

	return false;
}

Matrix44f
PlayerModelProxy::GetTransform() {
	if (m_movementObj) {
		//return m_movementObj->getTransformMatrix();
	}

	return Matrix44f::GetIdentity();
}

void PlayerModelProxy::GetBoundingBox(Vector4f& min, Vector4f& max) {
}

} // namespace KEP