/******************************************************************************
 iasset.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <string>

namespace KEP {

class IAsset {
public:
	virtual void MakeMeVirtual() {}

protected:
};

} // namespace KEP
