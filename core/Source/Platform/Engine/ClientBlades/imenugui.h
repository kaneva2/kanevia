#pragma once

#include "gui_events.h"
#include "KEPMenu/WeakRef.h"
#include <string>
#include <vector>
#include <d3d9types.h>
#include <d3dx9tex.h>

#include "Core/Util/HighPrecisionTime.h"

namespace KEP {
class IMenuStatic;
class IMenuListBox;
class IMenuEditBox;
class IMenuComboBox;
class IMenuDialog;
class IMenuControl;
class IMenuFont;
struct IMenuBlendColor;
class IMenuScrollBar;
class CDXUTButton;
class CDXUTStatic;
class CDXUTImage;
class CDXUTCheckBox;
class CDXUTRadioButton;
class CDXUTComboBox;
class CDXUTSlider;
class CDXUTEditBox;
class CDXUTListBox;
class CDXUTFlashPlayer;
class CDXUTScrollBar;
class CDXUTElement;
struct DXUTElementHolder;
struct DXUTTextureNode;
class CDXUTFontNode;
}

class TiXmlNode;
class TiXmlDocument;

namespace KEP {

enum DXUT_CONTROL_STATE {
	DXUT_STATE_NORMAL,
	DXUT_STATE_DISABLED,
	DXUT_STATE_HIDDEN,
	DXUT_STATE_FOCUS,
	DXUT_STATE_MOUSEOVER,
	DXUT_STATE_PRESSED,

	DXUT_CONTROL_STATES_MAX
};

enum DXUT_CONTROL_STATE_FLAG {
	DXUT_STATE_FLAG_NONE = 0,
	DXUT_STATE_FLAG_NORMAL = (1 << DXUT_STATE_NORMAL),
	DXUT_STATE_FLAG_DISABLED = (1 << DXUT_STATE_DISABLED),
	DXUT_STATE_FLAG_HIDDEN = (1 << DXUT_STATE_HIDDEN),
	DXUT_STATE_FLAG_FOCUS = (1 << DXUT_STATE_FOCUS),
	DXUT_STATE_FLAG_MOUSEOVER = (1 << DXUT_STATE_MOUSEOVER),
	DXUT_STATE_FLAG_PRESSED = (1 << DXUT_STATE_PRESSED),
	DXUT_STATE_FLAG_ALL = 0xFF,

	DXUT_STATE_FLAG_NOT_HIDDEN = (DXUT_STATE_FLAG_ALL & ~DXUT_STATE_FLAG_HIDDEN)
};

// IMenuDialog Location Format Flags
#define IMDL_NONE 0x00000000
#define IMDL_TOP 0x00000001
#define IMDL_LEFT 0x00000002
#define IMDL_CENTER 0x00000004
#define IMDL_RIGHT 0x00000008
#define IMDL_VCENTER 0x00000010
#define IMDL_BOTTOM 0x00000020

class IMenuElement : virtual public WeakRefObject {
public:
	virtual void SetTexture(UINT iTexture, RECT* prcTexture, D3DCOLOR defaultTextureColor = D3DCOLOR_ARGB(255, 255, 255, 255)) = 0;
	virtual void SetFont(UINT iFont, D3DCOLOR defaultFontColor = D3DCOLOR_ARGB(255, 255, 255, 255), DWORD dwTextFormat = DT_CENTER | DT_VCENTER) = 0;
	virtual void SetFontIndex(UINT iFont) = 0;
	virtual void SetTextFormat(UINT textFormat) = 0;

	virtual void SetTextureCoords(RECT coords) = 0;
	virtual void SetTextureSlices(RECT coords) = 0;

	virtual bool RetryFailedTexture(IMenuDialog* pDialog) = 0;

	virtual bool AddTexture(IMenuDialog* pDialog, const std::string& sTextureName, RECT coords, RECT slices) = 0;
	virtual bool AddExternalTexture(IMenuDialog* pDialog, const std::string& sTextureName) = 0;
	virtual bool AddProgressTexture(IMenuDialog* pDialog, const std::string& sTextureName) = 0;
	virtual bool AddDynamicTexture(IMenuDialog* pDialog, unsigned dynTextureId) = 0;

	virtual bool AddFont(IMenuDialog* pDialog, const std::string& sFaceName, long height, long weight, bool italic, bool underline) = 0;

	virtual IMenuFont* GetFont(IMenuDialog* pDialog) = 0;
	virtual UINT GetFontIndex() = 0;
	virtual UINT GetTextFormat() = 0;
	virtual IMenuBlendColor* GetTextureColor() = 0;
	virtual IMenuBlendColor* GetFontColor() = 0;

	virtual RECT GetTextureCoords() const = 0;
	virtual RECT GetTextureSlices() const = 0;

	virtual const char* GetTextureNamePtr(IMenuDialog* pDialog) = 0;
	virtual std::string GetTextureName(IMenuDialog* pDialog) = 0;
	virtual UINT GetTextureWidth(IMenuDialog* pDialog) = 0;
	virtual UINT GetTextureHeight(IMenuDialog* pDialog) = 0;
	virtual UINT GetProgressTextureWidth(IMenuDialog* pDialog) = 0;
	virtual UINT GetProgressTextureHeight(IMenuDialog* pDialog) = 0;
};

class IMenuFont {
public:
	virtual const char* GetFacePtr() const = 0;
	virtual std::string GetFace() const = 0;
	virtual LONG GetHeight() const = 0;
	virtual LONG GetWeight() const = 0;
	virtual bool GetItalic() const = 0;
	virtual bool GetUnderline() const = 0;
};

struct IMenuBlendColor : virtual public WeakRefObject {
	virtual void Init(D3DCOLOR defaultColor, D3DCOLOR disabledColor = D3DCOLOR_ARGB(0, 128, 128, 128), D3DCOLOR hiddenColor = 0) = 0;
	virtual void Blend(DXUT_CONTROL_STATE iState, float fElapsedTime, float fRate = 0.7f) = 0;

	virtual void SetColor(DXUT_CONTROL_STATE state, D3DCOLOR color) = 0;
	virtual D3DCOLOR GetColor(DXUT_CONTROL_STATE state) = 0;

	virtual void SetCurrentColor(D3DCOLOR color) = 0;
	virtual D3DXCOLOR GetCurrentColor() const = 0;

	virtual bool Load(TiXmlNode* root, unsigned int iStatesToLoad = DXUT_STATE_FLAG_ALL) = 0;
	virtual bool Save(TiXmlNode* parent, unsigned int iStatesToSave = DXUT_STATE_FLAG_ALL) = 0;
};

typedef bool(CALLBACK* PCALLBACKDXUTDIALOGGUIEVENT)(GUI_EVENT nEvent, IMenuDialog* pDialog, POINT pt);
typedef bool(CALLBACK* PCALLBACKDXUTCONTROLGUIEVENT)(GUI_EVENT nEvent, int nControlID, IMenuControl* pControl, POINT pt);

//-----------------------------------------------------------------------------
// All controls must be assigned to a dialog, which handles
// input and rendering for the controls.
//-----------------------------------------------------------------------------
class IMenuDialog : virtual public WeakRefObject {
public:
	static std::string s_sDefaultMenuDir;
	static void SetDefaultMenuDir(std::string sCurDir) {
		s_sDefaultMenuDir = sCurDir;
	}
	static std::string GetDefaultMenuDir() {
		return s_sDefaultMenuDir;
	}

	virtual void SetMenuId(const std::string& menuId) = 0;
	virtual const char* GetMenuIdPtr() = 0;
	virtual std::string GetMenuId() const = 0;

	virtual const char* GetMenuFilePathPtr() = 0;
	virtual std::string GetMenuFilePath() const = 0;

	virtual const char* GetMenuFileNamePtr() = 0;
	virtual std::string GetMenuFileName() const = 0;

	virtual const char* GetScriptFileNamePtr() = 0;
	virtual std::string GetScriptFileName() const = 0;

	virtual std::string ToStr() const = 0;

	virtual void Delete() = 0;

	virtual bool LoadMenuXmlResources(TiXmlNode* root) = 0;

	virtual bool LoadElements(TiXmlNode* elements_root) = 0;

	virtual IMenuControl* LoadControl(TiXmlNode* controlXml) = 0;
	virtual IMenuControl* LoadControlByXml(const std::string& xml) = 0;

	virtual bool Save(TiXmlDocument* xml_doc) = 0;

	virtual bool SetCallback(PCALLBACKDXUTDIALOGGUIEVENT pCallback) = 0;

	virtual bool SetCallback(PCALLBACKDXUTCONTROLGUIEVENT pCallback) = 0;

	// Control creation
	virtual HRESULT AddStatic(int ID, const char* strText, int x, int y, int width, int height, bool bShadow = false, bool bIsDefault = false, CDXUTStatic** ppCreated = NULL) = 0;
	virtual HRESULT AddImage(int ID, int x, int y, int width, int height, UINT iTexture, RECT* prcTexture, D3DCOLOR defaultTextureColor = D3DCOLOR_ARGB(255, 255, 255, 255), CDXUTImage** ppCreated = NULL) = 0;
	virtual HRESULT AddButton(int ID, const char* strText, int x, int y, int width, int height, UINT nHotkey = 0, bool bIsDefault = false, CDXUTButton** ppCreated = NULL) = 0;
	virtual HRESULT AddCheckBox(int ID, const char* strText, int x, int y, int width, int height, bool bChecked = false, UINT nHotkey = 0, bool bIsDefault = false, CDXUTCheckBox** ppCreated = NULL) = 0;
	virtual HRESULT AddRadioButton(int ID, UINT nButtonGroup, const char* strText, int x, int y, int width, int height, bool bChecked = false, UINT nHotkey = 0, bool bIsDefault = false, CDXUTRadioButton** ppCreated = NULL) = 0;
	virtual HRESULT AddComboBox(int ID, int x, int y, int width, int height, UINT nHotKey = 0, bool bIsDefault = false, CDXUTComboBox** ppCreated = NULL) = 0;
	virtual HRESULT AddSlider(int ID, int x, int y, int width, int height, int min = 0, int max = 100, int value = 50, bool bIsDefault = false, CDXUTSlider** ppCreated = NULL) = 0;
	virtual HRESULT AddEditBox(int ID, const char* strText, int x, int y, int width, int height, bool typeSimple = false, bool bIsDefault = false, CDXUTEditBox** ppCreated = NULL) = 0;
	virtual HRESULT AddListBox(int ID, int x, int y, int width, int height, DWORD dwStyle = 0, CDXUTListBox** ppCreated = NULL) = 0;
	virtual HRESULT AddFlash(int ID, int x, int y, int width, int height, bool wantInput, bool sizeToFit, bool loop, const char* params, CDXUTFlashPlayer** ppCreated = NULL) = 0;

	// Control creation by name
	virtual HRESULT AddStatic(const char* strName, const char* strText, int x, int y, int width, int height, bool bShadow = false, bool bIsDefault = false, IMenuControl** ppCreated = NULL) = 0;
	virtual HRESULT AddImage(const char* strName, int x, int y, int width, int height, const std::string& textureName, RECT coords, D3DCOLOR defaultTextureColor = D3DCOLOR_ARGB(255, 255, 255, 255), IMenuControl** ppCreated = NULL) = 0;
	virtual HRESULT AddImageToBack(const char* strName, int x, int y, int width, int height, const std::string& textureName, RECT coords, D3DCOLOR defaultTextureColor = D3DCOLOR_ARGB(255, 255, 255, 255), IMenuControl** ppCreated = NULL) = 0;
	virtual HRESULT AddButton(const char* strName, const char* strText, int x, int y, int width, int height, UINT nHotkey = 0, bool bIsDefault = false, IMenuControl** ppCreated = NULL) = 0;
	virtual HRESULT AddCheckBox(const char* strName, const char* strText, int x, int y, int width, int height, bool bChecked = false, UINT nHotkey = 0, bool bIsDefault = false, IMenuControl** ppCreated = NULL) = 0;
	virtual HRESULT AddRadioButton(const char* strName, UINT nButtonGroup, const char* strText, int x, int y, int width, int height, bool bChecked = false, UINT nHotkey = 0, bool bIsDefault = false, IMenuControl** ppCreated = NULL) = 0;
	virtual HRESULT AddComboBox(const char* strName, int x, int y, int width, int height, UINT nHotKey = 0, bool bIsDefault = false, IMenuControl** ppCreated = NULL) = 0;
	virtual HRESULT AddSlider(const char* strName, int x, int y, int width, int height, int min = 0, int max = 100, int value = 50, bool bIsDefault = false, IMenuControl** ppCreated = NULL) = 0;
	virtual HRESULT AddEditBox(const char* strName, const char* strText, int x, int y, int width, int height, bool typeSimple = false, bool bIsDefault = false, IMenuControl** ppCreated = NULL) = 0;
	virtual HRESULT AddListBox(const char* strName, int x, int y, int width, int height, DWORD dwStyle = 0, IMenuControl** ppCreated = NULL) = 0;
	virtual HRESULT AddFlash(const char* strName, int x, int y, int width, int height, bool wantInput, bool sizeToFit, bool loop, const char* params, IMenuControl** ppCreated = NULL) = 0;

	// Control retrieval
	virtual CDXUTStatic* GetStatic(int ID) = 0;
	virtual CDXUTImage* GetImage(int ID) = 0;
	virtual CDXUTButton* GetButton(int ID) = 0;
	virtual CDXUTCheckBox* GetCheckBox(int ID) = 0;
	virtual CDXUTRadioButton* GetRadioButton(int ID) = 0;
	virtual CDXUTComboBox* GetComboBox(int ID) = 0;
	virtual CDXUTSlider* GetSlider(int ID) = 0;
	virtual CDXUTEditBox* GetEditBox(int ID) = 0;
	virtual CDXUTListBox* GetListBox(int ID) = 0;

	// Control retrieval by name
	virtual CDXUTStatic* GetStatic(const char* strName) = 0;
	virtual CDXUTImage* GetImage(const char* strName) = 0;
	virtual CDXUTButton* GetButton(const char* strName) = 0;
	virtual CDXUTCheckBox* GetCheckBox(const char* strName) = 0;
	virtual CDXUTRadioButton* GetRadioButton(const char* strName) = 0;
	virtual CDXUTComboBox* GetComboBox(const char* strName) = 0;
	virtual CDXUTSlider* GetSlider(const char* strName) = 0;
	virtual CDXUTEditBox* GetEditBox(const char* strName) = 0;
	virtual CDXUTListBox* GetListBox(const char* strName) = 0;
	virtual CDXUTFlashPlayer* GetFlash(const char* strName) = 0;

	//Ankit ----- return IMenu equivalent interface objects to everything outside KepMenu
	virtual IMenuListBox* GetIMenuListBox(const char* strName) = 0;
	virtual IMenuEditBox* GetIMenuEditBox(const char* strName) = 0;
	virtual IMenuComboBox* GetIMenuComboBox(const char* strName) = 0;
	virtual IMenuStatic* GetIMenuStatic(const char* strName) = 0;

	virtual IMenuControl* GetControl(const char* strName) = 0;
	virtual IMenuControl* GetControl(int ID) = 0;
	virtual IMenuControl* GetControl(int ID, UINT nControlType) = 0;
	virtual IMenuControl* GetControlByIndex(int index) = 0;
	virtual IMenuControl* GetControlAtPoint(POINT pt) = 0;
	virtual IMenuControl* GetNextControl(IMenuControl* pControl) = 0;
	virtual IMenuControl* GetPrevControl(IMenuControl* pControl) = 0;

	virtual const char* GetControlXml(IMenuControl* pControl) = 0;

	virtual HRESULT AddControl(IMenuControl* pControl) = 0;
	virtual HRESULT InitControl(IMenuControl* pControl) = 0;

	virtual bool RemoveControl(int ID) = 0;
	virtual bool RemoveControl(const char* strName) = 0;
	virtual bool RemoveAllControls() = 0;

	virtual bool GetControlEnabled(int ID) = 0;
	virtual bool SetControlEnabled(int ID, bool bEnabled) = 0;

	virtual bool ClearRadioButtonGroup(UINT nGroup) = 0;
	virtual bool ClearComboBox(int ID) = 0;

	virtual bool ClearFocus() = 0;
	virtual bool FocusDefaultControl() = 0;

	virtual bool ClearSelection() = 0;

	virtual bool MoveControlForward(const std::string& ctlName) = 0;
	virtual bool MoveControlBackward(const std::string& ctlName) = 0;
	virtual bool MoveControlToFront(const std::string& ctlName) = 0;
	virtual bool MoveControlToBack(const std::string& ctlName) = 0;
	virtual bool MoveControlToIndex(const std::string& ctlName, unsigned int index) = 0;

	// Access the default display Elements used when adding new controls
	virtual HRESULT SetDefaultElement(UINT nControlType, UINT iElement, CDXUTElement* pElement) = 0;
	virtual CDXUTElement* GetDefaultElement(UINT nControlType, UINT iElement) = 0;

	// Methods called by controls
	virtual bool SendEvent(GUI_EVENT nEvent, bool bTriggeredByUser, IMenuControl* pControl, POINT pt) = 0;
	virtual bool SendEvent(GUI_EVENT nEvent, bool bTriggeredByUser, IMenuControl* pControl) = 0;
	virtual bool RequestFocus(IMenuControl* pControl) = 0;
	virtual bool RequestSelection(IMenuControl* pControl, bool bEnable) = 0;

	// Render helpers
	virtual HRESULT DrawRect(RECT* pRect, D3DCOLOR color) = 0;
	virtual HRESULT DrawPolyLine(POINT* apPoints, UINT nNumPoints, D3DCOLOR color) = 0;
	virtual HRESULT DrawSprite(CDXUTElement* pElement, RECT* prcDest) = 0;
	virtual int DrawText(const wchar_t* strText, CDXUTElement* pElement, RECT* prcDest, bool bShadow = false, int nCount = -1) = 0;

	virtual bool MsgProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) = 0;

	virtual void OnBeginFrame() = 0;
	virtual void OnEndFrame() = 0;

	virtual HRESULT OnRender(float fElapsedTime) = 0;

	Timer m_timerScriptOnRender;
	TimeMs m_timeScriptOnRender = (TimeMs)15.0; // ~60hz

	virtual bool IsFullyConstructed() const = 0;

	virtual void SetPendingDeletion(bool pendingDelete) = 0;
	virtual bool IsPendingDeletion() const = 0;

	virtual bool IsTrustedMenu() const = 0;

	virtual bool HitTest(int x, int y) const = 0;

	virtual bool ClearStaticControlsForThisDialog() = 0;

	virtual int GetID() const = 0;
	virtual bool GetMinimized() const = 0;
	virtual bool GetCaptionEnabled() const = 0;
	virtual const char* GetCaptionText() = 0;
	virtual int GetCaptionHeight() const = 0;
	virtual bool GetCaptionShadow() const = 0;

	virtual bool GetCursorLocation(POINT& pt) const = 0;
	virtual int GetCursorLocationX() const = 0;
	virtual int GetCursorLocationY() const = 0;

	virtual bool GetLocation(POINT& pt, bool useOffset = true) const = 0;
	virtual int GetLocationX(bool useOffset = true) const = 0;
	virtual int GetLocationY(bool useOffset = true) const = 0;

	virtual DWORD GetLocationFormat() const = 0;

	virtual int GetWidth() const = 0;
	virtual int GetHeight() const = 0;

	virtual bool GetSnapToEdges() const = 0;

	virtual bool GetIsSelected() const = 0;

	virtual bool GetModal() const = 0;
	virtual bool IsActuallyModal() const = 0;

	virtual bool GetMovable() const = 0;
	virtual bool GetEnableMouseInput() const = 0;
	virtual bool GetEnableKeyboardInput() const = 0;

	virtual bool BringToFrontEnable(bool enable) = 0;
	virtual bool BringToFrontEnabled() const = 0;
	virtual bool GetBringToFront() const = 0;
	virtual bool IsTopmost() const = 0;

	virtual IMenuElement* GetElement(std::string elementTag) = 0;
	virtual IMenuElement* GetCaptionElement() = 0;
	virtual IMenuElement* GetBodyElement() = 0;

	virtual unsigned int GetElementTagUsesFont(std::string elementTag) = 0;
	virtual unsigned int GetElementTagUsesTexture(std::string elementTag) = 0;

	virtual int GetNumControls() const = 0;
	virtual int GetControlIndex(IMenuControl* pControl) = 0;
	virtual IMenuDialog* GetNextDialog() = 0;
	virtual IMenuDialog* GetPrevDialog() = 0;
	virtual D3DCOLOR GetColorTopLeft() const = 0;
	virtual D3DCOLOR GetColorTopRight() const = 0;
	virtual D3DCOLOR GetColorBottomLeft() const = 0;
	virtual D3DCOLOR GetColorBottomRight() const = 0;

	virtual bool GetEdit() const = 0;
	virtual bool SetEdit(bool bEnable) = 0;

	virtual bool SetAllControls(bool bMinimized, bool bEnable) = 0;

	virtual bool SetID(int ID) = 0;
	virtual bool SetMinimized(bool bMinimized) = 0;
	virtual bool SetBackgroundColors(D3DCOLOR colorAllCorners) = 0;
	virtual bool SetBackgroundColors(D3DCOLOR colorTopLeft, D3DCOLOR colorTopRight, D3DCOLOR colorBottomLeft, D3DCOLOR colorBottomRight) = 0;
	virtual bool EnableCaption(bool bEnable) = 0;
	virtual bool SetCaptionHeight(int nHeight) = 0;
	virtual bool SetCaptionText(const char* pwszText) = 0;
	virtual bool SetCaptionShadow(const bool bShadow) = 0;

	virtual bool SetLocation(int x, int y) = 0;
	virtual bool SetLocationX(int x) = 0;
	virtual bool SetLocationY(int y) = 0;

	virtual bool SetHeight(int height) = 0;
	virtual bool SetWidth(int width) = 0;
	virtual bool SetSize(int width, int height) = 0;

	virtual bool SetLocationInScreen(int x, int y) = 0;
	virtual bool SetLocationFormat(DWORD dwFormat) = 0;
	virtual bool SetSnapToEdges(bool bSnap) = 0;

	virtual bool SetCaptureKeys(bool capture) = 0;

	virtual bool GetPassthrough() const = 0;
	virtual bool SetPassthrough(bool passthrough) = 0;

	virtual bool SetIsSelected(bool bIsSelected) = 0;
	virtual bool SetModal(bool bModal) = 0;

	virtual bool SetBringToFront(bool bFront) = 0;

	virtual bool SetNextDialog(IMenuDialog* pNextDialog) = 0;
	virtual bool SetPrevDialog(IMenuDialog* pPrevDialog) = 0;
	virtual bool SetRefreshTime(float fTime) = 0;

	virtual bool SetMenuFilePath(const std::string& filePath) = 0;
	virtual bool SetScriptFileName(const std::string& scriptFileName) = 0;

	virtual bool SetColorTopLeft(D3DCOLOR color) = 0;
	virtual bool SetColorTopRight(D3DCOLOR color) = 0;
	virtual bool SetColorBottomLeft(D3DCOLOR color) = 0;
	virtual bool SetColorBottomRight(D3DCOLOR color) = 0;
	virtual bool EnableNonUserEvents(bool bEnable) = 0;

	virtual bool EnableKeyboardInput(bool bEnable) = 0;
	virtual bool EnableMouseInput(bool bEnable) = 0;

	virtual bool SetCloseOnESC(bool bEnable) = 0;
	virtual bool GetCloseOnESC() = 0;

	virtual bool SetResizable(bool enable) = 0;
	virtual bool SetMovable(bool enable) = 0;

	virtual bool SetEnabled(bool bEnable) = 0;

	virtual bool ContainsPoint(POINT pt) = 0;
	virtual bool ApplyLocationFormatting() = 0;
	virtual int GetScreenWidth() = 0;
	virtual int GetScreenHeight() = 0;
	virtual bool IsLMouseDown() = 0;
	virtual bool IsRMouseDown() = 0;

#if DEPRECATED
	// included dialogs
	typedef std::vector<std::string> IncludedDialogsVector;
	typedef IncludedDialogsVector::iterator IncludedDialogsIter;

	virtual bool AddIncludedDialog(const char* filename) = 0;
	virtual bool RemoveIncludedDialog(const char* filename) = 0;
	virtual bool GetIncludedDialogs(IncludedDialogsVector& dialogs) = 0;
#endif

	virtual bool CheckForDuplicateControlName(const char* strName, bool bShowMsg) = 0;
	virtual bool CheckForDuplicateControlNames(bool bShowMsg) = 0;

	// Shared resource access. Indexed fonts and textures are shared among all the controls.

	virtual int SetFont(size_t index, const std::string& strFaceName, LONG height, LONG weight, bool italic, bool underline) = 0;

	virtual CDXUTFontNode* GetFont(size_t index) = 0;

	virtual int AddFont(const std::string& strFaceName, LONG height, LONG weight, bool italic, bool underline) = 0;

	virtual int GetResourceManagerFontIndex(size_t index) = 0;

	virtual int SetTexture(size_t index, const std::string& strFilename, D3DXIMAGE_INFO* textInfoToBeReturned = nullptr) = 0;
	virtual DXUTTextureNode* GetTexture(size_t index) = 0;
	virtual int AddTexture(const std::string& strFilename, D3DXIMAGE_INFO* textInfoToBeReturned = nullptr) = 0;
	virtual int AddExternalTexture(const std::string& strFilename) = 0;
	virtual int AddDynamicTexture(unsigned dynTextureId) = 0;
};

class IMenuControl : virtual public WeakRefObject {
public:
	IMenuControl() :
			m_bPassthrough(false) {}

	virtual bool Load(TiXmlNode* root) = 0;
	virtual bool LoadElements(TiXmlNode* elements_root) = 0;
	virtual bool Save(TiXmlNode* parent) = 0;
	virtual bool SaveElements(TiXmlNode* parent) = 0;

	virtual HRESULT OnInit() = 0;
	virtual void Refresh() = 0;
	virtual void Render(float fElapsedTime) = 0;

	// Windows message handler
	virtual bool MsgProc(UINT uMsg, WPARAM wParam, LPARAM lParam) = 0;

	virtual bool HandleKeyboard(UINT uMsg, WPARAM wParam, LPARAM lParam) = 0;
	virtual bool HandleMouse(UINT uMsg, POINT pt, WPARAM wParam, LPARAM lParam) = 0;

	virtual bool CanHaveFocus() = 0;
	virtual void OnFocusIn() = 0;
	virtual void OnFocusOut() = 0;
	virtual void OnSelectionOn() = 0;
	virtual void OnSelectionOff() = 0;
	virtual void OnMouseEnter(POINT pt) = 0;
	virtual void OnMouseLeave() = 0;
	virtual void OnHotkey() = 0;
	virtual void OnMoved() = 0;
	virtual void OnResized() = 0;

	virtual BOOL ContainsPoint(POINT pt) = 0;

	bool m_bPassthrough;
	bool GetPassthrough() const {
		return m_bPassthrough;
	}
	bool SetPassthrough(bool passthrough) {
		m_bPassthrough = passthrough;
		return true;
	}

	virtual IMenuDialog* GetDialog() = 0;
	virtual bool GetEnabled() = 0;
	virtual bool GetVisible() = 0;
	virtual bool GetMouseOver() = 0;
	virtual bool GetIsInFocus() = 0;
	virtual UINT GetType() const = 0;
	virtual std::string GetTag() const = 0;
	virtual int GetID() const = 0;
	virtual const char* GetName() = 0;
	virtual bool GetLocation(POINT& Pt, bool useOffset = true) const = 0;
	virtual int GetWidth() const = 0;
	virtual int GetHeight() const = 0;
	virtual UINT GetHotkey() const = 0;
	virtual void* GetUserData() const = 0;
	virtual IMenuElement* GetElement(UINT iElement) = 0;
	virtual IMenuElement* GetElement(std::string elementTag) = 0;
	virtual bool GetIsDefault() const = 0;
	virtual int GetNumElementTags() const = 0;
	virtual const char* GetElementTag(int index) = 0;
	virtual const char* GetComment() = 0;
	virtual const char* GetToolTip() = 0;
	virtual const char* GetXMLCSS() = 0;
	virtual int GetGroupId() const = 0;
	virtual const char* GetLinkClicked() const = 0;
	virtual unsigned int GetElementTagUsesFont(std::string elementTag) = 0;
	virtual unsigned int GetElementTagUsesTexture(std::string elementTag) = 0;
	virtual RECT GetClipRect() const = 0;
	virtual bool IsClipped() const = 0;
	virtual bool IsCulled() const = 0;

	virtual bool GetMovable() const = 0;
	virtual bool GetResizable() const = 0;

	virtual bool SetEnabled(bool bEnabled) = 0;
	virtual bool SetVisible(bool bVisible) = 0;
	virtual bool SetID(int ID) = 0;
	virtual bool SetName(const char* strName) = 0;

	virtual bool SetLocation(int x, int y) = 0;
	virtual bool SetLocationX(int x) = 0;
	virtual bool SetLocationY(int y) = 0;

	virtual bool SetSize(int width, int height) = 0;
	virtual bool SetHotkey(UINT nHotkey) = 0;
	virtual bool SetUserData(void* pUserData) = 0;
	virtual bool SetTextColor(D3DCOLOR Color) = 0;
	virtual HRESULT SetElement(UINT iElement, CDXUTElement* pElement) = 0;
	virtual bool SetIsDefault(bool bIsDefault) = 0;
	virtual bool SetComment(const char* strText) = 0;
	virtual bool SetToolTip(const char* strText) = 0;
	virtual bool SetXMLCSS(const char* strText) = 0;
	virtual bool SetClipRect(int left, int top, int right, int bottom) = 0;

	virtual bool SetMovable(bool enable) = 0;
	virtual bool SetResizable(bool enable) = 0;

	virtual bool SetGroupId(int id) = 0;

	virtual bool SetXml(const char* xml) = 0;
	virtual const char* GetXml() = 0;
};

class IMenuStatic : virtual public WeakRefObject {
public:
	virtual const char* GetText() = 0;
	virtual bool GetShadow() const = 0;
	virtual bool GetUseHTML() const = 0;

	virtual HRESULT SetText(const char* strText) = 0;
	virtual HRESULT SetShadow(bool bShadow) = 0;
	virtual HRESULT SetUseHTML(bool bUseHTML) = 0;
};

class IMenuImage : virtual public WeakRefObject {
public:
	virtual HRESULT SetColor(D3DCOLOR color) = 0;
};

class IMenuButton : virtual public WeakRefObject {
public:
	virtual void SetSimpleMode(bool bSimpleMode) = 0;
	virtual bool GetSimpleMode() const = 0;
};

class IMenuCheckBox : virtual public WeakRefObject {
public:
	virtual bool GetChecked() const = 0;
	virtual void SetChecked(bool bChecked) = 0;

	virtual LONG GetIconWidth() const = 0;
	virtual void SetIconWidth(LONG iIconWidth) = 0;
};

class IMenuRadioButton : virtual public WeakRefObject {
public:
	virtual UINT GetButtonGroup() = 0;
	virtual void SetButtonGroup(UINT nButtonGroup) = 0;
};

class IMenuEditBox : virtual public WeakRefObject {
public:
	virtual IMenuScrollBar* GetScrollBar() = 0;
	virtual int GetScrollBarWidth() const = 0;

	virtual bool IsMultiline() const = 0;

	virtual size_t GetTextLength() const = 0;

	virtual bool SetMaxCharCount(size_t) = 0;

	virtual bool ClearText() = 0;

	virtual const char* GetTextOrig() = 0;
	virtual const char* GetText() = 0;
	virtual bool SetText(const char* szText, bool bSelected = false) = 0;

	virtual D3DCOLOR GetTextColor() const = 0;
	virtual bool SetTextColor(D3DCOLOR Color) = 0;

	// DRF - Sets edit box selection from start to end character indexes. (default selects all)
	virtual bool SetSelected(int startIndex = 0, int endIndex = -1) = 0;

	virtual D3DCOLOR GetSelectedTextColor() const = 0;
	virtual bool SetSelectedTextColor(D3DCOLOR Color) = 0;

	virtual D3DCOLOR GetSelectedBackColor() const = 0;
	virtual bool SetSelectedBackColor(D3DCOLOR Color) = 0;

	virtual D3DCOLOR GetCaretColor() const = 0;
	virtual bool SetCaretColor(D3DCOLOR Color) = 0;

	virtual int GetBorderWidth() const = 0;
	virtual bool SetBorderWidth(int nBorder) = 0;

	virtual int GetSpacing() const = 0;
	virtual bool SetSpacing(int nSpacing) = 0;

	virtual bool GetEditable() const = 0;
	virtual bool SetEditable(bool editable) = 0;

	virtual bool GetPassword() const = 0;
	virtual bool SetPassword(bool bPassword) = 0;
};

class IMenuComboBox : virtual public WeakRefObject {
public:
	virtual IMenuScrollBar* GetScrollBar() = 0;
	virtual int GetScrollBarWidth() const = 0;

	virtual int GetDropHeight() const = 0;
	virtual bool SetTextColor(D3DCOLOR color) = 0;

	virtual void SetDropHeight(UINT nHeight) = 0;
	virtual void SetScrollBarWidth(int nWidth) = 0;

	virtual HRESULT AddItem(const char* strText, void* pData) = 0;
	virtual void RemoveAllItems() = 0;
	virtual void RemoveItem(UINT index) = 0;
	virtual bool ContainsItem(const char* strText, UINT iStart = 0) = 0;
	virtual int FindItem(const char* strText, UINT iStart = 0) = 0;
	virtual const char* GetItemText(UINT nIndex) = 0;
	virtual void* GetItemData(const char* strText) = 0;
	virtual void* GetItemData(UINT index) = 0;
	virtual void* GetSelectedData() = 0;
	virtual const char* GetSelectedText() = 0;

	virtual int GetSelectedIndex() = 0;
	virtual UINT GetNumItems() = 0;

	virtual HRESULT SetSelectedByIndex(UINT index) = 0;
	virtual HRESULT SetSelectedByText(const char* strText) = 0;
	virtual HRESULT SetSelectedByData(void* pData) = 0;
};

class IMenuScrollBar : virtual public WeakRefObject {
public:
	virtual int GetTrackStart() const = 0;
	virtual int GetTrackEnd() const = 0;
	virtual int GetTrackPos() const = 0;
	virtual int GetPageSize() const = 0;
	virtual bool GetShowThumb() const = 0;
	virtual IMenuControl* GetParent() = 0;

	virtual void SetTrackRange(int nStart, int nEnd) = 0;
	virtual void SetTrackPos(int nPosition) = 0;
	virtual void SetPageSize(int nPageSize) = 0;
	virtual void SetParent(IMenuControl* pParent) = 0;
};

class IMenuListBox : virtual public WeakRefObject {
public:
	virtual IMenuScrollBar* GetScrollBar() = 0;
	virtual int GetScrollBarWidth() const = 0;

	virtual bool GetMultiSelection() const = 0;
	virtual bool GetDragSelection() const = 0;
	virtual int GetBorder() const = 0;
	virtual int GetLeftMargin() const = 0;
	virtual int GetRightMargin() const = 0;
	virtual int GetRowHeight() const = 0;
	virtual DWORD GetStyle() const = 0;
	virtual const char* GetItemText(int nIndex) = 0;
	virtual void* GetItemData(int nIndex) = 0;
	virtual const char* GetSelectedItemText() = 0;
	virtual void* GetSelectedItemData() = 0;
	virtual int GetSelectedIndex(int = -1) = 0;
	virtual int GetSize() const = 0;

	virtual void SetMultiSelection(bool bMultiSelection) = 0;
	virtual void SetDragSelection(bool bDragSelection) = 0;
	virtual void SetScrollBarWidth(int nWidth) = 0;
	virtual void SetBorder(int nBorder) = 0;
	virtual void SetMargins(int nMargin) = 0;
	virtual void SetLeftMargin(int nMargin) = 0;
	virtual void SetRightMargin(int nMargin) = 0;
	virtual void SetRowHeight(int nHeight) = 0;
	virtual void SetStyle(DWORD dwStyle) = 0;
	virtual void SetHTMLEnabled(bool) = 0;
	virtual void SetItemText(int, const char*) = 0;

	virtual HRESULT AddItem(const char* wszText, void* pData) = 0;
	virtual HRESULT InsertItem(int nIndex, const char* wszText, void* pData) = 0;

	virtual void RemoveItem(int nIndex) = 0;
	virtual void RemoveAllItems() = 0;
	virtual void SelectItem(int) = 0;
	virtual int FindItem(void*) = 0;
	virtual void ClearSelection() = 0;
};

class IMenuSlider : virtual public WeakRefObject {
public:
	virtual int GetRangeMin() const = 0;
	virtual int GetRangeMax() const = 0;
	virtual int GetValue() = 0;
	virtual void SetValue(int nValue) = 0;
	virtual void SetRange(int nMin, int nMax) = 0;
};

class IFlashPlayerControl : virtual public WeakRefObject {
public:
	virtual const char* GetSwfName() const = 0;
	virtual const char* GetSwfParameters() const = 0;
	virtual bool GetSizeToFit() const = 0;
	virtual bool GetLoopSwf() const = 0;
	virtual bool GetWantInput() const = 0;

	virtual void SetSwfName(const char* name) = 0;
	virtual void SetSwfParameters(const char* params) = 0;
	virtual void SetSizeToFit(bool fit) = 0;
	virtual void SetLoopSwf(bool loop) = 0;
	virtual void SetWantInput(bool wantIt) = 0;
};

} // namespace KEP