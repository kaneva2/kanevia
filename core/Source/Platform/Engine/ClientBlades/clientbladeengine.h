///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// for our DLL Export defines
#include "bladeexports.h"
#include "IClientEngine.h"

namespace KEP {

class IAssetFactory;
class ClientBladeFactory;

class ClientBladeEngine : public IClientEngine {
public:
	virtual void GetViewMatrix(Matrix44f* out) const override;
	virtual void GetProjectionMatrix(Matrix44f* out) const override;
	virtual IDirect3DDevice9* GetD3DDevice() override;
	virtual IAssetFactory* GetAssetFactory() override;
	virtual IClientBladeFactory* GetBladeFactory() override;
	virtual const IClientBladeFactory* GetBladeFactory() const override;

protected:
	Matrix44f m_view_matrix;
	Matrix44f m_projection_matrix;

	IDirect3DDevice9* m_device;
};

}; // namespace KEP
