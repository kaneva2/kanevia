///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MsgSpawnStaticMovementObj.h"
#include "CServerItemClass.h"
#include "CStructuresMisl.h"
#include "NetworkV2/Protocols.h"
#include "LogHelper.h"

static LogInstance("Engine");

namespace KEP {

MsgSpawnStaticMovementObj::MsgSpawnStaticMovementObj(unsigned messageVersion) :
		m_messageVersion(messageVersion),
		m_serverItemType((SERVER_ITEM_TYPE)-1),
		m_edbIndex(0),
		m_scale(1.0f, 1.0f, 1.0f),
		m_netTraceId(0),
		m_position(0.0f, 0.0f, 0.0f),
		m_rotationDeg(0),
		m_animationGlid(0),
		m_placementId(0),
		m_charConfig(m_charConfigInternal) {}

MsgSpawnStaticMovementObj::MsgSpawnStaticMovementObj(
	unsigned messageVersion,
	SERVER_ITEM_TYPE serverItemType,
	const std::string& name,
	const CharConfig& charConfig,
	const std::vector<GLID>& armedGlids,
	int edbIndex,
	const Vector3f& scale,
	int netTraceId,
	const Vector3f& position,
	double rotationDeg,
	GLID animationGLID,
	int placementId) :
		m_messageVersion(messageVersion),
		m_serverItemType(serverItemType),
		m_name(name),
		m_charConfig(charConfig),
		m_armedGlids(armedGlids),
		m_edbIndex(edbIndex),
		m_scale(scale),
		m_netTraceId(netTraceId),
		m_position(position),
		m_rotationDeg(rotationDeg),
		m_animationGlid(animationGLID),
		m_placementId(placementId) {
}

size_t MsgSpawnStaticMovementObj::getEncodedSize() const {
	// Calculate encoded message size
	return sizeof(SPAWN_STATIC_MO_DATA) + m_name.size() * sizeof(BYTE) + m_charConfig.getItemCount() * (sizeof(GLID_OR_BASE) + sizeof(BYTE)) + m_charConfig.getTotalSlotCount() * sizeof(CHAR_CONFIG_DATA) + m_armedGlids.size() * sizeof(GLID);
}

bool MsgSpawnStaticMovementObj::encode(unsigned char* pBytes, size_t& iNumBytes) const {
	// Encode Message Parameters
	size_t headerSize = m_messageVersion == 0 ? encodeHeaderV0(pBytes) : encodeHeaderV1(pBytes);

	// Skip header
	pBytes += headerSize;

	// Append Display Name
	std::copy(m_name.begin(), m_name.end(), pBytes);
	pBytes += m_name.size();

	// Append Character Configuration (body & head items)
	std::string charsCfgStr;
	for (size_t i = 0; i < m_charConfig.getItemCount(); i++) {
		const CharConfigItem* pItem = m_charConfig.getItemByIndex(i);
		assert(pItem != nullptr);

		// DRF - WARNING - Client holds GLID as signed int and uses <0 to mean local item
		GLID_OR_BASE glidOrBase = pItem->getGlidOrBase();
		*reinterpret_cast<GLID_OR_BASE*>(pBytes) = glidOrBase;
		pBytes += sizeof(GLID_OR_BASE);

		size_t numSlots = pItem->getSlotCount();
		*pBytes = (BYTE)numSlots;
		pBytes++;

		StrAppend(charsCfgStr, (unsigned int)glidOrBase << " ");

		auto pCfgData = reinterpret_cast<CHAR_CONFIG_DATA*>(pBytes);
		for (size_t s = 0; s < numSlots; s++) {
			const CharConfigItemSlot* pSlot = pItem->getSlot(s);
			if (!pSlot)
				continue;
			pCfgData[s].m_meshId = pSlot->m_meshId;
			pCfgData[s].m_matlId = pSlot->m_matlId;
			pBytes += sizeof(CHAR_CONFIG_DATA);
		}
	}

	// Append Armed GLIDs
	if (!m_armedGlids.empty()) {
		auto pGlidsArmed = reinterpret_cast<GLID*>(pBytes);
		pBytes += m_armedGlids.size() * sizeof(GLID);

		// Append Old Armed Inventory Objects Glids
		size_t numArmed = 0;
		std::string glidsArmedStr;

		// DRF - Added - Append New Armed Glids
		for (const auto& glid : m_armedGlids) {
			StrAppend(glidsArmedStr, glid << " ");
			pGlidsArmed[numArmed] = glid;
			numArmed++;
		}

		LogInfo(" ... glidsArmed=" << numArmed << " [" << glidsArmedStr << "]");
	}

	return true;
}

bool MsgSpawnStaticMovementObj::decode(const unsigned char* pBytes) {
	size_t nameLen = 0, charConfigItemCount = 0, numGlidsArmed = 0;

	// Decode Message Parameters
	size_t headerSize =
		m_messageVersion == 0 ?
			decodeHeaderV0(pBytes, nameLen, charConfigItemCount, numGlidsArmed) :
			decodeHeaderV1(pBytes, nameLen, charConfigItemCount, numGlidsArmed);

	// Skip header
	pBytes += headerSize;

	// Extract Displayed Name
	m_name = std::string(reinterpret_cast<const char*>(pBytes), nameLen);
	pBytes += nameLen;

	// Extract Character Configuration (cfgLength x [GLID slotCount itemCfgData] ...)
	for (size_t i = 0; i < charConfigItemCount; ++i) {
		// Extract GLID
		GLID_OR_BASE glidOrBase = *reinterpret_cast<const GLID_OR_BASE*>(pBytes);
		pBytes += sizeof(GLID_OR_BASE);

		// Extract Slot Count
		size_t slotCount = (size_t)*pBytes; // from (BYTE)
		pBytes += sizeof(BYTE);
		m_charConfigInternal.addItem(glidOrBase, slotCount);

		// Extract Slot Config Data (meshIndex materialIndex)
		CharConfigItem* pConfigItem = m_charConfigInternal.getItemByGlidOrBase(glidOrBase);
		assert(pConfigItem != nullptr);

		for (size_t j = 0; j < slotCount; j++) {
			auto pCfgData = reinterpret_cast<const CHAR_CONFIG_DATA*>(pBytes);
			pBytes += sizeof(CHAR_CONFIG_DATA);

			if (pConfigItem && j < pConfigItem->getSlotCount()) {
				CharConfigItemSlot* pSlot = pConfigItem->getSlot(j);
				pSlot->m_meshId = pCfgData->m_meshId;
				pSlot->m_matlId = pCfgData->m_matlId;
			} else {
				LogError("getItemConfig() FAILED - glidOrBase=" << glidOrBase << " j=" << j << " slotCount=" << slotCount);
			}
		}
	}

	// Extract Armed Glids
	m_armedGlids.clear();
	if (numGlidsArmed > 0) {
		const GLID* pGlidsArmed = reinterpret_cast<const GLID*>(pBytes);
		pBytes += numGlidsArmed * sizeof(GLID);

		for (size_t j = 0; j < numGlidsArmed; j++) {
			GLID glid = pGlidsArmed[j];
			m_armedGlids.push_back(glid);
		}
	}

	return true;
}

size_t MsgSpawnStaticMovementObj::encodeHeaderV0(unsigned char* pBytes) const {
	auto pMsgEncoded = reinterpret_cast<SPAWN_STATIC_MO_DATA_v0*>(pBytes);
	pMsgEncoded->m_siType = (BYTE)m_serverItemType;
	pMsgEncoded->m_nameLength = (BYTE)m_name.size();
	pMsgEncoded->m_charConfigItems = (BYTE)m_charConfig.getItemCount();
	pMsgEncoded->m_glidsArmed = (BYTE)m_armedGlids.size();
	pMsgEncoded->dbIndex = (short)m_edbIndex;
	pMsgEncoded->netTraceId = (short)m_netTraceId;
	pMsgEncoded->m_rotation = (float)m_rotationDeg;
	pMsgEncoded->Xpos = m_position.x;
	pMsgEncoded->Ypos = m_position.y;
	pMsgEncoded->Zpos = m_position.z;
	pMsgEncoded->glidAnimSpecial = m_animationGlid;
	pMsgEncoded->m_placementId = m_placementId;
	return sizeof(SPAWN_STATIC_MO_DATA_v0);
}

size_t MsgSpawnStaticMovementObj::encodeHeaderV1(unsigned char* pBytes) const {
	auto pMsgEncoded = reinterpret_cast<SPAWN_STATIC_MO_DATA*>(pBytes);
	pMsgEncoded->m_siType = (BYTE)m_serverItemType;
	pMsgEncoded->m_nameLength = (BYTE)m_name.size();
	pMsgEncoded->m_charConfigItems = (BYTE)m_charConfig.getItemCount();
	pMsgEncoded->m_glidsArmed = (BYTE)m_armedGlids.size();
	pMsgEncoded->dbIndex = (short)m_edbIndex;
	pMsgEncoded->scale = m_scale; // Version 1 - added scale vector
	pMsgEncoded->netTraceId = (short)m_netTraceId;
	pMsgEncoded->m_rotation = (float)m_rotationDeg;
	pMsgEncoded->Xpos = m_position.x;
	pMsgEncoded->Ypos = m_position.y;
	pMsgEncoded->Zpos = m_position.z;
	pMsgEncoded->glidAnimSpecial = m_animationGlid;
	pMsgEncoded->m_placementId = m_placementId;
	return sizeof(SPAWN_STATIC_MO_DATA);
}

size_t MsgSpawnStaticMovementObj::decodeHeaderV0(const unsigned char* pBytes, size_t& nameLength, size_t& charConfigLength, size_t& numGlidsArmed) {
	auto pMsgEncoded = reinterpret_cast<const SPAWN_STATIC_MO_DATA_v0*>(pBytes);
	m_serverItemType = (SERVER_ITEM_TYPE)pMsgEncoded->m_siType;
	nameLength = (size_t)pMsgEncoded->m_nameLength; // from (BYTE)
	charConfigLength = (size_t)pMsgEncoded->m_charConfigItems; // from (BYTE)
	numGlidsArmed = (size_t)pMsgEncoded->m_glidsArmed; // from (BYTE)
	m_edbIndex = (int)pMsgEncoded->dbIndex;
	m_netTraceId = (int)pMsgEncoded->netTraceId;
	m_rotationDeg = (double)pMsgEncoded->m_rotation; // from (float)
	m_position.Set(pMsgEncoded->Xpos, pMsgEncoded->Ypos, pMsgEncoded->Zpos); // from (float)
	m_animationGlid = pMsgEncoded->glidAnimSpecial; // (<0=reverse)
	m_placementId = pMsgEncoded->m_placementId;
	return sizeof(SPAWN_STATIC_MO_DATA_v0);
}

size_t MsgSpawnStaticMovementObj::decodeHeaderV1(const unsigned char* pBytes, size_t& nameLength, size_t& charConfigLength, size_t& numGlidsArmed) {
	auto pMsgEncoded = reinterpret_cast<const SPAWN_STATIC_MO_DATA*>(pBytes);
	m_serverItemType = (SERVER_ITEM_TYPE)pMsgEncoded->m_siType;
	nameLength = (size_t)pMsgEncoded->m_nameLength; // from (BYTE)
	charConfigLength = (size_t)pMsgEncoded->m_charConfigItems; // from (BYTE)
	numGlidsArmed = (size_t)pMsgEncoded->m_glidsArmed; // from (BYTE)
	m_edbIndex = (int)pMsgEncoded->dbIndex;
	m_scale = pMsgEncoded->scale; // Version 1 - added scale vector
	m_netTraceId = (int)pMsgEncoded->netTraceId;
	m_rotationDeg = (double)pMsgEncoded->m_rotation; // from (float)
	m_position.Set(pMsgEncoded->Xpos, pMsgEncoded->Ypos, pMsgEncoded->Zpos); // from (float)
	m_animationGlid = pMsgEncoded->glidAnimSpecial; // (<0=reverse)
	m_placementId = pMsgEncoded->m_placementId;
	return sizeof(SPAWN_STATIC_MO_DATA);
}

} // namespace KEP
