///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Math/Vector.h"
#include "CharConfigClass.h"

namespace KEP {

enum SERVER_ITEM_TYPE;

// Holding decoded data from SPAWN_STATIC_MO_DATA
class MsgSpawnStaticMovementObj {
	unsigned m_messageVersion;

	SERVER_ITEM_TYPE m_serverItemType;
	std::string m_name;
	const CharConfig& m_charConfig;
	std::vector<GLID> m_armedGlids;
	int m_edbIndex;
	Vector3f m_scale;
	int m_netTraceId;
	Vector3f m_position;
	double m_rotationDeg;
	GLID m_animationGlid;
	int m_placementId;

	CharConfig m_charConfigInternal;

public:
	MsgSpawnStaticMovementObj(unsigned messageVersion);
	MsgSpawnStaticMovementObj(
		unsigned messageVersion,
		SERVER_ITEM_TYPE serverItemType,
		const std::string& name,
		const CharConfig& charConfig,
		const std::vector<GLID>& armedGlids,
		int edbIndex,
		const Vector3f& scale,
		int netTraceId,
		const Vector3f& position,
		double rotationDeg,
		GLID animationGLID,
		int placementId);

	size_t getEncodedSize() const;
	bool encode(unsigned char* pBytes, size_t& iNumBytes) const;
	bool decode(const unsigned char* pBytes);

	size_t encodeHeaderV0(unsigned char* pBytes) const;
	size_t encodeHeaderV1(unsigned char* pBytes) const;
	size_t decodeHeaderV0(const unsigned char* pBytes, size_t& nameLength, size_t& charConfigLength, size_t& numGlidsArmed);
	size_t decodeHeaderV1(const unsigned char* pBytes, size_t& nameLength, size_t& charConfigLength, size_t& numGlidsArmed);

	SERVER_ITEM_TYPE getServerItemType() const { return m_serverItemType; }
	const std::string& getName() const { return m_name; }
	const CharConfig& getCharConfig() const { return m_charConfig; }
	const std::vector<GLID>& getArmedGlids() const { return m_armedGlids; }
	int getEDBIndex() const { return m_edbIndex; }
	const Vector3f& getScale() const { return m_scale; }
	int getNetTraceId() const { return m_netTraceId; }
	const Vector3f& getPosition() const { return m_position; }
	double getRotationDeg() const { return m_rotationDeg; }
	GLID getAnimationGlid() const { return m_animationGlid; }
	int getPlacementId() const { return m_placementId; }
};

} // namespace KEP
