///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <KEPConfig.h>
#include "EngineTypes.h"

#include "../Blades/EngineBlade/IEngine.h"
#include "../Blades/BladeFactory.h"
#include "Event/EventSystem/Dispatcher.h"
#include "Event/Base/IEventBlade.h"

const char* const ENGINE_TYPE_TAG = "EngineType";
const char* const ENGINE_INSTANCE_ID_TAG = "EngineInstanceId";
const char* const INSTANCENAME_TAG = "EngineName";
const char* const AUTOEXEC_TAG = "AutoexecFileName";
const char* const IPPORT_TAG = "IpPort";
const char* const IPADDR_TAG = "IpAddress";
const char* const GAMEID_TAG = "GameId";
const char* const BASEDIR_TAG = "Directory";
const char* const ENABLED_TAG = "Enabled";
const char* const ENGINE_CONFIG_TAG = "EngineConfig";
const char* const CLASSNAME_TAG = "ClassName";
const char* const SERVER_ID_TAG = "ServerId";

namespace KEP {

class BaseEngine : public IEngine {
public:
	virtual ~BaseEngine() {
		// Release Build Shutdown Crash Fix - Destroy eventBlades before eventBladeFactory!
		m_eventBlades.clear();
	}

	// IEngine overrides
	virtual bool LoadConfigValues(const IKEPConfig* cfg) override;
	virtual void SaveConfigValues(IKEPConfig* cfg) override;
	virtual const char* InstanceName() const override {
		return m_instanceName.c_str();
	}
	virtual bool Enabled() const override {
		return m_enabled;
	}
	virtual int GetGameId() const override {
		return 0;
	}

	// IBlade overrides
	virtual ULONG Version() const override {
		return m_version;
	}

	virtual std::string PathBase() const override {
		return m_pathBase;
	}
	virtual void SetPathBase(const std::string& baseDir, bool readonly) override {
		m_pathBase = baseDir;
		m_pathBaseReadOnly = readonly;
	}

	virtual const char* Name() const override {
		return m_name.c_str();
	}

	virtual void FreeAll() override {}

	static int DEFAULT_SERVER_PORT;

	static const char* typeToString(eEngineType e);

	std::string ipAddress() const { return m_ipAddress; }
	UINT port() const { return m_port_controlledaccess; }

protected:
	BaseEngine(eEngineType engineType, ULONG version, const char* name);

	bool IsBaseDirReadOnly() const {
		return m_pathBaseReadOnly;
	}

	Logger m_engineLogger;
	std::string m_instanceName;

private:
	std::string m_pathBase;
	bool m_pathBaseReadOnly;
	std::string m_name;
	ULONG m_version;

	UINT m_port_controlledaccess;

protected:
	std::string m_autoexec;
	std::string m_ipAddress;
	std::string m_serverIpAddress;
	std::string m_serverHostName;
	bool m_enabled;
	std::string m_scriptDir;

	BaseEngine& setPort(unsigned short port) {
		m_port_controlledaccess = port;
		return *this;
	}

	// event handling
	EventBladeVector m_eventBlades;
	BladeFactory m_eventBladeFactory;
	Dispatcher m_dispatcher;
};

} // namespace KEP
