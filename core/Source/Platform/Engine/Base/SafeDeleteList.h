///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

template <typename T>
void SafeDeleteList(T list) {
	if (list) {
		list->SafeDelete();
		delete list;
		list = 0;
	}
}
