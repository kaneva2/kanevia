///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include "KEPCommon.h"

#include "BaseEngine.h"

#include <jsEnumFiles.h>
#include <winsock.h>
#include <direct.h>
#include "KEPException.h"
#include "EngineStrings.h"
#include "SocketSubsystem.h"

namespace KEP {

const char* const SCRIPTDIR_TAG = "ScriptDir";
const char* const SCRIPT_DIR_SUFFIX = "\\GameFiles\\ServerScripts";

int BaseEngine::DEFAULT_SERVER_PORT = 25857;

BaseEngine::BaseEngine(eEngineType engineType, ULONG version, const char* name) :
		m_name(name),
		m_version(version),
		m_engineLogger(Logger::getInstance(Utf8ToUtf16(name))),
		m_autoexec(std::string(name) + ".xml"),
		m_ipAddress("Server"),
		m_port_controlledaccess(DEFAULT_SERVER_PORT),
		m_enabled(false),
		m_eventBladeFactory(engineType),
		m_pathBaseReadOnly(false) {}

const char* BaseEngine::typeToString(eEngineType e) {
	switch (e) {
		case eEngineType::Server:
			return "ServerEngine";
		case eEngineType::Client:
			return "ClientEngine";
		case eEngineType::AI:
			return "AIEngine";
		case eEngineType::Editor:
			return "EditorEngine";
		case eEngineType::DispatcherServer:
			return "DispatcherEngineServer";
		case eEngineType::DispatcherClient:
			return "DispatcherEngineClient";
		default:
			return "UnknownEngine";
	}
}

void GetModuleVersion(const char* fname, std::string& s) {
	std::wstring fnameW = Utf8ToUtf16(fname);
	s.clear();
	VS_FIXEDFILEINFO pvsf;
	DWORD dwHandle;
	DWORD cchver = GetFileVersionInfoSizeW(fnameW.c_str(), &dwHandle);
	if (cchver != 0) {
		char* pver = new char[cchver];
		BOOL bret = GetFileVersionInfoW(fnameW.c_str(), dwHandle, cchver, pver);
		if (bret) {
			UINT uLen;
			void* pbuf;
			bret = VerQueryValueW(pver, L"\\", &pbuf, &uLen);
			if (bret) {
				memcpy(&pvsf, pbuf, sizeof(VS_FIXEDFILEINFO));
				char ss[50];
				_snprintf_s(ss, _countof(ss), _TRUNCATE, "%d.%d.%d.%d",
					HIWORD(pvsf.dwFileVersionMS),
					LOWORD(pvsf.dwFileVersionMS),
					HIWORD(pvsf.dwFileVersionLS),
					LOWORD(pvsf.dwFileVersionLS));
				s = ss;
			}
		}
		delete[] pver;
	}
}

bool BaseEngine::LoadConfigValues(const IKEPConfig* cfg) {
	m_enabled = cfg->Get(ENABLED_TAG, true);

	// see if there is a different default directory, relative to our current base
	std::string newDir = cfg->Get(BASEDIR_TAG, "");
	if (!newDir.empty()) {
		m_pathBase = IsAbsolutePath(newDir) ? newDir : PathAddInPlace(m_pathBase, newDir.c_str());
	}

	// If script dir is set: copy if absolute, otherwise relative to current base dir.
	std::string scriptDir = cfg->Get(SCRIPTDIR_TAG, "");
	if (!scriptDir.empty()) {
		m_scriptDir = m_pathBase;
		m_scriptDir = IsAbsolutePath(scriptDir) ? scriptDir : PathAddInPlace(m_scriptDir, scriptDir.c_str());
	}

	if (m_enabled) {
		bool isDir = false;
		if (!jsFileExists(m_pathBase.c_str(), &isDir) || !isDir) {
			LOG4CPLUS_ERROR(m_engineLogger, loadStr(IDS_CFG_DIR_MISSING) << m_pathBase.c_str());
			return false;
		} else {
			if (_chdir(m_pathBase.c_str()) == -1) {
				LOG4CPLUS_ERROR(m_engineLogger, loadStr(IDS_CFG_DIR_MISSING) << m_pathBase);
				return false;
			}
		}
	}

	if (!cfg->GetValue(INSTANCENAME_TAG, m_instanceName, "NoEngineName")) // default to config file name
	{
		LOG4CPLUS_WARN(m_engineLogger, loadStr(IDS_CFG_ENGINENAME_MISSING));
	}

	NDCContextCreator ndx(Utf8ToUtf16(m_instanceName));

	if (!cfg->GetValue(AUTOEXEC_TAG, m_autoexec)) {
		LOG4CPLUS_WARN(m_engineLogger, loadStr(IDS_NO_AUTOEXEC));
	}

	setPort(cfg->Get(IPPORT_TAG, 0));
	m_ipAddress = cfg->Get(IPADDR_TAG, "");
	if (m_ipAddress.length() == 0) {
		LOG4CPLUS_WARN(m_engineLogger, loadStr(IDS_MISSING_IP));
	}

	// Get the local hostname and ip for monitoring purposes
	SocketSubSystem ssub; // initialize winsock, destructed when it goes off the stack
	char szHostName[255];
	int ret = gethostname(szHostName, 255);

	if (ret == 0) {
		struct hostent* host_entry = gethostbyname(szHostName);

		if (host_entry) {
			// No IPv6
			char* szLocalIP;
			// First one
			szLocalIP = inet_ntoa(*(struct in_addr*)*host_entry->h_addr_list);

			if (szLocalIP) {
				m_serverIpAddress = szLocalIP;
				m_serverHostName = szHostName;
			}
		}
	}

	return true;
}

void BaseEngine::SaveConfigValues(IKEPConfig* cfg) {
	cfg->SetValue(INSTANCENAME_TAG, m_instanceName.c_str());
	cfg->SetValue(AUTOEXEC_TAG, m_autoexec.c_str());
	cfg->SetValue(IPADDR_TAG, m_ipAddress.c_str());
	cfg->SetValue(IPPORT_TAG, (int)port());
	cfg->SetValue(BASEDIR_TAG, m_pathBase.c_str());
	cfg->SetValue(ENABLED_TAG, m_enabled);
	cfg->SetValue(SCRIPTDIR_TAG, m_scriptDir.c_str());
}

} // namespace KEP