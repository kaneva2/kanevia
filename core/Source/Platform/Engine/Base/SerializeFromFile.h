///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CFileFactory.h"

namespace KEP {

template <class T>
BOOL SerializeFromFile(const FileName &fileName, T &obj, Logger &logger) {
	return SerializeFromFile(fileName.str().c_str(), obj, logger);
}

template <class T>
BOOL SerializeFromFile(const char *fileName, T &obj, Logger &logger) {
	BEGIN_CFILE_SERIALIZE(fl, fileName, false)

	CArchive the_in_Archive(fl, CArchive::load);
	the_in_Archive >> obj;

	the_in_Archive.Close();

	END_CFILE_SERIALIZE(fl, logger, CStringA("SerializeFromFile: ") + fileName)

	return serializeOk;
}

template <class T, class V>
bool SerializeFromFile(const FileName &fileName, T &obj1, V &obj2, Logger &logger) {
	return SerializeFromFile(fileName.str().c_str(), obj1, obj2, logger);
}

template <class T, class V>
bool SerializeFromFile(const std::string &filePath, T &obj1, V &obj2, Logger &logger) {
	BEGIN_CFILE_SERIALIZE(fl, filePath, false)
	CArchive the_in_Archive(fl, CArchive::load);
	the_in_Archive >> obj1 >> obj2;

	the_in_Archive.Close();
	END_CFILE_SERIALIZE(fl, logger, "")

	return serializeOk;
}

} // namespace KEP