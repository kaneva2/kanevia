///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/fast_mutex.h"
namespace log4cplus {
class Logger;
}

namespace KEP {

/************************************************************************
 factory class for getting derived types of 
 CFile objects for use with CArchive

 Derive from this class, and initialize it with the derived class so 
 the static GetFile returns the derived class.  

 The CFile your class returns can do enryption, compression, etc.  
 ***********************************************************************/
class CFileFactory {
public:
	static CFile *GetFile() {
		std::lock_guard<fast_recursive_mutex> lock(getSync());
		if (m_theFactory)
			return m_theFactory->getFile();
		else
			return 0;
	}

	static void Initialize(CFileFactory *factory) {
		std::lock_guard<fast_recursive_mutex> lock(getSync());
		if (m_theFactory)
			delete m_theFactory;
		m_theFactory = factory;
	}
	static void DeInitialize() {
		std::lock_guard<fast_recursive_mutex> lock(getSync());
		if (m_theFactory)
			delete m_theFactory;
		m_theFactory = nullptr;
	}

	static void ReportSerializeError(CException *e, const char *fnName, log4cplus::Logger &logger, const char *FILE, int LINE);
	static void ReportSerializeError(CException *e, const std::string &fnName, log4cplus::Logger &logger, const char *FILE, int LINE) { ReportSerializeError(e, fnName.c_str(), logger, FILE, LINE); }

protected:
	virtual CFile *getFile() { return new CFile(); } // default impl

private:
	static CFileFactory *m_theFactory;
	static fast_recursive_mutex &getSync() { return m_sync; }
	static fast_recursive_mutex m_sync;
};

// helper macros for serializing files and processing errors
#define BEGIN_CFILE_SERIALIZE(file, fileName, storing)                                                                                                                            \
	CFile *file = CFileFactory::GetFile();                                                                                                                                        \
	bool serializeOk = true;                                                                                                                                                      \
	ASSERT(file);                                                                                                                                                                 \
	try {                                                                                                                                                                         \
		CFileException *exc = new CFileException();                                                                                                                               \
		if (!file->Open(Utf8ToUtf16(fileName).c_str(), storing ? (CFile::modeCreate | CFile::modeWrite | CFile::shareExclusive) : (CFile::modeRead | CFile::shareDenyNone), exc)) \
			throw exc;                                                                                                                                                            \
		else                                                                                                                                                                      \
			exc->Delete();

#define END_CFILE_SERIALIZE(file, logger, fnName)                                  \
	}                                                                              \
	catch (CException * e) {                                                       \
		CFileFactory::ReportSerializeError(e, fnName, logger, __FILE__, __LINE__); \
		serializeOk = false;                                                       \
	}                                                                              \
	delete file;

} // namespace KEP

