///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPCommon.h"
#include "BaseEngine.h"

class PingThd;
#define AUTOEXEC "autoexec.aut"

namespace KEP {
class CMovementObjList;
class ActorDatabase;
class CABFunctionLib;
class CAIObjList;
class CAIRaidObjList;
class CAIScriptObjList;
class CArmedInventoryList;
class CElementObjList;
class CEnvironmentObj;
class CExplosionObjList;
class CEXScriptObj;
class CEXScriptObjList;
class CGlobalInventoryObjList;
class CHousingObjList;
class CMissileObj;
class CMissileObjList;
class CMultiplayerObj;
class CPendingCommandObjList;
class CPlayerObject;
class CSkillObjectList;
class CStateManagementObj;
class CStatObjList;
class CSoundBankList;
class CTextRenderObjList;

class Engine : public BaseEngine {
public:
	std::string PathBase() const { return BaseEngine::PathBase(); }
	std::string PathGameFiles() const;
	std::string PathMapsModels() const;
	std::string PathSounds() const;
	std::string PathCustomTexture() const;
	std::string PathCharacterMeshes() const;
	std::string PathCharacterAnimations() const;
	std::string PathDynamicObjects() const;
	std::string PathEquippableItems() const;
	std::string PathParticles() const;

	virtual bool Initialize(const char* baseDir, const char* configFile, const DBConfig& dbConfig) override;

	virtual void FreeAll() override {}

	virtual int GetGameId() const override;

	virtual bool InitEvents() = 0;

	CMultiplayerObj* multiplayerObj() { return m_multiplayerObj; }

protected:
	Engine(eEngineType engineType, ULONG version, const char* name);
	virtual ~Engine();

	virtual void Callback() = 0;

	virtual bool HandleMsgs(BYTE* pData, NETID netIdFrom, size_t sizeData, bool& bDisposeMessage) = 0;

	virtual void InitAllDatabaseEntities() {}
	virtual void DestroyEnvironment();
	virtual void DestroyAllMissileClasses();
	virtual bool DestroyMovementObjectRefModelList();
	virtual BOOL RunScript(const FileName& fileName);
	virtual bool ExecAllAttributes(CEXScriptObjList* pSOL);
	virtual bool ProcessScriptAttribute(CEXScriptObj* pSO);
	virtual bool DeleteScriptList(CEXScriptObjList** ppSOL);

	virtual BOOL LoadElementDatabase(const std::string& fileName);
	virtual BOOL LoadGlobalInventoryData(const std::string& fileName);
	virtual BOOL LoadStatsDatabase(const std::string& fileName);
	virtual BOOL LoadSkillsDatabase(const std::string& fileName);
	virtual BOOL LoadHouseZoneDB(const std::string& fileName);
	virtual BOOL LoadHouseDB(const std::string& fileName);
	virtual bool LoadEntityDatabase(const std::string& fileName);
	virtual BOOL LoadMissileDatabase(const std::string& fileName);
	virtual BOOL LoadExplosionFile(const std::string& fileName);
	virtual BOOL LoadAiData(const std::string& fileName);
	virtual BOOL LoadAIRaidDB(const std::string& fileName);

	CMultiplayerObj* m_multiplayerObj;
	eEngineType m_type;
	bool m_closeDownCall;
	HANDLE m_hRenderLoopWaitableTimer; // RenderLoop delay timer

	CHousingObjList* m_housingDB;
	CMovementObjList* m_movObjRefModelList;
	ActorDatabase* m_ugcActorDB;
	CEnvironmentObj* m_environment;
	CMissileObjList* m_missileDB;
	CMissileObjList* m_missileRuntime;
	CExplosionObjList* m_explosionDBList;
	CSkillObjectList* m_skillDatabase;
	CStatObjList* m_statDatabase;
	CGlobalInventoryObjList* m_globalInventoryDB;
	CAIRaidObjList* m_aiRaidDatabase;
	CElementObjList* m_elementDB;
	CAIObjList* m_AIOL;
	CAIScriptObjList* m_pAISOL;

	// Console IO redirection
	struct {
		HANDLE handle;
		int fd;
		FILE* fp;
	} m_consoleIOHandles[3]; // Allow redirection of certain console IOs
};

} // namespace KEP