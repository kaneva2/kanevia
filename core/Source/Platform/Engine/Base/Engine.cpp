///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "Engine.h"

#include "EngineStrings.h"
#include "KEPException.h"
#include "CHousingClass.h"
#include "ActorDatabase.h"
#include "CMovementObj.h"
#include "CEnvironmentClass.h"
#include "CMissileClass.h"
#include "CExplosionClass.h"
#include "CEXScriptClass.h"
#include "CAIRaidClass.h"
#include "CAIScriptClass.h"
#include "CElementsClass.h"
#include "CGlobalInventoryClass.h"
#include "CHousePlacementClass.h"
#include "CSkillClass.h"
#include "CStatClass.h"
#include "AIclass.h"
#include "LogHelper.h"
#include "CMultiPlayerClass.h"
#include "CFileFactory.h"
#include <jsEnumFiles.h> // for jsExists
#include "SerializeFromFile.h"
#include "SafeDeleteList.h"

namespace KEP {

Engine::Engine(eEngineType engineType, ULONG version, const char* name) :
		BaseEngine(engineType, version, name),
		m_type(engineType),
		m_pAISOL(0),
		m_statDatabase(0) {
	m_multiplayerObj = NULL;
	m_hRenderLoopWaitableTimer = CreateWaitableTimer(NULL, TRUE, NULL);
	m_closeDownCall = false;
	m_housingDB = NULL;
	m_movObjRefModelList = NULL;
	m_ugcActorDB = NULL;
	m_environment = NULL;
	m_missileDB = NULL;
	m_missileRuntime = NULL;
	m_explosionDBList = NULL;
	m_skillDatabase = NULL;
	m_globalInventoryDB = NULL;
	m_movObjRefModelList = new CMovementObjList;
	m_environment = new CEnvironmentObj;
	m_missileDB = new CMissileObjList;
	m_missileRuntime = new CMissileObjList;
	m_explosionDBList = new CExplosionObjList;
	m_skillDatabase = new CSkillObjectList();
	m_globalInventoryDB = 0;
	m_housingDB = new CHousingObjList();
	m_AIOL = new CAIObjList;
	m_pAISOL = new CAIScriptObjList;
	m_aiRaidDatabase = new CAIRaidObjList();
	m_elementDB = new CElementObjList();

	ZeroMemory(&m_consoleIOHandles, sizeof(m_consoleIOHandles));
}

Engine::~Engine() {
}

std::string Engine::PathGameFiles() const {
	static std::string s_path;
	if (s_path.empty()) {
		s_path = PathAdd(PathBase(), "GameFiles");
		FileHelper::CreateFolderDeep(s_path);
	}
	return s_path;
}

std::string Engine::PathMapsModels() const {
	static std::string s_path;
	if (s_path.empty()) {
		s_path = PathAdd(PathBase(), "MapsModels");
		FileHelper::CreateFolderDeep(s_path);
	}
	return s_path;
}

std::string Engine::PathSounds() const {
	static std::string s_path;
	if (s_path.empty()) {
		s_path = PathAdd(PathBase(), "Sounds");
		FileHelper::CreateFolderDeep(s_path);
	}
	return s_path;
}

std::string Engine::PathCustomTexture() const {
	static std::string s_path;
	if (s_path.empty()) {
		s_path = PathAdd(PathBase(), "CustomTexture");
		FileHelper::CreateFolderDeep(s_path);
	}
	return s_path;
}

std::string Engine::PathCharacterMeshes() const {
	static std::string s_path;
	if (s_path.empty()) {
		s_path = PathAdd(PathGameFiles(), "CharacterMeshes");
		FileHelper::CreateFolderDeep(s_path);
	}
	return s_path;
}

std::string Engine::PathCharacterAnimations() const {
	static std::string s_path;
	if (s_path.empty()) {
		s_path = PathAdd(PathGameFiles(), "CharacterAnimations");
		FileHelper::CreateFolderDeep(s_path);
	}
	return s_path;
}

std::string Engine::PathDynamicObjects() const {
	static std::string s_path;
	if (s_path.empty()) {
		s_path = PathAdd(PathGameFiles(), "DynamicObjects");
		FileHelper::CreateFolderDeep(s_path);
	}
	return s_path;
}

std::string Engine::PathEquippableItems() const {
	static std::string s_path;
	if (s_path.empty()) {
		s_path = PathAdd(PathGameFiles(), "EquippableItems");
		FileHelper::CreateFolderDeep(s_path);
	}
	return s_path;
}

std::string Engine::PathParticles() const {
	static std::string s_path;
	if (s_path.empty()) {
		s_path = PathAdd(PathGameFiles(), "Particles");
		FileHelper::CreateFolderDeep(s_path);
	}
	return s_path;
}

bool Engine::Initialize(const char* baseDir, const char* configFile, const DBConfig& /*dbConfig*/) {
	// moved here so gets created on the init
	m_globalInventoryDB = CGlobalInventoryObjList::GetInstance();
	m_dispatcher.Initialize();
	return true;
}

BOOL Engine::RunScript(const FileName& fileName) {
	try {
		// If fileName.xml exists read that otherwise do old way
		CEXScriptObjList* scriptList = 0;
		FileName fileNameXml(fileName);
		fileNameXml.setExt(".xml");
		bool xmlExists = ::jsFileExists(fileNameXml.str().c_str(), NULL);
		if (xmlExists) {
			scriptList = new CEXScriptObjList();
			scriptList->SerializeFromXML(fileNameXml); // throws
		} else {
			if (!SerializeFromFile(fileName.toString(), scriptList, m_engineLogger)) {
				LogErrorTo(m_engineLogger, "SerializeFromFile() FAILED - '" << fileName.toString() << "'");
				return FALSE;
			}
		}

		if (!xmlExists) {
			// create it for next time!
			LogWarnTo(m_engineLogger, "'" << fileNameXml.toString() << "' NOT FOUND - Creating...");
			scriptList->SerializeToXML(fileNameXml);
		}

		ExecAllAttributes(scriptList);
		DeleteScriptList(&scriptList);

		return TRUE;
	} catch (KEPException* e) {
		LogErrorTo(m_engineLogger, "Caught Exception - " << fileName.str() << " " << e->m_msg << " " << e->m_err);
		e->Delete();
		return FALSE;
	}
}

bool Engine::ExecAllAttributes(CEXScriptObjList* pSOL) {
	if (!pSOL)
		return false;

	for (POSITION posLoc = pSOL->GetHeadPosition(); posLoc;) {
		auto pSO = (CEXScriptObj*)pSOL->GetNext(posLoc);
		ProcessScriptAttribute(pSO);
		if (pSO->m_miscInt == MISCINT_BREAK_FROM_SCRIPT)
			break;
	}

	return true;
}

bool Engine::DeleteScriptList(CEXScriptObjList** ppSOL) {
	CEXScriptObjList* pSOL = *ppSOL;
	if (!pSOL)
		return false;

	for (POSITION posLoc = pSOL->GetHeadPosition(); posLoc;) {
		auto pSO = dynamic_cast<CEXScriptObj*>(pSOL->GetNext(posLoc));
		if (!pSO)
			continue;
		delete pSO;
	}

	pSOL->RemoveAll();
	delete pSOL;
	pSOL = nullptr;
	*ppSOL = pSOL;

	return true;
}

bool Engine::ProcessScriptAttribute(CEXScriptObj* pSO) {
	if (!pSO)
		return false;

	// Must load global inventory before all other inventories. Other inventories
	// will only reference the global inventory and not create their own.
	if (m_globalInventoryDB == 0 || m_globalInventoryDB->GetSize() == 0)
		LoadGlobalInventoryData("Inventory.GIL");

	// UPDATE NOTE: If the processing of these actions are updated to either
	// load a new texture into a CEXMeshObj or involve the removal of an existing
	// CEXMeshObj, the Editor's distribution.cpp module must be updated
	// (methods Mesh_*) to identify this object.
	switch (pSO->m_actionAttribute) {
		case LOAD_ENTITY_DATABASE:
			LoadEntityDatabase(pSO->m_miscString.GetString());
			break;

		case INIT_TCP_DEFAULTS:
			break;

		case LOAD_GLOBAL_INVENTORY:
			if (m_globalInventoryDB == 0 || m_globalInventoryDB->GetSize() == 0)
				LoadGlobalInventoryData(pSO->m_miscString.GetString());
			break;

		case LOAD_MISSLE_DATABASE:
			LoadMissileDatabase(pSO->m_miscString.GetString());
			break;

		case LOAD_EXPLOSION_DATABASE:
			LoadExplosionFile(pSO->m_miscString.GetString());
			break;

		case LOAD_SKILLS_DATABASE:
			LoadSkillsDatabase(pSO->m_miscString.GetString());
			break;

		case LOAD_HOUSE_ZONES:
			LoadHouseZoneDB(pSO->m_miscString.GetString());
			break;

		case LOAD_HOUSING:
			LoadHouseDB(pSO->m_miscString.GetString());
			break;

		case LOAD_AI_CFG:
			LoadAiData(pSO->m_miscString.GetString());
			break;

		case LOAD_AI_RAID_DB:
			LoadAIRaidDB(pSO->m_miscString.GetString());
			break;

		default:
			return false;
	}

	return true;
}

BOOL Engine::LoadAIRaidDB(const std::string& fileName) {
	SafeDeleteList(m_aiRaidDatabase);

	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), m_aiRaidDatabase, m_engineLogger)) {
		LogErrorTo(m_engineLogger, "SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	}

	return TRUE;
}

BOOL Engine::LoadElementDatabase(const std::string& fileName) {
	SafeDeleteList(m_elementDB);

	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), m_elementDB, m_engineLogger)) {
		LogErrorTo(m_engineLogger, "SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	}

	return TRUE;
}

BOOL Engine::LoadGlobalInventoryData(const std::string& fileName) {
	// Random clients can however, determine that the list can be reset
	m_globalInventoryDB->SafeDelete();
	GILSerializer* serializer;
	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), serializer, m_engineLogger)) {
		LogErrorTo(m_engineLogger, "SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	}

	// Okay, we serialized okay...
	// Copy the contents we just read into the globaal list
	CGlobalInventoryObjList* ptemp = serializer->retrieveList();
	m_globalInventoryDB->copyFrom(*ptemp);
	delete ptemp;
	delete serializer;
	return TRUE;
}

BOOL Engine::LoadStatsDatabase(const std::string& fileName) {
	delete m_statDatabase;
	m_statDatabase = NULL;

	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), m_statDatabase, m_engineLogger)) {
		LogErrorTo(m_engineLogger, "SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	}

	return TRUE;
}

BOOL Engine::LoadSkillsDatabase(const std::string& fileName) {
	delete m_skillDatabase;
	m_skillDatabase = NULL;

	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), m_skillDatabase, m_engineLogger)) {
		LogErrorTo(m_engineLogger, "SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	}

	return TRUE;
}

BOOL Engine::LoadHouseZoneDB(const std::string& fileName) {
	SafeDeleteList(m_multiplayerObj->m_housingDB);

	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), m_multiplayerObj->m_housingDB, m_engineLogger)) {
		LogErrorTo(m_engineLogger, "SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	}

	return TRUE;
}

bool Engine::LoadEntityDatabase(const std::string& fileName) {
	CMovementObjList* pMOL;
	wchar_t buffer[_MAX_PATH];
	_wgetcwd(buffer, _MAX_PATH);
	CStringW CurDir = buffer;
	BOOL success = SerializeFromFile(PathAdd(PathGameFiles(), fileName), pMOL, m_engineLogger);
	if (success) {
		InitAllDatabaseEntities();

		// destroy list that will be filled
		DestroyMovementObjectRefModelList();

		// DRF - Movement Object Ref Model List Is Loaded Entity List
		m_movObjRefModelList = pMOL;

		if (m_ugcActorDB)
			delete m_ugcActorDB;
		m_ugcActorDB = new ActorDatabase(m_movObjRefModelList);
	} else {
		LogErrorTo(m_engineLogger, "SerializeFromFile() FAILED - '" << fileName << "'");
		SetCurrentDirectory(CurDir);
		return false;
	}

	return true;
}

// server needs this to validate hits
BOOL Engine::LoadMissileDatabase(const std::string& fileName) {
	CMissileObjList* tempList;
	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), tempList, m_engineLogger)) {
		LogErrorTo(m_engineLogger, "SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	} else {
		DestroyAllMissileClasses();
		m_missileRuntime = new CMissileObjList;
		m_missileDB = tempList;
	}
	return TRUE;
}

// server needs this for damage
BOOL Engine::LoadExplosionFile(const std::string& fileName) {
	CExplosionObjList* tempList;

	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), tempList, m_engineLogger)) {
		LogErrorTo(m_engineLogger, "SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	}

	SafeDeleteList(m_explosionDBList);
	m_explosionDBList = tempList;

	return TRUE;
}

BOOL Engine::LoadAiData(const std::string& fileName) {
	SafeDeleteList(m_AIOL);
	SafeDeleteList(m_pAISOL);

	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), m_AIOL, m_pAISOL, m_engineLogger)) {
		LogErrorTo(m_engineLogger, "SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	}

	m_AIOL->ResetIndexReferences();

	return TRUE;
}

BOOL Engine::LoadHouseDB(const std::string& fileName) {
	SafeDeleteList(m_housingDB);

	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), m_housingDB, m_engineLogger)) {
		LogErrorTo(m_engineLogger, "SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	}

	return TRUE;
}

bool Engine::DestroyMovementObjectRefModelList() {
	if (!m_movObjRefModelList)
		return false;

	POSITION posLoc;
	for (posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		auto pMO = (CMovementObj*)m_movObjRefModelList->GetNext(posLoc);
		pMO->SafeDelete();
		delete pMO;
	}

	m_movObjRefModelList->RemoveAll();
	delete m_movObjRefModelList;
	m_movObjRefModelList = 0;

	return true;
}

void Engine::DestroyEnvironment() {
	SafeDeleteList(m_environment);
}

void Engine::DestroyAllMissileClasses() {
	// runtime
	if (m_missileRuntime) {
		for (POSITION posLoc = m_missileRuntime->GetHeadPosition(); posLoc != NULL;) { // visual loop
			CMissileObj* mPtr = (CMissileObj*)m_missileRuntime->GetNext(posLoc);
			SafeDeleteList(mPtr);
		}

		m_missileRuntime->RemoveAll();
		delete m_missileRuntime;
	}

	m_missileRuntime = 0;

	// DB
	if (m_missileDB) {
		for (POSITION posLoc = m_missileDB->GetHeadPosition(); posLoc != NULL;) { // visual loop
			CMissileObj* mPtr = (CMissileObj*)m_missileDB->GetNext(posLoc);
			SafeDeleteList(mPtr);
		}

		m_missileDB->RemoveAll();
		delete m_missileDB;
	}

	m_missileDB = 0;
}

int Engine::GetGameId() const {
	if (!m_multiplayerObj || !m_multiplayerObj->m_serverNetwork)
		return 0;
	return m_multiplayerObj->m_serverNetwork->pinger().GetGameId();
}

} // namespace KEP