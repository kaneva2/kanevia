///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CFileFactory.h"
#include "EngineStrings.h"
#include "Core/Util/Unicode.h"

namespace KEP {

CFileFactory *CFileFactory::m_theFactory = 0;
fast_recursive_mutex CFileFactory::m_sync;

void CFileFactory::ReportSerializeError(CException *e, const char *fnName, log4cplus::Logger &logger, const char *FILE, int LINE) {
	CStringW s;
	wchar_t buff[1024];
	e->GetErrorMessage(buff, 1024);
	s.Format(IDS_SERIALIZE_EXECPTION, Utf8ToUtf16(fnName).c_str(), buff);
}

} // namespace KEP
