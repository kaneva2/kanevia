///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include ".\OldPhysics.h"

#include "CMovementObj.h"
#include "CFrameObj.h"
#include "CPhysicsEmuClass.h"
#include "CEnvironmentClass.h"
#include "CWaterZoneClass.h"
#include "Core/Math/Rotation.h"
#include "Core/Math/Angle.h"
#include "LogHelper.h"
#include "boost/format.hpp"
#include "IClientEngine.h"

#include "common\include\glAdjust.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

void OldPhysics::MovPhysicsSystem(
	CMovementObj* pMO,
	const Vector3f& posForceExt,
	const Vector3f& rotForceExt,
	const Vector3f& posDeltaExt) {
	if (!pMO || !pMO->getBaseFrame() || !m_pEnvironment)
		return;

	auto pMC = pMO->getMovementCaps();
	if (!pMC)
		return;

	// Update FreeFall Detector
	m_freeFallDetector.SetSurfaceProperty(pMC->GetSurfaceProp());
	m_freeFallDetector.SetIsJumpApplied(pMC->IsJumping());
	m_freeFallDetector.SetIsCollisionEnabled(pMC->IsCollisionEnabled());

	// Get Frame Rate Ratio
	auto deltaT = RenderMetrics::GetFrameTimeRatio();

	// Get Positional Drag
	bool offTheGround = (pMC->IsGravityEnabled() || pMC->IsJumping());
	float posDrag = offTheGround ? pMC->GetPosDrag() : pMC->GetPosGroundDrag();

	// Get Positional Force
	auto posForce = pMC->GetPosForce();
	posForce.x = -posForce.x;
	posForce.z = -posForce.z;
	if (offTheGround)
		posForce *= pMC->GetPosForceAirGain();

	// Convert Positional Force To World Coordinates
	auto baseFrameMtx = pMO->getBaseFrameMatrix();
	posForce = TransformVector(baseFrameMtx, posForce);
	posForce += (posForceExt * (1.0f - pMC->GetPosDrag()));

	// Add Gravity
	float gravityApplicable = 0.0f;
	if ((pMC->GetAntiGravity() == 0.0) && pMC->IsGravityEnabled())
		gravityApplicable = m_pEnvironment->m_gravity;
	posForce.y += gravityApplicable * (1.0 / deltaT);

	// Add Jump ?
	auto startJump = (pMC->GetJumpForce() > 0.0);
	if (startJump) {
		posForce.y += pMC->GetJumpForce() * (1.0 / (deltaT * deltaT));
		pMC->SetJumpForce(0.0); // jump force is an impulse

		// DRF - Added - Start Jump Animation Now!
		// Assures jump physics and jump animation are in sync
		auto pICE = IClientEngine::Instance();
		if (pICE)
			pICE->EntityAnimationManagementCallbackForMe();
	}

	// Calculate Positional Velocity
	auto pos = pMO->getBaseFramePosition();
	auto posVel = (pos - pMC->GetPosLast()) * (1.0 / deltaT);
	if (!offTheGround) {
		// DRF - ED-7877 - Avatar Slow Slide Bug
		auto posSpeed = posVel.Length();
		if (posSpeed < 0.01)
			posVel.Fill(0.0);
	}
	pMC->SetPosVel(posVel);
	pMC->SetPosLast(pos);

	// Calculate New Position
	auto posNew = CalculateNewPos(
		pos,
		posVel,
		posDrag,
		posForce);

	// Get Rotational Drag
	auto rotDrag = offTheGround ? pMC->GetRotDrag() : (pMC->GetRotGroundDrag() - (1.0f - pMC->GetRotDrag()));

	// Get Rotational Force
	auto rotForce = pMC->GetRotForce();
	rotForce.x = -rotForceExt.x;
	rotForce.y = -rotForceExt.y;
	rotForce.z = -(rotForceExt.z + rotForce.z);

	// Calculate New Rotation
	auto rot = pMC->GetRot();
	auto rotNew = CalculateNewRot(
		rot,
		rotDrag,
		rotForce);
	pMC->SetRot(rotNew);

	Matrix44f rotMatrixZ;
	Matrix44f rotMatrixX;
	Matrix44f rotMatrixY;
	ConvertRotationX(ToRadians(rotNew.x), out(rotMatrixX));
	ConvertRotationY(ToRadians(rotNew.y), out(rotMatrixY));
	ConvertRotationZ(ToRadians(rotNew.z), out(rotMatrixZ));
	Matrix44f rotMatrix = rotMatrixZ * rotMatrixX * rotMatrixY;
	Matrix44f newMatrix = rotMatrix * pMO->getBaseFrameMatrix();

	// Adjust For External Positional Movement (eg. moving platforms)
	newMatrix(3, 0) = posNew.x + posDeltaExt.x;
	newMatrix(3, 1) = posNew.y + posDeltaExt.y;
	newMatrix(3, 2) = posNew.z + posDeltaExt.z;

	// Update CMovementCaps::m_lastPosition to avoid introducing speed changes
	// CMovementObj::m_lastPosition will also be updated later in caller
	pMC->SetPosLast(pos + posDeltaExt);

	pMO->setBaseFrameMatrix(newMatrix, true);
}

Vector3f OldPhysics::CalculateNewPos(
	const Vector3f& pos,
	const Vector3f& posVel,
	float posDrag,
	const Vector3f& posForce) {
	auto deltaT = RenderMetrics::GetFrameTimeRatio();
	Vector3f velNew = (posVel + posForce * deltaT) * posDrag;
	Vector3f posNew = pos + (velNew * deltaT);

	m_freeFallDetector.Update(posNew, velNew);

	return posNew;
}

Vector3f OldPhysics::CalculateNewRot(
	const Vector3f& rot,
	float rotDrag,
	const Vector3f& rotForce) {
	auto deltaT = RenderMetrics::GetFrameTimeRatio();
	Vector3f rotNew = (rot + rotForce * deltaT) * rotDrag;

	return rotNew;
}

} // namespace KEP
