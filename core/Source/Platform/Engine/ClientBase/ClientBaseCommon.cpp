///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include "ClientBaseEngine.h"

#include "OldPhysics.h"
#include "../Base/EngineStrings.h"
#include "KEPHelpers.h"
#include "KEPDiag.h"
#include "CMultiplayerClass.h"
#include "CMovementObj.h"
#include "CCollisionClass.h"
#include "CEXScriptClass.h"
#include "CHousingClass.h"
#include "ViewPortClass.h"
#include "CCameraClass.h"
#include "CtextReadoutClass.h"
#include "CFrameObj.h"
#include "CWldObject.h"
#include "CHousingClass.h"
#include "CRTLightClass.h"
#include "CReferenceLibrary.h"
#include "CExplosionClass.h"
#include "CMissileClass.h"
#include "CEnvironmentClass.h"
#include "CGlobalInventoryClass.h"
#include "CInventoryClass.h"
#include "CSafeZones.h"
#include "CWaterZoneClass.h"
#include "CWorldGroupClass.h"
#include "AIclass.h"
#include "CArmedInventoryClass.h"
#include "CPhysicsEmuClass.h"
#include "SkeletonConfiguration.h"
#include "RuntimeSkeleton.h"
#include "CSkeletonObject.h"
#include "UgcConstants.h"
#include "ActorDatabase.h"
#include "PhysicsUserData.h"
#include "../KEPPhysics/Avatar.h"
#include "../KEPPhysics/PhysicsFactory.h"
#include "Core/Math/Units.h"
#include "IClientEngine.h" // ClientBaseEngine should not depend on IClientEngine - we need to fix this
#include "EquippableSystem.h"
#include <KEPPhysics/Universe.h>
#include <KEPPhysics/Vehicle.h>

namespace KEP {

static LogInstance("Instance");

bool ClientBaseEngine::CreateMovementObject(
	int aiConfig,
	const Matrix44f& transformMatrix,
	int dbIndex,
	eActorControlType controlType,
	int networkDefinedID,
	bool networkEntity,
	bool addHead,
	bool spawnAtServer,
	int generatorLinkReference,
	CMovementObj** ppMO_out,
	const CharConfig* pCharConfig,
	bool bLocal,
	const std::string& name) {
	if (dbIndex < 0 || !m_movObjList || !m_movObjRefModelList)
		return false;

	// Get Movement Object Reference Model
	POSITION dbIndexPos = m_movObjRefModelList->FindIndex(dbIndex);
	if (!dbIndexPos)
		return false;
	auto pMORM = (CMovementObj*)m_movObjRefModelList->GetAt(dbIndexPos);
	if (!pMORM)
		return false;

	// Create New Movement Object (based on reference model)
	int* pMeshIdsOpt = NULL;
	int* pMatlIdsOpt = NULL;
	D3DCOLORVALUE* pColorsOpt = NULL;
	if (pCharConfig) {
		size_t baseSlotsRM = (size_t)pMORM->getSkeleton()->getSkinnedMeshCount();
		pMeshIdsOpt = new int[baseSlotsRM];
		pMatlIdsOpt = new int[baseSlotsRM];
		pColorsOpt = new D3DCOLORVALUE[baseSlotsRM];

		const CharConfigItem* pBaseConfigItem = pCharConfig->getItemByGlidOrBase(GOB_BASE_ITEM);
		assert(pBaseConfigItem != nullptr);

		for (size_t s = 0; s < baseSlotsRM; s++) {
			if (pBaseConfigItem != nullptr && s < pBaseConfigItem->getSlotCount()) {
				const CharConfigItemSlot* pSlot = pBaseConfigItem->getSlot(s);
				assert(pSlot != nullptr);
				if (!pSlot)
					continue;
				pMeshIdsOpt[s] = pSlot->m_meshId;
				pMatlIdsOpt[s] = pSlot->m_matlId;
				pColorsOpt[s].r = pSlot->m_r;
				pColorsOpt[s].g = pSlot->m_g;
				pColorsOpt[s].b = pSlot->m_b;
			} else {
				pMeshIdsOpt[s] = 0;
				pMatlIdsOpt[s] = 0;
				pColorsOpt[s].r = -1.0f;
				pColorsOpt[s].g = -1.0f;
				pColorsOpt[s].b = -1.0f;
			}
		}
	}

	// Get Next Free Trace Number
	auto traceNumber = (int)m_movObjList->getNextFreeTraceNumber();

	// Clone Reference Model Into New Movement Object
	CMovementObj* pMO = NULL;
	EntityClone(
		pMORM,
		&pMO,
		dbIndex,
		pMeshIdsOpt, pMatlIdsOpt, pColorsOpt,
		traceNumber,
		networkDefinedID);

	// Give It A Name
	if (pMO->getName() == "Jimmy" || pMO->getName() == "Hondo" ||
		pMO->getName() == "Meaty" || pMO->getName() == "Isabella" ||
		pMO->getName() == "Sofia" || pMO->getName() == "Eva") {
		if (name.empty()) {
			pMO->setName("_____");
		} else {
			pMO->setName(name); // drf - added
		}
	}

	// Configure All Equippable Items ?
	if (pCharConfig) {
		delete pMeshIdsOpt;
		delete pMatlIdsOpt;
		delete pColorsOpt;
		for (size_t i = CC_ITEM_FIRST_EQUIP; i < pCharConfig->getItemCount(); i++)
			ConfigPlayerEquippableItem(pMO, pCharConfig->getItemByIndex(i));
	}

	pMO->setControlType(controlType);
	pMO->setNetworkEntity(networkEntity);
	pMO->setIdxRefModel(dbIndex);

	// base frame cam setup
	pMO->setBaseFrame(new CFrameObj(NULL));
	pMO->setBaseFrameMatrix(transformMatrix, true);
	pMO->saveLastPositionFromBaseFrame();

	pMO->MovementInterpolatorInit();

	// insert list object
	if (addHead)
		m_movObjList->AddHead(pMO);
	else
		m_movObjList->AddTail(pMO);

	if (controlType == eActorControlType::PC) {
		pMO->setCurrentCameraDataIndex(-1);
		InitCamerasForMovementObject(pMO);
	}

	// Physics
	pMO->initPhysicsObjects();
	m_pUniverse->Add(pMO->getVisiblePhysicsBody());

	// Copy Movement Object To Given Pointer
	if (ppMO_out)
		*ppMO_out = pMO;

	// Spawn To Server If Me
	if (!networkEntity && ClientIsEnabled() && spawnAtServer) {
		ZoneIndex zoneIndex(m_zoneIndex);
		int aiCfgIndex = 0;
		int useNewPresets = (controlType == eActorControlType::PC) ? -1 : 0;
		m_multiplayerObj->SendMsgSpawnToServer(
			pMO->getIdxRefModel(),
			zoneIndex,
			m_zoneFileName,
			pMO->getTraceNumber(),
			useNewPresets,
			m_multiplayerObj->m_currentCharacterInUse,
			aiCfgIndex);
	}

	if (!ClientIsEnabled() || bLocal) {
		// Arm Configured Object List Items (don't realize until end, don't arm defaults until end)
		if (pMO->getAutoCfg()) {
			LogInfo("ARMING autoCfg ...");
			if (!pMO->armItems(pMO->getAutoCfg())) {
				LogError(pMO->ToStr() << " ArmItems(CfgObjList) FAILED");
				// just do our best anyway
			}
		}

		// ED-5023 - Accessory Import Male/Female Actor Head Missing
		// Puts Head On Male/Female Model In Accessory Import - Do Not Remove!
		POSITION aiPos = m_AIOL->FindIndex(aiConfig);
		if (aiPos) {
			auto pAIO = (CAIObj*)m_AIOL->GetAt(aiPos);
			if (pAIO->m_inventoryCfg && pAIO->m_inventoryCfg->GetCount()) {
				// arm inventory items
				LogInfo("ARMING inventoryCfg ...");
				for (POSITION pos = pAIO->m_inventoryCfg->GetHeadPosition(); pos;) {
					auto pIO = dynamic_cast<CInventoryObj*>(pAIO->m_inventoryCfg->GetNext(pos));
					if (!pIO)
						continue;

					if (pIO->isArmed()) {
						GLID glid = pIO->m_itemData->m_globalID;

						// Arm Entity Item (don't realize until end, don't arm defaults until end)
						if (!ArmItem(glid, pMO, false, false)) {
							LogError("ArmEntity() FAILED - glid<" << glid << "> on " << pMO->getName());
							// just do the best we can
						}

						pMO->getSkeleton()->realizeEquippableItemDimConfig(m_equippableRM);
					}
				}

				// Arm Defaults Not Excluded (prevent nakedness)
				pMO->armDefaultItemsNotExcluded();

				// Realize Now
				pMO->realizeDimConfig();
			}
		}
	}

	// set fade in
	// TODO - Get Rid Of This Performance Issue!
	if (pMO->getSkeleton() && pMO->getSkeleton()->areAllMeshesLoaded())
		pMO->fadeIn(0.0); // 1000 );

	// DRF - Added - Arm/Disarm Not Allowed Until ClientBaseEngine::HandleMovementObjectCfgEvent()
	bool allowArmDisarm = (controlType == eActorControlType::STATIC);
	pMO->setAllowArmDisarm(allowArmDisarm);

	LogInfo(pMO->ToStr() << " OK");

	return true;
}

bool ClientBaseEngine::DestroyMovementObjectList() {
	if (!m_movObjList)
		return false;

	ExitAllTriggers();

	for (POSITION posLoc = m_movObjList->GetHeadPosition(); posLoc != NULL;) {
		auto pMO = (CMovementObj*)m_movObjList->GetNext(posLoc);
		DestroyMovementObject(pMO);
	}
	m_movObjList->RemoveAll();
	return true;
}

bool ClientBaseEngine::DestroyMovementObject(CMovementObj* pMO) {
	if (!pMO)
		return false;

	// Remove Movement Object From List
	for (POSITION pos = m_movObjList->GetHeadPosition(); pos;) {
		auto pos_find = pos;
		auto pMO_find = (CMovementObj*)m_movObjList->GetNext(pos);
		if (pMO_find == pMO) {
			m_movObjList->RemoveAt(pos_find);
			break;
		}
	}

	// Remove Possible Physics Vehicle
	if (pMO->getPhysicsVehicle())
		m_pUniverse->Remove(pMO->getPhysicsVehicle().get());
	if (pMO->getVehicleRigidBody())
		m_pUniverse->Remove(pMO->getVehicleRigidBody().get());
	m_pUniverse->Remove(pMO->getVisiblePhysicsBody().get());

	// Delete Movement Object
	pMO->SafeDelete();
	delete pMO;

	return true;
}

// DRF - I think somewhere around here is where we are messing up the avatar to prevent being able to
// remove the shoes and still see the feet and remove the shirt and actually see the body instead
// of some seemingly random shirt in it's place in the first zone of our new avatar.
bool ClientBaseEngine::ConfigPlayerEquippableItem(CMovementObj* pMO, const CharConfigItem* pCCI) {
	if (!pMO)
		return false;

	auto pRS = pMO->getSkeleton();
	if (!pRS)
		return false;

	// Already Armed ?
	bool alreadyArmed = (pRS->getEquippableItemByGLID(pCCI->getGlidOrBase()) != nullptr);
	if (alreadyArmed)
		return true;

	// Obtain item exclusion group
	std::set<int> exclusionGroupIDs;
	CGlobalInventoryObj* pGIO = CGlobalInventoryObjList::GetInstance()->GetByGLID(pCCI->getGlidOrBase());
	if (pGIO && pGIO->m_itemData)
		exclusionGroupIDs.insert(pGIO->m_itemData->m_exclusionGroupIDs.begin(), pGIO->m_itemData->m_exclusionGroupIDs.end());

	// now arm this new item
	if (pRS->ArmEquippableItem(pCCI->getGlidOrBase(), exclusionGroupIDs, pMO, pMO->getSkeletonConfig(), pCCI) == RuntimeSkeleton::eEquipArmState::Failed) {
		LogError("ArmEquippableItem() FAILED - glidOrBase=" << pCCI->getGlidOrBase());
		return false;
	}

	return true;
}

} // namespace KEP