///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IOldPhysics.h"

#include "FreeFallDetector.h"

namespace KEP {

class OldPhysics : public IOldPhysics {
public:
	OldPhysics() :
			m_pEnvironment(nullptr) {}

	virtual ~OldPhysics(void) {}

	void OldPhysics::Initialize(const CEnvironmentObj* pEO) override final {
		m_pEnvironment = pEO;
	}

	void SetEnabledFreeFall(bool enable) {
		m_freeFallDetector.SetEnabled(enable);
	}

	void OldPhysics::MovPhysicsSystem(
		CMovementObj* pMO,
		const Vector3f& posForceExt, // drf - dead code - impulse force from TriggerObject::TA_ACTIVATE_FORCE_PAD
		const Vector3f& rotForceExt,
		const Vector3f& posDeltaExt) override final;

private:
	Vector3f CalculateNewPos(
		const Vector3f& pos,
		const Vector3f& posVel,
		float posDrag,
		const Vector3f& posForce);

	Vector3f CalculateNewRot(
		const Vector3f& rot,
		float rotDrag,
		const Vector3f& rotForce);

	const CEnvironmentObj* m_pEnvironment;

	FreeFallDetector m_freeFallDetector;
};

} // namespace KEP
