///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PathFinding.h"
#include "math.h"
#include "matrixarb.h"
#include "ClientBaseEngine.h"
#include "CMovementObj.h"
#include "CPhysicsEmuClass.h"
#include "CFrameObj.h"

#define FP_TOLERANCE 1E-6
#define ROTATION_TOLERANCE (5 * (M_PI / 180.0)) // 5 degree
#define MIN_DISTANCE 1.0f
#define MIN_DISTANCE_SQ (MIN_DISTANCE * MIN_DISTANCE)

namespace KEP {

// Return true if path available, false otherwise
bool SimplePathFinder::Start(
	IKinematicEntity* pObj,
	const Vector3f& destPos,
	const Vector3f& destRot,
	float desiredSpeed,
	int recalcInterval,
	int motionStyle,
	bool bPhysicsEnabled,
	int timeoutInMS,
	bool bPlayerControl) {
	if (!pObj)
		return false;

	Reset();

	m_pObject = pObj;
	m_destPos = destPos;
	m_destRot = destRot;
	m_posForceY = desiredSpeed;
	m_motionStyle = motionStyle;
	m_recalcInterval = recalcInterval;
	m_bPhysicsEnabled = bPhysicsEnabled;
	m_timeoutInMS = timeoutInMS;
	m_bPlayerControl = bPlayerControl;
	m_pObject->GetPosition(m_initPos);
	m_pObject->GetOrientation(m_initRot.x, m_initRot.z);
	m_timer.Start();
	m_prevRotationDirection = 0;

	SetState(ENROUTE);

	m_dir = m_destPos - m_initPos;
	if (m_bPhysicsEnabled)
		m_dir.y = 0.0f;

	float distSq = m_dir.LengthSquared();
	if (distSq > MIN_DISTANCE_SQ) {
		m_distance = sqrt(distSq);
		m_dir /= m_distance;
		m_subState = INITIAL_ROTATING;
	} else {
		// Already at destination
		m_dir.MakeZero();
		m_subState = FINAL_ROTATING;
	}

	return true;
}

// Return path finding state.
int SimplePathFinder::Update() {
	// Check if enroute
	if (m_pObject == NULL || GetState() != ENROUTE || m_subState == NA)
		return GetState();

	m_updateCount++;

	// check if timeout
	unsigned long elapsed = m_timer.ElapsedMs();
	if (m_timeoutInMS != 0 && elapsed > m_timeoutInMS) {
		// timeout
		SetState(CANCELED);
		SetCancelReason(TIMEOUT);
		return CANCELED;
	}

	// check if rotating
	if (m_subState == INITIAL_ROTATING || m_subState == FINAL_ROTATING) {
		Vector3f currRot(0.0f, 0.0f, 0.0f);
		Vector3f nextRot(0.0f, 0.0f, 0.0f);
		m_pObject->GetOrientation(currRot.x, currRot.z);

		if (m_subState == INITIAL_ROTATING) {
			// check initial orientation
			nextRot = -m_dir;
		} else {
			// check final orientation
			nextRot = m_destRot;
		}

		int rotDir = 0;
		float currOri = atan2(currRot.x, currRot.z);
		float nextOri = atan2(nextRot.x, nextRot.z);
		if (abs(nextOri - currOri) > ROTATION_TOLERANCE || M_PI * 2.0f - abs(nextOri - currOri) > ROTATION_TOLERANCE) {
			if (nextOri > currOri)
				rotDir = (nextOri - currOri) > M_PI ? 1 : -1;
			else
				rotDir = (currOri - nextOri) <= M_PI ? 1 : -1;
		}

		if (rotDir == 0 || rotDir == -m_prevRotationDirection) { // Either exact match or just passed
			// desired orientation reached
			if (nextRot.x != 0.0f || nextRot.z != 0.0f) {
				m_pObject->SetRotForce(Vector3f(0.0f, 0.0f, 0.0f));
				m_pObject->SetOrientation(nextRot.x, nextRot.z); // Final adjustment to compensate FP precisions loss
			}

			// Reset prev direction for next comparison
			m_prevRotationDirection = 0;

			if (m_subState == FINAL_ROTATING) {
				SetState(ARRIVED);
				m_subState = NA;
			} else {
				m_subState = TRAVELING;
			}
		} else {
			// turning to desired orientation
			m_pObject->SetPosForce(Vector3f(0.0f, 0.0f, 0.0f));
			m_pObject->SetRotForce(Vector3f(0.0f, rotDir * 5.0f, 0.0f));
			m_prevRotationDirection = rotDir;
		}

		return GetState();
	}

	// check if arrived
	Vector3f currPos;
	m_pObject->GetPosition(currPos);

	Vector3f vec1(m_destPos.x - m_initPos.x, m_bPhysicsEnabled ? 0.0f : (m_destPos.y - m_initPos.y), m_destPos.z - m_initPos.z);
	Vector3f vec2(currPos.x - m_initPos.x, m_bPhysicsEnabled ? 0.0f : (currPos.y - m_initPos.y), currPos.z - m_initPos.z);
	Vector3f vec3 = vec1 - vec2;

	if (vec3.LengthSquared() <= MIN_DISTANCE_SQ) {
		// arrived at destination, wait for next update to check orientation
		m_pObject->SetPosForce(Vector3f(0.0f, 0.0f, 0.0f));
		m_pObject->SetPosition(Vector3f(m_destPos.x, m_bPhysicsEnabled ? currPos.y : m_destPos.y, m_destPos.z));
		m_subState = FINAL_ROTATING;
		return GetState();
	}

	// check if object is still on planned route (not deviated by collision, external force, etc)
	Vector3f vecX = Cross(vec1, vec2);
	float areaSq = vecX.LengthSquared();
	float deviSq = areaSq * 4.0f / vec1.LengthSquared();

	if (deviSq > MIN_DISTANCE_SQ) { // away from planned
		if (m_recalcInterval >= 0) {
			if ((long)elapsed > m_recalcInterval) { // warning: unsigned long -> long
				// Recalc if allowed
				Start(m_pObject, m_destPos, m_destRot, m_posForceY, m_recalcInterval, m_motionStyle, m_bPhysicsEnabled, m_timeoutInMS == 0 ? 0 : (m_timeoutInMS - elapsed), m_bPlayerControl);

				// NOTE: m_startingTickCount/ticksElapsed will be reset after Start(),
				// so there is no need to check when last recalculation happens.
				return GetState();
			}
		} else {
			// unable to recover, set routing to failed
			SetState(CANCELED);
			SetCancelReason(ROUTING_FAILED);
			return CANCELED;
		}
	} else {
		// check if already go beyond the destination
		if (vec2.LengthSquared() > vec1.LengthSquared()) {
			// Move to destination and end routing instantly
			m_pObject->SetPosForce(Vector3f(0.0f, 0.0f, 0.0f));
			m_pObject->SetPosition(Vector3f(m_destPos.x, m_bPhysicsEnabled ? currPos.y : m_destPos.y, m_destPos.z));
			SetState(ARRIVED);
			return ARRIVED;
		}
	}

	// keep going by maintaining current facing and velocity
	m_pObject->SetMotionStyle(m_motionStyle);
	m_pObject->SetPosForce(Vector3f(0.0f, 0.0f, m_posForceY));
	return GetState();
}

// Return true if canceled, false if failed -- already canceled/arrived or not started yet.
bool SimplePathFinder::Cancel(int reason) {
	if (GetState() == ENROUTE) {
		SetState(CANCELED);
		SetCancelReason(reason);
		return true;
	}

	return false;
}

void SimplePathFinder::Reset() {
	m_pObject = NULL;
	m_destPos.MakeZero();
	m_dir.MakeZero();
	m_posForceY = 0.0f;
	m_bPhysicsEnabled = false;
	m_timer.Pause().Reset();
	m_timeoutInMS = 0;
	m_updateCount = 0;
	SetState(IDLE);
	SetCancelReason(NONE);
}

void KEMovementObject::GetPosition(Vector3f& pos) {
	auto pMO = m_pEngine->GetMovementObjByNetId(m_netTraceId);
	if (!pMO || !pMO->getBaseFrame())
		return;
	pos = pMO->getBaseFramePosition();
}

void KEMovementObject::SetPosition(const Vector3f& pos) {
	auto pMO = m_pEngine->GetMovementObjByNetId(m_netTraceId);
	if (!pMO || !pMO->getBaseFrame())
		return;

	// Set player position
	// Set physics last position to avoid introducing new velocity (need to double check PhysicsBladeImpl::MovPhysicsSystem to see if this is necessary)
	pMO->setBaseFramePosition(pos, false);
	pMO->saveLastPositionFromBaseFrame();
}

void KEMovementObject::GetOrientation(float& dirX, float& dirZ) {
	dirX = dirZ = 0.0f;

	auto pMO = m_pEngine->GetMovementObjByNetId(m_netTraceId);
	if (!pMO || !pMO->getBaseFrame())
		return;

	Vector3f dir, up;
	pMO->getBaseFrame()->GetOrientation(dir, up);
	dirX = dir.x;
	dirZ = dir.z;
}

void KEMovementObject::SetOrientation(float dirX, float dirZ) {
	auto pMO = m_pEngine->GetMovementObjByNetId(m_netTraceId);
	if (!pMO || !pMO->getBaseFrame())
		return;

	Vector3f dir, up;
	pMO->getBaseFrame()->GetOrientation(dir, up);
	dir.x = dirX;
	dir.y = 0.0f;
	dir.z = dirZ;

	pMO->setBaseFrameOrientationCascade(dir, up);
}

void KEMovementObject::GetPosForce(Vector3f& posForce) {
	posForce = Vector3f(0.0, 0.0, 0.0);
	auto pMO = m_pEngine->GetMovementObjByNetId(m_netTraceId);
	if (!pMO)
		return;
	auto pMC = pMO->getMovementCaps();
	if (!pMC)
		return;
	posForce = pMC->GetPosForce();
}

void KEMovementObject::SetPosForce(const Vector3f& posForce) {
	auto pMO = m_pEngine->GetMovementObjByNetId(m_netTraceId);
	if (!pMO)
		return;
	auto pMC = pMO->getMovementCaps();
	if (!pMC)
		return;
	pMC->SetPosForce(posForce);
	if (posForce.z != 0)
		pMC->SetMoving(true);
	if (posForce.x != 0)
		pMC->SetSideStepping(true);
}

void KEMovementObject::GetRotForce(Vector3f& rotForce) {
	rotForce = Vector3f(0.0, 0.0, 0.0);
	auto pMO = m_pEngine->GetMovementObjByNetId(m_netTraceId);
	if (!pMO)
		return;
	auto pMC = pMO->getMovementCaps();
	if (!pMC)
		return;
	rotForce = pMC->GetRotForce();
}

void KEMovementObject::SetRotForce(const Vector3f& rotForce) {
	auto pMO = m_pEngine->GetMovementObjByNetId(m_netTraceId);
	if (!pMO)
		return;
	auto pMC = pMO->getMovementCaps();
	if (!pMC)
		return;
	pMC->SetRotForce(rotForce);
	if (rotForce.y != 0)
		pMC->SetTurning(true);
}

void KEMovementObject::SetMotionStyle(int style) {
	auto pMO = m_pEngine->GetMovementObjByNetId(m_netTraceId);
	if (!pMO)
		return;
	auto pMC = pMO->getMovementCaps();
	if (!pMC)
		return;
	if (style == Walk)
		pMC->SetWalking(true);
	else if (style == Run)
		pMC->SetWalking(false);
	else if (style == Auto) {
		auto curTrans = pMC->GetPosForce();
		pMC->SetWalking(curTrans.z <= pMC->GetWalkMaxSpeed());
	}
}

} // namespace KEP
