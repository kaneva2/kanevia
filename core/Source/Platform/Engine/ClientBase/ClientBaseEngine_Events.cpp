///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ClientBaseEngine.h"
#include "CMultiplayerClass.h"
#include "IClientEngine.h"
#include "RenderEngine\ReD3DX9Constants.h"
#include "KEPPhysics\Universe.h"
#include "CPhysicsEmuClass.h"
#include "CWldObject.h"
#include "DynamicObj.h"
#include "SkeletonLabels.h"
#include "CArmedInventoryClass.h"
#include "RuntimeSkeleton.h"
#include "SkeletonConfiguration.h"
#include "CSkeletonObject.h"
#include "TextureProvider.h"
#include "ContentService.h"
#include "CoreItems.h"
#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

class EventSyncHandler : public IEventHandler {
public:
	EventSyncHandler(Dispatcher& disp) :
			m_dispatcher(disp) {}

	virtual EVENT_PROC_RC ProcessEvent(IN IDispatcher* disp, IN IEvent* e) {
		auto pEvent = dynamic_cast<EventSyncEvent*>(e);
		if (!pEvent)
			return EVENT_PROC_RC::NOT_PROCESSED;

		EventRegistry remoteRegistry;
		ULONG characterCount;
		pEvent->ExtractRegistry(remoteRegistry, &characterCount);

		LogInfo("LOGON OK - remoteRegistry.size()=" << remoteRegistry.size() << ", characterCount=" << characterCount);
		m_dispatcher.ReconcileRemoteRegistry(remoteRegistry);

		// successful logon indicated, raise event indicating that
		// changed to sync event since race with scripting if queued
		auto pEvent_lr = new LogonResponseEvent();
		pEvent_lr->SetErrorCodes("Logon ok", S_OK, characterCount);
		m_dispatcher.ProcessSynchronousEvent(pEvent_lr);
		return EVENT_PROC_RC::CONSUMED; // MUST return this since dispatcher could crash otherwise
	}

private:
	Dispatcher& m_dispatcher;
};

bool ClientBaseEngine::InitEvents() {
	LogInfo("START");

	m_dispatcher.SetSendHandler(new OldNetworkSender(m_multiplayerObj));

	RegisterEvent<MovementObjectCfgEvent>(m_dispatcher);
	RegisterEvent<UpdateEquippableAnimationEvent>(m_dispatcher);
	RegisterEvent<ClientExitEvent>(m_dispatcher);
	RegisterEvent<DisconnectedEvent>(m_dispatcher);
	RegisterEvent<ClientSpawnEvent>(m_dispatcher);
	RegisterEvent<SystemServiceEvent>(m_dispatcher);

	m_attribEventId = m_dispatcher.RegisterEvent("AttribEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_outOfAmmoEventId = m_dispatcher.RegisterEvent("OutOfAmmoEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_dynObjectTextureEventId = m_dispatcher.RegisterEvent("DynamicObjTextureDownloadEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_friendObjectTextureEventId = m_dispatcher.RegisterEvent("FriendTextureDownloadEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_worldObjectTextureEventId = m_dispatcher.RegisterEvent("WorldObjTextureDownloadEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_zoneReloadedEventId = m_dispatcher.RegisterEvent("ZoneReloadedEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
#if DEPRECATED
	m_itemsReadyToArmEventId = m_dispatcher.RegisterEvent("ItemsReadyToArmEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
#endif
	m_iconMetadataReadyEventId = m_dispatcher.RegisterEvent("IconMetadataReadyEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_childGameWebCallsReadyEventId = m_dispatcher.RegisterEvent("ChildGameWebCallsReadyEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_entityCfgCompleteEventId = m_dispatcher.RegisterEvent("EntityCfgCompleteEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_volumeChangedEventId = m_dispatcher.RegisterEvent("VolumeChangedEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_dynamicObjectMovedEventId = m_dispatcher.RegisterEvent("DynamicObjectMovedEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);

	RegisterWorldObjectEvents(m_dispatcher);
	RegisterZonePermissionEvents(m_dispatcher);
	RegisterTargetingEvents(m_dispatcher);
	RegisterDynamicObjectEvents(m_dispatcher);

	// Initialize All Script Blades (class KEP::LuaEventHandler)
	if (!InitScriptBlades()) {
		LogError("InitScriptBlades() FAILED");
		return false;
	}

	auto syncHandler = std::make_shared<EventSyncHandler>(m_dispatcher);
	m_dispatcher.RegisterHandler(syncHandler, EventSyncEvent::ClassVersion(), EventSyncEvent::ClassId());
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleLogonResponseEventStatic, (ULONG)this, "LogonResponseEvent", LogonResponseEvent::ClassVersion(), LogonResponseEvent::ClassId());
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleStringListEventStatic, (ULONG)this, "StringListEvent", StringListEvent::ClassVersion(), StringListEvent::ClassId());
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleSetZonePermissionEventStatic, (ULONG)this, "SetZonePermissionEvent", SetZonePermissionEvent::ClassVersion(), SetZonePermissionEvent::ClassId(), EP_HIGH);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleRenderTextEvent, (ULONG)this, "RenderTextEvent", RenderTextEvent::ClassVersion(), RenderTextEvent::ClassId(), EP_LOW);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleRenderLocaleTextEvent, (ULONG)this, "RenderLocaleTextEvent", RenderLocaleTextEvent::ClassVersion(), RenderLocaleTextEvent::ClassId(), EP_LOW);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleMovementObjectCfgEvent, (ULONG)this, "MovementObjectCfgEvent", MovementObjectCfgEvent::ClassVersion(), MovementObjectCfgEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleAddDynamicObjectEvent, (ULONG)this, "AddDynamicObjectEvent", AddDynamicObjectEvent::ClassVersion(), m_dispatcher.GetEventId("AddDynamicObjectEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleMoveDynamicObjectEvent, (ULONG)this, "MoveDynamicObjectEvent", MoveDynamicObjectEvent::ClassVersion(), m_dispatcher.GetEventId("MoveDynamicObjectEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleMoveAndRotateDynamicObjectEvent, (ULONG)this, "MoveAndRotateDynamicObjectEvent", MoveAndRotateDynamicObjectEvent::ClassVersion(), m_dispatcher.GetEventId("MoveAndRotateDynamicObjectEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleScaleDynamicObjectEvent, (ULONG)this, "ScaleDynamicObjectEvent", ScaleDynamicObjectEvent::ClassVersion(), m_dispatcher.GetEventId("ScaleDynamicObjectEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleRotateDynamicObjectEvent, (ULONG)this, "RotateDynamicObjectEvent", RotateDynamicObjectEvent::ClassVersion(), m_dispatcher.GetEventId("RotateDynamicObjectEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleRemoveDynamicObjectEvent, (ULONG)this, "RemoveDynamicObjectEvent", RemoveDynamicObjectEvent::ClassVersion(), m_dispatcher.GetEventId("RemoveDynamicObjectEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleDynamicObjectInitializedEvent, (ULONG)this, "DynamicObjectInitializedEvent", DynamicObjectInitializedEvent::ClassVersion(), m_dispatcher.GetEventId("DynamicObjectInitializedEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleUpdateDynamicObjectTextureEvent, (ULONG)this, "UpdateDynamicObjectTextureEvent", UpdateDynamicObjectTextureEvent::ClassVersion(), m_dispatcher.GetEventId("UpdateDynamicObjectTextureEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleUpdatePictureFrameFriendEvent, (ULONG)this, "UpdatePictureFrameFriendEvent", UpdatePictureFrameFriendEvent::ClassVersion(), m_dispatcher.GetEventId("UpdatePictureFrameFriendEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleUpdateWorldObjectTextureEvent, (ULONG)this, "UpdateWorldObjectTextureEvent", UpdateWorldObjectTextureEvent::ClassVersion(), m_dispatcher.GetEventId("UpdateWorldObjectTextureEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleControlDynamicObjectEvent, (ULONG)this, "ControlDynamicObjectEvent", ControlDynamicObjectEvent::ClassVersion(), m_dispatcher.GetEventId("ControlDynamicObjectEvent"), EP_NORMAL);
	m_dispatcher.RegisterFilteredHandler(ClientBaseEngine::HandleControlObjectEventSetFriction, (ULONG)this, "ObjectControlEventSetFriction", ControlDynamicObjectEvent::ClassVersion(), m_dispatcher.GetEventId("ControlDynamicObjectEvent"), FRICTION_ENABLE);
	m_dispatcher.RegisterFilteredHandler(ClientBaseEngine::HandleControlObjectEventSetFriction, (ULONG)this, "ObjectControlEventSetFriction", ControlDynamicObjectEvent::ClassVersion(), m_dispatcher.GetEventId("ControlDynamicObjectEvent"), FRICTION_DISABLE);
	m_dispatcher.RegisterFilteredHandler(ClientBaseEngine::HandleControlObjectEventShowHide, (ULONG)this, "ControlObjectEventShowHideHandler", ControlDynamicObjectEvent::ClassVersion(), m_dispatcher.GetEventId("ControlDynamicObjectEvent"), OBJECT_SHOW);
	m_dispatcher.RegisterFilteredHandler(ClientBaseEngine::HandleControlObjectEventShowHide, (ULONG)this, "ControlObjectEventShowHideHandler", ControlDynamicObjectEvent::ClassVersion(), m_dispatcher.GetEventId("ControlDynamicObjectEvent"), OBJECT_HIDE);
	EVENT_ID redirectConsoleIOEventId = m_dispatcher.RegisterEvent("RedirectConsoleIOEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_dispatcher.RegisterHandler(ClientBaseEngine::HandleRedirectConsoleIOEvent, (ULONG)this, "RedirectConsoleIOEvent", PackVersion(1, 0, 0, 0), redirectConsoleIOEventId, EP_NORMAL);

	LogInfo("END");

	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Event Senders
///////////////////////////////////////////////////////////////////////////////

bool ClientBaseEngine::SendArmItemEvent(const GLID& glid, eArmItemAction action, eArmItemResult result) {
	auto pEvent = m_dispatcher.MakeEvent(m_attribEventId);
	if (!pEvent)
		return false;
	pEvent->SetFilter((ULONG)eAttribEventType::ArmItem);
	pEvent->SetObjectId((OBJECT_ID)result);
	(*pEvent->OutBuffer()) << (int)action << (int)glid;
	m_dispatcher.QueueEvent(pEvent);
	return true;
}

bool ClientBaseEngine::SendEntityCfgCompleteEvent() {
	auto pEvent = m_dispatcher.MakeEvent(m_entityCfgCompleteEventId);
	if (!pEvent)
		return false;
	(*pEvent->OutBuffer()) << (int)GetNumHumanPlayersInRuntimeList();
	m_dispatcher.QueueEvent(pEvent);
	return true;
}

bool ClientBaseEngine::SendSystemServiceEvent(const std::string& userId) {
	if (!ClientIsEnabled())
		return false;

	// Send System Service Event To Server For Forwarding To User's Client
	LogWarn("OK - userId=" << userId);
	IEvent* pEvent = new SystemServiceEvent(userId);
	if (!pEvent)
		return false;
	pEvent->AddTo(SERVER_NETID);
	m_dispatcher.QueueEvent(pEvent);
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Event Handlers
///////////////////////////////////////////////////////////////////////////////

EVENT_PROC_RC ClientBaseEngine::HandleAddDynamicObjectEvent(ULONG lparam, IDispatcher* disp, IEvent* pAddDynamicObjectEvent) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = (AddDynamicObjectEvent*)pAddDynamicObjectEvent;
	if (!me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	// Extract Event Params
	PlacementSource source;
	int playerId, placementId, assetId, friendId, globalId, inventory_sub_type, gameItemId;
	GLID animGlidSpecial; // < 0 = reverse animation
	std::string mediaUrl, mediaParams, textureUrl;
	Vector3f position, direction, slide;
	bool isAttachable, isInteractive, mediaSupported, hasCollision;
	Vector3f boundBox_min, boundBox_max;
	bool derivable;
	pEvent->ExtractInfo(
		source,
		playerId,
		globalId,
		position.x, position.y, position.z,
		direction.x, direction.y, direction.z,
		slide.x, slide.y, slide.z,
		placementId,
		animGlidSpecial,
		assetId,
		textureUrl,
		friendId,
		mediaUrl,
		mediaParams,
		isAttachable,
		isInteractive,
		mediaSupported,
		hasCollision,
		boundBox_min.x, boundBox_min.y, boundBox_min.z,
		boundBox_max.x, boundBox_max.y, boundBox_max.z,
		derivable, inventory_sub_type, gameItemId);

	Vector3f min, max;
	min.x = boundBox_min.x;
	min.y = boundBox_min.y;
	min.z = boundBox_min.z;
	max.x = boundBox_max.x;
	max.y = boundBox_max.y;
	max.z = boundBox_max.z;

	// Place Object
	PlaceDynamicObjectArgs po_args;
	po_args.position = position;
	po_args.direction = direction;
	po_args.slide = slide;
	po_args.playerId = playerId;
	po_args.placementId = placementId;
	po_args.globalId = globalId;
	po_args.min = min;
	po_args.max = max;
	po_args.attachable = isAttachable;
	po_args.interactive = isInteractive;
	po_args.mediaSupported = mediaSupported;
	po_args.isDerivable = derivable;
	po_args.scaleFactor = 1.0f;
	po_args.assetId = assetId;
	po_args.invSubType = inventory_sub_type;
	po_args.textureUrl = textureUrl;
	po_args.gameItemId = gameItemId;

	if (globalId == 0 || globalId == GLID_PAPERDOLL_NPC) {
		// Place actor-based dynamic object
		auto pPlacement = me->PlaceActorAsDynamicObject(po_args);
		assert(pPlacement != nullptr);
		pPlacement;
		return EVENT_PROC_RC::OK;
	}

	CDynamicPlacementObj* pDPO = me->PlaceDynamicObject(po_args);
	if (!pDPO) {
		LogError("PlaceObject() FAILED");
		return EVENT_PROC_RC::OK;
	}

	pDPO->FlushBoundBox();

#ifdef _ClientOutput
	bool mediaValid = (mediaSupported && !mediaUrl.empty());
	if (assetId > 0 && !mediaValid) {
		// Set Object Custom Texture
		IEvent* e = me->m_dispatcher.MakeEvent(me->m_dynObjectTextureEventId);
		if (!e) {
			LogError("FAILED dispatcher.MakeEvent(dynObjectTextureEvent)");
		} else {
			//Ankit ----- set the customTextureBeingDownloaded to true
			pDPO->setWaitForCustomTextAndApply(true);

			e->SetObjectId((OBJECT_ID)placementId);
			(*e->OutBuffer()) << false; // do not overwrite any existing texture changes
			if (!me->GetExternalTextureAsync(CUSTOM_TEXTURE, assetId, textureUrl.c_str(), e)) {
				LogWarn("GetDynamicTextureAsync(CUSTOM) FAILED - '" << textureUrl << "'");
				pDPO->setWaitForCustomTextAndApply(false);
			}
		}
	} else if (friendId > 0 && !mediaValid) {
		// Set Friend Custom Texture
		IEvent* e = me->m_dispatcher.MakeEvent(me->m_friendObjectTextureEventId);
		if (!e) {
			LogError("FAILED dispatcher.MakeEvent(friendObjectTextureEvent)");
		} else {
			e->SetObjectId((OBJECT_ID)placementId);
			(*e->OutBuffer()) << false << friendId; // do not overwrite any existing texture changes
			if (!me->GetExternalTextureAsync(FRIEND_PICTURE, friendId, textureUrl.c_str(), e)) {
				LogWarn("GetDynamicTextureAsync(FRIEND_PICTURE) FAILED - '" << textureUrl << "'");
			}
		}

	} else if (mediaValid) {
		// Set Dynamic Object Media Params (don't change volume, radius or playlist)
		pDPO->SetMediaParams(MediaParams(mediaUrl, mediaParams, -1, -1, -1, -1));
	}

	// If we have an animation, try to start playing it now.
	if (IS_VALID_GLID(animGlidSpecial)) {
		AnimSettings animSettings;
		animSettings.setGlidSpecial(animGlidSpecial);
		pDPO->setPrimaryAnimationSettings(animSettings);
	}
#endif

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientBaseEngine::HandleUpdateWorldObjectTextureEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = (UpdateWorldObjectTextureEvent*)e;
	if (!me || !pEvent)
		return EVENT_PROC_RC::CONSUMED;

	// DRF - Crash Fix
	MutexGuardLock lock(me->m_WldObjListMutex);
	if (!me->m_pWOL)
		return EVENT_PROC_RC::OK;

	std::string objSerialNumber, textureUrl;
	int assetId;
	pEvent->ExtractInfo(objSerialNumber, assetId, textureUrl);

	for (POSITION pos = me->m_pWOL->GetHeadPosition(); pos;) {
		auto pWO = (CWldObject*)me->m_pWOL->GetNext(pos);

		//todo probably better if we cache this string?
		std::string uniqueIdStr;
		unsigned char* s = 0;
		VERIFY(UuidToStringA(&(pWO->m_uniqueId), &s) == S_OK);
		uniqueIdStr = (const char*)s;
		RpcStringFreeA(&s);

		if (objSerialNumber.compare(uniqueIdStr) == 0) {
			if (assetId <= 0) {
				pWO->ResetCustomMaterial();
			} else {
				pWO->m_assetId = assetId;

				// DRF - Added Null Check
				IEvent* pWorldObjTextureDownloadEvent = me->m_dispatcher.MakeEvent(me->m_worldObjectTextureEventId);
				if (!pWorldObjTextureDownloadEvent) {
					LogError("FAILED dispatcher.MakeEvent(worldObjectTextureEvent)");
				} else {
					(*pWorldObjTextureDownloadEvent->OutBuffer()) << false << uniqueIdStr << textureUrl;
					if (!me->GetExternalTextureAsync(CUSTOM_TEXTURE, assetId, textureUrl.c_str(), pWorldObjTextureDownloadEvent)) {
						LogWarn("Failed to start download of world obj texture " << textureUrl);
					}
				}
			} // if (assetId <= 0)
		} // if (objSerialNumber.compare)
	} // for (posLoc)

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientBaseEngine::HandleRotateDynamicObjectEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = (RotateDynamicObjectEvent*)e;
	if (!me || !pEvent)
		return EVENT_PROC_RC::CONSUMED;

	// Extract Event Info
	int placementId;
	double dx, dy, dz, sx, sy, sz;
	DWORD timeMs;
	pEvent->ExtractInfo(placementId, dx, dy, dz, sx, sy, sz, timeMs);

	// Get Dynamic Object By Id
	auto pDPO = me->GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return EVENT_PROC_RC::OK;
	}

	// position doesn't change
	Vector3f direction, slide;
	direction.x = dx;
	direction.y = dy;
	direction.z = dz;
	slide.x = sx;
	slide.y = sy;
	slide.z = sz;
	pDPO->Relocate(direction, slide, timeMs);

	auto pSkeleton = pDPO->getSkeleton();
	if (pSkeleton) {
		// rebuild the light cache
		pSkeleton->flushLightCache();
		pSkeleton->invalidateShaders();

		// invalidate the light caches of anything in range of the light's new position
		if (pDPO->GetLight())
			me->InvalidateLightCaches(&pDPO->m_position, pDPO->GetLight()->m_range);
	}

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientBaseEngine::HandleMoveDynamicObjectEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = (MoveDynamicObjectEvent*)e;
	if (!me || !pEvent)
		return EVENT_PROC_RC::CONSUMED;

	// Extract Event Info
	int placementId;
	double x, y, z;
	DWORD timeMs, accDur, constVelDur;
	double accDistFrac, constVelDistFrac;
	pEvent->ExtractInfo(placementId, x, y, z, timeMs, accDur, constVelDur, accDistFrac, constVelDistFrac);

	// Get Dynamic Object By Id
	auto pDPO = me->GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return EVENT_PROC_RC::OK;
	}

	Vector3f position;
	position.x = x;
	position.y = y;
	position.z = z;
	pDPO->Relocate(position, timeMs, accDur, constVelDur, accDistFrac, constVelDistFrac);

	auto pSkeleton = pDPO->getSkeleton();
	if (pSkeleton) {
		// rebuild the light cache
		pSkeleton->flushLightCache();
		pSkeleton->invalidateShaders();

		// invalidate the light caches of anything in range of the light's new position
		if (pDPO->GetLight())
			me->InvalidateLightCaches(&pDPO->m_position, pDPO->GetLight()->m_range);
	}

	// update origin on triggers if instant move
	if (timeMs == 0)
		me->MoveDynamicObjectTriggers(placementId, position, pDPO->m_direction);

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientBaseEngine::HandleMoveAndRotateDynamicObjectEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = (MoveAndRotateDynamicObjectEvent*)e;
	if (!me || !pEvent)
		return EVENT_PROC_RC::CONSUMED;

	// Extract Event Info
	int placementId;
	double x, y, z, dx, dy, dz, sx, sy, sz;
	DWORD timeMs;
	pEvent->ExtractInfo(placementId, x, y, z, dx, dy, dz, sx, sy, sz, timeMs);

	// Get Dynamic Object By Id
	auto pDPO = me->GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return EVENT_PROC_RC::OK;
	}

	auto pMO = me->GetMovementObjMe();
	if (pMO) {
		if (pMO->getPhysicsVehicle()) {
			CDynamicPlacementObj* pDynamicPlacementObj = me->GetDynamicObject(pMO->getVehiclePlacementId());
			if (pDPO == pDynamicPlacementObj) {
				return EVENT_PROC_RC::CONSUMED;
			}
		}
	}

	Vector3f position;
	Vector3f direction;
	Vector3f slide;
	position.x = x;
	position.y = y;
	position.z = z;
	direction.x = dx;
	direction.y = dy;
	direction.z = dz;
	slide.x = sx;
	slide.y = sy;
	slide.z = sz;
	pDPO->Relocate(position, timeMs, direction, slide, timeMs);

	auto pSkeleton = pDPO->getSkeleton();
	if (pSkeleton) {
		// rebuild the light cache
		pSkeleton->flushLightCache();
		pSkeleton->invalidateShaders();

		// invalidate the light caches of anything in range of the light's new position
		if (pDPO->GetLight())
			me->InvalidateLightCaches(&pDPO->m_position, pDPO->GetLight()->m_range);
	}

	// update origin on triggers if instant move
	if (timeMs == 0)
		me->MoveDynamicObjectTriggers(placementId, position, direction);

	IEvent* moveEvent = me->m_dispatcher.MakeEvent(me->m_dynamicObjectMovedEventId);
	if (moveEvent) {
		*moveEvent->OutBuffer() << placementId << position.x << position.y << position.z;
		me->m_dispatcher.ProcessSynchronousEvent(moveEvent);
	}

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientBaseEngine::HandleScaleDynamicObjectEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = (ScaleDynamicObjectEvent*)e;
	if (!me || !pEvent)
		return EVENT_PROC_RC::CONSUMED;

	// Extract Event Info
	int placementId;
	double x, y, z;
	pEvent->ExtractInfo(placementId, x, y, z);

	// Get Dynamic Object By Id
	auto pDPO = me->GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return EVENT_PROC_RC::OK;
	}

	pDPO->SetScale(x, y, z);

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientBaseEngine::HandleControlDynamicObjectEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = (ControlDynamicObjectEvent*)e;
	if (!me || !pEvent)
		return EVENT_PROC_RC::CONSUMED;

	std::vector<int> ctlArgs;
	int objId;
	ULONG ctlId;
	pEvent->ExtractInfo(objId, ctlId, ctlArgs);
	auto cdoef = (CONTROL_DYNAMIC_OBJECT_EVENT_FILTER)ctlId;

	// If this switch becomes non-trivial, use filtered handlers instead, since I'd have
	// to call different functions anyway.
	switch (cdoef) {
		case ANIM_START: {
			auto pICE = IClientEngine::Instance();
			auto numArgs = ctlArgs.size();
			if (pICE && numArgs) {
				int animGlidSpecial = ctlArgs[0]; // <0 = reverse animation
				eAnimLoopCtl animLoopCtl = eAnimLoopCtl::Default;
				if (numArgs > 1)
					animLoopCtl = (eAnimLoopCtl)ctlArgs[1];
				AnimSettings animSettings;
				animSettings.setGlidSpecial(animGlidSpecial);
				animSettings.setLoopCtl(animLoopCtl);
				pICE->SetAnimationOnDynamicObject(objId, animSettings);
			}
		} break;
	}

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientBaseEngine::HandleUpdateDynamicObjectTextureEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = (UpdateDynamicObjectTextureEvent*)e;
	if (!me || !pEvent)
		return EVENT_PROC_RC::CONSUMED;

	int placementId, assetId, playerId;
	std::string textureUrl;
	pEvent->ExtractInfo(placementId, assetId, textureUrl, playerId);

	auto pDPO = me->GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - objId=" << placementId);
		return EVENT_PROC_RC::CONSUMED;
	}

	if (assetId > 0) {
		// DRF - Added Null Check
		IEvent* pDynObjTextureDownloadEvent = me->m_dispatcher.MakeEvent(me->m_dynObjectTextureEventId);
		if (!pDynObjTextureDownloadEvent) {
			LogError("FAILED dispatcher.MakeEvent(dynObjectTextureEvent)");
		} else {
			pDynObjTextureDownloadEvent->SetObjectId((OBJECT_ID)placementId);
			(*pDynObjTextureDownloadEvent->OutBuffer()) << true; // do overwrite any existing texture changes to the object
			if (!me->GetExternalTextureAsync(CUSTOM_TEXTURE, assetId, textureUrl.c_str(), pDynObjTextureDownloadEvent)) {
				LogWarn("Failed to start download of texture " << textureUrl);
			}
		}
	} else {
		pDPO->ResetCustomTexture();
		pDPO->UpdateCustomMaterials();
	}

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientBaseEngine::HandleUpdatePictureFrameFriendEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = (UpdatePictureFrameFriendEvent*)e;
	if (!me || !pEvent)
		return EVENT_PROC_RC::CONSUMED;

	int placementId, friendId;
	std::string textureUrl;
	pEvent->ExtractInfo(placementId, friendId, textureUrl);

	auto pDPO = me->GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - objId=" << placementId);
		return EVENT_PROC_RC::CONSUMED;
	}

	//set custom texture
	if (friendId > 0) {
		// DRF - Added Null Check
		IEvent* pFriendTextureDownloadEvent = me->m_dispatcher.MakeEvent(me->m_friendObjectTextureEventId);
		if (!pFriendTextureDownloadEvent) {
			LogError("FAILED dispatcher.MakeEvent(friendObjectTextureEvent)");
		} else {
			pFriendTextureDownloadEvent->SetObjectId((OBJECT_ID)placementId);
			(*pFriendTextureDownloadEvent->OutBuffer()) << friendId;

			if (!me->GetExternalTextureAsync(FRIEND_PICTURE, friendId, textureUrl.c_str(), pFriendTextureDownloadEvent)) {
				LogWarn("Failed to start download of friend texture " << textureUrl);
			}
		}
	} else {
		pDPO->ResetCustomTexture();
		pDPO->UpdateCustomMaterials();
	}

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientBaseEngine::HandleRemoveDynamicObjectEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = (RemoveDynamicObjectEvent*)e;
	if (!me || !pEvent)
		return EVENT_PROC_RC::CONSUMED;

	int placementId;
	pEvent->ExtractInfo(placementId);

	if (me->IsLoadingScene()) {
		// Ignore server DO removal event if zone loading in progress
		return EVENT_PROC_RC::CONSUMED;
	}

	me->RemoveDynamicObjectTriggers(placementId);

	// Get Object Being Deleted By Placement Id
	auto pDPO = me->GetDynamicObject(placementId);
	if (pDPO) {
		// Invalidate Light Cache States For Light Objects Being Deleted
		if (pDPO->GetLight())
			me->InvalidateLightCaches(&pDPO->m_position, pDPO->GetLight()->m_range);

		// Stop Media Players (also deletes media mesh)
		if (pDPO->MediaSupported())
			me->StopMediaOnDynamicObject(placementId);

		// Remove from physics system.
		pDPO->ForEachRigidBody([me](Physics::RigidBodyStatic* pRigidBody) { me->m_pUniverse->Remove(pRigidBody); });
	}

	// Remove Object From World
	me->RemoveDynamicObject(placementId);

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientBaseEngine::HandleControlObjectEventSetFriction(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = (ControlDynamicObjectEvent*)e;
	if (!me || !pEvent)
		return EVENT_PROC_RC::CONSUMED;

	int placementId;
	ULONG ctlId;
	std::vector<int> ctlArgs;
	pEvent->ExtractInfo(placementId, ctlId, ctlArgs);
	auto cdoef = (CONTROL_DYNAMIC_OBJECT_EVENT_FILTER)ctlId;

	auto pDPO = me->GetDynamicObject(placementId);
	if (pDPO)
		pDPO->m_frictionEnabled = (cdoef == FRICTION_ENABLE);

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientBaseEngine::HandleControlObjectEventShowHide(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = (ControlDynamicObjectEvent*)e;
	if (!me || !pEvent)
		return EVENT_PROC_RC::CONSUMED;

	int placementId;
	ULONG ctlId;
	std::vector<int> ctlArgs;
	pEvent->ExtractInfo(placementId, ctlId, ctlArgs);
	auto cdoef = (CONTROL_DYNAMIC_OBJECT_EVENT_FILTER)ctlId;

	auto pDPO = me->GetDynamicObject(placementId);
	if (pDPO)
		pDPO->SetVisibility(cdoef == OBJECT_SHOW ? eVisibility::VISIBLE : eVisibility::HIDDEN_FOR_ALL);

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientBaseEngine::HandleDynamicObjectInitializedEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	if (!me)
		return EVENT_PROC_RC::CONSUMED;

	DynamicObjectInitializedEvent* pInitializedEvent = static_cast<DynamicObjectInitializedEvent*>(e);

	int placementId;
	pInitializedEvent->ExtractInfo(placementId);

	CDynamicPlacementObj* pDPO = me->GetDynamicObject(placementId);
	if (pDPO) {
		pDPO->ForEachRigidBody([me](Physics::RigidBodyStatic* pRigidBody) {
			me->m_pUniverse->Add(pRigidBody);
		});
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientBaseEngine::HandleRedirectConsoleIOEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	if (!me)
		return EVENT_PROC_RC::CONSUMED;

	std::string strStdIn, strStdOut, strStdErr;
	*e->InBuffer() >> strStdIn >> strStdOut >> strStdErr;
	me->SetConsoleIOHandles(strStdIn, strStdOut, strStdErr);
	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientBaseEngine::HandleMovementObjectCfgEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = dynamic_cast<MovementObjectCfgEvent*>(e);
	if (!me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	EVENT_PROC_RC ret = me->HandleMovementObjectCfgEvent(pEvent);
	if (ret == EVENT_PROC_RC::CONSUMED)
		me->SendEntityCfgCompleteEvent();

	return ret;
}

EVENT_PROC_RC ClientBaseEngine::HandleStringListEventStatic(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = dynamic_cast<StringListEvent*>(e);
	if (!me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;
	return me->HandleStringListEvent(pEvent);
}

EVENT_PROC_RC ClientBaseEngine::HandleLogonResponseEventStatic(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = dynamic_cast<LogonResponseEvent*>(e);
	if (!me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;
	return me->HandleLogonResponseEvent(pEvent->m_connectionErrorCode.c_str(), pEvent->m_hResultCode, pEvent->m_characterCount, pEvent->m_replyCode);
}

EVENT_PROC_RC ClientBaseEngine::HandleSetZonePermissionEventStatic(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = dynamic_cast<SetZonePermissionEvent*>(e);
	if (!me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;
	int zonePermissions;
	pEvent->ExtractInfo(zonePermissions);
	return me->HandleSetZonePermissionEvent(zonePermissions);
}

EVENT_PROC_RC ClientBaseEngine::HandleRenderTextEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	return EVENT_PROC_RC::NOT_PROCESSED;
}

EVENT_PROC_RC ClientBaseEngine::HandleRenderLocaleTextEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ClientBaseEngine*)lparam;
	auto pEvent = dynamic_cast<RenderLocaleTextEvent*>(e);
	if (!me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	std::string text;
	eChatType ct;
	pEvent->ExtractInfo(text, ct);
	me->m_dispatcher.QueueEvent(new RenderTextEvent(text.c_str(), ct));

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientBaseEngine::HandleMovementObjectCfgEvent(MovementObjectCfgEvent* ece) {
	LogInfo("START");

	// Extract Event Data
	int isItem;
	int netTraceId;
	short dbIndex;
	short aiConfig;
	int passGroups; // bitmapped PASS_GROUPS (1=AP, 2=VIP)
	unsigned short nameR;
	unsigned short nameG;
	unsigned short nameB;
	Vector3f scale;
	Vector3f pos;
	Vector3f dir;
	Vector3f up;
	GLID animGlid1;
	GLID animGlid2;
	std::vector<ItemAnimation> itemAnimations;
	ece->ExtractInfo(
		isItem,
		netTraceId,
		dbIndex,
		aiConfig,
		passGroups,
		nameR, nameG, nameB,
		scale,
		pos,
		dir,
		up,
		animGlid1,
		animGlid2,
		itemAnimations);

	// Extract Additional Event Data
	short clanMember;
	std::string name;
	std::string displayName;
	std::string lastName; // unused
	std::string clanName;
	std::string title;
	(*ece->InBuffer()) >> clanMember;
	(*ece->InBuffer()) >> name;
	(*ece->InBuffer()) >> displayName;
	(*ece->InBuffer()) >> lastName;
	(*ece->InBuffer()) >> clanName;
	(*ece->InBuffer()) >> title;

	// Get Character Configuration (head, feet, etc)
	CharConfig charCfg;
	if (!charCfg.extractFromEvent(ece))
		return EVENT_PROC_RC::NOT_PROCESSED;

	// Update Existing Movement Object Or Create New One ?
	if (!GetMovementObjByNetId(netTraceId)) {
		eActorControlType controlType;
		controlType = isItem ? eActorControlType::STATIC : eActorControlType::OPC;

		// Activate New Movement Object (only OPC - other player's avatars)
		CMovementObj* pMO_New = NULL;
		bool ok = CreateMovementObject(
			0,
			Matrix44f::GetIdentity(),
			dbIndex,
			controlType,
			netTraceId,
			true, // networkEntity
			false, // add at tail (index!=0)
			false, // don't spawn at server
			-1,
			&pMO_New,
			NULL,
			false,
			name);
		if (!ok || !pMO_New || !pMO_New->getBaseFrame()) {
			LogError("CreateMovementObject(TYPE_OPC - OTHER AVATAR) FAILED");
		} else {
			pMO_New->setValidThisRound(true);

			// Set Initial Position & Orientation
			pMO_New->setBaseFramePosDirUp(pos, dir, up);

			// Initialize Movement Interpolator
			pMO_New->MovementInterpolatorInit();

			// DRF - TODO - REMOVE - Redundant With UpdateNetworkEntity Below!
			if (pMO_New->getSkeleton()) {
				AnimSettings animSettings;
				animSettings.setGlidSpecial(animGlid1);
				pMO_New->getSkeleton()->setPrimaryAnimationSettings(animSettings);
			}
		}
	}

	// Get Entity
	auto pMO = GetMovementObjByNetId(netTraceId);
	if (!pMO) {
		LogError("GetMovementObjByNetId() FAILED - netId=" << netTraceId);
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	// Is It Me ?
	auto pMO_Me = GetMovementObjMe();
	bool isMe = (pMO_Me && pMO_Me->getTraceNumber() == pMO->getTraceNumber());

	// Validate Reference Model
	auto pMORM = m_movObjRefModelList->getObjByIndex(pMO->getIdxRefModel());
	if (!pMORM) {
		LogError("getObjByIndex() FAILED - idxRefModel=" << pMO->getIdxRefModel());
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	// DRF - Naked Bug Fix - Set State To Configured (can now be safely armed!)
	if (pMO->getState() != CMovementObj::STATE_UNCONFIGURED) {
		LogError("DRF_NAKED_BUG_FIX - ALREADY CONFIGURED - Ignoring ...");
		return EVENT_PROC_RC::CONSUMED;
	}
	pMO->setState(CMovementObj::STATE_CONFIGURED);
	pMO->setAllowArmDisarm(true);

	if (pMO->getSkeleton() != nullptr && pMO->getSkeleton()->getOverheadLabels() != nullptr)
		pMO->getSkeleton()->getOverheadLabels()->reset();

	// DRF - Get New Player's Web Configuration Data (passGroups, nameColor, etc.)
	int passGroupsUser = passGroups;
	int passGroupsWorld = PASS_GROUP_INVALID;
	Vector3d nameColor(nameR, nameG, nameB);
	if (WebGetPlayerCfg(name, nameColor, passGroupsUser, passGroupsWorld)) {
		pMO->setPassGroups(passGroupsUser);
		m_passGroupsWorld = passGroupsWorld;
		nameR = nameColor[0];
		nameG = nameColor[1];
		nameB = nameColor[2];
		LogInfo("'" << name << "'"
					<< " passGroupsUser=" << passGroupsUser
					<< " passGroupsWorld=" << passGroupsWorld
					<< " nameColor=[" << nameR << " " << nameG << " " << nameB << "]");
	}

	// Set name
	if (!name.empty()) {
		pMO->setName(name);
		pMO->setDisplayName(displayName);
		pMO->setClanName(clanName);
		pMO->setTitle(title);

		// comes down as a byte from server
		pMO->setNameColor(nameR / 255.0f, nameG / 255.0f, nameB / 255.0f);

		// Set Overhead Labels
		setOverheadLabels(pMO);

		// If Updating My Avatar
		if (isMe) {
			// pass Name as attrib message to scripting as an event
			IEvent* e = m_dispatcher.MakeEvent(m_attribEventId);
			if (!e) {
				LogError("FAILED dispatcher.MakeEvent() - eventId=" << m_attribEventId);
			} else {
				e->SetFilter((ULONG)eAttribEventType::PlayerInfo);
				(*e->OutBuffer()) << pMO->getName();
				m_dispatcher.QueueEvent(e);
			}

			if (pMO->getSkeleton())
				pMO->getSkeleton()->setLoadPriorityDelta(RES_PRIO_MY_DELTA);
		}
	}

	// USER CHARACTER CONFIG
	// Set up the character according to the user's selected body type, hair, etc
	if (pMO->getSkeletonConfig())
		pMO->getSkeletonConfig()->InitialDimConfig(charCfg);

	// EQUIPPABLE ITEM CONFIG - This includes faces
	for (size_t i = CC_ITEM_FIRST_EQUIP; i < charCfg.getItemCount(); i++)
		ConfigPlayerEquippableItem(pMO, charCfg.getItemByIndex(i));

	// Extract Number Of Glids
	size_t glids = 0;
	(*ece->InBuffer()) >> glids;

	// Extract Player Physics & Armed Glids
	std::vector<GLID> armedGlids;
	for (size_t i = 0; i < glids; i++) {
		// Extract Glid (glid < 0 is special server side physics max speed parameter)
		int glidSpecial = 0;
		(*ece->InBuffer()) >> glidSpecial;
		GLID glid = (GLID)abs(glidSpecial);

		// Special Server-Side Physics Parameters ? (glid of <0)
		bool serverPhysics = (glidSpecial < 0);
		if (serverPhysics) {
			auto pMC = pMO->getMovementCaps();
			if (pMC) {
				float maxSpeed = .001f * (float)glid;
				LogInfo(pMO->ToStr() << " SET PLAYER PHYSICS - maxSpeed=" << maxSpeed);
				pMC->SetMaxSpeed(maxSpeed);
				pMC->SetMinSpeed(-maxSpeed);
				pMC->SetAccel(maxSpeed);
				pMC->SetDecel(maxSpeed);
			}
			continue;
		}

		armedGlids.push_back(glid);
	}

	// Set Equippable Animations
	if (itemAnimations.size()) {
		LogInfo(pMO->ToStr() << " SET EQUIPPABLE ANIMATIONS - items=" << itemAnimations.size() << " ...");
		for (const auto& ia : itemAnimations)
			SetEquippableAnimation(pMO, ia.m_glidItem, ia.m_glidAnim, false);
	}

	// handle mount function
	if (aiConfig > -1) {
		LogWarn("DEAD CODE - aiConfig=" << aiConfig);
		GenAndMount(pMO, aiConfig);
	}

	// Update Avatar Scale
	float maxScale = std::max<float>({ scale.x, scale.y, scale.z });
	pMO->setBoundingRadius(pMO->getUnscaledBoundingRadius() * maxScale);
	if (pMO->getSkeleton())
		pMO->getSkeleton()->scaleSystem(scale.x, scale.y, scale.z);

	// DRF - Added
	// Update Other Avatar's Move Data (first movement update)
	if (!isMe) {
		MOVE_DATA moveData((short)netTraceId, pos, dir, up, animGlid1, animGlid2);
		pMO->setMoveDataToClient(moveData);
		UpdateEntityMoveData(moveData);
	}

	// ED-8331 - Arm All Items Async (realize now, arm defaults, sendMyEvent)
	ArmItemsAsync(armedGlids, pMO, true, true, true);

	LogInfo("END");

	return EVENT_PROC_RC::CONSUMED;
}

} // namespace KEP