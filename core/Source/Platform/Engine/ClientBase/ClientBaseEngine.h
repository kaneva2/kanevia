///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPCommon.h"

#include "Engine\Base\Engine.h"
#include "Engine\Base\EngineStrings.h"

#include "Event\ClientEvents.h"

#include "Core/Util/HighPrecisionTime.h"
#include "Common\KEPUtil\CrashReporter.h"
#include "Common\KEPUtil\RuntimeReporter.h"
#include "Common\KEPUtil\WebCall.h"

#include "IOldPhysics.h"
#include "InstanceId.h"
#include "PathFinding.h"
#include "../KEPPhysics/Declarations.h"
#include "CTriggerClass.h"
#include "StreamableDynamicObj.h"
#include "DownloadPriority.h"
#include "DynamicPlacementManager.h"
#include "CMovementObj.h"
#include <functional>
#include <mmsystem.h>

#include "Core\Math\Rotation.h"

#include "ContentService.h"

namespace KEP {

class CCollisionObj;
class DynamicObjectRM;
class EquippableRM;
class CCameraObj;
class CDynamicPlacementObj;
class CEXScriptObj;
class CGlobalInventoryObj;
class CMovementObj;
class CMovementObjList;
class CFrameObj;
class TextureDatabase;
class CharConfig;
class CharConfigItem;
class RuntimeSkeleton;
class CExplosionObjList;
class IMemSizeGadget;
class CReferenceObj;
class CReferenceObjList;
class CSafeZoneObjList;
class CSymbolMapperObjList;
class CtextReadoutObj;
class CTriggerObject;
class CViewportObj;
class CViewportList;
class CWaterZoneObjList;
class CWldObjectList;
class CWldGroupObjList;
class IExternalEXMesh;
class ParticleRM;
class StringListEvent;
class MovementObjectCfgEvent;

// for GetDynamicTexture
const int CUSTOM_TEXTURE = 1;
const int FRIEND_PICTURE = 2;
const int GIFT_PICTURE = 3;

class ClientBaseEngine : public Engine {
public:
	virtual ~ClientBaseEngine() {}

	// Returns the local player CMovementObj (my avatar).
	// NOTE: This can become NULL at any time for any player (including yourself) if they leave the zone.
	// If you don't validate this pointer on every call the client will crash!
	CMovementObj* GetMovementObjMe() const {
		CMovementObj* pMO = (m_movObjList && !m_movObjList->IsEmpty()) ? (CMovementObj*)m_movObjList->GetHead() : NULL;
		return pMO;
	}

	// Returns the CMovementObj (avatar) of the indexed player in the zone (0 is current local player).
	// NOTE: This can become NULL at any time for any player (including yourself) if they leave the zone.
	// If you don't validate this pointer on every call the client will crash!
	CMovementObj* GetMovementObj(int index) const {
		CMovementObj* pMO = (m_movObjList ? m_movObjList->getObjByIndex(index) : NULL);
		return pMO;
	}

	// Returns the CMovementObj (avatar) of the player by network id.
	// NOTE: This can become NULL at any time for any player (including yourself) if they leave the zone.
	// If you don't validate this pointer on every call the client will crash!
	CMovementObj* GetMovementObjByNetId(int netId) const {
		CMovementObj* pMO = netId ? (m_movObjList ? m_movObjList->getObjByNetTraceId(netId) : NULL) : GetMovementObjMe();
		return pMO;
	}

	// Returns the static CMovementObj (city vendor, etc.) by network id.
	// NOTE: This can become NULL at any time for any object if it is removed from the zone.
	// If you don't validate this pointer on every call the client will crash!
	CMovementObj* GetStaticMovementObjByNetId(int netId) const {
		CMovementObj* pMO = (m_movObjList ? m_movObjList->getObjTypeStaticByNetTraceId(netId) : NULL);
		return pMO;
	}

	// Returns the CMovementObj (avatar) of the player by trace id.
	// NOTE: This can become NULL at any time for any player (including yourself) if they leave the zone.
	// If you don't validate this pointer on every call the client will crash!
	CMovementObj* GetMovementObjByTraceId(int traceId) const {
		CMovementObj* pMO = (m_movObjList ? m_movObjList->getObjByTraceNumber(traceId) : NULL);
		return pMO;
	}

	// Returns the CMovementObj (avatar) of the player by name.
	// NOTE: This can become NULL at any time for any player (including yourself) if they leave the zone.
	// If you don't validate this pointer on every call the client will crash!
	CMovementObj* GetMovementObjByName(const std::string& name) const {
		CMovementObj* pMO = (m_movObjList ? m_movObjList->getObjByName(name.c_str()) : NULL);
		return pMO;
	}

	// Returns the CMovementObj Reference Model (object avatar is based off of) for the given movement object.
	// NOTE: This can become NULL at any time for any player (including yourself) if they leave the zone.
	// If you don't validate this pointer on every call the client will crash!
	CMovementObj* GetMovementObjRefModel(const CMovementObj* pMO = NULL) const {
		// Assume My Avatar ?
		if (!pMO) {
			pMO = GetMovementObjMe();
			if (!pMO)
				return NULL;
		}

		// Get Reference Model Movement Object
		if (!m_movObjRefModelList)
			return NULL;
		POSITION pos = m_movObjRefModelList->FindIndex(pMO->getIdxRefModel());
		if (!pos)
			return NULL;
		return (CMovementObj*)m_movObjRefModelList->GetAt(pos);
	}

	IMemSizeGadget* CreateMemSizeGadget() const;

	///////////////////////////////////////////////////////////////////////
	// Async Arm/Disarm Functions
	///////////////////////////////////////////////////////////////////////
	bool ArmItemsAsync(const std::vector<GLID>& glids, CMovementObj* pMO = NULL, bool realizeNow = true, bool armDefaults = true, bool sendMyEvent = true);
	bool EquipItemsAsync(const std::vector<GLID>& glidsSpecial, NETID netTraceId, bool sendMyEvent = true);

	///////////////////////////////////////////////////////////////////////
	// Sync Arm/Disarm Functions
	///////////////////////////////////////////////////////////////////////
	bool ArmItem(const GLID& glid, CMovementObj* pMO = NULL, bool realizeNow = true, bool armDefaults = true, bool sendMyEvent = true);
	bool DisarmItem(const GLID& glid, CMovementObj* pMO = NULL, bool realizeNow = true, bool armDefaults = true, bool sendMyEvent = true);
	bool EquipItemsSync(const std::vector<GLID>& glidsSpecial, NETID netTraceId, bool sendMyEvent = true);

	void ContentMetadataCacheAsync(const std::vector<GLID>& glids, std::function<void()> fnCallback, void* pCallbackOwner) {
		auto pCS = ContentService::Instance();
		if (pCS)
			pCS->ContentMetadataCacheAsync(glids, fnCallback, pCallbackOwner);
	}
	void ContentMetadataCacheSync(const std::vector<GLID>& glids);

	CGlobalInventoryObj* UGCItemDataCacheSync(const GLID& glid);

	bool DownloadFile(const std::string& strURL, const std::string& filePath);

	virtual std::string GetExternalTexture(int textureType, bool thumbnail, int assetId, const char* textureUrl) {
		return "";
	}

	virtual bool GetExternalTextureAsync(int textureType, int assetId, const char* textureUrl, IEvent* event, DownloadPriority prio = DL_PRIO_MEDIUM, bool thumbnail = false) {
		return true;
	}

	virtual bool ArmPreLoadProgress(IEvent* e) {
		return true;
	}

	virtual bool TestPatchFileForUpdate(const char* filename) {
		return true;
	}

	virtual BOOL LoadGlobalAttachmentDB(const std::string& fileName);

	virtual bool ConfigPlayerEquippableItem(CMovementObj* pMO, const CharConfigItem* pCCI);

	bool SetEquippableAnimation(CMovementObj* pMO, const GLID& glidAccessory, const GLID& glidAnimation, bool updateServer);
	bool SetEquippableAnimation(CDynamicPlacementObj* pDPO, const GLID& glidAccessory, const GLID& glidAnimation);

	bool RepositionEquippableItem(CDynamicPlacementObj* pDPO, const GLID& glidAccessory, const Vector3f& ptBBoxRelativePosition, const Vector3f& vOffset, const Vector3f& vEulerZXY, const Vector3f& vScale);

	// Media Functions
	virtual bool StopMediaOnDynamicObject(int objId) = 0;

	Timer m_msgMoveEnabledTimer; // total time msgMove has been enabled

	P2pAnimData m_p2pAnimData;

	bool P2pIsActive() const {
		return m_p2pAnimData.animState != eP2pAnimState::Inactive;
	}
	bool P2pIsActive(CMovementObj* pMO) const {
		return P2pIsActive() && pMO && (pMO->getNetTraceId() == m_p2pAnimData.netTraceId_init || pMO->getNetTraceId() == m_p2pAnimData.netTraceId_target);
	}

	// Override animation settings replace normal avatar animations
	bool setMyOverrideAnimationByType(eAnimType animType, int animVer = 0);
	bool setMyOverrideAnimationSettings(const AnimSettings& animSettings);
	bool getMyOverrideAnimationSettings(AnimSettings& animSettings);

	// Primary animations are in current use and include normal avatar animations
	bool getMyPrimaryAnimationSettings(AnimSettings& animSettings);

	ZoneIndex GetZoneIndex() const {
		return m_zoneIndex;
	}

	ULONG GetZoneIndexAsULong() const {
		return m_zoneIndex.GetULong();
	}

	ULONG GetZoneIndexType() const {
		return m_zoneIndex.GetType();
	}

	std::string GetZoneFileName() {
		return m_zoneFileName.GetString();
	}

	// World Pass Groups (AP)
	int m_passGroupsWorld; // bitmapped PASS_GROUP
	bool WorldPassGroup(const PASS_GROUP& passGroup) {
		return ((m_passGroupsWorld & (int)passGroup) != 0);
	}
	bool WorldPassGroupAP() {
		return WorldPassGroup(PASS_GROUP_AP);
	}

	// My Pass Groups (AP, VIP, GM, OWN, MOD)
	bool MyPassGroup(const PASS_GROUP& passGroup) {
		auto pMO = GetMovementObjMe();
		return (pMO ? pMO->hasPass(passGroup) : false);
	}
	bool MyPassGroupAP() {
		return MyPassGroup(PASS_GROUP_AP);
	}
	bool MyPassGroupVIP() {
		return MyPassGroup(PASS_GROUP_VIP);
	}
	bool MyPassGroupGM() {
		return MyPassGroup(PASS_GROUP_GM);
	}
	bool MyPassGroupOWN() {
		return MyPassGroup(PASS_GROUP_OWNER);
	}
	bool MyPassGroupMOD() {
		return MyPassGroup(PASS_GROUP_MODERATOR);
	}

	virtual bool WebGetPlayerCfg(const std::string& username, Vector3d& nameColor, int& passGroupsUser, int& passGroupsWorld) = 0;

	virtual bool setOverheadLabels(CMovementObj* pMO = nullptr);

	double m_GameTimeOffset; // Difference between game time and real time. Can be adjusted to synchronize clients and server.
	double m_CurrentGameTime; // Updated at the beginning of each frame.
	double m_PreviousGameTime; // Game time for previous frame.
	double m_PrevFrameStartTime; // This is real time. Use for frame rate related calculations.
	LONG m_iPrevFrameTickCount; // Same as above but in windows ticks. Used to convert between tick counts and our time stored as floating point seconds.

	double GetGameTimeDelta() const {
		return m_CurrentGameTime - m_PreviousGameTime;
	}

	double TickCountToRealTime(LONG tickCount) const {
		return (tickCount - m_iPrevFrameTickCount) * .001 + m_PrevFrameStartTime;
	}

	double GetFrameStartTime() const {
		return m_PrevFrameStartTime;
	}

protected:
	ClientBaseEngine(eEngineType engineType, ULONG version, const char* name);

	CStringA m_zoneFileName; // current zone exg file 'zoneName.exg'
	void SetZoneFileName(const std::string& zoneFileName) {
		m_zoneFileName = zoneFileName.c_str();
	}

	void SetConsoleIOHandles(const std::string& strStdInFile, const std::string& strStdOutFile, const std::string& strStdErrFile);

	bool ClientIsEnabled() const;

	// Initializes all script blades by loading them and registering events and handlers.
	bool InitScriptBlades();

	// Post-initializes all script blades.  Call only after all script and client blades are loaded.
	bool PostInitScriptBlades();

	virtual bool InitEvents();

	// Event Senders
	bool SendSystemServiceEvent(const std::string& userId);
	bool SendArmItemEvent(const GLID& glid, eArmItemAction action, eArmItemResult result);
	bool SendEntityCfgCompleteEvent();

	// Event Handlers (overidden)
	static EVENT_PROC_RC HandleLogonResponseEventStatic(ULONG lparam, IDispatcher* disp, IEvent* e);
	virtual EVENT_PROC_RC HandleLogonResponseEvent(const char* connectionErrorCode, HRESULT hResultCode, ULONG characcterCount, LOGON_RET replyCode) = 0;
	static EVENT_PROC_RC HandleStringListEventStatic(ULONG lparam, IDispatcher* disp, IEvent* e);
	virtual EVENT_PROC_RC HandleStringListEvent(StringListEvent* sle) = 0;
	static EVENT_PROC_RC HandleSetZonePermissionEventStatic(ULONG lparam, IDispatcher* disp, IEvent* e);
	virtual EVENT_PROC_RC HandleSetZonePermissionEvent(int zonePermissions) = 0;

	// Event Handlers (not overidden)
	static EVENT_PROC_RC HandleAddDynamicObjectEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleMoveDynamicObjectEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleMoveAndRotateDynamicObjectEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleScaleDynamicObjectEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleRotateDynamicObjectEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleRemoveDynamicObjectEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleUpdateDynamicObjectTextureEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleUpdatePictureFrameFriendEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleUpdateWorldObjectTextureEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleControlDynamicObjectEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleControlObjectEventSetFriction(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleControlObjectEventShowHide(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleDynamicObjectInitializedEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleRedirectConsoleIOEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleRenderTextEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleRenderLocaleTextEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleMovementObjectCfgEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	EVENT_PROC_RC HandleMovementObjectCfgEvent(MovementObjectCfgEvent* ece);

	// Last Move Data Sent From Client To Server For This Player
	MOVE_DATA m_moveDataToServer;

	// Message Senders
	bool SendMsgUpdateToServer();
	bool SendMsgMoveToServer();

	// Message Handlers (overridden)
	virtual bool HandleMsgs(BYTE* pData, NETID netIdFrom, size_t dataSize, bool& bDisposeMessage);
	virtual bool HandleMsgAttribToClient(BYTE* pData);
	virtual bool HandleMsgSpawnToClient(BYTE* pData, NETID netIdFrom, size_t dataSize);

	// Message Handlers (not overridden)
	bool HandleMsgEvent(BYTE* pData, size_t dataSize, NETID netIdFrom);
	bool HandleMsgLogToClient(BYTE* pData);
	bool HandleMsgGeneralToClient(BYTE* pData);
	bool HandleMsgUpdateToClient(BYTE* pData, NETID netIdFrom);
	bool HandleMsgMoveToClient(BYTE* pData, NETID netIdFrom);
	bool HandleMsgEquipToClient(BYTE* pData);
	bool HandleMsgSpawnStaticMOToClient(BYTE* pData);

	virtual CMovementObj* SpawnStaticMovementObject(
		int siType, // SERVER_ITEM_TYPE
		const std::string& name,
		const CharConfig* pCharConfig,
		const std::vector<GLID>& glidsArmed,
		int dbIndex,
		const Vector3f& scale,
		int netId,
		const Vector3f& pos,
		double rotDeg,
		const GLID& glidAnimSpecial,
		int placementId,
		bool networkEntity);

	virtual bool DeSpawnStaticMovementObj(NETID netTraceId);

	bool ProcessScriptAttribute(CEXScriptObj* sctPtr);

	void Callback();

	bool Initialize(const char* baseDir, const char* configFile, const DBConfig& dbConfig);

	virtual void HandleCollision();

	void EnvironmentUpdate();

	void TriggerCheckCallback();

	void InitAllDatabaseEntities() override; // engine

	bool DestroyMovementObjectRefModelList() {
		return Engine::DestroyMovementObjectRefModelList() && DestroyMovementObjectList();
	}

	void UniversalControlUnit();

	void ExitAllTriggers();

	virtual void DestroyWorldObjects();

	virtual void DestroyDynamicObjects();

	virtual void DeleteWorldGroupList();

	void UpdateAllEntityMovements();

	virtual void moveEntitySubstitute(CMovementObj* pMO, const Vector3f& newPos, const Vector3f& newDir) = 0;

	virtual bool UpdateEntityMoveData(MOVE_DATA moveData);

	virtual bool PlayCharacter(int index, bool reconnecting = false);

	size_t GetNumHumanPlayersInRuntimeList() const;

	virtual bool CreateMovementObject(
		int aiConfig, // Use physics configuration from AI DB
		const Matrix44f& transformMatrix, // Spawn transform matrix
		int dbIndex, // EDB model index
		eActorControlType controlType,
		int networkDefinedID, // networkDefinedID - entity trace ID from server?
		bool networkEntity, // entity is not locally defined
		bool addHead, // add at head (index=0 -> my avatar)
		bool spawnAtServer, // true if need to send spawn message to server for this entity
		int generatorLinkReference,
		CMovementObj** ppMO_out, // Return pointer to created CMovementObj
		const CharConfig* cfg, // DIM config
		bool bLocal = false, // true if local entity (should not notify server, bug?)
		const std::string& name = "");

	virtual bool DestroyMovementObject(CMovementObj* pMO);

	virtual bool DestroyMovementObjectList();

	virtual bool GenAndMount(
		CMovementObj* pMO_host,
		int aiConfig,
		int mountType = MT_RIDE,
		bool bLocal = false,
		CMovementObj** ppMO_out = NULL);

	virtual BOOL DeMount(CMovementObj* pMO_host);

	virtual void InitCamerasForMovementObject(CMovementObj* pMO) {}

	virtual bool AlwaysSpawnEntityAtServer() const {
		return false;
	}

	virtual void EntityClone(
		CMovementObj* pMO,
		CMovementObj** ppMO_out,
		int EDBindex,
		int* pSpawnCfgOpt,
		int* pMaterialCfgOpt,
		D3DCOLORVALUE* pSpawnColorOpt) {
		pMO->Clone(ppMO_out, NULL, pSpawnCfgOpt, pMaterialCfgOpt, pSpawnColorOpt, true);
	}

	virtual void EntityClone(
		CMovementObj* pMO,
		CMovementObj** ppMO_out,
		int EDBindex,
		int* pSpawnCfgOpt,
		int* pMaterialCfgOpt,
		D3DCOLORVALUE* pSpawnColorOpt,
		int traceNumber,
		int networkDefinedID) {
		pMO->Clone(ppMO_out, NULL, pSpawnCfgOpt, pMaterialCfgOpt, pSpawnColorOpt, true, traceNumber, networkDefinedID);
	}

	virtual void UserControlUse(CMovementObj* pMO, int controlConfigNum) {} // rendering only ?

	virtual bool updateAnimations(RuntimeSkeleton* pRS, const Matrix44f& matrix, bool firstPersonMode, bool updateParticleAndShadow);

	virtual bool EntityAnimationManagementCallback(CMovementObj* pMO);

	virtual bool EntityAnimationManagementCallbackForMe() {
		auto pMO_me = GetMovementObjMe();
		return EntityAnimationManagementCallback(pMO_me);
	}

	bool SetEquippableAnimation(RuntimeSkeleton* pRuntimeSkeleton, const GLID& glidAccessory, const GLID& glidAnimation, bool updateServer);

	virtual bool DynamicObjectAnimationManagementCallback(CDynamicPlacementObj* pDPO);

	virtual void SkeletalParticleEmitterCallback(const Matrix44f& positionalMatrix, RuntimeSkeleton* pRS, bool killExisting) {} // rendering only

	virtual BOOL LoadReferenceLibrary(const std::string& fileName);

	ZoneIndex m_zoneIndex; // current zone index union { type:4 | instanceId:16 | channelId:12 }
	void SetZoneIndex(ZoneIndex zoneIndex) {
		m_zoneIndex = zoneIndex;
	}

	bool m_rezoning;
	void SetRezoning(bool val) {
		m_rezoning = val;
	}
	bool IsRezoning() const {
		return m_rezoning;
	}

	volatile bool m_loadingScene; // Set to true if LoadSceneThread is in progress
	void SetLoadingScene(bool val) {
		m_loadingScene = val;
	}
	bool IsLoadingScene() const {
		return m_loadingScene;
	}

	bool m_zoneLoadingUIActive; // Set to true if zone loading progress menu is open
	void SetZoneLoadingUIActive(bool val) {
		if (m_zoneLoadingUIActive == val)
			return;
		m_zoneLoadingUIActive = val;
		if (val)
			SetZoneLoadingProgressPercent(0.0);
	}
	bool IsZoneLoadingUIActive() const {
		return m_zoneLoadingUIActive;
	}

	double m_zoneLoadingProgressPercent;
	void SetZoneLoadingProgressPercent(double percent) {
		m_zoneLoadingProgressPercent = percent;
	}
	double GetZoneLoadingProgressPercent() const {
		return m_zoneLoadingProgressPercent;
	}

	bool m_spawning; // Set to true when avatar is spawning to a new zone, reset when ZoneCustomizationDLCompleteEvent is received
	void SetSpawning(bool val) {
		m_spawning = val;
	}
	bool IsSpawning() const {
		return m_spawning;
	}

	bool m_msgMoveEnabled = true;
	void SetMsgMoveEnabled(bool enable) {
		m_msgMoveEnabled = enable;
	}
	bool IsMsgMoveEnabled();

	bool IsMsgUpdateEnabled() const;

	void RemoveAllTriggers();
	void RemoveAllDynamicObjectTriggers();

	// My avatar just spawned into the world
	void OnJustSpawned();

	virtual bool LoadScene(const std::string& exgFileName, const char* url = nullptr) = 0;

	void InitializeWorldObjectCollision();
	void InitWorldObjectTriggerList();

	virtual void RebuildTraceNumber();

	virtual bool EvaluateAllTriggers(const Vector3f& pos, CMovementObj* pMO);
	virtual bool EvaluateTrigger(CTriggerObject* pTO, eTriggerState triggerState, const Vector3f& pos, CMovementObj* pMO, bool async) = 0;

	//! \brief Invalid light cache for world objects and dynamic placement objects
	//  which are within 'range' units of 'location'
	//  Passing in NULL for 'location' causes everything in zone to be invalidated
	virtual void InvalidateLightCaches(const Vector3f* location = NULL, float range = 0.0f);

	virtual void DestroyAllParticleInterfaces();

	virtual void ZoneLoaded(const char* _exg, const char* _url, bool reloading) {} // base class does nothing

	virtual void MoveDynamicObjectTriggers(int placementId, const Vector3f& origin, const Vector3f& dir);
	virtual void RemoveDynamicObjectTriggers(int placementId) = 0;

	virtual bool AttachPathFinder(ObjectType targetType, int targetId, float destX, float destY, float destZ, float destRX, float destRY, float destRZ, float speed, int motionStyle, int timeout, int recalcInterval, int aiMode, bool noPhysics, bool noPlayerControl);
	virtual bool DetachPathFinder(ObjectType targetType, int targetId, bool bCancel, int cancelReason);
	virtual IPathFinder* GetPathFinder(ObjectType targetType, int targetId);
	virtual void ClearAllPathFinders(bool bCancel, int cancelReason);

	int ShowPhysicsWindow(int mode);

	virtual IDirect3DDevice9* GetD3DDevice();

	virtual CSoundBankList* GetSoundManager() {
		return 0;
	}

	size_t GetDynamicObjectCount() const {
		return m_dynamicPlacementManager.GetObjectCount();
	}

	CDynamicPlacementObj* GetDynamicObjectByIndex(size_t index);

	CDynamicPlacementObj* GetDynamicObject(int placementId);
	const CDynamicPlacementObj* GetDynamicObject(int placementId) const;
	bool RemoveDynamicObject(int placementId);
	void RemoveAllDynamicPlacements();

	std::unique_ptr<CDynamicPlacementObj> CreateDynamicObjectWithPlacementId(GLID globalId, int placementId, bool fromMovementObj = false);

	struct PlaceDynamicObjectArgs {
		Vector3f position;
		Vector3f direction;
		Vector3f slide;
		int playerId;
		int placementId;
		int globalId;
		Vector3f min;
		Vector3f max;
		bool attachable = false;
		bool interactive = false;
		bool mediaSupported = false;
		bool isDerivable = false;
		float scaleFactor = 1.0f;
		int assetId = 0;
		int invSubType = IT_NORMAL;
		std::string textureUrl = "";
		int gameItemId = INVALID_GAMEITEM;
		bool fromMovementObj = false;
	};
	CDynamicPlacementObj* PlaceDynamicObject(const PlaceDynamicObjectArgs& args);

	CDynamicPlacementObj* PlaceActorAsDynamicObject(const PlaceDynamicObjectArgs& args);
	virtual void LightListDirty() = 0;

	template <typename Callable>
	__forceinline void IterateObjects(Callable&& callable) {
		m_dynamicPlacementManager.IterateObjects(std::forward<Callable>(callable));
	}
	template <typename Callable>
	__forceinline void IterateObjects(Callable&& callable) const {
		m_dynamicPlacementManager.IterateObjects(std::forward<Callable>(callable));
	}
	template <typename Callable>
	__forceinline void ForEachTrigger(const Callable& callable) {
		m_dynObjTriggers.ForEachTrigger(callable);
		m_wldObjTriggers.ForEachTrigger(callable);
	}

	void StepPhysics();

	// #MLB_BulletSensor - ClientBaseEngine LandClaim
	bool IsObjectInLandClaim(int objectPid, int landClaimPid);
	bool RegisterLandClaim(int landClaimPid);
	bool UnRegisterLandClaim(int landClaimPid);

	int m_startCamera;
	EVENT_ID m_attribEventId;
	EVENT_ID m_outOfAmmoEventId;
	EVENT_ID m_dynObjectTextureEventId;
	EVENT_ID m_worldObjectTextureEventId;
	EVENT_ID m_friendObjectTextureEventId;
	EVENT_ID m_zoneReloadedEventId;
	EVENT_ID m_iconMetadataReadyEventId;
	EVENT_ID m_childGameWebCallsReadyEventId;
	EVENT_ID m_entityCfgCompleteEventId;
	EVENT_ID m_volumeChangedEventId;
	EVENT_ID m_dynamicObjectMovedEventId;

	CMovementObjList* m_movObjList; // list of avatars currently in zone

	DynamicObjectRM* m_dynamicObjectRM;
	EquippableRM* m_equippableRM;
	ParticleRM* m_particleRM;

	DynamicPlacementManager m_dynamicPlacementManager;

	CSymbolMapperObjList* m_symbolMapDB;

	CtextReadoutObj* m_pTextReadoutObj;

	CCollisionObj* m_pCollisionObj;

	CTriggerObjectList m_dynObjTriggers;
	CTriggerObjectList m_wldObjTriggers;

	CWldObjectList* m_pWOL;
	Mutex m_WldObjListMutex;

	CWldObjectList* m_curWldObjPrtList;

	CReferenceObjList* m_libraryReferenceDB;

	CWldGroupObjList* worldGroupList;

	TimeMs m_triggerDelayStamp;

	int m_globalTraceNumber;

	std::unique_ptr<IOldPhysics> m_oldPhysics;

	TextureDatabase* m_pTextureDB;

	std::map<UINT64, IPathFinder*> m_pathFinderMap;

	std::shared_ptr<Physics::Universe> m_pUniverse;
	std::vector<std::shared_ptr<Physics::RigidBodyStatic>> m_apRigidBodyWorldObjects;
	std::shared_ptr<Physics::DebugWindow> m_pPhysicsDebugWindow;
	int m_iShowPhysicsWindow;

	Timer m_msgUpdateToServerTimer;
	size_t m_msgUpdateToServerOutstanding = 0;
	Timer m_msgMoveToServerTimer;
	size_t m_msgMoveToServerOutstanding = 0;

	std::set<NETID> m_moNetTraceIdsRequested; // movement object config net trace id's requested
};

} // namespace KEP