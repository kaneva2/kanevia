///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "FreeFallDetector.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

void FreeFallDetector::Update(const Vector3f& currentPos, const Vector3f& currentVel) {
	m_Pos = currentPos;
	m_Vel = currentVel;
	m_elapsedFrameTime = m_lastFrameTime > 0 ? fTime::ElapsedSec(m_lastFrameTime) : 0;
	m_lastFrameTime = fTime::TimeSec();

	switch (m_state) {
		case State::eIDLE:
			if (IsFreeFallStart()) {
				Reset();
				SetNextState(State::eTRACKING);
			}
			break;

		case State::eTRACKING:
			TrackFreeFall();
			break;

		case State::eCOOL_DOWN:
			if (IsCoolDownDone()) {
				Reset();
				SetNextState(State::eIDLE);
			}
			break;
	}
}

float FreeFallDetector::CummlVelAverage() {
	float n = m_totalTrackingSteps;
	m_totalTrackingSteps = n + 1.0f;
	float cummVelAverage = (m_Vel.Y() + (n * m_cummlVelAverage)) / m_totalTrackingSteps;
	return cummVelAverage;
}

bool FreeFallDetector::IsCoolDownDone() {
	return (fTime::ElapsedSec(m_coolDownStartTime) > m_coolDownTime);
}

void FreeFallDetector::LogFreeFallWarning() {
	LogWarn("FREEFALL DETECTED - Respawning...");
}

void FreeFallDetector::Reset() {
	m_totalTime = 0.0f;
	m_elapsedFrameTime = 0.0f;
	m_totalTrackingSteps = 0.0f;
	m_cummlVelAverage = 0.0f;
}

void FreeFallDetector::SetNextState(State nextState) {
	if (m_state != nextState) {
		m_state = nextState;
	}
}

void FreeFallDetector::TrackFreeFall() {
	m_totalTime += m_elapsedFrameTime;
	m_cummlVelAverage = CummlVelAverage();

	// The Sky Dome is an artifact from our EXG file structure
	// and is not easily modified, but has collision enabled.
	// If the player collides with the Sky Dome first, we still
	// want to respawn the player.
	//
	if (IsSkyDome()) {
		LogWarn("Trigger Player Respawn From Collision with Sky Dome");
		TriggerPlayerRespawn();
	} else {
		// It's possible for the user or some other component to
		// respawn/teleport the player to a new position/world. Don't
		// continue to track if the threshold condition is no
		// longer valid.
		//
		if (IsFreeFallStart()) {
			if (IsFreeFallStop()) {
				LogWarn("Trigger Player Respawn From Stop Constraints");
				TriggerPlayerRespawn();
			}
		} else {
			Reset();
			SetNextState(State::eIDLE);
		}
	}
}

void FreeFallDetector::TriggerPlayerRespawn() {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	LogFreeFallWarning();
	Reset();
	m_coolDownStartTime = fTime::TimeSec();
	SetNextState(State::eCOOL_DOWN);

	if (m_enabled) {
		// #MLB_ED_7947 New event that resolves ED-7830, ED-7833
		pICE->RequestPlayerRespawn();

	} else {
		// #MLB_ED_7397 SERVER FALL THROUGH WORLD...
		LogWarn("FreeFallDetector is disabled - Are you in DevMode?");
	}
}

} // namespace KEP
