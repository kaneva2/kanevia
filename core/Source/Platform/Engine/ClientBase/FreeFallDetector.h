///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/Include/KEPConstants.h"
#include "Core/Util/HighPrecisionTime.h"

#include "IClientEngine.h"

namespace KEP {

class FreeFallDetector {
public:
	enum class State {
		eIDLE,
		eTRACKING,
		eCOOL_DOWN,
	};

	FreeFallDetector() = default;
	~FreeFallDetector() = default;
	FreeFallDetector(const FreeFallDetector&) = delete;

	// #MLB_ED_7397 SERVER FALL THROUGH WORLD...
	void SetEnabled(bool enabled) {
		m_enabled = true;
	}

	fTime::Sec GetFrameTime() const {
		return m_elapsedFrameTime;
	}

	bool IsInFreeFall() const {
		return m_isCollisionEnabled && (m_surfaceProperty == ObjectType::NONE) && (m_Vel.Y() < m_thresholdStartVel) && !m_isJumping;
	}

	void SetIsCollisionEnabled(bool isCollisionEnabled) {
		m_isCollisionEnabled = isCollisionEnabled;
	}

	void SetIsJumpApplied(bool isJumping) {
		m_isJumping = isJumping;
	}

	void SetSurfaceProperty(ObjectType currentSurfaceProp) {
		m_surfaceProperty = currentSurfaceProp;
	}

	void Update(const Vector3f& currentPos, const Vector3f& currentVel);

private:
	float CummlVelAverage();

	bool IsCoolDownDone();

	bool IsFreeFallStart() {
		return (IsInFreeFall() && (m_Pos.Y() < m_thresholdStartPos));
	}

	bool IsFreeFallStop() {
		return ((m_cummlVelAverage < m_thresholdVel) && (m_totalTrackingSteps > m_thresholdTrackingSteps));
	}

	bool IsSkyDome() {
		return ((m_Pos.Y() < m_SkyDomeBoundary) && (m_surfaceProperty == ObjectType::WORLD));
	}

	void LogFreeFallWarning();

	void SetNextState(State nextState);

	void Reset();

	void TrackFreeFall();

	void TriggerPlayerRespawn();

	const float m_thresholdStartPos = { -25.0f }; // Start tracking when the current Y pos is at or below this Y pos
	const float m_thresholdStartVel = { -.50 }; // Start tracking when the current velocity is less than this
	const float m_thresholdVel = { -.72f }; // From profiling, this is about 90% of the unit-less terminal velocity -.82
	const float m_thresholdTrackingSteps = { 75.0 }; // Number of tracking steps required to trigger falling free
	const double m_coolDownTime = { 30.0 }; // Number of seconds to cool down after trigger has fired
	const float m_SkyDomeBoundary = { -50.0f }; // A zone object below this level is most likely the SkyDome

	State m_state = { State::eIDLE };
	float m_totalTrackingSteps = { 0 };
	float m_cummlVelAverage = { 0 };
	fTime::Sec m_lastFrameTime = { 0 };
	fTime::Sec m_elapsedFrameTime = { 0 };
	fTime::Sec m_totalTime = { 0 };
	fTime::Sec m_coolDownStartTime = { 0 };

	Vector3f m_Pos = { 0, 0, 0 };
	Vector3f m_Vel = { 0, 0, 0 };
	ObjectType m_surfaceProperty = { ObjectType::NONE };
	bool m_isJumping = { false };
	bool m_isCollisionEnabled = { true };
	bool m_enabled = { true }; // drf - added
};

} // namespace KEP
