///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <fcntl.h>

#include "ClientBaseEngine.h"

#include "KEPHelpers.h"
#include "KEPDiag.h"

#include <SerializeHelper.h>

#include "CMovementObj.h"
#include "CCollisionClass.h"
#include "CEXScriptClass.h"
#include "CHousingClass.h"
#include "ViewPortClass.h"
#include "CCameraClass.h"
#include "CtextReadoutClass.h"
#include "CFrameObj.h"
#include "CWldObject.h"
#include "CHousingClass.h"
#include "CRTLightClass.h"
#include "CReferenceLibrary.h"
#include "CExplosionClass.h"
#include "CABD3DFunctionLib.h"
#include "CMissileClass.h"
#include "CEnvironmentClass.h"
#include "CSafeZones.h"
#include "CStormClass.h"
#include "CTimeSystem.h"
#include "CWaterZoneClass.h"
#include "CWorldGroupClass.h"
#include "ScriptAttributes.h"
#include "DynamicObj.h"
#include "IClientEngine.h"
#include "ReDynamicGeom.h"
#include "CPhysicsEmuClass.h"
#include "AIclass.h"
#include "CArmedInventoryClass.h"
#include "CBoneAnimationClass.h"
#include "CFireRangeClass.h"
#include "CShadowClass.h"
#include "CSphereCollisionClass.h"
#include "CBoneClass.h"
#include "CDamagePathClass.h"
#include "SkeletonConfiguration.h"
#include "RuntimeSkeleton.h"
#include "CSkeletonObject.h"
#include "CDeformableMesh.h"
#include "CGlobalInventoryClass.h"
#include "CMultiplayerClass.h"
#include "CTextRenderClass.h"
#include "CWldObject.h"
#include "Tools/NetworkV2/Protocols.h"
#include "TextureDatabase.h"
#include "TextureManager.h"
#include "ReMeshCreate.h"
#include "DynamicObjectManager.h"
#include "SkeletonManager.h"
#include "EquippableSystem.h"
#include "CServerItemClass.h"
#include "CoreItems.h"
#include "UgcConstants.h"
#include "UseTypes.h"
#include "CNodeObject.h"
#include "PassGroups.h"
#include "EnvParticleSystem.h"

#include "SkeletonLabels.h"
#include "ActorDatabase.h"

#if defined(_ClientOutput)
#include "SkeletalAnimationManager.h"
#include "AnimationProxy.h"
#endif

#include "common\KEPUtil\Helpers.h"
#include "common\include\CMemSizeGadget.h"

#include "LogHelper.h"
static LogInstance("Instance");

#include "UnicodeMFCHelpers.h"

#include "OldPhysics.h"

#include "PhysicsUserData.h"
#include <KEPPhysics/Avatar.h>
#include <KEPPhysics/CollisionShape.h>
#include <KEPPhysics/PhysicsFactory.h>
#include <KEPPhysics/RigidBodyStatic.h>
#include <KEPPhysics/Universe.h>
#include <KEPPhysics/Vehicle.h>
#include <KEPPhysics/Sensor.h>

#include "CharConfigStatic.cpp" // drf - added
#include "SerializeFromFile.h"
#include "SafeDeleteList.h"

namespace KEP {

ULONG EventBladeVersion = PackVersion(1, 0, 0, 0);

static void readSystemReadoutSettings(CtextReadoutObj*& systemReadout, Logger& logger, Dispatcher& dispatcher) {
	if (systemReadout)
		delete systemReadout;

	systemReadout = new CtextReadoutObj();
	systemReadout->SetDispatcher(&dispatcher);
	systemReadout->m_locationX = 200;
	systemReadout->m_locationY = 9500;
	systemReadout->m_cgVertSpacing = .015f;
	systemReadout->m_maxLineLength = 51;
	systemReadout->m_verticleLineVistCount = 9;
	systemReadout->m_cgFontSizeX = .009f;
	systemReadout->m_cgFontSizeY = .009f;
	systemReadout->m_maxMeasuredLength = .5f;

	CFileException excAuto;
	CFile flAuto;
	CArchive the_ArchiveAuto(&flAuto, CArchive::load);
	BOOL resAuto = flAuto.Open(L"vis.cfg", CFile::modeRead, &excAuto);
	if (resAuto) {
		BEGIN_SERIALIZE_TRY
		CStringA vertSpacing, maxLineLength, verticleVisCount, cfFontSzX, cfFontSzY;
		ReadString(the_ArchiveAuto, vertSpacing);
		ReadString(the_ArchiveAuto, maxLineLength);
		ReadString(the_ArchiveAuto, verticleVisCount);
		ReadString(the_ArchiveAuto, cfFontSzX);
		ReadString(the_ArchiveAuto, cfFontSzY);
		the_ArchiveAuto.Close();
		flAuto.Close();

		systemReadout->m_cgVertSpacing = (float)atof(vertSpacing); // kep add cast
		systemReadout->m_maxLineLength = atoi(maxLineLength);
		systemReadout->m_verticleLineVistCount = atoi(verticleVisCount);
		systemReadout->m_cgFontSizeX = (float)atof(cfFontSzX); // kep add cast
		systemReadout->m_cgFontSizeY = (float)atof(cfFontSzY); // kep add cast
		END_SERIALIZE_TRY(logger)
	}
}

ClientBaseEngine::ClientBaseEngine(eEngineType engineType, ULONG version, const char* name) :
		Engine(engineType, version, name),
		m_rezoning(false),
		m_loadingScene(false),
		m_zoneLoadingUIActive(false),
		m_spawning(false),
		m_startCamera(-1),
		m_attribEventId(BAD_EVENT_ID),
		m_outOfAmmoEventId(BAD_EVENT_ID),
		m_dynObjectTextureEventId(BAD_EVENT_ID),
		m_worldObjectTextureEventId(BAD_EVENT_ID),
		m_friendObjectTextureEventId(BAD_EVENT_ID),
		m_zoneReloadedEventId(BAD_EVENT_ID),
		m_iconMetadataReadyEventId(BAD_EVENT_ID),
		m_childGameWebCallsReadyEventId(BAD_EVENT_ID),
		m_entityCfgCompleteEventId(BAD_EVENT_ID),
		m_volumeChangedEventId(BAD_EVENT_ID),
		m_dynamicObjectMovedEventId(BAD_EVENT_ID),
		m_movObjList(nullptr),
		m_symbolMapDB(nullptr),
		m_pTextReadoutObj(nullptr),
		m_pCollisionObj(nullptr),
		m_pWOL(nullptr),
		m_libraryReferenceDB(nullptr),
		m_curWldObjPrtList(nullptr),
		m_triggerDelayStamp(fTime::TimeMs()),
		m_globalTraceNumber(1),
		worldGroupList(nullptr),
		m_pTextureDB(nullptr),
		m_particleRM(nullptr),
		m_passGroupsWorld(PASS_GROUP_INVALID),
		m_iShowPhysicsWindow(0),
		m_GameTimeOffset(0),
		m_CurrentGameTime(-DBL_MAX),
		m_PreviousGameTime(DBL_MAX),
		m_PrevFrameStartTime(-DBL_MAX),
		m_iPrevFrameTickCount(fTime::TimeMs()) {
	m_dispatcher.SetNetId(-1); // set to invalidId until we login
	readSystemReadoutSettings(m_pTextReadoutObj, m_engineLogger, m_dispatcher);
	m_zoneFileName = "NONE";

	m_pWOL = new CWldObjectList();
	m_movObjList = new CMovementObjList();
	m_libraryReferenceDB = new CReferenceObjList();
	m_curWldObjPrtList = new CWldObjectList();
	worldGroupList = new CWldGroupObjList();
	m_pCollisionObj = new CCollisionObj();
	m_symbolMapDB = new CSymbolMapperObjList();

	// DRF - Construct Resource Managers
	m_dynamicObjectRM = new DynamicObjectRM("dynamicObjectRM"); // drf - also done in ClientEngine_UserInferface::ProcessScriptAttribute()
	m_equippableRM = new EquippableRM("equippableRM");
	m_particleRM = new ParticleRM("particleRM");

	Physics::UniverseOptions opt;
	opt.m_vGravityMPS2.Set(0, -9.8f, 0);
	m_pUniverse = Physics::CreateUniverse(opt);
}

bool ClientBaseEngine::Initialize(const char* baseDir, const char* configFile, const DBConfig& dbConfig) {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	bool ret = Engine::Initialize(baseDir, configFile, dbConfig);

	if (ret) {
		m_multiplayerObj = new CMultiplayerObj(m_instanceName.c_str());
		m_multiplayerObj->SetPathBase(PathBase());
		m_multiplayerObj->SetDispatcher(&m_dispatcher);

		FileName fileName = PathAdd(PathGameFiles(), m_autoexec);
		if (!RunScript(fileName))
			return false;
		m_multiplayerObj->GetIPDataFromDisk();

		m_oldPhysics = std::make_unique<OldPhysics>();
		m_oldPhysics->Initialize(m_environment);
	}

	return InitEvents();
}

void ClientBaseEngine::SetConsoleIOHandles(const std::string& strStdInFile, const std::string& strStdOutFile, const std::string& strStdErrFile) {
	static const wchar_t* mode[3] = { L"r", L"w", L"w" };
	FILE* aStdFiles[3] = { stdin, stdout, stderr };
	const std::string* apstrNewFileNames[3] = { &strStdInFile, &strStdOutFile, &strStdErrFile };

	for (size_t i = 0; i < 3; ++i) {
		if (apstrNewFileNames[i]->empty())
			continue;

		FILE* pFile = _wfreopen(Utf8ToUtf16(*apstrNewFileNames[i]).c_str(), mode[i], aStdFiles[i]);
		if (!pFile)
			LogError("Failed to redirect console handle " << i);
	}
}

void ClientBaseEngine::InitializeWorldObjectCollision() {
	for (const std::shared_ptr<Physics::RigidBodyStatic>& pRigidBody : m_apRigidBodyWorldObjects)
		m_pUniverse->Remove(pRigidBody.get());
	m_apRigidBodyWorldObjects.clear();

	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	if (!m_pWOL)
		return;

	std::set<int> setFilterGroups;
	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) {
		auto pWO = (CWldObject*)m_pWOL->GetNext(posLoc);

		setFilterGroups.insert(pWO->m_filterGroup);
		ReMesh* pReMesh = pWO->GetReMesh();
		if (!pReMesh)
			continue;

		Physics::CollisionShapeMeshProperties meshProps = pReMesh->GetPhysicsMeshProps();
		std::shared_ptr<Physics::CollisionShape> pCollisionShape = Physics::CreateCollisionShape(meshProps, Vector3f(FeetToMeters(1.0f)));
		Physics::RigidBodyStaticProperties rigidBodyProps; // Use defaults.
		std::shared_ptr<Physics::RigidBodyStatic> pRigidBody = Physics::CreateRigidBodyStatic(rigidBodyProps, pCollisionShape.get(), 1.0f);
		if (pRigidBody) {
			uint16_t iGroup, iMask;
			if (pWO->m_collisionInfoFilter == 0) {
				// Used for physical collisions.
				iGroup = CollisionGroup::Object | CollisionGroup::Visible;
				iMask = CollisionMask::Object;
			} else {
				// Visible object. Used for picking and camera collision.
				iGroup = CollisionGroup::Visible;
				iMask = CollisionGroup::None;
			}
			pRigidBody->SetCollisionMasks(iGroup, iMask);
			pRigidBody->SetUserData(std::make_shared<PhysicsBodyData>(ObjectType::WORLD, pWO->m_identity, pWO));
			const Matrix44f* pWorldMatrix = pReMesh->GetWorldMatrix();
			pRigidBody->SetTransformMetric(FeetToMeters(*pWorldMatrix));
			m_pUniverse->Add(pRigidBody);
			m_apRigidBodyWorldObjects.push_back(std::move(pRigidBody));
		}
	}
}

void ClientBaseEngine::InitWorldObjectTriggerList() {
	m_wldObjTriggers.Clear();

	for (POSITION pos = m_pWOL->GetHeadPosition(); pos != NULL;) {
		CWldObject* pWO = dynamic_cast<CWldObject*>(m_pWOL->GetNext(pos));
		if (pWO && pWO->m_triggerList)
			m_wldObjTriggers.AddTriggers(*pWO->m_triggerList);
	}
}

bool ClientBaseEngine::InitScriptBlades() {
	// Implemented in InitScriptEvents.h
	return ::KEP::InitScriptBlades(m_engineLogger, &m_dispatcher, "", m_scriptDir, IsBaseDirReadOnly(), NULL, m_eventBladeFactory, m_eventBlades);
}

bool ClientBaseEngine::PostInitScriptBlades() {
	// Implemented in InitScriptEvents.h
	return ::KEP::PostInitScriptBlades(m_engineLogger, &m_dispatcher, m_eventBladeFactory, m_eventBlades);
}

int ClientBaseEngine::ShowPhysicsWindow(int mode) {
	switch (mode) {
		case 0:
		case 1:
			m_iShowPhysicsWindow = mode;
			break;
	}
	return m_iShowPhysicsWindow;
}

void ClientBaseEngine::StepPhysics() {
	m_pUniverse->Step(GetFrameStartTime(), GetGameTimeDelta());
}

// Collision tests using physics system.
void ClientBaseEngine::HandleCollision() {
	for (POSITION posLoc = m_movObjList->GetHeadPosition(); posLoc != NULL;) {
		auto pMO = (CMovementObj*)m_movObjList->GetNext(posLoc);
		if (!pMO || pMO->isNetworkEntity() || pMO->isTypeStatic() || pMO->canFilterCollision() || pMO->getPosses() || pMO->getPhysicsVehicle())
			continue;

		auto pMC = pMO->getMovementCaps();
		if (!pMC)
			continue;

		if (!pMC->IsJumping())
			pMO->m_recentSurfaceInfo.objectType = ObjectType::NONE;

		pMC->SetGravityEnabled(TRUE);

		if (!pMC->IsCollisionEnabled()) {
			// Can't Land So Disable Jump
			pMC->SetJumping(false);
			pMO->m_recentSurfaceInfo.objectType = ObjectType::NONE;
			if (pMO->getPhysicsAvatar()) {
				// When collision is disabled, reset the current surface
				// property of the avatar to NONE
				pMC->SetSurfaceProp(ObjectType::NONE);
			}
			continue;
		}

		if (pMO->getPhysicsAvatar()) {
			// Reset the current surface property of the avatar to NONE
			// If the avatar is standing on a surface it is set below
			pMC->SetSurfaceProp(ObjectType::NONE);

			const Vector3f& ptStartPositionFeet = pMO->getLastPosition();
			Vector3f ptStartPositionMeters = FeetToMeters(ptStartPositionFeet);
			Vector3f ptEndPositionFeet = pMO->getBaseFramePosition();
			Vector3f ptEndPositionMeters = FeetToMeters(ptEndPositionFeet);

			Physics::AvatarMoveResult collideResult;
			Vector3f ptCollide;
			if (pMO->getPhysicsAvatar()->AdjustAfterMove(m_pUniverse.get(), ptStartPositionMeters, ptEndPositionMeters, out(collideResult))) {
				Vector3f ptFinalMeters = collideResult.m_ptFinal;
				Vector3f ptFinalFeet = MetersToFeet(ptFinalMeters);

				pMC->SetPosLast(pMC->GetPosLast() + ptFinalFeet - ptEndPositionFeet);
				pMO->setBaseFramePosition(ptFinalFeet, true);

				std::vector<int> aDynObjIds;
				for (Physics::AvatarMoveResult::Collision& collision : collideResult.m_aCollisions) {
					// Send collision event
					PhysicsBodyData* pPhysicsBodyData = nullptr;
					if (collision.m_pCollidable) {
						PhysicsUserData* pPhysicsUserData = static_cast<PhysicsUserData*>(collision.m_pCollidable->GetUserData().get());
						pPhysicsBodyData = dynamic_cast<PhysicsBodyData*>(pPhysicsUserData);
					}

					if (pPhysicsBodyData && pPhysicsBodyData->m_ObjectType == ObjectType::DYNAMIC) {
						aDynObjIds.push_back(pPhysicsBodyData->m_iObjectId);
						pMC->SetSurfaceProp(pPhysicsBodyData->m_ObjectType);
					}

					if (ptEndPositionFeet.y <= ptStartPositionFeet.y && collision.m_vContactNormal.y > 0.5f) {
						// Handle standing on object. (Can't be standing on an object if moving upward.)
						if (pPhysicsBodyData) {
							pMO->m_recentSurfaceInfo.objectType = pPhysicsBodyData->m_ObjectType;
							pMO->m_recentSurfaceInfo.objectId = pPhysicsBodyData->m_iObjectId;
							pMO->m_recentSurfaceInfo.transform = MetersToFeet(collision.m_pCollidable->GetTransformMetric());
							pMO->m_recentSurfaceInfo.relativeTransform = pMO->getBaseFrameMatrix() * InverseOf(pMO->m_recentSurfaceInfo.transform);

							// Set the avatars current surface property to this object it's standing on
							pMC->SetSurfaceProp(pPhysicsBodyData->m_ObjectType);
						}
						pMO->m_recentSurfaceInfo.contactLocation = ptFinalFeet;
						pMO->m_recentSurfaceInfo.faceNormal = collision.m_vContactNormal;
						pMC->SetJumping(false);
						pMC->SetGravityEnabled(FALSE);
					}
				}

				// Send ScriptServerEvent for all objects that had their collision state change.
				pMO->setCurrentCollision(std::move(aDynObjIds), [this, pMO](int iId, bool bNewState) {
					CDynamicPlacementObj* pDPO = this->GetDynamicObject(iId);
					if (pDPO && !pDPO->IsLocal()) { // Ignore any local object that does not carry a global PID
						auto pSkeleton = pDPO->getSkeleton();
						if (pSkeleton) {
							IEvent* e = new ScriptServerEvent(
								bNewState ? ScriptServerEvent::Collide : ScriptServerEvent::EndCollide,
								pDPO->GetPlacementId(),
								0,
								0,
								pMO->getNetTraceId());
							(*e->OutBuffer())
								<< pSkeleton->getWorldMatrix()(3, 0)
								<< pSkeleton->getWorldMatrix()(3, 1)
								<< pSkeleton->getWorldMatrix()(3, 2);
							e->AddTo(SERVER_NETID);
							m_dispatcher.QueueEvent(e);
						}
					}
				});
			}
		}
	}
}

// This function controls my avatar's movement
void ClientBaseEngine::UniversalControlUnit() {
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;
	UserControlUse(pMO, pMO->getControlConfig());
}

// My Avatar Just Spawned In To This Zone
void ClientBaseEngine::OnJustSpawned() {
	// Remove dynamic object triggers upon spawn. World triggers are only removed when a new world is loaded.
	RemoveAllDynamicObjectTriggers();

	// Reset Move Data To Server
	m_moveDataToServer = MOVE_DATA();

	// Reset Message Stats
	m_msgUpdateToServerTimer.Reset();
	m_msgUpdateToServerOutstanding = 0;
	m_msgMoveToServerTimer.Reset();
	m_msgMoveToServerOutstanding = 0;

	// Reset NetIds Requested
	m_moNetTraceIdsRequested.clear();
}

void ClientBaseEngine::DestroyWorldObjects() {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	if (!m_pWOL)
		return;

	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) {
		auto pWO = (CWldObject*)m_pWOL->GetNext(posLoc);
		if (pWO && pWO->m_node) {
			auto pRO = (CReferenceObj*)m_libraryReferenceDB->GetByIndex(pWO->m_node->m_libraryReference);
			if (pRO)
				pRO->m_externalSource.reset();
		}

		pWO->SafeDelete();
		delete pWO;
		pWO = NULL;
	}

	m_pWOL->RemoveAll();

	delete m_pWOL;
	m_pWOL = NULL;
}

void ClientBaseEngine::DestroyDynamicObjects() {
	//clear runtime objects on zone load
	RemoveAllDynamicPlacements();

	//destroying dynamic object resources/assets as well
	m_dynamicObjectRM->RemoveResourcesByState(IResource::LOADED_AND_CREATED); // Destroy LOADED_AND_CREATED only to avoid crash on pending resource event callbacks

	//delete dynamic object meshes
	MeshRM::Instance()->ResetMeshPool(MeshRM::RMP_ID_DYNAMIC);
}

void ClientBaseEngine::DeleteWorldGroupList() {
	if (!worldGroupList)
		return;

	for (POSITION posLoc = worldGroupList->GetHeadPosition(); posLoc != NULL;) {
		auto pWGO = (CWldGroupObj*)worldGroupList->GetNext(posLoc);
		if (pWGO)
			delete pWGO;
		pWGO = nullptr;
	}
	worldGroupList->RemoveAll();
	delete worldGroupList;
	worldGroupList = nullptr;
}

bool ClientBaseEngine::setMyOverrideAnimationByType(eAnimType animType, int animVer) {
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;
	pMO->setOverrideAnimationByType(animType, animVer);
	return true;
}

bool ClientBaseEngine::setMyOverrideAnimationSettings(const AnimSettings& animSettings) {
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;
	pMO->setOverrideAnimationSettings(animSettings);
	return true;
}

bool ClientBaseEngine::getMyOverrideAnimationSettings(AnimSettings& animSettings) {
	animSettings = AnimSettings();
	auto pMO = GetMovementObjMe();
	if (!pMO || !pMO->getSkeleton())
		return false;
	animSettings = pMO->getOverrideAnimationSettings();
	return true;
}

bool ClientBaseEngine::getMyPrimaryAnimationSettings(AnimSettings& animSettings) {
	animSettings = AnimSettings();
	auto pMO = GetMovementObjMe();
	if (!pMO || !pMO->getSkeleton())
		return false;
	auto animGlid = pMO->getSkeleton()->getAnimInUseGlid();
	auto animDirForward = pMO->getSkeleton()->isAnimInUseDirForward();
	// todo - add loopCtl
	animSettings = AnimSettings(animGlid, animDirForward);
	return true;
}

void ClientBaseEngine::UpdateAllEntityMovements() {
	// For All Entities
	for (POSITION posEnt = m_movObjList->GetHeadPosition(); posEnt != NULL;) {
		auto pMO = (CMovementObj*)m_movObjList->GetNext(posEnt);
		if (!pMO || !pMO->isTypeOPC() || !pMO->isNetworkEntity() || !pMO->getSkeleton() || P2pIsActive(pMO))
			continue;

		// Get Entity Interpolated Movement Data
		MovementData md = pMO->InterpolatedMovementData();

		// Move Entity Substitute New Position & Rotation
		moveEntitySubstitute(pMO, md.pos, md.dir);

		// Set Entity New Position & Rotation
		pMO->setBaseFramePosDirUp(md.pos, md.dir, md.up);

		// Update Attached Vehicle
		if (pMO->getVehiclePlacementId() != 0) {
			auto pDPO = this->GetDynamicObject(pMO->getVehiclePlacementId());
			Matrix44f mtxMovementObjToDynamicObject = InverseOf(pMO->getMtxDynamicObjectToMovementObject());
			Matrix44f mtxDynamicObject = mtxMovementObjToDynamicObject * pMO->getBaseFrameMatrix();
			if (pDPO)
				pDPO->SetWorldMatrix(mtxDynamicObject);

			if (pMO->getVehicleRigidBody()) {
				Matrix44f mtxDynObjToPhysics = InverseOf(pMO->getMtxPhysicsVehicleToDynamicObject());
				Matrix44f mtxPhysics = mtxDynObjToPhysics * mtxDynamicObject;
				pMO->getVehicleRigidBody()->SetTransformMetric(FeetToMeters(mtxPhysics));
			}
		}

#if 1
		// Apply Pending Stand Animation
		// ED-8586 - Slide Fixes
		auto animStand = pMO->getAnimStandPending();
		bool canStand = pMO->canStand(animStand);
		if (animStand && canStand) {
			auto pRS = pMO->getSkeleton();
			if (pRS)
				pRS->setPrimaryAnimationByAnimTypeAndVer(eAnimType::Stand);
		}
#endif
	}
}

bool ClientBaseEngine::UpdateEntityMoveData(MOVE_DATA moveData) {
	// Get Movement Object
	auto pMO = GetMovementObjByNetId(moveData.netTraceId);
	if (!pMO) {
		// Already Requested ?
		auto it = m_moNetTraceIdsRequested.find(moveData.netTraceId);
		if (it != m_moNetTraceIdsRequested.end())
			return false;
		m_moNetTraceIdsRequested.insert(moveData.netTraceId);

		// Request Movement Object Configuration (normal players)
		ATTRIB_DATA attribData;
		attribData.aeType = eAttribEventType::GetMovementObjectCfg;
		attribData.aeShort1 = moveData.netTraceId;
		// attribData.aeShort2 = isItem // server reads but never set ??!!
		attribData.aeInt2 = moveData.netTraceId; // server never reads but set anyway ??!!
		return m_multiplayerObj->QueueMsgAttribToServer(attribData);
	}

	// Movement Object Is Valid (don't delete it)
	pMO->setValidThisRound(true);

	// Stop Jumping
	auto pMC = pMO->getMovementCaps();
	if (pMC)
		pMC->SetJumping(false);

	// Skip Updates While P2P Animating
	if (P2pIsActive(pMO))
		return true;

	// Update Movement Object Movement Interpolator
	pMO->MovementInterpolatorUpdate(MovementData(moveData.pos, moveData.dir, moveData.up));

	// Movement Object Is Visible Only While Animation Is Valid
	bool newAnimDirForward1 = (moveData.animGlid1 > 0);
	GLID newAnimGlid1 = abs(moveData.animGlid1);
	GLID newAnimGlid2 = abs(moveData.animGlid2);
	pMO->setVisible(IS_VALID_GLID(newAnimGlid1) || IS_VALID_GLID(newAnimGlid2));

	// Update Movement Object Animations
	auto pRS = pMO->getSkeleton();
	if (!pRS)
		return false;

	// Get Animation Types
	auto pSAH1 = pRS->getSkeletonAnimHeaderByGlid(newAnimGlid1);
	eAnimType animType1 = pSAH1 ? pSAH1->m_animType : eAnimType::Invalid;
	auto pSAH2 = pRS->getSkeletonAnimHeaderByGlid(newAnimGlid2);
	eAnimType animType2 = pSAH2 ? pSAH2->m_animType : eAnimType::Invalid;

#if 0
	// ED-7832 - Player continues running on moving platforms even while standing
	// Set Primary Animation (don't stand until we 'can stand' to prevent slide)
	bool animStand = (animType1 == eAnimType::Stand);
	bool canStand = pMO->canStand(animStand);
	if (!animStand || canStand) {
		AnimSettings animSettings(newAnimGlid1, newAnimDirForward1);
		pRS->setPrimaryAnimationSettings(animSettings);
	}
#else
	// ED-8586 - Slide Fixes
	bool animStand = (animType1 == eAnimType::Stand);
	pMO->setAnimStandPending(animStand);
	if (!animStand) {
		AnimSettings animSettings(newAnimGlid1, newAnimDirForward1);
		pRS->setPrimaryAnimationSettings(animSettings);
	}
#endif

	// Set Secondary Animation
	if (pRS->getAnimInUseGlid(ANIM_SEC) != newAnimGlid2)
		pRS->setSecondaryAnimation(newAnimGlid2);

	// Update Armed Item Animations ?
	if (!pRS->areAllMeshesLoaded() || pRS->getEquippableItemCount() <= 0)
		return false;

	// Set Armed Item Animations
	std::vector<CArmedInventoryObj*> pAIOs;
	pRS->getAllEquippableItems(pAIOs);
	for (auto pAIO : pAIOs) {
		if (!pAIO)
			continue;
		auto pRS_aio = pAIO->getSkeleton();
		if (!pRS_aio)
			continue;
		GLID animGlid1 = pRS_aio->getAnimationGlidByAnimTypeAndVer(animType1);
		GLID animGlid2 = pRS_aio->getAnimationGlidByAnimTypeAndVer(animType2);
		AnimSettings animSettings(animGlid1, newAnimDirForward1);
		pRS_aio->setPrimaryAnimationSettings(animSettings);
		if (pRS_aio->getAnimInUseGlid(ANIM_SEC) != animGlid2)
			pRS_aio->setSecondaryAnimation(animGlid2);
	}

	return true;
}

bool ClientBaseEngine::PlayCharacter(int index, bool reconnecting) {
	if (index < 0)
		return false;

	if (!ClientIsEnabled())
		return false;

	m_multiplayerObj->m_currentCharacterInUse = index; // pending though

	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::SelectCharacter;
	attribData.aeShort1 = reconnecting ? 1 : 0;
	attribData.aeShort2 = 8; // VERSION EXPLOIT
	attribData.aeInt1 = index;
	attribData.aeInt2 = -1;
	return m_multiplayerObj->QueueMsgAttribToServer(attribData);
}

void ClientBaseEngine::RemoveAllTriggers() {
	RemoveAllDynamicObjectTriggers();
	m_wldObjTriggers.Clear();
}

void ClientBaseEngine::RemoveAllDynamicObjectTriggers() {
	m_dynObjTriggers.Clear();
}

void ClientBaseEngine::EnvironmentUpdate() {
	if (!m_environment || !m_environment->m_environmentTimeSystem || !m_environment->m_environmentTimeSystem->m_stormPtr || (m_environment->m_environmentTimeSystem->m_stormPtr->m_runtimeParticleId != 0))
		return;

	auto pEPS = m_particleRM->GetStockItemByIndex(m_environment->m_environmentTimeSystem->m_stormPtr->m_particleSystemRef);
	if (!pEPS)
		return;

	// if there is a storm system, and it uses a particle effect, and it doesn't have one, then make one
	UINT runtimeParticleId = m_particleRM->AddRuntimeEffect(pEPS->m_systemName);
	m_environment->m_environmentTimeSystem->m_stormPtr->m_runtimeParticleId = runtimeParticleId;
	m_particleRM->SetRuntimeEffectStatus(runtimeParticleId, 1, -1, -1);
}

// This function evaluates triggers against my avatar
void ClientBaseEngine::TriggerCheckCallback() {
	auto pMO = GetMovementObjMe();
	if (!pMO || !pMO->getBaseFrame())
		return;
	auto pos = pMO->getBaseFramePosition();
	EvaluateAllTriggers(pos, pMO);
}

bool ClientBaseEngine::ProcessScriptAttribute(CEXScriptObj* pSO) {
	// UPDATE NOTE: If the processing of these actions are updated to either
	// load a new texture into a CEXMeshObj or involve the removal of an existing
	// CEXMeshObj, the Editor's distribution.cpp module must be updated
	// (methods Mesh_*) to identify this object.
	switch (pSO->m_actionAttribute) {
		case LOAD_WATER_ZONES_DB:
		case LOAD_SAFE_ZONES:
		case LOAD_AI_COLLISION_DATABASE:
		case LOAD_HOUSING:
			// DEPRECATED
			break;

		case OPEN_SCENE: {
			std::string exgFileName = pSO->m_miscString.GetString();
			if (!LoadScene(exgFileName))
				return false;
			m_zoneFileName = pSO->m_miscString;
			return true;
		} break;

		case LOAD_GLOBAL_ATTACHMENTDB: {
			std::string fileName = pSO->m_miscString.GetString();
			return (LoadGlobalAttachmentDB(fileName) != FALSE);
		} break;

		default:
			return Engine::ProcessScriptAttribute(pSO);
	}

	return true;
}

void ClientBaseEngine::InitAllDatabaseEntities() {
	RebuildTraceNumber(); // rebuild trace numbers
}

void ClientBaseEngine::ExitAllTriggers() {
	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;

	// exit all triggers if destroying the entities
	ForEachTrigger([this, pMO](CTriggerObject* pTO) {
		auto triggerState = pTO->ExitTrigger();
		if (triggerState == eTriggerState::MovedOutside) {
			Vector3f pos(FLT_MAX, FLT_MAX, FLT_MAX);
			EvaluateTrigger(pTO, triggerState, pos, pMO, false);
		}
	});
}

bool ClientBaseEngine::GenAndMount(
	CMovementObj* pMO_host,
	int aiConfig,
	int mountType,
	bool bLocal,
	CMovementObj** ppMO_out) {
	// client only?
	if (pMO_host->getPosses())
		return false;

	if (aiConfig < 0)
		return false;

	CAIObj* pAIO = m_AIOL->GetByIndex(aiConfig);
	if (!pAIO)
		return false;

	Matrix44f transformMatrix = pMO_host->getBaseFrameMatrix();

	eActorControlType controlType = eActorControlType::NPC;
	bool networkEntity = ClientIsEnabled();
	if (ClientIsEnabled()) {
		controlType = eActorControlType::OPC;
		networkEntity = true;
	}

	if (pMO_host->isTypePC()) {
		networkEntity = false;
		controlType = eActorControlType::NPC;
	}

	// Create New Movement Object (only OPC & NPC)
	CMovementObj* pMO_new;
	bool ok = CreateMovementObject(
		aiConfig,
		transformMatrix,
		pAIO->m_entityCfgDBindexRef,
		controlType,
		0,
		networkEntity,
		false, // add at tail (index!=0)
		false, // don't spawn at server
		-1,
		&pMO_new,
		NULL,
		bLocal,
		"");
	if (!ok) {
		LogError("CreateMovementObject(GEN_AND_MOUNT) FAILED");
		return false;
	}

	if (!pMO_new)
		return false;

	pMO_host->mount(pMO_new, mountType);
	pMO_new->characterAnimationManagement(pMO_new->getSkeleton(), true);

	// Copy New Movement Object To Given Pointer
	if (ppMO_out)
		*ppMO_out = pMO_new;

	return true;
}

BOOL ClientBaseEngine::DeMount(CMovementObj* pMO) {
	if (!pMO || !pMO->getPosses())
		return FALSE;

	auto pMC = pMO->getMovementCaps();
	if (!pMC)
		return FALSE;

	int traceNumber = pMO->getPosses()->getTraceNumber();
	pMC->SetPosLast(pMO->getBaseFramePosition());

	POSITION posLast = NULL;
	for (POSITION ePos = m_movObjList->GetHeadPosition(); (posLast = ePos) != NULL;) {
		auto pMO_del = (CMovementObj*)m_movObjList->GetNext(ePos);
		if (pMO_del->getTraceNumber() == traceNumber) {
			DestroyMovementObject(pMO_del);
			break;
		}
	}

	return TRUE;
}

bool ClientBaseEngine::updateAnimations(RuntimeSkeleton* pRS, const Matrix44f& matrix, bool firstPersonMode, bool updateParticleAndShadow) {
	if (!pRS)
		return false;

	// Update Skeleton Animation
	pRS->updateAnimations();

	// Update Particles And Shadows ?
	if (updateParticleAndShadow) {
		SkeletalParticleEmitterCallback(matrix, pRS, false);
		if (pRS->isShadowEnabled()) {
			Vector3f shdPos = matrix.GetRowW().SubVector<3>();
			pRS->calculateShadow(shdPos, m_pCollisionObj);
		}
	}

	// Armed Inventory Objects ?
	if (!pRS->getEquippableItemCount())
		return true;

	// Update Animations And Particles For All Armed Inventory Objects
	std::vector<CArmedInventoryObj*> pAIOs;
	pRS->getAllEquippableItems(pAIOs);
	for (auto pAIO : pAIOs) {
		if (!pAIO)
			continue;
		auto pRS_aio = pAIO->getSkeleton(firstPersonMode ? eVisualType::FirstPerson : eVisualType::ThirdPerson);
		if (!pRS_aio)
			continue;
		pRS_aio->updateAnimations();
		if (updateParticleAndShadow) {
			Matrix44f aBoneMatrix = pRS->getBoneMatrixByIndex(pAIO->getAttachedBoneIndex());
			aBoneMatrix = aBoneMatrix * matrix;
			SkeletalParticleEmitterCallback(aBoneMatrix, pRS_aio, false);
		}
	}

	return true;
}

BOOL ClientBaseEngine::LoadReferenceLibrary(const std::string& fileName) {
	SafeDeleteList(m_libraryReferenceDB);

	// Unload All Textures From Memory
	m_pTextureDB->TextureMapUnloadAll();

	// delete the library meshes.....they're streamed in again on each level load
	MeshRM::Instance()->ResetMeshPool(MeshRM::RMP_ID_LIBRARY);

	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), m_libraryReferenceDB, m_engineLogger)) {
		LogError("SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	}

	return TRUE;
}

bool ClientBaseEngine::EvaluateAllTriggers(const Vector3f& pos, CMovementObj* pMO) {
	ForEachTrigger([this, &pos, pMO](CTriggerObject* pTO) {
		auto triggerState = pTO->EvaluatePosition(pos);
		if (triggerState != eTriggerState::TriggerOnce && triggerState != eTriggerState::Deleted)
			EvaluateTrigger(pTO, triggerState, pos, pMO, true);
	});

	return true;
}

bool ClientBaseEngine::DynamicObjectAnimationManagementCallback(CDynamicPlacementObj* pDPO) {
	if (!pDPO)
		return false;

	auto pRS = pDPO->getSkeleton();
	if (!pRS)
		return false;

	// Animate Dynamic Object
	AnimSettings animSettings;
	pDPO->getPrimaryAnimationSettings(animSettings);
	pDPO->setPrimaryAnimationSettings(animSettings);

	// Armed Inventory Objects ?
	if (!pRS->getEquippableItemCount())
		return true;

	// Animate All Armed Inventory Objects
	std::vector<CArmedInventoryObj*> pAIOs;
	pRS->getAllEquippableItems(pAIOs);
	for (const auto& pAIO : pAIOs) {
		if (!pAIO)
			continue;
		auto pRS_aio3 = pAIO->getSkeleton(eVisualType::ThirdPerson);
		if (!pRS_aio3)
			continue;
		pAIO->getPrimaryAnimationSettings(animSettings);
		// ED-7903 - Fixing DO Animation Zero Glid Broke Accessory Animations
		if (animSettings.isValid())
			pRS_aio3->setPrimaryAnimationSettings(animSettings);
	}

	return true;
}

bool ClientBaseEngine::EntityAnimationManagementCallback(CMovementObj* pMO) {
	if (!pMO)
		return false;

	auto pRS = pMO->getSkeleton();
	if (!pRS)
		return false;

	// Animate Movement Object
	pMO->characterAnimationManagement(pRS, true);

	// Armed Inventory Objects ?
	if (!pRS->getEquippableItemCount())
		return true;

	// Animate All Armed Inventory Objects
	std::vector<CArmedInventoryObj*> pAIOs;
	pRS->getAllEquippableItems(pAIOs);
	for (const auto& pAIO : pAIOs) {
		if (!pAIO || !IS_KANEVA_GLID(pAIO->getGlobalId()))
			continue;
		auto pRS_aio1 = pAIO->getSkeleton(eVisualType::FirstPerson);
		if (pRS_aio1)
			pMO->characterAnimationManagement(pRS_aio1, false);
		auto pRS_aio3 = pAIO->getSkeleton(eVisualType::ThirdPerson);
		if (pRS_aio3)
			pMO->characterAnimationManagement(pRS_aio3, false);
	}

	return true;
}

void ClientBaseEngine::Callback() {
	if (m_multiplayerObj->m_clientStatusFromServer != 0) {
		// server informing of problem
		if (m_multiplayerObj->m_clientStatusFromServer == 1) {
			// inform the user of the login failure and redisplay the login dialog
			m_pTextReadoutObj->AddLine(m_multiplayerObj->m_connectionErrorCode, eChatType::System);
			m_multiplayerObj->m_clientStatusFromServer = 0;
		}
	}

	if (!ClientIsEnabled())
		return;

	// Send Msg Log To Server ?
	if (m_multiplayerObj->m_sendMsgLogToServer) {
		// Send Message Log To Server
		if (m_multiplayerObj->SendMsgLogToServer(
				m_multiplayerObj->m_userName.GetString(),
				m_multiplayerObj->m_passWord.GetString()))
			m_multiplayerObj->m_sendMsgLogToServer = false;
	}

	// ED-7834 - Warn On Stalling
	Timer timer;
	TimeMs timeMs = 0;

	// Handle All Messages Coming From Server
	BYTE* pData;
	NETID netIdFrom;
	size_t dataSize;
	bool bDisposeMessage;
	size_t msgs = 0;
	while (m_multiplayerObj->MsgRx(pData, dataSize, netIdFrom)) {
		if (!pData || !dataSize)
			continue;
		HandleMsgs(pData, netIdFrom, dataSize, bDisposeMessage);
		if (bDisposeMessage)
			delete pData;

		// Abort On Stalling
		++msgs;
		timeMs = timer.ElapsedMs();
		if (timeMs > 200)
			break;
	}

	// ED-7834 - Warn On Stalling
	if (timeMs > 200) {
		LogWarn("SLOW(" << FMT_TIME << timeMs << "ms) - MsgRx() msgs=" << msgs);
	}

	// Periodically Send Message Update To Server
	SendMsgUpdateToServer();

	// Periodically Send Message Move To Server (my avatar position)
	SendMsgMoveToServer();
}

void ClientBaseEngine::RebuildTraceNumber() {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	if (!m_pWOL)
		return;

	m_globalTraceNumber = 0;

	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) {
		auto pMO = (CMovementObj*)m_movObjRefModelList->GetNext(posLoc);
		if (!pMO)
			continue;
		pMO->setTraceNumber(m_globalTraceNumber++);
	}

	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) {
		auto pWO = (CWldObject*)m_pWOL->GetNext(posLoc);
		if (!pWO)
			continue;
		pWO->m_identity = m_globalTraceNumber++;
	}
}

IDirect3DDevice9* ClientBaseEngine::GetD3DDevice() {
	return 0;
}

void ClientBaseEngine::ContentMetadataCacheSync(const std::vector<GLID>& glids) {
	// Parse Valid & Ugc Glids
	std::vector<GLID> glidsValid;
	std::vector<GLID> glidsUgc;
	for (const auto& glid : glids) {
		if (!IS_VALID_CONTENTMETADATA_GLID(glid))
			continue;
		glidsValid.push_back(glid);
		if (IS_UGC_GLID(glid))
			glidsUgc.push_back(glid);
	}

	// Cache All Valid Glids Metadata Synchronously Now
	auto pCS = ContentService::Instance();
	if (pCS)
		pCS->ContentMetadataCacheSync(glidsValid);

	// Cache All UGC Item Data Into Global Inventory List
	for (const auto& glid : glidsUgc)
		UGCItemDataCacheSync(glid);
}

CGlobalInventoryObj* ClientBaseEngine::UGCItemDataCacheSync(const GLID& glid) {
	// Get Already Cached Content Metadata (skip if not already cached)
	// Should already be cached from CacheUGCItemData(vector<GLID>) Above !
	ContentMetadata md(glid);
	if (!md.IsValid())
		return NULL;

	// Get Global Inventory List
	CGlobalInventoryObjList* pGIOL = CGlobalInventoryObjList::GetInstance();
	if (!pGIOL)
		return NULL;

	// Item Already In Global Inventory List ?
	CGlobalInventoryObj* pGIO = pGIOL->GetByGLID(md.getBaseGlobalId());
	CGlobalInventoryObj* pGIO_exist = pGIOL->GetByGLID(glid);
	if (pGIO_exist)
		return pGIO_exist;

	// All Kaneva Glids Should Have Base Glids
	if (!pGIO && md.getBaseGlobalId() != 0 && IS_KANEVA_GLID(md.getBaseGlobalId())) {
		LogWarn("glid<" << glid << "> references non-existent UGC template item baseGlid=" << md.getBaseGlobalId());
	}

	// Cache Item Into Global Inventory List
	std::string texFileName = md.getPrimaryTexturePath();
	std::string texUrl = md.getPrimaryTextureUrl();
	if (pGIO && pGIO->m_itemData) {
		CGlobalInventoryObj* pGIO_custom = new CGlobalInventoryObj();
		if (pGIO_custom) {
			// Clone the base item
			pGIO->m_itemData->CloneFromBaseGLID(&pGIO_custom->m_itemData, pGIO->m_itemData, glid);

			if (pGIO_custom->m_itemData) {
				// Assign Same As Glid Metadata
				pGIO_custom->m_itemData->m_itemName = md.getItemName().c_str();
				pGIO_custom->m_itemData->m_itemDescription = md.getItemDesc().c_str();
				pGIO_custom->m_itemData->m_globalID = glid;
				pGIO_custom->m_itemData->m_passGroup = md.getPassGroups(); // drf - added

				CMaterialObject* material = new CMaterialObject();
				pGIO_custom->m_itemData->m_materialOverride = material;

				// Set up custom material
				ZeroMemory(&material->m_useMaterial, sizeof(material->m_useMaterial));
				ZeroMemory(&material->m_baseMaterial, sizeof(material->m_baseMaterial));
				material->m_useMaterial.Diffuse = D3DXCOLOR(1, 1, 1, 1);
				material->m_useMaterial.Ambient = D3DXCOLOR(1, 1, 1, 1);

				// Fill in first texture of custom material
				if (ValidTextureFileName(texFileName)) {
					// Add Texture To Texture Database
					DWORD nameHash = INVALID_TEXTURE_NAMEHASH;
					m_pTextureDB->TextureAdd(nameHash, texFileName, "", texUrl, 0, eTexturePath::CustomTexture);
					if (!ValidTextureNameHash(nameHash)) {
						LogError("AddTextureToDatabase(BASE_GLID_YES) FAILED - glid<" << glid << "> texName='" << texFileName << "' texUrl='" << texUrl << "'");
					} else {
						material->m_texNameHash[0] = nameHash;
						material->m_texFileName[0] = texUrl; // hacky -- just need to pass in a texture name "template" instead of a material, will fix later
					}
				}

				// Store the new item in global inventory list
				pGIOL->AddTail(pGIO_custom);

				return pGIO_custom;
			} else {
				delete pGIO_custom;
			}
		}
	} else {
		// baseGLID not zero but base item does not exist: possibly UGC DO
		if (IS_DYNOBJ_GLID(md.getBaseGlobalId())) {
			pGIO = new CGlobalInventoryObj();
			CItemObj* pIO = new CItemObj();

			pIO->m_globalID = md.getBaseGlobalId();
			pIO->m_baseGlid = GLID_INVALID;
			pIO->m_useType = md.getUseType();
			pIO->m_miscUseValue = md.getUseValue();
			pIO->m_passGroup = md.getPassGroups(); // drf - added
			pIO->m_itemName = md.getItemName().c_str();
			pIO->m_itemDescription = md.getItemDesc().c_str();
			pIO->m_marketCost = 0; //temp
			pIO->m_sellingPrice = 0; //temp
			pIO->m_disarmable = TRUE;
			pIO->m_skillUseRef = 0;
			pIO->m_minimumLevel = 0;
			pIO->m_nonDrop = FALSE;
			pIO->m_nonTransferable = FALSE;
			pIO->m_destroyOnUse = FALSE;
			pIO->m_stackable = FALSE; //temp
			pIO->m_requiredSkillType = 0;
			pIO->m_requiredSkillLevel = 0;
			pIO->m_visualRef = 0;
			pIO->ParseExclusionGroupsCSV(md.getExclusionGroups());

			pGIO->m_itemData = pIO;
			pGIOL->AddTail(pGIO);

			CGlobalInventoryObj* pGIO_custom = new CGlobalInventoryObj();

			// Clone the base item
			pGIO->m_itemData->CloneFromBaseGLID(&pGIO_custom->m_itemData, pGIO->m_itemData, glid);

			// Override fields with UGC data
			pGIO_custom->m_itemData->m_itemName = md.getItemName().c_str();
			pGIO_custom->m_itemData->m_itemDescription = md.getItemDesc().c_str();
			pGIO_custom->m_itemData->m_globalID = glid;
			pGIO_custom->m_itemData->m_passGroup = md.getPassGroups(); // drf - added

			CMaterialObject* material = new CMaterialObject();
			pGIO_custom->m_itemData->m_materialOverride = material;

			// Set up custom material
			ZeroMemory(&material->m_useMaterial, sizeof(material->m_useMaterial));
			ZeroMemory(&material->m_baseMaterial, sizeof(material->m_baseMaterial));
			material->m_useMaterial.Diffuse = D3DXCOLOR(1, 1, 1, 1);
			material->m_useMaterial.Ambient = D3DXCOLOR(1, 1, 1, 1);

			// Fill in first texture of custom material
			if (ValidTextureFileName(texFileName)) {
				// Add Texture To Texture Database
				DWORD nameHash = INVALID_TEXTURE_NAMEHASH;
				m_pTextureDB->TextureAdd(nameHash, texFileName, "", texUrl, 0, eTexturePath::CustomTexture);
				if (!ValidTextureNameHash(nameHash)) {
					LogError("AddTextureToDatabase(UGC_DO) FAILED - glid<" << glid << "> texName='" << texFileName << "' texUrl='" << texUrl << "'");
				} else {
					material->m_texNameHash[0] = nameHash;
					material->m_texFileName[0] = texUrl; // hacky -- just need to pass in a texture name "template" instead of a material, will fix later
				}
			}

			// Store the new item in global inventory list
			pGIOL->AddTail(pGIO_custom);

			return pGIO_custom;

		} else if (md.getBaseGlobalId() == 0) {
			// For items such as equippable items, sounds which are UGC but have no base glid
			CGlobalInventoryObj* pGIO_custom = new CGlobalInventoryObj();
			CItemObj* pIO = new CItemObj();

			pIO->m_globalID = glid;
			pIO->m_baseGlid = 0;
			pIO->m_useType = md.getUseType();
			pIO->m_miscUseValue = md.getUseValue();
			pIO->m_passGroup = md.getPassGroups(); // drf - added
			pIO->m_itemName = md.getItemName().c_str();
			pIO->m_itemDescription = md.getItemDesc().c_str();
			pIO->m_marketCost = 0; //temp
			pIO->m_sellingPrice = 0; //temp
			pIO->m_disarmable = TRUE;
			pIO->m_skillUseRef = 0;
			pIO->m_minimumLevel = 0;
			pIO->m_nonDrop = FALSE;
			pIO->m_nonTransferable = FALSE;
			pIO->m_destroyOnUse = FALSE;
			pIO->m_stackable = FALSE; //temp
			pIO->m_requiredSkillType = 0;
			pIO->m_requiredSkillLevel = 0;
			pIO->m_visualRef = 0;
			pIO->ParseExclusionGroupsCSV(md.getExclusionGroups());

			// handle equippable items
			if (pIO->m_useType == USE_TYPE_EQUIP) { // TEMP
				pIO->m_attachmentRefList = new CAttachmentRefList;
				pIO->m_attachmentRefList->AddHead(new CAttachmentRefObj(pIO->m_miscUseValue));
			}

			pGIO_custom->m_itemData = pIO;

			CMaterialObject* material = new CMaterialObject();
			pGIO_custom->m_itemData->m_materialOverride = material;

			// Set up custom material
			ZeroMemory(&material->m_useMaterial, sizeof(material->m_useMaterial));
			ZeroMemory(&material->m_baseMaterial, sizeof(material->m_baseMaterial));
			material->m_useMaterial.Diffuse = D3DXCOLOR(1, 1, 1, 1);
			material->m_useMaterial.Ambient = D3DXCOLOR(1, 1, 1, 1);

			// Fill in first texture of custom material
			if (ValidTextureFileName(texFileName)) {
				// Add Texture To Texture Database
				DWORD nameHash = INVALID_TEXTURE_NAMEHASH;
				m_pTextureDB->TextureAdd(nameHash, texFileName, "", texUrl, 0, eTexturePath::CustomTexture);
				if (!ValidTextureNameHash(nameHash)) {
					LogError("AddTextureToDatabase(BASE_GLID_NO) FAILED - glid<" << glid << "> texName='" << texFileName << "' texUrl='" << texUrl << "'");
				} else {
					material->m_texNameHash[0] = nameHash;
					material->m_texFileName[0] = texUrl; // hacky -- just need to pass in a texture name "template" instead of a material, will fix later
				}
			}

			// Store the new item in global inventory list
			pGIOL->AddTail(pGIO_custom);

			return pGIO_custom;
		}
	}

	return nullptr;
}

bool ClientBaseEngine::SetEquippableAnimation(RuntimeSkeleton* pRS, const GLID& accessoryGlid, const GLID& animGlid, bool updateServer) {
	if (!pRS)
		return false;

	// Find In Movement Object's Armed Inventory
	bool foundIt = false;
	std::vector<ItemAnimation> itemAnims;
	std::vector<CArmedInventoryObj*> pAIOs;
	pRS->getAllEquippableItems(pAIOs);
	for (const auto& pAIO : pAIOs) {
		if (!pAIO)
			continue;

		auto pRS_aio = pAIO->getSkeleton();
		if (!pRS_aio)
			continue;

		AnimSettings animSettings(animGlid);

		// If I'm not updating the server, then it's because these glids have been given to me
		// by something that wants to set the state locally, for example a game server sendin
		// entity cfg data.  In that case, just do it.
		if (pRS_aio && pAIO->getGlobalId() == accessoryGlid) {
			// set the animation here
			foundIt = true;
			pAIO->setPrimaryAnimationSettings(animSettings);
		} else {
			// get the animation that's on this (unrelated) item
			pAIO->getPrimaryAnimationSettings(animSettings);
		}

		// all the items are listed in the itemAnims vector, so that
		// the server gets a complete list and can send a complete
		// list to other clients
		if (updateServer && !IS_LOCAL_GLID(animSettings.m_glid)) {
			ItemAnimation ia;
			ia.m_glidAnim = animSettings.m_glid;
			ia.m_glidItem = pAIO->getGlobalId();
			itemAnims.push_back(ia);
		}
	}

	// Send the current set of animations up to the server for distribution.
	if (!itemAnims.empty()) {
		// Get My Avatar
		auto pMO_me = GetMovementObjMe();
		if (pMO_me->getSkeleton() == pRS) {
			IEvent* e = new UpdateEquippableAnimationEvent(pMO_me->getNetTraceId(), itemAnims);
			e->AddTo(SERVER_NETID);
			m_dispatcher.QueueEvent(e);
		}
	}

	if (foundIt)
		return true;

	// Otherwise check pending list
	return pRS->updatePendingEquippableItemAnimation(accessoryGlid, animGlid);
}

bool ClientBaseEngine::SetEquippableAnimation(CDynamicPlacementObj* pDPO, const GLID& accessoryGLID, const GLID& animationGLID) {
	return pDPO ? SetEquippableAnimation(pDPO->getSkeleton(), accessoryGLID, animationGLID, false) : false;
}

bool ClientBaseEngine::SetEquippableAnimation(CMovementObj* pMO, const GLID& accessoryGLID, const GLID& animationGLID, bool updateServer) {
	return pMO ? SetEquippableAnimation(pMO->getSkeleton(), accessoryGLID, animationGLID, updateServer) : false;
}

bool ClientBaseEngine::RepositionEquippableItem(CDynamicPlacementObj* pDPO, const GLID& glidAccessory, const Vector3f& ptBBoxRelativePosition, const Vector3f& vOffset, const Vector3f& vEulerZXY, const Vector3f& vScale) {
	if (!pDPO)
		return false;

	auto pRS = pDPO->getSkeleton();
	if (!pRS)
		return false;

	Vector3f ptPosition = pRS->getLocalBoundingBox().GetMin() + pRS->getLocalBoundingBox().GetSize() * (Vector3f(1) + ptBBoxRelativePosition) * 0.5f + vOffset;

	return pRS->updateEquippableItemTransform(glidAccessory, ptPosition, vEulerZXY, vScale, true);
}

size_t ClientBaseEngine::GetNumHumanPlayersInRuntimeList() const {
	POSITION pos = m_movObjList->GetHeadPosition();
	size_t humanCount = 0;
	while (pos) {
		auto pMO = (CMovementObj*)m_movObjList->GetNext(pos);
		if (pMO && (pMO->isTypePC() || pMO->isTypeOPC()))
			++humanCount;
	}
	return humanCount;
}

bool ClientBaseEngine::DownloadFile(const std::string& strURL, const std::string& filePath) {
	GBPO gbpo;
	gbpo.url = strURL;
	gbpo.resultFile = filePath;
	return GetBrowserPage(gbpo);
}

BOOL ClientBaseEngine::LoadGlobalAttachmentDB(const std::string& fileName) {
	CArmedInventoryList* tempList;
	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), tempList, m_engineLogger)) {
		LogError("SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	}

	// LoadFromAPD transfers ownership of the pointers to m_equippableRM.
	// That is why RemoveAll is called before SafeDelete below.
	m_equippableRM->LoadFromAPD(tempList);
	tempList->RemoveAll();
	tempList->SafeDelete(); // Should do nothing as list was just emptied. In most cases, SafeDelete() needs to be called before delete.
	delete tempList;
	return TRUE;
}

void ClientBaseEngine::MoveDynamicObjectTriggers(int placementId, const Vector3f& pos, const Vector3f& dir) {
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO)
		return;

	for (const auto& it : pDPO->m_ClientEngineState.GetTriggers()) {
		CTriggerObject* pTO = it.second;
		if (!pTO)
			continue;
		pTO->SetTransform(pos, dir);
	}
}

void ClientBaseEngine::DestroyAllParticleInterfaces() {
	if (!m_particleRM)
		return;
	m_particleRM->ClearRuntimeEffects();
	delete m_particleRM;
	m_particleRM = NULL;
}

bool ClientBaseEngine::AttachPathFinder(ObjectType targetType, int netId, float destX, float destY, float destZ, float destRX, float destRY, float destRZ, float speed, int motionStyle, int timeout, int recalcInterval, int aiMode, bool noPhysics, bool noPlayerControl) {
	// check if target is available
	IKinematicEntity* pTargetObj = NULL;
	switch (targetType) {
		case ObjectType::MOVEMENT: {
			CMovementObj* pMovObj = m_movObjList->getObjByNetTraceId(netId);
			if (pMovObj) {
				pTargetObj = new KEMovementObject(this, netId);
			}
			break;
		}
	}

	if (!pTargetObj)
		return false;

	UINT64 targetIdCombined = (((UINT64)targetType) << 32) + (UINT)netId;
	std::map<UINT64, IPathFinder*>::iterator iter = m_pathFinderMap.find(targetIdCombined);
	if (iter != m_pathFinderMap.end()) {
		// replace existing path finder
		iter->second->Cancel(IPathFinder::CANCEL_BY_GAME);
		delete iter->second;
		m_pathFinderMap.erase(iter);
	}

	IPathFinder* pPathFinder = NULL;

	switch (aiMode) {
		case PF_SIMPLE:
			pPathFinder = new SimplePathFinder();
			break;
	}

	if (pPathFinder) {
		if (pPathFinder->Start(
				pTargetObj,
				Vector3f(destX, destY, destZ),
				Vector3f(destRX, destRY, destRZ),
				speed,
				recalcInterval,
				motionStyle,
				!noPhysics,
				timeout,
				!noPlayerControl)) {
			m_pathFinderMap.insert(std::make_pair(targetIdCombined, pPathFinder));
			return true;
		} else {
			delete pPathFinder;
		}
	}

	return false;
}

bool ClientBaseEngine::DetachPathFinder(ObjectType targetType, int targetId, bool bCancel, int cancelReason) {
	UINT64 targetIdCombined = (((UINT64)targetType) << 32) + (UINT)targetId;
	std::map<UINT64, IPathFinder*>::iterator iter = m_pathFinderMap.find(targetIdCombined);
	if (iter != m_pathFinderMap.end()) {
		if (bCancel && iter->second->GetState() == IPathFinder::ENROUTE)
			iter->second->Cancel(cancelReason);
		delete iter->second;
		m_pathFinderMap.erase(iter);
		return true;
	}

	return false;
}

IPathFinder* ClientBaseEngine::GetPathFinder(ObjectType targetType, int targetId) {
	UINT64 targetIdCombined = (((UINT64)targetType) << 32) + (UINT)targetId;
	std::map<UINT64, IPathFinder*>::iterator iter = m_pathFinderMap.find(targetIdCombined);
	if (iter != m_pathFinderMap.end())
		return iter->second;
	return NULL;
}

void ClientBaseEngine::ClearAllPathFinders(bool bCancel, int cancelReason) {
	for (auto keyValuePair : m_pathFinderMap) {
		if (bCancel && keyValuePair.second->GetState() == IPathFinder::ENROUTE)
			keyValuePair.second->Cancel(cancelReason);
		delete keyValuePair.second;
	}
	m_pathFinderMap.clear();
}

bool ClientBaseEngine::setOverheadLabels(CMovementObj* pMO) {
	// Assume My Avatar ?
	if (!pMO)
		pMO = GetMovementObjMe();
	if (!pMO || !pMO->getSkeleton())
		return false;

	// Reset Movement Object Overhead Labels
	auto pMORSOL = pMO->getSkeleton()->getOverheadLabels();
	if (!pMORSOL)
		return false;
	pMORSOL->reset();

	// Get Name, Title, & Clan Strings
	std::string nameStr = pMO->getDisplayName();
	std::string titleStr = pMO->getTitle();
	std::string clanStr = pMO->getClanName();

	// Append PassGroups To Clan In DevMode
	auto pICE = IClientEngine::Instance();
	bool devMode = (pICE && pICE->AppDevMode());
	if (devMode) {
		StrAppend(clanStr, pMO->PassGroupStr());
	}

	// Add Avatar Overhead Name Text (color defined by movement object)
	const Vector3d colorName(pMO->getNameColor().r, pMO->getNameColor().g, pMO->getNameColor().b);
	Vector3d nameColor = colorName;
	pMORSOL->addLabel(nameStr, .014f, nameColor, SkeletonLabels::SL_POS_NAME);

	// Add Avatar Overhead Title Text
	const Vector3d colorTitle(0.9, 0.45, 0.0);
	Vector3d titleColor = colorTitle;
	pMORSOL->addLabel(titleStr, .008f, titleColor, SkeletonLabels::SL_POS_TITLE);

	// Add Avatar Overhead Clan Text (color red in DevMode)
	const Vector3d colorClan(1.0, 1.0, 0.5);
	const Vector3d colorClanDev(1.0, 0.0, 0.0);
	Vector3d clanColor = (devMode ? colorClanDev : colorClan);
	pMORSOL->addLabel(clanStr, .008f, clanColor, SkeletonLabels::SL_POS_CLAN);

	return true;
}

// Get dynamic placement object By placement ID
CDynamicPlacementObj* ClientBaseEngine::GetDynamicObject(int placementId) {
	return m_dynamicPlacementManager.LookUpByPlacementId(placementId);
}

// Get dynamic placement object By placement ID (const)
const CDynamicPlacementObj* ClientBaseEngine::GetDynamicObject(int placementId) const {
	return m_dynamicPlacementManager.LookUpByPlacementId(placementId);
}

// Remove and dispose dynamic placement object By placement ID
bool ClientBaseEngine::RemoveDynamicObject(int placementId) {
	CDynamicPlacementObj* pDPO = m_dynamicPlacementManager.LookUpByPlacementId(placementId);
	if (!pDPO)
		return false;

	if (pDPO->HasLight())
		LightListDirty();

	// Remove and dispose
	pDPO->ForEachRigidBody([this](Physics::RigidBodyStatic* pRigidBody) {
		this->m_pUniverse->Remove(pRigidBody);
	});

	m_dynamicPlacementManager.Remove(pDPO);
	return true;
}

// Remove and dispose all dynamic object placements
void ClientBaseEngine::RemoveAllDynamicPlacements() {
	IterateObjects([this](CDynamicPlacementObj* pDPO) {
		pDPO->ForEachRigidBody([this](Physics::RigidBodyStatic* pRigidBody) { this->m_pUniverse->Remove(pRigidBody); });
	});

	m_dynamicPlacementManager.RemoveAll();

	LightListDirty();
}

// TODO: remove this function and always index by placement ID.
CDynamicPlacementObj* ClientBaseEngine::GetDynamicObjectByIndex(size_t index) {
	return m_dynamicPlacementManager.LookUpByIndex(index);
}

std::unique_ptr<CDynamicPlacementObj> ClientBaseEngine::CreateDynamicObjectWithPlacementId(GLID globalId, int placementId, bool fromMovementObj) {
	return std::make_unique<CDynamicPlacementObj>(globalId, placementId, fromMovementObj);
}

CDynamicPlacementObj* ClientBaseEngine::PlaceDynamicObject(const PlaceDynamicObjectArgs& args) {
	std::unique_ptr<CDynamicPlacementObj> pDPO = CreateDynamicObjectWithPlacementId(args.globalId, args.placementId, args.fromMovementObj);
	if (pDPO == nullptr) {
		LogError("CreateDynamicObjectWithPlacementId() FAILED - glid=" << args.globalId << " placementId=" << args.placementId);
		return nullptr;
	}

	pDPO->m_playerId = args.playerId;
	pDPO->SetIsAttachable(args.attachable);
	pDPO->SetIsInteractive(args.interactive);
	pDPO->SetMediaSupported(args.mediaSupported);
	pDPO->SetCollisionEnabled(true); // for some unknown reason all 3d app objects are marked as not having collision.
	pDPO->SetIsDerivable(args.isDerivable);
	pDPO->SetAssetId(args.assetId);
	pDPO->SetInventorySubType(args.invSubType);
	pDPO->SetTextureUrl(args.textureUrl);
	pDPO->SetGameItemId(args.gameItemId);
	pDPO->m_minObjectSpace = args.min;
	pDPO->m_maxObjectSpace = args.max;

	pDPO->Relocate(args.position, 0, args.direction, args.slide, 0);

	if (IS_LOCAL_GLID(args.globalId))
		pDPO->GetBaseObj(); //immediately get local objects.

	if (args.scaleFactor != 1.0f)
		pDPO->SetScale(args.scaleFactor, args.scaleFactor, args.scaleFactor);

	CDynamicPlacementObj* pDPOReturn = pDPO.get();
	if (!m_dynamicPlacementManager.Add(std::move(pDPO)))
		return nullptr;

	return pDPOReturn;
}

CDynamicPlacementObj* ClientBaseEngine::PlaceActorAsDynamicObject(const PlaceDynamicObjectArgs& args) {
	// Check unsupported arguments
	assert((args.globalId == 0 || args.globalId == GLID_PAPERDOLL_NPC) &&
		   args.attachable == false &&
		   args.interactive == false &&
		   args.mediaSupported == false &&
		   args.isDerivable == false &&
		   args.scaleFactor == 1.0f &&
		   args.assetId == 0 &&
		   args.textureUrl.empty());

	assert(args.placementId != 0);
	if (args.placementId == 0) {
		LogError("Placement ID must not be 0");
		return nullptr;
	}

	// Rest of the dynamic object will be initialized when it's used and the movement object is ready

	// Make a copy of the arguments so that we can modify globalId
	PlaceDynamicObjectArgs po_args(args);
	po_args.globalId = ResourceManager::AllocateLocalGLID();
	; // Local global ID
	po_args.fromMovementObj = true;

	CDynamicPlacementObj* placedObj = PlaceDynamicObject(po_args);
	if (placedObj) {
		placedObj->FlushBoundBox();

		// InitPlacement() will be called once CMovementObj is spawned on this NPC. Currently server is delaying the
		// NPC spawn message until progress menu is closed. Notify progress menu now to avoid the dead lock.

		// Notify progress menu that InitPlacement()/DynamicObjectInitializedEvent will be delayed on this placement
		auto pEvent = std::make_unique<DynamicObjectInitDelayedEvent>(placedObj->GetPlacementId());
		m_dispatcher.QueueEvent(pEvent.release());
	}

	return placedObj;
}

CMovementObj* ClientBaseEngine::SpawnStaticMovementObject(
	int siType, // SERVER_ITEM_TYPE
	const std::string& name,
	const CharConfig* pCharConfig,
	const std::vector<GLID>& glidsArmed,
	int dbIndex,
	const Vector3f& scale,
	int netId,
	const Vector3f& pos,
	double rotDeg,
	const GLID& animGlidSpecial,
	int placementId,
	bool networkEntity) {
	if (!m_movObjList)
		return nullptr;

	// Already Spawned ?
	auto pMO = GetMovementObjByNetId(netId);
	if (pMO)
		return pMO;

	// DRF - Added
	StaticCharConfig(pCharConfig, dbIndex);

	// Invalid Reference Model ?
	if (dbIndex <= -1)
		return nullptr;

	// Old static MO rotation workaround
	if (siType != SI_TYPE_PAPER_DOLL) {
		rotDeg *= 2;
	}

	LogInfo("'" << name << "'"
				<< " siType=" << siType
				<< " dbIndex=" << dbIndex
				<< " netId=" << netId
				<< " pos=(" << pos.x << " " << pos.y << " " << pos.z << ")"
				<< " rotDeg=" << rotDeg
				<< " " << (pCharConfig ? pCharConfig->ToStr(false) : ""));

	// Create New Movement Object (static npcs, city vendors, etc.)
	bool ok = CreateMovementObject(
		0,
		Matrix44f::GetIdentity(),
		dbIndex,
		eActorControlType::STATIC,
		netId,
		networkEntity,
		false, // add at tail (index!=0)
		false, // don't spawn at server
		netId,
		&pMO,
		pCharConfig,
		false,
		__FUNCTION__ // drf - will get replaced by static npc & vendor names
	);
	if (!ok || !pMO || !pMO->getSkeleton()) {
		LogError("CreateMovementObject(TYPE_STATIC) FAILED");
		return nullptr;
	}

	// Arm Glids
	if (glidsArmed.size()) {
		// Cache Armed Glids Content Metadata Before Arming (synchronously)
		ContentMetadataCacheSync(glidsArmed);

		// Arm Glids (don't realize until end, don't arm default until end)
		LogInfo("glidsArmed=" << glidsArmed.size() << " - Arming...");
		for (const auto& glid : glidsArmed) {
			if (!ArmItem(glid, pMO, false, false)) {
				LogError(pMO->ToStr() << " ArmItem() FAILED - glid<" << glid << ">");
				// just do our best anyway
			}
		}
	}

	// Unconfigured Movement Objects Are Naked!
	if (!pCharConfig) {
		// Arm Defaults Not Excluded (prevent nakedness)
		pMO->armDefaultItemsNotExcluded();
	}

	// Realize Now
	pMO->realizeDimConfig();

	// Update Scale
	float maxScale = scale.x > scale.y ? scale.x : scale.y;
	maxScale = maxScale > scale.z ? maxScale : scale.z;
	pMO->setBoundingRadius(pMO->getUnscaledBoundingRadius() * maxScale);
	if (pMO->getSkeleton())
		pMO->getSkeleton()->scaleSystem(scale.x, scale.y, scale.z);

	// Realize Entity (starts rendering)
	POSITION refActPos = m_movObjRefModelList->FindIndex(dbIndex);
	if (refActPos) {
		auto pMO_ref = (CMovementObj*)m_movObjRefModelList->GetAt(refActPos);
		if (pMO->getSkeletonConfig() && pMO_ref && pMO_ref->getSkeletonConfig())
			pMO->getSkeletonConfig()->RealizeDimConfig(pMO->getSkeleton(), pMO_ref->getSkeletonConfig()->m_meshSlots);
	}

	// Set State Configured
	pMO->setState(CMovementObj::STATE_CONFIGURED);

	// Set Type
	pMO->setServerItemType((SERVER_ITEM_TYPE)siType);

	// Set Movement Object Name
	pMO->setName(name);

	// Set Movement Object Position
	Matrix44f positionalMatrix;
	ConvertRotationY(ToRadians(rotDeg), out(positionalMatrix));
	positionalMatrix(3, 0) = pos.x;
	positionalMatrix(3, 1) = pos.y;
	positionalMatrix(3, 2) = pos.z;
	pMO->setBaseFrameMatrix(positionalMatrix, true);
	pMO->setValidThisRound(true);

	// Reset Overhead Labels
	if (!pMO->getSkeleton())
		return nullptr;
	pMO->getSkeleton()->getOverheadLabels()->reset();

	// Add NPC Overhead Name Text
	const Vector3d color(0.3, 0.3, 0.8);
	pMO->getSkeleton()->getOverheadLabels()->addLabel(
		pMO->getName(),
		.014f,
		color,
		SkeletonLabels::SL_POS_NAME);

	// Valid Animation ? (glidSpecial < 0 = reverse animation)
	GLID animGlid = abs(animGlidSpecial);
	if (IS_VALID_GLID(animGlid)) {
		// Set Movement Object Animation
		if (networkEntity) {
			AnimSettings animSettings;
			animSettings.setGlidSpecial(animGlidSpecial);
			pMO->getSkeleton()->setPrimaryAnimationSettings(animSettings);

			// Set Armed Items Animations (head?)
			if (IS_KANEVA_GLID(animGlid) && pMO->getSkeleton()->getEquippableItemCount() > 0) {
				auto pSAH = pMO->getSkeleton()->getSkeletonAnimHeaderByGlid(animGlid);
				if (pSAH) {
					// Propagate typed character animation to equippables - intended for facial animation
					std::vector<CArmedInventoryObj*> allEquips;
					pMO->getSkeleton()->getAllEquippableItems(allEquips);
					for (auto pAIO : allEquips) {
						if (!pAIO || !pAIO->getSkeleton())
							continue;

						// Check if armed item has animation matching character animation type
						GLID animGlid_aio = pAIO->getSkeleton()->getAnimationGlidByAnimTypeAndVer(pSAH->m_animType);
						if (!IS_VALID_GLID(animGlid_aio))
							continue;

						// If so, set matching animation on the armed item
						pAIO->getSkeleton()->setPrimaryAnimationSettings(AnimSettings(animGlid_aio));
					}
				}
			}
		} else {
			pMO->setOverrideAnimationSettings(animGlid);
		}
	}

	return pMO;
}

bool ClientBaseEngine::DeSpawnStaticMovementObj(NETID netTraceId) {
	return DestroyMovementObject(GetStaticMovementObjByNetId(netTraceId));
}

void ClientBaseEngine::InvalidateLightCaches(const Vector3f* location, float range) {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	if (!m_pWOL)
		return;

	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) {
		auto pWO = (CWldObject*)m_pWOL->GetNext(posLoc);
		if (!pWO)
			continue;

		if (location) {
			if (pWO->GetReMesh()) {
				// check if object within light's range
				Vector3f lightVec = pWO->GetReMesh()->GetWorldMatrix()->GetTranslation() - *location;
				FLOAT dsq = lightVec.LengthSquared();
				FLOAT r = (range + pWO->GetReMesh()->GetRadius()) * ReD3DX9DEVICERENDERSTATERADIUSADJ;

				// skip object if outside light's range
				if (dsq >= r * r) {
					continue;
				}
			}
		}

		// flush the light cache
		pWO->m_lightCache.Flush();

		// tell the prelight shader to recalc vertex lighting
		if (pWO->GetReMesh()) {
			pWO->GetReMesh()->InvalidateShader();

			for (UINT i = 0; i < CWldObject::m_maxLODs; i++) {
				if (pWO->GetReLODMesh(i)) {
					pWO->GetReLODMesh(i)->InvalidateShader();
				}
			}
		}
	}

	// invalidate the light cache of the dynamic object since a new light is being added
	IterateObjects([location, range](CDynamicPlacementObj* pDPO) -> bool {
		if (pDPO->GetBaseObj()) {
			for (size_t vi = 0; vi < pDPO->GetBaseObj()->getVisualCount(); vi++) {
				StreamableDynamicObjectVisual* visualPtr = (StreamableDynamicObjectVisual*)pDPO->GetBaseObj()->Visual(vi);

				if (location) {
					if (pDPO->GetReLODMesh(vi, 0)) {
						// check if object within light's range
						Vector3f lightVec = pDPO->GetReLODMesh(vi, 0)->GetWorldMatrix()->GetTranslation() - *location;
						FLOAT dsq = lightVec.LengthSquared();
						FLOAT r = (range + pDPO->GetBoundingRadius()) * ReD3DX9DEVICERENDERSTATERADIUSADJ;

						// skip object if outside light's range
						if (dsq >= r * r) {
							continue;
						}
					} else {
						//Possibly bad asset--see CDynamicPlacementObj::GetReLODMesh/InitReMeshes
						ASSERT(!visualPtr->getIsVisible());
					}
				}

				pDPO->getSkeleton()->flushLightCache();

				// tell the prelight shader to recalc vertex lighting
				for (UINT i = 0; i < (UINT)visualPtr->getLodCount(); i++) {
					if (pDPO->GetReLODMesh(vi, i))
						pDPO->GetReLODMesh(vi, i)->InvalidateShader();
				}
			}
		}
		return true; // next
	});
}

bool ClientBaseEngine::ClientIsEnabled() const {
	return m_multiplayerObj ? m_multiplayerObj->isClientEnabled() : false;
}

// #MLB_BulletSensor - ClientBaseEngine LandClaim
bool ClientBaseEngine::IsObjectInLandClaim(int objectPid, int landClaimPid) {
	std::shared_ptr<Physics::Sensor> pSensor;
	if (m_pUniverse->FindSensor(landClaimPid, pSensor)) {
		return pSensor->IsObjectBounded(objectPid);
	}

	return false;
}

bool ClientBaseEngine::RegisterLandClaim(int landClaimPid) {
	CTriggerObject* pTO = m_dynObjTriggers.GetTriggerByPlacementId(landClaimPid);
	if (pTO && (pTO->GetPosition() != Vector3f::Zero())) {
		Physics::BoxShapeSensorProperties landClaimProps;

		landClaimProps.m_objectType = ObjectType::DYNAMIC;
		landClaimProps.m_Pid = landClaimPid;
		landClaimProps.m_transform = *pTO->GetMatrix();
		landClaimProps.m_BoxShapeSensorProps.m_vDimensionsMeters = pTO->GetHalfExtents();

		std::shared_ptr<Physics::Sensor> pSensor = Physics::CreateSensor(landClaimProps);
		if (pSensor == nullptr)
			return false;
		return m_pUniverse->Add(pSensor);
	}

	return false;
}

bool ClientBaseEngine::UnRegisterLandClaim(int landClaimPid) {
	std::shared_ptr<Physics::Sensor> pSensor;
	if (m_pUniverse->FindSensor(landClaimPid, pSensor)) {
		return m_pUniverse->Remove(pSensor.get());
	}

	return false;
}

IMemSizeGadget* ClientBaseEngine::CreateMemSizeGadget() const {
	return new CMemSizeGadget;
}

} // namespace KEP