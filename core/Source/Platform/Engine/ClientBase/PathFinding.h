///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include "Core/Math/Vector.h"

namespace KEP {

class CMovementObj;
class ClientBaseEngine;

class IKinematicEntity {
public:
	enum MotionStyle {
		Auto,
		Walk,
		Run,
	};

	virtual ~IKinematicEntity() {}

	virtual void GetPosition(Vector3f& pos) = 0;
	virtual void SetPosition(const Vector3f& pos) = 0;

	virtual void GetOrientation(float& dirX, float& dirZ) = 0;
	virtual void SetOrientation(float dirX, float dirZ) = 0;

	virtual void GetPosForce(Vector3f& posForce) = 0;
	virtual void SetPosForce(const Vector3f& posForce) = 0;

	virtual void GetRotForce(Vector3f& rotForce) = 0;
	virtual void SetRotForce(const Vector3f& rotForce) = 0;

	virtual void SetMotionStyle(int style) = 0;
};

class IPathFinder {
public:
	enum State {
		IDLE,
		ENROUTE,
		ARRIVED,
		CANCELED,
	};

	enum CancelReason {
		NONE,
		CANCEL_BY_USER, // User canceled path finding by manual intervention (if allowed)
		ROUTING_FAILED, // AI unable to find a route
		TIMEOUT, // AI unable finish routing within specified time
		CANCEL_BY_GAME, // Died, lose mobility, no longer applicable due to game state transition, etc.
		CLIENT_SHUTDOWN // Close-down
	};

	virtual ~IPathFinder() {}

	virtual bool Start(
		IKinematicEntity* obj,
		const Vector3f& destPos,
		const Vector3f& destRot,
		float desiredSpeed,
		int recalcInterval,
		int motionStyle,
		bool bPhysicsEnabled = false,
		int timeoutInMS = 0,
		bool bPlayerControl = false) = 0;

	// Return path finding state.
	virtual int Update() = 0;

	// Return true if canceled, false if failed -- already canceled/arrived or not started yet.
	virtual bool Cancel(int reason) = 0;

	virtual int GetState() const = 0;

	virtual int GetCancelReason() const = 0;

	virtual bool AllowPlayerControl() const = 0;
};

class SimplePathFinder : public IPathFinder {
public:
	enum EnrouteSubState {
		NA,
		INITIAL_ROTATING,
		TRAVELING,
		FINAL_ROTATING,
	};

	SimplePathFinder() :
			m_initPos(0.0f, 0.0f, 0.0f),
			m_initRot(0.0f, 0.0f, 0.0f),
			m_destPos(0.0f, 0.0f, 0.0f),
			m_destRot(0.0f, 0.0f, 0.0f),
			m_distance(0.0f),
			m_posForceY(0.0f),
			m_motionStyle(IKinematicEntity::Auto),
			m_dir(0.0f, 0.0f, 0.0f),
			m_angularVel(0.0f),
			m_state(IDLE),
			m_cancelReason(NONE),
			m_recalcInterval(-1),
			m_bPhysicsEnabled(false),
			m_timer(Timer::State::Paused),
			m_timeoutInMS(0),
			m_bPlayerControl(false),
			m_prevRotationDirection(0),
			m_subState(NA) {}

	virtual ~SimplePathFinder() {
		if (m_pObject) {
			delete m_pObject;
			m_pObject = nullptr;
		}
	}

	bool Start(
		IKinematicEntity* obj,
		const Vector3f& destPos,
		const Vector3f& destRot,
		float desiredSpeed,
		int recalcInterval,
		int motionStyle,
		bool bPhysicsEnabled = false,
		int timeoutInMS = 0,
		bool bPlayerControl = false) override;

	int Update() override;

	bool Cancel(int reason) override;

	int GetState() const override {
		return m_state;
	}

	int GetCancelReason() const override {
		return m_cancelReason;
	}

protected:
	bool AllowPlayerControl() const override {
		return m_bPlayerControl;
	}

	void SetState(int state) {
		m_state = state;
	}

	void SetCancelReason(int reason) {
		m_cancelReason = reason;
	}

	void Reset();

private:
	IKinematicEntity* m_pObject;
	Vector3f m_initPos;
	Vector3f m_initRot;
	Vector3f m_destPos;
	Vector3f m_destRot;
	float m_distance;
	float m_posForceY;
	int m_motionStyle;
	Vector3f m_dir;
	float m_angularVel;
	bool m_bPhysicsEnabled;
	long m_recalcInterval;
	Timer m_timer;
	unsigned long m_timeoutInMS;
	bool m_bPlayerControl;
	int m_state, m_cancelReason;
	int m_updateCount;
	int m_prevRotationDirection;
	int m_subState;
};

class KEMovementObject : public IKinematicEntity {
public:
	KEMovementObject(ClientBaseEngine* engine, int netTraceId) :
			m_pEngine(engine), m_netTraceId(netTraceId) {}

	void GetPosition(Vector3f& pos) override;
	void SetPosition(const Vector3f& pos) override;

	void GetOrientation(float& dirX, float& dirZ) override;
	void SetOrientation(float dirX, float dirZ) override;

	void GetPosForce(Vector3f& posForce) override;
	void SetPosForce(const Vector3f& posForce) override;

	void GetRotForce(Vector3f& rotForce) override;
	void SetRotForce(const Vector3f& rotForce) override;

	void SetMotionStyle(int style) override;

private:
	ClientBaseEngine* m_pEngine;
	int m_netTraceId;
};

} // namespace KEP
