///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientBaseEngine.h"
#include "CtextReadoutClass.h"
#include "CWldObject.h"
#include "CPlayerClass.h"
#include "RuntimeSkeleton.h"
#include "SkeletonConfiguration.h"
#include "CFrameObj.h"
#include "CMultiplayerClass.h"
#include "CPacketQueClass.h"
#include "Engine/Messages/MsgSpawnStaticMovementObj.h"
#include "Tools/NetworkV2/Protocols.h"
#include "GlobalEventRegistry.h"
#include "LogHelper.h"
#include "LocaleStrings.h"

namespace KEP {

static LogInstance("Instance");

// See HandleItemLocationalMessagesClientSide
const static int HeadIndexToGLIDRemap[][2] = {
	{ 79, 3744 }, // F_Head_004
	{ 80, 3745 }, // F_Head_005
	{ 44, 2993 }, // F_Head_African
	{ 43, 2992 }, // F_Head_Asian
	{ 42, 2991 }, // F_Head_Caucasian
	{ 77, 3720 }, // M_Head_004
	{ 78, 3746 }, // M_Head_005
	{ 47, 2990 }, // M_Head_Africa
	{ 46, 2989 }, // M_Head_Asian
	{ 45, 2988 }, // M_Head_Caucasian
	{ 0, 0 },
};

#define DRF_NO_MOVE_DURING_REZONE 1 ///< client does not send move messages during rezone

///////////////////////////////////////////////////////////////////////////////
// Message Senders
///////////////////////////////////////////////////////////////////////////////

bool ClientBaseEngine::IsMsgUpdateEnabled() const {
	return true && ClientIsEnabled() && !IsRezoning() && !IsLoadingScene();
}

bool ClientBaseEngine::SendMsgUpdateToServer() {
	// Update Messages Enabled ?
	if (!IsMsgUpdateEnabled())
		return false;

	// Limit Update Messages To Server
	TimeMs timeMs = (m_msgUpdateToServerOutstanding > 5) ? msgUpdateMaxTimeMs : msgUpdateMinTimeMs;
	TimeMs timeElapsedMs = m_msgUpdateToServerTimer.ElapsedMs();
	if (timeElapsedMs < timeMs)
		return false;

	// Encode Message Update To Server
	BYTE* pData = 0;
	size_t dataSize = 0;
	bool forceSend = (timeElapsedMs > 10.0 * MS_PER_SEC); // ED-7350 - server keep alive
	CPacketObjList::EncodeMsgUpdate(
		pData,
		dataSize,
		MSG_CAT::UPDATE_TO_SERVER,
		m_multiplayerObj->m_pMsgOutboxAttrib,
		m_multiplayerObj->m_pMsgOutboxEquip,
		nullptr, // msgOutboxGeneral
		m_multiplayerObj->m_pMsgOutboxEvent,
		forceSend);
	if (!pData)
		return false;

	// Send Message Update To Server
	bool ok = m_multiplayerObj->MsgTx(pData, dataSize);
	if (ok)
		m_msgUpdateToServerOutstanding++;

	// Delete Message Data
	delete[] pData;

	// Reset Message Timer
	m_msgUpdateToServerTimer.Reset();

	return ok;
}

bool ClientBaseEngine::IsMsgMoveEnabled() {
	bool enabled = m_msgMoveEnabled && ClientIsEnabled() && !IsRezoning() && !IsLoadingScene()
#if (DRF_NO_MOVE_DURING_REZONE == 1)
				   && !IsZoneLoadingUIActive()
#endif
		;
	if (enabled)
		m_msgMoveEnabledTimer.Start();
	else
		m_msgMoveEnabledTimer.Pause();
	return enabled;
}

bool ClientBaseEngine::SendMsgMoveToServer() {
	// Move Messages Enabled ?
	if (!IsMsgMoveEnabled())
		return false;

	// Get My Avatar & Skeleton
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;
	auto pSkeleton = pMO->getSkeleton();
	if (!pSkeleton)
		return false;

	// Get Target Animations (being blended to)
	GLID animTargetGlid = GLID_INVALID;
	bool animTargetChanged = false;
	pSkeleton->getAnimTarget(animTargetGlid, animTargetChanged);

	// Limit Move Messages To Server
	TimeMs timeMs = (m_msgMoveToServerOutstanding > 5) ? msgMoveMaxTimeMs : msgMoveMinTimeMs;
	TimeMs timeElapsedMs = m_msgMoveToServerTimer.ElapsedMs();
	if ((timeElapsedMs < timeMs) && !animTargetChanged)
		return false;

	// Get Current Position & Orientation
	Vector3f pos, dir, up;
	pMO->getBaseFramePosDirUp(pos, dir, up);

	// Get Target Animations (being blended to)
	GLID animGlid1 = pMO->isVisible() ? animTargetGlid : GLID_INVALID;
	GLID animGlid2 = pMO->isVisible() ? pSkeleton->getAnimInUseGlid(ANIM_SEC) : GLID_INVALID;

	// Form Move Data To Server
	MOVE_DATA moveData(pMO->getNetTraceId(), pos, dir, up, animGlid1, animGlid2);

	// Encode Message Move To Server (encodes deltas only)
	std::vector<BYTE> msgMoveToServer;
	msgMoveToServer.push_back((BYTE)MSG_CAT::MOVE_TO_SERVER); // msgCat
	msgMoveToServer.push_back((BYTE)0); // msgVersion
	moveData.Encode(m_moveDataToServer, msgMoveToServer);

	// Store Move Data To Server For Delta Encoding Next Update
	m_moveDataToServer = moveData;

	// Send Message Move To Server
	bool ok = m_multiplayerObj->MsgTx((BYTE*)&msgMoveToServer[0], msgMoveToServer.size());
	if (ok)
		m_msgMoveToServerOutstanding++;

	// Reset Message Timer
	m_msgMoveToServerTimer.Reset();

	return ok;
}

///////////////////////////////////////////////////////////////////////////////
// Message Handlers
///////////////////////////////////////////////////////////////////////////////

bool ClientBaseEngine::HandleMsgs(BYTE* pData, NETID netIdFrom, size_t dataSize, bool& bDisposeMessage) {
	// Reset Return Values
	bDisposeMessage = true;

	if (!pData || !dataSize) {
		LogError("Invalid Message - netId=" << netIdFrom);
		return false;
	}

	// ED-7834 - Warn On Stalling
	Timer timer;

	// Handle Message
	bool ok = false;
	MSG_HEADER* pMsgHdr = (MSG_HEADER*)pData;
	MSG_CAT msgCat = pMsgHdr->GetCategory();
	switch (msgCat) {
		case MSG_CAT::EVENT:
			ok = HandleMsgEvent(pData, dataSize, netIdFrom);
			break;

		case MSG_CAT::LOG_TO_CLIENT:
			ok = HandleMsgLogToClient(pData);
			break;

		case MSG_CAT::ATTRIB_TO_CLIENT:
			ok = HandleMsgAttribToClient(pData + sizeof(MSG_HEADER));
			break;

		case MSG_CAT::SPAWN_TO_CLIENT:
			ok = HandleMsgSpawnToClient(pData, netIdFrom, dataSize);
			break;

		case MSG_CAT::MOVE_TO_CLIENT:
			ok = HandleMsgMoveToClient(pData, netIdFrom);
			break;

		case MSG_CAT::UPDATE_TO_CLIENT:
			ok = HandleMsgUpdateToClient(pData, netIdFrom);
			break;
	}

	// ED-7834 - Warn On Stalling
	auto timeMs = timer.ElapsedMs();
	if (timeMs > 100) {
		LogWarn("SLOW(" << FMT_TIME << timeMs << "ms) - HandleMsg(" << MsgCatStr(msgCat) << ")");
	}

	return ok;
}

bool ClientBaseEngine::HandleMsgEvent(BYTE* pData, size_t dataSize, NETID netIdFrom) {
	if (!pData || (dataSize < sizeof(EventHeader)))
		return false;
	try {
		IEvent* pEvent = m_dispatcher.MakeEvent((PCBYTE)pData, dataSize, netIdFrom);
		if (!pEvent) {
			LogFatal("MakeEvent() FAILED");
			return false;
		}

		// events are always positive
		auto eventId = pEvent->EventId();
		if (eventId > 0) {
			// ED-7834 - Warn On Stalling
			Timer timer;

			m_dispatcher.ProcessOneEvent(pEvent);

			// ED-7834 - Warn On Stalling
			auto timeMs = timer.ElapsedMs();
			if (timeMs > 100) {
				LogWarn("SLOW(" << FMT_TIME << timeMs << "ms) - ProcessOneEvent(" << GlobalEventName(eventId) << ")");
			}
		} else
			pEvent->Delete();
	} catch (KEPException* e) {
		LogError("Event Exception Error: " << e->m_msg);
		e->Delete();
	}
	return true;
}

bool ClientBaseEngine::HandleMsgLogToClient(BYTE* pData) {
	auto pMsg = (MSG_LOG_TO_CLIENT*)pData;
	if (!pMsg || !m_multiplayerObj)
		return false;

	// Sync To Server Time (accounting for network latency)
	if (pMsg->timeLogToServer && pMsg->timeProcessing && pMsg->timeLogToClient) {
		TimeMs timeLogToClient = fTime::TimeMs();
		TimeMs timeDelta = timeLogToClient - pMsg->timeLogToServer;
		TimeMs timeLatency = (timeDelta - pMsg->timeProcessing) / 2.0;
		TimeMs timeSync = pMsg->timeLogToClient + timeLatency;
		TimeMs timeSyncDelta = fTime::SetTimeSyncMs(timeSync);
		LogInfo("timeDelta=" << timeDelta
							 << " timeProcessing=" << pMsg->timeProcessing
							 << " timeLatency=" << timeLatency
							 << " timeSync=" << timeSync
							 << " timeSyncDelta=" << timeSyncDelta);
	}

	return true;
}

bool ClientBaseEngine::HandleMsgAttribToClient(BYTE* pData) {
	auto pMsg = (ATTRIB_DATA*)pData;
	if (!pMsg)
		return false;

	switch (pMsg->aeType) {
		case eAttribEventType::PlayerCreatedHealth: {
			// Player Already Spawned ?
			auto traceNumber = pMsg->aeShort1;
			auto netTraceId = pMsg->aeInt1;
			for (POSITION posLoc = m_movObjList->GetHeadPosition(); posLoc != NULL;) {
				auto pMO = (CMovementObj*)m_movObjList->GetNext(posLoc);
				if (!pMO || (pMO->getTraceNumber() != traceNumber))
					continue;

				// set network id for communication purposes
				pMO->setNetTraceId(netTraceId);
				m_dispatcher.SetNetId(pMO->getNetTraceId());

				// Already Requested ?
				auto it = m_moNetTraceIdsRequested.find(netTraceId);
				if (it != m_moNetTraceIdsRequested.end())
					return false;
				m_moNetTraceIdsRequested.insert(netTraceId);

				// Request Movement Object Configuration (normal players)
				ATTRIB_DATA attribData;
				attribData.aeType = eAttribEventType::GetMovementObjectCfg;
				attribData.aeShort1 = netTraceId;
				// attribData.aeShort2 = isItem // server reads but never set ??!!
				attribData.aeInt2 = netTraceId; // server never reads but set anyway ??!!
				m_multiplayerObj->QueueMsgAttribToServer(attribData);
			}
		} break;

		case eAttribEventType::DeleteEntity: {
			POSITION posLast;
			for (POSITION posLoc = m_movObjList->GetHeadPosition(); (posLast = posLoc) != NULL;) {
				auto pMO = (CMovementObj*)m_movObjList->GetNext(posLoc);
				if (pMsg->aeShort1 == pMO->getNetTraceId())
					DestroyMovementObject(pMO);
			}
		} break;

		case eAttribEventType::ChangeDefCfg: {
			// Change Default Config On Movement Object
			auto pMO = GetMovementObjByNetId(pMsg->aeShort1);
			if (!pMO) {
				LogError("GetMovementObjByNetId() FAILED - netId=" << pMsg->aeShort1);
				return false;
			}

			if (pMO->getSkeleton()->getSkinnedMeshCount() <= pMsg->aeInt1)
				return false;

			POSITION posRef = m_movObjRefModelList->FindIndex(pMO->getIdxRefModel());
			if (!posRef)
				return false;

			auto pMORM = (CMovementObj*)m_movObjRefModelList->GetAt(posRef);
			return pMO->getSkeletonConfig()->HotSwapDim(
				pMsg->aeInt1,
				pMsg->aeInt2,
				pMORM->getSkeleton()->getSkinnedMeshCount(),
				true);
		} break;

		case eAttribEventType::PlayerCredits: {
			m_multiplayerObj->m_cashCount = pMsg->aeInt1;
		} break;

		case eAttribEventType::BankCredits: {
			m_multiplayerObj->m_bankCashCount = pMsg->aeInt1;
		} break;

		default:
			return false;
	}

	return true;
}

bool ClientBaseEngine::HandleMsgSpawnToClient(BYTE* pData, NETID netIdFrom, size_t dataSize) {
	auto pMsg = (MSG_SPAWN_TO_CLIENT*)pData;
	if (!pMsg || !dataSize || !m_multiplayerObj)
		return false;

	// Extract EXG File Name
	char* pExgFileName = pMsg->exgFileName;
	std::string exgFileName = pExgFileName ? pExgFileName : "";

	// Extract Optional URL
	CStringA url;
	if (dataSize > sizeof(MSG_SPAWN_TO_CLIENT) + exgFileName.size())
		url = pExgFileName + exgFileName.size() + 1;

	// EXG File Change ?
	if (!StrSameFilePath(exgFileName, m_zoneFileName.GetString())) {
		// Load Scene
		if (!LoadScene(exgFileName, url)) {
			std::string msg;
			StrBuild(msg, "FAILED LoadScene() '" << exgFileName << "' url='" << url << "'");
			LogError(msg);
			m_pTextReadoutObj->AddLine(msg, eChatType::System);
			return false;
		}

		m_zoneFileName = exgFileName.c_str();
	} else {
		ZoneLoaded(exgFileName.c_str(), url, true);

		// Here we reset all the custom textures
		MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
		if (m_pWOL) {
			for (POSITION pos = m_pWOL->GetHeadPosition(); pos;) {
				auto pWO = (CWldObject*)m_pWOL->GetNext(pos);
				if (pWO)
					pWO->ResetCustomMaterial();
			}
		}

		// DRF - Added Null Check
		IEvent* e = m_dispatcher.MakeEvent(m_dispatcher.GetEventId("ZoneReloadedEvent"));
		if (!e) {
			LogError("FAILED dispatcher.MakeEvent(ZoneReloadedEvent)");
		} else {
			(*e->OutBuffer()) << exgFileName.c_str() << (url ? url : "");
			m_dispatcher.QueueEvent(e);
		}
	}
	m_zoneIndex.fromLong(pMsg->zoneIndexInt);

	// DRF - TODO - Don't think we should be doing this
	if (m_multiplayerObj->m_pMsgOutboxAttrib)
		m_multiplayerObj->m_pMsgOutboxAttrib->SafeDelete();

	// calculate the rotation
	Matrix44f transformMatrix;
	transformMatrix.MakeIdentity();
	ConvertRotationY(ToRadians(pMsg->rotation * 2.0f), out(transformMatrix));
	transformMatrix(3, 0) = pMsg->x;
	transformMatrix(3, 1) = pMsg->y;
	transformMatrix(3, 2) = pMsg->z;

	// Create New Movement Object (my avatar)
	bool ok = CreateMovementObject(
		0, //pMsg->aiConfigDeprecated,
		transformMatrix,
		pMsg->dbIndex,
		eActorControlType::PC,
		0,
		false, // not networkEntity
		true, // add at head (index=0)
		true, // spawn at server
		-1,
		NULL,
		NULL,
		false,
		__FUNCTION__ // drf - eventually replaced with my avatar name
	);

	CMovementObj* pMO = NULL;
	if (!ok) {
		LogError("CreateMovementObject(TYPE_PC - MY AVATAR) FAILED");
	} else {
		// Get New My Avatar
		pMO = GetMovementObjMe();
		if (!pMO) {
			LogError("GetMovementObjMe() FAILED");
		} else {
			// Disable Rendering Until Finished Configuration
			pMO->setRender(false);
		}
	}

	m_dispatcher.QueueEvent(new ClientSpawnEvent(
		ZoneIndex(pMsg->zoneIndexInt).getClearedInstanceId(),
		pMsg->instanceId,
		pMsg->x, pMsg->y, pMsg->z));

	// DRF - Added
	OnJustSpawned();

	return (pMO != NULL);
}

bool ClientBaseEngine::HandleMsgUpdateToClient(BYTE* pData, NETID netIdFrom) {
	auto pMsgHdr = (MSG_UPDATE*)pData;
	if (!pMsgHdr)
		return false;

	// Reset Messages Outstanding
	m_msgUpdateToServerOutstanding = 0;

	// Skip Message Header
	pData += sizeof(MSG_UPDATE);

	// Handle Attribute Updates
	auto attribMsgs = (size_t)pMsgHdr->m_attribMsgs;
	for (size_t i = 0; i < attribMsgs; ++i) {
		auto pMsg = (ATTRIB_DATA*)pData;
		HandleMsgAttribToClient((BYTE*)pMsg);
		pData += sizeof(ATTRIB_DATA);
	}

	// Handle Equip Updates
	auto equipMsgs = (size_t)pMsgHdr->m_equipMsgs;
	for (size_t i = 0; i < equipMsgs; ++i) {
		auto pMsg = (EQUIP_DATA*)pData;
		HandleMsgEquipToClient((BYTE*)pMsg);
		pData += sizeof(EQUIP_DATA) + (pMsg->glids * sizeof(GLID));
	}

	// Handle General Updates
	auto generalMsgs = (size_t)pMsgHdr->m_generalMsgs;
	for (size_t i = 0; i < generalMsgs; i++) {
		auto pMsg = (GENERAL_DATA*)pData;
		HandleMsgGeneralToClient((BYTE*)pMsg);
		pData += sizeof(GENERAL_DATA);
	}

	// Handle Event Updates
	auto eventMsgs = (size_t)pMsgHdr->m_eventMsgs;
	for (size_t i = 0; i < eventMsgs; i++) {
		auto msgSize = *(ULONG*)pData;
		pData += sizeof(ULONG); // LEN_FMT_ULONG
		if (!msgSize)
			continue;
		HandleMsgEvent(pData, msgSize, netIdFrom);
		pData += msgSize;
	}

	return true;
}

bool ClientBaseEngine::HandleMsgMoveToClient(BYTE* pData, NETID netIdFrom) {
	auto pMsg = (MSG_MOVE_TO_CLIENT*)pData;
	if (!pMsg || !m_multiplayerObj)
		return false;

	// Reset Messages Outstanding
	m_msgMoveToServerOutstanding = 0;

	// Flag All Entities Invalid
	for (POSITION pos = m_movObjList->GetHeadPosition(); pos;) {
		auto pMO = (CMovementObj*)m_movObjList->GetNext(pos);
		pMO->setValidThisRound(false);
	}

	// Skip Message Header
	pData += sizeof(MSG_MOVE_TO_CLIENT);

	// Handle Avatar Movement Updates
	auto numMoveToClientData = (size_t)pMsg->m_numMoveToClientData;
	for (size_t i = 0; i < numMoveToClientData; ++i) {
		auto pMoveToClientData = (MOVE_TO_CLIENT_DATA*)pData;
		auto netTraceId = pMoveToClientData->netTraceId;
		MOVE_DATA moveData;
		auto pMO = GetMovementObjByNetId(netTraceId);
		if (pMO)
			pMO->getMoveDataToClient(moveData);
		moveData.Decode(pData); // updates pData
		if (pMO)
			pMO->setMoveDataToClient(moveData);
		UpdateEntityMoveData(moveData); // flags entity valid
	}

	// Handle Static Entity Updates
	auto numStaticEntityData = (size_t)pMsg->m_numStaticEntityData;
	for (size_t i = 0; i < numStaticEntityData; i++) {
		auto pStaticEntityData = (STATIC_ENTITY_DATA*)pData;
		auto netTraceId = abs(pStaticEntityData->netTraceIdSpecial); // <0 means static mo but it is never anything else

		// Get Movement Object (always a static)
		auto pMO = GetStaticMovementObjByNetId(netTraceId);

		// Have We Spawned This Movement Object Yet ?
		if (pMO) {
			pMO->setValidThisRound(true);
			if (pMO->getPosses())
				pMO->getPosses()->setValidThisRound(true);
		} else {
			// Already Requested ?
			auto it = m_moNetTraceIdsRequested.find(netTraceId);
			if (it == m_moNetTraceIdsRequested.end()) {
				m_moNetTraceIdsRequested.insert(netTraceId);

				// Request Static Movement Object Configuration (city vendors, paper dolls, etc.)
				ATTRIB_DATA attribData;
				attribData.aeType = eAttribEventType::GetStaticMovementObjectCfg;
				attribData.aeInt1 = netTraceId;
				m_multiplayerObj->QueueMsgAttribToServer(attribData);
			}
		}
		pData += sizeof(STATIC_ENTITY_DATA);
	}

	// Handle Spawn Static MO Updates
	auto numSpawnStaticMOData = (size_t)pMsg->m_numSpawnStaticMOData;
	for (size_t i = 0; i < numSpawnStaticMOData; i++) {
		size_t msgSize = *(USHORT*)pData;
		pData += sizeof(USHORT); // LEN_FMT_USHORT
		if (!msgSize)
			continue;
		HandleMsgSpawnStaticMOToClient(pData);
		pData += msgSize;
	}

	// Remove All Entities Still Flagged Invalid
	POSITION tagPosLast = NULL;
	for (POSITION tagPos = m_movObjList->GetHeadPosition(); (tagPosLast = tagPos) != NULL;) {
		auto pMO = (CMovementObj*)m_movObjList->GetNext(tagPos);
		if (pMO && !pMO->isValidThisRound() && pMO->isNetworkEntity()) {
			m_moNetTraceIdsRequested.erase(pMO->getNetTraceId());
			DestroyMovementObject(pMO);
		}
	}

	return true;
}

bool ClientBaseEngine::HandleMsgEquipToClient(BYTE* pData) {
	auto pMsg = (EQUIP_DATA*)pData;
	if (!pMsg)
		return false;

	// Extract Message Parameters
	NETID netTraceId = (NETID)pMsg->netTraceId; // from (short)
	size_t glids = (size_t)pMsg->glids; // from (unsigned char)
	GLID* pGlids = (GLID*)(((BYTE*)pMsg) + sizeof(EQUIP_DATA));

	// Equip All Items Async (0=disarmAll, <0=disarm, >0=arm)
	LogInfo("glidsSpecial=" << glids << " netTraceId=" << netTraceId);
	std::vector<GLID> glidsSpecial;
	for (size_t i = 0; i < glids; ++i)
		glidsSpecial.push_back(pGlids[i]);
	return EquipItemsAsync(glidsSpecial, netTraceId);
}

bool ClientBaseEngine::HandleMsgSpawnStaticMOToClient(BYTE* pData) {
	if (!pData || !m_multiplayerObj)
		return false;

	// Decode Message Data
	MsgSpawnStaticMovementObj msg(ProtocolCompatibility::Instance().getMessageVersionByType(m_multiplayerObj->GetClientProtocol(), PC_MSG_SPAWN_STATIC_MO));
	if (!msg.decode(pData)) {
		LogError("msg.decode() FAILED");
		return false;
	}

	// Spawn Static Movement Object
	auto pMO = SpawnStaticMovementObject(
		msg.getServerItemType(),
		msg.getName(),
		&msg.getCharConfig(),
		msg.getArmedGlids(),
		msg.getEDBIndex(),
		msg.getScale(),
		msg.getNetTraceId(),
		msg.getPosition(),
		msg.getRotationDeg(),
		msg.getAnimationGlid(),
		msg.getPlacementId(),
		true // network entity (receives movement updates)
	);

	return (pMO != NULL);
}

bool ClientBaseEngine::HandleMsgGeneralToClient(BYTE* pData) {
	auto pMsg = (GENERAL_DATA*)pData;
	if (!pMsg || !m_multiplayerObj)
		return false;

	unsigned char genData = pMsg->m_data;

	// Load Locale String Given By Data
	unsigned int localeId = LS_GM_BASE + (unsigned int)genData;
	std::string message = loadLocaleString(localeId);
	if (!message.empty()) {
		IEvent* pEvent = new RenderTextEvent(message.c_str(), eChatType::System);
		m_dispatcher.QueueEvent(pEvent);
	}

	IEvent* pEvent = m_dispatcher.MakeEvent(m_attribEventId);
	if (!pEvent) {
		LogError("FAILED dispatcher.MakeEvent() - eventId=" << m_attribEventId);
	} else {
		LogWarn("Sending eAttribEventType::Generic - localeId=" << localeId);
		pEvent->SetFilter((ULONG)eAttribEventType::Generic);
		pEvent->SetObjectId((OBJECT_ID)genData);
		m_dispatcher.QueueEvent(pEvent);
	}

	return true;
}

} // namespace KEP