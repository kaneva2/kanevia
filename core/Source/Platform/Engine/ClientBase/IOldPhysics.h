///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <d3d9.h>
#include "MatrixARB.h"
#include "CABFunctionLib.h"

namespace KEP {

class CEnvironmentObj;
class CMovementCaps;
class CMovementObj;
class CWaterZoneObjList;

class IOldPhysics {
public:
	IOldPhysics() {}

	virtual ~IOldPhysics(void) {}

	virtual void Initialize(const CEnvironmentObj* pEO) = 0;

	virtual void SetEnabledFreeFall(bool enable) = 0;

	virtual void MovPhysicsSystem(
		CMovementObj* pMO,
		const Vector3f& posForceExt,
		const Vector3f& rotForceExt,
		const Vector3f& posDeltaExt) = 0;
};

} // namespace KEP
