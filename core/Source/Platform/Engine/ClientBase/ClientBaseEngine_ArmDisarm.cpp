///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "ClientBaseEngine.h"
#ifdef _ClientOutput
#include "ContentService.h"
#endif

namespace KEP {

static LogInstance("Instance");

///////////////////////////////////////////////////////////////////////////////
// ED-8331 - Async Arm/Disarm Functions
///////////////////////////////////////////////////////////////////////////////

bool ClientBaseEngine::ArmItemsAsync(const std::vector<GLID>& glids, CMovementObj* pMO, bool realizeNow, bool armDefaults, bool sendMyEvent) {
	pMO = (pMO ? pMO : GetMovementObjMe());
	if (!pMO)
		return false;

	// Async Arm All Glids After Caching Metadata
	if (!glids.empty()) {
		std::function<void()> fnCallback = [this, glids, pMO, realizeNow, armDefaults, sendMyEvent]() {
			// Arm All Glids (don't realize until end, don't arm default until end)
			LogInfo(pMO->ToStr() << " ARM GLIDS - glids=" << glids.size() << " ...");
			for (const auto& glid : glids) {
				if (!ArmItem(glid, pMO, false, false, sendMyEvent)) {
					LogError(pMO->ToStr() << " ArmItem() FAILED - glid<" << glid << ">");
					// just do the best we can
				}
			}
			if (armDefaults)
				pMO->armDefaultItemsNotExcluded();
			if (realizeNow)
				pMO->realizeDimConfig();
			pMO->setRender(true);
		};
		ContentMetadataCacheAsync(glids, fnCallback, pMO);
	} else {
		if (armDefaults)
			pMO->armDefaultItemsNotExcluded();
		if (realizeNow)
			pMO->realizeDimConfig();
		pMO->setRender(true);
	}

	return true;
}

bool ClientBaseEngine::EquipItemsAsync(const std::vector<GLID>& glidsSpecial, NETID netTraceId, bool sendMyEvent) {
	if (glidsSpecial.empty())
		return false;

	std::function<void()> fnCallback = [this, glidsSpecial, netTraceId, sendMyEvent]() {
		this->EquipItemsSync(glidsSpecial, netTraceId, sendMyEvent);
	};
	ContentMetadataCacheAsync(glidsSpecial, fnCallback, this);
	return true;
}

////////////////////////////////////////////////////////////////////////
// Sync Arm/Disarm Functions
////////////////////////////////////////////////////////////////////////

bool ClientBaseEngine::ArmItem(const GLID& glid, CMovementObj* pMO, bool realizeNow, bool armDefaults, bool sendMyEvent) {
	// Assume My Avatar ?
	auto pMO_me = GetMovementObjMe();
	if (!pMO)
		pMO = pMO_me;
	if (!pMO || !pMO->getSkeletonConfig())
		return false;
	bool isMe = (pMO == pMO_me);

	// Valid Glid ?
	if (!IS_VALID_GLID(glid))
		goto ArmItemFail;

	// Arm Item
	if (!pMO->armItem(glid, realizeNow, armDefaults)) {
		// UGC Failures Get Ignored (they will arm later asynchronously)
		if (IS_UGC_GLID(glid)) {
			LogWarn(pMO->ToStr() << " armItem() FAILED UGC GLID - glid<" << glid << "> - Ignoring...");
			goto ArmItemSuccess; // it 'should' get armed eventually
		}

		LogError(pMO->ToStr() << " armItem() FAILED - glid<" << glid << ">");
		goto ArmItemFail;
	}

	goto ArmItemSuccess;

ArmItemSuccess:
	if (isMe && sendMyEvent)
		SendArmItemEvent(glid, eArmItemAction::Arm, eArmItemResult::Success);
	return true;

ArmItemFail:
	if (isMe && sendMyEvent)
		SendArmItemEvent(glid, eArmItemAction::Arm, eArmItemResult::Failure);
	return false;
}

bool ClientBaseEngine::DisarmItem(const GLID& glid, CMovementObj* pMO, bool realizeNow, bool armDefaults, bool sendMyEvent) {
	// Assume My Avatar ?
	auto pMO_me = GetMovementObjMe();
	if (!pMO)
		pMO = pMO_me;
	if (!pMO || !pMO->getSkeleton())
		return false;
	bool isMe = (pMO == pMO_me);

	// Valid Glid ?
	if (!IS_VALID_GLID(glid))
		goto DisarmItemFail;

	// Disarm Item
	if (!pMO->disarmItem(glid, realizeNow, armDefaults)) {
		LogError(pMO->ToStr() << " disarmItem() FAILED - glid<" << glid << ">");
		goto DisarmItemFail;
	}

	goto DisarmItemSuccess;

DisarmItemSuccess:
	if (isMe && sendMyEvent)
		SendArmItemEvent(glid, eArmItemAction::Disarm, eArmItemResult::Success);
	return true;

DisarmItemFail:
	if (isMe && sendMyEvent)
		SendArmItemEvent(glid, eArmItemAction::Disarm, eArmItemResult::Failure);
	return false;
}

bool ClientBaseEngine::EquipItemsSync(const std::vector<GLID>& glidsSpecial, NETID netTraceId, bool sendMyEvent) {
	if (glidsSpecial.empty())
		return false;

	// Get Avatar
	bool ok = true;
	bool isMe = (netTraceId == 0);
	auto pMO = (isMe ? GetMovementObjMe() : GetMovementObjByNetId(netTraceId));
	if (!pMO) {
		LogError("GetMovementObj() FAILED - netTraceId=" << netTraceId);
		ok = false;
	}

	// Cache Armed Glids Content Metadata Before Arming (synchronously)
	ContentMetadataCacheSync(glidsSpecial);

	// Equip All Special Glids On Avatar
	LogInfo("glidsSpecial=" << glidsSpecial.size());
	for (const auto& glidSpecial : glidsSpecial) {
		// Special GLID (<0=disarm, 0=disarmAll, >0=arm, -9999=tryon)
		bool disarm = (glidSpecial < 0);
		bool disarmAll = (glidSpecial == 0);
		bool notTryOn = (glidSpecial == -9999);
		GLID glid = (notTryOn ? 0 : abs(glidSpecial));

		// Movement Object Failures ?
		if (!pMO) {
			if (isMe && sendMyEvent)
				SendArmItemEvent(glid, disarm ? eArmItemAction::Disarm : eArmItemAction::Arm, eArmItemResult::Failure);
			continue;
		}

		// Arm/Disarm Entity On Avatar (don't realize or arm default until end)
		if (disarmAll) {
			if (!pMO->disarmAllItems(false, false)) {
				LogError(pMO->ToStr() << " disarmAllItems() FAILED");
				ok = false;
			}
		} else if (disarm) {
			if (!DisarmItem(glid, pMO, false, false, sendMyEvent)) {
				LogError(pMO->ToStr() << " DisarmItem() FAILED - glid<" << glid << ">");
				ok = false;
			}
		} else {
			if (!ArmItem(glid, pMO, false, false, sendMyEvent)) {
				LogError(pMO->ToStr() << " ArmItem() FAILED - glid<" << glid << ">");
				ok = false;
			}
		}
	}

	// Arm Defaults & Realize Now
	if (pMO) {
		pMO->armDefaultItemsNotExcluded();
		pMO->realizeDimConfig();
	}

	return ok;
}

} // namespace KEP