///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#ifndef _CPPRTTI
#error Must use /GR to define RTTI
#endif

#include "BladeExports.h"

#include <jsLibraryFunction.h>
#include <typeinfo.h>
#include <vector>
#include <map>
#include <memory>

namespace KEP {

class BladeCaster {
public:
	virtual bool isMyType(const IBladePtr &b) const = 0;
};

template <class t>
class BC : public BladeCaster {
public:
	bool isMyType(const IBladePtr &b) const {
		try {
			dynamic_cast<const t &>(*b);
		} catch (std::bad_cast) {
			return false;
		}
		return true;
	}
};

typedef IBlade *(*CreateBladeFn)(const char *className);
typedef std::vector<IBladePtr> IBladePtrVector;

class BladeFactory {
	class BladeEntry : public BladeInfo {
	public:
		BladeEntry() {}

		BladeEntry(const BladeEntry &s) {
			operator=(s);
		}

		BladeEntry(BladeInfo *info, const std::shared_ptr<jsLibraryFunction> &pLibFunc) :
				m_pLibFunc(pLibFunc) {
			className = info->className;
			version = info->version;
			createBlade = info->createBlade;
		}

		BladeEntry &operator=(const BladeEntry &s) {
			if (this != &s) {
				className = s.className;
				version = s.version;
				createBlade = s.createBlade;

				m_pLibFunc = s.m_pLibFunc;
			}
			return *this;
		}

		bool operator==(const BladeEntry &s) const {
			return ::strcmp(className, s.className) == 0;
		}

		IBladePtr createNew() {
			return IBladePtr(createBlade());
		}

		std::shared_ptr<jsLibraryFunction> m_pLibFunc;
	};

	typedef std::vector<BladeEntry> Blades;

	bool LoadBlade(const char *path, eEngineType engineType);

	Blades m_blades;
	eEngineType m_engineType;

public:
	BladeFactory(eEngineType engineType) :
			m_engineType(engineType) {}

	virtual ~BladeFactory() {
		Clear();
	}

	void Initialize(const char *baseDir);

	IBladePtr CreateBlade(const BladeCaster &caster, const char *nameToFind, ULONG myVersion);

	IBladePtrVector CreateBlades(BladeCaster &caster, ULONG myVersion);

	void Clear();

	bool classNamesEqual(const char *l, const char *r);

	const Blades &GetBlades() const {
		return m_blades;
	};
};

} // namespace KEP
