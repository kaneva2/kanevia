///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <memory>

#include "BladeFactory.h"
#include "BladeStrings.h"
#include "BladeExports.h"

#include "Core/Util/FileName.h"

#include "common\KEPUtil\Helpers.h"

#include "jsEnumFiles.h"

static LogInstance("Instance");

namespace KEP {

static const char* GETBLADESINFO = "GetBladesInfo";
typedef BladeInfo* (*GetBladesInfoFn)(eEngineType engineType);

static const char* GETLIBVERSION = "GetLibVersion";
typedef ULONG (*GetLibVersionFn)();

// for debugging found that leaks from DLL will crash during leak dump if DLL unloaded
#ifdef _DEBUG
static const bool FREE_ON_DELETE = false;
#else
static const bool FREE_ON_DELETE = true;
#endif

// load all the libraries found in the baseDir+BLADE_DIR
void BladeFactory::Initialize(const char* bladeDir) {
	if (!bladeDir) {
		LogError("bladeDir is null");
		return;
	}

	std::string verStr;
	LogInfo("'" << bladeDir << "' bladeLibVer=" << VersionToString(BLADE_LIB_VERSION, verStr));

	std::string mask = bladeDir;
	bool isDir;
	if (!jsFileExists(mask.c_str(), &isDir) || !isDir) {
		throw new KEPException((loadStrPrintf(IDS_DIR_DOESNT_EXIST, mask) + mask), 0, __FILE__, __LINE__);
	}

	// Blades Are .dll
	mask = PathAdd(mask, "*.dll");

	jsEnumFiles ef(Utf8ToUtf16(mask).c_str());

	int totalLibs = 0;

	// enumerate all the libraries
	while (ef.next()) {
		if (LoadBlade(Utf16ToUtf8(ef.currentPath()).c_str(), m_engineType))
			totalLibs++;
	}

	LogInfo("DONE (" << m_blades.size() << " blades, " << totalLibs << " libs)");
}

bool BladeFactory::LoadBlade(const char* path, eEngineType engineType) {
	if (!path) {
		LogError("path is null");
		return false;
	}
	LogDebug("'" << path << "'");

	bool ok = false;
	std::string verStr;
	VersionToString(BLADE_LIB_VERSION, verStr);

	std::string fileName = FileName::parse(path).toString(FileName::Name | FileName::Ext);

	// for each library, enumerate all the possible entry points
	// must have GetVersion
	auto pLibFunc = std::make_shared<jsLibraryFunction>(path, FREE_ON_DELETE);
	if (pLibFunc->lastError() != 0) {
		LogWarn(loadStr(IDS_WARN_LOAD_BLADE) << fileName << " (" << pLibFunc->lastError() << ")");
		return false;
	}

	std::string s, v; // for printing version

	// check the library version to see if we should go on
	GetLibVersionFn getVersion = (GetLibVersionFn)pLibFunc->getFunction(GETLIBVERSION);
	if (getVersion == 0) {
		if (pLibFunc->lastError() != 0) {
			std::string msg;
			LogWarn(fileName << " FAILED - " << errorNumToString(pLibFunc->lastError(), msg));
		} else
			LogWarn(fileName << " FAILED - Module Not Found");
		return false;
	}

	if (getVersion() >= BLADE_LIB_VERSION && IsDebugVersion(getVersion()) == IsDebugVersion(BLADE_LIB_VERSION)) {
		LogDebug(" ... libVer=" << VersionToString(getVersion(), s));

		v = GetModuleVersion(path);

		GetBladesInfoFn getBladesInfoFn = (GetBladesInfoFn)pLibFunc->getFunction(GETBLADESINFO);
		if (getBladesInfoFn) {
			int bladeCount = 0;
			ok = true;

			// now iterate over all the possible blades in the file
			for (BladeInfo* bladeInfo = getBladesInfoFn(engineType); bladeInfo && bladeInfo->className; bladeInfo++) {
				LogDebug(" ... className=" << bladeInfo->className);

				IBladePtr blade; // ( createBladeFn( *bladeName ) );

				ULONG ver = bladeInfo->version; // blade->Version();

				BladeEntry newEntry(bladeInfo, pLibFunc);

				// we always add all versions in the event that the server wants an
				// older version and we have multiples

				// do we already have another blade with a higher version?
				BladeFactory::Blades::iterator i = find(m_blades.begin(), m_blades.end(), newEntry);

				if (i != m_blades.end() && i->version == ver) {
					// don't add duplicate one
					LogWarn(loadStr(IDS_WARN_DUP_BLADE_VER) << bladeInfo->className);
				} else {
					// if different version, just warn them
					if (i != m_blades.end() && i->version != ver) {
						LogInfo(loadStr(IDS_INFO_DUP_BLADE_VER) << bladeInfo->className);
					}

					LogDebug(" ... className=" << bladeInfo->className << " classVer=" << VersionToString(ver, s));
					m_blades.push_back(newEntry);
					bladeCount++;
				}
				LogInfo(fileName << " OK - (" << bladeInfo->className << " v" << v.c_str() << ")");
			}
			if (bladeCount == 0) {
				LogWarn(fileName << " FAILED - bladeCount=0");
				ok = false;
			}
		} else {
			if (pLibFunc->lastError() != 0) {
				std::string msg;
				LogWarn(fileName << " FAILED - " << errorNumToString(pLibFunc->lastError(), msg));
			} else
				LogWarn(fileName << " FAILED - Module Not Found");
		}
	} else {
		LogWarn(fileName << " FAILED - v" << VersionToString(getVersion(), s) << " != v" << verStr);
	}

	return ok;
}

bool BladeFactory::classNamesEqual(const char* l, const char* r) {
	if (::strncmp(l, "class ", 6) == 0)
		l += 6;
	if (::strncmp(r, "class ", 6) == 0)
		r += 6;
	return ::strcmp(l, r) == 0;
}

IBladePtr BladeFactory::CreateBlade(const BladeCaster& caster, const char* nameToFind, ULONG myVersion) {
	BladeFactory::Blades::iterator i, j;
	IBladePtr ret;
	j = m_blades.end();

	for (i = m_blades.begin(); i != m_blades.end(); i++) {
		// first two levels of version and name must match
		if (classNamesEqual(i->className, nameToFind) && VersionEqual(myVersion, i->version, 2)) {
			// if already have one, take one with higher minor version (all four levels)
			if (j == m_blades.end() || VersionLess(j->version, i->version, 4)) {
				j = i;
			}
		}
	}
	if (j != m_blades.end()) // found it?
	{
		try {
			ret = IBladePtr(j->createBlade());
			typeid(*ret); // did they compile with RTTI? this will throw, and we'll catch it
			if (!caster.isMyType(ret)) {
				ret->FreeAll();
				ret = IBladePtr();
			}
		} catch (std::__non_rtti_object) {
			if (ret)
				ret->FreeAll();
			ret = IBladePtr();
			LogWarn(loadStr(IDS_WARN_NO_RTTI) << j->className);
		}
	}
	return ret;
}

IBladePtrVector BladeFactory::CreateBlades(BladeCaster& caster, ULONG myVersion) {
	IBladePtrVector ret;
	for (auto i = m_blades.begin(); i != m_blades.end(); i++) {
		// first two levels of version and name must match
		if (VersionEqual(myVersion, i->version, 2)) {
			//			IBladePtr pBlade;
			try {
				IBladePtr pBlade = i->createNew();
				if (pBlade) {
					typeid(*pBlade); // did they compile with RTTI? this will throw, and we'll catch it

					if (!caster.isMyType(pBlade)) {
						pBlade->FreeAll();
						pBlade = nullptr; //IBladePtr();
					} else {
						ret.push_back(pBlade);
					}
				}
			} catch (std::__non_rtti_object) {
				LogWarn(loadStr(IDS_WARN_NO_RTTI) << i->className);
				//				if ( pBlade )
				//					pBlade->FreeAll();
			}
		}
	}
	return ret;
}

void BladeFactory::Clear() {
	m_blades.clear();
}

} // namespace KEP
