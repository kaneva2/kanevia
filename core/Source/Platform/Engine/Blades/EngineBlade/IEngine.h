///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../IBlade.h"

#define ENGINE_VERSION PackVersion(1, 0, 0, 0)

namespace KEP {

class DBConfig;

class BLADE_EXPORT IEngine : public IBlade {
public:
	IEngine() {}

	virtual ~IEngine() {}

	///---------------------------------------------------------
	// Called to get blade configuration
	// For engines on the server only
	//
	// [in] cfg the config object loaded from file or db
	//
	// [Returns]
	//     TRUE if initalized ok, and the server should continue
	virtual bool LoadConfigValues(const IKEPConfig* cfg) = 0;

	///---------------------------------------------------------
	// Called to flush blade configuration
	// For engines on the server only
	//
	// [in] cfg the config object loaded from file or db
	//
	// [Returns]
	//     TRUE if initalized ok, and the server should continue
	virtual void SaveConfigValues(IKEPConfig* cfg) = 0;

	///---------------------------------------------------------
	// Initialize the engine.  Be sure to call the base class.
	//
	// [in] baseDir the base directory where the DLL was loaded from
	// [in] configFile the config file name since it may not
	//      be in the same folder as the baseDir
	//
	// [Returns]
	//     TRUE if initalized ok, and the server should continue
	virtual bool Initialize(const char* baseDir, const char* configFile, const DBConfig& dbConfig) = 0;

	///---------------------------------------------------------
	// Free all the allocated objects.  Be sure to call the base class.
	//
	virtual void FreeAll() = 0;

	///---------------------------------------------------------
	// Main processing loop.  Called in a loop by the server.
	//
	// [in] canWait true if this object can wait until it
	// can continue processing.
	//
	// [Returns]
	//     number of ms to wait to call this engine back
	virtual bool RenderLoop(bool canWait) = 0;

	///---------------------------------------------------------
	// Engines can have an instance, get that name
	//
	// [Returns]
	//     the instance name
	virtual const char* InstanceName() const = 0;

	///---------------------------------------------------------
	// Save the engine's state
	//
	// [Returns]
	//     true if saved ok
	virtual bool Save() { return true; }

	///---------------------------------------------------------
	// Is this engine enabled to run
	//
	// [Returns]
	//     true if saved ok
	virtual bool Enabled() const = 0;

	virtual int GetGameId() const = 0;
};

} // namespace KEP
