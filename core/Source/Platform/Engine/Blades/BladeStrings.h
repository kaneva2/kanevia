///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#define IDS_DIR_DOESNT_EXIST 11000
#define IDS_WARN_LOAD_BLADE 11001
#define IDS_WARN_LIB_VER 11002
#define IDS_WARN_NO_ENTPT 11003
#define IDS_WARN_BLADE_CREATE_FAILED 11004
#define IDS_INFO_DUP_BLADE_VER 11005
#define IDS_WARN_DUP_BLADE_VER 11006
#define IDS_NO_CLASSNAME 11007
#define IDS_WARN_NO_RTTI 11008
#define IDS_DUMP_PROMPT 11009
#define IDS_DUMPSAVED 11010
#define IDS_FAILED_COMMAND_FILE 11011
#define IDS_ERROR_INITIALIZING_BLADES 11012
#define IDS_ERROR_BLADES_NOT_LOADED 11013
#define IDS_ERROR_BLADES_NOT_INITIALIZED 11013
#define IDS_ERROR_INIT_BLADE 11014
#define IDS_BLADE_LOADED 11015

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE 102
#define _APS_NEXT_COMMAND_VALUE 40001
#define _APS_NEXT_CONTROL_VALUE 1001
#define _APS_NEXT_SYMED_VALUE 101
#endif
#endif
