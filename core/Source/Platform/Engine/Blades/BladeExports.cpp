///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#ifdef _AFXDLL

#ifndef WINVER
#define WINVER 0x0501
#endif

#include <afx.h>
#include <afxwin.h>
#endif

#include <js.h>
#include "BladeExports.h"

#include "IBlade.h"
using namespace KEP;

bool engineTypeSupported(eEngineType engineType);

extern BladeInfo gBladeInfo[];

extern "C" BLADE_EXPORT ULONG GetLibVersion() {
#ifdef _AFX
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
#endif
	return BLADE_LIB_VERSION;
}

extern "C" BLADE_EXPORT const char **GetBladeNames() {
#ifdef _AFX
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
#endif

	static const char **gBladeNames = nullptr;
	if (gBladeNames == nullptr) {
		int i;
		for (i = 0; gBladeInfo[i].createBlade != 0; i++)
			;
		gBladeNames = new const char *[i + 1];
		for (i = 0; gBladeInfo[i].createBlade != 0; i++) {
			gBladeNames[i] = gBladeInfo[i].className;
		}
		gBladeNames[i] = nullptr; // end the list
	}
	return gBladeNames;
}

extern "C" BLADE_EXPORT BladeInfo *GetBladesInfo(eEngineType engineType) {
#ifdef _AFX
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
#endif
	if (engineTypeSupported(engineType))
		return gBladeInfo;
	else
		return nullptr;
}

extern "C" BLADE_EXPORT IBlade *CreateBlade(const char *instanceName) {
#ifdef _AFX
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
#endif

	for (int i = 0; gBladeInfo[i].createBlade != 0; i++) {
		if (::strcmp(gBladeInfo[i].className, instanceName) == 0) {
			return gBladeInfo[i].createBlade();
		}
	}
	return nullptr;
}
