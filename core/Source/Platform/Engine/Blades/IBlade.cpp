/******************************************************************************
 IBlade.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#error Dead code?
#include "stdafx.h"
#include "IBlade.h"

namespace KEP {

BLADE_EXPORT IBlade::IBlade(ULONG version) :
		m_version(version) {
}

BLADE_EXPORT IBlade::~IBlade(void) {
}

///---------------------------------------------------------
// [Returns]
//    the version of the Blade
ULONG BLADE_EXPORT IBlade::Version() const {
	return m_version;
}

///---------------------------------------------------------
// [Returns]
//    the base directory for the server
const char* BLADE_EXPORT IBlade::BaseDir() const {
	return m_baseDir.c_str();
}

///---------------------------------------------------------
// Set the base directory.  Called by the server
//
// [in] dir the base directory
//
void BLADE_EXPORT IBlade::SetBaseDir(const char* dir) {
	m_baseDir = dir;
}

} // namespace KEP