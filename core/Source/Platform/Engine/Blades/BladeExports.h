///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "SDKCommon.h"

#include "IBlade.h"
#include "EngineTypes.h"

namespace KEP {

typedef IBlade* (*CREATE_BLADE_FN)();

typedef struct strBladeInfo {
	const char* className;
	CREATE_BLADE_FN createBlade;
	ULONG version;
} BladeInfo;

} // namespace KEP

#define BEGIN_BLADE_LIST                                                  \
	bool engineTypeSupported(eEngineType /*engineType*/) { return true; } \
	BladeInfo gBladeInfo[] = {
#define BEGIN_BLADE_LIST_ENGINE_TYPE(et)                                                                             \
	bool engineTypeSupported(eEngineType engineType) { return engineType == eEngineType::None || engineType == et; } \
	BladeInfo gBladeInfo[] = {
#define ADD_BLADE(className) { typeid(className).name(), create##className, className::BladeVersion() },
#define END_BLADE_LIST \
	{ 0, 0 }           \
	}                  \
	;

#ifdef _AFX
#define IMPLEMENT_CREATE(className)                  \
	IBlade* create##className() {                    \
		AFX_MANAGE_STATE(AfxGetStaticModuleState()); \
		return new className();                      \
	}
#else
#define IMPLEMENT_CREATE(className) \
	IBlade* create##className() { return new className(); }
#endif
