///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include <SDKCommon.h>
#include <memory>
#include "common/include/kepcommon.h"
#include "Core/Util/monitored.h"

#ifdef BUILD_BLADE
#define BLADE_EXPORT __declspec(dllexport)
#elif defined(USE_BLADE_DLL)
#define BLADE_EXPORT __declspec(dllimport)
#else
#define BLADE_EXPORT
#endif

// this forces version to be different for
// debug builds for base library
#define DEBUG_OFFSET 100
#ifdef _DEBUG
#define VER_OFFSET DEBUG_OFFSET
#else
#define VER_OFFSET 0
#endif

#define BLADE_LIB_VERSION PackVersion(1 + VER_OFFSET, 0, 0, 0)

// check to see if this is a debug version of the lib
inline bool IsDebugVersion(ULONG version) {
	BYTE v, x;
	UnpackVersion(version, v, x, x, x);
	return x >= DEBUG_OFFSET;
}

namespace KEP {

class BLADE_EXPORT IBlade : public Monitored {
public:
	IBlade() {}

	virtual ~IBlade() {}

	virtual ULONG Version() const = 0;

	// Client: BaseDir is <pathApp> for engine blades and <scriptPath> for event blades
	// Server: BaseDir is <pathXml> for engine blades and <scriptPath> for event blades
	virtual std::string PathBase() const = 0;
	virtual void SetPathBase(const std::string& baseDir, bool readonly) = 0;

	virtual void FreeAll() = 0;

	virtual const char* Name() const = 0;

protected:
	virtual bool IsBaseDirReadOnly() const = 0;
};

typedef std::shared_ptr<IBlade> IBladePtr;

} // namespace KEP
