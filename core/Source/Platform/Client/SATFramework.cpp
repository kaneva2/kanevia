///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SATFramework.h"
#include "LogHelper.h"

namespace KEP {

SATFramework* SATFramework::s_this;

SATFramework::SATFramework() {
	m_logger = Logger::getInstance(Utf8ToUtf16("SATFramework"));
	LogMessage("Log for SATFramework initialized");

	m_clientEngineInstance = IClientEngine::Instance();
	if (m_clientEngineInstance == nullptr) {
		LogMessage("Client Engine Instance not found. Exiting. Testing Failed");
		m_successfulCompletion = false;
		EndTestingAndCloseClient();
		return;
	}

	m_dispatcherInstance = m_clientEngineInstance->GetDispatcher();
	if (m_dispatcherInstance == nullptr) {
		LogMessage("Event Dispatcher Instance not found. Exiting. Testing Failed");
		m_successfulCompletion = false;
		EndTestingAndCloseClient();
		return;
	}

	m_successfulCompletion = true;

	m_indexAtInTestScriptExecution = -1;

	m_email = "";
	m_password = "";

	m_clientSpawned = false;
	m_zoneLoaded = false;

	m_testScriptsToRun.clear();
	std::string menuName;
	std::ifstream nameFile(PathAdd(m_clientEngineInstance->PathBase(), PathAdd(GAMEFILES, "listOfTestsToRun.txt")));
	if (nameFile.is_open()) {
		while (getline(nameFile, menuName))
			m_testScriptsToRun.push_back(menuName);

		nameFile.close();
		m_myCurrentState = ready;
	} else {
		m_myCurrentState = idle;
		LogMessage("listsOfTestsToRun.txt not found. Exiting. Testing Failed");
		m_successfulCompletion = false;
		EndTestingAndCloseClient();
		return;
	}

	m_dispatcherInstance->RegisterEvent("StartClientTestingEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_dispatcherInstance->RegisterEvent("TestScriptExecutionCompletedSuccessfullyEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);

	m_dispatcherInstance->RegisterHandler(SATFramework::StartClientTestingEventHandler, (ULONG)this, "StartClientTestingEvent", GenericEvent::ClassVersion(), m_dispatcherInstance->GetEventId("StartClientTestingEvent"), EP_NORMAL);
	m_dispatcherInstance->RegisterHandler(SATFramework::TestScriptExecutionCompletedSuccessfullyEventHandler, (ULONG)this, "TestScriptExecutionCompletedSuccessfullyEvent", GenericEvent::ClassVersion(), m_dispatcherInstance->GetEventId("TestScriptExecutionCompletedSuccessfullyEvent"), EP_NORMAL);

	LogMessage("SATFramework instance created");
}

SATFramework::~SATFramework() {
	if (s_this)
		delete (s_this);
}

SATFramework* SATFramework::GetSATInstance() {
	if (!s_this)
		s_this = new SATFramework();

	return s_this;
}

void SATFramework::executeTestScript(int index) {
	IEvent* e = m_dispatcherInstance->MakeEvent(m_dispatcherInstance->GetEventId("ExecuteTestSuiteEvent" + m_testScriptsToRun.at(index)));
	if (e) {
		m_dispatcherInstance->QueueEvent(e);
		LogMessage("Execution started for test script " + m_testScriptsToRun.at(index));
	} else {
		LogMessage("Could not create event to execute test script " + m_testScriptsToRun.at(index));
		m_successfulCompletion = false;
		EndTestingAndCloseClient();
	}
}

EVENT_PROC_RC SATFramework::StartClientTestingEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	if (s_this->m_myCurrentState != SATstate::executing) {
		if ((s_this->m_clientEngineInstance)->CheckIfMenuLoaded("LoginMenu") == true) {
			s_this->m_indexAtInTestScriptExecution = 0;
			s_this->m_myCurrentState = SATstate::executing;
			s_this->executeTestScript(0);
		}
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC SATFramework::TestScriptExecutionCompletedSuccessfullyEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	s_this->LogMessage("Execution completed successfully for test script " + (s_this->m_testScriptsToRun).at(s_this->m_indexAtInTestScriptExecution));
	if ((++(s_this->m_indexAtInTestScriptExecution)) < (s_this->m_testScriptsToRun).size())
		s_this->executeTestScript(s_this->m_indexAtInTestScriptExecution);
	else {
		s_this->m_myCurrentState = SATstate::idle;
		s_this->m_successfulCompletion = true;
		s_this->EndTestingAndCloseClient();
	}

	return EVENT_PROC_RC::CONSUMED;
}

void SATFramework::LogMessage(std::string msg) {
	LOG4CPLUS_INFO(m_logger, msg);
}

void SATFramework::EndTestingAndCloseClient() {
	if (m_successfulCompletion)
		LOG4CPLUS_INFO(m_logger, "All test scripts ran succesfully.");
	else
		LOG4CPLUS_INFO(m_logger, "Failure detected. Please check the logs and fix the client.");

	LogFatal("Shutting Down ...");
	PostMessage(m_clientEngineInstance->GetWindowHandle(), WM_CLOSE, NULL, NULL);
}

} // namespace KEP