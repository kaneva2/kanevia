///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common\include\KEPConstants.h"
#include "Core/Util/HighPrecisionTime.h"

class ClientMetrics {
public:
	// Enumerated FPS Identifiers (Frames Per Second)
	enum class FPS_ID {
		RUNTIME, // reset in ClientEngineApp::RR_AppCallback(REPORT_TYPE_STOPPED|CRASHED)
		CUSTOM, // reset in ClientEngineApp::RR_AppCallback(REPORT_TYPE_CUSTOM)
		FPS_IDS
	};

	ClientMetrics() {}

	void Reset();
	void Enable(bool enable);
	void Log();

	// Metrics Render API
	void RenderLoop();

	// Metrics FPS API
	bool FpsReset(FPS_ID fpsId);
	bool FpsEnable(FPS_ID fpsId, bool enable);
	bool Fps(FPS_ID fpsId, double& fps) const;
	bool FpsUpdate(FPS_ID fpsId);
	bool FpsLog(FPS_ID fpsId);

private:
	// Metrics FPS Statistics
	struct FPS_STRUCT {
		bool enabled;
		Timer timer;
		ULONGLONG frames;
		FPS_STRUCT() :
				enabled(false), timer(Timer::State::Paused), frames(0) {}
	} fps[FPS_ID::FPS_IDS];
};
