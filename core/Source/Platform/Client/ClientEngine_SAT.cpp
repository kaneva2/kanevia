///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ClientEngine.h"
#include "RuntimeSkeleton.h"
#include "imenugui.h"
#include "common\include\NetworkCfg.h"
#include "config.h"

namespace KEP {

static LogInstance("ClientEngine");

void ClientEngine::LogMsgForSAT(std::string msg) {
	m_SATFrameworkInstance->LogMessage(msg);
}

int ClientEngine::GETGetSetInt(std::string varName) {
	return GetINT(FindIdByName(varName.c_str()));
}

void ClientEngine::SETGetSetInt(std::string varName, int value) {
	ULONG varID = FindIdByName(varName.c_str());
	if (varID == FIND_ID_NOT_FOUND) {
		varID = AddNewIntMember(varName.c_str(), 0, amfTransient);
	}
	SetNumber(varID, value);
}

std::string ClientEngine::GETGetSetString(std::string varName) {
	return GetString(FindIdByName(varName.c_str()));
}

void ClientEngine::SETGetSetString(std::string varName, std::string value) {
	ULONG varID = FindIdByName(varName.c_str());
	if (varID == FIND_ID_NOT_FOUND) {
		varID = AddNewStringMember(varName.c_str(), "", amfTransient);
	}
	SetString(varID, value.c_str());
}

void ClientEngine::SetScrollBarInMenuToContainIndex(std::string lstName, std::string menuName, int index) {
	auto kep_menu = KepMenuInstance();
	IMenuDialog* menu = kep_menu->GetDialog(menuName.c_str());
	IMenuListBox* controlInMenu = (IMenuListBox*)menu->GetListBox(lstName.c_str());
	if (controlInMenu != nullptr) {
		IMenuScrollBar* sb = controlInMenu->GetScrollBar();
		if (sb != nullptr) {
			sb->SetTrackPos(index - sb->GetPageSize() + 1);
		}
	}
}

void ClientEngine::SetTextForEditBoxInMenu(std::string text, std::string edtBox, std::string menuName) {
	auto kep_menu = KepMenuInstance();
	IMenuDialog* menu = kep_menu->GetDialog(menuName.c_str());
	IMenuEditBox* controlInMenu = (IMenuEditBox*)menu->GetIMenuEditBox(edtBox.c_str());
	controlInMenu->SetText(text.c_str());
}

void ClientEngine::ComboBoxSetSelectedByDataInMenu(int data, std::string cmbBox, std::string menuName) {
	auto kep_menu = KepMenuInstance();
	IMenuDialog* menu = kep_menu->GetDialog(menuName.c_str());
	IMenuComboBox* controlInMenu = (IMenuComboBox*)menu->GetIMenuComboBox(cmbBox.c_str());
	controlInMenu->SetSelectedByData(&data);
}

std::string ClientEngine::GetInfoBoxMessage() {
	std::string msg = "";
	auto kep_menu = KepMenuInstance();
	if (CheckIfMenuLoaded("InfoBox")) {
		IMenuDialog* menu = kep_menu->GetDialog("InfoBox");
		IMenuStatic* controlInMenu = (IMenuStatic*)menu->GetIMenuStatic("lblMessage");
		if (controlInMenu != nullptr)
			msg = controlInMenu->GetText();
	}
	return msg;
}

bool ClientEngine::IsButtonInMenuEnabled(std::string btnName, std::string menuName) {
	auto kep_menu = KepMenuInstance();
	IMenuDialog* menu = kep_menu->GetDialog(menuName.c_str());
	IMenuControl* controlInMenu = menu->GetControl(btnName.c_str());
	return controlInMenu->GetEnabled();
}

void ClientEngine::UseButtonInsideMenu(std::string btnName, std::string menuName) {
	auto kep_menu = KepMenuInstance();
	IMenuDialog* menu = kep_menu->GetDialog(menuName.c_str());
	IMenuControl* controlInMenu = menu->GetControl(btnName.c_str());
	if (!controlInMenu->GetEnabled()) {
		LogMsgForSAT("Button " + btnName + " trying to be used is disabled inside menu " + menuName);
		FinishTestingAndCloseClient(false);
		return;
	}

	if (((btnName == "btnExit") || (btnName == "btnSettings")) && (menuName == "HUDBuild"))
		menu->SendEvent(EVENT_CONTROL_LBUTTONDOWN, true, controlInMenu);
	else
		menu->SendEvent(EVENT_BUTTON_CLICKED, true, controlInMenu);
}

void ClientEngine::UseRadioButtonInsideMenu(std::string radioBtnName, std::string menuName) {
	auto kep_menu = KepMenuInstance();
	IMenuDialog* menu = kep_menu->GetDialog(menuName.c_str());
	IMenuControl* controlInMenu = menu->GetControl(radioBtnName.c_str());

	menu->SendEvent(EVENT_RADIOBUTTON_CHANGED, true, controlInMenu);
}

bool ClientEngine::CheckIfMenuLoaded(std::string menuName) {
	auto kep_menu = KepMenuInstance();
	if (kep_menu->MenuIsOpen(menuName.c_str()))
		return true;
	else
		return false;
}

bool ClientEngine::CheckIfScreenshotImageSaved() {
	std::ifstream imageFileStream(m_captureScreenPath);
	if (imageFileStream.good()) {
		imageFileStream.close();
		return true;
	} else {
		imageFileStream.close();
		return false;
	}
}

void ClientEngine::CheckIfPacketRedeemedForUser(int packetId, int userId) {
	std::string webcall = "kgp/fameInfo.aspx?action=GetPacketForUser&packetId=" + std::to_string(packetId) + "&userId=" + std::to_string(userId);
	if (GetBrowserPageAsync(webcall, NULL, 219) == false) {
		LogMsgForSAT("Webcall " + webcall + "Failed");
		FinishTestingAndCloseClient(false);
	}
}

bool ClientEngine::CheckIfParticleEffectPlayingOnPlayer(int particleGlid, int netTraceId) {
	auto pMO = GetMovementObjByNetId(netTraceId);
	if (!pMO || !pMO->getSkeleton())
		return false;

	bool found = false;
	pMO->getSkeleton()->iterateParticleEmitters([&found, particleGlid](CParticleEmitRangeObj* emitPtr) {
		if (emitPtr && (emitPtr->getParticleGLID() == particleGlid)) {
			found = true;
			return false;
		}
		return true;
	});
	return found;
}

std::vector<int> ClientEngine::GetItemsArmedByPlayer(int particleGlid, int netTraceId) {
	std::vector<int> itemsArmedByPlayer;
	POSITION dbPosLast;
	auto pMO = GetMovementObjMe();
	if (!pMO) {
		LogError("GetMovementObj() FAILED");
		return itemsArmedByPlayer;
	}

	// DRF - List Lock Crash Fix
	auto pCAI = pMO->getCurrentlyArmedItems();
	if (pCAI) {
		OBJ_LIST_GUARD(pCAI);
		for (POSITION dbPos = pCAI->GetHeadPosition(); (dbPosLast = dbPos) != NULL;) {
			auto pIO = static_cast<const CItemObj*>(pCAI->GetNext(dbPos));
			if (!pIO)
				continue;
			itemsArmedByPlayer.push_back(pIO->m_globalID);
		}
	}

	return itemsArmedByPlayer;
}

bool ClientEngine::IsPlayerAP() {
	return MyPassGroupAP();
}

bool ClientEngine::IsPlayerVIP() {
	return MyPassGroupVIP();
}

bool ClientEngine::IsDynamicObjectWithPIDValid(int pid) {
	CDynamicPlacementObj* placedObj = GetDynamicObject(pid);
	if (!placedObj)
		return false;
	else
		return true;
}

void ClientEngine::CreateTestUser(std::string username, std::string password) {
	std::string webcall = std::string() + NET_KGP + "userProfile.aspx?action=generateTestUser&username=" + username + "&password=" + password + "&tugkey=";
	webcall += tugkey;
	if (GetBrowserPageAsync(webcall, NULL, 220) == false) {
		LogMsgForSAT("Webcall " + webcall + "Failed");
		FinishTestingAndCloseClient(false);
	}
}

void ClientEngine::PostKeyboardMouseMsg(bool isKeybdInput, int isPressed, int code, int dx, int dy) {
	INPUT i;
	if (isKeybdInput) {
		i.type = 1;
		(i.ki).wVk = code;
		(i.ki).dwFlags = 0;
		(i.ki).time = 0;
		(i.ki).wScan = MapVirtualKey(code, MAPVK_VK_TO_VSC);
		(i.ki).dwExtraInfo = 0;
		if (!isPressed)
			(i.ki).dwFlags |= KEYEVENTF_KEYUP;
	} else {
		i.type = 0;
		(i.mi).dx = dx;
		(i.mi).dy = dy;
		(i.mi).time = 0;
		(i.mi).dwExtraInfo = 0;
		(i.mi).mouseData = 0;
		DWORD mouseEvent[3][3];
		mouseEvent[0][0] = MOUSEEVENTF_LEFTUP;
		mouseEvent[0][1] = MOUSEEVENTF_MIDDLEUP;
		mouseEvent[0][2] = MOUSEEVENTF_RIGHTUP;
		mouseEvent[1][0] = MOUSEEVENTF_LEFTDOWN;
		mouseEvent[1][1] = MOUSEEVENTF_MIDDLEDOWN;
		mouseEvent[1][2] = MOUSEEVENTF_RIGHTDOWN;
		mouseEvent[2][0] = MOUSEEVENTF_MOVE;
		mouseEvent[2][1] = MOUSEEVENTF_MOVE;
		mouseEvent[2][2] = MOUSEEVENTF_MOVE;
		(i.mi).dwFlags = mouseEvent[isPressed][code];
	}
	if (SendInput(1, &i, sizeof(i)) <= 0) {
		LogMsgForSAT("Couldn't post keyboard/mouse msg for client");
		FinishTestingAndCloseClient(false);
	}
}

static BOOL CALLBACK EnumWindowsProcAndCloseBrowsers(HWND hwnd, LPARAM lParam) {
	char browserName[64];
	char browserTitle[512];
	GetClassNameA(hwnd, browserName, sizeof(browserName));
	GetWindowTextA(hwnd, browserTitle, sizeof(browserTitle));
	if (((std::string)browserName == "IEFrame") || ((std::string)browserName == "Chrome_WidgetWin_1") || ((std::string)browserName == "MozillaWindowClass")) {
		if ((std::string)browserTitle != "") {
			SendMessage(hwnd, WM_CLOSE, NULL, NULL);
		}
	}

	return TRUE;
}

void ClientEngine::CloseAllBrowsers() {
	EnumWindows(EnumWindowsProcAndCloseBrowsers, NULL);
}

bool ClientEngine::IsBrowserOpenWithTitleAndURL(std::string windowClassName, std::string titleOfWebpage, std::string URL) {
	bool found = false;

	HWND browserWindow = FindWindowA(windowClassName.c_str(), titleOfWebpage.c_str());
	if (browserWindow != NULL) {
		SwitchToThisWindow(browserWindow, false);

		INPUT i;
		i.type = 1;
		(i.ki).time = 0;
		(i.ki).dwExtraInfo = 0;

		(i.ki).dwFlags = 0;
		(i.ki).wVk = VK_LCONTROL;
		(i.ki).wScan = MapVirtualKey(VK_LCONTROL, MAPVK_VK_TO_VSC);
		SendInput(1, &i, sizeof(i));
		(i.ki).wVk = 0x4C;
		(i.ki).wScan = MapVirtualKey(0x4C, MAPVK_VK_TO_VSC);
		SendInput(1, &i, sizeof(i));
		Sleep(5);
		(i.ki).dwFlags = KEYEVENTF_KEYUP;
		(i.ki).wVk = 0x4C;
		(i.ki).wScan = MapVirtualKey(0x4C, MAPVK_VK_TO_VSC);
		SendInput(1, &i, sizeof(i));
		(i.ki).wVk = VK_LCONTROL;
		(i.ki).wScan = MapVirtualKey(VK_LCONTROL, MAPVK_VK_TO_VSC);
		SendInput(1, &i, sizeof(i));
		Sleep(1000);
		(i.ki).dwFlags = 0;
		(i.ki).wVk = VK_LCONTROL;
		(i.ki).wScan = MapVirtualKey(VK_LCONTROL, MAPVK_VK_TO_VSC);
		SendInput(1, &i, sizeof(i));
		(i.ki).wVk = 0x43;
		(i.ki).wScan = MapVirtualKey(0x43, MAPVK_VK_TO_VSC);
		SendInput(1, &i, sizeof(i));
		Sleep(5);
		(i.ki).dwFlags = KEYEVENTF_KEYUP;
		(i.ki).wVk = 0x43;
		(i.ki).wScan = MapVirtualKey(0x43, MAPVK_VK_TO_VSC);
		SendInput(1, &i, sizeof(i));
		(i.ki).wVk = VK_LCONTROL;
		(i.ki).wScan = MapVirtualKey(VK_LCONTROL, MAPVK_VK_TO_VSC);
		SendInput(1, &i, sizeof(i));

		if (::OpenClipboard(nullptr)) {
			HANDLE hData = GetClipboardData(CF_TEXT);
			if (hData != nullptr) {
				char* pText = static_cast<char*>(GlobalLock(hData));
				if (pText != nullptr) {
					std::string text(pText);
					if (text.find(URL) != std::string::npos)
						found = true;
				}
				GlobalUnlock(hData);
			}
			CloseClipboard();
		}
	}

	return found;
}

bool ClientEngine::IsChatWindowInForeground() {
	HWND chatWindow = FindWindowA("gdkWindowToplevel", (GetString(IDS_CLIENTENGINE_LOGINNAME) + " - home").c_str());
	if (&chatWindow == (HWND*)GetForegroundWindow())
		return true;
	else
		return false;
}

void ClientEngine::BringKanevaClientToForeground() {
	SetFocus();
}

void ClientEngine::FinishTestingAndCloseClient(bool successfulCompletion) {
	m_SATFrameworkInstance->setSuccessfulCompletion(successfulCompletion);
	m_SATFrameworkInstance->EndTestingAndCloseClient();
}

void ClientEngine::GetDevOptionsDefinedInEngine(std::string& devOptionsCombinedString) {
	devOptionsCombinedString = "";
	for (std::map<std::string, std::function<void()>>::iterator it = mapForDevOptionFunctions.begin(); it != mapForDevOptionFunctions.end(); it++) {
		devOptionsCombinedString.append(it->first);
		devOptionsCombinedString.append("&&");
	}
}

void ClientEngine::ExecuteFunction(std::string funcName) {
	std::map<std::string, std::function<void()>>::iterator it = mapForDevOptionFunctions.find(funcName);
	if (it != mapForDevOptionFunctions.end())
		it->second();
}

} // namespace KEP