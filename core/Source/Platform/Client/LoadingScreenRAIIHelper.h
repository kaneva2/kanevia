///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "EventHeader.h"

namespace KEP {

class Dispatcher;

// A helper class to start and end the loading screen regardless of which early return we hit.
class LoadingScreenRAIIHelper {
	bool m_bStarted;
	Dispatcher& m_dispatcher;
	EVENT_ID m_zoneLoadingStartEventId;
	EVENT_ID m_zoneLoadingCompleteEventId;
	EVENT_ID m_zoneLoadingFailedEventId;
	bool m_syncEvents; ///< DRF - process events synchronously
public:
	bool m_success;

	LoadingScreenRAIIHelper(Dispatcher& dispatcher, EVENT_ID startID, EVENT_ID successID, EVENT_ID failedID, bool syncEvents);

	void Start();
	void Stop();

	~LoadingScreenRAIIHelper() {
		Stop();
	}

private:
	void DispatchEvent(EVENT_ID eventId);
};

} // namespace KEP