///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "WebCallTask.h"
#include "common\KEPUtil\Helpers.h"

namespace KEP {

static LogInstance("Instance");

std::string WebCallTask::GetWebCallerId() {
	return WebCaller::GetInstance(WCT_WEBCALLER, true, WCT_THREADS) ? WCT_WEBCALLER : "";
}

void WebCallTask::ExecuteTask() {
	// Blocking Web Call
	DWORD httpStatusCode = 0;
	std::string resultText;
	WebCallAndWait(m_url, true, httpStatusCode, resultText, m_startIndex, m_maxItems, m_retries, m_timeoutMs);

	// If the user is not logged in, then log them in now
	if (httpStatusCode == HTTP_STATUS_KANEVA_AUTH) {
		if (m_urlAuth.size() > 0) {
			// Issue the login request (showing password in log = fail)
			DWORD authStatus = 0;
			std::string authText;
			bool ok = WebCallAndWait(m_urlAuth, false, authStatus, authText, -1, -1, m_retries, m_timeoutMs);
			if (ok && authStatus == HTTP_STATUS_OK) {
				LogInfo("KANEVA_AUTH OK");
				WebCallAndWait(m_url, true, httpStatusCode, resultText, m_startIndex, m_maxItems, m_retries, m_timeoutMs);
			} else {
				LogError("KANEVA_AUTH FAILED");
			}
		}
	}

	// 'BrowserPageReadyEvent' is always sent
	// Send the completion events, this needs to happen on success or
	// failure.  Caller should examine resultText and httpStatusCode
	// to determine what happened.
	IEvent* pEvent = m_dispatcher.MakeEvent(m_browserPageReadyEventId, 0);
	if (pEvent) {
		pEvent->SetFilter(m_eventFilter);
		(*pEvent->OutBuffer()) << resultText.c_str() << (int)httpStatusCode << m_url << m_startIndex << m_maxItems;
		m_dispatcher.QueueEvent(pEvent);
	}

	// A per-request completion event may also be specified
	if (m_pEventCompletion) {
		(*m_pEventCompletion->OutBuffer()) << resultText.c_str() << (int)httpStatusCode << m_url << m_startIndex << m_maxItems;
		m_dispatcher.QueueEvent(m_pEventCompletion);
	}
}

bool WebCallTask::WebCallAndWait(const std::string& url, bool logUrl, DWORD& httpStatusCode, std::string& resultText, int startIndex, int maxItems, size_t retries, TimeMs timeoutMs) {
	// Clear Return Values
	httpStatusCode = 0;
	resultText.clear();

	// Set Web Call Options (blocking + response)
	WebCallOpt wco;
	wco.url = url;
	wco.response = true;
#ifndef _DEBUG
	if (!logUrl)
		wco.verbose &= ~WC_VERBOSE_URL; // jeff's request
#endif
	wco.timeoutMs = timeoutMs;

	// Add Extra Url Parameters
	if (startIndex > 0)
		wco.AddParam("start", startIndex);
	if (maxItems > 0)
		wco.AddParam("max", maxItems);

	// Get Web Caller Id
	std::string wcId = GetWebCallerId();
	if (wcId.empty())
		return false;

	// Do Web Call And Wait (get response)
	for (size_t i = 0; i < retries; ++i) {
		DWORD errorCode = 0;
		if (WebCaller::WebCallAndWaitOk(wcId, wco, resultText, errorCode, httpStatusCode))
			return true;
	}

	return false;
}

} // namespace KEP