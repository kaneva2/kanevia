///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "TaskObject.h"
#include "Event\EventSystem\Dispatcher.h"

#include "Common\KEPUtil\WebCall.h"

#define WCT_WEBCALLER "WebCallTask"
#define WCT_THREADS 4
#define WCT_TIMEOUT_MS WEB_CALL_OPT_TIMEOUT_MS
#define WCT_RETRIES 3

namespace KEP {

class WebCallTask : public TaskObject {
public:
	// DRF - Returns web caller identifier after initialization or empty string on error.
	static std::string GetWebCallerId();

	WebCallTask(Dispatcher& dispatcher) :
			m_dispatcher(dispatcher),
			m_pEventCompletion(nullptr),
			m_startIndex(-1),
			m_maxItems(-1),
			m_timeoutMs(WCT_TIMEOUT_MS),
			m_retries(WCT_RETRIES) {}

	virtual void ExecuteTask() override;

	bool WebCallAndWait(const std::string& url, bool logUrl, DWORD& httpStatusCode, std::string& resultText, int startIndex, int maxItems, size_t retries, TimeMs timeoutMs);

	Dispatcher& m_dispatcher;

	std::string m_url;
	std::string m_urlAuth;
	EVENT_ID m_browserPageReadyEventId;
	int m_eventFilter;
	IEvent* m_pEventCompletion;
	TimeMs m_timeoutMs;
	size_t m_retries;
	int m_startIndex;
	int m_maxItems;
};

} // namespace KEP