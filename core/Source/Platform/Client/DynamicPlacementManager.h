///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <unordered_map>
#include <functional>
#include <Core/Util/FunctionTraits.h>
#include <Common/include/CoreStatic/DynamicObjectSubListId.h>

namespace KEP {

class CDynamicPlacementObj;

class DynamicPlacementManager {
public:
	DynamicPlacementManager();
	~DynamicPlacementManager();

	void Log(bool verbose = true);

	size_t GetObjectCount() const {
		return m_apDynamicObjects.size();
	}

	void GetObjectMemoryUsage(size_t& objVis, size_t& bytesVis, size_t& objInv, size_t& bytesInv) const;

	CDynamicPlacementObj* Add(std::unique_ptr<CDynamicPlacementObj> pDPO); // Returns object added. Returns nullptr on failure.

	bool Remove(CDynamicPlacementObj* pDPO);

	void RemoveAll();

	void ReleaseAll(); // For quick exits. Does not delete objects.

	CDynamicPlacementObj* LookUpByPlacementId(int id) const;

	CDynamicPlacementObj* LookUpByIndex(size_t idx) const;

	void UpdateCustomMaterials(bool chainLoad); // drf - added

	// Adds/removes object from sublist
	void UpdateObjectInSubList(CDynamicPlacementObj* pDPO, size_t iSubListId);

	// Iteration
	template <typename Callable>
	__forceinline void IterateObjects(Callable&& callable) { DoIterateObjects<CDynamicPlacementObj*>(m_apDynamicObjects, std::forward<Callable>(callable)); }
	template <typename Callable>
	__forceinline void IterateObjects(Callable&& callable) const { DoIterateObjects<const CDynamicPlacementObj*>(m_apDynamicObjects, std::forward<Callable>(callable)); }

	template <typename Callable>
	void IterateObjectsForUpdate(Callable&& callable) {
		if (m_bUpdating)
			return; // Nested iterations are not allowed for updates.
		m_bUpdating = true;
		IterateObjectsSubList<DynObjSubListId_NeedsUpdate>(std::forward<Callable>(callable));
		ApplyPendingUpdateChanges();
		m_bUpdating = false;
	}

	template <typename Callable>
	void IterateObjectsForBoundingBoxRender(Callable&& callable) { IterateObjectsSubList<DynObjSubListId_BoundingBoxRender>(std::forward<Callable>(callable)); }

	template <typename Callable>
	void IterateObjectsWithMedia(Callable&& callable) { IterateObjectsSubList<DynObjSubListId_HasMedia>(std::forward<Callable>(callable)); }

	template <typename Callable>
	void IterateObjectsWithMedia(Callable&& callable) const { IterateObjectsSubList<DynObjSubListId_HasMedia>(std::forward<Callable>(callable)); }

	template <DynObjSubListId slid, typename Callable>
	void IterateObjectsSubList(Callable&& callable) {
		DoIterateObjects<CDynamicPlacementObj*>(m_apDynamicObjectSubLists[slid], std::forward<Callable>(callable));
	}

	template <DynObjSubListId slid, typename Callable>
	void IterateObjectsSubList(Callable&& callable) const {
		DoIterateObjects<const CDynamicPlacementObj*>(m_apDynamicObjectSubLists[slid], std::forward<Callable>(callable));
	}

private:
	// Iteration function that abstracts out const-ness of CDynamicPlacementObj and exits early if iteration function returns false.
	template <typename DynObj, typename Container, typename Callable>
	__forceinline static void DoIterateObjects(Container& container, Callable&& callable) {
		for (DynObj pDynamicPlacementObj : container) {
			bool bContinue = true;
			CallPossiblyVoidFunction(&bContinue, std::forward<Callable>(callable), pDynamicPlacementObj);
			if (!bContinue)
				break;
		}
	}

	void ApplyPendingUpdateChanges();
	void ApplyUpdateChange(CDynamicPlacementObj* pDPO);

	struct HelperFunctions; // Functions that I don't want to declare in the header to avoid additional #include dependencies.
private:
	std::unordered_map<int, std::unique_ptr<CDynamicPlacementObj>> m_mapDynamicObjects;
	std::vector<CDynamicPlacementObj*> m_apDynamicObjects;
	std::vector<CDynamicPlacementObj*> m_apDynamicObjectSubLists[NumberOfDynObjSubListIds];

	bool m_bUpdating = false; // Set to true while iterating for update.
	std::vector<CDynamicPlacementObj*> m_aPendingChangesToUpdateList; // Changes made while iterating. Applied when iteration completes.
};

} // namespace KEP
