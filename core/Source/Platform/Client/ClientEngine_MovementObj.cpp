///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "Common\include\CoreStatic\SkeletonConfiguration.h"
#include "common\include\CoreStatic\CSphereCollisionClass.h"
#include "Common\include\CoreStatic\CServerItemClass.h"
#include "RuntimeSkeleton.h"
#include "CPhysicsEmuClass.h"
#include "Common\KEPUtil\FrameTimeProfiler.h"
#include "ResourceManagers\SkeletalAnimationManager.h"

namespace KEP {

static LogInstance("ClientEngine");

std::vector<int> ClientEngine::getAllMovementObjectNetTraceIds() {
	std::vector<int> results;
	for (POSITION pos = m_movObjList->GetHeadPosition(); pos;) {
		auto pMO = (CMovementObj*)m_movObjRefModelList->GetNext(pos);
		if (pMO)
			results.push_back(pMO->getNetTraceId());
	}
	return results;
}

int ClientEngine::GetMyRefModelIndex() {
	auto pMO = GetMovementObjMe();
	return (pMO ? pMO->getIdxRefModel() : 0);
}

void ClientEngine::LogMovementObjectList(bool verbose) {
	LogInfo("objects=" << m_movObjList->GetCount() << " ...");
	if (!verbose)
		return;
	for (POSITION pos = m_movObjList->GetHeadPosition(); pos;) {
		auto pMO = (CMovementObj*)m_movObjList->GetNext(pos);
		if (!pMO)
			continue;
		pMO->logArmed();
	}
}

int ClientEngine::GetNumPlayers() {
	if (!m_movObjList)
		return 0;
	return m_movObjList->GetCount();
}

bool ClientEngine::GetMovementObjectPosRot(CMovementObj* pMO, Vector3f& pos, Vector3f& rot) {
	if (!pMO)
		return false;

	pos = pMO->getBaseFramePosition();
	rot = pMO->getBaseFrameMatrix().GetRowZ();
	return true;
}

void ClientEngine::configMovementObjectSubstitute(int edbIdx, ObjectType type, int globalId) {
	m_entitySubstitutes[edbIdx] = SubstituteRecord(type, globalId);
}

void ClientEngine::loadMovementObjectSubstitute(CMovementObj* pMO) {
	if (!pMO)
		return;

	auto itr_sub = m_entitySubstitutes.find(pMO->getIdxRefModel());
	if (itr_sub == m_entitySubstitutes.end())
		return;

	SubstituteRecord subRec = itr_sub->second;
	int runtimeId = 0;
	switch (subRec.type) {
		case ObjectType::DYNAMIC: {
			Vector3f pos, dir, up;
			pMO->getBaseFramePosDirUp(pos, dir, up);
			runtimeId = PlaceLocalDynamicObject(subRec.globalId, &pos, &dir);
			auto pDPO = GetDynamicObject(runtimeId);
			if (pDPO)
				pDPO->AddSkeletonLabels(*pMO->getSkeleton()->getOverheadLabels());
		} break;
	}

	if (runtimeId != 0)
		pMO->setSubstitute(subRec.type, runtimeId);
}

void ClientEngine::removeMovementObjectSubstitute(CMovementObj* pMO) {
	ObjectType objType = ObjectType::NONE;
	int objId = 0;
	if (!pMO || !pMO->getSubstitute(objType, objId))
		return;

	switch (objType) {
		case ObjectType::DYNAMIC:
			SendRemoveDynamicObjectEvent(objId);
			break;
	}
	pMO->setSubstitute(ObjectType::NONE, 0);
}

bool ClientEngine::isMovementObjectVisible(CMovementObj* pMO) const {
	if (!pMO || !pMO->isVisible())
		return false;

	ObjectType objType;
	int objId;
	if (pMO->getSubstitute(objType, objId)) {
		switch (objType) {
			case ObjectType::DYNAMIC: {
				auto pDPO = GetDynamicObject(objId);
				if (pDPO)
					return pDPO->hasSkeleton() && !pDPO->m_culled;
			} break;
		}
	} else {
		auto pSkeleton = pMO->getSkeleton();
		return pSkeleton && !pSkeleton->isCulled();
	}

	return false;
}

bool ClientEngine::GetMovementObjectByPlacementId(int placement, CMovementObj*& pMO, CMovementObj*& pMORM) {
	auto itr = m_npcs.find(placement);
	if (itr != m_npcs.end()) {
		pMO = itr->second;
		pMORM = GetMovementObjRefModel(pMO);
		assert(pMORM != nullptr);
		return true;
	}

	pMO = pMORM = nullptr;
	return false;
}

CMovementObj* ClientEngine::SpawnStaticMovementObject(
	int siType, // SERVER_ITEM_TYPE
	const std::string& name,
	const CharConfig* pCharConfig,
	const std::vector<GLID>& glidsArmedVec,
	int dbIndex,
	const Vector3f& scale,
	int netId,
	const Vector3f& pos,
	double rotDeg,
	const GLID& glidAnimSpecial,
	int placementId,
	bool networkEntity) {
	// Check if already spawned (might be getting multiple SPAWN_STATIC_MO messages for the same NPC)
	auto pMO = GetMovementObjByNetId(netId);
	if (pMO) {
		assert(placementId == 0 || m_npcs.find(placementId) != m_npcs.end());
		return pMO;
	}

	pMO = ClientBaseEngine::SpawnStaticMovementObject(siType, name, pCharConfig, glidsArmedVec, dbIndex, scale, netId, pos, rotDeg, glidAnimSpecial, placementId, networkEntity);
	if (pMO != nullptr && placementId != 0) {
		// Replacing Existing Paper Doll NPC
		auto itr = m_npcs.find(placementId);
		if (itr != m_npcs.end()) {
			CMovementObj* pMO_existing = itr->second;
			assert(pMO_existing->getNetTraceId() != netId);
			InvalidateDynamicObject(placementId);
			m_npcs.erase(itr);
			LogInfo("NPC [" << placementId << "] replace " << pMO_existing->ToStr() << " with " << pMO->ToStr());
		} else {
			LogInfo("NPC [" << placementId << "] spawn " << pMO->ToStr());
		}
		m_npcs.insert(std::make_pair(placementId, pMO));
		pMO->setSubstituteAllowed(false);
	}

	CDynamicPlacementObj* pDPO = GetDynamicObject(placementId);
	if (pDPO)
		pDPO->MovementObjectReady();

	return pMO;
}

bool ClientEngine::DestroyMovementObjectList() {
	bool res = ClientBaseEngine::DestroyMovementObjectList();
	m_npcs.clear();
	return res;
}

bool ClientEngine::DestroyMovementObject(CMovementObj* pMO) {
	if (!pMO)
		return false;

	if (pMO->hasSubstitute())
		removeMovementObjectSubstitute(pMO);

	// Remove Movement Object Cameras
	if (m_pRuntimeCameras) {
		for (size_t cameraIndex = 0; cameraIndex < pMO->getCameraCount(); cameraIndex++) {
			auto cameraData = pMO->getCameraData(cameraIndex);
			if (cameraData.m_runtimeCameraId > 0) {
				auto pCO = m_pRuntimeCameras->GetCamera(cameraData.m_runtimeCameraId);
				m_runtimeViewportDB->DelCameraCallback(pCO);
				m_pRuntimeCameras->Del(pCO);
				pMO->setRuntimeCameraId(cameraIndex, 0);
			}
		}
	}

	// Remove Movement Object From NPC List
	for (auto itr = m_npcs.begin(); itr != m_npcs.end(); ++itr) {
		auto pMO_find = itr->second;
		if (pMO_find != pMO)
			continue;
		int placementId = itr->first;
		LogInfo("NPC InvalidateDynamicObject - objId=" << placementId << " -> " << pMO->ToStr());
		InvalidateDynamicObject(placementId);
		m_npcs.erase(itr);
		break;
	}

	return ClientBaseEngine::DestroyMovementObject(pMO);
}

void ClientEngine::GetMyInfo(std::vector<std::string>& pInfo) {
	int nParamCount = 0;
	int nStatCount = 0;
	int nDefenseCount = 0;

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;

	auto pMC = pMO->getMovementCaps();
	if (!pMC)
		return;

	Vector3f lastPosition = pMC->GetPosLast();

	// indexes start at 3 since we pre-pend the counts, etc. at the beginning after
	// adding all of them

	//energy 3
	pInfo.push_back("0");
	nParamCount++;

	// max energy 4
	pInfo.push_back("0");
	nParamCount++;

	//Location x 5
	pInfo.push_back(std::to_string(lastPosition.x));
	nParamCount++;

	//Location y 6
	pInfo.push_back(std::to_string(lastPosition.y));
	nParamCount++;

	//Location z 7
	pInfo.push_back(std::to_string(lastPosition.z));
	nParamCount++;

	//cash on hand 8
	pInfo.push_back(std::to_string(m_multiplayerObj->m_cashCount));
	nParamCount++;

	//cash in bank 9
	pInfo.push_back(std::to_string(m_multiplayerObj->m_bankCashCount));
	nParamCount++;

	//player name 10
	pInfo.push_back(pMO->getName());
	nParamCount++;

	//trading with name 11
	pInfo.push_back(m_multiplayerObj->m_currentlyTradingWith);
	nParamCount++;

	//current title 12
	pInfo.push_back(""); //m_multiplayerObj->m_titleLink.GetString());
	nParamCount++;

	//current stasis 13
	pInfo.push_back("NORMAL"); //m_multiplayerObj->m_currentStasis.GetString());
	nParamCount++;

	//helper string 14
	pInfo.push_back(m_interactStr.empty() ? "" : m_interactStr.c_str());
	nParamCount++;

	//compass 15
	pInfo.push_back(GetCompassDir(pMO));
	nParamCount++;

	//Jet max and curr 16,17
	pInfo.push_back("0"); // DEPRECATED
	pInfo.push_back("0"); // DEPRECATED
	nParamCount += 2;

	//Stat 1-3  18, 19, 20
	nStatCount = 3;
	pInfo.push_back("0"); //m_multiplayerObj->m_currentStat1));
	pInfo.push_back("0"); //m_multiplayerObj->m_currentStat2));
	pInfo.push_back("0"); //m_multiplayerObj->m_currentStat3));

	//defenses (armor class, resistences, etc ) 21+
	const int maxAcTypes = 24;
	pInfo.push_back(std::to_string(maxAcTypes));

	// index 0: number of base parameters
	pInfo.insert(pInfo.begin(), std::to_string(nParamCount));
	// index 1: number of stats
	pInfo.insert(pInfo.begin() + 1, "3");
	// index 2: number of defenses
	pInfo.insert(pInfo.begin() + 2, std::to_string(nDefenseCount));
}

std::string ClientEngine::GetMyTitle() {
	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return "";

	return pMO->getTitle();
}

bool ClientEngine::GetMyPosition(float& posX, float& posY, float& posZ, float& rotDeg) {
	// Reset Return Values
	posX = posY = posZ = rotDeg = 0.0f;

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO || !pMO->getBaseFrame())
		return false;

	// Get Position & Rotation
	const Vector3f& retVector = pMO->getBaseFramePosition();
	posX = retVector.x;
	posY = retVector.y;
	posZ = retVector.z;
	rotDeg = pMO->getBaseFrame()->GetOrientationAngle();

	return true;
}

bool ClientEngine::SetMyPosition(float posX, float posY, float posZ) {
	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO || !pMO->getBaseFrame())
		return false;

	// Set Position
	pMO->setBaseFramePosition(Vector3f(posX, posY, posZ), true);
	pMO->saveLastPositionFromBaseFrame();

	return true;
}

bool ClientEngine::SetMyOrientation(float rotDegX, float rotDegY, float rotDegZ) {
	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO || !pMO->getBaseFrame())
		return false;

	// Set Rotation
	Vector3f dir, up;
	dir.x = rotDegX;
	dir.y = rotDegY;
	dir.z = rotDegZ;
	up.x = 0.0f;
	up.y = 1.0f;
	up.z = 0.0f;
	dir.Normalize();
	pMO->setBaseFrameOrientationCascade(dir, up);

	return true;
}

bool ClientEngine::SetMyLookAt(float posX, float posY, float posZ) {
	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO || !pMO->getBaseFrame())
		return false;

	// Set Rotation To Look At Position
	Vector3f lookAtVector;
	const Vector3f& retVector = pMO->getBaseFramePosition();
	lookAtVector.x = retVector.x - posX;
	lookAtVector.y = retVector.y - posY;
	lookAtVector.z = retVector.z - posZ;
	SetMyOrientation(lookAtVector.x, lookAtVector.y, lookAtVector.z);
	return true;
}

FLOAT ClientEngine::GetMyDistanceToMovementObject(int netTraceId) {
	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO || !pMO->getBaseFrame())
		return 0.0f;

	// Get My Distance To Given Avatar
	for (POSITION pos = m_movObjList->GetHeadPosition(); pos;) {
		auto pMO_find = (CMovementObj*)m_movObjList->GetNext(pos);
		if (!pMO_find || (pMO_find->getNetTraceId() != netTraceId))
			continue;

		const Vector3f& pos_find = pMO_find->getBaseFramePosition();
		const Vector3f& pos_me = pMO->getBaseFramePosition();
		return (pos_find - pos_me).Length();
	}

	return 0.0f;
}

bool ClientEngine::SetPlayerAltCharConfig(IGetSet* pGS_MO, int baseSlot, int meshId, int matlId) {
	auto pMO = dynamic_cast<CMovementObj*>(pGS_MO);
	if (!pMO)
		return false;
	return SetRuntimeSkeletonAltCharConfig(pMO->getSkeleton(), pMO->getSkeletonConfig(), baseSlot, meshId, matlId, pMO->getIdxRefModel());
}

bool ClientEngine::ScaleRuntimePlayer(IGetSet* pGS_MO, float xFactor, float yFactor, float zFactor) {
	auto pMO = dynamic_cast<CMovementObj*>(pGS_MO);
	if (!pMO || !pMO->getSkeleton())
		return false;
	return pMO->getSkeleton()->scaleSystem(xFactor, yFactor, zFactor);
}

Vector3f ClientEngine::GetRuntimePlayerScale(IGetSet* pGS_MO) {
	CMovementObj* pMO = dynamic_cast<CMovementObj*>(pGS_MO);
	if (!pMO)
		return Vector3f(1, 1, 1);

	RuntimeSkeleton* pSkel = pMO->getSkeleton();
	if (!pSkel)
		return Vector3f(1, 1, 1);

	return pSkel->getScaleVector();
}

int ClientEngine::GetPlayerCharConfigMeshId(IGetSet* pGS_MO, int configGlid, int configSlot) {
	auto pMO = dynamic_cast<CMovementObj*>(pGS_MO);
	if (pMO && pMO->getSkeleton()) {
		return pMO->getSkeletonConfig()->m_charConfig.getItemSlotMeshId(configGlid, configSlot);
	}
	return 0;
}

int ClientEngine::GetPlayerCharConfigMatlId(IGetSet* pGS_MO, int configGlid, int configSlot) {
	auto pMO = dynamic_cast<CMovementObj*>(pGS_MO);
	if (pMO && pMO->getSkeleton()) {
		return pMO->getSkeletonConfig()->m_charConfig.getItemSlotMaterialId(configGlid, configSlot);
	}
	return 0;
}

bool ClientEngine::GetPlayerAttachmentTypes(IGetSet* pGS_MO, std::vector<std::string>& attachments) {
	auto pMO = dynamic_cast<CMovementObj*>(pGS_MO);
	if (pMO == nullptr || pMO->getSkeleton() == nullptr)
		return false;

	std::vector<GLID> allEquipGLIDs;
	pMO->getSkeleton()->getAllEquippedGLIDs(allEquipGLIDs);

	for (auto glid : allEquipGLIDs) {
		attachments.push_back(std::to_string(glid));
	}
	return true;
}

bool ClientEngine::SetPlayerActorType(IGetSet* pGS_MO, int actorIndex) {
	auto pMO = dynamic_cast<CMovementObj*>(pGS_MO);
	if (!pMO)
		return false;

	if (actorIndex < 0)
		return false;

	POSITION refPos = m_movObjRefModelList->FindIndex(actorIndex);
	POSITION oldRun = m_movObjList->FindIndex(0); // your avatar always first
	if (!refPos)
		return false;

	auto pMORM = (CMovementObj*)m_movObjRefModelList->GetAt(refPos);
	auto pMO_old = (CMovementObj*)m_movObjList->GetAt(oldRun);
	if (!pMORM || pMO_old != pMO)
		return false;

	int* pMeshIdsOpt = NULL;
	int* pMatlIdsOpt = NULL;
	D3DCOLORVALUE* pColorOpt = NULL;
	CharConfig* pCC = &pMO->getSkeletonConfig()->m_charConfig;
	if (pCC) {
		pMeshIdsOpt = new int[pMORM->getSkeleton()->getSkinnedMeshCount()];
		pMatlIdsOpt = new int[pMORM->getSkeleton()->getSkinnedMeshCount()];
		pColorOpt = new D3DCOLORVALUE[pMORM->getSkeleton()->getSkinnedMeshCount()];

		const CharConfigItem* pConfigItem = pCC->getItemByGlidOrBase(GOB_BASE_ITEM);

		for (size_t s = 0; s < pMORM->getSkeleton()->getSkinnedMeshCount(); s++) {
			const CharConfigItemSlot* pSlot = pConfigItem != nullptr ? pConfigItem->getSlot(s) : nullptr;
			if (pSlot != nullptr) {
				pMeshIdsOpt[s] = pSlot->m_meshId;
				pMatlIdsOpt[s] = pSlot->m_matlId;
				pColorOpt[s].r = pSlot->m_r;
				pColorOpt[s].g = pSlot->m_g;
				pColorOpt[s].b = pSlot->m_b;
			} else {
				pMeshIdsOpt[s] = 0;
				pMatlIdsOpt[s] = 0;
				pColorOpt[s].r = -1.0f;
				pColorOpt[s].g = -1.0f;
				pColorOpt[s].b = -1.0f;
			}
		}
	}

	// Clone Movement Object
	CMovementObj* pMO_new = NULL;
	EntityClone(pMORM, &pMO_new, actorIndex, pMeshIdsOpt, pMatlIdsOpt, pColorOpt);

	// Configure Equippable Items
	if (pCC) {
		delete pMeshIdsOpt;
		delete pMatlIdsOpt;
		delete pColorOpt;
		for (size_t i = CC_ITEM_FIRST_EQUIP; i < pCC->getItemCount(); i++)
			ConfigPlayerEquippableItem(pMO_new, pCC->getItemByIndex(i));
	}

	// Copy Movement Object Members
	pMO_new->setBaseFrame(new CFrameObj(NULL));
	pMO_new->setBaseFrameMatrix(pMO->getBaseFrameMatrix(), false);
	pMO_new->setNetTraceId(pMO->getNetTraceId());

	pMO_new->setValidThisRound(pMO->isValidThisRound());

	pMO_new->m_movementInterpolator = pMO->m_movementInterpolator;

	pMO_new->setLastPosition(pMO->getLastPosition());

	pMO_new->setIdxRefModel(actorIndex);

	auto pMC = pMO->getMovementCaps();
	auto pMC_new = pMO_new->getMovementCaps();
	if (pMC && pMC_new)
		pMC->Copy(pMC_new);

	pMO_new->setTraceNumber(pMO->getTraceNumber());
	pMO_new->setCurrentCameraDataIndex(pMO->getCurrentCameraDataIndex());

	pMO_new->setName(pMO->getName());
	pMO_new->setClanName(pMO->getClanName());
	pMO_new->setTitle(pMO->getTitle());

	pMO_new->setControlType(pMO->getControlType());
	pMO_new->setNetTraceId(pMO->getNetTraceId());
	pMO_new->setNetworkEntity(pMO->isNetworkEntity());
	pMO_new->getSkeleton()->scaleSystem(pMO->getSkeleton()->getScaleVector().x, pMO->getSkeleton()->getScaleVector().y, pMO->getSkeleton()->getScaleVector().z);
	pMO_new->getSkeletonConfig()->m_charConfig = pMO->getSkeletonConfig()->m_charConfig;
	pMO_new->getSkeletonConfig()->m_newConfig = pMO->getSkeletonConfig()->m_newConfig;

	// Get Armed Glids
	std::vector<GLID> armedGlids;
	auto pCAI = pMO->getCurrentlyArmedItems();
	if (pCAI) {
		OBJ_LIST_GUARD(pCAI);
		for (POSITION pos = pCAI->GetHeadPosition(); pos;) {
			auto pIO = static_cast<const CItemObj*>(pCAI->GetNext(pos));
			if (!pIO)
				continue;

			GLID glid = pIO->m_globalID;
			armedGlids.push_back(glid);
		}
	}

	CCameraObj* pCO_old = GetActiveCameraForMovementObject(pMO);
	Vector3f camX = pCO_old->m_camWorldVectorX;
	Vector3f camY = pCO_old->m_camWorldVectorY;
	Vector3f camZ = pCO_old->m_camWorldVectorZ;
	Vector3f camPos = pCO_old->m_camWorldPosition;
	Vector3f camFrmPos;

	float oAzi, oEle, oMinD, oMaxD, oD, oStD, oDStep, oAziInit, oEleInit;
	Vector3f oFocus;
	bool oAutoFollow;
	float oAutoAzimuth;
	float oAutoElevation;

	oAzi = pCO_old->m_orbitAzimuth;
	oEle = pCO_old->m_orbitElevation;
	oMinD = pCO_old->m_orbitMinDist;
	oMaxD = pCO_old->m_orbitMaxDist;
	oD = pCO_old->m_orbitDist;
	oStD = pCO_old->m_orbitStartDist;
	oDStep = pCO_old->m_orbitDistStep;
	oAziInit = pCO_old->m_orbitAziInit;
	oEleInit = pCO_old->m_orbitEleInit;
	oFocus = pCO_old->m_orbitFocus;
	oAutoFollow = pCO_old->m_orbitAutoFollow;
	oAutoAzimuth = pCO_old->m_orbitAutoAzimuth;
	oAutoElevation = pCO_old->m_orbitAutoElevation;

	camFrmPos.Set(0, 0, 0);
	if (pCO_old->m_floatingCamParentFrame)
		pCO_old->m_floatingCamParentFrame->GetPosition(camFrmPos);

	// Delete Old Movement Object Cameras
	if (m_pRuntimeCameras) {
		for (size_t cameraIndex = 0; cameraIndex < pMO->getCameraCount(); cameraIndex++) {
			auto cameraData = pMO->getCameraData(cameraIndex);
			if (cameraData.m_runtimeCameraId > 0) {
				auto pCO = m_pRuntimeCameras->GetCamera(cameraData.m_runtimeCameraId);
				m_runtimeViewportDB->DelCameraCallback(pCO);
				m_pRuntimeCameras->Del(pCO);
				pMO->setRuntimeCameraId(cameraIndex, 0);
			}
		}
	}

	// Add/Attach New Movement Object Cameras
	CCameraObj* pCO_new = NULL;
	for (size_t cameraIndex = 0; cameraIndex < pMO_new->getCameraCount(); cameraIndex++) {
		auto cameraData = pMO_new->getCameraData(cameraIndex);
		if (cameraData.m_runtimeCameraId <= 0) {
			auto newCameraId = m_pRuntimeCameras->Add(cameraData.m_cameraDBIndex);
			pMO_new->setRuntimeCameraId(cameraIndex, newCameraId);
			pCO_new = m_pRuntimeCameras->GetCamera(newCameraId);
			AttachCameraToObject(pCO_new, ObjectType::MOVEMENT, pMO_new->getNetTraceId());
		}
	}

	SetActiveCameraForMovementObjectByIndexOffset(pMO_new, 0, true);

	if (pCO_new && pMC_new && (pMC_new->GetFovChangeSensitivity() != 0.0f)) {
		pMC_new->SetFov(pCO_new->GetFovY());
		pMC_new->SetFovLast(pCO_new->GetFovY());
	}

	pCO_old = GetActiveCameraForMovementObject(pMO_new);
	pCO_old->m_camWorldVectorX = camX;
	pCO_old->m_camWorldVectorY = camY;
	pCO_old->m_camWorldVectorZ = camZ;
	pCO_old->m_camWorldPosition = camPos;
	pCO_old->m_orbitAzimuth = oAzi;
	pCO_old->m_orbitElevation = oEle;
	pCO_old->m_orbitMinDist = oMinD;
	pCO_old->m_orbitMaxDist = oMaxD;
	pCO_old->m_orbitDist = oD;
	pCO_old->m_orbitStartDist = oStD;
	pCO_old->m_orbitDistStep = oDStep;
	pCO_old->m_orbitAziInit = oAziInit;
	pCO_old->m_orbitEleInit = oEleInit;
	pCO_old->m_orbitFocus = oFocus;
	pCO_old->m_orbitAutoFollow = oAutoFollow;
	pCO_old->m_orbitAutoAzimuth = oAutoAzimuth;
	pCO_old->m_orbitAutoElevation = oAutoElevation;

	if (pCO_old->m_floatingCamParentFrame)
		pCO_old->m_floatingCamParentFrame->SetPosition(camFrmPos);

	m_movObjList->RemoveAt(oldRun);
	pMO->SafeDelete();
	delete pMO;
	m_movObjList->AddHead(pMO_new);

	// ED-8331 - Arm All Items Async (realize now, arm defaults, don't sendMyEvent)
	ArmItemsAsync(armedGlids, pMO_new, true, true, false);

	return true;
}

bool ClientEngine::SetRuntimeSkeletonAltCharConfig(
	RuntimeSkeleton* pRS,
	SkeletonConfiguration* pSC,
	int baseSlot,
	int meshId,
	int matlId,
	int refModel) {
	if (!pRS || (pRS->getSkinnedMeshCount() <= baseSlot) || !m_movObjRefModelList)
		return false;

	POSITION pos = m_movObjRefModelList->FindIndex(refModel);
	if (!pos)
		return false;

	auto pMO = static_cast<CMovementObj*>(m_movObjRefModelList->GetAt(pos));
	if (!pMO)
		return false;

	return pMO->configMeshSlot(pRS, pSC, baseSlot, meshId, matlId);
}

bool ClientEngine::CreateCharacterOnServerFromPlayer(
	IGetSet* igs_CMovementObj,
	int spawnIndex,
	const std::string& name,
	int nTeam,
	int scaleX,
	int scaleY,
	int scaleZ) {
	auto pMO = (CMovementObj*)igs_CMovementObj;
	if (!pMO)
		return false;
	return CreateCharacterOnServer(pMO, spawnIndex, name, nTeam, scaleX * 1.0f / 100, scaleY * 1.0f / 100, scaleZ * 1.0f / 100);
}

int ClientEngine::GetCharacterMaxCount() const {
	if (!m_multiplayerObj)
		return -1;
	return m_multiplayerObj->m_clientCharacterMaxCount;
}

bool ClientEngine::GetMyPlayerCharConfigBaseMatlIds(std::vector<std::string>& matlIds) {
	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	auto pRS = pMO->getSkeleton();
	if (!pRS)
		return false;

	auto pSC = pMO->getSkeletonConfig();
	matlIds.push_back(std::to_string(pMO->getIdxRefModel()));
	for (int s = 0; s < pRS->getSkinnedMeshCount(); s++)
		matlIds.push_back(std::to_string(pSC->m_charConfig.getItemSlotMaterialId(GOB_BASE_ITEM, s)));
	return true;
}

bool ClientEngine::GetMyPlayerCharConfigBaseMeshIds(std::vector<std::string>& meshIds) {
	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	auto pRS = pMO->getSkeleton();
	if (!pRS)
		return false;

	auto pSC = pMO->getSkeletonConfig();
	meshIds.push_back(std::to_string(pMO->getIdxRefModel()));
	for (int s = 0; s < pRS->getSkinnedMeshCount(); s++)
		meshIds.push_back(std::to_string(pSC->m_charConfig.getItemSlotMeshId(GOB_BASE_ITEM, s)));
	return true;
}

void ClientEngine::GenerateEntityLastPosition() {
	for (POSITION posLoc = m_movObjList->GetHeadPosition(); posLoc != NULL;) {
		auto pMO = (CMovementObj*)m_movObjList->GetNext(posLoc);
		if (!pMO)
			continue;
		pMO->saveLastPositionFromBaseFrame(false);
	}
}

void ClientEngine::CollisionLoopEntityToEntity() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "CollisionLoopEntityToEntity");

	// Reset Interact String (city vendor 'Press TAB To Interact')
	m_interactStr = "";

	// Get My Avatar
	auto pMO_me = GetMovementObjMe();
	if (!pMO_me || !pMO_me->getBaseFrame() || !pMO_me->getSkeleton() || !pMO_me->getSkeleton()->getSphereBodyCollisionSystem() || !pMO_me->getSkeleton()->getCalculationSphere())
		return;

	// Get My Position
	Vector3f position_me = pMO_me->getBaseFramePosition();
	Matrix44f transform_me = pMO_me->getBaseFrameMatrix();

	// Check Me Against All Other Movement Objects For Hit
	for (POSITION cmpPosition = m_movObjList->GetHeadPosition(); cmpPosition;) {
		auto pMO_hit = (CMovementObj*)m_movObjList->GetNext(cmpPosition);
		if ((pMO_me == pMO_hit) || !pMO_hit->getBaseFrame() || !pMO_hit->getSkeleton() || !pMO_hit->getSkeleton()->getSphereBodyCollisionSystem() || !pMO_hit->getSkeleton()->getCalculationSphere())
			continue;

		// Get Bone Positions
		Matrix44f transform_hit = pMO_hit->getBaseFrameMatrix();
		Vector3f bonePosition_hit = TransformPoint_NonPerspective(
			transform_hit,
			pMO_hit->getSkeleton()->getBonePositionByIndex(pMO_hit->getSkeleton()->getCalculationSphere()->getBoneIndex()));
		Vector3f bonePosition_me = TransformPoint_NonPerspective(
			transform_me,
			pMO_hit->getSkeleton()->getBonePositionByIndex(pMO_me->getSkeleton()->getCalculationSphere()->getBoneIndex()));

		// In Range Of Other Movement Object ?
		bool inRange = (MatrixARB::BoundingSphereCheck(
							pMO_me->getSkeleton()->getCalculationSphere()->getRadius(),
							pMO_hit->getSkeleton()->getCalculationSphere()->getRadius(),
							bonePosition_me.x,
							bonePosition_me.y,
							bonePosition_me.z,
							bonePosition_hit.x,
							bonePosition_hit.y,
							bonePosition_hit.z) == TRUE);

		// Fire Trigger Exit Event If Touched & Now Out Of Range
		if (!inRange && pMO_hit->getTouchTriggered()) {
			pMO_hit->setTouchTriggered(false);
			GenericEvent* e = dynamic_cast<GenericEvent*>(m_dispatcher.MakeEvent(m_entityCollisionEventId));
			e->AddTo(SERVER_NETID);
			e->SetFilter((int)eTriggerEvent::Exit);
			(*e->OutBuffer()) << pMO_me->getNetTraceId() << pMO_hit->getNetTraceId(); // false = exiting
			m_dispatcher.QueueEvent(e);
		}

		if (!inRange)
			continue;

		// Trigger Interact String On Static Movement Objects (city vendors)
		if (pMO_hit->isTypeStatic() && (pMO_hit->getServerItemType() != SI_TYPE_PAPER_DOLL))
			m_interactStr = "Press TAB To Interact";

		// Bone For Bone Determine If Movement Objects Actually Touch
		for (POSITION pos_me = pMO_me->getSkeleton()->getSphereBodyCollisionSystem()->GetHeadPosition(); pos_me;) {
			auto pSCO_me = static_cast<const CSphereCollisionObj*>(pMO_me->getSkeleton()->getSphereBodyCollisionSystem()->GetNext(pos_me));
			if (!pSCO_me)
				continue;

			for (POSITION pos_hit = pMO_hit->getSkeleton()->getSphereBodyCollisionSystem()->GetHeadPosition(); pos_hit;) {
				auto pSCO_hit = static_cast<const CSphereCollisionObj*>(pMO_hit->getSkeleton()->getSphereBodyCollisionSystem()->GetNext(pos_hit));
				if (!pSCO_hit)
					continue;

				Vector3f adjust;
				Vector3f bonePositionCmp = TransformPoint_NonPerspective(transform_hit, pMO_hit->getSkeleton()->getBonePositionByIndex(pSCO_hit->getBoneIndex()));

				// Is Actually Touching Other Movement Object ?
				bool isTouch = (MatrixARB::CollisionSphereSeg(
									position_me,
									pMO_me->getLastPosition(),
									pSCO_hit->getRadius(),
									bonePositionCmp,
									TRUE,
									&adjust,
									pSCO_me->getRadius(),
									NULL) == TRUE);

				// Fire Trigger Enter Event On First Touch
				if (isTouch && !pMO_hit->getTouchTriggered()) {
					pMO_hit->setTouchTriggered(true);
					IEvent* e = m_dispatcher.MakeEvent(m_entityCollisionEventId);
					e->AddTo(SERVER_NETID);
					e->SetFilter((int)eTriggerEvent::Enter);
					(*e->OutBuffer()) << pMO_me->getNetTraceId() << pMO_hit->getNetTraceId();
					m_dispatcher.QueueEvent(e);
				}
			}
		}
	}
}

void ClientEngine::UpdateEntityPersist(CPersistObjList** pList) {
	CPersistObjList* persistList = *pList;
	for (POSITION posLoc = persistList->GetHeadPosition(); posLoc != NULL;) {
		CPersistObj* pstLoc = (CPersistObj*)persistList->GetNext(posLoc);
		POSITION entPositionLoc = m_movObjRefModelList->FindIndex(pstLoc->m_entDBIndex);
		if (entPositionLoc) {
			// Create New Movement Object
			bool ok = CreateMovementObject(
				0,
				pstLoc->m_entMatrix,
				pstLoc->m_entDBIndex,
				pstLoc->m_entControlType,
				0,
				false, // not networkEntity
				false, // add at tail (index!=0)
				false, // don't spawn at server
				-1,
				NULL,
				NULL,
				false,
				"");
			if (!ok) {
				LogError("CreateMovementObject(ENTITY_PERSIST) FAILED");
			}
		}

		ProgressUpdate("UpdateEntityPersistLoop");
	}
}

// Called only by AnimImportProg.lua for importing new animation
bool ClientEngine::BakeAnimation_MovementObj(IGetSet* pGS_player, const GLID& animGlid) {
	auto pMO = dynamic_cast<CMovementObj*>(pGS_player);
	if (!pMO)
		return false;

	auto pRS = pMO->getSkeleton();
	if (!pRS)
		return false;

	auto pSAH = pRS->getSkeletonAnimHeaderByGlid(animGlid);
	if (!pSAH) {
		// search animation on STUNT entity (if exists)
		if (pMO->getPosses() && pMO->getMountType() == MT_STUNT)
			pSAH = pMO->getPosses()->getSkeleton()->getSkeletonAnimHeaderByGlid(animGlid);
	}

	if (!pSAH)
		return false;

	auto pAP = AnimationRM::Instance()->GetAnimationProxy(animGlid);
	if (!pAP || !pAP->GetAnimDataPtr())
		return false;

	pAP->BakeEditingAttributes(pSAH->GetAnimSpeedFactor(), pSAH->GetAnimCropStartTimeMs(), pSAH->GetAnimCropEndTimeMs(), pSAH->GetAnimLooping());
	pSAH->SetAnimSpeedFactor(1.0f);
	pSAH->SetAnimCropTimes(0.0, -1.0);
	pSAH->m_animDurationMs = pAP->GetAnimDurationMs();

	if (pMO->getMountType() == MT_STUNT && pMO->getPosses()) {
		auto pSAH_posses = pMO->getPosses()->getSkeleton()->getSkeletonAnimHeaderByGlid(animGlid);
		if (pSAH_posses) {
			pSAH_posses->SetAnimSpeedFactor(1.0f);
			pSAH_posses->SetAnimCropTimes(0.0, -1.0);
			pSAH_posses->m_animDurationMs = pAP->GetAnimDurationMs();
		}
	}

	return true;
}

bool ClientEngine::LoadAnimationOnPlayer(IGetSet* pGS_player, const GLID& animGlid) {
	auto pMO = dynamic_cast<CMovementObj*>(pGS_player);
	if (!pMO)
		return false;

	auto pRS = pMO->getSkeleton();
	if (!pRS)
		return false;

	auto pSAH = pRS->getSkeletonAnimHeaderByGlid(animGlid);
	if (!pSAH) {
		// Queue Animation On Skeleton
		pRS->getOrQueueAnimationIndexByGlid(animGlid);
		return false;
	}

	if (!pSAH->IsDataReady()) {
		// Data not ready yet, trigger download if not already
		if (!pSAH->LoadData()) {
			// Still waiting for data
			return false;
		}
	}

	// Everything is ready
	return true;
}

int ClientEngine::GetBoneIndexByName(IGetSet* pGS_player, const std::string& boneName) {
	auto pMO = dynamic_cast<CMovementObj*>(pGS_player);
	if (!pMO)
		return -1;

	if (pMO->getPosses() && pMO->getMountType() == MT_STUNT)
		pMO = pMO->getPosses();

	auto pRS = pMO->getSkeleton();
	if (!pRS)
		return -1;

	return pRS->getBoneIndexByName(boneName);
}

} // namespace KEP