///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "ClientBladeFactory.h"
#include "SerializeHelper.h"
#include "CMultiplayerClass.h"
#include "CDayStateClass.h"
#include "CDeformableMesh.h"
#include "CHierarchyMesh.h"
#include "SkeletonConfiguration.h"
#include "CBoneClass.h"
#include "CSphereCollisionClass.h"
#include "CIconSystemClass.h"
#include "CServerItemClass.h"
#include "CArmedInventoryClass.h"
#include "CTimeSystem.h"
#include "ReMeshCreate.h"
#include "DynamicObjectManager.h"
#include "LoadingScreenRAIIHelper.h"
#include "ContentServiceGLIDCacheTask.h"
#include "EquippableSystem.h"
#include "UnicodeMFCHelpers.h"
#include "TextureProvider.h"
#include "Core/Math/Interpolate.h"
#include "Core/Math/Rotation.h"
#include <KEPPhysics/CollisionShape.h>
#include <KEPPhysics/PhysicsFactory.h>
#include <KEPPhysics/RigidBodyStatic.h>
#include <KEPPhysics/Universe.h>
#include "Common/KEPUtil/Helpers.h"
#include "Common/KEPUtil/FrameTimeProfiler.h"
#include "CFileFactory.h"
#include "SerializeFromFile.h"

namespace KEP {

static LogInstance("ClientEngine");

#ifndef MAPVK_VSC_TO_VK
#define MAPVK_VSC_TO_VK 1
#endif

// Only For Loading Splash Screen Progress Bar During Startup
bool ClientEngine::LoadScenePerformSplashScreen() {
	g_pD3dDevice->Clear(0, NULL, D3DCLEAR_ZBUFFER | D3DCLEAR_TARGET, m_backgroundColorBlack, 1.0f, 0);
	std::string texFilePath = PathAdd(IResource::BaseTexturePath(eTexturePath::MapsModels), "LoginMenu\\member_splash.tga");
	return RenderLoadingSplashScreen(texFilePath, true);
}

class AutoReset {
public:
	AutoReset(const std::function<void()>& setFunc, const std::function<void()>& resetFunc) :
			m_resetFunc(resetFunc) {
		setFunc();
	}
	~AutoReset() {
		m_resetFunc();
	}

private:
	std::function<void()> m_resetFunc;
};

// Asynchronous scene loading thread -- note this is ONLY used to allow some UI rendering while zone is being loaded.
// Doing anything other than rendering loading progress menu is dangerous, especially those query/manipulate global lists.
class LoadSceneThread : public jsThread {
public:
	LoadSceneThread(const std::string& exgFileName, const char* url, ClientEngine* pEngine) :
			jsThread("ClientEngineLoadSceneThread", PR_HIGHEST),
			m_exgFileName(exgFileName),
			m_url(url),
			m_pEngine(pEngine) {}

protected:
	virtual ULONG _doWork() {
		jsVerifyReturn(m_pEngine != NULL, -1);
		m_pEngine->enableResourceManagers(false);
		m_pEngine->cancelPendingResources();
		return m_pEngine->LoadSceneMain(m_exgFileName.c_str(), m_url) ? 0 : 1; // 0 = success, 1 = failure
	}

	std::string m_exgFileName;
	const char* m_url;
	ClientEngine* m_pEngine;
};

bool ClientEngine::LoadScene(const std::string& exgFileName, const char* url) {
	LoadSceneThread loadSceneThread(exgFileName, url, this);

	// Construct Loading Screen
	bool syncEvents = (GetCurrentThreadId() == m_mainThreadId); // DRF - synchronize events on main thread only
	LoadingScreenRAIIHelper loadingScreenInScope(m_dispatcher, m_zoneLoadingStartEventId, m_zoneLoadingCompleteEventId, m_zoneLoadingFailedEventId, syncEvents);
	loadingScreenInScope.Start();

	// Perform some trivial cleanup task in main thread before launching background load scene thread
	// This should reduce chance of possible races if server sends down zone manipulation events during the async loading process.
	LoadScenePre();

	// Arm m_sceneLoading flag in main thread to prevent race. Use braces to guarantee the flag to disarm when worker thread exits.
	{
		AutoReset sceneLoadingFlagSetter([this]() { SetLoadingScene(true); }, [this]() { SetLoadingScene(false); });

		loadSceneThread.start();

		bool bExiting = false;
		while (loadSceneThread.isRunning() || loadSceneThread.isSleeping()) {
			if (!RenderLoop_MenusOnly(!bExiting))
				bExiting = true;
			fTime::SleepMs(100.0); // Do fewer render loops to allow fast scene loading
		}
	}

	// 0 = success, 1 = failure
	bool ok = (loadSceneThread.exitCode() == 0);
	if (ok)
		ResizeClientRect(m_windowStateGL, m_szXMonitor, m_szYMonitor, true);

	// turn off widgets on zone
	ObjectDisplayTranslationWidget(ObjectType::NONE);

	return ok;
}

void ClientEngine::LoadScenePre() {
	ProgressUpdate("Begin LoadScene", "Preparing", 10, TRUE, FALSE);

	SetZoneAllowsLabels(true);
	m_bAllowDragDrop = false;

	// Remove All Triggers
	RemoveAllTriggers();
	ProgressUpdate("RemoveRuntimeTriggers", "Preparing", 10);

	// Remove all path finding helpers
	ClearAllPathFinders(false, IPathFinder::NONE);
	ProgressUpdate("ClearAllPathFinders", "Preparing", 10);

	ResetLocalPlacementId();

	ObjectClearSelection(); // This is the new (multi-select / build mode) way

	// destroy list that will be filled
	DestroyAllGeometries();
	ProgressUpdate("DeleteRuntimeLightweightGeometries", "Preparing", 10);

	// destroy list that will be filled
	DestroyDynamicObjects();
	ProgressUpdate("DestroyDynamicObjects", "Preparing", 30);

	// set world particle sys to non regen to flag it
	if (m_curWldObjPrtList) {
		for (POSITION pos = m_curWldObjPrtList->GetHeadPosition(); pos;) {
			auto pWO = (CWldObject*)m_curWldObjPrtList->GetNext(pos);
			if (pWO && pWO->m_runtimeParticleId)
				m_particleRM->SetRuntimeEffectStatus(pWO->m_runtimeParticleId, -1, 0, 0);
		}
	}

	// Remove runtime particle effects so residual particles will not be rendered in new zone.
	// This is especially important when we have user-designed effects in the zone, which
	// might happen to emit long-lived particles.
	//
	// This function must be called before cleanup of any particle-bearing objects like
	// dynobj, zoneobj or entities.
	m_particleRM->ClearRuntimeEffects();
	m_particleRM->FreeTextures();
	ProgressUpdate("ClearRuntimeEffects", "Preparing", 20);

	ClearRenderLists(RLClear_ALL);
	ProgressUpdate("ClearRenderLists", "Preparing", 10);

	m_curWldObjPrtList->RemoveAll();
	ProgressUpdate("RemoveWorldObjectPtrList", "Preparing", 10);
}

bool ClientEngine::LoadSceneMain(const std::string& exgFileName, const char* url) {
	LogInfo("START (" << exgFileName << " " << NULL_CK(url) << ")");

	ScopedDirectoryChanger sdc("GameFiles\\");
	std::wstring exgFileNameW = Utf8ToUtf16(exgFileName);
	CFileException exc;
	CFile* fl = CFileFactory::GetFile();
	BOOL res = fl->Open(exgFileNameW.c_str(), CFile::modeRead | CFile::shareDenyWrite, &exc);
	if (res == FALSE) {
		std::string msg;
		StrBuild(msg, "Zone File Missing '" << exgFileName << "'");
		LogError(msg);
		m_pTextReadoutObj->AddLine(msg, eChatType::System);
		m_bAllowDragDrop = true;
		LogError("END - Open() FAILED");
		return false;
	}

	m_bLightListDirty = true;

	DeleteSpawnDatabase();
	ProgressUpdate("DeleteSpawnDatabase", "Preparing", 10);

	DestroyWorldObjects();
	ProgressUpdate("DeleteWorldObjects", "Preparing", 10);

	GetReDeviceState()->InvalidateResources(RED3DX9RESOURCE_STATIC);
	ProgressUpdate("InvalidateResources", "Preparing", 10);

	DestroyEnvironment();
	if (m_scenePersistList) {
		for (POSITION posLocal = m_scenePersistList->GetHeadPosition(); posLocal != NULL;) {
			CPersistObj* perstPtr = (CPersistObj*)m_scenePersistList->GetNext(posLocal);
			delete perstPtr;
			perstPtr = nullptr;
		}
		delete m_scenePersistList;
		m_scenePersistList = nullptr;
	}
	ProgressUpdate("DestroyEnvironment", "Preparing", 10);

	DestroyMovementObjectList();
	ProgressUpdate("DestroyEntityRuntimeList", "Preparing", 20);

	DeleteWorldGroupList();
	ProgressUpdate("DeleteWorldGroupList", "Preparing", 10);

	DeleteScriptList(&m_sceneScriptExec);
	ProgressUpdate("DeleteScriptList", "Preparing", 10);

	DeleteSpawnDatabase();
	ProgressUpdate("DeleteSpawnDatabase", "Preparing", 10);

	// DestroyAllTexturesFromDatabase();
	if (m_pCollisionObj) {
		m_pCollisionObj->SafeDelete();
		delete m_pCollisionObj;
		m_pCollisionObj = 0;
	}
	ProgressUpdate("DeleteCollision", "Preparing", 10);

	//reopened: disposal of previous RLB before opening a new zone [Yanfeng 11/2008]
	if (m_libraryReferenceDB) {
		m_libraryReferenceDB->SafeDelete();
		delete m_libraryReferenceDB;
		m_libraryReferenceDB = 0;
	}
	ProgressUpdate("DeleteRLB", "Preparing", 10);

	enableResourceManagers(true);
	ProgressUpdate("ReenableResourceManagers", "Preparing", 20);

	// delete the level meshes in the global pool... they're streamed in again on each level load
	MeshRM::Instance()->ResetMeshPool(MeshRM::RMP_ID_LEVEL);
	ProgressUpdate("ResetMeshPool", "Preparing", 10);

	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix

	CStringA reserved;
	CArchive the_in_Archive(fl, CArchive::load);
	try {
		ProgressUpdate("OpenFile", "Loading New Zone", 0);

		the_in_Archive >> m_pWOL;
		ProgressUpdate("ReadWorldObjects", "Loading New Zone", 100);

		int deprecated;
		CStringA deprecatedStr;
		BOOL bufferPreffered_Deprecated;
		CAIWebObjList* pAIWOL_Deprecated;

		the_in_Archive >> m_backgroundList >> m_environment >> m_scenePersistList >> deprecated //m_currentTrack
			>> deprecatedStr //m_musicLoopFile
			>> m_globalWorldShader >> bufferPreffered_Deprecated >> m_sceneScriptExec >> reserved >> m_spawnGeneratorDB;
		ProgressUpdate("ReadMisc", "Loading New Zone", 20);

		the_in_Archive >> m_pCollisionObj >> worldGroupList >> pAIWOL_Deprecated;

		if (pAIWOL_Deprecated != nullptr) {
			pAIWOL_Deprecated->SafeDelete();
			delete pAIWOL_Deprecated;
			pAIWOL_Deprecated = nullptr;
		}

		ProgressUpdate("ReadMisc2", "Loading New Zone", 100);
	} catch (CArchiveException* archiveEx) {
		the_in_Archive.Close();
		fl->Close();
		delete fl;
		archiveEx->Delete();
		m_bAllowDragDrop = true;
		LogError("END - Caught Exception (CArchiveException)");
		return false;
	} catch (CFileException* fileEx) {
		the_in_Archive.Close();
		fl->Close();
		delete fl;
		fileEx->Delete();
		m_bAllowDragDrop = true;
		LogError("END - Caught Exception (CFileException)");
		return false;
	} catch (CMemoryException* memoryEx) {
		the_in_Archive.Close();
		fl->Close();
		delete fl;
		memoryEx->Delete();
		m_bAllowDragDrop = true;
		LogError("END - Caught Exception (CMemoryException)");
		return false;
	}

	the_in_Archive.Close();
	fl->Close();
	delete fl;

	ProgressUpdate("ProgressingStart", "Processing", 0);

	if (m_libraryReferenceDB)
		m_libraryReferenceDB->ReinitAll(g_pD3dDevice);
	ProgressUpdate("RLBReinitAll", "Processing", 10);

	if (m_shaderSupport)
		m_pRenderStateMgr->SetVertexShader(m_shaderSystem, m_globalWorldShader);
	ProgressUpdate("SetVertexShader");

	ProgressUpdate("GetTotalAIOutput");

	ProgressUpdate("ExecAllAttributes");
	ExecAllAttributes(m_sceneScriptExec);

	ProgressUpdate("GetTotalAIOutput", "Processing", 10);

	ReinitAllTexturesFromDatabase();
	ProgressUpdate("ReinitAllTexturesFromDatabase", "Processing", 200);

	InitializeOjectsIntoScene();
	ProgressUpdate("InitializeOjectsIntoScene", "Processing", 30);

	RebuildTraceNumber();
	ProgressUpdate("RebuildTraceNumber", "Processing", 10);

	if (!ClientIsEnabled() && m_scenePersistList)
		UpdateEntityPersist(&m_scenePersistList);
	ProgressUpdate("UpdateEntityPersist", "Processing", 10);

	ResetRenderState();
	ProgressUpdate("SetAllRenderStatesIM", "Processing", 10);

	m_globalTraceNumber++; // needs this

	m_pCollisionObj->BuildObjectReferenceArray(m_pWOL);
	ProgressUpdate("BuildObjectReferenceArray", "Processing", 10);

	InitializeEnvironment();
	ProgressUpdate("InitializeEnvironment", "Processing", 10);

	m_oldPhysics->Initialize(m_environment);
	ProgressUpdate("InitializePhysics", "Processing", 10);

	ZoneLoaded(exgFileName.c_str(), url, false);
	ProgressUpdate("ZoneLoaded", "Processing", 0);

	//Initialize a empty reference library if not loaded due to various reasons
	//this will fix the NULL m_libraryReferenceDB problem if RLB file is somehow missing or zone is configured improperly
	if (!m_libraryReferenceDB)
		m_libraryReferenceDB = new CReferenceObjList;

	m_bAllowDragDrop = true;

	ProgressUpdate("Complete", "Complete", 0, FALSE, TRUE);

	LogInfo("END - OK");

	return true;
}

bool ClientEngine::LoadEntityDatabase(const std::string& fileName) {
	if (!Engine::LoadEntityDatabase(fileName)) {
		LogError("LoadEntityDatabase() FAILED");
		return false;
	}

	if (m_scenePersistList) {
		ProgressUpdate("UpdateEntityPersist");
		UpdateEntityPersist(&m_scenePersistList); // fill in runtime entities
	}

	return true;
}

bool ClientEngine::InitializeOjectsIntoScene() {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	if (!m_pWOL)
		return false;

	for (POSITION pos = m_pWOL->GetHeadPosition(); pos;) {
		auto pWO = (CWldObject*)m_pWOL->GetNext(pos);
		if (!pWO)
			continue;
		pWO->InitReMeshes();
		InitializeWorldObject(pWO);
	}

	ClientBaseEngine::InitializeWorldObjectCollision();
	ClientBaseEngine::InitWorldObjectTriggerList();

	return true;
}

void ClientEngine::LoadTextFontInterfaceFromFile(const std::string& fileName) {
	ScopedDirectoryChanger sdc("GameFiles\\");
	CTextCollectionList* pTempTextInterfaceList;
	CFontObjList* pTempFontList;
	CGFontObjList* pTempCgFontList;
	CSymbolMapperObjList* pTempSymbolMapDB;
	BEGIN_CFILE_SERIALIZE(fl, fileName, false)
	CArchive the_in_Archive(fl, CArchive::load);
	the_in_Archive >> pTempTextInterfaceList >>
		pTempFontList >> pTempCgFontList >> pTempSymbolMapDB;
	the_in_Archive.Close();
	if (m_symbolMapDB) {
		m_symbolMapDB->SafeDelete();
		delete m_symbolMapDB;
		m_symbolMapDB = 0;
	}
	m_symbolMapDB = pTempSymbolMapDB;
	END_CFILE_SERIALIZE(fl, m_logger, "LoadTextFontInterfaceFromFile")
}

bool ClientEngine::ProcessScriptAttribute(CEXScriptObj* pSO) {
	if (!pSO)
		return false;

	switch (pSO->m_actionAttribute) {
		case LOAD_SOUNDTRACK:
		case LOAD_TEXTURE_BANK:
		case LOAD_STATIC_TEXTURE_BANK:
		case INIT_MOVIE_RUNTIME:
		case PLAY_MOVIE_FULLSCREEN:
		case LOAD_MOVIE_DATABASE:
		case LOAD_SAFE_ZONES:
		case LOAD_HUD_DATABASE:
		case LOAD_COMM_CONTROLS:
		case LOAD_ANIM_TEXTURES_DATABASE:
		case CONNECT_INTERUPTION_TIMEOUT:
		case LOAD_BANK_ZONES:
		case LOAD_COMMERCE_DATABASE:
			// DEPRECATED
			break;

		case OPEN_SCENE: {
			enableResourceManagers(false);
			cancelPendingResources();
			LoadScenePre();
			std::string exgFileName = pSO->m_miscString.GetString();
			if (LoadSceneMain(exgFileName)) {
				SetZoneFileName(exgFileName);
				ResizeClientRect(m_windowStateGL, m_szXMonitor, m_szYMonitor, true);
			} else
				return false;
		} break;

		case LOAD_TEXT_FONT_COLLECTIONS_DATABASE:
			LoadTextFontInterfaceFromFile(pSO->m_miscString.GetString());
			break;

		case LOAD_CONTROLS_DATABASE:
			LoadControlCfgList(pSO->m_miscString.GetString());
			break;

		case LOAD_DEFAULT_CAMERAS:
			InitializeCamerasAndViewports();
			break;

		case LOAD_SOUND_DATABASE:
			LoadSoundManager(pSO->m_miscString.GetString());
			break;

		case OPEN_MENU:
			MenuOpen(pSO->m_miscInt);
			break;

		case LOAD_PARTICLE_SYSTEMS:
			LoadParticleDatabase(pSO->m_miscString.GetString());
			break;

		case LOAD_ICON_DB:
			LoadIconDB(pSO->m_miscString.GetString());
			break;

		case LOAD_SHADER_LIBRARY:
			LoadShaderLibrary(pSO->m_miscString.GetString());
			break;

		case LOAD_GLOBAL_SHADER_LIBRARY:
			LoadGlobalShaderLibrary(pSO->m_miscString.GetString());
			break;

		case LOAD_PIXEL_SHADER_LIBRARY:
			LoadPixelShaderLibrary(pSO->m_miscString.GetString());
			break;

#if (DRF_OLD_PORTALS == 1)
		case LOAD_PORTAL_DATABASE:
			LoadPortalDatabase(pSO->m_miscString.GetString());
			break;
#endif

		case LOAD_ELEMENT_DATABASE:
			LoadElementDatabase(pSO->m_miscString.GetString());
			break;

		case LOAD_STATS_DATABASE:
			LoadStatsDatabase(pSO->m_miscString.GetString());
			break;

		case LOAD_WORLD_RULES:
			LoadWorldRules(pSO->m_miscString.GetString());
			break;

		case LOAD_REFERENCE_LIBRARY:
			LoadReferenceLibrary(pSO->m_miscString.GetString());
			break;

		case LOAD_ENTITY_DATABASE:
			ClientEngine::LoadEntityDatabase(pSO->m_miscString.GetString());
			break;

		default:
			return ClientBaseEngine::ProcessScriptAttribute(pSO);
	}

	return true;
}

void ClientEngine::InitializeEnvironment() {
	if (!g_pD3dDevice)
		return;

	if (!m_environment)
		m_environment = new CEnvironmentObj();

	m_environment->InitAll(g_pD3dDevice, &m_backgroundColorGlobal, m_pRenderStateMgr);
}

void ClientEngine::DeleteSpawnDatabase() {
	if (!m_spawnGeneratorDB)
		return;

	for (POSITION posLoc = m_spawnGeneratorDB->GetHeadPosition(); posLoc != NULL;) {
		auto pSO = (CSpawnObj*)m_spawnGeneratorDB->GetNext(posLoc);
		if (!pSO)
			continue;
		delete pSO;
	}
	m_spawnGeneratorDB->RemoveAll();
	delete m_spawnGeneratorDB;
	m_spawnGeneratorDB = NULL;
}

#if (DRF_OLD_PORTALS == 1)
BOOL ClientEngine::LoadPortalDatabase(const std::string& fileName) {
	// use gamefiles folder for organization
	ScopedDirectoryChanger sdc("GameFiles\\");

	// end use gamefiles folder for organization
	BEGIN_CFILE_SERIALIZE(fl, fileName, false)
	if (m_portalDatabase) {
		m_portalDatabase->SafeDelete();
		delete m_portalDatabase;
		m_portalDatabase = 0;
	}

	CArchive the_in_Archive(fl, CArchive::load);
	the_in_Archive >> m_portalDatabase;
	the_in_Archive.Close();
	END_CFILE_SERIALIZE(fl, m_logger, "LoadPortalDatabase")

	if (!serializeOk)
		return FALSE;

	return TRUE;
}
#endif

void ClientEngine::CategorizeMesh(
	CEXMeshObj* imMesh,
	const Matrix44f& cameraMatrix,
	const Vector3f& actualCenter,
	const Matrix44f& invCameraMatrix,
	const Vector3f& fadeCenter,
	CReferenceObj* refOpt,
	CMaterialObject* custMaterial) {
	//if cusMaterial is set, use it. otherwise use the material in the mesh
	CMaterialObject* matPtr = custMaterial == NULL ? imMesh->m_materialObject : custMaterial;
	if (!imMesh)
		return;

	if (!imMesh->m_Rendered)
		return;

	if (!imMesh->m_abVertexArray.m_indicieBuffer)
		imMesh->InitBuffer(g_pD3dDevice, m_cgGlobalShaderDB->GetByIndex(matPtr->m_optionalShaderIndex - 1), custMaterial);

	if (matPtr->GetFadeByDistance())
		matPtr->SetCurDistFromCam(Distance(cameraMatrix.GetTranslation(), fadeCenter));

	matPtr->Callback();

	if (m_globalShadowType == 1) {
		// Trace Shadows Enabled
		if (matPtr->m_shadowObject == 1) {
			m_shadowObjectsList->CacheMesh(
				imMesh,
				cameraMatrix,
				imMesh->GetWorldTransform(),
				actualCenter,
				refOpt,
				custMaterial);
		}
	}

	char oneBitAlpha = 0;
	if (matPtr->m_doAlphaDepthTest)
		oneBitAlpha = 1;

	if (matPtr->m_blendMode != -1 && oneBitAlpha == 0) {
		// transparent
		m_translucentPolyList->CacheMesh(
			imMesh,
			cameraMatrix,
			imMesh->GetWorldTransform(),
			actualCenter,
			refOpt,
			custMaterial);
	} else {
		if (oneBitAlpha == 1) {
			m_renderContainerAlphaFilterList->CacheMesh(
				imMesh,
				cameraMatrix,
				imMesh->GetWorldTransform(),
				actualCenter,
				refOpt,
				custMaterial);
		} else {
			m_renderContainerList->CacheMeshNoSort(
				imMesh,
				cameraMatrix,
				imMesh->GetWorldTransform(),
				actualCenter,
				refOpt,
				custMaterial);
		}
	}
}

void ClientEngine::SSE_Touch(const TraceInfo& trace) {
	// Send out a 'touch' event to the game server, which will forward it to the script server for this zone.
	ScriptServerEvent* se = new ScriptServerEvent(ScriptServerEvent::Touch, trace.objId);
	int size = SelectionListSize();
	*se->OutBuffer() << trace.objectIntersection.x << trace.objectIntersection.y << trace.objectIntersection.z;
	if (size > 0) {
		*se->OutBuffer() << size;
		for (selection_t::iterator itr = m_selectionList.begin(); itr != m_selectionList.end(); ++itr)
			*se->OutBuffer() << (int)itr->objType << itr->objId;
	} else {
		*se->OutBuffer() << 1 << ObjTypeToInt(ObjectType::DYNAMIC) << trace.objId;
	}
	m_dispatcher.QueueEvent(se);
}

void ClientEngine::SSE_ObjectClick(const TraceInfo& trace, bool rightClick) {
	// Send out a 'objectClick' event to the game server, which will forward it to the script server for this zone.
	ScriptServerEvent* se = new ScriptServerEvent(rightClick ? ScriptServerEvent::ObjectRightClick : ScriptServerEvent::ObjectLeftClick, trace.objId);
	int size = SelectionListSize();
	IWriteableBuffer& xBuffer = *se->OutBuffer();
	xBuffer << m_keyboardState.Shift() << m_keyboardState.Ctrl() << m_keyboardState.Alt();
	xBuffer << trace.objectIntersection.x << trace.objectIntersection.y << trace.objectIntersection.z;
	if (size > 0) {
		xBuffer << size;
		for (selection_t::iterator itr = m_selectionList.begin(); itr != m_selectionList.end(); ++itr)
			xBuffer << ObjTypeToInt(itr->objType) << itr->objId;
	} else {
		xBuffer << 1 << ObjTypeToInt(ObjectType::DYNAMIC) << trace.objId;
	}
	m_dispatcher.QueueEvent(se);
}

bool ClientEngine::IsModalDialogOpen() {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;
	auto pMB = pICE->GetBladeFactory()->GetMenuBlade();
	return pMB ? pMB->IsModalDialogOpen() : false;
}

bool ClientEngine::ResetCursorModesTypes() {
	return KepMenuInstance() ? KepMenuInstance()->ResetCursorModesTypes() : false;
}

bool ClientEngine::SetCursorModeType(const CURSOR_MODE& cursorMode, const CURSOR_TYPE& cursorType) {
	return KepMenuInstance() ? KepMenuInstance()->SetCursorModeType(cursorMode, cursorType) : false;
}

bool ClientEngine::SetCursorMode(const CURSOR_MODE& cursorMode) {
	return KepMenuInstance() ? KepMenuInstance()->SetCursorMode(cursorMode) : false;
}

void ClientEngine::OnMouseLButtonDown(POINT const& cursorPos, bool isOnBlade) {
	MOUSE_BUTTON_STATE& mbs = m_mouseButton[MOUSE_BUTTON_LEFT];

	// If Left Down Click Is On Blade (Menu) Let Blade Handle It
	if (isOnBlade)
		ClientBladeFactory::Instance()->OnLButtonDown(cursorPos.x, cursorPos.y);

	// Update Mouse Button State
	mbs.ButtonDown(cursorPos, isOnBlade);
}

void ClientEngine::OnMouseRButtonDown(POINT const& cursorPos, bool isOnBlade) {
	MOUSE_BUTTON_STATE& mbs = m_mouseButton[MOUSE_BUTTON_RIGHT];

	// If Right Down Click Is On Blade (Menu) Let Blade Handle It
	if (isOnBlade)
		ClientBladeFactory::Instance()->OnRButtonDown(cursorPos.x, cursorPos.y);

	// Update Mouse Button State
	mbs.ButtonDown(cursorPos, isOnBlade);
}

void ClientEngine::OnMouseMButtonDown(POINT const& cursorPos, bool isOnBlade) {
	MOUSE_BUTTON_STATE& mbs = m_mouseButton[MOUSE_BUTTON_MIDDLE];

	// If Middle Down Click Is On Blade (Menu) Let Blade Handle It
	if (isOnBlade)
		ClientBladeFactory::Instance()->OnMButtonDown(cursorPos.x, cursorPos.y);

	// Update Mouse Button State
	mbs.ButtonDown(cursorPos, isOnBlade);
}

void ClientEngine::OnMouseLButtonUp(POINT const& cursorPos) {
	MOUSE_BUTTON_STATE& mbs = m_mouseButton[MOUSE_BUTTON_LEFT];

	// If Saved Left Down Click Was On Blade (Menu) Let Blade Handle Up Click Also
	bool isOnBlade = mbs.clickOnBlade;
	bool allowClick = (!IsModalDialogOpen() && !MouseDragInProgress() && !mbs.HasExceededClickThreshold());
	if (isOnBlade)
		ClientBladeFactory::Instance()->OnLButtonUp(cursorPos.x, cursorPos.y);
	else if (allowClick)
		OnMouseLClick(mbs.clickPos);

	// Update Mouse Button State
	mbs.ButtonUp();
}

void ClientEngine::OnMouseRButtonUp(POINT const& cursorPos) {
	MOUSE_BUTTON_STATE& mbs = m_mouseButton[MOUSE_BUTTON_RIGHT];

	// If Saved Right Down Click Was On Blade (Menu) Let Blade Handle Up Click Also
	bool isOnBlade = mbs.clickOnBlade;
	bool allowClick = (!IsModalDialogOpen() && !MouseDragInProgress() && !mbs.HasExceededClickThreshold());
	if (isOnBlade)
		ClientBladeFactory::Instance()->OnRButtonUp(cursorPos.x, cursorPos.y);
	else if (allowClick)
		OnMouseRClick(mbs.clickPos);

	// Update Mouse Button State
	mbs.ButtonUp();
}

void ClientEngine::OnMouseMButtonUp(POINT const& cursorPos) {
	MOUSE_BUTTON_STATE& mbs = m_mouseButton[MOUSE_BUTTON_MIDDLE];

	// If Saved Middle Down Click Was On Blade (Menu) Let Blade Handle Up Click Also
	bool isOnBlade = mbs.clickOnBlade;
	bool allowClick = (!IsModalDialogOpen() && !MouseDragInProgress() && !mbs.HasExceededClickThreshold());
	if (isOnBlade)
		ClientBladeFactory::Instance()->OnMButtonUp(cursorPos.x, cursorPos.y);
	else if (allowClick)
		OnMouseMClick(mbs.clickPos);

	// Update Mouse Button State
	mbs.ButtonUp();
}

// Sends ClickEvent With 'Left Click' Parameter To Script & ObjectClickEvent To Object Clicked
void ClientEngine::OnMouseLClick(POINT const& cursorPos) {
	int x = (int)cursorPos.x;
	int y = (int)cursorPos.y;

	// Left Click To Place Object ?
	if (m_clickPlaceData.placementId != 0) {
		sendPlaceDynamicObjectAtPositionEvent(
			m_clickPlaceData.objectGlid,
			m_clickPlaceData.invenType,
			m_clickPlaceData.position.x,
			m_clickPlaceData.position.y,
			m_clickPlaceData.position.z,
			m_clickPlaceData.orientation.x,
			m_clickPlaceData.orientation.y,
			m_clickPlaceData.orientation.z);
		if (!m_keyboardState.Shift())
			PlaceDynamicObjectByGlid(GLID_INVALID, 0);
		return;
	}

	// Ray Trace Object Being Clicked On
	Vector3f rayOrigin, rayDir;
	GetPickRay(&rayOrigin, &rayDir, x, y);
	TraceInfo trace;
	RayTrace(&trace, rayOrigin, rayDir, FLT_MAX, ObjectType::ANY, RAYTRACE_VISIBLE);

	// Queue SSE::ObjectClick Event (false=leftClick)
	if (trace.objType == ObjectType::DYNAMIC) {
		SSE_ObjectClick(trace, false);
	}

	// Finally Broadcast 'ClickEvent' (left click)
	IEvent* clickEvent = m_dispatcher.MakeEvent(m_clickEventId);
	if (!clickEvent)
		return;
	clickEvent->SetFilter((ULONG)trace.objType);
	clickEvent->SetObjectId((OBJECT_ID)trace.objId);
	*clickEvent->OutBuffer()
		<< 0 // left click
		<< (int)trace.objType
		<< trace.objId
		<< (m_keyboardState.Ctrl() ? 1 : 0)
		<< (m_keyboardState.Shift() ? 1 : 0)
		<< (m_keyboardState.Alt() ? 1 : 0)
		<< x
		<< y
		<< trace.distance
		<< trace.intersection.x
		<< trace.intersection.y
		<< trace.intersection.z
		<< trace.normal.x
		<< trace.normal.y
		<< trace.normal.z;
	m_dispatcher.ProcessSynchronousEvent(clickEvent);
}

// Sends ClickEvent With 'Right Click' Parameter To Script & ObjectClickEvent To Object Clicked
void ClientEngine::OnMouseRClick(POINT const& cursorPos) {
	int x = (int)cursorPos.x;
	int y = (int)cursorPos.y;

	// Ray Trace Object Being Clicked On
	Vector3f rayOrigin, rayDir;
	GetPickRay(&rayOrigin, &rayDir, x, y);
	TraceInfo trace;
	RayTrace(&trace, rayOrigin, rayDir, FLT_MAX, ObjectType::ANY, RAYTRACE_VISIBLE);

	// Queue SSE::Touch & SSE::ObjectClick Event (true=rightClick) Events
	if (trace.objType == ObjectType::DYNAMIC) {
		SSE_Touch(trace);
		SSE_ObjectClick(trace, true);
	}

	// Finally Broadcast 'ClickEvent' (right click)
	IEvent* clickEvent = m_dispatcher.MakeEvent(m_clickEventId);
	if (!clickEvent)
		return;
	*clickEvent->OutBuffer()
		<< 1 // right click
		<< (int)trace.objType
		<< trace.objId
		<< (m_keyboardState.Ctrl() ? 1 : 0)
		<< (m_keyboardState.Shift() ? 1 : 0)
		<< (m_keyboardState.Alt() ? 1 : 0)
		<< x
		<< y
		<< trace.distance
		<< trace.intersection.x
		<< trace.intersection.y
		<< trace.intersection.z
		<< trace.normal.x
		<< trace.normal.y
		<< trace.normal.z;
	m_dispatcher.ProcessSynchronousEvent(clickEvent);
}

void ClientEngine::OnMouseMClick(POINT const& cursorPos) {
	// Nothing To Do
}

// Sends MouseOverEvent To Object Under Cursor
void ClientEngine::OnMouseOver3DWorld(POINT const& cursorPos) {
	int x = (int)cursorPos.x;
	int y = (int)cursorPos.y;

	// Some quick filtering
	if (m_mouseOverLastObjectType == ObjectType::NONE && m_mouseOver.objType == ObjectType::NONE)
		return;

	if (m_dispatcher.HasHandler(m_mouseOverEventId)) {
		// Create and send event
		IEvent* e = m_dispatcher.MakeEvent(m_mouseOverEventId);

		if (e) {
			//Send mouseover event
			e->SetFilter((ULONG)ObjTypeToInt(m_mouseOver.objType));
			e->SetObjectId((OBJECT_ID)m_mouseOver.objId);
			*e->OutBuffer()
				<< x
				<< y
				<< (m_keyboardState.Ctrl() ? 1 : 0)
				<< (m_keyboardState.Shift() ? 1 : 0)
				<< (m_keyboardState.Alt() ? 1 : 0)
				<< m_mouseOver.distance
				<< m_mouseOver.intersection.x
				<< m_mouseOver.intersection.y
				<< m_mouseOver.intersection.z
				<< m_mouseOver.normal.x
				<< m_mouseOver.normal.y
				<< m_mouseOver.normal.z;

			m_dispatcher.QueueEvent(e);
		}
	}

	// Record last state
	m_mouseOverLastObjectType = m_mouseOver.objType;
}

bool ClientEngine::GetCursorInfo(D3DVIEWPORT9& viewport, POINT& cursorPos, bool& cursorInViewport, bool& cursorOverBlade) {
	// Reset Cursor Info
	ZeroMemory(&viewport, sizeof(viewport));
	ZeroMemory(&cursorPos, sizeof(cursorPos));
	cursorInViewport = false;
	cursorOverBlade = false;

	// Get Client Window Viewport
	if (!g_pD3dDevice)
		return false;
	g_pD3dDevice->GetViewport(&viewport);

	// Get Cursor Position Relative To Client Window Viewport
	::GetCursorPos(&cursorPos);
	ScreenToClient(&cursorPos);
	cursorPos.x -= viewport.X;
	cursorPos.y -= viewport.Y;

	// Is Cursor Within Viewport ?
	RECT viewportRect = { 0, 0, static_cast<LONG>(viewport.Width), static_cast<LONG>(viewport.Height) };
	cursorInViewport = PointInsideRect(viewportRect, cursorPos);

	// Is Cursor Over Blade (menu)
	auto cbf = ClientBladeFactory::Instance();
	if (cbf)
		cursorOverBlade = cursorInViewport && cbf->HitTest(cursorPos.x, cursorPos.y);

	return true;
}

bool ClientEngine::MouseKeyboardUpdate() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "MouseKeyboardUpdate");

	// Get Cursor Info
	D3DVIEWPORT9 viewport;
	POINT cursorPos;
	bool cursorInViewport;
	bool cursorOverBlade;
	if (!GetCursorInfo(viewport, cursorPos, cursorInViewport, cursorOverBlade)) {
		LogError("GetCursorInfo() FAILED");
		return false;
	}

	// Save Normalized Cursor Position Within Viewport [0.0 -> 1.0] (used for object rotation drag)
	m_mouseNormalizedX = ((float)cursorPos.x) / ((float)viewport.Width);
	m_mouseNormalizedY = ((float)cursorPos.y) / ((float)viewport.Height);

	// Cursor Enter/Leave Viewport ?
	bool cursorEnterViewport = (cursorInViewport && !m_cursorInViewportWas);
	bool cursorLeaveViewport = (!cursorInViewport && m_cursorInViewportWas);
	if (cursorEnterViewport)
		LogInfo("cursorEnterViewport");
	if (cursorLeaveViewport)
		LogInfo("cursorLeaveViewport");
	m_cursorInViewportWas = cursorInViewport;

	// Save Previous Keyboard & Mouse State
	m_keyboardStateWas = m_keyboardState;
	m_mouseStateWas = m_mouseState;

	// Get Current Keyboard State
	m_keyboardState.Enable(m_activated); // drf - bug fix - only respond to keyboard while in focus
	m_keyboardState.Update();
	//	LogInfo(m_keyboardState.ToStr());

	// Get Current Mouse State (only used for buttons and scroll wheel)
	m_mouseState.Enable(m_activated && m_bIsDirectInputMouseProbeAllowed); // drf - bug fix - only respond to mouse while in focus
	m_mouseState.Update();
	//	LogInfo(m_mouseState.ToStr());

	// Get Previous Mouse State Buttons
	bool wasMouseDownL = m_mouseStateWas.ButtonDownLeft();
	bool wasMouseDownR = m_mouseStateWas.ButtonDownRight();
	bool wasMouseDownM = m_mouseStateWas.ButtonDownMiddle();

	// Get Current Mouse State Buttons
	bool isMouseDownL = m_mouseState.ButtonDownLeft();
	bool isMouseDownR = m_mouseState.ButtonDownRight();
	bool isMouseDownM = m_mouseState.ButtonDownMiddle();
	bool isMouseDown = (isMouseDownL || isMouseDownR || isMouseDownM);

	// Send Key Pressed Event (used only by DDR for triggering dance moves)
	if (m_activated && m_dispatcher.HasHandler(m_keyPressedEvent)) {
		IEvent* e = m_dispatcher.MakeEvent(m_keyPressedEvent);
		(*e->OutBuffer()) << (ULONG)m_keyboardState.Ptr();
		m_dispatcher.ProcessSynchronousEvent(e);
	}

	// Update Keyboard State (sends key changed events (down/up/repeat))
	// DRF - ED-1173 - Editbox Delete Pickup Bug Fix
	// Do not allow this while dialogs are capturing keys otherwise
	// build mode will pickup the selected object if you hit DEL key
	// to delete characters from the edit box in focus.
	if (!m_keyCaptured) {
		for (size_t i = 0; i < m_keyboardState.Keys(); i++)
			UpdateKey(i);
	}

	// Is Widget Visible ?
	bool widgetVisible = m_kepWidget && m_kepWidget->visible();

	// Is Cursor Within Client Viewport Or Leaving Viewport ?
	if (cursorInViewport || cursorLeaveViewport) {
		// Determine object cursor mode
		MOS mos;
		mos.cursorType = CURSOR_TYPE_DEFAULT;
		m_cursorModeObject = (MOS_Get(mos, m_mouseOver.objType, m_mouseOver.objId) && mos.modeCursor());

		// DRF - Determine Cursor Mode (highest priority first)
		// 1. MENU - cursor is over menu (priority 1)
		// 2. WIDGET - widget is visible (priority 2)
		// 3. OBJECT - cursor is over object (priority 3)
		// 4. WORLD - everything else (priority 4)
		if (cursorOverBlade) {
			SetCursorMode(CURSOR_MODE_MENU);
		} else if (widgetVisible) {
			SetCursorMode(CURSOR_MODE_WIDGET);
		} else if (m_cursorModeObject) {
			// Update cursor type before cursor mode to avoid unnecessary cursor switching in KEPMenu
			SetCursorModeType(CURSOR_MODE_OBJECT, mos.cursorType);
			SetCursorMode(CURSOR_MODE_OBJECT);
		} else {
			SetCursorMode(CURSOR_MODE_WORLD);
		}

		// Handle Widget If Visible
		bool widgetWants = false; // what the widget wants the widget gets
		if (widgetVisible) {
			// Get pick ray for this frame
			Vector3f pickRayOrigin(GetPickRayOrigin()), pickRayDirection(GetPickRayDirection());

			// See if cursor is over some part of the widget
			Vector3f intersection;
			int hoverAxis = m_kepWidget->mouseOverTest(pickRayOrigin, pickRayDirection, &intersection);

			// If the mouse is not over any part of the widget, then see
			// if it is over the object geometry itself
			if (hoverAxis == NoAxis) {
				TraceInfo trace;
				if (RayTrace(&trace, pickRayOrigin, pickRayDirection, FLT_MAX, ObjectType::ANY, RAYTRACE_VISIBLE | RAYTRACE_SELECTED | INTERSECTION_IN_WORLD_SPACE)) {
					// Depending on what mode the widget is in, we need to use a default movement axis/plane.
					// For translation, this is the XZ plane.  For rotation, it is the Y axis.
					switch (m_kepWidget->getWidgetMode()) {
						case Selected_Single_Translation:
						case Selected_Multi_Translation:
							hoverAxis = OnObject;
							break;

						case Selected_Single_Rotation:
						case Selected_Multi_Rotation:
							hoverAxis = YAxis;
							break;
					}

					if (hoverAxis != NoAxis) {
						// Get widget inverse transformation matrix
						Matrix44f widgetMatrixInverse;
						m_kepWidget->getWidgetMatrixInverse(&widgetMatrixInverse);

						// Convert 'intersection' to widget space
						intersection = TransformPoint(widgetMatrixInverse, trace.intersection);
					}
				}
			}

			Vector3f cameraPos(0, 0, 0);
			CMovementObj* player = GetMovementObjMe();
			if (player) {
				CCameraObj* camera = GetActiveCameraForMovementObject(player);
				if (camera)
					cameraPos = camera->m_camWorldPosition;
			}

			// Don't update hover state during a drag
			if (!isMouseDownR && !isMouseDownL)
				m_kepWidget->updateHoverState(cursorOverBlade ? 0 : hoverAxis);

			// Left Mouse Button Just Clicked ?
			if (isMouseDownL && !wasMouseDownL) {
				if (!cursorOverBlade) {
					widgetWants = m_kepWidget->handleMouse(KEP_WIDGET_L_MOUSE_DOWN, hoverAxis, intersection, pickRayOrigin, pickRayDirection, cameraPos);
					if (hoverAxis == OnObject && widgetWants) {
						// DRF - Disable Mouse Cursor & Save Build Move Widget Position
						if (GetCursorEnabled()) {
							SetCursorEnabled(false);
							m_kepWidget->saveCursorPos(intersection);
						}
					}
				}

				// Left Mouse Button Just Released ?
			} else if (!isMouseDownL && wasMouseDownL) {
				if (!cursorOverBlade)
					widgetWants = m_kepWidget->handleMouse(KEP_WIDGET_L_MOUSE_UP, hoverAxis, intersection, pickRayOrigin, pickRayDirection, cameraPos);

				// DRF - Enable Mouse Cursor & Restore Build Move Widget
				if (!GetCursorEnabled()) {
					SetCursorEnabled(true);
					m_kepWidget->restoreCursorPos();
				}

				// Left Mouse Button Still Clicked
			} else if (isMouseDownL && wasMouseDownL) {
				// Move/Rotate Widget ?
				Vector3f oldPos, newPos;
				oldPos = m_kepWidget->getWidgetPosition();
				widgetWants = m_kepWidget->handleMouse(KEP_WIDGET_L_MOUSE_DRAG, hoverAxis, intersection, pickRayOrigin, pickRayDirection, cameraPos);
				if (widgetWants) {
					if (m_kepWidget->getWidgetMode() == Selected_Single_Translation || m_kepWidget->getWidgetMode() == Selected_Multi_Translation) {
						// Move Widget
						newPos = m_kepWidget->getWidgetPosition();
						if (oldPos.x != newPos.x || oldPos.y != newPos.y || oldPos.z != newPos.z) {
							IEvent* moveEvent = m_dispatcher.MakeEvent(m_widgetTranslationEventId);
							if (moveEvent) {
								*moveEvent->OutBuffer() << newPos.x << newPos.y << newPos.z;
								m_dispatcher.ProcessSynchronousEvent(moveEvent);
							}
						}
					} else if (m_kepWidget->getWidgetMode() == Selected_Single_Rotation || m_kepWidget->getWidgetMode() == Selected_Multi_Rotation) {
						// Rotate Widget
						IEvent* rotateEvent = m_dispatcher.MakeEvent(m_widgetRotationEventId);
						if (rotateEvent) {
							Vector3f rotation = m_kepWidget->getRotationAngle();
							newPos = m_kepWidget->getWidgetPosition();
							*rotateEvent->OutBuffer() << newPos.x << newPos.y << newPos.z << rotation.x << rotation.y << rotation.z;
							m_dispatcher.ProcessSynchronousEvent(rotateEvent);
						}
					}
				}

				// Left Mouse Button Still Released
			} else if (!isMouseDownL) {
				// DRF - Enable Mouse Cursor & Restore Build Move Widget
				if (!GetCursorEnabled()) {
					SetCursorEnabled(true);
					m_kepWidget->restoreCursorPos();
				}
			}
		} // if (widgetVisible)

		// DRF - Dissappearing Mouse Cursor Bug Fix
		// Without this we lose the mouse cursor if a player enters or leave your zone while
		// you are moving a selected object in 'Advanced Build Mode' since Selection.lua does
		// many clearWidget() calls on any dynamic object changes which includes the green man
		// placeholder for players zoning in and out or if anyone else drops or moves an object
		// in the zone while you are doing the same.

		// CJW - Cursor is supposed to be kept hidden while steering vehicles with the mouse.
		if (!isMouseDownL && !IsSteeringCarWithMouse()) {
			// DRF - Enable Mouse Cursor & Restore Build Move Widget
			IMenuBlade* pKepMenu = KepMenuInstance();
			bool dialogMouseEnabled = (!pKepMenu || pKepMenu->IsDialogFrontmostMouseEnabled());
			if (dialogMouseEnabled && !GetCursorEnabled())
				SetCursorEnabled(true);
		}

		// Trigger Widget Event If Widget Wants It
		WidgetEventTriggered(widgetWants);

		// DRF - Bug Fix - Moved before drag logic below since OnMouseButtonDown()
		// is what sets m_clickPos which is needed to calculate drag deltas.
		if (isMouseDownL && !wasMouseDownL)
			OnMouseLButtonDown(cursorPos, cursorOverBlade);
		else if (!isMouseDownL && wasMouseDownL)
			OnMouseLButtonUp(cursorPos);
		if (isMouseDownR && !wasMouseDownR)
			OnMouseRButtonDown(cursorPos, cursorOverBlade);
		else if (!isMouseDownR && wasMouseDownR)
			OnMouseRButtonUp(cursorPos);
		if (isMouseDownM && !wasMouseDownM)
			OnMouseMButtonDown(cursorPos, cursorOverBlade);
		else if (!isMouseDownM && wasMouseDownM)
			OnMouseMButtonUp(cursorPos);

		// Update Mouse Button State Drag
		bool dragInProgress = MouseDragInProgress(cursorPos);

		// Allow OnMouseOver Of World Objects If:
		// - cursor is not over a blade (menu)
		// - mouse buttons are not down
		// - dragging is not in progress
		// - move widget is not active
		bool onMouseOverAllowed = (!cursorOverBlade && !isMouseDown && !dragInProgress && !widgetWants);
		if (onMouseOverAllowed)
			OnMouseOver3DWorld(cursorPos);

	} else { // if (!cursorInViewport)
		// Don't allow camera rotations while the widget is visible.
		// This is gonna cause problems when we turn off the widget
		// and only allow object dragging by default.  Need to test
		// that one and figure it out too.
		WidgetEventTriggered(widgetVisible);
	} // if (cursorInViewport)

	if (GetMouseKeyboardUpdate()) {
		m_mouseHiResX = (float)m_mouseState.DeltaX();
		m_mouseHiResY = (float)m_mouseState.DeltaY();
	} else {
		m_mouseHiResX = (float)0;
		m_mouseHiResY = (float)0;
	}

	return true;
}

void ClientEngine::EnableDirectInput(bool bEnable) {
	m_mouseState.Enable(bEnable);
	if (m_pCABD3DLIB) {
		m_pCABD3DLIB->AcquireDirectInput(bEnable);
	}
	m_bIsDirectInputMouseProbeAllowed = bEnable;
}

void ClientEngine::UpdateKey(size_t index) {
	// Has The Key State Changed ?
	bool keyDown = m_keyboardState.KeyDown(index);
	bool keyDownWas = m_keyboardStateWas.KeyDown(index);
	if (keyDown != keyDownWas) {
		// Send Key Changed Event (Down/Up)
		IEvent* e = m_dispatcher.MakeEvent(m_keyChangedEvent);
		e->SetFilter(ScanCodeToVK(index));
		if (keyDown) {
			(*e->OutBuffer()) << 1; // key down

			// Reset Key Repeat Rate
			m_keyboardPresses[index] = 1;
			m_keyboardPressedTimer[index].timer().Reset();
		} else {
			(*e->OutBuffer()) << 0; // key up
		}
		m_dispatcher.QueueEvent(e);

		// Is The Key Being Held Down ?
	} else if (keyDown) {
		// Update Key Repeat Rate
		int delay = KeyboardState::GetRepeatRateHz();
		if (m_keyboardPresses[index] == 1)
			delay = KeyboardState::GetRepeatDelayMs();
		m_keyboardPressedTimer[index].setDelayTime((TimeMs)delay);
		if (m_keyboardPressedTimer[index].elapsed()) {
			// Send Key Changed Event (Down Repeat)
			IEvent* e = m_dispatcher.MakeEvent(m_keyChangedEvent);
			e->SetFilter(ScanCodeToVK(index));
			(*e->OutBuffer()) << 1; // key down
			m_dispatcher.QueueEvent(e);

			// Reset Key Repeat Rate
			m_keyboardPresses[index]++;
			m_keyboardPressedTimer[index].timer().Reset();
		}
	}
}

int ClientEngine::ScanCodeToVK(size_t scanCode) {
	switch (scanCode) {
		case DIK_INSERT:
			return VK_INSERT;
		case DIK_DELETE:
			return VK_DELETE;
		case DIK_HOME:
			return VK_HOME;
		case DIK_END:
			return VK_END;
		case DIK_NEXT:
			return VK_NEXT;
		case DIK_PRIOR:
			return VK_PRIOR;
		case DIK_UP:
			return VK_UP;
		case DIK_DOWN:
			return VK_DOWN;
		case DIK_RIGHT:
			return VK_RIGHT;
		case DIK_LEFT:
			return VK_LEFT;
		case DIK_NUMPAD0:
			return VK_NUMPAD0;
		case DIK_NUMPAD1:
			return VK_NUMPAD1;
		case DIK_NUMPAD2:
			return VK_NUMPAD2;
		case DIK_NUMPAD3:
			return VK_NUMPAD3;
		case DIK_NUMPAD4:
			return VK_NUMPAD4;
		case DIK_NUMPAD5:
			return VK_NUMPAD5;
		case DIK_NUMPAD6:
			return VK_NUMPAD6;
		case DIK_NUMPAD7:
			return VK_NUMPAD7;
		case DIK_NUMPAD8:
			return VK_NUMPAD8;
		case DIK_NUMPAD9:
			return VK_NUMPAD9;
		case DIK_DECIMAL: // NumPad .
			return VK_DECIMAL;
		default:
			return MapVirtualKey(scanCode, MAPVK_VSC_TO_VK);
	}
}

void ClientEngine::AddGroupFilter(int groupNumber) {
	if (m_groupFilterCount > 99) {
		// maxed out
		return;
	}

	m_groupFilterArray[m_groupFilterCount] = groupNumber;
	m_groupFilterCount++;
}

BOOL ClientEngine::ValidateGroup(int groupNumber) {
	if (!m_groupFilterCount) {
		return TRUE;
	}

	for (int loop = 0; loop < m_groupFilterCount; loop++) {
		if (m_groupFilterArray[loop] == groupNumber) {
			return FALSE;
		}
	}

	return TRUE;
}

void ClientEngine::LoadSoundManager(const std::string& fileName) {
	// use gamefiles folder for organization
	ScopedDirectoryChanger sdc("GameFiles\\");

	// end use gamefiles folder for organization
	BEGIN_CFILE_SERIALIZE(fl, fileName, false)
	m_soundManager->SafeDelete();
	delete m_soundManager;
	m_soundManager = 0;

	CArchive the_in_Archive(fl, CArchive::load);
	the_in_Archive >> m_soundManager;
	the_in_Archive.Close();
	END_CFILE_SERIALIZE(fl, m_logger, "LoadSoundManager")
	if (!serializeOk) {
		return;
	}

	{
		ScopedDirectoryChanger sdc("sounds\\");
		m_soundManager->Initialize(m_pDirectSound);
	}
	return;
}

void ClientEngine::GetScreenXY(
	double localZpos,
	double clipPlane,
	const Vector3f& localizePosition,
	double& xScreenPosition,
	double& yScreenPosition,
	double fov,
	double vPortHAspect,
	double screenWidth,
	double screenHeight) {
	// Calculate Screen Aspect Ratio
	double fAspect = screenHeight / screenWidth;

	double compromizer = (1.0 + (1.0 - fAspect));
	double compromizer2 = (1.0 + (1.0 - vPortHAspect));
	double leftClipPlane = (-1 * tan((0.51 * compromizer) * fov));
	double rightClipPlane = (tan((0.51 * compromizer) * fov));
	double topClipPlane = (tan(((0.51 * compromizer2) * fov)));
	double bottomClipPlane = (-1 * tan(((0.51 * compromizer2) * fov)));

	double ScaleFactor = clipPlane / localZpos;
	double dScrnPosHorz = ((localizePosition.x * ScaleFactor) - leftClipPlane) * (1.2 / (rightClipPlane - leftClipPlane));
	double dScrnPosVert = (topClipPlane - (localizePosition.y * ScaleFactor)) * (0.9 / (topClipPlane - bottomClipPlane));

	// Set Screen Position
	xScreenPosition = dScrnPosHorz - 0.6;
	yScreenPosition = -(dScrnPosVert - 0.45);
}

bool ClientEngine::DisplayTextBasedOn3dPosition(
	const Vector3f& wldSpcPos,
	const Matrix44f& cameraMatrix,
	const std::string& textStr,
	double maxDistance,
	double minDistance,
	double fontSize,
	CTextRenderObj*& pTRO,
	const Vector3d& color,
	double yoffset) {
	if (!m_runtimeViewportDB)
		return false;

	// Trim String
	CStringA textCStr = textStr.c_str();
	if (textCStr.Trim().IsEmpty())
		return false;
	std::string textString = textCStr.GetString();

	// Get Active Viewport
	auto pVO = GetActiveViewport();
	if (!pVO)
		return false;

	// Get Active Viewport Camera
	CCameraObj* pCO = pVO->GetCamera();
	if (!pCO)
		return false;

	// Get My Avatar
	CMovementObj* pMO_me = GetMovementObjMe();
	if (!pMO_me || !pMO_me->getBaseFrame()) {
		//		LogFatal("DRF_CRASH_FIX"); // happens during rezone with paperdolls
		return false;
	}

	Vector3f camPos(cameraMatrix.GetTranslation());
	Vector3f camAt(cameraMatrix.GetRowZ());
	Vector3f distV = pMO_me->getBaseFramePosition() - camPos;
	Vector3f targPos = camPos + camAt * distV.Length();
	targPos.y = wldSpcPos.y;
	Vector3f localPosition = MatrixARB::GetLocalPositionFromMatrixView(cameraMatrix, targPos);

	// Get Screen Position
	double xLocal, yLocal;
	GetScreenXY(
		(double)localPosition.z,
		1.0,
		localPosition,
		xLocal,
		yLocal,
		(double)pVO->GetCameraFovY(),
		(double)pCO->m_heightBaseAspect,
		1.2,
		0.9);

	// Limit Distance Scaling
	double posZ = (double)localPosition.z;
	if (posZ < minDistance)
		posZ = minDistance;
	if (posZ > maxDistance)
		posZ = maxDistance;

	// Calculate Distance Scaling
	double camFovX = pVO->GetCameraFovX();
	double multiplier = (1.0 - (posZ * camFovX) / maxDistance);

	// Multiplier Cannot Be Negative Otherwise Text Labels Go Backwards
	multiplier = abs(multiplier);

	// Calculate Zoomed Font Size
	fontSize = fontSize * multiplier;

	// Hide Fonts Too Small
	const double fontSizeMin = 0.0062;
	if (fontSize < fontSizeMin)
		return true;

	Vector3f camToObj = cameraMatrix.GetTranslation() - wldSpcPos;
	const double maxDist = 20.0;
	const double minDist = 5.0;
	double curDist = camToObj.Length();
	if (curDist > maxDist)
		curDist = maxDist;
	else if (curDist < minDist)
		curDist = minDist;

	double scaleFactor = curDist - 0.3;
	Matrix44f worldCalc, scaleCalc;
	worldCalc = cameraMatrix;
	double yadjust = yLocal + (yoffset * scaleFactor);
	worldCalc.GetTranslation() = wldSpcPos + (yadjust * cameraMatrix.GetRowY());
	scaleCalc.MakeScale(Vector3f(scaleFactor, scaleFactor, 1.0f));
	worldCalc = scaleCalc * worldCalc;

	// Create New Text Render Object ?
	if (!pTRO)
		pTRO = new CTextRenderObj(textString, 2, xLocal, yLocal, fontSize, fontSize, true, color.x, color.y, color.z);
	if (!pTRO)
		return false;

	// Set Text Render Geometry
	pTRO->m_worldMatrix = worldCalc;
	pTRO->m_measuredDistanceRef = pTRO->m_unitLength * (2.0 * fontSize);
	pTRO->m_startPosX = xLocal - (0.5 * pTRO->m_measuredDistanceRef);
	pTRO->m_startPosY = 0.0;
	pTRO->m_fontSizeX = fontSize;
	pTRO->m_fontSizeY = fontSize;
	pTRO->UpdateGeometry(g_pD3dDevice, textString);

	m_textRenderList->AddTail(pTRO);

	return true;
}

void ClientEngine::UseItemByGLID(const GLID& glid) {
	if (!ClientIsEnabled())
		return;

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;

	auto netTraceId = pMO->getNetTraceId();
	if (netTraceId <= 0)
		return;

	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::UseItem;
	attribData.aeShort1 = netTraceId;
	attribData.aeInt1 = glid;
	m_multiplayerObj->QueueMsgAttribToServer(attribData);
}

BOOL ClientEngine::LoadIconDB(const std::string& fileName) {
	// use gamefiles folder for organization
	ScopedDirectoryChanger sdc("GameFiles\\");

	// end use gamefiles folder for organization
	BEGIN_CFILE_SERIALIZE(fl, fileName, false)
	CArchive the_in_Archive(fl, CArchive::load);

	m_iconDatabase->SafeDelete();
	delete m_iconDatabase;
	m_iconDatabase = 0;
	the_in_Archive >> m_iconDatabase;
	the_in_Archive.Close();
	END_CFILE_SERIALIZE(fl, m_logger, "LoadIconDB")
	if (!serializeOk) {
		return FALSE;
	}

	m_iconDatabase->Initialize(Vector3f(0, 0, 0), g_pD3dDevice);

	return TRUE;
}

BOOL ClientEngine::LoadHouseDB(const std::string& fileName) {
	if (ClientBaseEngine::LoadHouseDB(fileName)) {
		m_housingDB->ReinitAll(g_pD3dDevice);
		return TRUE;
	}

	return FALSE;
}

BOOL ClientEngine::LoadArenaSys(const std::string& fileName) {
	// use gamefiles folder for organization
	ScopedDirectoryChanger sdc("GameFiles\\");

	// end use gamefiles folder for organization
	BEGIN_CFILE_SERIALIZE(fl, fileName, false)
	CArchive the_in_Archive(fl, CArchive::load);
	if (m_battleSchedulerSys) {
		m_battleSchedulerSys->SafeDelete();
		delete m_battleSchedulerSys;
		m_battleSchedulerSys = 0;
	}

	the_in_Archive >> m_battleSchedulerSys;
	the_in_Archive.Close();
	END_CFILE_SERIALIZE(fl, m_logger, "LoadArenaSys")

	return serializeOk ? TRUE : FALSE;
}

BOOL ClientEngine::LoadWorldRules(const std::string& fileName) {
	// use gamefiles folder for organization
	ScopedDirectoryChanger sdc("GameFiles\\");

	// end use gamefiles folder for organization
	BEGIN_CFILE_SERIALIZE(fl, fileName, false)
	CArchive the_in_Archive(fl, CArchive::load);
	if (m_worldChannelRuleDB) {
		m_worldChannelRuleDB->SafeDelete();
		delete m_worldChannelRuleDB;
		m_worldChannelRuleDB = 0;
	}

	the_in_Archive >> m_worldChannelRuleDB;
	the_in_Archive.Close();
	END_CFILE_SERIALIZE(fl, m_logger, "LoadWorldRules")

	return serializeOk ? TRUE : FALSE;
}

void ClientEngine::ManageEnvEffects(CWldObject* wldObj, CEXMeshObj* mObj) {
	ManageEnvEffects(wldObj, mObj->m_materialObject);
}

void ClientEngine::ManageEnvEffects(CWldObject* wldObj, CMaterialObject* material) {
	if (wldObj->m_envReactionAttrib > 0) {
		// switch style for environment state
		if (m_environment->m_environmentTimeSystem) {
			if (m_environment->m_environmentTimeSystem->m_pDSO) {
				if (m_environment->m_environmentTimeSystem->m_pDSO->m_sunColorRed > m_environment->m_environmentTimeSystem->m_pDSO->m_ambientColorRed ||
					m_environment->m_environmentTimeSystem->m_pDSO->m_sunColorGreen > m_environment->m_environmentTimeSystem->m_pDSO->m_ambientColorGreen ||
					m_environment->m_environmentTimeSystem->m_pDSO->m_sunColorBlue > m_environment->m_environmentTimeSystem->m_pDSO->m_ambientColorBlue) {
					switch (static_cast<MeshEnvEffect>(wldObj->m_envReactionAttrib)) {
						case MeshEnvEffect::LockEmissiveModulation:
							material->m_texBlendMethod[0] = CMaterialObject::TextureOp::TOP_DISABLE;
							material->m_useMaterial.Emissive.r = 0.0f;
							material->m_useMaterial.Emissive.g = 0.0f;
							material->m_useMaterial.Emissive.b = 0.0f;
							material->m_useMaterial.Emissive.a = 0.0f;
							break;
						case MeshEnvEffect::LockEmissive:
							material->m_useMaterial.Emissive.r = 0.0f;
							material->m_useMaterial.Emissive.g = 0.0f;
							material->m_useMaterial.Emissive.b = 0.0f;
							material->m_useMaterial.Emissive.a = 0.0f;
							break;
						case MeshEnvEffect::MultiPassModulate2X:
							material->m_texBlendMethod[0] = CMaterialObject::TextureOp::TOP_MODULATE;
							material->m_texBlendMethod[1] = CMaterialObject::TextureOp::TOP_DISABLE;
							break;
					}
				} else {
					switch (static_cast<MeshEnvEffect>(wldObj->m_envReactionAttrib)) {
						case MeshEnvEffect::LockEmissiveModulation:
							material->m_texBlendMethod[0] = CMaterialObject::TextureOp::TOP_MODULATE;
							material->m_useMaterial.Emissive.r = 1.0f;
							material->m_useMaterial.Emissive.g = 1.0f;
							material->m_useMaterial.Emissive.b = 1.0f;
							material->m_useMaterial.Emissive.a = 1.0f;
							break;
						case MeshEnvEffect::LockEmissive:
							material->m_useMaterial.Emissive.r = 1.0f;
							material->m_useMaterial.Emissive.g = 1.0f;
							material->m_useMaterial.Emissive.b = 1.0f;
							material->m_useMaterial.Emissive.a = 1.0f;
							break;
						case MeshEnvEffect::MultiPassModulate2X:
							material->m_texBlendMethod[0] = CMaterialObject::TextureOp::TOP_MODULATE2X;
							material->m_texBlendMethod[1] = CMaterialObject::TextureOp::TOP_MODULATE2X;
							break;
					}
				}
			}
		}
	}
}

// Send out the inventory to subscribers. Most likely the inventory menu.
void ClientEngine::SendInventoryWebEvent(const std::list<InventoryWeb*>& webInventoryItems, int totalNumRecords) {
	if (webInventoryItems.size() <= 0)
		return;

	IEvent* e = m_dispatcher.MakeEvent(m_attribEventId);
	e->SetFilter((ULONG)eAttribEventType::PlayerInventory);

	(*e->OutBuffer()) << (int)totalNumRecords << (int)webInventoryItems.size();

	for (const auto& pWI : webInventoryItems) {
		(*e->OutBuffer()) << atoi(pWI->m_globalId.c_str())
						  << atoi(pWI->m_quantity.c_str())
						  << pWI->m_name.c_str()
						  << atoi(pWI->m_useTypeStr.c_str())
						  << pWI->m_texture_name
						  << pWI->m_texture_coords.left << pWI->m_texture_coords.top
						  << pWI->m_texture_coords.right << pWI->m_texture_coords.bottom
						  << atoi(pWI->m_sellingPrice.c_str());

		(*e->OutBuffer()) << (int)pWI->m_passes.size();

		for (const auto& pWIP : pWI->m_passes) {
			(*e->OutBuffer()) << atoi(pWIP->m_passId.c_str())
							  << pWIP->m_passName.c_str()
							  << pWIP->m_passDescription.c_str();
		}

		(*e->OutBuffer()) << atoi(pWI->m_isDefault.c_str())
						  << atoi(pWI->m_inventoryType.c_str())
						  << pWI->m_description.c_str()
						  // inventorySubType and armed given to subscriber instead of using negative glid to indicate
						  // a gift and a negative quantity to indicate the item is armed.
						  << atoi(pWI->m_inventorySubType.c_str())
						  << pWI->m_armed.c_str();
	}

	m_dispatcher.QueueEvent(e);
}

bool ClientEngine::RequestCharacterInventory(eAttribEventType invType) {
	eAttribEventType requestedAttribute = eAttribEventType::GetInventoryPlayer;
	if (invType >= eAttribEventType::GetInventoryPlayer && invType <= eAttribEventType::GetInventoryAll)
		requestedAttribute = invType;

	if (!ClientIsEnabled())
		return false;

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	// Request Character Inventory
	ATTRIB_DATA attribData;
	attribData.aeType = requestedAttribute;
	attribData.aeShort1 = pMO->getNetTraceId();
	attribData.aeInt1 = REQUEST_FOR_INVENTORY_ID;
	return m_multiplayerObj->QueueMsgAttribToServer(attribData);
}

bool ClientEngine::RequestCharacterInventoryAttachableObjects() {
	if (!ClientIsEnabled())
		return false;

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	// Request character attachable object inventory
	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::GetInventoryPlayerAttachable;
	attribData.aeShort1 = pMO->getNetTraceId();
	attribData.aeInt1 = REQUEST_FOR_INVENTORY_ID;
	return m_multiplayerObj->QueueMsgAttribToServer(attribData);
}

bool ClientEngine::RequestPictureFrames() {
	if (!ClientIsEnabled())
		return false;

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::GetInventoryPlayer;
	attribData.aeShort1 = pMO->getNetTraceId();
	attribData.aeInt1 = REQUEST_FOR_INVENTORY_ID;
	return m_multiplayerObj->QueueMsgAttribToServer(attribData);
}

bool ClientEngine::UseInventoryItemOnTarget(const GLID& glid, int objType, int objId, short invType) {
	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	// Valid Glid ?
	if (!IS_VALID_GLID(glid)) {
		LogError("Invalid glid<" << glid << ">");
		return false;
	}

	// Get Item From Inventory
	CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(glid);
	if (!pGIO) {
		LogError("GetByGLID() FAILED - glid<" << glid << ">");
		return false;
	}

	// Valid Item Data ?
	if (!pGIO->m_itemData) {
		LogError("itemData is null - glid<" << glid << ">");
		return false;
	}

	// Valid Use Type ?
	USE_TYPE useType = pGIO->m_itemData->m_useType;
	if (useType == USE_TYPE_NONE) {
		LogError("Invalid useType=" << (int)useType << " - glid<" << glid << ">");
		return false;
	}

	// house item verify placement validity locally before even using this item
	bool useItemNow = true;
	if (useType == USE_TYPE_PLACE_HOUSE) {
		Vector3f positionWld = pMO->getBaseFramePosition();
		CHousingObj* houseBasePtr = m_housingDB->GetByIndex(pGIO->m_itemData->m_miscUseValue);
		if (houseBasePtr) {
			//valid database base
			if (!HousePlacementValidCheck(positionWld, houseBasePtr->m_clipBox)) {
				useItemNow = false;
			}
		} else
			useItemNow = false;
	}

	if (useItemNow) {
		if (pGIO->m_itemData->m_reloadTime == 0) {
			ATTRIB_DATA attribData;
			attribData.aeType = eAttribEventType::UseItem;
			attribData.aeShort1 = pMO->getNetTraceId();
			attribData.aeShort2 = invType;
			attribData.aeInt1 = glid;
			attribData.aeInt2 = (objType & ObjTypeToInt(ObjectType::DYNAMIC)) ? objId : 0;
			m_multiplayerObj->QueueMsgAttribToServer(attribData);
		} else {
			m_pendingObject->AddCommand(1, glid, pMO->getNetTraceId(), pGIO->m_itemData->m_reloadTime, "");
		}
	}

	return true;
}

bool ClientEngine::UseInventoryItem(const GLID& glid, short invType) {
	return UseInventoryItemOnTarget(glid, -1, -1, invType);
}

bool ClientEngine::RemoveInventoryItem(const GLID& glid, int qty, short invType) {
	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	// Valid Glid ?
	if (!IS_VALID_GLID(glid)) {
		LogError("Invalid glid<" << glid << ">");
		return false;
	}

	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::DestroyItemByGlid;
	attribData.aeShort1 = pMO->getNetTraceId();
	attribData.aeShort2 = invType;
	attribData.aeInt1 = glid;
	attribData.aeInt2 = qty;
	return m_multiplayerObj->QueueMsgAttribToServer(attribData);
}

bool ClientEngine::RemoveBankItem(const GLID& glid, int qty, short invType) {
	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	// Valid Glid ?
	if (!IS_VALID_GLID(glid)) {
		LogError("Invalid glid<" << glid << ">");
		return false;
	}

	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::DestroyBankItemByGlid;
	attribData.aeShort1 = pMO->getNetTraceId();
	attribData.aeShort2 = invType;
	attribData.aeInt1 = glid;
	attribData.aeInt2 = qty;
	return m_multiplayerObj->QueueMsgAttribToServer(attribData);
}

// DRF - This function returns the list of keyboard/mouse control connection to UI actions.
// It is called from Control_Settings.lua when a user wants to change the control action mapping.
bool ClientEngine::GetControlConfiguration(std::vector<std::string>& pConfig) {
	POSITION cntPos = m_controlsList->FindIndex(0);
	if (!cntPos) {
		LogError("m_controlsList Empty");
		return false;
	}

	ControlDBObj* cObj = (ControlDBObj*)m_controlsList->GetAt(cntPos);
	int cntLoop = 0;

	CStringA controlAction;
	while (cntLoop < (int)eCommandAction::Num_Actions) {
		int miscTemp1 = 0;
		int miscTemp2 = 0;
		int miscTemp3 = 0;
		int miscTemp4 = 0;

		controlAction = cObj->m_controlInfoList->ReturnActionByIndexAdv(cntLoop, miscTemp1, miscTemp2, miscTemp3, miscTemp4, m_skillDatabase);

		if (controlAction != "") {
			CStringA keyAssigned = cObj->m_controlInfoList->GetKeyAssignmentByActionString(controlAction);
			keyAssigned += ": " + controlAction;
			pConfig.push_back(keyAssigned.GetString()); // string to display
			pConfig.push_back(std::to_string(cntLoop)); // id of this control key
		}
		cntLoop++;
	}

	return true;
}

bool ClientEngine::PendingControlAssignment(int index) {
	m_pendingControlSystem->SetIndex(index);
	return true;
}

// DRF - Keyboard/mouse control action mapping is stored in ...\star\####\GameFiles\defaultbak.CNT
// This file is loaded while processing ...\star\####\GameFiles\ClientAutoexec.xml
void ClientEngine::LoadControlCfgList(const std::string& fileName) {
	// use gamefiles folder for organization
	ScopedDirectoryChanger sdc("GameFiles\\");

	// Modification by Jonny 2/12/06 sometimes for some strange reason the loading of a custom user
	// control fails on serilization and it defaults to loading no controllist at all.  I will backup
	// the default control list and restore it if the loading of the custom one fails.
	ControlDBList* tempControlList = m_controlsList; // backup the default control list
	m_controlsList = NULL;

	if (FileHelper::Exists(fileName)) {
		// end use gamefiles folder for organization
		BEGIN_CFILE_SERIALIZE(fl, fileName, false)
		CArchive the_in_Archive(fl, CArchive::load);
		the_in_Archive >> m_controlsList; // load in the new control list
		the_in_Archive.Close();
		END_CFILE_SERIALIZE(fl, m_logger, "LoadControlCfgList")
		if (serializeOk) {
			ControlDBList* saveList = m_controlsList; // save the good list
			m_controlsList = tempControlList;
			DestroyControlList(); // Call the destructor on the old list
			m_controlsList = saveList; // Restore the good one
		} else {
			DestroyControlList(); // We failed to load the control list clean it up
			m_controlsList = tempControlList; // Restore our previous control list
		}
		return;
	} else {
		// if custom controls don't exist restore default controls -Jonny
		m_controlsList = tempControlList;
	}
}

void ClientEngine::DestroyControlList() {
	if (!m_controlsList)
		return;

	for (POSITION posLoc = m_controlsList->GetHeadPosition(); posLoc != NULL;) {
		// control loop
		ControlDBObj* conPtr = (ControlDBObj*)m_controlsList->GetNext(posLoc);
		for (POSITION posLoc2 = conPtr->m_controlInfoList->GetHeadPosition(); posLoc2 != NULL;) {
			// info loop
			ControlInfoObj* infoPtr = (ControlInfoObj*)conPtr->m_controlInfoList->GetNext(posLoc2);
			delete infoPtr;
			infoPtr = 0;
		}

		conPtr->m_controlInfoList->RemoveAll();
		delete conPtr->m_controlInfoList;
		conPtr->m_controlInfoList = 0;
		delete conPtr;
		conPtr = 0;
	}

	m_controlsList->RemoveAll();
	delete m_controlsList;
	m_controlsList = 0;
}

bool ClientEngine::UnassignControlByIndex(int index) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	if (!m_controlsList)
		return false;

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	POSITION posLoc = m_controlsList->FindIndex(pMO->getControlConfig());
	if (!posLoc)
		return true;

	ControlDBObj* conPtr = (ControlDBObj*)m_controlsList->GetNext(posLoc);
	CStringA actionString;
	int inventoryGlid = -1;
	int miscInt2 = 0;
	int miscInt3 = 0;
	int miscInt4 = 0;

	actionString = conPtr->m_controlInfoList->ReturnActionByIndexAdv(
		index,
		inventoryGlid,
		miscInt2,
		miscInt3,
		miscInt4,
		pICE->GetSkillDatabase());

	conPtr->m_controlInfoList->UnassignByActionName(actionString);

	return true;
}

bool ClientEngine::RestoreDefaultControlConfiguration() {
	const std::string fileName = "DEFAULTBAK.CNT";
	ScopedDirectoryChanger sdc("GameFiles\\");
	BEGIN_CFILE_SERIALIZE(fl, fileName, false)
	DestroyControlList();
	CArchive the_in_Archive(fl, CArchive::load);
	the_in_Archive >> m_controlsList;
	the_in_Archive.Close();
	END_CFILE_SERIALIZE(fl, m_logger, "RestoreDefaultControlConfiguration")
	return true;
}

bool ClientEngine::SaveControlConfiguration() {
	const std::string fileName = "Default.cnt";
	ScopedDirectoryChanger sdc("GameFiles\\");
	BEGIN_CFILE_SERIALIZE(fl, fileName, true)
	CArchive the_out_Archive(fl, CArchive::store);
	the_out_Archive << m_controlsList;
	the_out_Archive.Close();
	END_CFILE_SERIALIZE(fl, m_logger, "SaveControlConfiguration")
	return true;
}

bool ClientEngine::IsKeyDown(size_t keyCode) const {
#ifdef MAPVK_VK_TO_VSC_EX
	auto scanCode = ::MapVirtualKey(keyCode, MAPVK_VK_TO_VSC_EX); // if WINVER >= 0x0600
#else
	unsigned scanCode = 0;
	switch (keyCode) {
		case VK_LSHIFT:
			scanCode = DIK_LSHIFT;
			break; // Workarounds for left/right-handed keys
		case VK_RSHIFT:
			scanCode = DIK_RSHIFT;
			break;
		case VK_LCONTROL:
			scanCode = DIK_LCONTROL;
			break;
		case VK_RCONTROL:
			scanCode = DIK_RCONTROL;
			break;
		default:
			scanCode = ::MapVirtualKey(keyCode, MAPVK_VK_TO_VSC);
	}
#endif
	return m_keyboardState.KeyDown(scanCode);
}

} // namespace KEP