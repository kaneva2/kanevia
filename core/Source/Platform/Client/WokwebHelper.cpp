///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "WokwebHelper.h"

#include "common\KEPUtil\Helpers.h"

// DRF - TODO - Remove ASAP - Old Parse gamelogin.aspx result (tab delimited list)
static bool OldParseGameLoginResult(
	const std::string& resultText,
	GL_RESULT& glResult,
	std::string& resultDescription,
	LONG& userId,
	std::string& username,
	std::string& serverip,
	LONG& serverport,
	LONG& zoneindex,
	std::string& gender,
	LONG& gameId,
	std::string& gameName,
	bool& incubatorHosted,
	LONG& parentGameId,
	LONG& errorCode,
	std::string& templatePatchDir,
	std::string& appPatchDir) {
	bool done = false;

	std::string::size_type ii = 0;

	std::locale loc("");

	for (int i = 0; ii != std::string::npos; i++) {
		std::string::size_type prev = ii;

		ii = resultText.find("\t", ii);
		std::string token;
		if (ii == std::string::npos)
			token = resultText.substr(prev);
		else {
			token = resultText.substr(prev, ii - prev);
			ii++;
		}

		switch (i) {
			case 0: // Result code (see enum in wokwebhelper.h)
				if (token.empty() || !isdigit(token.at(0), loc))
					return false;
				glResult = (GL_RESULT)atol(token.c_str());
				break;

			case 1: // Result description
				resultDescription.assign(token);
				break;

			case 2: // User ID
				if (token.empty() || !isdigit(token.at(0), loc))
					return false;
				userId = atol(token.c_str());
				break;

			case 3: // User name
				username.assign(token);
				break;

			case 4: // Game server host name / ip address
				serverip.assign(token);
				break;

			case 5: // Game server port
				if (token.empty() || !isdigit(token.at(0), loc))
					return false;
				serverport = atol(token.c_str());
				break;

			case 6: // Zone index
				if (token.empty() || !isdigit(token.at(0), loc))
					return false;
				zoneindex = atol(token.c_str());
				break;

			case 7: // Gender
				gender.assign(token);
				done = true;
				break;

			case 8: // Game ID
				if (token.empty() || !isdigit(token.at(0), loc))
					return false;
				gameId = atol(token.c_str());
				break;

			case 9: // Game name
				gameName.assign(token);
				break;

			case 10: // parent game ID
				if (token.empty() || !isdigit(token.at(0), loc))
					return false;
				parentGameId = atol(token.c_str());
				break;

			case 11: // True if it's a 3dapp and hosted by incubator
				if (token.empty() || !isdigit(token.at(0), loc))
					return false;
				incubatorHosted = (token == "1");
				break;

			case 14: // Detail error code
				if (token.empty() || !isdigit(token.at(0), loc))
					return false;
				errorCode = atol(token.c_str());
				break;

			case 15: // Detail error code
				templatePatchDir = token;
				break;

			case 16: // Detail error code
				appPatchDir = token;
				break;
		}
	}

	return done;
}

#include "TinyXML/tinyxml.h"

#define ToLong(a) (a ? atol(a) : -1)
#define ToBool(a) (a ? StrSame(a, "1") : false)
#define ToString(a) (a ? a : "")

#define XmlText(root, tag) (root ? root->FirstChildElement(tag)->GetText() : "")
#define XmlString(root, tag) ToString(XmlText(root, tag))
#define XmlLong(root, tag) ToLong(XmlText(root, tag))
#define XmlBool(root, tag) ToBool(XmlText(root, tag))

// Parse gamelogin.aspx result (tab delimited list)
bool ParseGameLoginResult(
	const std::string& resultText,
	GL_RESULT& glResult,
	std::string& resultDescription,
	LONG& userId,
	std::string& username,
	std::string& serverip,
	LONG& serverport,
	std::string& serverVersion, // drf - added
	LONG& zoneindex,
	std::string& gender,
	LONG& gameId,
	std::string& gameName,
	bool& incubatorHosted,
	LONG& parentGameId,
	LONG& errorCode,
	std::string& templatePatchDir,
	std::string& appPatchDir) {
	bool ok;
	std::string xml = resultText;

	LogInstance("Instance");
	LogInfo(xml);

	// Parse Result Text XML
	TiXmlDocument doc;
	doc.Parse(xml.c_str());
	if (doc.Error()) {
		LogError("xml.Parse() FAILED\n"
				 << xml);
		goto ParserFailed;
	}

	auto root = doc.FirstChildElement("Result");
	if (!root) {
		LogError("xml.FirstChildElement(Result) FAILED" << xml);
		goto ParserFailed;
	}

	// Extract Xml Login Record Tags
	glResult = (GL_RESULT)XmlLong(root, "ResultCode");
	resultDescription = XmlString(root, "ResultDescription");
	userId = XmlLong(root, "UserId");
	username = XmlString(root, "UserName");
	serverip = XmlString(root, "Server");
	serverport = XmlLong(root, "Port");
	serverVersion = XmlString(root, "ServerVersion");
	zoneindex = XmlLong(root, "ZoneIndex");
	gender = XmlString(root, "Gender");
	gameId = XmlLong(root, "GameId");
	gameName = XmlString(root, "GameName");
	incubatorHosted = XmlBool(root, "IsIncubatorHosted");
	parentGameId = XmlLong(root, "ParentGameId");
	errorCode = XmlLong(root, "ErrorCode");
	templatePatchDir = XmlString(root, "TemplateClientPath");
	appPatchDir = XmlString(root, "WorldClientPath");

	// Login Record Ok To Try Login If We Have Valid Login Record
	ok = !gender.empty();

	return ok;

ParserFailed:

	// DRF - TODO - Remove ASAP - Try Old Parser
	ok = OldParseGameLoginResult(
		resultText,
		glResult,
		resultDescription,
		userId,
		username,
		serverip,
		serverport,
		zoneindex,
		gender,
		gameId,
		gameName,
		incubatorHosted,
		parentGameId,
		errorCode,
		templatePatchDir,
		appPatchDir);
	if (!ok) {
		LogError("OldParseGameLoginResult() FAILED\n");
	}

	return ok;
}
