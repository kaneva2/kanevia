///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

class ClientEngine_CWinApp : public CWinApp {
public:
	ClientEngine_CWinApp() :
			m_pCE_CFrameWnd(nullptr) {
		m_pMainWnd = nullptr;
	}

	BOOL OnIdle(LONG lCount) {
		return TRUE;
	}

	/** DRF - Overrides CWinApp::InitInstance()
	* Initializes the application. Shows the main client window and calls CWinApp::InitInstance().
	*/
	virtual BOOL InitInstance() override;

	/** DRF - Overrides CWinApp::Run()
	* Runs the application. Sits in a loop calling RenderLoop() to render a single frame of the client
	* window and services the windows message loop between frames.  Returns a call to CWinApp::Run()
	* when the message loop indicates the app has been terminated.
	*/
	virtual int Run() override;

	/** DRF - Overrides CWinApp::ProcessWndProcException()
	* This is where we land with some MFC exceptions. If we needed to show a message or something, we
	* could do that here. However, in most cases we just want to cause MFC to throw the exception out
	* to CrashRpt.
	*/
	virtual LRESULT ProcessWndProcException(CException* e, const MSG* pMsg) override {
		THROW_LAST();
	}

protected:
	DECLARE_MESSAGE_MAP()

private:
	friend class ClientEngine_CFrameWnd;
	ClientEngine_CFrameWnd* m_pCE_CFrameWnd;
};

} // namespace KEP