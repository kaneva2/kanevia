///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Math/Matrix.h"
#include "Core/Util/EnumBitFlags.h"
#include "Core/Util/HighPrecisionTime.h"

struct IDirect3DSurface9;

namespace KEP {

enum class WorldRenderStyle { // world map visual style
	FULL_TEXTURE = 0x00, // Object rendered in original textures
	OBJECT_COLOR = 0x01, // Object rendered with single pixel textures
	EDGE_TRACING = 0x02, // OBJECT_COLOR + edge tracing effect
	EDGE_WHITE_ON_BLACK = 0x03, // OBJECT_COLOR + edge tracing effect + gray scale + black threshold
	EDGE_BLACK_ON_WHITE = 0x04, // OBJECT_COLOR + edge tracing effect + gray scale + black threshold + invert
};

enum class WorldRenderOptions { // world map mapping options
	EXG_MESHES = 1,
	STATIONARY_OBJECTS = 2,
	MOVING_OBJECTS = 4,
};

//ENABLE_SCOPED_ENUM_BIT_FLAGS(WorldRenderOptions); // ENABLE_SCOPED_ENUM_BIT_FLAGS somehow is not working with enum types inside a namespace
DEFINE_ENUM_FLAG_OPERATORS(WorldRenderOptions);

struct OffscreenRenderTask {
	IDirect3DSurface9* m_target;
	IDirect3DSurface9* m_defaultPoolTarget;
	WorldRenderStyle m_style;
	WorldRenderOptions m_options;
	double m_minRadiusFilter;
	bool m_overrideMatrices;
	Matrix44f m_camMatrix;
	Matrix44f m_projMatrix;
	unsigned m_background;

	bool m_completed;
	int m_retriesLeft;
	TimeMs m_startTime;

	OffscreenRenderTask(IDirect3DSurface9* target, WorldRenderStyle style, WorldRenderOptions options, double minRadiusFilter, bool overrideMatrix, const Matrix44f& camMatrix, const Matrix44f& projMatrix) :
			m_target(target),
			m_defaultPoolTarget(nullptr),
			m_style(style),
			m_options(options),
			m_minRadiusFilter(minRadiusFilter),
			m_overrideMatrices(overrideMatrix),
			m_camMatrix(camMatrix),
			m_projMatrix(projMatrix),
			m_background(0xff000000),
			m_completed(false),
			m_retriesLeft(5),
			m_startTime(0) {
	}
};

} // namespace KEP
