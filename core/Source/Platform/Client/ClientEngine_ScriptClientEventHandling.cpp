///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "SkeletonLabels.h"
#include "RuntimeSkeleton.h"
#include "CDayStateClass.h"
#include "CPhysicsEmuClass.h"
#include "CTimeSystem.h"
#include "SkeletonConfiguration.h"
#include "dynamicObPlacementModelProxy.h"
#include "ActorDatabase.h"
#include "common/include/UgcConstants.h"
#include "LogHelper.h"
#include "KEPPhysics/CollisionShape.h"
#include "KEPPhysics/PhysicsFactory.h"
#include "KEPPhysics/RigidBodyStatic.h"
#include "KEPPhysics/Universe.h"
#include "KEPPhysics/Vehicle.h"
#include "Core/Math/TransformUtil.h"
#include "PhysicsUserData.h"
#include "Event/Events/SCE_ClientHandler.h"
#include "PrimitiveGeomArgs.h"
#include "CallEventHandlerWithArgs.h"
#include <Common/include/PreciseTime.h>
#include <boost\optional.hpp>

using namespace std;

namespace KEP {

static LogInstance("ClientEngine");
static std::map<ScriptClientEvent::EventType, std::function<void(ScriptClientEvent* sce)>> s_SCEHandlerMap;
static ClientEngine* s_pClientEngine = nullptr; // For use by SCE_ClientHandler Handler functions.

// Initialize SCE_ClientHandler objects that have a specialization.
template <ScriptClientEvent::EventType eventType>
struct InitSCEHandler {
	static void Call() {
		s_SCEHandlerMap[eventType] = [](ScriptClientEvent* sce) {
			CallEventHandlerWithArgs<ScriptClientEvent>(sce, &SCE_ClientHandler<eventType>::Handle);
		};
	}
};

bool ClientEngine::SCE_Handler(ScriptClientEvent* sce) {
	static bool s_SCEHandlersInitialized = false;
	if (!s_SCEHandlersInitialized) {
		s_pClientEngine = this;
		ForEachSCE_ClientHandler<InitSCEHandler>();
		s_SCEHandlersInitialized = true;
	}

	// Valid Script Client Event?
	auto eventType = sce->GetEventType();
	if (eventType >= ScriptClientEvent::NumEventTypes) {
		LogError("FAILED - Invalid ScriptClientEvent::EventType " << (UINT)eventType);
		return false;
	}

	auto itr = s_SCEHandlerMap.find(eventType);
	if (itr != s_SCEHandlerMap.end()) {
		itr->second(sce);
		return true;
	}

	// Call Script Client Event Handler
	switch (eventType) {
		case ScriptClientEvent::ScriptError:
			SCE_ScriptError(sce);
			break;
		case ScriptClientEvent::PlayerTeleport:
			SCE_PlayerTeleport(sce);
			break;
		case ScriptClientEvent::PlayerTell:
			SCE_PlayerTell(sce);
			break;
		case ScriptClientEvent::PlayerSetEquippedItemAnimation:
			SCE_PlayerSetEquippedItemAnimation(sce);
			break;
		case ScriptClientEvent::PlayerSetAnimation:
			SCE_PlayerSetAnimation(sce);
			break;
		case ScriptClientEvent::PlayerDefineAnimation:
			SCE_PlayerDefineAnimation(sce);
			break;
		case ScriptClientEvent::PlayerSetPhysics:
			SCE_PlayerSetPhysics(sce);
			break;
		case ScriptClientEvent::PlayerScale:
			SCE_PlayerScale(sce);
			break;
		case ScriptClientEvent::PlayerMove:
			SCE_PlayerMove(sce);
			break;
		case ScriptClientEvent::PlayerSetName:
			SCE_PlayerSetName(sce);
			break;
		case ScriptClientEvent::PlayerCastRay:
			SCE_PlayerCastRay(sce);
			break;
		case ScriptClientEvent::SoundAttachTo:
			SCE_SoundAttachTo(sce);
			break;
		case ScriptClientEvent::SoundAddModifier:
			SCE_SoundAddModifier(sce);
			break;
		case ScriptClientEvent::PlayerSetEffectTrack:
			SCE_PlayerSetEffectTrack(sce);
			break;
		case ScriptClientEvent::ObjectSetEffectTrack:
			SCE_ObjectSetEffectTrack(sce);
			break;
		case ScriptClientEvent::SoundPlayOnClient:
			SCE_SoundPlayOnClient(sce);
			break;
		case ScriptClientEvent::SoundStopOnClient:
			SCE_SoundStopOnClient(sce);
			break;
		case ScriptClientEvent::ScriptSetEnvironment:
			SCE_ScriptSetEnvironment(sce);
			break;
		case ScriptClientEvent::ScriptSetDayState:
			SCE_ScriptSetDayState(sce);
			break;
		case ScriptClientEvent::PlayerLookAt:
			SCE_PlayerLookAt(sce);
			break;
		case ScriptClientEvent::ObjectLookAt:
			SCE_ObjectLookAt(sce);
			break;
		case ScriptClientEvent::ObjectAddLabels:
			SCE_ObjectAddLabels(sce);
			break;
		case ScriptClientEvent::ObjectSetOutline:
			SCE_ObjectSetOutline(sce);
			break;
		case ScriptClientEvent::ObjectClearLabels:
			SCE_ObjectClearLabels(sce);
			break;
		case ScriptClientEvent::RenderLabelsOnTop:
			SCE_RenderLabelsOnTop(sce);
			break;
		case ScriptClientEvent::DownloadAsset:
			SCE_DownloadAsset(sce);
			break;
		case ScriptClientEvent::PublishAssetCompleted:
			SCE_PublishAssetCompleted(sce);
			break;
		case ScriptClientEvent::PublishResponseEvent:
			SCE_PublishResponseEvent(sce);
			break;
		case ScriptClientEvent::PublishDeleteAssetCompleted:
			SCE_PublishDeleteAssetCompleted(sce);
			break;
		case ScriptClientEvent::GetAllScripts:
			SCE_GetAllScripts(sce);
			break;
		case ScriptClientEvent::ObjectSetCollision:
			SCE_ObjectSetCollision(sce);
			break;
		case ScriptClientEvent::ObjectSetMouseOver:
			SCE_ObjectSetMouseOver(sce);
			break;
		case ScriptClientEvent::PlayerAddIndicator:
			SCE_PlayerAddIndicator(sce);
			break;
		case ScriptClientEvent::PlayerClearIndicators:
			SCE_PlayerClearIndicators(sce);
			break;
		case ScriptClientEvent::PlayerUpdateCoinHUD:
			SCE_PlayerUpdateCoinHUD(sce);
			break;
		case ScriptClientEvent::PlayerUpdateHealthHUD:
			SCE_PlayerUpdateHealthHUD(sce);
			break;
		case ScriptClientEvent::PlayerUpdateXPHUD:
			SCE_PlayerUpdateXPHUD(sce);
			break;
		case ScriptClientEvent::PlayerAddHealthIndicator:
			SCE_PlayerAddHealthIndicator(sce);
			break;
		case ScriptClientEvent::PlayerRemoveHealthIndicator:
			SCE_PlayerRemoveHealthIndicator(sce);
			break;
		case ScriptClientEvent::PlayerOpenShop:
			SCE_PlayerOpenShop(sce);
			break;
		case ScriptClientEvent::PlayerAddItemToShop:
			SCE_PlayerAddItemToShop(sce);
			break;
		case ScriptClientEvent::PlayerShowStatusInfo:
			SCE_PlayerShowStatusInfo(sce);
			break;
		case ScriptClientEvent::PlayerClosePresetMenu:
			SCE_PlayerClosePresetMenu(sce);
			break;
		case ScriptClientEvent::PlayerMovePresetMenu:
			SCE_PlayerMovePresetMenu(sce);
			break;
		case ScriptClientEvent::PlayerOpenListBoxMenu:
			SCE_PlayerOpenListBoxMenu(sce);
			break;
		case ScriptClientEvent::PlayerOpenYesNoMenu:
			SCE_PlayerOpenYesNoMenu(sce);
			break;
		case ScriptClientEvent::PlayerOpenKeyListenerMenu:
			SCE_PlayerOpenKeyListenerMenu(sce);
			break;
		case ScriptClientEvent::PlayerOpenTextTimerMenu:
			SCE_PlayerOpenTextTimerMenu(sce);
			break;
		case ScriptClientEvent::PlayerOpenButtonMenu:
			SCE_PlayerOpenButtonMenu(sce);
			break;
		case ScriptClientEvent::PlayerOpenStatusMenu:
			SCE_PlayerOpenStatusMenu(sce);
			break;
		case ScriptClientEvent::PlayerSetCustomTextureOnAccessory:
			SCE_PlayerSetCustomTextureOnAccessory(sce);
			break;
		case ScriptClientEvent::ObjectControl:
			SCE_PlayerIndividualObjectControl(sce);
			break;
		case ScriptClientEvent::ObjectSetDrawDistance:
			SCE_ObjectSetDrawDistance(sce);
			break;
		case ScriptClientEvent::PlayerResetCursorModesTypes:
			SCE_PlayerResetCursorModesTypes(sce);
			break;
		case ScriptClientEvent::PlayerSetCursorModeType:
			SCE_PlayerSetCursorModeType(sce);
			break;
		case ScriptClientEvent::PlayerSetCursorMode:
			SCE_PlayerSetCursorMode(sce);
			break;
		case ScriptClientEvent::PlayerSpawnVehicle:
			SCE_PlayerSpawnVehicle(sce);
			break;
		case ScriptClientEvent::PlayerLeaveVehicle:
			SCE_PlayerLeaveVehicle(sce);
			break;
		case ScriptClientEvent::PlayerSetVisibilityFlag:
			SCE_PlayerSetVisibility(sce);
			break;
	}

	return true;
}

void ClientEngine::SCE_ScriptError(ScriptClientEvent* sce) {
	string msg;
	int code;
	(*sce->InBuffer()) >> code >> msg;
	SendChatMessage(eChatType::External, msg);
	LogError("[" << code << "]" << msg);
}

void ClientEngine::SCE_PlayerTeleport(ScriptClientEvent* sce) {
	double x, y, z, rx, ry, rz;
	*sce->InBuffer() >> x >> y >> z >> rx >> ry >> rz;
	SetMyPosition(x, y, z);
	SetMyOrientation(rx, ry, rz);
}

void ClientEngine::SCE_PlayerTell(ScriptClientEvent* sce) {
	string msg, title;
	int placementId, isTell;
	ULONG clientId = 0;
	(*sce->InBuffer()) >> msg >> title >> placementId >> isTell >> clientId;
	msg = title + TELL_TEXT + msg;
	if (!isTell)
		return;
	IEvent* e = new RenderTextEvent(msg.c_str(), eChatType::Private);
	if (e)
		m_dispatcher.QueueEvent(e);
}

// DRF - TODO - Change glids to AnimSettings
void ClientEngine::SCE_PlayerSetEquippedItemAnimation(ScriptClientEvent* sce) {
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;

	ULONG clientId;
	GLID glidAnim;
	GLID glidItem;
	(*sce->InBuffer()) >> clientId >> glidItem >> glidAnim;

	// I want to update the server, too.  This is just as if
	// someone had pressed a button on the GUI, but it's coming
	// from the script server instead.
	SetEquippableAnimation(pMO, glidItem, glidAnim, true);
}

void ClientEngine::SCE_PlayerSetAnimation(ScriptClientEvent* sce) {
	ULONG clientId;
	GLID animGlidSpecial; // < 0 = reverse animation
	(*sce->InBuffer()) >> clientId >> animGlidSpecial;

	AnimSettings animSettings;
	animSettings.setGlidSpecial(animGlidSpecial);
	setMyOverrideAnimationSettings(animSettings);
}

void ClientEngine::SCE_PlayerDefineAnimation(ScriptClientEvent* sce) {
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;

	auto pRS = pMO->getSkeleton();
	if (!pRS)
		return;

	ULONG clientId;
	(*sce->InBuffer()) >> clientId;

	for (size_t i = 0; i < c_numLocomotionAnimTypes; i++) {
		// Unpack Event Animation Glid
		GLID glid;
		(*sce->InBuffer()) >> glid;
		if (!IS_VALID_GLID(glid))
			continue;

		// Cache UGC Animation Content Metadata If Not Already Cached
		if (IS_UGC_GLID(glid)) {
			if (!ContentService::Instance()->ContentMetadataIsCached(glid)) {
				LogWarn("ContentMetadataIsCached() FAILED - glid<" << glid << "> - Caching...");
				ContentService::Instance()->ContentMetadataCacheAsync(glid, NULL);
			}
		}

		GLID glidOld = pRS->getAnimationGlidByAnimTypeAndVer(c_locomotionAnimTypes[i]);
		if (glidOld != glid) {
			if (pMO->getUgcActor()) {
				GLID ugcActorGlid = pMO->getUGCActorGlobalId();
				string animName;
				if (i == 0)
					animName = "walk";
				else if (i == 1)
					animName = "idle";
				else if (i == 2)
					animName = "jump";
				else if (i == 3)
					animName = "run";
				m_ugcActorDB->AddSupportedAnimation(ugcActorGlid, glid, animName);
			} else {
				//
				// Abandon hope all ye who enter here.
				//
				// This code attempts to re-set the locomotion index (also called Attrib, also called CSkeletonAnimHeader::m_type in ancient scrolls
				// found behind a toilet cistern at a gas station near Oak Hill, GA) for the original animation to 0 (which is not a valid locomotion
				// type) and then add (if necessary) the new animation and set it's locomotion type to whatever it needs to be to replace the old one.
				//
				// For a non-ugc avatar, the correct animation is selected using the appropriate attrib/locomotion value during
				// RuntimeSkeleton::SetAnimationBasedOnAttrib().
				//
				int animVer = 0;
				pRS->remapSkeletonAnimHeaderByGlid(glidOld, eAnimType::None, animVer, false);
				animVer = 0;
				pRS->remapSkeletonAnimHeaderByGlid(glid, c_locomotionAnimTypes[i], animVer, false);
			}
		}
	}
}

void ClientEngine::SCE_PlayerSetPhysics(ScriptClientEvent* sce) {
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;

	int zoneId = 0;
	int size = 0;
	ULONG key = 0;
	int value = 0;
	float fvalue = 0.0;
	gsValues* gsVal = 0;
	gsMetaData* gsMD = 0;

	auto pMC = pMO->getMovementCaps();
	if (!pMC)
		return;

	pMC->GetValues(gsVal, gsMD);

	*sce->InBuffer() >> zoneId >> size;

	for (int i = 0; i < (size / 2); i++) {
		*sce->InBuffer() >> key >> value;
		if (key >= gsVal->size()) //make sure our index is in range.
			continue;

		if (key != MOVEMENTCAPSIDS_CURROTATIONVECT &&
			key != MOVEMENTCAPSIDS_CURVELOCITYVECT &&
			key != MOVEMENTCAPSIDS_LASTPOSITION) {
			switch (gsMD->at(key).GetType()) {
				case gsBool:
					pMC->SetNumber(key, value != 0);
					break;

				case gsInt:
				case gsBOOL:
					pMC->SetNumber(key, value);
					break;

				case gsFloat:
					fvalue = (value / 1000.0f);
					pMC->SetNumber(key, fvalue);
					break;

				case gsDouble:
					pMC->SetNumber(key, double(value / 1000.0));
					break;

				default:
					LogError("Invalid GetSet Type " << gsMD->at(key).GetType());
					break;
			}
		}
	}
}

void ClientEngine::SCE_PlayerScale(ScriptClientEvent* sce) {
	// This could be for any player object, so find the correct one and apply the scale.
	double x, y, z;
	ULONG netId;
	*sce->InBuffer() >> netId >> x >> y >> z;
	LogInfo("netId=" << netId << " scale=(" << x << " " << y << " " << z << ")");

	auto pMO = GetMovementObjByNetId(netId);
	if (!pMO || !pMO->getSkeleton())
		return;

	pMO->getSkeleton()->scaleSystem(x, y, z);
	CCameraObj* pCO = GetActiveCameraForMovementObject(pMO);
	if (!pCO) {
		LogError("GetActiveCameraForActor() FAILED");
		return;
	}
	pCO->m_cameraScale.x = x;
	pCO->m_cameraScale.y = y;
	pCO->m_cameraScale.z = z;
}

void ClientEngine::SCE_PlayerMove(ScriptClientEvent* sce) {
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;

	double x, y, z;
	double rx, ry, rz;
	double speed;
	int anim;
	int timeout;
	int recalcInterval;
	int aiMode;
	bool noPlayerControl, noPhysics;
	*sce->InBuffer() >> x >> y >> z >> rx >> ry >> rz >> speed >> anim >> timeout >> recalcInterval >> aiMode >> noPlayerControl >> noPhysics;
	LogInfo("(x=" << x << " y=" << y << " z=" << z << ")");
	if (!AttachPathFinder(ObjectType::MOVEMENT, pMO->getNetTraceId(), x, y, z, rx, ry, rz, speed, anim, timeout, recalcInterval, aiMode, noPhysics, noPlayerControl))
		LogError("AttachPathFinder() FAILED - networkId=" << pMO->getNetTraceId());
}

void ClientEngine::SCE_PlayerSetName(ScriptClientEvent* sce) {
	string name;
	int netId;
	*sce->InBuffer() >> name >> netId;

	auto pMO = GetMovementObjByNetId(netId);
	if (!pMO) {
		LogError("GetMovementObjByNetId() FAILED - netId=" << netId);
		return;
	}

	pMO->setDisplayName(name);
	setOverheadLabels(pMO);
}

void ClientEngine::SCE_PlayerCastRay(ScriptClientEvent* sce) {
	int placementId;
	ULONG zoneId;
	*sce->InBuffer() >> zoneId >> placementId;
	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerRayIntersects, placementId, zoneId, 0, 0);

	TraceInfo trace;
	Vector3f rayOrigin, rayDirection;

	auto pMO = GetMovementObjMe();
	if (pMO) {
		pMO->getSkeleton()->getAnimatedWorldPosition(rayOrigin);
		// This is also the position of the midpoint ray used for collision testing.
		rayOrigin.y += pMO->getSkeleton()->getBonesBoundingBox().max.Y() * 0.5f;
		rayDirection = -pMO->getSkeleton()->getWorldMatrix().GetRowZ();
		RayTrace(&trace, rayOrigin, rayDirection, FLT_MAX, ObjectType::ANY, RAYTRACE_VISIBLE);
	} else {
		// provide null output rather than complete garbage
		trace.objId = 0;
		trace.objType = ObjectType::NONE;
		trace.distance = 0;
	}
	*sse->OutBuffer() << trace.objId << (int)trace.objType << trace.distance;
	sse->AddTo(SERVER_NETID);
	m_dispatcher.QueueEvent(sse);
}

void ClientEngine::SCE_SoundAttachTo(ScriptClientEvent* sce) {
	int soundPlacementId = -1;
	ObjectType attachType = ObjectType::NONE;
	ULONG targetId;
	*sce->InBuffer() >> soundPlacementId >> targetId >> reinterpret_cast<int&>(attachType);

	// Get Sound Placement
	auto pSP = SoundPlacementGet(soundPlacementId);
	if (!pSP) {
		LogError("SoundPlacementGet() FAILED - soundPlacementId=" << soundPlacementId);
		return;
	}

	// Attach Sound Placement To Target (and follow)
	switch (attachType) {
		case ObjectType::MOVEMENT:
			pSP->AttachToPlayer(targetId, true);
			break;

		case ObjectType::DYNAMIC:
			pSP->AttachToDynObj(targetId, true);
			break;

		default:
			LogError("Unknown Attach Type - attachType=" << attachType);
			break;
	}
}

void ClientEngine::SCE_SoundAddModifier(ScriptClientEvent* sce) {
	// Decode Event
	ZoneIndex zoneId;
	int soundPlacementId;
	int input;
	int output;
	double gain;
	double offset;
	ScriptClientEvent::DecodeSoundAddModifier(sce, zoneId, soundPlacementId, input, output, gain, offset);
	LogInfo("soundPlacementId=" << soundPlacementId << " input=" << input << " output=" << output << " gain=" << gain << " offset=" << offset);

	// Add Modifier To Sound Placement
	SoundPlacement::Modifier mod(input, output, gain, offset);
	if (!SoundPlacementAddModifier(soundPlacementId, mod)) {
		LogWarn("SoundPlacementAddModifier() FAILED - soundPlacementId=" << soundPlacementId);
		return;
	}
}

void ClientEngine::SCE_PlayerSetEffectTrack(ScriptClientEvent* sce) {
	int zoneId, netId, numberOfEffects, numberOfStrings;
	*sce->InBuffer() >> zoneId >> netId >> numberOfEffects >> numberOfStrings;

	auto pMO = GetMovementObjByNetId(netId);
	if (!pMO) {
		LogError("GetMovementObjByNetId() FAILED - netId=" << netId);
		return;
	}

	ProcessEffectTrack(pMO->getEffectTrack(), pMO, sce, netId, numberOfEffects, numberOfStrings);
}

void ClientEngine::SCE_ObjectSetEffectTrack(ScriptClientEvent* sce) {
	int zoneId, placementId, numberOfEffects, numberOfStrings;
	*sce->InBuffer() >> zoneId >> placementId >> numberOfEffects >> numberOfStrings;

	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return;
	}

	std::unique_ptr<EffectTrack> pEffectTrack = std::make_unique<EffectTrack>(ObjectType::DYNAMIC);
	ProcessEffectTrack(*pEffectTrack, pDPO, sce, placementId, numberOfEffects, numberOfStrings);
	pDPO->SetEffectTrack(std::move(pEffectTrack));
}

void ClientEngine::SCE_SoundPlayOnClient(ScriptClientEvent* sce) {
	int globalId;
	*sce->InBuffer() >> globalId;
	PlaySoundOnPlayer(0, (GLID)globalId); // 0=me
}

void ClientEngine::SCE_SoundStopOnClient(ScriptClientEvent* sce) {
	int globalId;
	*sce->InBuffer() >> globalId;

	if (!RemoveSoundOnPlayer(0, (GLID)globalId)) { // 0=me
		LogError("RemoveSoundOnPlayer() FAILED - glid<" << globalId << ">");
	}
}

void ClientEngine::SCE_ScriptSetEnvironment(ScriptClientEvent* sce) {
	if (!m_environment)
		return;

	int zi = 0;
	int size = 0;
	double temp = 0;
	ULONG key = 0;
	gsValues* gsVal;
	gsMetaData* gsMD;

	m_environment->GetValues(gsVal, gsMD);

	if (!m_environment->m_environmentTimeSystem) {
		m_environment->m_environmentTimeSystem = new CTimeSystemObj();
		m_environment->m_environmentTimeSystem->m_pDSO = new CDayStateObj(m_environment);
	}

	*sce->InBuffer() >> zi >> size;
	int count = 0;
	while (count < size) {
		*sce->InBuffer() >> temp;
		count++;
		key = (ULONG)temp;
		if (key >= gsVal->size()) //make sure our index is in range.
			continue;

		switch (key) {
			case ENVIRONMENTIDS_SUNPOSITION: {
				Vector3f d3dvec;
				*sce->InBuffer() >> d3dvec.x >> d3dvec.y >> d3dvec.z;
				count += 3;
				m_environment->SetVector3f(key, d3dvec);
			} break;

			case ENVIRONMENTIDS_AMBIENTLIGHTREDDAY: //GetSet RGBA
			case ENVIRONMENTIDS_SUNRED:
			case ENVIRONMENTIDS_FOGRED:
			case ENVIRONMENTIDS_BACKRED: {
				double r, g, b;
				*sce->InBuffer() >> r >> g >> b;
				count += 3;
				m_environment->SetColor(key, (float)r, (float)g, (float)b);
			} break;

			case ENVIRONMENTIDS_AMBIENTLIGHTON: //GetSet Bool
			case ENVIRONMENTIDS_SUNLIGHTON:
			case ENVIRONMENTIDS_FOGON:
			case ENVIRONMENTIDS_LOCKBACKCOLOR:
			case ENVIRONMENTIDS_ENABLETIMESYSTEM: {
				double value;
				*sce->InBuffer() >> value;
				count++;
				m_environment->SetNumber(key, (value != 0) ? true : false);
			} break;

			case ENVIRONMENTIDS_FOGSTART: //GetSet Float
			case ENVIRONMENTIDS_FOGRANGE:
			case ENVIRONMENTIDS_GRAVITY:
			case ENVIRONMENTIDS_SEALEVEL:
			case ENVIRONMENTIDS_SEAVISCUSDRAG: {
				double value;
				*sce->InBuffer() >> value;
				count++;
				m_environment->SetNumber(key, (float)value);
			} break;
		} // end switch
	} // end while

	InvalidateLightCaches();
}

void ClientEngine::SCE_ScriptSetDayState(ScriptClientEvent* sce) {
	if (!m_environment)
		return;

	int zi = 0;
	int size = 0;
	double temp = 0;
	ULONG key = 0;
	gsValues* gsVal;
	gsMetaData* gsMD;

	if (!m_environment->m_environmentTimeSystem)
		m_environment->m_environmentTimeSystem = new CTimeSystemObj();
	if (!m_environment->m_environmentTimeSystem->m_pDSO)
		m_environment->m_environmentTimeSystem->m_pDSO = new CDayStateObj(m_environment);
	m_environment->m_environmentTimeSystem->m_pDSO->GetValues(gsVal, gsMD);

	*sce->InBuffer() >> zi >> size;
	int count = 0;
	while (count < size) {
		*sce->InBuffer() >> temp;
		count++;
		key = (ULONG)temp;
		if (key >= gsVal->size()) //make sure our index is in range.
			continue;

		switch (key) {
			case DAYSTATEIDS_FOGCOLORRED: //GetSet RGBA
			case DAYSTATEIDS_SUNCOLORRED:
			case DAYSTATEIDS_AMBIENTCOLORRED: {
				double r, g, b;
				*sce->InBuffer() >> r >> g >> b;
				count += 3;
				m_environment->m_environmentTimeSystem->m_pDSO->SetColor(key, (float)r, (float)g, (float)b);
			} break;

			case DAYSTATEIDS_SUNDISTANCE: //GetSet Long
			case DAYSTATEIDS_DURATION: {
				double value;
				*sce->InBuffer() >> value;
				count++;
				m_environment->m_environmentTimeSystem->m_pDSO->SetNumber(key, (LONG)value);
			} break;

			case DAYSTATEIDS_FOGSTARTRANGE: //GetSet Float
			case DAYSTATEIDS_FOGENDRANGE:
			case DAYSTATEIDS_SUNROTATIONX:
			case DAYSTATEIDS_SUNROTATIONY: {
				double value;
				*sce->InBuffer() >> value;
				count++;
				m_environment->m_environmentTimeSystem->m_pDSO->SetNumber(key, (float)value);
			} break;
		} // end switch
	} // end while

	InvalidateLightCaches();
}

void ClientEngine::SCE_PlayerLookAt(ScriptClientEvent* sce) {
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;

	ObjectType targetType;
	int targetId;
	string targetBone;
	Vector3f offset;
	bool followTarget, cancelableByPlayer;

	*sce->InBuffer() >> reinterpret_cast<int&>(targetType) >> targetId >> targetBone >> offset.x >> offset.y >> offset.z >> followTarget >> cancelableByPlayer;

	// If follow target with player control locked, prohibit turning. Otherwise re-enable turning.
	setMovementRestriction(MR_SRC_LOOKAT, RestrictMovement_Turn, followTarget && !cancelableByPlayer);

	pMO->setLookAt(targetType, targetId, targetBone, offset, followTarget);
}

void ClientEngine::SCE_ObjectLookAt(ScriptClientEvent* sce) {
	ULONG zoneId;
	ULONG placementId;
	*sce->InBuffer() >> zoneId >> placementId;

	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return;
	}

	ObjectType targetType;
	ULONG targetId;
	string targetBone;
	Vector3f offset;
	bool followTarget, allowXZRotation;
	Vector3f forwardDirection;

	*sce->InBuffer() >> reinterpret_cast<int&>(targetType) >> targetId >> targetBone >> offset.x >> offset.y >> offset.z >> followTarget >> allowXZRotation >> forwardDirection.x >> forwardDirection.y >> forwardDirection.z;
	pDPO->SetLookAt(targetType, targetId, targetBone, offset, followTarget, allowXZRotation, forwardDirection);
}

void ClientEngine::SCE_ObjectAddLabels(ScriptClientEvent* sce) {
	int placementId;
	int numberOfLabels;
	*sce->InBuffer() >> placementId >> numberOfLabels;

	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return;
	}

	for (int i = 0; i < numberOfLabels; ++i) {
		string text;
		float size, red, green, blue;
		*sce->InBuffer() >> text >> size >> red >> green >> blue;
		if (!pDPO->AddSkeletonLabel(text, size, red, green, blue)) {
			LogError("AddSkeletonLabel() FAILED - glid=" << pDPO->GetGlobalId() << " pid=" << pDPO->GetPlacementId() << " '" << text << "'");
		}
	}
}

void ClientEngine::SCE_ObjectSetOutline(ScriptClientEvent* sce) {
	ULONG placementId;
	float red, green, blue;
	bool show, setColor;
	D3DXCOLOR color;
	*sce->InBuffer() >> placementId >> show >> setColor;
	if (setColor) {
		*sce->InBuffer() >> red >> green >> blue;
		color.a = 1.0;
		color.r = red;
		color.g = green;
		color.b = blue;
	}
	CDynamicPlacementObj* pObj = GetDynamicObject(placementId);
	if (!pObj)
		return;
	if (setColor)
		pObj->SetOutlineColor(color);
	pObj->SetOutlineState(show);
}

void ClientEngine::SCE_ObjectClearLabels(ScriptClientEvent* sce) {
	int placementId;
	*sce->InBuffer() >> placementId;

	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return;
	}

	if (!pDPO->ResetSkeletonLabels()) {
		LogError("ResetSkeletonLabels() FAILED - glid=" << pDPO->GetGlobalId() << " pid=" << pDPO->GetPlacementId());
	}
}

void ClientEngine::SCE_RenderLabelsOnTop(ScriptClientEvent* sce) {
	bool onTop;
	*sce->InBuffer() >> onTop;
	RenderLabelsOnTop(onTop);
}

void ClientEngine::SCE_DownloadAsset(ScriptClientEvent* sce) {
	string filename;
	short chunkNumber = 0;
	int assetType;
	unsigned long bytesSent;
	unsigned long bytesRead;
	unsigned long seekPosition;
	(*sce->InBuffer()) >> filename;
	(*sce->InBuffer()) >> bytesSent;
	(*sce->InBuffer()) >> bytesRead;
	ASSERT(bytesRead <= bytesSent); // This is incompatible and we will have to bump protocol.
	unsigned int buffer_size = bytesSent / sizeof(unsigned long);
	unsigned long* buffer = NULL;
	if (buffer_size > 0) {
		buffer = new unsigned long[buffer_size];
		for (unsigned int i = 0; i < buffer_size; i++)
			(*sce->InBuffer()) >> buffer[i];
	}
	(*sce->InBuffer()) >> chunkNumber;
	(*sce->InBuffer()) >> assetType;
	(*sce->InBuffer()) >> seekPosition;
	bool success = false;
	bool continuedDownload = true;
	if (chunkNumber == 0 && seekPosition < ScriptServerEvent::BYTES_BEFORE_ACK)
		continuedDownload = false;

	if (chunkNumber == ScriptServerEvent::BUT_WAIT_THERES_MORE) {
		requestSourceAssetFromAppServer(assetType, filename, seekPosition);
		// DRF - Memory Leak Fix
		if (buffer)
			delete[] buffer;
		return;
	} else
		success = createAssetFromChunks(filename, chunkNumber, (BYTE*)buffer, bytesRead, (ScriptServerEvent::UploadAssetTypes)assetType, continuedDownload);

	if (buffer)
		delete[] buffer;

	// Last chunkNumber < 0
	if (chunkNumber < 0 && !success) {
		LogError("FAILED - Failed Downloading - filename=" << filename);
	} else if (chunkNumber < 0) {
		LogInfo("OK - filename=" << filename);
	}
}

void ClientEngine::SCE_PublishAssetCompleted(ScriptClientEvent* sce) {
	string fileName, errorMsg;
	int assetType;
	bool success;
	(*sce->InBuffer()) >> assetType;
	(*sce->InBuffer()) >> fileName;
	(*sce->InBuffer()) >> success;
	(*sce->InBuffer()) >> errorMsg;
	string path, extension;
	if (!classifyAssetType((ScriptServerEvent::UploadAssetTypes)assetType, path, extension)) {
		LogError("FAILED - classifyAssetType() - filename=" << fileName);
		return;
	}
	string fullFilePath(PathAdd(path.c_str(), fileName.c_str()));
	fullFilePath = fullFilePath + extension;
	if (success && !deleteFile(fullFilePath.c_str(), TRUE)) {
		DWORD err = GetLastError();
		if (err == ERROR_ACCESS_DENIED) {
			LogError("FAILED - ACCESS DENIED - path=" << fullFilePath);
		} else {
			LogError("FAILED - FILE NOT FOUND - path=" << fullFilePath);
		}
		return;
	} else {
#ifdef _ClientOutput
		auto pICE = IClientEngine::Instance();
		if (!pICE)
			return;
		IEvent* completeEvent = pICE->GetDispatcher()->MakeEvent(m_3DAppAssetPublishCompleteEventId);
		if (completeEvent) {
			*completeEvent->OutBuffer() << fileName << (int)assetType << (int)success << errorMsg;
			pICE->GetDispatcher()->QueueEvent(completeEvent);
		}
#endif
	}
	string bakFilePath = path + "~" + fileName + extension;
	::DeleteFileW(Utf8ToUtf16(bakFilePath).c_str());
}

void ClientEngine::SCE_PublishResponseEvent(ScriptClientEvent* sce) {
	string fileName, fullClientPath;
	unsigned long seekPos;
	int assetType;
	unsigned long objPlacementId;
	(*sce->InBuffer()) >> fileName;
	(*sce->InBuffer()) >> fullClientPath;
	(*sce->InBuffer()) >> assetType;
	(*sce->InBuffer()) >> seekPos;
	(*sce->InBuffer()) >> objPlacementId; // optional objPlacementId used if asset type is ActionInstance. for other type this value will be 0.
	UploadAssetTo3DApp(fileName, fullClientPath, assetType, seekPos, objPlacementId);
}

void ClientEngine::SCE_PublishDeleteAssetCompleted(ScriptClientEvent* sce) {
#ifdef _ClientOutput
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	string fileName, errorMsg;
	int assetType;
	bool success;
	(*sce->InBuffer()) >> assetType;
	(*sce->InBuffer()) >> fileName;
	(*sce->InBuffer()) >> success;
	(*sce->InBuffer()) >> errorMsg;
	IEvent* completeEvent = pICE->GetDispatcher()->MakeEvent(m_3DAppAssetPublishDeleteCompleteEventId);
	if (completeEvent) {
		*completeEvent->OutBuffer() << fileName << (int)assetType << (int)success << errorMsg;
		pICE->GetDispatcher()->QueueEvent(completeEvent);
	}
#endif
}

void ClientEngine::SCE_GetAllScripts(ScriptClientEvent* sce) {
	if (m_pendingLuaScriptFilePath.size() <= 0)
		return;

	bool fileFound = false;

	int lastSlash = m_pendingLuaScriptFilePath.find_last_of("\\");
	string fileName = m_pendingLuaScriptFilePath.substr(lastSlash + 1, m_pendingLuaScriptFilePath.size());

	unsigned long runningScriptsCount;
	unsigned long runningScriptObjectId;
	string runningScriptFileName;

	(*sce->InBuffer()) >> runningScriptsCount;

	for (unsigned int i = 0; i < runningScriptsCount; i++) {
		(*sce->InBuffer()) >> runningScriptObjectId;
		(*sce->InBuffer()) >> runningScriptFileName;

		if (runningScriptFileName == fileName) {
			fileFound = true;
			break;
		}
	}

	if (!fileFound) {
		// the filename was not found in the running scripts
		// now check all the saved scripts
		unsigned long savedScriptsCount;
		string savedScriptFileName;

		(*sce->InBuffer()) >> savedScriptsCount;

		for (unsigned int i = 0; i < savedScriptsCount; i++) {
			(*sce->InBuffer()) >> savedScriptFileName;

			if (savedScriptFileName == fileName) {
				fileFound = true;
				break;
			}
		}
	}

	if (fileFound) {
		// conflict found
		IEvent* nameConflictEvent = m_dispatcher.MakeEvent(m_3DAppAssetUploadNameConflictEventId);
		if (nameConflictEvent) {
			*nameConflictEvent->OutBuffer() << m_pendingLuaScriptFilePath << m_pendingLuaScriptObjectId;
			m_dispatcher.QueueEvent(nameConflictEvent);

			// Now uploading the file is the responsibility of the script that handles 3DAppAssetUploadNameConflictEvent
			m_pendingLuaScriptFilePath = "";
			m_pendingLuaScriptObjectId = 0;
		}
	} else {
		// no conflict found, upload file
		UploadDroppedLuaScript(m_pendingLuaScriptFilePath, m_pendingLuaScriptObjectId);
	}
}

void ClientEngine::SCE_ObjectSetCollision(ScriptClientEvent* sce) {
	int zoneId, placementId, collisionValue;
	(*sce->InBuffer()) >> zoneId >> placementId >> collisionValue;
	DynamicObjectCollisionFilter(placementId, collisionValue != 0);
}

void ClientEngine::SCE_ObjectSetMouseOver(ScriptClientEvent* sce) {
	ObjectType objType;
	int objId, mosMode, cursorType, mosCategory;
	string toolTip;
	(*sce->InBuffer()) >> reinterpret_cast<int&>(objType) >> objId >> mosMode >> toolTip >> cursorType >> mosCategory;
	MOS_Add(objType, objId, (MOS::MODE)mosMode, toolTip, (CURSOR_TYPE)cursorType, (MOS::CATEGORY)mosCategory);
}

void ClientEngine::SCE_ObjectSetDrawDistance(ScriptClientEvent* sce) {
	int zoneId, placementId;
	double dist;
	(*sce->InBuffer()) >> zoneId >> placementId >> dist;

	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return;
	}

	pDPO->SetMinDrawDistanceOverride(dist);
}

void ClientEngine::SCE_PlayerAddIndicator(ScriptClientEvent* sce) {
	int glid, type;
	float offsetX, offsetY, offsetZ, speed, moveY;
	string id;
	(*sce->InBuffer()) >> type >> id >> offsetX >> offsetY >> offsetZ >> glid >> speed >> moveY;
	IEvent* addEvent = m_dispatcher.MakeEvent(m_addIndicatorEventId);
	if (addEvent) {
		*addEvent->OutBuffer() << type << id << offsetX << offsetY << offsetZ << glid << speed << moveY;
		m_dispatcher.QueueEvent(addEvent);
	}
}

void ClientEngine::SCE_PlayerClearIndicators(ScriptClientEvent* sce) {
	int type;
	string id;
	(*sce->InBuffer()) >> type >> id;
	IEvent* addEvent = m_dispatcher.MakeEvent(m_clearObjectIndicatorsEventId);
	if (addEvent) {
		*addEvent->OutBuffer() << type << id;
		m_dispatcher.QueueEvent(addEvent);
	}
}

void ClientEngine::SCE_PlayerUpdateCoinHUD(ScriptClientEvent* sce) {
	int placementId, value;
	string type;
	(*sce->InBuffer()) >> placementId >> type >> value;
	MenuOpen("Smart_HUDCoin.xml");
	IEvent* coinEvent = m_dispatcher.MakeEvent(m_smartCoinHUDEventId);
	if (coinEvent) {
		*coinEvent->OutBuffer() << placementId << type << value;
		m_dispatcher.QueueEvent(coinEvent);
	}
}

void ClientEngine::SCE_PlayerUpdateHealthHUD(ScriptClientEvent* sce) {
	int placementId, health, healthChange;
	string type, target;
	(*sce->InBuffer()) >> placementId >> type >> target >> health >> healthChange;
	MenuOpen("Smart_HUDHealth.xml");
	IEvent* healthEvent = m_dispatcher.MakeEvent(m_smartHealthHUDEventId);
	if (healthEvent) {
		*healthEvent->OutBuffer() << placementId << type << target << health << healthChange;
		m_dispatcher.QueueEvent(healthEvent);
	}
}

void ClientEngine::SCE_PlayerUpdateXPHUD(ScriptClientEvent* sce) {
	int placementId, level, levelXP, xpToNextLevel;
	string type;
	(*sce->InBuffer()) >> placementId >> level >> levelXP >> xpToNextLevel;
	MenuOpen("Smart_HUDXP.xml");
	IEvent* xpEvent = m_dispatcher.MakeEvent(m_smartXPHUDEventId);
	if (xpEvent) {
		*xpEvent->OutBuffer() << placementId << level << levelXP << xpToNextLevel;
		m_dispatcher.QueueEvent(xpEvent);
	}
}

void ClientEngine::SCE_PlayerAddHealthIndicator(ScriptClientEvent* sce) {
	int placementId, type, healthPercentage;
	string id;
	(*sce->InBuffer()) >> placementId >> type >> id >> healthPercentage;
	IEvent* hEvent = m_dispatcher.MakeEvent(m_addHealthIndicatorEventId);
	if (hEvent) {
		*hEvent->OutBuffer() << placementId << type << id << healthPercentage;
		m_dispatcher.QueueEvent(hEvent);
	}
}

void ClientEngine::SCE_PlayerRemoveHealthIndicator(ScriptClientEvent* sce) {
	int placementId, type;
	string id;
	(*sce->InBuffer()) >> placementId >> type >> id;
	IEvent* hEvent = m_dispatcher.MakeEvent(m_removeHealthIndicatorEventId);
	if (hEvent) {
		*hEvent->OutBuffer() << placementId << type << id;
		m_dispatcher.QueueEvent(hEvent);
	}
}

void ClientEngine::SCE_PlayerResetCursorModesTypes(ScriptClientEvent* sce) {
	int placementId;
	(*sce->InBuffer()) >> placementId;
	LogWarn("placementId=" << placementId);
	ResetCursorModesTypes();
}

void ClientEngine::SCE_PlayerSetCursorModeType(ScriptClientEvent* sce) {
	int placementId, cursorMode, cursorType;
	(*sce->InBuffer()) >> placementId >> cursorMode >> cursorType;
	LogWarn("placementId=" << placementId << " cursorMode=" << cursorMode << " cursorType=" << cursorType);
	SetCursorModeType((CURSOR_MODE)cursorMode, (CURSOR_TYPE)cursorType);
}

void ClientEngine::SCE_PlayerSetCursorMode(ScriptClientEvent* sce) {
	int placementId, cursorMode;
	(*sce->InBuffer()) >> placementId >> cursorMode;
	LogWarn("placementId=" << placementId << " cursorMode=" << cursorMode);
	SetCursorMode((CURSOR_MODE)cursorMode);
}

void ClientEngine::SCE_PlayerOpenShop(ScriptClientEvent* sce) {
	int placementId;
	string title, identifier;
	(*sce->InBuffer()) >> placementId >> title >> identifier;
	MenuOpen("Smart_Shop.xml");
	IEvent* sEvent = m_dispatcher.MakeEvent(m_openShopMenuEventId);
	if (sEvent) {
		*sEvent->OutBuffer() << placementId << title << identifier;
		m_dispatcher.QueueEvent(sEvent);
	}
}

void ClientEngine::SCE_PlayerAddItemToShop(ScriptClientEvent* sce) {
	int placementId, id, cost;
	string itemName, icon, identifier;
	(*sce->InBuffer()) >> placementId >> id >> itemName >> cost >> icon >> identifier;
	IEvent* sEvent = m_dispatcher.MakeEvent(m_addItemToShopEventId);
	if (sEvent) {
		*sEvent->OutBuffer() << placementId << id << itemName << cost << icon << identifier;
		m_dispatcher.QueueEvent(sEvent);
	}
}

void ClientEngine::SCE_PlayerShowStatusInfo(ScriptClientEvent* sce) {
	int placementId, time, r, g, b;
	string message;
	(*sce->InBuffer()) >> placementId >> message >> time >> r >> g >> b;
	IEvent* sEvent = m_dispatcher.MakeEvent(m_showStatusInfoEventId);
	if (sEvent) {
		*sEvent->OutBuffer() << placementId << message << time << r << g << b;
		m_dispatcher.QueueEvent(sEvent);
	}
}

void ClientEngine::SCE_PlayerSpawnVehicle(ScriptClientEvent* sce) {
	// Pull all of the data from the event.
	int placementId;
	Vector3f offset;
	Quaternionf orientation;
	Vector3f dimensions;
	float speed, hp;
	float loadSusp, unloadSusp;
	float mass, wheelFriction;
	Vector2f jumpVelocityMPS;
	GLID glidThrottle;
	GLID glidSkid;
	ULONG clientId;
	(*sce->InBuffer()) >> placementId >> offset >> orientation >> dimensions >> speed >> hp >> loadSusp >> unloadSusp >> mass >> wheelFriction >> jumpVelocityMPS >> glidThrottle >> glidSkid >> clientId;

	// Get My Avatar
	CMovementObj* pMO = GetMovementObjMe();
	if (!pMO)
		return;

	// Get the target movement object.
	CMovementObj* pMO_target = GetMovementObjByNetId(clientId);
	if (!pMO_target) {
		LogError("User " << clientId << " does not exist in this world.");
		return;
	}

	LogInfo("New vehicle in world for " << (pMO == pMO_target ? "local" : "remote") << " user " << clientId);

	// Build the offset position and rotation.
	Vector3f offsetPos = offset;
	Quaternionf offsetRot = orientation;
	if (!offsetRot.Normalize())
		offsetRot.MakeIdentity();

	CDynamicPlacementObj* pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("Vehicle object " << placementId << " does not exist in this world.");
		return;
	}

	Matrix44f mtxDynObjToPhysics;
	RotationTranslationToMatrix(offsetRot, MetersToFeet(offsetPos), out(mtxDynObjToPhysics));

	// Get the vehicle position, then adjust for the offset and set the player's position.
	// Get the matrix of the vehicle
	Matrix44f mtxDynObjWorld;
	pDPO->GetWorldMatrix(mtxDynObjWorld);

	// Position avatar at the center rear of the physics vehicle.
	Matrix44f mtxPhysicsToAvatar;
	mtxPhysicsToAvatar.MakeIdentity();
	mtxPhysicsToAvatar.MakeTranslation(MetersToFeet(Vector3f(0, -0.5f * dimensions.y, -0.5f * dimensions.z)));
	mtxPhysicsToAvatar(0, 0) = -1;
	mtxPhysicsToAvatar(2, 2) = -1;
	Matrix44f mtxDynObjToAvatar = mtxPhysicsToAvatar * mtxDynObjToPhysics;

	// My Avatar's Vehicle ?
	if (pMO == pMO_target) {
		// My Avatar Controls Vehicle
		if (pMO_target->getVehiclePlacementId() != placementId) {
			// Current player is not already controlling this vehicle.
			// Build the vehicle properties.
			Physics::VehicleProperties props;
			props.m_vDimensionsM = dimensions;
			props.m_fMaxSpeedKPH = speed;
			props.m_fEngineHorsepower = hp;
			props.m_fSuspensionLoadedLength = loadSusp;
			props.m_fSuspensionUnloadedLength = unloadSusp;
			props.m_fMassKG = mass;
			props.m_fWheelFriction = wheelFriction;
			props.m_iOtherVehicleCollisionGroups = CollisionGroup::OtherPlayerVehicle;
			props.m_vTurboVelocityMPS = jumpVelocityMPS;

			//Position avatar
			Matrix44f mtxAvatar = mtxDynObjToAvatar * mtxDynObjWorld;
			pMO->setBaseFrameMatrix(mtxAvatar, true);

			// Now engage the physics vehicle.
			EngagePhysicsVehicle(placementId, offsetPos, offsetRot, props);
		} else {
			LogInfo("Player is already driving this vehicle!");
		}
	} else {
		// Vehicle will be controlled by another client.

		// Attach vehicle to movement object.
		pMO_target->setVehiclePlacementId(placementId);
		pMO_target->setMtxDynamicObjectToMovementObject(mtxDynObjToAvatar);
		pMO_target->setMtxPhysicsVehicleToDynamicObject(InverseOf(mtxDynObjToPhysics));

		// Update collision flags for dynamic object.
		Physics::RigidBodyStatic* pRigidBodyStatic = pDPO->GetRigidBody().get();
		if (pRigidBodyStatic) {
			pRigidBodyStatic->SetCollisionMasks(CollisionGroup::VehicleDynamicObject, CollisionMask::VehicleDynamicObject);
		}

		// Add new rigid body.
		if (pMO_target->getVehicleRigidBody())
			m_pUniverse->Remove(pMO_target->getVehicleRigidBody().get()); // Make sure old vehicle rigid body is removed from universe.

		Physics::RigidBodyStaticProperties rigidBodyProps;
		rigidBodyProps.m_CollidesWithMask = CollisionMask::OtherPlayerVehicle;
		rigidBodyProps.m_CollisionGroupMask = CollisionGroup::OtherPlayerVehicle;
		Physics::CollisionShapeBoxProperties shapeProps;
		shapeProps.m_vDimensionsMeters = dimensions;
		std::shared_ptr<Physics::CollisionShape> pShape = Physics::CreateCollisionShape(shapeProps);
		pMO_target->setVehicleRigidBody(Physics::CreateRigidBodyStatic(rigidBodyProps, pShape.get(), 1.0f));
		m_pUniverse->Add(pMO_target->getVehicleRigidBody());
	}

	// DRF - ED-2803 - Vehicle Sounds - Optional Throttle Sound
	if (glidThrottle) {
		auto soundPlacementId1 = PlaySoundOnDynamicObject(placementId, glidThrottle);
		if (soundPlacementId1) {
			auto pSP = SoundPlacementGet(soundPlacementId1);
			if (pSP) {
				pSP->SetQuiet(true);
				pSP->SetMaxDist(200.0);
			}
			SoundPlacementAddModifier(soundPlacementId1, SoundPlacement::Modifier(
															 SoundPlacement::Modifier::Input::INPUT_SPEED,
															 SoundPlacement::Modifier::Output::OUTPUT_PITCH,
															 0.01,
															 1.0));
			SoundPlacementAddModifier(soundPlacementId1, SoundPlacement::Modifier(
															 SoundPlacement::Modifier::Input::INPUT_THROTTLE,
															 SoundPlacement::Modifier::Output::OUTPUT_GAIN,
															 0.01,
															 0.2));
		}
	}

	// DRF - ED-2803 - Vehicle Sounds - Optional Skid Sound
	if (glidSkid) {
		auto soundPlacementId2 = PlaySoundOnDynamicObject(placementId, glidSkid);
		if (soundPlacementId2) {
			auto pSP = SoundPlacementGet(soundPlacementId2);
			if (pSP) {
				pSP->SetQuiet(true);
				pSP->SetMaxDist(200.0);
			}
			SoundPlacementAddModifier(soundPlacementId2, SoundPlacement::Modifier(
															 SoundPlacement::Modifier::Input::INPUT_SKID,
															 SoundPlacement::Modifier::Output::OUTPUT_GAIN,
															 1.0,
															 0.0));
			SoundPlacementAddModifier(soundPlacementId2, SoundPlacement::Modifier(
															 SoundPlacement::Modifier::Input::INPUT_SKID,
															 SoundPlacement::Modifier::Output::OUTPUT_PLAY,
															 1.0,
															 0.1));
		}
	}
}

void ClientEngine::SCE_PlayerModifyVehicle(ScriptClientEvent* sce) {
	float maxSpeedKPH;
	float horsepower;
	Vector2f jumpVelocityMPS;
	ULONG ownerNetTraceId;
	*sce->InBuffer() >> maxSpeedKPH >> horsepower >> jumpVelocityMPS >> ownerNetTraceId;

	// Get My Avatar
	CMovementObj* pMO = GetMovementObjMe();
	if (!pMO)
		return;

	// Get the target movement object.
	CMovementObj* pMO_target = GetMovementObjByNetId(ownerNetTraceId);
	if (!pMO_target) {
		LogError("User " << ownerNetTraceId << " does not exist in this world.");
		return;
	}

	Physics::Vehicle* pVehicle = pMO_target->getPhysicsVehicle().get();
	if (!pVehicle) {
		LogError("User " << ownerNetTraceId << " is not driving a vehicle.");
		return;
	}

	LogInfo("Modifying vehicle properties for " << (pMO == pMO_target ? "local" : "remote") << " user " << ownerNetTraceId);

	Physics::VehicleProperties props = pVehicle->GetVehicleProperties();
	props.m_fMaxSpeedKPH = maxSpeedKPH;
	props.m_fEngineHorsepower = horsepower;
	props.m_vTurboVelocityMPS = jumpVelocityMPS;
	props.m_fMaxReverseSpeedKPH = std::min<float>(props.m_fMaxReverseSpeedKPH, maxSpeedKPH);
	pVehicle->UpdateVehicleProperties(props);
}

void ClientEngine::SCE_PlayerLeaveVehicle(ScriptClientEvent* sce) {
	// Pull all of the data from the event.
	int placementId;
	ULONG clientId;
	(*sce->InBuffer()) >> placementId >> clientId;

	// Get My Avatar
	CMovementObj* pMO = GetMovementObjMe();
	if (!pMO)
		return;

	// Get the target movement object.
	CMovementObj* pMO_target = GetMovementObjByNetId(clientId);
	if (!pMO_target)
		return;

	// Remove All Sounds On Dynamic Object
	RemoveSoundOnDynamicObject(placementId, 0);

	// My Avatar's Vehicle ?
	if (pMO == pMO_target) {
		DisengagePhysicsVehicle();
	} else {
		// Restore collision object of vehicle driven by other player.
		m_pUniverse->Remove(pMO_target->getVehicleRigidBody().get());
		pMO_target->resetVehicleRigidBody();

		CDynamicPlacementObj* pDPO = GetDynamicObject(placementId);
		if (pDPO) {
			std::shared_ptr<Physics::RigidBodyStatic> pRigidBodyStatic = pDPO->GetRigidBody();
			if (pRigidBodyStatic) {
				pRigidBodyStatic->SetCollisionMasks(CollisionGroup::Object, CollisionMask::Object);
				//m_pUniverse->Add(pRigidBodyStatic);
			}
		}

		// Detach the vehicle from the avatar.
		pMO_target->setVehiclePlacementId(0);
	}
}

void ClientEngine::SCE_PlayerSetVisibility(ScriptClientEvent* sce) {
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;

	ULONG clientId;
	int visibilityFlag;

	*sce->InBuffer() >> clientId >> visibilityFlag;
	pMO->setVisible(visibilityFlag != 0);
}

void ClientEngine::SCE_PlayerClosePresetMenu(ScriptClientEvent* sce) {
	string menuName;
	ULONG placementId;
	(*sce->InBuffer()) >> placementId >> menuName; // menuName can also be the identifier
	IEvent* closeEvent = m_dispatcher.MakeEvent(m_smartCloseMenuEventId);
	if (closeEvent) {
		*closeEvent->OutBuffer() << placementId << menuName;
		m_dispatcher.QueueEvent(closeEvent);
	}
}

void ClientEngine::SCE_PlayerMovePresetMenu(ScriptClientEvent* sce) {
	string menuName;
	ULONG placementId;
	int x, y;
	(*sce->InBuffer()) >> placementId >> menuName >> x >> y; // menuName can also be the identifier
	IEvent* moveEvent = m_dispatcher.MakeEvent(m_smartMoveMenuEventId);
	if (moveEvent) {
		*moveEvent->OutBuffer() << placementId << menuName << x << y;
		m_dispatcher.QueueEvent(moveEvent);
	}
}

void ClientEngine::SCE_PlayerOpenListBoxMenu(ScriptClientEvent* sce) {
	string title, listContents, buttonText, identifier;
	ULONG placementId;
	(*sce->InBuffer()) >> placementId >> title >> listContents >> buttonText >> identifier;
	MenuOpen("Smart_ListBox.xml");
	IEvent* listEvent = m_dispatcher.MakeEvent(m_smartListBoxMenuEventId);
	if (listEvent) {
		*listEvent->OutBuffer() << placementId << title << listContents << buttonText << identifier;
		m_dispatcher.QueueEvent(listEvent);
	}
}

void ClientEngine::SCE_PlayerOpenYesNoMenu(ScriptClientEvent* sce) {
	string title, description, yes, no, identifier;
	ULONG placementId;
	(*sce->InBuffer()) >> placementId >> title >> description >> yes >> no >> identifier;
	MenuOpen("Smart_YesNo.xml");
	IEvent* yesNoEvent = m_dispatcher.MakeEvent(m_smartYesNoMenuEventId);
	if (yesNoEvent) {
		*yesNoEvent->OutBuffer() << placementId << title << description << yes << no << identifier;
		m_dispatcher.QueueEvent(yesNoEvent);
	}
}

void ClientEngine::SCE_PlayerOpenKeyListenerMenu(ScriptClientEvent* sce) {
	string keys, identifier;
	ULONG placementId;
	(*sce->InBuffer()) >> placementId >> keys >> identifier;
	MenuOpen("Smart_KeyListener.xml");
	IEvent* keyListenerEvent = m_dispatcher.MakeEvent(m_smartKeyListenerEventId);
	if (keyListenerEvent) {
		*keyListenerEvent->OutBuffer() << placementId << keys << identifier;
		m_dispatcher.QueueEvent(keyListenerEvent);
	}
}

void ClientEngine::SCE_PlayerOpenTextTimerMenu(ScriptClientEvent* sce) {
	int time;
	string text, identifier;
	ULONG placementId;
	(*sce->InBuffer()) >> placementId >> text >> time >> identifier;
	MenuOpen("Smart_TextTimer.xml");
	IEvent* textTimerEvent = m_dispatcher.MakeEvent(m_smartTextTimerEventId);
	if (textTimerEvent) {
		*textTimerEvent->OutBuffer() << placementId << text << time << identifier;
		m_dispatcher.QueueEvent(textTimerEvent);
	}
}

void ClientEngine::SCE_PlayerOpenButtonMenu(ScriptClientEvent* sce) {
	string title, message, buttonText, identifier;
	ULONG placementId;
	(*sce->InBuffer()) >> placementId >> title >> message >> buttonText >> identifier;
	MenuOpen("Smart_Button.xml");
	IEvent* buttonEvent = m_dispatcher.MakeEvent(m_smartButtonEventId);
	if (buttonEvent) {
		*buttonEvent->OutBuffer() << placementId << title << message << buttonText << identifier;
		m_dispatcher.QueueEvent(buttonEvent);
	}
}

void ClientEngine::SCE_PlayerOpenStatusMenu(ScriptClientEvent* sce) {
	string title, status, identifier;
	ULONG placementId;
	(*sce->InBuffer()) >> placementId >> title >> status >> identifier;
	MenuOpen("Smart_Status.xml");
	IEvent* statusEvent = m_dispatcher.MakeEvent(m_smartStatusEventId);
	if (statusEvent) {
		*statusEvent->OutBuffer() << placementId << title << status << identifier;
		m_dispatcher.QueueEvent(statusEvent);
	}
}

void ClientEngine::SCE_PlayerSetCustomTextureOnAccessory(ScriptClientEvent* sce) {
	unsigned long playerId, accessoryGlid;
	string url;
	(*sce->InBuffer()) >> playerId >> accessoryGlid >> url;

	auto pMO = GetMovementObjByNetId(playerId);
	if (!pMO)
		return;

	SetPlayerCustomTextureOnAccessoryLocal(pMO, accessoryGlid, url);
}

void ClientEngine::SCE_PlayerIndividualObjectControl(ScriptClientEvent* sce) {
	int placementId, zoneId, instanceId, argCount;
	unsigned long type;
	vector<int> args;

	(*sce->InBuffer()) >> placementId >> zoneId >> instanceId >> type >> argCount;

	for (int i = 0; i < argCount; i++) {
		int arg;
		(*sce->InBuffer()) >> arg;
		args.push_back(arg);
	}

	if (type == OBJECT_SHOW || type == OBJECT_HIDE) {
		auto pDPO = GetDynamicObject(placementId);
		if (!pDPO) {
			LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
			return;
		}

		pDPO->SetVisibility(type == OBJECT_SHOW ? eVisibility::VISIBLE : eVisibility::HIDDEN_FOR_ME);
	}
	return;
}

void SCE_ClientHandler<ScriptClientEvent::ObjectEquipItem>::Handle(int placementId, GLID accessoryGlid, const boost::optional<std::string>& optstrBoneOverride) {
	s_pClientEngine->ObjectArmEquippableItemAsync(ObjectType::DYNAMIC, placementId, accessoryGlid, 0, optstrBoneOverride);
}

void SCE_ClientHandler<ScriptClientEvent::ObjectUnEquipItem>::Handle(int placementId, GLID accessoryGlid) {
	s_pClientEngine->ObjectDisarmEquippableItem(ObjectType::DYNAMIC, placementId, accessoryGlid);
}

// DRF - TODO - Change glids to AnimSettings
void SCE_ClientHandler<ScriptClientEvent::ObjectSetEquippedItemAnimation>::Handle(int placementId, GLID accessoryGlid, GLID animationGlid) {
	CDynamicPlacementObj* pDPO = s_pClientEngine->GetDynamicObject(placementId);
	s_pClientEngine->SetEquippableAnimation(pDPO, accessoryGlid, animationGlid);
}

void SCE_ClientHandler<ScriptClientEvent::ObjectRepositionEquippedItem>::Handle(int placementId, GLID accessoryGlid, const Vector3f& ptBBoxRelativePosition, const Vector3f& vOffset, const Vector3f& vEulerZXY, const Vector3f& vScale) {
	CDynamicPlacementObj* pDPO = s_pClientEngine->GetDynamicObject(placementId);
	s_pClientEngine->RepositionEquippableItem(pDPO, accessoryGlid, ptBBoxRelativePosition, vOffset, vEulerZXY, vScale);
}

void SCE_ClientHandler<ScriptClientEvent::ObjectSetTriggerV2>::Handle(ScriptPIDType placementId, int triggerId, int triggerAction, const PrimitiveGeomArgs<float>& volume, int param) {
	s_pClientEngine->AddDynamicObjectTrigger(placementId.value, triggerId, (eTriggerAction)triggerAction, volume, param, "");
}

void SCE_ClientHandler<ScriptClientEvent::ObjectRemoveTriggerV2>::Handle(ScriptPIDType placementId, int triggerId) {
	s_pClientEngine->PendingDeleteDynamicObjectTrigger(placementId.value, triggerId);
}

} // namespace KEP