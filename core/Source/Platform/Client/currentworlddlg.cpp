/******************************************************************************
 currentworlddlg.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// CurrentWorldsDlg.cpp : implementation file
//

#include "stdafx.h"

#include "d3d9.h"
#include "resource.h"
#include "CurrentWorldsDlg.h"
#include "direct.h"
#include "Common/KEPUtil/Helpers.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCurrentWorldsDlg dialog

CCurrentWorldsDlg::CCurrentWorldsDlg(CWnd* pParent /*=NULL*/) :
		CKEPEditDialog(CCurrentWorldsDlg::IDD, pParent) {
	//{{AFX_DATA_INIT(CCurrentWorldsDlg)
	//}}AFX_DATA_INIT

	m_colWorld.SetFrame(&m_grpWorld);
	m_colSpawnPointCfg.SetFrame(&m_grpSpawnPointCfg);
	m_previousSelCurrentWorld = -1;
	m_previousSelSpawnPointCfg = -1;
}

void CCurrentWorldsDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCurrentWorldsDlg)
	DDX_Control(pDX, IDC_LIST_SPAWNPOINTCFGLIST, m_spawnPointCfgList);
	DDX_Control(pDX, IDC_LIST_WORLDLIST, m_worldList);

	DDX_Control(pDX, IDC_BUTTON_ADD, m_btnAddWorld);
	DDX_Control(pDX, IDC_BUTTON_DELETE, m_btnDeleteWorld);
	DDX_Control(pDX, IDC_BUTTON_REPLACE, m_btnReplaceWorld);

	DDX_Control(pDX, IDC_BUTTON_ADDSPAWNPOINTCFG, m_btnAddSpawnPointCfg);
	DDX_Control(pDX, IDC_BUTTON_DELETESPAWNPOINTCFG, m_btnDeleteSpawnPointCfg);
	DDX_Control(pDX, IDC_BUTTON_REPLACESPAWNPOINTCFG, m_btnReplaceSpawnPointCfg);

	DDX_Control(pDX, IDC_PROP_WORLD, m_propWorld);
	DDX_Control(pDX, IDC_STATIC_WORLD, m_grpWorld);
	DDX_Control(pDX, IDC_CHECK_WORLD, m_colWorld);

	DDX_Control(pDX, IDC_PROP_SPAWN_POINT_CFG, m_propSpawnPointCfg);
	DDX_Control(pDX, IDC_STATIC_SPAWN_POINT_CFG, m_grpSpawnPointCfg);
	DDX_Control(pDX, IDC_CHECK_SPAWN_POINT_CFG, m_colSpawnPointCfg);

	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCurrentWorldsDlg, CDialog)
//{{AFX_MSG_MAP(CCurrentWorldsDlg)
ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)
ON_BN_CLICKED(IDC_BUTTON_REPLACE, OnBUTTONReplace)
ON_BN_CLICKED(IDC_BUTTON_ADD, OnBUTTONAdd)
ON_BN_CLICKED(IDC_BUTTON_ADDSPAWNPOINTCFG, OnBUTTONAddSpawnPointCfg)
ON_BN_CLICKED(IDC_BUTTON_DELETESPAWNPOINTCFG, OnBUTTONDeleteSpawnPointCfg)
ON_BN_CLICKED(IDC_BUTTON_REPLACESPAWNPOINTCFG, OnBUTTONReplaceSpawnPointCfg)
ON_LBN_SELCHANGE(IDC_LIST_SPAWNPOINTCFGLIST, OnSelchangeLISTSpawnPointCfgList)
ON_LBN_SELCHANGE(IDC_LIST_WORLDLIST, OnSelchangeLISTWorldList)
ON_MESSAGE(WM_DESTROY, OnDestroy)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCurrentWorldsDlg message handlers

BOOL CCurrentWorldsDlg::OnInitDialog() {
	CDialog::OnInitDialog();

	if (!GL_multiplayerObject->m_currentWorldsUsed)
		GL_multiplayerObject->m_currentWorldsUsed = new CWorldAvailableObjList();

	if (!GL_multiplayerObject->m_spawnPointCfgs)
		GL_multiplayerObject->m_spawnPointCfgs = new CSpawnCfgObjList();

	InListCurrentWorlds(0);
	InListCurrentSpawnCfg(0);

	// try and set the property control based on the current
	// selection (if there is one)
	SetWorldList();
	SetCurrentSpawnCfgList();

	m_colWorld.SetCheck(1);
	m_colSpawnPointCfg.SetCheck(0);

	//
	// setup image buttons
	//

	CStringA sImagePath;
	CStringA sSelectedImagePath;
	char path_buffer[_MAX_PATH];
	string pathApp = PathApp();
	strcpy_s(path_buffer, pathApp.c_str());

	sImagePath.Format("%s\\Images\\%s", path_buffer, "Button_Add.png");
	m_btnAddWorld.LoadImages(sImagePath);
	m_btnAddWorld.SetToolTipText(IDS_TOOLTIP_ADD_WORLD);
	m_btnAddWorld.SetShowCaption(false);

	sImagePath.Format("%s\\Images\\%s", path_buffer, "Button_Delete.png");
	m_btnDeleteWorld.LoadImages(sImagePath);
	m_btnDeleteWorld.SetToolTipText(IDS_TOOLTIP_DELETE_WORLD);
	m_btnDeleteWorld.SetShowCaption(false);

	sImagePath.Format("%s\\Images\\%s", path_buffer, "Button_Replace.png");
	m_btnReplaceWorld.LoadImages(sImagePath);
	m_btnReplaceWorld.SetToolTipText(IDS_TOOLTIP_REPLACE_WORLD);
	m_btnReplaceWorld.SetShowCaption(false);

	sImagePath.Format("%s\\Images\\%s", path_buffer, "Button_Add.png");
	m_btnAddSpawnPointCfg.LoadImages(sImagePath);
	m_btnAddSpawnPointCfg.SetToolTipText(IDS_TOOLTIP_ADD_SPAWN_POINT_CONFIG);
	m_btnAddSpawnPointCfg.SetShowCaption(false);

	sImagePath.Format("%s\\Images\\%s", path_buffer, "Button_Delete.png");
	m_btnDeleteSpawnPointCfg.LoadImages(sImagePath);
	m_btnDeleteSpawnPointCfg.SetToolTipText(IDS_TOOLTIP_DELETE_SPAWN_POINT_CONFIG);
	m_btnDeleteSpawnPointCfg.SetShowCaption(false);

	sImagePath.Format("%s\\Images\\%s", path_buffer, "Button_Replace.png");
	m_btnReplaceSpawnPointCfg.LoadImages(sImagePath);
	m_btnReplaceSpawnPointCfg.SetToolTipText(IDS_TOOLTIP_REPLACE_SPAWN_POINT_CONFIG);
	m_btnReplaceSpawnPointCfg.SetShowCaption(false);

	sImagePath.Format("%s\\Images\\%s", path_buffer, "exp-collbar_down.png");
	sSelectedImagePath.Format("%s\\Images\\%s", path_buffer, L"exp-collbar_up.png");

	m_colWorld.LoadImages(sImagePath, sSelectedImagePath);
	m_colWorld.SetToolTipText(IDS_TOOLTIP_CURRENT_WORLDS);
	m_colWorld.SetShowDepress(false);
	m_colWorld.SetShowCaption(true);
	m_colWorld.SetFontSize(9.0f);
	m_colWorld.SetFontStyle(Gdiplus::FontStyleBold);
	m_colWorld.SetEnableResize(false);

	m_colSpawnPointCfg.LoadImages(sImagePath, sSelectedImagePath);
	m_colSpawnPointCfg.SetToolTipText(IDS_TOOLTIP_SPAWN_POINT_CONFIG);
	m_colSpawnPointCfg.SetShowDepress(false);
	m_colSpawnPointCfg.SetShowCaption(true);
	m_colSpawnPointCfg.SetFontSize(9.0f);
	m_colSpawnPointCfg.SetFontStyle(Gdiplus::FontStyleBold);
	m_colSpawnPointCfg.SetEnableResize(false);

	return TRUE; // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

LPARAM CCurrentWorldsDlg::OnDestroy(WPARAM wParam, LPARAM lParam) {
	m_colWorld.UnsubclassWindow();
	m_colSpawnPointCfg.UnsubclassWindow();
	m_btnAddWorld.UnsubclassWindow();
	m_btnDeleteWorld.UnsubclassWindow();
	m_btnReplaceWorld.UnsubclassWindow();
	m_btnAddSpawnPointCfg.UnsubclassWindow();
	m_btnDeleteSpawnPointCfg.UnsubclassWindow();
	m_btnReplaceSpawnPointCfg.UnsubclassWindow();

	return TRUE;
}

void CCurrentWorldsDlg::InListCurrentWorlds(int sel) {
	UpdateData(TRUE);
	m_worldList.ResetContent();

	CMultiplayerObj* multiplayerObj = CMultiplayerObj::GetInstance();
	ListItemMap zoneMap = multiplayerObj->GetZones();
	ListItemMapIterator zoneMapIterator;

	for (zoneMapIterator = zoneMap.begin(); zoneMapIterator != zoneMap.end(); zoneMapIterator++) {
		int newsel = m_worldList.AddString(Utf8ToUtf16(zoneMapIterator->second).c_str());
		m_worldList.SetItemData(newsel, zoneMapIterator->first);
	}

	if (sel > -1) {
		m_worldList.SetCurSel(sel);
		m_previousSelCurrentWorld = sel;
		SetWorld(sel);
	}

	UpdateData(FALSE);
}

void CCurrentWorldsDlg::OnBUTTONReplace() {
	int wldSelect = m_worldList.GetCurSel();
	if (wldSelect == -1) {
		AfxMessageBox(IDS_MSG_SELZONE);
		return;
	}

	ReplaceCurrentWorld(wldSelect);
}

void CCurrentWorldsDlg::OnButtonDelete() {
	int wldSelect = m_worldList.GetCurSel();
	if (wldSelect == -1) {
		AfxMessageBox(IDS_MSG_SELZONE);
		return;
	}

	UpdateData(TRUE);

	GL_multiplayerObject->m_currentWorldsUsed->DeleteAndRemove(m_worldList.GetItemData(wldSelect));
	UpdateData(FALSE);
	InListCurrentWorlds(wldSelect == 0 ? 0 : wldSelect - 1);
}

void CCurrentWorldsDlg::OnBUTTONAdd() {
	UpdateData(TRUE);

	CWorldAvailableObj* wPtr = new CWorldAvailableObj();
	wPtr->m_channel.fromLong(m_propWorld.GetWorldID());
	wPtr->m_worldName = m_propWorld.GetWorldName();
	wPtr->m_displayName = m_propWorld.GetDisplayName();
	wPtr->m_maxOccupancy = m_propWorld.GetMaxOccupancy();
	wPtr->m_visibility = m_propWorld.GetVisibility();
	wPtr->m_index = GL_multiplayerObject->m_currentWorldsUsed->FindAvailableId();
	GL_multiplayerObject->m_currentWorldsUsed->AddTail(wPtr);

	UpdateData(FALSE);

	InListCurrentWorlds(m_worldList.GetCount());
}

void CCurrentWorldsDlg::OnBUTTONAddSpawnPointCfg() {
	int worldSelect = m_worldList.GetCurSel();
	if (worldSelect == -1) {
		AfxMessageBox(IDS_MSG_SELZONE);
		return;
	}

	UpdateData(TRUE);

	CSpawnCfgObj* spawnPtr = new CSpawnCfgObj();
	spawnPtr->m_spawnPointName = m_propSpawnPointCfg.GetSpawnPointCfgName();
	spawnPtr->m_position.x = m_propSpawnPointCfg.GetPositionX();
	spawnPtr->m_position.y = m_propSpawnPointCfg.GetPositionY();
	spawnPtr->m_position.z = m_propSpawnPointCfg.GetPositionZ();
	spawnPtr->m_rotation = m_propSpawnPointCfg.GetRotation();
	spawnPtr->m_zoneIndex.fromLong(worldSelect);
	GL_multiplayerObject->m_spawnPointCfgs->AddTail(spawnPtr);

	UpdateData(FALSE);
	//InListCurrentSpawnCfg(GL_multiplayerObject->m_spawnPointCfgs->GetCount()-1);
	InListCurrentSpawnCfg(m_spawnPointCfgList.GetCount()); //select last item
}

void CCurrentWorldsDlg::OnBUTTONDeleteSpawnPointCfg() {
	int worldSelect = m_worldList.GetCurSel();
	if (worldSelect == -1) {
		AfxMessageBox(IDS_MSG_SELZONE);
		return;
	}
	int spawnSelect = m_spawnPointCfgList.GetCurSel();
	if (spawnSelect == -1) {
		AfxMessageBox(IDS_MSG_SELSPAWNPOINT);
		return;
	}

	//POSITION spawnPos = GL_multiplayerObject->m_spawnPointCfgs->FindIndex(spawnSelect);
	CSpawnCfgObj* spawnPtr = GL_multiplayerObject->GetSpawnPointCfg(worldSelect, spawnSelect);
	if (spawnPtr) {
		//POSITION spawnPos = GL_multiplayerObject->m_spawnPointCfgs->FindIndex(spawnSelect);
		POSITION spawnPos = GL_multiplayerObject->m_spawnPointCfgs->Find(spawnPtr);
		if (spawnPos) {
			//CSpawnCfgObj * spawnPtr = (CSpawnCfgObj *)GL_multiplayerObject->m_spawnPointCfgs->GetAt(spawnPos);
			delete spawnPtr;
			spawnPtr = 0;
			GL_multiplayerObject->m_spawnPointCfgs->RemoveAt(spawnPos);
		}
	}
	InListCurrentSpawnCfg(spawnSelect == 0 ? 0 : spawnSelect - 1);
}

void CCurrentWorldsDlg::OnBUTTONReplaceSpawnPointCfg() {
	int spawnSelect = m_spawnPointCfgList.GetCurSel();
	if (spawnSelect == -1) {
		AfxMessageBox(IDS_MSG_SELSPAWNPOINT);
		return;
	}

	ReplaceSpawnPointCfg(spawnSelect);
}

void CCurrentWorldsDlg::InListCurrentSpawnCfg(int sel) {
	m_spawnPointCfgList.ResetContent();

	UpdateData(TRUE);

	for (POSITION posLoc = GL_multiplayerObject->m_spawnPointCfgs->GetHeadPosition(); posLoc != NULL;) {
		CSpawnCfgObj* spawnPtr = (CSpawnCfgObj*)GL_multiplayerObject->m_spawnPointCfgs->GetNext(posLoc);

		m_spawnPointCfgList.AddString(Utf8ToUtf16(spawnPtr->m_spawnPointName).c_str());
	}

	if (sel > -1) {
		m_spawnPointCfgList.SetCurSel(sel);
		m_previousSelSpawnPointCfg = sel;
		SetCurrentSpawnCfg(sel);
	}

	UpdateData(FALSE);
}

void CCurrentWorldsDlg::OnSelchangeLISTSpawnPointCfgList() {
	int spawnPointIndex = m_spawnPointCfgList.GetCurSel();

	if (m_previousSelSpawnPointCfg != -1) {
		ReplaceSpawnPointCfg(m_previousSelSpawnPointCfg);
	}

	if (spawnPointIndex == -1) {
		AfxMessageBox(IDS_MSG_SELSPAWNPOINT);
		return;
	}

	//put back user selection
	m_spawnPointCfgList.SetCurSel(spawnPointIndex);
	m_previousSelSpawnPointCfg = spawnPointIndex;
	SetCurrentSpawnCfg(spawnPointIndex);
}

void CCurrentWorldsDlg::SetCurrentSpawnCfgList() {
	int spawnPointIndex = m_spawnPointCfgList.GetCurSel();

	if (spawnPointIndex > -1) {
		SetCurrentSpawnCfg(spawnPointIndex);
	}
}

void CCurrentWorldsDlg::SetCurrentSpawnCfg(int spawnPointIndex) {
	CSpawnCfgObj* spawnPtr = NULL;
	int indexCounter = 0;
	bool bFound = false;

	for (POSITION posLoc = GL_multiplayerObject->m_spawnPointCfgs->GetHeadPosition(); posLoc != NULL;) {
		spawnPtr = (CSpawnCfgObj*)GL_multiplayerObject->m_spawnPointCfgs->GetNext(posLoc);

		if (indexCounter == spawnPointIndex) {
			bFound = true;
			break;
		}

		indexCounter++;
	}

	if (bFound && spawnPtr) {
		m_propSpawnPointCfg.SetSpawnPointCfgName(spawnPtr->m_spawnPointName);
		m_propSpawnPointCfg.SetPositionX(spawnPtr->m_position.x);
		m_propSpawnPointCfg.SetPositionY(spawnPtr->m_position.y);
		m_propSpawnPointCfg.SetPositionZ(spawnPtr->m_position.z);
		m_propSpawnPointCfg.SetZoneIndex(spawnPtr->m_zoneIndex.zoneIndex());
		m_propSpawnPointCfg.SetZoneInstanceTypeID(spawnPtr->m_zoneIndex.GetType());
		m_propSpawnPointCfg.SetRotation(spawnPtr->m_rotation);
	} else {
		m_propSpawnPointCfg.Reset();
	}

	m_propSpawnPointCfg.UpdateData();

	CRect l_rect;
	m_propSpawnPointCfg.GetWindowRect(l_rect);
	ScreenToClient(l_rect);
	RedrawWindow(&l_rect, 0, RDW_INVALIDATE | RDW_ALLCHILDREN);
}

void CCurrentWorldsDlg::OnSelchangeLISTWorldList() {
	int worldSel = m_worldList.GetCurSel();
	if (m_previousSelCurrentWorld != -1) {
		ReplaceCurrentWorld(m_previousSelCurrentWorld);
	}
	if (worldSel == -1) {
		AfxMessageBox(IDS_MSG_SELZONE);
		return;
	}

	//put back user selection
	m_worldList.SetCurSel(worldSel);
	m_previousSelCurrentWorld = worldSel;
	SetWorld(worldSel);
}

void CCurrentWorldsDlg::SetWorldList() {
	int worldIndex = m_worldList.GetCurSel();
	if (worldIndex != -1) {
		SetWorld(worldIndex);
	}
}

void CCurrentWorldsDlg::SetWorld(int worldSel) {
	CWorldAvailableObj* wPtr = GL_multiplayerObject->m_currentWorldsUsed->GetByIndex(m_worldList.GetItemData(worldSel));
	if (wPtr) {
		m_propWorld.SetWorldName(wPtr->m_worldName);
		m_propWorld.SetDisplayName(wPtr->m_displayName);
		m_propWorld.SetWorldID(wPtr->m_channel.toLong());
		m_propWorld.SetMaxOccupancy(wPtr->m_maxOccupancy);
		m_propWorld.SetVisibility(wPtr->m_visibility);
		m_propWorld.SetZoneIndex(wPtr->m_index);

		m_propWorld.UpdateData();

		CRect l_rect;
		m_propWorld.GetWindowRect(l_rect);
		ScreenToClient(l_rect);
		RedrawWindow(&l_rect, 0, RDW_INVALIDATE | RDW_ALLCHILDREN);
	}
}

//-----------END FUNCTION--------------------------------------//
//-------------------------------------------------------------//
//-----converts a long into string-----------------------------//
CStringA CCurrentWorldsDlg::ConvToString(int number) {
	if (number == 0)
		return "0";

	int decimal, sign;
	char buffer[_CVTBUFSIZE];
	CStringA temp;
	_fcvt_s(buffer, _CVTBUFSIZE, number, 0, &decimal, &sign);
	temp = buffer;
	if (sign != 0)
		temp = operator+("-", temp);

	return temp;
}

void CCurrentWorldsDlg::ReplaceCurrentWorld(int worldSel) {
	//apply changes to the current selection
	UpdateData(TRUE);

	if (worldSel < 0)
		return;
	int index = m_worldList.GetItemData(worldSel);

	CWorldAvailableObj* wPtr = GL_multiplayerObject->m_currentWorldsUsed->GetByIndex(index);
	if (wPtr) {
		wPtr->m_worldName = m_propWorld.GetWorldName();
		wPtr->m_displayName = m_propWorld.GetDisplayName();
		wPtr->m_channel.fromLong(m_propWorld.GetWorldID());
		wPtr->m_maxOccupancy = m_propWorld.GetMaxOccupancy();
		wPtr->m_visibility = m_propWorld.GetVisibility();
	}
	UpdateData(FALSE);
	InListCurrentWorlds(worldSel);
}
void CCurrentWorldsDlg::ReplaceSpawnPointCfg(int spawnPointIndex) {
	//apply changes to the current selection
	CSpawnCfgObj* spawnPtr = GL_multiplayerObject->m_spawnPointCfgs->FindId(spawnPointIndex);
	if (spawnPtr) {
		//CSpawnCfgObj * spawnPtr = GL_multiplayerObject->GetSpawnPointCfg(worldIndex, spawnPointIndex);
		UpdateData(TRUE);
		spawnPtr->m_spawnPointName = m_propSpawnPointCfg.GetSpawnPointCfgName();
		spawnPtr->m_position.x = m_propSpawnPointCfg.GetPositionX();
		spawnPtr->m_position.y = m_propSpawnPointCfg.GetPositionY();
		spawnPtr->m_position.z = m_propSpawnPointCfg.GetPositionZ();
		spawnPtr->m_zoneIndex = ZoneIndex(m_propSpawnPointCfg.GetZoneIndex(), 0, m_propSpawnPointCfg.GetZoneInstanceTypeID());
		spawnPtr->m_rotation = m_propSpawnPointCfg.GetRotation();
		UpdateData(FALSE);
	}
	InListCurrentSpawnCfg(spawnPointIndex);
}
