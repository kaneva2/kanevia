///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IKEPFile.h"
#include <stdio.h>

namespace KEP {

#define KEPFILE_BUFFSIZE 2048
// wrapper over a file object
class KEPFile : public IKEPFile {
public:
	KEPFile();
	virtual ~KEPFile();

	bool Open(const char* fname, const char* access, std::string& errMsg);
	virtual bool ReadLine(std::string& line, std::string& errMsg);
	virtual bool WriteLine(const char* line, std::string& errMsg);
	virtual void CloseAndDelete();

private:
	FILE* m_file;
	char m_buffer[KEPFILE_BUFFSIZE + 1];
};

} // namespace KEP
