///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DecryptingFileFactory.h"
#include "KEPHelpers.h"
#include "UnicodeMFCHelpers.h"

namespace KEP {

class DecryptingFile : public CFile {
public:
	DecryptingFile(const char *key) :
			m_currOffset(0), m_salt(0), m_blockSize(ENCRYPT_MIN_BLOCK_SIZE), m_writeBuffer(0), m_writeBufferLen(0) {
		ASSERT(strlen(key) == ENCRYPT_KEY_LEN);
		memcpy(m_key, key, ENCRYPT_KEY_LEN);
	}
	virtual ~DecryptingFile() {
		if (m_writeBuffer)
			::free(m_writeBuffer);
	}

	virtual UINT Read(void *lpBuf, UINT nCount);
	virtual void Write(const void *lpBuf, UINT nCount);
	virtual BOOL Open(const wchar_t *pwszFileName, UINT nOpenFlags, CFileException *pError = NULL) {
		std::string strFileName = Utf16ToUtf8(pwszFileName);
		m_currOffset = 0;
		m_blockSize = ENCRYPT_MIN_BLOCK_SIZE;
		m_salt = DecryptingFileFactory::GetSalt(strFileName.c_str());
		if (!CFile::Open(pwszFileName, nOpenFlags, pError)) {
			return FALSE;
		}

		m_blockSize = DecryptingFileFactory::GetBlockSize(*this);
		return TRUE;
	}
	virtual ULONGLONG Seek(LONGLONG lOff, UINT nFrom) {
		m_currOffset = CFile::Seek(lOff, nFrom);
		return m_currOffset;
	}

private:
	char m_key[ENCRYPT_KEY_LEN];
	ULONGLONG m_currOffset;
	unsigned char m_salt;
	ULONGLONG m_blockSize;
	unsigned char *m_writeBuffer;
	ULONG m_writeBufferLen;
};

CFile *DecryptingFileFactory::getFile() {
	CFile *ret = new DecryptingFile(m_key);
	return ret;
}

void DecryptingFile::Write(const void *lpBuf, UINT nCount) {
	if (m_writeBufferLen < nCount) {
		m_writeBuffer = (unsigned char *)::realloc(m_writeBuffer, nCount);
		m_writeBufferLen = nCount;
	}
	if (m_writeBuffer == 0)
		throw new CMemoryException;

	::memcpy(m_writeBuffer, lpBuf, nCount);

	for (unsigned long ii = 0; ii < nCount; ii++) {
		int keyIndex = DecryptingFileFactory::GetKeyIndex(m_currOffset, m_salt, m_blockSize);
		m_writeBuffer[ii] = m_writeBuffer[ii] ^ m_key[keyIndex];
		m_currOffset++;
	}

	CFile::Write(m_writeBuffer, nCount);
}

UINT DecryptingFile::Read(void *buf, UINT nCount) {
	char *buffer = (char *)buf;

	UINT ret = CFile::Read(buffer, nCount);
	if (ret > 0) {
		for (unsigned long ii = 0; ii < ret; ii++) {
			int keyIndex = DecryptingFileFactory::GetKeyIndex(m_currOffset, m_salt, m_blockSize);
			buffer[ii] = buffer[ii] ^ m_key[keyIndex];
			m_currOffset++;
		}
	}
	return ret;
}

} // namespace KEP
