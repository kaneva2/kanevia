///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "FlashEXMesh.h"
#include "CNodeObject.h"
#if (DRF_DIRECT_SHOW_MESH == 1)
#include "DirectShowEXMesh.h"
#endif
#include "UrlParser.h"
#include "common\KEPUtil\Helpers.h"
#include "UnicodeWindowsHelpers.h"
#include "FlashControlProxy.h"
#include "WindowsMixer.h"
#include "Common/KEPUtil/FrameTimeProfiler.h"

namespace KEP {

static LogInstance("ClientEngine");

bool ClientEngine::ConfigureAudio() {
	float fVolume;
	bool bMute;

	{
		std::lock_guard<std::mutex> lock(m_mtxVolume);

		fVolume = m_masterVolPct * .01;
		bMute = m_bMute;

		if (!m_pWindowsMixer)
			m_pWindowsMixer = IWindowsMixerProxy::New();

		if (m_pWindowsMixer) {
			if (m_pWindowsMixer->GetVolume(out(fVolume)))
				m_masterVolPct = fVolume * 100;

			if (m_pWindowsMixer->GetMute(out(bMute)))
				m_bMute = bMute;
#if defined(_DEBUG)
			m_pWindowsMixer->SetLabel("Kaneva (Main process)");
#else
			m_pWindowsMixer->SetLabel("Kaneva");
#endif
			//m_pWindowsMixer->SetIcon("");
			auto MixerVolumeChangeCallback = [this](float fNewVolume, bool bNewMute) {
				double dNewVolumePct = fNewVolume * 100;
				HandleVolumeChange(&dNewVolumePct, &bNewMute, true);
			};
			m_pWindowsMixer->SetVolumeChangeCallback(MixerVolumeChangeCallback);
		}
	}

	auto FlashSystemVolumeChangeCallback = [this](float fNewVolume, bool bNewMute) {
		double dNewVolumePct = fNewVolume * 100;
		HandleVolumeChange(&dNewVolumePct, &bNewMute, false);
	};
	m_pFlashSystem = IFlashSystem::New(FlashSystemVolumeChangeCallback);

	m_pFlashSystem->SetGlobalVolume(fVolume, bMute);

	return true;
}

void ClientEngine::CrashFlashProcess() {
	if (m_pFlashSystem) {
		LogInfo("Forcing a Flash process crash.");
		m_pFlashSystem->CrashFlashProcess();
	}
}

size_t ClientEngine::GetPeakFlashInstanceCount() {
	return m_pFlashSystem->GetPeakFlashInstanceCount();
}

size_t ClientEngine::GetFlashProcessCrashCount() {
	return m_pFlashSystem->GetFlashProcessCrashCount();
}

double ClientEngine::GetMasterVolume() const {
	std::lock_guard<std::mutex> lock(m_mtxVolume);
	return m_masterVolPct;
}

bool ClientEngine::GetMute() const {
	std::lock_guard<std::mutex> lock(m_mtxVolume);
	return m_bMute;
}

bool ClientEngine::SetMute(bool bMute) {
	return HandleVolumeChange(nullptr, &bMute, false);
}

bool ClientEngine::SetMasterVolume(double volPct) {
	return HandleVolumeChange(&volPct, nullptr, false);
}

bool ClientEngine::HandleVolumeChange(double* pdVolPct, bool* pbMute, bool bFromMixer) {
	// Set Master Volume
	bool bChanged = false;

	bool bNewMute;
	double dNewVolumePercent;
	{
		std::lock_guard<std::mutex> lock(m_mtxVolume);

		bNewMute = m_bMute;
		dNewVolumePercent = m_masterVolPct;
		if (pdVolPct) {
			dNewVolumePercent = Clamp(*pdVolPct, 0, 100);
			if (dNewVolumePercent != m_masterVolPct) {
				bChanged = true;
				m_masterVolPct = dNewVolumePercent;
				if (m_pWindowsMixer) {
					if (!bFromMixer)
						m_pWindowsMixer->SetVolume(m_masterVolPct * .01f);
				}
			}
		}
		if (pbMute) {
			bNewMute = *pbMute;
			if (bNewMute != m_bMute) {
				bChanged = true;
				m_bMute = bNewMute;
				if (m_pWindowsMixer) {
					if (!bFromMixer)
						m_pWindowsMixer->SetMute(m_bMute);
				}
			}
		}
	}

	if (bChanged) {
#if (DRF_DIRECT_SHOW_MESH == 1)
		DirectShowMesh::SetVolumeGlobal(dNewVolumePercent);
#endif

		m_pFlashSystem->SetGlobalVolume(dNewVolumePercent * 0.01, bNewMute);

		if (this->m_volumeChangedEventId != BAD_EVENT_ID) {
			IEvent* pEvent = this->m_dispatcher.MakeEvent(this->m_volumeChangedEventId);
			this->m_dispatcher.QueueEvent(pEvent);
		}
	}

	LogInfo("volPct=" << GetMasterVolume());
	return true;
}

bool ClientEngine::SetMediaEnabled(bool enable) {
	// Set Media Mesh Enable Globally
	m_mediaEnabled = enable;
	FlashMesh::SetEnabledGlobal(enable);
#if (DRF_DIRECT_SHOW_MESH == 1)
	DirectShowMesh::SetEnabledGlobal(enable);
#endif

	return true;
}

bool ClientEngine::StopAllMedia(bool andWait) {
	// Stop All Media Placements
	m_dynamicPlacementManager.IterateObjectsWithMedia([](CDynamicPlacementObj* pDPO) {
		pDPO->SetMediaState(MediaParams::STATE_STOPPED);
	});

	// Delete All Media Placements
	return MediaPlacementsDelAll(andWait);
}

std::shared_ptr<IExternalEXMesh> ClientEngine::StartMediaMesh(const MediaParams& mediaParams, bool shareMesh) {
	// Ignore Media Changes While Rezoning
	if (IsMediaRezoning()) {
		//		LogWarn("REZONING - Ignoring ...");
		return NULL;
	}

	// Local Media ?
	MediaParams mp = mediaParams;
	bool isUrlLocal = mp.IsUrlLocal();
	if (isUrlLocal) {
		std::string filePath = PathFind(PathApp(), mp.GetUrl());
		if (!PathExists(filePath, false)) {
			LogError("PathExists() FAILED - '" << filePath << "'");
			return NULL;
		}
		mp.SetUrl(filePath);
	}

	// Share Mesh Already Playing Same Media ?
	std::shared_ptr<IExternalEXMesh> pXM;
	if (shareMesh) {
		pXM = MediaPlacementsFindMesh(mp);
		if (pXM) {
			LogInfo("SHARING " << pXM->ToStr(true));
			return pXM;
		}
	}

	// Only KGPPlaylist Is Secure
	// KGPPlaylist.swf is a local file stored in Star folder indicated during zone customization.
	bool secure = mp.IsUrlKgpPlaylist();

	// Which Player Is Used ?
	// - Flash Is Used For SWF & Youtube
	// - Direct Show Is Used For ASX
	bool useFlash = mp.IsUrlFlash();

	LogInfo((useFlash ? "SWF" : "ASX") << " " << (secure ? "SECURE" : "INSECURE") << " - " << mp.ToStr(true));

	// Flash Installed ?
	if (useFlash && !FlashIsInstalled()) {
		LogError("FlashIsInstalled() FAILED");
		return NULL;
	}

	// Create New Media Mesh
	if (secure) {
		// add auth URL to end of params
		std::string allParams = mp.GetParams();
		std::string fullAuth = m_authUrl;
		if (!fillInAuthUrl(fullAuth)) {
			LogError("fillInAuthUrl() FAILED");
		}
		fullAuth = UrlParser::escapeString(fullAuth);
		allParams += "&authUrl=";
		allParams += fullAuth;
		pXM = FlashEXMesh::Create(m_pFlashSystem.get(), m_pRenderStateMgr, m_pTextureDB, AfxGetMainWnd()->m_hWnd, mp);
	} else {
#if (DRF_DIRECT_SHOW_MESH == 1)
		// DirectShowEXMesh is used for ASX files which are the result of the user selecting a 'TV Channel'
		// From the media selection menu.  This is basically a playlist with static image rendered and streaming
		// audio playing from our servers.  FlashEXMesh is used for SWF files which can be where swfUrl is either
		// a direct youtube url or it is our local 'KEPFlvPlayer.swf' flash script which takes swfParams as the
		// url of the flash resource itself to play.
		if (!useFlash) {
			pXM = DirectShowEXMesh::Create(m_pRenderStateMgr, m_pTextureDB, m_pDM, g_pD3dDevice, mp);
		} else {
			pXM = FlashEXMesh::Create(m_pFlashSystem.get(), m_pRenderStateMgr, m_pTextureDB, AfxGetMainWnd()->m_hWnd, mp);
		}
#else
		pXM = FlashEXMesh::Create(m_pRenderStateMgr, m_pTextureDB, AfxGetMainWnd()->m_hWnd, mp);
#endif
	}

	return pXM;
}

bool ClientEngine::SetMediaMeshMediaParams(int placementId, const MediaParams& mediaParams, bool allowRewind) {
	// Ignore Media Changes While Rezoning
	if (IsMediaRezoning()) {
		//		LogWarn("REZONING - Ignoring ...");
		return false;
	}

	// Set Dynamic Object Media Mesh Volume
	auto pXM = GetMediaMeshOnDynamicObject(placementId);
	if (!pXM)
		return false;
	return pXM->SetMediaParams(mediaParams, allowRewind);
}

bool ClientEngine::StartMediaOnMeshObject(CEXMeshObj* pXMO, CReferenceObj* pRO, const MediaParams& mediaParams) {
	if (!pXMO && !pRO)
		return false;

	if (pXMO)
		pXMO->ClearExternalSource();

	if (pRO)
		pRO->ClearExternalSource();

	// Stop Media On Mesh Object If Currently Playing
	StopMediaOnMeshObject(pXMO, pRO);

	// Start New Media On Mesh Object (don't share mesh)
	std::shared_ptr<IExternalEXMesh> pXM = StartMediaMesh(mediaParams, false);
	if (!pXM) {
		LogError("StartMediaMesh() FAILED - pMO=" << pXMO << " " << mediaParams.ToStr(true));
		return false;
	}

	//if this is a reference object lets do that!
	if (!pXMO && pRO)
		pRO->SetExternalSource(pXM);
	else //normal mesh
		pXMO->SetExternalSource(pXM);

	return true;
}

bool ClientEngine::StopMediaOnMeshObject(CEXMeshObj* meshObject, CReferenceObj* pRO) {
	if (pRO)
		pRO->ClearExternalSource();
	if (meshObject)
		meshObject->ClearExternalSource();
	return true;
}

bool ClientEngine::StartMediaOnWorldObjectMesh(CWldObject* pWO, const MediaParams& mediaParams) {
	if (!pWO)
		return false;
	CReferenceObj* pRO = NULL;
	if (pWO->m_node) {
		pRO = m_libraryReferenceDB->GetByIndex(pWO->m_node->m_libraryReference);
		if (!pRO)
			return false;
	}
	return StartMediaOnMeshObject(pWO->m_meshObject, pRO, mediaParams);
}

bool ClientEngine::StopMediaOnWorldObjectMesh(CWldObject* pWO) {
	if (!pWO)
		return false;
	CReferenceObj* pRO = NULL;
	if (pWO->m_node) {
		pRO = m_libraryReferenceDB->GetByIndex(pWO->m_node->m_libraryReference);
		if (!pRO)
			return false;
	}
	return StopMediaOnMeshObject(pWO->m_meshObject, pRO);
}

bool ClientEngine::StartMediaOnWorldObjectMesh(int objId, const MediaParams& mediaParams) {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	auto pWO = GetWorldObject(objId);
	return (pWO ? StartMediaOnWorldObjectMesh(pWO, mediaParams) : false);
}

bool ClientEngine::StopMediaOnWorldObjectMesh(int objId) {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	auto pWO = GetWorldObject(objId);
	return (pWO ? StopMediaOnWorldObjectMesh(pWO) : false);
}

bool ClientEngine::StartMediaOnWorldObjectMesh(const std::string& name, const MediaParams& mediaParams) {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	if (!m_pWOL)
		return false;
	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) {
		auto pWO = (CWldObject*)m_pWOL->GetNext(posLoc);
		if (!pWO)
			continue;
		if (pWO->m_fileNameRef.CompareNoCase(name.c_str()) == 0)
			return StartMediaOnWorldObjectMesh(pWO, mediaParams);
	}
	return false;
}

bool ClientEngine::StopMediaOnWorldObjectMesh(const std::string& name) {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	if (!m_pWOL)
		return false;
	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) {
		auto pWO = (CWldObject*)m_pWOL->GetNext(posLoc);
		if (!pWO)
			continue;
		if (pWO->m_fileNameRef.CompareNoCase(name.c_str()) == 0)
			return StopMediaOnWorldObjectMesh(pWO);
	}
	return false;
}

bool ClientEngine::StartMediaOnDynamicObject(int placementId) {
	// Ignore Media Changes While Rezoning
	if (IsMediaRezoning()) {
		//		LogWarn("REZONING - Ignoring ...");
		return false;
	}

	// Get Dynamic Object
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Supports Media ?
	if (!pDPO->MediaSupported()) {
		LogError("MediaSupported() FAILED - " << pDPO->ToStr(true));
		return false;
	}

	// Get Dynamic Object Media Params
	MediaParams mediaParams = pDPO->GetMediaParams();

	// Apply Media Mute ?
	if (pDPO->GetMediaMute())
		mediaParams.SetVolume(pDPO->GetMediaVolume());

	// Set Media Params ? (avoids full stop/start)
	bool fullStopStart = true;
	bool isStopped = pDPO->MediaIsStopped();
	if (!isStopped) {
		auto pXM = GetMediaMeshOnDynamicObject(placementId);
		if (pXM) {
			if (mediaParams.IsUrlKgpPlaylist()) {
				LogInfo("PLAYLIST KGP - " << pDPO->ToStr(true));
			} else if (!mediaParams.IsPlaylistSame(pXM->GetMediaParams())) {
				LogInfo("PLAYLIST CHANGE - " << pDPO->ToStr(true));
			} else {
				auto pFM = dynamic_cast<FlashEXMesh*>(pXM);
				bool mediaFlash = mediaParams.IsUrlFlash();
				bool sameType = (pFM && mediaFlash) || (!pFM && !mediaFlash);
				if (sameType) {
					LogInfo("SET MESH PARAMS - " << pDPO->ToStr(true));
					fullStopStart = !SetMediaMeshMediaParams(placementId, mediaParams);
				}
			}
		}
	}

	// Full Stop/Start Required ?
	if (fullStopStart) {
		// Stop Media
		StopMediaOnDynamicObject(placementId);

		// Get Skeletal Mesh Object (base object contains actual mesh where flash plays on)
		LogInfo("START - " << pDPO->ToStr(true));

		// Start New Media On Dynamic Object (share mesh)
		std::shared_ptr<IExternalEXMesh> pXM = StartMediaMesh(mediaParams, true);
		if (!pXM) {
			LogError("StartMediaMesh() FAILED - " << pDPO->ToStr(true));
			return false;
		}

		// Attach Flash Mesh To Dynamic Object
		MediaPlacementsSetMesh(placementId, std::move(pXM));
	}

	// Finally Set Media State Playing
	pDPO->SetMediaState(MediaParams::STATE_PLAYING);

	return true;
}

bool ClientEngine::StopMediaOnDynamicObject(int placementId) {
	// Get Dynamic Object
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Supports Media ?
	if (!pDPO->MediaSupported()) {
		LogError("MediaSupported() FAILED - " << pDPO->ToStr(true));
		return false;
	}

	// Get Skeletal Mesh Object (base object contains actual mesh where flash plays on)
	LogWarn("STOP - " << pDPO->ToStr(true));

	// Clear Flash Mesh From Dynamic Object
	MediaPlacementsSetMesh(placementId, NULL);

	// Finally Set Media State Stopped
	pDPO->SetMediaState(MediaParams::STATE_STOPPED);

	return true;
}

IExternalEXMesh* ClientEngine::GetMediaMeshOnDynamicObject(int placementId) {
	return MediaPlacementsGetMesh(placementId);
}

bool ClientEngine::GetMediaParamsOnDynamicObject(int placementId, MediaParams& mediaParams) {
	// Get Media Params On Dynamic Object
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}
	mediaParams = pDPO->GetMediaParams();
	return true;
}

bool ClientEngine::SetMediaParamsOnDynamicObject(int placementId, const MediaParams& mediaParams) {
	// Set Media Params On Dynamic Object
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}
	pDPO->SetMediaParams(mediaParams);
	return true;
}

bool ClientEngine::SetMediaVolumeOnDynamicObject(int placementId, double volPct) {
	// Set Media Volume On Dynamic Object
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}
	return pDPO->SetMediaVolume(volPct);
}

bool ClientEngine::GetMediaVolumeOnDynamicObject(int placementId, double& volPct) {
	// Get Media Volume On Dynamic Object
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		volPct = 0;
		return false;
	}
	volPct = pDPO->GetMediaVolume();
	return true;
}

bool ClientEngine::SetMediaVolumeOnDynamicObjectMediaMesh(int placementId, double volPct) {
	LogInfo("placementId=" << placementId << " volPct=" << volPct);

	// Set Dynamic Object Media Mesh Volume
	auto pXM = GetMediaMeshOnDynamicObject(placementId);
	if (!pXM)
		return false;
	return pXM->SetVolume(volPct);
}

bool ClientEngine::GetMediaVolumeOnDynamicObjectMediaMesh(int placementId, double& volPct) {
	// Get Dynamic Object Media Mesh Volume
	auto pXM = GetMediaMeshOnDynamicObject(placementId);
	if (!pXM) {
		volPct = 0;
		return false;
	}
	volPct = pXM->GetVolume();
	return true;
}

bool ClientEngine::SetMediaRadiusOnDynamicObject(int placementId, const MediaRadius& radius) {
	// Set Media Radius On Dynamic Object
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}
	pDPO->SetMediaRadius(radius);
	return true;
}

bool ClientEngine::GetMediaRadiusOnDynamicObject(int placementId, MediaRadius& radius) {
	// Get Media Volume On Dynamic Object
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		radius = 0;
		return false;
	}
	radius = pDPO->GetMediaRadius();
	return true;
}

bool ClientEngine::SetMediaMuteOnDynamicObject(int placementId, bool mute) {
	// Get Dynamic Object
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}
	return pDPO->SetMediaMute(mute);
}

bool ClientEngine::GetMediaMuteOnDynamicObject(int placementId, bool& mute) {
	// Get Dynamic Object
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}
	mute = pDPO->GetMediaMute();
	return true;
}

bool ClientEngine::GetMediaRangeOnDynamicObject(int placementId, MediaRange& mediaRange, bool logIt) {
	// Get Dynamic Object
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Supports Media ?
	if (!pDPO->MediaSupported())
		return false;

	// Set Media Range Radius
	MediaRadius mediaRadius = pDPO->GetMediaRadius();
	mediaRange.SetRadius(mediaRadius);

	// Set Media Range Distance
	double distance = 0;
	GetDynamicObjectDistanceToPlayer(placementId, distance);
	mediaRange.SetDistance(distance);

	// Log Local Media Player Range
	if (pDPO->IsMediaLocal() && logIt) {
		LogInfo("placementId=" << placementId << " " << mediaRange.ToStr());
	}

	return true;
}

bool ClientEngine::UpdateMediaOnDynamicObject(int placementId, bool logIt) {
	// Get Dynamic Object
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Supports Media ?
	if (!pDPO->MediaSupported()) {
		LogError("MediaSupported() FAILED - placementId=" << placementId);
		return false;
	}

	// Get Media Range On Dynamic Object (audio & video)
	bool inRangeAudio = false;
	bool inRangeVideo = false;
	bool isValid = m_mediaEnabled && pDPO->ValidMediaUrlAndParams();
	if (isValid) {
		MediaRange mediaRange;
		GetMediaRangeOnDynamicObject(placementId, mediaRange, logIt);
		inRangeAudio = mediaRange.InRangeAudio();
		inRangeVideo = mediaRange.InRangeVideo();
	}

	// Set Media In Range Flags (reset in MediaPlacementsUpdate())
	bool isGlobal = pDPO->IsMediaGlobal();
	m_isMediaGlobalInRangeAudio |= (isGlobal && inRangeAudio);
	m_isMediaGlobalInRangeVideo |= (isGlobal && inRangeVideo);
	m_isMediaLocalInRangeAudio |= (!isGlobal && inRangeAudio);
	m_isMediaLocalInRangeVideo |= (!isGlobal && inRangeVideo);

	// Apply Video Range Trigger (stop video out of range)
	bool isChanged = pDPO->MediaIsChanged();
	if (inRangeVideo) {
		if (pDPO->MediaIsStopped() || isChanged)
			StartMediaOnDynamicObject(placementId);
	} else {
		if (pDPO->MediaIsPlaying() || isChanged)
			StopMediaOnDynamicObject(placementId);
	}

	// Apply Audio Range Trigger (mute audio out of range)
	double volMult = isGlobal ? GetGlobalMediaVolMult() : GetLocalMediaVolMult();
	SetMediaMuteOnDynamicObject(placementId, !inRangeAudio);
	if (pDPO->MediaIsPlaying()) {
		double volPctDO = 0.0, volPctMesh = 0.0;
		if (GetMediaVolumeOnDynamicObject(placementId, volPctDO) && GetMediaVolumeOnDynamicObjectMediaMesh(placementId, volPctMesh) && ((volPctDO * volMult) != volPctMesh)) {
			SetMediaVolumeOnDynamicObjectMediaMesh(placementId, volPctDO * volMult);
		}
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Flash Only Functions (Not DirectShow)
///////////////////////////////////////////////////////////////////////////////

bool ClientEngine::FlashIsInstalled() {
	// Flash Installed ?
	if (m_pFlashSystem && m_pFlashSystem->IsFlashInstalled())
		return true;

	LogWarn("FLASH NOT INSTALLED - Disabling Media ...");

	// Disable Media
	SetMediaEnabled(false);
	return false;
}

IExternalMesh* ClientEngine::CreateFlashMesh(
	HWND parent,
	const MediaParams& mediaParams,
	uint32_t width, uint32_t height,
	bool loop,
	bool wantInput,
	int32_t x, int32_t y) {
	return FlashMesh::Create(m_pFlashSystem.get(), parent, mediaParams, width, height, loop, wantInput, x, y);
}

bool ClientEngine::GetFlashMovieSize(const std::string& filePath, uint32_t& width, uint32_t& height) {
	return FlashMesh::GetMovieSize(filePath, width, height);
}

///////////////////////////////////////////////////////////////////////////////
// Media Placements Functions
///////////////////////////////////////////////////////////////////////////////
size_t ClientEngine::MediaPlacements() const {
	size_t count = 0;
	m_dynamicPlacementManager.IterateObjectsWithMedia([&count](const CDynamicPlacementObj* pDPO) {
		++count;
	});
	return count;
}

void ClientEngine::MediaPlacementsLog(bool verbose) const {
	LogInfo("placements=" << MediaPlacements());
	if (!verbose)
		return;
	m_dynamicPlacementManager.IterateObjectsWithMedia([&](const CDynamicPlacementObj* pDPO) {
		IExternalEXMesh* pXM = pDPO->GetExternalMesh();
		if (!pXM)
			return;
		bool duped = (m_mediaDupeObjId == pDPO->GetPlacementId());
		bool playing = pXM->GetMediaParams().IsPlaying();
		std::string state = (duped ? " *** " : (playing ? " >>> " : " ... "));
		_LogInfo(state << pXM->ToStr(false) << " " << pDPO->ToStr());
	});
}

IExternalEXMesh* ClientEngine::MediaPlacementsGetMesh(int placementId) {
	CDynamicPlacementObj* pDPO = m_dynamicPlacementManager.LookUpByPlacementId(placementId);
	if (!pDPO)
		return nullptr;
	return pDPO->GetExternalMesh();
}

bool ClientEngine::MediaPlacementsSetMesh(int placementId, std::shared_ptr<IExternalEXMesh> pXM) {
	CDynamicPlacementObj* pDPO = m_dynamicPlacementManager.LookUpByPlacementId(placementId);
	if (!pDPO)
		return false;

	LogInfo("placementId=" << placementId << " XM<" << pXM << "> -> " << (pXM ? pXM->ToStr(false) : ""));
	pDPO->SetExternalMesh(std::move(pXM));
	MediaPlacementsLog(AppDevMode() != 0);

	return true;
}

std::shared_ptr<IExternalEXMesh> ClientEngine::MediaPlacementsFindMesh(const MediaParams& mediaParams) {
	// Mesh With Same Media Already Playing ?
	std::shared_ptr<IExternalEXMesh> pXM_found;
	m_dynamicPlacementManager.IterateObjectsWithMedia([&mediaParams, &pXM_found](CDynamicPlacementObj* pDPO) {
		IExternalEXMesh* pXM = pDPO->GetExternalMesh();
		if (!pXM)
			return true; // continue iteration
		if (mediaParams.IsUrlAndParamsSame(pXM->GetMediaParams())) {
			pXM_found = pDPO->GetExternalMeshShared();
			LogInfo("FOUND " << pXM_found->ToStr(true));
			return false; // Done iterating
		}
		return true;
	});

	return pXM_found;
}

size_t ClientEngine::MediaPlacementsRefsMesh(IExternalEXMesh* pXM) {
	// Count References Of Media Mesh
	size_t refs = 0;
	m_dynamicPlacementManager.IterateObjectsWithMedia([&refs, pXM](CDynamicPlacementObj* pDPO) {
		if (pXM == pDPO->GetExternalMesh())
			++refs;
	});
	return refs;
}

bool ClientEngine::MediaPlacementsDel(int placementId, bool andWait) {
	CDynamicPlacementObj* pDPO = m_dynamicPlacementManager.LookUpByPlacementId(placementId);
	if (!pDPO)
		return false;

	IExternalEXMesh* pXM = MediaPlacementsGetMesh(placementId);
	LogInfo("placementId=" << placementId << " " << (pXM ? pXM->ToStr(false) : ""));
	pDPO->SetExternalMesh(nullptr);
	return true;
}

bool ClientEngine::MediaPlacementsDelAll(bool andWait) {
	// Copy list of objects with media because we can't modify the list while iterating.
	std::vector<CDynamicPlacementObj*> apAllDynObjWithMedia;
	m_dynamicPlacementManager.IterateObjectsWithMedia([&apAllDynObjWithMedia](CDynamicPlacementObj* pDPO) {
		apAllDynObjWithMedia.push_back(pDPO);
	});

	LogInfo("placements=" << apAllDynObjWithMedia.size());
	for (CDynamicPlacementObj* pDPO : apAllDynObjWithMedia) {
		pDPO->SetExternalMesh(nullptr);
	}
	return true;
}

bool ClientEngine::MediaPlacementsUpdate() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "MediaPlacementsUpdate");

	// Log Every 5 Seconds (debug only)
	bool logIt = false;
#if 0 //def _DEBUG
	static Timer timer;
	if (timer.ElapsedMs() > (5 * MS_PER_SEC)) {
		timer.Reset();
		logIt = true;
	}
#endif

	// ED-4317 - Silence Global Media While Local Media In Range Audio
	SetGlobalMediaVolMult(m_isMediaLocalInRangeAudio ? 0.0 : 1.0);

	// Reset Media In Range Flags
	m_isMediaGlobalInRangeAudio = false;
	m_isMediaGlobalInRangeVideo = false;
	m_isMediaLocalInRangeAudio = false;
	m_isMediaLocalInRangeVideo = false;

	// Ignore Media Changes While Rezoning
	if (IsMediaRezoning())
		return false;

	// Update All Media Placements (sets media in range flags)
	m_dynamicPlacementManager.IterateObjectsWithMedia([this, logIt](CDynamicPlacementObj* pDPO) {
		int objId = pDPO->GetPlacementId();
		if (!UpdateMediaOnDynamicObject(objId, logIt))
			MediaPlacementsDel(objId, false);
	});

	return true;
}

// ED-3195 - Sync Playlist Across All Media Placements
bool ClientEngine::MediaPlacementsPlaylistSync(const MediaParams& mediaParams) {
	// Sync All Media Placements Sharing Playlist
	m_dynamicPlacementManager.IterateObjectsWithMedia([&mediaParams](CDynamicPlacementObj* pDPO) {
		const MediaParams& mp = pDPO->GetMediaParams();
		bool playlistSame = mediaParams.IsPlaylistSame(mp);
		bool urlAndParamsSame = mediaParams.IsUrlAndParamsSame(mp);
		if (playlistSame && !urlAndParamsSame)
			pDPO->SetMediaURLandParams(mediaParams.GetUrl(), mediaParams.GetParams());
	});

	return true;
}

} // namespace KEP