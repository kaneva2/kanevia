///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define D3D_OVERLOADS
#define INITGUID
#include <d3d9.h>
#include <d3dX9.h>

#include "ClientEngine_CFrameWnd.h"

#include "IClientEngine.h"

#include "Engine/ClientBase/ClientBaseEngine.h"
#include "Engine/ClientBlades/ClientBladeEngine.h"

#include <mmsystem.h>
#include <cstdlib>

#include "common\include\Rect.h"

#include "IMenuBlade.h"

#include "Common/KEPUtil/URL.h"
#include "Core/Util/HighPrecisionTime.h"

#include "CWldObject.h"
#include "CMovementObj.h"
#include "CCameraClass.h"
#include "RuntimeCameras.h"
#include "ViewPortClass.h"
#include "SceneOptions.h"
#include "CBackgroundClass.h"
#include "CEnvironmentClass.h"
#include "CTextClass.h"
#include "CPersistClass.h"
#include "CMovieRuntimeClass.h"
#include "ControlsClass.h"
#include "PendingControlSystem.h"
#include "AIclass.h"
#include "CMissileClass.h"
#include "CAttributeClass.h"
#include "CPlayerClass.h"
#include "CEXScriptClass.h"
#include "CInventoryClass.h"
#include "CEXMeshClass.h"
#include "TextureDatabase.h"
#include "C2DVectorDrawClass.h"
#include "EnvParticleSystem.h"
#include "CFrameObj.h"
#include "CCollisionClass.h"
#include "CLightTrackerClass.h"
#include "textureObj.h"
#include "CAITraceClass.h"

#include <afxcmn.h>
#include "CSpawnClass.h"
#include "CWorldGroupClass.h"
#include "CSkeletonObject.h"
#include "CSSoundRange.h"
#include "CCharacterDefinitionsClass.h"
#include "CResourceClass.h"
#include "CTextReadoutClass.h"
#include "CTextRenderClass.h"
#include "CGFONTCLASS.h"
#include "CAIWebClass.h"
#include "CAIScriptClass.h"

#include "MenuGameObjects.h"
#include "MenuList.h"
#include "CStructuresMisl.h"
#include "CExplosionClass.h"
#include "CTranslucentPolyCache.h"
#include "CObjectLibraryClass.h"
#include "CLightningEffectClass.h"
#include "CStateManagementClass.h"
#include "CLaserClass.h"
#include "CSkillClass.h"
#include "CReferenceLibrary.h"
#include "CSoundManagerClass.h"
#include "CGlobalInventoryClass.h"
#include "CCurrencyClass.h"
#include "CCommerceClass.h"
#include "CBankClass.h"
#include "CSafeZones.h"
#include "CSecureTradeClass.h"
#include "CRTLightClass.h"
#include "CStatClass.h"
#include "CFilePatchClass.h"
#include "CScreenFlashClass.h"
#include "CWaterZoneClass.h"
#if (DRF_OLD_PORTALS == 1)
#include "CPortalSysClass.h"
#endif
#include "CCompressionClass.h"
#include "CFilterSpeechClass.h"
#include "CHousingClass.h"
#include "CDynamicOverlay.h"
#include "DynamicObj.h"
#include "SoundPlacement.h"
#include "CGmPageClass.h"
#include "CBattleSchedulerClass.h"
#include "CWorldChannelRules.h"
#include "CShaderLibClass.h"
#include "CGlobalMaterialShaderLib.h"
#include "CFloraGenClass.h"
#include "CpendingCommandsClass.h"
#include "CReferenceImporterExporter.h"
#include "IKEPDialogProcessor.h"
#include "ProgressStatus.h"
#include "ContentService.h"
#include "TaskSystem.h"
#include "TaskObject.h"
#include "RenderStats.h"
#include "DynamicObjectSpawnMonitor.h"

#include "CMultiplayerClass.h"

#include "ClientStrings.h" // common string res file

#include "TextCommands.h"
#include "Engine\Blades\BladeFactory.h"

#include "InventoryWeb.h"
#include "wkgtypes.h"
#include "IKEPDropTarget.h"
#include "KepWidgets.h"

#include "DXInputSystem.h"

#include "WokwebHelper.h"

#include "SATFramework.h"
#include "Core/Math/Plane.h"

#include "ClientMetrics.h"

#include "common\include\glAdjust.h"

#include "common\include\IAssetsDatabase.h"

enum CR_SCREENSHOT;

namespace KEP {
class IExternalEXMesh;
class SceneMultipassOptions;
class ScenePassOptions;
class DownloadManager;
class UploadManager;
class TextureRM;
class Grid;
class IAssetsDatabase;
class IAssetImporterFactory;
class IAssetImporter;
struct KeyEventHandlerArgs;
class CBackGroundObjList;
class CIconSysList;
class CLoadingProgressDlg;
class CMenuGameObject;
class CWldObject;
class ReD3DX9DeviceState;
class InputSystem;
class IWindowsMixerProxy;
class IFlashSystem;
struct OffscreenRenderTask;
class OffscreenWorldRenderer;
enum class WorldRenderStyle;
enum class WorldRenderOptions;
class FrameTimeProfiler;
class MeshRM;
class AnimationRM;
class SATFramework;
class SkeletonRM;
template <typename T>
struct PrimitiveGeomArgs;
class FlashEXMesh;

namespace LWG {
	class IGeom;
} // namespace LWG

class ClientEngine : public ClientEngine_CFrameWnd,
					 public ClientBaseEngine,
					 public ClientBladeEngine,
					 public ProgressUpdater,
					 public IKEPDropTarget,
					 public GetSet {
	template <ScriptClientEvent::EventType eventType>
	friend struct SCE_ClientHandler;

	ClientEngine();

public:
	static ClientEngine* Instance() {
		static ClientEngine* s_pCE = nullptr;
		if (!s_pCE)
			s_pCE = new ClientEngine();
		return s_pCE;
	}

	// DRF - Never Called!
	virtual ~ClientEngine();

	void Init();

	bool Startup();
	bool Shutdown();

	void setRuntimeId(const std::string& runtimeId) {
		m_runtimeId = runtimeId;
	}
	std::string getRuntimeId() const {
		return m_runtimeId;
	}

	bool LoadNetworkCfg();
	bool CreateGAInstTrkLua();
	bool CreateUrlsLua();

	virtual bool SetD3dDevice() override;

	virtual bool AppReadStartCfgFile(const IKEPConfig& cfg) override;
	virtual bool AppWriteStartCfgFile(IKEPConfig& cfg) override;

	//---------------------------------------------------------------------
	// DRF - App API
	//---------------------------------------------------------------------
	std::string AppVersion() const;
	std::string AppPath() const;
	std::string AppStarPath() const;
	std::string AppStarId() const;
	std::string AppWokUrl() const;
	std::string AppPatchUrl() const;
	std::string AppLoginEmail() const;
	std::string AppLoginPassword() const;
	std::string AppTTLang() const;
	bool AppIsProduction() const;
	DEV_MODE AppDevMode() const;
	int AppTestGroup() const;
	int AppRipperDetected() const;
	bool AppSetStateStopped() const;

	// Events and dispatchers
	IDispatcher* GetDispatcher() {
		return &m_dispatcher;
	}
	bool InitEvents();

	EVENT_ID RegisterNamedEvent(const std::string& eventName, EVENT_PRIORITY pri = EP_NORMAL);

	bool RegisterVMsEvents(IScriptVM* vm, const char* filename);
	bool UnRegisterVMsEvents(IScriptVM* vm);

	EVENT_ID GetControlsUpdatedEventID() const {
		return m_controlsUpdatedEventId;
	}
	EVENT_ID GetDownloadZoneEventId() const {
		return m_downloadZoneEventId;
	}
	EVENT_ID GetDownloadLooseTextureEventId() const {
		return m_downloadLooseTextureEventId;
	}
	EVENT_ID GetRequestSoundItemDataId() const {
		return m_requestSoundItemDataId;
	}
	EVENT_ID GetUpdateSoundUGCEventId() const {
		return m_updateSoundUGCEventId;
	}
	EVENT_ID GetDialogEventId() const {
		return m_dialogEventId;
	}
	EVENT_ID GetItemsMetadataResultEventId() const {
		return m_itemsMetadataResultEventId;
	}

	virtual CMovementObjList* GetMovementObjectRefModelList() override {
		return m_movObjRefModelList;
	}
	virtual CMovementObjList* GetMovementObjectList() override {
		return m_movObjList;
	}
	virtual CReferenceObjList* GetReferenceLibrary() override {
		return m_libraryReferenceDB;
	}
	virtual ControlDBList* GetControlsList() override {
		return m_controlsList;
	}
	virtual CMovementObjList* GetMovementObjectShadowList() override {
		return m_shadowEntityList;
	}
	virtual CSkillObjectList* GetSkillDatabase() override {
		return m_skillDatabase;
	}
	virtual CMissileObjList* GetProjectiles() override {
		return m_missileDB;
	}
	virtual CExplosionObjList* GetExplosions() override {
		return m_explosionDBList;
	}
	virtual CCameraObjList* GetCameraList() override {
		return m_pCameraList;
	}

	bool CrashReportSilent() const {
		return (m_crashReportSilent == TRUE);
	}
	CR_SCREENSHOT CrashReportScreenshot() const {
		return m_crashReportScreenshot;
	}

	//---------------------------------------------------------------------
	// DRF - Log API
	//---------------------------------------------------------------------
	void LogMemory() const {
		ClientEngine_CFrameWnd::LogMemory();
	}
	void LogWebCalls() const;
	void LogResources(int resources);
	void LogDynamicObjectList(bool verbose);
	void LogMovementObjectList(bool verbose);
	void LogMetrics() {
		MetricsLog();
	}
	void LogTriggers();

	//---------------------------------------------------------------------
	// DRF - WebCall API
	// Provides both asynchronous and synchronous single thread low priority
	// webcalls using the WebCaller given by id.  If no WebCaller is found
	// with matching id a new one is created.
	//---------------------------------------------------------------------
	bool WebCallDisabled(const std::string& id = CLIENTENGINE_WEBCALLER_ID);
	bool WebCall(const std::string& url, const std::string& id = CLIENTENGINE_WEBCALLER_ID);
	bool WebCallAndWaitOk(const std::string& url, const std::string& id = CLIENTENGINE_WEBCALLER_ID);

	//---------------------------------------------------------------------
	// DRF - ClientMetrics API
	// Provides basic metrics accounting for the client.
	// The ProgressMenu disables metrics accounting while open.
	//---------------------------------------------------------------------
	ClientMetrics m_metrics;
	void MetricsReset() {
		m_metrics.Reset();
	}
	void MetricsEnable(bool enable) {
		m_metrics.Enable(enable);
	}
	void MetricsLog() {
		m_metrics.Log();
	}
	void MetricsRenderLoop() {
		m_metrics.RenderLoop();
	}
	bool MetricsFpsReset(ClientMetrics::FPS_ID fpsId) {
		return m_metrics.FpsReset(fpsId);
	}
	bool MetricsFps(ClientMetrics::FPS_ID fpsId, double& fps) const {
		return m_metrics.Fps(fpsId, fps);
	}

	float RenderMetricsFps(bool filter) const {
		return RenderMetrics::GetFramesPerSec(filter);
	}

	ULONGLONG GetAllTextureMemoryUsage() const {
		return CTextureEX::GetAllTextureMemoryUsage();
	}

	// DRF - Sends System Service Event To User's Client (causes forced crash delivery of logs, etc)
	bool SendSystemServiceEvent(const std::string& userId) {
		return ClientBaseEngine::SendSystemServiceEvent(userId);
	}

	// DRF - Added
	std::string Encrypt(const std::string& s) const {
		return KEP_Encrypt(s);
	}
	std::string Decrypt(const std::string& s) const {
		return KEP_Decrypt(s);
	}

	int GetGameId() const {
		return m_gameId;
	}
	int GetPrevGameId() const {
		return m_prevGameId;
	}
	int GetParentGameId() const {
		return m_parentGameId;
	}
	int GetZoneInstanceId() const {
		return m_zoneInstanceId;
	}

	ZoneIndex GetZoneIndex() const {
		return ClientBaseEngine::GetZoneIndex();
	}
	virtual ULONG GetZoneIndexAsULong() const {
		return ClientBaseEngine::GetZoneIndexAsULong();
	}
	virtual ULONG GetZoneIndexType() const {
		return ClientBaseEngine::GetZoneIndexType();
	}
	virtual std::string GetZoneFileName() {
		return ClientBaseEngine::GetZoneFileName();
	}

	virtual void SetZoneFileName(const std::string& zoneFileName) {
		ClientBaseEngine::SetZoneFileName(zoneFileName);
	}

	bool WorldPassGroupAP() {
		return ClientBaseEngine::WorldPassGroupAP();
	}
	bool MyPassGroupAP() {
		return ClientBaseEngine::MyPassGroupAP();
	}
	bool MyPassGroupVIP() {
		return ClientBaseEngine::MyPassGroupVIP();
	}
	bool MyPassGroupGM() {
		return ClientBaseEngine::MyPassGroupGM();
	}
	bool MyPassGroupOWN() {
		return ClientBaseEngine::MyPassGroupOWN();
	}
	bool MyPassGroupMOD() {
		return ClientBaseEngine::MyPassGroupMOD();
	}

	///////////////////////////////////////////////////////////////////////
	// Sync Arm/Disarm Functions
	// MovementObj=NULL Assumes Local Avatar
	// GetSet Variants Are For Script Glue Pointer Conversion Only
	///////////////////////////////////////////////////////////////////////

	virtual std::vector<GLID> GetPlayerArmedItems(CMovementObj* pMO = NULL) override;
	virtual std::vector<GLID> GetPlayerArmedItemsGS(IGetSet* pGS_MO = NULL) override {
		return GetPlayerArmedItems(dynamic_cast<CMovementObj*>(pGS_MO));
	}

	// Sync Arm Functions
	bool ArmInventoryItem_MO(const GLID& glid, CMovementObj* pMO = NULL) override;
	bool ArmInventoryItem_GS_MO(const GLID& glid, IGetSet* pGS_MO = NULL) override {
		return ArmInventoryItem_MO(glid, dynamic_cast<CMovementObj*>(pGS_MO));
	}
	bool ArmEquippableItem_MO(const GLID& glid, CMovementObj* pMO = NULL, int material = 0) override;
	bool ArmEquippableItem_GS_MO(const GLID& glid, IGetSet* pGS_MO = NULL, int material = 0) override;
	bool ArmEquippableItem_GS_MGO(const GLID& glid, IGetSet* pGS_MGO = NULL, int material = 0) override {
		auto pMGO = dynamic_cast<CMenuGameObject*>(pGS_MGO);
		CMovementObj* pMO = pMGO ? pMGO->GetMovementObject() : nullptr;
		return ArmEquippableItem_MO(glid, pMO, material);
	}
	bool ArmEquippableItem_DPO(const GLID& glid, CDynamicPlacementObj* pDPO, const boost::optional<std::string>& strBoneOverride) override;

	// Sync Disarm Functions
	bool DisarmInventoryItem_MO(const GLID& glid, CMovementObj* pMO = NULL) override;
	bool DisarmInventoryItem_GS_MO(const GLID& glid, IGetSet* pGS_MO = NULL) override {
		return DisarmInventoryItem_MO(glid, dynamic_cast<CMovementObj*>(pGS_MO));
	}
	bool DisarmEquippableItem_MO(const GLID& glid, CMovementObj* pMO = NULL) override;
	bool DisarmEquippableItem_GS_MO(const GLID& glid, IGetSet* pGS_MO = NULL) override;
	bool DisarmEquippableItem_DPO(const GLID& glid, CDynamicPlacementObj* pDPO) override;

	virtual Monitored::DataPtr monitor() const {
		return Monitored::DataPtr(new Monitored::Data());
	}

	// Compensating initialization. Split from main line of OnCreate so that the Editor can use the same code.
	void AutoExecInitAndParseCommandLine(HWND hwndLoc = NULL);

	bool ParseCommandLine();

	virtual void ZoneLoaded(const char* _exg, const char* _url, bool reloading);

	bool IsShutdown() {
		return m_shutdown;
	}

	// DRF - Added
	void ShutdownResourceManagers();

	// helper for handling effect track processing
	void ProcessEffectTrack(EffectTrack& effectTrack, void* const entity, IEvent* const sce, int objectId, int numberOfEffects, int numberOfStr);

	// DRF - Script Client Event Handlers
	bool SCE_Handler(ScriptClientEvent* sce);
	void SCE_ScriptError(ScriptClientEvent* sce);
	void SCE_PlayerTeleport(ScriptClientEvent* sce);
	void SCE_PlayerTell(ScriptClientEvent* sce);
	void SCE_PlayerSetEquippedItemAnimation(ScriptClientEvent* sce);
	void SCE_PlayerSetAnimation(ScriptClientEvent* sce);
	void SCE_PlayerDefineAnimation(ScriptClientEvent* sce);
	void SCE_PlayerSetPhysics(ScriptClientEvent* sce);
	void SCE_PlayerScale(ScriptClientEvent* sce);
	void SCE_PlayerMove(ScriptClientEvent* sce);
	void SCE_PlayerSetName(ScriptClientEvent* sce);
	void SCE_PlayerCastRay(ScriptClientEvent* sce);
	void SCE_SoundAttachTo(ScriptClientEvent* sce);
	void SCE_SoundAddModifier(ScriptClientEvent* sce);
	void SCE_PlayerSetEffectTrack(ScriptClientEvent* sce);
	void SCE_ObjectSetEffectTrack(ScriptClientEvent* sce);
	void SCE_SoundPlayOnClient(ScriptClientEvent* sce);
	void SCE_SoundStopOnClient(ScriptClientEvent* sce);
	void SCE_ScriptSetEnvironment(ScriptClientEvent* sce);
	void SCE_ScriptSetDayState(ScriptClientEvent* sce);
	void SCE_PlayerLookAt(ScriptClientEvent* sce);
	void SCE_ObjectLookAt(ScriptClientEvent* sce);
	void SCE_ObjectAddLabels(ScriptClientEvent* sce);
	void SCE_ObjectSetOutline(ScriptClientEvent* sce);
	void SCE_ObjectClearLabels(ScriptClientEvent* sce);
	void SCE_RenderLabelsOnTop(ScriptClientEvent* sce);
	void SCE_DownloadAsset(ScriptClientEvent* sce);
	void SCE_PublishAssetCompleted(ScriptClientEvent* sce);
	void SCE_PublishResponseEvent(ScriptClientEvent* sce);
	void SCE_PublishDeleteAssetCompleted(ScriptClientEvent* sce);
	void SCE_GetAllScripts(ScriptClientEvent* sce);
	void SCE_ObjectSetCollision(ScriptClientEvent* sce);
	void SCE_ObjectSetMouseOver(ScriptClientEvent* sce);
	void SCE_ObjectSetDrawDistance(ScriptClientEvent* sce);
	void SCE_PlayerAddIndicator(ScriptClientEvent* sce);
	void SCE_PlayerClearIndicators(ScriptClientEvent* sce);
	void SCE_PlayerUpdateCoinHUD(ScriptClientEvent* sce);
	void SCE_PlayerUpdateHealthHUD(ScriptClientEvent* sce);
	void SCE_PlayerUpdateXPHUD(ScriptClientEvent* sce);
	void SCE_PlayerAddHealthIndicator(ScriptClientEvent* sce);
	void SCE_PlayerRemoveHealthIndicator(ScriptClientEvent* sce);
	void SCE_PlayerOpenShop(ScriptClientEvent* sce);
	void SCE_PlayerAddItemToShop(ScriptClientEvent* sce);
	void SCE_PlayerShowStatusInfo(ScriptClientEvent* sce);
	void SCE_PlayerClosePresetMenu(ScriptClientEvent* sce);
	void SCE_PlayerMovePresetMenu(ScriptClientEvent* sce);
	void SCE_PlayerOpenListBoxMenu(ScriptClientEvent* sce);
	void SCE_PlayerOpenYesNoMenu(ScriptClientEvent* sce);
	void SCE_PlayerOpenKeyListenerMenu(ScriptClientEvent* sce);
	void SCE_PlayerOpenTextTimerMenu(ScriptClientEvent* sce);
	void SCE_PlayerOpenButtonMenu(ScriptClientEvent* sce);
	void SCE_PlayerOpenStatusMenu(ScriptClientEvent* sce);
	void SCE_PlayerSetCustomTextureOnAccessory(ScriptClientEvent* sce);
	void SCE_PlayerIndividualObjectControl(ScriptClientEvent* sce);
	void SCE_PlayerResetCursorModesTypes(ScriptClientEvent* sce);
	void SCE_PlayerSetCursorModeType(ScriptClientEvent* sce);
	void SCE_PlayerSetCursorMode(ScriptClientEvent* sce);
	void SCE_PlayerSpawnVehicle(ScriptClientEvent* sce);
	void SCE_PlayerModifyVehicle(ScriptClientEvent* sce);
	void SCE_PlayerLeaveVehicle(ScriptClientEvent* sce);
	void SCE_PlayerSetVisibility(ScriptClientEvent* sce);

	virtual bool setOverheadLabels(CMovementObj* pMO = nullptr) override;

	int GetTryOnPlacementId() {
		return m_tryOnObjPlacementId;
	}
	void SetTryOnPlacementId(int id) {
		m_tryOnObjPlacementId = id;
	}
	bool InTryOnState() {
		return m_inTryOnState.compare("T") == 0;
	}

	inline void InTryOnState(bool inTryOn) {
		if (inTryOn)
			m_inTryOnState.assign("T");
		else {
			m_inTryOnState.assign("F");

			m_tryOnOnStartup = false;
			m_tryOnGlid = 0;
			m_tryOnClothingGlid = 0;
			m_tryonUseType = USE_TYPE_INVALID;
			m_tryOnObjPlacementId = 0;
		}
	}

	std::string PathBase(void) const {
		return Engine::PathBase();
	}
	std::string LocalDevBaseDir(void) const {
		return m_localDevBaseDir;
	}
	std::string LocalDevAppDir(void) const;

	std::string PathConfigs() {
		return m_pathBaseConfig;
	}
	void SetPathConfigs(const std::string& baseName) {
		m_pathBaseConfig = baseName;
	}

	virtual IGetSet* Values() override {
		return m_multiplayerObj;
	}

	virtual void SetZoneLoadingUIActive(bool active) override {
		if (active == IsZoneLoadingUIActive())
			return;
		ClientBaseEngine::SetZoneLoadingUIActive(active);
		OnZoneLoadingUIActiveChange(active);
	}

	virtual void SetZoneLoadingProgressPercent(double percent) override {
		ClientBaseEngine::SetZoneLoadingProgressPercent(percent);
	}

	virtual void SetMsgMoveEnabled(bool enable) override {
		ClientBaseEngine::SetMsgMoveEnabled(enable);
	}
	virtual bool IsMsgMoveEnabled() override {
		return ClientBaseEngine::IsMsgMoveEnabled();
	}

	virtual void SetMsgGzipSize(size_t bytes) override {
		CMultiplayerObj::s_msgGzipSize = bytes;
	}
	virtual size_t GetMsgGzipSize() const override {
		return CMultiplayerObj::s_msgGzipSize;
	}

	virtual void SetMsgGzipLogEnabled(bool enable) override {
		CMultiplayerObj::s_msgGzipLog = enable;
	}
	virtual bool IsMsgGzipLogEnabled() const override {
		return CMultiplayerObj::s_msgGzipLog;
	}

	void SyncGameTime(double serverGameTime);

	void PreRender();

	virtual bool RenderLoop(bool canWait = true) override;

	bool RenderMenus();

	bool RenderLoop_MenusOnly(bool bProcessWndMsgs = TRUE); // Mini-render loop: menu system event/rendering only

	unsigned getDispatchTime() const {
		return (IsSpawning() || !m_legacyDispatchTimeMode && IsZoneLoadingUIActive()) ? m_dispatchTimeLoading : m_dispatchTimeInGame;
	}

	virtual void ProgressUpdate(const std::string& position, BOOL init = FALSE, BOOL deinit = FALSE);
	virtual void ProgressUpdate(const std::string& position, const std::string& message, int weight, BOOL init = FALSE, BOOL deinit = FALSE);

	void TurnOffWidget();
	void setWidgetRotation(float x, float y, float z);
	void setWidgetPosition(float x, float y, float z);
	void widgetWorldSpace();
	void widgetObjectSpace();
	void widgetShowWidget(bool show);
	void widgetAdvancedMovement(bool adv);
	void getWidgetPosition(float* x, float* y, float* z);
	void getWidgetRotation(float* x, float* y, float* z);

	float GetMouseNormalizedX() const {
		return m_mouseNormalizedX;
	}
	float GetMouseNormalizedY() const {
		return m_mouseNormalizedY;
	}

	bool ObjectExists(const ObjectType& objType, int objId);

	bool ObjectIsMe(const ObjectType& objType, int objId);

	RuntimeSkeleton* ObjectGetRuntimeSkeleton(const ObjectType& objType, int objId, bool posses = false);

	CFrameObj* ObjectGetBaseFrame(const ObjectType& objType, int objId, bool posses = false);

	CSkeletonAnimHeader* ObjectGetSkeletonAnimHeader(const ObjectType& objType, int objId, const GLID& animGlid);

	bool ObjectSetVisibility(const ObjectType& objType, int objId, bool visible);
	bool ObjectSetNeedsUpdate(const ObjectType& objType, int objId, bool update);

	bool ObjectSetPosition(const ObjectType& objType, int objId, const Vector3f& pos);
	bool ObjectGetPosition(const ObjectType& objType, int objId, Vector3f& pos);

	bool ObjectSetRotation(const ObjectType& objType, int objId, const Vector3f& rot);
	bool ObjectGetRotation(const ObjectType& objType, int objId, Vector3f& rot);

	bool ObjectGetSpeed(const ObjectType& objType, int objId, double& speed) const;
	bool ObjectGetAccel(const ObjectType& objType, int objId, double& accel);
	bool ObjectGetThrottle(const ObjectType& objType, int objId, double& throttle);
	bool ObjectGetSkid(const ObjectType& objType, int objId, double& skid) const;

	void ObjectDisplayTranslationWidget(const ObjectType& objType);
	void ObjectDisplayRotationWidget(const ObjectType& objType);

	bool ObjectIsSnappable(const ObjectType& objType, int objId) const;
	bool ObjectIsInteractable(const ObjectType& objType, int objId);

	int ObjectsSelected() const;
	void ObjectClearSelection();
	void ObjectAddToSelection(const ObjectType& objType, int objId, bool reCalcPivot = true);
	void ObjectRemoveFromSelection(const ObjectType& objType, int objId, bool reCalcPivot = true);
	bool ObjectIsSelected(const ObjectType& objType, int objId) const;
	void ObjectGetSelection(ObjectType* pObjType, int* pObjId, unsigned int index) const;
	void ObjectToggleSelected(const ObjectType& objType, int objId, bool reCalcPivot = true);
	void ObjectReplaceSelection(const ObjectType& objType, int objId);
	void ObjectLockSelection(BOOL lock);
	void ObjectSnapSelection(float tolerance);
	void ObjectGetSelectionPivotOffset(float* x, float* y, float* z, int index) const;

	virtual bool ObjectGetWorldTransformation(ObjectType objType, int objId, Matrix44f& matrix);
	virtual bool ObjectGetBoneRelativeTransformation(ObjectType objType, int objId, const std::string& boneName, Matrix44f& matrix);

	virtual bool ObjectBakeAnimation(const ObjectType& objType, int objId, const GLID& animGlid);
	virtual bool ObjectSetAnimSpeedFactor(const ObjectType& objType, int objId, const GLID& animGlid, float animSpeedScale);
	virtual bool ObjectSetAnimLooping(const ObjectType& objType, int objId, const GLID& animGlid, bool animLooping);
	virtual bool ObjectSetAnimCropTimes(const ObjectType& objType, int objId, const GLID& animGlid, TimeMs animCropStartTimeMs, TimeMs animCropEndTimeMs);
	virtual bool ObjectGetAnimInfo(const ObjectType& objType, int objId, const GLID& animGlid, TimeMs& animDurationMs, TimeMs& animMinTimeMs, bool& animLooping, float& animSpeedScale, TimeMs& animCropStartTimeMs, TimeMs& animCropEndTimeMs);

	bool ObjectAddParticleAsync(const ObjectType& objType, int objId, const std::string& boneName, int particleSlot, const GLID& particleGlid, const Vector3f& offset, const Vector3f& dir, const Vector3f& up);
	bool ObjectAddParticleSync(const ObjectType& objType, int objId, const std::string& boneName, int particleSlot, const GLID& particleGlid, const Vector3f& offset, const Vector3f& dir, const Vector3f& up);
	bool ObjectRemoveParticle(const ObjectType& objType, int objId, const std::string& boneName, int particleSlot);

	bool ObjectArmEquippableItemAsync(const ObjectType& objType, int objId, const GLID& glid, int material, const boost::optional<std::string>& strBoneOverride);
	bool ObjectArmEquippableItemSync(const ObjectType& objType, int objId, const GLID& glid, int material, const boost::optional<std::string>& strBoneOverride);
	bool ObjectDisarmEquippableItem(const ObjectType& objType, int objId, const GLID& glid);

	bool PlaceDynamicObjectByGlid(const GLID& glid, int invenType) override;

	void handleBuildPlaceMouseUpdate(Vector3f rayOrigin, Vector3f rayDir, bool mouseInClient);

	void adjustBuildPlaceHeight(float newHeight);

	void setGridSpacing(float spacing);
	float getGridSpacing();

	bool m_cursorModeObject; // cursor is over object overriding cursor

	bool ResetCursorModesTypes();
	bool SetCursorModeType(const CURSOR_MODE& cursorMode, const CURSOR_TYPE& cursorType);
	bool SetCursorMode(const CURSOR_MODE& cursorMode);

	virtual std::string GetCursorFileName(const CURSOR_TYPE& type) override {
		return ClientEngine_CFrameWnd::GetCursorFileName(type);
	}
	virtual void SetCursorFileName(const CURSOR_TYPE& type, const std::string& fileName) override {
		ClientEngine_CFrameWnd::SetCursorFileName(type, fileName);
	}

	// DRF - Holds Build Mode Selected Objects
	struct SelectionEntry {
		ObjectType objType;
		int objId;
		Vector3f pivotOffset;
		SelectionEntry(const ObjectType& _objType, int _objId) :
				objType(_objType), objId(_objId) {}
	};

	typedef std::vector<SelectionEntry> selection_t;

	void SelectionListAdd(const SelectionEntry& entry);
	void SelectionListDel(const selection_t::iterator& itr);
	void SelectionListClear();
	size_t SelectionListSize() const;

	virtual void ObjectSetOutlineState(const ObjectType& objType, int objId, bool outline) override;
	virtual void ObjectSetOutlineColor(const ObjectType& objType, int objId, const D3DXCOLOR& color) override;
	virtual void ObjectSetOutlineZSorted(const ObjectType& objType, int objId, bool sort) override;
	virtual void ObjectSetRolloverMode(const ObjectType& objType, bool show) override;

	std::string movementRestrictionStr(const MovementRestrictionSource& source);

	unsigned int getMovementRestriction() const;
	void setMovementRestriction(const MovementRestrictionSource& source, const MovementRestriction& restriction, bool enable);
	void ResetMovementRestriction() {
		m_restrictMovements.clear();
	}

	void WidgetEventTriggered(bool trigger) {
		m_widgetEventOccured = trigger;
	}

	/** DRF
	* This function flags to not allow click and drag type camera movement
	* while the mouse is being clicked on a menu.  It is set when a menu
	* decides to handle WM_LBUTTONDOWN and reset on WM_LBUTTONUP in
	* KEPMenu->MsgProc() and acted on in ClientEngine::ProcessAction().
	*/
	void MenuEventTriggered(bool trigger) {
		m_menuEventOccured = trigger;
	}
	void KeyCapturedTriggered(bool trigger) {
		m_keyCaptured = trigger;
	}

	afx_msg BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg BOOL OnQueryEndSession();
	afx_msg void OnEndSession(BOOL ending);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg BOOL OnNcActivate(BOOL active);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);

	void ResizeClientRect(UINT nType, int cx, int cy, bool resetDevice);

	/** DRF
	* This functions resets a window refresh timer scheduling a window refresh from NeedsWindowRefresh()
	* which is called from RenderLoop().
	*/
	void FlagWindowRefresh();

	/** DRF
	* This functions triggers a client window resize 500ms after the last call to FlagWindowRefresh()
	* it is called from RenderLoop(). This was added to prevent 1000 resizes while dragging the
	* window size.
	*/
	void NeedsWindowRefresh();

	/** DRF
	* This functions triggers a client window refresh.
	*/
	void WindowRefresh();

	/** DRF
	* Do whatever needs done pre/post rezone (called from HandleSpawnMessagesClientSide())
	*/
	void OnRezonePre();
	void OnRezonePost();
	void OnRezoneCleanup();
	void OnZoneLoadingUIActiveChange(bool active);

	// ED-8437 - Clear Errored Resources On Rezone
	void ClearErroredResources();

	virtual BOOL GetDragDropModeOn() const;

	//! \brief Dragging cursor entering target
	//! \return true if willing to accept, false otherwise
	bool DragEnter(int type, const std::string& str, int mouseX, int mouseY, unsigned int keyState, unsigned int flags) override;

	//! \brief Dragging cursor moving over target
	void DragOver(int mouseX, int mouseY, unsigned int keyState, unsigned int flags) override;

	//! \brief Dragging cursor leaving target
	void DragLeave(void) override;

	//! \brief Dropped over target
	//! \return true if accepted, false if rejected
	bool Drop(int type, const std::string& str, int mouseX, int mouseY, unsigned int keyState, unsigned int flags) override;

	void OnLostDevice();
	void OnResetDevice();

	virtual HWND GetWindowHandle() {
		return m_hWnd;
	}
	virtual IDirect3DDevice9* GetD3DDevice() {
		return g_pD3dDevice;
	}
	virtual BOOL WindowMinimized() {
		return d3dpp.BackBufferWidth == 0;
	}

	virtual bool IsFullScreen();
	virtual void SetFullScreen(bool bFullScreen); // 0: windowed, 1: full-screen

	ReD3DX9DeviceState* GetReDeviceState() {
		return m_pReDeviceState;
	}
	const ReD3DX9DeviceState* GetReDeviceState() const {
		return m_pReDeviceState;
	}

	int CreateViewport(const std::string& viewportName);
	int AddRuntimeViewport(int viewport_db_index);
	bool ActivateViewport(CViewportObj* pVO, int width = 0, int height = 0);
	void BindCameraToViewport(CCameraObj* pCO, int runtime_viewport_index);
	CViewportObj* GetActiveViewport() const;
	CCameraObj* GetActiveViewportCamera() const;
	void DestroyAllViewports();

	int CreateCamera(const std::string& cameraName);
	bool SetActiveCameraForMovementObjectByIndexOffset(CMovementObj* pMO, int cameraIndexOffset, bool forceChange);
	bool SetActiveCameraForMovementObjectByIndex(CMovementObj* pMO, int cameraIndex);
	int GetActiveCameraIndexForMovementObject(CMovementObj* pMO) const;
	CCameraObj* GetActiveCameraForMovementObject(CMovementObj* pMO) const;
	virtual CCameraObj* GetActiveCameraForMe() const override;
	virtual Vector3f GetActiveCameraPositionForMe() const override;
	virtual Vector3f GetActiveCameraOrientationForMe() const override;
	virtual Vector3f GetActiveCameraUpVectorForMe() const override;

	float m_cfgCamAutoAzimuth = CAM_AUTO_AZIMUTH_DEFAULT;
	float m_cfgCamAutoElevation = CAM_AUTO_ELEVATION_DEFAULT;
	virtual bool SetActiveCameraAutoAzimuthForMe(float radians) override;
	virtual float GetActiveCameraAutoAzimuthForMe() const override;
	virtual bool SetActiveCameraAutoElevationForMe(float radians) override;
	virtual float GetActiveCameraAutoElevationForMe() const override;

	float m_camToggleDist = 0;
	void SetCameraToggleDist(float camDist) {
		m_camToggleDist = camDist;
	}
	float GetCameraToggleDist() const {
		return m_camToggleDist;
	}

	virtual void SetCameraCollision(bool collide) override {
		m_cameraCollision = collide;
	}
	virtual void SetCameraLock(bool lock) override {
		m_cameraLock = lock;
	}
	virtual bool GetCameraLock() const override {
		return m_cameraLock;
	}
	virtual void SetCameraMoveSpeed(double speed) override {
		m_cameraMoveSpeed = speed;
	}
	virtual double GetCameraMoveSpeed() const override {
		return m_cameraMoveSpeed;
	}

	virtual RuntimeCameras* GetRuntimeCameras() const override {
		return m_pRuntimeCameras;
	}
	void DestroyAllCameras();

	bool AttachCameraToObject(CCameraObj* pCO, ObjectType objType, int objId);
	bool AttachActiveViewportCameraToObject(ObjectType objType, int objId) override;

	void InitCamerasForMovementObject(CMovementObj* pMO);

	void HandleCameraCollision(CCameraObj* pCO, const Vector3f& startSegment, const Vector3f& endSegment);
	void CameraCallback(CCameraObj* pCO, CameraAction cameraAction);
	void OrbitCameraCallback(CCameraObj* pCO);

	virtual void SendCameraEvent(CCameraObj* pCO, CameraAction action, int runtime_camera_id, int runtime_viewport_index = 0) override;

	// Camera Script GetSet Interface
	virtual IGetSet* GetActiveCameraForMeAsGetSet() const override {
		return dynamic_cast<IGetSet*>(GetActiveCameraForMe());
	}
	virtual bool SetActiveCameraForActorByIndexOffset(IGetSet* pGS_mo, int cameraIndexOffset, bool forceChange) override {
		auto pMO = dynamic_cast<CMovementObj*>(pGS_mo);
		return SetActiveCameraForMovementObjectByIndexOffset(pMO, cameraIndexOffset, forceChange);
	}
	virtual int GetActiveCameraIndexForActor(IGetSet* pGS_mo) const override {
		auto pMO = dynamic_cast<CMovementObj*>(pGS_mo);
		return GetActiveCameraIndexForMovementObject(pMO);
	}

	virtual bool PreloadAnimationByHash(const std::string& sAnimHash, int prioDelta) override;
	virtual bool PreloadAnimation(GLID animGLID, int prioDelta) override;
	virtual bool PreloadMesh(const std::string& meshHash, int prioDelta) override;

	CStateManagementObj* GetRenderStateManager(void) const {
		return m_pRenderStateMgr;
	}

	bool ResetRenderState();

	void EnableLightForPosition(Vector3f& position);

	EffectHandle CreateParticleSystem(const char* effectName, Vector3f position, Quaternionf orientation);

	virtual IDirect3DTexture9* GetD3DTextureIfLoadedOrQueueToLoad(const std::string& texFileName) override;
	virtual IDirect3DTexture9* GetD3DTextureIfLoadedOrQueueToLoad(DWORD textureKey) override;
	virtual ITexture* GetTextureIfLoadedOrQueueToLoad(DWORD texKey) override;

	bool RenderLoadingSplashScreen(const std::string& aFullFileName, bool maintainAspect);

	std::string AddItemThumbnailToDatabase(const GLID& glid);
	BOOL AddTextureToDatabase(CEXMeshObj* meshObj, BOOL remesh = TRUE, DWORD maxTex = 8);
	BOOL AddTextureToDatabase(CMaterialObject* mat, BOOL remesh = TRUE, DWORD maxTex = 8);
	CTextureEX* GetTextureFromDatabase(DWORD texNameHash);
	TextureDatabase* GetTextureDataBase() const {
		return m_pTextureDB;
	}

	// Called From CTextureEX::~CTextureEX()
	void OnTextureDestroy(CTextureEX* pTex);

	// DRF - ED-7035 - Investigate texture memory impact
	// Called From CTextureEX::UnloadTexture()
	virtual void OnTextureUnload(CTextureEX* pTex) override;

	//Ankit ----- getters and setters for local/global media volume multipliers
	double GetGlobalMediaVolMult() const {
		return m_globalMediaVolMult;
	}
	void SetGlobalMediaVolMult(double mult) {
		m_globalMediaVolMult = Clamp(mult, 0.0, 1.0);
	}
	double GetLocalMediaVolMult() const {
		return m_localMediaVolMult;
	}
	void SetLocalMediaVolMult(double mult) {
		m_localMediaVolMult = Clamp(mult, 0.0, 1.0);
	}

	bool ConfigureAudio();
	virtual bool SetMasterVolume(double volPct) override;
	virtual double GetMasterVolume() const override;
	virtual bool SetMute(bool bMute) override;
	virtual bool GetMute() const override;
	size_t GetPeakFlashInstanceCount();
	size_t GetFlashProcessCrashCount();

	// Global Media Enable
	bool SetMediaEnabled(bool enabled);
	bool GetMediaEnabled() const {
		return m_mediaEnabled;
	}

	// Media Placements Functions
	size_t MediaPlacements() const;
	void MediaPlacementsLog(bool verbose) const;
	IExternalEXMesh* MediaPlacementsGetMesh(int placementId);
	bool MediaPlacementsSetMesh(int placementId, std::shared_ptr<IExternalEXMesh> pXM);
	bool MediaPlacementsDel(int placementId, bool andWait);
	bool MediaPlacementsDelAll(bool andWait);
	bool MediaPlacementsUpdate();

	std::shared_ptr<IExternalEXMesh> MediaPlacementsFindMesh(const MediaParams& mediaParams);
	size_t MediaPlacementsRefsMesh(IExternalEXMesh* pXM);

	bool MediaPlacementsPlaylistSync(const MediaParams& mediaParams);

	// Render media trigger volume
	void EnableMediaTriggerRender(bool enable) {
		m_renderMediaTriggers = enable;
	}

	// Ignore Media Changes During Rezoning
	bool IsMediaRezoning() const {
		return IsRezoning() || IsZoneLoadingUIActive();
	}

	// Stops all media and destroys all media meshes
	bool StopAllMedia(bool andWait = true);

	// Root Play Function - Everything eventually calls this to start playing flash.
	std::shared_ptr<IExternalEXMesh> StartMediaMesh(const MediaParams& mediaParams, bool shareMesh);

	// Experimental Mesh Update (instead of stop/start)
	bool SetMediaMeshMediaParams(int placementId, const MediaParams& mediaParams, bool allowRewind = true);

	// Returns media mesh on given dynamic object if actively playing.
	IExternalEXMesh* GetMediaMeshOnDynamicObject(int placementId);

	// Called By: *MediaOnWorldObjectMesh(pWO)
	bool StartMediaOnMeshObject(CEXMeshObj* meshObject, CReferenceObj* pRO, const MediaParams& mediaParams);
	bool StopMediaOnMeshObject(CEXMeshObj* meshObject, CReferenceObj* pRO);

	// Called By: *MediaOnWorldObjectMesh(traceId) | *MediaOnWorldObjectMesh(name)
	bool StartMediaOnWorldObjectMesh(CWldObject* pWO, const MediaParams& mediaParams);
	bool StopMediaOnWorldObjectMesh(CWldObject* pWO);

	// Called By: LUA ClientEngineGlue
	bool StartMediaOnWorldObjectMesh(int traceId, const MediaParams& mediaParams);
	bool StopMediaOnWorldObjectMesh(int traceId);

	// Called By: LUA ClientEngineGlue
	bool StartMediaOnWorldObjectMesh(const std::string& name, const MediaParams& mediaParams);
	bool StopMediaOnWorldObjectMesh(const std::string& name);

	// Dynamic Object Media Control
	bool StartMediaOnDynamicObject(int placementId);
	virtual bool StopMediaOnDynamicObject(int placementId) override;

	// Dynamic Object Media Params
	bool SetMediaParamsOnDynamicObject(int placementId, const MediaParams& mediaParams);
	bool GetMediaParamsOnDynamicObject(int placementId, MediaParams& mediaParams);

	// Dynamic Object Media Volume (-1=no change, 0=mute)
	bool SetMediaVolumeOnDynamicObject(int placementId, double volPct = -1);
	bool GetMediaVolumeOnDynamicObject(int placementId, double& volPct);

	// Dynamic Object Media Mesh Volume (-1=no change, 0=mute)
	bool SetMediaVolumeOnDynamicObjectMediaMesh(int placementId, double volPct = -1);
	bool GetMediaVolumeOnDynamicObjectMediaMesh(int placementId, double& volPct);

	// Dynamic Object Media Radius (-1=no change, 0=global)
	bool SetMediaRadiusOnDynamicObject(int placementId, const MediaRadius& radius);
	bool GetMediaRadiusOnDynamicObject(int placementId, MediaRadius& radius);

	// Dynamic Object Media Mute
	bool SetMediaMuteOnDynamicObject(int placementId, bool mute);
	bool GetMediaMuteOnDynamicObject(int placementId, bool& mute);

	// Dynamic Object Media Range
	bool GetMediaRangeOnDynamicObject(int placementId, MediaRange& mediaRange, bool logIt = false);

	// Dynamic Object Media Update (ranges & triggers media)
	bool UpdateMediaOnDynamicObject(int placementId, bool logIt = false);

	// Returns true if flash support is installed.
	bool FlashIsInstalled();

	virtual IExternalMesh* CreateFlashMesh(
		HWND parent,
		const MediaParams& mediaParams,
		uint32_t width = 0, uint32_t height = 0,
		bool loop = false,
		bool wantInput = false,
		int32_t x = 0, int32_t y = 0) override;

	virtual bool GetFlashMovieSize(const std::string& filePath, uint32_t& width, uint32_t& height) override;

	// Sound Placements
private:
	bool SoundPlacementDelInternal(SoundPlacement* pSP);
	typedef std::map<int, SoundPlacement*> SoundPlacementsMap;
	SoundPlacementsMap m_soundPlacements;

public:
	bool SoundPlacementsInit();
	void SoundPlacementsLog(bool verbose) const;
	bool SoundPlacementsClear();
	bool SoundPlacementsEnableRender(bool toRender) {
		m_renderSoundPlacements = toRender;
		return true;
	}
	bool SoundPlacementsGetIds(std::vector<int>& soundPlacementIdList);
	SoundPlacement* SoundPlacementGet(int soundPlacementId) const;
	int SoundPlacementGetId(const SoundPlacement* pSP) const;
	bool SoundPlacementValid(int soundPlacementId) const {
		return SoundPlacementGet(soundPlacementId) != NULL;
	}
	bool SoundPlacementAdd(int soundPlacementId);
	bool SoundPlacementDel(SoundPlacement* pSP);
	bool SoundPlacementDel(int soundPlacementId);
	bool SoundPlacementGetInfo(const SoundPlacement* pSP, std::string& name, GLID& glid) const;
	bool SoundPlacementGetInfo(int soundPlacementId, std::string& name, GLID& glid) const;
	bool SoundPlacementSetPosition(SoundPlacement* pSP, const Vector3f& pos);
	bool SoundPlacementSetPosition(int soundPlacementId, const Vector3f& pos);
	bool SoundPlacementGetPosition(const SoundPlacement* pSP, Vector3f& pos) const;
	bool SoundPlacementGetPosition(int soundPlacementId, Vector3f& pos) const;
	bool SoundPlacementSetRotationRad(SoundPlacement* pSP, float radians);
	bool SoundPlacementSetRotationRad(int soundPlacementId, float radians);
	bool SoundPlacementGetRotationRad(const SoundPlacement* pSP, float& radians) const;
	bool SoundPlacementGetRotationRad(int soundPlacementId, float& radians) const;
	bool SoundPlacementSetDirection(SoundPlacement* pSP, const Vector3f& dir);
	bool SoundPlacementSetDirection(int soundPlacementId, const Vector3f& dir);
	bool SoundPlacementPlaySound(SoundPlacement* pSP, const GLID& glid);
	bool SoundPlacementPlaySound(int soundPlacementId, const GLID& glid);
	bool SoundPlacementUpdate(SoundPlacement* pSP = NULL);
	bool SoundPlacementUpdatePos(SoundPlacement* pSP, const Vector3f& pos);
	bool SoundPlacementAttachValid(SoundPlacement* pSP);
	bool SoundPlacementAttachPosNew(SoundPlacement* pSP, Vector3f& posNew);
	bool SoundPlacementPickRayHitTest(SoundPlacement* pSP, const Vector3f& rayOrigin, const Vector3f& rayDir, Vector3f& intersection, Vector3f& normal, float& distance) const;
	int GetSoundOnDynamicObject(int placementId, const GLID& glid);
	int PlaySoundOnDynamicObject(int placementId, const GLID& glid);
	bool RemoveSoundOnDynamicObject(int placementId, const GLID& glid);
	int GetSoundOnPlayer(int netId, const GLID& glid);
	int PlaySoundOnPlayer(int netId, const GLID& glid);
	bool RemoveSoundOnPlayer(int netId, const GLID& glid);
	bool SoundPlacementIsMyVehicle(const SoundPlacement* pSP) const;
	bool SoundPlacementAddModifier(SoundPlacement* pSP, const SoundPlacement::Modifier& mod);
	bool SoundPlacementAddModifier(int soundPlacementId, const SoundPlacement::Modifier& mod);
	bool SoundPlacementAddModifier(int soundPlacementId, int input, int output, double gain, double offset) {
		return SoundPlacementAddModifier(soundPlacementId, SoundPlacement::Modifier(input, output, gain, offset));
	}
	bool SoundPlacementUpdateModifiers(SoundPlacement* pSP);
	bool SoundPlacementUpdateModifier(SoundPlacement* pSP, SoundPlacement::Modifier& mod);

	virtual void MoveDynamicObjectTriggers(int placementId, const Vector3f& origin, const Vector3f& dir);
	virtual void RemoveDynamicObjectTriggers(int placementId) override;
	void EnableTriggerRender(bool renderDynamicTriggers, bool renderWorldTriggers) {
		m_renderDynamicTriggers = renderDynamicTriggers;
		m_renderWorldTriggers = renderWorldTriggers;
	}

	bool GetTransformRelativeToBone(int boneId, double x, double y, double z, double* outX, double* outY, double* outZ);

	virtual bool EntityAnimationManagementCallbackForMe() override {
		return ClientBaseEngine::EntityAnimationManagementCallbackForMe();
	}

	TimeSec AssignCurrentAnimation(eAnimType animType, int animVer = ANIM_VER_ANY, bool repeat = true);

	virtual bool setMyOverrideAnimationByType(eAnimType animType, int animVer) override {
		return ClientBaseEngine::setMyOverrideAnimationByType(animType, animVer);
	}

	virtual bool setMyOverrideAnimationSettings(const AnimSettings& animSettings) override {
		return ClientBaseEngine::setMyOverrideAnimationSettings(animSettings);
	}

	virtual bool getMyOverrideAnimationSettings(AnimSettings& animSettings) override {
		return ClientBaseEngine::getMyOverrideAnimationSettings(animSettings);
	}

	virtual bool getMyPrimaryAnimationSettings(AnimSettings& animSettings) override {
		return ClientBaseEngine::getMyPrimaryAnimationSettings(animSettings);
	}

	BOOL SetEntityAnimByNetAssignedID(int netAssignedID, eAnimType animType, int animVer = 0);

	bool SetMenuGameObjectEquippableAltCharConfig(CMovementObj* pMO, const GLID_OR_BASE& glidOrBase, size_t slot, int meshId, int matlId);

	virtual bool SetMenuGameObjectEquippableAltCharConfig(IGetSet* pGS_MGO, const GLID_OR_BASE& glidOrBase, size_t slot, int meshId, int matlId) override {
		auto pMGO = dynamic_cast<CMenuGameObject*>(pGS_MGO);
		return pMGO ? SetMenuGameObjectEquippableAltCharConfig(pMGO->GetMovementObject(), glidOrBase, slot, meshId, matlId) : false;
	}

	virtual bool SetPlayerEquippableAltCharConfig(IGetSet* pGS_MO, const GLID_OR_BASE& glidOrBase, size_t slot, int meshId, int matlId) override {
		return SetMenuGameObjectEquippableAltCharConfig(dynamic_cast<CMovementObj*>(pGS_MO), glidOrBase, slot, meshId, matlId);
	}

	virtual bool SetPlayerEquippableAnimation(IGetSet* pGS_MO, const GLID& glidItem, const GLID& glidAnim, bool updateServer) override {
		return SetEquippableAnimation(dynamic_cast<CMovementObj*>(pGS_MO), glidItem, glidAnim, updateServer);
	}

	virtual bool SetPlayerCustomTextureOnAccessoryLocal(RuntimeSkeleton* pRuntimeSkeleton, int glid, const std::string& textureUrl) override;

	virtual bool SetPlayerCustomTextureOnAccessoryLocal(IGetSet* pGS_MO, int glid, const std::string& textureUrl) override {
		CMovementObj* pMO = dynamic_cast<CMovementObj*>(pGS_MO);
		if (!pMO)
			return false;
		RuntimeSkeleton* pSkeleton = pMO->getSkeleton();
		return SetPlayerCustomTextureOnAccessoryLocal(pSkeleton, glid, textureUrl);
	}

	CMenuGameObject* AddMenuGameObject(int game_object_index, int animation_index);
	CMenuGameObject* AddMenuGameObjectByEquippableGlid(long long glid);
	CMenuGameObject* AddMenuDynamicObject(CDynamicPlacementObj* pDPO);
	CMenuGameObject* AddMenuDynamicObjectByPlacement(int placementId);

	virtual IGetSet* AddMenuGameObjectAsGetSet(int game_object_index, int animation_index) override {
		return dynamic_cast<IGetSet*>(AddMenuGameObject(game_object_index, animation_index));
	}
	virtual IGetSet* AddMenuGameObjectByEquippableGlidAsGetSet(long long glid) override {
		return dynamic_cast<IGetSet*>(AddMenuGameObjectByEquippableGlid(glid));
	}
	virtual IGetSet* AddMenuDynamicObjectByPlacementAsGetSet(int placementId) override {
		return dynamic_cast<IGetSet*>(AddMenuDynamicObjectByPlacement(placementId));
	}

	void RemoveMenuGameObject(CMenuGameObject* pMGO) {
		if (pMGO)
			m_pMenuGameObjects->Remove(pMGO);
	}

	std::string MenuGameObjectScreenshotCapture(CMenuGameObject* pMGO, std::string fileName) {
		return pMGO ? ScreenshotCapture(pMGO->GetViewportRect(), fileName, "MGOImages") : "";
	}

	void MenuGameObjectMoveToTop(CMenuGameObject* pMGO) {
		if (pMGO)
			pMGO->SetOnTop(true);
	}

	void MenuGameObjectMoveToBottom(CMenuGameObject* pMGO) {
		if (pMGO)
			pMGO->SetOnTop(false);
	}

	void MenuGameObjectSetDOZoomFactor(CMenuGameObject* pMGO, float zoomFactor) {
		if (pMGO)
			pMGO->SetDynamicObjZoomFactor(zoomFactor);
	}

	void MenuGameObjectSetViewportBackgroundColor(CMenuGameObject* pMGO, int r, int g, int b) {
		if (pMGO)
			pMGO->SetViewportBackgroundColor(r, g, b);
	}

	virtual void RemoveMenuGameObjectAsGetSet(IGetSet* pGS_MGO) override {
		RemoveMenuGameObject(dynamic_cast<CMenuGameObject*>(pGS_MGO));
	}
	virtual std::string MenuGameObjectScreenshotCaptureAsGetSet(IGetSet* pGS_MGO, const std::string& fileName) override {
		return MenuGameObjectScreenshotCapture(dynamic_cast<CMenuGameObject*>(pGS_MGO), fileName);
	}
	virtual void MenuGameObjectMoveToTopAsGetSet(IGetSet* pGS_MGO) override {
		MenuGameObjectMoveToTop(dynamic_cast<CMenuGameObject*>(pGS_MGO));
	}
	virtual void MenuGameObjectMoveToBottomAsGetSet(IGetSet* pGS_MGO) override {
		MenuGameObjectMoveToBottom(dynamic_cast<CMenuGameObject*>(pGS_MGO));
	}
	virtual void MenuGameObjectSetDOZoomFactorAsGetSet(IGetSet* pGS_MGO, float zoomFactor) override {
		MenuGameObjectSetDOZoomFactor(dynamic_cast<CMenuGameObject*>(pGS_MGO), zoomFactor);
	}
	virtual void MenuGameObjectSetViewportBackgroundColorAsGetSet(IGetSet* pGS_MGO, int r, int g, int b) override {
		MenuGameObjectSetViewportBackgroundColor(dynamic_cast<CMenuGameObject*>(pGS_MGO), r, g, b);
	}
	virtual void MenuGameObjectSetAnimationTimeAsGetSet(IGetSet* pGS_MGO, TimeMs animTimeMs) override {
		auto pMGO = dynamic_cast<CMenuGameObject*>(pGS_MGO);
		if (pMGO)
			pMGO->SetAnimTimeMs(animTimeMs);
	}
	virtual void MenuGameObjectSetAnimationByTypeAsGetSet(IGetSet* pGS_MGO, eAnimType animType, int animVer) override {
		auto pMGO = dynamic_cast<CMenuGameObject*>(pGS_MGO);
		if (pMGO)
			pMGO->setCurrentAnimationByType(animType, animVer);
	}
	virtual bool MenuGameObjectSetAnimationAsGetSet(IGetSet* pGS_MGO, const AnimSettings& animSettings) override {
		auto pMGO = dynamic_cast<CMenuGameObject*>(pGS_MGO);
		return pMGO ? pMGO->setPrimaryAnimationSettings(animSettings) : false;
	}

	// DRF - Opens the given menu with the given menu identifier.
	bool MenuOpenAs(const std::string& menuName, const std::string& menuId);

	// DRF - Opens the given menu with the menu name as identifier.
	bool MenuOpen(const std::string& menuName) {
		return MenuOpenAs(menuName, menuName);
	}
	bool MenuOpen(int menuId) {
		std::string menuName = m_menuList->GetFilenameByID(menuId);
		return menuName.empty() ? false : MenuOpen(menuName);
	}

	// DRF - Closes the given menu by name.
	bool MenuClose(const std::string& menuName);
	bool MenuClose(int menuId) {
		std::string menuName = m_menuList->GetFilenameByID(menuId);
		return MenuClose(menuName);
	}

	// DRF - Closes all menus except the given menu by name, empty for all.
	bool MenuCloseAllExcept(const std::string& menuName = "");

	// DRF - Calls all menus OnExit() funciton, preparing them for shutdown.
	bool MenuExitAll();

	virtual int ShowPhysicsWindow(int mode) override {
		return ClientBaseEngine::ShowPhysicsWindow(mode);
	}

	std::shared_ptr<Physics::Vehicle> GetMyVehicle() const;
	int GetMyVehiclePlacementId() const;
	virtual double GetMyVehicleSpeedKPH() const override;
	double GetMyVehicleSkid() const;
	double GetMyVehicleThrottle();

	virtual void EngagePhysicsVehicle(int placementId, const Vector3f& offsetMeters, const Quaternionf& orientation, const Physics::VehicleProperties& props);
	virtual void DisengagePhysicsVehicle();

	std::vector<int> getAllMovementObjectNetTraceIds();

	virtual bool DeSpawnNPC(NETID netTraceId) {
		return DeSpawnStaticMovementObj(netTraceId);
	}

	virtual bool DestroyMovementObject(CMovementObj* pMO) override;

	virtual bool DestroyMovementObjectList();

	bool isMovementObjectVisible(CMovementObj* pMO) const;

	void GetMyInfo(std::vector<std::string>& pInfo);
	std::string GetMyTitle();
	bool SetMyPosition(float x, float y, float z);
	bool SetMyOrientation(float rx, float ry, float rz);
	bool SetMyLookAt(float x, float y, float z);
	FLOAT GetMyDistanceToMovementObject(int netTraceId);

	IGetSet* GetMovementObjectMeAsGetSet() {
		return dynamic_cast<IGetSet*>(GetMovementObjMe());
	}
	IGetSet* GetMovementObjectByNetIdAsGetSet(int netId = 0) {
		return dynamic_cast<IGetSet*>(GetMovementObjByNetId(netId));
	}
	IGetSet* GetMovementObjectByNameAsGetSet(const std::string& name) {
		return dynamic_cast<IGetSet*>(GetMovementObjByName(name));
	}

	bool GetMovementObjectPosRot(CMovementObj* pMO, Vector3f& pos, Vector3f& rot);

	virtual CMovementObj* SpawnStaticMovementObject(
		int siType, // SERVER_ITEM_TYPE
		const std::string& name,
		const CharConfig* pCharConfig,
		const std::vector<GLID>& glidsArmedVec,
		int dbIndex,
		const Vector3f& scale,
		int netId,
		const Vector3f& pos,
		double rotDeg,
		const GLID& glidAnimSpecial,
		int placementId,
		bool networkEntity);

	// DRF - Added
	virtual IGetSet* SpawnNPC(
		int siType // SERVER_ITEM_TYPE
		,
		const std::string& name, CharConfig* pCharConfig, const std::vector<GLID>& glidsArmedVec, int dbIndex, NETID netTraceId, const Vector3f& pos, double rotDeg, const GLID& glidAnimSpecial, bool networkEntity) {
		return SpawnStaticMovementObject(
			siType, name, pCharConfig, glidsArmedVec, dbIndex, Vector3f(1.0f, 1.0f, 1.0f) // scale - default to 1 for now
			,
			netTraceId, pos, rotDeg, glidAnimSpecial, 0, networkEntity);
	}

	virtual void configMovementObjectSubstitute(int edbIdx, ObjectType type, int globalId);
	void loadMovementObjectSubstitute(CMovementObj* pMO);
	void removeMovementObjectSubstitute(CMovementObj* pMO);

	void ProcessAction(ControlObjActionState const& act_state, CMovementObj* pMO, ControlInfoObj* control_info_obj, TimeMs cur_time);

	virtual bool HitBoundingBox(const Vector4f& minB, const Vector4f& maxB, const Vector3f& origin, const Vector3f& dir, Vector3f& coord, Vector3f& normal, BOOL inOut = TRUE); // Woo's method ray bs AABB intersector
	virtual std::tuple<bool, Vector3f> DynamicObjectCollisionTest(int iPlacementId, const Vector3f& direction, const boost::optional<Vector3f>& startPos) override; // Determines where a placement would collide with another object when moving in the provided direction

	virtual bool SphereToBox(const Vector4f& minB, const Vector4f& maxB, const Vector3f& origin, const float radius, Vector3f& intersection);

	virtual bool IsWorldObjectCustomizableByIdentity(int identity) override;

	virtual bool IsDynamicObjectCustomizable(int placementId) override;

	virtual int PlaceLocalDynamicObject(int globalId, const Vector3f* positionOverride = NULL, const Vector3f* directionOverride = NULL, const Vector3f* slideOverride = NULL, float scaleFactor = 1.0f); // Return placement ID

	virtual void SetAnimationOnDynamicObject(int placementId, const AnimSettings& animSettings) override;
	virtual bool GetDynamicObjectAnimation(int placementId, AnimSettings& animSettings, std::string& name);

	virtual int GetNumPlayers();
	virtual bool GetMyPosition(float& posX, float& posY, float& posZ, float& rotDeg);

	virtual void SetMenuSelectMode(BOOL mode = FALSE);

	// -- Character
	virtual bool PlayCharacter(int index, const char* startUrl, bool reconnect = false) override;
	virtual bool DeleteCharacter(int index) override;
	virtual bool CreateCharacterOnServerFromMenuGameObject(IGetSet* pGS_MGO, int spawnIndex, const std::string& name, int nTeam, int scaleX, int scaleY, int scaleZ) override; //scale factors in percentage, ie 80 for 80%
	virtual bool CreateCharacterOnServerFromPlayer(IGetSet* pGS_MGO, int spawnIndex, const std::string& name, int nTeam, int scaleX, int scaleY, int scaleZ) override; //scale factors in percentage, ie 80 for 80%
	virtual int GetCharacterMaxCount() const override;

	// -- Character config
	virtual bool GetMyPlayerCharConfigBaseMeshIds(std::vector<std::string>& configs) override;
	virtual bool GetMyPlayerCharConfigBaseMatlIds(std::vector<std::string>& materials) override;
	virtual bool SetMenuGameObjectAltCharConfig(IGetSet* pGS_MGO, int cfg, int altIndex, int material = 0) override;
	virtual bool SetPlayerAltCharConfig(IGetSet* pGS_MGO, int cfg, int altIndex, int material = 0) override;
	virtual bool SetPlayerActorType(IGetSet* pGS_MGO, int actorIndex) override;
	virtual bool ScaleRuntimePlayer(IGetSet* pGS_MGO, float xFactor, float yFactor, float zFactor) override;
	virtual Vector3f GetRuntimePlayerScale(IGetSet* pGS_MO) override;
	virtual int GetPlayerCharConfigMeshId(IGetSet* pGS_MGO, int configType, int configSlot) override;
	virtual int GetPlayerCharConfigMatlId(IGetSet* pGS_MGO, int configType, int configSlot) override;
	virtual bool GetPlayerAttachmentTypes(IGetSet* pGS_MGO, std::vector<std::string>& attachments) override;

	bool RequestCharacterInventory(eAttribEventType invType);

	void SendInventoryWebEvent(const std::list<InventoryWeb*>& webInventoryItems, int totalNumRecords);
	void AddUGCTexturesToDatabase(const std::list<int>& items);
	void presetMenuMessage(const std::string& menuName, const int placementId, const std::string& messageType, const std::string& data1, const std::string& data2, const std::string& identifier);

	void AddUGCTexture(const GLID& glid, const std::string& element_name);

	// Menus
	std::string GetMenuDir() const; // GameFiles\\Menus
	std::string GetMenuScriptDir() const; // GameFiles\\MenuScripts
	std::string GetTemplateMenuDir();
	std::string GetTemplateScriptDir();
	std::string GetAppMenuDir();
	std::string GetAppScriptDir();
	int GetNumMenus(int categoryMask);
	std::string GetMenuByIndex(int categoryMask, int index);
	void ReloadMenus();
	int GetNumMenuScripts(int categoryMask);
	std::string GetMenuScriptByIndex(int categoryMask, int index);

	// DRF - UGC Icon Files Are 'icon<glid>.jpg', non-UGC are '<glid>.dds'
	std::string TextureFileName(const GLID& glid);

	bool RequestCharacterInventoryAttachableObjects();
	void EncodeInventoryItem(IWriteableBuffer& buffer, int GLID, int quantity, CGlobalInventoryObj* invObj);
	bool TryEncodeUGCInvItem(IWriteableBuffer& buffer, int GLID, int quantity, bool bankOnly);
	void EncodeUnrecognizedInvItem(IWriteableBuffer& buffer, int GLID, int quantity, bool bankOnly);
	bool RequestPictureFrames();
	bool GetItemInfo(const GLID& glid, std::string& textureName, RECT& textureCoords);
	bool UseInventoryItem(const GLID& glid, short invType = IT_NORMAL);
	bool UseInventoryItemOnTarget(const GLID& glid, int objType, int objId, short invType = IT_NORMAL);
	bool RemoveInventoryItem(const GLID& glid, int qty = -1, short invType = IT_NORMAL);
	bool RemoveBankItem(const GLID& glid, int qty = -1, short invType = IT_NORMAL);
	bool DepositItemToBank(int glID, int nQuantity, short invType);
	bool WithdrawItemFromBank(int glID, int nQuantity, short invType);
	BOOL BuyItemFromStore(int glID, int nQuantity, int nCurrencyType);
	BOOL SellItemToStore(int glID, int nQuantity);

	void SpawnToRebirthPoint();
	void RequestPlayerRespawn(); // #MLB_ED_7947 Concrete implementation for new event RequestPlayerRespawnEvent

	// -- Controls
	bool GetControlConfiguration(std::vector<std::string>& pConfig);
	bool PendingControlAssignment(int index);
	bool RestoreDefaultControlConfiguration();
	bool SaveControlConfiguration();
	bool UnassignControlByIndex(int index);

	virtual bool SendChatMessage(eChatType chatType, const std::string& msg, bool fromIMClient = false) override;
	virtual bool SendChatMessageBroadcast(eChatType chatType, const std::string& msg, const char* toDistribution, const char* fromRecipient) override;

	void SendInteractCommand(); // sends CMD_LOOT

	bool ChildGameWebCallAllowed(const std::string& url);

	void Quit();
	void ClearChatFocus();

	void RotatePointAroundPoint(Vector3f& outPos, const Vector3f& point, const Vector3f& pivot, const Vector3f& angle);

	virtual size_t GetDynamicObjectCount() const override {
		return ClientBaseEngine::GetDynamicObjectCount();
	}

	int GetDynamicObjectPlacementIdByPos(int pos);

	virtual CDynamicPlacementObj* GetDynamicObject(int placementId) override final {
		return ClientBaseEngine::GetDynamicObject(placementId);
	}
	virtual const CDynamicPlacementObj* GetDynamicObject(int placementId) const override final {
		return ClientBaseEngine::GetDynamicObject(placementId);
	}

	bool isDynamicObjectInited(int placementId);

	bool GetDynamicObjectInfo(
		int placementId,
		std::string& name,
		std::string& desc,
		bool& mediaSupported,
		bool& isAttachable,
		bool& isInteractive,
		bool& isLocal,
		bool& isDerivable,
		int& assetId,
		int& friendId,
		std::string& customTexture,
		int& globalId,
		int& invSubType,
		std::string& textureUrl,
		int& gameItemId,
		int& playerId,
		DYN_OBJ_TYPE& objType);

	std::string GetDynamicObjectName(int placementId);

	int GetDynamicObjectGlid(int placementId);

	bool SetDynamicObjectPosition(int placementId, const Vector3f& pos);
	bool GetDynamicObjectPosition(int placementId, Vector3f& pos);

	bool SetDynamicObjectRotation(int placementId, const Vector3f& rot);
	bool GetDynamicObjectRotation(int placementId, Vector3f& rot);

	bool GetDynamicObjectVisibility(int placementId, eVisibility& vis);
	bool SetDynamicObjectVisibility(int placementId, const eVisibility& vis);

	bool GetDynamicObjectAnimPosRot(int placementId, float& posx, float& posY, float& posZ, float& RotY);

	bool GetDynamicObjectLastLocation(int placementId, Vector3f& pos); // DRF - Added - Returns the last location of the dynamic object (accurate during animations).

	bool GetDynamicObjectDistanceToPlayer(int placementId, double& distance); // DRF - Added - Returns the distance between the given dynamic object and my avatar.

	virtual void UpdateDynamicObjectInSubList(CDynamicPlacementObj* pDPO, DynObjSubListId sublistId) override final {
		m_dynamicPlacementManager.UpdateObjectInSubList(pDPO, sublistId);
	}
	virtual void UpdateDynamicObjectLight(CDynamicPlacementObj* pDPO) override final {
		LightListDirty();
	}
	virtual void LightListDirty() override final {
		m_bLightListDirty = true;
	}

	bool GetDynamicObjectBoundingBox(int placementId, float* x1, float* y1, float* z1, float* x2, float* y2, float* z2);
	bool EnableDynamicObjectBoundingBoxRender(int placementId, bool enable);

	bool CanDynamicObjectSupportAnimation(int placementId);

	bool DynamicObjectCollisionFilter(int placementId, bool filter);

	bool DynamicObjectMakeTranslucent(int placementId);

	bool SendRemoveDynamicObjectEvent(int placementId);

	bool AddDynamicObjectPlacement(int objId, const char* type);

	bool MoveDynamicObjectPlacement(int placementId, double dir, double slide, double lift);
	bool MoveDynamicObjectPlacementInDirection(int placementId, double x, double y, double z, double distance);

	bool SaveDynamicObjectPlacementChanges(int placementId);
	bool CancelDynamicObjectPlacementChanges(int placementId);

	bool ApplyCustomTexture(int objId, int assetId, bool thumbnail, const char* textureURL);
	bool ApplyFriendPicture(int objId, int friendId, bool thumbnail);
	bool ApplyGiftPicture(int objId, int friendId, bool thumbnail);
	bool ApplyCustomTextureWorldObject(int objId, int assetId, bool thumbnail);

	CWldObject* GetWorldObject(int objId);
	bool GetWorldObjectInfo(int objId, int* assetId, std::string* customTexture, bool* canHangPictures, bool* customizableTexture, std::string* textureUrl);

	bool ApplyCustomTextureLocal(int objId, const std::string& fileName);
	bool ApplyCustomTextureWorldObjectLocal(int objId, const std::string& fileName);
	void SaveWorldObjectChanges(int objId, const char* textureURL);
	void CancelWorldObjectChanges(int objId);

	std::string DownloadTexture(int type, int assetId, const char* texUrl, bool thumbnail = false);
	std::string DownloadTextureAsync(int type, int assetId, const char* texUrl, bool thumbnail = false);
	std::string DownloadTextureAsync(const std::string& texFileName, const std::string& texUrl, const std::string& ctPath = "");

	/** DRF
	* This function returns the user configuration file directory used by the script
	* glue functions KEP_OpenConfig() and KEP_OpenFile() when CONFIG_DIR is specified
	* as the directory.  It currently maps to '%APPDATA%\Kaneva\<gameId>' which is
	* outside of the star folder so as not to be overwritten during patch.
	*/
	std::string ConfigDir();

	// -- File access
	IKEPFile* OpenFile(const char* fname, const char* access, std::string& errMsg, const char* dir);
	IKEPConfig* OpenConfig(const char* _fname, const char* rootNodeName, std::string& errMsg);
	bool DeleteAsset(int type, std::string const& baseName);

	// DRF - Performs blocking webcall and return PassGroups bitmap for given username (1=AP, 2=VIP)
	bool WebGetPassGroups(const std::string& username, int& passGroupsUser);
	virtual bool WebGetPlayerCfg(const std::string& username, Vector3d& nameColor, int& passGroupsUser, int& passGroupsWorld) override;

	static DWORD WINAPI _GetBrowserPageAsync(const class WebRequestAsync* request);
	bool GetBrowserPageAsync(const std::string& url, IEvent* completionEvent, int filter = 0, int startIndex = 0, int maxItems = 0, int retries = 1, bool doAuth = true);
	bool GetBrowserPage(const std::string& url, int filter, int startIndex = 0, int maxItems = 0);
	bool _GetBrowserPage(const std::string& url, int startIndex, int maxItems, std::string& resultText, DWORD* httpStatusCode, std::string& httpStatusDescription);
	bool _GetBrowserLogin(const std::string& url, std::string& resultText, DWORD* httpStatusCode, std::string& httpStatusDescription, const std::string& gotoUrlOverride = "");

	bool LaunchBrowser(const char* url);
	HINSTANCE shellExec(const char* url, const char* verb, const char* args);

	std::string GetURL(const char* urlTag, bool bReturnCompleted);

	// Graphics options
	void SetAntiAlias(int aaTypeMax) {
		m_antiAlias = aaTypeMax;
	}
	int GetAntiAlias() const {
		return m_antiAlias;
	}
	void SetLodBias_MovementObjects(int bias, bool biasAuto) {
		m_LodBiasMovObj = bias;
		m_LodBiasMovObjAuto = biasAuto;
	}
	void GetLodBias_MovementObjects(int& bias, bool& biasAuto) const {
		bias = m_LodBiasMovObj;
		biasAuto = m_LodBiasMovObjAuto;
	}
	void SetCullBias_DynamicObjects(int bias, bool biasAuto) {
		m_cullBiasDynObj = bias;
		m_cullBiasDynObjAuto = biasAuto;
	}
	void GetCullBias_DynamicObjects(int& bias, bool& biasAuto) const {
		bias = m_cullBiasDynObj;
		biasAuto = m_cullBiasDynObjAuto;
	}
	void SetCullDist_MovementObjects(float distance) {
		m_cullDistMovObj = distance;
	}
	float GetCullDist_MovementObjects() const {
		return m_cullDistMovObj;
	}
	void SetParticleBias(int bias, bool biasAuto) {
		m_particleBias = bias;
		m_particleBiasAuto = biasAuto;
	}
	void GetParticleBias(int& bias, bool& biasAuto) const {
		bias = m_particleBias;
		biasAuto = m_particleBiasAuto;
	}
	void SetAnimationBias_MovementObjects(int bias, bool biasAuto) {
		m_animBiasMovObj = bias;
		m_animBiasMovObjAuto = biasAuto;
	}
	void GetAnimationBias_MovementObjects(int& bias, bool& biasAuto) const {
		bias = m_animBiasMovObj;
		biasAuto = m_animBiasMovObjAuto;
	}
	void SetAnimationBias_DynamicObjects(int bias, bool biasAuto) {
		m_animBiasDynObj = bias;
		m_animBiasDynObjAuto = biasAuto;
	}
	void GetAnimationBias_DynamicObjects(int& bias, bool& biasAuto) const {
		bias = m_animBiasDynObj;
		biasAuto = m_animBiasDynObjAuto;
	}
	void SetAvatarNames(int names, bool namesAuto) {
		m_avatarNames = names;
		m_avatarNamesAuto = namesAuto;
	}
	void GetAvatarNames(int& names, bool& namesAuto) const {
		names = m_avatarNames;
		namesAuto = m_avatarNamesAuto;
	}
	void SetShadows(bool enabled) {
		m_shadows = enabled;
	}
	bool GetShadows() const {
		return m_shadows;
	}
	void SetInvertY(bool enabled) {
		m_invertY = enabled;
	}
	bool GetInvertY() const {
		return m_invertY;
	}
	void SetMLookSwap(bool enabled) {
		m_mLookSwap = enabled;
	}
	bool GetMLookSwap() const {
		return m_mLookSwap;
	}
	bool ZoneAllowsLabels() {
		return m_zoneAllowsLabels;
	}
	void SetZoneAllowsLabels(bool _allow) {
		m_zoneAllowsLabels = _allow;
	}

	bool IsMouseLookEnabled();
	bool IsCameraAutoFollowEnabled();

	void WriteConfigSettings() {
		ClientEngine_CFrameWnd::WriteStartCfgFile();
	}

	void SetControlConfig(int controlConfig);

	long GetRegKeyValue(HKEY hKey, const char* subKey, const char* name, std::string* pData);

#ifdef EXTERNAL_PING
	// 2016.10.14
	// There is no intention of turning this back on.  However, this is conditioned in the event we should need
	// turn it back on quickly.
	//
	// This can be removed after one release cycle.
	//
	bool ExternalPing(double& pingTimeMs, const std::string& hostName = "", const std::string& service = "");
#endif

	// Saves a screenshot to the 'screenshots' directory using a generated filename
	virtual std::string ScreenshotCapture(const RECT& rect, std::string destFile = "", char* folderOverride = nullptr) override;
	virtual bool ScreenshotIsCapturing() const override { return m_captureScreen; }

	// UGC and uploads
	IUGCManager* GetUGCManager() const {
		return m_ugcMgr;
	}

	virtual bool SubmitLocalDynamicObjectByPlacementId(long placementId, IEvent* submissionFeedbackEvent);

	bool UploadFileAsync(const char* uploadFile, const char* url, int prio, ValueList* pAddlValues = NULL, bool authFirst = false, const char* authUrl = NULL, IEvent* e = NULL, DWORD eventMask = 0);
	bool UploadFileAsync(const char* uploadName, char* dataBuf, DWORD dataBufLen, const char* url, int prio, ValueList* pAddlValues = NULL, bool authFirst = false, const char* authUrl = NULL, IEvent* e = NULL, DWORD eventMask = 0);
	bool UploadMultiFilesAsync(const std::vector<std::string>& uploadFiles, const char* url, int prio, ValueList* pAddlValues = NULL, bool authFirst = false, const char* authUrl = NULL, IEvent* e = NULL, DWORD eventMask = 0);

	BOOL PlayParticleEffectOnPlayer(int playerAssId, const std::string& boneName, int particleSlotId, UINT particleGLID);

	int SendScriptServerMenuEvent(std::vector<std::string> params);
	void sendPlaceDynamicObjectAtPositionEvent(const long long& glid, const short& invenType, const float& posX, const float& posY, const float& posZ, const float& rotX, const float& rotY, const float& rotZ);

	bool SendMouseOverToolTipEvent(const std::string& toolTipText);

	// ED-8451 - Client runs Launcher if not given valid patch URL
	bool RunLauncher();

	// Patch URL - Specified On Command Line (eg. '-khttp://patch.kaneva.com/4.2.0.15312_a/')
	void SetPatchUrl(const std::string& patchUrl) {
		m_patchUrl = patchUrl;
	}
	std::string PatchUrl() const override {
		return m_patchUrl;
	}

	// User Login Parameters
	struct LoginParams {
		bool autoLogin; ///< auto login allowed flag
		ULONG userId; ///< login user's id (eg. 5769908)
		std::string userEmail; ///< login user's email address (eg. 'user@email.com')
		std::string userPassword; ///< login user's password
		std::string userName; ///< login user's username (eg.'user123')
		std::string userGender; ///< login user's gender (eg. 'M')
		std::string authToken;

		LoginParams() :
				autoLogin(false), userId(0) {}
	} m_login;

	// Returns if m_login.autoLogin is flagged and we have a valid m_login.username and m_login.password.
	bool CanAutoLogin() const;

	// Login to WOK using given email and password to set m_login properties. Calls LoginToServer() & WebValidateUser().
	bool Login(const std::string& userEmail, const std::string& userPassword, bool promptAllowAccess = true);

	// Logout of the client
	void Logout();

	// 'validateUser' webcall using m_login params to authenticate user webcalls and avoid [490]KANEVA_AUTH errors.
	bool WebValidateUser();

	bool fillInAuthUrl(std::string& s);
	bool fillInAuthUrl(URL& url);
	bool fillInLoginUrl(std::string& s, const std::string& emailaddress);
	bool fillInLoginUrl(std::string& s, const std::string& emailaddress, const std::string& gotoUrl);
	bool fillInStartAppUrl(std::string& startAppUrl, const std::string& gameUrl);

	// Procedural/simple geometries support for scripts
	virtual int CreateStandardGeometry(const PrimitiveGeomArgs<float>& args) override;
	virtual int CreateCustomGeometry(int geomType, const std::vector<float>& positions, const std::vector<float>& normals) override;
	virtual bool DestroyGeometry(int geomId) override;
	virtual void DestroyAllGeometries() override;
	virtual bool SetGeometryVisible(int geomId, bool visible) override;
	virtual bool MoveGeometry(int geomId, float x, float y, float z, bool bRelative) override;
	virtual bool RotateGeometry(int geomId, float radx, float rady, float radz) override;
	virtual bool ScaleGeometry(int geomId, float sx, float sy, float sz) override;
	virtual bool SetGeometryRefObj(int geomId, ObjectType refType, int refObjId) override;
	virtual bool SetGeometryDrawMode(int geomId, int drawMode) override;
	virtual bool SetGeometryMaterial(int geomId, const TMaterial<float>& material) override;

	virtual void Mount(int aiBotId, int mountType, bool bLocal);
	virtual void Unmount();

	// Bake animation
	virtual bool BakeAnimation_MovementObj(IGetSet* player, const GLID& glid);

	// Load animation onto player skeleton. Return true if all data ready, false otherwise.
	virtual bool LoadAnimationOnPlayer(IGetSet* player, const GLID& animGLID);

	// Get posture matrices
	virtual bool GetPostureMatrices(const GLID& animGLID, TimeMs animTimeMs, std::map<std::string, Matrix44f>& posture);

	// Translate bone name into index (-1 if not found)
	virtual int GetBoneIndexByName(IGetSet* player, const std::string& boneName);

	// UGC asset URL
	virtual ContentMetadata* GetCachedUGCMetadataPtr(const GLID& glid);
	virtual bool GetCachedUGCMetadata(const GLID& glid, ContentMetadata& md);
	virtual void CacheUGCMetadata(const GLID& glid) {
		ClientBaseEngine::UGCItemDataCacheSync(glid);
	}

	// Is current zone customizable by player
	virtual bool IsZoneCustomizableByPlayer() {
		return m_zonePermissions >= 0 && m_zonePermissions <= ZP_MODERATOR;
	}
	// Is current zone owned by player
	virtual bool IsZoneOwnedByPlayer() {
		return m_zonePermissions == ZP_OWNER;
	}

	// User dropped lua script onto a specific object
	bool ProcessDroppedLuaScript(const std::string& fullPath, int objectType, int objectID);
	bool UploadDroppedLuaScript(const std::string& fullPath, int objectID);
	bool UploadAssetTo3DApp(const std::string& filename, const std::string& fullPath, int assetType, unsigned long seekPos, unsigned long objPlacementId);
	static bool classifyAssetType(ScriptServerEvent::UploadAssetTypes assetType, std::string& folder, std::string& extension);
	int countDevToolAssets(int assetType);
	bool getDevToolAssetInfo(int assetType, int index, std::string& name, std::string& fullPath, bool& modified);
	std::string getDevToolAssetPath(int assetType);
	bool Import3DAppAsset(int assetType, const std::string& path);
	bool Import3DAppAssetFromString(int assetType, const std::string& baseName, const std::string& content, std::string& outFullPath);
	bool detectLocal3DAppAssetChanges(std::string const& name, ScriptServerEvent::UploadAssetTypes type);
	void NotifyServerOfAssetUpload(int uploadId, std::string const& name, int assetType);

	int GetKanevaUserId() const {
		return m_userId;
	}

	// Trigger manipulations
	CTriggerObject* AddDynamicObjectTrigger(CDynamicPlacementObj* pDPO, int triggerId, eTriggerAction triggerAction, const PrimitiveGeomArgs<float>& volumeArgs, int intParam, const std::string& strParam) override;
	bool AddDynamicObjectTrigger(int placementId, int triggerId, eTriggerAction triggerAction, const PrimitiveGeomArgs<float>& volumeArgs, int intParam, const std::string& strParam);
	bool PendingDeleteDynamicObjectTriggers(int placementId);
	bool PendingDeleteDynamicObjectTrigger(int placementId, int triggerId);

	// Create UGC particle system and return local GLID
	virtual int CreateNewUGCParticleSystem();

	// Particle system access
	virtual IGetSet* GetParticleSystemByGlid(GLID glid);
	virtual IGetSet* GetParticleSystemByName(const std::string& effectName);
	virtual bool AddParticleSystemToDynamicObject(int placementId, const std::string& effectName);
	virtual void RemoveParticleSystemFromDynamicObject(int placementId);
	virtual bool GetDynamicObjectParticleSystem(int placementId, GLID& glid, std::string& effectName);

	// Update all in-world DOs for position and state changes.
	virtual void UpdateAllDynamicObjects();

	// Update download priorities for all pending resource
	void UpdateDownloadPriorities();

	virtual bool ConfigPlayerEquippableItem(IGetSet* pGS_MO, const CharConfigItem* pCCI) override {
		auto pMO = dynamic_cast<CMovementObj*>(pGS_MO);
		return pMO ? ClientBaseEngine::ConfigPlayerEquippableItem(pMO, pCCI) : false;
	}

	void UGCAvatarUpdate();
	virtual void RequestItemAnimDataAsync(int DOGlid);

	virtual void LoadUGCActor(const GLID& actorGLID) override;
	virtual void addAnimationUGCActor(const GLID& actorGlid, const GLID& animGlid, const std::string& name) override;

	virtual SIZE getClientRectSize();

	void GetGeometryStats(std::ostream& strm);
	void CopyStatsToClipboard();

	bool PickRayHitTest_WorldObject(CWldObject* pWO, const Vector3f& rayOrigin, const Vector3f& rayDir, Vector3f& intersection, Vector3f& normal, float& distance);
	bool PickRayHitTest_DynamicObject(CDynamicPlacementObj* pDPO, const Vector3f& rayOrigin, const Vector3f& rayDir, Vector3f& intersection, Vector3f& normal, float& distance, bool bTestAlpha);
	bool PickRayHitTest_MovementObject(CMovementObj* pMO, const Vector3f& rayOrigin, const Vector3f& rayDir, Vector3f& intersection, Vector3f& normal, float& distance);

	bool SphereTest_DynamicObject(CDynamicPlacementObj* pDPO, const Vector3f& sphereOrigin, float radius, float& distance, Vector3f& intersection, const Vector3f& orientation, float maxAngle);
	bool SphereTest_MovementObject(CMovementObj* pMO, const Vector3f& sphereOrigin, float radius, float& distance, Vector3f& intersection, const Vector3f& orientation, float maxAngle);

	void enableResourceManagers(bool bEnable);
	void cancelPendingResources();

	void setDynamicObjectTexturePanning(int objPlacementId, int meshId, int uvSetId, float uIncr, float vIncr);

	size_t GetNumHumanPlayersInRuntimeList() const {
		return ClientBaseEngine::GetNumHumanPlayersInRuntimeList();
	}

	static bool createAssetFromChunks(std::string fileName, short chunkNumber, BYTE* buffer, unsigned long bytesRead, ScriptServerEvent::UploadAssetTypes type, bool continuedDownload);
	bool requestSourceAssetFromAppServer(int type, std::string name, int seekPos);

	virtual void moveEntitySubstitute(CMovementObj* pMovObj, const Vector3f& newPos, const Vector3f& newDir) override;

	/**
	* This function calls appropriate gamelogin.aspx url to obtain login parameters.
	* If the result from gamelogin.aspx indicates an offline hosted 3dapp server, it will send
	* GoToApp command to incubator to try to bring it up. Once GoToApp is sent, this function will
	* poll gamelogin.aspx up to a pre-set preiod of time until the app server is pinging or timeout.
	* @param emailaddress = user entry from login menu (initial login) / saved email (in-game patching)
	* @param gotoUrl = NULL (initial login) / Kaneva URL (in-game patching)
	*/
	bool getGameLoginInfo(
		const std::string& gotoUrl,
		bool doRenderLoop,
		GL_RESULT& glResult,
		std::string& resultDescription,
		std::string& serverIP,
		LONG& serverPort,
		std::string& serverVersion, // drf - added
		LONG& zoneIndex,
		LONG& gameId,
		std::string& gameName,
		bool& incubatorHosted,
		LONG& parentGameId,
		LONG& errorCode,
		std::string& templatePatchDir,
		std::string& appPatchDir);

	virtual void ResetTextureQualityLevels() override;
	virtual bool EnableTextureQualityLevel(unsigned quality) override; // return false if quality level is unrecognized / unavailable
	virtual void ForceOnePixelTexture(bool force) override;
	virtual bool IsForcingOnePixelTexture() const override;

	// Query metadata of imported texture. Return true if found.
	virtual bool GetTextureImportInfo(const std::string& fileName, TextureDef& textureInfo) override;

	bool IsSATRunning() {
		return m_SATRunning;
	}

	DECLARE_GETSET

protected:
	friend class LoadSceneThread;
	friend class OffscreenWorldRenderer;

	struct TraceInfo {
		ObjectType objType;
		int objId;

		Vector3f intersection; // world-space intersection
		Vector3f normal; // world space normal
		Vector3f objectIntersection; // object-space intersection
		float distance;

		TraceInfo() :
				objType(ObjectType::NONE),
				objId(-1) {}
	};

	enum {
		RAYTRACE_NOT_CULLED = 0x01, // Like RAYTRACE_VISIBLE, but includes culled objects.
		RAYTRACE_NOT_HIDDEN = 0x02, // Like RAYTRACE_VISIBLE, but includes culled objects.
		RAYTRACE_VISIBLE = RAYTRACE_NOT_CULLED | RAYTRACE_NOT_HIDDEN,
		RAYTRACE_SELECTED = 0x04, // test only objects in the selection set
		INTERSECTION_IN_WORLD_SPACE = 0x08, // return a world-space point instead of object space
		RAYTRACE_SKIP_NONCOLLIDABLE_WORLD_OBJECTS = 0x10,
		RAYTRACE_SKIP_TRANSPARENT = 0x20,
	};

	struct ClickPlaceData {
		long long objectGlid;
		long long placementId;
		int invenType;
		TimeMs timeLast;
		Vector3f position;
		Vector3f orientation;
		float placementHeight;
		float gridSpacing;
		ClickPlaceData() {
			objectGlid = 0;
			placementId = 0;
			timeLast = 0;
			position.x = 0.0f;
			position.y = 0.0f;
			position.z = 0.0f;
			orientation.x = 0.0f;
			orientation.y = 0.0f;
			orientation.z = 0.0f;
			placementHeight = 0.0f;
			gridSpacing = 1.0f;
		}
	};

	enum class eCarControl {
		Left,
		Right,
		Forward,
		Reverse,
		Brake,
		TurboBoost, // Knight Rider style
		None,
	};

	class CarSteeringState {
	public:
		enum class eInput {
			KEYBOARD_LEFT,
			KEYBOARD_RIGHT,
			MOUSE_LEFT,
			MOUSE_RIGHT,
			MOUSE_STRAIGHT,
		};

		enum class eDirection {
			LEFT,
			RIGHT,
			STRAIGHT,
		};

		void SetInput(eInput input, double dTime) {
			if (dTime != 0.0) {
				switch (input) {
					case eInput::KEYBOARD_LEFT:
						m_bIsSteeringLeftWithKeyboard = true;
						break;
					case eInput::KEYBOARD_RIGHT:
						m_bIsSteeringRightWithKeyboard = true;
						break;
					case eInput::MOUSE_LEFT:
						m_bIsSteeringLeftWithMouse = true;
						break;
					case eInput::MOUSE_RIGHT:
						m_bIsSteeringRightWithMouse = true;
						break;
					case eInput::MOUSE_STRAIGHT:
						m_bIsSteeringRightWithMouse = false;
						m_bIsSteeringLeftWithMouse = false;
						break;
				}

				if (m_dTime > dTime) {
					m_dTime = dTime;
				}
			}
		}

		void ResetInput() {
			m_eLastDirection = GetDirection();

			m_bIsSteeringLeftWithKeyboard = false;
			m_bIsSteeringRightWithKeyboard = false;
			m_bIsSteeringLeftWithMouse = false;
			m_bIsSteeringRightWithMouse = false;
			m_dTime = 0.0;
		}

		bool HasStateChanged() const {
			eDirection dir = GetDirection();
			return dir != m_eLastDirection;
		}

		double GetTime() const {
			return m_dTime;
		}

		eDirection GetDirection() const {
			const int iSteeringDirection = (int)m_bIsSteeringRightWithKeyboard + (int)m_bIsSteeringRightWithMouse -
										   ((int)m_bIsSteeringLeftWithKeyboard + (int)m_bIsSteeringLeftWithMouse);

			if (iSteeringDirection > 0) {
				return eDirection::RIGHT;
			}
			if (iSteeringDirection < 0) {
				return eDirection::LEFT;
			}
			return eDirection::STRAIGHT;
		}

	private:
		bool m_bIsSteeringLeftWithKeyboard;
		bool m_bIsSteeringRightWithKeyboard;
		bool m_bIsSteeringLeftWithMouse;
		bool m_bIsSteeringRightWithMouse;
		double m_dTime;

		eDirection m_eLastDirection;
	};

	/// DRF - Mouse Buttons Enumeration
	enum MOUSE_BUTTON {
		MOUSE_BUTTON_LEFT,
		MOUSE_BUTTON_MIDDLE,
		MOUSE_BUTTON_RIGHT,
		MOUSE_BUTTONS
	};

	/// DRF - Mouse Buttons State
	struct MOUSE_BUTTON_STATE {
		size_t clicks; // number of clicks since ClicksReset() (last OnActivate())
		Timer clickTimer; // button click down timer
		POINT clickPos; // button click down cursor position

		bool clickOnBlade; // button was clicked down on a blade (menu)
		bool dragAllowed; // button drag is allowed (camera drag)
		bool dragInProgress; // button drag is in progress

		static const LONG dragThreshPos = 150; // drag threshold position (pix^2)
		static const LONG dragThreshTime = 150; // drag threshold time (ms)
		static const LONG CLICK_THRESHOLD_TIME = 500;

		MOUSE_BUTTON_STATE() :
				clickTimer(Timer::State::Paused) {
			ClicksReset();
			ButtonReset();
		}

		// DRF - Click Count
		void ClicksReset() {
			clicks = 0;
		}
		size_t Clicks() const {
			return clicks;
		}

		// DRF - Button State
		void ButtonReset() {
			clickTimer.Pause().Reset();
			clickPos.x = clickPos.y = 0;
			clickOnBlade = false;
			dragAllowed = false;
			dragInProgress = false;
		}
		void ButtonDown(const POINT& pos, bool isOnBlade) {
			++clicks;
			clickTimer.Start();
			clickPos = pos;
			clickOnBlade = isOnBlade;
			dragAllowed = !isOnBlade; // only allow drag while not over blade
		}
		void ButtonUp() {
			ButtonReset();
		}

		// DRF - Drag State - Returns true if drag is in progress otherwise false.
		bool DragInProgress(const POINT& pos) {
			if (!dragInProgress && dragAllowed && clickTimer.IsRunning()) {
				LONG dx = pos.x - clickPos.x;
				LONG dy = pos.y - clickPos.y;
				bool threshPosOk = (((dx * dx) + (dy * dy)) > dragThreshPos);
				bool threshTimeOk = (clickTimer.ElapsedMs() > dragThreshTime);
				if (threshPosOk && threshTimeOk)
					dragInProgress = true;
			}
			return dragInProgress;
		}

		bool DragInProgress() const {
			return dragInProgress;
		}

		bool HasExceededClickThreshold() const {
			return clickTimer.ElapsedMs() > CLICK_THRESHOLD_TIME;
		}

	} m_mouseButton[MOUSE_BUTTONS]; ///< drf - mouse button states

	struct SubstituteRecord {
		ObjectType type;
		int globalId;
		SubstituteRecord(ObjectType t = ObjectType::NONE, int g = 0) :
				type(t), globalId(g) {}
	};

protected:
	///////////////////////////////////////////////////////////////////////////////
	// Protected Functions
	///////////////////////////////////////////////////////////////////////////////

	bool ProcessScriptAttribute(CEXScriptObj* sctPtr);

	virtual bool LoadScene(const std::string& exgFileName, const char* url = nullptr) override;
	void LoadScenePre();
	bool LoadSceneMain(const std::string& exgFileName, const char* url = nullptr);
	bool LoadScenePerformSplashScreen();

	bool InitializeCamerasAndViewports();
	virtual bool LoadEntityDatabase(const std::string& fileName) override;
	BOOL LoadShaderLibrary(const std::string& fileName);
	BOOL LoadGlobalShaderLibrary(const std::string& fileName);
	BOOL LoadPixelShaderLibrary(const std::string& fileName);
	BOOL LoadIconDB(const std::string& fileName);
	void LoadParticleDatabase(const std::string& fileName);
	void LoadSoundManager(const std::string& fileName); // DDR Sounds Only!
	BOOL LoadHouseDB(const std::string& fileName);
	BOOL LoadWorldRules(const std::string& fileName);
	BOOL LoadArenaSys(const std::string& fileName);

	// DRF - DO NOT DELETE - Appears like dead code but isn't
	void LoadTextFontInterfaceFromFile(const std::string& fileName);

#if (DRF_OLD_PORTALS == 1)
	BOOL LoadPortalDatabase(const std::string& fileName);
#endif
	void LoadControlCfgList(const std::string& fileName);
	void DestroyControlList();

	virtual void SetDynamicObjectRM(DynamicObjectRM* sys) {
		m_dynamicObjectRM = sys;
	}
	DynamicObjectRM* GetDynamicObjectRM() {
		return m_dynamicObjectRM;
	}
	SoundRM* GetSoundRM() {
		return m_soundRM;
	}
	EquippableRM* GetEquippableRM() {
		return m_equippableRM;
	}
	ParticleRM* GetParticleDatabase() {
		return m_particleRM;
	}
	ActorDatabase* GetActorDatabase() {
		return m_ugcActorDB;
	}
	CSoundBankList* GetSoundManager() {
		return m_soundManager;
	}

	BOOL UpdateEverything();

	bool InitializeOjectsIntoScene();

	bool InitializeWorldObject(CWldObject* ptrLoc);

	void ConfigEventDispatchTime(unsigned dispatchTimeInGame, unsigned dispatchTimeLoading, bool legacyMode) {
		m_dispatchTimeInGame = dispatchTimeInGame;
		m_dispatchTimeLoading = dispatchTimeLoading;
		m_legacyDispatchTimeMode = legacyMode;
	}

	void UpdatePositionFromPhysicsVehicle(CMovementObj* pMO);

	// Traces a ray through scene and returns info in 'tr' about first object hit
	// 'rayDirection' needs to be normalized
	// 'mask' is any combination of the ObjectType class enum values
	// 'flags' controls the behavior
	bool RayTrace(TraceInfo* tr, const Vector3f& rayOrigin, const Vector3f& rayDirection, float fDistance, ObjectType mask, unsigned long flags, std::function<bool(CDynamicPlacementObj*)> fnDynObjFilter = nullptr);
	bool RayTraceLegacy(TraceInfo* tr, const Vector3f& rayOrigin, const Vector3f& rayDirection, ObjectType mask, unsigned long flags = 0);
	bool GlueRayTrace(const Vector3f& rayOrigin, const Vector3f& rayDirection, ObjectType mask, ObjectType& typeResult, int& idResult, Vector3f& intersection, float& distance);
	bool SphereTestObject(const ObjectType& objType, int objId, Vector3f& sphereOrigin, float radius, Vector3f& intersection, float& distance, const Vector3f& orientation, float maxAngle);
	void GetPickRay(Vector3f* rayOrigin, Vector3f* rayDir, int x, int y);

	// Mouse Over Settings
	void MOS_Log();
	bool MOS_Reset();
	bool MOS_Clear(const MOS::CATEGORY& mosCategory = MOS::CATEGORY_ALL);
	bool MOS_Add(const ObjectType& objType, int objId, const MOS::MODE& mosMode, const std::string& toolTipText = "", const CURSOR_TYPE& cursorType = CURSOR_TYPE_CLICKABLE, const MOS::CATEGORY& mosCategory = MOS::CATEGORY_NONE);
	bool MOS_Get(MOS& mos_out, const ObjectType& objType, int objId) const;
	bool MOS_Sync(const TraceInfo& traceInfo, bool setToolTip);

	// These are the pick ray for this frame
	const Vector3f& GetPickRayOrigin() const {
		return m_pickRayOrigin;
	}
	const Vector3f& GetPickRayDirection() const {
		return m_pickRayDirection;
	}

	void ImmRender_MovementObject(CMovementObj* pMO, LPDIRECT3DDEVICE9 device);
	void ImmRender_DynamicObject(CDynamicPlacementObj* pDPO, LPDIRECT3DDEVICE9 device);
	void ImmRender_WorldObject(const CWldObject* pWO, LPDIRECT3DDEVICE9 device);
	bool ImmRender_SoundPlacement(SoundPlacement* pSP, LPDIRECT3DDEVICE9 device);

	void OutlinesDeviceInit();
	void OutlinesRender();
	int OutlinesRenderObjects(bool usePerObjectZEnableState); // Returns number of objects in view and outlined

	int OutlinesStencilPass(LPDIRECT3DDEVICE9);
	void OutlinesColorPass(LPDIRECT3DDEVICE9);
	void OutlinesBlurPass(LPDIRECT3DDEVICE9, LPDIRECT3DSURFACE9);
	void OutlinesCompositePass(LPDIRECT3DDEVICE9);

	void HighlightsRender();
	void WidgetsRender();

	void UpdateCarSteering();
	void ControlCarUsingKeyboard(eCarControl controlType, const KeyEventHandlerArgs& args);
	void ControlCarUsingMouse(const KeyEventHandlerArgs& args);
	void SteerCarLeft(double dEventTime);
	void SteerCarRight(double dEventTime);
	void SteerCarStraight(double dEventTime);
	void StartSteeringCarWithMouse(double dEventTime);
	void StopSteeringCarWithMouse(double dEventTime);
	bool IsSteeringCarWithMouse() const;
	void DriveCar(bool bActivate, eCarControl eControlType, double dEventTime);
	void ChangeCarSensitivity(bool bIncrease);
	int GetCarSteeringDeadZoneRange();
	void OnVehicleEntered();
	void OnVehicleLeft();
	void LoadKeyBindings();
	void RegisterKeyEventHandlers();
	void EnableDirectInput(bool bEnable);

	void UserControlUse(CMovementObj* entityPtr, int controlConfigNum);
	void ControlsInLoop();

	bool GetCursorInfo(D3DVIEWPORT9& viewport, POINT& cursorPos, bool& cursorInViewport, bool& cursorOverBlade);

	bool MouseKeyboardUpdate();
	bool MouseOverUpdate();
	int ScanCodeToVK(size_t scanCode); //needed because MapVirtualKey does not map arrow keys, home, end etc.
	bool DetectKeyStateChange(ControlInfoObj* aInfoPtr, bool detectPressed);
	virtual void HandleWindowsKeyEvent(int iVirtualKey, bool bDown);
	virtual void HandleWindowsMouseMoveEvent(const Vector2i& vMovement, int iButtonsPressed);
	virtual void HandleWindowsMouseButtonEvent(int iButton, bool bDown);
	virtual void HandleFocusChange(bool bFocusGain);
	virtual void HandleReturnFromNonclientAreaAction();

	// DRF - Queue ScriptServerEvent::Touch (Right Click)
	void SSE_Touch(const TraceInfo& trace);

	// DRF - Queue ScriptServerEvent::ObjectClick (false is left click, true is right click)
	void SSE_ObjectClick(const TraceInfo& trace, bool rightClick);

	// DRF - OnClick Occurs In OnButtonUp If Not OnDown Over Blade (Menu) And Not Left Dragging
	void OnMouseLButtonDown(POINT const& cursorPos, bool isOnBlade);
	void OnMouseLButtonUp(POINT const& cursorPos);
	void OnMouseLClick(POINT const& cursorPos);

	// DRF - OnClick Occurs In OnButtonUp If Not OnDown Over Blade (Menu) And Not Right Dragging
	void OnMouseRButtonDown(POINT const& cursorPos, bool isOnBlade);
	void OnMouseRButtonUp(POINT const& cursorPos);
	void OnMouseRClick(POINT const& cursorPos);

	// DRF - OnClick Occurs In OnButtonUp If Not OnDown Over Blade (Menu) (Is No Middle Drag)
	void OnMouseMButtonDown(POINT const& cursorPos, bool isOnBlade);
	void OnMouseMButtonUp(POINT const& cursorPos);
	void OnMouseMClick(POINT const& cursorPos);

	void OnMouseOver3DWorld(POINT const& cursorPos);

	// DRF - Resets mouse clicks.
	void MouseClicksReset(const MOUSE_BUTTON& mouseButton) {
		m_mouseButton[(int)mouseButton].ClicksReset();
	}
	void MouseClicksReset() {
		for (size_t i = 0; i < MOUSE_BUTTONS; ++i)
			MouseClicksReset((MOUSE_BUTTON)i);
	}

	// DRF - Returns mouse clicks since last reset (occurs in OnActivate()).
	size_t MouseClicks(const MOUSE_BUTTON& mouseButton) const {
		return m_mouseButton[(int)mouseButton].Clicks();
	}
	size_t MouseClicks() const {
		size_t clicks = 0;
		for (size_t i = 0; i < MOUSE_BUTTONS; ++i)
			clicks += MouseClicks((MOUSE_BUTTON)i);
		return clicks;
	}

	// DRF - Returns true if mouse drag is in progress otherwise false.
	bool MouseDragInProgress(const MOUSE_BUTTON& mouseButton) {
		return m_mouseButton[(int)mouseButton].DragInProgress();
	}
	bool MouseDragInProgress() {
		bool dragInProgress = false;
		for (size_t i = 0; i < MOUSE_BUTTONS; ++i)
			dragInProgress |= MouseDragInProgress((MOUSE_BUTTON)i);
		return dragInProgress;
	}

	// DRF - Updates mouse drag state and returns true if drag is in progress otherwise false.
	bool MouseDragInProgress(const MOUSE_BUTTON& mouseButton, const POINT& pos) {
		return m_mouseButton[(int)mouseButton].DragInProgress(pos);
	}
	bool MouseDragInProgress(const POINT& pos) {
		bool dragInProgress = false;
		for (size_t i = 0; i < MOUSE_BUTTONS; ++i)
			dragInProgress |= MouseDragInProgress((MOUSE_BUTTON)i, pos);
		return dragInProgress;
	}

	// Input is Win32 VK values
	bool IsKeyDown(size_t keyCode) const;

	BOOL GetMouseKeyboardUpdate() {
		return m_doMouseKeyboardUpdate;
	}
	void SetMouseKeyboardUpdate(BOOL val) {
		m_doMouseKeyboardUpdate = val;
	}

	void UpdateKey(size_t index);

	void RenderTextCache();

	/** DRF
	* Returns true while client window is in view and false if out of view or minimized.
	*/
	bool IsInView();

	bool RenderAndPresentActiveViewport();

	bool RenderViewport(CViewportObj* pVO);

	bool RenderViewportScene(CViewportObj* pVO);

	void DrawRenderLists(CViewportObj* pVO, const ScenePassOptions& aPassOptions);

	void RenderPhysics();

	void SetRenderBladesRenderOptions();

	BOOL RenderOverlays(CViewportObj* pVO);

	void RenderViewportOutline(CViewportObj* pVO);

#if DEPRECATED
	void MultipassRenderMesh(CEXMeshObj* mesh, int lod);
#endif

	BOOL SetViewMatrixAB(Matrix44f& mat, Vector3f& vFrom, Vector3f& vAt, Vector3f& vWorldUp);

	void RenderLabelsOnTop(bool onTop) {
		m_labelsOnTop = onTop;
	}

	// Dev-mode options
	virtual bool GetMediaScrapeEnabled() const override {
		return m_mediaScrapeEnabled;
	}
	virtual void SetMediaScrapeEnabled(bool enabled) override {
		m_mediaScrapeEnabled = enabled;
	}
	virtual bool GetAnimationsEnabled() const override {
		return m_animationsEnabled;
	}
	virtual void SetAnimationsEnabled(bool enabled) override {
		m_animationsEnabled = enabled;
	}
	virtual bool GetParticlesEnabled() const override {
		return m_particlesEnabled;
	}
	virtual void SetParticlesEnabled(bool enabled) override {
		m_particlesEnabled = enabled;
	}
	virtual bool GetLimitFPS() const override {
		return m_limitFPS;
	}
	virtual void SetLimitFPS(bool enabled) override {
		m_limitFPS = enabled;
	}
	virtual TimeMs GetOnRenderTimeMs() const override {
		return m_menuRenderOptions.m_onRenderTimeMs;
	}
	virtual void SetOnRenderTimeMs(TimeMs timeMs) override {
		m_menuRenderOptions.m_onRenderTimeMs = timeMs;
	}

	// GenerateRenderLists
	void ClearRenderLists(ULONG aClearFlags);
	bool GenerateRenderLists(CViewportObj* pVO, const ScenePassOptions& aPassOptions);
	bool GenRenderList_WorldObjects(CViewportObj* pVO, const ScenePassOptions& aPassOptions);
	bool GenRenderList_MovementObjects(CViewportObj* pVO, const ScenePassOptions& aPassOptions);
	bool GenRenderList_Environment(CViewportObj* pVO, const ScenePassOptions& aPassOptions);
	bool GenRenderList_DynamicSystem(CViewportObj* pVO, const ScenePassOptions& aPassOptions);
	void GenRenderList_OneDynamicObject(CViewportObj* pVO, const ScenePassOptions& aPassOptions, CDynamicPlacementObj* pDPO, const struct OneDynObjArgs& args);
	bool GenRenderList_SoundPlacement(CViewportObj* pVO, const ScenePassOptions& aPassOptions);
	bool GenRenderList_Triggers(CViewportObj* pVO, const ScenePassOptions& aPassOptions);
	void GenRenderList_MediaTriggers(CViewportObj* pVO, const ScenePassOptions& aPassOptions);
	bool GenRenderList_LWG(CViewportObj* pVO, const ScenePassOptions& aPassOptions);
	void GenRenderList_SkeletonLabelHelper(CViewportObj* pVO, const ScenePassOptions& aPassOptions, RuntimeSkeleton* pRS);
	void GenRenderList_Attachments(CViewportObj* pVO, const ScenePassOptions& aPassOption, const Matrix44f& aParentMatrix, RuntimeSkeleton* pRS_parent, bool abFirstPersonMode);

	bool CullDynamicObject(CDynamicPlacementObj* pDPO, RuntimeSkeleton* pRS, const ScenePassOptions& aPassOptions, float fCamDist, const OneDynObjArgs& args);

	void CategorizeMesh(CEXMeshObj* imMesh, const Matrix44f& cameraMatrix, const Vector3f& actualCenter,
		const Matrix44f& invCameraMatrix, const Vector3f& fadeCenter, CReferenceObj* refOpt,
		CMaterialObject* custMaterial = NULL);

	void DeleteWorldObjectGuids(); //editor

	BOOL XSectTest_DynamicObject(CDynamicPlacementObj* pDPO, Vector3f& rayCenter, FLOAT rayRadius);

	void AddGroupFilter(int groupNumber);
	BOOL ValidateGroup(int groupNumber);

	void CreateViewVolumePlanes(const CViewportObj* pVO, Plane3f (&aViewVolumePlanes)[6]) const;
	BOOL IsMeshInView(const CEXMeshObj* mesh) const;
	BOOL IsBoxInView(const ABBOX& cl_box) const;

	bool OutOfFovUntransformed(const Matrix44f& matrix, const ABBOX& bndBox) const;
	bool OutOfFOV(const Vector3f& min, const Vector3f& max) const;
	bool OutOfFOV_CenterHalfSize(const Vector3f& ptBoxCenter, const Vector3f& vBoxHalfSize) const;
	bool OutOfFOV(const Vector3f& ptSphereCenter, float fRadius) const;

	void InitializeEnvironment();

#if (DRF_WORLD_ENVIRONMENT_LIST == 1)
	void RenderEnvironmentAttachedObjects(CViewportObj* pVO, BOOL mirrorModeOn);
#endif

	void ManageEnvEffects(CWldObject* pWO, CEXMeshObj* pMesh);
	void ManageEnvEffects(CWldObject* pWO, CMaterialObject* pMatl);

	virtual IGetSet* GetEnvironment() {
		return m_environment;
	}

	BOOL EvaluateAndRenderCharonaOverlaysV2(CViewportObj* pVO);

	void ReinitAllTexturesFromDatabase();
	void SkeletalTextureReassignment(SkeletonConfiguration* skeletonCfg);
	bool TextureSearch(const std::string& texFileName, DWORD& index, int maxMipLevels);

	void GetScreenXY(
		double localZpos,
		double clipPlane,
		const Vector3f& localizePosition,
		double& xScreenPosition,
		double& yScreenPosition,
		double fov,
		double vPortHAspect,
		double screenWidth,
		double screenHeight);

	bool DisplayTextBasedOn3dPosition(
		const Vector3f& wldSpcPos,
		const Matrix44f& cameraMatrix,
		const std::string& textStr,
		double maxDistance,
		double minDistance,
		double fontSize,
		CTextRenderObj*& pTRO,
		const Vector3d& color,
		double yoffset = 0);

	void CacheEntityForShadow(CMovementObj* entity);
	void RenderSpotShadows();
	void GetShadowPolyMeshSimple(CCollisionObj* collisionObject, ABVERTEX1LNT* buffer, int& returnVCount,
		Vector3f objectPos, float radius, float extentUp, float extentDown);

	void ParticleSystem(int bLoopLoc, Vector3f camLocation, Matrix44f matrixLoc, float fov);
	void SkeletalIsClippedStopParticles(RuntimeSkeleton* skeletonPtr);
	void EntityIsClippedStopParticles(CMovementObj* ptrLoc);
	void RuntimeParticleCleanup();
	virtual void SkeletalParticleEmitterCallback(const Matrix44f& positionalMatrix, RuntimeSkeleton* skeletonPtr, bool killExisting) override;

	void CollisionLoopEntityToEntity();

	ObjectType EnvironmentSegmentTest(
		Vector3f& contactPos,
		float& contactDist,
		const Vector3f& startSegment,
		const Vector3f& endSegment,
		float tolerance = 0.5f,
		const std::function<bool(CDynamicPlacementObj*)>& filter = nullptr);

	ReDynamicGeom* GetDynamicGeom() {
		return m_dynamicGeom;
	}

	bool PlayBySoundID(int id, BOOL looping, double x, double y, double z, float soundFactor, float clipRadius) {
		Vector3f v;
		v.x = (float)x;
		v.y = (float)y;
		v.z = (float)z;
		return m_soundManager->PlayByID(id, looping, v, soundFactor, clipRadius) != 0;
	}

	void PlayBySoundID(int id, BOOL looping) {
		m_soundManager->PlayByID(id, looping);
	}

	virtual void Callback() override;

	// Event Handlers (overidden)
	virtual EVENT_PROC_RC HandleLogonResponseEvent(const char* connectionErrorCode, HRESULT hResultCode, ULONG characterCount, LOGON_RET replyCode) override;
	virtual EVENT_PROC_RC HandleStringListEvent(StringListEvent* sle) override;
	virtual EVENT_PROC_RC HandleSetZonePermissionEvent(int zonePermissions) override;

	// Event Handlers (not overidden)
	static EVENT_PROC_RC HandleClientSpawnEventStatic(ULONG lparam, IDispatcher* disp, IEvent* e);
	EVENT_PROC_RC HandleClientSpawnEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleMovementObjectCfgEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	EVENT_PROC_RC HandleMovementObjectCfgEvent(MovementObjectCfgEvent* ece);
	static EVENT_PROC_RC HandlePlayerInventoryCompletionEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	EVENT_PROC_RC HandlePlayerInventoryCompletionEvent(IEvent* e);
	static EVENT_PROC_RC HandleScriptIconCompletionEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	EVENT_PROC_RC HandleScriptIconCompletionEvent(IEvent* e);
	static EVENT_PROC_RC HandleDownloadedTextureEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandlePendingSpawnEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleDisconnectedEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleAddUGCTextureEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleUpdateSoundUGCEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleUpdateEquippableAnimationEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleChildGameWebCallsReadyEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleShutdownRequestEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleEventQueueProbeEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleGotoPlaceEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleTryOnEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleTryOnExpireEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleTryOnRemoveEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleP2pAnimPermissionEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleP2pAnimEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleDragDropEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleScriptClientEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandlePromptAllowedEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleAddTriggerEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleRemoveTriggerEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleAddParticleEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleRemoveParticleEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleRestartParticleEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleMoveParticleEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleUpdateParticleEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleAddSoundEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleRemoveSoundEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleMoveSoundEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleUpdateSoundEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleSetDynamicObjectTexturePanningEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleClanEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleTitleEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleAddObjectMonitorEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleZoneCustomizationDLCompleteEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleMissing3DAppCharacterEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC UpdateDynamicObjectMediaEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC UpdateDynamicObjectMediaVolumeAndRadiusEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleTextEventStatic(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleCameraEventStatic(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleResourceDownloadCompletionEvent(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC HandleZoneCustomizationSummaryEvent(ULONG lparam, IDispatcher* disp, IEvent* e);

	EVENT_PROC_RC HandleTextEvent(IEvent* e);
	EVENT_PROC_RC HandleCameraEvent(IEvent* e);

	// P2p Animation Functions
	void P2pAnimDataDeref(
		const P2pAnimData& p2pAnimData,
		CMovementObj*& pMO_init,
		CMovementObj*& pMO_target,
		CMovementObj*& pMO_me,
		CMovementObj*& pMO_other,
		eAnimType& animType_me,
		eAnimType& animType_other);
	bool P2pAnimStateInactive();
	bool P2pAnimStateBegin(const P2pAnimData& p2pAnimData);
	bool P2pAnimStateStart();
	bool P2pAnimStateEnd();
	bool P2pAnimStateTerminate();

	virtual int GetMyRefModelIndex();

	bool CreateCharacterOnServer(CMovementObj* pMO, int spwnCfg, const std::string& characterName, int nTeam, float scaleX, float scaleY, float scaleZ);

	std::string GetCompassDir(CMovementObj* pMO);
	void PossesCallback();

	bool SetMenuGameObjectEquippableAltCharConfig(RuntimeSkeleton* pSkeleton, SkeletonConfiguration* pSkeletonCfg, int cfg, int meshId, int matlId, const GLID_OR_BASE& glidOrBase);

	bool SetRuntimeSkeletonAltCharConfig(RuntimeSkeleton* pRS, SkeletonConfiguration* skeletonCfg, int cfg, int meshId, int matlId, int refModel);

	void DeleteSpawnDatabase(); // AI and editor -> move up to ClientBaseEngine

	void CleanupPendingTriggerDeletions();

	bool EvaluateTrigger(CTriggerObject* pTO, eTriggerState triggerState, const Vector3f& pos, CMovementObj* pMO, bool async) override;

	void PendingCommandsCallback();

	void GetInventoryIconInfo(CGlobalInventoryObj* invObj, RECT& textureCoords, std::string& textureName);
	void GetInventoryIconInfo(int iconIndex, RECT& textureCoords, std::string& textureName);

	void UseItemByGLID(const GLID& glid);

	void GenerateEntityLastPosition();
	void UpdateEntityPersist(CPersistObjList** pList); // duplicated in AI

	BOOL HousePlacementValidCheck(const Vector3f& charPosition, ABBOX box);

	bool DownloadExternalTexture(int textureType, bool thumbnail, int assetId, const char* textureUrl, const char* filePath);

	std::string GetExternalTexture(int textureType, bool thumbnail, int assetId, const char* textureUrl);
	bool GetExternalTextureAsync(int textureType, int assetId, const char* textureUrl, IEvent* event, DownloadPriority prio = DL_PRIO_MEDIUM, bool thumbnail = false);

	bool DownloadPatchFile(const char* exgFileName, IEvent* e, FileSize fileSize);
	bool DownloadPatchFile(const char* exgFileName, const char* folder, IEvent* e, FileSize fileSize);
	bool TestPatchFileForUpdate(const char* filename);
	void CancelAllDownloads();
	BOOL CancelPatch();
	bool ArmPreLoadProgress(IEvent* e);

	BOOL deleteFile(const char* fullPath, bool defyReadonlyAttr);

	BOOL WorldToScreen(Vector3f wldPosition, Vector3f* scrPosition);

	std::string buildTexturePath(int textureType, bool thumbnail, int itemId, std::string& url);

	int GetNextLocalPlacementId() {
		return (int)InterlockedDecrement(&m_localPlacementId);
	}

	void ResetLocalPlacementId() {
		m_localPlacementId = 0;
	}

	void RenderMenuGameObjects();

	DECLARE_MESSAGE_MAP()

private:
	// Login to server using m_login properties.
	bool LoginToServer();

	void GameLoginFailed(const GL_RESULT& glResult);

	/**
	* Called after user credentials have been validated (e.g. via GameLogin call).
	* Checks user config to determine if autosave has been enabled and if so
	* (the default if AutoLogon isn't indicated in the configuration file) stores
	* the credentials.
	*/
	void SaveAutoLogonCredentials();

	void SetGameId(int gameId) {
		m_gameId = gameId;
	}
	void SetPrevGameId(int gameId) {
		m_prevGameId = gameId;
	}
	void SetParentGameId(int gameId) {
		m_parentGameId = gameId;
	}
	void SetZoneInstanceId(int zoneInstanceId) {
		m_zoneInstanceId = zoneInstanceId;
	}
	void SetZoneIndex(ZoneIndex zoneIndex) {
		ClientBaseEngine::SetZoneIndex(zoneIndex);
	}

	bool ProcessActionAutoRun(CMovementObj* pMO, TimeMs timeMs);
	bool ProcessActionAccelerate(CMovementObj* pMO);
	bool ProcessActionDecelerate(CMovementObj* pMO);
	bool ProcessActionRightTurn(CMovementObj* pMO);
	bool ProcessActionLeftTurn(CMovementObj* pMO);
	bool ProcessActionRightSidestep(CMovementObj* pMO);
	bool ProcessActionLeftSidestep(CMovementObj* pMO);

	void EventQueueStats(); // Report event queue stats in the log

	void ReCalcSelectionPivot();

	bool GetSnapVector(Vector3f& snapVector, float& snapLenSq, const CDynamicPlacementObj* pDPO_from, const CDynamicPlacementObj* pDPO_to);

	bool IsModalDialogOpen();

	bool HandleVolumeChange(double* pdVolPct, bool* pbMute, bool bFromMixer);

	// Message Senders
	bool QueueMsgEquipToServer(const std::vector<GLID>& glidsSpecial, NETID netTraceId);

	// Message Handlers (overidden)
	virtual bool HandleMsgs(BYTE* pData, NETID netIdFrom, size_t sizeData, bool& bDisposeMessage) override;
	virtual bool HandleMsgAttribToClient(BYTE* pData) override;
	virtual bool HandleMsgSpawnToClient(BYTE* pData, NETID netIdFrom, size_t dataSize) override;

	// Message Handlers (not overidden)
	bool HandleMsgBlockItemToClient(LPVOID lpMessage, int msgSize, NETID& netIdFrom, bool& bDisposeMessage);
#if (DRF_OLD_VENDORS == 1)
	bool HandleMsgBlockStoreToClient(LPVOID lpMessage, int msgSize, NETID& netIdFrom);
#endif

	void LoadChildGameWebCallValues();

	//Ankit ----- APIs for the SATFrameworkMenuScript
	void PostKeyboardMouseMsg(bool isKeybdInput, int isPressed, int code, int dx, int dy);
	int GETGetSetInt(std::string varName);
	void SETGetSetInt(std::string varName, int value);
	std::string GETGetSetString(std::string varName);
	void SETGetSetString(std::string varName, std::string value);
	void LogMsgForSAT(std::string msg);
	void SetScrollBarInMenuToContainIndex(std::string lstName, std::string menuName, int index);
	void SetTextForEditBoxInMenu(std::string text, std::string edtBox, std::string menuName);
	void ComboBoxSetSelectedByDataInMenu(int data, std::string cmbBox, std::string menuName);
	std::string GetInfoBoxMessage();
	bool IsButtonInMenuEnabled(std::string btnName, std::string menuName);
	void UseButtonInsideMenu(std::string btnName, std::string menuName);
	void UseRadioButtonInsideMenu(std::string radioBtnName, std::string menuName);
	bool CheckIfMenuLoaded(std::string menuName);
	bool CheckIfScreenshotImageSaved();
	void CheckIfPacketRedeemedForUser(int packetId, int userId);
	bool CheckIfParticleEffectPlayingOnPlayer(int particleGlid, int playerNetID = 0);
	std::vector<int> GetItemsArmedByPlayer(int particleGlid, int playerNetID = 0);
	bool IsPlayerAP();
	bool IsPlayerVIP();
	bool IsDynamicObjectWithPIDValid(int pid);
	void CreateTestUser(std::string username, std::string password);
	void CloseAllBrowsers();
	bool IsBrowserOpenWithTitleAndURL(std::string windowClassName, std::string titleOfWebpage, std::string URL);
	bool IsChatWindowInForeground();
	void BringKanevaClientToForeground();
	void FinishTestingAndCloseClient(bool successfulCompletion);
	void ExecuteFunction(std::string funcName);
	void GetDevOptionsDefinedInEngine(std::string& devOptionsCombinedString);

	//Ankit ----- Function called to copy the rendertarget/backbuffer data into the image.
	void CaptureScreen(const RECT& rect, const std::string& fullPath);

	// DRF - ED-8089 - Fix Anti-Aliasing Issues
	HRESULT GetRenderTarget_NonMultiSample(CComPtr<IDirect3DSurface9>* pRenderTarget);

	// Download URL to disk, with optional completion event
	virtual bool DownloadFileAsync(ResourceType jobType, const std::string& url, DownloadPriority prio, const std::string& destFile, FileSize expectedFileSize, bool compressed, IEvent* event, bool preload) override;

	// Download URL with data handler callback
	virtual bool DownloadFileAsyncWithHandler(ResourceType jobType, const std::string& url, DownloadPriority prio, DownloadHandler* handler, bool preload) override;

	// Download resource, with optional completion event
	virtual bool DownloadResourceAsync(IResource* res, bool compressed, IEvent* event, bool preload) override;

	// Enable download priority visualization
	virtual void EnableDownloadPriorityVisualization(bool enable) override;
	virtual bool IsDownloadPriorityVisualizationEnabled() const override;

	// Experimental engine options
	virtual void SetExperimentalEngineOption(int option, int value) override;
	virtual int GetExperimentalEngineOption(int option) const override;

	// D3D surface copy between pools
	bool CopyD3DSurfaceFromDefaultPool(IDirect3DSurface9* source, IDirect3DSurface9* dest);

	// Off-screen render task (target surface can be in any pool)
	void QueueOffscreenRenderTask(IDirect3DSurface9* target, WorldRenderStyle style, WorldRenderOptions options, double minRadiusFilter, bool overrideMatrices, const Matrix44f* camMatrix, const Matrix44f* projMatrix);
	bool ProcessOneOffscreenRenderTask();

	// Local, procedurally generated dynamic texture A8R8G8B8. Usage: world map, paintable surface, etc.
	virtual unsigned CreateDynamicTexture(unsigned width, unsigned height) override;
	virtual void DestroyDynamicTexture(unsigned dynamicTextureId) override;
	virtual IDirect3DTexture9* GetDynamicTexture(unsigned dynamicTextureId) override;

	// World map generation support
	virtual void GetWorldMapBoundary(WorldRenderOptions options, double minRadiusFilter, double& minX, double& minY, double& minZ, double& maxX, double& maxY, double& maxZ) override;
	virtual bool GenerateWorldMap(unsigned dynTextureId, WorldRenderStyle style, WorldRenderOptions options, double minRadiusFilter, double minX, double minY, double minZ, double maxX, double maxY, double maxZ) override;

	// Object texture highlight
	virtual bool SetDynamicObjectHighlight(int placementId, bool highlight, float r, float g, float b, float a) override;

	// Suspend/Resume server connection
	virtual void DisableClient() override;
	virtual void EnableClient() override;
	virtual bool IsClientEnabled() const override;

	virtual FrameTimeProfiler* GetFrameTimeProfiler() const override final {
		return m_frameTimeProfilerEnabled ? m_frameTimeProfiler : nullptr;
	}

	void LogFrameTimeProfilerReport();

	virtual void EnableFrameTimeProfiler(bool enable) override {
		if (m_frameTimeProfilerEnabled != enable) {
			if (!enable) {
				LogFrameTimeProfilerReport();
			}
			m_frameTimeProfilerEnabled = enable;
		}
	}

	virtual bool IsFrameTimeProfilerEnabled() const {
		return m_frameTimeProfilerEnabled;
	}

	virtual bool GetMovementObjectByPlacementId(int placement, CMovementObj*& pMovObj, CMovementObj*& pMovObjRefModel);

	// #MLB_BulletSensor - ClientEngine LandClaim
	virtual bool IsObjectInLandClaim(int objectPid, int landClaimPid) {
		return ClientBaseEngine::IsObjectInLandClaim(objectPid, landClaimPid);
	}

	virtual bool RegisterLandClaim(int pid) override {
		return ClientBaseEngine::RegisterLandClaim(pid);
	}

	virtual bool UnRegisterLandClaim(int pid) override {
		return ClientBaseEngine::UnRegisterLandClaim(pid);
	}

	void InvalidateDynamicObject(int placementId);

	void CrashFlashProcess();

	LONG PatchGameUrl(
		const char* url,
		const char* allowAccessUrl,
		const char* email,
		const char* password,
		const char* baseDir,
		LONG gameId,
		LONG parentGameId,
		std::string& gameName,
		std::string& parentServer,
		LONG& parentPort,
		std::string& templatePatchDir,
		std::string& appPatchDir);

	IMenuBlade* KepMenuInstance();

	BOOL promptAllowAccessAtLogin(const char* gameUrl, const char* accessUrl, LONG gameId);

	size_t GetPaperDollMemoryUsage(std::ostream& strm) const;

	void RezoneStart();
	void RezoneStop();

private:
	std::string m_loginName; // drf - copy of m_login.userName for legacy GetSet
	std::string m_loginGender; // drf - copy of m_login.userGender for legacy GetSet
	size_t m_loginCount; // login counter within this client session
	int m_userId; //kaneva user id for disconnect reporting
	bool m_firstLogin; //used for launching first time user experience survey

	std::string m_gameName;
	int m_gameId; // current game id
	int m_prevGameId; // prev game id of last zone
	int m_parentGameId; // parent game id, zero if a parent
	int m_zoneInstanceId; // in case we need to know it
	int m_zonePermissions; // 0 = gm, 1 = owner, 2 = moderator, 3 = subscriber

	std::string m_patchUrl;
	std::string m_templatePatchDir;
	std::string m_appPatchDir;
	bool m_legacyAppPatch;

	std::string m_wokUrl;
	std::string m_unsecureWokUrl;
	std::string m_kanevaUrl;
	std::string m_shopUrl;

	UINT m_wmKEPClientService;

	IEvent* m_reconnectEvent; // used to reconnect to another server

	bool m_startingUp;
	bool m_shutdown; // used to indicate to blades when shutdown occurs (versus reload)
	bool m_closeOnStartup;

	bool m_commandlineOverrideIPPort; // if specified on the command line, don't use kaneva web scrape data
	bool m_gotoplayeronstartup;
	bool m_gotoapartmentonstartup;
	bool m_gotochannelonstartup;
	int m_gotoonstartupId;
	std::string m_gotochannelurlonstartup;
	std::string m_urlParameters;

	// TryOn State
	bool m_tryOnOnStartup;
	int m_tryOnGlid, m_tryOnClothingGlid;
	USE_TYPE m_tryonUseType;
	std::string m_inTryOnState;
	int m_tryOnObjPlacementId;

	TaskSystem m_taskSystem_WebCall; // Async Script GetBrowserPage Tasks
	static const size_t m_taskThreads_WebCallTask = 4; // number of task system worker threads

	TaskSystem m_taskSystem_ContentService; // Async ContentServiceGLIDCacheTasks
	static const size_t m_taskThreads_ContentService = 4; // number of task system worker threads

	TextureRM* m_textureRM;
	MeshRM* m_meshRM;
	AnimationRM* m_animationRM;
	SkeletonRM* m_skeletonRM;
	SoundRM* m_soundRM;

	CCameraObjList* m_pCameraList;

	CViewportList* m_pViewportList;

	RuntimeCameras* m_pRuntimeCameras;

	ControlDBList* m_controlsList; // DRF - This holds the mapping of keyboard/mouse commands 'space' -> action 'jump' controls
	CTranslucentPolyList* m_translucentPolyList;
	CTranslucentPolyList* m_renderContainerList;
	CTranslucentPolyList* m_renderContainerAlphaFilterList;
	CDynamicOverlayList* m_inViewEntityList; // used for some icons on top of clanmates' head
#if (DRF_WORLD_MIRROR_LIST == 1)
	CWldObjectList* m_worldMirrorList;
#endif
#if (DRF_WORLD_ENVIRONMENT_LIST == 1)
	CWldObjectList* m_worldEnvironmentList;
#endif
	CEXMeshObjList* m_visualsRenderList; // holds "charona" data
	CEXScriptObjList* m_sceneScriptExec;
	CEXScriptObjList* m_scriptList;
	CIconSysList* m_iconDatabase;
	CSoundBankList* m_soundManager;
	IDirectSound8* m_pDirectSound;
	CPendingCommandObjList* m_pendingObject;
	CPersistObjList* m_scenePersistList;
	CWorldChannelRuleList* m_worldChannelRuleDB;
	CBattleSchedulerList* m_battleSchedulerSys;
#if (DRF_OLD_PORTALS == 1)
	CPortalSysList* m_portalDatabase;
#endif
	CFilterSpeechList* m_filterSpeechDB;
	CLightningObjList* m_lightningSystem;
	CBackGroundObjList* m_backgroundList;
	CSpawnObjList* m_spawnGeneratorDB; // AI and editor -> move up to ClientBaseEngine
	CGlobalMaterialShaderList* m_cgGlobalShaderDB;
	CShaderObjList* m_shaderSystem;
	CShaderObjList* m_pxShaderSystem;
	int m_globalWorldShader; //-1 = no shader-enable fixed pipeline
	TextCommands m_textCommands; // for translating text to command events (2/16/05)

	CRuntimeViewportList* m_runtimeViewportDB;
	CRTLightObjList* m_runtimeLightSys;
	bool m_bLightListDirty = false;
	ReDynamicGeom* m_dynamicGeom;
	CMenuGameObjects* m_pMenuGameObjects;
	CMenuList* m_menuList;
	CTextRenderObjList* m_textRenderList;
	CStateManagementObj* m_pRenderStateMgr;
	CTranslucentPolyList* m_shadowObjectsList;
	CMovementObjList* m_shadowEntityList;
	ABVERTEX1LNT m_shadowVertexBuffer[300];
	DWORD m_shadowIndexTemp; // spot shadow texture index

	std::map<int, LWG::IGeom*> m_lwgAllGeoms; // created geometries dumpster -- for cleanup purposes if not done by script
	std::map<int, SubstituteRecord> m_entitySubstitutes;
	KEP3DObjectWidget* m_kepWidget;

	std::string m_abGroupList; // DEPRECATED - GETSET

	CLoadingProgressDlg* m_pWin32ProgDlg;
	int m_progressUpdateTotal;
	int m_progressUpdateCurrent;

	DWORD m_mainThreadId; ///< DRF - main thread unique identifier

	unsigned m_eventQueueStatsCounter; // Iteration ID
	double m_eventQueueStatsInterval; // In milliseconds. 0 = disabled. Interval between two event queue depth stats calls.
	double m_eventQueueStatsTime;
	Logger m_eventQueueStatsLogger;

	// Pending render tasks to be feed through render loops
	std::deque<OffscreenRenderTask*> m_offscreenRenderTasks;

	// Frame Time Profiler
	FrameTimeProfiler* m_frameTimeProfiler;
	bool m_frameTimeProfilerEnabled;

	// list of server-spawned NPCs (actors with dynamic object linkage)
	std::map<int, CMovementObj*> m_npcs;

	bool m_loadScene;
	std::string m_loadSceneExgFileName;

	std::string m_localDevBaseDir;
	std::string m_pathBaseConfig;

	bool m_activated; // drf - added - we are activated when our application takes focus (OnActivate)
	BOOL m_paused;

	// Dispatcher settings
	unsigned m_dispatchTimeInGame;
	unsigned m_dispatchTimeLoading;
	bool m_legacyDispatchTimeMode; // For A/B testing. If TRUE, getDispatchTime() only considers m_spawning flag. Otherwise both m_spawning and m_zoneLoadingUIActive are used.

	// Ray-Casting
	Vector3f m_pickRayOrigin, m_pickRayDirection;

	// Mouse Over
	std::vector<MOS> m_mosVector; // mouse over settings for all objects
	MOS m_mos; // mouse over setting active
	TraceInfo m_mouseOver; // update regularly inside of MouseOverUpdate
	ObjectType m_mouseOverLastObjectType;
	Timer m_mouseOverUpdateTimer;

	unsigned long m_rolloverMode;

	// Object outline
	LPDIRECT3DTEXTURE9 m_pGlowTEX[2]; // offscreen render targets for outline rendering
	static const int GLOW_DOWNSAMPLE_FACTOR = 4;

	// used to detect pressed key for action-key assignment
	CPendingControlSystem* m_pendingControlSystem;

	std::map<int, unsigned int> m_restrictMovements;
	bool m_pitchLock;
	bool m_widgetEventOccured;
	bool m_menuEventOccured;
	bool m_keyCaptured; // drf - key captured by menu

	std::unique_ptr<InputSystem> m_pInputSystem;

	// DRF - Keyboard State
	KeyboardState m_keyboardState;
	KeyboardState m_keyboardStateWas;

	int m_keyboardPresses[DIK_KEYS]; //tracks how many times a key is pressed consecutively (including holding down key)
	TimeDelay m_keyboardPressedTimer[DIK_KEYS];

	// DRF - Mouse State
	MouseState m_mouseState;
	MouseState m_mouseStateWas;

	bool m_bIsDirectInputMouseProbeAllowed;

	// DRF - Cursor State
	bool m_cursorInViewportWas;

	// Flag to disable mouse and keyboard input during
	// rendering. Used by the Editor to allow user to see
	// ongoing changes without their keyboard/mouse interferring.
	BOOL m_doMouseKeyboardUpdate;
	float m_mouseNormalizedX; // cursor x position normalized [0.0 -> 1.0]
	float m_mouseNormalizedY; // cursor y position normalized [0.0 -> 1.0]
	float m_mouseHiResX;
	float m_mouseHiResY;
	BOOL m_invertMouse;

	UINT m_typeOnSize; // drf - added
	TimeMs m_timeRefresh; // drf - added resize timer

	ReD3DX9DeviceState* m_pReDeviceState;
	D3DCAPS9 m_devcaps;
	D3DCOLOR m_backgroundColorGlobal;
	D3DCOLOR m_backgroundColorBlack;

	float m_readoutFontX;
	float m_readoutFontY;
	float m_readoutFontSpacing;
	float m_readoutMaxMeasuredLength;

	bool m_cameraCollision;
	double m_cameraMoveSpeed;
	double m_cameraZoomSpeed; // drf - added
	bool m_cameraLock;

	bool m_mediaScrapeEnabled = true;
	bool m_animationsEnabled = true;
	bool m_particlesEnabled = true;
	bool m_limitFPS = true;

	MenuRenderOptions m_menuRenderOptions;

	BOOL m_showAllNames;
	int m_groupFilterArray[100]; //maximum group filters
	int m_groupFilterCount;
	Plane3f m_aViewVolumePlanes[6]; // Planes in world space.
	bool m_labelsOnTop;
	BOOL m_bRenderEnvironment;

	std::shared_ptr<IEventHandler> m_zoneCustomizationDownloadHandler;

	EVENT_ID m_zoneLoadedEventId;
	EVENT_ID m_downloadZoneEventId;
	EVENT_ID m_downloadLooseTextureEventId;
	EVENT_ID m_requestSoundItemDataId;
	EVENT_ID m_updateSoundUGCEventId;
	EVENT_ID m_dialogEventId;
	EVENT_ID m_keyPressedEvent;
	EVENT_ID m_keyChangedEvent;
	EVENT_ID m_entityCollisionEventId;
	EVENT_ID m_rebirthEventId;
	EVENT_ID m_controlsUpdatedEventId;
	EVENT_ID m_setTargetEventId;
	EVENT_ID m_targetInfoEventId;
	EVENT_ID m_cameraEventId;
	EVENT_ID m_inputEventId;
	EVENT_ID m_tryOnRemoveEventId;
	EVENT_ID m_p2pAnimRequestNotifyEventId;
	EVENT_ID m_browserPageReadyEventId;
	EVENT_ID m_selectEventId;
	EVENT_ID m_clickEventId;
	EVENT_ID m_mouseOverEventId;
	EVENT_ID m_widgetTranslationEventId;
	EVENT_ID m_widgetRotationEventId;
	EVENT_ID m_getItemAnimationsId;
	EVENT_ID m_zoneLoadingStartEventId;
	EVENT_ID m_zoneLoadingCompleteEventId;
	EVENT_ID m_zoneLoadingFailedEventId;
	EVENT_ID m_itemsMetadataResultEventId;
	EVENT_ID m_3DAppAssetDownloadCompleteEventId;
	EVENT_ID m_3DAppAssetUploadNameConflictEventId;
	EVENT_ID m_3DAppAssetPublishCompleteEventId;
	EVENT_ID m_3DAppAssetPublishDeleteCompleteEventId;
	EVENT_ID m_unreadChatEventId;
	EVENT_ID m_chatViewedEventId;
	EVENT_ID m_trayOpenEventId;
	EVENT_ID m_trayMinimizedEventId;
	EVENT_ID m_trayMissingEventId;
	EVENT_ID m_saveActionItemEventId;
	EVENT_ID m_saveActionItemResponseEventId;
	EVENT_ID m_downloadActionItemEventId; // for action items coming from shop.
	EVENT_ID m_smartYesNoMenuEventId;
	EVENT_ID m_smartKeyListenerEventId;
	EVENT_ID m_smartCloseMenuEventId;
	EVENT_ID m_smartMoveMenuEventId;
	EVENT_ID m_smartTextTimerEventId;
	EVENT_ID m_smartButtonEventId;
	EVENT_ID m_smartStatusEventId;
	EVENT_ID m_smartListBoxMenuEventId;
	EVENT_ID m_mouseOverToolTipEventId;
	EVENT_ID m_addIndicatorEventId;
	EVENT_ID m_clearObjectIndicatorsEventId;
	EVENT_ID m_smartCoinHUDEventId;
	EVENT_ID m_smartHealthHUDEventId;
	EVENT_ID m_smartXPHUDEventId;
	EVENT_ID m_addHealthIndicatorEventId;
	EVENT_ID m_removeHealthIndicatorEventId;
	EVENT_ID m_openShopMenuEventId;
	EVENT_ID m_addItemToShopEventId;
	EVENT_ID m_showStatusInfoEventId;
	EVENT_ID m_clientResizeEventId;
	EVENT_ID m_titleSetEventId;
	EVENT_ID m_eventQueueProbeEventId;
	EVENT_ID m_prevImgAvailableEventId;
	EVENT_ID m_clientBackInViewEventId; //Ankit ----- event for when client is activated(brought out of minimized state)
	EVENT_ID m_RepositionVehicleAfterExitEventId;
	EVENT_ID m_RequestPlayerRespawnEventId; // #MLB_ED_7947 add new event RequestPlayerRespawnEvent

	// UGC and drag-drop
	IAssetsDatabase* m_assetsDatabase; // Moved up from KEPEditView
	IAssetImporterFactory* m_assetImporterFactory; // Moved up from KEPEditView
	IUGCManager* m_ugcMgr;
	IAssetImporter* m_textureImporter;
	long m_localPlacementId; // Local dynamic object placement Id
	IDropTarget* m_dropTargetProxy;
	bool m_bDragDropRegistered, m_bAllowDragDrop;
	int m_currDropType;
	std::string m_currDropContent;

	std::string m_pendingLuaScriptFilePath;
	int m_pendingLuaScriptObjectId;

	BOOL m_selectionModeOn;
	BOOL m_bMenuSelectModeOn; //used to disable selection when menu is open
	LegacyModelProxy* m_selectedLmp;

	float m_smallUGCPopOutDist;
	float m_mediumUGCPopOutDist;

	BOOL m_respawning;
	TimeMs m_respawnTimeStamp;
	TimeMs m_respawnWait;
	TimeMs m_controlTimeStamp;

	BOOL m_entityCollisionOn;
	REFBOX* m_goodSubBox; // cached, so it can be used if frames aren't rendered to re-build the trigger list for world objects

	DynamicObjectSpawnMonitor* m_DOSpawnMonitor;

	CFloraObj* m_floraObjTemp;

	std::string m_interactStr; // entity to entity collision interact string (city vendor 'Press TAB To Interact')

	int m_originalCharacterCount;

	bool m_vmFix; // drf - fix parallels on mac vm problems

	TimeMs m_durationToServer;
	TimeMs m_durationToServerStamp;

	ClickPlaceData m_clickPlaceData;

	selection_t m_selectionList;
	Vector3f m_selectionPivot;
	BOOL m_selectionLocked;

	int m_mediaDupeObjId; // object id of duped media player

	std::shared_ptr<IFlashSystem> m_pFlashSystem;

	// Global Master Volume (sounds, media, everything)
	mutable std::mutex m_mtxVolume;
	bool m_bMute = false;
	double m_masterVolPct; // master volume percent globally
	std::shared_ptr<IWindowsMixerProxy> m_pWindowsMixer;

	// Global Media Volume (-1=no change, 0=mute)
	double m_mediaVolPct; // media volume percent globally

	// ED-4317 - Media Audio Volume Multipliers
	double m_globalMediaVolMult = 1.0; // 0.0=silent -> 1.0=full
	double m_localMediaVolMult = 1.0; // 0.0=silent -> 1.0=full

	// ED-4317 - Media In Range Flags
	bool m_isMediaGlobalInRangeAudio = false;
	bool m_isMediaGlobalInRangeVideo = false;
	bool m_isMediaLocalInRangeAudio = false;
	bool m_isMediaLocalInRangeVideo = false;

	// These are used by OpenConfig, although they're not actually exposed in the scripting API.
	typedef std::map<std::string, IKEPConfig*> ConfigMap;
	typedef std::pair<std::string, IKEPConfig*> ConfigPair;
	ConfigMap m_loadedConfigs;

	TimeMs m_clickHandlerStamp;

	std::string m_pathLuaCompiler;

	HRESULT m_hrTCL; // drf - TestCooperativeLevel Result
	bool m_inView; // drf - client window is in view

	std::string m_runtimeId;

	std::string m_authUrl; // url used to authenticate to the website
	std::string m_runtimeReportUrl; // drf - url used to report runtime stats to the website
	std::string m_loginUrl; // url used to gather data prior to a user's login that will direct them to the correct server, return their userid from e-mail address, support MMI3D (meet me in 3D)
	std::string m_startAppUrl; // url to start an incubator-hosted 3d app
	std::string m_allowAccessUrl; // url to get details for the Allow Access dialog for 3d Apps
	std::string m_imageUrl;
	std::string m_imageThumbUrl;
	std::string m_friendUrl;
	std::string m_friendThumbUrl;
	std::string m_giftUrl;
	std::string m_giftThumbUrl;
	std::string m_imageServerUrl;
	std::string m_zoneCutomizationsUrl;
	std::string m_errorReportUrl;
	std::string m_inventoryServerUrl;
	std::string m_childGameWebCallUrl;
	std::string m_itemAnimationsUrl;
	std::string m_scriptDownloadUrl;
	std::string m_pingLocation;

	bool m_zoneAllowsLabels;

	bool m_childGameWebCallValuesLoaded;
	std::vector<std::string> m_childGameWebCallValues;

	bool m_captureScreen;
	RECT m_captureScreenRect;
	std::string m_captureScreenPath;

	//Ankit ----- Dev Mode functionality. Map to convert strings to function pointers.
	std::map<std::string, std::function<void()>> mapForDevOptionFunctions;

	//Ankit - SAT Framework
	bool m_SATRunning; // flag to state that in Testing mode
	SATFramework* m_SATFrameworkInstance;

	// Car Control
	int m_iCarSteeringPointer;
	bool m_bIsSteeringCarWithMouse;
	static int s_iCarSteeringSensitivity;
	CarSteeringState m_xCarSteeringState;

	std::map<int, double> m_speedWas; // objId -> speedWas

	Matrix44f m_mirrorTransform;

	DownloadManager* m_pDM;
	UploadManager* m_pUM;

	bool m_renderSoundPlacements;
	bool m_renderDynamicTriggers;
	bool m_renderWorldTriggers;
	bool m_renderBoundingBoxForAllDynamicObjects; // Global bounding box rendering flag - overrides per-DO settings
	bool m_renderMediaTriggers;

	std::function<bool()> m_RenderSplash; // Encapsulates function and data used to render the splash screen. Reset to nullptr when splash is no longer needed in order to free memory.

	float m_fRezoneRenderIntervalSeconds = 1.0f;
};

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)     \
	{                       \
		if (p) {            \
			(p)->Release(); \
			(p) = nullptr;  \
		}                   \
	}
#endif

} // namespace KEP