///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "EffectTrack.h"
#include "Core/Math/Vector.h"

namespace KEP {

class IClientEngine;
class IDispatcher;
class CDynamicPlacementObj;
class CMovementObj;

// animGlidSpecial < 0 = reverse animation
class AnimationEffect : public IEffectEvent {
public:
	AnimationEffect(TimeMs offset, CMovementObj* pMO, GLID animGlidSpecial) :
			IEffectEvent(offset), m_pMO(pMO), m_pDPO(nullptr), m_animGlidSpecial(animGlidSpecial) {}

	AnimationEffect(TimeMs offset, CDynamicPlacementObj* pDPO, GLID animGlidSpecial) :
			IEffectEvent(offset), m_pMO(nullptr), m_pDPO(pDPO), m_animGlidSpecial(animGlidSpecial) {}

	virtual ~AnimationEffect() {}

	virtual void execute();

private:
	AnimationEffect operator=(const AnimationEffect&);
	AnimationEffect(const AnimationEffect&);

	CMovementObj* m_pMO;
	CDynamicPlacementObj* m_pDPO;
	GLID m_animGlidSpecial; // <0 = reverse animation
};

class StartParticleEffect : public IEffectEvent {
public:
	StartParticleEffect(TimeMs offset, IClientEngine* pICE, int objId, const std::string& boneName, int slot, GLID particleGlid, const Vector3f& ofv, const Vector3f& fwd, const Vector3f& upd) :
			IEffectEvent(offset), m_pICE(pICE), m_objId(objId), m_boneName(boneName), m_slot(slot), m_particleGlid(particleGlid), m_offsetVector(ofv), m_forwardDirection(fwd), m_upDirection(upd) {}
	virtual ~StartParticleEffect() {}

	virtual void execute();

private:
	StartParticleEffect operator=(const StartParticleEffect&);
	StartParticleEffect(const StartParticleEffect&);

	IClientEngine* m_pICE;
	int m_objId;
	std::string m_boneName;
	int m_slot;
	GLID m_particleGlid;
	Vector3f m_offsetVector;
	Vector3f m_forwardDirection;
	Vector3f m_upDirection;
};

class StopParticleEffect : public IEffectEvent {
public:
	StopParticleEffect(TimeMs offset, IClientEngine* pICE, int objId, const std::string& boneName, int slot) :
			IEffectEvent(offset), m_pICE(pICE), m_objId(objId), m_boneName(boneName), m_slot(slot) {}
	virtual ~StopParticleEffect() {}

	virtual void execute();

private:
	StopParticleEffect operator=(const StopParticleEffect&);
	StopParticleEffect(const StopParticleEffect&);

	IClientEngine* m_pICE;
	int m_objId;
	std::string m_boneName;
	int m_slot;
};

class SoundEffect : public IEffectEvent {
public:
	SoundEffect(TimeMs offset, IClientEngine* pICE, int objId, GLID soundGlid, bool stop = false) :
			IEffectEvent(offset), m_soundGlid(soundGlid), m_objId(objId), m_pICE(pICE), m_stop(stop) {}
	virtual ~SoundEffect() {}

	virtual void execute();

private:
	SoundEffect operator=(const SoundEffect&);
	SoundEffect(const SoundEffect&);

	GLID m_soundGlid;
	int m_objId;
	IClientEngine* m_pICE;
	bool m_stop;
};

/*
* This works on an extant sound object and allows the effect track
* to start and stop it on cue.
*/
class SoundEffectObject : public IEffectEvent {
public:
	SoundEffectObject(TimeMs offset, IClientEngine* pICE, int soundId, bool stop = false) :
			IEffectEvent(offset), m_id(soundId), m_pICE(pICE), m_stop(stop) {}
	virtual ~SoundEffectObject() {}

	virtual void execute();

private:
	SoundEffectObject operator=(const SoundEffectObject&);
	SoundEffectObject(const SoundEffectObject&);

	int m_id;
	IClientEngine* m_pICE;
	bool m_stop;
};

class MoveEffect : public IEffectEvent {
public:
	MoveEffect(TimeMs offset, IDispatcher* dispatcher, int objId, TimeMs time, double x, double y, double z) :
			IEffectEvent(offset), m_dispatcher(dispatcher), m_objId(objId), m_time(time), m_x(x), m_y(y), m_z(z) {}
	virtual ~MoveEffect() {}
	virtual void execute();

private:
	MoveEffect operator=(const MoveEffect&);
	MoveEffect(const MoveEffect&);

	int m_objId;
	TimeMs m_time;
	double m_x, m_y, m_z;
	IDispatcher* m_dispatcher;
};

class RotateEffect : public IEffectEvent {
public:
	RotateEffect(TimeMs offset, IDispatcher* dispatcher, int objId, TimeMs time, double x, double y, double z) :
			IEffectEvent(offset), m_dispatcher(dispatcher), m_objId(objId), m_time(time), m_x(x), m_y(y), m_z(z) {}
	virtual ~RotateEffect() {}
	virtual void execute();

private:
	RotateEffect operator=(const RotateEffect&);
	RotateEffect(const RotateEffect&);

	int m_objId;
	TimeMs m_time;
	double m_x, m_y, m_z; // angles
	IDispatcher* m_dispatcher;
};

} // namespace KEP