///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "Common\KEPUtil\FrameTimeProfiler.h"
#include "CCharonaClass.h"

namespace KEP {

static LogInstance("ClientEngine");

bool ClientEngine::InitializeWorldObject(CWldObject* pWO) {
	if (!pWO || !pWO->m_meshObject)
		return false;

	if (pWO->m_meshObject->blade_mesh)
		return true;

	if (pWO->m_meshObject && !pWO->GetReMesh() && !pWO->m_meshObject->m_abVertexArray.m_indecieCount)
		return false;

	if (pWO->m_meshObject) {
		if (pWO->m_meshObject->m_materialObject) {
			if (m_cgGlobalShaderDB)
				pWO->m_meshObject->InitBuffer(g_pD3dDevice, m_cgGlobalShaderDB->GetByIndex(pWO->m_meshObject->m_materialObject->m_optionalShaderIndex - 1));
		} else
			pWO->m_meshObject->InitBuffer(g_pD3dDevice, NULL);

		if (pWO->m_meshObject->m_charona && pWO->m_meshObject->m_charona->m_visual)
			pWO->m_meshObject->m_charona->m_visual->InitBuffer(g_pD3dDevice, pWO->m_meshObject->m_charona->m_visual->m_uvCount);
	}

	if (pWO->m_attribute)
		pWO->m_attribute->m_inUse = FALSE;

#if (DRF_WORLD_ENVIRONMENT_LIST == 1)
	pWO->m_poppedOut = TRUE;
#endif
	if (pWO->m_useSound == TRUE)
		pWO->m_isPlaying = FALSE;

	return true;
}

CWldObject* ClientEngine::GetWorldObject(int objId) {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	if (!m_pWOL)
		return nullptr;
	for (POSITION pos = m_pWOL->GetHeadPosition(); pos;) {
		auto pWO = (CWldObject*)m_pWOL->GetNext(pos);
		if (pWO->m_identity == objId)
			return pWO;
	}
	return nullptr;
}

bool ClientEngine::GetWorldObjectInfo(int objId, int* assetId, std::string* customTexture, bool* canHangPictures, bool* customizable, std::string* textureUrl) {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	auto pWO = GetWorldObject(objId);
	if (!pWO) {
		*assetId = 0;
		*customTexture = "";
		*canHangPictures = false;
		*customizable = false;
		*textureUrl = "";
		return false;
	}
	*assetId = pWO->m_assetId;
	*customTexture = pWO->GetCustomTexture();
	*canHangPictures = pWO->m_canHangPictures != 0;
	*customizable = pWO->m_customizableTexture != 0;
	*textureUrl = pWO->GetCustomTextureUrl();
	return true;
}

bool ClientEngine::IsWorldObjectCustomizableByIdentity(int objId) {
	if (!IsZoneCustomizableByPlayer())
		return false;

	// Get World Object
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	auto pWO = GetWorldObject(objId);
	return (pWO ? (pWO->m_customizableTexture != FALSE) : false);
}

void ClientEngine::SaveWorldObjectChanges(int objId, const char* textureURL) {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	auto pWO = GetWorldObject(objId);
	if (!pWO)
		return;

	pWO->SaveChanges();

	std::string textureFileName = pWO->GetCustomTexture();

	//the file name is the asset id
	std::string::size_type begin = textureFileName.find_last_of('\\');
	std::string::size_type end = textureFileName.find_last_of('.');

	std::string idStr = textureFileName.substr(begin + 1, end - begin - 1);
	char* pc;
	int assetId = (int)strtol(idStr.c_str(), &pc, 10);

	//update server
	std::string uniqueIdStr;
	unsigned char* s = 0;
	VERIFY(UuidToStringA(&(pWO->m_uniqueId), &s) == S_OK);
	uniqueIdStr = (const char*)s;
	RpcStringFreeA(&s);
	if (textureFileName.find(".dds") != std::string::npos) {
		IEvent* e = new UpdateWorldObjectTextureEvent(uniqueIdStr, pWO->m_assetId, textureURL);
		e->AddTo(SERVER_NETID);
		m_dispatcher.QueueEvent(e);
	} else {
		IEvent* e = new UpdateWorldObjectTextureEvent(uniqueIdStr, assetId, textureURL);
		e->AddTo(SERVER_NETID);
		m_dispatcher.QueueEvent(e);
	}
}

void ClientEngine::CancelWorldObjectChanges(int objId) {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	auto pWO = GetWorldObject(objId);
	if (!pWO)
		return;
	pWO->CancelChanges();
}

void ClientEngine::DeleteWorldObjectGuids() {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	if (!m_pWOL)
		return;

	//Serialize will create new ones on save
	POSITION posLast;
	for (POSITION posLocal = m_pWOL->GetHeadPosition(); (posLast = posLocal) != NULL;) {
		auto pWO = (CWldObject*)m_pWOL->GetNext(posLocal);
		if (pWO) {
			if (memcmp(&pWO->m_uniqueId, &GUID_NULL, sizeof(GUID)) != 0)
				memcpy(&pWO->m_uniqueId, &GUID_NULL, sizeof(pWO->m_uniqueId));
		}
	}
}

} // namespace KEP