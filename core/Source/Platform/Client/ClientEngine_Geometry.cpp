///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "Common\include\CoreStatic\LightweightGeom.h"

using namespace KEP::LWG;

namespace KEP {

static LogInstance("ClientEngine");

int ClientEngine::CreateStandardGeometry(const PrimitiveGeomArgs<float>& args) {
	if (!IGeomFactory::Instance())
		return 0;

	IGeom* geom = IGeomFactory::Instance()->createStandardGeometry(args);
	if (!geom)
		return -1;

	int geomId = geom->getUniqueId();
	m_lwgAllGeoms.insert(std::make_pair(geomId, geom));
	return geomId;
}

int ClientEngine::CreateCustomGeometry(int geomType, const std::vector<float>& positions, const std::vector<float>& normals) {
	if (!IGeomFactory::Instance())
		return 0;

	IGeom* geom = IGeomFactory::Instance()->createCustomGeometry(geomType, positions, normals);
	if (!geom)
		return -1;

	int geomId = geom->getUniqueId();
	m_lwgAllGeoms.insert(std::make_pair(geomId, geom));
	return geomId;
}

bool ClientEngine::DestroyGeometry(int geomId) {
	auto itr = m_lwgAllGeoms.find(geomId);
	if (itr == m_lwgAllGeoms.end())
		return false;

	IGeom* geom = itr->second;

	// now remove it from list and dispose
	m_lwgAllGeoms.erase(itr);

	if (geom != nullptr) {
		delete geom;
	}
	return true;
}

void ClientEngine::DestroyAllGeometries() {
	for (auto& pr : m_lwgAllGeoms) {
		assert(pr.second);
		delete pr.second;
	}
	m_lwgAllGeoms.clear();
}

bool ClientEngine::SetGeometryVisible(int geomId, bool visible) {
	// check if we have it (not created somewhere else)
	auto itr = m_lwgAllGeoms.find(geomId);
	if (itr == m_lwgAllGeoms.end())
		return false;

	itr->second->setVisible(visible);
	return true;
}

bool ClientEngine::MoveGeometry(int geomId, float x, float y, float z, bool bRelative) {
	auto itr = m_lwgAllGeoms.find(geomId);
	if (itr == m_lwgAllGeoms.end())
		return false;

	IGeom* geom = itr->second;
	if (!geom)
		return false;

	Vector3f pt(x, y, z);
	geom->setPosition(bRelative ? pt + geom->getPosition() : pt);
	return true;
}

bool ClientEngine::RotateGeometry(int geomId, float radX, float radY, float radZ) {
	auto itr = m_lwgAllGeoms.find(geomId);
	if (itr == m_lwgAllGeoms.end())
		return false;

	IGeom* geom = itr->second;
	if (!geom)
		return false;

	geom->setRotationAngles(Vector3f(radX, radY, radZ));
	return true;
}

bool ClientEngine::ScaleGeometry(int geomId, float sx, float sy, float sz) {
	auto itr = m_lwgAllGeoms.find(geomId);
	if (itr == m_lwgAllGeoms.end())
		return false;

	IGeom* geom = itr->second;
	if (!geom)
		return false;

	geom->setScale(Vector3f(sx, sy, sz));
	return true;
}

bool ClientEngine::SetGeometryRefObj(int geomId, ObjectType refType, int refObjId) {
	auto itr = m_lwgAllGeoms.find(geomId);
	if (itr == m_lwgAllGeoms.end())
		return false;

	IGeom* geom = itr->second;
	if (!geom)
		return false;

	geom->setReferenceObj(refType, refObjId);
	return true;
}

bool ClientEngine::SetGeometryDrawMode(int geomId, int drawMode) {
	auto itr = m_lwgAllGeoms.find(geomId);
	if (itr == m_lwgAllGeoms.end())
		return false;

	IGeom* geom = itr->second;
	if (!geom)
		return false;

	geom->setDrawMode(drawMode);
	return true;
}

bool ClientEngine::SetGeometryMaterial(int geomId, const TMaterial<float>& material) {
	auto itr = m_lwgAllGeoms.find(geomId);
	if (itr == m_lwgAllGeoms.end())
		return false;

	IGeom* geom = itr->second;
	if (!geom)
		return false;

	geom->setMaterial(material);
	return true;
}

} // namespace KEP