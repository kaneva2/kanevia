///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LoadingProgressDlg.h"

namespace KEP {

IMPLEMENT_DYNAMIC(CLoadingProgressDlg, CDialog)

CLoadingProgressDlg::CLoadingProgressDlg(CWnd* pParent /*=NULL*/) :
		CDialog(CLoadingProgressDlg::IDD, pParent), m_percentage(0) {
}

CLoadingProgressDlg::~CLoadingProgressDlg() {
}

void CLoadingProgressDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CLoadingProgressDlg, CDialog)
ON_MESSAGE(WM_INITDIALOG, OnInitDialog)
ON_MESSAGE(WM_DESTROY, OnDestroy)
ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

void CLoadingProgressDlg::UpdateProgress(int percent) {
	if (percent != m_percentage) {
		m_percentage = percent;
		m_progressCtrl.SetPos(m_percentage);
	}
}

// CLoadingProgressDlg message handlers

LRESULT CLoadingProgressDlg::OnInitDialog(WPARAM wParam, LPARAM lParam) {
	m_blackBrush.CreateStockObject(BLACK_BRUSH);
	m_progressCtrl.SubclassDlgItem(IDC_LOADINGPROGRESS, this);
	m_progressCtrl.SetRange(0, 100);
	return S_OK;
}

LRESULT CLoadingProgressDlg::OnDestroy(WPARAM wParam, LPARAM lParam) {
	m_progressCtrl.UnsubclassWindow();
	return S_OK;
}

HBRUSH CLoadingProgressDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) {
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	switch (nCtlColor) {
		case CTLCOLOR_DLG:
			hbr = m_blackBrush;
			break;

		case CTLCOLOR_STATIC:
			//pDC->SetBkMode( TRANSPARENT );
			pDC->SetBkColor(RGB(0, 0, 0));
			pDC->SetTextColor(RGB(0xff, 0xff, 0xff));
			hbr = m_blackBrush;
			break;
	}

	return hbr;
}

} // namespace KEP