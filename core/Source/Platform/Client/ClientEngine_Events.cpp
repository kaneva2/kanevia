///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "CMultiplayerClass.h"
#include "ActionEvent.h"
#include "PrimitiveGeomArgs.h"
#include "TriggerFile.h"
#include "PatchReturns.h"
#include "SlashCommand.h"
#include "TextureProvider.h"
#include "SkeletonLabels.h"
#include "ResourceManagers\DownloadManager.h"
#include "ResourceManagers\UploadManager.h"
#include "ResourceLoader.h"

namespace KEP {

static LogInstance("ClientEngine");

#define DECLARE_CE_PTR(pCE)              \
	auto pCE = ClientEngine::Instance(); \
	if (!pCE)                            \
		return EVENT_PROC_RC::NOT_PROCESSED;

class TriggerHandler : public IEventHandler {
public:
	TriggerHandler(Dispatcher& disp, ClientEngine& engine) :
			m_dispatcher(disp),
			m_engine(engine) {
	}

	virtual EVENT_PROC_RC ProcessEvent(IDispatcher* disp, IEvent* e) {
		auto pEvent = dynamic_cast<TriggerEvent*>(e);
		if (!pEvent)
			return EVENT_PROC_RC::NOT_PROCESSED;
		e->AddTo(SERVER_NETID);
		m_dispatcher.QueueEvent(e);
		return EVENT_PROC_RC::CONSUMED;
	}

private:
	Dispatcher& m_dispatcher;
	ClientEngine& m_engine;
};

class MenuControlSelectedHandler : public IEventHandler {
public:
	MenuControlSelectedHandler(Dispatcher& disp, ClientEngine& engine) :
			m_dispatcher(disp),
			m_engine(engine) {
	}

	virtual EVENT_PROC_RC ProcessEvent(IDispatcher* disp, IEvent* e) {
		return EVENT_PROC_RC::CONSUMED;
	}

private:
	Dispatcher& m_dispatcher;
	ClientEngine& m_engine;
};

class ActionHandler : public IEventHandler {
public:
	ActionHandler(Dispatcher& disp, ClientEngine& engine) :
			m_dispatcher(disp),
			m_engine(engine) {
	}

	virtual EVENT_PROC_RC ProcessEvent(IDispatcher* disp, IEvent* e) {
		ActionEvent* aEvent = dynamic_cast<ActionEvent*>(e);

		if (aEvent) {
			ControlObjActionState act_state;
			CMovementObj* entityPtr;
			ControlInfoObj* control_info_obj;
			TimeMs curTimeMs;
			aEvent->ExtractInfo(act_state, &entityPtr, &control_info_obj, curTimeMs);
			m_engine.ProcessAction(act_state, entityPtr, control_info_obj, curTimeMs);

			return EVENT_PROC_RC::CONSUMED;
		} else
			return EVENT_PROC_RC::NOT_PROCESSED;
	}

private:
	Dispatcher& m_dispatcher;
	ClientEngine& m_engine;
};

EVENT_ID ClientEngine::RegisterNamedEvent(const std::string& eventName, EVENT_PRIORITY pri) {
	return m_dispatcher.RegisterEvent(eventName.c_str(), PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, pri);
}

/** DRF
* This function calls the PreInitialize->InitializeKEPEvents/Handlers->PostInitialize menu script functions after a menu is loaded.
*/
bool ClientEngine::RegisterVMsEvents(IScriptVM* vm, const char* filename) {
	bool ret = true;

	int count = 0;
	for (const auto& pEB : m_eventBlades) {
		auto pHandler = dynamic_cast<IScriptingEventHandler*>(pEB.get());
		if (!pHandler)
			continue;

		count++;

		if (pHandler->FunctionRegistration(*vm)) {
			// Call Menu Script InitializeKEPEvents/Handlers/KEP_PostInitialize Functions
#ifdef _ClientOutput
			pHandler->PreInitialize(*vm, &m_dispatcher, m_dispatcher.EncodingFactory(), filename); // DRF - added only for client
#endif
			pHandler->RegisterScriptingEvents(*vm, &m_dispatcher, m_dispatcher.EncodingFactory(), filename);
			pHandler->RegisterScriptingEventHandlers(*vm, &m_dispatcher, m_dispatcher.EncodingFactory(), filename);
#ifdef _ClientOutput
			pHandler->PostInitialize(*vm, &m_dispatcher, m_dispatcher.EncodingFactory(), filename); // DRF - added only for client
#endif
		} else {
			if ((strstr(typeid(*vm).name(), "Lua") != 0 && strstr(typeid(*pEB).name(), "Lua") != 0) ||
				(strstr(typeid(*vm).name(), "Python") != 0 && strstr(typeid(*pEB).name(), "Python") != 0)) {
				// suppress this if unregistering Lua in Python, or vice versa
				LogDebug("Failed to register vm of type " << typeid(*vm).name() << " into vm " << typeid(*pEB).name());
				ret = false;
			}
		}
	}

	if (count == 0) {
		LogFatal("No script handlers (Lua or Python) registered");
	}
	return ret;
}

bool ClientEngine::UnRegisterVMsEvents(IScriptVM* vm) {
	bool ret = true;
	for (const auto& pEB : m_eventBlades) {
		auto pHandler = dynamic_cast<IScriptingEventHandler*>(pEB.get());
		if (!pHandler)
			continue;

		if (!pHandler->UnRegisterScriptingEventHandlers(&m_dispatcher, vm)) {
			if ((strstr(typeid(*vm).name(), "Lua") != 0 && strstr(typeid(*pHandler).name(), "Lua") != 0) ||
				(strstr(typeid(*vm).name(), "Python") != 0 && strstr(typeid(*pHandler).name(), "Python") != 0)) {
				// suppress this if unregistering Lua in Python, or vice versa
				LogDebug("Failed to unregister vm of type " << typeid(*vm).name() << " in handler of " << typeid(*pHandler).name());
				ret = false;
			}
		}
	}
	return ret;
}

bool ClientEngine::InitEvents() {
	bool ret = true;

	try {
		RegisterEvent<TriggerEvent>(m_dispatcher);
		RegisterEvent<ActionEvent>(m_dispatcher);
		RegisterEvent<GotoPlaceEvent>(m_dispatcher);
		RegisterEvent<PendingSpawnEvent>(m_dispatcher);
		RegisterEvent<TryOnExpireEvent>(m_dispatcher);
		RegisterEvent<TryOnInventoryEvent>(m_dispatcher);
		RegisterEvent<MountEvent>(m_dispatcher);
		RegisterEvent<P2pAnimPermissionEvent>(m_dispatcher);
		RegisterEvent<P2pAnimEvent>(m_dispatcher);
		RegisterEvent<GenericInfoBoxEvent>(m_dispatcher);
		RegisterEvent<DragDropEvent>(m_dispatcher);
		RegisterEvent<DragLeaveEvent>(m_dispatcher);
		RegisterEvent<TargetedPositionEventEx>(m_dispatcher);
		RegisterEvent<AddTriggerEvent>(m_dispatcher);
		RegisterEvent<RemoveTriggerEvent>(m_dispatcher);
		RegisterEvent<ContentServiceCompletionEvent>(m_dispatcher);
		RegisterEvent<ClanEvent>(m_dispatcher);
		RegisterEvent<TitleEvent>(m_dispatcher);
		RegisterEvent<ZoneCustomizationSummaryEvent>(m_dispatcher);
		RegisterEvent<ResourceDownloadCompletionEvent>(m_dispatcher);

		m_tryOnRemoveEventId = RegisterNamedEvent("TryOnRemoveEvent");
		m_keyPressedEvent = RegisterNamedEvent("Keyboard");
		m_keyChangedEvent = RegisterNamedEvent("KeyChangedEvent");
		RegisterNamedEvent("DDRMenuEvent");
		m_dialogEventId = RegisterNamedEvent("DialogEvent");
		m_zoneLoadedEventId = RegisterNamedEvent("ZoneLoadedEvent");
		m_entityCollisionEventId = RegisterNamedEvent("EntityCollisionEvent");
		m_rebirthEventId = RegisterNamedEvent("RebirthEvent");
		m_controlsUpdatedEventId = RegisterNamedEvent("ControlsUpdatedEvent");
		m_setTargetEventId = RegisterNamedEvent("SetTargetEvent");
		m_targetInfoEventId = RegisterNamedEvent("TargetInfoEvent");
		m_cameraEventId = RegisterNamedEvent("CameraEvent");
		m_inputEventId = RegisterNamedEvent("InputEvent");
		m_downloadZoneEventId = RegisterNamedEvent("ZoneDownloadEvent");
		m_downloadLooseTextureEventId = RegisterNamedEvent("LooseTextureDownloadEvent");
		m_requestSoundItemDataId = RegisterNamedEvent("RequestSoundDataEvent");
		m_p2pAnimRequestNotifyEventId = RegisterNamedEvent("P2pAnimRequestNotifyEvent");
		m_browserPageReadyEventId = RegisterNamedEvent("BrowserPageReadyEvent");
		m_selectEventId = RegisterNamedEvent("SelectEvent", EP_HIGH);
		m_clickEventId = RegisterNamedEvent("ClickEvent", EP_HIGH);
		m_mouseOverEventId = RegisterNamedEvent("MouseOverEvent");
		m_widgetTranslationEventId = RegisterNamedEvent("WidgetTranslationEvent");
		m_widgetRotationEventId = RegisterNamedEvent("WidgetRotationEvent");
		m_getItemAnimationsId = RegisterNamedEvent("GetItemAnimationsEvent");
		m_updateSoundUGCEventId = RegisterNamedEvent("UpdateSoundUGCEvent");
		EVENT_ID promptAllowedEventId = RegisterNamedEvent("PromptAllowedEvent");
		m_zoneLoadingStartEventId = RegisterNamedEvent("ZoneLoadingStartEvent");
		m_zoneLoadingCompleteEventId = RegisterNamedEvent("ZoneLoadingCompleteEvent");
		m_zoneLoadingFailedEventId = RegisterNamedEvent("ZoneLoadingFailedEvent");
		m_itemsMetadataResultEventId = RegisterNamedEvent("ItemsMetadataResultEvent");
		m_3DAppAssetDownloadCompleteEventId = RegisterNamedEvent("3DAppAssetDownloadCompleteEvent", EP_HIGH);
		m_smartCloseMenuEventId = RegisterNamedEvent("SmartCloseMenuEvent", EP_HIGH);
		m_smartYesNoMenuEventId = RegisterNamedEvent("SmartYesNoMenuEvent", EP_HIGH);
		m_smartKeyListenerEventId = RegisterNamedEvent("SmartKeyListenerMenuEvent", EP_HIGH);
		m_smartTextTimerEventId = RegisterNamedEvent("SmartTextTimerMenuEvent", EP_HIGH);
		m_smartButtonEventId = RegisterNamedEvent("SmartButtonMenuEvent", EP_HIGH);
		m_smartStatusEventId = RegisterNamedEvent("SmartStatusMenuEvent", EP_HIGH);
		m_smartMoveMenuEventId = RegisterNamedEvent("SmartMoveMenuEvent", EP_HIGH);
		m_smartListBoxMenuEventId = RegisterNamedEvent("SmartListBoxMenuEvent", EP_HIGH);
		m_smartCoinHUDEventId = RegisterNamedEvent("SmartCoinHUDEvent", EP_HIGH);
		m_smartHealthHUDEventId = RegisterNamedEvent("SmartHealthHUDEvent", EP_HIGH);
		m_smartXPHUDEventId = RegisterNamedEvent("SmartXPHUDEvent", EP_HIGH);
		m_addHealthIndicatorEventId = RegisterNamedEvent("AddHealthIndicatorEvent", EP_HIGH);
		m_removeHealthIndicatorEventId = RegisterNamedEvent("RemoveHealthIndicatorEvent", EP_HIGH);
		m_openShopMenuEventId = RegisterNamedEvent("OpenShopMenuEvent", EP_HIGH);
		m_addItemToShopEventId = RegisterNamedEvent("AddItemToShopEventEvent", EP_HIGH);
		m_showStatusInfoEventId = RegisterNamedEvent("PlayerShowStatusInfoEvent", EP_HIGH);
		m_3DAppAssetUploadNameConflictEventId = RegisterNamedEvent("3DAppAssetUploadNameConflictEvent");
		m_3DAppAssetPublishCompleteEventId = RegisterNamedEvent("3DAppAssetPublishCompleteEvent", EP_HIGH);
		m_3DAppAssetPublishDeleteCompleteEventId = RegisterNamedEvent("3DAppAssetPublishDeleteCompleteEvent", EP_HIGH);
		m_unreadChatEventId = RegisterNamedEvent("UnReadChatMessagesEvent");
		m_chatViewedEventId = RegisterNamedEvent("RoomChatViewedEvent");
		m_trayOpenEventId = RegisterNamedEvent("RoomChatOpenEvent");
		m_trayMinimizedEventId = RegisterNamedEvent("RoomChatMinimizedEvent");
		m_trayMissingEventId = RegisterNamedEvent("RoomChatMissingEvent");
		m_saveActionItemEventId = RegisterNamedEvent("SaveActionItemEvent");
		m_saveActionItemResponseEventId = RegisterNamedEvent("SaveActionItemResultEvent");
		m_downloadActionItemEventId = RegisterNamedEvent("DownloadActionItemEvent");
		m_mouseOverToolTipEventId = RegisterNamedEvent("MouseOverToolTipEvent", EP_HIGH);
		m_addIndicatorEventId = RegisterNamedEvent("AddIndicatorEvent", EP_HIGH);
		m_clearObjectIndicatorsEventId = RegisterNamedEvent("ClearObjectIndicatorsEvent", EP_HIGH);
		m_clientResizeEventId = RegisterNamedEvent("ClientResizeEvent", EP_HIGH);
		m_titleSetEventId = RegisterNamedEvent("TitleSetEvent");
		m_eventQueueProbeEventId = RegisterNamedEvent("EventQueueProbeEvent");
		m_prevImgAvailableEventId = RegisterNamedEvent("PrevImgAvailableEvent", EP_HIGH);

		//Ankit ----- Event for when client is activated after being out of view
		m_clientBackInViewEventId = RegisterNamedEvent("ClientBackInViewEvent", EP_HIGH);

		// EP_LOW guarantees that it's delivered after the AddDynamicObjectEvents that it's supposed to indicate have all been processed,
		// even if someone goes and reduces the priority of the ADOE in future.
		RegisterNamedEvent("ZoneCustomizationDLCompleteEvent", EP_LOW);

		RegisterNamedEvent("ShowToolTipEvent");
		RegisterNamedEvent("ParticleEffectCreatedEvent");
		m_RepositionVehicleAfterExitEventId = RegisterNamedEvent("RepositionVehicleAfterExitEvent");

		// #MLB_ED_7947 new event RequestPlayerRespawnEvent
		m_RequestPlayerRespawnEventId = RegisterNamedEvent("RequestPlayerRespawnEvent");

		EVENT_ID shutdownRequestEventId = RegisterNamedEvent("ShutdownRequestEvent");

		ASSERT(m_zoneLoadedEventId != BAD_EVENT_ID && m_entityCollisionEventId != BAD_EVENT_ID &&
			   m_rebirthEventId != BAD_EVENT_ID && m_controlsUpdatedEventId != BAD_EVENT_ID && m_cameraEventId != BAD_EVENT_ID &&
			   m_inputEventId != BAD_EVENT_ID &&
			   m_p2pAnimRequestNotifyEventId != BAD_EVENT_ID &&
			   m_browserPageReadyEventId != BAD_EVENT_ID);

		// load and register the trigger events
		std::string path = PathGameFiles();

		CTriggerObjectList list;
		TriggerFile::RegisterAllTriggerEvents(path.c_str(), m_dispatcher, &list);

		// Register particle events
		RegisterParticleEvents(m_dispatcher);

		//Register sound events
		RegisterSoundEvents(m_dispatcher);

		if (ClientBaseEngine::InitEvents()) {
			// register our internal handlers
			// set our own low-priority trigger handler for each trigger that just sends it on to the server
			// that way higher priority local handlers can consume it
			auto triggerHandler = std::make_shared<TriggerHandler>(m_dispatcher, *this);
			m_dispatcher.RegisterHandler(triggerHandler, TriggerEvent::ClassVersion(), TriggerEvent::ClassId(), EP_LOW);
			m_dispatcher.RegisterHandler(HandleTextEventStatic, (ULONG)this, "TextEvent", TextEvent::ClassVersion(), TextEvent::ClassId(), EP_LOW);
			auto actionHandler = std::make_shared<ActionHandler>(m_dispatcher, *this);
			m_dispatcher.RegisterHandler(actionHandler, ActionEvent::ClassVersion(), ActionEvent::ClassId(), EP_LOW);
			m_dispatcher.RegisterHandler(HandleCameraEventStatic, (ULONG)this, "CameraEvent", PackVersion(1, 0, 0, 0), m_cameraEventId, EP_NORMAL);
			m_dispatcher.RegisterHandler(UpdateDynamicObjectMediaEventHandler, (ULONG)this, "UpdateDynamicObjectMediaEvent", UpdateDynamicObjectMediaEvent::ClassVersion(), UpdateDynamicObjectMediaEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(UpdateDynamicObjectMediaVolumeAndRadiusEventHandler, (ULONG)this, "UpdateDynamicObjectMediaVolumeAndRadiusEvent", UpdateDynamicObjectMediaVolumeAndRadiusEvent::ClassVersion(), UpdateDynamicObjectMediaVolumeAndRadiusEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleGotoPlaceEvent, (ULONG)this, "GotoPlaceEvent", GotoPlaceEvent::ClassVersion(), GotoPlaceEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterFilteredHandler(HandleTryOnEvent, (ULONG)this, "TryOnHandler", GenericEvent::ClassVersion(), m_attribEventId, (int)eAttribEventType::PlayerInfo);
			m_dispatcher.RegisterFilteredHandler(HandleTryOnExpireEvent, (ULONG)this, "TryOnExpireEvent", TryOnExpireEvent::ClassVersion(), TryOnExpireEvent::ClassId(), TryOnExpireEvent::INFORM_EXPIRATION);
			m_dispatcher.RegisterHandler(HandleTryOnRemoveEvent, (ULONG)this, "TryOnRemoveEvent", PackVersion(1, 0, 0, 0), m_tryOnRemoveEventId, EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleDownloadedTextureEvent, (ULONG)this, "TextureDownloadEvent", PackVersion(1, 0, 0, 0), m_dynObjectTextureEventId, EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleDownloadedTextureEvent, (ULONG)this, "TextureDownloadEvent", PackVersion(1, 0, 0, 0), m_friendObjectTextureEventId, EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleDownloadedTextureEvent, (ULONG)this, "TextureDownloadEvent", PackVersion(1, 0, 0, 0), m_worldObjectTextureEventId, EP_NORMAL);
			m_dispatcher.RegisterHandler(HandlePendingSpawnEvent, (ULONG)this, "PendingSpawnEventHandler", PackVersion(1, 0, 0, 0), PendingSpawnEvent::ClassId(), EP_LOW);
			m_dispatcher.RegisterHandler(HandleAddUGCTextureEvent, (ULONG)this, "IconMetadataReadyEvent", PackVersion(1, 0, 0, 0), m_iconMetadataReadyEventId);
			m_dispatcher.RegisterHandler(HandleChildGameWebCallsReadyEvent, (ULONG)this, "ChildGameWebCallsReadyEvent", GenericEvent::ClassVersion(), m_childGameWebCallsReadyEventId);
			m_dispatcher.RegisterHandler(HandleDisconnectedEvent, (ULONG)this, "DisconnectedEvent", DisconnectedEvent::ClassVersion(), DisconnectedEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleClientSpawnEventStatic, (ULONG)this, "ClientSpawnEvent", ClientSpawnEvent::ClassVersion(), ClientSpawnEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(SoundRM::ProcessSoundDataEvent, (ULONG)this, "SoundDataResponseEvent", PackVersion(1, 0, 0, 0), m_requestSoundItemDataId);
			m_dispatcher.RegisterHandler(HandleUpdateSoundUGCEvent, (ULONG)this, "UpdateSoundUGCEvent", PackVersion(1, 0, 0, 0), m_updateSoundUGCEventId);
			m_dispatcher.RegisterHandler(HandleResourceDownloadCompletionEvent, (ULONG)this, "ResourceDownloadCompletionEvent", PackVersion(1, 0, 0, 0), ResourceDownloadCompletionEvent::ClassId());
			m_dispatcher.RegisterHandler(HandleUpdateEquippableAnimationEvent, (ULONG)this, "UpdateEquippableAnimationEvent", PackVersion(1, 0, 0, 0), UpdateEquippableAnimationEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleP2pAnimPermissionEvent, (ULONG)this, "P2pAnimPermissionEvent", PackVersion(1, 0, 0, 0), P2pAnimPermissionEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleP2pAnimEvent, (ULONG)this, "P2pAnimEvent", PackVersion(1, 0, 0, 0), P2pAnimEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterFilteredHandler(HandlePlayerInventoryCompletionEvent, (ULONG)this, "ContentServiceCompletionEvent", PackVersion(1, 0, 0, 0), ContentServiceCompletionEvent::ClassId(), ContentServiceCompletionEvent::PLAYER_INVENTORY);
			m_dispatcher.RegisterFilteredHandler(HandleScriptIconCompletionEvent, (ULONG)this, "ContentServiceCompletionEvent", PackVersion(1, 0, 0, 0), ContentServiceCompletionEvent::ClassId(), ContentServiceCompletionEvent::SCRIPT_ICON, EP_HIGH);
			m_dispatcher.RegisterHandler(HandleDragDropEvent, (ULONG)this, "DragDropEvent", PackVersion(1, 0, 0, 0), DragDropEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleScriptClientEvent, (ULONG)this, "ScriptClientEvent", PackVersion(1, 0, 0, 0), ScriptClientEvent::ClassId(), EP_NORMAL);
			ZoneCustomizationDownloadHandler* zch = new ZoneCustomizationDownloadHandler(m_dispatcher, m_logger);
			m_zoneCustomizationDownloadHandler.reset(zch);
			m_dispatcher.RegisterHandler(m_zoneCustomizationDownloadHandler, PackVersion(1, 0, 0, 0), zch->zoneCustomId(), EP_HIGH);
			m_dispatcher.RegisterHandler(HandleZoneCustomizationSummaryEvent, (ULONG)this, "ZoneCustomizationSummaryEvent", PackVersion(1, 0, 0, 0), ZoneCustomizationSummaryEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandlePromptAllowedEvent, (ULONG)this, "PromptAllowedEvent", PackVersion(1, 0, 0, 0), promptAllowedEventId, EP_HIGH + 1); // higher than scripting so C++ only processor of this event
			m_dispatcher.RegisterHandler(HandleAddTriggerEvent, (ULONG)this, "AddTriggerEvent", PackVersion(1, 0, 0, 0), AddTriggerEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleRemoveTriggerEvent, (ULONG)this, "RemoveTriggerEvent", PackVersion(1, 0, 0, 0), RemoveTriggerEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleAddParticleEvent, (ULONG)this, "AddParticleEvent", PackVersion(1, 0, 0, 0), AddParticleEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleRemoveParticleEvent, (ULONG)this, "RemoveParticleEvent", PackVersion(1, 0, 0, 0), RemoveParticleEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleAddSoundEvent, (ULONG)this, "AddSoundEvent", PackVersion(1, 0, 0, 0), AddSoundEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleRemoveSoundEvent, (ULONG)this, "RemoveSoundEvent", PackVersion(1, 0, 0, 0), RemoveSoundEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleMoveSoundEvent, (ULONG)this, "MoveSoundEvent", PackVersion(1, 0, 0, 0), MoveSoundEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleUpdateSoundEvent, (ULONG)this, "UpdateSoundEvent", PackVersion(1, 0, 0, 0), UpdateSoundEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(ContentService::ItemsMetadataResultHandler, (ULONG)this, "ItemsMetadataResultEvent", PackVersion(1, 0, 0, 0), m_itemsMetadataResultEventId);
			m_dispatcher.RegisterHandler(HandleSetDynamicObjectTexturePanningEvent, (ULONG)this, "SetDynamicObjectTexturePanningEvent", SetDynamicObjectTexturePanningEvent::ClassVersion(), m_dispatcher.GetEventId("SetDynamicObjectTexturePanningEvent"), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleClanEvent, (ULONG)this, "ClanEvent", PackVersion(1, 0, 0, 0), ClanEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleTitleEvent, (ULONG)this, "TitleEvent", PackVersion(1, 0, 0, 0), TitleEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleAddObjectMonitorEvent, (ULONG)this, "AddDynamicObjectEvent", AddDynamicObjectEvent::ClassVersion(), m_dispatcher.GetEventId("AddDynamicObjectEvent"), EP_LOW);
			m_dispatcher.RegisterHandler(HandleZoneCustomizationDLCompleteEvent, (ULONG)this, "ZoneCustomizationDLCompleteEvent", PackVersion(1, 0, 0, 0), m_dispatcher.GetEventId("ZoneCustomizationDLCompleteEvent"), EP_HIGH);
			m_dispatcher.RegisterHandler(HandleMissing3DAppCharacterEvent, (ULONG)this, "Missing3DAppCharacterEvent", PackVersion(1, 0, 0, 0), Missing3DAppCharacterEvent::ClassId(), EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleShutdownRequestEvent, (ULONG)this, "ShutdownRequestEvent", PackVersion(1, 0, 0, 0), shutdownRequestEventId, EP_NORMAL);
			m_dispatcher.RegisterHandler(HandleEventQueueProbeEvent, (ULONG)this, "EventQueueProbeEvent", PackVersion(1, 0, 0, 0), m_eventQueueProbeEventId, EP_NORMAL);
		} else {
			ret = false;
		}

		// client and editor need the download manager
		// but what about the EntityManager?
		m_pDM = new DownloadManager(m_dispatcher, this);
		m_pUM = new UploadManager(m_dispatcher);

	} catch (KEPException* e) {
		LogError("Exception reading blades: " << e->ToString());
		e->Delete();
		ret = false;
	}

	return ret;
}

void ClientEngine::EventQueueStats() {
	if (m_eventQueueStatsInterval == 0)
		return;

	double timestamp = fTime::TimeMs();
	if (timestamp >= m_eventQueueStatsTime) {
		m_eventQueueStatsCounter++; // Increment first so it matches the ID of the new event.
		m_eventQueueStatsTime = timestamp + m_eventQueueStatsInterval;

		// Report current configuration
		LOG4CPLUS_INFO(m_eventQueueStatsLogger, "[" << m_eventQueueStatsCounter << "] engine.dispatch_time=" << getDispatchTime() << " ms");

		// Report event queue depth
		LOG4CPLUS_INFO(m_eventQueueStatsLogger, "[" << m_eventQueueStatsCounter << "] queue.depth=" << m_dispatcher.GetEventQueueDepth());

		// Queue a dummy event to measure latency
		ASSERT(m_eventQueueProbeEventId != BAD_EVENT_ID);
		IEvent* e = m_dispatcher.MakeEvent(m_eventQueueProbeEventId);
		ASSERT(e != nullptr);
		if (e != nullptr) {
			e->SetObjectId((OBJECT_ID)m_eventQueueStatsCounter);
			*e->OutBuffer() << timestamp;
			m_dispatcher.QueueEvent(e);
		}
	}
}

EVENT_PROC_RC ClientEngine::HandleTextEventStatic(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);
	return pCE->HandleTextEvent(e);
}

EVENT_PROC_RC ClientEngine::HandleCameraEventStatic(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);
	return pCE->HandleCameraEvent(e);
}

EVENT_PROC_RC ClientEngine::HandleDisconnectedEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto pEvent = dynamic_cast<DisconnectedEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	NETID player_id; //No good on client (should be 0 from CMultiplayerObj::Disconnected)
	ULONG reason; //reason for disconnect 0
	LONG loggedOn; //time logged on, no good either (should be 0 from CMultiplayerObj::Disconnected)
	std::string strReason; //string reason for disconnect
	int error_code; //disconnect code
	(*pEvent->InBuffer()) >> player_id >> reason >> loggedOn >> strReason >> error_code;

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandlePendingSpawnEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<PendingSpawnEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	LONG zoneIndex;
	USHORT spawnId;
	bool initialSpawn;
	long coverCharge;
	long zoneInstanceId;

	pEvent->extractInfo(zoneIndex, spawnId, initialSpawn, coverCharge, zoneInstanceId);

	pCE->m_dispatcher.QueueEvent(new PendingSpawnEvent(zoneIndex, spawnId, SERVER_NETID, initialSpawn, coverCharge, zoneInstanceId));

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleAddUGCTextureEvent(ULONG lparam, IDispatcher* dispatcher, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	std::string name;
	int glid = 0;
	std::list<int> globalIds;
	std::string resultText;

	(*e->InBuffer()) >> name >> glid >> resultText; // The name isn't used here, but is used in the script to ID the control to be updated.
	globalIds.push_back(glid);

	ContentService::Instance()->ContentMetadataXmlUnpackToCache(resultText);
	pCE->AddUGCTexturesToDatabase(globalIds);

	// Don't consume the event, the script will need it too.
	return EVENT_PROC_RC::OK;
}

// DRF - TODO - Change glids to AnimSettings
EVENT_PROC_RC ClientEngine::HandleUpdateEquippableAnimationEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<UpdateEquippableAnimationEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;
	;

	NETID netId = 0;
	std::vector<ItemAnimation> itemAnims;
	pEvent->ExtractInfo(netId, itemAnims);

	auto pMO = pCE->GetMovementObjByNetId(netId);
	if (!pMO) {
		LogError("GetMovementObjByNetId() FAILED - netId=" << netId);
		return EVENT_PROC_RC::CONSUMED;
	}

	for (const auto& ia : itemAnims)
		pCE->SetEquippableAnimation(pMO, ia.m_glidItem, ia.m_glidAnim, false);

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleUpdateSoundUGCEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	int placementId = -1;
	std::string resultText;

	(*e->InBuffer()) >> placementId >> resultText;

	ContentService::Instance()->ContentMetadataXmlUnpackToCache(resultText);

	// Set Sound Placement Name
	auto pSP = pCE->SoundPlacementGet(placementId);
	if (pSP) {
		// Get Already Cached Content Metadata (don't cache if not already cached? bug?)
		GLID glid = pSP->GetGlid();
		ContentMetadata md(glid);
		if (md.IsValid())
			pSP->SetName(md.getItemName());
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleDownloadedTextureEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	int eventId = e->EventId();
	int friendId = 0;
	int objId = 0;
	int percentComplete = 0;
	std::string worldObjSerialNo;
	std::string fname;
	int returnCode = E_INVALIDARG;
	bool overwrite;

	percentComplete = e->Filter();
	objId = e->ObjectId();

	// will be false for ClientBaseEngine::AddObjectEventHandler() but true if the change has come from script,
	// which means that if the script event arrives first, it will block the regular adding-an-object-as-I-zone-in
	// workflow.
	(*e->InBuffer()) >> overwrite;

	if (percentComplete != 200)
		return EVENT_PROC_RC::CONSUMED;

	std::string customTextureUrl;
	if (eventId == pCE->m_friendObjectTextureEventId) {
		(*e->InBuffer()) >> friendId;
	} else if (eventId == pCE->m_worldObjectTextureEventId) {
		(*e->InBuffer()) >> worldObjSerialNo;
		(*e->InBuffer()) >> customTextureUrl;
	} else if (eventId != pCE->m_dynObjectTextureEventId) { // nothing extra encoded for this
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	(*e->InBuffer()) >> returnCode >> fname;

	if (returnCode != 0) {
		LogError("Error downloading file " << fname << " return code of " << returnCode);
		return EVENT_PROC_RC::CONSUMED;
	}

	if (eventId == pCE->m_dynObjectTextureEventId) {
		auto pDPO = pCE->GetDynamicObject(objId);
		if (!pDPO) {
			LogError("GetDynamicObject() FAILED - objId=" << objId);
			return EVENT_PROC_RC::CONSUMED;
		}
		pDPO->SetCustomTexture(overwrite, std::make_shared<CustomTextureProvider>(fname));
		pDPO->UpdateCustomMaterials();
	} else if (eventId == pCE->m_friendObjectTextureEventId) {
		auto pDPO = pCE->GetDynamicObject(objId);
		if (!pDPO) {
			LogError("GetDynamicObject() FAILED - objId=" << objId);
			return EVENT_PROC_RC::CONSUMED;
		}
		pDPO->SetFriendId(friendId);
		pDPO->SetCustomTexture(overwrite, std::make_shared<CustomTextureProvider>(fname));
		pDPO->UpdateCustomMaterials();
	} else if (eventId == pCE->m_worldObjectTextureEventId) {
		MutexGuardLock lock(pCE->m_WldObjListMutex); // drf - crash fix
		if (pCE->m_pWOL) {
			for (POSITION pos = pCE->m_pWOL->GetHeadPosition(); pos;) {
				auto pWO = reinterpret_cast<CWldObject*>(pCE->m_pWOL->GetNext(pos));
				if (!pWO)
					continue;

				std::string uniqueIdStr;
				unsigned char* s = 0;
				VERIFY(UuidToStringA(&(pWO->m_uniqueId), &s) == S_OK);
				uniqueIdStr = (const char*)s;
				RpcStringFreeA(&s);

				if (worldObjSerialNo.compare(uniqueIdStr) == 0) {
					pWO->SetCustomTextureUrl(customTextureUrl);
					pWO->SetCustomTexture(fname);
					pWO->UpdateCustomMaterials();
					pWO->SaveChanges();
				}
			}
		}
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleChildGameWebCallsReadyEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	std::string resultText;
	(*e->InBuffer()) >> resultText;

	TiXmlDocument doc;
	doc.Parse(resultText.c_str());

	if (!doc.Error()) {
		TiXmlElement* rootElem = doc.RootElement();
		if (rootElem) {
			// Valid XML document returned, allow all web calls except for the following
			pCE->m_childGameWebCallValuesLoaded = true;

			TiXmlElement* urlElement = rootElem->FirstChildElement("url_value");
			while (urlElement) {
				if (urlElement->FirstChild() && urlElement->FirstChild()->ToText()) {
					std::string newURL = urlElement->FirstChild()->ToText()->Value();
					pCE->m_childGameWebCallValues.push_back(newURL);
				}

				urlElement = urlElement->NextSiblingElement("url_value");
			}
		} else {
			LogError("Result text has no root element");
		}
	} else {
		LogError("Error parsing result text");
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::UpdateDynamicObjectMediaEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<UpdateDynamicObjectMediaEvent*>(e);
	if (!pCE->m_dynamicObjectRM || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	// Extract Event Params
	int objId = 0;
	MediaParams mediaParams;
	pEvent->ExtractInfo(objId, mediaParams);
	LogInfo("objId=" << objId << " " << mediaParams.ToStr(true));

	// Set Media Params On Dynamic Object
	if (!pCE->SetMediaParamsOnDynamicObject(objId, mediaParams)) {
		LogError("SetMediaParamsOnDynamicObject() FAILED - objId=" << objId << " " << mediaParams.ToStr(true));
	}

	// ED-3195 - Sync Playlist Across All Media Placements
	pCE->MediaPlacementsPlaylistSync(mediaParams);

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::UpdateDynamicObjectMediaVolumeAndRadiusEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<UpdateDynamicObjectMediaVolumeAndRadiusEvent*>(e);
	if (!pCE->m_dynamicObjectRM || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	// Extract Event Params
	int objId = 0;
	MediaParams mediaParams;
	pEvent->ExtractInfo(objId, mediaParams);
	LogInfo("objId=" << objId << " " << mediaParams.ToStr(true));

	// Set Media Volume On Dynamic Object
	if (!pCE->SetMediaVolumeOnDynamicObject(objId, mediaParams.GetVolume())) {
		LogError("SetMediaVolumeOnDynamicObject() FAILED - objId=" << objId << " volPct=" << mediaParams.GetVolume());
		return EVENT_PROC_RC::CONSUMED;
	}

	// Set Media Radius On Dynamic Object
	if (!pCE->SetMediaRadiusOnDynamicObject(objId, mediaParams.GetRadius())) {
		LogError("SetMediaRadiusOnDynamicObject() FAILED - objId=" << objId << " " << mediaParams.GetRadius().ToStr());
		return EVENT_PROC_RC::CONSUMED;
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleP2pAnimPermissionEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<P2pAnimPermissionEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	int netId = 0;
	std::string animName = "";
	pEvent->ExtractInfo(netId, animName);

	auto pMO = pCE->GetMovementObjByNetId(netId);
	if (!pMO) {
		LogError("GetMovementObjByNetId() FAILED - netId=" << netId);
		return EVENT_PROC_RC::CONSUMED;
	}

	//Fire locally for the clientscript p2pAnimations.lua to catch and display the confirmation message box
	e = pCE->m_dispatcher.MakeEvent(pCE->m_p2pAnimRequestNotifyEventId);
	e->SetFilter(netId);
	(*e->OutBuffer()) << pMO->getName() << animName.c_str();
	pCE->m_dispatcher.QueueEvent(e);

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientEngine::HandleP2pAnimEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<P2pAnimEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	// Decode Event Data
	P2pAnimData p2pAnimData;
	pEvent->ExtractInfo(p2pAnimData);

	// Update My P2p State
	switch (p2pAnimData.animState) {
		case eP2pAnimState::Inactive:
			pCE->P2pAnimStateInactive();
			break;

		case eP2pAnimState::Begin:
			pCE->P2pAnimStateBegin(p2pAnimData);
			break;

		case eP2pAnimState::Start:
			pCE->P2pAnimStateStart();
			break;

		case eP2pAnimState::End:
			pCE->P2pAnimStateEnd();
			break;

		case eP2pAnimState::Terminate:
			pCE->P2pAnimStateTerminate();
			break;
	}

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientEngine::HandleScriptClientEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<ScriptClientEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	if (!pCE->SCE_Handler(pEvent))
		return EVENT_PROC_RC::NOT_PROCESSED;

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientEngine::HandleDragDropEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<DragDropEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	bool ret = false;
	bool initial;
	bool dropped;
	int type;
	std::string fullPath;
	int mouseX, mouseY;
	DWORD keyState;
	pEvent->ExtractInfo(initial, dropped, type, fullPath, mouseX, mouseY, keyState);

	if (dropped == true) {
		// check ownership and file type
		// Only owner of a zone should be able to upload script according to MB [YC 02/2010]
		if (pCE->IsZoneCustomizableByPlayer() && type == DROP_FILE && STLEndWithIgnoreCase(fullPath, ".lua")) {
			Vector3f rayOrigin;
			Vector3f rayDirection;
			POINT ptCursor;
			::GetCursorPos(&ptCursor);
			::ScreenToClient(pCE->m_hWndSafe, &ptCursor);
			pCE->GetPickRay(&rayOrigin, &rayDirection, ptCursor.x, ptCursor.y);
			TraceInfo trace;
			pCE->RayTrace(&trace, rayOrigin, rayDirection, FLT_MAX, ObjectType::ANY, RAYTRACE_VISIBLE);
			// I do not want to drop into dynamic objects that are hidden behind world objects
			// for example, so I use OBJTYPE_ANY and check the type.

			if (trace.objType != ObjectType::NONE)
				ret = pCE->ProcessDroppedLuaScript(fullPath, (int)trace.objType, trace.objId);
		}
	}

	return ret ? EVENT_PROC_RC::OK : EVENT_PROC_RC::NOT_PROCESSED;
}

EVENT_PROC_RC ClientEngine::HandleTryOnEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	if (pCE->m_tryOnOnStartup != 0) {
		LogInfo("Trying on glid<" << pCE->m_tryOnGlid << "> useType=" << (int)pCE->m_tryonUseType);

		if ((pCE->m_tryonUseType == USE_TYPE_ADD_DYN_OBJ) || (pCE->m_tryonUseType == USE_TYPE_ADD_ATTACH_OBJ) || (pCE->m_tryonUseType == USE_TYPE_SOUND)) {
			auto pMO = pCE->GetMovementObjMe();
			if (pMO) {
				ATTRIB_DATA attribData;
				attribData.aeType = eAttribEventType::TryOnItem;
				attribData.aeShort1 = pMO->getNetTraceId();
				attribData.aeShort2 = IT_NORMAL;
				attribData.aeInt1 = pCE->m_tryOnGlid;
				pCE->m_multiplayerObj->QueueMsgAttribToServer(attribData);

				pCE->m_tryOnOnStartup = false;
			}
		} else if (pCE->m_tryonUseType == USE_TYPE_NONE || pCE->m_tryonUseType == USE_TYPE_EQUIP) {
			pCE->m_tryOnClothingGlid = pCE->m_tryOnGlid;

			if (pCE->ArmInventoryItem_MO(pCE->m_tryOnGlid)) {
				pCE->InTryOnState(true);
				pCE->m_tryOnOnStartup = false;
			}
		} else if (IS_UGC_GLID(pCE->m_tryOnGlid)) {
			// catch-all UGC tryon handler
			TryOnInventoryEvent* tryInventoryEvent = new TryOnInventoryEvent();
			tryInventoryEvent->InvokeAlternateInventoryMenu(pCE->m_tryOnGlid, pCE->m_tryonUseType);
			pCE->m_dispatcher.QueueEvent(tryInventoryEvent);
			pCE->m_tryOnOnStartup = false;
		}
	} else
		pCE->InTryOnState(false);

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientEngine::HandleTryOnExpireEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<TryOnExpireEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	pEvent->ExtractInfo();

	pCE->SetTryOnPlacementId(pEvent->m_dynamic_obj_placement_id);

	IEvent* pEvent_tor = pCE->m_dispatcher.MakeEvent(pCE->m_tryOnRemoveEventId);

	if (pEvent_tor->OutBuffer()) {
		IWriteableBuffer& buffer(*pEvent_tor->OutBuffer());
		TimeMs timeMsExp = SEC_PER_MIN * MS_PER_SEC * pEvent->m_expireDuration;

		buffer << pEvent->m_playerId
			   << pEvent->m_glid
			   << pEvent->m_baseGlid
			   << (int)pEvent->m_useType
			   << pEvent->m_dynamic_obj_placement_id;

		pCE->m_dispatcher.QueueEventInFutureMs(pEvent_tor, timeMsExp);
	}

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientEngine::HandleTryOnRemoveEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	if (!disp || !e)
		return EVENT_PROC_RC::NOT_PROCESSED;

	IReadableBuffer* buffer = e->InBuffer();
	if (!buffer)
		return EVENT_PROC_RC::NOT_PROCESSED;

	int playerId = 0;
	int tryOnGlid = 0;
	int tryOnBaseGlid = 0;
	USE_TYPE useType = USE_TYPE_NONE;
	int placementId = 0; // will be garbage if trying on clothing

	int useTypeInt = USE_TYPE_NONE;
	*buffer >> playerId;
	*buffer >> tryOnGlid;
	*buffer >> tryOnBaseGlid;
	*buffer >> useTypeInt;
	*buffer >> placementId;
	useType = (USE_TYPE)useTypeInt;

	// Send event to menu system. It will handle warning dialog of expiration
	// and disarming the item.
	if ((useType == USE_TYPE_NONE || useType == USE_TYPE_EQUIP) && pCE->InTryOnState() && pCE->m_tryOnClothingGlid == tryOnGlid) {
		// Check and make sure that the expiration of the clothing GLID is in fact
		// armed on our character before attempting removal.
		bool itemArmed = false;
		POSITION dbPosLast;
		auto pMO = pCE->GetMovementObjMe();
		if (!pMO) {
			LogError("GetMovementObj() FAILED");
			return EVENT_PROC_RC::CONSUMED;
		}

		// DRF - List Lock Crash Fix
		auto pCAI = pMO->getCurrentlyArmedItems();
		if (pCAI) {
			OBJ_LIST_GUARD(pCAI);
			for (POSITION dbPos = pCAI->GetHeadPosition(); (dbPosLast = dbPos) != NULL;) {
				auto pIO = static_cast<const CItemObj*>(pCAI->GetNext(dbPos));
				if (!pIO)
					continue;

				if (pIO->m_globalID == tryOnGlid) {
					itemArmed = true;
					break;
				}
			}
		}

		if (!itemArmed) {
			LogInfo("TRY ON ITEM NOT ARMED - glid<" << tryOnGlid << ">");
			return EVENT_PROC_RC::OK;
		}

		// Remove Try On Item
		TryOnInventoryEvent* tryInventoryEvent = new TryOnInventoryEvent();
		tryInventoryEvent->InvokeExpiredInventoryMenu(tryOnGlid, useType, placementId);
		pCE->m_dispatcher.QueueEvent(tryInventoryEvent);
		pCE->m_tryOnClothingGlid = 0;
	} else if (((useType == USE_TYPE_ADD_DYN_OBJ) || (useType == USE_TYPE_ADD_ATTACH_OBJ) || (useType == USE_TYPE_SOUND)) &&
			   (pCE->m_zonePermissions == 1 && pCE->GetZoneIndex().isHousing())) {
		// Dynamic objects
		// If the object has already been picked up, then don't show the expiration menu
		CDynamicPlacementObj* placementObj = pCE->GetDynamicObject(placementId);

		if (placementObj || pCE->SoundPlacementValid(placementId)) {
			TryOnInventoryEvent* tryInventoryEvent = new TryOnInventoryEvent();
			tryInventoryEvent->InvokeExpiredInventoryMenu(tryOnGlid, useType, placementId);
			pCE->m_dispatcher.QueueEvent(tryInventoryEvent);
		}
	} else if (useType == USE_TYPE_ANIMATION) {
		// Animation try-on, check if
		// 1. tried animation still in use
		// 2. user hasn't bought it (not in inventory) -- checked in menu
		// then stop animation (in menu)
		auto pMO = pCE->GetMovementObjMe();
		if (pMO && (pMO->getOverrideAnimType() == eAnimType::None) && (pMO->getOverrideAnimationSettings().m_glid == tryOnGlid)) {
			TryOnInventoryEvent* tryInventoryEvent = new TryOnInventoryEvent();
			tryInventoryEvent->InvokeExpiredInventoryMenu(tryOnGlid, useType, placementId);
			pCE->m_dispatcher.QueueEvent(tryInventoryEvent);
		}
	}

	return EVENT_PROC_RC::OK;
}

// sent from server to tell us to switch servers
EVENT_PROC_RC ClientEngine::HandleGotoPlaceEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<GotoPlaceEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	std::string gameNameFromUrl;

	// decode the data from the server
	pEvent->ExtractInfo();

	StpUrl::ExtractGameName(pEvent->m_url, gameNameFromUrl);

	if (!pCE->ClientIsEnabled()) {
		// allow event to be processed if jumping off current server if not connected
		// this is same logic as below
		if (pEvent->m_type == GotoPlaceEvent::GOTO_URL &&
			pEvent->m_server.empty() &&
			STLCompareIgnoreCase(gameNameFromUrl.c_str(), pCE->m_gameName.c_str()) != 0 &&
			atol(gameNameFromUrl.c_str()) != pCE->GetGameId()) {
			// do nothing
		} else
			return EVENT_PROC_RC::NOT_PROCESSED; // only accept these events if connected
	}

	// setup the reconnection data
	if (pCE->m_reconnectEvent) {
		pCE->m_reconnectEvent->Delete();
		pCE->m_reconnectEvent = 0;
	}

	// goto game should only be used internally here or from vw_url event handler, or maybe no where
	jsVerifyReturn(pEvent->m_type != GotoPlaceEvent::GOTO_GAME || e->From() == 0, EVENT_PROC_RC::NOT_PROCESSED);

	if (pEvent->m_type == GotoPlaceEvent::GOTO_URL) {
		LogInfo("'" << pCE->m_gameName << "' ==> '" << pEvent->m_url << "'");
		if (pEvent->m_childGameId != 0) {
			pCE->SetGameId(pEvent->m_childGameId);
			pCE->SetParentGameId(pEvent->m_parentGameId);
		}

		if (pEvent->m_server.empty()) {
			if (STLCompareIgnoreCase(gameNameFromUrl.c_str(), pCE->m_gameName.c_str()) == 0 ||
				atol(gameNameFromUrl.c_str()) == pCE->GetGameId()) {
				// send to our server since our url
				e->AddTo(SERVER_NETID);
				disp->QueueEvent(e);
				return EVENT_PROC_RC::OK;
			} else {
				// figure out where we're going

				// get clean one from menu
				std::string cleanUrl(pEvent->m_url);

				std::string gameName;
				std::string parentServer;
				LONG parentPort;
				std::string templatePatchDir;
				std::string appPatchDir;

				LONG ret = pCE->PatchGameUrl(
					cleanUrl.c_str(),
					pCE->m_allowAccessUrl.c_str(),
					pCE->m_login.userEmail.c_str(),
					pCE->m_login.userPassword.c_str(),
					pCE->PathBase().c_str(),
					pCE->GetGameId(),
					pCE->GetParentGameId(),
					gameName,
					parentServer,
					parentPort,
					templatePatchDir,
					appPatchDir);
				if (ret == UP_ERROR) {
					// PatchGameUrl always sends err msg to client on error
					return EVENT_PROC_RC::NOT_PROCESSED;
				} else if (ret == UP_PARENT_GAME) {
					// no patch needed, just set server and port then drop through to reconnect
					pCE->SetGameId(pCE->GetParentGameId());
					pCE->SetParentGameId(0);

					pCE->m_gameName = gameName;
					pEvent->m_port = parentPort;
					pEvent->m_server = parentServer;

					// Clear child-game patch directories
					pCE->m_templatePatchDir.clear();
					pCE->m_appPatchDir.clear();
					pCE->m_legacyAppPatch = true; // This flag is not used for WOK.
				} else if (ret != UP_SAME_GAME) { // we should have caught this earlier
					pCE->m_templatePatchDir = templatePatchDir;
					pCE->m_appPatchDir = appPatchDir;
					pCE->m_legacyAppPatch = templatePatchDir.empty() && appPatchDir.empty();

					// it started patching
					return EVENT_PROC_RC::OK;
				}
			}
		}
	}

	if (pEvent->m_port != 0 && pEvent->m_server.length() > 0) {
		if (pEvent->m_type != GotoPlaceEvent::GOTO_SERVER) { // vanilla goto server, no reconnect event
			// clone the event w/o server:port to send it to new server later
			GotoPlaceEvent* serverGoto = new GotoPlaceEvent();
			serverGoto->CloneForClient(*pEvent, pCE->ClientIsEnabled());
			pCE->m_reconnectEvent = serverGoto;
			pCE->m_reconnectEvent->AddTo(SERVER_NETID);
		}

		// reconnect to the new server
		pCE->m_multiplayerObj->setIPAddress(pEvent->m_server.c_str());
		pCE->m_multiplayerObj->m_port = pEvent->m_port;

		// Login To Server (Network Layer)
		if (!pCE->LoginToServer()) {
			LogError("LoginToServer() FAILED");
			pCE->GameLoginFailed(GL_NETWORK_ERROR);
			return EVENT_PROC_RC::OK;
		}
	} else {
		LogError("Bad server:port combination [" << pEvent->m_server << ":" << pEvent->m_port << "] for GotoPlaceEvent type: " << pEvent->m_type << ", URL: " << pEvent->m_url);

		// DRF - Added
		pCE->GameLoginFailed(GL_PORT_ERROR);

		// This happens client side for obviously bad urls (eg. 'junk')
		// It is handled in the ProgressController.lua Client Script
		EVENT_ID eId = pCE->m_dispatcher.GetEventId("BadGotoPlaceURLEvent");
		if (eId != BAD_EVENT_ID) {
			IEvent* pBadUrlEvent = pCE->m_dispatcher.MakeEvent(eId);
			if (pBadUrlEvent) {
				pBadUrlEvent->SetFilter(BAD_URL);
				(*pBadUrlEvent->OutBuffer()) << pEvent->m_url;
				pCE->m_dispatcher.QueueEvent(pBadUrlEvent);
			}
		}
	}

	return EVENT_PROC_RC::OK;
}

// user typed something at the keyboard
EVENT_PROC_RC ClientEngine::HandleTextEvent(IEvent* e) {
	auto pEvent = dynamic_cast<TextEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	bool localCommand = false;
	SlashCommand commandId;
	int networkAssignedId;
	std::string remainder;
	eChatType ct;
	bool fromIMClient = false;
	pEvent->ExtractInfo(commandId, networkAssignedId, remainder, ct, NULL, NULL, NULL, &fromIMClient);

	// Handle Special Local Commands
	switch (commandId) {
		case SlashCommand::SQUELCH: {
			m_filterSpeechDB->AddName(remainder.c_str());
			//			m_pTextReadoutObj->AddLine (remainder, eChatType::Talk);
			localCommand = true;
		} break;

		case SlashCommand::DESQUELCH: {
			m_filterSpeechDB->UnBlockName(remainder.c_str());
			//			m_pTextReadoutObj->AddLine(remainder, eChatType::Talk);
			localCommand = true;
		} break;
	}

	if (localCommand)
		return EVENT_PROC_RC::CONSUMED;

	// Send Command To Server
	pEvent->AddTo(SERVER_NETID);
	e->AddTo(SERVER_NETID);
	m_dispatcher.QueueEvent(e);

	// If PM, send a copy to IMClient if attached
	if (fromIMClient)
		return EVENT_PROC_RC::CONSUMED;

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleClientSpawnEventStatic(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	if (pCE->GetParentGameId() != 0)
		return EVENT_PROC_RC::NOT_PROCESSED;
	else
		return pCE->HandleClientSpawnEvent(lparam, disp, e);
}

// This functionality was previously done in ClientEngine::HandleSpawnMessagesClientSide.
// It was moved here to an event handler with a low priority to accomodate a client script
// being able to catch the event first and request dynamic objects from the server.
// This was done for STAR
EVENT_PROC_RC ClientEngine::HandleClientSpawnEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto pEvent = dynamic_cast<ClientSpawnEvent*>(e);
	if (!pEvent || !m_pDM)
		return EVENT_PROC_RC::NOT_PROCESSED;

	LONG zoneIndex;
	LONG instanceId;
	float x, y, z;
	pEvent->extractInfo(zoneIndex, instanceId, x, y, z);

#ifdef _DEBUG
	// Sanity check on zoneIndex (should not carry non-zero instance ID otherwise zonecustomizations.aspx will fail)
	ZoneIndex ziDebug(zoneIndex);
	assert(ziDebug.GetInstanceId() == 0);
#endif

	CStringA url(m_zoneCutomizationsUrl.c_str());
	url.Replace("{0}", numToString(zoneIndex, "%d").c_str());
	url.Replace("{1}", numToString(instanceId, "%d").c_str());
	if (url.Find("{2}") >= 0 && url.Find("{3}") >= 0 && url.Find("{4}") >= 0) {
		url.Replace("{2}", numToString(x, "%.2f").c_str());
		url.Replace("{3}", numToString(y, "%.2f").c_str());
		url.Replace("{4}", numToString(z, "%.2f").c_str());
	}

	DownloadHandler* dh = dynamic_cast<DownloadHandler*>(m_zoneCustomizationDownloadHandler.operator->());
	jsVerifyReturn(dh != 0, EVENT_PROC_RC::NOT_PROCESSED);

	dh->DownloadCanceled();

	std::string urlStr = url.GetString();
	if (urlStr.empty())
		return EVENT_PROC_RC::CONSUMED;

	// Download File Asynchronously
	DownloadFileAsyncWithHandler(ResourceType::ZONECUST, urlStr, DL_PRIO_MEDIUM, dh, false);

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandlePromptAllowedEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	// call validate user to tell it we approve the game
	std::string s_url(pCE->m_authUrl);
	s_url += "&a=1"; // allow

	// fillInAuth fills in m_gameId, but we want to auth to new game id
	int curGameId = pCE->GetGameId();
	pCE->SetGameId(e->ObjectId());
	if (!pCE->fillInAuthUrl(s_url)) {
		LogError("fillInAuthUrl() FAILED");
	}
	pCE->SetGameId(curGameId);

	std::string resultText;
	DWORD httpStatusCode;
	std::string httpStatusDescription;

	bool ret = pCE->_GetBrowserLogin(s_url, resultText, &httpStatusCode, httpStatusDescription);
	e->SetFilter(E_INVALIDARG);
	if (!ret) {
		LogError("Error in browser login request: " << httpStatusCode << " Description=" << httpStatusDescription.c_str());
	} else if (resultText == "<resp>ok</resp>")
		e->SetFilter(NO_ERROR);

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleAddTriggerEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<AddTriggerEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	LONG placementId;
	eTriggerAction triggerAction;
	float radius;
	int intParam;
	std::string strParam;
	int triggerId;
	pEvent->ExtractInfo(placementId, triggerAction, radius, intParam, strParam, triggerId);
	pCE->AddDynamicObjectTrigger((int)placementId, triggerId, triggerAction, PrimitiveGeomArgs<float>::getSphere(radius), intParam, strParam);

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleRemoveTriggerEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<RemoveTriggerEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	LONG placementId;
	int triggerId;
	pEvent->ExtractInfo(placementId, triggerId);
	pCE->PendingDeleteDynamicObjectTrigger((int)placementId, triggerId);

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleAddParticleEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<AddParticleEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	try {
		ObjectType parentType;
		int parentId, particleSlot;
		GLID particleGlid;
		std::string boneName;
		Vector3f offset, dir, up;
		pEvent->ExtractInfo(particleGlid, parentType, parentId, boneName, particleSlot, offset.x, offset.y, offset.z, dir.x, dir.y, dir.z, up.x, up.y, up.z);
		pCE->ObjectAddParticleAsync(parentType, parentId, boneName, particleSlot, particleGlid, offset, dir, up);
	} catch (CException* e) {
		LogError("Caught exception=" << e);
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleRemoveParticleEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<RemoveParticleEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	try {
		ObjectType parentType;
		int parentId;
		int particleSlot;
		std::string boneName;
		pEvent->ExtractInfo(parentType, parentId, boneName, particleSlot);
		pCE->ObjectRemoveParticle(parentType, parentId, boneName, particleSlot);
	} catch (CException* e) {
		LogError("Caught exception=" << e);
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleTitleEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<TitleEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	std::string title;
	int netId;
	(*pEvent->InBuffer()) >> title >> netId;

	auto pMO = pCE->GetMovementObjByNetId(netId);
	if (!pMO) {
		LogError("GetMovementObjByNetId() FAILED - netId=" << netId);
		return EVENT_PROC_RC::CONSUMED;
	}

	pMO->setTitle(title);
	pCE->setOverheadLabels(pMO);

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleClanEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<ClanEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	auto clanAction = pEvent->GetClanAction();
	switch (clanAction) {
		case ClanAction::CREATE: {
			std::string clanName;
			(*pEvent->InBuffer()) >> clanName;
			auto pMO = pCE->GetMovementObjMe();
			if (pMO) {
				pMO->setClanName(clanName);
				pCE->setOverheadLabels(pMO);
			}
		} break;

		case ClanAction::DISBAND: {
			auto pMO = pCE->GetMovementObjMe();
			if (pMO) {
				pMO->setClanName("");
				pCE->setOverheadLabels(pMO);
			}
		} break;
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleZoneCustomizationDLCompleteEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	// DRF - ED-4177 - DynamicObjectSpawnMonitor Bug
	auto totalObjects = e->Filter();
	int dynamicObjects;
	int soundPlacements;
	(*e->InBuffer()) >> dynamicObjects >> soundPlacements;
	LogInfo("totalObjects=" << totalObjects << " [dynamicObjects=" << dynamicObjects << " + soundPlacements=" << soundPlacements << "]");

	// Spawn Monitor Only Keeps Track Of Dynamic Objects
	pCE->m_DOSpawnMonitor->CustomizationComplete(dynamicObjects);

	pCE->SetSpawning(false); // User spawning progress has completed. Client engine will reduce dispatch time to minimum in order to maximize FPS over event processing
	pCE->InvalidateLightCaches(); // re-light everything, now that there's a reasonable chance all of the DO lights have been added

	// Any changes from the game state manager or script can be safely
	// processed on this client now, so send the server the completion event.
	EVENT_ID eId = pCE->m_dispatcher.GetEventId("ZoneCustomizationDLCompleteEvent");
	if (eId == BAD_EVENT_ID)
		return EVENT_PROC_RC::NOT_PROCESSED;

	IEvent* pEvent = pCE->m_dispatcher.MakeEvent(eId);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	pEvent->AddTo(SERVER_NETID);
	pCE->m_dispatcher.QueueEvent(pEvent);

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientEngine::HandleAddObjectMonitorEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<AddDynamicObjectEvent*>(e);
	if (!pEvent || !pCE->m_DOSpawnMonitor)
		return EVENT_PROC_RC::NOT_PROCESSED;

	if (!pCE->m_DOSpawnMonitor->IsSpawning())
		return EVENT_PROC_RC::CONSUMED;

	int placementId;
	pEvent->ExtractBasicInfo(placementId);
	auto pDPO = pCE->GetDynamicObject(placementId);
	if (pDPO) {
		auto pMO = pCE->GetMovementObjMe();
		if (pMO)
			pCE->m_DOSpawnMonitor->CheckAddDynObjToMonitor(pDPO, pMO->getBaseFramePosition());
	}

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientEngine::HandleMissing3DAppCharacterEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = dynamic_cast<Missing3DAppCharacterEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	LONG parentGameId, appGameId;
	(*pEvent->InBuffer()) >> parentGameId >> appGameId;

	//This is caught by Gamefiles\ClientScripts\ZoneLoader.lua
	IEvent* pEvent_are = pCE->m_dispatcher.MakeEvent(pCE->m_dispatcher.GetEventId("3DAppReturnEvent"));
	(*pEvent_are->OutBuffer()) << appGameId;
	pCE->m_dispatcher.QueueEvent(pEvent_are);

	auto pEvent_gpe = new GotoPlaceEvent();
	pEvent_gpe->GotoUrl(0, StpUrl::MakeUrlPrefix(parentGameId).c_str());
	LogInfo("No character information available, sending to parent game to create character: " << parentGameId);
	pCE->m_dispatcher.QueueEvent(pEvent_gpe);

	return EVENT_PROC_RC::CONSUMED;
}

// Entirely Handled By ClientScript::SoundPlacementListener.lua
EVENT_PROC_RC ClientEngine::HandleAddSoundEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	return EVENT_PROC_RC::CONSUMED;
}

// Entirely Handled By ClientScript::SoundPlacementListener.lua
EVENT_PROC_RC ClientEngine::HandleRemoveSoundEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	return EVENT_PROC_RC::CONSUMED;
}

// Entirely Handled By ClientScript::SoundPlacementListener.lua
EVENT_PROC_RC ClientEngine::HandleMoveSoundEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	return EVENT_PROC_RC::CONSUMED;
}

// Entirely Handled By ClientScript::SoundPlacementListener.lua
EVENT_PROC_RC ClientEngine::HandleUpdateSoundEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandlePlayerInventoryCompletionEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);
	return pCE->HandlePlayerInventoryCompletionEvent(e);
}

EVENT_PROC_RC ClientEngine::HandlePlayerInventoryCompletionEvent(IEvent* evt) {
	auto pEvent = dynamic_cast<ContentServiceCompletionEvent*>(evt);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	LPVOID lpMessage;
	int msgSize;
	std::vector<GLID> glids;
	pEvent->ExtractValues(lpMessage, msgSize, glids);

	MSG_BLOCK_ITEM_TO_CLIENT* pMsgBlockItem = (MSG_BLOCK_ITEM_TO_CLIENT*)lpMessage;
	int msgCount = pMsgBlockItem->computePayloadCount(msgSize);

	// pass inventory as attrib message to scripting as an event
	IEvent* pEvent_ae = m_dispatcher.MakeEvent(m_attribEventId);

	pEvent_ae->SetFilter(pMsgBlockItem->m_header.m_miscInt);
	(*pEvent_ae->OutBuffer()) << msgCount; // num items

	m_dispatcher.QueueEvent(pEvent_ae);

	ContentMetadataCacheSync(glids);

	delete lpMessage;
	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleScriptIconCompletionEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);
	return pCE->HandleScriptIconCompletionEvent(e);
}

// Callback From AddItemThumbnailToDatabase() For Uncached UGC GLID Icons
EVENT_PROC_RC ClientEngine::HandleScriptIconCompletionEvent(IEvent* e) {
	auto pEvent = dynamic_cast<ContentServiceCompletionEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	// For All GLIDs
	std::vector<GLID> glids;
	pEvent->ExtractValues(glids);
	for (const auto& glid : glids) {
		// Get Already Cached Content Metadata (skip if not already cached? bug?)
		ContentMetadata md(glid);
		if (!md.IsValid())
			continue;

		// Get Texture File Name
		// for UGC Items, I want to download them to the same place the LUA will look when
		// it gets around to it, but download them from the proper UGC URL found in the
		// meta data.  so, the object is added to the texture database here, and when the
		// texture is actually used (by the LUA), the texture will be streamed
		std::string texFileName = TextureFileName(glid);
		std::string texUrl = md.getThumbnailPath();

		// Add Texture To Texture Database
		DWORD nameHash = INVALID_TEXTURE_NAMEHASH;
		m_pTextureDB->TextureAdd(nameHash, texFileName, "Icons", texUrl);
		if (!ValidTextureNameHash(nameHash)) {
			LogError("AddTextureToDatabase() FAILED - glid<" << glid << "> '" << texUrl << "' =NO=> '" << texFileName << "'");
		} else {
			LogInfo("OK - glid<" << glid << "> '" << texUrl << "' => '" << texFileName << "'");
		}
	}

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientEngine::HandleSetDynamicObjectTexturePanningEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	auto pEvent = (SetDynamicObjectTexturePanningEvent*)e;
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	int objPlacementId;
	int meshId, uvSetId;
	float uIncr, vIncr;

	pEvent->ExtractInfo(objPlacementId, meshId, uvSetId, uIncr, vIncr);
	pCE->setDynamicObjectTexturePanning(objPlacementId, meshId, uvSetId, uIncr, vIncr);
	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleShutdownRequestEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	DECLARE_CE_PTR(pCE);

	LogFatal("Shutdown Request - Shutting Down ...");
	pCE->PostMessage(WM_CLOSE);

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleEventQueueProbeEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	double timestamp;
	*e->InBuffer() >> timestamp;
	LogInfo("[" << e->ObjectId() << "] queue.latency=" << (int)fTime::ElapsedMs(timestamp) << " ms");
	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientEngine::HandleResourceDownloadCompletionEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ResourceDownloadCompletionEvent* rdce = static_cast<ResourceDownloadCompletionEvent*>(e);
	assert(rdce->getResource() != nullptr && rdce->getResourceManager() != nullptr);

	return ResourceLoader::Instance().ProcessDownloadCompletionEvent(rdce);
}

EVENT_PROC_RC ClientEngine::HandleZoneCustomizationSummaryEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ZoneCustomizationSummaryEvent* zcse = static_cast<ZoneCustomizationSummaryEvent*>(e);
	int rc = 0;
	size_t dynamicObjectCount = 0, worldObjectSettingsCount = 0;
	int frameworkEnabledFlag = 0;
	unsigned ddsTextureSize = 0, meshSize = 0, meshInstSize = 0;

	zcse->ExtractInfo(rc, dynamicObjectCount, worldObjectSettingsCount, frameworkEnabledFlag, ddsTextureSize, meshSize, meshInstSize);
	if (rc == 0) {
		// Estimate basis:
		// (*) Dynamic object file contents are mostly mesh data.
		// (*) ReMesh data are compressed at ~50%. (* 2)
		// (*) Most DDS textures from UGC dynamic objects are of POT dimensions and without mipmaps (* 1.33)
		// (-) Generated objects are NOT measured at this moment, lacking information from DB. Will be added later.
		// (-) Custom textures are NOT measured at this moment, lacking information from DB. Will be added later.
		unsigned videoMemoryEstimate = meshInstSize * 2 + ddsTextureSize * 4 / 3; // Vertex buffers + Index Buffers + Texture Resources
		unsigned systemMemoryEstimate = meshSize * 2 + videoMemoryEstimate; // Mesh + Managed copy of video memory usage
		DxDevice::Ptr()->SetExpectedTextureMemoryUsage(videoMemoryEstimate, systemMemoryEstimate);
		LogInfo("Estimated memory requirement for current zone: " << videoMemoryEstimate << " VRAM bytes, " << systemMemoryEstimate << " system bytes");
	}

	return EVENT_PROC_RC::OK;
}

bool ClientEngine::SendMouseOverToolTipEvent(const std::string& toolTipText) {
	IEvent* pEvent = m_dispatcher.MakeEvent(m_mouseOverToolTipEventId);
	if (!pEvent)
		return false;
	*pEvent->OutBuffer() << toolTipText;
	m_dispatcher.QueueEvent(pEvent);
	return true;
}

} // namespace KEP