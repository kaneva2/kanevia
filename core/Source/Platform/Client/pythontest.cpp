///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
extern "C" {
#include "Tools/Python/include/Python.h"
}
#undef HAVE_FTIME
#include "Event/Base/EventHeader.h"
#include "Event/PythonEventHandler/PythonScriptVM.h"
#include <KEPHelpers.h>

KEP::IScriptVM* pythonTest() {
	::Py_Initialize(); // void nothing to check

	string baseDir = "c:\\code\\kep\\deploy\\gamecontentdbg\\scripts";

	// put our path in the Python path, if not already there
	const char* existingPath = ::Py_GetPath();
	if (existingPath) {
		string bd(baseDir);
		string ep(existingPath);

		::STLToLower(ep);
		::STLToLower(bd);
		if (ep.find(bd) == string::npos) {
			// prepend out path
			string newPath(baseDir);
			newPath += ";";
			newPath += existingPath;
			::PySys_SetPath((char*)newPath.c_str());
		}
	} else
		::PySys_SetPath((char*)baseDir.c_str());

	PyObject* pName = ::PyString_FromString("KEPPythonTest");
	PyObject* pModule = PyImport_Import(pName);

	// prints to stderr
	PyObject* err = PyErr_Occurred();
	if (err) { // should be set
		PyObject *exception, *v, *tb;

		PyErr_Fetch(&exception, &v, &tb);
		PyErr_NormalizeException(&exception, &v, &tb);
		if (exception != NULL) {
			char* lt = "";
			char* lv = "";
			char* tbs = "";

			PyObject* n = PyObject_Str(exception);
			if (n)
				lt = PyString_AS_STRING(n);
			if (v && (n = PyObject_Str(v)))
				lv = PyString_AS_STRING(n);
			if (tb && (n = PyObject_Str(tb)))
				tbs = PyString_AS_STRING(n);
		}
	}

	return new KEP::PythonScriptVM(pModule);
}
