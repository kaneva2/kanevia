///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "CIconSystemClass.h"
#include "CShadowClass.h"
#include "RuntimeSkeleton.h"
#include "SkeletonConfiguration.h"
#include "CDeformableMesh.h"
#include "TextureProvider.h"
#include "CArmedInventoryClass.h"
#include "SerializeHelper.h"
#include "client\ContentServiceGLIDCacheTask.h"

namespace KEP {

static LogInstance("ClientEngine");

void ClientEngine::ResetTextureQualityLevels() {
	LogInfo("");
	CTextureEX::ResetQualityLevels();
}

bool ClientEngine::EnableTextureQualityLevel(unsigned quality) {
	LogInfo("quality=" << quality);
	return CTextureEX::EnableQualityLevel((TextureQuality)quality);
}

void ClientEngine::ForceOnePixelTexture(bool force) {
	LogInfo(LogBool(force));
	return CTextureEX::SetForceOnePixel(force);
}

bool ClientEngine::IsForcingOnePixelTexture() const {
	return CTextureEX::IsForcingOnePixel();
}

bool ClientEngine::GetTextureImportInfo(const std::string& fileName, TextureDef& textureInfo) {
	return m_assetsDatabase->GetTextureImportInfo(textureInfo, fileName);
}

unsigned ClientEngine::CreateDynamicTexture(unsigned width, unsigned height) {
	assert(GetTextureDataBase() != nullptr);
	auto res = GetTextureDataBase()->DynamicTextureAdd(width, height);
	if (res == TextureDatabase::INVALID_DYNAMIC_TEXTURE_ID) {
		LogWarn("FAILED: width = " << width << ", height = " << height);
	} else {
		LogInfo("OK: width = " << width << ", height = " << height << ", texture ID = " << res);
	}
	return res;
}

void ClientEngine::DestroyDynamicTexture(unsigned dynamicTextureId) {
	assert(GetTextureDataBase() != nullptr);
	return GetTextureDataBase()->DynamicTextureDestroy(dynamicTextureId);
}

IDirect3DTexture9* ClientEngine::GetDynamicTexture(unsigned dynTextureId) {
	assert(GetTextureDataBase() != nullptr);
	return GetTextureDataBase()->DynamicTextureGetHandle(dynTextureId);
}

IDirect3DTexture9* ClientEngine::GetD3DTextureIfLoadedOrQueueToLoad(const std::string& texFileName) {
	// Valid Filename ?
	if (texFileName.empty()) {
		LogError("texFileName is empty");
		return NULL;
	}

	// Get Texture From Texture Database (immediately or asynchronously queue)
	auto pTex = const_cast<CTextureEX*>(m_pTextureDB->TextureGet(texFileName));
	if (!pTex)
		return nullptr;

	// Return Direct3D Texture Surface
	return pTex->GetD3DTextureIfLoadedOrQueueToLoad(true);
}

IDirect3DTexture9* ClientEngine::GetD3DTextureIfLoadedOrQueueToLoad(DWORD texKey) {
	// Get Texture From Texture Database (immediately or asynchronously queue)
	CTextureEX* pTex = m_pTextureDB->TextureGet(texKey);
	if (!pTex)
		return NULL;

	// Return Direct3D Texture Surface
	return pTex->GetD3DTextureIfLoadedOrQueueToLoad(true);
}

ITexture* ClientEngine::GetTextureIfLoadedOrQueueToLoad(DWORD texKey) {
	// Get Texture From Texture Database (immediately or asynchronously queue)
	CTextureEX* pTex = m_pTextureDB->TextureGet(texKey);
	return pTex;
}

// DRF - UGC Icon Files Are 'icon<glid>.jpg', non-UGC are '<glid>.dds'
std::string ClientEngine::TextureFileName(const GLID& glid) {
	std::string fileName;
	if (IS_UGC_GLID(glid)) {
		StrBuild(fileName, "icon" << (ULONG)glid << ".jpg");
	} else {
		StrBuild(fileName, (ULONG)glid << ".dds");
	}
	return fileName;
}

std::string ClientEngine::buildTexturePath(int textureType, bool thumbnail, int itemId, std::string& url) {
	// Build Texture Path
	std::string texPath = PathCustomTexture();

	std::string fileExt = "";
	switch (textureType) {
		case CUSTOM_TEXTURE:
			fileExt = "ct";
			break;

		case FRIEND_PICTURE:
			fileExt = "fp";
			break;

		case GIFT_PICTURE:
			fileExt = "gt";
			break;

		default:
			LogError("Invalid textureType=" << textureType);
			return "";
	}

	if (textureType == FRIEND_PICTURE) {
		static std::map<std::string, std::string> urlFileMap; // url -> filename map

		// Build Get Image Url
		std::string urlGetImage = thumbnail ? m_friendThumbUrl : m_friendUrl;
		urlGetImage = StrReplace(urlGetImage, "{0}", std::to_string(itemId));

		// Get The Image Url (blocking)
		std::string resultText;
		auto it = urlFileMap.find(urlGetImage);
		if (it == urlFileMap.end()) {
			// Get Url Filename From Web
			DWORD httpStatusCode;
			std::string httpStatusDescription;
			if (!_GetBrowserPage(urlGetImage, 0, 0, resultText, &httpStatusCode, httpStatusDescription)) {
				LogError("GetBrowserPage() FAILED - '" << urlGetImage << "'");
				return "";
			}

			urlFileMap[urlGetImage] = resultText;
		} else {
			resultText = it->second;
		}
		url = resultText;

		// Strip Filename From Result
		std::string fileName = StrStripLeftLast(resultText, "/");
		if (fileName.empty()) {
			LogError("Bad GetImage Result - " << resultText);
			return "";
		}

		// Build Texture Path Using Filename
		std::string texFile;
		StrBuild(texFile, itemId << "_" << fileName << "." << fileExt);
		texPath = PathAdd(texPath, texFile);

	} else {
		if (!url.empty()) {
			url = m_imageServerUrl + url;
		}

		std::string texFile;
		StrBuild(texFile, itemId << "." << fileExt);
		texPath = PathAdd(texPath, texFile);
	}

	if (thumbnail) {
		StrAppend(texPath, "_thumb");
	}

	return texPath;
}

bool ClientEngine::DownloadExternalTexture(int textureType, bool thumbnail, int itemId, const char* textureUrl, const char* filePath) {
	CStringA strURL;
	CStringA strItemId;
	strItemId.Format("%d", itemId);

	switch (textureType) {
		case CUSTOM_TEXTURE: {
			// Make Webcall To Get Actual Texture Url
			std::string resultText;
			DWORD httpStatusCode;
			std::string httpStatusDescription;
			strURL = thumbnail ? m_imageThumbUrl.c_str() : m_imageUrl.c_str();
			strURL.Replace("{0}", strItemId);
			if (textureUrl != 0 && *textureUrl != '\0') { // we already have the relative URL, don't need to get it
				resultText = textureUrl;
			} else if (!_GetBrowserPage((const char*)strURL, 0, 0, resultText, &httpStatusCode, httpStatusDescription)) {
				LogError("GetBrowserPage() FAILED - '" << strURL << "'");
				return false;
			}

			// Download Texture File
			return DownloadFile(resultText, filePath);
		} break;

		case FRIEND_PICTURE: {
			// Make Webcall To Get Actual Texture Url
			std::string resultText;
			DWORD httpStatusCode;
			std::string httpStatusDescription;
			strURL = thumbnail ? m_friendThumbUrl.c_str() : m_friendUrl.c_str();
			strURL.Replace("{0}", strItemId);
			if (textureUrl != 0 && *textureUrl != '\0') { // we already have the relative URL, don't need to get it
				resultText = textureUrl;
			} else if (!_GetBrowserPage((const char*)strURL, 0, 0, resultText, &httpStatusCode, httpStatusDescription)) {
				LogError("GetBrowserPage() FAILED - '" << strURL << "'");
				return false;
			}

			// Download Texture File
			return DownloadFile(resultText, filePath);
		} break;

		case GIFT_PICTURE: {
			// Download Texture File
			strURL = thumbnail ? m_giftThumbUrl.c_str() : m_giftUrl.c_str();
			strURL.Replace("{0}", strItemId);
			return DownloadFile(strURL.GetString(), filePath);
		} break;
	}

	return false;
}

// get a texture file, given a url, returns filename in customTextures folder
std::string ClientEngine::DownloadTexture(int textureType, int itemId, const char* textureUrl, bool thumbnail) {
	return GetExternalTexture(textureType, thumbnail, itemId, textureUrl);
}

// get a texture file, given a url, returns filename in customTextures folder but
// lets the texture itself download in the background
std::string ClientEngine::DownloadTextureAsync(int textureType, int itemId, const char* textureUrl, bool thumbnail) {
	// This implements a slightly different version of ClientEngine::GetDynamicTextureAsync()
	// which perhaps should just be a different version of that function.
	if (!textureUrl)
		return "";

	std::string url(textureUrl);
	std::string filePath = buildTexturePath(textureType, thumbnail, itemId, url);
	if (filePath.empty())
		return "";

	if (url.empty())
		return "";

	IEvent* e = m_dispatcher.MakeEvent(GetDownloadLooseTextureEventId());
	// Use DL_PRIO_HIGH - assuming we want menu icons before 3D contents
	if (!DownloadFileAsync(ResourceType::OTHER_TEXTURE, url, DL_PRIO_HIGH, filePath, 0, false, e, false)) {
		e->Delete();
		return "";
	}
	return filePath;
}

// Asynchronously download texture to 'CustomTexture\<ctPath>\<texFileName>'
std::string ClientEngine::DownloadTextureAsync(const std::string& texFileName, const std::string& texUrl, const std::string& ctPath) {
	if (texUrl.empty() || texFileName.empty())
		return "";

	// Build Texture Url
	std::string texUrlFull = m_imageServerUrl + texUrl;

	// Build Texture Path 'CustomTexture\<ctPath>'
	std::string texPath = PathCustomTexture();
	if (!ctPath.empty())
		texPath = PathAdd(texPath, ctPath);

	// Create Texture Path Folder (deep)
	if (!FileHelper::CreateFolderDeep(texPath)) {
		LogError("FileHelper::CreateFolderDeep() FAILED - '" << texPath << "'");
		return "";
	}

	// Download Texture Asynchronously
	IEvent* e = m_dispatcher.MakeEvent(GetDownloadLooseTextureEventId());
	texPath = PathAdd(texPath, texFileName);
	// Use DL_PRIO_HIGH - assuming we want menu icons before 3D contents
	if (!DownloadFileAsync(ResourceType::OTHER_TEXTURE, texUrlFull, DL_PRIO_HIGH, texPath, 0, false, e, false)) {
		e->Delete();
		return "";
	}

	return texPath;
}

std::string ClientEngine::GetExternalTexture(int textureType, bool thumbnail, int itemId, const char* textureUrl) {
	jsVerifyReturn(m_pDM != nullptr, "");

	std::string url;
	if (textureUrl != nullptr)
		url = textureUrl;
	std::string filePath = buildTexturePath(textureType, thumbnail, itemId, url);

	// File Already Downloaded ?
	if (FileHelper::Exists(filePath))
		return filePath;

	// Download Texture File
	if (!DownloadExternalTexture(textureType, thumbnail, itemId, url.empty() ? NULL : url.c_str(), filePath.c_str()))
		return "";
	return filePath;
}

bool ClientEngine::GetExternalTextureAsync(
	int textureType,
	int assetId,
	const char* textureUrl,
	IEvent* pEvent,
	DownloadPriority prio,
	bool thumbnail) {
	jsVerifyReturn(textureUrl != nullptr, false);

	std::string url(textureUrl);
	std::string filePath = buildTexturePath(textureType, thumbnail, assetId, url);

	if (filePath.empty()) {
		pEvent->Delete();
		return false;
	}

	// Valid URL ?
	bool urlOk = STLBeginWithIgnoreCase(url, "http:");
	if (!urlOk) {
		LogError("BAD URL '" << url << "' file='" << filePath << "'");
		return false;
	}

	// Download Texture (asynchronously)
	// This texture is associated with a dynamic object. It should be integrated with the new dynamic resource priority.
	// However it does not use IResource at this moment. Use the old logic for now considering we don't have lots of
	// user textures in game worlds.
	// TODO: use IResource for custom textures.
	return DownloadFileAsync(ResourceType::CUSTOM_TEXTURE, url, prio, filePath, 0, false, pEvent, false);
}

bool ClientEngine::ApplyCustomTexture(int objId, int assetId, bool thumbnail, const char* texUrl) {
	// Find Dynamic Object
	auto pDPO = GetDynamicObject(objId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - objId=" << objId);
		return false;
	}

	// Use Default Texture Url ?
	std::string texUrlStr = texUrl ? texUrl : "";

	// DRF - ED-1173 - Build Mode Fixes - Set Default Texture For assetId=0!
	// Use Default Texture ? (assetId <= 0) -> 0
	if (assetId <= 0)
		assetId = 0;

	// Get Texture File Name
	std::string texFileName;
	if (assetId > 0) {
		texFileName = GetExternalTexture(CUSTOM_TEXTURE, thumbnail, assetId, 0);
		if (texFileName.empty()) {
			// Probably a drag drop where we don't know the texture name yet.
			// Send the event anyway and we will get an update from the server with correct URL.
			if (!pDPO->IsLocal()) {
				IEvent* e = new UpdateDynamicObjectTextureEvent(objId, assetId, texUrlStr.c_str(), pDPO->m_playerId);
				e->AddTo(SERVER_NETID);
				m_dispatcher.QueueEvent(e);
				return true;
			}
			return false;
		}
	}

	// JONNY - All we wanted was to make sure we downloaded the texture we can stop now
	if (objId < 0) {
		return true;
	}

	// Valid File Name ?
	if (assetId > 0) {
		if (pDPO->GetCustomTextureUrl() == texFileName) {
			LogError("texFileName INVALID - '" << texFileName << "'");
			return false;
		}
	}

	// Apply Texture
	pDPO->SetAssetId(assetId);
	pDPO->SetFriendId(-1);
	if (!texFileName.empty()) {
		pDPO->SetCustomTexture(true, std::make_shared<CustomTextureProvider>(texFileName));
	} else {
		pDPO->SetCustomTexture(true, std::shared_ptr<CustomTextureProvider>());
	}
	pDPO->SetTextureUrl(texUrlStr);
	pDPO->UpdateCustomMaterials();
	IEvent* e = new UpdateDynamicObjectTextureEvent(objId, assetId, texUrlStr.c_str(), pDPO->m_playerId);
	if (!pDPO->IsLocal()) // bypass server if local dynamic object
		e->AddTo(SERVER_NETID);
	m_dispatcher.QueueEvent(e);
	return true;
}

bool ClientEngine::ApplyCustomTextureWorldObject(int objId, int assetId, bool thumbNail) {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix

	auto pWO = GetWorldObject(objId);
	if (!pWO) {
		LogError("GetWorldObject() FAILD - objId=" << objId);
		return false;
	}

	// DRF - ED-1173 - Build Mode Fixes - Set Default Texture For assetId=0!
	// Use Default Texture ? (assetId <= 0) -> 0
	if (assetId <= 0)
		assetId = 0;

	// Get Texture File Name
	std::string texFileName;
	if (assetId > 0) {
		texFileName = GetExternalTexture(CUSTOM_TEXTURE, thumbNail, assetId, 0);
		if (!texFileName.length() || (pWO->GetCustomTexture().compare(texFileName) == 0)) {
			LogError("texFileName INVALID - '" << texFileName << "'");
			return false;
		}
	}

	// Apply Texture
	pWO->m_assetId = assetId;
	pWO->SetCustomTexture(texFileName);
	pWO->UpdateCustomMaterials();
	return true;
}

bool ClientEngine::ApplyCustomTextureLocal(int objId, const std::string& textureFileName) {
	if (textureFileName.empty())
		return false;

	if (objId < 0) {
		// All we wanted was to make sure we downloaded the texture we can stop now. -- Jonny
		return true;
	}

	auto pDPO = GetDynamicObject(objId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - objId=" << objId);
		return false;
	}

	if (pDPO->GetCustomTextureUrl() != textureFileName) {
		pDPO->SetFriendId(-1);
		pDPO->SetCustomTexture(true, std::make_shared<CustomTextureProvider>(textureFileName));
		pDPO->UpdateCustomMaterials();
	}
	return true;
}

bool ClientEngine::ApplyCustomTextureWorldObjectLocal(int objId, const std::string& textureFileName) {
	if (textureFileName.empty())
		return false;

	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix

	auto pWO = GetWorldObject(objId);
	if (!pWO) {
		LogError("GetWorldObject() FAILED - objId=" << objId);
		return false;
	}

	if (pWO->GetCustomTexture().compare(textureFileName) != 0) {
		pWO->SetCustomTexture(textureFileName, eTexturePath::MapsModels, IMPORTED_TEXTURE);
		pWO->UpdateCustomMaterials();
	}
	return true;
}

bool ClientEngine::SetPlayerCustomTextureOnAccessoryLocal(RuntimeSkeleton* pRuntimeSkeleton, int glid, const std::string& textureUrl) {
	if (!pRuntimeSkeleton)
		return false;

	if (!pRuntimeSkeleton->updateEquippableItemCustomTexture(glid, textureUrl, GetTextureDataBase())) {
		LogError("FAILED - Unknown Player - glid<" << glid << ">");
		return false;
	}

	return true;
}

BOOL ClientEngine::AddTextureToDatabase(CEXMeshObj* pMO, BOOL remesh, DWORD maxTex) {
	// Valid Mesh Object ?
	if (!pMO)
		return FALSE;

	// Add Mesh's Material Object To Database
	return AddTextureToDatabase(pMO->m_materialObject, remesh, maxTex);
}

BOOL ClientEngine::AddTextureToDatabase(CMaterialObject* pMO, BOOL remesh, DWORD maxTex) {
	// Valid Material Object ?
	if (!pMO)
		return FALSE;

	// Add All Material Object Texture Stages To Database
	for (int textureNum = 0; textureNum < 7; textureNum++) {
		if ((DWORD)textureNum > maxTex - 1) {
			pMO->m_texNameHash[textureNum] = INVALID_TEXTURE_NAMEHASH;
		} else {
			std::string texFileName = pMO->m_texFileName[textureNum];

			// Add Texture To Texture Database
			m_pTextureDB->TextureAdd(pMO->m_texNameHashStored[textureNum], texFileName);
			DWORD nameHash = pMO->m_texNameHashStored[textureNum];
			if (!ValidTextureNameHash(nameHash)) {
				LogError("AddTextureToDatabase() FAILED - texName='" << texFileName << "'");
			} else {
				pMO->m_texNameHash[textureNum] = nameHash;
			}
		}
	}

	// update the meshobject ReMaterial
	if (remesh && pMO->GetReMaterial()) {
		ReRenderStateData state;
		pMO->GetReRenderState(&state);
		pMO->GetReMaterial()->Update(&state);
	}

	return TRUE;
}

// Content service must have the URL ready for requested texture
void ClientEngine::AddUGCTexturesToDatabase(const std::list<int>& items) {
	if (items.size() <= 0)
		return;

	for (const auto& glidItem : items) {
		GLID glid = (GLID)abs(glidItem);

		// Skip Non-UGC Objects
		if (!IS_UGC_GLID(glid))
			continue;

		// Get Already Cached Content Metadata (skip if not already cached? bug?)
		ContentMetadata md(glid);
		if (!md.IsValid())
			continue;

		// Add Texture To Texture Database
		std::string texFileName = TextureFileName(glid);
		std::string texUrl = md.getThumbnailPath();
		DWORD nameHash = INVALID_TEXTURE_NAMEHASH;
		m_pTextureDB->TextureAdd(nameHash, texFileName, "", texUrl);
		if (!ValidTextureNameHash(nameHash)) {
			LogError("AddTextureToDatabase(UGC) FAILED - glid<" << glid << "> texName='" << texFileName << "' texUrl='" << texUrl << "'");
			continue;
		} else {
			// force it to start downloading, if you don't do this, the
			// icon won't ever show up, because menus don't load their textures
			// via the ResourceManager system we have in place
			CTextureEX* pTex = m_pTextureDB->TextureGet(nameHash);
			if (pTex)
				pTex->GetD3DTextureIfLoadedOrQueueToLoad();
		}
	}
}

std::string ClientEngine::AddItemThumbnailToDatabase(const GLID& glid) {
	// DRF - Added
	if (!IS_VALID_GLID(glid)) {
		LogError("INVALID GLID glid<" << glid << ">");
		return "";
	}

	// Get Texture File Name
	std::string texFileName = TextureFileName(glid);

	// UGC Texture ?
	if (IS_UGC_GLID(glid)) {
		// Get Already Cached Content Metadata (or cache asynchronously if not already cached)
		// DRF - TODO - Should this use ContentService::RequestItemData() instead?
		ContentMetadata md(glid);
		if (md.IsValid()) {
			// for UGC Items, I want to download them to the same place the LUA will look when
			// it gets around to it, but download them from the proper UGC URL found in the
			// meta data.  so, the object is added to the texture database here, and when the
			// texture is actually used (by the LUA), the texture will be streamed
			std::string texUrl = md.getThumbnailPath();

			// Add Texture To Texture Database
			DWORD nameHash = INVALID_TEXTURE_NAMEHASH;
			m_pTextureDB->TextureAdd(nameHash, texFileName, "Icons", texUrl);
			if (!ValidTextureNameHash(nameHash)) {
				LogError("AddTextureToDatabase(UGC) FAILED - glid<" << glid << "> texName='" << texFileName << "' texUrl='" << texUrl << "'");
				return "";
			}
		}
#ifdef _ClientOutput
		else {
			LogWarn("NOT CACHED - glid<" << glid << "> - Queueing ContentServiceGLIDCacheTask...");
			std::vector<GLID> glids;
			glids.push_back(glid);
			ContentServiceGLIDCacheTask* pTask = new ContentServiceGLIDCacheTask(m_dispatcher, glids);
			pTask->m_pEventCompletion = new ContentServiceCompletionEvent(glids);
			m_taskSystem_ContentService.QueueTask(pTask);

			// Eventually Calls ProcessScriptIconCompletionEvent() which stores the downloaded icon
			// to the same texFileName location. It isn't there yet but return the name anyway for
			// when it arrives that is where it will be. Script normally renders 'loading.jpg'
			// until the icon arrives.
		}
#endif
	} else {
		// Add Texture To Texture Database
		DWORD nameHash = INVALID_TEXTURE_NAMEHASH;
		m_pTextureDB->TextureAdd(nameHash, texFileName, "Icons");
		if (!ValidTextureNameHash(nameHash)) {
			LogError("AddTextureToDatabase(NON_UGC) FAILED - glid<" << glid << "> texName='" << texFileName << "'");
			return "";
		}
	}

	return texFileName;
}

bool ClientEngine::TextureSearch(const std::string& texFileName, DWORD& index, int maxMipLevels) {
	CTextureEX* pTex = m_pTextureDB->TextureGet(index, texFileName);
	return (pTex != nullptr);
}

// DRF - WARN - Does Not Unload Texture D3D Memory!
// Only called from CTextureEX::~CTextureEX() and is required to assure
// it is also removed from TextureDatabase::m_texMap.
void ClientEngine::OnTextureDestroy(CTextureEX* pTex) {
	if (!pTex)
		return;

	// Remove From Texture Database Texture Map
	if (!m_pTextureDB)
		return;
	m_pTextureDB->TextureMapDel(pTex);
}

// DRF - ED-7035 - Investigate texture memory impact
// Only called from CTextureEX::UnloadTexture() and is required to release
// the shared KepMenu::DXUTgui::CDXUTDialogResourceManager::DXUTTextureNode::m_pTexture.
void ClientEngine::OnTextureUnload(CTextureEX* pTex) {
	if (!pTex)
		return;

	// Release Texture Node From KepMenu
	auto pMenu = ClientEngine::KepMenuInstance();
	if (!pMenu)
		return;
	IDirect3DTexture9* pD3dTex = pTex->GetD3DTexture();
	if (!pD3dTex)
		return;
	pMenu->ReleaseTextureNode(pD3dTex);
}

CTextureEX* ClientEngine::GetTextureFromDatabase(DWORD texNameHash) {
	return m_pTextureDB ? m_pTextureDB->TextureGet(texNameHash) : nullptr;
}

void ClientEngine::ReinitAllTexturesFromDatabase() {
	// Unload All Textures From Memory
	if (m_pTextureDB)
		m_pTextureDB->TextureMapUnloadAll();

	// Add Texture To Texture Database
	std::string shadowFileName = GetShadowFileName();
	if (m_pTextureDB && !shadowFileName.empty())
		m_pTextureDB->TextureAdd(m_shadowIndexTemp, shadowFileName, "Common");
}

void ClientEngine::SkeletalTextureReassignment(SkeletonConfiguration* pSC) {
	if (!pSC || !pSC->m_meshSlots)
		return;

	for (POSITION posLoc = pSC->m_meshSlots->GetHeadPosition(); posLoc != NULL;) {
		auto pAUDBO = (CAlterUnitDBObject*)pSC->m_meshSlots->GetNext(posLoc);
		if (!pAUDBO || !pAUDBO->m_configurations)
			continue;

		for (POSITION cfgPos = pAUDBO->m_configurations->GetHeadPosition(); cfgPos != NULL;) {
			auto pDM = (CDeformableMesh*)pAUDBO->m_configurations->GetNext(cfgPos);
			if (!pDM || !pDM->m_supportedMaterials)
				continue;

			for (POSITION matPos = pDM->m_supportedMaterials->GetHeadPosition(); matPos != NULL;) {
				auto pMO = (CMaterialObject*)pDM->m_supportedMaterials->GetNext(matPos);
				if (!pMO)
					continue;

				// Add Textures To Texture Database
				for (int t = 0; t < 7; t++) {
					std::string texFileName = pMO->m_texFileName[t];
					m_pTextureDB->TextureAdd(pMO->m_texNameHashStored[t], texFileName, "CharacterTextures");
					DWORD nameHash = pMO->m_texNameHashStored[t];
					if (!ValidTextureNameHash(nameHash)) {
						LogError("AddTextureToDatabase() FAILED - texName='" << texFileName << "'");
					} else {
						pMO->m_texNameHash[t] = nameHash;
					}
				}

				//// NOTE: ReMesh NOT supporting more than 1 texture!!!!!
				//// Update the "supported material" ReMaterial
				if (pMO->GetReMaterial()) {
					ReRenderStateData state;
					pMO->GetReRenderState(&state);
					pMO->GetReMaterial()->Update(&state);
				}
			}

			// Add Texture To Database
			AddTextureToDatabase(pDM->m_material);
		}
	}
}

void ClientEngine::AddUGCTexture(const GLID& glid, const std::string& element_name) {
	if (!IS_UGC_GLID(glid))
		return;

	auto pEvent = m_dispatcher.MakeEvent(m_iconMetadataReadyEventId);
	if (!pEvent)
		return;

	(*pEvent->OutBuffer()) << element_name.c_str() << (int)glid;

	// This will kick off the web request and the event will come in upon completion.
	ContentService::Instance()->ContentMetadataCacheAsync(glid, pEvent);
}

void ClientEngine::GetInventoryIconInfo(int iconIndex, RECT& textureCoords, std::string& textureName) {
	// if icon can't be found, use the 0th icon (default)
	CIconSysObj* pIcon = m_iconDatabase->GetByIndex(iconIndex);
	if (!pIcon)
		pIcon = m_iconDatabase->GetByIndex(0);
	if (!pIcon)
		return;

	// Get Texture Dimension (or force immediate load)
	std::string texFileName = (const char*)pIcon->m_textureName;
	GetD3DTextureIfLoadedOrQueueToLoad(texFileName); // force immediate texture load
	DWORD nameHash = m_pTextureDB->TextureMapGetNameHash(texFileName);
	CTextureEX* pTexture = m_pTextureDB->TextureGet(nameHash, texFileName);
	if (!pTexture)
		return;

	// width and height should be the same
	int width = pTexture->GetWidth();
	int icon_width = (int)((float)width / pIcon->m_gridDensity);

	// get the texture coords
	RECT coords = { (LONG)(pIcon->m_xIndex * icon_width),
		(LONG)(pIcon->m_yIndex * icon_width),
		(LONG)(pIcon->m_xIndex * icon_width + icon_width),
		(LONG)(pIcon->m_yIndex * icon_width + icon_width) };

	textureCoords = coords;
	textureName = texFileName;
}

void ClientEngine::GetInventoryIconInfo(CGlobalInventoryObj* invObj, RECT& textureCoords, std::string& textureName) {
	if (!invObj || !invObj->m_itemData)
		return;

	int icon_index = invObj->m_itemData->m_visualRef;
	GetInventoryIconInfo(icon_index, textureCoords, textureName);
}

bool ClientEngine::GetItemInfo(const GLID& glid, std::string& textureName, RECT& textureCoords) {
	auto pGIO = m_globalInventoryDB->GetByGLID(glid);
	if (!pGIO)
		return false;

	GetInventoryIconInfo(pGIO, textureCoords, textureName);
	return true;
}

} // namespace KEP