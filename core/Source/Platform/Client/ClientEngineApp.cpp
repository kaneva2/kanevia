///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

// Warning in Windows SDK header when building with VS2015
#pragma warning(push)
#pragma warning(disable : 4458) // declaration of '' hides class member
#include <gdiplus.h>
#pragma warning(pop)

#include "ClientEngineApp.h"
#include "Common\KEPUtil\Helpers.h"
#include "Common\KEPUtil\RefPtr.h"
#include "Common\KEPUtil\WebCall.h"
#include "Common\KEPUtil\CrashReporter.h"
#include "Common\KEPUtil\RuntimeReporter.h"
#include "DownloadManager.h"
#include "LuaHelper.h"
#include "ImageHelper.h"
#include "KEPStar.h"
#include "FlashControlProxy.h"
#include "KEPClientRes.h"
#include "resource.h"
#include "NetworkCfg.h"

#define REG_APP_NAME K_CLIENT_NAME
#define REG_KEY_NAME "Status"

namespace KEP {

static LogInstance("Exception");
ClientEngineApp g_ceApp; ///< singleton global app instance

BEGIN_MESSAGE_MAP(ClientEngineApp, ClientEngine_CWinApp)
END_MESSAGE_MAP()

//////////////////////////////////////////////////
// Out of memory handler code.
//
// NewHandler is installed by calling _set_new_handler(NewHandler). This is not the
// standard C++ set_new_handler (note the underscore). When NewHandler returns 0, the
// new operator will return a nullptr instead of throwing an exception.

// Allocations larger than this will continue execution. Otherwise, a crash is triggered.
//const size_t s_CrashDumpAllocationThreshold = 0x10000;
const size_t s_CrashDumpAllocationThreshold = size_t(0) - 1; // There is a lot of code that does not handle allocation failures. Better to just log out-of-memory here and exit.
const size_t s_ReservedMemorySize = 0x20000; // Reserve this amount of memory for logging and crash reporting after allocation failure.
static std::unique_ptr<char[]> s_pReservedMemory(std::make_unique<char[]>(s_ReservedMemorySize));
std::atomic<bool> s_bInNewHandler(false);

static int NewHandler(size_t size) {
	// C++ runtime allocates memory when acquiring a lock so we end up in an infinite loop.
	// The situation I encountered was first an attempt to a acquire a lock, out of memory, then could not acquire this lock because
	// the lock acquiring subsystem was holding its own lock and deadlocked on itself.
	if (s_bInNewHandler.exchange(true))
		return 0;

	// Free some memory for logging.
	if (s_pReservedMemory)
		s_pReservedMemory.reset();

	// Write to log.
	try {
		LogError("Allocation FAILED - size=" << size);
		auto pCE = ClientEngine::Instance();
		if (pCE)
			pCE->LogMemory();
	} catch (...) {
	}

	// If this is a large allocation, we will just return 0 and hope the calling code handles it correctly.
	// If this is a small allocation, we have very little memory available (or it's very highly fragmented),
	// so will probably crash soon anyway.
	// Forcing the crash here allows us to collect diagnostics using the memory we reserved.
	if (size <= s_CrashDumpAllocationThreshold) {
		CrashReporter::ReportError(ErrorData("Out Of Memory", ErrorData::TYPE_CRASH), false);
		::ExitProcess(1);
	}

	// Reclaim the logging memory if possible.
	try {
		s_pReservedMemory = std::make_unique<char[]>(s_ReservedMemorySize);
	} catch (std::bad_alloc&) {
		// Couldn't reclaim. Future out of memory condition may cause error report to fail.
	}

	return 0; // return nullptr to memory request.
}

// Initializes Client Log
#define LogInitClient() _LogInitClient(__FUNCTION__)
static bool _LogInitClient(const std::string& func) {
	// Find Log Configuration File (should be in <pathApp>/KClientLogCfg.cfg)
	std::string pathLogCfg = PathFind(PathApp(), K_CLIENT_LOG_CFG, false);

	// Initialize Logger
	return _LogInit(func, pathLogCfg);
}

// Config Dir for all environments '%APPDATA%\Kaneva'
static std::string ConfigDirAllEnv() {
	static std::string configDirAllEnvStr = "";
	if (configDirAllEnvStr == "") {
		wchar_t appDataDir[MAX_PATH] = { 0 };
		wchar_t configDir[MAX_PATH] = { 0 };
		GetEnvironmentVariableW(L"APPDATA", appDataDir, MAX_PATH);
		swprintf_s(configDir, L"%s\\Kaneva", appDataDir);
		configDirAllEnvStr.assign(Utf16ToUtf8(configDir).c_str());
	}
	return configDirAllEnvStr;
}

ClientEngineApp::ClientEngineApp() :
		m_stdOut(0),
		m_stdErr(0),
		m_state(STATE_STOPPED),
		m_gdiplusToken(0),
		m_testGroup(0),
		m_alreadyExists(false),
		m_crashReportSilent(true),
		m_crashReportScreenshot(CR_SCREENSHOT_NONE),
		m_devMode(DEV_MODE_ON),
		m_ripperDetected(0) {
	// Enable Pointer Validation
	::RefPtrEnable(true);
}

// DRF - Never Called!
ClientEngineApp::~ClientEngineApp() {
#if (DRF_QUICK_QUIT_CLEANUP == 1)

	// Set State Stopped (if not already)
	SetStateStopped(false);

	// Disable Web Callers
	WebCaller::DisableInstances();

	// Log App End
	LogFatal("END");
#endif
}

std::string ClientEngineApp::WokUrl() const {
	return StarWokUrl(StarId());
}

std::string ClientEngineApp::PatchUrl() const {
	// Only Do This Once
	static std::string s_patchUrl;
	if (!s_patchUrl.empty())
		return s_patchUrl;

	// Default Patch Version To Best Guess
	auto patchVer = StarVersion(StarId());

	// Discover True Patch Version Via Webcall
	GBPO gbpo;
	StrBuild(gbpo.url, WokUrl() << "serverlist.aspx?id=" << StarId() << SERVERLIST_V);
	auto isProd = StarIsProduction(StarId());
	if (isProd)
		StrAppend(gbpo.url, SERVERLIST_VER);
	if (GetBrowserPage(gbpo)) {
		STLToLower(gbpo.resultText);
		auto pS = gbpo.resultText.find("/patchdata/");
		if (pS != std::string::npos) {
			pS += 11; // all='/patchdata/<version>'
			auto pE = gbpo.resultText.find("/", pS);
			if (pE != std::string::npos)
				patchVer = gbpo.resultText.substr(pS, pE - pS);
		} else {
			LogError("'/patchdata/' NOT FOUND - ...\n"
					 << gbpo.resultText);
		}
	}

	// Build Patch Url
	StrBuild(s_patchUrl, StarPatchUrl(StarId()) << patchVer << "/");
	LogWarn("DODGY PATCH URL DISCOVERY - patchUrl='" << s_patchUrl << "'");
	return s_patchUrl;
}

std::string ClientEngineApp::LoginEmail() const {
	return m_loginEmail;
}

std::string ClientEngineApp::LoginPassword() const {
	return m_loginPassword;
}

std::string ClientEngineApp::TTLang() const {
	return m_ttLang;
}

void ClientEngineApp::ParseStartCfgXml() {
	try {
		// should be in <pathApp>/startcfg.xml
		KEPConfig cfg;
		std::string pathStartCfg = PathFind(PathApp(), "startcfg.xml", false);
		cfg.Load(pathStartCfg.c_str(), "startconfig");
		m_crashReportSilent = (cfg.Get("crash_report_silent", 1) != 0);
		m_crashReportScreenshot = (CR_SCREENSHOT)cfg.Get("crash_report_screenshot", (int)CR_SCREENSHOT_NONE);
	} catch (KEPException* e) {
		e->Delete();
	}
}

static bool RegCUKeyExists(const std::string& key) {
	HKEY hCuSwRipKey;
	return (::RegOpenKeyExW(HKEY_CURRENT_USER, Utf8ToUtf16(key).c_str(), 0, KEY_READ, &hCuSwRipKey) == ERROR_SUCCESS);
}

// DRF - Returns enumerated detected ripper.
int ClientEngineApp::RipperDetected() {
	// Ripper Already Detcted ?
	if (m_ripperDetected)
		return m_ripperDetected;

#if (DRF_RIPPER > 0)
	// Direct DirectX DLL Injection
	if (FileHelper::Exists("d3d8.dll") || FileHelper::Exists("d3d9.dll") || FileHelper::Exists("d3d10.dll") || FileHelper::Exists("d3d11.dll"))
		m_ripperDetected = 1;

	// NinjaRipper (http://gamebanana.com/tools/5638)
	if (FileHelper::Exists("_Ripper") || RegCUKeyExists("software\\black_ninja") || LoadLibraryW(L"intruder.dll"))
		m_ripperDetected = 2;

	// 3DRipperDx (http://www.deep-shadows.com/hax/3DRipperDX.htm)
	if (LoadLibraryW(L"injector.dll") || (LoadLibraryW(L"detoured.dll") && LoadLibraryW(L"proxy.dll")))
		m_ripperDetected = 3;

	// 3DRipper (http://denull.ucoz.ru/load/1-1-0-40)
	if (FileHelper::Exists("ripped") || LoadLibraryW(L"hook3d.dll"))
		m_ripperDetected = 4;

	// 3DVIA Printscreen (http://dassault-systemes-3dvia-printscreen.software.informer.com)
	if (LoadLibraryW(L"ENOV3DFAProcessPatcher.dll") || LoadLibraryW(L"ENOV3DFACapture.dll"))
		m_ripperDetected = 5;
#endif

	if (m_ripperDetected)
		LogWarn("RIPPER DETECTED - ripper=" << m_ripperDetected);

	return m_ripperDetected;
}

BOOL ClientEngineApp::InitInstance() {
	if (!IsWindowsVistaOrGreater()) {
		MessageBoxW(nullptr, L"Kaneva requires a PC with Windows 7 or newer.\nWe look forward to seeing you on a compatible PC.", L"Newer Windows version required", MB_OK);
		return FALSE;
	}

	// Enable per-thread locale for prevent race condition in setlocale() calls issued by D3DXCreateTextureFromFileInMemoryEx
	_configthreadlocale(_ENABLE_PER_THREAD_LOCALE);

#if defined(_DEBUG)
	// Disable dumping memory leaks on program exit because it just takes too long.
	AfxEnableMemoryLeakDump(FALSE);
#endif
	// Initialize OLE
	if (!AfxOleInit()) {
		AfxMessageBox(L"AfxOleInit FAILED");
		return FALSE;
	}
	AfxEnableControlContainer();

	// Install Registry Support
	_RegInstall(REG_APP_NAME, REG_KEY_NAME);
	SetRegistryKey(_T("Kaneva"));

	// Set Max Open File Limit (seems to be 512)
	_setmaxstdio(_getmaxstdio());

	// Sets New Handler For Failed new/malloc()
	_set_new_handler(NewHandler);
	_set_new_mode(1);

	// Determine App Version String 'xx.xx.xx.xx'
	m_appVersion = VersionApp(false);

	// Determine App Path 	'c:/.../kaneva/star/xxxx'
	m_appPath = PathApp(false);

	// Determine Star Path 	'c:/.../kaneva/star/xxxx'
	m_starPath = m_appPath;

	// Determine Star Id 'xxxx'
	m_starId = StrStripLeftLast(m_starPath, "/\\");

	// Runtime In Production Environment?
	m_isProduction = StarIsProduction(m_starId);

	// Get Registry Profile Auto Login Parameters
	_RegCuRxStr(L"loginEmail", m_loginEmail);
	_RegCuRxStr(L"loginPassword", m_loginPassword);

	// Get Registry Profile Enumerated Test Group (randomize if not in registry)
	int testGroup;
	if (_RegCuRxInt(L"testGroup", testGroup)) {
		m_testGroup = testGroup;
	} else {
		m_testGroup = (::GetRandomInt() & 0x01);
	}
	CrashReporter::SetErrorData("testGroup", m_testGroup);

	// Get Registry Profile Translate To Language
	_RegCuRxStr(L"ttLang", m_ttLang);

	// Check If Client Semaphore Already Exists
	CreateSemaphoreW(NULL, 1L, INT_MAX, K_CLIENT_SEMAPHORE_NAME);
	m_alreadyExists = (GetLastError() == ERROR_ALREADY_EXISTS);

	// Parse StartCfg.xml Options (gets m_crashReportSilent)
	ParseStartCfgXml();

	// Get Registry Profile Enumerated Developer Mode (off if not in registry)
	int devMode;
	if (_RegCuRxInt(L"devMode", devMode))
		m_devMode = (DEV_MODE) devMode;

	// Give DevMode To Helpers
	Helpers_SetDevMode(DevMode());

	// Install Crash Reporter (must be before creating any threads) for release builds only
	// Using it in debug build is not helpful and interferes with CRT asserts.
#ifndef _DEBUG
	CR_Install();
#endif

	// Initialize Logger (creates a thread)
	LogInitClient();
	LogInfo("====================================================================");
	LogInfo("===== " << K_CLIENT_EXE << " - v" << AppVersion());
	LogInfo("====================================================================");
	LogInfo("appPath=" << AppPath());
	LogInfo("starPath=" << StarPath());
	LogInfo("starId=" << StarId());
	LogInfo("wokUrl=" << WokUrl());
	LogInfo("testGroup=" << TestGroup());
	LogInfo("isProduction=" << LogBool(IsProduction()));
	if (!LoginEmail().empty())
		LogWarn("loginEmail=" << LoginEmail());
	if (!LoginPassword().empty())
		LogWarn("loginPassword=" << LoginPassword());

	if (DevMode())
		LogWarn("devMode=" << LogBool(DevMode()));

	// Splash System Information
	SysInfo sysInfo;
	if (GetSysInfo(sysInfo)) {
#if defined(KANEVA_ORIG)
		LogInfo("sysName=" << sysInfo.m_sysName);
#endif
		LogInfo("sysVersion=" << sysInfo.m_sysVersion);
	}

	// Splash Helper Versions
	LogInfo("Using " << ImageVersion());
	LogInfo("Using " << LuaVersion());

	// DRF - Test Web Caller - TESTING ONLY!
	//WebCaller::Test();

	// Don't Allow Rippers
	if (RipperDetected()) {
#if (DRF_RIPPER > 1)
		MessageBoxW(NULL, L"Better Luck Next Time!", L"Error", MB_ICONEXCLAMATION);
		return FALSE;
#endif
	}

	// Initialize GDI+
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&m_gdiplusToken, &gdiplusStartupInput, NULL);

#ifdef _DEBUG
	afxTraceEnabled = false;
#endif

	// Get Client Engine Singleton As This CWinApp's Main Window (constructs ClientEngine)
	auto pCE = ClientEngine::Instance();
	m_pMainWnd = dynamic_cast<CWnd*>(pCE);
	if (!m_pMainWnd) {
		CrashReporter::ReportError(ErrorData("InitInstance(ClientEngine)"), true);
		LogFatal("InitInstance(ClientEngine)");
		return FALSE;
	}

	// Get Window Coordinates From Registry (adjusted for window style)
	RECT rect;
	GetWindowCoords(rect, true);

	// Create & Show Client Engine Window
	if (!pCE->Create("KEP Engine", IDR_MAINFRAME, IDR_MAINMENU, rect)) {
		CrashReporter::ReportError(ErrorData("InitInstance(Create)"), true);
		LogFatal("InitInstance(Create)");
		return FALSE;
	}
	pCE->SetForegroundWindow();

	// Initialize Autoexec & Parse Command Line (presents 'Loading, please wait...' screen)
	pCE->AutoExecInitAndParseCommandLine();

#if (DRF_FLASH_COMPATIBILITY == 1)
	// Verify Flash Compatibility
	SFPCVersion version;
	if (pCE->GetMediaEnabled() && FlashIsInstalled() && GetInstalledFlashVersionEx(&version)) {
		// DRF - ED-3798 - Microsoft Update Flash Crash (v20.0.0.267 + Win8/8.1)
		bool ed3798 = true && (sysInfo.m_majorVersion == 6) && ((sysInfo.m_minorVersion == 2) || (sysInfo.m_minorVersion == 3)) && (version.v[3] == 20) && (version.v[2] == 0) && (version.v[1] == 0) && (version.v[0] == 267);

		// DRF - ED-3800 - Disable Media For Incompatible Flash
		if (ed3798) {
			LogError("ED-3800 - FLASH INCOMPATIBLE - Disabling Media...");
			std::string msg;
			StrBuild(msg, "Flash v" << version.v[3] << "." << version.v[2] << "." << version.v[1] << "." << version.v[0] << " is currently incompatible with Kaneva and media will be disabled.");
			LogError(msg);
			AfxMessageBox(Utf8ToUtf16(msg).c_str(), MB_ICONEXCLAMATION);
			pCE->SetMediaEnabled(false);
		}
	}
#endif

	// Initialize Application
	if (!ClientEngine_CWinApp::InitInstance()) {
		CrashReporter::ReportError(ErrorData("ClientEngine_CWinApp::InitInstance() FAILED"), true);
		LogFatal("ClientEngine_CWinApp::InitInstance() FAILED");
		return FALSE;
	}

	// Install Runtime Reporter (runtime timer begins now)
	auto runtimeId = pCE->getRuntimeId();
	RR_Install(runtimeId);
	runtimeId = RuntimeReporter::RuntimeId();
	pCE->setRuntimeId(runtimeId);

	return TRUE;
}

int ClientEngineApp::Run() {
	// Set Application State Running (restore window coords)
	SetStateRunning(true);

	// Run Application
	ClientEngine_CWinApp::Run();

	// Set Application State Stopped (save window coords)
	SetStateStopped(true);

	// Exit Application (no error)
	return ErrorData::TYPE_NONE;
}

///////////////////////////////////////////////////////////////////////////////
// Application State Functions
///////////////////////////////////////////////////////////////////////////////

bool ClientEngineApp::SetStateRunning(bool restoreWindowCoords) {
	// Not Stopped ?
	if (m_state != STATE_STOPPED)
		return false;
	m_state = STATE_RUNNING;

	// Set Runtime Profile State Running If Only Instance
	if (!AlreadyExists())
		RuntimeReporter::ProfileStateRunning(); // sends running report

	// Show Memory Log
	auto pCE = ClientEngine::Instance();
	if (pCE)
		pCE->LogMemory();

	// Restore Window Coordinates?
	if (restoreWindowCoords)
		RestoreWindowCoords();

	return true;
}

bool ClientEngineApp::SetStateStopped(bool saveWindowCoords) {
	// Not Running ?
	if (m_state != STATE_RUNNING)
		return false;
	m_state = STATE_STOPPED;

	// Set Runtime Profile State Stopped If Only Instance
	if (!AlreadyExists())
		RuntimeReporter::ProfileStateStopped(); // sends stopped report

	// Show Memory Log
	auto pCE = ClientEngine::Instance();
	if (pCE)
		pCE->LogMemory();

	// Save Window Coordinates?
	if (saveWindowCoords)
		SaveWindowCoords();

	return true;
}

bool ClientEngineApp::SetStateCrashed() {
	// Already Crashed ?
	if (m_state == STATE_CRASHED)
		return false;
	m_state = STATE_CRASHED;

	// Set Runtime Profile State Crashed If Only Instance
	if (!AlreadyExists())
		RuntimeReporter::ProfileStateCrashed(); // sends crashed report

#if (DRF_LOGS_ON_CRASH == 1)
	// Shutdown Logs
	auto pCE = ClientEngine::Instance();
	if (pCE)
		pCE->LogMemory();
#endif

	return true;
}

bool ClientEngineApp::SaveWindowCoords() {
	HWND hWnd = CWinApp::GetMainWnd()->GetSafeHwnd();

	// Do Not Save If Main Window Minimized
	WINDOWPLACEMENT wndpl;
	if (!::GetWindowPlacement(hWnd, &wndpl) || (wndpl.showCmd == SW_SHOWMINIMIZED))
		return false;

	// Get Main Window Coordinates
	RECT rect;
	bool ok = (::GetWindowRect(hWnd, &rect) == TRUE);
	if (ok) {
		// Write Coordinates In Registry
		char str[128] = "";
		sprintf_s(str, "%ld %ld %ld %ld", rect.left, rect.top, rect.right, rect.bottom);
		ok &= _RegCuTxStr(L"coords", str);
	}

	LogInfo("coords=[" << rect.left << " " << rect.top << " " << rect.right << " " << rect.bottom << "] " << (ok ? "OK" : "FAIL"));

	return ok;
}

bool ClientEngineApp::GetWindowCoords(RECT& rect, bool adjStyle) {
	// Read Coordinates From Registry (or set to workarea)
	std::string str;
	bool ok = _RegCuRxStr(L"coords", str);
	ok &= (sscanf_s(str.c_str(), "%ld %ld %ld %ld", &rect.left, &rect.top, &rect.right, &rect.bottom) == 4);
	if (!ok)
		ok = (::SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0) == TRUE);

	// Sanity Check Window Size
	LONG width = rect.right - rect.left;
	LONG height = rect.bottom - rect.top;
	ok &= (width >= 640 && height >= 480 && width <= 6400 && height <= 4800);

	// Sanity Check Window Overlaps Monitor
	RECT rectBorder = { rect.left + 100, rect.top + 100, rect.right - 100, rect.bottom - 100 }; // bordered coords
	ok &= (::MonitorFromRect(&rectBorder, MONITOR_DEFAULTTONULL) != NULL);

	// Use Default If Bad
	if (!ok)
		rect = RECT({ 0, 0, WIN_WIDTH_DEFAULT, WIN_HEIGHT_DEFAULT });

	// Adjust For Window Style ? (subtract [16 x 39] to match DXUT::DXUTChangeDevice())
	if (adjStyle) {
		RECT rectAdj = RECT({ 0, 0, 0, 0 });
		::AdjustWindowRect(&rectAdj, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN, false);
		rect.right -= RectWidth(rectAdj);
		rect.bottom -= RectHeight(rectAdj);
	}

	return ok;
}

bool ClientEngineApp::RestoreWindowCoords() {
	// Set Main Window Coordinates From Registry
	RECT rect;
	bool ok = GetWindowCoords(rect, false);
	if (ok) {
		HWND hWnd = CWinApp::GetMainWnd()->GetSafeHwnd();
		ok &= (::MoveWindow(hWnd, rect.left, rect.top, RectWidth(rect), RectHeight(rect), TRUE) == TRUE);
	}

	LogInfo("coords=[" << rect.left << " " << rect.top << " " << rect.right << " " << rect.bottom << "] " << (ok ? "OK" : "FAIL"));

	return ok;
}

///////////////////////////////////////////////////////////////////////////////
// RuntimeReporter Support
///////////////////////////////////////////////////////////////////////////////
static void RR_AppCallback(RuntimeReporter::CallbackStruct& cbs) {
	g_ceApp.RR_AppCallback(cbs);
}

bool ClientEngineApp::RR_Install(const std::string& runtimeId) {
	// Install Runtime Reporter
	return RuntimeReporter::Install(REG_APP_NAME, REG_KEY_NAME, WokUrl() + "runtimeReport.aspx?", ::KEP::RR_AppCallback, runtimeId);
}

static void RR_WebCallerParams(RuntimeReporterParams& params, const std::string& wcId, const std::string& paramId) {
	// Build WebCaller Runtime Report Params String
	WebCaller* pWC = WebCaller::GetInstance(wcId, false);
	if (pWC) {
		params.Add(paramId + ".webCalls", pWC->CallsCompleted());
		params.Add(paramId + ".webCallsMs", pWC->CallTimeMs());
		params.Add(paramId + ".webCallsResponseMs", pWC->ResponseTimeMs());
		params.Add(paramId + ".webCallsBytes", pWC->ResponseBytes());
		params.Add(paramId + ".webCallsErrored", pWC->CallsErrored());
	}
}

void ClientEngineApp::RR_AppCallback(RuntimeReporter::CallbackStruct& cbs) {
	RuntimeReporterWebParams params;

	// Append User Id If We Have It
	auto pCE = ClientEngine::Instance();
	if (pCE) {
		params.Add("userId", pCE->GetKanevaUserId());
	}

	// Append Report Type Parameters
	switch (cbs.type) {
		case RuntimeReporter::REPORT_TYPE_RUNNING: {
			// Append Test Group
			params.Add("testGroup", TestGroup());

			// Append GPU Info
			GpuInfo gpuInfo;
			if (GetGpuInfo(gpuInfo)) {
				params.Add("gpu", gpuInfo.m_gpuName);
				params.Add("dxVer", gpuInfo.m_dxVer);
			}

			// Append Client Engine Stats
			if (pCE) {
				// Append Display Modes
				params.Add("gpuX", pCE->DisplayModeWidth());
				params.Add("gpuY", pCE->DisplayModeHeight());
				params.Add("gpuHz", pCE->DisplayModeRefreshRate());
				params.Add("gpuFmt", pCE->DisplayModeFormat());
			}
		} break;

		case RuntimeReporter::REPORT_TYPE_STOPPED:
		case RuntimeReporter::REPORT_TYPE_CRASHED: {
			// Append Client Engine Stats
			if (pCE) {
				// Append Texture Memory Stats
				auto tms = pCE->GetTextureMemoryStats();
				params.Add("texMemNum", tms.m_num);
				params.Add("texMemAvg", tms.m_avg);
				params.Add("texMemMin", tms.m_min);
				params.Add("texMemMax", tms.m_max);

				// Append Frames Per Second
				double fps = 0.0;
				if (pCE->MetricsFps(ClientMetrics::FPS_ID::RUNTIME, fps)) {
					params.Add("fps", fps);
					pCE->MetricsFpsReset(ClientMetrics::FPS_ID::RUNTIME);
				}

				// Append Flash Stats
				params.Add("flashCrashes", pCE->GetFlashProcessCrashCount());
				params.Add("flashPeakInstances", pCE->GetPeakFlashInstanceCount());

				// Append Move Message Enabled Time
				params.Add("msgMoveEnabledTimeSec", pCE->m_msgMoveEnabledTimer.ElapsedSec());
			}

			// Append WebCaller Stats
			RR_WebCallerParams(params, "WebCallTask", "WCT");
			RR_WebCallerParams(params, "RuntimeReporter", "RR");
			RR_WebCallerParams(params, "GetBrowserPage", "GBP");
			RR_WebCallerParams(params, "Downloader", "DL");

			// Append Ripper Detected
			params.Add("ripperDetected", m_ripperDetected);
		} break;

		case RuntimeReporter::REPORT_TYPE_CUSTOM: {
			// Append Client Engine Stats
			if (pCE) {
				// Append Zone Info (gameId, zoneInstanceId, zoneType)
				int gameId = pCE->GetPrevGameId();
				int zoneInstanceId = pCE->GetZoneInstanceId();
				ULONG zoneType = pCE->GetZoneIndex().GetType();
				ULONG zoneIndex = pCE->GetZoneIndex().GetTypeAndChannelId();
				params.Add("gameId", gameId);
				params.Add("zoneInstanceId", zoneInstanceId);
				params.Add("zoneType", zoneType);
				params.Add("zoneIndex", zoneIndex);

				// Append Session Frames Per Second
				double fps = 0.0;
				if (pCE->MetricsFps(ClientMetrics::FPS_ID::CUSTOM, fps)) {
					params.Add("fps", fps);
					pCE->MetricsFpsReset(ClientMetrics::FPS_ID::CUSTOM);
				}

				// ED-7732 - Texture Scaling
				// Append Zone Texture Memory Usage
				auto texMem = pCE->GetAllTextureMemoryUsage();
				params.Add("texMem", texMem);
			}
		} break;
	}

	// Update CallbackStruct
	cbs.params = params.GetString();
	cbs.willLog = false; // jeff's request
}

///////////////////////////////////////////////////////////////////////////////
// CrashReporter Support
///////////////////////////////////////////////////////////////////////////////
static void CR_AppCallback(CrashReporter::CallbackStruct& cbs) {
	g_ceApp.CR_AppCallback(cbs);
}

bool ClientEngineApp::CR_Install() {
	// Install Crash Reporter (default version)
	bool ok = CrashReporter::Install(K_CLIENT_NAME, "", ::KEP::CR_AppCallback, CR_MINIDUMP_LOW, !CrashReportSilent());

	// Add Crash Report Attachments
	CrashReporter::AddFile(PathAdd(PathApp(), "*.log*"), "Log");
	CrashReporter::AddFile(PathAdd(PathApp(), "..\\..\\*.log*"), "Launcher Log");
	CrashReporter::AddFile(PathAdd(PathApp(), "..\\..\\KepDiag.txt"), "Launcher DxDiag");
	CrashReporter::AddRegistryKey("HKEY_CURRENT_USER\\Software\\Kaneva");
	CrashReporter::AddScreenshot(CrashReportScreenshot());

	return ok;
}

void ClientEngineApp::CR_AppCallback(CrashReporter::CallbackStruct& cbs) {
	// Is Not Crash (report only) ?
	if (cbs.isNotCrash)
		return;

	// Cancel Crash Reports During Shutdown
	auto pCE = ClientEngine::Instance();
	bool isShutdown = (pCE ? pCE->IsShutdown() : false);
	if (isShutdown) {
		LogFatal("Shutdown - Canceling Report...");

		// Set State Stopped
		SetStateStopped(false);

		// Cancel Report
		cbs.willSend = false;

		return;
	}

	// Identify As 'Client Crash' If No Known Reason
	ErrorData* pED = CrashReporter::GetErrorData();
	bool noReason = (pED && pED->reason.empty());
	if (noReason) {
		pED->reason = "Client Crash";
		pED->type = ErrorData::TYPE_CRASH;
	}

	// Set Runtime Reporter State
	if (pED && pED->type != ErrorData::TYPE_CRASH)
		SetStateStopped(false);
	else
		SetStateCrashed();

	// Wait For Runtime Report Response Complete
	RuntimeReporter::WaitForReportResponseComplete();
}

} // namespace KEP