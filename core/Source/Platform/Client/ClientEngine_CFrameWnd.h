///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "KEPCommon.h"
#include "KEPConstants.h"
#include "CABD3DFunctionLib.h"
#include "DxDevice.h"

#define CAM_AUTO_AZIMUTH_TAG "camAutoAzimuth"
#define CAM_AUTO_AZIMUTH_DEFAULT 0.0

#define CAM_AUTO_ELEVATION_TAG "camAutoElevation"
#define CAM_AUTO_ELEVATION_DEFAULT 0.349066 // 20 degrees (radians)

// Forward declarations
enum CR_SCREENSHOT;

namespace KEP {

class IKEPConfig;

struct DisplayMode {
	int width, hight, depth;
};

class ClientEngine_CFrameWnd : public CFrameWnd {
public:
	bool m_devMode_CFrameWnd;

	CRect GuiRect;

	RECT m_curModeRect;
	int m_curModeDepth;

	int m_lastModeHeight;
	int m_lastModeWidth;
	int m_lastModeDepth;

	int m_szXMonitor;
	int m_szYMonitor;
	UINT m_windowStateGL;

	int m_antiAlias;

	int m_LodBiasMovObj;
	bool m_LodBiasMovObjAuto;

	int m_cullBiasDynObj;
	bool m_cullBiasDynObjAuto;

	float m_cullDistMovObj;

	int m_particleBias;
	bool m_particleBiasAuto;

	int m_animBiasMovObj;
	bool m_animBiasMovObjAuto;

	int m_animBiasDynObj;
	bool m_animBiasDynObjAuto;

	int m_avatarNames;
	bool m_avatarNamesAuto;

	bool m_invertY;

	bool m_mLookSwap;

	bool m_mediaEnabled;

	bool m_SATEnabled;

	bool m_shadows;
	BOOL m_globalShadowType;
	float m_globalShadowDist;
	BOOL m_showFirstPersonShadow;
	int m_maxShadowCount;

	DisplayMode* m_displayModes;

	//shadowPlane used with stencil
	LPDIRECT3DVERTEXBUFFER9 m_shadowSquareVB;

	//Supported features
	BOOL m_supportedStencilBuffer;

	BOOL m_shaderSupport;

	BOOL m_crashReportSilent; ///< send crash reports silently (no gui)

	CR_SCREENSHOT m_crashReportScreenshot; ///< crash report screenshot setting

	CStringA m_loginEmail; ///< email to use for auto login

	CStringA m_loginPassword; ///< password to use for auto login

	BOOL m_feedBackReportPrompted; // whether has asked user to send feedback report or not

	HINSTANCE instanceHandle;

	CABD3DFunctionLib* m_pCABD3DLIB;

	D3DPRESENT_PARAMETERS d3dpp;

	// DRF - Current Display Mode for Runtime Report (set in CreateImmediateModeDevice())
	D3DDISPLAYMODE d3ddm;
	UINT DisplayModeWidth() {
		return d3ddm.Width;
	}
	UINT DisplayModeHeight() {
		return d3ddm.Height;
	}
	UINT DisplayModeFormat() {
		return d3ddm.Format;
	}
	UINT DisplayModeRefreshRate() {
		return d3ddm.RefreshRate;
	}

	HWND m_hWndSafe;

	D3D_ERROR m_d3dError;

	ClientEngine_CFrameWnd() {}

	virtual ~ClientEngine_CFrameWnd() {}

	void Init();

	BOOL Create(const CStringA& sTitle, int icon, int menu, RECT rect);

	// Config File Functions (StartConfig.xml)
	bool ReadStartCfgFile();
	bool WriteStartCfgFile();

	// Allow child class to read/write additional entries from/to startcfg.xml
	virtual bool AppReadStartCfgFile(const IKEPConfig& cfg) {
		return true;
	}
	virtual bool AppWriteStartCfgFile(IKEPConfig& cfg) {
		return true;
	}

	BOOL GetFeedBackReportPrompted() const {
		return m_feedBackReportPrompted;
	}
	void SetFeedBackReportPrompted(BOOL prompted) {
		m_feedBackReportPrompted = prompted;
	}

	bool CreateNewD3dDevice(bool antiAlias);

	D3D_ERROR CreateD3dDevice(BOOL fullscreen, int mode, bool antiAlias);

	virtual bool RenderLoop(bool canWait = true) = 0;

	std::string GetCursorFileName(CURSOR_TYPE type) {
		return m_cursorFileName[type].GetString();
	}
	void SetCursorFileName(CURSOR_TYPE type, const std::string& fileName) {
		m_cursorFileName[type] = fileName.c_str();
	}

	std::string GetShadowFileName() {
		return m_shadowFileName.GetString();
	}

	/** DRF
	* Gets texture memory statistics from DxDevice->Ptr() singleton.
	*/
	DxDevice::TextureMemoryStats GetTextureMemoryStats() const;

	/** DRF
	* Logs process and texture memory usage.
	*/
	void LogMemory() const;

protected:
	static LPDIRECT3D9 g_pD3D;
	static LPDIRECT3DDEVICE9 g_pD3dDevice;
	static UINT mousestate;

	static long mousex;
	static int GetMouseX() {
		return mousex;
	}

	static long mousey;
	static int GetMouseY() {
		return mousey;
	}

	virtual void OnIdle(long) {}
	virtual void HandleWindowsKeyEvent(int iVirtualKey, bool bDown) {}
	virtual void HandleWindowsMouseMoveEvent(const Vector2i& vMovement, int iButtonsPressed) {}
	virtual void HandleWindowsMouseButtonEvent(int iButton, bool bDown) {}
	virtual void HandleFocusChange(bool bFocusGain) {}
	virtual void HandleReturnFromNonclientAreaAction() {}

	void TrapCursor(bool bTrap);

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy() {}
	afx_msg void OnActivate(UINT state, CWnd* other, BOOL minimize) {}
	afx_msg void OnSize(UINT type, int cx, int cy);
	afx_msg void OnMouseMove(UINT state, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC) { return TRUE; }
	afx_msg void OnMove(int x, int y);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnSysChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSysKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnXButtonDown(UINT nFlags, UINT nButton, CPoint point);
	afx_msg void OnXButtonUp(UINT nFlags, UINT nButton, CPoint point);
	afx_msg void OnRawInput(UINT nInputCode, HRAWINPUT hRawInput);
	afx_msg void OnExitMenuLoop(BOOL bIsTrackPopupMenu);
	afx_msg void OnExitSizeMove();

	DECLARE_MESSAGE_MAP()

	friend class StartCfg;

	void EnableRawInput(bool bEnabled);
	bool IsMouseButtonPressed(int iKeyCode);
	const Vector2i& GetVirtualCursorPos() const {
		return m_xVirtualCursor;
	};

private:
	virtual bool SetD3dDevice() = 0;

	CStringA m_cursorFileName[CURSOR_TYPES];

	CStringA m_shadowFileName;

	void RegisterRawInputDevices();
	void UnregisterRawInputDevices();
	void ResetVirtualCursor();
	void MoveVirtualCursor(int x, int y);
	void UpdateMouseButtonStates();
	void SetMouseButtonState(int iKeyCode, bool bPressed, bool bSuppressEvent /* = false */);

	void OnMouseStateChanged(int iKeyCode, bool bPressed);
	void OnMouseMoved(int xRelative, int yRelative);

	static const UINT s_uMOUSE_BUTTON_COUNT = 5;
	bool m_abMouseButtonStates[s_uMOUSE_BUTTON_COUNT];
	bool m_bIsRawInputEnabled;
	Vector2i m_xVirtualCursor;
};

} // namespace KEP