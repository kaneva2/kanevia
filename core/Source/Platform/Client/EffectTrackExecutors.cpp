///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EffectTrackExecutors.h"

#include "SoundPlacement.h"
#include "CMovementObj.h"
#include "DynamicObj.h"
#include "..\Events\DynamicObjectEvents.h"
#include "Event/EventSystem/Dispatcher.h"
#include "Core/Math/Rotation.h"
#include "IClientEngine.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

void AnimationEffect::execute() {
	if (m_pMO) {
		m_pMO->setOverrideAnimationSettings(m_animGlidSpecial);
	} else if (m_pDPO) {
		AnimSettings animSettings;
		animSettings.setGlidSpecial(m_animGlidSpecial);
		m_pDPO->setPrimaryAnimationSettings(animSettings);
	}
}

void StartParticleEffect::execute() {
	m_pICE->ObjectAddParticleAsync(
		m_objType,
		m_objId,
		m_boneName,
		m_slot,
		m_particleGlid,
		m_offsetVector,
		m_forwardDirection,
		m_upDirection);
}

void StopParticleEffect::execute() {
	m_pICE->ObjectRemoveParticle(
		m_objType,
		m_objId,
		m_boneName,
		m_slot);
}

void SoundEffect::execute() {
	if (m_objType == ObjectType::MOVEMENT) {
		if (m_stop)
			m_pICE->RemoveSoundOnPlayer(m_objId, m_soundGlid);
		else
			m_pICE->PlaySoundOnPlayer(m_objId, m_soundGlid);
	} else if (m_objType == ObjectType::DYNAMIC || m_objType == ObjectType::EQUIPPABLE) {
		if (m_stop)
			m_pICE->RemoveSoundOnDynamicObject(m_objId, m_soundGlid);
		else
			m_pICE->PlaySoundOnDynamicObject(m_objId, m_soundGlid);
	} else {
		_ASSERTE(!"SoundEffect::execute() invalid type");
	}
}

void SoundEffectObject::execute() {
	auto pSP = m_pICE->SoundPlacementGet(m_id);
	if (!pSP)
		return;

	// One would expect that re-starting a sound using the effect track so synchronize it would
	// result in the sound starting at the beginning at the time speficied, which requires that
	// the source be rewound and re-started, even if it's currently playing.
	if (m_stop) {
		pSP->Stop();
	} else {
		pSP->Rewind();
		pSP->Play();
	}
}

void MoveEffect::execute() {
	IEvent* e = new MoveDynamicObjectEvent(m_objId, m_x, m_y, m_z, m_time, 0, m_time, 0, 1);
	m_dispatcher->QueueEvent(e);
}

void RotateEffect::execute() {
	// We stored angles (in degrees), but internally need vectors.
	Matrix44f rotMat;
	ConvertRotationZYX(ToRadians(Vector3f(m_x, m_y, m_z)), out(rotMat));

	Vector3f slide(1.f, 0, 0), direction(0, 0, 1.f);
	Vector3f slideReturn, directionReturn;
	slideReturn = TransformVector(rotMat, slide);
	directionReturn = TransformVector(rotMat, direction);
	IEvent* e = new RotateDynamicObjectEvent(m_objId, directionReturn.x, directionReturn.y, directionReturn.z, slideReturn.x, slideReturn.y, slideReturn.z, m_time);
	m_dispatcher->QueueEvent(e);
}

} // namespace KEP
