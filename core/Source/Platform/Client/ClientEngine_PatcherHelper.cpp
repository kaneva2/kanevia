///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "ClientEngine.h"
#include "../Patcher/PatchLib/PatchLibGlobals.h"
#include "../Patcher/PatchLib/CPatchHandler.h"
#include "../Patcher/Common/AllowAccessDlg.h"
#include "PatchReturns.h"
#include "NetworkCfg.h"
#include "WokwebHelper.h"
#include "Common/include/UnicodeMFCHelpers.h"
#include "Common/KEPUtil/CrashReporter.h"

#define MAX_VAL -3
#define NOT_SET -1

namespace KEP {

static LogInstance("Instance");

static UINT KEI_MSG_ID = ::RegisterWindowMessageW(L"KEI_PATCH_STATUS_MSG");

static HWND g_patchWnd;

class PatchState {
public:
	PatchState(HWND wnd, CWnd* parent, Dispatcher* disp, const char* url, const char* accessUrl) :
			m_dispatcher(disp), pPatchHandler(0), m_totalSize(0), m_totalFiles(0), m_totalSubSize(0), m_progress(0), m_fileProgress(0), m_initCalled(false), m_packCleared(false), m_unpackCleared(false), m_url(url), m_logger(Logger::getInstance(L"Patcher")), m_hWnd(wnd), m_lastRet(UP_ERROR), m_lastAllowedId(0), m_gameId(0), m_parent(parent), m_parentGameId(0), m_allowAccessUrl(accessUrl) {
		srand((unsigned int)fTime::TimeMs());
	}

	// create the modeless allow dialog
	void PromptAllow(LONG gameId, const char* msg) {
		m_msg = msg;
		m_gameId = gameId;
		m_lastAllowedId = rand(); // just an arbitrary number

		AfxEnableControlContainer();

		std::string allowAccessUrl = m_allowAccessUrl;
		std::string::size_type p = allowAccessUrl.find("{0}");
		char gameIdStr[64];
		_ltoa_s(m_gameId, gameIdStr, _countof(gameIdStr), 10);

		if (p != std::string::npos) {
			allowAccessUrl = allowAccessUrl.replace(p, 3, gameIdStr);
		}

		m_parent->EnableWindow(FALSE);
		auto pDlg = new CAllowAccessDlg(allowAccessUrl.c_str(), m_parent, m_hWnd, m_lastAllowedId);
		pDlg->Create(IDD_ALLOW_DLG, m_parent);
		pDlg->ShowWindow(SW_SHOW);
	}

	// clean up everything
	~PatchState() {
		if (pPatchHandler) {
			delete pPatchHandler;
			pPatchHandler = NULL;
		}

		if (m_hWnd) {
			if (m_lastRet != UP_CANCELED) {
				jsAssert(m_hWnd == g_patchWnd);
			}
			DestroyWindow(m_hWnd);
		}
		if (m_lastRet != UP_CANCELED && m_hWnd == g_patchWnd) {
			g_patchWnd = 0; // could be another patch in progress after cancel
		}
	}

	Dispatcher* m_dispatcher;
	CPatchHandler* pPatchHandler;

	LONG m_totalSize;
	LONG m_totalFiles;
	LONG m_totalSubSize;
	LONG m_progress;
	LONG m_fileProgress;

	std::string m_sTotalUpdates;
	std::string m_sTotalSize;
	std::string m_sSubSize;
	std::string m_url;
	std::string m_msg;
	std::string m_allowAccessUrl;

	bool m_initCalled;
	bool m_packCleared;
	bool m_unpackCleared;

	Logger m_logger;
	HWND m_hWnd;
	CWnd* m_parent;

	LONG m_lastRet;
	LONG m_gameId;
	LONG m_parentGameId;
	ULONG m_lastAllowedId;

	static EVENT_ID m_progressEventId;
	static EVENT_ID m_promptAllowedEventId;
};

void FormatFileProgress(std::string& dest, LONG curSize, LONG totalSize) {
	std::string sCurSizePrefix = "KB";
	std::string sTotalSizePrefix = "KB";
	double curSizeScaled = (double)curSize / 1024.0f;
	double totalSizeScaled = (double)totalSize / 1024.0f;

	if (curSizeScaled >= 1024.0f) {
		curSizeScaled = curSizeScaled / 1024.0f;
		sCurSizePrefix = "MB";
	}

	if (totalSizeScaled >= 1024.0f) {
		totalSizeScaled = totalSizeScaled / 1024.0f;
		sTotalSizePrefix = "MB";
	}

	char s[100];
	_snprintf_s(s, _countof(s), _TRUNCATE, "%.1f %s / %.1f %s", curSizeScaled, sCurSizePrefix.c_str(), totalSizeScaled, sTotalSizePrefix.c_str());
	dest = s;
}

EVENT_ID PatchState::m_progressEventId = BAD_EVENT_ID;
EVENT_ID PatchState::m_promptAllowedEventId = BAD_EVENT_ID;

// much of this logic similar to that of CProgressDlg2.cpp

LRESULT CALLBACK patchWndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp) {
	if (msg == KEI_MSG_ID) {
		// fire events for status
		PatchState* me = (PatchState*)GetWindowLongPtr(hWnd, GWL_USERDATA);

		if (PatchState::m_progressEventId == BAD_EVENT_ID) {
			PatchState::m_progressEventId = me->m_dispatcher->RegisterEvent("PatchProgressEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
		}
		if (PatchState::m_promptAllowedEventId == BAD_EVENT_ID) {
			PatchState::m_promptAllowedEventId = me->m_dispatcher->RegisterEvent("PromptAllowedEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
		}

		switch (wp) {
			case MSG_PATCH_HANDLER_COMPLETE: {
				PATCHING_COMPLETE_MSG* ppcm = (PATCHING_COMPLETE_MSG*)lp;
				IEvent* e = me->m_dispatcher->MakeEvent(PatchState::m_progressEventId);
				jsVerifyReturn(e != 0, 0);
				e->SetFilter(wp - WM_USER);
				me->m_dispatcher->QueueEvent(e);

				if ((ppcm->NewGameServer.empty() == false) && (ppcm->NewGamePort > 0)) {
					// post a message to switch to other server if GameStar
					auto pGotoPlaceEvent = new GotoPlaceEvent();
					pGotoPlaceEvent->GotoUrl(0, me->m_url.c_str(), ppcm->NewGamePort, ppcm->NewGameServer.c_str(), ppcm->GameID, ppcm->ChildGameId);
					me->m_dispatcher->ProcessOneEvent(pGotoPlaceEvent);
				} else {
					LogError("NEW STAR GAME REDIRECT -- NOT IMPL");
				}
				delete me;
			} break;

			case MSG_STATUS: {
				STATUSU* stat = (STATUSU*)lp;

				int totalPercent = 0;
				int subPercent = 0;

				if (stat->nTotalSizeComplete > 0) {
					if (me->m_totalSize) {
						totalPercent = (100 * stat->nTotalSizeComplete) / me->m_totalSize;
					} else {
						totalPercent = 0;
					}
				} else if (stat->nTotalSizeComplete == MAX_VAL) {
					totalPercent = 100;
				}

				if (stat->nTotalSizeComplete != NOT_SET) {
					me->m_progress = totalPercent;
				}

				if (stat->nTotalSizeComplete > 0) {
					FormatFileProgress(me->m_sTotalSize, stat->nTotalSizeComplete, me->m_totalSize);

					char buffer[MAX_PATH];
					if (totalPercent > 0) {
						sprintf_s(buffer, _countof(buffer), "Completed: %d%%", totalPercent);
					} else {
						sprintf_s(buffer, _countof(buffer), "Scanning downloaded files.");
					}

					me->m_sTotalUpdates = buffer;
				}

				if (stat->nSubSizeComplete != NOT_SET) {
					me->m_fileProgress = subPercent;
				}

				IEvent* e = me->m_dispatcher->MakeEvent(PatchState::m_progressEventId);
				jsVerifyReturn(e != 0, 0);
				e->SetFilter(wp - WM_USER);
				(*e->OutBuffer()) << me->m_url
								  << stat->wcsStatus
								  << me->m_sTotalUpdates
								  << me->m_sTotalSize
								  << me->m_sSubSize
								  << me->m_progress
								  << me->m_fileProgress;

				me->m_dispatcher->QueueEvent(e);
			} break;

			case MSG_INIT: {
				INIT_TOTALS* totals = (INIT_TOTALS*)lp;

				if (totals->nTotalFiles != NOT_SET) {
					me->m_totalFiles = totals->nTotalFiles;
				}

				if (totals->nTotalSize != NOT_SET) {
					if (!me->m_initCalled) {
						me->m_totalSize = totals->nTotalSize;
						me->m_initCalled = true;
					}
				}

				if (totals->nTotalSubSize != NOT_SET) {
					me->m_totalSubSize = totals->nTotalSubSize;
					me->m_fileProgress = 0;
					FormatFileProgress(me->m_sSubSize, 0, totals->nTotalSubSize);
				}

				IEvent* e = me->m_dispatcher->MakeEvent(PatchState::m_progressEventId);
				jsVerifyReturn(e != 0, 0);
				e->SetFilter(wp - WM_USER);
				(*e->OutBuffer()) << me->m_url
								  << me->m_sTotalUpdates
								  << me->m_sTotalSize
								  << me->m_sSubSize
								  << me->m_progress
								  << me->m_fileProgress;

				me->m_dispatcher->QueueEvent(e);
			} break;

			case MSG_ERROR: {
				ERROR_MSG* errMsg = (ERROR_MSG*)lp;
				IEvent* e = me->m_dispatcher->MakeEvent(PatchState::m_progressEventId);
				jsVerifyReturn(e != 0, 0);
				e->SetFilter(wp - WM_USER);
				(*e->OutBuffer()) << me->m_url << errMsg->errorCode;

				LogError("MSG_ERROR - " << errMsg->errorCode);

				me->m_dispatcher->QueueEvent(e);
				delete me;
			} break;

			case MSG_VERBOSE_ERROR: {
				ERROR_MSG_VERBOSE* errMsg = (ERROR_MSG_VERBOSE*)lp;
				if (errMsg->errorCode == RC_PROMPT_ALLOW) {
					// close the menu
					IEvent* e = me->m_dispatcher->MakeEvent(PatchState::m_progressEventId);
					jsVerifyReturn(e != 0, 0);
					e->SetFilter(MSG_ABORT - WM_USER);
					(*e->OutBuffer()) << me->m_url << (int)wp;

					me->m_dispatcher->QueueEvent(e);

					me->PromptAllow(errMsg->gameId, errMsg->msg);
				} else {
					IEvent* e = me->m_dispatcher->MakeEvent(PatchState::m_progressEventId);
					jsVerifyReturn(e != 0, 0);
					e->SetFilter(wp - WM_USER);
					(*e->OutBuffer()) << me->m_url << errMsg->errorCode << errMsg->msg;

					LogError("MSG_VERBOSE_ERROR - " << errMsg->errorCode << " " << errMsg->msg);

					me->m_dispatcher->QueueEvent(e);
					delete me;
				}

			} break;

			case MSG_GOTOAPP: {
				auto gotoMsg = (GOTOAPP_MSG*)lp;
				IEvent* e = me->m_dispatcher->MakeEvent(PatchState::m_progressEventId);
				jsVerifyReturn(e != 0, 0);
				e->SetFilter(wp - WM_USER);
				(*e->OutBuffer()) << me->m_url << gotoMsg->msg;

				me->m_dispatcher->QueueEvent(e);
			} break;

			case MSG_RECONNECT: {
				// this is fired on retries, etc.
			} break;

			case MSG_CONNECTED: {
				IEvent* e = me->m_dispatcher->MakeEvent(PatchState::m_progressEventId);
				jsVerifyReturn(e != 0, 0);
				e->SetFilter(wp - WM_USER);
				(*e->OutBuffer()) << me->m_url;
				me->m_dispatcher->QueueEvent(e);
			} break;

			case MSG_UNPACKING: {
				STATUSU* stat = (STATUSU*)lp;

				int size = stat->nSubSizeComplete;
				if (me->m_packCleared == false) {
					me->m_progress = 0;
					me->m_packCleared = true;
				}
				LONG totalsize = 0;
				if (size > 0) {
					totalsize = (100 * stat->nTotalSizeComplete) / size;
				}
				me->m_progress = totalsize;

				IEvent* e = me->m_dispatcher->MakeEvent(PatchState::m_progressEventId);
				jsVerifyReturn(e != 0, 0);
				e->SetFilter(wp - WM_USER);
				(*e->OutBuffer()) << me->m_url
								  << stat->wcsStatus
								  << me->m_sTotalUpdates
								  << me->m_sTotalSize
								  << me->m_sSubSize
								  << me->m_progress
								  << me->m_fileProgress;

				me->m_dispatcher->QueueEvent(e);
			} break;

			case MSG_UNPACKINGGZ: {
				STATUSU* stat = (STATUSU*)lp;
				int size = stat->nSubSizeComplete;
				if (me->m_unpackCleared == false) {
					me->m_progress = 0;
					me->m_unpackCleared = true;
				}

				LONG totalsize = 0;
				if (size > 0) {
					totalsize = (100 * stat->nTotalSizeComplete) / size;
				}
				me->m_progress = totalsize;

				IEvent* e = me->m_dispatcher->MakeEvent(PatchState::m_progressEventId);
				jsVerifyReturn(e != 0, 0);
				e->SetFilter(wp - WM_USER);
				(*e->OutBuffer()) << me->m_url
								  << stat->wcsStatus
								  << me->m_sTotalUpdates
								  << me->m_sTotalSize
								  << me->m_sSubSize
								  << me->m_progress
								  << me->m_fileProgress;

				me->m_dispatcher->QueueEvent(e);
			} break;

			case MSG_ABORT: {
				IEvent* e = me->m_dispatcher->MakeEvent(PatchState::m_progressEventId);
				jsVerifyReturn(e != 0, 0);
				e->SetFilter(wp - WM_USER);
				(*e->OutBuffer()) << me->m_url << (int)wp;
				me->m_dispatcher->QueueEvent(e);
			}
				delete me;
				break;

			case MSG_ALLOWED: {
				// they've confirmed that they want to goto that place
				// call validate user to set that they don't want to be prompted
				// and then re-queue the event to start it over
				if (me->m_lastAllowedId == lp) { // ids must match to make sure from us
					// call event sync
					IEvent* e = me->m_dispatcher->MakeEvent(PatchState::m_promptAllowedEventId);
					jsAssert(e != 0);
					e->SetObjectId((OBJECT_ID)me->m_gameId);
					(*e->OutBuffer()) << me->m_url;
					// should this be async? it makes web call to validateUser

					me->m_dispatcher->ProcessSynchronousEventNoRelease(e); // Keep the event
					if (e->Filter() == 0) { // result code in filter upon return
						GotoPlaceEvent* pGotoPlaceEvent = new GotoPlaceEvent();
						pGotoPlaceEvent->GotoUrl(0, me->m_url.c_str());
						me->m_dispatcher->QueueEvent(pGotoPlaceEvent);
					}
					e->Release(); // Release now
				} else {
					LogError("Mismatched last allowed ids");
				}
			}
				delete me;
				break;

			case MSG_COMPLETE:
				delete me; // called if no patching takes place to cleanup
				break;

			default:
				LogError("Unknown msg from patcher: " << wp);
				break;
		}
	}
	return DefWindowProc(hWnd, msg, wp, lp);
}

struct PatchGameThreadParams {
	PatchState*& ps;
	HWND& g_patchWnd;
	const char*& baseDir;
	const char*& email;
	const char*& password;
	const char*& url;
	LONG& gameId;
	LONG& parentGameId;
	LONG& ret;
	ClientEngine* pClientEngine;
	std::string& gameName;
	std::string& parentServer;
	LONG& parentPort;
	std::string& templatePatchDir;
	std::string& appPatchDir;

	PatchGameThreadParams(
		PatchState*& a_ps,
		HWND& a_g_patchWnd,
		const char*& a_baseDir,
		const char*& a_email,
		const char*& a_password,
		const char*& a_url,
		LONG& a_gameId,
		LONG& a_parentGameId,
		LONG& a_ret,
		std::string& a_gameName,
		std::string& a_parentServer,
		LONG& a_parentPort,
		std::string& a_templatePatchDir,
		std::string& a_appPatchDir,
		ClientEngine* a_pEngine) :
			ps(a_ps),
			g_patchWnd(a_g_patchWnd), baseDir(a_baseDir), email(a_email), password(a_password), url(a_url), gameId(a_gameId), parentGameId(a_parentGameId), ret(a_ret), gameName(a_gameName), parentServer(a_parentServer), parentPort(a_parentPort), templatePatchDir(a_templatePatchDir), appPatchDir(a_appPatchDir), pClientEngine(a_pEngine) {
	}
};

static DWORD WINAPI BackgroundPatchGame(LPVOID vThreadParm) {
	auto p = (PatchGameThreadParams*)vThreadParm;
	if (!p || !p->ps)
		return 0;
	CPatchHandler* pcph = p->ps->pPatchHandler;
	if (!pcph)
		return 0;
	ClientEngine* pce = p->pClientEngine;
	if (!pce)
		return 0;

	// Setup Login Parameters
	pce->m_login.userEmail = p->email;
	pce->m_login.userPassword = p->password;

	// Call gamelogin.aspx. If the app is hosted and server is offline, call GoToApp then poll gamelogin.aspx until server is ready or timeout.
	// We are not using the result returned from this function. Just use it to bring up the 3dapp server if necessary.
	std::string url = p->url;
	GL_RESULT glResult = GL_UNDEFINED;
	std::string resultDescription;
	std::string serverVersion;
	LONG zoneIndex;
	LONG gameId = 0;
	bool incubatorHosted = false;
	LONG parentGameId = 0;
	LONG errorCode = 0;
	if (!pce->getGameLoginInfo(
			url,
			false,
			glResult,
			resultDescription,
			p->parentServer,
			p->parentPort,
			serverVersion, // drf - added
			zoneIndex,
			gameId,
			p->gameName,
			incubatorHosted,
			parentGameId,
			errorCode,
			p->templatePatchDir,
			p->appPatchDir)) {
		LogError("GetGameLoginInfo() FAILED - '" << url << "'");

		// Trigger patch menu
		GOTOAPP_MSG status(gameId, "Contacting Server");
		SendMessage(p->g_patchWnd, KEI_MSG_ID, MSG_GOTOAPP, (LONG)&status);

		// Display error
		ERROR_MSG_VERBOSE err(gameId, glResult, resultDescription.c_str());
		SendMessage(p->g_patchWnd, KEI_MSG_ID, MSG_VERBOSE_ERROR, (LONG)&err);
		return 0;
	}

	// Do patch automatically sends all error information via messagess
	pcph->start();
	return 0;
}

bool ClientEngine::TestPatchFileForUpdate(const char* filename) {
	CStringA basedirtemp = PathBase().c_str();
	CStringA filenametemp = filename;

	CFileException exc;
	CFile fl;
	BOOL res = OpenCFile(fl, (basedirtemp + filenametemp), CFile::modeRead, &exc);
	if (res) {
		fl.Close();
		return true;
	} else {
		return false;
	}
}

// create window that patcher uses as callback then dispatch msgs to it
LONG ClientEngine::PatchGameUrl(const char* url, const char* allowAccessUrl, const char* email, const char* password, const char* baseDir, LONG gameId, LONG parentGameId, std::string& gameName, std::string& parentServer, LONG& parentPort, std::string& templatePatchDir, std::string& appPatchDir) {
	PatchState* ps = 0;

	if (g_patchWnd == 0 || !IsWindow(g_patchWnd)) {
		WNDCLASS wndclass;
		memset(&wndclass, 0, sizeof(wndclass));

		wndclass.lpfnWndProc = patchWndProc;
		wchar_t className[100];
		_snwprintf_s(className, _countof(className), _TRUNCATE, L"PatchWindowClass%d", GetCurrentProcessId());
		wndclass.lpszClassName = className;

		static ATOM ant = 0;
		if (ant == 0) {
			ant = RegisterClass(&wndclass);
		}

		// going to a new game, start patch
		CWnd* w = new CWnd();
		w->Create((const wchar_t*)ant, L"GamePatchHiddenProgress", 0, CRect(0, 0, 1, 1), AfxGetMainWnd(), 0);
		g_patchWnd = w->m_hWnd;

		if (g_patchWnd == NULL) {
			DWORD dwError = GetLastError();
			std::string msg;
			char s[1024] = "";
			sprintf_s(s, _countof(s), "Can't create hidden window: %s", errorNumToString(dwError, msg));
			m_dispatcher.LogMessage(log4cplus::WARN_LOG_LEVEL, s);
		}
		ps = new PatchState(g_patchWnd, AfxGetMainWnd(), &m_dispatcher, url, allowAccessUrl);
		SetWindowLongPtr(g_patchWnd, GWL_USERDATA, (LONG)ps);
	} else {
		// shouldn't be race condition since window is owned by this thread
		// the patchWndProc and this code will always be on same thd
		ps = (PatchState*)GetWindowLongPtr(g_patchWnd, GWL_USERDATA);
		jsAssert(ps && ps->m_hWnd == g_patchWnd);

		if (ps->m_url != url) {
			IEvent* e = new RenderTextEvent("Patch already in progress.  Please wait.", eChatType::System);
			m_dispatcher.QueueEvent(e);
			return UP_ERROR;
		} else { // same patch still going
			return ps->m_lastRet;
		}
	}
	LONG ret = UP_ERROR;
	ps->pPatchHandler = new CPatchHandler(g_patchWnd, baseDir, email, url, gameId, parentGameId, ret);
	ps->m_gameId = gameId;
	ps->m_parentGameId = parentGameId;

	// Spawn off a thread to handle patching so we can keep rendering immediately.
	{
		// Create a worker thread

		PatchGameThreadParams params(ps, g_patchWnd, baseDir, email, password, url, gameId, parentGameId, ret, gameName, parentServer, parentPort, templatePatchDir, appPatchDir, this);
		HANDLE hThread;
		DWORD dwThreadID;

		hThread = CreateThread(
			NULL, // Pointer to thread security attributes
			0, // Initial thread stack size, in bytes
			BackgroundPatchGame, // Pointer to thread function
			&params, // The argument for the new thread
			0, // Creation flags
			&dwThreadID // Pointer to returned thread identifier
		);

		// Wait for the worker to complete
		Timer timer;
		DWORD dwret = WAIT_TIMEOUT;
		while (((dwret = WaitForSingleObject(hThread, 0)) == WAIT_TIMEOUT) && (timer.ElapsedSec() < 100.0)) {
			RenderLoop();

			// Pump Messages Until Empty (10 messages max)
			if (!::MsgProcPumpUntilEmpty(10)) {
				// Quit Client
				::PostQuitMessage(0);
				return UP_ERROR;
			}

			// Give It A Chance
			fTime::SleepMs(1);
		}

		// The state of the specified object (thread) is signaled
		DWORD dwExitCode = 9999;
		bool ok = false;
		if (GetExitCodeThread(hThread, &dwExitCode) && dwExitCode == 0) {
			ok = true;
		}
		if (dwExitCode == STILL_ACTIVE) {
			int errorCode = 69;
			const char* errText = "Patching timeout. Try again.";
			TerminateThread(hThread, 1);
			// Totally send message to patch menu letting them know there was some error
			IEvent* e = m_dispatcher.MakeEvent(PatchState::m_progressEventId);
			jsVerifyReturn(e != 0, 0);
			e->SetFilter(MSG_VERBOSE_ERROR - WM_USER);
			(*e->OutBuffer()) << url << errorCode << errText;
			m_dispatcher.QueueEvent(e);
		}

		CloseHandle(hThread);
		if (!ok) {
			delete ps; //
			return UP_ERROR;
		}
	}

	// CheckAndPatchGameUrl may delete ps immediately, if g_patchWnd not 0, it's still alive
	if (g_patchWnd) {
		ps->m_lastRet = ret;
	}

	// make sure we have set
	// gameName,
	// parentServer
	// parentPort
	return ret;
}

BOOL ClientEngine::CancelPatch() {
	PatchState* ps = (PatchState*)GetWindowLongPtr(g_patchWnd, GWL_USERDATA);
	IDispatcher* disp = GetDispatcher();
	if (ps && disp)
		ps->m_lastRet = UP_CANCELED;
	return TRUE;
}

BOOL ClientEngine::promptAllowAccessAtLogin(const char* gameUrl, const char* accessUrl, LONG gameId) {
	AfxEnableControlContainer();
	BOOL res = FALSE;

	std::stringstream ssGameId;
	ssGameId << gameId;

	std::string allowAccessUrl = accessUrl;
	std::string::size_type p = allowAccessUrl.find("{0}");

	if (p != std::string::npos) {
		allowAccessUrl = allowAccessUrl.replace(p, 3, ssGameId.str());
	}

	AfxGetMainWnd()->EnableWindow(FALSE);
	CAllowAccessDlg* dlg = new CAllowAccessDlg(allowAccessUrl.c_str(), AfxGetMainWnd());
	if (dlg->DoModal() == IDOK) {
		//Send event to allow access
		// call event sync
		if (PatchState::m_promptAllowedEventId == BAD_EVENT_ID) {
			PatchState::m_promptAllowedEventId = m_dispatcher.RegisterEvent("PromptAllowedEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
		}

		IEvent* e = m_dispatcher.MakeEvent(PatchState::m_promptAllowedEventId);
		jsAssert(e != 0);
		e->SetObjectId((OBJECT_ID)gameId);
		(*e->OutBuffer()) << gameUrl;

		m_dispatcher.ProcessSynchronousEventNoRelease(e); //Event handler will call validateuser.aspx

		// result code in filter upon return
		if (e->Filter() == 0) {
			res = TRUE;
		}

		e->Release(); // Release now
	}

	return res;
}

} // namespace KEP