///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "RenderEngine/ReDynamicGeom.h"
#include "LocaleStrings.h"

namespace KEP {

static LogInstance("Instance");

BEGIN_MESSAGE_MAP(ClientEngine, ClientEngine_CFrameWnd)
ON_WM_CLOSE()
ON_WM_SIZE()
ON_WM_CREATE()
ON_WM_TIMER()
ON_WM_ACTIVATE()
ON_WM_COPYDATA()
ON_WM_NCACTIVATE()
ON_WM_QUERYENDSESSION()
ON_WM_ENDSESSION()
ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

static const wchar_t* const s_clientWindowName = K_CLIENT_WINDOW_NAME;

// Override the creation of the window's class name. Specify a specific name
// so the patcher process can accurately find the window. Vista's Aero UI creates
// temporary windows of the same title, e.g. thumbnail. This interferes with the
// patcher determing the correct kepclient window. Allow the patcher to find
// the client by class name.
BOOL ClientEngine::PreCreateWindow(CREATESTRUCT& cs) {
	// Register Window Class
	WNDCLASS wnd;
	memset(&wnd, 0, sizeof(wnd));
	HINSTANCE hInst = AfxGetInstanceHandle();
	if (!(::GetClassInfoW(hInst, s_clientWindowName, &wnd))) {
		wnd.style = CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
		wnd.lpfnWndProc = ::DefWindowProc;
		wnd.cbClsExtra = wnd.cbWndExtra = 0;
		wnd.hInstance = hInst;
		wnd.hIcon = NULL;
		wnd.hCursor = AfxGetApp()->LoadStandardCursor(IDC_ARROW);
		wnd.hbrBackground = (HBRUSH)(COLOR_3DFACE + 1);
		wnd.lpszMenuName = NULL;
		wnd.lpszClassName = s_clientWindowName;
		AfxRegisterClass(&wnd);
	}
	cs.lpszClass = s_clientWindowName;
	return CFrameWnd::PreCreateWindow(cs);
}

void ClientEngine::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) {
	// We Are Activated When Our Application Takes Focus
	bool active = (nState != WA_INACTIVE);
	m_activated = active;
	m_paused = bMinimized;

	// Log Windows Activate State
	std::string waState;
	StrBuild(waState, (active ? "ACTIVATED" : "DEACTIVATED") << (bMinimized ? " MINIMIZED" : ""));
	LogWarn(waState);

	// DRF - Set Crash Report Error Data
	CrashReporter::SetErrorData("waState", waState);

	// Don't Allow Rippers (clever ones load OnActivate)
	if (active && AppRipperDetected()) {
#if (DRF_RIPPER > 1)
		LogFatal("Ripper Detected - Shutting Down ...");
		PostMessage(WM_CLOSE);
#endif
	}

	// Call Super Class OnActivate Handler
	__super::OnActivate(nState, pWndOther, bMinimized);

	// DRF - Reset Mouse Clicks On Activated (allows filtering of first click to select)
	if (active)
		MouseClicksReset();

	// DRF - Parallels VM Bug Fix
	// If we do this within a Parallels VM the first mouse click following a click out of the client and then
	// back in the client (when the client returns in focus) remains clicked even though you are not holding
	// the mouse button down, turning everything into a drag.  If we do not do this some modifier keys like
	// SHIFT & CTRL become sticky between apps instead.  Set registry testGroup to choose your poison.
	if (!m_vmFix)
		EnableDirectInput(active);
}

BOOL ClientEngine::OnNcActivate(BOOL active) {
	return CWnd::OnNcActivate(active);
}

int ClientEngine::OnCreate(LPCREATESTRUCT lpCreateStruct) {
	m_devMode_CFrameWnd = (AppDevMode() != DEV_MODE_OFF);

	// Call super::OnCreate()
	if (ClientEngine_CFrameWnd::OnCreate(lpCreateStruct) == -1) {
		LogError("ClientEngine_CFrameWnd::OnCreate() FAILED");
		return -1;
	}

	// Startup Client Engine
	return Startup() ? 0 : -1;
}

void ClientEngine::OnClose() {
	// Flag To Close Immediately Following Startup
	if (m_startingUp) {
		LogInfo("Close During Startup - Shutdown Flagged");
		m_closeOnStartup = true;
		return;
	}

	// Shutdown Client Engine (calls ExitProcess and never returns)
	Shutdown();

	// Call super::OnClose()
	ClientEngine_CFrameWnd::OnClose();
}

void ClientEngine::OnTimer(UINT nIDEvent) {
	// Skip most messages while starting up
	if (m_startingUp)
		return;

	// Call super::OnTimer()
	ClientEngine_CFrameWnd::OnTimer(nIDEvent);
}

void ClientEngine::OnLostDevice() {
	LogInfo("START");

	SAFE_RELEASE(m_pGlowTEX[0]);
	SAFE_RELEASE(m_pGlowTEX[1]);

	m_paused = TRUE;

	if (m_shaderSystem)
		m_shaderSystem->OnLostDevice();
	if (m_pxShaderSystem)
		m_pxShaderSystem->OnLostDevice();

	if (m_dynamicGeom)
		m_dynamicGeom->OnLostDevice();

	ClientBladeFactory::Instance()->OnLostDevice();

	LogInfo("END");
}

void ClientEngine::OnResetDevice() {
	LogInfo("START");

	// Glow render targets need to be released because they are D3DPOOL_DEFAULT
	// If this doesn't happen all hell will break loose
	SAFE_RELEASE(m_pGlowTEX[0]);
	SAFE_RELEASE(m_pGlowTEX[1]);

	// Release all D3DPOOL_DEFAULT and D3D9EFFECT resources before device release
	GetReDeviceState()->InvalidateResources(RED3DX9RESOURCE_DYNAMIC | RED3DX9RESOURCE_SYSTEM);

	// attempt to reset the device
	if (IsFullScreen()) {
		if (!m_activated && !m_shutdown) {
			LogInfo("END - not resetting because not active.");
			return;
		}
		d3dpp.Windowed = FALSE;
	} else {
		d3dpp.Windowed = TRUE;
	}
	LogInfo("Windowed: " << d3dpp.Windowed);

	HRESULT hr1 = g_pD3dDevice->Reset(&d3dpp);

	// reset the device caps
	ZeroMemory(&m_devcaps, sizeof(m_devcaps));
	HRESULT hr2 = GetD3DDevice()->GetDeviceCaps(&m_devcaps);

	// Failure Expected During Minimize
	if (FAILED(hr1) || FAILED(hr2)) {
		fTime::SleepMs(25);
		OnLostDevice();
	} else {
		if (m_shaderSystem)
			m_shaderSystem->OnResetDevice();

		if (m_pxShaderSystem)
			m_pxShaderSystem->OnResetDevice();

		ClientBladeFactory::Instance()->OnResetDevice();

		if (m_dynamicGeom)
			m_dynamicGeom->OnReset();

		ResetRenderState();

		OutlinesDeviceInit();

		m_paused = FALSE;
	}

	LogInfo("END");
}

void ClientEngine::OnSize(UINT typeOnSize, int cx, int cy) {
	bool resetDevice = m_szXMonitor != cx || m_szYMonitor != cy;
	ClientEngine_CFrameWnd::OnSize(typeOnSize, cx, cy);
	ResizeClientRect(typeOnSize, cx, cy, resetDevice); // reset device if size changed
}

afx_msg BOOL ClientEngine::OnQueryEndSession() {
	LogFatal("Shutting Down ...");
	SendMessage(WM_CLOSE);
	return TRUE; // confirm end session is ok
}

afx_msg void ClientEngine::OnEndSession(BOOL ending) {
	if (ending) {
		LogFatal("Shutting Down ...");
		SendMessage(WM_CLOSE);
	}
}

// Sent from the patcher process to signal an already running client to
// change locations in support of meet me in 3D
// slash commands etc.
afx_msg BOOL ClientEngine::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) {
	CStringA str = (const char*)pCopyDataStruct->lpData;
	CStringA resToken = str; // no longer tokenize since only one param passes in, and now with kaneva:// can have spaces

	if (resToken.GetLength() > 4 && resToken.Left(4).CompareNoCase("-try") == 0) {
		int tryonglid;
		int useTypeInt = USE_TYPE_NONE;

		if (sscanf_s(resToken, "-try%d:%d", &tryonglid, &useTypeInt) == 2) {
			USE_TYPE usetype = (USE_TYPE)useTypeInt;

			// Clothing
			if (usetype == USE_TYPE_NONE ||
				usetype == USE_TYPE_EQUIP ||
				usetype == USE_TYPE_ADD_DYN_OBJ ||
				usetype == USE_TYPE_ADD_ATTACH_OBJ ||
				usetype == USE_TYPE_ANIMATION ||
				usetype == USE_TYPE_SOUND) {
				auto pMO = GetMovementObjMe();
				if (pMO) {
					if (IsIconic()) {
						ShowWindow(SW_SHOW);
					} else {
						SetForegroundWindow();
						ShowWindow(SW_SHOW);
					}

					m_tryOnOnStartup = true;
					m_tryOnGlid = tryonglid;
					m_tryonUseType = usetype;

					int parentGameId = GetParentGameId();
					if (m_zonePermissions <= ZP_OWNER && // we have admin permission for zone
						((parentGameId == 0 && GetZoneIndex().isHousing()) || parentGameId != 0) //In owner's wok home or owner's 3DApp allow tryon
					) {
						// Allow tryon of multiple items. For dynamic objects, each will
						// have their own timeout. For clothing, the tryon will expire when
						// the last item that was tried on times out.

						HandleTryOnEvent((ULONG)this, &m_dispatcher, NULL);
					} else {
						GotoPlaceEvent* e = new GotoPlaceEvent();
						if (parentGameId != 0)
							e->GotoUrl(0, StpUrl::MakeAptUrl(parentGameId, pMO->getName().c_str()).c_str());
						else
							e->GotoUrl(0, StpUrl::MakeAptUrl(GetGameId(), pMO->getName().c_str()).c_str());
						m_dispatcher.QueueEvent(e);
					}
				}
			} else {
				LogWarn("Invalid tryon use type passed in, skipping it: " << usetype);
				m_tryOnOnStartup = false;
				m_tryOnGlid = 0;
			}
		}
	} else if (resToken.GetLength() > 4 && resToken.Left(3).CompareNoCase("-gu") == 0) {
		std::ostringstream strStream;
		strStream << resToken.Mid(3);
		std::string url = strStream.str();
		StpUrl::ExtractParameters(url, m_urlParameters);

		// remove the parameters from the URL when sending it up to the server
		if (m_urlParameters.length() > 0) {
			int paramLength = m_urlParameters.length() + 1; // +1 for the question mark
			url = url.substr(0, url.length() - paramLength);
		}

		GotoPlaceEvent* e = new GotoPlaceEvent();
		e->GotoUrl(0, url.c_str());
		m_dispatcher.QueueEvent(e);
	} else if (resToken.GetLength() > 4 && resToken.Left(3).CompareNoCase("-gp") == 0) {
		// do they want to jump to a player on startup?
		int playerServerId = atoi(resToken.Mid(3));
		if (playerServerId > 0) {
			GotoPlaceEvent* e = new GotoPlaceEvent();
			e->GotoPlayer(playerServerId, 0); // char index of zero, WOK specific
			e->AddTo(SERVER_NETID);
			m_dispatcher.QueueEvent(e);
		}
	} else if (resToken.GetLength() > 4 && resToken.Left(3).CompareNoCase("-ga") == 0) {
		// do they want to jump to a player's apartment on startup?
		int playerServerId = atoi(resToken.Mid(3));
		if (playerServerId > 0) {
			GotoPlaceEvent* e = new GotoPlaceEvent();
			e->GotoApartment(playerServerId, 0); // char index of zero, WOK specific
			e->AddTo(SERVER_NETID);
			m_dispatcher.QueueEvent(e);
		}
	} else if (resToken.GetLength() > 4 && resToken.Left(3).CompareNoCase("-gz") == 0) {
		// Not supported by patcher at this time.

		// do they want to jump to an instance zone on startup?
		// format of param is -gz<zoneIndex>:<instanceId>
		long zoneIndex;
		long instanceId;
		if (sscanf_s(resToken, "-gz%d:%d", &zoneIndex, &instanceId) == 2) {
			GotoPlaceEvent* e = new GotoPlaceEvent();
			e->GotoZone(ZoneIndex(zoneIndex), instanceId, 0); // char index of zero, WOK specific
			e->AddTo(SERVER_NETID);
			m_dispatcher.QueueEvent(e);
		}
	} else if (resToken.GetLength() > 3 && resToken.Left(3).CompareNoCase("-gc") == 0) {
		// do they want to jump to a channel on startup?
		// format of param is -gc<instanceId>
		long instanceId = atol(resToken.Mid(3));

		if (instanceId > 0) {
			m_gotochannelonstartup = true;
			m_gotoonstartupId = instanceId;

			// Determine the zoneindex for this instanceId
			std::string resultText;
			std::string httpStatusDescription;
			DWORD httpStatusCode;

			std::string loginUrl(m_loginUrl);

			if (m_login.userEmail.empty()) {
				LogWarn("Skipping send to channel. Email address is empty. Maybe not logged in yet.");
				return 0;
			}
			fillInLoginUrl(loginUrl, m_login.userEmail);

			m_gotochannelonstartup = true;
			m_gotoonstartupId = instanceId;

			bool ret = _GetBrowserLogin(loginUrl, resultText, &httpStatusCode, httpStatusDescription);

			if (!ret) {
				LogError("Error in browser login request when traveling to zone: Status code=" << httpStatusCode << " Description=" << httpStatusDescription.c_str());
				m_gotochannelonstartup = false;
				m_gotoonstartupId = 0;
			} else {
				GL_RESULT glResult = GL_UNDEFINED;
				std::string resultDescription;
				LONG userId = 0;
				std::string serverip;
				LONG serverport = 0;
				std::string serverVersion; // drf - added
				LONG zoneindex = 0;
				LONG gameId = 0;
				bool incubatorHosted = false;
				LONG parentGameId = 0;
				LONG errorCode = 0;
				std::string userName;
				std::string userGender;
				std::string templatePatchDir;
				std::string appPatchDir;

				LogInfo("GameLogin.aspx call returned " << resultText);
				ret = ParseGameLoginResult(
					resultText,
					glResult,
					resultDescription,
					userId,
					userName,
					serverip,
					serverport,
					serverVersion, // drf - added
					zoneindex,
					userGender,
					gameId,
					m_gameName,
					incubatorHosted,
					parentGameId,
					errorCode,
					templatePatchDir,
					appPatchDir);
				if (!ret) {
					m_gotochannelonstartup = false;
					m_gotoonstartupId = 0;

					if (glResult == GL_OK) { // parsed ok, but no servers
						LogError("ParseGameLoginResult() FAILED - No Servers Available");
					} else {
						LogError("ParseGameLoginResult() FAILED - '" << resultDescription << "' glResult=" << glResult);
					}
					return 0;
				}

				m_login.userName = m_loginName = userName;
				m_login.userGender = m_loginGender = userGender;

				LONG replyZoneIndex = zoneindex;

				// Set New Game Id
				SetGameId(gameId);
				SetParentGameId(parentGameId);

				if (replyZoneIndex != 0) {
					// if can't go to channel for whatever reason, will get back our own apartment
					// instead, need to check type of zone index
					ZoneIndex tempZi(replyZoneIndex);
					GotoPlaceEvent* e = new GotoPlaceEvent();
					if (tempZi.isChannel()) {
						ZoneIndex zoneIndex(replyZoneIndex, instanceId, CI_CHANNEL);
						e->GotoZone(zoneIndex, instanceId, 0); // char index of zero, WOK specific
						e->AddTo(SERVER_NETID);
						m_dispatcher.QueueEvent(e);
					} else {
						// tell them they can't goto that place
						m_dispatcher.QueueEvent(new RenderLocaleTextEvent(LS_SPAWN_BAD, eChatType::System));
						LogWarn("Skipping send to channel since got different zone type.  For instanceId  " << instanceId << " got zone index of " << numToString(replyZoneIndex));

						//TODO: provide more error detail to the user
					}
				} else {
					LogWarn("Skipping send to channel " << m_gotoonstartupId << " since got zoneIndex of zero from web");
					m_gotochannelonstartup = false;
					m_gotoonstartupId = 0;

					//TODO: provide more error detail to the user
				}
			}
		} else {
			// if <= 0, assume permanent zone index
			ZoneIndex zoneIndex(-instanceId, 0, CI_PERMANENT);
			instanceId = 0;
			GotoPlaceEvent* e = new GotoPlaceEvent();
			e->GotoZone(zoneIndex, instanceId, 0); // char index of zero, WOK specific
			e->AddTo(SERVER_NETID);
			m_dispatcher.QueueEvent(e);
		}
	}

	return 0;
}

void ClientEngine::OnLButtonDown(UINT nFlags, CPoint point) {
	// Try to recover from a device lost if most recent Reset call failed
	if (!m_inView && m_hrTCL == D3DERR_DEVICENOTRESET) {
		// D3D device was lost and is ready to reset
		// Left button click event on client window will trigger a device reset
		m_hrTCL = D3DERR_DEVICELOST; // Update last hr to trigger a device reset (if next hr is still D3DERR_DEVICENOTRESET)
	}

	ClientEngine_CFrameWnd::OnLButtonDown(nFlags, point);
}

} // namespace KEP