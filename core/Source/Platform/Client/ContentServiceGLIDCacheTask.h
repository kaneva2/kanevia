///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "TaskObject.h"
#include "Event\EventSystem\Dispatcher.h"
#include "UgcConstants.h"

namespace KEP {

class ContentServiceGLIDCacheTask : public TaskObject {
public:
	ContentServiceGLIDCacheTask(Dispatcher& dispatcher, const std::vector<GLID>& glids) :
			m_dispatcher(dispatcher),
			m_glidVector(glids),
			m_pEventCompletion(NULL) {}

	virtual void ExecuteTask() override;

	Dispatcher& m_dispatcher;
	std::vector<GLID> m_glidVector;
	IEvent* m_pEventCompletion;
};

} // namespace KEP