///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "InventoryWeb.h"

namespace KEP {

InventoryWeb::InventoryWeb() {
	m_texture_coords.left = 0;
	m_texture_coords.right = 0;
	m_texture_coords.top = 0;
	m_texture_coords.bottom = 0;
}

InventoryWeb::~InventoryWeb() {
	for (std::list<InventoryWeb_Pass*>::iterator jj = m_passes.begin(); jj != m_passes.end();) {
		delete (*jj);
		jj = m_passes.erase(jj);
	}
}

} // namespace KEP