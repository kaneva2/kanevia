///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "DynamicPlacementManager.h"
#include "DynamicObj.h"
#include "Common\include\CMemSizeGadget.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

struct DynamicPlacementManager::HelperFunctions {
	static void RemoveFromList(
		DynamicPlacementManager* pDPM,
		CDynamicPlacementObj* pDPO,
		DynObjSubListId iSubListId) {
		if (pDPO->m_PlacementManagerState.m_iDynObjIndex == SIZE_MAX)
			return; // Object not in list.

		size_t* pIndex = &(pDPO->m_PlacementManagerState.m_SubListIndex[iSubListId]);
		size_t iOldIndex = *pIndex;

		if (iOldIndex == SIZE_MAX)
			return; // Nothing to do.

		std::vector<CDynamicPlacementObj*>* pArray = &pDPM->m_apDynamicObjectSubLists[iSubListId];

		// Remove from list.
		CDynamicPlacementObj* pMovedDPO = pArray->back();

		// Swap with back() element
		std::swap((*pArray)[iOldIndex], pArray->back());

		// Old back() element's index needs to be updated.
		pMovedDPO->m_PlacementManagerState.m_SubListIndex[iSubListId] = iOldIndex;

		// Delete
		pArray->pop_back();
		*pIndex = SIZE_MAX;
	}

	static void ApplyChange(
		DynamicPlacementManager* pDPM,
		CDynamicPlacementObj* pDPO,
		size_t iSubListId) {
		size_t* pIndex = &pDPO->m_PlacementManagerState.m_SubListIndex[iSubListId];
		std::vector<CDynamicPlacementObj*>* pArray = &pDPM->m_apDynamicObjectSubLists[iSubListId];

		size_t iOldIndex = *pIndex;
		if (pDPO->m_PlacementManagerState.m_iDynObjIndex != SIZE_MAX && pDPO->ShouldBeInSublist(static_cast<DynObjSubListId>(iSubListId))) {
			// Needs to be in list.

			if (iOldIndex != SIZE_MAX) {
				assert(iOldIndex < pArray->size() && (*pArray)[iOldIndex] == pDPO); // Verify it's where it thinks it is.
				return; // Nothing to do.
			}

			// Add to list
			*pIndex = pArray->size();
			pArray->push_back(pDPO);
		} else {
			// Should not be in list.

			if (iOldIndex == SIZE_MAX)
				return; // Nothing to do.

			// Remove from list.
			// Swap with back() element
			std::swap((*pArray)[iOldIndex], pArray->back());

			// Old back() element's index needs to be updated.
			(*pArray)[iOldIndex]->m_PlacementManagerState.m_SubListIndex[iSubListId] = iOldIndex;

			// Delete
			pArray->pop_back();
			*pIndex = SIZE_MAX;
		}
	}
};

DynamicPlacementManager::DynamicPlacementManager() {
}

DynamicPlacementManager::~DynamicPlacementManager() {
}

void DynamicPlacementManager::Log(bool verbose) {
	LogInfo("objects=" << GetObjectCount() << " ...");
	if (!verbose)
		return;
	for (CDynamicPlacementObj* pDPO : m_apDynamicObjects) {
		if (!pDPO)
			continue;
		_LogInfo(" ... " << pDPO->ToStr());
	}

	// ED-8333 - Invisible Object Memory Impact
	size_t objVis, bytesVis, objInv, bytesInv;
	GetObjectMemoryUsage(objVis, bytesVis, objInv, bytesInv);
	LogWarn(FMT_FLT << "objVis=" << objVis << " (" << (bytesVis / BYTES_PER_MB) << "mb) objInv=" << objInv << " (" << (bytesInv / BYTES_PER_MB) << "mb)");
}

// ED-8333 - Invisible Object Memory Impact
void DynamicPlacementManager::GetObjectMemoryUsage(size_t& objVis, size_t& bytesVis, size_t& objInv, size_t& bytesInv) const {
	objVis = objInv = bytesVis = bytesInv = 0;
	IMemSizeGadget* pMemSizeGadget = new CMemSizeGadget;
	for (CDynamicPlacementObj* pDPO : m_apDynamicObjects) {
		if (!pDPO)
			continue;
		if (!pDPO->IsVisible())
			continue;
		++objVis;
		pDPO->GetMemSize(pMemSizeGadget);
	}
	bytesVis = pMemSizeGadget->GetTotalSizeInBytes();
	for (CDynamicPlacementObj* pDPO : m_apDynamicObjects) {
		if (!pDPO)
			continue;
		if (pDPO->IsVisible())
			continue;
		++objInv;
		pDPO->GetMemSize(pMemSizeGadget);
	}
	bytesInv = pMemSizeGadget->GetTotalSizeInBytes() - bytesVis;
	pMemSizeGadget->Release();
}

CDynamicPlacementObj* DynamicPlacementManager::Add(std::unique_ptr<CDynamicPlacementObj> pDPO) {
	auto objId = pDPO->GetPlacementId();
	auto ib = m_mapDynamicObjects.insert(std::make_pair(objId, std::move(pDPO)));
	if (!ib.second) {
		LogError("Object Already Exists - objId=" << objId);
		return nullptr;
	}
	CDynamicPlacementObj* pInsertedDPO = ib.first->second.get(); // pDPO is now invalidated
	size_t iIndex = m_apDynamicObjects.size();
	m_apDynamicObjects.push_back(pInsertedDPO);
	pInsertedDPO->m_PlacementManagerState.m_iDynObjIndex = iIndex;

	for (size_t i = 0; i < NumberOfDynObjSubListIds; ++i)
		HelperFunctions::ApplyChange(this, pInsertedDPO, i);

	return pInsertedDPO;
}

bool DynamicPlacementManager::Remove(CDynamicPlacementObj* pDPO) {
	if (!pDPO)
		return false;

	size_t idx = pDPO->m_PlacementManagerState.m_iDynObjIndex;
	size_t nDynObj = m_apDynamicObjects.size();
	if (idx >= nDynObj || m_apDynamicObjects[idx] != pDPO)
		return false;

	// Assuming placement ID never changes.
	auto objId = pDPO->GetPlacementId();
	auto itr = m_mapDynamicObjects.find(objId);
	if (itr == m_mapDynamicObjects.end()) {
		LogError("Object Not Found - objId=" << objId);
		return false;
	}

	// Remove from sublists. Do before removal from main list because of sanity check that object is in the main list.
	for (size_t i = 0; i < DynObjSubListId::NumberOfDynObjSubListIds; ++i) {
		HelperFunctions::RemoveFromList(this, pDPO, static_cast<DynObjSubListId>(i));
	}

	// Swap deleted object with last element in array, updating last element's index.
	// Note that pDPO may be the last element, so order the operations so it will still work in this case.
	std::swap(m_apDynamicObjects[idx], m_apDynamicObjects[nDynObj - 1]);
	m_apDynamicObjects[idx]->m_PlacementManagerState.m_iDynObjIndex = idx;
	pDPO->m_PlacementManagerState.m_iDynObjIndex = SIZE_MAX;
	m_apDynamicObjects.pop_back();

	// Remove object from map, deleting it.
	m_mapDynamicObjects.erase(itr);

	return true;
}

// Remove everything. Used when rezoning.
void DynamicPlacementManager::RemoveAll() {
	for (CDynamicPlacementObj* pDPO : m_apDynamicObjects)
		pDPO->m_PlacementManagerState.m_iDynObjIndex = SIZE_MAX;
	m_mapDynamicObjects.clear();
	m_apDynamicObjects.clear();
	for (size_t i = 0; i < DynObjSubListId::NumberOfDynObjSubListIds; ++i) {
		m_apDynamicObjectSubLists[i].clear();
	}
}

// Remove objects but do not delete them. For quick exits.
void DynamicPlacementManager::ReleaseAll() {
	for (auto& mapEntry : m_mapDynamicObjects) {
		mapEntry.second.release();
	}
	RemoveAll();
}

CDynamicPlacementObj* DynamicPlacementManager::LookUpByPlacementId(int id) const {
	auto itr = m_mapDynamicObjects.find(id);
	if (itr == m_mapDynamicObjects.end())
		return nullptr;
	return itr->second.get();
}

CDynamicPlacementObj* DynamicPlacementManager::LookUpByIndex(size_t idx) const {
	if (idx < m_apDynamicObjects.size())
		return m_apDynamicObjects[idx];
	return nullptr;
}

void DynamicPlacementManager::UpdateCustomMaterials(bool chainLoad) {
	if (chainLoad) {
		LogInfo("CHAIN LOADING - numDOs=" << m_apDynamicObjects.size());
	}
	for (CDynamicPlacementObj* pDPO : m_apDynamicObjects)
		if (pDPO)
			pDPO->UpdateCustomMaterials(chainLoad);
}

void DynamicPlacementManager::UpdateObjectInSubList(CDynamicPlacementObj* pDPO, size_t iSubListId) {
	if (iSubListId >= NumberOfDynObjSubListIds) {
		LogError("Invalid sublist ID");
		return;
	}

	if (iSubListId == DynObjSubListId_NeedsUpdate) {
		if (m_bUpdating)
			m_aPendingChangesToUpdateList.push_back(pDPO);
		else
			ApplyUpdateChange(pDPO);
	} else {
		HelperFunctions::ApplyChange(this, pDPO, iSubListId);
	}
}

void DynamicPlacementManager::ApplyPendingUpdateChanges() {
	for (CDynamicPlacementObj* pDPO : m_aPendingChangesToUpdateList)
		ApplyUpdateChange(pDPO);
	m_aPendingChangesToUpdateList.clear();
}

void DynamicPlacementManager::ApplyUpdateChange(CDynamicPlacementObj* pDPO) {
	HelperFunctions::ApplyChange(this, pDPO, DynObjSubListId_NeedsUpdate);
}

} // namespace KEP
