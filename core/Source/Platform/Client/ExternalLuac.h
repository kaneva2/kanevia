#pragma once


class ExternalLuac {
public:
	static std::string CallExternalLuaCompiler(std::string const& compilerPath, std::string const& sourceLuaFile, bool deleteCompiled = false);
};