///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ClientEngine.h"

#include "CMultiplayerClass.h"
#include "KEPDiag.h"
#include "Event/Events/TextEvent.h"
#include "Event/Events/GetAttributeEvent.h"
#include "Event/Events/TryOnInventoryEvent.h"
#include "ActionEvent.h"
#include "d3dx9math.h"
#include "Engine/ClientBlades/iclientbladefactory.h"
#include "Engine/ClientBlades/IMenuBlade.h"
#include "Engine/ClientBlades/imenugui.h"
#include "RenderEngine/ReDynamicGeom.h"
#include "Core/Math/Rotation.h"
#include "Core/Math/TransformUtil.h"
#include "KEPPhysics/Vehicle.h"
#include "InputSystem.h"
#include "KeyCodeMap.h"
#include "KEPPhysics/PhysicsFactory.h"
#include "KEPPhysics/Universe.h"
#include "KEPPhysics/Vehicle.h"
#include "KEPPhysics/Avatar.h"
#include "KEPPhysics/RigidBodyStatic.h"
#include "Common/KEPUtil/FrameTimeProfiler.h"
#include "ReMesh.h"
#include "RuntimeSkeleton.h"
#include "CPhysicsEmuClass.h"

namespace KEP {

static LogInstance("ClientEngine");

static const int Chat_MenuID = 605; // ID of chat menu
static const int Group_Chat_MenuID = 606; // ID of group chat menu
static const int Clan_Chat_MenuID = 607; // ID of clan chat menu
static const int Cursor_Access_MenuID = 1009; // ID of menu used just to display the cursor

bool ClientEngine::IsMouseLookEnabled() {
	return m_mouseState.ButtonDownRight();
}

bool ClientEngine::IsCameraAutoFollowEnabled() {
	return !m_mouseState.ButtonDownLeft() && !m_mouseState.ButtonDownRight();
}

void ClientEngine::ControlsInLoop() {
	if (m_keyboardState.Ctrl() && m_keyboardState.KeyDown(DIK_Q)) {
		m_closeDownCall = true;
		return;
	}
}

bool ClientEngine::DetectKeyStateChange(ControlInfoObj* infoPtr, bool detectPressed) {
	bool prevState = false;
	bool currentState = false;

	if (infoPtr->m_controlType == CO_TYPE_KEYBOARD) {
		prevState = m_keyboardStateWas.KeyDown(infoPtr->m_buttonDown);
		currentState = m_keyboardState.KeyDown(infoPtr->m_buttonDown);
	} else if (infoPtr->m_controlType == CO_TYPE_MOUSE) {
		// DRF - TODO - Mouse Only Has 4 Buttons!!!
		// Since this code was written there was a change from using DIMOUSESTATE2 (8 buttons) to DIMOUSESTATE (4 buttons)
		// check first five buttons (last two "buttons" are really mouse wheel related")
		if (infoPtr->m_buttonDown < 5) {
			prevState = m_mouseStateWas.ButtonDown(infoPtr->m_buttonDown);
			currentState = m_mouseState.ButtonDown(infoPtr->m_buttonDown);
		}
	}

	// Detect State Change
	bool stateChange = false;
	if (detectPressed)
		stateChange = (!prevState && currentState);
	else
		stateChange = (prevState && !currentState);

	// Ignore Keyboard State Changes If Key Already Captured
	bool isKeyboard = (infoPtr->m_controlType == CO_TYPE_KEYBOARD);
	if (stateChange && isKeyboard && m_keyCaptured) {
		LogDebug("keyCaptured - Keyboard State Change Ignored");
		stateChange = false;
	}

	return stateChange;
}

void ClientEngine::HandleWindowsKeyEvent(int iVirtualKey, bool bDown) {
	LONG nTicks = GetMessageTime();
	double dTime = TickCountToRealTime(nTicks);
	m_pInputSystem->EnqueueKeyStateChange(dTime, iVirtualKey, bDown);
}

void ClientEngine::HandleWindowsMouseMoveEvent(const Vector2i& vMovement, int iButtonsPressed) {
	if (iButtonsPressed == 0)
		return;

	LONG nTicks = GetMessageTime();
	double dTime = TickCountToRealTime(nTicks);
	if (iButtonsPressed & 0x01)
		m_pInputSystem->EnqueueMouseButtonStateChange(dTime, VK_LBUTTON, true, vMovement);

	if (iButtonsPressed & 0x02)
		m_pInputSystem->EnqueueMouseButtonStateChange(dTime, VK_RBUTTON, true, vMovement);

	if (iButtonsPressed & 0x04)
		m_pInputSystem->EnqueueMouseButtonStateChange(dTime, VK_MBUTTON, true, vMovement);

	if (iButtonsPressed & 0x08)
		m_pInputSystem->EnqueueMouseButtonStateChange(dTime, VK_XBUTTON1, true, vMovement);

	if (iButtonsPressed & 0x10)
		m_pInputSystem->EnqueueMouseButtonStateChange(dTime, VK_XBUTTON2, true, vMovement);
}

void ClientEngine::HandleWindowsMouseButtonEvent(int iButton, bool bDown) {
	LONG nTicks = GetMessageTime();
	double dTime = TickCountToRealTime(nTicks);
	m_pInputSystem->EnqueueMouseButtonStateChange(dTime, iButton, bDown, Vector2i(0, 0));
}

void ClientEngine::HandleFocusChange(bool bFocusGain) {
	LogInfo("ClientEngine::HandleFocusChange FocusGain: " << bFocusGain);
	LONG nTicks = GetMessageTime();
	double dTime = TickCountToRealTime(nTicks);
	if (bFocusGain) {
		BYTE aKeys[256];
		if (GetKeyboardState(aKeys)) {
			std::vector<int> aDepressedKeys;
			for (int i = 0; i < 256; ++i) {
				if (aKeys[i] & 0x80) {
					// Return key is not handled correctly. There appears to be no way to determine whether the return on the main
					// keyboard area or the number pad is pressed. Tried to use DirectInput to query the state because it has different
					// codes for each key, but it did not return correct results. DirectInput is always reporting the keys as in the UP position.
					// It appears as if DirectInput does not actually detect the current key state when switching to the application.
					// Keys are assumed up unless they go down while the app has focus (or if a key repeat event occurs).

					aDepressedKeys.push_back(i);
				}
			}
			m_pInputSystem->ResetKeys(dTime);
			m_pInputSystem->UpdateKeyStates(aDepressedKeys, dTime);
		}
	} else {
		m_pInputSystem->CancelActiveHandlers(dTime);
	}
}

void ClientEngine::HandleReturnFromNonclientAreaAction() {
	LONG nTicks = GetMessageTime();
	double dTime = TickCountToRealTime(nTicks);
	m_pInputSystem->ResetKeys(dTime);
}

// #MLB_PaperDoll - Paper doll interface to Copy World Stats
void ClientEngine::CopyStatsToClipboard() {
	auto CopyToClipBoard = [](const char* psz) {
		if (!::OpenClipboard(NULL))
			return false;

		bool bSucceeded = false;
		::EmptyClipboard();
		std::wstring strw = Utf8ToUtf16(psz);
		size_t nChar = strw.size();
		HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, (nChar + 1) * sizeof(wchar_t));
		if (hGlobal) {
			wchar_t* pwszText = static_cast<wchar_t*>(GlobalLock(hGlobal));
			if (pwszText) {
				std::copy(strw.c_str(), strw.c_str() + nChar, pwszText);
				pwszText[nChar] = 0;
				GlobalUnlock(hGlobal);
				bSucceeded = true;
			}
			SetClipboardData(CF_UNICODETEXT, hGlobal);
			GlobalFree(hGlobal);
		}
		::CloseClipboard();
		return bSucceeded;
	};

	std::ostringstream strm;
	GetGeometryStats(strm);
	GetPaperDollMemoryUsage(strm);
	// #MLB_BulletProfile - Profiling
	m_pUniverse->DumpProfile(strm);
	CopyToClipBoard(strm.str().c_str());
}

void ClientEngine::GetGeometryStats(std::ostream& strm) {
	struct VisualData {
		VisualData(StreamableDynamicObjectVisual* pVisual, size_t iVisual) {
			m_iVisual = iVisual;
			if (!pVisual)
				return;
			m_strName = pVisual->getName();
			m_bIsVisible = pVisual->getIsVisible();
			m_bCanCollide = pVisual->getCanCollide();
			ReMesh* pMesh = pVisual->getReMesh(0);
			if (pMesh) {
				ReMeshData* pMeshData;
				pMesh->Access(&pMeshData);
				assert(pMeshData->PrimType == ReMeshPrimType::TRISTRIP || pMeshData->PrimType == ReMeshPrimType::TRILIST);
				m_nTriangles = pMeshData->PrimType == ReMeshPrimType::TRISTRIP ? std::max<size_t>(2, pMeshData->NumI) - 2 : pMeshData->NumI / 3;
				m_nVertices = pMeshData->NumV;
			}
		}
		size_t m_iVisual = 0;
		std::string m_strName;
		bool m_bIsVisible = false;
		bool m_bCanCollide = false;
		size_t m_nTriangles = 0;
		size_t m_nVertices = 0;
	};
	struct DynObjData {
		DynObjData(StreamableDynamicObject* pDynObj, GLID glid) {
			Init(pDynObj, glid);
		}
		void Init(StreamableDynamicObject* pDynObj, GLID glid) {
			size_t nVisuals = pDynObj->getVisualCount();
			for (size_t i = 0; i < nVisuals; ++i) {
				m_aVisualData.emplace_back(VisualData(pDynObj->Visual(i), i));
			}
			m_strName = pDynObj->getName();
			m_glid = glid;
		}
		size_t m_nInstances = 0;
		GLID m_glid = 0;
		std::string m_strName;
		std::vector<VisualData> m_aVisualData;
		size_t m_nPerInstanceMeshes = 0;
		size_t m_nPerInstanceTriangles = 0;
		size_t m_nPerInstanceVertices = 0;

		size_t m_nCombinedMeshes = 0;
		size_t m_nCombinedTriangles = 0;
		size_t m_nCombinedVertices = 0;
	};

	struct ZoneData {
		size_t m_nDynObj = 0;
		size_t m_nVertices = 0;
		size_t m_nTriangles = 0;
		size_t m_nDynObjMeshes = 0;

		size_t m_nUniqueVerts = 0;
		size_t m_nUniqueTriangles = 0;
		size_t m_nUniqueDynObjMeshes = 0;
		size_t m_nTotalPlacements = 0;
		std::map<StreamableDynamicObject*, DynObjData> m_mapDynObj;

		void GetStats(ClientEngine* pClientEngine) {
			std::function<bool(CDynamicPlacementObj*)> ProcessDynObj = [this](CDynamicPlacementObj* pDynObj) -> bool {
				if (!pDynObj)
					return true;
				StreamableDynamicObject* pStreamableDynObj = pDynObj->GetBaseObj();
				GLID glid = pDynObj->GetGlobalId();
				auto itr = m_mapDynObj.find(pStreamableDynObj);
				if (itr == m_mapDynObj.end()) {
					itr = m_mapDynObj.emplace(std::piecewise_construct, std::forward_as_tuple(pStreamableDynObj), std::forward_as_tuple(pStreamableDynObj, glid)).first;
				}
				++itr->second.m_nInstances;
				return true;
			};
			pClientEngine->IterateObjects(ProcessDynObj);

			for (auto& pr : m_mapDynObj) {
				DynObjData& dynObjData = pr.second;
				for (VisualData& visData : dynObjData.m_aVisualData) {
					dynObjData.m_nPerInstanceMeshes += 1;
					dynObjData.m_nPerInstanceTriangles += visData.m_nTriangles;
					dynObjData.m_nPerInstanceVertices += visData.m_nVertices;
				}
				dynObjData.m_nCombinedMeshes = dynObjData.m_nInstances * dynObjData.m_nPerInstanceMeshes;
				dynObjData.m_nCombinedTriangles = dynObjData.m_nInstances * dynObjData.m_nPerInstanceTriangles;
				dynObjData.m_nCombinedVertices = dynObjData.m_nInstances * dynObjData.m_nPerInstanceVertices;
				m_nUniqueDynObjMeshes += dynObjData.m_nPerInstanceMeshes;
				m_nUniqueTriangles += dynObjData.m_nPerInstanceTriangles;
				m_nUniqueVerts += dynObjData.m_nPerInstanceVertices;
				m_nDynObjMeshes += dynObjData.m_nCombinedMeshes;
				m_nTriangles += dynObjData.m_nCombinedTriangles;
				m_nVertices += dynObjData.m_nCombinedVertices;

				m_nTotalPlacements += dynObjData.m_nInstances;
			}
			m_nDynObj = m_mapDynObj.size();
		}
		void PrintStats(std::ostream& strm) {
			strm << "Total dynamic objects: " << m_nDynObj << "\n";
			strm << "Total placements: " << m_nTotalPlacements << "\n";
			strm << "Total meshes: " << m_nDynObjMeshes << "\n";
			strm << "Total triangles: " << m_nTriangles << "\n";
			strm << "Total vertices: " << m_nVertices << "\n";
			strm << "Unique meshes: " << m_nUniqueDynObjMeshes << "\n";
			strm << "Unique triangles: " << m_nUniqueTriangles << "\n";
			strm << "Unique vertices: " << m_nUniqueVerts << "\n";

			std::vector<DynObjData*> apDynObjData;
			for (auto& pr : m_mapDynObj)
				apDynObjData.push_back(&pr.second);

			auto PrintTop = [&strm, &apDynObjData](size_t(DynObjData::*pMember), const char* pszElementDesc) {
				size_t nTopElem = std::min<size_t>(20, apDynObjData.size());
				std::partial_sort(apDynObjData.begin(), apDynObjData.begin() + nTopElem, apDynObjData.end(), [pMember](DynObjData* left, DynObjData* right) {
					return left->*pMember > right->*pMember;
				});
				strm << "\nTop " << nTopElem << " dynamic objects with the most " << pszElementDesc << ":\n";
				for (size_t i = 0; i < nTopElem; ++i) {
					const DynObjData& dynObjData = *apDynObjData[i];
					strm.width(2);
					strm << i + 1;
					strm.width(0);
					strm << " \"" << dynObjData.m_strName << "\" glid=" << dynObjData.m_glid << " " << pszElementDesc << ": " << dynObjData.*pMember << "\n";
				}
			};
			PrintTop(&DynObjData::m_nPerInstanceTriangles, "individual triangles");
			PrintTop(&DynObjData::m_nCombinedTriangles, "combined triangles");
			PrintTop(&DynObjData::m_nCombinedMeshes, "meshes");
			PrintTop(&DynObjData::m_nInstances, "instances");

			std::sort(apDynObjData.begin(), apDynObjData.end(), [](DynObjData* left, DynObjData* right) {
				return strcmpi(left->m_strName.c_str(), right->m_strName.c_str()) < 0;
			});
			strm << "\nDynamic Objects\n";
			for (const DynObjData* pDynObjData : apDynObjData) {
				const DynObjData& dynObjData = *pDynObjData;
				size_t nInstances = dynObjData.m_nInstances;

				strm << "Dynamic Object: \"" << dynObjData.m_strName << "\" glid=" << dynObjData.m_glid << "\n";
				strm << " Instances: " << nInstances << "\n";
				strm << " Per instance: Triangles: " << dynObjData.m_nPerInstanceTriangles << "  vertices: " << dynObjData.m_nPerInstanceVertices << "\n";
				strm << " All instances: Triangles: " << dynObjData.m_nCombinedTriangles << "  vertices: " << dynObjData.m_nCombinedVertices << "\n";
				size_t nVisuals = dynObjData.m_aVisualData.size();
				strm << " Number of visuals: " << nVisuals << "\n";
				for (const VisualData& visualData : dynObjData.m_aVisualData) {
					strm << " Visual " << visualData.m_iVisual << " \"" << visualData.m_strName << "\"\n";
					strm << "  IsVisible: " << (int)visualData.m_bIsVisible << "  CanCollide: " << (int)visualData.m_bCanCollide << "\n";
					strm << "  Triangles: " << visualData.m_nTriangles << " Vertices: " << visualData.m_nVertices << "\n";
				}
			}
		}
	};
	ZoneData zoneData;
	zoneData.GetStats(this);
	zoneData.PrintStats(strm);
}

void ClientEngine::UserControlUse(CMovementObj* pMO, int controlConfigNum) {
	if (!pMO)
		return;

	auto pMC = pMO->getMovementCaps();
	if (!pMC)
		return;

	const CFrameObj* pFO = pMO->getBaseFrame();
	if (!pFO)
		return;

	TimeMs timeMs = fTime::TimeMs();

	//	Update camera axis rotation for this entity
	//	Update the axis to use for camera rotation
	//	Get x axis vector of the player - if we're in cameraRotate mode, save off this
	CCameraObj* pCO = GetActiveCameraForMovementObject(pMO);
	if (pCO && !(pCO->m_inCameraRotMode)) {
		pCO->m_RotAxisForTarget.x = pFO->m_frameMatrix(0, 0);
		pCO->m_RotAxisForTarget.y = pFO->m_frameMatrix(0, 1);
		pCO->m_RotAxisForTarget.z = pFO->m_frameMatrix(0, 2);
	}

	if (pMC->GetSurfaceProp() == ObjectType::WORLD)
		pMC->SetGravityEnabled(FALSE);

	m_respawning = FALSE;
	m_respawnTimeStamp = timeMs;

	unsigned int movementRestriction = getMovementRestriction();

	// Loading zone: disable gravity and movements but allow turning or looking
	if (m_DOSpawnMonitor->IsSpawning() || IsZoneLoadingUIActive())
		pMC->SetGravityEnabled(FALSE);

	POSITION posLoc = 0;

	if (m_controlsList)
		posLoc = m_controlsList->FindIndex(controlConfigNum);

	if (!posLoc)
		return;

	ControlDBObj* pControlDB = (ControlDBObj*)m_controlsList->GetAt(posLoc);

	CMovementObj* pMO_posses = pMO->getPosses() ? pMO->getPosses() : pMO;

	auto pMC_posses = pMO_posses->getMovementCaps();
	if (!pMC_posses)
		return;

	const CFrameObj* pFO_posses = pMO_posses->getBaseFrame();
	if (!pFO_posses)
		return;

	// look for attached path finder
	IPathFinder* pPathFinder = GetPathFinder(ObjectType::MOVEMENT, pMO_posses->getNetTraceId());
	if (pPathFinder && pPathFinder->GetState() == IPathFinder::ENROUTE) {
		if (!pPathFinder->AllowPlayerControl())
			movementRestriction = RestrictMovement_All;
	}

	// Get External Positional Forces (dead code - TriggerObject::TA_ACTIVATE_FORCE_PAD)
	Vector3f posForceExt = pMO_posses->getPosForceExt();
	pMO_posses->setPosForceExt(Vector3f(0.0f, 0.0f, 0.0f)); // external force is an impulse

	// Note these mouse coordinates are assumed to be in DirectInput relative mode (they are deltas)
	float mouseControlXLoc = (m_mouseHiResX * 3.0f);
	float mouseControlYLoc = (m_mouseHiResY * 3.0f);

	if (!pControlDB->m_invertMouse)
		mouseControlYLoc = -mouseControlYLoc;

	bool restrictMouseLook = ((movementRestriction & RestrictMovement_MouseLook) != 0);
	if (restrictMouseLook || m_pitchLock)
		mouseControlXLoc = mouseControlYLoc = 0.0f;

	// Take Screenshot On F8 Key Down
	if (m_keyboardStateWas.KeyUp(DIK_F8) && m_keyboardState.KeyDown(DIK_F8)) {
		RECT rect = { -1, -1, -1, -1 }; // full screen
		ScreenshotCapture(rect);
	}

	pMC_posses->SetUseStatesToFalse();

	// Perform Mouse Look ?
	if (!restrictMouseLook) {
		float additionalDrag = 1.0f - pMC_posses->GetRotDrag();
		float drag = pMC_posses->GetRotGroundDrag() - additionalDrag;

		if (pMC_posses->IsGravityEnabled())
			drag = pMC_posses->GetRotDrag();

		pMC_posses->SetTurning(false);
	}

	// All Movement Restricted ?
	bool restrictAllMovement = (movementRestriction == RestrictMovement_All);
	if (!restrictAllMovement) {
		// Perform Movement Controls
		for (POSITION posLoc2 = pControlDB->m_controlInfoList->GetHeadPosition(); posLoc2 != NULL;) {
			auto pCIO = (ControlInfoObj*)pControlDB->m_controlInfoList->GetNext(posLoc2);
			if (!pCIO)
				continue;

			// Keyboard, Mouse or Joystick input
			ControlObjActionState act_state = CO_ACTION_STATE_UP;
			if (GetMouseKeyboardUpdate()) {
				if (DetectKeyStateChange(pCIO, true)) {
					if (!m_keyCaptured)
						act_state = CO_ACTION_STATE_PRESSED;
				} else if (DetectKeyStateChange(pCIO, false)) {
					if (!m_keyCaptured)
						act_state = CO_ACTION_STATE_RELEASED;
				} else if (pCIO->m_controlType == CO_TYPE_MOUSE && m_mouseState.ButtonDown(pCIO->m_buttonDown)) {
					act_state = CO_ACTION_STATE_DOWN;
				} else if ((pCIO->m_controlType == CO_TYPE_MOUSE) && (((m_mouseState.DeltaWheel() > 0) && (pCIO->m_buttonDown == 5)))) {
					// drf - mouse has only 4 buttons !!!
					act_state = CO_ACTION_STATE_DOWN;
				} else if ((m_mouseState.DeltaWheel() < 0) && (pCIO->m_buttonDown == 6)) {
					// drf - mouse has only 4 buttons !!!
					act_state = CO_ACTION_STATE_DOWN;
				} else if ((pCIO->m_controlType == CO_TYPE_KEYBOARD) && m_keyboardState.KeyDown(pCIO->m_buttonDown)) {
					if (!m_keyCaptured)
						act_state = CO_ACTION_STATE_DOWN;
				}
			}

			if (act_state != CO_ACTION_STATE_UP) {
				switch (pCIO->m_cmdAction) {
					case eCommandAction::ForceJet:
						// DEPRECATED
						break;

					default: {
						// create a new ActionEvent and fire it synchronously
						IEvent* e = new ActionEvent(act_state, pMO, pCIO, timeMs);
						m_dispatcher.ProcessSynchronousEvent(e);
						break;
					}
				}
			}
		}
	}

	if (pMC_posses->IsAutoRun())
		pMC_posses->DoMoveForward();

	// Path finder update (call GetPathFinder again in case it has canceled by player interaction)
	pPathFinder = GetPathFinder(ObjectType::MOVEMENT, pMO_posses->getNetTraceId());
	if (pPathFinder && pPathFinder->GetState() == IPathFinder::ENROUTE) {
		int newState = pPathFinder->Update();
		bool bRemove = false;
		switch (newState) {
			case IPathFinder::ARRIVED:
				bRemove = true;
				break;

			case IPathFinder::CANCELED:
				bRemove = true;
				break;

			case IPathFinder::IDLE:
				bRemove = true;
				break;
		}

		if (bRemove)
			DetachPathFinder(ObjectType::MOVEMENT, pMO_posses->getNetTraceId(), false, IPathFinder::NONE);
	}

	// Stop Movement While Not Being Controlled
	if (!pMC_posses->IsTurning())
		pMC_posses->DoTurnDecel(false);
	if (!pMC_posses->IsMoving())
		pMC_posses->DoMoveDecel();
	if (!pMC_posses->IsSideStepping())
		pMC_posses->DoSideStepDecel();

	// Get External Rotational Forces
	Vector3f rotForceExt(0.0f, 0.0f, 0.0f);
	if (!restrictAllMovement) {
		// Get Yaw Change
		if (pMC_posses->GetMaxYaw() != 0.0f) {
			auto curRot = pMC_posses->GetRotForce();
			if (pControlDB->m_controlType == CO_TYPE_MOUSE || pMO_posses->isFirstPersonModel()) {
				if (pMC_posses->IsTurning())
					rotForceExt.y += curRot.y;
			} else {
				rotForceExt.y = curRot.y;
			}
		}
	}

	if (pMC_posses->IsStandVerticalToSurfaceEnabled())
		pMO_posses->standVerticalToSurface();

	CDynamicPlacementObj* pDPO_friction = nullptr;
	RuntimeSkeleton* pSkeleton_friction = nullptr;

	// Get External Positional Changes (eg. moving platform)
	Vector3f posDeltaExt(0.0f, 0.0f, 0.0f);

	// Break friction contact when flying
	if (!pMC_posses->IsCollisionEnabled()) {
		pMO_posses->m_recentSurfaceInfo.objectType = ObjectType::NONE;
	} else if (pMO_posses->m_recentSurfaceInfo.objectType == ObjectType::DYNAMIC) {
		CDynamicPlacementObj* pDPO_surface = GetDynamicObject(pMO_posses->m_recentSurfaceInfo.objectId);

		// Disable the IsMoving check so avatar always follows the platform.
		// We used to have problems at the end of an animation. IsMoving is false but the server does a manual reposition
		// of the platform to ensure it's exactly where it ought to be. The avatar would not follow this reposition and would
		// eventually get thrown off the platform after enough back-and-forth cycles.
		if (pDPO_surface && pDPO_surface->m_frictionEnabled) {
			pDPO_friction = pDPO_surface;

			pSkeleton_friction = pDPO_friction->getSkeleton();
			if (!pSkeleton_friction)
				return;

			const Matrix44f& currLocation = pSkeleton_friction->getWorldMatrix();
			const Matrix44f& relativePosition = pMO_posses->m_recentSurfaceInfo.relativeTransform;
			const Matrix44f& mtxDO = pSkeleton_friction->getWorldMatrix();

			Matrix44f mtxNew = relativePosition * mtxDO;
			posDeltaExt = mtxNew.GetTranslation() - pMO_posses->getLastPosition();

			std::vector<const Physics::Collidable*> apIgnore;
			apIgnore.push_back(pDPO_friction->GetRigidBody().get());
			float fRelativeMovement;
			if (pMO_posses->getPhysicsAvatar()->SweepTest(
					m_pUniverse.get(),
					FeetToMeters(pMO_posses->getLastPosition()),
					FeetToMeters(mtxNew.GetTranslation()),
					&apIgnore,
					out(fRelativeMovement))) {
				// Collision.
				posDeltaExt *= fRelativeMovement;
			}

			const Matrix44f& prevLocation = pMO_posses->m_recentSurfaceInfo.transform;
			Matrix44f inversePrev = InverseOf(prevLocation);
			Matrix44f deltaMatrix = inversePrev * currLocation;

			// Apply rotation.
			// If deltaMatrix is only rotation and translation, we could just zero out the bottom row,
			// and use the 3x3 rotation part here. However, if there is a scale involved, we need to
			// clean it up.
			Matrix44f deltaRotation;
			ToCoordSysZY(deltaMatrix.GetRowZ(), deltaMatrix.GetRowY(), out(deltaRotation));
			pMO_posses->transformBaseFrameCascade(deltaRotation);
		}
	}

	// Disable Player Physics While P2P Animating
	if (P2pIsActive(pMO_posses))
		return;

	// Update Player Physics
	m_oldPhysics->MovPhysicsSystem(
		pMO_posses,
		posForceExt, // drf - dead code - always 0
		rotForceExt,
		posDeltaExt);

	if (!pSkeleton_friction)
		return;

	// Update relative position of avatar on surface based on movement calculation.
	const Matrix44f& mtxFrictionObj = pSkeleton_friction->getWorldMatrix();
	const Matrix44f& mtxAvatar = pFO_posses->m_frameMatrix;
	pMO_posses->m_recentSurfaceInfo.relativeTransform = mtxAvatar * InverseOf(mtxFrictionObj);

	// As far as collision is concerned, m_lastPosition is moved by ExternalTranslation because we already did a collision test for moving with the physics object.
	pMO_posses->setLastPosition(pMO_posses->getLastPosition() + posDeltaExt);
}

void ClientEngine::ProcessAction(ControlObjActionState const& act_state, CMovementObj* pMO, ControlInfoObj* pControlInfoObj, TimeMs timeMs) {
	if (!pMO)
		return;

	auto pMC = pMO->getMovementCaps();
	if (!pMC)
		return;

	auto pCO = GetActiveCameraForMovementObject(pMO);

	auto pMO_posses = pMO->getPosses() ? pMO->getPosses() : pMO;

	auto pMC_posses = pMO_posses->getMovementCaps();
	if (!pMC_posses)
		return;

	bool mouseDownLeft = m_mouseState.ButtonDownLeft();
	bool mouseDownRight = m_mouseState.ButtonDownRight();

	// DRF - ED-7194 - Improved P2P Animation Sync
	bool p2pEnd = false;

	eCommandAction cmdAction = pControlInfoObj->m_cmdAction;
	switch (cmdAction) {
		// DEPRECATED
		case eCommandAction::Reload:
		case eCommandAction::Shoot1:
		case eCommandAction::Dismount:
		case eCommandAction::Respawn:
		case eCommandAction::ForceJet:
		case eCommandAction::LookUp:
		case eCommandAction::LookDown:
		case eCommandAction::LookRight:
		case eCommandAction::LookLeft:
		case eCommandAction::RollLeft:
		case eCommandAction::RollRight:
		case eCommandAction::YokeDown:
		case eCommandAction::YokeUp:
			break;

		case eCommandAction::AutoRun: {
			ProcessActionAutoRun(pMO_posses, timeMs);
		} break;

		case eCommandAction::Accelerate: {
			ProcessActionAccelerate(pMO_posses);
			p2pEnd = true;
		} break;

		case eCommandAction::Decelerate: {
			ProcessActionDecelerate(pMO_posses);
			p2pEnd = true;
		} break;

		case eCommandAction::SidestepRight: {
			ProcessActionRightSidestep(pMO_posses);
			p2pEnd = true;
		} break;

		case eCommandAction::SidestepLeft: {
			ProcessActionLeftSidestep(pMO_posses);
			p2pEnd = true;
		} break;

		case eCommandAction::TurnRight: {
			if (IsMouseLookEnabled() && GetMLookSwap())
				ProcessActionRightSidestep(pMO_posses);
			else
				ProcessActionRightTurn(pMO_posses);
			p2pEnd = true;
		} break;

		case eCommandAction::TurnLeft: {
			if (IsMouseLookEnabled() && GetMLookSwap())
				ProcessActionLeftSidestep(pMO_posses);
			else
				ProcessActionLeftTurn(pMO_posses);
			p2pEnd = true;
		} break;

		case eCommandAction::Jump: {
			if (getMovementRestriction() & RestrictMovement_Jump)
				return;
			if (!pMC_posses->DoJump())
				return;
			DetachPathFinder(ObjectType::MOVEMENT, pMO_posses->getNetTraceId(), true, IPathFinder::CANCEL_BY_USER);
			p2pEnd = true;
		} break;

		case eCommandAction::ToggleRun: {
			if ((timeMs - m_durationToServerStamp) <= 750)
				return;
			pMC_posses->ToggleWalking();
			m_durationToServerStamp = timeMs;
		} break;

		case eCommandAction::CameraRotate: {
			if (getMovementRestriction() & RestrictMovement_Turn)
				return;

			if (!pCO)
				return;

			// DRF - Don't Do This If Menu Or Widget Is Handling The Event
			if (m_menuEventOccured || m_widgetEventOccured)
				return;

			// DRF - Only Do This While Left But Not Right Drag Is In Progress
			if (!MouseDragInProgress(MOUSE_BUTTON_LEFT) || MouseDragInProgress(MOUSE_BUTTON_RIGHT))
				return;

			// Orbit Camera Adjust Azimuth & Elevation
			long xAxisDiff = m_mouseState.DeltaX();
			long yAxisDiff = m_mouseState.DeltaY();
			if (GetInvertY())
				yAxisDiff = -yAxisDiff;
			if (pCO->m_floatingCamParentFrame) {
				double mouseSens = 0.01 * m_cameraMoveSpeed;
				double dx = mouseSens * -xAxisDiff;
				double dy = mouseSens * yAxisDiff;
				pCO->AdjustAzimuth((float)dx);
				pCO->AdjustElevation((float)dy);
			}
		} break;

		case eCommandAction::CameraCharRotate: {
			if (getMovementRestriction() & RestrictMovement_Turn)
				return;

			if (!pMO_posses || !pMC_posses || !pCO)
				return;

			// DRF - Don't Do This If Menu Or Widget Is Handling The Event
			if (m_menuEventOccured || m_widgetEventOccured)
				return;

			// Are We Allowed To Turn ?
			if ((getMovementRestriction() & RestrictMovement_Turn) != 0)
				return;

			// DRF - Moved From Below To Allow Run Without Beginning Right Drag
			// Run If Both Left And Right Buttons Pressed
			if (mouseDownLeft && mouseDownRight)
				pMC_posses->DoMoveForward();

			// DRF - Only Do This While Mouse Is In Progress
			if (!MouseDragInProgress())
				return;

			pMO_posses->clearLookAt();

			//	Look at mouse states, if the user is dragging the mouse, we need to rotate
			long xAxisDiff = m_mouseState.DeltaX();
			long yAxisDiff = m_mouseState.DeltaY();

			if (GetInvertY())
				yAxisDiff = -yAxisDiff;

			// Only adjust the camera if the user drags, not if they just click
			bool mouseMove = (xAxisDiff != 0 || yAxisDiff != 0);
			if (mouseMove || mouseDownLeft) {
				// set the camera to be directly behind player
				pCO->m_orbitAzimuth = 0.0f;
				double mouseSens = 0.01 * m_cameraMoveSpeed;
				double mouseX = mouseSens * xAxisDiff;
				double mouseY = mouseSens * yAxisDiff;
				pCO->AdjustElevation((float)mouseY);
				pCO->OrientToViewDir(pMO_posses, (float)mouseX);
			}
		} break;

		case eCommandAction::NextCamera: { // Toggle First-Person Mode
			if (!m_cameraLock && DetectKeyStateChange(pControlInfoObj, true)) {
				float camToggleDist = GetCameraToggleDist();
				if (camToggleDist) {
					pCO->SetDist(camToggleDist);
					SetCameraToggleDist(0);
				} else {
					SetCameraToggleDist(pCO->GetDist());
					pCO->SetDist(0);
				}
			}
		} break;

		case eCommandAction::ZoomOut:
		case eCommandAction::ZoomIn: {
			if (!pCO || m_menuEventOccured)
				return;

			// ED-7677 - Mouse wheel shouldn't zoom while mouse is outside the window
			if (!m_cursorInViewportWas)
				return;

			// Adjust Camera Zoom From Mouse Wheel Delta
			double incr = m_keyboardState.Shift() ? -1.0 : -10.0;
			double steps = m_cameraZoomSpeed * ((incr * (double)m_mouseState.DeltaWheel()) / WHEEL_DELTA);

			// Debounce
			if (m_vmFix) {
				static double lastSteps = 0.0;
				static Timer timer;
				TimeMs elapsedTimeMs = timer.ElapsedMs();
				timer.Reset();
				bool bounce = (elapsedTimeMs < 100) && (((lastSteps < 0) && (steps > 0)) || ((lastSteps > 0) && (steps < 0)));
				if (bounce)
					steps = -steps;
				lastSteps = steps;
			}

			// Adjust Camera Distance
			pCO->AdjustDist((float)steps);

			// Update Camera Toggle Distance
			SetCameraToggleDist(0);
		} break;

		case eCommandAction::OpenMenu: {
			MenuOpen((int)pControlInfoObj->m_miscFloat1);
		} break;

		case eCommandAction::StopAll: {
			pMC_posses->ClearPosForce();
		} break;

		case eCommandAction::AnimCrouch:
		case eCommandAction::AnimSit:
		case eCommandAction::AnimTaunt1:
		case eCommandAction::AnimTaunt2:
		case eCommandAction::AnimTaunt3:
		case eCommandAction::AnimDance1:
		case eCommandAction::AnimDance2:
		case eCommandAction::AnimDance3:
		case eCommandAction::AnimWave:
		case eCommandAction::AnimSalute:
		case eCommandAction::AnimBow:
		case eCommandAction::AnimLaugh: {
			eAnimType animType;
			if (cmdAction == eCommandAction::AnimSit)
				animType = eAnimType::Sit;
			else if (cmdAction == eCommandAction::AnimCrouch)
				animType = eAnimType::Crouch;
			else
				animType = (eAnimType)((int)cmdAction - 17);
			pMO_posses->setOverrideAnimType(animType);
			pMO_posses->setOverrideDuration(0);
			pMO_posses->setOverrideTimeMs(fTime::TimeMs());
			p2pEnd = true;
		} break;

		case eCommandAction::ClanChat: {
			if (m_multiplayerObj->m_groupChatEnabled == FALSE && m_multiplayerObj->m_chatEnabled == FALSE) {
				m_multiplayerObj->m_clanChatEnabled = TRUE;
				m_pCABD3DLIB->TakingKeyStrokes = FALSE;
				MenuOpen(Clan_Chat_MenuID);
			}
		} break;

		case eCommandAction::ToggleCursor: {
			if (DetectKeyStateChange(pControlInfoObj, true))
				MenuOpen(Cursor_Access_MenuID);
		} break;

		case eCommandAction::ToggleMPitchLock: {
			if (DetectKeyStateChange(pControlInfoObj, true))
				m_pitchLock = !m_pitchLock; // lock completely
		} break;

		case eCommandAction::OpenMainMenu:
		case eCommandAction::OpenInventoryMenu: {
			if (DetectKeyStateChange(pControlInfoObj, true))
				MenuOpen((int)pControlInfoObj->m_miscFloat1);
		} break;

		case eCommandAction::Chat: {
			if (m_multiplayerObj->m_groupChatEnabled == FALSE && m_multiplayerObj->m_clanChatEnabled == FALSE) {
				m_multiplayerObj->m_chatEnabled = TRUE;
				m_pCABD3DLIB->TakingKeyStrokes = FALSE;
				MenuOpen(Chat_MenuID);
			}
		} break;

		case eCommandAction::SendServerCommand: {
			if ((timeMs - m_controlTimeStamp) > 1000) {
				ULONG commandId;
				const char* remainder = nullptr;
				m_textCommands.GetCommandId(pControlInfoObj->m_miscString, commandId, remainder);
				m_dispatcher.QueueEvent(new TextEvent(remainder, static_cast<SlashCommand>(commandId), pMO_posses->getNetTraceId(), eChatType::Talk));
				m_controlTimeStamp = timeMs;
			}
		} break;

		case eCommandAction::UseItem: {
			UseItemByGLID((GLID)pControlInfoObj->m_miscFloat1);
		} break;

		case eCommandAction::GroupChat: {
			if (m_multiplayerObj->m_chatEnabled == FALSE && m_multiplayerObj->m_clanChatEnabled == FALSE) {
				m_multiplayerObj->m_groupChatEnabled = TRUE;
				m_pCABD3DLIB->TakingKeyStrokes = FALSE;
				MenuOpen(Group_Chat_MenuID);
			}
		} break;

		case eCommandAction::UseSkill: {
			if ((timeMs - m_durationToServerStamp) > m_durationToServer) {
				m_durationToServerStamp = timeMs;

				if (ClientIsEnabled()) {
					ATTRIB_DATA attribData;
					attribData.aeType = eAttribEventType::UseSkill;
					attribData.aeShort1 = pMO_posses->getNetTraceId();
					attribData.aeShort2 = (short)pControlInfoObj->m_miscFloat3;
					attribData.aeInt1 = (int)pControlInfoObj->m_miscFloat1;
					attribData.aeInt2 = (int)pControlInfoObj->m_miscFloat2;
					m_multiplayerObj->QueueMsgAttribToServer(attribData);
				}
			}
		} break;

		case eCommandAction::EquipItem: {
			GLID glid = (GLID)pControlInfoObj->m_miscFloat1;
			ArmInventoryItem_MO(glid, pMO_posses);
		} break;

		case eCommandAction::Hide: {
			if ((timeMs - m_durationToServerStamp) > m_durationToServer) {
				m_durationToServerStamp = timeMs;

				ATTRIB_DATA attribData;
				attribData.aeType = eAttribEventType::UseSkill;
				attribData.aeShort1 = pMO_posses->getNetTraceId();
				attribData.aeShort2 = 3;
				attribData.aeInt1 = -1;
				attribData.aeInt2 = -1;
				m_multiplayerObj->QueueMsgAttribToServer(attribData);
			}
		} break;

		case eCommandAction::Reveal: {
			if ((timeMs - m_durationToServerStamp) > m_durationToServer) {
				m_durationToServerStamp = timeMs;

				ATTRIB_DATA attribData;
				attribData.aeType = eAttribEventType::UseSkill;
				attribData.aeShort1 = pMO_posses->getNetTraceId();
				attribData.aeShort2 = 11;
				attribData.aeInt1 = -1;
				attribData.aeInt2 = -1;
				m_multiplayerObj->QueueMsgAttribToServer(attribData);
			}
		} break;
	}

	// DRF - ED-7194 - Improved P2P Animation Sync
	// End P2P Animations On Move
	if (p2pEnd && P2pIsActive())
		P2pAnimStateEnd();
}

static const int TARGET_ENTITY_INFO_EVT_FILTER = 1;
static const int TARGET_POSITION_ONLY_EVT_FILTER = 2;

void GetPickRayFromPoint(
	int in_x, int in_y,
	IClientEngine* in_engine,
	Vector3f& out_p1, Vector3f& out_p2) {
	Matrix44f proj;
	in_engine->GetProjectionMatrix(&proj);

	Matrix44f view;
	in_engine->GetViewMatrix(&view);

	Matrix44f view_i;
	view_i.MakeInverseOf(view);

	D3DVIEWPORT9 viewport;
	in_engine->GetD3DDevice()->GetViewport(&viewport);

	float x = (float)in_x / (viewport.Width / 2.0f) - 1.0f;
	float y = 1.0f - (float)in_y / (viewport.Height / 2.0f);

	float dx = (1.0f / proj(0, 0)) * x;
	float dy = (1.0f / proj(1, 1)) * y;

	real hither = 0.01f;
	real yon = 10.0f;

	Vector4f p1(dx * hither, dy * hither, hither, 1.0f);
	Vector4f p2(dx * yon, dy * yon, yon, 1.0f);

	p1 = TransformVector(view_i, p1);
	p2 = TransformVector(view_i, p2);

	out_p1.x = p1.x;
	out_p1.y = p1.y;
	out_p1.z = p1.z;

	out_p2.x = p2.x;
	out_p2.y = p2.y;
	out_p2.z = p2.z;
}

void ClientEngine::PossesCallback() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "PossesCallback");

	for (POSITION pos = m_movObjList->GetHeadPosition(); pos;) {
		auto pMO = (CMovementObj*)m_movObjList->GetNext(pos);
		if (!pMO)
			continue;

		auto pMO_posses = pMO->getPosses();
		if (!pMO_posses)
			continue;

		auto pRS = pMO->getSkeleton();
		if (!pRS)
			continue;

		auto pFrame = pRS->getBoneFrameByIndex(0);
		if (!pFrame)
			continue;

		auto pRS_posses = pMO_posses->getSkeleton();
		if (!pRS_posses)
			continue;

		auto pFrame_posses = pRS_posses->getBoneFrameByIndex(pMO_posses->getRidableBoneIndex());
		if (!pFrame_posses)
			continue;

		Vector3f up, dir, baseBonePos;
		pFrame->GetPosition(baseBonePos);
		pMO_posses->getBaseFrame()->GetOrientation(dir, up);

		Matrix44f matrixAp = pRS_posses->getBoneMatrixByIndex(pMO_posses->getRidableBoneIndex());
		matrixAp = matrixAp * pMO_posses->getBaseFrameMatrix();

		pMO->setBaseFramePosition(matrixAp.GetTranslation(), true);
		pMO->setBaseFrameOrientationCascade(dir, up);

		Matrix44f nowMatrix = pMO->getBaseFrameMatrix();
		Matrix44f locOffset;
		locOffset.MakeTranslation(Vector3f(0.0f, -((baseBonePos.y / 3.0f) * 2.0f), 0.0f));
		nowMatrix = locOffset * nowMatrix;
		pMO->setBaseFrameMatrix(nowMatrix, true);
	}
}

bool ClientEngine::ProcessActionAutoRun(CMovementObj* pMO, TimeMs timeMs) {
	if (!pMO)
		return false;

	auto pMC = pMO->getMovementCaps();
	if (pMC) {
		if ((timeMs - m_durationToServerStamp) > 750) {
			pMC->ToggleAutoRun();
			m_durationToServerStamp = timeMs;
		}
	}

	auto pCO = GetActiveCameraForMovementObject(pMO);
	if (pCO && pCO->ObjectAttachIsMe()) {
		pCO->m_inCameraRotMode = false;
	}

	return true;
}

bool ClientEngine::ProcessActionAccelerate(CMovementObj* pMO) {
	if (!pMO || ((getMovementRestriction() & RestrictMovement_Forward) != 0))
		return false;

	DetachPathFinder(ObjectType::MOVEMENT, pMO->getNetTraceId(), true, IPathFinder::CANCEL_BY_USER);
	auto pMC = pMO->getMovementCaps();
	if (pMC) {
		pMC->DoMoveForward();
		pMC->SetAutoRun(FALSE);
	}

	auto pCO = GetActiveCameraForMovementObject(pMO);
	if (pCO && pCO->ObjectAttachIsMe()) {
		pCO->m_inCameraRotMode = false;
		if (IsCameraAutoFollowEnabled())
			pCO->AutoFollow();
	}

	return true;
}

bool ClientEngine::ProcessActionDecelerate(CMovementObj* pMO) {
	if (!pMO || ((getMovementRestriction() & RestrictMovement_Backward) != 0))
		return false;

	DetachPathFinder(ObjectType::MOVEMENT, pMO->getNetTraceId(), true, IPathFinder::CANCEL_BY_USER);
	auto pMC = pMO->getMovementCaps();
	if (pMC) {
		pMC->DoMoveBackward();
		pMC->SetAutoRun(FALSE);
	}

	auto pCO = GetActiveCameraForMovementObject(pMO);
	if (pCO && pCO->ObjectAttachIsMe()) {
		pCO->m_inCameraRotMode = false;
		if (IsCameraAutoFollowEnabled())
			pCO->AutoFollow();
	}

	return true;
}

bool ClientEngine::ProcessActionRightTurn(CMovementObj* pMO) {
	if (!pMO || ((getMovementRestriction() & RestrictMovement_Turn) != 0))
		return false;

	pMO->clearLookAt();
	auto pMC = pMO->getMovementCaps();
	if (pMC)
		pMC->DoTurnRight(false);

	auto pCO = GetActiveCameraForMovementObject(pMO);
	if (pCO && pCO->ObjectAttachIsMe()) {
		if (IsCameraAutoFollowEnabled())
			pCO->AutoFollow();
	}

	return true;
}

bool ClientEngine::ProcessActionLeftTurn(CMovementObj* pMO) {
	if (!pMO || ((getMovementRestriction() & RestrictMovement_Turn) != 0))
		return false;

	pMO->clearLookAt();
	auto pMC = pMO->getMovementCaps();
	if (pMC)
		pMC->DoTurnLeft(false);

	auto pCO = GetActiveCameraForMovementObject(pMO);
	if (pCO && pCO->ObjectAttachIsMe()) {
		if (IsCameraAutoFollowEnabled())
			pCO->AutoFollow();
	}

	return true;
}

bool ClientEngine::ProcessActionRightSidestep(CMovementObj* pMO) {
	if (!pMO || ((getMovementRestriction() & RestrictMovement_Side) != 0))
		return false;

	DetachPathFinder(ObjectType::MOVEMENT, pMO->getNetTraceId(), true, IPathFinder::CANCEL_BY_USER);
	auto pMC = pMO->getMovementCaps();
	if (pMC)
		pMC->DoSideStepRight();

	auto pCO = GetActiveCameraForMovementObject(pMO);
	if (pCO && pCO->ObjectAttachIsMe()) {
		pCO->m_inCameraRotMode = false;
		if (IsCameraAutoFollowEnabled())
			pCO->AutoFollow();
	}

	return true;
}

bool ClientEngine::ProcessActionLeftSidestep(CMovementObj* pMO) {
	if (!pMO || ((getMovementRestriction() & RestrictMovement_Side) != 0))
		return false;

	DetachPathFinder(ObjectType::MOVEMENT, pMO->getNetTraceId(), true, IPathFinder::CANCEL_BY_USER);
	auto pMC = pMO->getMovementCaps();
	if (pMC)
		pMC->DoSideStepLeft();

	auto pCO = GetActiveCameraForMovementObject(pMO);
	if (pCO && pCO->ObjectAttachIsMe()) {
		pCO->m_inCameraRotMode = false;
		if (IsCameraAutoFollowEnabled())
			pCO->AutoFollow();
	}

	return true;
}

} // namespace KEP