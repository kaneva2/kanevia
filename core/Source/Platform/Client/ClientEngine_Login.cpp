///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "client\LoadingScreenRAIIHelper.h"
#include "client\WebCallTask.h"
#include "common\include\UrlParser.h"
#include "common\KEPUtil\Application.h"
#include "NetworkCfg.h"

namespace KEP {

static LogInstance("Instance");

EVENT_PROC_RC ClientEngine::HandleLogonResponseEvent(const char* connectionErrorCode, HRESULT hResultCode, ULONG characterCount, LOGON_RET replyCode) {
	LogInfo("hResultCode = " << hResultCode << ", characterCount = " << characterCount << ", replyCode = " << replyCode);

	// shouldn't send anything until login ok, don't start tamper thd until logon ok
	m_originalCharacterCount = characterCount;

	// are we reconnecting? or going to player/apt off command line?
	if (m_reconnectEvent != 0 && characterCount > 0) {
		if (m_multiplayerObj->m_currentCharacterInUse < 0)
			m_multiplayerObj->m_currentCharacterInUse = 0; // WOK specific, set the current player to 0th

		// send the reconnect event
		m_dispatcher.QueueEvent(m_reconnectEvent);
		m_reconnectEvent = 0; // dispatcher owns it now

		return EVENT_PROC_RC::CONSUMED; // don't let menu system handle it
	}

	if (replyCode != LR_OK) {
		//Re-Logon will send us back to the parent so clean up.
		if (m_reconnectEvent) {
			m_reconnectEvent->Delete();
			m_reconnectEvent = 0;
		}

		if (GetParentGameId() != 0) {
			// send to parent
			IEvent* rte = new RenderTextEvent("This World is no longer available", eChatType::System);
			m_dispatcher.QueueEvent(rte);
			GotoPlaceEvent* gpe = new GotoPlaceEvent();
			gpe->GotoUrl(0, StpUrl::MakeUrlPrefix(GetParentGameId()).c_str());
			LogInfo("Sending to parent since got disconnected from child");
			m_dispatcher.QueueEvent(gpe);
		} else {
			SetParentGameId(0);

			// Previous login counter
			auto prevLoginCount = m_loginCount;

			// Open up the logon menu
			MenuOpen("LoginMenu.xml");

			// Check if a new login attempt has been initiated or completed
			if (m_loginCount > prevLoginCount) {
				// LoginMenu must has called Login() automatically. There is no need to pass the failed LogonResponseEvent
				// to the menu under such circumstances because 1) it's not useful as the situation has changed by the new
				// login attempt; 2) the event handler for LogonResponseEvent in the login menu would display the error
				// even if the new login attempt is already in progress.
				// Consume the event
				LogInfo("New logon in progress - discard old LogonResponseEvent");
				return EVENT_PROC_RC::CONSUMED;
			}
		}
	}

	return EVENT_PROC_RC::OK;
}

void ClientEngine::SaveAutoLogonCredentials() {
	std::string errormessage;
	IKEPConfig* pconf = OpenConfig("WOKConfig.xml", "WOKConfig", errormessage);
	if (!pconf) {
		LogError(errormessage.c_str());
		return;
	}

	bool AutoLogon = false;
	pconf->GetValue("AutoLogon", AutoLogon, true);
	if (!AutoLogon)
		return;

	std::string pwEnc = KEP_Encrypt(m_login.userPassword);
	pconf->SetValue("LastLogonName", m_login.userEmail.c_str(), false);
	pconf->SetValue("LastLogonPassword", pwEnc.c_str(), false);
	pconf->Save(); // ED-4809 - drf - bug fix
}

bool ClientEngine::WebValidateUser() {
#if (NET_AUTH_WEBCALL_DISABLED == 1)
	LogWarn("NET_AUTH_WEBCALL_DISABLED");
#else
	DWORD httpStatusCode;
	std::string responseStr;

	// Build 'validateUser' WebCall (KANEVA_AUTH)
	std::string authUrl = m_authUrl;
	if (!fillInAuthUrl(authUrl)) {
		LogError("fillInAuthUrl() FAILED");
		return false;
	}

	// Set Web Call Options (secure + response + hide url for jeff)
	WebCallOpt wco;
	wco.url = authUrl;
	wco.secure = true;
	wco.response = true;
#ifndef _DEBUG
	wco.verbose &= ~WC_VERBOSE_URL; // jeff's request
#endif

	// Get Web Caller Id From WebCallTask
	std::string wcId = WebCallTask::GetWebCallerId();
	if (wcId.empty())
		return false;

	// Blocking Web Call (without the auth token result none of the rest of the game webcalls work)
	DWORD errorCode;
	bool ok = WebCaller::WebCallAndWaitOk(wcId, wco, responseStr, errorCode, httpStatusCode);
	if (!ok) {
		LogError("[" << httpStatusCode << "] KANEVA_AUTH FAILED");
		return false;
	}
	LogInfo("OK");

	// Parse Auth Token
	size_t p1 = responseStr.find("<authtoken>");
	if (p1 != std::string::npos) {
		size_t p2 = responseStr.find("</authtoken>");
		std::string authToken = responseStr.substr(p1 + 11, p2 - p1 - 11);
		m_login.authToken = authToken;
	}
#endif

	// Query user config to determine if we should save the credentials on this successful auth.
	SaveAutoLogonCredentials();

	return true;
}

bool ClientEngine::getGameLoginInfo(
	const std::string& gotoUrl,
	bool doRenderLoop,
	GL_RESULT& glResult,
	std::string& resultDescription,
	std::string& serverIP,
	LONG& serverPort,
	std::string& serverVersion, // drf - added
	LONG& zoneIndex,
	LONG& gameId,
	std::string& gameName,
	bool& incubatorHosted,
	LONG& parentGameId,
	LONG& errorCode,
	std::string& templatePatchDir,
	std::string& appPatchDir) {
	std::string errMsg;

	// Create Login Url
	std::string loginUrl(m_loginUrl);
	fillInLoginUrl(loginUrl, m_login.userEmail);

	// Create Goto Url Override
	std::string gotoUrlOverride = gotoUrl;
	if (gotoUrlOverride.empty())
		gotoUrlOverride = m_gotochannelurlonstartup;

	// Construct Loading Screen
	bool syncEvents = (GetCurrentThreadId() == m_mainThreadId); // DRF - synchronize events on main thread only
	LoadingScreenRAIIHelper loadingScreenInScope(m_dispatcher, m_zoneLoadingStartEventId, m_zoneLoadingCompleteEventId, m_zoneLoadingFailedEventId, syncEvents);

	// Do Login Retries
	bool bStartAppStarted = false;
	bool bAccessPrompted = false;
	(void)bAccessPrompted; // unused variable for KEPEdit builds.
	bool bRetry = true;
	int retries = 0;
	while (bRetry) {
		bRetry = false;

		// Reset Return Parameters
		glResult = GL_UNDEFINED;
		resultDescription = "World Unavailable"; // used as ProgressMenu error message
		serverIP.clear();
		serverPort = 0;
		serverVersion.clear();
		zoneIndex = 0;
		gameId = 0;
		gameName.clear();
		parentGameId = 0;
		incubatorHosted = false;
		errorCode = 0;
		LONG userId = 0;
		std::string userName;
		std::string userGender;

		// Do Web Login
		DWORD httpStatusCode = 0;
		std::string httpStatusDescription;
		std::string resultText;
		bool ret = _GetBrowserLogin(loginUrl, resultText, &httpStatusCode, httpStatusDescription, gotoUrlOverride);
		if (!ret) {
			LogError("_GetBrowserLogin() FAILED - '" << httpStatusDescription << "' httpStatus=" << httpStatusCode);
			glResult = GL_HTTP_ERROR;
			goto LoginFailed;
		}

		// Parse Login Result Text
		ret = ParseGameLoginResult(
			resultText,
			glResult,
			resultDescription,
			userId,
			userName,
			serverIP,
			serverPort,
			serverVersion, // drf - added
			zoneIndex,
			userGender,
			gameId,
			gameName,
			incubatorHosted,
			parentGameId,
			errorCode,
			templatePatchDir,
			appPatchDir);
		if (!ret) {
			LogError("ParseGameLoginResult() FAILED - '" << resultDescription << "' glResult=" << glResult);
			goto LoginFailed;
		}

		// Set User Login Parameters
		m_login.userId = m_userId = userId;
		if (!userName.empty())
			m_login.userName = m_loginName = userName;
		if (!userGender.empty())
			m_login.userGender = m_loginGender = userGender;

		// DRF - Set Crash Report Error Data
		CrashReporter::SetErrorData("userId", m_login.userId);
		CrashReporter::SetErrorData("userName", m_login.userName);
		CrashReporter::SetErrorData("userEmail", m_login.userEmail);
		CrashReporter::SetErrorData("serverIP", serverIP);
		CrashReporter::SetErrorData("serverPort", serverPort);
		CrashReporter::SetErrorData("serverVersion", serverVersion);
		CrashReporter::SetErrorData("zoneIndex", zoneIndex);
		CrashReporter::SetErrorData("gameId", gameId);
		CrashReporter::SetErrorData("gameName", gameName);

		if (glResult == GL_OK) {
			LogInfo("ParseGameLoginResult() OK");
			LogInfo("... userId=" << m_login.userId << " userEmail=" << m_login.userEmail << " userName=" << m_login.userName << " userGender=" << m_login.userGender);
			LogInfo("... serverIP=" << serverIP << " serverPort=" << serverPort << " serverVersion=" << serverVersion);
			LogInfo("... zoneIndex=" << zoneIndex << " gameId=" << gameId << " gameName='" << gameName << "' parentGameId=" << parentGameId);
			loadingScreenInScope.m_success = true;
			return true;
		}

		LogWarn("ParseGameLoginResult() '" << resultDescription << "' glResult=" << glResult);

		// Too Many Retries ?
		if (++retries > 5) {
			glResult = GL_MAX_RETRIES;
			goto LoginFailed;
		}

		switch (glResult) {
			case GL_WEB_WORLD_NOT_FOUND: { // really means offline?

				// World Server Unavailable ?
				if (!incubatorHosted || gotoUrlOverride.empty() || (errorCode != GLA_NO_WORLD_SERVERS)) {
					glResult = GL_WORLD_NOT_FOUND;
					goto LoginFailed;
				}

				// 'World Unavailable' - Incubator Hosted - Try Bringing It Up For A Minute
				if (!bStartAppStarted) {
					bStartAppStarted = true;

					// Update Loading Screen
					m_progressUpdateTotal = 60;
					loadingScreenInScope.Start();
					ProgressUpdate("StartApp", "Contacting Server", 0, TRUE, FALSE);

					// Build Start App Url
					std::string s = UrlParser::unescapeString(gotoUrlOverride);
					std::string startAppUrl(m_startAppUrl);
					fillInStartAppUrl(startAppUrl, s);

					// Backup Username
					CStringA backupUserName = m_multiplayerObj->m_userName;
					m_multiplayerObj->m_userName = m_login.userName.c_str();

					// Do Start App (spins up app incubator world server)
					DWORD httpStatusCodeStartApp;
					std::string httpStatusDescriptionStartApp;
					std::string resultTextStartApp;
					_GetBrowserPage(startAppUrl, 0, 0, resultTextStartApp, &httpStatusCodeStartApp, httpStatusDescriptionStartApp);

					// Restore Username
					m_multiplayerObj->m_userName = backupUserName;
				}

				// Wait 3 Seconds While Rendering Loading Screen
				Timer waitTimer;
				bool bExiting = false;
				while (!bExiting && (waitTimer.ElapsedMs() < (3 * MS_PER_SEC))) {
					if (doRenderLoop) {
						if (!RenderLoop())
							bExiting = true;
					}
					Sleep(50); // Do fewer render loops to allow fast scene loading
					ProgressUpdate("StartApp", "Contacting Server", 1, m_progressUpdateCurrent >= m_progressUpdateTotal);
				}

				// Do Retry
				bRetry = true;
			} break;

#ifdef _ClientOutput // promptAllowAccessAtLogin does not exist in KEPEngineLib
			case GL_WEB_NO_PERMISSION: {
				// Only Prompt Once
				if (bAccessPrompted || !(parentGameId != 0))
					break;
				bAccessPrompted = true;

				// World Server Requires Access Request Prompt
				std::string gameUrl;
				StrBuild(gameUrl, "kaneva://" << gameId);
				if (!promptAllowAccessAtLogin(gameUrl.c_str(), m_allowAccessUrl.c_str(), gameId))
					goto LoginFailed;

				// Do Retry
				bRetry = true;
			} break;
#endif
		} // switch(resultCode)

		// Log Retries
		if (bRetry) {
			LogWarn("Login Retry - retries=" << retries);
		}
	} // while(bTryAgain)

LoginFailed:

	// Signal Progress Menu Error
	GameLoginFailed(glResult);

	return false;
}

bool ClientEngine::CanAutoLogin() const {
	return m_login.autoLogin && !m_login.userEmail.empty() && !m_login.userPassword.empty();
}

bool ClientEngine::Login(const std::string& userEmail, const std::string& userPassword, bool promptAllowAccess) {
	// Increment logon counter
	m_loginCount++;

	// Auto Logon ? (only allowed once)
	bool canAutoLogin = CanAutoLogin();
	std::string loginEmail = (canAutoLogin && userEmail.empty()) ? m_login.userEmail : userEmail;
	std::string loginPassword = (canAutoLogin && userPassword.empty()) ? m_login.userPassword : userPassword;

	LogInfo("Logging In " << loginEmail << " ...");

	// Setup Login Parameters
	m_login.userEmail = loginEmail;
	m_login.userPassword = loginPassword;

	//Call gamelogin.aspx. If the app is hosted and server is offline, call GoToApp then poll gamelogin.aspx until server is ready or timeout.
	GL_RESULT glResult = GL_UNDEFINED;
	std::string resultDescription;
	std::string serverIP;
	LONG serverPort = 0;
	std::string serverVersion; // drf - added
	LONG zoneIndex;
	LONG gameId = 0;
	std::string gameName;
	bool incubatorHosted = false;
	LONG parentGameId = 0;
	LONG errorCode = 0;
	std::string templatePatchDir;
	std::string appPatchDir;
	if (!getGameLoginInfo(
			"",
			true,
			glResult,
			resultDescription,
			serverIP,
			serverPort,
			serverVersion,
			zoneIndex,
			gameId,
			gameName,
			incubatorHosted,
			parentGameId,
			errorCode,
			templatePatchDir,
			appPatchDir)) {
		LogError("GetGameLoginInfo() FAILED");
		GameLoginFailed(glResult);
		return false;
	}

	m_gameName = gameName;

	SetGameId(gameId);
	SetParentGameId(parentGameId);

	if (GetParentGameId() == 0) {
		// Clear child-game patch directories
		m_templatePatchDir.clear();
		m_appPatchDir.clear();
		m_legacyAppPatch = true; // This flag is not used for WOK.
	} else {
		m_templatePatchDir = templatePatchDir;
		m_appPatchDir = appPatchDir;
		m_legacyAppPatch = templatePatchDir.empty() && appPatchDir.empty();
	}

	// 12/06 if override, allow it to work, even if no public servers available
	if (!m_commandlineOverrideIPPort) {
		m_multiplayerObj->setIPAddress(serverIP);
		m_multiplayerObj->m_port = serverPort;
	} else
		m_commandlineOverrideIPPort = false; // only use once

	// if no port, no servers available
	if (m_multiplayerObj->m_port == 0) {
		LogError("serverIP=" << m_multiplayerObj->getIPAddress().c_str() << " port=0 FAILED");
		GameLoginFailed(GL_PORT_ERROR);
		return false;
	}

	// Login To Server We Are Zoning To (if m_gotochannelonstartup is false we should go home)
	if (m_gotochannelonstartup) {
		// The rest of these events were setup when parsing the command line parameters. This event was delayed until zoneindex could be determined.
		LONG replyZoneIndex = zoneIndex;
		if (replyZoneIndex != 0) {
			GotoPlaceEvent* e = new GotoPlaceEvent();
			e->GotoZone(ZoneIndex(replyZoneIndex), m_gotoonstartupId, 0); // char index of zero, WOK specific
			e->AddTo(SERVER_NETID);
			m_reconnectEvent = e;
		} else {
			LogWarn("Skipping send to channel " << m_gotoonstartupId << " since got zoneIndex of zero from web");
		}
	}

	// Login To Server (Network Layer)
	if (!LoginToServer()) {
		LogError("LoginToServer() FAILED");
		GameLoginFailed(GL_NETWORK_ERROR);
		return false;
	}

	// Finally Invalidate Auto Login
	m_login.autoLogin = false;

	// Validate User To Allow Webcalls
	WebValidateUser();

	return true;
}

void ClientEngine::GameLoginFailed(const GL_RESULT& glResult) {
	if (glResult == GL_OK)
		return;

	// Convert glResult To Error Message
	std::string glResultStr = GL_ResultStr(glResult);
	LogError("'" << glResultStr << "' glResult=" << (int)glResult);

	// Send Error Messages For Menus To Catch (stops progress menu spinning)
	m_dispatcher.QueueEvent(new RenderTextEvent(glResultStr, eChatType::System));

	// Send LogonResponseEvent -> LoginMenu.lua
	LogonResponseEvent* e = new LogonResponseEvent();
	if (e) {
		e->SetErrorCodes(glResultStr.c_str(), (HRESULT)glResult);
		m_dispatcher.QueueEvent(e);
	}

	// Validate User To Allow Webcalls
	WebValidateUser();

	// We Need To Fully Disable Wherever We Were Trying To Go
	if (m_gotochannelonstartup) {
		LogError("Disabling Goto Channel On Startup");
		m_gotochannelonstartup = false;
	}
}

bool ClientEngine::LoginToServer() {
	std::string loginUserName = m_login.userName;
	std::string loginPassword = m_login.userPassword;

	LogInfo(loginUserName << " ==> " << m_multiplayerObj->getIPAddress().c_str() << ":" << m_multiplayerObj->m_port);

	m_triggerDelayStamp = fTime::TimeMs();

	SetZoneFileName(""); // force scene reload on reconnect

	m_multiplayerObj->m_passWord = loginPassword.c_str(); // gets hashed in CMultiplayerObj::EnableClient()
	m_multiplayerObj->m_userName = loginUserName.c_str();

	// for re-connect, remove all runtime objects
	// this will prevent movement updates from being send to reconnected server
	DestroyMovementObjectList();

	// do the actual connect
	return m_multiplayerObj->EnableClient();
}

void ClientEngine::Logout() {
	m_multiplayerObj->DisableClient(false);
	MenuExitAll();
	MenuCloseAll();
	StopAllMedia(true);
	m_login.userPassword = "";
	SaveAutoLogonCredentials();
	MenuOpen("LoginMenu.xml");
}

bool ClientEngine::fillInAuthUrl(URL& url) {
	std::map<std::string, std::string>::const_iterator i = url.paramMap().find("version");

	if (i == url.paramMap().end()) {
		LogError("No version");
		return false;
	}

	if (i->second != "1") {
		LogError("Unsupported protocol version");
		return false;
	}

	if (m_login.userName.empty()) {
		LogError("m_login.userName empty");
		return false;
	}

	if (m_login.userPassword.empty()) {
		LogError("m_login.password empty");
		return false;
	}

	// Replace UserName Field
	url.setParam("user", m_login.userName)
		.setParam("pw", m_login.userPassword)
		.setParam("gid", StringHelpers::parseNumber<int>(GetGameId()))
		.setParam("sessid", Application::singleton().sessionUUId());

	if (m_login.authToken.length() > 0)
		url.setParam("authToken", m_login.authToken);
	std::string t = url.toString();
	return true;
}

bool ClientEngine::fillInAuthUrl(std::string& s) {
	URL url = URL::parse(s);
	if (url.paramMap().find("version") != url.paramMap().end()) {
		if (fillInAuthUrl(url)) {
			s = url.toString();
			return true;
		}
		return false;
	}

	// Replace UserName Field
	std::string::size_type p;
	if ((p = s.find("{0}")) == std::string::npos)
		return false;
	if (m_login.userName.empty()) {
		LogError("m_login.userName empty");
		return false;
	}
	s = s.replace(p, 3, UrlEncode(m_login.userName, false));

	// Replace Password Field
	if ((p = s.find("{1}")) == std::string::npos)
		return false;
	if (m_login.userPassword.empty()) {
		LogError("m_login.password empty");
		return false;
	}
	s = s.replace(p, 3, UrlEncode(m_login.userPassword, false));

	// Replace GameId Field
	// Once all servers are using STAR code, the game id should be required
	// before returning true; for now, return true anyway for backwards compatibility
	if ((p = s.find("{2}")) == std::string::npos)
		return true;
	s = s.replace(p, 3, numToString(GetGameId(), "%d").c_str());

	return true;
}

bool ClientEngine::fillInLoginUrl(std::string& s, const std::string& email) {
	// DRF - Added - ParseGameLoginResult() Only Parses Xml Format Now
	if (!STLContainsIgnoreCase(s, "format=xml"))
		s.append("&format=xml");

	// DRF - Added - Append RuntimeReporter::RuntimeId Session GUID (allows system banning)
	std::string runtimeId = RuntimeReporter::RuntimeId();
	if (!runtimeId.empty()) {
		s.append("&runtimeId=");
		s.append(UrlEncode(runtimeId, false));
	}

	// Replace Email Field
	std::string::size_type p;
	if ((p = s.find("{0}")) == std::string::npos)
		return false;
	if (email.empty()) {
		LogError("email empty");
		return false;
	}
	s = s.replace(p, 3, UrlEncode(email, false));

	return true;
}

bool ClientEngine::fillInLoginUrl(std::string& s, const std::string& email, const std::string& gotoUrl) {
	// Replace Email Field
	if (!fillInLoginUrl(s, email))
		return false;

	// Replace GotoUrl Field
	if (gotoUrl.empty()) {
		LogError("gotoUrl empty");
		//return false; // legacy returns true
	}

	std::string::size_type p;
	if ((p = s.find("{1}")) == std::string::npos) {
		s.append("&goto=u&id=");
		s.append(UrlEncode(gotoUrl, false));
	} else {
		s = s.replace(p, 3, UrlEncode(gotoUrl, false));
	}

	return true;
}

bool ClientEngine::fillInStartAppUrl(std::string& s, const std::string& gameUrl) {
	// Replace GameUrl
	std::string::size_type p;
	if ((p = s.find("{0}")) == std::string::npos)
		return false;
	if (gameUrl.empty()) {
		LogError("gameUrl empty");
		//return false; // legacy returns true
	}
	s = s.replace(p, 3, UrlEncode(gameUrl, false));

	return true;
}

} // namespace KEP