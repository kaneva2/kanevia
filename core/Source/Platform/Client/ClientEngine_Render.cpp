///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "resource.h"
#include "ReMeshCreate.h"
#include "DynamicObjectManager.h"
#include "CArmedInventoryClass.h"
#include "CBoneClass.h"
#include "CCharonaClass.h"
#include "CDayStateClass.h"
#include "CDeformableMesh.h"
#include "CHierarchyMesh.h"
#include "CIconSystemClass.h"
#include "CNodeObject.h"
#include "CTimeSystem.h"
#include "RuntimeSkeleton.h"
#include "SkeletonLabels.h"
#include "LightweightGeom.h"
#include <numeric>
#include "Common/KEPUtil/FrameTimeProfiler.h"
#include "Core/Math/TransformUtil.h"
#include "common\include\PreciseTime.h"
#include "OffscreenRenderTask.h"
#include "OffscreenWorldRenderer.h"
#include "RenderEngine\ReDynamicGeom.h"
#include "KEPPhysics\DebugWindow.h"
#include "Core\Math\Units.h"
#include <Core\Math\Simd.h>
#include "KEPPhysics\PhysicsFactory.h"
#include "SerializeFromFile.h"

namespace KEP {

static LogInstance("Instance");

bool ClientEngine::SetD3dDevice() {
	if (!g_pD3dDevice)
		return false;

	m_pRenderStateMgr->SetD3dDevice(g_pD3dDevice);

	GetReDeviceState()->SetD3dDevice(g_pD3dDevice);

	ResetRenderState();

	Matrix44f matWorld;
	matWorld.MakeIdentity();
	g_pD3dDevice->SetTransform(D3DTS_WORLD, reinterpret_cast<const D3DMATRIX*>(&matWorld));
	g_pD3dDevice->Clear(0, nullptr, D3DCLEAR_ZBUFFER | D3DCLEAR_TARGET, m_backgroundColorGlobal, 1.0f, 0);

	return true;
}

bool ClientEngine::RenderLoop(bool /*canWait*/) {
	// FrameTimeProfiler: register ending/starting of a frame
	auto frameTimeProfiler = GetFrameTimeProfiler();
	if (frameTimeProfiler)
		frameTimeProfiler->frame();

	// Update Render Metrics
	RenderMetrics::FrameUpdate();
	MetricsRenderLoop();

	PreRender();

	EventQueueStats();

	// Update Memory Statistics Once Per Minute
	static Timer minTimer;
	if (minTimer.ElapsedSec() > SEC_PER_MIN) {
		minTimer.Reset();
		GetAppMemoryStats();
	}

	// Scheduled Resize ?
	NeedsWindowRefresh();

	MouseOverUpdate();

	MouseKeyboardUpdate();

	// Load New Scene ?
	if (m_loadScene) {
		m_loadScene = false;
		if (!LoadScene(m_loadSceneExgFileName)) {
			ProgressUpdate("STOP", FALSE, TRUE);
			return false;
		}
		ProgressUpdate("STOP", FALSE, TRUE);
	}

	m_pRenderStateMgr->m_stateChangesPerPass = 0;

	UpdateEverything();

	// Limit rendering when rezoning so more time can be spent processing events
	// and initializing objects.
	bool bDoRender = true;
	if (IsZoneLoadingUIActive()) {
		static double s_dRezoneLastRenderTime = 0;
		double dTimeCurrent = GetCurrentTimeInSeconds();
		if (dTimeCurrent < s_dRezoneLastRenderTime + m_fRezoneRenderIntervalSeconds)
			bDoRender = false;
		else
			s_dRezoneLastRenderTime = dTimeCurrent;
	}

	if (bDoRender) {
		RenderAndPresentActiveViewport();
		ProcessOneOffscreenRenderTask();
		RenderPhysics();

		if (m_textRenderList)
			m_textRenderList->Flush();
	}

	{
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "ProcessEvents");

		// m_dispatchTime can be varied, e.g. when the user
		// first zones (and it locked in place) the dispatch
		// time can be much longer, to allow the client to process
		// add object event backlogs, than after the user starts
		// to move around and FPS is an issue.
		m_dispatcher.ProcessEventsUntil(getDispatchTime());
	}

	if (m_closeDownCall) {
		LogFatal("Close Down Call - Shutting Down ...");
		PostMessage(WM_CLOSE);
	}

	return true;
}

void ClientEngine::PreRender() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "PreRender");

	// Wait until it's time for the next frame.
	double currentTime = GetCurrentTimeInSeconds();

	const float fDesiredFrameDuration = c_frameTimeTargetMs / 1000.0f;

	// This test has been ported from old code because I don't understand the meaning of the m_serverStarted
	// variable well enough to remove or refactor it.
	if (!m_multiplayerObj->m_serverStarted && GetLimitFPS()) {
		// Need to use double types for these calculations. Machines that haven't been rebooted for a while
		// may lose too much precision if using float types.
		const double desiredFrameStartTime = m_PrevFrameStartTime + fDesiredFrameDuration;
		const double minWaitTime = 1e-4; // Don't bother waiting for very short durations.
		const double waitTime = desiredFrameStartTime - currentTime;
		if (waitTime > minWaitTime) {
			// Duration for SetWaitableTimer is in multiples of 100ns.
			LARGE_INTEGER waitDuration;
			waitDuration.QuadPart = -waitTime * 1e7f; // Negative sign tells API that wait is relative to the current time.
			if (SetWaitableTimer(m_hRenderLoopWaitableTimer, &waitDuration, 0, nullptr, nullptr, FALSE)) {
				if (!WaitForSingleObject(m_hRenderLoopWaitableTimer, INFINITE) != WAIT_OBJECT_0) {
					// Ignore error.
				}
			}
			// currentTime is changed after the wait.
			currentTime = GetCurrentTimeInSeconds();
		}
	}

	// Update game time.
	double newGameTime = currentTime + m_GameTimeOffset;
	if (m_PreviousGameTime <= m_CurrentGameTime) {
		m_PreviousGameTime = m_CurrentGameTime;

		// Game time cannot go backwards.
		if (newGameTime > m_CurrentGameTime)
			m_CurrentGameTime = newGameTime;
	} else {
		// First time through. There was no previous.
		m_PreviousGameTime = newGameTime;
		m_CurrentGameTime = newGameTime;
	}

	// Save this frame's time to be the next iteration's previous frame.
	m_PrevFrameStartTime = currentTime;
	m_iPrevFrameTickCount = GetTickCount(); // Need to use Windows tick count because this value is compared against the tick count stored with windows messages.
}

bool ClientEngine::RenderAndPresentActiveViewport() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "RenderAndPresentActiveViewport");

	if (!g_pD3dDevice)
		return false;

	// Get Active Viewport
	auto pVO = GetActiveViewport();
	if (!pVO)
		return false;

	// Render Viewport
	if (!RenderViewport(pVO))
		return false;

	//Ankit ----- if m_captureScreen flag is set, then before presenting, copy the back buffer data to the specified image file
	if (m_captureScreen) {
		m_captureScreen = false;
		CaptureScreen(m_captureScreenRect, m_captureScreenPath);
	}

	{
		FrameTimeNestedContext ftnc1(GetFrameTimeProfiler(), "D3DPresent");

		// Present back buffer
		HRESULT err = g_pD3dDevice->Present(nullptr, nullptr, nullptr, nullptr);
		if (FAILED(err)) {
			LogError("Present() FAILED - '" << DXGetErrorDescription9A(err) << "'");
			return false;
		}
	}

	return true;
}

bool ClientEngine::RenderViewport(CViewportObj* pVO) {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "RenderViewport");

	if (!g_pD3dDevice || !pVO)
		return false;

	auto pCBF = ClientBladeFactory::Instance();
	if (!pCBF)
		return false;

	auto pRD = GetReDeviceState();
	if (!pRD)
		return false;

	// Is Client Window In View ?
	if (!IsInView())
		return false;

	auto pRSM = GetRenderStateManager();
	if (!pRSM)
		return false;

	auto pDRS = pRD->GetRenderState();
	if (!pDRS)
		return false;

	// Call All Blades OnUpdate()
	pCBF->OnUpdateBlades();

	// Begin Scene
	if (FAILED(g_pD3dDevice->BeginScene())) {
		if (FAILED(g_pD3dDevice->EndScene()))
			return false;
		if (FAILED(g_pD3dDevice->BeginScene()))
			return false;
	}

	// Activate Viewport
	pVO->m_activated = false; // reset this so the viewport is correctly initialized
	ActivateViewport(pVO); // drf - moved from RenderViewportScene

	pDRS->SetCamMatrix(reinterpret_cast<D3DXMATRIX*>(&pVO->m_camWorldMatrix));

	// Set All Render Blades Render Options
	SetRenderBladesRenderOptions();

	// Render Everything Requiring Z Use
	pRSM->ResetRenderState();
	RenderViewportScene(pVO);
	pCBF->RenderBlades(RequiresZUse);
	WidgetsRender();

	pRSM->ResetRenderState();
	EvaluateAndRenderCharonaOverlaysV2(pVO);
	RenderOverlays(pVO);
	RenderMenuGameObjects();

	// Render Everything That Clears Z
	pRSM->ResetRenderState();
	pCBF->RenderBlades(ClearsZ);
	if (m_dynamicGeom)
		m_dynamicGeom->DrawDynamicGeom();

	// Render Menu Game Objects that are "Always on Top"
	pRSM->ResetRenderState();
	if (m_pMenuGameObjects)
		m_pMenuGameObjects->Render(Matrix44f::GetIdentity(), true);

	g_pD3dDevice->EndScene();

	return true;
}

bool ClientEngine::RenderViewportScene(CViewportObj* pVO) {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "RenderViewportScene");

	if (!g_pD3dDevice)
		return false;

	SceneMultipassOptions multipassOptions;
	multipassOptions.shadowType = (SceneMultipassOptions::ShadowType)m_globalShadowType;

	ScenePassOptions passOptions;
	passOptions.renderTargetClearFlags = D3DCLEAR_ZBUFFER | D3DCLEAR_TARGET;
	passOptions.drawWithSpotShadows = (multipassOptions.shadowType == SceneMultipassOptions::ST_SPOTSHADOW);

	if (passOptions.renderTargetClearFlags != 0)
		g_pD3dDevice->Clear(0, nullptr, passOptions.renderTargetClearFlags, m_backgroundColorGlobal, 1.0f, 0);

	// preprocess world object to generate render lists for potentially visible objects in the appropriate order
	GenerateRenderLists(pVO, passOptions);

	DrawRenderLists(pVO, passOptions);

	if (m_pRenderStateMgr)
		m_pRenderStateMgr->OverrideFXShader(0);

	return true;
}

bool ClientEngine::ProcessOneOffscreenRenderTask() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "ProcessOneOffscreenRenderTask");

	if (!IsInView())
		return false;

	// Any render tasks?
	if (m_offscreenRenderTasks.empty())
		return false;

	// Process it
	auto pTask = m_offscreenRenderTasks.front();
	if (OffscreenWorldRenderer(this, pTask)()) {
		// Completed or retries exhausted
		if (pTask->m_completed) {
			LogDebug("COMPLETED - elapsed: " << fTime::ElapsedMs(pTask->m_startTime));
		} else {
			LogWarn("FAILED");
		}
		m_offscreenRenderTasks.pop_front();
		delete pTask;
	}

	return true;
}

void ClientEngine::QueueOffscreenRenderTask(IDirect3DSurface9* target, WorldRenderStyle style, WorldRenderOptions options, double minRadiusFilter, bool overrideMatrices, const Matrix44f* camMatrix, const Matrix44f* projMatrix) {
	if (target != nullptr) {
		// Cancel previously queued tasks on the same target, if any
		for (auto itr = m_offscreenRenderTasks.begin(); itr != m_offscreenRenderTasks.end();) {
			if ((*itr)->m_target == target) {
				itr = m_offscreenRenderTasks.erase(itr);
				LogDebug("Cancelled previously queued task on " << std::hex << std::setw(8) << std::setfill('0') << (unsigned)target);
			} else {
				++itr;
			}
		}
	}

	m_offscreenRenderTasks.push_back(new OffscreenRenderTask(target, style, options, minRadiusFilter, overrideMatrices, camMatrix == nullptr ? Matrix44f() : *camMatrix, projMatrix == nullptr ? Matrix44f() : *projMatrix));
}

//Ankit ----- Function called to copy the rendertarget/backbuffer data into the image.
void ClientEngine::CaptureScreen(const RECT& rect, const std::string& fullPath) {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "CopyBackBufferDataToImage");

	bool success = false;

	bool fullscreen = ((rect.bottom == -1) && (rect.top == -1) && (rect.right == -1) && (rect.left == -1));

	if (g_pD3dDevice) {
		// Create a temporary surface to store the screenshot
		HRESULT hr;
		CComPtr<IDirect3DSurface9> renderTarget;
		//		hr = g_pD3dDevice->GetRenderTarget(0, &renderTarget);
		hr = GetRenderTarget_NonMultiSample(std::addressof(renderTarget)); // drf - ed-8089
		if (SUCCEEDED(hr) && renderTarget) {
			D3DSURFACE_DESC renderTargetDesc;
			hr = renderTarget->GetDesc(&renderTargetDesc);
			if (SUCCEEDED(hr)) {
				CComPtr<IDirect3DSurface9> surface = nullptr;
				hr = g_pD3dDevice->CreateOffscreenPlainSurface(
					renderTargetDesc.Width,
					renderTargetDesc.Height,
					renderTargetDesc.Format,
					D3DPOOL_SYSTEMMEM,
					&surface,
					nullptr);
				if (SUCCEEDED(hr) && surface) {
					hr = g_pD3dDevice->GetRenderTargetData(renderTarget, surface);
					if (SUCCEEDED(hr)) {
						hr = D3DXSaveSurfaceToFile(
							Utf8ToUtf16(fullPath).c_str(),
							D3DXIFF_JPG,
							surface,
							nullptr,
							fullscreen ? nullptr : &rect);
						if (SUCCEEDED(hr))
							success = true;
					}
				}
			}
		}
	}

	if (success) {
		std::string msg = "Screenshot saved to '" + fullPath + "'";
		SendChatMessage(eChatType::System, msg);
	} else {
		SendChatMessage(eChatType::System, "Error saving screenshot!");
		LogError("Error saving screenshot to " << fullPath);
	}

	IEvent* pEvent = m_dispatcher.MakeEvent(m_prevImgAvailableEventId);
	if (pEvent)
		m_dispatcher.QueueEvent(pEvent);
}

// DRF - ED-8089 - Fix Anti-Aliasing Issues - Capture Screen
HRESULT ClientEngine::GetRenderTarget_NonMultiSample(CComPtr<IDirect3DSurface9>* pRenderTarget_out) {
	IDirect3DDevice9* pd3dDevice = GetD3DDevice();
	if (!pd3dDevice)
		return -1;

	// Get Active Viewport
	auto pVO = GetActiveViewport();
	if (!pVO)
		return -1;

	// Get Viewport Dimensions
	auto width = pVO->GetWidth();
	auto height = pVO->GetHeight();

	CComPtr<IDirect3DSurface9> newRenderTarget = nullptr;
	CComPtr<IDirect3DSurface9> newZBuffer = nullptr;
	CComPtr<IDirect3DSurface9> savedZBuffer = nullptr;
	CComPtr<IDirect3DSurface9> savedRenderTarget = nullptr;

	// Create Non-Multi-Sampled Render Target
	HRESULT hr = pd3dDevice->CreateRenderTarget(width, height, D3DFMT_X8R8G8B8, D3DMULTISAMPLE_NONE, 0, FALSE, &newRenderTarget, nullptr);
	if (hr == D3D_OK && newRenderTarget) {
		// Create Non-Multi-Sampled Z Buffer
		hr = pd3dDevice->CreateDepthStencilSurface(width, height, D3DFMT_D24S8, D3DMULTISAMPLE_NONE, 0, TRUE, &newZBuffer, nullptr);
		if (hr == D3D_OK && newZBuffer) {
			// Override Z Buffer
			hr = pd3dDevice->GetDepthStencilSurface(&savedZBuffer);
			if (hr == D3D_OK && savedZBuffer) {
				hr = pd3dDevice->SetDepthStencilSurface(newZBuffer);
				if (hr == D3D_OK) {
					// Override Render Target
					hr = pd3dDevice->GetRenderTarget(0, &savedRenderTarget);
					if (hr == D3D_OK && savedRenderTarget) {
						hr = pd3dDevice->SetRenderTarget(0, newRenderTarget);
						if (hr == D3D_OK) {
							// Render Viewport
							RenderViewport(pVO);
							*pRenderTarget_out = newRenderTarget;
						}
					}
				}
			}
		}
	}

	// Restore Z Buffer
	if (savedZBuffer)
		pd3dDevice->SetDepthStencilSurface(savedZBuffer);

	// Restore Render Target
	if (savedRenderTarget)
		pd3dDevice->SetRenderTarget(0, savedRenderTarget);

	return S_OK;
}

#if DEPRECATED
// DRF - DEAD CODE - Have yet to see this get called
void ClientEngine::MultipassRenderMesh(CEXMeshObj* mesh, int lod) {
	LogWarn("DEAD CODE");
	m_pRenderStateMgr->SetTextureState(mesh->m_materialObject->m_texNameHash[0], 0, m_pTextureDB);
	if (mesh->m_materialObject->m_texBlendMethod[9] < 0) {
		m_pRenderStateMgr->SetBlendLevel(0, 0);
	} else {
		m_pRenderStateMgr->SetBlendLevel(0, mesh->m_materialObject->m_texBlendMethod[9]);
	}

	for (int loop = 1; loop < m_pRenderStateMgr->m_multiTextureSupported; loop++) {
		m_pRenderStateMgr->SetTextureState(mesh->m_materialObject->m_texNameHash[loop], loop, m_pTextureDB);
		m_pRenderStateMgr->SetBlendLevel(loop, mesh->m_materialObject->m_texBlendMethod[(loop - 1)]);
		if (loop > 7) {
			break;
		}
	}

	for (int loop = 0; loop < 8; loop++) { // VS8 fix added "int"
		m_pRenderStateMgr->SetColorArg1Level(loop, mesh->m_materialObject->m_colorArg1[loop]);
		m_pRenderStateMgr->SetColorArg2Level(loop, mesh->m_materialObject->m_colorArg2[loop]);
	}

	m_pRenderStateMgr->SetDiffuseSource(mesh->m_abVertexArray.m_advancedFixedMode);
	m_pRenderStateMgr->SetVertexType(mesh->m_abVertexArray.m_uvCount);
	mesh->TextureMovementCallback(m_pRenderStateMgr->m_currentVertexType);
	m_pRenderStateMgr->SetPxShader(m_pxShaderSystem, mesh->m_materialObject->m_pixelShader);
	m_pRenderStateMgr->SetVertexShader(m_shaderSystem, mesh->m_materialObject->m_pixelShader);
	mesh->Render(g_pD3dDevice, m_pRenderStateMgr->m_currentVertexType, mesh->m_chromeUvSet);
}
#endif

void ClientEngine::SetRenderBladesRenderOptions() {
	// Set Menu Render Options
	auto pMB = ClientBladeFactory::Instance()->GetMenuBlade();
	if (pMB)
		pMB->SetRenderOptions(m_menuRenderOptions);

	// Set Particle Render Options
	auto pPB = ClientBladeFactory::Instance()->GetClientParticleBlade();
	if (pPB) {
		ParticleRenderOptions pro;
		pro.m_enabled = GetParticlesEnabled();
		pro.m_fps = RenderMetrics::GetFramesPerSec(true); // filtered
		GetParticleBias(pro.m_bias, pro.m_biasAuto);
		GetCullBias_DynamicObjects(pro.m_DOCullBias, pro.m_DOCullBiasAuto);
		pPB->SetRenderOptions(pro);
	}
}

bool ClientEngine::ResetRenderState() {
	m_pRenderStateMgr->m_stencilBufferAble = m_supportedStencilBuffer;
	return (m_pRenderStateMgr->ResetRenderState() != CStateManagementObj::RS_FAILURE);
}

void ClientEngine::ClearRenderLists(ULONG aClearFlags) {
	if (aClearFlags && RLClear_ALL) {
		if (m_bLightListDirty)
			m_runtimeLightSys->FlushSys(g_pD3dDevice, m_pRenderStateMgr);
		m_renderContainerList->SafeDelete();
		m_renderContainerAlphaFilterList->SafeDelete();
		m_visualsRenderList->RemoveAll();
#if (DRF_WORLD_MIRROR_LIST == 1)
		m_worldMirrorList->RemoveAll();
#endif

		// flush transparent primatives
		m_translucentPolyList->SafeDelete();
		m_shadowObjectsList->SafeDelete();
		m_shadowEntityList->RemoveAll();
		RuntimeParticleCleanup();

#if (DRF_WORLD_ENVIRONMENT_LIST == 1)
		for (POSITION posRenLoc = m_worldEnvironmentList->GetHeadPosition(); posRenLoc != NULL;) {
			auto pWO = (CWldObject*)m_worldEnvironmentList->GetNext(posRenLoc);
			if (!pWO)
				continue;
			pWO->m_poppedOut = TRUE;
		}
		m_worldEnvironmentList->RemoveAll();
#endif
	}
}

void ClientEngine::CreateViewVolumePlanes(const CViewportObj* pVO, Plane3f (&aViewVolumePlanes)[6]) const {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "CreateViewVolumePlanes");

	Matrix44f mtxVP = pVO->m_viewMatrix * pVO->m_projMatrix;
	CreateD3DViewVolumePlanes(mtxVP, aViewVolumePlanes, 6);
}

bool ClientEngine::GenerateRenderLists(CViewportObj* pVO, const ScenePassOptions& aPassOptions) {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "GenerateRenderLists");

	if (aPassOptions.renderListsClearFlags == RLClear_NONE)
		return true;

	Matrix44f camTransform = pVO->m_camWorldMatrix;
	Matrix44f camTransformInverse = pVO->m_viewMatrix;
	Vector3f camPosition = pVO->m_camWorldPosition;

	static int has_rendered_successfully = true;

	MeshRM::Instance()->ResetMeshCaches(GetReDeviceState());

	try {
		m_bRenderEnvironment = TRUE;

		// prepare frustums
		CreateViewVolumePlanes(pVO, m_aViewVolumePlanes);

		// Clear Render Lists to be filled up again
		ClearRenderLists(aPassOptions.renderListsClearFlags);

		GenRenderList_WorldObjects(pVO, aPassOptions);

		if (!aPassOptions.disableObjectRendering)
			GenRenderList_DynamicSystem(pVO, aPassOptions);

		if (!aPassOptions.disableVisualizations && m_renderSoundPlacements)
			GenRenderList_SoundPlacement(pVO, aPassOptions);

		if (!aPassOptions.disableVisualizations && (m_renderDynamicTriggers || m_renderWorldTriggers))
			GenRenderList_Triggers(pVO, aPassOptions);

		if (!aPassOptions.disableVisualizations && m_renderMediaTriggers)
			GenRenderList_MediaTriggers(pVO, aPassOptions);

		if (!aPassOptions.disableEnvironmentRendering)
			GenRenderList_Environment(pVO, aPassOptions);

		if (!aPassOptions.disableAvatarRendering)
			GenRenderList_MovementObjects(pVO, aPassOptions);

		if (!aPassOptions.disableVisualizations)
			GenRenderList_LWG(pVO, aPassOptions);

		m_bLightListDirty = false;

		has_rendered_successfully = true;

		return true;
	} catch (...) {
		LogError("CAUGHT EXCEPTION");

		// last time through, we successfully rendered, so this is the first time exception
		//  as a measure of defense, try to rebuild the collision set (better than crashing)
		if (has_rendered_successfully) {
			// sometimes it may be useful to rebuild the collision table
			// there was here a call to do this
			// but the function is in the editor subclass, and we don't want it to run
			// in a client anyhow...
			has_rendered_successfully = false;
		}

		return false;
	}
}

bool ClientEngine::GenRenderList_WorldObjects(CViewportObj* pVO, const ScenePassOptions& aPassOptions) {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "GenRenderList_WorldObjects");

	if (!pVO)
		return false;

	// Get Viewport Camera Transform
	Matrix44f camTransform = pVO->m_camWorldMatrix;
	Matrix44f camTransformInverse = pVO->m_viewMatrix;
	Vector3f camPosition = pVO->m_camWorldPosition;

	// WORLD OBJECTS
	if (!m_pCollisionObj)
		return false;

	if (!m_pCollisionObj->IsSubBoxesValid())
		return false;

	Vector3f viewPoint;
	Vector3f center, transformedCenter;

	m_goodSubBox = NULL;

	// flag all reference items to be able to animate again (optimization)
	if (m_libraryReferenceDB)
		m_libraryReferenceDB->FlagAllForAnimUpdate();

	// set world particle sys to non regen to flag it
	if (GetParticlesEnabled()) {
		for (POSITION wldObjPosition = m_curWldObjPrtList->GetHeadPosition(); wldObjPosition;) {
			auto pWO = reinterpret_cast<CWldObject*>(m_curWldObjPrtList->GetNext(wldObjPosition));
			if (!pWO || !pWO->m_runtimeParticleId)
				continue;
			m_particleRM->SetRuntimeEffectStatus(pWO->m_runtimeParticleId, -1, -1, 0);
		}
	}

	if (aPassOptions.disableDistanceCulling || m_pCollisionObj->BaseBoxContains(camPosition)) {
		// if in bounds
		m_goodSubBox = m_pCollisionObj->FindLeafBoxAtPosition(camPosition);

		// build render list
		m_groupFilterCount = 0;

		if (m_pCollisionObj->HasWorldObjectReferenceArray()) {
			// get group filters
			for (auto wldObjIndex : m_goodSubBox->m_objectReferenceList) {
				CWldObject* pWO = m_pCollisionObj->GetWorldObjectReference(wldObjIndex);
				if (!pWO || (pWO->m_filterGroup == -1))
					continue;

				BoundBox box(pWO->m_popoutBox);
				if (box.contains(camPosition))
					AddGroupFilter(pWO->m_filterGroup);
			}

			// List of world objects to iterate
			// If distance culling is disabled, fill another list so that we will iterate through all world objects in the zone.
			// Default (with distance culling) to the list of world objects inside m_goodSubBox
			auto objectReferenceList = &m_goodSubBox->m_objectReferenceList;
			std::vector<WORD> allObjectReferences;
			if (aPassOptions.disableDistanceCulling) {
				allObjectReferences.resize(m_pCollisionObj->GetWorldObjectCount());
				std::iota(allObjectReferences.begin(), allObjectReferences.end(), 0);
				objectReferenceList = &allObjectReferences;
			}

			// World Object Render Loop
			for (auto objectReference : *objectReferenceList) {
				CWldObject* pWO = m_pCollisionObj->GetWorldObjectReference(objectReference);
				if (!pWO || pWO->m_renderFilter)
					continue;

				BoundBox box(pWO->m_popoutBox);
				if (!aPassOptions.disableDistanceCulling && !box.contains(camPosition))
					continue;

				if (!aPassOptions.disableWorldObjectGroupFiltering && !ValidateGroup(pWO->m_groupNumber))
					continue;

				if (pWO->m_removeEnvironment)
					m_bRenderEnvironment = FALSE;

				// Apply Environmental Animations (panning textures)
				if (GetAnimationsEnabled() && pWO->m_animModule && pWO->m_meshObject && pWO->GetReMesh()) {
					pWO->m_animModule->ApplyAnimation(pWO->GetReMesh(), g_pD3dDevice);
				}

				bool proceed = (pWO->m_environmentAttached > 0) || (pWO->m_renderInMirrorTag == TRUE);

				// Add Lighting
				if (m_bLightListDirty && pWO->m_lightObject) {
					if (pWO->m_meshObject) {
						m_runtimeLightSys->AddLight(pWO->m_lightObject, pWO->m_meshObject->GetMeshCenter());
						proceed = true;
						if (!pWO->m_lightObject->m_render)
							continue;
					} else if (!pWO->m_node) {
						m_runtimeLightSys->AddLight(pWO->m_lightObject, pWO->m_lightObject->m_wldspacePos);
						proceed = false;
					} else if (pWO->m_node) {
						proceed = true;
					}
				}

				// Do Frustum Culling
				// DRF - Have yet to see a case where environmentAttached is > 0
				// I believe worldEnvironmentList may be dead code.
				if (pWO->m_environmentAttached > 0) {
#if (DRF_WORLD_ENVIRONMENT_LIST == 1)
					m_worldEnvironmentList->AddTail(pWO);
					pWO->m_poppedOut = FALSE;
#endif
				} else if (pWO->m_node) {
					if (!proceed) {
						if (!IsBoxInView(pWO->m_node->m_clipBox)) {
							pWO->m_culled = TRUE;
							continue;
						}
					}

					pWO->m_culled = FALSE;
					CReferenceObj* refObj = NULL;
					refObj = m_libraryReferenceDB->GetByIndex(pWO->m_node->m_libraryReference);
					if (refObj) {
						// valid reference object
						if (m_bLightListDirty && pWO->m_lightObject) {
							// add light
							Vector3f& meshCenter = *((Vector3f*)&refObj->m_matrix[3]);
							Vector3f lightCenter;
							lightCenter.x = meshCenter.x + pWO->m_node->GetTranslation().x;
							lightCenter.y = meshCenter.y + pWO->m_node->GetTranslation().y;
							lightCenter.z = meshCenter.z + pWO->m_node->GetTranslation().z;
							m_runtimeLightSys->AddLight(pWO->m_lightObject, lightCenter);
						} // end add light

						// Update Environmental Animations (texture panning)
						if (GetAnimationsEnabled() && refObj->m_animModule) {
							if (!refObj->m_animationUpdatedThisRound) {
								if (refObj->GetReMesh())
									refObj->m_animModule->ApplyAnimation(refObj->GetReMesh(), g_pD3dDevice);
								refObj->m_animationUpdatedThisRound = TRUE;
							}
						}

						center.x = pWO->m_node->GetTranslation().x;
						center.y = pWO->m_node->GetTranslation().y;
						center.z = pWO->m_node->GetTranslation().z;
						transformedCenter = TransformPoint_NonPerspective(pWO->m_node->GetWorldTransform(), refObj->m_matrix.GetTranslation());

						if (refObj->m_lodVisualList) {
							// LOD sys exists
							if (refObj->m_lodVisualList->m_lodType == 0) {
								viewPoint = camTransform.GetTranslation();
							} else if (refObj->m_lodVisualList->m_lodType == 1) {
								viewPoint = camTransform.GetTranslation() - pWO->m_node->GetTranslation();
							} else {
								viewPoint = camTransform.GetTranslation();
							}

							float alphaTest1 = 1.0f;
							float alphaTest2 = 1.0f;
							CMeshLODObject* mObjSecond = NULL;
							UINT index;

							CMaterialObject* matPtr = pWO->GetCustomMaterial() == NULL ? refObj->m_material : pWO->GetCustomMaterial();

							matPtr->Callback();

							CMeshLODObject* mObj = refObj->m_lodVisualList->GetLodVisualExt(
								viewPoint,
								center,
								pVO->GetCameraFovY(),
								&mObjSecond,
								&alphaTest1,
								&alphaTest2,
								index);

							if (mObjSecond) {
								ReMesh* mesh = pWO->GetReLODMesh(index);
								if (mesh) {
									mesh->SetMaterial(matPtr->GetReMaterial());
									MeshRM::Instance()->LoadMeshCache(mesh);
								}
							}

							if (mObj) {
								// valid LOD
								if (refObj->m_faceCamera)
									MatrixARB::FaceTowardPoint(camPosition, (Matrix44f*)pWO->GetReLODMesh(index)->GetWorldMatrix());

								ReMesh* mesh = pWO->GetReLODMesh(index);
								if (mesh) {
									mesh->SetMaterial(matPtr->GetReMaterial());
									MeshRM::Instance()->LoadMeshCache(mesh);
								}
							} else {
								// invalid LOD
								if (refObj->m_faceCamera)
									MatrixARB::FaceTowardPoint(camPosition, (Matrix44f*)pWO->GetReMesh()->GetWorldMatrix());

								ReMesh* mesh = pWO->GetReMesh();
								if (mesh) {
									mesh->SetMaterial(matPtr->GetReMaterial());
									MeshRM::Instance()->LoadMeshCache(mesh);
								}
							}
						} else {
							// standard implement
							refObj->m_matrix = pWO->m_node->GetWorldTransform();
							if (refObj->m_faceCamera)
								MatrixARB::FaceTowardPoint(camPosition, &refObj->m_matrix);

							// load ReMaterial texture pointer with video texture
							auto pExternalSource = refObj->GetExternalSource();
							if (GetMediaScrapeEnabled() && pExternalSource) {
								// DRF - ScrapeTexture() Is Self Metered
								pExternalSource->ScrapeTexture(m_pReDeviceState);

								if (pExternalSource->GetReMaterial()) {
									ReMesh* mesh = pWO->GetReMesh();
									if (mesh) {
										mesh->SetMaterial(pExternalSource->GetReMaterial());
										MeshRM::Instance()->LoadMeshCache(mesh);
									}

									continue;
								}
							} else {
								auto matPtr = pWO->GetCustomMaterial();
								if (!matPtr)
									matPtr = refObj->m_material;
								if (matPtr)
									matPtr->Callback();

								ReMesh* mesh = pWO->GetReMesh();
								if (mesh && matPtr) {
									mesh->SetMaterial(matPtr->GetReMaterial());
									MeshRM::Instance()->LoadMeshCache(mesh);
								}
							}
						} // end standard implement
					} // end valid reference object
				} // end node type
				else if (pWO->m_meshObject) {
					// use identity matrix if NOT using ReMesh or animating
					if (pWO->m_ReMesh == NULL || pWO->m_animModule)
						pWO->m_meshObject->ResetWorldTransform(); // reset to identity matrix

					if (pWO->m_meshObject->m_charona)
						m_visualsRenderList->AddTail(pWO->m_meshObject);

					// load ReMaterial texture pointer with video texture
					auto pExternalSource = pWO->m_meshObject->GetExternalSource();
					if (GetMediaScrapeEnabled() && pExternalSource) {
						// DRF - ScrapeTexture() Is Self Metered
						pExternalSource->ScrapeTexture(m_pReDeviceState);

						if (pExternalSource->GetReMaterial()) {
							ReMesh* mesh = pWO->GetReMesh();
							if (mesh) {
								mesh->SetMaterial(pExternalSource->GetReMaterial());
								MeshRM::Instance()->LoadMeshCache(mesh);
							}

							continue;
						}
					}

					// if transparent handle insertion differently
					if (pWO->m_meshObject->m_materialObject->m_mirrorEnabled == FALSE) {
						if (pWO->m_lodVisualList) {
							// LOD list exists
							viewPoint = camTransform.GetTranslation();

							UINT index;
							CMeshLODObject* mObj = pWO->m_lodVisualList->GetLodVisual(
								viewPoint,
								pWO->m_meshObject->GetMeshCenter(),
								pVO->GetCameraFovY(),
								index);

							if (mObj) {
								if (!proceed) {
									if (!IsBoxInView(mObj->GetBoundingBox())) {
										if (pWO->m_runtimeParticleId)
											m_particleRM->SetRuntimeEffectStatus(pWO->m_runtimeParticleId, -1, 0, 1);
										pWO->m_culled = TRUE;
										continue;
									}
								}

								pWO->m_culled = FALSE;

								ManageEnvEffects(pWO, mObj->m_material);

								// particle expansion
								if (GetParticlesEnabled() && (pWO->m_particleLink > -1)) {
									// create new
									if (pWO->m_runtimeParticleId) {
										m_particleRM->SetRuntimeEffectStatus(pWO->m_runtimeParticleId, -1, 1, 1);
									} else {
										EnvParticleSystem* pEPS = m_particleRM->GetStockItemByIndex(pWO->m_particleLink);
										if (pEPS) {
											// ref db
											UINT id = m_particleRM->AddRuntimeEffect(pEPS->m_systemName);
											if (id) {
												pWO->m_runtimeParticleId = id;
												m_particleRM->SetRuntimeEffectStatus(id, 1, 1, -1);
											}

											Matrix44f genMatrix;
											genMatrix.MakeTranslation(pWO->m_meshObject->GetMeshCenter());

											Vector3f focusPoint = pWO->m_meshObject->GetMeshCenter() - pWO->m_particleDirection;
											MatrixARB::FaceTowardPoint(focusPoint, &genMatrix);

											m_particleRM->SetRuntimeEffectMatrix(pWO->m_runtimeParticleId, genMatrix);
											m_curWldObjPrtList->AddTail(pWO); // for persistence

										} // end ref db
									} // end create new
								} // end particle expansion

								// CMeshLODObject no longer carries CEXMeshObj. Should always use reMesh now.
								ReMesh* mesh = pWO->GetReLODMesh(index);

								if (mesh) {
									mesh->SetMaterial(pWO->m_meshObject->m_materialObject->GetReMaterial());
									MeshRM::Instance()->LoadMeshCache(mesh);
								}
							} else {
								if (!proceed) {
									if (!IsMeshInView(pWO->m_meshObject)) {
										if (pWO->m_runtimeParticleId)
											m_particleRM->SetRuntimeEffectStatus(pWO->m_runtimeParticleId, -1, 0, 1);
										pWO->m_culled = TRUE;
										continue;
									}
								}

								ManageEnvEffects(pWO, pWO->m_meshObject);

								pWO->m_culled = FALSE;

								if (GetParticlesEnabled() && (pWO->m_particleLink > -1)) {
									// particle expansion
									if (pWO->m_runtimeParticleId) {
										// prolong existing
										m_particleRM->SetRuntimeEffectStatus(pWO->m_runtimeParticleId, -1, 1, 1);
									} else {
										// create new
										EnvParticleSystem* pSys = m_particleRM->GetStockItemByIndex(pWO->m_particleLink);
										if (pSys) {
											// ref db
											UINT id = m_particleRM->AddRuntimeEffect(pSys->m_systemName);
											pWO->m_runtimeParticleId = id;

											m_particleRM->SetRuntimeEffectStatus(pWO->m_runtimeParticleId, 1, -1, -1);

											Matrix44f genMatrix;
											genMatrix.MakeTranslation(pWO->m_meshObject->GetMeshCenter());

											Vector3f focusPoint = pWO->m_meshObject->GetMeshCenter() - pWO->m_particleDirection;
											MatrixARB::FaceTowardPoint(focusPoint, &genMatrix);

											m_particleRM->SetRuntimeEffectMatrix(pWO->m_runtimeParticleId, genMatrix);
											m_curWldObjPrtList->AddTail(pWO); // for persistence
										} // end ref db
									} // end create new
								} // end particle expansion

								// Fixed "Base mesh not rendered for zone objects with LODs
								if (pWO->m_ReMesh == NULL) {
									CategorizeMesh(pWO->m_meshObject,
										camTransform,
										pWO->m_meshObject->GetMeshCenter(),
										camTransformInverse,
										pWO->m_meshObject->GetMeshCenter(),
										NULL,
										pWO->GetCustomMaterial());
								} else {
									// use ReMesh renderer
									if (!pWO->m_meshObject->m_Rendered)
										return true;

									CMaterialObject* matPtr = pWO->GetCustomMaterial() == NULL ?
																  pWO->m_meshObject->m_materialObject :
																  pWO->GetCustomMaterial();

									matPtr->Callback();

									ReMesh* mesh = pWO->GetReMesh();

									if (mesh) {
										mesh->SetMaterial(matPtr->GetReMaterial());
										MeshRM::Instance()->LoadMeshCache(mesh);
									}
								}
							}
						} else {
							if (!proceed) {
								if (!IsMeshInView(pWO->m_meshObject)) {
									if (pWO->m_runtimeParticleId)
										m_particleRM->SetRuntimeEffectStatus(pWO->m_runtimeParticleId, -1, 0, 1);
									pWO->m_culled = TRUE;
									continue;
								}
							}

							ManageEnvEffects(pWO, pWO->m_meshObject);

							pWO->m_culled = FALSE;

							if (GetParticlesEnabled() && (pWO->m_particleLink > -1)) {
								// particle expansion
								if (pWO->m_runtimeParticleId) {
									m_particleRM->SetRuntimeEffectStatus(pWO->m_runtimeParticleId, -1, 1, 1);
								} else {
									// create new
									EnvParticleSystem* pSys = m_particleRM->GetStockItemByIndex(pWO->m_particleLink);
									if (pSys) {
										// ref db
										UINT id = m_particleRM->AddRuntimeEffect(pSys->m_systemName);
										pWO->m_runtimeParticleId = id;

										m_particleRM->SetRuntimeEffectStatus(pWO->m_runtimeParticleId, 1, -1, -1);

										Matrix44f genMatrix;
										genMatrix.MakeTranslation(pWO->m_meshObject->GetMeshCenter());

										Vector3f focusPoint = pWO->m_meshObject->GetMeshCenter() - pWO->m_particleDirection;
										MatrixARB::FaceTowardPoint(focusPoint, &genMatrix);

										m_particleRM->SetRuntimeEffectMatrix(pWO->m_runtimeParticleId, genMatrix);
										m_curWldObjPrtList->AddTail(pWO); // for persistence
									} // end ref db
								} // end create new
							} // end particle expansion

							// use legacy renderer
							if (pWO->m_ReMesh == NULL) {
								CategorizeMesh(pWO->m_meshObject,
									camTransform,
									pWO->m_meshObject->GetMeshCenter(),
									camTransformInverse,
									pWO->m_meshObject->GetMeshCenter(),
									NULL,
									pWO->GetCustomMaterial());
							} else {
								if (!pWO->m_meshObject->m_Rendered)
									return true;

								CMaterialObject* matPtr = pWO->GetCustomMaterial() == NULL ?
															  pWO->m_meshObject->m_materialObject :
															  pWO->GetCustomMaterial();

								matPtr->Callback();

								ReMesh* mesh = pWO->GetReMesh();

								if (mesh) {
									mesh->SetMaterial(matPtr->GetReMaterial());
									MeshRM::Instance()->LoadMeshCache(mesh);
								}
							}
						}
					}
				}
			} // end for - world object render loop
		}
	}

	// remove link if still set to non regen
	if (GetParticlesEnabled()) {
		POSITION prtPosLast;
		for (POSITION prtPos = m_curWldObjPrtList->GetHeadPosition(); (prtPosLast = prtPos) != NULL;) {
			auto pWO = (CWldObject*)m_curWldObjPrtList->GetNext(prtPos);
			if (pWO->m_runtimeParticleId) {
				int bRegen = 0;
				if (m_particleRM->GetRuntimeEffectStatus(pWO->m_runtimeParticleId, NULL, NULL, &bRegen) && !bRegen) {
					m_particleRM->SetRuntimeEffectStatus(pWO->m_runtimeParticleId, -1, 0, -1);
					pWO->m_runtimeParticleId = 0; // unlink this cause its gunna auto delete
					m_curWldObjPrtList->RemoveAt(prtPosLast);
				}
			}
		}
	}

	return true;
}

bool ClientEngine::GenRenderList_MovementObjects(CViewportObj* pVO, const ScenePassOptions& aPassOptions) {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "GenRenderList_Entities");

	if (!pVO)
		return false;

	// Get My Avatar
	auto pMO_me = GetMovementObjMe();

	// Get Viewport Camera Transform
	Matrix44f camTransform = pVO->m_camWorldMatrix;
	Matrix44f camTransformInverse = pVO->m_viewMatrix;
	Vector3f camPosition = pVO->m_camWorldPosition;

	// Insert Entities Into Vector For Sorting By Distance
	std::vector<std::pair<float, CMovementObj*>> pMO_sort;
	for (POSITION pos = m_movObjList->GetHeadPosition(); pos;) {
		auto pMO = (CMovementObj*)m_movObjList->GetNext(pos);
		if (!pMO || !pMO->getRender())
			continue;

		auto pRS = pMO->getSkeleton();
		if (!pRS)
			continue;

		// Reset Graphics Options (assuming entity is culled)
		const FLOAT lodDistCulled = 10000.0; // culled objects
		const TimeMs animDelayCulled = (TimeMs)10000.0; // culled objects
		DWORD lodNumCulled;
		DWORD lodCulled = pMO->queryLOD(lodDistCulled, lodNumCulled);
		pRS->setCurrentLOD(lodCulled);
		pRS->setAnimUpdateDelayMs(animDelayCulled);

		if (!pMO->isVisible() || pMO->getMountType() == MT_STUNT)
			continue;

		// Get current transformation matrix
		const Matrix44f& positionMatrix = pMO->getBaseFrameMatrix();

		// Check asset status
		bool assetsLoaded = pMO->assetsLoaded();
		bool bSubstituteLoaded = pMO->hasSubstitute();
		if (!assetsLoaded) {
			// Asset not loaded -- try render with a substitute
			if (!bSubstituteLoaded && pMO->isSubstituteAllowed())
				loadMovementObjectSubstitute(pMO);
			moveEntitySubstitute(pMO, positionMatrix.GetTranslation(), positionMatrix.GetRowZ());
			continue;
		} else if (assetsLoaded && bSubstituteLoaded) {
			removeMovementObjectSubstitute(pMO);
			pMO->setSubstituteAllowed(false); // Only use substitute once
		}

		pRS->setCulled(true);
		pRS->setAnimUpdated(true);

		// get camera distance of RuntimeSkeleton
		float distanceToCam = Distance(positionMatrix.GetTranslation(), camTransform.GetTranslation());
		float camFovX = pVO->GetCameraFovX();
		distanceToCam = distanceToCam * camFovX;
		pMO->setDistanceToCamTemp(distanceToCam);
		pMO->setInFov(false);

		// Avatar Within Camera Distance View ?
		if (distanceToCam >= GetCullDist_MovementObjects())
			continue;

		// Determine Avatar Bounding Box
		ABBOX bndBox;
		bndBox.maxX = bndBox.maxZ = pMO->getBoundingRadius();
		bndBox.maxY = pMO->getBoundingRadius();
		bndBox.minX = bndBox.minZ = -bndBox.maxZ;
		bndBox.minY = -bndBox.maxY;

		// Avatar Inside Camera Field Of View ?
		if (OutOfFovUntransformed(positionMatrix, bndBox))
			continue;

		pRS->setCulled(false);

		// Update Avatar Animation (from keyboard)
		if (!pMO->isNetworkEntity()) {
			EntityAnimationManagementCallback(pMO);
			if (pMO->getPossedBy() && pMO->getMountType() == MT_STUNT)
				EntityAnimationManagementCallback(pMO->getPossedBy());
		}

		// Update effect track.  Is this the correct place to do it?  This doesn't animate anything, it
		// just sets current animation, particle effect and sound based on the time and content of the
		// effect track for this CMovementObj
		pMO->updateEffectTrack(fTime::TimeMs());

		// Avatar Has Valid Animation ?
		if (!pRS->isAnimInUseValid() && !IS_UGC_GLID(pMO->getUGCActorGlobalId()))
			continue;

		// Add Avatar To List For Sorting By Distance To Camera
		pMO_sort.push_back(std::make_pair(pMO->getDistanceToCamTemp(), pMO));
	}

	// Nothing To Render ?
	auto numEntities = pMO_sort.size();
	if (!numEntities)
		return TRUE;

	// Sort By Distance
	std::sort(pMO_sort.begin(), pMO_sort.end());

	// Get Graphics Options
	int lodBias, animBias, avatarNames;
	bool lodBiasAuto, animBiasAuto, avatarNamesAuto;
	GetLodBias_MovementObjects(lodBias, lodBiasAuto);
	GetAnimationBias_MovementObjects(animBias, animBiasAuto);
	GetAvatarNames(avatarNames, avatarNamesAuto);

	// ED-5427 - Auto Graphics Options
	// Automatically adjust graphics options based on FPS (2hz max)
	// Priorities:
	// 1. Animation Bias (10 -> 1)
	// 2. Avatar LOD Bias (10 -> 1)
	// 3. Avatar Names (1)
	static int s_animBias = 1;
	static int s_lodBias = 1;
	static int s_avatarNames = 1;
	static Timer s_timerAuto;
	auto fps = RenderMetrics::GetFramesPerSec(true); // filtered
	bool tooSlow = (fps < 15.0);
	bool tooFast = (fps > 20.0);
	bool tooSoon = (s_timerAuto.ElapsedMs() < 500.0);
	if (!tooSoon && tooSlow) {
		// Too Slow - Decrease Quality
		if (animBiasAuto && (s_animBias > 1)) {
			--s_animBias;
			LogInfo("SLOW - fps=" << fps << " entities=" << numEntities << " animBias=" << s_animBias);
			s_timerAuto.Reset();
		} else if (lodBiasAuto && (s_lodBias > 1)) {
			--s_lodBias;
			LogInfo("SLOW - fps=" << fps << " entities=" << numEntities << " lodBias=" << s_lodBias);
			s_timerAuto.Reset();
		}
	} else if (!tooSoon && tooFast) {
		// Too Fast - Increase Quality
		if (lodBiasAuto && (s_lodBias < 10)) {
			++s_lodBias;
			LogInfo("FAST - fps=" << fps << " entities=" << numEntities << " lodBias=" << s_lodBias);
			s_timerAuto.Reset();
		} else if (animBiasAuto && (s_animBias < 10)) {
			++s_animBias;
			LogInfo("FAST - fps=" << fps << " entities=" << numEntities << " animBias=" << s_animBias);
			s_timerAuto.Reset();
		}
	}
	if (animBiasAuto)
		animBias = s_animBias;
	else
		s_animBias = animBias;
	if (lodBiasAuto)
		lodBias = s_lodBias;
	else
		s_lodBias = lodBias;
	if (avatarNamesAuto)
		avatarNames = s_avatarNames;

	// Determine Avatar Level Of Detail Allocation Buckets
	static const DWORD s_lodBuckets[10][SKELETAL_LODS_MAX] = {
		{ 0, 1, 2, 3, 4, 1000 },
		{ 2, 3, 4, 5, 6, 1000 },
		{ 4, 5, 6, 7, 8, 1000 },
		{ 6, 7, 8, 9, 10, 1000 },
		{ 8, 9, 10, 11, 12, 1000 },
		{ 10, 11, 12, 13, 14, 1000 },
		{ 12, 13, 14, 15, 16, 1000 },
		{ 14, 15, 16, 17, 18, 1000 },
		{ 16, 17, 18, 19, 20, 1000 },
		{ 100, 100, 100, 100, 100, 1000 }
	};
	lodBias = Clamp(lodBias - 1, 0, 9);
	const DWORD* lodBucket = s_lodBuckets[lodBias];
	DWORD lodBucketCount[SKELETAL_LODS_MAX] = { 0 };

	// Determine Animation Delay Allocation Buckets
	static const DWORD s_animDelayBuckets[10][SKELETAL_LODS_MAX] = {
		{ 0, 1, 2, 3, 4, 1000 },
		{ 2, 3, 4, 5, 6, 1000 },
		{ 4, 5, 6, 7, 8, 1000 },
		{ 6, 7, 8, 9, 10, 1000 },
		{ 8, 9, 10, 11, 12, 1000 },
		{ 10, 11, 12, 13, 14, 1000 },
		{ 12, 13, 14, 15, 16, 1000 },
		{ 14, 15, 16, 17, 18, 1000 },
		{ 16, 17, 18, 19, 20, 1000 },
		{ 100, 100, 100, 100, 100, 1000 }
	};
	static const TimeMs s_animDelay[SKELETAL_LODS_MAX] = {
		15.0, 30.0, 60.0, 90.0, 120.0, 150.0 // 15ms is approx 60hz
	};
	animBias = Clamp(animBias - 1, 0, 9);
	const DWORD* animDelayBucket = s_animDelayBuckets[animBias];
	DWORD animDelayBucketCount[SKELETAL_LODS_MAX] = { 0 };

	// Determine Max Avatar Name Labels
	static const DWORD s_avatarNameLabels[10] = {
		0, 1, 2, 4, 8, 16, 32, 64, 128, 256
	};
	avatarNames = Clamp(avatarNames - 1, 0, 9);
	DWORD numLabels = s_avatarNameLabels[avatarNames];

	// Allocate Avatar LODs
	DWORD lodBucketCurrent = 0;
	for (DWORD i = 0; i < numEntities; i++) {
		auto pMO = pMO_sort[i].second;
		if (!pMO)
			continue;

		auto pRS = pMO->getSkeleton();
		if (!pRS)
			continue;

		// My Avatar Always Looks Great
		bool isMe = (pMO == pMO_me);
		if (isMe || !pMO->getSkeletonLODList()) {
			pRS->setCurrentLOD(0);
			continue;
		}

		// Find Next Free Bucket
		DWORD numLOD, lodToUse;
		DWORD lodBucketEditor = pMO->queryLOD(pMO->getDistanceToCamTemp(), numLOD);
		lodBucketCurrent = (lodBucketCurrent > lodBucketEditor) ? lodBucketCurrent : lodBucketEditor;
		while ((lodBucketCurrent < SKELETAL_LODS_MAX) && (lodBucketCount[lodBucketCurrent] >= lodBucket[lodBucketCurrent]))
			lodBucketCurrent++;
		lodBucketCurrent = Clamp(lodBucketCurrent, 0, SKELETAL_LODS_MAX - 1);
		lodBucketCount[lodBucketCurrent]++;

		// Assign Avatar LOD
		lodToUse = (lodBucketCurrent > (DWORD)numLOD - 1) ? numLOD - 1 : lodBucketCurrent;
		lodToUse = Clamp(lodToUse, 0, SKELETAL_LODS_MAX - 1);
		pRS->setCurrentLOD(lodToUse);
	}

	// Allocate Avatar Animation Delays
	DWORD animDelayCurrent = 0;
	for (DWORD i = 0; i < numEntities; i++) {
		auto pMO = pMO_sort[i].second;
		if (!pMO)
			continue;

		auto pRS = pMO->getSkeleton();
		if (!pRS)
			continue;

		// My Avatar Always Animates Smoothly (minimum update delay)
		bool isMe = (pMO == pMO_me);
		if (isMe) {
			pRS->setAnimUpdateDelayMs(ANIM_UPDATE_DELAY_MS_MIN);
			continue;
		}

		// Find Next Free Bucket
		if ((animDelayCurrent < SKELETAL_LODS_MAX) && (animDelayBucketCount[animDelayCurrent] >= animDelayBucket[animDelayCurrent]))
			animDelayCurrent++;
		animDelayCurrent = Clamp(animDelayCurrent, 0, SKELETAL_LODS_MAX - 1);
		animDelayBucketCount[animDelayCurrent]++;

		// Set Avatar Animation Update Delay
		pRS->setAnimUpdateDelayMs(s_animDelay[animDelayCurrent]);
	}

	m_inViewEntityList->SafeDelete();

	// render the bone upload group
	for (DWORD i = 0; i < numEntities; i++) {
		auto pMO = pMO_sort[i].second;
		if (!pMO)
			continue;

		auto pRS = pMO->getSkeleton();
		if (!pRS)
			continue;

		CacheEntityForShadow(pMO);
		pMO->setInFov(true);

		// Update Avatar Animation (and particles)
		if (GetAnimationsEnabled()) {
			if (pMO && pMO->getBaseFrame())
				updateAnimations(pRS, pMO->getBaseFrameMatrix(), pMO->isFirstPersonMode(), true);
		}

		// scale the world matrix
		Matrix44f positionMatrix = pMO->getBaseFrameMatrix();
		const Vector3f& scaleVector = pRS->getScaleVector();
		positionMatrix[0] *= scaleVector.x;
		positionMatrix[1] *= scaleVector.y;
		positionMatrix[2] *= scaleVector.z;

		GenRenderList_Attachments(pVO, aPassOptions, positionMatrix, pRS, pMO->isFirstPersonMode());

		// update bones & add to mesh cache if NOT in world alpha sort array
		if (!pMO->isFirstPersonMode())
			pRS->update(positionMatrix);
		else
			pRS->setWorldMatrixOnly(positionMatrix);
	}

	// display text on player if requested
	if (m_showAllNames) {
		// render the character labels in 2 passes...the first pass does NOT render labels on entities near edge of screen
		UINT numLabelsrendered = 0;
		for (DWORD i = 0; i < numEntities; i++) {
			auto pMO = pMO_sort[i].second;
			if (!pMO)
				continue;

			auto pRS = pMO->getSkeleton();
			if (!pRS)
				continue;

			bool notFirstPerson = !pMO->isFirstPersonMode();
			bool labelsOk = ZoneAllowsLabels();
			bool labelMe = labelsOk && (pMO == pMO_me) && numLabels;
			bool numLabelsOk = labelsOk && ((numLabelsrendered + 1) < numLabels);
			bool isMouseOverEntity = (m_mouseOver.objType == ObjectType::MOVEMENT) && (pMO->getNetTraceId() == m_mouseOver.objId);
			if (notFirstPerson && (labelMe || numLabelsOk || isMouseOverEntity)) {
				// transform 3d position of label into clip space
				Vector3f targetLoc = pMO->getHeadPosition();
				Vector4f screenLoc = TransformPointToHomogeneousVector(
					*reinterpret_cast<const Matrix44f*>(GetReDeviceState()->GetRenderState()->GetProjView()),
					targetLoc);
				if (screenLoc.w != 0.f) {
					FLOAT denom = 1.f / screenLoc.w;
					float screenx = screenLoc.x * denom;
					float screeny = screenLoc.y * denom;
					float screenz = screenLoc.z * denom;

					// reject if too close to edge of screen or in back of it
					if (screenx < -.9f || screenx > .9f || screeny < -.9f || screeny > .9f || screenz < 0.1f || screenz > .992f)
						continue;
				}

				GenRenderList_SkeletonLabelHelper(pVO, aPassOptions, pRS);
				numLabelsrendered++;
			}
		}
	}

	return true;
}

void ClientEngine::GenRenderList_SkeletonLabelHelper(CViewportObj* pVO, const ScenePassOptions& aPassOptions, RuntimeSkeleton* pRS) {
	if (!pRS)
		return;

	auto pSL = pRS->getOverheadLabels();
	if (!pSL)
		return;

	Vector3f textPos = pRS->getWorldMatrix().GetTranslation();
	textPos.y += pRS->getLocalBoundingBox().max.Y() * pRS->getScaleVector().Y() + 1.5f;

	pSL->setCameraMatrix(pVO->m_camWorldMatrix);
	pSL->render(textPos);
}

bool ClientEngine::GenRenderList_Environment(CViewportObj* pVO, const ScenePassOptions& aPassOptions) {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "GenRenderList_Environment");

	Matrix44f camTransform = pVO->m_camWorldMatrix;
	Matrix44f camTransformInverse = pVO->m_viewMatrix;
	Vector3f camPosition = pVO->m_camWorldPosition;

	if (m_environment) {
		Matrix44f soundCenterMatrix; // drf - added - deprecated
		soundCenterMatrix.MakeIdentity(); // drf - added - deprecated
		m_environment->CallBack(
			g_pD3dDevice,
			&m_backgroundColorGlobal,
			m_lightningSystem,
			soundCenterMatrix,
			m_soundManager,
			m_bRenderEnvironment,
			m_pRenderStateMgr,
			m_visualsRenderList);

		if (m_bRenderEnvironment && m_environment->m_environmentTimeSystem &&
			m_environment->m_environmentTimeSystem->m_pDSO &&
			(m_environment->m_environmentTimeSystem->m_pDSO->m_sunColorRed != 0.0f ||
				m_environment->m_environmentTimeSystem->m_pDSO->m_sunColorGreen != 0.0f ||
				m_environment->m_environmentTimeSystem->m_pDSO->m_sunColorBlue != 0.0f) &&
			m_environment->m_environmentTimeSystem->m_visual) {
			// s visual
			m_environment->m_environmentTimeSystem->m_currentSunPosition;
			m_environment->m_environmentTimeSystem->m_visual->ResetWorldTransform();

			Vector3f newCenter = m_environment->m_environmentTimeSystem->m_currentSunPosition + camTransform.GetTranslation();

			m_environment->m_environmentTimeSystem->m_visual->SetMeshCenter(newCenter);

			if (m_environment->m_environmentTimeSystem->m_visual->m_charona)
				m_visualsRenderList->AddTail(m_environment->m_environmentTimeSystem->m_visual);
		}
	}
	return true;
}

struct OneDynObjArgs {
	Simd4f m_v4CamPos;
	float m_fAnimDelay;
	float m_fMeshDiameterThreshold;
	float m_fCullRadiusTransitionDistance;
	float m_fDORelativeCullRadiusNear;
	float m_fDORelativeCullRadiusFar;
	bool m_bCullMeshesByScreenSize;
};

bool ClientEngine::GenRenderList_DynamicSystem(CViewportObj* pVO, const ScenePassOptions& aPassOptions) {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "GenRenderList_DynamicSystem");

	if (!pVO)
		return false;

	// Get Graphics Options
	int animBias;
	bool animBiasAuto;
	TimeMs animDelay = 15.0;
	{
		GetAnimationBias_MovementObjects(animBias, animBiasAuto);

		// Update Dynamic Object Animation
		// ED-5427 - Auto Graphics Options
		// Automatically adjust graphics options based on FPS (2hz max)
		// Priorities:
		// 1. Animation Bias (10 -> 1)
		static int s_animBias = 1;
		static Timer s_timerAuto;
		auto fps = RenderMetrics::GetFramesPerSec(true); // filtered
		bool tooSlow = (fps < 20.0);
		bool tooFast = (fps > 25.0);
		bool tooSoon = (s_timerAuto.ElapsedMs() < 500.0);
		if (!tooSoon && tooSlow) {
			// Too Slow - Decrease Quality
			if (animBiasAuto && (s_animBias > 1)) {
				--s_animBias;
				LogInfo("SLOW - fps=" << fps << " animBias=" << s_animBias);
				s_timerAuto.Reset();
			}
		} else if (!tooSoon && tooFast) {
			// Too Fast - Increase Quality
			if (animBiasAuto && (s_animBias < 10)) {
				++s_animBias;
				LogInfo("FAST - fps=" << fps << " animBias=" << s_animBias);
				s_timerAuto.Reset();
			}
		}
		if (animBiasAuto)
			animBias = s_animBias;
		else
			s_animBias = animBias;

		// Determine Animation Delay
		static const TimeMs s_animDelay[10] = {
			15.0, 30.0, 45.0, 60.0, 90.0, 120.0, 150.0, 300.0, 500.0, 1000.0 // 15ms is approx 60hz
		};
		animBias = Clamp(animBias - 1, 0, 9);
		animDelay = s_animDelay[9 - animBias];
	}

	float fMeshRelativeDiameterThreshold = 0;

//#define USE_LEGACY_CULLING
#if !defined(USE_LEGACY_CULLING)
	float fDORelativeCullRadiusFar = 0;
#else
	float fRelativeCullRadiusLegacy = 0;
#endif
	if (!aPassOptions.disableDistanceCulling) {
		// Get Graphics Options
		int doCullBias;
		bool doCullBiasAuto;
		GetCullBias_DynamicObjects(doCullBias, doCullBiasAuto);

		// ED-5427 - Auto Graphics Options
		// Automatically adjust graphics options based on FPS (1hz max)
		// - Object Visibility (10 -> 3) no one likes <3 ever
		static int s_doCullBias = 3;
		static Timer s_timerAuto;
		auto fps = RenderMetrics::GetFramesPerSec(true); // filtered
		bool tooSlow = (fps < 20.0);
		bool tooFast = (fps > 50.0);
		bool tooSoon = (s_timerAuto.ElapsedMs() < 1000.0);
		if (!tooSoon && tooSlow) {
			// Too Slow - Decrease Quality
			if (doCullBiasAuto && (s_doCullBias > 3)) {
				--s_doCullBias;
				LogInfo("SLOW - fps=" << fps << " doCullBias=" << s_doCullBias);
				s_timerAuto.Reset();
			}
		} else if (!tooSoon && tooFast) {
			// Too Fast - Increase Quality
			if (doCullBiasAuto && (s_doCullBias < 10)) {
				++s_doCullBias;
				LogInfo("FAST - fps=" << fps << " doCullBias=" << s_doCullBias);
				s_timerAuto.Reset();
			}
		}
		if (doCullBiasAuto)
			doCullBias = s_doCullBias;
		else
			s_doCullBias = doCullBias;
		doCullBias = Clamp(doCullBias, 1, 10);

#if !defined(USE_LEGACY_CULLING)
		// cull the DO by distance and radius from my avatar
		// Diameter threshold: tan(halfFOV) * camDistance / (500 pixels / 10 pixels * cullingFactor)
		/*
		const int maxQual = 10;
		double dTangentHalfFOVY = tan(0.5 * pVO->GetCameraFovY());
		double dHalfHeight = 0.5 * pVO->GetHeight();
		double thresholdCoeff = dTangentHalfFOVY / dHalfHeight;
		fMeshRelativeDiameterThreshold = thresholdCoeff * (float(maxQual) / min(maxQual, doCullBias));
		*/

		static const float afDORelativeCullRadius[10] = {
			//0.35f, 0.2f,
			0.15f,
			0.1f,
			0.07f,
			0.05f,
			0.035f,
			0.02f,
			0.015f,
			0.01f,
			0.007f,
			0.005f,
		};
		fDORelativeCullRadiusFar = afDORelativeCullRadius[doCullBias - 1];
		fMeshRelativeDiameterThreshold = fDORelativeCullRadiusFar * .25f;
#else
		const float distAdjust = 5.0f;
		// cull the DO by distance and radius from my avatar
		// Diameter threshold: tan(halfFOV) * camDistance / (500 pixels / 10 pixels * cullingFactor)
		// tan(halfFOV) / (1000 pixels / 2). Assumptions: FOV = 100 deg, spanning across 1000 pixels on screen
		const int maxQual = 10;
		static const double thresholdCoeff = tan(M_PI * 100 / 180.0 / 2) / (1000.0 / 2);
		//double diameterThreshold = thresholdCoeff * camDist * (maxQual / min(maxQual, doCullBias));
		fMeshRelativeDiameterThreshold = thresholdCoeff * (maxQual / min(maxQual, doCullBias));

		const float distAdjust = 5.0f;
		fRelativeCullRadiusLegacy = 1 / (distAdjust * doCullBias * doCullBias);
#endif
	}

	OneDynObjArgs args;
	args.m_v4CamPos.Set(pVO->m_camWorldPosition, 0.0f);
	args.m_fAnimDelay = animDelay;
	args.m_fMeshDiameterThreshold = fMeshRelativeDiameterThreshold;
	static float s_fCullRadiusTransitionDistance = 200.0f;
	args.m_fCullRadiusTransitionDistance = s_fCullRadiusTransitionDistance;
#if !defined(USE_LEGACY_CULLING)
	static float s_fDORelativeCullRadiusNear = 0.01f;
	args.m_fDORelativeCullRadiusNear = (std::min)(s_fDORelativeCullRadiusNear, fDORelativeCullRadiusFar);
	args.m_fDORelativeCullRadiusFar = fDORelativeCullRadiusFar;
#else
	args.m_fDORelativeCullRadiusNear = fDORelativeCullRadiusLegacy;
	args.m_fDORelativeCullRadiusFar = fDORelativeCullRadiusLegacy;
#endif
	args.m_bCullMeshesByScreenSize = !aPassOptions.disableDistanceCulling;

	m_dynamicPlacementManager.IterateObjectsWithMedia([this, &args, &aPassOptions, pVO](CDynamicPlacementObj* pDPO) {
		if (!pDPO)
			return;

		StreamableDynamicObject* pBaseObj = pDPO->GetBaseObj();
		RuntimeSkeleton* pRS = pDPO->getSkeleton();
		if (!pBaseObj || !pRS)
			return;

		float fCamDist = Distance(pVO->m_camWorldPosition, pRS->getWorldMatrix().GetTranslation());
		bool bCulled = this->CullDynamicObject(pDPO, pRS, aPassOptions, fCamDist, args);
		if (!bCulled) {
			size_t i = 0;
			pRS->IterateMeshes(0, [this, pBaseObj, pDPO, &i](ReMesh* pMesh) {
				// load ReMaterial texture pointer with video texture
				if (pBaseObj->VisualMediaSupported(i++)) {
					int placementId = pDPO->GetPlacementId();

					// Get Object Media Params
					const MediaParams& mediaParams = pDPO->GetMediaParams();
					bool isGlobal = mediaParams.IsRadiusGlobal();
					bool isTypePlayer = (pDPO->ObjType() == DYN_OBJ_TYPE::MEDIA_PLAYER);

					// Get Object Media Mesh
					IExternalEXMesh* pXM = GetMediaMeshOnDynamicObject(placementId);

					// Dupe Valid Global Player Mesh To Unassigned Global Players ?
					bool isDupe = false;
					if (!pXM && isGlobal && isTypePlayer && m_mediaDupeObjId) {
						isDupe = true;
						pXM = GetMediaMeshOnDynamicObject(m_mediaDupeObjId);
						if (!pXM)
							m_mediaDupeObjId = 0;
					}

					// Valid Media Mesh ?
					if (GetMediaScrapeEnabled() && pXM) {
						// Remember Global Media Players (not frames or games)
						if (!isDupe && isGlobal && isTypePlayer && !m_mediaDupeObjId)
							m_mediaDupeObjId = placementId;

						// Scrape Media Mesh (self-metered)
						pXM->ScrapeTexture(m_pReDeviceState);

						// Assign Media Mesh To Object Mesh
						auto pRM_media = pXM->GetReMaterial();
						if (pRM_media) {
							pMesh->SetMaterial(pRM_media);
							MeshRM::Instance()->LoadMeshCache(pMesh);
							return;
						}
					}
					// Media stopped or failed - reapply non-media material
					pDPO->getSkeleton()->applyMaterials(i - 1);
				}
			});
		}
	});

	// Render All DOs
	IterateObjects([this, &pVO, &aPassOptions, &args](CDynamicPlacementObj* pDPO) {
		GenRenderList_OneDynamicObject(pVO, aPassOptions, pDPO, args);
	});

	// Render bounding boxes.
	if (m_renderBoundingBoxForAllDynamicObjects) {
		m_dynamicPlacementManager.IterateObjects([this](CDynamicPlacementObj* pDPO) {
			if (pDPO->IsBoundingBoxRenderEnabled()) {
				ReMesh* boxMesh = pDPO->GetBoundingBoxMesh();
				boxMesh->SetWorldMatrix(pDPO->GetBoundingBoxMatrix());
				MeshRM::Instance()->LoadMeshCache(boxMesh);
			}
		});
	} else {
		m_dynamicPlacementManager.IterateObjectsForBoundingBoxRender([this](CDynamicPlacementObj* pDPO) {
			ReMesh* boxMesh = pDPO->GetBoundingBoxMesh();
			boxMesh->SetWorldMatrix(pDPO->GetBoundingBoxMatrix());
			MeshRM::Instance()->LoadMeshCache(boxMesh);
		});
	}

	// Update light list
	if (m_bLightListDirty) {
		// Lights don't change enough to justify having their own list.
		//m_dynamicPlacementManager.IterateObjectsWithLights([this](CDynamicPlacementObj* pDPO) {
		m_dynamicPlacementManager.IterateObjects([this](CDynamicPlacementObj* pDPO) {
			if (!pDPO)
				return;

			CRTLightObj* pLight = pDPO->GetLight();
			if (!pLight)
				return;

			if (!pDPO->IsVisible())
				return;

			RuntimeSkeleton* pRS = pDPO->getSkeleton();
			StreamableDynamicObject* pBaseObj = pDPO->GetBaseObj();
			if (!pRS || !pBaseObj)
				return;

			const Vector3f& rootBonePos = pRS->getRootBonePosition();
			Vector3f translatedLightPosition = pBaseObj->getLightPosition() + rootBonePos;
			m_runtimeLightSys->AddLight(pLight, translatedLightPosition);
		});
	}

	return true;
}

bool ClientEngine::CullDynamicObject(CDynamicPlacementObj* pDPO, RuntimeSkeleton* pRS, const ScenePassOptions& aPassOptions, float fCamDist, const OneDynObjArgs& args) {
	if (!pDPO || !pRS)
		return true;

	float boundingRadius = pRS->getWorldRadius();
	if (boundingRadius < aPassOptions.minRadiusFilter)
		return true;

	if (fCamDist > pDPO->GetMinDrawDistanceOverride()) {
		float fRelativeCullRadius = fCamDist < args.m_fCullRadiusTransitionDistance ? args.m_fDORelativeCullRadiusNear : args.m_fDORelativeCullRadiusFar;
		if (boundingRadius < fCamDist * fRelativeCullRadius)
			return true;
	}

	// Cull objects out of camera view
	const Vector3f& ptBoxCenter = pRS->getWorldBoxCenter();
	const Vector3f& ptBoxHalfSize = pRS->getWorldBoxHalfSize();
	if (OutOfFOV_CenterHalfSize(ptBoxCenter, ptBoxHalfSize))
		return true;

	return false;
}

void ClientEngine::GenRenderList_OneDynamicObject(
	CViewportObj* pVO,
	const ScenePassOptions& aPassOptions,
	CDynamicPlacementObj* pDPO,
	const struct OneDynObjArgs& args) {
	auto pRS = pDPO->getSkeleton();
	if (!pRS || !pDPO->IsVisible() || !pRS->areAllMeshesLoaded())
		return;

	if (pRS->getBoneList() != nullptr || pRS->getEquippableItemCount() > 0) {
		if (GetAnimationsEnabled()) {
			// Set Animation Delay
			pRS->setAnimUpdateDelayMs(args.m_fAnimDelay);

			// Update Animation of DPO as well as equipped items.
			DynamicObjectAnimationManagementCallback(pDPO);
			updateAnimations(pRS, pRS->getWorldMatrix(), false, false);
		}
	}

	// Update Built-In Particle Effects
	// Update positions for bone-attached particles (particles attached by server APIs)
	if (pRS->hasParticles()) {
		if (GetParticlesEnabled())
			SkeletalParticleEmitterCallback(pRS->getWorldMatrix(), pRS, false);
	}

	// Frustum and distance culling
	const Simd4f& ptCamera = args.m_v4CamPos;
	const Simd4f& ptBoxCenter = pRS->getWorldBoxCenterSimd();
	Simd4f vCamBoxCenter = ptBoxCenter - ptCamera;

	// Use center of bounding box for camera distance.
	float fCamDistSq = vCamBoxCenter.AsVector3f().LengthSquared();
	float fCamDist = Sqrt_Approx(fCamDistSq);
	bool bCulled = CullDynamicObject(pDPO, pRS, aPassOptions, fCamDist, args);
	pDPO->m_culled = bCulled;
	if (bCulled)
		return;

	// TODO: Set ReMesh diameter for actor-based dynamic objects
	bool bCullMeshesByScreenSize = args.m_bCullMeshesByScreenSize;

	float fMeshDiameterThreshold = args.m_fMeshDiameterThreshold;
	pRS->setCurrentLODFromDistance(fCamDist);

	if (!pRS->isMeshSortValid())
		pRS->sortMeshCache();

	pRS->IterateMeshes(pRS->getCurrentLOD(), [pDPO, pRS, fMeshDiameterThreshold, bCullMeshesByScreenSize](ReMesh* pMesh) {
		if (!pMesh)
			return; // CMovementObj derived skeletons have embedded nulls.

		if (bCullMeshesByScreenSize) {
			ReMeshData* reMeshData = pMesh->GetMeshData();
			if (reMeshData->Diameter() < fMeshDiameterThreshold)
				return;
		}

		if (pDPO->isPrimaryAnimationSettingsValid())
			pMesh->SetBones(pRS->isAnimUpdated() ? pRS->getBoneList()->getBoneMatrices() : nullptr);

		MeshRM::Instance()->LoadMeshCache(pMesh);
	});

	// Render attachments on skeleton
	if (pRS->getArmedInventoryList())
		GenRenderList_Attachments(pVO, aPassOptions, pRS->getWorldMatrix(), pRS, false);

	// Render labels if specified
	if (!pRS->getOverheadLabels()->isEmpty())
		GenRenderList_SkeletonLabelHelper(pVO, aPassOptions, pRS);
}

void ClientEngine::GenRenderList_Attachments(CViewportObj* pVO, const ScenePassOptions& aPassOption, const Matrix44f& aParentMatrix, RuntimeSkeleton* pRS_parent, bool abFirstPersonMode) {
	// render equippable items BEFORE the skeleton
	std::vector<CArmedInventoryObj*> allEquips;
	pRS_parent->getAllEquippableItems(allEquips);

	for (const auto& pAIO : allEquips) {
		if (!pAIO)
			continue;

		// Load new animations for accessories
		auto pRS = pAIO->getSkeleton(abFirstPersonMode ? eVisualType::FirstPerson : eVisualType::ThirdPerson);
		if (!pRS || !pRS->areAllMeshesLoaded())
			continue;

		// copy over the LOD & scale vectors
		const double armedDelayFactor = 2.0;
		pRS->setCurrentLOD(pRS_parent->getCurrentLOD());
		pRS->setScaleVector(pRS_parent->getScaleVector());
		pRS->setAnimUpdateDelayMs(pRS_parent->getAnimUpdateDelayMs() * armedDelayFactor);

		//if the bone attached is selected then join to that
		//otherwise use the position of the parent actor
		Matrix44f mat = aParentMatrix;
		if (pAIO->getIgnoreParentScale()) {
			mat.GetRowX().Normalize();
			mat.GetRowY().Normalize();
			mat.GetRowZ().Normalize();
		}
		if (pAIO->getAttachedBoneIndex() != -1)
			mat = pRS_parent->getAnimBoneMatrix(pAIO->getAttachedBoneIndex()) * mat;

		const Matrix44f* relMat = pAIO->getRelativeMatrix();
		if (relMat)
			mat = (*relMat) * mat;

		// load the CSkeletonObject meshes, if any
		if (pRS->getSkinnedMeshCount() > 0) {
			pRS->update(mat);
			for (int j = 0; j < pRS->getSkinnedMeshCount(); j++)
				pRS->getMeshCacheEntry(pRS->getCurrentLOD(), j);
		}

		// load the static "hiarchy" meshes stored per-bone
		if (pRS->getBoneList() != nullptr) {
			for (POSITION posLoc = pRS->getBoneList()->GetHeadPosition(); posLoc != NULL;) {
				auto pBO = static_cast<const CBoneObject*>(pRS->getBoneList()->GetNext(posLoc));
				if (!pBO || !pBO->m_boneFrame)
					continue;

				for (const auto& pHM : pBO->m_rigidMeshes) {
					if (!pHM)
						continue;

					auto pHVO = pHM->GetByIndex(pRS_parent->getCurrentLOD());
					if (!pHVO)
						continue;

					pHVO->m_mat = pBO->m_boneFrame->m_frameMatrix * mat;

					if (pHVO->m_mesh && pHVO->m_mesh->m_materialObject && pHVO->m_mesh->m_materialObject->GetReMaterial()) {
						pHVO->m_mesh->m_materialObject->GetReMaterial()->PreLight(FALSE);
						if (!pHVO->GetReMesh())
							continue;

						pHVO->GetReMesh()->SetWorldMatrix(&pHVO->m_mat);
						pHVO->GetReMesh()->SetRadius(pHVO->m_mesh->GetBoundingRadius());
						pHVO->GetReMesh()->SetMaterial(pHVO->m_mesh->m_materialObject->GetReMaterial());
						MeshRM::Instance()->LoadMeshCache(pHVO->GetReMesh());
					}
				}
			}
		}
	}
}

bool ClientEngine::GenRenderList_SoundPlacement(CViewportObj* pVO, const ScenePassOptions& aPassOptions) {
	for (const auto& it : m_soundPlacements) {
		auto pSP = it.second;
		if (!pSP)
			continue;

		// DRF - Some Sound Placements Are Never Shown (attached to player)
		if (!pSP->ShowSoundPlacement())
			continue;

		pSP->GetSoundMesh()->SetWorldMatrix(pSP->GetLocationMat());
		pSP->GetRangeMesh()->SetWorldMatrix(pSP->GetRangeMat());
		pSP->GetInnerMesh()->SetWorldMatrix(pSP->GetLocationMat());
		pSP->GetOuterMesh()->SetWorldMatrix(pSP->GetLocationMat());
		MeshRM::Instance()->LoadMeshCache(pSP->GetSoundMesh());
		MeshRM::Instance()->LoadMeshCache(pSP->GetRangeMesh());
		MeshRM::Instance()->LoadMeshCache(pSP->GetInnerMesh());
		MeshRM::Instance()->LoadMeshCache(pSP->GetOuterMesh());
	}
	return true;
}

bool ClientEngine::GenRenderList_Triggers(CViewportObj* pVO, const ScenePassOptions& aPassOptions) {
	ForEachTrigger([this](CTriggerObject* pTO) {
		if (pTO->IsPendingDelete())
			return;

		if (pTO->GetAttachObjId() == 0 ? m_renderWorldTriggers : m_renderDynamicTriggers) {
			pTO->GetMesh()->SetWorldMatrix(pTO->GetMatrix());
			MeshRM::Instance()->LoadMeshCache(pTO->GetMesh());
		}
	});
	return true;
}

void ClientEngine::GenRenderList_MediaTriggers(CViewportObj* pVO, const ScenePassOptions& aPassOptions) {
	CDynamicPlacementObj::UpdateMediaVisMaterials();

	auto pRM = MeshRM::Instance();
	if (!pRM)
		return;

	m_dynamicPlacementManager.IterateObjectsWithMedia([pRM](CDynamicPlacementObj* pDPO) {
		if (!pDPO)
			return;

		auto vidMesh = pDPO->GetMediaVideoRangeMesh();
		if (vidMesh)
			pRM->LoadMeshCache(vidMesh);

		auto audMesh = pDPO->GetMediaAudioRangeMesh();
		if (audMesh)
			pRM->LoadMeshCache(audMesh);
	});
}

bool ClientEngine::GenRenderList_LWG(CViewportObj* pVO, const ScenePassOptions& aPassOptions) {
	auto pRM = MeshRM::Instance();
	if (!pRM)
		return false;

	for (const auto& pr : m_lwgAllGeoms) {
		LWG::IGeom* pGeom = pr.second;
		if (!pGeom)
			continue;

		std::vector<ReMesh*> reMeshes;
		pGeom->render(reMeshes);
		for (ReMesh* pMesh : reMeshes)
			pRM->LoadMeshCache(pMesh);
	}

	return true;
}

void ClientEngine::OutlinesDeviceInit() {
	HRESULT hr;

	m_pGlowTEX[0] = NULL;
	m_pGlowTEX[1] = NULL;

	if (GetD3DDevice()) {
		LPDIRECT3DSURFACE9 pBackBuffer = NULL;

		// Get the back buffer pointer
		hr = GetD3DDevice()->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer);

		if (pBackBuffer) {
			D3DSURFACE_DESC bbsd;
			ZeroMemory(&bbsd, sizeof(bbsd));

			// Get a surface desc for the back buffer
			hr = pBackBuffer->GetDesc(&bbsd);

			if (SUCCEEDED(hr)) {
				int rtWidth = bbsd.Width / GLOW_DOWNSAMPLE_FACTOR,
					rtHeight = bbsd.Height / GLOW_DOWNSAMPLE_FACTOR;

				// Create 2 render target textures
				// Create the object silhouette texture with same resolution as the back buffer to allow depth testing
				hr = GetD3DDevice()->CreateTexture(bbsd.Width, bbsd.Height, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &m_pGlowTEX[0], NULL);
				if (FAILED(hr)) // try to create with no alpha
					hr = GetD3DDevice()->CreateTexture(bbsd.Width, bbsd.Height, 1, D3DUSAGE_RENDERTARGET, D3DFMT_X8R8G8B8, D3DPOOL_DEFAULT, &m_pGlowTEX[0], NULL);

				if (SUCCEEDED(hr)) {
					hr = GetD3DDevice()->CreateTexture(rtWidth, rtHeight, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &m_pGlowTEX[1], NULL);

					if (FAILED(hr)) // try to create with no alpha
						hr = GetD3DDevice()->CreateTexture(rtWidth, rtHeight, 1, D3DUSAGE_RENDERTARGET, D3DFMT_X8R8G8B8, D3DPOOL_DEFAULT, &m_pGlowTEX[1], NULL);
				}
			}

			SAFE_RELEASE(pBackBuffer);
		}
	}
}

int ClientEngine::OutlinesStencilPass(LPDIRECT3DDEVICE9 pD3D) {
	// Clear stencil buffer
	HRESULT hr = pD3D->Clear(0, NULL, D3DCLEAR_STENCIL, 0, 1.0f, 0);

	hr = pD3D->SetRenderState(D3DRS_LIGHTING, FALSE);

	// Set up stencil state
	hr = pD3D->SetRenderState(D3DRS_STENCILENABLE, TRUE);
	hr = pD3D->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
	hr = pD3D->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_REPLACE);
	hr = pD3D->SetRenderState(D3DRS_STENCILREF, 1);

	// Turn off color buffer writes, zbuffer tests and writes
	hr = pD3D->SetRenderState(D3DRS_COLORWRITEENABLE, 0);
	hr = pD3D->SetRenderState(D3DRS_ZENABLE, FALSE);
	hr = pD3D->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

	// Use alpha blending
	hr = pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	hr = pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	hr = pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	// Use alpha test to discard regions that are sort of transparent,
	// this is totally subjective but needed in some way for objects
	// with alpha to look correct when outlines
	hr = pD3D->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	hr = pD3D->SetRenderState(D3DRS_ALPHAREF, 25);
	hr = pD3D->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);

	hr = pD3D->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	int res = OutlinesRenderObjects(false);

	hr = pD3D->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	hr = pD3D->SetRenderState(D3DRS_ALPHAREF, 0);
	hr = pD3D->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS);

	hr = pD3D->SetRenderState(D3DRS_COLORWRITEENABLE, 0xffffffff);
	hr = pD3D->SetRenderState(D3DRS_STENCILENABLE, FALSE);

	return res;
}

void ClientEngine::OutlinesColorPass(LPDIRECT3DDEVICE9 pD3D) {
	// Clear render target to transparent black
	HRESULT hr = pD3D->Clear(0, NULL, D3DCLEAR_TARGET, D3DXCOLOR(0, 0, 0, 0), 1.0f, 0);

	// Turn off all zbuffer activity since we're just rendering a flat color
	// Outline ZENABLE is determined per object (see OutlinesRenderObjects)
	hr = pD3D->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

	// Turn alpha blending so that transparent geometry and billboards look right
	hr = pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	hr = pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	hr = pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	// Use ambient lighting to render flat color
	hr = pD3D->SetRenderState(D3DRS_AMBIENT, 0xffffffff);
	hr = pD3D->SetRenderState(D3DRS_LIGHTING, TRUE);

	// Disable all lights
	for (DWORD light = 0; light < ReD3DX9DEVICEMAXUPLOADLIGHTS; light++)
		hr = pD3D->LightEnable(light, FALSE);

	// Disable all texture blend stages
	for (DWORD stage = 0; stage < ReDEVICEMAXTEXTURESTAGES; stage++)
		hr = pD3D->SetTexture(stage, NULL);

	OutlinesRenderObjects(true);
}

void ClientEngine::OutlinesBlurPass(LPDIRECT3DDEVICE9 pD3D, LPDIRECT3DSURFACE9 pGlowRT) {
	// Clear the blur render target
	HRESULT hr = pD3D->Clear(0, NULL, D3DCLEAR_TARGET, D3DXCOLOR(0, 0, 0, 0), 1.0f, 0);

	// Build ortho projection matrix, and set world/view matrices to identity
	D3DXMATRIX matIdent, matProj;
	D3DXMatrixIdentity(&matIdent);
	D3DXMatrixOrthoOffCenterLH(&matProj, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f);

	hr = pD3D->SetTransform(D3DTS_WORLD, &matIdent);
	hr = pD3D->SetTransform(D3DTS_VIEW, &matIdent);
	hr = pD3D->SetTransform(D3DTS_PROJECTION, &matProj);

	hr = pD3D->SetRenderState(D3DRS_ZENABLE, FALSE); // Disable depth-test
	hr = pD3D->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);

	hr = pD3D->SetRenderState(D3DRS_AMBIENT, 0xffffffff);

	hr = pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	hr = pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
	hr = pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

	// Set offscreen as source texture
	hr = pD3D->SetTexture(0, m_pGlowTEX[0]);
	hr = pD3D->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	hr = pD3D->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_CURRENT);
	hr = pD3D->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_TEXTURE);
	hr = pD3D->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	hr = pD3D->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_CURRENT);

	// Disable other blend stages
	for (DWORD stage = 1; stage < ReDEVICEMAXTEXTURESTAGES; stage++)
		hr = pD3D->SetTexture(stage, NULL);

	// Set texture address to clamp in both directions so
	// the blur doesn't wrap to the other side of the screen
	hr = pD3D->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	hr = pD3D->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

	hr = pD3D->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	hr = pD3D->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	hr = pD3D->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);

	// Get the glow buffer dimensions
	D3DSURFACE_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	float fx = 0, fy = 0;
	hr = pGlowRT->GetDesc(&sd);
	if (SUCCEEDED(hr)) {
		fx = 1.0f / sd.Width;
		fy = 1.0f / sd.Height;
	}

	// Render 25 quads displaced in a 5x5 grid arrangement

	float kernel[5] = { 0.25f, 0.50f, 1.0f, 0.50f, 0.25f }; // This controls the shape of the glow

	if (sd.Width > 0 && sd.Height > 0) {
		// This could probably be made more efficient by rendering all 25 quads as a
		// single batch, but it would require using vertex color or another set of
		// texture coordinates on each vertex.  Maybe later.

		D3DMATERIAL9 mtl;

		mtl.Emissive = D3DXCOLOR(0, 0, 0, 0);
		mtl.Diffuse = D3DXCOLOR(0, 0, 0, 1);
		mtl.Specular = D3DXCOLOR(0, 0, 0, 0);
		mtl.Power = 0.0f;

		for (int i = -2; i <= 2; i++) {
			for (int j = -2; j <= 2; j++) {
				float dx = fx * i - fx;
				float dy = fy * j;

				const float vertices[] = {
					// pos					uv
					dx, dy, 0, 0, 0,
					dx + 1, dy, 0, 1, 0,
					dx + 1, dy + 1, 0, 1, 1,
					dx, dy + 1, 0, 0, 1
				};

				float strength = 0.67f;
				float opacity = strength * kernel[i + 2] * kernel[j + 2];
				mtl.Ambient = D3DXCOLOR(opacity, opacity, opacity, 1);
				hr = pD3D->SetMaterial(&mtl);

				hr = pD3D->SetFVF(D3DFVF_XYZ | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0));
				hr = pD3D->DrawPrimitiveUP(D3DPT_TRIANGLEFAN, 2, vertices, 5 * sizeof(float));
			}
		}
	}
}

void ClientEngine::OutlinesCompositePass(LPDIRECT3DDEVICE9 pD3D) {
	float fx = 0.0f, fy = 0.0f; // half-pixel displacements for screen mapping

	LPDIRECT3DSURFACE9 pBackBuffer = NULL;
	HRESULT hr = pD3D->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer);

	if (pBackBuffer) {
		D3DSURFACE_DESC bbsd;
		ZeroMemory(&bbsd, sizeof(bbsd));

		hr = pBackBuffer->GetDesc(&bbsd);

		if (SUCCEEDED(hr)) {
			// Compute x and y half-pixel displacements
			fx = bbsd.Width > 0 ? (-0.5f / bbsd.Width) : 0.0f;
			fy = bbsd.Height > 0 ? (-0.5f / bbsd.Height) : 0.0f;
		}

		SAFE_RELEASE(pBackBuffer);
	}

	// Build ortho projection matrix, and set world/view matrices to identity
	D3DXMATRIX matIdent, matProj;
	D3DXMatrixIdentity(&matIdent);
	D3DXMatrixOrthoOffCenterLH(&matProj, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f);
	hr = pD3D->SetTransform(D3DTS_WORLD, &matIdent);
	hr = pD3D->SetTransform(D3DTS_VIEW, &matIdent);
	hr = pD3D->SetTransform(D3DTS_PROJECTION, &matProj);

	// Use additive blending to composite glow
	hr = pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	hr = pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
	hr = pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCCOLOR);

	hr = pD3D->SetRenderState(D3DRS_LIGHTING, TRUE);

	float opacity = 0.70f;

	D3DMATERIAL9 mat;

	mat.Ambient = D3DXCOLOR(0, 0, 0, 0);
	mat.Diffuse = D3DXCOLOR(0, 0, 0, 0);
	mat.Specular = D3DXCOLOR(0, 0, 0, 0);
	mat.Emissive = D3DXCOLOR(opacity, opacity, opacity, 0);
	mat.Power = 0;

	pD3D->SetMaterial(&mat);

	for (int light = 0; light < ReD3DX9DEVICEMAXUPLOADLIGHTS; light++)
		pD3D->LightEnable(light, FALSE);

	// Use stencil test to composite glow only where stencil = 0
	// These regions are where no objects have been drawn
	hr = pD3D->SetRenderState(D3DRS_STENCILENABLE, TRUE);
	hr = pD3D->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP);
	hr = pD3D->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_EQUAL);
	hr = pD3D->SetRenderState(D3DRS_STENCILREF, 0);

	hr = pD3D->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);

	hr = pD3D->SetTexture(0, m_pGlowTEX[1]);

	// Disable all but the first texture stage
	for (DWORD stage = 1; stage < ReDEVICEMAXTEXTURESTAGES; stage++)
		hr = pD3D->SetTexture(stage, NULL);

	// Clamp in U and V so that glow doesn't wrap around screen
	hr = pD3D->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	hr = pD3D->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

	const float vertices[] = {
		// pos		uv
		fx, fy, 0, 0, 0,
		fx + 1, fy, 0, 1, 0,
		fx + 1, fy + 1, 0, 1, 1,
		fx, fy + 1, 0, 0, 1
	};

	hr = pD3D->SetFVF(D3DFVF_XYZ | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0));
	hr = pD3D->DrawPrimitiveUP(D3DPT_TRIANGLEFAN, 2, vertices, 5 * sizeof(float));

	hr = pD3D->SetRenderState(D3DRS_STENCILENABLE, FALSE);
	hr = pD3D->SetTexture(0, NULL);
}

void ClientEngine::OutlinesRender() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "OutlinesRender");

	LPDIRECT3DDEVICE9 pD3D = GetD3DDevice();
	if (!pD3D)
		return;

	// This function monkeys with the view/projection matrices
	// so remember them here and then restore them before returning
	D3DXMATRIX matView, matProj;
	pD3D->GetTransform(D3DTS_VIEW, &matView);
	pD3D->GetTransform(D3DTS_PROJECTION, &matProj);

	if (m_pGlowTEX[0] && m_pGlowTEX[1]) {
		LPDIRECT3DSURFACE9 pGlowRT[2] = { NULL, NULL };

		// Get surface pointer
		HRESULT hr = m_pGlowTEX[0]->GetSurfaceLevel(0, &pGlowRT[0]);

		if (pGlowRT[0]) {
			hr = m_pGlowTEX[1]->GetSurfaceLevel(0, &pGlowRT[1]);

			if (pGlowRT[1]) {
				LPDIRECT3DSURFACE9 pBB = NULL;

				// Get back buffer render target
				hr = pD3D->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &pBB);

				if (SUCCEEDED(hr) && pBB) {
					// Render outlined objects into stencil buffer, do nothing else if no objects selected
					if (OutlinesStencilPass(pD3D) > 0) {
						hr = pD3D->SetRenderTarget(0, pGlowRT[0]);

						if (SUCCEEDED(hr)) {
							// Render outlined objects into offscreen 0
							OutlinesColorPass(pD3D);

							hr = pD3D->SetRenderTarget(0, pGlowRT[1]);

							if (SUCCEEDED(hr)) {
								// Blur offscreen 0 into offscreen 1
								OutlinesBlurPass(pD3D, pGlowRT[1]);

								hr = pD3D->SetRenderTarget(0, pBB);

								if (SUCCEEDED(hr)) {
									// Composite offscreen 1 with screen, stencil out objects
									OutlinesCompositePass(pD3D);
								}
							}

							// Restore original render target
							hr = pD3D->SetRenderTarget(0, pBB);
						}
					}

					SAFE_RELEASE(pBB);
				}

				SAFE_RELEASE(pGlowRT[1]);
			}

			SAFE_RELEASE(pGlowRT[0]);
		}
	}

	pD3D->SetTransform(D3DTS_VIEW, &matView);
	pD3D->SetTransform(D3DTS_PROJECTION, &matProj);
}

int ClientEngine::OutlinesRenderObjects(bool usePerObjectZEnableState) {
	int res = 0;

	LPDIRECT3DDEVICE9 pD3D = GetD3DDevice();
	if (pD3D) {
		IterateObjects([this, pD3D, usePerObjectZEnableState, &res](CDynamicPlacementObj* dynObj) -> bool {
			if (dynObj && dynObj->GetBaseObj() && dynObj->GetOutlineState() && !dynObj->m_culled) {
				++res;

				D3DMATERIAL9 mtl;

				mtl.Ambient = D3DXCOLOR(0, 0, 0, 0);
				mtl.Diffuse = D3DXCOLOR(0, 0, 0, 1);
				mtl.Emissive = dynObj->GetOutlineColor();
				mtl.Specular = D3DXCOLOR(0, 0, 0, 0);
				mtl.Power = 0.0f;

				pD3D->SetMaterial(&mtl);
				if (usePerObjectZEnableState) {
					pD3D->SetRenderState(D3DRS_ZENABLE, dynObj->IsOutlineZSorted() ? TRUE : FALSE);
				}
				ImmRender_DynamicObject(dynObj, pD3D);
			}
			return true;
		});

		// Characters
		if (m_movObjList) {
			// Unbind all texture stages
			for (UINT stage = 0; stage < ReDEVICEMAXTEXTURESTAGES; stage++)
				pD3D->SetTexture(stage, NULL);

			POSITION pos = m_movObjList->GetHeadPosition();
			while (pos) {
				auto pMO = (CMovementObj*)m_movObjList->GetNext(pos);
				if (pMO && pMO->getSkeleton() && pMO->getRender() && pMO->assetsLoaded() && !pMO->getSkeleton()->isCulled() && pMO->getOutlineState()) {
					D3DMATERIAL9 mtl;

					mtl.Ambient = D3DXCOLOR(0, 0, 0, 0);
					mtl.Diffuse = D3DXCOLOR(0, 0, 0, 1);
					mtl.Emissive = D3DXCOLOR(pMO->getOutlineColor());
					mtl.Specular = D3DXCOLOR(0, 0, 0, 0);
					mtl.Power = 0.0f;

					pD3D->SetMaterial(&mtl);
					if (usePerObjectZEnableState) {
						pD3D->SetRenderState(D3DRS_ZENABLE, pMO->isOutlineZSorted() ? TRUE : FALSE);
					}
					ImmRender_MovementObject(pMO, pD3D);
					++res;
				}
			}
		}

		// World Objects
		MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
		if (m_pWOL) {
			// Unbind all texture stages
			for (UINT stage = 0; stage < ReDEVICEMAXTEXTURESTAGES; stage++)
				pD3D->SetTexture(stage, NULL);

			POSITION pos = m_pWOL->GetHeadPosition();
			while (pos) {
				auto pWO = (CWldObject*)m_pWOL->GetNext(pos);
				if (pWO && !pWO->m_culled && pWO->GetOutlineState()) {
					D3DMATERIAL9 mtl;

					mtl.Ambient = D3DXCOLOR(0, 0, 0, 0);
					mtl.Diffuse = D3DXCOLOR(0, 0, 0, 1);
					mtl.Emissive = pWO->GetOutlineColor();
					mtl.Specular = D3DXCOLOR(0, 0, 0, 0);
					mtl.Power = 0.0f;

					pD3D->SetMaterial(&mtl);
					if (usePerObjectZEnableState) {
						pD3D->SetRenderState(D3DRS_ZENABLE, pWO->IsOutlineZSorted() ? TRUE : FALSE);
					}
					ImmRender_WorldObject(pWO, GetD3DDevice());
					++res;
				}
			}
		}
	}

	// Render Sound Placement Outlines
	if (m_renderSoundPlacements) {
		for (const auto& it : m_soundPlacements) {
			auto pSP = it.second;
			if (!pSP)
				continue;

			// DRF - Some Sound Placements Are Never Shown (attached to player)
			if (!pSP->ShowSoundPlacement())
				continue;

			// Has Outline ?
			if (!pSP->GetOutlineState())
				continue;

			D3DMATERIAL9 mtl;
			mtl.Ambient = D3DXCOLOR(0, 0, 0, 0);
			mtl.Diffuse = D3DXCOLOR(0, 0, 0, 1);
			mtl.Emissive = pSP->GetOutlineColor();
			mtl.Specular = D3DXCOLOR(0, 0, 0, 0);
			mtl.Power = 0.0f;
			pD3D->SetMaterial(&mtl);
			if (usePerObjectZEnableState) {
				pD3D->SetRenderState(D3DRS_ZENABLE, pSP->IsOutlineZSorted() ? TRUE : FALSE);
			}
			ImmRender_SoundPlacement(pSP, pD3D);
			++res;
		}
	}
	return res;
}

void ClientEngine::ImmRender_MovementObject(CMovementObj* pMO, LPDIRECT3DDEVICE9 pD3D) {
	if (!pMO)
		return;

	auto pRS = pMO->getSkeleton();
	if (!pRS)
		return;

	size_t smCount = pRS->getSkinnedMeshCount();
	for (size_t slot = 0; slot < smCount; slot++) {
		ReMesh* pRM = pRS->getMeshCacheEntry(pRS->getCurrentLOD(), slot);
		if (!pRM)
			continue;
		pD3D->SetTransform(D3DTS_WORLD, reinterpret_cast<const D3DMATRIX*>(pRM->GetWorldMatrix()));
		pRM->Render();
	}

	std::vector<CArmedInventoryObj*> allEquips;
	pMO->getSkeleton()->getAllEquippableItems(allEquips);

	for (auto pAIO : allEquips) {
		if (!pAIO)
			continue;

		auto pRS_aio = pAIO->getSkeleton(pMO->isFirstPersonMode() ? eVisualType::FirstPerson : eVisualType::ThirdPerson);
		if (!pRS_aio || !pRS_aio->areAllMeshesLoaded())
			continue;

		for (int slot = 0; slot < pRS_aio->getSkinnedMeshCount(); slot++) {
			auto pRM = pRS_aio->getMeshCacheEntry(pRS_aio->getCurrentLOD(), slot);
			if (!pRM || !pRM->GetWorldMatrix())
				continue;
			pD3D->SetTransform(D3DTS_WORLD, reinterpret_cast<const D3DMATRIX*>(pRM->GetWorldMatrix()));
			pRM->Render();
		}
	}
}

void ClientEngine::ImmRender_DynamicObject(CDynamicPlacementObj* pDPO, LPDIRECT3DDEVICE9 pD3D) {
	if (!pDPO || !pDPO->GetBaseObj())
		return;

	auto pRS = pDPO->getSkeleton();
	if (!pRS)
		return;

	pRS->IterateMeshes(pRS->getCurrentLOD(), [pD3D](ReMesh* pRM) {
		if (pRM) {
			if (pRM->GetMaterial() && pRM->GetMaterial()->GetRenderState() && pRM->GetMaterial()->GetRenderState()->GetData()) {
				const ReRenderStateData* rsData = pRM->GetMaterial()->GetRenderState()->GetData();

				for (DWORD i = 0; i < rsData->NumTexStages; i++) {
					// Ignore texture color information
					pD3D->SetTextureStageState(i, D3DTSS_COLORARG1, D3DTA_CURRENT);
					pD3D->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_SELECTARG1);

					// Use texture alpha component
					pD3D->SetTextureStageState(i, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
					pD3D->SetTextureStageState(i, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

					LPDIRECT3DTEXTURE9 tex = (LPDIRECT3DTEXTURE9)rsData->TexPtr[i].get();
					pD3D->SetTexture(i, tex);
				}

				// Disable remaining texture stages
				for (DWORD i = rsData->NumTexStages; i < ReDEVICEMAXTEXTURESTAGES; i++) {
					pD3D->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_DISABLE);
					pD3D->SetTextureStageState(i, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
					pD3D->SetTexture(i, NULL);
				}
			}

			if (pRM->GetWorldMatrix()) {
				pD3D->SetTransform(D3DTS_WORLD, reinterpret_cast<const D3DMATRIX*>(pRM->GetWorldMatrix()));
				pRM->Render();
			}
		}
	});
}

void ClientEngine::ImmRender_WorldObject(const CWldObject* pWO, LPDIRECT3DDEVICE9 pD3D) {
	if (!pWO)
		return;

	ReMesh* pMesh = NULL;
	Matrix44f matWorld;

	if (pWO->m_meshObject) {
		matWorld = pWO->m_meshObject->GetWorldTransform();
		pMesh = pWO->GetReMesh();
	} else if (pWO->m_meshObject == 0 && pWO->m_node != 0) {
		CReferenceObj* pRO = pWO->GetReferenceObj();
		if (pRO) {
			matWorld = pWO->m_node->GetWorldTransform();
			pMesh = pRO->GetReMesh();
		}
	}

	if (!pMesh)
		return;

	if (pMesh->GetMaterial() && pMesh->GetMaterial()->GetRenderState() && pMesh->GetMaterial()->GetRenderState()->GetData()) {
		const ReRenderStateData* rsData = pMesh->GetMaterial()->GetRenderState()->GetData();

		for (DWORD i = 0; i < rsData->NumTexStages; i++) {
			// Ignore texture color information
			pD3D->SetTextureStageState(i, D3DTSS_COLORARG1, D3DTA_CURRENT);
			pD3D->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_SELECTARG1);

			// Use texture alpha component
			pD3D->SetTextureStageState(i, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
			pD3D->SetTextureStageState(i, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

			LPDIRECT3DTEXTURE9 tex = (LPDIRECT3DTEXTURE9)rsData->TexPtr[i].get();
			pD3D->SetTexture(i, tex);
		}

		// Disable remaining texture stages
		for (DWORD i = rsData->NumTexStages; i < ReDEVICEMAXTEXTURESTAGES; i++) {
			pD3D->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_DISABLE);
			pD3D->SetTextureStageState(i, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
			pD3D->SetTexture(i, NULL);
		}
	}

	if (pMesh->GetWorldMatrix()) {
		pD3D->SetTransform(D3DTS_WORLD, reinterpret_cast<const D3DMATRIX*>(pMesh->GetWorldMatrix()));
		pMesh->Render();
	}
}

bool ClientEngine::ImmRender_SoundPlacement(SoundPlacement* pSP, LPDIRECT3DDEVICE9 pD3D) {
	if (!pSP || !pD3D)
		return false;

	auto pRM = pSP->GetSoundMesh();
	if (!pRM)
		return false;

	auto pRMP = pRM->GetMaterial();
	if (!pRMP)
		return false;

	auto pRS = pRMP->GetRenderState();
	if (!pRS)
		return false;

	auto pRSD = pRS->GetData();
	if (!pRSD)
		return false;

	auto pWM = pRM->GetWorldMatrix();
	if (!pWM)
		return false;

	for (DWORD i = 0; i < pRSD->NumTexStages; i++) {
		// Ignore texture color information
		pD3D->SetTextureStageState(i, D3DTSS_COLORARG1, D3DTA_CURRENT);
		pD3D->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_SELECTARG1);

		// Use texture alpha component
		pD3D->SetTextureStageState(i, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		pD3D->SetTextureStageState(i, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

		LPDIRECT3DTEXTURE9 tex = (LPDIRECT3DTEXTURE9)pRSD->TexPtr[i].get();
		pD3D->SetTexture(i, tex);
	}

	// Disable remaining texture stages
	for (DWORD i = pRSD->NumTexStages; i < ReDEVICEMAXTEXTURESTAGES; i++) {
		pD3D->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_DISABLE);
		pD3D->SetTextureStageState(i, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
		pD3D->SetTexture(i, NULL);
	}

	pD3D->SetTransform(D3DTS_WORLD, reinterpret_cast<const D3DMATRIX*>(pWM));

	pRM->Render();

	return true;
}

void ClientEngine::HighlightsRender() {
#ifdef _ClientOutput
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "HighlightsRender");

	// Highlight MouseOver Object ?
	LPDIRECT3DDEVICE9 pD3D = GetD3DDevice();
	bool renderHighlight = pD3D && (((unsigned long)m_mouseOver.objType & m_rolloverMode) != 0);
	if (!renderHighlight) {
		// Object MouseOverSetting Mode Highlight ?
		MOS mos;
		renderHighlight = (MOS_Get(mos, m_mouseOver.objType, m_mouseOver.objId) && mos.modeLighten());
		if (!renderHighlight)
			return;
	}

	// stencil state
	pD3D->SetRenderState(D3DRS_STENCILENABLE, FALSE);

	// z-buffer state
	pD3D->SetRenderState(D3DRS_ZENABLE, TRUE);
	pD3D->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	pD3D->SetRenderState(D3DRS_ZFUNC, D3DCMP_EQUAL);

	// blending state
	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_DESTCOLOR);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

	// material state
	D3DMATERIAL9 mat;
	ZeroMemory(&mat, sizeof(mat));
	mat.Emissive = D3DXCOLOR(0.20f, 0.20f, 0.20f, 0.0f);
	pD3D->SetMaterial(&mat);

	// lighting state
	pD3D->SetRenderState(D3DRS_LIGHTING, TRUE);
	for (UINT light = 0; light < ReD3DX9DEVICEMAXUPLOADLIGHTS; light++)
		pD3D->LightEnable(light, FALSE);

	// texture bind state
	for (UINT stage = 0; stage < ReDEVICEMAXTEXTURESTAGES; stage++)
		pD3D->SetTexture(stage, NULL);

	switch (m_mouseOver.objType) {
		case ObjectType::MOVEMENT: {
			auto pMO = GetMovementObjByNetId(m_mouseOver.objId);
			if (pMO && pMO->getSkeleton() && pMO->getRender() && pMO->assetsLoaded() && !pMO->getSkeleton()->isCulled())
				ImmRender_MovementObject(pMO, GetD3DDevice());
		} break;

		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(m_mouseOver.objId);
			if (pDPO && !pDPO->m_culled && m_kepWidget && pDPO->GetOutlineState()) {
				switch (m_kepWidget->getWidgetMode()) {
					case Selected_Single_Rotation:
					case Selected_Multi_Rotation:
						SetCursorModeType(CURSOR_MODE_WIDGET, CURSOR_TYPE_ROTATABLE);
						break;

					case Selected_Single_Translation:
					case Selected_Multi_Translation:
						SetCursorModeType(CURSOR_MODE_WIDGET, CURSOR_TYPE_TRANSLATABLE);
						break;
				}

				ImmRender_DynamicObject(pDPO, GetD3DDevice());
			}
		} break;

		case ObjectType::WORLD: {
			MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
			auto pWO = GetWorldObject(m_mouseOver.objId);
			if (pWO && !pWO->m_culled && pWO->IsInteractable())
				ImmRender_WorldObject(pWO, GetD3DDevice());
		} break;

		case ObjectType::SOUND: {
			if (!m_renderSoundPlacements)
				break;
			auto pSP = SoundPlacementGet(m_mouseOver.objId);
			if (pSP)
				ImmRender_SoundPlacement(pSP, GetD3DDevice());
		} break;
	}

	pD3D->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	pD3D->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	pD3D->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
#endif
}

void ClientEngine::RenderMenuGameObjects() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "RenderMenuGameObjects");

	// render menu game objects at normal level
	//still need for spinning characters on character creation
	GetRenderStateManager()->ResetRenderState();

	m_pMenuGameObjects->Render(Matrix44f::GetIdentity(), false);
}

#if (DRF_WORLD_ENVIRONMENT_LIST == 1)
void ClientEngine::RenderEnvironmentAttachedObjects(CViewportObj* pVO, BOOL mirrorModeOn) {
	if (!pVO || !m_bRenderEnvironment || !m_pRenderStateMgr || !m_worldEnvironmentList)
		return;

	Vector3f cameraPos = pVO->m_camWorldPosition;
	m_pRenderStateMgr->SetZWritableState(FALSE);
	m_pRenderStateMgr->SetShadeMode(1);

	// DRF - DEAD CODE ? - IS THIS LIST ALWAYS EMPTY?
	for (POSITION posLocal = m_worldEnvironmentList->GetHeadPosition(); posLocal != NULL;) {
		auto pWO = (CWldObject*)m_worldEnvironmentList->GetNext(posLocal);
		if (!pWO || !pWO->m_meshObject || !m_cgGlobalShaderDB)
			continue;

		if (!pWO->m_meshObject->m_abVertexArray.m_indicieBuffer) {
			if (!pWO->m_meshObject->m_materialObject)
				continue;
			pWO->m_meshObject->InitBuffer(g_pD3dDevice, m_cgGlobalShaderDB->GetByIndex(pWO->m_meshObject->m_materialObject->m_optionalShaderIndex - 1));
		}

		float fPosY;
		if (pWO->m_environmentAttached == 2) {
			fPosY = -cameraPos.y;
		} else if (m_environment && (cameraPos.y < m_environment->m_seaLevel)) {
			fPosY = m_environment->m_seaLevel - cameraPos.y;
		}
		Matrix44f tempLocal;
		tempLocal.MakeTranslation(Vector3f(0, fPosY, 0));

		if (mirrorModeOn == TRUE) {
			tempLocal = tempLocal * m_mirrorTransform;
			m_pRenderStateMgr->SetWorldMatrix(&tempLocal);
		} else {
			m_pRenderStateMgr->SetWorldMatrix(&tempLocal);
		}

		auto pMaterial = pWO->m_meshObject->m_materialObject;
		if (pMaterial && m_environment) {
			pMaterial->Callback();
			if (m_environment->m_environmentTimeSystem) {
				auto pDSO = m_environment->m_environmentTimeSystem->m_pDSO;
				if (pDSO) {
					if ((pDSO->m_sunColorRed > pDSO->m_ambientColorRed) || (pDSO->m_sunColorGreen > pDSO->m_ambientColorGreen) || (pDSO->m_sunColorBlue > pDSO->m_ambientColorBlue)) {
						pMaterial->m_useMaterial.Emissive.r = pDSO->m_sunColorRed;
						pMaterial->m_useMaterial.Emissive.g = pDSO->m_sunColorGreen;
						pMaterial->m_useMaterial.Emissive.b = pDSO->m_sunColorBlue;
					} else {
						pMaterial->m_useMaterial.Emissive.r = pDSO->m_ambientColorRed;
						pMaterial->m_useMaterial.Emissive.g = pDSO->m_ambientColorGreen;
						pMaterial->m_useMaterial.Emissive.b = pDSO->m_ambientColorBlue;
					}
				}
			}

			m_pRenderStateMgr->SetBlendType(pMaterial->m_blendMode);
			m_pRenderStateMgr->SetMaterial(&(pMaterial->m_useMaterial));
			m_pRenderStateMgr->SetTextureTileMode(pMaterial->m_textureTileType);
			m_pRenderStateMgr->SetPxShader(m_pxShaderSystem, pMaterial->m_pixelShader);
			m_pRenderStateMgr->SetVertexShader(m_shaderSystem, pMaterial->m_vertexShader);
		}
#if DEPRECATED
		MultipassRenderMesh(pWO->m_meshObject, 0);
#endif
	} // end loop through objects

	// force to background works
	m_pRenderStateMgr->SetZWritableState(TRUE);
	m_pRenderStateMgr->SetShadeMode(0);
}
#endif

// Only For Rendering Loading Splash Screen Progress Bar During Startup
bool ClientEngine::RenderLoadingSplashScreen(const std::string& texFilePath, bool maintainAspect) {
	if (!g_pD3dDevice || g_pD3dDevice->TestCooperativeLevel() != D3D_OK)
		return false;

	// Initialize splash if necessary.
	if (!m_RenderSplash) {
		// Using std::shared_ptr instead of std::unique_ptr because of VS2015 issues with capturing move-only objects inside of lambdas.
		std::shared_ptr<CViewportObj> pVO = std::make_unique<CViewportObj>("StartupViewport");
		std::shared_ptr<CTextureEX> pTex = std::make_shared<CTextureEX>();
		std::shared_ptr<CEXMeshObj> im_mesh = MakeUniqueSafeDeleteThenDelete<CEXMeshObj>();
		static const WORD nVertices = 4;
		static const WORD nIndices = 6;
		std::unique_ptr<ABVERTEX1L[]> vertexArray = std::make_unique<ABVERTEX1L[]>(nVertices);
		std::unique_ptr<WORD[]> indexArray = std::make_unique<WORD[]>(nIndices);

		float bbHalfWidth = 0;
		float bbHalfHeight = 0;
		if (pTex->LoadTextureByPath(texFilePath, 1, false, 0, false) && pTex->GetD3DTextureIfLoadedOrQueueToLoad()) {
			pTex->SetForceFullRes(true);

			float minWidth = std::max(pTex->GetWidth(), (int)pVO->m_imViewPort.Width);
			float minHeight = std::max(pTex->GetHeight(), (int)pVO->m_imViewPort.Height);
			float widthRatio = (minWidth / pTex->GetWidth());
			float heightRatio = (minHeight / pTex->GetHeight());
			if (!maintainAspect) {
				bbHalfWidth = 0.5f * widthRatio * pTex->GetWidth();
				bbHalfHeight = 0.5f * heightRatio * pTex->GetHeight();
			} else {
				float finalRatio = std::min(widthRatio, heightRatio);
				bbHalfWidth = 0.5f * finalRatio * pTex->GetHeight();
				bbHalfHeight = 0.5f * finalRatio * pTex->GetWidth();
			}
		} else {
			pTex.reset(); // Could not load texture.
		}

		// vertex 1 upper left
		vertexArray[0].x = -bbHalfWidth;
		vertexArray[0].y = bbHalfHeight;
		vertexArray[0].z = 0.0f;
		vertexArray[0].tx0.tu = 0.0f;
		vertexArray[0].tx0.tv = 0.0f;
		vertexArray[0].nx = 0.0f;
		vertexArray[0].ny = 0.0f;
		vertexArray[0].nz = -1.0f;

		// vertex 2 bottom left
		vertexArray[1].x = -bbHalfWidth;
		vertexArray[1].y = -bbHalfHeight;
		vertexArray[1].z = 0.0f;
		vertexArray[1].tx0.tu = 0.0f;
		vertexArray[1].tx0.tv = 1.0f;
		vertexArray[1].nx = 0.0f;
		vertexArray[1].ny = 0.0f;
		vertexArray[1].nz = -1.0f;

		// vertex 3 bottom right
		vertexArray[2].x = bbHalfWidth;
		vertexArray[2].y = -bbHalfHeight;
		vertexArray[2].z = 0.0f;
		vertexArray[2].tx0.tu = 1.0f;
		vertexArray[2].tx0.tv = 1.0f;
		vertexArray[2].nx = 0.0f;
		vertexArray[2].ny = 0.0f;
		vertexArray[2].nz = -1.0f;

		// vertex 4 upper right
		vertexArray[3].x = bbHalfWidth;
		vertexArray[3].y = bbHalfHeight;
		vertexArray[3].z = 0.0f;
		vertexArray[3].tx0.tu = 1.0f;
		vertexArray[3].tx0.tv = 0.0f;
		vertexArray[3].nx = 0.0f;
		vertexArray[3].ny = 0.0f;
		vertexArray[3].nz = -1.0f;

		// indices
		// no facing
		indexArray[0] = 1;
		indexArray[1] = 0;
		indexArray[2] = 3;
		indexArray[3] = 2;
		indexArray[4] = 1;
		indexArray[5] = 3;

		im_mesh->m_materialObject = new CMaterialObject();
		im_mesh->m_materialObject->m_useMaterial.Emissive.r = 1.0f;
		im_mesh->m_materialObject->m_useMaterial.Emissive.g = 1.0f;
		im_mesh->m_materialObject->m_useMaterial.Emissive.b = 1.0f;
		im_mesh->m_materialObject->m_useMaterial.Emissive.a = 0.0f;

		im_mesh->SetGeometry2("", vertexArray.release(), nVertices, indexArray.release(), nIndices);

		im_mesh->m_abVertexArray.m_uvCount = 1;
		im_mesh->InitBuffer(g_pD3dDevice, NULL);

		m_RenderSplash = [this, pVO, pTex, im_mesh]() {
			if (!pTex)
				return false;

			ActivateViewport(pVO.get());

			DWORD prevZbState;
			g_pD3dDevice->GetRenderState(D3DRS_ZENABLE, &prevZbState);
			g_pD3dDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);

			D3DXMATRIX matProj, matView, matWorld, prevProjMatrix;
			g_pD3dDevice->GetTransform(D3DTS_PROJECTION, &prevProjMatrix);
			{
				// fill in matProj with an orthogonal projection matrix that maps coordinates to pixels
				ZeroMemory(&matProj, sizeof(matProj));
				matProj._11 = 2.0f / pVO->GetWidth(); // make sure ActivateViewport is called before this
				matProj._22 = 2.0f / pVO->GetHeight(); //  so that these values are correct
				matProj._33 = -1.0f / (0.0f - 200.0f) /* zn - zf */;
				matProj._43 = 0.0f / (0.0f - 200.0f) /* zn - zf */;
				matProj._44 = 1.0f;
			}
			D3DXMatrixIdentity(&matView);
			D3DXMatrixIdentity(&matWorld);

			m_pRenderStateMgr->SetViewMatrix(&matView);
			m_pRenderStateMgr->SetProjectionMatrix(&matProj);
			m_pRenderStateMgr->SetWorldMatrix(&matWorld);

			// disable lighting
			m_pRenderStateMgr->SetLightingMode(FALSE);

			// disable culling
			m_pRenderStateMgr->SetCullState(FALSE);

			// disable texture blending ops
			m_pRenderStateMgr->SetBlendLevel(0, 0);
			for (int loop = 1; (loop < m_pRenderStateMgr->m_multiTextureSupported) && (loop < 9); loop++)
				m_pRenderStateMgr->SetBlendLevel(loop, -1);

			// disable alpha blending
			m_pRenderStateMgr->SetBlendType(-1);

			m_pRenderStateMgr->SetMaterial(&im_mesh->m_materialObject->m_useMaterial);

			g_pD3dDevice->SetTexture(0, pTex->GetD3DTextureIfLoadedOrQueueToLoad()); // set texture

			if (g_pD3dDevice->BeginScene() == 0) {
				m_pRenderStateMgr->SetDiffuseSource(im_mesh->m_abVertexArray.m_advancedFixedMode);
				m_pRenderStateMgr->SetVertexType(im_mesh->m_abVertexArray.m_uvCount);
				im_mesh->Render(g_pD3dDevice, m_pRenderStateMgr->m_currentVertexType, im_mesh->m_chromeUvSet);
				g_pD3dDevice->EndScene();
			}

			g_pD3dDevice->SetTexture(0, 0); // set texture
			m_pRenderStateMgr->SetTextureState(-1, 0, m_pTextureDB);

			RECT* pRect = &m_pCABD3DLIB->m_curModeRect;
			if (pRect) {
				// Present (swap back and front framebuffers)
				HRESULT err = g_pD3dDevice->Present(NULL, NULL, NULL, NULL);
				if (FAILED(err)) {
					LogError(
						"FAILED d3dDevice->Present() -"
						" rect=["
						<< pRect->left << " " << pRect->top << " " << pRect->right << " " << pRect->bottom << "]"
																											  " err='"
						<< DXGetErrorDescription9A(err) << "'");
				}
			}

			// restore state
			g_pD3dDevice->SetRenderState(D3DRS_ZENABLE, prevZbState);
			m_pRenderStateMgr->SetProjectionMatrix(&prevProjMatrix);
			return true;
		};
	}

	return m_RenderSplash();
}

BOOL ClientEngine::EvaluateAndRenderCharonaOverlaysV2(CViewportObj* pVO) {
	if (!pVO)
		return FALSE;

	// Get Viewport Camera
	auto pCO = pVO->GetCamera();
	if (!pCO)
		return FALSE;

	// Get Viewport Camera Position
	Vector3f camPosition = pCO->m_camWorldPosition;

	Matrix44f matrixBase = pVO->m_camWorldMatrix;
	for (POSITION posRen = m_visualsRenderList->GetTailPosition(); posRen != NULL;) {
		CEXMeshObj* meshex = (CEXMeshObj*)m_visualsRenderList->GetPrev(posRen);
		if (meshex->m_charona) {
			Vector3f wldPosition = TransformPoint_NonPerspective(meshex->GetWorldTransform(), meshex->GetMeshCenter());

			BOOL inRange = MatrixARB::BoundingSphereCheck(
				meshex->m_charona->m_visibleDistance,
				0.0f,
				meshex->GetMeshCenter().x,
				meshex->GetMeshCenter().y,
				meshex->GetMeshCenter().z,
				camPosition.x,
				camPosition.y,
				camPosition.z);
			if (inRange) {
				ABBOX clipBox;
				clipBox.maxX = wldPosition.x + 1.0f;
				clipBox.maxY = wldPosition.y + 1.0f;
				clipBox.maxZ = wldPosition.z + 1.0f;

				clipBox.minX = wldPosition.x - 1.0f;
				clipBox.minY = wldPosition.y - 1.0f;
				clipBox.minZ = wldPosition.z - 1.0f;

				if (IsBoxInView(clipBox)) {
					BOOL coronaBlocked = m_pCollisionObj->CollisionSegmentIntersectBasic(camPosition, wldPosition, 20.0f, true);
					if (coronaBlocked == FALSE) {
						meshex->m_charona->m_currentDistance = Distance(wldPosition, matrixBase.GetTranslation());
						float camFovX = pVO->GetCameraFovX();
						meshex->m_charona->m_currentDistance = meshex->m_charona->m_currentDistance * camFovX;
						if (meshex->m_charona->m_currentDistance < meshex->m_charona->m_visibleDistance) {
							meshex->m_charona->m_validThisRound = TRUE;
							meshex->m_charona->m_material->SetFadeIn(meshex->m_charona->m_fadeTime, FALSE);
						} else {
							meshex->m_charona->m_validThisRound = FALSE;
							meshex->m_charona->m_material->SetFadeOut(meshex->m_charona->m_fadeTime);
						}
					} else {
						meshex->m_charona->m_validThisRound = FALSE;
						meshex->m_charona->m_material->SetFadeOut(meshex->m_charona->m_fadeTime);
					}
				} else {
					// not in view
					meshex->m_charona->m_validThisRound = FALSE;
					meshex->m_charona->m_material->SetFadeOut(meshex->m_charona->m_fadeTime);
				}
			} else {
				// not in range
				meshex->m_charona->m_validThisRound = FALSE;
				meshex->m_charona->m_material->SetFadeOut(meshex->m_charona->m_fadeTime);
			}

			g_pD3dDevice->SetRenderState(D3DRS_ZENABLE, FALSE);
			m_pRenderStateMgr->SetBlendLevel(0, 0);
			for (int loop = 1; loop < m_pRenderStateMgr->m_multiTextureSupported; loop++) {
				m_pRenderStateMgr->SetBlendLevel(loop, -1);
				if (loop > 7) {
					break;
				}
			}

			// render it
			m_pRenderStateMgr->SetTextureState(meshex->m_charona->m_textureIndex, 0, m_pTextureDB);
			meshex->m_charona->m_material->m_fogFilter = TRUE;
			meshex->m_charona->m_material->Callback();
			meshex->m_charona->m_material->m_blendMode = 2;
			m_pRenderStateMgr->SetBlendType(meshex->m_charona->m_material->m_blendMode);
			m_pRenderStateMgr->SetCullState(meshex->m_charona->m_material->m_cullOn);
			m_pRenderStateMgr->SetWireFillState(meshex->m_charona->m_material->m_forceWireFrame);

			m_pRenderStateMgr->SetMaterial(&meshex->m_charona->m_material->m_useMaterial);

			Vector3f camPosLoc = matrixBase.GetTranslation();
			wldPosition = TransformPoint_NonPerspective(meshex->GetWorldTransform(), meshex->GetMeshCenter());
			Matrix44f matrixTempLocal;
			matrixTempLocal.MakeTranslation(wldPosition);
			MatrixARB::FaceTowardPoint(camPosLoc, &matrixTempLocal);

			// do scaleing
			float scaleFactor = 0.0f;
			if (meshex->m_charona->m_currentDistance <= meshex->m_charona->m_startDistance) {
				scaleFactor = 0.0f;
			} else if (meshex->m_charona->m_currentDistance >= meshex->m_charona->m_endDistance) {
				scaleFactor = 1.0f;
			} else {
				scaleFactor = MatrixARB::InterpolateFloats(0.0f,
					1.0f,
					(int)(meshex->m_charona->m_endDistance - meshex->m_charona->m_startDistance),
					(int)(meshex->m_charona->m_currentDistance - meshex->m_charona->m_startDistance));
			}

			Matrix44f scaleMatrix;
			scaleMatrix.MakeScale(scaleFactor);
			matrixTempLocal = scaleMatrix * matrixTempLocal;

			// end scaleing
			m_pRenderStateMgr->SetWorldMatrix(&matrixTempLocal);
			m_pRenderStateMgr->SetDiffuseSource(meshex->m_charona->m_visual->m_advancedFixedMode);
			m_pRenderStateMgr->SetVertexType(meshex->m_charona->m_visual->m_uvCount);

			meshex->m_charona->m_visual->Render(g_pD3dDevice, m_pRenderStateMgr->m_currentVertexType);

			// end render it
			g_pD3dDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
		}
	}

	return TRUE;
}

BOOL ClientEngine::RenderOverlays(CViewportObj* pVO) {
	if (!pVO)
		return FALSE;

	//set projection
	Matrix44f matProj, origMatrix, matView;
	origMatrix.MakeIdentity();

	//initial offset
	Vector3f upVect(0, 1, 0);
	Vector3f dirVect(0, 0, 1);
	Vector3f init(0, 0, 0);
	SetViewMatrixAB(matView, init, dirVect, upVect);
	g_pD3dDevice->GetTransform(D3DTS_PROJECTION, reinterpret_cast<D3DMATRIX*>(&origMatrix));

	float aspectRatio = pVO->GetAspectRatio();
	CViewportObj::SetProjectionMatrix(
		matProj,
		.3f,
		aspectRatio,
		1.0f,
		10000.0f,
		1.0f);

	//	We want to render 3D text before we clear depth
	if (m_labelsOnTop)
		g_pD3dDevice->SetRenderState(D3DRS_ZENABLE, FALSE);
	else
		g_pD3dDevice->SetRenderState(D3DRS_ZENABLE, TRUE);

	RenderTextCache();
	m_pRenderStateMgr->SetViewMatrix(&matView);
	g_pD3dDevice->SetRenderState(D3DRS_ZENABLE, FALSE);
	m_pRenderStateMgr->SetBlendType(1);

	for (POSITION ovrPos = m_inViewEntityList->GetHeadPosition(); ovrPos != NULL;) {
		CDynamicOverlayObj* newPtr = (CDynamicOverlayObj*)m_inViewEntityList->GetNext(ovrPos);
		Matrix44f initMatCopy;
		initMatCopy.MakeTranslation(newPtr->m_screenPos);
		Matrix44f scaleMatrix;
		scaleMatrix.MakeScale(newPtr->m_multiplier);
		initMatCopy = scaleMatrix * initMatCopy;
		m_pRenderStateMgr->SetWorldMatrix(&initMatCopy);
		POSITION iconPos = m_iconDatabase->FindIndex(5);
		if (iconPos) {
			CIconSysObj* iconSys = (CIconSysObj*)m_iconDatabase->GetAt(iconPos);
			iconSys->Render(g_pD3dDevice, m_pRenderStateMgr, m_pTextureDB);
		}
	}

	m_pRenderStateMgr->SetWorldMatrix(&Matrix44f::GetIdentity());
	g_pD3dDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);

	//reset view and projection matrices
	m_pRenderStateMgr->SetViewMatrix(&matView);
	m_pRenderStateMgr->SetProjectionMatrix(&origMatrix);

	return TRUE;
}

void ClientEngine::RenderViewportOutline(CViewportObj* pVO) {
	m_pRenderStateMgr->SetBlendType(1);

	// this ensures any previous calls are cleared (see RenderOverlays::RenderTextCache)
	m_pRenderStateMgr->SetTextureState(-1, 0, m_pTextureDB);

	if (m_runtimeViewportDB->GetCount() > 1) {
		CViewportObj* pVO_active = GetActiveViewport();
		if (pVO_active == pVO) {
			pVO->DrawOutline(g_pD3dDevice, 2, D3DCOLOR_ARGB(255, 255, 255, 0));
		} else {
			// it's a bit wasteful to render this outline every time even though this viewport isn't the active one
			// but this ensures we re-render the outline with slightly more transparent border in case the active viewport is behind this one
			pVO_active->DrawOutline(g_pD3dDevice, 2, D3DCOLOR_ARGB(64, 255, 255, 0));
		}
	}
}

void ClientEngine::RenderTextCache() {
	if (!m_textRenderList || !m_pRenderStateMgr || !m_pTextReadoutObj || !g_pD3dDevice)
		return;

	static CTextureEX fontTexture;

	m_pRenderStateMgr->SetBlendLevel(0, 0);
	for (int loop = 1; loop < m_pRenderStateMgr->m_multiTextureSupported; loop++) {
		m_pRenderStateMgr->SetBlendLevel(loop, -1);
		if (loop > 7)
			break;
	}

	m_pRenderStateMgr->SetBlendType(2);
	if (RectWidth(m_curModeRect) > 1000) {
		m_pTextReadoutObj->m_cgVertSpacing = m_readoutFontSpacing * .8f;
		m_pTextReadoutObj->m_cgFontSizeX = m_readoutFontX * .8f;
		m_pTextReadoutObj->m_cgFontSizeY = m_readoutFontY * .8f;
	} else {
		m_pTextReadoutObj->m_cgVertSpacing = m_readoutFontSpacing;
		m_pTextReadoutObj->m_cgFontSizeX = m_readoutFontX;
		m_pTextReadoutObj->m_cgFontSizeY = m_readoutFontY;
	}

	m_pTextReadoutObj->RenderTextCG(m_pRenderStateMgr, g_pD3dDevice);

	m_pRenderStateMgr->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_MATERIAL);
	m_pRenderStateMgr->SetRenderState(D3DRS_COLORVERTEX, FALSE);
	m_pRenderStateMgr->SetRenderState(D3DRS_LIGHTING, TRUE);
	for (int i = 1; i < 8; ++i)
		g_pD3dDevice->SetTexture(i, nullptr);

	m_pRenderStateMgr->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	for (POSITION pos = m_textRenderList->GetHeadPosition(); pos;) {
		// Get Overhead Label (CTextRenderObj)
		auto pTRO = (CTextRenderObj*)m_textRenderList->GetNext(pos);
		if (!pTRO)
			continue;

		pTRO->m_blendMode = 1;

		m_pRenderStateMgr->SetWorldMatrix(&(pTRO->m_worldMatrix));

		static bool bloaded = false;
		if (!bloaded) {
			fontTexture.SetForceFullRes(true);
			fontTexture.LoadTextureByName("Common\\512Font.dds", 0);
			bloaded = true;
		}

		g_pD3dDevice->SetTexture(0, fontTexture.GetD3DTextureIfLoadedOrQueueToLoad());

		m_pRenderStateMgr->SetBlendType(pTRO->m_blendMode);

		try {
			m_pRenderStateMgr->SetMaterial(&(pTRO->m_materialObj->m_useMaterial));
		} catch (...) {
			LogError("Uncaught exception in ClientEngine::RenderTextCache");
			return;
		}

		// Render Overhead Label
		pTRO->Render(g_pD3dDevice, m_pRenderStateMgr);
	}

	m_pRenderStateMgr->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
}

BOOL ClientEngine::LoadGlobalShaderLibrary(const std::string& fileName) {
	// use gamefiles folder for organization
	ScopedDirectoryChanger sdc("GameFiles\\");

	// end use gamefiles folder for organization
	BEGIN_CFILE_SERIALIZE(fl, fileName, false)
	CArchive the_in_Archive(fl, CArchive::load);
	if (m_cgGlobalShaderDB) {
		m_cgGlobalShaderDB->SafeDelete();
		delete m_cgGlobalShaderDB;
		m_cgGlobalShaderDB = 0;
	}

	the_in_Archive >> m_cgGlobalShaderDB;
	the_in_Archive.Close();
	END_CFILE_SERIALIZE(fl, m_logger, "LoadGlobalShaderLibrary")
	if (!serializeOk)
		return FALSE;

	m_cgGlobalShaderDB->ScaleForRuntimeCompatibility(m_pRenderStateMgr->m_supportBUMPENVMAP);

	return TRUE;
}

BOOL ClientEngine::LoadPixelShaderLibrary(const std::string& fileName) {
	// use gamefiles folder for organization
	ScopedDirectoryChanger sdc("GameFiles\\");

	// end use gamefiles folder for organization
	BEGIN_CFILE_SERIALIZE(fl, fileName, false)
	CArchive the_in_Archive(fl, CArchive::load);
	if (m_pxShaderSystem) {
		m_pxShaderSystem->SafeDelete();
		delete m_pxShaderSystem;
		m_pxShaderSystem = 0;
	}

	the_in_Archive >> m_pxShaderSystem;
	the_in_Archive.Close();
	END_CFILE_SERIALIZE(fl, m_logger, "LoadPixelShaderLibrary")
	if (!serializeOk)
		return FALSE;

	m_pxShaderSystem->Compile(g_pD3dDevice);

	// since we deleted it, folks have dangling ptrs, reset it now
	m_pRenderStateMgr->SetPxShader(m_pxShaderSystem, -1);

	return TRUE;
}

BOOL ClientEngine::LoadShaderLibrary(const std::string& fileName) {
	// use gamefiles folder for organization
	ScopedDirectoryChanger sdc("GameFiles\\");

	// end use gamefiles folder for organization
	BEGIN_CFILE_SERIALIZE(fl, fileName, false)
	CArchive the_in_Archive(fl, CArchive::load);
	if (m_shaderSystem) {
		m_shaderSystem->SafeDelete();
		delete m_shaderSystem;
		m_shaderSystem = 0;
	}

	the_in_Archive >> m_shaderSystem;
	the_in_Archive.Close();
	END_CFILE_SERIALIZE(fl, m_logger, "LoadShaderLibrary")
	if (!serializeOk)
		return FALSE;

	if (m_shaderSupport)
		m_shaderSystem->Compile(g_pD3dDevice);

	// since we deleted it, folks have dangling ptrs, reset it now
	m_pRenderStateMgr->SetVertexShader(m_shaderSystem, m_globalWorldShader);

	return TRUE;
}

BOOL ClientEngine::SetViewMatrixAB(Matrix44f& mat, Vector3f& vFrom, Vector3f& vAt, Vector3f& vWorldUp) {
	Vector3f vView = vAt; // - vFrom;

	// Get the dot product, and calculate the projection of the z
	// basis
	// vector onto the up vector. The projection is the y basis
	// vector.
	FLOAT fDotProduct = vWorldUp.Dot(vView);
	Vector3f vUp = vWorldUp - fDotProduct * vView;
	Vector3f vRight = Cross(vUp, vView);

	// Start building the matrix. The first three rows contains the
	// basis
	// vectors used to rotate the view to point at the lookat point
	mat.MakeIdentity();
	mat(0, 0) = vRight.x;
	mat(0, 1) = vUp.x;
	mat(0, 2) = vView.x;
	mat(1, 0) = vRight.y;
	mat(1, 1) = vUp.y;
	mat(1, 2) = vView.y;
	mat(2, 0) = vRight.z;
	mat(2, 1) = vUp.z;
	mat(2, 2) = vView.z;

	// Do the translation values (rotations are still about the
	// eyepoint)
	mat(3, 0) = -vFrom.Dot(vRight);
	mat(3, 1) = -vFrom.Dot(vUp);
	mat(3, 2) = -vFrom.Dot(vView);
	return TRUE;
}

bool ClientEngine::IsFullScreen() {
	DWORD dwOldStyle = ::GetWindowLong(m_hWnd, GWL_STYLE);
	bool bIsFullScreen = (dwOldStyle & WS_CAPTION) == 0;
	return bIsFullScreen;
}

void ClientEngine::SetFullScreen(bool bFullScreen) {
	DWORD dwNewStyle;
	DWORD dwNewExStyle;
	RECT newPosition;
	bool bMaximizeWindow = false;

	struct WindowedSettings {
		WINDOWPLACEMENT m_Placement;
		DWORD m_dwStyle;
		DWORD m_dwExStyle;
	};
	static WindowedSettings s_WindowedSettings;

	DWORD dwOldStyle = ::GetWindowLong(m_hWnd, GWL_STYLE);
	bool bWasFullScreen = (dwOldStyle & WS_CAPTION) == 0;
	if (bFullScreen == bWasFullScreen)
		return; // Already in requested mode.

	if (bWasFullScreen) {
		LogInfo("Entering windowed mode");
		dwNewStyle = s_WindowedSettings.m_dwStyle;
		dwNewExStyle = s_WindowedSettings.m_dwExStyle;
		newPosition = s_WindowedSettings.m_Placement.rcNormalPosition;
		bMaximizeWindow = (s_WindowedSettings.m_Placement.showCmd & SW_SHOWMAXIMIZED) == SW_SHOWMAXIMIZED;
	} else {
		LogInfo("Entering full-screen mode");
		DWORD dwOldExStyle = ::GetWindowLong(m_hWnd, GWL_EXSTYLE);
		dwNewStyle = dwOldStyle & ~(WS_BORDER | WS_CAPTION | WS_THICKFRAME | WS_DLGFRAME);
		dwNewExStyle = dwOldExStyle & ~(WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE);
		s_WindowedSettings.m_dwStyle = dwOldStyle;
		s_WindowedSettings.m_dwExStyle = dwOldExStyle;

		s_WindowedSettings.m_Placement.length = sizeof(s_WindowedSettings.m_Placement);
		GetWindowPlacement(&s_WindowedSettings.m_Placement); // Save old position.

		// Full-screen seems to force the window to the primary monitor.
		// For consistent behavior, we will position the window base on the size and
		// position of the primary monitor.
		HMONITOR hMonitor = MonitorFromWindow(NULL, MONITOR_DEFAULTTOPRIMARY);
		if (!hMonitor) {
			LogError("No monitor detected for full screen mode.");
			return;
		}
		MONITORINFO monitorInfo;
		monitorInfo.cbSize = sizeof(monitorInfo);
		if (!GetMonitorInfo(hMonitor, &monitorInfo)) {
			LogError("Could not get monitor info");
			return;
		}
		newPosition = monitorInfo.rcMonitor;
		LogInfo("Monitor position: " << monitorInfo.rcMonitor.left << " " << monitorInfo.rcMonitor.top << " " << monitorInfo.rcMonitor.right << " " << monitorInfo.rcMonitor.bottom << " ");
	}

	::SetWindowLong(m_hWnd, GWL_STYLE, dwNewStyle);
	::SetWindowLong(m_hWnd, GWL_EXSTYLE, dwNewExStyle);
	::SetWindowPos(m_hWnd, wndNoTopMost, newPosition.left, newPosition.top, newPosition.right - newPosition.left, newPosition.bottom - newPosition.top, SWP_FRAMECHANGED | SWP_SHOWWINDOW | SWP_NOCOPYBITS);

	if (bMaximizeWindow)
		::ShowWindow(m_hWnd, SW_SHOWMAXIMIZED);
}

bool ClientEngine::RenderMenus() {
	// Get Menu Blade
	auto pMB = ClientBladeFactory::Instance()->GetMenuBlade();
	if (!pMB)
		return false;

	// Set Menu Render Options
	pMB->SetRenderOptions(m_menuRenderOptions);

	// Render Menus
	IClientRenderBlade* pCRB = dynamic_cast<IClientRenderBlade*>(pMB);
	if (!pCRB)
		return false;

	pCRB->Update();
	pCRB->Render();
	return true;
}

// Mini-render loop: menu system event/rendering only
bool ClientEngine::RenderLoop_MenusOnly(bool bProcessWndMsgs) {
	if (ClientIsEnabled()) {
		// Handle All Messages Coming From Server
		BYTE* pData;
		NETID netIdFrom;
		size_t dataSize;
		bool bDisposeMessage;
		while (m_multiplayerObj->MsgRx(pData, dataSize, netIdFrom)) {
			if (!pData || !dataSize)
				continue;
			HandleMsgs(pData, netIdFrom, dataSize, bDisposeMessage);
			if (bDisposeMessage)
				delete pData;
		}
	}

	// Handle events
	m_dispatcher.ProcessEventsUntil(getDispatchTime());

	// Is Client Window In View ?
	if (IsInView()) {
		// Render Menus
		bool beginSceneSucceeded = true;
		if (FAILED(g_pD3dDevice->BeginScene())) {
			beginSceneSucceeded = false;
			if (!FAILED(g_pD3dDevice->EndScene())) {
				if (!FAILED(g_pD3dDevice->BeginScene()))
					beginSceneSucceeded = true;
			}
		}
		if (beginSceneSucceeded) {
			RenderMenus();
			g_pD3dDevice->EndScene();
		}

		// Present (swap back and front framebuffers)
		HRESULT err = g_pD3dDevice->Present(nullptr, nullptr, nullptr, nullptr);
		if (FAILED(err))
			LogError("Frame End - D3D Error Code = " << err);
	}

	// Process Message Loop ?
	if (!bProcessWndMsgs)
		return true;

	// Pump Messages Until Empty (10 messages max)
	if (!::MsgProcPumpUntilEmpty(10)) {
		// Quit Client
		LogFatal("MsgProcPumpUntilEmpty() FAILED - Shutting Down ...");
		::PostQuitMessage(0);
		return false;
	}

	return true;
}

void ClientEngine::RenderPhysics() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "RenderPhysics");

	if (m_pPhysicsDebugWindow && !m_pPhysicsDebugWindow->IsValid()) {
		m_pPhysicsDebugWindow.reset();
		m_iShowPhysicsWindow = 0;
	}

	if (m_iShowPhysicsWindow == 1) {
		if (!m_pPhysicsDebugWindow) {
			Physics::DebugWindowOptions opts;
			opts.m_hWndParent = AfxGetMainWnd()->GetSafeHwnd();
			m_pPhysicsDebugWindow = Physics::CreateDebugWindow(opts);
		}
		m_pPhysicsDebugWindow->SetTitle("Ghysics\xE2\x84\xA2 (New and improved)");

		m_pPhysicsDebugWindow->Show(true);

		POSITION pos = m_runtimeViewportDB->GetHeadPosition();
		if (pos) {
			auto pVO = (CViewportObj*)m_runtimeViewportDB->GetNext(pos);

			const Matrix44f& mtxCamera = pVO->m_camWorldMatrix;

			Physics::CameraProperties camProps;
			camProps.m_ptCameraPositionMeters = FeetToMeters(mtxCamera.GetTranslation());
			camProps.m_vViewDirection = mtxCamera.GetRowZ();
			camProps.m_vViewUp = mtxCamera.GetRowY();

			const Matrix44f& mtxProj = pVO->m_projMatrix;

			camProps.m_fRelativeViewWidth = 1 / mtxProj(0, 0);
			camProps.m_fRelativeViewHeight = 1 / mtxProj(1, 1);
			camProps.m_fNear = 2.0f;
			camProps.m_fFar = 4096.0f;
			m_pPhysicsDebugWindow->SetCameraProperties(camProps);
			if (m_pPhysicsDebugWindow->RenderBegin()) {
				m_pPhysicsDebugWindow->Render(m_pUniverse.get());
				CMovementObj* pMovementObj = GetMovementObjMe();
				if (pMovementObj) {
					Matrix44f mtxAvatar = pMovementObj->getBaseFrameMatrix();
					mtxAvatar.GetTranslation() = FeetToMeters(mtxAvatar.GetTranslation());
					m_pPhysicsDebugWindow->Render(pMovementObj->getPhysicsAvatar().get(), mtxAvatar);
				}
				m_pPhysicsDebugWindow->RenderEnd();
			}
		}
	} else {
		if (m_pPhysicsDebugWindow) {
			m_pPhysicsDebugWindow->Show(false);
		}
	}
}

void ClientEngine::DrawRenderLists(CViewportObj* pVO, const ScenePassOptions& aPassOptions) {
	FrameTimeNestedContext ftnc1(GetFrameTimeProfiler(), "DrawRenderLists");

	if (!pVO || !m_runtimeLightSys)
		return;

	auto pRD = GetReDeviceState();
	if (!pRD)
		return;

	m_runtimeLightSys->GetLightsRenderEngine(pRD);

	Matrix44f camTransform = pVO->m_camWorldMatrix;
	Vector3f camPosition = pVO->m_camWorldPosition;

	BOOL mirrorEnable = FALSE;

#if (DRF_WORLD_ENVIRONMENT_LIST == 1)
	RenderEnvironmentAttachedObjects(pVO, FALSE); // Environment backdrop renderings
#endif

	if (!m_pRenderStateMgr || !m_renderContainerAlphaFilterList || !m_renderContainerList)
		return;

	// Get Viewport Camera
	auto pCO = pVO->GetCamera();
	if (!pCO)
		return;

	auto camFovY = pVO->GetCameraFovY();

	m_pRenderStateMgr->SetZWritableState(TRUE);
	m_renderContainerAlphaFilterList->m_alphaMode = TRUE;
	m_renderContainerAlphaFilterList->Render(
		g_pD3dDevice,
		mirrorEnable,
		m_mirrorTransform,
		m_pRenderStateMgr,
		m_pTextureDB,
		FALSE,
		camPosition,
		camTransform,
		camFovY);

	m_renderContainerList->Render(
		g_pD3dDevice,
		mirrorEnable,
		m_mirrorTransform,
		m_pRenderStateMgr,
		m_pTextureDB,
		TRUE,
		camPosition,
		camTransform,
		camFovY);

	// clean up after multitexturing
	pRD->ResetTextureState();

	auto pRDS = pRD->GetRenderState();
	if (!pRDS)
		return;

	// limit max lights for performance
	pRDS->SetMaxLights(ReD3DX9DEVICEMAXUPLOADLIGHTS);

	auto pMRM = MeshRM::Instance();
	if (!pMRM)
		return;

	{
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "RenderMeshCache-0");
		// render static opaque non-multitextured geometry
		pMRM->RenderMeshCache(MeshRM::RMC_ID_0, false);
	}

	{
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "RenderMeshCache-4");
		// render static opaque multitextured geometry
		pMRM->RenderMeshCache(MeshRM::RMC_ID_4, false);
	}

	// clean up after multitexturing
	pRD->ResetTextureState();

	{
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "RenderMeshCache-1");
		// render 1-bit alpha transparent geometry
		pMRM->RenderMeshCache(MeshRM::RMC_ID_1, false);
	}

	{
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "RenderMeshCache-3");
		// render fixed func pipeline geometry
		pMRM->RenderMeshCache(MeshRM::RMC_ID_3, false);
	}

	{
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "RenderMeshCache-5");
		// render the single textured entities...assume CPU skinning (no sorting by vertex shader!!!)
		pMRM->RenderMeshCache(MeshRM::RMC_ID_5, false);
	}

	{
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "RenderMeshCache-6");
		// render the multitextured entities...assume CPU skinning (no sorting by vertex shader!!!)
		pMRM->RenderMeshCache(MeshRM::RMC_ID_6, false);
	}

	// clean up after multitexturing
	pRD->ResetTextureState();

	// render any legacy transparent geometry here
	m_pRenderStateMgr->SetZWritableState(FALSE);

	// DRF - Added
	if (!m_translucentPolyList)
		return;

	// RENDER PARTICLE SYSTEMS
	m_translucentPolyList->m_alphaMode = TRUE;
	ParticleSystem(1, camPosition, camTransform, camFovY);

	m_translucentPolyList->Render(
		g_pD3dDevice,
		mirrorEnable,
		m_mirrorTransform,
		m_pRenderStateMgr,
		m_pTextureDB,
		FALSE,
		camPosition,
		camTransform,
		camFovY);

	m_pRenderStateMgr->SetZWritableState(TRUE);

	// clean up after multitexturing
	pRD->ResetTextureState();

	{
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "RenderMeshCache-7");
		// render full alpha transparent geometry in sort order
		pMRM->RenderMeshCache(MeshRM::RMC_ID_7, false);
	}

	{
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "RenderMeshCache-2");
		// render full alpha transparent geometry
		pMRM->RenderMeshCache(MeshRM::RMC_ID_2, false);
	}

	m_pRenderStateMgr->SetZWritableState(FALSE);

	if (aPassOptions.drawWithSpotShadows && GetShadows())
		RenderSpotShadows();

	if (m_lightningSystem)
		m_lightningSystem->RenderSystem(camPosition, g_pD3dDevice, m_pRenderStateMgr, m_pTextureDB);

	m_pRenderStateMgr->SetVertexShader(0, -1);
	m_pRenderStateMgr->SetPxShader(0, -1);

	IDirect3DDevice9* pD3DDevice = pRD->GetDevice();
	if (!pD3DDevice)
		return;

	HighlightsRender();
	OutlinesRender();

	// Reset textures to a state expected by later code.
	for (DWORD i = 0; i < ReDEVICEMAXTEXTURESTAGES; ++i) {
		pD3DDevice->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_DISABLE);
		pD3DDevice->SetTextureStageState(i, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
		pD3DDevice->SetTexture(i, nullptr);
	}
}

void ClientEngine::WidgetsRender() {
	if (m_kepWidget && m_kepWidget->visible()) {
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "WidgetsRender");

		m_kepWidget->render();

		// Clear zbuffer and then draw widget overlays
		g_pD3dDevice->Clear(0, nullptr, D3DCLEAR_ZBUFFER, 0, 1.0f, 0);

		g_pD3dDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
		g_pD3dDevice->SetRenderState(D3DRS_ZENABLE, TRUE);
		g_pD3dDevice->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
		g_pD3dDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
		g_pD3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

		Matrix44f matView;
		g_pD3dDevice->GetTransform(D3DTS_VIEW, reinterpret_cast<D3DMATRIX*>(&matView));

		D3DLIGHT9 light;
		ZeroMemory(&light, sizeof(light));

		light.Type = D3DLIGHT_DIRECTIONAL;
		light.Diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1);
		light.Range = 10000.0f;

		Vector3f tmp = matView.GetColumnZ();
		reinterpret_cast<Vector3f&>(light.Direction) = tmp.GetNormalized();
		g_pD3dDevice->LightEnable(0, TRUE);
		g_pD3dDevice->SetLight(0, &light);

		// Disable all other lights
		for (UINT i = 1; i < ReD3DX9DEVICEMAXUPLOADLIGHTS; i++)
			g_pD3dDevice->LightEnable(i, FALSE);

		// Reset texture stage states
		for (UINT i = 0; i < ReDEVICEMAXTEXTURESTAGES; i++) {
			g_pD3dDevice->SetTextureStageState(i, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
			g_pD3dDevice->SetTextureStageState(i, D3DTSS_ALPHAARG1, D3DTA_CURRENT);
		}

		// Render widgets with our own lighting
		MeshRM::Instance()->RenderMeshCache(MeshRM::RMC_ID_8, true);
	}
}

void ClientEngine::RenderSpotShadows() {
	if (!m_shadowEntityList)
		return;

	g_pD3dDevice->SetTransform(D3DTS_WORLD, reinterpret_cast<const D3DMATRIX*>(&Matrix44f::GetIdentity()));

	m_pRenderStateMgr->SetLightingMode(FALSE);
	m_pRenderStateMgr->SetShadeMode(1);
	m_pRenderStateMgr->SetCullState(1);
	m_pRenderStateMgr->SetDiffuseSource(0);
	m_pRenderStateMgr->SetBlendType(4);
	m_pRenderStateMgr->SetTextureTileMode(2);
	m_pRenderStateMgr->SetTextureState(m_shadowIndexTemp, 0, m_pTextureDB);
	m_pRenderStateMgr->SetTextureState(-1, 1, m_pTextureDB);
	m_pRenderStateMgr->SetTextureState(-1, 2, m_pTextureDB);
	m_pRenderStateMgr->SetTextureState(-1, 3, m_pTextureDB);
	m_pRenderStateMgr->SetTextureState(-1, 4, m_pTextureDB);
	m_pRenderStateMgr->SetTextureState(-1, 5, m_pTextureDB);
	m_pRenderStateMgr->SetTextureState(-1, 6, m_pTextureDB);
	m_pRenderStateMgr->SetTextureState(-1, 7, m_pTextureDB);

	m_pRenderStateMgr->SetBlendLevel(0, 0); // REMOVE
	for (int loop = 1; loop < m_pRenderStateMgr->m_multiTextureSupported; loop++) {
		m_pRenderStateMgr->SetBlendLevel(loop, -1);
		if (loop > 7)
			break;
	}

	g_pD3dDevice->SetFVF(D3DFVF_XYZ | D3DFVF_TEX1);
	m_pRenderStateMgr->SetCullState(0);

	int vxCount = 0;
	if (m_shadowEntityList) {
		for (POSITION pos = m_shadowEntityList->GetHeadPosition(); pos;) {
			CMovementObj* entPtrLoc = (CMovementObj*)m_shadowEntityList->GetNext(pos);
			vxCount = 0;

			GetShadowPolyMeshSimple(m_pCollisionObj,
				m_shadowVertexBuffer,
				vxCount,
				entPtrLoc->getBaseFramePosition(),
				(entPtrLoc->getBoundingRadius() * .5f), // float radius,
				3.0f,
				10.0f);

			if (vxCount > 0) {
				g_pD3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST, vxCount / 3, m_shadowVertexBuffer, sizeof(ABVERTEX1LNT));
			}
		}
	}
	m_pRenderStateMgr->m_currentVertexType = 99;

	m_pRenderStateMgr->SetLightingMode(TRUE);
	m_pRenderStateMgr->SetShadeMode(0);
}

} // namespace KEP