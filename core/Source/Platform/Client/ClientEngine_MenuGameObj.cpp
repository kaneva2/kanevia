///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "Common\include\CoreStatic\SkeletonConfiguration.h"
#include "Common\include\CoreStatic\EquippableSystem.h"
#include "Common\include\CoreStatic\CDeformableMesh.h"
#include "RuntimeSkeleton.h"

namespace KEP {

static LogInstance("ClientEngine");

bool ClientEngine::SetMenuGameObjectEquippableAltCharConfig(CMovementObj* pMO, const GLID_OR_BASE& glidOrBase, size_t slot, int meshId, int matlId) {
	if (!pMO)
		return false;

	auto pRS = pMO->getSkeleton();
	if (!pRS || pRS->getEquippableItemCount() == 0)
		return false;

	auto pAIO = pRS->getEquippableItemByGLID(glidOrBase);
	if (pAIO) {
		if (!pAIO->getSkeleton())
			return true;

		SetMenuGameObjectEquippableAltCharConfig(pAIO->getSkeleton(), pAIO->getSkeletonConfig(), slot, meshId, matlId, glidOrBase);
		pMO->getSkeletonConfig()->SetEquippableCharConfigBlock(glidOrBase, slot, meshId, matlId, pRS->getSkinnedMeshCount());
		CMovementObj* pMORM = m_movObjRefModelList->getObjByIndex(pMO->getIdxRefModel());
		if (pMORM)
			pMO->getSkeletonConfig()->RealizeDimConfig(pRS, pMORM->getSkeletonConfig()->m_meshSlots);
		return true;
	}

	// Look into the pending equip list - fixes missing head issue
	return pRS->configPendingEquippableItemMeshSlot(glidOrBase, slot, meshId, matlId);
}

CMenuGameObject* ClientEngine::AddMenuGameObject(int game_object_index, int animation_index) {
	CMenuGameObject* pMGO_new = new CMenuGameObject(game_object_index, animation_index);
	if (pMGO_new->GetMovementObject()) {
		m_pMenuGameObjects->Add(pMGO_new);
	} else {
		delete pMGO_new;
		pMGO_new = 0;
	}
	return pMGO_new;
}

CMenuGameObject* ClientEngine::AddMenuGameObjectByEquippableGlid(long long glid) {
	IResource* pRes = NULL;
	pRes = m_equippableRM->GetResource(glid);
	if (pRes && pRes->GetResourceType() == ResourceType::EQUIPPABLE) {
		CArmedInventoryProxy* p = dynamic_cast<CArmedInventoryProxy*>(pRes);
		if (p) {
			CMenuGameObject* pMGO_new = new CMenuGameObject(p);
			m_pMenuGameObjects->Add(pMGO_new);
			return pMGO_new;
		}

		return NULL;
	}
	return NULL;
}

CMenuGameObject* ClientEngine::AddMenuDynamicObjectByPlacement(int placementId) {
	CDynamicPlacementObj* pDPO = GetDynamicObject(placementId);
	return pDPO ? AddMenuDynamicObject(pDPO) : nullptr;
}

CMenuGameObject* ClientEngine::AddMenuDynamicObject(CDynamicPlacementObj* pDPO) {
	if (!pDPO)
		return NULL;

	StreamableDynamicObject* pSDO = pDPO->GetBaseObj();
	if (!pSDO) {
		LogError("GetBaseObj() FAILED");
		return NULL;
	}

	std::unique_ptr<CDynamicPlacementObj> pDPO_new = CreateDynamicObjectWithPlacementId(pDPO->GetGlobalId(), -1);
	if (pDPO_new == nullptr) {
		LogError("CreateObjectPlacement() FAILED");
		ASSERT(false);
		return nullptr;
	}

	pDPO_new->GetBaseObj();

	std::string customTextureUrl = pDPO->GetCustomTextureUrl();
	if (!customTextureUrl.empty()) {
		pDPO_new->SetCustomTexture(true, std::make_shared<CustomTextureProvider>(customTextureUrl));
		pDPO_new->UpdateCustomMaterials();
	}

	for (size_t i = 0; i < pDPO->GetBaseObj()->getVisualCount(); i++) {
		CMaterialObject* pMaterial = pDPO->GetMaterial(i);
		if (pMaterial)
			pDPO_new->SetMaterial(i, pMaterial);
	}

	CMenuGameObject* pMGO_new = new CMenuGameObject(std::move(pDPO_new));
	m_pMenuGameObjects->Add(pMGO_new);
	return pMGO_new;
}

bool ClientEngine::CreateCharacterOnServerFromMenuGameObject(
	IGetSet* igs_CMenuGameObject,
	int spawnIndex,
	const std::string& name,
	int nTeam,
	int scaleX,
	int scaleY,
	int scaleZ) {
	auto pMGO = (CMenuGameObject*)igs_CMenuGameObject;
	if (!pMGO || !pMGO->GetMovementObject())
		return false;
	return CreateCharacterOnServer(pMGO->GetMovementObject(), spawnIndex, name, nTeam, scaleX * 1.0f / 100, scaleY * 1.0f / 100, scaleZ * 1.0f / 100);
}

bool ClientEngine::SetMenuGameObjectEquippableAltCharConfig(
	RuntimeSkeleton* pSkeleton,
	SkeletonConfiguration* pSkeletonCfg,
	int baseSlot,
	int meshId,
	int matlId,
	const GLID_OR_BASE& glidOrBase) {
	if (pSkeleton == nullptr || pSkeleton->getSkinnedMeshCount() <= baseSlot)
		return false;

	CArmedInventoryObj* pEquipRef = m_equippableRM->GetProxy(glidOrBase, true)->GetData();
	if (pEquipRef == nullptr) {
		return false;
	}

	RuntimeSkeleton* pSkeletonRef = pEquipRef->getSkeleton();
	SkeletonConfiguration* pSkeletonCfgRef = pEquipRef->getSkeletonConfig();
	if (pSkeletonRef == nullptr || pSkeletonCfgRef == nullptr || pSkeletonCfgRef->m_meshSlots == nullptr) {
		return false;
	}

	POSITION alternateConfigPos;
	alternateConfigPos = pSkeletonCfgRef->m_meshSlots->FindIndex(baseSlot);
	if (!alternateConfigPos)
		return false;

	CAlterUnitDBObject* pAlterUnit;
	pAlterUnit = static_cast<CAlterUnitDBObject*>(pSkeletonCfgRef->m_meshSlots->GetAt(alternateConfigPos));
	POSITION currentConfigPos;
	currentConfigPos = pAlterUnit->m_configurations->FindIndex(meshId);
	if (!currentConfigPos)
		return false;

	if (!pSkeletonCfg->HotSwapDim(baseSlot, meshId, pSkeletonRef->getSkinnedMeshCount(), false))
		return false;

	if (!pSkeletonCfg->HotSwapMat(baseSlot, meshId, matlId))
		return false;

	pSkeletonCfg->m_charConfig.setItemSlotMeshId(GOB_BASE_ITEM, baseSlot, meshId);
	pSkeletonCfg->m_charConfig.setItemSlotMaterialId(GOB_BASE_ITEM, baseSlot, matlId); // drf - bug fix was SetMeshId()
	pSkeletonCfg->RealizeDimConfig(pSkeleton, pSkeletonCfgRef->m_meshSlots);
	return true;
}

bool ClientEngine::SetMenuGameObjectAltCharConfig(IGetSet* pGS_MGO, int baseSlot, int meshId, int matlId) {
	auto pMGO = dynamic_cast<CMenuGameObject*>(pGS_MGO);
	if (!pMGO)
		return false;
	return SetRuntimeSkeletonAltCharConfig(pMGO->GetMovementObject()->getSkeleton(), pMGO->GetMovementObject()->getSkeletonConfig(), baseSlot, meshId, matlId, pMGO->GetMovementObject()->getIdxRefModel());
}

} // namespace KEP