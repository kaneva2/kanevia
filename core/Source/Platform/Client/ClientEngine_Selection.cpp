///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"

namespace KEP {

static LogInstance("ClientEngine");

void ClientEngine::ObjectLockSelection(BOOL lock) {
	m_selectionLocked = lock;
}

void ClientEngine::SelectionListAdd(const SelectionEntry& entry) {
	m_selectionList.push_back(entry);
}

void ClientEngine::SelectionListClear() {
	m_selectionList.clear();
}

void ClientEngine::SelectionListDel(const selection_t::iterator& itr) {
	m_selectionList.erase(itr);
}

size_t ClientEngine::SelectionListSize() const {
	return m_selectionList.size();
}

int ClientEngine::ObjectsSelected() const {
	return static_cast<int>(SelectionListSize());
}

void ClientEngine::ObjectAddToSelection(const ObjectType& objType, int objId, bool reCalcPivot) {
	if (m_selectionLocked)
		return;

	if (!ObjectIsSelected(objType, objId)) {
		// Add Object To Selection List
		SelectionListAdd(SelectionEntry(objType, objId));

		// Send Select Event
		IEvent* selectEvent = m_dispatcher.MakeEvent(m_selectEventId);
		if (selectEvent) {
			*selectEvent->OutBuffer() << 1 << (int)objType << objId << 1;
			m_dispatcher.QueueEvent(selectEvent);
		}
	}

	if (reCalcPivot)
		ReCalcSelectionPivot();
}

void ClientEngine::ObjectRemoveFromSelection(const ObjectType& objType, int objId, bool reCalcPivot) {
	if (m_selectionLocked)
		return;

	for (selection_t::iterator itr = m_selectionList.begin(); itr != m_selectionList.end(); ++itr) {
		if ((itr->objId != objId) || (itr->objType != objType))
			continue;

		// Remove Object From Selection List
		m_selectionList.erase(itr);

		// Send Select Event
		IEvent* selectEvent = m_dispatcher.MakeEvent(m_selectEventId);
		if (selectEvent) {
			*selectEvent->OutBuffer() << 1 << (int)objType << objId << 0;
			m_dispatcher.QueueEvent(selectEvent);
		}

		return;
	}

	if (reCalcPivot)
		ReCalcSelectionPivot();
}

void ClientEngine::ObjectToggleSelected(const ObjectType& objType, int objId, bool reCalcPivot) {
	if (m_selectionLocked)
		return;

	for (selection_t::iterator itr = m_selectionList.begin(); itr != m_selectionList.end(); ++itr) {
		if ((itr->objId != objId) || (itr->objType != objType))
			continue;

		// Remove Object From Selection List
		SelectionListDel(itr);

		// Send Select Event
		IEvent* selectEvent = m_dispatcher.MakeEvent(m_selectEventId);
		if (selectEvent) {
			*selectEvent->OutBuffer() << 1 << (int)objType << objId << 0;
			m_dispatcher.QueueEvent(selectEvent);
		}

		if (reCalcPivot)
			ReCalcSelectionPivot();

		return;
	}

	// Add Object To Selection List
	SelectionListAdd(SelectionEntry(objType, objId));

	if (reCalcPivot)
		ReCalcSelectionPivot();

	// Send Select Event
	IEvent* selectEvent = m_dispatcher.MakeEvent(m_selectEventId);
	if (selectEvent) {
		*selectEvent->OutBuffer() << 1 << (int)objType << objId << 1;
		m_dispatcher.QueueEvent(selectEvent);
	}
}

void ClientEngine::ObjectReplaceSelection(const ObjectType& objType, int objId) {
	if (m_selectionLocked)
		return;

	IEvent* selectEvent = m_dispatcher.MakeEvent(m_selectEventId);
	if (selectEvent) {
		if (ObjectIsSelected(objType, objId)) {
			if (SelectionListSize() > 1) {
				// N - 1 objects deselected
				*selectEvent->OutBuffer() << static_cast<int>(m_selectionList.size()) - 1;

				// Add deselected objects to event
				for (const auto& se : m_selectionList)
					if ((se.objType != objType) || (se.objId != objId))
						*selectEvent->OutBuffer() << (int)se.objType << se.objId << 0;
			} else {
				// No need to send an event when we're replacing the same object
				selectEvent->Delete();
				selectEvent = NULL;
			}
		} else {
			// N objects deselected + 1 object selected
			*selectEvent->OutBuffer() << static_cast<int>(m_selectionList.size()) + 1;

			// Add deselected objects to event
			for (const auto& se : m_selectionList)
				*selectEvent->OutBuffer() << (int)se.objType << se.objId << 0;

			// Add selected object to event
			*selectEvent->OutBuffer() << (int)objType << objId << 1;
		}
	}

	// Clear Entire Selection List
	SelectionListClear();

	// Add Object To Selection List
	SelectionListAdd(SelectionEntry(objType, objId));

	ReCalcSelectionPivot();

	// Send Select Event
	if (selectEvent)
		m_dispatcher.QueueEvent(selectEvent);
}

void ClientEngine::ObjectClearSelection() {
	if (m_selectionLocked)
		return;

	if (SelectionListSize() <= 0)
		return;

	IEvent* selectEvent = m_dispatcher.MakeEvent(m_selectEventId);
	if (selectEvent) {
		*selectEvent->OutBuffer() << static_cast<int>(m_selectionList.size());
		for (const auto& se : m_selectionList)
			*selectEvent->OutBuffer() << (int)se.objType << se.objId << 0;
	}

	SelectionListClear();

	ReCalcSelectionPivot();

	if (selectEvent)
		m_dispatcher.QueueEvent(selectEvent);
}

bool ClientEngine::ObjectIsSelected(const ObjectType& objType, int objId) const {
	for (const auto& se : m_selectionList) {
		if ((se.objId == objId) && (se.objType == objType))
			return true;
	}
	return false;
}

void ClientEngine::ObjectGetSelection(ObjectType* objectType, int* objectID, unsigned int index) const {
	if (index < 0 || index >= SelectionListSize())
		return;
	*objectType = m_selectionList[index].objType;
	*objectID = m_selectionList[index].objId;
}

void ClientEngine::ObjectGetSelectionPivotOffset(float* x, float* y, float* z, int index) const {
	if (index < 0 || index >= SelectionListSize())
		return;
	*x = m_selectionList[index].pivotOffset.x;
	*y = m_selectionList[index].pivotOffset.y;
	*z = m_selectionList[index].pivotOffset.z;
}

// Puts SelectionPivot At Centroid Of Selected Objects! (used by widget)
void ClientEngine::ReCalcSelectionPivot() {
	// Find Selected Objects Centroid
	Vector3f pos;
	Vector3f vTotal(0, 0, 0);
	int count = 0;
	for (const auto& se : m_selectionList) {
		if (ObjectGetPosition(se.objType, se.objId, pos)) {
			vTotal += pos;
			++count;
		}
	}
	if (count > 0)
		m_selectionPivot = (vTotal / count);
	else
		m_selectionPivot.Set(0, 0, 0);

	// Set Selected Objects Pivot Offset From Centroid
	for (auto& se : m_selectionList) {
		if (ObjectGetPosition(se.objType, se.objId, pos))
			se.pivotOffset = pos - m_selectionPivot;
	}
}

} // namespace KEP