///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CFileFactory.h"
#include "Core/Util/Unicode.h"

#undef min
#undef max

namespace KEP {

const int ENCRYPT_KEY_LEN = 16;
const int ENCRYPT_MIN_BLOCK_SIZE = 4096; // rotating single-byte key per-4k block

// class used for de-serialing files encrypted by the editor
class DecryptingFileFactory : public CFileFactory {
public:
	DecryptingFileFactory(const char *key) {
		if (key)
			m_key = key;
	}
	void SetKey(const char *key) {
		m_key = key;
	}

	static unsigned char GetSalt(const char *hint) {
		unsigned char salt = 0;
		if (hint != NULL) { // use file name as salt - name must be consistent between write/read operation
			const char *p = strrchr(hint, '\\');
			if (p)
				p++;
			else
				p = hint;

			while (*p != 0) {
				salt = salt ^ (unsigned char)(*p++ & 0x1f);
			}
			salt %= ENCRYPT_KEY_LEN;
		}
		return salt;
	}

	static int GetKeyIndex(int byteOffset, unsigned char salt, ULONGLONG blockSize) {
		unsigned keyIndex = (unsigned)byteOffset / blockSize + salt;
		return keyIndex % ENCRYPT_KEY_LEN;
	}

	static ULONGLONG GetBlockSize(const char *fileName) {
		CFile fl;
		if (!fl.Open(Utf8ToUtf16(fileName).c_str(), CFile::modeRead)) {
			return ENCRYPT_MIN_BLOCK_SIZE;
		}

		return GetBlockSize(fl);
	}

	static ULONGLONG GetBlockSize(CFile &file) {
		ULONGLONG curPos = file.GetPosition();
		ULONGLONG fileSize = file.Seek(0, SEEK_END);
		file.Seek(curPos, SEEK_SET);

		ULONGLONG blockSize = fileSize / ENCRYPT_KEY_LEN;
		blockSize = std::max(blockSize / 4 * 4, (ULONGLONG)ENCRYPT_MIN_BLOCK_SIZE);
		return blockSize;
	}

protected:
	virtual CFile *getFile();
	CStringA m_key;
};

// if want it to generate a key, pass in null for key, and non-null newKey
// it will assert if both null
// key must be ENCRYPT_KEY_LEN bytes long, if not null
// retruns errno
int WriteEncryptionKeyFile(const char *baseDir, const char *key, std::string *newKey = 0);

// key must be ENCRYPT_KEY_LEN+1 bytes long (+1 for null)
// I'm only giving you t/f on retrun
bool ReadEncryptionKeyFile(const char *CurDir, CHAR *key);

} // namespace KEP
