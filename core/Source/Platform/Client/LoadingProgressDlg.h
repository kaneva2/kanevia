///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ClientDialogs.h"

namespace KEP {

class CLoadingProgressDlg : public CDialog {
	DECLARE_DYNAMIC(CLoadingProgressDlg)

public:
	CLoadingProgressDlg(CWnd* pParent = NULL); // standard constructor
	virtual ~CLoadingProgressDlg();

	// Dialog Data
	enum { IDD = IDD_LOADING_PROGRESS_DIALOG };

	void UpdateProgress(int percent);

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

private:
	int m_percentage;
	CProgressCtrl m_progressCtrl;
	CBrush m_blackBrush;
	CPen m_whitePen;
};

} // namespace KEP