///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"

namespace KEP {

static LogInstance("ClientEngine");

bool ClientEngine::SendChatMessage(eChatType chatType, const std::string& msg, bool fromIMClient) {
	if (!ClientIsEnabled())
		return false;

	if ((chatType == eChatType::System) || (chatType == eChatType::GM) || (chatType == eChatType::ArenaMgr)) {
		m_dispatcher.QueueEvent(new RenderTextEvent(msg, chatType));
		return true;
	}

	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	ULONG commandId;
	const char* remainder = nullptr;
	bool ok = m_textCommands.GetCommandId(msg.c_str(), commandId, remainder);
	if (!ok) {
		LogError("GetCommandId() FAILED - cmd='" << msg << "'");
		return false;
	} else if (fromIMClient && commandId == 0) { //Tray/IMC now only sends /<cmd> - YC July 2011
		LogError("GetCommandId() (fromClient && commandId==0) - cmd='" << msg << "'");
		return false;
	}

	LogInfo("commandId=" << commandId << " cmd='" << msg << "'");
	m_dispatcher.QueueEvent(new TextEvent(remainder, static_cast<SlashCommand>(commandId), pMO->getNetTraceId(), chatType, NULL, NULL, eChatType::Talk, fromIMClient));
	return true;
}

bool ClientEngine::SendChatMessageBroadcast(eChatType chatType, const std::string& msg, const char* toDistribution, const char* fromRecipient) {
	if (!ClientIsEnabled())
		return false;

	if (chatType != eChatType::Private)
		return false;

	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	ULONG commandId;
	const char* remainder = nullptr;
	m_textCommands.GetCommandId(msg.c_str(), commandId, remainder);

	m_dispatcher.QueueEvent(new TextEvent(remainder, static_cast<SlashCommand>(commandId), pMO->getNetTraceId(), chatType, toDistribution, fromRecipient));
	return true;
}

} // namespace KEP