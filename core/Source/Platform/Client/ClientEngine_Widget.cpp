///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

void ClientEngine::TurnOffWidget() {
	m_kepWidget->ModeOff();
}

void ClientEngine::setWidgetRotation(float x, float y, float z) {
	m_kepWidget->setRotation(x, y, z);
}

void ClientEngine::setWidgetPosition(float x, float y, float z) {
	m_kepWidget->setPosition(x, y, z);
}

void ClientEngine::widgetWorldSpace() {
	m_kepWidget->switchWorldSpace();
}

void ClientEngine::widgetObjectSpace() {
	m_kepWidget->switchObjectSpace();
}

void ClientEngine::widgetAdvancedMovement(bool adv) {
	m_kepWidget->advancedMovement(adv);
}

void ClientEngine::widgetShowWidget(bool show) {
	m_kepWidget->showWidget(show);
}

void ClientEngine::getWidgetPosition(float* x, float* y, float* z) {
	Vector3f pos = m_kepWidget->getWidgetPosition();
	*x = pos.x;
	*y = pos.y;
	*z = pos.z;
}

void ClientEngine::getWidgetRotation(float* x, float* y, float* z) {
	const Vector3f angle = m_kepWidget->getRotationAngle();
	*x = angle.x;
	*y = angle.y;
	*z = angle.z;
}

// DRF - ORDER MATTERS - Resets Position Which Script Also Sets!
void ClientEngine::ObjectDisplayTranslationWidget(const ObjectType& objType) {
	bool selected = (ObjectsSelected() > 0);
	if (selected && (objType == ObjectType::DYNAMIC || objType == ObjectType::SOUND || objType == ObjectType::EQUIPPABLE)) {
		ReCalcSelectionPivot();
		m_kepWidget->ModeTranslate(m_selectionPivot.x, m_selectionPivot.y, m_selectionPivot.z);
	} else {
		TurnOffWidget();
	}
}

// DRF - ORDER MATTERS - Resets Rotation Which Script Also Sets!
void ClientEngine::ObjectDisplayRotationWidget(const ObjectType& objType) {
	bool selected = (ObjectsSelected() > 0);
	if (selected && (objType == ObjectType::DYNAMIC || objType == ObjectType::SOUND || objType == ObjectType::EQUIPPABLE)) {
		ReCalcSelectionPivot();
		m_kepWidget->ModeRotate(m_selectionPivot.x, m_selectionPivot.y, m_selectionPivot.z);
	} else {
		TurnOffWidget();
	}
}

} // namespace KEP