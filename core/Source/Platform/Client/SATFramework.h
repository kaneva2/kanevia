///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <vector>
#include <fstream>

#include "ClientEngine.h"
#include "Log4CPlus\logger.h"

namespace KEP {

enum SATstate {
	idle,
	ready,
	executing
};

class SATFramework {
private:
	static SATFramework* s_this;
	SATFramework();

public:
	virtual ~SATFramework();
	static SATFramework* GetSATInstance();

private:
	std::string m_email;
	std::string m_password;

	IClientEngine* m_clientEngineInstance;
	IDispatcher* m_dispatcherInstance;
	Logger m_logger;

	SATstate m_myCurrentState;
	std::vector<std::string> m_testScriptsToRun;

	int m_indexAtInTestScriptExecution;

	bool m_clientSpawned;
	bool m_zoneLoaded;

	bool m_successfulCompletion;

public:
	void executeTestScript(int index);

	bool getSuccessfulCompletion() {
		return m_successfulCompletion;
	}
	void setSuccessfulCompletion(bool isSuccessful) {
		m_successfulCompletion = isSuccessful;
	}

public:
	static EVENT_PROC_RC StartClientTestingEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC TestScriptExecutionCompletedSuccessfullyEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);

	void LogMessage(std::string msg);

	void EndTestingAndCloseClient();
};

} // namespace KEP