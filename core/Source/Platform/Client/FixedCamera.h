///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

class FixedCamera : public CCameraObj {
public:
	FixedCamera(const Matrix44f& camMatrix, const Matrix44f& projMatrix) :
			CCameraObj("FixedCamera"),
			m_projectionMatrix(projMatrix) {
		m_camWorldVectorX = camMatrix.GetRowX();
		m_camWorldVectorY = camMatrix.GetRowY();
		m_camWorldVectorZ = camMatrix.GetRowZ();
		m_camWorldPosition = camMatrix.GetRowW();
	}

	virtual const Matrix44f* GetCustomProjectionMatrix() override {
		return &m_projectionMatrix;
	}

private:
	Matrix44f m_projectionMatrix;
};

} // namespace KEP