///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


// ED-8604 - Game Over - Woot! - 2016/11/08
// DRF - I'll miss this code ;)

#pragma once

#include <string>

#include "ClientEngine_CWinApp.h"
#include "ClientEngine.h"

// forward declarations
namespace RuntimeReporter {
struct CallbackStruct;
}
namespace CrashReporter {
struct CallbackStruct;
}

namespace KEP {

/** DRF
* This class is the base class for the KEPClient application.  A global singleton of this
* class allocates and initializes a ClientEngine instance on the heap which forms the basis
* of all client functionality. As well this class installs the application RuntimeReporter and
* CrashReporter.
*/
class ClientEngineApp : public ClientEngine_CWinApp {
	FILE* m_stdOut;
	FILE* m_stdErr;

	// Application State Enumeration
	enum STATE {
		STATE_STOPPED,
		STATE_RUNNING,
		STATE_CRASHED,
		STATES
	};
	STATE m_state; ///< application state

	std::string m_appVersion; ///< application.exe version string '##.##.##.##'
	std::string m_appPath; ///< application.exe path string '...\Kaneva\Star\####'

	std::string m_starPath; ///< star path string '...\Kaneva\Star\####'
	std::string m_starId; ///< star id string '####'

	std::string m_loginEmail; ///< auto login email string
	std::string m_loginPassword; ///< auto login password string

	std::string m_ttLang; ///< translate to language string 'eng'

	bool m_isProduction; ///< runtime is in production environment
	bool m_alreadyExists; ///< another client instance already exists
	bool m_crashReportSilent; ///< send crash reports silently (no gui)

	CR_SCREENSHOT m_crashReportScreenshot; ///< crash report screenshot setting

	int m_testGroup; ///< random client testing group

	DEV_MODE m_devMode; ///< registry selected enumerated developer mode

	int m_ripperDetected; ///< enumerated ripper detected

public:
	ClientEngineApp();

	virtual ~ClientEngineApp();

	/** DRF
	* Parses StartCfg.xml to get m_crashReportSilent only.
	*/
	void ParseStartCfgXml();

	/** DRF
	* Initializes the application. Graphics support is setup, a new ClientEngine is constructed which
	* will form the basis of the application, crash reporting is tied to this instance and the main
	* client window is shown. A call to CheckAppSemaphore() then determines if another client app instance
	* AlreadyExists() which is not allowed on production and if not then it calls super::InitInstance().
	*/
	BOOL InitInstance() override;

	/** DRF
	* Runs the application. Initializes crash reporting, sets the application state to 'Running', calls
	* super::Run(), and upon return sets the state to 'Stopped'.  Optionally, with
	* DRF_CATCH_ALL enabled all unhandled exceptions thrown during runtime are caught, written to the
	* logfile and reported via webcall.
	*/
	int Run() override;

	/** DRF
	* Checks for application semaphore 'GAME_ENGINE_<parentGameId>' already exists and sets m_alreadyExists.
	* We do not allow multiple clients running on production only.  In this case it displays
	* a message box to the user and returns false and the client should exit, otherwise it
	* returns true and the client may continue to run.
	*/
	bool CheckAppSemaphore();

	/** DRF
	* Return the version string of the application.exe. (eg. '##.##.##.##')
	*/
	std::string AppVersion() const {
		return m_appVersion;
	}

	/** DRF
	* Return the path of the application.exe. (eg. 'c:/.../kaneva/star/####')
	*/
	std::string AppPath() const {
		return m_appPath;
	}

	/** DRF
	* Return the star path of the runtime environment. (eg. 'c:/.../kaneva/star/####')
	*/
	std::string StarPath() const {
		return m_starPath;
	}

	/** DRF
	* Return the star identifier of the runtime environment. (eg. '####')
	*/
	std::string StarId() const {
		return m_starId;
	}

	/** DRF
	* Return the wok url of the runtime environment. (eg. 'http://wok.kaneva.com/kgp/')
	*/
	std::string WokUrl() const;

	/** DRF
	* Return the patch url of the runtime environment. (eg. 'http://patch.kaneva.com/<version>/')
	*/
	std::string PatchUrl() const;

	/** DRF
	* Return the auto login email string
	*/
	std::string LoginEmail() const;

	/** DRF
	* Return the auto login password string
	*/
	std::string LoginPassword() const;

	/** DRF
	* Return the translate to language string (eg. 'eng')
	*/
	std::string TTLang() const;

	/** DRF
	* Return whether or not the client app is in production environment.
	*/
	bool IsProduction() const {
		return m_isProduction;
	}

	/** DRF
	* Return whether or not another client app instance already exists. Set by CheckAppSemaphore().
	*/
	bool AlreadyExists() const {
		return m_alreadyExists;
	}

	/** DRF
	* Return if crash reports are delivered silently (no gui).
	*/
	bool CrashReportSilent() const {
		return m_crashReportSilent;
	}

	/** DRF
	* Return crash report screenshot setting.
	*/
	CR_SCREENSHOT CrashReportScreenshot() const {
		return m_crashReportScreenshot;
	}

	/** DRF
	* Return random client testing group.
	*/
	int TestGroup() const {
		return m_testGroup;
	}

	/** DRF
	* Return enumerated development mode.
	*/
	DEV_MODE DevMode() const {
		return m_devMode;
	}

	/** DRF
	* Return enumerated ripper detected.
	*/
	int RipperDetected();

	/** DRF
	* Sets the application state to STATE_RUNNING.
	* Optionally the client window coordinates are restored from the registry.
	* Returns true on success or false on failure.
	*/
	bool SetStateRunning(bool restoreWindowCoords);

	/** DRF
	* Sets the application state to STATE_STOPPED.
	* This function is called upon client shutdown during normal app destruction and indicates
	* that it was a clean shutdown or during non-'bona-fide' crashes (crashes with a known reason).
	* Optionally the client window coordinates are saved to the registry.
	* Returns true on success or false on failure.
	*/
	bool SetStateStopped(bool saveWindowCoords);

	/** DRF
	* Sets the application state to STATE_CRASHED.
	* This function is only called from the crash callback during 'bona-fide' crashes.
	* Returns true on success or false on failure.
	*/
	bool SetStateCrashed();

	/** DRF
	* Sets the registry HKEY_CURRENT_USER/Software/Kaneva/KEPClient/Status/Coords to the main
	* window coordinates as a string of long integers '<left> <top> <right> <bottom>'.
	* This function is called upon client shutdown to save the window coordinates to restore
	* during the next client launch by RestoreWindowCoords().
	* Returns true on success or false on failure to save to the registry value.
	*/
	bool SaveWindowCoords();

	/** DRF
	* Gets the registry HKEY_CURRENT_USER/Software/Kaneva/KEPClient/Status/Coords as a string
	* of long integers '<left> <top> <right> <bottom>' and sets the main window coordinates.
	* This function is called upon client initialization to restore the window coordinates as
	* saved during last client shutdown by SaveWindowCoords().
	* Returns true on success or false on failure to restore from the registry value.
	*/
	bool RestoreWindowCoords();
	bool GetWindowCoords(RECT& rect, bool adjStyle);

protected:
	DECLARE_MESSAGE_MAP()

	ULONG_PTR m_gdiplusToken;

public:
	// RuntimeReporter Support
	bool RR_Install(const std::string& runtimeId);
	void RR_AppCallback(RuntimeReporter::CallbackStruct& cbs);

	// CrashReporter Support
	bool CR_Install();
	void CR_AppCallback(CrashReporter::CallbackStruct& cbs);
};

extern ClientEngineApp g_ceApp; // singleton app global instance

} // namespace KEP