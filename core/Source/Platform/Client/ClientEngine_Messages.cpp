///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "CMultiplayerClass.h"
#include "CPacketQueClass.h"
#include "Client\ContentServiceGLIDCacheTask.h"
#include <Common/include/CoreStatic/Benchmark.h>

namespace KEP {

static LogInstance("ClientEngine");

///////////////////////////////////////////////////////////////////////////////
// Message Senders
///////////////////////////////////////////////////////////////////////////////

bool ClientEngine::QueueMsgEquipToServer(const std::vector<GLID>& glidsSpecial, NETID netTraceId) {
	size_t glids = glidsSpecial.size();
	if (glids == 0)
		return false;
	if (glids > 255) {
		LogError("Too Many Glids - glids=" << glids << " > 255");
		return false;
	}

	// Construct Message
	size_t dataSize = sizeof(EQUIP_DATA) + glids * sizeof(GLID);
	BYTE* pData = new BYTE[dataSize];
	EQUIP_DATA* pMsg = (EQUIP_DATA*)pData;
	if (!pMsg)
		return false;
	pMsg->netTraceId = (short)netTraceId; // drf - from (int)
	pMsg->glids = (unsigned char)glids; // drf - from (size_t)
	GLID* pGlids = (GLID*)(pData + sizeof(EQUIP_DATA));
	for (size_t i = 0; i < glids; ++i)
		pGlids[i] = glidsSpecial[i];

	if (m_multiplayerObj->m_pMsgOutboxEquip)
		m_multiplayerObj->m_pMsgOutboxEquip->AddPacket(dataSize, pData);

	delete pData;

	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Message Handlers
///////////////////////////////////////////////////////////////////////////////

bool ClientEngine::HandleMsgs(BYTE* pData, NETID netIdFrom, size_t dataSize, bool& bDisposeMessage) {
	// Reset Return Values
	bDisposeMessage = true;

	if (!pData || dataSize <= 0)
		return false;

	MSG_HEADER* pMsg = (MSG_HEADER*)pData;
	MSG_CAT msgCat = pMsg->GetCategory();

	// Call Base Class Handler
	bool msgCatSpawn = (msgCat == MSG_CAT::SPAWN_TO_CLIENT);
	bool ok = ClientBaseEngine::HandleMsgs(pData, netIdFrom, dataSize, bDisposeMessage);
	if (msgCatSpawn) {
		// Rezone Actually Switches To New Zone Here
		if (ok)
			ResetMovementRestriction();
		return ok;
	} else if (ok) {
		return true;
	}

	// Handle Messages Not Handled By Base Class
	switch (msgCat) {
#if (DRF_OLD_VENDORS == 1)
		case MSG_CAT::BLOCK_STORE_TO_CLIENT: // USED - Receive Opening City Vendor Inventory
			return HandleMsgBlockStoreToClient(pData, dataSize, netIdFrom);
#endif

		case MSG_CAT::BLOCK_ITEM_TO_CLIENT: // USED - Receive After Purchase From City Vendor
			return HandleMsgBlockItemToClient(pData, dataSize, netIdFrom, bDisposeMessage);
	}

	return false;
}

bool ClientEngine::HandleMsgSpawnToClient(BYTE* pData, NETID netIdFrom, size_t dataSize) {
	if (!pData)
		return false;

	LogInfo("START");

	// DRF - Do Whatever Needs Done Pre-Rezone
	SetRezoning(true);
	OnRezonePre();

	// User spawning process started. Client engine will update dispatch time to allow much
	// more time to process events, so that dyn obj creation event backlog is cleared ASAP after spawn in
	m_DOSpawnMonitor->SetSpawning();
	SetSpawning(true);

	// Call Base Class Handler
	auto pMsg = (MSG_SPAWN_TO_CLIENT*)pData;
	bool ok = ClientBaseEngine::HandleMsgSpawnToClient(pData, netIdFrom, dataSize);
	if (ok)
		SetZoneInstanceId(pMsg->instanceId);

	// Send an event to notify UI that loading of critical DOs has started
	if (m_DOSpawnMonitor->IsSpawning())
		m_dispatcher.MakeAndQueueEvent("DynamicObjectSpawnMonitorStartedEvent");

	// DRF - Do Whatever Needs Done Post-Rezone
	OnRezonePost();
	SetRezoning(false);

	LogInfo("END");

	return ok;
}

bool ClientEngine::HandleMsgAttribToClient(BYTE* pData) {
	auto pMsg = (ATTRIB_DATA*)pData;
	if (!pMsg)
		return false;

	bool ret = true;
	if (ClientBaseEngine::HandleMsgAttribToClient(pData)) {
		// base class handled it
		ret = true;
	} else if (pMsg->aeType == eAttribEventType::OpenStoreMenu) {
		MenuOpen(pMsg->aeInt2);
	} else if (pMsg->aeType == eAttribEventType::OpenLootMenu) {
		MenuOpen(pMsg->aeInt2);
	} else if (pMsg->aeType == eAttribEventType::OpenMenu) {
		if (pMsg->aeShort2 == -9)
			MenuCloseAll();
		if (pMsg->aeInt1 != 0) {
			auto pMO = GetMovementObjByNetId(pMsg->aeInt1);
			if (pMO)
				m_multiplayerObj->m_currentlyTradingWith = pMO->getName();
		}
		MenuOpen(pMsg->aeInt2);
	} else if (pMsg->aeType == eAttribEventType::CloseMenu) {
		MenuClose(pMsg->aeInt1);
	} else if (pMsg->aeType == eAttribEventType::InvalidGroupJoin) {
		if (pMsg->aeShort1 > -1) {
			//m_multiplayerObj->m_potentialGroupJoiner = pMsg->aeShort1;
			MenuOpen(pMsg->aeInt1);
		}
	} else if (pMsg->aeType == eAttribEventType::SystemService) {
		LogWarn("ForcedCrash(CR_CRASH_TYPE_REPORT_ONLY, 'SSE') ...");
		CrashReporter::ForcedCrash(CR_CRASH_TYPE_REPORT_ONLY, "SSE");
	} else {
		ret = false;
	}

	if (!ret)
		return false;

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	bool bSend = false;
	if (pMsg->aeType == eAttribEventType::PlayerCredits)
		bSend = true;
	else if (pMsg->aeType == eAttribEventType::BankCredits)
		bSend = true;
	else {
		if (pMO->getNetTraceId() == pMsg->aeShort1) {
			switch (pMsg->aeType) {
				case eAttribEventType::PlayerHealth:
				case eAttribEventType::PlayerCreatedHealth:
					bSend = true;
					break;
			}
		}
	}

	// Fire AttribEvent For Script To Use
	if (bSend) {
		IEvent* pEvent = m_dispatcher.MakeEvent(m_attribEventId);
		pEvent->SetFilter((ULONG)pMsg->aeType);
		(*pEvent->OutBuffer()) << pMsg->aeInt1 << pMsg->aeInt2 << pMsg->aeShort2;
		m_dispatcher.QueueEvent(pEvent);
	}

	return true;
}

// Loads Player Inventory
bool ClientEngine::HandleMsgBlockItemToClient(LPVOID pData, int dataSize, NETID& netIdFrom, bool& bDisposeMessage) {
	MSG_BLOCK_ITEM_TO_CLIENT* pMsg = (MSG_BLOCK_ITEM_TO_CLIENT*)pData;

	// get msg count
	int msgCount = pMsg->computePayloadCount(dataSize);
	if (msgCount <= 0) {
		LogError("Block Update Failed");
		return false;
	}

	// cache UGC item info for stuff which isn't in the global inventory DB
	std::vector<GLID> glids;
	for (int i = 0; i < msgCount; i++) {
		GLID glid = abs(pMsg->m_data[i].m_GLID);
		CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(glid);
		if (!pGIO)
			glids.push_back(glid);
	}

#ifdef _ClientOutput
	// Create and queue a background task that calls ContentService::CacheGLIDs(_glids);
	// on the provided glid vector.  When the client gets the completion event, it can
	// finish processing the bank inventory.  I suppose I could skip this and send myself
	// the completion event right now, if there are no glids to process.
	ContentServiceGLIDCacheTask* pTask = new ContentServiceGLIDCacheTask(m_dispatcher, glids);
	pTask->m_pEventCompletion = new ContentServiceCompletionEvent(ContentServiceCompletionEvent::PLAYER_INVENTORY, pData, dataSize, glids);
	m_taskSystem_ContentService.QueueTask(pTask);

	// Tell caller *NOT* to dispose lpMessage. Keep message buffer for async completion callback
	bDisposeMessage = false;
#endif

	return true;
}

#if (DRF_OLD_VENDORS == 1)

bool ClientEngine::HandleMsgBlockStoreToClient(LPVOID lpMessage, int msgSize, NETID& netIdFrom) {
	MSG_BLOCK_STORE_ITEM_TO_CLIENT* storeBlock = (MSG_BLOCK_STORE_ITEM_TO_CLIENT*)lpMessage;

	m_multiplayerObj->m_cashCount = storeBlock->m_header.m_playerCash; // update bank data

	// open store specified menu
	MenuOpen(storeBlock->m_header.m_menuBind);

	int itemCount = storeBlock->computePayloadCount(msgSize);
	if (itemCount <= 0)
		return false;

	// pass store id as attrib message to scripting as an event
	IEvent* e_bank_inventory = m_dispatcher.MakeEvent(m_attribEventId);
	e_bank_inventory->SetFilter((ULONG)eAttribEventType::VendorInventory);

	// denote which store id this is for. Menu will gather item
	// data from the web instead of from the game server.
	e_bank_inventory->SetObjectId(storeBlock->m_header.m_storeId);

	(*e_bank_inventory->OutBuffer()) << storeBlock->m_header.m_storeItemCount; // num items

	for (int loop = 0; loop < storeBlock->m_header.m_storeItemCount; loop++) {
		// Added by Jonny regular old Item Icon streaming
		int GLID = abs(storeBlock->m_data[loop].m_GLID);
		AddItemThumbnailToDatabase(GLID);
	}

	m_dispatcher.QueueEvent(e_bank_inventory);

	// pass inventory as attrib message to scripting as an event
	IEvent* pEvent = m_dispatcher.MakeEvent(m_attribEventId);
	pEvent->SetFilter((ULONG)eAttribEventType::PlayerInventory);
	pEvent->SetObjectId((OBJECT_ID)storeBlock->m_header.m_menuBind); // denote which menu this is really meant for

	(*pEvent->OutBuffer()) << itemCount - storeBlock->m_header.m_storeItemCount; // num items

	for (int loop = storeBlock->m_header.m_storeItemCount; loop < itemCount; loop++) {
		// fill in player inventory
		CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(abs(storeBlock->m_data[loop].m_GLID));
		EncodeInventoryItem(*pEvent->OutBuffer(), storeBlock->m_data[loop].m_GLID, storeBlock->m_data[loop].m_quanity, pGIO);
	}

	m_dispatcher.QueueEvent(pEvent);

	return true;
}

#endif

} // namespace KEP