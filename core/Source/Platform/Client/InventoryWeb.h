///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <list>
#include <string>

namespace KEP {

class InventoryWeb_Pass {
public:
	std::string m_passId;
	std::string m_passName;
	std::string m_passDescription;
};

/**
 * Inventory items retrieved from web.
 */
class InventoryWeb {
public:
	InventoryWeb();
	virtual ~InventoryWeb();

	std::string m_globalId;
	std::string m_quantity;
	std::string m_armed;
	std::string m_inventorySubType;
	std::string m_baseGlobalId;
	std::string m_name;
	std::string m_description;
	std::string m_accessTypeId;
	std::string m_sellingPrice;
	std::string m_useTypeStr;
	std::string m_disarmable;
	std::string m_inventoryType;
	std::string m_visualRef;
	std::string m_isDefault;
	std::string m_texturePath;
	std::string m_templatePathEncrypted;
	std::string m_thumbNailMediumPath;
	std::string m_thumbNailLargePath;
	std::string m_thumbNailAssetDetailsPath;

	std::list<InventoryWeb_Pass*> m_passes;

	RECT m_texture_coords;
	std::string m_texture_name;
};

} // namespace KEP