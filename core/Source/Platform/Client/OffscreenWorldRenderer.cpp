///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "OffscreenWorldRenderer.h"
#include "OffscreenRenderTask.h"
#include "ClientEngine.h"
#include "FixedCamera.h"
#include "KEP/Images/KEPImages_Convolve.h"
#include "LogHelper.h"

#undef min
#undef max

namespace KEP {

static LogInstance("ClientEngine");

OffscreenWorldRenderer::~OffscreenWorldRenderer() {
	if (m_pTask->m_target != m_pTask->m_defaultPoolTarget) {
		m_pTask->m_defaultPoolTarget->Release();
		m_pTask->m_defaultPoolTarget = nullptr;
	}

	if (m_pTask->m_completed || m_pTask->m_retriesLeft <= 0) {
		// Copy surface data back to original target if necessary
		// Release reference to target surface. Before queuing render task, caller should increment
		// surface reference by either calling IDirect3DTexture9::GetSurfaceLevel or explicitly
		// calling IDirect3DSurface9::AddRef.
		m_pTask->m_target->Release();
	}
}

bool OffscreenWorldRenderer::operator()() {
	assert(!m_pTask->m_completed && m_pTask->m_retriesLeft > 0);
	assert(m_pTask->m_target != nullptr);
	assert(m_pTask->m_defaultPoolTarget == nullptr);

	m_pTask->m_startTime = fTime::TimeMs();

	if (m_pTask->m_target == nullptr) {
		LogWarn("Render target is null");
		return true; // fatal, no retries
	}

	HRESULT hr;
	IDirect3DDevice9* pd3dDevice = m_pEngine->GetD3DDevice();

	// Prepare render target
	D3DSURFACE_DESC desc;
	m_pTask->m_target->GetDesc(&desc);
	switch (desc.Pool) {
		case D3DPOOL_DEFAULT:
			m_pTask->m_defaultPoolTarget = m_pTask->m_target;
			break;
		case D3DPOOL_MANAGED:
		case D3DPOOL_SYSTEMMEM:
			hr = pd3dDevice->CreateRenderTarget(desc.Width, desc.Height, desc.Format, D3DMULTISAMPLE_NONE, 0, FALSE, &m_pTask->m_defaultPoolTarget, nullptr);
			if (hr != D3D_OK || m_pTask->m_defaultPoolTarget == nullptr) {
				LogWarn("Error creating render target, hr = " << hr);
				m_pTask->m_retriesLeft--;
				return m_pTask->m_retriesLeft <= 0;
			}
			break;
		default:
			LogWarn("Unsupported target pool type: " << desc.Pool);
			return true; // fatal: no retries
	}

	// Temporary camera and view port object
	FixedCamera fixedCamera(m_pTask->m_camMatrix, m_pTask->m_projMatrix);
	CViewportObj viewportObj("OffscreenRenderer");
	viewportObj.SetCamera(&fixedCamera);
	m_pEngine->ActivateViewport(&viewportObj, desc.Width, desc.Height);

	// Override render target
	IDirect3DSurface9* savedRenderTarget = nullptr;
	hr = pd3dDevice->GetRenderTarget(0, &savedRenderTarget);
	if (hr != D3D_OK) {
		LogWarn("Error saving current render target, hr = " << hr);
		m_pTask->m_retriesLeft--;
		return m_pTask->m_retriesLeft <= 0;
	}

	hr = pd3dDevice->SetRenderTarget(0, m_pTask->m_defaultPoolTarget);
	if (hr != D3D_OK) {
		LogWarn("Error setting render target, hr = " << hr);
		m_pTask->m_retriesLeft--;
		return m_pTask->m_retriesLeft <= 0;
	}

	// BeginScene
	if (FAILED(pd3dDevice->BeginScene())) {
		m_pTask->m_retriesLeft--;
		return m_pTask->m_retriesLeft <= 0;
	}

	// Preparing
	m_pEngine->GetRenderStateManager()->ResetRenderState();
	pd3dDevice->Clear(0, NULL, D3DCLEAR_ZBUFFER | D3DCLEAR_TARGET, m_pTask->m_background, 1.0f, 0);

	// Force One-Pixel Textures ?
	bool savedForcingOnePixel = m_pEngine->IsForcingOnePixelTexture();
	bool useOnePixelTexture = (m_pTask->m_style != WorldRenderStyle::FULL_TEXTURE);
	m_pEngine->ForceOnePixelTexture(useOnePixelTexture);

	// Compiling meshes
	ScenePassOptions scenePassOptions;
	scenePassOptions.disableDistanceCulling = true;
	scenePassOptions.disableAvatarRendering = true;
	scenePassOptions.disableEnvironmentRendering = true;
	scenePassOptions.disableVisualizations = true;
	scenePassOptions.disableWorldObjectGroupFiltering = true;
	scenePassOptions.minRadiusFilter = (float)m_pTask->m_minRadiusFilter;
	m_pEngine->GenerateRenderLists(&viewportObj, scenePassOptions);

	// Render meshes
	m_pEngine->DrawRenderLists(&viewportObj, scenePassOptions);

	// EndScene
	pd3dDevice->EndScene();
	m_pTask->m_completed = true;

	// Restore
	m_pEngine->ForceOnePixelTexture(savedForcingOnePixel);

	// Copy surface
	if (m_pTask->m_target != m_pTask->m_defaultPoolTarget)
		m_pEngine->CopyD3DSurfaceFromDefaultPool(m_pTask->m_defaultPoolTarget, m_pTask->m_target);

	// Apply post-render effects if possible
	if (m_pTask->m_style != WorldRenderStyle::FULL_TEXTURE && m_pTask->m_style != WorldRenderStyle::OBJECT_COLOR) {
		if (desc.Pool == D3DPOOL_MANAGED || desc.Pool == D3DPOOL_SYSTEMMEM) {
			// Lock texture
			D3DLOCKED_RECT lockedRect;
			hr = m_pTask->m_target->LockRect(&lockedRect, nullptr, 0);
			assert(hr == D3D_OK);
			if (hr != D3D_OK) {
				LogWarn("Error locking render target, hr = " << hr);
			}

			bool grayscale = false;
			bool invert = false;
			switch (m_pTask->m_style) {
				case WorldRenderStyle::EDGE_BLACK_ON_WHITE:
					invert = true;
				// fall-thru
				case WorldRenderStyle::EDGE_WHITE_ON_BLACK:
					grayscale = true;
				// fall-thru
				case WorldRenderStyle::EDGE_TRACING:
					assert(desc.Format == D3DFMT_A8R8G8B8 || desc.Format == D3DFMT_X8R8G8B8);
					applyEffect(lockedRect, desc.Width, desc.Height, true, grayscale, invert);
					break;

				default:
					LogInfo("Post-render effect cannot be applied: don't know how to handle style: " << (int)m_pTask->m_style);
					break;
			}

			// Unlock texture
			hr = m_pTask->m_target->UnlockRect();
			assert(hr == D3D_OK);
		} else {
			LogInfo("Post-render effect cannot be applied: target not lockable");
		}
	}

	// Restore render target
	hr = pd3dDevice->SetRenderTarget(0, savedRenderTarget);
	if (hr != D3D_OK) {
		LogWarn("Error restoring render target, hr = " << hr);
	}

	if (savedRenderTarget) {
		// Must deref savedRenderTarget (a default pool resource) otherwise ResetDevice will fail
		savedRenderTarget->Release();
	}

	return true; // Completed
}

void OffscreenWorldRenderer::applyEffect(D3DLOCKED_RECT& lockedRect, size_t width, size_t height, bool traceEdges, bool grayscale, bool invert) {
	const unsigned BRIGHTNESS = 4;
	const unsigned MAP_LUMI_LB = 0x0C; // Anything darker than this will be clipped
	const unsigned MAP_LUMI_UB = 0x40; // Anything brighter than this will be clamped (GRAYSCALE only)

	//PixelSampler_Median medianSampler(4, 4);	// 4 x 4 neighborhood median
	PixelSampler_Median medianSampler(5, 5); // 5 x 5 neighborhood median

	// Edge detection kernel
	KernelFlt edgeDetect{ 2, 1, { -0.5f, 0.5f } };

	// Double width kernel
	KernelFlt dblWidth = { 2, 2, { 0.5f, 0.5f, 0.5f, 0.8f } };

#ifdef CONV_ALLOW_CUSTOM_PITCH
	auto pixels = reinterpret_cast<unsigned*>(lockedRect.pBits);
#else
	// Copy texture pixel into a buffer with standard pitch
	auto pixelsPtr = std::make_unique<unsigned[]>(width * height);
	auto pixels = pixelsPtr.get();
	{
		unsigned char* pTexBytes = reinterpret_cast<unsigned char*>(lockedRect.pBits);
		for (size_t i = 0; i < height; i++, pTexBytes += lockedRect.Pitch) {
			auto pScanline = reinterpret_cast<unsigned*>(pTexBytes);
			std::copy(pScanline, pScanline + width, pixels + i * width);
		}
	}
#endif

	// Batched convolutions with one median pass and two 1x2 matrices
	unsigned* pixelsConvol = nullptr;
#ifdef CONV_ALLOW_CUSTOM_PITCH
	if (!convolve2(pixels, lockedRect.Pitch, width, height, edgeDetect, &medianSampler, BRIGHTNESS, grayscale, MAP_LUMI_LB, MAP_LUMI_UB, (void**)&pixelsConvol)) {
#else
	if (!convolve2(pixels, width * 4, width, height, edgeDetect, &medianSampler, BRIGHTNESS, grayscale, MAP_LUMI_LB, MAP_LUMI_UB, (void**)&pixelsConvol)) {
#endif
		assert(false);
		return;
	}

	// Extra pass to enhance the width of edges
	unsigned* pixelsDoubleWidth = nullptr;
	if (!convolve(pixelsConvol, width * 4, width, height, dblWidth, nullptr, (void**)&pixelsDoubleWidth)) {
		assert(false);
		return;
	}

	// Copy back to texture, with optional pixel invert
	auto pTexBytes = reinterpret_cast<unsigned char*>(lockedRect.pBits);
	size_t pixCount = 0;
	for (size_t i = 0; i < height; i++, pTexBytes += lockedRect.Pitch) {
		auto pScanline = reinterpret_cast<unsigned*>(pTexBytes);
		if (invert) {
			for (size_t j = 0; j < width; j++, pScanline++, pixCount++) {
				*pScanline = pixelsDoubleWidth[pixCount] ^ 0x00ffffff;
			}
		} else {
			std::copy(pixelsDoubleWidth + i * width, pixelsDoubleWidth + (i + 1) * width, pScanline);
		}
	}
}

} // namespace KEP
