///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

// Game Login Result Codes
enum GL_RESULT {
	GL_UNDEFINED = -2,
	GL_OK = 0,
	// web errors
	GL_WEB_FAILED = -1,
	GL_WEB_NOT_IN_GAME = 1,
	GL_WEB_EMAIL_INVALID = 2,
	GL_WEB_NO_PERMISSION = 3,
	GL_WEB_SERVER_OFFLINE = 4,
	GL_WEB_WORLD_NOT_FOUND = 5,
	GL_WEB_SERVER_FULL = 6,
	GL_WEB_NO_PASS = 7,
	GL_WEB_VALIDATION_ERROR = 9,
	GL_WEB_CRITICAL_ERROR = 10,
	// client errors
	GL_NETWORK_ERROR = 20,
	GL_PORT_ERROR = 21,
	GL_HTTP_ERROR = 22,
	GL_WORLD_NOT_FOUND = 23,
	GL_MAX_RETRIES = 24
};

inline std::string GL_ResultStr(const GL_RESULT& glResult) {
	switch (glResult) {
		case GL_UNDEFINED:
			return "Login Failed - GL_UNDEFINED";
		case GL_OK:
			return "Login Success";
		// web errors
		case GL_WEB_FAILED:
			return "Login Failed - Web Failed";
		case GL_WEB_NOT_IN_GAME:
			return "Login Failed - User Not In Game";
		case GL_WEB_EMAIL_INVALID:
			return "Login Failed - Email Invalid";
		case GL_WEB_NO_PERMISSION:
			return "Login Failed - World Private";
		case GL_WEB_SERVER_OFFLINE:
			return "Login Failed - Server Offline";
		case GL_WEB_WORLD_NOT_FOUND:
			return "Login Failed - World Not Found";
		case GL_WEB_SERVER_FULL:
			return "Login Failed - Server Full";
		case GL_WEB_NO_PASS:
			return "Login Failed - Access Pass Required";
		case GL_WEB_VALIDATION_ERROR:
			return "Login Failed - Validation Error";
		case GL_WEB_CRITICAL_ERROR:
			return "Login Failed - Critical Error";
		// client errors
		case GL_NETWORK_ERROR:
			return "Login Failed - Network Error";
		case GL_PORT_ERROR:
			return "Login Failed - Server Port Error";
		case GL_HTTP_ERROR:
			return "Login Failed - Internet Error";
		case GL_WORLD_NOT_FOUND:
			return "Login Failed - World Not Found";
		case GL_MAX_RETRIES:
			return "Login Failed - Max Retries";
	}

	return "Login Failed";
}

// Parse tab separated list of values. Return false if parsing failed.
bool ParseGameLoginResult(
	const std::string& resultText,
	GL_RESULT& glResult,
	std::string& resultDescription,
	LONG& userId,
	std::string& username,
	std::string& serverip,
	LONG& serverport,
	std::string& serverVersion, // drf - added
	LONG& zoneindex,
	std::string& gender,
	LONG& gameId,
	std::string& gameName,
	bool& incubatorHosted,
	LONG& parentGameId,
	LONG& errorCode,
	std::string& templatePatchDir,
	std::string& appPatchDir);

// game login errorCode for 3dapps (Constants.eLOGIN_RESULTS) - result code from get_game_info SP
enum {
	GLA_FAILED = 0, // 0 not authenticated
	GLA_SUCCESS = 1, // 1 successuful authentication
	//	GLA_USER_NOT_FOUND = 2,		// 2a user not retrieved from database										// Not used by SP get_game_login
	GLA_WORLD_NOT_FOUND = 2, // 2b 3D app not found in developer database (used by gamelogin.aspx / get_game_info SP)
	//	GLA_INVALID_PASSWORD = 3,	// 3 invalid password														// Not used by SP get_game_login
	//	GLA_NO_GAME_ACCESS = 4,		// 4a user was authenticated, but not to game supplied						// Not used by SP get_game_login
	GLA_NO_WORLD_SERVERS = 4, // 4b 3D app server not running (used by gamelogin.aspx / get_game_info SP)
	//	GLA_NOT_VALIDATED = 5,		// 5 user has not validated the account										// Not used by SP get_game_login
	GLA_ACCOUNT_DELETED = 6, // 6 user account was deleted												// Probably not used
	GLA_ACCOUNT_LOCKED = 7, // 7 user account was locked												// Probably not used
	//	GLA_ALREADY_LOGGED_IN = 8,	// 8 user alreay logged in, performed by DS web service						// Not used by SP get_game_login
	GLA_GAME_FULL = 9, // 9 Max user limit hit for the game
	GLA_NOT_AUTHORIZED = 10, // 10 mature, age, access pass, etc.
	//	GLA_WRONG_SERVER = 11,		// 11 mismatched server info serverId doesn't match serverId for actualIp	// Not used by SP get_game_login
	GLA_ADMIN_ONLY = 12, // 12 the server is admin_only and you're not an admin
	//	GLA_PROMPT_ALLOW = 13,		// 13 the player must be prompted before going on to patch 3dApp			// gamelogin.aspx already translated this into GL_PROMPT_ALLOW
	GLA_GAME_BANNED = 14 // 14 the game (3DApp) has been banned										// Probably not used
};

// game login errorCode for WOK (Constants.eCANSPAWN_RESULTS) - result code from "can xxx spawn to xxx" SPs
enum {
	GLW_FAILED = -1, // Invalid parameter or etc.
	GLW_SUCCESS = 0,
	GLW_NOT_IN_GAME = 1, // User not in game
	GLW_DEST_NOT_FOUND = 2, // Destination not found
	GLW_NO_PERMISSION = 3, // Private community
	GLW_BLOCKED = 4, // Blocked or member only. NOTE: 3 and 4 are not clearly distinguished, especially for community.
	GLW_ZONE_FULL = 5, // Zone is full (not used?)
	GLW_SERVER_FULL = 6, // Server is full
	GLW_NO_PASS = 7, // Access pass or VIP required
	GLW_CRITICAL_ERROR = 10, // Bug in SPs
};
