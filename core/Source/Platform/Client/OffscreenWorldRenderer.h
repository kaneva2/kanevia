///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

class ClientEngine;
struct OffscreenRenderTask;

class OffscreenWorldRenderer {
public:
	OffscreenWorldRenderer(ClientEngine* pEngine, OffscreenRenderTask* pTask) :
			m_pEngine(pEngine), m_pTask(pTask) {}
	~OffscreenWorldRenderer();

	bool operator()();

private:
	void applyEffect(D3DLOCKED_RECT& lockedRect, size_t width, size_t height, bool traceEdges, bool grayscale, bool invert);

	ClientEngine* m_pEngine;
	OffscreenRenderTask* m_pTask;
};

} // namespace KEP
