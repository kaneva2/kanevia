///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ClientMetrics.h"

#include "common\include\glAdjust.h"

#include "LogHelper.h"
static LogInstance("Instance");

void ClientMetrics::Reset() {
	for (size_t i = 0; i < (size_t)FPS_ID::FPS_IDS; ++i)
		FpsReset((FPS_ID)i);
}

void ClientMetrics::Enable(bool enable) {
	for (size_t i = 0; i < (size_t)FPS_ID::FPS_IDS; ++i)
		FpsEnable((FPS_ID)i, enable);
	Log();
}

void ClientMetrics::Log() {
	RenderMetrics::Log();
	for (size_t i = 0; i < (size_t)FPS_ID::FPS_IDS; ++i)
		FpsLog((FPS_ID)i);
}

void ClientMetrics::RenderLoop() {
	for (size_t i = 0; i < (size_t)FPS_ID::FPS_IDS; ++i)
		FpsUpdate((FPS_ID)i);
}

bool ClientMetrics::FpsReset(FPS_ID fpsId) {
	fps[(size_t)fpsId].timer.Reset();
	fps[(size_t)fpsId].frames = 0;
	return true;
}

bool ClientMetrics::FpsEnable(FPS_ID fpsId, bool enable) {
	if (enable)
		fps[(size_t)fpsId].timer.Start();
	else
		fps[(size_t)fpsId].timer.Pause();
	fps[(size_t)fpsId].enabled = enable;
	return true;
}

bool ClientMetrics::Fps(FPS_ID fpsId, double& framesPerSec) const {
	TimeMs timeMs = fps[(size_t)fpsId].timer.ElapsedMs();
	framesPerSec = timeMs ? ((double)fps[(size_t)fpsId].frames / ((double)timeMs / MS_PER_SEC)) : 0.0;
	return true;
}

bool ClientMetrics::FpsUpdate(FPS_ID fpsId) {
	if (!fps[(size_t)fpsId].enabled)
		return false;
	++fps[(size_t)fpsId].frames;
	if (fps[(size_t)fpsId].frames == 0)
		fps[(size_t)fpsId].timer.Reset();
	return true;
}

bool ClientMetrics::FpsLog(FPS_ID fpsId) {
	double framesPerSec = 0.0;
	Fps(fpsId, framesPerSec);
	LogInfo("FPS<" << (size_t)fpsId << ">"
				   << (fps[(size_t)fpsId].enabled ? " ENABLED" : " DISABLED")
				   << " frames=" << fps[(size_t)fpsId].frames
				   << " timeMs=" << FMT_TIME << fps[(size_t)fpsId].timer.ElapsedMs()
				   << " fps=" << framesPerSec);
	return true;
}
