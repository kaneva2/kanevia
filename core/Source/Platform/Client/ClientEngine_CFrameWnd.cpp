///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include <dinput.h>
#include <d3d9.h>
#include "ClientEngine_CWinApp.h"
#include "ClientEngine_CFrameWnd.h"
#include "ClientEngine.h"
#include <stdio.h>
#include <conio.h>
#include <direct.h>
#include <stdlib.h>
#include "getdxver.h"
#include "DxDevice.h"
#include "KEPFileNames.h"
#include "CabFunctionLib.h"
#include "engine/blades/BladeStrings.h"
#include "engine/clientblades/clientbladefactory.h"
#include <SerializeHelper.h>
#include "Common\KEPUtil\Helpers.h"
#include "UnicodeMFCHelpers.h"
#include "KeyCodeMap.h"
#include "LogHelper.h"
#include "Common/KEPUtil/CrashReporter.h"
#include "common\include\NetworkCfg.h"

#define BIT_DEPTH_TAG "bit_depth"
#define BIT_DEPTH_DEFAULT 16

#define ANTI_ALIAS_TAG "anti_alias"
#define ANTI_ALIAS_MIN (int)D3DMULTISAMPLE_NONE
#define ANTI_ALIAS_MAX (int)D3DMULTISAMPLE_16_SAMPLES
#define ANTI_ALIAS_DEFAULT ANTI_ALIAS_MIN

#define SHADOW_TYPE_TAG "shadow_type"
#define SHADOW_TYPE_DEFAULT 2

#define SHADOW_DIST_TAG "shadow_dist"
#define SHADOW_DIST_DEFAULT 500

#define FIRST_PER_SHADOW_TAG "first_per_shadow"
#define FIRST_PER_SHADOW_DEFAULT 0

#define MAX_SHADOW_TAG "max_shadow"
#define MAX_SHADOW_DEFAULT 100

#define FEEDBACK_REPORT_TAG "feedback_report"
#define FEEDBACK_REPORT_DEFAULT 1

#define LOD_BIAS_MOV_OBJ_TAG "lod_bias_mov_obj"
#define LOD_BIAS_MOV_OBJ_DEFAULT 5

#define LOD_BIAS_MOV_OBJ_AUTO_TAG "lod_bias_mov_obj_auto"
#define LOD_BIAS_MOV_OBJ_AUTO_DEFAULT true

#define PARTICLE_BIAS_TAG "particle_bias"
#define PARTICLE_BIAS_DEFAULT 10

#define PARTICLE_BIAS_AUTO_TAG "particle_bias_auto"
#define PARTICLE_BIAS_AUTO_DEFAULT true

#define ANIM_BIAS_MOV_OBJ_TAG "anim_bias_mov_obj"
#define ANIM_BIAS_MOV_OBJ_DEFAULT 3

#define ANIM_BIAS_MOV_OBJ_AUTO_TAG "anim_bias_mov_obj_auto"
#define ANIM_BIAS_MOV_OBJ_AUTO_DEFAULT true

#define ANIM_BIAS_DYN_OBJ_TAG "anim_bias_dyn_obj"
#define ANIM_BIAS_DYN_OBJ_DEFAULT 3

#define ANIM_BIAS_DYN_OBJ_AUTO_TAG "anim_bias_dyn_obj_auto"
#define ANIM_BIAS_DYN_OBJ_AUTO_DEFAULT true

#define CULL_BIAS_DYN_OBJ_TAG "cull_bias_dyn_obj"
#define CULL_BIAS_DYN_OBJ_DEFAULT 3

#define CULL_BIAS_DYN_OBJ_AUTO_TAG "cull_bias_dyn_obj_auto"
#define CULL_BIAS_DYN_OBJ_AUTO_DEFAULT true

#define CULL_DIST_MOV_OBJ_TAG "cull_dist_mov_obj"
#define CULL_DIST_MOV_OBJ_DEFAULT 1800.0f

#define AVATAR_NAMES_TAG "avatar_names"
#define AVATAR_NAMES_DEFAULT 5

#define AVATAR_NAMES_AUTO_TAG "avatar_names_auto"
#define AVATAR_NAMES_AUTO_DEFAULT false

#define INVERT_Y_TAG "invert_y"
#define INVERT_Y_DEFAULT false

#define M_LOOK_SWAP_TAG "m_look_swap"
#define M_LOOK_SWAP_DEFAULT true

#define MEDIA_ENABLED_TAG "media_enabled"
#define MEDIA_ENABLED_DEFAULT true

#define CURSOR_FILE_TAG "cursor_file"

#define SHADOW_FILE_TAG "shadow_file"
#define SHADOW_FILE_DEFAULT "128CircleShadow.tga"

#define CRASH_REPORT_SILENT_TAG "crash_report_silent"
#define CRASH_REPORT_SILENT_DEFAULT TRUE

#define CRASH_REPORT_SCREENSHOT_TAG "crash_report_screenshot"
#define CRASH_REPORT_SCREENSHOT_DEFAULT CR_SCREENSHOT_NONE

#define LOGIN_EMAIL_TAG "login_email"
#define LOGIN_EMAIL_DEFAULT ""

#define LOGIN_PASSWORD_TAG "login_password"
#define LOGIN_PASSWORD_DEFAULT ""

#define SAT_ENABLED_TAG "SAT_enabled"
#define SAT_ENABLED_DEFAULT false

namespace KEP {

static LogInstance("Instance");

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

BEGIN_MESSAGE_MAP(ClientEngine_CFrameWnd, CFrameWnd)
ON_WM_CREATE()
ON_WM_DESTROY()
ON_WM_ACTIVATE()
ON_WM_SIZE()
ON_WM_MOUSEMOVE()
ON_WM_ERASEBKGND()
ON_WM_MOVE()
ON_WM_SYSCOMMAND()
ON_WM_SYSCHAR()
ON_WM_SYSKEYDOWN()
ON_WM_SYSKEYUP()
ON_WM_SETFOCUS()
ON_WM_KILLFOCUS()
ON_WM_KEYDOWN()
ON_WM_KEYUP()
ON_WM_LBUTTONDOWN()
ON_WM_LBUTTONUP()
ON_WM_RBUTTONDOWN()
ON_WM_RBUTTONUP()
ON_WM_MBUTTONDOWN()
ON_WM_MBUTTONUP()
ON_WM_XBUTTONDOWN()
ON_WM_XBUTTONUP()
ON_WM_INPUT()
ON_WM_EXITMENULOOP()
ON_WM_EXITSIZEMOVE()
END_MESSAGE_MAP()

LPDIRECT3D9 ClientEngine_CFrameWnd::g_pD3D;
LPDIRECT3DDEVICE9 ClientEngine_CFrameWnd::g_pD3dDevice;

long ClientEngine_CFrameWnd::mousex;
long ClientEngine_CFrameWnd::mousey;
UINT ClientEngine_CFrameWnd::mousestate;

#pragma comment(lib, "ddraw.lib")

void ClientEngine_CFrameWnd::Init() {
	// Detect DX Version
	if (!D3DXCheckVersion(D3D_SDK_VERSION, D3DX_SDK_VERSION)) {
		AfxMessageBox(IDS_ERROR_DIRECTXREQUIRED);
		LogError(loadStr(IDS_ERROR_DIRECTXREQUIRED));
		exit(1);
	}

	// Disable Anti-Aliasing On Intel Graphics
#if 1
	int antiAliasDefault = ANTI_ALIAS_MIN;
	GpuInfo gpuInfo;
	if (GetGpuInfo(gpuInfo)) {
		if (!STLContainsIgnoreCase(gpuInfo.m_gpuName, "Intel"))
			antiAliasDefault = ANTI_ALIAS_MAX;
	}
#else
	int antiAliasDefault = ANTI_ALIAS_DEFAULT;
#endif

	// Set Configuration Defaults
	m_curModeRect = RECT({ 0, 0, WIN_WIDTH_DEFAULT, WIN_HEIGHT_DEFAULT });
	m_curModeDepth = BIT_DEPTH_DEFAULT;
	m_antiAlias = antiAliasDefault;
	m_LodBiasMovObj = LOD_BIAS_MOV_OBJ_DEFAULT;
	m_LodBiasMovObjAuto = LOD_BIAS_MOV_OBJ_AUTO_DEFAULT;
	m_cullBiasDynObj = CULL_BIAS_DYN_OBJ_DEFAULT;
	m_cullBiasDynObjAuto = CULL_BIAS_DYN_OBJ_AUTO_DEFAULT;
	m_cullDistMovObj = CULL_DIST_MOV_OBJ_DEFAULT;
	m_particleBias = PARTICLE_BIAS_DEFAULT;
	m_particleBiasAuto = PARTICLE_BIAS_AUTO_DEFAULT;
	m_animBiasMovObj = ANIM_BIAS_MOV_OBJ_DEFAULT;
	m_animBiasMovObjAuto = ANIM_BIAS_MOV_OBJ_AUTO_DEFAULT;
	m_animBiasDynObj = ANIM_BIAS_DYN_OBJ_DEFAULT;
	m_animBiasDynObjAuto = ANIM_BIAS_DYN_OBJ_AUTO_DEFAULT;
	m_avatarNames = AVATAR_NAMES_DEFAULT;
	m_avatarNamesAuto = AVATAR_NAMES_AUTO_DEFAULT;
	m_invertY = INVERT_Y_DEFAULT;
	m_mLookSwap = M_LOOK_SWAP_DEFAULT;
	m_mediaEnabled = MEDIA_ENABLED_DEFAULT;

	m_shadows = false;
	m_globalShadowType = SHADOW_TYPE_DEFAULT;
	m_globalShadowDist = SHADOW_DIST_DEFAULT;
	m_showFirstPersonShadow = FIRST_PER_SHADOW_DEFAULT;
	m_maxShadowCount = MAX_SHADOW_DEFAULT;
	m_shadowFileName = SHADOW_FILE_DEFAULT;

	m_crashReportSilent = CRASH_REPORT_SILENT_DEFAULT;
	m_crashReportScreenshot = CRASH_REPORT_SCREENSHOT_DEFAULT;
	m_loginEmail = LOGIN_EMAIL_DEFAULT;
	m_loginPassword = LOGIN_PASSWORD_DEFAULT;
	m_feedBackReportPrompted = FEEDBACK_REPORT_DEFAULT;

	m_SATEnabled = SAT_ENABLED_DEFAULT;

	m_bIsRawInputEnabled = false;

	// Set Default Cursor File Names
	for (int i = 0; i < CURSOR_TYPES; ++i)
		m_cursorFileName[i] = CursorFile((CURSOR_TYPE)i);

	m_supportedStencilBuffer = FALSE;
	m_shadowSquareVB = NULL;
	m_windowStateGL = 0;

	m_shaderSupport = FALSE;

	m_lastModeHeight = 0;
	m_lastModeWidth = 0;
	m_lastModeDepth = 0;

	m_d3dError = D3D_ERROR_NULL;

	m_pCABD3DLIB = new CABD3DFunctionLib;

	ReadStartCfgFile();

	g_pD3D = 0;
	g_pD3dDevice = nullptr;
	m_displayModes = NULL;

	ClientEngine_CWinApp* pApp = (ClientEngine_CWinApp*)AfxGetApp();
	pApp->m_pCE_CFrameWnd = this;
}

// should be at <pathApp>\StartCfg.xml
static std::string PathStartCfg() {
	static std::string pathStartCfg;
	if (pathStartCfg.empty())
		pathStartCfg = PathFind(PathApp(), "StartCfg.xml");
	return pathStartCfg;
}

bool ClientEngine_CFrameWnd::ReadStartCfgFile() {
	// If Not Exists Create Default Config File
	std::string pathStartCfg = PathStartCfg();
	LogInfo("pathStartCfg='" << LogPath(pathStartCfg) << "'");
	if (!PathExists(pathStartCfg))
		WriteStartCfgFile();

	// Read Config File
	try {
		KEPConfig cfg;
		cfg.Load(pathStartCfg, "startconfig");

		m_curModeDepth = cfg.Get(BIT_DEPTH_TAG, BIT_DEPTH_DEFAULT);
		m_antiAlias = cfg.Get(ANTI_ALIAS_TAG, ANTI_ALIAS_DEFAULT);
		m_LodBiasMovObj = cfg.Get(LOD_BIAS_MOV_OBJ_TAG, LOD_BIAS_MOV_OBJ_DEFAULT);
		m_LodBiasMovObjAuto = cfg.Get(LOD_BIAS_MOV_OBJ_AUTO_TAG, LOD_BIAS_MOV_OBJ_AUTO_DEFAULT);
		m_cullBiasDynObj = cfg.Get(CULL_BIAS_DYN_OBJ_TAG, CULL_BIAS_DYN_OBJ_DEFAULT);
		m_cullBiasDynObjAuto = cfg.Get(CULL_BIAS_DYN_OBJ_AUTO_TAG, CULL_BIAS_DYN_OBJ_AUTO_DEFAULT);
		m_cullDistMovObj = float(cfg.Get(CULL_DIST_MOV_OBJ_TAG, double(CULL_DIST_MOV_OBJ_DEFAULT)));
		m_particleBias = cfg.Get(PARTICLE_BIAS_TAG, PARTICLE_BIAS_DEFAULT);
		m_particleBiasAuto = cfg.Get(PARTICLE_BIAS_AUTO_TAG, PARTICLE_BIAS_AUTO_DEFAULT);
		m_animBiasMovObj = cfg.Get(ANIM_BIAS_MOV_OBJ_TAG, ANIM_BIAS_MOV_OBJ_DEFAULT);
		m_animBiasMovObjAuto = cfg.Get(ANIM_BIAS_MOV_OBJ_AUTO_TAG, ANIM_BIAS_MOV_OBJ_AUTO_DEFAULT);
		m_animBiasDynObj = cfg.Get(ANIM_BIAS_DYN_OBJ_TAG, ANIM_BIAS_DYN_OBJ_DEFAULT);
		m_animBiasDynObjAuto = cfg.Get(ANIM_BIAS_DYN_OBJ_AUTO_TAG, ANIM_BIAS_DYN_OBJ_AUTO_DEFAULT);
		m_avatarNames = cfg.Get(AVATAR_NAMES_TAG, AVATAR_NAMES_DEFAULT);
		m_avatarNamesAuto = cfg.Get(AVATAR_NAMES_AUTO_TAG, AVATAR_NAMES_AUTO_DEFAULT);
		m_invertY = cfg.Get(INVERT_Y_TAG, INVERT_Y_DEFAULT);
		m_mLookSwap = cfg.Get(M_LOOK_SWAP_TAG, M_LOOK_SWAP_DEFAULT);

		m_cursorFileName[CURSOR_TYPE_DEFAULT] = cfg.Get(CURSOR_FILE_TAG, CursorFile(CURSOR_TYPE_DEFAULT)).c_str();
		m_shadowFileName = cfg.Get(SHADOW_FILE_TAG, SHADOW_FILE_DEFAULT).c_str();

		m_globalShadowType = cfg.Get(SHADOW_TYPE_TAG, SHADOW_TYPE_DEFAULT);
		m_globalShadowDist = cfg.Get(SHADOW_DIST_TAG, SHADOW_DIST_DEFAULT);
		m_showFirstPersonShadow = cfg.Get(FIRST_PER_SHADOW_TAG, FIRST_PER_SHADOW_DEFAULT);
		m_maxShadowCount = cfg.Get(MAX_SHADOW_TAG, MAX_SHADOW_DEFAULT);

		m_feedBackReportPrompted = cfg.Get(FEEDBACK_REPORT_TAG, FEEDBACK_REPORT_DEFAULT);

		// DRF - Added
		m_crashReportSilent = cfg.Get(CRASH_REPORT_SILENT_TAG, CRASH_REPORT_SILENT_DEFAULT);
		if (m_crashReportSilent != CRASH_REPORT_SILENT_DEFAULT)
			LogWarn("crashReportSilent=" << LogBool(m_crashReportSilent));
		m_crashReportScreenshot = (CR_SCREENSHOT)cfg.Get(CRASH_REPORT_SCREENSHOT_TAG, (int)CRASH_REPORT_SCREENSHOT_DEFAULT);
		if (m_crashReportScreenshot != CRASH_REPORT_SCREENSHOT_DEFAULT)
			LogWarn("crashReportScreenshot=" << (int)m_crashReportScreenshot);
		m_loginEmail = cfg.Get(LOGIN_EMAIL_TAG, LOGIN_EMAIL_DEFAULT).c_str();
		if (m_loginEmail != LOGIN_EMAIL_DEFAULT)
			LogWarn("loginEmail='" << m_loginEmail << "'");
		m_loginPassword = cfg.Get(LOGIN_PASSWORD_TAG, LOGIN_PASSWORD_DEFAULT).c_str();
		if (m_loginPassword != LOGIN_PASSWORD_DEFAULT)
			LogWarn("loginPassword='" << m_loginPassword << "'");
		m_mediaEnabled = cfg.Get(MEDIA_ENABLED_TAG, MEDIA_ENABLED_DEFAULT);
		if (m_mediaEnabled != MEDIA_ENABLED_DEFAULT)
			LogWarn("mediaEnabled=" << LogBool(m_mediaEnabled));

		//Ankit ----- check config file to see if to start in SAT mode
		m_SATEnabled = cfg.Get(SAT_ENABLED_TAG, SAT_ENABLED_DEFAULT);
		if (m_SATEnabled != SAT_ENABLED_DEFAULT)
			LogWarn("SATEnabled=" << LogBool(m_SATEnabled));

		return AppReadStartCfgFile(cfg);

	} catch (KEPException* e) {
		LogError("Caught Exception - startcfg.xml " << e->m_msg);
		e->Delete();
	}

	return false;
}

bool ClientEngine_CFrameWnd::WriteStartCfgFile() {
	try {
		// Read From 'StartCfg.xml'
		std::string pathStartCfg = PathStartCfg();
		if (!PathExists(pathStartCfg)) {
			FileHelper fh;
			fh.Open(pathStartCfg, GENERIC_READ | GENERIC_WRITE, OPEN_ALWAYS);
			const std::string startCfg = "<startconfig> </startconfig>";
			fh.Tx(startCfg.c_str(), startCfg.size());
		}

		KEPConfig cfg;
		cfg.Load(pathStartCfg, "startconfig");

		cfg.SetValue(BIT_DEPTH_TAG, m_curModeDepth);
		cfg.SetValue(ANTI_ALIAS_TAG, m_antiAlias);
		cfg.SetValue(LOD_BIAS_MOV_OBJ_TAG, m_LodBiasMovObj);
		cfg.SetValue(LOD_BIAS_MOV_OBJ_AUTO_TAG, m_LodBiasMovObjAuto);
		cfg.SetValue(CULL_BIAS_DYN_OBJ_TAG, m_cullBiasDynObj);
		cfg.SetValue(CULL_BIAS_DYN_OBJ_AUTO_TAG, m_cullBiasDynObjAuto);
		cfg.SetValue(CULL_DIST_MOV_OBJ_TAG, m_cullDistMovObj);
		cfg.SetValue(PARTICLE_BIAS_TAG, m_particleBias);
		cfg.SetValue(PARTICLE_BIAS_AUTO_TAG, m_particleBiasAuto);
		cfg.SetValue(ANIM_BIAS_MOV_OBJ_TAG, m_animBiasMovObj);
		cfg.SetValue(ANIM_BIAS_MOV_OBJ_AUTO_TAG, m_animBiasMovObjAuto);
		cfg.SetValue(ANIM_BIAS_DYN_OBJ_TAG, m_animBiasDynObj);
		cfg.SetValue(ANIM_BIAS_DYN_OBJ_AUTO_TAG, m_animBiasDynObjAuto);
		cfg.SetValue(AVATAR_NAMES_TAG, m_avatarNames);
		cfg.SetValue(AVATAR_NAMES_AUTO_TAG, m_avatarNamesAuto);
		cfg.SetValue(INVERT_Y_TAG, m_invertY);
		cfg.SetValue(M_LOOK_SWAP_TAG, m_mLookSwap);

		cfg.SetValue(CURSOR_FILE_TAG, m_cursorFileName[CURSOR_TYPE_DEFAULT]);

		cfg.SetValue(SHADOW_FILE_TAG, m_shadowFileName);
		cfg.SetValue(SHADOW_TYPE_TAG, m_globalShadowType);
		cfg.SetValue(SHADOW_DIST_TAG, (int)m_globalShadowDist);
		cfg.SetValue(FIRST_PER_SHADOW_TAG, m_showFirstPersonShadow);
		cfg.SetValue(MAX_SHADOW_TAG, m_maxShadowCount);

		cfg.SetValue(CRASH_REPORT_SILENT_TAG, m_crashReportSilent);
		cfg.SetValue(CRASH_REPORT_SCREENSHOT_TAG, (int)m_crashReportScreenshot);
		cfg.SetValue(LOGIN_EMAIL_TAG, m_loginEmail);
		cfg.SetValue(LOGIN_PASSWORD_TAG, m_loginPassword);
		cfg.SetValue(MEDIA_ENABLED_TAG, m_mediaEnabled);
		cfg.SetValue(FEEDBACK_REPORT_TAG, m_feedBackReportPrompted);
		cfg.SetValue(SAT_ENABLED_TAG, m_SATEnabled);

		bool ok = AppWriteStartCfgFile(cfg);
		cfg.SaveAs(pathStartCfg);
		return ok;
	} catch (KEPException* e) {
		LogWarn("Caught Exception - " << e->m_msg);
		e->Delete();
	}

	return false;
}

BOOL ClientEngine_CFrameWnd::Create(const CStringA& sTitle, int icon, int menu, RECT rect) {
	instanceHandle = AfxGetInstanceHandle();

	CStringW sClassName = AfxRegisterWndClass(
		CS_HREDRAW | CS_VREDRAW,
		LoadCursor(0, IDC_ARROW),
		0,
		LoadIcon(instanceHandle, MAKEINTRESOURCE(icon)));

	m_pCABD3DLIB->currentVideoMode = 3;

	// Set Our Current Window Rectangle
	m_curModeRect = rect;

	// Create Client Window
	DWORD style = WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN; // WS_CLIPCHILDREN added for child flash windows
	BOOL ok = CFrameWnd::Create(sClassName, K_CLIENT_WINDOW_NAME, style, rect, NULL, 0, WS_EX_APPWINDOW);

	// for some reason, not getting icons.  this fixes it, but why doesn't MFC set it from AfxRegisterWndClass above
	SetIcon(LoadIcon(instanceHandle, MAKEINTRESOURCE(icon)), false);
	SetIcon(LoadIcon(instanceHandle, MAKEINTRESOURCE(icon)), true);

	return ok;
}

void ClientEngine_CFrameWnd::OnSetFocus(CWnd* pOldWnd) {
	CFrameWnd::OnSetFocus(pOldWnd);
	HandleFocusChange(true);
}

void ClientEngine_CFrameWnd::OnKillFocus(CWnd* pNewWnd) {
	CFrameWnd::OnKillFocus(pNewWnd);
	HandleFocusChange(false);
}

void ClientEngine_CFrameWnd::OnExitMenuLoop(BOOL bIsTrackPopupMenu) {
	CFrameWnd::OnExitMenuLoop(bIsTrackPopupMenu);
	HandleReturnFromNonclientAreaAction();
}

void ClientEngine_CFrameWnd::OnExitSizeMove() {
	CFrameWnd::OnExitSizeMove();
	HandleReturnFromNonclientAreaAction();
}

static WPARAM ConvertVirtualKey(WPARAM wParam, LPARAM lParam) {
	bool bExtendedKey = (lParam & (1 << 24)) != 0;
	switch (wParam) {
		case VK_SHIFT: {
			uint8_t scanCode = lParam >> 16;
			return MapVirtualKey(scanCode, MAPVK_VSC_TO_VK_EX);
		}

		case VK_RETURN:
			// There does not appear to be a special key code for enter on the number pad
			// so we'll invent our own.
			return bExtendedKey ? KEP_VK_NUMPADRETURN : VK_RETURN;

		case VK_CONTROL:
			return bExtendedKey ? VK_LCONTROL : VK_RCONTROL;

		case VK_MENU:
			return bExtendedKey ? VK_LMENU : VK_RMENU;
	}

	return wParam;
}

void ClientEngine_CFrameWnd::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) {
	const MSG* pMsg = GetCurrentMessage();
	int iVirtualKey = ConvertVirtualKey(pMsg->wParam, pMsg->lParam);
	HandleWindowsKeyEvent(iVirtualKey, true);
	CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}

void ClientEngine_CFrameWnd::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) {
	const MSG* pMsg = GetCurrentMessage();
	int iVirtualKey = ConvertVirtualKey(pMsg->wParam, pMsg->lParam);
	HandleWindowsKeyEvent(iVirtualKey, false);
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

void ClientEngine_CFrameWnd::OnLButtonDown(UINT nFlags, CPoint point) {
	if (!m_bIsRawInputEnabled)
		HandleWindowsMouseButtonEvent(VK_LBUTTON, true);
	CFrameWnd::OnLButtonDown(nFlags, point);
}

void ClientEngine_CFrameWnd::OnLButtonUp(UINT nFlags, CPoint point) {
	if (!m_bIsRawInputEnabled)
		HandleWindowsMouseButtonEvent(VK_LBUTTON, false);
	CFrameWnd::OnLButtonUp(nFlags, point);
}

void ClientEngine_CFrameWnd::OnRButtonDown(UINT nFlags, CPoint point) {
	if (!m_bIsRawInputEnabled)
		HandleWindowsMouseButtonEvent(VK_RBUTTON, true);
	CFrameWnd::OnRButtonDown(nFlags, point);
}

void ClientEngine_CFrameWnd::OnRButtonUp(UINT nFlags, CPoint point) {
	if (!m_bIsRawInputEnabled)
		HandleWindowsMouseButtonEvent(VK_RBUTTON, false);
	CFrameWnd::OnRButtonUp(nFlags, point);
}

void ClientEngine_CFrameWnd::OnMButtonDown(UINT nFlags, CPoint point) {
	if (!m_bIsRawInputEnabled)
		HandleWindowsMouseButtonEvent(VK_MBUTTON, true);
	CFrameWnd::OnMButtonDown(nFlags, point);
}

void ClientEngine_CFrameWnd::OnMButtonUp(UINT nFlags, CPoint point) {
	if (!m_bIsRawInputEnabled)
		HandleWindowsMouseButtonEvent(VK_MBUTTON, false);
	CFrameWnd::OnMButtonUp(nFlags, point);
}

void ClientEngine_CFrameWnd::OnXButtonDown(UINT nFlags, UINT nButton, CPoint point) {
	if (!m_bIsRawInputEnabled)
		HandleWindowsMouseButtonEvent(nButton == XBUTTON1 ? VK_XBUTTON1 : VK_XBUTTON2, true);
	CFrameWnd::OnXButtonDown(nFlags, nButton, point);
}

void ClientEngine_CFrameWnd::OnXButtonUp(UINT nFlags, UINT nButton, CPoint point) {
	if (!m_bIsRawInputEnabled)
		HandleWindowsMouseButtonEvent(nButton == XBUTTON1 ? VK_XBUTTON1 : VK_XBUTTON2, false);
	CFrameWnd::OnXButtonUp(nFlags, nButton, point);
}

// This handles cursor movements (as opposed to actual mouse movements)
void ClientEngine_CFrameWnd::OnMouseMove(UINT state, CPoint point) {
	mousestate = state;
	mousex = point.x;
	mousey = point.y;

	// Handle mouse button motion while a button is pressed
	if (state & MK_LBUTTON) {
		//HandleWindowsMouseButtonEvent(VK_LBUTTON, true, point);
	} else if (state & MK_MBUTTON) {
		//HandleWindowsMouseButtonEvent(VK_MBUTTON, true, point);
	} else if (state & MK_RBUTTON) {
		//HandleWindowsMouseButtonEvent(VK_RBUTTON, true, point);
	}
}

void ClientEngine_CFrameWnd::OnSysChar(UINT nChar, UINT nRepCnt, UINT nFlags) {
	// Not calling Default() for WM_SYSKEYDOWN message.
	return;
}

void ClientEngine_CFrameWnd::OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) {
	if (nFlags & (1 << 14))
		return; // Key repeat message

	const MSG* pMsg = GetCurrentMessage();
	int iVirtualKey = ConvertVirtualKey(pMsg->wParam, pMsg->lParam);
	HandleWindowsKeyEvent(iVirtualKey, true);

	// Not calling Default() for WM_SYSKEYDOWN message.
	return;
}

void ClientEngine_CFrameWnd::OnSysKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) {
	const MSG* pMsg = GetCurrentMessage();
	int iVirtualKey = ConvertVirtualKey(pMsg->wParam, pMsg->lParam);
	HandleWindowsKeyEvent(iVirtualKey, false);

	// Not calling Default() for WM_SYSKEYUP message.
	return;
}

void ClientEngine_CFrameWnd::OnSysCommand(UINT nID, LPARAM lParam) {
	// any WM_SYSCOMMAND messages not handled by the application must be passed to DefWindowProc
	Default();
}

int ClientEngine_CFrameWnd::OnCreate(LPCREATESTRUCT) {
	//Use a dummy timer to pump WM_TIMER to message queue
	//This will hopefully address the idle-force-repaint (aka blinking/flashing/blackout) issue caused by DWM in Windows Vista/7
	SetTimer(9999, 500, NULL);

	// Create DirectX Device (enable anti-aliasing)
	if (!CreateNewD3dDevice(true)) {
		MessageBox(
			L"Direct3D Failure - Please re-install DirectX 9.0c or above and update your video drivers and try again.",
			L"Direct3D Failure",
			MB_ICONEXCLAMATION | MB_OK);
		::ExitProcess(1);
	}

	return 0;
}

bool ClientEngine_CFrameWnd::CreateNewD3dDevice(bool antiAlias) {
	// Create DirectX Device
	CreateD3dDevice(FALSE, m_pCABD3DLIB->currentVideoMode, antiAlias);

#ifdef _ClientOutput
	// On Failure Try Again At Min Window Size
	if (!g_pD3dDevice) {
		m_curModeRect = RECT({ 0, 0, WIN_WIDTH_MIN, WIN_HEIGHT_MIN });
		m_curModeDepth = 16;
		CreateD3dDevice(FALSE, m_pCABD3DLIB->currentVideoMode, antiAlias);
	}
#endif

	return (g_pD3dDevice != nullptr);
}

static std::string D3dResultStr(HRESULT hr) {
	switch (hr) {
		case D3DERR_WRONGTEXTUREFORMAT:
			return "D3DERR_WRONGTEXTUREFORMAT";
		case D3DERR_UNSUPPORTEDCOLOROPERATION:
			return "D3DERR_UNSUPPORTEDCOLOROPERATION";
		case D3DERR_UNSUPPORTEDCOLORARG:
			return "D3DERR_UNSUPPORTEDCOLORARG";
		case D3DERR_UNSUPPORTEDALPHAOPERATION:
			return "D3DERR_UNSUPPORTEDALPHAOPERATION";
		case D3DERR_UNSUPPORTEDALPHAARG:
			return "D3DERR_UNSUPPORTEDALPHAARG";
		case D3DERR_TOOMANYOPERATIONS:
			return "D3DERR_TOOMANYOPERATIONS";
		case D3DERR_CONFLICTINGTEXTUREFILTER:
			return "D3DERR_CONFLICTINGTEXTUREFILTER";
		case D3DERR_UNSUPPORTEDFACTORVALUE:
			return "D3DERR_UNSUPPORTEDFACTORVALUE";
		case D3DERR_CONFLICTINGRENDERSTATE:
			return "D3DERR_CONFLICTINGRENDERSTATE";
		case D3DERR_UNSUPPORTEDTEXTUREFILTER:
			return "D3DERR_UNSUPPORTEDTEXTUREFILTER";
		case D3DERR_CONFLICTINGTEXTUREPALETTE:
			return "D3DERR_CONFLICTINGTEXTUREPALETTE";
		case D3DERR_DRIVERINTERNALERROR:
			return "D3DERR_DRIVERINTERNALERROR";
		case D3DERR_NOTFOUND:
			return "D3DERR_NOTFOUND";
		case D3DERR_MOREDATA:
			return "D3DERR_MOREDATA";
		case D3DERR_DEVICELOST:
			return "D3DERR_DEVICELOST";
		case D3DERR_DEVICENOTRESET:
			return "D3DERR_DEVICENOTRESET";
		case D3DERR_NOTAVAILABLE:
			return "D3DERR_NOTAVAILABLE";
		case D3DERR_OUTOFVIDEOMEMORY:
			return "D3DERR_OUTOFVIDEOMEMORY";
		case D3DERR_INVALIDDEVICE:
			return "D3DERR_INVALIDDEVICE";
		case D3DERR_INVALIDCALL:
			return "D3DERR_INVALIDCALL";
		case D3DERR_DRIVERINVALIDCALL:
			return "D3DERR_DRIVERINVALIDCALL";
		case D3DERR_WASSTILLDRAWING:
			return "D3DERR_WASSTILLDRAWING";
	}
	std::string resultStr;
	StrBuild(resultStr, "Code(" << HRESULT_CODE(hr) << ")");
	return resultStr;
}

#define dwBehaviorFlagsMixed (D3DCREATE_MIXED_VERTEXPROCESSING | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE)
#define dwBehaviorFlagsSW (D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE)

D3D_ERROR ClientEngine_CFrameWnd::CreateD3dDevice(BOOL fullscreen, int mode, bool antiAlias) {
	// Create DirectX Device
	if (!g_pD3D) {
		g_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
		if (!g_pD3D) {
			AfxMessageBox(L"Error: No Compatible DirectX Device Available.");
			LogError("Direct3DCreate9() FAILED - D3D_ERROR_NO_COMPATIBLE_DEVICE");
			m_d3dError = D3D_ERROR_NO_COMPATIBLE_DEVICE;
			return m_d3dError;
		}
		LogInfo("Direct3DCreate9() OK");
	}

	// Get DirectX Display Mode
	if (FAILED(g_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm))) {
		AfxMessageBox(L"Error: Failed Getting DirectX Display Mode. Update DirectX And Try Again.");
		LogError("GetAdapterDisplayMode() FAILED - D3D_ERROR_DISPLAY_MODE");
		m_d3dError = D3D_ERROR_FAILED_DISPLAY_MODE;
		return m_d3dError;
	}
	LogInfo("GetAdapterDisplayMode() OK");

	D3DFORMAT displayFormat = D3DFMT_R5G6B5;
	if (m_curModeDepth > 16)
		displayFormat = D3DFMT_X8R8G8B8;

	// Log Possible R5G6B5 Display Modes
	int totalDisplayModes = g_pD3D->GetAdapterModeCount(D3DADAPTER_DEFAULT, D3DFMT_R5G6B5);
	int LoggedDisplayModes = 0;
	for (int i = 0; i < totalDisplayModes; i++) {
		// find good display modes
		D3DDISPLAYMODE displayMode;
		g_pD3D->EnumAdapterModes(D3DADAPTER_DEFAULT, D3DFMT_R5G6B5, i, &displayMode);

		if (displayMode.Format == D3DFMT_R5G6B5) {
			if ((m_lastModeHeight != displayMode.Height || m_lastModeWidth != displayMode.Width)) {
				m_pCABD3DLIB->videoMDWidth[LoggedDisplayModes] = displayMode.Width;
				m_pCABD3DLIB->videoMDHeight[LoggedDisplayModes] = displayMode.Height;
				m_pCABD3DLIB->videoMDBpp[LoggedDisplayModes] = 16;
				m_lastModeHeight = displayMode.Height;
				m_lastModeWidth = displayMode.Width;
				m_lastModeDepth = 16;
				LoggedDisplayModes++;
			}
		}

		m_pCABD3DLIB->totalDisplayModes = LoggedDisplayModes;
		if (LoggedDisplayModes > 25) {
			break;
		}
	} // end find good display modes
	LogInfo("GetAdapterModeCount(R5G6B5) totalDisplayModes=" << totalDisplayModes << " logDisplayModes=" << LoggedDisplayModes);

	// Log Possible R8G8B8 Display Modes
	m_lastModeHeight = -1; //reset so the next check will be ok in 32 bit
	totalDisplayModes = g_pD3D->GetAdapterModeCount(D3DADAPTER_DEFAULT, D3DFMT_X8R8G8B8);
	for (int i = 0; i < totalDisplayModes; i++) {
		// find good display modes
		D3DDISPLAYMODE displayMode;
		g_pD3D->EnumAdapterModes(D3DADAPTER_DEFAULT, D3DFMT_X8R8G8B8, i, &displayMode);

		if (displayMode.Format == D3DFMT_X8R8G8B8) {
			if ((m_lastModeHeight != displayMode.Height || m_lastModeWidth != displayMode.Width)) {
				m_pCABD3DLIB->videoMDWidth[LoggedDisplayModes] = displayMode.Width;
				m_pCABD3DLIB->videoMDHeight[LoggedDisplayModes] = displayMode.Height;
				m_pCABD3DLIB->videoMDBpp[LoggedDisplayModes] = 32;
				m_lastModeHeight = displayMode.Height;
				m_lastModeWidth = displayMode.Width;
				m_lastModeDepth = 32;
				LoggedDisplayModes++;
			}
		}

		m_pCABD3DLIB->totalDisplayModes = LoggedDisplayModes;
		if (LoggedDisplayModes > 49) {
			break;
		}
	}

	LogInfo("GetAdapterModeCount(R8G8B8) totalDisplayModes=" << totalDisplayModes << " logDisplayModes=" << LoggedDisplayModes);

	int width = RectWidth(m_curModeRect);
	int height = RectHeight(m_curModeRect);
	m_pCABD3DLIB->m_curModeRect = RECT({ 0, 0, width, height });
	GuiRect = RECT({ 0, 0, width, height });

	m_hWndSafe = GetSafeHwnd(); // copy window handle from afxapp to ClientEngine_CFrameWnd

#if DEPRECATED
	if (g_pD3dDevice) {
		d3dpp.Windowed = TRUE;
		d3dpp.BackBufferWidth = width;
		d3dpp.BackBufferHeight = height;

		g_pD3dDevice->Reset(&d3dpp);

		// Disable Mouse In FullScreen
		SetCursorEnabled(!fullscreen);

		if (m_shadowSquareVB) {
			m_shadowSquareVB->Release();
			m_shadowSquareVB = 0;
		}

		// create shadow plane - a big square for rendering the stencilbuffer contents
		if (g_pD3dDevice->CreateVertexBuffer(
				4 * sizeof(SHADOWVERTEX),
				D3DUSAGE_WRITEONLY,
				D3DFVF_XYZRHW | D3DFVF_DIFFUSE,
				D3DPOOL_MANAGED,
				&m_shadowSquareVB,
				NULL) == 0) {
			SHADOWVERTEX* v;
			float sx = (float)d3dpp.BackBufferWidth;
			float sy = (float)d3dpp.BackBufferHeight;
			m_shadowSquareVB->Lock(0, 0, (void**)&v, 0);
			v[0].p = D3DXVECTOR4(0, sy, 0.0f, 1.0f);
			v[1].p = D3DXVECTOR4(0, 0, 0.0f, 1.0f);
			v[2].p = D3DXVECTOR4(sx, sy, 0.0f, 1.0f);
			v[3].p = D3DXVECTOR4(sx, 0, 0.0f, 1.0f);
			v[0].color = 0x7f000000;
			v[1].color = 0x7f000000;
			v[2].color = 0x7f000000;
			v[3].color = 0x7f000000;
			m_shadowSquareVB->Unlock();
			LogInfo("CreateVertexBuffer() OK");
		} else {
			LogWarn("CreateVertexBuffer() FAILED");
		}

		LogInfo("D3D_SUCCESS 1");
		m_d3dError = D3D_SUCCESS;
		return m_d3dError;
	}
#else
	static LPDIRECT3DDEVICE9 pD3dDeviceBackup = nullptr;
	if (g_pD3dDevice)
		pD3dDeviceBackup = g_pD3dDevice;
	g_pD3dDevice = nullptr;
#endif

	// create device - querry for best depth buffer
	ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16; // default format;

	int formatCount = 6;
	D3DFORMAT depthStencilFmts[50];
	BOOL stencilSupported[50];
	CStringA depthStencilFmtsNames[50];

	depthStencilFmts[0] = D3DFMT_D24X4S4;
	depthStencilFmts[1] = D3DFMT_D24S8;
	depthStencilFmts[2] = D3DFMT_D24X8;
	depthStencilFmts[3] = D3DFMT_D15S1;
	depthStencilFmts[4] = D3DFMT_D32;
	depthStencilFmts[5] = D3DFMT_D16;

	depthStencilFmtsNames[0] = "D3DFMT_D24X4S4";
	depthStencilFmtsNames[1] = "D3DFMT_D24S8";
	depthStencilFmtsNames[2] = "D3DFMT_D24X8";
	depthStencilFmtsNames[3] = "D3DFMT_D15S1";
	depthStencilFmtsNames[4] = "D3DFMT_D32";
	depthStencilFmtsNames[5] = "D3DFMT_D16";

	stencilSupported[0] = TRUE;
	stencilSupported[1] = TRUE;
	stencilSupported[2] = TRUE;
	stencilSupported[3] = TRUE;
	stencilSupported[4] = FALSE;
	stencilSupported[5] = FALSE;

	D3DFORMAT defaultDSFormat = d3dpp.AutoDepthStencilFormat;
	D3DFORMAT bestDSFormat = defaultDSFormat;

	// Get The Best Format
	int dbLoop;
	for (dbLoop = 0; dbLoop < formatCount; dbLoop++) {
		if (g_pD3D->CheckDeviceFormat(
				D3DADAPTER_DEFAULT,
				D3DDEVTYPE_HAL,
				displayFormat, // AdapterFormat,
				D3DUSAGE_DEPTHSTENCIL,
				D3DRTYPE_SURFACE,
				depthStencilFmts[dbLoop]) == 0) {
			bestDSFormat = depthStencilFmts[dbLoop]; // d3ddm.Format;
			m_supportedStencilBuffer = stencilSupported[dbLoop];

			break;
		}
	}

	// Anti-Aliasing Disabled ?
	if (!antiAlias)
		m_antiAlias = 0;

	// Anti-Aliasing Supported ?
	DWORD aaQuality = 0;
	D3DMULTISAMPLE_TYPE aaType = D3DMULTISAMPLE_NONE;
	if (m_antiAlias) {
		for (int i = ANTI_ALIAS_MIN; i <= m_antiAlias; ++i) {
			DWORD quality = 0;
			auto antiAliasType = (D3DMULTISAMPLE_TYPE)i;
			bool ok = SUCCEEDED(g_pD3D->CheckDeviceMultiSampleType(
				D3DADAPTER_DEFAULT,
				D3DDEVTYPE_HAL,
				d3ddm.Format,
				TRUE, //FALSE,
				antiAliasType,
				&quality));
			if (ok) {
				aaType = antiAliasType;
				aaQuality = quality ? quality - 1 : 0;
			}
		}
	}
	LogInfo("ANTI_ALIASING - type=" << (int)aaType << " quality=" << aaQuality);

	// Set Presentation Parameters
	d3dpp.Windowed = TRUE;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.MultiSampleType = aaType;
	d3dpp.MultiSampleQuality = aaQuality;
	d3dpp.BackBufferCount = 1;
	d3dpp.BackBufferWidth = width;
	d3dpp.BackBufferHeight = height;
	d3dpp.BackBufferFormat = d3ddm.Format;
	d3dpp.hDeviceWindow = m_hWnd;

	HRESULT hresult = D3DERR_NOTAVAILABLE;
	UINT d3dPresentInterval = D3DPRESENT_INTERVAL_DEFAULT;

	if (m_devMode_CFrameWnd)
		d3dPresentInterval = D3DPRESENT_INTERVAL_IMMEDIATE; // Don't wait for vsync

	// Try Creating Using Mixed Vertex Shader
	if (!g_pD3dDevice) {
		// 1 - Try Create Device (hal, mixed vertex, stencil format best)
		d3dpp.PresentationInterval = d3dPresentInterval;
		d3dpp.AutoDepthStencilFormat = bestDSFormat;
		hresult = g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_hWnd, dwBehaviorFlagsMixed, &d3dpp, &g_pD3dDevice);
		if (hresult == D3D_OK) {
			LogInfo("CreateDevice(1) OK");
		} else {
			LogWarn("CreateDevice(1) FAILED - hresult=" << D3dResultStr(hresult));
		}

		if (!g_pD3dDevice) {
			// 2 - Try Create Device (hal, mixed vertex, stencil format default)
			d3dpp.AutoDepthStencilFormat = defaultDSFormat;
			hresult = g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_hWnd, dwBehaviorFlagsMixed, &d3dpp, &g_pD3dDevice);
			if (hresult == D3D_OK) {
				LogInfo("CreateDevice(2) OK");
			} else {
				LogWarn("CreateDevice(2) FAILED - hresult=" << D3dResultStr(hresult));
			}
		}

		// If Created Validate Mixed Vertex Shader
		if (g_pD3dDevice) {
			D3DCAPS9 pCaps;
			g_pD3dDevice->GetDeviceCaps(&pCaps);
			if (pCaps.VertexShaderVersion >= D3DVS_VERSION(1, 1))
				m_shaderSupport = TRUE;
		}
	}

	// Try Creating Using Software Vertex Shader
	if (!g_pD3dDevice) {
		// 3 - Try Create Device (hal, sw vertex, stencil format best, presentation interval default)
		d3dpp.PresentationInterval = d3dPresentInterval;
		d3dpp.AutoDepthStencilFormat = bestDSFormat;
		hresult = g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_hWnd, dwBehaviorFlagsSW, &d3dpp, &g_pD3dDevice);
		if (hresult == D3D_OK) {
			LogInfo("CreateDevice(3) OK");
		} else {
			LogWarn("CreateDevice(3) FAILED - hresult=" << D3dResultStr(hresult));
		}

		if (!g_pD3dDevice) {
			// 4 - Try Create Device (hal, sw vertex, stencil format default, presentation interval default)
			d3dpp.AutoDepthStencilFormat = defaultDSFormat;
			hresult = g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_hWnd, dwBehaviorFlagsSW, &d3dpp, &g_pD3dDevice);
			if (hresult == D3D_OK) {
				LogInfo("CreateDevice(4) OK");
			} else {
				LogWarn("CreateDevice(4) FAILED - hresult=" << D3dResultStr(hresult));
			}
		}

		// If Created Validate Software Vertex Shader
		if (g_pD3dDevice) {
			D3DCAPS9 pCaps;
			g_pD3dDevice->GetDeviceCaps(&pCaps);
			if (pCaps.VertexShaderVersion >= D3DVS_VERSION(1, 1))
				m_shaderSupport = TRUE;
		}
	}

	// Try Full Software Support
	BOOL usingFullSw = false;
	if (!g_pD3dDevice) {
		hresult = g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, m_hWnd, dwBehaviorFlagsSW, &d3dpp, &g_pD3dDevice);
		if (hresult == D3D_OK) {
			usingFullSw = TRUE;
			LogInfo("CreateDevice(5) OK");
		} else {
			LogWarn("CreateDevice(5) FAILED - hresult=" << D3dResultStr(hresult));
		}
	}

	// If Still Not Created There Is Nothing We Can Do
	if (!g_pD3dDevice) {
		g_pD3dDevice = pD3dDeviceBackup;
		LogError("D3D_ERROR_CREATE_DEVICE");
		m_d3dError = D3D_ERROR_CREATE_DEVICE;
		return m_d3dError;
	}

	// set up shadow plane
	if (m_shadowSquareVB) {
		m_shadowSquareVB->Release();
		m_shadowSquareVB = 0;
	}

	// get the monitor refresh rate in milliseconds
	D3DDISPLAYMODE displayMode;
	g_pD3dDevice->GetDisplayMode(0, &displayMode);

	// create shadow plane - a big square for rendering the stencilbuffer contents
	if (g_pD3dDevice->CreateVertexBuffer(
			4 * sizeof(SHADOWVERTEX),
			D3DUSAGE_WRITEONLY,
			D3DFVF_XYZRHW | D3DFVF_DIFFUSE,
			D3DPOOL_MANAGED,
			&m_shadowSquareVB, NULL) == 0) {
		SHADOWVERTEX* v;
		float sx = (float)d3dpp.BackBufferWidth;
		float sy = (float)d3dpp.BackBufferHeight;
		m_shadowSquareVB->Lock(0, 0, (void**)&v, 0);
		v[0].p = D3DXVECTOR4(0, sy, 0.0f, 1.0f);
		v[1].p = D3DXVECTOR4(0, 0, 0.0f, 1.0f);
		v[2].p = D3DXVECTOR4(sx, sy, 0.0f, 1.0f);
		v[3].p = D3DXVECTOR4(sx, 0, 0.0f, 1.0f);
		v[0].color = 0x7f000000;
		v[1].color = 0x7f000000;
		v[2].color = 0x7f000000;
		v[3].color = 0x7f000000;
		m_shadowSquareVB->Unlock();
		LogInfo("CreateVertexBuffer() OK");
	} else {
		LogWarn("CreateVertexBuffer() FAILED");
	}

	// Disable Mouse In FullScreen
	SetCursorEnabled(!fullscreen);

	if (fullscreen != TRUE) {
		if (usingFullSw) {
			m_curModeRect = RECT({ 0, 0, WIN_WIDTH_MIN, WIN_HEIGHT_MIN });
			m_curModeDepth = 16;
		}
	}

	// Sets Helpers::GlobalD3DDevice() & Helpers::G_DEVICE()
	DxDevice::Ptr()->SetDevice(g_pD3dDevice);

	// Set Client Engine Device
	SetD3dDevice();

	LogInfo("D3D_SUCCESS");
	m_d3dError = D3D_SUCCESS;
	return m_d3dError;
}

void ClientEngine_CFrameWnd::OnMove(int x, int y) {
	if (!m_pCABD3DLIB)
		return;

	// Call Windows Default OnMove() Handler
	CFrameWnd::OnMove(x, y);

	m_pCABD3DLIB->m_curModeRect = RECT({ 0, 0, RectWidth(m_curModeRect), RectHeight(m_curModeRect) });
}

void ClientEngine_CFrameWnd::OnSize(UINT type, int x, int y) {
	// Call Windows Default OnSize() Handler
	CFrameWnd::OnSize(type, x, y);

	m_windowStateGL = type;
	m_szXMonitor = x;
	m_szYMonitor = y;
}

DxDevice::TextureMemoryStats ClientEngine_CFrameWnd::GetTextureMemoryStats() const {
	DxDevice::TextureMemoryStats tms;
	DxDevice* pdd = DxDevice::Ptr();
	if (pdd)
		tms = pdd->GetTextureMemoryStats();
	return tms;
}

void ClientEngine_CFrameWnd::LogMemory() const {
	GetAppMemoryStats().Log();
	GetTextureMemoryStats().Log();
}

void ClientEngine_CFrameWnd::TrapCursor(bool bTrap) {
	static POINT p = { 0, 0 };

	if (bTrap) {
		// Record the last position where the cursor was trapped.
		GetCursorPos(&p);

		RECT clipRect = {
			0, 0, 0, 0
		};
		ClipCursor(&clipRect);
	} else {
		ClipCursor(NULL);
		// Restore the previously recorded cursor position.
		SetCursorPos(p.x, p.y);
	}
}

void ClientEngine_CFrameWnd::EnableRawInput(bool bEnabled) {
	LogInfo("EnableRawInput(" << bEnabled << ")");
	if (bEnabled) {
		RegisterRawInputDevices();

		UpdateMouseButtonStates();

		ResetVirtualCursor();
	} else {
		//UnregisterRawInputDevices();
	}

	m_bIsRawInputEnabled = bEnabled;
}

void ClientEngine_CFrameWnd::RegisterRawInputDevices() {
	RAWINPUTDEVICE Rid;

	Rid.usUsagePage = 0x01;
	Rid.usUsage = 0x02;
	Rid.dwFlags = 0; // adds HID mouse and also ignores legacy mouse messages
	Rid.hwndTarget = NULL; // AfxGetMainWnd()->m_hWnd;
	BOOL bResult = ::RegisterRawInputDevices(&Rid, 1, sizeof(RAWINPUTDEVICE));
	if (bResult == FALSE) {
		//registration failed. Call GetLastError for the cause of the error
		LogWarn("Could not register a mouse as a RawInput device!");
	}
}

// NOTE: The unregistering seems to mess up DirectInput too, so best to keep off from calling this function for now.
void ClientEngine_CFrameWnd::UnregisterRawInputDevices() {
	RAWINPUTDEVICE Rid;

	Rid.usUsagePage = 0x01;
	Rid.usUsage = 0x02;
	Rid.dwFlags = RIDEV_REMOVE;
	Rid.hwndTarget = NULL;
	BOOL bResult = ::RegisterRawInputDevices(&Rid, 1, sizeof(RAWINPUTDEVICE));
	if (bResult == FALSE) {
		//registration failed. Call GetLastError for the cause of the error
		LogWarn("Could not unregister the mouse as a RawInput device!");
	}
}

void ClientEngine_CFrameWnd::ResetVirtualCursor() {
	m_xVirtualCursor = Vector2i(0, 0);
}

void ClientEngine_CFrameWnd::MoveVirtualCursor(int x, int y) {
	m_xVirtualCursor.x += x;
	m_xVirtualCursor.y += y;

	OnMouseMoved(x, y);
}

void ClientEngine_CFrameWnd::UpdateMouseButtonStates() {
	// Update mouse key states
	static const int s_aiKEY_CODES[s_uMOUSE_BUTTON_COUNT] = {
		VK_LBUTTON,
		VK_RBUTTON,
		VK_MBUTTON,
		VK_XBUTTON1,
		VK_XBUTTON2
	};
	for (UINT u = 0; u < s_uMOUSE_BUTTON_COUNT; u++) {
		int iKeyCode = s_aiKEY_CODES[u];
		bool bPressed = (GetKeyState(iKeyCode) & 0x100) != 0;
		SetMouseButtonState(iKeyCode, bPressed, true);
	}
}

void ClientEngine_CFrameWnd::SetMouseButtonState(int iKeyCode, bool bPressed, bool bSuppressEvent = false) {
	bool bSuccess = false;
	switch (iKeyCode) {
		case VK_LBUTTON:
			m_abMouseButtonStates[0] = bPressed;
			bSuccess = true;
			break;
		case VK_RBUTTON:
			m_abMouseButtonStates[1] = bPressed;
			bSuccess = true;
			break;
		case VK_MBUTTON:
			m_abMouseButtonStates[2] = bPressed;
			bSuccess = true;
			break;
		case VK_XBUTTON1:
			m_abMouseButtonStates[3] = bPressed;
			bSuccess = true;
			break;
		case VK_XBUTTON2:
			m_abMouseButtonStates[4] = bPressed;
			bSuccess = true;
			break;
		default:
			LogWarn("Invalid mouse button key code");
	}

	if (bSuccess && !bSuppressEvent) {
		OnMouseStateChanged(iKeyCode, bPressed);
	}
}

bool ClientEngine_CFrameWnd::IsMouseButtonPressed(int iKeyCode) {
	switch (iKeyCode) {
		case VK_LBUTTON:
			return m_abMouseButtonStates[0];
		case VK_RBUTTON:
			return m_abMouseButtonStates[1];
		case VK_MBUTTON:
			return m_abMouseButtonStates[2];
		case VK_XBUTTON1:
			return m_abMouseButtonStates[3];
		case VK_XBUTTON2:
			return m_abMouseButtonStates[4];
		default:
			LogWarn("Invalid mouse button key code");
	}

	return false;
}

void ClientEngine_CFrameWnd::OnMouseStateChanged(int iKeyCode, bool bPressed) {
	if (m_bIsRawInputEnabled) {
		HandleWindowsMouseButtonEvent(iKeyCode, bPressed);
	}
}

void ClientEngine_CFrameWnd::OnMouseMoved(int xRelative, int yRelative) {
	if (m_bIsRawInputEnabled) {
		int iButtonsPressed =
			(int)IsMouseButtonPressed(VK_LBUTTON) |
			(int)IsMouseButtonPressed(VK_RBUTTON) << 1 |
			(int)IsMouseButtonPressed(VK_MBUTTON) << 2 |
			(int)IsMouseButtonPressed(VK_XBUTTON1) << 3 |
			(int)IsMouseButtonPressed(VK_XBUTTON2) << 4;

		HandleWindowsMouseMoveEvent(Vector2i(xRelative, yRelative), iButtonsPressed);
	}
}

void ClientEngine_CFrameWnd::OnRawInput(UINT nInputCode, HRAWINPUT hRawInput) {
	// Did the input occur while the application was in the foreground?
	if (nInputCode != RIM_INPUT)
		return;

	UINT uSize = 0;
	RAWINPUT* pRawData = NULL;
	bool bSuccess = false;

	// Get data size
	UINT uResult = GetRawInputData(hRawInput, RID_INPUT, NULL, &uSize, sizeof(RAWINPUTHEADER));
	if (uResult == 0) {
		if (uSize > 0) {
			pRawData = (RAWINPUT*)malloc(sizeof(BYTE) * uSize);
			uResult = GetRawInputData(hRawInput, RID_INPUT, (void*)pRawData, &uSize, sizeof(RAWINPUTHEADER));
			bSuccess = (uResult != (UINT)(-1));
		}
	}

	if (bSuccess) {
		// Is the input coming from a mouse device?
		if (pRawData->header.dwType == RIM_TYPEMOUSE) {
			if ((pRawData->data.mouse.usFlags & (USHORT)0x0001) == 0) {
				const int xPosRelative = pRawData->data.mouse.lLastX;
				const int yPosRelative = pRawData->data.mouse.lLastY;

				// Update mouse button states
				const ULONG ulButtonStates = pRawData->data.mouse.ulButtons;
				if (ulButtonStates & RI_MOUSE_LEFT_BUTTON_DOWN)
					SetMouseButtonState(VK_LBUTTON, true);
				else if (ulButtonStates & RI_MOUSE_LEFT_BUTTON_UP)
					SetMouseButtonState(VK_LBUTTON, false);

				if (ulButtonStates & RI_MOUSE_RIGHT_BUTTON_DOWN)
					SetMouseButtonState(VK_RBUTTON, true);
				else if (ulButtonStates & RI_MOUSE_RIGHT_BUTTON_UP)
					SetMouseButtonState(VK_RBUTTON, false);

				if (ulButtonStates & RI_MOUSE_MIDDLE_BUTTON_DOWN)
					SetMouseButtonState(VK_MBUTTON, true);
				else if (ulButtonStates & RI_MOUSE_MIDDLE_BUTTON_UP)
					SetMouseButtonState(VK_MBUTTON, false);

				if (ulButtonStates & RI_MOUSE_BUTTON_4_DOWN)
					SetMouseButtonState(VK_XBUTTON1, true);
				else if (ulButtonStates & RI_MOUSE_BUTTON_4_UP)
					SetMouseButtonState(VK_XBUTTON1, false);

				if (ulButtonStates & RI_MOUSE_BUTTON_5_DOWN)
					SetMouseButtonState(VK_XBUTTON2, true);
				else if (ulButtonStates & RI_MOUSE_BUTTON_5_UP)
					SetMouseButtonState(VK_XBUTTON2, false);

				if (xPosRelative != 0 || yPosRelative != 0)
					MoveVirtualCursor(xPosRelative, yPosRelative);
			}
		}
	}

	if (pRawData)
		free(pRawData);
}

} // namespace KEP