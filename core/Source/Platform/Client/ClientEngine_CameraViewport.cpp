///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ClientEngine.h"
#include "CPhysicsEmuClass.h"
#include "Core/Math/Interpolate.h"
#include "Core/Math/Rotation.h"
#include "Common/KEPUtil/FrameTimeProfiler.h"

namespace KEP {

static LogInstance("ClientEngine");

bool ClientEngine::InitializeCamerasAndViewports() {
	DestroyAllViewports();
	m_runtimeViewportDB = new CRuntimeViewportList();
	DestroyAllCameras();
	m_pRuntimeCameras->DelAllExceptDefault();

	m_pCameraList = new CCameraObjList();
	m_pViewportList = new CViewportList();
	CreateCamera("MainCamera");
	CreateViewport("MainViewport");
	AddRuntimeViewport(0);

	return true;
}

int ClientEngine::CreateCamera(const std::string& cameraName) {
	if (!m_pCameraList)
		return 0;
	auto pCO = new CCameraObj(cameraName);
	m_pCameraList->AddTail(pCO);
	return m_pCameraList->GetCount() - 1;
}

void ClientEngine::DestroyAllCameras() {
	if (!m_pCameraList)
		return;

	POSITION posDel = NULL;
	for (POSITION posLoc = m_pCameraList->GetHeadPosition(); (posDel = posLoc) != NULL;) {
		auto pCO = (CCameraObj*)m_pCameraList->GetNext(posLoc);
		if (!pCO)
			continue;
		delete pCO;
		pCO = nullptr;
	}

	m_pCameraList->RemoveAll();
	delete m_pCameraList;
	m_pCameraList = nullptr;
}

int ClientEngine::CreateViewport(const std::string& viewportName) {
	if (!m_pViewportList)
		return FALSE;
	auto pVO = new CViewportObj(viewportName);
	m_pViewportList->AddTail(pVO);
	return m_pCameraList->GetCount() - 1;
}

void ClientEngine::DestroyAllViewports() {
	if (!m_pViewportList)
		return;

	for (POSITION posLoc = m_pViewportList->GetHeadPosition(); posLoc != NULL;) {
		auto pVO = (CViewportObj*)m_pViewportList->GetNext(posLoc);
		delete pVO;
		pVO = nullptr;
	}

	m_pViewportList->RemoveAll();
	delete m_pViewportList;
	m_pViewportList = nullptr;

	if (m_runtimeViewportDB) {
		for (POSITION posLoc = m_runtimeViewportDB->GetHeadPosition(); posLoc != NULL;) {
			auto pVO = (CViewportObj*)m_runtimeViewportDB->GetNext(posLoc);
			delete pVO;
			pVO = nullptr;
		}
	}

	m_runtimeViewportDB->RemoveAll();
	delete m_runtimeViewportDB;
	m_runtimeViewportDB = nullptr;
}

int ClientEngine::AddRuntimeViewport(int viewport_db_index) {
	if (!m_pViewportList)
		return 0;
	POSITION posLoc = m_pViewportList->FindIndex(viewport_db_index);
	if (posLoc) {
		auto pVO = (CViewportObj*)m_pViewportList->GetAt(posLoc);
		if (!pVO || !m_runtimeViewportDB)
			return 0;
		std::string new_viewport_name = m_runtimeViewportDB->GetNewViewportName(pVO->GetName());
		CViewportObj* pVO_new = new CViewportObj(
			new_viewport_name,
			pVO->m_X1,
			pVO->m_Y1,
			pVO->m_X2,
			pVO->m_Y2);
		ActivateViewport(pVO_new);
		return m_runtimeViewportDB->AddViewport(pVO_new);
	}
	return 0;
}

bool ClientEngine::ActivateViewport(CViewportObj* pVO, int width, int height) {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "ActivateViewport");

	if (!pVO || !g_pD3dDevice || !m_pRenderStateMgr)
		return false;

	// Get Actual Viewport Dimensions
	if (width == 0 || height == 0) {
		width = RectWidth(m_pCABD3DLIB->m_curModeRect);
		height = RectHeight(m_pCABD3DLIB->m_curModeRect);
	}

	// Activate Viewport
	pVO->Activate(g_pD3dDevice, width, height);

	// Projection Matrix save for RenderBlades
	m_projection_matrix = pVO->m_projMatrix;
	m_pRenderStateMgr->SetProjectionMatrix(reinterpret_cast<D3DXMATRIX*>(&m_projection_matrix));

	auto reDevice = GetReDeviceState();
	if (reDevice) {
		auto reDeviceState = reDevice->GetRenderState();
		if (reDeviceState)
			reDeviceState->SetProjMatrix(reinterpret_cast<D3DXMATRIX*>(&m_projection_matrix));
	}

	// View Matrix save view matrix for RenderBlades
	m_view_matrix = pVO->m_viewMatrix;
	m_pRenderStateMgr->SetViewMatrix(reinterpret_cast<D3DXMATRIX*>(&pVO->m_viewMatrix));

	reDevice = GetReDeviceState();
	if (reDevice) {
		auto reDeviceState = reDevice->GetRenderState();
		if (reDeviceState)
			reDeviceState->SetViewMatrix(reinterpret_cast<D3DMATRIX*>(&m_view_matrix));
	}

	// save camera world position in state manager
	m_pRenderStateMgr->m_camWorldPosition = reinterpret_cast<D3DVECTOR&>(pVO->m_camWorldPosition);

	return true;
}

void ClientEngine::BindCameraToViewport(CCameraObj* pCO, int runtime_viewport_index) {
	if (!pCO)
		return;

	POSITION pos = m_runtimeViewportDB->FindIndex(runtime_viewport_index);
	if (!pos)
		return;

	auto pVO = (CViewportObj*)m_runtimeViewportDB->GetAt(pos);
	if (!pVO)
		return;

	pVO->SetCamera(pCO);
	SendCameraEvent(pCO, CameraAction::Bind, runtime_viewport_index);
}

CViewportObj* ClientEngine::GetActiveViewport() const {
	return m_runtimeViewportDB->GetActiveViewport();
}

CCameraObj* ClientEngine::GetActiveViewportCamera() const {
	auto pVO = GetActiveViewport();
	return pVO ? pVO->GetCamera() : nullptr;
}

bool ClientEngine::SetActiveCameraForMovementObjectByIndexOffset(CMovementObj* pMO, int cameraIndexOffset, bool forceChange) {
	if (!pMO)
		pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	// default to first camera in list if a current cam has not been set
	bool can_change = false;
	if (pMO->getCurrentCameraDataIndex() == -1 && pMO->getCameraCount() > 0) {
		if (m_startCamera == -1)
			m_startCamera = 0;
		pMO->setCurrentCameraDataIndex(m_startCamera);
		can_change = true;
	} else if (cameraIndexOffset != 0) {
		// if index offset isn't zero, we can change because we are going to a diff cam
		can_change = true;
	}
	if (!can_change && !forceChange)
		return false;

	int cameraIndex = (pMO->getCurrentCameraDataIndex() + pMO->getCameraCount() + cameraIndexOffset) % pMO->getCameraCount();
	return SetActiveCameraForMovementObjectByIndex(pMO, cameraIndex);
}

bool ClientEngine::SetActiveCameraForMovementObjectByIndex(CMovementObj* pMO, int cameraIndex) {
	if (!pMO)
		pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	if (cameraIndex < 0 || cameraIndex >= (int)pMO->getCameraCount())
		return false;

	// Set Movement Object Active Camera
	pMO->setCurrentCameraDataIndex(cameraIndex);

	// set CMovementObj::m_firstPersonMode based on camera's mode
	CCameraObj* pCO = GetActiveCameraForMovementObject(pMO);
	if (pCO) {
		Matrix44f positionMatrix = pMO->getBaseFrameMatrix();
		pMO->setFirstPersonMode(false);
		SkeletalParticleEmitterCallback(positionMatrix, pMO->getSkeleton(), true);

		// set the actor's 'current' fov to match the new camera's
		auto pMC = pMO->getMovementCaps();
		if (pMC)
			pMC->SetFov(pCO->GetFovY());
		BindCameraToViewport(pCO, 0);
		m_startCamera = pMO->getCurrentCameraDataIndex();
	}

	return true;
}

int ClientEngine::GetActiveCameraIndexForMovementObject(CMovementObj* pMO) const {
	return (pMO ? pMO->getCurrentCameraDataIndex() : -1);
}

CCameraObj* ClientEngine::GetActiveCameraForMovementObject(CMovementObj* pMO) const {
	if (!pMO || !m_pRuntimeCameras)
		return nullptr;
	auto camData = pMO->getCurrentCameraData();
	return (camData.m_runtimeCameraId > 0) ? m_pRuntimeCameras->GetCamera(camData.m_runtimeCameraId) : nullptr;
}

CCameraObj* ClientEngine::GetActiveCameraForMe() const {
	auto pMO_me = GetMovementObjMe();
	return (pMO_me ? GetActiveCameraForMovementObject(pMO_me) : nullptr);
}

Vector3f ClientEngine::GetActiveCameraPositionForMe() const {
	auto pCO = GetActiveCameraForMe();
	return pCO ? pCO->m_camWorldPosition : Vector3f(0, 0, 0);
}

Vector3f ClientEngine::GetActiveCameraOrientationForMe() const {
	auto pCO = GetActiveCameraForMe();
	return pCO ? pCO->m_camWorldVectorZ : Vector3f(0, 0, 0);
}

Vector3f ClientEngine::GetActiveCameraUpVectorForMe() const {
	auto pCO = GetActiveCameraForMe();
	return pCO ? pCO->m_camWorldVectorY : Vector3f(0, 0, 0);
}

bool ClientEngine::SetActiveCameraAutoAzimuthForMe(float radians) {
	m_cfgCamAutoAzimuth = radians;
	auto pCO = GetActiveCameraForMe();
	return pCO ? pCO->SetOrbitAutoAzimuth(radians) : false;
}

float ClientEngine::GetActiveCameraAutoAzimuthForMe() const {
	auto pCO = GetActiveCameraForMe();
	return pCO ? pCO->GetOrbitAutoAzimuth() : 0.0f;
}

bool ClientEngine::SetActiveCameraAutoElevationForMe(float radians) {
	m_cfgCamAutoElevation = radians;
	auto pCO = GetActiveCameraForMe();
	return pCO ? pCO->SetOrbitAutoElevation(radians) : false;
}

float ClientEngine::GetActiveCameraAutoElevationForMe() const {
	auto pCO = GetActiveCameraForMe();
	return pCO ? pCO->GetOrbitAutoElevation() : 0.0f;
}

void ClientEngine::InitCamerasForMovementObject(CMovementObj* pMO) {
	if (!pMO)
		return;

	// Add Movement Object Cameras To Runtime Cameras List
	CCameraObj* pCO = nullptr;
	auto cameraCount = pMO->getCameraCount();
	for (size_t cameraIndex = 0; cameraIndex < cameraCount; cameraIndex++) {
		const auto& cameraData = pMO->getCameraData(cameraIndex);
		if (cameraData.m_runtimeCameraId < 1) {
			auto newCameraId = m_pRuntimeCameras->Add(cameraData.m_cameraDBIndex);
			pMO->setRuntimeCameraId(cameraIndex, newCameraId);
			pCO = m_pRuntimeCameras->GetCamera(newCameraId);
			AttachCameraToObject(pCO, ObjectType::MOVEMENT, pMO->getNetTraceId());
		}
	}

	// Set Active Camera
	SetActiveCameraForMovementObjectByIndexOffset(pMO, 0, false);

	// Initialize Camera Settings
	if (pCO) {
		auto pMC = pMO->getMovementCaps();
		if (pMC && pMC->GetFovChangeSensitivity() != 0.0f) {
			pMC->SetFov(pCO->GetFovY());
			pMC->SetFovLast(pCO->GetFovY());
		}
		SetActiveCameraAutoAzimuthForMe(m_cfgCamAutoAzimuth);
		SetActiveCameraAutoElevationForMe(m_cfgCamAutoElevation);
	}
}

bool ClientEngine::AttachCameraToObject(CCameraObj* pCO, ObjectType objType, int objId) {
	if (!pCO)
		return false;
	return pCO->ObjectAttach(objType, objId);
}

bool ClientEngine::AttachActiveViewportCameraToObject(ObjectType objType, int objId) {
	auto pCO = GetActiveViewportCamera();
	if (!pCO)
		return false;
	return pCO->ObjectAttach(objType, objId);
}

void ClientEngine::HandleCameraCollision(CCameraObj* pCO, const Vector3f& startSegment, const Vector3f& endSegment) {
	if (!pCO || !pCO->m_baseFrame)
		return;

	// Build the look-at vector.
	Vector3f lookAt = startSegment - endSegment;
	if (!lookAt.Normalize())
		return;

	// Radius is the "fudge factor" for tweaking the ray collision.
	// This is a temporary hack until I can fully rework the camera collision code.
	// (Static variable for debug tweaking purposes.)
	static float radius = 1.5f;

	std::function<bool(CDynamicPlacementObj*)> CollisionFilter;

	// Get Camera Object Attachment
	ObjectType objType;
	int objId;
	pCO->ObjectAttachGet(objType, objId);

	// Collision Filter ?
	if (ObjectIsMe(objType, objId)) {
		// My Vehicle Camera Collision ?
		auto pMO = GetMovementObjMe();
		if (pMO && pMO->getPhysicsVehicle()) {
			int iVehiclePlacementId = pMO->getVehiclePlacementId();
			if (iVehiclePlacementId) {
				CollisionFilter = [iVehiclePlacementId](CDynamicPlacementObj* pObj) -> bool {
					return pObj->GetPlacementId() != iVehiclePlacementId;
				};
			}
		}
	} else if (objType == ObjectType::DYNAMIC) {
		// Attached Dynamic Object Camera Collision ?
		auto pDPO = GetDynamicObject(objId);
		if (pDPO) {
			int placementId = pDPO->GetPlacementId();
			CollisionFilter = [placementId](CDynamicPlacementObj* pDPO) -> bool {
				return pDPO->GetPlacementId() != placementId;
			};
		}
	}

	Vector3f cameraPos(endSegment);
	Vector3f contactPos;
	float contactDist;
	static const float tolerance = 0.5f;
	Vector3f adjustedEndSegment = endSegment - (lookAt * radius); // Push the "end" segment out by the adjustment amount.
	if (EnvironmentSegmentTest(contactPos, contactDist, startSegment, adjustedEndSegment, tolerance, CollisionFilter) != ObjectType::NONE) {
		// Now adjust the position by the radius.
		cameraPos = contactPos + (lookAt * radius);
	}

	// Set the camera's adjusted position.
	pCO->m_baseFrame->SetPosition(cameraPos);
}

void ClientEngine::CameraCallback(CCameraObj* pCO, CameraAction cameraAction) {
	if (!pCO || (cameraAction != CameraAction::Update))
		return;

	OrbitCameraCallback(pCO);

	pCO->EngineCameraCallback();
}

void ClientEngine::OrbitCameraCallback(CCameraObj* pCO) {
	if (!pCO || !pCO->m_baseFrame || (pCO != GetActiveViewportCamera()))
		return;

	pCO->UpdatePosition();

	// DRF - ED-7251 - Camera Cull To Fog Distance
	auto pSMO = GetRenderStateManager();
	if (pSMO) {
		auto fogCull = pSMO->GetFogEnabled() && pSMO->GetFogCanCull();

		// Use fog distance as far clip plane as long as it's greater then near plane distance.
		float farPlaneDist = 30000.0f;
		if (fogCull) {
			float fFogRange = pSMO->GetFogRange()->y;
			if (fFogRange > pCO->m_nearPlaneDist)
				farPlaneDist = fFogRange;
		}
		pCO->SetFarPlaneDist(farPlaneDist);
	}

	Vector3f focusPos, cameraPos;
	auto pFO = pCO->ObjectAttachGetBaseFrame();
	if (!pFO)
		return;

	pFO->GetPosition(focusPos);
	pCO->m_baseFrame->GetPosition(cameraPos);
	focusPos += pCO->m_orbitFocus;

	if (!pCO->m_useCollision || !m_cameraCollision)
		return;

	// Find the collision point (if any) between the line segment and the environment
	HandleCameraCollision(pCO, focusPos, cameraPos);
}

void ClientEngine::SendCameraEvent(CCameraObj* pCO, CameraAction cameraAction, int runtime_camera_id, int runtime_viewport_index) {
	auto pGS = dynamic_cast<IGetSet*>(pCO);
	if (!pGS || (runtime_viewport_index <= -1) || !m_dispatcher.HasHandler(m_cameraEventId))
		return;

	// Send Camera Event (handled by ClientEngine::HandleCameraEvent())
	IEvent* pEvent = m_dispatcher.MakeEvent(m_cameraEventId);
	if (!pEvent)
		return;
	(*pEvent->OutBuffer()) << (ULONG)pGS << (int)cameraAction << runtime_camera_id;
	m_dispatcher.ProcessSynchronousEvent(pEvent);
}

BOOL ClientEngine::IsMeshInView(const CEXMeshObj* mesh) const {
	Vector3f boxmin, boxmax;

	boxmin.x = mesh->GetBoundingBox().minX;
	boxmin.y = mesh->GetBoundingBox().minY;
	boxmin.z = mesh->GetBoundingBox().minZ;

	boxmax.x = mesh->GetBoundingBox().maxX;
	boxmax.y = mesh->GetBoundingBox().maxY;
	boxmax.z = mesh->GetBoundingBox().maxZ;

	if (!OutOfFOV(boxmin, boxmax)) {
		return TRUE;
	}

	return FALSE;
}

BOOL ClientEngine::IsBoxInView(const ABBOX& cl_box) const {
	Vector3f boxmin, boxmax;

	boxmin.x = cl_box.minX;
	boxmin.y = cl_box.minY;
	boxmin.z = cl_box.minZ;

	boxmax.x = cl_box.maxX;
	boxmax.y = cl_box.maxY;
	boxmax.z = cl_box.maxZ;

	if (!OutOfFOV(boxmin, boxmax)) {
		return TRUE;
	}

	return FALSE;
}

bool ClientEngine::OutOfFOV(const Vector3f& min, const Vector3f& max) const {
	Vector3f ptCenter = 0.5f * (min + max);
	Vector3f vHalfSize = 0.5f * (max - min);
	return OutOfFOV_CenterHalfSize(ptCenter, vHalfSize);
}

bool ClientEngine::OutOfFOV_CenterHalfSize(const Vector3f& ptBoxCenter, const Vector3f& vBoxHalfSize) const {
	for (size_t i = 0; i < 6; ++i) {
		const Plane3f& plane = m_aViewVolumePlanes[i];
		if (IsBoxCenterHalfSizeOutsideOfPlane(ptBoxCenter, vBoxHalfSize, plane))
			return true;
	}
	return false;
}

bool ClientEngine::OutOfFOV(const Vector3f& ptSphereCenter, float fRadius) const {
	for (size_t i = 0; i < 6; ++i) {
		const Plane3f& plane = m_aViewVolumePlanes[i];
		if (IsSphereOutsideOfNormalizedPlane(ptSphereCenter, fRadius, plane))
			return true;
	}
	return false;
}

bool ClientEngine::OutOfFovUntransformed(const Matrix44f& matrix, const ABBOX& bndBox) const {
	Vector3f bbmin(bndBox.minX, bndBox.minY, bndBox.minZ);
	Vector3f bbmax(bndBox.maxX, bndBox.maxY, bndBox.maxZ);
	Vector3f min = bbmin + matrix.GetTranslation();
	Vector3f max = bbmax + matrix.GetTranslation();
	return OutOfFOV(min, max);
}

EVENT_PROC_RC ClientEngine::HandleCameraEvent(IEvent* e) {
	if (!e)
		return EVENT_PROC_RC::NOT_PROCESSED;

	// Decode Event Data
	ULONG pGS;
	int cameraActionInt;
	int runtime_camera_id;
	(*e->InBuffer()) >> pGS >> cameraActionInt >> runtime_camera_id;
	CameraAction cameraAction = (CameraAction)cameraActionInt;

	auto pCO = dynamic_cast<CCameraObj*>((IGetSet*)pGS);
	if (!pCO)
		return EVENT_PROC_RC::NOT_PROCESSED;

	switch (cameraAction) {
		case CameraAction::Update:
		case CameraAction::Bind:
			CameraCallback(pCO, cameraAction);
			break;
	}

	return EVENT_PROC_RC::CONSUMED;
}

} // namespace KEP