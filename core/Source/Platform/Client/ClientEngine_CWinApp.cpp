///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "KEPCommon.h"
#include "ClientEngine_CWinApp.h"
#include "ClientEngine_CFrameWnd.h"
#include "LogHelper.h"
#include "Core/Math/KEPMath.h"

namespace KEP {

static LogInstance("Instance");

BEGIN_MESSAGE_MAP(ClientEngine_CWinApp, CWinApp)
END_MESSAGE_MAP()

BOOL ClientEngine_CWinApp::InitInstance() {
	// Treat all denormals as zero for MMX ops. This is a performance optimization.
	// Arithmetic operations with denormal arguments take much longer than normal.
	// "Flush to zero" mode just converts denormal results of calculations to zero.
	// "Denormals are zero" mode also converts denormal values to zero on load from memory.
	_MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);

	// No macro available to turn on "Denormals are zero".
	const unsigned int kDenormalsAreZero = 0x40;
	_mm_setcsr((_mm_getcsr() & ~kDenormalsAreZero) | kDenormalsAreZero);

	// Show Main Window
	m_pMainWnd->ShowWindow(SW_SHOWNORMAL);
	m_pMainWnd->UpdateWindow();
	return CWinApp::InitInstance();
}

int ClientEngine_CWinApp::Run() {
	// Run Main Loop
	FOREVER {
		// Assigned in ClientEngine_CFrameWnd::Init()
		if (!m_pCE_CFrameWnd)
			return 0;

		// Render Single Frame
		m_pCE_CFrameWnd->RenderLoop();

		// Dispatch Messages
		MSG msg = { 0 };
		while (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT)
				return 0;

			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
	}
}

} // namespace KEP