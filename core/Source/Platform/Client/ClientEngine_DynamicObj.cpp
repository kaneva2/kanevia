///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "Common\include\NetworkCfg.h"
#include "KEPPhysics\Universe.h"
#include "KEPPhysics\RigidBodyStatic.h"
#include "RuntimeSkeleton.h"
#include "Common\KEPUtil\FrameTimeProfiler.h"
#include "Common\include\CoreStatic\PhysicsUserData.h"
#include "UGC\UGCManager.h"

namespace KEP {

static LogInstance("Instance");

void ClientEngine::LogDynamicObjectList(bool verbose) {
	m_dynamicPlacementManager.Log(verbose);
}

// Add this object into the WOK inventory if it does not currently exist for the user
// in WOK. It can not be in the user's inventory, nor be placed in WOK (i.e. in the dynamic_objects
// table. This will allow a 3DApp to pickup an item from a deed (the individual item never existed in
// their inventory) and place it in their inventory. If the user has the item already on WOK, do nothing.
// Adding it when they already have it in WOK would allow them to accumulate multiple copies. WOK has limited
// numbers of an inventory item, 3DApps have infinite.
bool ClientEngine::AddDynamicObjectPlacement(int objId, const char* type) {
	CStringA strURL;
	CStringA strItemId;
	CStringA strGameId;

	strGameId.Format("%d", GetGameId());
	strItemId.Format("%d", objId);

	std::string kstrURL;

	kstrURL = GetURL(CLIENTDEFAULTINVENTORYSERVER_TAG, false);

	strURL = kstrURL.c_str();

	strURL += "&action=add&glid={1}&inventorySubType={2}&gameId={3}";

	strURL.Replace("{0}", "new"); // not used
	strURL.Replace("{1}", strItemId);
	strURL.Replace("{2}", "512"); //type );
	strURL.Replace("{3}", strGameId);

	std::string resultText;
	DWORD httpStatusCode;
	std::string httpStatusDescription;

	if (!_GetBrowserPage((const char*)strURL, 0, 0, resultText, &httpStatusCode, httpStatusDescription)) {
		LogError("GetBrowserPage() FAILED - '" << strURL << "'");
		return false;
	}

	return true;
}

bool ClientEngine::SendRemoveDynamicObjectEvent(int placementId) {
	// Check if local object before sending event to server
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	if (!pDPO->IsLocal()) {
		// Send message to server
		IEvent* newEvt = new RemoveDynamicObjectEvent(placementId);
		newEvt->SetFilter(GetZoneIndexAsULong());
		newEvt->AddTo(SERVER_NETID);
		m_dispatcher.QueueEvent(newEvt);
	} else {
		// Dispatch a local event instead
		IEvent* newEvt = new RemoveDynamicObjectEvent(placementId);
		m_dispatcher.QueueEvent(newEvt);
	}

	return true;
}

void ClientEngine::InvalidateDynamicObject(int placementId) {
	CDynamicPlacementObj* pDPO = m_dynamicPlacementManager.LookUpByPlacementId(placementId);
	if (pDPO != nullptr) {
		pDPO->InvalidateBaseObj();
		pDPO->ForEachRigidBody([this](Physics::RigidBodyStatic* pRigidBody) { this->m_pUniverse->Remove(pRigidBody); });
	}
}

//TODO: retire this function. Always look up dynamic objects by placement IDs.
// pos in [0 .. count)
int ClientEngine::GetDynamicObjectPlacementIdByPos(int pos) {
	auto pDPO = GetDynamicObjectByIndex(pos);
	return (pDPO ? pDPO->GetPlacementId() : 0);
}

std::string ClientEngine::GetDynamicObjectName(int placementId) {
	// Reset Return Value
	std::string name = "<null>";

	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return name;
	}

	if (pDPO->IsLocal()) {
		if (pDPO->IsBaseObjReady()) {
			StreamableDynamicObject* pSDO = pDPO->GetBaseObj();
			if (pSDO)
				name = pSDO->getName();
		}
	} else {
		GLID globalId = pDPO->GetGlobalId();
		auto pGIO = CGlobalInventoryObjList::GetInstance()->GetByGLID(globalId);
		if (pGIO) {
			if (pGIO->m_itemData)
				name = pGIO->m_itemData->m_itemName.GetString();
		} else {
			// Get Already Cached Content Metadata (don't cache if not already cached? bug?)
			ContentMetadata md(globalId);
			if (md.IsValid())
				name = md.getItemName();
		}
	}
	return name;
}

int ClientEngine::GetDynamicObjectGlid(int placementId) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return 0;
	}

	return pDPO->GetGlobalId();
}

bool ClientEngine::GetDynamicObjectAnimPosRot(int placementId, float& posX, float& posY, float& posZ, float& rotY) {
	// Reset Return Values
	posX = 0.0f;
	posY = 0.0f;
	posZ = 0.0f;
	rotY = 0.0f;

	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		// script does this all the time...
		// LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Get Dynamic Object Runtime Skeleton
	auto pRS = pDPO->getSkeleton();
	if (!pRS) {
		LogError("CDynamicPlacementObject->RuntimeSkeleton is null - placementId=" << placementId);
		return false;
	}

	// Get Dynamic Placement Object Position And Rotation
	float tempRx, tempRz;
	Matrix44f m = pRS->getWorldMatrix();
	posX = m.GetTranslation().x;
	posY = m.GetTranslation().y;
	posZ = m.GetTranslation().z;
	MatrixARB::RotationMatrixToYRot(m, tempRx, rotY, tempRz);
	return true;
}

bool ClientEngine::isDynamicObjectInited(int placementId) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObjectInited() FAILED - placementId=" << placementId);
		return false;
	}

	return pDPO->IsBaseObjInited();
}

// Determines where a placement would collide with another object when moving in the provided direction
// Returns whether a collision occurs and the location of the object when it collides.
// If no collision occurs, the position returned is the original object location.
std::tuple<bool, Vector3f> ClientEngine::DynamicObjectCollisionTest(int iPlacementId, const Vector3f& direction_, const boost::optional<Vector3f>& startPos) {
	static const float kfWorldRadius = 8192; // Use actual world bounds once we start keeping track of it.

	std::tuple<bool, Vector3f> error_ret(false, Vector3f(0, 0, 0));
	CDynamicPlacementObj* pDPO = GetDynamicObject(iPlacementId);
	if (!pDPO) {
		LogError("No object with placement ID " << iPlacementId);
		return error_ret;
	}

	Vector3f ptStartPos = startPos.is_initialized() ? startPos.get() : pDPO->GetPosition();
	std::get<1>(error_ret) = ptStartPos;

	Vector3f direction = direction_;
	if (!direction.Normalize()) {
		LogError("Invalid direction");
		return error_ret;
	}

	Physics::RigidBodyStatic* pRigidBody = pDPO->GetRigidBody().get();
	if (!pRigidBody) {
		LogError("Physics data not yet initialized for object " << iPlacementId << ". Base objected " << (pDPO->GetBaseObj() ? "loaded" : "not loaded"));
		return error_ret;
	}

	Vector4f boxMin, boxMax;
	pDPO->GetBoundingBox(boxMin, boxMax);
	Matrix44f mtxCurrent = pRigidBody->GetTransformMetric();
	Vector3f ptCurrent = FeetToMeters(ptStartPos);
	float fDistanceMax = kfWorldRadius - ptCurrent.Dot(direction) + Distance(boxMin, boxMax);
	float fDistance = fDistanceMax;
	if (fDistance <= 0) {
		// No collision. Should not be possible unless roundoff error or this object is not accounted for in world radius
		LogError("Object outside of world bounds.");
		return error_ret;
	}
	Vector3f ptTarget = ptCurrent + fDistance * direction;

	float fRelMovement = 0;
	if (!m_pUniverse->Collide(*pRigidBody, ptCurrent, ptTarget, out(fRelMovement), Physics::CollideOptions().SetCollisionMasks(CollisionGroup::All, CollisionGroup::Object)))
		return error_ret;

	Vector3f ptNewMeters = ptCurrent + fRelMovement * (ptTarget - ptCurrent);
	return std::make_tuple(true, MetersToFeet(ptNewMeters));
}

bool ClientEngine::IsDynamicObjectCustomizable(int placementId) {
	auto placementObj = GetDynamicObject(placementId);
	return placementObj ? placementObj->IsCustomizable() : false;
}

// This places a dynamic object locally.
// @param dynObjId the id of the dynamic object to place.
int ClientEngine::PlaceLocalDynamicObject(int globalId, const Vector3f* positionOverride, const Vector3f* directionOverride, const Vector3f* slideOverride, float scaleFactor) {
	Vector3f position, direction, slide;
	if (positionOverride) {
		position = *positionOverride;
	} else {
		position.x = position.y = position.z = 0.0f;

		auto pMO = GetMovementObjMe();
		if (pMO) {
			position = pMO->getLastPosition();
		}
	}

	if (directionOverride && slideOverride) {
		direction = *directionOverride;
		slide = *slideOverride;
	} else {
		direction.y = direction.x = 0.0f; // default direction
		direction.z = 1.0f;
		slide.z = slide.y = 0;
		slide.x = 1.f;
	}

	CDynamicPlacementObj* placementObjPtr = nullptr;
	ASSERT(globalId != GLID_INVALID);
	if (globalId != GLID_INVALID) {
		// Place local obj by GLID (Client DynObj)
		PlaceDynamicObjectArgs po_args;
		po_args.position = position;
		po_args.direction = direction;
		po_args.slide = slide;
		po_args.playerId = 0;
		po_args.placementId = GetNextLocalPlacementId();
		po_args.globalId = globalId;
		po_args.min.Set(-1, -1, -1);
		po_args.max.Set(1, 1, 1);
		po_args.scaleFactor = scaleFactor;
		placementObjPtr = PlaceDynamicObject(po_args);
	}
	if (!placementObjPtr)
		return 0; // local placement ID starts from -1

	placementObjPtr->FlushBoundBox();

	// invalidate the light caches since a new light is being added
	if (placementObjPtr && placementObjPtr->GetLight())
		InvalidateLightCaches(&placementObjPtr->m_position, placementObjPtr->GetLight()->m_range);

	return placementObjPtr->GetPlacementId();
}

bool ClientEngine::SubmitLocalDynamicObjectByPlacementId(long placementId, IEvent* submissionFeedbackEvent) {
	auto pDPO = GetDynamicObject(placementId);
	if (!GetUGCManager() || !pDPO || !pDPO->IsLocal() || !pDPO->IsBaseObjReady())
		return false;

	// Set ugcObjId for event
	GLID globalId = pDPO->GetGlobalId();
	if (submissionFeedbackEvent)
		submissionFeedbackEvent->SetObjectId((OBJECT_ID)globalId);

	return GetUGCManager()->SubmitUGCObj(UGC_TYPE_DYNAMIC_OBJ, globalId, submissionFeedbackEvent);
}

bool ClientEngine::GetDynamicObjectInfo(
	int placementId,
	std::string& name,
	std::string& desc,
	bool& mediaSupported,
	bool& isAttachable,
	bool& isInteractive,
	bool& isLocal,
	bool& isDerivable,
	int& assetId,
	int& friendId,
	std::string& customTexture,
	int& globalId,
	int& invSubType,
	std::string& textureUrl,
	int& gameItemId,
	int& playerId,
	DYN_OBJ_TYPE& objType) {
	// Reset Return Values
	name = "unknown";
	desc = "no description";
	mediaSupported = false;
	isAttachable = false;
	isInteractive = false;
	isLocal = false;
	isDerivable = false;
	assetId = 0;
	friendId = 0;
	customTexture = "None";
	globalId = 0;
	invSubType = 0;
	textureUrl = "";
	gameItemId = 0;
	playerId = 0;

	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	if (pDPO->IsLocal()) {
		if (pDPO->IsBaseObjReady()) {
			auto pSDO = pDPO->GetBaseObj();
			if (pSDO) {
				name = pSDO->getName();
				desc = "Imported from " + name;
			}
		}
	} else {
		GLID glid = pDPO->GetGlobalId();
		auto pGIO = CGlobalInventoryObjList::GetInstance()->GetByGLID(glid);
		if (pGIO) {
			if (pGIO->m_itemData) {
				name = pGIO->m_itemData->m_itemName;
				desc = pGIO->m_itemData->m_itemDescription;
			}
		} else {
			// Get Already Cached Content Metadata (or cache asynchronously if not already cached)
			ContentMetadata md(glid);
			if (md.IsValid()) {
				name = md.getItemName();
				desc = md.getItemDesc();
			} else {
				ContentService::Instance()->ContentMetadataCacheAsync(glid, nullptr);
			}
		}
	}

	mediaSupported = pDPO->MediaSupported();
	isAttachable = pDPO->IsAttachable();
	isInteractive = pDPO->IsInteractive();
	isLocal = pDPO->IsLocal();
	isDerivable = pDPO->IsDerivable();
	assetId = pDPO->GetAssetId();
	friendId = pDPO->GetFriendId();
	customTexture = pDPO->GetCustomTextureUrl(); // The only important value is the custom texture applied through pattern browser, which should be returned as ...\CustomTextures\...
	invSubType = pDPO->GetInventorySubType();
	globalId = pDPO->GetGlobalId();
	textureUrl = pDPO->GetTextureUrl();
	gameItemId = pDPO->GetGameItemId();
	playerId = pDPO->m_playerId; // ben - added
	objType = pDPO->ObjType(); // drf - added

	return true;
}

void ClientEngine::SetAnimationOnDynamicObject(int placementId, const AnimSettings& animSettings) {
	auto pDPO = GetDynamicObject(placementId);
	if (pDPO)
		pDPO->setPrimaryAnimationSettings(animSettings);
}

bool ClientEngine::GetDynamicObjectAnimation(int placementId, AnimSettings& animSettings, std::string& name) {
	// Reset Return Values
	animSettings = AnimSettings();
	name = "";

	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO)
		return false;

	// Get Animation Override Settings
	pDPO->getPrimaryAnimationSettings(animSettings);

	auto pRS = pDPO->getSkeleton();
	if (!pRS)
		return false;

	auto pSAH = pRS->getSkeletonAnimHeaderByGlid(animSettings.m_glid);
	if (!pSAH || !pSAH->LoadData())
		return false;

	// Get Animation Name
	name = pSAH->m_name;

	return true;
}

bool ClientEngine::GetDynamicObjectBoundingBox(int placementId, float* x1, float* y1, float* z1, float* x2, float* y2, float* z2) {
	// Reset Return Values
	*x1 = 0;
	*y1 = 0;
	*z1 = 0;
	*x2 = 0;
	*y2 = 0;
	*z2 = 0;

	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Get Dynamic Object Bounding Box
	Vector4f minB, maxB;
	pDPO->GetBoundingBox(minB, maxB);
	*x1 = minB.x;
	*y1 = minB.y;
	*z1 = minB.z;
	*x2 = maxB.x;
	*y2 = maxB.y;
	*z2 = maxB.z;
	return true;
}

bool ClientEngine::DynamicObjectCollisionFilter(int placementId, bool filter) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Set Collision Filter
	pDPO->SetCollisionEnabled(filter);
	return true;
}

bool ClientEngine::MoveDynamicObjectPlacement(int placementId, double dir, double slide, double lift) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Move Dynamic Object
	pDPO->MoveForward(dir);
	pDPO->Slide(slide);
	pDPO->Lift(lift);
	return true;
}

bool ClientEngine::SetDynamicObjectPosition(int placementId, const Vector3f& pos) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Set Dynamic Object Position
	pDPO->SetPosition(pos);
	return true;
}

bool ClientEngine::GetDynamicObjectPosition(int placementId, Vector3f& pos) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Get Dynamic Object Position
	pos = pDPO->GetPosition();

	return true;
}

bool ClientEngine::SetDynamicObjectRotation(int placementId, const Vector3f& rot) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Set Dynamic Object Rotation
	pDPO->SetRotationEulerAngleZXY(rot.x, rot.y, rot.z);

	return true;
}

bool ClientEngine::GetDynamicObjectRotation(int placementId, Vector3f& rot) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Get Dynamic Object Rotation
	Matrix44f matrix;
	if (!pDPO->GetWorldMatrix(matrix)) {
		// Skeleton not available
		rot.MakeZero();
	} else {
		// Use Daryl's version for centralized half-PI compensation logic
		MatrixARB::RotationMatrixToYRot(matrix, rot.x, rot.y, rot.z);
	}

	return true;
}

bool ClientEngine::GetDynamicObjectVisibility(int placementId, eVisibility& vis) {
	vis = eVisibility::HIDDEN_FOR_ALL;

	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Get DO visibility
	vis = pDPO->GetVisibility();

	return true;
}

bool ClientEngine::SetDynamicObjectVisibility(int placementId, const eVisibility& vis) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Set DO visibility
	pDPO->SetVisibility(vis);

	return true;
}

// Returns the last location of the dynamic object even if moving.
bool ClientEngine::GetDynamicObjectLastLocation(int placementId, Vector3f& pos) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Get Dynamic Object Last Location
	pos = pDPO->GetLastLocation();

	return true;
}

bool ClientEngine::GetDynamicObjectDistanceToPlayer(int placementId, double& dist) {
	// Reset Return Value
	dist = 0;

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	// Get My Avatar Position & Rotation
	Vector3f pos, rot;
	if (!GetMovementObjectPosRot(pMO, pos, rot))
		return false;

	// Get Dynamic Object Last Location (accurate even if animated)
	Vector3f posObj;
	if (!GetDynamicObjectLastLocation(placementId, posObj)) {
		LogWarn("GetDynamicObjectLastLocation() FAILED - placementId=" << placementId);
		return false;
	}

	// Return Distance Between Avatar & Dynamic Object
	dist = Distance(pos, posObj);
	return true;
}

//-----------------------------------------------------------------------------------------------
// This was added by Jonny 12/02/06 to allow the movement of a dynamic object in any direction
// in world coordinates.  This function is different from ClientEngine::MoveDynamicObjectPlacement
// because the other function uses coordinate space of the dynamic object, while this function
// uses world coordinate space.  We also round the direction in 45 degree increments and assume
// the up or y vector to be 0.  To allow more than 8 directions change the degree increment
// from 45 to something else.
//-----------------------------------------------------------------------------------------------
bool ClientEngine::MoveDynamicObjectPlacementInDirection(int placementId, double x, double y, double z, double distance) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Move Dynamic Object In Direction
	Vector3f dir;
	dir.x = x;
	dir.y = y;
	dir.z = z;
	pDPO->MoveInDirectionRounding(distance, dir, 8); //8 movable directions for WOK.
	return true;
}

bool ClientEngine::CanDynamicObjectSupportAnimation(int placementId) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Get Dynamic Object Runtime Skeleton
	auto pSkeleton = pDPO->getSkeleton();
	if (!pSkeleton) {
		LogError("CDynamicPlacementObject->RuntimeSkeleton is null - placementId=" << placementId);
		return false;
	}

	// Objects Cannot Animate Without Bones
	return pSkeleton->getBoneCount() > 0;
}

void ClientEngine::setDynamicObjectTexturePanning(int placementId, int meshId, int uvSetId, float uIncr, float vIncr) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return;
	}

	pDPO->SetTexturePanning(meshId, uvSetId, uIncr, vIncr);
}

bool ClientEngine::SetDynamicObjectHighlight(int placementId, bool highlight, float r, float g, float b, float a) {
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO)
		return false;
	pDPO->SetHighlight(highlight, D3DXCOLOR(r, g, b, a));
	return true;
}

inline bool isInfOrFltMax(float x) {
	return (x >= FLT_MAX) || (x <= -FLT_MAX);
}

inline bool isNanOrInfOrFltMax(float x) {
	return _isnan(x) || isInfOrFltMax(x);
}

bool ClientEngine::SaveDynamicObjectPlacementChanges(int placementId) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// don't let the client send invalid floats to server, this will result in DB syntax error alerts
	// when it tries to update floats with -1.#IND (for example)
	if (isNanOrInfOrFltMax(pDPO->m_position.x) || isNanOrInfOrFltMax(pDPO->m_position.y) || isNanOrInfOrFltMax(pDPO->m_position.z) ||
		isNanOrInfOrFltMax(pDPO->m_direction.x) || isNanOrInfOrFltMax(pDPO->m_direction.y) || isNanOrInfOrFltMax(pDPO->m_direction.z)) {
		LogError("Bad values! "
				 << "position=" << pDPO->m_position.x << "," << pDPO->m_position.y << "," << pDPO->m_position.z << " "
				 << "direction=" << pDPO->m_direction.x << "," << pDPO->m_direction.y << "," << pDPO->m_direction.z << " "
				 << "placementID=" << pDPO->GetPlacementId() << " "
				 << "zoneIndex=" << GetZoneIndexAsULong() << " "
				 << "zoneInstID=" << GetZoneInstanceId());
		return false;
	}

	if (pDPO->GetLight())
		InvalidateLightCaches(&(pDPO->m_positionOrig), pDPO->GetLight()->m_range);

	pDPO->SaveChanges();

	IEvent* e = new MoveAndRotateDynamicObjectEvent(placementId,
		pDPO->m_position.x,
		pDPO->m_position.y,
		pDPO->m_position.z,
		pDPO->m_direction.x,
		pDPO->m_direction.y,
		pDPO->m_direction.z,
		pDPO->m_slide.x,
		pDPO->m_slide.y,
		pDPO->m_slide.z);

	if (!pDPO->IsLocal())
		e->AddTo(SERVER_NETID);
	m_dispatcher.QueueEvent(e);

	//@@Watch: anything missing after bypassing server?
	// Server event handler profiling:
	// 1) ServerEngine validates request, queues a ZoneCustomizationBackgroundEvent/ZC_MOVE event in GameStateDispatcher
	// 2) GameStateDispatcher uses its worker thread to call registered MySqlEventProcessor to process ZoneCustomizationBackgroundEvent
	// 3) MySqlEventProcessor validates request, updates database and pass ZoneCustomizationBackgroundEvent to ServerEngine (with replyEvent set to a new MoveAndRotateDynamicObjectEvent)
	// 4) ServerEngine does nothing but requests CMultiPlayerObj to broadcast reply event to all players
	// 5) MoveAndRotateDynamicObjectEvent reaches all players (including initiator?)
	// For local dynamic objects, instead of going thru server, we send a local MoveAndRotateDynamicObjectEvent event directly to initiator itself.
	return true;
}

bool ClientEngine::CancelDynamicObjectPlacementChanges(int placementId) {
	// Get Dynamic Object By Id
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO) {
		LogWarn("GetDynamicObject() FAILED - placementId=" << placementId);
		return false;
	}

	// Cancel Dynamic Object Changes
	pDPO->CancelChanges();

	return true;
}

void ClientEngine::UpdateAllDynamicObjects() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "UpdateDynamicObjects");

	TimeMs timeMs = fTime::TimeMs();
	// Update dynamic objects positions
	m_dynamicPlacementManager.IterateObjectsForUpdate([this, timeMs](CDynamicPlacementObj* pDPO) {
		// Moved before camera cull to make sure their positions are updated,
		// even if the object isn't visible.  It could become visible if it
		// was given the chance to update.  Moved it again so particle and lights
		// move with it, too.  Hopefully.
		//
		// NOTE: That right now, this will set the m_WorldMatrix, but only if the object
		// is moving between two points.  Otherwise, the world matrix stays the same between
		// frames.
		pDPO->UpdateCallback(timeMs); // animated movement callback (i.e. when the DO is moved from point A to point B in C seconds)
	});
}

bool ClientEngine::DynamicObjectMakeTranslucent(int placementId) {
	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO)
		return false;

	pDPO->MakeTranslucent();
	return true;
}

bool ClientEngine::EnableDynamicObjectBoundingBoxRender(int placementId, bool enable) {
	if (placementId == 0) {
		// Global flag
		m_renderBoundingBoxForAllDynamicObjects = enable;
		return true;
	}

	auto pDPO = GetDynamicObject(placementId);
	if (!pDPO)
		return false;

	pDPO->SetBoundingBoxRenderEnabled(enable);
	return true;
}

void ClientEngine::sendPlaceDynamicObjectAtPositionEvent(const long long& glid, const short& invenType, const float& posX, const float& posY, const float& posZ, const float& rotX, const float& rotY, const float& rotZ) {
	IEvent* e = new PlaceDynamicObjectAtPositionEvent(glid, invenType, posX, posY, posZ, rotX, rotY, rotZ);
	e->AddTo(SERVER_NETID);
	m_dispatcher.QueueEvent(e);
}

bool ClientEngine::PlaceDynamicObjectByGlid(const GLID& glid, int invenType) {
	m_clickPlaceData.objectGlid = (long long)glid;
	if (m_clickPlaceData.placementId != 0) {
		RemoveDynamicObject(m_clickPlaceData.placementId);
		m_clickPlaceData.placementId = 0;
	}

	if (glid == GLID_INVALID)
		return true;

	float playerX, playerY, playerZ, playerRotDeg;
	GetMyPosition(playerX, playerY, playerZ, playerRotDeg);
	m_clickPlaceData.placementId = PlaceLocalDynamicObject(glid);
	m_clickPlaceData.placementHeight = playerY;
	m_clickPlaceData.orientation.x = 0.0f;
	m_clickPlaceData.orientation.y = 0.0f;
	m_clickPlaceData.orientation.z = 1.0f;
	m_clickPlaceData.invenType = invenType;
	if (m_clickPlaceData.placementId == 0)
		return false;

	auto pDPO = GetDynamicObject(m_clickPlaceData.placementId);
	if (!pDPO)
		return false;

	D3DCOLORVALUE col;
	col.a = 200;
	col.r = 128;
	col.g = 128;
	col.b = 128;
	pDPO->MakeTranslucent();

	return true;
}

} // namespace KEP