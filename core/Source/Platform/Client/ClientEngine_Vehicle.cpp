///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "RuntimeSkeleton.h"
#include "CPhysicsEmuClass.h"
#include "KEPPhysics/PhysicsFactory.h"
#include "KEPPhysics/DebugWindow.h"
#include "KEPPhysics/RigidBodyStatic.h"
#include "KEPPhysics/Universe.h"
#include "KEPPhysics/Vehicle.h"
#include "Common\include\CoreStatic\InputSystem.h"
#include "include\Core\Math\TransformUtil.h"

namespace KEP {

static LogInstance("ClientEngine");

std::shared_ptr<Physics::Vehicle> ClientEngine::GetMyVehicle() const {
	auto pMO = GetMovementObjMe();
	return pMO ? pMO->getPhysicsVehicle() : nullptr;
}

int ClientEngine::GetMyVehiclePlacementId() const {
	auto pMO = GetMovementObjMe();
	return pMO ? pMO->getVehiclePlacementId() : 0;
}

double ClientEngine::GetMyVehicleSpeedKPH() const {
	auto pVehicle = GetMyVehicle();
	return pVehicle ? pVehicle->GetSpeedKPH() : 0.0;
}

double ClientEngine::GetMyVehicleThrottle() {
	auto objId = GetMyVehiclePlacementId();
	double throttle = 0.0;
	ObjectGetAccel(ObjectType::DYNAMIC, objId, throttle);
	return throttle;
}

double ClientEngine::GetMyVehicleSkid() const {
	auto pVehicle = GetMyVehicle();
	return pVehicle ? pVehicle->GetSkidFactor() : 0.0;
}

void ClientEngine::EngagePhysicsVehicle(int placementId, const Vector3f& offsetMeters, const Quaternionf& orientation, const Physics::VehicleProperties& props) {
	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;
	DisengagePhysicsVehicle();

	// Set My Vehicle Placement Id
	pMO->setVehiclePlacementId(placementId);

	// Disable Vehicle Collision
	CDynamicPlacementObj* pDPO = this->GetDynamicObject(pMO->getVehiclePlacementId());
	if (pDPO)
		pDPO->SuspendCollision(true);

	Matrix44f mtxDOToPhysicsVehicleMeters;
	Quaternionf normalizedOrientation = orientation;
	if (normalizedOrientation.Normalize())
		ConvertRotation(normalizedOrientation, out(mtxDOToPhysicsVehicleMeters));
	else
		mtxDOToPhysicsVehicleMeters.MakeIdentity();
	mtxDOToPhysicsVehicleMeters.GetTranslation() = offsetMeters;

	Matrix44f mtxMovementObj = pMO->getBaseFrameMatrix();

	Matrix44f mtxDynamicObject;
	if (pDPO && pDPO->GetWorldMatrix(mtxDynamicObject)) {
	} else {
		mtxDynamicObject = mtxMovementObj;
		mtxDynamicObject.GetRowX() = -mtxDynamicObject.GetRowX();
		mtxDynamicObject.GetRowZ() = -mtxDynamicObject.GetRowZ();
	}

	pMO->setMtxPhysicsVehicleToDynamicObject(MetersToFeet(InverseOf(mtxDOToPhysicsVehicleMeters)));
	pMO->setMtxDynamicObjectToMovementObject(mtxMovementObj * InverseOf(mtxDynamicObject));

	std::shared_ptr<Physics::Vehicle> pVehicle = Physics::CreateVehicle(props);
	pMO->setPhysicsVehicle(pVehicle);

	Matrix44f mtxPhysicsVehicleMeters = mtxDOToPhysicsVehicleMeters * FeetToMeters(mtxDynamicObject);
	Matrix44f mtx1 = mtxPhysicsVehicleMeters;
	Matrix44f mtx2 = mtxPhysicsVehicleMeters;
	mtx1(3, 1) += 2.0f;
	mtx2(3, 1) -= 2.0f;

	// Collide with ground.
	float fRelDist;
	if (m_pUniverse->Collide(*pVehicle, mtx1, mtx2, out(fRelDist))) {
		float& fHeight = mtxPhysicsVehicleMeters(3, 1);
		fHeight = mtx1(3, 1) + (mtx2(3, 1) - mtx1(3, 1)) * fRelDist; // At collision point.
		fHeight += props.GetRestingGroundClearance();
	}
	pVehicle->SetTransformMetric(mtxPhysicsVehicleMeters);

	m_pUniverse->Add(pVehicle);

	OnVehicleEntered();
}

void ClientEngine::DisengagePhysicsVehicle() {
	CMovementObj* pMO = GetMovementObjMe();
	if (!pMO)
		return;

	CDynamicPlacementObj* pDPO = this->GetDynamicObject(pMO->getVehiclePlacementId());
	pMO->setVehiclePlacementId(0);

	if (!pMO->getPhysicsVehicle())
		return;

	if (pDPO) {
		pDPO->SuspendCollision(false);

		// Place Dynamic Object on the ground.
		Physics::Vehicle* pPhysicsVehicle = pMO->getPhysicsVehicle().get();
		if (pPhysicsVehicle) {
			Matrix44f mtxVehOld = pPhysicsVehicle->GetTransformMetric();
			Matrix44f mtxVehLeveled;
			if (!ToCoordSysPosZY(mtxVehOld.GetTranslation(), mtxVehOld.GetRowZ().GetPartPerpendicularTo(Vector3f(0, 1, 0)), Vector3f(0, 1, 0), out(mtxVehLeveled)))
				mtxVehLeveled.MakeTranslation(mtxVehOld.GetTranslation());
			Matrix44f mtxFrom = mtxVehLeveled;
			Matrix44f mtxTo = mtxFrom;
			mtxTo.GetTranslation().Y() -= 1000.0f; // Should use world bounding box to find how far down to test, but I don't think we keep track of it.
			float fRelMovement = 0;

			// Collision test of vehicle needs to skip the test against the vehicle DO. We create a filter to ignore it.
			Physics::RigidBodyStatic* pDOBody = pDPO->GetRigidBody().get();
			auto filter = [pDOBody](const Physics::Collidable* pCollidable) -> bool {
				return pCollidable != pDOBody;
			};

			Matrix44f mtxDONew;
			if (m_pUniverse->Collide(*pPhysicsVehicle, mtxFrom, mtxTo, out(fRelMovement), Physics::CollideOptions().SetFilter(filter))) {
				Matrix44f mtxVehicleNew = mtxFrom;
				mtxVehicleNew.GetTranslation().Y() = mtxFrom.GetTranslation().Y() + fRelMovement * (mtxTo.GetTranslation().Y() - mtxFrom.GetTranslation().Y());
				mtxVehicleNew.GetTranslation().Y() += pPhysicsVehicle->GetVehicleProperties().GetRestingGroundClearance();
				mtxDONew = pMO->getMtxPhysicsVehicleToDynamicObject() * MetersToFeet(mtxVehicleNew);
			} else {
				mtxDONew = mtxFrom;
			}

			// Send reposition event for client script to capture and tell server to update.
			// Sending a position update directly to the server does not work when driving vehicles that you don't own.
			const Vector3f& newPos = mtxDONew.GetTranslation();
			const Vector3f& newDir = mtxDONew.GetRowZ();
			const Vector3f& newSide = mtxDONew.GetRowX();
			IEvent* e(GetDispatcher()->MakeEvent(m_RepositionVehicleAfterExitEventId));
			(*e->OutBuffer()) << pDPO->GetPlacementId() << newPos.X() << newPos.Y() << newPos.Z() << newDir.X() << newDir.Y() << newDir.Z() << newSide.X() << newSide.Y() << newSide.Z();
			GetDispatcher()->QueueEvent(e);
		}
	}

	m_pUniverse->Remove(pMO->getPhysicsVehicle().get());
	pMO->resetPhysicsVehicle();

	// Re-orient avatar in case vehicle flipped.
	const Matrix44f& mtxOld = pMO->getBaseFrameMatrix();
	Matrix44f mtxMONew;
	if (!ToCoordSysPosZY(mtxOld.GetTranslation(), mtxOld.GetRowZ().GetPartPerpendicularTo(Vector3f(0, 1, 0)), Vector3f(0, 1, 0), out(mtxMONew)))
		mtxMONew.MakeTranslation(mtxOld.GetTranslation());

	// Don't place avatar inside of an object.
	mtxMONew.GetTranslation().Y() += 1.5f;
	pMO->setBaseFrameMatrix(mtxMONew, true);

	OnVehicleLeft();
}

void ClientEngine::UpdatePositionFromPhysicsVehicle(CMovementObj* pMO) {
	if (!pMO || !pMO->getPhysicsVehicle())
		return;

	Matrix44f mtxPhysicsVehicle = MetersToFeet(pMO->getPhysicsVehicle()->GetTransformMetric());
	Matrix44f mtxDynamicObject = pMO->getMtxPhysicsVehicleToDynamicObject() * mtxPhysicsVehicle;
	Matrix44f mtxMovementObject = pMO->getMtxDynamicObjectToMovementObject() * mtxDynamicObject;
	pMO->setBaseFrameMatrix(mtxMovementObject, true);
	CDynamicPlacementObj* pDPO = this->GetDynamicObject(pMO->getVehiclePlacementId());
	if (pDPO)
		pDPO->SetWorldMatrix(mtxDynamicObject);
}

void ClientEngine::ControlCarUsingKeyboard(eCarControl controlType, const KeyEventHandlerArgs& args) {
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;

	// Temporarily use the None type to toggle test.
	if (controlType == eCarControl::None) {
		if (!args.m_EventTypes.Test(eActionHandlerEventType::Activate))
			return;

		if (pMO->getPhysicsVehicle()) {
			DisengagePhysicsVehicle();
		} else {
			Physics::VehicleProperties vehicleProps;
			int iVehiclePlacementId = m_mouseOver.objType == ObjectType::DYNAMIC ? m_mouseOver.objId : 0;
			CDynamicPlacementObj* pDynamicPlacementObj = this->GetDynamicObject(iVehiclePlacementId);
			Quaternionf qVehicleToDO(0, 0, 0, 1);
			Vector3f vVehicleToDOMeters(0, 0, 0);
			if (pDynamicPlacementObj) {
				Matrix44f mtxOnVehicle;
				pDynamicPlacementObj->GetWorldMatrix(mtxOnVehicle);
				Vector4f min, max;
				pDynamicPlacementObj->GetBoundingBox(min, max);
				mtxOnVehicle.GetTranslation().Set(0.5f * (min.x + max.x), max.y, 0.5f * (min.z + max.z));
				mtxOnVehicle(3, 1) -= (pMO->getSkeleton()->getLocalBoundingBox().max.Y() - 1.0f);
				pMO->setBaseFrameMatrix(mtxOnVehicle, true);

				qVehicleToDO.Set(0, 1, 0, 0);
			} else {
				// Avatar could have been moved a large amount since previous frame.
				// Collision correction has not yet occurred. Use previous frame position to work around this problem.
				auto pMC = pMO->getMovementCaps();
				if (pMC)
					pMO->setBaseFramePosition(pMC->GetPosLast(), false);
			}

			EngagePhysicsVehicle(iVehiclePlacementId, vVehicleToDOMeters, qVehicleToDO, vehicleProps);
		}
	}

	bool bActivate = args.m_EventTypes.Test(eActionHandlerEventType::Activate) || args.m_EventTypes.Test(eActionHandlerEventType::Restore);
	bool bDeactivate = args.m_EventTypes.Test(eActionHandlerEventType::Deactivate) || args.m_EventTypes.Test(eActionHandlerEventType::Cancel);

	//int iKey = args.m_iChangedKey;
	//eKeyEventType keyEventType = args.m_KeyEventType;
	//LogInfo("car controlType: " << int(controlType) << " KeyCode: " << iKey << " KeyEvent: " << int(keyEventType) << ", Activate: " << bActivate << ", Deactivate: " << bDeactivate << ", Time: " << args.m_dEventTime);

	// Input that controls steering left and right is handled later together with the input from the mouse
	if (controlType == eCarControl::Left) {
		if (!bDeactivate) {
			m_xCarSteeringState.SetInput(CarSteeringState::eInput::KEYBOARD_LEFT, args.m_dEventTime);
		}
		return;
	} else if (controlType == eCarControl::Right) {
		if (!bDeactivate) {
			m_xCarSteeringState.SetInput(CarSteeringState::eInput::KEYBOARD_RIGHT, args.m_dEventTime);
		}
		return;
	}

	if (!bActivate && !bDeactivate) {
		// Only processing key state changes and ignoring continuous key presses to comply with the vehicle system.
		return;
	}

	DriveCar(bActivate, controlType, args.m_dEventTime);
}

// NOTE: This is a temporary piece of code for setting the sensitivity of the steering wheel.
const int iMAX_CAR_SENSITIVITY = 50;
const int iMIN_CAR_SENSITIVITY = 1;
const int iDEFAULT_CAR_SENSITIVITY = 45;
int ClientEngine::s_iCarSteeringSensitivity = iDEFAULT_CAR_SENSITIVITY;

// Called when local player enters a vehicle
void ClientEngine::OnVehicleEntered() {
	if (m_pInputSystem) {
		m_pInputSystem->SetCallbackForProcessingInput(std::bind(&ClientEngine::UpdateCarSteering, this));
	}

	m_bIsSteeringCarWithMouse = false;
	SteerCarStraight(0.0f);
}

// Called when local player leaves a vehicle
void ClientEngine::OnVehicleLeft() {
	if (m_pInputSystem) {
		m_pInputSystem->SetCallbackForProcessingInput(nullptr);
	}

	StopSteeringCarWithMouse(0.0f);
}

void ClientEngine::ChangeCarSensitivity(bool bIncrease) {
	if (bIncrease) {
		s_iCarSteeringSensitivity = std::min(s_iCarSteeringSensitivity + 1, iMAX_CAR_SENSITIVITY);
	} else {
		s_iCarSteeringSensitivity = std::max(s_iCarSteeringSensitivity - 1, iMIN_CAR_SENSITIVITY);
	}
	//LogInfo("Car steering sensitivity: " << s_iCarSteeringSensitivity);
}

int ClientEngine::GetCarSteeringDeadZoneRange() {
	return iMAX_CAR_SENSITIVITY - s_iCarSteeringSensitivity;
}

void ClientEngine::ControlCarUsingMouse(const KeyEventHandlerArgs& args) {
	auto pVehicle = GetMyVehicle();
	if (!pVehicle) {
		return;
	}

	const int iSteeringDeadZoneLimit = GetCarSteeringDeadZoneRange();

	bool bIsStartingSteering = args.m_EventTypes.Test(eActionHandlerEventType::Activate) || args.m_EventTypes.Test(eActionHandlerEventType::Restore);
	//bool bIsSteering = args.m_EventTypes.Test(eActionHandlerEventType::Sustain);
	bool bIsHaltingSteering = args.m_EventTypes.Test(eActionHandlerEventType::Deactivate) || args.m_EventTypes.Test(eActionHandlerEventType::Cancel);
	//LogInfo("Mask: " << args.m_EventTypes.m_Mask.to_string());
	// Begin steering
	if (bIsStartingSteering) {
		StartSteeringCarWithMouse(args.m_dEventTime);

		m_iCarSteeringPointer = 0;
	}
	// Stop steering
	else if (bIsHaltingSteering && IsSteeringCarWithMouse()) {
		StopSteeringCarWithMouse(args.m_dEventTime);
	} else if (m_bIsSteeringCarWithMouse) {
		int iDiffX = args.m_mouseMovement.x;
		m_iCarSteeringPointer += iDiffX;
		// Steer left
		if (m_iCarSteeringPointer < -iSteeringDeadZoneLimit) {
			//LogInfo("Steering LEFT " << iDiffX);
			// Adjust the steering center so that it is never too far away from the cursor
			m_iCarSteeringPointer = -iSteeringDeadZoneLimit;

			m_xCarSteeringState.SetInput(CarSteeringState::eInput::MOUSE_LEFT, args.m_dEventTime);
		}
		// Steer right
		else if (m_iCarSteeringPointer > iSteeringDeadZoneLimit) {
			//LogInfo("Steering RIGHT " << iDiffX);
			// Adjust the steering center so that it is never too far away from the cursor
			m_iCarSteeringPointer = iSteeringDeadZoneLimit;

			m_xCarSteeringState.SetInput(CarSteeringState::eInput::MOUSE_RIGHT, args.m_dEventTime);
		}
		// We are in the dead-zone for steering.
		else {
			//LogInfo("Steering STOP");
			m_xCarSteeringState.SetInput(CarSteeringState::eInput::MOUSE_STRAIGHT, args.m_dEventTime);
		}
	}
}

void ClientEngine::StartSteeringCarWithMouse(double dEventTime) {
	//LogInfo("Starting steering!!!");

	if (!m_bIsSteeringCarWithMouse) {
		// 04.12.2016, CJW - Mark's InputSystem now feeds on RawInput data.
		// Note that DirectInput and RawInput don't mix well, i.e. they don't work simultaneously.
		// Since the vehicle steering with the mouse utilises RawInput, we're forced to switch the control system here.
		EnableDirectInput(false);
		EnableRawInput(true);
		// Make sure your cursor stays still
		TrapCursor(true);
		// Hide the cursor
		SetCursorEnabled(false);

		m_bIsSteeringCarWithMouse = true;
	}
}

void ClientEngine::StopSteeringCarWithMouse(double dEventTime) {
	//LogInfo("Stopping steering!!!");

	if (m_bIsSteeringCarWithMouse) {
		// Switch back from RawInput to DirectInput
		EnableRawInput(false);
		EnableDirectInput(true);
		// Release the mosue trap, so the player can move it around freely.
		TrapCursor(false);
		// Show the cursor
		SetCursorEnabled(true);

		m_bIsSteeringCarWithMouse = false;
	}
}

bool ClientEngine::IsSteeringCarWithMouse() const {
	return m_bIsSteeringCarWithMouse;
}

// Controls car steering based on mouse and keyboard input
void ClientEngine::UpdateCarSteering() {
	if (m_xCarSteeringState.HasStateChanged()) {
		CarSteeringState::eDirection dir = m_xCarSteeringState.GetDirection();

		//LogInfo("Direction: " << (int)dir);
		// Is steering left
		if (dir == CarSteeringState::eDirection::LEFT) {
			SteerCarLeft(m_xCarSteeringState.GetTime());
		}
		// Is steering right
		else if (dir == CarSteeringState::eDirection::RIGHT) {
			SteerCarRight(m_xCarSteeringState.GetTime());
		}
		// Is steering straight
		else {
			SteerCarStraight(m_xCarSteeringState.GetTime());
		}
	}

	m_xCarSteeringState.ResetInput();
}

void ClientEngine::SteerCarLeft(double dEventTime) {
	DriveCar(false, eCarControl::Right, dEventTime);
	DriveCar(true, eCarControl::Left, dEventTime);
}

void ClientEngine::SteerCarRight(double dEventTime) {
	DriveCar(false, eCarControl::Left, dEventTime);
	DriveCar(true, eCarControl::Right, dEventTime);
}

void ClientEngine::SteerCarStraight(double dEventTime) {
	DriveCar(false, eCarControl::Left, dEventTime);
	DriveCar(false, eCarControl::Right, dEventTime);
}

void ClientEngine::DriveCar(bool bActivate, eCarControl eControlType, double dEventTime) {
	auto pVehicle = GetMyVehicle();
	if (!pVehicle) {
		return;
	}

	Physics::VehicleControlCommand cmd;
	switch (eControlType) {
		default:
		case eCarControl::None:
			return;
		case eCarControl::Forward:
			cmd.m_Type = Physics::eVehicleControlType::Forward;
			break;
		case eCarControl::Reverse:
			cmd.m_Type = Physics::eVehicleControlType::Reverse;
			break;
		case eCarControl::Left:
			cmd.m_Type = Physics::eVehicleControlType::Left;
			break;
		case eCarControl::Right:
			cmd.m_Type = Physics::eVehicleControlType::Right;
			break;
		case eCarControl::Brake:
			cmd.m_Type = Physics::eVehicleControlType::Brake;
			break;
		case eCarControl::TurboBoost:
			cmd.m_Type = Physics::eVehicleControlType::TurboBoost;
			break;
	}

	cmd.m_bActivate = bActivate;
	cmd.m_dTime = dEventTime;
	//LogInfo("Cmd: " << cmd.m_Type << " " << (cmd.m_bActivate ? "Activate" : "Deactivate") << " Time: " << std::setprecision(4) << std::fixed << cmd.m_dTime);
	pVehicle->ApplyControl(cmd);
}

void ClientEngine::LoadKeyBindings() {
	const char* pszFixedBindings =
		""
		"car_sensitivity_increase on() when(down(OEM_6))\n"
		"car_sensitivity_decrease on() when(down(OEM_4))\n"
		"car_steer on() when(down(RMOUSE))\n"
		"car_left on() when(down(a) | down(leftarrow))\n"
		"car_right on() when(down(d) | down(rightarrow))\n"
		"car_reverse on() when(down(s) | down(downarrow))\n"
		"car_forward on() when(down(w) | down(uparrow))\n"
		//"car_exit on(press(x)) when(!down(shift) & !down(control) & !down(alt))\n"
		"car_brake on() when(down(space))\n"
		"car_turboboost on(press(t)) when()\n"
		//"car_none on(press(x)) when(down(shift) & down(control))\n"
		"toggle_fullscreen on(press(enter,numpadenter)) when(down(alt))\n";
	std::istringstream istrm(pszFixedBindings);
	m_pInputSystem->AddBindingsFromStream(istrm);
}

void ClientEngine::RegisterKeyEventHandlers() {
	m_pInputSystem->RegisterHandler("car_sensitivity_increase", std::bind(&ClientEngine::ChangeCarSensitivity, this, true));
	m_pInputSystem->RegisterHandler("car_sensitivity_decrease", std::bind(&ClientEngine::ChangeCarSensitivity, this, false));
	m_pInputSystem->RegisterHandler("car_steer", std::bind(&ClientEngine::ControlCarUsingMouse, this, std::placeholders::_1));
	m_pInputSystem->RegisterHandler("car_left", std::bind(&ClientEngine::ControlCarUsingKeyboard, this, eCarControl::Left, std::placeholders::_1));
	m_pInputSystem->RegisterHandler("car_right", std::bind(&ClientEngine::ControlCarUsingKeyboard, this, eCarControl::Right, std::placeholders::_1));
	m_pInputSystem->RegisterHandler("car_forward", std::bind(&ClientEngine::ControlCarUsingKeyboard, this, eCarControl::Forward, std::placeholders::_1));
	m_pInputSystem->RegisterHandler("car_reverse", std::bind(&ClientEngine::ControlCarUsingKeyboard, this, eCarControl::Reverse, std::placeholders::_1));
	m_pInputSystem->RegisterHandler("car_brake", std::bind(&ClientEngine::ControlCarUsingKeyboard, this, eCarControl::Brake, std::placeholders::_1));
	m_pInputSystem->RegisterHandler("car_turboboost", std::bind(&ClientEngine::ControlCarUsingKeyboard, this, eCarControl::TurboBoost, std::placeholders::_1));
	m_pInputSystem->RegisterHandler("car_none", std::bind(&ClientEngine::ControlCarUsingKeyboard, this, eCarControl::None, std::placeholders::_1));

	auto ToggleFullScreen = [this](const KeyEventHandlerArgs& args) {
		if (!args.m_EventTypes.Test(eActionHandlerEventType::Activate))
			return;

		this->SetFullScreen(!this->IsFullScreen());
	};
	m_pInputSystem->RegisterHandler("toggle_fullscreen", ToggleFullScreen);
}

} // namespace KEP