///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "RuntimeSkeleton.h"
#include "Common\KEPUtil\FrameTimeProfiler.h"

namespace KEP {

static LogInstance("Instance");

void ClientEngine::LogTriggers() {
	LogInfo("...");
	m_dynObjTriggers.Log("dynObjTriggers");
	m_wldObjTriggers.Log("wldObjTriggers");
}

void ClientEngine::CleanupPendingTriggerDeletions() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "CleanupPendingTriggerDeletions");

	// Cleanup All Dynamic Object Trigger Pending Deletions
	m_dynObjTriggers.CleanupPendingDeletions();
}

CTriggerObject* ClientEngine::AddDynamicObjectTrigger(
	CDynamicPlacementObj* pDPO,
	int triggerId,
	eTriggerAction triggerAction,
	const PrimitiveGeomArgs<float>& volumeArgs,
	int intParam,
	const std::string& strParam) {
	// Assign Trigger Position and Orientation
	Vector3f triggerPos, triggerDir;
	auto pSkeleton = pDPO->getSkeleton();
	if (pSkeleton) {
		triggerPos = pSkeleton->getWorldMatrix().GetTranslation();
		triggerDir = pSkeleton->getWorldMatrix().GetRowZ();
		triggerDir.Normalize();
	} else {
		triggerPos = pDPO->m_position;
		triggerDir = pDPO->m_direction;
	}

	// Add Trigger To Dynamic Object Trigger List
	CTriggerObject* pTO = m_dynObjTriggers.AddTrigger(
		triggerId,
		triggerAction,
		intParam,
		volumeArgs,
		strParam.c_str(),
		Vector3f::Zero(),
		triggerPos,
		triggerDir,
		false,
		pDPO->GetPlacementId());

	// Update Associated Dynamic Object Triggers Map
	pDPO->m_ClientEngineState.SetTrigger(triggerId, pTO);

	//LogInfo(pTO->ToStr() << " (" << m_dynObjTriggers.GetTriggerCount() << " triggers)");

	return pTO;
}

bool ClientEngine::AddDynamicObjectTrigger(
	int objId,
	int triggerId,
	eTriggerAction triggerAction,
	const PrimitiveGeomArgs<float>& volumeArgs,
	int intParam,
	const std::string& strParam) {
	// Delete Existing Trigger For Dynamic Object
	PendingDeleteDynamicObjectTrigger(objId, triggerId);

	CDynamicPlacementObj* pDPO = GetDynamicObject(objId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - objId=" << objId);
		return false;
	}

	return AddDynamicObjectTrigger(pDPO, triggerId, triggerAction, volumeArgs, intParam, strParam) != nullptr;
}

void ClientEngine::MoveDynamicObjectTriggers(int objId, const Vector3f& origin, const Vector3f& dir) {
	ClientBaseEngine::MoveDynamicObjectTriggers(objId, origin, dir);
}

void ClientEngine::RemoveDynamicObjectTriggers(int objId) {
	PendingDeleteDynamicObjectTriggers(objId);
}

bool ClientEngine::PendingDeleteDynamicObjectTriggers(int objId) {
	CDynamicPlacementObj* pDPO = GetDynamicObject(objId);
	if (!pDPO)
		return false;

	for (const auto& it : pDPO->m_ClientEngineState.GetTriggers()) {
		auto triggerId = it.first;
		PendingDeleteDynamicObjectTrigger(objId, triggerId);
	}
	return true;
}

bool ClientEngine::PendingDeleteDynamicObjectTrigger(int objId, int triggerId) {
	CDynamicPlacementObj* pDPO = GetDynamicObject(objId);
	if (!pDPO)
		return false;

	// Get Dynamic Object Trigger
	CTriggerObject* pTO = pDPO->m_ClientEngineState.GetTrigger(triggerId);
	if (!pTO || pTO->IsPendingDelete())
		return false;

	// Exit Trigger Pre-Delete
	auto triggerState = pTO->ExitTrigger();
	if (triggerState == eTriggerState::MovedOutside) {
		auto pMO = GetMovementObjMe();
		if (pMO) {
			Vector3f pos(FLT_MAX, FLT_MAX, FLT_MAX);
			EvaluateTrigger(pTO, triggerState, pos, pMO, true);
		}
	}

	// Trigger Pending Delete
	pTO->SetPendingDelete();

	//LogInfo(pTO->ToStr());

	pDPO->m_ClientEngineState.SetTrigger(triggerId, nullptr);
	return true;
}

bool ClientEngine::EvaluateTrigger(CTriggerObject* pTO, eTriggerState triggerState, const Vector3f& pos, CMovementObj* pMO, bool async) {
	if (!pTO || !pMO)
		return false;

	bool triggered = (triggerState == eTriggerState::MovedInside || triggerState == eTriggerState::MovedOutside);

	auto triggerAction = pTO->GetTriggerAction();
	switch (triggerAction) {
		case eTriggerAction::ServerScript: {
			if (!triggered)
				return false;

			// Send Server Script Trigger Event
			IEvent* pEvent = new ScriptServerEvent(ScriptServerEvent::Trigger, pTO->GetAttachObjId(), 0, 0, pMO->getNetTraceId());
			auto posTrig = pTO->GetPosition();
			(*pEvent->OutBuffer())
				<< (int)(triggerState == eTriggerState::MovedInside ? eTriggerEvent::Enter : eTriggerEvent::Exit)
				<< pTO->GetSphereRadius()
				<< pTO->GetIntParam()
				<< pTO->GetStrParam().c_str()
				<< pos.x
				<< pos.y
				<< pos.z
				<< posTrig.x
				<< posTrig.y
				<< posTrig.z
				<< (pTO->GetAttachGlid() == 0 ? ObjTypeToInt(ObjectType::WORLD) : ObjTypeToInt(ObjectType::DYNAMIC))
				<< pTO->GetAttachObjId()
				<< pTO->GetTriggerId(); // drf - added
			pEvent->AddTo(SERVER_NETID);
			m_dispatcher.QueueEvent(pEvent);
			return true;
		}

		case eTriggerAction::ClientScript: {
			if (!pMO->isTypePC() || !triggered)
				return false;

			// Send Client Script Trigger Event
			auto posTrig = pTO->GetPosition();
			IEvent* pEvent = new TriggerEvent(
				pMO->getNetTraceId(),
				pTO->GetAttachObjId(),
				pTO->GetAttachGlid(),
				triggerState == eTriggerState::MovedInside ? eTriggerEvent::Enter : eTriggerEvent::Exit,
				0, //pTO->GetRadius(), -- radius no longer available YC 06/2016
				pTO->GetIntParam(),
				pTO->GetStrParam().c_str(),
				pos.x, pos.y, pos.z,
				posTrig.x, posTrig.y, posTrig.z,
				pTO->GetTriggerId() // drf - added
			);
			if (async)
				m_dispatcher.QueueEvent(pEvent);
			else
				m_dispatcher.ProcessSynchronousEvent(pEvent);
			return true;
		}

#if (DRF_OLD_PORTALS == 1)
		case eTriggerAction::ActivatePortal:
		case eTriggerAction::ActivateExternalPortal: {
			if (triggerState != eTriggerState::Inside && triggerState != eTriggerState::MovedInside)
				return false;

			if (triggerAction == eTriggerAction::ActivatePortal) {
				pMO->setBaseFramePosition(pTO->GetVector(), true);
				pMO->saveLastPositionFromBaseFrame();
			} else if (triggerAction == eTriggerAction::ActivateExternalPortal && pMO->getControlType() == eActorControlType::PC) {
				if (!ClientIsEnabled() || (fTime::ElapsedMs(m_triggerDelayStamp) < 1500))
					return false;
				m_triggerDelayStamp = fTime::TimeMs();

				ATTRIB_DATA attribData;
				attribData.aeType = eAttribEventType::RequestPortal;
				attribData.aeShort1 = (short)pTO->GetVector().y;
				attribData.aeShort2 = (short)pTO->GetIntParam();
				attribData.aeInt1 = (int)pTO->GetVector().x;
				attribData.aeInt2 = (int)pTO->GetVector().z;
				m_multiplayerObj->QueueMsgAttribToServer(attribData);
			}
			return true;
		}
#endif
	}

	return false;
}

} // namespace KEP