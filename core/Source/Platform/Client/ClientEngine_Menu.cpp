///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"

namespace KEP {

static LogInstance("ClientEngine");

IMenuBlade* ClientEngine::KepMenuInstance() {
	// Only Get This Once
	static IMenuBlade* s_pMB = nullptr;
	if (!s_pMB) {
		s_pMB = GetBladeFactory()->GetMenuBlade();

		// Set Test Group
		if (s_pMB) {
			s_pMB->SetTestGroup(AppTestGroup());
		}
	}

	return s_pMB;
}

bool ClientEngine::MenuOpenAs(const std::string& menuName, const std::string& menuId) {
	auto kep_menu = KepMenuInstance();
	if (!kep_menu)
		return false;
	if (kep_menu->MenuIsOpen(menuId))
		return true;
	return (kep_menu->MenuOpenAs(menuName, menuId, false, false) != NULL);
}

bool ClientEngine::MenuClose(const std::string& menuName) {
	auto kep_menu = KepMenuInstance();
	if (!kep_menu)
		return false;
	if (!kep_menu->MenuIsOpen(menuName))
		return true;
	return kep_menu->MenuClose(menuName);
}

bool ClientEngine::MenuCloseAllExcept(const std::string& menuName) {
	auto kep_menu = KepMenuInstance();
	if (!kep_menu)
		return false;
	return kep_menu->MenuCloseAllExcept(menuName);
}

bool ClientEngine::MenuExitAll() {
	auto kep_menu = KepMenuInstance();
	if (!kep_menu)
		return false;
	return kep_menu->MenuExitAll();
}

void ClientEngine::SetMenuSelectMode(BOOL mode) {
	if (m_bMenuSelectModeOn == mode)
		return;
	m_bMenuSelectModeOn = mode;
	LogInfo(LogBool(mode));
}

std::string ClientEngine::GetMenuDir() const {
	ASSERT(!PathBase().empty());
	return PathAdd(PathBase(), PathAdd(GAMEFILES, MENUSDIR));
}

std::string ClientEngine::GetMenuScriptDir() const {
	ASSERT(!PathBase().empty());
	return PathAdd(PathBase(), PathAdd(GAMEFILES, MENUSCRIPTSDIR));
}

std::string ClientEngine::GetTemplateMenuDir() {
	ASSERT(!PathBase().empty());

	if (GetGameId() == 0 || GetParentGameId() == 0) {
		// WOK
		jsAssert(false);
		LogWarn("Possible BUG - requesting template directory for WOK");
		return "";
	}

	if (m_legacyAppPatch) {
		return "";
	}

	if (m_templatePatchDir.empty()) {
		jsAssert(false);
		LogWarn("Possible BUG - template patch directory not defined.");
		return "";
	}

	return PathAdd(PathBase(), PathAdd(m_templatePatchDir, MENUSDIR)) + "\\";
}

std::string ClientEngine::GetTemplateScriptDir() {
	ASSERT(!PathBase().empty());

	if (GetGameId() == 0 || GetParentGameId() == 0) {
		// WOK
		jsAssert(false);
		LogWarn("Possible BUG - requesting template directory for WOK");
		return "";
	}

	if (m_legacyAppPatch) {
		return "";
	}

	if (m_templatePatchDir.empty()) {
		jsAssert(false);
		LogWarn("Possible BUG - template patch directory not defined.");
		return "";
	}

	return PathAdd(PathBase(), PathAdd(m_templatePatchDir, MENUSCRIPTSDIR)) + "\\";
}

std::string ClientEngine::GetAppMenuDir() {
	ASSERT(!PathBase().empty());

	if (GetGameId() == 0 || GetParentGameId() == 0) { // WOK
		return PathAdd(PathBase(), PathAdd(GAMEFILES, MENUSDIR)) + "\\";
	}

	if (m_legacyAppPatch) { // Legacy App
		std::stringstream ssLegacyPath;
		ssLegacyPath << GAMEFILES << "\\" << MENUSDIR << "-" << GetGameId();
		return PathAdd(PathBase(), ssLegacyPath.str()) + "\\";
	}

	// Bug detection - to be removed
	std::stringstream ssDefaultPath;
	ssDefaultPath << GAMEFILES << "\\Worlds\\" << GetGameId() << "\\" << MENUSDIR;
	std::string sDefaultFullPath = PathAdd(PathBase(), ssDefaultPath.str()) + "\\";

	if (m_appPatchDir.empty()) {
		jsAssert(false);
		LogWarn("App patch directory not defined");
		LogDebug("Use default directory [" << sDefaultFullPath << "]");
		return sDefaultFullPath;
	}

	// Compose app menus directory
	std::string sAppMenuDir = PathAdd(PathBase(), PathAdd(m_appPatchDir, MENUSDIR)) + "\\";

	// Bug detection - to be removed
	if (STLCompareIgnoreCase(sAppMenuDir, sDefaultFullPath) != 0) {
		LogWarn("Possible BUG: using app menus directory [" << sAppMenuDir << "] for game ID [" << GetGameId() << "]");
	}

	return sAppMenuDir;
}

std::string ClientEngine::GetAppScriptDir() {
	ASSERT(!PathBase().empty());

	if (GetGameId() == 0 || GetParentGameId() == 0) { // WOK
		return PathAdd(PathBase(), PathAdd(GAMEFILES, MENUSCRIPTSDIR)) + "\\";
	}

	if (m_legacyAppPatch) { // Legacy App
		std::stringstream ssLegacyPath;
		ssLegacyPath << GAMEFILES << "\\" << MENUSCRIPTSDIR << "-" << GetGameId();
		return PathAdd(PathBase(), ssLegacyPath.str()) + "\\";
	}

	// Bug detection - to be removed
	std::stringstream ssDefaultPath;
	ssDefaultPath << GAMEFILES << "\\Worlds\\" << GetGameId() << "\\" << MENUSCRIPTSDIR;
	std::string sDefaultFullPath = PathAdd(PathBase(), ssDefaultPath.str()) + "\\";

	if (m_appPatchDir.empty()) {
		jsAssert(false);
		LogWarn("App patch directory not defined");
		LogDebug("Use default directory [" << sDefaultFullPath << "]");
		return sDefaultFullPath;
	}

	// Compose app menuscripts directory
	std::string sAppScriptDir = PathAdd(PathBase(), PathAdd(m_appPatchDir, MENUSCRIPTSDIR)) + "\\";

	// Bug detection - to be removed
	if (STLCompareIgnoreCase(sAppScriptDir, sDefaultFullPath) != 0) {
		LogWarn("Possible BUG: using app menuscripts directory [" << sAppScriptDir << "] for game ID [" << GetGameId() << "]");
	}

	return sAppScriptDir;
}

int ClientEngine::GetNumMenus(int categoryMask) {
	return m_menuList->GetNumMenus(categoryMask);
}

std::string ClientEngine::GetMenuByIndex(int categoryMask, int index) {
	return m_menuList->GetMenuNameByIndex(categoryMask, index);
}

int ClientEngine::GetNumMenuScripts(int categoryMask) {
	return m_menuList->GetNumScripts(categoryMask);
}

std::string ClientEngine::GetMenuScriptByIndex(int categoryMask, int index) {
	return m_menuList->GetScriptNameByIndex(categoryMask, index);
}

void ClientEngine::ReloadMenus() {
	m_menuList->Load(GetGameId(), GetParentGameId() != 0, false);
}

void ClientEngine::presetMenuMessage(const std::string& menuName, const int placementId, const std::string& messageType, const std::string& data1, const std::string& data2, const std::string& identifier) {
	if (messageType.compare("ButtonClick") == 0 || messageType.compare("Close") == 0 || messageType.compare("KeyDown") == 0 || messageType.compare("KeyUp") == 0 || messageType.compare("Choice") == 0 || messageType.compare("Buy") == 0) {
		ScriptServerEvent::EventType eventType;
		std::string newData1 = data1;
		std::string newData2 = data2;

		if (messageType == "ButtonClick") {
			eventType = ScriptServerEvent::MenuButton;
		} else if (messageType == "Close") {
			eventType = ScriptServerEvent::MenuClose;
			newData1 = identifier;
			newData2 = identifier;
		} else if (messageType == "KeyDown") {
			eventType = ScriptServerEvent::MenuKeyDown;
			newData2 = identifier;
		} else if (messageType == "KeyUp") {
			eventType = ScriptServerEvent::MenuKeyUp;
			newData2 = identifier;
		} else if (messageType == "Choice") {
			eventType = ScriptServerEvent::MenuListSelect;
			newData2 = identifier;
		} else if (messageType == "Buy") {
			eventType = ScriptServerEvent::MenuShopBuy;
		} else {
			LogError("Unrecognized PresetMenu EventType");
			return;
		}

		ScriptServerEvent* se = new ScriptServerEvent(eventType, placementId);
		(*se->OutBuffer()) << menuName;
		(*se->OutBuffer()) << newData1;
		(*se->OutBuffer()) << newData2;
		(*se->OutBuffer()) << identifier;
		se->AddTo(SERVER_NETID);
		m_dispatcher.QueueEvent(se);
	}
}

} // namespace KEP