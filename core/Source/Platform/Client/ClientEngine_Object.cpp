///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "CArmedInventoryClass.h"
#include "ResourceManagers\SkeletalAnimationManager.h"
#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

bool ClientEngine::ObjectExists(const ObjectType& objType, int objId) {
	switch (objType) {
		case ObjectType::MOVEMENT:
			return (GetMovementObjByNetId(objId) != nullptr);

		case ObjectType::DYNAMIC:
		case ObjectType::SOUND:
			// TODO - Separate but script relies on them being together
			return (GetDynamicObject(objId) != nullptr) || SoundPlacementValid(objId);

		case ObjectType::WORLD:
		case ObjectType::EQUIPPABLE:
			// TODO
			return true;
	}
	return false;
}

bool ClientEngine::ObjectIsMe(const ObjectType& objType, int objId) {
	return (objType == ObjectType::MOVEMENT) && (!objId || (GetMovementObjByNetId(objId) == GetMovementObjMe()));
}

RuntimeSkeleton* ClientEngine::ObjectGetRuntimeSkeleton(const ObjectType& objType, int objId, bool posses) {
	switch (objType) {
		case ObjectType::MOVEMENT: {
			auto pMO = GetMovementObjByNetId(objId);
			if (pMO && posses && pMO->getPosses())
				pMO = pMO->getPosses();
			return pMO ? pMO->getSkeleton() : nullptr;
		}

		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(objId);
			return pDPO ? pDPO->getSkeleton() : nullptr;
		}

		case ObjectType::EQUIPPABLE: {
			auto pMO = GetMovementObjMe();
			if (!pMO)
				return nullptr;
			auto pRS = pMO->getSkeleton();
			if (!pRS)
				return nullptr;
			auto pAIO = pRS->getEquippableItemByGLID((GLID)objId);
			return pAIO ? pAIO->getSkeleton(pMO->isFirstPersonMode() ? eVisualType::FirstPerson : eVisualType::ThirdPerson) : nullptr;
		}
	}
	return nullptr;
}

CFrameObj* ClientEngine::ObjectGetBaseFrame(const ObjectType& objType, int objId, bool posses) {
	switch (objType) {
		case ObjectType::MOVEMENT: {
			auto pMO = GetMovementObjByNetId(objId);
			if (pMO && posses && pMO->getPosses())
				pMO = pMO->getPosses();
			return pMO ? pMO->getBaseFrame() : nullptr;
		}

		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(objId);
			return pDPO ? pDPO->getBaseFrame() : nullptr;
		}
	}
	return nullptr;
}

CSkeletonAnimHeader* ClientEngine::ObjectGetSkeletonAnimHeader(const ObjectType& objType, int objId, const GLID& animGlid) {
	auto pRS = ObjectGetRuntimeSkeleton(objType, objId, true); // use posses
	return pRS ? pRS->getSkeletonAnimHeaderByGlid(animGlid) : nullptr;
}

bool ClientEngine::ObjectSetPosition(const ObjectType& objType, int objId, const Vector3f& pos) {
	switch (objType) {
		case ObjectType::DYNAMIC:
			return SetDynamicObjectPosition(objId, pos);

		case ObjectType::SOUND:
			return SoundPlacementSetPosition(objId, pos);
	}
	return true;
}

bool ClientEngine::ObjectSetVisibility(const ObjectType& objType, int objId, bool visible) {
	switch (objType) {
		case ObjectType::MOVEMENT: {
			auto pMO = GetMovementObjByNetId(objId);
			return pMO ? pMO->setFirstPersonMode(!visible) : false;
		}

		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(objId);
			return pDPO ? pDPO->SetVisibility(visible ? eVisibility::VISIBLE : eVisibility::HIDDEN_FOR_ME) : false;
		}
	}
	return false;
}

// ED-8445 - Force Object Updates If Camera Attached
bool ClientEngine::ObjectSetNeedsUpdate(const ObjectType& objType, int objId, bool update) {
	switch (objType) {
		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(objId);
			return pDPO ? pDPO->SetNeedsUpdate(update) : false;
		}
	}
	return false;
}

bool ClientEngine::ObjectGetPosition(const ObjectType& objType, int objId, Vector3f& pos) {
	pos.x = pos.y = pos.z = 0;
	switch (objType) {
		case ObjectType::DYNAMIC:
			return GetDynamicObjectPosition(objId, pos);

		case ObjectType::SOUND:
			return SoundPlacementGetPosition(objId, pos);

		case ObjectType::EQUIPPABLE: {
			Matrix44f mtx;
			if (!ObjectGetWorldTransformation(ObjectType::EQUIPPABLE, objId, mtx))
				return false;
			pos = mtx.GetTranslation();
			return true;
		}
	}
	return true; // todo - should be false but script relies on it being true
}

bool ClientEngine::ObjectSetRotation(const ObjectType& objType, int objId, const Vector3f& rot) {
	switch (objType) {
		case ObjectType::DYNAMIC:
			return SetDynamicObjectRotation(objId, rot);

		case ObjectType::SOUND:
			return SoundPlacementSetRotationRad(objId, rot.y);
	}
	return true; // todo - should be false but script relies on it being true
}

bool ClientEngine::ObjectGetRotation(const ObjectType& objType, int objId, Vector3f& rot) {
	rot.x = rot.y = rot.z = 0;
	switch (objType) {
		case ObjectType::DYNAMIC:
			return GetDynamicObjectRotation(objId, rot);

		case ObjectType::SOUND:
			return SoundPlacementGetRotationRad(objId, rot.y);
	}
	return true; // todo - should be false but script relies on it being true
}

bool ClientEngine::ObjectGetSpeed(const ObjectType& objType, int objId, double& speed) const {
	speed = 0.0;
	switch (objType) {
		case ObjectType::DYNAMIC:
			speed = (objId == GetMyVehiclePlacementId()) ? GetMyVehicleSpeedKPH() : 0.0;
			break;
	}
	return true; // todo - should be false but script relies on it being true
}

bool ClientEngine::ObjectGetAccel(const ObjectType& objType, int objId, double& accel) {
	// Get Object Accel Based On Speed Difference
	double speed = 0.0;
	ObjectGetSpeed(objType, objId, speed);
	auto it = m_speedWas.find(objId);
	double speedWas = (it == m_speedWas.end()) ? 0.0 : it->second;
	accel = speed - speedWas;
	m_speedWas[objId] = speed;

	return true;
}

bool ClientEngine::ObjectGetThrottle(const ObjectType& objType, int objId, double& throttle) {
	throttle = 0.0;
	switch (objType) {
		case ObjectType::DYNAMIC:
			throttle = (objId == GetMyVehiclePlacementId()) ? GetMyVehicleThrottle() : 0.0;
			break;
	}
	return true;
}

bool ClientEngine::ObjectGetSkid(const ObjectType& objType, int objId, double& skid) const {
	skid = 0.0;
	switch (objType) {
		case ObjectType::DYNAMIC:
			skid = (objId == GetMyVehiclePlacementId()) ? GetMyVehicleSkid() : 0.0;
			break;
	}
	return true;
}

bool ClientEngine::ObjectIsInteractable(const ObjectType& objType, int objId) {
	switch (objType) {
		case ObjectType::MOVEMENT:
		case ObjectType::DYNAMIC:
		case ObjectType::SOUND:
			return true;

		case ObjectType::WORLD: {
			MutexGuardLock lock(m_WldObjListMutex);
			auto pWO = GetWorldObject(objId);
			return (pWO ? pWO->IsInteractable() : false);
		}
	}
	return false;
}

void ClientEngine::ObjectSnapSelection(float tolerance) {
	// For All Selected Objects To Snap From Find Nearest Object To Snap To
	Vector3f snapVector(0, 0, 0);
	float snapLenSq = tolerance * tolerance;
	for (const auto& selectionEntry : m_selectionList) {
		// Is Object Snappable ?
		ObjectType typeFrom = selectionEntry.objType;
		int idFrom = selectionEntry.objId;
		if (!ObjectIsSnappable(typeFrom, idFrom))
			continue;

		// Get Next Selected Object To Snap From
		auto pDPO_from = GetDynamicObject(idFrom);
		if (!pDPO_from)
			continue;

		// Find Nearest Object To Snap To
		IterateObjects([this, &snapVector, &snapLenSq, pDPO_from, tolerance](CDynamicPlacementObj* pDPO_to) -> bool {
			if (!pDPO_to)
				return false; // end iteration

			// Is Object Snappable ?
			ObjectType typeTo = ObjectType::DYNAMIC;
			int idTo = pDPO_to->GetPlacementId();
			if (!ObjectIsSnappable(typeTo, idTo))
				return true; // next

			// Snap Only To Non-Selected Objects
			if (ObjectIsSelected(typeTo, idTo))
				return true; // next

			// Get Snap Vector (if close enough to possibly snap)
			Vector3f displacement = pDPO_to->m_boundSphereCenter - pDPO_from->m_boundSphereCenter;
			float lenSq = displacement.LengthSquared();
			float maxDist = pDPO_from->m_boundRadius + pDPO_to->m_boundRadius + tolerance;
			if (lenSq <= maxDist * maxDist)
				GetSnapVector(snapVector, snapLenSq, pDPO_from, pDPO_to);

			return true; // next
		});
	}

	// Snap Vector Found ?
	if (snapVector == Vector3f(0, 0, 0))
		return;

	// Apply Snap Vector To All Selected Objects
	for (const auto& selectionEntry : m_selectionList) {
		// Is Object Snappable ?
		ObjectType typeFrom = selectionEntry.objType;
		int idFrom = selectionEntry.objId;
		if (!ObjectIsSnappable(typeFrom, idFrom))
			continue;

		// Get Next Selected Object To Snap From
		auto pDPO_from = GetDynamicObject(idFrom);
		if (!pDPO_from)
			continue;

		// Apply Snap Vector To Object
		pDPO_from->SetPosition(pDPO_from->m_position + snapVector);
	}
}

/** DRF - BUG FIX
* Returns if the given object type and id are valid for snapping.
*  - Only Dynamic Objects (don't snap placed objects to avatars, sounds, entities, equippables, etc.)
*  - Only Positive PlacementId (don't snap smart chairs to their scripted bouncing diamonds)
*/
bool ClientEngine::ObjectIsSnappable(const ObjectType& objType, int objId) const {
	return (objType == ObjectType::DYNAMIC) && (objId > 0);
}

void ClientEngine::ObjectSetOutlineState(const ObjectType& objType, int objId, bool outline) {
	switch (objType) {
		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(objId);
			if (pDPO)
				pDPO->SetOutlineState(outline);
			return;
		}

		case ObjectType::MOVEMENT: {
			auto pMO = GetMovementObjByNetId(objId);
			if (pMO)
				pMO->setOutlineState(outline);
			return;
		}

		case ObjectType::WORLD: {
			MutexGuardLock lock(m_WldObjListMutex);
			auto pWO = GetWorldObject(objId);
			if (pWO)
				pWO->SetOutlineState(outline);
			return;
		}

		case ObjectType::SOUND: {
			if (this->m_renderSoundPlacements) {
				auto pSP = SoundPlacementGet(objId);
				if (pSP)
					pSP->SetOutlineState(outline);
			}
			return;
		}
	}
}

void ClientEngine::ObjectSetOutlineColor(const ObjectType& objType, int objId, const D3DXCOLOR& color) {
	switch (objType) {
		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(objId);
			if (pDPO)
				pDPO->SetOutlineColor(color);
			return;
		}

		case ObjectType::MOVEMENT: {
			auto pMO = GetMovementObjByNetId(objId);
			if (pMO)
				pMO->setOutlineColor(color);
			return;
		}

		case ObjectType::WORLD: {
			MutexGuardLock lock(m_WldObjListMutex);
			auto pWO = GetWorldObject(objId);
			if (pWO)
				pWO->SetOutlineColor(color);
			return;
		}

		case ObjectType::SOUND: {
			for (const auto& it : m_soundPlacements) {
				auto pSP = it.second;
				if (!pSP)
					continue;
				auto soundPlacementId = it.first;
				if (soundPlacementId == objId)
					pSP->SetOutlineColor(color);
			}
			return;
		}
	}
}

void ClientEngine::ObjectSetOutlineZSorted(const ObjectType& objType, int objId, bool sort) {
	switch (objType) {
		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(objId);
			if (pDPO)
				pDPO->SetOutlineZSorted(sort);
		} break;

		case ObjectType::MOVEMENT: {
			auto pMO = GetMovementObjByNetId(objId);
			if (pMO)
				pMO->setOutlineZSorted(sort);
		} break;

		case ObjectType::WORLD: {
			MutexGuardLock lock(m_WldObjListMutex);
			auto pWO = GetWorldObject(objId);
			if (pWO)
				pWO->SetOutlineZSorted(sort);
		} break;

		case ObjectType::SOUND: {
			for (const auto& it : m_soundPlacements) {
				auto pSP = it.second;
				if (!pSP)
					continue;
				auto soundPlacementId = it.first;
				if (soundPlacementId == objId)
					pSP->SetOutlineZSorted(sort);
			}
		} break;
	}
}

void ClientEngine::ObjectSetRolloverMode(const ObjectType& objType, bool show) {
	if (show)
		m_rolloverMode |= (int)objType;
	else
		m_rolloverMode &= ~(int)objType;
}

bool ClientEngine::ObjectGetWorldTransformation(ObjectType objType, int objId, Matrix44f& matrix) {
	switch (objType) {
		case ObjectType::WORLD: {
			MutexGuardLock lock(m_WldObjListMutex);
			if (!m_pWOL)
				return false;
			POSITION pos = m_pWOL->FindIndex(objId);
			if (pos) {
				auto pWO = static_cast<CWldObject*>(m_pWOL->GetAt(pos)); // use static cast to speed up type casting (might be called deep inside render loop)
				if (pWO) {
					matrix = pWO->GetWorldTransform();
					return true;
				}
			}
			return false;
		}

		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(objId);
			if (!pDPO)
				return false;
			auto pRS = pDPO->getSkeleton();
			if (!pRS)
				return false;
			matrix = pRS->getWorldMatrix();
			return true;
		}

		case ObjectType::MOVEMENT: {
			CMovementObj* pMO;
			if (objId == 0)
				pMO = GetMovementObjMe();
			else
				pMO = GetMovementObjByNetId(objId);

			if (pMO && pMO->getPosses() && pMO->getMountType() == MT_STUNT)
				pMO = pMO->getPosses();

			if (pMO && pMO->getBaseFrame()) {
				matrix = pMO->getBaseFrameMatrix();
				return true;
			}
			return false;
		}

		case ObjectType::EQUIPPABLE: {
			auto pMO = GetMovementObjMe();
			if (!pMO)
				return false;

			if (pMO->getPosses() && (pMO->getMountType() == MT_STUNT))
				pMO = pMO->getPosses();

			auto pRS = pMO->getSkeleton();
			if (!pRS)
				return false;

			auto pAIO = pRS->getEquippableItemByGLID(objId);
			if (!pAIO)
				return false;

			Matrix44f boneMatrix;
			if (!ObjectGetWorldTransformation(ObjectType::BONE, pAIO->getAttachedBoneIndex(), boneMatrix))
				return false;

			Matrix44f finalMatrix;
			const Matrix44f* relMatrix = pAIO->getRelativeMatrix();
			if (relMatrix)
				finalMatrix = *relMatrix * boneMatrix;
			else
				finalMatrix = boneMatrix;

			matrix = finalMatrix;
			return true;
		}

		case ObjectType::BONE: {
			auto pMO = GetMovementObjMe();
			if (!pMO)
				return false;

			if (pMO->getPosses() && (pMO->getMountType() == MT_STUNT))
				pMO = pMO->getPosses();

			auto pRS = pMO->getSkeleton();
			if (!pRS)
				return false;

			if (!pMO->getBaseFrame() || !pRS->isAnimBoneMatricesInited())
				return false;

			Matrix44f positionMatrix = pMO->getBaseFrameMatrix();

			// DRF - ED-2815 - Accessory Import Bounding Box & Widget Offset For 'My Avatar'
			// Following code was scaling incorrectly...
			positionMatrix[0].SubVector<3>(0) *= pMO->getSkeleton()->getScaleVector().x;
			positionMatrix[1].SubVector<3>(0) *= pMO->getSkeleton()->getScaleVector().y;
			positionMatrix[2].SubVector<3>(0) *= pMO->getSkeleton()->getScaleVector().z;

			Matrix44f boneMatrix;
			int boneIndex = objId;
			if (boneIndex >= 0 && boneIndex < pRS->getBoneCount())
				boneMatrix = pRS->getAnimBoneMatrix(boneIndex) * positionMatrix;
			else
				boneMatrix = positionMatrix;

			matrix = boneMatrix;
			return true;
		}

		case ObjectType::CAMERA: {
			Matrix44f m;
			GetViewMatrix(&m);
			matrix = m;
			return true;
		}
	}
	return false;
}

bool ClientEngine::ObjectGetBoneRelativeTransformation(ObjectType objType, int objId, const std::string& boneName, Matrix44f& matrix) {
	RuntimeSkeleton* pRS = nullptr;
	switch (objType) {
		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(objId);
			if (pDPO)
				pRS = pDPO->getSkeleton();
		} break;

		case ObjectType::MOVEMENT: {
			CMovementObj* pMO;
			if (objId == 0)
				pMO = GetMovementObjMe();
			else
				pMO = GetMovementObjByNetId(objId);

			if (pMO && pMO->getPosses() && pMO->getMountType() == MT_STUNT)
				pMO = pMO->getPosses();

			if (pMO)
				pRS = pMO->getSkeleton();
		} break;

		case ObjectType::EQUIPPABLE: {
			auto pMO = GetMovementObjMe();
			if (!pMO)
				return false;

			if (pMO->getPosses() && pMO->getMountType() == MT_STUNT)
				pMO = pMO->getPosses();

			pRS = pMO->getSkeleton();
			if (!pRS)
				return false;

			// compare by GLID for now until we have a APD instance ID
			auto pAIO = pRS->getEquippableItemByGLID(objId);
			if (pAIO) {
				pRS = pAIO->getSkeleton(eVisualType::ThirdPerson);
				if (pRS == nullptr)
					pRS = pAIO->getSkeleton(eVisualType::FirstPerson);
			}
		} break;
	}

	if (pRS && pRS->isAnimBoneMatricesInited()) {
		Matrix44f scaleMatrix;
		scaleMatrix.MakeScale(pRS->getScaleVector());

		Matrix44f boneMatrix;
		boneMatrix.MakeIdentity();

		int boneIndex = objId;
		if (boneIndex >= 0 && boneIndex < pRS->getBoneCount())
			boneMatrix = pRS->getAnimBoneMatrix(boneIndex) * scaleMatrix;

		matrix = boneMatrix;
		return true;
	}

	return false;
}

bool ClientEngine::ObjectBakeAnimation(const ObjectType& objType, int objId, const GLID& animGlid) {
	auto pAP = AnimationRM::Instance()->GetAnimationProxy(animGlid);
	if (!pAP || !pAP->GetAnimDataPtr())
		return false;

	auto pSAH = ObjectGetSkeletonAnimHeader(objType, objId, animGlid);
	if (!pSAH)
		return false;

	pAP->BakeEditingAttributes(pSAH->GetAnimSpeedFactor(), pSAH->GetAnimCropStartTimeMs(), pSAH->GetAnimCropEndTimeMs(), pSAH->GetAnimLooping());
	pSAH->SetAnimSpeedFactor(1.0f);
	pSAH->SetAnimCropTimes(0.0, -1.0);
	pSAH->m_animDurationMs = pAP->GetAnimDurationMs();

	return true;
}

bool ClientEngine::ObjectSetAnimSpeedFactor(const ObjectType& objType, int objId, const GLID& animGlid, float animSpeedFactor) {
	auto pSAH = ObjectGetSkeletonAnimHeader(objType, objId, animGlid);
	if (!pSAH)
		return false;
	return pSAH->SetAnimSpeedFactor(animSpeedFactor);
}

bool ClientEngine::ObjectSetAnimLooping(const ObjectType& objType, int objId, const GLID& animGlid, bool animLooping) {
	auto pSAH = ObjectGetSkeletonAnimHeader(objType, objId, animGlid);
	if (!pSAH)
		return false;
	return pSAH->SetAnimLooping(animLooping);
}

bool ClientEngine::ObjectSetAnimCropTimes(const ObjectType& objType, int objId, const GLID& animGlid, TimeMs animCropStartTimeMs, TimeMs animCropEndTimeMs) {
	auto pSAH = ObjectGetSkeletonAnimHeader(objType, objId, animGlid);
	if (!pSAH)
		return false;
	return pSAH->SetAnimCropTimes(animCropStartTimeMs, animCropEndTimeMs);
}

bool ClientEngine::ObjectGetAnimInfo(const ObjectType& objType, int objId, const GLID& animGlid, TimeMs& animDurationMs, TimeMs& animMinTimeMs, bool& animLooping, float& animSpeedScale, TimeMs& animCropStartTimeMs, TimeMs& animCropEndTimeMs) {
	animDurationMs = 0.0;
	animMinTimeMs = 0.0;
	animLooping = false;
	animSpeedScale = 1.0;
	animCropStartTimeMs = 0.0;
	animCropEndTimeMs = -1.0;

	auto pSAH = ObjectGetSkeletonAnimHeader(objType, objId, animGlid);
	if (!pSAH)
		return false;

	animDurationMs = pSAH->GetAnimDurationMs();
	animMinTimeMs = pSAH->GetAnimMinTimeMs();
	animLooping = (pSAH->GetAnimLooping() == TRUE);
	animSpeedScale = pSAH->GetAnimSpeedFactor();
	animCropStartTimeMs = pSAH->GetAnimCropStartTimeMs();
	animCropEndTimeMs = pSAH->GetAnimCropEndTimeMs();
	return true;
}

bool ClientEngine::ObjectAddParticleAsync(
	const ObjectType& objType,
	int objId,
	const std::string& boneName,
	int particleSlot,
	const GLID& particleGlid,
	const Vector3f& offset,
	const Vector3f& dir,
	const Vector3f& up) {
	std::function<void()> fnCallback = [this, objType, objId, boneName, particleSlot, particleGlid, offset, dir, up]() {
		this->ObjectAddParticleSync(objType, objId, boneName, particleSlot, particleGlid, offset, dir, up);
	};
	ContentMetadataCacheAsync(std::vector<GLID>({ particleGlid }), fnCallback, this);
	return true;
}

// BLOCKING - Better to call ObjectAddParticleAsync() which caches metadata asynchronously first!
bool ClientEngine::ObjectAddParticleSync(
	const ObjectType& objType,
	int objId,
	const std::string& boneName,
	int particleSlot,
	const GLID& particleGlid,
	const Vector3f& offset,
	const Vector3f& dir,
	const Vector3f& up) {
	// Should Already Be Cached If Calling ObjectAddParticleAsync()
	if (IS_UGC_GLID(particleGlid) && !ContentService::Instance()->ContentMetadataIsCached(particleGlid))
		ContentMetadataCacheSync(std::vector<GLID>({ particleGlid }));

	switch (objType) {
		case ObjectType::MOVEMENT: {
			auto pRS = ObjectGetRuntimeSkeleton(objType, objId);
			return pRS ? pRS->addParticleByGlid(boneName, particleSlot, particleGlid, offset, dir, up) : false;
		}

		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(objId);
			return pDPO ? pDPO->AddParticle(boneName, particleSlot, particleGlid, "", offset, dir, up) : false;
		}
	}
	return false;
}

bool ClientEngine::ObjectRemoveParticle(const ObjectType& objType, int objId, const std::string& boneName, int particleSlot) {
	auto pRS = ObjectGetRuntimeSkeleton(objType, objId);
	return pRS ? pRS->removeParticle(boneName, particleSlot) : false;
}

bool ClientEngine::ObjectArmEquippableItemAsync(
	const ObjectType& objType,
	int objId,
	const GLID& glid,
	int material,
	const boost::optional<std::string>& strBoneOverride) {
	std::function<void()> fnCallback = [this, objType, objId, glid, material, strBoneOverride]() {
		this->ObjectArmEquippableItemSync(objType, objId, glid, material, strBoneOverride);
	};
	ContentMetadataCacheAsync(std::vector<GLID>({ glid }), fnCallback, this);
	return true;
}

// BLOCKING - Better to call ObjectArmEquippableAsync() which caches metadata asynchronously first!
bool ClientEngine::ObjectArmEquippableItemSync(
	const ObjectType& objType,
	int objId,
	const GLID& glid,
	int material,
	const boost::optional<std::string>& strBoneOverride) {
	// Should Already Be Cached If Calling ObjectArmEquippableItemAsync()
	if (IS_UGC_GLID(glid) && !ContentService::Instance()->ContentMetadataIsCached(glid))
		ContentMetadataCacheSync(std::vector<GLID>({ glid }));

	switch (objType) {
		case ObjectType::MOVEMENT: {
			auto pMO = GetMovementObjByNetId(objId);
			return pMO ? ArmEquippableItem_MO(glid, pMO, material) : false;
		}

		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(objId);
			return pDPO ? ArmEquippableItem_DPO(glid, pDPO, strBoneOverride) : false;
		}
	}
	return false;
}

bool ClientEngine::ObjectDisarmEquippableItem(
	const ObjectType& objType,
	int objId,
	const GLID& glid) {
	switch (objType) {
		case ObjectType::MOVEMENT: {
			auto pMO = GetMovementObjByNetId(objId);
			return pMO ? DisarmEquippableItem_MO(glid, pMO) : false;
		}

		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(objId);
			return pDPO ? DisarmEquippableItem_DPO(glid, pDPO) : false;
		}
	}
	return false;
}

} // namespace KEP