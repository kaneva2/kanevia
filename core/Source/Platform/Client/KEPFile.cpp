///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "KEPFile.h"
#include "clientStrings.h"

#include "..\KEPUtil\RefPtr.h"

#include "LogHelper.h"

namespace KEP {

KEPFile::KEPFile() :
		m_file(0) {
}

KEPFile::~KEPFile() {
	// DRF - Invalidate Reference Pointer (as KEPFile*)
	REF_PTR_DEL(this);

	if (m_file != 0)
		fclose(m_file);
}

bool KEPFile::Open(const char* fname, const char* access, std::string& errMsg) {
	errMsg.clear();
	if (m_file != 0) {
		fclose(m_file);
		m_file = 0;
	}
	m_file = 0;
	if (fopen_s(&m_file, fname, access) != 0 || m_file == 0) {
		std::string errMsgWin;
		errorNumToString(HRESULT_FROM_WIN32(errno), errMsgWin);
		StrBuild(errMsg, fname << " - " << errMsgWin);
		return false;
	}
	return true;
}

bool KEPFile::ReadLine(std::string& line, std::string& errMsg) {
	bool ret = true;
	errMsg.empty();
	line.empty();

	if (m_file != 0) {
		if (fgets(m_buffer, KEPFILE_BUFFSIZE, m_file) == 0) {
			if (!feof(m_file))
				errorNumToString(HRESULT_FROM_WIN32(ferror(m_file)), errMsg);
			ret = false;
		} else {
			size_t len = ::strlen(m_buffer);
			while (len > 0 && (m_buffer[len - 1] == '\r' || m_buffer[len - 1] == '\n')) {
				// strip crlf
				m_buffer[len - 1] = '\0';
				len--;
			}
			line = m_buffer;
		}
	} else {
		errMsg = loadStr(IDS_FILE_NOT_OPEN);
		ret = false;
	}
	return ret;
}

bool KEPFile::WriteLine(const char* line, std::string& errMsg) {
	bool ret = true;
	errMsg.empty();

	if (line == 0 || *line == 0)
		return ret;

	if (m_file != 0) {
		size_t len = ::strlen(line);
		size_t retLen = fwrite(line, 1, len, m_file);
		if (retLen != len) {
			errorNumToString(HRESULT_FROM_WIN32(ferror(m_file)), errMsg);
			ret = false;
		}
	} else {
		errMsg = loadStr(IDS_FILE_NOT_OPEN);
		ret = false;
	}
	return ret;
}

void KEPFile::CloseAndDelete() {
	if (m_file != 0) {
		fclose(m_file);
		m_file = 0;
	}
	delete this;
}

} // namespace KEP
