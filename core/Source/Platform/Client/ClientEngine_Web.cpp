///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "client\WebCallTask.h"
#include "Common\include\UnicodeWindowsHelpers.h"

namespace KEP {

static LogInstance("ClientEngine");

void ClientEngine::LogWebCalls() const {
	WebCallerLogInstances();
}

bool ClientEngine::WebCallDisabled(const std::string& id) {
	return false // true to disable all web calls
		   || (id == "GoogleAnalytics") || (id == "ProgressMetrics");
}

bool ClientEngine::WebCall(const std::string& url, const std::string& id) {
	// Web Calls Disabled ?
	if (WebCallDisabled(id))
		return true;

	// Do Web Call (not blocking + no response + no verbose)
	WebCallOpt wco;
	wco.url = url;
	wco.response = false;

	// Get Identified Web Caller (or create if not found)
	auto pWC = WebCaller::GetInstance(id, true, 1, THREAD_PRIORITY_BELOW_NORMAL);
	if (!pWC)
		return false;

	// Make Web Call, Don't Wait For Complete & Return WebCallAct
	return pWC->Call(wco) != nullptr;
}

bool ClientEngine::WebCallAndWaitOk(const std::string& url, const std::string& id) {
	// Web Calls Disabled ?
	if (WebCallDisabled(id))
		return true;

	// Do Web Call (blocking + yes response)
	WebCallOpt wco;
	wco.url = url;
	wco.response = true;
	return WebCaller::WebCallAndWaitOk(id, wco);
}

bool ClientEngine::WebGetPlayerCfg(const std::string& username, Vector3d& nameColor, int& passGroupsUser, int& passGroupsWorld) {
	// Valid Username ?
	if (username.empty())
		return false;

	// Build Player Configuration Url
	std::string url;
	StrBuild(url, AppWokUrl() << "getPlayerInfo.aspx?action=entityCfg&username=" << UrlEncode(username, false));

	// Perform Blocking Webcall
	std::string resultText;
	DWORD httpStatusCode;
	std::string httpStatusDescription;
	if (!_GetBrowserPage(url, 0, 0, resultText, &httpStatusCode, httpStatusDescription) || resultText.empty())
		return false;

	// Parse Response Xml
	nameColor[0] = XmlGetFirstChildTextAsInt(resultText, "NameColorR", nameColor[0]);
	nameColor[1] = XmlGetFirstChildTextAsInt(resultText, "NameColorG", nameColor[1]);
	nameColor[2] = XmlGetFirstChildTextAsInt(resultText, "NameColorB", nameColor[2]);
	passGroupsUser = XmlGetFirstChildTextAsInt(resultText, "PassGroupsUser", passGroupsUser);
	passGroupsWorld = XmlGetFirstChildTextAsInt(resultText, "PassGroupsWorld", passGroupsWorld);

	return true;
}

bool ClientEngine::LaunchBrowser(const char* url) {
	// Valid Url ?
	if ((strncmp(url, "http://", 7) != 0) && (strncmp(url, "https://", 8) != 0) && (strncmp(url, "mailto:", 7) != 0)) {
		LogError("Invalid Url - '" << url << "'");
		return false;
	}

	// Shell Execute 'open <url>' (url right click menu behavior)
	LogInfo("'" << url << "'");
	HINSTANCE rv = ShellExecute(AfxGetMainWnd()->m_hWnd, L"open", Utf8ToUtf16(url).c_str(), nullptr, L"", SW_SHOWNORMAL);
	if ((int)rv <= 32) {
		LogError("ShellExecute() FAILED - rv=" << rv);
		return false;
	}

	return true;
}

void ClientEngine::LoadChildGameWebCallValues() {
	if (m_childGameWebCallValuesLoaded)
		return;

	// make the web call to get the black list
	IEvent* completionEvent = m_dispatcher.MakeEvent(m_childGameWebCallsReadyEventId, 0);
	if (completionEvent) {
		// If there is an error getting the list, m_childGameWebCallValuesLoaded will remain false
		// and web calls will not be allowed in child game menus
		GetBrowserPageAsync(m_childGameWebCallUrl, completionEvent);
	} else {
		LogError("Error creating child game web calls ready event");
	}
}

bool ClientEngine::ChildGameWebCallAllowed(const std::string& url) {
	if (!m_childGameWebCallValuesLoaded)
		return false;

	// if the url contains a string within the m_childGameWebCalls then return false
	for (std::vector<std::string>::iterator i = m_childGameWebCallValues.begin(); i != m_childGameWebCallValues.end(); i++) {
		if (url.find((*i)) != std::string::npos) {
			// string found in URL, web call not allowed
			return false;
		}
	}

	// no unacceptable strings found in URL, allow web call
	return true;
}

bool ClientEngine::GetBrowserPageAsync(const std::string& url, IEvent* completionEvent, int filter, int startIndex, int maxItems, int retries, bool doAuth /*=false*/) {
	// Valid Url?
	if (url.empty())
		return false;

	// Queue Web Call Task
	auto pTask = new WebCallTask(m_dispatcher);
	pTask->m_url = url;
	pTask->m_pEventCompletion = completionEvent;
	pTask->m_startIndex = startIndex;
	pTask->m_maxItems = maxItems;
	pTask->m_retries = retries;
	pTask->m_eventFilter = filter;
	pTask->m_browserPageReadyEventId = m_browserPageReadyEventId;
	if (doAuth)
		pTask->m_urlAuth = m_authUrl;
	if (!fillInAuthUrl(pTask->m_urlAuth)) {
		LogError("fillInAuthUrl() FAILED");
	}
	m_taskSystem_WebCall.QueueTask(pTask);

	return true;
}

bool ClientEngine::GetBrowserPage(const std::string& url, int filter, int startIndex, int maxItems) {
	// Valid Url?
	if (url.empty())
		return false;

	std::string resultText;
	std::string httpStatusDescription;
	DWORD httpStatusCode;

	bool ret = _GetBrowserPage(url, startIndex, maxItems, resultText, &httpStatusCode, httpStatusDescription);

	if (!ret) {
		// resultText is possibly available here with the bad page but
		// we'll overwrite it to give only error xml to any event clients

		char buf[128];

		resultText.assign("<Result>\r\n  <ReturnCode>");
		_itoa_s(httpStatusCode, buf, _countof(buf), 10);
		resultText.append(buf);
		resultText.append("</ReturnCode>\r\n");
		resultText.append("  <ReturnDescription>");
		resultText.append(httpStatusDescription.c_str());
		resultText.append("</ReturnDescription>\r\n");
		resultText.append("</Result>");
	}

	// Send GetBrowserPageReadyEvent (mostly used by script::GetBrowserPageReadyHandler())
	IEvent* e = GetDispatcher()->MakeEvent(m_browserPageReadyEventId, 0);
	if (e) {
		e->SetFilter(filter);
		(*e->OutBuffer()) << resultText.c_str() << httpStatusCode << url << startIndex << maxItems;
		GetDispatcher()->QueueEvent(e);
	}

	return ret;
}

bool ClientEngine::_GetBrowserPage(const std::string& url, int startIndex, int maxItems, std::string& resultText, DWORD* httpStatusCode, std::string& httpStatusDescription) {
	// DRF - Only Process Message Loop From Main Thread (threaded by ContentServiceGLIDCacheTask)
	bool isMainThread = (GetCurrentThreadId() == m_mainThreadId);

	const size_t retries = 3;

	// Clear Return Values
	*httpStatusCode = 0;
	httpStatusDescription.clear();
	resultText.clear();

	// Set Web Call Options (blocking + response)
	WebCallOpt wco;
	wco.url = url;
	wco.response = true;

	// Add Extra Url Parameters
	if (startIndex > 0)
		wco.AddParam("start", startIndex);
	if (maxItems > 0)
		wco.AddParam("max", maxItems);

	// Get Web Caller Id From WebCallTask
	std::string wcId = WebCallTask::GetWebCallerId();
	if (wcId.empty())
		return false;

	// Retry Web Call On Failure
	for (size_t i = 0; i < retries; ++i) {
		// Non-Blocking Web Call
		auto pWCA = WebCaller::WebCall(wcId, wco);
		if (!pWCA)
			continue;

		// Legacy Timeout & Windows Message Loop
		FOREVER {
			// Web Call Complete Or Timeout ?
			if (pWCA->IsComplete() || pWCA->IsTimeout())
				break;

			// DRF - Only Process Message Loop From Main Thread
			if (isMainThread) {
				// Pump Messages Until Empty (1 messages max)
				if (!::MsgProcPumpUntilEmpty(1)) {
					// Quit Client
					::PostQuitMessage(0);
					return false;
				}
			}

			// Allow Thread To Pre-empt
			::Sleep(1);
		}

		// Web Call Complete ?
		if (pWCA->IsComplete()) {
			// Get Return Values
			DWORD status = pWCA->StatusCode();
			*httpStatusCode = status;
			httpStatusDescription = pWCA->StatusStr();

			// Success ?
			if (status == HTTP_STATUS_OK) {
				// Success - Get Result Text & Return OK
				resultText = pWCA->ResponseStr();
				return true;

			} else if (status == HTTP_STATUS_KANEVA_AUTH) {
				// Kaneva Authentication Error - Validate User & Retry
				LogError("[409] KANEVA_AUTH '" << url << "'");
				WebValidateUser();
			}
		}
	}

	return false;
}

// Take the user's email address to a Kaneva page to gather the user's username, default server or MMI3D data. Does not require
// login. Login will fail at the game server if login data is invalid.
bool ClientEngine::_GetBrowserLogin(const std::string& url, std::string& resultText, DWORD* httpStatusCode, std::string& httpStatusDescription, const std::string& gotoUrlOverride) {
	// Valid Url?
	if (url.empty())
		return false;

	// Reset Return Values
	*httpStatusCode = 0;
	httpStatusDescription.clear();
	resultText.clear();

	// Set Web Call Options (response)
	WebCallOpt wco;
	wco.url = url;
	wco.response = true;

	// Add Extra Url Parameters
	if (!gotoUrlOverride.empty()) {
		wco.AddParam("goto", "u");
		wco.AddParam("id", gotoUrlOverride);
	} else if (!m_gotochannelurlonstartup.empty()) {
		wco.AddParam("goto", "u");
		wco.AddParam("id", m_gotochannelurlonstartup);
	} else if (m_gotoplayeronstartup) {
		wco.AddParam("goto", "p");
		wco.AddParam("id", m_gotoonstartupId);
	} else if (m_gotoapartmentonstartup) {
		wco.AddParam("goto", "a");
		wco.AddParam("id", m_gotoonstartupId);
	} else if (m_gotochannelonstartup) {
		wco.AddParam("goto", "c");
		wco.AddParam("id", m_gotoonstartupId);
	}

	// Get Web Caller Id From WebCallTask
	std::string wcId = WebCallTask::GetWebCallerId();
	if (wcId.empty())
		return false;

	// Blocking Web Call
	auto pWCA = WebCaller::WebCallAndWait(wcId, wco);
	if (!pWCA)
		return false;

	// Get Return Values
	DWORD status = pWCA->StatusCode();
	*httpStatusCode = status;
	httpStatusDescription = pWCA->StatusStr();

	// Success ?
	if (status == HTTP_STATUS_OK) {
		// Success - Get Result Text & Return OK
		resultText = pWCA->ResponseStr();
		return true;
	}

	return false;
}

} // namespace KEP