///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "LuaHelper.h"
#include "Event/Base/EventHeader.h"
#include "Event/LuaEventHandler/LuaScriptVM.h"
#include <KEPHelpers.h>

int my_dofile(lua_State* L, int nresults, const char* name) {
	// load the script
	int rc = luaL_loadfile(L, name);
	if (rc)
		return rc; // there was a compile-time error. it's on the top of the stack

	// there were no compilation errors. run the script
	// if there is a runtime error it is on the top of the stack
	return lua_pcall(L, 0, nresults, 0);
}

KEP::IScriptVM* scriptTest() {
	string baseDir = "c:\\code\\kep\\deploy\\gamecontentdbg\\scripts\\KEPLuaTest.lua";

	lua_State* state = lua_open();

	lua_baselibopen(state);
	lua_iolibopen(state);
	lua_strlibopen(state);
	lua_mathlibopen(state);

	int rc = my_dofile(state, 1, baseDir.c_str());
	if (rc != 0) {
		string s = lua_tostring(state, -1);
		return 0;
	}

	return new KEP::LuaScriptVM(state);
}
