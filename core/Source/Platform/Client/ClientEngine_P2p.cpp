///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "RuntimeSkeleton.h"
#include "CPhysicsEmuClass.h"

namespace KEP {

static LogInstance("ClientEngine");
static IEvent* s_pEventTimed = nullptr;

void ClientEngine::P2pAnimDataDeref(
	const P2pAnimData& p2pAnimData,
	CMovementObj*& pMO_init,
	CMovementObj*& pMO_target,
	CMovementObj*& pMO_me,
	CMovementObj*& pMO_other,
	eAnimType& animType_me,
	eAnimType& animType_other) {
	pMO_init = GetMovementObjByNetId(p2pAnimData.netTraceId_init);
	pMO_target = GetMovementObjByNetId(p2pAnimData.netTraceId_target);
	pMO_me = GetMovementObjMe();
	if (pMO_me == pMO_init) {
		pMO_other = pMO_target;
		animType_me = p2pAnimData.animType_init;
		animType_other = p2pAnimData.animType_target;
	} else {
		pMO_other = pMO_init;
		animType_me = p2pAnimData.animType_target;
		animType_other = p2pAnimData.animType_init;
	}
}

bool ClientEngine::P2pAnimStateInactive() {
	// Update My P2p Animation State
	m_p2pAnimData.animState = eP2pAnimState::Inactive;

	// Clear Timed Inactive Event
	s_pEventTimed = nullptr;

	LogInfo("OK");

	return true;
}

bool ClientEngine::P2pAnimStateBegin(const P2pAnimData& p2pAnimData) {
	// Only Begin From Inactive
	if (m_p2pAnimData.animState != eP2pAnimState::Inactive)
		return false;

	// Update My P2p Animation Data
	m_p2pAnimData = p2pAnimData;

	// Dereference My P2p Animation Data
	CMovementObj *pMO_init, *pMO_target, *pMO_me, *pMO_other;
	eAnimType animType_me, animType_other;
	P2pAnimDataDeref(m_p2pAnimData, pMO_init, pMO_target, pMO_me, pMO_other, animType_me, animType_other);
	if (!pMO_init || !pMO_target || !pMO_me || !pMO_other || !pMO_me->getSkeleton() || !pMO_other->getSkeleton()) {
		m_p2pAnimData.animState = eP2pAnimState::Inactive;
		return false;
	}

	// Preload Animations To Assure Proper Sync
	const int prioDelta = 2000; // added to DM_PRELOAD_PRIORITY to give DL_PRIO_HIGH
	auto animGlid_me = pMO_me->getSkeleton()->getAnimationGlidByAnimTypeAndVer(animType_me);
	PreloadAnimation(animGlid_me, prioDelta);
	auto animGlid_other = pMO_other->getSkeleton()->getAnimationGlidByAnimTypeAndVer(animType_other);
	PreloadAnimation(animGlid_other, prioDelta);

	// Schedule Timed Start Event (to future server synced time)
	TimeMs startTimeMs = p2pAnimData.animStartTimeMs - fTime::TimeSyncMs();
	if (startTimeMs < 0.0)
		startTimeMs = 0.0;
	P2pAnimData animData = m_p2pAnimData;
	animData.animState = eP2pAnimState::Start;
	s_pEventTimed = new P2pAnimEvent(animData);
	m_dispatcher.QueueEventInFutureMs(s_pEventTimed, startTimeMs);

	LogInfo("OK -"
			<< " " << pMO_other->ToStr()
			<< " animGlids<" << animGlid_me << ":" << animGlid_other << ">"
			<< " startTimeMs=" << startTimeMs
			<< " animTimeMs=" << p2pAnimData.animTimeMs
			<< (p2pAnimData.animLooping ? " LOOP" : ""));

	return true;
}

bool ClientEngine::P2pAnimStateStart() {
	// Only Start From Begin
	if (m_p2pAnimData.animState != eP2pAnimState::Begin)
		return false;

	// Clear Timed Start Event
	s_pEventTimed = nullptr;

	// Dereference My P2p Animation Data
	CMovementObj *pMO_init, *pMO_target, *pMO_me, *pMO_other;
	eAnimType animType_me, animType_other;
	P2pAnimDataDeref(m_p2pAnimData, pMO_init, pMO_target, pMO_me, pMO_other, animType_me, animType_other);
	if (!pMO_init || !pMO_target || !pMO_me || !pMO_other || !pMO_other->getSkeleton()) {
		m_p2pAnimData.animState = eP2pAnimState::Inactive;
		return false;
	}

	// Update My P2p Animation State
	m_p2pAnimData.animState = eP2pAnimState::Start;

	// Move Initiator On Top Of And Facing Target
	pMO_init->setBaseFramePosition(pMO_target->getBaseFramePosition(), true);
	Matrix44f rotMat;
	ConvertRotationY(M_PI, out(rotMat));
	Vector3f initPos = pMO_init->getBaseFramePosition();
	pMO_init->setBaseFrameMatrix(pMO_target->getBaseFrameMatrix() * rotMat, false);
	pMO_init->setBaseFramePosition(initPos, false);

	// Set Current Animation Settings (dirForward, default looping, no blending)
	setMyOverrideAnimationByType(animType_me, ANIM_VER_ANY);
	auto animGlid = pMO_other->getSkeleton()->getAnimationGlidByAnimTypeAndVer(animType_other);
	AnimSettings animSettings(animGlid, true, eAnimLoopCtl::Default, 0.0);
	pMO_other->getSkeleton()->setPrimaryAnimationSettings(animSettings);

	// Schedule Timed End Event
	if (!m_p2pAnimData.animLooping) {
		TimeMs endTimeMs = m_p2pAnimData.animTimeMs;
		P2pAnimData animData = m_p2pAnimData;
		animData.animState = eP2pAnimState::End;
		s_pEventTimed = new P2pAnimEvent(animData);
		m_dispatcher.QueueEventInFutureMs(s_pEventTimed, endTimeMs);
	}

	return true;
}

bool ClientEngine::P2pAnimStateEnd() {
	// Only End From Start
	if (m_p2pAnimData.animState != eP2pAnimState::Start)
		return false;

	// Clear Timed End Event
	s_pEventTimed = nullptr;

	// Update My P2p Animation Statre
	m_p2pAnimData.animState = eP2pAnimState::End;

	// Send Terminate Event To Server
	P2pAnimData animData = m_p2pAnimData;
	animData.animState = eP2pAnimState::Terminate;
	IEvent* pEvent = new P2pAnimEvent(animData);
	pEvent->AddTo(SERVER_NETID);
	m_dispatcher.QueueEvent(pEvent);

	return true;
}

bool ClientEngine::P2pAnimStateTerminate() {
	// Only Terminate If Active And Not Already Terminated
	if (!P2pIsActive() || (m_p2pAnimData.animState == eP2pAnimState::Terminate))
		return false;

	// Cancel Any Timed Event
	if (s_pEventTimed) {
		m_dispatcher.CancelEvent(s_pEventTimed);
		s_pEventTimed = nullptr;
	}

	// Update My P2p Animation State
	m_p2pAnimData.animState = eP2pAnimState::Terminate;

	// Dereference My P2p Animation Data
	CMovementObj *pMO_init, *pMO_target, *pMO_me, *pMO_other;
	eAnimType animType_me, animType_other;
	P2pAnimDataDeref(m_p2pAnimData, pMO_init, pMO_target, pMO_me, pMO_other, animType_me, animType_other);

	// Stop Animating & Move Initiator Back
	if (pMO_me) {
		setMyOverrideAnimationByType(eAnimType::Stand, 0);
		auto pMC = pMO_me->getMovementCaps();
		if (pMC)
			pMO_me->setBaseFramePosition(pMC->GetPosLast(), true);
	}
	if (pMO_other) {
		auto pRS = pMO_other->getSkeleton();
		if (pRS)
			pRS->setPrimaryAnimationByAnimTypeAndVer(eAnimType::Stand);
	}

	// Schedule Timed Inactive Event (300ms to give server-side movement updates time to stabilize)
	TimeMs inactiveTimeMs = 300.0;
	P2pAnimData animData = m_p2pAnimData;
	animData.animState = eP2pAnimState::Inactive;
	s_pEventTimed = new P2pAnimEvent(animData);
	m_dispatcher.QueueEventInFutureMs(s_pEventTimed, inactiveTimeMs);

	return true;
}

} // namespace KEP