///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ExternalLuac.h"

#include "UnicodeWindowsHelpers.h"

std::string ExternalLuac::CallExternalLuaCompiler(std::string const& compilerPath, std::string const& sourceLuaFile, bool deleteCompiled) {
	bool fatalError = false;
	std::string errorString;
	std::string compileOut = sourceLuaFile + "c";
	std::wstring compileOutW = KEP::Utf8ToUtf16(compileOut);
	HANDLE previousH = ::CreateFileW(compileOutW.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (previousH != INVALID_HANDLE_VALUE) {
		CloseHandle(previousH);
		if (!::DeleteFileW(compileOutW.c_str())) {
			fatalError = true;
			errorString = "Cannot delete existing output file: ";
			errorString = errorString + compileOut;
		}
	}
	std::wstring wbuffer;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	memset(&si, 0, sizeof(si));
	memset(&pi, 0, sizeof(pi));
	si.cb = sizeof(si);
	std::wstring sourceLuaFileW = KEP::Utf8ToUtf16(sourceLuaFile);
	wbuffer += L" -o \"" + sourceLuaFileW + L"c\" \"" + sourceLuaFileW + L"\"";
	std::string luacOutput;
	bool luaCompileFailed = false;
	// Add pipes to catch compiler output for display to client
	if (!fatalError) {
		HANDLE hOutputWrite, hOutputRead;

		SECURITY_ATTRIBUTES sa;
		sa.nLength = sizeof(SECURITY_ATTRIBUTES);
		sa.lpSecurityDescriptor = NULL;
		sa.bInheritHandle = TRUE;
		si.cb = sizeof(STARTUPINFO);
		si.dwFlags = STARTF_USESTDHANDLES;
		si.wShowWindow = SW_HIDE;
		if (!CreatePipe(&hOutputRead, &hOutputWrite, &sa, 0)) {
			fatalError = true;
			errorString = "Failed to create luac pipe";
		}
		if (!fatalError && !SetHandleInformation(hOutputRead, HANDLE_FLAG_INHERIT, 0)) {
			fatalError = true;
			errorString = "Failed to Set output Read Handle Info";
		}
		if (!fatalError) {
			si.hStdInput = NULL;
			si.hStdOutput = hOutputWrite;
			si.hStdError = hOutputWrite;

			auto compilerPathW = KEP::Utf8ToUtf16(compilerPath);
			bool ok = KEP::CreateProcess(
				static_cast<LPCWSTR>(compilerPathW.c_str()),
				static_cast<LPCWSTR>(wbuffer.c_str()),
				NULL,
				NULL,
				TRUE,
				CREATE_NO_WINDOW,
				NULL,
				NULL,
				&si,
				&pi);
			if (!ok) {
				fatalError = true;
				errorString = "Failed to create luac process";
			}

			::WaitForSingleObject(pi.hProcess, INFINITE);

			DWORD luacExitCode;
			if (GetExitCodeProcess(pi.hProcess, &luacExitCode) == TRUE) {
				if (luacExitCode != 0) {
					luaCompileFailed = true;
				}
			} else {
				fatalError = true;
				errorString = "Failed to get return value from luac process";
			}

			::CloseHandle(hOutputWrite);

			// Read in the output of luac
			bool finishedReadingOutput = false;
			while (!finishedReadingOutput) {
				DWORD bytesRead;
				static const unsigned int PIPEBUFFER_READ_SIZE = 1024;
				char lpBuffer[PIPEBUFFER_READ_SIZE];
				if (!ReadFile(hOutputRead, lpBuffer, PIPEBUFFER_READ_SIZE - 1, &bytesRead, NULL) || bytesRead == 0) {
					if (GetLastError() == ERROR_BROKEN_PIPE) {
						// end of pipe
						finishedReadingOutput = true;
						if (bytesRead != 0) {
							lpBuffer[bytesRead] = '\0';
							luacOutput = luacOutput + lpBuffer;
						}
					} else {
						fatalError = true;
						errorString = "Error reading output pipe";
						finishedReadingOutput = true;
					}
				} else {
					lpBuffer[bytesRead] = '\0';
					luacOutput = luacOutput + lpBuffer;
				}
			}
		}
	}

	if (!fatalError) {
		// Check that lua compile was successful
		HANDLE outH = ::CreateFile(compileOutW.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (outH == INVALID_HANDLE_VALUE || luaCompileFailed) {
			fatalError = true;
			errorString = "Compiler error for ";
			errorString = sourceLuaFile + ":\n" + luacOutput;
		}

		if (outH != INVALID_HANDLE_VALUE)
			::CloseHandle(outH);
	}

	if (deleteCompiled) {
		previousH = ::CreateFile(compileOutW.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (previousH != INVALID_HANDLE_VALUE) {
			::CloseHandle(previousH);
			if (!::DeleteFile(compileOutW.c_str())) {
				fatalError = true;
				errorString = "Cannot delete resulting luac: ";
				errorString = errorString + compileOut;
			}
		}
	}

	return fatalError ? errorString : "Success";
}