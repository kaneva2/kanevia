///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "RenderEngine\ReMesh.h"
#include "RuntimeSkeleton.h"
#include "KEPPhysics/Universe.h"
#include "KEPPhysics/Collidable.h"
#include "common\include\CoreStatic\PhysicsUserData.h"
#include "include\core\Math\Units.h"

namespace KEP {

static LogInstance("ClientEngine");

void ClientEngine::GetPickRay(Vector3f* rayOrigin, Vector3f* rayDir, int x, int y) {
	if (GetD3DDevice()) {
		Matrix44f matProj;
		GetProjectionMatrix(&matProj);

		Matrix44f matView;
		GetViewMatrix(&matView);

		Matrix44f matViewInv;
		matViewInv.MakeInverseOf(matView);

		D3DVIEWPORT9 viewport;
		GetD3DDevice()->GetViewport(&viewport);

		if (viewport.Height != 0) {
			float fx = (float)x / (viewport.Width / 2.0f) - 1.0f;
			float fy = 1.0f - (float)y / (viewport.Height / 2.0f);

			float dx = (1.0f / matProj(0, 0)) * fx;
			float dy = (1.0f / matProj(1, 1)) * fy;

			real hither = 0.01f;
			real yon = 10.0f;

			Vector4f p1(dx * hither, dy * hither, hither, 1.0f);
			Vector4f p2(dx * yon, dy * yon, yon, 1.0f);

			p1 = TransformVector(matViewInv, p1);
			p2 = TransformVector(matViewInv, p2);

			*rayOrigin = Vector3f(p1.x, p1.y, p1.z);

			Vector4f d(p2 - p1);
			*rayDir = Vector3f(d.x, d.y, d.z);

			rayDir->Normalize();
		}
	}
}

bool ClientEngine::GlueRayTrace(const Vector3f& rayOrigin, const Vector3f& rayDirection, ObjectType mask, ObjectType& typeResult, int& idResult, Vector3f& intersection, float& distance) {
	TraceInfo trace;
	bool result = RayTrace(&trace, rayOrigin, rayDirection, FLT_MAX, mask, RAYTRACE_VISIBLE);
	if (result) {
		typeResult = trace.objType;
		idResult = trace.objId;
		intersection = trace.intersection;
		distance = trace.distance;
		return result;
	}
	typeResult = ObjectType::NONE;
	idResult = 0;
	return false;
}

bool ClientEngine::RayTrace(
	TraceInfo* pTI,
	const Vector3f& rayOrigin,
	const Vector3f& rayDir,
	float fDistance,
	ObjectType mask,
	unsigned long flags,
	std::function<bool(CDynamicPlacementObj*)> fnDynObjFilter) {
	// Initialize results
	pTI->distance = FLT_MAX;
	pTI->objType = ObjectType::NONE;
	pTI->objId = -1;
	pTI->intersection = Vector3f();
	pTI->normal = Vector3f(0, 0, 1);

	bool bSkipInvisible = (flags & RAYTRACE_NOT_HIDDEN) != 0;
	bool bSkipCulled = (flags & RAYTRACE_NOT_CULLED) != 0;
	bool bSkipUnselected = (flags & RAYTRACE_SELECTED) != 0;
	bool bSkipNoncollidableWorldObjects = (flags & RAYTRACE_SKIP_NONCOLLIDABLE_WORLD_OBJECTS) != 0;
	bool bSkipTransparent = (flags & RAYTRACE_SKIP_TRANSPARENT) != 0;

	Physics::RayTestResult result;
	Vector3f vNormalizedDir = rayDir.GetNormalized();

	std::function<bool(Physics::Collidable*)> objectFilter = [this, bSkipInvisible, bSkipCulled, bSkipUnselected, bSkipNoncollidableWorldObjects, mask, fnDynObjFilter](Physics::Collidable* pCollidable) {
		PhysicsBodyData* pPhysicsBodyData = static_cast<PhysicsBodyData*>(pCollidable->GetUserData().get());
		if (!pPhysicsBodyData)
			return false;

		if ((pPhysicsBodyData->m_ObjectType & mask) == ObjectType::NONE)
			return false;

		switch (pPhysicsBodyData->m_ObjectType) {
			case ObjectType::CAMERA:
				break;
			case ObjectType::SOUND:
				if (!m_renderSoundPlacements)
					return false;
				break;
			case ObjectType::WORLD: {
				CWldObject* pWorldObj = static_cast<CWldObject*>(pPhysicsBodyData->m_pObject);
				if (bSkipCulled && pWorldObj->m_culled)
					return false;
				if (bSkipNoncollidableWorldObjects && pWorldObj->m_collisionInfoFilter)
					return false;
				if (bSkipInvisible && pWorldObj->m_renderFilter)
					return false;
				break;
			}
			case ObjectType::DYNAMIC: {
				CDynamicPlacementObj* pDPO = static_cast<CDynamicPlacementObj*>(pPhysicsBodyData->m_pObject);
				if ((bSkipCulled && pDPO->m_culled) || (bSkipInvisible && !pDPO->IsVisible()))
					return false;
				if (fnDynObjFilter && !fnDynObjFilter(pDPO))
					return false;
				break;
			}
			case ObjectType::MOVEMENT: {
				CMovementObj* pMO = static_cast<CMovementObj*>(pPhysicsBodyData->m_pObject);
				if (bSkipInvisible && !pMO->isVisible())
					return false;
				break;
			}
			case ObjectType::EQUIPPABLE:
				break;
			case ObjectType::BONE:
				break;
			default:
				break;
		}

		if (bSkipUnselected) {
			if (!this->ObjectIsSelected(pPhysicsBodyData->m_ObjectType, pPhysicsBodyData->m_iObjectId))
				return false;
		}

		return true;
	};

	std::function<bool(Physics::Collidable * pCollidable, size_t iPart, size_t iTriangle)> subPartFilter;
	if (bSkipTransparent) {
		subPartFilter = [bSkipTransparent](Physics::Collidable* pCollidable, size_t iPart, size_t iTriangle) -> bool {
			const Physics::UserData* pUserData = pCollidable->GetUserData().get();
			if (!pUserData)
				return true;
			const PhysicsBodyData* pPhysicsBodyData = static_cast<const PhysicsBodyData*>(pUserData);
			const CMaterialObject* pMatObj = nullptr;
			if (pPhysicsBodyData->m_ObjectType == ObjectType::DYNAMIC) {
				CDynamicPlacementObj* pDPO = static_cast<CDynamicPlacementObj*>(pPhysicsBodyData->m_pObject);
				if (!pDPO)
					return true;
				StreamableDynamicObject* pSDO = pDPO->GetBaseObj();
				if (!pSDO)
					return true;
				StreamableDynamicObjectVisual* pVisual = pSDO->GetVisualFromCollisionData(pPhysicsBodyData->m_iRigidBodyIndex, iPart);
				if (!pVisual)
					return true;
				pMatObj = pVisual->getMaterial();
			} else if (pPhysicsBodyData->m_ObjectType == ObjectType::WORLD) {
				CWldObject* pWorldObj = static_cast<CWldObject*>(pPhysicsBodyData->m_pObject);
				if (!pWorldObj)
					return true;
				pMatObj = pWorldObj->GetMaterial();
			}
			if (!pMatObj)
				return false; // Objects without materials are not rendered, so are considered transparent.
			bool bTransparent = pMatObj->m_blendMode != CMaterialObject::BlendMode::BLEND_DISABLED;
			if (/*!bSkipTransparent ||*/ !bTransparent) // This function not used if bSkipTransparent is false.
				return true;
			return false;
		};
	};

	Physics::RayTestOptions rayTestOptions = Physics::RayTestOptions(CollisionGroup::Visible).SetCollidableFilter(std::move(objectFilter)).SetSubPartFilter(std::move(subPartFilter));
	if (m_pUniverse->RayTest(FeetToMeters(rayOrigin), vNormalizedDir, FeetToMeters(fDistance), rayTestOptions, out(result)) && result.m_pHitObject) {
		PhysicsBodyData* pBodyData = static_cast<PhysicsBodyData*>(result.m_pHitObject->GetUserData().get());
		pTI->distance = MetersToFeet(result.m_fRelativeHitLocation);
		pTI->intersection = rayOrigin + pTI->distance * vNormalizedDir;
		pTI->normal = result.m_vNormal;

		Vector3f ptIntersection(0, 0, 0);
		bool intersectionInWorldSpace = (flags & INTERSECTION_IN_WORLD_SPACE) != 0;
		if (!intersectionInWorldSpace) {
			Matrix44f worldTransform = MetersToFeet(result.m_pHitObject->GetTransformMetric());
			Matrix44f invWorldTransform;
			if (invWorldTransform.MakeInverseOf(worldTransform))
				ptIntersection = TransformPoint(invWorldTransform, pTI->intersection);
		}
		pTI->objectIntersection = ptIntersection;
		pTI->objId = pBodyData ? pBodyData->m_iObjectId : 0;
		pTI->objType = pBodyData ? pBodyData->m_ObjectType : ObjectType::NONE;
	}

	static volatile bool bTestLegacy = false;
	if (bTestLegacy) {
		TraceInfo tiLegacy;
		RayTraceLegacy(&tiLegacy, rayOrigin, rayDir, mask, flags);
		*pTI = tiLegacy;
		int kk = 0;
		kk;
	}

	return pTI->objType != ObjectType::NONE;
}

bool ClientEngine::RayTraceLegacy(
	TraceInfo* pTI,
	const Vector3f& rayOrigin,
	const Vector3f& rayDir,
	ObjectType objType,
	unsigned long flags) {
	// Initialize results
	pTI->distance = FLT_MAX;
	pTI->objType = ObjectType::NONE;
	pTI->objId = -1;
	pTI->intersection = Vector3f();
	pTI->normal = Vector3f(0, 0, 1);

	bool visibleOnly = (flags & RAYTRACE_VISIBLE) != 0;
	bool selectedOnly = (flags & RAYTRACE_SELECTED) != 0;
	bool intersectionInWorldSpace = (flags & INTERSECTION_IN_WORLD_SPACE) != 0;
	bool bSkipTransparent = (flags & RAYTRACE_SKIP_TRANSPARENT) != 0;

	Matrix44f worldTransform;

	// World Object ?
	if ((objType & ObjectType::WORLD) != ObjectType::NONE) {
		MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
		if (m_pWOL) {
			for (POSITION pos = m_pWOL->GetHeadPosition(); pos;) {
				auto pWO = (CWldObject*)m_pWOL->GetNext(pos);
				if (!pWO)
					continue;

				if (visibleOnly && pWO->m_culled)
					continue;

				if (selectedOnly && !ObjectIsSelected(ObjectType::WORLD, pWO->m_identity))
					continue;

				Vector3f intersection, normal;
				float distance = FLT_MAX;
				bool hit = PickRayHitTest_WorldObject(pWO, rayOrigin, rayDir, intersection, normal, distance);
				if (!hit || (distance > pTI->distance))
					continue;

				pTI->objType = ObjectType::WORLD;
				pTI->objId = pWO->m_identity;
				pTI->intersection = intersection;
				pTI->normal = normal;
				pTI->distance = distance;
				worldTransform = *pWO->GetReMesh()->GetWorldMatrix();
			}
		}
	}

	// Dynamic Object ?
	if ((objType & ObjectType::DYNAMIC) != ObjectType::NONE) {
		IterateObjects([this, visibleOnly, selectedOnly, pTI, &worldTransform, rayOrigin, rayDir, bSkipTransparent](CDynamicPlacementObj* pDPO) -> bool {
			if (pDPO && pDPO->hasSkeleton() && (!visibleOnly || pDPO->IsVisible() && !pDPO->m_culled) && !IS_CORE_GLID(pDPO->GetGlobalId())) {
				if (!selectedOnly || ObjectIsSelected(ObjectType::DYNAMIC, pDPO->GetPlacementId())) {
					Vector3f intersection, normal;
					float distance = FLT_MAX;
					bool hit = PickRayHitTest_DynamicObject(pDPO, rayOrigin, rayDir, intersection, normal, distance, !bSkipTransparent);
					if (hit && distance < pTI->distance) {
						pTI->objType = ObjectType::DYNAMIC;
						pTI->objId = pDPO->GetPlacementId();
						pTI->intersection = intersection;
						pTI->normal = normal;
						pTI->distance = distance;
						worldTransform = pDPO->getSkeleton()->getWorldMatrix();
					}
				}
			}
			return true;
		});
	}

	// Entity Object ? (movement object)
	if ((objType & ObjectType::MOVEMENT) != ObjectType::NONE) {
		if (m_movObjList) {
			POSITION pos = m_movObjList->GetHeadPosition();

			while (pos) {
				auto pMO = (CMovementObj*)m_movObjList->GetNext(pos);
				if (pMO && (!visibleOnly || isMovementObjectVisible(pMO)) && (!selectedOnly || ObjectIsSelected(ObjectType::MOVEMENT, pMO->getNetTraceId()))) {
					bool hit = false;
					Vector3f intersection, normal;
					float distance = FLT_MAX;

					ObjectType objType_sub;
					int objId_sub;
					if (pMO->getSubstitute(objType_sub, objId_sub)) {
						switch (objType_sub) {
							case ObjectType::DYNAMIC: {
								auto pDPO = GetDynamicObject(objId_sub);
								if (pDPO)
									hit = PickRayHitTest_DynamicObject(pDPO, rayOrigin, rayDir, intersection, normal, distance, !bSkipTransparent);
							} break;
						}
					} else {
						hit = PickRayHitTest_MovementObject(pMO, rayOrigin, rayDir, intersection, normal, distance);
					}

					if (hit && distance < pTI->distance) {
						pTI->objType = ObjectType::MOVEMENT;
						pTI->objId = (int)pMO->getNetTraceId();
						pTI->intersection = intersection;
						pTI->normal = normal;
						pTI->distance = distance;
						worldTransform = pMO->getBaseFrameMatrix();
					}
				}
			}
		}
	}

	// Sound Object ?
	if ((objType & ObjectType::SOUND) != ObjectType::NONE && m_renderSoundPlacements) {
		for (const auto& it : m_soundPlacements) {
			auto soundPlacementId = it.first;
			auto pSP = it.second;
			if (!pSP)
				continue;

			// DRF - Some Sound Placements Are Never Shown (attached to player)
			if (!pSP->ShowSoundPlacement())
				continue;

			if (!selectedOnly || ObjectIsSelected(ObjectType::SOUND, soundPlacementId)) {
				Vector3f intersection, normal;
				float distance = FLT_MAX;
				bool hit = SoundPlacementPickRayHitTest(pSP, rayOrigin, rayDir, intersection, normal, distance);
				if (hit && distance < pTI->distance) {
					pTI->objType = ObjectType::SOUND;
					pTI->objId = soundPlacementId;
					pTI->intersection = intersection;
					pTI->normal = normal;
					pTI->distance = distance;
					worldTransform = *(pSP->GetLocationMat());
				}
			}
		}
	}

	bool found = (pTI->objType != ObjectType::NONE);
	if (found && !intersectionInWorldSpace) {
		// Calculate the intersection point in object space.
		// Transform (x, y, z, 1) by inverse world transform matrix, project result back into w=1.
		Matrix44f invWorldTransform;
		if (invWorldTransform.MakeInverseOf(worldTransform))
			pTI->objectIntersection = TransformPoint(invWorldTransform, pTI->intersection);
	}

	return found;
}

bool ClientEngine::SphereTest_DynamicObject(CDynamicPlacementObj* pDPO, const Vector3f& sphereOrigin, float radius, float& distance, Vector3f& intersection, const Vector3f& orientation, float maxAngle) {
	Vector4f minB, maxB;

	distance = FLT_MAX;

	if (pDPO->isPrimaryAnimationSettingsValid())
		pDPO->GetBoundingBoxAnimAdjust(minB, maxB);
	else
		pDPO->GetBoundingBox(minB, maxB);

	return SphereToBox(minB, maxB, sphereOrigin, radius, intersection) ? pDPO->IntersectSphere(sphereOrigin, radius, orientation, maxAngle, &distance, &intersection, NULL, true) : false;
}

bool ClientEngine::SphereTest_MovementObject(CMovementObj* pMO, const Vector3f& sphereOrigin, float radius, float& distance, Vector3f& intersection, const Vector3f& orientation, float maxAngle) {
	ObjectType objType;
	int objId;
	if (pMO->getSubstitute(objType, objId)) {
		switch (objType) {
			case ObjectType::DYNAMIC: {
				CDynamicPlacementObj* pDPO = GetDynamicObject(objId);
				if (pDPO)
					return SphereTest_DynamicObject(pDPO, sphereOrigin, radius, distance, intersection, orientation, maxAngle);
			} break;
		}
	} else {
		Vector3f charPos;
		RuntimeSkeleton* skeleton = pMO->getSkeleton();

		if (skeleton->getAnimatedWorldPosition(charPos)) {
			Vector3f boxMin, boxMax;

			boxMin = charPos + skeleton->getBonesBoundingBox().GetMin();
			boxMax = charPos + skeleton->getBonesBoundingBox().GetMax();

			if (SphereToBox(Vector4f(boxMin, 0.0f), Vector4f(boxMax, 0.0f), sphereOrigin, radius, intersection)) {
				bool result = skeleton->intersectSphere(sphereOrigin, radius, orientation, maxAngle, &intersection);
				if (result) {
					distance = Distance(intersection, sphereOrigin);
				}
				// normal will be based on bound box not actual mesh :(
				return result;
			}
		}
	}

	return false;
}

bool ClientEngine::SphereTestObject(const ObjectType& objType, int objId, Vector3f& sphereOrigin, float radius, Vector3f& intersection, float& distance, const Vector3f& orientation, float maxAngle) {
	switch (objType) {
		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(objId);
			return (pDPO ? SphereTest_DynamicObject(pDPO, sphereOrigin, radius, distance, intersection, orientation, maxAngle) : false);
		} break;

		case ObjectType::MOVEMENT: {
			auto pMO = GetMovementObjByNetId(objId);
			return (pMO ? SphereTest_MovementObject(pMO, sphereOrigin, radius, distance, intersection, orientation, maxAngle) : false);
		} break;
	}

	return false;
}

/*
Fast Ray-Box Intersection
by Andrew Woo
from "Graphics Gems", Academic Press, 1990
http://www.acm.org/tog/GraphicsGems/gems/RayBox.c

char HitBoundingBox(minB,maxB, origin, dir,coord)
float minB[3], maxB[3];		//box
float origin[3], dir[3];	//ray
float coord[3];				// hit point
*/
bool ClientEngine::HitBoundingBox(const Vector4f& minB, const Vector4f& maxB, const Vector3f& origin, const Vector3f& dir, Vector3f& coord, Vector3f& normal, BOOL inOut) {
	const int RIGHT = 0;
	const int LEFT = 1;
	const int MIDDLE = 2;
	char inside = TRUE;
	char quadrant[3];
	register int i;
	int whichPlane;
	float maxT[3];
	float candidatePlane[3];

	/* Find candidate planes; this loop can be avoided if
	rays cast all from the eye(assume perpsective view) */
	for (i = 0; i < 3; i++)
		if (origin[i] < minB[i]) {
			quadrant[i] = LEFT;
			candidatePlane[i] = minB[i];
			inside = FALSE;
		} else if (origin[i] > maxB[i]) {
			quadrant[i] = RIGHT;
			candidatePlane[i] = maxB[i];
			inside = FALSE;
		} else {
			quadrant[i] = MIDDLE;
		}

	/* Ray origin inside bounding box */
	if (inside) {
		if (inOut)
			return FALSE;
		coord = origin;
		return TRUE;
	}

	/* Calculate T distances to candidate planes */
	for (i = 0; i < 3; i++) {
		if (quadrant[i] != MIDDLE && dir[i] != 0.)
			maxT[i] = (candidatePlane[i] - origin[i]) / dir[i];
		else
			maxT[i] = -1.;
	}

	/* Get largest of the maxT's for final choice of intersection */
	whichPlane = 0;
	for (i = 1; i < 3; i++) {
		if (maxT[whichPlane] < maxT[i])
			whichPlane = i;
	}

	/* Check final candidate actually inside box */
	if (maxT[whichPlane] < 0.)
		return (FALSE);
	for (i = 0; i < 3; i++) {
		if (whichPlane != i) {
			coord[i] = origin[i] + maxT[whichPlane] * dir[i];
			if (coord[i] < minB[i] || coord[i] > maxB[i])
				return (FALSE);
		} else {
			coord[i] = candidatePlane[i];
		}
	}

	/* get hit normal */
	normal.x = normal.y = normal.z = 0.f;

	if (coord[0] == minB.x) {
		normal.x = -1.f;
	} else if (coord[0] == maxB.x) {
		normal.x = 1.f;
	}

	if (coord[1] == minB.y) {
		normal.y = -1.f;
	} else if (coord[1] == maxB.y) {
		normal.y = 1.f;
	}

	if (coord[2] == minB.z) {
		normal.z = -1.f;
	} else if (coord[2] == maxB.z) {
		normal.z = 1.f;
	}

	return true; // ray hits box
}

bool ClientEngine::SphereToBox(const Vector4f& minB, const Vector4f& maxB, const Vector3f& origin, const float radius, Vector3f& intersection) {
	// Find the closest point on the box by clamping the coords.
	Vector3f closestPoint;
	closestPoint.x = (origin.x < minB.x) ? minB.x : (origin.x > maxB.x) ? maxB.x : origin.x;
	closestPoint.y = (origin.y < minB.y) ? minB.y : (origin.y > maxB.y) ? maxB.y : origin.y;
	closestPoint.z = (origin.z < minB.z) ? minB.z : (origin.z > maxB.z) ? maxB.z : origin.z;

	// Do a distance check against the radius.
	Vector3f vecToBox = origin - closestPoint;

	// Set the out vector. This is where the collision theoretically happens (IE, closest point on/in box to sphere center).
	intersection = closestPoint;

	return vecToBox.LengthSquared() <= radius * radius;
}

bool ClientEngine::PickRayHitTest_WorldObject(CWldObject* pWO, const Vector3f& rayOrigin, const Vector3f& rayDir, Vector3f& intersection, Vector3f& normal, float& distance) {
	Vector4f boxMin, boxMax;
	if (pWO->GetBoundingBox(boxMin, boxMax)) {
		if (HitBoundingBox(boxMin, boxMax, rayOrigin, rayDir, intersection, normal, FALSE)) {
			return pWO->IntersectRay(rayOrigin, rayDir, &distance, &intersection, &normal);
		}
	}

	return false;
}

bool ClientEngine::PickRayHitTest_DynamicObject(CDynamicPlacementObj* pDPO, const Vector3f& rayOrigin, const Vector3f& rayDir, Vector3f& intersection, Vector3f& normal, float& distance, bool bTestAlpha) {
	Vector4f minB, maxB;

	distance = FLT_MAX;

	if (pDPO->isPrimaryAnimationSettingsValid()) {
		pDPO->GetBoundingBoxAnimAdjust(minB, maxB);
		if (HitBoundingBox(minB, maxB, rayOrigin, rayDir, intersection, normal, FALSE)) {
			distance = Distance(intersection, rayOrigin);
			return true;
		}
	} else {
		pDPO->GetBoundingBox(minB, maxB);
		if (HitBoundingBox(minB, maxB, rayOrigin, rayDir, intersection, normal, FALSE)) {
			// Test against geometry
			distance = Distance(intersection, rayOrigin);
			if (pDPO->IntersectRay(rayOrigin, rayDir, &distance, &intersection, &normal, bTestAlpha)) {
				return true;
			}
		}
	}

	return false;
}

bool ClientEngine::PickRayHitTest_MovementObject(CMovementObj* pMO, const Vector3f& rayOrigin, const Vector3f& rayDir, Vector3f& intersection, Vector3f& normal, float& distance) {
	Vector3f charPos;
	RuntimeSkeleton* skeleton = pMO->getSkeleton();
	if (skeleton->getAnimatedWorldPosition(charPos)) {
		Vector3f boxMin, boxMax;
		boxMin = charPos + skeleton->getBonesBoundingBox().GetMin();
		boxMax = charPos + skeleton->getBonesBoundingBox().GetMax();

		// NOTE: the 'TRUE' here means that if the ray starts inside the box, then no intersection is found.
		// This is mainly so that selection still works if you are in first-person mode.
		// If you need different behavior talk to Matt.
		if (HitBoundingBox(Vector4f(boxMin, 0.0f), Vector4f(boxMax, 0.0f), rayOrigin, rayDir, intersection, normal, TRUE)) {
			if (pMO->isTypePC()) {
				// Do actual geometry test iff ray testing your own avatar.
				bool result = skeleton->intersectRay(rayOrigin, rayDir, &intersection);
				if (result) {
					distance = Distance(intersection, rayOrigin);
				}
				// normal will be based on bound box not actual mesh :(
				return result;
			}
			distance = Distance(intersection, rayOrigin);
			return true;
		}
	}

	return false;
}

// Arvo sphere/box test for 1st pass camera/dynamic object collision test
BOOL ClientEngine::XSectTest_DynamicObject(CDynamicPlacementObj* pDPO, Vector3f& rayCenter, FLOAT rayRadius) {
	// get bounding box of dynamic object
	Vector4f aMin, aMax;

	// boundbox not created yet
	pDPO->GetBoundingBox(aMin, aMax);

	// do boundsphere boundbox overlap test
	FLOAT d = 0;
	// Arvo sphere/box intersection
	for (DWORD i = 0; i < 3; i++) {
		if (rayCenter[i] < aMin[i]) {
			d += (rayCenter[i] - aMin[i]) * (rayCenter[i] - aMin[i]);
		} else if (rayCenter[i] > aMax[i]) {
			d += (rayCenter[i] - aMax[i]) * (rayCenter[i] - aMax[i]);
		}
	}

	// ray bound sphere does not overlap boundbox
	return (d > rayRadius * rayRadius) ? FALSE : TRUE;
}

ObjectType ClientEngine::EnvironmentSegmentTest(
	Vector3f& contactPos,
	float& contactDist,
	const Vector3f& startSegment,
	const Vector3f& endSegment,
	float tolerance,
	const std::function<bool(CDynamicPlacementObj*)>& filter) {
#if 1
	Vector3f vDir = endSegment - startSegment;
	float fLength = vDir.Length();
	if (fLength == 0)
		return ObjectType::NONE;

	vDir /= fLength;
	TraceInfo traceInfo;
	if (!RayTrace(&traceInfo, startSegment, vDir, fLength, ObjectType::DYNAMIC | ObjectType::WORLD, RAYTRACE_NOT_HIDDEN /*| RAYTRACE_SKIP_NONCOLLIDABLE_WORLD_OBJECTS*/ | RAYTRACE_SKIP_TRANSPARENT, filter))
		return ObjectType::NONE;
	contactDist = traceInfo.distance;
	contactPos = traceInfo.intersection;
	return traceInfo.objType;
#else
	// collision and adjustments
	Vector3f normal;
	int objectHit = 0;

	Vector3f dir(Vector3f(startSegment) - Vector3f(endSegment));
	Vector3f origin(endSegment);
	Vector3f dirNorm = dir.GetNormalized();

	Vector3f dynObjHit;
	Vector3f intersect;
	float dynObjDist = dir.Length();
	BOOL hitDynObj = FALSE;
	ObjectType hitType = ObjectType::NONE;

	float rayRadius = dir.Length() * .5f;
	Vector3f rayCenter((startSegment + endSegment) * 0.5f);

	IterateObjects([this, &startSegment, &rayCenter, rayRadius, &origin, &dirNorm, &dynObjDist, &dynObjHit, &hitDynObj, &filter](CDynamicPlacementObj* pDPO) {
		// cull the object by ray boundsphere vs. object boundbox overlap test
		// This will also test pObject->hasCollision().
		if (!pDPO->IsVisible())
			return;
		if (filter && !filter(pDPO))
			return;
		if (XSectTest_DynamicObject(pDPO, rayCenter, rayRadius)) {
			// do a full intersection
			Vector3f hit, normal;
			Vector4f minB, maxB;
			pDPO->GetBoundingBox(minB, maxB);
			if (HitBoundingBox(minB, maxB, origin, dirNorm, hit, normal, FALSE)) {
				// Do not test objects that are selected.  Check to see if this object is selected.
				int numSelected = ObjectsSelected();
				for (int selectedIndex = 0; selectedIndex < numSelected; selectedIndex++) {
					ObjectType type;
					int pid;
					ObjectGetSelection(&type, &pid, selectedIndex);
					if (type == ObjectType::DYNAMIC && pid == pDPO->GetPlacementId())
						return;
				}
				float dist = FLT_MAX;

				// Visuals with material alpha < 1.0 will not be tested.
				const bool testAlpha = false;
				if (pDPO->IntersectRay(Vector3f(startSegment), -dirNorm, &dist, &hit, NULL, testAlpha)) {
					if (dist < dynObjDist) {
						dynObjDist = dist;
						dynObjHit = hit;
						hitDynObj = TRUE;
					}
				}
			}
		}
		return; // next
	});

	if (!m_pCollisionObj)
		return ObjectType::NONE;

	// camera-world collision
	BOOL hitWorldObj = m_pCollisionObj->CollisionSegmentIntersect(startSegment, endSegment, tolerance, false, false, &intersect, &normal, &objectHit);

	// chose closer of world-object and dynamic-object hits
	if (hitWorldObj || hitDynObj) {
		float worldObjDistToEnd = 0.f;

		if (hitWorldObj) {
			worldObjDistToEnd = Distance(intersect, endSegment);
		}

		float dynObjDistToEnd = 0.f;
		if (hitDynObj) {
			dynObjDistToEnd = Distance(dynObjHit, endSegment);
		}

		if (worldObjDistToEnd > dynObjDistToEnd) {
			contactDist = worldObjDistToEnd;
			contactPos = intersect;
			hitType = ObjectType::WORLD;
		} else {
			contactDist = dynObjDistToEnd;
			contactPos = dynObjHit;
			hitType = ObjectType::DYNAMIC;
		}
	}

	return hitType;
#endif
}

} // namespace KEP