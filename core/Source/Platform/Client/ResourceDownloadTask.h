///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "DownloadManager.h"

namespace KEP {

// Forward declarations
class IClientEngine;
class IEvent;
class IDispatcher;

// Download task with dynamic priority that holds by an IResource instance
class ResourceDownloadTask : public DownloadInfo {
public:
	ResourceDownloadTask(IResource* res, bool compressed, IEvent* pEvent, IDispatcher* dispatcher, bool preload) :
			DownloadInfo(
				res->GetResourceType(),
				res->GetURL(),
				DL_PRIO_DYN,
				res->GetFilePath(),
				res->GetExpectedSize(),
				compressed,
				pEvent,
				dispatcher,
				preload),
			m_res(res) {}

	virtual DownloadPriority getPriority() const override {
		return m_res->GetDownloadPriority();
	}

private:
	IResource* m_res;
};

} // namespace KEP
