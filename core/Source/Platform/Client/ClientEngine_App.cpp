///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "ClientEngineApp.h"

namespace KEP {

bool ClientEngine::AppIsProduction() const {
	return g_ceApp.IsProduction();
}

DEV_MODE ClientEngine::AppDevMode() const {
	return g_ceApp.DevMode();
}

std::string ClientEngine::AppVersion() const {
	return g_ceApp.AppVersion();
}

std::string ClientEngine::AppPath() const {
	return g_ceApp.AppPath();
}

std::string ClientEngine::AppStarPath() const {
	return g_ceApp.StarPath();
}

std::string ClientEngine::AppStarId() const {
	return g_ceApp.StarId();
}

std::string ClientEngine::AppWokUrl() const {
	return g_ceApp.WokUrl();
}

std::string ClientEngine::AppPatchUrl() const {
	return g_ceApp.PatchUrl();
}

std::string ClientEngine::AppLoginEmail() const {
	return g_ceApp.LoginEmail();
}

std::string ClientEngine::AppLoginPassword() const {
	return g_ceApp.LoginPassword();
}

std::string ClientEngine::AppTTLang() const {
	return g_ceApp.TTLang();
}

int ClientEngine::AppTestGroup() const {
	return g_ceApp.TestGroup();
}

int ClientEngine::AppRipperDetected() const {
	return g_ceApp.RipperDetected();
}

bool ClientEngine::AppSetStateStopped() const {
	return g_ceApp.SetStateStopped(!m_closeOnStartup);
}

} // namespace KEP