///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "SoundPlacement.h"
#include "Common/KEPUtil/FrameTimeProfiler.h"
#include <KEPPhysics/Universe.h>

namespace KEP {

static LogInstance("ClientEngine");

bool ClientEngine::SoundPlacementsInit() {
	return SoundPlacement::InitOpenAL();
}

void ClientEngine::SoundPlacementsLog(bool verbose) const {
	LogInfo("placements=" << m_soundPlacements.size());
	if (!verbose)
		return;
	for (const auto& it : m_soundPlacements) {
		auto pSP = it.second;
		if (!pSP)
			continue;
		bool isMyVehicle = SoundPlacementIsMyVehicle(pSP);
		_LogInfo(" ... " << pSP->ToStr() << (isMyVehicle ? " MY VEHICLE" : ""));
	}
}

bool ClientEngine::SoundPlacementsClear() {
	// Delete All Sound Placements
	for (const auto& it : m_soundPlacements)
		SoundPlacementDelInternal(it.second);
	m_soundPlacements.clear();

	return true;
}

bool ClientEngine::SoundPlacementsGetIds(std::vector<int>& soundPlacementIdList) {
	// Build List Of All Non-Attached Sound Placement Ids
	for (const auto& it : m_soundPlacements) {
		auto pSP = it.second;
		if (!pSP)
			continue;
		if (pSP->IsAttached())
			continue;
		auto soundPlacementId = it.first;
		soundPlacementIdList.push_back(soundPlacementId);
	}

	return true;
}

SoundPlacement* ClientEngine::SoundPlacementGet(int soundPlacementId) const {
	if (soundPlacementId == 0)
		return NULL;

	// Find Sound Placement With Matching Placement Id
	auto it = m_soundPlacements.find(soundPlacementId);
	if (it == m_soundPlacements.end())
		return NULL;
	auto pSP = it->second;
	return pSP;
}

int ClientEngine::SoundPlacementGetId(const SoundPlacement* pSP) const {
	if (!pSP)
		return 0;

	// Find Sound Placement Id With Matching Placement
	for (const auto& it : m_soundPlacements) {
		auto pSP_find = it.second;
		if (pSP == pSP_find)
			return it.first;
	}
	return 0;
}

bool ClientEngine::SoundPlacementAdd(int soundPlacementId) {
	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO || !pMO->getBaseFrame())
		return false;

	// Add New Sound Placement At My Avatar Position
	auto pSP = new SoundPlacement(soundPlacementId, pMO->getBaseFramePosition());
	if (!pSP)
		return false;

	// Add To Sound Placement List
	m_soundPlacements.insert(std::pair<int, SoundPlacement*>(soundPlacementId, pSP));

	m_pUniverse->Add(pSP->GetVisibleBody());

	return true;
}

bool ClientEngine::SoundPlacementDel(int soundPlacementId) {
	// Find Sound Placement With Matching Id
	auto it = m_soundPlacements.find(soundPlacementId);
	if (it == m_soundPlacements.end())
		return false;

	// Delete Sound Placement
	SoundPlacementDelInternal(it->second);
	m_soundPlacements.erase(soundPlacementId);

	return true;
}

bool ClientEngine::SoundPlacementDel(SoundPlacement* pSP) {
	// Delete Sound Placement By Placement Id
	int soundPlacementId = SoundPlacementGetId(pSP);
	if (soundPlacementId == 0)
		return false;

	// Delete Sound Placement
	SoundPlacementDelInternal(pSP);
	m_soundPlacements.erase(soundPlacementId);

	return true;
}

bool ClientEngine::SoundPlacementDelInternal(SoundPlacement* pSP) {
	if (!pSP)
		return false;
	m_pUniverse->Remove(pSP->GetVisibleBody().get());
	delete pSP;
	return true;
}

bool ClientEngine::SoundPlacementGetInfo(const SoundPlacement* pSP, std::string& name, GLID& glid) const {
	name = pSP ? pSP->GetName() : "";
	glid = pSP ? pSP->GetGlid() : GLID_INVALID;
	return (pSP != NULL);
}

bool ClientEngine::SoundPlacementGetInfo(int soundPlacementId, std::string& name, GLID& glid) const {
	return SoundPlacementGetInfo(SoundPlacementGet(soundPlacementId), name, glid);
}

bool ClientEngine::SoundPlacementSetPosition(SoundPlacement* pSP, const Vector3f& pos) {
	return pSP ? pSP->SetPosition(pos) : false;
}

bool ClientEngine::SoundPlacementSetPosition(int soundPlacementId, const Vector3f& pos) {
	return SoundPlacementSetPosition(SoundPlacementGet(soundPlacementId), pos);
}

bool ClientEngine::SoundPlacementGetPosition(const SoundPlacement* pSP, Vector3f& pos) const {
	pos = pSP ? pSP->GetPosition() : Vector3f(0.0, 0.0, 0.0);
	return (pSP != NULL);
}

bool ClientEngine::SoundPlacementGetPosition(int soundPlacementId, Vector3f& pos) const {
	return SoundPlacementGetPosition(SoundPlacementGet(soundPlacementId), pos);
}

bool ClientEngine::SoundPlacementSetRotationRad(SoundPlacement* pSP, float radians) {
	return pSP ? pSP->SetRotationRad(radians) : false;
}

bool ClientEngine::SoundPlacementSetRotationRad(int soundPlacementId, float radians) {
	return SoundPlacementSetRotationRad(SoundPlacementGet(soundPlacementId), radians);
}

bool ClientEngine::SoundPlacementGetRotationRad(const SoundPlacement* pSP, float& radians) const {
	radians = pSP ? pSP->GetRotationRad() : 0.0;
	return (pSP != NULL);
}

bool ClientEngine::SoundPlacementGetRotationRad(int soundPlacementId, float& radians) const {
	return SoundPlacementGetRotationRad(SoundPlacementGet(soundPlacementId), radians);
}

bool ClientEngine::SoundPlacementSetDirection(SoundPlacement* pSP, const Vector3f& dir) {
	return pSP ? pSP->SetDirection(dir) : false;
}

bool ClientEngine::SoundPlacementSetDirection(int soundPlacementId, const Vector3f& dir) {
	return SoundPlacementSetDirection(SoundPlacementGet(soundPlacementId), dir);
}

bool ClientEngine::SoundPlacementPlaySound(SoundPlacement* pSP, const GLID& glid) {
	if (!pSP)
		return false;

	// Load Sound (does not play)
	pSP->LoadSoundByGlid(glid);

#ifdef _ClientOutput
	// Cache UGC Sound Metadata
	// Completion event is handled by UpdateSoundUGCEventHandler() which only calls
	// pSP->SetName() updating the sound placement name from the metadata result.
	// It does not play the sound however the sound will normally be triggered to
	// play when in range of the player.
	if (IS_UGC_GLID(glid)) {
		IEvent* completionEvent = m_dispatcher.MakeEvent(m_updateSoundUGCEventId, 0);
		if (completionEvent) {
			(*completionEvent->OutBuffer()) << pSP->GetId();
			ContentService::Instance()->ContentMetadataCacheAsync(glid, completionEvent);
		}
	}
#endif

	return true;
}

bool ClientEngine::SoundPlacementPlaySound(int soundPlacementId, const GLID& glid) {
	return SoundPlacementPlaySound(SoundPlacementGet(soundPlacementId), glid);
}

bool ClientEngine::SoundPlacementUpdate(SoundPlacement* pSP) {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "SoundPlacementUpdate");

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO || !pMO->getBaseFrame())
		return false;

	// Update Sound Placement Listener With My Position, Rotation & Velocity
	Vector3f pos(0.0), rot(0.0), vel(0.0);
	GetMovementObjectPosRot(pMO, pos, rot);
	SoundPlacement::UpdateListener(pos, rot, vel);

	// Update Single Sound Placement ?
	if (pSP)
		return SoundPlacementUpdatePos(pSP, pos);

	// Update All Sound Placements (invalidated if attached object invalid)
	auto itr = m_soundPlacements.begin();
	while (itr != m_soundPlacements.end()) {
		// Sound placement can be deleted in call to SoundPlacementUpdatePos, so
		// advance iterator before calling it.
		auto itrNext = itr;
		++itrNext;
		if (itr->second)
			SoundPlacementUpdatePos(itr->second, pos);
		itr = itrNext;
	}

	return true;
}

// WARNING - Invalidates m_soundPlacements If Attached Object Invalid !!!
bool ClientEngine::SoundPlacementUpdatePos(SoundPlacement* pSP, const Vector3f& pos) {
	if (!pSP)
		return false;

	// Attached To Object ?
	bool attached = pSP->IsAttached();
	if (!attached) {
		pSP->Update(pos);
		return true;
	}

	// Attached Object Valid ?
	bool valid = SoundPlacementAttachValid(pSP);
	if (!valid) {
		SoundPlacementDel(pSP);
		return false;
	}

	// Update Sound Placement Modifiers
	SoundPlacementUpdateModifiers(pSP);

	// Attached Object Follow ?
	Vector3f posNew;
	bool follow = pSP->AttachedTargetFollow();
	if (follow && SoundPlacementAttachPosNew(pSP, posNew))
		pSP->Update(pos, &posNew);
	else
		pSP->Update(pos);

	return true;
}

bool ClientEngine::SoundPlacementAttachValid(SoundPlacement* pSP) {
	if (!pSP || !pSP->IsAttached())
		return true;

	// Get Sound Placement Attached Object Validity
	switch (pSP->AttachedTargetType()) {
		case ObjectType::MOVEMENT: {
			auto pMO = GetMovementObjByNetId(pSP->AttachedTargetId());
			return (pMO && pMO->getBaseFrame());
		} break;

		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(pSP->AttachedTargetId());
			return (pDPO != NULL);
		} break;
	}

	return true;
}

bool ClientEngine::SoundPlacementAttachPosNew(SoundPlacement* pSP, Vector3f& posNew) {
	if (!pSP || !pSP->IsAttached())
		return false;

	// Get Sound Placement Attached Object New Position
	switch (pSP->AttachedTargetType()) {
		case ObjectType::MOVEMENT: {
			auto pMO = GetMovementObjByNetId(pSP->AttachedTargetId());
			if (!pMO || !pMO->getBaseFrame())
				return false;
			posNew = pMO->getBaseFramePosition();
			return true;
		} break;

		case ObjectType::DYNAMIC: {
			auto pDPO = GetDynamicObject(pSP->AttachedTargetId());
			if (!pDPO)
				return false;
			posNew = pDPO->GetLastLocation();
			return true;
		} break;
	}

	return false;
}

bool ClientEngine::SoundPlacementPickRayHitTest(
	SoundPlacement* pSP,
	const Vector3f& rayOrigin,
	const Vector3f& rayDir,
	Vector3f& intersection,
	Vector3f& normal,
	float& distance) const {
	return pSP ? pSP->IntersectRay(rayOrigin, rayDir, &distance, &intersection, &normal) : false;
}

int ClientEngine::GetSoundOnDynamicObject(int placementId, const GLID& glid) {
	// Get Sound Placement On Dynamic Object With Matching Glid
	for (const auto& it : m_soundPlacements) {
		auto pSP = it.second;
		if (!pSP)
			continue;
		if (!pSP->IsAttachedToDynObj(placementId))
			continue;
		if (pSP->GetGlid() == glid)
			return pSP->GetId();
	}
	return 0;
}

int ClientEngine::PlaySoundOnDynamicObject(int placementId, const GLID& glid) {
	// Get Existing Sound On Dynamic Object With Matching Glid
	int soundPlacementId = GetSoundOnDynamicObject(placementId, glid);
	if (soundPlacementId == 0) {
		// Add New Sound Placement
		soundPlacementId = GetNextLocalPlacementId();
		SoundPlacementAdd(soundPlacementId);
	}

	// Get Sound Placement
	auto pSP = SoundPlacementGet(soundPlacementId);
	if (!pSP)
		return 0;

	// Attach Sound Placement To Dynamic Object (and follow)
	pSP->AttachToDynObj(placementId, true);

	// Play Sound (not looping)
	SoundPlacementPlaySound(soundPlacementId, glid);
	//pSP->SetSoundAttribute( SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_LOOP, 0 );

	// Update Sound Placement Position & Play Sound
	SoundPlacementUpdate(pSP);

	return soundPlacementId;
}

bool ClientEngine::RemoveSoundOnDynamicObject(int placementId, const GLID& glid) {
	// Remove Sound From Dynamic Object (glid=0 all sounds)
	auto soundPlacements = m_soundPlacements;
	for (const auto& it : soundPlacements) {
		auto pSP = it.second;
		if (!pSP)
			continue;
		if (!pSP->IsAttachedToDynObj(placementId))
			continue;
		if ((glid != 0) && (glid != pSP->GetGlid()))
			continue;
		auto soundPlacementId = it.first;
		SoundPlacementDel(soundPlacementId);
	}
	return true;
}

int ClientEngine::GetSoundOnPlayer(int netId, const GLID& glid) {
	// Get Movement Object (0=me)
	if (netId == 0) {
		auto pMO = GetMovementObjMe();
		if (pMO)
			netId = pMO->getNetTraceId();
	}

	// Get Sound Placement On Player With Matching Glid
	for (const auto& it : m_soundPlacements) {
		auto pSP = it.second;
		if (!pSP)
			continue;
		if (!pSP->IsAttachedToPlayer(netId))
			continue;
		if (pSP->GetGlid() == glid)
			return pSP->GetId();
	}
	return 0;
}

int ClientEngine::PlaySoundOnPlayer(int netId, const GLID& glid) {
	// Get Movement Object (0=me)
	if (netId == 0) {
		auto pMO = GetMovementObjMe();
		if (pMO)
			netId = pMO->getNetTraceId();
	}

	// Get Existing Sound On Player With Matching Glid
	int soundPlacementId = GetSoundOnPlayer(netId, glid);
	if (soundPlacementId == 0) {
		// Add New Sound Placement
		soundPlacementId = GetNextLocalPlacementId();
		SoundPlacementAdd(soundPlacementId);
	}

	// Get Sound Placement
	auto pSP = SoundPlacementGet(soundPlacementId);
	if (!pSP)
		return 0;

	// Attach Sound Placement To Player (and follow)
	pSP->AttachToPlayer(netId, true);

	// Play Sound (not looping)
	SoundPlacementPlaySound(soundPlacementId, glid);
	//pSP->SetSoundAttribute( SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_LOOP, 0 );

	// Update Sound Placement Position & Play Sound
	SoundPlacementUpdate(pSP);

	return soundPlacementId;
}

bool ClientEngine::RemoveSoundOnPlayer(int netId, const GLID& glid) {
	// Get Movement Object (0=me)
	if (netId == 0) {
		auto pMO = GetMovementObjMe();
		if (pMO)
			netId = pMO->getNetTraceId();
	}

	// Remove Sound From Player (glid=0 all sounds)
	auto soundPlacements = m_soundPlacements;
	for (const auto& it : soundPlacements) {
		auto pSP = it.second;
		if (!pSP)
			continue;
		if (!pSP->IsAttachedToPlayer(netId))
			continue;
		if ((glid != 0) && (glid != pSP->GetGlid()))
			continue;
		auto soundPlacementId = it.first;
		SoundPlacementDel(soundPlacementId);
	}
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// ED-2803 - Vehicle Sounds - Sound Modifiers
///////////////////////////////////////////////////////////////////////////////

bool ClientEngine::SoundPlacementIsMyVehicle(const SoundPlacement* pSP) const {
	return pSP ? pSP->IsAttachedToDynObj(GetMyVehiclePlacementId()) : false;
}

bool ClientEngine::SoundPlacementAddModifier(SoundPlacement* pSP, const SoundPlacement::Modifier& mod) {
	return pSP ? pSP->AddModifier(mod) : false;
}

bool ClientEngine::SoundPlacementAddModifier(int soundPlacementId, const SoundPlacement::Modifier& mod) {
	return SoundPlacementAddModifier(SoundPlacementGet(soundPlacementId), mod);
}

bool ClientEngine::SoundPlacementUpdateModifiers(SoundPlacement* pSP) {
	if (!pSP || !pSP->IsAttached())
		return false;

	// Update All Sound Placement Modifiers
	auto& modifiers = pSP->Modifiers();
	for (auto& m : modifiers)
		SoundPlacementUpdateModifier(pSP, m);

	return true;
}

bool ClientEngine::SoundPlacementUpdateModifier(SoundPlacement* pSP, SoundPlacement::Modifier& mod) {
	if (!pSP || !pSP->IsAttached())
		return false;

	// Get Modifier Input Value
	double vInput = 0.0;
	ObjectType objType = pSP->IsAttachedToDynObj() ? ObjectType::DYNAMIC : ObjectType::MOVEMENT;
	auto objId = pSP->AttachedTargetId();
	switch (mod.m_input) {
		case SoundPlacement::Modifier::Input::INPUT_SPEED: {
			if (!ObjectGetSpeed(objType, objId, vInput))
				return false;
		} break;

		case SoundPlacement::Modifier::Input::INPUT_ACCEL: {
			if (!ObjectGetAccel(objType, objId, vInput))
				return false;
		} break;

		case SoundPlacement::Modifier::Input::INPUT_THROTTLE: {
			if (!ObjectGetThrottle(objType, objId, vInput))
				return false;
		} break;

		case SoundPlacement::Modifier::Input::INPUT_SKID: {
			if (!ObjectGetSkid(objType, objId, vInput))
				return false;
		} break;
	}

	// Set Modifier Output Value (vOutput = vInput * mod.vGain * mod.vInit + mod.vOffset)
	return pSP->SetModifierOutputValue(mod, fabs(vInput));
}

} // namespace KEP