///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "common\KEPUtil\FrameTimeProfiler.h"

namespace KEP {

static LogInstance("ClientEngine");

void ClientEngine::MOS_Log() {
	LogInfo("numMOS=" << m_mosVector.size());
	for (auto& mos : m_mosVector) {
		_LogInfo(" ... " << mos.ToStr());
	}
}

bool ClientEngine::MOS_Reset() {
	m_mouseOverLastObjectType = ObjectType::NONE;
	m_mouseOver = TraceInfo();
	MOS_Sync(m_mouseOver, false);
	MOS_Clear();
	return true;
}

bool ClientEngine::MOS_Clear(const MOS::CATEGORY& mosCategory) {
	MOS::CATEGORY category = mosCategory ? mosCategory : MOS::CATEGORY_ALL;

	// Clear All Elements In Given Category
	size_t num = 0;
	if (category == MOS::CATEGORY_ALL) {
		num = m_mosVector.size();
		m_mosVector.clear();
		LogInfo("mosCategory=ALL num=" << num << " OK");
	} else {
		for (auto it = m_mosVector.begin(); it != m_mosVector.end();) {
			if ((*it).category & category) {
				it = m_mosVector.erase(it);
				++num;
			} else {
				++it;
			}
		}
		LogInfo("mosCategory=" << (void*)category << " num=" << num << " OK");
	}

	return true;
}

bool ClientEngine::MOS_Add(
	const ObjectType& objType,
	int objId,
	const MOS::MODE& mosMode,
	const std::string& toolTipText,
	const CURSOR_TYPE& cursorType,
	const MOS::CATEGORY& mosCategory) {
	// Update Existing Mouse Over Setting ?
	for (auto& mos : m_mosVector) {
		if (mos.objType != objType || mos.objId != objId)
			continue;
		mos.mode = mosMode;
		mos.toolTipText = toolTipText;
		mos.cursorType = cursorType;
		mos.category = mosCategory;
		LogInfo("UPDATE - " << mos.ToStr() << " (" << m_mosVector.size() << " settings)");
		return true;
	}

	// Add New Setting If Not Existing
	MOS mos;
	mos.objType = objType;
	mos.objId = objId;
	mos.toolTipText = toolTipText;
	mos.mode = mosMode;
	mos.cursorType = cursorType;
	mos.category = mosCategory;
	m_mosVector.push_back(mos);
	LogInfo("ADD - " << mos.ToStr() << " (" << m_mosVector.size() << " settings)");
	return true;
}

bool ClientEngine::MOS_Get(MOS& mos_out, const ObjectType& objType, int objId) const {
	for (const auto& mos : m_mosVector) {
		if (mos.objType != objType || mos.objId != objId)
			continue;
		mos_out = mos;
		return true;
	}
	return false;
}

bool ClientEngine::MOS_Sync(const TraceInfo& traceInfo, bool setToolTip) {
	// Object Change ?
	bool objChange = (traceInfo.objType != m_mos.objType) || (traceInfo.objId != m_mos.objId);
	if (objChange) {
		// Reset Mode
		if (m_mos.modeOutline())
			ObjectSetOutlineState(m_mos.objType, m_mos.objId, false);

		// Reset Tooltip
		if (!m_mos.toolTipText.empty())
			SendMouseOverToolTipEvent("");

		// Reset Mouse Over Settings
		m_mos = MOS();
	}

	// Mouse Over Setting Change ?
	MOS mos;
	if (MOS_Get(mos, traceInfo.objType, traceInfo.objId)) {
		// Set Mode
		if (m_mos.modeOutline() != mos.modeOutline())
			ObjectSetOutlineState(mos.objType, mos.objId, mos.modeOutline());

		// Set Tooltip
		if (setToolTip && (m_mos.toolTipText != mos.toolTipText))
			SendMouseOverToolTipEvent(mos.toolTipText);

		// Set Mouse Over Settings
		m_mos = mos;
		if (!setToolTip)
			m_mos.toolTipText = "";
	}

	return true;
}

bool ClientEngine::MouseOverUpdate() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "MouseOverUpdate");

	// Get Cursor Info
	D3DVIEWPORT9 viewport;
	POINT cursorPos;
	bool cursorInViewport;
	bool cursorOverBlade;
	if (!GetCursorInfo(viewport, cursorPos, cursorInViewport, cursorOverBlade)) {
		LogError("GetCursorInfo() FAILED");
		return false;
	}

	// Always update the pick ray
	GetPickRay(&m_pickRayOrigin, &m_pickRayDirection, cursorPos.x, cursorPos.y);

	// Get Current Mouse State
	bool mouseDownLeft = m_mouseState.ButtonDownLeft();
	bool mouseDownRight = m_mouseState.ButtonDownRight();

	// Don't update mouseover object while a mouse button is held down
	if (mouseDownLeft || mouseDownRight)
		return true;

	// Check for cursor position inside client area
	TraceInfo mouseOverWas = m_mouseOver;
	if (cursorInViewport) {
		TimeMs timeMs = fTime::TimeMs();
		const TimeMs MS_BETWEEN_UPDATE = 100;
		if (cursorOverBlade) {
			// Cursor is inside client area but over some client blade (i.e. menus)
			m_mouseOver = TraceInfo();
			if (m_clickPlaceData.objectGlid != 0 && (timeMs - m_clickPlaceData.timeLast) > MS_BETWEEN_UPDATE) {
				m_clickPlaceData.timeLast = timeMs;
				handleBuildPlaceMouseUpdate(m_pickRayOrigin, m_pickRayDirection, false);
			}
		} else {
			// Cursor is in client area and over 3D stuff check to see if it's over the movement widget.
			Vector3f intersection;
			int axis = m_kepWidget->mouseOverTest(m_pickRayOrigin, m_pickRayDirection, &intersection);
			if (axis == OnObject || axis == NoAxis) {
				if (m_clickPlaceData.objectGlid != 0 && (timeMs - m_clickPlaceData.timeLast) > MS_BETWEEN_UPDATE) {
					m_clickPlaceData.timeLast = timeMs;
					handleBuildPlaceMouseUpdate(m_pickRayOrigin, m_pickRayDirection, false);
				} else {
					RayTrace(&m_mouseOver, m_pickRayOrigin, m_pickRayDirection, FLT_MAX, ObjectType::ANY, RAYTRACE_VISIBLE);
				}
			} else {
				// treat as not over anything ('cos the widget is in the way)
				m_mouseOver.distance = FLT_MAX;
				m_mouseOver.objType = ObjectType::NONE;
				m_mouseOver.objId = -1;
				m_mouseOver.intersection = Vector3f();
				m_mouseOver.normal = Vector3f(0, 0, 1);
			}
		}
	} else {
		// Cursor position is outside client rectangle therefore no object is under cursor
		m_mouseOver = TraceInfo();
	}

	// Sync Mouse Over Settings (set tooltip only on timed update)
	bool objChange = (mouseOverWas.objType != m_mouseOver.objType) || (mouseOverWas.objId != m_mouseOver.objId);
	bool timedUpdate = !objChange && (m_mouseOverUpdateTimer.ElapsedMs() > 500);
	if (objChange || timedUpdate) {
		MOS_Sync(m_mouseOver, timedUpdate);
		m_mouseOverUpdateTimer.Reset();
	}

	return true;
}

} // namespace KEP