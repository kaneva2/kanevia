///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "UGC\UGCManager.h"
#include "UGC\UGCItemUploadState.h"
#include "RuntimeSkeleton.h"
#include "common\include\CoreStatic\CBoneClass.h"
#include "common\include\CoreStatic\CArmedInventoryClass.h"
#include "Common\KEPUtil\FrameTimeProfiler.h"

namespace KEP {

static LogInstance("ClientEngine");

int ClientEngine::CreateNewUGCParticleSystem() {
	GLID glid = (GLID)ParticleRM::AllocateLocalGLID();
	m_particleRM->AddStockItem("IMP_" + std::to_string(glid), glid);
	GetUGCManager()->AddUGCObj(new UGCParticleEffect(glid, m_particleRM->GetResource((UINT64)glid)));
	return glid;
}

EffectHandle ClientEngine::CreateParticleSystem(const char* effectName, Vector3f position, Quaternionf orientation) {
	auto pCPB = ClientBladeFactory::Instance()->GetClientParticleBlade();
	return pCPB ? pCPB->CreateEffect(
					  effectName,
					  position.x, position.y, position.z,
					  orientation.x, orientation.y, orientation.z, orientation.w) :
				  INVALID_EFFECT_HANDLE;
}

void ClientEngine::ParticleSystem(int bLoopLoc, Vector3f camLocation, Matrix44f matrixLoc, float fov) {
	if (!m_particleRM || !GetParticlesEnabled())
		return;

	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "ParticleUpdate");

	// Get All Particle Effects
	std::vector<UINT> allRuntimeEffectIDs;
	m_particleRM->GetAllRuntimeEffectsIds(allRuntimeEffectIDs);

	// Activate/Move All Particle Effects
	for (const auto& runtimeId : allRuntimeEffectIDs) {
		Vector3f actualCenter;
		Matrix44f tmp;
		if (m_particleRM->GetRuntimeEffectMatrix(runtimeId, tmp))
			actualCenter = tmp.GetTranslation();

		// check to see if this particle system has been created yet
		Quaternionf q;
		ConvertRotation(tmp, out(q));
		if (INVALID_EFFECT_HANDLE == m_particleRM->GetRuntimeEffectHandle(runtimeId))
			m_particleRM->ActivateRuntimeEffect(runtimeId, actualCenter, q);
		else
			m_particleRM->MoveRuntimeEffect(runtimeId, actualCenter, q);
	}
}

// Particle Callback
void ClientEngine::RuntimeParticleCleanup() {
	if (!m_particleRM)
		return;

	// Get All Particle Effects
	std::vector<UINT> allRuntimeEffectIDs;
	m_particleRM->GetAllRuntimeEffectsIds(allRuntimeEffectIDs);

	// Cleanup All Particle Effects
	for (const auto& runtimeId : allRuntimeEffectIDs) {
		int bSysOn = 0, bOn = 0, bRegen = 0;
		if (m_particleRM->GetRuntimeEffectStatus(runtimeId, &bSysOn, &bOn, &bRegen) && bSysOn) {
			if (!bOn || !bRegen) {
				POSITION posWldLast = NULL;
				for (POSITION wldPos = m_curWldObjPrtList->GetHeadPosition(); (posWldLast = wldPos) != NULL;) {
					// check world particle List and update it
					auto pWO = (CWldObject*)m_curWldObjPrtList->GetNext(wldPos);
					if (pWO && (pWO->m_runtimeParticleId == runtimeId)) {
						m_particleRM->SetRuntimeEffectStatus(pWO->m_runtimeParticleId, -1, 0, -1);
						pWO->m_runtimeParticleId = 0;
						m_curWldObjPrtList->RemoveAt(posWldLast);
					}
				}
				m_particleRM->RemoveRuntimeEffect(runtimeId);
			}
		}
	}
}

IGetSet* ClientEngine::GetParticleSystemByGlid(GLID glid) {
	return dynamic_cast<IGetSet*>(dynamic_cast<EnvParticleSystem*>(m_particleRM->GetResource((UINT64)glid)));
}

IGetSet* ClientEngine::GetParticleSystemByName(const std::string& effectName) {
	return dynamic_cast<IGetSet*>(m_particleRM->GetByName(effectName));
}

bool ClientEngine::AddParticleSystemToDynamicObject(int placementId, const std::string& effectName) {
	CDynamicPlacementObj* pDPO = GetDynamicObject(placementId);
	if (!pDPO)
		return false;

	EnvParticleSystem* pEPS = m_particleRM->GetByName(effectName);
	if (!pEPS)
		return false;

	pDPO->RemoveParticle(std::string(), 0);
	pDPO->AddParticle(std::string(), 0, INVALID_GLID, effectName, Vector3f(0, 0, 0), Vector3f(0, 0, 1), Vector3f(0, 1, 0));

	return true;
}

bool ClientEngine::GetDynamicObjectParticleSystem(int placementId, GLID& glid, std::string& effectName) {
	// Reset Return Values
	glid = GLID_INVALID;
	effectName = "";

	CDynamicPlacementObj* pDPO = GetDynamicObject(placementId);
	if (!pDPO)
		return false;

	return pDPO->GetParticleGlidAndName(std::string(), 0, glid, effectName);
}

void ClientEngine::RemoveParticleSystemFromDynamicObject(int placementId) {
	CDynamicPlacementObj* pDPO = GetDynamicObject(placementId);
	if (!pDPO)
		return;
	pDPO->RemoveParticle(std::string(), 0);
}

BOOL ClientEngine::PlayParticleEffectOnPlayer(int netId, const std::string& boneName, int particleSlot, UINT particleGlid) {
	auto pMO = GetMovementObjByNetId(netId);
	if (!pMO || !pMO->getSkeleton())
		return FALSE;

	Vector3f dir = { 0.0f, 0.0f, 1.0f };
	Vector3f up = { 0.0f, 1.0f, 0.0f };
	Vector3f offset = { 0.0f, 0.0f, 0.0f };
	return ObjectAddParticleAsync(ObjectType::MOVEMENT, netId, boneName, particleSlot, particleGlid, offset, dir, up);
}

void ClientEngine::SkeletalIsClippedStopParticles(RuntimeSkeleton* pRS) {
	if (!pRS->hasParticles())
		return;

	pRS->iterateParticleEmitters([this](CParticleEmitRangeObj* pPERO) {
		if (pPERO->getRuntimeParticleId() != 0) {
			m_particleRM->SetRuntimeEffectStatus(pPERO->getRuntimeParticleId(), -1, -1, 0);
			pPERO->setRuntimeParticleId(0);
			pPERO->setInRange(false);
		}
	});
}

void ClientEngine::EntityIsClippedStopParticles(CMovementObj* pMO) {
	if (pMO->getSkeleton())
		SkeletalIsClippedStopParticles(pMO->getSkeleton());

	std::vector<CArmedInventoryObj*> allEquips;
	pMO->getSkeleton()->getAllEquippableItems(allEquips);

	for (auto pAIO : allEquips) {
		RuntimeSkeleton* pRS = pAIO->getSkeleton(eVisualType::ThirdPerson);
		if (pRS != nullptr)
			SkeletalIsClippedStopParticles(pRS);

		pRS = pAIO->getSkeleton(eVisualType::FirstPerson);
		if (pRS != nullptr)
			SkeletalIsClippedStopParticles(pRS);
	}
}

void ClientEngine::LoadParticleDatabase(const std::string& fileName) {
	if (!m_particleRM)
		return;

	// use gamefiles folder for organization
	ScopedDirectoryChanger sdc("GameFiles\\");

	// see if this definition belongs to any external plugins first
	auto pCPB = ClientBladeFactory::Instance()->GetClientParticleBlade();

	DestroyAllParticleInterfaces();

	std::string idParticleRM;
	StrBuild(idParticleRM, "particleRM_" << fileName);

	if (pCPB) {
		pCPB->LoadParticleDefinitions(fileName.c_str());
		m_particleRM = new ParticleRM(idParticleRM);
		m_particleRM->SetBladeProvider(typeid(*pCPB).name());

		for (size_t i = 0; i < pCPB->GetNumEffects(); i++) {
			m_particleRM->AddStockItem(pCPB->GetEffectName(i), GLID_INVALID);
		}
	} else {
		// we're no longer supporting the legacy particle systems
		m_particleRM = new ParticleRM(idParticleRM);
		m_particleRM->SetBladeProvider("");
	}
}

void ClientEngine::SkeletalParticleEmitterCallback(const Matrix44f& positionalMatrix, RuntimeSkeleton* pRS, bool killExisting) {
	if (!pRS || !pRS->hasParticles())
		return;

	auto animGlid1 = pRS->getAnimInUseGlid(ANIM_PRI);
	auto animGlid2 = pRS->getAnimInUseGlid(ANIM_SEC);

	auto pSAH1 = pRS->getSkeletonAnimHeaderByGlid(animGlid1);
	auto pSAH2 = pRS->getSkeletonAnimHeaderByGlid(animGlid2);

	pRS->iterateParticleEmitters([this, pRS, killExisting, positionalMatrix, pSAH1, pSAH2](CParticleEmitRangeObj* pPERO) {
		if (!killExisting) {
			bool bTriggerPri = false;
			bool bTriggerSec = false;
			if (pPERO->getAnimationGLID() == GLID_INVALID && pPERO->getAnimationType() == eAnimType::None) {
				bTriggerPri = true;
			} else {
				if (pSAH2 && pPERO->triggerOnAnimation(pSAH2->m_animType, pSAH2->m_animVer, pSAH2->m_animGlid))
					bTriggerPri = bTriggerSec = true;

				if (!bTriggerPri && pSAH1 && pPERO->triggerOnAnimation(pSAH1->m_animType, pSAH1->m_animVer, pSAH1->m_animGlid))
					bTriggerPri = true;
			}

			if (bTriggerPri) {
				// if we are not told to kill the existing systems
				// and if we are playing the correct animation

				const CBoneObject* pBO = nullptr;
				if (pPERO->getBoneIndex() != -1) {
					POSITION bnPos = pRS->getBoneList()->FindIndex(pPERO->getBoneIndex());
					if (!bnPos)
						return;

					pBO = static_cast<const CBoneObject*>(pRS->getBoneList()->GetAt(bnPos));
				} else {
					// Not attached to any bone, use root matrix
				}

				if (!pPERO->useExactRange()) {
					// just activate by animation
					if (!pPERO->inRange()) {
						EnvParticleSystem* pSys = (pPERO->getParticleGLID() != GLID_INVALID) ?
													  m_particleRM->GetByGLID(pPERO->getParticleGLID(), true) :
													  !pPERO->getParticleSystemName().empty() ? m_particleRM->GetByName(pPERO->getParticleSystemName()) : m_particleRM->GetStockItemByIndex(pPERO->getParticleDBIndex());

						if (pSys) {
							if (pPERO->getRuntimeParticleId() != 0) {
								m_particleRM->SetRuntimeEffectStatus(pPERO->getRuntimeParticleId(), -1, 0, 0);
								pPERO->setRuntimeParticleId(0); // do not delete::self deleting
							}

							unsigned runtimeParticleId = m_particleRM->AddRuntimeEffect(pSys->m_systemName);
							m_particleRM->SetRuntimeEffectStatus(runtimeParticleId, 1, -1, -1);
							pPERO->setRuntimeParticleId(runtimeParticleId);
						}

						pPERO->setInRange(true);
					}
				} else {
					// activate by exact range
					if (!pPERO->inRange()) {
						// check range
						TimeMs timeMs = (TimeMs)pPERO->getStartRange();
						if (RuntimeSkeleton::AnimInUseTimeElapsedIsPast(timeMs, pRS, bTriggerSec)) {
							// requires init
							EnvParticleSystem* pSys = NULL;
							if (pPERO->getParticleGLID() != 0) {
								pSys = m_particleRM->GetByGLID(pPERO->getParticleGLID(), false);
							} else {
								pSys = m_particleRM->GetStockItemByIndex(pPERO->getParticleDBIndex());
							}

							if (pSys) {
								// ref db
								if (pPERO->getRuntimeParticleId()) {
									m_particleRM->SetRuntimeEffectStatus(pPERO->getRuntimeParticleId(), -1, 0, 0);
									pPERO->setRuntimeParticleId(0); // do not delete::self deleting
								}

								unsigned runtimeParticleId = m_particleRM->AddRuntimeEffect(pSys->m_systemName);
								m_particleRM->SetRuntimeEffectStatus(runtimeParticleId, 1, -1, -1);
								pPERO->setRuntimeParticleId(runtimeParticleId);
							} // end ref db

							pPERO->setInRange(true);
						}
					} else {
						// out of range -> kill it
						pPERO->setInRange(false);
						if (pPERO->getRuntimeParticleId()) {
							m_particleRM->SetRuntimeEffectStatus(pPERO->getRuntimeParticleId(), -1, 0, 0);
							pPERO->setRuntimeParticleId(0); // do not delete::self deleting
						}
					}
				} // end activate by exact range

				// update emitter's transform
				if (pPERO->getRuntimeParticleId()) {
					Matrix44f offsetFromSkeletonRoot;
					if (pBO != nullptr) {
						Matrix44f boneMatrix;
						pBO->m_boneFrame->GetTransform(NULL, &boneMatrix);

						offsetFromSkeletonRoot = pPERO->getMatrix() * boneMatrix;
					} else {
						//No bone specified
						offsetFromSkeletonRoot = pPERO->getMatrix();
					}

					Matrix44f genMatrix;
					genMatrix = offsetFromSkeletonRoot * positionalMatrix;
					m_particleRM->SetRuntimeEffectMatrix(pPERO->getRuntimeParticleId(), genMatrix);
				}
			} else if (pPERO->inRange()) {
				// we are playing a different animation, stop emitting particles
				pPERO->setInRange(false);
				if (pPERO->getRuntimeParticleId() != 0) {
					m_particleRM->SetRuntimeEffectStatus(pPERO->getRuntimeParticleId(), -1, 0, 0);
					pPERO->setRuntimeParticleId(0); // do not delete::self deleting
				}
			}
		}
	});
}

} // namespace KEP