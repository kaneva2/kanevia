///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "ClientEngine_CmdLineInfo.h"
#include "resource.h"
#include "config.h"
#include "NetworkCfg.h"
#include "CSkeletonObject.h"
#include "RuntimeSkeleton.h"
#include "SkeletonConfiguration.h"
#include "SkeletonManager.h"
#include "EquippableSystem.h"
#include "CDeformableMesh.h"
#include "CBoneClass.h"
#include "CBoneAnimationClass.h"
#include "rmxftmpl.h"
#include "CPluginTransfer.h"
#include "UrlParser.h"
#include "ActorDatabase.h"
#include "CIconSystemClass.h"
#include "RenderEngine/Utils/ReUtils.h"
#include "UseTypes.h"
#include "SlashCommand.h"
#include "ViewportClass.h"
#include "iclientblade.h"
#include "iclientrenderblade.h"
#include "iclientparticleblade.h"
#include "clientbladefactory.h"
#include "legacymodelproxy.h"
#include "KEPConfig.h"
#include "Engine/Blades/BladeStrings.h"
#include "KEPException.h"
#include "CElementStatusClass.h"
#include "Common\KEPUtil\Application.h"
#include "Client\KEPFile.h"
#include "Client\ContentServiceGLIDCacheTask.h"
#include "Client\EffectTrackExecutors.h"
#include "CMultiplayerClass.h"
#include <SerializeHelper.h>
#include "DynamicObPlacementModelProxy.h"
#include "PlayerModelProxy.h"
#include "ContentService.h"
#include "TextureProvider.h"
#include "WebCallTask.h"
#include "HashUtil.h"
#include <PassList.h>
#include <Rpc.h>
#include "ReMeshCreate.h"
#include "SkeletalAnimationManager.h"
#include "TextureManager.h"
#include "DownloadManager.h"
#include "UploadManager.h"
#include "AnimationProxy.h"
#include "MeshProxy.h"
#include "SkeletonProxy.h"
#include "DynamicObjectManager.h"
#include "ReD3DX9.h"
#include "ReDynamicGeom.h"
#include "ExternalLuac.h"
#include "UGC/UGCManager.h"
#include "UGC/UGCItemUploadState.h"
#include "UGC/UGCImport.h"
#include "AssetsDatabase/TextureImporter.h"
#include "TextureDef.h"
#include "LoadingProgressDlg.h"
#include "WokwebHelper.h"
#include "LoadingScreenRAIIHelper.h"
#include <iphlpapi.h>
#include <icmpapi.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <al.h>
#include <alc.h>
#include "UnicodeMFCHelpers.h"
#include "UnicodeWindowsHelpers.h"
#include "PreciseTime.h"
#include "DownloadManager.h"
#include "DecryptingFileFactory.h"
#include "FlashEXMesh.h"
#if (DRF_DIRECT_SHOW_MESH == 1)
#include "DirectShowEXMesh.h"
#endif
#include "SetThreadName.h"
#include "AssetsDatabase.h"
#include "DropTargetProxy_Win32.h"
#include "LightweightGeom.h"
#include "UgcConstants.h"
#include "SkeletonLabels.h"
#include "InputSystem.h"
#include "CompressHelper.h"
#include "ResourceDownloadTask.h"
#include "DownloadPriorityDistanceModel.h"
#include "PhysicsUserData.h"
#include "Core/Math/Rotation.h"
#include "Core/Math/Units.h"
#include "Core/Math/TransformUtil.h"
#include "Engine/ClientBase/OldPhysics.h"
#include "KEPPhysics/PhysicsFactory.h"
#include "KEPPhysics/DebugWindow.h"
#include "KEPPhysics/RigidBodyStatic.h"
#include "KEPPhysics/Universe.h"
#include "KEPPhysics/Vehicle.h"
#include "Common/KEPUtil/FrameTimeProfiler.h"
#include "clientbladefactory.h"
#include <Core/Util/CallbackSystem.h>
#include "OffscreenRenderTask.h"
#include "boost/format.hpp"
#include "common/include/IMemSizeGadget.h"
#include "ResourceLoader.h"
#include "LocaleStrings.h"

#define TRAY_3D_NOTIFICATION_MSG_NAME L"msgKanevaTray3DNotification"
#define KEPCLIENT_SERVICE_MSG_NAME L"msgKEPClientService"

#pragma comment(lib, "IPHLPAPI.lib")
#pragma comment(lib, "Rpcrt4.lib")

namespace KEP {

static LogInstance("Instance");

extern IAssetImporter* g_pColladaImporter;
extern IAssetImporter* createTextureImporter();

ClientEngine::ClientEngine() :
		ClientBaseEngine(eEngineType::Client, PackVersion(1, 0, 0, 0), "ClientEngine"),
		m_zoneLoadedEventId(BAD_EVENT_ID),
		m_entityCollisionEventId(BAD_EVENT_ID),
		m_rebirthEventId(BAD_EVENT_ID),
		m_browserPageReadyEventId(BAD_EVENT_ID),
		m_selectEventId(BAD_EVENT_ID),
		m_clickEventId(BAD_EVENT_ID),
		m_mouseOverEventId(BAD_EVENT_ID),
		m_widgetTranslationEventId(BAD_EVENT_ID),
		m_widgetRotationEventId(BAD_EVENT_ID),
		m_getItemAnimationsId(BAD_EVENT_ID),
		m_zoneLoadingStartEventId(BAD_EVENT_ID),
		m_zoneLoadingCompleteEventId(BAD_EVENT_ID),
		m_zoneLoadingFailedEventId(BAD_EVENT_ID),
		m_itemsMetadataResultEventId(BAD_EVENT_ID),
		m_3DAppAssetDownloadCompleteEventId(BAD_EVENT_ID),
		m_3DAppAssetPublishCompleteEventId(BAD_EVENT_ID),
		m_3DAppAssetPublishDeleteCompleteEventId(BAD_EVENT_ID),
		m_3DAppAssetUploadNameConflictEventId(BAD_EVENT_ID),
		m_smartCloseMenuEventId(BAD_EVENT_ID),
		m_smartYesNoMenuEventId(BAD_EVENT_ID),
		m_smartKeyListenerEventId(BAD_EVENT_ID),
		m_smartTextTimerEventId(BAD_EVENT_ID),
		m_smartButtonEventId(BAD_EVENT_ID),
		m_smartStatusEventId(BAD_EVENT_ID),
		m_smartMoveMenuEventId(BAD_EVENT_ID),
		m_smartListBoxMenuEventId(BAD_EVENT_ID),
		m_smartCoinHUDEventId(BAD_EVENT_ID),
		m_smartHealthHUDEventId(BAD_EVENT_ID),
		m_smartXPHUDEventId(BAD_EVENT_ID),
		m_addHealthIndicatorEventId(BAD_EVENT_ID),
		m_removeHealthIndicatorEventId(BAD_EVENT_ID),
		m_openShopMenuEventId(BAD_EVENT_ID),
		m_addItemToShopEventId(BAD_EVENT_ID),
		m_showStatusInfoEventId(BAD_EVENT_ID),
		m_mouseOverToolTipEventId(BAD_EVENT_ID),
		m_addIndicatorEventId(BAD_EVENT_ID),
		m_clearObjectIndicatorsEventId(BAD_EVENT_ID),
		m_reconnectEvent(nullptr),
		m_commandlineOverrideIPPort(false),
		m_gotoplayeronstartup(false),
		m_tryOnOnStartup(false),
		m_tryOnGlid(0),
		m_tryOnClothingGlid(0),
		m_tryonUseType(USE_TYPE_INVALID),
		m_tryOnObjPlacementId(0),
		m_downloadZoneEventId(BAD_EVENT_ID),
		m_downloadLooseTextureEventId(BAD_EVENT_ID),
		m_requestSoundItemDataId(BAD_EVENT_ID),
		m_downloadActionItemEventId(BAD_EVENT_ID),
		m_prevImgAvailableEventId(BAD_EVENT_ID),
		m_clientBackInViewEventId(BAD_EVENT_ID),
		m_gotoapartmentonstartup(false),
		m_gotochannelonstartup(false),
		m_gotoonstartupId(0),
		m_loginUrl(DEFAULT_LOGIN_URL),
		m_runtimeReportUrl(DEFAULT_RUNTIME_REPORT_URL), // drf - added
		m_startAppUrl(DEFAULT_START_APP_URL),
		m_allowAccessUrl(DEFAULT_ALLOW_ACCESS_URL),
		m_authUrl(DEFAULT_AUTH_URL),
		m_imageUrl(DEFAULT_IMAGE_URL),
		m_imageThumbUrl(DEFAULT_IMAGE_THUMB_URL),
		m_friendUrl(DEFAULT_FRIEND_URL),
		m_friendThumbUrl(DEFAULT_FRIEND_THUMB_URL),
		m_giftUrl(DEFAULT_GIFT_URL),
		m_giftThumbUrl(DEFAULT_GIFT_THUMB_URL),
		m_imageServerUrl(DEFAULT_IMAGESERVER_URL),
		m_inventoryServerUrl(DEFAULT_INVENTORYSERVER_URL),
		m_zoneCutomizationsUrl(DEFAULT_ZONECUSTOM_URL),
		m_errorReportUrl(DEFAULT_ERROR_URL),
		m_childGameWebCallUrl(DEFAULT_CHILDGAMEWEBCALLS_URL),
		m_originalCharacterCount(0),
		m_zoneInstanceId(0),
		m_meshRM(nullptr),
		m_animationRM(nullptr),
		m_textureRM(nullptr),
		m_gameId(0),
		m_parentGameId(0),
		m_prevGameId(0),
		m_pDM(nullptr),
		m_pUM(nullptr),
		m_dropTargetProxy(nullptr),
		m_bDragDropRegistered(false),
		m_currDropType(DROP_NONE),
		m_localPlacementId(0),
		m_mouseNormalizedX(0.0f),
		m_mouseNormalizedY(0.0f),
		m_cameraCollision(true),
		m_cameraMoveSpeed(1.0),
		m_cameraZoomSpeed(1.0),
		m_cameraLock(false),
		m_rolloverMode(0),
		m_mouseOverLastObjectType(ObjectType::NONE),
		m_typeOnSize(~SIZE_RESTORED),
		m_hrTCL(-1),
		m_inView(false),
		m_goodSubBox(nullptr),
		m_selectionLocked(FALSE),
		m_smallUGCPopOutDist(30.0f),
		m_mediumUGCPopOutDist(60.0f),
		m_renderSoundPlacements(false),
		m_renderDynamicTriggers(false),
		m_renderWorldTriggers(false),
		m_renderBoundingBoxForAllDynamicObjects(false),
		m_renderMediaTriggers(false),
		m_startingUp(true),
		m_closeOnStartup(false),
		m_pWin32ProgDlg(nullptr),
		m_progressUpdateTotal(0),
		m_progressUpdateCurrent(0),
		m_labelsOnTop(true),
		m_dispatchTimeInGame(8),
		m_dispatchTimeLoading(1000),
		m_legacyDispatchTimeMode(false),
		m_userId(0),
		m_wmKEPClientService(0),
		m_mainThreadId(GetCurrentThreadId()),
		m_legacyAppPatch(true),
		m_cursorModeObject(false),
		m_menuEventOccured(false),
		m_widgetEventOccured(false),
		m_patchUrl(""),
		m_zoneAllowsLabels(true),
		m_clientResizeEventId(BAD_EVENT_ID),
		m_titleSetEventId(BAD_EVENT_ID),
		m_eventQueueProbeEventId(BAD_EVENT_ID),
		m_cursorInViewportWas(false),
		m_activated(false),
		m_mediaDupeObjId(0),
		m_masterVolPct(100.0),
		m_mediaVolPct(100.0),
		m_pInputSystem(std::make_unique<InputSystem>(eKeyCodeType::WindowsMessage)),
		m_eventQueueStatsCounter(0),
		m_eventQueueStatsInterval(0),
		m_eventQueueStatsTime(0),
		m_eventQueueStatsLogger(Logger::getInstance(L"STATS.EventQueue")),
		m_frameTimeProfiler(GetFrameTimeProfiler()),
		m_frameTimeProfilerEnabled(false),
		m_bIsDirectInputMouseProbeAllowed(true),
		m_loginCount(0),
		m_SATRunning(false),
		m_taskSystem_WebCall("WebCall"),
		m_taskSystem_ContentService("ContentService"),
		m_runtimeId(""),
		m_captureScreen(false) {
	// Resource Managers
	m_textureRM = new TextureRM("textureRM");
	m_meshRM = new MeshRM("meshRM");
	m_animationRM = new AnimationRM("animationRM");
	m_skeletonRM = new SkeletonRM("skeletonRM");
	m_soundRM = new SoundRM("soundRM");

	// DRF - Forward Test Group To Whoever Needs It
	auto testGroup = AppTestGroup();
	EventMap::SetTestGroup(testGroup);

	// moved here after log initialized to avoid losing err msgs
	ClientEngine_CFrameWnd::Init();

	Init();

	// DRF - Parallels VM Fix ?
	GpuInfo gpuInfo;
	GetGpuInfo(gpuInfo);
	bool vmFixGpu = STLBeginWith(gpuInfo.m_gpuName, "Parallels");
	bool vmFixTestGroup = ((AppTestGroup() & DRF_TEST_GROUP_VM_FIX) != 0);
	m_vmFix = (vmFixGpu || vmFixTestGroup);
	if (m_vmFix) {
		LogWarn("PARALLELS VM FIX");

		// DRF - Parallels VM Fix - Camera movement is too fast and zoom too slow
		m_cameraMoveSpeed = 0.025;
		m_cameraZoomSpeed = 4.0;
	}

	m_wmKEPClientService = RegisterWindowMessageW(KEPCLIENT_SERVICE_MSG_NAME);
	if (m_wmKEPClientService == 0)
		LogError("Register KEPClient service message failed, err=" << GetLastError());

	InTryOnState(false);

	ZeroMemory(m_keyboardPresses, sizeof(m_keyboardPresses));

	m_pGlowTEX[0] = nullptr;
	m_pGlowTEX[1] = nullptr;

	m_DOSpawnMonitor = new DynamicObjectSpawnMonitor();

	//Ankit ----- populating map for functions related to dev options
	mapForDevOptionFunctions.insert(std::make_pair("CrashFlashProcess", std::bind(&ClientEngine::CrashFlashProcess, this)));
	mapForDevOptionFunctions.insert(std::make_pair("CopyWorldStatistics", std::bind(&ClientEngine::CopyStatsToClipboard, this)));
}

// DRF - Never Called!
ClientEngine::~ClientEngine() {
#if (DRF_QUICK_QUIT_CLEANUP == 1)
	if (m_pDM)
		delete m_pDM;
	if (m_pUM)
		delete m_pUM;
#endif
}

bool ClientEngine::Startup() {
	LogInfo("START");

	ZeroMemory(&m_devcaps, sizeof(m_devcaps));

	if (GetD3DDevice())
		GetD3DDevice()->GetDeviceCaps(&m_devcaps);

	OutlinesDeviceInit();

	RegisterWellKnownEvents(&m_dispatcher);

	//	Init the dynamic draw system
	m_dynamicGeom = new ReDynamicGeom();
	m_dynamicGeom->Init(m_curModeRect, g_pD3dDevice, PathBase());

	m_multiplayerObj = new CMultiplayerObj("ClientEngine");
	m_multiplayerObj->SetPathBase(PathApp());
	m_multiplayerObj->SetDispatcher(&m_dispatcher);

	//	Set resource paths
	IResource::SetBaseTexturePath(eTexturePath::MapsModels, PathMapsModels());
	IResource::SetBaseTexturePath(eTexturePath::CustomTexture, PathCustomTexture());
	IResource::SetBaseAnimPath(eAnimPath::CharacterAnimations, PathCharacterAnimations());
	IResource::SetBaseMeshPath(eMeshPath::CharacterMeshes, PathCharacterMeshes());
	IResource::mBaseDolPath[0] = PathDynamicObjects();
	IResource::mBaseEqpPath[0] = PathEquippableItems();
	IResource::mBaseSoundPath[0] = PathSounds();
	EnvParticleSystem::mResourceStore = PathParticles();

	// Load Loose Resource File Info (*\textureInfo.dat)
	if (m_dynamicObjectRM)
		m_dynamicObjectRM->loadLooseFileInfo();

	if (m_animationRM)
		m_animationRM->loadLooseFileInfo();

	if (m_equippableRM)
		m_equippableRM->loadLooseFileInfo();

	// Register Drag & Drop
	if (m_bDragDropRegistered) {
		LogError("DragDrop Already Registered");
	} else if (!m_dropTargetProxy) {
		LogError("DragDrop dropTargetProxy null");
	} else {
		if (CoLockObjectExternal(m_dropTargetProxy, TRUE, FALSE) == S_OK) {
			if (m_hWnd && IsWindow(m_hWnd)) {
				HRESULT res = RegisterDragDrop(m_hWnd, m_dropTargetProxy);
				if (res != S_OK) {
					LogError("RegisterDragDrop() FAILED - res=" << res);
					CoLockObjectExternal(m_dropTargetProxy, FALSE, TRUE);
				} else {
					LogInfo("DragDrop Registered OK");
					m_bDragDropRegistered = true;
				}
			} else {
				LogError("DragDrop IsWindow() FAILED");
			}
		} else {
			LogError("DragDrop CoLockObjectExternal() FAILED");
		}
	}

	InitEvents();

	// DRF - Perform Client Blade Pre-Initialization (before both client and script blades are initialized)
	if (!ClientBladeFactory::Instance()->PreInitClientBlades()) {
		LogError("PreInitClientBlades() FAILED");
		return false;
	}

	// DRF - Perform Client Blade Initialization
	if (!ClientBladeFactory::Instance()->InitClientBlades()) {
		LogError("InitClientBlades() FAILED");
		return false;
	}

	// DRF - Perform Client Blade Post-Initialization (after both client and script blades are initialized)
	if (!ClientBladeFactory::Instance()->PostInitClientBlades()) {
		LogError("PostInitClientBlades() FAILED");
		return false;
	}

	// DRF - Perform Script Blade Post-Initialization (after both client and script blades are initialized)
	if (!PostInitScriptBlades()) {
		LogError("PreInitScriptBlades() FAILED");
		return false;
	}

	// Display startup progress dialog
	m_pWin32ProgDlg = new CLoadingProgressDlg(this);
	m_pWin32ProgDlg->Create(CLoadingProgressDlg::IDD);

	// Initialize client window to black.
	CBrush br(COLORREF(0));
	CRect rc;
	GetClientRect(&rc);
	CDC* pDC = GetDC();
	pDC->FillRect(&rc, &br);
	ReleaseDC(pDC);

	SIZE sz = getClientRectSize();
	CRect r;
	m_pWin32ProgDlg->GetWindowRect(r);

	m_pWin32ProgDlg->SetWindowPos(nullptr, (sz.cx - r.Width()) / 2, (sz.cy * 1.6 - r.Height()) / 2, r.Width(), r.Height(), SWP_SHOWWINDOW);

	// Get 'My Documents' Folder
	wchar_t progFilesPathW[_MAX_PATH];
	if (FAILED(SHGetFolderPathW(AfxGetMainWnd()->m_hWnd, CSIDL_PERSONAL | CSIDL_FLAG_CREATE, nullptr, 0, progFilesPathW))) {
		wcscpy_s(progFilesPathW, _MAX_PATH, L"C:\\");
		LogError("SHGetFolderPath(MyDocuments) FAILED - Using 'c:\\'");
	}

	// Create Local Developer Folder (c:\Users\<user>\Documents\My 3D Apps\LocalDev)
	std::string progFilesPath = Utf16ToUtf8(progFilesPathW);
	m_localDevBaseDir = PathAdd(progFilesPath, "My 3D Apps\\LocalDev");
	if (!FileHelper::CreateFolderDeep(m_localDevBaseDir)) {
		LogError("CreateFolderDeep() FAILED - '" << m_localDevBaseDir << "'");
	}

	// Load Local Cache Of Custom Textures
	ContentService::Instance()->LocalCacheFileLoad();

	LogInfo("END");
	return true;
}

bool ClientEngine::Shutdown() {
	LogFatal("START");

	// Set Shutdown Flag
	m_shutdown = true;

	// Normal Shutdown - Set State Stopped
	AppSetStateStopped();

	// Save Local Content Metadata Cache
	ContentService::Instance()->LocalCacheFileSave();

	// ED-5754 - DRF - Report Messaging Metrics
	auto msgMetricStr = m_multiplayerObj->GetMsgMetrics(RuntimeReporter::RuntimeId());
	LogInfo("MsgMetrics...\n"
			<< msgMetricStr);

	// ED-7201 - DRF - Report Compression Metrics
	auto cmpMetricStr = GetCompressMetrics(RuntimeReporter::RuntimeId());
	LogInfo("CmpMetrics...\n"
			<< cmpMetricStr);

	// Close Network Connection
	if (m_multiplayerObj)
		m_multiplayerObj->DisableClient(false);

	// Call All Menus OnExit()
	MenuExitAll();

	// Close All Menus Allowing Them To Save State
	MenuCloseAll();

	// Stop All Media (and wait, avoids crashes)
	StopAllMedia(true);

	// Shutdown All Resource Managers
	ShutdownResourceManagers();

	// Wait For Runtime Report Response Complete
	RuntimeReporter::WaitForReportResponseComplete();

	// Disable Crash Reporting
	CrashReporter::Enable(false);

	CoUninitialize();

	// Avoid The Destructor Mess
	LogFatal("END");
	fTime::SleepMs(500.0); // give metrics time to go
	::ExitProcess(0); // never returns
	return true;
}

void ClientEngine::Init() {
	// Set Engine Base Path Same As Client Application Path '...\Star\3296\'
	SetPathBase(PathApp(), false);

	// DRF - Added - Cleanup Import Folder 'MapsModels\imported'
	FileHelper::DeleteFolder(PathMapsModelsImported());

	ConfigureAudio();

	m_dispatcher.Initialize();
	m_dispatcher.SetGame(this);

	m_taskSystem_WebCall.Initialize(m_taskThreads_WebCallTask);
	m_taskSystem_ContentService.Initialize(m_taskThreads_ContentService);

	// Default texture quality selections (5% JPG -> DDS)
	ResetTextureQualityLevels();
	EnableTextureQualityLevel(TEX_LOW); // Currently map to 5% JPG
	EnableTextureQualityLevel(TEX_DDS);

	m_shutdown = false;

	m_dynamicGeom = nullptr;

	m_doMouseKeyboardUpdate = TRUE;

	m_floraObjTemp = new CFloraObj();

	m_globalWorldShader = -1;

	m_interactStr = "";

	m_durationToServer = 2500;
	m_durationToServerStamp = fTime::TimeMs();
	m_paused = FALSE;
	m_triggerDelayStamp = fTime::TimeMs();
	m_showAllNames = TRUE;
	m_controlTimeStamp = fTime::TimeMs();
	m_respawnTimeStamp = fTime::TimeMs();
	m_respawnWait = 5000;
	m_respawning = FALSE;
	ResetMovementRestriction();
	m_pitchLock = false;
	m_startCamera = -1;

	m_clickHandlerStamp = fTime::TimeMs();

	ZeroMemory(m_groupFilterArray, (sizeof(int) * 100)); // maximum group filters
	m_groupFilterCount = 0;

	m_readoutMaxMeasuredLength = .5f;

	m_scriptDir = PathAdd(PathGameFiles(), "ClientScripts");

	m_readoutFontX = m_pTextReadoutObj->m_cgFontSizeX;
	m_readoutFontY = m_pTextReadoutObj->m_cgFontSizeY;
	m_readoutFontSpacing = m_pTextReadoutObj->m_cgVertSpacing;

	m_multiplayerObj = nullptr;
	m_bRenderEnvironment = TRUE;

	m_mouseHiResX = 0.0f;
	m_mouseHiResY = 0.0f;

	m_invertMouse = TRUE;

	m_entityCollisionOn = TRUE;

	m_backgroundColorGlobal = D3DCOLOR_XRGB(0, 0, 0);
	m_backgroundColorBlack = D3DCOLOR_XRGB(0, 0, 0);

	m_loadScene = false;
	m_loadSceneExgFileName = "NONE";
	m_bMenuSelectModeOn = FALSE;
	m_selectionModeOn = FALSE;
	m_selectedLmp = 0;
	m_closeDownCall = false;

	m_mirrorTransform.MakeIdentity();

	m_globalTraceNumber = 1;

	SetZoneFileName("NONE");

	m_scriptList = nullptr;
	m_scenePersistList = nullptr;
	m_pDirectSound = nullptr;
#if (DRF_WORLD_ENVIRONMENT_LIST == 1)
	m_worldEnvironmentList = nullptr;
#endif
	m_runtimeViewportDB = nullptr;
	m_pViewportList = nullptr;
	m_pReDeviceState = nullptr;
	m_backgroundList = nullptr;
	m_pCameraList = nullptr;
	m_pRuntimeCameras = nullptr;
	m_controlsList = nullptr;
	m_pendingControlSystem = nullptr;
	m_pTextureDB = nullptr;
	m_sceneScriptExec = nullptr;
#if (DRF_WORLD_MIRROR_LIST == 1)
	m_worldMirrorList = nullptr;
#endif
	m_spawnGeneratorDB = nullptr;
	m_visualsRenderList = nullptr;
	m_textRenderList = nullptr;
	m_pMenuGameObjects = nullptr;
	m_menuList = nullptr;
	m_translucentPolyList = nullptr;
	m_lightningSystem = nullptr;
	m_pRenderStateMgr = nullptr;
	m_renderContainerList = nullptr;
	m_soundManager = nullptr;
	m_runtimeLightSys = nullptr;
	m_statDatabase = nullptr;
	m_shadowObjectsList = nullptr;

#if (DRF_WORLD_ENVIRONMENT_LIST == 1)
	m_worldEnvironmentList = new CWldObjectList;
#endif
	m_runtimeViewportDB = new CRuntimeViewportList();
	m_pViewportList = new CViewportList;
	m_pReDeviceState = new ReD3DX9DeviceState;
	m_backgroundList = new CBackGroundObjList;
	m_pCameraList = new CCameraObjList;
	m_pRuntimeCameras = new RuntimeCameras();
	m_controlsList = new ControlDBList;
	m_pendingControlSystem = new CPendingControlSystem();
	m_pTextureDB = new TextureDatabase();
	m_sceneScriptExec = new CEXScriptObjList;
#if (DRF_WORLD_MIRROR_LIST == 1)
	m_worldMirrorList = new CWldObjectList;
#endif
	m_spawnGeneratorDB = new CSpawnObjList;
	m_visualsRenderList = new CEXMeshObjList;
	m_textRenderList = new CTextRenderObjList;
	m_pMenuGameObjects = CMenuGameObjects::GetInstance();
	m_menuList = CMenuList::GetInstance();
	m_translucentPolyList = new CTranslucentPolyList();
	m_shadowObjectsList = new CTranslucentPolyList();
	m_lightningSystem = new CLightningObjList();
	m_pRenderStateMgr = new CStateManagementObj(PathBase().c_str(), m_shaderSystem, m_pxShaderSystem);
	m_renderContainerList = new CTranslucentPolyList();
	m_renderContainerAlphaFilterList = new CTranslucentPolyList();
	m_soundManager = new CSoundBankList();
	m_pendingObject = new CPendingCommandObjList();
	if (!m_globalInventoryDB)
		m_globalInventoryDB = CGlobalInventoryObjList::GetInstance();
	m_runtimeLightSys = new CRTLightObjList();
	m_statDatabase = new CStatObjList();
#if (DRF_OLD_PORTALS == 1)
	m_portalDatabase = new CPortalSysList();
#endif
	m_filterSpeechDB = new CFilterSpeechList();
	m_iconDatabase = CIconSysList::GetInstance();
	m_inViewEntityList = new CDynamicOverlayList();
	m_worldChannelRuleDB = new CWorldChannelRuleList();
	m_battleSchedulerSys = new CBattleSchedulerList();
	m_shaderSystem = new CShaderObjList();
	m_pxShaderSystem = new CShaderObjList();
	m_cgGlobalShaderDB = new CGlobalMaterialShaderList();
	m_shadowEntityList = new CMovementObjList();

	// Global Bowl Ship for mouse
	m_shadowIndexTemp = 0;

	new ClientBladeFactory();

	ClientBladeFactory::Instance()->FindBlades(PathBase());
	size_t blades = ClientBladeFactory::Instance()->GetNumBlades();
	for (size_t i = 0; i < blades; i++)
		ClientBladeFactory::Instance()->GetBlade(i)->SetEngineInterface(this);

	m_pathBaseConfig.clear();

	std::string pathCommandsXml = PathFind(PathApp(), "GameFiles\\commands.xml");
	LogInfo("pathCommandsXml='" << LogPath(pathCommandsXml) << "'");

	// init the text commands
	if (!m_textCommands.Init(pathCommandsXml)) {
		LogError("TextCommands::Init() FAILED - pathCommandsXml='" << LogPath(pathCommandsXml) << "'");
	}

	CFileFactory::Initialize(new DecryptingFileFactory(ASSET_KEY));

	std::string fname;
	if (!initializeLocaleStrings("lang", fname)) {
		LogError("initializeLocaleStrings() FAILED - '" << fname << "'");
	}

	m_zonePermissions = ZP_GM; // ZP_NONE; KANEVIA HACK
	m_firstLogin = false;

	// Set WinInet max connections per server. The menu system and flash both use
	// wininet to perform its duties. The default max connections is 2 as specified by
	// the HTTP spec. We have a need to raise this limit.
	DWORD dwMaxConnectionsPerServer = 16;
	if (FALSE == ::InternetSetOption(
					 nullptr, INTERNET_OPTION_MAX_CONNS_PER_SERVER, &dwMaxConnectionsPerServer, sizeof(dwMaxConnectionsPerServer))) {
		LogError("Set max connections per server. Error:" << ::GetLastError());
	}

	// initialize the ReRenderEngine
	GetReDeviceState()->Initialize();

	// initialize the ReMeshCreate
	MeshRM::Instance()->Initialize();

	// set defaults
	// Save the wok call url
	m_wokUrl = DEFAULT_SECURE_WOK_URL;
	m_unsecureWokUrl = DEFAULT_WOK_URL;
	m_kanevaUrl = DEFAULT_KANEVA_URL;
	m_shopUrl = DEFAULT_SHOP_URL;
	m_scriptDownloadUrl = DEFAULT_SCRIPT_URL;

	// Load Network.cfg
	LoadNetworkCfg();

	// Create GAInstTrk.lua
	CreateGAInstTrkLua();

	// Create Urls.lua (with values loaded from Network.cfg)
	CreateUrlsLua();

	m_assetImporterFactory = new CSimpleImporterFactory();
	m_assetsDatabase = new CAssetsDatabase();

	m_ugcMgr = InitUGCManager(this);
	g_pColladaImporter->Register();

	m_textureImporter = createTextureImporter();
	m_textureImporter->Register();

	InitUGCImportModule(GetDispatcher());
	m_dropTargetProxy = new DropTargetProxy(*this);
	if (!m_dropTargetProxy) {
		LogError("DragDrop dropTargetProxy null");
	}

	m_kepWidget = new KEP3DObjectWidget();
	m_selectionPivot.x = 0.0f;
	m_selectionPivot.y = 0.0f;
	m_selectionPivot.z = 0.0f;

	LWG::InitModule();

	m_pathLuaCompiler = PathFind(PathApp(), "luac.exe");
	LogInfo("pathLuaCompiler='" << LogPath(m_pathLuaCompiler) << "'");
}

bool ClientEngine::LoadNetworkCfg() {
	// should be at <pathApp>/Network.cfg
	std::string pathNetworkCfg = PathFind(PathApp(), "Network.cfg");
	LogInfo("pathNetworkCfg='" << LogPath(pathNetworkCfg) << "'");
	if (!PathExists(pathNetworkCfg, false))
		return false;

	try {
		KEPConfig cfg;
		cfg.Load(pathNetworkCfg, NETCFG_ROOT);
		std::string s;
		cfg.GetValue(CLIENTDEFAULTAUTHURL_TAG, s, DEFAULT_AUTH_URL);
		m_authUrl = s;
		cfg.GetValue(CLIENTDEFAULT_RUNTIME_REPORT_URL_TAG, s, DEFAULT_RUNTIME_REPORT_URL);
		m_runtimeReportUrl = s;
		cfg.GetValue(CLIENTDEFAULTLOGINURL_TAG, s, DEFAULT_LOGIN_URL);
		if (!STLContainsIgnoreCase(s, "format=xml"))
			StrAppend(s, "&format=xml");
		m_loginUrl = s;
		cfg.GetValue(CLIENTDEFAULTSTARTAPPURL_TAG, s, DEFAULT_START_APP_URL);
		m_startAppUrl = s;
		cfg.GetValue(ALLOWACCESSURL_TAG, s, DEFAULT_ALLOW_ACCESS_URL);
		m_allowAccessUrl = s;
		cfg.GetValue(CLIENTDEFAULTIMAGEURL_TAG, s, DEFAULT_IMAGE_URL);
		m_imageUrl = s;
		cfg.GetValue(CLIENTDEFAULTIMAGETHUMBURL_TAG, s, DEFAULT_IMAGE_THUMB_URL);
		m_imageThumbUrl = s;
		cfg.GetValue(CLIENTDEFAULTFRIENDURL_TAG, s, DEFAULT_FRIEND_URL);
		m_friendUrl = s;
		cfg.GetValue(CLIENTDEFAULTFRIENDTHUMBURL_TAG, s, DEFAULT_FRIEND_THUMB_URL);
		m_friendThumbUrl = s;
		cfg.GetValue(CLIENTDEFAULTIMAGESERVER_TAG, s, DEFAULT_IMAGESERVER_URL);
		m_imageServerUrl = s;
		cfg.GetValue(UNIQUETEXTURESERVERURL_TAG, s, DEFAULT_UNIQUE_TEXTURE_SERVER_URL);
		TextureProvider::SetUniqueTextureUrlPrefix(s);
		cfg.GetValue(CLIENTDEFAULTINVENTORYSERVER_TAG, s, DEFAULT_INVENTORYSERVER_URL);
		m_inventoryServerUrl = s;
		cfg.GetValue(CLIENTDEFAULTGIFTURL_TAG, s, DEFAULT_GIFT_URL);
		m_giftUrl = s;
		cfg.GetValue(CLIENTDEFAULTGIFTTHUMBURL_TAG, s, DEFAULT_GIFT_THUMB_URL);
		m_giftThumbUrl = s;
		m_wokUrl = cfg.Get(SECUREWOKURL_TAG, DEFAULT_SECURE_WOK_URL);
		m_unsecureWokUrl = cfg.Get(WOKURL_TAG, DEFAULT_WOK_URL);
		m_kanevaUrl = cfg.Get(KANEVAURL_TAG, DEFAULT_KANEVA_URL);
		m_shopUrl = cfg.Get(SHOPURL_TAG, DEFAULT_SHOP_URL);
		m_scriptDownloadUrl = cfg.Get(SCRIPTURL_TAG, DEFAULT_SCRIPT_URL);
		m_pingLocation = cfg.Get(PING_LOCATION_TAG, DEFAULT_PING_LOCATION);
		m_zoneCutomizationsUrl = cfg.Get(ZONECUSTOM_TAG, DEFAULT_ZONECUSTOM_URL);
		m_errorReportUrl = cfg.Get(ERROR_URL_TAG, DEFAULT_ERROR_URL);
		m_childGameWebCallUrl = cfg.Get(CHILDGAMEWEBCALLS_URL_TAG, DEFAULT_CHILDGAMEWEBCALLS_URL);
		m_itemAnimationsUrl = cfg.Get(ITEM_ANIMATIONS_URL_TAG, DEFAULT_ITEM_ANIMATIONS_URL);
	} catch (KEPException* e) {
		e->Delete();
		return false;
	}

	return true;
}

bool ClientEngine::CreateGAInstTrkLua() {
	// Won't Be Found On First Launch (gets created)
	std::string pathGaLua = PathFind(PathApp(), "GameFiles\\ScriptData\\GAInstTrk.lua", false);
	LogInfo("pathGaLua='" << LogPath(pathGaLua) << "'");

	// Truncate previously-generated lua file
	std::ofstream ofsGAIni(pathGaLua.c_str(), std::ios::out | std::ios::trunc);
	ofsGAIni << "GAInstTrk = {}" << std::endl;

	// Won't Be Found On First Launch (gets created)
	std::string pathGaIni = PathFind(PathApp(), "..\\..\\GA.ini", false);
	LogInfo("pathGaIni='" << LogPath(pathGaIni) << "'");
	if (!PathExists(pathGaIni, false))
		return false;

	// Bring in patcher ga.ini for GA tracking
	std::string domain, account, uniqueId;
	std::string sFirstVisit, sPrevVisit, sCurrVisit, sNumVisits;
	std::string sScreenRes, sColorDepth, sLang, sFlashVersion, sDxVersion, sGPU, sGMem;

#define INI_READ_BUFFER_SIZE 1024
	char buffer[INI_READ_BUFFER_SIZE];
	GetPrivateProfileStringA("GA", "Domain", "", buffer, INI_READ_BUFFER_SIZE - 1, pathGaIni.c_str());
	domain = buffer;

	GetPrivateProfileStringA("GA", "Account", "", buffer, INI_READ_BUFFER_SIZE - 1, pathGaIni.c_str());
	account = buffer;

	GetPrivateProfileStringA("GA", "GUID", "", buffer, INI_READ_BUFFER_SIZE - 1, pathGaIni.c_str());
	uniqueId = buffer;

	GetPrivateProfileStringA("GA", "FirstVisit", "", buffer, INI_READ_BUFFER_SIZE - 1, pathGaIni.c_str());
	sFirstVisit = buffer;

	GetPrivateProfileStringA("GA", "PreviousVisit", "", buffer, INI_READ_BUFFER_SIZE - 1, pathGaIni.c_str());
	sPrevVisit = buffer;

	GetPrivateProfileStringA("GA", "CurrentVisit", "", buffer, INI_READ_BUFFER_SIZE - 1, pathGaIni.c_str());
	sCurrVisit = buffer;

	GetPrivateProfileStringA("GA", "NumberVisit", "", buffer, INI_READ_BUFFER_SIZE - 1, pathGaIni.c_str());
	sNumVisits = buffer;

	GetPrivateProfileStringA("GA", "utmsr", "na", buffer, INI_READ_BUFFER_SIZE - 1, pathGaIni.c_str());
	sScreenRes = buffer;

	GetPrivateProfileStringA("GA", "utmsc", "na", buffer, INI_READ_BUFFER_SIZE - 1, pathGaIni.c_str());
	sColorDepth = buffer;

	GetPrivateProfileStringA("GA", "utmul", "na", buffer, INI_READ_BUFFER_SIZE - 1, pathGaIni.c_str());
	sLang = buffer;

	GetPrivateProfileStringA("GA", "utmfl", "na", buffer, INI_READ_BUFFER_SIZE - 1, pathGaIni.c_str());
	sFlashVersion = buffer;

	GetPrivateProfileStringA("GA", "dxver", "na", buffer, INI_READ_BUFFER_SIZE - 1, pathGaIni.c_str());
	sDxVersion = buffer;

	GetPrivateProfileStringA("GA", "gpu", "na", buffer, INI_READ_BUFFER_SIZE - 1, pathGaIni.c_str());
	sGPU = buffer;

	GetPrivateProfileStringA("GA", "gmem", "na", buffer, INI_READ_BUFFER_SIZE - 1, pathGaIni.c_str());
	sGMem = buffer;

	if (!domain.empty() && !account.empty() && !uniqueId.empty() &&
		!sFirstVisit.empty() && !sPrevVisit.empty() && !sCurrVisit.empty() && !sNumVisits.empty()) {
		ULONGLONG firstVisit, prevVisit, currVisit;
		int numVisits;
		firstVisit = _strtoui64(sFirstVisit.c_str(), nullptr, 10);
		prevVisit = _strtoui64(sPrevVisit.c_str(), nullptr, 10);
		currVisit = _strtoui64(sCurrVisit.c_str(), nullptr, 10);
		numVisits = atoi(sNumVisits.c_str());

		if (firstVisit != 0 && prevVisit != 0 && currVisit != 0 && numVisits != 0) {
			// Generate domain hash (ported from javascript)
			DWORD hash = 0;
			for (std::string::reverse_iterator iter = domain.rbegin(); iter != domain.rend(); ++iter) {
				unsigned char ch = (unsigned char)*iter;
				hash <<= 6;
				hash &= 0x0FFFFFFF;
				hash += ch + (ch << 14);
				hash ^= (hash & 0x0FE00000) >> 21;
			}

			// Generate lua file
			ofsGAIni << "GAInstTrk.Domain = \"" << domain << "\"" << std::endl;
			ofsGAIni << "GAInstTrk.DomainHash = \"" << hash << "\"" << std::endl;
			ofsGAIni << "GAInstTrk.Account = \"" << account << "\"" << std::endl;
			ofsGAIni << "GAInstTrk.GUID = \"" << uniqueId << "\"" << std::endl;
			ofsGAIni << "GAInstTrk.FirstVisit = \"" << firstVisit << "\"" << std::endl;
			ofsGAIni << "GAInstTrk.PreviousVisit = \"" << prevVisit << "\"" << std::endl;
			ofsGAIni << "GAInstTrk.CurrentVisit = \"" << currVisit << "\"" << std::endl;
			ofsGAIni << "GAInstTrk.NumberVisit = \"" << numVisits << "\"" << std::endl;
			ofsGAIni << "GAInstTrk.ScreenRes = \"" << sScreenRes << "\"" << std::endl;
			ofsGAIni << "GAInstTrk.ColorDepth = \"" << sColorDepth << "\"" << std::endl;
			ofsGAIni << "GAInstTrk.Lang = \"" << sLang << "\"" << std::endl;
			ofsGAIni << "GAInstTrk.FlashVersion = \"" << sFlashVersion << "\"" << std::endl;
			ofsGAIni << "GAInstTrk.DxVersion = \"" << sDxVersion << "\"" << std::endl;
			ofsGAIni << "GAInstTrk.GPU = \"" << sGPU << "\"" << std::endl;
			ofsGAIni << "GAInstTrk.GMem = \"" << sGMem << "\"" << std::endl;
		}
	}

	ofsGAIni.close();
	return true;
}

bool ClientEngine::CreateUrlsLua() {
	// Won't Be Found On First Launch (gets created)
	std::string pathUrlsLua = PathFind(PathApp(), "Gamefiles\\Scripts\\urls.lua", false);
	LogInfo("pathUrlsLua='" << LogPath(pathUrlsLua) << "'");

	// write out the urls to scripts
	std::ofstream urls(pathUrlsLua.c_str());
	urls << "-- generated by ClientEngine on startup to be included at the end of GameGlobals.lua" << std::endl;
	urls << "GameGlobals.WEB_SITE_PREFIX		= \"" << m_unsecureWokUrl << "\"" << std::endl;
	urls << "GameGlobals.SECURE_WEB_SITE_PREFIX		= \"" << m_wokUrl << "\"" << std::endl;
	urls << "GameGlobals.WEB_SITE_PREFIX_KANEVA = \"" << m_kanevaUrl << "\"" << std::endl;
	urls << "GameGlobals.WEB_SITE_PREFIX_SHOP = \"" << m_shopUrl << "\"" << std::endl;
	urls << "GameGlobals.WEB_SITE_PREFIX_IMAGES = \"" << m_imageServerUrl << "\"" << std::endl;
	urls.close();

	return true;
}

void ClientEngine::ResizeClientRect(UINT typeOnSize, int cx, int cy, bool resetDevice) {
	// Skip Most Messages During Startup
	if (m_startingUp)
		return;

	// Skip Messages With Invalid DirectX Device
	if (!g_pD3dDevice)
		return;

	if (m_dynamicGeom)
		m_dynamicGeom->ReSize(cx, cy);

	// Notify Window State Changes
	std::string typeStr;
	switch (typeOnSize) {
		case SIZE_RESTORED:
			typeStr = (m_typeOnSize == SIZE_RESTORED) ? "Resized" : "Restored";
			break;
		case SIZE_MINIMIZED:
			typeStr = "Minimized";
			break;
		case SIZE_MAXIMIZED:
			typeStr = "Maximized";
			break;
		case SIZE_MAXSHOW:
			typeStr = "MaxShow";
			break;
		case SIZE_MAXHIDE:
			typeStr = "MaxHide";
			break;
		default:
			typeStr = "OnSized";
			break;
	}

	m_typeOnSize = typeOnSize;

	// Resize Client Window
	CRect rect;
	GetClientRect(&rect);
	InvalidateRect(rect, true);

	m_curModeRect = rect;

	int width = rect.Width();
	int height = rect.Height();
	m_pCABD3DLIB->m_curModeRect = RECT({ 0, 0, width, height });
	GuiRect = RECT({ 0, 0, width, height });

	d3dpp.BackBufferWidth = width;
	d3dpp.BackBufferHeight = height;

	IEvent* resizeEvent = m_dispatcher.MakeEvent(m_clientResizeEventId);
	if (resizeEvent) {
		*resizeEvent->OutBuffer() << cx << cy;
		*resizeEvent->OutBuffer() << typeStr;
		m_dispatcher.QueueEvent(resizeEvent);
	}

	// DRF - Flag Window Refresh (NeedsWindowRefresh() called from RenderLoop())
	if (!m_shutdown && resetDevice) {
		LogInfo("Window " << typeStr << " - Flagging Window Refresh");
		FlagWindowRefresh();
	}
}

std::string ClientEngine::LocalDevAppDir(void) const {
	// Get Current Game Id
	int gameId = GetGameId();

	// Try to detect Game Id if it's unknown (e.g. before calling gameLogin or connecting to server)
	if (gameId == 0) {
		std::string pathBase = PathBase();
		size_t posLastSlash = pathBase.find_last_of("\\/");
		if (posLastSlash != std::string::npos && posLastSlash != pathBase.length() - 1) {
			std::string sGameId = pathBase.substr(posLastSlash + 1);
			gameId = atoi(sGameId.c_str());
		}

		if (gameId < 0)
			gameId = 0;
	}

	std::stringstream ssLocalDevAppDir;
	ssLocalDevAppDir << LocalDevBaseDir() << "\\" << gameId;
	return ssLocalDevAppDir.str();
}

inline int getActorMeshSlot(int gender, int slot) {
	const int GEN_MALE = 0;
	const int GEN_FEMALE = 1;

	const int PANTS = 0;
	const int SHIRT = 1;
	const int SHOES = 2;
	const int TORSO = 3;
	const int HIPS = 4;
	const int LEGS = 5;
	const int ARMS = 6;

	const int PANTS_SLOT = 9;
	const int SHIRT_SLOT = 8;
	const int SHOES_SLOT = 10;
	const int TORSO_SLOT = 2;
	const int HIPS_SLOT = 3;
	const int LEGS_SLOT = 5;
	const int ARMS_SLOT = 4;

	if (gender != GEN_MALE && gender != GEN_FEMALE)
		return -1;

	switch (slot) {
		case PANTS_SLOT:
			return PANTS;
		case SHIRT_SLOT:
			return SHIRT;
		case SHOES_SLOT:
			return SHOES;
		case TORSO_SLOT:
			return TORSO;
		case HIPS_SLOT:
			return HIPS;
		case LEGS_SLOT:
			return LEGS;
		case ARMS_SLOT:
			return ARMS;
		default:
			return -1;
	}
}

inline int getDOActorMeshIndex(int gender, int clothingSet, int slot) {
	const int STARTER = 0;

	const int GEN_MALE = 0;
	const int GEN_FEMALE = 1;

	const int PANTS = 0;
	const int SHIRT = 1;
	const int SHOES = 2;
	const int TORSO = 3;
	const int HIPS = 4;
	const int LEGS = 5;
	const int ARMS = 6;

	int clothingSets[1][7][2]; // [clothingset][# slots to change][gender]
	clothingSets[STARTER][PANTS][GEN_MALE] = 1;
	clothingSets[STARTER][PANTS][GEN_FEMALE] = 1;
	clothingSets[STARTER][SHIRT][GEN_MALE] = 1;
	clothingSets[STARTER][SHIRT][GEN_FEMALE] = 1;
	clothingSets[STARTER][SHOES][GEN_MALE] = 1;
	clothingSets[STARTER][SHOES][GEN_FEMALE] = 1;
	clothingSets[STARTER][TORSO][GEN_MALE] = 1;
	clothingSets[STARTER][TORSO][GEN_FEMALE] = 1;
	clothingSets[STARTER][HIPS][GEN_MALE] = 1;
	clothingSets[STARTER][HIPS][GEN_FEMALE] = 1;
	clothingSets[STARTER][LEGS][GEN_MALE] = 1;
	clothingSets[STARTER][LEGS][GEN_FEMALE] = 1;
	clothingSets[STARTER][ARMS][GEN_MALE] = 1;
	clothingSets[STARTER][ARMS][GEN_FEMALE] = 0;

	int clothingSetIndex = getActorMeshSlot(gender, slot);
	if (clothingSetIndex >= 0) {
		return clothingSets[clothingSet][clothingSetIndex][gender];
	} else {
		return 0;
	}
}

bool ClientEngine::GetTransformRelativeToBone(int boneId, double x, double y, double z, double* outX, double* outY, double* outZ) {
	Matrix44f boneMatrix;
	if (!ObjectGetWorldTransformation(ObjectType::BONE, boneId, boneMatrix)) {
		LogError("ObjectGetWorldTransformation() FAILED - boneId=" << boneId);
		return false;
	}

	Matrix44f relativeMatrix, worldPosMatrix, inverseBone;
	worldPosMatrix.MakeTranslation(Vector3f(x, y, z));
	inverseBone.MakeInverseOf(boneMatrix);
	relativeMatrix = worldPosMatrix * inverseBone;
	*outX = relativeMatrix(3, 0);
	*outY = relativeMatrix(3, 1);
	*outZ = relativeMatrix(3, 2);

	return true;
}

std::string ClientEngine::GetCompassDir(CMovementObj* pMO) {
	if (!pMO)
		return "Error";

	Vector3f normal;
	normal.x = 0.0f;
	normal.y = 0.0f;
	normal.z = -1.0f;
	normal = TransformVector(pMO->getBaseFrameMatrix(), normal);

	normal.y = 0.0f;
	normal.Normalize();

	float rotationOnY = atan(fabs(normal.x) / fabs(normal.z)) * 57.3f;
	if (normal.z > 0) {
		if (normal.x > 0.0f) {
			if (rotationOnY < 22.5)
				return "N";
			else if (rotationOnY < 67.5)
				return "NE";
			else
				return "E";
		} else {
			if (rotationOnY < 22.5)
				return "N";
			else if (rotationOnY < 67.5)
				return "NW";
			else
				return "W";
		}
	} else {
		if (normal.x > 0.0f) {
			if (rotationOnY < 22.5)
				return "S";
			else if (rotationOnY < 67.5)
				return "SE";
			else
				return "E";
		} else {
			if (rotationOnY < 22.5)
				return "S";
			else if (rotationOnY < 67.5)
				return "SW";
			else
				return "W";
		}
	}
}

bool ClientEngine::IsInView() {
	// Test Cooperative Level (D3D_OK while client window is in view)
	HRESULT hrTCL = (g_pD3dDevice ? g_pD3dDevice->TestCooperativeLevel() : -1);
	bool tclChanged = (m_hrTCL != hrTCL);
	if (tclChanged) {
		m_hrTCL = hrTCL;
		if (hrTCL == D3DERR_DEVICENOTRESET) {
			LogInfo("Calling OnResetDevice()");
			OnResetDevice();
		} else if (hrTCL == D3DERR_DEVICELOST) {
			LogInfo("Calling OnLostDevice()");
			OnLostDevice();
		}
	}

	// Log Client Window In View State Change
	bool inView = SUCCEEDED(hrTCL);
	bool viewChanged = (m_inView != inView);
	if (viewChanged) {
		if (inView) {
			IEvent* event = m_dispatcher.MakeEvent(m_clientBackInViewEventId);
			if (event) {
				m_dispatcher.QueueEvent(event);
			}
		}
		m_inView = inView;
		LogInfo(LogBool(inView));
	}

	return inView;
}

bool ClientEngine::GetSnapVector(Vector3f& snapVector, float& snapLenSq, const CDynamicPlacementObj* snapFrom, const CDynamicPlacementObj* snapTo) {
	bool snapped = false;

	int numSnapTo = 0, numSnapFrom = 0;

	if (!snapTo)
		return false;
	const Vector3f* snapToLoc = snapTo->GetSnapLocations(&numSnapTo);
	if (!snapToLoc)
		return false;

	if (!snapFrom)
		return false;
	const Vector3f* snapFromLoc = snapFrom->GetSnapLocations(&numSnapFrom);
	if (!snapFromLoc)
		return false;

	// Test each pair of snap locations, looking for the smallest displacement
	for (int i = 0; i < numSnapTo; i++) {
		for (int j = 0; j < numSnapFrom; j++) {
			Vector3f displacement = snapToLoc[i] - snapFromLoc[j];

			float lenSq = displacement.LengthSquared();

			if (lenSq <= snapLenSq) {
				snapVector = displacement;
				snapLenSq = lenSq;
				snapped = true;
			}
		}
	}

	return snapped;
}

void ClientEngine::SyncGameTime(double serverGameTime) {
	m_PreviousGameTime = serverGameTime;
	m_CurrentGameTime = m_PreviousGameTime;
	m_GameTimeOffset = serverGameTime - GetCurrentTimeInSeconds();
}

EVENT_PROC_RC ClientEngine::HandleStringListEvent(StringListEvent* sle) {
	int count = -1;
	int userInt = 0;
	sle->ExtractPrefix(count, userInt);
	if (count == 0)
		m_firstLogin = true;
	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientEngine::HandleSetZonePermissionEvent(int zonePermissions) {
	m_zonePermissions = zonePermissions;
	return EVENT_PROC_RC::OK;
}

void ClientEngine::AutoExecInitAndParseCommandLine(HWND hwndLoc) {
	// Get Main Window Handle
	if (!hwndLoc)
		hwndLoc = AfxGetMainWnd()->m_hWnd;

	CFileException e;

	static const int STARTUP_PROGRESS_WEIGHT_TOTAL = 780;
	m_progressUpdateTotal = STARTUP_PROGRESS_WEIGHT_TOTAL;

	ProgressUpdate("AutoExecInit", "Initializing", 0, TRUE, FALSE);

	// Initialize DirectInput
	if (!m_pCABD3DLIB) {
		LogFatal("CABD3DLIB is null");
		return;
	}
	bool ok = m_pCABD3DLIB->InitializeDirectInput(hwndLoc, instanceHandle);
	if (!ok) {
		LogFatal("InitializeDirectInput() FAILED");
		return;
	}

	// Attach DirectInput Devices (keyboard & mouse)
	m_keyboardState.DirectInputDevice(m_pCABD3DLIB->directKeyboard);
	m_keyboardStateWas = m_keyboardState;
	m_mouseState.DirectInputDevice(m_pCABD3DLIB->directMouse);
	m_mouseStateWas = m_mouseState;

	m_pCABD3DLIB->MakeDirectSound(hwndLoc, &m_pDirectSound); // making direct sound
	m_soundManager->Initialize(m_pDirectSound);

	// autoexec
	LoadScenePerformSplashScreen();

	// Old Physics
	m_oldPhysics = std::make_unique<OldPhysics>();
	m_oldPhysics->Initialize(m_environment);

	// Disable Freefall Detector In DevMode
	// #MLB_ED_7397 SERVER FALL THROUGH WORLD...(This is currently disabled in the FreeFallDetector)
	// Because, this is always returning false, even not in DevMode or so asserts Brandy.
	m_oldPhysics->SetEnabledFreeFall(AppDevMode() == 0);

	// DRF - Apply Registry & StartConfig Auto Login Settings ?
	if (!AppLoginEmail().empty())
		m_login.userEmail = AppLoginEmail();
	if (m_loginEmail.GetLength() > 0)
		m_login.userEmail = m_loginEmail;
	if (!AppLoginPassword().empty())
		m_login.userPassword = AppLoginPassword();
	if (m_loginPassword.GetLength() > 0)
		m_login.userPassword = m_loginPassword;

	// Parse Command Line
	ParseCommandLine();

	//Ankit ----- if SATEnabled flag to true(read from the StartCfg or cmdLine param), setup SATFramework and start once client ready
	if (m_SATEnabled) {
		m_SATFrameworkInstance = SATFramework::GetSATInstance();
		if (!m_SATFrameworkInstance) {
			LogInfo("Failed to start up SAT bot. Exit");
			//failed to locate SATFrameworkInstance
		} else {
			m_SATRunning = true;
			m_login.userEmail = "";
			m_login.userPassword = "";
			SaveAutoLogonCredentials();
			m_dispatcher.QueueEventInFutureMs(m_dispatcher.MakeEvent(m_dispatcher.GetEventId("StartClientTestingEvent")), 15000);
		}
	}

	// Allow Auto Logon (assuming we have username and password)
	m_login.autoLogin = true;
	LogInfo("canAutoLogin=" << LogBool(CanAutoLogin()));

	// DRF - Set Default Values
	if (m_multiplayerObj->getIPAddress().empty())
		m_multiplayerObj->GetIPDataFromDisk();

	// ED-8451 - Client runs Launcher if not given valid patch URL
	// DEV_MODE - Let devs use the dodgy patch discovery via AppPatchUrl()
	if (PatchUrl().empty()) {
		if (AppDevMode()) {
			SetPatchUrl(AppPatchUrl());
		} else {
			LogFatal("EMPTY PATCH URL - Running Launcher ...");
			RunLauncher();
			::ExitProcess(0); // never returns
		}
	}
	LogInfo("patchUrl='" << PatchUrl() << "'");

	// start the tamperChecker after event system init
	m_menuList->Load(0, false, false);

	// load default client XML in client
	FileName fileName = PathAdd(PathGameFiles(), "clientautoexec.xml");
	RunScript(fileName);

	LoadKeyBindings();
	RegisterKeyEventHandlers();

	m_oldPhysics->Initialize(m_environment);

	std::vector<std::string> search_path;

	// init texture databases
	if (m_pTextureDB)
		m_pTextureDB->Initialize();

	// Initialize Sounds
	SoundPlacementsInit();

	// Hide progress dialog
	if (m_pWin32ProgDlg) {
		m_pWin32ProgDlg->ShowWindow(SW_HIDE);
		m_pWin32ProgDlg->DestroyWindow();
		delete m_pWin32ProgDlg;
		m_pWin32ProgDlg = nullptr;
	}

	ProgressUpdate("AutoExecInit", "Initializing", 0, FALSE, TRUE);

	// Reset progress update total
	m_progressUpdateTotal = 0;
	m_progressUpdateCurrent = 0;

	// Start-up processing completed
	m_startingUp = false;

	if (m_closeOnStartup) {
		LogFatal("Close On Start - Shutting Down ...");
		SendMessage(WM_CLOSE, 0, 0);
	}

	// Unload All Splash Screen Resources (textures, etc)
	m_RenderSplash = nullptr;
}

bool ClientEngine::RunLauncher() {
	SetCurrentDirectoryW(Utf8ToUtf16("..\\..").c_str());
	STARTUPINFO si;
	memset(&si, 0, sizeof(si));
	si.cb = sizeof(si);
	PROCESS_INFORMATION pi;
	memset(&pi, 0, sizeof(pi));
	
	// Run Launcher
	std::string cmdLine;
	StrBuild(cmdLine, K_LAUNCHER_EXE " kaneva://" << AppStarId());
	if (!::KEP::CreateProcess(
			NULL,
			static_cast<LPCSTR>(cmdLine.c_str()),
			NULL,
			NULL,
			FALSE,
			0,
			NULL,
			NULL,
			&si,
			&pi)) {
		LogError("FAILED CreateProcess() - cmdLine='" << cmdLine << "'");
		return false;
	}
	LogInfo("OK");
	return true;
}

// parse command line after InitEvents since need GotoPlaces registered.
// Note that I also do this in KEPEdit::InitInstance, because some command
// line arguments need to be processed when the application starts and
// not here, after the WoK.kep file is loaded.
bool ClientEngine::ParseCommandLine() {
	// Get This App
	CWinApp* pApp = AfxGetApp();
	if (!pApp) {
		LogError("pApp is null");
		return false;
	}

	// Parse App Command Line
	ClientEngine_CmdLineInfo cmdInfo;
	pApp->ParseCommandLine(cmdInfo);
	if (pApp->m_lpCmdLine[0]) {
		if (AppDevMode()) {
			LogInfo("cmdLine='" << Utf16ToUtf8(pApp->m_lpCmdLine) << "'");
		}

		if (m_runtimeId.empty()) {
			setRuntimeId(cmdInfo.m_runtimeId);
			LogInfo("runtimeId=" << getRuntimeId());
		}

		if (cmdInfo.m_commandline_override_ip_port) {
			if (cmdInfo.m_ip != "") {
				m_multiplayerObj->setIPAddress((const char*)cmdInfo.m_ip);
				if (cmdInfo.m_port != "") {
					m_multiplayerObj->m_port = atol(cmdInfo.m_port);
				}
				m_commandlineOverrideIPPort = true;
			}
		}

		if (cmdInfo.m_gotoplayeronstartup) {
			int playerServerId = cmdInfo.m_playerServerId;
			if (playerServerId > 0) {
				GotoPlaceEvent* e = new GotoPlaceEvent();
				e->GotoPlayer(playerServerId, 0); // char index of zero, WOK specific
				e->AddTo(SERVER_NETID);
				m_reconnectEvent = e;
				m_gotoplayeronstartup = true;
				m_gotoonstartupId = playerServerId;
			}
		} else if (cmdInfo.m_gotoapartmentonstartup) {
			m_gotoapartmentonstartup = true;

			// do they want to jump to a player's apartment on startup?
			int playerServerId = cmdInfo.m_playerServerId;
			if (playerServerId > 0) {
				GotoPlaceEvent* e = new GotoPlaceEvent();
				e->GotoApartment(playerServerId, 0); // char index of zero, WOK specific
				e->AddTo(SERVER_NETID);
				m_reconnectEvent = e;
				m_gotoapartmentonstartup = true;
				m_gotoonstartupId = playerServerId;
			}
		} else if (cmdInfo.m_gotozoneonstartup) {
			// do they want to jump to an instance zone on startup?
			// format of param is -gz<zoneIndex>:<instanceId>
			long zoneIndex = cmdInfo.m_zoneIndex;
			long instanceId = cmdInfo.m_instanceId;
			GotoPlaceEvent* e = new GotoPlaceEvent();
			e->GotoZone(ZoneIndex(zoneIndex), instanceId, 0); // char index of zero, WOK specific
			e->AddTo(SERVER_NETID);
			m_reconnectEvent = e;
		} else if (cmdInfo.m_gotochannelonstartup) {
			// do they want to jump to a channel on startup?
			// format of param is -gc<instanceId>
			long instanceId = cmdInfo.m_instanceId;
			if (instanceId > 0) {
				// Don't create the event yet, we must determine the zoneIndex during the connection process
				m_gotochannelonstartup = true;
				m_gotoonstartupId = instanceId;
			} else {
				// if <= 0, assume permanent zone index
				ZoneIndex zoneIndex(-instanceId, 0, CI_PERMANENT);
				instanceId = 0;

				GotoPlaceEvent* e = new GotoPlaceEvent();
				e->GotoZone(zoneIndex, instanceId, 0); // char index of zero, WOK specific
				e->AddTo(SERVER_NETID);
				m_reconnectEvent = e;
			}
		} else if (cmdInfo.m_gotourlonstartup) {
			// do they want to goto a URL on startup?
			// format of param is -gu<url>
			std::string url = cmdInfo.m_url.GetString();
			size_t protocolPos = url.find("://"); // assume stp, but don't look for it
			if (!url.empty() && protocolPos != std::string::npos) {
				std::string placeName;
				StpUrl::ExtractPlaceName(url, placeName);
				if (placeName.empty()) {
					// Ignore default URL: kaneva://<GAME_ID>
					LogInfo("URL ignored: " << url);
				} else {
					StpUrl::ExtractParameters(url, m_urlParameters);

					// remove the parameters from the URL when sending it up to the server
					if (m_urlParameters.length() > 0) {
						int paramLength = m_urlParameters.length() + 1; // +1 for the question mark
						url = url.substr(0, url.size() - paramLength);
					}

					LogInfo("URL: " << url);
					m_gotochannelurlonstartup = url;
					GotoPlaceEvent* e = new GotoPlaceEvent();
					e->GotoUrl(0, url.c_str()); // char index of zero, WOK specific
					e->AddTo(SERVER_NETID);
					m_reconnectEvent = e;
				}
			} else {
				// bad url
				LogWarn("Bad url passed in, skipping it: " << url);
			}
		}

		if (cmdInfo.m_tryon) {
			int tryonglid = cmdInfo.m_tryonGlid;
			USE_TYPE usetype = cmdInfo.m_tryonUseType;

			// Clothing
			if (usetype == USE_TYPE_NONE ||
				usetype == USE_TYPE_EQUIP ||
				usetype == USE_TYPE_ADD_DYN_OBJ ||
				usetype == USE_TYPE_ADD_ATTACH_OBJ ||
				usetype == USE_TYPE_ANIMATION ||
				usetype == USE_TYPE_SOUND) {
				m_tryOnOnStartup = true;
				m_tryOnGlid = tryonglid;
				m_tryonUseType = usetype;
			} else {
				LogWarn("Invalid tryon use type passed in, skipping it: " << usetype);
				m_tryOnOnStartup = false;
				m_tryOnGlid = 0;
			}
		}

		// Command Line userName -u is Actually Email
		if (cmdInfo.m_userName.GetLength() > 0)
			m_login.userEmail = cmdInfo.m_userName.GetString();

		if (cmdInfo.m_origPassword.GetLength() > 0)
			m_login.userPassword = cmdInfo.m_origPassword.GetString();

		if (cmdInfo.m_patchUrl.GetLength() > 0)
			SetPatchUrl(cmdInfo.m_patchUrl.GetString());

		if (cmdInfo.m_startSATFramework)
			m_SATEnabled = true;
	}

	return true;
}

BOOL ClientEngine::SetEntityAnimByNetAssignedID(int netAssignedID, eAnimType animType, int animVer) {
	auto pMO = GetMovementObjByNetId(netAssignedID);
	if (!pMO || !pMO->getSkeleton())
		return FALSE;

	pMO->setOverrideAnimType(animType);
	pMO->setOverrideDuration(0);
	pMO->getSkeleton()->setAnimVerPreference(animVer);
	pMO->setOverrideTimeMs(fTime::TimeMs());
	return TRUE;
}

TimeSec ClientEngine::AssignCurrentAnimation(eAnimType animType, int animVer, bool repeat) {
	auto pMO = GetMovementObjMe();
	if (!pMO || !pMO->getSkeleton())
		return 0.0f;

	GLID animGlid = pMO->getSkeleton()->getAnimationGlidByAnimTypeAndVer(animType, animVer);
	if (!IS_VALID_GLID(animGlid) && (animVer != 0) && (animVer != ANIM_VER_ANY))
		animGlid = pMO->getSkeleton()->getAnimationGlidByAnimTypeAndVer(animType);

	// Set Current Animation Settings (dirForward, default looping)
	TimeSec animTimeSec = 0.0f;
	if (IS_VALID_GLID(animGlid)) {
		auto pSAH = pMO->getSkeleton()->getSkeletonAnimHeaderByGlid(animGlid);
		if (pSAH && pSAH->LoadData()) {
			animTimeSec = pSAH->m_animDurationMs * .001f;
			pMO->getSkeleton()->setAnimVerPreference(animVer);
			AnimSettings animSettings(animGlid);
			pMO->getSkeleton()->setPrimaryAnimationSettings(animSettings);
		}
	}

	return animTimeSec;
}

BOOL ClientEngine::UpdateEverything() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "MovementUpdate");

	ContentService::Instance()->OnUpdate();
	UGCAvatarUpdate();
	EnvironmentUpdate();

	if (m_soundManager)
		m_soundManager->CallBack();

	//Ankit ----- Building trigger list just before performing trigger checks in the update loop
	CleanupPendingTriggerDeletions();
	{
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "TriggerCheckCallback");
		TriggerCheckCallback();
	}

	// Update all in-world DOs for position and state changes.
	UpdateAllDynamicObjects();

	CallbackSystem::Instance()->ExecuteCallbacks();

	{
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "UniversalControlUnit");
		UniversalControlUnit();
	}
	if (GetMouseKeyboardUpdate())
		ControlsInLoop();

	if (m_pendingControlSystem)
		m_pendingControlSystem->CheckCaptureControl(m_mouseState, m_keyboardState);

	{
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "InputSystem-ProcessQueuedEvents");
		m_pInputSystem->ProcessQueuedEventsUntil(GetFrameStartTime());
	}

	{
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "ManageSmoothMovementOnNetworkEntities");
		UpdateAllEntityMovements();
	}

	CMovementObj* pMO = GetMovementObjMe();
	bool bDrivingVehicle = pMO && pMO->getPhysicsVehicle();
	// #MLB_StepPhysicsEveryFrame - When the Avatar is valid, StepPhysics
	if (pMO) {
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "StepPhysics");
		StepPhysics();
	}

	if (bDrivingVehicle) {
		UpdatePositionFromPhysicsVehicle(pMO);
	}

	CollisionLoopEntityToEntity();

	if (!bDrivingVehicle) {
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "HandleCollision");
		HandleCollision();
	}

	PossesCallback();
	Callback();
	GenerateEntityLastPosition();

	PendingCommandsCallback();

	{
		FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "UpdateRuntimeCameras");
		m_pRuntimeCameras->Update();
	}

	// Update Sound Placements
	SoundPlacementUpdate();

	// Update Media Placements
	MediaPlacementsUpdate();

	// DRF - Bug Fix
	if (m_DOSpawnMonitor && m_DOSpawnMonitor->IsSpawning()) {
		IterateObjects([this](CDynamicPlacementObj* pDPO) -> bool {
			if (pDPO && pDPO->GetBaseObj())
				m_DOSpawnMonitor->CheckDynObjLoadComplete(pDPO->GetGlobalId());
			return true; // next
		});
	}

	UpdateDownloadPriorities();

	return TRUE;
}

void ClientEngine::EnableLightForPosition(Vector3f& position) {
	m_runtimeLightSys->EnableLightsForPosition(g_pD3dDevice, m_pRenderStateMgr, position);
}

BOOL ClientEngine::HousePlacementValidCheck(const Vector3f& charPosition, ABBOX box) {
	Vector3f intersect, normal;
	int objectHit = 0;
	BOOL result = FALSE;

	Vector3f currentPosition, anchorPosition;

	// box seg 1
	currentPosition.x = box.minX + charPosition.x;
	currentPosition.y = box.maxY + charPosition.y;
	currentPosition.z = box.maxZ + charPosition.z;
	anchorPosition.x = box.minX + charPosition.x;
	anchorPosition.y = box.minY + charPosition.y;
	anchorPosition.z = box.maxZ + charPosition.z;

	result = m_pCollisionObj->CollisionSegmentIntersect(currentPosition, anchorPosition, 20.0f, true, false, &intersect, &normal, &objectHit);
	if (result == FALSE)
		return FALSE; // not in ground

	// box seg 2
	currentPosition.x = box.maxX + charPosition.x;
	currentPosition.y = box.maxY + charPosition.y;
	currentPosition.z = box.maxZ + charPosition.z;
	anchorPosition.x = box.maxX + charPosition.x;
	anchorPosition.y = box.minY + charPosition.y;
	anchorPosition.z = box.maxZ + charPosition.z;

	result = m_pCollisionObj->CollisionSegmentIntersect(currentPosition, anchorPosition, 20.0f, true, false, &intersect, &normal, &objectHit);
	if (result == FALSE)
		return FALSE; // not in ground

	// box seg 3
	currentPosition.x = box.maxX + charPosition.x;
	currentPosition.y = box.maxY + charPosition.y;
	currentPosition.z = box.minZ + charPosition.z;
	anchorPosition.x = box.maxX + charPosition.x;
	anchorPosition.y = box.minY + charPosition.y;
	anchorPosition.z = box.minZ + charPosition.z;

	result = m_pCollisionObj->CollisionSegmentIntersect(currentPosition, anchorPosition, 20.0f, true, false, &intersect, &normal, &objectHit);
	if (result == FALSE)
		return FALSE; // not in ground

	// box seg 4
	currentPosition.x = box.minX + charPosition.x;
	currentPosition.y = box.maxY + charPosition.y;
	currentPosition.z = box.minZ + charPosition.z;
	anchorPosition.x = box.minX + charPosition.x;
	anchorPosition.y = box.minY + charPosition.y;
	anchorPosition.z = box.minZ + charPosition.z;

	result = m_pCollisionObj->CollisionSegmentIntersect(currentPosition, anchorPosition, 20.0f, true, false, &intersect, &normal, &objectHit);
	if (result == FALSE)
		return FALSE; // not in ground

	return TRUE;
}

void ClientEngine::EncodeUnrecognizedInvItem(IWriteableBuffer& buffer, int itemGLID, int quantity, bool bankOnly) {
	std::string itemName = "Unknown (" + numToString(itemGLID, "%d") + ")";

	buffer
		<< itemGLID // GLID
		<< quantity // Quantity
		<< itemName // Item name
		<< 0 // Use type
		<< "" // texture name (ignored by script)
		<< 0 // Texture uv left
		<< 0 // Texture uv top
		<< 0 // Texture uv right
		<< 0; // Texture uv bottom

	if (!bankOnly) {
		buffer
			<< 0 // invObj->m_itemData->m_sellingPrice;
			<< 0 // pass list count
			<< 0 // invObj->m_itemData->m_defaultArmedItem;
			<< 0 // invObj->m_itemData->m_itemType
			<< ""; // invObj->m_itemData->m_itemDescription
	}
}

bool ClientEngine::TryEncodeUGCInvItem(IWriteableBuffer& buffer, int itemGLID, int quantity, bool bankOnly) {
	// Get Cached Content Metadata (synchronously cache if not already cached)
	GLID glid = (GLID)abs(itemGLID);
	ContentMetadata md(glid, true);
	if (!md.IsValid()) {
		LogError("ContentMetadata::IsValid() FAILED - glid=" << glid);
		return false;
	}

	std::string texFileName = TextureFileName(glid);
	std::string texUrl = md.getThumbnailPath();

	// Add Texture To Texture Database
	DWORD nameHash = INVALID_TEXTURE_NAMEHASH;
	m_pTextureDB->TextureAdd(nameHash, texFileName, "Icons", texUrl);
	if (!ValidTextureNameHash(nameHash)) {
		LogError("AddTextureToDatabase(UGC) FAILED - glid<" << glid << "> texName='" << texFileName << "' texUrl='" << texUrl << "'");
		return false;
	}

	CTextureEX* texPtr = m_pTextureDB->TextureGet(nameHash);

	// force it to start downloading, if you don't do this, the
	// icon won't ever show up, because menus don't load their textures
	// via the ResourceManager system we have in place
	if (texPtr)
		texPtr->GetD3DTextureIfLoadedOrQueueToLoad();

	CGlobalInventoryObj* baseObj = m_globalInventoryDB->GetByGLID(md.getBaseGlobalId());
	if (!baseObj)
		return false;

	buffer
		<< itemGLID // GLID
		<< quantity // Quantity
		<< md.getItemName() // Item name
		<< (int)baseObj->m_itemData->m_useType // Use type
		<< "" // texture name (ignored by script)
		<< 0 // Texture uv left
		<< 0 // Texture uv top
		<< 0 // Texture uv right
		<< 0; // Texture uv bottom

	if (!bankOnly) {
		buffer
			<< 0 // invObj->m_itemData->m_sellingPrice;
			<< 0 // pass list count
			<< 0 // invObj->m_itemData->m_defaultArmedItem;
			<< baseObj->m_itemData->m_itemType // invObj->m_itemData->m_itemType
			<< md.getItemDesc(); // invObj->m_itemData->m_itemDescription
	}

	return true;
}

void ClientEngine::EncodeInventoryItem(IWriteableBuffer& buffer, int intGlid, int quantity, CGlobalInventoryObj* invObj) {
	RECT texture_coords = { 0, 0, 0, 0 };
	CStringA texture_name = "";

	GLID glid = (GLID)abs(intGlid);

	if (!IS_UGC_GLID(glid)) {
		if (invObj) {
			// GetInventoryIconInfo(invObj, texture_coords, texture_name);
			buffer << (int)glid
				   << quantity
				   << invObj->m_itemData->m_itemName
				   << (int)invObj->m_itemData->m_useType
				   << texture_name
				   << texture_coords.left << texture_coords.top
				   << texture_coords.right << texture_coords.bottom
				   << invObj->m_itemData->m_sellingPrice;

			if (invObj->m_itemData->m_passList) {
				buffer << (ULONG)invObj->m_itemData->m_passList->ids().size();
				for (const auto& passId : invObj->m_itemData->m_passList->ids()) {
					buffer << (int)passId;
				}
			} else {
				buffer << 0;
			}
			buffer << invObj->m_itemData->m_defaultArmedItem
				   << invObj->m_itemData->m_itemType
				   << invObj->m_itemData->m_itemDescription;

			std::string texFileName = TextureFileName(glid);

			// Add Texture To Texture Database
			DWORD nameHash = INVALID_TEXTURE_NAMEHASH;
			m_pTextureDB->TextureAdd(nameHash, texFileName, "Icons");
			if (!ValidTextureNameHash(nameHash)) {
				LogError("AddTextureToDatabase() FAILED - glid<" << glid << "> texName='" << texFileName << "'");
			}
		}
	} else {
		if (!TryEncodeUGCInvItem(buffer, intGlid, quantity, false))
			EncodeUnrecognizedInvItem(buffer, intGlid, quantity, false);
	}
}

void ClientEngine::CacheEntityForShadow(CMovementObj* pMO) {
	if (!pMO)
		return;

	if (m_globalShadowType != 2)
		return;

	if (!m_showFirstPersonShadow && pMO->isFirstPersonMode())
		return;

	if (pMO->getDistanceToCamTemp() > m_globalShadowDist)
		return;

	if (m_shadowEntityList->GetCount() >= m_maxShadowCount)
		return;

	for (POSITION pos = m_shadowEntityList->GetHeadPosition(); pos;) {
		auto pMO_find = (CMovementObj*)m_shadowEntityList->GetNext(pos);
		if (pMO->getDistanceToCamTemp() < pMO_find->getDistanceToCamTemp()) {
			m_shadowEntityList->InsertBefore(pos, pMO);
			return;
		}
	}

	m_shadowEntityList->AddTail(pMO);
}

void ClientEngine::GetShadowPolyMeshSimple(CCollisionObj* collisionObject, ABVERTEX1LNT* buffer, int& returnVCount, Vector3f objectPos, float radius, float extentUp, float extentDown) {
	returnVCount = 0;

	Vector3f startPath, endPath, intersection, normal;

	int objectHit = 0;

	startPath.x = objectPos.x;
	startPath.y = objectPos.y + extentUp;
	startPath.z = objectPos.z;

	endPath.x = objectPos.x;
	endPath.y = objectPos.y - extentDown;
	endPath.z = objectPos.z;

	if (TRUE == collisionObject->CollisionSegmentIntersect(startPath, endPath, 50.0f, true, false, &intersection, &normal, &objectHit)) {
		// collided
		returnVCount = 6;

		Matrix44f rotationMat;
		rotationMat.MakeIdentity();
		float posX, posY, posZ;

		MatrixARB::FaceTowardPoint(normal, &rotationMat);

		// VX0
		buffer[0].x = -radius;
		buffer[0].z = -.1f;
		buffer[0].y = radius;
		buffer[0].tx0.tu = 0.0f;
		buffer[0].tx0.tv = 0.0f;

		posX = buffer[0].x;
		posY = buffer[0].y;
		posZ = buffer[0].z;

		buffer[0].x = (rotationMat[0][0] * posX) + (rotationMat[1][0] * posY) + (rotationMat[2][0] * posZ);
		buffer[0].y = (rotationMat[0][1] * posX) + (rotationMat[1][1] * posY) + (rotationMat[2][1] * posZ);
		buffer[0].z = (rotationMat[0][2] * posX) + (rotationMat[1][2] * posY) + (rotationMat[2][2] * posZ);

		buffer[0].x = intersection.x + buffer[0].x;
		buffer[0].y = intersection.y + buffer[0].y;
		buffer[0].z = intersection.z + buffer[0].z;

		// VX1
		buffer[1].x = radius;
		buffer[1].z = -.1f;
		buffer[1].y = radius;
		buffer[1].tx0.tu = 1.0f;
		buffer[1].tx0.tv = 0.0f;

		posX = buffer[1].x;
		posY = buffer[1].y;
		posZ = buffer[1].z;

		buffer[1].x = (rotationMat[0][0] * posX) + (rotationMat[1][0] * posY) + (rotationMat[2][0] * posZ);
		buffer[1].y = (rotationMat[0][1] * posX) + (rotationMat[1][1] * posY) + (rotationMat[2][1] * posZ);
		buffer[1].z = (rotationMat[0][2] * posX) + (rotationMat[1][2] * posY) + (rotationMat[2][2] * posZ);

		buffer[1].x = intersection.x + buffer[1].x;
		buffer[1].y = intersection.y + buffer[1].y;
		buffer[1].z = intersection.z + buffer[1].z;

		// VX2
		buffer[2].x = -radius;
		buffer[2].z = -.1f;
		buffer[2].y = -radius;
		buffer[2].tx0.tu = 0.0f;
		buffer[2].tx0.tv = 1.0f;

		posX = buffer[2].x;
		posY = buffer[2].y;
		posZ = buffer[2].z;

		buffer[2].x = (rotationMat[0][0] * posX) + (rotationMat[1][0] * posY) + (rotationMat[2][0] * posZ);
		buffer[2].y = (rotationMat[0][1] * posX) + (rotationMat[1][1] * posY) + (rotationMat[2][1] * posZ);
		buffer[2].z = (rotationMat[0][2] * posX) + (rotationMat[1][2] * posY) + (rotationMat[2][2] * posZ);

		buffer[2].x = intersection.x + buffer[2].x;
		buffer[2].y = intersection.y + buffer[2].y;
		buffer[2].z = intersection.z + buffer[2].z;

		// VX0
		buffer[3].x = buffer[1].x;
		buffer[3].y = buffer[1].y;
		buffer[3].z = buffer[1].z;
		buffer[3].tx0.tu = buffer[1].tx0.tu;
		buffer[3].tx0.tv = buffer[1].tx0.tv;

		// VX1
		buffer[4].x = radius;
		buffer[4].z = -.1f;
		buffer[4].y = -radius;
		buffer[4].tx0.tu = 1.0f;
		buffer[4].tx0.tv = 1.0f;

		posX = buffer[4].x;
		posY = buffer[4].y;
		posZ = buffer[4].z;

		buffer[4].x = (rotationMat[0][0] * posX) + (rotationMat[1][0] * posY) + (rotationMat[2][0] * posZ);
		buffer[4].y = (rotationMat[0][1] * posX) + (rotationMat[1][1] * posY) + (rotationMat[2][1] * posZ);
		buffer[4].z = (rotationMat[0][2] * posX) + (rotationMat[1][2] * posY) + (rotationMat[2][2] * posZ);

		buffer[4].x = intersection.x + buffer[4].x;
		buffer[4].y = intersection.y + buffer[4].y;
		buffer[4].z = intersection.z + buffer[4].z;

		// VX2
		buffer[5].x = buffer[2].x;
		buffer[5].y = buffer[2].y;
		buffer[5].z = buffer[2].z;
		buffer[5].tx0.tu = buffer[2].tx0.tu;
		buffer[5].tx0.tv = buffer[2].tx0.tv;
	} // end collided
}

void ClientEngine::PendingCommandsCallback() {
	CPendingCommandObj* pObj = m_pendingObject->CallBack();
	if (!pObj)
		return; // no commands at this time

	switch (pObj->m_command) {
		case 1: {
			ATTRIB_DATA attribData;
			attribData.aeType = eAttribEventType::UseItem;
			attribData.aeShort1 = pObj->m_register1;
			attribData.aeInt1 = pObj->m_register0;
			m_multiplayerObj->QueueMsgAttribToServer(attribData);
		} break;
	}
}

void ClientEngine::ProgressUpdate(const std::string& position, BOOL init, BOOL deinit) {
	// Do Nothing
}

void ClientEngine::ProgressUpdate(const std::string& position, const std::string& message, int weight, BOOL init, BOOL deinit) {
	if (init)
		m_progressUpdateCurrent = 0;
	m_progressUpdateCurrent += weight;

	if (m_pWin32ProgDlg) {
		// Pump Messages Until Empty (20 messages max)
		if (!::MsgProcPumpUntilEmpty(20)) {
			// Quit Client
			::PostQuitMessage(0);
			return;
		}

		LoadScenePerformSplashScreen();
		if (m_progressUpdateTotal != 0)
			m_pWin32ProgDlg->UpdateProgress(std::min(m_progressUpdateCurrent * 100 / m_progressUpdateTotal, 100));
	} else {
		static EVENT_ID eventId = BAD_EVENT_ID;
		if (eventId == BAD_EVENT_ID)
			eventId = m_dispatcher.RegisterEvent("ZoneLoadingProgressEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);

		if (eventId != BAD_EVENT_ID) {
			if (m_dispatcher.HasHandler(eventId)) {
				IEvent* e = m_dispatcher.MakeEvent(eventId);
				(*e->OutBuffer()) << 0 << message;
				(*e->OutBuffer()) << m_progressUpdateCurrent << m_progressUpdateTotal;
				m_dispatcher.QueueEvent(e);
			}
		}
	}

	if (m_progressUpdateTotal == 0 && deinit)
		m_progressUpdateTotal = m_progressUpdateCurrent;

	return;
}

// Helper function that rotates the point about the pivot by angle radians in each world space axis.
void ClientEngine::RotatePointAroundPoint(Vector3f& outPos, const Vector3f& point, const Vector3f& pivot, const Vector3f& angle) {
	// move pivot to origin
	outPos = point - pivot;

	// Apply rotations in correct order
	// to get back to the matrix from which they came, if
	// they came from MatrixARB::RotationMatrixToEulerAnglesXYZ().
	Matrix44f rotMat, tmpMat;
	ConvertRotationZ(angle.z, out(rotMat));
	ConvertRotationY(angle.y, out(tmpMat));
	rotMat = rotMat * tmpMat;
	ConvertRotationX(angle.x, out(tmpMat));
	rotMat = rotMat * tmpMat;

	// rotate point
	Vector3f result = TransformVector(rotMat, outPos);

	// move back
	outPos = result + pivot;
}

bool ClientEngine::ApplyGiftPicture(int objId, int giftId, bool thumbNail) {
	//check if a gift picture exists, download if from kaneva if not
	std::string giftPictureFileName = GetExternalTexture(GIFT_PICTURE, false, giftId, 0);
	if (giftPictureFileName.empty())
		return false;
	if (objId < 0)
		return true; // just do d/l

	return false;
}

bool ClientEngine::ApplyFriendPicture(int objId, int friendId, bool thumbNail) {
	//check if a friend's picture exists, download if from kaneva if not
	std::string friendPictureFileName = GetExternalTexture(FRIEND_PICTURE, thumbNail, friendId, 0);
	if (friendPictureFileName.empty())
		return false;
	if (objId < 0)
		return true; // just do d/l

	CDynamicPlacementObj* pDPO = GetDynamicObject(objId);
	if (!pDPO) {
		LogError("GetDynamicObject() FAILED - objId=" << objId);
		return false;
	}

	if (pDPO->GetCustomTextureUrl() != friendPictureFileName) {
		pDPO->SetFriendId(friendId);
		pDPO->SetCustomTexture(true, std::make_shared<CustomTextureProvider>(friendPictureFileName));
		pDPO->UpdateCustomMaterials();

		//update server
		IEvent* e = new UpdatePictureFrameFriendEvent(objId, friendId, "");
		if (!pDPO->IsLocal())
			e->AddTo(SERVER_NETID);

		m_dispatcher.QueueEvent(e);
	}

	return true;
}

#define DRF_WINDOW_REFRESH_DELAY 0 // drf - disabled for now

void ClientEngine::FlagWindowRefresh() {
	if (!m_shutdown) {
#if (DRF_WINDOW_REFRESH_DELAY == 0)
		// Do Window Refresh Now
		WindowRefresh();
#else
		// Start Refresh Timer
		m_timeRefresh = fTime::TimeMs();
#endif
	}
}

void ClientEngine::NeedsWindowRefresh() {
	// Refresh Timer Active ?
	if (m_timeRefresh == 0)
		return;

	// Refresh After 500ms
	TimeMs timeMs = fTime::TimeMs();
	TimeMs timeDelta = timeMs - m_timeRefresh;
	if (timeDelta < DRF_WINDOW_REFRESH_DELAY)
		return;

	// Do Window Refresh Now
	WindowRefresh();
	m_timeRefresh = 0;
}

void ClientEngine::WindowRefresh() {
	// Do Window Refresh Now
	LogInfo("START");
	OnLostDevice();
	OnResetDevice();
	LogInfo("END");
}

bool ClientEngine::DownloadPatchFile(const char* exgFileName, IEvent* e, FileSize fileSize) {
	return DownloadPatchFile(exgFileName, "\\GameFiles\\", e, fileSize);
}

bool ClientEngine::DownloadPatchFile(const char* exgFileName, const char* folder, IEvent* e, FileSize fileSize) {
	// Form Destination Path 'c:\Program Files (x86)\Kaneva\Star\3296\<folder>\<exgFileName>'
	std::string filePathDest = PathAdd(PathAdd(PathBase(), folder), exgFileName);

	// Form Dashed Folder '\folder\slashed\' -> 'folder_dashed'
	std::string folderDashed = StrReplace(folder, '\\', '_');
	folderDashed.erase(0, folderDashed.find_first_not_of('_'));
	folderDashed.erase(folderDashed.find_last_not_of('_') + 1);

	// Form Source File Name '<folderDashed>--<zonename.exg>.<compressTypeExt>'
	std::string fileNameSource;
	StrBuild(fileNameSource, folderDashed << "---" << exgFileName);
	fileNameSource = ::CompressTypeExtAdd(fileNameSource);

	// Form Source Url '<patchUrl>/<patchVersion>/<fileNameSource>'
	std::string urlSource;
	urlSource = PathAdd(PatchUrl(), fileNameSource);

	// JONNY - A patch file comes before the 'pre-load' stuff and should be
	// queued at a higher priority,  so that when it is queued
	// (by the zonedownloader.lua) it goes first and we're not stuck
	// waiting for some stupid .pak file to download while all the
	// pre-load files are downloaded first.
	//
	// Patch files > pre-load > other stuff.
	//
	// Pre-load stuff can (and in the case of login, is) queued
	// before the patch files for the zone, so some or most of it
	// gets a chance to download before the patch files.
	DownloadFileAsync(ResourceType::PATCHFILE, urlSource, DL_PRIO_HIGH, filePathDest, fileSize, true, e, false);
	return TRUE;
}

void ClientEngine::CancelAllDownloads() {
	m_pDM->CancelAll();
}

bool ClientEngine::ArmPreLoadProgress(IEvent* e) {
	return m_pDM->ArmPreLoadProgress(e);
}

long ClientEngine::GetRegKeyValue(HKEY hKey, const char* subKey, const char* name, std::string* pData) {
	//HKEY_LOCAL_MACHINE
	HKEY phkResult;
	DWORD type;
	DWORD ret;
	if ((ret = RegOpenKeyExW(hKey, Utf8ToUtf16(subKey).c_str(), 0, KEY_QUERY_VALUE, &phkResult)) == ERROR_SUCCESS) {
		std::wstring nameW = Utf8ToUtf16(name);

		// Determine length of buffer required.
		DWORD size = 0;
		if (RegQueryValueExW(phkResult, nameW.c_str(), nullptr, &type, nullptr, &size) != ERROR_SUCCESS)
			size = 0;

		// Allocate buffer and get data.
		std::vector<wchar_t> buffer(size);
		ret = RegQueryValueExW(phkResult, nameW.c_str(), nullptr, &type, (LPBYTE)buffer.data(), &size);
		if (ret == ERROR_SUCCESS) {
			if (type == REG_SZ) {
				if (pData)
					*pData = Utf16ToUtf8(buffer.data());
			} else {
				// Wrong data type stored in registry.
				ret = ERROR_BAD_FORMAT;
			}
		}
		RegCloseKey(phkResult);
	}
	return ret;
}

bool ClientEngine::PreloadAnimationByHash(const std::string& sAnimHash, int prioDelta) {
	std::stringstream ss(sAnimHash);
	UINT64 animHash;
	ss >> animHash;
	if (!animHash)
		return false;

	// search all entities for matching anim hash
	for (POSITION pos = m_movObjRefModelList->GetHeadPosition(); pos;) {
		auto pMO = dynamic_cast<CMovementObj*>(m_movObjRefModelList->GetNext(pos));
		if (!pMO || !pMO->getSkeleton())
			continue;

		auto animIndex = pMO->getSkeleton()->getAnimationIndexByHash(animHash);
		auto pSAH = pMO->getSkeleton()->getSkeletonAnimHeaderByIndex(animIndex);
		if (!pSAH)
			continue;

		return PreloadAnimation(pSAH->m_animGlid, prioDelta);
	}

	// search all APDs for matching anim hash
	std::vector<UINT64> allEquippableKeys;
	m_equippableRM->GetAllResourceKeys(allEquippableKeys);
	for (const auto& resKey : allEquippableKeys) {
		auto pAIP = dynamic_cast<CArmedInventoryProxy*>(m_equippableRM->GetResource(resKey));
		if (!pAIP)
			continue;

		CArmedInventoryObj* pAIO = pAIP->GetInventoryObj();
		if (!pAIO)
			continue;

		auto pSkeleton = pAIO->getSkeleton(eVisualType::ThirdPerson);
		if (!pSkeleton)
			pSkeleton = pAIO->getSkeleton(eVisualType::FirstPerson);
		if (!pSkeleton)
			continue;

		auto animIndex = pSkeleton->getAnimationIndexByHash(animHash);
		auto pSAH = pSkeleton->getSkeletonAnimHeaderByIndex(animIndex);
		if (!pSAH)
			continue;

		return PreloadAnimation(pSAH->m_animGlid, prioDelta);
	}

	return false;
}

bool ClientEngine::PreloadAnimation(GLID animGlid, int prioDelta) {
	if (!IS_VALID_GLID(animGlid))
		return false;

	auto pAP = AnimationRM::Instance()->GetAnimationProxy(animGlid);
	if (!pAP)
		return false;

	pAP->SetDownloadPriority(DM_PRELOAD_PRIORITY + prioDelta);
	pAP->SetPreload(true);
	pAP->GetAnimData();
	return true;
}

bool ClientEngine::PreloadMesh(const std::string& sMeshHash, int prioDelta) {
	auto meshHash = _strtoui64(sMeshHash.c_str(), nullptr, 10);
	if (!meshHash)
		return false;

	auto pMP = MeshRM::Instance()->GetMeshProxy(meshHash);
	if (!pMP)
		return false;

	pMP->SetDownloadPriority(DM_PRELOAD_PRIORITY + prioDelta);
	pMP->SetPreload(true);
	pMP->LoadMesh();
	return true;
}

#ifdef EXTERNAL_PING
// 2016.10.14
// There is no intention of turning this back on.  However, this is conditioned in the event we should need
// turn it back on quickly.
//
// This can be removed after one release cycle.
//
bool ClientEngine::ExternalPing(double& pingTimeMs, const std::string& host, const std::string& service) {
	pingTimeMs = 0.0;

	// Use Default Host ?
	std::string hostName = host;
	if (hostName.empty())
		hostName = m_pingLocation;

	// Startup WSA Support
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		LogError("FAILED WSAStartup() - iResult=" << iResult);
		return false;
	}

	LogInfo("host: " << hostName);

	// Get IP Address (via DNS)
	struct addrinfo hints;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	struct addrinfo* result = nullptr;
	DWORD dwRetval = getaddrinfo(hostName.c_str(), service.c_str(), &hints, &result);
	if (dwRetval != 0) {
		LogError("FAILED getaddrinfo() - rv=" << dwRetval);
		WSACleanup();
		return false;
	}
	if (!result) {
		LogError("FAILED getaddrinfo() - result=<null>");
		WSACleanup();
		return false;
	}

	//ip address begins at at index 2.
	//inet_ntoa will give us our string.
	//ipAddr is of format "209.85.165.99"
	struct in_addr addr;
	addr.S_un.S_un_b.s_b1 = result->ai_addr->sa_data[2];
	addr.S_un.S_un_b.s_b2 = result->ai_addr->sa_data[3];
	addr.S_un.S_un_b.s_b3 = result->ai_addr->sa_data[4];
	addr.S_un.S_un_b.s_b4 = result->ai_addr->sa_data[5];
	char* ipAddr = inet_ntoa(addr);
	freeaddrinfo(result);

	// Cleanup WSA Support
	WSACleanup();

	HANDLE icmphandle = IcmpCreateFile();
	if (icmphandle == INVALID_HANDLE_VALUE) {
		LogError("FAILED IcmpCreateFile()");
		return false;
	}

	// Average 5 Pings With Max 5 Second Timeout
	const size_t numPings = 5;
	const DWORD timeoutMs = (5 * MS_PER_SEC);
	size_t successPings = 0;
	ULONG totalPingTimeMs = 0;
	for (size_t i = 0; i < numPings; ++i) {
		char reply[sizeof(icmp_echo_reply) + 8];
		icmp_echo_reply* iep = (icmp_echo_reply*)&reply;
		ZeroMemory(reply, sizeof(reply));
		DWORD dwReturn = IcmpSendEcho(icmphandle, inet_addr(ipAddr), 0, 0, nullptr, reply, sizeof(reply), timeoutMs);
		if (dwReturn == 0) {
			LogError("FAILED IcmpSendEcho()");
		} else {
			ULONG timeMs = iep->RoundTripTime;
			++successPings;
			totalPingTimeMs += timeMs;
			if (timeMs > (timeoutMs / 2))
				++i; // half the pings if half the timeout
		}
	}
	if (successPings) {
		pingTimeMs = ((double)totalPingTimeMs) / ((double)successPings) + 0.1; // avoid impossible 0.0 ping time
		LogInfo("pingHost=" << hostName << " pings=" << successPings << " pingTimeMs=" << pingTimeMs);
	}

	IcmpCloseHandle(icmphandle);

	return (successPings > 0);
}
#endif

// wholescreen -> rect=(-1, -1, -1, -1)
std::string ClientEngine::ScreenshotCapture(const RECT& rect, std::string destFile, char* folderOverride) {
	// This function will fail entirely if no part of the
	// app window is on the primary monitor in a multimonitor setup.

	std::string fullPath;

	std::string pathBase = PathBase();

	// Make sure that the path ends with a backslash
	if (pathBase[pathBase.size() - 1] != '\\')
		pathBase.append("\\");

	// Don't put a trailing slash on this or _stat will fail
	std::string folder = pathBase;
	if (folderOverride)
		folder = folder + folderOverride; // Allow folder overrides
	else
		folder = folder + "Screenshots"; // Default folder

	// Check for the existence of the screenshots directory
	bool exist = FileHelper::Exists(folder);

	// If it exists and is a directory, OR if it doesn't exist
	// but we successfully create the directory ...
	if (exist || (!exist && (_mkdir(folder.c_str()) == 0))) {
		// Generate a filename of the form:
		// '<avatar name>_<year>_<day>_<month>_<time>_<random>.jpg'
		// This makes it so that sorting screenshots
		// by name will also sort them chronologically

		// Try to get the avatar name from EntObjRuntime
		// (so we can prepend it to the generated filename)

		std::string username = "anonymous";

		auto pMO = GetMovementObjMe();
		if (pMO) {
			if (!pMO->getName().empty())
				username = pMO->getName();
			size_t limit = 20; // don't allow arbitrarily long usernames
			if (username.size() > limit)
				username = username.substr(0, limit);
		}

		unsigned long suffix = rand() & 0xffff;

		SYSTEMTIME st;
		ZeroMemory(&st, sizeof(st));
		GetLocalTime(&st);

		char basename[500]; // filename doesn't include path, and is generated by us,
		// so we can guarantee it doesn't overflow
		if (destFile == "") {
			_snprintf_s(
				basename,
				sizeof(basename),
				sizeof(basename) - 1,
				"%s_%04d_%02d_%02d_%02d%02d_%04x",
				username.c_str(), st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, suffix);
		} else {
			// passed in a destination file.
			_snprintf_s(
				basename,
				sizeof(basename),
				sizeof(basename) - 1,
				"%s",
				destFile.c_str());
		}

		fullPath = folder + "\\" + basename + ".jpg";

		// Flag Capture Screen
		m_captureScreen = true;
		m_captureScreenRect = rect;
		m_captureScreenPath = fullPath;
	}

	return m_captureScreenPath;
}

bool ClientEngine::DragEnter(int type, const std::string& str, int mouseX, int mouseY, unsigned int keyState, unsigned int flags) {
	if (!m_bAllowDragDrop)
		return false;

	ASSERT(!GetDragDropModeOn());

	LogInfo("'" << str << "'");

	m_currDropType = type;
	m_currDropContent = str;
	DragDropEvent* e = new DragDropEvent(true, false, m_currDropType, m_currDropContent, mouseX, mouseY, keyState);
	m_dispatcher.QueueEvent(e);
	return true;
}

void ClientEngine::DragOver(int mouseX, int mouseY, unsigned int keyState, unsigned int flags) {
	if (!m_bAllowDragDrop || !GetDragDropModeOn())
		return;

	DragDropEvent* e = new DragDropEvent(false, false, m_currDropType, m_currDropContent, mouseX, mouseY, keyState);
	m_dispatcher.QueueEvent(e);
}

void ClientEngine::DragLeave(void) {
	if (!m_bAllowDragDrop || !GetDragDropModeOn())
		return;

	LogInfo("'" << m_currDropContent << "'");

	m_currDropType = DROP_NONE;
	m_currDropContent.clear();

	DragLeaveEvent* e = new DragLeaveEvent();
	m_dispatcher.QueueEvent(e);
}

bool ClientEngine::Drop(int type, const std::string& str, int mouseX, int mouseY, unsigned int keyState, unsigned int flags) {
	// This function is called inside the same thread that calls RegisterDragDrop, which should be the main thread.
	// Drag drop events are delivered as OLE messages, through message loop to client engine.
	// See: https://msdn.microsoft.com/en-us/library/windows/desktop/ms678405%28v=vs.85%29.aspx

	// Drag And Drop Allowed ?
	if (!m_bAllowDragDrop)
		return false;

	LogInfo("'" << str << "'");

	TraceInfo trace;
	RayTrace(&trace, GetPickRayOrigin(), GetPickRayDirection(), FLT_MAX, ObjectType::ANY, RAYTRACE_VISIBLE);
	if (trace.objType != ObjectType::NONE && m_dispatcher.HasHandler(TargetedPositionEventEx::ClassId())) {
		// Send advanced targeting event
		ULONG mouseButtonState = 0;
		if (m_mouseState.ButtonDownLeft())
			mouseButtonState |= TargetedPositionEventEx::LBUTTONDOWN;
		if (m_mouseState.ButtonDownRight())
			mouseButtonState |= TargetedPositionEventEx::RBUTTONDOWN;

		ObjectType targetType = trace.objType;
		int targetId = trace.objId;

		TargetedPositionEventEx* tpe = new TargetedPositionEventEx(
			trace.intersection.x, trace.intersection.y, trace.intersection.z,
			trace.normal.x, trace.normal.y, trace.normal.z,
			mouseButtonState, keyState,
			(int)targetType,
			(int)targetId,
			"");

		// Local delivery
		m_dispatcher.QueueEvent(tpe);
	}

	DragDropEvent* e = new DragDropEvent(false, true, type, str, mouseX, mouseY, keyState);
	m_dispatcher.QueueEvent(e);

	m_currDropType = DROP_NONE;
	m_currDropContent.clear();
	return true;
}

BOOL ClientEngine::GetDragDropModeOn() const {
	BOOL ret = m_currDropType != DROP_NONE;

#ifdef _DEBUG
	BOOL ret2 = !m_currDropContent.empty();
	ASSERT(ret == ret2);
#endif

	return ret;
}

std::string ClientEngine::GetURL(const char* urlTag, bool bReturnCompleted) {
	if (_stricmp(urlTag, WOKURL_TAG) == 0)
		return m_wokUrl;
	else if (_stricmp(urlTag, CLIENTDEFAULTAUTHURL_TAG) == 0) {
		std::string authUrl = m_authUrl;
		if (fillInAuthUrl(authUrl)) {
			return authUrl;
		} else {
			LogError("fillInAuthUrl() FAILED");
		}
	} else if (_stricmp(urlTag, CLIENTDEFAULTLOGINURL_TAG) == 0)
		return m_loginUrl;
	else if (_stricmp(urlTag, CLIENTDEFAULT_RUNTIME_REPORT_URL_TAG) == 0)
		return m_runtimeReportUrl;
	else if (_stricmp(urlTag, CLIENTDEFAULTSTARTAPPURL_TAG) == 0)
		return m_startAppUrl;
	else if (_stricmp(urlTag, CLIENTDEFAULTIMAGEURL_TAG) == 0)
		return m_imageUrl;
	else if (_stricmp(urlTag, CLIENTDEFAULTIMAGETHUMBURL_TAG) == 0)
		return m_imageThumbUrl;
	else if (_stricmp(urlTag, CLIENTDEFAULTFRIENDURL_TAG) == 0)
		return m_friendUrl;
	else if (_stricmp(urlTag, CLIENTDEFAULTFRIENDTHUMBURL_TAG) == 0)
		return m_friendThumbUrl;
	else if (_stricmp(urlTag, CLIENTDEFAULTGIFTURL_TAG) == 0)
		return m_giftUrl;
	else if (_stricmp(urlTag, CLIENTDEFAULTGIFTTHUMBURL_TAG) == 0)
		return m_giftThumbUrl;
	else if (_stricmp(urlTag, CLIENTDEFAULTIMAGESERVER_TAG) == 0)
		return m_imageServerUrl;
	else if (_stricmp(urlTag, CLIENTDEFAULTINVENTORYSERVER_TAG) == 0)
		return m_inventoryServerUrl;
	else if (_stricmp(urlTag, ZONECUSTOM_TAG) == 0)
		return m_zoneCutomizationsUrl;
	else if (_stricmp(urlTag, ERROR_URL_TAG) == 0)
		return m_errorReportUrl;
	else if (_stricmp(urlTag, CLIENTDEFAULTSTARTAPPURL_TAG) == 0)
		return m_startAppUrl;

	return "";
}

bool ClientEngine::UploadFileAsync(const char* uploadFile, const char* url, int prio, ValueList* pAddlValues, bool authFirst, const char* authUrl, IEvent* e, DWORD eventMask) {
	if (!m_pUM || !uploadFile || !url)
		return false;
	return m_pUM->UploadFileAsync(uploadFile, url, prio, pAddlValues, authFirst, authUrl, e, eventMask);
}

bool ClientEngine::UploadFileAsync(const char* uploadName, char* dataBuf, DWORD dataBufLen, const char* url, int prio, ValueList* pAddlValues, bool authFirst, const char* authUrl, IEvent* e, DWORD eventMask) {
	if (!m_pUM || !uploadName || !url || !dataBuf || !dataBufLen)
		return false;
	return m_pUM->UploadFileAsync(uploadName, dataBuf, dataBufLen, url, prio, pAddlValues, authFirst, authUrl, e, eventMask);
}

bool ClientEngine::UploadMultiFilesAsync(const std::vector<std::string>& uploadFiles, const char* url, int prio, ValueList* pAddlValues, bool authFirst, const char* authUrl, IEvent* e, DWORD eventMask) {
	if (!m_pUM || uploadFiles.empty() || !url)
		return false;
	return m_pUM->UploadMultiFilesAsync(uploadFiles, url, prio, pAddlValues, authFirst, authUrl, e, eventMask);
}

int ClientEngine::SendScriptServerMenuEvent(std::vector<std::string> params) {
	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::MenuEvent, 0);
	*sse->OutBuffer() << (int)params.size();
	for (const auto& param : params)
		*sse->OutBuffer() << param;
	sse->AddTo(SERVER_NETID);
	return m_dispatcher.QueueEvent(sse);
}

// DRF - Used For Accessory & Animation Imports
void ClientEngine::Mount(int aiConfig, int mountType, bool bLocal) {
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;
	GenAndMount(pMO, aiConfig, mountType, bLocal);
}

// DRF - Used For Accessory & Animation Imports
void ClientEngine::Unmount() {
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;
	DeMount(pMO);
}

bool ClientEngine::GetPostureMatrices(const GLID& animGLID, TimeMs animTimeMs, std::map<std::string, Matrix44f>& posture) {
	if (animGLID == GLID_INVALID)
		return false;

	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	auto pRS = pMO->getSkeleton();
	if (!pRS)
		return false;

	// Animation not loaded - call LoadAnimationToPlayer first ?
	auto pSAH = pRS->getSkeletonAnimHeaderByGlid(animGLID);
	if (!pSAH || !pSAH->IsDataReady())
		return false;

	if (pSAH->GetNumBones() == 0) {
		// No data, return empty list
		posture.clear();
		return true;
	}

	// Now return the data
	for (UINT boneLoop = 0; boneLoop < pRS->getBoneCount(); boneLoop++) {
		auto pBO = pRS->getBoneByIndex(boneLoop);
		if (!pBO)
			continue;

		std::string boneName = pBO->m_boneName;
		auto pBAO = pSAH->GetBoneAnimationObj(boneLoop);
		if (!pBAO)
			continue;

		Matrix44f& matrix = posture[boneName];
		if (!pBAO->GetBoneMatrixByTime(animTimeMs, matrix))
			matrix.MakeIdentity();
	}

	return true;
}

ContentMetadata* ClientEngine::GetCachedUGCMetadataPtr(const GLID& glid) {
	return ContentService::Instance()->ContentMetadataCacheGetPtr(glid);
}

bool ClientEngine::GetCachedUGCMetadata(const GLID& glid, ContentMetadata& md) {
	return ContentService::Instance()->ContentMetadataCacheGet(md, glid);
}

inline ScriptServerEvent::UploadAssetTypes classifyAssetByPath(std::string path) {
	size_t extensionStart = path.find_last_of('.');
	if (extensionStart == std::string::npos) {
		return ScriptServerEvent::UndefinedAssetType;
	}
	std::string extension = path.substr(extensionStart + 1, 4);
	if (STLCompareIgnoreCase(extension, "dds") == 0) {
		return ScriptServerEvent::MenuDDS;
	}
	if (STLCompareIgnoreCase(extension, "tga") == 0) {
		return ScriptServerEvent::MenuTGA;
	}
	if (STLCompareIgnoreCase(extension, "xml") == 0) {
		return ScriptServerEvent::MenuXml;
	}
	if (STLCompareIgnoreCase(extension, "bmp") == 0) {
		return ScriptServerEvent::MenuBMP;
	}
	if (STLCompareIgnoreCase(extension, "png") == 0) {
		return ScriptServerEvent::MenuPNG;
	}
	if (STLCompareIgnoreCase(extension, "jpg") == 0) {
		return ScriptServerEvent::MenuJPG;
	}
	if (STLCompareIgnoreCase(extension, "jpeg") == 0) {
		return ScriptServerEvent::MenuJPEG;
	}
	if (STLCompareIgnoreCase(extension, "lua") == 0) {
		// Look at name of folder the file is in.
		size_t folderEnd = path.find_last_of('\\');
		if (folderEnd == std::string::npos) {
			folderEnd = path.find_last_of('/');
		}
		if (folderEnd != std::string::npos) {
			size_t folderStart = path.find_last_of('\\', folderEnd - 1);
			if (folderStart == std::string::npos) {
				folderStart = path.find_last_of('/', folderEnd - 1);
			}
			if (folderStart != std::string::npos) {
				std::string folder = path.substr(folderStart + 1, folderEnd - folderStart - 1);
				if (folder.compare(MENUSCRIPTSDIR) == 0) {
					return ScriptServerEvent::MenuScript;
				} else if (folder.compare(SCRIPTSERVERDIR) == 0) {
					return ScriptServerEvent::ServerScript;
				} else if (folder.compare(SAVEDSCRIPTDIR) == 0) {
					return ScriptServerEvent::SavedScript;
				} else if (folder.compare(ACTIONSCRIPTDIR) == 0) {
					return ScriptServerEvent::ActionItem;
				} else if (folder.compare(ACTIONINSTANCEDIR) == 0) {
					return ScriptServerEvent::ActionInstance;
				} else {
					return ScriptServerEvent::SavedScript; // make this the default if I have script on desktop or something
				}
			}
		}
	}
	if (STLCompareIgnoreCase(extension, "json") == 0) {
		size_t folderEnd = path.find_last_of('\\');
		if (folderEnd == std::string::npos) {
			folderEnd = path.find_last_of('/');
		}
		if (folderEnd != std::string::npos) {
			size_t folderStart = path.find_last_of('\\', folderEnd - 1);
			if (folderStart == std::string::npos) {
				folderStart = path.find_last_of('/', folderEnd - 1);
			}
			if (folderStart != std::string::npos) {
				std::string folder = path.substr(folderStart + 1, folderEnd - folderStart - 1);
				if (folder.compare(ACTIONSCRIPTDIR) == 0) {
					return ScriptServerEvent::ActionItemParams;
				} else if (folder.compare(ACTIONINSTANCEDIR) == 0) {
					return ScriptServerEvent::ActionInstanceParams;
				}
			}
		}
	}
	return ScriptServerEvent::UndefinedAssetType;
}

bool ClientEngine::UploadAssetTo3DApp(const std::string& filename, const std::string& fullPath, int assetType, unsigned long seekPos, unsigned long objPlacementId) {
	if (filename.empty() || fullPath.empty()) {
		std::string msg = "Provided file name or file path is invalid";
		SendChatMessage(eChatType::System, msg);
		LogError(msg);
		return false;
	}

	std::wstring fullPathW = Utf8ToUtf16(fullPath);

	// load and add the script data to the event's buffer.
	// http://msdn.microsoft.com/en-us/library/aa365430(VS.85).aspx
	// On the subject of OpenFile, says, "Only use this function with 16-bit versions of Windows. For newer applications, use the CreateFile function."
	// I include this as a note to myself, so I remember why I didn't use OpenFile.
	HANDLE file = ::CreateFileW(fullPathW.c_str(), GENERIC_READ, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (file == INVALID_HANDLE_VALUE) {
		LogError("CreateFile() FAILED - '" << fullPath << "'");
		return false;
	}
	CloseHandle(file);

	if (seekPos == 0 && detectLocal3DAppAssetChanges(filename, (ScriptServerEvent::UploadAssetTypes)assetType) == FALSE) {
		if (!deleteFile(fullPath.c_str(), TRUE)) {
			LogError("DeleteFile() FAILED - '" << fullPath << "'");
		}
		std::string folder, extension, bakFilePath;
		classifyAssetType((ScriptServerEvent::UploadAssetTypes)assetType, folder, extension);
		bakFilePath = folder + "~" + filename + extension;
		deleteFile(bakFilePath.c_str(), TRUE);
#ifdef _ClientOutput
		IEvent* completeEvent = GetDispatcher()->MakeEvent(GetDispatcher()->GetEventId("3DAppAssetPublishCompleteEvent"));
		if (completeEvent) {
			*completeEvent->OutBuffer() << filename << (int)assetType << 1 << "File unmodified";
			GetDispatcher()->QueueEvent(completeEvent);
		}
#endif
		return true;
	}
	file = ::CreateFileW(fullPathW.c_str(), GENERIC_READ, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	//
	// Read the LUA file (which should always be an ASCII file?)
	// and load the code into the event for transmission to
	// the server.
	//
	const unsigned long UPLOAD_SCRIPT_BUFFER_SIZE = 256;
	unsigned long buffer[UPLOAD_SCRIPT_BUFFER_SIZE];
	DWORD bytesRead;
	DWORD totBytesRead = 0;
	BOOL res = FALSE;
	short chunk = 0;
	if (seekPos > 0) {
		chunk = 1;
	}
	DWORD fileSize = ::GetFileSize(file, 0);
	if (fileSize == INVALID_FILE_SIZE) {
		LogError("GetFileSize() FAILED - '" << fullPath << "'");
		CloseHandle(file);
		return false;
	}

	if (assetType == ScriptServerEvent::UndefinedAssetType) {
		// Client will guess what type of asset this is based on path.
		assetType = classifyAssetByPath(fullPath);
		if (assetType == ScriptServerEvent::UndefinedAssetType) {
			LogError("Undefined Asset Type - '" << fullPath << "'");
			CloseHandle(file);
			return false;
		}
	}

	if (seekPos > 0) {
		if (SetFilePointer(file, seekPos, 0, FILE_BEGIN) == INVALID_SET_FILE_POINTER) {
			LogError("SetFilePointer() FAILED - '" << fullPath << "'");
			return false;
		}
	}

	while ((res = ::ReadFile(file, buffer, sizeof(buffer), &bytesRead, nullptr)) == TRUE && bytesRead > 0 && totBytesRead < ScriptServerEvent::BYTES_BEFORE_ACK) {
		if (ScriptServerEvent::FILE_TRANSFER_PACKET_DELAY > 0)
			Sleep(ScriptServerEvent::FILE_TRANSFER_PACKET_DELAY);
		totBytesRead += bytesRead;
		ScriptServerEvent* se = new ScriptServerEvent(ScriptServerEvent::UploadScript);
		(*se->OutBuffer()) << filename;
		(*se->OutBuffer()) << (unsigned long)sizeof(buffer); // bytesSent
		(*se->OutBuffer()) << (unsigned long)bytesRead; // bytesRead
		for (unsigned int i = 0; i < UPLOAD_SCRIPT_BUFFER_SIZE; i++) {
			(*se->OutBuffer()) << buffer[i];
		}
		(*se->OutBuffer()) << ((seekPos + totBytesRead) >= fileSize ? ScriptServerEvent::LAST_CHUNK : chunk);
		(*se->OutBuffer()) << assetType;
		(*se->OutBuffer()) << fullPath; // added full Client Path to event.
		(*se->OutBuffer()) << objPlacementId;

		se->AddTo(SERVER_NETID);
		m_dispatcher.QueueEvent(se);
		chunk++;
	}
	::CloseHandle(file);

	// Send special there is more packet that will cause another upload request after this one processes.
	if (totBytesRead >= ScriptServerEvent::BYTES_BEFORE_ACK) {
		ScriptServerEvent* se = new ScriptServerEvent(ScriptServerEvent::UploadScript);
		(*se->OutBuffer()) << filename;
		(*se->OutBuffer()) << (unsigned long)0; // bytesSent
		(*se->OutBuffer()) << (unsigned long)0; // bytesRead

		(*se->OutBuffer()) << ScriptServerEvent::BUT_WAIT_THERES_MORE;
		(*se->OutBuffer()) << assetType;
		(*se->OutBuffer()) << fullPath;
		(*se->OutBuffer()) << objPlacementId;

		se->AddTo(SERVER_NETID);
		m_dispatcher.QueueEvent(se);
	}

	if (res == FALSE) {
		std::string msg = "Error reading: " + fullPath;
		SendChatMessage(eChatType::System, msg);
		LogError(msg);
		return false;
	}

	return true;
}

bool ClientEngine::Import3DAppAsset(int assetType, const std::string& path) {
	std::wstring pathW = Utf8ToUtf16(path);
	HANDLE file = ::CreateFile(pathW.c_str(), GENERIC_READ, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (file == INVALID_HANDLE_VALUE) {
		LogError("CreateFile() FAILED - '" << path << "'");
		return false;
	}
	::CloseHandle(file);
	size_t slashPos = path.find_last_of('\\');
	std::string fileName = path.substr(slashPos + 1);

	std::string localDevFolder, extension;
	if (!classifyAssetType((ScriptServerEvent::UploadAssetTypes)assetType, localDevFolder, extension)) {
		return false;
	}

	std::string localDevTarget = PathAdd(localDevFolder, fileName);
	if (!::CopyFileW(pathW.c_str(), Utf8ToUtf16(localDevTarget).c_str(), FALSE)) {
		LogError("CopyFile() FAILED - '" << path << "' => '" << localDevTarget << "'");
		return false;
	}
	std::string runtimeDest;
	switch (assetType) {
		case ScriptServerEvent::ServerScript:
		case ScriptServerEvent::SavedScript:
		case ScriptServerEvent::ActionItem:
		case ScriptServerEvent::ActionInstance:
		case ScriptServerEvent::ActionItemParams:
		case ScriptServerEvent::ActionInstanceParams:
			// Don't have to do anything for server stuff
			return true;
		case ScriptServerEvent::MenuScript:
			runtimeDest = PathAdd(GetAppScriptDir(), fileName);
			break;
		case ScriptServerEvent::MenuDDS:
		case ScriptServerEvent::MenuTGA:
		case ScriptServerEvent::MenuXml:
		case ScriptServerEvent::MenuBMP:
		case ScriptServerEvent::MenuPNG:
		case ScriptServerEvent::MenuJPG:
		case ScriptServerEvent::MenuJPEG:
			runtimeDest = PathAdd(GetAppMenuDir(), fileName);
			break;
		default:
			return false;
	}
	if (!::CopyFileW(pathW.c_str(), Utf8ToUtf16(runtimeDest).c_str(), FALSE)) {
		LogError("CopyFile() FAILED - '" << path << "' => '" << runtimeDest << "'");
		return false;
	}
	return true;
}

// Import an asset file from a string and write out to LocalDev.
bool ClientEngine::Import3DAppAssetFromString(int assetType, const std::string& baseName, const std::string& content, std::string& outFullPath) {
	std::string localDevFolder, extension;
	if (!classifyAssetType((ScriptServerEvent::UploadAssetTypes)assetType, localDevFolder, extension)) {
		LogError("Not a valid asset type: " << assetType);
		return false;
	}

	std::string fileName = baseName + extension;
	if (!IsValidFileName(fileName)) {
		LogError("Not a valid file name: " << fileName);
		return false;
	}

	std::string localDevTarget = PathAdd(localDevFolder, fileName);
	FILE* fp;
	fopen_s(&fp, localDevTarget.c_str(), "w+");
	if (!fp) {
		LogError("Error opening [" << localDevTarget.c_str() << "] for write");
		return false;
	}

	if (fwrite(content.c_str(), 1, content.size(), fp) != content.size()) {
		LogError("Error writing into [" << localDevTarget.c_str() << "]");
		fclose(fp);
		return false;
	}

	fclose(fp);

	outFullPath = localDevTarget;
	return true;
}

// User dropped lua script onto a specific object
bool ClientEngine::ProcessDroppedLuaScript(const std::string& fullPath, int objType, int objId) {
	if (objType != ObjTypeToInt(ObjectType::DYNAMIC)) { // Currently only handles lua on DO
		LogError("Attempt to upload script on non-Dynamic object type " << objType);
		return false;
	}

	if (fullPath.length() == 0) {
		LogError("Failure uploading script with no full path");
		return false;
	}

	m_pendingLuaScriptFilePath = fullPath;
	m_pendingLuaScriptObjectId = objId;

	// First check name conflicts: compare it with a list of existing scripts.
	// Ask server for the script list. Server will send down a ScriptClientEvent::GetAllScripts in response
	IEvent* e = new ScriptServerEvent(ScriptServerEvent::GetAllScripts);
	e->AddTo(SERVER_NETID);
	m_dispatcher.QueueEvent(e);

	return true;
}

void ClientEngine::NotifyServerOfAssetUpload(int uploadId, std::string const& name, int assetType) {
	ScriptServerEvent* se = new ScriptServerEvent(ScriptServerEvent::ProcessUGCUpload);
	(*se->OutBuffer()) << uploadId;
	(*se->OutBuffer()) << name;
	(*se->OutBuffer()) << assetType;
}

bool ClientEngine::UploadDroppedLuaScript(const std::string& fullPath, int objId) {
	int lastslash = fullPath.find_last_of("\\");
	std::string filename = fullPath.substr(lastslash + 1, fullPath.length());
	std::string filenameNoExtension = filename.substr(0, filename.find_last_of("."));

	if (UploadAssetTo3DApp(filenameNoExtension, fullPath, (int)ScriptServerEvent::SavedScript, 0, 0)) {
		IEvent* e = new ScriptServerEvent(ScriptServerEvent::AttachScript, objId);
		(*e->OutBuffer()) << filename;
		e->AddTo(SERVER_NETID);
		m_dispatcher.QueueEvent(e);

		m_pendingLuaScriptFilePath = "";
		m_pendingLuaScriptObjectId = 0;

		return true;
	} else {
		return false;
	}
}

void ClientEngine::UGCAvatarUpdate() {
	for (POSITION pos = m_movObjList->GetHeadPosition(); pos;) {
		auto pMO = (CMovementObj*)m_movObjList->GetNext(pos);
		if (!pMO->getSkeleton() && IS_UGC_GLID(pMO->getUGCActorGlobalId())) {
			// DRF - Bug? - Only Do This For Already Cached Objects ??
			GLID glid = pMO->getUGCActorGlobalId();
			if (!ContentService::Instance()->ContentMetadataIsCached(glid))
				continue;

			// IResource
			TResource<StreamableDynamicObject>* pResource = CDynamicPlacementObj::GetDynamicObjectResource(glid);
			assert(pResource != nullptr);

			// Dynamic object ready?
			StreamableDynamicObject* dob = pResource->GetAsset();
			UGCActor* actorData = m_ugcActorDB->getActorData(pMO->getUGCActorGlobalId());
			if (dob && actorData) {
				SkeletonProxy* skeleton_proxy = SkeletonRM::Instance()->GetSkeletonProxy(dob->getSkeletonGLID());
				if (skeleton_proxy && skeleton_proxy->GetRuntimeSkeleton()) {
					pMO->setSkeleton(RuntimeSkeleton::New());
					pMO->getSkeleton()->setArmedInventoryList(new CArmedInventoryList());

					CBoneList* pNewBoneList = nullptr;
					if (skeleton_proxy->GetRuntimeSkeleton()->getBoneList()) {
						skeleton_proxy->GetRuntimeSkeleton()->getBoneList()->Clone(&pNewBoneList, nullptr);
					} else {
						pNewBoneList = new CBoneList();
						CBoneObject* pBone = new CBoneObject();
						pBone->m_boneFrame = new CFrameObj(nullptr);
						pNewBoneList->AddTail(pBone);
					}
					pMO->getSkeleton()->setBoneList(pNewBoneList);

					pMO->getSkeleton()->initFromDynamicObject(dob, glid);
					pMO->getSkeleton()->initBones();

					pMO->getSkeleton()->setLocalBoundingBox(dob->getBoundBox());

					setOverheadLabels(pMO);

					pMO->setUgcActor(actorData);
				}
			}
		}
	}
}

void ClientEngine::RequestItemAnimDataAsync(int DOGlid) {
	CStringA requestURL(m_itemAnimationsUrl.c_str());

	if (requestURL.GetLength() > 0) {
		IEvent* e = m_dispatcher.MakeEvent(m_getItemAnimationsId, 0);
		(*e->OutBuffer()) << DOGlid;
		requestURL.Replace("{0}", numToString(DOGlid, "%d").c_str());
		GetBrowserPageAsync((const char*)requestURL, e);
	}
}

void ClientEngine::LoadUGCActor(const GLID& actorGLID) {
	GetActorDatabase()->CreateActorBasedOnDO(actorGLID, actorGLID);

	DynamicObjectRM* doRM = GetDynamicObjectRM();
	if (!doRM)
		return;

	// IResource
	TResource<StreamableDynamicObject>* pResource = CDynamicPlacementObj::GetDynamicObjectResource(actorGLID);
	assert(pResource != nullptr);
	pResource->GetAsset(); // Trigger download if not already
}

void ClientEngine::addAnimationUGCActor(const GLID& actorGlid, const GLID& animGlid, const std::string& name) {
	m_ugcActorDB->AddSupportedAnimation(actorGlid, animGlid, name);
}

SIZE ClientEngine::getClientRectSize() {
	CRect r;
	ClientEngine_CFrameWnd::GetClientRect(r);
	return r.Size();
}

void ClientEngine::ShutdownResourceManagers() {
	ResourceLoader::Instance().Shutdown();
#if 0 // No shutdown required for resource managers anymore. Only the resource loader.
	if (m_animationRM)
		m_animationRM->Shutdown();
	if (m_soundRM)
		m_soundRM->Shutdown();
	if (m_dynamicObjectRM)
		m_dynamicObjectRM->Shutdown();
	if (m_equippableRM)
		m_equippableRM->Shutdown();
	if (m_particleRM)
		m_particleRM->Shutdown();
	if (m_textureRM)
		m_textureRM->Shutdown();
	if (m_meshRM)
		m_meshRM->Shutdown();
	if (m_skeletonRM)
		m_skeletonRM->Shutdown();
#endif
}

void ClientEngine::LogResources(int resources) {
	// Log All Content Metadata Cached
	if (resources & 0x01)
		ContentService::Instance()->ContentMetadataCacheLog(true);

	// Log All Resource Managers
	if (resources & 0x02) {
		if (m_animationRM)
			m_animationRM->LogResources();
		if (m_soundRM)
			m_soundRM->LogResources();
		if (m_dynamicObjectRM)
			m_dynamicObjectRM->LogResources();
		if (m_equippableRM)
			m_equippableRM->LogResources();
		if (m_particleRM)
			m_particleRM->LogResources();
		if (m_textureRM)
			m_textureRM->LogResources();
		if (m_meshRM)
			m_meshRM->LogResources();
		if (m_skeletonRM)
			m_skeletonRM->LogResources();

		if (m_pTextureDB)
			m_pTextureDB->TextureMapLog();
	}

	// Log All Dynamic Objects
	LogDynamicObjectList(true);

	// Log All Movement Objects
	if (resources & 0x04)
		LogMovementObjectList(true);

	// Log All Media Placements
	if (resources & 0x08)
		MediaPlacementsLog(true);

	// Log All Sound Placements
	if (resources & 0x10)
		SoundPlacementsLog(true);

	// Log All Mouse Over Settings
	MOS_Log();

	// Log All Triggers
	LogTriggers();
}

void ClientEngine::enableResourceManagers(bool bEnable) {
	if (m_dynamicObjectRM)
		m_dynamicObjectRM->setQueueEnabled(bEnable);

	if (m_soundRM)
		m_soundRM->setQueueEnabled(bEnable);

	if (m_animationRM)
		m_animationRM->setQueueEnabled(bEnable);

	if (m_equippableRM)
		m_equippableRM->setQueueEnabled(bEnable);

	if (m_particleRM)
		m_particleRM->setQueueEnabled(bEnable);

	if (m_textureRM)
		m_textureRM->setQueueEnabled(bEnable);
}

void ClientEngine::cancelPendingResources() {
	if (m_dynamicObjectRM)
		m_dynamicObjectRM->cancelAllPendingTasks();

	if (m_soundRM)
		m_soundRM->cancelAllPendingTasks();

	if (m_animationRM)
		m_animationRM->cancelAllPendingTasks();

	if (m_equippableRM)
		m_equippableRM->cancelAllPendingTasks();

	if (m_particleRM)
		m_particleRM->cancelAllPendingTasks();

	if (m_textureRM)
		m_textureRM->cancelAllPendingTasks();
}

unsigned int ClientEngine::getMovementRestriction() const {
	unsigned int combinedRestriction = 0;
	for (auto keyValuePair : m_restrictMovements) {
		combinedRestriction |= keyValuePair.second;
	}

	if (m_DOSpawnMonitor->IsSpawning() || IsZoneLoadingUIActive())
		combinedRestriction |= RestrictMovement_Forward | RestrictMovement_Backward | RestrictMovement_Jump;

	return combinedRestriction;
}

std::string ClientEngine::movementRestrictionStr(const MovementRestrictionSource& source) {
	const static std::string mrSrc[MR_SRCS] = {
		"DEATH",
		"MENU",
		"PATHFINDER",
		"LOOKAT",
		"DDR"
	};

	// Invalid MovementRestrictSource ?
	if (source >= MR_SRCS) {
		LogError("MR<INVALID> - source=" << (int)source);
		return "MR<INVALID>";
	}

	// Build String
	unsigned int restriction = m_restrictMovements[(int)source];
	std::string restrictStr;
	if (restriction == RestrictMovement_None) {
		StrAppend(restrictStr, " NONE");
	} else if (restriction == RestrictMovement_All) {
		StrAppend(restrictStr, " ALL");
	} else {
		if (restriction & RestrictMovement_MouseLook)
			StrAppend(restrictStr, " MouseLook");
		if (restriction & RestrictMovement_Forward)
			StrAppend(restrictStr, " Forward");
		if (restriction & RestrictMovement_Backward)
			StrAppend(restrictStr, " Backward");
		if (restriction & RestrictMovement_Turn)
			StrAppend(restrictStr, " Turn");
		if (restriction & RestrictMovement_Jump)
			StrAppend(restrictStr, " Jump");
	}
	StrBuild(restrictStr, "MR<" << mrSrc[(int)source] << "> " << restrictStr);
	return restrictStr;
}

void ClientEngine::setMovementRestriction(const MovementRestrictionSource& source, const MovementRestriction& mr, bool enable) {
	// Invalid MovementRestrictSource ?
	if (source >= MR_SRCS) {
		LogError("MR<INVALID> - source=" << (int)source);
		return;
	}

	// Update Restriction
	unsigned int restriction = m_restrictMovements[(int)source];
	if (enable)
		restriction |= (unsigned int)mr;
	else
		restriction &= ~(unsigned int)mr;
	if (m_restrictMovements[(int)source] == restriction)
		return;
	m_restrictMovements[(int)source] = restriction;
	LogInfo(movementRestrictionStr(source));
}

bool ClientEngine::classifyAssetType(ScriptServerEvent::UploadAssetTypes assetType, std::string& folder, std::string& extension) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	std::string withGameId(pICE->LocalDevAppDir());
	_mkdir(withGameId.c_str());
	withGameId = withGameId + "\\GameFiles";
	_mkdir(withGameId.c_str());

	switch (assetType) {
		case ScriptServerEvent::ServerScript:
			folder = withGameId + "\\ScriptServerScripts\\";
			extension = ".lua";
			break;
		case ScriptServerEvent::MenuXml:
			folder = withGameId + "\\Menus\\";
			extension = ".xml";
			break;
		case ScriptServerEvent::MenuDDS:
			folder = withGameId + "\\Menus\\";
			extension = ".dds";
			break;
		case ScriptServerEvent::MenuTGA:
			folder = withGameId + "\\Menus\\";
			extension = ".tga";
			break;
		case ScriptServerEvent::MenuBMP:
			folder = withGameId + "\\Menus\\";
			extension = ".bmp";
			break;
		case ScriptServerEvent::MenuPNG:
			folder = withGameId + "\\Menus\\";
			extension = ".png";
			break;
		case ScriptServerEvent::MenuJPG:
			folder = withGameId + "\\Menus\\";
			extension = ".jpg";
			break;
		case ScriptServerEvent::MenuJPEG:
			folder = withGameId + "\\Menus\\";
			extension = ".jpeg";
			break;
		case ScriptServerEvent::MenuScript:
			folder = withGameId + "\\MenuScripts\\";
			extension = ".lua";
			break;
		case ScriptServerEvent::SavedScript:
			folder = withGameId + "\\ScriptServerScripts\\";
			_mkdir(folder.c_str());
			folder = folder + "SavedScripts\\";
			extension = ".lua";
			break;
		case ScriptServerEvent::ActionItem:
		case ScriptServerEvent::ActionItemParams:
			folder = withGameId + "\\ScriptServerScripts\\";
			_mkdir(folder.c_str());
			folder = folder + "ActionItems\\";
			_mkdir(folder.c_str());
			extension = assetType == ScriptServerEvent::ActionItem ? ".lua" : ".json";
			break;
		case ScriptServerEvent::ActionInstance:
		case ScriptServerEvent::ActionInstanceParams:
			folder = withGameId + "\\ScriptServerScripts\\";
			_mkdir(folder.c_str());
			folder = folder + "ActionInstances\\";
			_mkdir(folder.c_str());
			extension = assetType == ScriptServerEvent::ActionInstance ? ".lua" : ".json";
			break;
		default:
			return false;
	}
	_mkdir(folder.c_str());
	return true;
}

bool ClientEngine::createAssetFromChunks(std::string fileName, short chunkNumber, BYTE* buffer, unsigned long bytesRead, ScriptServerEvent::UploadAssetTypes type, bool continuedDownload) {
	auto pCE = ClientEngine::Instance();
	if (!pCE)
		return false;

	std::string folder, extension;
	if (!classifyAssetType(type, folder, extension))
		return false;

	std::string errMsg;
	std::string fname(PathAdd(folder.c_str(), fileName.c_str()));
	fname += ".tmp";
	std::wstring fnameW = Utf8ToUtf16(fname);

	DWORD openFlags = OPEN_ALWAYS;
	if (chunkNumber == 0 && continuedDownload == false) { // first chunk of first block
		openFlags = CREATE_ALWAYS;
	}
	HANDLE file = ::CreateFileW(fnameW.c_str(), GENERIC_WRITE, 0, nullptr, openFlags, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (file == INVALID_HANDLE_VALUE && chunkNumber == 0) {
		// the directory may not be created, create it and then try creating the file again
		::CreateDirectory(Utf8ToUtf16(folder).c_str(), nullptr);
		file = ::CreateFileW(fnameW.c_str(), GENERIC_WRITE, 0, nullptr, openFlags, FILE_ATTRIBUTE_NORMAL, nullptr);
	}

	if (file == INVALID_HANDLE_VALUE) {
		LogError("Cannot open: " << fname << " " << errorNumToString(GetLastError(), errMsg));
		return false;
	}

	if (::SetFilePointer(file, 0, 0, FILE_END) == INVALID_SET_FILE_POINTER) { // move to end
		LogError("Seek to end in : " << fname << " " << errorNumToString(GetLastError(), errMsg));
		::CloseHandle(file);
		if (!pCE->deleteFile(fname.c_str(), FALSE)) {
			LogError("Failure to delete : " << fname << " after fatal error.  File may be corrupt.");
		}
		return false;
	}

	DWORD bytesWritten = 0;
	if (bytesRead > 0 && !::WriteFile(file, buffer, bytesRead, &bytesWritten, 0)) {
		LogError("Cannot write to: " << fname << " " << errorNumToString(GetLastError(), errMsg));
		::CloseHandle(file);
		if (!pCE->deleteFile(fname.c_str(), FALSE)) {
			LogError("Failure to delete : " << fname << " after fatal error.  File may be corrupt.");
		}
		return false;
	}

	::CloseHandle(file);

	if (chunkNumber < 0) { // last chunk
		std::string newFname(fileName);
		newFname += extension;
		std::string fullPath = PathAdd(folder.c_str(), newFname.c_str());
		std::wstring fullPathW = Utf8ToUtf16(fullPath);
		if (!::MoveFileEx(fnameW.c_str(), fullPathW.c_str(), MOVEFILE_REPLACE_EXISTING)) {
			pCE->deleteFile(fname.c_str(), FALSE);
			LogError("Failed to rename : " << fname << " to " << newFname << " " << errorNumToString(GetLastError(), errMsg));
			return false;
		} else {
			std::string backPath = PathAdd(folder.c_str(), (std::string("~") + newFname).c_str());
			std::wstring backPathW = Utf8ToUtf16(backPath);
			if (!::CopyFile(fullPathW.c_str(), backPathW.c_str(), FALSE)) {
				LogWarn("Failed to backup asset: " << fname << " " << errorNumToString(GetLastError(), errMsg));
			}
			if (!::SetFileAttributes(backPathW.c_str(), FILE_ATTRIBUTE_HIDDEN)) {
				LogWarn("Failed to hide backup: " << fname << " " << errorNumToString(GetLastError(), errMsg));
			}
			// Let anyone listening know the download completed
#ifdef _ClientOutput
			IDispatcher* pd = pCE->GetDispatcher();
			if (!pd)
				return false;
			IEvent* completeEvent = pd->MakeEvent(pd->GetEventId("3DAppAssetDownloadCompleteEvent"));
			if (completeEvent) {
				*completeEvent->OutBuffer() << fullPath << (int)type;
				pd->QueueEvent(completeEvent);
			}
#endif
			return true;
		}
	}
	return true;
}

bool ClientEngine::requestSourceAssetFromAppServer(int type, std::string name, int seekPos = 0) {
	if (type == ScriptServerEvent::ServerScript || type == ScriptServerEvent::SavedScript || type == ScriptServerEvent::MenuXml ||
		type == ScriptServerEvent::MenuDDS || type == ScriptServerEvent::MenuTGA || type == ScriptServerEvent::MenuScript ||
		type == ScriptServerEvent::MenuBMP || type == ScriptServerEvent::MenuPNG || type == ScriptServerEvent::MenuJPG ||
		type == ScriptServerEvent::MenuJPEG || type == ScriptServerEvent::ActionItem || type == ScriptServerEvent::ActionInstance ||
		type == ScriptServerEvent::ActionItemParams || type == ScriptServerEvent::ActionInstanceParams) {
		//check to see if File exists
		std::string folder, extension;
		if (!classifyAssetType((ScriptServerEvent::UploadAssetTypes)type, folder, extension)) {
			return false;
		}
		std::string fullPath = folder + name + extension;

		if (seekPos == 0) { // Initial request?
			// Verify existing local files
			if (jsFileExists(fullPath.c_str(), nullptr)) {
				// File already exists and modified, just fire the event to tell script it is ready to be opened
				if (detectLocal3DAppAssetChanges(name, (ScriptServerEvent::UploadAssetTypes)type)) {
#ifdef _ClientOutput
					IDispatcher* pd = GetDispatcher();
					if (!pd)
						return false;
					IEvent* completeEvent = pd->MakeEvent(m_3DAppAssetDownloadCompleteEventId);
					if (completeEvent) {
						*completeEvent->OutBuffer() << fullPath << (int)type;
						pd->QueueEvent(completeEvent);
					} else {
						LogError("Failed to create 3DAppAssetDownloadCompletedEvent");
						return false;
					}
#endif
					return true;
				}

				// File already exists but not modified: replace it with the latest server copy
				LogDebug("File [" << fullPath << "] already exists but not modified: replace it with the latest server copy");
				deleteFile(fullPath.c_str(), TRUE);
			}

			// If there is an existing backup delete it because we will download fresh copy
			std::string backupFile = folder + "~" + name + extension;
			deleteFile(backupFile.c_str(), TRUE);
		}

		ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::DownloadAsset, 0);

		*sse->OutBuffer() << (int)type;
		*sse->OutBuffer() << (int)seekPos; // starting position in the file
		// TODO check name for "safe" input characters.  Don't want to create any vulnerabilities.
		*sse->OutBuffer() << name.c_str();

		sse->AddTo(SERVER_NETID);
		if (m_dispatcher.QueueEvent(sse)) {
			return true;
		}
	}
	return false;
}

int ClientEngine::countDevToolAssets(int assetType) {
	int count = 0;
	std::string folder, extension;
	if (!classifyAssetType((ScriptServerEvent::UploadAssetTypes)assetType, folder, extension)) {
		return -1;
	}
	std::string searchStr = folder + "*" + extension;

	// jsEnumFiles is now a safer options which takes care of the 8.3 short file name issue. See CS13529 for detail.
	jsEnumFiles ef(Utf8ToUtf16(searchStr).c_str());
	while (ef.next()) {
		if (ef.isHidden() || ef.isDirectory()) {
			continue;
		}

		count++;
	}
	return count;
}

bool ClientEngine::getDevToolAssetInfo(int assetType, int index, std::string& name, std::string& fullPath, bool& modified) {
	int count = 0;
	name.clear();
	fullPath.clear();
	modified = false;

	std::string folder, extension;
	if (!classifyAssetType((ScriptServerEvent::UploadAssetTypes)assetType, folder, extension)) {
		return false;
	}
	std::string searchStr = folder + "*" + extension;

	jsEnumFiles ef(Utf8ToUtf16(searchStr).c_str());
	while (ef.next()) {
		if (ef.isHidden() || ef.isDirectory()) {
			continue;
		}

		count++;
		if (index == count) {
			//found the file we are looking for
			std::string currentFileName = Utf16ToUtf8(ef.currentFileName());
			name = currentFileName.c_str();
			size_t pos = name.rfind('.');
			name = name.substr(0, pos);
			fullPath = folder + currentFileName;
			modified = detectLocal3DAppAssetChanges(name, (ScriptServerEvent::UploadAssetTypes)assetType);
			return true;
		}
	}

	return false;
}

std::string ClientEngine::getDevToolAssetPath(int assetType) {
	std::string folder, extension;
	if (!classifyAssetType((ScriptServerEvent::UploadAssetTypes)assetType, folder, extension)) {
		return "";
	}

	return folder;
}

bool ClientEngine::detectLocal3DAppAssetChanges(std::string const& name, ScriptServerEvent::UploadAssetTypes type) {
	// somewhat expensive function if files are identical.  HopeFully we are talking mostly small script files and
	// menu textures.
	const unsigned int compareBufferSize = 4096;

	std::string folder, extension;
	classifyAssetType(type, folder, extension);
	std::string backupPath = folder + "~" + name + extension;
	std::string modifiedPath = folder + name + extension;
	HANDLE bakHandle, modHandle;

	modHandle = ::CreateFile(Utf8ToUtf16(modifiedPath).c_str(), GENERIC_READ, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
	ASSERT(modHandle != nullptr);
	if (!modHandle) {
		LogError("Error opening file: [" << modifiedPath << "], err=" << GetLastError());
		return false;
	}

	bakHandle = ::CreateFile(Utf8ToUtf16(backupPath).c_str(), GENERIC_READ, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_HIDDEN, nullptr);
	if (bakHandle == INVALID_HANDLE_VALUE) {
		// No backup copy, consider file as modified
		CloseHandle(modHandle);
		return true;
	}

	// Compare file with backup copy
	DWORD modSize, bakSize;
	modSize = ::GetFileSize(modHandle, nullptr);
	bakSize = ::GetFileSize(bakHandle, nullptr);
	if (modSize != bakSize) {
		CloseHandle(modHandle);
		CloseHandle(bakHandle);
		return true;
	}
	BYTE* bakData = new BYTE[compareBufferSize];
	BYTE* modData = new BYTE[compareBufferSize];
	DWORD modBytesRead, bakBytesRead;
	unsigned int bytesRemaining = bakSize;
	bool returnVal = false;
	while (bytesRemaining > 0) {
		ZeroMemory(modData, compareBufferSize);
		ZeroMemory(bakData, compareBufferSize);
		if (!ReadFile(bakHandle, bakData, compareBufferSize, &bakBytesRead, nullptr)) {
			if (bytesRemaining - bakBytesRead != 0) {
				LogError("Read backup failed: " << backupPath);
				returnVal = true;
				break;
			}
		}
		if (!ReadFile(modHandle, modData, compareBufferSize, &modBytesRead, nullptr)) {
			LogError("Read modified failed: " << modifiedPath);
			returnVal = true;
			break;
		}
		if (modBytesRead != bakBytesRead) {
			returnVal = true;
			break;
		}
		if (memcmp(bakData, modData, compareBufferSize) != 0) {
			returnVal = true;
			break;
		}
		bytesRemaining = bytesRemaining - bakBytesRead;
	}
	delete[] bakData;
	delete[] modData;
	CloseHandle(modHandle);
	CloseHandle(bakHandle);
	return returnVal;
}

void ClientEngine::handleBuildPlaceMouseUpdate(Vector3f rayOrigin, Vector3f rayDir, bool mouseInClient) {
	Vector3f p0(0.0f, m_clickPlaceData.placementHeight, 0.0f); // a point on the plane
	Vector3f planeNormal(0.0f, 1.0f, 0.0f);
	Vector3f temp;
	temp = p0 - rayOrigin;
	float denominator = rayDir.Dot(planeNormal);
	if (denominator != 0.0f) {
		// line does not intersect plane if d == 0
		float d = temp.Dot(planeNormal) / denominator;
		if (d > 0) {
			// line intersects behind camera if d < 0
			m_clickPlaceData.position = rayOrigin + (d * rayDir);
			// Do the grid rounding
			m_clickPlaceData.position.x = m_clickPlaceData.gridSpacing * floor(m_clickPlaceData.position.x / m_clickPlaceData.gridSpacing + 0.5f);
			m_clickPlaceData.position.y = m_clickPlaceData.gridSpacing * floor(m_clickPlaceData.position.y / m_clickPlaceData.gridSpacing + 0.5f);
			m_clickPlaceData.position.z = m_clickPlaceData.gridSpacing * floor(m_clickPlaceData.position.z / m_clickPlaceData.gridSpacing + 0.5f);
			SetDynamicObjectPosition(m_clickPlaceData.placementId, m_clickPlaceData.position);
		}
	}
}

void ClientEngine::adjustBuildPlaceHeight(float newHeight) {
	m_clickPlaceData.placementHeight = newHeight;
}

void ClientEngine::setGridSpacing(float spacing) {
	m_clickPlaceData.gridSpacing = spacing;
}

float ClientEngine::getGridSpacing() {
	return m_clickPlaceData.gridSpacing;
}

// Return FALSE if file exists but cannot be deleted (directory, read-only, etc). Return TRUE otherwise (deleted or not found)
BOOL ClientEngine::deleteFile(const char* fullPath, bool defyReadonlyAttr) {
	jsVerifyReturn(fullPath != nullptr, TRUE);
	std::wstring fullPathW = Utf8ToUtf16(fullPath);
	bool isDir = false;
	if (!jsFileExists(fullPath, &isDir)) {
		LogDebug("File not found: [" << fullPath << "]");
		return TRUE;
	}

	if (isDir) {
		LogWarn("Cannot delete a directory: [" << fullPath << "]");
		return FALSE;
	}

	DWORD attr = ::GetFileAttributesW(fullPathW.c_str());
	if (attr == INVALID_FILE_ATTRIBUTES) {
		LogWarn("Error stating file: [" << fullPath << "], err=" << GetLastError());
		return FALSE;
	}

	if (attr & FILE_ATTRIBUTE_READONLY) {
		LogInfo("Remove read-only attribute for: [" << fullPath << "]");
		::SetFileAttributesW(fullPathW.c_str(), attr & ~FILE_ATTRIBUTE_READONLY);
	}

	return ::DeleteFileW(fullPathW.c_str());
}

bool ClientEngine::setOverheadLabels(CMovementObj* pMO) {
	// Assume My Avatar ?
	if (!pMO)
		pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	// Base Class Sets Overhead Labels
	if (!ClientBaseEngine::setOverheadLabels(pMO))
		return false;

	// Send Title Set Event ?
	if (!pMO->isNetworkEntity()) {
		IEvent* titleEvent = m_dispatcher.MakeEvent(m_titleSetEventId);
		if (titleEvent) {
			*titleEvent->OutBuffer() << pMO->getTitle();
			m_dispatcher.QueueEvent(titleEvent);
		}
	}

	return true;
}

HINSTANCE ClientEngine::shellExec(const char* url, const char* verb, const char* args) {
	return ShellExecuteW(AfxGetMainWnd()->m_hWnd, Utf8ToUtf16(verb).c_str(), Utf8ToUtf16(url).c_str(), Utf8ToUtf16(args).c_str(), nullptr, SW_SHOWNORMAL);
}

void ClientEngine::moveEntitySubstitute(CMovementObj* pMO, const Vector3f& newPos, const Vector3f& newDir) {
	if (!pMO || !pMO->hasSubstitute())
		return;

	ObjectType substititeType = ObjectType::NONE;
	int substituteId = 0;
	pMO->getSubstitute(substititeType, substituteId);
	if (substititeType != ObjectType::DYNAMIC)
		return;

	CDynamicPlacementObj* pDPO = GetDynamicObject(substituteId);
	if (!pDPO)
		return;

	Matrix44f matrix;
	if (!pDPO->GetWorldMatrix(matrix))
		return;

	const Vector3f& curLocSub = matrix.GetTranslation();
	const Vector3f& curDirSub = matrix.GetRowZ();

	// Ignore Insignificant Changes
	bool isSignificant = false || (abs(newPos.x - curLocSub.x) > 0.5) || (abs(newPos.y - curLocSub.y) > 0.5) || (abs(newPos.z - curLocSub.z) > 0.5) || (abs(newDir.x - curDirSub.x) > 0.005) || (abs(newDir.y - curDirSub.y) > 0.005) || (abs(newDir.z - curDirSub.z) > 0.005);
	if (!isSignificant)
		return;

	IEvent* e = new MoveAndRotateDynamicObjectEvent(
		substituteId,
		newPos.x,
		newPos.y,
		newPos.z,
		newDir.x,
		newDir.y,
		newDir.z,
		newDir.z,
		newDir.y,
		-newDir.x);

	m_dispatcher.QueueEvent(e);
}

bool ClientEngine::AppReadStartCfgFile(const IKEPConfig& cfg) {
	// ED-7506 - Camera Auto-Elevation
	m_cfgCamAutoAzimuth = cfg.Get(CAM_AUTO_AZIMUTH_TAG, CAM_AUTO_AZIMUTH_DEFAULT);
	m_cfgCamAutoElevation = cfg.Get(CAM_AUTO_ELEVATION_TAG, CAM_AUTO_ELEVATION_DEFAULT);

	m_dispatchTimeInGame = (unsigned)cfg.Get("dispatch_time_in_game", (int)m_dispatchTimeInGame);
	m_dispatchTimeLoading = (unsigned)cfg.Get("dispatch_time_loading", (int)m_dispatchTimeLoading);
	m_eventQueueStatsInterval = cfg.Get("event_queue_stats_interval", m_eventQueueStatsInterval);
	ASSERT(m_eventQueueStatsInterval == 0 || m_eventQueueStatsInterval >= 1000); // Alert if interval too small

	LogInfo("dispatch_time_in_game      = " << m_dispatchTimeInGame);
	LogInfo("dispatch_time_loading      = " << m_dispatchTimeLoading);
	LogInfo("event_queue_stats_interval = " << m_eventQueueStatsInterval);

	return true;
}

bool ClientEngine::AppWriteStartCfgFile(IKEPConfig& cfg) {
	// ED-7506 - Camera Auto-Elevation
	cfg.SetValue(CAM_AUTO_AZIMUTH_TAG, m_cfgCamAutoAzimuth);
	cfg.SetValue(CAM_AUTO_ELEVATION_TAG, m_cfgCamAutoElevation);

	cfg.SetValue("dispatch_time_in_game", (int)m_dispatchTimeInGame);
	cfg.SetValue("dispatch_time_loading", (int)m_dispatchTimeLoading);
	cfg.SetValue("event_queue_stats_interval", m_eventQueueStatsInterval);

	return true;
}

bool ClientEngine::DownloadFileAsync(ResourceType jobType, const std::string& url, DownloadPriority prio, const std::string& destFile, FileSize expectedFileSize, bool compressed, IEvent* event, bool preload) {
	ASSERT(m_pDM != nullptr);

	if (m_pDM) {
		// Download File Asynchronously
		DownloadInfo* pDI = new DownloadInfo(jobType, url, prio, destFile, expectedFileSize, compressed, event, GetDispatcher(), preload);
		return m_pDM->DownloadFileAsync(pDI);
	} else {
		ASSERT(false);
		LogError("DownloadManager not available");
		return false;
	}
}

bool ClientEngine::DownloadFileAsyncWithHandler(ResourceType jobType, const std::string& url, DownloadPriority prio, DownloadHandler* handler, bool preload) {
	ASSERT(m_pDM != nullptr);

	if (m_pDM) {
		// Download File Asynchronously
		DownloadInfo* pDI = new DownloadInfo(jobType, url, prio, handler, preload);
		return m_pDM->DownloadFileAsync(pDI);
	} else {
		ASSERT(false);
		LogError("DownloadManager not available");
		return false;
	}
}

bool ClientEngine::DownloadResourceAsync(IResource* res, bool compressed, IEvent* event, bool preload) {
	ASSERT(m_pDM != nullptr);

	if (m_pDM) {
		// Download File Asynchronously
		DownloadInfo* pDI = new ResourceDownloadTask(res, compressed, event, GetDispatcher(), preload);
		return m_pDM->DownloadFileAsync(pDI);
	} else {
		ASSERT(false);
		LogError("DownloadManager not available");
		return false;
	}
}

void ClientEngine::UpdateDownloadPriorities() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "UpdateDownloadPriorities");

#ifdef LOAD_RESOURCE_BY_DISTANCE
	static TimeMs lastUpdateTimestamp = 0;
	const TimeMs updateIntervalMs = 2000; // Update every two seconds
	if (fTime::ElapsedMs(lastUpdateTimestamp) < updateIntervalMs) {
		return;
	}

	Matrix44f viewMatrix;
	if (IsDownloadPriorityVisualizationEnabled()) {
		// Construct view matrix with player matrix (for demoing purpose, discounting free look rotation so we can look around without affecting priorities)
		auto pMO = GetMovementObjMe();
		viewMatrix = Matrix44f::GetIdentity();
		if (pMO && pMO->getBaseFrame()) {
			viewMatrix = pMO->getBaseFrameMatrix();
		}

		Vector3f playerPos = viewMatrix.GetTranslation();
		playerPos.y = 0.0f; // Zero Y so that avatar can look down in the sky
		viewMatrix.GetTranslation().MakeZero(); // Rotation only
		viewMatrix.Transpose(); // Invert orthonormal matrix
		viewMatrix.GetTranslation() = -TransformPoint_NonPerspective(viewMatrix, playerPos);
	} else {
		// Use current view matrix directly (standard approach, allowing free look action)
		viewMatrix = m_view_matrix;
	}

	std::map<IResource*, double> resourceDistances;

	// 1. Update download priority for all pending dynamic object resources
	IterateObjects([this, viewMatrix, &resourceDistances](CDynamicPlacementObj* pDynPlacement) {
		if (!pDynPlacement->IsVisible()) {
			return;
		}

		// Measure distance based on dynamic object pivot points
		Vector3f offset = TransformPoint_NonPerspective(viewMatrix, pDynPlacement->GetLastLocation());
		double dist = IResource::GetDownloadPriorityModel().getNormalizedDistance(offset.x, offset.y, offset.z);

		if (!pDynPlacement->IsBaseObjInited()) {
			IResource* pRes = pDynPlacement->GetResource();
			if (pRes) {
				auto itrResDis = resourceDistances.find(pRes);
				if (itrResDis == resourceDistances.end()) {
					resourceDistances.insert(std::make_pair(pRes, dist));
				} else if (dist < itrResDis->second) {
					itrResDis->second = dist;
				}
			}
		} else {
			for (auto texHash : pDynPlacement->getTextureHashes()) {
				ASSERT(texHash != 0 && texHash != 0xFFFFFFFF);
				//auto * pRes = m_textureRM->GetResource(texHash);
				CTextureEX* pRes = GetTextureFromDatabase(texHash);
				//ASSERT(pRes != nullptr);
				if (pRes) {
					auto itrResDis = resourceDistances.find(pRes);
					if (itrResDis == resourceDistances.end()) {
						resourceDistances.insert(std::make_pair(pRes, dist));
					} else if (dist < itrResDis->second) {
						itrResDis->second = dist;
					}
				}
			}
		}
	});

	// Commit resource distance updates
	for (auto& pr : resourceDistances) {
		auto pRes = pr.first;
		auto dist = pr.second;
		pRes->SetCamDistance(dist);
	}

	// Update timestamp
	lastUpdateTimestamp = fTime::TimeMs();

#endif //defined(LOAD_RESOURCE_BY_DISTANCE)
}

void ClientEngine::EnableDownloadPriorityVisualization(bool enable) {
	LogInfo(LogBool(enable));
	CTextureEX::EnablePriorityVisualization(enable);
}

bool ClientEngine::IsDownloadPriorityVisualizationEnabled() const {
	return CTextureEX::IsPriorityVisualizationEnabled();
}

// Experimental engine options
enum class XpEO {
	MENU_TEXTURE_ATLAS = 4, // Use texture atlas for 2D menus
	MENU_ON_RENDER_TIME_MS = 5, // time between menu OnRender() calls
	FRAME_TIME_PROFILER = 6,
};

void ClientEngine::SetExperimentalEngineOption(int option, int value) {
	XpEO xpEOption = static_cast<XpEO>(option);
	switch (xpEOption) {
		case XpEO::MENU_TEXTURE_ATLAS:
			m_menuRenderOptions.m_renderOption = (value ? MenuRenderOption::USE_DRAW_QUEUE : MenuRenderOption::PASS_THROUGH);
			break;
		case XpEO::MENU_ON_RENDER_TIME_MS:
			m_menuRenderOptions.m_onRenderTimeMs = (TimeMs)value;
			break;
		case XpEO::FRAME_TIME_PROFILER:
			EnableFrameTimeProfiler(value != 0);
			break;
		default:
			assert(false);
			break;
	}
}

int ClientEngine::GetExperimentalEngineOption(int option) const {
	XpEO xpEOption = static_cast<XpEO>(option);
	switch (xpEOption) {
		case XpEO::MENU_TEXTURE_ATLAS:
			return GetBladeFactory()->GetMenuBlade()->IsRenderOptionSet(MenuRenderOption::USE_DRAW_QUEUE) ? 1 : 0;
		case XpEO::MENU_ON_RENDER_TIME_MS:
			return (int)m_menuRenderOptions.m_onRenderTimeMs;
		case XpEO::FRAME_TIME_PROFILER:
			return IsFrameTimeProfilerEnabled() ? 1 : 0;
		default:
			assert(false);
			return 0;
	}
}

bool ClientEngine::CopyD3DSurfaceFromDefaultPool(IDirect3DSurface9* source, IDirect3DSurface9* dest) {
	assert(source != nullptr && dest != nullptr);

	D3DSURFACE_DESC srcDesc, dstDesc;
	HRESULT hr;

	hr = source->GetDesc(&srcDesc);
	if (hr != D3D_OK) {
		LogWarn("Error getting source surface descriptions");
		return false;
	}

	hr = dest->GetDesc(&dstDesc);
	if (hr != D3D_OK) {
		LogWarn("Error getting dest surface descriptions");
		return false;
	}

	if (srcDesc.Pool != D3DPOOL_DEFAULT) {
		assert(false);
		LogWarn("Source surface not in D3DPOOL_DEFAULT. Actual pool type is " << srcDesc.Pool);
		return false;
	}

	if (dstDesc.Pool != D3DPOOL_MANAGED && dstDesc.Pool != D3DPOOL_SYSTEMMEM) {
		assert(false);
		LogWarn("Dest surface not in D3DPOOL_MANAGED or D3DPOOL_SYSTEMMEM. Actual pool type is " << dstDesc.Pool);
		return false;
	}

	// No stretching or format conversion support at this moment
	if (srcDesc.Width != dstDesc.Width || srcDesc.Height != dstDesc.Height || srcDesc.Format != dstDesc.Format) {
		assert(false);
		LogWarn("Source and dest surfaces are in different dimensions or formats");
		return false;
	}

	IDirect3DSurface9* surfaceSysMem = nullptr;

	if (dstDesc.Pool == D3DPOOL_SYSTEMMEM) {
		surfaceSysMem = dest;
	} else {
		// Create an temporary surface in D3DPOOL_SYSTEMMEM
		hr = GetD3DDevice()->CreateOffscreenPlainSurface(dstDesc.Width, dstDesc.Height, dstDesc.Format, D3DPOOL_SYSTEMMEM, &surfaceSysMem, nullptr);
		if (hr != D3D_OK || surfaceSysMem == nullptr) {
			LogWarn("Error creating temporary surface in D3DPOOL_SYSTEMMEM, hr = " << hr);
			return false;
		}
	}

	// Transfer from D3DPOOL_DEFAULT to D3DPOOL_SYSTEMMEM
	hr = GetD3DDevice()->GetRenderTargetData(source, surfaceSysMem);
	if (hr != D3D_OK) {
		LogWarn("Error copying from source surface into D3DPOOL_SYSTEMMEM, hr = " << hr);
		return false;
	}

	if (dstDesc.Pool == D3DPOOL_SYSTEMMEM) {
		// Done
		return true;
	}

	// Transfer from D3DPOOL_SYSTEMMEM to D3DPOOL_MANAGED if necessary
	assert(dstDesc.Pool == D3DPOOL_MANAGED);
	assert(dest != surfaceSysMem);
	bool res = true;

	// Lock SYSTEMMEM surface
	D3DLOCKED_RECT lockedRectSysMem;
	hr = surfaceSysMem->LockRect(&lockedRectSysMem, nullptr, 0);
	if (hr != D3D_OK) {
		LogWarn("Error locking temporary D3DPOOL_SYSTEMMEM surface, hr = " << hr);
		res = false;
	} else {
		// Lock MANAGED surface
		D3DLOCKED_RECT lockedRectDest;
		hr = dest->LockRect(&lockedRectDest, nullptr, 0);
		if (hr != D3D_OK) {
			LogWarn("Error locking dest D3DPOOL_MANAGED surface, hr = " << hr);
			res = false;
		} else {
			if (lockedRectSysMem.Pitch != lockedRectDest.Pitch) {
				LogWarn("Error temporary D3DPOOL_SYSTEMMEM surface has a different buffer pitch (" << lockedRectSysMem.Pitch << ") than dest surface (" << lockedRectDest.Pitch << ")");
				res = false;
			} else {
				// Transfer data between two surfaces
				memcpy(lockedRectDest.pBits, lockedRectSysMem.pBits, lockedRectDest.Pitch * dstDesc.Height);
			}

			dest->UnlockRect();
		}

		surfaceSysMem->UnlockRect();
	}

	surfaceSysMem->Release();
	return res;
}

void ClientEngine::GetWorldMapBoundary(WorldRenderOptions options, double minRadiusFilter, double& minX, double& minY, double& minZ, double& maxX, double& maxY, double& maxZ) {
	static const double MIN_RADIUS = 1E-2; // Filter out singularity objects
	static const double MAX_RADIUS = 30000; // Filter out extremely large objects (> ~5mi radius)
	static const BoundBox MAPPING_BOUNDS(-30000, -300, -30000, 30000, 300, 30000);

	minX = minY = minZ = maxX = maxY = maxZ = 0.0;

	// Determine map boundary
	size_t objectCount = 0;
	BoundBox mappedBox;
	IterateObjects([this, minRadiusFilter, options, &mappedBox, &objectCount](CDynamicPlacementObj* pPlacement) {
		if (pPlacement->IsVisible() &&
			pPlacement->GetBoundingRadius() >= MIN_RADIUS && pPlacement->GetBoundingRadius() < MAX_RADIUS &&
			pPlacement->GetBoundingRadius() >= minRadiusFilter &&
			(!pPlacement->IsMoving() && (options & WorldRenderOptions::STATIONARY_OBJECTS) != (WorldRenderOptions)0 ||
				pPlacement->IsMoving() && (options & WorldRenderOptions::MOVING_OBJECTS) != (WorldRenderOptions)0)) {
			Vector4f min, max;
			pPlacement->GetBoundingBox(min, max);

			// Skip objects completely outside maximum mapping bounds
			if (min.x > MAPPING_BOUNDS.maxX || max.x < MAPPING_BOUNDS.minX ||
				min.y > MAPPING_BOUNDS.maxY || max.y < MAPPING_BOUNDS.minY ||
				min.z > MAPPING_BOUNDS.maxZ || max.z < MAPPING_BOUNDS.minZ) {
				return;
			}

			// Clamp bounding box within maximum map range
			min.x = std::min(std::max(min.x, MAPPING_BOUNDS.minX), MAPPING_BOUNDS.maxX);
			max.x = std::min(std::max(max.x, MAPPING_BOUNDS.minX), MAPPING_BOUNDS.maxX);
			min.z = std::min(std::max(min.z, MAPPING_BOUNDS.minZ), MAPPING_BOUNDS.maxZ);
			max.z = std::min(std::max(max.z, MAPPING_BOUNDS.minZ), MAPPING_BOUNDS.maxZ);

			mappedBox.merge(BoundBox(min.x, 0, min.z, max.x, 0, max.z));
			objectCount++;
		}
	});

	if (objectCount > 0) {
		minX = mappedBox.minX;
		minY = mappedBox.minY;
		minZ = mappedBox.minZ;
		maxX = mappedBox.maxX;
		maxY = mappedBox.maxY;
		maxZ = mappedBox.maxZ;
	}
}

bool ClientEngine::GenerateWorldMap(unsigned dynTextureId, WorldRenderStyle style, WorldRenderOptions options, double minRadiusFilter, double minX, double minY, double minZ, double maxX, double maxY, double maxZ) {
	LogDebug("texture ID: " << dynTextureId << ", style: " << (int)style << ", options: " << (int)options << ", minR: " << minRadiusFilter
							<< ", bounds: (" << minX << ", " << minY << ", " << minZ << ") - (" << maxX << ", " << maxY << ", " << maxZ << ")");

	if (dynTextureId == TextureDatabase::INVALID_DYNAMIC_TEXTURE_ID) {
		LogWarn("Invalid texture ID: " << dynTextureId);
		assert(false);
		return false;
	}

	// D3D Texture
	auto pd3dTexture = GetDynamicTexture(dynTextureId);
	if (pd3dTexture == nullptr) {
		LogWarn("Target texture not found: " << dynTextureId);
		assert(false);
		return false;
	}

	HRESULT hr;
	D3DSURFACE_DESC desc;
	desc;
	hr = pd3dTexture->GetLevelDesc(0, &desc);
	if (hr != D3D_OK) {
		LogWarn("Error obtaining description for texture " << dynTextureId);
		assert(false);
		return false;
	}

	LogInfo("Using texture ID: " << dynTextureId << ", width: " << desc.Width << ", height: " << desc.Height);

	IDirect3DSurface9* pd3dSurface;
	hr = pd3dTexture->GetSurfaceLevel(0, &pd3dSurface);
	if (hr != D3D_OK || pd3dSurface == nullptr) {
		LogWarn("Error obtaining D3D surface for texture " << dynTextureId);
		assert(false);
		return false;
	}

	// Use Single-Pixel Texture At Distance (default map scale sizeX=1000)
	auto sizeX = maxX - minX;
	if (style == WorldRenderStyle::FULL_TEXTURE && (sizeX >= 200))
		style = WorldRenderStyle::OBJECT_COLOR;

	// Render selected world area with orthographic projection
	Matrix44f camMatrix, projMatrix;
	Vector4f eye((minX + maxX) / 2, 0.0f, (minZ + maxZ) / 2, 1.0f);
	Vector3f fwd(0.0f, -1.0f, 0.0f);
	Vector3f up(1.0f, 0.0f, 0.0f);
	camMatrix.SetRowX(Cross(up, fwd)); // view-to-world matrix
	camMatrix.SetRowY(up);
	camMatrix.SetRowZ(fwd);
	camMatrix.SetRowW(eye);
	D3DXMatrixOrthoLH(reinterpret_cast<D3DXMATRIX*>(&projMatrix), maxX - minX, maxZ - minZ, -1000.0f, 1000.0f);
	QueueOffscreenRenderTask(pd3dSurface, style, options, minRadiusFilter, true, &camMatrix, &projMatrix);
	return true;
}

void ClientEngine::DisableClient() {
	if (!m_multiplayerObj->isClientEnabled()) {
		LogWarn("Client not ENABLED");
	} else {
		m_multiplayerObj->DisableClient(false);
	}
}

void ClientEngine::EnableClient() {
	if (m_multiplayerObj->isClientEnabled()) {
		LogWarn("Client already ENABLED");
	} else {
		m_multiplayerObj->EnableClient(); //LOGON_CLIENT);
	}
}

bool ClientEngine::IsClientEnabled() const {
	return m_multiplayerObj->isClientEnabled();
}

// called when a zone has been loaded or re-loaded.  Could use event handling, but we need to be
// called sync not async
void ClientEngine::ZoneLoaded(const char* _exg, const char* _url, bool reloading) {
	MutexGuardLock lock(m_WldObjListMutex); // drf - crash fix
	if (!m_pWOL)
		return;

	// Synchronize our time with the server's. Time is hardcoded until server updated to transmit time to us.
	SyncGameTime(0);

	// Reset global camera collision flag
	SetCameraCollision(true);

	// Reset the "ZoneAllowsLabels" flag.
	SetZoneAllowsLabels(true);

	// Get rid of the old environment customizations.
	m_environment->DeleteEnvironmentTimeSystem();

	// Initialize camera position before removal of runtime entities, if only persist entities are used (no spawn)
	// This will record last camera's position in viewport's default camera, allowing login screen rendered at right perspective without an actor
	if (!ClientIsEnabled() && m_scenePersistList)
		m_pRuntimeCameras->Update();

	// Pause and cancel resource manager pending tasks (if not called from LoadScene nor Editor)
	if (reloading) {
		enableResourceManagers(false);
		cancelPendingResources();
		enableResourceManagers(true);
	}

	for (POSITION pos = m_pWOL->GetHeadPosition(); pos;) {
		auto pWO = (CWldObject*)m_pWOL->GetNext(pos);
		if (!pWO)
			continue;

		if (pWO->m_meshObject)
			pWO->m_meshObject->ClearExternalSource(); // Clear external source if exists

		// Wipe out runtime particle IDs for world objects to trigger particle regeneration
		pWO->m_runtimeParticleId = 0;
	}

	// These are also called by LoadScene().
	ClearAllPathFinders(false, IPathFinder::NONE);
	m_particleRM->ClearRuntimeEffects();
	DestroyMovementObjectList();
	DestroyDynamicObjects();

	m_skeletonRM->RemoveAllResources();
	std::shared_ptr<RuntimeSkeleton> skel = RuntimeSkeleton::New();
	m_skeletonRM->AddRuntimeSkeleton(0, skel.get());

	// Dispose texture resources
	ReinitAllTexturesFromDatabase();

	// Reset zone texture memory estimate
	DxDevice::Ptr()->SetExpectedTextureMemoryUsage(0, 0);

	// Reinit the environment.
	InitializeEnvironment();

	// This cache is not valid after I've zoned.  It must be
	// re-calculated the next time GenRenderList_WorldObjects is called.
	m_goodSubBox = nullptr;

	// Reset camera lock once a new zone is loaded
	m_cameraLock = false;

	// Reset 'render-labels-on-top' flag
	m_labelsOnTop = true;

	// this event has the exg, then url
	std::string exg(NULL_CK(_exg));
	std::string url(NULL_CK(_url));

	if (!url.empty()) {
		// save off the current game name so we know if we're switching
		StpUrl::ExtractGameName(url, m_gameName);
	}

	// If we are going to a different game
	if (GetGameId() != GetPrevGameId()) {
		// Don't reload menu if this is the initial spawn to a parent game (since it's already loaded in InitDirectInputAutoExecTextures)
		if (!(GetParentGameId() == 0 && GetPrevGameId() == 0)) {
			// Load menu if we are going to a different child game, or jumping back to parent
			m_menuList->Load(GetGameId(), GetParentGameId() != 0, false);
		}

		// going to a child, or between children
		if (GetParentGameId() != 0) {
			// load child game web call value list
			LoadChildGameWebCallValues();
		}
	}
	SetPrevGameId(GetGameId());

	IEvent* e = m_dispatcher.MakeEvent(m_zoneLoadedEventId);
	if (e != 0) {
		(*e->OutBuffer()) << _exg << (_url ? _url : "");
		m_dispatcher.QueueEvent(e);
	} else {
		LogFatal("dispatcher.MakeEvent() FAILED");
	}
}
// method to quit app via scripting
void ClientEngine::Quit() {
	m_closeDownCall = true;
}

void ClientEngine::ClearChatFocus() {
	m_multiplayerObj->m_chatEnabled = FALSE;
	m_multiplayerObj->m_groupChatEnabled = FALSE;
	m_multiplayerObj->m_clanChatEnabled = FALSE;
}

void ClientEngine::SpawnToRebirthPoint() {
	IEvent* e = m_dispatcher.MakeEvent(m_rebirthEventId);
	e->AddTo(SERVER_NETID);
	m_dispatcher.QueueEvent(e);
}

void ClientEngine::RequestPlayerRespawn() {
	IEvent* e = m_dispatcher.MakeEvent(m_RequestPlayerRespawnEventId);
	// Send to the client not the server, so don't call e->AddTo(SERVER_NETID)
	m_dispatcher.QueueEvent(e);
}

bool ClientEngine::WithdrawItemFromBank(int glID, int nQuantity, short invType) {
	TimeMs timeMs = fTime::TimeMs();
	if ((timeMs - m_clickHandlerStamp) < 500)
		return false;

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	if (nQuantity < 1)
		nQuantity = 1;

	m_clickHandlerStamp = timeMs;

	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::BankWithdrawItem;
	attribData.aeShort1 = pMO->getNetTraceId();
	attribData.aeShort2 = nQuantity;
	attribData.aeInt1 = glID;
	attribData.aeInt2 = invType;
	return m_multiplayerObj->QueueMsgAttribToServer(attribData);
}

bool ClientEngine::DepositItemToBank(int glID, int nQuantity, short invType) {
	TimeMs timeMs = fTime::TimeMs();
	if ((timeMs - m_clickHandlerStamp) < 500)
		return false;

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	if (nQuantity < 1)
		nQuantity = 1;

	m_clickHandlerStamp = timeMs;

	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::BankDepositItem;
	attribData.aeShort1 = pMO->getNetTraceId();
	attribData.aeShort2 = nQuantity;
	attribData.aeInt1 = glID;
	attribData.aeInt2 = invType;
	return m_multiplayerObj->QueueMsgAttribToServer(attribData);
}

BOOL ClientEngine::BuyItemFromStore(int glID, int nQuantity, int nCurrencyType) {
	if (glID == -1 || nQuantity <= 0)
		return FALSE;

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return FALSE;

	CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(glID);
	if (!pGIO)
		return FALSE;

	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::BuyItem;
	attribData.aeShort1 = pMO->getNetTraceId();
	attribData.aeShort2 = nQuantity;
	attribData.aeInt1 = glID;
	attribData.aeInt2 = nCurrencyType;
	return m_multiplayerObj->QueueMsgAttribToServer(attribData) ? TRUE : FALSE;
}

BOOL ClientEngine::SellItemToStore(int glID, int nQuantity) {
	if (glID == -1 || nQuantity <= 0) {
		LogError("Invalid glid or quantity");
		return FALSE;
	}

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return FALSE;

	CGlobalInventoryObj* invObj = m_globalInventoryDB->GetByGLID(glID);
	if (!invObj) {
		LogError("invObj is null");
		return FALSE;
	}

	LogInfo("SendAttribMsg(SELL_ITEM) - netId=" << pMO->getNetTraceId() << " glid=" << glID << " quantity=" << nQuantity);

	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::SellItem;
	attribData.aeShort1 = pMO->getNetTraceId();
	attribData.aeShort2 = nQuantity;
	attribData.aeInt1 = glID;
	return m_multiplayerObj->QueueMsgAttribToServer(attribData) ? TRUE : FALSE;
}

BOOL ClientEngine::WorldToScreen(Vector3f wldPosition, Vector3f* scrPosition) {
	Matrix44f matView, matProj;
	GetViewMatrix(&matView);
	GetProjectionMatrix(&matProj);

	D3DVIEWPORT9 viewport;
	g_pD3dDevice->GetViewport(&viewport);

	Vector3f viewPos = TransformPoint_NonPerspective(matView, wldPosition);
	Vector4f projPosH = TransformPointToHomogeneousVector(matProj, viewPos);
	if (projPosH.W() == 0) {
		scrPosition->Set(0, 0, 0);
		return FALSE;
	}
	Vector3f projPos = projPosH.SubVector<3>() / projPosH.W();
	Vector3f screenPos = {
		(1.0f + projPos.X()) * 0.5f * viewport.Width + viewport.X,
		(1.0f - projPos.Y()) * 0.5f * viewport.Height + viewport.Y,
		projPos.Z() * (viewport.MaxZ - viewport.MinZ) + viewport.MinZ
	};

	*scrPosition = screenPos;
	return TRUE;
}

//loot closest thing
void ClientEngine::SendInteractCommand() {
	TimeMs timeMs = fTime::TimeMs();
	if ((timeMs - m_controlTimeStamp) < 500)
		return;

	// Get My Avatar
	auto pMO = GetMovementObjMe();
	if (!pMO)
		return;

	// create a new TextEvent and fire it
	m_dispatcher.QueueEvent(new TextEvent("", SlashCommand::LOOT, pMO->getNetTraceId(), eChatType::Talk));
	m_controlTimeStamp = timeMs;
}

static bool checkScriptDataName(const char* _fname, const char* dir, std::string& fname, std::string& errMsg) {
	if (::strchr(_fname, '\\') != 0 || ::strchr(_fname, '/') != 0 || ::strchr(_fname, ':') > 0) {
		StrBuild(errMsg, _fname << " - Filename can not contain slashes or colons");
		return false;
	}

	if (!FileHelper::CreateFolderDeep(dir)) {
		StrBuild(errMsg, dir << " - Script directory not found, or insufficient rights");
		return false;
	}

	fname = PathAdd(dir, _fname);

	return true;
}

#include "Userenv.h"
#pragma comment(lib, "userenv.lib")
std::string ClientEngine::ConfigDir() {
	// Determine Config Dir '%APPDATA%\K...\<starId>'
	static std::string configDirStr = "";
	if (configDirStr == "") {
		wchar_t appDataDir[MAX_PATH] = { 0 };
		wchar_t configDir[MAX_PATH] = { 0 };
		GetEnvironmentVariableW(L"APPDATA", appDataDir, MAX_PATH);
		swprintf_s(configDir, L"%s\\" K_NAME "\\%s", appDataDir, Utf8ToUtf16(AppStarId()).c_str());
		configDirStr.assign(Utf16ToUtf8(configDir).c_str());
	}
	return configDirStr;
}

IKEPConfig* ClientEngine::OpenConfig(const char* _fname, const char* rootNodeName, std::string& errMsg) {
	// Clear Error Message
	errMsg.clear();

	// Get Config File Full Path
	std::string fname;
	if (!checkScriptDataName(_fname, ConfigDir().c_str(), fname, errMsg)) {
		LogError(errMsg);
		return 0;
	}

	// See if we already have filename in our map.  If so, return it,
	// otherwise allocate a new KEPConfig, add it to the map and return that instead.
	std::string key(_fname);
	STLToLower(key);
	ConfigMap::iterator itr = m_loadedConfigs.find(key);
	if (itr != m_loadedConfigs.end()) {
		return itr->second;
	}

	// Insert Config Node Into File
	KEPConfig* cfg = new KEPConfig();
	if (cfg->Load(fname.c_str(), rootNodeName))
	{
		m_loadedConfigs.insert(ConfigPair(key, cfg));
		return cfg;
	}

	// Delete KEPConfig
	delete cfg;

	// Delete Corrupted File
	if (FileHelper::Exists(fname) && !FileHelper::Delete(fname)) {
		StrAppend(errMsg, "\nFAILED Delete(" << fname << ")");
		LogError(errMsg);
		return 0;
	}

	// Try Creating Again
	errMsg.clear();
	cfg = new KEPConfig();
	LogInfo("Creating " << fname);
	KEPConfig cfgNew;
	cfgNew.New(rootNodeName);
	if (cfgNew.SaveAs(fname.c_str())) {
		//Load new config file
		if (cfg->Load(fname.c_str(), rootNodeName))
		{
			m_loadedConfigs.insert(ConfigPair(key, cfg));
			return cfg;
		}
	}

	// Delete KEPConfig
	delete cfg;

	StrAppend(errMsg, "\nFAILED Creating " << fname);
	LogError(errMsg);
	return nullptr;
}

IKEPFile* ClientEngine::OpenFile(const char* _fname, const char* access, std::string& errMsg, const char* dir) {
	// Clear Error Message
	errMsg.clear();

	// Valid Directory?
	if (strcmp(dir, CLIENTSCRIPTSDIR) != 0 &&
		strcmp(dir, MENUSDIR) != 0 &&
		strcmp(dir, MENUSCRIPTSDIR) != 0 &&
		strcmp(dir, SCRIPTDATADIR) != 0 &&
		strcmp(dir, SCRIPTDIR) != 0 &&
		strcmp(dir, CONFIGDIR) != 0) {
		StrBuild(errMsg, __FUNCTION__ ": Parameter 'dir' must be one of:"
							 << " " << CLIENTSCRIPTSDIR
							 << " " << MENUSDIR
							 << " " << MENUSCRIPTSDIR
							 << " " << SCRIPTDATADIR
							 << " " << SCRIPTDIR
							 << " " << CONFIGDIR);
		//		LogError(errMsg);
		return 0;
	}

	// DRF - Select File Directory
	std::string fileDir = "";
	if (strcmp(dir, CONFIGDIR) == 0)
		fileDir = ConfigDir();
	else
		fileDir = PathAdd(PathGameFiles(), dir);

	// Get File Full Path
	std::string errMsgTmp;
	std::string fname;
	if (!checkScriptDataName(_fname, fileDir.c_str(), fname, errMsgTmp)) {
		StrBuild(errMsg, "checkScriptDataName() FAILED - " << errMsgTmp);
		//		LogError(errMsg);
		return 0;
	}

	// Attempt Open File
	KEPFile* f = new KEPFile();
	if (!f->Open(fname.c_str(), access, errMsgTmp)) {
		f->CloseAndDelete();
		StrBuild(errMsg, "Open() FAILED - " << errMsgTmp);
		//		LogError( errMsg );
		return 0;
	}

	return f;
}

bool ClientEngine::DeleteAsset(int type, std::string const& baseName) {
	if (!(type == ScriptServerEvent::MenuXml || type == ScriptServerEvent::MenuDDS || type == ScriptServerEvent::MenuTGA ||
			type == ScriptServerEvent::MenuScript || type == ScriptServerEvent::MenuBMP || type == ScriptServerEvent::MenuPNG ||
			type == ScriptServerEvent::MenuJPG || type == ScriptServerEvent::MenuJPEG)) {
		// can only delete things that are in the patch
		LogError("DeleteAsset unexpected type for: " << baseName);
		return false;
	}

	std::string appFolder, localDevFolder, extension;
	localDevFolder = getDevToolAssetPath(type);

	switch (type) {
		case ScriptServerEvent::MenuXml:
		case ScriptServerEvent::MenuDDS:
		case ScriptServerEvent::MenuTGA:
		case ScriptServerEvent::MenuBMP:
		case ScriptServerEvent::MenuPNG:
		case ScriptServerEvent::MenuJPG:
		case ScriptServerEvent::MenuJPEG:
			appFolder = GetAppMenuDir();
			break;
		case ScriptServerEvent::MenuScript:
			appFolder = GetAppScriptDir();
			break;
		default:
			return false;
	}

	switch (type) {
		case ScriptServerEvent::MenuXml:
			extension = ".xml";
			break;
		case ScriptServerEvent::MenuDDS:
			extension = ".dds";
			break;
		case ScriptServerEvent::MenuTGA:
			extension = ".tga";
			break;
		case ScriptServerEvent::MenuBMP:
			extension = ".bmp";
			break;
		case ScriptServerEvent::MenuPNG:
			extension = ".png";
			break;
		case ScriptServerEvent::MenuJPG:
			extension = ".jpg";
			break;
		case ScriptServerEvent::MenuJPEG:
			extension = ".jpeg";
			break;
		case ScriptServerEvent::MenuScript:
			extension = ".lua";
			break;
		default:
			return false;
	}

	std::string fileName = baseName + extension;
	std::string errMsg;

	if (fileName.find('\\') != std::string::npos || fileName.find('/') != std::string::npos || fileName.find(':') != std::string::npos) {
		errMsg = loadStr(IDS_BAD_SCRIPTDATANAME);
		LOG4CPLUS_WARN(m_logger, errMsg << fileName);
	} else {
		// Delete from LocalDev first
		std::string fullPath = PathAdd(localDevFolder, fileName);
		remove(fullPath.c_str()); // Don't report a failure for local dev.

		// Delete from App folder
		fullPath = PathAdd(appFolder, fileName);
		if (remove(fullPath.c_str()) != 0) {
			errorNumToString(HRESULT_FROM_WIN32(errno), errMsg);
			LOG4CPLUS_WARN(m_logger, errMsg << fileName);
		}
	}

	ScriptServerEvent* se = new ScriptServerEvent(ScriptServerEvent::DeleteAsset);
	(*se->OutBuffer()) << type;
	(*se->OutBuffer()) << baseName;
	se->AddTo(SERVER_NETID);
	m_dispatcher.QueueEvent(se);
	return true;
}

// SetControlConfig exposed to client to set what entry to use
// within the controls file.  This allows changing key mappings
// through client scripting.
void ClientEngine::SetControlConfig(int configNumber) {
	auto pMO = GetMovementObjMe();
	if (pMO)
		pMO->setControlConfig(configNumber);
}

void ClientEngine::OnZoneLoadingUIActiveChange(bool active) {
	if (active) {
		OnRezoneCleanup();

		SYSTEM_INFO sysInfo;
		GetSystemInfo(&sysInfo);
		if (sysInfo.dwNumberOfProcessors >= 4)
			m_fRezoneRenderIntervalSeconds = .25f;
		else
			m_fRezoneRenderIntervalSeconds = 1.0f;
		TextureRM::Instance()->SuspendLoad(true);
	} else {
		RezoneStop();

		// ED-7733 - Load all DO textures before you look at them
		m_dynamicPlacementManager.UpdateCustomMaterials(true); // chainLoad
		TextureRM::Instance()->SuspendLoad(false);
	}
}

void ClientEngine::OnRezoneCleanup() {
	StopAllMedia(true);
	SoundPlacementsClear();
	m_particleRM->ClearRuntimeEffects();
	MOS_Reset();
	ObjectClearSelection();
	ResetCursorModesTypes();
	m_speedWas.clear();
}

void ClientEngine::OnRezonePre() {
	RezoneStart();

	// Close All Menus Except Progress Pre-Rezone
	MenuCloseAllExcept("ProgressMenu.xml");

	// Send Custom Runtime Report (Previous Zone Statistics)
	RuntimeReporter::SendReport(RuntimeReporter::REPORT_TYPE_CUSTOM);

	// Cleanup Everything
	OnRezoneCleanup();
}

void ClientEngine::OnRezonePost() {
	// ED-8437 - Clear Errored Resources On Rezone
	ClearErroredResources();
}

// ED-8437 - Clear Errored Resources On Rezone
void ClientEngine::ClearErroredResources() {
	if (m_animationRM)
		m_animationRM->ClearErroredResources();
	if (m_soundRM)
		m_soundRM->ClearErroredResources();
	if (m_dynamicObjectRM)
		m_dynamicObjectRM->ClearErroredResources();
	if (m_equippableRM)
		m_equippableRM->ClearErroredResources();
	if (m_particleRM)
		m_particleRM->ClearErroredResources();
	if (m_textureRM)
		m_textureRM->ClearErroredResources();
	if (m_meshRM)
		m_meshRM->ClearErroredResources();
	if (m_skeletonRM)
		m_skeletonRM->ClearErroredResources();
	if (m_pTextureDB)
		m_pTextureDB->TextureMapClearErrored();
}

void ClientEngine::ProcessEffectTrack(EffectTrack& effectTrack, void* const entity, IEvent* const e, int objectId, int numberOfEffects, int numberOfStrings) {
	if (numberOfEffects > EFX_MAX_EFFECTS) {
		// Corrupted data?
		jsAssert(false);
		LogError("Too many effects: " << numberOfEffects);
		return;
	}

	ScriptClientEvent* sce = dynamic_cast<ScriptClientEvent*>(e);
	std::vector<std::string> allStrings;
	for (int i = 0; i < numberOfStrings; ++i) {
		std::string str;
		*sce->InBuffer() >> str;
		allStrings.push_back(str);
	}

	std::vector<std::vector<int>> allEffects;
	allEffects.resize(numberOfEffects);

	for (int i = 0; i < numberOfEffects; ++i) {
		int numberOfArgs;
		*sce->InBuffer() >> numberOfArgs;
		jsAssert(numberOfArgs >= EFX_NUM_REQUIRED_ARGS);

		if (numberOfArgs > EFX_MAX_ARGS) {
			// Corrupted data?
			jsAssert(false);
			LogError("Too many arguments for effect # " << (i + 1) << ": " << numberOfArgs);
			return;
		}

		allEffects[i].resize(numberOfArgs);
		for (int j = 0; j < numberOfArgs; ++j) {
			*sce->InBuffer() >> allEffects[i][j];
		}
	}

	effectTrack.clearTrack();
	for (int i = 0; i < numberOfEffects; ++i) {
		std::vector<int>& effect = allEffects[i];
		int numberOfArgs = effect.size();
		if (numberOfArgs < EFX_NUM_REQUIRED_ARGS) {
			LogWarn("Too few arguments for effect # " << (i + 1));
			continue;
		}

		EffectCode effectType = static_cast<EffectCode>(effect[EFX_CODE]);
		int intStartTime = effect[EFX_STARTTIME];
		jsAssert(intStartTime >= 0);
		DWORD startTime = (DWORD)intStartTime;

		switch (effectType) {
			case EffectCode::EFX_ANIMATION: // Animation
				if (numberOfArgs >= EFX_ANM_v0_NUM_ARGS) {
					DWORD stopTime = 0;
					GLID glid;
					if (numberOfArgs == EFX_ANM_v0_NUM_ARGS) {
						glid = effect[EFX_ANM_v0_GLID];
					} else {
						stopTime = effect[EFX_STOPTIME];
						glid = effect[EFX_ANM_GLID];
					}

					if (effectTrack.type() == ObjectType::MOVEMENT) {
						effectTrack.addEffect(IEffectEventPtr(new AnimationEffect(startTime, reinterpret_cast<CMovementObj*>(entity), glid)));
						if (stopTime > startTime) {
							effectTrack.addEffect(IEffectEventPtr(new AnimationEffect(stopTime, reinterpret_cast<CMovementObj*>(entity), 0)));
						}
					} else {
						effectTrack.addEffect(IEffectEventPtr(new AnimationEffect(startTime, reinterpret_cast<CDynamicPlacementObj*>(entity), glid)));
						if (stopTime > startTime) {
							effectTrack.addEffect(IEffectEventPtr(new AnimationEffect(stopTime, reinterpret_cast<CDynamicPlacementObj*>(entity), 0)));
						}
					}
				}
				break;
			case EffectCode::EFX_SOUND: // Sound
				if (numberOfArgs >= EFX_SND_v0_NUM_ARGS) {
					DWORD stopTime = 0;
					GLID glid;
					if (numberOfArgs == EFX_SND_v0_NUM_ARGS) {
						glid = effect[EFX_SND_v0_GLID];
					} else {
						stopTime = effect[EFX_STOPTIME];
						glid = effect[EFX_SND_GLID];
					}

					effectTrack.addEffect(IEffectEventPtr(new SoundEffect(startTime, this, objectId, glid, false)));
					if (stopTime > startTime) {
						effectTrack.addEffect(IEffectEventPtr(new SoundEffect(stopTime, this, objectId, glid, true)));
					}
				}
				break;
			case EffectCode::EFX_PARTICLE: // Start and Stop Particle Effect
				if (numberOfArgs >= EFX_PTC_NUM_ARGS_BASE) {
					DWORD stopTime = effect[EFX_STOPTIME];
					int boneNameIdx = effect[EFX_PTC_BONE];
					int slot = effect[EFX_PTC_SLOT];
					GLID glid = effect[EFX_PTC_GLID];
					Vector3f ofv(0.0f, 0.0f, 0.0f), fwd(0.0f, 0.0f, 1.0f), upd(0.0f, 1.0f, 0.0f);

					// Optional offset vector
					if (numberOfArgs >= EFX_PTC_NUM_ARGS_WOFV) {
						ofv.x = effect[EFX_PTC_OFV_X] / 1000.0f;
						ofv.y = effect[EFX_PTC_OFV_Y] / 1000.0f;
						ofv.z = effect[EFX_PTC_OFV_Z] / 1000.0f;
					}

					// Optional forward direction
					if (numberOfArgs >= EFX_PTC_NUM_ARGS_WFWD) {
						fwd.x = effect[EFX_PTC_FWD_X] / 1000.0f;
						fwd.y = effect[EFX_PTC_FWD_Y] / 1000.0f;
						fwd.z = effect[EFX_PTC_FWD_Z] / 1000.0f;
					}

					// Optional up direction
					if (numberOfArgs >= EFX_PTC_NUM_ARGS) {
						upd.x = effect[EFX_PTC_UPD_X] / 1000.0f;
						upd.y = effect[EFX_PTC_UPD_Y] / 1000.0f;
						upd.z = effect[EFX_PTC_UPD_Z] / 1000.0f;
					}

					effectTrack.addEffect(IEffectEventPtr(new StartParticleEffect(startTime, this, objectId, allStrings[boneNameIdx], slot, glid, ofv, fwd, upd)));
					if (stopTime > startTime) {
						effectTrack.addEffect(IEffectEventPtr(new StopParticleEffect(stopTime, this, objectId, allStrings[boneNameIdx], slot)));
					}
				}
				break;
			case EffectCode::EFX_SOUND_EXISTING: // a sound that's already placed
				if (numberOfArgs >= EFX_SNX_v0_NUM_ARGS) {
					DWORD stopTime = 0;
					int soundPID;
					if (numberOfArgs == EFX_SNX_v0_NUM_ARGS) {
						soundPID = effect[EFX_SNX_v0_PID];
					} else {
						stopTime = effect[EFX_STOPTIME];
						soundPID = effect[EFX_SNX_PID];
					}

					effectTrack.addEffect(IEffectEventPtr(new SoundEffectObject(startTime, this, soundPID, false)));
					if (stopTime > startTime) {
						effectTrack.addEffect(IEffectEventPtr(new SoundEffectObject(stopTime, this, soundPID, true)));
					}
				}
				break;
			case EffectCode::EFX_OBJECT_MOVE: // move - all number parameters are of double precision floating point type
			case EffectCode::EFX_OBJECT_ROTATE: // rotate - all number parameters are of double precision floating point type
				if (numberOfArgs >= EFX_MOVROT_NUM_ARGS) {
					DWORD time = effect[EFX_MOVROT_DURATION];
					double x, y, z;
					// Scale x/y/z back (see objectSetEffectTrack() in Script_API.cpp)
					x = effect[EFX_MOVROT_X] / 1000.0f;
					y = effect[EFX_MOVROT_Y] / 1000.0f;
					z = effect[EFX_MOVROT_Z] / 1000.0f;

					IEffectEvent* efx;
					if (effectType == EffectCode::EFX_OBJECT_MOVE)
						efx = new MoveEffect(startTime, GetDispatcher(), objectId, time, x, y, z);
					else
						efx = new RotateEffect(startTime, GetDispatcher(), objectId, time, x, y, z);

					effectTrack.addEffect(IEffectEventPtr(efx));
				}
				break;
			default:
				break;
		}
	}

	effectTrack.setStartTime(fTime::TimeMs());
}

void ClientEngine::Callback() {
	FrameTimeNestedContext ftnc(GetFrameTimeProfiler(), "Callback");
	ClientBaseEngine::Callback();
}

void ClientEngine::LogFrameTimeProfilerReport() {
	assert(m_frameTimeProfiler != nullptr);
	if (m_frameTimeProfiler != nullptr) {
		const auto& reports = m_frameTimeProfiler->reports();
		assert(!reports.empty());
		const auto& contexts = m_frameTimeProfiler->contexts();

		// Log latest report
		auto currReport = reports.back();
		_LogInfo("Profiler: Frame count: " << currReport.getFrameCount());
		_LogInfo("Profiler: Average Frame Duration (us): " << (currReport.getTotalDuration() * 1000 / currReport.getFrameCount()));

		size_t index = 0;
		for (TimeMs duration : currReport.getAccumulatedDurationsByContext()) {
			std::string context = index < contexts.size() ? contexts[index] : std::to_string(index);
			_LogInfo("Profiler: [" << context << "]: " << std::fixed << std::setw(5) << std::setprecision(2) << (duration / currReport.getTotalDuration() * 100) << " %");
			index++;
		}

		// Print an compare matrix to a text file
		const int W1 = 50, W2 = 10;
		std::fstream fout("FrameTime.log", std::ios::app);
		fout << "+-" << std::string(W1, '-') << "-+";
		for (size_t i = 0; i < reports.size() * 2; i++) {
			fout << "-" << std::string(W2, '-') << "-+";
		}

		fout << std::endl
			 << "| " << std::setw(W1) << std::left << "Begin Date / Time "
			 << " |" << std::right;
		for (const auto& report : reports) {
			struct tm tm;
			localtime_s(&tm, &report.getTimestamp());
			fout << " " << std::setfill('0') << std::setw(2) << tm.tm_mday << "/" << std::setw(2) << tm.tm_mon << "/" << std::setw(4) << (1900 + tm.tm_year) << std::setfill(' ') << " |";
			fout << "   " << std::setfill('0') << std::setw(2) << tm.tm_hour << ":" << std::setw(2) << tm.tm_min << ":" << std::setw(2) << tm.tm_sec << std::setfill(' ') << " |";
		}

		fout << std::endl
			 << "| " << std::setw(W1) << std::left << "Frame Count "
			 << " |" << std::right;
		for (const auto& report : reports) {
			fout << " " << std::setw(W2) << report.getFrameCount() << " |";
			fout << " " << std::string(W2, ' ') << " |";
		}

		fout << std::endl
			 << "| " << std::setw(W1) << std::left << "Total Duration (us)"
			 << " |" << std::right;
		for (const auto& report : reports) {
			fout << " " << std::setw(W2) << std::fixed << std::setprecision(0) << report.getTotalDuration() * 1000 << " |";
			fout << " " << std::string(W2, ' ') << " |";
		}

		fout << std::endl
			 << "| " << std::setw(W1) << std::left << "Frames Per Second "
			 << " |" << std::right;
		for (const auto& report : reports) {
			fout << " " << std::setw(W2) << std::fixed << std::setprecision(1) << (report.getFrameCount() / report.getTotalDuration() * 1000) << " |";
			fout << " " << std::string(W2, ' ') << " |";
		}

		fout << std::endl
			 << "| " << std::setw(W1) << std::left << "Average Frame Duration (us)"
			 << " |" << std::right;
		for (const auto& report : reports) {
			fout << " " << std::setw(W2) << std::fixed << std::setprecision(0) << (report.getTotalDuration() * 1000 / report.getFrameCount()) << " |";
			fout << " " << std::string(W2, ' ') << " |";
		}

		index = 0;
		for (std::string context : contexts) {
			fout << std::endl
				 << "| ";
			size_t width = W1;
			size_t ofs;
			while ((ofs = context.find('/')) != std::string::npos) {
				context = context.substr(ofs + 1);
				fout << ".   ";
				width -= 4;
			}

			fout << std::setw(width) << std::left << context << " |" << std::right;
			for (const auto& report : reports) {
				const auto& data = report.getAccumulatedDurationsByContext();
				if (data.size() > index) {
					fout << " " << std::setw(W2) << std::fixed << std::setprecision(0) << (data[index] * 1000 / report.getFrameCount()) << " |";
					fout << " " << std::setw(W2 - 2) << std::fixed << std::setprecision(2) << (data[index] / report.getTotalDuration() * 100) << " % |";
				}
			}

			index++;
		}

		fout << std::endl
			 << "+-" << std::string(W1, '-') << "-+";
		for (size_t i = 0; i < reports.size() * 2; i++) {
			fout << "-" << std::string(W2, '-') << "-+";
		}

		fout << std::endl;

		// Rollover report data
		m_frameTimeProfiler->rollover();
	}
}

// #MLB_PaperDoll - Copy World Stats
using SrvrSpwndNPCsMap = std::map<int, CMovementObj*>;
using paperDollResPair = std::pair<const CMovementObj*, const CDynamicPlacementObj*>;
using paperDollPIDPair = std::pair<int, CMovementObj*>;
using MemoryUsedSourcesVec = std::vector<paperDollResPair>;

inline paperDollPIDPair findPlacementId(const SrvrSpwndNPCsMap& npcs, CMovementObj* pMO) {
	for (auto it = npcs.begin(); it != npcs.end(); ++it) {
		if (pMO == it->second) {
			return *it;
		}
	}
	return paperDollPIDPair(0, pMO);
}

size_t ClientEngine::GetPaperDollMemoryUsage(std::ostream& strm) const {
	size_t memoryUsageInBytes = 0;

	MemoryUsedSourcesVec memoryUsedSourcesVec;

	for (POSITION posEnt = m_movObjList->GetHeadPosition(); posEnt != NULL;) {
		auto pMO = static_cast<CMovementObj*>(m_movObjList->GetNext(posEnt));
		if (pMO) {
			paperDollPIDPair isSpawned = findPlacementId(m_npcs, pMO);
			if (isSpawned.first) {
				// Paper Doll entity
				const CDynamicPlacementObj* pDO = GetDynamicObject(isSpawned.first);
				paperDollResPair resUsedSource(pMO, pDO);
				memoryUsedSourcesVec.push_back(resUsedSource);
			} else {
				// Avatar entity
				memoryUsedSourcesVec.push_back(paperDollResPair(pMO, nullptr));
			}
		}
	}

	{
		IMemSizeGadget* pMemSizeGadget = CreateMemSizeGadget();

		for (auto resPair : memoryUsedSourcesVec) {
			size_t currentMemUsed = pMemSizeGadget->GetTotalSizeInBytes();
			resPair.first->GetMemSize(pMemSizeGadget);
			if (resPair.second) {
				resPair.second->GetMemSize(pMemSizeGadget);

				size_t memoryUsedByThisPaperDoll = pMemSizeGadget->GetTotalSizeInBytes() - currentMemUsed;
				strm << boost::format("[%6d] PaperDoll object is using total memory in bytes of      [%12d]\n") % resPair.second->GetGlobalId() % memoryUsedByThisPaperDoll;
			} else {
				size_t memoryUsedByThisAvatar = pMemSizeGadget->GetTotalSizeInBytes() - currentMemUsed;
				strm << boost::format("[Player] Avatar object is using total memory in bytes of         [%12d]\n") % memoryUsedByThisAvatar;
			}
		}

		memoryUsageInBytes = pMemSizeGadget->GetTotalSizeInBytes();
		strm << boost::format("Total memory usage for Paper Doll and Avatar objects in bytes    [%12d]\n") % memoryUsageInBytes;

		pMemSizeGadget->Release();
	}

	return memoryUsageInBytes;
}

static double s_dRezoneStartSec = GetCurrentTimeInSeconds();
void ClientEngine::RezoneStart() {
	s_dRezoneStartSec = GetCurrentTimeInSeconds();
	LogInfo("REZONE START");
}

void ClientEngine::RezoneStop() {
	double dRezoneStopSec = GetCurrentTimeInSeconds();
	LogInfo("REZONE END - timeSec=" << dRezoneStopSec - s_dRezoneStartSec);
}

BEGIN_GETSET_IMPL(ClientEngine, IDS_ENGINE_NAME)
GETSET(m_instanceName, gsString, GS_FLAGS_DEFAULT | GS_FLAG_INSTANCE_NAME, 0, 0, ENGINE, NAME)
GETSET_CUSTOM(0, gsString, GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE, 0, 0, ENGINE, BASEDIR, 0, 0)
GETSET(m_autoexec, gsString, GS_FLAGS_DEFAULT, 0, 0, ENGINE, AUTOEXEC)
GETSET_CUSTOM(&m_type, gsListOfValues, GS_FLAGS_DEFAULT | GS_FLAG_ADDONLY, 0, 0, ENGINE, TYPE, 0, 3);
GETSET(m_enabled, gsBool, GS_FLAGS_DEFAULT, 0, 0, ENGINEINFO, ENABLED)
GETSET_CUSTOM(0, gsBool, GS_FLAG_EXPOSE, 0, 0, ENGINE, RUNNING, 0, 0)
GETSET_ACTION(this, GS_FLAGS_ACTION, 0, 0, ADMIN, DELETE)
GETSET(m_paused, gsBool, GS_FLAGS_DEFAULT, 0, 0, ENGINE, PAUSED)
GETSET(m_showAllNames, gsBool, GS_FLAGS_DEFAULT, 0, 0, ENGINE, SHOWNAMES)
GETSET(m_pitchLock, gsBool, GS_FLAGS_DEFAULT, 0, 0, ENGINE, LOCKPITCH)
GETSET(m_zoneFileName, gsString, GS_FLAGS_ACTION, 0, 0, ENGINE, CURRENTZONENAME)
GETSET(m_zoneIndex, gsInt, GS_FLAGS_ACTION, 0, 0, ENGINE, CURRENTZONEINDEX)
GETSET2(m_loginName, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTENGINE_LOGINNAME, IDS_CLIENTENGINE_LOGINNAME)
GETSET2(m_loginGender, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTENGINE_LOGINGENDER, IDS_CLIENTENGINE_LOGINGENDER)
GETSET2(m_zonePermissions, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTENGINE_ZONEPERMISSIONS, IDS_CLIENTENGINE_ZONEPERMISSIONS)
GETSET2(m_zoneInstanceId, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTENGINE_CURRENTZONEINSTANCEID, IDS_CLIENTENGINE_CURRENTZONEINSTANCEID)
GETSET2(m_tryOnGlid, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTENGINE_TRYONGLID, IDS_CLIENTENGINE_TRYONGLID)
GETSET2(m_inTryOnState, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTENGINE_TRYONSTATE, IDS_CLIENTENGINE_TRYONSTATE)
GETSET2(m_tryonUseType, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTENGINE_TRYONUSETYPE, IDS_CLIENTENGINE_TRYONUSETYPE)
GETSET2(m_tryOnObjPlacementId, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTENGINE_TRYONOBJPLACEMENTID, IDS_CLIENTENGINE_TRYONOBJPLACEMENTID)
GETSET2(m_tryOnOnStartup, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTENGINE_TRYONONSTARTUP, IDS_CLIENTENGINE_TRYONONSTARTUP)
GETSET2(m_gameId, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_ENGINE_GAMEID, IDS_ENGINE_GAMEID)
GETSET2(m_parentGameId, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_PARENT_ID, IDS_PARENT_ID)
GETSET2(m_gameName, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_GAME_NAME, IDS_GAME_NAME)
GETSET2(m_urlParameters, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_URL_PARAMETERS, IDS_URL_PARAMETERS)
GETSET2(m_abGroupList, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTENGINE_HUD_AB_GROUP, IDS_CLIENTENGINE_HUD_AB_GROUP)
END_GETSET_IMPL

} // namespace KEP