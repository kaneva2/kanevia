///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ClientEngine.h"
#include "RuntimeSkeleton.h"
#include "SkeletonConfiguration.h"

namespace KEP {

static LogInstance("ClientEngine");

std::vector<GLID> ClientEngine::GetPlayerArmedItems(CMovementObj* pMO) {
	std::vector<GLID> armedGlids;

	auto pMO_me = GetMovementObjMe();
	if (!pMO)
		pMO = pMO_me;
	if (!pMO)
		return armedGlids;

	// Get Armed Items
	auto armedItems = pMO->getArmedItems();
	for (const auto& pIO : armedItems) {
		if (!pIO)
			continue;
		armedGlids.push_back(pIO->m_globalID);
	}
	return armedGlids;
}

///////////////////////////////////////////////////////////////////////////////
// DRF - Inventory Arm Functions
///////////////////////////////////////////////////////////////////////////////

bool ClientEngine::ArmInventoryItem_MO(const GLID& glid, CMovementObj* pMO) {
	if (!IS_VALID_GLID(glid))
		return false;

	auto pMO_me = GetMovementObjMe();
	if (!pMO)
		pMO = pMO_me;
	if (!pMO)
		return false;

	// Arm Locally ?
	bool isMe = (pMO == pMO_me);
	if (!ClientIsEnabled() || !isMe) {
		// Arm Entity Item Locally (realize now, arm defaults)
		if (!ArmItem(glid, pMO)) {
			LogError(pMO->ToStr() << " ArmItem() FAILED - glid<" << glid << ">");
			return false;
		}
		return true;
	}

	// Arm Globally Via Equip Message (glidSpecial: <0=disarm, >0=arm)
	// Message first goes to server for validation and broadcasting back out to everyone.
	// We will actually do it once we receive the response in HandleMsgEquipToClient().
	std::vector<GLID> glidsSpecial({ glid });
	return QueueMsgEquipToServer(glidsSpecial, pMO->getNetTraceId());
}

///////////////////////////////////////////////////////////////////////////////
// DRF - Inventory Disarm Functions
///////////////////////////////////////////////////////////////////////////////

bool ClientEngine::DisarmInventoryItem_MO(const GLID& glid, CMovementObj* pMO) {
	if (!IS_VALID_GLID(glid))
		return false;

	auto pMO_me = GetMovementObjMe();
	if (!pMO)
		pMO = pMO_me;
	if (!pMO)
		return false;

	// Disarm Locally ?
	bool isMe = (pMO == pMO_me);
	if (!ClientIsEnabled() || !isMe) {
		// Disarm Entity Item Locally (realize now, arm defaults)
		if (!DisarmItem(glid, pMO)) {
			LogError(pMO->ToStr() << " DisarmItem() FAILED - glid<" << glid << ">");
			return false;
		}
		return true;
	}

	// Disarm Globally Via Equip Message (glidSpecial: <0=disarm, >0=arm)
	// Message first goes to server for validation and broadcasting back out to everyone.
	// We will actually do it once we receive the response in HandleMsgEquipToClient().
	GLID glidSpecial = -glid;
	if (glidSpecial == 0)
		glidSpecial = -9999;
	std::vector<GLID> glidsSpecial;
	glidsSpecial.push_back(glidSpecial);
	return QueueMsgEquipToServer(glidsSpecial, pMO->getNetTraceId());
}

///////////////////////////////////////////////////////////////////////////////
// DRF - Equippable Arm Functions
///////////////////////////////////////////////////////////////////////////////

bool ClientEngine::ArmEquippableItem_MO(const GLID& glid, CMovementObj* pMO, int material) {
	if (!IS_VALID_GLID(glid))
		return false;

	if (!pMO)
		pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	auto pRS = pMO->getSkeleton();
	if (!pRS)
		return false;

	// Get Movement Object Reference Model
	auto pMORM = GetMovementObjRefModel(pMO);
	if (!pMORM) {
		LogError(pMO->ToStr() << " GetMovementObjRefModel() FAILED - glid<" << glid << ">");
		return false;
	}

	// Obtain Item Exclusion Group (list of conflicting items)
	std::set<int> exclusionGroupIDs;
	auto pGIO = CGlobalInventoryObjList::GetInstance()->GetByGLID(glid);
	if (pGIO && pGIO->m_itemData)
		exclusionGroupIDs.insert(pGIO->m_itemData->m_exclusionGroupIDs.begin(), pGIO->m_itemData->m_exclusionGroupIDs.end());

	// Arm Attachment
	if (pRS->ArmEquippableItem(glid, exclusionGroupIDs, pMO, pMO->getSkeletonConfig()) == RuntimeSkeleton::eEquipArmState::Failed) {
		LogError(pMO->ToStr(pMORM) << " ArmEquippableItem() FAILED - glid<" << glid << ">");
		return false;
	}

	return true;
}

// BLOCKING - Better to call ObjectArmEquippableAsync() which caches metadata asynchronously first!
bool ClientEngine::ArmEquippableItem_DPO(const GLID& glid, CDynamicPlacementObj* pDPO, const boost::optional<std::string>& strBoneOverride) {
	if (!IS_VALID_GLID(glid))
		return false;

	if (!pDPO)
		return false;

	auto pRS = pDPO->getSkeleton();
	if (!pRS)
		return false;

	// Should Already Be Cached If Calling ObjectArmEquippableItemAsync()
	ContentService::Instance()->ContentMetadataCacheSync(glid, false);

	// Obtain Item Exclusion Group (list of conflicting items)
	// Disable exclusion groups for dynamic object accessories.
	std::set<int> exclusionGroupIDs;

	// Make sure we have a skeleton config.
	if (!pDPO->GetSkeletonConfiguration()) {
		std::unique_ptr<SkeletonConfiguration> pSkeletonConfiguration = std::make_unique<SkeletonConfiguration>();
		pDPO->SetSkeletonConfiguration(std::move(pSkeletonConfiguration));
	}

	// Arm Attachment
	if (pRS->ArmEquippableItem(glid, exclusionGroupIDs, nullptr, pDPO->GetSkeletonConfiguration(), nullptr, strBoneOverride) == RuntimeSkeleton::eEquipArmState::Failed) {
		LogError(pDPO->ToStr(false) << " ArmEquippableItem() FAILED - glid<" << glid << ">");
		return false;
	}

	return true;
}

// Called From Script Glue
bool ClientEngine::ArmEquippableItem_GS_MO(const GLID& glid, IGetSet* pGS_MO, int material) {
	if (!IS_VALID_GLID(glid))
		return false;

	auto pMO = dynamic_cast<CMovementObj*>(pGS_MO);
	if (!pMO)
		pMO = GetMovementObjMe();
	if (!pMO)
		return false;

	// Arm Equippable Item & Any Posses
	bool ok = ArmEquippableItem_MO(glid, pMO, material);
	if (pMO->getPosses() && pMO->getMountType() == MT_STUNT)
		ok &= ArmEquippableItem_MO(glid, pMO->getPosses(), material);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////
// DRF - Attachment Disarm Functions
///////////////////////////////////////////////////////////////////////////////

bool ClientEngine::DisarmEquippableItem_MO(const GLID& glid, CMovementObj* pMO) {
	if (!IS_VALID_GLID(glid))
		return false;

	pMO = (pMO ? pMO : GetMovementObjMe());
	if (!pMO)
		return false;

	auto pRS = pMO->getSkeleton();
	if (!pRS)
		return false;

	if (!pRS->DisarmEquippableItem(glid)) {
		LogError(pMO->ToStr() << " DisarmEquippableItem() FAILED - glid<" << glid << ">");
		return false;
	}

	return true;
}

bool ClientEngine::DisarmEquippableItem_DPO(const GLID& glid, CDynamicPlacementObj* pDPO) {
	if (!IS_VALID_GLID(glid))
		return false;

	if (!pDPO)
		return false;

	auto pRS = pDPO->getSkeleton();
	if (!pRS)
		return false;

	if (!pRS->DisarmEquippableItem(glid)) {
		LogError(pDPO->ToStr() << " DisarmEquippableItem() FAILED - glid<" << glid << ">");
		return false;
	}

	return true;
}

// Called From Script Glue
bool ClientEngine::DisarmEquippableItem_GS_MO(const GLID& glid, IGetSet* pGS_MO) {
	if (!IS_VALID_GLID(glid))
		return false;

	auto pMO = dynamic_cast<CMovementObj*>(pGS_MO);
	pMO = (pMO ? pMO : GetMovementObjMe());
	if (!pMO)
		return false;

	// Disarm Equippable Item & Any Posses
	bool ok = DisarmEquippableItem_MO(glid, pMO);
	if (pMO->getPosses() && pMO->getMountType() == MT_STUNT)
		ok &= DisarmEquippableItem_MO(glid, pMO->getPosses());
	return ok;
}

} // namespace KEP