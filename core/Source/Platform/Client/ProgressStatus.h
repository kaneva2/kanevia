/******************************************************************************
 ProgressStatus.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <afx.h>

// For the Editor, allow call back into the view to update progress during lengthy load operations.

class ProgressUpdater {
public:
	inline ProgressUpdater(){};

	// For use by ClientEngine without pulling in the Editor files
	inline virtual void ProgressUpdate(CStringA position, BOOL init = FALSE, BOOL deinit = FALSE) {
		return;
	}
	inline virtual void ProgressUpdate(const CStringA& position, const CStringA& message, int weight, BOOL init = FALSE, BOOL deinit = FALSE) {
		return;
	}
	inline virtual void SetInitialized(bool val) {
		return;
	}

#ifdef VIEW_BASE_CLASS
	// client or editor
	static ProgressUpdater* m_thisProgressUpdate;
	inline static void UpdateProgress(CStringA position, BOOL init = FALSE, BOOL deinit = FALSE) {
		if (m_thisProgressUpdate)
			m_thisProgressUpdate->ProgressUpdate(position, init, deinit);
	}
	inline static void UpdateInitialized(bool val) {
		if (m_thisProgressUpdate)
			m_thisProgressUpdate->SetInitialized(val);
	}
#else // server
	inline static void UpdateProgress(CStringA position, BOOL init = FALSE, BOOL deinit = FALSE) {}
	inline static void UpdateInitialized(bool val) {}
#endif
};
