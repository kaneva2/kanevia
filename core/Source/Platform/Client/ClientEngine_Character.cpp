///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientEngine.h"
#include "CMultiplayerClass.h"
#include "ContentServiceGLIDCacheTask.h"
#include "RuntimeSkeleton.h"
#include "SkeletonConfiguration.h"
#include "CIconSystemClass.h"
#include "CElementsClass.h"
#include <PassList.h>
#include "CDeformableMesh.h"
#include "common/include/ugcconstants.h"

namespace KEP {

static LogInstance("ClientEngine");

bool ClientEngine::DeleteCharacter(int index) {
	if (!ClientIsEnabled())
		return false;

	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::DeletePlayer;
	attribData.aeInt1 = index;
	return m_multiplayerObj->QueueMsgAttribToServer(attribData);
}

// DRF - This is used via KEP_CreateCharacter()...
// - LoginMenu.lua - Spawn Avatar
// - CharacterCreation2.lua - Avatar Customization
bool ClientEngine::CreateCharacterOnServer(CMovementObj* pMO, int spwnCfg, const std::string& name, int nTeam, float scaleX, float scaleY, float scaleZ) {
	if (!pMO || !ClientIsEnabled())
		return false;

	if (name.empty() || name.size() > 25)
		return false;

	int DBindex = pMO->getIdxRefModel();

	// Accumulate All Currently Armed Items
	int attachmentCount = 0;
	auto pCAI = pMO->getCurrentlyArmedItems();
	if (pCAI) {
		OBJ_LIST_GUARD(pCAI);
		for (POSITION pos = pCAI->GetHeadPosition(); pos;) {
			auto pIO = static_cast<const CItemObj*>(pCAI->GetNext(pos));
			if (!pIO)
				continue;

			if (pIO->m_equiped)
				attachmentCount++;
		}
	}

	// Number of DIM config entries
	// some items (like head) might still be in loading progress
	size_t defCfgCount = pMO->getTotalConfigSlotCount();

	// Calculate message size in bytes
	int msgSize = sizeof(MSG_CREATE_CHAR_TO_SERVER); // Constant size of the header
	msgSize += name.size(); // Variable-length character name
	msgSize += attachmentCount * sizeof(CHAR_ITEM_DATA); // Armed items
	msgSize += defCfgCount * sizeof(CHAR_CONFIG_ITEM_DATA); // DIM configs

	// Pack Message header
	BYTE* dataBlock = new BYTE[msgSize];
	MSG_CREATE_CHAR_TO_SERVER* header = (MSG_CREATE_CHAR_TO_SERVER*)dataBlock;
	header->SetCategory(MSG_CAT::CREATE_CHAR_TO_SERVER);
	header->m_nameLength = static_cast<short>(name.size());
	header->m_spawnCfg = spwnCfg;
	header->m_entityType = DBindex;
	header->m_numCharItems = attachmentCount;
	header->m_numCharConfigItems = (short)defCfgCount;
	header->m_team = nTeam;
	header->m_scaleX = scaleX;
	header->m_scaleY = scaleY;
	header->m_scaleZ = scaleZ;
	int byteOffset = sizeof(MSG_CREATE_CHAR_TO_SERVER);

	// Pack Character name
	CopyMemory(dataBlock + byteOffset, name.c_str(), name.size());
	byteOffset += name.size();

	// Pack Armed items and DIM slot configurations
	if (defCfgCount + attachmentCount > 0) {
		CHAR_ITEM_DATA* itemDataStruct = (CHAR_ITEM_DATA*)&dataBlock[byteOffset];
		byteOffset += attachmentCount * sizeof(CHAR_ITEM_DATA);

		// DRF - List Lock Crash Fix
		int trace = 0;
		if (pCAI) {
			OBJ_LIST_GUARD(pCAI);
			for (POSITION pos = pCAI->GetHeadPosition(); pos;) {
				auto pIO = (CItemObj*)pCAI->GetNext(pos);
				if (!pIO)
					continue;

				if (pIO->m_equiped) {
					itemDataStruct[trace].m_GLID = pIO->m_globalID;
					trace++;
				}
			}
		}

		CHAR_CONFIG_ITEM_DATA* cfgDataStruct = (CHAR_CONFIG_ITEM_DATA*)&dataBlock[byteOffset];
		byteOffset += defCfgCount * sizeof(CHAR_CONFIG_ITEM_DATA);

		int loop = 0;
		for (size_t i = 0; i < pMO->getSkeletonConfig()->m_charConfig.getItemCount(); i++) {
			const CharConfigItem* pConfigItem = pMO->getSkeletonConfig()->m_charConfig.getItemByIndex(i);
			assert(pConfigItem != nullptr);
			if (pConfigItem == nullptr) {
				continue;
			}

			for (size_t s = 0; s < pConfigItem->getSlotCount(); s++) {
				const CharConfigItemSlot* pSlot = pConfigItem->getSlot(s);
				assert(pSlot != nullptr);
				if (pSlot == nullptr) {
					continue;
				}

				cfgDataStruct[loop].m_ccGlidOrBase = pConfigItem->getGlidOrBase();
				cfgDataStruct[loop].m_ccSlot = (short)s;
				cfgDataStruct[loop].m_ccMeshId = pSlot->m_meshId;
				if (cfgDataStruct[loop].m_ccGlidOrBase == GOB_BASE_ITEM && cfgDataStruct[loop].m_ccSlot < pMO->getSkeleton()->getSkinnedMeshCount() && pMO->getSkeleton()->getSkinnedMesh(s) != nullptr) {
					cfgDataStruct[loop].m_ccMatlId = pMO->getSkeleton()->getSkinnedMesh(s)->m_currentMatInUse;
					cfgDataStruct[loop].m_ccR = -1;
					cfgDataStruct[loop].m_ccG = -1; // < This is not a custom color mesh
					cfgDataStruct[loop].m_ccB = -1;
				} else {
					// Empty Dim Slot.  Lets not panic and give some default values
					// This actually happens for the L eye for some reason.  I will blindly trust the char config struct
					// here even though this should be an error condition.  I don't know why this is happening.
					// This will also happen if the create character button is hit before the mesh finishes loading.
					// Or config value is set to a mesh that does not exist
					cfgDataStruct[loop].m_ccMatlId = pSlot->m_matlId;
					cfgDataStruct[loop].m_ccR = pSlot->m_r;
					cfgDataStruct[loop].m_ccG = pSlot->m_g;
					cfgDataStruct[loop].m_ccB = pSlot->m_b;
				}
				loop++;
			}
		}

		// now loop thru pending items
		for (size_t pendingLoop = 0; pendingLoop < pMO->getSkeleton()->getNumPendingEquippableItems(); pendingLoop++) {
			cfgDataStruct[loop].m_ccGlidOrBase = pMO->getSkeleton()->getPendingEquippableItem(pendingLoop).m_glidOrBase;
			cfgDataStruct[loop].m_ccSlot = 0;
			cfgDataStruct[loop].m_ccMeshId = 0;
			cfgDataStruct[loop].m_ccMatlId = 0;
			cfgDataStruct[loop].m_ccR = -1;
			cfgDataStruct[loop].m_ccG = -1;
			cfgDataStruct[loop].m_ccB = -1;
			loop++;
		}
	}

	// Send Message
	bool ok = m_multiplayerObj->MsgTx(dataBlock, msgSize);

	delete[] dataBlock;

	return ok;
}

bool ClientEngine::PlayCharacter(int index, const char* startUrl, bool reconnect) {
	// Validate startUrl if provided
	std::string sStartUrl(startUrl != nullptr ? startUrl : "");
	if (!sStartUrl.empty() && !STLBeginWithIgnoreCase(sStartUrl, "kaneva://")) {
		assert(false);
		LogWarn("Malformed URL: " << startUrl);
		sStartUrl.clear();
	}

	if (m_reconnectEvent != nullptr || !sStartUrl.empty()) {
		// Use startUrl if m_reconnectEvent is not specified
		if (m_reconnectEvent == nullptr) {
			assert(!sStartUrl.empty());
			GotoPlaceEvent* e = new GotoPlaceEvent();
			assert(index == 0); // char index of zero, WOK specific
			e->GotoUrl(index, sStartUrl.c_str());
			e->AddTo(SERVER_NETID);
			m_reconnectEvent = e;
		}

		if (m_multiplayerObj->m_currentCharacterInUse < 0)
			m_multiplayerObj->m_currentCharacterInUse = index;

		// send the reconnect event
		m_dispatcher.QueueEvent(m_reconnectEvent);
		m_reconnectEvent = 0; // dispatcher owns it now

		return true;
	} else if (m_originalCharacterCount == 0) {
		if (m_multiplayerObj->m_currentCharacterInUse < 0)
			m_multiplayerObj->m_currentCharacterInUse = index;

		GotoPlaceEvent* e = new GotoPlaceEvent();
		e->GotoTutorialZone(m_multiplayerObj->m_currentCharacterInUse);
		e->AddTo(SERVER_NETID);
		m_dispatcher.QueueEvent(e);

		return true;
	} else {
		return ClientBaseEngine::PlayCharacter(index, reconnect);
	}
}

} // namespace KEP