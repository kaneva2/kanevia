///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "KEPMacros.h"
#include "Event\eventsystem\dispatcher.h"
#include "LoadingScreenRAIIHelper.h"

namespace KEP {

LoadingScreenRAIIHelper::LoadingScreenRAIIHelper(Dispatcher& dispatcher, EVENT_ID startID, EVENT_ID successID, EVENT_ID failedID, bool syncEvents) :
		m_bStarted(false),
		m_dispatcher(dispatcher),
		m_zoneLoadingStartEventId(startID),
		m_zoneLoadingCompleteEventId(successID),
		m_zoneLoadingFailedEventId(failedID),
		m_syncEvents(syncEvents),
		m_success(false) {
}

void LoadingScreenRAIIHelper::Start() {
	// Dispatch ZoneLoadingStart Event
	DispatchEvent(m_zoneLoadingStartEventId);

	// Now Started
	m_bStarted = true;
}

void LoadingScreenRAIIHelper::Stop() {
	// Are We Started ?
	if (!m_bStarted)
		return;

	// Dispatch ZoneLoadingComplete/Failed Event
	DispatchEvent(m_success ? m_zoneLoadingCompleteEventId : m_zoneLoadingFailedEventId);

	// Now Stopped
	m_bStarted = false;
}

void LoadingScreenRAIIHelper::DispatchEvent(EVENT_ID eventId) {
	if (eventId != BAD_EVENT_ID && m_dispatcher.HasHandler(eventId)) {
		// Make & Dispatch Event
		IEvent* e = m_dispatcher.MakeEvent(eventId);
		if (m_syncEvents)
			m_dispatcher.ProcessSynchronousEvent(e);
		else
			m_dispatcher.QueueEvent(e);
	}
}

} // namespace KEP