///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <windows.h> // for generic types, .. etc
#include <stdarg.h> // for va_arg, va_start, va_end
#include <stdexcept> // for std::exception class

/*!
 * \brief	The namespace of the Ryeol's library
 *
 * This is the namespace for source codes written by Jo Hyeong-ryeol.
 */
namespace Ryeol {

enum {
	EXCEPTION_BUFF_SIZE = 512 //!< Maximum buffer size for an error message stored
	//! in the varerrmsg_exception class.
};

/*!
 * \brief	A simple exception class with a error message which is allocated in heap by malloc. (Ansi Ver.)
 *
 * This is a simple exception class derived from the standard exception class.
 * This class can set or get an error message. The message is internally allocated in heap by malloc.
 * So it is possible to lose the error message if memory allocation failed.
 */
class errmsg_exceptionA : public std::exception {
public:
	/*! \brief	Default constructor */
	errmsg_exceptionA(void) throw();
	/*! \brief	Constructor with an initial error message */
	errmsg_exceptionA(const char* szErrMsg) throw();
	/*! \brief	Copy constructor */
	errmsg_exceptionA(const errmsg_exceptionA& ee) throw();
	/*! \brief	Default destructor */
	virtual ~errmsg_exceptionA(void) throw();

	/*! \brief	Returns an error message. */
	const char* what(void) const throw();
	/*! \brief	Returns an error message. */
	virtual const char* errmsg(void) const throw();
	/*! \brief	Assigns a new error message. */
	virtual void seterrmsg(const char* szErrMsg) throw();
	/*! \brief	Assignment operator. */
	errmsg_exceptionA& operator=(const errmsg_exceptionA& ee) throw();

protected:
	char* m_szErrMsg; //!< An internal error message pointer.
};

/*!
 * \brief	A simple exception class with a error message which is allocated in heap by malloc. (Unicode Ver.)
 *
 * This is a simple exception class derived from the standard exception class.
 * This class can set or get an error message. The message is internally allocated in heap by malloc.
 * So it is possible to lose the error message if memory allocation failed.
 */
class errmsg_exceptionW : public std::exception {
public:
	/*! \brief	Default constructor */
	errmsg_exceptionW(void) throw();
	/*! \brief	Constructor with an initial error message */
	errmsg_exceptionW(const wchar_t* szErrMsg) throw();
	/*! \brief	Copy constructor */
	errmsg_exceptionW(const errmsg_exceptionW& ee) throw();
	/*! \brief	Default destructor */
	virtual ~errmsg_exceptionW(void) throw();

	/*! \brief	Returns "errmsg_exceptionW" */
	const char* what(void) const throw();
	/*! \brief	Returns an error message. */
	virtual const wchar_t* errmsg(void) const throw();
	/*! \brief	Assigns a new error message. */
	virtual void seterrmsg(const wchar_t* szErrMsg) throw();
	/*! \brief	Assignment operator. */
	errmsg_exceptionW& operator=(const errmsg_exceptionW& ee) throw();

protected:
	wchar_t* m_szErrMsg; //!< An internal error message pointer.
};

/*!
 * \brief	A simple exception class which can take a variable number of arguments. (Ansi Ver.)
 *
 * This class takes a format string and optional arguments and then stores a formatted
 * error message using _vsnprintf. The stored error message can not exceed
 * EXCEPTION_BUFF_SIZE - 1 characters. If an error occured, the stored error messsage will be NULL.
 */
class varerrmsg_exceptionA : public errmsg_exceptionA {
public:
	/*! \brief	Default constructor */
	varerrmsg_exceptionA(void) throw();
	/*! \brief	Constructor with a format string and optional arguments */
	varerrmsg_exceptionA(const char* szErrMsg, ...) throw();
	/*! \brief	Constructor with a format string and a pointer to list of arguments */
	varerrmsg_exceptionA(const char* szErrMsg, va_list arg) throw();

	/*! \brief	Assigns a new error message using _vsnprintf */
	virtual void seterrmsg(const char* szErrMsg, ...) throw();
	/*! \brief	Assigns a new error message using _vsnprintf */
	virtual void seterrmsg(const char* szErrMsg, va_list args) throw();
};

/*!
 * \brief	A simple exception class which can take a variable number of arguments. (Unicode Ver.)
 *
 * This class takes a format string and optional arguments and then stores a formatted
 * error message using _vsnwprintf. The stored error message can not exceed
 * EXCEPTION_BUFF_SIZE - 1 characters. If an error occured, the stored error messsage will be NULL.
 */
class varerrmsg_exceptionW : public errmsg_exceptionW {
public:
	/*! \brief	Default constructor */
	varerrmsg_exceptionW(void) throw();
	/*! \brief	Constructor with a format string and optional arguments */
	varerrmsg_exceptionW(const wchar_t* szErrMsg, ...) throw();
	/*! \brief	Constructor with a format string and a pointer to list of arguments */
	varerrmsg_exceptionW(const wchar_t* szErrMsg, va_list args) throw();

	/*! \brief	Returns "varerrmsg_exceptionW" */
	virtual const char* what(void) const throw();
	/*! \brief	Assigns a new error message using _vsnwprintf */
	virtual void seterrmsg(const wchar_t* szErrMsg, ...) throw();
	/*! \brief	Assigns a new error message using _vsnwprintf */
	virtual void seterrmsg(const wchar_t* szErrMsg, va_list args) throw();
};

} // namespace Ryeol
