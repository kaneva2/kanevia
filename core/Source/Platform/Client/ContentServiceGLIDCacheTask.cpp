///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ContentServiceGLIDCacheTask.h"
#include "ContentService.h"

namespace KEP {

void ContentServiceGLIDCacheTask::ExecuteTask() {
	ContentService::Instance()->ContentMetadataCacheSync(m_glidVector);

	// A per-request completion event may also be specified
	if (m_pEventCompletion) {
		//(*_completionEvent->OutBuffer()) << resultText.c_str() << httpStatusCode;
		m_dispatcher.QueueEvent(m_pEventCompletion);
	}
}

} // namespace KEP