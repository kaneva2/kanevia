@echo off

setlocal

set ProjectDir=%1
set IntDir=%2
set OutDir=%3
set ProjectName=%4
set ConfigurationName=%5

@rem echo ProjectDir=%ProjectDir%                >> \test.out
@rem @rem echo IntDir=%IntDir%                        >> \test.out
@rem echo OutDir=%OutDir%                        >> \test.out
@rem echo ProjectName=%ProjectName%              >> \test.out
@rem echo ConfigurationName=%ConfigurationName%d  >> \test.out



@rem @echo lib /nologo /out:%OutDir%\%ProjectName%_s%ConfigurationName%d.lib  %ProjectDir%%IntDir%*.obj >> \test.out
@echo on
lib /nologo /out:%OutDir%\%ProjectName%_s%ConfigurationName%.lib  %ProjectDir%%IntDir%*.obj >> make_lib.out
@echo off

endlocal
