These files are meant to override standard include files. Include this directory in your C++ compiler include path so that the compiler finds these files before the built-in include files of the same name.

type_traits
This file was modified in order to produce warnings when calling std::make_shared on an object that requires more than 8 byte alignment. With the original file, the compiler silently allows make_shared to be used when it allocates SSE types (or objects containing SSE types). The pointer returned by make_shared is only guaranteed to be aligned to an 8 byte boundary, so usage of SSE objects will randomly fail if certain SSE instructions are used on unaligned memory.


afx.inl
afssock.h
These files were modified to support using the _CSTRING_DISABLE_NARROW_WIDE_CONVERSION preprocessor #define. We want to use _CSTRING_DISABLE_NARROW_WIDE_CONVERSION to avoid automatic conversion between CStringT<char> and CStringT<wchar_t>. These automatic conversions assume an incorrect code page for the conversion. The CStringT<char> objects that we use contain UTF-8 characters, which are not supported in any of the system code pages. Without these file modifications, there will be compile errors when _CSTRING_DISABLE_NARROW_WIDE_CONVERSION is defined.
