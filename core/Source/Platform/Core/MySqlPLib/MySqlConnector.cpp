///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <Log4CPlus/Logger.h>
#include "MySqlConnector.h"

using namespace log4cplus;

namespace KEP {

class MySqlConnectorPlugin : public IMySqlConnector, public AbstractPlugin {
public:
	MySqlConnectorPlugin();
	~MySqlConnectorPlugin();

	virtual void EnumConnections(std::vector<std::string>& connections);
	virtual KGP_Connection* GetConnection(const std::string& conn_name);
	virtual KGP_Connection::ConnectionPtr getConnection(const std::string& conn_name);
	virtual Parameters GetConfig(const std::string& conn_name);

private:
	Logger m_logger;

	std::map<std::string, Parameters> m_dbConfigs;
	std::string m_alertMailHost, m_alertSender, m_alertRecipient;

	friend class MySqlConnectorPluginFactory;
};

MySqlConnectorPlugin::MySqlConnectorPlugin() :
		AbstractPlugin(MODULE_NAME_MYSQLCONNECTOR), m_logger(Logger::getInstance(Utf8ToUtf16(MODULE_NAME_MYSQLCONNECTOR))) {
	addSupportedInterface(INTERFACE_NAME_MYSQLCONNECTOR);
}

MySqlConnectorPlugin::~MySqlConnectorPlugin() {}

void MySqlConnectorPlugin::EnumConnections(std::vector<std::string>& connections) {
	connections.clear();

	for (std::map<std::string, Parameters>::const_iterator it = m_dbConfigs.begin(); it != m_dbConfigs.end(); ++it) {
		connections.push_back(it->first);
	}
}

KGP_Connection* MySqlConnectorPlugin::GetConnection(const std::string& conn_name) {
	LOG4CPLUS_INFO(m_logger, "GetConnection: [" << conn_name << "]");

	std::map<std::string, Parameters>::const_iterator it = m_dbConfigs.find(conn_name);
	if (it == m_dbConfigs.end()) {
		LOG4CPLUS_WARN(m_logger, "GetConnection: db config not found for [" << conn_name << "]");
		return NULL;
	}

	const Parameters& config = it->second;
	KGP_Connection* conn = new KGP_Connection();

	try {
		conn->connect(
			config.dbName.c_str(),
			config.dbServer.c_str(),
			config.dbUser.c_str(),
			config.dbPass.c_str(),
			config.dbPort);
	} catch (mysqlpp::Exception& e) {
		LOG4CPLUS_WARN(m_logger, "GetConnection: mysqlpp exception occurred: " << e.what());
		delete conn;
		conn = NULL;
	}

	return conn;
}

KGP_Connection::ConnectionPtr MySqlConnectorPlugin::getConnection(const std::string& conn_name) {
	LOG4CPLUS_INFO(m_logger, "GetConnection: [" << conn_name << "]");

	std::map<std::string, Parameters>::const_iterator it = m_dbConfigs.find(conn_name);
	if (it == m_dbConfigs.end()) {
		std::stringstream ss;
		ss << "GetConnection: db config not found for [" << conn_name << "]";
		throw IMySqlConnector::Exception(SOURCELOCATION, ss.str());
	}

	const Parameters& config = it->second;
	auto pConnection = std::make_shared<KGP_Connection>();

	try {
		pConnection->connect(config.dbName.c_str(), config.dbServer.c_str(), config.dbUser.c_str(), config.dbPass.c_str(), config.dbPort);
	} catch (mysqlpp::Exception& e) {
		std::stringstream ss;
		ss << "GetConnection: mysqlpp exception occurred: " << e.what();
		pConnection = nullptr;
		throw IMySqlConnector::Exception(SOURCELOCATION, ss.str());
	}

	return pConnection;
}

IMySqlConnector::Parameters MySqlConnectorPlugin::GetConfig(const std::string& conn_name) {
	LOG4CPLUS_INFO(m_logger, "GetConfig: [" << conn_name << "]");

	std::map<std::string, Parameters>::const_iterator it = m_dbConfigs.find(conn_name);
	if (it == m_dbConfigs.end()) {
		LOG4CPLUS_WARN(m_logger, "GetConfig: db config not found for [" << conn_name << "]");
		return Parameters();
	}

	return it->second;
}

////////////////////////////////////////////////////////////////////////
// PluginFactory
char gMySqlConnectorModuleName[] = MODULE_NAME_MYSQLCONNECTOR;

MySqlConnectorPluginFactory::MySqlConnectorPluginFactory() :
		AbstractPluginFactory(MODULE_NAME_MYSQLCONNECTOR) {}

MySqlConnectorPluginFactory::MySqlConnectorPluginFactory(KEPConfig* config) :
		AbstractPluginFactory(MODULE_NAME_MYSQLCONNECTOR), _config(config) {}

const std::string& MySqlConnectorPluginFactory::name() const {
	return AbstractPluginFactory::name();
}

MySqlConnectorPluginFactory::~MySqlConnectorPluginFactory() {}

PluginPtr MySqlConnectorPluginFactory::createPlugin(const TIXMLConfig::Item& configItem) const {
	TiXmlElement* config = configItem._internal;
	if (!config)
		throw NullPointerException(SOURCELOCATION, "No configuration provided!");
	Logger m_logger = Logger::getInstance(Utf8ToUtf16(MODULE_NAME_MYSQLCONNECTOR));

	MySqlConnectorPlugin* p = new MySqlConnectorPlugin();

	// <AlertMailer>
	const TiXmlElement* nodeAlertMailer = config->FirstChildElement("AlertMailer");
	if (nodeAlertMailer) {
		std::string mailHost;
		if (TIXML_SUCCESS == nodeAlertMailer->QueryValueAttribute("mail_host", &mailHost)) {
			p->m_alertMailHost = mailHost;
		}

		std::string sender;
		if (TIXML_SUCCESS == nodeAlertMailer->QueryValueAttribute("from", &sender)) {
			p->m_alertSender = sender;
		}

		std::string recipient;
		if (TIXML_SUCCESS == nodeAlertMailer->QueryValueAttribute("to", &recipient)) {
			p->m_alertRecipient = recipient;
		}
	}

	if (p->m_alertMailHost.empty() || p->m_alertSender.empty() || p->m_alertRecipient.empty()) {
		LOG4CPLUS_WARN(m_logger, "Init: missing one or more parameters for DB alert mailer");
	}

	// <databases>
	const TiXmlElement* group = config->FirstChildElement("databases");

	// Should actually throw an exception, but until we change PluginException to a ChainedException()
	// there just return a null
	//
	if (group == 0)
		return PluginPtr((MySqlConnectorPlugin*)0);

	for (const TiXmlElement* node = group->FirstChildElement("database"); node; node = node->NextSiblingElement("database")) {
		std::string connName;
		if (TIXML_SUCCESS == node->QueryValueAttribute("connection_name", &connName)) {
			IMySqlConnector::Parameters connParams;
			connParams.dbPort = 3306;

			for (const TiXmlElement* elem = node->FirstChildElement(); elem; elem = elem->NextSiblingElement()) {
				if (elem->Value() && elem->GetText()) {
					if (stricmp(elem->Value(), DB_SERVER) == 0) {
						connParams.dbServer = elem->GetText();
					} else if (stricmp(elem->Value(), DB_PORT) == 0) {
						connParams.dbPort = atoi(elem->GetText());
					} else if (stricmp(elem->Value(), DB_NAME) == 0) {
						connParams.dbName = elem->GetText();
					} else if (stricmp(elem->Value(), DB_USER) == 0) {
						connParams.dbUser = elem->GetText();
					} else if (stricmp(elem->Value(), DB_PASSWORD_CLEAR) == 0) {
						connParams.dbPass = elem->GetText();
					} else if (stricmp(elem->Value(), DB_LOCAL_PLAYERS) == 0) {
						connParams.localPlayers = atoi(elem->GetText()) != 0;
					} else {
						connParams.otherSettings.push_back(std::make_pair(elem->Value(), elem->GetText()));
					}
				}
			}

			if (!connParams.dbServer.empty() && connParams.dbPort != 0 && !connParams.dbUser.empty()) {
				if (connParams.dbPass.empty()) {
					//Warn about empty password
					LOG4CPLUS_WARN(m_logger, "Init: [" << connName << "] password NOT specified");
				}

				//Save current entry
				p->m_dbConfigs.insert(make_pair(connName, connParams));

				LOG4CPLUS_INFO(m_logger, "Init: [" << connName << "] = {" << connParams.dbServer << ":" << connParams.dbPort << " " << connParams.dbUser << "/" << connParams.dbPass);
			} else {
				LOG4CPLUS_WARN(m_logger, "Init: Invalid db config record, connection name:" << connName);
			}
		} else {
			LOG4CPLUS_WARN(m_logger, "Init: Invalid db config record, no connection name defined");
		}
	}

	LOG4CPLUS_INFO(m_logger, "Init: *** Loaded " << p->m_dbConfigs.size() << " db config records ***");
	return PluginPtr(p);
}

} // namespace KEP