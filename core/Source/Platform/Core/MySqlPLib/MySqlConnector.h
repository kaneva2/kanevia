///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/KEPUtil/Plugin.h"
#include "mysql++/mysql++.h"
#include "Server/Database/MySqlUtil.h"
#include "Core/Util/ChainedException.h"

#define MODULE_NAME_MYSQLCONNECTOR "MySqlConnector"
#define INTERFACE_NAME_MYSQLCONNECTOR "IMySqlConnector"

namespace KEP {

class __declspec(dllexport) IMySqlConnector {
public:
	CHAINED(Exception);

	struct Parameters {
		std::string dbServer, dbName, dbUser, dbPass;
		UINT dbPort;
		bool localPlayers;

		std::vector<std::pair<std::string, std::string>> otherSettings;

		Parameters() :
				dbPort(0), localPlayers(false) {}
	};

	virtual void EnumConnections(std::vector<std::string>& connections) = 0;
	virtual KGP_Connection* GetConnection(const std::string& conn_name) = 0;

	/**
     * Given the name of a connector defined in the IMySqlConnector, allocation
     * a KGP_Connection reference
     * @param  conn_name, the name of the connector with which to connect.
     * @return Reference to requested connection
     * @throws IMySqlConnection::Exception()
     */
	virtual KGP_Connection::ConnectionPtr getConnection(const std::string& conn_name) = 0;

	virtual Parameters GetConfig(const std::string& conn_name) = 0;
};

class __declspec(dllexport) MySqlConnectorPluginFactory : public AbstractPluginFactory {
public:
	MySqlConnectorPluginFactory();
	MySqlConnectorPluginFactory(KEPConfig* config);
	virtual ~MySqlConnectorPluginFactory();

	const std::string& name() const;
	PluginPtr createPlugin(const TIXMLConfig::Item& config) const;

private:
	KEPConfig* _config;
};

AbstractPluginFactory* createPluginFactory(IMySqlConnector* pFactoryTypeHint);

} // namespace KEP