///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Common/KEPUtil/Plugin.h"
#include "MySqlConnector.h"

using namespace KEP;

extern "C" __declspec(dllexport) void EnumeratePlugins(PluginFactoryVecPtr v) {
	v->push_back(PluginFactoryPtr(new MySqlConnectorPluginFactory()));
}
