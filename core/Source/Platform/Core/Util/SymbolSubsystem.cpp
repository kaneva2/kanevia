///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Core/Util/SymbolSubsystem.h"

// Warning in Windows SDK header when building with VS2015
#pragma warning(push)
#pragma warning(disable : 4091) // 'typedef ': ignored on left of '' when no variable is declared
#include "dbghelp.h"
#pragma warning(pop)

SymbolSubsystem::SymbolSubsystem() {
	// Would be nice if vld.h defined some flag to determine that
	// vld is in play.
	//
	//#ifdef _DEBUG
	SymInitialize(GetCurrentProcess(), NULL, TRUE);
	//#endif
}

SymbolSubsystem::~SymbolSubsystem() {
	//#ifdef _DEBUG
	SymCleanup(GetCurrentProcess());
	//#endif
}
