///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Core/Util/HighPrecisionTime.h"

typedef int64_t Tick; // timer ticks

static inline Tick TimeTick() {
	LARGE_INTEGER count = { 0 };
	::QueryPerformanceCounter(&count);
	return (Tick)count.QuadPart;
}

static inline Tick TimeTickHz() {
	static Tick s_tickHz = 0;
	if (s_tickHz)
		return s_tickHz;
	LARGE_INTEGER tickHz = { 0 };
	::QueryPerformanceFrequency(&tickHz);
	return s_tickHz = (Tick)tickHz.QuadPart;
}

double fTime::Time(double scale) {
	return scale * (double)TimeTick() / (double)TimeTickHz();
}

void fTime::SleepNs(const fTime::Ns& ns) {
	if (ns == 0)
		return;

	// this_thread::sleep_for appears to be causing intermittent crashes in Microsoft's timer code in VS2013.
	HANDLE hTimer = ::CreateWaitableTimer(NULL, TRUE, NULL);
	LARGE_INTEGER dueTime;
	dueTime.QuadPart = llrint(0.01 * ns); // 100ns intervals
	dueTime.QuadPart = -dueTime.QuadPart; // neg=relative time
	if (hTimer && ::SetWaitableTimer(hTimer, &dueTime, 0, NULL, NULL, FALSE))
		::WaitForSingleObject(hTimer, INFINITE);
	else
		::Sleep(lrint(ns * 1e-6)); // Argument to ::Sleep is in milliseconds.

	if (hTimer)
		::CloseHandle(hTimer);
}

static fTime::Ms s_timeSyncMs = 0.0; // delta sync time (ms)
fTime::Ms fTime::SetTimeSyncMs(const Ms& ms) {
	s_timeSyncMs = ms - TimeMs();
	return s_timeSyncMs; // delta time sync (ms)
}
fTime::Ms fTime::TimeSyncMs() {
	return s_timeSyncMs + TimeMs();
}
void fTime::SleepMs(const Ms& ms) {
	SleepNs(ms * 1e6);
}
void fTime::SleepSec(const Sec& sec) {
	SleepNs(sec * 1e9);
}

Timer::Timer(const State& state) :
		m_state(state), m_startNs((state == Timer::State::Running) ? fTime::TimeNs() : 0), m_elapseNs(0), m_count(0) {}
//fTime::Ms Timer::ElapsedMs() const {
//	return ElapsedNs() / 1e6;
//}
TimeMs Timer::ElapsedMs() const {
	return ElapsedNs() / 1e6;
}

Timer& Timer::Reset(bool resetElapse) {
	if (resetElapse)
		m_elapseNs = 0.0;
	m_count = 0;
	m_startNs = (IsRunning() ? fTime::TimeNs() : 0.0);
	return *this;
}
Timer& Timer::Pause() {
	if (IsPaused())
		return *this;
	m_elapseNs += fTime::ElapsedNs(m_startNs);
	SetState(State::Paused);
	return *this;
}

fTime::Ms fTime::ElapsedMs(const Ms& ms) {
	return TimeMs() - ms;
}

Timer& Timer::Start() {
	if (IsRunning())
		return *this;
	m_startNs = fTime::TimeNs();
	SetState(State::Running);
	m_count++;
	return *this;
}
