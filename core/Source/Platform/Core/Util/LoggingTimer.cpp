///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Core/Util/LoggingTimer.h"
using namespace log4cplus;

unsigned long LoggingTimer::m_infoLimit = 1000;
unsigned long LoggingTimer::m_errorLimit = 1000;

LoggingTimer::~LoggingTimer() {
	CheckTime();
}

void LoggingTimer::CheckTime() {
	if (m_logged)
		return;
	TimeMs elapsed = m_timer.ElapsedMs();
	if (elapsed >= m_errorLimit)
		LogMessage(m_timingLogger, true, elapsed);
	else if (elapsed >= m_infoLimit)
		LogMessage(m_timingLogger, false, elapsed);
	m_logged = true;
}

void LoggingTimer::LogMessage(Logger& logger, bool errorLimit, TimeMs elapsed) {
	if (errorLimit) {
		LOG4CPLUS_ERROR(m_timingLogger, m_msg << "," << elapsed << "ms");
	} else {
		LOG4CPLUS_INFO(m_timingLogger, m_msg << "," << elapsed << "ms");
	}
}

void LoggingTimer::SetTimeLimits(ULONG errorLimit, ULONG infoLimit) {
	m_errorLimit = errorLimit;
	m_infoLimit = infoLimit;
}
