///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/BlockingQueue.h"

namespace KEP {

/**
 * Handoff box is a mechanism typically for passing items over a thread 
 * boundary.  It is most easily understood as a queue with a fixed size 
 * of 1. 
 * 
 * While it provides both blocking and non-blocking methods for 
 * retrieving the item in the HandoffBox, it's typical use as a mechanism
 * for passing data over a thread boundary, the blocking method is
 * typically used.   When accessed in blocking mode, if there is nothing 
 * in the handoff box, a call to get() will block until either an item is 
 * placed in the hand off box, or the HandoffBox is closed() (e.g. as in 
 * application shutdown).  In the latter case a null pointer is returned.
 * So either the results of a get() or the HandoffBox's state() should be
 * checked.
 */
template <typename T>
class HandoffBox {
public:
	/**
	 * Determine if there is an element in the handoff box.
	 */
	bool occupied() {
		return _queue.size() > 0;
	}

	/**
	 * Place an element in the handoff box.
	 */
	bool push(const T& x) {
		//	cout << "HandoffBox::push()" << endl;
		if (occupied())
			return false;

		//	cout << "HandoffBox::push() pushing!" << endl;
		_queue.push(x);
		return true;
	}

	/**
	 * Pop the only element off of this one element queue.
	 * blocks until an element is placed in the handoff box
	 * or until the handoff box is closed.
	 */
	jsWaitRet pop(T& retObj, unsigned long timeout = INFINITE_WAIT) {
		// cout << "HandoffBox::pop(" << timeout << ")" << endl;
		jsWaitRet ret = _queue.pop(retObj, timeout);

		// cout << "HandoffBox::pop() returning [" << ret << "]" << endl;
		return ret;
	}

	/**
	 * Close this queue.  Unblocks any waiters.
	 */
	void close() {
		_queue.close();
	}

private:
	BlockingQueue<T> _queue;
};

} // namespace KEP
