///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>
#include <vector>
#include <functional>

// ParameterBuffer
// Stores function parameters in a binary buffer.
// Simplifies forwarding function parameters across process boundaries.
class ParameterBuffer {
#pragma pack(push, 4)
	struct ParameterListHeader {
		uint32_t m_nParameters;
		struct Parameter {
			uint32_t m_nOffset;
			uint32_t m_nLength;
			uint32_t m_iType;
		};

#pragma warning(push)
#pragma warning(disable : 4200) // nonstandard extension: zero length array
		Parameter m_Parameters[];
#pragma warning(pop)
	};
#pragma pack(pop, 4)

	enum class eParameterType {
		None,
		Bool,
		UInt8,
		Int32,
		Uint32,
		Double,
		String,
	};

	// Overloaded functions for converting types to an enum value.
	// This should be converted to templated data once supported by the compiler.
	static eParameterType GetParameterTypeFor(const char** const) {
		return eParameterType::String;
	}
	static eParameterType GetParameterTypeFor(bool* const) {
		return eParameterType::Bool;
	}
	static eParameterType GetParameterTypeFor(uint8_t* const) {
		return eParameterType::UInt8;
	}
	static eParameterType GetParameterTypeFor(int32_t* const) {
		return eParameterType::Int32;
	}
	static eParameterType GetParameterTypeFor(uint32_t* const) {
		return eParameterType::Uint32;
	}
	static eParameterType GetParameterTypeFor(double* const) {
		return eParameterType::Double;
	}
	static eParameterType GetParameterTypeFor(std::string* const) {
		return eParameterType::String;
	}

public:
	void Init(uint32_t nParameters) {
		m_buffer.resize(sizeof(ParameterListHeader) + nParameters * sizeof(ParameterListHeader::Parameter));
		ParameterListHeader* pParameterListHeader = reinterpret_cast<ParameterListHeader*>(m_buffer.data());
		pParameterListHeader->m_nParameters = nParameters;
		for (uint32_t i = 0; i < nParameters; ++i) {
			pParameterListHeader->m_Parameters[i].m_nOffset = m_buffer.size();
			pParameterListHeader->m_Parameters[i].m_nLength = 0;
			pParameterListHeader->m_Parameters[i].m_iType = (uint32_t)eParameterType::None;
		}
	}

	template <typename T>
	static bool IsObjectInBuffer(const T* pPointer, const void* pBuffer, size_t nBufferBytes) {
		if (reinterpret_cast<const char*>(pPointer + 1) > static_cast<const char*>(pBuffer) + nBufferBytes)
			return false;
		return true;
	}

	// Gets a parameter from specified buffer.
	template <typename ParamType>
	static bool GetParameterFromBuffer(const void* pBuffer, size_t nBufferBytes, uint32_t iParam, ParamType* pParam) {
		const ParameterListHeader* pParameterListHeader = reinterpret_cast<const ParameterListHeader*>(pBuffer);
		const uint32_t* pnParameters = &pParameterListHeader->m_nParameters;
		if (!IsObjectInBuffer(pnParameters, pBuffer, nBufferBytes))
			return false; // Buffer error. Buffer does not contain a count of parameters.
		if (iParam >= *pnParameters)
			return false; // Buffer does not contain requested parameter.
		const ParameterListHeader::Parameter* pParameter = &pParameterListHeader->m_Parameters[iParam];
		if (!IsObjectInBuffer(pParameter, pBuffer, nBufferBytes))
			return false; // Buffer error. Metadata for parameter iParam is not in buffer.
		uint32_t iType = pParameter->m_iType;
		if (iType != (uint32_t)GetParameterTypeFor(pParam))
			return false; // Type in buffer does not match requested type.
		uint32_t nLength = pParameter->m_nLength;
		uint32_t iOffset = pParameter->m_nOffset;
		const char* pParamData = static_cast<const char*>(pBuffer) + iOffset;
		if (iOffset > nBufferBytes)
			return false; // Buffer error. Data starts outside of buffer.
		if (nLength > nBufferBytes - iOffset)
			return false; // Buffer error. Data does not fit in buffer.
		return InitializeParamFromData(pParam, pParamData, nLength);
	}

	// Get parameters stored in buffer. Returns false if not enough parameters in buffer or types don't match arguments.
	// Extra parameters in buffer are ignored.
	template <typename... ParameterTypes>
	static bool GetParametersFromBuffer(const void* pBuffer, size_t nBufferBytes, ParameterTypes... params) {
		return GetNextParameterFromBuffer(pBuffer, nBufferBytes, 0, params...);
	}

	// Get one parameter from the buffer.
	// Returns false if the parameter does not exist in the buffer or is of the wrong type.
	template <typename ParamType>
	bool GetParameter(uint32_t iParam, ParamType* pParam) const {
		return GetParameterFromBuffer(m_buffer.data(), m_buffer.size(), iParam, pParam);
	}

	// Get parameters stored in buffer. Returns false if not enough parameters in buffer or types don't match arguments.
	// Extra parameters in buffer are ignored.
	template <typename... ParameterTypes>
	bool GetParameters(ParameterTypes... params) const {
		return GetNextParameter(0, params...);
	}

	// Get parameters stored in buffer. Returns false if parameter count or types don't match arguments.
	template <typename... ParameterTypes>
	bool GetAllParameters(ParameterTypes... params) const {
		if (sizeof...(params) != reinterpret_cast<ParameterListHeader*>(m_buffer.data())->m_nParameters)
			return false;
		return GetParameters(params...);
	}

	// Set null terminated string parameter.
	bool SetParameter(uint32_t iParam, const char* pszString) {
		size_t len = strlen(pszString) + 1;
		return SetParameterData(iParam, eParameterType::String, pszString, len);
	}

	// Set std::string parameter.
	bool SetParameter(uint32_t iParam, const std::string& str) {
		return SetParameterData(iParam, eParameterType::String, str.c_str(), str.size() + 1);
	}

	// Set parameter that can be copied as binary data. (integer or float types)
	template <typename ParamType>
	bool SetParameter(uint32_t iParam, ParamType iValue) {
		static_assert(std::is_pod<ParamType>::value && !std::is_pointer<ParamType>::value, "Unsupported parameter type");
		return SetParameterData(iParam, GetParameterTypeFor(&iValue), reinterpret_cast<const char*>(&iValue), sizeof(iValue));
	}

	// Set a bool parameter
	bool SetParameter(uint32_t iParam, bool bValue) {
		uint8_t iValue = bValue;
		return SetParameterData(iParam, eParameterType::Bool, reinterpret_cast<const char*>(&iValue), sizeof(iValue));
	}

	// Initialize this buffer with parameter list.
	template <typename... ParameterTypes>
	bool InitWithParams(ParameterTypes... params) {
		Init(sizeof...(params));
		return SetParameters(params...);
	}

	// Take function arguments and put each in buffer as a separate parameter.
	template <typename... ParameterTypes>
	bool SetParameters(ParameterTypes... params) {
		return SetNextParameters(0, params...);
	}

	// Take buffer data from caller
	void AcquireBuffer(std::vector<char>&& buffer) {
		m_buffer.clear();
		m_buffer.swap(buffer);
	}

	// Give buffer data to caller
	std::vector<char> ReleaseBuffer() {
		std::vector<char> buffer;
		buffer.swap(m_buffer);
		return buffer;
	}

	// Returns the number of parameters in the buffer. Returns 0 on error.
	uint32_t GetParameterCount() const {
		if (m_buffer.size() < sizeof(ParameterListHeader))
			return 0;
		return reinterpret_cast<const ParameterListHeader*>(m_buffer.data())->m_nParameters;
	}

	// Calls a std::function with the parameters stored in this buffer.
	// Returns false if parameters are invalid.
	// The return value of the handler can be captured by passing in a pointer to a variable to store it.
	template <typename ReturnType, typename... Args>
	bool HandleMessage(const std::function<ReturnType(Args...)>& func, ReturnType* pReturnValue, size_t iFirstParam = 0) const {
		// Validate parameter count.
		if (GetParameterCount() != iFirstParam + sizeof...(Args)) {
			//LogError("Invalid number of parameters for handler: " << GetParameterCount() << ", should be " << sizeof...(Args));
			return false;
		}
		// Unpack arguments and call handler.
		return HandleMessageHelper<sizeof...(Args)>(iFirstParam, func, pReturnValue);
	}
	// Ignore return value.
	template <typename ReturnType, typename... Args>
	bool HandleMessage(const std::function<ReturnType(Args...)>& func, size_t iFirstParam = 0) const {
		return HandleMessage(func, (ReturnType*)nullptr, iFirstParam);
	}

	// Handle with member function.
	template <typename ClassType, typename ReturnType, typename... Args>
	bool HandleMessageWithMemberFunction(ClassType* pClass, ReturnType (ClassType::*pfnHandler)(Args...), ReturnType* pReturnValue, size_t iFirstParam = 0) const {
		auto AdapterFunction = [pClass, pfnHandler](Args... args) -> ReturnType {
			return (pClass->*pfnHandler)(args...);
		};
		std::function<ReturnType(Args...)> F = AdapterFunction;
		return HandleMessage(F, pReturnValue, iFirstParam);
	}
	// Ignore return value.
	template <typename ClassType, typename ReturnType, typename... Args>
	bool HandleMessageWithMemberFunction(ClassType* pClass, ReturnType (ClassType::*pfnHandler)(Args...), size_t iFirstParam = 0) const {
		return HandleMessageWithMemberFunction(pClass, pfnHandler, (ReturnType*)nullptr, iFirstParam);
	}

private:
	// Copy parameter data from buffer to provided pointer.
	// Utility function for handling POD types.
	template <typename ParamType>
	static bool InitializeParamFromData(ParamType* pParam, const char* pParamData, uint32_t nLength) {
		static_assert(std::is_pod<ParamType>::value && !std::is_pointer<ParamType>::value, "Unsupported parameter type");
		if (sizeof(ParamType) != nLength)
			return false;
		memcpy(pParam, pParamData, nLength);
		return true;
	}
	// Copy parameter data from buffer to bool.
	static bool InitializeParamFromData(bool* pParam, const char* pParamData, uint32_t nLength) {
		if (nLength != 1)
			return false;
		*pParam = *pParamData != 0;
		return true;
	}
	// Copy a pointer to a null terminated string in the buffer to a char*.
	static bool InitializeParamFromData(const char** pParam, const char* pParamData, uint32_t nLength) {
		if (nLength == 0 || pParamData[nLength - 1] != 0)
			return false; // Should have a null terminator.
		*pParam = pParamData;
		return true;
	}
	// Copy parameter data from buffer to std::string
	static bool InitializeParamFromData(std::string* pParam, const char* pParamData, uint32_t nLength) {
		const char* pChars;
		if (!InitializeParamFromData(&pChars, pParamData, nLength))
			return false;
		pParam->assign(pChars, pChars + nLength - 1);
		return true;
	}

	// Low-level call to set a single parameter from pointer to binary data.
	bool SetParameterData(uint32_t iParam, eParameterType type, const char* pData, size_t nLen) {
		if (m_buffer.empty())
			return false; // Need to call Init().
		ParameterListHeader* pParameterListHeader = reinterpret_cast<ParameterListHeader*>(m_buffer.data());
		if (iParam >= pParameterListHeader->m_nParameters)
			return false; // Invalid parameter.
		pParameterListHeader->m_Parameters[iParam].m_nOffset = m_buffer.size();
		pParameterListHeader->m_Parameters[iParam].m_nLength = nLen;
		pParameterListHeader->m_Parameters[iParam].m_iType = (uint32_t)type;
		m_buffer.insert(m_buffer.end(), pData, pData + nLen);
		return true;
	}

	// Set an arbitrary number of parameters. Set first parameter then call recursively with remaining parameters.
	template <typename... ParameterTypes, typename NextParameterType>
	bool SetNextParameters(uint32_t iNextParamIndex, NextParameterType nextParamValue, ParameterTypes... params) {
		if (!SetParameter(iNextParamIndex, nextParamValue))
			return false;
		return SetNextParameters(iNextParamIndex + 1, params...);
	}
	// No more parameters to set. Nothing to do.
	bool SetNextParameters(uint32_t iNextParamIndex) {
		return true;
	}
	// Get an arbitrary number of parameters. Get first parameter then call recursively with remaining parameters.
	template <typename NextParameterType, typename... ParameterTypes>
	bool GetNextParameter(uint32_t iNextParameter, NextParameterType* pNextParameter, ParameterTypes... params) const {
		if (!GetParameter(iNextParameter, pNextParameter))
			return false;
		return GetNextParameter(iNextParameter + 1, params...);
	}
	// No more parameters to get. Nothing to do.
	template <typename... ParameterTypes>
	bool GetNextParameter(uint32_t iNextParameter) const {
		return true;
	}

	// Get an arbitrary number of parameters. Get first parameter then call recursively with remaining parameters.
	template <typename NextParameterType, typename... ParameterTypes>
	static bool GetNextParameterFromBuffer(const void* pBuffer, size_t nBufferBytes, uint32_t iNextParameter, NextParameterType* pNextParameter, ParameterTypes... params) {
		if (!GetParameterFromBuffer(pBuffer, nBufferBytes, iNextParameter, pNextParameter))
			return false;
		return GetNextParameterFromBuffer(pBuffer, nBufferBytes, iNextParameter + 1, params...);
	}
	// No more parameters to get. Nothing to do.
	template <typename... ParameterTypes>
	static bool GetNextParameterFromBuffer(const void* pBuffer, size_t nBufferBytes, uint32_t iNextParameter) {
		return true;
	}

	template <typename ReturnType, typename FunctionType, typename... Args>
	static void CallFunctionWithArgs(ReturnType* pReturnValue, FunctionType f, Args... args) {
		if (pReturnValue)
			*pReturnValue = f(args...);
		else
			f(args...);
	}
	template <typename FunctionType, typename... Args>
	static void CallFunctionWithArgs(const void* pReturnValue, FunctionType f, Args... args) {
		f(args...);
	}

	// Automatic marshalling of function parameters.
	template <int i, typename... functionArgs, typename ReturnType, typename... UnpackedArgTypes>
	bool HandleMessageHelper(std::enable_if_t<i == 0, uint32_t> iNextParam, const std::function<ReturnType(functionArgs...)>& handler, ReturnType* pReturnValue, UnpackedArgTypes&... unpackedArgs) const {
		// No more parameters to unpack. Call the handler.
		CallFunctionWithArgs(pReturnValue, handler, unpackedArgs...);
		return true;
	}
	// Unpack a single argument and recurse
	template <int i, typename... functionArgs, typename ReturnType, typename... UnpackedArgTypes>
	bool HandleMessageHelper(std::enable_if_t<i != 0, uint32_t> iNextParam, const std::function<ReturnType(functionArgs...)>& handler, ReturnType* pReturnValue, UnpackedArgTypes&... unpackedArgs) const {
		// Type of the next parameter
		static const size_t iFunctionArg = sizeof...(functionArgs) - i;
		std::remove_const_t<std::remove_reference_t<std::tuple_element<iFunctionArg, std::tuple<functionArgs...>>::type>> nextValue;
		// Get the parameter from the buffer.
		if (!GetParameter(iNextParam, &nextValue))
			return false;
		// Process remaining parameters.
		return HandleMessageHelper<i - 1>(iNextParam + 1, handler, pReturnValue, unpackedArgs..., nextValue);
	}

private:
	std::vector<char> m_buffer;
};
