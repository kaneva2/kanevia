///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "Core/Util/CallbackSystem.h"
#include "Core/Util/Memory.h"
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/key_extractors.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <set>

namespace KEP {

CallbackSystem CallbackSystem::s_Instance;

namespace {
struct CallbackMapEntry {
	void* m_pCallbackProvider;
	uint64_t m_CallbackId;

	// Pointer to owner of the callback. Raw pointer to do pointer comparisons in map
	// and weak_ptr to test that owner is still alive when making callback.
	void* m_pOwner;
	std::weak_ptr<void> m_pWeakOwner;

	std::function<void()> m_fnCallback;
};
} // namespace

// First map index is to look up all callbacks registered to a particular object. This is for efficient removal
// when objects cancel their callbacks (usually because they're being deleted).
// Second map index is to look up all callbacks registered for a given resource. This one is used for efficiently
// determining which callbacks to call when a resource is loaded.
class CallbackSystem::CallbackMap : public boost::multi_index_container<
										CallbackMapEntry,
										boost::multi_index::indexed_by<
											boost::multi_index::ordered_non_unique<
												boost::multi_index::composite_key<
													CallbackMapEntry,
													boost::multi_index::member<CallbackMapEntry, void*, &CallbackMapEntry::m_pOwner>,
													boost::multi_index::member<CallbackMapEntry, void*, &CallbackMapEntry::m_pCallbackProvider>,
													boost::multi_index::member<CallbackMapEntry, uint64_t, &CallbackMapEntry::m_CallbackId>>>,
											boost::multi_index::ordered_non_unique<
												boost::multi_index::composite_key<
													CallbackMapEntry,
													boost::multi_index::member<CallbackMapEntry, void*, &CallbackMapEntry::m_pCallbackProvider>,
													boost::multi_index::member<CallbackMapEntry, uint64_t, &CallbackMapEntry::m_CallbackId>>>>> {
};

CallbackSystem::CallbackSystem() {
	m_pCallbacksPending = std::make_unique<CallbackMap>();
	m_pCallbacksReady = std::make_unique<CallbackMap>();
}

CallbackSystem::~CallbackSystem() {
}

bool CallbackSystem::RegisterCallback(void* pCallbackProvider, uint64_t callbackId, void* pCallbackOwner, const std::weak_ptr<void>& pWeakCallbackOwner, std::function<void()> callback) {
	if (!pCallbackOwner)
		return false;

	{
		std::lock_guard<fast_mutex> lock(m_mtxCallbackMaps);
		m_pCallbacksPending->insert(CallbackMapEntry{ pCallbackProvider, callbackId, pCallbackOwner, pWeakCallbackOwner, std::move(callback) });
	}

	return true;
}

bool CallbackSystem::RegisterCallback(void* pCallbackProvider, const std::vector<uint64_t>& aCallbackIds, void* pCallbackOwner, const std::weak_ptr<void>& pWeakCallbackOwner, std::function<void()> callback) {
	if (!pCallbackOwner || aCallbackIds.empty())
		return false;

	// If only one callback given, use more efficient version of RegisterCallback().
	if (aCallbackIds.size() == 1)
		return RegisterCallback(pCallbackProvider, aCallbackIds.front(), pCallbackOwner, pWeakCallbackOwner, std::move(callback));

	// Create shared data to detect when all requested IDs are ready.
	struct CombinedCallbackData {
		std::set<uint64_t> m_RemainingIds;
		std::function<void()> m_fnCallback;
	};
	std::shared_ptr<CombinedCallbackData> pCombinedCallbackData = std::make_shared<CombinedCallbackData>();
	pCombinedCallbackData->m_RemainingIds.insert(aCallbackIds.begin(), aCallbackIds.end());
	pCombinedCallbackData->m_fnCallback = std::move(callback);

	// Register callback that is called as each requested ID is ready, and call user's callback when all are ready.
	for (uint64_t callbackId : aCallbackIds) {
		auto combinedCallback = [callbackId, pCombinedCallbackData]() {
			pCombinedCallbackData->m_RemainingIds.erase(callbackId);
			if (pCombinedCallbackData->m_RemainingIds.empty())
				pCombinedCallbackData->m_fnCallback();
		};
		RegisterCallback(pCallbackProvider, callbackId, pCallbackOwner, pWeakCallbackOwner, combinedCallback);
	}
	return true;
}

// Unregisters a single callback
size_t CallbackSystem::UnregisterCallback(void* pCallbackProvider, uint64_t callbackId, void* pCallbackOwner) {
	if (!pCallbackOwner)
		return 0;

	// Local helper function to erase from one map.
	auto EraseFromMap = [pCallbackOwner, callbackId, pCallbackProvider](CallbackMap* pMap) {
		auto rng = pMap->get<0>().equal_range(std::make_tuple(pCallbackOwner, pCallbackProvider, callbackId));
		size_t nErased = 0;
		while (rng.first != rng.second) {
			rng.first = pMap->get<0>().erase(rng.first);
			++nErased;
		}
		return nErased;
	};

	size_t nErased = 0;
	{
		std::lock_guard<fast_mutex> lock(m_mtxCallbackMaps);
		nErased += EraseFromMap(m_pCallbacksPending.get());
		nErased += EraseFromMap(m_pCallbacksReady.get());
	}
	return nErased;
}

// Unregisters all callbacks with a particular owner. Usually used when owner is destroyed.
size_t CallbackSystem::UnregisterAllCallbacksWithOwner(void* pCallbackOwner) {
	// Local helper function to erase from one map.
	auto EraseFromMap = [pCallbackOwner](CallbackMap* pMap) {
		auto rng = pMap->get<0>().equal_range(pCallbackOwner);
		size_t nErased = 0;
		while (rng.first != rng.second) {
			rng.first = pMap->get<0>().erase(rng.first);
			++nErased;
		}
		return nErased;
	};

	size_t nErased = 0;
	{
		std::lock_guard<fast_mutex> lock(m_mtxCallbackMaps);
		nErased += EraseFromMap(m_pCallbacksPending.get());
		nErased += EraseFromMap(m_pCallbacksReady.get());
	}
	return nErased;
}

// Called by provider when callbacks associated with callbackId should be called.
void CallbackSystem::CallbackReady(void* pCallbackProvider, uint64_t callbackId) {
	{
		// Move from pending list to ready list.
		std::lock_guard<fast_mutex> lock(m_mtxCallbackMaps);
		auto range = m_pCallbacksPending->get<1>().equal_range(std::make_tuple(pCallbackProvider, callbackId));
		for (auto itr = range.first; itr != range.second; ++itr)
			m_pCallbacksReady->insert(*itr);
		m_pCallbacksPending->get<1>().erase(range.first, range.second);
	}
}

void CallbackSystem::ExecuteCallbacks() {
	CallbackMap callbacksReady;
	{
		std::lock_guard<fast_mutex> lock(m_mtxCallbackMaps);
		callbacksReady.swap(*m_pCallbacksReady);

		// This code can be useful for forcing all callbacks to be triggered.
		// Triggering them can help us identify them to figure out which callback was not triggered.
		static bool s_bFakeAllReady = false;
		if (s_bFakeAllReady) {
			for (auto itr = m_pCallbacksPending->begin(); itr != m_pCallbacksPending->end(); ++itr) {
				callbacksReady.insert(*itr);
			}
			m_pCallbacksPending->clear();
		}
	}

	for (auto& mapEntry : callbacksReady) {
		// Grab a shared_ptr to the owner to keep it alive until after the callback.
		// If we can't get a shared_ptr to it, it's either because the object was deleted (in which case
		// we don't call the callback), or the owner shared_ptr is empty (in which case the owner does not
		// need to be alive for the call).
		std::shared_ptr<void> pOwner = mapEntry.m_pWeakOwner.lock();
		if (pOwner || IsWeakPtrEmpty(mapEntry.m_pWeakOwner))
			mapEntry.m_fnCallback();
	}
}

} // namespace KEP
