///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <iostream>
#include <ostream>
#include <iomanip>
#include <sstream>
#include "Core/Util/ChainedException.h"

/**
Class to format memory blocks for analysis.  Given and address (char*)
and a then size of the memory block, will generate out like...

================== buffer [0x3ceef4] length [      32] =================
00                       01
00 01 02 03 04 05 06 07  00 01 02 03 04 05 06 07
-----------------------  -----------------------
0000|20 21 22 23 24 25 26 27  28 29 2a 2b 2c 2d 2e 2f    !"#$%&'()*+,-./
0001|30 31 32 33 34 35 36 37  38 39 3a 3b 3c 3d 3e 3f   0123456789:;<=>?
0002|40 41 42 43 44 45 46 47  48 49 4a 4b 4c 4d 4e 4f   @ABCDEFGHIJKLMNO
0003|50 51 52 53 54 55 56 57  58 59 5a 5b 5c 5d 5e 5f   PQRSTUVWXYZ[\]^_
-----------------------  -----------------------
*/

namespace KEP {
class ByteBufRuler {
public:
	enum OffsetFormat {
		RowOffset // Simply a row number for each line of output
		,
		DecOffset // The offset of the first byte in the row in decimal format
		,
		HexOffset // The offset of the first byte in the row in hexadecimal format
	};

	static void formatTo(std::ostream& ss, const unsigned char* buf, unsigned long buflen, OffsetFormat format = HexOffset) {
		//#ifdef __CHAINEDEXCEPTION_H__
		// ChainedException has not been migrated yet
		// and including it adds a lot of dependencies
		//
		// Bottom line, if you want this to throw a ChainedException, include
		// it before including this file.
		//
		if (buf == 0)
			throw NullPointerException(SOURCELOCATION);
		//#endif
		header(ss, buf, buflen);
		const unsigned char* curr = buf;
		unsigned long rows = (buflen + 15) / 16;
		int limit = 16;
		for (unsigned long nrow = 0; nrow < rows; nrow++) {
			if (nrow == (rows - 1))
				// last row.
				limit = (int)(buflen % 16);
			if (limit == 0)
				limit = 16;

			// Column 1 to be formatted according to provided RowFormat
			ss << std::setfill('0');
			//				<< std::setw(4);

			switch (format) {
				case RowOffset:
					ss << std::setfill('0')
					   << std::setw(6)
					   << std::dec << nrow;
					break;
				case DecOffset:
					ss << std::setfill('0')
					   << std::setw(6)
					   << std::dec
					   << nrow * 16;
					break;
				case HexOffset:
					ss << "0x"
					   << std::setfill('0')
					   << std::setw(4)
					   << std::hex
					   << nrow * 16;
			}
			ss << "|";

			row(ss, curr, limit);
			curr += 16;
		}

		rule(ss);
		ss << std::dec;
	}

private:
	static void rule(std::ostream& s) {
		s << "       ";
		s << "-----------------------  "
		  << "-----------------------"
			//            << std::endl
			;
	}

	static void header(std::ostream& ss, const unsigned char* buf, unsigned long buflen, OffsetFormat format = HexOffset) {
		ss << "================== buffer [" << std::showbase << std::hex << (unsigned long)buf << "] length [" << std::dec << std::noshowbase << std::setw(8) << std::setfill(' ') << buflen << "] =================" << std::noshowbase << std::endl;
		ss << "                  00                       01" << std::endl;

		switch (format) {
			case HexOffset:
				ss << "       00 01 02 03 04 05 06 07  08 09 0A 0B 0C 0D 0E 0F";
				break;
			case DecOffset:
				ss << "       00 01 02 03 04 05 06 07  08 09 10 11 12 13 14 15";
				break;
			case RowOffset:
				ss << "       00 01 02 03 04 05 06 07  00 01 01 03 04 05 06 07";
				break;
		};
		ss << std::endl;
		rule(ss);
		ss << std::endl;
	}

	static void rawbyte(std::ostream& ss, unsigned char b) {
		ss << std::setfill('0') << std::setw(2)
		   << std::hex
		   << (unsigned short)b
		   << std::dec;
	}

	static void nicebyte(std::ostream& ss, unsigned char c) {
		ss << (unsigned char)((c < 32) ? '.' : c);
	}

	static void row(std::ostream& ss, const unsigned char* buf, unsigned long len) {
		for (unsigned int n = 0; n < 16; n++) {
			if (n < len)
				rawbyte(ss, buf[n]);
			else
				ss << "  ";
			ss << " ";
			if (!((n + 1) % 8))
				ss << " ";
		}
		ss << " ";

		for (unsigned int n = 0; n < len; n++)
			nicebyte(ss, buf[n]);
		ss << std::endl;
	}
};

} // namespace KEP