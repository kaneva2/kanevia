///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <Psapi.h>
#include "Core/Util/CoreUtil.h"
#include "Core/Util/ChainedException.h"
#include "Core/Util/Unicode.h"
#include "Core/Util/OS.h"
using namespace KEP;
using namespace std;

static char* DosErrors[] = {
	"Operaton Successful", "Operation not permitted" // EPERM         1
	,
	"No such file or directory" // ENOENT        2
	,
	"No such process" // ESRCH         3
	,
	"Interrupted function" // EINTR         4
	,
	"I/O error" // EIO           5
	,
	"No such device or address" // ENXIO         6
	,
	"Argument list too long" // E2BIG         7
	,
	"Exec format error" // ENOEXEC       8
	,
	"Bad file number" // EBADF         9
	,
	"No spawned processes" // ECHILD       10
	,
	"No more processes or not enough memory or maximum nesting level reached" //  EAGAIN  11
	,
	"Not enough memory" // ENOMEM       12
	,
	"Permission denied" // EACCES       13
	,
	"Bad address" // EFAULT       14
	,
	"", "Device or resource busy" // EBUSY        16
	,
	"File exists" // EEXIST       17
	,
	"Cross-device link" // EXDEV        18
	,
	"No such device" // ENODEV       19
	,
	"Not a directory" // ENOTDIR      20
	,
	"Is a directory" // EISDIR       21
	,
	"Invalid argument" // EINVAL       22
	,
	"Too many files open in system" // ENFILE       23
	,
	"Too many open files" // EMFILE       24
	,
	"Inappropriate I/O control operation" //  ENOTTY  25
	,
	"", "File too large" // EFBIG        27
	,
	"No space left on device" // ENOSPC       28
	,
	"Invalid seek" // ESPIPE       29
	,
	"Read-only file system" // EROFS        30
	,
	"Too many links" // EMLINK       31
	,
	"Broken pipe" // EPIPE        32
	,
	"Math argument" // EDOM         33
	,
	"Result too large" // ERANGE       34
	,
	"", "Resource deadlock would occur" // EDEADLK      36
	//                        , "Same as EDEADLK for compatibility with older Microsoft C versions" // EDEADLOCK 36
	,
	"" // 37
	,
	"Filename too long" // ENAMETOOLONG 38
	,
	"No locks available" // ENOLCK 39
	,
	"Function not supported" // ENOSYS 40
	,
	"Directory not empty" // ENOTEMPTY 41
	,
	"Illegal byte sequence" // EILSEQ 42
	,
	"" // 43
	,
	"" // 44
	,
	"" // 45
	,
	"" // 46
	,
	"" // 47
	,
	"" // 48
	,
	"" // 49
	,
	"" // 50
	,
	"" // 51
	,
	"" // 52
	,
	"" // 53
	,
	"" // 54
	,
	"" // 55
	,
	"" // 56
	,
	"" // 57
	,
	"" // 58
	,
	"" // 59
	,
	"" // 60
	,
	"" // 61
	,
	"" // 62
	,
	"" // 63
	,
	"" // 64
	,
	"" // 65
	,
	"" // 66
	,
	"" // 67
	,
	"" // 68
	,
	"" // 69
	,
	"" // 70
	,
	"" // 71
	,
	"" // 72
	,
	"" // 73
	,
	"" // 74
	,
	"" // 75
	,
	"" // 76
	,
	"" // 77
	,
	"" // 78
	,
	"" // 79
	,
	"String was truncated" // STRUNCATE 80
};

std::string OS::dosMessage(unsigned long errno) {
	// todo: add range checking and throw range exception if appropriate
	return std::string(DosErrors[errno]);
}

std::string OS::getUserTempPath() {
	wchar_t buf[MAX_PATH];
	::GetTempPathW(MAX_PATH, buf);
	return Utf16ToUtf8(buf);
}

std::string OS::makeGUID() {
	std::string ret;
	char* szGUID = 0;
	GUID guid;

	UuidCreate(&guid);

	if (RPC_S_OK == UuidToStringA(&guid, (RPC_CSTR*)&szGUID)) {
		ret = std::string(szGUID);
		RpcStringFreeA((RPC_CSTR*)&szGUID);
	}
	return ret;
}

size_t OS::privateBytes() {
	PROCESS_MEMORY_COUNTERS_EX pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
	return pmc.PrivateUsage;
}

std::string OS::makeTempFileName(const std::string& extension /* = "" */
	,
	const std::string& prefix /* = "" */
	,
	const std::string& suffix /* = "" */) {
	return getUserTempPath() + prefix + makeGUID() + suffix + "." + extension;
}

std::string OS::makeTempFileMask(const std::string& extension /* = "" */
	,
	const std::string& prefix /* = "" */
	,
	const std::string& suffix /* = "" */) {
	return getUserTempPath() + prefix + "*" + suffix + "." + extension;
}

FileName OS::moduleFileName() {
	wchar_t buffer[MAX_PATH + 1];
	memset(buffer, 0, sizeof(buffer));
	if (!(::GetModuleFileNameW(NULL, buffer, MAX_PATH) > 0)) {
		stringstream ss;
		ss << "Dev Tools: error calling GetModuleFileName. GetLastError=" << GetLastError();
		throw ChainedException(SOURCELOCATION, ss.str());
	}

	return FileName::parse(Utf16ToUtf8(buffer));
}

#include "lmerr.h"

///---------------------------------------------------------
// convert an error number to a meaningful string for
// OS-type error messages
//
// [in] errNo the error number
// [out] msg the resulting message or "Error number 0x%x"
//
// [Returns]
//		msg.c_str(), for convenience
const wchar_t* OS::errorNumToString(ULONG dwLastError, wstring& msg) {
	// copied largely from MSDN to include network error codes

	HMODULE hModule = NULL; // default to system source
	wchar_t* MessageBuffer;
	DWORD dwBufferLength;

	DWORD dwFormatFlags = FORMAT_MESSAGE_ALLOCATE_BUFFER |
						  FORMAT_MESSAGE_IGNORE_INSERTS |
						  FORMAT_MESSAGE_FROM_SYSTEM;

	//
	// If dwLastError is in the network range,
	//  load the message source.
	//

	if (dwLastError >= NERR_BASE && dwLastError <= MAX_NERR) {
		hModule = LoadLibraryExW(
			L"netmsg.dll",
			NULL,
			LOAD_LIBRARY_AS_DATAFILE);

		if (hModule != NULL)
			dwFormatFlags |= FORMAT_MESSAGE_FROM_HMODULE;
	}

	//
	// Call FormatMessage() to allow for message
	//  text to be acquired from the system
	//  or from the supplied module handle.
	//
	dwBufferLength = FormatMessageW(
		dwFormatFlags,
		hModule, // module to get message from (NULL == system)
		dwLastError,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // default language
		(wchar_t*)&MessageBuffer,
		0,
		NULL);
	if (dwBufferLength) {
		if (dwBufferLength > 2 &&
			(MessageBuffer[dwBufferLength - 2] == '\r' || MessageBuffer[dwBufferLength - 2] == '\n'))
			dwBufferLength -= 2; // remove \r\n

		msg.assign(MessageBuffer, dwBufferLength);

		//
		// Free the buffer allocated by the system.
		//
		LocalFree(MessageBuffer);
	} else {
		if (HRESULT_FACILITY(dwLastError) == FACILITY_ITF) {
			// TODO: if our custom error message, load a string
		}
		wchar_t s[80];
		_snwprintf_s(s, _countof(s), _TRUNCATE, L"Error number 0x%x", dwLastError);
		msg = s;
	}

	//
	// If we loaded a message source, unload it.
	//
	if (hModule != NULL)
		FreeLibrary(hModule);

	return msg.c_str();
}

std::string OS::errorNumToString(ULONG dwLastError) {
	std::wstring wmsg;
	errorNumToString(dwLastError, wmsg);
	return Utf16ToUtf8(wmsg);
}

void OS::setThreadName(std::thread& thrd, const std::string& threadName) {
	typedef struct tagTHREADNAME_INFO {
		DWORD dwType; // must be 0x1000
		const char* szName; // pointer to name (in user addr space)
		DWORD dwThreadID; // thread ID (-1=caller thread)
		DWORD dwFlags; // reserved for future use, must be zero
	} THREADNAME_INFO;

	THREADNAME_INFO info;
	info.dwType = 0x1000;
	info.szName = threadName.c_str();
	info.dwThreadID = ::GetThreadId(thrd.native_handle());
	info.dwFlags = 0;

	__try {
		RaiseException(0x406D1388, 0, sizeof(info) / sizeof(DWORD), (DWORD*)&info);
	} __except (EXCEPTION_CONTINUE_EXECUTION) {
	}
}
