///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "Core/Util/CoreUtil.h"

#ifdef _WIN32

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD ul_reason_for_call,
	LPVOID lpReserved) {
	(void)hModule; // unused
	(void)lpReserved; // unused

	switch (ul_reason_for_call) {
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
			// Enable per-thread locale for prevent race condition in setlocale() calls issued by D3DXCreateTextureFromFileInMemoryEx
			_configthreadlocale(_ENABLE_PER_THREAD_LOCALE);
			break;

		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
	}
	return TRUE;
}

#endif

// Dummy function to force linkage to CoreUtil.dll
void forceCoreUtilLinkage() {
}
