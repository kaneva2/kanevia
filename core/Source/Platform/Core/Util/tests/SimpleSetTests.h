///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "Core/Util/SimpleSet.h"

TEST(SimpleSetTests, Test01) {
	SimpleSet<std::string> subject;
	subject.add("1.1.1.1");
	ASSERT_TRUE(subject.exists("1.1.1.1"));
	ASSERT_EQ(1, subject.size());
	subject.remove("1.1.1.1");
	ASSERT_FALSE(subject.exists("1.1.1.1"));
	ASSERT_EQ(0, subject.size());
	subject.add("1.1.1.1");
	ASSERT_TRUE(subject.exists("1.1.1.1"));
	ASSERT_EQ(1, subject.size());
	subject.clear();
	ASSERT_FALSE(subject.exists("1.1.1.1"));
	ASSERT_EQ(0, subject.size());
};
