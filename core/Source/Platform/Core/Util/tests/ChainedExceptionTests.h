#pragma once
///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Core/Util/ChainedException.h"

static void _Recurse(int level) {
	//cout << "_Recurse(" << level << ")" << endl;
	std::stringstream ss;
	if (level == 0) {
		ss << "Initial throw of a plain ChainedException! Level[" << level << "]";
		throw ChainedException(SOURCELOCATION, ss.str());
	}
	try {
		_Recurse(level - 1);
	} catch (const ChainedException& exc) {
		// cout << "catch(" << level << ")" << endl;
		ss << "Rethrow throw of a plain ChainedException! Level[" << level << "]";
		throw ChainedException(SOURCELOCATION, ss.str()).chain(exc);
	}
}

class TracedException : public ChainedException {
public:
	TracedException() :
			ChainedException(TRACETHROW) {}
	TracedException(const SourceLocation& l) :
			ChainedException(TRACETHROW, l) {}

	virtual ~TracedException() {}
};

class UntracedException : public ChainedException {
public:
	UntracedException() :
			ChainedException() {}
	UntracedException(const SourceLocation& location) :
			ChainedException(location) {}
	virtual ~UntracedException() {}
};

// Not so much a test as a development exercise.  Note that we assert nothing
TEST(CORE_ChainedExceptionTests, TracedExceptionTest01) {
	try {
		try {
			throw TracedException(SOURCELOCATION);
		} catch (const ChainedException& exc) {
			throw TracedException(SOURCELOCATION).chain(exc);
		}
	} catch (const ChainedException& exc) {
		EXPECT_TRUE(exc.str().find("at CORE_ChainedExceptionTests_TracedExceptionTest01_Test::TestBody(") != std::string::npos);
	}
}

// Not so much a test as a development exercise.  Note that we assert nothing
TEST(CORE_ChainedExceptionTests, UntracedExceptionTest01) {
	try {
		try {
			throw UntracedException(SOURCELOCATION);
		} catch (const ChainedException& exc) {
			throw UntracedException(SOURCELOCATION).chain(exc);
		}
	} catch (const ChainedException& exc) {
		EXPECT_TRUE(exc.str().find("Caused by: ChainedException:") != std::string::npos);
	}
}

TEST(CORE_ChainedExceptionTests, UntracedExceptionStringTest) {
	try {
		_Recurse(4);
	} catch (const ChainedException& exc) {
		ASSERT_TRUE(exc.str().find("ChainedException: Rethrow throw of a plain ChainedException! Level[4]") != std::string::npos);
		ASSERT_TRUE(exc.str().find("Caused by: ChainedException: Rethrow throw of a plain ChainedException! Level[3]") != std::string::npos);
		ASSERT_TRUE(exc.str().find("Caused by: ChainedException: Rethrow throw of a plain ChainedException! Level[2]") != std::string::npos);
		ASSERT_TRUE(exc.str().find("Caused by: ChainedException: Rethrow throw of a plain ChainedException! Level[1]") != std::string::npos);
		ASSERT_TRUE(exc.str().find("Caused by: ChainedException: Initial throw of a plain ChainedException! Level[0]") != std::string::npos);
	}
}

/**
 * below is an example of using the CHAINED macro.
 * First, declare the new ChainedException.
 */
CHAINED(ChainedMacroTestException);
TEST(CORE_ChainedExceptionTests, ChainedMacroTest) {
	try {
		throw ChainedMacroTestException();
	} catch (const ChainedMacroTestException&) {
	} catch (const ChainedException&) {
		FAIL() << "Exception not caught";
	} catch (...) {
		FAIL() << "Exception not caught";
	}
}

/**
 * Test the new family of ErrorSpec based ChainedExceptions and the generation macros
 */
TEST(CORE_ChainedExceptionTests, ErrorSpecTest01) {
	try {
		throw ChainedMacroTestException(ErrorSpec(1, "test message"));
	} catch (const ChainedException& ce) {
		EXPECT_STREQ("ChainedMacroTestException: test message\n:0\n", ce.str().c_str());
		EXPECT_EQ(1, ce.errorSpec().first);
		EXPECT_STREQ("test message", ce.errorSpec().second.c_str());
	}
}

TEST(CORE_ChainedExceptionTests, ErrorSpecTest02) {
	try {
		throw ChainedMacroTestException(TraceFlag(), ErrorSpec(1, "test message"));
	} catch (const ChainedException& ce) {
		EXPECT_EQ(1, ce.errorSpec().first);
		EXPECT_STREQ("test message", ce.errorSpec().second.c_str());
	}
}

TEST(CORE_ChainedExceptionTests, ErrorSpecTest03) {
	SourceLocation sl(SOURCELOCATION);
	try {
		throw ChainedMacroTestException(sl, ErrorSpec(1, "test message"));
	} catch (const ChainedException& ce) {
		//std::string s("\\source\\platform\\common\\keputil\\tests\\chainedexceptiontests.h");
		//EXPECT_TRUE( ce.str().find(s) != std::string::npos );
		EXPECT_EQ(1, ce.errorSpec().first);
		EXPECT_STREQ("test message", ce.errorSpec().second.c_str());
	}
}

TEST(CORE_ChainedExceptionTests, ErrorSpecTest04) {
	SourceLocation sl(SOURCELOCATION);
	try {
		throw ChainedMacroTestException(TraceFlag(), sl, ErrorSpec(1, "test message"));
	} catch (const ChainedException& ce) {
		string s = "at CORE_ChainedExceptionTests_ErrorSpecTest04_Test::TestBody(";
		EXPECT_TRUE(ce.str().find(s) != std::string::npos);
		EXPECT_EQ(1, ce.errorSpec().first);
		EXPECT_STREQ("test message", ce.errorSpec().second.c_str());
	}
}

// Declare a chained exception that is derived from
// the NullPointerException chained exception
//
CHAINED_DERIVATIVE(NullPointerException, TestNullPointerExceptionDerivative);
TEST(CORE_ChainedExceptionTests, DerivativeMacroTest01) {
	SourceLocation sl(SOURCELOCATION);
	try {
		throw TestNullPointerExceptionDerivative(TraceFlag(), sl, ErrorSpec(1, "test message"));
	} catch (const ChainedException& ce) {
		string expected("at CORE_ChainedExceptionTests_DerivativeMacroTest01_Test::TestBody(");
		EXPECT_TRUE(ce.str().find(expected) != std::string::npos);
		EXPECT_EQ(1, ce.errorSpec().first);
		EXPECT_STREQ("test message", ce.errorSpec().second.c_str());
	} catch (...) {
		FAIL();
	}
}

TEST(CORE_ChainedExceptionTests, DerivativeMacroTest02) {
	SourceLocation sl(SOURCELOCATION);
	try {
		throw TestNullPointerExceptionDerivative(TraceFlag(), sl, ErrorSpec(1, "test message"));
	} catch (const NullPointerException& npe) {
		string expected("at CORE_ChainedExceptionTests_DerivativeMacroTest02_Test::TestBody(");
		EXPECT_TRUE(npe.str().find(expected) != std::string::npos);
		EXPECT_EQ(1, npe.errorSpec().first);
		EXPECT_STREQ("test message", npe.errorSpec().second.c_str());
	} catch (const ChainedException ce) {
		FAIL();
	} catch (...) {
		FAIL();
	}
}

TEST(CORE_ChainedExceptionTests, DerivativeMacroTest03) {
	SourceLocation sl(SOURCELOCATION);
	try {
		throw TestNullPointerExceptionDerivative(TraceFlag(), sl, ErrorSpec(1, "test message"));
	} catch (const TestNullPointerExceptionDerivative& npe) {
		string expected("at CORE_ChainedExceptionTests_DerivativeMacroTest03_Test::TestBody(");
		EXPECT_TRUE(npe.str().find(expected) != std::string::npos);
		EXPECT_EQ(1, npe.errorSpec().first);
		EXPECT_STREQ("test message", npe.errorSpec().second.c_str());
	} catch (const NullPointerException npe) {
		FAIL();
	} catch (const ChainedException ce) {
		FAIL();
	} catch (...) {
		FAIL();
	}
}

TEST(CORE_ChainedExceptionTests, DerivativeMacroTest04) {
	SourceLocation sl(SOURCELOCATION);
	try {
		throw TestNullPointerExceptionDerivative(TraceFlag(), sl, ErrorSpec(1, "test message"));
	} catch (const NullPointerException npe) {
		string expected("at CORE_ChainedExceptionTests_DerivativeMacroTest04_Test::TestBody(");
		EXPECT_TRUE(npe.str().find(expected) != std::string::npos);
		EXPECT_EQ(1, npe.errorSpec().first);
		EXPECT_STREQ("test message", npe.errorSpec().second.c_str());
	} catch (const TestNullPointerExceptionDerivative& /*npe*/) {
		FAIL();
	} catch (const ChainedException& /*ce*/) {
		FAIL();
	} catch (...) {
		FAIL();
	}
}

TEST(CORE_ChainedExceptionTests, StreamingTest01) {
	try {
		throw ChainedException(TRACETHROW, SOURCELOCATION, ErrorSpec(1, "An Error Has Occurred"));
	} catch (ChainedException& e) {
		stringstream ss;
		ss << e;
		EXPECT_STREQ(e.str().c_str(), ss.str().c_str());
	}
}

// todo: Move to CoreTest
//
TEST(CORE_ErrorSpecTests, StreamTest01) {
	ErrorSpec spec;
	spec << string("A test!")
		 << string(" And another!");
	ASSERT_STREQ("ErrorSpec[code=0;message=\"A test! And another!\"]", spec.str().c_str());
}

TEST(CORE_ErrorSpecTests, StreamTest02) {
	ErrorSpec spec(1, "An error message");
	std::stringstream ss;
	ss << spec;
	ASSERT_STREQ("ErrorSpec[code=1;message=\"An error message\"]", ss.str().c_str());
}

/* Design notes: 
 * How to make StackTrace::capture() optional so not every exception need take on the
 * overhead.  E.g. checked/application level exceptions likely don't warrant the overhead
 * since they are likely being handled.
 *
 * option: Inheritance
 *
 *    TracedException.  On construction captures.  
 * drawback to this solution is that exceptions that may unavoidably be derived from this
 * class cannot opt out.
 * 
 * drawback dependencies introduced as artifact of using inheritance.
 *
 * option: via
 *
 * pro Allows opt in/opt out on call by call basis
 * pro no unnecessar dependencies introduced.
 * Do we need a copy constructor?  Moving a bunch of memory around in an exception context
 * doesn't sound terribly safe.
 */