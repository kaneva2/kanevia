#pragma once
///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include "Core/Util/ChainedException.h"
#include "Core/Util/ByteBufRuler.h"
#include "Common/KEPUtil/Algorithms.h"

class CORE_ByteBufRulerTests : public ::testing::Test {
public:
	CORE_ByteBufRulerTests() {
		for (unsigned int i = 0; i < 256; i++)
			_buf[i] = (unsigned char)i;
	}

	unsigned char _buf[256];

private:
};

TEST_F(CORE_ByteBufRulerTests, ExceptionTest01) {
	stringstream ass;
	ASSERT_THROW(KEP::ByteBufRuler::formatTo(ass, 0, 0), NullPointerException);
}

TEST_F(CORE_ByteBufRulerTests, Boundary01) {
	stringstream ass;

	KEP::ByteBufRuler::formatTo(ass, _buf + 32, 15);
	const string& as = ass.str();
	const char* ap = as.c_str();

	stringstream ess;
	ess << "================== buffer [" << std::showbase << hex << (unsigned long)(_buf + 32) << "] length [      15] =================" << endl
		<< "                  00                       01" << endl
		<< "       00 01 02 03 04 05 06 07  08 09 0A 0B 0C 0D 0E 0F" << endl
		<< "       -----------------------  -----------------------" << endl
		<< "0x0000|20 21 22 23 24 25 26 27  28 29 2a 2b 2c 2d 2e       !\"#$%&'()*+,-." << endl
		<< "       -----------------------  -----------------------";
	const string& es = ess.str();
	const char* ep = es.c_str();
	EXPECT_STREQ(ep, ap);
}
TEST_F(CORE_ByteBufRulerTests, BoundaryTest02) {
	stringstream ass;

	KEP::ByteBufRuler::formatTo(ass, _buf + 32, 64);
	const string& as = ass.str();
	const char* ap = as.c_str();

	stringstream ess;
	ess << "================== buffer [" << std::showbase << hex << (unsigned long)(_buf + 32) << "] length [      64] =================" << endl
		<< "                  00                       01" << endl
		<< "       00 01 02 03 04 05 06 07  08 09 0A 0B 0C 0D 0E 0F" << endl
		<< "       -----------------------  -----------------------" << endl
		<< "0x0000|20 21 22 23 24 25 26 27  28 29 2a 2b 2c 2d 2e 2f    !\"#$%&'()*+,-./" << endl
		<< "0x0010|30 31 32 33 34 35 36 37  38 39 3a 3b 3c 3d 3e 3f   0123456789:;<=>?" << endl
		<< "0x0020|40 41 42 43 44 45 46 47  48 49 4a 4b 4c 4d 4e 4f   @ABCDEFGHIJKLMNO" << endl
		<< "0x0030|50 51 52 53 54 55 56 57  58 59 5a 5b 5c 5d 5e 5f   PQRSTUVWXYZ[\\]^_" << endl
		<< "       -----------------------  -----------------------";
	const string& es = ess.str();
	const char* ep = es.c_str();

	EXPECT_STREQ(ep, ap);
}

// Test boundary conditions
//
TEST_F(CORE_ByteBufRulerTests, BoundaryTest03) {
	stringstream ass;

	KEP::ByteBufRuler::formatTo(ass, _buf + 32, 17);
	const string& as = ass.str();
	const char* ap = as.c_str();

	stringstream ess;
	ess << "================== buffer [" << std::showbase << hex << (unsigned long)(_buf + 32) << "] length [      17] =================" << endl
		<< "                  00                       01" << endl
		<< "       00 01 02 03 04 05 06 07  08 09 0A 0B 0C 0D 0E 0F" << endl
		<< "       -----------------------  -----------------------" << endl
		<< "0x0000|20 21 22 23 24 25 26 27  28 29 2a 2b 2c 2d 2e 2f    !\"#$%&'()*+,-./" << endl
		<< "0x0010|30                                                 0" << endl
		<< "       -----------------------  -----------------------";
	const string& es = ess.str();
	const char* ep = es.c_str();

	EXPECT_STREQ(ep, ap);
}

// Test boundary conditions
//
TEST_F(CORE_ByteBufRulerTests, BoundaryTest04) {
	stringstream ass;

	KEP::ByteBufRuler::formatTo(ass, _buf + 32, 1);
	const string& as = ass.str();
	const char* ap = as.c_str();

	stringstream ess;
	ess << "================== buffer [" << std::showbase << hex << (unsigned long)(_buf + 32) << "] length [       1] =================" << endl
		<< "                  00                       01" << endl
		<< "       00 01 02 03 04 05 06 07  08 09 0A 0B 0C 0D 0E 0F" << endl
		<< "       -----------------------  -----------------------" << endl
		<< "0x0000|20                                                  " << endl
		<< "       -----------------------  -----------------------";
	const string& es = ess.str();
	const char* ep = es.c_str();

	EXPECT_STREQ(ep, ap);
}

TEST_F(CORE_ByteBufRulerTests, BoundaryTest06) {
	stringstream ass;

	KEP::ByteBufRuler::formatTo(ass, _buf + 32, 32);
	const string& as = ass.str();
	const char* ap = as.c_str();

	stringstream ess;
	ess << "================== buffer [" << std::showbase << hex << (unsigned long)(_buf + 32) << "] length [      32] =================" << endl
		<< "                  00                       01" << endl
		<< "       00 01 02 03 04 05 06 07  08 09 0A 0B 0C 0D 0E 0F" << endl
		<< "       -----------------------  -----------------------" << endl
		<< "0x0000|20 21 22 23 24 25 26 27  28 29 2a 2b 2c 2d 2e 2f    !\"#$%&'()*+,-./" << endl
		<< "0x0010|30 31 32 33 34 35 36 37  38 39 3a 3b 3c 3d 3e 3f   0123456789:;<=>?" << endl
		<< "       -----------------------  -----------------------";
	const string& es = ess.str();
	const char* ep = es.c_str();

	EXPECT_STREQ(ep, ap);
}
