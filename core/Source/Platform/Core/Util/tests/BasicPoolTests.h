///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <Core/Util/Pool.h>
#include <Core/Util/ThreadPool.h>
#include "Common/KEPUtil/CountDownLatch.h"

class Fish {
public:
	Fish(unsigned long& created_counter) :
			_created_counter(created_counter) {
		_created_counter++;
	}

	~Fish() {
		_created_counter--;
	}
	unsigned long& _created_counter;
};

class Fish_factory : public BasicFactory<Fish> {
public:
	virtual Fish* create() {
		return new Fish(_created_counter);
	}

	Fish_factory(unsigned long& created_counter) :
			_created_counter(created_counter) {}

	unsigned long& _created_counter;
};

TEST(BasicPoolTests, Test01) {
	unsigned long created_counter = 0;
	{
		BasicPool<Fish> fishPool(std::make_shared<Fish_factory>(created_counter));
		{
			BasicPool<Fish>::borrowed_t fish = fishPool.borrow();
			ASSERT_EQ(1, fishPool.size());
			ASSERT_EQ(0, fishPool.available());
		}

		ASSERT_EQ(1, fishPool.size());
		ASSERT_EQ(1, fishPool.available());

		BasicPool<Fish>::borrowed_t fish = fishPool.borrow();
		ASSERT_EQ(1, fishPool.size());
		ASSERT_EQ(0, fishPool.available());
	}
	ASSERT_EQ(0, created_counter);
}

TEST(BasicPoolTests, test02) {
	unsigned long created_counter = 0;
	{
		BasicPool<Fish>::borrowed_t c1;
		{
			BasicPool<Fish> c(shared_ptr<Fish_factory>(new Fish_factory(created_counter)));
			c1 = c.borrow();
			ASSERT_EQ(1, c.size());
			ASSERT_EQ(0, c.available());
			ASSERT_EQ(1, created_counter);

			BasicPool<Fish>::borrowed_t c2 = c.borrow();
			ASSERT_EQ(2, c.size());
			ASSERT_EQ(0, c.available());
			ASSERT_EQ(2, created_counter);

			// pool rolls off the stack and cleans up.
			// leaving our existing resource intact, to be cleaned up.
			// This should create issues as the Deleter associated with
			// the shared_ptr is now non-existent pool
			// Seems like this could create a problem since the
			// borrowed version still references this pool.
		}

		ASSERT_TRUE(c1.get() != 0);
		ASSERT_EQ(1, c1.use_count());

		// And the only remaining reference to an object
		// formerly owned by the pool will roll off the
		// stack causing the last allocate Fish to to get cleaned up.
	}
	ASSERT_EQ(0, created_counter);
}

TEST(BasicPoolTests, Test03) {
	int threads = 5; // Test parameters
	int poolsize = 1;
	int iterations = 10;

	unsigned long created_counter = 0; // passed by references to instances

	ConcreteThreadPool threadpool(threads);
	threadpool.start()->wait();
	BasicPool<Fish> pool(shared_ptr<Fish_factory>(new Fish_factory(created_counter)), poolsize);
	for (int iteration = 0; iteration < iterations; iteration++) {
		threadpool.enqueue([=, &pool]() -> RunResultPtr {
			int cnt = iteration * 1000;
			for (int lcv1 = 0; lcv1 < cnt; lcv1++) {
				shared_ptr<Fish> Fish = pool.borrow();
				//	cout << ".";
			}
			return RunResultPtr();
		});
	}

	// Initiate and wait for thread pool shutdown complete
	//	cout << ".";
	threadpool.shutdown(ThreadPool::patient)->wait();
	//	cout << ".";
}

TEST(BasicPoolTests, DISCO_Test04) {
	// We'll use a thread pool to perform operations
	// on the pool from multiple threads.
	//
	unsigned long created_counter = 0;
	{
		int iterations = 100000;
		int threads = 20;
		int poolsize = 1;
		int taskduration = 0;

		// for this test a pool of 1.
		// a thread pool of two to cause the underflow
		//
		ConcreteThreadPool threadpool(threads);
		BasicPool<Fish> pool(shared_ptr<Fish_factory>(new Fish_factory(created_counter)), poolsize);
		int c = 0;

		CountDownLatch latch(iterations);
		threadpool.start()->wait();

		for (int iteration = 0; iteration < iterations; iteration++) {
			c++;
			if (iteration < poolsize)
				ASSERT_EQ(0, pool.backlog());
			threadpool.enqueue([=, &pool, &latch]() -> RunResultPtr {
				shared_ptr<Fish> Fish = pool.borrow();
				::Sleep(taskduration);
				latch.countDown();
				return RunResultPtr();
			});
		}

		latch.await();
		::Sleep(1000);
		ASSERT_EQ(0, pool.backlog());
		ASSERT_EQ(poolsize, pool.available());
		// Initiate and wait for thread pool shutdown complete
		//cout << "closing the pool" << endl;
		threadpool.shutdown(ThreadPool::patient)->wait();
	}
}

// Can I write a test to verify that we can't end up in a state where we have a candidate
// in the available collection, AND a promise in the backlog?  This could cause starvation
// including never acquiring the resource?
//
TEST(BasicPoolTests, Test05) {
	int threads = 6; // Test parameters
	int poolsize = 3;
	int iterations = 100000;

	std::atomic<unsigned long> borrow_counter = 0;
	unsigned long created_counter = 0; // passed by references to instances
	{
		ConcreteThreadPool threadpool(threads);
		threadpool.start()->wait();
		BasicPool<Fish> pool(shared_ptr<Fish_factory>(new Fish_factory(created_counter)), poolsize);
		CountDownLatch iterationlatch(iterations);
		for (int iteration = 0; iteration < iterations; iteration++) {
			threadpool.enqueue([=, &borrow_counter, &pool, &iterationlatch]() -> RunResultPtr {
				shared_ptr<Fish> Fish = pool.borrow();
				borrow_counter++;
				iterationlatch.countDown();
				return nullptr;
			});
		}

		iterationlatch.await();
		ASSERT_EQ(poolsize, pool.size()); // the size (allocated) should be maxed out.
		ASSERT_EQ(0, pool.backlog()); // backlog should be cleared
		ASSERT_EQ(poolsize, pool.available()); // and all instances should be available for loan
		threadpool.shutdown(ThreadPool::patient)->wait(); // Initiate and wait for thread pool shutdown complete
	}
	EXPECT_EQ(0, created_counter); // balance of allocated objects should be zero.
	EXPECT_EQ(iterations, borrow_counter); // We should have borrowed exactly one connection
		// for every iteration.  Hard to understand
		// this might fail.
}

TEST(BasicPoolTests, DISCO_ThreadStarvationTest01) {
	// This test set up such that loan order should match the request order
	// thus proving that the anti-starvation logic is working.
	//
	//
	int outer_timeout = 20; // job will be queued every 20ms
	int inner_timeout = 80; // each job will linger for 60ms
	int iterations = 2000; // The number jobs that will be submitted
	int poolsize = 5;
	int threads = 10;

	std::atomic<int> last(0);

	std::atomic<unsigned long> borrow_counter(0);
	unsigned long created_counter = 0; // passed by references to instances
	{
		ConcreteThreadPool threadpool(threads);
		threadpool.start()->wait();
		BasicPool<Fish> pool(shared_ptr<Fish_factory>(new Fish_factory(created_counter)), poolsize);

		unsigned long nsigma = 0;
		std::atomic<unsigned long> asigma(0);

		CountDownLatch iterationlatch(iterations);
		for (unsigned long iteration = 0; iteration < iterations;) {
			for (unsigned long n = 0; n < threads && iteration < iterations; n++, iteration++) {
				nsigma += iteration;
				threadpool.enqueue([=, &asigma, &last, &borrow_counter, &pool, &iterationlatch]() -> RunResultPtr {
					int diff = iteration - last.fetch_add(1);
					EXPECT_LE(abs(diff), poolsize);
					{
						shared_ptr<Fish> Fish = pool.borrow();
						asigma += iteration;
						borrow_counter++;
						::Sleep(inner_timeout);
					}
					iterationlatch.countDown();
					return RunResultPtr();
				});
			}
			::Sleep(outer_timeout);
		}

		iterationlatch.await();
		::Sleep(1000);
		EXPECT_EQ(nsigma, asigma.load());
		EXPECT_EQ(iterations, borrow_counter);
		EXPECT_EQ(iterations, pool.borrowed());
		EXPECT_TRUE(pool.maxbacklog() > 0);
		EXPECT_EQ(poolsize, pool.size()); // the size (allocated) should be maxed out.
		EXPECT_EQ(0, pool.backlog()); // backlog should be cleared
		EXPECT_EQ(poolsize, pool.available()); // and all instances should be available for loan

		// Initiate and wait for thread pool shutdown complete
		threadpool.shutdown(ThreadPool::patient)->wait();
	}
	//	EXPECT_EQ(0, created_counter);
	//	EXPECT_EQ(iterations, borrow_counter);
}

TEST(BasicPoolTests, DISABLED_Test06) {
	int threads = 6; // Test parameters
	int poolsize = 5;
	int iterations = 1000;

	unsigned long borrow_counter = 0;
	unsigned long created_counter = 0; // passed by references to instances
	{
		ConcreteThreadPool threadpool(threads);
		threadpool.start()->wait();
		BasicPool<Fish> pool(shared_ptr<Fish_factory>(new Fish_factory(created_counter)), poolsize);
		CountDownLatch iterationlatch(iterations);
		for (int iteration = 0; iteration < iterations; iteration++) {
			threadpool.enqueue([=, &borrow_counter, &pool, &iterationlatch]() -> RunResultPtr {
				shared_ptr<Fish> Fish = pool.borrow();
				::Sleep(iteration);
				iterationlatch.countDown();
				borrow_counter++;
				return RunResultPtr();
			});
			::Sleep(iterations - iteration);
		}

		iterationlatch.await();

		// Initiate and wait for thread pool shutdown complete
		// got it to lock up here.  all threads are closed,
		// but the term count is short.  Looks there is a race
		// condition in the shutdown logic.
		// e.g. listening for executor shutdown after it has been stopped
		//
		cout << ".";
		threadpool.shutdown(ThreadPool::patient)->wait();
	}
	EXPECT_EQ(0, created_counter);
	EXPECT_EQ(iterations, borrow_counter);
}
