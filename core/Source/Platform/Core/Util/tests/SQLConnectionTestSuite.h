///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "Core/Util/SQLConnection.h"

/**
* Simple test suite for the "SQLConnection" core component.
*
* Tests assume that MySQL is installed on localhost, with the following
* requirements:
* -- "test" schema
* -- "test.test_table" table with an auto-incrementing primary key and varchar "test_col" field
* -- user with name "localadmin" and password "localadmin1234"
*/
class SQLConnectionTests : public ::testing::Test {
public:
	SQLConnectionTests() {
		host = "localhost";
		port = 3306;
		user = "testuser";
		password = "testpass";
		db = "test";
	}

	virtual void SetUp() {
		conn.connect(db.c_str(), host.c_str(), user.c_str(), password.c_str(), port);
		executor = conn.getSQLExecutor();
	}

	std::string host;
	unsigned short port;
	std::string user;
	std::string password;
	std::string db;
	SQLConnection conn;
	std::shared_ptr<SQLExecutor> executor;
};

TEST_F(SQLConnectionTests, TestCreateFromFactory) {
	SQLConnectionFactory* factory = new SQLConnectionFactory(host.c_str(), port, user.c_str(), password.c_str(), db.c_str());
	SQLConnection* localConn = factory->create();
	EXPECT_EQ(true, localConn->connected());
}

TEST_F(SQLConnectionTests, TestCreateFromPool) {
	SQLConnectionFactory* factory = new SQLConnectionFactory(host.c_str(), port, user.c_str(), password.c_str(), db.c_str());
	std::shared_ptr<SQLConnectionFactory> pf(factory);
	SQLConnectionPool* pool = new SQLConnectionPool(pf, 10);
	std::shared_ptr<SQLConnection> localConn = pool->borrow();
	EXPECT_EQ(true, localConn->connected());
}

TEST_F(SQLConnectionTests, TestCRUDSelect) {
	mysqlpp::StoreQueryResult res = executor->execSelectNoThrow("SELECT * FROM test_table LIMIT 1; ");

	EXPECT_EQ(true, res.num_rows() > 0);
}

TEST_F(SQLConnectionTests, TestCRUDInsert) {
	bool success = executor->execUpdateNoThrow("INSERT INTO test_table (test_col) VALUES ('%0');", "myval");

	EXPECT_EQ(true, success);
}

TEST_F(SQLConnectionTests, TestStoredProcSingleResult) {
	mysqlpp::StoreQueryResult res = executor->execProcNoThrow("CALL test_proc_1(%0);", 0, 1);

	EXPECT_EQ(true, res.num_rows() > 0);
}

TEST_F(SQLConnectionTests, TestStoredProcMultipleResult) {
	mysqlpp::StoreQueryResult res = executor->execProcThrow("CALL test_proc_2(%0);", 1, 1);

	EXPECT_EQ(true, res.num_rows() > 0);
}

TEST_F(SQLConnectionTests, TestStoredProcDiscardResult) {
	executor->execProcThrow("CALL test_proc_2(%0);", 0, 1);

	EXPECT_EQ(true, conn.connected());
}

TEST_F(SQLConnectionTests, TestInvalidQueryError) {
	std::string errMsg = "NO ERROR";
	try {
		executor->execProcThrow("CALL bad_query(%0);", 0, 1);
	} catch (ChainedException e) {
		errMsg = e.errorSpec().message();
	}

	EXPECT_NE("NO ERROR", errMsg);
}