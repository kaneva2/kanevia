#pragma once
///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Core/Util/StackTrace.h"

TEST(CORE_StackTraceTests, Test01) {
	StackTrace st1;
	StackTrace st2;

	// Okey, everything but the this location should be the same betwee the two stacks
	//
	ASSERT_EQ(st1.frames().size(), st2.frames().size());

	int nframes = st1.frames().size();
	StackTrace::FrameVector::const_iterator i1 = st1.frames().cbegin();
	StackTrace::FrameVector::const_iterator i2 = st1.frames().cbegin();
	for (int nframe = 1; nframe < nframes; nframe++) {
		ASSERT_STREQ(i1->str().c_str(), i2->str().c_str());
		i1++;
		i2++;
	}
}
