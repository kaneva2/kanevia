///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/ObjectRegistry.h"

class DecrementingDestructor {
public:
	int& _n;
	DecrementingDestructor(int& n) :
			_n(n) {}
	~DecrementingDestructor() { _n--; }
};

/**
 * Verifies behavior of ObjectRegistry::destroyObject()
 */
TEST(ObjectRegistryTestSuite, ObjectDestructorTest) {
	LOG4CPLUS_INFO(Logger::getInstance(L"Testing"), "************ ObjectRegistryTestSuite, ObjectDestructorTest");
	int n = 1; // If the destroy operation is successful,
	DecrementingDestructor* p = new DecrementingDestructor(n); // this DecrementingDestructor will decrement
		// n to 0

	KEP::ObjectRegistry or ; // Place an ObjectRegistry on the stack
	or.registerObject("test" // Register our decrementing destructor
		  ,
		  p, new KEP::BasicDestructor<DecrementingDestructor>());

	or.destroyObject("test"); // Remove it from the list and destroy it.
	EXPECT_EQ(0, n);
}

/**
 * ObjectRegistry will attempt to clean up resources when it is destroyed.
 * This test verifies that behaviour.
 */
TEST(ObjectRegistryTestSuite, ObjectRegistryDestructionTest) {
	int n = 1; // If the destroy operation is successful,
	DecrementingDestructor* p = new DecrementingDestructor(n); // this DecrementingDestructor will decrement
		// n to 0

	KEP::ObjectRegistry* or = new KEP::ObjectRegistry(); // Place an ObjectRegistry on the heap
	or->registerObject("test" // Register our decrementing destructor
		  ,
		  p, new KEP::BasicDestructor<DecrementingDestructor>());
	delete or ;
	EXPECT_EQ(0, n);
}

/**
 * When an object is explicitly removed from the registry no action is taken to 
 * clean up memory or resources associated with that object.  This test verifies
 * than an object removed from the registry remains intact.
 */
TEST(ObjectRegistryTestSuite, ObjectRegistryRemovalTest) {
	int n = 1; // If the destroy operation is successful,
	DecrementingDestructor* p = new DecrementingDestructor(n); // this DecrementingDestructor will decrement
		// n to 0

	KEP::ObjectRegistry or ; // Place an ObjectRegistry on the stack
	or.registerObject("test" // Register our decrementing destructor
		  ,
		  p, new KEP::BasicDestructor<DecrementingDestructor>());
	or.removeObject("test");

	EXPECT_EQ(1, n);

	delete p;
}
