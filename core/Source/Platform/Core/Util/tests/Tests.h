///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/Tests/BasicPoolTests.h"
#include "Core/Util/tests/StackTraceTests.h"
#include "Core/Util/tests/ByteBufRulerTests.h"
#include "Core/Util/tests/ChainedExceptionTests.h"
#include "Core/Util/tests/ObjectRegistryTestSuite.h"
