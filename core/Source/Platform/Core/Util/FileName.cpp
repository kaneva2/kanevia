///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "Core/Util/CoreUtil.h"
#include "Core/Util/ChainedException.h"
#include "Core/Util/FileName.h"
#include "Core/Util/OS.h"
#include "Core/Util/StringHelpers.h"

using namespace std;

namespace KEP {

/**
	* Default constructor
	*/
FileName::FileName() {}

FileName::FileName(const std::string& subject) {
	*this = parse(subject);
}

FileName::FileName(const char* fileName) {
	// Parse and invoke assignment operator
	*this = parse(fileName);
}

FileName::FileName(const FileName& fileName) :
		_drive(fileName._drive)
		//		, _dir( fileName._dir )
		,
		_pathComponents(fileName._pathComponents),
		_fname(fileName._fname),
		_ext(fileName._ext) {}

FileName::~FileName() {}

FileName& FileName::operator=(const FileName& fileName) {
	_drive = fileName._drive;
	//	_dir	= fileName._dir;
	_pathComponents = fileName._pathComponents;
	_fname = fileName._fname;
	_ext = fileName._ext;
	return *this;
}

void FileName::streamOut(std::stringstream& ss) const {
	std::ofstream os;
	std::string fname = toString();
	try {
		os.exceptions(std::ofstream::failbit | std::ofstream::badbit);
		os.open(fname, ios_base::trunc | ios_base::out);
		os << ss.rdbuf(); // _config->deflated();
		os.close();
	} catch (ofstream::failure e) {
		stringstream ssError;
		ssError << "Failed to open file (truncate|output) [" << fname << "][" << e.code() << "][" << e.what() << "]";
		throw ChainedException(SOURCELOCATION, ssError.str());
	}
}

//std::string toString( unsigned short makeup = Drive | Path | Name | Ext ) const {

std::string FileName::toString(unsigned short makeup) const {
	stringstream ss;
	if ((makeup & Drive) > 0)
		ss << _drive;
	//if ( (makeup & Path)  > 0 ) ss << _dir;

	if ((makeup & Path) > 0 && (_pathComponents.size() > 0))
		ss << "\\" << StringHelpers::implode(_pathComponents, "\\") << "\\";

	if ((makeup & Name) > 0)
		ss << _fname;

	if ((makeup & Ext) > 0)
		ss << extWithDelim();
	std::string ret = ss.str();
	return ss.str();
}

std::string FileName::str(unsigned short makeup) const {
	return toString(makeup);
}

const std::string& FileName::drive() const {
	return _drive;
}

FileName& FileName::setDrive(const std::string& drive) {
	_drive = drive;
	return *this;
}

std::string FileName::dir() const {
	std::stringstream ss;
	ss << '\\' << StringHelpers::implode(_pathComponents, "\\") << '\\';
	return ss.str();
}

FileName& FileName::setDir(const std::string& dir) {
	if (!StringHelpers::startsWith("\\\\", dir)) {
		StringHelpers::explode(_pathComponents, StringHelpers::trim(dir, "\t\r\n\\ "), "\\");
		return *this;
	}

	std::vector<std::string> temp;
	StringHelpers::explode(temp, StringHelpers::trim(dir, "\t\r\n\\ "), "\\");
	_drive = "\\\\" + temp[0];
	for (std::vector<std::string>::const_iterator istart = (temp.cbegin() + 1); istart != temp.cend(); istart++) {
		_pathComponents.push_back(*istart);
	}

	return *this;
}

FileName& FileName::appendDir(const std::string& dir) {
	std::vector<std::string> pathComponents;
	StringHelpers::explode(pathComponents, StringHelpers::trim(dir, "\t\r\n\\ "), "\\");
	for (std::vector<std::string>::const_iterator i = pathComponents.cbegin(); i != pathComponents.cend(); i++) {
		_pathComponents.push_back(*i);
	}
	return *this;
}

const std::string& FileName::fname() const {
	return _fname;
}

std::string FileName::toNameExt() const {
	return toString(Name | Ext);
}

FileName& FileName::setFName(const std::string& fname) {
	_fname = fname;
	return *this;
}

const std::string& FileName::ext() const {
	return _ext;
}

std::string FileName::extWithDelim() const {
	if (_ext.empty())
		return _ext;
	return "." + _ext;
}

FileName& FileName::setExt(const std::string& ext) {
	if (ext[0] == '.')
		_ext = ext.c_str() + 1;
	else
		_ext = ext;
	return *this;
}

FileName FileName::getCWD() {
	char baseDir[_MAX_PATH + 1];
	baseDir[0] = '\0';
	_getcwd(baseDir, _MAX_PATH);
	stringstream ss;
	ss << baseDir << "\\";
	return parse(ss.str());
}

FileName FileName::parse(const std::string& fileString) {
	if (fileString == ".") {
		FileName ret;
		return ret.setDir(fileString);
	}

	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];

	_splitpath_s(fileString.c_str(), drive, _countof(drive), dir, _countof(dir), fname, _countof(fname), ext, _countof(ext));

	return FileName().setDrive(drive).setDir(dir).setFName(fname).setExt(ext);
}

FileName FileName::createTemp(const std::string& extension, const std::string& prefix, const std::string& suffix) {
	std::string fname = OS::makeTempFileName(extension, prefix, suffix);
	return FileName::parse(fname);
}

std::string FileName::tempMask(const std::string& extension, const std::string& prefix, const std::string& suffix) {
	return OS::makeTempFileMask(extension, prefix, suffix);
}

void FileName::GetShortNames(const std::string& pathSource, std::string& pathDest, const char* subDirSource, std::string* subDirDest) {
	size_t i = pathSource.find_last_of('\\'); // Initialize index var with position of the last path-delim

	if (i == std::string::npos) // If NOT found
		i = pathSource.find_last_of('/'); //		Perhaps it's non-windows. look for last forward slash

	if (i != std::string::npos) { // If now found
		i++; //      Increment the index var
		(pathDest) = pathSource.substr(i, pathSource.size() - i); //
	} else {
		(pathDest) = (pathSource);
	}

	if (!subDirDest || !subDirSource)
		return;

	std::string subDir(subDirSource);

	i = subDir.find_last_of('\\');

	if (i == std::string::npos)
		i = subDir.find_last_of('/');

	std::string finalSubString;

	if (i != std::string::npos) {
		i++;
		finalSubString = subDir.substr(i, subDir.size() - i);

		i = finalSubString.find_last_of('.');

		std::string ext = finalSubString.substr(i + 1, finalSubString.size() - 1);

		(*subDirDest) = finalSubString.substr(0, i);

		//	If the file has an RLB extension, then we should add the RLB tag to the directory to search through
		if (ext == "RLB")
			(*subDirDest) += ext;

		//	Add final slash
		(*subDirDest) += '\\';
	} else {
		(*subDirDest) = subDirSource;
		(*subDirDest) += '\\';
	}

	*subDirDest = StringHelpers::findandreplace(*subDirDest, std::string("\\"), std::string("---"));
}

} // namespace KEP
