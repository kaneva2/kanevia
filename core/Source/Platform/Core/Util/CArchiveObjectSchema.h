///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <type_traits>

#define DECLARE_SERIAL_SCHEMA(class_name, schema)                \
	DECLARE_SERIAL(class_name)                                   \
public:                                                          \
	static constexpr unsigned m_CArchiveObjectSchema = (schema); \
                                                                 \
protected:

#define IMPLEMENT_SERIAL_SCHEMA(class_name, base_class_name) \
	IMPLEMENT_SERIAL(class_name, base_class_name, CArchiveObjectSchema_v<class_name>)

namespace KEP {

template <typename T, typename enable = void>
struct CArchiveObjectSchema {
	static constexpr unsigned value = 0;
};

template <typename T>
struct CArchiveObjectSchema<T, std::enable_if_t<T::m_CArchiveObjectSchema >= 0>> {
	static constexpr unsigned value = T::m_CArchiveObjectSchema;
};

template <typename T>
constexpr unsigned CArchiveObjectSchema_v = CArchiveObjectSchema<T>::value;

template <typename T>
constexpr unsigned CArchiveObjectSchemaVersion = CArchiveObjectSchema<T>::value & ~VERSIONABLE_SCHEMA;

} // namespace KEP
