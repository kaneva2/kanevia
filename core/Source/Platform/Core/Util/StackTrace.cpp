///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

// Warning in Windows SDK header when building with VS2015
#pragma warning(push)
#pragma warning(disable : 4091) // 'typedef ': ignored on left of '' when no variable is declared
#include "dbghelp.h"
#pragma warning(pop)

#include "Include/Core/Util/StackTrace.h"
#include <iostream>
#include <type_traits>

//#include "Common\include\LogHelper.h"
//static LogInstance("Instance");

const unsigned long InfoSize = sizeof(SYMBOL_INFO) + 256 * sizeof(std::remove_extent_t<decltype(SYMBOL_INFO::Name)>); // determinant

// StackFrame implementation
std::string StackFrame::symbolName() const {
	return ((SYMBOL_INFO*)_symbol)->Name;
}

// SourceLocation implementation
SourceLocation::SourceLocation() :
		_file(), _line(0) {}
SourceLocation::SourceLocation(const char* file, unsigned long line, const char* fn) :
		_file(file), _line(line), _function(fn ? fn : "") {}
SourceLocation::SourceLocation(const SourceLocation& other) :
		_file(other._file), _line(other._line), _function(other._function) {}
SourceLocation::~SourceLocation() {}

const std::string& SourceLocation::file() const {
	return _file;
}
SourceLocation& SourceLocation::setFile(char* fileName) {
	_file = fileName;
	return *this;
}

unsigned long SourceLocation::line() const {
	return _line;
}
SourceLocation& SourceLocation::setLine(unsigned long line) {
	_line = line;
	return *this;
}

const std::string& SourceLocation::fn() const {
	return _function;
};
SourceLocation& SourceLocation::setFn(char* fnName) {
	_function = fnName;
	return *this;
}

SourceLocation& SourceLocation::operator=(const SourceLocation& rhs) {
	_line = rhs._line;
	_file = rhs._file;
	_function = rhs._function;
	return *this;
}

// StackTrace implementation
//
StackTrace::StackTrace() :
		_internal(new InternalState()) {}
StackTrace::StackTrace(const StackTrace& other) :
		_internal(other._internal) {}
StackTrace::~StackTrace() {}

StackTrace& StackTrace::operator=(const StackTrace& rhs) {
	_internal = rhs._internal;
	return *this;
}
bool StackTrace::operator==(const StackTrace& rhs) {
	if (_internal.operator->() == 0)
		return rhs._internal.operator->() == 0;
	return _internal->_hash == rhs._internal->_hash;
}

/**
 * Output the stack to console.  Intended for testing/debugging purposes.
 *  
 */
void StackTrace::printStack(void) const {
	if (_internal.operator->() == 0)
		return;
	if (_internal->_frameVec.size() == 0)
		return;
	for (auto iter = _internal->_frameVec.begin(); iter != _internal->_frameVec.end(); iter++) {
		printf("    at %s(%s:%d)\n", iter->symbolName().c_str(), iter->file().c_str(), iter->line());
	}
}

/**
 * Output a formatted stack to the supplied ostream.
 */
std::ostream& StackTrace::streamOut(std::ostream& os) const {
	if (_internal.operator->() == 0)
		return os;
	if (_internal->_frameVec.size() == 0)
		return os;
	for (auto iter = _internal->_frameVec.begin(); iter != _internal->_frameVec.end(); iter++) {
		os << "    at " << iter->symbolName() << "(" << iter->file() << ":" << iter->line() << ")" << std::endl;
	}
	return os;
}

COREUTIL_API std::ostream& operator<<(std::ostream& o, const StackTrace& st) {
	return st.streamOut(o);
}

unsigned long StackTrace::hashCode() const {
	return _internal->_hash;
}

// Internal State implementation
StackTrace::InternalState::InternalState() :
		_frameVec(0), _hash(0), _symbol((char*)0)

{
	captureStack();
}

StackTrace::InternalState::~InternalState() {
	if (_symbol > 0)
		free(_symbol);
}

bool StackTrace::InternalState::captureStack(ULONG frameStart, ULONG frames) {
	HANDLE process = GetCurrentProcess();

	void* stack[100] = { 0 };
	if (frames > 100)
		return false;

	// todo: resolve initialization issues by testing for debug
	// and assuming that vld is active.  Use SymRefreshProcesses
	// Would be nice if vld.h defined some flag to determine that
	// vld is in play.
	//
	SymInitialize(process, NULL, TRUE);

	// Capture Stack Frames Using Win32 API
	USHORT framesCaptured = CaptureStackBackTrace(frameStart, frames, stack, &_hash);
	if (!framesCaptured)
		return false;

	_symbol = (char*)calloc(framesCaptured * InfoSize, 1);

	for (USHORT i = 0; i < framesCaptured; i++) {
		SYMBOL_INFO* symbol = (SYMBOL_INFO*)(_symbol + (i * InfoSize));
		symbol->MaxNameLen = 255;
		symbol->SizeOfStruct = sizeof(SYMBOL_INFO);
		SymFromAddr(process, (DWORD64)(stack[i]), 0, symbol);

		// Attempt to retrieve line number information.
		DWORD line_displacement = 0;
		IMAGEHLP_LINE line = {};
		line.SizeOfStruct = sizeof(IMAGEHLP_LINE);
		SourceLocation sl;
		if (SymGetLineFromAddr(GetCurrentProcess(), (DWORD)stack[i], &line_displacement, &line))
			sl.setFile(line.FileName).setLine(line.LineNumber);

		StackFrame stackFrame((char*)symbol, sl);
		_frameVec.push_back(stackFrame);
	}

	//	SymCleanup( process );
	return true;
}
