///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <io.h>
#include <direct.h>
#include <sstream>
#include <log4cplus/logger.h>
#include <Shlwapi.h>
#include "Core/Util/ObjectRegistry.h"

using namespace log4cplus;

namespace KEP {

class ObjectHolder {
public:
	void* _p;
	Destructor* _destructor;
	ObjectHolder(void* p, Destructor* destructor = 0) :
			_p(p), _destructor(destructor) {}

	virtual ~ObjectHolder() {
		LOG4CPLUS_TRACE(Logger::getInstance(L"ObjectRegistry"), "ObjectHolder::~ObjectHolder()");
		if (_p == 0) {
			if (_destructor == 0) {
				LOG4CPLUS_DEBUG(Logger::getInstance(L"ObjectRegistry"), "ObjectHolder::~ObjectHolder() No content! No Destructor!");
			} else {
				LOG4CPLUS_DEBUG(Logger::getInstance(L"ObjectRegistry"), "ObjectHolder::~ObjectHolder() No content!");
			}
		} else {
			if (_destructor == 0) {
				LOG4CPLUS_DEBUG(Logger::getInstance(L"ObjectRegistry"), "ObjectHolder::~ObjectHolder() Blind delete()");
				delete _p;
				// DRF - If this causes a crash in the future make sure your class inheritance begins with the type of the pointer
				// you are passing to the register function.  Otherwise just delete this delete and don't worry about it.
			} else {
				LOG4CPLUS_DEBUG(Logger::getInstance(L"ObjectRegistry"), "ObjectHolder::~ObjectHolder() Destructor::destroy()");
				_destructor->destroy(_p);
			}
			_p = 0;
		}

		if (_destructor == 0)
			return;
		delete _destructor;
		_destructor = 0;
	}
};

ObjectRegistry* ObjectRegistry::_instance = 0;
fast_recursive_mutex ObjectRegistry::_sync;

class ObjectRegistryCleaner {
public:
	virtual ~ObjectRegistryCleaner() {
		LOG4CPLUS_TRACE(Logger::getInstance(L"ObjectRegistry"), "ObjectRegistryCleaner::~ObjectRegistryCleaner()");
		if (ObjectRegistry::_instance == 0)
			return;
		delete ObjectRegistry::_instance;
	}
};

// Declare a static instance of the clean.
// When it goes away, it will clean up object registry resources.
//
ObjectRegistryCleaner cleaner;

ObjectRegistry::ObjectRegistry() :
		_map() { /*ctor*/
}

ObjectRegistry::~ObjectRegistry() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"ObjectRegistry"), "ObjectRegistry::~ObjectRegistry()");
	for (MapT::iterator iter = _map.begin(); iter != _map.end(); iter++) {
		delete ((ObjectHolder*)iter->second);
	}
	_map.clear();
}

ObjectRegistry&
ObjectRegistry::instance() {
	if (_instance != 0)
		return *_instance; // Check 1
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	if (_instance != 0)
		return *_instance; // Check 2
	_instance = new ObjectRegistry();
	return *_instance;
}

ObjectRegistry&
ObjectRegistry::registerObject(const std::string& key, void* value, Destructor* pd) {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	LOG4CPLUS_TRACE(Logger::getInstance(L"ObjectRegistry"), "ObjectRegistry::registerObject(\"" << key << "\")");

	// Insert will fail if the object already exists.
	// When the need arises, implement an explicit update method.
	//
	_map.insert(_map.begin(), std::pair<std::string, void*>(key, new ObjectHolder(value, pd)));
	return *this;
}

void* ObjectRegistry::query(const std::string& key) {
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	MapT::iterator iter = _map.find(key);
	if (iter == _map.end())
		return 0;
	return ((ObjectHolder*)iter->second)->_p;
}

std::string
ObjectRegistry::toString() {
	std::stringstream ss;
	ss << "ObjectRegistry[" << this << "]";
	for (MapT::iterator iter = _map.begin(); iter != _map.end(); iter++)
		ss << "{" << iter->first << "," << ((ObjectHolder*)iter->second) << "}";
	return ss.str();
}

void* ObjectRegistry::removeObject(const std::string& key) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"ObjectRegistry"), "ObjectRegistry::removeObject(\"" << key << "\")");
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	ObjectHolder* poh = ((ObjectHolder*)_map[key]);
	void* ret = poh->_p;
	poh->_p = 0;
	delete poh;
	_map.erase(key);
	// ExplicitlyRemoving an object
	return ret;
}

void ObjectRegistry::destroyObject(const std::string& key) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"ObjectRegistry"), "ObjectRegistry::removeObject(\"" << key << "\")");
	std::lock_guard<fast_recursive_mutex> lock(_sync);
	ObjectHolder* poh = ((ObjectHolder*)_map[key]);
	delete poh;
	_map.erase(key);
	return;
}

} // namespace KEP