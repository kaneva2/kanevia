///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <iostream>

#include <log4cplus/logger.h>
using namespace log4cplus;

#include "Core/Util/Executor.h"

using namespace KEP;

thread_local jsThreadExecutor* jsThreadExecutor::s_pCurrentExecutor = nullptr;

jsThreadExecutor::jsThreadExecutor(const std::string& name, jsThdPriority priority, jsThreadType ttype) :
		jsThread(name.c_str(), priority, ttype), AbstractExecutor(name), _closed(false) {}

jsThreadExecutor::~jsThreadExecutor() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Executor"), "jsThreadExecutor::~jsThreadExecutor()");
	//	if ( Executor::running == getState()  ) stop();
	//	_stopACT->wait();
	//	LOG4CPLUS_TRACE(	Logger::getInstance( L"Executor" )
	//								, "jsThreadExecutor::~jsThreadExecutor() Stop complete!" );
}

jsThreadExecutor& jsThreadExecutor::getCurrent() {
	jsThreadExecutor* p = s_pCurrentExecutor;
	if (p == 0) {
		throw ChainedException(SOURCELOCATION, ErrorSpec(-1) << "No thread data");
	}
	return *p;
}

ULONG jsThreadExecutor::_doWork() {
	s_pCurrentExecutor = this;

	setState(running);

	while (!_closed) {
		// Let listeners know we are idling
		//
		ExecutablePtr e;

		setState(polling);
		jsWaitRet wr = _handoff.pop(e); // != WR_ERROR )
		switch (wr) {
			case WR_OK:
				// cout << "Executor[" << name() << "]::doWork(): work acquired." << endl;
				setState(busy);
				timer().Start();
				e->Execute();
				timer().Pause();
				// cout << "Executor[" << name() << "]::doWork(): work complete." << endl;
				continue;
			case WR_TIMEOUT:
				// cout << "Executor::doWork(): timeout" << endl;
				continue;
		}
		break;
	}
	// cout << "Executor::doWork(): complete" << endl;
	setState(stopped);
	act()->complete();
	return 0;
}
