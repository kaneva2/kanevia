///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Core/Util/SQLConnection.h"

namespace KEP {

std::shared_ptr<SQLExecutor> SQLConnection::getSQLExecutor(const std::string& callingFunction, const std::string& logName) {
	std::shared_ptr<SQLExecutor> executor = std::make_shared<SQLExecutor>(*this, callingFunction, logName);
	executor->setCallingFunction(callingFunction);
	executor->setLogName(logName);
	return executor;
}

} // namespace KEP