///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// Configurations: should not be changed once released, otherwise it would affect decryption of existing UGC files
#define KRX_KEY_COUNT 32
#define KRX_BLOCK_SIZE 4096
#define KRX_HASH_LEN 16

// create key ring
bool krx_generate(const unsigned char* hash, int hash_len, const char* salt, unsigned char* out_key_ring);

// transform data (symmetric)
bool krx_transform(const unsigned char* in_buf, int len, const unsigned char* key_ring, unsigned char* out_buf);

// scramble hash chunk to desired length
bool krx_scramble_hash(const unsigned char* orig_hash, int orig_hash_len, unsigned char* out_hash, int out_hash_len);
