///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Core/Crypto/Crypto_KRX.h"
#include "krx.h"

namespace KEP {

BYTE* KRXEncrypt(BYTE* in_buf, FileSize len, const char* salt, const unsigned char* data_hash /*= NULL*/, int data_hash_len /*= 0*/) {
	// KRX is symmetric
	return KRXDecrypt(in_buf, len, salt, data_hash, data_hash_len);
}

BYTE* KRXDecrypt(const BYTE* in_buf, FileSize len, const char* salt, const unsigned char* data_hash, int data_hash_len) {
	DWORD dwCheckSum = 0;
	if (data_hash == NULL) {
		// Hash not passed in, use a checksum over first KRX_BLOCK_SIZE bytes, which will allow
		// the checksum be regenerated the same way out of either clear text or encrypted data.
		for (size_t byteOfs = 0; byteOfs < len && byteOfs < KRX_BLOCK_SIZE; byteOfs += 4)
			dwCheckSum ^= *(DWORD*)(in_buf + byteOfs);
		data_hash = (unsigned char*)&dwCheckSum;
		data_hash_len = sizeof(dwCheckSum);
	}

	const unsigned char* krx_hash = data_hash;
	unsigned char scrambledHash[KRX_HASH_LEN];
	if (data_hash_len < KRX_HASH_LEN) {
		krx_scramble_hash(data_hash, data_hash_len, scrambledHash, KRX_HASH_LEN);
		krx_hash = scrambledHash;
	}

	unsigned char key_ring[KRX_KEY_COUNT];
	if (krx_generate(krx_hash, KRX_HASH_LEN, salt, key_ring)) {
		BYTE* out_buf = new BYTE[len];
		if (out_buf && krx_transform(in_buf, (int)len, key_ring, out_buf))
			return out_buf;
		delete out_buf;
	}

	return NULL;
}

} // namespace KEP
