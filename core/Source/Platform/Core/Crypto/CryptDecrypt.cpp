///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0501
#endif
#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#include <string>
using namespace std;
#include "Core/Crypto/base64.h"

#define MY_ENCRYPT CALG_RC4
//#define MY_ENCRYPT CALG_DES

/*!
 * \brief
 * Attempts to get the default key set container.
 *
 * \param hProv
 * An [out] param that's the handle to the CSP.
 *
 * \returns
 * true, if it's been obtained or false if not
 *
 * If it can't obtain the default context because the
 * user's password has changed, it will attempt to
 * delete and re-create the default key set.
 *
 * The hProv parameter is only valid if GetOrCreateKeyContext
 * returns true.
 */
static bool GetOrCreateKeyContext(HCRYPTPROV& hProv) {
	// Attempt to acquire a handle to the default key container.
	if (!CryptAcquireContext(&hProv, NULL, 0, PROV_RSA_FULL, 0)) {
#ifdef _DEBUG
		// Error creating key container!
		DWORD dw = GetLastError();
		char* lpMsgBuf;
		FormatMessageA(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
				FORMAT_MESSAGE_FROM_SYSTEM |
				FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			dw,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			reinterpret_cast<char*>(&lpMsgBuf),
			0, NULL);
		char msg[256];
		sprintf_s(msg, _countof(msg), "CryptAcquireContext() WARNING: %s", lpMsgBuf);
		LocalFree(lpMsgBuf);
		::OutputDebugStringA(msg);
		::OutputDebugStringA("If you see a message such as \"Decryption of keys failed.\", look in CryptDecrypt.cpp for a possible explanation.");
#endif
		// Whatever the error was, make an attempt to create the default key container
		// before giving up.
		if (!CryptAcquireContext(&hProv, NULL, 0, PROV_RSA_FULL, CRYPT_NEWKEYSET)) {
			return false;
		}
	}
	return true;
}

BOOL SetupCryptoClient() {
	// Ensure that the default cryptographic client is set up.
	HCRYPTPROV hProv;
	HCRYPTKEY hKey;
	if (!GetOrCreateKeyContext(hProv))
		return FALSE;
	// Attempt to get handle to signature key.
	if (!CryptGetUserKey(hProv, AT_SIGNATURE, &hKey)) {
		if (GetLastError() == NTE_NO_KEY) {
			// Create signature key pair.
			if (!CryptGenKey(hProv, AT_SIGNATURE, 0, &hKey)) {
				// Error during CryptGenKey!
				CryptReleaseContext(hProv, 0);
				return FALSE;
			} else {
				CryptDestroyKey(hKey);
			}
		} else {
			// Error during CryptGetUserKey!
			CryptReleaseContext(hProv, 0);
			return FALSE;
		}
	}

	// Attempt to get handle to exchange key.
	if (!CryptGetUserKey(hProv, AT_KEYEXCHANGE, &hKey)) {
		if (GetLastError() == NTE_NO_KEY) {
			// Create key exchange key pair.
			if (!CryptGenKey(hProv, AT_KEYEXCHANGE, 0, &hKey)) {
				// Error during CryptGenKey!
				CryptReleaseContext(hProv, 0);
				return FALSE;
			} else {
				CryptDestroyKey(hKey);
			}
		} else {
			// Error during CryptGetUserKey!
			CryptReleaseContext(hProv, 0);
			return FALSE;
		}
	}

	CryptReleaseContext(hProv, 0);
	return TRUE;
}

BOOL EncryptBytes(const char* bytesToEncrypt, DWORD len, const char* szKey) {
	BOOL bResult = TRUE;
	HCRYPTPROV hProv = NULL;
	HCRYPTKEY hKey = NULL;
	HCRYPTHASH hHash = NULL;
	DWORD dwLength;

	if (len == 0)
		return TRUE;

	if (!GetOrCreateKeyContext(hProv))
		return FALSE;

	// Get handle to user default provider.
	// Create hash object.
	if (CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash)) {
		// Hash password string.
		dwLength = (DWORD)strlen(szKey);
		if (CryptHashData(hHash, (BYTE*)szKey, dwLength, 0)) {
			// Create block cipher session key based on hash of the password.
			if (CryptDeriveKey(hProv, MY_ENCRYPT, hHash, CRYPT_EXPORTABLE, &hKey)) {
				if (!CryptEncrypt(hKey, 0, TRUE, 0, (BYTE*)bytesToEncrypt, &len, len)) {
					bResult = FALSE;
				}
				CryptDestroyKey(hKey); // Release provider handle.
			} else {
				// Error during CryptDeriveKey!
				bResult = FALSE;
			}
		} else {
			// Error during CryptHashData!
			bResult = FALSE;
		}
		CryptDestroyHash(hHash);
		// Destroy session key.
	} else {
		// Error during CryptCreateHash!
		bResult = FALSE;
	}
	CryptReleaseContext(hProv, 0);
	return bResult;
}

BOOL DecryptBytes(const char* bytesToDecrypt, DWORD len, const char* szKey) {
	BOOL bResult = TRUE;
	HCRYPTPROV hProv = NULL;
	HCRYPTKEY hKey = NULL;
	HCRYPTHASH hHash = NULL;
	DWORD dwLength;

	if (len == 0)
		return TRUE;

	if (!GetOrCreateKeyContext(hProv))
		return FALSE;

	// Create hash object.
	if (CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash)) {
		// Hash password string.
		dwLength = (DWORD)strlen(szKey);
		if (CryptHashData(hHash, (BYTE*)szKey, dwLength, 0)) {
			// Create block cipher session key based on hash of the password.
			if (CryptDeriveKey(
					hProv, MY_ENCRYPT, hHash, CRYPT_EXPORTABLE, &hKey)) {
				if (!CryptDecrypt(hKey, 0, TRUE, 0, (BYTE*)bytesToDecrypt, &len)) {
					bResult = FALSE;
				}

				CryptDestroyKey(hKey); // Release provider handle.

			} else {
				// Error during CryptDeriveKey!
				bResult = FALSE;
			}
		} else {
			// Error during CryptHashData!
			bResult = FALSE;
		}
		CryptDestroyHash(hHash); // Destroy session key.
	} else {
		// Error during CryptCreateHash!
		bResult = FALSE;
	}
	CryptReleaseContext(hProv, 0);
	return bResult;
}

BOOL EncryptAndEncodeString(const char* szPassword, string& encryptPwd, const char* szKey) {
	if (szPassword == 0 || strlen(szPassword) == 0) {
		encryptPwd.clear();
		return TRUE;
	}

	char* szEncryptPwd = _strdup(szPassword);
	BOOL ret = EncryptBytes(szEncryptPwd, strlen(szPassword), szKey);
	if (ret)
		encryptPwd = Base64::encode((const BYTE*)szEncryptPwd, strlen(szPassword));
	if (szEncryptPwd)
		free(szEncryptPwd);
	return ret;
}

BOOL DecodeAndDecryptString(const char* szEncryptPwd, string& szPassword, const char* szKey) {
	BYTE* result = 0;
	DWORD len = 0;
	Base64::decode(szEncryptPwd, &result, &len);
	BOOL ret = DecryptBytes((const char*)result, len, szKey);
	if (ret && result)
		szPassword.assign((const char*)result, len);
	else
		szPassword.clear();

	if (result)
		free(result);
	return ret;
}

//#define CRYPT_TEST
#ifdef CRYPT_TEST
#pragma comment(lib, "Advapi32.lib")

int main(int argc, char* argv[]) {
	// Init the crypto API
	BOOL bResult = SetupCryptoClient();
	if (bResult)
		printf("Cryptographic client Ok\r\n");
	else
		printf("Cryptographic client error\r\n");

	// input password
	printf("Input password: ");
	char szPassword[32] = _T("");
	_getts(szPassword);

	// The Key: has to be the same for encrypt and decryption!
	char szKey[] = _T("Mz6@a0i*");

	// encrpyt password
	char* szEncryptPwd;
	string b64;
	char* szPasswordDecrypted;

	// encrpyt password using bytes
	szEncryptPwd = strdup(szPassword);
	EncryptBytes(szEncryptPwd, strlen(szPassword), szKey);
	b64 = Base64::encode((const BYTE*)szEncryptPwd, strlen(szPassword));
	printf("The base64 version is %s\r\n", b64.c_str());

	// decrypt password
	DecryptBytes(szEncryptPwd, strlen(szPassword), szKey);
	printf("The decrypted password is %s\r\n", szEncryptPwd);

	BYTE* result = 0;
	DWORD len = 0;
	Base64::decode(b64, &result, &len);
	DecryptBytes((const char*)result, len, szKey);
	string pw((const char*)result, len);
	printf("The decrypted Base64 password is %s - %s\r\n", pw.c_str(), (pw == szPassword) ? "ok" : "failed\b");

	while (strlen(szPassword) > 0) {
		// new fns
		EncryptAndEncodeString(szPassword, b64, szKey);
		printf("\nThe base64 version is %s\r\n", b64.c_str());

		DecodeAndDecryptString(b64.c_str(), pw, szKey);
		printf("The decrypted Base64 password is %s - %s\r\n", pw.c_str(), (pw == szPassword) ? "ok" : "failed\b");

		// finish
		printf("Press enter for end the program!\n");
		_getts(szPassword);
	}

	return 0;
}
#endif
