///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Core/Crypto/Crypto_MD5.h"
#include "md5.h"
#include "assert.h"

namespace KEP {

void hashString(const char* str, MD5_DIGEST_STR& outStr) {
	MD5_DIGEST digest;
	ZeroMemory(digest.s, sizeof(digest.s));

	MD5_CTX ctx;
	MD5Init(&ctx);
	MD5Update(&ctx, (unsigned char*)str, ::strlen(str));
	MD5Final(digest.s, &ctx);

	ZeroMemory(outStr.s, 2 * MD5_DIGEST_LEN + 1);
	for (int i = 0; i < MD5_DIGEST_LEN; i++) {
		_snprintf_s(outStr.s, _countof(outStr.s), _countof(outStr.s) - 1, "%s%02x", outStr.s, (int)digest.s[i]);
	}
}

void hashString(const char* str, MD5_DIGEST& digest) {
	ZeroMemory(digest.s, sizeof(digest.s));

	MD5_CTX ctx;
	MD5Init(&ctx);
	MD5Update(&ctx, (unsigned char*)str, ::strlen(str));
	MD5Final(digest.s, &ctx);
}

bool hashFile(const char* fname, MD5_DIGEST_STR& outStr, ULONG loopSleep) {
	MD5_DIGEST digest;
	ZeroMemory(digest.s, sizeof(digest.s));

	if (hashFile(fname, digest, loopSleep)) {
		ZeroMemory(outStr.s, 2 * MD5_DIGEST_LEN + 1);
		for (int i = 0; i < MD5_DIGEST_LEN; i++) {
			_snprintf_s(outStr.s, _countof(outStr.s), _TRUNCATE, "%s%02x", outStr.s, (int)digest.s[i]);
		}
		return true;
	} else {
		return false;
	}
}

bool hashFile(const char* fname, MD5_DIGEST& digest, ULONG loopSleep) {
	bool ret = false;

#define BUFFER_SIZE 4096
	unsigned char buffer[BUFFER_SIZE];

	FILE* f = 0;
	if (fopen_s(&f, fname, "rb") == 0 && f) {
		ZeroMemory(digest.s, sizeof(digest.s));

		MD5_CTX ctx;
		MD5Init(&ctx);

		ULONG bytesRead = 0;
		while ((bytesRead = fread(buffer, 1, BUFFER_SIZE, f)) > 0) {
			if (loopSleep > 0)
				::Sleep(loopSleep);
			MD5Update(&ctx, buffer, bytesRead);
		}

		MD5Final(digest.s, &ctx);

		int err = ferror(f);
		if (err != 0) {
			//errorNumToString(HRESULT_FROM_WIN32(ferror(f)), errMsg);
		} else {
			ret = true;
		}
		fclose(f);
	} else {
		//errorNumToString(HRESULT_FROM_WIN32(errno), errMsg);
	}

	return ret;
}

void hashData(const unsigned char* data, FileSize len, MD5_DIGEST_STR& outStr, ULONG loopSleep /*= 0 */) {
	MD5_DIGEST digest;
	ZeroMemory(digest.s, sizeof(digest.s));

	hashData(data, len, digest, loopSleep);

	ZeroMemory(outStr.s, 2 * MD5_DIGEST_LEN + 1);
	for (size_t i = 0; i < MD5_DIGEST_LEN; i++) {
		_snprintf_s(outStr.s, _countof(outStr.s), _TRUNCATE, "%s%02x", outStr.s, (int)digest.s[i]);
	}
}

void hashData(const unsigned char* data, FileSize len, MD5_DIGEST& digest, ULONG loopSleep /*= 0 */) {
#define CHUNK_SIZE 4096

	ZeroMemory(digest.s, sizeof(digest.s));

	MD5_CTX ctx;
	MD5Init(&ctx);

	FileSize bytesProcessed = 0;
	while (bytesProcessed < len) {
		if (loopSleep > 0)
			::Sleep(loopSleep);
		FileSize bytes = min(CHUNK_SIZE, len - bytesProcessed);
		MD5Update(&ctx, const_cast<unsigned char*>(data + bytesProcessed), (unsigned int)bytes);
		bytesProcessed += bytes;
	}

	MD5Final(digest.s, &ctx);
}

bool decodeHashString(const char* hashString, unsigned char* hashBytes) {
	int length = strlen(hashString);
	if (length != MD5_DIGEST_LEN * 2) {
		return false;
	}

	const char* p = hashString;
	for (UINT i = 0; i < MD5_DIGEST_LEN; i++) {
		hashBytes[i] = 0;

		for (int j = 0; j < 2; j++) {
			char c = *p++;
			unsigned char val = 0;
			if (isdigit(c)) {
				val = c - '0';
			} else if (isalpha(c)) {
				val = (c - 'A') % 32 + 10;
			} else {
				return false;
			}

			assert(val < 16);

			hashBytes[i] <<= 4;
			hashBytes[i] += val;
		}
	}

	return true;
}

bool MD5VerifyData(const unsigned char* dataBuffer, FileSize bufferLen, const unsigned char* digest, size_t digestLen) {
	assert(dataBuffer != nullptr && bufferLen != 0);
	assert(digest != nullptr && digestLen == MD5_DIGEST_LEN);

	if (digest == nullptr || digestLen != MD5_DIGEST_LEN) {
		return false;
	}

	std::string errMsg;
	MD5_DIGEST md5;
	hashData(dataBuffer, bufferLen, md5);
	return memcmp(md5.s, digest, digestLen) == 0;
}

} // namespace KEP
