///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Core/Crypto/Crypto_SHA2.h"
#include <memory>

namespace KEP {

#if _WIN32_WINNT >= 0x0502 // WinCrypt SHA2 support available on Windows 5.2 or later

bool SHA256VerifyData(const unsigned char* dataBuffer, unsigned bufferLen, const unsigned char* digest, unsigned digestLen) {
	assert(dataBuffer != nullptr && bufferLen != 0);
	assert(digest != nullptr && digestLen == SHA256_DIGEST_LEN);

	if (digest == nullptr || digestLen != SHA256_DIGEST_LEN) {
		return false;
	}

	bool res = true;

	// Use Microsoft crypto library for SHA-256 verification
	// TODO: replace with a cross-platform crypto lib

	// https://msdn.microsoft.com/en-us/library/windows/desktop/aa379908%28v=vs.85%29.aspx
	HCRYPTPROV hCryptProv = NULL;
	HCRYPTHASH hHash = NULL;

	if (res && !CryptAcquireContext(&hCryptProv, NULL, NULL, PROV_RSA_AES, 0)) {
		//_LogError("VerifyChecksum: error creating context");
		res = false;
	}

	// Acquire a hash object handle.
	if (res && !CryptCreateHash(hCryptProv, CALG_SHA_256, 0, 0, &hHash)) {
		//_LogError("VerifyChecksum: error creating hash object");
		res = false;
	}

	// Generate hash
	if (res && !CryptHashData(hHash, dataBuffer, bufferLen, 0)) {
		//_LogError("VerifyChecksum: error hashing data");
		res = false;
	}

	if (res) {
		assert(hCryptProv);
		assert(hHash);

		// Get calculated hash size
		unsigned calcualtedHashSize = 0;
		DWORD cbHashSize = sizeof(unsigned);

		if (res && !CryptGetHashParam(hHash, HP_HASHSIZE, reinterpret_cast<unsigned char*>(&calcualtedHashSize), &cbHashSize, 0)) {
			//_LogError("VerifyChecksum: error getting hash size");
			res = false;
		} else {
			assert(cbHashSize == sizeof(unsigned));
			assert(calcualtedHashSize == digestLen);

			if (calcualtedHashSize == digestLen) {
				// Get calculated hash value
				auto calculatedHash = std::make_unique<unsigned char[]>(digestLen);
				DWORD cbHashVal = digestLen;

				if (res && !CryptGetHashParam(hHash, HP_HASHVAL, calculatedHash.get(), &cbHashVal, 0)) {
					//_LogError("VerifyChecksum: error retrieving calculated hash");
					res = false;
				} else {
					assert(cbHashVal == digestLen);
					res = memcmp(calculatedHash.get(), digest, digestLen) == 0;
				}
			}
		}
	}

	// hHash must be released.
	if (hHash) {
		CryptDestroyHash(hHash);
	}

	// hCryptProv must be released.
	if (hCryptProv) {
		CryptReleaseContext(hCryptProv, 0);
	}

	return res;
}

#endif

} // namespace KEP
