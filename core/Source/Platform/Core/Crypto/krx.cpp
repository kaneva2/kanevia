///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "krx.h"

bool krx_generate(const unsigned char* hash, int hash_len, const char* salt, unsigned char* out_key_ring) {
	if (hash == NULL || hash_len <= 0 || salt == NULL || strlen(salt) != KRX_KEY_COUNT || out_key_ring == NULL) {
		return false;
	}

	for (UINT i = 0; i < KRX_KEY_COUNT; i++) {
		UINT idx = (salt[i] - 'A') % hash_len;
		out_key_ring[i] = hash[idx];
	}

	return true;
}

#define MAKE_KEY(ring, kid, key, dwKey) \
	key = ring[kid];                    \
	dwKey = key;                        \
	dwKey = (dwKey << 8) + dwKey;       \
	dwKey = (dwKey << 16) + dwKey;

bool krx_transform(const unsigned char* in_buf, int len, const unsigned char* key_ring, unsigned char* out_buf) {
	int keyIndex = 0;
	int DW_ofs = 0;

	unsigned char key;
	unsigned int dwKey;

	MAKE_KEY(key_ring, keyIndex, key, dwKey);

	while (DW_ofs < len / 4) {
		((unsigned int*)out_buf)[DW_ofs] = ((unsigned int*)in_buf)[DW_ofs] ^ dwKey;
		++DW_ofs;

		if ((DW_ofs << 2) % KRX_BLOCK_SIZE == 0) {
			keyIndex++;
			if (keyIndex == KRX_KEY_COUNT) {
				keyIndex = 0;
			}
			MAKE_KEY(key_ring, keyIndex, key, dwKey);
		}
	}

	int ofs = DW_ofs << 2;
	while (ofs < len) {
		out_buf[ofs] = in_buf[ofs] ^ key;
		ofs++;
	}

	return true;
}

bool krx_scramble_hash(const unsigned char* orig_hash, int orig_hash_len, unsigned char* out_hash, int out_hash_len) {
	// Input hash must be at least 4 bytes long
	if (orig_hash_len < 4) {
		return false;
	}

	int idx = 0;
	for (; idx < min(orig_hash_len, out_hash_len); idx++) {
		out_hash[idx] = orig_hash[idx];
	}

	for (; idx < out_hash_len; idx++) {
		out_hash[idx] = out_hash[idx - 4] ^ out_hash[idx - 3];
	}

	return true;
}
