///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Core/Math/KEPMath.h"
#include "Core/Math/Rotation.h"

#include <crtdbg.h>

namespace KEP {

#define CheckResult(test) CheckResultFunction(__FILE__, __LINE__, #test, test)

#define StaticAssertVectorSize(N, T) static_assert(sizeof(Vector<N, T>) == N * sizeof(T), "Packing problem");
StaticAssertVectorSize(1, float);
StaticAssertVectorSize(2, float);
StaticAssertVectorSize(3, float);
StaticAssertVectorSize(4, float);
StaticAssertVectorSize(1, double);
StaticAssertVectorSize(2, double);
StaticAssertVectorSize(3, double);
StaticAssertVectorSize(4, double);

bool CheckResultFunction(const char* fileName, int lineNum, const char* test, bool b) {
	if (!b) {
#if defined(_DEBUG)
		_CrtDbgReport(0, fileName, lineNum, "", "Test failed: %s\n", test);
		_CrtDbgBreak();
#endif
	}
	return b;
}

bool TestBasicVector() {
	bool bResult = true;
#if 0

	Vector3f vZero(0, 0, 0);
	Vector3f vX(1, 0, 0);
	Vector3f vY(0, 1, 0);
	Vector3f vZ = Vector3f::Basis<2>();

	Vector3f vXplusY = vX + vY;
	Vector3f vXminusY = vX - vY;
	float vXdotY = vX.Dot(vY);
	Vector3f vXcrossY = vX.Cross(vY);
	Vector3f vXcrossY_;
	vXcrossY_.Cross(vX, vY);
	float xyLength2 = vXplusY.LengthSquared();
	float xyLength = vXplusY.Length();
	Vector3f v2X = 2 * vX;
	Vector3f vX2 = vX * 2;
	Vector3f vXover2 = vX / 2;
	Vector2d vA(1.1,3.0/7.0);
	Vector3f vB{3.3f, 9.7f, 2.1f};
	Vector3f vXYNormalized = vXplusY.GetNormalizedUnchecked();

	float sum = vB.X() + vB.Y() + vB.Z();

	Vector3f vXYNormalizedAlt = vXplusY;
	vXYNormalizedAlt.NormalizeUnchecked();
	bResult &= CheckResult(Distance(vXYNormalizedAlt, vXYNormalized) < .01);

	float dist2 = DistanceSquared(vXplusY, vZ);
	float dist = Distance(vXplusY, vZ);

	bResult &= CheckResult(AreEqual(dist,1.7,.1));

#endif
	return bResult;
}

bool TestBasicMatrix() {
	bool bResult = true;

	Matrix44f mat1;
	mat1.SetRowX(Vector3f(1, 2, 3));
	mat1.SetRowY(Vector3f(4, 5, 6));
	mat1.SetRowZ(Vector3f(7, 8, 0));
	mat1.SetRowW(Vector3f(10, 11, 12));
	mat1.SetColumn(3, Vector4f(13, 14, 15, 16));

	Vector3f v3f_in(1, 2, 3);
	Vector4f v4f_in(1, 2, 3, 4);
	Vector3f v3f_out = TransformVector(mat1, v3f_in);
	Vector4f v4f_out = TransformVector(mat1, v4f_in);
	Vector3f v3fH_out = TransformPoint(mat1, v3f_in);
	return bResult;
}

bool TestBasicQuaternion() {
	bool bResult = true;

	Quaternionf q(0, 3, 0, 1);
	q.MakeIdentity();
	Quaternionf qI = Quaternionf::GetIdentity();

	Quaternionf q1(1, 3, 4, 1);
	q1.NormalizeUnchecked();
	Matrix44f m;
	ConvertRotation(q1, out(m));
	Quaternionf q2;
	ConvertRotation(m, out(q2));
	bResult &= CheckResult(Distance(q1, q2) < .01);

	return bResult;
}

KEPMATH_EXPORT bool TestKEPMath() {
	bool bResult = true;
	bResult &= TestBasicVector();
	bResult &= TestBasicQuaternion();

	return bResult;
}

#if defined(_DEBUG)
static int s_RunTest = (TestKEPMath(), 0);
#endif
} // namespace KEP