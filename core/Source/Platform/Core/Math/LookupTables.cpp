///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include <Core/Math/KEPMathUtil.h>

namespace KEP {

namespace KEPMathDetail {
KEPMATH_EXPORT float g_SinCosLookupTable[knSinCosLookupTableEntries];
struct SinCosTableInitializer {
	SinCosTableInitializer() {
		for (size_t d = 0; d < knSinCosLookupTableEntries; ++d) {
			double rad = 2 * M_PI * d / knSinCosLookupTableEntries;
			double dSine = sin(rad);
			if (d == knSinCosLookupTableEntries / 2)
				dSine = 0;
			g_SinCosLookupTable[(d + 3 * knSinCosLookupTableEntries / 4) % knSinCosLookupTableEntries] = dSine;
		}
	}
};
static SinCosTableInitializer s_SinCosTableInitializer;
} // namespace KEPMathDetail

} // namespace KEP
