/******************************************************************************
 BladeExports.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "SDKCommon.h"

#include "IBlade.h"
#include "EngineTypes.h"

namespace KEP {

typedef IBlade *(*CREATE_BLADE_FN)();

typedef struct strBladeInfo {
	const char *className;
	CREATE_BLADE_FN createBlade;
	ULONG version;
} BladeInfo;

} // namespace KEP

// 5/09 added engine checking, added new macros to let old ones default to same behavior
#define BEGIN_BLADE_LIST                                          \
	bool engineTypeSupported(int /*engineType*/) { return true; } \
	BladeInfo gBladeInfo[] = {
#define BEGIN_BLADE_LIST_ENGINE_TYPE(et)                                                     \
	bool engineTypeSupported(int engineType) { return engineType == 0 || engineType == et; } \
	BladeInfo gBladeInfo[] = {
#define ADD_BLADE(className) { typeid(className).name(), create##className, className::BladeVersion() },
#define END_BLADE_LIST \
	{ 0, 0 }           \
	}                  \
	;

#ifdef _AFX
#define IMPLEMENT_CREATE(className)                  \
	IBlade *create##className() {                    \
		AFX_MANAGE_STATE(AfxGetStaticModuleState()); \
		return new className();                      \
	}
#else
#define IMPLEMENT_CREATE(className) \
	IBlade *create##className() { return new className(); }
#endif

