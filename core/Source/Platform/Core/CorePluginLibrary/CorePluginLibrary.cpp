///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Common/KEPUtil/Plugin.h"
#include "Common/KEPUtil/Mailer.h"
#include "Common/KEPUtil/MongooseHTTPServer.h"
#include "Common/KEPUtil/Heartbeat.h"
#include "Common/KEPUtil/ThreadPoolPlugin.h"
#include "Common/KEPUtil/Plugin.h"
#include "Metrics.h"
#include "MessageQueue.h"

using namespace KEP;

extern "C" __declspec(dllexport) void EnumeratePlugins(PluginFactoryVecPtr v) {
	v->push_back(PluginFactoryPtr(new MailerPluginFactory()));
	v->push_back(PluginFactoryPtr(new MongooseHTTPPluginFactory()));
	v->push_back(PluginFactoryPtr(new HeartbeatGeneratorPluginFactory()));
	v->push_back(PluginFactoryPtr(new ThreadPoolPluginFactory()));
	//	v->push_back(PluginFactoryPtr(new MySqlConnectorPluginFactory() ) );
	v->push_back(PluginFactoryPtr(new MetricsPluginFactory()));
	v->push_back(PluginFactoryPtr(new MessageQueuePluginFactory()));
}
