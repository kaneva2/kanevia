///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <Log4CPlus/Logger.h>
#include "Core\Util\Runnable.h"
#include "Common\KEPUtil\Algorithms.h"
#include "Common\KEPUtil\HTTPServer.h"
#include "Common\KEPUtil\ParamString.h"
#include "Core\Util\ThreadPool.h"
#include "Common\include\KEPConfig.h"
#include "Tools/HttpClient/GetBrowserPage.h"
#include "TinyXML/tinyxml.h"
#include "Metrics.h"

using namespace std;
using namespace log4cplus;

namespace KEP {

struct MetricsAgentInput {
	// Version 0 Unkeyed metric
	//
	MetricsAgentInput(const string& counterName, double a1, double a2, double b1, double b2) :
			_counterName(counterName), _counterKey(""), _counterData(a1, a2, b1, b2), _version(0) {}

	// Version 1 Keyed Metric
	//
	MetricsAgentInput(const string& counterName, const MetricsAgent::CounterKey& counterKey, const MetricsAgent::CounterData& counterData) :
			_counterName(counterName), _counterKey(counterKey), _counterData(counterData), _version(1) {}

	MetricsAgentInput(const MetricsAgentInput& other) :
			_counterName(other._counterName), _counterKey(other._counterKey), _counterData(other._counterData), _version(other._version) {}

	MetricsAgentInput& operator=(const MetricsAgentInput& rhs) {
		_counterName = rhs._counterName;
		_version = rhs._version;
		_counterKey = rhs._counterKey;
		_counterData = rhs._counterData;
		return *this;
	}

	string toString() {
		stringstream ss;
		ss << "MetricsAgentInput(" << this << ")counterName=" << _counterName << ";"
		   << _counterKey.toString()
		   << _counterData.toString();
		return ss.str();
	}

	const string& name() const { return _counterName; }
	const MetricsAgent::CounterKey& key() const { return _counterKey; }
	const MetricsAgent::CounterData& data() const { return _counterData; }
	unsigned int version() const { return _version; }

private:
	string _counterName;
	MetricsAgent::CounterData _counterData;
	MetricsAgent::CounterKey _counterKey;
	unsigned int _version;
};

/**
 * A MetricsAgent implementation that serves as a proxy for a concrete MetricsAgent implementation.
 * This concrete implementation can be local or remote.  Initially, the business end of this plugin
 * was implemented such that the plugin accessed the Metrics database directly.  This has now been
 * abandone and the functionality has been moved to WoKWeb, hence the introduction of this class.
 */
class MetricsAgentProxy : public MetricsAgent {
public:
	/**
	 * Construct with a URL that will be used to access the remote MetricsAgent
	 */
	MetricsAgentProxy(const URL& reportingURL) :
			MetricsAgent(), _reportingURL(reportingURL), _threadPool(0) {
		LOG4CPLUS_TRACE(Logger::getInstance(L"Metrics"), "[0x" << hex << this << "]MetricsAgentProxy()(" << reportingURL.toString().c_str() << ")");
	}

	MetricsAgentProxy(const MetricsAgentProxy& other) :
			MetricsAgent(), _reportingURL(other._reportingURL), _threadPool(other._threadPool) {
		LOG4CPLUS_TRACE(Logger::getInstance(L"Metrics"), "[0x" << hex << this << "]MetricsAgentProxy(const MetricsAgentProxy& )(" << &other << ")");
	}

	MetricsAgentProxy& reportMetric(const std::string& counterName, const CounterKey& counterKey, const CounterData& counterData) {
		LOG4CPLUS_TRACE(Logger::getInstance(L"Metrics"), "[0x" << hex << this << "]MetricsAgentProxy::reportMetric()(" << counterName << ")");
		MetricsAgentInput input(counterName, counterKey, counterData);
		if (_threadPool == 0)
			reportMetricImpl(input);
		else
			_threadPool->enqueue(RunnablePtr(new MetricsTask(*this, input)));
		return *this;
	}

	MetricsAgentProxy& setThreadPool(ThreadPool& threadPool) {
		LOG4CPLUS_TRACE(Logger::getInstance(L"Metrics"), "[0x" << hex << this << "]MetricsAgentProxy::setThreadPool()(" << threadPool.name().c_str() << ")");
		_threadPool = &threadPool;
		return *this;
	}

	MetricsAgentProxy& enable(bool /* doEnable */ = true) {
		return *this;
	}

private:
	class MetricsTask : public Runnable {
	public:
		MetricsTask(MetricsAgentProxy& impl, MetricsAgentInput input) :
				_impl(impl), _input(input) {}

		RunResultPtr run() override {
			_impl.reportMetricImpl(_input);
			return RunResultPtr();
		}

	private:
		MetricsAgentProxy& _impl;
		MetricsAgentInput _input;
	};

	/**
	 * The business end of this Plugin.  
	 * @see MetricsAgent
	 */
	MetricsAgentProxy& reportMetricImpl(const MetricsAgentInput& input) {
		std::lock_guard<fast_recursive_mutex> lock(_urlSync);
		switch (input.version()) {
			case 0:
				_reportingURL.clearParams()
					.setParam("version", "0")
					.setParam("action", "report")
					.setParam("countername", input.name())
					.setParam("a1", StringHelpers::parseNumber<double>(input.data().a1()))
					.setParam("a2", StringHelpers::parseNumber<double>(input.data().a2()))
					.setParam("b1", StringHelpers::parseNumber<double>(input.data().b1()))
					.setParam("b2", StringHelpers::parseNumber<double>(input.data().b2()));
				break;
			case 1:
				_reportingURL.clearParams()
					.setParam("version", "1")
					.setParam("action", "report")
					.setParam("countername", input.name())
					.setParam("k1", input.key().k1())
					.setParam("k2", input.key().k2())
					.setParam("k3", input.key().k3())
					.setParam("k4", input.key().k4())
					.setParam("a1", StringHelpers::parseNumber<double>(input.data().a1()))
					.setParam("a2", StringHelpers::parseNumber<double>(input.data().a2()))
					.setParam("b1", StringHelpers::parseNumber<double>(input.data().b1()))
					.setParam("b2", StringHelpers::parseNumber<double>(input.data().b2()));
				break;
			default:
				LOG4CPLUS_DEBUG(Logger::getInstance(L"Metrics"), "Unsupported Metrics API version [" << input.version() << "]");
				break;
		};

		string url = _reportingURL.encodedString();
		LOG4CPLUS_DEBUG(Logger::getInstance(L"Metrics"), "Calling url [" << url.c_str() << "]");

		string content;
		unsigned long statusCode;
		string statusMessage;
		size_t resultSize = 0;
		if (!GetBrowserPage(
				url, content, resultSize, statusCode, statusMessage)) {
			stringstream ss;
			ss << "Remote call failed [" << url.c_str() << "][" << statusCode << "][" << statusMessage.c_str() << "]";
			throw MetricsException(SOURCELOCATION, ErrorSpec(statusCode, ss.str()));
		}

		LOG4CPLUS_DEBUG(Logger::getInstance(L"Metrics"), "Remote call completed [" << url.c_str() << "]"
																				   << "[" << statusCode << "]"
																				   << "[" << statusMessage << "]"
																				   << "[" << content << "]");

		return *this;
	}

	fast_recursive_mutex _urlSync;
	URL _reportingURL;
	ThreadPool* _threadPool;
};

MetricsPlugin& MetricsPlugin::reportMetric(const string& counterName, const CounterKey& counterKey, const CounterData& counterData) {
	_impl->reportMetric(counterName, counterKey, counterData);
	return *this;
}

MetricsPlugin& MetricsPlugin::enable(bool doEnable /* = true */) {
	_impl->enable(doEnable);
	return *this;
}

MetricsPlugin::MetricsPlugin() :
		AbstractPlugin("Metrics"), _impl(MetricsAgent::MetricsAgentPtr(new NullMetricsAgent())) {}

MetricsPlugin::MetricsPlugin(const MetricsPlugin& other) :
		AbstractPlugin(other.name()), _impl(other._impl) {}

MetricsPlugin&
MetricsPlugin::setImpl(MetricsAgent::MetricsAgentPtr impl) {
	_impl = impl;
	return *this;
}

MetricsPlugin::~MetricsPlugin() {}

MetricsPlugin&
MetricsPlugin::operator=(const MetricsPlugin& /* other*/) {
	return *this;
}

MetricsPlugin&
MetricsPlugin::setUp() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Metrics"), "[0x" << hex << this << "]MetricsPlugin::setup()");
	// Acquire reference to thread pool.  And add it to our implementation.
	// Note that we are assuming that the implementation is a MetricsAgentProxy.
	// When and if we add another implementation, this step will need to
	// be moved to the implementation (MetricsAgentProxy);
	//
	// Note that for better decouple the name of this plugin should be configured for
	// the client component.  e.g. KGPServer.xml should specify e.g.
	// <kgpserver>
	//   <MetricsAgent name="Metrics">
	// </kgpserver>
	//
	ThreadPool* threadPool = PluginHost::singleton().DEPRECATED_getInterface<ThreadPool>("ThreadPool");
	if (threadPool == 0) {
		LOG4CPLUS_WARN(Logger::getInstance(L"Metrics"), "No thread pool available.  Reporting will occur on calling thread!");
	} else {
		LOG4CPLUS_DEBUG(Logger::getInstance(L"Metrics"), "[0x" << hex << this << "]MetricsPlugin::setup()impl=0x" << hex << _impl.operator->() << ")");
		((MetricsAgentProxy*)_impl.operator->())->setThreadPool(*threadPool);
	}
	return *this;
}

MetricsPlugin&
MetricsPlugin::tearDown() {
	return *this;
}

// Plugin support
MetricsPluginFactory::MetricsPluginFactory() :
		AbstractPluginFactory("Metrics"), _config(0) {
	//    cout << "MetricsPluginFactory::MetricsPluginFactory()" << endl;
}

MetricsPluginFactory::MetricsPluginFactory(KEPConfig* config) :
		AbstractPluginFactory("Metrics"), _config(config) {
	//    cout << "MetricsPluginFactory::MetricsPluginFactory( KEPConfig& config )" << endl;
}

MetricsPluginFactory::~MetricsPluginFactory() {
	//    cout << "MetricsPluginFactory::~MetricsPluginFactory()" << endl;
}

const std::string& MetricsPluginFactory::name() const {
	//    cout << "MetricsPluginFactory::name()" << endl;
	return AbstractPluginFactory::name();
}

PluginPtr
MetricsPluginFactory::createPlugin(const TIXMLConfig::Item& configItem) const {
	TiXmlElement* config = configItem._internal;
	LOG4CPLUS_TRACE(Logger::getInstance(L"Metrics"), "MetricsPluginFactory::createPlugin()");

	// Guard clause.  If no configuration provided, we can go with
	// default settings.  We're done here.
	//
	if (config == 0)
		return PluginPtr(new MetricsPlugin());

	// Note that we cannot default this to production because we cannot risk
	// bad configurations contaminating our metrics
	//
	const char* pcURL = config->Attribute("metrics_url");
	if (pcURL == 0) {
		LOG4CPLUS_ERROR(Logger::getInstance(L"Metrics"), "metrics_url not specified in configuration");
		return PluginPtr(new MetricsPlugin());
	}

	try {
		// if and when we "unabandon" the MySqlMetricsAgent, this will need to be modified
		// to switch based on the configuration.
		//
		MetricsAgent::MetricsAgentPtr impl(new MetricsAgentProxy(URL::parse(pcURL)));
		MetricsPlugin* pPlugin = new MetricsPlugin();
		pPlugin->setImpl(impl);
		return PluginPtr(pPlugin);
	} catch (const ParseException& exc) {
		LOG4CPLUS_ERROR(Logger::getInstance(L"MetricsAgent"), "Failed to parse url [" << pcURL << "] Using null metrics implementation [" << exc.str() << "]");
		return PluginPtr(new MetricsPlugin());
	}
}

} // namespace KEP