///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/KEPUtil/Plugin.h"
#include "Common/KEPUtil/Heartbeat.h"
#include "Common/KEPUtil/URL.h"
#include <memory>

namespace KEP {

CHAINED(MetricsException);

class MetricsInternals;

class _declspec(dllexport) MetricsAgent {
public:
	typedef std::shared_ptr<MetricsAgent> MetricsAgentPtr;

	class CounterKey {
	public:
		CounterKey(const std::string& k1, const std::string& k2 = "", const std::string& k3 = "", const std::string& k4 = "") :
				_k1(k1), _k2(k2), _k3(k3), _k4(k4) {}

		CounterKey(const CounterKey& other) :
				_k1(other._k1), _k2(other._k2), _k3(other._k3), _k4(other._k4) {}

		CounterKey& operator=(const CounterKey& rhs) {
			_k1 = rhs._k1;
			_k2 = rhs._k2;
			_k3 = rhs._k3;
			_k4 = rhs._k4;
			return *this;
		}

		virtual ~CounterKey() {}

		std::string toString() {
			std::stringstream ss;
			ss << "k1=" << _k1
			   << ";k2=" << _k2
			   << ";k3=" << _k3
			   << ";k4=" << _k4;

			return ss.str();
		}

		const std::string& k1() const { return _k1; }
		const std::string& k2() const { return _k2; }
		const std::string& k3() const { return _k3; }
		const std::string& k4() const { return _k4; }

	private:
		/**
         * Disabled default constructor
         */
		CounterKey() :
				_k1(), _k2(), _k3(), _k4() {}

		std::string _k1;
		std::string _k2;
		std::string _k3;
		std::string _k4;
	};

	class CounterData {
	public:
		CounterData(double a1, double a2 = 0, double b1 = 0, double b2 = 0) :
				_a1(a1), _a2(a2), _b1(b1), _b2(b2) {}

		CounterData(const CounterData& other) :
				_a1(other._a1), _a2(other._a2), _b1(other._b1), _b2(other._b2) {}

		CounterData& operator=(const CounterData& rhs) {
			_a1 = rhs._a1;
			_a2 = rhs._a2;
			_b1 = rhs._b1;
			_b2 = rhs._b2;
			return *this;
		}

		virtual ~CounterData() {}

		double a1() const { return _a1; }
		double a2() const { return _a2; }
		double b1() const { return _b1; }
		double b2() const { return _b2; }

		std::string toString() {
			std::stringstream ss;
			ss << ";a1=" << _a1
			   << ";a2=" << _a2
			   << ";b1=" << _b1
			   << ";b2=" << _b2;
			return ss.str();
		}

	private:
		/**
         * Disabled default constructor
         */
		CounterData() :
				_a1(0), _a2(0), _b1(0), _b2(0) {}

		double _a1;
		double _a2;
		double _b1;
		double _b2;
	};

	/**
	 * Default ctor
	 */
	MetricsAgent() {}

	/**
	 * dtor
	 */
	virtual ~MetricsAgent() {}

	/**
	 * Copy constructor
	 */
	MetricsAgent(const MetricsAgent& /* other */) {}

	/**
	 * Assigment operator
	 */
	MetricsAgent& operator=(const MetricsAgent& /* rhs */) { return *this; }

	virtual MetricsAgent& reportMetric(const std::string& counterName, const CounterKey& counterKey, const CounterData& counterData) = 0;

	virtual MetricsAgent& enable(bool doEnable = true) = 0;
};

class _declspec(dllexport) NullMetricsAgent : public MetricsAgent {
public:
	NullMetricsAgent& reportMetric(const std::string& /* counterName */
		,
		const CounterKey& /* counterKey */
		,
		const CounterData& /* counterData */) { return *this; }

	virtual NullMetricsAgent& enable(bool /*doEnable*/ = true) { return *this; }
};

/**
 * Plugin that provides a rest access to a simple Metrics API
 */
class _declspec(dllexport) MetricsPlugin
		: public AbstractPlugin
		,
		  public MetricsAgent {
public:
	MetricsPlugin();
	MetricsPlugin(const MetricsPlugin& other);
	virtual ~MetricsPlugin();

	MetricsPlugin& operator=(const MetricsPlugin& other);
	MetricsPlugin& setUp();
	MetricsPlugin& tearDown();

	virtual MetricsPlugin& reportMetric(const std::string& counterName, const CounterKey& counterKey, const CounterData& counterData);

	virtual MetricsPlugin& enable(bool doEnable = true);

	// configuration methods
	/**
	 * Stores the name of a MySqlConnector which will connect to the metrics store.
	 */
	MetricsPlugin& setReportingStore(const std::string& storeName);

	/**
	 * Set the MetricsAgent implementation to be used by this plugin
	 *
	 */
	MetricsPlugin& setImpl(MetricsAgent::MetricsAgentPtr impl);

private:
	MetricsAgent::MetricsAgentPtr _impl;
	URL identifyApplication();
};

class __declspec(dllexport) MetricsPluginFactory : public AbstractPluginFactory {
public:
	MetricsPluginFactory();
	MetricsPluginFactory(KEPConfig* config);
	virtual ~MetricsPluginFactory();

	const std::string& name() const;
	PluginPtr createPlugin(const TIXMLConfig::Item& config) const;

private:
	KEPConfig* _config;
};

} // namespace KEP