///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "Common\KEPUtil\Algorithms.h"
#include "Core\Util\Runnable.h"
#include "Common\include\KEPConfig.h"
#include "Core/Util/Threadpool.h"
#include "TinyXML/tinyxml.h"
#include <SimpleAmqpClient\SimpleAmqpClient.h>
#include <SimpleAmqpClient\AmqpLibraryException.h>
#include "MessageQueue.h"
#include "LogHelper.h"

using namespace std;

namespace KEP {

static LogInstance("Instance");

struct MessageQueueAgentInput {
	MessageQueueAgentInput(const string& queueName, const string& message, const bool durable) :
			_queueName(queueName), _message(message), _durable(durable) {}

	MessageQueueAgentInput(const MessageQueueAgentInput& other) :
			_queueName(other._queueName), _message(other._message), _durable(other._durable) {}

	MessageQueueAgentInput& operator=(const MessageQueueAgentInput& rhs) {
		_queueName = rhs._queueName;
		_message = rhs._message;
		_durable = rhs._durable;
		return *this;
	}

	string toString() {
		stringstream ss;
		ss << "MessageQueueAgentInput(" << this << ")queueName=" << _queueName << ";"
		   << "message=" << _message << ";"
		   << boolalpha << "durable=" << _durable;
		return ss.str();
	}

	const string& queueName() const {
		return _queueName;
	}
	const string& message() const {
		return _message;
	}
	const bool durable() const {
		return _durable;
	}

private:
	string _queueName;
	string _message;
	bool _durable;
};

/**
* A MessageQueueAgent implementation that serves as a proxy for a concrete MessageQueueAgent implementation.
* This concrete implementation can be local or remote.
*/
class MessageQueueAgentProxy
		: public MessageQueueAgent {
public:
	/**
	* Construct with a hostname that will be used to access the remote MessageQueueAgent
	*/
	MessageQueueAgentProxy(const string& hostName, const int port, const string& userName,
		const string& password, const string& vhost) :
			MessageQueueAgent(),
			_hostName(hostName), _port(port), _userName(userName), _password(password), _vhost(vhost), _threadPool(0) {}

	MessageQueueAgentProxy(const MessageQueueAgentProxy& other) :
			MessageQueueAgent(), _hostName(other._hostName), _port(other._port), _userName(other._userName), _password(other._password), _vhost(other._vhost), _threadPool(other._threadPool) {}

	ACTPtr publishMessageToQueue(const string& queueName, const string& message, const bool durable = false) {
		MessageQueueAgentInput input(queueName, message, durable);
		if (_threadPool == 0) {
			publishMessageToQueueImpl(input);
			ACTPtr act(new ACT(RunnablePtr(new NoopRunnable())));
			act->complete();
			return act;
		}

		return _threadPool->enqueue(RunnablePtr(new MessageQueueTask(*this, input)));
	}

	MessageQueueAgentProxy& setThreadPool(ThreadPool::ptr_t threadPool) {
		_threadPool = threadPool;
		return *this;
	}

	MessageQueueAgentProxy& enable(bool /* doEnable */ = true) {
		return *this;
	}

private:
	class MessageQueueTask
			: public Runnable {
	public:
		MessageQueueTask(MessageQueueAgentProxy& impl, MessageQueueAgentInput input) :
				_impl(impl), _input(input) {}

		RunResultPtr run() override {
			_impl.publishMessageToQueueImpl(_input);
			return RunResultPtr();
		}

	private:
		MessageQueueAgentProxy& _impl;
		MessageQueueAgentInput _input;
	};

	/**
	* This should be called only from the "publishMessageToQueueImpl" below.
	*/
	void handleAmqpConnectionException(const string& message, const exception& exc) {
		LogError(message << exc.what());
		_connection = nullptr;
	}

	/**
	* The business end of this Plugin.
	* @see MessageQueueAgent
	*/
	MessageQueueAgentProxy& publishMessageToQueueImpl(const MessageQueueAgentInput& input) {
		std::lock_guard<fast_recursive_mutex> lock(_queueSync);

		bool retry = false;
		int retryCount = 0;
		int retryLimit = 5;

		do {
			retry = false;

			// Connect to the message server if not already connected.
			try {
				if (_connection == NULL) {
					_connection = AmqpClient::Channel::Create(_hostName, _port, _userName, _password, _vhost);
				}

				AmqpClient::BasicMessage::ptr_t message = AmqpClient::BasicMessage::Create(input.message());
				if (_connection && message) {
					_connection->DeclareQueue(input.queueName(), false, input.durable(), false, false);
					_connection->BasicPublish("", input.queueName(), message);
				}
			} catch (const AmqpClient::ConnectionException& exc) {
				handleAmqpConnectionException("Amqp connection failed.", exc);
			} catch (const AmqpClient::ConnectionClosedException& exc) {
				handleAmqpConnectionException("Attemped to write to a closed Amqp connection.", exc);
			} catch (const AmqpClient::AmqpResponseLibraryException& exc) {
				handleAmqpConnectionException("A rabbitmq-c library error occurred. Resetting connection.", exc);
			} catch (const AmqpClient::AmqpLibraryException& exc) {
				// Add retry logic for TCP connection failure.
				retry = ++retryCount < retryLimit;
				_connection = nullptr;
				if (!retry) {
					handleAmqpConnectionException("Unable to send Amqp message. Resetting connection.", exc);
				}
			} catch (const exception& exc) {
				handleAmqpConnectionException("Unable to send Amqp message. Resetting connection.", exc);
			} catch (...) {
				LogError("Caught Exception");
			}
		} while (retry);

		return *this;
	}

	fast_recursive_mutex _queueSync;
	string _hostName;
	int _port;
	string _userName;
	string _password;
	string _vhost;
	ThreadPool::ptr_t _threadPool;
	AmqpClient::Channel::ptr_t _connection;
};

ACTPtr MessageQueuePlugin::publishMessageToQueue(const string& queueName, const string& message, const bool durable) {
	return _impl->publishMessageToQueue(queueName, message, durable);
}

MessageQueuePlugin& MessageQueuePlugin::enable(bool doEnable) {
	_impl->enable(doEnable);
	return *this;
}

MessageQueuePlugin::MessageQueuePlugin() :
		AbstractPlugin("MessageQueue"), _impl(MessageQueueAgent::MessageQueueAgentPtr(new NullMessageQueueAgent())) {}

MessageQueuePlugin::MessageQueuePlugin(const MessageQueuePlugin& other) :
		AbstractPlugin(other.name()), _impl(other._impl) {}

MessageQueuePlugin& MessageQueuePlugin::setImpl(MessageQueueAgent::MessageQueueAgentPtr impl) {
	_impl = impl;
	return *this;
}

MessageQueuePlugin::~MessageQueuePlugin() {}

MessageQueuePlugin& MessageQueuePlugin::operator=(const MessageQueuePlugin& /* other*/) {
	return *this;
}

MessageQueuePlugin& MessageQueuePlugin::setUp() {
	// Acquire reference to thread pool and add it to our implementation.
	// Note that we are assuming that the implementation is a MessageQueueAgentProxy.
	// When and if we add another implementation, this step will need to
	// be moved to the implementation (MessageQueueAgentProxy);
	//
	try {
		// ThreadPool* threadPool = AbstractPlugin::host()->DEPRECATED_getInterface<ThreadPool>("ThreadPool");
		ThreadPool::ptr_t threadPool = host()->getInterface<ThreadPool>("ThreadPool");
		if (threadPool == 0) {
			LogWarn("No thread pool available. Message queue publishing will occur on calling thread!");
		} else {
			((MessageQueueAgentProxy*)_impl.operator->())->setThreadPool(threadPool);
		}
	} catch (PluginNotFoundException exc) {
		LogWarn("Unable to find the thread pool plugin. Message queue publishing will occur on calling thread!");
	}

	return *this;
}

MessageQueuePlugin&
MessageQueuePlugin::tearDown() {
	return *this;
}

// Plugin support
MessageQueuePluginFactory::MessageQueuePluginFactory() :
		AbstractPluginFactory("MessageQueue"), _config(0) {}

MessageQueuePluginFactory::MessageQueuePluginFactory(KEPConfig* config) :
		AbstractPluginFactory("MessageQueue"), _config(config) {}

MessageQueuePluginFactory::~MessageQueuePluginFactory() {}

const string& MessageQueuePluginFactory::name() const {
	return AbstractPluginFactory::name();
}

PluginPtr MessageQueuePluginFactory::createPlugin(const TIXMLConfig::Item& configItem) const {
	TiXmlElement* config = configItem._internal;

	// Guard clause.  If no configuration provided, we can go with
	// default settings.  We're done here.
	//
	if (config == 0)
		return PluginPtr(new MessageQueuePlugin());

	const char* hostName = config->Attribute("message_queue_host");
	if (hostName == 0) {
		LogError("message_queue_host not specified in configuration");
		return PluginPtr(new MessageQueuePlugin());
	}

	const char* port = config->Attribute("message_queue_port");
	if (port == 0) {
		LogError("message_queue_port not specified in configuration");
		return PluginPtr(new MessageQueuePlugin());
	}
	int portNum = atoi(port);

	const char* mqUserName = config->Attribute("message_queue_user");
	if (mqUserName == 0) {
		LogError("message_queue_user not specified in configuration");
		return PluginPtr(new MessageQueuePlugin());
	}

	const char* mqUserPass = config->Attribute("message_queue_pass");
	if (mqUserPass == 0) {
		LogError("message_queue_pass not specified in configuration");
		return PluginPtr(new MessageQueuePlugin());
	}

	const char* mqVHost = config->Attribute("message_queue_vhost");
	if (mqVHost == 0) {
		LogError("message_queue_vhost not specified in configuration");
		return PluginPtr(new MessageQueuePlugin());
	}

	MessageQueueAgent::MessageQueueAgentPtr impl(new MessageQueueAgentProxy(hostName, portNum, mqUserName, mqUserPass, mqVHost));
	MessageQueuePlugin* pPlugin = new MessageQueuePlugin();
	pPlugin->setImpl(impl);
	return PluginPtr(pPlugin);
}

} // namespace KEP