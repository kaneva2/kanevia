///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/AsynchronousCompletionToken.h"
#include "Common/KEPUtil/Plugin.h"
#include "Common/KEPUtil/Heartbeat.h"
#include "Common/KEPUtil/URL.h"

namespace KEP {

CHAINED(MessageQueueException);

class _declspec(dllexport) MessageQueueAgent {
public:
	typedef std::shared_ptr<MessageQueueAgent> MessageQueueAgentPtr;

	/**
	* Default ctor
	*/
	MessageQueueAgent() {}

	/**
	* dtor
	*/
	virtual ~MessageQueueAgent() {}

	/**
	* Copy constructor
	*/
	MessageQueueAgent(const MessageQueueAgent& /* other */) {}

	/**
	* Assigment operator
	*/
	MessageQueueAgent& operator=(const MessageQueueAgent& /* rhs */) {
		return *this;
	}

	virtual ACTPtr publishMessageToQueue(const std::string& queueName, const std::string& message,
		const bool durable = false) = 0;

	virtual MessageQueueAgent& enable(bool doEnable = true) = 0;
};

class _declspec(dllexport) NullMessageQueueAgent
		: public MessageQueueAgent {
public:
	ACTPtr publishMessageToQueue(const std::string& /* queueName */, const std::string& /* message */,
		const bool /* durable */ = false) {
		ACTPtr act(new ACT(RunnablePtr(new NoopRunnable())));
		act->complete();
		return act;
	}

	virtual NullMessageQueueAgent& enable(bool /*doEnable*/ = true) {
		return *this;
	}
};

/**
* Plugin that provides access to a simple MessageQueue client API
*/
class _declspec(dllexport) MessageQueuePlugin
		: public AbstractPlugin,
		  public MessageQueueAgent {
public:
	MessageQueuePlugin();
	MessageQueuePlugin(const MessageQueuePlugin& other);
	virtual ~MessageQueuePlugin();

	MessageQueuePlugin& operator=(const MessageQueuePlugin& other);
	MessageQueuePlugin& setUp();
	MessageQueuePlugin& tearDown();

	/**
	* Publishes a string message to a message queue.
	*
	* @param queueName The name of the queue that we want to publish a message to.
	* @param message The message to be sent.
	* @param durable If True, the message should survive a MessageQueue restart. Default is false.
	* @returns true if a message was delivered, false if the queue was empty
	*/
	virtual ACTPtr publishMessageToQueue(const std::string& queueName, const std::string& message,
		const bool durable = false);

	virtual MessageQueuePlugin& enable(bool doEnable = true);

	/**
	* Set the MessageQueueAgent implementation to be used by this plugin
	*
	*/
	MessageQueuePlugin& setImpl(MessageQueueAgent::MessageQueueAgentPtr impl);

private:
	MessageQueueAgent::MessageQueueAgentPtr _impl;
	URL identifyApplication();
};

class __declspec(dllexport) MessageQueuePluginFactory
		: public AbstractPluginFactory {
public:
	MessageQueuePluginFactory();
	MessageQueuePluginFactory(KEPConfig* config);
	virtual ~MessageQueuePluginFactory();

	const std::string& name() const;
	PluginPtr createPlugin(const TIXMLConfig::Item& config) const;

private:
	KEPConfig* _config;
};

} // namespace KEP