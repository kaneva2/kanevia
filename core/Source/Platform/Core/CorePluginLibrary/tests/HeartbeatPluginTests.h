#pragma once
///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Common/KEPUtil/Heartbeat.h"
#include "Common/KEPUtil/Tests/PluginHostTestHelper.h"

class HeartbeatPluginTests : public PluginHostTests {
public:
	HeartbeatPluginTests() :
			PluginHostTests() {
		stringstream ss1;
		// This is going to become cumbersome.  Being able construct a template
		// and modify it will be a good thing.
		ss1 << "<Config>"
			<< "<PluginHost>"
			<< "<Libraries>"
			<< "<Library path=\"CorePluginLibrary.dll\"/>"
			<< "</Libraries>"
			<< "<Plugins>"
			<< "<Plugin factory_name=\"Heartbeat\">"
			<< "<PluginConfig>" // enabled=\"1\">"
			<< "<intervalMillis>3000</intervalMillis>"
			<< "</PluginConfig>"
			<< "</Plugin>"
			<< "</Plugins>"
			<< "</PluginHost>"
			<< "</Config>";
		addConfig("TestHost01", ss1.str());
	}

	virtual void SetUp() { PluginHostTests::SetUp(); }
	virtual void TearDown() { PluginHostTests::TearDown(); }
};

class TestListener : public HeartbeatListener {
	void thump() {}
};

// Disabled.  Doesn't really assert anything...except that it didn't fail
TEST_F(HeartbeatPluginTests, DISABLED_HeartbeatPluginTest01) {
	HeartbeatGeneratorPlugin& hgp = (HeartbeatGeneratorPlugin&)*((hosts.find("TestHost01")->second.second).getPlugin("Heartbeat"));
	TestListener* tl = new TestListener();
	hgp.addHeartbeatListener(tl);
}
