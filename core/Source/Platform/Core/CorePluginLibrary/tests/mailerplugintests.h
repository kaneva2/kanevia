#pragma once
///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <sstream>
#include "Common/KEPUtil/Plugin.h"
#include "Common/KEPUtil/PluginHostConfigurator.h"
#include "TinyXML/tinyxml.h"
#include "Common/KEPUtil/Mailer.h"
#include "Common/KEPUtil/SocketSubsystem.h"
#include "Common/KEPUtil/Tests/PluginHostTestHelper.h"

class MailerPluginTests : public PluginHostTests {
public:
	MailerPluginTests() :
			PluginHostTests() {
		stringstream ss1;
		// This is going to become cumbersome.  Being able construct a template
		// and modify it will be a good thing.
		ss1 << "<Config>"
			<< "<PluginHost>"
			<< "<Libraries>"
			<< "<Library path=\"CorePluginLibrary.dll\"/>"
			<< "</Libraries>"
			<< "<Plugins>"
			<< "<Plugin factory_name=\"Mailer\">"
			<< "<PluginConfig>" // enabled=\"1\">"
			<< "<defaults mail_host=\"mail.kanevia.com\" to=\"recipient@kanevia.com\" from=\"mailer@kanevia.com\" />"
			<< "</PluginConfig>"
			<< "</Plugin>"
			<< "</Plugins>"
			<< "</PluginHost>"
			<< "</Config>";
		addConfig("TestHost01", ss1.str());
	}

	virtual void SetUp() { PluginHostTests::SetUp(); }
	virtual void TearDown() { PluginHostTests::TearDown(); }
};

TEST(XMLDiscovery, EntityTest) {
}

TEST_F(MailerPluginTests, DISABLED_MailerPluginTest01) {
	// Mailer does use the socket subsystem...This will initialize the subsystem and clean it uup
	// when it goes off the stack.
	//
	SocketSubSystem ss;
	MailerPlugin& p = (MailerPlugin&)*((hosts.find("TestHost01")->second.second).getPlugin("Mailer"));
	p.send("test subject", "test body");
}

TEST_F(MailerPluginTests, DISABLED_FIXME_MailerPluginTest02) {
	// Mailer does use the socket subsystem...This will initialize the subsystem and clean it uup
	// when it goes off the stack.
	//
	SocketSubSystem ss;
	MailerPlugin& p = (MailerPlugin&)*((hosts.find("TestHost01")->second.second).getPlugin("Mailer"));
	ASSERT_TRUE(p.enabled());
	p.setEnabled(false);
	ASSERT_FALSE(p.enabled());
}
