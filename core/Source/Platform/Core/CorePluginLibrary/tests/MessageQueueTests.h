///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <sstream>
#include "Core/CorePluginLibrary/MessageQueue.h"
#include "Common/KEPUtil/Plugin.h"
#include "Common/KEPUtil/PluginHostConfigurator.h"
#include "TinyXML/tinyxml.h"
#include "Common/KEPUtil/Tests/PluginHostTestHelper.h"

class MessageQueuePluginTests : public PluginHostTests {
public:
	MessageQueuePluginTests() :
			PluginHostTests() {
		stringstream ss1;
		// This is going to become cumbersome.  Being able construct a template
		// and modify it will be a good thing.
		ss1 << "<Config>"
			<< "<PluginHost>"
			<< "<Libraries>"
#ifdef _DEBUG
			<< "<Library path=\"CorePluginLibraryd.dll\"/>"
#else
			<< "<Library path=\"CorePluginLibrary.dll\"/>"
#endif
			<< "</Libraries>"
			<< "<Plugins>"
			<< "<Plugin factory_name=\"ThreadPool\">"
			<< "<PluginConfig>"
			<< "<name>GeneralUse</name>"
			<< "<size>2</size>"
			<< "<priority>3</priority>"
			<< "</PluginConfig>"
			<< "</Plugin>"
			<< "<Plugin factory_name=\"MessageQueue\">"
			<< "<PluginConfig message_queue_host = \"dev-mq.kaneva.com\" message_queue_port=\"5672\" message_queue_user=\"testuser\" message_queue_pass=\"testpass\" message_queue_vhost=\"metrics\" />"
			<< "</Plugin>"
			<< "</Plugins>"
			<< "</PluginHost>"
			<< "</Config>";
		addConfig("TestHost01", ss1.str());
	}

	virtual void SetUp() { PluginHostTests::SetUp(); }
	virtual void TearDown() { PluginHostTests::TearDown(); }
};

//TEST(XMLDiscovery, EntityTest) {

//}
//
TEST_F(MessageQueuePluginTests, MessageQueuePluginTest01) {
	MessageQueuePlugin& p = (MessageQueuePlugin&)*((hosts.find("TestHost01")->second.second).getPlugin("MessageQueue"));
	ACTPtr ptr = p.publishMessageToQueue("testqueue", "test message");
	ptr->wait();
	ptr->isComplete();
}