#pragma once
///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "core/CorePluginLibrary/tests/heartbeatPluginTests.h"
#include "Core/CorePluginLibrary/tests/mailerplugintests.h"
#include "Core/CorePluginLibrary/tests/MessageQueueTests.h"