///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EquippableItemProperties.h"
#include "Common/include/CoreStatic/EquippableSystem.h"
#include "Common/include/CoreStatic/CArmedInventoryClass.h"
#include "Common/include/CoreStatic/CSkeletonObject.h"
#include "Common/include/CoreStatic/RuntimeSkeleton.h"
#include "Common/include/CoreStatic/CBoneClass.h"
#include "Common/include/CoreStatic/CHierarchyMesh.h"
#include "Common/include/CoreStatic/CEXMeshClass.h"
#include "Common/include/CoreStatic/CMaterialClass.h"
#include "Common/include/CoreStatic\TextureObj.h"
#include "Common/include/IClientEngine.h"
#include "Common/include/TextureDef.h"
#include "SkeletonConfiguration.h"
#include "CDeformableMesh.h"
#include "ResourceManagers/ReMeshCreate.h"
#include "RenderEngine/ReSkinMesh.h"
#include "../UGC_Global.h"

namespace KEP {

EquippableItemProperties::EquippableItemProperties(const CArmedInventoryProxy* pProxy, const BoundBox& transformedBox) :
		meshCount(0),
		vertexCount(0),
		triangleCount(0),
		materialCount(0),
		textureCount(0), // Note that unlike vtxcnt or tricnt, texture counts are accumulated for all LODs
		distinctTextureCount(0),
		boneCount(0),
		actorType(0), // TBD
		actorGLID(0), // TBD
		exclusionGroupID(0),
		boundBoxInActorSpace(transformedBox) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	memset(textures, 0, sizeof(textures));
	boundBox.invalidate();

	ASSERT(pProxy != NULL && pProxy->GetInventoryObj() != NULL);
	if (pProxy != NULL && pProxy->GetInventoryObj() != NULL) {
		CArmedInventoryObj* pEquip = pProxy->GetInventoryObj();
		auto pRuntimeSkeleton = pEquip->getSkeleton(); // Only handle third person system for now
		auto pSkeletonConfig = pEquip->getSkeletonConfig();

		ASSERT(pRuntimeSkeleton != NULL);
		if (pRuntimeSkeleton == NULL) {
			return;
		}

		boneCount = pRuntimeSkeleton->getBoneCount();
		actorType = pEquip->getAbilityTypeUse();
		exclusionGroupID = pEquip->getLocationOccupancy();

		boundBox = pRuntimeSkeleton->getLocalBoundingBox();

		// Scale skeleton boundbox with EI scaling factor (pivot at origin)
		boundBox.minX *= pEquip->getScale().x;
		boundBox.minY *= pEquip->getScale().y;
		boundBox.minZ *= pEquip->getScale().z;
		boundBox.maxX *= pEquip->getScale().x;
		boundBox.maxY *= pEquip->getScale().y;
		boundBox.maxZ *= pEquip->getScale().z;

		// Temp store for all textures found
		std::set<DWORD> texturesFound;

		// NOTE (03102010): currently triangle count obtained here matches original design (DAE)
		// It's because we only use rigid mesh (hence CEXMesh without tristripping) and we didn't
		// call OptimzeMesh. It would be better to let ColladaImporter to provide total triangle count
		// and use it for validation. Otherwise we will see problems once we add deformable mesh
		// and/or optimze mesh.

		// stat rigid meshes
		for (UINT boneLoop = 0; boneLoop < pRuntimeSkeleton->getBoneCount(); boneLoop++) {
			CBoneObject* pBone = pRuntimeSkeleton->getBoneByIndex(boneLoop);
			meshCount += pBone->m_rigidMeshes.size();

			for (auto it = pBone->m_rigidMeshes.begin(); it != pBone->m_rigidMeshes.end(); ++it) {
				CHierarchyMesh* pHMesh = *it;

				UINT visVCount = 0;
				UINT visTCount = 0;
				bool visHasMaterial = false;

				for (int lodLoop = 0; lodLoop < pHMesh->m_lodChain->GetCount(); ++lodLoop) {
					CHierarchyVisObj* pHVis = pHMesh->GetByIndex(lodLoop);
					ReMesh* rm = pHVis->GetReMesh();
					ASSERT(rm != NULL);

					ReMeshData* rmData = NULL;
					rm->Access(&rmData);
					ASSERT(rmData != NULL);

					if (rmData->NumI > visVCount) {
						visVCount = rmData->NumI;
					}

					CMaterialObject* pMaterial = &pHVis->m_mesh->GetMaterial();
					if (pMaterial) {
						visHasMaterial = true;
						for (int texLoop = 0; texLoop < NUM_CMATERIAL_TEXTURES; texLoop++) {
							DWORD texNameHash = pMaterial->m_texNameHash[texLoop];
							if (texNameHash != -1 && texNameHash != 0) {
								if (texturesFound.find(texNameHash) == texturesFound.end()) {
									texturesFound.insert(texNameHash);
									distinctTextureCount++;
								}

								textureCount++;
							}
						}
					}
				}

				visTCount = visVCount / 3;

				vertexCount += visVCount;
				triangleCount += visTCount;
				if (visHasMaterial)
					materialCount++;
			}
		}

		// stat skinned meshes
		if (pSkeletonConfig != nullptr && pSkeletonConfig->m_meshSlots != nullptr) {
			for (POSITION dbPos = pSkeletonConfig->m_meshSlots->GetHeadPosition(); dbPos != NULL;) {
				UINT visVCount = 0;
				UINT visTCount = 0;
				bool visHasMaterial = false;

				CAlterUnitDBObject* aPtr = static_cast<CAlterUnitDBObject*>(pSkeletonConfig->m_meshSlots->GetNext(dbPos));
				POSITION dMeshPos = aPtr->m_configurations->FindIndex(0); // There should only be one configuration for UGC currently
				CDeformableMesh* dMesh = static_cast<CDeformableMesh*>(aPtr->m_configurations->GetNext(dMeshPos));

				for (unsigned int i = 0; i < dMesh->m_lodCount; i++) {
					ReMesh* rm = MeshRM::Instance()->FindMesh(dMesh->m_lodHashes[i], MeshRM::RMP_ID_PERSISTENT);
					if (rm == NULL) {
						rm = (ReSkinMesh*)MeshRM::Instance()->FindMesh(dMesh->m_lodHashes[i], MeshRM::RMP_ID_DYNAMIC);
					}
					ASSERT(rm != NULL);

					ReMeshData* rmData = NULL;
					rm->Access(&rmData);
					ASSERT(rmData != NULL);
					if (rmData->NumI > visVCount) {
						visVCount = rmData->NumI;
					}
				}

				visTCount = visVCount / 3;
				vertexCount += visVCount;
				triangleCount += visTCount;
				if (visHasMaterial) {
					materialCount++;
				}

				if (dMesh->m_supportedMaterials && dMesh->m_supportedMaterials->GetCount() > 0) {
					POSITION matPos = dMesh->m_supportedMaterials->FindIndex(0);
					CMaterialObject* pMaterial = static_cast<CMaterialObject*>(dMesh->m_supportedMaterials->GetNext(matPos));
					if (pMaterial) {
						visHasMaterial = true;
						for (int texLoop = 0; texLoop < NUM_CMATERIAL_TEXTURES; texLoop++) {
							DWORD texNameHash = pMaterial->m_texNameHash[texLoop];
							if (texNameHash != -1 && texNameHash != 0) {
								if (texturesFound.find(texNameHash) == texturesFound.end()) {
									texturesFound.insert(texNameHash);
									distinctTextureCount++;
								}

								textureCount++;
							}
						}
					}
				}
			}
		}

		// Collect information for textures
		int texTrace = 0;
		for (const auto& texNameHash : texturesFound) {
			textures[texTrace].nameHash = texNameHash;

			CTextureEX* pTex = pICE->GetTextureFromDatabase(texNameHash);
			if (pTex) {
				//Warning: width/height not available until CreateResource is called on CTextureEX
				//This is OK for current validation needs but must take great cautious here if client
				//validation on textures is needed in the future
				textures[texTrace].width = pTex->GetWidth();
				textures[texTrace].height = pTex->GetHeight();

				TextureDef textureInfo;
				if (pICE->GetTextureImportInfo(pTex->GetFileName(), textureInfo)) {
					textures[texTrace].opacity = textureInfo.opacity;
					textures[texTrace].averageColor = textureInfo.averageColor;
				} else {
					ASSERT(false);
				}
			}

			texTrace++;
		}
	}

	boundBox.validate();
}

VLD_VALUE_TYPE EquippableItemProperties::getType(int propId) const {
	switch (PROPID_BASE(propId)) {
		case PROP_MESH_CNT:
		case PROP_VERTEX_CNT:
		case PROP_TRIANGLE_CNT:
		case PROP_MATERIAL_CNT:
		case PROP_TEXTURE_CNT:
		case PROP_MESH_VERTEX_CNT:
		case PROP_MESH_TRIANGLE_CNT:
		case PROP_MESH_MATERIAL_CNT:
		case PROP_MESH_TEXTURE_CNT:
		case PROP_IMAGE_FILESIZE:
		case PROP_IMAGE_PIXEL_CNT:
		case PROP_IMAGE_WIDTH:
		case PROP_IMAGE_HEIGHT:
		case PROP_IMAGE_DEPTH:
		case PROP_IMAGE_OPACITY:
		case PROP_SKELETON_BONE_COUNT:
		case PROP_TARGET_ACTOR_TYPE:
		case PROP_TARGET_ACTOR_GLID:
			return VVT_INT;

		case PROP_BOUNDBOX_SIZEX:
		case PROP_BOUNDBOX_SIZEY:
		case PROP_BOUNDBOX_SIZEZ:
		case PROP_BOUNDBOX_MINX:
		case PROP_BOUNDBOX_MAXX:
		case PROP_BOUNDBOX_MINY:
		case PROP_BOUNDBOX_MAXY:
		case PROP_BOUNDBOX_MINZ:
		case PROP_BOUNDBOX_MAXZ:
		case PROP_BOUNDBOX_VOLUME:
		case PROP_BOUNDBOX_XYZSUM:
		case PROP_MESH_BOUNDBOX_SIZEX:
		case PROP_MESH_BOUNDBOX_SIZEY:
		case PROP_MESH_BOUNDBOX_SIZEZ:
		case PROP_MESH_BOUNDBOX_VOLUME:
		case PROP_MESH_BOUNDBOX_MINX:
		case PROP_MESH_BOUNDBOX_MAXX:
		case PROP_MESH_BOUNDBOX_MINY:
		case PROP_MESH_BOUNDBOX_MAXY:
		case PROP_MESH_BOUNDBOX_MINZ:
		case PROP_MESH_BOUNDBOX_MAXZ:
		case PROP_MESH_BOUNDBOX_XYZSUM:
		case PROP_BOUNDBOXWACTOR_SIZEX:
		case PROP_BOUNDBOXWACTOR_SIZEY:
		case PROP_BOUNDBOXWACTOR_SIZEZ:
		case PROP_BOUNDBOXWACTOR_VOLUME:
		case PROP_BOUNDBOXWACTOR_MINX:
		case PROP_BOUNDBOXWACTOR_MINY:
		case PROP_BOUNDBOXWACTOR_MINZ:
		case PROP_BOUNDBOXWACTOR_MAXX:
		case PROP_BOUNDBOXWACTOR_MAXY:
		case PROP_BOUNDBOXWACTOR_MAXZ:
		case PROP_BOUNDBOXWACTOR_XYZSUM:
			return VVT_FLOAT;

		case PROP_EXCLUSION_GROUPS:
			return VVT_INT; // VVT_STRING if we want to support multi-groups (in CSV format)

		case PROP_IMAGE_AVG_COLOR: // Unsigned 32-bit value. Use string for now
		case PROP_IMAGE_ID: // Unsigned 32-bit value. Use string for now
			return VVT_STRING;
	}

	return VVT_UNDEFINED;
}

Value EquippableItemProperties::get(int propId) const {
	int propIndex = PROPID_INDEX(propId);

	switch (PROPID_BASE(propId)) {
		case PROP_MESH_CNT:
			return Value(meshCount);
		case PROP_VERTEX_CNT:
			return Value(vertexCount);
		case PROP_TRIANGLE_CNT:
			return Value(triangleCount);
		case PROP_BOUNDBOX_SIZEX:
			return Value(boundBox.maxX - boundBox.minX);
		case PROP_BOUNDBOX_SIZEY:
			return Value(boundBox.maxY - boundBox.minY);
		case PROP_BOUNDBOX_SIZEZ:
			return Value(boundBox.maxZ - boundBox.minZ);
		case PROP_MATERIAL_CNT:
			return Value(materialCount);
		case PROP_TEXTURE_CNT:
			return Value(textureCount);
		case PROP_DISTINCT_TEXTURE_CNT:
			return Value(distinctTextureCount);
		case PROP_BOUNDBOX_VOLUME:
			return Value(abs((boundBox.maxX - boundBox.minX) * (boundBox.maxY - boundBox.minY) * (boundBox.maxZ - boundBox.minZ)));
		case PROP_BOUNDBOX_MINX:
			return Value(boundBox.minX);
		case PROP_BOUNDBOX_MAXX:
			return Value(boundBox.maxX);
		case PROP_BOUNDBOX_MINY:
			return Value(boundBox.minY);
		case PROP_BOUNDBOX_MAXY:
			return Value(boundBox.maxY);
		case PROP_BOUNDBOX_MINZ:
			return Value(boundBox.minZ);
		case PROP_BOUNDBOX_MAXZ:
			return Value(boundBox.maxZ);
		case PROP_BOUNDBOX_XYZSUM:
			return Value(abs(boundBox.maxX - boundBox.minX) + abs(boundBox.maxY - boundBox.minY) + abs(boundBox.maxZ - boundBox.minZ));
		case PROP_IMAGE_WIDTH:
			if (propIndex < distinctTextureCount)
				return Value(textures[propIndex].width);
			break;
		case PROP_IMAGE_HEIGHT:
			if (propIndex < distinctTextureCount)
				return Value(textures[propIndex].height);
			break;
		case PROP_IMAGE_OPACITY:
			if (propIndex < distinctTextureCount)
				return Value((int)textures[propIndex].opacity);
			break;
		case PROP_IMAGE_AVG_COLOR:
			if (propIndex < distinctTextureCount)
				return Value(std::to_string(textures[propIndex].averageColor));
			break;
		case PROP_IMAGE_ID:
			if (propIndex < distinctTextureCount)
				return Value(std::to_string(textures[propIndex].nameHash));
			break;
		case PROP_SKELETON_BONE_COUNT:
			return Value(boneCount);
		case PROP_TARGET_ACTOR_TYPE:
			return Value(actorType);
		case PROP_TARGET_ACTOR_GLID:
			return Value(actorGLID);

		case PROP_BOUNDBOXWACTOR_SIZEX:
			return Value(boundBoxInActorSpace.maxX - boundBoxInActorSpace.minX);
		case PROP_BOUNDBOXWACTOR_SIZEY:
			return Value(boundBoxInActorSpace.maxY - boundBoxInActorSpace.minY);
		case PROP_BOUNDBOXWACTOR_SIZEZ:
			return Value(boundBoxInActorSpace.maxZ - boundBoxInActorSpace.minZ);
		case PROP_BOUNDBOXWACTOR_VOLUME:
			return Value(abs((boundBoxInActorSpace.maxX - boundBoxInActorSpace.minX) * (boundBoxInActorSpace.maxY - boundBoxInActorSpace.minY) * (boundBoxInActorSpace.maxZ - boundBoxInActorSpace.minZ)));
		case PROP_BOUNDBOXWACTOR_MINX:
			return Value(boundBoxInActorSpace.minX);
		case PROP_BOUNDBOXWACTOR_MINY:
			return Value(boundBoxInActorSpace.minY);
		case PROP_BOUNDBOXWACTOR_MINZ:
			return Value(boundBoxInActorSpace.minZ);
		case PROP_BOUNDBOXWACTOR_MAXX:
			return Value(boundBoxInActorSpace.maxX);
		case PROP_BOUNDBOXWACTOR_MAXY:
			return Value(boundBoxInActorSpace.maxY);
		case PROP_BOUNDBOXWACTOR_MAXZ:
			return Value(boundBoxInActorSpace.maxZ);
		case PROP_BOUNDBOXWACTOR_XYZSUM:
			return Value(abs(boundBoxInActorSpace.maxX - boundBoxInActorSpace.minX) + abs(boundBoxInActorSpace.maxY - boundBoxInActorSpace.minY) + abs(boundBoxInActorSpace.maxZ - boundBoxInActorSpace.minZ));

		case PROP_EXCLUSION_GROUPS:
			return Value(exclusionGroupID);
	}

	return Value(VVT_ERROR);
}

void EquippableItemProperties::getAllSupportedPropertyIds(std::vector<int>& allPropIds) {
	allPropIds.clear();
	allPropIds.push_back(PROP_MESH_CNT);
	allPropIds.push_back(PROP_VERTEX_CNT);
	allPropIds.push_back(PROP_TRIANGLE_CNT);
	allPropIds.push_back(PROP_BOUNDBOX_SIZEX);
	allPropIds.push_back(PROP_BOUNDBOX_SIZEY);
	allPropIds.push_back(PROP_BOUNDBOX_SIZEZ);
	allPropIds.push_back(PROP_MATERIAL_CNT);
	allPropIds.push_back(PROP_TEXTURE_CNT);
	allPropIds.push_back(PROP_DISTINCT_TEXTURE_CNT);
	allPropIds.push_back(PROP_BOUNDBOX_VOLUME);
	allPropIds.push_back(PROP_BOUNDBOX_MINX);
	allPropIds.push_back(PROP_BOUNDBOX_MINY);
	allPropIds.push_back(PROP_BOUNDBOX_MINZ);
	allPropIds.push_back(PROP_BOUNDBOX_MAXX);
	allPropIds.push_back(PROP_BOUNDBOX_MAXY);
	allPropIds.push_back(PROP_BOUNDBOX_MAXZ);
	allPropIds.push_back(PROP_BOUNDBOX_XYZSUM);
	for (int i = 0; i < distinctTextureCount; i++) {
		allPropIds.push_back(MAKE_PROPID(PROP_IMAGE_WIDTH, i));
		allPropIds.push_back(MAKE_PROPID(PROP_IMAGE_HEIGHT, i));
		allPropIds.push_back(MAKE_PROPID(PROP_IMAGE_OPACITY, i));
		allPropIds.push_back(MAKE_PROPID(PROP_IMAGE_AVG_COLOR, i));
		allPropIds.push_back(MAKE_PROPID(PROP_IMAGE_ID, i));
	}
	allPropIds.push_back(PROP_SKELETON_BONE_COUNT);
	allPropIds.push_back(PROP_TARGET_ACTOR_TYPE);
	allPropIds.push_back(PROP_TARGET_ACTOR_GLID);

	allPropIds.push_back(PROP_BOUNDBOXWACTOR_SIZEX);
	allPropIds.push_back(PROP_BOUNDBOXWACTOR_SIZEY);
	allPropIds.push_back(PROP_BOUNDBOXWACTOR_SIZEZ);
	allPropIds.push_back(PROP_BOUNDBOXWACTOR_VOLUME);
	allPropIds.push_back(PROP_BOUNDBOXWACTOR_MINX);
	allPropIds.push_back(PROP_BOUNDBOXWACTOR_MINY);
	allPropIds.push_back(PROP_BOUNDBOXWACTOR_MINZ);
	allPropIds.push_back(PROP_BOUNDBOXWACTOR_MAXX);
	allPropIds.push_back(PROP_BOUNDBOXWACTOR_MAXY);
	allPropIds.push_back(PROP_BOUNDBOXWACTOR_MAXZ);
	allPropIds.push_back(PROP_BOUNDBOXWACTOR_XYZSUM);

	allPropIds.push_back(PROP_EXCLUSION_GROUPS);
}

} // namespace KEP