///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../UGCImport.h"

namespace KEP {

class EquippableObjectImport : public IUGCImport {
public:
	IUGCImportStateObj* createState();
	virtual void disposeState(IUGCImportStateObj* state) override;

	virtual bool preview(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) override;
	virtual bool import(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) override;
	virtual bool convert(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) override;
	virtual bool process(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) override;
	virtual bool commit(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) override;
	virtual bool cleanup(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) override;
};

} // namespace KEP