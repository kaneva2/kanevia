///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "EquippableObjectImport.h"
#include "../UGCImportStateObserver.h"
#include "GetSet.h"
#include "Common/include/IAssetsDatabase.h"
#include "../UGCManager.h"
#include "../UGCItemUploadState.h"
#include "AssetTree.h"
#include "Common/include/KEPConstants.h"
#include "Common/include/CoreStatic/CSkeletonObject.h"
#include "Common/include/CoreStatic/RuntimeSkeleton.h"
#include "Common/include/CoreStatic/SkeletonConfiguration.h"
#include "CMovementObj.h"
#include "Common/include/CoreStatic/CArmedInventoryClass.h"
#include "Common/include/CoreStatic/EquippableSystem.h"
#include "../SkeletalObjectImportState.h"
#include "Event/Glue/EquippableImportStateIds.h"
#include "Common/include/CoreItems.h"
#include "EquippableItemProperties.h"
#include "LogHelper.h"

namespace KEP {

static LogInstance("UGCImport");

static ClassifyAssetNodeStd classifyFunc;

class EquippableImportState : public SkeletalObjectImportState, public ISOPlayerItem, public GetSet {
public:
	enum { EOT_UNKNOWN = -1,
		EOT_RIGID = 0,
		EOT_SKELETAL = 2 };

	AssetTree* m_assets;
	int m_objectType; // EOT_UNKNOWN, EOT_RIGID or EOT_SKELETAL
	std::string m_actorMountBone;
	Vector3f m_position;
	Vector3f m_rotation;
	bool m_normalizeNormal;
	int m_exclusionGroup;
	BoundBox m_boundBoxInActorSpace;

	EquippableImportState() :
			m_assets(NULL), m_objectType(EOT_UNKNOWN), m_normalizeNormal(false), m_exclusionGroup(999), m_position(Vector3f::Zero()), m_rotation(Vector3f::Zero()) {
	}

	~EquippableImportState() {
		if (m_assets)
			delete m_assets;
		m_assets = NULL;
	}

	virtual IGetSet* Values() {
		return this;
	}

	virtual int getImportType() {
		switch (m_objectType) {
			case EOT_RIGID:
				return UGC_TYPE_EQUIPPABLE_RIGID;
			case EOT_SKELETAL:
				return UGC_TYPE_EQUIPPABLE_SKELETAL;
			default:
				return UGC_TYPE_EQUIPPABLE;
		}
	}

	virtual PropertySource* getPropertySource(bool bRefresh) {
		assert(false);
		return NULL;
	}

	virtual void SetString(ULONG index, const char* s) {
		GetSet::SetString(index, s);

		if (index == EQUIPPABLEIMPORTSTATEIDS_ACTORMOUNTBONE) {
			UpdateMountBone();
		}
	}

#define IMPL_SETNUMBER_FUNC(_TYPE_)                           \
	void SetNumber(ULONG index, _TYPE_ value) {               \
		switch (index) {                                      \
			case EQUIPPABLEIMPORTSTATEIDS_AUTOCORRECTAMBIENT: \
				GetSet::SetNumber(index, value);              \
				UpdateMaterial(false);                        \
				break;                                        \
			case EQUIPPABLEIMPORTSTATEIDS_UPDATEMATRIX:       \
				UpdateMatrix();                               \
				break;                                        \
			case EQUIPPABLEIMPORTSTATEIDS_TARGETACTORTYPE:    \
				GetSet::SetNumber(index, value);              \
				UpdateActorType();                            \
				break;                                        \
			case EQUIPPABLEIMPORTSTATEIDS_EXCLUSIONGROUP:     \
				GetSet::SetNumber(index, value);              \
				UpdateExclusionGroup();                       \
				break;                                        \
			default:                                          \
				GetSet::SetNumber(index, value);              \
				break;                                        \
		}                                                     \
	}

	IMPL_SETNUMBER_FUNC(ULONG);
	IMPL_SETNUMBER_FUNC(LONG);
	IMPL_SETNUMBER_FUNC(double);
	IMPL_SETNUMBER_FUNC(BOOL);
	IMPL_SETNUMBER_FUNC(bool);

	void UpdateMountBone() {
		auto pICE = IClientEngine::Instance();
		if (!pICE)
			return;

		CMovementObj* pMO_target = GetTargetActor(); // get target actor based on current NPC
		int mountBoneIndex = pMO_target->getSkeleton()->getBoneIndexByName(m_actorMountBone);
		if (mountBoneIndex == -1)
			mountBoneIndex = 0;

		// Update main item
		if (m_localGLID != GLID_INVALID) {
			CArmedInventoryProxy* pAIP = EquippableRM::Instance()->GetProxy(m_localGLID, false);
			if (pAIP) {
				CArmedInventoryObj* pAIO = pAIP->GetInventoryObj();
				if (pAIO) {
					pAIO->setAttachedBone(mountBoneIndex, m_actorMountBone);
					UpdateBoundBoxInActorSpace(pAIO);
				}
			}
		}

		// Also update item on current player if armed
		auto pMO = dynamic_cast<CMovementObj*>(pICE->GetMovementObjectMeAsGetSet());
		if (pMO) {
			UpdateMountBoneForActor(pMO, mountBoneIndex);
			if (pMO->getPosses() != NULL && pMO->getMountType() == MT_STUNT)
				UpdateMountBoneForActor(pMO->getPosses(), mountBoneIndex);
		}
	}

	void UpdateExclusionGroup() {
		// Update main item
		if (m_localGLID != GLID_INVALID) {
			CArmedInventoryProxy* pAIP = EquippableRM::Instance()->GetProxy(m_localGLID, false);
			if (pAIP) {
				CArmedInventoryObj* pAIO = pAIP->GetInventoryObj();
				if (pAIO)
					pAIO->setLocationOccupancy(m_exclusionGroup);
			}
		}
	}

	void UpdateMountBoneForActor(CMovementObj* pMO, int mountBoneIndex) {
		if (!pMO || !pMO->getSkeleton())
			return;
		pMO->getSkeleton()->updateEquippableItemBoneAttachment(m_localGLID, mountBoneIndex, m_actorMountBone);
	}

	void UpdateMaterial(bool bFinalize = false) {
		auto pICE = IClientEngine::Instance();
		if (!pICE)
			return;

		// Update main item
		if (m_localGLID != GLID_INVALID) {
			CArmedInventoryProxy* pAIP = EquippableRM::Instance()->GetProxy(m_localGLID, false);
			if (pAIP) {
				CArmedInventoryObj* pAIO = pAIP->GetInventoryObj();
				if (pAIO)
					UpdateEquippableObjectMaterial(pAIO, bFinalize);
			}
		}

		// Also update item on current player if armed
		auto pMO = dynamic_cast<CMovementObj*>(pICE->GetMovementObjectMeAsGetSet());
		if (pMO) {
			UpdateEquippableObjectMaterialForActor(pMO, bFinalize);
			if (pMO->getPosses() && pMO->getMountType() == MT_STUNT)
				UpdateEquippableObjectMaterialForActor(pMO->getPosses(), bFinalize);
		}
	}

	void UpdateEquippableObjectMaterialForActor(CMovementObj* pMO, bool bFinalize = false) {
		if (!pMO || !pMO->getSkeleton())
			return;
		auto pAIO = pMO->getSkeleton()->getEquippableItemByGLID(m_localGLID);
		if (pAIO)
			UpdateEquippableObjectMaterial(pAIO, bFinalize);
	}

	void UpdateEquippableObjectMaterial(CArmedInventoryObj* pAIO, bool bFinalize = false) {
		RuntimeSkeleton* pSkeleton;

		pSkeleton = pAIO->getSkeleton(eVisualType::ThirdPerson);
		if (pSkeleton)
			ProcessSkeletonMaterials(pSkeleton, pAIO->getSkeletonConfig(eVisualType::ThirdPerson), bFinalize);

		pSkeleton = pAIO->getSkeleton(eVisualType::FirstPerson);
		if (pSkeleton)
			ProcessSkeletonMaterials(pSkeleton, pAIO->getSkeletonConfig(eVisualType::FirstPerson), bFinalize);
	}

	void UpdateMatrix() {
		auto pICE = IClientEngine::Instance();
		if (!pICE)
			return;

		// Update matrix for main item
		if (m_localGLID != GLID_INVALID) {
			CArmedInventoryProxy* pAIP = EquippableRM::Instance()->GetProxy(m_localGLID, false);
			if (pAIP) {
				CArmedInventoryObj* pAIO = pAIP->GetInventoryObj();
				if (pAIO) {
					UpdateEquippableObjectMatrix(pAIO);
					UpdateBoundBoxInActorSpace(pAIO);
				}
			}
		}

		// Also update item on current player if armed
		auto pMO = dynamic_cast<CMovementObj*>(pICE->GetMovementObjectMeAsGetSet());
		if (pMO) {
			UpdateEquippableObjectMatrixForActor(pMO);
			if (pMO->getPosses() && pMO->getMountType() == MT_STUNT)
				UpdateEquippableObjectMatrixForActor(pMO->getPosses());
		}
	}

	void UpdateEquippableObjectMatrixForActor(CMovementObj* pMO) {
		if (!pMO || !pMO->getSkeleton())
			return;
		auto pAIO = pMO->getSkeleton()->getEquippableItemByGLID(m_localGLID);
		if (pAIO)
			UpdateEquippableObjectMatrix(pAIO);
	}

	void UpdateEquippableObjectMatrix(CArmedInventoryObj* pAIO) {
		if (!pAIO)
			return;
		auto pICE = IClientEngine::Instance();
		if (!pICE)
			return;
		pAIO->setTranslation(m_position);
		pAIO->setRotationZXY(ToRadians(m_rotation));
		pAIO->setScale(Vector3f(m_scaleFactor));
		if (m_scaleFactor != 1.0f) {
			m_normalizeNormal = true;
			pICE->GetD3DDevice()->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);
		} else if (m_scaleFactor == 1.0f && m_normalizeNormal) {
			m_normalizeNormal = false;
			pICE->GetD3DDevice()->SetRenderState(D3DRS_NORMALIZENORMALS, FALSE);
		}
	}

	void UpdateActorType() {
		if (m_localGLID != GLID_INVALID) {
			CArmedInventoryProxy* pAIP = EquippableRM::Instance()->GetProxy(m_localGLID, false);
			if (pAIP) {
				CArmedInventoryObj* pAIO = pAIP->GetInventoryObj();
				if (pAIO) {
					pAIO->setAbilityTypeUse(m_targetActorType);
					UpdateBoundBoxInActorSpace(pAIO);
				}
			}
		}
	}

	void UpdateBoundBoxInActorSpace(CArmedInventoryObj* pAIO) {
		auto pICE = IClientEngine::Instance();
		if (!pICE)
			return;

		// Get mount bone matrix
		int mountPostureGLID = GLID_RESTING_POSE_MALE;
		if (m_targetActorType == ACTOR_TYPE_FEMALE) {
			mountPostureGLID = GLID_RESTING_POSE_FEMALE;
		}

		// Before proceeding, we want to make sure requested animation data is loaded and mount slot is set
		// m_actorMountBone might not have been assigned at initialization
		auto pGS_me = pICE->GetMovementObjectMeAsGetSet();
		if (!pGS_me || !pICE->LoadAnimationOnPlayer(pGS_me, mountPostureGLID) || m_actorMountBone.empty()) {
			// need to validate when there is no anim
			m_boundBoxInActorSpace.maxX = 0.25f;
			m_boundBoxInActorSpace.maxY = 0.25f;
			m_boundBoxInActorSpace.maxZ = 0.25f;
			m_boundBoxInActorSpace.minX = .5f;
			m_boundBoxInActorSpace.minY = .5f;
			m_boundBoxInActorSpace.minZ = .5f;
			return;
		}

		std::map<std::string, Matrix44f> mountPosture;
		pICE->GetPostureMatrices(mountPostureGLID, (TimeMs)0.0, mountPosture);
		assert(mountPosture.find(m_actorMountBone) != mountPosture.end());
		Matrix44f& mountBoneMatrix = mountPosture[m_actorMountBone];
		Matrix44f mountMatrix;
		if (pAIO->getRelativeMatrix() != NULL) {
			mountMatrix = *pAIO->getRelativeMatrix() * mountBoneMatrix;
		} else {
			mountMatrix = mountBoneMatrix;
		}

		// NOTE: pEquip->getRelativeMatrix already contains scaling component, don't scale it again here
		BoundBox boundBoxLocal(m_boundBoxMin.x, m_boundBoxMin.y, m_boundBoxMin.z, m_boundBoxMax.x, m_boundBoxMax.y, m_boundBoxMax.z);

		float boundBoxVertices[8][3];
		boundBoxLocal.getVertices(boundBoxVertices);

		m_boundBoxInActorSpace.invalidate();
		for (int vtxLoop = 0; vtxLoop < 8; vtxLoop++) {
			Vector4f v = TransformPointToHomogeneousVector(mountMatrix, (Vector3f&)boundBoxVertices[vtxLoop][0]);
			m_boundBoxInActorSpace.merge(v.x, v.y, v.z);
		}
		m_boundBoxInActorSpace.validate();
	}

	CMovementObj* GetTargetActor() const {
		if (m_pTargetMovObj == NULL) {
			return NULL;
		}

		// If NPC mounted, use NPC
		if (m_pTargetMovObj->getPosses() != NULL && m_pTargetMovObj->getMountType() == MT_STUNT) {
			return m_pTargetMovObj->getPosses();
		}

		return m_pTargetMovObj;
	}

	DECLARE_GETSET
};

IUGCImportStateObj* EquippableObjectImport::createState() {
	return new EquippableImportState();
}

void EquippableObjectImport::disposeState(IUGCImportStateObj* state) {
	EquippableImportState* pStateObj = dynamic_cast<EquippableImportState*>(state);
	VERIFY_RETVOID(pStateObj != NULL);

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	if (pStateObj->m_normalizeNormal) {
		pICE->GetD3DDevice()->SetRenderState(D3DRS_NORMALIZENORMALS, FALSE);
	}

	// dispose memory
	if (state) {
		delete state;
	}
}

bool EquippableObjectImport::preview(IUGCImportStateObj* _pStateObj, UGCImportStateObserver* pObserver) {
	EquippableImportState* pStateObj = dynamic_cast<EquippableImportState*>(_pStateObj);
	VERIFY_RETURN(pStateObj != NULL, false);

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	bool res = false;

	std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());

	assert(pStateObj->m_state == UGCIS_PREVIEWING);
	if (pStateObj->m_state == UGCIS_PREVIEWING) {
		// Get target skeleton
		assert(pStateObj->m_pTargetMovObj == NULL);
		pStateObj->m_pTargetMovObj = NULL;

		if (pStateObj->m_targetEntityNetworkID == TARGET_NET_ID_ME)
			pStateObj->m_pTargetMovObj = dynamic_cast<CMovementObj*>(pICE->GetMovementObjectMeAsGetSet());
		else
			pStateObj->m_pTargetMovObj = dynamic_cast<CMovementObj*>(pICE->GetMovementObjectByNetIdAsGetSet(pStateObj->m_targetEntityNetworkID));

		assert(pStateObj->m_pTargetMovObj != NULL);

		AssetTree* pAssets = IAssetsDatabase::Instance()->OpenAndPreviewURI(pStateObj->m_rootUri, KEP_SKA_SKELETAL_OBJECT, IMP_GENERAL_NOUI, &classifyFunc);
		pStateObj->m_assets = pAssets;

		AssetTree* pStaticMeshes = NULL;
		AssetTree* pSkinnedMeshes = NULL;
		AssetTree* pSkeletons = NULL;

		if (pAssets != NULL) {
			pStaticMeshes = AssetData::GetAssetGroup(pAssets, KEP_STATIC_MESH);
			pSkinnedMeshes = AssetData::GetAssetGroup(pAssets, KEP_SKA_SKINNED_MESH);
			pSkeletons = AssetData::GetAssetGroup(pAssets, KEP_SKA_SKELETAL_OBJECT);
		}

		// Mark skeletons for import
		if (pSkeletons != NULL) {
			for (AssetTree::pre_order_iterator it = pSkeletons->pre_order_begin(); it != pSkeletons->pre_order_end(); ++it) {
				AssetData& asset = *it;

				if (asset.GetType() != KEP_SKA_SKINNED_MESH && asset.GetType() != KEP_SKA_SKELETAL_OBJECT) {
					continue;
				}

				asset.SetFlag(AssetData::STA_SELECTED);
			}
		}

		int meshCount = 0;
		if (pStaticMeshes != NULL) {
			for (AssetTree::pre_order_iterator it = pStaticMeshes->pre_order_begin(); it != pStaticMeshes->pre_order_end(); ++it) {
				AssetData& asset = *it;
				if (asset.GetLevel() == AssetData::LVL_LOD) {
					asset.SetFlag(AssetData::STA_SELECTED);
					meshCount += asset.GetNumSubmeshes();
				}
			}
			// Preview succeeded
			res = true;
		}

		if (pSkinnedMeshes != NULL) {
			for (AssetTree::pre_order_iterator it = pSkinnedMeshes->pre_order_begin(); it != pSkinnedMeshes->pre_order_end(); ++it) {
				AssetData& asset = *it;
				if (asset.GetType() != KEP_SKA_SKINNED_MESH && asset.GetType() != KEP_SKA_SKELETAL_OBJECT) {
					continue;
				}

				asset.SetFlag(AssetData::STA_SELECTED);
				meshCount += asset.GetNumSubmeshes();
			}
			// Preview succeeded
			res = true;
		}

		if (!res) {
			pStateObj->ReportError(UGCIE_NO_MESH, "Unable to find a mesh in the import file.", RA_NORECOVERY, true);
		}

		if (meshCount > CC_SLOTS_MAX) {
			res = false;
			std::stringstream ssMsg;
			ssMsg << "3D model contains too many meshes. Found " << meshCount << " meshes, max allowed: " << CC_SLOTS_MAX;
			pStateObj->ReportError(UGCIE_TOO_MANY_MESHES, ssMsg.str(), RA_NORECOVERY, true);
		}
	} else {
		assert(false);
		// Invalid state
	}

	// Update import state
	if (pObserver != NULL) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool EquippableObjectImport::import(IUGCImportStateObj* _pStateObj, UGCImportStateObserver* pObserver) {
	EquippableImportState* pStateObj = dynamic_cast<EquippableImportState*>(_pStateObj);
	VERIFY_RETURN(pStateObj != NULL, false);

	bool res = true;

	std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());

	assert(pStateObj->m_state == UGCIS_IMPORTING);
	if (pStateObj->m_state == UGCIS_IMPORTING) {
		// Import flags
		int flags = IMP_SKELETAL_MULTISLOT | IMP_SKELETAL_MULTICONFIG | IMP_GENERAL_MULTILOD | IMP_GENERAL_SINGLESUBMESH | IMP_SKELETAL_ALLMESHES | IMP_SKELETAL_DMESHMAT | IMP_SKELETAL_USEDUMMYBONE | IMP_SKELETAL_ORPHANMESHES | IMP_GENERAL_NOUI;
		flags |= IMP_GENERAL_SINGLELOD; // TODO: support LODs

		//Assuming this is in Client so we would like to limit texture size
		//If dynamic object importer will be called from Editor in the future, I'll need to change the interface.
		flags |= IMP_TEXTURE_POWEROF2_ROUNDING | IMP_TEXTURE_SIZE_LIMIT_MEDIUM;

		// Attempt Object Import
		CSkeletonObject* pNewSkeleton = IAssetsDatabase::Instance()->ImportSkeletalObjectByAssetTree(pStateObj->m_rootUri, flags, pStateObj->m_assets);
		if (!pNewSkeleton) {
			LogError("ImportSkeletalObjectByAssetTree() FAILED");
			pStateObj->skeleton = NULL;
			res = false;
		} else {
			pNewSkeleton->getRuntimeSkeleton()->computeBoundingBoxWithSkinnedMesh(pNewSkeleton->getSkeletonConfiguration());
			pNewSkeleton->getRuntimeSkeleton()->initReMeshBuffer();
			pStateObj->skeleton = pNewSkeleton;
			res = true;
		}
	}

	if (pObserver) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool EquippableObjectImport::convert(IUGCImportStateObj* _pStateObj, UGCImportStateObserver* pObserver) {
	EquippableImportState* pStateObj = dynamic_cast<EquippableImportState*>(_pStateObj);
	VERIFY_RETURN(pStateObj != NULL, false);

	bool res = true;

	std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());

	assert(pStateObj->m_state == UGCIS_CONVERTING);
	if (pStateObj->m_state == UGCIS_CONVERTING) {
		assert(pStateObj->skeleton);
		if (pStateObj->ProcessSkeletonMeshes(pStateObj->skeleton->getRuntimeSkeleton(), pStateObj->skeleton->getSkeletonConfiguration())) {
			pStateObj->skeleton->getRuntimeSkeleton()->computeBoundingBoxWithSkinnedMesh(pStateObj->skeleton->getSkeletonConfiguration());
			if (pStateObj->savedSkeleton == NULL && pStateObj->skeleton != NULL) {
				// Save original bound box (before any scaling)
				const BNDBOX& skelBox = pStateObj->skeleton->getRuntimeSkeleton()->getLocalBoundingBox();
				pStateObj->m_boundBoxMin = skelBox.GetMin();
				pStateObj->m_boundBoxMax = skelBox.GetMax();
			}

			{
				std::unique_ptr<CArmedInventoryObj> armedPtr(new CArmedInventoryObj("UGC"));

				CMovementObj* pTargetActor = pStateObj->GetTargetActor(); // get target actor based on current NPC
				int mountBoneIndex = pTargetActor->getSkeleton()->getBoneIndexByName(pStateObj->m_actorMountBone);
				if (mountBoneIndex == -1) {
					mountBoneIndex = 0;
				}

				pStateObj->m_localGLID = EquippableRM::Instance()->AllocateLocalGLID();

				armedPtr->setGlobalId(pStateObj->m_localGLID);
				armedPtr->setAttachedBone(mountBoneIndex, pStateObj->m_actorMountBone);
				armedPtr->setAbilityTypeUse(pStateObj->m_targetActorType);
				armedPtr->setThirdPersonSkeleton(pStateObj->skeleton);
				armedPtr->setLocationOccupancy(pStateObj->m_exclusionGroup);

				EquippableRM::Instance()->AddNew(std::move(armedPtr), pStateObj->m_localGLID);
			}

			// Obtain texture count and triangle count
			CArmedInventoryProxy* proxy = EquippableRM::Instance()->GetProxy(pStateObj->m_localGLID, false);
			if (proxy) {
				EquippableItemProperties ps(proxy, pStateObj->m_boundBoxInActorSpace);
				size_t meshCount = (size_t)ps.get(PROP_MESH_CNT).getInt();
				if (meshCount > CC_SLOTS_MAX) {
					LogError("Too many meshes: got " << meshCount << ", max: " << CC_SLOTS_MAX);
					res = false;
				} else {
					pStateObj->m_triangleCount = ps.get(PROP_TRIANGLE_CNT).getInt();
					pStateObj->m_textureCount = ps.get(PROP_DISTINCT_TEXTURE_CNT).getInt();
					proxy->GetInventoryObj()->getSkeletonConfig()->m_charConfig.initBaseItem(meshCount);
				}
			}
		} else {
			res = false;
		}
	}

	if (pObserver != NULL) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool EquippableObjectImport::process(IUGCImportStateObj* _pStateObj, UGCImportStateObserver* pObserver) {
	EquippableImportState* pStateObj = dynamic_cast<EquippableImportState*>(_pStateObj);
	VERIFY_RETURN(pStateObj != NULL, false);

	std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());

	bool res = false;

	assert(pStateObj->m_state == UGCIS_PROCESSING);
	if (pStateObj->m_state == UGCIS_PROCESSING) {
		// 1. Obtain imported APD (latest version) and make a copy
		CArmedInventoryProxy* pProxy = EquippableRM::Instance()->GetProxy(pStateObj->m_localGLID, false);
		assert(pProxy != NULL);

		if (pProxy) {
			CArmedInventoryObj* pEquip = pProxy->GetInventoryObj();
			assert(pEquip != NULL);

			if (pEquip) {
				CArmedInventoryObj* pNewEquip = pEquip;
				//pEquip->Clone( &pNewEquip, NULL );
				RuntimeSkeleton* pNewSkeleton = pNewEquip->getSkeleton();

				assert(pNewSkeleton != nullptr);
				if (pNewSkeleton != nullptr) {
					// 2. Bake scaling factor into mesh
					//pStateObj->ProcessSkeletonScaling( pNewEquip->m_thirdPersonSystem );
					//pNewEquip->Scale( 1.0f, 1.0f, 1.0f );	// Reset scaling matrix

					// 3. Create data for rendering
					pStateObj->ProcessSkeletonMaterials(pNewSkeleton, pNewEquip->getSkeletonConfig(), true);
					if (pStateObj->ProcessSkeletonMeshes(pNewSkeleton, pNewEquip->getSkeletonConfig())) {
						// 3. Add baked APD into resource manager (and get a new GLID)
						//pStateObj->m_localGLID = EquippableSystem::Instance()->AllocateLocalGLID();
						//pNewEquip->m_type = pStateObj->m_localGLID;
						//EquippableSystem::Instance()->Add(pNewEquip, pStateObj->m_localGLID);

						res = true;
					} // else leave res = false
				}
			}
		}
	}

	if (pObserver) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool EquippableObjectImport::commit(IUGCImportStateObj* _pStateObj, UGCImportStateObserver* pObserver) {
	EquippableImportState* pStateObj = dynamic_cast<EquippableImportState*>(_pStateObj);
	VERIFY_RETURN(pStateObj != NULL, false);

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());

	bool res = false;

	assert(pStateObj->m_state == UGCIS_COMMITTING);
	if (pStateObj->m_state == UGCIS_COMMITTING) {
		// 1. Obtain imported APD (latest version) and make a copy
		CArmedInventoryProxy* pProxy = EquippableRM::Instance()->GetProxy(pStateObj->m_localGLID, false);
		assert(pProxy != NULL);

		if (pProxy) {
			// 4. Create upload state in UGC manager
			assert(pICE->GetUGCManager() && EquippableRM::Instance());
			if (pICE->GetUGCManager() && EquippableRM::Instance()) {
				//CArmedInventoryProxy* pProxy = EquippableSystem::Instance()->GetProxy( pStateObj->m_localGLID, false );
				assert(pProxy != NULL);
				if (pProxy) {
					pICE->GetUGCManager()->AddUGCObj(new UGCEquippableItem(pStateObj->m_localGLID, pStateObj->m_targetActorType, pStateObj->m_boundBoxInActorSpace, pStateObj->m_rootUri, pProxy));
					res = true;
				} // else leave res = false
			}
		}
	}

	if (pObserver) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool EquippableObjectImport::cleanup(IUGCImportStateObj* _pStateObj, UGCImportStateObserver* pObserver) {
	EquippableImportState* pStateObj = dynamic_cast<EquippableImportState*>(_pStateObj);
	VERIFY_RETURN(pStateObj != NULL, false);

	try {
		{
			std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());
			IAssetsDatabase::Instance()->CloseURI(pStateObj->m_rootUri, KEP_SKA_SKELETAL_OBJECT, IMP_GENERAL_NOUI);

			if (pObserver) {
				pObserver->doUpdate(pStateObj, EV_DESTROYED);
			}
		}

		disposeState(pStateObj);
	} catch (CException*) {
		assert(false);
		return false;
	}

	return true;
}

BEGIN_GETSET_IMPL(EquippableImportState, IDS_EQUIPPABLEIMPORTSTATE_NAME)
GETSET(m_state, gsInt, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, STATE)
GETSET(m_stateMsg, gsString, GS_FLAG_EXPOSE, 0, 0, UGCIMPORTSTATE, STATEMSG)
GETSET_OBJ_PTR(m_importedObjectGetSet, GS_FLAG_EXPOSE, 0, 0, UGCIMPORTSTATE, IMPORTEDOBJECTGETSET, IGetSet)
GETSET(m_isCanceling, gsBool, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, ISCANCELING)
GETSET(m_eventId, gsShort, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, EVENTID)
GETSET(m_eventObjId, gsDword, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, EVENTOBJID)
GETSET(m_targetType, gsInt, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETTYPE)
GETSET(m_targetId, gsInt, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETID)
GETSET(m_targetPos.x, gsFloat, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETX)
GETSET(m_targetPos.y, gsFloat, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETY)
GETSET(m_targetPos.z, gsFloat, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETZ)
GETSET(m_useImportDlg, gsBool, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, USEIMPORTDLG)
GETSET(m_isValid, gsBool, GS_FLAG_EXPOSE, 0, 0, UGCIMPORTSTATE, ISVALID)
GETSET2(m_rootUri, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCIMPORTSTATE_ROOTURI, IDS_UGCIMPORTSTATE_ROOTURI)
GETSET2(m_importFlags, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCIMPORTSTATE_IMPORTFLAGS, IDS_UGCIMPORTSTATE_IMPORTFLAGS)
GETSET2(m_assetFound, gsBool, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_UGCIMPORTSTATE_ASSETFOUND, IDS_UGCIMPORTSTATE_ASSETFOUND)
GETSET2(m_localGLID, gsInt, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_UGCIMPORTSTATE_LOCALGLID, IDS_UGCIMPORTSTATE_LOCALGLID)
GETSET2(m_triangleCount, gsInt, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_UGCIMPORTSTATE_TRIANGLECOUNT, IDS_UGCIMPORTSTATE_TRIANGLECOUNT)
GETSET2(m_textureCount, gsInt, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_UGCIMPORTSTATE_TEXTURECOUNT, IDS_UGCIMPORTSTATE_TEXTURECOUNT)
GETSET2(m_targetEntityNetworkID, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCPLAYERITEMIMPORTSTATE_TARGETENTITYNETWORKID, IDS_UGCPLAYERITEMIMPORTSTATE_TARGETENTITYNETWORKID)
GETSET2(m_aiMounted, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCPLAYERITEMIMPORTSTATE_AIMOUNTED, IDS_UGCPLAYERITEMIMPORTSTATE_AIMOUNTED)
GETSET2(m_targetActorType, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCPLAYERITEMIMPORTSTATE_ACTORTYPE, IDS_UGCPLAYERITEMIMPORTSTATE_ACTORTYPE)
GETSET2(m_autoCorrectAmbient, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCMATERIALIMPORTSTATE_AUTOCORRECTAMBIENT, IDS_UGCMATERIALIMPORTSTATE_AUTOCORRECTAMBIENT)
GETSET2(m_ignoreMissingTextures, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCMATERIALIMPORTSTATE_IGNOREMISSINGTEXTURES, IDS_UGCMATERIALIMPORTSTATE_IGNOREMISSINGTEXTURES)
GETSET2(m_boundBoxMin, gsD3dvector, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_UGCSCALABLEIMPORTSTATE_BOUNDBOXMIN, IDS_UGCSCALABLEIMPORTSTATE_BOUNDBOXMIN)
GETSET2(m_boundBoxMax, gsD3dvector, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_UGCSCALABLEIMPORTSTATE_BOUNDBOXMAX, IDS_UGCSCALABLEIMPORTSTATE_BOUNDBOXMAX)
GETSET2(m_scaleFactor, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCSCALABLEIMPORTSTATE_SCALEFACTOR, IDS_UGCSCALABLEIMPORTSTATE_SCALEFACTOR)
GETSET2(m_objectType, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_EQUIPPABLEIMPORTSTATE_OBJECTTYPE, IDS_EQUIPPABLEIMPORTSTATE_OBJECTTYPE)
GETSET2(m_actorMountBone, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_EQUIPPABLEIMPORTSTATE_ACTORMOUNTBONE, IDS_EQUIPPABLEIMPORTSTATE_ACTORMOUNTBONE)
GETSET2(m_position, gsD3dvector, GS_FLAGS_DEFAULT, 0, 0, IDS_EQUIPPABLEIMPORTSTATE_POSITION, IDS_EQUIPPABLEIMPORTSTATE_POSITION)
GETSET2(m_rotation, gsD3dvector, GS_FLAGS_DEFAULT, 0, 0, IDS_EQUIPPABLEIMPORTSTATE_ROTATION, IDS_EQUIPPABLEIMPORTSTATE_ROTATION)
GETSET2(m_exclusionGroup, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_EQUIPPABLEIMPORTSTATE_EXCLUSIONGROUP, IDS_EQUIPPABLEIMPORTSTATE_EXCLUSIONGROUP)
GETSET_CUSTOM(this, gsInt, GS_FLAGS_DEFAULT, 0, 0, EQUIPPABLEIMPORTSTATE, UPDATEMATRIX, 0, INT_MAX)
GETSET2(m_boundBoxInActorSpace.minX, gsFloat, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_EQUIPPABLEIMPORTSTATE_BOUNDBOXINACTORSPACE_MINX, IDS_EQUIPPABLEIMPORTSTATE_BOUNDBOXINACTORSPACE_MINX)
GETSET2(m_boundBoxInActorSpace.minY, gsFloat, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_EQUIPPABLEIMPORTSTATE_BOUNDBOXINACTORSPACE_MINY, IDS_EQUIPPABLEIMPORTSTATE_BOUNDBOXINACTORSPACE_MINY)
GETSET2(m_boundBoxInActorSpace.minZ, gsFloat, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_EQUIPPABLEIMPORTSTATE_BOUNDBOXINACTORSPACE_MINZ, IDS_EQUIPPABLEIMPORTSTATE_BOUNDBOXINACTORSPACE_MINZ)
GETSET2(m_boundBoxInActorSpace.maxX, gsFloat, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_EQUIPPABLEIMPORTSTATE_BOUNDBOXINACTORSPACE_MAXX, IDS_EQUIPPABLEIMPORTSTATE_BOUNDBOXINACTORSPACE_MAXX)
GETSET2(m_boundBoxInActorSpace.maxY, gsFloat, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_EQUIPPABLEIMPORTSTATE_BOUNDBOXINACTORSPACE_MAXY, IDS_EQUIPPABLEIMPORTSTATE_BOUNDBOXINACTORSPACE_MAXY)
GETSET2(m_boundBoxInActorSpace.maxZ, gsFloat, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_EQUIPPABLEIMPORTSTATE_BOUNDBOXINACTORSPACE_MAXZ, IDS_EQUIPPABLEIMPORTSTATE_BOUNDBOXINACTORSPACE_MAXZ)
END_GETSET_IMPL

} // namespace KEP