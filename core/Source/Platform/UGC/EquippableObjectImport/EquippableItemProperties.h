///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../UGCProperties.h"
#include "BoundBox.h"

namespace KEP {

class CArmedInventoryProxy;

class EquippableItemProperties : public PropertySource {
public:
	EquippableItemProperties(const CArmedInventoryProxy* pProxy, const BoundBox& transformedBox); // mountMatrix is in actor space

	VLD_VALUE_TYPE getType(int propId) const;
	Value get(int propId) const;

	void getAllSupportedPropertyIds(std::vector<int>& allPropIds);

protected:
	int meshCount;
	int vertexCount;
	int triangleCount;
	int materialCount;
	int textureCount;
	int distinctTextureCount;
	BoundBox boundBox, boundBoxInActorSpace;
	TexInfo textures[MAX_TEX_COUNT];
	int boneCount;
	int actorType;
	int actorGLID;
	int exclusionGroupID;
};

} // namespace KEP