///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define VVT_UNDEFINED '\0'
#define VVT_FLOAT 'f'
#define VVT_INT 'i'
#define VVT_BOOL 'b'
#define VVT_STRING 's'
#define VVT_ERROR 'e'

#define MAKE_PROPID(propIdBase, index) ((index << 16) + propIdBase)
#define PROPID_BASE(propId) (propId & 0xFFFF)
#define PROPID_INDEX(propId) (propId >> 16)

namespace KEP {

typedef char VLD_VALUE_TYPE;
enum class ImageOpacity;

class Value;

class PropertySource {
public:
	virtual Value get(int propId) const = 0;
	virtual VLD_VALUE_TYPE getType(int propId) const = 0;
	virtual void getAllSupportedPropertyIds(std::vector<int>& allPropIds) = 0;
	virtual ~PropertySource() {}
};

class Value {
public:
	Value(VLD_VALUE_TYPE aType = VVT_UNDEFINED);
	Value(float aValue);
	Value(int aValue);
	Value(bool aValue);
	Value(const std::string& aValue);

	float getFloat() const;
	int getInt() const;
	bool getBoolean() const;
	std::string getString() const;

	Value evaluate() const { return Value(*this); }

	std::string toString() const;
	friend std::ostream& operator<<(std::ostream& os, const Value& val);

	bool isBound() const { return bound; }
	int getType() const { return type; }

protected:
	int type;
	bool bound;

	float floatValue;
	int intValue;
	bool boolValue;
	std::string stringValue;

	void init();
	void setValue(float aValue);
	void setValue(int aValue);
	void setValue(bool aValue);
	void setValue(const std::string& aValue);
};

// recovery actions
enum {
	RA_NORECOVERY = 0,
	RA_RESIZE = 1,
};

struct Violation {
	std::string violationMessage;
	int recoveryAction;

	Violation(const std::string& msg, int action) :
			violationMessage(msg), recoveryAction(action) {}
};

#define MAX_TEX_COUNT 4096
struct TexInfo {
	DWORD nameHash;
	int width;
	int height;
	ImageOpacity opacity;
	unsigned averageColor;

	TexInfo() :
			nameHash(0), width(0), height(0), opacity((ImageOpacity)0), averageColor(0) {}
};

class STLMapPropertySource : public PropertySource {
public:
	STLMapPropertySource(const std::map<int, Value>& propMap) :
			m_propMap(propMap) {}

	virtual Value get(int propId) const {
		std::map<int, Value>::const_iterator it = m_propMap.find(propId);
		if (it == m_propMap.end()) {
			return Value();
		}

		return it->second;
	}

	virtual VLD_VALUE_TYPE getType(int propId) const {
		std::map<int, Value>::const_iterator it = m_propMap.find(propId);
		if (it == m_propMap.end()) {
			return VVT_UNDEFINED;
		}

		return (VLD_VALUE_TYPE)it->second.getType();
	}

	virtual void getAllSupportedPropertyIds(std::vector<int>& allPropIds) {
		for (std::map<int, Value>::const_iterator it = m_propMap.begin(); it != m_propMap.end(); ++it) {
			allPropIds.push_back(it->first);
		}
	}

private:
	std::map<int, Value> m_propMap;
};

} // namespace KEP
