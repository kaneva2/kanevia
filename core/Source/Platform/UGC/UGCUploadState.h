///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common\include\KEPHelpers.h"
#include "UGC_Global.h"
#include <string>
#include <map>
#include <vector>

namespace KEP {

class IEvent;
class IUGCManager;
class IClientEngine;
class PropertySource;
class IResource;

class UGCUploadState {
private:
	static IUGCManager* ms_pManager;

	int m_ugcType;
	long long m_ugcId;

	int m_state; // UGC Submission/Upload State
	std::string m_uploadId; // Upload ID assigned by wokweb
	IEvent* m_submissionReplyEvent; // Event triggered when wokweb processed initial submission request
	IEvent* m_uploadProgressEvent; // Event for file upload progress update
	IEvent* m_uploadCompletionEvent; // Event triggered when file upload completes

	unsigned m_bytesTotal; // Total bytes for upload
	int m_lastPercentage; // Last known file upload percentage

	unsigned char* m_fileUploadBuffer; // Buffer holding upload data

	// Original size and hash (before any encryption or compression)
	int m_origSize;
	std::string m_origHash;

public:
	UGCUploadState(int type, long long id, int state) :
			m_ugcType(type),
			m_ugcId(id),
			m_state(state),
			m_submissionReplyEvent(NULL),
			m_uploadProgressEvent(NULL),
			m_uploadCompletionEvent(NULL),
			m_bytesTotal(0),
			m_lastPercentage(-1),
			m_fileUploadBuffer(NULL),
			m_origSize(0) {}

	virtual ~UGCUploadState() {
		if (m_fileUploadBuffer) {
			delete m_fileUploadBuffer;
		}
	}

	static void SetUGCManager(IUGCManager* pManager) { ms_pManager = pManager; }

	// Accessors
	int getType() const { return m_ugcType; }
	long long getId() const { return m_ugcId; }

	std::string getUploadId() const { return m_uploadId; }
	void setUploadId(const std::string& uploadId) { m_uploadId = uploadId; }

	IEvent* getSubmissionReplyEvent() const { return m_submissionReplyEvent; }
	void setSubmissionReplyEvent(IEvent* e) { m_submissionReplyEvent = e; }
	IEvent* getUploadProgressEvent() const { return m_uploadProgressEvent; }
	void setUploadProgressEvent(IEvent* e) { m_uploadProgressEvent = e; }
	void setUploadCompletionEvent(IEvent* e) { m_uploadCompletionEvent = e; }

	unsigned getBytesTotal() const { return m_bytesTotal; }
	void setBytesTotal(unsigned bytes) { m_bytesTotal = bytes; }

	int getOrigSize() const { return m_origSize; }
	void setOrigSize(int size) { m_origSize = size; }

	std::string getOrigHash() const { return m_origHash; }
	void setOrigHash(const std::string& hash) { m_origHash = hash; }

	bool setLastPercentage(int percent) {
		if (m_lastPercentage == percent)
			return false;
		m_lastPercentage = percent;
		return true;
	}

	// Overridables
	virtual std::string getSubmissionUrl(IClientEngine* pEngine) = 0;

	virtual void dumpAllProperties(std::map<int, std::string>& allPropertiesFound);
	virtual FileSize computeUploadSize(IClientEngine* pEngine);
	virtual unsigned char* prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles) = 0;

	virtual std::string getOrigName() const = 0;

	void setState(int newState) { m_state = newState; }
	bool checkState(int desiredStates) { return desiredStates == PS_DONTCARE || (m_state & desiredStates) != 0; }
	virtual bool changeState(int preStates, int postState);
	virtual bool completeUpload();

protected:
	virtual PropertySource* getPropertySource() { return NULL; }

	void setType(int type) { m_ugcType = type; }

	static IUGCManager* GetUGCManager() { return ms_pManager; }
};

class UGCUploadChildState : public UGCUploadState {
private:
	std::string m_masterUploadId; // Upload ID of parent asset
	std::string m_childId; // Child item identification key if multiple children of same type exists

public:
	UGCUploadChildState(int aType, long long aId, const std::string& childId, int aState = PS_CREATED) :
			UGCUploadState(aType, aId, aState), m_childId(childId) {}
	virtual std::string getSubmissionUrl(IClientEngine* pEngine);
	virtual bool changeState(int preStates, int postState) override;

	std::string getMasterUploadId() const { return m_masterUploadId; }
	void setMasterUploadId(const std::string& masterId) { m_masterUploadId = masterId; }

	std::string getChildId() const { return m_childId; }
};

class UGCItemTexture : public UGCUploadChildState {
private:
	std::string m_fileName;

public:
	UGCItemTexture(int type, unsigned hash, const std::string& path, long long texSlotId, int state = PS_CREATED);

	virtual unsigned char* prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles);
	virtual std::string getOrigName() const { return m_fileName; } // Not actually original name
};

#define SUBID_PREVIEWIMAGE "0"

class UGCItemThumbnails : public UGCUploadChildState {
private:
	std::map<int, std::string> m_previewImages;

public:
	UGCItemThumbnails(int s = PS_CREATED) :
			UGCUploadChildState(UGC_TYPE_TEX_ICON, 0, SUBID_PREVIEWIMAGE, s) {}

	virtual unsigned char* prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles);
	virtual std::string getOrigName() const { return "Preview Image"; }

	bool addImage(const std::string& imgPath, int imgId);
	bool isEmpty() { return m_previewImages.empty(); }
	void clear() { m_previewImages.clear(); }
};

typedef std::map<unsigned, std::string> TexNameHash_TexNameMap;
typedef std::map<int, TexNameHash_TexNameMap> TexNameHash_TexNameMaps;

class UGCItemUpload : public UGCUploadState {
private:
	std::map<int, std::map<int, UGCUploadChildState*>> m_childItems;

	// preview images
	UGCItemThumbnails m_previewImagesState;

	int m_uploadFilesTotal;
	int m_uploadFilesCompleted;
	int m_uploadFilesInProgress;
	FileSize m_uploadBytesTotal;
	FileSize m_uploadBytesProcessed;
	std::map<std::string, unsigned> m_itemBytesProcessed;

public:
	UGCItemUpload(int aType, long long aId, int aState);

	bool addPreviewImage(const std::string& filePath, int previewImageId);
	void clearPreviewImages();

	void addChildItem(int childType, int childId, UGCUploadChildState* childState);
	void clearChildItemByType(int childType);
	UGCUploadChildState* getChildItem(int childType, int childId);
	unsigned getChildItemCount() const;
	bool checkAllChildItemsStates(int desiredStates);
	void collectChildItemsUploadTotal(unsigned& numItems, FileSize& totalSize);

	int getUploadFilesTotal() const { return m_uploadFilesTotal; }
	int getUploadFilesCompleted() const { return m_uploadFilesCompleted; }
	int getUploadFilesInProgress() const { return m_uploadFilesInProgress; }
	FileSize getUploadBytesTotal() const { return m_uploadBytesTotal; }
	FileSize getUploadBytesProcessed() const { return m_uploadBytesProcessed; }

	const std::map<int, std::map<int, UGCUploadChildState*>>& getAllChildItems() const { return m_childItems; }

	void updateChildItemProgressInBytes(const std::string& childId, unsigned bytes) {
		auto prev = m_itemBytesProcessed[childId];
		m_itemBytesProcessed[childId] = bytes;
		m_uploadBytesProcessed = (prev > m_uploadBytesProcessed) ? 0 : (m_uploadBytesProcessed - prev); // back out old bytes
		m_uploadBytesProcessed += bytes; // add back new bytes
	}

	void fileUploadStarted() { m_uploadFilesInProgress++; }
	void fileUploadCompleted() {
		m_uploadFilesInProgress--;
		m_uploadFilesCompleted++;
	}

	void resetProgress() {
		m_uploadFilesCompleted = 0;
		m_uploadFilesInProgress = 0;
		m_uploadBytesProcessed = 0;
		m_itemBytesProcessed.clear();
	}

	virtual bool isDerivative() const { return false; }

	virtual void dumpAllTexturesFromEngineObj(IClientEngine* /*pEngine*/, TexNameHash_TexNameMaps& /*allTexturesFound*/) {}
	virtual std::string getSubmissionUrl(IClientEngine* pEngine) override;

	virtual bool changeState(int preStates, int postState) override;
	virtual bool childItemUploadCompleted(UGCUploadChildState* child);
};

class UGCItemUpload_IResource : public UGCItemUpload {
private:
	IResource* m_pRes;
	unsigned char* m_uploadData;
	FileSize m_uploadSize;

public:
	UGCItemUpload_IResource(IResource* res, int aType, long long aId, int aState = PS_CREATED) :
			UGCItemUpload(aType, aId, aState),
			m_pRes(res),
			m_uploadData(NULL),
			m_uploadSize(0) {}

	virtual unsigned char* prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles) override;

	IResource* getResource() { return m_pRes; }
};

class UGCItemDerivation : public UGCItemUpload {
private:
	std::string m_name;

public:
	UGCItemDerivation(int derivationType, long long derivationId, const std::string& templateName, int state = PS_CREATED) :
			UGCItemUpload(derivationType, derivationId, state),
			m_name(templateName) {}

	virtual std::string getOrigName() const override { return m_name; }
	virtual bool isDerivative() const override { return true; }
};

}; // namespace KEP
