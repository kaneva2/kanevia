///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UGCManager.h"
#include "UGCItemUploadState.h"
#include "IClientEngine.h"
#include "CMaterialClass.h"
#include "StreamableDynamicObj.h"
#include "DynamicObj.h"
#include "Engine/clientblades/dynamicObPlacementModelProxy.h"
#include "DynamicObjectImport/DynamicObjectProperties.h"
#include "Engine/clientblades/imodel.h"
#include "AnimationImport/AnimationProperties.h"
#include "EquippableObjectImport/EquippableItemProperties.h"
#include "ResourceManagers/AnimationProxy.h"
#include "ResourceManagers/DynamicObjectManager.h"
#include "EquippableSystem.h"
#include "EnvParticleSystem.h"
#include "CSkeletonObject.h"
#include "RuntimeSkeleton.h"
#include "CBoneClass.h"
#include "CDeformableMesh.h"
#include "CHierarchyMesh.h"
#include "SkeletonConfiguration.h"
#include "CEXMeshClass.h"
#include "TextureObj.h"
#include "config.h"
#include "Core/Crypto/Crypto_MD5.h"
#include "Core/Crypto/Crypto_KRX.h"
#include "LogHelper.h"

namespace KEP {

static LogInstance("UGCImport");

// Texture dump helpers
void dumpTexturesFromCMaterialObject(IClientEngine* pEngine, CMaterialObject* pMaterial, TexNameHash_TexNameMap& texturesFound) {
	VERIFY_RETVOID(pMaterial != NULL);
	for (int texLoop = 0; texLoop < NUM_CMATERIAL_TEXTURES; texLoop++) {
		DWORD nameHash = pMaterial->m_texNameHash[texLoop];
		if (ValidTextureNameHash(nameHash)) {
			std::string texFileName = pMaterial->m_texFileName[texLoop];
			if (ValidTextureFileName(texFileName)) {
				texturesFound[nameHash] = texFileName;
			}
		}
	}
}

void dumpTexturesFromDeformableMesh(IClientEngine* pEngine, CDeformableMesh* pDMesh, TexNameHash_TexNameMap& texturesFound) {
	VERIFY_RETVOID(pDMesh != NULL);
	if (pDMesh->m_material) {
		dumpTexturesFromCMaterialObject(pEngine, pDMesh->m_material, texturesFound);
	}
}

void dumpTexturesFromRigidMesh(IClientEngine* pEngine, CHierarchyMesh* pHMesh, TexNameHash_TexNameMap& texturesFound) {
	VERIFY_RETVOID(pHMesh != NULL);
	for (int lodLoop = 0; lodLoop < pHMesh->m_lodChain->GetCount(); lodLoop++) {
		CHierarchyVisObj* pHVis = pHMesh->GetByIndex(lodLoop);
		if (pHVis) {
			if (pHVis->m_mesh && pHVis->m_mesh->m_materialObject) {
				dumpTexturesFromCMaterialObject(pEngine, pHVis->m_mesh->m_materialObject, texturesFound);
			}
		}
	}
}

void dumpTexturesFromCSkeletonObject(IClientEngine* pEngine, RuntimeSkeleton* pSkeleton, SkeletonConfiguration* pSkeletonConfig, TexNameHash_TexNameMap& texturesFound) {
	VERIFY_RETVOID(pSkeleton != NULL);

	// Dump textures from rigid meshes
	for (UINT boneLoop = 0; boneLoop < pSkeleton->getBoneCount(); boneLoop++) {
		CBoneObject* pBone = pSkeleton->getBoneByIndex(boneLoop);
		if (pBone) {
			for (std::vector<CHierarchyMesh*>::iterator it = pBone->m_rigidMeshes.begin(); it != pBone->m_rigidMeshes.end(); ++it) {
				if (*it) {
					dumpTexturesFromRigidMesh(pEngine, *it, texturesFound);
				}
			}
		}
	}

	// Dump textures from deformable mesh
	for (int meshSlotId = 0; meshSlotId < pSkeleton->getSkinnedMeshCount(); meshSlotId++) {
		CDeformableMesh* pDeformableMesh = pSkeleton->getSkinnedMesh(meshSlotId);
		if (pDeformableMesh) {
			dumpTexturesFromDeformableMesh(pEngine, pDeformableMesh, texturesFound);
		}
	}

	assert(pSkeletonConfig != nullptr);
	if (pSkeletonConfig != nullptr && pSkeletonConfig->m_meshSlots != nullptr) {
		for (POSITION pos = pSkeletonConfig->m_meshSlots->GetHeadPosition(); pos != NULL;) {
			CAlterUnitDBObject* pAltCfgs = dynamic_cast<CAlterUnitDBObject*>(pSkeletonConfig->m_meshSlots->GetNext(pos));
			if (pAltCfgs && pAltCfgs->m_configurations) {
				for (POSITION posCfg = pAltCfgs->m_configurations->GetHeadPosition(); posCfg != NULL;) {
					CDeformableMesh* pDMesh = dynamic_cast<CDeformableMesh*>(pAltCfgs->m_configurations->GetNext(posCfg));
					if (pDMesh) {
						dumpTexturesFromDeformableMesh(pEngine, pDMesh, texturesFound);
					}
				}
			}
		}
	}
}

UGCDynamicObj::UGCDynamicObj(StreamableDynamicObject* pDynObj, GLID glid, IResource* pRes, int state /*= PS_CREATED */) :
		UGCItemUpload_IResource(pRes, UGC_TYPE_DYNAMIC_OBJ, glid, state),
		m_pDynObj(pDynObj) {
}

std::string UGCDynamicObj::getOrigName() const {
	assert(m_pDynObj != nullptr);
	if (m_pDynObj) {
		return m_pDynObj->getName();
	}

	return "";
}

void UGCDynamicObj::dumpAllTexturesFromEngineObj(IClientEngine* pEngine, TexNameHash_TexNameMaps& allTexturesFound) {
	allTexturesFound.clear();
	allTexturesFound.insert(make_pair(UGC_TYPE_TEX_DYNAMIC_OBJ_UGC, TexNameHash_TexNameMap()));
	TexNameHash_TexNameMap& ugcDoTextures = allTexturesFound[UGC_TYPE_TEX_DYNAMIC_OBJ_UGC];

	assert(m_pDynObj != nullptr);
	if (m_pDynObj) {
		for (size_t visLoop = 0; visLoop < m_pDynObj->getVisualCount(); visLoop++) {
			StreamableDynamicObjectVisual* pVis = m_pDynObj->Visual(visLoop);
			CMaterialObject* pMaterial = pVis->getMaterial();
			if (pMaterial) {
				dumpTexturesFromCMaterialObject(pEngine, pMaterial, ugcDoTextures);
			}
		}
	}
}

PropertySource* UGCDynamicObj::getPropertySource() {
	assert(m_pDynObj != nullptr);
	if (m_pDynObj) {
		return new DynamicObjectProperties(*m_pDynObj);
	}

	return nullptr;
}

void UGCDynamicObjDerivation::dumpAllTexturesFromEngineObj(IClientEngine* pEngine, TexNameHash_TexNameMaps& allTexturesFound) {
	CDynamicPlacementObj* pDPO = pEngine->GetDynamicObject(getPlacementId());
	jsVerifyReturnVoid(pDPO != NULL);

	std::map<DWORD, CStringA> textures;
	pDPO->getInUseTextures(textures);

	for (std::map<DWORD, CStringA>::iterator it = textures.begin(); it != textures.end(); ++it) {
		std::string texName = (const char*)it->second;
		DWORD nameHash = it->first;
		allTexturesFound[UGC_TYPE_TEX_DYNAMIC_OBJ_UGC].insert(make_pair(nameHash, texName));
	}
}

std::string UGCDynamicObjDerivation::getSubmissionUrl(IClientEngine* pEngine) {
	std::stringstream ssBytes;
	ssBytes << getBytesTotal();

	GLID globalId = pEngine->GetDynamicObjectGlid(getPlacementId());
	assert(globalId != GLID_INVALID);
	if (globalId == GLID_INVALID) {
		assert(false);
		return "";
	}

	std::string url = UGC_DERIVE_REQUEST_URL;
	STLStringSubstitute(url, TAG_WOK_URL, pEngine->GetURL(CFG_WOKURL, false));
	STLStringSubstitute(url, TAG(UGC_UPLOAD_TYPE), UGC_UPLOAD_DOB_GZ);
	STLStringSubstitute(url, TAG(UGC_UPLOAD_SIZE), ssBytes.str());
	std::stringstream ssGlid;
	assert(false); // was base global ID -- need to rework this
	ssGlid << globalId;
	STLStringSubstitute(url, TAG(UGC_VAR_BASEGLID), ssGlid.str());
	STLStringSubstitute(url, TAG(UGC_ORIG_NAME), getOrigName());

	std::map<int, std::string> ugcProps;
	dumpAllProperties(ugcProps);
	url = url + "&" + EncodePropertiesToQueryString(UGC_PROPS, ugcProps);

	return url;
}

UGCTextureCustomization::UGCTextureCustomization(ObjectType objType, int objId, const char* texPath, const char* texOrig, int state /*= PS_CREATED */) :
		UGCItemUpload(UGC_TYPE_UNKNOWN, objId, state),
		m_targetType(objType),
		m_targetId(objId),
		m_texFullPath(texPath),
		m_texOrigName(texOrig) {
	if (m_targetType == ObjectType::WORLD) {
		setType(UGC_TYPE_WORLD_OBJ | UGC_TYPE_CUSTOMIZATION);
	} else if (m_targetType == ObjectType::DYNAMIC) {
		setType(UGC_TYPE_DYNAMIC_OBJ | UGC_TYPE_CUSTOMIZATION);
	} else {
		assert(false);
		setType(UGC_TYPE_UNKNOWN);
	}
}

void UGCTextureCustomization::dumpAllTexturesFromEngineObj(IClientEngine* pEngine, TexNameHash_TexNameMaps& allTexturesFound) {
	allTexturesFound[UGC_TYPE_MEDIA_IMAGE_PATTERN][0] = m_texOrigName;
}

UGCAnimation::UGCAnimation(int animGLID, int animActorType, int doGLID, IResource* res, int state /*= PS_CREATED */) :
		UGCItemUpload_IResource(res, UGC_TYPE_ANIMATION, animGLID, state),
		m_actorType(animActorType),
		m_dynamicObjectGLID(doGLID) {
}

std::string UGCAnimation::getOrigName() const {
	//TBD
	return "";
}

PropertySource* UGCAnimation::getPropertySource() {
	AnimationProxy* animProxy = dynamic_cast<AnimationProxy*>(getResource());
	VERIFY_RETURN(animProxy != NULL, NULL);

	AnimationProperties* ps = new AnimationProperties(animProxy);
	ps->setActorType(m_actorType);
	ps->setActorGLID(m_dynamicObjectGLID);
	return ps;
}

PropertySource* UGCEquippableItem::getPropertySource() {
	CArmedInventoryProxy* pProxy = dynamic_cast<CArmedInventoryProxy*>(getResource());
	VERIFY_RETURN(pProxy != NULL, NULL);
	return new EquippableItemProperties(pProxy, m_boundBoxInActorSpace);
}

void UGCEquippableItem::dumpAllTexturesFromEngineObj(IClientEngine* pEngine, TexNameHash_TexNameMaps& allTexturesFound) {
	allTexturesFound.clear();
	allTexturesFound.insert(make_pair(UGC_TYPE_TEX_EQUIP, TexNameHash_TexNameMap()));
	TexNameHash_TexNameMap& ugcTexturesFound = allTexturesFound[UGC_TYPE_TEX_EQUIP];

	CArmedInventoryProxy* pProxy = dynamic_cast<CArmedInventoryProxy*>(getResource());
	VERIFY_RETVOID(pProxy != NULL);

	CArmedInventoryObj* pEquip = pProxy->GetInventoryObj();
	VERIFY_RETVOID(pEquip != NULL);

	RuntimeSkeleton* pSkeleton;

	pSkeleton = pEquip->getSkeleton(eVisualType::ThirdPerson);
	if (pSkeleton != nullptr) {
		dumpTexturesFromCSkeletonObject(pEngine, pSkeleton, pEquip->getSkeletonConfig(eVisualType::ThirdPerson), ugcTexturesFound);
	}

	pSkeleton = pEquip->getSkeleton(eVisualType::FirstPerson);
	if (pSkeleton != nullptr) {
		dumpTexturesFromCSkeletonObject(pEngine, pSkeleton, pEquip->getSkeletonConfig(eVisualType::FirstPerson), ugcTexturesFound);
	}
}

UGCParticleEffect::UGCParticleEffect(int psGLID, IResource* res, int state /*= PS_CREATED */) :
		UGCItemUpload_IResource(res, UGC_TYPE_PARTICLE_EFFECT, psGLID, state) {
}

std::string UGCParticleEffect::getOrigName() const {
	return "ParticleEffect";
}

void UGCParticleEffect::dumpAllTexturesFromEngineObj(IClientEngine* pEngine, TexNameHash_TexNameMaps& allTexturesFound) {
	allTexturesFound.clear();
	// Do nothing here
	// Particle editor is responsible for adding all particle textures from script
}

class ParticleEffectProperties : public PropertySource {
private:
	EnvParticleSystem* m_particleSystem;
	std::vector<std::pair<DWORD, CTextureEX*>> m_textureNameHashes;

public:
	ParticleEffectProperties(EnvParticleSystem* ps, const std::vector<std::pair<DWORD, CTextureEX*>>& textures) :
			m_particleSystem(ps), m_textureNameHashes(textures) {}

	virtual Value get(int propId) const {
		UINT propIndex = PROPID_INDEX(propId);

		switch (PROPID_BASE(propId)) {
			case PROP_MESH_CNT:
				//TODO
				return Value(1);

			case PROP_VERTEX_CNT:
				//TODO
				return Value(1);

			case PROP_TEXTURE_CNT:
				return Value((int)m_textureNameHashes.size());

			case PROP_IMAGE_WIDTH:
				if (propIndex < m_textureNameHashes.size())
					return Value(m_textureNameHashes[propIndex].second->GetWidth());
				return Value(0);

			case PROP_IMAGE_HEIGHT:
				if (propIndex < m_textureNameHashes.size())
					return Value(m_textureNameHashes[propIndex].second->GetHeight());
				return Value(0);
		}

		return VVT_UNDEFINED;
	}

	virtual VLD_VALUE_TYPE getType(int propId) const {
		switch (PROPID_BASE(propId)) {
			case PROP_MESH_CNT:
			case PROP_VERTEX_CNT:
			case PROP_TEXTURE_CNT:
			case PROP_IMAGE_WIDTH:
			case PROP_IMAGE_HEIGHT:
				return VVT_INT;
		}

		return VVT_UNDEFINED;
	}

	virtual void getAllSupportedPropertyIds(std::vector<int>& allPropIds) {
		allPropIds.clear();
		allPropIds.push_back(PROP_MESH_CNT);
		allPropIds.push_back(PROP_VERTEX_CNT);
		allPropIds.push_back(PROP_TEXTURE_CNT);
		for (UINT i = 0; i < m_textureNameHashes.size(); i++) {
			allPropIds.push_back(MAKE_PROPID(PROP_IMAGE_WIDTH, i));
			allPropIds.push_back(MAKE_PROPID(PROP_IMAGE_HEIGHT, i));
		}
	}
};

PropertySource* UGCParticleEffect::getPropertySource() {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return nullptr;

	std::vector<std::pair<DWORD, CTextureEX*>> textures;
	auto itrGroup = getAllChildItems().find(UGC_TYPE_TEX_PARTICLE);
	if (itrGroup != getAllChildItems().end()) {
		for (const auto& pr : itrGroup->second) {
			UGCItemTexture* pTexItem = dynamic_cast<UGCItemTexture*>(pr.second);
			assert(pTexItem);

			if (pTexItem) {
				DWORD textureKey = (DWORD)pTexItem->getId();
				CTextureEX* pTex = pICE->GetTextureFromDatabase(textureKey);
				if (pTex) {
					textures.push_back(std::make_pair(textureKey, pTex));
				}
			}
		}
	}

	return new ParticleEffectProperties(dynamic_cast<EnvParticleSystem*>(getResource()), textures);
}

BYTE* UGCActionItem::prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles) {
	uploadFiles.clear();
	uploadFiles.push_back(m_scriptPath);
	retBufLen = 0;

	// Check if file exists
	CFileStatus status;
	if (!CFile::GetStatus(Utf8ToUtf16(m_scriptPath).c_str(), status)) {
		LogWarn("Missing script file: " << m_scriptPath);
		return NULL;
	}

	// Get size and hash
	MD5_DIGEST_STR hashStr;
	if (!hashFile(m_scriptPath.c_str(), hashStr, 0)) {
		LogWarn("data checksum failed: " << m_scriptPath);
		return NULL;
	}
	setOrigHash(hashStr.s);
	setOrigSize(0); // drf - was -1
	return NULL;
}

PropertySource* UGCActionItem::getPropertySource() {
	std::map<int, Value> propMap;
	if (m_parentGlid != GLID_INVALID) {
		propMap.insert(std::make_pair(PROP_ACTIONITEM_PARENTSCRIPTGLID, Value(m_parentGlid)));
	}

	return new STLMapPropertySource(propMap);
}

BYTE* AppAssetUpload::prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles) {
	uploadFiles.clear();
	uploadFiles.push_back(m_assetPath);
	retBufLen = 0;
	// Check if file exists
	CFileStatus status;
	if (!CFile::GetStatus(Utf8ToUtf16(m_assetPath).c_str(), status)) {
		LogWarn("Missing asset file: " << m_assetPath);
		return NULL;
	}

	// Get size and hash
	MD5_DIGEST_STR hashStr;
	if (!hashFile(m_assetPath.c_str(), hashStr, 0)) {
		LogWarn("data checksum failed: " << m_assetPath);
		return NULL;
	}
	setOrigHash(hashStr.s);
	setOrigSize(0); // drf - was -1
	return NULL;
}

int UGCSound::importIndex = 1;

BYTE* UGCSound::prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles) {
	uploadFiles.clear();

	// file Name Or Buffer Specified ?
	if (!m_rawData) {
		retBufLen = 0;
		// read in the ogg file
		FILE* uploadFile;
		if (fopen_s(&uploadFile, m_fileName.c_str(), "rb") != 0 || !uploadFile)
			return NULL;
		m_fileSize = (size_t)FileHelper::Size(m_fileName);
		m_rawData = new BYTE[m_fileSize];
		fread(m_rawData, 1, (size_t)m_fileSize, uploadFile);

		// DRF - File Handle Leak Fix
		fclose(uploadFile);
	}

	// If origsize/orighash not set, calc them
	if (getOrigSize() == 0 || getOrigHash().empty()) {
		// Get size and hash
		MD5_DIGEST_STR hashStr;
		hashData(m_rawData, m_fileSize, hashStr, 0);
		setOrigHash(hashStr.s);
		setOrigSize(m_fileSize);
	}

	retBufLen = m_fileSize;

	return m_rawData;
}

void UGCSound::padAndEncrypt() {
	// Swap out raw data with encrypted data
	BYTE* bak = m_rawData;
	LogInfo("Encrypting - size=" << m_fileSize << " ...");
	m_rawData = KRXEncrypt(bak, m_fileSize, KRX_SALT);
	LogInfo("OK");
	delete bak;

	// Recalc hash after encryption
	MD5_DIGEST_STR hashStr;
	LogInfo("Rehashing ...");
	hashData(m_rawData, m_fileSize, hashStr, 0);
	LogInfo("OK - '" << hashStr.s << "'");
	setOrigHash(hashStr.s);
}

} // namespace KEP