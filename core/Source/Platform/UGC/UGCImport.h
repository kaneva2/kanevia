///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "UGC_Global.h"
#include "Tools\wkglib\singleton.h"
#include "Common/include/KEPHelpers.h"
#include "Common/include/LogHelper.h"
#include "Core/Crypto/Crypto_MD5.h"

namespace KEP {

class IDispatcher;
class IUGCImportStateObj;
class UGCImportStateObserver;

//!
//! \brief
//!		UGC importer implementation base class
//!
//! Override to create implementations for different types of imports. This class is wrapped inside a UGCImporter class
//! when used. All import actions can be written in blocking mode. UGCImporter class should be able to prepare necessary
//! resource to make these calls sync or async.
//!
class IUGCImport {
public:
	IUGCImport() {}

	//! Create a state object for tracking import progress
	virtual IUGCImportStateObj* createState() = 0;
	//! Delete state object after import is completed (or failed)
	virtual void disposeState(IUGCImportStateObj* state) = 0;

	//! Peak into import data source and verify data integrity and return asset structure and availbility quickly (without taking extended period of time).
	virtual bool preview(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) = 0;
	//! Import data into an intermediate format with data integrity verified.
	virtual bool import(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) = 0;
	//! Convert intermediate data to engine format so we can validate imported object (don't go too far just get enough data for validation)
	virtual bool convert(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) = 0;
	//! Proceed with final processing to make the imported object ready for the engine.
	virtual bool process(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) = 0;
	//! Commit the new object to the engine (front-end thread, safe to call IClientEngine)
	virtual bool commit(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) = 0;
	//! Dispose any temporary memory allocation
	virtual bool cleanup(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) = 0;

protected:
	//! Dummy code for unimplemented states
	bool doNothing(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver, UGCImportState expectedState);
};

struct UGCImportSTTEntry; // State Transition Table Entry

//!
//! \brief
//!		Generic UGC importer (a wrapper class for IUGCImport) support both sync and async import.
//!
//! This wrapper class should accommodate most UGC import needs. Actual import logic is implemented in classes inherited from IUGCImport.
//! Individual import actions like 'preview', 'import' will be carried out asynchronously (by using jsThreads) if an observer is provided.
//! Otherwise they would be blocking calls. A global FSM object is used to drive import progress. At some point user intervention might
//! be needed. This is done thru progress event notification and UI script integration.
//!
class UGCImporter {
public:
	UGCImporter(IUGCImport* pImpl, const std::vector<UGCImportSTTEntry>& stt) :
			m_stateTransitionTable(stt) { m_pImporterImpl = pImpl; }
	~UGCImporter() {
		if (m_pImporterImpl)
			delete m_pImporterImpl;
		m_pImporterImpl = NULL;
	}

	virtual IUGCImportStateObj* beginSession();
	virtual void endSession(IUGCImportStateObj* state);

	virtual void performAction(UGCImportAction action, IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver);

	size_t getStateTransitionTableEntryCount() { return m_stateTransitionTable.size(); }
	const UGCImportSTTEntry& getStateTransitionTableEntry(size_t entryId);

private:
	IUGCImport* m_pImporterImpl;
	std::vector<UGCImportSTTEntry> m_stateTransitionTable;
};

// State Transistion Table Entry
struct UGCImportSTTEntry {
	UGCImportState fromState;
	UGCImportEventType expectedEvent;
	UGCImportState toState;
	UGCImportAction actionToPerform;
};

class IUGCImporterFactory : public wkg::Singleton<IUGCImporterFactory> {
public:
	//! Return an appropriate importer for specified UGC type
	virtual UGCImporter* getImporter(int ugcType) = 0;
};

void InitUGCImportModule(IDispatcher* apDispatcher);
void CleanupUGCImportModule();
IUGCImporterFactory* GetImporterFactory();
UGCImportStateObserver* GetImportStateMachine();

// Creates/returns temporary path used for import.
inline std::string PathMapsModels() {
	return PathAdd(PathApp(false), "MapsModels");
}
inline std::string CreatePathMapsModels(const std::string& subDir) {
	std::string path = PathMapsModels();
	::CreateDirectoryA(path.c_str(), NULL);
	if (!subDir.empty()) {
		path = PathAdd(path, subDir);
		::CreateDirectoryA(path.c_str(), NULL);
	}
	return path;
}
inline std::string PathMapsModelsImported() {
	return PathAdd(PathMapsModels(), "imported");
}
inline std::string CreatePathMapsModelsImported() {
	return CreatePathMapsModels("imported");
}
inline std::string PathMapsModelsUploaded() {
	return PathAdd(PathMapsModels(), "imported");
}
inline std::string CreatePathMapsModelsUploaded() {
	return CreatePathMapsModels("imported");
}

// Returns legacy timestamp based hash
inline std::string GetLegacyHash() {
	SYSTEMTIME st;
	FILETIME ft;
	GetSystemTime(&st);
	SystemTimeToFileTime(&st, &ft);
	std::stringstream nameBuilder;
	nameBuilder.fill('0');
	nameBuilder << std::setw(8) << std::hex << ft.dwHighDateTime << ft.dwLowDateTime;
	return nameBuilder.str();
}

// Returns MD5 hash of named file otherwise legacy hash.
inline std::string GetFileHash(const std::string& filePath) {
	// File MD5 Hash ?
	if (FileHelper::Exists(filePath)) {
		MD5_DIGEST_STR md5;
		if (hashFile(filePath.c_str(), md5))
			return md5.s;
	}

	return GetLegacyHash();
}

// Returns MD5 hash of given data otherwise legacy hash.
inline std::string GetDataHash(const void* data, FileSize dataLen) {
	// Data MD5 Hash ?
	if (data && dataLen) {
		MD5_DIGEST_STR md5;
		hashData((const unsigned char*)data, dataLen, md5);
		return md5.s;
	}

	return GetLegacyHash();
}

} // namespace KEP