///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

#define CFG_WOKURL "WOKUrl"
#define CFG_AUTHURL "AuthUrl"

// Also Update UGCGlobals.lua !!!
enum UGC_TYPE {
	UGC_TYPE_INVALID = -1, // Invalid
	UGC_TYPE_UNKNOWN = 0x00, // To be determined
	UGC_TYPE_COLLADA = 0x01, // Collada DAE file, usage to be determined

	UGC_TYPE_DYNAMIC_OBJ = 0x10, // Dynamic objects

	UGC_TYPE_WORLD_OBJ = 0x20, // World objects (CWldObject/CReferenceObj) - used as parent type for custom textures

	UGC_TYPE_TEXTURE = 0x30,
	UGC_TYPE_TEX_WORLD_OBJ = 0x31, // Custom textures for CWldObject and CReferenceObj
	UGC_TYPE_TEX_DYNAMIC_OBJ = 0x32, // Custom textures for dynamic objects from server
	UGC_TYPE_TEX_DYNAMIC_OBJ_UGC = 0x33, // Item textures for user created dynamic objects
	UGC_TYPE_TEX_ICON = 0x34, // Item images and thumbnail icons
	UGC_TYPE_TEX_EQUIP = 0x35, // Item textures for user created equippable items
	UGC_TYPE_TEX_PARTICLE = 0x36, // Item textures for user created particle systems

	UGC_TYPE_MOVEMENT_OBJ = 0x40,

	UGC_TYPE_ANIMATION = 0x50, // UGC Animation

	UGC_TYPE_SOUND = 0x60, // sound
	UGC_TYPE_SOUND_WAV = 0x61,
	UGC_TYPE_SOUND_OGG = 0x62,
	UGC_TYPE_SOUND_MP3 = 0x63,

	UGC_TYPE_EQUIPPABLE = 0x70,
	UGC_TYPE_EQUIPPABLE_RIGID = 0x71,
	UGC_TYPE_EQUIPPABLE_SKELETAL = 0x72,

	UGC_TYPE_MEDIA_IMAGE = 0x80,
	UGC_TYPE_MEDIA_IMAGE_PATTERN = 0x81,
	UGC_TYPE_MEDIA_IMAGE_PICTURE = 0x82,

	UGC_TYPE_MEDIA_VIDEO = 0x90,
	UGC_TYPE_PARTICLE_EFFECT = 0xA0,

	UGC_TYPE_SCRIPT = 0xC0,
	UGC_TYPE_SCRIPT_DYNAMIC_OBJ = 0xC1, // Custom dynamic object scripts
	UGC_TYPE_SCRIPT_MOVEMENT_OBJ = 0xC2, // Custom player control scripts
	UGC_TYPE_SCRIPT_CHATTER = 0xC3, // Custom chatting scripts
	UGC_TYPE_ACTION_ITEM = 0xC4,
	UGC_TYPE_APP_EDIT = 0xC5,

	UGC_TYPE_URL = 0xF0, // Unclassified URL - to be determined by server
	UGC_TYPE_URL_DYNAMIC_OBJ = 0xF1, // set URL on a dynamic object - set texture or video or flash
	UGC_TYPE_URL_MOVEMENT_OBJ = 0xF2, // set URL on a movement object - send to a friend in chat window?

	UGC_TYPE_CATEMASK = 0x0FF0,

	UGC_TYPE_DERIVATIVE = 0x8000,
	UGC_TYPE_CUSTOMIZATION = 0x4000,
	UGC_TYPE_TYPEMASK = 0x3FFF,
};

enum UGC_OBJECT_COLLISION_TYPE {
	UGC_COL_NONE = 0, // No collision

	UGC_COL_BND_BOX = 0x0001, // Bounding box
	UGC_COL_BND_SPHERE = 0x0002, // Bounding sphere
	UGC_COL_BND_ELLIPSOID = 0x0003, // Bounding ellipsoid
	UGC_COL_TRIANGLES = 0x0010, // Triangle mesh
	UGC_COL_CPLX_HIERARCHY = 0x1000, // Hierarchical
	UGC_COL_CPLX_ARRAY = 0x2000, // Array

	UGC_COL_FACE_FRONT = 0x4000, // Front faces
	UGC_COL_FACE_BACK = 0x8000, // Back faces
	UGC_COL_FACE_BOTH = UGC_COL_FACE_FRONT | UGC_COL_FACE_BACK, // Both

	UGC_COL_TYPE_MASK = 0x0FFF,
	UGC_COL_FACE_MASK = 0xC000,
};

#define UGC_ACTION "action"
#define UGC_UPLOAD_TYPE "uploadtype"
#define UGC_UPLOAD_ID "uploadid"
#define UGC_UPLOAD_SIZE "uploadsize"
#define UGC_ORIG_NAME "origname"
#define UGC_ORIG_SIZE "origsize"
#define UGC_ORIG_HASH "orighash"
#define UGC_MASTER_ID "masterid"
#define UGC_SUB_ID "subid"
#define UGC_PROPS "props"
#define UGC_VAR_BASEGLID "baseglid"

#define UGC_ACTION_NEW "new"
#define UGC_ACTION_CANCEL "cancel"
#define UGC_ACTION_DATA "data"
#define UGC_ACTION_DERIVE "derive"
#define UGC_ACTION_DEPLOY "deploy"

#define TAG(x) "{" x "}"
#define TAG_WOK_URL TAG("wokurl")

#define UGC_UPLOAD_REQUEST_URL TAG_WOK_URL "kgp/ugcLink.aspx?" UGC_ACTION "=" UGC_ACTION_NEW "&" UGC_UPLOAD_TYPE "=" TAG(UGC_UPLOAD_TYPE) "&" UGC_MASTER_ID "=" TAG(UGC_MASTER_ID) "&" UGC_SUB_ID "=" TAG(UGC_SUB_ID) "&" UGC_UPLOAD_SIZE "=" TAG(UGC_UPLOAD_SIZE)
#define UGC_UPLOAD_CANCEL_URL TAG_WOK_URL "kgp/ugcLink.aspx?" UGC_ACTION "=" UGC_ACTION_CANCEL "&" UGC_UPLOAD_TYPE "=" TAG(UGC_UPLOAD_TYPE) "&" UGC_UPLOAD_ID "=" TAG(UGC_UPLOAD_ID)
#define UGC_UPLOAD_DATA_URL TAG_WOK_URL "kgp/ugcLink.aspx" // no parameters, POST values instead
#define UGC_DERIVE_REQUEST_URL TAG_WOK_URL "kgp/ugcLink.aspx?" UGC_ACTION "=" UGC_ACTION_DERIVE "&" UGC_UPLOAD_TYPE "=" TAG(UGC_UPLOAD_TYPE) "&" UGC_UPLOAD_SIZE "=" TAG(UGC_UPLOAD_SIZE) "&" UGC_VAR_BASEGLID "=" TAG(UGC_VAR_BASEGLID) "&" UGC_ORIG_NAME "=" TAG(UGC_ORIG_NAME)
#define UGC_DEPLOY_REQUEST_URL TAG_WOK_URL "kgp/ugcLink.aspx?" UGC_ACTION "=" UGC_ACTION_DEPLOY "&" UGC_UPLOAD_TYPE "=" TAG(UGC_UPLOAD_TYPE) "&" UGC_UPLOAD_ID "=" TAG(UGC_UPLOAD_ID)

// Upload type accepted by ugclink.aspx
#define UGC_UPLOAD_DOB_GZ "dou" // dob.gz
#define UGC_UPLOAD_TEX_DDS_GZ "dds.gz"
#define UGC_UPLOAD_CUSTOM_TEX "ctu"
#define UGC_UPLOAD_ICON_TEX "itu"
#define UGC_UPLOAD_MEDIA "mu"
#define UGC_UPLOAD_ANM_GZ "cau" // anm.gz
#define UGC_UPLOAD_SND_OGG_KRX "snd" // ogg.krx
#define UGC_UPLOAD_EQP_GZ "equ" // eqp.gz
#define UGC_UPLOAD_MEDIUM_PATTERN "patu"
#define UGC_UPLOAD_MEDIUM_PICTURE "picu"
#define UGC_UPLOAD_PARTICLE "peu" // par
#define UGC_UPLOAD_ACTION_ITEM "act"
#define UGC_UPLOAD_ASSET "ass"

enum UGC_SUBMISSION_STATE {
	PS_DONTCARE = 0x00,
	PS_CREATED = 0x01,
	PS_REQUESTING = 0x02,
	PS_REQUESTED = 0x04,
	PS_UPLOADING = 0x08,
	PS_UPLOADED = 0x10,
	PS_SERVERREADY = 0x20,
	PS_FAILED = 0x40,
	PS_CANCELLED = 0x80,
	PS_REJECTED = 0x100,
};

enum PROPERTY_ID {
	// Object-level (0x10) properties
	PROP_MESH_CNT = 0x1000,
	PROP_VERTEX_CNT = 0x1001,
	PROP_TRIANGLE_CNT = 0x1002,
	PROP_BOUNDBOX_SIZEX = 0x1003,
	PROP_BOUNDBOX_SIZEY = 0x1004,
	PROP_BOUNDBOX_SIZEZ = 0x1005,
	PROP_MATERIAL_CNT = 0x1006,
	PROP_TEXTURE_CNT = 0x1007,
	PROP_DISTINCT_TEXTURE_CNT = 0x1008,
	PROP_BOUNDBOX_VOLUME = 0x1009,
	PROP_BOUNDBOX_MINX = 0x100A,
	PROP_BOUNDBOX_MINY = 0x100B,
	PROP_BOUNDBOX_MINZ = 0x100C,
	PROP_BOUNDBOX_MAXX = 0x100D,
	PROP_BOUNDBOX_MAXY = 0x100E,
	PROP_BOUNDBOX_MAXZ = 0x100F,
	PROP_BOUNDBOX_XYZSUM = 0x1010,
	PROP_MEDIA_PLAYBACK = 0x1011,
	PROP_COLLISION = 0x1012,

	// Mesh-level (0x20) properties
	PROP_MESH_VERTEX_CNT = 0x2001,
	PROP_MESH_TRIANGLE_CNT = 0x2002,
	PROP_MESH_BOUNDBOX_SIZEX = 0x2003,
	PROP_MESH_BOUNDBOX_SIZEY = 0x2004,
	PROP_MESH_BOUNDBOX_SIZEZ = 0x2005,
	PROP_MESH_MATERIAL_CNT = 0x2006,
	PROP_MESH_TEXTURE_CNT = 0x2007,
	PROP_MESH_DISTINCT_TEXTURE_CNT = 0x2008,
	PROP_MESH_BOUNDBOX_VOLUME = 0x2009,
	PROP_MESH_BOUNDBOX_MINX = 0x200A,
	PROP_MESH_BOUNDBOX_MINY = 0x200B,
	PROP_MESH_BOUNDBOX_MINZ = 0x200C,
	PROP_MESH_BOUNDBOX_MAXX = 0x200D,
	PROP_MESH_BOUNDBOX_MAXY = 0x200E,
	PROP_MESH_BOUNDBOX_MAXZ = 0x200F,
	PROP_MESH_BOUNDBOX_XYZSUM = 0x2010,

	// Texture-level (0x40) properties
	PROP_IMAGE_FILESIZE = 0x4000,
	PROP_IMAGE_PIXEL_CNT = 0x4001,
	PROP_IMAGE_WIDTH = 0x4002,
	PROP_IMAGE_HEIGHT = 0x4003,
	PROP_IMAGE_DEPTH = 0x4004,
	PROP_IMAGE_OPACITY = 0x4005,
	PROP_IMAGE_AVG_COLOR = 0x4006,
	PROP_IMAGE_ID = 0x4007,

	// Non-hierarchical (0x80) properties
	PROP_EFFECT_DURATION = 0x8000,
	PROP_EFFECT_FRAME_COUNT = 0x8001,

	PROP_SKELETON_BONE_COUNT = 0x8002,
	PROP_TARGET_ACTOR_TYPE = 0x8003,
	PROP_TARGET_ACTOR_GLID = 0x8004,

	PROP_BOUNDBOXWACTOR_SIZEX = 0x8005, // Skeleton bound box with item equippped
	PROP_BOUNDBOXWACTOR_SIZEY = 0x8006,
	PROP_BOUNDBOXWACTOR_SIZEZ = 0x8007,
	PROP_BOUNDBOXWACTOR_VOLUME = 0x8008,
	PROP_BOUNDBOXWACTOR_MINX = 0x8009,
	PROP_BOUNDBOXWACTOR_MINY = 0x800A,
	PROP_BOUNDBOXWACTOR_MINZ = 0x800B,
	PROP_BOUNDBOXWACTOR_MAXX = 0x800C,
	PROP_BOUNDBOXWACTOR_MAXY = 0x800D,
	PROP_BOUNDBOXWACTOR_MAXZ = 0x800E,
	PROP_BOUNDBOXWACTOR_XYZSUM = 0x800F,

	PROP_EXCLUSION_GROUPS = 0x8010,

	PROP_ACTIONITEM_PARENTSCRIPTGLID = 0x8011,
};

enum UGCImportState {
	UGCIS_NONE = 0,
	UGCIS_INIT,
	UGCIS_PREVIEWING,
	UGCIS_PREVIEWED,
	UGCIS_PREVIEWFAILED,
	UGCIS_IMPORTING,
	UGCIS_IMPORTED,
	UGCIS_IMPORTFAILED,
	UGCIS_PROCESSING,
	UGCIS_PROCESSED,
	UGCIS_PROCESSINGFAILED,
	UGCIS_UPLOADING,
	UGCIS_UPLOADED,
	UGCIS_UPLOADFAILED,
	UGCIS_DESTROYING,
	UGCIS_DESTROYED,
	UGCIS_CANCELED,
	UGCIS_CONVERTING,
	UGCIS_CONVERTED,
	UGCIS_CONVERSIONFAILED,
	UGCIS_STARTING,
	UGCIS_COMMITTING, // a new step between UGCIS_PROCESSING and UGCIS_PROCESSED
	UGCIS_ANY = -1,
};

enum UGCImportEventType {
	EV_NONE = 0,
	EV_OP_DONE,
	EV_OP_FAIL,
	EV_START,
	EV_CONT,
	EV_CANCEL,
	EV_RETRY,
	EV_PREVIEW,
	EV_IMPORT,
	EV_CONVERT,
	EV_PROCESS,
	EV_UPLOAD,
	EV_DESTROY,
	EV_DESTROYED, // special destroyed event -- state obj is not valid when it's received
};

enum class UGCImportAction {
	None,
	Init,
	Preview,
	Import,
	Convert,
	Process,
	Commit,
	Cleanup
};

enum UGCImportError {
	UGCIE_BAD_DAE_FILE = 100, // DAE file is ill-formed
	UGCIE_BAD_DAE_VERSION = 101, // Unsupported COLLADA version
	UGCIE_SIZE_TOO_BIG = 110, // Invalid mesh dimension or total dimension
	UGCIE_SIZE_TOO_SMALL = 111,
	UGCIE_NO_MESH = 120, // No mesh
	UGCIE_TOO_MANY_MESHES = 121, // Too many meshes
	UGCIE_TOO_FEW_TRIANGLES = 122, // Geometry too simple (degenerated)
	UGCIE_TOO_MANY_TRIANGLES = 123, // Geometry too complex
	UGCIE_TOO_MANY_VERTICES = 124, // Too many vertices
	UGCIE_TOO_MANY_UVSETS = 125, // Too many sets of UVs
	UGCIE_TOO_MANY_TEXTURES = 126, // Too many textures (per mesh or total)
	UGCIE_TOO_MANY_BONES = 127, // Too many bones in the skeleton
	UGCIE_EXTRA_BONES = 130, // Extra bone found while comparing to reference skeleton
	UGCIE_MISSING_BONES = 131, // Missing bone found while comparing to reference skeleton
	UGCIE_DURATION_TOO_SHORT = 140, // Duration is too short
	UGCIE_DURATION_TOO_LONG = 141, // Duration is too long
	UGCIE_NO_ANIMATION = 150, // No animation found while importing an animation
	UGCIE_NO_SKELETON = 151, // No skeleton found while importing an animation

	UGCIE_MANDATORY_VALIDATION_FAILED = 200, // Client-side mandatory validation failed

	UGCIE_OUT_OF_MEMORY = 900, // Insufficient memory to process DAE file

	UGCIE_UNSPECIFIED_ERROR = 1000, // Exception (not a valid error code): import fails without explicit error code.
	UGCIE_SESSION_NOT_FOUND = 1001, // Exception (not a valid error code): import session not found
};

} // namespace KEP