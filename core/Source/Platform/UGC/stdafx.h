// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>

#include <set>
#include <map>
#include <vector>
#include <stack>
#include <algorithm>

#include "Common\include\KEPDiag.h"
#include "assert.h"

#ifndef VERIFY_RETURN
#define VERIFY_RETURN(cond, rv) assert(cond); if (!(cond)) return (rv);
#endif

#ifndef VERIFY_RETVOID
#define VERIFY_RETVOID(cond) assert(cond); if (!(cond)) return;
#endif

#if !defined(_NullCk) && defined(_DEBUG) && !defined(_NOINT3)
#define _NullCk(x) ((int) jsAssert(x!=NULL), x)
#else
#define _NullCk(x) x
#endif

#define DIRECTINPUT_VERSION 0x0800