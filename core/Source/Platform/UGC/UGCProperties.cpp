///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <math.h>
#include <sstream>
#include "UGCProperties.h"

namespace KEP {

Value::Value(VLD_VALUE_TYPE aType) :
		type(aType) {
	init();
}

Value::Value(float aValue) :
		type(VVT_FLOAT) {
	init();
	setValue(aValue);
}

Value::Value(int aValue) :
		type(VVT_INT) {
	init();
	setValue(aValue);
}

Value::Value(bool aValue) :
		type(VVT_BOOL) {
	init();
	setValue(aValue);
}

Value::Value(const std::string& aValue) :
		type(VVT_STRING) {
	init();
	setValue(aValue);
}

void Value::init() {
	bound = false;
	floatValue = (std::numeric_limits<float>::max)();
	intValue = (std::numeric_limits<int>::max)();
	boolValue = false;
	stringValue = "";
}

void Value::setValue(float aValue) {
	assert(type == VVT_FLOAT);
	bound = true;
	floatValue = aValue;
}

void Value::setValue(int aValue) {
	assert(type == VVT_INT);
	bound = true;
	intValue = aValue;
}

void Value::setValue(bool aValue) {
	assert(type == VVT_BOOL);
	bound = true;
	boolValue = aValue;
}

void Value::setValue(const std::string& aValue) {
	assert(type == VVT_STRING);
	bound = true;
	stringValue = aValue;
}

float Value::getFloat() const {
	assert(bound && type == VVT_FLOAT);
	return floatValue;
}

int Value::getInt() const {
	assert(bound && type == VVT_INT);
	return intValue;
}

bool Value::getBoolean() const {
	assert(bound && type == VVT_BOOL);
	return boolValue;
}

std::string Value::getString() const {
	assert(bound && type == VVT_STRING);
	return stringValue;
}

std::string Value::toString() const {
	std::stringstream ss;
	ss << *this;
	return ss.str();
}

std::ostream& operator<<(std::ostream& os, const Value& val) {
	assert(val.isBound());

	if (val.isBound()) {
		switch (val.type) {
			case VVT_FLOAT:
				os << val.floatValue;
				break;

			case VVT_INT:
				os << val.intValue;
				break;

			case VVT_BOOL:
				os << val.boolValue;
				break;

			case VVT_STRING:
				os << val.stringValue;
				break;

			default:
				os << "*INVALIDTYPE*";
				break;
		}
	} else {
		os << "*UNBOUNDVALUE*";
	}

	return os;
}

} // namespace KEP