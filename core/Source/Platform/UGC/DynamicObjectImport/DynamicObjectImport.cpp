///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "../UGC_Global.h"
#include "../UGCProperties.h"
#include "../UGCManager.h"
#include "../UGCItemUploadState.h"
#include "DynamicObjectImport.h"
#include "DynamicObjectProperties.h"
#include "Common\include\IAssetsDatabase.h"
#include "Common\include\AssetTree.h"
#include "Common\AssetsDatabase\AssetURI.h"
#include "Common\include\CoreStatic\StreamableDynamicObj.h"
#include "Common\include\CoreStatic\CEXMeshClass.h"
#include "ResourceManagers\ReMeshCreate.h"
#include "ResourceManagers/DynamicObjectManager.h"
#include "Common\include\IClientEngine.h"
#include "Common\include\CoreStatic\DynamicObj.h"
#include "Editor\ImporterBlades\ColladaImporter\ColladaClasses.h"
#include "Editor\ImporterBlades\ColladaImporter\AssetConverter.h"
#include "Common\AssetsDatabase\TextureHelper_nvtt.h"
#include "..\UGCImportStateObserver.h"
#include "DynamicObjectCreate.h"
#include "RuntimeSkeleton.h"
#include "SkeletonConfiguration.h"
#include "ResourceManagers\SkeletonManager.h"
#include "Common/include/CoreStatic/CSkeletonObject.h"
#include "CDeformableMesh.h"
#include "MaterialPersistence.h"
#include "LogHelper.h"
#include <Psapi.h>
#pragma comment(lib, "Psapi.lib")

namespace KEP {

static LogInstance("UGCImport");

static DWORD GetMemoryUsage() {
	PROCESS_MEMORY_COUNTERS pmc;
	if (!GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc)))
		return 0;
	return pmc.WorkingSetSize;
}

// DRF - Profile RAII
struct Profile {
	std::string id;
	TimeMs sTime;
	SIZE_T sMem;
	Profile(std::string mid) :
			id(mid), sTime(fTime::TimeMs()), sMem(GetMemoryUsage()) {}
	~Profile() {
		TimeMs timeMs = fTime::ElapsedMs(sTime);
		SIZE_T eMem = GetMemoryUsage();
		long dMem = eMem - sMem;
		_LogInfo("PROFILE(" << id << "): ms=" << FMT_TIME << timeMs << " mem=[" << sMem / 1024 << "k -> " << eMem / 1024 << "k = " << dMem / 1024 << "k]");
	}
};
#define PROFILE volatile Profile profile(__FUNCTION__)

#define SCALEFACTOR_MIN 1E-8f

static ClassifyAssetNodeStd classifyFunc;

DynamicVisLODCreate::~DynamicVisLODCreate() {
	if (m_pOriginalMesh) {
		m_pOriginalMesh->SafeDelete();
		delete m_pOriginalMesh;
		m_pOriginalMesh = NULL;
	}

	if (m_pBakedMesh) {
		m_pBakedMesh->SafeDelete();
		delete m_pBakedMesh;
		m_pBakedMesh = NULL;
	}
}

CEXMeshObj* DynamicVisLODCreate::bakeMesh(const Matrix44f& newMatrix) {
	if (m_pBakedMesh) {
		m_pBakedMesh->SafeDelete();
		delete m_pBakedMesh;
		m_pBakedMesh = NULL;
	}

	m_pOriginalMesh->Clone(&m_pBakedMesh, NULL);
	if (m_pBakedMesh) {
		m_pBakedMesh->SetWorldTransform(newMatrix);
		m_pBakedMesh->BakeVertices(); // Bake matrix into vertices
		m_pBakedMesh->ComputeBoundingVolumes(); // Compute bounding volumes
		m_pBakedMesh->OptimizeMesh(); // Merge duplicated vertices
	}
	return m_pBakedMesh;
}

DynamicObjectCreate::DynamicObjectCreate() :
		m_isAttachable(false), m_isInteractive(false), m_pNamingSyntax(NULL), m_importedObject(NULL), m_glid(GLID_INVALID), m_assets(NULL), m_skeleton(NULL), m_light(NULL), m_propSrc(NULL) {
	m_light = new LightCreate();
}

DynamicObjectCreate::~DynamicObjectCreate() {
	for (auto pr : m_allMeshes) {
		if (pr.second) {
			delete pr.second;
		}
	}

	m_allMeshes.clear();
	m_gsMeshes.clear();

	for (std::vector<TextureCreate*>::iterator itTex = m_allTextures.begin(); itTex != m_allTextures.end(); ++itTex) {
		if (*itTex) {
			delete *itTex;
		}
	}

	m_allTextures.clear();
	m_gsTextures.clear();

	if (m_propSrc) {
		delete m_propSrc;
	}

	if (m_assets) {
		delete m_assets;
	}

	if (m_light) {
		delete m_light;
	}
}

PropertySource* DynamicObjectCreate::getPropertySource(bool bRefresh) {
	if (bRefresh && m_propSrc) {
		delete m_propSrc;
		m_propSrc = NULL;
	}

	if (!m_importedObject) {
		return NULL;
	}

	if (!m_propSrc) {
		m_propSrc = new DynamicObjectProperties(*m_importedObject);
	}

	return m_propSrc;
}

void DynamicObjectCreate::disposeImportedObject() {
	if (m_importedObject) {
		delete m_importedObject;
	}
	m_importedObject = NULL;
	m_importedObjectGetSet = NULL;
}

class MeshGroupingHelper {
public:
	enum MeshType {
		DONTCARE = -1,
		TRILIST,
		TRISTRIP
	};

	struct Key {
		CMaterialObject* material; // Material and texture settings must match
		MeshType meshType; // Mesh type must match: triangle list or triangle strip (both indexed)
		int numUVs; // UV count must match
		unsigned collisionType; // Collision type must match
		bool isCustomizable; // Texture customization flag must match
		bool mediaSupported; // Media meshes should not be merged
		std::string groupDiscriminator; // Set unique values for meshes that should not be merged (see Key::Key)

		Key(DynamicVisCreate* pVisCreate);
		bool operator<(const Key& rhs) const;
	};

	struct LODData {
		std::vector<std::pair<unsigned, unsigned>> vertexAndIndexCounts; // vertex and index count
		int vertexUVCount; // vertex UV count
		MeshType meshType; // mesh type

		ReMesh* remesh;

		LODData() :
				vertexUVCount(-1), meshType(DONTCARE), remesh(nullptr) {}
	};

	struct Group {
		std::vector<LODData> LODs;
		std::vector<DynamicVisCreate*> visuals;
	};

	MeshGroupingHelper(DynamicObjectCreate* pCreateStruct);
	~MeshGroupingHelper();

	std::map<Key, Group>& getMap() {
		return m_groups;
	}
	const std::map<Key, Group>& getMap() const {
		return m_groups;
	}

private:
	std::map<Key, Group> m_groups;
};

IUGCImportStateObj* DynamicObjectImport::createState() {
	return new DynamicObjectCreate;
}

void DynamicObjectImport::disposeState(IUGCImportStateObj* state) {
	if (state) {
		delete state;
	}
}

bool DynamicObjectImport::preview(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	auto pDOC = dynamic_cast<DynamicObjectCreate*>(pStateObj);
	if (!pDOC)
		return false;

	bool res = true;

	std::lock_guard<fast_recursive_mutex> lock(pDOC->getMutex());

	assert(pDOC->m_state == UGCIS_PREVIEWING);
	if (pDOC->m_state == UGCIS_PREVIEWING) {
		PROFILE; // drf - profiler raii

		// Let's do some experiments... I'm interested in skinned meshes and
		// skeletons, too.  Can I find them?  If I ask for skeletal objects,
		// it'll check for skinned meshes too.
		AssetTree* pAssets = IAssetsDatabase::Instance()->OpenAndPreviewURI(pDOC->m_rootUri, KEP_SKA_SKELETAL_OBJECT, IMP_GENERAL_NOUI, &classifyFunc);
		AssetTree* pStaticMeshes = NULL;
		AssetTree* pSkinnedMeshes = NULL;
		AssetTree* pSkeletons = NULL;
		if (pAssets != NULL) {
			pStaticMeshes = AssetData::GetAssetGroup(pAssets, KEP_STATIC_MESH);
			pSkinnedMeshes = AssetData::GetAssetGroup(pAssets, KEP_SKA_SKINNED_MESH);
			pSkeletons = AssetData::GetAssetGroup(pAssets, KEP_SKA_SKELETAL_OBJECT);
		}
		pDOC->setAssets(pAssets);

		if (pSkeletons != NULL) {
			for (AssetTree::pre_order_iterator it = pSkeletons->pre_order_begin(); it != pSkeletons->pre_order_end(); ++it) {
				AssetData& asset = *it;
				if (asset.GetType() != KEP_SKA_SKINNED_MESH && asset.GetType() != KEP_SKA_SKELETAL_OBJECT) {
					continue;
				}
				asset.SetFlag(AssetData::STA_SELECTED);
			}
		}

		if (!pSkinnedMeshes && !pStaticMeshes) {
			pDOC->ReportError(UGCIE_NO_MESH, "DynamicObjectImport::preview(): Unable to find a mesh in the import file.", RA_NORECOVERY, true);
			res = false;
		} else {
			// res = false if it doesn't have any meshes in it, skinned and/or static, even
			// if it defines a skeleton.
			if (pSkinnedMeshes != NULL) {
				for (AssetTree::pre_order_iterator it = pSkinnedMeshes->pre_order_begin(); it != pSkinnedMeshes->pre_order_end(); ++it) {
					AssetData& asset = *it;

					if (asset.GetType() != KEP_SKA_SKINNED_MESH && asset.GetType() != KEP_SKA_SKELETAL_OBJECT) {
						continue;
					}

					asset.SetFlag(AssetData::STA_SELECTED);
					// process the LOD to create the IDynamicVisCreate
					// GetSet object I need to get collision flags from
					// the GUI.
					if (!processLOD(it.node(), pDOC, true)) {
						pDOC->ReportError(UGCIE_NO_MESH, "DynamicObjectImport::preview(): Unable to process a skinned mesh LOD.", RA_NORECOVERY, true);
						res = false;
						break;
					}
				}
			}

			if (pStaticMeshes != NULL) {
				for (AssetTree::pre_order_iterator it = pStaticMeshes->pre_order_begin(); it != pStaticMeshes->pre_order_end(); ++it) {
					AssetData& asset = *it;

					if (asset.GetType() != KEP_STATIC_MESH && asset.GetType() != KEP_SKA_SKELETAL_OBJECT) {
						continue;
					}

					if (!processLOD(it.node(), pDOC, false)) {
						pDOC->ReportError(UGCIE_NO_MESH, "DynamicObjectImport::preview(): Unable to process a static mesh LOD.", RA_NORECOVERY, true);
						res = false;
						break;
					}
				}
			}
		}
	} else {
		// Invalid state
		assert(false);
		res = false;
	}

	// Update import state
	if (pObserver)
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);

	return res;
}

bool DynamicObjectImport::processLOD(const AssetTree* pAssetNode, DynamicObjectCreate* const pDOC, bool isDynamic) const {
	const AssetData* pAsset = pAssetNode->get();

	switch (pAsset->GetLevel()) {
		case AssetData::LVL_OBJECT:
		case AssetData::LVL_PART:
		case AssetData::LVL_VARIATION:
			break;

		case AssetData::LVL_LOD: {
			assert(pAsset->GetNumSubmeshes() > 0);
			bool bMultiMat = pAsset->GetNumSubmeshes() > 1;

			const AssetTree* pParentNode = _NullCk(pAssetNode->parent());
			const AssetData* pParentAsset = _NullCk(pParentNode->get());

			for (int subMeshId = 0; subMeshId < pAsset->GetNumSubmeshes(); subMeshId++) {
				std::string key = pParentAsset->GetKey();
				if (bMultiMat) {
					std::stringstream ss;
					ss << pParentAsset->GetKey() << "[" << subMeshId << "]";
					key = ss.str();
				}

				DynamicVisCreate* pVisCreate = pDOC->getVisInfoByName(key);
				if (!pVisCreate) {
					// Create and record visual settings struct
					pVisCreate = new DynamicVisCreate;

					// Initialize from template
					const DynamicVisCreate& tmplt = pDOC->getVisTemplate();
					pVisCreate->setName(key);
					pVisCreate->setCustomizable(tmplt.isCustomizable());
					pVisCreate->SetMediaSupported(tmplt.MediaSupported());
					pVisCreate->setHasLOD(tmplt.getHasLOD());
					pVisCreate->setCollisionType(tmplt.getCollisionType());
					pVisCreate->setSubMeshId(subMeshId);
					pVisCreate->setDynamic(isDynamic);

					pDOC->addVisInfo(pVisCreate);
				}

				// Add per-LOD settings to visual settings struct
				DynamicVisLODCreate* pLodInfo = new DynamicVisLODCreate;
				pLodInfo->setLodLevel(pAsset->GetLODLevel());
				pLodInfo->setMeshUri(pAsset->GetURI());
				if (bMultiMat) {
					std::stringstream ss;
					ss << pAsset->GetURI() << "[" << subMeshId << "]";
					pLodInfo->setMeshUri(ss.str());
				}
				pLodInfo->setAssetDef(*pAsset);
				pVisCreate->addLODInfo(pLodInfo);
			}
		} break;
	}

	return true;
}

bool DynamicObjectImport::import(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	auto pDOC = dynamic_cast<DynamicObjectCreate*>(pStateObj);
	if (!pDOC)
		return false;

	bool res = true;

	std::lock_guard<fast_recursive_mutex> lock(pDOC->getMutex());

	assert(pDOC->m_state == UGCIS_IMPORTING);
	if (pDOC->m_state == UGCIS_IMPORTING) {
		PROFILE; // drf - profiler raii

		// Check to see if we need to import skeletons
		// and skinned meshes.
		if (pDOC) {
			// Based on editor import and UGCEquippableObjectImport code.
			int flags = IMP_SKELETAL_SINGLESLOT |
						IMP_SKELETAL_SINGLECONFIG |
						IMP_GENERAL_MULTISUBMESH |
						IMP_GENERAL_NOUI |
						IMP_SKELETAL_SKINNEDMESHES |
						IMP_SKELETAL_NODMESHMAT |
						IMP_GENERAL_SINGLELOD;
			// ColladaImporter::Import() will import skeletons, skinned meshes and animations,
			// depending on how the flags are set above.
			//
			CSkeletonObject* pNewSkeleton = IAssetsDatabase::Instance()->ImportSkeletalObjectByAssetTree(pDOC->m_rootUri, flags, pDOC->getAssets());
			pDOC->setSkeleton(pNewSkeleton);
			if (pNewSkeleton) {
				res = true;
			}
		}

		// Import rigid meshes
		std::set<std::string> allVisNames;
		pDOC->getAllVisNames(allVisNames);
		for (const auto& visName : allVisNames) {
			DynamicVisCreate* pVisCreate = pDOC->getVisInfoByName(visName);
			if (pVisCreate->isDynamic())
				continue; // this isn't a rigid mesh

			for (int lodLoop = 0; lodLoop < pVisCreate->getLODCount(); ++lodLoop) {
				DynamicVisLODCreate* pLodCreate = pVisCreate->getLODInfo(lodLoop);

				int flags = IMP_GENERAL_MULTIPART | IMP_GENERAL_SINGLESUBMESH | IMP_GENERAL_NOUI;
				flags |= pVisCreate->getHasLOD() ? IMP_GENERAL_MULTILOD : IMP_GENERAL_SINGLELOD;

				//Assuming this is in Client so we would like to limit texture size
				//If dynamic object importer will be called from Editor in the future, I'll need to change the interface.
				flags |= IMP_TEXTURE_POWEROF2_ROUNDING | IMP_TEXTURE_SIZE_LIMIT_LARGE;

				if (IAssetsDatabase::Instance()->ImportStaticMeshByAssetDefinition(pDOC->m_rootUri, flags, pLodCreate->getAssetDefRef(), pVisCreate->getSubMeshId())) {
					CEXMeshObj* tmpMeshObj = pLodCreate->getAssetDefRef().ExtractExMesh(0);
					pLodCreate->setOrigMesh(tmpMeshObj);
					if (!tmpMeshObj)
						res = false;
				}
			}
		}
	} else {
		// Invalid state
		assert(false);
		res = false;
	}

	if (pObserver)
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);

	return res;
}

bool DynamicObjectImport::convert(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	auto pDOC = dynamic_cast<DynamicObjectCreate*>(pStateObj);
	if (!pDOC)
		return false;

	std::lock_guard<fast_recursive_mutex> lock(pDOC->getMutex());

	bool res = true;

	if (pDOC->m_scaleFactor < SCALEFACTOR_MIN) { // scale factor must be > 0
		if (pDOC->m_scaleFactor < 0)
			pDOC->m_scaleFactor = 1.0f; // reset to 1.0f if negative
		else
			pDOC->m_scaleFactor = SCALEFACTOR_MIN; // or use minimum if just too small
	}

	assert(pDOC->m_state == UGCIS_CONVERTING);
	if (pDOC->m_state == UGCIS_CONVERTING) {
		PROFILE; // drf - profiler raii

		StreamableDynamicObject* pSDO = pDOC->getImportedObject();

		if (pSDO) { // Already converted, now dispose old one -- must be careful this object shouldn't have been used by the engine
			pDOC->disposeImportedObject();
			pSDO = NULL;
		}

		GLID glid = ResourceManager::AllocateLocalGLID();
		pSDO = new StreamableDynamicObject();
		pSDO->setName(pDOC->getName().c_str());
		pSDO->setAttachable(pDOC->isAttachable());
		pSDO->setInteractive(pDOC->isInteractive());

		// NOTE: consider fixing the inconsistency between importing skeletal meshes and static meshes.
		// Currently all skeletal meshes are processed and added to resource manager by Collada importer
		// while static meshes have to be processed afterwards in DO importer.

		// 1) Skeleton and skinned meshes (including meshes controlled by one single bone weighted at 100%)
		res = res && convertSkeletonWithMeshes(pDOC, pSDO); // Note: boolean expression short circuit

		// 2) Import object light
		res = res && convertLight(pDOC, pSDO); // Note: boolean expression short circuit

		// 3) Import attached particle system
		res = res && convertParticleSystem(pDOC, pSDO); // Note: boolean expression short circuit

		// 4) Rigid meshes as direct children of the object
		res = res && convertStaticMeshes(pDOC, pSDO); // Note: boolean expression short circuit

		// 5) Setup bounding box
		if (res) {
			BNDBOX boundBox = pSDO->computeBoundingBox();
			pSDO->setBoundBox(boundBox);
			pDOC->m_boundBoxMin = boundBox.min;
			pDOC->m_boundBoxMax = boundBox.max;

			pDOC->setImportedObject(pSDO, glid);
			pDOC->m_importedObjectGetSet = static_cast<IGetSet*>(pSDO);
			assert(pDOC->m_importedObjectGetSet != NULL);
		}
	} else {
		// Invalid state
		assert(false);
		res = false;
	}

	if (pObserver != NULL) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool DynamicObjectImport::process(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	auto pDOC = dynamic_cast<DynamicObjectCreate*>(pStateObj);
	if (!pDOC)
		return false;

	StreamableDynamicObject* pSDO = pDOC->getImportedObject();

	std::lock_guard<fast_recursive_mutex> lock(pDOC->getMutex());

	bool res = false;

	assert(pDOC->m_state == UGCIS_PROCESSING);
	if (pDOC->m_state == UGCIS_PROCESSING) {
		PROFILE; // drf - profiler raii

		assert(pSDO != NULL);
		if (pSDO) {
			// Copy ambient from m_useMaterial to m_baseMaterial
			for (size_t dovIndex = 0; dovIndex < pSDO->getVisualCount(); dovIndex++) {
				StreamableDynamicObjectVisual* pDynObjVis = pSDO->Visual(dovIndex);
				pDOC->ProcessMaterial(*pDynObjVis->getMaterial(), true);

				// Configure collision on a per-visual basis (so don't use the DO's
				// setCollisionSourceByLOD()) function anymore.  And don't build
				// collision for the whole DO at all.

				// UPDATE 09/2015: StreamableDynamicObjectVisual and DynamicVisCreate no longer have
				// a one-to-one mapping due to mesh merging. getVisInfoByDynObjVisualIndex will return
				// a proper DynamicVisCreate pointer based on its internal map.
				DynamicVisCreate* pVisCreate = pDOC->getVisInfoByName((const char*)pDynObjVis->getName());
				assert(pVisCreate != NULL);
				if (!pVisCreate) {
					continue; // drf - crash fix line 568
				}

				// Can Collide ?
				bool canCollide = (pVisCreate->getCollisionType() != UGC_COL_NONE);
				if (canCollide) {
					pDynObjVis->setCollisionSource(0);
				} else {
					pDynObjVis->setCanCollide(false);
				}

				//Set UGC dynamic object visuals to be customizable (for applying UGC DO textures)
				bool customizable = pVisCreate->isCustomizable();
				pSDO->SetVisualCustomizable(dovIndex, customizable);

				//Media supported flag is already set for static meshes (and propagated to DO).
				//Skinned meshes do not play flash at this moment (unfortunately the UI allows
				//selecting them for media) and the flag is not enabled for them (see SDO::AddVisual
				//calls above) anyway.
			}
			pSDO->createCollisionShapes();

			res = true;
		}
	}

	if (pObserver) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool DynamicObjectImport::commit(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	auto pDOC = dynamic_cast<DynamicObjectCreate*>(pStateObj);
	if (!pDOC)
		return false;

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	StreamableDynamicObject* pSDO = pDOC->getImportedObject();

	std::lock_guard<fast_recursive_mutex> lock(pDOC->getMutex());

	bool res = false;

	assert(pDOC->m_state == UGCIS_COMMITTING);
	if (pDOC->m_state == UGCIS_COMMITTING) {
		assert(pSDO != NULL);
		if (pSDO) {
			// Create IResource
			GLID glid = pDOC->getGlid();
			pDOC->m_localGLID = glid;
			auto pRes = AddResourceToManager<TResource<StreamableDynamicObject>>((UINT64)glid, glid, true);
			assert(pRes != nullptr);
			pRes->SetAsset(pSDO);

			// Add to global runtime list
			assert(pICE->GetUGCManager());
			if (pICE->GetUGCManager()) {
				pICE->GetUGCManager()->AddUGCObj(new UGCDynamicObj(pSDO, glid, pRes));
			}

			// Add skeleton to resource manager.
			// Use a different GLID to the dynamic object itself.
			if (pDOC->getSkeleton()) {
				GLID skeletonGLID = ResourceManager::AllocateLocalGLID();
				pSDO->setSkeletonGLID(skeletonGLID);
				if (!SkeletonRM::Instance()->AddRuntimeSkeleton(skeletonGLID, pDOC->getSkeleton()->getRuntimeSkeleton())) {
					res = false; // just a test
				}
			}
			res = true;
		}
	}

	if (pObserver) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool DynamicObjectImport::cleanup(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	auto pDOC = dynamic_cast<DynamicObjectCreate*>(pStateObj);
	if (!pDOC || (pDOC->m_state == UGCIS_DESTROYED))
		return false;

	PROFILE; // drf - profiler raii
	try {
		{
			std::lock_guard<fast_recursive_mutex> lock(pDOC->getMutex());
			IAssetsDatabase::Instance()->CloseURI(pDOC->m_rootUri, KEP_STATIC_MESH, IMP_GENERAL_NOUI);

			if (pObserver) {
				pObserver->doUpdate(pStateObj, EV_DESTROYED);
			}
		}

		disposeState(pDOC);
	} catch (CException*) {
		LogError("FAILED");
		assert(false);
		return false;
	}

	return true;
}

bool DynamicObjectImport::convertSkeletonWithMeshes(DynamicObjectCreate* pDOC, StreamableDynamicObject* pSDO) {
	if (!pDOC)
		return false;

	if (!pDOC->getSkeleton())
		return true;

	RuntimeSkeleton* pRS = pDOC->getSkeleton()->getRuntimeSkeleton();

	pRS->scaleSystem(pDOC->m_scaleFactor, pDOC->m_scaleFactor, pDOC->m_scaleFactor);

	// Create visuals for skinned meshes here.
	// They mesh data should have been added to the mesh pools during the import process.
	// Import skinned meshes for each slot
	// Sometimes, m_meshSlots can still be null at this point is that a bug?
	SkeletonConfiguration* pSC = pDOC->getSkeleton()->getSkeletonConfiguration();
	if (pSC && pSC->m_meshSlots) {
		for (POSITION p = pSC->m_meshSlots->GetHeadPosition(); p != NULL;) {
			CAlterUnitDBObject* pAltDB = dynamic_cast<CAlterUnitDBObject*>(pSC->m_meshSlots->GetNext(p));

			// for each configuration
			int meshIdx = 0;
			for (POSITION pp = pAltDB->m_configurations->GetHeadPosition(); pp != NULL; ++meshIdx) {
				auto pSDOV_new = new StreamableDynamicObjectVisual;
				CDeformableMesh* pMesh = dynamic_cast<CDeformableMesh*>(pAltDB->m_configurations->GetNext(pp));

				// add the mesh hashes to the visual for each lod
				for (unsigned int i = 0; i < pMesh->m_lodCount; ++i) {
					unsigned __int64 hash = pMesh->m_lodHashes[i];
					pSDOV_new->addLOD(hash); // what about distance?
				}

				// Add a material
				// uses a clone of the supplied material
				pSDOV_new->setMaterial(pMesh->m_material, IMPORTED_TEXTURE);

				// Copy over mesh name
				assert(!pMesh->m_dimName.IsEmpty());
				pSDOV_new->setName(pMesh->m_dimName);

				// add the visual to the object
				// I should experiment with allowing flash on a skinned mesh.
				// That would be cool.
				pSDO->AddVisual(pSDOV_new, false, false);
			}
		}
	}

	return true;
}

bool DynamicObjectImport::convertStaticMeshes(DynamicObjectCreate* pDOC, StreamableDynamicObject* pSDO) {
	if (!pDOC)
		return false;

	bool res = true;

	// Sort and merge meshes by a set of properties: material, textures, collisionType, isDynamic, canPlayFlash, isCustomizable and hasLOD
	LogInfo("Original mesh count: " << pDOC->getVisCount() << ", input: [" << pDOC->m_rootUri << "]");

	// Sort visuals by key
	MeshGroupingHelper mg(pDOC);

	std::set<std::string> texturesScanned;

	// Build dynamic object visuals
	for (auto& pr : mg.getMap()) {
		const auto& key = pr.first;
		auto& grp = pr.second;
		assert(!grp.visuals.empty());

		DynamicVisCreate* pVisCreate0 = grp.visuals[0];

		auto pSDOV_new = new StreamableDynamicObjectVisual;
		pSDOV_new->setName(pVisCreate0->getName().c_str()); // Must inherit name from the group head (first visual) for future DynamicVisCreate map lookup

		assert(pVisCreate0->getLODCount() == 1 || grp.visuals.size() == 1); // No grouping if multiple LODs

		// All meshes
		for (int lodLoop = 0; lodLoop < pVisCreate0->getLODCount(); ++lodLoop) {
			assert(lodLoop < grp.LODs.size());
			auto& LODHelper = grp.LODs[lodLoop];

			assert(LODHelper.vertexUVCount != -1);
			assert(LODHelper.meshType != MeshGroupingHelper::DONTCARE);

			assert(LODHelper.remesh == nullptr);
			LODHelper.remesh = MeshRM::Instance()->AllocateReStaticMesh(LODHelper.vertexAndIndexCounts, LODHelper.vertexUVCount, LODHelper.meshType == MeshGroupingHelper::TRISTRIP);

			for (auto pVisCreate : grp.visuals) {
				DynamicVisLODCreate* pLODInfo = pVisCreate->getLODInfo(lodLoop);
				assert(pLODInfo != NULL);
				if (!pLODInfo) {
					continue;
				}

				CEXMeshObj* bakedEXMesh = pLODInfo->getBakedMesh();
				if (bakedEXMesh) {
					// Correct material ambient color if requested
					pDOC->ProcessMaterial(bakedEXMesh->GetMaterial(), false);

					// Create the ReMaterial
					MeshRM::Instance()->CreateReMaterial(bakedEXMesh->m_materialObject, IMPORTED_TEXTURE);

					// Copy material to visual if LOD0 or the only LOD
					if (pLODInfo->getLodLevel() == 0 || pVisCreate->getLODCount() == 1)
						pSDOV_new->setMaterial(&bakedEXMesh->GetMaterial(), IMPORTED_TEXTURE);

					// Scan all textures
					CMaterialObject* pMaterial = bakedEXMesh->m_materialObject;
					if (pMaterial) {
						for (UINT texID = 0; texID < NUM_CMATERIAL_TEXTURES; texID++) {
							if (pMaterial->m_texNameHash[texID] != (DWORD)-1) // -1 = not used
							{
								std::string texFileName = pMaterial->m_texFileName[texID];
								if (texturesScanned.find(texFileName) != texturesScanned.end()) {
									// Already processed
									continue;
								} else {
									texturesScanned.insert(texFileName);
								}

								bool textureFound = true;
								bool texValid = ValidTextureFileName(texFileName);
								if (texValid) {
									std::string originalFileName;
									if (!TextureHelper::GetRawImagePath(texFileName, originalFileName)) {
										textureFound = false;
										/*bool bFoundMissing = */ TextureHelper::GetRawImagePath(texFileName, originalFileName, true);
										pDOC->m_missingTextures.push_back(originalFileName);
									}
								}

								if (textureFound) {
									// Instantiate TextureCreate
									TextureCreate* pTexCreate = new TextureCreate(pDOC->getTexureTemplate());

									pTexCreate->setFullPath(texFileName);
									pDOC->addTextureInfo(pTexCreate);
								} else {
									if (texValid) {
										LogError("Texture File Not Found - texFileName='" << texFileName << "'");
									} else {
										LogWarn("Texture File Missing");
									}
								}
							}
						}
					}

					if (bakedEXMesh != nullptr) {
						if (LODHelper.remesh != nullptr) {
							// Assign material to remesh if not already
							if (LODHelper.remesh->GetMaterial() == nullptr) {
								LODHelper.remesh->SetMaterial(bakedEXMesh->GetMaterial().GetReMaterial());
							}

							// append EXMesh into ReMesh
							MeshRM::Instance()->AppendEXMesh(LODHelper.remesh, bakedEXMesh);
						}
					}
				}
			} // Visual loop

			if (LODHelper.remesh) {
				// Build remesh
				MeshRM::Instance()->BuildReMesh(LODHelper.remesh);

				// Add remesh to pool
				MeshRM::Instance()->AddMesh(LODHelper.remesh, MeshRM::RMP_ID_DYNAMIC);
				pSDOV_new->addLOD(LODHelper.remesh->GetHash());
			} else {
				unsigned totalVertexCount = 0, totalIndexCount = 0;
				for (auto counts : LODHelper.vertexAndIndexCounts) {
					totalVertexCount += counts.first;
					totalIndexCount += counts.second;
				}

				LogError("Failed to create resource for imported mesh"
						 << ": vcnt=" << totalVertexCount
						 << ", icnt=" << totalIndexCount
						 << ", uvcnt=" << LODHelper.vertexUVCount);

				pDOC->ReportError(UGCIE_OUT_OF_MEMORY, "The 3D object you are trying to import is too complex.", RA_NORECOVERY, true);
				pSDOV_new->addLOD(0);

				res = false; // Return false and change state to Conversion_Failed
			}

		} // LOD loop

		if (!pSDOV_new->getMaterial()) {
			assert(false);
			LogError("FAILED getMaterial()");

			// Use a default if no material specified
			CMaterialObject newMat;
			pDOC->ProcessMaterial(newMat, false);
			pSDOV_new->setMaterial(&newMat); // material will be cloned inside
		}

		pSDO->AddVisual(pSDOV_new, key.isCustomizable, key.mediaSupported);
	}

	LogInfo("Final mesh count: " << pSDO->getVisualCount() << ", input: [" << pDOC->m_rootUri << "]");
	return res;
}

bool DynamicObjectImport::convertLight(DynamicObjectCreate* pDOC, StreamableDynamicObject* pSDO) {
	if (!pDOC)
		return false;

	LightCreate* csLight = pDOC->getLight();
	if (csLight->isEnabled()) {
		StreamableDynamicObjectLight* pLight;
		pLight = new StreamableDynamicObjectLight(
			csLight->getR(), csLight->getG(), csLight->getB(), csLight->getRange(),
			csLight->getAttenuation0(), csLight->getAttenuation1(), csLight->getAttenuation2());
		pSDO->setLight(pLight);
		pSDO->setLightPosition(csLight->getPosition().x, csLight->getPosition().y, csLight->getPosition().z);
	}

	return true;
}

bool DynamicObjectImport::convertParticleSystem(DynamicObjectCreate* pDOC, StreamableDynamicObject* pSDO) {
	if (!pDOC)
		return false;

	ParticleCreate& csParticle = pDOC->getParticle();
	if (csParticle.isEnabled()) {
		pSDO->setParticleEffectId(csParticle.getParticleId());
		pSDO->setParticleOffset(csParticle.getPosition().x, csParticle.getPosition().y, csParticle.getPosition().z);
	}

	return true;
}

MeshGroupingHelper::Key::Key(DynamicVisCreate* pVisCreate) :
		material(nullptr),
		meshType(DONTCARE),
		numUVs(-1),
		collisionType(0),
		isCustomizable(false),
		mediaSupported(false),
		groupDiscriminator(pVisCreate->getName()) {
	this->collisionType = pVisCreate->getCollisionType();
	this->isCustomizable = pVisCreate->isCustomizable();
	this->mediaSupported = pVisCreate->MediaSupported();

	// Do not merge if:
	//   1) multi-LOD, or
	//   2) flash surface, or
	//   3) non-indexed mesh (arbitrary choice for simplicity, I believe imported meshes are always indexed)
	// By default group discriminators are set to different value for each meshes (i.e. no merging)

	// If meshes can be merged, merge them by following common properties: material, collision flag, mesh type and UV count
	if (pVisCreate->getLODCount() == 1 && !this->mediaSupported) {
		// Single LOD and non-flash
		DynamicVisLODCreate* pLODInfo = dynamic_cast<DynamicVisLODCreate*>(pVisCreate->getLODInfo(0));
		assert(pLODInfo != nullptr);
		CEXMeshObj* origEXMesh = pLODInfo->getOrigMesh();
		assert(origEXMesh != nullptr);

		if (origEXMesh->GetIndexCount() > 0) {
			// Indexed mesh
			origEXMesh->GetMaterial().Clone(&this->material);
			this->meshType = origEXMesh->IsTriStrip() ? TRISTRIP : TRILIST;
			this->numUVs = origEXMesh->m_abVertexArray.GetUVCount();
			this->groupDiscriminator.clear(); // Wipe
		}
	}
}

bool MeshGroupingHelper::Key::operator<(const Key& rhs) const {
	int res;
	res = MaterialCompressor::compare(groupDiscriminator, rhs.groupDiscriminator);
	if (res != 0)
		return res == -1;
	res = MaterialCompressor::compare(*material, *rhs.material);
	if (res != 0)
		return res == -1;
	res = MaterialCompressor::compare(meshType, rhs.meshType);
	if (res != 0)
		return res == -1;
	res = MaterialCompressor::compare(numUVs, rhs.numUVs);
	if (res != 0)
		return res == -1;
	res = MaterialCompressor::compare(collisionType, rhs.collisionType);
	if (res != 0)
		return res == -1;
	res = MaterialCompressor::compare(isCustomizable, rhs.isCustomizable);
	if (res != 0)
		return res == -1;
	res = MaterialCompressor::compare(mediaSupported, rhs.mediaSupported);
	if (res != 0)
		return res == -1;
	return false;
}

MeshGroupingHelper::MeshGroupingHelper(DynamicObjectCreate* pDOC) {
	if (!pDOC)
		return;

	std::set<std::string> allVisNames;
	pDOC->getAllVisNames(allVisNames);
	for (const auto& visName : allVisNames) {
		DynamicVisCreate* pVisCreate = pDOC->getVisInfoByName(visName);

		// Skip skinned or invalid visuals
		if (pVisCreate->isDynamic() || pVisCreate->getLODCount() <= 0)
			continue;

		Key key(pVisCreate);
		auto itr = m_groups.find(key);
		if (itr == m_groups.end()) {
			itr = m_groups.insert(std::make_pair(key, Group())).first;
		}
		Group& grp = itr->second;

		grp.visuals.push_back(pVisCreate);

		assert(grp.LODs.empty() || grp.LODs.size() == 1);
		grp.LODs.resize(pVisCreate->getLODCount());
		for (size_t i = 0; i < pVisCreate->getLODCount(); i++) {
			auto pLODInfo = pVisCreate->getLODInfo(i);
			assert(pLODInfo != nullptr);
			if (pLODInfo != nullptr) {
				CEXMeshObj* origEXMesh = pLODInfo->getOrigMesh();
				assert(origEXMesh->GetIndexCount() > 0); // Indexed mesh only

				assert(origEXMesh != nullptr);
				if (origEXMesh != nullptr) {
					Matrix44f matrix = origEXMesh->GetWorldTransform();
					Matrix44f parentMatrix;
					parentMatrix.MakeIdentity();
					MatrixConverter mc(parentMatrix);
					mc.ConvertFrom(pLODInfo->getAssetDef().GetMatrix(), pLODInfo->getAssetDef().GetUpAxis());
					matrix = matrix * parentMatrix;
					if (pDOC->m_scaleFactor != 1.0f) {
						Matrix44f scaleMatrix;
						scaleMatrix.MakeScale(pDOC->m_scaleFactor);
						matrix = matrix * scaleMatrix;
					}

					auto pBakedMesh = pLODInfo->bakeMesh(matrix);
					assert(pBakedMesh != NULL);

					if (pBakedMesh) {
						grp.LODs[i].vertexAndIndexCounts.push_back(std::make_pair(pBakedMesh->GetVertexCount(), pBakedMesh->GetIndexCount()));

						auto currUVCount = pBakedMesh->GetVertexUVCount();
						assert(grp.LODs[i].vertexUVCount == -1 || grp.LODs[i].vertexUVCount == currUVCount); // Either not set, or same as other visuals/LODs in the same group
						grp.LODs[i].vertexUVCount = currUVCount;

						auto currMeshType = pBakedMesh->IsTriStrip() ? TRISTRIP : TRILIST;
						assert(grp.LODs[i].meshType == DONTCARE || grp.LODs[i].meshType == currMeshType); // Same as above
						grp.LODs[i].meshType = currMeshType;
					} else {
						LogError("Error baking mesh for mesh [" << pVisCreate->getName() << "]");
					}
				}
			}
		}
	}
}

MeshGroupingHelper::~MeshGroupingHelper() {
	for (const auto& pr : m_groups) {
		auto key = pr.first;
		if (key.material) {
			delete key.material;
			key.material = nullptr;
		}
	}
}

#pragma region GetSet(TextureCreate, IDS_TEXTURECREATE_NAME)
BEGIN_GETSET_LT_IMPL(std::string, TextureCreate)
GETSET_LT(m_fullPath, IDS_TEXTURECREATE_FULLPATH)
END_GETSET_LT
#pragma endregion

#pragma region GetSet(DynamicVisLODCreate, IDS_DYNLODCREATE_NAME)
BEGIN_GETSET_LT_IMPL(std::string, DynamicVisLODCreate)
GETSET_LT(m_meshUri, IDS_DYNLODCREATE_MESHURI)
END_GETSET_LT
BEGIN_GETSET_LT_IMPL(int, DynamicVisLODCreate)
GETSET_LT(m_lodLevel, IDS_DYNLODCREATE_LODLEVEL)
END_GETSET_LT
#pragma endregion

#pragma region GetSet(DynamicVisCreate, IDS_DYNVISCREATE_NAME)
BEGIN_GETSET_LT_IMPL(std::string, DynamicVisCreate)
GETSET_LT(m_visName, IDS_DYNVISCREATE_NAME)
END_GETSET_LT
BEGIN_GETSET_LT_IMPL(bool, DynamicVisCreate)
GETSET_LT(m_isCustomizable, IDS_DYNVISCREATE_ISCUSTOMIZABLE)
GETSET_LT(m_canPlayFlash, IDS_DYNVISCREATE_CANPLAYFLASH)
GETSET_LT(m_hasLOD, IDS_DYNVISCREATE_HASLOD)
END_GETSET_LT
BEGIN_GETSET_LT_IMPL(int, DynamicVisCreate)
GETSET_LT(m_collisionType, IDS_DYNVISCREATE_COLLISIONTYPE)
GETSET_LT(m_subMeshId, IDS_DYNVISCREATE_SUBMESHID)
END_GETSET_LT
#pragma endregion

#pragma region GetSet(LightCreate, IDS_LIGHTCREATE_NAME)
BEGIN_GETSET_LT_IMPL(bool, LightCreate)
GETSET_LT(m_enabled, IDS_LIGHTCREATE_ENABLE)
END_GETSET_LT
BEGIN_GETSET_LT_IMPL(unsigned char, LightCreate)
GETSET_LT(m_color.r, IDS_LIGHTCREATE_R)
GETSET_LT(m_color.g, IDS_LIGHTCREATE_G)
GETSET_LT(m_color.b, IDS_LIGHTCREATE_B)
END_GETSET_LT
BEGIN_GETSET_LT_IMPL(float, LightCreate)
GETSET_LT(m_position.x, IDS_LIGHTCREATE_X)
GETSET_LT(m_position.y, IDS_LIGHTCREATE_Y)
GETSET_LT(m_position.z, IDS_LIGHTCREATE_Z)
GETSET_LT(m_range, IDS_LIGHTCREATE_RANGE)
GETSET_LT(m_attenuation0, IDS_LIGHTCREATE_ATTENUATION0)
GETSET_LT(m_attenuation1, IDS_LIGHTCREATE_ATTENUATION1)
GETSET_LT(m_attenuation2, IDS_LIGHTCREATE_ATTENUATION2)
END_GETSET_LT
#pragma endregion

#pragma region GetSet(ParticleCreate, IDS_PARTICLECREATE_NAME)
BEGIN_GETSET_LT_IMPL(bool, ParticleCreate)
GETSET_LT(m_enabled, IDS_PARTICLECREATE_ENABLE)
END_GETSET_LT
BEGIN_GETSET_LT_IMPL(int, ParticleCreate)
GETSET_LT(m_particleId, IDS_PARTICLECREATE_ID)
END_GETSET_LT
BEGIN_GETSET_LT_IMPL(float, ParticleCreate)
GETSET_LT(m_position.x, IDS_PARTICLECREATE_X)
GETSET_LT(m_position.y, IDS_PARTICLECREATE_Y)
GETSET_LT(m_position.z, IDS_PARTICLECREATE_Z)
END_GETSET_LT
#pragma endregion

#pragma region GetSet(DynamicObjectCreate, IDS_DYNOBJCREATE_NAME)
BEGIN_GETSET_LT_IMPL(std::string, DynamicObjectCreate)
// Inherited from IUGCImportStateObj
GETSET_LT_RO(m_stateMsg, IDS_UGCIMPORTSTATE_STATEMSG)
GETSET_LT(m_rootUri, IDS_UGCIMPORTSTATE_ROOTURI)
// DynamicObjectCreate members
GETSET_LT(m_objectName, IDS_DYNOBJCREATE_OBJECTNAME)
END_GETSET_LT
BEGIN_GETSET_LT_IMPL(int, DynamicObjectCreate)
// Inherited from IUGCImportStateObj
GETSET_LT_CAST(m_state, IDS_UGCIMPORTSTATE_STATE, int)
GETSET_LT(targetType, IDS_UGCIMPORTSTATE_TARGETTYPE)
GETSET_LT(m_targetId, IDS_UGCIMPORTSTATE_TARGETID)
GETSET_LT(m_importFlags, IDS_UGCIMPORTSTATE_IMPORTFLAGS)
GETSET_LT_RO(m_localGLID, IDS_UGCIMPORTSTATE_LOCALGLID)
GETSET_LT_CAST(m_eventObjId, IDS_UGCIMPORTSTATE_EVENTOBJID, int)
END_GETSET_LT
BEGIN_GETSET_LT_IMPL(unsigned short, DynamicObjectCreate)
// Inherited from IUGCImportStateObj
GETSET_LT(m_eventId, IDS_UGCIMPORTSTATE_EVENTID)
END_GETSET_LT
BEGIN_GETSET_LT_IMPL(bool, DynamicObjectCreate)
// Inherited from IUGCImportStateObj
GETSET_LT(m_isCanceling, IDS_UGCIMPORTSTATE_ISCANCELING)
GETSET_LT(m_useImportDlg, IDS_UGCIMPORTSTATE_USEIMPORTDLG)
GETSET_LT_RO(m_isValid, IDS_UGCIMPORTSTATE_ISVALID)
// Inherited from ISOMaterial
GETSET_LT(m_autoCorrectAmbient, IDS_UGCMATERIALIMPORTSTATE_AUTOCORRECTAMBIENT)
GETSET_LT(m_ignoreMissingTextures, IDS_UGCMATERIALIMPORTSTATE_IGNOREMISSINGTEXTURES)
// DynamicObjectCreate members
GETSET_LT(m_isAttachable, IDS_DYNOBJCREATE_ISATTACHABLE)
GETSET_LT(m_isInteractive, IDS_DYNOBJCREATE_ISINTERACTIVE)
END_GETSET_LT
BEGIN_GETSET_LT_IMPL(float, DynamicObjectCreate)
// Inherited from IUGCImportStateObj
GETSET_LT(m_targetPos.x, IDS_UGCIMPORTSTATE_TARGETX)
GETSET_LT(m_targetPos.y, IDS_UGCIMPORTSTATE_TARGETY)
GETSET_LT(m_targetPos.z, IDS_UGCIMPORTSTATE_TARGETZ)
// Inherited from ISOScalable
GETSET_LT(m_scaleFactor, IDS_UGCSCALABLEIMPORTSTATE_SCALEFACTOR)
END_GETSET_LT
BEGIN_GETSET_LT_IMPL(Vector3f, DynamicObjectCreate)
// Inherited from ISOScalable
GETSET_LT_RO(m_boundBoxMin, IDS_UGCSCALABLEIMPORTSTATE_BOUNDBOXMIN)
GETSET_LT_RO(m_boundBoxMax, IDS_UGCSCALABLEIMPORTSTATE_BOUNDBOXMAX)
END_GETSET_LT
BEGIN_GETSET_LT_IMPL(IGetSet*, DynamicObjectCreate)
// Inherited from ISOScalable
GETSET_LT_RO(m_importedObjectGetSet, IDS_UGCIMPORTSTATE_IMPORTEDOBJECTGETSET)
GETSET_LT_CAST(m_light, IDS_DYNOBJCREATE_LIGHT, IGetSet*)
END_GETSET_LT
BEGIN_GETSET_VECTOR_LT_IMPL(DynamicObjectCreate)
// DynamicObjectCreate members
GETSET_LT(m_gsMeshes, IDS_DYNOBJCREATE_MESHES)
GETSET_LT(m_gsTextures, IDS_DYNOBJCREATE_TEXTURES)
END_GETSET_VECTOR_LT
#pragma endregion

} // namespace KEP