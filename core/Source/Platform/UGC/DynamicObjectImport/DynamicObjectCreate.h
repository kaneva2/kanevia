///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../UGC_Global.h"
#include "../UGCImportStateObj.h"
#include "AssetTree.h"
#include "GetSetLt.h"

// forward declarations
class CSkeletonObject;

namespace KEP {
struct NamingSyntax;
class StreamableDynamicObject;

class TextureCreate : GETSET_LT_BASE {
private:
	std::string m_fullPath;

public:
	TextureCreate() {}
	virtual ~TextureCreate() {}

	const std::string& getFullPath() const { return m_fullPath; }
	void setFullPath(const std::string& val) { m_fullPath = val; }

	DECLARE_GETSET_LT(std::string);
};

class DynamicVisLODCreate : GETSET_LT_BASE {
private:
	int m_lodLevel;

	// Import Specific
	std::string m_meshUri;
	AssetData m_assetDef;

	CEXMeshObj* m_pOriginalMesh;
	CEXMeshObj* m_pBakedMesh;

public:
	DynamicVisLODCreate() :
			m_lodLevel(-1), m_pOriginalMesh(NULL), m_pBakedMesh(NULL) {}
	virtual ~DynamicVisLODCreate();

	int getLodLevel() const { return m_lodLevel; }
	const AssetData& getAssetDef() const { return m_assetDef; }
	AssetData& getAssetDefRef() { return m_assetDef; }

	void setLodLevel(int val) { m_lodLevel = val; }
	void setMeshUri(const std::string& val) { m_meshUri = val; }
	void setAssetDef(const AssetData& val) { m_assetDef = val; }

	void setOrigMesh(CEXMeshObj* m) {
		assert(m_pOriginalMesh == NULL);
		m_pOriginalMesh = m;
	}
	CEXMeshObj* getOrigMesh() const { return m_pOriginalMesh; }

	CEXMeshObj* bakeMesh(const Matrix44f& newMatrix);
	CEXMeshObj* getBakedMesh() const { return m_pBakedMesh; }

	DECLARE_GETSET_LT(std::string);
	DECLARE_GETSET_LT(int);
};

class DynamicVisCreate : GETSET_LT_BASE {
private:
	// Basic Attributes
	std::string m_visName;
	bool m_isCustomizable;
	bool m_canPlayFlash; // GETSET - do not rename
	bool m_hasLOD;
	int m_collisionType;
	bool m_isDynamic;

	// Import Specific
	int m_subMeshId;
	std::vector<DynamicVisLODCreate*> m_allLODs;

public:
	DynamicVisCreate() :
			m_isCustomizable(false), m_canPlayFlash(false), m_hasLOD(false), m_collisionType(UGC_COL_CPLX_ARRAY | UGC_COL_FACE_FRONT), m_subMeshId(0), m_isDynamic(false) {}

	virtual ~DynamicVisCreate() {
		for (auto it = m_allLODs.begin(); it != m_allLODs.end(); ++it) {
			if (*it) {
				delete *it;
			}
		}

		m_allLODs.clear();
	}

	const std::string& getName() const { return m_visName; }
	void setName(const std::string& val) { m_visName = val; }
	bool isCustomizable() const { return m_isCustomizable; }
	void setCustomizable(bool val) { m_isCustomizable = val; }
	bool MediaSupported() const { return m_canPlayFlash; }
	void SetMediaSupported(bool val) { m_canPlayFlash = val; }
	bool getHasLOD() const { return m_hasLOD; }
	void setHasLOD(bool val) { m_hasLOD = val; }
	int getCollisionType() const { return m_collisionType; }
	void setCollisionType(int val) { m_collisionType = val; }
	bool isDynamic() const { return m_isDynamic; }
	void setDynamic(bool val) { m_isDynamic = val; }
	int getSubMeshId() const { return m_subMeshId; }
	void setSubMeshId(int val) { m_subMeshId = val; }

	int getLODCount() const { return m_allLODs.size(); }

	DynamicVisLODCreate* getLODInfo(unsigned int lodLevel) const {
		if (lodLevel < m_allLODs.size()) {
			return m_allLODs[lodLevel];
		}
		return NULL;
	}

	DynamicVisLODCreate* addLODInfo(DynamicVisLODCreate* pLODInfo) {
		m_allLODs.push_back(pLODInfo);
		return pLODInfo;
	}

	DECLARE_GETSET_LT(std::string);
	DECLARE_GETSET_LT(bool);
	DECLARE_GETSET_LT(int);
};

class LightCreate : GETSET_LT_BASE {
private:
	bool m_enabled;
	struct {
		unsigned char r, g, b;
	} m_color;
	Vector3f m_position;
	float m_range;
	float m_attenuation0;
	float m_attenuation1;
	float m_attenuation2;

public:
	LightCreate() :
			m_enabled(false), m_position(0.0f, 0.0f, 0.0f), m_range(0.0f) {
		m_color.r = m_color.g = m_color.b = 0;
		m_attenuation0 = m_attenuation1 = m_attenuation2 = 0.0f;
	}
	virtual ~LightCreate() {}

	bool isEnabled() const { return m_enabled; }
	unsigned char getR() const { return m_color.r; }
	unsigned char getG() const { return m_color.g; }
	unsigned char getB() const { return m_color.b; }
	const Vector3f& getPosition() { return m_position; }
	float getRange() const { return m_range; }
	float getAttenuation0() const { return m_attenuation0; }
	float getAttenuation1() const { return m_attenuation2; }
	float getAttenuation2() const { return m_attenuation2; }

	DECLARE_GETSET_LT(bool);
	DECLARE_GETSET_LT(unsigned char);
	DECLARE_GETSET_LT(float);
};

class ParticleCreate : GETSET_LT_BASE {
private:
	bool m_enabled;
	int m_particleId;
	Vector3f m_position;

public:
	ParticleCreate() :
			m_enabled(false), m_particleId(-1), m_position(0.0f, 0.0f, 0.0f) {}
	virtual ~ParticleCreate() {}

	bool isEnabled() const { return m_enabled; }
	int getParticleId() const { return m_particleId; }
	const Vector3f& getPosition() const { return m_position; }

	DECLARE_GETSET_LT(bool);
	DECLARE_GETSET_LT(int);
	DECLARE_GETSET_LT(float);
};

class DynamicObjectCreate : public IUGCImportStateObj, public ISOScalable, public ISOMaterial, GETSET_LT_BASE {
private:
	// Basic Attributes
	std::string m_objectName;
	bool m_isAttachable;
	bool m_isInteractive;

	// Import Specific
	NamingSyntax* m_pNamingSyntax;
	StreamableDynamicObject* m_importedObject;
	GLID m_glid;

	AssetTree* m_assets;
	CSkeletonObject* m_skeleton;

	// Child Assets
	LightCreate* m_light;
	ParticleCreate m_particle;
	DynamicVisCreate m_visTemplate;
	TextureCreate m_texTemplate;

	std::map<std::string, DynamicVisCreate*> m_allMeshes;
	std::vector<TextureCreate*> m_allTextures;
	std::vector<IGetSet*> m_gsMeshes, m_gsTextures; // for GETSET_VECTOR access

	// Property Exporter
	PropertySource* m_propSrc;

public:
	DynamicObjectCreate();
	virtual ~DynamicObjectCreate();

	// Accessors
	const std::string& getName() const { return m_objectName; }
	bool isAttachable() const { return m_isAttachable; }
	bool isInteractive() const { return m_isInteractive; }
	GLID getGlid() const { return m_glid; }

	// Optional Properties (check enabled flag)
	const LightCreate* getLight() const { return m_light; }
	const ParticleCreate& getParticle() const { return m_particle; }
	LightCreate* getLight() { return m_light; }
	ParticleCreate& getParticle() { return m_particle; }

	// Templates for sub items
	const DynamicVisCreate& getVisTemplate() const { return m_visTemplate; }
	const TextureCreate& getTexureTemplate() const { return m_texTemplate; }
	DynamicVisCreate& getMutableVisTemplate() { return m_visTemplate; }
	TextureCreate& getMutableTexureTemplate() { return m_texTemplate; }

	int getVisCount() const { return m_allMeshes.size(); }

	void getAllVisNames(std::set<std::string>& names) const {
		names.clear();
		for (auto pr : m_allMeshes) {
			names.insert(pr.first);
		}
	}

	DynamicVisCreate* getVisInfoByName(const std::string& name) const {
		auto itr = m_allMeshes.find(name);
		if (itr != m_allMeshes.end()) {
			return itr->second;
		}
		return NULL;
	}

	DynamicVisCreate* addVisInfo(DynamicVisCreate* vis) {
		jsAssert(m_allMeshes.find(vis->getName()) == m_allMeshes.end());
		m_allMeshes.insert(make_pair(vis->getName(), vis));
		m_gsMeshes.push_back(vis);
		return vis;
	}

	int getTextureCount() const { return m_allTextures.size(); }

	TextureCreate* getTextureInfo(unsigned int idx) const {
		jsVerifyReturn(idx < m_allTextures.size(), NULL);
		return m_allTextures[idx];
	}

	TextureCreate* getTextureInfo(const std::string& fullPath) const {
		// Sequential search in small set - performance is not a problem here
		for (auto pTC : m_allTextures) {
			if (!pTC) {
				continue;
			}
			if (pTC->getFullPath() == fullPath) {
				return pTC;
			}
		}
		return NULL;
	}

	TextureCreate* addTextureInfo(TextureCreate* tex) {
		m_allTextures.push_back(tex);
		m_gsTextures.push_back(tex);
		return tex;
	}

	void setAssets(AssetTree* a) { m_assets = a; }
	AssetTree* getAssets() const { return m_assets; }
	void setSkeleton(CSkeletonObject* s) { m_skeleton = s; }
	CSkeletonObject* getSkeleton() const { return m_skeleton; }

	StreamableDynamicObject* getImportedObject() const { return m_importedObject; }
	void setImportedObject(StreamableDynamicObject* obj, GLID glid) {
		m_importedObject = obj;
		m_glid = glid;
	}
	void disposeImportedObject();

	int getImportType() override { return UGC_TYPE_DYNAMIC_OBJ; }
	PropertySource* getPropertySource(bool bRefresh) override;
	IGetSet* Values() override { return this; }

	DECLARE_GETSET_LT(std::string);
	DECLARE_GETSET_LT(int);
	DECLARE_GETSET_LT(unsigned short);
	DECLARE_GETSET_LT(bool);
	DECLARE_GETSET_LT(float);
	DECLARE_GETSET_LT(Vector3f);
	DECLARE_GETSET_LT(IGetSet*);
	DECLARE_GETSET_VECTOR_LT();
};

} // namespace KEP