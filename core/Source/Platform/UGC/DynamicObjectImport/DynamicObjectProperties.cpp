///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DynamicObjectProperties.h"
#include "DynamicObjectImport.h"
#include "Common\include\CoreStatic\StreamableDynamicObj.h"
#include "Common\include\CoreStatic\CMaterialClass.h"
#include "ResourceManagers\ReMeshCreate.h"
#include "Common\include\IClientEngine.h"
#include "Common\include\CoreStatic\TextureObj.h"
#include "ResourceManagers\SkeletonProxy.h"
#include "ResourceManagers\SkeletonManager.h"
#include "Common\include\CoreStatic\RuntimeSkeleton.h"
#include "Common/include/TextureDef.h"

namespace KEP {

VLD_VALUE_TYPE DynamicObjectProperties::getType(int propId) const {
	switch (PROPID_BASE(propId)) {
		case PROP_MESH_CNT:
		case PROP_VERTEX_CNT:
		case PROP_TRIANGLE_CNT:
		case PROP_MATERIAL_CNT:
		case PROP_TEXTURE_CNT:
		case PROP_MESH_VERTEX_CNT:
		case PROP_MESH_TRIANGLE_CNT:
		case PROP_MESH_MATERIAL_CNT:
		case PROP_MESH_TEXTURE_CNT:
		case PROP_IMAGE_FILESIZE:
		case PROP_IMAGE_PIXEL_CNT:
		case PROP_IMAGE_WIDTH:
		case PROP_IMAGE_HEIGHT:
		case PROP_IMAGE_DEPTH:
		case PROP_IMAGE_OPACITY:
			return VVT_INT;

		case PROP_BOUNDBOX_SIZEX:
		case PROP_BOUNDBOX_SIZEY:
		case PROP_BOUNDBOX_SIZEZ:
		case PROP_BOUNDBOX_MINX:
		case PROP_BOUNDBOX_MAXX:
		case PROP_BOUNDBOX_MINY:
		case PROP_BOUNDBOX_MAXY:
		case PROP_BOUNDBOX_MINZ:
		case PROP_BOUNDBOX_MAXZ:
		case PROP_BOUNDBOX_VOLUME:
		case PROP_BOUNDBOX_XYZSUM:
		case PROP_MESH_BOUNDBOX_SIZEX:
		case PROP_MESH_BOUNDBOX_SIZEY:
		case PROP_MESH_BOUNDBOX_SIZEZ:
		case PROP_MESH_BOUNDBOX_VOLUME:
		case PROP_MESH_BOUNDBOX_MINX:
		case PROP_MESH_BOUNDBOX_MAXX:
		case PROP_MESH_BOUNDBOX_MINY:
		case PROP_MESH_BOUNDBOX_MAXY:
		case PROP_MESH_BOUNDBOX_MINZ:
		case PROP_MESH_BOUNDBOX_MAXZ:
		case PROP_MESH_BOUNDBOX_XYZSUM:
			return VVT_FLOAT;

		case PROP_MEDIA_PLAYBACK:
		case PROP_COLLISION:
			return VVT_BOOL;

		case PROP_IMAGE_AVG_COLOR: // Unsigned 32-bit value. Use string for now
		case PROP_IMAGE_ID: // Unsigned 32-bit value. Use string for now
			return VVT_STRING;
	}

	return VVT_UNDEFINED;
}

Value DynamicObjectProperties::get(int propId) const {
	int propIndex = PROPID_INDEX(propId);

	switch (PROPID_BASE(propId)) {
		case PROP_MESH_CNT:
			return Value(meshCount);
		case PROP_VERTEX_CNT:
			return Value(vertexCount);
		case PROP_TRIANGLE_CNT:
			return Value(triangleCount);
		case PROP_BOUNDBOX_SIZEX:
			return Value(boundBox.maxX - boundBox.minX);
		case PROP_BOUNDBOX_SIZEY:
			return Value(boundBox.maxY - boundBox.minY);
		case PROP_BOUNDBOX_SIZEZ:
			return Value(boundBox.maxZ - boundBox.minZ);
		case PROP_MATERIAL_CNT:
			return Value(materialCount);
		case PROP_TEXTURE_CNT:
			return Value(textureCount);
		case PROP_DISTINCT_TEXTURE_CNT:
			return Value(distinctTextureCount);
		case PROP_BOUNDBOX_VOLUME:
			return Value(abs((boundBox.maxX - boundBox.minX) * (boundBox.maxY - boundBox.minY) * (boundBox.maxZ - boundBox.minZ)));
		case PROP_BOUNDBOX_MINX:
			return Value(boundBox.minX);
		case PROP_BOUNDBOX_MAXX:
			return Value(boundBox.maxX);
		case PROP_BOUNDBOX_MINY:
			return Value(boundBox.minY);
		case PROP_BOUNDBOX_MAXY:
			return Value(boundBox.maxY);
		case PROP_BOUNDBOX_MINZ:
			return Value(boundBox.minZ);
		case PROP_BOUNDBOX_MAXZ:
			return Value(boundBox.maxZ);
		case PROP_BOUNDBOX_XYZSUM:
			return Value(abs(boundBox.maxX - boundBox.minX) + abs(boundBox.maxY - boundBox.minY) + abs(boundBox.maxZ - boundBox.minZ));
		case PROP_IMAGE_WIDTH:
			if (propIndex < distinctTextureCount)
				return Value(textures[propIndex].width);
			break;
		case PROP_IMAGE_HEIGHT:
			if (propIndex < distinctTextureCount)
				return Value(textures[propIndex].height);
			break;
		case PROP_IMAGE_OPACITY:
			if (propIndex < distinctTextureCount)
				return Value((int)textures[propIndex].opacity);
			break;
		case PROP_IMAGE_AVG_COLOR:
			if (propIndex < distinctTextureCount)
				return Value(std::to_string(textures[propIndex].averageColor));
			break;
		case PROP_IMAGE_ID:
			if (propIndex < distinctTextureCount)
				return Value(std::to_string(textures[propIndex].nameHash));
			break;
		case PROP_MEDIA_PLAYBACK:
			return Value(mediaSupported);
		case PROP_COLLISION:
			return Value(canCollide);
	}

	return Value(VVT_ERROR);
}

DynamicObjectProperties::DynamicObjectProperties(const StreamableDynamicObject& aDynObj) :
		meshCount(aDynObj.getVisualCount()),
		vertexCount(0),
		triangleCount(0),
		materialCount(0),
		textureCount(0),
		distinctTextureCount(0),
		mediaSupported(aDynObj.MediaSupported()),
		canCollide(false) {
	memset(textures, 0, sizeof(textures));

	std::set<DWORD> texturesFound;

	for (size_t visLoop = 0; visLoop < aDynObj.getVisualCount(); ++visLoop) {
		StreamableDynamicObjectVisual* pVis = aDynObj.Visual(visLoop);

		if (pVis->getCanCollide()) {
			canCollide = true;
		}

		UINT visVCount = 0;
		UINT visTCount = 0;

		for (int lodLoop = 0; lodLoop < pVis->getLodCount(); ++lodLoop) {
			UINT64 hash = pVis->getLOD(lodLoop);
			ReMesh* rm = MeshRM::Instance()->FindMesh(hash, MeshRM::RMP_ID_DYNAMIC);
			ASSERT(rm != NULL);

			ReMeshData* rmData = NULL;
			rm->Access(&rmData);
			ASSERT(rmData != NULL);

			if (rmData->NumI > visVCount)
				visVCount = rmData->NumI;
		}

		visTCount = visVCount / 3;

		vertexCount += visVCount;
		triangleCount += visTCount;

		if (pVis->getMaterial() != NULL) {
			materialCount++;
			for (int texLoop = 0; texLoop < NUM_CMATERIAL_TEXTURES; texLoop++) {
				DWORD texNameHash = pVis->getMaterial()->m_texNameHash[texLoop];
				if (texNameHash != -1 && texNameHash != 0) {
					if (texturesFound.find(texNameHash) == texturesFound.end()) {
						texturesFound.insert(texNameHash);
						distinctTextureCount++;
					}

					textureCount++;
				}
			}
		}
	}

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	// Collect information for textures
	int texTrace = 0;
	for (auto itTex = texturesFound.begin(); itTex != texturesFound.end(); ++itTex) {
		DWORD texNameHash = *itTex;
		textures[texTrace].nameHash = texNameHash;

		CTextureEX* pTex = pICE->GetTextureFromDatabase(texNameHash);
		if (pTex) {
			//Warning: width/height not available until CreateResource is called on CTextureEX
			//This is OK for current validation needs but must take great cautious here if client
			//validation on textures is needed in the future
			textures[texTrace].width = pTex->GetWidth();
			textures[texTrace].height = pTex->GetHeight();

			TextureDef textureInfo;
			if (pICE->GetTextureImportInfo(pTex->GetFileName(), textureInfo)) {
				ASSERT(textureInfo.width == pTex->GetWidth() && textureInfo.height == pTex->GetHeight());
				textures[texTrace].opacity = textureInfo.opacity;
				textures[texTrace].averageColor = textureInfo.averageColor;
			} else {
				ASSERT(false);
			}
		}

		texTrace++;
	}

	boundBox = aDynObj.computeBoundingBox();

	// If ADO, scale bound box by skeleton scaling vector (static DOs already have scaling factor baked-in in mesh data)
	if (aDynObj.getSkeletonGLID() != 0) {
		// Has a skeleton
		SkeletonProxy* pSkeletonProxy = SkeletonRM::Instance()->GetSkeletonProxy(aDynObj.getSkeletonGLID());
		ASSERT(pSkeletonProxy != NULL);
		if (pSkeletonProxy) {
			RuntimeSkeleton* skel = pSkeletonProxy->GetRuntimeSkeleton();
			ASSERT(skel != NULL);

			if (skel) {
				const auto& scaleVector = skel->getScaleVector();
				boundBox.minX *= scaleVector.x;
				boundBox.maxX *= scaleVector.x;
				boundBox.minY *= scaleVector.y;
				boundBox.maxY *= scaleVector.y;
				boundBox.minZ *= scaleVector.z;
				boundBox.maxZ *= scaleVector.z;
			}
		}
	}
}

void DynamicObjectProperties::getAllSupportedPropertyIds(std::vector<int>& allPropIds) {
	allPropIds.clear();

	allPropIds.push_back(PROP_MESH_CNT);
	allPropIds.push_back(PROP_VERTEX_CNT);
	allPropIds.push_back(PROP_TRIANGLE_CNT);
	allPropIds.push_back(PROP_BOUNDBOX_SIZEX);
	allPropIds.push_back(PROP_BOUNDBOX_SIZEY);
	allPropIds.push_back(PROP_BOUNDBOX_SIZEZ);
	allPropIds.push_back(PROP_MATERIAL_CNT);
	allPropIds.push_back(PROP_TEXTURE_CNT);
	allPropIds.push_back(PROP_DISTINCT_TEXTURE_CNT);
	allPropIds.push_back(PROP_BOUNDBOX_VOLUME);
	allPropIds.push_back(PROP_BOUNDBOX_MINX);
	allPropIds.push_back(PROP_BOUNDBOX_MINY);
	allPropIds.push_back(PROP_BOUNDBOX_MINZ);
	allPropIds.push_back(PROP_BOUNDBOX_MAXX);
	allPropIds.push_back(PROP_BOUNDBOX_MAXY);
	allPropIds.push_back(PROP_BOUNDBOX_MAXZ);
	allPropIds.push_back(PROP_BOUNDBOX_XYZSUM);
	allPropIds.push_back(PROP_MEDIA_PLAYBACK);
	allPropIds.push_back(PROP_COLLISION);
	for (int i = 0; i < distinctTextureCount; i++) {
		allPropIds.push_back(MAKE_PROPID(PROP_IMAGE_WIDTH, i));
		allPropIds.push_back(MAKE_PROPID(PROP_IMAGE_HEIGHT, i));
		allPropIds.push_back(MAKE_PROPID(PROP_IMAGE_OPACITY, i));
		allPropIds.push_back(MAKE_PROPID(PROP_IMAGE_AVG_COLOR, i));
		allPropIds.push_back(MAKE_PROPID(PROP_IMAGE_ID, i));
	}
}

} // namespace KEP