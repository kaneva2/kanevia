///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common\include\CoreStatic\BoundBox.h"
#include "../UGCProperties.h"

namespace KEP {

class StreamableDynamicObject;

class DynamicObjectProperties : public PropertySource {
public:
	DynamicObjectProperties(const StreamableDynamicObject& aDynObj);

	VLD_VALUE_TYPE getType(int propId) const;
	Value get(int propId) const;

	void getAllSupportedPropertyIds(std::vector<int>& allPropIds);

protected:
	int meshCount;
	int vertexCount;
	int triangleCount;
	int materialCount;
	int textureCount;
	int distinctTextureCount;
	bool mediaSupported; // true if it is also a media player
	bool canCollide; // true if whole or part of the object can collide with other entities
	BoundBox boundBox;
	TexInfo textures[MAX_TEX_COUNT];
};

} // namespace KEP