///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/include/IObserver.h"
#include "Common/include/IClientEngine.h"
#include "Event/Base/IDispatcher.h"
#include "UGCImportStateObj.h"

namespace KEP {

class UGCImportStateObserver {
public:
	virtual ~UGCImportStateObserver() {}
	virtual void doUpdate(IUGCImportStateObj* pStateObj, UGCImportEventType type) = 0;
};

} // namespace KEP