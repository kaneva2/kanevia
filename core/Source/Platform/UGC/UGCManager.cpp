///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "UGCManager.h"
#include "IKEPDropTarget.h"
#include "UGCImport.h"
#include "UGCImportStateMachine.h"
#include "UGCUploadState.h"
#include "UGCItemUploadState.h"
#include "UGCProperties.h"

#include "Common/include/IClientEngine.h"
#include "Common/include/UrlParser.h"
#include "Event/Base/EventHeader.h"
#include "Event/Base/IDispatcher.h"
#include "Event/Events/GenericEvent.h"
#include "Common/include/CoreStatic/TextureObj.h"
#include "Common/include/CoreStatic/TextureDatabase.h"
#include "Common/include/CoreStatic/SoundPlacement.h"
#include "Common/include/CoreStatic/OALSoundProxy.h"
#include "ResourceManagers/UploadManager.h"
#include "Common/include/CoreStatic/OggConverter.h"
#include "config.h"
#include "afxdlgs.h"
#include "LogHelper.h"

#define UGCMGR_SUBMISSIONFEEDBACKEVENT "UGCManager_SubmissionFeedbackEvent"
#define UGCMGR_UPLOADPROGRESSEVENT "UGCManager_UploadProgressEvent"

namespace KEP {

static LogInstance("UGCImport");

typedef std::map<long long, UGCItemUpload*> UGCTexturedObjMap;
typedef std::map<DWORD, UGCItemTexture> UGCTextureMap;

struct UGCTypeDef {
	UGC_TYPE ugcType;
	std::string ugcTypeName;
	std::set<std::string> knownFileExts;
};

class UGCManager : public IUGCManager {
public:
	UGCManager() {
		UGCUploadState::SetUGCManager(this);
	}

	~UGCManager() {
		for (const auto& it : m_ugcTexturedObjs)
			ClearUGCObjList(it.first);
	}

	void RegisterImportType(UGC_TYPE ugcType, const std::string& ugcTypeName, const std::string& fileExt);
	void RegisterImportType(UGC_TYPE ugcType, const std::string& ugcTypeName, const std::set<std::string>& knownFileExts);

	UGC_TYPE ClassifyImportType(int dropType, const std::string& importUri, IModelType targetType, int targetId, const Vector3f& targetPos, const Vector3f& targetNml);

	virtual bool BrowseFileByImportType(UGC_TYPE type, IEvent* completionEvent);

	virtual UGCImporter* GetImporterByType(UGC_TYPE type);
	virtual void ChangeImportState(IUGCImportStateObj* pStateObj, int newState, bool bDoUpdate);
	virtual void TickleImportState(IUGCImportStateObj* pStateObj, int eventType);

	virtual long long AddUGCObj(UGCItemUpload* ugcObj, bool bAddUGCTextures = true);
	virtual void RemoveUGCObj(int ugcType, long long ugcObjId);
	virtual void ClearUGCObjList(int ugcType);
	virtual void EnumUGCObjs(int ugcType, std::vector<long long>& allUGCObjIds);

	virtual UGCUploadState* GetUGCState(int ugcType, long long ugcObjId);

	virtual bool AddImageToUGCObj(int ugcType, long long ugcObjId, int imgType, const std::string& imgPath, long long subID = -1);
	virtual bool ClearUGCObjImagesByType(int ugcType, long long ugcObjId, int imgType);

	virtual bool AddTextureToUGCObj(UGCItemUpload* pUgcObj, int texType, const std::string& texName, long long texSlotId);

	virtual bool SubmitUGCObj(int ugcType, long long ugcObjId, IEvent* submissionFeedbackEvent = NULL);
	virtual bool SubmitChildItemsForUGCObj(int ugcType, long long ugcObjId, IEvent* submissionFeedbackEvent = NULL);
	virtual bool SubmitChildItemsForUGCObj(UGCItemUpload& ugcObj, IEvent* submissionFeedbackEvent = NULL);

	virtual bool Upload(const std::string& uploadId, int ugcType, long long ugcObjId, IEvent* uploadProgressEvent, IEvent* uploadCompletionEvent);
	virtual bool UploadChildItem(const std::string& uploadId, UGCItemUpload* pUgcObj, int childType, int childId, IEvent* uploadProgressEvent, IEvent* uploadCompletionEvent);

	virtual int ImportSoundFile(const std::string& filePath, bool encrypt = false);

	virtual int ImportActionItem(const std::string& fullPath, int defaultGlid, const std::string& bundleGlids, int parentGlid);

	void ReportUGCObjUploadProgress(const std::string& masterUploadId, int ugcType, int ugcObjId, const char* subid, int uploadState, float percentage);
	float GetUGCObjUploadProgress(const std::string& masterUploadId, int& filesTotal, int& filesCompleted, int& filesInProgress, FileSize& bytesTotal, FileSize& bytesProcessed);
	void FireUGCObjUploadProgressEvent(const std::string& masterUploadId);
	void FireUGCSubmissionReplyEvent(UGCUploadState& ugcState, const std::string& resultMsg, int httpStatusCode);

	// Deployment
	virtual bool DeployUGCUpload(const std::string& uploadId, int type, IEvent* deployCompletionEvent);

	// Engine access
	virtual IClientEngine* GetEngine() { return ms_pEngine; }
	virtual void SetEngine(IClientEngine* engine) { ms_pEngine = engine; }

	// Proxy to Engine
	virtual void SendEventToEngine(IEvent* e);

	// Get UGC Obj
	virtual UGCItemUpload* GetUGCObjByUploadId(const std::string& uploadId);

private:
	static IClientEngine* ms_pEngine;

	// Pending imports - all items stay after added until explicitly removed
	std::map<int, UGCTexturedObjMap> m_ugcTexturedObjs; // Imported/temporary UGC dynamic objects

	static std::vector<UGCTypeDef> ms_supportedImportTypes;

	bool DoSubmitChildItem(int parentType, int parentId, UGCUploadChildState& childState, const char* masterUploadId, IEvent* submissionFeedbackEvent = NULL);

	// Return true if iterator valid, false otherwise
	bool RemoveUGCObjByIterator(int ugcType, const UGCTexturedObjMap::iterator& itUgcObj);

	// common upload function
	bool DoUpload(const std::string& uploadId, UGCUploadState* pUgcState, IEvent* uploadProgressEvent, IEvent* uploadCompletionEvent);

public:
	// Return true if ugc type is one of the texture types
	static bool IsTextureType(int ugcType) { return (ugcType & UGC_TYPE_CATEMASK) == UGC_TYPE_TEXTURE || (ugcType & UGC_TYPE_CATEMASK) == UGC_TYPE_MEDIA_IMAGE; }
	static bool IsMasterType(int ugcType) { return !IsChildType(ugcType); }
	static bool IsChildType(int ugcType) { return IsTextureType(ugcType); } // Add more child types when necessary
};

std::vector<UGCTypeDef> UGCManager::ms_supportedImportTypes;
IClientEngine* UGCManager::ms_pEngine = NULL;

void UGCManager::RegisterImportType(UGC_TYPE ugcType, const std::string& ugcTypeName, const std::string& fileExt) {
	UGCTypeDef ugcDef;
	ugcDef.ugcType = ugcType;
	ugcDef.ugcTypeName = ugcTypeName;
	std::string ext = fileExt;
	STLToLower(ext);
	ugcDef.knownFileExts.insert(ext);
	ms_supportedImportTypes.push_back(ugcDef);
}

void UGCManager::RegisterImportType(UGC_TYPE ugcType, const std::string& ugcTypeName, const std::set<std::string>& knownFileExts) {
	UGCTypeDef ugcDef;
	ugcDef.ugcType = ugcType;
	ugcDef.ugcTypeName = ugcTypeName;
	for (const auto& ext : knownFileExts) {
		std::string extLwr = ext;
		STLToLower(extLwr);
		ugcDef.knownFileExts.insert(extLwr);
	}
	ms_supportedImportTypes.push_back(ugcDef);
}

UGC_TYPE UGCManager::ClassifyImportType(int dropType, const std::string& importUri, IModelType targetType, int targetId, const Vector3f& targetPos, const Vector3f& targetNml) {
	// Quick fix for "C:\xxx.x\...": append file:/ for url starting with drive letters to avoid confusion between scheme and drive letter
	std::string uri = importUri;
	if (uri.size() > 2 && isalpha(uri[0]) && uri[1] == ':') {
		uri = "file:/" + uri;
	}

	// Fix UNC-style path "\\host\path\..."
	if (uri.size() > 4 && uri.substr(0, 2) == "\\\\") {
		uri = "file:" + uri;
	}

	UrlParser parser;
	parser.parse(uri.c_str(), true);

	std::string scheme;
	if (parser.scheme()) {
		scheme = parser.scheme();
	}

	// If no scheme is provided, default to "file"
	if (scheme.empty()) {
		scheme = "file";
	}

	std::string path;
	if (parser.leftPart()) {
		path = parser.leftPart();
	}

	UGC_TYPE importType = UGC_TYPE_INVALID;

	if (STLCompareIgnoreCase(scheme, "file") == 0) { // file:/...
		if (parser.map().empty()) { // no arguments provided
			// Trim off leading '/' if drive letter found in path
			if (path.size() > 3 && path[0] == '/' && isalpha(path[1]) && path[2] == ':')
				path.erase(path.begin());

			// Quick and dirty implementation
			// Redo once we have many more import types.
			size_t posDot = importUri.rfind('.');
			if (posDot != std::string::npos) { // has extension
				std::string ext = importUri.substr(posDot + 1);
				STLToLower(ext);

				for (auto it = ms_supportedImportTypes.begin(); it != ms_supportedImportTypes.end(); ++it) {
					auto itExt = it->knownFileExts.find(ext);
					if (itExt != it->knownFileExts.end()) {
						importType = it->ugcType;
						break;
					}
				}
			}
		}
	} else if (STLCompareIgnoreCase(scheme, "http") == 0 || STLCompareIgnoreCase(scheme, "https") == 0) { // http(s):/...
		assert(dropType != DROP_FILE); // Just to double check to make sure no weird thing happens
		importType = UGC_TYPE_URL;
	}

	return importType;
}

class FileDialogThread : public jsThread {
private:
	std::string filterStr;
	BOOL bFileOpenDlg;
	HWND hParentWnd;
	IDispatcher* dispatcher;
	IEvent* completionEvent;

public:
	FileDialogThread(const std::string& filter, BOOL bOpen, HWND hParent, IDispatcher* disp, IEvent* e) :
			jsThread("FileDialogThread"), filterStr(filter), bFileOpenDlg(bOpen), hParentWnd(hParent), dispatcher(disp), completionEvent(e) {}

	virtual ULONG _doWork() {
		CFileDialog fileDlg(bFileOpenDlg, NULL, NULL, OFN_NOCHANGEDIR | OFN_FILEMUSTEXIST, Utf8ToUtf16(filterStr).c_str(), CWnd::FromHandle(hParentWnd));
		int dialogResult = fileDlg.DoModal();

		(*completionEvent->OutBuffer()) << (BOOL)(dialogResult == IDOK);
		if (dialogResult == IDOK) {
			(*completionEvent->OutBuffer()) << Utf16ToUtf8(fileDlg.GetPathName().GetString());
		}
		dispatcher->QueueEvent(completionEvent);

		//@@Watch: is it safe to delete here?
		delete this;
		return 0;
	}
};

bool UGCManager::BrowseFileByImportType(UGC_TYPE type, IEvent* completionEvent) {
	jsVerifyReturn(completionEvent != NULL, false);

	//Ankit -- Instead of opening the file dialog, straight away set the respective complete file path in the output buffer
	if (ms_pEngine->IsSATRunning()) {
		(*completionEvent->OutBuffer()) << (BOOL) true;
		(*completionEvent->OutBuffer()) << ms_pEngine->GETGetSetString("SAT_UGCImportAssetFilePath");
		(ms_pEngine->GetDispatcher())->QueueEvent(completionEvent);
		return true;
	}

	for (auto it = ms_supportedImportTypes.begin(); it != ms_supportedImportTypes.end(); ++it) {
		if (it->ugcType == type) {
			std::string filterName = it->ugcTypeName + "( ";
			std::string filterExts;
			for (auto itExt = it->knownFileExts.begin(); itExt != it->knownFileExts.end(); ++itExt) {
				if (itExt != it->knownFileExts.begin()) {
					// not first one
					filterName = filterName + ", ";
					filterExts = filterExts + ";";
				}
				filterName = filterName + "*." + itExt->c_str();
				filterExts = filterExts + "*." + itExt->c_str();
			}
			filterName = filterName + " )";

			std::string filterStr;
			filterStr.append(filterName.begin(), filterName.end());
			filterStr.append(1, '|');
			filterStr.append(filterExts.begin(), filterExts.end());
			filterStr.append(2, '|');

			FileDialogThread* dlgThd = new FileDialogThread(filterStr, TRUE, ms_pEngine->GetWindowHandle(), ms_pEngine->GetDispatcher(), completionEvent);
			dlgThd->start();
			return true;
		}
	}

	return false;
}

UGCImporter* UGCManager::GetImporterByType(UGC_TYPE type) {
	IUGCImporterFactory* factory = GetImporterFactory();
	if (!factory) {
		return NULL;
	}

	return factory->getImporter((int)type);
}

void UGCManager::ChangeImportState(IUGCImportStateObj* pStateObj, int newState, bool bDoUpdate) {
	UGCImportStateObserver* stateMachine = GetImportStateMachine();
	if (!stateMachine) {
		return;
	}

	pStateObj->m_state = (UGCImportState)newState;
	if (bDoUpdate) {
		stateMachine->doUpdate(pStateObj, EV_NONE);
	}
}

void UGCManager::TickleImportState(IUGCImportStateObj* pStateObj, int eventType) {
	UGCImportStateObserver* stateMachine = GetImportStateMachine();
	if (!stateMachine) {
		return;
	}
	stateMachine->doUpdate(pStateObj, (UGCImportEventType)eventType);
}

char* GetUploadTypeStringFromUGCType(int ugcType) {
	static char* szUnknown = ""; // If unknown, return empty string instead of NULL

	// Get upload type based on ugc type
	switch (ugcType) {
		case UGC_TYPE_DYNAMIC_OBJ:
			return UGC_UPLOAD_DOB_GZ;

		case UGC_TYPE_TEX_DYNAMIC_OBJ:
		case UGC_TYPE_TEX_WORLD_OBJ:
			return UGC_UPLOAD_CUSTOM_TEX;

		case UGC_TYPE_TEX_DYNAMIC_OBJ_UGC:
		case UGC_TYPE_TEX_EQUIP:
		case UGC_TYPE_TEX_PARTICLE:
			return UGC_UPLOAD_TEX_DDS_GZ; // All UGC item textures are now dds.gz

		case UGC_TYPE_TEX_ICON:
			return UGC_UPLOAD_ICON_TEX;

		case UGC_TYPE_DYNAMIC_OBJ | UGC_TYPE_CUSTOMIZATION:
		case UGC_TYPE_WORLD_OBJ | UGC_TYPE_CUSTOMIZATION:
			return UGC_UPLOAD_MEDIA;

		case UGC_TYPE_MEDIA_IMAGE_PATTERN:
			return UGC_UPLOAD_MEDIUM_PATTERN;

		case UGC_TYPE_MEDIA_IMAGE_PICTURE:
			return UGC_UPLOAD_MEDIUM_PICTURE;

		case UGC_TYPE_ANIMATION:
			return UGC_UPLOAD_ANM_GZ;

		case UGC_TYPE_SOUND_OGG:
			return UGC_UPLOAD_SND_OGG_KRX;

		case UGC_TYPE_EQUIPPABLE:
		case UGC_TYPE_EQUIPPABLE_RIGID:
		case UGC_TYPE_EQUIPPABLE_SKELETAL:
			return UGC_UPLOAD_EQP_GZ;

		case UGC_TYPE_PARTICLE_EFFECT:
			return UGC_UPLOAD_PARTICLE;
		case UGC_TYPE_ACTION_ITEM:
			return UGC_UPLOAD_ACTION_ITEM;
		case UGC_TYPE_APP_EDIT:
			return UGC_UPLOAD_ASSET;
	}

	return szUnknown;
}

long long UGCManager::AddUGCObj(UGCItemUpload* ugcObj, bool bAddUGCTextures) {
	if (!ms_pEngine)
		return 0;

	int ugcType = ugcObj->getType();
	long long ugcObjId = ugcObj->getId();

	UGCTexturedObjMap& texturedObjMap = m_ugcTexturedObjs[ugcType];
	UGCTexturedObjMap::iterator findIt = texturedObjMap.find(ugcObjId);
	if (findIt != texturedObjMap.end()) {
		LogWarn("removed existing record, type=" << ugcType << ", id=" << ugcObjId);
		RemoveUGCObjByIterator(ugcType, findIt);
	}

	texturedObjMap.insert(std::make_pair(ugcObjId, ugcObj));

	if (bAddUGCTextures) {
		TexNameHash_TexNameMaps textures;
		ugcObj->dumpAllTexturesFromEngineObj(ms_pEngine, textures);

		for (const auto& itTexMap : textures) {
			int texType = itTexMap.first;
			for (const auto& itTex : itTexMap.second) {
				DWORD origTexNameHash = itTex.first;
				const std::string& texName = itTex.second;
				AddTextureToUGCObj(ugcObj, texType, texName, origTexNameHash);
			}
		}
	}

	return ugcObjId;
}

void UGCManager::RemoveUGCObj(int ugcType, long long ugcObjId) {
	jsVerifyReturnVoid(m_ugcTexturedObjs.find(ugcType) != m_ugcTexturedObjs.end());
	RemoveUGCObjByIterator(ugcType, m_ugcTexturedObjs[ugcType].find(ugcObjId));
}

bool UGCManager::RemoveUGCObjByIterator(int ugcType, const UGCTexturedObjMap::iterator& itUgcObj) {
	if (itUgcObj != m_ugcTexturedObjs[ugcType].end()) {
		UGCItemUpload* pUgcObj = itUgcObj->second;
		m_ugcTexturedObjs[ugcType].erase(itUgcObj);

		delete pUgcObj;
		return true;
	}

	return false;
}

void UGCManager::ClearUGCObjList(int ugcType) {
	for (auto& itList : m_ugcTexturedObjs) {
		if ((ugcType != UGC_TYPE_UNKNOWN) && (ugcType != itList.first)) {
			continue;
		}
		for (auto it = itList.second.begin(); it != itList.second.end();) {
			RemoveUGCObjByIterator(itList.first, it);
		}
	}
}

void UGCManager::EnumUGCObjs(int ugcType, std::vector<long long>& allUGCObjIds) {
	allUGCObjIds.clear();

	if (m_ugcTexturedObjs.find(ugcType) == m_ugcTexturedObjs.end()) { // If list not created, return empty enumeration
		return;
	}

	UGCTexturedObjMap::iterator it = m_ugcTexturedObjs[ugcType].begin();
	for (; it != m_ugcTexturedObjs[ugcType].end(); ++it) {
		long long ugcObjId = it->first;
		allUGCObjIds.push_back(ugcObjId);
	}
}

UGCUploadState* UGCManager::GetUGCState(int ugcType, long long ugcObjId) {
	if (IsTextureType(ugcType)) {
		LogError("IsTextureType() FAILED");
		// icons/textures are not stored globally, can't be retrieved here.
		// call UGCObj::GetChildItem istead
		return NULL;
	} else {
		jsVerifyReturn(m_ugcTexturedObjs.find(ugcType) != m_ugcTexturedObjs.end(), NULL);

		UGCTexturedObjMap::iterator findIt = m_ugcTexturedObjs[ugcType].find(ugcObjId);
		if (findIt == m_ugcTexturedObjs[ugcType].end()) {
			LogError("UGC Textured Object Not Found - ugcObjId=" << ugcObjId << " ugcType=" << ugcType);
			return NULL;
		}

		return findIt->second;
	}
}

bool UGCManager::AddImageToUGCObj(int ugcType, long long ugcObjId, int imgType, const std::string& imgPath, long long subId /*= -1*/) {
	jsVerifyReturn(m_ugcTexturedObjs.find(ugcType) != m_ugcTexturedObjs.end(), false);

	UGCTexturedObjMap::iterator findIt = m_ugcTexturedObjs[ugcType].find(ugcObjId);
	if (findIt == m_ugcTexturedObjs[ugcType].end()) {
		LogError("UGC Textured Object Not Found - ugcObjId=" << ugcObjId << " ugcType=" << ugcType);
		return false;
	}

	switch (imgType) {
		case UGC_TYPE_TEX_ICON:
			jsVerifyReturn(findIt->second != NULL, false);
			findIt->second->addPreviewImage(imgPath, (int)subId);
			return true;
		default:
			return AddTextureToUGCObj(findIt->second, imgType, imgPath, subId);
	}
}

bool UGCManager::ClearUGCObjImagesByType(int ugcType, long long ugcObjId, int imgType) {
	jsVerifyReturn(m_ugcTexturedObjs.find(ugcType) != m_ugcTexturedObjs.end(), false);

	UGCTexturedObjMap::iterator findIt = m_ugcTexturedObjs[ugcType].find(ugcObjId);
	if (findIt == m_ugcTexturedObjs[ugcType].end()) {
		LogError("UGC Textured Object Not Found - ugcObjId=" << ugcObjId << " ugcType=" << ugcType);
		return false;
	}

	switch (imgType) {
		case UGC_TYPE_TEX_ICON:
			jsVerifyReturn(findIt->second != NULL, false);
			findIt->second->clearPreviewImages();
			return true;
		default:
			if ((imgType & UGC_TYPE_CATEMASK) == UGC_TYPE_TEXTURE) {
				findIt->second->clearChildItemByType(imgType);
				return true;
			} else {
				return false;
			}
	}
}

bool UGCManager::AddTextureToUGCObj(UGCItemUpload* pUgcObj, int texType, const std::string& texName, long long texSlotId) {
	jsVerifyReturn(pUgcObj != NULL, false);

	// Calculate Texture Name Hash
	DWORD nameHash = TextureDatabase::TextureNameHash(texName);
	if (texSlotId == -1) {
		texSlotId = (long long)nameHash; // slotID not provided, use namehash
	}

	// Add to texture list of current UGC object
	pUgcObj->addChildItem(texType, nameHash, new UGCItemTexture(texType, nameHash, texName, texSlotId));
	return true;
}

std::string EncodePropertiesToQueryString(const std::string& paramName, const std::map<int, std::string>& props) {
	std::string queryStr;
	for (auto it = props.cbegin(); it != props.cend(); ++it) {
		std::stringstream ss;
		ss << it->first << "!" << it->second;
		std::string paramStr = ss.str();
		paramStr = UrlParser::escapeString(paramStr);

		if (!queryStr.empty()) {
			queryStr.append("&");
		}

		queryStr.append(paramName);
		queryStr.append("=");
		queryStr.append(paramStr);
	}

	return queryStr;
}

bool UGCManager::SubmitUGCObj(int ugcType, long long ugcObjId, IEvent* submissionFeedbackEvent /*=NULL*/) {
	jsVerifyReturn(ms_pEngine != NULL, false);
	jsVerifyReturn(m_ugcTexturedObjs.find(ugcType) != m_ugcTexturedObjs.end(), false);

	UGCTexturedObjMap::iterator findIt = m_ugcTexturedObjs[ugcType].find(ugcObjId);
	if (findIt == m_ugcTexturedObjs[ugcType].end()) {
		LogError("UGC Textured Object Not Found - ugcObjId=" << ugcObjId << " ugcType=" << ugcType);
		return false;
	}

	UGCItemUpload* pUgcObj = findIt->second;
	if (!pUgcObj) {
		LogError("null UgcObj - ugcObjId=" << ugcObjId << " ugcType=" << ugcType);
		return false;
	}

	if (pUgcObj->checkState(PS_REQUESTING)) {
		//Reject if previous submission is in progress
		LogWarn("submission already in progress - ugcObjId=" << ugcObjId << " ugcType=" << ugcType);
		return false;
	}

	if (!pUgcObj->changeState(PS_DONTCARE, PS_REQUESTING)) {
		return false;
	}

	pUgcObj->computeUploadSize(ms_pEngine);

	EVENT_ID eventId = ms_pEngine->GetDispatcher()->GetEventId(UGCMGR_SUBMISSIONFEEDBACKEVENT);
	IEvent* e = ms_pEngine->GetDispatcher()->MakeEvent(eventId);
	(*e->OutBuffer()) << (pUgcObj->isDerivative() ? 1 : 0) << ugcType << ugcObjId;

	if (submissionFeedbackEvent) {
		submissionFeedbackEvent->SetFilter((ULONG)ugcType);
		submissionFeedbackEvent->SetObjectId((OBJECT_ID)ugcObjId);
	}
	pUgcObj->setSubmissionReplyEvent(submissionFeedbackEvent);

	// Submit through wokweb
	std::string url = pUgcObj->getSubmissionUrl(ms_pEngine);
	if (!url.empty()) {
		ms_pEngine->GetBrowserPageAsync(url.c_str(), e, 0, 0, 0, 1, true);
		return true;
	}

	return false;
}

bool UGCManager::SubmitChildItemsForUGCObj(int ugcType, long long ugcObjId, IEvent* submissionFeedbackEvent /*= NULL*/) {
	jsVerifyReturn(ms_pEngine != NULL, false);
	jsVerifyReturn(m_ugcTexturedObjs.find(ugcType) != m_ugcTexturedObjs.end(), false);

	UGCTexturedObjMap::iterator findIt = m_ugcTexturedObjs[ugcType].find(ugcObjId);
	if (findIt == m_ugcTexturedObjs[ugcType].end()) {
		LogError("UGC Textured Object Not Found - ugcObjId=" << ugcObjId << " ugcType=" << ugcType);
		return false;
	}

	return SubmitChildItemsForUGCObj(*findIt->second, submissionFeedbackEvent);
}

bool UGCManager::SubmitChildItemsForUGCObj(UGCItemUpload& ugcObj, IEvent* submissionFeedbackEvent /*= NULL */) {
	// must be either submitted or uploaded
	if (!ugcObj.checkState(PS_REQUESTED | PS_UPLOADED)) {
		return false;
	}

	// Submit child items
	int submitCount = 0;
	for (const auto& group : ugcObj.getAllChildItems()) {
		for (const auto& pr : group.second) {
			UGCUploadChildState* pChildItem = pr.second;
			DoSubmitChildItem(ugcObj.getType(), ugcObj.getId(), *pChildItem, ugcObj.getUploadId().c_str(), submissionFeedbackEvent);
			submitCount++;
		}
	}

	return submitCount > 0;
}

bool UGCManager::DoSubmitChildItem(int parentType, int parentId, UGCUploadChildState& childState, const char* masterUploadId, IEvent* submissionFeedbackEvent) {
	jsVerifyReturn(ms_pEngine != NULL, false);

	// Get upload type based on list id
	char* szUploadType = GetUploadTypeStringFromUGCType(childState.getType());
	jsVerifyReturn(szUploadType != NULL, false);
	std::string uploadType = szUploadType;

	if (!childState.changeState(PS_DONTCARE, PS_REQUESTING)) {
		return false;
	}

	// record master upload ID
	assert(masterUploadId != NULL);
	childState.setMasterUploadId(masterUploadId);

	EVENT_ID eventId = ms_pEngine->GetDispatcher()->GetEventId(UGCMGR_SUBMISSIONFEEDBACKEVENT);
	IEvent* e = ms_pEngine->GetDispatcher()->MakeEvent(eventId);
	int ugcType = childState.getType();
	long long ugcObjId = childState.getId();
	(*e->OutBuffer()) << 0 << ugcType << ugcObjId << parentType << parentId;

	if (submissionFeedbackEvent) {
		submissionFeedbackEvent->SetFilter((ULONG)ugcType);
		submissionFeedbackEvent->SetObjectId((OBJECT_ID)ugcObjId);
	}
	childState.setSubmissionReplyEvent(submissionFeedbackEvent);

	// Submit through wokweb
	std::string url = childState.getSubmissionUrl(ms_pEngine);
	if (!url.empty()) {
		ms_pEngine->GetBrowserPageAsync(url.c_str(), e, 0, 0, 0, 1, true);
		return true;
	}

	return false;
}

bool UGCManager::Upload(const std::string& uploadId, int ugcType, long long ugcObjId, IEvent* uploadProgressEvent, IEvent* uploadCompletionEvent) {
	jsVerifyReturn(ms_pEngine != NULL, false);
	jsVerifyReturn(GetUploadTypeStringFromUGCType(ugcType) != NULL, false);

	if (IsChildType(ugcType)) {
		// To upload child items, call UploadChildItem instead
		return false;
	}

	UGCUploadState* pUgcState = NULL;
	if (m_ugcTexturedObjs.find(ugcType) != m_ugcTexturedObjs.end()) {
		UGCTexturedObjMap::iterator findIt = m_ugcTexturedObjs[ugcType].find(ugcObjId);
		if (findIt != m_ugcTexturedObjs[ugcType].end())
			pUgcState = findIt->second;
	}

	if (pUgcState == NULL) {
		LogError("state not found, cancel upload - ugcObjId=" << ugcObjId << " ugcType=" << ugcType);

		// If dynamic object no longer exists, cancel upload request
		std::string url = UGC_UPLOAD_CANCEL_URL;
		STLStringSubstitute(url, TAG_WOK_URL, ms_pEngine->GetURL(CFG_WOKURL, false));
		STLStringSubstitute(url, TAG(UGC_UPLOAD_TYPE), GetUploadTypeStringFromUGCType(ugcType));
		STLStringSubstitute(url, TAG(UGC_UPLOAD_ID), uploadId);
		ms_pEngine->GetBrowserPageAsync(url.c_str(), NULL, 0, 0, 0, 1, true);

		return false;
	}

	return DoUpload(uploadId, pUgcState, uploadProgressEvent, uploadCompletionEvent);
}

UGCItemUpload* UGCManager::GetUGCObjByUploadId(const std::string& uploadId) {
	for (auto itList = m_ugcTexturedObjs.begin(); itList != m_ugcTexturedObjs.end(); ++itList) {
		for (auto itUgcObj = itList->second.begin(); itUgcObj != itList->second.end(); ++itUgcObj) {
			if (itUgcObj->second->getUploadId() == uploadId) {
				return itUgcObj->second;
			}
		}
	}

	// not found
	return NULL;
}

void UGCManager::ReportUGCObjUploadProgress(const std::string& masterUploadId, int ugcType, int ugcObjId, const char* szSubid, int uploadState, float percentage) {
	UGCItemUpload* pUgcObj = GetUGCObjByUploadId(masterUploadId);
	if (!pUgcObj) {
		return;
	}

	std::string sChildId = "-1";

	UGCUploadState* pState;
	if (szSubid == NULL || szSubid[0] == 0) {
		// master object
		pState = pUgcObj;
	} else {
		// other types: assuming child items - including textures
		UGCUploadChildState* pChildItem = pUgcObj->getChildItem(ugcType, ugcObjId);
		if (pChildItem) {
			sChildId = pChildItem->getChildId();
		}
		pState = pChildItem;
	}

	if (pState) {
		if (uploadState == TS_PROC_PROGRESS) {
			unsigned currItemProgressInBytes = (unsigned)(pState->getBytesTotal() * percentage / 100.0f);
			pUgcObj->updateChildItemProgressInBytes(sChildId, currItemProgressInBytes);
		} else if (uploadState == TS_PROC_START) {
			pUgcObj->fileUploadStarted();
		} else if (uploadState == TS_PROC_COMPLETE) {
			pUgcObj->fileUploadCompleted();
		}
	}
}

float UGCManager::GetUGCObjUploadProgress(const std::string& masterUploadId, int& filesTotal, int& filesCompleted, int& filesInProgress, FileSize& bytesTotal, FileSize& bytesProcessed) {
	double percentage = -1;

	filesTotal = 0;
	filesCompleted = 0;
	filesInProgress = 0;
	bytesTotal = 0;
	bytesProcessed = 0;

	UGCItemUpload* pUgcObj = GetUGCObjByUploadId(masterUploadId);
	if (pUgcObj != NULL) {
		filesTotal = pUgcObj->getUploadFilesTotal();
		filesCompleted = pUgcObj->getUploadFilesCompleted();
		filesInProgress = pUgcObj->getUploadFilesInProgress();
		bytesTotal = pUgcObj->getUploadBytesTotal();
		bytesProcessed = pUgcObj->getUploadBytesProcessed();

		if (bytesTotal > 0)
			percentage = ((double)bytesProcessed * 100.0) / (double)bytesTotal;
	}

	return (float)percentage;
}

void UGCManager::FireUGCObjUploadProgressEvent(const std::string& masterUploadId) {
	UGCItemUpload* pUgcObj = GetUGCObjByUploadId(masterUploadId);
	if (!pUgcObj) {
		return;
	}

	IEvent* uploadProgressEvent = pUgcObj->getUploadProgressEvent();
	long long ugcObjId = pUgcObj->getId();

	if (uploadProgressEvent != NULL) {
		int filesTotal = 0, filesCompleted = 0, filesInProgress = 0;
		FileSize bytesTotal = 0, bytesProcessed = 0;
		int percentage = -1;
		percentage = (int)floor(GetUGCObjUploadProgress(masterUploadId, filesTotal, filesCompleted, filesInProgress, bytesTotal, bytesProcessed) + 0.5);

		if (!pUgcObj->setLastPercentage(percentage)) {
			// Percentage not changed
			return;
		}

		// Create an event with same ID as template
		IEvent* e = ms_pEngine->GetDispatcher()->MakeEvent(uploadProgressEvent->EventId());

		// Copy attributes
		e->SetFilter(uploadProgressEvent->Filter());
		e->SetObjectId(uploadProgressEvent->ObjectId());

		// Copy from and to
		e->SetFrom(uploadProgressEvent->From());
		for (auto it = uploadProgressEvent->To().begin(); it != uploadProgressEvent->To().end(); ++it) {
			e->AddTo(*it);
		}

		// Copy all data from template
		e->OutBuffer()->SetFromReadable(uploadProgressEvent->InBuffer());

		// Append progress information
		(*e->OutBuffer()) << pUgcObj->getType() << masterUploadId << ugcObjId;
		(*e->OutBuffer()) << percentage << filesTotal << filesCompleted << filesInProgress << (DWORD)bytesTotal << (DWORD)bytesProcessed;

		ms_pEngine->GetDispatcher()->QueueEvent(e);
	}
}

void UGCManager::FireUGCSubmissionReplyEvent(UGCUploadState& ugcState, const std::string& resultMsg, int httpStatusCode) {
	IEvent* e = ugcState.getSubmissionReplyEvent();
	VERIFY_RETVOID(e != nullptr);

	(*e->OutBuffer()) << resultMsg << httpStatusCode;
	ms_pEngine->GetDispatcher()->QueueEvent(e);
}

bool UGCManager::DeployUGCUpload(const std::string& uploadId, int type, IEvent* deployCompletionEvent) {
	// Submit through wokweb
	std::string url = UGC_DEPLOY_REQUEST_URL;
	STLStringSubstitute(url, TAG_WOK_URL, ms_pEngine->GetURL(CFG_WOKURL, false));
	STLStringSubstitute(url, TAG(UGC_UPLOAD_TYPE), GetUploadTypeStringFromUGCType(type));
	STLStringSubstitute(url, TAG(UGC_UPLOAD_ID), uploadId);
	ms_pEngine->GetBrowserPageAsync(url.c_str(), deployCompletionEvent, 0, 0, 0, 1, true);
	return true;
}

// DRF - Support MP3 Imports
static void __stdcall CbFunc(unsigned long FrameCnt, unsigned long InByteTot, struct mad_header* Header) {}

int UGCManager::ImportSoundFile(const std::string& filePathSnd, bool encrypt) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return -1;

	std::string filePath = filePathSnd;

	// Create Temp Path Imported '<pathApp>\MapsModels\imported\'
	std::string tmpPath = CreatePathMapsModelsImported();

	// Get MD5 File Hash Name
	std::string hashName = GetFileHash(filePath);

	// Copy To Hash-Named Temp Path (skip if already exists)
	std::string fileExt = StrFileExt(filePath);
	std::string filePathOut = PathAdd(tmpPath, hashName + "." + fileExt);
	bool alreadyExists = FileHelper::Exists(filePathOut);
	if (alreadyExists || FileHelper::Copy(filePath, filePathOut))
		filePath = filePathOut;

	// Create UGC Sound From File
	UGCSound* pUGCS = NULL;
	if (StrSameFileExt(filePath, "wav")) {
		// WAV -> Convert To OGG (in memory only)
		int errorCode;
		int bufLen = 0;
		LogInfo("Converting WAV to OGG(mem) - '" << filePath << "' ...");
		BYTE* buffer = OggConverter::convert(filePath, bufLen, errorCode);
		if (!buffer) {
			LogError("OggConverter::convert() FAILED - '" << filePath << "'");
			return errorCode;
		}
		LogInfo("OK");

		// Create Sound From Memory
		pUGCS = new UGCSound(buffer, bufLen, filePath);
		if (!pUGCS) {
			LogError("new UGCSound(mem) FAILED - bufLen=" << bufLen);
			delete buffer; // drf - memory leak fix
		}
	} else if (StrSameFileExt(filePath, "ogg")) {
		// OGG -> Create Sound Directly From File
		pUGCS = new UGCSound(filePath);
		if (!pUGCS) {
			LogError("new UGCSound(file) FAILED - '" << filePath << "'");
		}
	} else {
		LogError("UNSUPPORTED SOUND FORMAT - '" << filePath << "'");
		return -1;
	}

	// Sound Created ?
	if (!pUGCS) {
		LogError("UGCSound() FAILED");
		return -1;
	}

	assert(pUGCS != nullptr);

	pICE->GetUGCManager()->AddUGCObj(pUGCS, false);

	// Prepare For UGC Upload
	FileSize theSize = 0;
	std::vector<std::string> retUploadFiles;
	BYTE* theBuff = pUGCS->prepareDataForUpload(pICE, theSize, retUploadFiles);
	if (!theBuff) {
		LogError("prepareDataForUpload() FAILED - '" << filePath << "'");
		return -1;
	}

	// Get Sound Resource Manager
	auto pSRM = pICE->GetSoundRM();
	if (!pSRM) {
		return -1;
	}

	// Immediately Loads Proxy To Be Played (and deletes theBuf)
	pSRM->addUGCProxy(pUGCS->getId(), theBuff, theSize);

	// Set Sound Duration
	OALSoundProxy* ugcProx = pSRM->getProxy(pUGCS->getId(), false);
	if (ugcProx) {
		pUGCS->setDurationMS(ugcProx->GetDuration());
	}

	// Encrypt Sound Data
	if (encrypt) {
		pUGCS->padAndEncrypt();
	}

	return pUGCS->getId();
}

static EVENT_PROC_RC ProcessUGCManagerUploadProgressEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	UGCManager* pUgcMgr = reinterpret_cast<UGCManager*>(lparam);
	jsVerifyReturn(pUgcMgr != NULL, EVENT_PROC_RC::NOT_PROCESSED);

	std::string sUploadId;
	(*e->InBuffer()) >> sUploadId;

	int ugcType = UGC_TYPE_UNKNOWN;
	(*e->InBuffer()) >> ugcType;

	long long ugcObjId = -1;
	(*e->InBuffer()) >> ugcObjId;

	std::string sTaskType; // should be "UPLOAD"
	ULONG taskId; // no use
	int state = -1;
	float percent; // percentage
	DWORD bytesProcessed = 0, bytesTotal = 0;
	std::string masterUploadId = sUploadId; // For parent items, use its own upload ID, otherwise will be updated below
	std::string subId;

	if (UGCManager::IsMasterType(ugcType)) {
		std::string name;

		(*e->InBuffer()) >> sTaskType >> taskId >> state >> percent >> bytesProcessed >> bytesTotal;

		UGCUploadState* pUgcState = pUgcMgr->GetUGCState(ugcType, ugcObjId);
		assert(pUgcState != NULL);

		if (state == TS_COMPLETE) {
			// change state to "uploaded"
			pUgcState->changeState(PS_UPLOADING, PS_UPLOADED);
		} else if (state == TS_FAILED) {
			// change state to "failed"
			pUgcState->changeState(PS_UPLOADING, PS_FAILED);
		} else if (state == TS_CANCELED) {
			// change state to "canceled"
			pUgcState->changeState(PS_UPLOADING, PS_CANCELLED);
		}
	} else if (UGCManager::IsTextureType(ugcType)) {
		(*e->InBuffer()) >> masterUploadId >> subId;
		(*e->InBuffer()) >> sTaskType >> taskId >> state >> percent >> bytesProcessed >> bytesTotal;

		UGCUploadState* pUgcState = NULL;
		UGCItemUpload* pParentObj = pUgcMgr->GetUGCObjByUploadId(masterUploadId);
		assert(pParentObj != NULL);
		if (pParentObj) {
			pUgcState = pParentObj->getChildItem(ugcType, ugcObjId);
		}
		assert(pUgcState != NULL);

		if (state == TS_COMPLETE) {
			// change texture state to "uploaded"
			if (pUgcState) {
				pUgcState->changeState(PS_UPLOADING, PS_UPLOADED);
			}
		} else if (state == TS_FAILED) {
			if ((ugcType & UGC_TYPE_TYPEMASK) == UGC_TYPE_TEX_ICON) {
				// If icon upload failed, ignore error and assume completion
				if (pUgcState) {
					pUgcState->changeState(PS_UPLOADING, PS_UPLOADED);
				}
				state = TS_COMPLETE;
				percent = 100;
				bytesProcessed = bytesTotal;
			}
		}
	} else {
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	if (state != -1) {
		pUgcMgr->ReportUGCObjUploadProgress(masterUploadId, ugcType, ugcObjId, subId.empty() ? NULL : subId.c_str(), state, percent);
		if (state == TS_PROC_PROGRESS || state == TS_PROC_COMPLETE) {
			pUgcMgr->FireUGCObjUploadProgressEvent(masterUploadId);
		}
	}

	// consume the event
	return EVENT_PROC_RC::CONSUMED;
}

// Copied from UGCUtility.cs
//@@Todo: move to a proper location
#define UGCLINK_RET_INTERNAL_ERROR -1
#define UGCLINK_RET_UNKNOWN_TYPE -2
#define UGCLINK_RET_VALIDATION_FAILED -3
#define UGCLINK_RET_NO_UPLOADS 0
#define UGCLINK_RET_SUCCEEDED 1
#define UGCLINK_RET_TOO_MANY_FILES 9999

#define UGCLINK_RET_TAG_RETURNCODE "ReturnCode"
#define UGCLINK_RET_TAG_UPLOADID "UploadId"
#define UGCLINK_RET_TAG_STATE "State"
#define UGCLINK_RET_TAG_GLOBALID "GlobalId"

std::string ParseXMLTag(const std::string& xml, const std::string& tag) {
	size_t ofsTag, ofsEndTag;

	ofsTag = xml.find("<" + tag + ">");
	ofsEndTag = xml.find("</" + tag + ">");
	if (ofsTag != std::string::npos && ofsEndTag != std::string::npos && ofsEndTag > ofsTag) {
		size_t ofsValue = ofsTag + tag.size() + 2;
		return xml.substr(ofsValue, ofsEndTag - ofsValue);
	}

	return "";
}

bool ParseUGCLinkResponseMsg(const std::string& sXML, int& result, std::string& sUploadId, int& state, long long& globalId) {
	std::string sResult, sState, sGlobalId;

	sUploadId.empty();
	result = -1;
	state = -1;
	globalId = -1;

	sResult = ParseXMLTag(sXML, UGCLINK_RET_TAG_RETURNCODE);
	if (sResult.empty())
		return false;

	result = atoi(sResult.c_str());

	sUploadId = ParseXMLTag(sXML, UGCLINK_RET_TAG_UPLOADID);
	sState = ParseXMLTag(sXML, UGCLINK_RET_TAG_STATE);
	sGlobalId = ParseXMLTag(sXML, UGCLINK_RET_TAG_GLOBALID);

	if (!sState.empty()) {
		state = atoi(sState.c_str());
	}
	if (!sGlobalId.empty()) {
		globalId = _atoi64(sGlobalId.c_str());
	}

	return true;
}

static EVENT_PROC_RC ProcessUGCManagerSubmissionReplyEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	UGCManager* pUgcMgr = reinterpret_cast<UGCManager*>(lparam);
	jsVerifyReturn(pUgcMgr != NULL, EVENT_PROC_RC::NOT_PROCESSED);

	int isDerivation = 0;
	int ugcType = UGC_TYPE_UNKNOWN;
	long long ugcObjId = -1;
	std::string resultText;
	int httpStatusCode;

	int resultCode, state;
	std::string sUploadId;
	long long globalId;

	(*e->InBuffer()) >> isDerivation >> ugcType >> ugcObjId;

	if (UGCManager::IsMasterType(ugcType)) {
		UGCUploadState* pUgcState = pUgcMgr->GetUGCState(ugcType, ugcObjId);
		if (!pUgcState) {
			LogError("upload state not found - ugcObjId=" << ugcObjId << " ugcType=" << ugcType);
		} else {
			(*e->InBuffer()) >> resultText >> httpStatusCode;

			if (httpStatusCode != 200) {
				LogError("server returned HTTP " << httpStatusCode << " - ugcObjId=" << ugcObjId << " ugcType=" << ugcType);
				pUgcState->changeState(PS_REQUESTING, PS_FAILED);
			} else {
				ParseUGCLinkResponseMsg(resultText, resultCode, sUploadId, state, globalId);

				if (resultCode == UGCLINK_RET_SUCCEEDED) {
					// change UGC state to "requested"
					pUgcState->changeState(PS_REQUESTING, PS_REQUESTED);
				} else {
					// change UGC state to "rejected"
					pUgcState->changeState(PS_REQUESTING, PS_REJECTED);
				}
			}

			pUgcMgr->FireUGCSubmissionReplyEvent(*pUgcState, resultText, httpStatusCode);
		}
	} else if (UGCManager::IsChildType(ugcType)) {
		int parentType = UGC_TYPE_UNKNOWN;
		int parentId = -1;
		(*e->InBuffer()) >> parentType >> parentId >> resultText >> httpStatusCode;

		UGCUploadState* pUgcState = pUgcMgr->GetUGCState(parentType, parentId);
		UGCItemUpload* pObj = dynamic_cast<UGCItemUpload*>(pUgcState);
		UGCUploadChildState* pChildState = NULL;
		if (pObj) {
			pChildState = pObj->getChildItem(ugcType, ugcObjId);
		}

		if (!pChildState) {
			LogError("upload state not found - ugcObjId=" << ugcObjId << " ugcType=" << ugcType << " parentType=" << parentType << " parentId=" << parentId);
		} else {
			if (httpStatusCode != 200) {
				LogError("server returned HTTP " << httpStatusCode << " - ugcObjId=" << ugcObjId << " ugcType=" << ugcType << " parentType=" << parentType << " parentId=" << parentId);
				pChildState->changeState(PS_REQUESTING, PS_FAILED);
			} else {
				ParseUGCLinkResponseMsg(resultText, resultCode, sUploadId, state, globalId);
				if (resultCode == UGCLINK_RET_SUCCEEDED) {
					// change texture state to "requested"
					pChildState->changeState(PS_REQUESTING, PS_REQUESTED);
				} else {
					// change UGC state to "rejected"
					pChildState->changeState(PS_REQUESTING, PS_REJECTED);
				}

				pUgcMgr->UploadChildItem(sUploadId, pObj, ugcType, ugcObjId, NULL, NULL);
			}
		}
	} else {
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	// consume the event
	return EVENT_PROC_RC::CONSUMED;
}

IUGCManager* InitUGCManager(IClientEngine* pEngine) {
	UGCManager* ptr = new UGCManager();
	ptr->SetEngine(pEngine);

	EVENT_ID eventId = pEngine->GetDispatcher()->RegisterEvent(UGCMGR_UPLOADPROGRESSEVENT, PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	pEngine->GetDispatcher()->RegisterHandler(ProcessUGCManagerUploadProgressEvent, (ULONG)ptr, UGCMGR_UPLOADPROGRESSEVENT, PackVersion(1, 0, 0, 0), eventId, EP_HIGH);

	eventId = pEngine->GetDispatcher()->RegisterEvent(UGCMGR_SUBMISSIONFEEDBACKEVENT, PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	pEngine->GetDispatcher()->RegisterHandler(ProcessUGCManagerSubmissionReplyEvent, (ULONG)ptr, UGCMGR_SUBMISSIONFEEDBACKEVENT, PackVersion(1, 0, 0, 0), eventId, EP_HIGH);

	ptr->RegisterImportType(UGC_TYPE_COLLADA, "COLLADA 3D Files", "dae"); // Register this before DYNOBJ/ANIM to make sure generic type takes priority
	ptr->RegisterImportType(UGC_TYPE_DYNAMIC_OBJ, "3D Mesh Files", "dae");
	ptr->RegisterImportType(UGC_TYPE_ANIMATION, "Character Animation Files", "dae");
	ptr->RegisterImportType(UGC_TYPE_EQUIPPABLE, "3D Mesh Files", "dae");
	ptr->RegisterImportType(UGC_TYPE_SCRIPT_DYNAMIC_OBJ, "Dynamic Object Scripts", "lua");

	ptr->RegisterImportType(UGC_TYPE_SOUND_OGG, "OGG Sound Files", "ogg");
	ptr->RegisterImportType(UGC_TYPE_SOUND_WAV, "WAV Sound Files", "wav");
	ptr->RegisterImportType(UGC_TYPE_SOUND_MP3, "MP3 Sound Files", "mp3");

	std::set<std::string> soundExts;
	soundExts.insert("ogg");
	soundExts.insert("wav");
	soundExts.insert("mp3");
	ptr->RegisterImportType(UGC_TYPE_SOUND, "Sound Files", soundExts);

	// DRF - Amended (must match TextureImporter::s_fmtMap)
	std::set<std::string> textureExts;
	textureExts.insert("jpg");
	textureExts.insert("jpeg");
	textureExts.insert("jpe");
	textureExts.insert("jfif");
	textureExts.insert("tif");
	textureExts.insert("tiff");
	textureExts.insert("bmp");
	textureExts.insert("dib");
	textureExts.insert("gif");
	textureExts.insert("png");
	textureExts.insert("tga");
	textureExts.insert("dds");

	std::set<std::string> assetExts;
	assetExts.insert("jpg");
	assetExts.insert("gif");
	assetExts.insert("bmp");
	assetExts.insert("png");
	assetExts.insert("tga");
	assetExts.insert("dds");
	assetExts.insert("xml");
	assetExts.insert("lua");

	ptr->RegisterImportType(UGC_TYPE_TEXTURE, "Textures", textureExts); // Register this before TEX_DO/TEX_ZONE to make sure generic type takes priority
	ptr->RegisterImportType(UGC_TYPE_TEX_DYNAMIC_OBJ, "Dynamic Object Textures", textureExts);
	ptr->RegisterImportType(UGC_TYPE_TEX_WORLD_OBJ, "World Object Textures", textureExts);
	ptr->RegisterImportType(UGC_TYPE_APP_EDIT, "Publish assets", assetExts);
	return ptr;
}

void DisposeUGCManager(IUGCManager* ugcMgr) {
	if (ugcMgr) {
		delete ugcMgr;
	}
}

bool UGCManager::UploadChildItem(const std::string& uploadId, UGCItemUpload* pUgcObj, int childType, int childId, IEvent* uploadProgressEvent, IEvent* uploadCompletionEvent) {
	jsVerifyReturn(ms_pEngine != NULL, false);
	jsVerifyReturn(pUgcObj != NULL, false);

	UGCUploadChildState* pChildItem = pUgcObj->getChildItem(childType, childId);
	if (!pChildItem) {
		return false;
	}

	return DoUpload(uploadId, pChildItem, uploadProgressEvent, uploadCompletionEvent);
}

void UGCManager::SendEventToEngine(IEvent* e) {
	ms_pEngine->GetDispatcher()->QueueEvent(e);
}

bool UGCManager::DoUpload(const std::string& uploadId, UGCUploadState* pUgcState, IEvent* uploadProgressEvent, IEvent* uploadCompletionEvent) {
	if (!pUgcState->changeState(PS_REQUESTED, PS_UPLOADING)) {
		return false;
	}

	// record upload ID
	pUgcState->setUploadId(uploadId);
	pUgcState->setUploadProgressEvent(uploadProgressEvent);
	pUgcState->setUploadCompletionEvent(uploadCompletionEvent);

	FileSize buffLen = 0;
	std::vector<std::string> uploadFiles;
	BYTE* data = pUgcState->prepareDataForUpload(ms_pEngine, buffLen, uploadFiles);
	assert(data == NULL || uploadFiles.empty()); // only support one type

	if (data || !uploadFiles.empty()) {
		std::string url = UGC_UPLOAD_DATA_URL;
		STLStringSubstitute(url, TAG_WOK_URL, ms_pEngine->GetURL(CFG_WOKURL, false));

		ValuesList valuesToPost;
		valuesToPost.push_back(std::make_pair("game", "true")); // This is a request from game
		valuesToPost.push_back(std::make_pair(UGC_ACTION, UGC_ACTION_DATA));
		valuesToPost.push_back(std::make_pair(UGC_UPLOAD_ID, uploadId));
		valuesToPost.push_back(std::make_pair(UGC_UPLOAD_TYPE, GetUploadTypeStringFromUGCType(pUgcState->getType())));
		valuesToPost.push_back(std::make_pair(UGC_ORIG_NAME, pUgcState->getOrigName()));

		// Append original hash/size if specified
		if (!pUgcState->getOrigHash().empty()) {
			std::stringstream ssOrigSize;
			UINT origSize = pUgcState->getOrigSize();
			ssOrigSize << origSize;
			valuesToPost.push_back(make_pair(UGC_ORIG_SIZE, ssOrigSize.str()));
			valuesToPost.push_back(make_pair(UGC_ORIG_HASH, pUgcState->getOrigHash()));
		}

		//Bake login url
		std::string authUrl = ms_pEngine->GetURL(CFG_AUTHURL, true); //m_authUrl;

		// Master upload progress event -> will trigger custom upload progress event (as passed in parameter) in its handler
		EVENT_ID eventId = ms_pEngine->GetDispatcher()->GetEventId(UGCMGR_UPLOADPROGRESSEVENT);
		IEvent* e = ms_pEngine->GetDispatcher()->MakeEvent(eventId);
		int ugcType = pUgcState->getType();
		long long ugcObjId = pUgcState->getId();
		(*e->OutBuffer()) << uploadId << ugcType << ugcObjId;

		if (IsTextureType(ugcType)) {
			UGCUploadChildState* pChildState = dynamic_cast<UGCUploadChildState*>(pUgcState);
			if (pChildState) {
				(*e->OutBuffer()) << pChildState->getMasterUploadId() << pChildState->getChildId();
			}
		}

		std::stringstream ss;
		ss << ugcObjId;
		if (data) {
			return ms_pEngine->UploadFileAsync(ss.str().c_str(), (char*)data, buffLen, url.c_str(), TSK_PRIO_MEDIUM, &valuesToPost, true, authUrl.c_str(), e, TS_ALL_EVENTS);
		} else if (uploadFiles.size() == 1) {
			return ms_pEngine->UploadFileAsync(uploadFiles[0].c_str(), url.c_str(), TSK_PRIO_MEDIUM, &valuesToPost, true, authUrl.c_str(), e, TS_ALL_EVENTS);
		} else {
			return ms_pEngine->UploadMultiFilesAsync(uploadFiles, url.c_str(), TSK_PRIO_MEDIUM, &valuesToPost, true, authUrl.c_str(), e, TS_ALL_EVENTS);
		}
	} else {
		// nothing to upload for main entry
		if (!pUgcState->changeState(PS_UPLOADING, PS_UPLOADED)) {
			return false;
		}
	}

	return false;
}

int UGCManager::ImportActionItem(const std::string& fullPath, int defaultGlid, const std::string& bundleGlids, int parentGlid) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return -1;

	assert(!fullPath.empty()); // source file name must have been provided
	if (fullPath.empty()) {
		LogError("file name not specified for import");
		return -1;
	}

	if (defaultGlid == 0) {
		LogError("default glid not specified for action item");
		return -1;
	}

	UGCActionItem* pUGCObj = new UGCActionItem(fullPath, defaultGlid, bundleGlids, parentGlid);
	if (pUGCObj) {
		pICE->GetUGCManager()->AddUGCObj(pUGCObj, false);
		return pUGCObj->getId();
	}

	return -1;
}

} // namespace KEP
