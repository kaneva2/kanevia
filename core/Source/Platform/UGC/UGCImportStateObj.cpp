///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UGCImportStateObj.h"
#include "Common/include/CoreStatic/CMaterialClass.h"

namespace KEP {

void ISOMaterial::ProcessMaterial(CMaterialObject& material, bool bFinalize /*= false*/) {
	jsVerifyReturnVoid(&material != NULL);

	// restore ambient from m_baseMaterial
	material.m_useMaterial.Ambient = material.m_baseMaterial.Ambient;

	if (m_autoCorrectAmbient) {
		float targetR = 0.8f, targetG = 0.8f, targetB = 0.8f; // preset ambient color

		D3DCOLORVALUE amb = material.m_useMaterial.Ambient;
		// check if we got enough brightness on ambient
		if (amb.r < targetR && amb.g < targetG && amb.b < targetB) {
			// not enough ambient brightness: correct ambient color
			if (amb.r < 1E-5 || amb.g < 1E-5 || amb.b < 1E-5) { // if completely dark, use preset directly
				amb.r = targetR;
				amb.g = targetG;
				amb.b = targetB;
			} else { // otherwise compute final color
				float amp = 1.0f;
				if (amb.r > amb.g && amb.r > amb.b)
					amp = targetR / amb.r; // red is dominant
				else if (amb.g > amb.b)
					amp = targetG / amb.g; // green is dominent
				else
					amp = targetB / amb.b; // blue is dominant

				amb.r *= amp;
				amb.g *= amp;
				amb.b *= amp;
			}

			if (amb.a < 1E-5) {
				amb.a = material.m_useMaterial.Diffuse.a;
			}

			material.m_useMaterial.Ambient = amb;
		}
	}

	if (bFinalize) {
		// Finalizing materials: copy back ambient from m_useMaterial to m_baseMaterial
		// This is the last call to ProcessMaterial before disposal of import state
		material.m_baseMaterial.Ambient = material.m_useMaterial.Ambient;
	}
}

} // namespace KEP