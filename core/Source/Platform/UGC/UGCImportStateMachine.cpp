///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UGCImportStateMachine.h"
#include "UGCImport.h"
#include "Event/Events/UGCImportEvent.h"

namespace KEP {

UGCImportStateMachine::UGCImportStateMachine(IDispatcher* pDispatcher) :
		m_pDispatcher(pDispatcher) {
	assert(pDispatcher != NULL);
}

void UGCImportStateMachine::doUpdate(IUGCImportStateObj* pStateObj, UGCImportEventType eventType) {
	m_pDispatcher->QueueEvent(new UGCImportEvent(pStateObj, eventType));
}

EVENT_PROC_RC UGCImportStateMachine::UGCImportEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (UGCImportStateMachine*)lparam;
	if (!me)
		return EVENT_PROC_RC::NOT_PROCESSED;

	auto uie = dynamic_cast<UGCImportEvent*>(e);
	if (!uie)
		return EVENT_PROC_RC::NOT_PROCESSED;

	IUGCImportStateObj* pStateObj = NULL;
	int eventType = EV_NONE;
	uie->ExtractInfo(pStateObj, eventType);
	if (!pStateObj)
		return EVENT_PROC_RC::NOT_PROCESSED;

	me->handleEvent(pStateObj, (UGCImportEventType)eventType);
	return EVENT_PROC_RC::CONSUMED;
}

void UGCImportStateMachine::handleEvent(IUGCImportStateObj* pStateObj, UGCImportEventType eventType) {
	if (m_pDispatcher == NULL || pStateObj == NULL || eventType != EV_DESTROYED && (pStateObj->m_eventId == -1 || pStateObj->m_pImporter == NULL))
		return;

	if (eventType == EV_DESTROYED) {
		// do not use pStateObj -- probably already deleted
		// no action is required if a state is destroyed
		return;
	}

	for (size_t i = 0; i < pStateObj->m_pImporter->getStateTransitionTableEntryCount(); i++) {
		const UGCImportSTTEntry& entry = pStateObj->m_pImporter->getStateTransitionTableEntry(i);
		if ((entry.fromState == pStateObj->m_state || entry.fromState == UGCIS_ANY) && entry.expectedEvent == eventType) {
			changeState(pStateObj, entry.toState);
			UGCImporter* imp = pStateObj->m_pImporter;
			imp->performAction(entry.actionToPerform, pStateObj, this);

			if (pStateObj->m_state != UGCIS_DESTROYING) { // Don't queue EV_NONE if import state is being destroyed. Otherwise there is a chance that EV_NONE will be processed after destroy.
				// Trigger a NULL event to allow automatic state changes (if specified in state diagram)
				doUpdate(pStateObj, EV_NONE);
			}
			return;
		}
	}

	// If no matching rule and event is not EV_NONE, report an pseudo state change event with original state
	if (eventType != EV_NONE) {
		// This is to respond a unrecognized state tickle attempt
		changeState(pStateObj, pStateObj->m_state);
	}
}

void UGCImportStateMachine::changeState(IUGCImportStateObj* pStateObj, UGCImportState newState) {
	if (pStateObj == NULL)
		return;

	pStateObj->m_state = newState;

	if (m_pDispatcher == NULL || pStateObj->m_eventId == -1)
		return;

	IEvent* e = m_pDispatcher->MakeEvent(pStateObj->m_eventId);
	e->SetObjectId((OBJECT_ID)pStateObj->m_eventObjId);
	e->SetFilter((ULONG)pStateObj->m_state);
	m_pDispatcher->QueueEvent(e);
}

} // namespace KEP
