///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "UGCImportStateObj.h"

namespace KEP {

class RuntimeSkeleton;
class SkeletonConfiguration;
class CSkeletonObject;

class SkeletalObjectImportState : public IUGCImportStateObj, public ISOMaterial, public ISOScalable {
public:
	CSkeletonObject* skeleton;
	CSkeletonObject* savedSkeleton;

	SkeletalObjectImportState() :
			skeleton(NULL), savedSkeleton(NULL) {
	}

	~SkeletalObjectImportState();

	//void ProcessSkeletonScaling( CSkeletonObject* pSkeletonToProcess );
	void ProcessSkeletonMaterials(RuntimeSkeleton* pSkeletonToProcess, const SkeletonConfiguration* pSkeletonCfg, bool bFinalize = false);
	bool ProcessSkeletonMeshes(RuntimeSkeleton* pSkeletonToProcess, const SkeletonConfiguration* pSkeletonCfg);
};

} // namespace KEP