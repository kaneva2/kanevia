///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "UGCUploadState.h"
#include "BoundBox.h"
#include "ObjectType.h"

//////////////////////////////////////////////////////////
// UGC upload states

namespace KEP {

class IResource;
class IDynamicObjectProxy;
class StreamableDynamicObject;

// UGC dynamic object
class UGCDynamicObj : public UGCItemUpload_IResource {
private:
	StreamableDynamicObject* m_pDynObj;

public:
	UGCDynamicObj(StreamableDynamicObject* pDynObj, GLID glid, IResource* pRes, int state = PS_CREATED);

	virtual std::string getOrigName() const;

	virtual void dumpAllTexturesFromEngineObj(IClientEngine* pEngine, TexNameHash_TexNameMaps& allTexturesFound);

protected:
	virtual PropertySource* getPropertySource();
};

// UGC dynamic object derivation
class UGCDynamicObjDerivation : public UGCItemDerivation {
private:
	int m_placementId;

public:
	UGCDynamicObjDerivation(long long id, const std::string& templateName, int placementId, int state = PS_CREATED) :
			UGCItemDerivation(UGC_TYPE_DYNAMIC_OBJ | UGC_TYPE_DERIVATIVE, id, templateName, state), m_placementId(placementId) {}

	virtual BYTE* prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles) {
		uploadFiles.clear();
		retBufLen = 0;
		return nullptr;
	}

	virtual void dumpAllTexturesFromEngineObj(IClientEngine* pEngine, TexNameHash_TexNameMaps& allTexturesFound);

	virtual std::string getSubmissionUrl(IClientEngine* pEngine);

protected:
	int getPlacementId() const { return m_placementId; }
};

//
class UGCActionItem : public UGCItemUpload {
private:
	std::string m_scriptPath;
	int m_defaultGlid;
	std::string m_bundleGlids;
	int m_parentGlid; //If this is a derived action item

public:
	UGCActionItem(const std::string& sPath, int dGlid, const std::string& bGlids, int pGlid) :
			UGCItemUpload(UGC_TYPE_ACTION_ITEM, 0, PS_CREATED),
			m_scriptPath(sPath),
			m_defaultGlid(dGlid),
			m_bundleGlids(bGlids),
			m_parentGlid(pGlid) {
	}

	virtual BYTE* prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles);

	virtual std::string getOrigName() const {
		return m_scriptPath;
	}

protected:
	virtual PropertySource* getPropertySource();
};

//
class AppAssetUpload : public UGCItemUpload {
private:
	std::string m_assetPath;

public:
	AppAssetUpload(const std::string& path) :
			UGCItemUpload(UGC_TYPE_APP_EDIT, 0, PS_CREATED), m_assetPath(path) {}

	virtual BYTE* prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles);
	virtual void dumpAllTexturesFromEngineObj(IClientEngine* pEngine, TexNameHash_TexNameMaps& allTexturesFound) {}
	virtual std::string getOrigName() const { return m_assetPath; }
};

// Texture import for pattern browser and image/pattern library
class UGCTextureCustomization : public UGCItemUpload {
private:
	ObjectType m_targetType; // zone object or dynamic object
	int m_targetId; // zone object ID or dynamic object placement ID
	std::string m_texFullPath;
	std::string m_texOrigName;

public:
	UGCTextureCustomization(ObjectType objType, int objId, const char* texPath, const char* texOrig, int state = PS_CREATED);

	virtual std::string getOrigName() const { return m_texOrigName; }

	virtual BYTE* prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles) {
		uploadFiles.clear();
		retBufLen = 0;
		return nullptr;
	}

	virtual void dumpAllTexturesFromEngineObj(IClientEngine* pEngine, TexNameHash_TexNameMaps& allTexturesFound);
};

// UGC animation for dynamic objects, equippables or avatars
class UGCAnimation : public UGCItemUpload_IResource {
private:
	int m_actorType;
	int m_dynamicObjectGLID; // Used when importing animations for ADO, otherwise zero.

public:
	UGCAnimation(int animGLID, int animActorType, int dynamicObjectGLID, IResource* res, int state = PS_CREATED);

	virtual std::string getOrigName() const;

protected:
	virtual PropertySource* getPropertySource();
};

// UGC equippable item
class UGCEquippableItem : public UGCItemUpload_IResource {
private:
	int m_targetActorType;
	std::string m_fileName;
	BoundBox m_boundBoxInActorSpace;

public:
	UGCEquippableItem(int glid, int actorType, const BoundBox& transformedBox, const std::string& fname, IResource* res, int state = PS_CREATED) :
			UGCItemUpload_IResource(res, UGC_TYPE_EQUIPPABLE, glid, state), m_targetActorType(actorType), m_boundBoxInActorSpace(transformedBox), m_fileName(fname) {}

	virtual std::string getOrigName() const { return m_fileName; }
	virtual void dumpAllTexturesFromEngineObj(IClientEngine* pEngine, TexNameHash_TexNameMaps& allTexturesFound);

protected:
	virtual PropertySource* getPropertySource();
};

// UGC particle effect
class UGCParticleEffect : public UGCItemUpload_IResource {
public:
	UGCParticleEffect(int psGLID, IResource* res, int state = PS_CREATED);

	virtual std::string getOrigName() const;
	virtual void dumpAllTexturesFromEngineObj(IClientEngine* pEngine, TexNameHash_TexNameMaps& allTexturesFound);

protected:
	virtual PropertySource* getPropertySource();
};

// UGC sound
class UGCSound : public UGCItemUpload {
private:
	FileSize m_fileSize;
	BYTE* m_rawData;
	std::string m_fileName;
	int m_durationMS;

	static int importIndex;
	static int getNextImportIndex() { return importIndex++; }

public:
	UGCSound(const std::string& fileName) :
			UGCItemUpload(UGC_TYPE_SOUND_OGG, getNextImportIndex(), PS_CREATED),
			m_fileSize(0),
			m_rawData(NULL),
			m_durationMS(0),
			m_fileName(fileName) {}

	UGCSound(BYTE* rawData, FileSize dataSize, const std::string& fileName) :
			UGCItemUpload(UGC_TYPE_SOUND_OGG, getNextImportIndex(), PS_CREATED),
			m_fileSize(dataSize),
			m_rawData(rawData),
			m_durationMS(0),
			m_fileName(fileName) {}

	~UGCSound() {
		if (m_rawData)
			delete m_rawData;
	}

	BYTE* prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles);

	void dumpAllProperties(std::map<int, std::string>& allPropertiesFound) {
		char strDur[12];
		_itoa_s(m_durationMS, strDur, 10);
		allPropertiesFound.clear();
		allPropertiesFound.insert(std::make_pair(PROP_EFFECT_DURATION, strDur));
	}

	std::string getOrigName() const {
		int start = m_fileName.find_last_of("\\");
		std::string origName = m_fileName.substr(start + 1);
		return origName;
	}

	void setDurationMS(int ms) { m_durationMS = ms; }

	void padAndEncrypt();
};

} // namespace KEP