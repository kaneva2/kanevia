///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "UGC_Global.h"
#include "Common/include/IKEPObject.h"
#include "Common/include/KEPConstants.h"
#include <d3d9types.h>
#include "Core/Math/Vector.h"
#include "Core/Util/fast_mutex.h"

namespace KEP {
typedef USHORT EVENT_ID;
class PropertySource;
class UGCImporter;
class CMaterialObject;
class CMovementObj;

// Generic import state holder
class IUGCImportStateObj : public IKEPObject {
public:
	struct ErrorInfo {
		int errorId;
		std::string message;
		int recoveryAction;
		bool suppressDups;
		int dupCount;
	};

	// Asynchronous state
	UGCImportState m_state; // Current state
	std::string m_stateMsg; // Sub-state message
	IGetSet* m_importedObjectGetSet; // IGetSet interface of imported object
	bool m_isCanceling; // Is import being canceled?
	EVENT_ID m_eventId; // Event to send when state transition occurs
	DWORD m_eventObjId; // e->objectId

	// Import specific
	union {
		ObjectType m_targetType;
		int targetType; // For GETSET
	};
	int m_targetId;
	Vector3f m_targetPos;
	bool m_useImportDlg;
	UGCImporter* m_pImporter; // importer used
	std::string m_rootUri;
	int m_importFlags;
	bool m_assetFound;
	int m_localGLID;

	int m_triangleCount;
	int m_textureCount;

	// Validation Specific
	bool m_isValid;
	std::vector<ErrorInfo> m_errorList;

	IUGCImportStateObj() :
			m_state(UGCIS_NONE), m_importedObjectGetSet(NULL), m_isCanceling(false), m_eventId(USHRT_MAX), m_isValid(false), m_targetType(ObjectType::NONE), m_targetId(0), m_targetPos(Vector3f::Zero()), m_useImportDlg(false), m_mutex(), m_pImporter(NULL), m_importFlags(0), m_assetFound(false), m_localGLID(GLID_INVALID), m_triangleCount(-1), m_textureCount(-1) {}

	virtual ~IUGCImportStateObj() {}
	virtual int getImportType() = 0;
	virtual PropertySource* getPropertySource(bool bRefresh) = 0;

	fast_recursive_mutex& getMutex() { return m_mutex; }

	void ReportError(int errorId, const std::string& message, int recoveryAction, bool suppressDups);
	void ClearErrors() { m_errorList.clear(); }
	UINT GetErrorCount() const { return m_errorList.size(); }
	bool GetErrorInfo(UINT index, ErrorInfo& info) {
		if (index >= m_errorList.size())
			return false;
		info = m_errorList[index];
		return true;
	}

private:
	fast_recursive_mutex m_mutex;
};

inline void IUGCImportStateObj::ReportError(int errorId, const std::string& message, int recoveryAction, bool suppressDups) {
	if (suppressDups) {
		for (UINT i = 0; i < m_errorList.size(); i++) {
			if (m_errorList[i].errorId == errorId) {
				m_errorList[i].suppressDups = suppressDups;
				m_errorList[i].dupCount++;
				return;
			}
		}
	}

	m_errorList.push_back(ErrorInfo());
	ErrorInfo& info = m_errorList.back();
	info.errorId = errorId;
	info.message = message;
	info.recoveryAction = recoveryAction;
	info.suppressDups = suppressDups;
	info.dupCount = 1;
}

////////////////////////////////////////////////////
// Miscellaneous import state optional attributes

// Material state
class ISOMaterial {
public:
	std::vector<std::string> m_missingTextures;
	bool m_autoCorrectAmbient;
	bool m_ignoreMissingTextures;

	ISOMaterial() :
			m_autoCorrectAmbient(false), m_ignoreMissingTextures(false) {
	}

	void ProcessMaterial(CMaterialObject& material, bool bFinalize = false);
};

// Scaling
class ISOScalable {
public:
	Vector3f m_boundBoxMin, m_boundBoxMax; // original bounding box
	float m_scaleFactor;
	float m_prevScaleFactor;

	ISOScalable() :
			m_scaleFactor(1.0f), m_prevScaleFactor(1.0f) {
		m_boundBoxMin.x = 0.0f;
		m_boundBoxMin.y = 0.0f;
		m_boundBoxMin.z = 0.0f;
		m_boundBoxMax.x = 0.0f;
		m_boundBoxMax.y = 0.0f;
		m_boundBoxMax.z = 0.0f;
	}
};

#define ACTOR_TYPE_NONE 0
#define ACTOR_TYPE_MALE 1
#define ACTOR_TYPE_FEMALE 2

#define TARGET_NET_ID_ME -1

// Import states related to an owner avatar
class ISOPlayerItem {
public:
	int m_targetEntityNetworkID;
	bool m_aiMounted;
	int m_targetActorType;
	CMovementObj* m_pTargetMovObj;

	ISOPlayerItem() :
			m_targetEntityNetworkID(TARGET_NET_ID_ME),
			m_pTargetMovObj(nullptr),
			m_aiMounted(false),
			m_targetActorType(ACTOR_TYPE_NONE) {}
};

} // namespace KEP