///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AnimationImport.h"
#include "../UGCImportStateObserver.h"
#include "GetSet.h"
#include "Common/include/IAssetsDatabase.h"
#include "../UGCManager.h"
#include "../UGCItemUploadState.h"
#include "AssetTree.h"
#include "RuntimeSkeleton.h"
#include "CSkeletonObject.h"
#include "CMovementObj.h"
#include "CSkeletonAnimation.h"
#include "CBoneAnimationClass.h"
#include "CBoneClass.h"
#include "ResourceManagers/SkeletalAnimationManager.h"
#include "ResourceManagers/AnimationProxy.h"
#include "Common/include/KEPConstants.h"
#include "Engine/clientblades/dynamicObPlacementModelProxy.h"
#include "DynamicObj.h"
#include "..\UGCProperties.h"

namespace KEP {

class ClassifyAssetNode_Skeletal : public IClassifyAssetNode {
private:
	RuntimeSkeleton* referenceSkeleton;

public:
	ClassifyAssetNode_Skeletal(RuntimeSkeleton* ske) :
			referenceSkeleton(ske) {}

	virtual KEP_ASSET_TYPE operator()(const std::string& nodeName, int typeHint, bool hasGeom) {
		if (referenceSkeleton != nullptr && referenceSkeleton->getBoneList() != nullptr) { // If name matches one of the bones in the reference skeleton, consider this node as a joint
			int tmp;
			if (referenceSkeleton->getBoneList()->getBoneObjectByName(nodeName, &tmp) != NULL) {
				return KEP_SKA_SKELETAL_OBJECT;
			}

			if (nodeName.find('_') != std::string::npos) {
				// Try converting understores to spaces before matching
				CStringA newName = nodeName.c_str();
				newName.Replace(' ', '_');
				if (referenceSkeleton->getBoneList()->getBoneObjectByName((const char*)newName, &tmp, true) != NULL) {
					return KEP_SKA_SKELETAL_OBJECT;
				}
			};
		}

		if (typeHint == KEP_SKA_RIGID_MESH && !hasGeom) {
			return KEP_SKA_SKELETAL_OBJECT;
		}

		return (KEP_ASSET_TYPE)typeHint;
	}

	virtual bool isDummy(const std::string& nodeName) {
		return STLBeginWithIgnoreCase(nodeName, "nub__") || STLEndWithIgnoreCase(nodeName, "_PIVOT");
	}
};

bool IsSkeletonRoot(const std::string& nodeName) {
	// Distinguish "real" skeleton root from orphan bones like "bone - L mount"
	// Before calling this function, make sure the node in question must not have a parent in COLLADA node hierarchy.
	return STLCompareIgnoreCase(nodeName, "Bip01") == 0 || STLBeginWithIgnoreCase(nodeName, "Root_");
}

class CharAnimImportState : public IUGCImportStateObj, public ISOPlayerItem, public GetSet {
public:
	RuntimeSkeleton* m_pTargetSkeleton; // the thing that the animation is being dragged onto (MO or DO)
	int m_targetGLID;
	CSkeletonObject* m_pSkeleton; // what's being imported has to contain a skeleton?
	bool m_skeletonMismatched;
	std::string m_animUri;
	int m_animDuration; // duration in ms
	int m_currActorType;
	float m_speedScale;
	int m_cropStart, m_cropEnd;
	Vector3f m_currOffset; // Current animation translation offset
	Vector3f m_targetOffset; // Target animation translation offset
	Vector3f m_defaultOffset; // Default animation translation offset - shift back to origin
	AssetData m_assetDef;
	bool m_looping;

	struct ConversionCacheItem // conversion cache
	{
		int glid;
		Vector3f offset;
		int actorType;
	};

	std::vector<ConversionCacheItem> conversionCache;

	CharAnimImportState() :
			m_pTargetSkeleton(NULL),
			m_targetGLID(0),
			m_pSkeleton(NULL),
			m_skeletonMismatched(false),
			m_animDuration(0),
			m_currActorType(ACTOR_TYPE_NONE),
			m_speedScale(1.0f),
			m_cropStart(0),
			m_cropEnd(-1),
			m_looping(true) {
		m_currOffset.x = m_currOffset.y = m_currOffset.z = 0.0f;
		m_targetOffset.x = m_targetOffset.y = m_targetOffset.z = 0.0f;
	}

	virtual ~CharAnimImportState() {
		if (m_pSkeleton != nullptr) {
			delete m_pSkeleton;
		}
		m_pSkeleton = nullptr;
	}

	virtual IGetSet* Values() { return this; }
	virtual int getImportType() { return UGC_TYPE_ANIMATION; }
	virtual PropertySource* getPropertySource(bool bRefresh) {
		assert(false);
		return NULL;
	}

	void addToCache() {
		ConversionCacheItem item;
		item.glid = m_localGLID;
		item.actorType = m_currActorType;
		item.offset = m_currOffset;
		conversionCache.push_back(item);
	}

	int getFromCache() {
		for (auto it = conversionCache.begin(); it != conversionCache.end(); ++it) {
			if (it->actorType == m_targetActorType &&
				abs(it->offset.x - m_targetOffset.x) < 1E-6 &&
				abs(it->offset.y - m_targetOffset.y) < 1E-6 &&
				abs(it->offset.z - m_targetOffset.z) < 1E-6) {
				return it->glid;
			}
		}

		return GLID_INVALID;
	}

	DECLARE_GETSET
};

IUGCImportStateObj* AnimationImport::createState() {
	return new CharAnimImportState();
}

void AnimationImport::disposeState(IUGCImportStateObj* state) {
	CharAnimImportState* pStateObj = dynamic_cast<CharAnimImportState*>(state);
	assert(pStateObj != NULL);
	pStateObj;

	if (state) {
		delete state;
	}
}

bool AnimationImport::preview(IUGCImportStateObj* _pStateObj, UGCImportStateObserver* pObserver) {
	CharAnimImportState* pStateObj = dynamic_cast<CharAnimImportState*>(_pStateObj);
	assert(pStateObj != nullptr);
	if (!pStateObj) {
		return false;
	}

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	bool res = false;

	std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());

	assert(pStateObj->m_state == UGCIS_PREVIEWING);
	if (pStateObj->m_state == UGCIS_PREVIEWING) {
		// Get target skeleton
		pStateObj->m_pTargetMovObj = NULL;

		// find out if the target was a DO or a MO
		// and set the target skeleton.
		if (pStateObj->m_targetType == ObjectType::DYNAMIC) {
			CDynamicPlacementObj* pDPO = pICE->GetDynamicObject(pStateObj->m_targetId);
			pStateObj->m_pTargetSkeleton = pDPO->getSkeleton();
			pStateObj->m_targetGLID = pDPO->GetGlobalId();
		} else if (pStateObj->m_targetType == ObjectType::EQUIPPABLE) {
			RuntimeSkeleton* pSkeleton = pICE->ObjectGetRuntimeSkeleton(pStateObj->m_targetType, pStateObj->m_targetId);
			pStateObj->m_pTargetSkeleton = pSkeleton;
			pStateObj->m_targetGLID = pStateObj->m_targetId;
		} else if (pStateObj->m_targetType == ObjectType::MOVEMENT) {
			// For now assume if you didn't drag it on a DO you meant it to be a char anim
			if (pStateObj->m_targetEntityNetworkID == TARGET_NET_ID_ME)
				pStateObj->m_pTargetMovObj = dynamic_cast<CMovementObj*>(pICE->GetMovementObjectMeAsGetSet());
			else
				pStateObj->m_pTargetMovObj = dynamic_cast<CMovementObj*>(pICE->GetMovementObjectByNetIdAsGetSet(pStateObj->m_targetEntityNetworkID));
			pStateObj->m_pTargetSkeleton = pStateObj->m_pTargetMovObj->getSkeleton();
		}

		std::auto_ptr<ClassifyAssetNode_Skeletal> apClassifyFunc(new ClassifyAssetNode_Skeletal(pStateObj->m_pTargetSkeleton));

		AssetTree* pAssets = IAssetsDatabase::Instance()->OpenAndPreviewURI(pStateObj->m_rootUri, KEP_SKA_ANIMATION, IMP_GENERAL_NOUI, apClassifyFunc.get());
		if (pAssets) {
			AssetTree* pAnimGroup = NULL;
			AssetTree* pSkeletonGroup = NULL;

			// Check if animation found
			pAnimGroup = AssetData::GetAssetGroup(pAssets, KEP_SKA_ANIMATION);
			if (pAnimGroup) {
				if (pAnimGroup->size() > 0) {
					pStateObj->m_assetDef = *pAnimGroup->begin();
					pStateObj->m_animUri = pAnimGroup->begin()->GetURI();
					pSkeletonGroup = AssetData::GetAssetGroup(pAssets, KEP_SKA_SKELETAL_OBJECT);
				}
			}

			// Verify skeleton
			if (pSkeletonGroup) {
				// First skeleton
				AssetTree::iterator itSkeleton = pSkeletonGroup->begin();
				if (itSkeleton != pSkeletonGroup->end()) {
					itSkeleton->SetFlag(AssetData::STA_SELECTED); // Mark as selected
				}

				pStateObj->m_pSkeleton = IAssetsDatabase::Instance()->ImportSkeletalObjectByAssetTree(pStateObj->m_rootUri, IMP_GENERAL_NOUI | IMP_SKELETAL_NOMESH | IMP_SKELETAL_NOANIM, pAssets);

				if (pStateObj->m_pSkeleton != NULL) {
					if (pStateObj->m_pTargetSkeleton) {
						if (IAssetsDatabase::Instance()->CompareSkeletons(*pStateObj->m_pSkeleton->getRuntimeSkeleton(), *pStateObj->m_pTargetSkeleton) == 1) {
							// Preview succeeded
							res = true;
						} else {
							// Skeleton mismatched
							pStateObj->m_skeletonMismatched = true;
						}
					}
				}
			}

			if (!res) {
				pStateObj->ReportError(UGCIE_NO_SKELETON, "Unable to find a matching skeleton in the import file.", RA_NORECOVERY, true);
			}

			// Dispose asset tree
			delete pAssets;
		}
	} else {
		// Invalid state
	}

	// Update import state
	if (pObserver != NULL) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool AnimationImport::import(IUGCImportStateObj* _pStateObj, UGCImportStateObserver* pObserver) {
	CharAnimImportState* pStateObj = dynamic_cast<CharAnimImportState*>(_pStateObj);
	assert(pStateObj != nullptr);
	if (!pStateObj) {
		return false;
	}

	bool res = true;

	std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());

	assert(pStateObj->m_state == UGCIS_IMPORTING);
	if (pStateObj->m_state == UGCIS_IMPORTING) {
		// Import animation
		//
		// Should I be using a tmp CSkeleton for this?
		// If so, should I point it at the existing pStateObk->skeleton, instead
		// of a temp runtime skeleton?
		//
		// Does the old runtime skeleton need to be deleted?
		//
		pStateObj->m_localGLID = GLID_LOCAL_BASE;
		auto pSO_new = new CSkeletonObject(pStateObj->m_pTargetSkeleton->shared_from_this(), nullptr);

		res = IAssetsDatabase::Instance()->ImportAnimationByAssetDefinition(*pSO_new,
			pStateObj->m_rootUri, pStateObj->m_localGLID, SKELETAL_REPLACE_ANIMATION | IMP_GENERAL_NOUI, pStateObj->m_assetDef);

		// should already be the same runtime skeleton in both cases
		pSO_new->detach();
		delete pSO_new;
		pSO_new = nullptr;

		if (res) {
			auto pAnim = AnimationRM::Instance()->GetSkeletonAnimationByGlid(pStateObj->m_localGLID);
			assert(pAnim != nullptr);
			if (pAnim == nullptr) {
				return false;
			}
			pStateObj->m_localGLID = pAnim->m_animGlid;
			pStateObj->m_animDuration = pAnim->m_animDurationMs;

			if (pStateObj->m_pTargetMovObj)
				pStateObj->m_targetActorType = pStateObj->m_pTargetMovObj->getActorGroupByIndex(0); // is there a primary actor group? hard-coded for now
			else
				pStateObj->m_targetActorType = ACTOR_TYPE_NONE; // is this ok?

			pStateObj->m_currActorType = pStateObj->m_targetActorType;
			pStateObj->addToCache();

			RuntimeSkeleton* pRefSkeleton = pStateObj->m_pTargetSkeleton;

			// Get default root offset
			for (UINT boneLoop = 0; boneLoop < pRefSkeleton->getBoneCount(); boneLoop++) {
				CBoneObject* pBone = pRefSkeleton->getBoneByIndex(boneLoop);
				CStringA boneName = pBone->m_boneName.c_str();
				boneName.Replace(' ', '_');
				if (IsSkeletonRoot((const char*)boneName)) {
					//read root offset from first frame
					int mappedIndex = boneLoop;
					if (pAnim->m_skeletonDef) {
						mappedIndex = pAnim->m_skeletonDef->GetBoneIndexByName((const char*)boneName, true, false);
					}
					if (mappedIndex != -1) {
						CBoneAnimationObject* pBAO = pAnim->m_ppBoneAnimationObjs[mappedIndex];
						if (pBAO->m_animKeyFrames > 0) {
							Vector3f ofs = pAnim->m_ppBoneAnimationObjs[mappedIndex]->m_pAnimKeyFrames[0].pos;
							ofs -= pBone->m_startPosition.GetTranslation();

							pStateObj->m_defaultOffset = ofs;

							// take default offset from first root bone for now
							// need a better solution for multi-root skeleton (not currently applicable)
							break;
						}
					}
				}
			}
		}
	}

	if (pObserver != NULL) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool AnimationImport::convert(IUGCImportStateObj* _pStateObj, UGCImportStateObserver* pObserver) {
	CharAnimImportState* pStateObj = dynamic_cast<CharAnimImportState*>(_pStateObj);
	assert(pStateObj != nullptr);
	if (!pStateObj) {
		return false;
	}

	bool res = true;

	std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());

	assert(pStateObj->m_state == UGCIS_CONVERTING);
	if (pStateObj->m_state == UGCIS_CONVERTING) {
		// Check if we need conversion
		if (pStateObj->m_targetActorType != pStateObj->m_currActorType && pStateObj->m_targetActorType != ACTOR_TYPE_NONE ||
			pStateObj->m_currOffset.x != pStateObj->m_targetOffset.x ||
			pStateObj->m_currOffset.y != pStateObj->m_targetOffset.y ||
			pStateObj->m_currOffset.z != pStateObj->m_targetOffset.z) {
			// Get reference skeleton
			RuntimeSkeleton* pRefSkeleton = pStateObj->m_pTargetSkeleton;
			if (pStateObj->m_pTargetMovObj && pStateObj->m_targetActorType != pStateObj->m_pTargetMovObj->getActorGroupByIndex(0)) {
				// check if AIBot is mounted
				assert(pStateObj->m_pTargetMovObj->getMountType() == MT_STUNT && pStateObj->m_pTargetMovObj->getPosses());
				if (pStateObj->m_pTargetMovObj->getMountType() == MT_STUNT && pStateObj->m_pTargetMovObj->getPosses()) {
					pRefSkeleton = pStateObj->m_pTargetMovObj->getPosses()->getSkeleton();
				}
			}

			// Check if already converted
			int glid = pStateObj->getFromCache();
			bool bCached = false;
			if (!IS_VALID_GLID(glid)) {
				AnimImportOptions options;
				ImpFloatVector3 offsetVector(pStateObj->m_targetOffset.x, pStateObj->m_targetOffset.y, pStateObj->m_targetOffset.z);

				// Prepare root offset table
				for (UINT boneLoop = 0; boneLoop < pRefSkeleton->getBoneCount(); boneLoop++) {
					CBoneObject* pBone = pRefSkeleton->getBoneByIndex(boneLoop);
					std::string boneName = pBone->m_boneName;
					if (IsSkeletonRoot(boneName)) {
						options.rootOffsets.insert(make_pair(boneName, offsetVector));
					}
				}

				// Convert animation
				std::shared_ptr<RuntimeSkeleton> pRefSkeletonShared = pRefSkeleton->shared_from_this();
				CSkeletonObject* tmpSkel = new CSkeletonObject(pRefSkeletonShared, nullptr);

				pStateObj->m_localGLID = GLID_LOCAL_BASE;
				res = IAssetsDatabase::Instance()->ImportAnimationByAssetDefinition(*tmpSkel,
					pStateObj->m_rootUri, pStateObj->m_localGLID, SKELETAL_REPLACE_ANIMATION | IMP_GENERAL_NOUI, pStateObj->m_assetDef, &options);

				tmpSkel->detach();
				delete tmpSkel;
			} else {
				// already converted
				bCached = true;
				pStateObj->m_localGLID = glid;
				pRefSkeleton->queueAnimForLoad(glid);
			}

			if (res) {
				auto pAnim = AnimationRM::Instance()->GetSkeletonAnimationByGlid(pStateObj->m_localGLID);
				assert(pAnim != nullptr);
				if (pAnim == nullptr) {
					return false;
				}

				pStateObj->m_localGLID = pAnim->m_animGlid;
				pStateObj->m_currOffset = pStateObj->m_targetOffset;
				pStateObj->m_currActorType = pStateObj->m_targetActorType;
				if (!bCached) {
					pStateObj->addToCache();
				}
			}
		}
	}

	if (pObserver != NULL) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool AnimationImport::process(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	return doNothing(pStateObj, pObserver, UGCIS_PROCESSING);
}

bool AnimationImport::commit(IUGCImportStateObj* _pStateObj, UGCImportStateObserver* pObserver) {
	CharAnimImportState* pStateObj = dynamic_cast<CharAnimImportState*>(_pStateObj);
	assert(pStateObj != nullptr);
	if (!pStateObj) {
		return false;
	}

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());

	bool res = false;

	assert(pStateObj->m_state == UGCIS_COMMITTING);
	if (pStateObj->m_state == UGCIS_COMMITTING) {
		auto pAnim = AnimationRM::Instance()->GetAnimationProxy(pStateObj->m_localGLID);
		assert(pAnim != nullptr);
		if (pAnim) {
			pICE->GetUGCManager()->AddUGCObj(new UGCAnimation(pStateObj->m_localGLID, pStateObj->m_targetActorType, pStateObj->m_targetGLID, pAnim));
			res = true;
		}
	}

	if (pObserver) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool AnimationImport::cleanup(IUGCImportStateObj* _pStateObj, UGCImportStateObserver* pObserver) {
	CharAnimImportState* pStateObj = dynamic_cast<CharAnimImportState*>(_pStateObj);
	assert(pStateObj != nullptr);
	if (!pStateObj) {
		return false;
	}

	try {
		{
			std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());
			IAssetsDatabase::Instance()->CloseURI(pStateObj->m_rootUri, KEP_STATIC_MESH, IMP_GENERAL_NOUI);

			if (pObserver) {
				pObserver->doUpdate(pStateObj, EV_DESTROYED);
			}
		}

		disposeState(pStateObj);
	} catch (CException*) {
		assert(false);
		return false;
	}

	return true;
}

BEGIN_GETSET_IMPL(CharAnimImportState, IDS_CHARANIMIMPORTSTATE_NAME)
GETSET(m_state, gsInt, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, STATE)
GETSET(m_stateMsg, gsString, GS_FLAG_EXPOSE, 0, 0, UGCIMPORTSTATE, STATEMSG)
GETSET_OBJ_PTR(m_importedObjectGetSet, GS_FLAG_EXPOSE, 0, 0, UGCIMPORTSTATE, IMPORTEDOBJECTGETSET, IGetSet)
GETSET(m_isCanceling, gsBool, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, ISCANCELING)
GETSET(m_eventId, gsShort, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, EVENTID)
GETSET(m_eventObjId, gsDword, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, EVENTOBJID)
GETSET(m_targetType, gsInt, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETTYPE)
GETSET(m_targetId, gsInt, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETID)
GETSET(m_targetPos.x, gsFloat, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETX)
GETSET(m_targetPos.y, gsFloat, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETY)
GETSET(m_targetPos.z, gsFloat, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETZ)
GETSET(m_useImportDlg, gsBool, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, USEIMPORTDLG)
GETSET(m_isValid, gsBool, GS_FLAG_EXPOSE, 0, 0, UGCIMPORTSTATE, ISVALID)
GETSET2(m_rootUri, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCIMPORTSTATE_ROOTURI, IDS_UGCIMPORTSTATE_ROOTURI)
GETSET2(m_importFlags, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCIMPORTSTATE_IMPORTFLAGS, IDS_UGCIMPORTSTATE_IMPORTFLAGS)
GETSET2(m_localGLID, gsInt, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_UGCIMPORTSTATE_LOCALGLID, IDS_UGCIMPORTSTATE_LOCALGLID)
GETSET2(m_targetEntityNetworkID, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCPLAYERITEMIMPORTSTATE_TARGETENTITYNETWORKID, IDS_UGCPLAYERITEMIMPORTSTATE_TARGETENTITYNETWORKID)
GETSET2(m_aiMounted, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCPLAYERITEMIMPORTSTATE_AIMOUNTED, IDS_UGCPLAYERITEMIMPORTSTATE_AIMOUNTED)
GETSET2(m_targetActorType, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCPLAYERITEMIMPORTSTATE_ACTORTYPE, IDS_UGCPLAYERITEMIMPORTSTATE_ACTORTYPE)
GETSET2(m_pSkeleton, gsObjectPtr, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_CHARANIMIMPORTSTATE_SKELETON, IDS_CHARANIMIMPORTSTATE_SKELETON)
GETSET2(m_skeletonMismatched, gsBool, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_CHARANIMIMPORTSTATE_SKELETONMISMATCHED, IDS_CHARANIMIMPORTSTATE_SKELETONMISMATCHED)
GETSET2(m_animUri, gsString, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_CHARANIMIMPORTSTATE_ANIMURI, IDS_CHARANIMIMPORTSTATE_ANIMURI)
GETSET2(m_animDuration, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_CHARANIMIMPORTSTATE_ANIMDURATION, IDS_CHARANIMIMPORTSTATE_ANIMDURATION)
GETSET2(m_speedScale, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_CHARANIMIMPORTSTATE_SPEEDSCALE, IDS_CHARANIMIMPORTSTATE_SPEEDSCALE)
GETSET2(m_cropStart, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_CHARANIMIMPORTSTATE_CROPSTART, IDS_CHARANIMIMPORTSTATE_CROPSTART)
GETSET2(m_cropEnd, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_CHARANIMIMPORTSTATE_CROPEND, IDS_CHARANIMIMPORTSTATE_CROPEND)
GETSET2(m_targetOffset, gsD3dvector, GS_FLAGS_DEFAULT, 0, 0, IDS_CHARANIMIMPORTSTATE_OFFSET, IDS_CHARANIMIMPORTSTATE_OFFSET)
GETSET2(m_defaultOffset, gsD3dvector, GS_FLAGS_DEFAULT, 0, 0, IDS_CHARANIMIMPORTSTATE_DEFAULTOFFSET, IDS_CHARANIMIMPORTSTATE_DEFAULTOFFSET)
GETSET2(m_looping, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_CHARANIMIMPORTSTATE_LOOPING, IDS_CHARANIMIMPORTSTATE_LOOPING)
END_GETSET_IMPL

} // namespace KEP