///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "AnimationProperties.h"
#include "ResourceManagers/AnimationProxy.h"
#include "../UGC_Global.h"

namespace KEP {

AnimationProperties::AnimationProperties(const AnimationProxy* pAnimProxy) :
		durationMs(0.0),
		frameCount(-1), // undermined
		boneCount(0),
		actorType(0), // TBD
		actorGLID(0) // Used by ADO to pass the DO GLID.
{
	ASSERT(pAnimProxy != NULL && pAnimProxy->HasAnimData());
	if (pAnimProxy && pAnimProxy->HasAnimData()) {
		const AnimationProxy::AnimationData* pAnimData = pAnimProxy->GetAnimDataPtr();
		durationMs = pAnimData->m_animDurationMs;
		boneCount = pAnimData->m_numBones;
	}
}

VLD_VALUE_TYPE AnimationProperties::getType(int propId) const {
	switch (propId) {
		case PROP_EFFECT_DURATION:
		case PROP_EFFECT_FRAME_COUNT:
		case PROP_SKELETON_BONE_COUNT:
		case PROP_TARGET_ACTOR_TYPE:
		case PROP_TARGET_ACTOR_GLID:
			return VVT_INT;
	}

	return VVT_UNDEFINED;
}

Value AnimationProperties::get(int propId) const {
	switch (propId) {
		case PROP_EFFECT_DURATION:
			return Value((float)durationMs);
		case PROP_EFFECT_FRAME_COUNT:
			return Value(frameCount);
		case PROP_SKELETON_BONE_COUNT:
			return Value(boneCount);
		case PROP_TARGET_ACTOR_TYPE:
			return Value(actorType);
		case PROP_TARGET_ACTOR_GLID:
			return Value(actorGLID);
	}

	return Value(VVT_ERROR);
}

void AnimationProperties::getAllSupportedPropertyIds(std::vector<int>& allPropIds) {
	allPropIds.clear();
	allPropIds.push_back(PROP_EFFECT_DURATION);
	allPropIds.push_back(PROP_EFFECT_FRAME_COUNT);
	allPropIds.push_back(PROP_SKELETON_BONE_COUNT);
	allPropIds.push_back(PROP_TARGET_ACTOR_TYPE);
	allPropIds.push_back(PROP_TARGET_ACTOR_GLID);
}

} // namespace KEP