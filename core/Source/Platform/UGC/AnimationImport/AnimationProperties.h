///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include "../UGCProperties.h"

namespace KEP {

class AnimationProxy;

class AnimationProperties : public PropertySource {
public:
	AnimationProperties(const AnimationProxy* pAnimProxy);

	VLD_VALUE_TYPE getType(int propId) const;
	Value get(int propId) const;

	void getAllSupportedPropertyIds(std::vector<int>& allPropIds);

	// allow actorType overriden as a special case -- currently we don't have such info from AnimationProxy
	void setActorType(int type) { actorType = type; }
	void setActorGLID(int glid) { actorGLID = glid; }

protected:
	TimeMs durationMs;
	int frameCount;
	int boneCount;
	int actorType;
	int actorGLID; // used to pass DO GLID when animation assigned to ADO
};

} // namespace KEP