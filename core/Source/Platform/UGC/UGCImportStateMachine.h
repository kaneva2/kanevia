///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "UGCImport.h"
#include "UGCImportStateObserver.h"
#include "jsThread.h"
#include "jsSemaphore.h"
#include <list>

namespace KEP {

class IDispatcher;
class IUGCImportStateObj;

class UGCImportStateMachine : public UGCImportStateObserver {
public:
	UGCImportStateMachine(IDispatcher* pDispatcher);
	void doUpdate(IUGCImportStateObj* pStateObj, UGCImportEventType eventType);

	static EVENT_PROC_RC UGCImportEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);

protected:
	IDispatcher* m_pDispatcher;

protected:
	void handleEvent(IUGCImportStateObj* pStateObj, UGCImportEventType eventType);
	void changeState(IUGCImportStateObj* pStateObj, UGCImportState newState);
};

} // namespace KEP