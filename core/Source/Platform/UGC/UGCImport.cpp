///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UGCImport.h"
#include <jsThread.h>
#include "DynamicObjectImport/DynamicObjectImport.h"
#include "TextureImport/UGCTextureImport.h"
#include "AnimationImport/AnimationImport.h"
#include "AppAssetImport/AppAssetImport.h"
#include "EquippableObjectImport/EquippableObjectImport.h"
#include "UGCImportStateMachine.h"
#include "UGCProperties.h"
#include "KEPHelpers.h"
#include "Event/Events/UGCImportEvent.h"

#include <future>

namespace KEP {

static std::vector<UGCImportSTTEntry> fsm_v1 = {
	{ UGCIS_NONE, EV_START, UGCIS_STARTING, UGCImportAction::Init },
	{ UGCIS_STARTING, EV_NONE, UGCIS_PREVIEWING, UGCImportAction::Preview },
	{ UGCIS_PREVIEWING, EV_OP_DONE, UGCIS_PREVIEWED, UGCImportAction::None },
	{ UGCIS_PREVIEWING, EV_OP_FAIL, UGCIS_PREVIEWFAILED, UGCImportAction::None },
	{ UGCIS_PREVIEWED, EV_CONT, UGCIS_IMPORTING, UGCImportAction::Import },
	{ UGCIS_IMPORTING, EV_OP_DONE, UGCIS_IMPORTED, UGCImportAction::None },
	{ UGCIS_IMPORTING, EV_OP_FAIL, UGCIS_IMPORTFAILED, UGCImportAction::None },
	{ UGCIS_IMPORTED, EV_CONT, UGCIS_CONVERTING, UGCImportAction::Convert },
	{ UGCIS_CONVERTING, EV_OP_DONE, UGCIS_CONVERTED, UGCImportAction::None },
	{ UGCIS_CONVERTING, EV_OP_FAIL, UGCIS_CONVERSIONFAILED, UGCImportAction::None },
	{ UGCIS_CONVERTED, EV_CONT, UGCIS_PROCESSING, UGCImportAction::Process },
	{ UGCIS_CONVERTED, EV_CONVERT, UGCIS_CONVERTING, UGCImportAction::Convert },
	{ UGCIS_PROCESSING, EV_OP_DONE, UGCIS_COMMITTING, UGCImportAction::Commit },
	{ UGCIS_COMMITTING, EV_OP_DONE, UGCIS_PROCESSED, UGCImportAction::None },
	{ UGCIS_PROCESSING, EV_OP_FAIL, UGCIS_PROCESSINGFAILED, UGCImportAction::None },
	{ UGCIS_COMMITTING, EV_OP_FAIL, UGCIS_PROCESSINGFAILED, UGCImportAction::None },
	{ UGCIS_UPLOADING, EV_OP_DONE, UGCIS_UPLOADED, UGCImportAction::None },
	{ UGCIS_UPLOADING, EV_OP_FAIL, UGCIS_UPLOADFAILED, UGCImportAction::None },
	{ UGCIS_ANY, EV_DESTROY, UGCIS_DESTROYING, UGCImportAction::Cleanup },
	{ UGCIS_DESTROYING, EV_OP_DONE, UGCIS_DESTROYED, UGCImportAction::None },
	{ UGCIS_ANY, EV_CANCEL, UGCIS_CANCELED, UGCImportAction::None },
};

IUGCImportStateObj* UGCImporter::beginSession() {
	VERIFY_RETURN(m_pImporterImpl != NULL, NULL);
	IUGCImportStateObj* pStateObj = m_pImporterImpl->createState();
	pStateObj->m_pImporter = this;
	return pStateObj;
}

void UGCImporter::endSession(IUGCImportStateObj* state) {
	VERIFY_RETVOID(m_pImporterImpl != NULL);
	m_pImporterImpl->disposeState(state);
}

void UGCImporter::performAction(UGCImportAction action, IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	jsAssert(pObserver != nullptr);

	switch (action) {
		case UGCImportAction::None:
		case UGCImportAction::Init:
			// Do nothing
			break;

		case UGCImportAction::Preview:
			std::thread(&IUGCImport::preview, m_pImporterImpl, pStateObj, pObserver).detach();
			break;

		case UGCImportAction::Import:
			std::thread(&IUGCImport::import, m_pImporterImpl, pStateObj, pObserver).detach();
			break;

		case UGCImportAction::Convert:
			std::thread(&IUGCImport::convert, m_pImporterImpl, pStateObj, pObserver).detach();
			break;

		case UGCImportAction::Process:
			std::thread(&IUGCImport::process, m_pImporterImpl, pStateObj, pObserver).detach();
			break;

		case UGCImportAction::Commit:
			m_pImporterImpl->commit(pStateObj, pObserver);
			break;

		case UGCImportAction::Cleanup:
			std::thread(&IUGCImport::cleanup, m_pImporterImpl, pStateObj, pObserver).detach();
			break;

		default:
			assert(false);
			break;
	}
}

const UGCImportSTTEntry& UGCImporter::getStateTransitionTableEntry(size_t entryId) {
	static UGCImportSTTEntry dummy;
	assert(entryId < m_stateTransitionTable.size());
	if (entryId < m_stateTransitionTable.size()) {
		return m_stateTransitionTable[entryId];
	}
	return dummy;
}

class UGCImporterFactory : public IUGCImporterFactory {
public:
	UGCImporterFactory();
	~UGCImporterFactory();
	UGCImporter* getImporter(int ugcType);

private:
	std::map<int, UGCImporter*> m_importers;
};

UGCImporterFactory::UGCImporterFactory() {
}

UGCImporterFactory::~UGCImporterFactory() {
	for (auto it = m_importers.begin(); it != m_importers.end(); ++it) {
		UGCImporter* pImp = it->second;
		if (pImp) {
			delete pImp;
		}
	}

	m_importers.clear();
}

UGCImporter* UGCImporterFactory::getImporter(int ugcType) {
	auto findIt = m_importers.find(ugcType);
	if (findIt != m_importers.end()) {
		return findIt->second;
	}

	UGCImporter* pImporter = NULL;

	switch (ugcType) {
		case UGC_TYPE_DYNAMIC_OBJ:
			pImporter = new UGCImporter(new DynamicObjectImport(), fsm_v1);
			break;

		case UGC_TYPE_TEXTURE:
			pImporter = new UGCImporter(new UGCTextureImport(), fsm_v1);
			break;

		case UGC_TYPE_ANIMATION:
			pImporter = new UGCImporter(new AnimationImport(), fsm_v1);
			break;

		case UGC_TYPE_EQUIPPABLE:
			pImporter = new UGCImporter(new EquippableObjectImport(), fsm_v1);
			break;

		case UGC_TYPE_APP_EDIT:
			pImporter = new UGCImporter(new AppAssetImport(), fsm_v1);
			break;
	}

	if (pImporter) {
		m_importers[ugcType] = pImporter;
	}

	return pImporter;
}

static UGCImporterFactory* factory = NULL;
static UGCImportStateMachine* stateMachine = NULL;

void InitUGCImportModule(IDispatcher* apDispatcher) {
	factory = new UGCImporterFactory();
	stateMachine = new UGCImportStateMachine(apDispatcher);

	RegisterEvent<UGCImportEvent>(*apDispatcher);
	auto eventId = apDispatcher->GetEventId("UGCImportEvent");
	jsAssert(eventId != BAD_EVENT_ID);
	if (eventId != BAD_EVENT_ID) {
		apDispatcher->RegisterHandler(UGCImportStateMachine::UGCImportEventHandler, (ULONG)stateMachine, "UGCImportEvent", PackVersion(1, 0, 0, 0), eventId);
	}
}

void CleanupUGCImportModule() {
	if (factory) {
		delete factory;
		factory = NULL;
	}

	if (stateMachine) {
		delete stateMachine;
		stateMachine = NULL;
	}
}

IUGCImporterFactory* GetImporterFactory() {
	assert(factory != NULL); // call InitUGCImportModule first
	return factory;
}

UGCImportStateObserver* GetImportStateMachine() {
	assert(stateMachine != NULL);
	return stateMachine;
}

bool IUGCImport::doNothing(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver, UGCImportState expectedState) {
	std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());

	bool res = false;

	assert(pStateObj->m_state == expectedState);
	if (pStateObj->m_state == expectedState) {
		// do nothing
		res = true;
	}

	if (pObserver != NULL) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

} // namespace KEP