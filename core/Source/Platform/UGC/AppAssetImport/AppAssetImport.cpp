///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AppAssetImport.h"
#include "../UGCImportStateObj.h"
#include "../UGCImportStateObserver.h"
#include "GetSet.h"
#include "../UGCManager.h"
#include "../UGCItemUploadState.h"

namespace KEP {

class AppEditingImportState : public IUGCImportStateObj, public GetSet {
public:
	std::string m_assetPath;
	int m_type;

	AppEditingImportState() :
			m_assetPath(""), m_type(0) {} //ScriptServerEvent::UndefinedAssetType

	virtual IGetSet* Values() { return this; }
	virtual int getImportType() { return UGC_TYPE_APP_EDIT; }
	virtual PropertySource* getPropertySource(bool bRefresh) {
		ASSERT(FALSE);
		return NULL;
	}

	DECLARE_GETSET
};

IUGCImportStateObj* AppAssetImport::createState() {
	return new AppEditingImportState();
}

void AppAssetImport::disposeState(IUGCImportStateObj* state) {
	if (state)
		delete state;
}

bool AppAssetImport::preview(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	return doNothing(pStateObj, pObserver, UGCIS_PREVIEWING);
}

bool AppAssetImport::import(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	AppEditingImportState* pAEImpState = dynamic_cast<AppEditingImportState*>(pStateObj);
	VERIFY_RETURN(pAEImpState != NULL, false);

	bool res = false;
	std::string errMsg;

	std::lock_guard<fast_recursive_mutex> lock(pAEImpState->getMutex());

	ASSERT(pAEImpState->m_state == UGCIS_IMPORTING);
	if (pAEImpState->m_state == UGCIS_IMPORTING) {
		ASSERT(!pAEImpState->m_assetPath.empty()); // source file name must have been provided
		if (pAEImpState->m_assetPath.empty()) {
			errMsg = "Internal error: file name not specified for import";
		} else {
			res = true;
		}
	} else {
		ASSERT(FALSE);
		// Invalid state
	}

	// Update import state message
	if (!res) {
		pAEImpState->m_stateMsg = errMsg;
	} else {
		pAEImpState->m_stateMsg.clear();
	}

	if (pObserver != NULL) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool AppAssetImport::convert(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	return doNothing(pStateObj, pObserver, UGCIS_CONVERTING);
}

bool AppAssetImport::process(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	return doNothing(pStateObj, pObserver, UGCIS_PROCESSING);
}

bool AppAssetImport::commit(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	AppEditingImportState* pAEImpState = dynamic_cast<AppEditingImportState*>(pStateObj);
	std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());

	bool res = false;

	ASSERT(pStateObj->m_state == UGCIS_COMMITTING);
	if (pStateObj->m_state == UGCIS_COMMITTING) {
		auto pICE = IClientEngine::Instance();
		if (pICE)
			pICE->GetUGCManager()->AddUGCObj(new AppAssetUpload(pAEImpState->m_assetPath));
		res = true;
	}

	if (pObserver != NULL) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool AppAssetImport::cleanup(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	{
		std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());

		// Update import state
		if (pObserver != NULL) {
			pObserver->doUpdate(pStateObj, EV_DESTROYED);
		}
	}

	disposeState(pStateObj);
	return true;
}

#define DUMMY_RESOURCE IDS_TEXTENTRY_TEXT
#define IDS_APPEDITINGIMPORTSTATE_TEXTUREKEY DUMMY_RESOURCE

BEGIN_GETSET_IMPL(AppEditingImportState, IDS_APPEDITINGIMPORTSTATE_NAME)
// Inherited from IUGCImportStateObj
GETSET(m_state, gsInt, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, STATE)
GETSET(m_stateMsg, gsString, GS_FLAG_EXPOSE, 0, 0, UGCIMPORTSTATE, STATEMSG)
GETSET_OBJ_PTR(m_importedObjectGetSet, GS_FLAG_EXPOSE, 0, 0, UGCIMPORTSTATE, IMPORTEDOBJECTGETSET, IGetSet)
GETSET(m_isCanceling, gsBool, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, ISCANCELING)
GETSET(m_eventId, gsShort, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, EVENTID)
GETSET(m_eventObjId, gsDword, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, EVENTOBJID)
GETSET(m_useImportDlg, gsBool, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, USEIMPORTDLG)
GETSET(m_isValid, gsBool, GS_FLAG_EXPOSE, 0, 0, UGCIMPORTSTATE, ISVALID)
GETSET2(m_rootUri, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCIMPORTSTATE_ROOTURI, IDS_UGCIMPORTSTATE_ROOTURI)
GETSET2(m_importFlags, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCIMPORTSTATE_IMPORTFLAGS, IDS_UGCIMPORTSTATE_IMPORTFLAGS)
// ActionItemImportState Members
GETSET(m_assetPath, gsString, GS_FLAGS_DEFAULT, 0, 0, APPEDITIMPORTSTATE, ASSETPATH)
GETSET(m_type, gsInt, GS_FLAGS_DEFAULT, 0, 0, APPEDITIMPORTSTATE, TYPE)
END_GETSET_IMPL

} // namespace KEP