///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UGCTextureImport.h"
#include "../UGCImportStateObj.h"
#include "../UGCImportStateObserver.h"
#include "GetSet.h"
#include "Common/include/IAssetsDatabase.h"
#include "Common/include/TextureDef.h"
#include "Common/include/CoreStatic/TextureObj.h"
#include "../UGCManager.h"
#include "../UGCItemUploadState.h"
#include "Common/include/KEPFileNames.h"
#include "Common/include/CoreStatic/TextureDatabase.h"
#include "KEPCommon.h"
#include "LogHelper.h"

namespace KEP {

static LogInstance("UGCImport");

class TextureImportState : public IUGCImportStateObj, public GetSet {
public:
	int m_importFlags;

	std::string m_sourceImagePath;
	std::string m_convertedImagePath;

	unsigned int m_sourceImageWidth, m_sourceImageHeight, m_sourceImageBytes;
	unsigned int m_convertedImageWidth, m_convertedImageHeight, m_convertedImageBytes;

	std::string m_textureKey; // texture name hash in string format to avoid sign conversion / precision loss

	TextureImportState() :
			m_importFlags(0),
			m_sourceImageWidth(0),
			m_sourceImageHeight(0),
			m_sourceImageBytes(0),
			m_convertedImageWidth(0),
			m_convertedImageHeight(0),
			m_convertedImageBytes(0) {}

	virtual IGetSet* Values() { return this; }
	virtual int getImportType() { return UGC_TYPE_TEXTURE; }
	virtual PropertySource* getPropertySource(bool bRefresh) {
		assert(false);
		return NULL;
	}

	DECLARE_GETSET
};

IUGCImportStateObj* UGCTextureImport::createState() {
	return new TextureImportState();
}

void UGCTextureImport::disposeState(IUGCImportStateObj* state) {
	if (state) {
		delete state;
	}
}

bool UGCTextureImport::preview(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	return doNothing(pStateObj, pObserver, UGCIS_PREVIEWING);
}

bool UGCTextureImport::import(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	TextureImportState* pTexImpState = dynamic_cast<TextureImportState*>(pStateObj);
	if (!pTexImpState) {
		return false;
	}

	bool res = false;
	std::string errMsg;

	std::lock_guard<fast_recursive_mutex> lock(pTexImpState->getMutex());

	if (pTexImpState->m_state == UGCIS_IMPORTING) {
		if (pTexImpState->m_sourceImagePath.empty()) {
			errMsg = "Internal error: file name not specified for import";
		} else {
			TextureDef texDef = IAssetsDatabase::Instance()->ImportTexture(pTexImpState->m_sourceImagePath, pTexImpState->m_importFlags);
			if (texDef.valid) {
				pTexImpState->m_convertedImagePath = texDef.path + texDef.fileName;
				pTexImpState->m_sourceImageWidth = texDef.origWidth;
				pTexImpState->m_sourceImageHeight = texDef.origHeight;
				pTexImpState->m_sourceImageBytes = (unsigned int)texDef.origBytes;
				pTexImpState->m_convertedImageWidth = texDef.width;
				pTexImpState->m_convertedImageHeight = texDef.height;
				pTexImpState->m_convertedImageBytes = (unsigned int)texDef.bytes;

#if (DRF_TEXTURE_IMPORT_OPT == 1)
				// DRF - Added - Import May Have Changed Source Path
				std::string newSourcePath = PathAdd(texDef.origPath, texDef.origFileName);
				if (!StrSameFilePath(pTexImpState->m_sourceImagePath, newSourcePath)) {
					LogWarn("Source Path Changed -> '" << newSourcePath << "'");
					pTexImpState->m_sourceImagePath = newSourcePath;
				}
#endif

				res = true;
			} else {
				// Import failed
				//@@Todo: return detailed error
				errMsg = "Texture import failed: " + pTexImpState->m_sourceImagePath;
			}
		}
	} else {
		errMsg = "Invalid Texture Import State";
	}

	// Update import state message
	if (!res) {
		pTexImpState->m_stateMsg = errMsg;
	} else {
		pTexImpState->m_stateMsg.clear();
	}

	if (pObserver != NULL) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool UGCTextureImport::convert(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	return doNothing(pStateObj, pObserver, UGCIS_CONVERTING);
}

bool UGCTextureImport::process(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	return doNothing(pStateObj, pObserver, UGCIS_PROCESSING);
}

bool UGCTextureImport::commit(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	TextureImportState* pTexImpState = dynamic_cast<TextureImportState*>(pStateObj);
	VERIFY_RETURN(pTexImpState != NULL, false);

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	auto pTDB = pICE->GetTextureDataBase();
	if (!pTDB) {
		return false;
	}

	std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());

	bool res = false;
	if (pStateObj->m_state == UGCIS_COMMITTING) {
		// Add to global runtime list
		if (pICE->GetUGCManager()) {
			std::string texFileName = pTexImpState->m_convertedImagePath;

			// Add Texture To Database
			DWORD nameHash = INVALID_TEXTURE_NAMEHASH;
			pTDB->TextureAdd(nameHash, texFileName, IMPORTED_TEXTURE);
			if (!ValidTextureNameHash(nameHash)) {
				LogError("AddTextureToDatabase() FAILED - texName='" << texFileName << "'");
			}

			std::stringstream ss;
			ss << nameHash;
			pTexImpState->m_textureKey = ss.str();

			pICE->GetUGCManager()->AddUGCObj(
				new UGCTextureCustomization(pTexImpState->m_targetType,
					pTexImpState->m_targetId,
					pTexImpState->m_convertedImagePath.c_str(),
					pTexImpState->m_sourceImagePath.c_str()));
		}

		res = true;
	} else {
		LogError("Invalid Texture Import State");
	}

	if (pObserver != NULL) {
		pObserver->doUpdate(pStateObj, res ? EV_OP_DONE : EV_OP_FAIL);
	}
	return res;
}

bool UGCTextureImport::cleanup(IUGCImportStateObj* pStateObj, UGCImportStateObserver* pObserver) {
	{
		std::lock_guard<fast_recursive_mutex> lock(pStateObj->getMutex());

		// Update import state
		if (pObserver != NULL) {
			pObserver->doUpdate(pStateObj, EV_DESTROYED);
		}
	}

	disposeState(pStateObj);
	return true;
}

#define DUMMY_RESOURCE IDS_TEXTENTRY_TEXT
#define IDS_TEXTUREIMPORTSTATE_TEXTUREKEY DUMMY_RESOURCE

BEGIN_GETSET_IMPL(TextureImportState, IDS_TEXTUREIMPORTSTATE_NAME)
GETSET(m_state, gsInt, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, STATE)
GETSET(m_stateMsg, gsString, GS_FLAG_EXPOSE, 0, 0, UGCIMPORTSTATE, STATEMSG)
GETSET_OBJ_PTR(m_importedObjectGetSet, GS_FLAG_EXPOSE, 0, 0, UGCIMPORTSTATE, IMPORTEDOBJECTGETSET, IGetSet)
GETSET(m_isCanceling, gsBool, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, ISCANCELING)
GETSET(m_eventId, gsShort, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, EVENTID)
GETSET(m_eventObjId, gsDword, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, EVENTOBJID)
GETSET(m_targetType, gsInt, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETTYPE)
GETSET(m_targetId, gsInt, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETID)
GETSET(m_targetPos.x, gsFloat, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETX)
GETSET(m_targetPos.y, gsFloat, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETY)
GETSET(m_targetPos.z, gsFloat, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, TARGETZ)
GETSET(m_useImportDlg, gsBool, GS_FLAGS_DEFAULT, 0, 0, UGCIMPORTSTATE, USEIMPORTDLG)
GETSET(m_isValid, gsBool, GS_FLAG_EXPOSE, 0, 0, UGCIMPORTSTATE, ISVALID)
GETSET2(m_rootUri, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCIMPORTSTATE_ROOTURI, IDS_UGCIMPORTSTATE_ROOTURI)
GETSET2(m_importFlags, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_UGCIMPORTSTATE_IMPORTFLAGS, IDS_UGCIMPORTSTATE_IMPORTFLAGS)
// TextureImportState Members
GETSET(m_sourceImagePath, gsString, GS_FLAGS_DEFAULT, 0, 0, TEXTUREIMPORTSTATE, SOURCEIMAGEPATH)
GETSET(m_sourceImageWidth, gsUint, GS_FLAGS_DEFAULT, 0, 0, TEXTUREIMPORTSTATE, SOURCEIMAGEWIDTH)
GETSET(m_sourceImageHeight, gsUint, GS_FLAGS_DEFAULT, 0, 0, TEXTUREIMPORTSTATE, SOURCEIMAGEHEIGHT)
GETSET(m_sourceImageBytes, gsUint, GS_FLAGS_DEFAULT, 0, 0, TEXTUREIMPORTSTATE, SOURCEIMAGEBYTES)
GETSET(m_convertedImagePath, gsString, GS_FLAGS_DEFAULT, 0, 0, TEXTUREIMPORTSTATE, CONVERTEDIMAGEPATH)
GETSET(m_convertedImageWidth, gsUint, GS_FLAGS_DEFAULT, 0, 0, TEXTUREIMPORTSTATE, CONVERTEDIMAGEWIDTH)
GETSET(m_convertedImageHeight, gsUint, GS_FLAGS_DEFAULT, 0, 0, TEXTUREIMPORTSTATE, CONVERTEDIMAGEHEIGHT)
GETSET(m_convertedImageBytes, gsUint, GS_FLAGS_DEFAULT, 0, 0, TEXTUREIMPORTSTATE, CONVERTEDIMAGEBYTES)
GETSET2(m_textureKey, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_TEXTUREIMPORTSTATE_TEXTUREKEY, IDS_TEXTUREIMPORTSTATE_TEXTUREKEY)
END_GETSET_IMPL

} // namespace KEP