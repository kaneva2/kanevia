///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SkeletalObjectImportState.h"
#include "CSkeletonObject.h"
#include "RuntimeSkeleton.h"
#include "CBoneClass.h"
#include "CHierarchyMesh.h"
#include "CEXMeshClass.h"
#include "ResourceManagers/ReMeshCreate.h"
#include "KEPFileNames.h"

namespace KEP {

SkeletalObjectImportState::~SkeletalObjectImportState() {
	if (savedSkeleton) {
		delete savedSkeleton;
	}
	savedSkeleton = NULL;
}
/*
void SkeletalObjectImportState::ProcessSkeletonScaling( CSkeletonObject* pSkeletonToProcess )
{
	jsVerifyReturnVoid( pSkeletonToProcess!=NULL );

	// Scale skeleton if necessary
	if( scaleFactor!=prevScaleFactor )
	{
		// scaling factor changed
		if( savedSkeleton==NULL )
		{
			// save original skeleton for future scaling
			pSkeletonToProcess->Clone( &savedSkeleton, NULL, NULL, NULL, NULL, true );
		}

		for( UINT boneLoop=0; boneLoop<pSkeletonToProcess->getBoneCount(); boneLoop++ )
		{
			CBoneObject* pBone = pSkeletonToProcess->getBoneByIndex( boneLoop );
			CBoneObject* pOrigBone = savedSkeleton->getBoneByIndex( boneLoop );
			assert( pBone!=NULL && pOrigBone!=NULL );
			if( pBone!=NULL && pOrigBone!=NULL )
			{
				pBone->m_startPosition = pOrigBone->m_startPosition;
				pBone->m_startPositionInv = pOrigBone->m_startPositionInv;
				pBone->m_startPositionTranspose = pOrigBone->m_startPositionTranspose;

				if( scaleFactor!=1.0f )
				{
					pBone->m_startPosition._41 *= scaleFactor;
					pBone->m_startPosition._42 *= scaleFactor;
					pBone->m_startPosition._43 *= scaleFactor;
					pBone->m_startPositionInv._41 *= scaleFactor;
					pBone->m_startPositionInv._42 *= scaleFactor;
					pBone->m_startPositionInv._43 *= scaleFactor;
					pBone->m_startPositionTranspose._14 *= scaleFactor;
					pBone->m_startPositionTranspose._24 *= scaleFactor;
					pBone->m_startPositionTranspose._34 *= scaleFactor;
				}

				if( pBone->m_boneFrame && pOrigBone->m_boneFrame )
				{
					pBone->m_boneFrame->m_frameMatrix = pOrigBone->m_boneFrame->m_frameMatrix;
					if( scaleFactor!=1.0f )
					{
						pBone->m_boneFrame->m_frameMatrix._41 *= scaleFactor;
						pBone->m_boneFrame->m_frameMatrix._42 *= scaleFactor;
						pBone->m_boneFrame->m_frameMatrix._43 *= scaleFactor;
					}
				}

				for( UINT hmLoop=0; hmLoop<pBone->m_rigidMeshes.size(); hmLoop++ )
				{
					CHierarchyMesh* pHMesh = pBone->m_rigidMeshes[hmLoop];
					CHierarchyMesh* pOrigHMesh = pOrigBone->m_rigidMeshes[hmLoop];
					assert( pHMesh!=NULL && pOrigHMesh!=NULL );
					if( pHMesh!=NULL && pOrigHMesh!=NULL )
					{
						for( int lodLoop=0; lodLoop<pHMesh->m_lodChain->GetCount(); lodLoop++ )
						{
							CHiarcleVisObj* pHVis = pHMesh->GetByIndex( lodLoop );
							CHiarcleVisObj* pOrigHVis = pOrigHMesh->GetByIndex( lodLoop );
							assert( pHVis!=NULL && pOrigHVis!=NULL && pHVis->m_mesh && pOrigHVis->m_mesh );
							if( pHVis!=NULL && pOrigHVis!=NULL && pHVis->m_mesh && pOrigHVis->m_mesh )
							{
								pOrigHVis->m_mesh->m_abVertexArray.CloneDataOnly( &pHVis->m_mesh->m_abVertexArray );
								if( scaleFactor!=1.0f )
								{
									D3DXMATRIX scaleMatrix;
									D3DXMatrixScaling( &scaleMatrix, scaleFactor, scaleFactor, scaleFactor );
									pHVis->m_mesh->m_abVertexArray.Transform( scaleMatrix );
								}
							}
						}
					}
				}
			}
		}
	}

	prevScaleFactor = scaleFactor;
}
*/
void SkeletalObjectImportState::ProcessSkeletonMaterials(RuntimeSkeleton* pSkeletonToProcess, const SkeletonConfiguration* pSkeletonCfg, bool bFinalize /*= false*/) {
	jsVerifyReturnVoid(pSkeletonToProcess != NULL);

	// Prepare skeleton for rendering
	// 1. Traverse all deformable meshes and materials
	// TBD

	// 2. Traverse all rigid meshes and materials
	for (UINT boneLoop = 0; boneLoop < pSkeletonToProcess->getBoneCount(); boneLoop++) {
		CBoneObject* pBone = pSkeletonToProcess->getBoneByIndex(boneLoop);
		for (std::vector<CHierarchyMesh*>::iterator itHM = pBone->m_rigidMeshes.begin(); itHM != pBone->m_rigidMeshes.end(); ++itHM) {
			for (int lodNo = 0; lodNo < (*itHM)->m_lodChain->GetCount(); lodNo++) {
				CHierarchyVisObj* pVis = (*itHM)->GetByIndex(lodNo);
				assert(pVis->m_mesh);

				// Create rematerial
				assert(pVis->m_mesh->m_materialObject);
				if (pVis->m_mesh->m_materialObject) {
					ProcessMaterial(*pVis->m_mesh->m_materialObject, bFinalize);
					MeshRM::Instance()->CreateReMaterial(pVis->m_mesh->m_materialObject, IMPORTED_TEXTURE);
				}
			}
		}
	}
}

bool SkeletalObjectImportState::ProcessSkeletonMeshes(RuntimeSkeleton* pSkeletonToProcess, const SkeletonConfiguration* pSkeletonCfg) {
	jsVerifyReturn(pSkeletonToProcess != NULL, false);

	// Prepare skeleton for rendering
	// 1. deformable meshes and materials: already done in ColladaImporter

	// 2. Traverse all rigid meshes and materials
	for (UINT boneLoop = 0; boneLoop < pSkeletonToProcess->getBoneCount(); boneLoop++) {
		CBoneObject* pBone = pSkeletonToProcess->getBoneByIndex(boneLoop);
		for (std::vector<CHierarchyMesh*>::iterator itHM = pBone->m_rigidMeshes.begin(); itHM != pBone->m_rigidMeshes.end(); ++itHM) {
			for (int lodNo = 0; lodNo < (*itHM)->m_lodChain->GetCount(); lodNo++) {
				CHierarchyVisObj* pVis = (*itHM)->GetByIndex(lodNo);
				assert(pVis->m_mesh);

				// Bake per-mesh transformations
				pVis->m_mesh->BakeVertices();
				pVis->m_mesh->ComputeBoundingVolumes();
				//pVis->m_mesh->OptimizeMesh(); // Necessary?

				// Create remesh
				ReMesh* rm = MeshRM::Instance()->CreateReStaticMesh(pVis->m_mesh, MeshRM::ORIGIN, IMPORTED_TEXTURE); // no pivot point needed - CreateReStaticMeshLibrary
				if (rm) {
					UINT64 meshHash = rm->GetHash();
					ReMesh* findMesh = MeshRM::Instance()->FindMesh(meshHash, MeshRM::RMP_ID_DYNAMIC);
					if (findMesh == NULL) {
						// if mesh is new add it to reference pool
						MeshRM::Instance()->AddMesh(rm, MeshRM::RMP_ID_DYNAMIC);
					} else {
						// if mesh already exists use reference copy.
						delete rm;
						rm = findMesh;
					}
					ReMesh* clonedMesh;
					rm->Clone(&clonedMesh);
					pVis->SetReMesh(clonedMesh);
				} else {
					// CreateReStaticMesh could return null if it can't allocate storage for the mesh.
					// That probably means that something else is going to fail soon, but the least we
					// can do is fail this import.
					return false;
				}
			}
		}
	}

	// 3. Recompute bounding box with meshes
	pSkeletonToProcess->computeBoundingBoxWithSkinnedMesh(pSkeletonCfg);
	return true;
}

} // namespace KEP