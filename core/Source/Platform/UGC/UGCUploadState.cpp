///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UGC_Global.h"
#include "UGCUploadState.h"
#include "UGCProperties.h"
#include "UGCManager.h"
#include "IClientEngine.h"
#include "Event/Base/IEvent.h"
#include "TextureObj.h"
#include "ResourceManagers/ResourceManager.h"
#include "config.h"
#include "CompressHelper.h"
#include "LogHelper.h"
#include "Core/Crypto/Crypto_MD5.h"
#include <sstream>

namespace KEP {

static LogInstance("UGCImport");

IUGCManager* UGCUploadState::ms_pManager = NULL;

void UGCUploadState::dumpAllProperties(std::map<int, std::string>& allPropertiesFound) {
	allPropertiesFound.clear();
	PropertySource* ps = getPropertySource();
	if (ps) {
		std::vector<int> allPropIds;
		ps->getAllSupportedPropertyIds(allPropIds);

		for (const auto& propId : allPropIds) {
			Value val = ps->get(propId);
			allPropertiesFound.insert(make_pair(propId, val.toString()));
		}
		delete ps;
	}
}

FileSize UGCUploadState::computeUploadSize(IClientEngine* pEngine) {
	FileSize retBufSize = 0;
	std::vector<std::string> retUploadFiles;
	(void)prepareDataForUpload(pEngine, retBufSize, retUploadFiles);
	assert(retBufSize == 0 || retUploadFiles.size() == 0); // can't have both

	FileSize totalSize = retBufSize;
	for (auto it = retUploadFiles.begin(); it != retUploadFiles.end(); ++it) {
		CFileStatus status;
		if (CFile::GetStatus(Utf8ToUtf16(*it).c_str(), status)) {
			if (status.m_size <= 0xFFFFFFFF)
				totalSize += status.m_size;
		}
	}

	setBytesTotal(totalSize);
	return totalSize;
}

bool UGCUploadState::changeState(int preStates, int postState) {
	// check upload state
	if (!checkState(preStates)) {
		LogError("invalid state, expected: " << preStates << ", actual: " << m_state);
		return false;
	}

	setState(postState);
	return true;
}

bool UGCUploadState::completeUpload() {
	jsVerifyReturn(ms_pManager != NULL, false);

	if (checkState(PS_UPLOADED)) {
		if (!changeState(PS_UPLOADED, PS_SERVERREADY))
			return false;
	}

	IEvent* e = m_uploadCompletionEvent;
	if (e) {
		int cancelled = 0, failed = 0;
		if (checkState(PS_CANCELLED))
			cancelled = 1;
		if (checkState(PS_FAILED))
			failed = 1;

		(*e->OutBuffer()) << getType() << m_uploadId << cancelled << failed;

		ms_pManager->SendEventToEngine(e);
	}

	return true;
}

std::string UGCUploadChildState::getSubmissionUrl(IClientEngine* pEngine) {
	std::stringstream ssBytes;
	ssBytes << getBytesTotal();

	std::string url = UGC_UPLOAD_REQUEST_URL;
	STLStringSubstitute(url, TAG_WOK_URL, pEngine->GetURL(CFG_WOKURL, false));
	STLStringSubstitute(url, TAG(UGC_UPLOAD_TYPE), GetUploadTypeStringFromUGCType(getType()));
	STLStringSubstitute(url, TAG(UGC_UPLOAD_SIZE), ssBytes.str());
	STLStringSubstitute(url, TAG(UGC_MASTER_ID), m_masterUploadId); // master upload ID
	STLStringSubstitute(url, TAG(UGC_SUB_ID), m_childId); // master upload ID

	std::map<int, std::string> ugcProps;
	dumpAllProperties(ugcProps);
	url = url + "&" + EncodePropertiesToQueryString(UGC_PROPS, ugcProps);

	return url;
}

bool UGCUploadChildState::changeState(int preStates, int postState) {
	if (!UGCUploadState::changeState(preStates, postState)) {
		return false;
	}

	VERIFY_RETURN(GetUGCManager() != nullptr, false);

	if (postState == PS_UPLOADED) {
		setState(PS_SERVERREADY);

		// retrieve parentId by master UploadId
		UGCItemUpload* pUgcObj = GetUGCManager()->GetUGCObjByUploadId(m_masterUploadId);
		// report upload completion to parent
		pUgcObj->childItemUploadCompleted(this);
	}

	return true;
}

UGCItemTexture::UGCItemTexture(int type, unsigned hash, const std::string& path, long long texSlotId, int state /*= PS_CREATED */) :
		UGCUploadChildState(type, (long long)hash, std::to_string(texSlotId), state), m_fileName(path) {
}

unsigned char* UGCItemTexture::prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles) {
	uploadFiles.clear();
	retBufLen = 0;
	jsVerifyReturn(pEngine != NULL, NULL);

	if (getType() == UGC_TYPE_TEX_ICON || getType() == UGC_TYPE_MEDIA_IMAGE_PATTERN || getType() == UGC_TYPE_MEDIA_IMAGE_PICTURE) {
		// Make a special case for icons and media as they are not in any texture databases
		uploadFiles.push_back(m_fileName);
		return NULL;
	}

	// Search texture database
	long texId = getId();
	CTextureEX* pTex = pEngine->GetTextureFromDatabase((unsigned)texId);
	if (!pTex) {
		LogError("GetTextureFromDBs() FAILED - texId=" << texId);
		return NULL;
	}

	// Discover Texture File Path & Hash
	std::string filePath = pTex->GetFilePath();
	unsigned texHash = (unsigned)pTex->GetHashKey();

	// Valid File ?
	auto fileSize = FileHelper::Size(filePath);
	if (!fileSize) {
		LogError("Invalid Texture File - '" << filePath << "' texHash=" << texHash);
		return NULL;
	}

	// Re-Hash Texture File
	MD5_DIGEST_STR hashStr;
	if (!hashFile(filePath.c_str(), hashStr, 0)) {
		LogError("hashFile() FAILED - '" << filePath << "' texHash=" << texHash);
		return NULL;
	}
	setOrigHash(hashStr.s);
	setOrigSize(fileSize);

	// Must be DDS
	assert(pTex->IsDDS());

	// Only DDS Requires Compression/Encryption
	if (!pTex->IsDDS()) {
		uploadFiles.push_back(filePath);
		return NULL;
	}

	// Allocate memory
	BYTE* rawData = new BYTE[fileSize];
	if (!rawData) {
		LogError("new FAILED - size=" << fileSize << " texHash=" << texHash);
		return NULL;
	}

	// Open Texture File
	FileHelper fhTex(filePath, GENERIC_READ, OPEN_EXISTING);
	if (!fhTex.IsOpen()) {
		LogError("FileHelper::Open FAILED - '" << filePath << "' texHash=" << texHash);
		delete[] rawData;
		return NULL;
	}

	// Read Texture File
	if (fhTex.Rx(rawData, fileSize) != fileSize) {
		LogError("FileHelper::Rx() FAILED - '" << filePath << "' size=" << fileSize);
		delete[] rawData;
		return NULL;
	}

	// We no longer encrypt textures
	auto pData = rawData;

	// Compress Texture Data
	if (!::CompressMemory(pData, fileSize, COMPRESS_TYPE_GZIP)) {
		LogError("CompressMemory() FAILED - size=" << fileSize);
		delete[] pData;
		return NULL;
	}

	// Return Compressed Data & Size
	retBufLen = fileSize;
	return pData;
}

BYTE* UGCItemThumbnails::prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles) {
	retBufLen = 0;
	for (const auto& it : m_previewImages)
		uploadFiles.push_back(it.second);
	return NULL;
}

bool UGCItemThumbnails::addImage(const std::string& imgPath, int imgId) {
	std::map<int, std::string>::iterator findIt = m_previewImages.find(imgId);
	if (findIt != m_previewImages.end()) { // ID already exists
		return false;
	}

	m_previewImages[imgId] = imgPath;
	return true;
}

UGCItemUpload::UGCItemUpload(int aType, long long aId, int aState) :
		UGCUploadState(aType, aId, aState),
		m_uploadFilesTotal(0),
		m_uploadFilesCompleted(0),
		m_uploadFilesInProgress(0),
		m_uploadBytesTotal(0),
		m_uploadBytesProcessed(0) {
	addChildItem(UGC_TYPE_TEX_ICON, 0, &m_previewImagesState);
}

std::string UGCItemUpload::getSubmissionUrl(IClientEngine* pEngine) {
	std::stringstream ssBytes;
	ssBytes << getBytesTotal();

	std::string url = UGC_UPLOAD_REQUEST_URL;
	STLStringSubstitute(url, TAG_WOK_URL, pEngine->GetURL(CFG_WOKURL, false));
	STLStringSubstitute(url, TAG(UGC_UPLOAD_TYPE), GetUploadTypeStringFromUGCType(getType()));
	STLStringSubstitute(url, TAG(UGC_UPLOAD_SIZE), ssBytes.str());
	STLStringSubstitute(url, TAG(UGC_MASTER_ID), ""); // no master ID
	STLStringSubstitute(url, TAG(UGC_SUB_ID), ""); // no sub ID

	std::map<int, std::string> ugcProps;
	dumpAllProperties(ugcProps);
	url = url + "&" + EncodePropertiesToQueryString(UGC_PROPS, ugcProps);

	return url;
}

bool UGCItemUpload::addPreviewImage(const std::string& filePath, int previewImageId) {
	return m_previewImagesState.addImage(filePath, previewImageId);
}

void UGCItemUpload::clearPreviewImages() {
	return m_previewImagesState.clear();
}

void UGCItemUpload::addChildItem(int childType, int childId, UGCUploadChildState* childState) {
	m_childItems[childType][childId] = childState;
}

void UGCItemUpload::clearChildItemByType(int childType) {
	if (m_childItems.find(childType) != m_childItems.end()) {
		m_childItems[childType].clear();
	}
}

UGCUploadChildState* UGCItemUpload::getChildItem(int childType, int childId) {
	if (m_childItems.find(childType) == m_childItems.end()) {
		return NULL;
	}

	std::map<int, UGCUploadChildState*>::iterator findIt = m_childItems[childType].find(childId);
	if (findIt == m_childItems[childType].end()) {
		return NULL;
	}

	return findIt->second;
}

bool UGCItemUpload::changeState(int preStates, int postState) {
	if (!UGCUploadState::changeState(preStates, postState)) {
		return false;
	}

	VERIFY_RETURN(GetUGCManager() != nullptr, false);

	if (postState == PS_UPLOADING) {
		// compute upload total
		unsigned numChildItems = 0;
		FileSize totalChildItemsSize = 0;
		collectChildItemsUploadTotal(numChildItems, totalChildItemsSize);
		m_uploadFilesTotal = 1 + numChildItems; // add one for main file

		m_uploadBytesTotal = computeUploadSize(GetUGCManager()->GetEngine()) + totalChildItemsSize;

		// clear progresses
		resetProgress();
	} else if (postState == PS_UPLOADED) {
		if (!GetUGCManager()->SubmitChildItemsForUGCObj(*this)) {
			// No textures to submit, complete dynamic object upload
			completeUpload();
		}
	} else if (postState == PS_FAILED || postState == PS_CANCELLED) {
		// Complete upload and report error if necessary
		completeUpload();
	}

	return true;
}

bool UGCItemUpload::childItemUploadCompleted(UGCUploadChildState* child) {
	VERIFY_RETURN(GetUGCManager() != nullptr, false);

	if (!checkState(PS_UPLOADED)) {
		return false;
	}

	// check if all children have been uploaded
	if (!checkAllChildItemsStates(PS_UPLOADED | PS_SERVERREADY)) {
		return false;
	}

	// All children uploaded, complete upload
	completeUpload();
	return true;
}

bool UGCItemUpload::checkAllChildItemsStates(int desiredStates) {
	for (auto itGrp = m_childItems.begin(); itGrp != m_childItems.end(); ++itGrp) {
		for (auto it = itGrp->second.begin(); it != itGrp->second.end(); ++it) {
			UGCUploadChildState* pChildState = it->second;
			assert(pChildState != NULL);
			if (pChildState && !pChildState->checkState(desiredStates)) {
				return false;
			}
		}
	}

	return true;
}

unsigned UGCItemUpload::getChildItemCount() const {
	unsigned numItems = 0;
	for (auto itGrp = m_childItems.begin(); itGrp != m_childItems.end(); ++itGrp) {
		numItems += itGrp->second.size();
	}
	return numItems;
}

void UGCItemUpload::collectChildItemsUploadTotal(unsigned& numItems, FileSize& totalSize) {
	numItems = 0;
	totalSize = 0;

	VERIFY_RETVOID(GetUGCManager() != nullptr);
	auto pEngine = GetUGCManager()->GetEngine();
	VERIFY_RETVOID(pEngine != nullptr);

	for (auto itGrp = m_childItems.begin(); itGrp != m_childItems.end(); ++itGrp) {
		for (auto it = itGrp->second.begin(); it != itGrp->second.end(); ++it) {
			UGCUploadChildState* pChildState = it->second;
			assert(pChildState != NULL);
			if (pChildState) {
				totalSize += pChildState->computeUploadSize(pEngine);
			}
			numItems++;
		}
	}
}

BYTE* UGCItemUpload_IResource::prepareDataForUpload(IClientEngine* pEngine, FileSize& retBufLen, std::vector<std::string>& uploadFiles) {
	uploadFiles.clear();

	// Only Do This Once
	if (m_uploadData) {
		retBufLen = m_uploadSize;
		return m_uploadData;
	}

	FileSize dataSize = 0;
	BYTE* pData = m_pRes->GetSerializedData(dataSize);
	if (pData) {
		// Get size and hash
		MD5_DIGEST_STR hashStr;
		hashData(pData, dataSize, hashStr, 0);
		setOrigHash(hashStr.s);
		setOrigSize(dataSize);
	}

	if (!pData || !m_pRes->IsSerializedDataCompressible()) {
		m_uploadSize = dataSize;
		m_uploadData = pData;
		retBufLen = dataSize;
		return m_uploadData;
	}

	// Compress Data
	if (!::CompressMemory(pData, dataSize, COMPRESS_TYPE_GZIP)) {
		LogError("CompressMemory() FAILED - size=" << dataSize);
		delete[] pData;
		pData = NULL;
		dataSize = 0;
	}

	m_uploadSize = dataSize;
	m_uploadData = pData;
	retBufLen = dataSize;
	return m_uploadData;
}

} // namespace KEP