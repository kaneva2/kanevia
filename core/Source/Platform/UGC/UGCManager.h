///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "UGC_Global.h"

#include "glids.h"
#include "Core/Math/Vector.h"

#include <sstream>
#include <string>
#include <vector>
#include <map>

enum IModelType;

namespace KEP {

class IClientEngine;
class IEvent;
class UGCImporter;
class IUGCImportStateObj;
class PropertySource;
class UGCUploadState;
class UGCItemUpload;
class CTextureEX;

class IUGCManager {
public:
	virtual UGC_TYPE ClassifyImportType(int dropType, const std::string& importUri, IModelType targetType, int targetId, const Vector3f& targetPos, const Vector3f& targetNml) = 0;

	virtual bool BrowseFileByImportType(UGC_TYPE type, IEvent* completionEvent) = 0;

	virtual UGCImporter* GetImporterByType(UGC_TYPE type) = 0;
	virtual void ChangeImportState(IUGCImportStateObj* pStateObj, int newState, bool bDoUpdate) = 0;
	virtual void TickleImportState(IUGCImportStateObj* pStateObj, int eventType) = 0;

	// UGC Obj
	virtual long long AddUGCObj(UGCItemUpload* ugcObj, bool bAddUGCTextures = true) = 0;
	virtual void RemoveUGCObj(int ugcType, long long ugcObjId) = 0;
	virtual void ClearUGCObjList(int ugcType) = 0;
	virtual void EnumUGCObjs(int ugcType, std::vector<long long>& allUGCObjIds) = 0;

	virtual UGCUploadState* GetUGCState(int ugcType, long long ugcId) = 0;

	// Child items
	virtual bool AddImageToUGCObj(int ugcType, long long ugcObjId, int imgType, const std::string& imgPath, long long subId) = 0;
	virtual bool ClearUGCObjImagesByType(int ugcType, long long ugcObjId, int imgType) = 0;

	// Submission
	virtual bool SubmitUGCObj(int ugcType, long long ugcObjId, IEvent* submissionFeedbackEvent = NULL) = 0;
	virtual bool SubmitChildItemsForUGCObj(int ugcType, long long ugcObjId, IEvent* submissionFeedbackEvent = NULL) = 0;
	virtual bool SubmitChildItemsForUGCObj(UGCItemUpload& ugcObj, IEvent* submissionFeedbackEvent = NULL) = 0;

	virtual bool Upload(const std::string& uploadId, int ugcType, long long ugcObjId, IEvent* uploadProgressEvent, IEvent* uploadCompletionEvent) = 0;

	virtual bool DeployUGCUpload(const std::string& uploadId, int type, IEvent* deployCompletionEvent) = 0;

	virtual int ImportSoundFile(const std::string& filePath, bool encrypt = true) = 0;

	virtual int ImportActionItem(const std::string& fullPath, int defaultGlid, const std::string& bundleGlids, int parentGlid) = 0;

	// Engine access
	virtual IClientEngine* GetEngine() = 0;
	virtual void SetEngine(IClientEngine* engine) = 0;

	// Proxy to Engine
	virtual void SendEventToEngine(IEvent* e) = 0;

	virtual UGCItemUpload* GetUGCObjByUploadId(const std::string& uploadId) = 0;
};

IUGCManager* InitUGCManager(IClientEngine* pEngine);
void DisposeUGCManager(IUGCManager* ugcMgr);

// Utility functions
std::string EncodePropertiesToQueryString(const std::string& paramName, const std::map<int, std::string>& props);
char* GetUploadTypeStringFromUGCType(int ugcType); // defined in UGCManager.cpp

} // namespace KEP