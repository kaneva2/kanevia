@echo off
rem copy the binaries from the templates directory to the given game dir
if not exist ..\templates goto wrongdir
if %1. == . goto nodir
if not exist %1 goto nodir
echo This will copy the binaries from current install to  
echo     %1
pause

@echo on
ECHO Copying Server Binaries...

REM Set Variables

set SERVER_BIN=%~1\bind
set SERVER_BLADES=%~1\Bladesd
set SERVER_BLADESCRIPTS=%~1\BladeScriptsd

set DEPLOY=%~dp0..\..
set DEPLOY_BIN=%DEPLOY%\GameContent\bind
set DEPLOY_BLADES=%DEPLOY%\GameContent\Bladesd
set DEPLOY_SHARED=%DEPLOY%\GameContent\templates\Shared
set DEPLOY_BLADESCRIPTS=%DEPLOY_SHARED%\GameFiles\BladeScriptsd

REM Copy Server Debug Binaries
xcopy "%DEPLOY_BIN%\*.exe" "%SERVER_BIN%" /y
xcopy "%DEPLOY_BIN%\*.dll" "%SERVER_BIN%" /y
xcopy /s "%DEPLOY_BLADES%" "%SERVER_BLADES%\" /y
xcopy /s "%DEPLOY_BLADESCRIPTS%" "%SERVER_BLADESCRIPTS%\" /y

REM Delete Blades Server Does Not Use
del /F /Q "%SERVER_BLADES%\*.pdb"
del /F /Q "%SERVER_BLADES%\*.lib"
del /F /Q "%SERVER_BLADES%\*.exp"
del /F /Q "%SERVER_BLADES%\*.ilk"
del /F /Q "%SERVER_BLADESCRIPTS%\*.pdb"
del /F /Q "%SERVER_BLADESCRIPTS%\*.lib"
del /F /Q "%SERVER_BLADESCRIPTS%\*.exp"
del /F /Q "%SERVER_BLADESCRIPTS%\*.ilk"

PAUSE
EXIT /B 0

goto end
:wrongdir
echo Must run from the installed bin directory
goto end

:nodir
echo Supply valid destination directory.  E.g. c:\newgame
goto end
:end 
