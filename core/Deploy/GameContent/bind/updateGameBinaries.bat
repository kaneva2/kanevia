@echo off
rem copy the binaries from the templates directory to the given game dir
if not exist ..\templates goto wrongdir
if %1. == . goto nodir
if not exist %1 goto nodir
echo This will copy the binaries from current install to  
echo     %1
pause
rem copy the files
xcopy CorePluginLibraryd.dll %1 /y
xcopy mysqlpp_d.dll %1 /y
xcopy SimpleAmqpClient.2d.dll %1 /y
xcopy rabbitmq.4d.dll %1 /y
xcopy CoreUtild.dll %1 /y
xcopy KEPMathd.dll %1 /y
xcopy KEPPhysicsd.dll %1 /y
xcopy KEPUtild.dll %1 /y
xcopy CrashRpt1403d.dll %1 /y
xcopy log4cplusUD.dll %1 /y
xcopy nvtt-d.dll %1 /y
xcopy dbghelp.dll %1 /y
xcopy vld_x86.dll %1 /y
pushd ..\templates\shared
xcopy kepclientd.exe %1 /y
xcopy ClientBladesd\*.dll %1\ClientBladesd\ /y
xcopy ClientBladesd\*.pdb %1\ClientBladesd\ /y
xcopy /s GameFiles\bladescriptsd %1\GameFiles\bladescriptsd\ /y
xcopy Crash*.dll %1 /y
xcopy Lua*.dll %1 /y
xcopy cjson*.dll %1 /y
xcopy f_in_box*.dll %1 /y
xcopy FreeImage*.dll %1 /y
xcopy libcollada14dom21-d.dll %1 /y
xcopy LzmaLib*.dll %1 /y
xcopy Zlib1*.dll %1 /y
xcopy jpeg62*.dll %1 /y
popd
echo ended
goto end
:wrongdir
echo Must run from the installed bin directory
goto end
:nodir
echo Supply valid destination directory.  E.g. c:\newgame
goto end
:end 
PAUSE
