dofile( "..\\Scripts\\KEP.lua" )
--MenuHelper

-- global used to save off dialog handle
gDialogHandle = 0

function Helper_Dialog_OnCreate(dialogHandle)
	Dialog_RegisterGenericEvents(dialogHandle);
	gDialogHandle = dialogHandle
end

function Helper_Dialog_OnDestroy(dialogHandle)
	Dialog_UnRegisterGenericEvents(dialogHandle);
	gDialogHandle = 0
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	KEP.parent = handler
	KEP.dispatcher = dispatcher
	KEP.debugLevel = debugLevel
	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	KEP.parent = handler
	KEP.dispatcher = dispatcher
	KEP.debugLevel = dbgLevel
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "OpenMenuEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "MoveResizeEvent", KEP.HIGH_PRIO )
end

---------------------------------------------------------------------
--
-- control Edit Handlers
--
---------------------------------------------------------------------

function OnControlSelected(controlHandle)

	local controlName = Control_GetName(controlHandle);

	MenuControlSelected(controlName)
end

function OnControlMoved(controlHandle, x, y)

	local controlName = Control_GetName(controlHandle);

	MenuControlMoved(controlName, x, y)
end


function OnControlResized(controlHandle, width, height)

	local controlName = Control_GetName(controlHandle);

	MenuControlResized(controlName, width, height)
end

---------------------------------------------------------------------
--
-- Dialog Edit Handlers
--
---------------------------------------------------------------------

function Dialog_OnSelected(dialogHandle)
	MenuSelected();
end

function Dialog_OnMoved(dialogHandle, x, y)
	MenuMoved(x, y)
end


function Dialog_OnResized(dialogHandle, width, height)
	MenuResized(width, height)
end

---------------------------------------------------------------------
--
-- control Helpers
--
---------------------------------------------------------------------

function MenuControlSelected(controlName)

	event = Dispatcher_MakeEvent( KEP.dispatcher, "MenuControlSelectedEvent" )

	Event_EncodeString( event, controlName )

	Dispatcher_QueueEvent( KEP.dispatcher, event )
end

function MenuControlMoved(controlName, x, y)

	event = Dispatcher_MakeEvent( KEP.dispatcher, "MenuControlMovedEvent" )

	Event_EncodeString( event, controlName )
	Event_EncodeNumber( event, x )
	Event_EncodeNumber( event, y )

	Dispatcher_QueueEvent( KEP.dispatcher, event )
end

function MenuControlResized(controlName, width, height)

	event = Dispatcher_MakeEvent( KEP.dispatcher, "MenuControlResizedEvent" )

	Event_EncodeString( event, controlName )
	Event_EncodeNumber( event, width )
	Event_EncodeNumber( event, height )

	Dispatcher_QueueEvent( KEP.dispatcher, event )
end

---------------------------------------------------------------------
--
-- Dialog Helpers
--
---------------------------------------------------------------------

function MenuSelected()

	event = Dispatcher_MakeEvent( KEP.dispatcher, "MenuSelectedEvent" )

	Dispatcher_QueueEvent( KEP.dispatcher, event )
end

function MenuMoved(x, y)

	event = Dispatcher_MakeEvent( KEP.dispatcher, "MenuMovedEvent" )

	Event_EncodeNumber( event, x )
	Event_EncodeNumber( event, y )

	Dispatcher_QueueEvent( KEP.dispatcher, event )
end

function MenuResized(width, height)

	event = Dispatcher_MakeEvent( KEP.dispatcher, "MenuResizedEvent" )

	Event_EncodeNumber( event, width )
	Event_EncodeNumber( event, height )

	Dispatcher_QueueEvent( KEP.dispatcher, event )
end

function DestroyMenu(dialogHandle)

	--event = Dispatcher_MakeEvent( KEP.dispatcher, "DestroyMenuEvent" )

	--Event_EncodeNumber( event, dialogHandle )

	--Dispatcher_QueueEvent( KEP.dispatcher, event )

	Dialog_Destroy( dialogHandle )

end
 

function MessageBox(text)
	msgBox = Dialog_Create( "MessageBox.xml" )
	Static_SetText( Dialog_GetStatic( msgBox, "lblText" ), text )
end

-- send a message to the chat window
function SendChatMessage( dispatcher, msg, msgType )

	textEvent = Dispatcher_MakeEvent( dispatcher, "RenderTextEvent" )
	if textEvent ~= nil then 
		Event_EncodeString( textEvent, msg )
		Event_EncodeNumber( textEvent, msgType )
		Dispatcher_QueueEvent( dispatcher, textEvent )
	else
		MessageBox( "nil textEvent in SendChatMessage!" )
	end

end 

-- Remove unsupported HTML tags from strings
function stripTags( text )

	if text ~= nil then
		-- Begin wierd control character stripping
		text = string.gsub(text, "&lt;", "<") 	-- left bracket
		text = string.gsub(text, "&gt;", ">") 	-- right bracket
		text = string.gsub(text, "&amp;", "&") 	-- ampersand
		text = string.gsub(text, "&nbsp;", "") 	-- backspace
		
		-- Ok now these we should actually natively support, but don't
		text = string.gsub(text, "&#8217;", "'")  	-- apostrophee(sp)
		text = string.gsub(text, "&#160;", " ")	-- non-breaking space
		text = string.gsub(text, "&amp;", "&")	-- literal ampersand (do me last)
		text = string.gsub(text, "<div>", "")		-- ignore css crap
		text = string.gsub(text, "</div>", "")		-- ignore css crap
		text = string.gsub(text, "<span>", "")	
		text = string.gsub(text, "<span />", "")

	end

    return text

end
