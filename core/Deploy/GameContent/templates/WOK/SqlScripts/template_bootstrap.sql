-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- boot strap channel zones
INSERT INTO channel_zones (`kaneva_user_id`,`zone_index`,`zone_index_plain`,`zone_type`,`zone_instance_id`,`name`,`tied_to_server`,`created_date`,`access_rights`,`raves`,`access_friend_group_id`,`server_id`,`cover_charge`,`visibility`,`last_update`,script_server_id) 
VALUES (0,2,2,0,0,'Store Front','NOT_TIED',now(),0,0,0,0,0,100,now(),0);

INSERT INTO channel_zones (`kaneva_user_id`,`zone_index`,`zone_index_plain`,`zone_type`,`zone_instance_id`,`name`,`tied_to_server`,`created_date`,`access_rights`,`raves`,`access_friend_group_id`,`server_id`,`cover_charge`,`visibility`,`last_update`,script_server_id) 
VALUES (0,1,1,0,0,'Conference Room','NOT_TIED',now(),0,0,0,0,0,100,now(),0);

INSERT INTO channel_zones (`kaneva_user_id`,`zone_index`,`zone_index_plain`,`zone_type`,`zone_instance_id`,`name`,`tied_to_server`,`created_date`,`access_rights`,`raves`,`access_friend_group_id`,`server_id`,`cover_charge`,`visibility`,`last_update`,script_server_id) 
VALUES (0,3,3,0,0,'Open Space','NOT_TIED',now(),0,0,0,0,0,100,now(),0);



-- boot strap WOK inventory
INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,3,1247,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,3,1248,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,3,1249,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,4,1247,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,4,1248,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,4,1249,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,5,1247,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,5,1248,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,5,1249,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,6,1172,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,6,1173,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,6,1174,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,7,1172,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,7,1173,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,7,1174,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,8,1172,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,8,1173,'P','T',1,0);

INSERT INTO starting_inventories(`active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity`) 
VALUES (1,8,1174,'P','T',1,0);



-- boot strap WOK cash
INSERT   INTO starting_balances (kei_point_id, balance)
   VALUES ('KPOINT', 500);

