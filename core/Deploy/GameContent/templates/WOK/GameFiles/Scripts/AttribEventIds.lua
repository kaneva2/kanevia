AttribEvent               		= {}

AttribEvent.SUCCESS			= 1
AttribEvent.FAIL			= 0

AttribEvent.NO_TYPE			= 0
AttribEvent.PLAYER_HEALTH		= 3
AttribEvent.PLAYER_CREATED_HEALTH	= 8
AttribEvent.PLAYER_CREDITS		= 19
AttribEvent.PLAYER_INFO			= 21
AttribEvent.BANK_CREDITS		= 22
AttribEvent.AMMO_DATA			= 27
AttribEvent.LOOT_DATA			= 29
AttribEvent.QUEST_MENU			= 33
AttribEvent.PLAYER_INVEN		= 51
AttribEvent.VENDOR_INVEN		= 52
AttribEvent.TRADE_OFFER			= 59
AttribEvent.SKILLS			= 141
AttribEvent.GENERIC_TYPE		= 142
AttribEvent.ARMITEM_TYPE		= 143

-- AttribEvent.BANK_INVEN is not explicitly defined -- the value is the ID of the bank menu
