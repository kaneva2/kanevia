-- Generated by getsetscript.pl on  01/10/2007 11:22 from ..\..\..\..\..\Source\Platform\Common\XStatic\CDayStateClass.cpp
DayStateIds                        = {}
DayStateIds.DAYSTATEIDS                                  =  14712  -- class Id
DayStateIds.DURATION                                     =      0  -- gsLong
DayStateIds.FOGSTARTRANGE                                =      1  -- gsFloat
DayStateIds.FOGENDRANGE                                  =      2  -- gsFloat
DayStateIds.SUNROTATIONX                                 =      3  -- gsFloat
DayStateIds.SUNROTATIONY                                 =      4  -- gsFloat
DayStateIds.SUNDISTANCE                                  =      5  -- gsLong
DayStateIds.FOGCOLORRED                                  =      6  -- RGB color
DayStateIds.SUNCOLORRED                                  =      7  -- RGB color
DayStateIds.AMBIENTCOLORRED                              =      8  -- RGB color
