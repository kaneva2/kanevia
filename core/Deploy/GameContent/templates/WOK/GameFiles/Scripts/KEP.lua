-- global variable and "constants" for Python
KEP = {}

-- event priority
KEP.LOW_PRIO = 10
KEP.MED_PRIO = 100
KEP.HIGH_PRIO = 200

-- event security
KEP.ESEC_LOCAL_ONLY         = 0 		-- 0x0000   -- never matches anything, so can be from network
KEP.ESEC_ALL                = 65535		-- 0xffff
KEP.ESEC_CLIENT             = 1			-- 0x0001   -- game client
KEP.ESEC_SERVER             = 2			-- 0x0002   -- game server
KEP.ESEC_AI                 = 4			-- 0x0004   -- ai server
KEP.ESEC_REMOTE_SERVER      = 8			-- 0x0008   -- a remote server, this allows


-- from log4cplus headers
KEP.OFF_LOG_LEVEL     = 60000
KEP.FATAL_LOG_LEVEL   = 50000
KEP.ERROR_LOG_LEVEL   = 40000
KEP.WARN_LOG_LEVEL    = 30000
KEP.INFO_LOG_LEVEL    = 20000
KEP.DEBUG_LOG_LEVEL   = 10000
KEP.TRACE_LOG_LEVEL   = 0

-- our parent object, used when logging
KEP.parent = 0

-- the dispatcher
KEP.dispatcher = 0

-- passed in from server
KEP.debugLevel = 0

-- return codes for event handling
KEP.EPR_OK				= 0	-- didn't change event
KEP.EPR_NOT_PROCESSED 	= 1 -- I don't want this event
KEP.EPR_CONSUMED		= 2 -- consumed event, don't let others process it

-- for getting C functions into the script
KEP.GET_SERVER_FN 		= "ServerGameFunctions"
KEP.GET_CLIENT_FN 		= "ClientGameFunctions"
KEP.GET_XWIN_FN 		= "XWinFunctions"
KEP.GET_MENU_FN 		= "MenuFunctions"
KEP.GET_ARENA_FN		= "ArenaFunctions"
KEP.GET_AI_FN           = "AIFunctions"
KEP.GET_PLAYER_FN		= "PlayerFunctions"
KEP.GET_SKILLDB_FN		= "SkillDBFunctions"
KEP.GET_KEPFILE_FN      = "KEPFileFunctions"
KEP.GET_KEPCONFIG_FN    = "KEPConfigFunctions"

-- instance types
KEP.CI_NOT_INSTANCED	= 0
KEP.CI_GROUP			= 1
KEP.CI_GUILD			= 2
KEP.CI_HOUSING   		= 3
KEP.CI_PERMANENT 		= 4
KEP.CI_ARENA			= 5

KEP.ArenaReady			= 0
KEP.ArenaWaiting 		= 1
KEP.ArenaTimedOut		= 2

KEP.INVALID_ZONE_INDEX  = 4293918720

-- from TextEvent.h
ChatType = {}
ChatType.Talk 			= 0	--
ChatType.Group 			= 1	--
ChatType.Clan 			= 2	--
ChatType.Private		= 3	-- just to you from one other
ChatType.Whisper 		= 4	-- talk w/ limited radius
ChatType.Shout 			= 5	-- talk w/ large radius
ChatType.GM 			= 6	-- from GameMaster
ChatType.System 		= 7	-- from System Admin
ChatType.External		= 8	-- from an external source
ChatType.ArenaMgr		= 9	-- related to an arena
ChatType.Team           = 10 -- restrict to same team
ChatType.Race           = 11 -- restrict to same race
ChatType.Battle         = 12 -- battle related, etc. damage
ChatType.S2S			= 15 -- server to server relay message

-- GetSet.AddNewMember types
KEP.amBool              = 1
KEP.amInt               = 8
KEP.amLong              = 10
KEP.amDouble            = 14
KEP.amString            = 15

-- add member flags
KEP.amfTransient            = 33554432  -- 0x02000000 -- this item is not serialized
KEP.amfReplicate            = 134217728 -- 0x08000000 -- transfer between server and client, for dynamic
KEP.amfSaveAcrossSpawn      = 268435456 -- 0x10000000 -- save across a spawn for dynamic player data
KEP.amfMask                 = 503316480 -- 0x1e000000

-- add member/find return values
KEP.amrDupeWrongType        = 4294967294 -- 0xfffffffe
KEP.amrBadType              = 4294967293 -- 0xfffffffd
KEP.amrNotFound             = 4294967295 -- 0xffffffff

-- type of damage
KEP.MISSILE				= 0
KEP.MELEE				= 1
KEP.EXPLOSION 			= 2

-- Network ID of the Server
KEP.SERVER_NETID        = 1

--triggers
KEP.TriggerEnter        = 1
KEP.TriggerExit         = 2

-- for registering filtered events
KEP.FILTER_IS_MASK      	= 1
KEP.FILTER_MATCH        	= 0
KEP.NO_FILTER           	= 0
KEP.NO_OBJID_FILTER     	= 0

-- for GotoPlaceEvent
KEP.GOTOPLACE_EVENT_NAME   = "GotoPlaceEvent"
KEP.GOTO_ZONE				= 1
KEP.GOTO_PLAYER				= 2
KEP.GOTO_PLAYERS_APT		= 3

-- for pending spawn event
KEP.PENDINGSPAWN_EVENT_NAME	= "PendingSpawnEvent"

-- For keyboard input
Keyboard = {}
Keyboard.ESC = 27
Keyboard.SPACEBAR = 32
Keyboard.PAGE_UP = 33
Keyboard.PAGE_DOWN = 34

-- for KDP
-- The world object id for the dance floor. This ID is the first kaneva dance floor
KEP.KDP1ID = 369
KEP.KDP2ID = 370
KEP.KDP3ID = 371
KEP.KDP1_EVENT = "DDREvent"
KEP.KDP1_MENU_EVENT = "DDRMenuEvent"
KEP.FILTER_START_GAME = 5
KEP.FILTER_POSTED_SCORE = 4
KEP.FILTER_END_GAME = 6
KEP.FILTER_CLOSE_DIALOG = 7
KEP.FILTER_GAME_OVER_DIALOG = 8
KEP.FILTER_READY_TO_PLAY = 9
KEP.FILTER_POST_PLAYERS_SCORES = 10
KEP.FILTER_ACCESS_DENIED = 13
KEP.FILTER_PAUSE_REQUEST = 16
KEP.FILTER_PAUSE_RESPONSE = 17

-- YES/NO box answer filters
KEP.FILTER_KDP_ANSWER_PLAY = 5


KEP.SERVERDYNOBJ_REQUEST_EVENT_NAME = "ServerDynObjRequestEvent"
KEP.SERVERDYNOBJ_REPLY_EVENT_NAME = "ServerDynObjReplyEvent"

