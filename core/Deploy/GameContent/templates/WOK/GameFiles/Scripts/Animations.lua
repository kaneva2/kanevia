--system defined animations

-- See http://wiki.intranet.kaneva.com/twiki/bin/view/Engineering/AnimationSlotMapping
-- for mapping definitions

anim_stand  = 1
anim_run    = 2
anim_walk   = 3
anum_unused = 4
anim_jump   = 5
anim_ssleft = 6
anim_ssright= 7
anim_die    = 8
anim_unused2= 9
anim_unused3= 10
anim_unused4= 11
anim_fire   = 12
anim_unused5= 13
anim_swimi  = 14
anim_swima  = 15
anim_jet    = 16
anim_pain   = 17
anim_cheer  = 70 --new: 29 + user_041 = 70, previous: 18
anim_yes    = 64 --new: 29 + user_035 = 64, previous: 19
anim_no     = 65 --new: 29 + user_036 = 65, previous: 20
anim_dance1 = 21
anim_dance2 = 22
anim_dance3 = 23
anim_wave   = 24
anim_unused5 = 25
anim_cry    = 71  -- new: 29 + user_042 = 71, previous: 26
anim_lol    = 67  -- new: 29 + user_038 = 67, previous: 27
anim_kneel  = 29  --28
anim_sit    = 29
anim_blow   = 79 -- new: 29 + user_050 = 79, previous: 30
anim_clap   = 69   -- new: 29 + user_040 = 69, previous: 31
anim_laugh  = 27--32
anim_smile  = 33
anim_surprize = 34
anim_point  = 66 -- new: 29 + user_037 = 66, previous: 35
anim_frown  = 36
anim_flirt  = 72 -- new: 29 + user_043 = 72, previous: 37
anim_rude   = 38
anim_male_rude = 75 --new: 29 + user_046 = 75, previous: 39
anim_male_techno =  113 -- new: 29 + 84 = 113, previous: 40
anim_male_disco = 111 -- new: 29 + user_082 = 111, previous: 41
anim_male_country = 110 -- new: 29 + user_081 = 110, previous: 42
anim_head_bang = 4
anim_break_dance = 10
anim_hip_hop = 9
anim_break_dance_3 = 112
anim_break_dance_2 = 114
anim_sexy_01 = 149
anim_sexy_07 = 155

anim_male_breakdance_1 = 163
anim_male_breakdance_2 = 164

anim_female_popdance_1 = 177
anim_female_popdance_3 = 179

anim_sit = 28
anim_laydown = 58

anim_country_1 = 170
anim_latin_1 = 156
anim_latin_2 = 157
anim_pop_2 = 178
anim_pop_5 = 181
anim_disco_1 = 184
anim_hiphop_2 = 192
anim_hiphop_6 = 196


-- dance game animations
anim_M_F_Idle_001  = 119
anim_M_F_Idle_002  = 120
anim_M_F_Idle_003  = 121
anim_M_F_Idle_004  = 122
anim_M_F_Idle_005  = 123
anim_M_F_Idle_006  = 124
anim_M_F_Idle_007  = 125
anim_M_F_Idle_008  = 126
anim_M_F_Flare_001 = 127
anim_M_F_Flare_002 = 128
anim_M_F_Flare_003 = 129
anim_M_F_Flare_004 = 130
anim_M_F_Flare_005 = 131
anim_M_F_Flare_006 = 132
anim_M_F_Flare_007 = 133
anim_M_F_Flare_008 = 134
anim_M_F_Flare_009 = 135


