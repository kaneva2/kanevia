SG = {}

SG.CLIP_RADIUS = "clipRadius"
SG.SOUND_FACTOR = "factor"
-- looping, 0 false, 1 true
SG.LOOPING = "looping"
--index is its list position in the editor
SG.INDEX = "index"

SG.ItemSounds = {}

sound = {}

-- Item Sounds --
-- These are indexed by the NPC id from the NPC configuration in the editor

-- Coins are NPC id 188-198 --

-- cound attributes
sound[SG.INDEX] = 9
sound[SG.LOOPING] = 0 
sound[SG.SOUND_FACTOR] = 0.03
sound[SG.CLIP_RADIUS] = 50

SG.ItemSounds[188] = sound
SG.ItemSounds[189] = sound
SG.ItemSounds[190] = sound
SG.ItemSounds[191] = sound
SG.ItemSounds[192] = sound
SG.ItemSounds[193] = sound
SG.ItemSounds[194] = sound
SG.ItemSounds[195] = sound
SG.ItemSounds[196] = sound
SG.ItemSounds[197] = sound
SG.ItemSounds[198] = sound

-- End Coins --
