GameGlobals = {}

--actors
GameGlobals.EDB_MALE          = 3
GameGlobals.EDB_FEMALE        = 4

--zones
GameGlobals.ZONE_MALL         = 0
GameGlobals.ZONE_LOGINMENU    = 1
GameGlobals.ZONE_CASINO       = 2
GameGlobals.ZONE_THEATRE      = 3
GameGlobals.ZONE_APT_01       = 4  --1bdr_big
GameGlobals.ZONE_APT_02       = 5  --studio
GameGlobals.ZONE_APT_03       = 6  --1bdr_mid
GameGlobals.ZONE_APT_LOFT     = 7  --loft apt
GameGlobals.ZONE_APT_04       = 8  --L shaped
GameGlobals.ZONE_DANCEBAR     = 9  --dance pub
GameGlobals.ZONE_PENTHOUSE    = 10 --penthouse
GameGlobals.ZONE_HANGAR       = 11
GameGlobals.ZONE_NIGHTCLUB    = 12
GameGlobals.ZONE_CONFERENCE   = 13
GameGlobals.ZONE_BBTHEATER    = 14
GameGlobals.ZONE_CAFE_SMALL   = 15
GameGlobals.ZONE_CAFE_LARGE   = 16 --kaneva cafe
GameGlobals.ZONE_APT_32x64x64 = 19

--use values
GameGlobals.UV_PORTAL_CASINO    = 100
GameGlobals.UV_PORTAL_THEATRE   = 101
GameGlobals.UV_PORTAL_PENTHOUSE = 102
GameGlobals.UV_APT_STUDIO       = 103
GameGlobals.UV_APT_1BDR         = 104
GameGlobals.UV_APT_FLAT         = 105
GameGlobals.UV_APT_LARGE1BDR    = 106
GameGlobals.UV_APT_LOFT         = 107
GameGlobals.UV_APT_PENTHOUSE    = 108
GameGlobals.UV_APT_32x64x64     = 109

--items GLID
GameGlobals.PORTAL_CASINO     = 0
GameGlobals.PORTAL_THEATRE    = 1
GameGlobals.PORTAL_PENTHOUSE  = 2 

--apartments GLID
GameGlobals.ITEM_APT_STUDIO    = 533
GameGlobals.ITEM_APT_1BDR      = 534
GameGlobals.ITEM_APT_FLAT      = 535
GameGlobals.ITEM_APT_LARGE1BDR = 536
GameGlobals.ITEM_APT_LOFT      = 537
GameGlobals.ITEM_APT_PENTHOUSE = 538
GameGlobals.ITEM_APT_32x64x64  = 1055

--weapons

--arena weapons
GameGlobals.FEMALE_HORNET      = 63
GameGlobals.MALE_HORNET        = 61

--ammo
GameGlobals.HORNET_AMMO        = 62

GameGlobals.CONFIG_FILE			= "WOKConfig.xml"
GameGlobals.CONFIG_ROOT			= "WOKConfig"

-- for asking for images
GameGlobals.THUMBNAIL			= 1
GameGlobals.FULLSIZE			= 0

-- types of thumbnails
GameGlobals.CUSTOM_TEXTURE = 1
GameGlobals.FRIEND_PICTURE = 2
GameGlobals.GIFT_PICTURE   = 3

-- inventory types
GameGlobals.IT_NORMAL = 256; -- 0x100
GameGlobals.IT_GIFT   = 512; -- 0x200

--cache some data so we dont have to query for it every time
PlayerInfo = {}
PlayerInfo.name = ""
PlayerInfo.userID = 0

-- web fiters for GetBrowserPage
WF = {}
WF.ACCEPT_FRIEND				= 1
WF.ACCEPT_GIFT					= 2
WF.ADDFRIEND					= 3
WF.COOL_PLACES					= 4
WF.DELETE_MSG					= 5
WF.FRIENDS						= 6
WF.GIFT_CATALOG					= 7
WF.IGNORE						= 8
WF.MESSAGES						= 9
WF.PLAYER_PROFILE				= 10
WF.PLAYLIST						= 11
WF.RAVE							= 12
WF.REJECT_FRIEND				= 13
WF.REJECT_GIFT					= 14
WF.TARGETED						= 15
WF.TEXTURE_LIST					= 16
WF.MESSAGE_COUNTS				= 17
WF.REPORT						= 18
WF.TRADE						= 19
WF.OBJECTINFO					= 20
WF.BLOCK                        = 21
WF.VENDOR                       = 22
WF.ZONE_ACCESS                  = 23
WF.LOC_HUD                      = 24
WF.INVENTORY                    = 25
WF.COVER_CHARGE					= 26

-- messages for actions that generate message boxes
RM = {}
RM.FRIEND_REQUEST               = "Friend request sent to "
RM.REMOVE_FRIEND                = " was removed from your friends list."
RM.ALREADY_FRIENDS              = "You are already friends with "
RM.ADD_IGNORE                   = "You will now ignore "
RM.REMOVE_IGNORE                = "You will no longer ignore "
RM.ALREADY_IGNORE               = "You are already ignoring "
RM.RAVE                         = "You raved "
RM.ALREADY_RAVED                = "You have already raved "
RM.FRIEND_REQUEST_SENT			= "You have already sent a request to "

GameGlobals.ZONE_INDEX_MALL 	= 1073741824
GameGlobals.ZONE_INDEX_COMPOUND = 1073741859
GameGlobals.ZONE_INDEX_DANCE    = 0
GameGlobals.ZONE_INDEX_KANEVACITY    = 1073741857
GameGlobals.ZONE_INDEX_PLAZA         = 1073741852
GameGlobals.ZONE_INDEX_UNDERGROUND   = 1073741850
GameGlobals.ZONE_INDEX_HELPCENTER    = 1073741855
GameGlobals.ZONE_INDEX_TBSHQ	     = 1073741853
GameGlobals.ZONE_INDEX_TNTBACKLOT    = 1073741847
GameGlobals.ZONE_INDEX_FAMILYGUY     = 1073741848
GameGlobals.ZONE_INDEX_CONFERENCE_ROOM = 1
GameGlobals.ZONE_INDEX_STORE_FRONT   = 2

GameGlobals.FEATURED_CHANNEL = "WOKMedia"

GameGlobals.INWORLD_PLAYLIST_NAME = "In-World Playlist"

dofile( "..\\Scripts\\urls.lua" )
