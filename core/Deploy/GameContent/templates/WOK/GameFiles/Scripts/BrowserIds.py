# event id

BROWSER_EVENT_NAME = "BrowserCallbackEvent"
BROWSER_REPLY_EVENT_NAME = "BrowserReplyEvent"
BROWSER_REQUEST_EVENT_NAME = "BrowserRequestEvent"

# call back types
BROWSER_KGP_LINK                = 1 # sp = sz string that has everything after scheme: (scheme passed into create), wp = 0
BROWSER_NAVIGATE_BEGIN          = 2 # sp = "",  wp = 0, can use to start throbber
BROWSER_NAVIGATE_COMPLETE       = 3 # sp = "", wp = 0, can use to stop throbber
BROWSER_UPDATE_PROGRESS         = 4 # sp = "", wp = % progress
BROWSER_STATUSTEXT_CHANGE       = 5 # sp = sz string for text, wp = 0
BROWSER_LOCATION_CHANGE         = 6 # sp = sz string or redirected URL, wp = 0

# types for the BROWSER_NAVIGATE type, add 100, just to avoid any confusion
BROWSER_FORWARD                 = 100
BROWSER_BACK                    = 101
BROWSER_STOP                    = 102
BROWSER_RELOAD                  = 103

# reply event actions from server
REPLY_REDIRECT                  = 1
REPLY_CLOSE_WINDOW              = 2

BROWSER_VALIDATION              = 1


