-- Generated by getsetscript.pl on  01/10/2007 11:22 from ..\..\..\..\..\Source\Platform\Common\XStatic\CStormClass.cpp
StormIds                           = {}
StormIds.STORMIDS                                        =  16363  -- class Id
StormIds.STORMNAME                                       =      0  -- gsCString
StormIds.DURATION                                        =      1  -- gsLong
StormIds.STORMTRANSITION                                 =      2  -- gsLong
StormIds.FREQUENCY                                       =      3  -- gsLong
StormIds.PARTICLESYSTEMREF                               =      4  -- gsInt
StormIds.STRIKEDURATION                                  =      5  -- gsLong
StormIds.TOTALSTRIKECOUNTPERMINUTE                       =      6  -- gsInt
StormIds.STRIKEPANELWIDTH                                =      7  -- gsFloat
StormIds.STRIKESPAWNHEIGHT                               =      8  -- gsFloat
StormIds.STRIKEDEPTH                                     =      9  -- gsFloat
StormIds.STORMRADIUS                                     =     10  -- gsFloat
StormIds.STRIKEVARIATIONGETSET                           =     11  -- array of objects
