-- Generated by getsetscript.pl on  01/10/2007 11:22 from ..\..\..\..\..\Source\Platform\Common\XStatic\CEnvironmentClass.cpp
EnvironmentIds                     = {}
EnvironmentIds.ENVIRONMENTIDS                            =  14859  -- class Id
EnvironmentIds.AMBIENTLIGHTREDDAY                        =      0  -- RGB color
EnvironmentIds.AMBIENTLIGHTON                            =      1  -- gsBOOL
EnvironmentIds.SUNRED                                    =      2  -- RGB color
EnvironmentIds.SUNLIGHTON                                =      3  -- gsBOOL
EnvironmentIds.SUNPOSITION                               =      4  -- x,y,x coords
EnvironmentIds.FOGRED                                    =      5  -- RGB color
EnvironmentIds.FOGON                                     =      6  -- gsBOOL
EnvironmentIds.BACKRED                                   =      7  -- RGB color
EnvironmentIds.LOCKBACKCOLOR                             =      8  -- gsBool
EnvironmentIds.FOGSTART                                  =      9  -- gsFloat
EnvironmentIds.FOGRANGE                                  =     10  -- gsFloat
EnvironmentIds.ENABLETIMESYSTEM                          =     11  -- gsBOOL
EnvironmentIds.ENVIORMENTTIMESYSTEM                      =     12  -- object
EnvironmentIds.ENABLEWINDRANDOMIZER                      =     13  -- gsBOOL
EnvironmentIds.WINDDIRECTIONX                            =     14  -- gsFloat
EnvironmentIds.WINDDIRECTIONY                            =     15  -- gsFloat
EnvironmentIds.WINDDIRECTIONZ                            =     16  -- gsFloat
EnvironmentIds.MAXWINDSTRENGTH                           =     17  -- gsFloat
EnvironmentIds.GRAVITY                                   =     18  -- gsFloat
EnvironmentIds.SEALEVEL                                  =     19  -- gsFloat
EnvironmentIds.SEAVISCUSDRAG                             =     20  -- gsFloat
