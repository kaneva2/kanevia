# Generated by getsetscript.pl on  01/10/2007 11:22 from ..\..\..\..\..\Source\Platform\Common\XStatic\CStormClass.cpp
STORMIDS                                                 =  16363  #  class Id
STORMNAME                                                =      0  #  gsCString
DURATION                                                 =      1  #  gsLong
STORMTRANSITION                                          =      2  #  gsLong
FREQUENCY                                                =      3  #  gsLong
PARTICLESYSTEMREF                                        =      4  #  gsInt
STRIKEDURATION                                           =      5  #  gsLong
TOTALSTRIKECOUNTPERMINUTE                                =      6  #  gsInt
STRIKEPANELWIDTH                                         =      7  #  gsFloat
STRIKESPAWNHEIGHT                                        =      8  #  gsFloat
STRIKEDEPTH                                              =      9  #  gsFloat
STORMRADIUS                                              =     10  #  gsFloat
STRIKEVARIATIONGETSET                                    =     11  #  array of objects
