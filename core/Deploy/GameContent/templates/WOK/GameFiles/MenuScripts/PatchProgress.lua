dofile("MenuHelper.lua")
dofile("..\\ClientScripts\\PatchHelper.lua")

function PatchProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local urlWnd = Dialog_GetStatic( gDialogHandle, "txtUrl" )
	local statusWnd = Dialog_GetStatic( gDialogHandle, "txtStatus" )
	local updateWnd = Dialog_GetStatic( gDialogHandle, "txtUpdate" )
	local sizeWnd = Dialog_GetStatic( gDialogHandle, "txtSize" )
	local subSizeWnd = Dialog_GetStatic( gDialogHandle, "txtSubSize" )
	local percentComplete = Dialog_GetStatic( gDialogHandle, "txtPercent" )
	local subPercentComplete = Dialog_GetStatic( gDialogHandle, "txtSubPercent" )

	if filter == MSG_STATUS or 
	   filter == MSG_UNPACKING or 
	   filter == MSG_UNPACKINGGZ then 
	    local url = Event_DecodeString( event )
		local status = Event_DecodeString( event )
		local totalUpdate = Event_DecodeString( event )
		local totalSize = Event_DecodeString( event )
		local totalSubSize = Event_DecodeString( event )
		local totalComplete = Event_DecodeNumber( event )
		local subComplete = Event_DecodeNumber( event )

		Static_SetText( urlWnd, "Patching "..url )
		Static_SetText( statusWnd, status )	
		Static_SetText( updateWnd, totalUpdate )
		Static_SetText( sizeWnd, totalSize )
		Static_SetText( subSizeWnd, totalSubSize )
		Static_SetText( percentComplete, tostring(totalComplete).."%")
		Static_SetText( subPercentComplete, tostring(subComplete).."%")
		
	elseif filter == MSG_INIT then 
		local url = Event_DecodeString( event )
		local totalUpdate = Event_DecodeString( event )
		local totalSize = Event_DecodeString( event )
		local totalSubSize = Event_DecodeString( event )
		local totalComplete = Event_DecodeNumber( event )
		local subComplete = Event_DecodeNumber( event )

		Static_SetText( urlWnd, "Patching "..url )
		Static_SetText( statusWnd, "" )	
		Static_SetText( updateWnd, totalUpdate )
		Static_SetText( sizeWnd, totalSize )
		Static_SetText( subSizeWnd, totalSubSize )
		Static_SetText( percentComplete, tostring(totalComplete).."%")
		Static_SetText( subPercentComplete, tostring(subComplete).."%")
	
		
	elseif filter == MSG_PATCH_HANDLER_COMPLETE then 
		Static_SetText( statusWnd, "Complete.  Launching STAR..." )	
		Static_SetText( percentComplete, "100%")
		
	elseif filter == MSG_ERROR then 
		local url = Event_DecodeString( event )
		local errNum = Event_DecodeNumber( event )
		local errMsg = "Unknown"
		if errNum == ERR_NOTCONNECTED then 
			errMsg = "Not connected"
		elseif errNum == ERR_NOVERSION then
			errMsg = "No version"
		elseif errNum == ERR_TIMEOUT then
			errMsg = "Timeout"
		end 
		
		Static_SetText( urlWnd, "Patching "..url )
		Static_SetText( statusWnd, "Error: "..errMsg )	

	elseif filter == MSG_CONNECTED then
		local url = Event_DecodeString( event )
		Static_SetText( statusWnd, "Connected" )	
		Static_SetText( urlWnd, "Patching "..url )
		
	elseif filter == MSG_RECONNECT then
		local url = Event_DecodeString( event )
		Static_SetText( statusWnd, "Lost connection" )	
		Static_SetText( urlWnd, "Patching "..url )
		
	elseif filter == MSG_VERBOSE_ERROR then
		local url = Event_DecodeString( event )
		local errNum = Event_DecodeNumber( event )
		local errMsg = Event_DecodeString( event )

		Static_SetText( statusWnd, "Error: "..errMsg.." ("..tostring(errNum)..")" )	
	
	else
		Static_SetText( statusWnd, "Unknown message of id: "..tostring(filter) )	
	end

	return KEP.EPR_CONSUMED
end 

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "PatchProgressEventHandler", 1,0,0,0, "PatchProgressEvent", KEP.HIGH_PRIO ) 
	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	
	ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "PatchProgressEvent", KEP.MED_PRIO )

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	-- TODO init here
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

