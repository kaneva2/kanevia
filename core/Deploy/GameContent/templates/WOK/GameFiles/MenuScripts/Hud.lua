dofile("MenuHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\BrowserIds.lua")
dofile("..\\Scripts\\MovementIds.lua")
dofile("..\\Scripts\\CMD.lua")
dofile("MenuLocation.lua")
dofile("CommonFunctions.lua")
dofile("CreditBalances.lua")
dofile("WebSuffix.lua")

------------------------------------------------------------------------------------------------
PROGRESSIVE = 0 -- Make me 1 to enable progressive Zone Download
------------------------------------------------------------------------------------------------

-- globals for the width and height of the 'bar indicators' (used by health and ammo)
-- it is assumed both bars have the same dimensions
g_bar_width 	= 0
g_bar_height	= 0


-- global game object	
g_game          = nil

-- global title label
g_lbhTitle      = nil
g_lbhCompass    = nil
g_lbhCharName   = nil
g_lbhhelperString = nil
g_nakedPeopleCover = nil
g_loadonce=0
g_prevCameraViewIndex = -1

--timer for updates
g_TimerEvent = 0
COMPASS_TIMER = "CompassTimer"
COMPASS_TIMER_INTERVAL = 1
g_Dispatcher = nil

HUD_EVENT = "HudEvent"

BEGIN_DOWNLOAD_EVENT = "BeginZoneDownloadEvent"

--This event come from other menus that impact credits.
--Sends a new credit total to the HUD
--UPDATE_HUD_EVENT = "UpdateHUDEvent"

-- globals for message counts and profile info



UPDATE_INTERVAL = 60

USERID_PARSE = 1
COUNT_PARSE = 2
BALANCE_PARSE = 3

g_currentUpdateCount = 0
g_newMsgCount = 0
g_friendReqCount = 0
g_channelReqCount = 0
g_newGiftCount = 0
g_raves = 0

g_web_address = GameGlobals.WEB_SITE_PREFIX

g_t_btnHandles = {}

-- Wait for config block to enable go home button or else we might get stuck
g_bEnableHome = false

g_userID = 0

g_parseType = USERID_PARSE
g_pageindex=1
g_emoteidlist={}
---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "attribEventHandler", 1,0,0,0, "AttribEvent", KEP.MED_PRIO )	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "compassUpdateHandler", 1,0,0,0, COMPASS_TIMER, KEP.LOW_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "LoadEmotesOnKey", 1,0,0,0, "LoadCurrentPageEmotes", KEP.HIGH_PRIO)
		
end


---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, COMPASS_TIMER, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1, 0, 0, HUD_EVENT, KEP.LOW_PRIO )
   	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, UPDATE_HUD_EVENT, KEP.MED_PRIO)
    --Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "EmoteAddEvent",KEP.MED_PRIO )
    Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "LoadCurrentPageEmotes",KEP.MED_PRIO ) 
	g_Dispatcher = dispatcher
	
end

--Switch the animation.
function ChangeAnimation( animation )
    if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	game = Dispatcher_GetGame(KEP.dispatcher)
	ClientEngine_SetCurrentAnimation(game, animation, 0)
end

--Loads the keyboard emotes
function LoadKeyBoardEmotes()
--Get the function if it does not yet exist.
 if ClientEngine_OpenFile == nil then
	Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
 end
 
 --Get the kep file interface
 if KEPFile_ReadLine == nil then
      Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPFILE_FN )
 end
 
 if ClientEngine_GetPlayer == nil then
	  Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
 end
 

 local t = { ClientEngine_GetPlayerInfo(g_game) }
 
 local f, errmsg = ClientEngine_OpenFile(g_game,t[11]..".cfg","r")

 if f~=0 then
 
    --Read the first line
  	ok=1 
	msg=nil
	errMsg=nil        

	--Read the number of pages that we have stored.
	ok,msg,errMsg = KEPFile_ReadLine( f )
	g_maxpages = tonumber(msg)
	ok,msg,errMsg = KEPFile_ReadLine( f )
	page=tonumber(msg)
	
	cpage=1

		
	 --Load the emote of the current page we exist out on
	 for i=1,g_maxpages do
	   for j =1,10 do
		ok,msg,errMsg = KEPFile_ReadLine( f )
		ok,msg2,errMsg = KEPFile_ReadLine( f )
		ok,msg3,errMsg = KEPFile_ReadLine( f )
		if cpage==page then
		g_emoteidlist[j]=tonumber(msg3)
		end		 
	   end
	   cpage=cpage+1		
	end
	    
	--close the file
 	KEPFile_CloseAndDelete(f)
 end
end

--Loads an emote when we press an key on the keyboard
function LoadEmotesOnKey(dispatcher, fromNetid, event, eventid, filter, objectid)
	LoadKeyBoardEmotes()
end


--Play an Specific animation index
function PlayAnimationIndex(index)

  if g_emoteidlist[index]~=-1 then
    ChangeAnimation(g_emoteidlist[index])
  end

end

---------------------------------------------------------------------
--
-- Keyboard Handler
--
---------------------------------------------------------------------

--Uses the Keys 0 to 9 to play animations.
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
	if(key-47>=1 and key-47<=10) then
        PlayAnimationIndex((key-47))	
	end	
end



function sendMessageToChat(msg)

	local hudmsgevent = Dispatcher_MakeEvent( KEP.dispatcher, HUD_EVENT )	
	Event_EncodeString( hudmsgevent, msg )
	Dispatcher_QueueEvent( KEP.dispatcher, hudmsgevent )

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	--get the functions	
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end	

	if KEPConfig_GetString == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end

	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)
	if g_game == 0 then
		g_game = nil
	end

    
    
	if g_game ~= nil then
	  g_nakedPeopleCover = gotoMenu( "BlackScreen.xml", 0, 1 )
	end
	
 	-- obtaining width of the indicator bar(s)
	local ImageHandle ih = Dialog_GetImage(gDialogHandle, "imgAmmo")
	if ih ~= 0 then
		g_bar_width	 = Control_GetWidth(ih)
		g_bar_height = Control_GetHeight(ih)
	end

	g_lbhTitle   = Dialog_GetStatic( gDialogHandle, "lblTitle")
	g_lbhCompass = Dialog_GetStatic( gDialogHandle, "lblCompass")
	g_lbhSTR     = Dialog_GetStatic( gDialogHandle, "lblSTR")
	g_lbhCharName= Dialog_GetStatic( gDialogHandle, "lblCharName")

	g_lbhhelperString = Dialog_GetStatic( gDialogHandle, "helperString" )
		
	Static_SetText( g_lbhCharName, " ")
    Static_SetText( g_lbhTitle, " ")
    Static_SetText( g_lbhhelperString, " ")
	Control_SetLocation(Static_GetControl(g_lbhhelperString), 0, -500)


  	--start the timer
	g_TimerEvent = Dispatcher_MakeEvent( g_Dispatcher, COMPASS_TIMER )
	Dispatcher_QueueEventInFuture( g_Dispatcher, g_TimerEvent, COMPASS_TIMER_INTERVAL ) 
	


	getButtonHandles()

	for i=1,10 do 
        g_emoteidlist[i]=-1 
    end
	

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	saveLocation(g_game, dialogHandle, "HUD"..PlayerInfo.name)

	--setPlayerCounts()

	Helper_Dialog_OnDestroy( dialogHandle )
end



---------------------------------------------------------------------
--
-- Go To  menu
--
---------------------------------------------------------------------
function gotoMenu(menu_path, bClose, bModal)

	dh = Dialog_Create( menu_path );
	if dh == 0 then
		Dialog_MsgBox("Menu [" .. menu_path .. "] not found.")
	end

	if bModal == 1 and dh ~= 0 then
		Dialog_SetModal(dh, 1)
		Dialog_SendToBack( gDialogHandle )
	end

	-- close this dialog
	if bClose == 1 then
		DestroyMenu(gDialogHandle)
	end

	return dh

end

---------------------------------------------------------------------
--
-- Attrib Event Handler
--
---------------------------------------------------------------------
function attribEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

	--Dialog_MsgBox(tostring(filter))

	if ClientEngine_GetPlayer == nil then
 		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
 	end
 	
 	if filter == 19 then
 	    -- biggs 05/10/07
 	    -- Filter 19 is the first attribute to come from the C code,
 	    -- added a call to ClientEngine_BindActorsCamera
		if g_nakedPeopleCover ~= nil then
			Dialog_Destroy(g_nakedPeopleCover)
			g_nakedPeopleCover = nil
			
			g_prevCameraViewIndex = ClientEngine_GetActorsCameraIndex(g_game)

   			if (g_prevCameraViewIndex ~= -1) then
 	   			ClientEngine_BindActorsCamera(g_game, 1)
			end
		end
	-- character name
	elseif filter == 21 then
		-- set the player name and show it
		local name	= Event_DecodeString( event )
		if g_lbhCharName ~= 0 then
		  	if name ~= nil then
		  	    PlayerInfo.name = name
		  	    loadLocation(g_game, gDialogHandle, "HUD"..PlayerInfo.name)
		   		Static_SetText(g_lbhCharName, PlayerInfo.name)

		   		g_bEnableHome = true
			else
		   		Static_SetText(g_lbhCharName, "loading...")
			end
			Control_SetVisible(g_lbhCharName, true)
		end

        if (g_prevCameraViewIndex ~= -1) then
 	    	ClientEngine_BindActorsCamera(g_game, g_prevCameraViewIndex)
		end

		
	-- health
	elseif filter == 3 or filter == 8 then
		local ratio		= 1
		local cur_energy 	= 0
		local max_energy 	= 0

		if filter == 3 then
			cur_energy 	= Event_DecodeNumber( event )
			local uniInt2 	= Event_DecodeNumber( event )
			max_energy 	= Event_DecodeNumber( event )
		elseif filter == 8 then
			local uniInt1 	= Event_DecodeNumber( event )
			cur_energy 	= Event_DecodeNumber( event )			
			max_energy 	= Event_DecodeNumber( event )

		end

		if cur_energy < 0 then
			cur_energy = 0
		end

		local StaticHandle sh	= Dialog_GetStatic(gDialogHandle, "lblHealth")
		local ImageHandle ih 	= Dialog_GetImage(gDialogHandle, "imgHealth")
		if sh ~= 0 and ih ~= 0 then
			-- Static_SetText(sh, tostring(cur_energy) .. " / " .. tostring(max_energy) )
			if max_energy ~= 0 then
				ratio = cur_energy / max_energy
				Control_SetVisible(sh, true)
			else
				Control_SetVisible(sh, false)
			end

			local width = g_bar_width * ratio
			Control_SetSize(ih, width, g_bar_height)			
		end

	-- ammo
	elseif filter == 27 then
		local ratio	= 1
		local cur_ammo 	= 0
		local max_ammo 	= 0

		max_ammo 	= Event_DecodeNumber( event )
		cur_ammo 	= Event_DecodeNumber( event )

		if cur_ammo < 0 then
			cur_ammo = 0
		end

		local StaticHandle sh	= Dialog_GetStatic(gDialogHandle, "lblAmmo")
		local ImageHandle ih	= Dialog_GetImage(gDialogHandle, "imgAmmo")
		if sh ~= 0 and ih ~= 0 then
			-- Static_SetText(sh, tostring(cur_ammo) .. " / " .. tostring(max_ammo) )
			if max_ammo ~= 0 then
				ratio = cur_ammo / max_ammo
				Control_SetVisible(sh, true)
			else
				Control_SetVisible(sh, false)
			end
 	
			local width = g_bar_width * ratio
			Control_SetSize(ih, width, g_bar_height)		
		end
	end

	--For whatever reason this function get calls multiple times,
	if g_loadonce==0 then
        LoadKeyBoardEmotes()
        g_loadonce=1
 	end
end

----------------------------------------------------------------------------------------
-- Compass Timer Event
----------------------------------------------------------------------------------------
function compassUpdateHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

  	if ClientEngine_GetPlayer == nil then
 		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
 	end

  --get the player info for the title incase it changed
	local t = { ClientEngine_GetPlayerInfo(g_game) }
	if t ~= 0  then
		local num_args = #t
		-- index 12 = Title
		temp = t[12+1]
		if temp ~= nil then
       		Static_SetText(g_lbhTitle, temp)
    	end
    	
		-- index 14 = helperString
		if t[14+1] ~= nil then
			temp = " " .. t[14+1] .. " "
			Static_SetText( g_lbhhelperString, temp)
		end
		if temp ~= "  " then
			--GKF should check the position of the menu, if the Y < 100 it should be 188, 87 otherwise
			--should be 188, -87
			Control_SetLocation(Static_GetControl(g_lbhhelperString), 188, -87)
		else
			Control_SetLocation(Static_GetControl(g_lbhhelperString), 0, -500)
		end
    	
		-- index 15 = Compass
		temp= t[15+1]
		if temp ~= nil then
       		Static_SetText(g_lbhCompass, temp)
    	end

    	-- index 8 = Player cash
	end	

	Dispatcher_QueueEventInFuture( dispatcher, g_TimerEvent, COMPASS_TIMER_INTERVAL ) 
 
end

----------------------------------------------------------------------------------------
-- Button Handlers
----------------------------------------------------------------------------------------
function btnTravel_OnButtonClicked( buttonHandle )

	if g_game ~= nil then
		g_dropdownmenu = gotoMenu( "wok_template_travel.xml", 0, 1 )
	end	

end

function btnProfile_OnButtonClicked( buttonHandle)
	if g_game ~= nil then 
		gotoMenu( "profile.xml", 0, 1 )
	end
end

function btnInventory_OnButtonClicked( buttonHandle )
	if Dialog_IsDialogOpenByName("inventory.xml") == 0 then
		if g_game ~= nil then 
			gotoMenu( "inventory.xml", 0, 1 )
		end
	end
end


function btnEmotes_OnButtonClicked( buttonHandle )
	if Dialog_IsDialogOpenByName("wok_template_emotes.xml") == 0 then
		if g_game ~= nil then 
			gotoMenu( "wok_template_emotes.xml", 0, 0 )
		end
	end
end

--Change camera view
function btnView_OnButtonClicked( buttonHandle )

	--This function will toggle the camera between first, "second", and 
	--third person views 
	local result = 0
	
	if ClientEngine_ChangeActorsCamera ~= nil then
		result = ClientEngine_ChangeActorsCamera( g_game, 1 )
	end
	
	if result ~= 1 then
		Dispatcher_LogMessage( g_dispatcher, KEP.DEBUG_LOG_LEVEL, "ChangeActorsCamera failed..." )
	end
end


function btnPreferences_OnButtonClicked( buttonHandle )

	--open the preferneces dialog
	if g_game ~= nil then
		gotoMenu( "Preferences.xml", 0, 1 )
	end

end

function btnHelp_OnButtonClicked( buttonHandle )
	if g_game ~= nil then 
		gotoMenu( "help.xml", 0, 1 )
	end
end

function btnSettings_OnButtonClicked( buttonHandle )
	--if g_dropdownmenu == nil then
		if g_game ~= nil then
			g_dropdownmenu = gotoMenu( "main_menu.xml", 0, 1 )
		end
	--end
end



function btnQuit_OnButtonClicked( buttonHandle )
	-- load the game functions
	if ClientEngine_GetPlayer == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	ClientEngine_SpawnToRebirthPoint( Dispatcher_GetGame( KEP.dispatcher) )
	DestroyMenu(gDialogHandle)
end


function btnStarTravel_OnButtonClicked(buttonHandle)

	if g_game ~= nil then 
		gotoMenu( "StarTravel.xml", 0, 1 )
	end

end


function btnHome_OnButtonClicked(buttonHandle)
	if ClientEngine_GetPlayer == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	if g_bEnableHome then
	
		event = Dispatcher_MakeEvent( KEP.dispatcher, "TextEvent" )
		g_game = Dispatcher_GetGame(KEP.dispatcher)
		local player = ClientEngine_GetPlayer( g_game )
		local playerId = GetSet_GetNumber( player, MovementIds.NETWORKDEFINEDID )
	
		Event_EncodeNumber( event, playerId )
		Event_EncodeString( event, "/GOHOME")
		Event_EncodeNumber( event, ChatType.Talk )
		Event_SetFilter( event, CMD.GOHOME )
		Event_AddTo( event, KEP.SERVER_NETID )
		
		Dispatcher_QueueEvent( KEP.dispatcher, event )
		
		g_bEnableHome = false
		
	end
	
	if buttonHandle ~= nil then
		ch = Button_GetControl(buttonHandle)
		if ch ~= nil then
			Control_SetEnabled(ch, false)
		end
	end
	
	
end	

---------------------------------------------------------------------
-- Mouse Over
---------------------------------------------------------------------
function getButtonHandles()
--g_t_btnHandles


	local ch = Dialog_GetControl(gDialogHandle, "btnTravel")
	if ch ~= nil then
     	g_t_btnHandles["btnTravel"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnEmotes")
	if ch ~= nil then
     	g_t_btnHandles["btnEmotes"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnInventory")
	if ch ~= nil then
     	g_t_btnHandles["btnInventory"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnView")
	if ch ~= nil then
     	g_t_btnHandles["btnView"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnHome")
	if ch ~= nil then
     	g_t_btnHandles["btnHome"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSettings")
	if ch ~= nil then
     	g_t_btnHandles["btnSettings"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnPreferences")
	if ch ~= nil then
     	g_t_btnHandles["btnPreferences"] = ch
	end




end



