dofile("MenuHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")

-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--
-- end 

g_dispatcher = 0

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	
	-- TODO register any of your handlers here like the following....
	-- ParentHandler_RegisterEventHandler( dispatcher, handler, "renderTextHandler", 1,0,0,0, "RenderTextEvent", KEP.HIGH_PRIO ) 

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	-- TODO register any new event types here like the following...
	-- ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "LuaEvent", KEP.MED_PRIO )	
	
	g_dispatcher = dispatcher	
end



---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	
	
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end	

	-- TODO init here
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	if g_game == 0 then
		g_game = nil
	end
	
	if KEPFile_ReadLine == nil then 
      Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPFILE_FN )
    end
    

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here

	Helper_Dialog_OnDestroy( dialogHandle )
end


---------------------------------------------------------------------
--
-- Cancel Button - cancel and discard changes
--
---------------------------------------------------------------------
function btnCancel_OnButtonClicked( buttonHandle )
  
	-- close this dialog
	DestroyMenu(gDialogHandle)
	
end

---------------------------------------------------------------------
--
-- Travel to the Store Front
--
---------------------------------------------------------------------
function btnGotoStorefront_OnButtonClicked( buttonHandle )

	local ev = Dispatcher_MakeEvent( KEP.dispatcher, KEP.GOTOPLACE_EVENT_NAME )
	Event_SetFilter( ev, KEP.GOTO_ZONE ) -- We are going to a zone
	Event_EncodeNumber( ev, GameGlobals.ZONE_INDEX_STORE_FRONT )
	Event_EncodeNumber( ev, nil  )
	Event_EncodeNumber( ev, -1 ) -- selected character is unknown now
	Event_AddTo( ev, KEP.SERVER_NETID )
	Dispatcher_QueueEvent( KEP.dispatcher, ev )
	DestroyMenu(gDialogHandle)

end

---------------------------------------------------------------------
--
-- Travel to the Conference Room
--
---------------------------------------------------------------------
function btnGotoConferenceRoom_OnButtonClicked( buttonHandle )

	local ev = Dispatcher_MakeEvent( KEP.dispatcher, KEP.GOTOPLACE_EVENT_NAME )
	Event_SetFilter( ev, KEP.GOTO_ZONE ) -- We are going to a zone
	Event_EncodeNumber( ev, GameGlobals.ZONE_INDEX_CONFERENCE_ROOM  )
	Event_EncodeNumber( ev, nil  )
	Event_EncodeNumber( ev, -1 ) -- selected character is unknown now
	Event_AddTo( ev, KEP.SERVER_NETID )
	Dispatcher_QueueEvent( KEP.dispatcher, ev )
	DestroyMenu(gDialogHandle)
  
end


---------------------------------------------------------------------
-- Keyboard Handler
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
      -- ESC key
      if key == 27 then
            -- close this menu
            DestroyMenu(gDialogHandle)
      end
end



