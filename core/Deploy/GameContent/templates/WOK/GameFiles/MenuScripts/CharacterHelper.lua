dofile("..\\Scripts\\GameGlobals.lua")
dofile("WebSuffix.lua")

g_EnableCharModify = false
g_CreateDefaultChar = true

function CharacterHelperInitializeKEPEvents( dispatcher, handler, debugLevel )
end	

function CharacterHelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel, prio )	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "stringListHandler", 1,0,0,0, "StringListEvent", prio )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "ModifyCharEventHandler", 1,0,0,0, "ModifyCharEvent", KEP.HIGH_PRIO )
	
end	

function GetAttribCRC( attribute, uniInt, uniInt2 )
	genCRC = 0

	genCRC = attribute +  uniInt +  uniInt2

	while (math.abs(genCRC) > 30000) do
		genCRC = genCRC / 10
	end

	finalValue = genCRC;

	return finalValue;
end 

---------------------------------------------------------------------
--
-- create a default character using given info
--
---------------------------------------------------------------------
function CreateDefaultCharacter()

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	if Dialog_Create == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_MENU_FN )
	end

	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	if g_game == 0 then
		g_game = nil
	end

	local gsGame = Dispatcher_GetGameAsGetSet( KEP.dispatcher );
	
	local name = GetSet_GetString( gsGame, ClientEngineIds.LOGINNAME )
	local gender = GetSet_GetString( gsGame, ClientEngineIds.LOGINGENDER )
	
	local actorID = GameGlobals.EDB_FEMALE
	if gender ~= "F" then 
		g_gender = GameGlobals.EDB_MALE
	end

	local IdleAnimationID = 1 -- for cw setting to 1
-- Dialog_MsgBox( "Creating default char of gender "..gender )	
	g_menu_game_obj = ClientEngine_AddMenuGameObject(g_game, actorID, IdleAnimationID)
	if g_menu_game_obj  ~= nil then
		-- TODO create random alt configs for male/female
		-- ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, HeadConfigID[g_gender], g_t_head[g_cur_head_index], g_cur_skin_color_index)
		
-- Dialog_MsgBox( "First step ok" )	
		-- TODO set variation in size (100,100,100)
		if ClientEngine_CreateCharacter(g_game, g_menu_game_obj, 0, name, 0, 100, 100, 100 ) ~= 0 then
			ClientEngine_RemoveMenuGameObject(g_game, g_menu_game_obj)
-- Dialog_MsgBox( "Calling get characters again" )	
			GetCharacters() -- if gets 1+ back will play it
		end
		
	end 
end 

---------------------------------------------------------------------
--
-- after login, get the list of characters
--
---------------------------------------------------------------------
function GetCharacters()

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	if g_game == 0 then
		g_game = nil
	end

	event = Dispatcher_MakeEvent( KEP.dispatcher, "GetAttributeEvent" )

	if event ~= nil then

		Event_EncodeNumber( event, 14 ) -- get character list
		Event_EncodeNumber( event, 50 )
		Event_EncodeNumber( event, 0 )
		Event_EncodeNumber( event, 0 )
		Event_EncodeNumber( event, 0 )
		Event_EncodeNumber( event, GetAttribCRC( 14, 50, 0 ) ) -- GetAttribCRC( attribute, uniInt, uniInt2 );
		Event_AddTo( event, 1)		-- send to server

		if g_game ~= nil then

			Dispatcher_QueueEvent( KEP.dispatcher, event )

		end

	end 
end


function ModifyCharEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

	local doCharMod = Event_DecodeNumber( event )
	if doCharMod == 1 then
		g_EnableCharModify = true
	end

	return KEP.EPR_CONSUMED

end

---------------------------------------------------------------------
--
-- String List Handler (Character list)
--
---------------------------------------------------------------------
function stringListHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

	local ListBoxHandle lbh = Dialog_GetControl(gDialogHandle, "lstCharacters")

	local count = Event_DecodeNumber( event )
	local userInt = Event_DecodeNumber( event)
	local sendingTwoStrings = true
	local charEdb = 2;

-- Dialog_MsgBox( "objectid is "..tostring(objectid))
	-- new version sends edb, too if objectid = 1
	if objectid ~= nil then
		if objectid == 1 then
			sendingTwoStrings = true
		end
	end
-- Dialog_MsgBox( "sending 2 is "..tostring(sendingTwoStrings))
	-- 50 is the logon response/list of characters
	if (userInt == 50) then
		count = count - 1

		for i=0, count do
			local character = Event_DecodeString( event)
			ListBox_AddItem(lbh, character, 0);
			if sendingTwoStrings then
				i = i + 1
				local s = Event_DecodeString( event)
				charEdb = tonumber(s)
			end
		end

		g_num_items = count + 1

		-- for WOK, if they have one, use it
		if (g_num_items) > 0 then

			-- close this dialog
			DestroyMenu(gDialogHandle)

			if g_EnableCharModify then
-- Dialog_MsgBox( "SL 3 charEdb is "..tostring(charEdb).." sending2 is "..tostring(sendingTwoStrings).." sending to char creation g_enable is "..tostring(g_EnableCharModify))
				Dialog_Create( "CharacterCreation.xml");
			else
-- Dialog_MsgBox( "SL 4 ////going to play char")
				
				if g_game ~= nil then
					ClientEngine_PlayCharacter(g_game, 0) -- 0 is index
				end
			end
			
		elseif g_CreateDefaultChar then 
-- Dialog_MsgBox( "SL 4.5 ////creating default char")
			CreateDefaultCharacter( )
		else -- allow them to create one use
		
-- Dialog_MsgBox( "SL 5 ////goingto char creation")
			-- close this dialog
			DestroyMenu(gDialogHandle)

			Dialog_Create( "CharacterCreation.xml");
			
		end
	end

	return KEP.EPR_CONSUMED	
end


