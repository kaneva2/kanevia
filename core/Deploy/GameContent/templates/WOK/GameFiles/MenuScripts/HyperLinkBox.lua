dofile("MenuHelper.lua")

-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--
-- end 

g_url = nil
g_game = nil

HYPER_LINK_BOX_EVENT = "HyperLinkBoxEvent"

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "HyperLinkBoxEventHandler", 1,0,0,0, HYPER_LINK_BOX_EVENT, KEP.MED_PRIO )	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, HYPER_LINK_BOX_EVENT, KEP.MED_PRIO )
end

function HyperLinkBoxEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	message = Event_DecodeString( event )
	buttontext = Event_DecodeString( event )
	local url = Event_DecodeString( event )
	if url ~= nil then
		g_url = url
	end 
 	sh = Dialog_GetStatic( gDialogHandle, "lblMessage" )
	Static_SetText( sh, message )
	bs = Button_GetStatic(Dialog_GetButton(gDialogHandle, "btnLink" ))
	Static_SetText(bs, buttontext)
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	g_game = Dispatcher_GetGame(KEP.dispatcher)	
end

---------------------------------------------------------------------
-- Button Handler
---------------------------------------------------------------------

function btnLink_OnButtonClicked( buttonHandle)
	if g_url ~= nil and g_game ~= nil then
		ClientEngine_LaunchBrowser( g_game, g_url )
	end
	DestroyMenu(gDialogHandle)
end

function btnClose_OnButtonClicked( buttonHandle )
	DestroyMenu(gDialogHandle)	
end

---------------------------------------------------------------------

-- Keyboard Handler

---------------------------------------------------------------------

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)

      -- ESC key
      if key == 27 then
	        -- close this menu
            DestroyMenu(gDialogHandle)
      end
end


---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

