dofile("MenuHelper.lua")
dofile("..\\Scripts\\BrowserIds.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("PageButtons.lua")
dofile("MenuLocation.lua")
dofile("CommonFunctions.lua")


WEB_ADDRESS = GameGlobals.WEB_SITE_PREFIX.."kgp/friendsList.aspx?game=true"


FRIEND_SELECTION_EVENT = "FriendSelectionEvent"
FRIEND_SELECTED_SELECTED = "FriendSelectedEvent"

g_game = nil
g_kanevaUserId = -1
g_friendid = -1
g_objId = -1


-- Hold current friend data returned from server
g_message = ""
-- Total number of friend records
g_totalNumRec = 0
-- Maximum number of places for server to return in one request
-- places per page
g_friendsPerPage = 10

--[[
-- Current page of data
g_curPage = 1
-- Number of pages matching buttons between previous and next arrows
-- See menu specs
g_window = 4
-- Which set of I am on (i.e. 1 would be pages 1,2,3,4; 2 would be 5,6,7,8
g_set = 1
]]--

g_t_friends = {} 
g_friends_count = 0
g_selected_friend = -1
g_display_friend = -1

--[[
-- Maximum page number with data
g_pageCap = 0
-- Maximum g_window size set we can have
g_setCap = 0
]]--

function friendSelectionEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )  
  
	g_kanevaUserId = Event_DecodeNumber( event )	
	
	Dispatcher_LogMessage( g_dispatcher, KEP.ERROR_LOG_LEVEL, "opening friend db of user "..g_kanevaUserId )
	
  -- set url
	local sh = Dialog_GetStatic(gDialogHandle, "ctrlBrowser")
	if sh ~= 0 then
		Static_SetText(sh, GameGlobals.WEB_SITE_PREFIX.. "kgp/FriendList.aspx?userId="..g_kanevaUserId)
	end	
end

---------------------------------------------------------------------
--
-- Cancel Button - cancel and discard changes
--
---------------------------------------------------------------------
function btnCancel_OnButtonClicked( buttonHandle )    
  
	-- close this dialog
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------
--
-- Save Button - save changes
--
---------------------------------------------------------------------
function btnSave_OnButtonClicked( buttonHandle )
  	Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "saving" ) 	
  	local friendid = g_t_friends[g_selected_friend + 1]["userID"]
	if friendid ~= nil then
	  	friendSelectedEvent = Dispatcher_MakeEvent( KEP.dispatcher, FRIEND_SELECTED_SELECTED )
		Event_EncodeNumber( friendSelectedEvent, friendid)
		Dispatcher_QueueEvent( KEP.dispatcher, friendSelectedEvent )
		DestroyMenu(gDialogHandle)
	end
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "friendSelectionEventHandler", 1,0,0,0, FRIEND_SELECTION_EVENT, KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "BrowserPageReadyHandler", 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	if Dialog_Create == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
	end

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, FRIEND_SELECTED_SELECTED, KEP.MED_PRIO )	
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, FRIEND_SELECTION_EVENT, KEP.MED_PRIO )
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	
	-- TODO init here
  	g_game = Dispatcher_GetGame(KEP.dispatcher)

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, WEB_ADDRESS, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end
	
	if KEPConfig_GetString == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end		
	
	clear()
	-------------------------------remove me----------------
	--local c = Static_GetControl(Dialog_GetStatic( gDialogHandle, "lblQuery" ))
	--Control_SetVisible(c, true)
	---------------------------------------------------------
	
	loadLocation(g_game, dialogHandle, "FriendSelect")
	
	initButtons()
	
	-- setSelectionColor is called from PageButtons.lua to highlight the first
	-- button when the menu is opend.
	local bh = Dialog_GetButton(dialogHandle, "btnRange1")
	if bh ~= nil then
		setSelectionColor( bh )
	end
	
end

function HelperPowTwo(num, tar)
	if num >= tar then
		return num
	else
		return HelperPowTwo(num * 2, tar)
	end
end

function LeastPowerofTwo(tar)
	return HelperPowTwo(2, tar)
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	saveLocation(g_game, dialogHandle, "FriendSelect")	
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_message = Event_DecodeString( event )
	--local sh = Dialog_GetStatic( gDialogHandle, "lblQuery" )
	--Static_SetText(sh, g_message)
    ParseFriendData(gDialogHandle)
	showThumbnails()
	return KEP.EPR_OK
end
								  
function ParseFriendData(dialogHandle)

	local s = 0     			-- start of substring
	local numRec = 0   		-- number of records in this chunk
	local strPos = 0    		-- current position in string to parse
	local result = ""  		-- return string from find temp

	local name = "" 	-- username
	local playerID = 0    -- numerical id of player
	local glueDate = "" -- date user became friend
	local glueTime = "" -- time user became friend
	local online = ""   -- online status of user
	local parseStr = ""  -- all parsed data (probably ditch later when using list box)
	local lastOnline = "" -- last time user was online
	local imageUrl = DEFAULT_AVATAR_PIC_PATH -- Path of our friend thumbnail image
	
	local parseStr = ""
	local sh = Dialog_GetStatic( gDialogHandle, "lblQuery" )
	s, e, result = string.find(g_message, "<ReturnCode>([0-9]+)</ReturnCode>")



	if result == "0" then
	
	    numRec = calculateRanges()

		g_t_friends = {}
		g_friends_count = 0
		
		for i=1, numRec do
			local friendstr = ""
			local trash = 0
			local s = 0
			s, strPos, friendstr = string.find(g_message, "<Friend>(.-)</Friend>", strPos)
			s, trash, name = string.find(friendstr, "<username>(.-)</username>", 1)
			s, trash, playerID = string.find(friendstr, "<player_id>([0-9]+)</player_id>", 1)
			s, trash, userID = string.find(friendstr, "<user_id>([0-9]+)</user_id>", 1)
			s, trash, glueDate, glueTime = string.find(friendstr, "<glued_date>(%w+%-%w+%-%w+)T(%w+:%w+:%w+)", 1)
  			s, trash, online = string.find(friendstr, "<online>(%w+)</online>", 1)
 			s, trash, imageUrl = string.find(friendstr, "<thumbnail_medium_path>(.-)</thumbnail_medium_path>", 1 )


			local pal = {}
			pal["name"] = name
			pal["playerID"] = playerID
			pal["userID"] = userID
			pal["glueDate"] = prettyDateFormat(glueDate)
			pal["glueTime"] = prettyTimeFormat(glueTime)
			if online == "T" then
				pal["online"] = "online"
			else
				pal["online"] = "offline"
			end
			
            pal["thumbnailFile"] = nil
			local full_path = ""
			
			if not imageUrl then
				imageUrl = DEFAULT_AVATAR_PIC_PATH
			end
			
			full_path = ClientEngine_DownloadTexture( g_game, GameGlobals.FRIEND_PICTURE, userID, imageUrl, GameGlobals.THUMBNAIL )
			
			-- Strip off begnning of file path leaving only file name
			pal["thumbnailFile"] = string.gsub(full_path,".-\\", "" )			
			
			table.insert(g_t_friends, pal)
			g_friends_count = g_friends_count + 1
			
			-- Pull me out to timer for responsiveness.
			--ClientEngine_ApplyFriendPicture(g_game, -1, userID, GameGlobals.THUMBNAIL )
			if i == 1 then
				g_selected_friend = 0
				g_display_friend = 0
			    selectTexture(0)
			end
		end
		
		alignButtons()
 	end
end

-- Used to fill the buttons with the correct numerical text
function calculateRanges()

	local numRec = 0  --number of records returned
	local s, e  -- start and end indices of found string

	local results_h = Dialog_GetStatic( gDialogHandle, "lblResults")

	local text = "Showing "

    local high = g_curPage * g_friendsPerPage
	local low = ((g_curPage - 1) * g_friendsPerPage) + 1


	s, e, g_totalNumRec = string.find(g_message, "<TotalNumberRecords>([0-9]+)</TotalNumberRecords>")
	s, e, numRec = string.find(g_message, "<NumberRecords>([0-9]+)</NumberRecords>")


	g_totalNumRec = tonumber(g_totalNumRec)

	g_pageCap = math.ceil(g_totalNumRec / g_friendsPerPage)
	g_setCap = math.ceil(g_pageCap / g_window)

	if high > g_totalNumRec then
        high = g_totalNumRec
	end


    if numRec == "0" then
        low = numRec
    end


	if totalRec_h ~= nil then
	   Static_SetText(totalRec_h, g_totalNumRec)
	end
	
	if results_h ~= nil then
		text = text..low.." - "..high.." of "..g_totalNumRec
		Static_SetText(results_h, text)		
	end
	
	if g_InitBtnText then
	    setButtonText()
	    g_InitBtnText = false
	end
	
	return numRec

end


function prettyDateFormat(datestr)
	local year = string.sub(datestr, 1, 4)
	local month = string.sub(datestr, 6, 7)
	local day = string.sub(datestr, 9, 10)
	local monthname = "January"
	if month == "01" then
		monthname = "January"
	elseif month == "02" then
		monthname = "Feburary"
	elseif month == "03" then
		monthname = "March"
	elseif month == "04" then
		monthname = "April"
	elseif month == "05" then
		monthname = "May"
	elseif month == "06" then
		monthname = "June"
	elseif month == "07" then
		monthname = "July"
	elseif month == "08" then
		monthname = "August"
	elseif month == "09" then
		monthname = "September"
	elseif month == "10" then
		monthname = "October"
	elseif month == "11" then
		monthname = "November"
	elseif month == "12" then
		monthname = "December"
	end
	return monthname .. " " .. day .. ", " .. year
end

function prettyTimeFormat(timestr)
	local hour = string.sub(timestr, 1, 2)
	local min = string.sub(timestr, 4, 5)
	local ampm = ""
	if tonumber(hour) == 0 then
		hour = 12
		ampm = "AM"
	elseif tonumber(hour) < 12 then
		hour = tonumber(hour)
		ampm = "AM"
	else
		hour = tonumber(hour) - 12
		ampm = "PM"
	end
	return hour .. ":" .. min .. " " .. ampm
end

function showThumbnails()
	clear()
	if g_t_friends ~= nil then
		for i = 0, g_friends_count - 1 do
		
			ih = Dialog_GetImage(gDialogHandle, "imgThumb"..(i + 1))
			ihBack = Dialog_GetImage(gDialogHandle, "imgThumbBack"..(i + 1))
     			 fileName = "../../CustomTexture/".. g_t_friends[i + 1]["thumbnailFile"]

			--displayBrowserThumbnail(ih, g_t_friends[i + 1]["thumbnailFile"])
			 scaleImage(ih,ihBack,fileName)
			sh = Dialog_GetStatic(gDialogHandle, "lblThumb" .. i + 1)

			Static_SetText(sh, g_t_friends[i + 1]["name"])

		end
	end
end

function clear()
	selectTexture(-1)
	for i = 0, 10 do
		Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgThumb"..(i + 1))), false)
		Static_SetText(Dialog_GetStatic(gDialogHandle, "lblThumb"..(i + 1)), "")
	end
	-- Static_SetText(Dialog_GetStatic( gDialogHandle, "lblQuery" ), "")
end

function Dialog_OnMouseMove( dialogHandle, x, y )
	if g_selected_friend < 0 then
		local found = false
		for i = 0, g_friends_count - 1 do
			ch = Image_GetControl(Dialog_GetImage(gDialogHandle, "imgThumb"..(i + 1)))
			ch2 = Static_GetControl(Dialog_GetStatic(gDialogHandle, "lblThumb"..(i + 1)))
			if Control_ContainsPoint(ch, x, y) ==  1 or Control_ContainsPoint(ch2, x, y) == 1 then
				displayTexture(i + 1, false)
				found = true
			end
		end
		if found == false then
			displayTexture(-1, false)
		end
	end
	--	displayTexture(g_selected_friend)
	--end
end

function displayTexture(index, overwrite)
	if g_selected_friend >= 0 and overwrite == false then
		return
	end
	if index < 0 then
		Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgMain")), false)
	end
	if g_display_friend ~= index then
		if index < 0 then
			g_display_friend = -1
			Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgMain")), false)
		else
			Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgMain")), true)
			g_display_friend = index

			local ihFore = Dialog_GetImage(gDialogHandle, "imgMain")
			local ihBack = Dialog_GetImage(gDialogHandle, "imgTextureBackground")

			--eh = Image_GetDisplayElement(Dialog_GetImage(gDialogHandle, "imgMain"))
			local fileName = "../../CustomTexture/".. g_t_friends[index]["thumbnailFile"]
			--local filename = "../../CustomTexture/".. g_t_friends[index]["thumbnailFile"]
			--Element_AddTexture(eh, gDialogHandle, filename)
			scaleImage(ihFore,ihBack,fileName)
			Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgTextureBackground")), false)
			--local r = Element_GetTextureWidth(eh, gDialogHandle)
			--local b = Element_GetTextureHeight(eh, gDialogHandle)
			--r = LeastPowerofTwo(r)
			--b = LeastPowerofTwo(b)
			--Element_SetCoords(eh, 0, 0, r, b)
		end
		if index > 0 and g_friends_count > 0 then
			Static_SetText(Dialog_GetStatic(gDialogHandle, "txtSelected"), g_t_friends[g_display_friend]["name"] )
			sh = Dialog_GetStatic( gDialogHandle, "lblQuery" )
			local metadata = "Name: ".. g_t_friends[g_display_friend]["name"] .. "\n"
			metadata = metadata .. "Friend Since: " .. g_t_friends[g_display_friend]["glueDate"] .. " - " .. g_t_friends[g_display_friend]["glueTime"] .."\n"
			metadata = metadata .. "Status: " .. g_t_friends[g_display_friend]["online"] .. "\n"
			Static_SetText(sh, metadata)
		else
			Static_SetText(Dialog_GetStatic(gDialogHandle, "txtSelected"), "None")
			--Static_SetText(Dialog_GetStatic( gDialogHandle, "lblQuery" ), "")
		end
	end
end

function selectTexture(index)
	g_selected_friend = index
	if index >= 0 then
		local ypos = Control_GetLocationY(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgThumbBack"..(index + 1))))
		local xpos = Control_GetLocationX(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgThumbBack"..(index + 1))))
		ch = Image_GetControl(Dialog_GetImage(gDialogHandle, "imgBackSelected"))
		Control_SetLocation(ch, xpos, ypos)
		Control_SetVisible(ch, true)
		displayTexture(index + 1, true)
	else
		ch = Image_GetControl(Dialog_GetImage(gDialogHandle, "imgBackSelected"))
		Control_SetVisible(ch, false)
	end

end


function clickSelect(x , y)
	local found = false
	for i = 0, g_friends_count - 1 do
		ch = Image_GetControl(Dialog_GetImage(gDialogHandle, "imgThumb"..(i + 1)))
		ch2 = Static_GetControl(Dialog_GetStatic(gDialogHandle, "lblThumb"..(i + 1)))
		if Control_ContainsPoint(ch, x, y) ==  1 or Control_ContainsPoint(ch2, x, y) == 1 then
			selectTexture(i)
			found = true
		end
	end
end

function imgThumb1_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function imgThumb2_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function imgThumb3_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function imgThumb4_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function imgThumb5_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function imgThumb6_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function imgThumb7_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function imgThumb8_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function imgThumb9_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function imgThumb10_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb1_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb2_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb3_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb4_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb5_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb6_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb7_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb8_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb9_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb10_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end

function btnResultsPrev_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

	    calcResultsPrev()

        alignButtons()
        
		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, WEB_ADDRESS, WF.FRIENDS, g_curPage, g_friendsPerPage)
		end 
	end
	clear()
end

function btnResultsNext_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

        calcResultsNext()

        alignButtons()
        
		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, WEB_ADDRESS, WF.FRIENDS, g_curPage, g_friendsPerPage)
		end
	end
	clear()
end

function btnFirst_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

		calcFirst()

        alignButtons()
        
		if (g_game ~= nil) then
			ClientEngine_GetBrowserPage(g_game, WEB_ADDRESS, WF.FRIENDS, g_curPage, g_friendsPerPage)
		end

	end
		
	clear()
end

function btnLast_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

		calcLast()

        alignButtons()
        
		if (g_game ~= nil) then
			ClientEngine_GetBrowserPage(g_game, WEB_ADDRESS, WF.FRIENDS, g_curPage, g_friendsPerPage)
		end

	end	
	clear()
end
   
   
function btnRange1_OnButtonClicked( buttonHandle )

	calcRange1()

	if (g_game ~= nil) then
		ClientEngine_GetBrowserPage(g_game, WEB_ADDRESS, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end
	
	clear()
end

function btnRange2_OnButtonClicked( buttonHandle )

	calcRange2()
	
	if (g_game ~= nil) then
		ClientEngine_GetBrowserPage(g_game, WEB_ADDRESS, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end

	clear()
end

function btnRange3_OnButtonClicked( buttonHandle )

	calcRange3()

	if (g_game ~= nil) then
		ClientEngine_GetBrowserPage(g_game, WEB_ADDRESS, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end
	
	clear()
end

function btnRange4_OnButtonClicked( buttonHandle )

	calcRange4()

	if (g_game ~= nil) then
		ClientEngine_GetBrowserPage(g_game, WEB_ADDRESS, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end
	
	clear()
end

-- Added by Jonny to handle alignment of the page buttons at the bottom.
function alignButtons()
    local imgLeftBar = Dialog_GetImage(gDialogHandle, "left arrow bar")
	Control_SetLocation(imgLeftBar, 25, 567)
	local btnLeftLeftArrow = Dialog_GetButton(gDialogHandle, "btnFirst")
	Control_SetLocation(btnLeftLeftArrow, 30, 567)
    local btnLeftArrow = Dialog_GetButton(gDialogHandle, "btnResultsPrev")
	Control_SetLocation(btnLeftArrow, 50, 567)

	local t = {}		-- new table
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange1"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange2"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange3"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange4"))
	leftAlignPageButtons(t, 70, 568, 5)

   	local rightMostXPos = Control_GetLocationX(Dialog_GetButton(gDialogHandle, "btnRange4")) +
   	                      Control_GetWidth(Dialog_GetButton(gDialogHandle, "btnRange4"))

	local btnRightArrow = Dialog_GetButton(gDialogHandle, "btnResultsNext")
	rightMostXPos = rightMostXPos + 5
	Control_SetLocation(btnRightArrow, rightMostXPos, 567)

	local btnRightRightArrow = Dialog_GetButton(gDialogHandle, "btnLast")
	rightMostXPos = rightMostXPos + Control_GetWidth(Dialog_GetButton(gDialogHandle, "btnResultsNext")) + 5
	Control_SetLocation(btnRightRightArrow, rightMostXPos, 567)

	local imgRightBar = Dialog_GetImage(gDialogHandle, "right arrow bar")
	rightMostXPos = rightMostXPos + Control_GetWidth(Dialog_GetButton(gDialogHandle, "btnLast")) + 2
	Control_SetLocation(imgRightBar, rightMostXPos, 567)
end