dofile("MenuHelper.lua")
dofile("CharacterHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("CommonFunctions.lua")


DOWNLOAD_FOR_LOGIN_EVENT = "DownloadForLoginEvent"
DOWNLOAD_FINISHED_EVENT = "DownloadFinishedEvent"

------------------------------------------------------------------------------------------------
PROGRESSIVE = 0 -- Make me 1 to enable progressive Zone Download
------------------------------------------------------------------------------------------------

-- global game object
g_game = nil
g_dispatcher = nil

-- global to denote when it's ok to call login()
g_ok_to_login = false

-- global to denote when it's ok to modify character
g_EnableModifyBtn = false


---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	CharacterHelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel, KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "textHandler", 1,0,0,0, "RenderTextEvent", KEP.MED_PRIO )	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "logonResponseHandler", 1,0,0,0, "LogonResponseEvent", KEP.LOW_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "downloadFinishedHandler", 1,0,0,0, DOWNLOAD_FINISHED_EVENT, KEP.LOW_PRIO ) 
	g_dispatcher = dispatcher
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	CharacterHelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, DOWNLOAD_FOR_LOGIN_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, DOWNLOAD_FINISHED_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "ModifyCharEvent", KEP.HIGH_PRIO ) 

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	
	Helper_Dialog_OnCreate( dialogHandle )
	
	
    Dialog_SetMinimized(dialogHandle,true)
    
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	if KEPConfig_GetString == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end
	
	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	g_ok_to_login = false;

	-- disable the login button
	local ButtonHandle bh = Dialog_GetButton(dialogHandle, "btnLogin")
	local ControlHandle ch = Button_GetControl(bh)
	Control_SetEnabled(ch, false)

	-- disable modify character button
	local ButtonHandle bh = Dialog_GetButton(dialogHandle, "btnCharCreate")
	local ControlHandle ch = Button_GetControl(bh)
	Control_SetEnabled(ch, false)

	local user = ""
	local pass = ""
	user,pass,avatar = ClientEngine_GetCredentials( g_game )
-------------------------------------------------------------------------------------
	if g_game ~= nil then
		local config = ClientEngine_OpenConfig( g_game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )
	
		if config ~= nil then

			local ok, name = KEPConfig_GetString( config, "cbWidgets", "false" )  	
		
			if name == "true" then
			 ClientEngine_DisableSelectWidgetDisplay(g_game)
			else
			 ClientEngine_EnableSelectWidgetDisplay(g_game)
			end
		end	
	end
	
	if g_game ~= nil then

		zoneName = ClientEngine_GetCurrentZoneName(g_game)

		if string.lower(zoneName) == "loginmenu.exg" then
		    g_EnableModifyBtn = true
		end

	end

	 
	if string.len(user) > 0 then
		
		local EditBoxHandle ebh_username = Dialog_GetEditBox(gDialogHandle, "txtUsername")
		EditBox_SetText( ebh_username, user, false )

		if string.len( pass ) > 0 then 
			local EditBoxHandle ebh_password = Dialog_GetEditBox(gDialogHandle, "txtPassword");
			EditBox_SetText( ebh_password, pass, false )
			if avatar > 0 then
				btnCharCreate_OnButtonClicked(nil)
			else	
				btnLogin_OnButtonClicked(nil)
			end
		end
	else
	
		-- get last logon name, if it exists
		Dialog_SetMinimized(dialogHandle,false)
		local config = ClientEngine_OpenConfig( g_game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )
		if config ~= nil then
			local ok, name = KEPConfig_GetString( config, "LastLogonName", "" )
			if ok ~= 0 then
				local EditBoxHandle ebh_username = Dialog_GetEditBox(gDialogHandle, "txtUsername")
				EditBox_SetText( ebh_username, name, false )
			end 
		end 
		
	end
	
	-- set focus to the default control (should be txtPassword)
	Dialog_FocusDefaultControl( dialogHandle)
	

	

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)

	Helper_Dialog_OnDestroy( dialogHandle )
end

---------------------------------------------------------------------
--
-- txtUsername has changed
--
---------------------------------------------------------------------
function txtUsername_OnEditBoxChange(editBoxHandle)

	local EditBoxHandle ebh_password = Dialog_GetEditBox(gDialogHandle, "txtPassword");

	local username = EditBox_GetText(editBoxHandle);
	local password = EditBox_GetText(ebh_password);

	local ButtonHandle bh = Dialog_GetButton(gDialogHandle, "btnLogin")
	local ControlHandle ch = Button_GetControl(bh)

	g_ok_to_login = (trim(username) ~= "") and (trim(password) ~= "")

	Control_SetEnabled(ch, g_ok_to_login)

    if g_EnableModifyBtn then
		local ButtonHandle bh = Dialog_GetButton(gDialogHandle, "btnCharCreate")
		local ControlHandle ch = Button_GetControl(bh)

		Control_SetEnabled(ch, g_ok_to_login)
	end

end

---------------------------------------------------------------------
--
-- txtPassword has changed
--
---------------------------------------------------------------------
function txtPassword_OnEditBoxChange(editBoxHandle)

	local EditBoxHandle ebh_username = Dialog_GetEditBox(gDialogHandle, "txtUsername");

	local username = EditBox_GetText(ebh_username);
	local password = EditBox_GetText(editBoxHandle);

	local ButtonHandle bh = Dialog_GetButton(gDialogHandle, "btnLogin")
	local ControlHandle ch = Button_GetControl(bh)

	g_ok_to_login = (trim(username) ~= "") and (trim(password) ~= "")

	Control_SetEnabled(ch, g_ok_to_login)
	
	if g_EnableModifyBtn then
		local ButtonHandle bh = Dialog_GetButton(gDialogHandle, "btnCharCreate")
		local ControlHandle ch = Button_GetControl(bh)

		Control_SetEnabled(ch, g_ok_to_login)
	end

end

---------------------------------------------------------------------
--
-- Exit Button - Quit game
--
---------------------------------------------------------------------
function btnExit_OnButtonClicked( buttonHandle )

	if g_game ~= nil then
		ClientEngine_Quit( g_game )
	end
	
end

---------------------------------------------------------------------
--
-- Login button clicked
--
---------------------------------------------------------------------
function btnLogin_OnButtonClicked( buttonHandle )

	local EditBoxHandle ebh_username = Dialog_GetEditBox(gDialogHandle, "txtUsername");
	local EditBoxHandle ebh_password = Dialog_GetEditBox(gDialogHandle, "txtPassword");

	local username = EditBox_GetText(ebh_username);
	local password = EditBox_GetText(ebh_password);

	EnableControlsDuringLogon(ebh_username, ebh_password, false)

	Login( username, password );
end

---------------------------------------------------------------------
--
-- 'ENTER' key was pressed within txtPassword
--
---------------------------------------------------------------------
function txtPassword_OnEditBoxString(editBoxHandle, password)

	if g_ok_to_login then

		local EditBoxHandle ebh_username = Dialog_GetEditBox(gDialogHandle, "txtUsername");
		local EditBoxHandle ebh_password = Dialog_GetEditBox(gDialogHandle, "txtPassword");
		local username = EditBox_GetText(ebh_username);

		EnableControlsDuringLogon(ebh_username, ebh_password, false)

		Login( username, password );
	end
end

---------------------------------------------------------------------
--
-- Enable/Disable controls
--
---------------------------------------------------------------------
function EnableControlsDuringLogon(ebh_username, ebh_password, enable)

	Control_SetEnabled(ebh_username, enable)
	Control_SetEnabled(ebh_password, enable)

	-- enable/disable the login button
	local ButtonHandle bh = Dialog_GetButton(gDialogHandle, "btnLogin")
	Control_SetEnabled(bh, enable)

	if g_EnableModifyBtn then
	    local ButtonHandle bh = Dialog_GetButton(gDialogHandle, "btnCharCreate")
		Control_SetEnabled(bh, enable)
	end

end

---------------------------------------------------------------------
--
-- Connect to the server
--
---------------------------------------------------------------------
function Login( username, password )
	-- attempt to connect
	if g_game ~= nil then
		local config = ClientEngine_OpenConfig( g_game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )
		if config ~= nil then
			KEPConfig_SetString( config, "LastLogonName", username )
			KEPConfig_SetString( config, "ChatHistory", "")
			KEPConfig_Save( config )
		end
		if PROGRESSIVE == 1 then
			local zi = ClientEngine_GetPlayerLoginZoneIndex(g_game, username)
			gotoMenu( "ZoneDownloader.xml", 0, 0 )
			local evt = Dispatcher_MakeEvent( g_dispatcher, DOWNLOAD_FOR_LOGIN_EVENT )
			Event_EncodeString(evt, username)
			Event_EncodeString(evt, password)
			Event_EncodeNumber(evt, zi) -- the zone index
			Dispatcher_QueueEvent( g_dispatcher, evt) 
			DestroyMenu(gDialogHandle)
		else
			ClientEngine_Connect2( g_game, username, password )
		end
	end
end

---------------------------------------------------------------------
--
-- Logon Response Handler
--
---------------------------------------------------------------------
function logonResponseHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

	local connectionErrorCode 	= Event_DecodeString( event )
	local hResultCode 		= Event_DecodeNumber( event )

	if hResultCode ~= 0 then

		if connectionErrorCode ~= "" then

        Dialog_SetMinimized(gDialogHandle,false);
        
			-- show the error message
			local StaticHandle sh = Dialog_GetStatic(gDialogHandle, "txtError")
			Control_SetVisible(sh, true)
			Static_SetText(sh, connectionErrorCode)

			-- show controls again
			local EditBoxHandle ebh_username = Dialog_GetEditBox(gDialogHandle, "txtUsername");
			local EditBoxHandle ebh_password = Dialog_GetEditBox(gDialogHandle, "txtPassword");
			EnableControlsDuringLogon(ebh_username, ebh_password, true)
		end
			
		-- set focus back to default control (txtUsername)
		Dialog_FocusDefaultControl( gDialogHandle )
		
	else
	-- successfully logged on

		GetCharacters();
		
		-- close this dialog
		-- DestroyMenu(gDialogHandle)

	end

	return KEP.EPR_OK

end

---------------------------------------------------------------------
--
-- Helper function for strings
--
---------------------------------------------------------------------
function trim (s)
      return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end

function textHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

	msg = Event_DecodeString( event )
	chatType = Event_DecodeNumber( event )

	if chatType == ChatType.System then  
		statusCtrl = Dialog_GetStatic(gDialogHandle, "txtStatus")
		Static_SetText( statusCtrl, msg )
	end
	
	--we dont want any text to go to the old text screen
	--return KEP.EPR_OK
	return KEP.EPR_CONSUMED
end

--[[
function Test_OnButtonClicked( buttonHandle )
	Dialog_Create( "BrowserTest.xml" )
	DestroyMenu(gDialogHandle)
end 
]]--

function btnJoin_OnButtonClicked( buttonHandle )
	ClientEngine_LaunchBrowser( g_game, GameGlobals.WEB_SITE_PREFIX_KANEVA .. "register.aspx?ILC=kvwclient&link=loginscreen" )
end

function btnLost_OnButtonClicked( buttonHandle )
	ClientEngine_LaunchBrowser( g_game, GameGlobals.WEB_SITE_PREFIX_KANEVA .. "lostPassword.aspx" )
end

function downloadFinishedHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == 1 then 
		local username = Event_DecodeString(event)
		local password = Event_DecodeString(event)
		local EditBoxHandle ebh_username = Dialog_GetEditBox(gDialogHandle, "txtUsername");
		local EditBoxHandle ebh_password = Dialog_GetEditBox(gDialogHandle, "txtPassword");
		EditBox_SetText(ebh_username, username, false);
		EditBox_SetText(ebh_password, password, false);
		EnableControlsDuringLogon(ebh_username, ebh_password, false)

		-- hide the error message text
		local StaticHandle sh = Dialog_GetStatic(gDialogHandle, "txtError")
		Control_SetVisible(sh, false)
		ClientEngine_Connect2( g_game, username, password )	

		return KEP.EPR_CONSUMED
	else
		return KEP.EPR_NOT_PROCESSED
	end
end

function btnCharCreate_OnButtonClicked( buttonHandle )

	local EditBoxHandle ebh_username = Dialog_GetEditBox(gDialogHandle, "txtUsername");
	local EditBoxHandle ebh_password = Dialog_GetEditBox(gDialogHandle, "txtPassword");

	local username = EditBox_GetText(ebh_username);
	local password = EditBox_GetText(ebh_password);

	EnableControlsDuringLogon(ebh_username, ebh_password, false)

	Login( username, password );

	e = Dispatcher_MakeEvent( g_dispatcher, "ModifyCharEvent" )
	if e ~= nil then
		Event_EncodeNumber( e, 1  )		
		Dispatcher_QueueEvent( g_dispatcher, e )
	end

end
