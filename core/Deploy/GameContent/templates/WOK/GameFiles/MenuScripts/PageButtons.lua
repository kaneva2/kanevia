dofile("MenuStyles.lua")
dofile("CommonFunctions.lua")

SHIFT_NONE = 0
SHIFT_UP = 1
SHIFT_DN = 2
SHIFT_ON_DELETE = 3

g_shift = SHIFT_NONE

--stores the buttonHandle of the selected button
g_selectedBtn = nil

--current button in a g_window amount of page buttons
g_currentBtn = 1

g_btnRange1 = nil

g_window = 4

g_set = 1

g_prev_set = nil

-- Current page of data
g_curPage = 1


-- Maximum page number with data
g_pageCap = 1
-- Maximum g_window size set we can have
g_setCap = 1

-- Used by menu scripts to know when to reset button text
g_InitBtnText = true


-- currentPage: current page of data the menu
-- window: How many numerical buttons are used
-- set: which set of size window we are on.  i.e. set 2 would contain the
--      numbers 5 6 7 8 in its window
function calcResultsPrev()

	--g_curPage = g_curPage - window

--Move 1 page at a time

	g_curPage = g_curPage - 1
	if g_curPage < 1 then
	    g_curPage = 1
 	else
 	
        setDeselectColor(g_selectedBtn)

		--Get the name of the currently selected button and see if it is the
		--first page button in our 4 button window.  If it is this button press
		--will cause a need for us to update the text for our buttons.
	    ch = Button_GetControl( g_selectedBtn )
	    local btnName = Control_GetName(ch)

	    if btnName == "btnRange1" then
			--Button numbers need to be decreased
			g_shift = SHIFT_DN
	  		setButtonText()
	    end

       	g_currentBtn = g_currentBtn - 1
		if g_currentBtn < 1 then
		    g_currentBtn = 1
    	end

		local bh = Dialog_GetButton(gDialogHandle, "btnRange"..g_currentBtn)
		if bh ~= nil then
			g_selectedBtn = bh
			setSelectionColor(bh)
		end

	end


--Move 4 pages at a time ---------------------------------------------
--[[
	set = set - 1
	g_curPage = (set * window) - 3

	if g_curPage < 1 then
	    g_curPage = 1
	end

	if set < 1 then
		set = 1
	end
]]--
---------------------------------------------
end

-- setCap: number of the highest possible set (of size window) possible with current
--          data set
function calcResultsNext()

--Move 1 page at a time

	g_curPage = g_curPage + 1

	-- Dont go too high
	if g_curPage > g_pageCap then
		g_curPage = g_pageCap
 	else

        setDeselectColor(g_selectedBtn)

		--Get the name of the currently selected button and see if it is the
		--last page button in our 4 button window.  If it is this button press
		--will cause a need for us to update the text for our buttons.
	    ch = Button_GetControl( g_selectedBtn )
	    local btnName = Control_GetName(ch)

	    if btnName == "btnRange4" then
			--Button numbers need to be increased
			g_shift = SHIFT_UP
	  		setButtonText()
	    end
	    
       	g_currentBtn = g_currentBtn + 1
		if g_currentBtn > 4 then
		    g_currentBtn = 4
  		end

		local bh = Dialog_GetButton(gDialogHandle, "btnRange"..g_currentBtn)
		if bh ~= nil then
			g_selectedBtn = bh
			setSelectionColor(bh)
		end

	end

-- Move 4 pages at a time ---------------------------------------------
--[[
	set = set + 1

	if set > setCap then
	    set = setCap
    else

		g_curPage = (set * window) - 3

		-- Dont go too high
		if g_curPage > g_pageCap then
			g_curPage = g_pageCap
		end

	end
]]--

---------------------------------------------


end

function resetPageButtonSelection()

    setDeselectColor(g_selectedBtn)
    
   	g_selectedBtn = Dialog_GetButton(gDialogHandle, "btnRange1")
	g_currentBtn = 1

    setSelectionColor(g_selectedBtn)
    
    g_shift = SHIFT_NONE
    
    setButtonText()

end

function initButtons( )

	local bh = Dialog_GetButton(gDialogHandle, "btnRange1")
	setDeselectColor(bh)

	bh = Dialog_GetButton(gDialogHandle, "btnRange2")
	setDeselectColor(bh)

	bh = Dialog_GetButton(gDialogHandle, "btnRange3")
	setDeselectColor(bh)

   	bh = Dialog_GetButton(gDialogHandle, "btnRange4")
	setDeselectColor(bh)
	
   	bh = Dialog_GetButton(gDialogHandle, "btnFirst")
	setDeselectColor(bh)
	
   	bh = Dialog_GetButton(gDialogHandle, "btnLast")
	setDeselectColor(bh)
	
   	bh = Dialog_GetButton(gDialogHandle, "btnResultsNext")
	setDeselectColor(bh)
	
   	bh = Dialog_GetButton(gDialogHandle, "btnResultsPrev")
	setDeselectColor(bh)

end

function setSelectionColor( buttonHandle )

	if buttonHandle ~= nil then

		g_selectedBtn = buttonHandle

		local eh = Button_GetNormalDisplayElement(buttonHandle)
		local bch = Element_GetFontColor(eh)

        Element_AddFont( eh, gDialogHandle, PB.FONT, PB.SELECT_POINT, PB.WEIGHT, PB.ITALIC )
	    BlendColor_SetColor( bch, DXUT_STATE_NORMAL, PB.SELECT_COLOR )

		eh = Button_GetFocusedDisplayElement(buttonHandle)
		bch = Element_GetFontColor(eh)

        Element_AddFont( eh, gDialogHandle, PB.FONT, PB.SELECT_POINT, PB.WEIGHT, PB.ITALIC )
	    BlendColor_SetColor( bch, DXUT_STATE_FOCUS, PB.SELECT_COLOR )

		eh = Button_GetPressedDisplayElement(buttonHandle)
		bch = Element_GetFontColor(eh)
		
        Element_AddFont( eh, gDialogHandle, PB.FONT, PB.SELECT_POINT, PB.WEIGHT, PB.ITALIC )
	    BlendColor_SetColor( bch, DXUT_STATE_PRESSED, PB.SELECT_COLOR )

	end

end

function setDeselectColor( buttonHandle )

	if buttonHandle ~= nil then
	
		local eh = Button_GetNormalDisplayElement(buttonHandle)
		local bch = Element_GetFontColor(eh)
		
		local ehF = Button_GetFocusedDisplayElement(buttonHandle)
		local bchF = Element_GetFontColor(ehF)
		
		Element_AddFont( eh, gDialogHandle, PB.FONT, PB.DESELECT_POINT, PB.WEIGHT, PB.ITALIC )
  	    BlendColor_SetColor( bch, DXUT_STATE_NORMAL, PB.DESELECT_COLOR )

        Element_AddFont( ehF, gDialogHandle, PB.FONT, PB.DESELECT_POINT, PB.WEIGHT, PB.ITALIC )
	    BlendColor_SetColor( bchF, DXUT_STATE_FOCUS, PB.DESELECT_COLOR )

	end
end

function calcFirst()

  	setDeselectColor(g_selectedBtn)

	g_curPage = 1
	g_set = 1
	
--New 1 page move stuff
	
	g_selectedBtn = Dialog_GetButton(gDialogHandle, "btnRange1")
	g_currentBtn = 1
	
	setSelectionColor(g_selectedBtn)
	
	g_shift = SHIFT_DN
	
	setButtonText()
	
end

function calcLast()

    setDeselectColor(g_selectedBtn)
 	
	g_curPage = g_pageCap
	g_set = g_setCap

--New 1 page move stuff
	g_selectedBtn = Dialog_GetButton(gDialogHandle, "btnRange4")
	g_currentBtn = 4

	setSelectionColor(g_selectedBtn)

	g_shift = SHIFT_UP
	
	setButtonText()

--Move 4 pages at a time
--[[
 	--Last button in window set (could be btnRange1 - 4)
	local btnPos = g_window - ((g_setCap * g_window) - g_pageCap)
	local btnName = "btnRange"..tostring(btnPos)
]]--

end

function decrementPage()

	g_curPage = g_curPage - 1
	if g_curPage < 1 then
		g_curPage = 1
	end

--[[
	g_pageCap = g_pageCap - 1
	if g_pageCap < 1 then
	    g_pageCap = 1
	end
]]--
	g_shift = SHIFT_ON_DELETE
	setButtonText()		
	
end

function calcRange1()

	local difference = g_currentBtn - 1
	
	g_curPage = g_curPage - difference

	g_currentBtn = 1

	setDeselectColor(g_selectedBtn)

	if gDialogHandle ~= nil then
		local bh = Dialog_GetButton(gDialogHandle, "btnRange1")
		setSelectionColor(bh)
	end

--Move 4 pages at a time technique
--[[
	g_curPage = (g_set * g_window) - 3

	if g_curPage > g_pageCap then
	    g_curPage = g_pageCap
	end
]]--
end

function calcRange2()

	local difference = g_currentBtn - 2

	g_curPage = g_curPage - difference

	g_currentBtn = 2

	setDeselectColor(g_selectedBtn)

	if gDialogHandle ~= nil then
		local bh = Dialog_GetButton(gDialogHandle, "btnRange2")
		setSelectionColor(bh)
	end

--Move 4 pages at a time technique
--[[
	g_curPage = (g_set * g_window) - 2

	if g_curPage > g_pageCap then
	    g_curPage = g_pageCap
	end
]]--
end

function calcRange3()

	local difference = g_currentBtn - 3

	g_curPage = g_curPage - difference

	g_currentBtn = 3

    setDeselectColor(g_selectedBtn)

	if gDialogHandle ~= nil then
		local bh = Dialog_GetButton(gDialogHandle, "btnRange3")
		setSelectionColor(bh)
	end

--Move 4 pages at a time technique
--[[
	g_curPage = (g_set * g_window) - 1

	if g_curPage > g_pageCap then
	    g_curPage = g_pageCap
	end
]]--
end

function calcRange4()

	local difference = g_currentBtn - 4

	g_curPage = g_curPage - difference

	g_currentBtn = 4

    setDeselectColor(g_selectedBtn)

	if gDialogHandle ~= nil then
		local bh = Dialog_GetButton(gDialogHandle, "btnRange4")
		setSelectionColor(bh)
	end

--Move 4 pages at a time technique
--[[
	g_curPage = (g_set * g_window)

	if g_curPage > g_pageCap then
	    g_curPage = g_pageCap
	end
]]--
end


-- Assign the right text number to the 4 page buttons on the menu
-- ie 1 2 3 4 or 21 22 23 24
function setButtonText()

--Move 4 buttons at a time code
--[[
	-- if the max number of possible pages is not greater than the number
	-- of page buttons skip this.  Dont want to shift the button names
	local frame = g_set * g_window

   	local page = frame - 3
	local bh = Dialog_GetButton(gDialogHandle, "btnRange1")

    page = frame - 2
	bh = Dialog_GetButton(gDialogHandle, "btnRange2")
	
	page = frame - 1
	bh = Dialog_GetButton(gDialogHandle, "btnRange3")
	
	page = frame
	bh = Dialog_GetButton(gDialogHandle, "btnRange4")
]]--

	local page1 = 0
	local page2 = 0
	local page3 = 0
	local page4 = 0

	if g_shift == SHIFT_UP then
	    page1 = g_curPage - 3
	    page2 = g_curPage - 2
	    page3 = g_curPage - 1
	    page4 = g_curPage
	elseif g_shift == SHIFT_ON_DELETE then

		if g_currentBtn > g_curPage then
		    g_currentBtn = g_curPage
		end

		if g_currentBtn == 1 then
		    page1 = g_curPage
		    page2 = g_curPage + 1
		    page3 = g_curPage + 2
		    page4 = g_curPage + 3
		elseif g_currentBtn == 2 then
		    page1 = g_curPage - 1
		    page2 = g_curPage
		    page3 = g_curPage + 1
		    page4 = g_curPage + 2
		elseif g_currentBtn == 3 then
		    page1 = g_curPage - 2
		    page2 = g_curPage - 1
		    page3 = g_curPage
		    page4 = g_curPage + 1
		elseif g_currentBtn == 4 then
		    page1 = g_curPage - 3
		    page2 = g_curPage - 2
		    page3 = g_curPage - 1
		    page4 = g_curPage
		end

	else --For down shifts and initialization
	    page1 = g_curPage
	    page2 = g_curPage + 1
	    page3 = g_curPage + 2
	    page4 = g_curPage + 3
	end	
			
	local bh = Dialog_GetButton(gDialogHandle, "btnRange1")
	-- store first range button handle for page forward/back and first/last
	--g_btnRange1 = bh
    local sh = Button_GetStatic( bh )

	if page1 <= g_pageCap and page1 > 0 then
		setButtonState( bh, true )
	    Static_SetText( sh, tostring(page1) )
	else
	    setButtonState( bh, false )
	    Static_SetText( sh, "" )
	end


	bh = Dialog_GetButton(gDialogHandle, "btnRange2")
    sh = Button_GetStatic( bh )
	if page2 <= g_pageCap and page2 > 0 then
		setButtonState( bh, true )
	    Static_SetText( sh, tostring(page2) )
	else
		setButtonState( bh, false )
	    Static_SetText( sh, "" )
	end


	bh = Dialog_GetButton(gDialogHandle, "btnRange3")
	sh = Button_GetStatic( bh )
	if page3 <= g_pageCap and page3 > 0  then
	
--Dialog_MsgBox("Page 3 should be lit")
	
		setButtonState( bh, true )
	    Static_SetText( sh, tostring(page3) )
	else
		setButtonState( bh, false )
	    Static_SetText( sh, "" )
	end


	bh = Dialog_GetButton(gDialogHandle, "btnRange4")
	sh = Button_GetStatic( bh )
    if page4 <= g_pageCap and page4 > 0 then
   		setButtonState( bh, true )
	    Static_SetText( sh, tostring(page4) )
	else
		setButtonState( bh, false )
	    Static_SetText( sh, "" )
	end


 -- Remove arrow buttons if less than window amount of pages
	if g_pageCap <= g_window then

		bh = Dialog_GetButton(gDialogHandle, "btnResultsPrev")
   		setButtonState( bh, false )
   		ch = Button_GetControl(bh)
   		Control_SetVisible(ch, false)

		bh = Dialog_GetButton(gDialogHandle, "btnResultsNext")
   		setButtonState( bh, false )
   		ch = Button_GetControl(bh)
   		Control_SetVisible(ch, false)

		bh = Dialog_GetButton(gDialogHandle, "btnFirst")
   		setButtonState( bh, false )
   		ch = Button_GetControl(bh)
   		Control_SetVisible(ch, false)

		bh = Dialog_GetButton(gDialogHandle, "btnLast")
   		setButtonState( bh, false )
   		ch = Button_GetControl(bh)
   		Control_SetVisible(ch, false)
   		
   		-- 05/16/07 mbiggs added to handle the new menus
   		bh = Dialog_GetImage(gDialogHandle, "right arrow bar")
     	Control_SetVisible(bh, false)
     	
     	bh = Dialog_GetImage(gDialogHandle, "left arrow bar")
     	Control_SetVisible(bh, false)

	else

		bh = Dialog_GetButton(gDialogHandle, "btnResultsPrev")
   		setButtonState( bh, true )
   		ch = Button_GetControl(bh)
   		Control_SetVisible(ch, true)

		bh = Dialog_GetButton(gDialogHandle, "btnResultsNext")
   		setButtonState( bh, true )
   		ch = Button_GetControl(bh)
   		Control_SetVisible(ch, true)

		bh = Dialog_GetButton(gDialogHandle, "btnFirst")
   		setButtonState( bh, true )
   		ch = Button_GetControl(bh)
   		Control_SetVisible(ch, true)

		bh = Dialog_GetButton(gDialogHandle, "btnLast")
   		setButtonState( bh, true )
   		ch = Button_GetControl(bh)
   		Control_SetVisible(ch, true)

		-- 05/16/07 mbiggs added to handle the new menus
        bh = Dialog_GetImage(gDialogHandle, "right arrow bar")
     	Control_SetVisible(bh, true)

     	bh = Dialog_GetImage(gDialogHandle, "left arrow bar")
     	Control_SetVisible(bh, true)

	end



end

function rightAlignPageButtons(buttonlist, startx, starty, spacebetween)
	local xoffset = 0
	for i=#buttonlist, 1, -1 do
		local but = buttonlist[i]
		local sta = Button_GetStatic(but)
		local con = Button_GetControl(but)
		local thiswidth = Static_GetStringWidth(sta, Static_GetText(sta)) + 1
		Control_SetSize(con, thiswidth, Control_GetHeight(con))
		Control_SetLocation(con, startx - thiswidth - xoffset, starty)
		if thiswidth == 1 then
			xoffset = xoffset
		else
			xoffset = thiswidth + xoffset + spacebetween
		end
	end
end

function leftAlignPageButtons(buttonlist, startx, starty, spacebetween)
	local xoffset = 0
	for i=1, #buttonlist do
		local but = buttonlist[i]
		local sta = Button_GetStatic(but)
		local con = Button_GetControl(but)
		local thiswidth = Static_GetStringWidth(sta, Static_GetText(sta)) + 1
		Control_SetSize(con, thiswidth, Control_GetHeight(con))
		Control_SetLocation(con, startx + xoffset, starty)
		if thiswidth == 1 then
			xoffset = xoffset
		else
			xoffset = thiswidth + xoffset + spacebetween
		end
	end
end