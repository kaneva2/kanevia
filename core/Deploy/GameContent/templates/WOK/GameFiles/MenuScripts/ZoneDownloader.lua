dofile("MenuHelper.lua")
dofile("..\\Scripts\\KEP.lua")
dofile("..\\ScriptData\\Zones.lua")
dofile("CommonFunctions.lua")

BEGIN_DOWNLOAD_EVENT = "BeginZoneDownloadEvent"
DOWNLOAD_FOR_LOGIN_EVENT = "DownloadForLoginEvent"
DOWNLOAD_FINISHED_EVENT = "DownloadFinishedEvent"


g_game = nil
g_dispatcher = nil

g_t_file_list = {}
g_cur_file_num = 0
g_total_download_size = 0
g_zone_index = nil
g_zone_instance_id = nil
g_login_username = nil
g_login_password = nil

g_debug = false

g_runLocal = false

---------------------------------------------------------------------
-- Called from C to initialize all the handlers in this script
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "ZoneDownloadHandler", 1,0,0,0, "ZoneDownloadEvent", KEP.MED_PRIO )
 	ParentHandler_RegisterEventHandler( dispatcher, handler, "beginDownloadHandler", 1,0,0,0, BEGIN_DOWNLOAD_EVENT, KEP.LOW_PRIO )
 	ParentHandler_RegisterEventHandler( dispatcher, handler, "loginDownloadHandler", 1,0,0,0, DOWNLOAD_FOR_LOGIN_EVENT, KEP.LOW_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "logonResponseHandler", 1,0,0,0, "LogonResponseEvent", KEP.LOW_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "pendingSpawnHandler", 1,0,0,0, KEP.PENDINGSPAWN_EVENT_NAME, KEP.HIGH_PRIO ) 
	g_dispatcher = dispatcher
end

---------------------------------------------------------------------
-- Called from C to initialize all the events
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, BEGIN_DOWNLOAD_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, DOWNLOAD_FINISHED_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, DOWNLOAD_FOR_LOGIN_EVENT, KEP.MED_PRIO )
end

---------------------------------------------------------------------
-- Called from C to when the dialog is created (OnInit)
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	g_game = Dispatcher_GetGame(KEP.dispatcher)	
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	Dialog_BringToFront(dialogHandle)
    
    checkForStarKep()
end

---------------------------------------------------------------------
---------------------------------------------------------------------
-- Visuals helpers.  Written by Jonny Mar 20 07 to provide basic
-- front end display for progressive downloads
---------------------------------------------------------------------
---------------------------------------------------------------------

---------------------------------------------------------------------
-- Updates percent downloaded display for currently downloading file
---------------------------------------------------------------------
function displayFilePercent( percent )
	local sh = Dialog_GetStatic(gDialogHandle, "lblFilePercent")
	Static_SetText(sh, tostring(percent).. "%")
	local ih = Dialog_GetImage(gDialogHandle, "imgFileProgressFront")
	local progwid = 250 * percent / 100
	Control_SetSize(Image_GetControl(ih), progwid, 25)
end

---------------------------------------------------------------------
-- Updates percent downloaded display total download
---------------------------------------------------------------------
function displayTotalPercent( filePercent )
	local completedtotal = 0
	for i=1, g_cur_file_num - 1 do
		completedtotal = completedtotal + g_t_file_list[i]["size"]
	end
	local curfileprog = g_t_file_list[g_cur_file_num]["size"] * (filePercent / 100)
	completedtotal = (completedtotal + curfileprog) / g_total_download_size
	completedtotal = math.ceil(completedtotal* 100) 
	local sh = Dialog_GetStatic(gDialogHandle, "lblTotalPercent")
	Static_SetText(sh, tostring(completedtotal).. "%")
	local ih = Dialog_GetImage(gDialogHandle, "imgTotalProgressFront")
	local progwid = 250 * completedtotal / 100
	Control_SetSize(Image_GetControl(ih), progwid, 25)
end

---------------------------------------------------------------------
-- Toggles visuals for checkmarks in the downloading files listbox
-- 0 - Not downloaded yet (Default)
-- 1 - Currently downloading
-- 2 - Already downloaded
---------------------------------------------------------------------
function displayCheck(filenum, status)
	local checkname = "imgCheck" .. filenum
	local ih = Dialog_GetImage(gDialogHandle, checkname)
	if status == 0 then
		-- Show nothing if don't have it yet
		Control_SetVisible(Image_GetControl(ih), false)
	elseif status == 1 then
		local eh = Image_GetDisplayElement(ih)
		Element_SetCoords(eh, 258, 60, 280, 86)
		Control_SetVisible(Image_GetControl(ih), true)
	else
		local eh = Image_GetDisplayElement(ih)
		Element_SetCoords(eh, 172, 20, 189, 38)
		Control_SetVisible(Image_GetControl(ih), true)
	end
end

function clearChecks()
	displayCheck(1, 0)
	displayCheck(2, 0)
	displayCheck(3, 0)
	displayCheck(4, 0)
end





---------------------------------------------------------------------
---------------------------------------------------------------------
-- End Visuals Helpers.
---------------------------------------------------------------------
---------------------------------------------------------------------



g_PendingSpawnEvent = nil

function pendingSpawnHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- remake the event to send to server if/when we complete download
	g_PendingSpawnEvent = Dispatcher_MakeEvent( dispatcher, KEP.PENDINGSPAWN_EVENT_NAME )
	Event_SetFilter( g_PendingSpawnEvent, filter )
	Event_SetObjectId( g_PendingSpawnEvent, objectid )
	Event_EncodeNumber( g_PendingSpawnEvent, Event_DecodeNumber( event ) )
	Event_AddTo( g_PendingSpawnEvent, KEP.SERVER_NETID )

	if g_debug then 
		Dialog_MsgBox( "ZoneDownloader downloading zone "..tostring(	InstanceId_GetId(filter) ) )
	end 
	
	downloadZone( InstanceId_GetId(filter) )

	return KEP.EPR_CONSUMED
	
end 

---------------------------------------------------------------------
-- Called from C to when the dialog is created (OnClose)
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function ZoneDownloadHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	displayFilePercent( filter / 2 )
	displayTotalPercent( filter / 2 )
	if filter == 200 then
	
		if g_debug then 
			Dialog_MsgBox( "nextFile since 100%" )			
		end 
		
		nextFile()
	end
end

function beginDownloadHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local zonename = Event_DecodeString(event)
	g_zone_index = Event_DecodeNumber(event)
	g_zone_instance_id = Event_DecodeNumber(event)
	downloadZone( InstanceId_GetId(g_zone_index) )
	if g_debug then 
		Dialog_MsgBox( "nextFile since beginDownloadHandler" )			
	end
	
	nextFile()
end

function loginDownloadHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	g_login_username = Event_DecodeString(event)
	g_login_password = Event_DecodeString(event)
	local fullid = Event_DecodeNumber(event)
	g_zone_index = fullid
	g_zone_instance_id = InstanceId_GetInstanceId(fullid)
	
	downloadZone( InstanceId_GetId(g_zone_index) )
	local lst = Dialog_GetListBox(gDialogHandle, "lstItems")
	local i = 1
	local file = g_t_file_list[i]
	while file ~= nil do
		ListBox_AddItem(lst, file["display_name"], 0)
		i = i + 1
		file = g_t_file_list[i]
	end

	if g_debug then 
		Dialog_MsgBox( "nextFile since loginDownloadHandler" )			
	end
	
	nextFile()
end

function btnClose_OnButtonClicked( buttonHandle )
	-- DestroyMenu(gDialogHandle)
end


-- For now we assume if we have file it is up to date.
function verifyFile(filename)
	return ClientEngine_TestPatchFileForUpdate( g_game, "\\GameFiles\\" .. filename)
end

function verifyTexFile(filename)
	return ClientEngine_TestPatchFileForUpdate( g_game, "\\MapsModels\\" .. filename)
end

function queueFileForDownload(displayname, filename)
	-- 
    
    if g_runLocal then
        
        return true
    
    end
    
	local i = 0
	local j = 0
	
	local sizeNameB = 0
	local sizeNameE = 0
	local strippedName = filename
	
	sizeNameB, sizeNameE = string.find( filename, "\\" )
	
	if sizeNameB then
		 strippedName = string.sub( filename, sizeNameE + 1, -1 )
	end

	i, j = string.find(filename, ".pak") -- assume if its a pak its a texture pak
	if i then	
		if verifyTexFile(filename) == 0 then
			local file = {}
			file["display_name"] = displayname
			file["file_name"] = strippedName
			file["size"] = Zones[strippedName]
			table.insert(g_t_file_list, file)
			g_total_download_size = g_total_download_size + file["size"]
			return false
		else
			return true
		end		
	else
		if verifyFile(filename) == 0 then
			local file = {}
			file["display_name"] = displayname
			file["file_name"] = filename
			file["size"] = Zones[filename]
			table.insert(g_t_file_list, file)
			g_total_download_size = g_total_download_size + file["size"]
			return false
		else
			return true
		end	
	end
	
	

end

ZoneFiles = { 
	{},
	{{"Conference room environment", "ConferenceRM.exg"}, {"Conference room textures", "ConferenceRM\\ConferenceRM.pak"}},
    {{"Store Front environment", "Storefront.exg"}, {"Store Front textures", "Storefront\\Storefront.pak"}}
	}


-- Ok for now lets hardcode our values, and figure out what we want to do later.
function downloadZone( zoneindex )
	g_cur_file_num = 0
	g_t_file_list = {}
	g_total_download_size = 0
	clearChecks()

	local lst = Dialog_GetListBox(gDialogHandle, "lstItems")
	if zoneindex >= 0 and zoneindex < #ZoneFiles then 
		
		local fileList = ZoneFiles[zoneindex+1]

		local queueCount = 0
		
		for key, value in pairs(fileList) do
			if not queueFileForDownload(fileList[key][1], fileList[key][2]) then
				if g_debug then 
					Dialog_MsgBox( "ZoneDownloader queueing "..fileList[key][2] )			
				end 
				ListBox_AddItem(lst, fileList[key][1], 0)
				queueCount = queueCount + 1
			end
		end
		
		if queueCount > 0 then 
			nextFile()
		else
			if g_PendingSpawnEvent ~= nil then 
			
				if g_debug then 
					Dialog_MsgBox( "ZoneDownloader firing spawn event since all files present" )			
				end
				
				Dispatcher_QueueEvent( g_dispatcher, g_PendingSpawnEvent )
				g_PendingSpawnEvent = nil
			end
			DestroyMenu(gDialogHandle)
		end 
	end
	
end

-- Here we determine if we have an up to date version of this file.
-- For now just return false, and redownload it.
function validateFile( filename )
	return false
end

function nextFile()
	if g_cur_file_num > 0 then
		displayCheck(g_cur_file_num, 2)
	end
	g_cur_file_num = g_cur_file_num + 1
	local file = g_t_file_list[g_cur_file_num]
	if g_debug then
		Dialog_MsgBox( "nextFile has file "..tostring(file).." number "..tostring(g_cur_file_num))			
	end
	if file ~= nil then
		displayCheck(g_cur_file_num , 1)
		--Dialog_MsgBox(file["file_name"] .. file["size"])
		local i = 0
		local j = 0
		i, j = string.find(file["file_name"], ".pak") -- assume if its a pak its a texture pak
		if i then
			ClientEngine_DownloadTexturePack(g_game, file["file_name"], file["size"])
		else
			ClientEngine_DownloadPatchFile( g_game, file["file_name"], file["size"])
		end
	else
		if g_login_username ~= nil then
			gotoMenu( "LoginMenu.xml", 0, 0 )
			local evt = Dispatcher_MakeEvent( g_dispatcher, DOWNLOAD_FINISHED_EVENT )
			Event_EncodeString(evt, g_login_username)
			Event_EncodeString(evt, g_login_password)
			Dispatcher_QueueEvent( g_dispatcher, evt) 
			DestroyMenu(gDialogHandle)
		else
			if g_PendingSpawnEvent ~= nil then 
				if g_debug then
					Dialog_MsgBox( "ZoneDownloader firing spawn event" )			
				end
				Dispatcher_QueueEvent( g_dispatcher, g_PendingSpawnEvent )
				g_PendingSpawnEvent = nil
			end
			DestroyMenu(gDialogHandle)
		end
	end
end

function checkForStarKep()

    ret = ClientEngine_TestPatchFileForUpdate( g_game, "\\STAR.kep")
    
    if ret ~= 0 then
        
       g_runLocal = true
    
    end


end

