----------------------------------------------
--
-- Functions for obtaining and parsing credit
-- balances (regular and gift) from the WOK
-- web query
--
----------------------------------------------

--This event come from menus that impact credits.
--Sends a new credit total to the HUD
UPDATE_HUD_EVENT = "UpdateHUDBalanceEvent"

--Send credit total to HUD
--credits:  number of credits
--getNewBalance: number representing bool for whether or not to call credit
--				 balance query again
function updateHUDBalance( credits, giftCredits, getNewBalance )

	if g_game ~= nil then

		local ev = Dispatcher_MakeEvent( KEP.dispatcher, UPDATE_HUD_EVENT )

		Event_EncodeNumber( ev, credits )
		Event_EncodeNumber( ev, giftCredits )
		Event_EncodeNumber( ev, getNewBalance )

		Dispatcher_QueueEvent( KEP.dispatcher, ev )

	end

end

function getUserID( playerName, menu_filter )

	if playerName ~= nil and playerName ~= "" then

		g_web_address = GameGlobals.WEB_SITE_PREFIX..USERID_SUFFIX..playerName
		--g_parseType = USERID_PARSE

		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, g_web_address, menu_filter, 0, 0)
		end

   	end

end

function getBalances(userID, menu_filter)

	g_web_address = GameGlobals.WEB_SITE_PREFIX..BALANCE_SUFFIX..userID

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, menu_filter, 0, 0)
	end

end

function ParseBalances( browserString )

	local s = 0 		-- start and end index of substring
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local regCredits = 0	-- regular credits
	local giftCredits = 0	-- gift credits


    s, strPos, result = string.find(browserString, "<ReturnCode>([0-9]+)</ReturnCode>")

	if result == "0" then

		s, strPos, regCredits = string.find(browserString, "<Balance>(.-)</Balance>", 1)

		if regCredits ~= nil then

			regCredits = tonumber(regCredits)

		end

		s, strPos, giftCredits = string.find(browserString, "<GiftBalance>(.-)</GiftBalance>", 1)

		if giftCredits ~= nil then

		  	giftCredits = tonumber(giftCredits)

		end


		return regCredits, giftCredits

	end

end
