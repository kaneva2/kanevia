dofile("MenuHelper.lua")

g_chat_type = ctTalk

CHAT_KEY_PRESSED_EVENT = "ChatKeyPressedEvent"
g_dispatcher = nil

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	g_dispatcher = dispatcher		
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, CHAT_KEY_PRESSED_EVENT, KEP.MED_PRIO )
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	local openChatEvent = Dispatcher_MakeEvent( KEP.dispatcher, CHAT_KEY_PRESSED_EVENT )	
	Dispatcher_QueueEvent( KEP.dispatcher, openChatEvent )
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------
function Dialog_OnDestroy(dialogHandle)
	
	Helper_Dialog_OnDestroy( dialogHandle )
end
