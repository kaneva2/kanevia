dofile("..\\Scripts\\GameGlobals.lua")

-- Default starting coords
DEFAULT_X  = 300
DEFAULT_Y  = 1


-- dh: DialogHandle
-- tag: xml tag that stores coords
function loadLocation(game, dh, tag)

	local success = false

	--Width and height of the game window.
	--Used to make sure windows dont open off screen.
	local screenHeight = Dialog_GetScreenHeight(dh)
	local screenWidth  = Dialog_GetScreenWidth(dh)

	local config = ClientEngine_OpenConfig( game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )
	if config ~= nil then

		local okfilterx, x = KEPConfig_GetNumber( config, tag.."X", DEFAULT_X )
		local okfiltery, y = KEPConfig_GetNumber( config, tag.."Y", DEFAULT_Y )

        --Dialog_MsgBox(" " .. x .. " " .. y .. " " .. okfilterx .. " " .. okfiltery)

		if okfilterx == 0 then
			okfilterx = false
		end
		if okfiltery == 0 then
			okfiltery = false
		end

		if okfilterx and okfiltery then

		    if x > screenWidth or x < 0 or y > screenHeight or y < 0 then
		        x = DEFAULT_X
				y = DEFAULT_Y
		    end

			Dialog_SetLocation( dh, x, y )
			
			success = true

		--else 
		--	x = DEFAULT_X
		--	y = DEFAULT_Y
		--	
		--	Dialog_SetLocation( dh, x, y )
		--	
		--	success = true
		end

	end
		
	return success

end

function saveLocation(game, dh, tag)

	local x = DEFAULT_X
	local y = DEFAULT_Y

	x = Dialog_GetLocationX( dh )
	y = Dialog_GetLocationY( dh )

	local config = ClientEngine_OpenConfig( game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )
	if config ~= nil then

		KEPConfig_SetNumber( config, tag.."X", x )
		KEPConfig_SetNumber( config, tag.."Y", y )

		KEPConfig_Save( config )
	end

end