--
--
--  Common functions to assist menus
--
--


-- Extensions for loading friend thumb or avatar pic
FRIEND_PIC_EXT      = ".fp"
FRIEND_THUMB_EXT    = ".fp_thumb"
GIFT_PIC_EXT        = ".gt"

-- Path for the default avatar image
DEFAULT_AVATAR_PIC_PATH = "/images/KanevaIconUnknownGender.jpg"

-- Event names
INFO_BOX_EVENT = "InfoBoxEvent"
HYPER_LINK_BOX_EVENT = "HyperLinkBoxEvent"

---------------------------------------------------------------------
-- Creates a notification message with an 'OK' button
---------------------------------------------------------------------

function createMessage( text )

	if g_game ~= nil and text ~= nil then

		gotoMenu( "InfoBox.xml", 0, 0 )
	
		local ev = Dispatcher_MakeEvent( KEP.dispatcher, INFO_BOX_EVENT )
	
		Event_EncodeString( ev, text )
	
		Dispatcher_QueueEvent( KEP.dispatcher, ev )

	end

end

---------------------------------------------------------------------
--Check if an file exist
---------------------------------------------------------------------
function FileExist(g_game,filename)
	return ClientEngine_TestPatchFileForUpdate( g_game,filename)
end

---------------------------------------------------------------------
--Decrypting an GLID id.
---------------------------------------------------------------------
function DecryptGlid(glid)
  local glidtable={}
  glidtable={"h","v","g","_","j","i","r","o","m","3"}
  local s = tostring(glid)
  local buff=""
  for i = 1, string.len(s) do
  num = tonumber(string.sub(s,i,i))+1
  buff = buff..glidtable[num]
  end
  return buff
end

---------------------------------------------------------------------
--Load and Display an Icon Texture
---------------------------------------------------------------------
function LoadIconTexture(g_game,g_t_item,eh,DialogHandle)
    local item = g_t_item

	--Load the encrypted glid version.
	local name = "Icons/"..DecryptGlid(item["GLID"])..".dds"

	--If the encrypted file does not exist load the non encrypted version.
	if FileExist(g_game,"\\MapsModels\\"..name )==0 then
		name = "Icons/"..tostring(item["GLID"])..".dds"
	end
	
	Element_AddTexture(eh, DialogHandle, name)
	local width = Element_GetTextureWidth(eh,DialogHandle)
	local height = Element_GetTextureHeight(eh,DialogHandle)
	local left 	= 0
	local top 	= 0
	local right = width
	local bottom= height
	Element_SetCoords(eh, left, top, right, bottom)
end



---------------------------------------------------------------------
-- Creates a notification message with a hyperlink button
---------------------------------------------------------------------
function createLinkMessage( text, buttontext, url )
	if g_game ~= nil and text ~= nil then
		gotoMenu( "HyperLinkBox.xml", 0, 0 )
		local ev = Dispatcher_MakeEvent( KEP.dispatcher, HYPER_LINK_BOX_EVENT )
		Event_EncodeString( ev, text )
		Event_EncodeString( ev, buttontext )
		Event_EncodeString( ev, url )
		Dispatcher_QueueEvent( KEP.dispatcher, ev )
	end
end

---------------------------------------------------------------------
-- These 2 functions find the least power of two.
-- Powers of two required for texture map
---------------------------------------------------------------------
function HelperPowTwo(num, tar)
	if num >= tar then
		return num
	else
		return HelperPowTwo(num * 2, tar)
	end
end

function LeastPowerofTwo(tar)
	return HelperPowTwo(2, tar)
end

---------------------------------------------------------------------
-- Displays avatar image for current profile
-- Parameters: string, number, string
-- ext: file extension for pic
---------------------------------------------------------------------
--[[
function displayThumbnail( labelName, ID, ext )

	if ID > 0 then
		Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, labelName)), true)
		eh = Image_GetDisplayElement(Dialog_GetImage(gDialogHandle, labelName))
		-- sh = Dialog_GetStatic(gDialogHandle, "lblThumb" .. i + 1)
		local filename = "../../CustomTexture/".. ID ..ext
		--Static_SetText(sh, g_t_friends[i + 1]["name"])
		-- Static_SetText(sh, filename) -- Used to debug finding file
		Element_AddTexture(eh, gDialogHandle, filename)
		local r = Element_GetTextureWidth(eh, gDialogHandle)
		local b = Element_GetTextureHeight(eh, gDialogHandle)
		r = LeastPowerofTwo(r)
		b = LeastPowerofTwo(b)
		Element_SetCoords(eh, 0, 0, r, b)
	end
end
]]--

-- Map a thumbnail to an image element
-- imgHandle:  	handle id of image control
-- fileName: 	path of desired thumbnail
function displayBrowserThumbnail( imgHandle, fileName )
	

	if fileName ~= nil then
		
		
		fileName = "../../CustomTexture/".. fileName
		Control_SetVisible(Image_GetControl(imgHandle), true)
		
		eh = Image_GetDisplayElement(imgHandle)
		Element_AddTexture(eh, gDialogHandle, fileName)

		local r = Element_GetTextureWidth(eh, gDialogHandle)
		local b = Element_GetTextureHeight(eh, gDialogHandle)

		r = LeastPowerofTwo(r)
		b = LeastPowerofTwo(b)
		Element_SetCoords(eh, 0, 0, r, b)
		
	end
	
end

-----------------------------------------------------------------------------------------------
--- Insert Black Box Thingy Behind Thumbnail Function
--- Parameters: Two images, a picture to display and a black box to display it on top of
		--Determine the Scaling:
	--Objective: Scale the smaller side accourding to the larger side to maintain the same aspect ratio
	--Case 1: Width of foreground greater than the width of background I.E greater ratio 16:9 > 4:3 
	--Determine the bounded wall and set lk;; 

-----------------------------------------------------------------------------------------------
function scaleImage(foregroundImgBoxHandle, backgroundImgBoxHandle,foregroundImage)
	backgroundImage = "wok_hud_new.tga"
	leftCord = 0
	topCord = 0
	rightCord = 1
	bottomCord = 1
     
	ih_fore = foregroundImgBoxHandle
      ih_back = backgroundImgBoxHandle
		
	foregroundWidth = Control_GetWidth(Image_GetControl(ih_fore))
	foregroundHeight = Control_GetHeight(Image_GetControl(ih_fore))
	backgroundWidth = Control_GetWidth(Image_GetControl(ih_back))
	backgroundHeight = Control_GetHeight(Image_GetControl(ih_back))
	
	Control_SetVisible(Image_GetControl(ih_fore), true)
	Control_SetVisible(Image_GetControl(ih_back), true)
	
	Control_SetSize(Image_GetControl(ih_back),backgroundWidth,backgroundHeight)
	
	eh_fore = Image_GetDisplayElement(ih_fore)
	eh_back = Image_GetDisplayElement(ih_back)
	
	Element_AddTexture(eh_fore, gDialogHandle, foregroundImage)

	local r_fore = Element_GetTextureWidth(eh_fore, gDialogHandle)
	local b_fore = Element_GetTextureHeight(eh_fore, gDialogHandle)
	
	backgroundRatio = backgroundWidth/(backgroundHeight*1.0)
	foregroundRatio = r_fore/(b_fore*1.0)
	
	backgroundWidth = Control_GetWidth(Image_GetControl(ih_back))
	backgroundHeight = Control_GetHeight(Image_GetControl(ih_back))

	r_fore = LeastPowerofTwo(r_fore)
	b_fore = LeastPowerofTwo(b_fore)
	
	foregroundLocationX = Control_GetLocationX(ih_fore)
	foregroundLocationY = Control_GetLocationY(ih_fore)
	backgroundLocationX = Control_GetLocationX(ih_back)
	backgroundLocationY = Control_GetLocationY(ih_back)

	--The RIGHT Idea for constricting size can be found right here
	if foregroundRatio < backgroundRatio then
		Control_SetSize(Image_GetControl(ih_fore),backgroundWidth*foregroundRatio,backgroundHeight)
		foregroundWidth = Control_GetWidth(Image_GetControl(ih_fore))
		Control_SetLocation(Image_GetControl(ih_fore),backgroundLocationX+((backgroundWidth-foregroundWidth)/2),backgroundLocationY)
		Element_SetCoords(eh_back, leftCord, topCord, rightCord, bottomCord)
		Element_SetCoords(eh_fore, 0, 0, r_fore, b_fore)

	elseif foregroundRatio > backgroundRatio then
		Control_SetSize(Image_GetControl(ih_fore),backgroundWidth,backgroundHeight*(1.0/foregroundRatio))
		foregroundHeight = Control_GetHeight(Image_GetControl(ih_fore))
		Control_SetLocation(Image_GetControl(ih_fore),backgroundLocationX,backgroundLocationY+((backgroundHeight-foregroundHeight)/2))
		Element_SetCoords(eh_back, leftCord, topCord, rightCord, bottomCord)
		Element_SetCoords(eh_fore, 0, 0, r_fore, b_fore)

	else
		Control_SetSize(Image_GetControl(ih_fore),backgroundWidth,backgroundHeight)
		Control_SetLocation(Image_GetControl(ih_fore),backgroundLocationX,backgroundLocationY)
		Element_SetCoords(eh_back, leftCord, topCord, rightCord, bottomCord)
		Element_SetCoords(eh_fore, 0, 0, r_fore, b_fore)
	end
	
end




----------------------------------------------------------------------------


---------------------------------------------------------------------
-- Helper to open the various menus that spawn off of this one.
---------------------------------------------------------------------
function gotoMenu(menu_path, bClose, bModal)

	dh = Dialog_Create( menu_path );
	if dh == 0 then
		Dialog_MsgBox("Menu [" .. menu_path .. "] not found.")
	end

	if bModal == 1 and dh ~= 0 then
		Dialog_SetModal(dh, 1)
		Dialog_SendToBack( gDialogHandle )
	else 
		if dh ~= 0 then
			Dialog_SetModal(dh, 0)
		end
	end
	-- close this dialog
	if bClose == 1 then
		DestroyMenu(gDialogHandle)
	end

	return dh

end 

---------------------------------------------------------------------
--
-- makes a deep copy of a given table (the 2nd param is optional and for internal use)
-- circular dependencies are correctly copied.
--
---------------------------------------------------------------------
function tcopy(t, lookup_table)

	local copy = {}
	for i,v in pairs(t) do
		if type(v) ~= "table" then
			copy[i] = v
  		else
   			lookup_table = lookup_table or {}
   			lookup_table[t] = copy
  			if lookup_table[v] then
    				copy[i] = lookup_table[v] -- we already copied this table. reuse the copy.
   			else
    				copy[i] = tcopy(v,lookup_table) -- not yet copied. copy it.
   			end
  		end
 	end

	return copy
end

---------------------------------------------------------------------
--
-- Switch a button control between enabled and disabled
--
---------------------------------------------------------------------

-- Enable or disable the selected button
function setButtonState( buttonHandle, state )

	if buttonHandle ~= nil then
		ch = Button_GetControl(buttonHandle)
		if ch ~= nil then
			Control_SetEnabled(ch, state)
		end
	end

end

-- setButtonState can set the state of any control.  
-- Copying for use with other controls and clarity
setState = setButtonState

---------------------------------------------------------------------
--
-- Remove unsupported HTML tags from strings
--
---------------------------------------------------------------------
function stripTags( text )

	if text ~= nil then
		-- Begin wierd control character stripping
		
		-- Change into typical tag format
		text = string.gsub(text, "&lt;", "<") 	-- left bracket
		text = string.gsub(text, "&gt;", ">") 	-- right bracket
		
		text = string.gsub(text, "</div>", "\n") -- line break after closing div
		text = string.gsub(text, "%b<>", "") -- remove anything that looks like html
		text = string.gsub(text, "%b&;", "")
		text = string.gsub(text, "nbsp;", "") 	-- backspace		

	end

    return text

end

---------------------------------------------------------------------
--
-- Function by Jonny when he was really tired and thought this would get a laugh
--
---------------------------------------------------------------------
-- staticName:  name of a static label used to split up the message using
--				the controls word wrap option,
--
-- msg: string we are breaking up
--
function letsGetFunky(msg, staticName)
	local t = {}
	sh = Dialog_GetStatic(gDialogHandle, staticName)
	wid = Control_GetWidth(Static_GetControl(sh))
	local line = ""
	--Dialog_MsgBox("Whole message:\n" .. msg)
	while msg ~= "" do
		-- make var adjustments
		local char = string.sub(msg, 1, 1)
		line = line .. char
		msg = string.sub(msg, 2, -1)
		if char == "\n" or Static_GetStringWidth(sh, line) > wid then
			-- This is a line break.
			if char ~= "\n"  and string.find(line, " ") ~= nil then
				-- There was a space in there that needs to be broken.
				local breakspcpos = string.find(line, " ")
				while string.find(line, " ", breakspcpos + 1) ~= nil do
					breakspcpos = string.find(line, " ", breakspcpos + 1)
				end         
				msg = string.sub(line, breakspcpos + 1, -1) .. msg
				line = string.sub(line, 1, breakspcpos)
			else
				if char == "\n" then
					-- strip off the newline
					line = string.sub(line, 1, -2)
				end
			end
            table.insert(t, line)
			line = ""
		end
		--Dialog_MsgBox(line)

	end
	table.insert(t, line)

	return t	
end

-- Convert the string from a GetBrowserPage call to a lua boolean value
function strToBool( strValue )

	if strValue == "1" then
		return true
	else
	    return false
	end

end

-- Convert the string from a GetBrowserPage call to a lua boolean value
function numToBool( numValue )

	if numValue == 1 then
		return true
	else
	    return false
	end

end

--Sort a table  of tables by a specific field
--This is for sorting by texture name in wok menus to hopefully help
--with video memory usage and render time in game when using inventory, bank,
--vendor, etc menus
--
-- theTable - A table of tables
-- index - the field to access within table
--          i.e.  theTable[1]["texture_name"] == "icons.tga"
--                index = "texture_name"
-- --Killgore
function sortTableByField( theTable, index )

	if theTable == nil or #theTable < 1 then
  		--not a valid table to sort
	    return theTable
	end

	t_texSort = {}
	t_buckets = {}
	bucketCnt = 0

	for i=1,#theTable do

    	nameIndex = theTable[i][index]

		if t_texSort[nameIndex] == nil then

			--found a new bucket, add to sort table
		    bucketCnt = bucketCnt + 1
			t_buckets[bucketCnt] = nameIndex

		    t_texSort[nameIndex] = {}

			--add table to new bucket
			table.insert(t_texSort[nameIndex], theTable[i])

		else

			--add table to existing bucket
			table.insert(t_texSort[nameIndex],theTable[i])

		end

	end


	t_sortFlatten = {}

	for i=1,bucketCnt do

		-- go through each bucket
	    for j=1,#t_texSort[t_buckets[i]] do

			--add to "flattened" table (table of tables)
	        table.insert(t_sortFlatten, t_texSort[t_buckets[i]][j])

	    end

	end

	return t_sortFlatten

end

function decodeInventoryEvent( invEvent, decodeSellPrice )

	local mainTable = {}	
	
	if invEvent ~= nil then
	
		local num_items = Event_DecodeNumber( invEvent )
	
		for i = 0, num_items-1 do
		
			local t_item 			= {}

			local GLID 			= Event_DecodeNumber( invEvent )
			local quantity 		= Event_DecodeNumber( invEvent )
			local name 			= Event_DecodeString( invEvent )

			local armed 		= false
			local gift			= false
			
			if quantity < 0 then
				quantity = -quantity -- if < 0 armed
				armed = true
			end
			
			if GLID < 0 then
				GLID = -GLID -- if < 0 gift
				gift = true
			end	            
            
			local use_type		= Event_DecodeNumber( invEvent )
			local texture_name 	= Event_DecodeString( invEvent )

			-- texture coords
			local left			= Event_DecodeNumber( invEvent )
			local top			= Event_DecodeNumber( invEvent )
			local right			= Event_DecodeNumber( invEvent )
			local bottom		= Event_DecodeNumber( invEvent )
			
			-- now always get it if decodeSellPrice then
		
            t_item["sell_price"] = Event_DecodeNumber(invEvent)
		
			-- end
			
			local passCount = Event_DecodeNumber(invEvent)
			local itemPassList = {}

			for pC=1,passCount do

				table.insert(itemPassList, Event_DecodeNumber(invEvent))

			end

			local isDefault = false
			local defaultItem = Event_DecodeNumber(invEvent)
			if defaultItem ~= 0 then 
				isDefault = true
			end 

            local itemType = Event_DecodeNumber( invEvent )

            itemType = tonumber(itemType)
            
            --item description
            local desc = Event_DecodeString( invEvent )

			t_item["GLID"]			= math.abs(GLID)
			t_item["name"]			= name
			t_item["quantity"] 		= quantity
			t_item["use_type"] 		= use_type
			t_item["texture_name"] 		= texture_name
			t_item["texture_coords"]	= {left, top, right, bottom}
			t_item["is_armed"]		= armed
			t_item["is_gift"]		= gift
			t_item["pass_list"] 	= itemPassList
			t_item["is_default"] 		= isDefault
            t_item["type"]          = itemType
            t_item["description"]   = desc

			t_item["can_trade"] = true	
			
			if t_item["is_armed"] or t_item["is_gift"] or t_item["is_default"] then
				t_item["can_trade"] = false
			end   			

            table.insert(mainTable, t_item)

		end	

	end 
	
	return mainTable

end

function getIconPreference()

	local config = ClientEngine_OpenConfig( g_game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )
	
	if config ~= nil then

		local okfilter, iconPref = KEPConfig_GetString( config, "ShowIcons", tostring(g_useIcons) )
		
		if iconPref == "false" then
			return false
		end
	
	end

	return true

end


