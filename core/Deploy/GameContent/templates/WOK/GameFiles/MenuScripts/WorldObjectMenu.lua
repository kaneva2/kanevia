dofile("MenuHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")

-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--
-- end 

WORLD_OBJ_MENU_EVENT = "WorldObjectMenuEvent"
EVENT_TEXTURE_SEL = "TextureSelectionEvent"
EVENT_TEXTURE_SELECTED = "textureSelectedEvent"
CLOSE_WORLDOBJ_EVENT = "CloseWorldObjEvent"

PICTURE_FRAME_SELECTED = "PictureFrameSelectedEvent"

g_objId = -1

g_game = nil
g_textureSelMenu = 0

g_textureAltered = false

g_pictureFrameSelMenu = 0

g_t_btnHandles = {}

g_lasttexture = nil;

--
-- update g_objId
--
function updateId(dispatcher, fromNetid, event, eventid, filter, objectid)	

  	Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "updating id" ) 
	
	g_objId  = Event_DecodeNumber( event );
		
  	Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, "setting id = "..g_objId ) 	

	return KEP.EPR_OK
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	
	-- TODO register any of your handlers here like the following....
	-- ParentHandler_RegisterEventHandler( dispatcher, handler, "renderTextHandler", 1,0,0,0, "RenderTextEvent", KEP.HIGH_PRIO ) 
	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "updateId", 1,0,0,0, WORLD_OBJ_MENU_EVENT, KEP.HIGH_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "textureListCloseHandler", 1,0,0,0, EVENT_TEXTURE_SELECTED, KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "pictureFrameListCloseHandler", 1,0,0,0, PICTURE_FRAME_SELECTED, KEP.MED_PRIO )		
	ParentHandler_RegisterEventHandler( dispatcher, handler, "CloseWorldObjHandler", 1,0,0,0, CLOSE_WORLDOBJ_EVENT, KEP.HIGH_PRIO )

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	-- TODO register any new event types here like the following...
	-- ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "LuaEvent", KEP.MED_PRIO )	
	
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, EVENT_TEXTURE_SELECTED, KEP.MED_PRIO )	
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, EVENT_TEXTURE_SEL, KEP.MED_PRIO )		
	
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, PICTURE_FRAME_SELECTED, KEP.MED_PRIO )		
	
	g_dispatcher = dispatcher	
end

function CloseWorldObjHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	--Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "brgin event handler... "..filter )
	local oldHandle = Event_DecodeNumber(event)


	if oldHandle == gDialogHandle then
		DestroyMenu(gDialogHandle)
	end

	return KEP.EPR_CONSUMED

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	
	
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end	

	-- TODO init here
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	if g_game == 0 then
		g_game = nil
	end
	
	getButtonHandles()
	
	if ClientEngine_SetMenuSelectMode ~= nil then
		ClientEngine_SetMenuSelectMode( g_game, 1.0 )
	end

	Control_SetVisible(g_t_btnHandles["btnRepeatTexture"], false)

	if KEPFile_ReadLine == nil then 
      Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPFILE_FN )
    end
    
	local f, errMsg  = ClientEngine_OpenFile( g_game, "lastasset.dat", "r")	
	if( f ~= nil) then
		local err, errMsg
		err, g_lasttexture, errMsg = KEPFile_ReadLine( f )
		KEPFile_CloseAndDelete( f )
		Control_SetVisible(g_t_btnHandles["btnRepeatTexture"], true)
	else
		g_lasttexture = nil
	end

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	if ClientEngine_SetMenuSelectMode ~= nil then
		ClientEngine_SetMenuSelectMode( g_game, 0.0 )
	end
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

---------------------------------------------------------------------
--
-- Save Button - save changes
--
---------------------------------------------------------------------
function btnSave_OnButtonClicked( buttonHandle )	
	--Dialog_MsgBox(g_objId)
	if g_textureAltered == true then
		ClientEngine_SaveWldObjectChanges(g_game, g_objId)
	end
	-- close this dialog
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------
--
-- Cancel Button - cancel and discard changes
--
---------------------------------------------------------------------
function btnCancel_OnButtonClicked( buttonHandle )
  
  ClientEngine_CancelWldObjectChanges(g_game, g_objId)			
  
	-- close this dialog
	DestroyMenu(gDialogHandle)
	
end

---------------------------------------------------------------------
--
-- Change Texture Button - show texture selection menu
--
---------------------------------------------------------------------
function btnChangeTexture_OnButtonClicked( buttonHandle )
	Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "changing texture..." ) 	
	g_textureSelMenu = Dialog_Create( "TextureBrowser.xml");
  
	textureSelEvent = Dispatcher_MakeEvent( KEP.dispatcher, EVENT_TEXTURE_SEL )	
	Event_EncodeNumber( textureSelEvent, -1 )
	Dispatcher_QueueEvent( KEP.dispatcher, textureSelEvent )
	Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "queued changing texture event..." )
  
end

---------------------------------------------------------------------
--
-- Hang Picture Button - show picture frame selection menu
--
---------------------------------------------------------------------
function btnHangPicture_OnButtonClicked( buttonHandle )

  Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "selecting picture frames..." ) 	
  g_pictureFrameSelMenu = Dialog_Create( "PictureFrameSelect.xml");
  
end

function btnResetTexture_OnButtonClicked( buttonHandle )
	-- Reset the custom texture
	ClientEngine_ApplyCustomTextureWldObject(g_game, g_objId, -1, GameGlobals.FULLSIZE)
	g_textureAltered = true

	local f, errMsg  = ClientEngine_DeleteFile( g_game, "lastasset.dat")	

	btnSave_OnButtonClicked()
end

function btnRepeatTexture_OnButtonClicked( buttonhandle )
	if g_lasttexture ~= nil then
		ClientEngine_ApplyCustomTextureWldObject(g_game, g_objId, g_lasttexture, GameGlobals.FULLSIZE)
		g_textureAltered = true

		btnSave_OnButtonClicked()
	end
end

---------------------------------------------------------------------
--
-- Texture List menu Close Handler
--
---------------------------------------------------------------------
function textureListCloseHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

  Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "texture selection event received" ) 
  
	local assetId = Event_DecodeNumber( event )

	--Dialog_Destroy(g_textureSelMenu)

	if assetId > 0 then
		ClientEngine_ApplyCustomTextureWldObject(g_game, g_objId, assetId, GameGlobals.FULLSIZE)
		g_textureAltered = true
	end
end


---------------------------------------------------------------------
--
-- Picture frame list menu Close Handler
--
---------------------------------------------------------------------
function pictureFrameListCloseHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

  Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "picture frame selection event received" ) 
  
	-- retrieve picture frame id
  local objId = Event_DecodeNumber( event )

	if objId > 0 then
    	ClientEngine_UseInventoryItem(g_game, objId, filter) -- filter is invType
	end
end

---------------------------------------------------------------------
-- Keyboard Handler
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
      -- ESC key
      if key == 27 then
            -- close this menu
            DestroyMenu(gDialogHandle)
      end
end


---------------------------------------------------------------------
-- Mouse Over
---------------------------------------------------------------------
function getButtonHandles()
--g_t_btnHandles


	local ch = Dialog_GetControl(gDialogHandle, "btnCancel")
	if ch ~= nil then
     	g_t_btnHandles["btnCancel"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSave")
	if ch ~= nil then
     	g_t_btnHandles["btnSave"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnRepeatTexture")
	if ch ~= nil then
     	g_t_btnHandles["btnRepeatTexture"] = ch
	end
end

function Dialog_OnMouseMove(dialogHandle, x, y)

    local sh = Dialog_GetStatic(dialogHandle, "lblStatus")

	if sh ~= nil then

		if Control_ContainsPoint(g_t_btnHandles["btnCancel"], x, y) ~= 0 then
			Static_SetText(sh, "Cancel and Close")
		elseif Control_ContainsPoint(g_t_btnHandles["btnSave"], x, y) ~= 0 then
	    	Static_SetText(sh, "Save and Close")
		else
            Static_SetText(sh, " ")
		end
	end

end
