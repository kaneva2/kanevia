dofile("MenuHelper.lua")

-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--
-- end 

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	
	-- TODO register any of your handlers here like the following....
	-- ParentHandler_RegisterEventHandler( dispatcher, handler, "renderTextHandler", 1,0,0,0, "RenderTextEvent", KEP.HIGH_PRIO ) 
	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	-- TODO register any new event types here like the following...
	-- ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "LuaEvent", KEP.MED_PRIO )


end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	-- TODO init here
	g_hText = Dialog_GetStatic( gDialogHandle, "lblDescription" )
	
	Static_SetText( g_hText, "<b>Topics</b><br><br>Please select a topic from the left.")
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

---------------------------------------------------------------------
--
-- Keyboard Handler
--
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)

	-- ESC key
	if key == 27 then

		-- close this menu
		DestroyMenu(gDialogHandle)

	end

end

function btnClose_OnButtonClicked( buttonHandle )
	DestroyMenu(gDialogHandle)	
end

function btnBuying_OnButtonClicked( buttonHandle )
  Static_SetText( g_hText, "<b>Buying</b><br><br>�	You can purchase items in this area. To do this, simply walk up to a shopkeeper and hit the TAB key. This will open the shopkeeper inventory menu. <br><br>�	To buy an item, highlight it by left-clicking on it and then click the �buy� button.	<br><br> ")
end

function btnCustomize_OnButtonClicked( buttonHandle )
  Static_SetText( g_hText, "<b>Customizing</b><br><br>�	Once you have purchased some items, you�ll want to use them!<br><br>�	To put on new clothing, simply press the �I� key to open your inventory. You�ll see any clothing you have purchased in this menu. To put something on, just highlight it by left-clicking on it and then click the �use� button.")
end




function btnChat_OnButtonClicked( buttonHandle )
  Static_SetText( g_hText, "<b>Chatting</b><br><br>�	There are two ways to chat in the world. The first is General Chat. This is chat that people in near proximity to you will see. To say something in chat, hit the Enter key, type your message, and then hit Enter again. <br><br>�	You can also send another player a private chat message, also called a Tell. To do this, press the Enter key, then type /tell and the person�s game and then your message. So if you wanted to send a tell to someone named �Fred� you would type, �/tell Fred Hi Fred, how are you!� and then hit the Enter key.")
end

function btnPM_OnButtonClicked( buttonHandle )
  Static_SetText( g_hText, "<b>Chat</b><br><br>�	You can send another player a private chat message, also called a Tell. To do this, press the Enter key, then type /tell and the person�s game and then your message. So if you wanted to send a tell to someone named �Fred� you would type, �/tell Fred Hi Fred, how are you!� and then hit the Enter key.")
end





function btnTravel_OnButtonClicked( buttonHandle )
  Static_SetText( g_hText, "<b>Travel</b><br><br>�	The travel menu displays all of the places in the world. Click on the name to travel to the location. ")
end

function btnInventory_OnButtonClicked( buttonHandle )
  Static_SetText( g_hText, "<b>Inventory</b><br><br>�	This button will open your inventory. To learn more about how to use items in your inventory you can read the �What do I do in this world?� section the help menu. That section will tell you how to wear clothing and place objects in your home.")
end

