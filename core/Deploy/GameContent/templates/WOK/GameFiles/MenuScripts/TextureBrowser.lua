dofile("MenuHelper.lua")
dofile("..\\Scripts\\BrowserIds.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("PageButtons.lua")
dofile("MenuLocation.lua")
dofile("CommonFunctions.lua")


DYNAMIC_OBJ_MENU_EVENT = "DynamicObjectMenuEvent"
EVENT_TEXTURE_SELECTED = "textureSelectedEvent"
EVENT_TEXTURE_SEL = "TextureSelectionEvent"
WEB_ADDRESS = GameGlobals.WEB_SITE_PREFIX.."kgp/textureslist.aspx?"
WEB_ADDRESS_FEATURED = GameGlobals.WEB_SITE_PREFIX.."kgp/textureslist.aspx?type=f&channelId=" .. GameGlobals.FEATURED_CHANNEL
WEB_ADDRESS_SEARCH = GameGlobals.WEB_SITE_PREFIX.."kgp/textureslist.aspx?type=s"
WEB_ADDRESS_ADD = GameGlobals.WEB_SITE_PREFIX.."kgp/textureslist.aspx?type=a"

MY_PATTERNS_TEXT = "Patterns are images that are stretched over an object to give it a new look.  Patterns that are in your media library are shown below.  You can upload an image from your computer to use as a pattern, or choose from thousands of patterns uploaded by others on the Featured Patterns tab."
FEATURED_PATTERNS_TEXT = "Patterns are images that are stretched over an object to give it a new look.  You can choose from Featured Patterns below, or upload an image from your computer to use as a pattern.  When you choose a Featured Pattern, it is added to My Patterns."
SEARCH_RESULTS_TEXT = "When you choose a pattern from Search Results, it is added to My Patterns."

g_game = nil
g_assetId = 4425
g_objId = -1

TAB_KANEVA = 1
TAB_MY = 2
g_onTab = TAB_MY
g_cur_webaddress = WEB_ADDRESS

-- Hold current friend data returned from server
g_message = ""

-- Total number of friend records
g_totalNumRec = 0

-- Maximum number of places for server to return in one request
-- places per page
g_imagesPerPage = 10

--[[
-- Current page of data
g_curPage = 1

-- Number of pages matching buttons between previous and next arrows
-- See menu specs
g_window = 4

-- Which set I am on (i.e. 1 would be pages 1,2,3,4; 2 would be 5,6,7,8
g_set = 1
]]--

g_t_textures = {} 
g_textures_count = 0
g_selected_texture = -1
g_display_texture = -1

--[[
-- Maximum page number with data
g_pageCap = 0
-- Maximum g_window size set we can have
g_setCap = 0
]]--

g_searching = false

---------------------------------------------------------------------
--
-- Confirmation Close Handler
--
---------------------------------------------------------------------
function textureSelectionEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )  
  
	g_objId = Event_DecodeNumber( event )

  -- set url
	local sh = Dialog_GetStatic(gDialogHandle, "ctrlBrowser")
	if sh ~= 0 then
		Static_SetText(sh, GameGlobals.WEB_SITE_PREFIX.."kgp/TexturesList.aspx")
	end
end

---------------------------------------------------------------------
--
-- Cancel Button - cancel and discard changes
--
---------------------------------------------------------------------
function btnCancel_OnButtonClicked( buttonHandle )    
  
	-- close this dialog
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------
--
-- Save Button - save changes
--
---------------------------------------------------------------------
function btnSave_OnButtonClicked( buttonHandle )
	
	if KEPFile_ReadLine == nil then 
      Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPFILE_FN )
    end

	Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "saving" )
	if g_selected_texture >= 0 then
	
		local assetid = g_t_textures[g_selected_texture + 1]["asset_id"] 
		local f, errMsg  = ClientEngine_OpenFile( g_game, "lastasset.dat", "w")		

		if g_objId >= 0 then
			-- save the last assetid
			KEPFile_WriteLine( f, tostring(assetid) )
			
			ClientEngine_ApplyCustomTexture(g_game, g_objId, assetid, GameGlobals.FULLSIZE)
			DestroyMenu(gDialogHandle)
		else
			--save the last asset id
			KEPFile_WriteLine( f, tostring(assetid) )
			
			textureSelectedEvent = Dispatcher_MakeEvent( KEP.dispatcher, EVENT_TEXTURE_SELECTED )
			Event_EncodeNumber( textureSelectedEvent, assetid)
			Dispatcher_QueueEvent( KEP.dispatcher, textureSelectedEvent )
			
			if (g_searching == true) then
				g_cur_webaddress = WEB_ADDRESS_ADD .. "&assetId=" .. assetid
				ClientEngine_GetBrowserPage(g_game, g_cur_webaddress, WF.TEXTURE_LIST,  1, 10)
			end
			
			DestroyMenu(gDialogHandle)
		end
		KEPFile_CloseAndDelete( f )
	end
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
  	ParentHandler_RegisterEventHandler( dispatcher, handler, "textureSelectionEventHandler", 1,0,0,0, EVENT_TEXTURE_SEL, KEP.MED_PRIO )   
	ParentHandler_RegisterEventHandler( dispatcher, handler, "BrowserPageReadyHandler", 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	if Dialog_Create == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
	end

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, EVENT_TEXTURE_SELECTED, KEP.MED_PRIO )
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	
	-- TODO init here
  	g_game = Dispatcher_GetGame(KEP.dispatcher)
  	
  	if KEPConfig_GetString == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end
  	  	
	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, WEB_ADDRESS, WF.TEXTURE_LIST, g_curPage, g_imagesPerPage)
	end


	clear()

	-------------------------------remove me----------------
	--local c = Static_GetControl(Dialog_GetStatic( gDialogHandle, "lblQuery" ))
	--Control_SetVisible(c, true)
	---------------------------------------------------------

	loadLocation(g_game, dialogHandle, "TexBrowser")

	initButtons()

	-- setSelectionColor is called from PageButtons.lua to highlight the first
	-- button when the menu is opend.
	local bh = Dialog_GetButton(dialogHandle, "btnRange1")
	if bh ~= nil then
		setSelectionColor( bh )
	end
	
	btnMy_OnButtonClicked(Dialog_GetButton(dialogHandle, "btnMy"))

	
end
---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	saveLocation(g_game, dialogHandle, "TexBrowser")	
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.TEXTURE_LIST then
		g_message = Event_DecodeString( event )
		--local sh = Dialog_GetStatic( gDialogHandle, "lblQuery" )
		--Static_SetText(sh, g_message)
	    ParseTextureData(gDialogHandle)
		showThumbnails()
		return KEP.EPR_CONSUMED
	elseif filter == WF.PLAYLIST then
  		g_message = Event_DecodeString( event )
		--local sh = Dialog_GetStatic( gDialogHandle, "lblQuery" )
		--Static_SetText(sh, g_message)
		ParseTextureData(gDialogHandle)
		showThumbnails()
		return KEP.EPR_CONSUMED
	end
end

function ParseTextureData(dialogHandle)

	local s = 0     			-- start of substring
	local numRec = 0   		-- number of records in this chunk
	local strPos = 0    		-- current position in string to parse
	local result = ""  		-- return string from find temp

	local name = "" 				-- name of the asset
	local file_size = 0 			-- file size of the image
	local content_extension = ""		-- the content extension of the image
	local asset_id = 0			-- what we are really looking for
	local created_date = ""			-- Date this image was created
	local created_time = ""
	local thumbnail_medium_path = "" 	-- Path to the medium sized thumbnails
	
	local parseStr = ""
	local sh = Dialog_GetStatic( gDialogHandle, "lblQuery" )
	s, e, result = string.find(g_message, "<ReturnCode>([0-9]+)</ReturnCode>")

	if result == "0" then
	
	    numRec = calculateRanges()

		g_t_textures = {}
		g_textures_count = 0
		--g_curPage = 1
		--g_set = 1 
		
		
		for i=1, numRec do
			local imagestr = ""
			local trash = 0
			local s = 0
			s, strPos, imagestr = string.find(g_message, "<Texture>(.-)</Texture>", strPos)
			s, trash, name = string.find(imagestr, "<name>(.-)</name>", 1)
			s, trash, file_size = string.find(imagestr, "<file_size>([0-9]+)</file_size>", 1)
			s, trash, content_extension = string.find(imagestr, "<content_extension>(.-)</content_extension>", 1)
			s, trash, asset_id = string.find(imagestr, "<asset_id>([0-9]+)</asset_id>", 1)
			s, trash, created_date, created_time = string.find(imagestr, "<created_date>(%w+%-%w+%-%w+)T(%w+:%w+:%w+)", 1)
 			--s, trash, created_date = string.find(imagestr, "<created_date>(.-)</created_date>", 1)
			s, trash, imageUrl = string.find(imagestr, "<thumbnail_medium_path>(.-)</thumbnail_medium_path>", 1)


			local tex = {}
			tex["name"] = name
			tex["file_size"] = file_size
			tex["content_extension"] = content_extension
			tex["asset_id"] = tonumber(asset_id)
			tex["created_date"] = prettyDateFormat(created_date) .. " - " .. prettyTimeFormat(created_time) 
			--tex["thumbnail_medium_path"] = imageUrl
			local full_path = ClientEngine_DownloadTexture( g_game, GameGlobals.CUSTOM_TEXTURE, asset_id, imageUrl, GameGlobals.THUMBNAIL )
			
			-- Strip off begnning of file path leaving only file name
			tex["thumbnailFile"] = string.gsub(full_path,".-\\", "" )
			
			table.insert(g_t_textures, tex)
			g_textures_count = g_textures_count + 1
			-- Pull me out to timer for responsiveness.
			--ClientEngine_ApplyCustomTextureWldObject(g_game, -1, asset_id, GameGlobals.THUMBNAIL)  -- Ensure that this texture is downloaded.				
		end
		

		alignButtons()
 	end
end

function calculateRanges()

		local numRec = 0
		local results_h = Dialog_GetStatic( gDialogHandle, "lblResults")
		local text = "Showing "
		local s, strPos


        local high = g_curPage * g_imagesPerPage
		local low = ((g_curPage - 1) * g_imagesPerPage) + 1

		s, strPos, g_totalNumRec = string.find(g_message, "<TotalNumberRecords>([0-9]+)</TotalNumberRecords>")
		s, strPos, numRec = string.find(g_message, "<NumberRecords>([0-9]+)</NumberRecords>")

		g_totalNumRec = tonumber(g_totalNumRec)

		g_pageCap = math.ceil(g_totalNumRec / g_imagesPerPage)
		g_setCap = math.ceil(g_pageCap / g_window)



	    if high > g_totalNumRec then
	      	high = g_totalNumRec
		end

		if results_h then
			text = text..tostring(low).." - "..tostring(high).." of "..tostring(g_totalNumRec)
			Static_SetText(results_h, text)
		end

		if g_InitBtnText then
		    setButtonText()
		    g_InitBtnText = false
		end

		return numRec

end

function prettyDateFormat(datestr)
	local year = string.sub(datestr, 1, 4)
	local month = string.sub(datestr, 6, 7)
	local day = string.sub(datestr, 9, 10)
	local monthname = "January"
	if month == "01" then
		monthname = "January"
	elseif month == "02" then
		monthname = "Feburary"
	elseif month == "03" then
		monthname = "March"
	elseif month == "04" then
		monthname = "April"
	elseif month == "05" then
		monthname = "May"
	elseif month == "06" then
		monthname = "June"
	elseif month == "07" then
		monthname = "July"
	elseif month == "08" then
		monthname = "August"
	elseif month == "09" then
		monthname = "September"
	elseif month == "10" then
		monthname = "October"
	elseif month == "11" then
		monthname = "November"
	elseif month == "12" then
		monthname = "December"
	end
	return monthname .. " " .. day .. ", " .. year
end

function prettyTimeFormat(timestr)
	local hour = string.sub(timestr, 1, 2)
	local min = string.sub(timestr, 4, 5)
	local ampm = ""
	if tonumber(hour) == 0 then
		hour = 12
		ampm = "AM"
	elseif tonumber(hour) < 12 then
		hour = tonumber(hour)
		ampm = "AM"
	else
		hour = tonumber(hour) - 12
		ampm = "PM"
	end
	return hour .. ":" .. min .. " " .. ampm
end

function showThumbnails()

	clear()
	local temp = g_textures_count - 1
	
	for i = 0, g_textures_count - 1 do
			Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgMain")), false)
			 ih = Dialog_GetImage(gDialogHandle, "ImgThumb"..(i + 1))
			 ihBack = Dialog_GetImage(gDialogHandle, "ImgThumbBack"..(i + 1))
			
     		 fileName = "../../CustomTexture/".. g_t_textures[i + 1]["thumbnailFile"]

			 scaleImage(ih,ihBack,fileName)

			 --displayBrowserThumbnail(ih, g_t_textures[i + 1]["thumbnailFile"])

			 sh = Dialog_GetStatic(gDialogHandle, "lblThumb" .. i + 1)
				
      	       Static_SetText(sh, g_t_textures[i + 1]["name"])
        
	end

end

function clear()
	selectTexture(-1)
	for i = 0, 10 do
		Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "ImgThumb"..(i + 1))), false)
		Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "ImgThumbBack"..(i + 1))), false)
		Static_SetText(Dialog_GetStatic(gDialogHandle, "lblThumb"..(i + 1)), "")
	end 
	--Static_SetText(Dialog_GetStatic( gDialogHandle, "lblQuery" ), "")
end

function Dialog_OnMouseMove( dialogHandle, x, y )
	if g_selected_texture < 0 then
		local found = false
		for i = 0, g_textures_count - 1 do
			ch = Image_GetControl(Dialog_GetImage(gDialogHandle, "ImgThumb"..(i + 1)))
			ch2 = Static_GetControl(Dialog_GetStatic(gDialogHandle, "lblThumb"..(i + 1)))
			if Control_ContainsPoint(ch, x, y) ==  1 or Control_ContainsPoint(ch2, x, y) == 1 then
				displayTexture(i + 1, false)
				found = true
			end
		end
		if found == false then
			displayTexture(-1, false)
		end
	end
	--	displayTexture(g_selected_texture)
	--end
end

function displayTexture(index, overwrite)
	if g_selected_texture >= 0 and overwrite == false then
		return
	end
	if index < 0 then
		Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgMain")), false)
	end
	if g_displaytexture ~= index then
		if index < 0 then
			g_displaytexture = -1
			Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgMain")), false)
		else
			Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgMain")), true)
			g_displaytexture = index
			
			local ihFore = Dialog_GetImage(gDialogHandle, "imgMain")
			local ihBack = Dialog_GetImage(gDialogHandle, "imgTextureBackground")

			--eh = Image_GetDisplayElement(Dialog_GetImage(gDialogHandle, "imgMain"))
			local fileName = "../../CustomTexture/".. g_t_textures[index]["thumbnailFile"]
			--local filename = "../../CustomTexture/".. g_t_textures[index]["asset_id"] ..".ct_thumb"
			scaleImage(ihFore,ihBack,fileName)
			Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgTextureBackground")), false)

			--Element_AddTexture(eh, gDialogHandle, filename)

			--local r = Element_GetTextureWidth(eh, gDialogHandle)
			--local b = Element_GetTextureHeight(eh, gDialogHandle)
			--r = LeastPowerofTwo(r)
			--b = LeastPowerofTwo(b)
			--Element_SetCoords(eh, 0, 0, r, b)
			
		end
		if index > 0 and g_textures_count > 0 then
			Static_SetText(Dialog_GetStatic(gDialogHandle, "txtSelected"), g_t_textures[g_displaytexture]["asset_id"] )
			--[[
			sh = Dialog_GetStatic( gDialogHandle, "lblQuery" )
			local metadata = "Name: ".. g_t_textures[g_displaytexture]["name"] .. "\n"
			metadata = metadata .. "Asset ID: " .. g_t_textures[g_displaytexture]["asset_id"] .. "\n"
			metadata = metadata .. "File Size: " .. g_t_textures[g_displaytexture]["file_size"] .. "\n"
			metadata = metadata .. "Uploaded: " .. g_t_textures[g_displaytexture]["created_date"] .. "\n"
			Static_SetText(sh, metadata)
			]]--
		else
			Static_SetText(Dialog_GetStatic(gDialogHandle, "txtSelected"), "None")
			--Static_SetText(Dialog_GetStatic( gDialogHandle, "lblQuery" ), "")
		end
	end
end

function selectTexture(index)
	g_selected_texture = index
	if index >= 0 then
		local xpos = Control_GetLocationX(Image_GetControl(Dialog_GetImage(gDialogHandle, "ImgThumbBack"..(index + 1))))
		local ypos = Control_GetLocationY(Image_GetControl(Dialog_GetImage(gDialogHandle, "ImgThumbBack"..(index + 1))))
		ch = Image_GetControl(Dialog_GetImage(gDialogHandle, "imgBackSelected"))
		Control_SetLocation(ch, xpos, ypos)
		Control_SetVisible(ch, true)
		displayTexture(index + 1, true)
	else
		ch = Image_GetControl(Dialog_GetImage(gDialogHandle, "imgBackSelected"))
		Control_SetVisible(ch, false)
	end

end


function clickSelect(x , y)
	local found = false
	for i = 0, g_textures_count - 1 do
		ch = Image_GetControl(Dialog_GetImage(gDialogHandle, "ImgThumb"..(i + 1)))
		ch2 = Static_GetControl(Dialog_GetStatic(gDialogHandle, "lblThumb"..(i + 1)))
		if Control_ContainsPoint(ch, x, y) ==  1 or Control_ContainsPoint(ch2, x, y) == 1 then
			selectTexture(i)
			found = true
		end
	end
end

function ImgThumb1_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function ImgThumb2_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function ImgThumb3_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function ImgThumb4_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function ImgThumb5_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function ImgThumb6_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function ImgThumb7_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function ImgThumb8_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function ImgThumb9_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function ImgThumb10_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb1_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb2_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb3_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb4_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb5_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb6_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb7_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb8_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb9_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end
function lblThumb10_OnLButtonDown( dialogHandle, x, y )
	clickSelect(x , y)
end

function btnResultsPrev_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then
	
	    calcResultsPrev()

	    alignButtons()
	    
		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, g_cur_webaddress, WF.TEXTURE_LIST, g_curPage, g_imagesPerPage)
		end
    
		clear()
	end

end

function btnResultsNext_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

        calcResultsNext()

	    alignButtons()
	    
		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, g_cur_webaddress, WF.TEXTURE_LIST, g_curPage, g_imagesPerPage)
		end
		
		clear()
	end
	
end

function btnFirst_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

		calcFirst(g_curPage, g_set)

	    alignButtons()
	    
		if (g_game ~= nil) then
			ClientEngine_GetBrowserPage(g_game, g_cur_webaddress, WF.TEXTURE_LIST, g_curPage, g_imagesPerPage)
		end

		clear()	
	end
		
end

function btnLast_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

		calcLast()
	
	    alignButtons()
	    
		if (g_game ~= nil) then
			ClientEngine_GetBrowserPage(g_game, g_cur_webaddress, WF.TEXTURE_LIST, g_curPage, g_imagesPerPage)
		end
		
		clear()	
	end
	
end
   
   
function btnRange1_OnButtonClicked( buttonHandle )

	calcRange1()
	
	if (g_game ~= nil) then
		ClientEngine_GetBrowserPage(g_game, g_cur_webaddress, WF.TEXTURE_LIST, g_curPage, g_imagesPerPage)
	end
	
	clear()	
	
end

function btnRange2_OnButtonClicked( buttonHandle )

	calcRange2()
			
	if (g_game ~= nil) then
		ClientEngine_GetBrowserPage(g_game, g_cur_webaddress, WF.TEXTURE_LIST, g_curPage, g_imagesPerPage)
	end

    clear()
	
end

function btnRange3_OnButtonClicked( buttonHandle )

	calcRange3()
	
	if (g_game ~= nil) then
		ClientEngine_GetBrowserPage(g_game, g_cur_webaddress, WF.TEXTURE_LIST, g_curPage, g_imagesPerPage)
	end

	clear()	
	
end

function btnRange4_OnButtonClicked( buttonHandle )

	calcRange4()
	
	if (g_game ~= nil) then
		ClientEngine_GetBrowserPage(g_game, g_cur_webaddress, WF.TEXTURE_LIST, g_curPage, g_imagesPerPage)
	end

	clear()	
	
end

function btnUpload_OnButtonClicked( buttonHandle )
	if g_game ~= nil then
		ClientEngine_LaunchBrowser( g_game, GameGlobals.WEB_SITE_PREFIX_KANEVA .. "mykaneva/upload.aspx" )
	end
end

---------------------------------------------------------------------
-- Keyboard Handler
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
      -- ESC key
      if key == 27 then
            -- close this menu
            DestroyMenu(gDialogHandle)
      end
end


-- Added by Jonny to handle alignment of the page buttons at the bottom.
function alignButtons()

    local imgLeftBar = Dialog_GetImage(gDialogHandle, "left arrow bar")
	Control_SetLocation(imgLeftBar, 25, 542)
	local btnLeftLeftArrow = Dialog_GetButton(gDialogHandle, "btnFirst")
	Control_SetLocation(btnLeftLeftArrow, 30, 542)
    local btnLeftArrow = Dialog_GetButton(gDialogHandle, "btnResultsPrev")
	Control_SetLocation(btnLeftArrow, 50, 542)

	local t = {}		-- new table
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange1"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange2"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange3"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange4"))
	leftAlignPageButtons(t, 70, 543, 5)

   	local rightMostXPos = Control_GetLocationX(Dialog_GetButton(gDialogHandle, "btnRange4")) +
   	                      Control_GetWidth(Dialog_GetButton(gDialogHandle, "btnRange4"))

	local btnRightArrow = Dialog_GetButton(gDialogHandle, "btnResultsNext")
	rightMostXPos = rightMostXPos + 5
	Control_SetLocation(btnRightArrow, rightMostXPos, 542)

	local btnRightRightArrow = Dialog_GetButton(gDialogHandle, "btnLast")
	rightMostXPos = rightMostXPos + Control_GetWidth(Dialog_GetButton(gDialogHandle, "btnResultsNext")) + 5
	Control_SetLocation(btnRightRightArrow, rightMostXPos, 542)

	local imgRightBar = Dialog_GetImage(gDialogHandle, "right arrow bar")
	rightMostXPos = rightMostXPos + Control_GetWidth(Dialog_GetButton(gDialogHandle, "btnLast")) + 2
	Control_SetLocation(imgRightBar, rightMostXPos, 542)

end

function btnMy_OnButtonClicked( buttonHandle )

	setButtonState(buttonHandle, false)

    reInitButtons()

	bh = Dialog_GetButton(gDialogHandle, "btnKaneva")
    setButtonState(bh, true)
    
    local sh = Dialog_GetStatic(gDialogHandle, "lblInfoHeader")
    Static_SetText(sh, "My Patterns")
    
    sh = Dialog_GetStatic(gDialogHandle, "Patterns Title")
    Static_SetText(sh, "My Patterns")

    sh = Dialog_GetStatic(gDialogHandle, "Static29")
    Static_SetText(sh, "Search My Patterns")
    
    sh = Dialog_GetStatic(gDialogHandle, "lblQuery")
    Static_SetText(sh, MY_PATTERNS_TEXT)

 	g_onTab = TAB_MY
 	g_cur_webaddress = WEB_ADDRESS
 	g_searching = false
 	if g_game ~= nil then
 	    ClientEngine_GetBrowserPage(g_game, g_cur_webaddress, WF.TEXTURE_LIST,  1, 10)
 	end
end

function btnKaneva_OnButtonClicked( buttonHandle )

	setButtonState(buttonHandle, false)

    reInitButtons()

	bh = Dialog_GetButton(gDialogHandle, "btnMy")
    setButtonState(bh, true)
    
    local sh = Dialog_GetStatic(gDialogHandle, "lblInfoHeader")
    Static_SetText(sh, "Featured Patterns")
    
    sh = Dialog_GetStatic(gDialogHandle, "Patterns Title")
    Static_SetText(sh, "Featured Patterns")

    sh = Dialog_GetStatic(gDialogHandle, "Static29")
    Static_SetText(sh, "Search for patterns")

    sh = Dialog_GetStatic(gDialogHandle, "lblQuery")
    Static_SetText(sh, FEATURED_PATTERNS_TEXT)

 	g_onTab = TAB_KANEVA
 	g_cur_webaddress = WEB_ADDRESS_FEATURED
 	g_searching = false
 	if g_game ~= nil then
 	    ClientEngine_GetBrowserPage(g_game, g_cur_webaddress, WF.TEXTURE_LIST,  1, 10)
 	end
end

function btnSearch_OnButtonClicked( buttonHandle )
	if g_game ~= nil then
        local ebh = Dialog_GetEditBox(gDialogHandle, "txtSearch")
		if ebh ~= nil then
			local searchString = EditBox_GetText(ebh)

			if searchString ~= "" then
				local sh = Dialog_GetStatic(gDialogHandle, "Patterns Title")
	    		Static_SetText(sh, "Search Results")
			
			    local sh = Dialog_GetStatic(gDialogHandle, "lblInfoHeader")
	    		Static_SetText(sh, "Search Results")
	
	    		sh = Dialog_GetStatic(gDialogHandle, "lblQuery")
	    		Static_SetText(sh, SEARCH_RESULTS_TEXT)
	    		
	    		g_searching = true
	    		
	    		reInitButtons()
			
				if g_onTab == TAB_KANEVA then
					if g_game ~= nil then
						g_cur_webaddress = WEB_ADDRESS_SEARCH .. "&channelId=" .. GameGlobals.FEATURED_CHANNEL .. "&ss=" .. searchString
		   				ClientEngine_GetBrowserPage(g_game, g_cur_webaddress, WF.TEXTURE_LIST,  1, 10)
		 			end
				elseif g_onTab == TAB_MY then
					if g_game ~= nil then
						g_cur_webaddress = WEB_ADDRESS_SEARCH .. "&channelId=-1&ss=" .. searchString
						ClientEngine_GetBrowserPage(g_game, g_cur_webaddress, WF.TEXTURE_LIST,  1, 10)
		 			end
				end
			else 
			    if g_onTab == TAB_KANEVA then
			    	btnKaneva_OnButtonClicked(Dialog_GetButton(dialogHandle, "btnKaneva"))
			    elseif g_onTab == TAB_MY then
			    	btnMy_OnButtonClicked(Dialog_GetButton(dialogHandle, "btnMy"))
			    end
			end
		end
	end
end

function reInitButtons()
   	g_shift = SHIFT_NONE
	g_selectedBtn = nil
	g_currentBtn = 1
	g_btnRange1 = nil
	g_window = 4
	g_set = 1
	g_prev_set = nil
	g_curPage = 1
	g_pageCap = 1
	g_setCap = 1
	g_InitBtnText = true
end