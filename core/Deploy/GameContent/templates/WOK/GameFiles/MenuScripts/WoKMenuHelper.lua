
---------------------------------------------------------------------
--Sync the List Boxes
---------------------------------------------------------------------
g_bSyncAllLists = 0
function syncAllLists( listhandles, listchanged, callback )
	
	if g_bSyncAllLists == 1 then
		return
	end

	
	g_bSyncAllLists = 1
	lindex = ListBox_GetSelectedItemIndex(listchanged)
	for key, value in pairs(listhandles) do
		if listhandles[key] ~= listchanged then
			ListBox_SelectItem(listhandles[key], lindex )
		end
	end

	if callback ~= nil then 
		callback( lindex )	
	end

	sb = ListBox_GetScrollBar(listchanged)
	spos = ScrollBar_GetTrackPos(sb)
	for key, value in pairs(listhandles) do
		if listhandles[key] ~= listchanged then
			sb = ListBox_GetScrollBar(listhandles[key])
			ScrollBar_SetTrackPos(sb, spos)
		end
	end
	g_bSyncAllLists = 0
end

function setupScrollbarLook( dialogHandle, scrollbarName )

	-- setup look of listbox scrollbars and populate list
	local lbh = Dialog_GetListBox(dialogHandle, scrollbarName)
	if lbh ~= 0 then

		local sbh = ListBox_GetScrollBar(lbh)

		if sbh ~= 0 then

			local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then

				Element_AddTexture(eh, dialogHandle, "gpb_controls.dds")
				Element_SetCoords(eh, 151, 33, 170, 92)
			
			end

			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then

				Element_AddTexture(eh, dialogHandle, "gpb_controls.dds")
				Element_SetCoords(eh, 179, 33, 198, 68)
			
			end

			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then

				Element_AddTexture(eh, dialogHandle, "gpb_controls.dds")
				Element_SetCoords(eh, 151, 3, 170, 31)
			
			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then

				Element_AddTexture(eh, dialogHandle, "gpb_controls.dds")
				Element_SetCoords(eh, 150, 93, 169, 121)
			
			end

		end

	end

end

