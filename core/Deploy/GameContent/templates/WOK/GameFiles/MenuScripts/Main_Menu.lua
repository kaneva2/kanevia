dofile("MenuHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("MenuLocation.lua")

-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--
-- end 

YES_NO_BOX_EVENT = "YesNoBoxEvent"
YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"

WEB_HELP_SUFFIX = "channel/channelPage.aspx?communityId=13818&pageId=514421"
WEB_FAQ_SUFFIX = "channel/channelPage.aspx?communityId=1118&pageId=14277"
WEB_FEEDBACK_SUFFIX = "suggestions.aspx?mode=F&category=Feedback"


-- Button names to keep track of which button pressed
BTN_NONE = 0
BTN_QUIT = 1

-- global game object
g_game = nil
g_ScriptDir = ""

g_t_btnHandles = {}

g_btnPressed = BTN_NONE

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	

	ParentHandler_RegisterEventHandler( dispatcher, handler, "answerEventHandler", 1,0,0,0, YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
	
	-- TODO register any of your handlers here like the following....
	-- ParentHandler_RegisterEventHandler( dispatcher, handler, "renderTextHandler", 1,0,0,0, "RenderTextEvent", KEP.HIGH_PRIO ) 
	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, YES_NO_BOX_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, YES_NO_ANSWER_EVENT, KEP.MED_PRIO )


end

function answerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local answer = Event_DecodeNumber( event )

	if answer ~= 0 then
	    if g_btnPressed == BTN_QUIT then
	    	ClientEngine_Quit( g_game )
	    end
	end
	
	g_btnPressed = BTN_NONE
	
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	gDialogHandle = dialogHandle

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	
	if KEPConfig_GetNumber == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end

    getButtonHandles()

	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	loadLocation(g_game, gDialogHandle, "Help")

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	saveLocation(g_game, dialogHandle, "Help")
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

---------------------------------------------------------------------
--
-- Keyboard Handler
--
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)

	-- ESC key
	if key == 27 then

		-- close this menu
		DestroyMenu(gDialogHandle)

	end

end

---------------------------------------------------------------------
--
-- Go To  menu
--
---------------------------------------------------------------------
function gotoMenu(menu_path, bClose, bModal)

	dh = Dialog_Create( menu_path );
	if dh == 0 then
		Dialog_MsgBox("Menu [" .. menu_path .. "] not found.")
	end

	if bModal and dh ~= 0 then
		Dialog_SetModal(dh, 1)
	end

	-- close this dialog
	if bClose == 1 then
		DestroyMenu(gDialogHandle)
	end

end

---------------------------------------------------------------------
--
-- Button Handlers
--
---------------------------------------------------------------------
function btnVideoConfig_OnButtonClicked( buttonHandle )
	if g_game ~= nil then 
		gotoMenu( "Video_Settings.xml", 1, 1 )
	end
end

function btnQuit_OnButtonClicked( buttonHandle )

	g_btnPressed = BTN_QUIT

	gotoMenu( "YesNoBox.xml", 0, 1 )
	local yesnoEvent = Dispatcher_MakeEvent( KEP.dispatcher, YES_NO_BOX_EVENT )
	Event_EncodeString( yesnoEvent, "Are you sure you want to leave?" )
	Event_EncodeString( yesnoEvent, "Confirm" )
	Dispatcher_QueueEvent( KEP.dispatcher, yesnoEvent )

end

function btnCtrlSet_OnButtonClicked( buttonHandle )
	if g_game ~= nil then 
		gotoMenu( "Control_Settings.xml", 1, 1 )
	end
end

function btnFAQ_OnButtonClicked( buttonHandle )
	ClientEngine_LaunchBrowser( g_game, GameGlobals.WEB_SITE_PREFIX_KANEVA..WEB_FAQ_SUFFIX )
end


function btnFeedback_OnButtonClicked( buttonHandle )
	ClientEngine_LaunchBrowser( g_game, GameGlobals.WEB_SITE_PREFIX_KANEVA..WEB_FEEDBACK_SUFFIX )		
end

function btnClose_OnButtonClicked( buttonHandle )
	DestroyMenu( gDialogHandle )
end

function btnAbout_OnButtonClicked( buttonHandle )
	gotoMenu("About.xml", 1, 1)
end

---------------------------------------------------------------------
-- Mouse Over
---------------------------------------------------------------------
function getButtonHandles()


	local ch = Dialog_GetControl(gDialogHandle, "btnQuit")
	if ch ~= nil then
     	g_t_btnHandles["btnQuit"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnClose")
	if ch ~= nil then
     	g_t_btnHandles["btnClose"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnFAQ")
	if ch ~= nil then
		g_t_btnHandles["btnFAQ"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnMoveRight")
	if ch ~= nil then
     	g_t_btnHandles["btnMoveRight"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnFeedback")
	if ch ~= nil then
     	g_t_btnHandles["btnFeedback"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnAbout")
	if ch ~= nil then
     	g_t_btnHandles["btnAbout"] = ch
	end	


end

function Dialog_OnMouseMove(dialogHandle, x, y)

    local sh = Dialog_GetStatic(dialogHandle, "lblInfo")

	if sh ~= nil then

		if Control_ContainsPoint(g_t_btnHandles["btnQuit"], x, y) ~= 0 then
			Static_SetText(sh, "Leave World")
   		elseif Control_ContainsPoint(g_t_btnHandles["btnClose"], x, y) ~= 0 then
	    	Static_SetText(sh, "Close Menu")
		elseif Control_ContainsPoint(g_t_btnHandles["btnFAQ"], x, y) ~= 0 then
			Static_SetText(sh, "FAQ Webpage")
		elseif Control_ContainsPoint(g_t_btnHandles["btnFeedback"], x, y) ~= 0 then
			Static_SetText(sh, "Send Feedback")
		elseif Control_ContainsPoint(g_t_btnHandles["btnAbout"], x, y) ~= 0 then
			Static_SetText(sh, "World of Kaneva")
		else
            Static_SetText(sh, "Select an Option")
		end
	end

end