-- global game object
g_game = nil

-- chat type constants
ctTalk 	= 0
ctGroup	= 1
ctClan	= 2
ctPrivate   = 3
ctSystem = 6

g_chat_type = ctTalk

---------------------------------------------------------------------
--
-- Called from 'derived' menu
--
---------------------------------------------------------------------
function Helper_Chat_OnCreate(dialogHandle)

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	if g_game == 0 then
		g_game = nil
	end

	-- set focus to the default control (should be txtChat)
	Dialog_FocusDefaultControl( dialogHandle)
end

---------------------------------------------------------------------
--
-- 'ENTER' key was pressed within txtChat
--
---------------------------------------------------------------------
function txtChat_OnEditBoxString(editBoxHandle, password)

	local msg = EditBox_GetText(editBoxHandle)

	-- send chat message
	if g_game ~= nil and msg ~= "" then
		ClientEngine_SendChatMessage( g_game, g_chat_type, msg )
	end

	-- close this menu
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------
--
-- Keyboard Handler
--
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
		
	-- ESC key
	if key == 27 then

		-- close this menu
		DestroyMenu(gDialogHandle)
	end
end

