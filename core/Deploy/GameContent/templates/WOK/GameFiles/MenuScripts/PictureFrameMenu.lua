dofile("MenuHelper.lua")
dofile("..\\Scripts\\BrowserIds.lua")
dofile("..\\Scripts\\GameGlobals.lua")

-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--w
-- end 
PICTURE_FRAME_MENU_EVENT = "PictureFrameMenuEvent"
FRIEND_SELECTION_EVENT = "FriendSelectionEvent"
FRIEND_SELECTED_SELECTED = "FriendSelectedEvent"
CLOSE_DYNMENU_EVENT = "CloseDynMenuEvent"

-- id of the dynamic placement obj
g_objId = -1
g_friendId = -1

g_game = nil
g_dispatcher = nil
g_confirmation = 0
g_objName = nil

-- Global keeping track of the amount to multiply the movement distance
-- by depending on shift being held down
g_ShiftMultiple = 1

-- Base step size when moving frame
g_StepSize = 0.2

-- indicates the state of the arrow buttons.
CLEAR 		= 0	-- No arrow is down.
CLICKED_LEFT 	= 1	-- Left arrow is clicked.
CLICKED_RIGHT 	= 2	-- Right arrow is clicked.
HELD_LEFT 	= 3	-- Left arrow is held down for sustained period.
HELD_RIGHT 	= 4	-- Right arrow is held down for sustained period.

--Hold Button handles
g_t_btnHandles = {} 

-- helper to enable/disable friend buttons
function enableFriendButtons( setEnabled )

	b = Dialog_GetButton( gDialogHandle, "btnGotoFriend" )
	bc = Button_GetControl( b )
	Control_SetEnabled( bc, setEnabled )
	
	b = Dialog_GetButton( gDialogHandle, "btnGotoFriendsApt" )
	bc = Button_GetControl( b )
	Control_SetEnabled( bc, setEnabled )
	
end

--
-- update g_objId
--
function updateId(dispatcher, fromNetid, event, eventid, filter, objectid)	
	
	g_objId  = Event_DecodeNumber( event );				-- placement Id
	g_friendId = Event_DecodeNumber( event );			-- friend Id, <= 0 means no friend
	g_objName = Event_DecodeString( event );  -- name of this picture frame.

	Static_SetText(Dialog_GetStatic(gDialogHandle, "lblTitle"), g_objName)

	enableFriendButtons( g_friendId > 0 )
	
	Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, ">>>>>setting id = "..g_objId.." and "..g_friendId ) 		
	
	return KEP.EPR_OK
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "updateId", 1,0,0,0, PICTURE_FRAME_MENU_EVENT, KEP.HIGH_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "confirmationCloseHandler", 1,0,0,0, "ConfirmationCloseEvent", KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "friendListCloseHandler", 1,0,0,0, FRIEND_SELECTED_SELECTED, KEP.HIGH_PRIO )	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "CloseDynObjHandler", 1,0,0,0, CLOSE_DYNMENU_EVENT, KEP.HIGH_PRIO )

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "ConfirmationCloseEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "TextureSelectionEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, PICTURE_FRAME_MENU_EVENT, KEP.MED_PRIO )	
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, FRIEND_SELECTION_EVENT, KEP.MED_PRIO )	
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, FRIEND_SELECTED_SELECTED, KEP.MED_PRIO )		
	
	g_dispatcher = dispatcher

end

function CloseDynObjHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	--Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "brgin event handler... "..filter )
	local oldHandle = Event_DecodeNumber(event)


	if oldHandle == gDialogHandle then
		DestroyMenu(gDialogHandle)
	end

	return KEP.EPR_CONSUMED

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
  
  if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end	
	
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	if g_game == 0 then
		g_game = nil
	end
	
	getButtonHandles()
	
	if ClientEngine_SetMenuSelectMode ~= nil then
		ClientEngine_SetMenuSelectMode( g_game, 1.0 )
	end

	-- TODO init here
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	if ClientEngine_SetMenuSelectMode ~= nil then
		ClientEngine_SetMenuSelectMode( g_game, 0.0 )
	end
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

---------------------------------------------------------------------
--
-- Delete Button - Delete selected object
--
---------------------------------------------------------------------
function btnDelete_OnButtonClicked( buttonHandle )

	-- show confirmation dialog
	g_confirmation = Dialog_Create("Yes_No_OutGame.xml")
	if g_confirmation ~= 0 then	  
		local sh = Dialog_GetStatic(g_confirmation, "lblMessage")		
		if sh ~= 0 then
			Static_SetText(sh, "Take Down Picture?")
		end
	end

	-- close this dialog
	--DestroyMenu(gDialogHandle)
	--if g_game ~= nil then
	--	ClientEngine_Quit( g_game )
	--end
end

---------------------------------------------------------------------
--
-- Confirmation Close Handler
--
---------------------------------------------------------------------
function confirmationCloseHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

  Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "confirmation event received" ) 
  
	local yes = Event_DecodeNumber( event )

	Dialog_Destroy(g_confirmation)

	if yes == 1 then
		DeleteObject()
		DestroyMenu(gDialogHandle)  
	end
end

---------------------------------------------------------------------
--
-- Delete an object
--
---------------------------------------------------------------------
function DeleteObject()

	ClientEngine_DeleteDynamicObjectPlacement(g_game, g_objId)

end

---------------------------------------------------------------------
--
-- Save Button - save changes
--
---------------------------------------------------------------------
function btnSave_OnButtonClicked( buttonHandle )	
	
	Dispatcher_LogMessage( g_dispatcher, KEP.ERROR_LOG_LEVEL, "saving wld obj changes" ) 	
  ClientEngine_SaveDynamicObjectPlacementChanges(g_game, g_objId)	
	-- close this dialog
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------
--
-- Cancel Button - cancel and discard changes
--
---------------------------------------------------------------------
function btnCancel_OnButtonClicked( buttonHandle )

  Dispatcher_LogMessage( g_dispatcher, KEP.ERROR_LOG_LEVEL, "cancelling wld obj changes" ) 	
  ClientEngine_CancelDynamicObjectPlacementChanges(g_game, g_objId)			
	-- close this dialog
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------
--
-- Change Texture Button - show texture selection menu
--
---------------------------------------------------------------------
function btnSetPicture_OnButtonClicked( buttonHandle )

  Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "changing texture..." ) 	
  Dialog_Create( "TextureBrowser.xml");
  
  textureSelEvent = Dispatcher_MakeEvent( KEP.dispatcher, "TextureSelectionEvent" )	
	Event_EncodeNumber( textureSelEvent, g_objId )
	Dispatcher_QueueEvent( KEP.dispatcher, textureSelEvent )
  
end

---------------------------------------------------------------------
--
-- Set Friend Picture Button - show friend selection menu
--
---------------------------------------------------------------------
function btnSetFriendPicture_OnButtonClicked( buttonHandle )

  Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "setting friend's picture..." ) 	
  Dialog_Create( "FriendSelect.xml");
  
  local friendSelEvent = Dispatcher_MakeEvent( KEP.dispatcher, FRIEND_SELECTION_EVENT )	
	Event_EncodeNumber( friendSelEvent, g_kanevaUserId )
	Dispatcher_QueueEvent( KEP.dispatcher, friendSelEvent )
  
end

---------------------------------------------------------------------
--
-- Start moving right along the slide vector
--
---------------------------------------------------------------------
function btnRight_OnLButtonDown(controlHandle, x, y)
  -- note that the direction from users perspective is reverted, so this
  -- is actually moving to left in object space  
  
    local step = -g_StepSize * g_ShiftMultiple
  
	MoveModel(0,step,0)
end

---------------------------------------------------------------------
--
-- Stop moving right along the slide vector
--
---------------------------------------------------------------------
function btnRight_OnLButtonUp(controlHandle, x, y)
end

---------------------------------------------------------------------
--
-- Start moving left along the slide vector
--
---------------------------------------------------------------------
function btnLeft_OnLButtonDown(controlHandle, x, y)
  -- note that the direction from users perspective is reverted, so this
  -- is actually moving to right in object space
  
    local step = g_StepSize * g_ShiftMultiple
  
	MoveModel(0,step,0)
end

---------------------------------------------------------------------
--
-- Stop moving left along the slide vector
--
---------------------------------------------------------------------
function btnLeft_OnLButtonUp(controlHandle, x, y)
end

---------------------------------------------------------------------
--
-- Start moving right along the up vector
--
---------------------------------------------------------------------
function btnUp_OnLButtonDown(controlHandle, x, y)

	local step = g_StepSize * g_ShiftMultiple

	MoveModel(0,0,step)
end

---------------------------------------------------------------------
--
-- Stop moving right along the up vector
--
---------------------------------------------------------------------
function btnUp_OnLButtonUp(controlHandle, x, y)
end

---------------------------------------------------------------------
--
-- Start moving left along the up vector
--
---------------------------------------------------------------------
function btnDown_OnLButtonDown(controlHandle, x, y)

    local step = -g_StepSize * g_ShiftMultiple

	MoveModel(0,0,step)
end

---------------------------------------------------------------------
--
-- Stop moving left along the up vector
--
---------------------------------------------------------------------
function btnDown_OnLButtonUp(controlHandle, x, y)
end


---------------------------------------------------------------------
--
-- spawn to friend's location
--
---------------------------------------------------------------------
function btnGotoFriend_OnButtonClicked( buttonHandle )

	Dispatcher_LogMessage( g_dispatcher, KEP.ERROR_LOG_LEVEL, "going to friend" ) 	
	ClientEngine_CancelDynamicObjectPlacementChanges(g_game, g_objId)			

	-- fire event to server telling it where we want to go
	ev = Dispatcher_MakeEvent( g_dispatcher, KEP.GOTOPLACE_EVENT_NAME )
	Event_SetFilter( ev, KEP.GOTO_PLAYER ) -- we're going to a player
	Event_EncodeNumber( ev, g_friendId )
	Event_EncodeNumber( ev, -1 ) -- selected character is unknown now

	Event_AddTo( ev, KEP.SERVER_NETID )
	Dispatcher_QueueEvent( g_dispatcher, ev )
	
	-- close this dialog
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------
--
-- spawn to friend's apt
--
---------------------------------------------------------------------
function btnGotoFriendsApt_OnButtonClicked( buttonHandle )

  	Dispatcher_LogMessage( g_dispatcher, KEP.ERROR_LOG_LEVEL, "going to friend's apt" ) 	
  
  	ClientEngine_CancelDynamicObjectPlacementChanges(g_game, g_objId)			

	-- fire event to server telling it where we want to go
	ev = Dispatcher_MakeEvent( g_dispatcher, KEP.GOTOPLACE_EVENT_NAME )
	Event_SetFilter( ev, KEP.GOTO_PLAYERS_APT ) -- we're going to a player
	Event_EncodeNumber( ev, g_friendId )
	Event_EncodeNumber( ev, -1 ) -- selected character is unknown now

	Event_AddTo( ev, KEP.SERVER_NETID )
	Dispatcher_QueueEvent( g_dispatcher, ev )
	

	-- close this dialog
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------
--
-- Move 3D Model
--
---------------------------------------------------------------------
function MoveModel(dir, slide, lift)	
	
	Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "calling ClientEngine_MoveDynamicObjectPlacement" ) 	
	ClientEngine_MoveDynamicObjectPlacement(g_game, g_objId, dir, slide, lift)
	
end

---------------------------------------------------------------------
--
-- Friend List menu Close Handler
--
---------------------------------------------------------------------
function friendListCloseHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

  Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "friend selection event received" ) 
  
	local friendId = Event_DecodeNumber( event )

	--Dialog_Destroy(g_textureSelMenu)
	enableFriendButtons( true )

	if friendId > 0 then
		ClientEngine_ApplyFriendPicture(g_game, g_objId, friendId, GameGlobals.FULLSIZE )
	end
end

---------------------------------------------------------------------
-- Keyboard Handler
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)

	  -- Shift
      if bShiftDown then
        g_ShiftMultiple = 10
      end

      -- ESC key
      if key == 27 then
            -- close this menu
            DestroyMenu(gDialogHandle)
      end
end

function Dialog_OnKeyUp(dialogHandle, key, bShiftDown)
      -- shift

      if not(bShiftDown) then
        g_ShiftMultiple = 1
      end

end
---------------------------------------------------------------------
-- Mouse Over
---------------------------------------------------------------------
function getButtonHandles()
--g_t_btnHandles


	local ch = Dialog_GetControl(gDialogHandle, "btnRight")
	if ch ~= nil then
     	g_t_btnHandles["btnRight"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnLeft")
	if ch ~= nil then
     	g_t_btnHandles["btnLeft"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnDown")
	if ch ~= nil then
     	g_t_btnHandles["btnDown"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnUp")
	if ch ~= nil then
		g_t_btnHandles["btnUp"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSetPicture")
	if ch ~= nil then
     	g_t_btnHandles["btnSetPicture"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnCancel")
	if ch ~= nil then
     	g_t_btnHandles["btnCancel"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnDelete")
	if ch ~= nil then
     	g_t_btnHandles["btnDelete"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSave")
	if ch ~= nil then
     	g_t_btnHandles["btnSave"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSetFriendPicture")
	if ch ~= nil then
     	g_t_btnHandles["btnSetFriendPicture"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnGotoFriend")
	if ch ~= nil then
     	g_t_btnHandles["btnGotoFriend"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnGotoFriendsApt")
	if ch ~= nil then
     	g_t_btnHandles["btnGotoFriendsApt"] = ch
	end	


end

function Dialog_OnMouseMove(dialogHandle, x, y)

	--Make sure we have the interface we need (stop errors in editor)
	if Control_ContainsPoint ~= nil then
	
	    local sh = Dialog_GetStatic(dialogHandle, "lblStatus")
	
		if sh ~= nil then
	
			if Control_ContainsPoint(g_t_btnHandles["btnRight"], x, y) ~= 0 then
				Static_SetText(sh, "Move the Picture Frame Right")
			elseif Control_ContainsPoint(g_t_btnHandles["btnLeft"], x, y) ~= 0 then
		    	Static_SetText(sh, "Move the Picture Frame Left")
			elseif Control_ContainsPoint(g_t_btnHandles["btnDown"], x, y) ~= 0 then
				Static_SetText(sh, "Move the Picture Frame Down")
			elseif Control_ContainsPoint(g_t_btnHandles["btnUp"], x, y) ~= 0 then
			    Static_SetText(sh, "Move the Picture Frame Up")
			elseif Control_ContainsPoint(g_t_btnHandles["btnSetPicture"], x, y) ~= 0 then
				Static_SetText(sh, "Select a Picture for the Frame")
			elseif Control_ContainsPoint(g_t_btnHandles["btnCancel"], x, y) ~= 0 then
				Static_SetText(sh, "Close Menu")
			elseif Control_ContainsPoint(g_t_btnHandles["btnDelete"], x, y) ~= 0 then
				Static_SetText(sh, "Take Down Picture Frame")
			elseif Control_ContainsPoint(g_t_btnHandles["btnSave"], x, y) ~= 0 then
				Static_SetText(sh, "Save Changes")
			elseif Control_ContainsPoint(g_t_btnHandles["btnSetFriendPicture"], x, y) ~= 0 then
				Static_SetText(sh, "Select a Friend's Picture for the Frame")
			elseif Control_ContainsPoint(g_t_btnHandles["btnGotoFriend"], x, y) ~= 0 then
				Static_SetText(sh, "Travel to Friend")
			elseif Control_ContainsPoint(g_t_btnHandles["btnGotoFriendsApt"], x, y) ~= 0 then
				Static_SetText(sh, "Visit Friend's Apartment")
			else
	            Static_SetText(sh, "Hold shift & click for greater movement")
			end
		end

	end
end
