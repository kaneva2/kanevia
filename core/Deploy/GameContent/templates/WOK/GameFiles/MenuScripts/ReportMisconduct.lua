dofile("MenuHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")

-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--
-- end 

INFO_BOX_EVENT = "InfoBoxEvent"
PLAYER_USERID_EVENT = "PlayerUserIdEvent"

SUBJECT_BUG = "kgp/reportAbuse.aspx?subject=ReportBug"
--SUBJECT_COPYRIGHT = "kgp/reportAbuse.aspx?subject=CopyRightViolation"
SUBJECT_CONTENT = "kgp/reportAbuse.aspx?subject=InappropriateContent"
SUBJECT_MATURE = "kgp/reportAbuse.aspx?subject=Mature"
SUBJECT_OTHER = "kgp/reportAbuse.aspx?subject=Other"
 
MESSAGE_SUFFIX = "&message="

g_t_subjects = {SUBJECT_CONTENT, SUBJECT_MATURE, SUBJECT_OTHER, SUBJECT_BUG}
 
--g_selectedSubject = "" 
 
g_game = nil

g_targetName = "General Submission"
g_targetID = 0

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	

	ParentHandler_RegisterEventHandler( dispatcher, handler, "BrowserPageReadyHandler", 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "PlayerUserIdEventHandler", 1,0,0,0, PLAYER_USERID_EVENT, KEP.MED_PRIO )
	
	-- TODO register any of your handlers here like the following....
	-- ParentHandler_RegisterEventHandler( dispatcher, handler, "renderTextHandler", 1,0,0,0, "RenderTextEvent", KEP.HIGH_PRIO ) 
	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, PLAYER_USERID_EVENT, KEP.MED_PRIO )
	-- TODO register any new event types here like the following...
	-- ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "LuaEvent", KEP.MED_PRIO )


end


---------------------------------------------------------------------
-- Event Handlers
---------------------------------------------------------------------
-- Catch the xml from the server
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter == WF.REPORT then
	--	local sh = Dialog_GetStatic( gDialogHandle, "Static78" )
	
		returnData = Event_DecodeString( event )
		
	--	Static_SetText(sh, returnData)
	
		ParseResults(returnData)
	
		return KEP.EPR_CONSUMED
	end
end


function PlayerUserIdEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	g_targetID = Event_DecodeNumber( event )
	g_targetName = Event_DecodeString( event )
	
	--Dialog_MsgBox(g_targetID.."   "..g_targetName)
	
end
---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

    g_game = Dispatcher_GetGame(KEP.dispatcher)

    if g_game == 0 then
            g_game = nil
    end
      	
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	
	loadListBox()
	
	Dialog_BringToFront(gDialogHandle)

end

---------------------------------------------------------------------
-- Parser
---------------------------------------------------------------------

function ParseResults(data)

	local result = ""  -- result description
	local code = -1
	local s, e = 0      -- start and end of captured string
	
	s, e, code = string.find(data, "<ReturnCode>(%d+)</ReturnCode>")
	s, e, result = string.find(data, "<ResultDescription>(%w+)</ResultDescription>")
	
	if code == "0" then
		createMessage("Feedback sent.")
	else
		createMessage("There was an error sending feedback.")
	end
	--createMessage(data)
	
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------
-- Helpers
---------------------------------------------------------------------

--[[  -- Ditch for now.  Combo box does not want to display text

function cbSubject_OnComboBoxSelectionChanged( comboBoxHandle )

	if comboBoxHandle ~= nil then
		local text = ComboBox_GetSelectedText( comboBoxHandle )
		Dialog_MsgBox( text )	
	end
	

end


function loadDropDown()



	cbh = Dialog_GetComboBox(gDialogHandle, "cbSubject")
	if cbh~= nil then
	       
		ComboBox_AddItem(cbh, "Report a Bug", 1) 	
		ComboBox_AddItem(cbh, "Copyright Violation", 2) 	
		ComboBox_AddItem(cbh, "Inappropriate Content", 3) 	
		ComboBox_AddItem(cbh, "Report Mature Content", 4) 	
		ComboBox_AddItem(cbh, "Other (Specify in Message)", 5) 	
	
	end
end
]]--

function loadListBox()
	
	-- Populate the list box in the order matching the g_t_subjects table
	lbh = Dialog_GetListBox(gDialogHandle, "lstSubject")
	if lbh~= nil then
	       

--		ListBox_AddItem(lbh, "Copyright Violation", 2) 	
		ListBox_AddItem(lbh, "Inappropriate Content", 1)
		ListBox_AddItem(lbh, "Report Mature Content", 2)
		ListBox_AddItem(lbh, "Other (Specify in Message)", 3)
		ListBox_AddItem(lbh, "Report a Bug", 4)
	end	

end



function btnSubmit_OnButtonClicked( buttonHandle )

	-- index relates to the g_t_subjects table
	local index = -1
	local web_address = nil

	lbh = Dialog_GetListBox(gDialogHandle, "lstSubject")
	if lbh~= nil then
	       
		index = ListBox_GetSelectedItemData(lbh) 	
	    if index > 0 then
	    
	    	local sh = Dialog_GetStatic(gDialogHandle, "lblDarkness")
			if (sh ~= nil) and (g_game ~= nil) then 
				
				local text = " "
				
				if g_targetID > 0 then
					text = "Player being reported: "..g_targetName.." - "..g_targetID.."\n"
				else
					text = g_targetName.." - "..g_targetID.."\n"
				end

				
				text = text..Static_GetText(sh)	
				if text == "" or text == nil then
				   text = "No message entered"
				end
				text = URLSafe(text)
				--Dialog_MsgBox(text)
				web_address = GameGlobals.WEB_SITE_PREFIX..g_t_subjects[index]..MESSAGE_SUFFIX..text
				--Dialog_MsgBox(web_address)
	            ClientEngine_GetBrowserPage(g_game, web_address, WF.REPORT, 0, 0)
	    	end
	    	
     	else
            createMessage("Please select a report subject.")
		end
		
	end	
	
end

function createMessage( text )

	if g_game ~= nil and text ~= nil then

		gotoMenu( "InfoBox.xml", 0, 1 )

		local ev = Dispatcher_MakeEvent( KEP.dispatcher, INFO_BOX_EVENT )

		Event_EncodeString( ev, text )

		Dispatcher_QueueEvent( KEP.dispatcher, ev )

	end

end

---------------------------------------------------------------------
-- Helper to open the various menus that spawn off of this one.
---------------------------------------------------------------------
function gotoMenu(menu_path, bClose, bModal)

	dh = Dialog_Create( menu_path );
	if dh == 0 then
		MsgBox("Menu [" .. menu_path .. "] not found.")
	end

	if bModal == 1 and dh ~= 0 then
		Dialog_SetModal(dh, 1)
		Dialog_SendToBack( gDialogHandle )
	else
		if dh ~= 0 then
			Dialog_SetModal(dh, 0)
		end
	end
	-- close this dialog
	if bClose == 1 then
		DestroyMenu(gDialogHandle)
	end

	return dh

end

function btnClose_OnButtonClicked( buttonHandle)
	DestroyMenu(gDialogHandle)
end

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)

      -- ESC key
      if key == 27 then

            -- close this menu

            DestroyMenu(gDialogHandle)
      end
end
---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

function ebMessage_OnEditBoxChange(editBoxHandle)
	sh = Dialog_GetStatic(gDialogHandle, "lblDarkness")
	Static_SetText(sh, EditBox_GetText(editBoxHandle))
end

function ebMessage_OnEditBoxString(editBoxHandle, mystring)
	sh = Dialog_GetStatic(gDialogHandle, "lblDarkness")
	Static_SetText(sh, EditBox_GetText(editBoxHandle) .. "\n")
	EditBox_SetText(editBoxHandle, mystring .. "\n", false)
end

-- This should be included from a global for efficiency everywhere it is used
-- But lets just make it work for now.
-- Jonny
function URLSafe(s)
	-- first replace the % for obvious reasons
	s = string.gsub(s, "%%", "%%25")
	-- Reserved characters for URLS
	s = string.gsub(s, "\n", "<br>" )
	--s = string.gsub(s, "$", "%%24")
	s = string.gsub(s, "+", "%%2B")
	s = string.gsub(s, ",", "%%2C")
	s = string.gsub(s, "/", "%%2F")
	s = string.gsub(s, ":", "%%3A")
	s = string.gsub(s, ";", "%%3B")
	s = string.gsub(s, "=", "%%3D")
	s = string.gsub(s, "?", "%%3F")
	s = string.gsub(s, "@", "%%40")
	s = string.gsub(s, " ", "%%20")
	-- Unsafe characters
	s = string.gsub(s, "<", "&gt;")
	s = string.gsub(s, ">", "&lt;")
	s = string.gsub(s, '"', "%%22")
	s = string.gsub(s, "#", "%%23")
	s = string.gsub(s, "&", "%%26")
	return s
end

