dofile("MenuHelper.lua")

-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--
-- end 


INFO_BOX_EVENT = "InfoBoxEvent"

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "InfoBoxEventHandler", 1,0,0,0, INFO_BOX_EVENT, KEP.MED_PRIO )
	
	-- TODO register any of your handlers here like the following....
	-- ParentHandler_RegisterEventHandler( dispatcher, handler, "renderTextHandler", 1,0,0,0, "RenderTextEvent", KEP.HIGH_PRIO ) 
	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, INFO_BOX_EVENT, KEP.MED_PRIO )

	-- TODO register any new event types here like the following...
	-- ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "LuaEvent", KEP.MED_PRIO )


end

function InfoBoxEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local message = Event_DecodeString( event )
	
 	sh = Dialog_GetStatic( gDialogHandle, "lblMessage" )
	Static_SetText( sh, message )

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	Dialog_BringToFront(gDialogHandle)

	-- TODO init here
end

---------------------------------------------------------------------
-- Button Handler
---------------------------------------------------------------------

function btnOk_OnButtonClicked( buttonHandle)
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------

-- Keyboard Handler

---------------------------------------------------------------------

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)

      -- ESC key
      if key == 27 then
	        -- close this menu
            DestroyMenu(gDialogHandle)
      end
end


---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

