dofile("MenuHelper.lua")

g_game = nil


g_lodBias = 0
g_gpuskin = false
g_animBias = 0
g_vertexAnim = true
g_avatarNames = 5
g_flashQuality = 5
g_spotShadows = true -- Damnit i forgot to implement spot shadow controls.
g_invertY = false


---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	
	-- TODO register any of your handlers here like the following....
	-- ParentHandler_RegisterEventHandler( dispatcher, handler, "renderTextHandler", 1,0,0,0, "RenderTextEvent", KEP.HIGH_PRIO ) 
	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	-- TODO register any new event types here like the following...
	-- ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "LuaEvent", KEP.MED_PRIO )


end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

  	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end	

	if KEPConfig_GetString == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end

	g_game = Dispatcher_GetGame(KEP.dispatcher)

	local slider = Dialog_GetSlider(gDialogHandle, "sldAnimQuality")	
	Slider_SetRange( slider, 1, 10 )

	slider = Dialog_GetSlider(gDialogHandle, "sldCharDetail")	
	Slider_SetRange( slider, 1, 10 )

	slider = Dialog_GetSlider(gDialogHandle, "sldAvatarNames")	
	Slider_SetRange( slider, 1, 10 )

	slider = Dialog_GetSlider(gDialogHandle, "sldFlash")	
	Slider_SetRange( slider, 1, 10 )
	loadSettings()
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	Helper_Dialog_OnDestroy( dialogHandle )
end


function sldAnimQuality_OnSliderValueChanged( sh )
	g_animBias = Slider_GetValue( sh )
end

function sldCharDetail_OnSliderValueChanged( sh )
	g_lodBias = Slider_GetValue( sh )
end

function sldAvatarNames_OnSliderValueChanged( sh )
	g_avatarNames = Slider_GetValue( sh )
end

function sldFlash_OnSliderValueChanged( sh )
	g_flashQuality = Slider_GetValue( sh )
end

function radSoftware_OnRadioButtonChanged( rh )
	g_gpuskin = 0
end

function radHardware_OnRadioButtonChanged( rh )
	g_gpuskin = 1
end

function chkEnvironmentalAnim_OnCheckBoxChanged( ch)
	value = CheckBox_GetChecked(ch)
	if value == 0 then
		g_vertexAnim = 0
	else
		g_vertexAnim = 1
	end
end

function chkShadows_OnCheckBoxChanged( ch)
	value = CheckBox_GetChecked(ch)
	if value == 0 then
		g_spotShadows = 0
	else
		g_spotShadows = 1
	end
end

function chkInvertY_OnCheckBoxChanged( ch)
	value = CheckBox_GetChecked(ch)
	if value == 0 then
		g_invertY = 0
	else
		g_invertY = 1
	end
end

function loadSettings()
	g_lodBias = ClientEngine_GetLodBias(g_game)
	g_gpuskin = ClientEngine_GetGPUSkinning(g_game)
	g_animBias = ClientEngine_GetAnimationBias(g_game)
	g_vertexAnim = ClientEngine_GetVertexAnimations(g_game)
	g_avatarNames = ClientEngine_GetAvatarNames(g_game)
	g_flashQuality = ClientEngine_GetFlashQuality(g_game)
	g_spotShadows = ClientEngine_GetShadowQuality(g_game)

	local slider = Dialog_GetSlider(gDialogHandle, "sldAnimQuality")	
	Slider_SetValue( slider, g_animBias )

	slider = Dialog_GetSlider(gDialogHandle, "sldCharDetail")	
	Slider_SetValue( slider, g_lodBias )

	slider = Dialog_GetSlider(gDialogHandle, "sldAvatarNames")	
	Slider_SetValue( slider, g_avatarNames )

	slider = Dialog_GetSlider(gDialogHandle, "sldFlash")	
	Slider_SetValue( slider, g_flashQuality )	
	
	if g_gpuskin == 0 then
		local rad = Dialog_GetRadioButton(gDialogHandle, "radSoftware")
		RadioButton_SetChecked(rad, true)
	else
		local rad = Dialog_GetRadioButton(gDialogHandle, "radHardware")
		RadioButton_SetChecked(rad, true)
	end

	local chk = Dialog_GetCheckBox(gDialogHandle, "chkEnvironmentalAnim")
	if g_vertexAnim == 0 then
		CheckBox_SetChecked(chk, false)
	else
		CheckBox_SetChecked(chk, true)
	end
	chk = Dialog_GetCheckBox(gDialogHandle, "chkShadows")
	if g_spotShadows == 0 then
		CheckBox_SetChecked(chk, false)
	else
		CheckBox_SetChecked(chk, true)
	end
	chk = Dialog_GetCheckBox(gDialogHandle, "chkInvertY")
	if g_invertY == 0 then
		CheckBox_SetChecked(chk, false)
	else
		CheckBox_SetChecked(chk, true)
	end
end

function btnOk_OnButtonClicked()
	ClientEngine_SetLodBias(g_game, g_lodBias)
	ClientEngine_SetGPUSkinning(g_game, g_gpuskin)
	ClientEngine_SetAnimationBias(g_game, g_animBias)
	ClientEngine_SetVertexAnimations(g_game, g_vertexAnim)
	ClientEngine_SetFlashQuality(g_game, g_flashQuality)
	ClientEngine_SetAvatarNames(g_game, g_avatarNames)
	ClientEngine_SetShadowQuality(g_game, g_spotShadows)
	ClientEngine_SetInvertY(g_game, g_invertY)

	ClientEngine_WriteConfigSettings(g_game)
	DestroyMenu(gDialogHandle)
end

function btnCancel_OnButtonClicked()
    DestroyMenu(gDialogHandle)
end
function btnClose_OnButtonClicked()
    DestroyMenu(gDialogHandle)
end



