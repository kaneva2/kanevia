dofile("MenuHelper.lua")
dofile("MenuLocation.lua")
dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\CMD.lua")
dofile("..\\Scripts\\MovementIds.lua")
dofile("..\\Scripts\\Animations.lua")
dofile("..\\Scripts\\GameGlobals.lua")

g_game = nil
g_tab = nil
g_gender = nil
g_pageindex = 1
g_maxpages = 1

--Holds all the emote id using  the emotename list index as reference.
--So both the emote idlist and emote name works together.
g_emoteidlist={}
g_emotenamelist={}

g_emoteselectedindex=0
g_emotename=""
g_animationindex=-1
MALE = 3
FEMALE = 4 
SKINNYCHICK = 10
MEDCHICK = 11
HUGECHICK = 12
SKINNYDUDE = 13
MEDDUDE = 14
HUGEDUDE = 15
g_gender=nil
g_nextpage = nil
g_previouspage = nil      
---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	g_dispatcher = dispatcher
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "EmoteAddEvent",KEP.MED_PRIO )	
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)

	Helper_Dialog_OnCreate( dialogHandle )
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	if Dialog_Create == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_MENU_FN )
	end
	
	if KEPConfig_GetNumber == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end
	
	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)
	
	--Get the player Gender
	local player = ClientEngine_GetPlayer(g_game)
	if player ~= nil then
		g_gender = GetSet_GetNumber(player, MovementIds.REFMODEL)
	end
	
	if g_game == 0 then
		g_game = nil
	end
	g_tab = EMOTIONS
	
	-- TODO init here
	local player = ClientEngine_GetPlayer(g_game)
	if player ~= nil then
		g_gender = GetSet_GetNumber(player, MovementIds.REFMODEL)
    end
   

	 
	 --Determines which animations we want to set up.
	 if (g_gender == SKINNYDUDE) or (g_gender == MEDDUDE) or (g_gender == HUGEDUDE) then	
        SetUpMaleAnimations(1)
     else
        SetUpFemaleAnimations(1)
     end
     
	  g_nextpage = Dialog_GetButton(gDialogHandle,"btnNextPage")
	  g_previouspage = Dialog_GetButton(gDialogHandle,"btnPreviousPage")
	  
	  
	  --Hide the next page button if we only have one page.
	  if(g_maxpages==1) then                
	  	Control_SetVisible(g_nextpage,false)
	  end
	  
	  --Hide the previous button when we are on page 1
	  if(g_pageindex==1)then
	  	Control_SetVisible(g_previouspage,false)
	  end		
end

--Added by Jerome to set up the male animation for the 
--new emote system 
function SetUpMaleAnimations(cpage)
	
	--For now all the animations are hard coded until, we can figure out 
	--how we are going to get them from the database.
	if cpage==1 then
		g_emotenamelist[1]="Laugh"
		g_emotenamelist[2]="Wave"
		g_emotenamelist[3]="Sit"
		g_emotenamelist[4]="Head Bang"
		g_emotenamelist[5]="Break Dance"
		g_emotenamelist[6]="Two Steps"
		g_emotenamelist[7]=""
		g_emotenamelist[8]=""
		g_emotenamelist[9]=""
		g_emotenamelist[10]=""
		g_emotenamelist[11]=""
		g_emotenamelist[12]=""
	
			
		--Saving all the animation id's using the emote name as reference
		--Once we can retrive it from the database this will change.
		g_emoteidlist[g_emotenamelist[1]]=anim_laugh
		g_emoteidlist[g_emotenamelist[2]]=anim_wave
        g_emoteidlist[g_emotenamelist[3]]=anim_sit
        g_emoteidlist[g_emotenamelist[4]]=anim_head_bang
        g_emoteidlist[g_emotenamelist[5]]=anim_break_dance
        g_emoteidlist[g_emotenamelist[6]]=anim_hip_hop        
		g_emoteidlist[g_emotenamelist[7]]=anim_laugh
		g_emoteidlist[g_emotenamelist[8]]=anim_wave
        g_emoteidlist[g_emotenamelist[9]]=anim_sit
        g_emoteidlist[g_emotenamelist[10]]=anim_head_bang
        g_emoteidlist[g_emotenamelist[11]]=anim_break_dance
        g_emoteidlist[g_emotenamelist[12]]=anim_hip_hop  
        
	end
	
	 
	--Fill the emote buttons with the correct text.
	for i = 1, 12 do
	 bs = Button_GetStatic(Dialog_GetButton(gDialogHandle, "btnEmote"..tostring(i))) 	
		 if bs~=nil then
			Static_SetText(bs, g_emotenamelist[i])
		 end
	end				
end

--Added by Jerome to set up the female animation for the 
--new emote system 
function SetUpFemaleAnimations(cpage)
	--For now all the animations are hard coded until, we can figure out 
	--how we are going to get them from the database.
	if cpage==1 then
		g_emotenamelist[1]="Laugh"
		g_emotenamelist[2]="Wave"
		g_emotenamelist[3]="Sit"
		g_emotenamelist[4]="Head Bang"
		g_emotenamelist[5]="Break Dance"
		g_emotenamelist[6]="Two Steps"        
		g_emotenamelist[7]= ""
        g_emotenamelist[8]= ""
		g_emotenamelist[9]= ""
		g_emotenamelist[10]= ""
		g_emotenamelist[11]= ""
		g_emotenamelist[12]= ""
	
			
		--Saving all the animation id's using the emote name as reference
		--Once we can retrive it from the database this will change.
		g_emoteidlist[g_emotenamelist[1]]=anim_laugh
		g_emoteidlist[g_emotenamelist[2]]=anim_wave        
        g_emoteidlist[g_emotenamelist[3]]=anim_sit
        g_emoteidlist[g_emotenamelist[4]]=anim_head_bang
        g_emoteidlist[g_emotenamelist[5]]=anim_break_dance
        g_emoteidlist[g_emotenamelist[6]]=anim_hip_hop        
		g_emoteidlist[g_emotenamelist[7]]=anim_laugh
		g_emoteidlist[g_emotenamelist[8]]=anim_wave        
        g_emoteidlist[g_emotenamelist[9]]=anim_sit
        g_emoteidlist[g_emotenamelist[10]]=anim_head_bang
        g_emoteidlist[g_emotenamelist[11]]=anim_break_dance
        g_emoteidlist[g_emotenamelist[12]]=anim_hip_hop           
	end
	
	 
	--Fill the emote buttons with the correct text.
	for i = 1, 12 do
	 bs = Button_GetStatic(Dialog_GetButton(gDialogHandle, "btnEmote"..tostring(i))) 	
		 if bs~=nil then
			Static_SetText(bs, g_emotenamelist[i])
		 end
	end				
end

function btnEmote1_OnButtonClicked(buttonHandle)
	 g_animationindex = g_emoteidlist[g_emotenamelist[1]]  
     ChangeAnimation(g_animationindex)
  	 g_emotename=g_emotenamelist[1]
end

function btnEmote2_OnButtonClicked(buttonHandle)
	 g_animationindex = g_emoteidlist[g_emotenamelist[2]]  
     ChangeAnimation(g_animationindex)
  	 g_emotename=g_emotenamelist[2]
end


function btnEmote3_OnButtonClicked(buttonHandle)
	 g_animationindex = g_emoteidlist[g_emotenamelist[3]]  
     ChangeAnimation(g_animationindex)
  	 g_emotename=g_emotenamelist[3]
end


function btnEmote4_OnButtonClicked(buttonHandle)
	 g_animationindex = g_emoteidlist[g_emotenamelist[4]]  
     ChangeAnimation(g_animationindex)
  	 g_emotename=g_emotenamelist[4]
end


function btnEmote5_OnButtonClicked(buttonHandle)
	 g_animationindex = g_emoteidlist[g_emotenamelist[5]]  
     ChangeAnimation(g_animationindex)
  	 g_emotename=g_emotenamelist[5]
end


function btnEmote6_OnButtonClicked(buttonHandle)
	 g_animationindex = g_emoteidlist[g_emotenamelist[6]]  
     ChangeAnimation(g_animationindex)
  	 g_emotename=g_emotenamelist[6]
end

function btnEmote7_OnButtonClicked(buttonHandle)
	 g_animationindex = g_emoteidlist[g_emotenamelist[7]]  
     ChangeAnimation(g_animationindex)
  	 g_emotename=g_emotenamelist[7]
end

function btnEmote8_OnButtonClicked(buttonHandle)
	 g_animationindex = g_emoteidlist[g_emotenamelist[8]]  
     ChangeAnimation(g_animationindex)
  	 g_emotename=g_emotenamelist[8]
end

function btnEmote9_OnButtonClicked(buttonHandle)
	 g_animationindex = g_emoteidlist[g_emotenamelist[9]]  
     ChangeAnimation(g_animationindex)
  	 g_emotename=g_emotenamelist[9]
end

function btnEmote10_OnButtonClicked(buttonHandle)
	 g_animationindex = g_emoteidlist[g_emotenamelist[10]]  
     ChangeAnimation(g_animationindex)
  	 g_emotename=g_emotenamelist[10]
end

function btnEmote11_OnButtonClicked(buttonHandle)
	 g_animationindex = g_emoteidlist[g_emotenamelist[11]]  
     ChangeAnimation(g_animationindex)
  	 g_emotename=g_emotenamelist[11]
end

function btnEmote12_OnButtonClicked(buttonHandle)
	 g_animationindex = g_emoteidlist[g_emotenamelist[12]]
     ChangeAnimation(g_animationindex)
  	 g_emotename=g_emotenamelist[12]
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

--Call this function to change teh animation.
function ChangeAnimation( animation )
    if ClientEngine_GetPlayer == nil then
    	
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	game = Dispatcher_GetGame(KEP.dispatcher)
	if game~=nil then
		ClientEngine_SetCurrentAnimation(game, animation, 0)
	end
end


function Dialog_OnDestroy(dialogHandle)
	saveLocation(g_game, dialogHandle, "Emotes")
	Helper_Dialog_OnDestroy( dialogHandle )
end


--Close the full emote box.
function btnClose_OnButtonClicked( buttonHandle )
	if ClientEngine_GetPlayer == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	DestroyMenu(gDialogHandle)
end



function Dialog_OnMouseMove(dialogHandle, x, y)
end
