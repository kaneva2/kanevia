dofile("MenuHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")

-- global game object
g_game = nil

-- global handle to controls listbox
g_lbh_controls = nil

-- global boolean to denote when setting a key
g_b_set_key = false
g_sel_index = 0

g_t_btnHandles = {}


---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "controlsUpdatedHandler", 1,0,0,0, "ControlsUpdatedEvent", KEP.MED_PRIO ) 
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	--Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "ControlsUpdatedEvent", KEP.MED_PRIO )
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	if KEPConfig_GetString == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end

	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	if g_game == 0 then
		g_game = nil
	end

	-- setup look of listbox scrollbars
	SetupListBoxScrollBars("lstControls")

	g_lbh_controls = Dialog_GetListBox(gDialogHandle, "lstControls")

	UpdateControlConfig(0)
	
	getButtonHandles()

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

---------------------------------------------------------------------
--
-- Setup look of listbox scrollbars
--
---------------------------------------------------------------------
function SetupListBoxScrollBars(listbox_name)

	local lbh = Dialog_GetListBox(gDialogHandle, listbox_name)
	if lbh ~= 0 then

		local sbh = ListBox_GetScrollBar(lbh)

		if sbh ~= 0 then

			local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then
				--Blue body
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				
				Element_SetCoords(eh, 361, 579, 390, 586)
			end

			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				--Grey lined
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 573, 409, 588)

			end

			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				--Up arrow button
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 547, 409, 561)

			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				--Down Arrow Button
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 642, 409, 656)

			end


		end

	end
end

---------------------------------------------------------------------
--
-- Get the control config
--
---------------------------------------------------------------------
function UpdateControlConfig(sel_index)
	
	ListBox_RemoveAllItems(g_lbh_controls)

	if g_game ~= nil then

		local config_string = ClientEngine_GetControlConfiguration(g_game)
		local t = split(config_string, ",")

		if t ~= 0  then
			local num_args = #t
			if math.fmod(num_args,2) ~= 0 then
				return
			end

			for i = 0, num_args-1 do
				ListBox_AddItem(g_lbh_controls, t[i], tonumber(t[i+1]))
                                i = i + 1
			end
		end		
	end

	ListBox_SelectItem(g_lbh_controls, sel_index)
end

---------------------------------------------------------------------
--
-- Controls Updated Handler
--
---------------------------------------------------------------------
function controlsUpdatedHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

	local sel_index = Event_DecodeNumber( event )

	UpdateControlConfig(g_sel_index)

	UpdateMessage("Assignment complete.")

	g_b_set_key = false

	EnableButtonsDuringSetKey(true)
end

---------------------------------------------------------------------
--
-- Keyboard Handler
--
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)

	-- ESC key
	if key == 27 and g_b_set_key == false then
		
		-- close this menu
		DestroyMenu(gDialogHandle)

	end

end

---------------------------------------------------------------------
--
-- Set Key Clicked
--
---------------------------------------------------------------------
function btnSetKey_OnButtonClicked(buttonHandle)

	g_b_set_key = true

	EnableButtonsDuringSetKey(false)

	if g_game ~= nil then
		g_sel_index = ListBox_GetSelectedItemIndex(g_lbh_controls)
		ClientEngine_PendingControlAssignment(g_game, ListBox_GetSelectedItemData(g_lbh_controls))
	end

	UpdateMessage("The next key you press will be bound.")
end

---------------------------------------------------------------------
--
-- Defaults Clicked
--
---------------------------------------------------------------------
function btnDefaults_OnButtonClicked(buttonHandle)

	if g_game ~= nil then		
		ClientEngine_RestoreDefaultControlConfiguration(g_game)
	end

	UpdateControlConfig(0)

	UpdateMessage("Default configuration restored.")
end

---------------------------------------------------------------------
--
-- Save Config Clicked
--
---------------------------------------------------------------------
function btnSaveConfig_OnButtonClicked(buttonHandle)

	if g_game ~= nil then		
		ClientEngine_SaveControlConfiguration(g_game)
	end

	UpdateMessage("Configuration saved.")
end


--[[
---------------------------------------------------------------------
--
-- Load Config Clicked
--
---------------------------------------------------------------------
function btnLoadConfig_OnButtonClicked(buttonHandle)

	local configString = nil

	if g_game ~= nil then
		configString = ClientEngine_GetControlConfiguration(g_game)
	end
	
	--Dialog_MsgBox(configString)

	UpdateMessage("Configuration loaded.")
end
]]--

---------------------------------------------------------------------
--
-- Set enabled state of buttons while setting a key
--
---------------------------------------------------------------------
function EnableButtonsDuringSetKey(bEnable)

	local bh = Dialog_GetButton(gDialogHandle, "btnDefaults")
	if bh ~= 0 then
		Control_SetEnabled(bh, bEnable)
	end

	bh = Dialog_GetButton(gDialogHandle, "btnSaveConfig")
	if bh ~= 0 then
		Control_SetEnabled(bh, bEnable)
	end
end

---------------------------------------------------------------------
--
-- Update status message
--
---------------------------------------------------------------------
function UpdateMessage(message)

	local sh = Dialog_GetStatic(gDialogHandle, "lblMessage")
	if sh ~= 0 then
		Static_SetText(sh, message)
	end
end

function btnClose_OnButtonClicked( buttonHandle )
	DestroyMenu(gDialogHandle)	
end

---------------------------------------------------------------------
-- Mouse Over
---------------------------------------------------------------------
function getButtonHandles()
--g_t_btnHandles


	local ch = Dialog_GetControl(gDialogHandle, "btnSetKey")
	if ch ~= nil then
     	g_t_btnHandles["btnSetKey"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSaveConfig")
	if ch ~= nil then
     	g_t_btnHandles["btnSaveConfig"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnDefaults")
	if ch ~= nil then
		g_t_btnHandles["btnDefaults"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnClose")
	if ch ~= nil then
     	g_t_btnHandles["btnClose"] = ch
	end


end

function Dialog_OnMouseMove(dialogHandle, x, y)

    local sh = Dialog_GetStatic(dialogHandle, "lblStatus")

	if sh ~= nil then

		if Control_ContainsPoint(g_t_btnHandles["btnSetKey"], x, y) ~= 0 then
			Static_SetText(sh, "Bind Command to a Key")
   		elseif Control_ContainsPoint(g_t_btnHandles["btnSaveConfig"], x, y) ~= 0 then
	    	Static_SetText(sh, "Save Current Configuration")
		elseif Control_ContainsPoint(g_t_btnHandles["btnDefaults"], x, y) ~= 0 then
			Static_SetText(sh, "Load Default Configuration")
		elseif Control_ContainsPoint(g_t_btnHandles["btnClose"], x, y) ~= 0 then
		    Static_SetText(sh, "Close Menu")
		else
            Static_SetText(sh, "Select an Option")
		end
	end

end