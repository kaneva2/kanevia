dofile("MenuHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("CommonFunctions.lua")

-- global game object
g_game = nil


---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	if g_game == 0 then
		g_game = nil
	end

	-- set focus to the default control (should be btnExit)
	Dialog_FocusDefaultControl(dialogHandle)
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

---------------------------------------------------------------------
--
-- Exit Button - Quit game
--
---------------------------------------------------------------------
function btnExit_OnButtonClicked( buttonHandle )

	if g_game ~= nil then
		ClientEngine_Quit( g_game )
	end
end

function btnRetry_OnButtonClicked( buttonHandle )


	if g_game ~= nil then
		gotoMenu( "LoginMenu.xml", 1, 1 )
		
		--DestroyMenu(gDialogHandle)
	end
	--ClientEngine_Connect2( g_game, username, password )

end

