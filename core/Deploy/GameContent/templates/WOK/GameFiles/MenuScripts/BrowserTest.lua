dofile("MenuHelper.lua")
dofile("..\\Scripts\\BrowserIds.lua")

-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--
-- end 

function BrowserCallbackEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)


	if Dialog_Create == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
	end

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end

	type = Event_DecodeNumber( event )

	if gDialogHandle == nil then 
		return KEP.EPR_OK
	end
	local st = Dialog_GetStatic( gDialogHandle, "statusMsg" )
	local text = Event_DecodeString( event )
	local num = Event_DecodeNumber( event )
	
	if type == BrowserIds.BROWSER_STATUSTEXT_CHANGE then -- status text change
		Static_SetText( st, text )
	elseif type == BrowserIds.BROWSER_UPDATE_PROGRESS then -- % complete
		Static_SetText( st, tostring(num).."% complete" )
	elseif type == BrowserIds.BROWSER_KGP_LINK then
		Dialog_MsgBox( "You clicked a kgp link: "..text )
		local br = Dialog_GetStatic( gDialogHandle, "browser" )
		Static_SetText( br, "http://www.kaneva.com" )
	else
		Static_SetText( st, "Huh?.  Type: "..tostring(type).." string "..st.." num "..num )
	end 

	
	return KEP.EPR_OK
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "BrowserCallbackEventHandler", 1,0,0,0, "BrowserCallbackEvent", KEP.HIGH_PRIO ) 
	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "BrowserCallbackEvent", KEP.MED_PRIO )


end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

  -- set url
	local sh = Dialog_GetStatic(gDialogHandle, "ctrlBrowser")
	if sh ~= 0 then
		MessageBox("Got browser")
		Static_SetText(sh, "http://www.ibm.com")
	else
		MessageBox("Didn't get browser")
	end	

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnNext_OnButtonClicked(buttonHandle)

	local brh = Dialog_GetBrowser(gDialogHandle, "browser")
	if brh ~= 0 then

		Browser_Navigate( brh, BrowserIds.BROWSER_FORWARD );
	end
end

function btnPrev_OnButtonClicked(buttonHandle)

	local brh = Dialog_GetBrowser(gDialogHandle, "browser")
	if brh ~= 0 then

		Browser_Navigate( brh, BrowserIds.BROWSER_BACK);
	end
end

function btnStop_OnButtonClicked(buttonHandle)

	local brh = Dialog_GetBrowser(gDialogHandle, "browser")
	if brh ~= 0 then

		Browser_Navigate( brh, BrowserIds.BROWSER_STOP );
	end
end

function btnReload_OnButtonClicked(buttonHandle)

	local brh = Dialog_GetBrowser(gDialogHandle, "browser")
	if brh ~= 0 then

		Browser_Navigate( brh, BrowserIds.BROWSER_RELOAD );
	end
end

function btnClose_OnButtonClicked(buttonHandle)

		DestroyMenu(gDialogHandle)
end		

