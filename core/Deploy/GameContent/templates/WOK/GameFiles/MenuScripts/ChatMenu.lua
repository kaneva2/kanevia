dofile("MenuHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\CMD.lua")
dofile("..\\Scripts\\MovementIds.lua")
dofile("..\\Scripts\\Animations.lua")
dofile("..\\ScriptData\\CustomColors.lua")
dofile("..\\Scripts\\ChatType.lua")
dofile("languageFilter.lua")
dofile("MenuLocation.lua")
dofile("CommonFunctions.lua")
dofile("MenuStyles.lua")
dofile("WebSuffix.lua")
--dofile("..\\Scripts\\Chattype.lua")

OPEN_POKER_EVENT = "OpenPokerWindow"
PREF_EVENT = "PrefEvent"



-- New message icons dont quite line up.  Change offset
-- to make sure they align with text properly.
BUTTON_OFFSET_Y = 10
BUTTON_OFFSET_X = 15

g_game = nil

g_chatlines = {}
g_chattypes = {}

g_actionXs = {}
g_actionYs = {}

g_displaylines = {}
g_displaylinecount = 0

-- Chat history globals
g_chatlinecnt = 10
g_chatlinelen = 58
g_chatbuffersize = 60
g_chatscrollpos = 0
g_chatcurrentframetrans = 2130706432
CHAT_BACKCOLOR = 3422552064
g_chattranslevel = 5


g_nop = nil

g_dropdownmenu = nil
g_dispatcher = nil

-- Positioning globals
g_chathistopen = 1
g_transparent = 0
g_x_resizeoffset = 0
g_y_resizeoffset = 0
g_h_size = 533
g_v_size = 167

g_typing = false        -- typing in the chat bar at the bottom
g_sizing = false		-- resizing the window.
g_ignoreEnter = false
--g_trans

--globals for things we hide, show, and move
g_chathistbtn = nil
g_chattext    = nil
g_public	  = nil
g_mainframe   = nil
g_backdrop    = nil
g_edtSendBar  = nil
g_imgMouse    = nil
g_resizeBar   = nil
g_inctransback= nil
g_dectransback= nil
g_translevels = {}
g_line1	  = nil
g_line2	  = nil
g_line3 	  = nil
g_line4       = nil
g_line5	  = nil
g_line6       = nil
g_line7       = nil
g_line8	  = nil

-- who we got last tell from
g_replyTo 	  = ""

-- chat type constants
ctTalk 	= 0
ctGroup	= 1
ctClan	= 2

g_chat_type = ctTalk

-- globals for timer for system messages
g_TimerEvent = 0
MESSAGE_TIMER = "MessageTimer"
MESSAGE_TIMER_INTERVAL = 10
CHAT_KEY_PRESSED_EVENT = "ChatKeyPressedEvent"
TYPE_START_EVENT = "StartTypingEvent"
HUD_EVENT = "HudEvent"
ENTER_DELAY_EVENT = "EnterDelayEvent"
RENDER_LOCALE_TEXT_EVENT = "RenderLocaleTextEvent"
REPLY_UPDATE_EVENT = "ReplyUpdateEvent"
REPLY_REQUEST_EVENT = "	ReplyRequestEvent"

gDialogHandle = nil

--toggle for naughty word filter
gUseFilter = true

-- Hold previous chat entries.  Just for you Jim!
g_cmdBuffer = {}

-- Position in our command history
g_bufferPos = 0


---------------------------------------------------------------------
--
-- Go To  menu
--
---------------------------------------------------------------------
function openFlashMenu(game, bClose, bModal)

	menu_path = "LargeFlashGame.xml"
	dh = Dialog_Create( menu_path );
 	if dh == 0 then
		Dialog_MsgBox("Menu [" .. menu_path .. "] not found.")
	end

	if bModal == 1 and dh ~= 0 then
		Dialog_SendToBack( gDialogHandle )
	end

	-- set the text of the flash control
	flash = Dialog_GetStatic( dh, "flashControl" )
 	Static_SetText( flash, game )
	
	
	-- close this dialog
	if bClose == 1 then
		DestroyMenu(gDialogHandle)
	end

	return dh

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "textHandler", 1,0,0,0, "RenderTextEvent", KEP.MED_PRIO )	 
	ParentHandler_RegisterEventHandler( dispatcher, handler, "attribEventHandler", 1,0,0,0, "AttribEvent", KEP.MED_PRIO )	
 	ParentHandler_RegisterEventHandler( dispatcher, handler, "systemMessageUpdateHandler", 1,0,0,0, MESSAGE_TIMER, KEP.LOW_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "chatKey", 1,0,0,0, CHAT_KEY_PRESSED_EVENT, KEP.HIGH_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "hudEventHandler", 1,0,0,0, HUD_EVENT, KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "typeStartHandler", 1,0,0,0, TYPE_START_EVENT, KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "prefEventHandler", 1,0,0,0, PREF_EVENT, KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "enterDelayHandler", 1,0,0,0, ENTER_DELAY_EVENT, KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "renderLocaleTextHandler", 1,0,0,0, RENDER_LOCALE_TEXT_EVENT, KEP.HIGH_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "ReplyRequestHandler", 1,0,0,0, REPLY_REQUEST_EVENT, KEP.HIGH_PRIO )

	g_dispatcher = dispatcher
end

function ReplyRequestHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	--Dialog handle of requester
	local handle = Event_DecodeNumber( event )
	
	fireReplyUpdateEvent()

end

function prefEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local lang_pref = Event_DecodeString( event )
 	if lang_pref == "true" then
 	    gUseFilter = true
 	else
 	    gUseFilter = false
 	end
	return KEP.EPR_CONSUMED
end


function OpenPokerWindowHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- extract out the URL
	url = Event_DecodeString( event )
	ParentHandler_LogMessage( KEP.parent, KEP.ERROR_LOG_LEVEL,  "OpenPoker with URL of "..tostring(url) )
	openFlashMenu( url, 0, 0 )
	return KEP.EPR_CONSUMED
end 

function hudEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local msg = Event_DecodeString( event )
	table.insert( g_chatlines, msg)
	table.remove( g_chatlines, 0)

	table.insert( g_chattypes, ChatType.System)
	table.remove( g_chattypes, 0)
	genDisplayLines()
	renderText()
end

function enterDelayHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	--Dialog_MsgBox("delay event")
	EditBox_ClearText( g_edtSendBar )
	g_ignoreEnter = false
	-- testing
	if g_nop ~= nil then
		Dialog_FocusDefaultControl( g_nop )
	end
	Dialog_SendToBack(gDialogHandle)
	Dialog_SetModal(gDialogHandle, false)
	ih = Image_GetControl(g_imgMouse)
	Control_SetEnabled(ih, false)
	Control_SetVisible(ih, false)
	ClientEngine_ClearChatFocus(g_game)
end

function typeStartHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local msg = Event_DecodeString( event )
	local edt = Dialog_GetEditBox(gDialogHandle, "edtSendBar")
	if g_chathistopen == 0 then
		g_chathistopen = 1
		doUnMinimize()
		setChatPosition()
	end
	if edt ~= nil then
		EditBox_ClearText(edt)
		EditBox_SetText(edt, msg, false)
	end
	Dialog_FocusDefaultControl( gDialogHandle )
end

function systemMessageUpdateHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	Static_SetText(g_systemMsg, "") 
	-- Dispatcher_QueueEventInFuture( dispatcher, g_TimerEvent, MESSAGE_TIMER_INTERVAL ) 
end

--[[
function cbFilter_OnCheckBoxChanged( )

	if gUseFilter then
		gUseFilter = false
	else
	   	gUseFilter = true
	end

end


]]--

function chatKey()
	if g_ignoreEnter == false then
		if g_chathistopen == 0 then
			g_chathistopen = 1
			doUnMinimize()
			setChatPosition()
		end
		Dialog_FocusDefaultControl( gDialogHandle )
	else
		--g_ignoreEnter = false
		--Dialog_MsgBox("Ignoring that enter")
	end
end

function renderLocaleTextHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local stringID = Event_DecodeNumber( event )
	
	if ( stringID == 120 ) then
		local chatType = Event_DecodeNumber( event )
		local msg = Event_DecodeString( event )
		--string is empty so fill with message			
		createLinkMessage( "You do not have the passes required to do that.", "Purchase Access Pass", GameGlobals.WEB_SITE_PREFIX_KANEVA..BUY_ACCESS_PASS_SUFFIX)
	end

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, MESSAGE_TIMER, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, HUD_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, TYPE_START_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, CHAT_KEY_PRESSED_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, PREF_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, ENTER_DELAY_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, REPLY_UPDATE_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, REPLY_REQUEST_EVENT, KEP.MED_PRIO )

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	g_chat_type = ctTalk
	

	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	
	if KEPConfig_GetString == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end	

	Dialog_SetIgnoreArrowKeys(dialogHandle, true)
	for i = 1, g_chatbuffersize do
		g_chatlines[i] = " "
		g_chattypes[i] = 0
	end


	width  = Dialog_GetWidth( dialogHandle )
	btn = Dialog_GetButton( dialogHandle, "btnQuit" )
	Control_SetLocation( btn, 560, 11)

	local lst = Dialog_GetListBox(gDialogHandle, "lstChatHist")
	ListBox_SetHTMLEnabled(lst, true)
	if lbh ~= 0 then
		local sbh = ListBox_GetScrollBar(lst)
		if sbh ~= 0 then
			local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then
				--Blue body
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				
				Element_SetCoords(eh, 185, 55, 186, 56)
			end
			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then

				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 572, 409, 589)

			end
			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then

			Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 547, 409, 561)

			end
			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then

				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 642, 409, 656)

			end
		end
	end

	g_chathistbtn = Dialog_GetImage(   dialogHandle, "btnMinimize"        )
	g_public	  = Dialog_GetButton(   dialogHandle, "btnPublic"         )
	g_chattext    = Dialog_GetStatic(   dialogHandle, "lblChat"           )
	g_mainframe   = Dialog_GetImage(    dialogHandle, "imgMainFrame"      )
	g_backdrop    = Dialog_GetImage(    dialogHandle, "imgBackDrop"       ) 
	g_edtSendBar  = Dialog_GetEditBox(  dialogHandle, "edtSendBar"        )
	g_imgMouse    = Dialog_GetControl(  dialogHandle, "imgMouseExploit"   )
	g_titleBarTxt = Dialog_GetStatic(   dialogHandle, "lblTitleBarText"   )
	g_systemMsg   = Dialog_GetStatic(   dialogHandle, "lblSystemMsg"      )
	g_resizeBar   = Dialog_GetImage(	dialogHandle, "imgResize"	      )
	g_inctransback= Dialog_GetButton(	gDialogHandle, "btnIncTrans"	  )
	g_dectransback= Dialog_GetButton(	gDialogHandle, "btnDecTrans"      )
	g_line1	  = Dialog_GetImage(	gDialogHandle, "trimH1"			)
	g_line2	  = Dialog_GetImage(	gDialogHandle, "trimH2"			)
	g_line3 	  = Dialog_GetImage(	gDialogHandle, "trimH3"			)
	g_line4       = Dialog_GetImage(	gDialogHandle, "trimH4"			)
	g_line5	  = Dialog_GetImage(	gDialogHandle, "trimV1"			)
	g_line6       = Dialog_GetImage(	gDialogHandle, "trimV2"			)
	g_line7       = Dialog_GetImage(	gDialogHandle, "trimV3"			)
	g_line8	  = Dialog_GetImage(	gDialogHandle, "trimV4"			)
	
	g_translevels[0] = 0 	--in hex, 0x19000000
	g_translevels[1] = 419430400 	--in hex, 0x19000000
	g_translevels[2] = 855638016 	--in hex, 0x33000000
	g_translevels[3] = 1275068416 	--in hex, 0x4C000000
	g_translevels[4] = 1711276032 	--in hex, 0x66000000
	g_translevels[5] = 2130706432 	--in hex, 0x7F000000
	g_translevels[6] = 2550136832 	--in hex, 0x98000000
	g_translevels[7] = 2986344448 	--in hex, 0xB2000000
	g_translevels[8] = 3405774848 	--in hex, 0xCB000000
	g_translevels[9] = 3841982464	--in hex, 0xE5000000
	g_translevels[10] = 4278190080 	--in hex, 0xFF000000
	
	Dialog_SetModal(dialogHandle, false)
	g_transparent = 1
	Static_SetText( g_chattext, "")
	Static_SetText(g_systemMsg, "")
	exploitMouseDown( dialogHandle, -5000, -5000 )
	g_nop = Dialog_Create( "nop.xml" )
	
	loadLocation(g_game, dialogHandle, "Chat")
	getChatWindowInfo()
	genDisplayLines()
	setChatPosition()
	renderText()
	g_chatscrollpos = 0
	
	--Element_GetTextureColor(Image_GetDisplayElement(Dialog_GetImage(gDialogHandle, "imgMainFrame")))
	--local transparencyadjust =  tonumber("7F", 16)
	--g_chatcurrentframetrans = 2130706432--tonumber(string.sub(string.format("%x", transparencyadjust), 1, 2) .. string.sub(string.format("%x", CHAT_BACKCOLOR), 3, 8), 16)
	
	--Display the Press enter to chat in the text box.
	EditBox_SetText(g_edtSendBar,"Press Enter To Chat",false);
end


function getChatWindowInfo()

--	local x = 500
--	local y = 200
	local hsize = 500
	local vsize = 200
--	local okx
--	local oky
	local okhsize
	local okvsize
	local config = ClientEngine_OpenConfig( g_game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )
	if config ~= nil then
		--Dialog_MsgBox("Here")
		--okx, x = KEPConfig_GetNumber( config, "ChatX", x )
		--oky, y = KEPConfig_GetNumber( config, "ChatY", y )
		okhsize, hsize = KEPConfig_GetNumber( config, "ChatHSize", g_h_size )
		okvsize, vsize = KEPConfig_GetNumber( config, "ChatVSize", g_v_size )
       	local oktrans, trans = KEPConfig_GetNumber( config, "ChatTransparency", g_chatcurrentframetrans )
       	local oktlvl, tlvl = KEPConfig_GetNumber(config, "ChatTLevel", g_chattranslevel)
       	--Dialog_MsgBox("tlvl= " .. tlvl)
		local okfilter, filter = KEPConfig_GetString( config, "ChatFilter", tostring(gUseFilter) )
		local okhist, history = KEPConfig_GetString(config, "ChatHistory" , "No save chat history.")
		local okreply, reply = KEPConfig_GetString(config, "ChatReply" , g_replyTo)
		g_replyTo = reply
		
		g_chatcurrentframetrans = trans
		g_chattranslevel = tlvl
		if filter == "true" then
		    gUseFilter = true
		else
		    gUseFilter = false
		end
		
		regenChatBuf( history )
	end
	g_h_size = hsize
	g_v_size = vsize	
    setChatPosition()
    
    --Dialog_MsgBox("getChatWindowInfo() " .. g_chattranslevel)
    
   
end


--reconstruct the chat buffer with the history from the config file

function regenChatBuf( strHist )

	
	-- previous position of tag found
	local lastPos = 1
	-- beginning and end of found string
	local start = 1
	local stop = 1
	local chatLine = ""
	local tag = ""
	
	for i = 1, g_chatbuffersize do
		lastPos = stop  -- remember where we were in string
		
		--get location of tag that delimits new line of text and chat type
	    start, stop, type = string.find(strHist, "<kbr (%d+)>", lastPos)

		--check to make sure we found the next tag in the string
		if start == nil or stop == nil then
			return 0
  		else
  		    g_chattypes[i] = tonumber(type)
			--take text from between end of previous tag and just before start of next tag
        	chatLine = string.sub(strHist, lastPos, start-1)
        	g_chatlines[i] = chatLine
    		stop = stop + 1
		end

	end
	

	genDisplayLines()
end

function saveChatWindowInfo()
	-- get last chat coordinates if exist
--	local x = Dialog_GetLocationX(gDialogHandle)
--	local y = Dialog_GetLocationY(gDialogHandle)
	local config = ClientEngine_OpenConfig( g_game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )
	if config ~= nil then
--		KEPConfig_SetNumber( config, "ChatX", x )
--		KEPConfig_SetNumber( config, "ChatY", y )
		KEPConfig_SetNumber( config, "ChatHSize", g_h_size )
		KEPConfig_SetNumber( config, "ChatVSize", g_v_size )
		KEPConfig_SetNumber( config, "ChatTransparency", g_chatcurrentframetrans )
		KEPConfig_SetNumber( config, "ChatTLevel", g_chattranslevel )
		KEPConfig_SetString( config, "ChatFilter", tostring(gUseFilter) )
		KEPConfig_SetString( config, "ChatHistory", genChatHistStr())
		
		if g_replyTo ~= nil then
			KEPConfig_SetString( config, "ChatReply", g_replyTo)
		end

		KEPConfig_Save( config )
		
		--Dialog_MsgBox("saveChatWindowInfo() " .. g_chattranslevel)
	end
end
---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	--if g_nop ~= nil then
	--	Dialog_Destroy(g_nop)
	--end
	
	saveChatWindowInfo()
	saveLocation(g_game, dialogHandle, "Chat")	
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

---------------------------------------------------------------------
--
-- Loops through backwards rendering the text and counting the output
-- lines.
--
---------------------------------------------------------------------

function renderText()
	local text = ""

	local lbh = Dialog_GetListBox(gDialogHandle, "lstChatHist")  --list box handle
	local sbh = ListBox_GetScrollBar(lbh)  						 -- scroll bar handle
	local sb_control = ScrollBar_GetControl(sbh)
	local visible_line_cnt = ScrollBar_GetPageSize( sbh )
	local icon_cnt = 1
	local scroll_pos = ScrollBar_GetTrackPos( sbh )
	local lb_row_height = ListBox_GetRowHeight( lbh )
	--Dialog_MsgBox("renderText")	
	if g_chathistopen ~= 0 then
		ClearActionIcons()	
		--Dialog_MsgBox("ChatHistOpen")
		local i = 1
		for index = scroll_pos, (scroll_pos + (visible_line_cnt - 1)) do
			 
			if ListBox_GetItemData(lbh, index) ~= 0 then
				local mybutton = Dialog_GetImage(gDialogHandle, "imgAction" .. icon_cnt)
				local mycontrol = Image_GetControl(mybutton)
				local list_control = ListBox_GetControl( lbh )
				local buttonwidth = Static_GetStringWidth( g_chattext, ListBox_GetItemText(lbh, index) )
				local butx = buttonwidth + BUTTON_OFFSET_X
				local buty = (Control_GetLocationY( list_control ) + lb_row_height * i) - BUTTON_OFFSET_Y
				local maxWidth = Control_GetLocationX( list_control ) + Control_GetWidth( list_control )
				
				if (butx + Control_GetWidth(mycontrol)) > Control_GetLocationX(sb_control) then
					butx = Control_GetLocationX(sb_control) - Control_GetWidth(mycontrol) - BUTTON_OFFSET_X
				end
				
				Control_SetLocation(mycontrol, butx, buty)
				g_actionXs[index] = butx
				g_actionYs[index] = buty
				Control_SetVisible(mycontrol, true)
				Control_SetEnabled(mycontrol, true)
				icon_cnt = icon_cnt + 1
			end
			i = i + 1
		end
	end
	
end

function lstChatHist_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	renderText()
end

--Concate all strings in buffer into onestring to save.
function genChatHistStr()

	local chatString = ""
	local typeString = ""
	--Dialog_MsgBox("genChatHistStr")
	for i = 1, g_chatbuffersize do

		if g_chatlines[ i ] ~= "" and g_chattypes[i] ~= nil then

		    chatString = chatString..g_chatlines[i].."<kbr "..g_chattypes[i]..">"
		
		end	
	end
	return chatString

end

function breaktoken(message)
	local spcpos = string.find(message, " ")
	local brackpos = string.find(message, "<")
	local tokenpos
	local work
	local restmessage
	local tag = false
	if message == nil then
		return nil, nil
	end
	if spcpos == nil and brackpos == nil then
		return message, nil
	elseif spcpos ~= nil and brackpos == nil then
		tokenpos = spcpos
		tag = false
	elseif spcpos == nil and brackpos ~= nil then
		if brackpos == 1 then
			tag = true
		else
			tag = false
		end
		tokenpos = brackpos
	else
		if brackpos == 1 then
			tag = true
		else
			tag = false
		end
		tokenpos = math.min(spcpos, brackpos)
	end
	if tag == true then
		local closepos = string.find(message, ">")
		if closepos ~= nil then
			-- Hey this is an HTML tag... the entire tag is one token
			word = string.sub(message, 1, closepos)
			restmessage = string.sub(message, closepos + 1, -1)
		else
			-- Some loose bracket was in the string
			word = string.sub(message, 1, tokenpos)
			restmessage = string.sub(message, tokenpos + 1, -1)
		end
	else
		word = string.sub(message, 1, tokenpos)
		restmessage = string.sub(message, tokenpos + 1, -1)
	end
	--Dialog_MsgBox(word)
	return word, restmessage
end

function getColorText(type)
	if type == ChatType.Clan then
		colortext = "<font color=" .. chatcolor.clan ..">"
	elseif type == ChatType.Group then
		colortext = "<font color=" .. chatcolor.group ..">"
	elseif type == ChatType.System then
		colortext = "<font color=" .. chatcolor.system ..">"
	elseif type == ChatType.Whisper then
		colortext = "<font color=" .. chatcolor.whisper ..">"
	elseif type == ChatType.Shout then
		colortext = "<font color=" .. chatcolor.shout ..">"
	elseif type == ChatType.Private then
		colortext = "<font color=" .. chatcolor.private ..">"
	else
		colortext = "<font color=" .. chatcolor.talk ..">"
	end
	return colortext
end


function genDisplayLines()
	g_displaylines = {}
	g_displaylinecount = 0
	local chatwidth = Control_GetWidth(Static_GetControl(g_chattext))
	g_chatlinecnt = (g_v_size / 15.5) - .5 
	--Dialog_MsgBox("genDisplayLines")
	-- Loop through all of the messages in our chat history
	for i = 1, g_chatbuffersize do
		-- sanity check
		if g_chatlines[ i ] ~= "" and g_chattypes[ i ] ~= nil then
			-- now identify the color of the text.
			--Dialog_MsgBox("genDisplayLines inside loop")
			local colortext = getColorText(g_chattypes[i])
			local currentlineonthisline = ""
			-- Now the magic
			if Static_GetStringWidth(g_chattext, g_chatlines[i]) < chatwidth then
				-- This message will fit one one line
				local text =  colortext .. g_chatlines[ i ] .. "</font>" .. "<br>"
				table.insert( g_displaylines, 1, text)
				g_displaylinecount = g_displaylinecount + 1
			else
				-- This is not going to fit on one line.
				local wholemessage = g_chatlines[i]
				local lineover = false
				while wholemessage ~= nil do
					lineover = false
					local finishedline = false
					-- loop through entire message
					word, wholemessage = breaktoken(wholemessage)
					currentlineonthisline = ""
					while finishedline == false do
						-- loop through a line
						if word == nil then
							word = ""
						end
						if wholemessage == nil then
							wholemessage = ""
						end
						if Static_GetStringWidth(g_chattext, currentlineonthisline .. word) > chatwidth then
							-- need to wrap
							if string.len( currentlineonthisline ) == 0 then 
								-- one loooong line, truncate it
								currentlineonthisline = word
								lineover = true
								finishedline = true
							else
								lineover = true
								finishedline = true
								wholemessage = word .. wholemessage
							end
						else
							currentlineonthisline = currentlineonthisline .. word
							lineover = false
							finishedline = false
							word, wholemessage = breaktoken(wholemessage)
						end
						if wholemessage == nil then
							finishedline = true
						end
					end
					if lineover == true then
						local text =  colortext .. currentlineonthisline .. "</font>" .. "<br>"
						table.insert(g_displaylines, 1, text)
						g_displaylinecount = g_displaylinecount + 1
					else
						if word ~= nil then
							local text =  colortext .. currentlineonthisline .. word .. "</font>" .. "<br>"
							table.insert(g_displaylines, 1, text)
							g_displaylinecount = g_displaylinecount + 1
						end
					end
				end
			end
		end
	end
	-- Ok we just computed the lines as they should be displayed
	local lstbox = Dialog_GetListBox(gDialogHandle, "lstChatHist")
	ListBox_RemoveAllItems(lstbox)
	for i = 1, g_chatbuffersize do
		local line = g_displaylines[g_chatbuffersize + 1 - i] 
		if line ~= nil and line ~= "" then
			if string.find(line, "</messagecenter>") ~= nil then		
				ListBox_AddItem(lstbox, line, 1)
			else
				ListBox_AddItem(lstbox, line, 0)
			end
			
		end
	end
	ListBox_SelectItem(lstbox, g_chatbuffersize)
end

---------------------------------------------------------------------
--
-- Gets the 4096 event from the client that tells us to render
--
---------------------------------------------------------------------
function attribEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	-- text readout event
	if filter == 4096 then
	
		local msg   = Event_DecodeString( event )
		local red   = Event_DecodeNumber( event )
		local green = Event_DecodeNumber( event )
		local blue  = Event_DecodeNumber( event )
		local type  = Event_DecodeNumber( event )

		table.insert( g_chatlines, msg)
		table.remove( g_chatlines, 0)
		
		table.insert( g_chattypes, type)
		table.remove( g_chattypes, 0)
		
		genDisplayLines()
		renderText()
		if type == ChatType.System then
			local colortext = "<font color=" .. chatcolor.system ..">" .. msg .. "</font>" .. "<br>"
			Static_SetText(g_systemMsg, colortext)
			g_TimerEvent = Dispatcher_MakeEvent( g_dispatcher, MESSAGE_TIMER )
			Dispatcher_QueueEventInFuture( g_dispatcher, g_TimerEvent, MESSAGE_TIMER_INTERVAL ) 
		end
		return KEP.EPR_OK --KEP.EPR_CONSUMED
	end
end


function textHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	if ClientEngine_GetPlayer == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	--Dialog_MsgBox("textHandler")
	local msg   = Event_DecodeString( event )
	local type  = Event_DecodeNumber( event )


	if gUseFilter then
		msg = langFilter(msg)
	end

	local lmsg = string.lower(msg)


	-- Check to see if we are in the midst of traveling.  If we are then the
	-- GetPlayer call will fail and bomb out chat to old system
	if not(type == ChatType.System and string.find(msg, "Travel")) then


 		local game = Dispatcher_GetGame( KEP.dispatcher )
		local player = ClientEngine_GetPlayer( game )
 		local name = string.lower( GetSet_GetString( player, MovementIds.NAME ) )

		if string.find(lmsg, name) == 1 then
			--look for hey,hi,hello,yo,howdy and play a sound
			-- added space delimiter to avoid things like yesterday,  know, etc. triggering
			local padMsg = " "..lmsg.." "
			
			if string.find( padMsg, "%syes%s" ) ~= nil then
				ClientEngine_SetCurrentAnimation( Dispatcher_GetGame( KEP.dispatcher), anim_yes, 0)
			elseif string.find( padMsg, "%sno%s" ) ~= nil then
				ClientEngine_SetCurrentAnimation( Dispatcher_GetGame( KEP.dispatcher), anim_no, 0)
			elseif string.find( padMsg, "%sawesome%s" ) ~= nil or string.find( padMsg, "%scheer%s" ) ~= nil then
				ClientEngine_SetCurrentAnimation( Dispatcher_GetGame( KEP.dispatcher), anim_cheer, 0)
			elseif string.find( padMsg, "%slaugh%s" ) ~= nil or string.find( padMsg, "%slol%s" ) ~= nil or string.find( padMsg, "%srofl%s" ) ~= nil then
				ClientEngine_SetCurrentAnimation( Dispatcher_GetGame( KEP.dispatcher), anim_laugh, 0)
			elseif string.find( padMsg, "%ssad%s" ) ~= nil or string.find( padMsg, "%ssucks%s" ) ~= nil then
				ClientEngine_SetCurrentAnimation( Dispatcher_GetGame( KEP.dispatcher), anim_bow, 0)
			end
		end

		local replyTo = nil
		ret, ret2, replyTo, ret = string.find( msg, "(.+) tells you >" )
		if replyTo ~= nil then
			g_replyTo = replyTo
			
            fireReplyUpdateEvent()
			
		end

	end

	-- proceed normally
	table.insert( g_chatlines, msg)
	table.remove( g_chatlines, 0)

	table.insert( g_chattypes, type)
	table.remove( g_chattypes, 0)
	-- end



	genDisplayLines()
	renderText()
	
	if type == ChatType.System then
		local colortext = "<font color=" .. chatcolor.system ..">" .. msg .. "</font>" .. "<br>"
		Static_SetText(g_systemMsg, colortext)
		g_TimerEvent = Dispatcher_MakeEvent( g_dispatcher, MESSAGE_TIMER )
		Dispatcher_QueueEventInFuture( g_dispatcher, g_TimerEvent, MESSAGE_TIMER_INTERVAL ) 
	end
	return KEP.EPR_CONSUMED
end

function fireReplyUpdateEvent()

	--Fire event for kdp menu chat so we can reply appropriately
	local updateReply = Dispatcher_MakeEvent( KEP.dispatcher, REPLY_UPDATE_EVENT )
	Event_EncodeString( updateReply, g_replyTo )

	Dispatcher_QueueEvent( KEP.dispatcher, updateReply )

end

--[[
function imgChatHist_OnLButtonDown( dialogHandle, x, y )

	if g_chathistopen == 1 then
		g_chathistopen = 0
		doMinimize()
	else
		g_chathistopen = 1
		doUnMinimize()
		setChatPosition()
	end
end
]]--
function doMinimize()
	ClearActionIcons()
	Control_SetVisible( Static_GetControl( g_chattext ), false )
	Control_SetLocation( Image_GetControl( g_chathistbtn ), 74, -12)
	Dialog_SetSize(gDialogHandle, 73, 40)
	Control_SetSize(Image_GetControl(g_mainframe), 100, 30)
	Control_SetVisible( Image_GetControl( g_chathistbtn ), true ) 

	Control_SetVisible( Button_GetStatic( g_public ), false )
	Control_SetVisible( Image_GetControl( g_mainframe ), true )
	Control_SetVisible( Image_GetControl( g_backdrop ), false )  
	Control_SetVisible( EditBox_GetControl( g_edtSendBar ), false )
	Control_SetVisible( Image_GetControl(g_resizeBar), false )
	
	Control_SetVisible( Button_GetControl( g_inctransback ), false )
	Control_SetVisible( Button_GetControl( g_dectransback ), false )
	
	Control_SetVisible( Image_GetControl( g_line1 ), false )
	Control_SetVisible( Image_GetControl( g_line2 ), false )
	Control_SetVisible( Image_GetControl( g_line3 ), false )
	Control_SetVisible( Image_GetControl( g_line4 ), false )
	Control_SetVisible( Image_GetControl( g_line5 ), false )
	Control_SetVisible( Image_GetControl( g_line6 ), false )
	Control_SetVisible( Image_GetControl( g_line7 ), false )
	Control_SetVisible( Image_GetControl( g_line8 ), false )
	local chatlst = Dialog_GetListBox(gDialogHandle, "lstChatHist")
	Control_SetVisible( Image_GetControl(chatlst), false)
	Control_SetEnabled( Image_GetControl(chatlst), false)
end


function doUnMinimize()
	Control_SetVisible( Image_GetControl( g_chathistbtn ), true ) 
	Control_SetVisible( Static_GetControl( g_chattext ), true )
	Control_SetVisible( Button_GetStatic( g_public ), true )  
	Control_SetVisible( Image_GetControl( g_mainframe ), true )
	Control_SetVisible( Image_GetControl( g_backdrop ), true )  
	Control_SetVisible( EditBox_GetControl( g_edtSendBar ), true )
	Control_SetVisible( Image_GetControl(g_resizeBar), true )
	Control_SetVisible( Button_GetControl( g_inctransback ), true )
	Control_SetVisible( Button_GetControl( g_dectransback ), true )
	Control_SetVisible( Image_GetControl( g_line1 ), true )
	Control_SetVisible( Image_GetControl( g_line2 ), true )
	Control_SetVisible( Image_GetControl( g_line3 ), true )
	Control_SetVisible( Image_GetControl( g_line4 ), true )
	Control_SetVisible( Image_GetControl( g_line5 ), true )
	Control_SetVisible( Image_GetControl( g_line6 ), true )
	Control_SetVisible( Image_GetControl( g_line7 ), true )
	Control_SetVisible( Image_GetControl( g_line8 ), true )

	local chatlst = Dialog_GetListBox(gDialogHandle, "lstChatHist")
	Control_SetVisible( Image_GetControl(chatlst), true)
	Control_SetEnabled( Image_GetControl(chatlst), true)
	renderText() -- To display the actionicons
end

function edtSendBar_OnFocusIn(editBoxHandle)

    local ebh = Dialog_GetEditBox(gDialogHandle, "edtSendBar")
    if ebh ~= 0 then
		local currentText = EditBox_GetText( ebh )
		if currentText == "Press Enter To Chat" then
		    EditBox_SetText( g_edtSendBar, "", false )
		end
    end

    g_typing = true
	g_sizing = false
	--Dialog_MsgBox("bar got focus")	
	if gDialogHandle ~= nil then
		Dialog_SetModal(gDialogHandle, true)
		Dialog_BringToFront(gDialogHandle)
		ih = Image_GetControl(g_imgMouse)
		Control_SetEnabled(ih, true)
		Control_SetVisible(ih, true)
	end
end

---------------------------------------------------------------------
-- 'ENTER' key was pressed within the bottom bar
---------------------------------------------------------------------
function edtSendBar_OnEditBoxString(editBoxHandle, password)
	local msg = EditBox_GetText(editBoxHandle)
	--Dialog_MsgBox(msg)
	g_bufferPos = 0
	
--Clear Chat History-----------------------------------------------------
	if g_game ~= nil and msg ~= "" then
		--Dialog_MsgBox("You Entered text")
		table.insert(g_cmdBuffer, 1, msg)
		-- see if the user wants to clear chat history, by looking for /c or /clear

		local clearKey = "/clear"
		local clearKey2 = "/c"
		--Dialog_MsgBox(clearKey..clearKey2) --
		if (((string.find(string.lower(msg), clearKey) ~= nil) and (string.len(msg) == 6)) or ((string.find(string.lower(msg), clearKey2) ~= nil)and (string.len(msg) == 2))) then 
			--filters out any other text or letters so the /c and /clear can be clearly read
		--		Dialog_MsgBox("/c or /clear found")

	
			-- Loop through all of the messages in our chat history
			for i = 1, g_chatbuffersize do
		 		--g_chatlines[ i ] = "" 
					table.insert( g_chatlines, " ")
					table.remove( g_chatlines, 0)

					table.insert( g_chattypes, ChatType.Talk)
					table.remove( g_chattypes, 0)
		
			end
		--Dialog_MsgBox("You found me")
		
		genDisplayLines()
		renderText()
		--regenChatBuf("")
		EditBox_ClearText( g_edtSendBar )
		msg = ""
		return
		end
	end
--------------------------------------------------------------------------

	-- send chat message
	
	if g_game ~= nil and msg ~= "" then
		table.insert(g_cmdBuffer, 1, msg)
		-- see if replying look for /r or /reply
		local replyLen = 0
		local replyStart = nil           
		replyStart, replyLen = string.find( msg, "/reply ." )
		if replyStart == nil then 
			replyStart, replyLen = string.find( msg, "/r ." )
		end 
		
		if replyLen ~= nil and replyStart == 1 then
			if g_replyTo ~= nil then 
				msg = "/tell "..g_replyTo..string.sub( msg, replyLen - 1 )
				g_chat_type = ctPrivate
			else
				e = Dispatcher_MakeEvent( g_dispatcher, "RenderTextEvent" )
				Event_EncodeString( e, "No one to reply to." )
				Event_EncodeNumber( e, ctSystem  )		
				Dispatcher_QueueEvent( g_dispatcher, e )
				return 
			end 
		end
		ClientEngine_SendChatMessage( g_game, g_chat_type, msg )
	end
	if msg == "" then
		Dialog_SetModal(gDialogHandle, false)
		if g_nop ~= nil then
			Dialog_FocusDefaultControl( g_nop )
		end
		EditBox_ClearText( g_edtSendBar )
		Dialog_SendToBack(gDialogHandle)
		ih = Image_GetControl(g_imgMouse)
		Control_SetEnabled(ih, false)
		Control_SetVisible(ih, false)
		g_typing = false
		g_ignoreEnter = true
		--enterd = Dispatcher_MakeEvent( g_dispatcher, ENTER_DELAY_EVENT )
		--Dispatcher_QueueEventInFuture( g_dispatcher, enterd, 1 ) 
		local enterdelay = Dispatcher_MakeEvent( g_dispatcher, ENTER_DELAY_EVENT )	
		Dispatcher_QueueEventInFuture( g_dispatcher, enterdelay, 2 )
		--Dialog_MsgBox("you entered a blank string")
		ClientEngine_ClearChatFocus(g_game)
	end

	EditBox_ClearText( editBoxHandle ) 


	
end

---------------------------------------------------------------------
-- Mouse L Button Down
---------------------------------------------------------------------

function imgResize_OnLButtonDown(dialogHandle, x, y)
	g_x_resizeoffset = x - Control_GetLocationX(Image_GetControl(g_resizeBar))
	g_y_resizeoffset = y - Control_GetLocationY(Image_GetControl(g_resizeBar))
	g_sizing = true
	g_typing = false
	--Dialog_MsgBox("Here I am")
	ListBox_RemoveAllItems(Dialog_GetListBox(gDialogHandle, "lstChatHist"))	
end

function imgMouseExploit_OnRButtonDown( dialogHandle, x, y )
	exploitMouseDown( dialogHandle, x, y)
end

function imgMouseExploit_OnLButtonDown( dialogHandle, x, y )
	exploitMouseDown( dialogHandle, x, y)
end

function exploitMouseDown( dialogHandle, x, y )
	local ec = EditBox_GetControl(g_edtSendBar)
	local eh = g_edtSendBar
	xmin = Control_GetLocationX(ec)
	ymin = Control_GetLocationY(ec)
	xmax = xmin + Control_GetWidth(ec)
	ymax = ymin + Control_GetHeight(ec)
	if x < xmin or x > xmax or y < ymin or y > ymax then
		if g_nop ~= nil then
			Dialog_FocusDefaultControl( g_nop)
		end
		Dialog_SendToBack(gDialogHandle)
		Dialog_SetModal(gDialogHandle, false)
		ih = Image_GetControl(g_imgMouse)
		Control_SetEnabled(ih, false)
		Control_SetVisible(ih, false)
	end
	g_typing = false
	Control_SetLocation( g_imgMouse, -100, -100 )		
end
---------------------------------------------------------------------
-- Mouse moved hack to catch mouse down events.
---------------------------------------------------------------------
function Dialog_OnMouseMove( dialogHandle, x, y )
	-- We can only be doing one at a time.
	if g_typing then
		Control_SetLocation( g_imgMouse, x - 16, y - 16 )
	end
	if g_sizing then
		if Dialog_IsLMouseDown(dialogHandle) ~= 0 then
			if x + 7 < Dialog_GetScreenWidth(dialogHandle) and y + 22 < Dialog_GetScreenHeight(dialogHandle) then
				local hsize = x - g_x_resizeoffset - 7
				local vsize = y - g_y_resizeoffset - 62
				g_chatscrollpos = 0
				if hsize > 250 then
					g_h_size = hsize
				else
					g_h_size = 250
				end
				if vsize > 75 then
					g_v_size = vsize
				else
					g_v_size = 75
				end
				setChatPosition()
			end
		else
			g_sizing = false
			setChatPosition()
			genDisplayLines()
			renderText()
		end
	end
	if Control_ContainsPoint ~= nil then

		local mytemptex = Element_GetTextureColor(Image_GetDisplayElement(g_mainframe))
		local myTextEntryImage = Element_GetTextureColor(Image_GetDisplayElement(Dialog_GetImage(gDialogHandle, "imgBackDrop")))
   		local myTextHistoryImage = Element_GetTextureColor(EditBox_GetTextAreaDisplayElement(Dialog_GetEditBox(gDialogHandle, "edtSendBar")))
   		local myTransBack = Element_GetTextureColor(Image_GetDisplayElement(Dialog_GetImage(gDialogHandle, "imgTransBack")))

		if Control_ContainsPoint(g_mainframe, x, y) ~= 0 then
			local alpha = string.format("%x", g_chatcurrentframetrans)
			    for i=1, 8-string.len(tostring(alpha)) do
        			alpha = "0" .. alpha
    			end
			alpha = string.sub(alpha, 1, 2)
			local alphaecho = tonumber(alpha, 16)
			if alphaecho ~= 255 then
				alpha = tonumber(alpha, 16)
   				alpha = alpha + math.floor(255 * .05)
   				if alpha >= 255 then
   				   alpha = 255
   				end
   				local percent = tonumber(string.format("%x", alpha) .. string.sub(g_chatcurrentframetrans, 3, 8), 16)
				BlendColor_SetColor( mytemptex, DXUT_STATE_NORMAL, percent )
   				BlendColor_SetColor( myTextEntryImage, DXUT_STATE_NORMAL, percent )
   				BlendColor_SetColor( myTextHistoryImage, DXUT_STATE_NORMAL, percent )
   				BlendColor_SetColor( myTransBack, DXUT_STATE_NORMAL, percent )

   			else
   				--Dialog_MsgBox("Here")
				BlendColor_SetColor( mytemptex, DXUT_STATE_NORMAL, 4278190080 )
   				BlendColor_SetColor( myTextEntryImage, DXUT_STATE_NORMAL, 4278190080 )
   				BlendColor_SetColor( myTextHistoryImage, DXUT_STATE_NORMAL, 4278190080 )
   				BlendColor_SetColor( myTransBack, DXUT_STATE_NORMAL, 4278190080 )
			end
   		else
   			BlendColor_SetColor( mytemptex, DXUT_STATE_NORMAL, g_chatcurrentframetrans )
   			BlendColor_SetColor( myTextEntryImage, DXUT_STATE_NORMAL, g_chatcurrentframetrans )
   			BlendColor_SetColor( myTextHistoryImage, DXUT_STATE_NORMAL, g_chatcurrentframetrans )
   			BlendColor_SetColor( myTransBack, DXUT_STATE_NORMAL, g_chatcurrentframetrans )
		end
	end
end

---------------------------------------------------------------------
--
-- Keyboard Handler
--
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
	-- ESC key
	if key == Keyboard.ESC then
		EditBox_ClearText( g_edtSendBar )
		if g_nop ~= nil then
			Dialog_FocusDefaultControl( g_nop )
		end
		Dialog_SendToBack(gDialogHandle)
		Dialog_SetModal(gDialogHandle, false)
		ih = Image_GetControl(g_imgMouse)
		Control_SetEnabled(ih, false)
		Control_SetVisible(ih, false)
	end
	  
	-- Page down - Move up in chat history
	if key == Keyboard.PAGE_UP then
	
		if g_cmdBuffer[g_bufferPos + 1] ~= nil then
			
			g_bufferPos = g_bufferPos + 1
			ebh = Dialog_GetEditBox(gDialogHandle, "edtSendBar")
			EditBox_SetText(ebh, g_cmdBuffer[g_bufferPos], false)

		end	
				
	end

	-- Page down - Move down in chat history
	if key == Keyboard.PAGE_DOWN then
		
		ebh = Dialog_GetEditBox(gDialogHandle, "edtSendBar")		
		EditBox_SetText(ebh, "", false)	
		
		if (g_bufferPos - 1) < 1 then
			g_bufferPos = 0		
		elseif g_cmdBuffer[g_bufferPos - 1] ~= nil then
			g_bufferPos = g_bufferPos - 1
			EditBox_SetText(ebh, g_cmdBuffer[g_bufferPos], false)
		end
					
	end
	
	--Dialog_MsgBox(key)
end

function edtSendBar_OnEditBoxKeyDown(dialogHandle, key, bShiftDown, bControlDown, bAltDown)



	-- Page down - Move up in chat history
	if key == Keyboard.PAGE_UP then

		if g_cmdBuffer[g_bufferPos + 1] ~= nil then

			g_bufferPos = g_bufferPos + 1
			ebh = Dialog_GetEditBox(gDialogHandle, "edtSendBar")
			EditBox_SetText(ebh, g_cmdBuffer[g_bufferPos], false)

		end

	end

	-- Page down - Move down in chat history
	if key == Keyboard.PAGE_DOWN then

		ebh = Dialog_GetEditBox(gDialogHandle, "edtSendBar")
		EditBox_SetText(ebh, "", false)

		if (g_bufferPos - 1) < 1 then
			g_bufferPos = 0
		elseif g_cmdBuffer[g_bufferPos - 1] ~= nil then
			g_bufferPos = g_bufferPos - 1
			EditBox_SetText(ebh, g_cmdBuffer[g_bufferPos], false)
		end

	end
	
	-- Space Bar
	if key == Keyboard.SPACEBAR then
		local ebh = Dialog_GetEditBox(gDialogHandle, "edtSendBar")
	    local msg = EditBox_GetText(ebh)

  		if msg ~= nil then

			-- see if replying look for /r or /reply
			local replyEnd = nil
			local replyStart = nil
			local command = ""
			local isReply = false
			replyStart, replyEnd, command = string.find( msg, "/(.+)", 1)
			if command then
				command = string.lower(command)

				if command == "reply" or command == "r" then
					isReply = true
				end

				if replyStart == 1 and isReply then
					if g_replyTo ~= nil then
	          			msg = "/tell "..g_replyTo..string.sub( msg, replyEnd + 1, -1 )
						EditBox_SetText(ebh, msg, false)
					end
				end
			end

		end


	end



end

function setChatPosition()
	local tempx
	local tempy
	local tempheight
	local tempwidth
	-- Keep size from being bigger than the whole screen
	if g_v_size > Dialog_GetScreenHeight(gDialogHandle) - 100 then
		g_v_size = Dialog_GetScreenHeight(gDialogHandle) - 100
	end
	if g_h_size > Dialog_GetScreenWidth(gDialogHandle) - 30 then
		g_h_size = Dialog_GetScreenWidth(gDialogHandle) - 30
	end
	
	-- ChatBar EditBox
	tempx = Control_GetLocationX(EditBox_GetControl(g_edtSendBar))
	tempheight = Control_GetHeight(EditBox_GetControl(g_edtSendBar))
	Control_SetLocation(EditBox_GetControl(g_edtSendBar), tempx, g_v_size + 20)
	Control_SetSize(EditBox_GetControl(g_edtSendBar), g_h_size + 5, tempheight)

	-- ChatHist Area
	local chatlst = Dialog_GetListBox(gDialogHandle, "lstChatHist")
	Control_SetSize(Static_GetControl(g_chattext), g_h_size - 55, g_v_size)
	Control_SetSize(ListBox_GetControl(chatlst), g_h_size + 33, g_v_size + 7)
	Control_SetSize(Image_GetControl(g_backdrop), g_h_size + 5, g_v_size + 7)		
	
	-- OuterFrame
	Control_SetSize(Image_GetControl(g_mainframe), g_h_size + 40, g_v_size + 90)
	Dialog_SetSize(gDialogHandle, g_h_size + 40, g_v_size + 90)
	
	-- Resize and minimize controls
	Control_SetLocation(Image_GetControl(g_resizeBar), g_h_size +15, g_v_size + 51)

	-- Evils little white lines

	-- H1 is the top of the editbox
	tempx = Control_GetLocationX(EditBox_GetControl(g_edtSendBar))
	tempy = Control_GetLocationY(EditBox_GetControl(g_edtSendBar)) - 1
	tempwidth = Control_GetWidth(EditBox_GetControl(g_edtSendBar))
	Control_SetSize(Image_GetControl(g_line1), tempwidth, 1)
	Control_SetLocation(Image_GetControl(g_line1), tempx, tempy)
	-- H4 is at the bottom of the editbox
	tempy = tempy + Control_GetHeight(EditBox_GetControl(g_edtSendBar))
	Control_SetSize(Image_GetControl(g_line4), tempwidth, 1)
	Control_SetLocation(Image_GetControl(g_line4), tempx, tempy)
	-- V2 is the left of the editbox
	tempheight = Control_GetHeight(EditBox_GetControl(g_edtSendBar))
	tempy = Control_GetLocationY(EditBox_GetControl(g_edtSendBar))
	Control_SetSize(Image_GetControl(g_line6), 1, tempheight)
	Control_SetLocation(Image_GetControl(g_line6), tempx, tempy)
	-- V3 is the right of the editbox
	tempx = tempx + Control_GetWidth(EditBox_GetControl(g_edtSendBar))
	Control_SetSize(Image_GetControl(g_line7), 1, tempheight)
	Control_SetLocation(Image_GetControl(g_line7), tempx, tempy)

	-- H3 is top of history
	tempx = Control_GetLocationX(Image_GetControl(g_backdrop))
	tempy = Control_GetLocationY(Image_GetControl(g_backdrop))
	tempwidth = Control_GetWidth(Image_GetControl(g_backdrop))
	Control_SetSize(Image_GetControl(g_line3), tempwidth, 1)
	Control_SetLocation(Image_GetControl(g_line3), tempx, tempy)
	-- H2 is the bottom of the history
	tempy = tempy + Control_GetHeight(Image_GetControl(g_backdrop))
	Control_SetSize(Image_GetControl(g_line2), tempwidth, 1)
	Control_SetLocation(Image_GetControl(g_line2), tempx, tempy)
	-- V4 is the left of the history
	tempy = Control_GetLocationY(Image_GetControl(g_backdrop))
	tempheight = Control_GetHeight(Image_GetControl(g_backdrop))
	Control_SetSize(Image_GetControl(g_line8), 1, tempheight)
	Control_SetLocation(Image_GetControl(g_line8), tempx, tempy)

	--V5 is the right of the history
	tempx = tempx + Control_GetWidth(Image_GetControl(g_backdrop))
	Control_SetSize(Image_GetControl(g_line5), 1, tempheight)
	Control_SetLocation(Image_GetControl(g_line5), tempx, tempy)
	
	
	
	if g_chathistopen == 1 then
		for i=1, 25 do
			local mycon = Image_GetControl(Dialog_GetImage(gDialogHandle, "imgAction" .. i))
			if Control_GetVisible(mycon) == true then
				Control_SetLocation(mycon, g_actionXs[i], g_actionYs[i])
			end
		end
		Control_SetLocation( Image_GetControl( g_chathistbtn ), g_h_size - 3, -14)
 		Control_SetLocation( Image_GetControl( g_inctransback ), g_h_size - 22, -14)
  		Control_SetLocation( Image_GetControl( g_dectransback ), g_h_size - 40, -14)
		
		
		doUnMinimize()
	else
		Control_SetLocation( Image_GetControl( g_chathistbtn ), 74, 0)
		Control_SetVisible( Image_GetControl( g_chathistbtn ), true ) 	
	end
	
	local mytemptex = Element_GetTextureColor(Image_GetDisplayElement(g_mainframe))
	local myTextEntryImage = Element_GetTextureColor(Image_GetDisplayElement(Dialog_GetImage(gDialogHandle, "imgBackDrop")))
   	local myTextHistoryImage = Element_GetTextureColor(EditBox_GetTextAreaDisplayElement(Dialog_GetEditBox(gDialogHandle, "edtSendBar")))
   	--Dialog_MsgBox( g_chattranslevel )
   	g_chatcurrentframetrans = g_translevels[g_chattranslevel]
   	BlendColor_SetColor( mytemptex, DXUT_STATE_NORMAL, g_chatcurrentframetrans )
   	--Dialog_MsgBox( g_chatcurrentframetrans )
   	BlendColor_SetColor( myTextEntryImage, DXUT_STATE_NORMAL, g_chatcurrentframetrans )
   	BlendColor_SetColor( myTextHistoryImage, DXUT_STATE_NORMAL, g_chatcurrentframetrans )
	--Dialog_MsgBox("Finished	setChatPosition() g_chatcurrentframetrans= " .. g_chatcurrentframetrans .. "\ng_chattranslevel= " .. g_chattranslevel)	
end

function btnMinimize_OnButtonClicked()

	if g_chathistopen == 1 then
		g_chathistopen = 0
		doMinimize()
	else
		g_chathistopen = 1
		doUnMinimize()
		setChatPosition()
	end

	if Dialog_ClearFocus then
		Dialog_ClearFocus(gDialogHandle)
	end
	
end

function ActionButtons( whichButton )
	local msgtxt = g_displaylines[whichbutton]
	if Dialog_IsDialogOpenByName("messagecenter.xml") == 0 then
		
		gotoMenu( "messagecenter.xml", 0, 1 )
	end
end

function imgAction1_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(1)
end
function imgAction2_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(2)
end
function imgAction3_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(3)
end
function imgAction4_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(4)
end
function imgAction5_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(5)
end
function imgAction6_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(6)
end
function imgAction7_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(7)
end
function imgAction8_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(8)
end
function imgAction9_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(9)
end
function imgAction10_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(10)
end
function imgAction11_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(11)
end
function imgAction12_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(12)
end
function imgAction13_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(13)
end
function imgAction14_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(14)
end
function imgAction15_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(15)
end
function imgAction16_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(16)
end
function imgAction17_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(17)
end
function imgAction18_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(18)
end
function imgAction19_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(19)
end
function imgAction20_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(20)
end
function imgAction21_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(21)
end
function imgAction22_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(22)
end
function imgAction23_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(23)
end
function imgAction24_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(24)
end
function imgAction25_OnLButtonDown( dialogHandle, x, y)
	ActionButtons(25)
end

function ClearActionIcons()
	for i=1, 25 do
		local mycon = Image_GetControl(Dialog_GetImage(gDialogHandle, "imgAction" .. i))
		Control_SetVisible(mycon, false)
		Control_SetEnabled(mycon, false)
	end
end

function Dialog_OnMoved()
	if Dialog_GetLocationX(gDialogHandle) > Dialog_GetScreenWidth(gDialogHandle) - g_h_size then
		Dialog_SetLocation(gDialogHandle, Dialog_GetScreenWidth(gDialogHandle) - g_h_size - 1, Dialog_GetLocationY(gDialogHandle))
	end
	if Dialog_GetLocationY(gDialogHandle) > Dialog_GetScreenHeight(gDialogHandle) - g_v_size then
		Dialog_SetLocation(gDialogHandle, Dialog_GetLocationX(gDialogHandle), Dialog_GetScreenHeight(gDialogHandle) - g_v_size - 1)
	end
end

function btnIncTrans_OnButtonClicked( buttonHandle )
	
	local mytemptex = Element_GetTextureColor(Image_GetDisplayElement(g_mainframe))
	local myTextEntryImage = Element_GetTextureColor(Image_GetDisplayElement(Dialog_GetImage(gDialogHandle, "imgBackDrop")))
   	local myTextHistoryImage = Element_GetTextureColor(EditBox_GetTextAreaDisplayElement(Dialog_GetEditBox(gDialogHandle, "edtSendBar")))
   	g_chatcurrentframetrans = chat_ModifyTransparency(1)
   	BlendColor_SetColor( mytemptex, DXUT_STATE_NORMAL, g_chatcurrentframetrans )
   	BlendColor_SetColor( myTextEntryImage, DXUT_STATE_NORMAL, g_chatcurrentframetrans )
   	BlendColor_SetColor( myTextHistoryImage, DXUT_STATE_NORMAL, g_chatcurrentframetrans )
	if Dialog_ClearFocus then
		Dialog_ClearFocus(gDialogHandle)
	end   	

end

function btnDecTrans_OnButtonClicked( buttonHandle )

	local mytemptex = Element_GetTextureColor(Image_GetDisplayElement(g_mainframe))
	g_chatcurrentframetrans = chat_ModifyTransparency(-1)
   	local myTextEntryImage = Element_GetTextureColor(Image_GetDisplayElement(Dialog_GetImage(gDialogHandle, "imgBackDrop")))
   	local myTextHistoryImage = Element_GetTextureColor(EditBox_GetTextAreaDisplayElement(Dialog_GetEditBox(gDialogHandle, "edtSendBar")))
   	
   	BlendColor_SetColor( mytemptex, DXUT_STATE_NORMAL, g_chatcurrentframetrans )
   	BlendColor_SetColor( myTextEntryImage, DXUT_STATE_NORMAL, g_chatcurrentframetrans )
   	BlendColor_SetColor( myTextHistoryImage, DXUT_STATE_NORMAL, g_chatcurrentframetrans )
   	
	if Dialog_ClearFocus then
		Dialog_ClearFocus(gDialogHandle)
	end
end

---------------------------------------------------------------------
--
-- back_ModifyTransparency takes in a number representing whether to
-- increase or decrease the alpha value of the number before it is
-- returned.
--
---------------------------------------------------------------------
function chat_ModifyTransparency(modify)
  
    
	if modify == 1 then
	    if g_chattranslevel == 0 then
	    	g_chattranslevel = g_chattranslevel + 1
			return g_translevels[1]
	
		elseif g_chattranslevel == 1 then
			g_chattranslevel = g_chattranslevel + 1
			return g_translevels[2]
		
		elseif g_chattranslevel == 2 then
			g_chattranslevel = g_chattranslevel + 1
			return g_translevels[3]
	
		elseif g_chattranslevel == 3 then
			g_chattranslevel = g_chattranslevel + 1
			return g_translevels[4]
	
		elseif g_chattranslevel == 4 then
			g_chattranslevel = g_chattranslevel + 1
			return g_translevels[5]
	
		elseif g_chattranslevel == 5 then
			g_chattranslevel = g_chattranslevel + 1
			return g_translevels[6]
		
		elseif g_chattranslevel == 6 then
			g_chattranslevel = g_chattranslevel + 1
			return g_translevels[7]
		
		elseif g_chattranslevel == 7 then
			g_chattranslevel = g_chattranslevel + 1
			return g_translevels[8]
		
		elseif g_chattranslevel == 8 then
			g_chattranslevel = g_chattranslevel + 1
			return g_translevels[9]
		
		elseif g_chattranslevel == 9 then
			g_chattranslevel = g_chattranslevel + 1
			return g_translevels[10]
	
		elseif g_chattranslevel == 10 then
			g_chattranslevel = 0
			return g_translevels[0]
	
	    end
	else
		if g_chattranslevel == 0 then
			g_chattranslevel = 10
			return g_translevels[10]
	
		elseif g_chattranslevel == 1 then
			g_chattranslevel = g_chattranslevel - 1
			return g_translevels[0]
		
		elseif g_chattranslevel == 2 then
			g_chattranslevel = g_chattranslevel - 1
			return g_translevels[1]
	
		elseif g_chattranslevel == 3 then
			g_chattranslevel = g_chattranslevel - 1
			return g_translevels[2]
	
		elseif g_chattranslevel == 4 then
			g_chattranslevel = g_chattranslevel - 1
			return g_translevels[3]
	
		elseif g_chattranslevel == 5 then
			g_chattranslevel = g_chattranslevel - 1
			return g_translevels[4]
		
		elseif g_chattranslevel == 6 then
			g_chattranslevel = g_chattranslevel - 1
			return g_translevels[5]
		
		elseif g_chattranslevel == 7 then
			g_chattranslevel = g_chattranslevel - 1
			return g_translevels[6]
		
		elseif g_chattranslevel == 8 then
			g_chattranslevel = g_chattranslevel - 1
			return g_translevels[7]
		
		elseif g_chattranslevel == 9 then
			g_chattranslevel = g_chattranslevel - 1
			return g_translevels[8]
	
		elseif g_chattranslevel == 10 then
			g_chattranslevel = g_chattranslevel - 1
			return g_translevels[9]
	    end
	end
	
end
