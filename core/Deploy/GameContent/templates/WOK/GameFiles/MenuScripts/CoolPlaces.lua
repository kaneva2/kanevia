dofile("MenuHelper.lua")
dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\CMD.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\Progressive.lua")
dofile("..\\Scripts\\MovementIds.lua")
dofile("PageButtons.lua")
dofile("MenuLocation.lua")
dofile("CommonFunctions.lua")
dofile("MenuStyles.lua")
dofile("WebSuffix.lua")
dofile("Help.lua")
-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--
-- end 

--Menu tabs
TAB_PUBLIC = 0
TAB_HANG_OUTS = 1
TAB_HOMES = 2

--Location Types
LOC_APARTMENT = 3
LOC_KANEVA = 4
LOC_HANGOUT = 6

--Order Types
ORDER_NAME = "&orderBy=Name"
ORDER_TYPE = "&orderBy=Type"
ORDER_POPULATION = "&orderBy=Population"
ORDER_RAVES = "&orderBy=Raves"
ORDER_OWNER = "&orderBy=Owner"

ORDER_DIR_DESC = "&orderByDirection=DESC"
ORDER_DIR_ASC = "&orderByDirection=ASC"

PARAM_SEARCH = "&search="

YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"
PRIVATE_MESSAGE_EVENT = "PrivateMessageEvent"
BEGIN_DOWNLOAD_EVENT = "BeginZoneDownloadEvent"
WEB_ADDRESS = GameGlobals.WEB_SITE_PREFIX .. TRAVEL_LIST_SUFFIX
g_web_ext = "type="..LOC_KANEVA
g_sort = ""
g_search = ""


--Toggle between on and off for the help.
g_helpbutton = false;

--Toggle between sort ascend and descend
g_nameSortDescend = true
g_nameSortDirection = ORDER_DIR_DESC
g_typeSortDescend = true
g_typeSortDirection = ORDER_DIR_DESC
g_ravesSortDescend = false
g_ravesSortDirection = ORDER_DIR_ASC
g_populationSortDescend = true
g_populationSortDirection = ORDER_DIR_DESC

--decide whether or not to use search text for queries
g_useSearch = false

g_game = nil
g_dispatcher = nil

--current url we are using
g_web_address = WEB_ADDRESS.. g_web_ext

-- Hold current friend data returned from server
g_placeData = ""
-- Total number of friend records
g_totalNumRec = 0
-- Maximum number of places for server to return in one request
-- places per page
g_placesPerPage = 8

g_selectedIndex = -1
g_t_places = {} 

-- Button handle storage
g_t_btnHandles = {}

--current menu tab we are on
g_current_tab = TAB_PUBLIC

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )		
	ParentHandler_RegisterEventHandler( dispatcher, handler, "BrowserPageReadyHandler", 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "textHandler", 1,0,0,0, "RenderTextEvent", KEP.HIGH_PRIO )	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "answerEventHandler", 1,0,0,0, YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
	g_dispatcher = dispatcher
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, BEGIN_DOWNLOAD_EVENT, KEP.MED_PRIO )
	ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )	
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	
	-- TODO init here
	
	g_game = Dispatcher_GetGame(KEP.dispatcher)
	
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	
	if KEPConfig_GetNumber == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end
	
	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end
	
	bh = Dialog_GetButton(gDialogHandle, "btnKaneva")
    setButtonState(bh, false)

	bh = Dialog_GetButton(gDialogHandle, "btnApartments")
    setButtonState(bh, true)
    
    bh = Dialog_GetButton(gDialogHandle, "btnBroadband")
    setButtonState(bh, true)
    
    loadLocation(g_game, gDialogHandle, "Travel")
    
	initButtons()
    
    getButtonHandles()
    
    setSearchState(false)

    local sh = Dialog_GetStatic(dialogHandle, "lblStatus")
    if sh ~= nil then
		Static_SetText(sh, "Mouse Over a Button for Information")
	end
	
	-- setSelectionColor is called from PageButtons.lua to highlight the first
	-- button when the menu is opend.
	local bh = Dialog_GetButton(dialogHandle, "btnRange1")
	if bh ~= nil then
		setSelectionColor( bh )
	end
	
	setListBoxStyle(Dialog_GetListBox(dialogHandle, "lstName"))
	setListBoxStyle(Dialog_GetListBox(dialogHandle, "lstType"))
    setListBoxStyle(Dialog_GetListBox(dialogHandle, "lstPopulation"))
    setListBoxStyle(Dialog_GetListBox(dialogHandle, "lstRaves"))
	HideHelp();
end



---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
    saveLocation(g_game, dialogHandle, "Travel")
    
	Helper_Dialog_OnDestroy( dialogHandle )
end

function textHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

	local msg   = Event_DecodeString( event )
	local type  = Event_DecodeNumber( event )

	s, e = string.find(msg, "Travel")

	if not(s == 1 and type == ChatType.System) then
		local bh = Dialog_GetButton(gDialogHandle, "btnGoThere")
		setButtonState(bh, true)
	end

	return KEP.EPR_OK

end


-- Catch the xml from the server
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter == WF.COOL_PLACES then
	
		g_placeData = Event_DecodeString( event )
 		--local sh = Dialog_GetStatic( gDialogHandle, "lblQuery" )
		--Static_SetText(sh, g_placeData)
		
--Dialog_MsgBox(g_placeData)
		
	    ParsePlaceData(gDialogHandle)
		return KEP.EPR_CONSUMED
		
	end
	
end

function answerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
   	local answerNumber = Event_DecodeNumber( event )
	if answerNumber == 0 then
	    local bh = Dialog_GetButton(gDialogHandle, "btnGoThere")
    	setButtonState(bh, true)
	end
end


-- Parse friend data from server
function ParsePlaceData(dialogHandle)

	local s = 0     			-- start of substring
	local numRec = 0   		-- number of records in this chunk
	local strPos = 0    		-- current position in string to parse
	local result = ""  		-- return string from find temp

	local name = "" 			-- name of the zone
	local zone_index = 0    	-- index of the zone
	local zone_instance_id = 0	-- instance of the zone
	local server_name = "" 		-- name of the server
	local type = "" 			-- the type of the zone
	local count = 0	  		-- number of players in the zone
	local raves = "0"       -- number of raves
	local parseStr = ""
	local username = ""     -- Owner of space
	
	local sh = Dialog_GetStatic( gDialogHandle, "lblQuery" )
	s, e, result = string.find(g_placeData, "<ReturnCode>([0-9]+)</ReturnCode>")
	
	if result == "0" then
	
		numRec = calculateRanges()

		local lbh = Dialog_GetListBox(gDialogHandle, "lstName")
		local lbhOn = Dialog_GetListBox(gDialogHandle, "lstPopulation")
		local lbhT = Dialog_GetListBox(gDialogHandle, "lstType")
		local lbhR = Dialog_GetListBox(gDialogHandle, "lstRaves")
		
		if (lbh ~= nil) and (lbhOn ~= nil) and (lbhT ~= nil) and (lbhR ~= nil) then
			g_t_places = {}
			ListBox_RemoveAllItems(lbh)
			ListBox_RemoveAllItems(lbhOn)
   			ListBox_RemoveAllItems(lbhT)
   			ListBox_RemoveAllItems(lbhR)
			-- g_curPage = 1
			-- g_set = 1 
			
			for i=1,numRec do
				local zonestr = ""
				local trash = 0
				local s = 0
				s, strPos, zonestr = string.find(g_placeData, "<Zone>(.-)</Zone>", strPos)
				-- Dialog_MsgBox(zonestr)
				if g_current_tab == TAB_HANG_OUTS then
					s, trash, username = string.find(zonestr, "<username>(.-)</username>", 1)
				end
				s, trash, name = string.find(zonestr, "<name>(.-)</name>", 1)
				s, trash, raves = string.find(zonestr, "<wok_raves>(.-)</wok_raves>", 1)
				s, trash, zone_index = string.find(zonestr, "<zone_index>([0-9]+)</zone_index>", 1)
				s, trash, zone_instance_id = string.find(zonestr, "<zone_instance_id>([0-9]+)</zone_instance_id>", 1)
  				-- s, strPos, server_name = string.find(zonestr, "<server_name>(%w+)</server_name>", 1)
				s, trash, type = string.find(zonestr, "<display_name>(.-)</display_name>", 1)  				
				s, trash, count = string.find(zonestr, "<count>([0-9]+)</count>", 1)

				local place = {}
				place["name"] = name
				place["zone_index"] = zone_index
				place["zone_instance_id"] = zone_instance_id
				-- place["server_name"] = server_name
				place["type"] = type
				place["count"] = count

				table.insert(g_t_places, place)				
				ListBox_AddItem(lbh, name, zone_index)
				--ListBox_AddItem(zonestr, name, zone_index)
                ListBox_AddItem(lbhOn, count, zone_index)
                if g_current_tab == TAB_HANG_OUTS then
    				ListBox_AddItem(lbhT, username, zone_index)
                else
    				ListBox_AddItem(lbhT, type, zone_index)
    			end
    			
				if g_current_tab == TAB_PUBLIC then
					raves = "N/A"
				end 

    			ListBox_AddItem(lbhR, raves, zone_index)

			end
			
			alignButtons()

		end
 	else
 		--s, e, parseStr = string.find(g_placeData, "<ReturnDescription>(%w+)</ReturnDescription>")
		--if sh ~= nil then
		--	Static_SetText(sh, " "..parseStr)
        	--end
 	end
 	
end


-- Used to fill the buttons with the correct numerical text
function calculateRanges()

	local numRec = 0  --number of records returned
	local s, e  -- start and end indices of found string
	local resultsstr = "Showing " -- text for result label
	local resultslbl = Dialog_GetStatic( gDialogHandle, "lblResults")

	local high = g_curPage * g_placesPerPage
	local low = ((g_curPage - 1) * g_placesPerPage) + 1

	s, e, g_totalNumRec = string.find(g_placeData, "<TotalNumberRecords>([0-9]+)</TotalNumberRecords>")
	s, e, numRec = string.find(g_placeData, "<NumberRecords>([0-9]+)</NumberRecords>")
	g_totalNumRec = tonumber(g_totalNumRec)
	g_pageCap = math.ceil(g_totalNumRec / g_placesPerPage)
	g_setCap = math.ceil(g_pageCap / g_window)

    	if high > g_totalNumRec then
		high = g_totalNumRec
	end

	if numRec == "0" then
		low = numRec
	end

	if resultslbl ~= nil then
		resultsstr = resultsstr..low.." - "..high.." of "..g_totalNumRec
		Static_SetText(resultslbl, resultsstr)
	end
	
	if g_InitBtnText then
	    setButtonText()
	    g_InitBtnText = false
	end
	
	return numRec
	
end

---------------------------------------------------------------------
-- List Box selection
---------------------------------------------------------------------

function lstFriends_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	local lbh = Dialog_GetListBox(gDialogHandle, "lstOnline")	
	
	if lbh ~= nil then
		ListBox_SelectItem( lbh, sel_index )
	end


end	

function lstOnline_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	local lbh = Dialog_GetListBox(gDialogHandle, "lstFriends")	
	
	if lbh ~= nil then
		ListBox_SelectItem( lbh, sel_index )
	end


end	

---------------------------------------------------------------------
-- Button Handler
---------------------------------------------------------------------
function btnClose_OnButtonClicked( buttonHandle )
	DestroyMenu(gDialogHandle)
end


--Display all the help buttons.
function DisplayHelpButtons()
	Dialog_MsgBox("Display Help");
end

--Hide all the hlp buttons
function HideHelpButtons()
   Dialog_MsgBox("Hide Help")
end

--Clicking on the help button
function btnTravelHelp_OnButtonClicked(buttonHandle)
	g_helpbutton = not g_helpbutton
	if g_helpbutton then
	DisplayHelpButtons();
	else
	HideHelpButtons();
	end
end


function btnResultsPrev_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

	    calcResultsPrev()
		alignButtons()

		--g_web_address = WEB_ADDRESS.. g_web_ext .. g_sort

		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
		end
	end

	if g_selectedIndex > -1 then
		clearSelection()
	end

end


function btnResultsNext_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

        calcResultsNext()
		alignButtons()

		--g_web_address = WEB_ADDRESS.. g_web_ext .. g_sort

		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
		end

 	end

	if g_selectedIndex > -1 then
		clearSelection()
	end

end


function btnFirst_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

		calcFirst()
		alignButtons()

		--g_web_address = WEB_ADDRESS.. g_web_ext .. g_sort

		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
		end

	end
	if g_selectedIndex > -1 then
		clearSelection()
	end
	
end

function btnLast_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

		calcLast()
		alignButtons()

		--g_web_address = WEB_ADDRESS.. g_web_ext .. g_sort

		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
		end

	end

	if g_selectedIndex > -1 then
		clearSelection()
	end

end
   
   
function btnRange1_OnButtonClicked( buttonHandle )

	calcRange1()

	--g_web_address = WEB_ADDRESS.. g_web_ext .. g_sort

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end
	
	clearSelection()

end

function btnRange2_OnButtonClicked( buttonHandle )

	calcRange2()

	--g_web_address = WEB_ADDRESS.. g_web_ext .. g_sort

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end

	 clearSelection()

end

function btnRange3_OnButtonClicked( buttonHandle )

	calcRange3()

	--g_web_address = WEB_ADDRESS.. g_web_ext .. g_sort

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end

	clearSelection()

end

function btnRange4_OnButtonClicked( buttonHandle )

	calcRange4()

	--g_web_address = WEB_ADDRESS.. g_web_ext .. g_sort

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end

	clearSelection()

end



---------------------------------------------------------------------
-- Keyboard Handler
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
      -- ESC key
      if key == 27 then
            -- close this menu
            DestroyMenu(gDialogHandle)
      end
end

---------------------------------------------------------------------
-- Helpers
---------------------------------------------------------------------

function clearSelection()

	g_selectedIndex = -1
	
	lhN = Dialog_GetListBox(gDialogHandle, "lstName")
	lhT = Dialog_GetListBox(gDialogHandle, "lstType")	
	lhP = Dialog_GetListBox(gDialogHandle, "lstPopulation")
    lhR = Dialog_GetListBox(gDialogHandle, "lstRaves")

	if lhN ~= nil then
		ListBox_ClearSelection(lhN)
	end
	
	if lhT ~= nil then
		ListBox_ClearSelection(lhT)
	end

	if lhP ~= nil then
		ListBox_ClearSelection(lhP)
	end

   	if lhR ~= nil then
		ListBox_ClearSelection(lhR)
	end

end

function btnChat_OnButtonClicked( buttonHandle)

	if g_selectedIndex >= 0 then
		if g_game ~= nil then
			gotoMenu( "PrivateMessage.xml", 0, 0 )
		end
		local ev = Dispatcher_MakeEvent( KEP.dispatcher, PRIVATE_MESSAGE_EVENT )
		Event_EncodeNumber( ev, g_t_friends[g_selectedIndex + 1]["ID"])
		Event_EncodeString( ev, g_t_friends[g_selectedIndex + 1]["name"])
		Dispatcher_QueueEvent( KEP.dispatcher, ev )
	end
	
end
	
function lstFriends_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	g_selectedIndex = sel_index
end


function btnHome_OnButtonClicked(buttonHandle)

    --Dialog_MsgBox("Hello")

	if ClientEngine_GetPlayer == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	event = Dispatcher_MakeEvent( KEP.dispatcher, "TextEvent" )
	g_game = Dispatcher_GetGame(KEP.dispatcher)
	local player = ClientEngine_GetPlayer( g_game )
	local playerId = GetSet_GetNumber( player, MovementIds.NETWORKDEFINEDID )

	Event_EncodeNumber( event, playerId )
	Event_EncodeString( event, "/GOHOME")
	Event_EncodeNumber( event, ChatType.Talk )
	Event_SetFilter( event, CMD.GOHOME )
	Event_AddTo( event, KEP.SERVER_NETID )
	
	Dispatcher_QueueEvent( KEP.dispatcher, event )
end	

--Go to specific menu location.
function gotoMenu(menu_path, bClose, bModal)

	dh = Dialog_Create( menu_path );
	if dh == 0 then
		Dialog_MsgBox("Menu [" .. menu_path .. "] not found.")
	end

	if bModal and dh ~= 0 then
		Dialog_SetModal(dh, 1)
	end

	-- close this dialog
	if bClose == 1 then
		DestroyMenu(gDialogHandle)
	end

end

--Adding quick travel menu to script
function btnQuickTravel_OnButtonClicked(buttonHandle)

	gotoMenu( "QuickTravel.xml", 1, 1 )
end

function btnHelp_OnButtonClicked(buttonHandle)
	g_help=not g_help
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static1"), g_help )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static2"), g_help )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static3"), g_help )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static4"), g_help )
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image1"),g_help);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image2"),g_help);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image3"),g_help);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image4"),g_help);
end


function btnGoThere_OnButtonClicked(buttonHandle)
	if ClientEngine_GetPlayer == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	local lbh = Dialog_GetListBox(gDialogHandle, "lstName")
	local index = ListBox_GetSelectedItemIndex(lbh) + 1
	if index < 0 then
		return
	end
	local ev = Dispatcher_MakeEvent( g_dispatcher, KEP.GOTOPLACE_EVENT_NAME )
	Event_SetFilter( ev, KEP.GOTO_ZONE ) -- We are going to a zone
	Event_EncodeNumber( ev, g_t_places[index]["zone_index"] )
	Event_EncodeNumber( ev, g_t_places[index]["zone_instance_id"]  )
	Event_EncodeNumber( ev, -1 ) -- selected character is unknown now
	Event_AddTo( ev, KEP.SERVER_NETID )
	Dispatcher_QueueEvent( g_dispatcher, ev )
	if PROGRESSIVE == 1 then
		DestroyMenu(gDialogHandle)
	else	
		setButtonState(buttonHandle, false)
	end
end

function btnBroadband_OnButtonClicked(buttonHandle)   
	g_web_ext = "type=6"
	g_sort = ""
	
	g_current_tab = TAB_HANG_OUTS

    g_sortDescend = true
    g_useSearch = false

    resetPageButtonSelection()
    
    setSearchState(true)

	ebh = Dialog_GetEditBox(gDialogHandle, "editSearchField")
	if ebh ~= nil then
  		EditBox_ClearText( ebh )
	end

    setButtonState(buttonHandle, false)
	Static_SetText(Button_GetStatic(Dialog_GetButton(gDialogHandle, "btnSortName")), "Hang Out")
	Static_SetText(Button_GetStatic(Dialog_GetButton(gDialogHandle, "btnType")), "Owner")
	bh = Dialog_GetButton(gDialogHandle, "btnApartments")
    setButtonState(bh, true)

    bh = Dialog_GetButton(gDialogHandle, "btnKaneva")
    setButtonState(bh, true)

   	bh = Dialog_GetControl(gDialogHandle, "btnSortRaves")
	setButtonState(bh, true)

	if g_selectedIndex > -1 then
		clearSelection()
	end

	g_curPage = 1
	g_set = 1

    g_InitBtnText = true

    resetPageButtonSelection()

	g_web_address = WEB_ADDRESS..g_web_ext

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end
end

function btnApartments_OnButtonClicked(buttonHandle)

	g_web_ext = "type=3"
	g_sort = ""
	
	g_current_tab = TAB_HOMES
	
	g_sortDescend = true
	g_useSearch = false
	
	resetPageButtonSelection()
	
	setSearchState(true)

	ebh = Dialog_GetEditBox(gDialogHandle, "editSearchField")
	if ebh ~= nil then
  		EditBox_ClearText( ebh )
	end

    setButtonState(buttonHandle, false)
	Static_SetText(Button_GetStatic(Dialog_GetButton(gDialogHandle, "btnSortName")), "Home of")
	Static_SetText(Button_GetStatic(Dialog_GetButton(gDialogHandle, "btnType")), "Type")
	bh = Dialog_GetButton(gDialogHandle, "btnBroadband")
    setButtonState(bh, true)
    
   	bh = Dialog_GetControl(gDialogHandle, "btnSortRaves")
	setButtonState(bh, true)

    
    bh = Dialog_GetButton(gDialogHandle, "btnKaneva")
    setButtonState(bh, true)

	if g_selectedIndex > -1 then
		clearSelection()
	end

	g_curPage = 1
	g_set = 1

    g_InitBtnText = true

    resetPageButtonSelection()

	g_web_address = WEB_ADDRESS..g_web_ext

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end
end

function btnKaneva_OnButtonClicked(buttonHandle)

	g_web_ext = "type=4"
	g_sort = ""

	g_current_tab = TAB_PUBLIC

	g_sortDescend = true
	g_useSearch = false

    resetPageButtonSelection()
    
    setSearchState(false)

	ebh = Dialog_GetEditBox(gDialogHandle, "editSearchField")
	if ebh ~= nil then
  		EditBox_ClearText( ebh )
	end

	Static_SetText(Button_GetStatic(Dialog_GetButton(gDialogHandle, "btnSortName")), "Name")
	Static_SetText(Button_GetStatic(Dialog_GetButton(gDialogHandle, "btnType")), "Type")
    setButtonState(buttonHandle, false)

	bh = Dialog_GetControl(gDialogHandle, "btnSortRaves")
	setButtonState(bh, false)

	bh = Dialog_GetButton(gDialogHandle, "btnApartments")
    setButtonState(bh, true)
    
    bh = Dialog_GetButton(gDialogHandle, "btnBroadband")
    setButtonState(bh, true)

	if g_selectedIndex > -1 then
		clearSelection()
	end

	g_curPage = 1
	g_set = 1

    g_InitBtnText = true

    resetPageButtonSelection()
    
    g_web_address = WEB_ADDRESS..g_web_ext..g_sort

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end
end

-- state: boolean describing whether or not controls enabled
function setSearchState( state )

	local handle = Dialog_GetControl(gDialogHandle, "btnSearch")
	Control_SetEnabled( handle, state )
	Control_SetVisible( handle, state )
	
	handle = Dialog_GetControl(gDialogHandle, "editSearchField")
	Control_SetEnabled( handle, state )
	Control_SetVisible( handle, state )


--[[ Future feature

	handle = Dialog_GetControl(gDialogHandle, "btnFilters")
	Control_SetEnabled( handle, state )
	Control_SetVisible( handle, state )
]]--

end



function lstName_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	local list1 = Dialog_GetListBox(gDialogHandle, "lstPopulation")
	local list2 = Dialog_GetListBox(gDialogHandle, "lstType")
	local list3 = Dialog_GetListBox(gDialogHandle, "lstRaves")

	if ListBox_GetSelectedItemIndex(list1) ~= sel_index then
		ListBox_SelectItem(list1, sel_index)
	end
	if ListBox_GetSelectedItemIndex(list2) ~= sel_index then
		ListBox_SelectItem(list2, sel_index)
	end
	if ListBox_GetSelectedItemIndex(list3) ~= sel_index then
		ListBox_SelectItem(list3, sel_index)
	end

end

function lstPopulation_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	local list1 = Dialog_GetListBox(gDialogHandle, "lstName")
	local list2 = Dialog_GetListBox(gDialogHandle, "lstType")
	local list3 = Dialog_GetListBox(gDialogHandle, "lstRaves")

	if ListBox_GetSelectedItemIndex(list1) ~= sel_index then
		ListBox_SelectItem(list1, sel_index)
	end
	if ListBox_GetSelectedItemIndex(list2) ~= sel_index then
		ListBox_SelectItem(list2, sel_index)
	end
	if ListBox_GetSelectedItemIndex(list3) ~= sel_index then
		ListBox_SelectItem(list3, sel_index)
	end

end

function lstType_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	local list1 = Dialog_GetListBox(gDialogHandle, "lstName")
	local list2 = Dialog_GetListBox(gDialogHandle, "lstPopulation")
	local list3 = Dialog_GetListBox(gDialogHandle, "lstRaves")

	if ListBox_GetSelectedItemIndex(list1) ~= sel_index then
		ListBox_SelectItem(list1, sel_index)
	end
	if ListBox_GetSelectedItemIndex(list2) ~= sel_index then
		ListBox_SelectItem(list2, sel_index)
	end
	if ListBox_GetSelectedItemIndex(list3) ~= sel_index then
		ListBox_SelectItem(list3, sel_index)
	end

end


function lstRaves_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	local list1 = Dialog_GetListBox(gDialogHandle, "lstName")
	local list2 = Dialog_GetListBox(gDialogHandle, "lstPopulation")
	local list3 = Dialog_GetListBox(gDialogHandle, "lstType")
	
	if ListBox_GetSelectedItemIndex(list1) ~= sel_index then
		ListBox_SelectItem(list1, sel_index)
	end
	if ListBox_GetSelectedItemIndex(list2) ~= sel_index then
		ListBox_SelectItem(list2, sel_index)
	end
	if ListBox_GetSelectedItemIndex(list3) ~= sel_index then
		ListBox_SelectItem(list3, sel_index)
	end

end

function btnSearch_OnButtonClicked(buttonHandle)

	local text = EditBox_GetText(Dialog_GetEditBox(gDialogHandle, "editSearchField"))
	
	initSearch(text)

end

function editSearchField_OnEditBoxString(editBoxHandle, password)

	initSearch(password)

end

function initSearch( keyword )

	g_web_address = WEB_ADDRESS.. g_web_ext

	if keyword ~= "" then

		g_search = PARAM_SEARCH .. keyword
		g_useSearch = true
		g_web_address =  g_web_address..g_search
 	else
 	    g_useSearch = false
 	end

	g_curPage = 1
	g_set = 1
	
	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end

	g_InitBtnText = true

    resetPageButtonSelection()

end

--Toggle between ascending and descending sort
function toggleNameSortDirection()

	g_nameSortDescend = not g_nameSortDescend

	if g_nameSortDescend then
	    g_nameSortDirection = ORDER_DIR_DESC
	else
	    g_nameSortDirection = ORDER_DIR_ASC
	end

end

function toggleTypeSortDirection()

	g_typeSortDescend = not g_typeSortDescend

	if g_typeSortDescend then
	    g_typeSortDirection = ORDER_DIR_DESC
	else
	    g_typeSortDirection = ORDER_DIR_ASC
	end

end

function togglePopulationSortDirection()

	g_populationSortDescend = not g_populationSortDescend

	if g_populationSortDescend then
	    g_populationSortDirection = ORDER_DIR_DESC
	else
	    g_populationSortDirection = ORDER_DIR_ASC
	end

end

function toggleRavesSortDirection()

	g_ravesSortDescend = not g_ravesSortDescend

	if g_ravesSortDescend then
	    g_ravesSortDirection = ORDER_DIR_DESC
	else
	    g_ravesSortDirection = ORDER_DIR_ASC
	end

end

function btnSortName_OnButtonClicked(buttonHandle)

	g_curPage = 1
	g_set = 1
	g_sort = ORDER_NAME
	
	toggleNameSortDirection()

	g_web_address = WEB_ADDRESS..g_web_ext..g_sort..g_nameSortDirection

	if g_useSearch then
	    g_web_address = g_web_address..g_search
	end

	clearSelection()

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end 

    resetPageButtonSelection()
    
    g_typeSortDescend = true
	g_typeSortDirection = ORDER_DIR_DESC
	g_ravesSortDescend = false
	g_ravesSortDirection = ORDER_DIR_ASC
	g_populationSortDescend = false
	g_populationSortDirection = ORDER_DIR_ASC

end

function btnType_OnButtonClicked(buttonHandle)

	g_curPage = 1
	g_set = 1
	
	if g_current_tab == TAB_HANG_OUTS then
		g_sort = ORDER_OWNER
	else
		g_sort = ORDER_TYPE
	end
	
	toggleTypeSortDirection()

	g_web_address = WEB_ADDRESS..g_web_ext..g_sort..g_typeSortDirection

	if g_useSearch then
	    g_web_address = g_web_address..g_search
	end

	clearSelection()

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end 

    resetPageButtonSelection()
    
    g_nameSortDescend = true
	g_nameSortDirection = ORDER_DIR_DESC
	g_ravesSortDescend = false
	g_ravesSortDirection = ORDER_DIR_ASC
	g_populationSortDescend = false
	g_populationSortDirection = ORDER_DIR_ASC

end

function btnSortPopulation_OnButtonClicked(buttonHandle)

	g_curPage = 1
	g_set = 1
	g_sort = ORDER_POPULATION

	togglePopulationSortDirection()

	g_web_address = WEB_ADDRESS..g_web_ext..g_sort..g_populationSortDirection

	if g_useSearch then
	    g_web_address = g_web_address..g_search
	end

	clearSelection()

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end 

    resetPageButtonSelection()
    
    g_nameSortDescend = true
	g_nameSortDirection = ORDER_DIR_DESC
	g_typeSortDescend = true
	g_typeSortDirection = ORDER_DIR_DESC
	g_ravesSortDescend = false
	g_ravesSortDirection = ORDER_DIR_ASC

end

function btnSortRaves_OnButtonClicked(buttonHandle)

	g_curPage = 1
	g_set = 1
	g_sort = ORDER_RAVES
	
	toggleRavesSortDirection()

	g_web_address = WEB_ADDRESS..g_web_ext..g_sort..g_ravesSortDirection

	if g_useSearch then
	    g_web_address = g_web_address..g_search
	end

	clearSelection()

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end

    resetPageButtonSelection()
    
    g_nameSortDescend = true
	g_nameSortDirection = ORDER_DIR_DESC
	g_typeSortDescend = true
	g_typeSortDirection = ORDER_DIR_DESC
	g_populationSortDescend = false
	g_populationSortDirection = ORDER_DIR_ASC

end

-- Added by Jonny to handle alignment of the page buttons at the bottom.
function alignButtons()
	local imgRightBar = Dialog_GetImage(gDialogHandle, "right arrow bar")
	Control_SetLocation(imgRightBar, 450, 210)
	local btnRightRightArrow = Dialog_GetButton(gDialogHandle, "btnLast")
	Control_SetLocation(btnRightRightArrow, 432, 210)
    local btnRightArrow = Dialog_GetButton(gDialogHandle, "btnResultsNext")
	Control_SetLocation(btnRightArrow, 412, 210)

	local t = {}		-- new table
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange1"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange2"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange3"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange4"))
	rightAlignPageButtons(t, 407, 212, 5)

	local leftMostXPos = Control_GetLocationX(Dialog_GetButton(gDialogHandle, "btnRange1"))

	local btnLeftArrow = Dialog_GetButton(gDialogHandle, "btnResultsPrev")
	local leftArrowWidth = Control_GetWidth(btnLeftArrow)
	leftMostXPos = leftMostXPos - leftArrowWidth - 5
	Control_SetLocation(btnLeftArrow, leftMostXPos, 210)

	local btnLeftLeftArrow = Dialog_GetButton(gDialogHandle, "btnFirst")
	local leftLeftArrowWidth = Control_GetWidth(btnLeftLeftArrow)
	leftMostXPos = leftMostXPos - leftLeftArrowWidth - 5
	Control_SetLocation(btnLeftLeftArrow, leftMostXPos, 210)

	local imgLeftBar = Dialog_GetImage(gDialogHandle, "left arrow bar")
	local leftBarWidth = Control_GetWidth(imgLeftBar)
    leftMostXPos = leftMostXPos - leftBarWidth - 2
	Control_SetLocation(imgLeftBar, leftMostXPos, 210)
end




---------------------------------------------------------------------
-- Mouse Over
---------------------------------------------------------------------
function getButtonHandles()
--g_t_btnHandles

	local ch = Dialog_GetControl(gDialogHandle, "btnClose")
	if ch ~= nil then
		g_t_btnHandles["btnClose"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnTravelHelp");
	if ch ~= nil then
		g_t_btnHandles["btnTravelHelp"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnKaneva")
	if ch ~= nil then
     	g_t_btnHandles["btnKaneva"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnBroadband")
	if ch ~= nil then
     	g_t_btnHandles["btnBroadband"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnApartments")
	if ch ~= nil then
     	g_t_btnHandles["btnApartments"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSearch")
	if ch ~= nil then
     	g_t_btnHandles["btnSearch"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSortName")
	if ch ~= nil then
     	g_t_btnHandles["btnSortName"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnSortRaves")
	if ch ~= nil then
     	g_t_btnHandles["btnSortRaves"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSortPopulation")
	if ch ~= nil then
     	g_t_btnHandles["btnSortPopulation"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnType")
	if ch ~= nil then
     	g_t_btnHandles["btnType"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnHome")
	if ch ~= nil then
     	g_t_btnHandles["btnHome"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnGoThere")
	if ch ~= nil then
     	g_t_btnHandles["btnGoThere"] = ch
	end

end

--Events that happens when the mouse move over an dialog
function Dialog_OnMouseMove(dialogHandle, x, y)

    local sh = Dialog_GetStatic(dialogHandle, "lblStatus")

	if sh ~= nil then
		if Control_ContainsPoint(g_t_btnHandles["btnTravelHelp"], x, y) ~= 0 then
		    if g_helpbutton then
				Static_SetText(sh, "Hide the help menus")
			else
				Static_SetText(sh, "Display the help menus");
			end
			
		elseif Control_ContainsPoint(g_t_btnHandles["btnClose"], x, y) ~= 0 then
			Static_SetText(sh, "Close This Menu")
		elseif Control_ContainsPoint(g_t_btnHandles["btnKaneva"], x, y) ~= 0 then
		    Static_SetText(sh, "See a List of Public Places")
		elseif Control_ContainsPoint(g_t_btnHandles["btnBroadband"], x, y) ~= 0 then
		    Static_SetText(sh, "See a List of Places to Hang Out")
		elseif Control_ContainsPoint(g_t_btnHandles["btnApartments"], x, y) ~= 0 then
		    Static_SetText(sh, "See a List of People's Homes")
		elseif Control_ContainsPoint(g_t_btnHandles["btnSearch"], x, y) ~= 0 then
			Static_SetText(sh, "Search for a Particular Place")
		elseif Control_ContainsPoint(g_t_btnHandles["btnSortName"], x, y) ~= 0 then
		    Static_SetText(sh, "Sort List by Name of Place")
		elseif Control_ContainsPoint(g_t_btnHandles["btnSortPopulation"], x, y) ~= 0 then
		    Static_SetText(sh, "Sort List by Population")
		elseif Control_ContainsPoint(g_t_btnHandles["btnSortRaves"], x, y) ~= 0 then
		    Static_SetText(sh, "Sort List by Raves")
		elseif Control_ContainsPoint(g_t_btnHandles["btnType"], x, y) ~= 0 then
			Static_SetText(sh, "Sort List by Type of Place")
		elseif Control_ContainsPoint(g_t_btnHandles["btnGoThere"], x, y) ~= 0 then
		    Static_SetText(sh, "Go to Selected Place")
	    elseif Control_ContainsPoint(g_t_btnHandles["btnHome"], x, y) ~= 0 then
		    Static_SetText(sh, "Go Home")
		else
            Static_SetText(sh, "Mouse Over a Button for Information")
		end
	end

end