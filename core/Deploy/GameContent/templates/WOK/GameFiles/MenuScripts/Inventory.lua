dofile( "..\\ClientScripts\\MenuGameIds.lua" )
dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\CMD.lua")
dofile("..\\Scripts\\MovementIds.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("MenuStyles.lua")
dofile("CommonFunctions.lua")
dofile("MenuHelper.lua")
dofile("MenuLocation.lua")

NAME = 11
CASH = 9

YES_NO_BOX_EVENT = "YesNoBoxEvent"
YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"
--What is actually seen by the user.  used to access images to assign proper
-- icon or highlight
VISIBLE_LIST_SIZE = 10

NO_TRADE_ICON_NAME = "imgNoTrade"

-- Enum menu tabs
TAB_ITEMS = 0
TAB_FURNITURE = 1


-- global game object
g_game = nil

-- all inventory items
g_t_items = {}

-- boolean used to keep the handler, lstInventory_OnScrollBarChanged, from being called
-- when attribEventHandler() calls ListBox_AddItem()
g_b_loading_items = false


-- currently selected menu tab
g_current_tab = TAB_ITEMS

g_sel = -1

RequestForInventoryID 	= 51

g_dispatcher = nil

g_help = false;

-- table to hold button handles.  accessed by mouse over functions (containsPoint)
g_t_btnHandles = {}

--Set to true when an item is used.  This will make the script send a second request
--after the initial request for inventory.  This is to ensure that the equipped item
--change is made on the server so we can refresh the inventory status appropriately.
g_reRequestInv = false

g_credits = 0
g_rewardPoints = 0

g_parseType = PARSE_USERID

g_playerName = ""
g_userID = 0

g_last_track_pos = 0

g_showIcons = true


---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	g_dispatcher = dispatcher	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "attribEventHandler", 1,0,0,0, "AttribEvent", KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "answerEventHandler", 1,0,0,0, YES_NO_ANSWER_EVENT, KEP.MED_PRIO )	

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, YES_NO_BOX_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, YES_NO_ANSWER_EVENT, KEP.MED_PRIO )


	g_Dispatcher = dispatcher
end


---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	
	if KEPConfig_GetNumber == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end

	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	if g_game == 0 then
		g_game = nil
	end


	-- get the credits
	if g_game ~= nil then
			
		local t = { ClientEngine_GetPlayerInfo(g_game) }
		if t ~= 0  then
			-- index 11 = Name
			g_playerName = t[NAME]
            g_credits = t[CASH]
		end

	end

	local desclbl = Dialog_GetStatic(gDialogHandle, "lblDescription")
	if desclbl ~= nil then
		Static_SetText(desclbl, "")
	end
    
    desclbl = Dialog_GetStatic(gDialogHandle, "lblCredits")
	if desclbl ~= nil then
		Static_SetText(desclbl, g_credits)
	end

    bh = Dialog_GetButton(dialogHandle, "btnPlayerItems")
	ch = Button_GetControl(bh)
	if ch ~= nil then
		Control_SetEnabled(ch, false)
	end
	
	g_lstInventory	= Dialog_GetListBox( dialogHandle, "lstInventory" )
	g_imgIconDrag 	= Dialog_GetImage( dialogHandle, "imgIconDrag" )

	Dialog_FocusDefaultControl(gDialogHandle)
	btnPlayerItems_OnButtonClicked(Dialog_GetButton(gDialogHandle, "btnPlayerItems"))
	Dialog_BringToFront(gDialogHandle)
	
	loadLocation(g_game, gDialogHandle, "Inventory")
	
	getButtonHandles()
	
	g_showIcons = getIconPreference()
	
	setListBoxTexture(dialogHandle, "lstInventory")
    setListBoxStyle(Dialog_GetListBox(dialogHandle, "lstInventory"))
    setListBoxTexture(dialogHandle, "lstQuantity")
	setListBoxStyle(Dialog_GetListBox(dialogHandle, "lstQuantity"))
	
	HideHelp()
	
end

function HideHelp()
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static1"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static2"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static3"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static4"), false )
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image1"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image2"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image3"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image4"),false);
end

function btnHelp_OnButtonClicked(buttonHandle)
	g_help=not g_help
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static1"), g_help )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static2"), g_help )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static3"), g_help )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static4"), g_help )
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image1"),g_help);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image2"),g_help);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image3"),g_help);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image4"),g_help);
end


--dialogHande: number
--lb_name: string
function setListBoxTexture(dialogHandle, lb_name)

	-- setup look of listbox scrollbars and populate list
	local lbh = Dialog_GetListBox(dialogHandle, lb_name)
	if lbh ~= 0 then

		local sbh = ListBox_GetScrollBar(lbh)

		if sbh ~= 0 then

			local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then
				--Blue body
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				
				Element_SetCoords(eh, 361, 579, 390, 586)
			end

			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				--Grey lined
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 573, 409, 588)

			end


			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				--Up arrow button
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 547, 409, 561)

			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				--Down Arrow Button
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 642, 409, 656)

			end

			
		end

	end


end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is destroyed (OnClose)
--
---------------------------------------------------------------------
function Dialog_OnDestroy(dialogHandle)

	saveLocation(g_game, dialogHandle, "Inventory")

	Helper_Dialog_OnDestroy( dialogHandle )
end

function answerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	
    local answer = Event_DecodeNumber( event )
	
    if answer == 0 then
    
		-- Do nothing
	else
    
		local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
		if lbh ~= 0 then
        
			local GLID = ListBox_GetSelectedItemData(lbh)
            
			if GLID >= 0 then
            
				if g_game ~= nil then
                
					local index = ListBox_GetSelectedItemIndex(lbh)
					local invType = GameGlobals.IT_NORMAL
                    
					if g_t_items[index+1]["is_gift"] then
						invType = GameGlobals.IT_GIFT
					end
                    
					ClientEngine_RemoveInventoryItem(g_game, GLID, 1, invType )

					g_reRequestInv = true
					clearHighlights()
					RequestInventory()
				
				end				
			end
		end
	end
end

---------------------------------------------------------------------
--
-- request inventory from server
--
---------------------------------------------------------------------
function RequestInventory()

	-- always remove all items (the list will repopulate when the request is answered)
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	if lbh ~= 0 then
		ListBox_RemoveAllItems(lbh)
	end

	g_t_items 	= {}

	if g_game ~= nil then
		ClientEngine_RequestCharacterInventory(g_game)
	end
end

---------------------------------------------------------------------
-- Attrib Event Handler
---------------------------------------------------------------------
function attribEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

	 --Dialog_MsgBox("hold it "..filter)

	-- Reset visuals then test for inventory event

	if #g_t_items <= 0 then
		local ich = Dialog_GetImage(gDialogHandle, "imgDetailView")
		Control_SetVisible(ich, false)
		clearListIcons()
		clearHighlights()
		local desclbl = Dialog_GetStatic(gDialogHandle, "lblDescription")
	end


 	if filter == RequestForInventoryID then
		
		if g_reRequestInv then
		    g_reRequestInv = false
		    RequestInventory()
		else

			local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
			if lbh ~= 0 then

				-- always remove all items
				local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
				if lbh ~= 0 then
					ListBox_RemoveAllItems(lbh)
				end

   				-- always remove all items
				local lbhQ = Dialog_GetListBox(gDialogHandle, "lstQuantity")
				if lbhQ ~= 0 then
					ListBox_RemoveAllItems(lbhQ)
				end

				g_t_items 	= {}

				g_b_loading_items = true

				g_t_items = decodeInventoryEvent( event )

				g_t_items = separateForTabs( g_t_items )

				--sort by texture for some performance gain for icons
				g_t_items = sortTableByField(g_t_items, "texture_name")
			
				g_t_items = sortArmed(g_t_items)

				for i=1, #g_t_items do
					local item = g_t_items[i]

 					highlightWorn(i, item["is_armed"])

					ListBox_AddItem(lbh, item["name"], item["GLID"])
					ListBox_AddItem(lbhQ, item["quantity"], item["GLID"])
				end

				g_b_loading_items = false
			end

            resetNoTradeIcons()

			local num_items = 0
			-- determine how many icons need to be shown
			if lbh ~= 0 then
				num_items = ListBox_GetSize(lbh)
			end

			for i = 0, 9 do

				-- show list icons
				local icon_name = "imgIcon" .. tostring(i + 1)
				local ich = Dialog_GetImage(gDialogHandle, icon_name)

				if i < num_items then

					local item = g_t_items[i+1]

					if g_showIcons and ich ~= 0 then
					
						Control_SetVisible(ich, true)

						-- set the icon

						local eh = Image_GetDisplayElement(ich)
						if eh ~= 0 then
                            local item = g_t_items[i+1]
                            LoadIconTexture(g_game,item,eh,gDialogHandle);
						end
						
					else
						Control_SetVisible(ich, false)
					end

					ih = Dialog_GetImage(gDialogHandle, NO_TRADE_ICON_NAME..i+1)
					setNoTradeIcon(item, ih)
					
				end
				if lbh ~= nil then
					if ListBox_GetSize(lbh) > 0 then
						ListBox_SelectItem(lbh, 0)
					end
				end
			end

		end
	end
end


function separateForTabs( items_table )

	local t_item = {}
	local trimTable = {}

	if items_table ~= nil then
	
		for i=1,#items_table do
	
	    	t_item = items_table[i]
	    	
	    	use_type = t_item["use_type"]
	                                        
			if g_current_tab == TAB_ITEMS then
				if use_type ~= 10 and use_type ~= 11 then
					table.insert(trimTable, t_item)
				end
			end

			if g_current_tab == TAB_FURNITURE then
				if use_type == 10 or use_type == 11 then
					table.insert(trimTable, t_item)
				end
			end
			
		end
		
	end

	return trimTable

end

-- 
-- Takes in a table of inventory items (each inv item a table) 
-- and sorts them based on whether or 
-- not the player is wearing the item.  Worn items are placed at the beginning
-- of the table then all other items by their purchase date.  
--
-- Table needs an is_armed field
--
-- Returns: sorted table
--
function sortArmed( itemTable )

	local sortedItems = {}	-- table to hold items sorted by "armed" status
	local t_size = #itemTable -- size of our table
	local i = 0
	local front = 0	-- keep track of how many armed items are at the front of table
		
	for i=1, t_size do
	
		if itemTable[i]["is_armed"] then
			front = front + 1 
			table.insert(sortedItems, front, itemTable[i])
		else
			table.insert(sortedItems, front + 1, itemTable[i])
		end
		
	end
	
	return sortedItems

end


-- Highlight a worn item
-- index: int designating a listbox slot
-- isWorn: bool for whether or not item is currently equipped
function highlightWorn( index, isWorn )

	if index > -1 and index <= VISIBLE_LIST_SIZE then
	
		local ih = Dialog_GetImage(gDialogHandle, "imgWornHL"..index)
		if ih ~= nil then
		    local ch = Image_GetControl(ih)
		    Control_SetVisible(ch, isWorn)
		end
	
	end

end

function clearHighlights()

	for i=1, #g_t_items do

		highlightWorn(i, false)
		
	end
		
end

---------------------------------------------------------------------
-- Manage no trade icons
---------------------------------------------------------------------
function resetNoTradeIcons()

	for i = 1,VISIBLE_LIST_SIZE  do

	    ih = Dialog_GetImage(gDialogHandle, NO_TRADE_ICON_NAME..i)
		if ih ~= nil then
			Control_SetVisible(Image_GetControl(ih), false)
		end

	end

end


function setNoTradeIcon(item, icon_handle)

	if icon_handle ~= nil then

		if item["is_gift"] then
			Control_SetVisible(Image_GetControl(icon_handle), true)
		else
			Control_SetVisible(Image_GetControl(icon_handle), false)
		end

	end

end


---------------------------------------------------------------------
-- Helper function to clear out the item icons when tabs changed
---------------------------------------------------------------------
function clearListIcons()

	for i = 0, 9 do
		-- show list icons
		local icon_name = "imgIcon" .. tostring(i + 1)
		local ich = Dialog_GetImage(gDialogHandle, icon_name)
		Control_SetVisible(ich, false)
	end
    
	-- Now get rid of large icon too
	local ich = Dialog_GetImage(gDialogHandle, "imgDetailView")
	if ich ~= 0 then
		Control_SetVisible(ich, false)
	end
    
	-- Now get rid of the text description.
	local desclbl = Dialog_GetStatic(gDialogHandle, "lblDescription")
	if desclbl ~= nil then
		Static_SetText(desclbl, "")
	end
    
end


---------------------------------------------------------------------
-- Toggle player items tag on
---------------------------------------------------------------------
function btnPlayerItems_OnButtonClicked(buttonHandle)

    local bh = Dialog_GetButton(gDialogHandle, "btnPlayerItems")
	setButtonState(bh, false)

    bh = Dialog_GetButton(gDialogHandle, "btnFurniture")
	setButtonState(bh, true)

	if g_game ~= nil then
		local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
		if lbh ~= nil then
			local index = ListBox_GetSelectedItemIndex(lbh)
			if index >= 0 then
				ListBox_ClearSelection(lbh)
			end
		end
		g_current_tab = TAB_ITEMS
		clearListIcons()
		clearUseButtons()
		resetNoTradeIcons()
		RequestInventory()
	end
end



---------------------------------------------------------------------
-- Toggle Furniture tag on
---------------------------------------------------------------------
function btnFurniture_OnButtonClicked(buttonHandle)

	clearHighlights()

    local bh = Dialog_GetButton(gDialogHandle, "btnPlayerItems")
	setButtonState(bh, true)
	
    bh = Dialog_GetButton(gDialogHandle, "btnFurniture")
	setButtonState(bh, false)

	if g_game ~= nil then
		local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
		if lbh ~= nil then
			local index = ListBox_GetSelectedItemIndex(lbh)
			if index >= 0 then
				ListBox_ClearSelection(lbh)
			end			 
		end
		g_current_tab = TAB_FURNITURE
		clearListIcons()
		clearUseButtons()
		resetNoTradeIcons()
		RequestInventory()
	end
end

---------------------------------------------------------------------
-- Clear use/discard/wear buttons
---------------------------------------------------------------------
function clearUseButtons()
	if g_game ~= nil then
		local bh_use 		= Dialog_GetButton(gDialogHandle, "btnUse")
		local bh_discard 	= Dialog_GetButton(gDialogHandle, "btnDiscard")
		if bh_use ~= nil then
			Control_SetEnabled(bh_use, false)
		end
		if bh_discard ~= nil then
			Control_SetEnabled(bh_discard, false)
		end
	end
end

---------------------------------------------------------------------
-- Use Item
---------------------------------------------------------------------
function btnUse_OnButtonClicked(buttonHandle)

	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
    
	if lbh ~= 0 then
    
		local GLID = ListBox_GetSelectedItemData(lbh)
        
		if GLID >= 0 then
        
			if g_game ~= nil then
		
 				if g_t_items[g_sel+1]["use_type"] ~= 0 then
                
 					local invType = GameGlobals.IT_NORMAL
                    
 					if g_t_items[g_sel+1]["is_gift"] then
 						invType = GameGlobals.IT_GIFT
 					end
                    
					ClientEngine_UseInventoryItem(g_game, GLID, invType)
                    
				else
                
					local desc = g_t_items[g_sel+1]["name"].."\nQty: " .. tostring(g_t_items[g_sel+1]["quantity"])
                    
					--if string.find(g_t_items[g_sel+1]["name"], "(armed)") ~= nil then
					if g_t_items[g_sel+1]["is_armed"] then
                    
						ClientEngine_DisarmInventoryItem(g_game, GLID)
						-- We will assume that the disarm was successful.  This is bad, but I see no other solution
						-- Jonny
						g_t_items[g_sel+1]["is_armed"] = false
      					ListBox_SetItemText(lbh, g_sel, desc)
                        
					else
                    
						ClientEngine_ArmInventoryItem(g_game, GLID)
						g_t_items[g_sel+1]["is_armed"] = true
						ListBox_SetItemText(lbh, g_sel, desc)
                        
					end
                    
					ListBox_SelectItem(lbh, g_sel)
     				-- update the icons
					local sbh = ListBox_GetScrollBar(lbh)
                    
					if sbh ~= 0 then
						lstInventory_OnScrollBarChanged(sbh, lbh, ScrollBar_GetPageSize(sbh), ScrollBar_GetTrackPos(sbh) )
					end
                    
				end
			end
		end
	end

	g_reRequestInv = true
	clearHighlights()
	RequestInventory()
	
end

---------------------------------------------------------------------
-- Discard Item
---------------------------------------------------------------------
function btnDiscard_OnButtonClicked(buttonHandle)

	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
    
	if lbh ~= 0 then
    
		local GLID = ListBox_GetSelectedItemData(lbh)
        
		if GLID >= 0 then
			gotoMenu( "YesNoBox.xml", 0, 1 )
			local yesnoEvent = Dispatcher_MakeEvent( KEP.dispatcher, YES_NO_BOX_EVENT )	
			Event_EncodeString( yesnoEvent, "Are you sure you want to trash the selected item?" )
			Event_EncodeString( yesnoEvent, "Confirm" )
			Dispatcher_QueueEvent( KEP.dispatcher, yesnoEvent )
		end
        
	end
	
end


---------------------------------------------------------------------
-- Scrollbar Changed Handler
---------------------------------------------------------------------
function mutualListBoxScroll(scrollBarHandle, listBoxHandle, page_size, track_pos)

	if g_b_loading_items == false then
		local num_items = 0
		-- determine how many icons need to be shown
		if listBoxHandle ~= 0 then
			num_items = ListBox_GetSize(listBoxHandle)
		end
		
		resetNoTradeIcons()
		
		if num_items > 0 then
      		for i = 0, 9 do
				-- show list icons
				local icon_name = "imgIcon" .. tostring(i + 1)
				local ich = Dialog_GetImage(gDialogHandle, icon_name)
				
				if i < num_items then
				
					local cur_item = g_t_items[i + track_pos + 1]
					highlightWorn(i+1, cur_item["is_armed"])

					if g_showIcons and ich ~= 0 then
						Control_SetVisible(ich, true)
						-- set the icon
						local eh = Image_GetDisplayElement(ich)
						if eh ~= 0 then
							local item =g_t_items[i + track_pos + 1] 
                            LoadIconTexture(g_game,item,eh,gDialogHandle);
						end
					end

					ih = Dialog_GetImage(gDialogHandle, NO_TRADE_ICON_NAME..i+1)
					setNoTradeIcon(cur_item, ih)

				end
			end
		end
	end

end

function lstInventory_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)

	g_last_track_pos = track_pos

	if g_bSyncAllLists == 0 then
		local lbh_change = Dialog_GetListBox(gDialogHandle, "lstQuantity")
		syncListBoxes( lbh_change, listBoxHandle)
 		mutualListBoxScroll(scrollBarHandle, listBoxHandle, page_size, track_pos)
	end
	
end

function lstQuantity_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)

	if g_bSyncAllLists == 0 then
		local lbh_change = Dialog_GetListBox(gDialogHandle, "lstInventory")
		syncListBoxes( lbh_change, listBoxHandle)
	 	mutualListBoxScroll(scrollBarHandle, listBoxHandle, page_size, track_pos)
	end
	
end

---------------------------------------------------------------------
-- ListBox Synchronization
---------------------------------------------------------------------
g_bSyncAllLists = 0

--Syncs 2 list boxes
--listchanged: number - listbox handle, listbox that was changed by user
--listreceive: number - listbox handle, receives update from listchanged
--both inputs are numbers representing listbox handles

function syncListBoxes( listreceive, listchanged )
	if g_bSyncAllLists == 1 then
		return
	end

    g_bSyncAllLists = 1
	index = ListBox_GetSelectedItemIndex(listchanged)
	if listreceive ~= listchanged then
		ListBox_SelectItem(listreceive, index )
	end

	sb = ListBox_GetScrollBar(listchanged)
	spos = ScrollBar_GetTrackPos(sb)
	if listreceive ~= listchanged then
		sb = ListBox_GetScrollBar(listreceive)
		ScrollBar_SetTrackPos(sb, spos)
	end
    g_bSyncAllLists = 0
end

---------------------------------------------------------------------
-- Inventory Item Selected
---------------------------------------------------------------------

function mutualListBoxAction(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	-- enable/disable buttons
	local bh_use 		= Dialog_GetButton(gDialogHandle, "btnUse")
	local bh_discard 		= Dialog_GetButton(gDialogHandle, "btnDiscard")
	
	if bh_discard ~= 0 then
		Control_SetEnabled(bh_discard, true)
	end
	
	if bh_use ~= 0 then
		Control_SetEnabled(bh_use, true)
	end
	
	if sel_index > -1 then
	
		if g_t_items[sel_index+1]["use_type"] ~= -1 then
		
			if g_t_items[sel_index+1]["use_type"] ~= 0 then
			
				Static_SetText(Button_GetStatic(bh_use), "Use")
			else
				--if string.find(g_t_items[sel_index+1]["name"], "(armed)", 1) ~= nil then
				if g_t_items[sel_index+1]["is_armed"] then
					Static_SetText(Button_GetStatic(bh_use), "Remove")
					if bh_discard ~= 0 then
						Control_SetEnabled(bh_discard, false)
					end
				else
					Static_SetText(Button_GetStatic(bh_use), "Wear")
				end
			end
			local ich = Dialog_GetImage(gDialogHandle, "imgDetailView")
			local i = sel_index+1
			if g_showIcons and ich ~= 0 then
				Control_SetVisible(ich, true)
				-- set the icon
				local eh = Image_GetDisplayElement(ich)
				if eh ~= 0 then
		            local item = g_t_items[i]
                    LoadIconTexture(g_game,item,eh,gDialogHandle);
				end
			end
			local desclbl = Dialog_GetStatic(gDialogHandle, "lblDescription")
			if desclbl ~= nil then
				Static_SetText(desclbl, g_t_items[i]["description"])
			end	
					sh = Dialog_GetStatic(gDialogHandle, "lblStatus")
			if sh ~= nil then
				Static_SetText(sh, g_t_items[i]["name"])
			end
		end

	end

	g_sel = sel_index

end

function lstInventory_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	local list1 = Dialog_GetListBox(gDialogHandle, "lstQuantity")
	
    if g_bSyncAllLists == 0 then
		syncListBoxes( list1, listBoxHandle)
		mutualListBoxAction(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	end
	
end 

function lstQuantity_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	local list1 = Dialog_GetListBox(gDialogHandle, "lstInventory")

    if g_bSyncAllLists == 0 then
		syncListBoxes( list1, listBoxHandle)    	 	
		mutualListBoxAction(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	end
	
end

---------------------------------------------------------------------
-- Keyboard Handler
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
	-- ESC key
	if key == 27 then
		-- close this menu
		DestroyMenu(gDialogHandle)
	end
end
---------------------------------------------------------------------
-- Close the menu button.
---------------------------------------------------------------------
function btnClose_OnButtonClicked( buttonHandle )
	DestroyMenu(gDialogHandle)	
end



-- Show or hide images
-- param: boolean - true for show images false to hide.
function hideImages( state )

	for i=1,10 do

		local ich = Dialog_GetImage(gDialogHandle, "imgIcon"..i)
		if ich ~= nil then
			Control_SetVisible(ich, state)
			Control_SetEnabled(ich, state)
		end

	end
	
	local ich = Dialog_GetImage(gDialogHandle, "imgDetailView")
	if ich ~= nil then
		Control_SetVisible(ich, state)
		Control_SetEnabled(ich, state)
	end
	
end


---------------------------------------------------------------------
-- Mouse Over
---------------------------------------------------------------------
function getButtonHandles()
--g_t_btnHandles

	local ch = Dialog_GetControl(gDialogHandle, "btnClose")
	if ch ~= nil then
		g_t_btnHandles["btnClose"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnUse")
	if ch ~= nil then
     	g_t_btnHandles["btnUse"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnDiscard")
	if ch ~= nil then
     	g_t_btnHandles["btnDiscard"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnPlayerItems")
	if ch ~= nil then
     	g_t_btnHandles["btnPlayerItems"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnFurniture")
	if ch ~= nil then
     	g_t_btnHandles["btnFurniture"] = ch
	end

end

function Dialog_OnMouseMove(dialogHandle, x, y)

    local sh = Dialog_GetStatic(dialogHandle, "lblStatus")

	if sh ~= nil then
	    if g_reRequestInv then
        	Static_SetText(sh, "Updating...")
	    else

			if Control_ContainsPoint(g_t_btnHandles["btnClose"], x, y) ~= 0 then
				Static_SetText(sh, "Close This Menu")
			elseif Control_ContainsPoint(g_t_btnHandles["btnUse"], x, y) ~= 0 then
			    if g_current_tab == TAB_FURNITURE then
			    	Static_SetText(sh, "Place Selected Piece of Furniture")
			    elseif g_current_tab == TAB_ITEMS then
			        Static_SetText(sh, "Wear/Remove Selected Item of Clothing (Worn Items are Blue)")
			    end
			elseif Control_ContainsPoint(g_t_btnHandles["btnDiscard"], x, y) ~= 0 then
				if g_current_tab == TAB_FURNITURE then
					Static_SetText(sh, "Permanently Trash Selected Piece of Furniture")
				elseif g_current_tab == TAB_ITEMS then
					Static_SetText(sh, "Premanently Trash Selected Item of Clothing")
				end
			elseif Control_ContainsPoint(g_t_btnHandles["btnPlayerItems"], x, y) ~= 0 then
			    Static_SetText(sh, "View Your Avatar Items")
			elseif Control_ContainsPoint(g_t_btnHandles["btnFurniture"], x, y) ~= 0 then
				Static_SetText(sh, "View Your Furniture")
			end
			
		end
	end

end

