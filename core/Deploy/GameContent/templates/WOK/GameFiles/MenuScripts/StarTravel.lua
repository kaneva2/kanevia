dofile("MenuHelper.lua")
dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\CMD.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\Progressive.lua")
dofile("..\\Scripts\\MovementIds.lua")
dofile("PageButtons.lua")
dofile("MenuLocation.lua")
dofile("CommonFunctions.lua")

g_dispatcher = 0
g_game = 0
g_dispatcher = 0


g_selectedIndex = -1
g_t_stars = {} 

--current url we are using
g_web_address = GameGlobals.WEB_SITE_PREFIX.."kgp/gameList.aspx" 
-- g_web_address = "http://localhost/WOK/kgp/gameList.aspx"

-- Hold current stars data returned from server
g_starData = ""
-- Total number of star records
g_totalNumRec = 0
-- Maximum number of stars for server to return in one request
-- places per page
g_starsPerPage = 8



---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )		

	ParentHandler_RegisterEventHandler( dispatcher, handler, "BrowserPageReadyHandler", 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "textHandler", 1,0,0,0, "RenderTextEvent", KEP.HIGH_PRIO )	
	--ParentHandler_RegisterEventHandler( dispatcher, handler, "answerEventHandler", 1,0,0,0, "YesNoAnswerEvent", KEP.MED_PRIO )

	g_dispatcher = dispatcher
end

--Event Handlers
function answerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

   	local answerNumber = Event_DecodeNumber( event )
	if answerNumber == 0 then
	    local bh = Dialog_GetButton(gDialogHandle, "btnGoStar")
    	setButtonState(bh, true)
	end

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	--Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, BEGIN_DOWNLOAD_EVENT, KEP.MED_PRIO )
	ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )	

end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	
	-- TODO init here
	
	g_game = Dispatcher_GetGame(KEP.dispatcher)
	
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	
	if KEPConfig_GetNumber == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end
	
	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.STAR_TRAVEL, g_curPage, g_starsPerPage)
	end
	
    loadLocation(g_game, gDialogHandle, "StarTravel")
    
	initButtons()
    
    setSearchState(false)

    --alignButtons() --TEMP REMOVE ME

	-- setSelectionColor is called from PageButtons.lua to highlight the first
	-- button when the menu is opend.
	local bh = Dialog_GetButton(dialogHandle, "btnRange1")
	if bh ~= nil then
		setSelectionColor( bh )
	end
   
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
    saveLocation(g_game, dialogHandle, "StarTravel")
    
	Helper_Dialog_OnDestroy( dialogHandle )
end


-- Catch the xml from the web server
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter == WF.STAR_TRAVEL then
	
		g_starData = Event_DecodeString( event )
		
	    ParseStarData(gDialogHandle)

		return KEP.EPR_CONSUMED
		
	end
	
end

-- Parse star data from server
function ParseStarData(dialogHandle)

	local numRec = 0   		    -- number of records in this chunk
	local strPos = 0    	    -- current position in string to parse
	local code = "-1"  		    -- return code from web

-- number<space><results>
-- 2 Game not found
-- 7 Bad parameter
-- 0 server port

	s, e, result = string.find(g_starData, "<ReturnCode>([0-9]+)</ReturnCode>")
	
	if result == "0" then
	
		local sh = Dialog_GetStatic(gDialogHandle, "lblResults")

		local lbh = Dialog_GetListBox(gDialogHandle, "lstName")
		local lbhOn = Dialog_GetListBox(gDialogHandle, "lstPopulation")
		local lbhR = Dialog_GetListBox(gDialogHandle, "lstRaves")
		
		if (lbh ~= 0) and (lbhOn ~= 0) and (lbhR ~= 0) and (sh ~= 0) then

            numRec, g_totalNumRec = calculateRanges(g_starData, sh, g_starsPerPage)
        
			g_t_stars = {}
            
			ListBox_RemoveAllItems(lbh)
			ListBox_RemoveAllItems(lbhOn)
   			ListBox_RemoveAllItems(lbhR)
			
			for i=1,numRec do

                local s = 0     		-- start of substring
                local e = 0             -- end of substring

				s, strPos, starstr = string.find(g_starData, "<game>(.-)</game>", strPos)
				s, e, name = string.find(starstr, "<name>(.-)</name>", 1)
                s, e, description = string.find(starstr, "<description>(.-)</description>", 1)
				s, e, patch_url = string.find(starstr, "<patch_url>(.-)</patch_url>", 1)
				s, e, server_url = string.find(starstr, "<server_url>(.-)</server_url>", 1)
				s, e, population = string.find(starstr, "<population>([0-9]+)</population>", 1)
				s, e, max_pop = string.find(starstr, "<max_population>([0-9]+)</max_population>", 1)
				s, e, raves = string.find(starstr, "<raves>([0-9]+)</raves>", 1)
				s, e, gameId = string.find(starstr, "<game_id>([0-9]+)</game_id>", 1)

				local star = {}
				star["name"] = name
                star["game_id"] = tonumber(gameId)
                star["description"] = description
				star["server_url"] = server_url
                star["patch_url"] = patch_url
				star["population"] = tonumber(population)
                star["raves"] = tonumber(raves)
                star["max_pop"] = tonumber(max_pop)
                
				table.insert(g_t_stars, star)				
				ListBox_AddItem(lbh, name, star["max_pop"])
                ListBox_AddItem(lbhOn, population, star["population"])
    			ListBox_AddItem(lbhR, raves, star["raves"] )
			end
			
			alignButtons()

		end
    else
        Dispatcher_LogMessage(g_dispatcher, KEP.DEBUG_LOG_LEVEL, "StarTravel.lua - ParseStarData - error in xml returned from web server - "..result)
    end

 	
end

---------------------------------------------------------------------
-- Button Handler
---------------------------------------------------------------------
function btnClose_OnButtonClicked( buttonHandle )
	DestroyMenu(gDialogHandle)
end

function btnResultsPrev_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

	    calcResultsPrev()
		alignButtons()

		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.STAR_TRAVEL, g_curPage, g_starsPerPage)
		end
	end

	if g_selectedIndex > -1 then
		clearSelection()
	end

end


function btnResultsNext_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

        calcResultsNext()
		alignButtons()

		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.STAR_TRAVEL, g_curPage, g_starsPerPage)
		end

 	end

	if g_selectedIndex > -1 then
		clearSelection()
	end

end


function btnFirst_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

		calcFirst()
		alignButtons()

		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.STAR_TRAVEL, g_curPage, g_starsPerPage)
		end

	end
	if g_selectedIndex > -1 then
		clearSelection()
	end
	
end

function btnLast_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

		calcLast()
		alignButtons()

		--g_web_address = WEB_ADDRESS.. g_web_ext .. g_sort

		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.STAR_TRAVEL, g_curPage, g_starsPerPage)
		end

	end

	if g_selectedIndex > -1 then
		clearSelection()
	end

end
   
   
function btnRange1_OnButtonClicked( buttonHandle )

	calcRange1()

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.STAR_TRAVEL, g_curPage, g_starsPerPage)
	end
	
	clearSelection()

end

function btnRange2_OnButtonClicked( buttonHandle )

	calcRange2()

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.STAR_TRAVEL, g_curPage, g_starsPerPage)
	end

	 clearSelection()

end

function btnRange3_OnButtonClicked( buttonHandle )

	calcRange3()

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.STAR_TRAVEL, g_curPage, g_starsPerPage)
	end

	clearSelection()

end

function btnRange4_OnButtonClicked( buttonHandle )

	calcRange4()

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.STAR_TRAVEL, g_curPage, g_starsPerPage)
	end

	clearSelection()

end

function btnGoStar_OnButtonClicked(buttonHandle)
	if ClientEngine_GetPlayer == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	local lbh = Dialog_GetListBox(gDialogHandle, "lstName")
	local index = ListBox_GetSelectedItemIndex(lbh) + 1
	if index < 0 then
		return
	end
    
    local ev = Dispatcher_MakeEvent( KEP.dispatcher, KEP.GOTOPLACE_EVENT_NAME ) 
    Event_SetFilter( ev, KEP.GOTO_GAME ) -- We are going to another game 
    Event_EncodeNumber( ev, 0 ) -- currently selected char (always 0) 
    Event_EncodeNumber( ev, g_t_stars[index]["game_id"]) 
    Event_EncodeString( ev, g_t_stars[index]["name"]) 
    Dispatcher_QueueEvent( KEP.dispatcher, ev ) -- processed locally (don't AddTo server) 
    
	if PROGRESSIVE == 1 then
		DestroyMenu(gDialogHandle)
	else	
		setButtonState(buttonHandle, false)
	end
    
end

function btnSearch_OnButtonClicked(buttonHandle)

	local text = EditBox_GetText(Dialog_GetEditBox(gDialogHandle, "editSearchField"))
	
	initSearch(text)

end

function btnSortName_OnButtonClicked(buttonHandle)

	g_curPage = 1
	g_set = 1
	g_sort = ORDER_NAME
	
	toggleNameSortDirection()

	g_web_address = WEB_ADDRESS..g_web_ext..g_sort..g_nameSortDirection

	if g_useSearch then
	    g_web_address = g_web_address..g_search
	end

	clearSelection()

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end 

    resetPageButtonSelection()
    
    g_typeSortDescend = true
	g_typeSortDirection = ORDER_DIR_DESC
	g_ravesSortDescend = false
	g_ravesSortDirection = ORDER_DIR_ASC
	g_populationSortDescend = false
	g_populationSortDirection = ORDER_DIR_ASC

end


function btnSortPopulation_OnButtonClicked(buttonHandle)

	g_curPage = 1
	g_set = 1
	g_sort = ORDER_POPULATION

	togglePopulationSortDirection()

	g_web_address = WEB_ADDRESS..g_web_ext..g_sort..g_populationSortDirection

	if g_useSearch then
	    g_web_address = g_web_address..g_search
	end

	clearSelection()

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end 

    resetPageButtonSelection()
    
    g_nameSortDescend = true
	g_nameSortDirection = ORDER_DIR_DESC
	g_typeSortDescend = true
	g_typeSortDirection = ORDER_DIR_DESC
	g_ravesSortDescend = false
	g_ravesSortDirection = ORDER_DIR_ASC

end

function btnSortRaves_OnButtonClicked(buttonHandle)

	g_curPage = 1
	g_set = 1
	g_sort = ORDER_RAVES
	
	toggleRavesSortDirection()

	g_web_address = WEB_ADDRESS..g_web_ext..g_sort..g_ravesSortDirection

	if g_useSearch then
	    g_web_address = g_web_address..g_search
	end

	clearSelection()

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.COOL_PLACES, g_curPage, g_placesPerPage)
	end

    resetPageButtonSelection()
    
    g_nameSortDescend = true
	g_nameSortDirection = ORDER_DIR_DESC
	g_typeSortDescend = true
	g_typeSortDirection = ORDER_DIR_DESC
	g_populationSortDescend = false
	g_populationSortDirection = ORDER_DIR_ASC

end

---------------------------------------------------------------------
-- Keyboard Handler
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
      -- ESC key
      if key == 27 then
            -- close this menu
            DestroyMenu(gDialogHandle)
      end
end

---------------------------------------------------------------------
-- List Boxes
---------------------------------------------------------------------
function lstName_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	local list1 = Dialog_GetListBox(gDialogHandle, "lstPopulation")
	local list2 = Dialog_GetListBox(gDialogHandle, "lstType")
	local list3 = Dialog_GetListBox(gDialogHandle, "lstRaves")

	if ListBox_GetSelectedItemIndex(list1) ~= sel_index then
		ListBox_SelectItem(list1, sel_index)
	end
	if ListBox_GetSelectedItemIndex(list2) ~= sel_index then
		ListBox_SelectItem(list2, sel_index)
	end
	if ListBox_GetSelectedItemIndex(list3) ~= sel_index then
		ListBox_SelectItem(list3, sel_index)
	end
    
    local desc = g_t_stars[sel_index+1]["description"]
    
    local sh = Dialog_GetStatic(gDialogHandle, "lblDescription")
    Static_SetText(sh, desc)
    
    fillListBox( desc )

end

function lstPopulation_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	local list1 = Dialog_GetListBox(gDialogHandle, "lstName")
	local list2 = Dialog_GetListBox(gDialogHandle, "lstType")
	local list3 = Dialog_GetListBox(gDialogHandle, "lstRaves")

	if ListBox_GetSelectedItemIndex(list1) ~= sel_index then
		ListBox_SelectItem(list1, sel_index)
	end
	if ListBox_GetSelectedItemIndex(list2) ~= sel_index then
		ListBox_SelectItem(list2, sel_index)
	end
	if ListBox_GetSelectedItemIndex(list3) ~= sel_index then
		ListBox_SelectItem(list3, sel_index)
	end

    local desc = g_t_stars[sel_index+1]["description"]

    local sh = Dialog_GetStatic(gDialogHandle, "lblDescription")
    Static_SetText(sh, desc)

    fillListBox( desc )

end

function lstRaves_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	local list1 = Dialog_GetListBox(gDialogHandle, "lstName")
	local list2 = Dialog_GetListBox(gDialogHandle, "lstPopulation")
	local list3 = Dialog_GetListBox(gDialogHandle, "lstType")
	
	if ListBox_GetSelectedItemIndex(list1) ~= sel_index then
		ListBox_SelectItem(list1, sel_index)
	end
	if ListBox_GetSelectedItemIndex(list2) ~= sel_index then
		ListBox_SelectItem(list2, sel_index)
	end
	if ListBox_GetSelectedItemIndex(list3) ~= sel_index then
		ListBox_SelectItem(list3, sel_index)
	end

    local desc = g_t_stars[sel_index+1]["description"]

    local sh = Dialog_GetStatic(gDialogHandle, "lblDescription")
    Static_SetText(sh, desc)

    fillListBox( desc )

end

---------------------------------------------------------------------
-- Edit Boxes
---------------------------------------------------------------------

function editSearchField_OnEditBoxString(editBoxHandle, password)

	initSearch(password)

end


---------------------------------------------------------------------
-- Helpers
---------------------------------------------------------------------

function clearSelection()

	g_selectedIndex = -1
	
	lhN = Dialog_GetListBox(gDialogHandle, "lstName")
	lhP = Dialog_GetListBox(gDialogHandle, "lstPopulation")
    lhR = Dialog_GetListBox(gDialogHandle, "lstRaves")

	if lhN ~= nil then
		ListBox_ClearSelection(lhN)
	end
	
	if lhP ~= nil then
		ListBox_ClearSelection(lhP)
	end

   	if lhR ~= nil then
		ListBox_ClearSelection(lhR)
	end

end

function initSearch( keyword )

	g_web_address = WEB_ADDRESS.. g_web_ext

	if keyword ~= "" then

		g_search = PARAM_SEARCH .. keyword
		g_useSearch = true
		g_web_address =  g_web_address..g_search
 	else
 	    g_useSearch = false
 	end

	g_curPage = 1
	g_set = 1
	
	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.STAR_TRAVEL, g_curPage, g_starsPerPage)
	end

	g_InitBtnText = true

    resetPageButtonSelection()

end

-- state: boolean describing whether or not controls enabled
function setSearchState( state )

	local handle = Dialog_GetControl(gDialogHandle, "btnSearch")
	Control_SetEnabled( handle, state )
	Control_SetVisible( handle, state )
	
	handle = Dialog_GetControl(gDialogHandle, "editSearchField")
	Control_SetEnabled( handle, state )
	Control_SetVisible( handle, state )


--[[ Future feature

	handle = Dialog_GetControl(gDialogHandle, "btnFilters")
	Control_SetEnabled( handle, state )
	Control_SetVisible( handle, state )
]]--

end

--Toggle between ascending and descending sort
function toggleNameSortDirection()

	g_nameSortDescend = not g_nameSortDescend

	if g_nameSortDescend then
	    g_nameSortDirection = ORDER_DIR_DESC
	else
	    g_nameSortDirection = ORDER_DIR_ASC
	end

end


function togglePopulationSortDirection()

	g_populationSortDescend = not g_populationSortDescend

	if g_populationSortDescend then
	    g_populationSortDirection = ORDER_DIR_DESC
	else
	    g_populationSortDirection = ORDER_DIR_ASC
	end

end

function toggleRavesSortDirection()

	g_ravesSortDescend = not g_ravesSortDescend

	if g_ravesSortDescend then
	    g_ravesSortDirection = ORDER_DIR_DESC
	else
	    g_ravesSortDirection = ORDER_DIR_ASC
	end

end

-- Function to fill our listbox with the parsed data.
-- Used for the interests and stats tab
--
-- msg: string of parsed message data
function fillListBox( msg )

	local t_msgParse = letsGetFunky(msg, "lblDescription")
	local lb = Dialog_GetListBox(gDialogHandle, "lstDescription")


	if ListBox_GetSize(lb) > 0 then
		ListBox_RemoveAllItems(lb)
	end
	for i=1,#t_msgParse do
		ListBox_AddItem(lb, t_msgParse[i], 1)
	end

end

-- Added by Jonny to handle alignment of the page buttons at the bottom.
function alignButtons()
	local imgRightBar = Dialog_GetImage(gDialogHandle, "right arrow bar")
	Control_SetLocation(imgRightBar, 450, 210)
	local btnRightRightArrow = Dialog_GetButton(gDialogHandle, "btnLast")
	Control_SetLocation(btnRightRightArrow, 347, 168)
    local btnRightArrow = Dialog_GetButton(gDialogHandle, "btnResultsNext")
	Control_SetLocation(btnRightArrow, 327, 168)

	local t = {}		-- new table
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange1"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange2"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange3"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange4"))
	rightAlignPageButtons(t, 327, 168, 5)

	local leftMostXPos = Control_GetLocationX(Dialog_GetButton(gDialogHandle, "btnRange1"))

	local btnLeftArrow = Dialog_GetButton(gDialogHandle, "btnResultsPrev")
	local leftArrowWidth = Control_GetWidth(btnLeftArrow)
	leftMostXPos = leftMostXPos - leftArrowWidth - 5
	Control_SetLocation(btnLeftArrow, leftMostXPos, 168)

	local btnLeftLeftArrow = Dialog_GetButton(gDialogHandle, "btnFirst")
	local leftLeftArrowWidth = Control_GetWidth(btnLeftLeftArrow)
	leftMostXPos = leftMostXPos - leftLeftArrowWidth - 5
	Control_SetLocation(btnLeftLeftArrow, leftMostXPos, 168)

	local imgLeftBar = Dialog_GetImage(gDialogHandle, "left arrow bar")
	local leftBarWidth = Control_GetWidth(imgLeftBar)
    leftMostXPos = leftMostXPos - leftBarWidth - 2
	Control_SetLocation(imgLeftBar, leftMostXPos, 168)
end

