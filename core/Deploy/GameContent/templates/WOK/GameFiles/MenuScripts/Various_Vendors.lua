dofile("MenuHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("MenuStyles.lua")
dofile("CommonFunctions.lua")

MAX_BUY_ITEMS = 10000

GIFT_ICON_INV_NAME = "imgGiftIconInv"

LIST_BOX_SIZE = 8

--Index of player's cash attribute from ClientEngine_GetPlayerInfo
CASH = 9

-- global game object
g_game = nil
g_help = false;

-- all inventory items
g_t_items = {}

-- all vendor inventory items
g_t_vendor_items = {}

-- boolean used to keep the handler, lstInventory_OnScrollBarChanged, from being called
-- when attribEventHandler() calls ListBox_AddItem()
g_b_loading_items = false


g_credits = 0

g_t_transaction 	= {}	-- type (buy, sell), index, new_quantity (0 = remove_all)

RequestForInventoryID 	= 51
VendorInventoryID 	    = 52
UpdateCashID		    = 19
AEGenericTypeID 	    = 142

-- helpers to speed up access to controls
g_imgIconDrag 	= nil
g_lstLeft 	= nil
g_lstRight 	= nil

g_dragging 	= false
g_source	= nil
g_target	= nil


g_last_inv_track_pos = 0
g_last_vendor_track_pos = 0

g_showIcons = true

--This will be a table of player attributes after OnCreate is called
--indexed by number
g_playerInfo = nil


---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "attribEventHandler", 1,0,0,0, "AttribEvent", KEP.MED_PRIO )
	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
   	
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	if KEPConfig_GetNumber == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end	

	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	if g_game == 0 then
		g_game = nil
	end

	-- setup look of listbox scrollbars
	SetupListBoxScrollBars("lstInventory")
	SetupListBoxScrollBars("lstVendorInventory")

	--GetCredits()
    resetInvGiftIcons()
    UpdateCredits()
    
	g_lstLeft	= Dialog_GetListBox( dialogHandle, "lstInventory" )
	g_lstRight 	= Dialog_GetListBox( dialogHandle, "lstVendorInventory" )
	g_imgIconDrag 	= Dialog_GetImage( dialogHandle, "imgIconDrag" )

	-- disable so that when dragging it will not intercept mouse messages
	Control_SetEnabled( g_imgIconDrag, false )

	
    local ebh = Dialog_GetEditBox( gDialogHandle, "txtQuantityStore" )

	if ebh ~= 0 then
   		EditBox_SetText( ebh, "1", false )
	end
	
	g_showIcons = getIconPreference()

    HideHelp()
end

function HideHelp()
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static2"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static3"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static4"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static5"), false )
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image2"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image3"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image4"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image5"),false);
end

function btnHelp_OnButtonClicked(buttonHandle)
	g_help=not g_help
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static2"), g_help )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static3"), g_help )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static4"), g_help )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static5"), g_help )
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image2"),g_help);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image3"),g_help);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image4"),g_help);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image5"),g_help);
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	Helper_Dialog_OnDestroy( dialogHandle )
end



function getPlayerInformation()

	if g_game ~= nil then
			
		local t = { ClientEngine_GetPlayerInfo(g_game) }
		if t ~= 0  then
            g_playerInfo = t
		end

	end

end


---------------------------------------------------------------------
--
-- Attrib Event Handler
--
---------------------------------------------------------------------
function attribEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

	if filter == RequestForInventoryID then

		local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
		if lbh ~= 0 then

			-- always remove all items		
			ListBox_RemoveAllItems(lbh)

			g_t_items 	= {}

			g_b_loading_items = true


			g_t_items = decodeInventoryEvent( event, true ) 

			local ich = Dialog_GetImage(gDialogHandle, "imgItemPicInv")
			
			if #g_t_items > 0 then
				setImageState(ich, true)
			else
				setImageState(ich, false)
			end
           	
			g_t_items = sortTableByField(g_t_items, "texture_name")
			     
            for i=1, #g_t_items do
            
                local text = "\nQuantity: " .. tostring(g_t_items[i]["quantity"])

				if g_t_items[i]["can_trade"] then
                	text = text.."  Sell Price: "..tostring(g_t_items[i]["sell_price"])
				end
            
				ListBox_AddItem(lbh, g_t_items[i]["name"]..text, g_t_items[i]["GLID"])
				
            end				 
				        
			g_b_loading_items = false

		end

		local num_items = 0
		-- determine how many icons need to be shown
		if lbh ~= 0 then
			num_items = ListBox_GetSize(lbh)
		end

		resetInvGiftIcons()

		for i = 0, 7 do
			
			if i < num_items then
				
				local item = g_t_items[i+1]

				-- show list icons
				local icon_name = "imgIcon" .. tostring(i + 1)
				local ich = Dialog_GetImage(gDialogHandle, icon_name)				
				if ich ~= 0 and g_showIcons then

					Control_SetVisible(ich, true)

					-- set the icon

					local eh = Image_GetDisplayElement(ich)
					if eh ~= 0 then

                    	--Display the Icons the player have using the new system.
	                    local item = g_t_items[i+1]
						LoadIconTexture(g_game,item,eh,gDialogHandle);
					end
					
				end

				ih = Dialog_GetImage(gDialogHandle, GIFT_ICON_INV_NAME..i+1)
	    	    setGiftIcon(item, ih)

				-- show list labels (second line)
				local label_name = "lblInventory" .. tostring(i + 1)
				local sh = Dialog_GetStatic(gDialogHandle, label_name)
				if sh ~= 0 then

					Control_SetVisible(sh, false)

				end
			end
	    end
	    
	elseif filter == VendorInventoryID then

		local lbh = Dialog_GetListBox(gDialogHandle, "lstVendorInventory")
		if lbh ~= 0 then

			-- always remove all items		
			ListBox_RemoveAllItems(lbh)

			g_t_vendor_items 	= {}

			g_b_loading_items = true

			local num_items = Event_DecodeNumber( event )
   			for i = 0, num_items-1 do

				local GLID 		= Event_DecodeNumber( event )

				local cost 		= Event_DecodeNumber( event )
				local name 		= Event_DecodeString( event )
				local use_type		= Event_DecodeNumber( event )
				local sellingPrice =  Event_DecodeNumber( event )
				local statCount = Event_DecodeNumber( event )
				local req_str		= 0
				local req_dex		= 0
				local req_knowledge	= 0
				for j = 0, statCount-1 do
				
					local statID = Event_DecodeNumber( event )
					local statBonus = Event_DecodeNumber( event )
					-- stats are GPB specific
					if statID == GPB.Stat_STR then
						req_str = statBonus
					elseif statID == GPB.Stat_DEX then
						req_dex = statBonus
					elseif statID == GPB.Stat_WIS then
						req_knowledge = statBonus
					end
				end
				local req_skill_level	= Event_DecodeNumber( event )
				local req_skill_type	= Event_DecodeNumber( event )
				local texture_name 	= Event_DecodeString( event )
				-- texture coords
				local left		= Event_DecodeNumber( event )
				local top		= Event_DecodeNumber( event )
				local right		= Event_DecodeNumber( event )
				local bottom		= Event_DecodeNumber( event )
                local itemType  = Event_DecodeNumber( event )
                local desc      = Event_DecodeString( event )
				
				

                local text = "\ncost: " .. tostring(cost)

				--ListBox_AddItem(lbh, name..text, GLID)

				local t_item 			= {}
				t_item["GLID"]			= GLID
				t_item["cost"]			= cost
				t_item["quantity"]		= 1
				t_item["name"]			= name
				t_item["use_type"] 		= use_type
				t_item["req_dex"] 		= req_dex
				t_item["req_knowledge"] 	= req_knowledge
				t_item["req_skill_level"] 	= req_skill_level
				t_item["req_skill_type"] 	= req_skill_type
				t_item["req_str"] 		= req_str
				t_item["texture_name"] 		= texture_name
				t_item["texture_coords"]	= {left, top, right, bottom}
				t_item["sell_price"]     = sellingPrice
                t_item["item_type"]     = itemType
                t_item["description"]   = desc

				-- add to items table
				table.insert(g_t_vendor_items, t_item)

			end

           	g_t_vendor_items = sortTableByField(g_t_vendor_items, "texture_name")
                 
            for i=1, #g_t_vendor_items do
            
				ListBox_AddItem(lbh, g_t_vendor_items[i]["name"].."\nCost: "..tostring(g_t_vendor_items[i]["cost"]), g_t_vendor_items[i]["GLID"])            
				
            end	

			g_b_loading_items = false

		end


		local num_items = 0
		-- determine how many icons need to be shown
		if lbh ~= 0 then
			num_items = ListBox_GetSize(lbh)
		end
		
		for i = 0, 7 do			

			if i < num_items then

				-- show list icons
				local icon_name = "imgVendorIcon" .. tostring(i + 1)
				local ich = Dialog_GetImage(gDialogHandle, icon_name)
				if ich ~= 0 and g_showIcons then

					Control_SetVisible(ich, true)

					-- set the icon

					local eh = Image_GetDisplayElement(ich)
					if eh ~= 0 then
						
						--Display the Vendor Items using the new icon system
                       	local item = g_t_vendor_items[i+1]
						LoadIconTexture(g_game,item,eh,gDialogHandle);
					end
				end

				-- show list labels (second line)
				local label_name = "lblVendor" .. tostring(i + 1)
				local sh = Dialog_GetStatic(gDialogHandle, label_name)
				if sh ~= 0 then

					Control_SetVisible(sh, false)

				end
			end
		end

	elseif filter == AEGenericTypeID then

		-- transaction was completed successfully
		if objectid == 92 then

			local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")

			-- add the item to the other list
			-- (to start, make a copy -- we will adjust quantity below)

			if g_t_transaction["type"] == "buy" then

				local t_item = tcopy(g_t_vendor_items[ g_t_transaction["index"] + 1])
	
				-- the amount of this transaction
				t_item["quantity"] = g_t_transaction["new_quantity"]
				t_item["sell_price"] = g_t_transaction["sell_price"]
				t_item["can_trade"] = g_t_transaction["can_trade"]

				-- actually add the item now that the quantity has been set
				AddItemToInventory(t_item)	

			elseif g_t_transaction["type"] == "sell" then

				local t_item = tcopy(g_t_items[ g_t_transaction["index"] + 1])
	
				-- the amount of this transaction
				t_item["quantity"] = g_t_transaction["new_quantity"]
				t_item["sell_price"] = g_t_transaction["sell_price"]
				t_item["can_trade"] = g_t_transaction["can_trade"]
			
				RemoveItemFromInventory(t_item)				

			end

            UpdateCredits()
			g_t_transaction = {}
			EnableTransactionControls(true)
		end
	end


end

--Color the items icon red if it cannot be traded
function setTradeColor( index, imageHandle )

	if not g_t_items[index]["can_trade"] then
		-- Have to send decimal of color this number is 0x88880000 in hex
		Image_SetColor( imageHandle, 4287102976 )
		--Image_SetColor( imageHandle, 2290614272 )
	else
		-- Have to send decimal of color this number is 0xFFFFFFFF in hex
		Image_SetColor( imageHandle, 4294967295 )
	end	


end

-- Turn off all gift icons
function resetInvGiftIcons()

	for i = 1,LIST_BOX_SIZE  do

	    ih = Dialog_GetImage(gDialogHandle, GIFT_ICON_INV_NAME..i)
		if ih ~= nil then
			Control_SetVisible(Image_GetControl(ih), false)
		end

	end

end


---------------------------------------------------------------------
--
-- update Credit labels
--
---------------------------------------------------------------------
function UpdateCredits()

    getPlayerInformation()
    
    if g_playerInfo ~= nil then
    
        local sh = Dialog_GetStatic(gDialogHandle, "lblCreditAmount")
        if sh ~= 0 then
            Static_SetText(sh, g_playerInfo[CASH])
        end        
        
        g_credits = tonumber(g_playerInfo[CASH])    
        
    end

end

---------------------------------------------------------------------
--
-- Setup look of listbox scrollbars
--
---------------------------------------------------------------------
function SetupListBoxScrollBars(listbox_name)

	local lbh = Dialog_GetListBox(gDialogHandle, listbox_name)
	if lbh ~= 0 then

		local sbh = ListBox_GetScrollBar(lbh)

		if sbh ~= 0 then

			local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then
				--Blue body
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				
				Element_SetCoords(eh, 361, 579, 390, 586)
			end

			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				--Grey lined
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 573, 409, 588)

			end

			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				--Up arrow button
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 547, 409, 561)

			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				--Down Arrow Button
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 642, 409, 656)

			end

		end

	end

end

---------------------------------------------------------------------
--
-- Keyboard Handler
--
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)

	-- ESC key
	if key == 27 then

		-- send request for inventory so other menus interested in the current inventory
		-- will be notified
		if g_game ~= nil then
			ClientEngine_RequestCharacterInventory(g_game)
		end

		-- close this menu
		DestroyMenu(gDialogHandle)

	end

end

---------------------------------------------------------------------
--
-- Inventory Scrollbar Changed Handler
--
---------------------------------------------------------------------
function lstInventory_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)

	g_last_inv_track_pos = track_pos

	if g_b_loading_items == false then

		local num_items = 0
		-- determine how many icons need to be shown
		if listBoxHandle ~= 0 then
			num_items = ListBox_GetSize(listBoxHandle)
		end

        resetInvGiftIcons()

		for i = 0, 7 do

			if i < num_items then

            	local index = i + track_pos + 1 

                local item = g_t_items[index]

				-- show list icons
				local icon_name = "imgIcon" .. tostring(i + 1)
				local ich = Dialog_GetImage(gDialogHandle, icon_name)

				if ich ~= 0 and g_showIcons then

					Control_SetVisible(ich, true)

					-- set the icon


					local eh = Image_GetDisplayElement(ich)
					if eh ~= 0 then
                        local item = g_t_items[i + track_pos + 1]
						LoadIconTexture(g_game,item,eh,gDialogHandle);
					end
					
				
				else
					Control_SetVisible(ich, false)
				end

				ih = Dialog_GetImage(gDialogHandle, GIFT_ICON_INV_NAME..i+1)
			    setGiftIcon(item, ih)				

			end

			-- show list labels (second line)
			local label_name = "lblInventory" .. tostring(i + 1)
			local sh = Dialog_GetStatic(gDialogHandle, label_name)
			if sh ~= 0 then
			
                Control_SetVisible(sh, false)

			end
			
		end
	end
	
end

---------------------------------------------------------------------
--
-- Vendor Inventory Scrollbar Changed Handler
--
---------------------------------------------------------------------
function lstVendorInventory_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)

	g_last_vendor_track_pos = track_pos

	if g_b_loading_items == false then

		local num_items = 0
		-- determine how many icons need to be shown
		if listBoxHandle ~= 0 then
			num_items = ListBox_GetSize(listBoxHandle)
		end

		for i = 0, 7 do	
	
			-- show list icons
			local icon_name = "imgVendorIcon" .. tostring(i + 1)
			local ich = Dialog_GetImage(gDialogHandle, icon_name)
			if ich ~= 0 and g_showIcons then

				if i < num_items then

					Control_SetVisible(ich, true)

					-- set the icon

					vendor_item = g_t_vendor_items[i + track_pos + 1]

					local eh = Image_GetDisplayElement(ich)
					if eh ~= 0 and vendor_item ~= nil then
                        local item = g_t_vendor_items[i + track_pos + 1]
						LoadIconTexture(g_game,item,eh,gDialogHandle);
					end
				else
					Control_SetVisible(ich, false)

				end
			end

		
			-- show list labels (second line)
			local label_name = "lblVendor" .. tostring(i + 1)
			local sh = Dialog_GetStatic(gDialogHandle, label_name)
			if sh ~= 0 then

				Control_SetVisible(sh, false)

			end

		end
		
	end

end




---------------------------------------------------------------------
--
-- Get the value entered in the quantity field
--
-- ebName: String of textbox name
---------------------------------------------------------------------
function GetQuantity( ebName )

	-- default quantity if not specified will be 1
	local quantity = 1

	local ebh = Dialog_GetEditBox(gDialogHandle, ebName)
	if ebh ~= 0 then
		quantity_text = EditBox_GetText(ebh)
		if quantity_text ~= "" then
			quantity  = tonumber(quantity_text)
			if not quantity then
				quantity = 0
			end
		end
	end

	return quantity
end



function AddItemToInventory(t_item)

	local size = #g_t_items
	local found = false
	
	--See if item exists in current table
	for i=1,size do
	    --Does GLID match?
		if t_item["GLID"] == g_t_items[i]["GLID"] then
			--Does trade type match?
			if t_item["can_trade"] == g_t_items[i]["can_trade"] then
			
				--Match found update entry!
				
				local new_quantity = g_t_items[i]["quantity"] + t_item["quantity"]

				-- update the existing quantity
				g_t_items[i]["quantity"] = new_quantity
				
				found = true
				
				break	--Match found stop looping
							
			end 
			
		end		
	
	end

	--We didn't find it in the above search so add it
	if not found then
	
		table.insert(g_t_items, t_item)	
	
	end

	RebuildInventoryListBox()		
	
end

function RemoveItemFromInventory(t_item)

	local size = #g_t_items
	
	--See if item exists in current table
	for i=1,size do
	    --Does GLID match?
		if t_item["GLID"] == g_t_items[i]["GLID"] then
			--Does trade type match?
			if t_item["can_trade"] == g_t_items[i]["can_trade"] then
					
				--Match found update entry!			
				if t_item["quantity"] == 0 then
					
					table.remove(g_t_items, i)
					
				elseif t_item["quantity"] > 0 then
					
					-- update the existing quantity
					g_t_items[i]["quantity"] =  t_item["quantity"]
					
				end	

				break	--Match found stop looping
							
			end 
			
		end		
	
	end
	
	RebuildInventoryListBox()
    
end

function RebuildInventoryListBox()

	--Rebuild listbox with updated information
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	if lbh ~= 0 then

		ListBox_RemoveAllItems(lbh)
		local size = #g_t_items

		for i=1,size do
	
			local text = g_t_items[i]["name"].."\nQuantity: "..tostring(g_t_items[i]["quantity"])
			
			if g_t_items[i]["can_trade"] then
            	text = text.."  Sell Price: "..tostring(g_t_items[i]["sell_price"])
			end	
	
			ListBox_AddItem(lbh, text, g_t_items[i]["GLID"])		
		
		end
		
		local num_items = 0
		-- determine how many icons need to be shown
		if lbh ~= 0 then
			num_items = ListBox_GetSize(lbh)
		end

		for i = 0, 7 do
			
            -- show list icons
            local icon_name = "imgIcon" .. tostring(i + 1)
            local ich = Dialog_GetImage(gDialogHandle, icon_name)

			if i < num_items then
				
                local item = g_t_items[i+1]
								
				if g_showIcons and ich ~= 0 then

					Control_SetVisible(ich, true)

					-- set the icon

					local eh = Image_GetDisplayElement(ich)
					if eh ~= 0 then
                        local item = g_t_items[i + 1]
						LoadIconTexture(g_game,item,eh,gDialogHandle);
					end
					
				end
				
 				ih = Dialog_GetImage(gDialogHandle, GIFT_ICON_INV_NAME..i+1)
			    setGiftIcon(item, ih)
    			    
				-- show list labels (second line)
				local label_name = "lblInventory" .. tostring(i + 1)
				local sh = Dialog_GetStatic(gDialogHandle, label_name)
				if sh ~= 0 then

					Control_SetVisible(sh, false)
				
				end
                
			elseif ich ~= 0 then
            
                Control_SetVisible(ich, false)
            end
            
		end		

        local desclbl = Dialog_GetStatic(gDialogHandle, "lblInvDesc")

        index = ListBox_GetSelectedItemIndex( lbh )

        if index ~= -1 then
        
            item = g_t_items[index + 1]
        
            if item ~= nil then
            
                if desclbl ~= nil then
                    Static_SetText(desclbl, item["description"])
                end
                
                local ich = Dialog_GetImage(gDialogHandle, "imgItemPicInv")
                if ich ~= 0 and g_showIcons then
                
                    setImageState(ich, true)
                    -- set the icon
                    local eh = Image_GetDisplayElement(ich)
                    
                    if eh ~= 0 then
                        LoadIconTexture(g_game,item,eh,gDialogHandle);
                    end
                    
                end   
                
            else
            
                if desclbl ~= nil then
                    Static_SetText(desclbl, "")
                end
            end
            
        else
        
            if desclbl ~= nil then
                Static_SetText(desclbl, "")
            end
            
        end

	end

end

function setGiftIcon(item, icon_handle)

	if icon_handle ~= nil then

		if item["can_trade"] then
			Control_SetVisible(Image_GetControl(icon_handle), false)
		else
			Control_SetVisible(Image_GetControl(icon_handle), true)
		end

	end

end

---------------------------------------------------------------------
--
-- Update Inventory ListBox Icons
--
---------------------------------------------------------------------
function UpdateInventoryListBoxIcons(listBoxHandle)

	if listBoxHandle ~= 0 then

		local sbh = ListBox_GetScrollBar(listBoxHandle)
		if sbh ~= 0 then
			local page_size = ScrollBar_GetPageSize(sbh)
			local track_pos = ScrollBar_GetTrackPos(sbh)            			
			lstInventory_OnScrollBarChanged(sbh, listBoxHandle, page_size, track_pos)
		end
	end


end

---------------------------------------------------------------------
--
-- Left Arrow clicked - Buy Item
--
---------------------------------------------------------------------
function btnLeftArrow_OnButtonClicked(buttonHandle)

	-- try to buy item
	local lbh = Dialog_GetListBox(gDialogHandle, "lstVendorInventory")
	if lbh ~= 0 then

		local index = ListBox_GetSelectedItemIndex(lbh)
		if index > -1 then
		
			-- get the quantity to be purchased
			local quantity		= GetQuantity("txtQuantityStore")
	
			if quantity > 0 then	
				if quantity <= MAX_BUY_ITEMS then
					buyItem(index, quantity)
				else
					createMessage(MAX_BUY_ITEMS.." is the maximum number of items you can purchase in one transaction")
				end
			else
				createMessage("Please enter a positive quantity")
			end
		else
			createMessage("Please select an item to purchase")
		end
	end	

end

---------------------------------------------------------------------
--
-- Right Arrow clicked - Sell Item
--
---------------------------------------------------------------------
function btnRightArrow_OnButtonClicked(buttonHandle)

	-- try to sell item
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	if lbh ~= 0 then

		local index = ListBox_GetSelectedItemIndex(lbh)
		if index >= 0 then
        	if g_t_items[index+1]["can_trade"] then

				local reqQty = GetQuantity("txtQuantityInv")  --requested quantity
				local actQty = g_t_items[index + 1]["quantity"] --actual
			    if (reqQty <= actQty) and (reqQty > 0) then
	          		--showConfirmMenu( true )	
					sellItem()
				else
			    	createMessage("You do not have that many items")
				end
			else
				createMessage("This item cannot be traded or sold")
			end
		else
		    createMessage("Please select an item to sell")
		end

	end

end

function buyItem(index, quantity)

	-- Error handling done in the buy buttons' code
	-- BtnLeftArrow and btnGiftCard

	local GLID 		    = g_t_vendor_items[index+1]["GLID"]
	local cost          = g_t_vendor_items[index+1]["cost"]
	local sell_price    = g_t_vendor_items[index+1]["sell_price"]

	-- get the quantity to be purchased
	local quantity		= GetQuantity("txtQuantityStore")
	local total_cost	= quantity * cost


	-- if there are enough credits, then buy it (them)

	if total_cost <= g_credits and quantity > 0 then

		if g_game ~= nil then

			g_t_transaction["type"]  	= "buy"
			g_t_transaction["index"] 	= index
			g_t_transaction["new_quantity"]	= quantity
			g_t_transaction["sell_price"] = sell_price
			
            g_t_transaction["can_trade"] = true

			--EnableTransactionControls(false)

			ClientEngine_BuyItemFromStore(g_game, GLID, quantity, cred_type)

            g_credits = g_credits - total_cost
            
		end

		local ebh = Dialog_GetEditBox(gDialogHandle, "txtQuantityStore")
		if ebh ~= nil then
			EditBox_ClearText(ebh)
		end
		
	else
		if cred_type == GIFT_CREDITS then
			createMessage( "You can't afford this item with REWARDS.  Try purchasing with regular credits.")
		else
			createMessage( "You can't afford this item. ")		
		end				
	end	

end

function btnCancelSend_OnButtonClicked( buttonHandle)

   	showConfirmMenu( false )

end

function btnSendNow_OnButtonClicked( buttonHandle )

 	showConfirmMenu( false )
	sellItem()

end

function sellItem()

	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	if lbh ~= 0 then

	    local index = ListBox_GetSelectedItemIndex(lbh)
		if index >= 0 then

			local GLID 		= g_t_items[index+1]["GLID"]
			local tot_quantity 	= g_t_items[index+1]["quantity"]
			local sell_price = g_t_items[index+1]["sell_price"]
            local can_trade = g_t_items[index+1]["can_trade"]

			-- get the quantity to be sold
			local quantity		= GetQuantity("txtQuantityInv")

			-- if there is more than 1
			if tot_quantity > 1 then

				-- if there are enough units, then sell them
				if quantity <= tot_quantity and quantity > 0 then

					if g_game ~= nil then

						g_t_transaction["type"]  	= "sell"
						g_t_transaction["index"] 	= index
						g_t_transaction["sell_price"] = sell_price
                        g_t_transaction["can_trade"] = can_trade

						-- removed all of this type of item
						if quantity == tot_quantity then
							
							g_t_transaction["new_quantity"]	= 0

						else
						-- update the quantity

							local new_quantity = tot_quantity - quantity

							g_t_transaction["new_quantity"]	= new_quantity
						end

						--EnableTransactionControls(false)

						ClientEngine_SellItemToStore(g_game, GLID, quantity)
						
					    g_credits = g_credits + (sell_price * quantity)
					

					end

				end

			else
			-- just one
				if g_game ~= nil and quantity > 0 then

					g_t_transaction["type"]  	= "sell"
					g_t_transaction["index"] 	= index
					g_t_transaction["new_quantity"]	= 0
                    g_t_transaction["can_trade"] = can_trade
                    
					--showConfirmMenu(true)
                    --
					--EnableTransactionControls(false)
                    local ich = Dialog_GetImage(gDialogHandle, "imgItemPicInv")
                    setImageState(ich, false)
                            
					ClientEngine_SellItemToStore(g_game, GLID, 1)
					
					g_credits = g_credits + sell_price
				end

			end
			
			local ebh = Dialog_GetEditBox(gDialogHandle, "txtQuantityInv")
			if ebh ~= nil then
				EditBox_ClearText(ebh)
			end
		end
	end
	
	showConfirmMenu( false )
end

function btnClose_OnButtonClicked(buttonHandle)

	-- close this menu
	DestroyMenu(gDialogHandle)

end

---------------------------------------------------------------------
--
-- Inventory item selected
--
---------------------------------------------------------------------
function lstInventory_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)



	if bMouseDown == 0 then

		if GLID == g_prev_inventory_sel_GLID and g_prev_inventory_index == sel_index then

			ListBox_ClearSelection(listBoxHandle)
			g_prev_inventory_sel_GLID = nil
			g_prev_inventory_index = nil
			
		else
			g_prev_inventory_sel_GLID = GLID
			g_prev_inventory_index = sel_index

		end

		local ich = Dialog_GetImage(gDialogHandle, "imgItemPicInv")
		local i = sel_index+1
		if ich ~= 0 and #g_t_items > 0  and g_showIcons then
				--local ich = Dialog_GetImage(gDialogHandle, "imgItemPicInv")
			setImageState(ich, true)
			-- set the icon
			local eh = Image_GetDisplayElement(ich)
			if eh ~= 0 and g_t_items[i] ~= nil then
			
	            local item = g_t_items[i]
				LoadIconTexture(g_game,item,eh,gDialogHandle);
			end
		end
		local desclbl = Dialog_GetStatic(gDialogHandle, "lblInvDesc")
		if desclbl ~= nil then
			Static_SetText(desclbl, g_t_items[i]["description"])
		end	
	else

		-- StartDrag( listBoxHandle, sel_index )
	
	end

	if bFromKeyBoard == 1 then
		g_prev_inventory_sel_GLID = ListBox_GetItemData(listBoxHandle, sel_index)
		g_prev_inventory_index = sel_index
	end    
    
end

---------------------------------------------------------------------
--
-- Vendor Inventory item selected
--
---------------------------------------------------------------------
function lstVendorInventory_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	if bMouseDown == 0 then

		--Dialog_MsgBox("poii")

		local GLID = ListBox_GetItemData(listBoxHandle, sel_index)
		if GLID == g_prev_Vendor_inventory_sel_GLID and g_prev_Vendor_inventory_index == sel_index then

			ListBox_ClearSelection(listBoxHandle)
			g_prev_Vendor_inventory_sel_GLID = nil
			g_prev_Vendor_inventory_index = nil
		else
			g_prev_Vendor_inventory_sel_GLID = GLID
			g_prev_Vendor_inventory_index = sel_index
		end

		local ich = Dialog_GetImage(gDialogHandle, "imgItemPicStore")
		local i = sel_index+1
		if ich ~= 0 and g_showIcons then
			Control_SetVisible(ich, true)
			-- set the icon
			local eh = Image_GetDisplayElement(ich)
			if eh ~= 0 then
			
				local item = g_t_vendor_items[i]
				LoadIconTexture(g_game,item,eh,gDialogHandle);
			end
		end	
		local desclbl = Dialog_GetStatic(gDialogHandle, "lblStoreDesc")
		if desclbl ~= nil then
			Static_SetText(desclbl, g_t_vendor_items[i]["description"])
		end		
		
	else

		-- StartDrag( listBoxHandle, sel_index )
	
	end

	if bFromKeyBoard == 1 then
		g_prev_Vendor_inventory_sel_GLID = ListBox_GetItemData(listBoxHandle, sel_index)
		g_prev_Vendor_inventory_index = sel_index
	end
	
end


function setStaticState( staticHandle, state )

	if staticHandle ~= nil then
		ch = Static_GetControl(staticHandle)
		if ch ~= nil then
			Control_SetEnabled(ch, state)
			Control_SetVisible(ch, state)
		end
	end

end

function setImageState( imageHandle, state )

	if imageHandle ~= nil then
		ch = Image_GetControl(imageHandle)
		if ch ~= nil then
			Control_SetEnabled(ch, state)
			Control_SetVisible(ch, state)
		end
	end

end

-- Enable or disable the selected button
function setButtonState( buttonHandle, state )

	if buttonHandle ~= nil then
		ch = Button_GetControl(buttonHandle)
		if ch ~= nil then
			Control_SetEnabled(ch, state)
			Control_SetVisible(ch, state)
		end
	end

end
---------------------------------------------------------------------
--
-- Enable/Disable confirmation menu
--
-- show: Boolean to set state
--
---------------------------------------------------------------------
function showConfirmMenu( show )
 	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
--[[
	if lbh ~= 0 then

		local index = ListBox_GetSelectedItemIndex(lbh)
		if index ~= -1 then

			index = index + 1   -- lua and listbox index difference

			local bh = Dialog_GetButton(gDialogHandle, "btnSendNow")
			setButtonState(bh, show)

			bh = Dialog_GetButton(gDialogHandle, "btnCancelSend")
			setButtonState(bh, show)

			local sh = Dialog_GetStatic(gDialogHandle, "lblConfirmTitle")
			setStaticState(sh, show)

			sh = Dialog_GetStatic(gDialogHandle, "lblConfirmRecipient")
			setStaticState(sh, show)

		    sh = Dialog_GetStatic(gDialogHandle, "lblConfirmName")
			setStaticState(sh, show)

		    sh = Dialog_GetStatic(gDialogHandle, "lblConfirmPrice")
			setStaticState(sh, show)

		    sh = Dialog_GetStatic(gDialogHandle, "lblItemName")
			setStaticState(sh, show)
			if show then
			    if sh ~= nil then
					Static_SetText(sh, g_t_items[index]["name"])
			    end
			end

			sh = Dialog_GetStatic(gDialogHandle, "lblItemPrice")
			setStaticState(sh, show)
			if show then
			    if sh ~= nil then
					Static_SetText(sh, g_t_items[index]["sell_price"])
			    end
			end

			sh = Dialog_GetStatic(gDialogHandle, "lblRecipient")
			setStaticState(sh, show)
			if show then
			    if sh ~= nil then
		            Static_SetText(sh, GetQuantity("txtQuantityInv"))
				end
			end

		  	sh = Dialog_GetStatic(gDialogHandle, "lblConfirmSend")
			setStaticState(sh, show)

		    sh = Dialog_GetStatic(gDialogHandle, "lblCancelSend")
			setStaticState(sh, show)


			local ih = Dialog_GetImage(gDialogHandle, "imgConfirmBackground")
			setImageState(ih, show)
			

			ih = Dialog_GetImage(gDialogHandle, "imgConfirmThumb")
			setImageState(ih, show)
   			if ih ~= 0 then

				local eh = Image_GetDisplayElement(ih)
				if eh ~= 0 then

					Element_AddTexture(eh, gDialogHandle, g_t_items[index]["texture_name"])

					local left 	= g_t_items[index]["texture_coords"][1]
					local top 	= g_t_items[index]["texture_coords"][2]
					local right 	= g_t_items[index]["texture_coords"][3]
					local bottom	= g_t_items[index]["texture_coords"][4]

					Element_SetCoords(eh, left, top, right, bottom)
				end
			end

			bh = Dialog_GetButton(gDialogHandle, "btnRightArrow")
			if bh ~= nil then
				if show then
				    setButtonState(bh, false)
				    Control_SetVisible( bh, false )
				else
		            setButtonState(bh, true)
		            Control_SetVisible( bh, true )
				end
			end

		end
	end
]]--
end


---------------------------------------------------------------------
--
-- Enable/Disable controls for a transaction
--
---------------------------------------------------------------------
function EnableTransactionControls(bEnable)

	local eh = Dialog_GetEditBox(gDialogHandle, "txtQuantity")
	if eh ~= 0 then
		Control_SetEnabled(eh, bEnable)
	end

	local bh = Dialog_GetButton(gDialogHandle, "btnLeftArrow")
	if bh ~= 0 then
		Control_SetEnabled(bh, bEnable)
	end

	bh = Dialog_GetButton(gDialogHandle, "btnRightArrow")
	if bh ~= 0 then
		Control_SetEnabled(bh, bEnable)
	end
end
