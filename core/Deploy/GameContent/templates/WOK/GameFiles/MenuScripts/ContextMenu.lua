dofile("MenuHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("CommonFunctions.lua")
dofile("WebSuffix.lua")

-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--
-- end 

PLAYER_TARGETED_MENU_EVENT = "PlayerTargetedMenuEvent"
PRIVATE_MESSAGE_EVENT = "PrivateMessageEvent"
PLAYER_USERID_EVENT = "PlayerUserIdEvent"
TYPE_START_EVENT = "StartTypingEvent"
CLOSE_CONTEXT_EVENT = "CloseContextEvent"
PLAYER_PLAYERID_EVENT = "PlayerPlayerIdEvent"
OPEN_CONTEXT_EVENT = "OpenContextEvent"


GET_USERID_SUFFIX = "kgp/userProfile.aspx?action=getUserIdFromAvatarName&avatar="
UPDATE_SUFFIX = "kgp/updateFriend.aspx?userId="
ACTION_SUFFIX = "&action="
RAVE_SUFFIX = "kgp/ravePlayer.aspx?userId="
IGNORE_SUFFIX = "kgp/blockUser.aspx?action=Block&userId="
WEB_FEEDBACK_SUFFIX = "suggestions.aspx?mode=F&category=Feedback"

-- Used to determine which button requested html so we can give accurate result
-- descriptions for the selected action via a message box
BTN_ADD_FRIEND = "friend"
BTN_IGNORE = "ignore"
BTN_RAVE = "rave"
-- Not one of the buttons that requires a message box
BTN_NONE = "none"


g_playerTargetted = 0
g_game = nil
g_playerName = "" -- player clicked on
g_userID = 0
g_playerID = 0

g_returnData = ""

g_resultParse = false

g_buttonPressed = BTN_NONE

g_myName = "" --player looking at context menu

g_t_btnHandles = {}

--gDialogHandle = 0
---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "playerTargetedMenuEventHandler", 1,0,0,0, PLAYER_TARGETED_MENU_EVENT, KEP.HIGH_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "BrowserPageReadyHandler", 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "ContextCloseEventHandler", 1,0,0,0, CLOSE_CONTEXT_EVENT, KEP.HIGH_PRIO )
	--RegisterFilteredEventHandler(dispatcher, handler, "ContextCloseEventHandler", 1,0,0,0, CLOSE_CONTEXT_EVENT, CF.PLAYER_TARGET, 0, 1, KEP.HIGH_PRIO )    

end


function ContextCloseEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local oldHandle = Event_DecodeNumber(event)

	--Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "old handle... "..oldHandle )
	Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "context menu handle... "..gDialogHandle )

	if oldHandle == gDialogHandle then
		DestroyMenu(gDialogHandle)
	end

	return KEP.EPR_CONSUMED

end


---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
      Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, TYPE_START_EVENT, KEP.LOW_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, PLAYER_PLAYERID_EVENT, KEP.LOW_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, OPEN_CONTEXT_EVENT, KEP.MED_PRIO )
end

function playerTargetedMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	g_kanevaUserNetId  = Event_DecodeNumber( event )
	g_playerName = Event_DecodeString( event )
	g_game = Dispatcher_GetGame(dispatcher)

	local ebh = Dialog_GetStatic(gDialogHandle, "lblTitle")
	if (ebh ~= 0)  and (g_playerName ~= nil) then
		Static_SetText(ebh, g_playerName)
	end
	
	if(g_playerName == g_myName) then
		local ebh = Dialog_GetStatic(gDialogHandle, "btnIgnore")
		if (ebh ~= 0)  and (g_playerName ~= nil) then
			Static_SetText(ebh, "Titles")
		end
	end

	local web_address = GameGlobals.WEB_SITE_PREFIX..GET_USERID_SUFFIX..g_playerName

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, web_address, WF.TARGETED, 0, 0)
	end

	return KEP.EPR_OK
end

-- Catch the xml from the server
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter == WF.TARGETED then
	--	local sh = Dialog_GetStatic( gDialogHandle, "Static17" )
	
		g_returnData = Event_DecodeString( event )
	
	
		if g_resultParse == true then
		    ParseResults()
		else
			ParseUserID()
		end
	
	    -- changed to ok to make sure a new context menu receives this event
		-- (timing)
		return KEP.EPR_OK --KEP.EPR_CONSUMED
	end
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

    g_game = Dispatcher_GetGame(KEP.dispatcher)

    if g_game == 0 then
            g_game = nil
    end


	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
   
    local t = { ClientEngine_GetPlayerInfo(g_game) }
	if t ~= 0  then
	 	-- index 10 = name
		temp = t[10+1]
		if temp ~= nil then
			g_myName = temp
		end
    end

	--gDialogHandle = dialogHandle
	local e = Dispatcher_MakeEvent( KEP.dispatcher, OPEN_CONTEXT_EVENT )
	if e ~= 0 then
		Dispatcher_QueueEvent( KEP.dispatcher, e )
	end

	Dialog_BringToFront(gDialogHandle)
	
	getButtonHandles()

	-- TODO init here
end

function ParseResults()

	local result = nil  -- result description
	local code = nil 	-- return code
	local s, e = 0      -- start and end of captured string
	
	s, e, code = string.find(g_returnData, "<ReturnCode>(.-)</ReturnCode>")
	s, e, result = string.find(g_returnData, "<ResultDescription>(.-)</ResultDescription>")

	if result ~= nil then
	
	    if g_buttonPressed == BTN_RAVE then
			if result == "already raved" then
				createMessage(RM.ALREADY_RAVED..g_playerName)
	        end
	        if result == "success" then
	            createMessage(RM.RAVE..g_playerName)
			end

		elseif g_buttonPressed == BTN_ADD_FRIEND then

        	if code ~= "-1" then
				if result == "already friends" then
					createMessage(RM.ALREADY_FRIENDS..g_playerName)
			    elseif result == "success" then
					createMessage(RM.FRIEND_REQUEST..g_playerName)
				end
			else
				createMessage(RM.FRIEND_REQUEST_SENT..g_playerName)
			end

		elseif g_buttonPressed == BTN_IGNORE then

			if result == "success" then
				createMessage(RM.ADD_IGNORE..g_playerName)
			else
				createMessage(RM.ALREADY_IGNORE..g_playerName)
			end
			
		else
		   	createMessage(result)
		end

	end

	g_buttonPressed = BTN_NONE
	g_resultParse = false

end

function ParseUserID()

	local result = nil  	-- result description
	local s, e = 0      	-- start and end of captured string
	local sh = nil			-- static handle
	local dbPath = ""			-- path of picture on website, returned from database
	local fileName = "" 	-- name of image to apply

	s, e, result = string.find(g_returnData, "<ReturnCode>(%d+)</ReturnCode>")

	if result == "0" then
	
		-- Parse needed info from XML
		s, e, result = string.find(g_returnData, "<user_id>(%d+)</user_id>", e)
		g_userID = tonumber(result)
		
		s, e, result = string.find(g_returnData, "<player_id>(%d+)</player_id>", e)
		g_playerID = tonumber(result)
		
		s, e, dbPath = string.find(g_returnData, "<thumbnail_medium_path>(.-)</thumbnail_medium_path>", e)

		if not dbPath then
			dbPath = DEFAULT_AVATAR_PIC_PATH
		end		
	
		if g_userID > 0 and dbPath then
			fileName = ClientEngine_DownloadTexture( g_game, GameGlobals.FRIEND_PICTURE, g_userID, dbPath, GameGlobals.THUMBNAIL )

            -- Strip off begnning of file path leaving only file name
			fileName = string.gsub(fileName,".-\\", "" )
			
			local ihFore = Dialog_GetImage(gDialogHandle, "imgAvatar")
			local ihBack = Dialog_GetImage(gDialogHandle, "imgBackground")
			
			if ihFore then
				fileName = "../../CustomTexture/".. fileName
				-- Calls the scaleImage function from CommonFunctions in order to scale the profile pics
				--displayBrowserThumbnail( ihFore, fileName )	
				scaleImage(ihFore,ihBack,fileName)
	        		
	        end	
		end
		
	else
  		--createMessage("Could not obtain user ID")
	end
	if g_userID == 0 then
		ClientEngine_SendInteractCommand(g_game)
		DestroyMenu(gDialogHandle)
	end
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

---------------------------------------------------------------------
-- Button Handler
---------------------------------------------------------------------
function btnIgnore_OnButtonClicked( buttonHandle )

	if g_myName ~= g_playerName then
    --if I clicked on someone other than myself, go to block menu, else go to titles menu
		if g_userID then
			if g_game ~= nil then
				gotoMenu( "BlockMenu.xml", 1, 1 )
			end
			local ev = Dispatcher_MakeEvent( KEP.dispatcher, PLAYER_USERID_EVENT )
			Event_EncodeNumber( ev, g_userID)
			Event_EncodeString( ev, g_playerName)
			Event_EncodeNumber( ev, 1 ) -- try to kick if use context menu
			Dispatcher_QueueEvent( KEP.dispatcher, ev )

			DestroyMenu( gDialogHandle )
		end

 	else
		gotoMenu( "TitlesMenu.xml", 1, 1)
		local ev = Dispatcher_MakeEvent( KEP.dispatcher, PLAYER_PLAYERID_EVENT )
		Event_EncodeNumber( ev, g_playerID )
		Dispatcher_QueueEvent( KEP.dispatcher, ev )
		DestroyMenu( gDialogHandle )
	end
	
end


function btnRave_OnButtonClicked( buttonHandle )

	if g_userID ~= nil then

		local web_address = GameGlobals.WEB_SITE_PREFIX..RAVE_SUFFIX..g_userID

		-- Tell browser handler to use parse result function
        g_resultParse = true
		g_buttonPressed = BTN_RAVE

		-- Send rave to site, use 0 for current page and amount per page
		if (g_game ~= nil) then
			ClientEngine_GetBrowserPage(g_game, web_address, WF.TARGETED, 0, 0)
		end

	else
		createMessage("Unknown user")
	end

end

function btnAddFriend_OnButtonClicked( buttonHandle )

	if g_userID ~= nil then

	    if g_myName ~= g_playerName then

			local web_address = GameGlobals.WEB_SITE_PREFIX..UPDATE_SUFFIX..g_userID..ACTION_SUFFIX.."Add"

			-- Tell browser handler to use parse result function
			g_resultParse = true
			g_buttonPressed = BTN_ADD_FRIEND

			-- Send request to website, use 0 for current page and amount per page
			if (g_game ~= nil) then
				ClientEngine_GetBrowserPage(g_game, web_address, WF.TARGETED, 0, 0)
			end

		else
		    createMessage("You cannot not send yourself a friend request.")
		end
	else
		createMessage("Unknown user")
	end

end



function btnClose_OnButtonClicked( buttonHandle)
	DestroyMenu(gDialogHandle)
end

function btnReport_OnButtonClicked( buttonHandle)

	ClientEngine_LaunchBrowser( g_game, GameGlobals.WEB_SITE_PREFIX_KANEVA..WEB_FEEDBACK_SUFFIX )
	
end

function btnGift_OnButtonClicked( buttonHandle)




end


function btnProfile_OnButtonClicked( buttonHandle )

	if g_userID then
		if g_playerName~=""  then  
			local web_address = GameGlobals.WEB_SITE_PREFIX_KANEVA..WEB_CHANNEL_PATH..g_playerName..".people"
			ClientEngine_LaunchBrowser( g_game, web_address )
		end

		local ev = Dispatcher_MakeEvent( KEP.dispatcher, PLAYER_USERID_EVENT )
		Event_EncodeNumber( ev, g_userID)
		Event_EncodeString( ev, g_playerName)
		Dispatcher_QueueEvent( KEP.dispatcher, ev )

		DestroyMenu(gDialogHandle)
	else
		createMessage("Could not obtain profile!")
	end


end

---------------------------------------------------------------------
-- Trade Button Handler
---------------------------------------------------------------------

function btnTrade_OnButtonClicked( buttonHandle)

	local tradePlayer = "/trade "

	if g_game ~= nil and  g_playerName ~= nil then

	    if g_myName ~= g_playerName then

			tradePlayer = tradePlayer..g_playerName
			ClientEngine_SendChatMessage( g_game, g_chat_type, tradePlayer )

	        DestroyMenu(gDialogHandle)
        else
            createMessage("You cannot trade with yourself.")
        end
 	else
		createMessage("Trade attempt failed.")
	end

end


---------------------------------------------------------------------
-- Message Button Handler
---------------------------------------------------------------------

function btnMessage_OnButtonClicked( buttonHandle)
	local starttypemsgevent = Dispatcher_MakeEvent( KEP.dispatcher, TYPE_START_EVENT )	
	Event_EncodeString( starttypemsgevent, "/tell " .. g_playerName .. " " )
	Dispatcher_QueueEvent( KEP.dispatcher, starttypemsgevent )
	DestroyMenu(gDialogHandle)
end


---------------------------------------------------------------------

-- Keyboard Handler

---------------------------------------------------------------------

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)

      -- ESC key
      if key == 27 then

            -- close this menu

            DestroyMenu(gDialogHandle)
      end
end



---------------------------------------------------------------------
-- Mouse Over
---------------------------------------------------------------------
function getButtonHandles()
--g_t_btnHandles


	local ch = Dialog_GetControl(gDialogHandle, "btnClose")
	if ch ~= nil then
     	g_t_btnHandles["btnClose"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnMessage")
	if ch ~= nil then
     	g_t_btnHandles["btnMessage"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnRave")
	if ch ~= nil then
     	g_t_btnHandles["btnRave"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnGift")
	if ch ~= nil then 
     	g_t_btnHandles["btnGift"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnProfile")
	if ch ~= nil then
     	g_t_btnHandles["btnProfile"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnAddFriend")
	if ch ~= nil then
     	g_t_btnHandles["btnAddFriend"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnIgnore")
	if ch ~= nil then	
     	g_t_btnHandles["btnIgnore"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnReport")
	if ch ~= nil then
     	g_t_btnHandles["btnReport"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnTrade")
	if ch ~= nil then
     	g_t_btnHandles["btnTrade"] = ch
	end
	
end

function Dialog_OnMouseMove(dialogHandle, x, y)

    local sh = Dialog_GetStatic(dialogHandle, "lblInfo")

	if sh ~= nil then

		if Control_ContainsPoint(g_t_btnHandles["btnMessage"], x, y) ~= 0 then
			Static_SetText(sh, "Send Message")
		elseif Control_ContainsPoint(g_t_btnHandles["btnRave"], x, y) ~= 0 then
	    	Static_SetText(sh, "Rave This Person")
		elseif Control_ContainsPoint(g_t_btnHandles["btnGift"], x, y) ~= 0 then
	    	Static_SetText(sh, "Send a Gift")
		elseif Control_ContainsPoint(g_t_btnHandles["btnProfile"], x, y) ~= 0 then
	    	Static_SetText(sh, "View Profile")
		elseif Control_ContainsPoint(g_t_btnHandles["btnAddFriend"], x, y) ~= 0 then
	    	Static_SetText(sh, "Add to Friends List")
		elseif Control_ContainsPoint(g_t_btnHandles["btnIgnore"], x, y) ~= 0 then
	    	Static_SetText(sh, "Block This Person")
		elseif Control_ContainsPoint(g_t_btnHandles["btnReport"], x, y) ~= 0 then
	    	Static_SetText(sh, "Report This Person")
		elseif Control_ContainsPoint(g_t_btnHandles["btnClose"], x, y) ~= 0 then
	    	Static_SetText(sh, "Close Menu")
		elseif Control_ContainsPoint(g_t_btnHandles["btnTrade"], x, y) ~= 0 then
	    	Static_SetText(sh, "Trade With This Person")
		else
            Static_SetText(sh, " ")
		end
	end

end