dofile("MenuHelper.lua")
dofile("ChatHelper.lua")

g_chat_type = ctPrivate
PRIVATE_MESSAGE_EVENT = "PrivateMessageEvent"
PLAYER_INFO_NAME = 11 -- Jonny loves magik numbers
g_Dispatcher = nil
g_game = nil
g_netID = -1
g_targetPlayer = ""
---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "PMInitHandler", 1,0,0,0, PRIVATE_MESSAGE_EVENT, KEP.MED_PRIO )	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	g_Dispatcher = dispatcher
	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, PRIVATE_MESSAGE_EVENT, KEP.MED_PRIO )
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	
	Helper_Chat_OnCreate( dialogHandle )
	--g_game = Dispatcher_GetGame(KEP.dispatcher)
      --if g_game == 0 then
      --    	g_game = nil
      --end
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

---------------------------------------------------------------------
-- 'ENTER' key was pressed within txtChat
--  not using txtChat because diff event for
--  private.  This should work -- Jonny
---------------------------------------------------------------------
function txtPrivate_OnEditBoxString(editBoxHandle, password)
	local playerId = g_netID -- net assigned ID of recipient
	local to = g_targetPlayer -- name of recipient
	local msg = EditBox_GetText(editBoxHandle)
	
	--Dialog_MsgBox(playerId)
	
	-- send private message because g_chat_type == 3, private.
	if g_game ~= nil and msg ~= "" then
		local from = ""
		local distribution = "R" .. playerId .. ":" .. to
		local t = { ClientEngine_GetPlayerInfo(g_game) }
		if t ~= 0  then
			local num_args = #t
			from = t[PLAYER_INFO_NAME]
		end
		-- EditBox_SetText(editBoxHandle, "|Type - " .. g_chat_type .. "|Dist - " .. distribution .. "|From - " .. from .. "--" .. msg, false) 
		ClientEngine_SendChatMessageBroadcast( g_game, g_chat_type, msg, distribution, from )
	end

	-- close this menu
	DestroyMenu(gDialogHandle)
end

function PMInitHandler(dispatcher, fromNetid, event, eventid, filter, objectid)	
	g_netID = Event_DecodeNumber( event )
	g_targetPlayer = Event_DecodeString( event )
	if g_game ~= nil and gDialogHandle ~= nil then
		if g_targetPlayer ~= nil and g_targetPlayer ~= "" then
			Dialog_SetCaptionText(gDialogHandle, "Private Message to " .. g_targetPlayer )
		end
	end
end

