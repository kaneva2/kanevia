dofile("MenuHelper.lua")


YES_NO_BOX_EVENT = "YesNoBoxEvent"
YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"

g_filter = 0

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "YesNoBoxEventHandler", 1,0,0,0, YES_NO_BOX_EVENT, KEP.MED_PRIO )
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, Yes_NO_BOX_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, Yes_NO_ANSWER_EVENT, KEP.MED_PRIO )
end

function YesNoBoxEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local message = Event_DecodeString( event )
	local title = Event_DecodeString( event )

    g_filter = filter

 	sh = Dialog_GetStatic( gDialogHandle, "lblMessage" )
	Static_SetText( sh, message )

 	sh = Dialog_GetStatic( gDialogHandle, "lblTitle" )
	Static_SetText( sh, title )
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)
end

---------------------------------------------------------------------
-- Button Handler
---------------------------------------------------------------------

function btnYes_OnButtonClicked( buttonHandle)
	local answerEvent = Dispatcher_MakeEvent( KEP.dispatcher, YES_NO_ANSWER_EVENT )

	if g_filter ~= 0 then
        Event_SetFilter( answerEvent, g_filter)
    end

	Event_EncodeNumber( answerEvent, 1 )
	Dispatcher_QueueEvent( KEP.dispatcher, answerEvent )
	DestroyMenu(gDialogHandle)
end

function btnNo_OnButtonClicked( buttonHandle)
	local answerEvent = Dispatcher_MakeEvent( KEP.dispatcher, YES_NO_ANSWER_EVENT )
	Event_EncodeNumber( answerEvent, 0 )
	Dispatcher_QueueEvent( KEP.dispatcher, answerEvent )
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------

-- Keyboard Handler

---------------------------------------------------------------------

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)

      -- ESC key
      if key == 27 then
	        -- close this menu
            DestroyMenu(gDialogHandle)
      end
end


---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here

	Helper_Dialog_OnDestroy( dialogHandle )
end

