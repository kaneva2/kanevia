dofile("MenuHelper.lua")
dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\CMD.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\Progressive.lua")
dofile("..\\Scripts\\MovementIds.lua")
dofile("CommonFunctions.lua")
dofile("MenuLocation.lua")
dofile("..\\Scripts\\ClientEngineIds.lua")
dofile("WebSuffix.lua")

g_game=nil
g_forward={}
g_backward={}
g_url=""
g_typingin="None"
g_t_LocationSettings = {}
g_once=false
g_public={}
g_apartement={}
g_hangout={}

g_locationData = ""
g_parseResults = false
g_listvisible=false
URL_PUBLIC=GameGlobals.WEB_SITE_PREFIX.."starTravelZone.aspx?type=4&search="
URL_HOME=GameGlobals.WEB_SITE_PREFIX.."starTravelZone.aspx?type=3&search="
URL_HANGOUT=GameGlobals.WEB_SITE_PREFIX.."starTravelZone.aspx?type=6&search="
g_web_address = GameGlobals.WEB_SITE_PREFIX..GET_RAVES_SUFFIX

-- Virtual World Url stuff, comment shows what is encoded in event
VwUrlEvent = {}
-- from client to menu
VwUrlEvent.Urls				= 1		-- totalInHistory, countSending, [id,url]

-- requests from menu to client
VwUrlEvent.GetHistory		= 2		-- start index, count
VwUrlEvent.GetCurrent		= 3		-- no parms
-- gotos
VwUrlEvent.GotoUrl			= 4		-- url
VwUrlEvent.GotoHistory		= 5		-- index of item
VwUrlEvent.GotoPrev			= 6		-- no parms
VwUrlEvent.GotoNext			= 7		-- no parms
VwUrlEvent.GotoFavorite		= 8		-- index of item in fav list
-- favorites
VwUrlEvent.Favorites		= 9		-- returned from handler, count [id,type,depth,name,url]
VwUrlEvent.GetFavorites		= 10	-- start index, count, depth
VwUrlEvent.AddFavorite		= 11	-- type (folder/url), name, uses current url
VwUrlEvent.DeleteFavorite	= 12	-- index
VwUrlEvent.name			= "VwUrlEvent"

g_waitingOnFirstUrl = true
g_currentIndex = 1

--... manage favorites..., folders, move, etc.

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "BrowserPageReadyHandler", 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )
	ParentHandler_RegisterFilteredEventHandler( dispatcher, handler, "VwUrlEventHandler", 1,0,0,0, VwUrlEvent.name, VwUrlEvent.Urls, 0, 0, KEP.MED_PRIO )
	g_dispatcher = dispatcher
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )
	g_game = Dispatcher_GetGame(KEP.dispatcher)

end

---------------------------------------------------------------------
--
-- Called when a URL event is sent from client, maybe in reply 
-- to our request
--
---------------------------------------------------------------------
function VwUrlEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter == VwUrlEvent.Urls then
	
		if g_waitingOnFirstUrl then
			local count, index, url, totalInHistory

			-- TODO: use index and totalInHistory to enable/disable forward and back
			totalInHistory = Event_DecodeNumber( event )
			count = Event_DecodeNumber( event )
			if count > 0 then
				index = Event_DecodeNumber( event )
				g_currentIndex = index
				url = Event_DecodeString( event )
				
				local EditBoxHandle edtUrl = Dialog_GetEditBox(gDialogHandle, "urltextbox")
				EditBox_SetText( edtUrl, url, false )
			end
			g_waitingOnFirstUrl = false

			-- TEMP -- show first 10
			local ev = Dispatcher_MakeEvent( dispatcher, VwUrlEvent.name )		
			Event_SetFilter( ev, VwUrlEvent.GetHistory )
			Event_EncodeNumber( ev, 1 );
			Event_EncodeNumber( ev, 10 );
			Dispatcher_QueueEvent( dispatcher, ev )		

		 else
			-- TODO: handle other cases where get list of urls
			local lbh = Dialog_GetListBox( gDialogHandle, "lbUrls" )
			ListBox_RemoveAllItems(lbh)
			
			totalInHistory = Event_DecodeNumber( event )
			count = Event_DecodeNumber( event )
			if count > 0 then
				for  i = 1, count do
					index = Event_DecodeNumber( event )
					url = Event_DecodeString( event )
					if index == g_currentIndex then 
						ListBox_AddItem(lbh, '>'..url,0)            
					else
						ListBox_AddItem(lbh, ' '..url,0)            
					end
				end
			end
			
			
		end 
		
		return KEP.EPR_CONSUMED
	-- elseif filter == VwUrlEvent.Favorites then 		
	-- TODO: 
	end

	return KEP.EPR_OK
end

---------------------------------------------------------------------
--
-- Called when a BrowserPageReadyEvent is sent from the client
--
---------------------------------------------------------------------
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	 if filter==WF.BROWSER then
		g_locationData = Event_DecodeString( event )
		if g_parseResults then
			ParseResults()
		else
			ParseLocationData()
		end
		return KEP.EPR_CONSUMED
	 end
end

--Save the zoneindex and zoneinstance id into the back button.
function SaveIntoBackPage(zoneindex,zoneinstance)
	
	if ClientEngine_GetCurrentZoneIndex==nil then
	    Dispatcher_GetFunctions( KEP.dispatcher,KEP.GET_XWIN_FN)
	end
	
	if ClientEngine_GetPlayer == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	
	local game = Dispatcher_GetGameAsGetSet( KEP.dispatcher )
	g_backward = LoadBrowserPageInfo(g_game,"BrowserBackward.cfg",g_backward)
	lastelement=#g_backward
	g_backward[lastelement+1]={}
	g_backward[lastelement+1]["zone_index"]=zoneindex
	g_backward[lastelement+1]["zone_instanceid"]=zoneinstance
	SaveBrowserPageInfo(g_game,"BrowserBackward.cfg",g_backward,#g_backward)
end

--Save the zoneindex and zoneinstance id into the forward. button.
function SaveIntoForwardPage(zoneindex,zoneinstance)
	if ClientEngine_GetCurrentZoneIndex==nil then
	    Dispatcher_GetFunctions( KEP.dispatcher,KEP.GET_XWIN_FN)
	end
	
	if ClientEngine_GetPlayer == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	local game = Dispatcher_GetGameAsGetSet( KEP.dispatcher )
	lastelement=#g_forward;
	g_forward[lastelement+1]={}
	g_forward[lastelement+1]["zone_index"]=zoneindex
	g_forward[lastelement+1]["zone_instanceid"]=zoneinstance
	SaveBrowserPageInfo(g_game,"BrowserForward.cfg",g_forward,#g_forward)
end

function ParseResults()
    
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local code = ""  		-- return code frome query
	local desc = ""         -- return description of query

	Dialog_MsgBox(g_locationData)
	s, strPos, code = string.find(g_locationData, "<ReturnCode>(.-)</ReturnCode>")
	if code == "0" then

   		s, strPos, code = string.find(g_locationData, "<TotalNumberRecords>(.-)</TotalNumberRecords>",strPos)

		if code=="0" then
        	Dialog_MsgBox("404 error,Location not found.")
		else

			--Read the zone index and instance id.
			s, strPos, code = string.find(g_locationData, "<zone_index>(.-)</zone_index>",strPos)
            local zoneindex=tonumber(code)

			s, strPos, code = string.find(g_locationData, "<zone_instance_id>(.-)</zone_instance_id>",strPos)
			if code==nil then
				code="0"
			 end
			 
			local zoneinstance=tonumber(code)
			

		if g_once==false then
			--Save Location we are in before teleporting.
             SaveIntoBackPage(zoneindex,zoneinstance)
        	g_once=false
        end
				
			--Now teleport myself there.
			local ev = Dispatcher_MakeEvent( g_dispatcher, KEP.GOTOPLACE_EVENT_NAME )
			Event_SetFilter( ev, KEP.GOTO_ZONE )
			Event_EncodeNumber( ev, zoneindex)
			Event_EncodeNumber( ev, zoneinstance)
			Event_EncodeNumber( ev, -1 )
			Event_AddTo( ev, KEP.SERVER_NETID )
			Dispatcher_QueueEvent( g_dispatcher, ev )
		end
	end
end

--Parse the location of the zone we are in.
function ParseLocationData()
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp


	s, strPos, result = string.find(g_locationData, "<ReturnCode>(.-)</ReturnCode>")
	if result == "0" then

		s, strPos, result = string.find(g_locationData, "<NumRaves>(.-)</NumRaves>", strPos)
		g_t_LocationSettings["NumRaves"] = tonumber(result)

		s, strPos, result = string.find(g_locationData, "<FriendlyName>(.-)</FriendlyName>", strPos)
		g_t_LocationSettings["Name"] = result

		s, strPos, result = string.find(g_locationData, "<AlreadyRaved>(.-)</AlreadyRaved>", strPos)
		g_t_LocationSettings["hasRaved"] = tonumber(result)

		s, strPos, result = string.find(g_locationData, "<isPrivate>(.-)</isPrivate>", strPos)
		g_t_LocationSettings["isPrivate"] = tonumber(result)

		s, strPos, result = string.find(g_locationData, "<AllowFriends>(.-)</AllowFriends>", strPos)
        g_t_LocationSettings["AllowFriends"] = tonumber(result)

		s, strPos, result = string.find(g_locationData, "<AllowMembers>(.-)</AllowMembers>", strPos)
        g_t_LocationSettings["AllowMembers"] = tonumber(result)

		s, strPos, result = string.find(g_locationData, "<AllowAll>(.-)</AllowAll>", strPos)
        g_t_LocationSettings["AllowAll"] = tonumber(result)

		s, strPos, result = string.find(g_locationData, "<GM>(.-)</GM>", strPos)
        g_t_LocationSettings["GM"] = tonumber(result)

		s, strPos, result = string.find(g_locationData, "<Moderator>(.-)</Moderator>", strPos)
        g_t_LocationSettings["Moderator"] = tonumber(result)

		s, strPos, result = string.find(g_locationData, "<Owner>(.-)</Owner>", strPos)
        g_t_LocationSettings["Owner"] = tonumber(result)

		s, strPos, result = string.find(g_locationData, "<Apartment>(.-)</Apartment>", strPos)
        g_t_LocationSettings["Apartment"] = tonumber(result)

		s, strPos, result = string.find(g_locationData, "<Hangout>(.-)</Hangout>", strPos)
        g_t_LocationSettings["Hangout"] = tonumber(result)

		location=""
		if g_t_LocationSettings["Apartment"]==1 then
			location="Home"
  		else
  		if g_t_LocationSettings["Hangout"] ==1 then
  		    location="Hangout"
		else
		    location="Public"
		end
		end


		find=string.find(g_t_LocationSettings["Name"],"-")
		
		if find~=nil then
	  		lastname = string.sub(g_t_LocationSettings["Name"],1,find-1)
			g_url = "vwtp://star.kaneva.com/"..location.."/"..lastname
  		else
  		    g_url = "vwtp://star.kaneva.com/"..location.."/"..g_t_LocationSettings["Name"]
		end
		
  		local EditBoxHandle url = Dialog_GetEditBox(gDialogHandle, "urltextbox")
		EditBox_SetText( url,g_url, false )
		Control_SetEnabled(url,true)
		g_parseResults=true
    end


end


---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate(dialogHandle)
	local buttonback = Dialog_GetButton(gDialogHandle,"btnBack");
	local buttonforward = Dialog_GetButton(gDialogHandle,"btnForward")
	-- Dialog_SetLocation(gDialogHandle,80,0)
	-- TODO: 
	-- Control_SetEnabled(buttonback,false)
	-- Control_SetEnabled(buttonforward,false)
    
	if ClientEngine_GetBrowserPage == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end


  	local EditBoxHandle url = Dialog_GetEditBox(gDialogHandle, "urltextbox")
	EditBox_SetText( url,"Loading...", false )
	-- Control_SetEnabled(url,false)

	local ev = Dispatcher_MakeEvent( g_dispatcher, VwUrlEvent.name )		
	Event_SetFilter( ev, VwUrlEvent.GetCurrent )
	Dispatcher_QueueEvent( g_dispatcher, ev )		
	
	--Load the forward and backward page for the browser.
	-- g_backward = LoadBrowserPageInfo(g_game,"BrowserBackward.cfg",g_backward)
 	-- g_forward =  LoadBrowserPageInfo(g_game,"BrowserForward.cfg",g_forward)
	

	-- if #g_forward~=0 then
	--		Control_SetEnabled(buttonforward,true)
	-- end
	
	-- if #g_backward~=0 then
	-- 	Control_SetEnabled(buttonback,true);
	-- end
	

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy(dialogHandle) -- MUST call this, otherwise crash client
end

--Clicking the search button
function btnSearch_OnButtonClicked( buttonHandle )
	local EditBoxHandle search = Dialog_GetEditBox(gDialogHandle, "searchtextbox")
	local txt = EditBox_GetText(search)
	local lbh=Dialog_GetListBox(gDialogHandle,"searchlistbox")
	if txt~="" then
		PerformSearch()
  		g_listvisible=not g_listvisible
  		Control_SetVisible(lbh,g_listvisible)
	else
		g_listvisible=false
		Control_SetVisible(lbh,g_listvisible)
	end
end

--Performs the search to find all the names that matches.
function PerformSearch()
	
end


--Check and see if the url has focus
function urltextbox_OnFocusIn(editBoxHandle)
	if gDialogHandle ~= nil then
		g_typingin="Browser"
		Dialog_SetModal(gDialogHandle, true)
		Dialog_BringToFront(gDialogHandle)
	end
end

--Check and see if the url lost focus.
function urltextbox_OnFocusOut(editBoxHandle)
	if gDialogHandle ~= nil then
		g_typingin="None"
		Dialog_SetModal(gDialogHandle, false)
		Dialog_BringToFront(gDialogHandle)
	end
end

--Check and see if the url has focus
function searchtextbox_OnFocusIn(editBoxHandle)
	if gDialogHandle ~= nil then
		g_typingin="Search"
		Dialog_SetModal(gDialogHandle, true)
		Dialog_BringToFront(gDialogHandle)
	end
end

--Check and see if the url lost focus.
function searchtextbox_OnFocusOut(editBoxHandle)
	if gDialogHandle ~= nil then
		g_typingin="None"
		Dialog_SetModal(gDialogHandle, false)
		Dialog_BringToFront(gDialogHandle)
	end
end

function searchtextbox_OnEditBoxKeyDown(dialogHandle, key, bShiftDown, bControlDown, bAltDown)
	if key==13 then
		local lbh=Dialog_GetButton(g_DialogHandle,"btnSearch")
		btnSearch_OnButtonClicked( lbh )	    
	end
end

function urltextbox_OnEditBoxKeyDown(dialogHandle, key, bShiftDown, bControlDown, bAltDown)

	if key==13 then

		local EditBoxHandle edtUrl = Dialog_GetEditBox(gDialogHandle, "urltextbox")
		local url = EditBox_GetText(edtUrl)
		gotoUrl( url )
	
--  	g_once=true
-- 		local EditBoxHandle url = Dialog_GetEditBox(gDialogHandle, "urltextbox")
-- 		g_url=EditBox_GetText(url)
-- 		local s1=string.sub(g_url,string.find(g_url,"//")+2,string.len(g_url))
-- 		local s2=string.sub(s1,string.find(s1,"/")+1,string.len(s1))
-- 		local locationname=string.sub(s2,string.find(s2,"/")+1,string.len(s2))	
-- 		local protocol=string.sub(g_url,1,string.find(g_url,"//")-2)	
-- 		local servername=string.sub(s1,1,string.find(s1,"/")-1)
-- 		local placetype=string.sub(s2,1,string.find(s2,"/")-1)
-- 	  	local EditBoxHandle url = Dialog_GetEditBox(gDialogHandle, "urltextbox")
-- 
-- 		--Map the Url.
-- 		placetype = string.upper(placetype)
-- 		if placetype=="PUBLIC" then
-- 		  g_web_address=URL_PUBLIC..locationname
-- 		else
-- 		if placetype=="HANGOUT" then
-- 		  g_web_address=URL_HANGOUT..locationname
-- 		else
-- 		if placetype=="HOME" then
--           g_web_address=URL_HOME..locationname
-- 		end
-- 		end
-- 		end	
-- 		
-- 
-- 		if (g_game ~= nil) then
-- 			ClientEngine_GetBrowserPage(g_game, g_web_address,WF.BROWSER, 0, 0)
-- 		end
	end	
end


function trim (s)
      return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end

-- validate URL, then fire event to go there
function gotoUrl( url )

	-- TODO: more validation here, since we can popup a message box
	-- make sure right format:  stp://<game>/...
	-- if http:// could popup browser :)
	url = trim(url)
	if string.len(url) == 0 then
		-- TODO
		return
	end

	-- assume valid url if get here
	local ev = Dispatcher_MakeEvent( g_dispatcher, VwUrlEvent.name )		
	Event_SetFilter( ev, VwUrlEvent.GotoUrl )
	Event_EncodeString( ev, url )
	Dispatcher_QueueEvent( g_dispatcher, ev )		
	
end 

function btnHome_OnButtonClicked(buttonHandle)

	if ClientEngine_GetPlayer == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	
	if ClientEngine_GetCurrentZoneIndex == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	
	--Save the current page into the backward button.	
	local game = Dispatcher_GetGameAsGetSet( KEP.dispatcher )
	zoneindex=GetSet_GetNumber(game, ClientEngineIds.CURRENTWORKINGZONEINDEX)
	zoneinstance=GetSet_GetNumber(game, ClientEngineIds.ZONEINSTANCEID)
	SaveIntoBackPage(zoneindex,zoneinstance)
	

	
	
	--Teleport me to my home.
	event = Dispatcher_MakeEvent( KEP.dispatcher, "TextEvent" )
	g_game = Dispatcher_GetGame(KEP.dispatcher)
	local player = ClientEngine_GetPlayer( g_game )
	local playerId = GetSet_GetNumber( player, MovementIds.NETWORKDEFINEDID )

	Event_EncodeNumber( event, playerId )
	Event_EncodeString( event, "/GOHOME")
	Event_EncodeNumber( event, ChatType.Talk )
	Event_SetFilter( event, CMD.GOHOME )
	Event_AddTo( event, KEP.SERVER_NETID )	
	Dispatcher_QueueEvent( KEP.dispatcher, event )
	
	
end

--Clicking on the browser back button.
function btnBack_OnButtonClicked(buttonHandle)

	local ev = Dispatcher_MakeEvent( g_dispatcher, VwUrlEvent.name )		
	Event_SetFilter( ev, VwUrlEvent.GotoPrev )
	Event_EncodeString( ev, url )
	Dispatcher_QueueEvent( g_dispatcher, ev )		
	
-- 	if ClientEngine_GetCurrentZoneIndex==nil then
-- 	    Dispatcher_GetFunctions( KEP.dispatcher,KEP.GET_XWIN_FN)
-- 	end
-- 	
-- 	if ClientEngine_GetPlayer == nil then 
-- 		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
-- 	end
-- 		
-- 	--Add the zone index to our forward button. 	
-- 	local game = Dispatcher_GetGameAsGetSet( KEP.dispatcher )
-- 	zoneindex=GetSet_GetNumber(game, ClientEngineIds.CURRENTWORKINGZONEINDEX)
-- 	zoneinstance=GetSet_GetNumber(game, ClientEngineIds.ZONEINSTANCEID)
-- 	SaveIntoForwardPage(zoneindex,zoneinstance)
-- 	
-- 	--remove the last index in our backward window.
-- 	local backzoneindex=g_backward[#g_backward]["zone_index"]
-- 	local zoneinstanceid=g_backward[#g_backward]["zone_instanceid"]
-- 	
-- 	--Remove the last index from the backward button.
-- 	size = #g_backward
-- 	table.remove(g_backward[size],1)
-- 	table.remove(g_backward[size],2)
-- 	table.remove(g_backward,size)	
-- 	SaveBrowserPageInfo(g_game,"BrowserBackward.cfg",g_backward,#g_backward)
-- 	
-- 	
-- 	--Now get the zone we want to teleport ourselves too.	
-- 	local ev = Dispatcher_MakeEvent( g_dispatcher, KEP.GOTOPLACE_EVENT_NAME )
-- 	Event_SetFilter( ev, KEP.GOTO_ZONE )
-- 	Event_EncodeNumber( ev, backzoneindex)
-- 	Event_EncodeNumber( ev, zoneinstanceid)
-- 	Event_EncodeNumber( ev, -1 ) 
-- 	Event_AddTo( ev, KEP.SERVER_NETID )
-- 	Dispatcher_QueueEvent( g_dispatcher, ev )		
end

--Clicking on the browser forward button.
function btnForward_OnButtonClicked(buttonHandle)

	local ev = Dispatcher_MakeEvent( g_dispatcher, VwUrlEvent.name )		
	Event_SetFilter( ev, VwUrlEvent.GotoNext )
	Event_EncodeString( ev, url )
	Dispatcher_QueueEvent( g_dispatcher, ev )		

-- 	if ClientEngine_GetCurrentZoneIndex==nil then
-- 	    Dispatcher_GetFunctions( KEP.dispatcher,KEP.GET_XWIN_FN)
-- 	end
-- 
-- 	if ClientEngine_GetPlayer == nil then
-- 		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
-- 	end
-- 
-- 	--Save the current page into the backward button.	
-- 	local game = Dispatcher_GetGameAsGetSet( KEP.dispatcher )
-- 	zoneindex=GetSet_GetNumber(game, ClientEngineIds.CURRENTWORKINGZONEINDEX)
-- 	zoneinstance=GetSet_GetNumber(game, ClientEngineIds.ZONEINSTANCEID)
-- 	SaveIntoBackPage(zoneindex,zoneinstance)
-- 
-- 	--remove the last index in our forward button.
-- 	local forwardzoneindex=g_forward[#g_forward]["zone_index"]
-- 	local zoneinstanceid=g_forward[#g_forward]["zone_instanceid"]
-- 
-- 	--Remove the last index from the forward button.
-- 	size = #g_forward
-- 	table.remove(g_forward[size],1)
-- 	table.remove(g_forward[size],2)
-- 	table.remove(g_forward,size)
-- 	SaveBrowserPageInfo(g_game,"BrowserForward.cfg",g_forward,#g_forward)
-- 
-- 
-- 	--Now get the zone we want to teleport ourselves too.
-- 	local ev = Dispatcher_MakeEvent( g_dispatcher, KEP.GOTOPLACE_EVENT_NAME )
-- 	Event_SetFilter( ev, KEP.GOTO_ZONE )
-- 	Event_EncodeNumber( ev, forwardzoneindex)
-- 	Event_EncodeNumber( ev, zoneinstanceid)
-- 	Event_EncodeNumber( ev, -1 )
-- 	Event_AddTo( ev, KEP.SERVER_NETID )
-- 	Dispatcher_QueueEvent( g_dispatcher, ev )
end




function Dialog_OnMouseMove(dialogHandle, x, y)

end
