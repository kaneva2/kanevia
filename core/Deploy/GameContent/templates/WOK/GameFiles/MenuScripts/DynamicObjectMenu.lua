dofile("MenuHelper.lua")
dofile("..\\Scripts\\BrowserIds.lua")
dofile("..\\Scripts\\MovementIds.lua")
dofile("..\\Scripts\\CameraObjIds.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("CommonFunctions.lua")

-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--
-- end 
DYNAMIC_OBJ_MENU_EVENT = "DynamicObjectMenuEvent"
PLAYLIST_MENU_EVENT = "PlayListMenuEvent"
FLASHFRAME_MENU_EVENT = "FlashFrameMenuEvent"
TIMER_EVENT = "JonnysTimerEvent"
CLOSE_DYNMENU_EVENT = "CloseDynMenuEvent"
INFO_BOX_EVENT = "InfoBoxEvent"

OBJ_MOVE_AMT = .2
OBJ_ROT_AMT = 5
OBJ_SHIFT_MOVE_AMT = 10

FLASH_MAX_VOL = 65535
FLASH_VOL_STEPS = 15

--genereated in OnCreate
g_t_TV_volumes = {}
g_t_TV_vol_rev = {}

g_cur_flash_vol = #g_t_TV_volumes

-- id of the dynamic placement obj
g_objId = -1
g_kanevaUserId = -1
g_Dispatcher = nil
g_objName = nil
g_objCanPlayMovie = 0
g_objIsAttachable = 0
g_objDescription = "None"

g_game = nil
g_dispatcher = nil
g_confirmation = 0

g_mov_dir = 0

g_vol = 50

g_tv = true

-- indicates the state of the arrow buttons.
CLEAR 		= 0	-- No arrow is down.
CLICKED_LEFT 	= 1	-- Left arrow is clicked.
CLICKED_RIGHT 	= 2	-- Right arrow is clicked.
HELD_LEFT 	= 3	-- Left arrow is held down for sustained period.
HELD_RIGHT 	= 4	-- Right arrow is held down for sustained period.

g_forcon = nil
g_backcon = nil
g_leftcon = nil
g_rightcon = nil
g_upcon = nil
g_downcon = nil
g_rotrightcon = nil
g_rotleftcon = nil 
g_repeatpattern = nil

g_x = 0
g_y = 0
g_mouseholdevent = nil

g_t_btnHandles = {}

g_ShiftMultiple = 1

g_lasttexture = nil

g_isFlashFrame = false

g_hasplaylist = true;

function generateLookupTables()

	local step = FLASH_MAX_VOL / FLASH_VOL_STEPS

	g_t_TV_volumes[1] = 0
	g_t_TV_vol_rev["0"] = 0

	for i=2,FLASH_VOL_STEPS do
		g_t_TV_volumes[i] = step * i
		g_t_TV_vol_rev[tostring(step * i)] = i - 1
	end
	
end

function showVolume()


	local result, isOk, vol

	isOk, vol = ClientEngine_GetFlashVolumeOnDynamicObject( g_game, g_objId )
	
	---Dialog_MsgBox("isOk: "..tostring(isOk).."  vol: "..tostring(vol))

	local sh = Dialog_GetStatic(gDialogHandle, "stVol")
	if isOk == 1.0  and vol > 0 then
	    vol = tostring( g_t_TV_vol_rev[tostring(vol)] )
		Static_SetText( sh, tostring(vol) )
	else
		Static_SetText( sh, "0" )  -- error, so no volume
	end

	return tonumber(vol)

end

function getCurrentRave()
	local result, isOk, ret
	ret = 1 -- default to true, indicating they can't rave
	local state = false
	
	isOk, result = ClientEngine_CallFlashFunctionOnDynamicObject( g_game, g_objId, "<invoke name=\"GetRaveState\" returntype=\"xml\"><arguments/></invoke>" )
	local btn = Button_GetControl( Dialog_GetButton(gDialogHandle, "btnRave") )
	if isOk == 1.0 then
		-- parse out the text
		-- result is <false/> or <true/>
		if string.find( result, "false") ~= nil then
			--Control_SetEnabled( btn, true )
			state = true
			ret = 0
			Static_SetText(Button_GetStatic(btn), "Rave Video")
		else
			Static_SetText(Button_GetStatic(btn), "Already Raved")
		end	
	end

	Control_SetEnabled( btn, state )

	return ret
end

function HideHelp()
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static2"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static3"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static4"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static5"), false )
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image1"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image2"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image3"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image4"),false);
end

function btnHelp_OnButtonClicked(buttonHandle)
	g_help=not g_help
	if g_hasplaylist== true then
    	Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static2"), g_help )
    	Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image1"),g_help);
    end
    
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static3"), g_help )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static4"), g_help )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static5"), g_help )

    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image2"),g_help);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image3"),g_help);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image4"),g_help);
end

function MoveHelpStatic(x,y,name)
	bc=Dialog_GetStatic(gDialogHandle,name);
	Control_SetLocation(bc,x, y)
end

function MoveHelpImage(x,y,name)
	bc=Dialog_GetImage(gDialogHandle,name);
	Control_SetLocation(bc,x, y)
end

function HelpStaticState(name,state)
  	bc=Dialog_GetStatic(gDialogHandle,name);
	Control_SetVisible(bc,state)
end

function HelpImageState(name,state)
  	bc=Dialog_GetImage(gDialogHandle,name);
	Control_SetVisible(bc,state)
end

function updateId(dispatcher, fromNetid, event, eventid, filter, objectid)
	local result, isOk
	g_objId  = Event_DecodeNumber( event );
	g_objName = Event_DecodeString( event );
	g_objCanPlayMovie = Event_DecodeNumber( event );
	g_objIsAttachable = Event_DecodeNumber( event );
    g_objDescription = Event_DecodeString( event );

	Static_SetText(Dialog_GetStatic(gDialogHandle, "Static1"), g_objName)
		
   	Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, ">>>>>setting id = "..g_objId ) 	

	if g_objCanPlayMovie ~= 1.0 then
		g_hasplaylist =  false;
		g_tv = false
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnPlaylist"))
		Control_SetVisible(bc, false)
		Control_SetEnabled(bc, false)
		HelpStaticState("Static2",false);
		HelpImageState("Image1",false);
		
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnRave"))
		Control_SetVisible(bc, false)
		Control_SetEnabled(bc, false)
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnVolDn"))
		Control_SetVisible(bc, false)
		Control_SetEnabled(bc, false)
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnVolUp"))
		Control_SetVisible(bc, false)
		Control_SetEnabled(bc, false)
		sc = Static_GetControl(Dialog_GetStatic(gDialogHandle, "Static21"))
		Control_SetVisible(sc, false)
		sc = Static_GetControl(Dialog_GetStatic(gDialogHandle, "stVol"))
		Control_SetVisible(sc, false)
		
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnChangeTexture"))
		Control_SetLocation(bc, 10, 15)
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnResetTexture"))
		Control_SetLocation(bc, 10, 45)
		MoveHelpImage(-225,-25,"Image2")
		MoveHelpStatic(-225,-25,"Static5")
		
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnRepeatTexture"))
		if g_lasttexture ~= nil then
			Control_SetLocation(bc, 10, 75)
		else
			Control_SetVisible(bc, false)
		end
		
	else
		g_tv = true
		g_hasplaylist = true;
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnPlaylist"))
		Control_SetVisible(bc, true)
		Control_SetEnabled(bc, true)
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnRave"))
		Control_SetVisible(bc, true)
		Control_SetEnabled(bc, true)
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnVolDn"))
		Control_SetVisible(bc, true)
		Control_SetEnabled(bc, true)
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnVolUp"))
		Control_SetVisible(bc, true)
		Control_SetEnabled(bc, true)
		sc = Static_GetControl(Dialog_GetStatic(gDialogHandle, "Static21"))
		Control_SetVisible(sc, true)
		sc = Static_GetControl(Dialog_GetStatic(gDialogHandle, "stVol"))
		Control_SetVisible(sc, true)
		
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnChangeTexture"))
		Control_SetLocation(bc, 10, 170)
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnResetTexture"))
		Control_SetLocation(bc, 10, 200)
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnRepeatTexture"))
		if g_lasttexture ~= nil then
			Control_SetLocation(bc, 10, 230)      
		else
			Control_SetVisible(bc, false)
		end
		
		local bs = Button_GetStatic(Dialog_GetButton(gDialogHandle, "btnPlaylist"))
		
		local index = string.find(g_objName, "Flash Frame")

		if index ~= nil then
			Static_SetText(bs, "Change Widget")	
			g_isFlashFrame = true		
		else
			Static_SetText(bs, "Change Media")
		end
	end

 	getConfig()
 	
	if g_tv == true then
		g_cur_flash_vol = showVolume()
		getCurrentRave()
	end


	
	return KEP.EPR_OK
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "updateId", 1,0,0,0, DYNAMIC_OBJ_MENU_EVENT, KEP.HIGH_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "confirmationCloseHandler", 1,0,0,0, "ConfirmationCloseEvent", KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "clickHoldHandler", 1,0,0,0, TIMER_EVENT, KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "CloseDynObjHandler", 1,0,0,0, CLOSE_DYNMENU_EVENT, KEP.HIGH_PRIO )

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "ConfirmationCloseEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "TextureSelectionEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, DYNAMIC_OBJ_MENU_EVENT, KEP.MED_PRIO )	
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, PLAYLIST_MENU_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, TIMER_EVENT, KEP.MED_PRIO )
	g_Dispatcher = dispatcher

end

function CloseDynObjHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	--Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "brgin event handler... "..filter )
	local oldHandle = Event_DecodeNumber(event)


	if oldHandle == gDialogHandle then
		DestroyMenu(gDialogHandle)
	end

	return KEP.EPR_CONSUMED

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
       	HideHelp()
  	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end	

	if KEPConfig_GetString == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end
	
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	if g_game == 0 then
		g_game = nil
	end

	g_forcon = 		Button_GetControl(Dialog_GetButton(gDialogHandle, "btnMoveForward"))
	g_backcon = 	Button_GetControl(Dialog_GetButton(gDialogHandle, "btnMoveBack"))
	g_leftcon = 	Button_GetControl(Dialog_GetButton(gDialogHandle, "btnMoveLeft"))
	g_rightcon = 	Button_GetControl(Dialog_GetButton(gDialogHandle, "btnMoveRight"))
	g_upcon = 		Button_GetControl(Dialog_GetButton(gDialogHandle, "btnMoveUp"))
	g_downcon = 	Button_GetControl(Dialog_GetButton(gDialogHandle, "btnMoveDown"))
	g_rotrightcon = 	Button_GetControl(Dialog_GetButton(gDialogHandle, "btnRotateRight"))
	g_rotleftcon = 	Button_GetControl(Dialog_GetButton(gDialogHandle, "btnRotateLeft"))
	g_mouseholdevent = Dispatcher_MakeEvent( g_Dispatcher, TIMER_EVENT )
	g_repeatpattern = 	Button_GetControl(Dialog_GetButton(gDialogHandle, "btnRepeatTexture"))
	Dispatcher_QueueEventInFuture( g_Dispatcher, g_mouseholdevent, 1 )
	
	getButtonHandles()
	
	generateLookupTables()

	if ClientEngine_SetMenuSelectMode ~= nil then
		ClientEngine_SetMenuSelectMode( g_game, 1.0 )
	end

	if KEPFile_ReadLine == nil then 
      Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPFILE_FN )
    end
    
	local f, errMsg  = ClientEngine_OpenFile( g_game, "lastasset.dat", "r")	
	if( f ~= nil) then
		local err, errMsg
		err, g_lasttexture, errMsg = KEPFile_ReadLine( f )
		KEPFile_CloseAndDelete( f )
		--Control_SetVisible(g_t_btnHandles["btnRepeatTexture"], true)
	else
		g_lasttexture = nil
	end
	--get config in updateID handler


end


function getConfig()
	-- get last logon name, if it exists
	local config = ClientEngine_OpenConfig( g_game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )
	if config ~= nil then
		local okfilter, filter = KEPConfig_GetString( config, "ChatFilter", tostring(gUseFilter) )
        local okVolFilter, volFilter = KEPConfig_GetNumber( config, "GlobalVolume", g_vol )

        if okVolFilter and g_objId > 0 then
            g_vol = volFilter
       		ClientEngine_CallFlashFunctionOnDynamicObject( g_game, g_objId, "<invoke name=\"SetFLVVolume\" returntype=\"xml\"><arguments><number>"..tostring(volFilter).."</number></arguments/></invoke>" )
        end

	end

end

function saveConfig()
	-- get last chat coordinates if exist
	local config = ClientEngine_OpenConfig( g_game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )
	if config ~= nil then

        KEPConfig_SetNumber( config, "GlobalVolume", g_vol )

		KEPConfig_Save( config )
	end
end
---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	if ClientEngine_SetMenuSelectMode ~= nil then
		ClientEngine_SetMenuSelectMode( g_game, 0.0 )
	end
	
	Helper_Dialog_OnDestroy( dialogHandle )
	
end

---------------------------------------------------------------------
--
-- Delete Button - Delete selected object
--
---------------------------------------------------------------------
function btnDelete_OnButtonClicked( buttonHandle )

	-- show confirmation dialog
	g_confirmation = Dialog_Create("Yes_No_OutGame.xml")
	if g_confirmation ~= 0 then	  
		local sh = Dialog_GetStatic(g_confirmation, "lblMessage")		
		if sh ~= 0 then
			Static_SetText(sh, "Pick up this object?")
		end
	end

	-- close this dialog
	--DestroyMenu(gDialogHandle)
	--if g_game ~= nil then
	--	ClientEngine_Quit( g_game )
	--end
end

---------------------------------------------------------------------
--
-- Confirmation Close Handler
--
---------------------------------------------------------------------
function confirmationCloseHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

  Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "confirmation event received" ) 
  
	local yes = Event_DecodeNumber( event )

	Dialog_Destroy(g_confirmation)

	if yes == 1 then
		DeleteObject()
		DestroyMenu(gDialogHandle)  
	end
end

---------------------------------------------------------------------
--
-- Delete an object
--
---------------------------------------------------------------------
function DeleteObject()

	ClientEngine_DeleteDynamicObjectPlacement(g_game, g_objId)

end

---------------------------------------------------------------------
--
-- Save Button - save changes
--
---------------------------------------------------------------------
function btnSave_OnButtonClicked( buttonHandle )	
	
	Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "saving wld obj changes" ) 	
  	ClientEngine_SaveDynamicObjectPlacementChanges(g_game, g_objId)
  	
  	saveConfig()
  	
	-- close this dialog
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------
--
-- Cancel Button - cancel and discard changes
--
---------------------------------------------------------------------
function btnCancel_OnButtonClicked( buttonHandle )

  Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "cancelling wld obj changes" ) 	
  ClientEngine_CancelDynamicObjectPlacementChanges(g_game, g_objId)			
	-- close this dialog
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------
--
-- Change Texture Button - show texture selection menu
--
---------------------------------------------------------------------
function btnChangeTexture_OnButtonClicked( buttonHandle )

  Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "changing texture..." ) 	
  Dialog_Create( "TextureBrowser.xml");
  
  textureSelEvent = Dispatcher_MakeEvent( KEP.dispatcher, "TextureSelectionEvent" )	
	Event_EncodeNumber( textureSelEvent, g_objId )
	Dispatcher_QueueEvent( KEP.dispatcher, textureSelEvent )
  
end

function btnResetTexture_OnButtonClicked( buttonHandle )
	Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "resetting texture..." ) 	
	ClientEngine_ApplyCustomTexture(g_game, g_objId, 0, GameGlobals.FULLSIZE)

	local f, errMsg  = ClientEngine_DeleteFile( g_game, "lastasset.dat")	

	btnSave_OnButtonClicked()
end


---------------------------------------------------------------------
--
-- Start Rotate 3D Model around the y-axis (left)
--
---------------------------------------------------------------------
function btnRotateLeft_OnLButtonDown(controlHandle, x, y)

	local amt = OBJ_ROT_AMT * g_ShiftMultiple

	Rotate3DModel(amt)
	g_mov_dir = 8	

   	--Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "rotating left" )

end

---------------------------------------------------------------------
--
-- Stop Rotate 3D Model around the y-axis (left)
--
---------------------------------------------------------------------
function btnRotateLeft_OnLButtonUp(controlHandle, x, y)
	g_mov_dir = 0
end

---------------------------------------------------------------------
--
-- Start Rotate 3D Model around the y-axis (right)
--
---------------------------------------------------------------------
function btnRotateRight_OnLButtonDown(controlHandle, x, y)

	local amt = OBJ_ROT_AMT * g_ShiftMultiple

	Rotate3DModel(-amt)
	g_mov_dir = 7

	--Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "rotating right" )

end

---------------------------------------------------------------------
--
-- Stop Rotate 3D Model around the y-axis (left)
--
---------------------------------------------------------------------
function btnRotateRight_OnLButtonUp(controlHandle, x, y)
	g_mov_dir = 0
end

---------------------------------------------------------------------
--
-- Start moving forward along the direction vector
--
---------------------------------------------------------------------
function btnMoveForward_OnLButtonDown(controlHandle, x, y)
	local camx, camy, camz
	local amt = OBJ_MOVE_AMT * g_ShiftMultiple
	
	camx, camy, camz = ClientEngine_GetPlayerCameraOrientation(g_game)
	-- Forward is directly in the cam's direction.
	ClientEngine_MoveDynamicObjectPlacementInDirection(g_game, g_objId, camx, camy, camz, amt)
	g_mov_dir = 1
end

---------------------------------------------------------------------
--
-- Stop moving forward along the direction vector
--
---------------------------------------------------------------------
function btnMoveForward_OnLButtonUp(controlHandle, x, y)
	g_mov_dir = 0
end

---------------------------------------------------------------------
--
-- Start moving backward along the direction vector
--
---------------------------------------------------------------------
function btnMoveBackward_OnLButtonDown(controlHandle, x, y)
	local camx, camy, camz
	local amt = OBJ_MOVE_AMT * g_ShiftMultiple

	camx, camy, camz = ClientEngine_GetPlayerCameraOrientation(g_game)
	-- backward is just a negative z
	ClientEngine_MoveDynamicObjectPlacementInDirection(g_game, g_objId, (-1.0 * camx), camy, (-1.0 * camz), amt)
	g_mov_dir = 3	
end

---------------------------------------------------------------------
--
-- Stop moving backward along the direction vector
--
---------------------------------------------------------------------
function btnMoveBackward_OnLButtonUp(controlHandle, x, y)
	g_mov_dir = 0
end

---------------------------------------------------------------------
--
-- Start moving right along the slide vector
--
---------------------------------------------------------------------
function btnMoveRight_OnLButtonDown(controlHandle, x, y)
	local camx, camy, camz
	local amt = OBJ_MOVE_AMT * g_ShiftMultiple
	
	camx, camy, camz = ClientEngine_GetPlayerCameraOrientation(g_game)
	-- right flip the x and the z the z is negative
	ClientEngine_MoveDynamicObjectPlacementInDirection(g_game, g_objId, camz, camy, (-1.0 * camx), amt)
	g_mov_dir = 2	
end

---------------------------------------------------------------------
--
-- Stop moving right along the slide vector
--
---------------------------------------------------------------------
function btnMoveRight_OnLButtonUp(controlHandle, x, y)
	g_mov_dir = 0
end

---------------------------------------------------------------------
--
-- Start moving left along the slide vector
--
---------------------------------------------------------------------
function btnMoveLeft_OnLButtonDown(controlHandle, x, y)
	local camx, camy, camz
	local amt = OBJ_MOVE_AMT * g_ShiftMultiple

	camx, camy, camz = ClientEngine_GetPlayerCameraOrientation(g_game)
	-- right flip the x and the z the x is negative
	ClientEngine_MoveDynamicObjectPlacementInDirection(g_game, g_objId, (-1.0 * camz), camy, camx, amt)
	g_mov_dir = 4	
end

---------------------------------------------------------------------
--
-- Stop moving left along the slide vector
--
---------------------------------------------------------------------
function btnMoveLeft_OnLButtonUp(controlHandle, x, y)
	g_mov_dir = 0
end

---------------------------------------------------------------------
--
-- Start moving right along the up vector
--
---------------------------------------------------------------------
function btnMoveUp_OnLButtonDown(controlHandle, x, y)
	local amt = OBJ_MOVE_AMT * g_ShiftMultiple

	MoveModel(0,0,amt)
	g_mov_dir = 5	
end

---------------------------------------------------------------------
--
-- Stop moving right along the up vector
--
---------------------------------------------------------------------
function btnMoveUp_OnLButtonUp(controlHandle, x, y)
	g_mov_dir = 0
end

---------------------------------------------------------------------
--
-- Start moving left along the up vector
--
---------------------------------------------------------------------
function btnMoveDown_OnLButtonDown(controlHandle, x, y)
	local amt = OBJ_MOVE_AMT * g_ShiftMultiple
	
	MoveModel(0,0,-amt)
	g_mov_dir = 6	
end

---------------------------------------------------------------------
--
-- Stop moving left along the up vector
--
---------------------------------------------------------------------
function btnMoveDown_OnLButtonUp(controlHandle, x, y)
	g_mov_dir = 0
end

function clickHoldHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local mov_amt = OBJ_MOVE_AMT * g_ShiftMultiple
	local rot_amt = OBJ_ROT_AMT * g_ShiftMultiple


	if g_mov_dir > 0 then
		local camx, camy, camz
		if g_mov_dir == 1 then
			if Control_ContainsPoint(g_forcon, g_x, g_y) then
				camx, camy, camz = ClientEngine_GetPlayerCameraOrientation(g_game)
				-- Forward is directly in the cam's direction.
				ClientEngine_MoveDynamicObjectPlacementInDirection(g_game, g_objId, camx, camy, camz, mov_amt)
			end
		elseif g_mov_dir == 2 then
			if Control_ContainsPoint(g_rightcon, g_x, g_y) then
				camx, camy, camz = ClientEngine_GetPlayerCameraOrientation(g_game)
				-- right flip the x and the z and z is neg
				ClientEngine_MoveDynamicObjectPlacementInDirection(g_game, g_objId, camz, camy, (-1.0 * camx), mov_amt)
			end
		elseif g_mov_dir == 3 then
			if Control_ContainsPoint(g_backcon, g_x, g_y) then
				camx, camy, camz = ClientEngine_GetPlayerCameraOrientation(g_game)
				-- backward is just a negative z
				ClientEngine_MoveDynamicObjectPlacementInDirection(g_game, g_objId, (-1.0 * camx), camy, (-1.0 * camz), mov_amt)
			end
		elseif g_mov_dir == 4 then
			if Control_ContainsPoint(g_leftcon, g_x, g_y) then
				camx, camy, camz = ClientEngine_GetPlayerCameraOrientation(g_game)
				-- left flip the x and the z and x is negative
				ClientEngine_MoveDynamicObjectPlacementInDirection(g_game, g_objId, (-1.0 * camz), camy, camx, mov_amt)
			end
		elseif g_mov_dir == 5 then
			if Control_ContainsPoint(g_upcon, g_x, g_y) then
				MoveModel(0,0,mov_amt)
			end
		elseif g_mov_dir == 6 then
			if Control_ContainsPoint(g_downcon, g_x, g_y) then
				MoveModel(0,0,-mov_amt)
			end
		elseif g_mov_dir == 7 then
			if Control_ContainsPoint(g_rotrightcon, g_x, g_y) then
				Rotate3DModel(-rot_amt)
			end
		elseif g_mov_dir == 8 then
			if Control_ContainsPoint(g_rotleftcon, g_x, g_y) then
				Rotate3DModel(rot_amt)
			end
		end
	end
	-- Dialog_MsgBox("x=".. g_x .. " y=" .. g_y .. " MoveDir=" .. g_mov_dir)
	Dispatcher_QueueEventInFuture( g_Dispatcher, g_mouseholdevent, 1 )
end

function Dialog_OnMouseMove( dialogHandle, x, y )
	if g_mov_dir > 0 then
		g_x = x
		g_y = y
	end
end

---------------------------------------------------------------------
--
-- Rotate 3D Model by degrees
--
---------------------------------------------------------------------
function Rotate3DModel(degrees)	

	-- for now, do not rotate around x-axis
	Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "calling ClientEngine_RotateDynamicObjectPlacement" ) 	
	ClientEngine_RotateDynamicObjectPlacement(g_game, g_objId, 0, degrees, 0)
	
end

---------------------------------------------------------------------
--
-- Move 3D Model
--
---------------------------------------------------------------------
function MoveModel(dir, slide, lift)	
	
	Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "calling ClientEngine_MoveDynamicObjectPlacement" ) 	
	ClientEngine_MoveDynamicObjectPlacement(g_game, g_objId, dir, slide, lift)
	
end

function btnPlaylist_OnButtonClicked( buttonHandle )

	Dispatcher_LogMessage( g_dispatcher, KEP.ERROR_LOG_LEVEL, "changing playlist..." ) 	
	
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	if g_isFlashFrame then
		Dialog_Create( "FlashSelect.xml" );
	
		--fire event to pass placement id to the menu script
		menuOpenEvent = Dispatcher_MakeEvent( KEP.dispatcher, FLASHFRAME_MENU_EVENT )
		
		Event_EncodeNumber( menuOpenEvent, g_objId )
		Event_EncodeNumber( menuOpenEvent, g_kanevaUserId )	
		Dispatcher_QueueEvent( KEP.dispatcher, menuOpenEvent )
	else
		Dialog_Create( "PlaylistSelect.xml" );
	
		--fire event to pass placement id to the menu script
		menuOpenEvent = Dispatcher_MakeEvent( KEP.dispatcher, PLAYLIST_MENU_EVENT )
		
		Event_EncodeNumber( menuOpenEvent, g_objId )
		Event_EncodeNumber( menuOpenEvent, g_kanevaUserId )	
		Dispatcher_QueueEvent( KEP.dispatcher, menuOpenEvent )
	end
	  
end 

function btnVolDn_OnButtonClicked( buttonHandle )

	if g_cur_flash_vol >= 1 then
		g_cur_flash_vol = g_cur_flash_vol - 1
		vol = g_t_TV_volumes[g_cur_flash_vol]
		ClientEngine_SetFlashVolumeOnDynamicObject(g_game, g_objId, vol)
		--ClientEngine_CallFlashFunctionOnDynamicObject( g_game, g_objId, "<invoke name=\"SetFLVVolume\" returntype=\"xml\"><arguments><number>"..tostring(vol).."</number></arguments/></invoke>" )
	end

	showVolume()

end 

function btnVolUp_OnButtonClicked( buttonHandle )


	if g_cur_flash_vol < FLASH_VOL_STEPS then
		g_cur_flash_vol = g_cur_flash_vol + 1
		ClientEngine_SetFlashVolumeOnDynamicObject(g_game, g_objId, g_t_TV_volumes[g_cur_flash_vol])
		--ClientEngine_CallFlashFunctionOnDynamicObject( g_game, g_objId, "<invoke name=\"SetFLVVolume\" returntype=\"xml\"><arguments><number>"..tostring(vol).."</number></arguments/></invoke>" )
	end

	showVolume()

	
end


function btnRave_OnButtonClicked( buttonHandle )

	ClientEngine_CallFlashFunctionOnDynamicObject( g_game, g_objId, "<invoke name=\"RaveItem\" returntype=\"xml\"><arguments></arguments/></invoke>" )
	createMessage( "Item Raved!" )
	getCurrentRave();

	
	--btnCancel_OnButtonClicked( buttonHandle )

end 

---------------------------------------------------------------------
-- Keyboard Handler
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
      -- ESC key
      
      if bShiftDown then
        g_ShiftMultiple = OBJ_SHIFT_MOVE_AMT
      end
      
      if key == 27 then
            -- close this menu
		Dispatcher_LogMessage( g_dispatcher, KEP.TRACE_LOG_LEVEL, "cancelling wld obj changes" ) 	
  		ClientEngine_CancelDynamicObjectPlacementChanges(g_game, g_objId)
      end
end

function Dialog_OnKeyUp(dialogHandle, key, bShiftDown)
      -- ESC key

      if not(bShiftDown) then
        g_ShiftMultiple = 1
      end

end


---------------------------------------------------------------------
-- Mouse Over
---------------------------------------------------------------------
function getButtonHandles()
--g_t_btnHandles


	local ch = Dialog_GetControl(gDialogHandle, "btnDelete")
	if ch ~= nil then
     	g_t_btnHandles["btnDelete"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSave")
	if ch ~= nil then
     	g_t_btnHandles["btnSave"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnCancel")
	if ch ~= nil then
     	g_t_btnHandles["btnCancel"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnRotateLeft")
	if ch ~= nil then
		g_t_btnHandles["btnRotateLeft"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnRotateRight")
	if ch ~= nil then
     	g_t_btnHandles["btnRotateRight"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnMoveRight")
	if ch ~= nil then
     	g_t_btnHandles["btnMoveRight"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnMoveLeft")
	if ch ~= nil then
     	g_t_btnHandles["btnMoveLeft"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnMoveForward")
	if ch ~= nil then
     	g_t_btnHandles["btnMoveForward"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnMoveBackward")
	if ch ~= nil then
     	g_t_btnHandles["btnMoveBackward"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnMoveUp")
	if ch ~= nil then
     	g_t_btnHandles["btnMoveUp"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnMoveDown")
	if ch ~= nil then
     	g_t_btnHandles["btnMoveDown"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnChangeTexture")
	if ch ~= nil then
     	g_t_btnHandles["btnChangeTexture"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnResetTexture")
	if ch ~= nil then
     	g_t_btnHandles["btnResetTexture"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnPlaylist")
	if ch ~= nil then
     	g_t_btnHandles["btnPlaylist"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnVolDown")
	if ch ~= nil then
     	g_t_btnHandles["btnVolDown"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnVolUp")
	if ch ~= nil then
     	g_t_btnHandles["btnVolUp"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnRave")
	if ch ~= nil then
     	g_t_btnHandles["btnRave"] = ch
	end


end

function Dialog_OnMouseMove(dialogHandle, x, y)

    local sh = Dialog_GetStatic(dialogHandle, "lblStatus")

	if sh ~= nil then

		if Control_ContainsPoint(g_t_btnHandles["btnDelete"], x, y) ~= 0 then
			Static_SetText(sh, "Pick Up This Object")
   		elseif Control_ContainsPoint(g_t_btnHandles["btnSave"], x, y) ~= 0 then
	    	Static_SetText(sh, "Save Changes to Object")
		elseif Control_ContainsPoint(g_t_btnHandles["btnCancel"], x, y) ~= 0 then
			Static_SetText(sh, "Cancel Changes and Close")
		elseif Control_ContainsPoint(g_t_btnHandles["btnRotateLeft"], x, y) ~= 0 then
		    Static_SetText(sh, "Rotate Object Clockwise")
		elseif Control_ContainsPoint(g_t_btnHandles["btnRotateRight"], x, y) ~= 0 then
			Static_SetText(sh, "Rotate Object Counter Clockwise")
		elseif Control_ContainsPoint(g_t_btnHandles["btnMoveForward"], x, y) ~= 0 then
			Static_SetText(sh, "Move Object Away From You")
		elseif Control_ContainsPoint(g_t_btnHandles["btnMoveBackward"], x, y) ~= 0 then
			Static_SetText(sh, "Move Object Toward You")
		elseif Control_ContainsPoint(g_t_btnHandles["btnMoveUp"], x, y) ~= 0 then
			Static_SetText(sh, "Move Object Up")
		elseif Control_ContainsPoint(g_t_btnHandles["btnMoveDown"], x, y) ~= 0 then
			Static_SetText(sh, "Move Object Down")
		elseif Control_ContainsPoint(g_t_btnHandles["btnMoveRight"], x, y) ~= 0 then
			Static_SetText(sh, "Move Object Right")
		elseif Control_ContainsPoint(g_t_btnHandles["btnMoveLeft"], x, y) ~= 0 then
			Static_SetText(sh, "Move Object Left")
		elseif Control_ContainsPoint(g_t_btnHandles["btnChangeTexture"], x, y) ~= 0 then
			Static_SetText(sh, "Change Pattern of Object")
		elseif Control_ContainsPoint(g_t_btnHandles["btnResetTexture"], x, y) ~= 0 then
			Static_SetText(sh, "Reset to Original Pattern")
		elseif Control_ContainsPoint(g_t_btnHandles["btnPlaylist"], x, y) ~= 0 
				and Control_GetEnabled(Button_GetControl( g_t_btnHandles["btnPlaylist"] )) == 1 then
			Static_SetText(sh, "Change the Current Playlist")
		elseif Control_ContainsPoint(g_t_btnHandles["btnVolDown"], x, y) ~= 0 
				and Control_GetEnabled(Button_GetControl( g_t_btnHandles["btnVolDown"] )) == 1 then
			Static_SetText(sh, "Turn Volume Down")
		elseif Control_ContainsPoint(g_t_btnHandles["btnVolUp"], x, y) ~= 0 
				and Control_GetEnabled(Button_GetControl( g_t_btnHandles["btnVolUp"] )) == 1 then
			Static_SetText(sh, "Turn Volume Up")
		elseif Control_ContainsPoint(g_t_btnHandles["btnRave"], x, y) ~= 0 
				and Control_GetEnabled(Button_GetControl( g_t_btnHandles["btnRave"] )) == 1 then
			Static_SetText(sh, "Rave This Item")

		else
            Static_SetText(sh, "Hold shift & click for greater movement")
		end
	end

end

function btnRepeatTexture_OnButtonClicked( buttonhandle )
	if g_lasttexture ~= nil then
		ClientEngine_ApplyCustomTexture(g_game, g_objId, g_lasttexture, GameGlobals.FULLSIZE)
		--ClientEngine_ApplyCustomTextureWldObject(g_game, g_objId, g_lasttexture, GameGlobals.FULLSIZE)
		g_textureAltered = true
		btnSave_OnButtonClicked()
	end
end

