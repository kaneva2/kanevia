dofile("MenuHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\KEP.lua")
dofile("MenuLocation.lua")

-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--
-- end 

PREF_EVENT = "PrefEvent"

g_cbLangFilter = nil
gUseFilter = true

g_cbhIcons = 0
g_useIcons = true

g_cbhWidgets = 0

g_game = nil

g_vol = 50

g_t_btnHandles = {}




---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	
	-- TODO register any of your handlers here like the following....
	-- ParentHandler_RegisterEventHandler( dispatcher, handler, "renderTextHandler", 1,0,0,0, "RenderTextEvent", KEP.HIGH_PRIO ) 
	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, PREF_EVENT, KEP.MED_PRIO )
	
	-- TODO register any new event types here like the following...
	-- ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "LuaEvent", KEP.MED_PRIO )


end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	if KEPConfig_GetString == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end
	
	getConfig()

	loadLocation(g_game, gDialogHandle, "Preferences")
	
	showVolume()
	
	g_cbLangFilter   = Dialog_GetCheckBox(gDialogHandle, "cbFilter" )
	g_cbhIcons = Dialog_GetCheckBox(gDialogHandle, "cbIcons" )
	g_cbhWidgets = Dialog_GetCheckBox( gDialogHandle, "cbWidgets" )
	
	CheckBox_SetChecked( g_cbLangFilter, gUseFilter )
	CheckBox_SetChecked( g_cbhIcons, g_useIcons )

	getButtonHandles()

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	saveLocation(g_game, dialogHandle, "Preferences")

	saveConfig()
	
	Helper_Dialog_OnDestroy( dialogHandle )
	

end



function getConfig()

	local config = ClientEngine_OpenConfig( g_game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )
	
	if config ~= nil then

		ok, name = KEPConfig_GetString( config, "cbWidgets", "false" )
	
		if name == "true" then
			CheckBox_SetChecked(g_cbhWidgets, true)
		else
			CheckBox_SetChecked(g_cbhWidgets, false)
		end

		local okfilter, filter = KEPConfig_GetString( config, "ChatFilter", tostring(gUseFilter) )
		
		if filter == "true" then
		    gUseFilter = true
		else
		    gUseFilter = false
		end

        local okVolFilter, volFilter = KEPConfig_GetNumber( config, "GlobalVolume", g_vol )
        
        if okVolFilter then
            g_vol = volFilter
        end
        
		local okfilter, iconPref = KEPConfig_GetString( config, "ShowIcons", tostring(g_useIcons) )
		
		if iconPref == "true" then
			g_useIcons = true
		else
			g_useIcons = false
		end

	end

end

function saveConfig()
	-- get last chat coordinates if exist
	local config = ClientEngine_OpenConfig( g_game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )
	if config ~= nil then

		KEPConfig_SetString( config, "ChatFilter", tostring(gUseFilter) )
        KEPConfig_SetNumber( config, "GlobalVolume", g_vol )
		KEPConfig_SetString( config, "ShowIcons", tostring(g_useIcons) )

		KEPConfig_Save( config )
	end
end

function showVolume()

	local sh = Dialog_GetStatic(gDialogHandle, "stVol")
	if g_vol > -1 then
		Static_SetText( sh, g_vol )
	else
		Static_SetText( sh, "###" )  -- error
	end

	return g_vol
end

function cbFilter_OnCheckBoxChanged( )

	local filter = ""

	if gUseFilter then
		gUseFilter = false
		filter = tostring(gUseFilter)
	else
	   	gUseFilter = true
     	filter = tostring(gUseFilter)
	end
	
	local prefEvent = Dispatcher_MakeEvent( KEP.dispatcher, PREF_EVENT )
	Event_EncodeString( prefEvent, filter )
	Dispatcher_QueueEvent( KEP.dispatcher, prefEvent )

end

---------------------------------------------------------------------
--
-- Button Handler
--
---------------------------------------------------------------------

function btnClose_OnButtonClicked( buttonHandle )
	DestroyMenu( gDialogHandle )
end

function btnVolDn_OnButtonClicked( buttonHandle )

	if g_game ~= nil then
	
		local vol = showVolume()
		if vol >= 10 then
			g_vol = vol - 10
			--ClientEngine_CallFlashFunctionOnDynamicObject( g_game, g_objId, "<invoke name=\"SetFLVVolume\" returntype=\"xml\"><arguments><number>"..tostring(vol).."</number></arguments/></invoke>" )
			--No API for this yet so just adjust flash volume
			--ClientEngine_SetSoundVolume(g_game, vol)
			showVolume()
		end
		
	end
end

function btnVolUp_OnButtonClicked( buttonHandle )

	if g_game ~= nil then

		local vol = showVolume()
		if vol <= 90 then
			g_vol = vol + 10
			--ClientEngine_CallFlashFunctionOnDynamicObject( g_game, g_objId, "<invoke name=\"SetFLVVolume\" returntype=\"xml\"><arguments><number>"..tostring(vol).."</number></arguments/></invoke>" )
	        --ClientEngine_SetSoundVolume(g_game, vol)
			showVolume()
		end
		
	end
	
end

function btnCtrlSet_OnButtonClicked( buttonHandle )
	if g_game ~= nil then
		gotoMenu( "Control_Settings.xml", 0, 1 )
	end
end

---------------------------------------------------------------------
--
-- Keyboard Handler
--
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)

	-- ESC key
	if key == 27 then

		-- close this menu
		DestroyMenu(gDialogHandle)

	end

end

---------------------------------------------------------------------
--
-- Go To  menu
--
---------------------------------------------------------------------
function gotoMenu(menu_path, bClose, bModal)

	dh = Dialog_Create( menu_path );
	if dh == 0 then
		Dialog_MsgBox("Menu [" .. menu_path .. "] not found.")
	end

	if bModal == 1 and dh ~= 0 then
		Dialog_SetModal(dh, 1)
		Dialog_SendToBack( gDialogHandle )
	end

	-- close this dialog
	if bClose == 1 then
		DestroyMenu(gDialogHandle)
	end

	return dh

end

---------------------------------------------------------------------
-- Mouse Over
---------------------------------------------------------------------
function getButtonHandles()
--g_t_btnHandles


	local ch = Dialog_GetControl(gDialogHandle, "btnClose")
	if ch ~= nil then
     	g_t_btnHandles["btnClose"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnVolDn")
	if ch ~= nil then
     	g_t_btnHandles["btnVolDn"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnVolUp")
	if ch ~= nil then
		g_t_btnHandles["btnVolUp"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnCtrlSet")
	if ch ~= nil then
     	g_t_btnHandles["btnCtrlSet"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "cbFilter")
	if ch ~= nil then
     	g_t_btnHandles["cbFilter"] = ch
	end

end

function Dialog_OnMouseMove(dialogHandle, x, y)

    local sh = Dialog_GetStatic(dialogHandle, "lblStatus")

	if sh ~= nil then

		if Control_ContainsPoint(g_t_btnHandles["btnClose"], x, y) ~= 0 then
			Static_SetText(sh, "Close Menu")
   		elseif Control_ContainsPoint(g_t_btnHandles["btnVolDn"], x, y) ~= 0 then
	    	Static_SetText(sh, "Increase Volume")
		elseif Control_ContainsPoint(g_t_btnHandles["btnVolUp"], x, y) ~= 0 then
			Static_SetText(sh, "Decrease Volume")
		elseif Control_ContainsPoint(g_t_btnHandles["btnCtrlSet"], x, y) ~= 0 then
		    Static_SetText(sh, "Configure Keyboard")
		elseif Control_ContainsPoint(g_t_btnHandles["cbFilter"], x, y) ~= 0 then
		    Static_SetText(sh, "Mask Out Bad Language")
		else
            Static_SetText(sh, "Select an Option")
		end
	end

end

function cbWidgets_OnCheckBoxChanged( )

	local setting = false

 	if g_cbhWidgets ~= nil then
   		
		SelectWidget = CheckBox_GetChecked( g_cbhWidgets )

		if  SelectWidget == 1 then
			ClientEngine_DisableSelectWidgetDisplay(g_game)
			setting = false
   		else
			ClientEngine_EnableSelectWidgetDisplay(g_game)
			setting = true
		end

		local config = ClientEngine_OpenConfig( g_game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )

    	if config ~= nil then

          	KEPConfig_SetString( config, "cbWidgets", tostring(setting))
		
          	KEPConfig_Save( config )
          	
		end
	
   	end
end

function cbIcons_OnCheckBoxChanged( )

	g_useIcons = not g_useIcons
	
end

function btnGraphicsSet_OnButtonClicked ( buttonhandle)
	if g_game ~= nil then
		gotoMenu( "GraphicsOptions.xml", 0, 1 )
	end
end