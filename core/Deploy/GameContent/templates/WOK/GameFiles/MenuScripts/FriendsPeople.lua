dofile("MenuHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("PageButtons.lua")
dofile("..\\Scripts\\Progressive.lua")
dofile("..\\Scripts\\BrowserIds.lua")
dofile("MenuLocation.lua")
dofile("CommonFunctions.lua")

-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--
-- end 

PRIVATE_MESSAGE_EVENT = "PrivateMessageEvent"
PLAYER_USERID_EVENT = "PlayerUserIdEvent"
YES_NO_BOX_EVENT = "YesNoBoxEvent"
YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"

TYPE_START_EVENT = "StartTypingEvent"

CODES_TEXT = "Adjust block settings for currently blocked people\n\nC - Communications Blocked              F - Follow Blocked              Z - Zone Blocked"


-- URLs for website queries
FRIENDS_SUFFIX = "kgp/friendsList.aspx"
SEARCH_SUFFIX = "kgp/friendsList.aspx?search="
-- Sorted by zone then name
PEOPLE_ZONE_SUFFIX = "kgp/peopleInGame.aspx"
-- Sort by name only
PEOPLE_NAME_SUFFIX = "kgp/peopleInGame.aspx?sortbyname=T"
-- Search for user
PEOPLE_SEARCH_SUFFIX = "kgp/peopleInGame.aspx?search="
--RAVE_SUFFIX = "kgp/ravePlayer.aspx?userId="
UPDATE_SUFFIX = "kgp/updateFriend.aspx?userId="
ACTION_SUFFIX = "&action="
ONLINE_SUFFIX = "?online=T"
IGNORE_SUFFIX = "kgp/blockUser.aspx?action="
ADD_IGNORE_SUFFIX = "Block&userId="
REMOVE_IGNORE_SUFFIX = "UnBlock&userId="
LIST_IGNORE_SUFFIX = "List"
LOGON_SORT_SUFFIX = "?sortbylogon=T"



-- Menu Tabs
TAB_FRIENDS = 0
TAB_PEOPLE = 1
TAB_IGNORE = 2

-- Buttons requiring a message box
BTN_NONE = 0
BTN_FRIEND = 1
BTN_IGNORE = 2

-- Web address for query
g_web_address = nil

-- Current tab selected (Friends, People, or Ignore)
g_current_tab = TAB_FRIENDS

g_game = nil

g_help = false;

-- Hold current friend data returned from server
g_friendData = ""
-- Total number of friend records
g_totalNumRec = 0
-- Maximum number of friends for server to return in one request
-- friends per page
g_friendsPerPage = 8


g_selectedIndex = -1
g_t_friends = {} 


-- Some calls to get browser page will return only success or failure info.
-- This is used to pick our parse function.
g_resultParse = false

-- Which button was pressed.  This is used to decide on the status
-- message after an action
g_buttonPressed = BTN_NONE

-- My character's name
g_myName = ""

g_t_btnHandles = {}

g_showConfrim = false

g_sortType = LOGON_SORT_SUFFIX



---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "BrowserPageReadyHandler", 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "textHandler", 1,0,0,0, "RenderTextEvent", KEP.HIGH_PRIO )	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "answerEventHandler", 1,0,0,0, YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
	
	-- TODO register any of your handlers here like the following....
	-- ParentHandler_RegisterEventHandler( dispatcher, handler, "renderTextHandler", 1,0,0,0, "RenderTextEvent", KEP.HIGH_PRIO ) 
	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, TYPE_START_EVENT, KEP.LOW_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, YES_NO_BOX_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
	-- TODO register any new event types here like the following...
	-- ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "LuaEvent", KEP.MED_PRIO )


end

function answerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local answer = Event_DecodeNumber( event )

	if answer ~= 0 then
		if g_buttonPressed == BTN_FRIEND and g_current_tab == TAB_FRIENDS then
           	sendFriendAction("Remove")
  		end  		
	end
	
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	-- TODO init here

	g_web_address = GameGlobals.WEB_SITE_PREFIX..PEOPLE_NAME_SUFFIX
	
	g_current_tab = TAB_PEOPLE

    g_game = Dispatcher_GetGame(KEP.dispatcher)

    if g_game == 0 then
            g_game = nil
    end
      	
	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	
	if KEPConfig_GetString == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end		

    local t = { ClientEngine_GetPlayerInfo(g_game) }
	if t ~= 0  then
	 	-- index 10 = name
		temp = t[10+1]
		if temp ~= nil then
			g_myName = temp
		end
    end


	
	g_sortType = getSettings()



	loadLocation(g_game, gDialogHandle, "FP")

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end

    bh = Dialog_GetButton(gDialogHandle, "btnPeople")
	setButtonState(bh, false)

	Dialog_BringToFront(gDialogHandle)
	
	initButtons()
	
	getButtonHandles()
	
	-- setSelectionColor is called from PageButtons.lua to highlight the first
	-- button when the menu is opend.
	local bh = Dialog_GetButton(dialogHandle, "btnRange1")
	if bh ~= nil then
		setSelectionColor( bh )
	end
	
	cbh = Dialog_GetCheckBox(gDialogHandle, "cbOffline")

   	if cbh ~= nil then
   	   	--CheckBox_SetChecked( cbh, false )
   		setButtonState(cbh, false)
	end
	
  	ih = Dialog_GetImage(gDialogHandle, "imgBgStatusSelect")
	if ih ~= nil then
		ch = Image_GetControl(ih)
		if ch ~= nil then
			Control_SetVisible(ch, false)
		end
	end
	
	setListBoxStyle(Dialog_GetListBox(dialogHandle, "lstFriends"))
    setListBoxStyle(Dialog_GetListBox(dialogHandle, "lstOnline"))

	sh = Dialog_GetStatic(gDialogHandle, "lblBlockCodes")
	Static_SetText(sh, CODES_TEXT)
    HideHelp();
end

function HideHelp()
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static1"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static2"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static3"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static4"), false )
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image1"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image2"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image3"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image4"),false);
end

function btnHelp_OnButtonClicked(buttonHandle)
	g_help=not g_help
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static1"), g_help )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static2"), g_help )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static3"), g_help )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static4"), g_help )
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image1"),g_help);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image2"),g_help);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image3"),g_help);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image4"),g_help);
end

-- return boolean noting whether all friends or 
-- only online friends should be shown
function getSettings()

	local sortType = ""

	local config = ClientEngine_OpenConfig( g_game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )
	if config ~= nil then
        
		local okfilter, checked = KEPConfig_GetString( config, "FPShowOnline", "false" )

		if okfilter then
			if checked == "true" then
			    sortType = ONLINE_SUFFIX
			    checked = true
			else
			    sortType = LOGON_SORT_SUFFIX
			    checked = false
			end

			local cbh = Dialog_GetCheckBox( gDialogHandle, "cbOffline" )
			if cbh ~= nil then
				CheckBox_SetChecked( cbh, checked )
			end
		end

		return sortType
	end

	return sortType
end

function saveSettings()
	
	local cbState = nil

	local cbh = Dialog_GetCheckBox( gDialogHandle, "cbOffline" )
	if cbh ~= nil then
		-- Get current state of checkbox
		cbState = CheckBox_GetChecked( cbh )
		if cbState ~= 1 then 
			cbState = false
		else
		   	cbState = true
		end 
	end
	

	local config = ClientEngine_OpenConfig( g_game, GameGlobals.CONFIG_FILE, GameGlobals.CONFIG_ROOT )
	if config ~= nil then
		KEPConfig_SetString( config, "FPShowOnline", tostring(cbState) )

		KEPConfig_Save( config )
	end
	
end
---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here

	saveSettings()
	saveLocation(g_game, dialogHandle, "FP")

	Helper_Dialog_OnDestroy( dialogHandle )
	
end



---------------------------------------------------------------------
-- Event Handlers
---------------------------------------------------------------------

function textHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

	local msg   = Event_DecodeString( event )
	local type  = Event_DecodeNumber( event )

	s, e = string.find(msg, "Travel")
	
	if not(s == 1 and type == ChatType.System) then
		local bh = Dialog_GetButton(gDialogHandle, "btnTravel")
		setButtonState(bh, true)
	end
	
	return KEP.EPR_OK
end

-- Catch the xml from the server
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter == WF.FRIENDS then

	--	local sh = Dialog_GetStatic( gDialogHandle, "Static69" )
	
		g_friendData = Event_DecodeString( event )
		
	--Dialog_MsgBox(g_friendData)

		if g_resultParse then
			ParseResults()
		else
			if g_current_tab == TAB_FRIENDS then
		    	ParseFriendData()
			elseif g_current_tab == TAB_PEOPLE then
			    ParsePeopleData()
			elseif g_current_tab == TAB_IGNORE then
			    ParseIgnoreData()
			end
			
			clearSelection()
		end
	
	
		return KEP.EPR_CONSUMED
	end
end

function ParseResults()

	local resultDesc = ""  -- result description
	local s, e = 0      -- start and end of captured string
	local resulCode = -1
	local name = g_t_friends[g_selectedIndex]["name"]

	
	s, e, resultCode = string.find(g_friendData, "<ReturnCode>(.-)</ReturnCode>")
	s, e, resultDesc = string.find(g_friendData, "<ResultDescription>(.-)</ResultDescription>")

	if resultCode == "0" then
		if g_buttonPressed == BTN_FRIEND then
		    if g_current_tab == TAB_FRIENDS then
				createMessage(name..RM.REMOVE_FRIEND)
				clearSelection()
		    else
				createMessage(RM.FRIEND_REQUEST..name)
		    end
		elseif g_buttonPressed == BTN_IGNORE then
			if g_current_tab == TAB_IGNORE then
				createMessage(RM.REMOVE_IGNORE..name)
				--Refresh ignore list after removing someone
				clearSelection()
				local web_address = GameGlobals.WEB_SITE_PREFIX..IGNORE_SUFFIX..LIST_IGNORE_SUFFIX
				ClientEngine_GetBrowserPage(g_game, web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
			else
				createMessage(RM.ADD_IGNORE..name)
			end
		end
	elseif resultCode == "1" and g_current_tab == TAB_PEOPLE then
		if resultDesc == "already friends" then
			createMessage(RM.ALREADY_FRIENDS..g_t_friends[g_selectedIndex]["name"])	
		elseif resultDesc == "already requested to be friends" then
			createMessage(RM.FRIEND_REQUEST_SENT..g_t_friends[g_selectedIndex]["name"])
		end
	end

	--createMessage(resultDesc)

    g_resultParse = false
    g_buttonPressed = nil
    	
end

function ParseIgnoreData()

	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local user = "" 		-- username
	local playerID = 0    -- WOK id of player
	local userID = 0      -- Kaneva id of player
	local blockDate = "" 	-- date user blocked
	local blockTime = "" 	-- time user blocked
 
	s, strPos, result = string.find(g_friendData, "<ReturnCode>([0-9]+)</ReturnCode>")

	if result == "0" then

		numRec = calculateRanges()

		local lbh = Dialog_GetListBox(gDialogHandle, "lstFriends")
		local lbhOn = Dialog_GetListBox(gDialogHandle, "lstOnline")

        --g_t_friends = {}

		if (lbh ~= nil) and (lbhOn ~= nil) then
			g_t_friends = {}
			ListBox_RemoveAllItems(lbh)
			ListBox_RemoveAllItems(lbhOn)

			for i=1,numRec do
				s, strPos, userID = string.find(g_friendData, "<block_owner>([0-9]+)</block_owner>", strPos)
				s, strPos, blockeeID = string.find(g_friendData, "<profile_id>([0-9]+)</profile_id>", strPos)
				s, strPos, blockDate = string.find(g_friendData, "<blocked_date>(%w+%-%w+%-%w+)T%w+:%w+:%w+", strPos)
				s, strPos, user = string.find(g_friendData, "<username>(.-)</username>", strPos)
				s, strPos, comm = string.find(g_friendData, "<comm_blocked>([0-9]+)</comm_blocked>", strPos)
				s, strPos, follow = string.find(g_friendData, "<follow_blocked>([0-9]+)</follow_blocked>", strPos)
				s, strPos, zone = string.find(g_friendData, "<zone_blocked>([0-9]+)</zone_blocked>", strPos)



				local friend = {}
				friend["name"] = user
				friend["userID"] = blockeeID
				friend["blockDate"] = blockDate
				friend["comm"] = comm
				friend["follow"] = follow
				friend["zone"] = zone
				--friend["blockTime"] = blockTime
				table.insert(g_t_friends, friend)

                local blockSettings = ""

				if comm == "1" then
				    blockSettings = blockSettings.." C "
				end

				if follow == "1" then
				    blockSettings = blockSettings.." F "
				end

				if zone == "1" then
				    blockSettings = blockSettings.." Z "
				end

				blockSettings = blockSettings.." --- "..blockDate

				ListBox_AddItem(lbh, user, blockeeID)
                ListBox_AddItem(lbhOn, blockSettings, blockeeID)

			end
			
  			alignButtons()
			
		end
   else
	  	-- Go back to beginning to grab description
		s, e, result = string.find(g_friendData, "<ReturnDescription>(%w+)</ReturnDescription>", 1)

		local sh = Dialog_GetStatic(gDialogHandle, "lblStatus")

		if sh ~= nil then
			Static_SetText(sh, result)
	    end
	end
	

end

-- Parse people in zone data
function ParsePeopleData()

	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local user = "" 		-- username 
	local playerID = 0    -- WOK id of player
	local userID = 0      -- Kaneva id of player
	local zone = ""	-- Zone player is currently located in


	s, strPos, result = string.find(g_friendData, "<ReturnCode>([0-9]+)</ReturnCode>")

	if result == "0" then

		numRec = calculateRanges()

		local lbh = Dialog_GetListBox(gDialogHandle, "lstFriends")
		local lbhOn = Dialog_GetListBox(gDialogHandle, "lstOnline")

        --g_t_friends = {}

		if (lbh ~= nil) and (lbhOn ~= nil) then
			g_t_friends = {}
			ListBox_RemoveAllItems(lbh)
			ListBox_RemoveAllItems(lbhOn)

			for i=1,numRec do
				s, strPos, user = string.find(g_friendData, "<name>(.-)</name>", strPos)
				s, strPos, playerID = string.find(g_friendData, "<player_id>([0-9]+)</player_id>", strPos)
				s, strPos, userID = string.find(g_friendData, "<kaneva_user_id>([0-9]+)</kaneva_user_id>", strPos)
				--s, strPos, zone = string.find(g_friendData, "<current_zone>(.-)%.", strPos)
				s, strPos, zone = string.find(g_friendData, "<display_name>(.-)</display_name>", strPos)				

				local friend = {}
				friend["ID"] = tonumber(playerID)
				friend["name"] = user
				friend["userID"] = tonumber(userID)
				friend["zone"] = zone
				table.insert(g_t_friends, friend)

				ListBox_AddItem(lbh, user, userID)
                ListBox_AddItem(lbhOn, zone, userID)

			end
			

      		alignButtons()
			
		end
   else
	  	-- Go back to beginning to grab description
		s, e, result = string.find(g_friendData, "<ReturnDescription>(%w+)</ReturnDescription>", 1)

		local sh = Dialog_GetStatic(gDialogHandle, "lblStatus")

		if sh ~= nil then
			Static_SetText(sh, result)
	    end
	end

end

-- Parse friend data from server
function ParseFriendData()


	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local user = "" 		-- username
	local playerID = 0    -- WOK id of player
	local userID = 0      -- Kaneva id of player
	local glueDate = "" 	-- date user became friend
	local glueTime = "" 	-- time user became friend
	local online = ""   	-- online status of user
	local lastOnline = "" 	-- last time user was online
	

	s, strPos, result = string.find(g_friendData, "<ReturnCode>([0-9]+)</ReturnCode>")

	if result == "0" then
	
	    numRec = calculateRanges()
	
		local lbh = Dialog_GetListBox(gDialogHandle, "lstFriends")
		local lbhOn = Dialog_GetListBox(gDialogHandle, "lstOnline")
	

		
		if (lbh ~= nil) and (lbhOn ~= nil) then

			g_t_friends = {}

			ListBox_RemoveAllItems(lbh)
			ListBox_RemoveAllItems(lbhOn)
			
			for i=1,numRec do

				s, strPos, user = string.find(g_friendData, "<username>(.-)</username>", strPos)
				s, strPos, userID = string.find(g_friendData, "<user_id>([0-9]+)</user_id>", strPos)
				s, strPos, playerID = string.find(g_friendData, "<player_id>([0-9]+)</player_id>", strPos)
				s, strPos, glueDate, glueTime = string.find(g_friendData, "<glued_date>(%w+%-%w+%-%w+)T(%w+:%w+:%w+)", strPos)
  				s, strPos, online = string.find(g_friendData, "<online>(%w+)</online>", strPos)
  				
  				
				if playerID ~= "0"  then 
					s, strPos, lastOnline = string.find(g_friendData, "<hours_since_last_logon>(%d+)</hours_since_last_logon>", strPos)
					lastOnline = tonumber(lastOnline)
					if (lastOnline ~= nil) and (lastOnline > 24) then
					    lastOnline = math.floor(lastOnline/24)
						if lastOnline == 1 then
							lastOnline = "Yesterday"
						else
						    lastOnline = lastOnline.." days ago"
						end
					else if lastOnline == 1 then
					    	lastOnline = lastOnline.." hour ago"
						else if lastOnline == 0 then
								lastOnline = "Under 1 hour ago"
							else
					    		lastOnline = lastOnline.." hours ago"
					    	end
					    end
					end
				else
					lastOnline = "No logons"
				end

				if online == "T" then
		    			online = "online"
				else
		    			online = lastOnline
				end


				local friend = {}
				friend["ID"] = playerID
				friend["name"] = user
				friend["date"] = glueDate
				friend["time"] = glueTime
				friend["online"] = online
				friend["userID"] = tonumber(userID)
				
				table.insert(g_t_friends, friend)
				
				ListBox_AddItem(lbh, user, userID)
                ListBox_AddItem(lbhOn, online, userID)
                
			end



  			alignButtons()

			
		end
   else
	  	-- Go back to beginning to grab description
		s, e, result = string.find(g_friendData, "<ReturnDescription>(%w+)</ReturnDescription>", 1)

		local sh = Dialog_GetStatic(gDialogHandle, "lblStatus")

		if sh ~= nil then
			Static_SetText(sh, result)
	    end
	end

end

-- Used to fill the buttons with the correct numerical text
function calculateRanges()

		local numRec = 0  --number of records returned
		local s, e  -- start and end indices of found string
		local resultsstr = "Showing " -- text for result label

		--local totalRec_h = Dialog_GetStatic( gDialogHandle, "lblTotal")
		--local rangeHigh_h = Dialog_GetStatic( gDialogHandle, "lblRangeHigh")
		--local rangeLow_h = Dialog_GetStatic( gDialogHandle, "lblRangeLow")
		local resultslbl = Dialog_GetStatic( gDialogHandle, "lblResults")

        local high = g_curPage * g_friendsPerPage
		local low = ((g_curPage - 1) * g_friendsPerPage) + 1


		s, e, g_totalNumRec = string.find(g_friendData, "<TotalNumberRecords>([0-9]+)</TotalNumberRecords>")
		s, e, numRec = string.find(g_friendData, "<NumberRecords>([0-9]+)</NumberRecords>")


		g_totalNumRec = tonumber(g_totalNumRec)

		g_pageCap = math.ceil(g_totalNumRec / g_friendsPerPage)
		g_setCap = math.ceil(g_pageCap / g_window)


    	if high > g_totalNumRec then
	        high = g_totalNumRec
		end

	    if numRec == "0" then
	        low = numRec
	    end

		if resultslbl ~= nil then
			resultsstr = resultsstr..low.." - "..high.." of "..g_totalNumRec
			Static_SetText(resultslbl, resultsstr)
		end
		 
		if g_InitBtnText then
		    setButtonText()
		    g_InitBtnText = false
		end
		
		return numRec

end


---------------------------------------------------------------------
-- List Box selection
---------------------------------------------------------------------

function lstFriends_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

		g_selectedIndex = sel_index + 1

		local lbh = Dialog_GetListBox(gDialogHandle, "lstOnline")

		if (ListBox_GetSelectedItemIndex(lbh) ~= sel_index) and (lbh ~= nil) then
			ListBox_SelectItem( lbh, sel_index )
		end

end	

function lstOnline_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

		g_selectedIndex = sel_index + 1

		local lbh = Dialog_GetListBox(gDialogHandle, "lstFriends")

		if (ListBox_GetSelectedItemIndex(lbh) ~= sel_index) and (lbh ~= nil) then
			ListBox_SelectItem( lbh, sel_index )
		end
		
end	

---------------------------------------------------------------------
-- Button Handlers
---------------------------------------------------------------------	

function btnSortOnline_OnButtonClicked( buttonHandle )

	clearSelection()

	if g_current_tab == TAB_FRIENDS then

	    local cbh = Dialog_GetCheckBox( gDialogHandle, "cbOffline" )
	
	   	if cbh ~= nil then
	
			-- CheckBox_GetChecked returns 0 or 1, lua treats 0 as empty string
			-- which evaluates to true.  Decide to sort by last online time
	
			if CheckBox_GetChecked( cbh ) ~= 1 then
		       	g_curPage = 1
	    	   	g_set = 1
	
	
				g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX..LOGON_SORT_SUFFIX
	
	            
				if (g_game ~= nil) then
					ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
				end
	
			end
		end
	elseif g_current_tab == TAB_PEOPLE then
		g_curPage = 1
		g_set = 1

		g_web_address = GameGlobals.WEB_SITE_PREFIX..PEOPLE_ZONE_SUFFIX
		
		if (g_game ~= nil) then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
		end	  	
	end
	
    resetPageButtonSelection()
	
end

function btnSortName_OnButtonClicked( buttonHandle )

	clearSelection()

	if g_current_tab == TAB_FRIENDS then
	    local cbh = Dialog_GetCheckBox( gDialogHandle, "cbOffline" )
	
	   	if cbh ~= nil then
	
	       	g_curPage = 1
		   	g_set = 1
	
			-- CheckBox_GetChecked returns 0 or 1, lua treats 0 as empty string
			-- which evaluates to true.  Decide to sort by last online time
	
			if CheckBox_GetChecked( cbh ) == 1 then
				g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX..ONLINE_SUFFIX
			else
				g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX
			end
	
			if (g_game ~= nil) then
				ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
			end
	
		end
	elseif g_current_tab == TAB_PEOPLE then
		g_curPage = 1
		g_set = 1

		g_web_address = GameGlobals.WEB_SITE_PREFIX..PEOPLE_NAME_SUFFIX
		
		if (g_game ~= nil) then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
		end		
				
	end
	
    resetPageButtonSelection()
	
end

function btnConfirmAccept_OnButtonClicked( buttonHandle )

	local userID = -1

	if g_buttonPressed == BTN_IGNORE then
	
		if g_selectedIndex >= 0 then
		   	userID = g_t_friends[g_selectedIndex]["userID"]	--listboxes start at 0
	
			local web_address = GameGlobals.WEB_SITE_PREFIX..IGNORE_SUFFIX
				
			if userID ~= nil then
			
				if g_current_tab == TAB_IGNORE then
			     	web_address = web_address..REMOVE_IGNORE_SUFFIX..userID
				else
					web_address = web_address..ADD_IGNORE_SUFFIX..userID
				end
	
				-- Tell browser handler to use parse result function
				g_resultParse = true
				
				-- Send request to website, use 0 for current page and amount per page
				if (g_game ~= nil) then
					ClientEngine_GetBrowserPage(g_game, web_address, WF.FRIENDS, 0, 0)
				end
			end
		end

	end
	
	showConfirmMenu(false)

end

function btnConfirmCancel_OnButtonClicked( buttonHandle )

	showConfirmMenu(false)	

	local bh = nil
	
	if g_current_tab == TAB_PEOPLE then
	    bh = Dialog_GetButton(gDialogHandle, "btnPeople")
	    setButtonState(bh, true)
	elseif g_current_tab == TAB_FRIENDS then
	    bh = Dialog_GetButton(gDialogHandle, "btnFriends")
	    setButtonState(bh, true)
	else
	    bh = Dialog_GetButton(gDialogHandle, "btnIgnored")
	    setButtonState(bh, not(state))	
	end

end

function btnMessages_OnButtonClicked( buttonHandle )

	if g_selectedIndex >= 0 then
	    name = g_t_friends[g_selectedIndex]["name"]
	    if g_myName ~= name then
			if g_game ~= nil then
				gotoMenu( "SendMessage.xml", 0, 1 )
			end
			local ev = Dispatcher_MakeEvent( KEP.dispatcher, PLAYER_USERID_EVENT )
			Event_EncodeNumber( ev, g_t_friends[g_selectedIndex]["userID"])
			Event_EncodeString( ev, g_t_friends[g_selectedIndex]["name"])
			-- Subject of message
			Event_EncodeString( ev, "" )
			-- Body of message
			Event_EncodeString( ev, "" )
			
			
			Dispatcher_QueueEvent( KEP.dispatcher, ev )
		else
		    createMessage("You cannot send yourself a message.")
		end
	else
		createMessage("Please select a person to send a message.")
	end				

end

function btnIgnore_OnButtonClicked( buttonHandle )

	local userID = nil
	local name = "" --selected name
	
	g_buttonPressed = BTN_IGNORE
	
	if g_selectedIndex >= 0 then
	
	   	userID = g_t_friends[g_selectedIndex]["userID"]	--listboxes start at 0
		name = g_t_friends[g_selectedIndex]["name"]

	    if name ~=  g_myName and userID then

			if g_game ~= nil then
				gotoMenu( "BlockMenu.xml", 1, 1 )
			end

			local ev = Dispatcher_MakeEvent( KEP.dispatcher, PLAYER_USERID_EVENT )
			Event_EncodeNumber( ev, userID)
			Event_EncodeString( ev, name)
			Event_EncodeNumber( ev, 0 ) -- dont kick if use friends menu
			Dispatcher_QueueEvent( KEP.dispatcher, ev )

			DestroyMenu(gDialogHandle)
		else
		    createMessage("You cannot block yourself")
		end

	else
	    createMessage("Please select a user")
	end
--[[
	if g_selectedIndex >= 0 and (g_current_tab == TAB_PEOPLE or g_current_tab == TAB_FRIENDS) then

	   	userID = g_t_friends[g_selectedIndex]["userID"]	--listboxes start at 0
		name = g_t_friends[g_selectedIndex]["name"]

    	if name ~=  g_myName then
    	
			showConfirmMenu(true)
		
		    sh = Dialog_GetStatic(gDialogHandle, "lblConfirmTitle")
		    --setState(sh, state)
			Static_SetText(sh, "Confirm Block")    
		
		    sh = Dialog_GetStatic(gDialogHandle, "lblConfirmDesc")
		    --setState(sh, state)
		    Static_SetText(sh, "Ignoring a person means you cannot get any messages, gifts, or chat from them, and that they cannot travel to you or your home.  You can always unignore them from the Friends and People menu")
	
	
	
	    	sh = Dialog_GetStatic(gDialogHandle, "lblConfirmName")
	   	 	--setState(sh, state)
		    Static_SetText(sh, "Are you sure you want to block "..name)
	    
	    else
			createMessage("You cannot block yourself.")
	    end
	    
	elseif g_selectedIndex >= 0 and g_current_tab == TAB_IGNORE then

	   	userID = g_t_friends[g_selectedIndex]["userID"]	--listboxes start at 0

		local web_address = GameGlobals.WEB_SITE_PREFIX..IGNORE_SUFFIX
			
		if userID ~= nil then
		
	     	web_address = web_address..REMOVE_IGNORE_SUFFIX..userID

			-- Tell browser handler to use parse result function
			g_resultParse = true
			g_buttonPressed = BTN_IGNORE
			
			-- Send request to website, use 0 for current page and amount per page
			if (g_game ~= nil) then
				ClientEngine_GetBrowserPage(g_game, web_address, WF.FRIENDS, 0, 0)
			end
		end
			
	else
		createMessage("Please select a person to add or remove from your block list.")
	end	
]]--

end


-- Sends add or remove friend message to web page
-- action:  string of either "Add" or "Remove"
function sendFriendAction( action )

	userID = g_t_friends[g_selectedIndex]["userID"]	--listboxes start at 0

	if userID ~= nil then
		
		local web_address = GameGlobals.WEB_SITE_PREFIX..UPDATE_SUFFIX..userID..ACTION_SUFFIX..action
	
		-- Tell browser handler to use parse result function
		g_resultParse = true
	
	
		-- Send request to website, use 0 for current page and amount per page
		if (g_game ~= nil) then
			ClientEngine_GetBrowserPage(g_game, web_address, WF.FRIENDS, 0, 0)
		end

		if g_current_tab == TAB_FRIENDS and action == "Remove" then
			if g_game ~= nil then
				ClientEngine_GetBrowserPage(g_game, GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX, WF.FRIENDS, 1, g_friendsPerPage)
			end
			
		end

	end
	
end

function btnAddRemoveFriend_OnButtonClicked( buttonHandle )


	local name = ""

	if g_selectedIndex >= 0 then
        name = g_t_friends[g_selectedIndex]["name"]
 		

        if g_myName ~= name then

			g_buttonPressed = BTN_FRIEND

			if g_current_tab == TAB_FRIENDS then

				gotoMenu( "YesNoBox.xml", 0, 1 )
				local yesnoEvent = Dispatcher_MakeEvent( KEP.dispatcher, YES_NO_BOX_EVENT )
				Event_EncodeString( yesnoEvent, "Are you sure you want to remove "..name.." as a friend?" )
				Event_EncodeString( yesnoEvent, "Confirm" )
				Dispatcher_QueueEvent( KEP.dispatcher, yesnoEvent )
			
		  	elseif g_current_tab == TAB_PEOPLE then

		     	sendFriendAction("Add")

			end
		
		else
		    createMessage("You cannot add yourself as a friend.")
		end

	else
		if g_current_tab == TAB_FRIENDS then
			createMessage("Please select a friend to remove.")
		else
			createMessage("Please select a friend to add.")
		end
	end
	
end

function btnChat_OnButtonClicked( buttonHandle)
	if g_selectedIndex >= 0 then
		--if g_game ~= nil then
		--	gotoMenu( "PrivateMessage.xml", 0, 1 )
		--end
		--local ev = Dispatcher_MakeEvent( KEP.dispatcher, PRIVATE_MESSAGE_EVENT )
		--Event_EncodeNumber( ev, g_t_friends[g_selectedIndex]["ID"])
		--Event_EncodeString( ev, g_t_friends[g_selectedIndex]["name"])
		--Dispatcher_QueueEvent( KEP.dispatcher, ev )
		local starttypemsgevent = Dispatcher_MakeEvent( KEP.dispatcher, TYPE_START_EVENT )	
		Event_EncodeString( starttypemsgevent, "/tell " .. g_t_friends[g_selectedIndex]["name"] .. " " )
		Dispatcher_QueueEvent( KEP.dispatcher, starttypemsgevent )
		DestroyMenu(gDialogHandle)
	else
		createMessage("Please select a person to chat with.")
	end
end
--[[ -- No rave from this menu
function btnRave_OnButtonClicked( buttonHandle )

	local userID = nil

	if g_selectedIndex >= 0 then
	   	userID = g_t_friends[g_selectedIndex]["userID"]	--listboxes start at 0
       --userID = userID["userID"]

		if userID ~= nil then
			local web_address = GameGlobals.WEB_SITE_PREFIX..RAVE_SUFFIX..userID
			
			-- Tell browser handler to use parse result function
	        g_resultParse = true

			-- Send rave to site, use 0 for current page and amount per page
			if (g_game ~= nil) then
				ClientEngine_GetBrowserPage(g_game, web_address, WF.FRIENDS, 0, 0)
			end
       		
		end
	else
		createMessage("Please select a person to rave.")	
	end
		
end
]]--

function btnGift_OnButtonClicked( buttonHandle )

	if g_selectedIndex >= 0 then
	    name = g_t_friends[g_selectedIndex]["name"]
	    if g_myName ~= name then
			if g_game ~= nil then
				gotoMenu( "SendGift.xml", 0, 1 )
			end
			local ev = Dispatcher_MakeEvent( KEP.dispatcher, PLAYER_USERID_EVENT )
			Event_EncodeNumber( ev, g_t_friends[g_selectedIndex]["userID"])
			Event_EncodeString( ev, g_t_friends[g_selectedIndex]["name"])
			Dispatcher_QueueEvent( KEP.dispatcher, ev )
			Dialog_SendToBack( gDialogHandle )
		else
		    createMessage("You cannot send yourself a gift.")
		end
	else
		createMessage("Please select a person to gift.")
	end


end


function btnProfile_OnButtonClicked( buttonHandle)

	if g_selectedIndex >= 0 then
		if g_game ~= nil then
			gotoMenu( "PlayerProfile.xml", 0, 1 )
		end
		local ev = Dispatcher_MakeEvent( KEP.dispatcher, PLAYER_USERID_EVENT )
		Event_EncodeNumber( ev, g_t_friends[g_selectedIndex ]["userID"])
		Event_EncodeString( ev, g_t_friends[g_selectedIndex ]["name"])
		Dispatcher_QueueEvent( KEP.dispatcher, ev )
		Dialog_SendToBack( gDialogHandle )
	else
		createMessage("Please select a person to view their profile.")
	end
	
end


function btnReport_OnButtonClicked( buttonHandle)

	if g_selectedIndex >= 0 then
		if g_game ~= nil then
			gotoMenu( "ReportMisconduct.xml", 0, 1 )
		end
		local ev = Dispatcher_MakeEvent( KEP.dispatcher, PLAYER_USERID_EVENT )
		Event_EncodeNumber( ev, g_t_friends[g_selectedIndex]["userID"])
		Event_EncodeString( ev, g_t_friends[g_selectedIndex]["name"])
		Dispatcher_QueueEvent( KEP.dispatcher, ev )
		Dialog_SendToBack( gDialogHandle )
	end

end

function btnTravel_OnButtonClicked( buttonHandle )

	local friendId = nil

	if g_selectedIndex >= 0 then
	   	friendId = g_t_friends[g_selectedIndex]["userID"]	--listboxes start at 0

	   	if friendId ~= nil then
			Dispatcher_LogMessage( KEP.dispatcher, KEP.ERROR_LOG_LEVEL, "going to friend" )
		
			-- fire event to server telling it where we want to go
			ev = Dispatcher_MakeEvent( KEP.dispatcher, KEP.GOTOPLACE_EVENT_NAME )
			Event_SetFilter( ev, KEP.GOTO_PLAYER ) -- we're going to a player
			Event_EncodeNumber( ev, friendId )
			Event_EncodeNumber( ev, -1 ) -- selected character is unknown now
		
			Event_AddTo( ev, KEP.SERVER_NETID )
			Dispatcher_QueueEvent( KEP.dispatcher, ev )
		
		    setButtonState(buttonHandle, false)
		
			-- close this dialog
			if PROGRESSIVE == 1 then
				DestroyMenu(gDialogHandle)
			end
		end
	else
		createMessage("Please select a person to travel to.")
	end



end

function btnClose_OnButtonClicked( buttonHandle )
	DestroyMenu(gDialogHandle)
end


function btnResultsPrev_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

		calcResultsPrev()

	    alignButtons()

		if (g_game ~= nil) then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
		end

	end
	
	clearSelection()
end

function btnResultsNext_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

        calcResultsNext()
  		alignButtons()

		if (g_game ~= nil) then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
		end

	end
	
	clearSelection()
end


function btnFirst_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

		calcFirst()

	 	alignButtons()

		if (g_game ~= nil) then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
		end
		
	end
	
	clearSelection()
end

function btnLast_OnButtonClicked( buttonHandle )

	if g_pageCap > g_window then

		calcLast()

  		alignButtons()

		if (g_game ~= nil) then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
		end

	end
	
	clearSelection()
end
   
   
function btnRange1_OnButtonClicked( buttonHandle )

	calcRange1()

	if (g_game ~= nil) then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end
	
	clearSelection()
end

function btnRange2_OnButtonClicked( buttonHandle )

	calcRange2()
	
	if (g_game ~= nil) then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end

	clearSelection()
end

function btnRange3_OnButtonClicked( buttonHandle )

	calcRange3()

	if (g_game ~= nil) then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end
	
	clearSelection()
end

function btnRange4_OnButtonClicked( buttonHandle )

    calcRange4()

	if (g_game ~= nil) then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end
	
	clearSelection()
end


function btnFriends_OnButtonClicked( buttonHandle )

	g_current_tab = TAB_FRIENDS
	
	clearSelection()

    ch = Dialog_GetControl(gDialogHandle, "lblBlockCodes")
	Control_SetVisible(ch, false)

    setButtonState(buttonHandle, false)

    bh = Dialog_GetButton(gDialogHandle, "btnPeople")
    setButtonState(bh, true)
    
    bh = Dialog_GetButton(gDialogHandle, "btnIgnored")
    setButtonState(bh, true)

    bh = Dialog_GetButton(gDialogHandle, "btnChat")
    setButtonState(bh, true)

    bh = Dialog_GetButton(gDialogHandle, "btnRave")
    setButtonState(bh, true)

    bh = Dialog_GetButton(gDialogHandle, "btnGift")
    setButtonState(bh, true)
	ch = Button_GetControl(bh)
	Control_SetVisible(ch, true)

    bh = Dialog_GetButton(gDialogHandle, "btnAddRemoveFriend")
    setButtonState(bh, true)

    bh = Dialog_GetButton(gDialogHandle, "btnTravel") 
    setButtonState(bh, true)
	ch = Button_GetControl(bh)
	Control_SetVisible(ch, true)     
    
    bh = Dialog_GetButton(gDialogHandle, "btnMessages")
    setButtonState(bh, true)        

    bh = Dialog_GetButton(gDialogHandle, "btnSearch")
    setButtonState(bh, true)

    bh = Dialog_GetButton(gDialogHandle, "btnSortName")
    setButtonState(bh, true)

	bh = Dialog_GetButton(gDialogHandle, "btnProfile")
	-- mbiggs 05/15/07 no need to hard code location values with new menus
	--Control_SetLocation(Button_GetControl(bh), 110, 260)

    bh = Dialog_GetButton(gDialogHandle, "btnSortOnline")
    setButtonState(bh, true)
    if bh ~= nil then
    	sh = Button_GetStatic(bh)
    	if sh~= nil then
			Static_SetText(bh, "Online")
		end
    end


	ebh = Dialog_GetEditBox( gDialogHandle, "editSearchField" )
	setButtonState(ebh, true)
	if ebh ~= nil then   
		ch = Image_GetControl(ebh)
		if ch ~= nil then
			Control_SetVisible(ch, true)
		end   	
	end		
	EditBox_ClearText( ebh )
--[[	
	sh = Dialog_GetStatic( gDialogHandle, "lblResults" )
	if sh ~= nil then
		Static_SetText(sh, "Showing")
	end
]]--

	cbh = Dialog_GetCheckBox(gDialogHandle, "cbOffline")

   	if cbh ~= nil then
   	   	--CheckBox_SetChecked( cbh, false )
   		setButtonState(cbh, true)
	end
	
  	ih = Dialog_GetImage(gDialogHandle, "imgBgStatusSelect")
	if ih ~= nil then   
		ch = Image_GetControl(ih)
		if ch ~= nil then
			Control_SetVisible(ch, true)
		end   	
	end
	
	-- mbiggs 05/15/07 no need to hard code location values with new menus
--[[
	local bh = Dialog_GetButton(gDialogHandle, "btnAddRemoveFriend")
	local eh = Button_GetNormalDisplayElement(bh)
	
	Element_SetCoords(eh, 137, 201, 160, 225)
	eh = Button_GetMouseOverDisplayElement(bh)
	Element_SetCoords(eh, 137, 201, 160, 225)
	eh = Button_GetFocusedDisplayElement(bh)
	Element_SetCoords(eh, 137, 201, 160, 225)
	eh = Button_GetPressedDisplayElement(bh)
	Element_SetCoords(eh, 137, 201, 160, 225)

	bh = Dialog_GetButton(gDialogHandle, "btnIgnore")
	eh = Button_GetNormalDisplayElement(bh)
	Element_SetCoords(eh, 158, 198, 182, 225)
	eh = Button_GetMouseOverDisplayElement(bh)
	Element_SetCoords(eh, 158, 198, 182, 225)
	eh = Button_GetFocusedDisplayElement(bh)
	Element_SetCoords(eh, 158, 198, 182, 225)
	eh = Button_GetPressedDisplayElement(bh)
	Element_SetCoords(eh, 158, 198, 182, 225)
]]--
	g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX..g_sortType

	if CheckBox_GetChecked( cbh ) == 1 then
		g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX..ONLINE_SUFFIX
	end	

	
	g_curPage = 1
	g_set = 1

    g_InitBtnText = true

    resetPageButtonSelection()

	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end

	
--	ParseFriendData()

end   

-- Need a push to Dev before People Suffix will work
function btnPeople_OnButtonClicked( buttonHandle )

	g_current_tab = TAB_PEOPLE

    clearSelection()
   

    ch = Dialog_GetControl(gDialogHandle, "lblBlockCodes")
	Control_SetVisible(ch, false)

    bh = Dialog_GetButton(gDialogHandle, "btnFriends")
    setButtonState(bh, true)
    
    setButtonState(buttonHandle, false)
    
    bh = Dialog_GetButton(gDialogHandle, "btnIgnored")
    setButtonState(bh, true)

    bh = Dialog_GetButton(gDialogHandle, "btnChat")
    setButtonState(bh, true)

    bh = Dialog_GetButton(gDialogHandle, "btnRave")
    setButtonState(bh, true)

    bh = Dialog_GetButton(gDialogHandle, "btnGift")
    setButtonState(bh, true)

	ch = Button_GetControl(bh)
	Control_SetVisible(ch, true)

    bh = Dialog_GetButton(gDialogHandle, "btnAddRemoveFriend")
    setButtonState(bh, true)

    bh = Dialog_GetButton(gDialogHandle, "btnTravel")
    setButtonState(bh, true)
	ch = Button_GetControl(bh)
	Control_SetVisible(ch, true)     

    bh = Dialog_GetButton(gDialogHandle, "btnMessages")
    setButtonState(bh, true)    
	
    bh = Dialog_GetButton(gDialogHandle, "btnSearch")
    setButtonState(bh, true)

    bh = Dialog_GetButton(gDialogHandle, "btnSortName")
    setButtonState(bh, true)

	-- mbiggs 05/15/07 no need to hard code location values with new menus
    -- bh = Dialog_GetButton(gDialogHandle, "btnProfile")
	-- Control_SetLocation(Button_GetControl(bh), 110, 260)

    bh = Dialog_GetButton(gDialogHandle, "btnIgnore")
    setButtonState(bh, true)
    ch = Button_GetControl(bh)
	Control_SetVisible(ch, true)

    bh = Dialog_GetButton(gDialogHandle, "btnSortOnline")
    setButtonState(bh, true)
    if bh ~= nil then
    	sh = Button_GetStatic(bh)
    	if sh~= nil then
			Static_SetText(bh, "Place")
		end
    end

	ebh = Dialog_GetEditBox( gDialogHandle, "editSearchField" )
	setButtonState(ebh, true)
	if ebh ~= nil then   
		ch = Image_GetControl(ebh)
		if ch ~= nil then
			Control_SetVisible(ch, true)
		end   	
	end		
	EditBox_ClearText( ebh )
--[[	
	sh = Dialog_GetStatic( gDialogHandle, "lblResults" )
	if sh ~= nil then
		Static_SetText(sh, "Showing")
	end
]]--
	cbh = Dialog_GetCheckBox(gDialogHandle, "cbOffline")

   	if cbh ~= nil then
   	   	--CheckBox_SetChecked( cbh, false )
   		setButtonState(cbh, false)
   	end
   	
  	ih = Dialog_GetImage(gDialogHandle, "imgBgStatusSelect")
	if ih ~= nil then   
		ch = Image_GetControl(ih)
		if ch ~= nil then
			Control_SetVisible(ch, false)
		end   	
	end

    -- mbiggs 05/15/07 no need to hard code location values with new menus
--[[
	local bh = Dialog_GetButton(gDialogHandle, "btnAddRemoveFriend")
	local eh = Button_GetNormalDisplayElement(bh)
	Element_SetCoords(eh, 115, 200, 139, 225)
	eh = Button_GetMouseOverDisplayElement(bh)
	Element_SetCoords(eh, 115, 200, 139, 225)
	eh = Button_GetFocusedDisplayElement(bh)
	Element_SetCoords(eh, 115, 200, 139, 225)
	eh = Button_GetPressedDisplayElement(bh)
	Element_SetCoords(eh, 115, 200, 139, 225)

	bh = Dialog_GetButton(gDialogHandle, "btnIgnore")
	eh = Button_GetNormalDisplayElement(bh)
	Element_SetCoords(eh, 158, 198, 182, 225)
	eh = Button_GetMouseOverDisplayElement(bh)
	Element_SetCoords(eh, 158, 198, 182, 225)
	eh = Button_GetFocusedDisplayElement(bh)
	Element_SetCoords(eh, 158, 198, 182, 225)
	eh = Button_GetPressedDisplayElement(bh)
	Element_SetCoords(eh, 158, 198, 182, 225)
]]--

	g_curPage = 1
	g_set = 1

    g_InitBtnText = true

    resetPageButtonSelection()

	g_web_address = GameGlobals.WEB_SITE_PREFIX..PEOPLE_NAME_SUFFIX
	
	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end
	

--	ParseFriendData()

end

-- Switch state to Ignore tab functions
function btnIgnored_OnButtonClicked( buttonHandle )

	g_current_tab = TAB_IGNORE
	
	clearSelection()

    ch = Dialog_GetControl(gDialogHandle, "lblBlockCodes")
	Control_SetVisible(ch, true)

    bh = Dialog_GetButton(gDialogHandle, "btnFriends")
    setButtonState(bh, true)
    
    bh = Dialog_GetButton(gDialogHandle, "btnPeople")
    setButtonState(bh, true)
    
    setButtonState(buttonHandle, false)

    bh = Dialog_GetButton(gDialogHandle, "btnChat")
    setButtonState(bh, false)

    bh = Dialog_GetButton(gDialogHandle, "btnRave")
    setButtonState(bh, false)

    bh = Dialog_GetButton(gDialogHandle, "btnGift")
    setButtonState(bh, false)

	ch = Button_GetControl(bh)
	Control_SetVisible(ch, false)
		

    bh = Dialog_GetButton(gDialogHandle, "btnAddRemoveFriend")
    setButtonState(bh, false)

    bh = Dialog_GetButton(gDialogHandle, "btnTravel")
    setButtonState(bh, false)
	ch = Button_GetControl(bh)
	Control_SetVisible(ch, false)    
    
    bh = Dialog_GetButton(gDialogHandle, "btnMessages")
    setButtonState(bh, false)    
    
    bh = Dialog_GetButton(gDialogHandle, "btnSearch")
    setButtonState(bh, false)

    bh = Dialog_GetButton(gDialogHandle, "btnSortName")
    setButtonState(bh, false)

	bh = Dialog_GetButton(gDialogHandle, "btnProfile")
	-- mbiggs 05/15/07 no need to hard code location values with new menus
	--Control_SetLocation(Button_GetControl(bh), 34, 263)

    bh = Dialog_GetButton(gDialogHandle, "btnSortOnline")
    setButtonState(bh, false)
    if bh ~= nil then
    	sh = Button_GetStatic(bh)
    	if sh~= nil then
			Static_SetText(bh, "Type --- Date")
		end
    end
    
	ebh = Dialog_GetEditBox( gDialogHandle, "editSearchField" )
	setButtonState(ebh, false)
	if ebh ~= nil then   
		ch = Image_GetControl(ebh)
		if ch ~= nil then
			Control_SetVisible(ch, false)
		end   	
	end	
	EditBox_ClearText( ebh )
--[[
	sh = Dialog_GetStatic( gDialogHandle, "lblResults" )
	if sh ~= nil then
		Static_SetText(sh, "Showing")
	end
]]--
	cbh = Dialog_GetCheckBox(gDialogHandle, "cbOffline")

   	if cbh ~= nil then
   	   	--CheckBox_SetChecked( cbh, false )
   		setButtonState(cbh, false)
  	end
  	
  	ih = Dialog_GetImage(gDialogHandle, "imgBgStatusSelect")
	if ih ~= nil then   
		ch = Image_GetControl(ih)
		if ch ~= nil then
			Control_SetVisible(ch, false)
		end   	
	end
	
	-- mbiggs 05/15/07 no need to hard code location values with new menus
--[[
	local bh = Dialog_GetButton(gDialogHandle, "btnIgnore")
	local eh = Button_GetNormalDisplayElement(bh)
	Element_SetCoords(eh, 9, 202, 31, 226)
	eh = Button_GetMouseOverDisplayElement(bh)
	Element_SetCoords(eh, 9, 202, 31, 226)
	eh = Button_GetFocusedDisplayElement(bh)
	Element_SetCoords(eh, 9, 202, 31, 226)
	eh = Button_GetPressedDisplayElement(bh)
	Element_SetCoords(eh, 9, 202, 31, 226)
]]--
	g_curPage = 1
	g_set = 1
	
	g_InitBtnText = true

    resetPageButtonSelection()

	g_web_address = GameGlobals.WEB_SITE_PREFIX..IGNORE_SUFFIX..LIST_IGNORE_SUFFIX
	
	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end

end

-- Grab text from edit box and pass off to search function
function btnSearch_OnButtonClicked( buttonHandle )

    local searchText = ""

	ebh = Dialog_GetEditBox( gDialogHandle, "editSearchField" )
   	if ebh ~= nil then
		searchText = EditBox_GetText( ebh )
   	end

	initSearch( searchText )

end




---------------------------------------------------------------------
-- Helpers
---------------------------------------------------------------------
-- input:  state - a boolean denoting whether or not a menu element should
--					be enabled
function showConfirmMenu( state )

	g_showConfirm = state

    bh = Dialog_GetButton(gDialogHandle, "btnChat")
    setButtonState(bh, not(state))

    bh = Dialog_GetButton(gDialogHandle, "btnRave")
    setButtonState(bh, not(state))

    bh = Dialog_GetButton(gDialogHandle, "btnGift")
    setButtonState(bh, not(state))

    bh = Dialog_GetButton(gDialogHandle, "btnAddRemoveFriend")
    setButtonState(bh, not(state))

    bh = Dialog_GetButton(gDialogHandle, "btnTravel")
    setButtonState(bh, not(state))

    bh = Dialog_GetButton(gDialogHandle, "btnIgnore")
    setButtonState(bh, not(state))
    
    bh = Dialog_GetButton(gDialogHandle, "btnSearch")
    setButtonState(bh, not(state))

    bh = Dialog_GetButton(gDialogHandle, "btnSortName")
    setButtonState(bh, not(state))
    
    bh = Dialog_GetButton(gDialogHandle, "btnFriends")
    setButtonState(bh, not(state))

    bh = Dialog_GetButton(gDialogHandle, "btnPeople")
    setButtonState(bh, not(state))

    bh = Dialog_GetButton(gDialogHandle, "btnIgnored")
    setButtonState(bh, not(state))

    bh = Dialog_GetButton(gDialogHandle, "btnMessages")
    setButtonState(bh, not(state))

    bh = Dialog_GetButton(gDialogHandle, "btnSortOnline")
    setButtonState(bh, not(state))
   
	ebh = Dialog_GetEditBox( gDialogHandle, "editSearchField" )
	setButtonState(ebh, not(state))
	if ebh ~= nil then   
		ch = Image_GetControl(ebh)
		if ch ~= nil then
			Control_SetVisible(ch, not(state))
		end   	
	end	

	ih = Dialog_GetImage(gDialogHandle, "imgConfirmBG")
    setState(ih, state)

    sh = Dialog_GetStatic(gDialogHandle, "lblConfirmTitle")
    setState(sh, state)

    sh = Dialog_GetStatic(gDialogHandle, "lblConfirmName")
    setState(sh, state)

    sh = Dialog_GetStatic(gDialogHandle, "lblConfirmDesc")
    setState(sh, state)

    bh = Dialog_GetStatic(gDialogHandle, "btnConfirmAccept")
    setState(bh, state)

    bh = Dialog_GetStatic(gDialogHandle, "btnConfirmCancel")
    setState(bh, state)


end


-- When the check box is checked or unchecked, change the web query we use to
-- display friends
function cbOffline_OnCheckBoxChanged( )

	clearSelection()

    local cbh = Dialog_GetCheckBox( gDialogHandle, "cbOffline" )

   	if cbh ~= nil then
   	
   	    local ebh = Dialog_GetEditBox( gDialogHandle, "editSearchField" )
		EditBox_ClearText( ebh )

       	g_curPage = 1
       	g_set = 1

		-- CheckBox_GetChecked returns 0 or 1, lua treats 0 as empty string
		-- which evaluates to true.  Decide if offline friends should be shown

		if CheckBox_GetChecked( cbh ) == 1 then
			g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX..ONLINE_SUFFIX
		else
			g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX
		end
	 		
		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
		end 		
		
	end
	
 	g_InitBtnText = true
	
    resetPageButtonSelection()
	
end


-- Grabs text from edit box after enter pressed.  Sends to search function
function editSearchField_OnEditBoxString(ebh, searchText)
	if searchText ~= nil then
		initSearch( searchText )
	end
end


-- Send search request to server
function initSearch( searchText )
	if g_current_tab == TAB_FRIENDS then
	    local cbh = Dialog_GetCheckBox( gDialogHandle, "cbOffline" )
		if cbh ~= nil then
		
			-- If checkbox selected then show only friends online
			if (searchText ~= nil) and (CheckBox_GetChecked( cbh ) == 1) then
				g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX..ONLINE_SUFFIX
			else
				g_web_address = GameGlobals.WEB_SITE_PREFIX..SEARCH_SUFFIX..searchText
			end
			
			g_curPage = 1
			g_set = 1
		
			if g_game ~= nil then
				ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
			end
		end
	elseif g_current_tab == TAB_PEOPLE then
		g_curPage = 1
		g_set = 1

        g_web_address = GameGlobals.WEB_SITE_PREFIX..PEOPLE_SEARCH_SUFFIX..searchText
		
		if g_game ~= nil then
			ClientEngine_GetBrowserPage(g_game, g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
		end	
	end
end


-- Clear listbox selections
function clearSelection()


 	g_selectedIndex = -1
 		
	if g_totalNumRec > 0 then
	
			
		lh = Dialog_GetListBox(gDialogHandle, "lstFriends")
		lhO = Dialog_GetListBox(gDialogHandle, "lstOnline")	
	
		--Fixed in C interface. OLD: If no item selected in listbox and you try to clear
		--list box it throws exception for index out of range.
	
		if lh ~= nil then
			--ListBox_SelectItem( lh, 1 )
			ListBox_ClearSelection(lh)
		end

				
		if lhO ~= nil then
			--ListBox_SelectItem( lhO, 1 )
			ListBox_ClearSelection(lhO)
		end

	end
	
end

---------------------------------------------------------------------
-- Keyboard Handler
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
      -- ESC key
      if key == 27 then
            -- close this menu
            DestroyMenu(gDialogHandle)
      end
end




-- Added by Jonny to handle alignment of the page buttons at the bottom.
-- mbiggs 05/16/07 modified to handle new menus, images are not sent
-- to the rightAlignPageButtons function, the right set of images are
-- hardcoded and the left set of images are located based on the x position
-- the rightAlignPageButtons function returns
function alignButtons()
	local imgRightBar = Dialog_GetImage(gDialogHandle, "right arrow bar")
	Control_SetLocation(imgRightBar, 450, 210)
	local btnRightRightArrow = Dialog_GetButton(gDialogHandle, "btnLast")
	Control_SetLocation(btnRightRightArrow, 432, 210)
    local btnRightArrow = Dialog_GetButton(gDialogHandle, "btnResultsNext")
	Control_SetLocation(btnRightArrow, 412, 210)

	local t = {}		-- new table
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange1"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange2"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange3"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange4"))
	rightAlignPageButtons(t, 407, 212, 5)
	
	local leftMostXPos = Control_GetLocationX(Dialog_GetButton(gDialogHandle, "btnRange1"))

	local btnLeftArrow = Dialog_GetButton(gDialogHandle, "btnResultsPrev")
	local leftArrowWidth = Control_GetWidth(btnLeftArrow)
	leftMostXPos = leftMostXPos - leftArrowWidth - 5
	Control_SetLocation(btnLeftArrow, leftMostXPos, 210)
	
	local btnLeftLeftArrow = Dialog_GetButton(gDialogHandle, "btnFirst")
	local leftLeftArrowWidth = Control_GetWidth(btnLeftLeftArrow)
	leftMostXPos = leftMostXPos - leftLeftArrowWidth - 5
	Control_SetLocation(btnLeftLeftArrow, leftMostXPos, 210)

	local imgLeftBar = Dialog_GetImage(gDialogHandle, "left arrow bar")
	local leftBarWidth = Control_GetWidth(imgLeftBar)
    leftMostXPos = leftMostXPos - leftBarWidth - 2
	Control_SetLocation(imgLeftBar, leftMostXPos, 210)

end

---------------------------------------------------------------------
-- Mouse Over
---------------------------------------------------------------------
function getButtonHandles()
--g_t_btnHandles

	local ch = Dialog_GetControl(gDialogHandle, "btnClose")
	if ch ~= nil then
		g_t_btnHandles["btnClose"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnFriends")
	if ch ~= nil then
     	g_t_btnHandles["btnFriends"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnPeople")
	if ch ~= nil then
     	g_t_btnHandles["btnPeople"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnIgnored")
	if ch ~= nil then
     	g_t_btnHandles["btnIgnored"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSearch")
	if ch ~= nil then
     	g_t_btnHandles["btnSearch"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSortName")
	if ch ~= nil then
     	g_t_btnHandles["btnSortName"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSortOnline")
	if ch ~= nil then
     	g_t_btnHandles["btnSortOnline"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnChat")
	if ch ~= nil then
     	g_t_btnHandles["btnChat"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnRave")
	if ch ~= nil then
     	g_t_btnHandles["btnRave"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnGift")
	if ch ~= nil then
     	g_t_btnHandles["btnGift"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnAddRemoveFriend")
	if ch ~= nil then
     	g_t_btnHandles["btnAddRemoveFriend"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnProfile")
	if ch ~= nil then
     	g_t_btnHandles["btnProfile"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnIgnore")
	if ch ~= nil then
     	g_t_btnHandles["btnIgnore"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnReport")
	if ch ~= nil then
     	g_t_btnHandles["btnReport"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnTravel")
	if ch ~= nil then
     	g_t_btnHandles["btnTravel"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnConfirmAccept")
	if ch ~= nil then
     	g_t_btnHandles["btnConfirmAccept"] = ch
	end
	
	ch = Dialog_GetControl(gDialogHandle, "btnConfirmCancel")
	if ch ~= nil then
     	g_t_btnHandles["btnConfirmCancel"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnMessages")
	if ch ~= nil then
     	g_t_btnHandles["btnMessages"] = ch
	end

end

function Dialog_OnMouseMove(dialogHandle, x, y)

    local sh = Dialog_GetStatic(dialogHandle, "lblStatus")

	if sh ~= nil then

		if g_showConfirm then
		
			if Control_ContainsPoint(g_t_btnHandles["btnConfirmAccept"], x, y) ~= 0 then
			   	Static_SetText(sh, "Block This Person")
			elseif Control_ContainsPoint(g_t_btnHandles["btnConfirmCancel"], x, y) ~= 0 then
			   	Static_SetText(sh, "Cancel Block")
			end
		
		else
		
			if Control_ContainsPoint(g_t_btnHandles["btnFriends"], x, y) ~= 0 then
			    Static_SetText(sh, "View Your Current Friends")
			elseif Control_ContainsPoint(g_t_btnHandles["btnPeople"], x, y) ~= 0 then
			    Static_SetText(sh, "View People Online Now")
			elseif Control_ContainsPoint(g_t_btnHandles["btnIgnored"], x, y) ~= 0 then
			    Static_SetText(sh, "View People on Your Block List")
			elseif Control_ContainsPoint(g_t_btnHandles["btnSearch"], x, y) ~= 0 then
				Static_SetText(sh, "Search for Friends or People")
			elseif Control_ContainsPoint(g_t_btnHandles["btnSortName"], x, y) ~= 0 then
			    Static_SetText(sh, "Sort Results by Name")
			elseif Control_ContainsPoint(g_t_btnHandles["btnSortOnline"], x, y) ~= 0 then
			    Static_SetText(sh, "Sort Results by Online Status")
			elseif Control_ContainsPoint(g_t_btnHandles["btnChat"], x, y) ~= 0 then
			    if g_current_tab ~= TAB_IGNORE then
					Static_SetText(sh, "Chat With This Person")
				end
			elseif Control_ContainsPoint(g_t_btnHandles["btnRave"], x, y) ~= 0 then
			    Static_SetText(sh, "Rave This Person")
			elseif Control_ContainsPoint(g_t_btnHandles["btnGift"], x, y) ~= 0 then
			    if g_current_tab ~= TAB_IGNORE then
			    	Static_SetText(sh, "Send Selected Person a 3D Gift")
			    end
			elseif Control_ContainsPoint(g_t_btnHandles["btnProfile"], x, y) ~= 0 then
			    Static_SetText(sh, "View This Person's Profile")
			elseif Control_ContainsPoint(g_t_btnHandles["btnAddRemoveFriend"], x, y) ~= 0 then
				if g_current_tab == TAB_PEOPLE then
				    Static_SetText(sh, "Add This Person as a Friend")				
				elseif g_current_tab == TAB_FRIENDS then
				    Static_SetText(sh, "Remove This Person as a Friend")
				end
				
			elseif Control_ContainsPoint(g_t_btnHandles["btnIgnore"], x, y) ~= 0 then
				    Static_SetText(sh, "Change the Block Settings for this Person")
			elseif Control_ContainsPoint(g_t_btnHandles["btnReport"], x, y) ~= 0 then
			    	Static_SetText(sh, "Report This Person for Misconduct")
			elseif Control_ContainsPoint(g_t_btnHandles["btnTravel"], x, y) ~= 0 then
				if g_current_tab == TAB_IGNORE then
				    Static_SetText(sh, "View This Person's Profile")
	   		    elseif g_current_tab ~= TAB_IGNORE  then
			    	Static_SetText(sh, "Travel to This Person's Location")
				end
			elseif Control_ContainsPoint(g_t_btnHandles["btnMessages"], x, y) ~= 0 then
				if g_current_tab ~= TAB_IGNORE then
			    	Static_SetText(sh, "Send Selected Person a Message")
			    end
			else
	            Static_SetText(sh, "Mouse Over a Button for Information")
			end
		end
		
		if Control_ContainsPoint(g_t_btnHandles["btnClose"], x, y) ~= 0 then
			Static_SetText(sh, "Close This Menu")
  		end
		
	end

end
