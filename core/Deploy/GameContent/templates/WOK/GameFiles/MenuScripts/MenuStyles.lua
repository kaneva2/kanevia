-- States for elements

DXUT_STATE_NORMAL = 0
DXUT_STATE_DISABLED = 1
DXUT_STATE_HIDDEN = 2
DXUT_STATE_FOCUS = 3
DXUT_STATE_MOUSEOVER = 4
DXUT_STATE_PRESSED = 5

---------------------------------------------------------------------
--
-- Page Button Styles 
--
---------------------------------------------------------------------
PB = {}

--Button Colors
--PB.SELECT_COLOR = 4278217429
--PB.DESELECT_COLOR = 4278190080
PB.SELECT_COLOR = 4278217429 --Black
PB.DESELECT_COLOR = 4294967295 --White

-- Font
PB.FONT = "Verdana"
--FontWeight.Normal == 3
PB.SELECT_POINT = -13
PB.DESELECT_POINT = -11
PB.WEIGHT = 400
PB.ITALIC = false

---------------------------------------------------------------------
--
-- List Box Styles 
--
---------------------------------------------------------------------
LB = {}

-- Font
LB.FONT = "Verdana"
LB.SELECT_HEIGHT = -11
LB.MAIN_WEIGHT = 400
LB.SEL_WEIGHT = 700
LB.ITALIC = false

-- mbiggs 05/15/07 Changed main font color from black to white for new menus
--LB.MAIN_FONT_COLOR = 4278190080 --Black
LB.MAIN_FONT_COLOR = 4294967295 --White
LB.SEL_FONT_COLOR = 4294967295  --White

-- Parameters: dialog handle (dh) and listbox handle (lbh)
function setListBoxStyle( lbh )

	if lbh ~= nil then

		-- get the dialog handle for this control
		local ch = ListBox_GetControl( lbh )
		local dh = Control_GetDialog( ch )

		if dh ~= nil then

        	-- Main Display
        	
        	local eh = ListBox_GetMainDisplayElement( lbh )
			local bch = Element_GetFontColor(eh)

	        Element_AddFont( eh, dh, LB.FONT, LB.SELECT_HEIGHT, LB.MAIN_WEIGHT, LB.ITALIC )
		    BlendColor_SetColor( bch, DXUT_STATE_NORMAL, LB.MAIN_FONT_COLOR )

			-- Selection Display

            eh = ListBox_GetSelectionDisplayElement( lbh )
			bch = Element_GetFontColor(eh)

	        Element_AddFont( eh, dh, LB.FONT, LB.SELECT_HEIGHT, LB.SEL_WEIGHT, LB.ITALIC )
		    BlendColor_SetColor( bch, DXUT_STATE_NORMAL, LB.SEL_FONT_COLOR )
		    
		    eh = ListBox_GetMainDisplayElement( lbh )
			bch = Element_GetFontColor(eh)

	        Element_AddFont( eh, dh, LB.FONT, LB.SELECT_HEIGHT, LB.MAIN_WEIGHT, LB.ITALIC )
		    BlendColor_SetColor( bch, DXUT_STATE_NORMAL, LB.MAIN_FONT_COLOR )

			
	--[[	
			local ehN = Button_GetNormalDisplayElement(buttonHandle)
			local bchN = Element_GetFontColor(ehN)
	
			local ehF = Button_GetFocusedDisplayElement(buttonHandle)
			local bchF = Element_GetFontColor(ehF)
			
	        Element_AddFont( ehN, gDialogHandle, PB.FONT, PB.SELECT_POINT, PB.WEIGHT, PB.ITALIC )
		    BlendColor_SetColor( bchN, PB.DXUT_STATE_NORMAL, PB.SELECT_COLOR )
	
	        Element_AddFont( ehF, gDialogHandle, PB.FONT, PB.SELECT_POINT, PB.WEIGHT, PB.ITALIC )
		    BlendColor_SetColor( bchF, PB.DXUT_STATE_FOCUS, PB.SELECT_COLOR )
	]]--		
		end

	end

end