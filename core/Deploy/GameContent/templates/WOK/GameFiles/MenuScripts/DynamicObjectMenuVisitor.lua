dofile("MenuHelper.lua")
dofile("..\\Scripts\\BrowserIds.lua")
dofile("..\\Scripts\\MovementIds.lua")
dofile("..\\Scripts\\CameraObjIds.lua")
dofile("..\\Scripts\\GameGlobals.lua")
--dofile("InventoryDescriptions.lua")
dofile("CommonFunctions.lua")

DYNAMIC_OBJ_MENU_EVENT = "DynamicObjectMenuEvent"
PLAYLIST_MENU_EVENT = "PlayListMenuEvent"
CLOSE_DYNMENU_EVENT = "CloseDynMenuEvent"
GLID_QUERY_SUFFIX = "kgp/dynamicObjectInfo.aspx?placementId="


g_web_address = GameGlobals.WEB_SITE_PREFIX..GLID_QUERY_SUFFIX

-- id of the dynamic placement obj
g_objId = -1
g_kanevaUserId = -1
g_Dispatcher = nil
g_objName = nil
g_objCanPlayMovie = 0
g_objIsAttachable = 0
g_objDescription = "None"

g_game = nil
g_dispatcher = nil
g_confirmation = 0

g_tv = true

g_x = 0
g_y = 0

FLASH_MAX_VOL = 65535
FLASH_VOL_STEPS = 15

--genereated in OnCreate
g_t_TV_volumes = {}
g_t_TV_vol_rev = {}

g_cur_flash_vol = #g_t_TV_volumes


function generateLookupTables()

	local step = FLASH_MAX_VOL / FLASH_VOL_STEPS

	g_t_TV_volumes[1] = 0
	g_t_TV_vol_rev["0"] = 0

	for i=2,FLASH_VOL_STEPS do
		g_t_TV_volumes[i] = step * i
		g_t_TV_vol_rev[tostring(step * i)] = i - 1
	end

end

function showVolume()


	local result, isOk, vol

	isOk, vol = ClientEngine_GetFlashVolumeOnDynamicObject( g_game, g_objId )

	---Dialog_MsgBox("isOk: "..tostring(isOk).."  vol: "..tostring(vol))

	local sh = Dialog_GetStatic(gDialogHandle, "stVol")
	if isOk == 1.0  and vol > 0 then
	    vol = tostring( g_t_TV_vol_rev[tostring(vol)] )
		Static_SetText( sh, tostring(vol) )
	else
		Static_SetText( sh, "0" )  -- error, so no volume
	end

	return tonumber(vol)

end

function getCurrentRave()

	local result, isOk, ret
	ret = 1 -- default to true, indicating they can't rave
	local state = false
	
	isOk, result = ClientEngine_CallFlashFunctionOnDynamicObject( g_game, g_objId, "<invoke name=\"GetRaveState\" returntype=\"xml\"><arguments/></invoke>" )
	local btn = Button_GetControl( Dialog_GetButton(gDialogHandle, "btnRave") )
	if isOk == 1.0 then
		-- parse out the text
		-- result is <false/> or <true/>
		if string.find( result, "false") ~= nil then
			--Control_SetEnabled( btn, true )
			state = true
			ret = 0
			Static_SetText(Button_GetStatic(btn), "Rave Video")
		else
			Static_SetText(Button_GetStatic(btn), "Already Raved")
		end	
	end

	Control_SetEnabled( btn, state )

	return ret


end

function updateId(dispatcher, fromNetid, event, eventid, filter, objectid)	
	local result, isOk
	g_objId  = Event_DecodeNumber( event );
	g_objName = Event_DecodeString( event );
	g_objCanPlayMovie = Event_DecodeNumber( event );
    g_objIsAttachable = Event_DecodeNumber( event );
    g_objDescription = Event_DecodeString( event );


	Static_SetText(Dialog_GetStatic(gDialogHandle, "Static1"), g_objName)
		
    	Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, ">>>>>setting id = "..g_objId ) 	

	if g_objCanPlayMovie ~= 1.0 then
		g_tv = false
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnRave"))
		Control_SetVisible(bc, false)
		Control_SetEnabled(bc, false)
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnVolDn"))
		Control_SetVisible(bc, false)
		Control_SetEnabled(bc, false)
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnVolUp"))
		Control_SetVisible(bc, false)
		Control_SetEnabled(bc, false)
		sc = Static_GetControl(Dialog_GetStatic(gDialogHandle, "Static21"))
		Control_SetVisible(sc, false)
		sc = Static_GetControl(Dialog_GetStatic(gDialogHandle, "stVol"))
		Control_SetVisible(sc, false)
	else
		g_tv = true
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnRave"))
		Control_SetVisible(bc, true)
		Control_SetEnabled(bc, true)
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnVolDn"))
		Control_SetVisible(bc, true)
		Control_SetEnabled(bc, true)
		bc = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnVolUp"))
		Control_SetVisible(bc, true)
		Control_SetEnabled(bc, true)
		sc = Static_GetControl(Dialog_GetStatic(gDialogHandle, "Static21"))
		Control_SetVisible(sc, true)
		sc = Static_GetControl(Dialog_GetStatic(gDialogHandle, "stVol"))
		Control_SetVisible(sc, true)
	end
	if g_tv == true then
  		g_cur_flash_vol = showVolume()
		getCurrentRave()
	end
	if g_game ~= nil then
		ClientEngine_GetBrowserPage(g_game, g_web_address..g_objId, WF.OBJECTINFO, 0, 0)
	end
	return KEP.EPR_OK
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "updateId", 1,0,0,0, DYNAMIC_OBJ_MENU_EVENT, KEP.HIGH_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "CloseDynObjHandler", 1,0,0,0, CLOSE_DYNMENU_EVENT, KEP.HIGH_PRIO )
	ParentHandler_RegisterEventHandler( dispatcher, handler, "BrowserPageReadyHandler", 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO)

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, DYNAMIC_OBJ_MENU_EVENT, KEP.MED_PRIO )
   	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "BrowserPageReadyEvent", KEP.MED_PRIO)	
	g_Dispatcher = dispatcher

end

function CloseDynObjHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	--Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "brgin event handler... "..filter )
	local oldHandle = Event_DecodeNumber(event)


	if oldHandle == gDialogHandle then
		DestroyMenu(gDialogHandle)
	end

	return KEP.EPR_CONSUMED

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
  
  	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end	
	
	g_game = Dispatcher_GetGame(KEP.dispatcher)
	
	if ClientEngine_SetMenuSelectMode ~= nil then
		ClientEngine_SetMenuSelectMode( g_game, 1.0 )
	end

	if g_game == 0 then
		g_game = nil
	end
	
	generateLookupTables()
	
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
	
	if ClientEngine_SetMenuSelectMode ~= nil then
		ClientEngine_SetMenuSelectMode( g_game, 0.0 )
	end
	
end


---------------------------------------------------------------------
--
-- Cancel Button - cancel and discard changes
--
---------------------------------------------------------------------
function btnCancel_OnButtonClicked( buttonHandle )				
	DestroyMenu(gDialogHandle)
end


-- Comment out future want to view but no change playlist?  --Jonny
--function btnPlaylist_OnButtonClicked( buttonHandle )
--
--	Dispatcher_LogMessage( g_dispatcher, KEP.ERROR_LOG_LEVEL, "changing playlist..." ) 	
--	
--	if ClientEngine_GetPlayer == nil then
--		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
--	end
--
--	Dialog_Create( "PlaylistSelect.xml" );
--
--	--fire event to pass placement id to the menu script
--	menuOpenEvent = Dispatcher_MakeEvent( KEP.dispatcher, PLAYLIST_MENU_EVENT )
--	
--	Event_EncodeNumber( menuOpenEvent, g_objId )
--	Event_EncodeNumber( menuOpenEvent, g_kanevaUserId )	
--	Dispatcher_QueueEvent( KEP.dispatcher, menuOpenEvent )
--  
--end 

function btnVolDn_OnButtonClicked( buttonHandle )

	if g_cur_flash_vol >= 1 then
		g_cur_flash_vol = g_cur_flash_vol - 1
		vol = g_t_TV_volumes[g_cur_flash_vol]
		ClientEngine_SetFlashVolumeOnDynamicObject(g_game, g_objId, vol)
		--ClientEngine_CallFlashFunctionOnDynamicObject( g_game, g_objId, "<invoke name=\"SetFLVVolume\" returntype=\"xml\"><arguments><number>"..tostring(vol).."</number></arguments/></invoke>" )
	end

	showVolume()

end

function btnVolUp_OnButtonClicked( buttonHandle )

	if g_cur_flash_vol < FLASH_VOL_STEPS then
		g_cur_flash_vol = g_cur_flash_vol + 1
		ClientEngine_SetFlashVolumeOnDynamicObject(g_game, g_objId, g_t_TV_volumes[g_cur_flash_vol])
		--ClientEngine_CallFlashFunctionOnDynamicObject( g_game, g_objId, "<invoke name=\"SetFLVVolume\" returntype=\"xml\"><arguments><number>"..tostring(vol).."</number></arguments/></invoke>" )
	end

	showVolume()


end


function btnRave_OnButtonClicked( buttonHandle )

	ClientEngine_CallFlashFunctionOnDynamicObject( g_game, g_objId, "<invoke name=\"RaveItem\" returntype=\"xml\"><arguments></arguments/></invoke>" )
	createMessage( "Item Raved!" )
	getCurrentRave();

	
	--btnCancel_OnButtonClicked( buttonHandle )

end 



---------------------------------------------------------------------
-- Keyboard Handler
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
      -- ESC key
      if key == 27 then
            -- close this menu
            DestroyMenu(gDialogHandle)
      end
end

-- Catch the message counts 
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.OBJECTINFO then
		-- xml containing the requested counts
		local estr = Event_DecodeString( event )
		ParseCountData(estr) 
		return KEP.EPR_CONSUMED
	end
end

function ParseCountData( browserString )
    	local globaluniqueid
	local marketprice
	local texname
	local left
	local top
	local right
	local bottom
	s, strPos, result = string.find(browserString, "<ReturnCode>([0-9]+)</ReturnCode>")	
	if result == "0" then
		if g_game ~= nil then
			s, strPos, globaluniqueid = string.find(browserString, "<global_id>([0-9]+)</global_id>", strPos)
			s, strPos, marketprice = string.find(browserString, "<market_cost>([0-9]+)</market_cost>", strPos)
		end
	end
	sh = Dialog_GetStatic(gDialogHandle, "lblDescription")
	Static_SetText(sh, g_objDescription.. "\nRetail Price: " .. marketprice)	
	texname, left, top, right, bottom = ClientEngine_GetItemInfo(g_game, globaluniqueid)
	--Dialog_MsgBox(texname .."("..math.floor(left)..","..math.floor(top)..","..math.floor(right)..",".. math.floor(bottom)..")")
	eh = Image_GetDisplayElement(Dialog_GetImage(gDialogHandle, "imgIcon"))
	Element_AddTexture(eh, gDialogHandle, texname)
	Element_SetCoords(eh, math.floor(left), math.floor(top), math.floor(right), math.floor(bottom))
end

