dofile("MenuHelper.lua")



---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "ConfirmationCloseEvent", KEP.MED_PRIO )

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	-- TODO init here
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	Helper_Dialog_OnDestroy( dialogHandle )
end


---------------------------------------------------------------------
--
-- Generate event to indicate this menu was should be closed
--
---------------------------------------------------------------------
function ConfirmationClose(value)
	
	event = Dispatcher_MakeEvent( KEP.dispatcher, "ConfirmationCloseEvent" )

	Event_EncodeNumber( event, value )

	Dispatcher_QueueEvent( KEP.dispatcher, event )
end


---------------------------------------------------------------------
--
-- Yes Clicked
--
---------------------------------------------------------------------
function btnYes_OnButtonClicked(buttonHandle)

	ConfirmationClose(1)
end


---------------------------------------------------------------------
--
-- No Clicked
--
---------------------------------------------------------------------
function btnNo_OnButtonClicked(buttonHandle)

	ConfirmationClose(0)
end

---------------------------------------------------------------------
--
-- Keyboard Handler
--
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)

	-- ESC key
	if key == 27 then

		ConfirmationClose(0)
	end

end

