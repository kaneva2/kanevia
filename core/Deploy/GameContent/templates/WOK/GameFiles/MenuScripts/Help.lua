g_help = false;
function HideHelp()
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static1"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static2"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static3"), false )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static4"), false )
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image1"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image2"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image3"),false);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image4"),false);
end

function ShowHelp()
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static1"), true )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static2"), true )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static3"), true )
    Control_SetVisible(Dialog_GetStatic(gDialogHandle,"Static4"), true )
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image1"),true);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image2"),true);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image3"),true);
    Control_SetVisible(Dialog_GetImage(gDialogHandle,"Image4"),true);
end
