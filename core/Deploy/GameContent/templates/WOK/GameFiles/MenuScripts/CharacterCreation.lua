dofile("MenuHelper.lua")
dofile("..\\ClientScripts\\MenuGameIds.lua" )
dofile("..\\Scripts\\ClientEngineIds.lua")
dofile("CharacterHelper.lua")

-- global game object
g_game = nil

g_menu_game_obj = nil

-- Back button pressed
g_backButton = false


maleStartHeight = 70
femaleStartHeight = 68

-- Keeps track of the height of the character
currentScaleY = 100

-- used for rotating character
g_bRotate = false
g_prevMousePointX = 0
g_prevMousePointY = 0
g_cur_rotation_X = 0
g_cur_rotation_Y = 0
g_3dmodel_width = 0
g_3dmodel_height = 0

FEMALE = 1
MALE = 2
SKINNYCHICK = 3
MEDCHICK = 4
HUGECHICK = 5
SKINNYDUDE = 6
MEDDUDE = 7
HUGEDUDE = 8

STARTER_APT_SPAWN = 1

-- value passed in from the tribe selection menu
g_teamID 	= 1
g_gender    = FEMALE

IdleAnimationID = {0,0}

g_ActorID          = {0,0,3,4,5,6,7,8}

HeadConfigID       = { 6, 6, 0, 0, 0, 0, 0, 0}
NeckConfigID       = {99,99, 1, 1, 1, 1, 1, 1}
TorsoConfigID	   = { 7, 7, 2, 2, 2, 2, 2, 2}
CrotchConfigID 	   = {99,99, 3, 3, 3, 3, 3, 3} -- Old actors don't have crotches
ArmsConfigID	   = {99,99, 4, 4, 4, 4, 4, 4}
LegsConfigID	   = { 8, 8, 5, 5, 5, 5, 5, 5}
HandsConfigID	   = {99,99, 6, 6, 6, 6, 6, 6}
FeetConfigID	   = {14, 9, 7, 7, 7, 7, 7, 7}
ShirtConfigID      = { 9,10, 8, 8, 8, 8, 8, 8}
PantsConfigID      = {11,12, 9, 9, 9, 9, 9, 9}
BootsConfigID      = {12,13,10,10,10,10,10,10}
EyesConfigID       = { 3, 0,11,11,11,11,11,11}
HairConfigID       = { 0, 1,12,12,12,12,12,12}
FacialConfigID     = { 4, 5,13,13,13,13,13,13}
LipsConfigID	   = {99,99,14,14,14,99,99,99}

-- state of the arrows
g_arrow = CLEAR

-- indicates the state of the arrow buttons.
CLEAR 		= 0	-- No arrow is down.
CLICKED_LEFT 	= 1	-- Left arrow is clicked.
CLICKED_RIGHT 	= 2	-- Right arrow is clicked.
HELD_LEFT 	= 3	-- Left arrow is held down for sustained period.
HELD_RIGHT 	= 4	-- Right arrow is held down for sustained period.

-- threshold for holding the mouse down (seconds)
ARROWCLICK_REPEAT = 0.005

-- threshold for time before holding the mouse down (seconds)
ARROWCLICK_DELAY = 0.22

-- used for changing character config
g_t_shirt = {}	-- table with config indices
g_cur_shirt_index = 1 -- tables are 1-based!!

g_t_head = {}
g_cur_head_index = 1

g_t_hair = {}
g_cur_hair_index = 1

g_t_eyes = {}
g_cur_eyes_index = 1

g_t_pants = {}
g_cur_pants_index = 1

g_t_boots = {}
g_cur_boots_index = 0

g_t_facial = {}
g_cur_facial_index = 0

g_t_eyes = {}
g_cur_eyes_index = 1

g_cur_hair_color_index = 0
g_cur_skin_color_index = 0
g_cur_eye_color_index = 0
g_cur_facial_color_index = 0
g_cur_lip_color_index = 0

g_cur_body_index = 1

HAIR_MAX = 11 -- 11 hairs starting at 0
SKIN_MAX = 8 -- 8 hairs starting at 0
EYE_MAX = 8 -- 8 eyes starting at 0
FACIAL_MAX = 12 -- index starts at 0

FACIALHAIR_TOTAL = 16 -- number of possible facial hair meshes per body type

BODY_MAX = 3

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	CharacterHelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel, KEP.MED_PRIO+1 )	
--	ParentHandler_RegisterEventHandler( dispatcher, handler, "initMenuHandler", 1,0,0,0, "InitMenuEvent", KEP.MED_PRIO )	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	CharacterHelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "DialogResizedEvent", KEP.MED_PRIO )
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	if Dialog_Create == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_MENU_FN )
	end

	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	if g_game == 0 then
		g_game = nil
	end

	gsGame = Dispatcher_GetGameAsGetSet( KEP.dispatcher );
	
	name = GetSet_GetString( gsGame, ClientEngineIds.LOGINNAME )
	gender = GetSet_GetString( gsGame, ClientEngineIds.LOGINGENDER )
	if gender == "F" then 
		g_gender = MEDCHICK
	else
		g_gender = MEDDUDE
	end
	
	local EditBoxHandle ebh = Dialog_GetEditBox(gDialogHandle, "editName")
	local name = EditBox_SetText(ebh, name, false )
	
	-- disable the ok button
	--local ControlHandle ch = Dialog_GetControl(dialogHandle, "btnNext")
	--Control_SetEnabled(ch, false)

	-- set focus to the default control (should be editName)
	Dialog_FocusDefaultControl( dialogHandle)

	--reset the indexes
	g_cur_shirt_index  = 1 
	g_cur_head_index   = 1
	g_cur_hair_index   = 1
	g_cur_eyes_index   = 1
	g_cur_pants_index  = 1
	g_cur_boots_index  = 0
	g_cur_facial_index = 0
	g_cur_eyes_index   = 1	
	setupActor()
	
	
	-- set scale factors, and set range to be 80% - 120%
	local SliderHandle handlesliderY = Dialog_GetSlider(gDialogHandle, "sliderY")	
	Slider_SetRange( handlesliderY, 90, 110 )
	Slider_SetValue( handlesliderY, 100 )	
	
	local SliderHandle handlesliderXZ = Dialog_GetSlider(gDialogHandle, "sliderXZ")	
	Slider_SetRange( handlesliderXZ, 90, 110 )
	Slider_SetValue( handlesliderXZ, 100 )
	
	local sh = Dialog_GetStatic(gDialogHandle, "lblFacialHairColorTotal")
	if sh ~= nil then
        Static_SetText(sh, FACIAL_MAX)
	end


	sh = Dialog_GetStatic(gDialogHandle, "lblFacialHairTotal")
	if sh ~= nil then
	    if (g_gender == SKINNYCHICK) or (g_gender == MEDCHICK) or (g_gender == HUGECHICK) then
            Static_SetText(sh, FACIAL_MAX)
		else
        	Static_SetText(sh, FACIALHAIR_TOTAL + 1)
		end
	end

	
	sh = Dialog_GetStatic(gDialogHandle, "lblEyesTotal")
	if sh ~= nil then
        Static_SetText(sh, EYE_MAX)
	end
	
	sh = Dialog_GetStatic(gDialogHandle, "lblHairColorTotal")
	if sh ~= nil then
        Static_SetText(sh, HAIR_MAX)
	end

    sh = Dialog_GetStatic(gDialogHandle, "lblFaceTotal")
    if sh ~= nil then
    	Static_SetText(sh, #g_t_head)
    end

    sh = Dialog_GetStatic(gDialogHandle, "lblHairTotal")
    if sh ~= nil then
    	Static_SetText(sh, #g_t_hair)
    end
    
    sh = Dialog_GetStatic(gDialogHandle, "lblSkinTotal")
    if sh ~= nil then
    	Static_SetText(sh, SKIN_MAX)
    end

    sh = Dialog_GetStatic(gDialogHandle, "lblBodyTotal")
    if sh ~= nil then
    	Static_SetText(sh, BODY_MAX)
    end

    
    local SliderHandle sh = Dialog_GetSlider(gDialogHandle, "sliderY")
    local sliderY = Slider_GetValue( sh )
    updateHeightLabel( sliderY )
    
end

function Dialog_OnMoved()
	moveActor()
end

function moveActor()
	if g_menu_game_obj ~= nil then
		local x 	= 0
		local y 	= 0
		local dlg_x = Dialog_GetLocationX(gDialogHandle)
		local dlg_y = Dialog_GetLocationY(gDialogHandle)
		local sh 	= Dialog_GetStatic(gDialogHandle, "lblModel")
		if sh ~= 0 then
			x 		= Control_GetLocationX(sh)
			y 		= Control_GetLocationY(sh)
			g_3dmodel_width = Control_GetWidth(sh)
			g_3dmodel_height= Control_GetHeight(sh)
		end
		GetSet_SetVector(g_menu_game_obj, MenuGameIds.VP_POSITION, dlg_x + x - 300, dlg_y + y, 0)
	end
end

function setupActor()

	local fst = Dialog_GetStatic(gDialogHandle, "lblFacialHair")
	local fsc = Dialog_GetStatic(gDialogHandle, "lblFacialHairColor")
	local fim = Dialog_GetImage(gDialogHandle, "imgFacialHairBG")
	local fbk = Dialog_GetButton(gDialogHandle, "btnFacialHairBack")
	local fne = Dialog_GetButton(gDialogHandle, "btnFacialHairNext")
	local hug = Dialog_GetButton(gDialogHandle, "btnHuge")
	if g_gender == FEMALE or g_gender == HUGECHICK or g_gender == MEDCHICK or g_gender == SKINNYCHICK then
        Static_SetText(fst, "Lip Color")
		Static_SetText(fsc, "Makeup Color")
		Static_SetText(hug, "Curvy")
	else
		Static_SetText(fst, "Facial Hair")
		Static_SetText(fsc, "Facial Hair Color")
	end

	if g_game ~= nil then

		local actorID = -1

		actorID = g_ActorID[g_gender]

		if g_game ~= nil and g_menu_game_obj ~= nil then
			ClientEngine_RemoveMenuGameObject(g_game, g_menu_game_obj )
		end

        --Dialog_MsgBox("actorID: "..tostring(actorID).."   IdleAnimationID[g_gender]: "..tostring(IdleAnimationID[g_gender]).."   g_gender:  "..tostring(g_gender))

		g_menu_game_obj = ClientEngine_AddMenuGameObject(g_game, actorID, IdleAnimationID[g_gender])

		local dlg_x = Dialog_GetLocationX(gDialogHandle)
		local dlg_y = Dialog_GetLocationY(gDialogHandle)

		local x 	= 0
		local y 	= 0
		local sh 	= Dialog_GetStatic(gDialogHandle, "lblModel")
		if sh ~= 0 then
			x 		= Control_GetLocationX(sh)
			y 		= Control_GetLocationY(sh)
			g_3dmodel_width = Control_GetWidth(sh)
			g_3dmodel_height= Control_GetHeight(sh)
		end

		g_cur_rotation_Y = 18

		GetSet_SetVector(g_menu_game_obj, MenuGameIds.VP_POSITION, dlg_x + x - 300, dlg_y + y, 0)
		--GetSet_SetVector(g_menu_game_obj, MenuGameIds.VP_DIMENSIONS, g_3dmodel_width, g_3dmodel_height, 0)
		GetSet_SetVector(g_menu_game_obj, MenuGameIds.VP_DIMENSIONS, 1200, 1200, 0)
		GetSet_SetVector(g_menu_game_obj, MenuGameIds.ROTATION, 0, g_cur_rotation_Y, 0)

		--GetSet_SetVector(g_menu_game_obj_face, MenuGameIds.VP_DIMENSIONS, g_3dmodel_width*6, g_3dmodel_height*6, 0)
		--GetSet_SetVector(g_menu_game_obj_face, MenuGameIds.ROTATION, -18, 0, 0)
		-- try to ensure floating point
		g_3dmodel_width  = g_3dmodel_width  * 1.0
		g_3dmodel_height = g_3dmodel_height * 1.0

		--clear the tables
		g_t_shirt = {}
		g_t_head = {}
		g_t_hair = {}
		g_t_eyes = {}
		g_t_pants = {}
		g_t_boots = {}
		g_t_facial = {}
		g_t_eyes = {}
		
		-- SetupConfig(ShirtConfigID[g_gender]  , g_t_shirt,  "btnShirtBack", "btnShirtNext")
		SetupConfig(HeadConfigID[g_gender]   , g_t_head,   "btnHeadBack", "btnHeadNext")
		SetupConfig(HairConfigID[g_gender]   , g_t_hair,   "btnHairBack", "btnHairNext")
		-- SetupConfig(PantsConfigID[g_gender]  , g_t_pants,  "btnPantsBack", "btnPantsNext")
		-- SetupConfig(BootsConfigID[g_gender]  , g_t_boots,  "btnBootsBack", "btnBootsNext")
		SetupConfig(FacialConfigID[g_gender] , g_t_facial, "btnFacialBack", "btnFacialNext")
		-- SetupConfig(EyesConfigID[g_gender] , g_t_eyes, "btnEyesBack", "btnEyesNext")
		
		putOnWizardsRobeAndHat()
	end
end

function putOnWizardsRobeAndHat()
	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, HeadConfigID[g_gender], g_t_head[g_cur_head_index], g_cur_skin_color_index)
	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, TorsoConfigID[g_gender], 1, g_cur_skin_color_index)
	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, LegsConfigID[g_gender], 1, g_cur_skin_color_index)
	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, CrotchConfigID[g_gender], 1, g_cur_skin_color_index)
	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, NeckConfigID[g_gender], 0, g_cur_skin_color_index)

    if (g_gender == SKINNYCHICK) or (g_gender == MEDCHICK) or (g_gender == HUGECHICK) then
        ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, ArmsConfigID[g_gender], 0, g_cur_skin_color_index)
    elseif (g_gender == SKINNYDUDE) or (g_gender == MEDDUDE) or (g_gender == HUGEDUDE) then
	    ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, ArmsConfigID[g_gender], 1, g_cur_skin_color_index)
	end

	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, HandsConfigID[g_gender], 0, g_cur_skin_color_index)
    ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, FeetConfigID[g_gender], 1, g_cur_skin_color_index)
	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, HairConfigID[g_gender], g_t_hair[g_cur_hair_index], g_cur_hair_color_index)
	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, EyesConfigID[g_gender], 0, g_cur_eye_color_index)
	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, ShirtConfigID[g_gender], 1, 0)
	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, PantsConfigID[g_gender], 1, 0)
	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, BootsConfigID[g_gender], 1, 0)
	
	if (g_gender == SKINNYCHICK) or (g_gender == MEDCHICK) or (g_gender == HUGECHICK) then
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, FacialConfigID[g_gender], g_t_head[g_cur_head_index], g_cur_facial_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, LipsConfigID[g_gender],   g_t_head[g_cur_head_index], g_cur_lip_color_index)
	elseif (g_gender == SKINNYDUDE) or (g_gender == MEDDUDE) or (g_gender == HUGEDUDE) then
		if (g_cur_facial_index == 0)  then
			ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, FacialConfigID[g_gender], g_cur_facial_index, g_cur_facial_color_index)
		else
			local updatedIndex = (g_cur_head_index-1)*FACIALHAIR_TOTAL + g_cur_facial_index
			ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, FacialConfigID[g_gender], updatedIndex, g_cur_facial_color_index)
		end
	end
end

function disRobe()
 ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, TorsoConfigID[g_gender], 0, g_cur_skin_color_index)
	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, LegsConfigID[g_gender], 0, g_cur_skin_color_index)
	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, CrotchConfigID[g_gender], 0, g_cur_skin_color_index)
	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, ShirtConfigID[g_gender], 0, 0)
	ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, PantsConfigID[g_gender], 0, 0)  
	
    ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, ArmsConfigID[g_gender], 0, g_cur_skin_color_index)
    ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, BootsConfigID[g_gender], 0, 0)
    ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, FeetConfigID[g_gender], 0, g_cur_skin_color_index)

end

---------------------------------------------------------------------
--
-- setup an alt config
--
---------------------------------------------------------------------
function SetupConfig(configID, configTableIndices, buttonIncrementName, buttonDecrementName)
	if g_game ~= nil and g_menu_game_obj ~= nil then
		local t = { ClientEngine_GetCharacterAltConfigsList(g_game, g_menu_game_obj, configID) }
		if t ~= 0  then
			local num_args = #t
			local num_indices = num_args - 1 	-- subtract off the name
			local name = t[1]
			if num_indices > 0 then
				for i = 0, num_indices-1 do
					table.insert(configTableIndices, t[i+2])
				end
				-- enable the up/down buttons since there are configs
				local ControlHandle ch_i = Dialog_GetControl(gDialogHandle, buttonIncrementName)
				local ControlHandle ch_d = Dialog_GetControl(gDialogHandle, buttonDecrementName)
				if ch_i ~= 0 and ch_d ~= 0 then
					Control_SetEnabled(ch_i, true)
					Control_SetEnabled(ch_d, true)
				end
			end
		end
	end
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)

	if g_game ~= nil then
		if g_menu_game_obj ~= nil then
			ClientEngine_RemoveMenuGameObject(g_game, g_menu_game_obj)
		end
	end
	
	Helper_Dialog_OnDestroy( dialogHandle )
	
	if g_backButton then
		ClientEngine_Quit( g_game )
	end

end


---------------------------------------------------------------------
--
-- start rotation of game object (3D model)
--
---------------------------------------------------------------------
function lblModel_OnLButtonDown(controlHandle, x, y)
	
	--g_bRotate = true
	
	--g_prevMousePointX = x
	--g_prevMousePointY = y

end

---------------------------------------------------------------------
--
-- rotating game object (3D model)
--
---------------------------------------------------------------------
function Dialog_OnMouseMove(dialogHandle, x, y)

	-- g_3dmodel_width pixels equals one full rotation

	if g_bRotate == true then

		local dx =  x - g_prevMousePointX
		local dy =  y - g_prevMousePointY

		g_prevMousePointX = x
		g_prevMousePointY = y

		-- positive dx should rotate counter-clockwise around y-axis
		local ry = math.fmod(dx / g_3dmodel_width * -360.0 + g_cur_rotation_Y, 360)
		-- positive dy should rotate counter-clockwise around x-axis
		local rx = math.fmod(dy / g_3dmodel_height * -360.0 + g_cur_rotation_X, 360)

		g_cur_rotation_Y = ry
		g_cur_rotation_X = rx

		-- for now, do not rotate around x-axis

		if g_menu_game_obj ~= nil then
			GetSet_SetVector(g_menu_game_obj, MenuGameIds.ROTATION, g_cur_rotation_X, g_cur_rotation_Y, 0)
		end
	
	end
end

---------------------------------------------------------------------
--
-- end rotation of game object (3D model)
--
---------------------------------------------------------------------
function EndRotation(controlHandle, x, y)	
	g_bRotate = false
	g_prevMousePointX = 0
	g_prevMousePointY = 0
	g_arrow = CLEAR
end

---------------------------------------------------------------------
--
-- one of three methods needed to check for lbuttonup
--
---------------------------------------------------------------------
function lblModel_OnLButtonUp(controlHandle, x, y)	

	EndRotation()
end

---------------------------------------------------------------------
--
-- one of three methods needed to check for lbuttonup
--
---------------------------------------------------------------------
function imgBackGround_OnLButtonUp(controlHandle, x, y)	

	EndRotation()
end

---------------------------------------------------------------------
--
-- one of three methods needed to check for lbuttonup
--
---------------------------------------------------------------------
function Dialog_OnLButtonUp(dialogHandle, x, y)	

	EndRotation()
end

---------------------------------------------------------------------
--
-- Start Rotate 3D Model around the y-axis (left)
--
---------------------------------------------------------------------
function btnRotateLeft_OnLButtonDown(controlHandle, x, y)

	Rotate3DModel(3)
	g_arrow = CLICKED_LEFT
	g_dArrowTS = Dispatcher_CurrentTime( KEP.dispatcher )/1000
end

---------------------------------------------------------------------
--
-- Stop Rotate 3D Model around the y-axis (left)
--
---------------------------------------------------------------------
function btnRotateLeft_OnLButtonUp(controlHandle, x, y)

	g_arrow = CLEAR
end

---------------------------------------------------------------------
--
-- Start Rotate 3D Model around the y-axis (right)
--
---------------------------------------------------------------------
function btnRotateRight_OnLButtonDown(controlHandle, x, y)

	Rotate3DModel(-3)
	g_arrow = CLICKED_RIGHT
	g_dArrowTS = Dispatcher_CurrentTime( KEP.dispatcher )/1000
end

---------------------------------------------------------------------
--
-- Stop Rotate 3D Model around the y-axis (left)
--
---------------------------------------------------------------------
function btnRotateRight_OnLButtonUp(controlHandle, x, y)

	g_arrow = CLEAR
end

---------------------------------------------------------------------
--
-- Rotate 3D Model around the y-axis
--
---------------------------------------------------------------------
function Dialog_OnRender(dialogHandle, fElapsedTime)
	if g_arrow ~= CLEAR then
		local cur_time = Dispatcher_CurrentTime( KEP.dispatcher )/1000
		if g_arrow == CLICKED_LEFT then
			if ARROWCLICK_DELAY < cur_time - g_dArrowTS then
				Rotate3DModel(3)
				g_arrow = HELD_LEFT
				g_dArrowTS = cur_time
			end
		elseif g_arrow == HELD_LEFT then
			if ARROWCLICK_REPEAT < cur_time - g_dArrowTS then
				Rotate3DModel(3)
				g_dArrowTS = cur_time
			end
		elseif g_arrow == CLICKED_RIGHT then
			if ARROWCLICK_DELAY < cur_time - g_dArrowTS then

				Rotate3DModel(-3)
				g_arrow = HELD_RIGHT
				g_dArrowTS = cur_time
			end
		elseif g_arrow == HELD_RIGHT then
			if ARROWCLICK_REPEAT < cur_time - g_dArrowTS then
				Rotate3DModel(-3)
				g_dArrowTS = cur_time
			end
		end
	end
	
	--apply scaling
	local SliderHandle handlesliderY = Dialog_GetSlider(gDialogHandle, "sliderY")
	local scaleY = Slider_GetValue( handlesliderY )
	local SliderHandle handlesliderXZ = Dialog_GetSlider(gDialogHandle, "sliderXZ")
	local scaleXZ = Slider_GetValue( handlesliderXZ )	
	if g_menu_game_obj ~= nil then	  
		GetSet_SetVector(g_menu_game_obj, MenuGameIds.SCALING, scaleXZ * 1.0 / 100, scaleY * 1.0 / 100, scaleXZ * 1.0 / 100)
	end
end

tolerance = 3

function sliderY_OnSliderValueChanged( sh )
	local scaleY = Slider_GetValue( sh )
	local SliderHandle handlesliderXZ = Dialog_GetSlider(gDialogHandle, "sliderXZ")
	local scaleXZ = Slider_GetValue( handlesliderXZ )	
	
	if (scaleY ~= currentScaleY) then
		
		updateHeightLabel( scaleY )
		
		currentScaleY = scaleY
	end
	
	diff = math.abs( scaleY - scaleXZ)
	if diff > tolerance then
		if scaleY > scaleXZ then
			scaleXZ = scaleY - tolerance
		else
			scaleXZ = scaleY + tolerance
		end
		Slider_SetValue( handlesliderXZ , scaleXZ)
	end		
end

function sliderXZ_OnSliderValueChanged( sh )
	local SliderHandle handlesliderY = Dialog_GetSlider(gDialogHandle, "sliderY")
	local scaleY = Slider_GetValue( handlesliderY )
	local scaleXZ = Slider_GetValue( sh )	
	
	diff = math.abs( scaleY - scaleXZ)
	if diff > tolerance then
		if scaleY < scaleXZ then
			scaleY = scaleXZ - tolerance
		else
			scaleY = scaleXZ + tolerance
		end
		Slider_SetValue( handlesliderY , scaleY)
		
		if (scaleY ~= currentScaleY) then
		    
			updateHeightLabel( scaleY )
			
			currentScaleY = scaleY
		end
	
	end		

end

function updateHeightLabel( scaleY )

	local startHeight = 0
	if (g_gender == SKINNYCHICK) or (g_gender == MEDCHICK) or (g_gender == HUGECHICK) then
        startHeight = femaleStartHeight
	else
     	startHeight = maleStartHeight
	end
	
	local height = ((scaleY - 100) / 100) * startHeight + startHeight
	local feet = math.floor( height / 12 )
	local inches = math.floor(height - (feet * 12) + 0.5)
	
	local sh = Dialog_GetStatic(gDialogHandle, "lblHeightCurrent")
	if sh ~= nil then
    	Static_SetText(sh, feet .. "\' " .. inches .. "\"")
	end

end

---------------------------------------------------------------------
--
-- Rotate 3D Model by degrees
--
---------------------------------------------------------------------
function Rotate3DModel(degrees)
	g_cur_rotation_Y = g_cur_rotation_Y + degrees
	-- wrapping
	if g_cur_rotation_Y < 0 then
		g_cur_rotation_Y = g_cur_rotation_Y + 360
	elseif g_cur_rotation_Y > 360 then
		g_cur_rotation_Y = g_cur_rotation_Y - 360
	end
	-- for now, do not rotate around x-axis
	if g_menu_game_obj ~= nil then
		GetSet_SetVector(g_menu_game_obj, MenuGameIds.ROTATION, g_cur_rotation_X, g_cur_rotation_Y, 0)
	end
end




---------------------------------------------------------------------
--
-- Create Button
--
---------------------------------------------------------------------
function btnNext_OnButtonClicked( buttonHandle )
	local EditBoxHandle ebh = Dialog_GetEditBox(gDialogHandle, "editName")
	local name = EditBox_GetText(ebh);

--	EditBox_SetText( ebh, tostring( g_teamID + 1) )

	local lTeam = g_teamID + 1	
	local SliderHandle handlesliderY = Dialog_GetSlider(gDialogHandle, "sliderY")
	local scaleY = Slider_GetValue( handlesliderY )
	local SliderHandle handlesliderXZ = Dialog_GetSlider(gDialogHandle, "sliderXZ")
	local scaleXZ = Slider_GetValue( handlesliderXZ )	

	if g_game ~= nil then
		Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgLoading")), true)
		Control_SetVisible(Static_GetControl(Dialog_GetStatic(gDialogHandle, "lblLoading")), true)
		-- if the character is created successfully
		disRobe() -- Take off wizard's robe and hat
		
		if ClientEngine_CreateCharacter(g_game, g_menu_game_obj, STARTER_APT_SPAWN, name, lTeam, scaleXZ, scaleY, scaleXZ ) ~= 0 then
			GetCharacters() -- if gets 1+ back will play it
		end
	end
end

---------------------------------------------------------------------
--
-- Cancel Button
--
---------------------------------------------------------------------
function btnBack_OnButtonClicked( buttonHandle )

    g_backButton = true
    DestroyMenu(gDialogHandle)

end

---------------------------------------------------------------------
--
-- Go To Character Selection menu
--
---------------------------------------------------------------------
function gotoCharacterSelection()
	gotoMenu( "CharacterSelection.xml" )
end

---------------------------------------------------------------------
--
-- Go To  menu
--
---------------------------------------------------------------------
function gotoMenu(menu_path)
	-- go to character selection
	Dialog_Create( menu_path );
			
	-- close this dialog
	DestroyMenu(gDialogHandle)

end
---------------------------------------------------------------------
--
-- txtName has changed
--
---------------------------------------------------------------------
function editName_OnEditBoxChange(editBoxHandle)
	local name = EditBox_GetText(editBoxHandle);
	local ButtonHandle bh = Dialog_GetButton(gDialogHandle, "btnNext")
	local ControlHandle ch = Button_GetControl(bh)
	local ok_to_login = (trim(name) ~= "") and g_teamID ~= -1
	Control_SetEnabled(ch, ok_to_login)
end

---------------------------------------------------------------------
-- Increment Alt Character Config
---------------------------------------------------------------------
function IncrementCharacterConfig(configID, configTableIndices)

	local currentIndex = -1
	local useMat = 0

	if configID == ShirtConfigID[g_gender] then
		currentIndex = g_cur_shirt_index
	elseif configID == HeadConfigID[g_gender] then
		currentIndex = g_cur_head_index
	elseif configID == HairConfigID[g_gender] then
		currentIndex = g_cur_hair_index
		useMat = g_cur_hair_color_index
	elseif configID == PantsConfigID[g_gender] then
		currentIndex = g_cur_pants_index
	elseif configID == BootsConfigID[g_gender] then
		currentIndex = g_cur_boots_index
	elseif configID == FacialConfigID[g_gender] then
		currentIndex = g_cur_facial_index
		useMat = g_cur_facial_color_index
	elseif configID == EyesConfigID[g_gender] then
		currentIndex = g_cur_eyes_index
	end

	if currentIndex ~= -1 then

		currentIndex = currentIndex  + 1

   		if configID == FacialConfigID[g_gender] then
		    if (g_gender == SKINNYDUDE) or (g_gender == MEDDUDE) or (g_gender == HUGEDUDE) then
	 			if currentIndex > FACIALHAIR_TOTAL then
		        	currentIndex = 0
				end
			end
  		elseif currentIndex > #configTableIndices then
			currentIndex = 1
		end


		if configID == ShirtConfigID[g_gender] then
			g_cur_shirt_index = currentIndex
		elseif configID == HeadConfigID[g_gender] then
			g_cur_head_index = currentIndex
		elseif configID == HairConfigID[g_gender] then
			g_cur_hair_index = currentIndex
		elseif configID == PantsConfigID[g_gender] then
			g_cur_pants_index = currentIndex
		elseif configID == BootsConfigID[g_gender] then
			g_cur_boots_index = currentIndex
		elseif configID == FacialConfigID[g_gender] then
			g_cur_facial_index = currentIndex
		elseif configID == EyesConfigID[g_gender] then
			g_cur_eyes_index = currentIndex
		end

		-- set new config
		if g_game ~= nil and g_menu_game_obj ~= nil then
			ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, configID, configTableIndices[currentIndex], useMat)
		end
	end
end

---------------------------------------------------------------------
-- Decrement Alt Character Config
---------------------------------------------------------------------
function DecrementCharacterConfig(configID, configTableIndices)

	local currentIndex = -1
	local useMat = 0

	if configID == ShirtConfigID[g_gender] then
		currentIndex = g_cur_shirt_index
	elseif configID == HeadConfigID[g_gender] then
		currentIndex = g_cur_head_index
	elseif configID == HairConfigID[g_gender] then
		currentIndex = g_cur_hair_index
		useMat = g_cur_hair_color_index
	elseif configID == PantsConfigID[g_gender] then
		currentIndex = g_cur_pants_index
	elseif configID == BootsConfigID[g_gender] then
		currentIndex = g_cur_boots_index
	elseif configID == FacialConfigID[g_gender] then
		currentIndex = g_cur_facial_index
		useMat = g_cur_facial_color_index
	elseif configID == EyesConfigID[g_gender] then
		currentIndex = g_cur_eyes_index
	end

	if currentIndex ~= -1 then
		currentIndex = currentIndex  - 1

        if configID == FacialConfigID[g_gender] then
		    if (g_gender == SKINNYDUDE) or (g_gender == MEDDUDE) or (g_gender == HUGEDUDE) then
	 			if currentIndex < 0 then
		        	currentIndex = FACIALHAIR_TOTAL
				end
			end
		elseif currentIndex <  1 then
			currentIndex = #configTableIndices
		end

		if configID == ShirtConfigID[g_gender] then
			g_cur_shirt_index = currentIndex
		elseif configID == HeadConfigID[g_gender] then
			g_cur_head_index = currentIndex
		elseif configID == HairConfigID[g_gender] then
			g_cur_hair_index = currentIndex
		elseif configID == PantsConfigID[g_gender] then
			g_cur_pants_index = currentIndex
		elseif configID == BootsConfigID[g_gender] then
			g_cur_boots_index = currentIndex
		elseif configID == FacialConfigID[g_gender] then
			g_cur_facial_index = currentIndex
		elseif configID == EyesConfigID[g_gender] then
			g_cur_eyes_index = currentIndex
		end

		-- set new config
		if g_game ~= nil and g_menu_game_obj ~= nil then
			ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, configID, configTableIndices[currentIndex], useMat)
		end
	end
end

---------------------------------------------------------------------
-- Increment Shirt - Alt Character Config
---------------------------------------------------------------------
--function btnShirtNext_OnButtonClicked(buttonHandle)
--	IncrementCharacterConfig(ShirtConfigID[g_gender], g_t_shirt)
--end


---------------------------------------------------------------------
-- Decrement Shirt - Alt Character Config
---------------------------------------------------------------------
--function btnShirtBack_OnButtonClicked(buttonHandle)
--	DecrementCharacterConfig(ShirtConfigID[g_gender], g_t_shirt)
--end

---------------------------------------------------------------------
-- Increment Head - Alt Character Config
---------------------------------------------------------------------
function btnHeadNext_OnButtonClicked(buttonHandle)
	g_cur_skin_color_index = g_cur_skin_color_index + 1
	if g_cur_skin_color_index >= SKIN_MAX then
		g_cur_skin_color_index = 0 
	end

	if g_game ~= nil and g_menu_game_obj ~= nil then
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, HeadConfigID[g_gender], g_t_head[g_cur_head_index], g_cur_skin_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, TorsoConfigID[g_gender], 1, g_cur_skin_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, LegsConfigID[g_gender], 1, g_cur_skin_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, FeetConfigID[g_gender], 1, g_cur_skin_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, CrotchConfigID[g_gender], 1, g_cur_skin_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, NeckConfigID[g_gender], 0, g_cur_skin_color_index)

	    if (g_gender == SKINNYCHICK) or (g_gender == MEDCHICK) or (g_gender == HUGECHICK) then
	        ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, ArmsConfigID[g_gender], 0, g_cur_skin_color_index)
	    elseif (g_gender == SKINNYDUDE) or (g_gender == MEDDUDE) or (g_gender == HUGEDUDE) then
		    ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, ArmsConfigID[g_gender], 1, g_cur_skin_color_index)
		end

		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, HandsConfigID[g_gender], 0, g_cur_skin_color_index)
		
	    local sh = Dialog_GetStatic(gDialogHandle, "lblSkinCurrent")
		if sh ~= nil then
	    	Static_SetText(sh, g_cur_skin_color_index + 1 )
		end
	end
end


---------------------------------------------------------------------
-- Decrement Head - Alt Character Config
---------------------------------------------------------------------
function btnHeadBack_OnButtonClicked(buttonHandle)
	g_cur_skin_color_index = g_cur_skin_color_index - 1
	if g_cur_skin_color_index < 0 then
		g_cur_skin_color_index = SKIN_MAX - 1 
	end

	if g_game ~= nil and g_menu_game_obj ~= nil then
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, HeadConfigID[g_gender], g_t_head[g_cur_head_index], g_cur_skin_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, TorsoConfigID[g_gender], 1, g_cur_skin_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, LegsConfigID[g_gender], 1, g_cur_skin_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, FeetConfigID[g_gender], 1, g_cur_skin_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, CrotchConfigID[g_gender], 1, g_cur_skin_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, NeckConfigID[g_gender], 0, g_cur_skin_color_index)

		if (g_gender == SKINNYCHICK) or (g_gender == MEDCHICK) or (g_gender == HUGECHICK) then
	        ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, ArmsConfigID[g_gender], 0, g_cur_skin_color_index)
	    elseif (g_gender == SKINNYDUDE) or (g_gender == MEDDUDE) or (g_gender == HUGEDUDE) then
		    ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, ArmsConfigID[g_gender], 1, g_cur_skin_color_index)
		end
		
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, HandsConfigID[g_gender], 0, g_cur_skin_color_index)

	    local sh = Dialog_GetStatic(gDialogHandle, "lblSkinCurrent")
		if sh ~= nil then
	    	Static_SetText(sh, g_cur_skin_color_index + 1 )
		end
		
	end
end

---------------------------------------------------------------------
-- Increment Hair - Alt Character Config
---------------------------------------------------------------------
function btnHairNext_OnButtonClicked(buttonHandle)
	IncrementCharacterConfig(HairConfigID[g_gender], g_t_hair)
	
    local sh = Dialog_GetStatic(gDialogHandle, "lblHairCurrent")
	if sh ~= nil then
	    Static_SetText(sh, g_cur_hair_index )
	end
end

---------------------------------------------------------------------
-- Decrement Head - Alt Character Config
---------------------------------------------------------------------
function btnHairBack_OnButtonClicked(buttonHandle)
	DecrementCharacterConfig(HairConfigID[g_gender], g_t_hair)

	local sh = Dialog_GetStatic(gDialogHandle, "lblHairCurrent")
	if sh ~= nil then
	    Static_SetText(sh, g_cur_hair_index )
	end
end

function btnHairMatNext_OnButtonClicked(buttonHandle)
	g_cur_hair_color_index = g_cur_hair_color_index + 1
	if g_cur_hair_color_index >= HAIR_MAX then
		g_cur_hair_color_index = 0 
	end

	if g_game ~= nil and g_menu_game_obj ~= nil then
		--Dialog_MsgBox("slot: " .. HairConfigID[g_gender] .. " index: " .. g_cur_hair_index .. " color: " .. g_cur_hair_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, HairConfigID[g_gender], g_t_hair[g_cur_hair_index], g_cur_hair_color_index)

        local sh = Dialog_GetStatic(gDialogHandle, "lblHairColorCurrent")
		if sh ~= nil then
	        Static_SetText(sh, g_cur_hair_color_index + 1)
		end
	end
end

function btnHairMatPrev_OnButtonClicked(buttonHandle)
	g_cur_hair_color_index = g_cur_hair_color_index - 1
	if g_cur_hair_color_index < 0 then
		g_cur_hair_color_index = HAIR_MAX - 1
	end

	if g_game ~= nil and g_menu_game_obj ~= nil then
		--Dialog_MsgBox("slot: " .. HairConfigID[g_gender] .. " index: " .. g_cur_hair_index .. " color: " .. g_cur_hair_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, HairConfigID[g_gender], g_t_hair[g_cur_hair_index], g_cur_hair_color_index)
		
        local sh = Dialog_GetStatic(gDialogHandle, "lblHairColorCurrent")
		if sh ~= nil then
	        Static_SetText(sh, g_cur_hair_color_index + 1)
		end
	end
end

---------------------------------------------------------------------
--
-- Increment Pants - Alt Character Config
--
---------------------------------------------------------------------
--function btnPantsNext_OnButtonClicked(buttonHandle)
--	IncrementCharacterConfig(PantsConfigID[g_gender], g_t_pants)
--end


---------------------------------------------------------------------
-- Decrement Pants - Alt Character Config
---------------------------------------------------------------------
--function btnPantsBack_OnButtonClicked(buttonHandle)
--	DecrementCharacterConfig(PantsConfigID[g_gender], g_t_pants)
--end

---------------------------------------------------------------------
--
-- Increment Boots - Alt Character Config
--
---------------------------------------------------------------------
--function btnBootsNext_OnButtonClicked(buttonHandle)
--	IncrementCharacterConfig(BootsConfigID[g_gender], g_t_boots)
--end


---------------------------------------------------------------------
-- Decrement Boots - Alt Character Config
---------------------------------------------------------------------
--function btnBootsBack_OnButtonClicked(buttonHandle)
--	DecrementCharacterConfig(BootsConfigID[g_gender], g_t_boots)
--end

---------------------------------------------------------------------
-- Increment Facial Hair - Alt Character Config
---------------------------------------------------------------------
function btnFacialHairNext_OnButtonClicked(buttonHandle)
    if (g_gender == SKINNYCHICK) or (g_gender == MEDCHICK) or (g_gender == HUGECHICK) then
        g_cur_lip_color_index = g_cur_lip_color_index + 1

		if g_cur_lip_color_index >= FACIAL_MAX then
			g_cur_lip_color_index = 0
		end

		if g_game ~= nil and g_menu_game_obj ~= nil then
            ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, LipsConfigID[g_gender],   g_t_head[g_cur_head_index], g_cur_lip_color_index)
            
	        local sh = Dialog_GetStatic(gDialogHandle, "lblFacialHairCurrent")
			if sh ~= nil then
		        Static_SetText(sh, g_cur_lip_color_index + 1)
			end
		end
	elseif (g_gender == SKINNYDUDE) or (g_gender == MEDDUDE) or (g_gender == HUGEDUDE) then
        IncrementCharacterConfig(FacialConfigID[g_gender], g_t_facial)
        
        local sh = Dialog_GetStatic(gDialogHandle, "lblFacialHairCurrent")
		if sh ~= nil then
	        Static_SetText(sh, g_cur_facial_index + 1)
		end
	end

	putOnWizardsRobeAndHat()
end


---------------------------------------------------------------------
-- Decrement Facial Hair - Alt Character Config
---------------------------------------------------------------------
function btnFacialHairBack_OnButtonClicked(buttonHandle)
    if (g_gender == SKINNYCHICK) or (g_gender == MEDCHICK) or (g_gender == HUGECHICK) then
        g_cur_lip_color_index = g_cur_lip_color_index - 1
        
		if g_cur_lip_color_index < 0 then
			g_cur_lip_color_index = FACIAL_MAX - 1
		end

		if g_game ~= nil and g_menu_game_obj ~= nil then
			ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, LipsConfigID[g_gender],   g_t_head[g_cur_head_index], g_cur_lip_color_index)
			
	        local sh = Dialog_GetStatic(gDialogHandle, "lblFacialHairCurrent")
			if sh ~= nil then
		        Static_SetText(sh, g_cur_lip_color_index + 1)
			end

		end
	elseif (g_gender == SKINNYDUDE) or (g_gender == MEDDUDE) or (g_gender == HUGEDUDE) then
    	DecrementCharacterConfig(FacialConfigID[g_gender], g_t_facial)
    	
    	local sh = Dialog_GetStatic(gDialogHandle, "lblFacialHairCurrent")
		if sh ~= nil then
	        Static_SetText(sh, g_cur_facial_index + 1)
		end
	end
	
	putOnWizardsRobeAndHat()
end

---------------------------------------------------------------------
-- Increment Face- Alt Character Config
---------------------------------------------------------------------
function btnFaceNext_OnButtonClicked(buttonHandle)
	IncrementCharacterConfig(HeadConfigID[g_gender], g_t_head)
	
	local sh = Dialog_GetStatic(gDialogHandle, "lblFaceCurrent")
	if sh ~= nil then
        Static_SetText(sh, g_cur_head_index)
	end
	
	putOnWizardsRobeAndHat()
end


---------------------------------------------------------------------
-- Decrement Face - Alt Character Config
---------------------------------------------------------------------
function btnFaceBack_OnButtonClicked(buttonHandle)
	DecrementCharacterConfig(HeadConfigID[g_gender], g_t_head)

	local sh = Dialog_GetStatic(gDialogHandle, "lblFaceCurrent")
	if sh ~= nil then
        Static_SetText(sh, g_cur_head_index)
	end

	putOnWizardsRobeAndHat()
end

---------------------------------------------------------------------
-- Increment Facial Hair Color
---------------------------------------------------------------------
function btnFacialHairColorNext_OnButtonClicked(buttonHandle)
	
	g_cur_facial_color_index = g_cur_facial_color_index + 1
	if g_cur_facial_color_index >= FACIAL_MAX then
		g_cur_facial_color_index = 0 
	end

	if g_game ~= nil and g_menu_game_obj ~= nil then
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, FacialConfigID[g_gender], g_t_facial[g_cur_facial_index], g_cur_facial_color_index)

        local sh = Dialog_GetStatic(gDialogHandle, "lblFacialHairColorCurrent")
		if sh ~= nil then
	        Static_SetText(sh, g_cur_facial_color_index + 1)
		end
	end
	
	putOnWizardsRobeAndHat()
end


---------------------------------------------------------------------
-- Decrement Facial Hair Color
---------------------------------------------------------------------
function btnFacialHairColorBack_OnButtonClicked(buttonHandle)
	
	g_cur_facial_color_index = g_cur_facial_color_index - 1
	if g_cur_facial_color_index < 0 then
		g_cur_facial_color_index = FACIAL_MAX - 1 
	end

	if g_game ~= nil and g_menu_game_obj ~= nil then
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, FacialConfigID[g_gender], g_t_facial[g_cur_facial_index], g_cur_facial_color_index)

        local sh = Dialog_GetStatic(gDialogHandle, "lblFacialHairColorCurrent")
		if sh ~= nil then
	        Static_SetText(sh, g_cur_facial_color_index + 1)
		end
	end
	
	putOnWizardsRobeAndHat()
end

---------------------------------------------------------------------
--
-- Increment Eyes - Alt Character Config
--
---------------------------------------------------------------------
function btnEyesNext_OnButtonClicked(buttonHandle)
	g_cur_eye_color_index = g_cur_eye_color_index + 1
	if g_cur_eye_color_index >= EYE_MAX then
		g_cur_eye_color_index = 0 
	end
	if g_game ~= nil and g_menu_game_obj ~= nil then
		-- Dialog_MsgBox("slot: " .. HeadConfigID[g_gender] .. " index: " .. 0 .. " color: " .. g_cur_eye_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, EyesConfigID[g_gender], 0, g_cur_eye_color_index)
		
		local sh = Dialog_GetStatic(gDialogHandle, "lblEyesCurrent")
		if sh ~= nil then
	        Static_SetText(sh, g_cur_eye_color_index + 1)
		end
	end
	putOnWizardsRobeAndHat()
end


---------------------------------------------------------------------
--
-- Decrement Eyes - Alt Character Config
--
---------------------------------------------------------------------
function btnEyesBack_OnButtonClicked(buttonHandle)
	g_cur_eye_color_index = g_cur_eye_color_index - 1
	if g_cur_eye_color_index < 0 then
		g_cur_eye_color_index = EYE_MAX - 1 
	end
	if g_game ~= nil and g_menu_game_obj ~= nil then
		-- Dialog_MsgBox("slot: " .. HeadConfigID[g_gender] .. " index: " .. 0 .. " color: " .. g_cur_eye_color_index)
		ClientEngine_SetCharacterAltConfig(g_game, g_menu_game_obj, EyesConfigID[g_gender], 0, g_cur_eye_color_index)
		
		local sh = Dialog_GetStatic(gDialogHandle, "lblEyesCurrent")
		if sh ~= nil then
	        Static_SetText(sh, g_cur_eye_color_index + 1)
		end
	end
	putOnWizardsRobeAndHat()
end

---------------------------------------------------------------------
--
-- Increment Body - Alt Character Config
--
---------------------------------------------------------------------
function btnBodyNext_OnButtonClicked(buttonHandle)
	g_cur_body_index = g_cur_body_index + 1
	if g_cur_body_index >= BODY_MAX then
		g_cur_body_index = 0
	end
	if g_game ~= nil and g_menu_game_obj ~= nil then

		if g_cur_body_index == 0 then
		    if g_gender == FEMALE or g_gender == SKINNYCHICK or g_gender == MEDCHICK or g_gender == HUGECHICK then
				g_gender = SKINNYCHICK
			else
				g_gender = SKINNYDUDE
			end
			setupActor()
		elseif g_cur_body_index == 1 then
			if g_gender == FEMALE or g_gender == SKINNYCHICK or g_gender == MEDCHICK or g_gender == HUGECHICK then
				g_gender = MEDCHICK
			else
				g_gender = MEDDUDE
			end
			setupActor()
		elseif g_cur_body_index == 2 then
			if g_gender == FEMALE or g_gender == SKINNYCHICK or g_gender == MEDCHICK or g_gender == HUGECHICK then
				g_gender = HUGECHICK
			else
				g_gender = HUGEDUDE
			end
			setupActor()
		end

		local sh = Dialog_GetStatic(gDialogHandle, "lblBodyCurrent")
		if sh ~= nil then
	        Static_SetText(sh, g_cur_body_index + 1)
		end
	end
end


---------------------------------------------------------------------
--
-- Decrement Body - Alt Character Config
--
---------------------------------------------------------------------
function btnBodyBack_OnButtonClicked(buttonHandle)
 	g_cur_body_index = g_cur_body_index - 1
	if g_cur_body_index < 0 then
		g_cur_body_index = BODY_MAX - 1
	end
	if g_game ~= nil and g_menu_game_obj ~= nil then

		if g_cur_body_index == 0 then
		    if g_gender == FEMALE or g_gender == SKINNYCHICK or g_gender == MEDCHICK or g_gender == HUGECHICK then
				g_gender = SKINNYCHICK
			else
				g_gender = SKINNYDUDE
			end
			setupActor()
		elseif g_cur_body_index == 1 then
			if g_gender == FEMALE or g_gender == SKINNYCHICK or g_gender == MEDCHICK or g_gender == HUGECHICK then
				g_gender = MEDCHICK
			else
				g_gender = MEDDUDE
			end
			setupActor()
		elseif g_cur_body_index == 2 then
			if g_gender == FEMALE or g_gender == SKINNYCHICK or g_gender == MEDCHICK or g_gender == HUGECHICK then
				g_gender = HUGECHICK
			else
				g_gender = HUGEDUDE
			end
			setupActor()
		end

		local sh = Dialog_GetStatic(gDialogHandle, "lblBodyCurrent")
		if sh ~= nil then
	        Static_SetText(sh, g_cur_body_index + 1)
		end
	end
end

---------------------------------------------------------------------
--
-- Helper function for strings
--
---------------------------------------------------------------------
function trim (s)
      return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end


