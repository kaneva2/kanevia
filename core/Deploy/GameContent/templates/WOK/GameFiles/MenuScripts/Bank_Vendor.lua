dofile("MenuHelper.lua")
dofile("..\\scripts\\GameGlobals.lua") 
dofile("CommonFunctions.lua")                                       

--base name of icon image to be concatenated with 1-10
GIFT_ICON_INV_NAME = "imgGiftIconInv"
GIFT_ICON_BANK_NAME = "imgGiftIconBank"

LIST_BOX_SIZE = 10

-- global game object
g_game = nil


-- all inventory items
g_t_items = {}

-- all bank inventory items
g_t_bank_items = {}

g_t_transaction = {}       -- type (buy, sell), index, new_quantity (0 = remove_all)

-- boolean used to keep the handler, lstInventory_OnScrollBarChanged, from being called
-- when attribEventHandler() calls ListBox_AddItem()
g_b_loading_items = false

g_credits 		= 0
g_bank_credits 	= 0

RequestForInventoryID 	= 51
BankInventoryID 		= 135
UpdateCashID			= 19
UpdateBankCashID		= 22
AEGenericTypeID 		= 142

g_last_inv_track_pos 	= 0
g_last_bank_track_pos 	= 0

g_showIcons = true

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "attribEventHandler", 1,0,0,0, "AttribEvent", KEP.MED_PRIO )
	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end
	
	if KEPConfig_GetNumber == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end	

	-- get the game
	g_game = Dispatcher_GetGame(KEP.dispatcher)

	if g_game == 0 then
		g_game = nil
	end

	-- setup look of listbox scrollbars
	SetupListBoxScrollBars("lstInventory")
	SetupListBoxScrollBars("lstBankInventory")

	GetCredits()
	UpdateCredits()
	resetBankGiftIcons()
	resetInvGiftIcons()
	
	g_showIcons = getIconPreference()
	
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

---------------------------------------------------------------------
--
-- Attrib Event Handler
--
---------------------------------------------------------------------
function attribEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

	if filter == RequestForInventoryID then

		local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
		if lbh ~= 0 then

			-- always remove all items		
			ListBox_RemoveAllItems(lbh)

			g_t_items 	= {}

			g_b_loading_items = true

			g_t_items = decodeInventoryEvent( event )
			
           	g_t_items = sortTableByField(g_t_items, "texture_name")
            
            for i=1, #g_t_items do
            
				ListBox_AddItem(lbh, g_t_items[i]["name"].."\nQty: "..tostring(g_t_items[i]["quantity"]), g_t_items[i]["GLID"])            
				
            end

			g_b_loading_items = false

		end

		
		local num_items = 0
		-- determine how many icons need to be shown
		if lbh ~= 0 then
			num_items = ListBox_GetSize(lbh)
		end

        resetInvGiftIcons()

		for i = 0, 9 do

			if i < num_items then

				local item = g_t_items[i+1]

				-- show list icons
				local icon_name = "imgIcon" .. tostring(i + 1)
				local ich = Dialog_GetImage(gDialogHandle, icon_name)

				if g_showIcons and ich ~= 0 then

					Control_SetVisible(ich, true)

					-- set the icon

					local eh = Image_GetDisplayElement(ich)
					if eh ~= 0 then
	                    
						local item = g_t_items[i + 1]
						LoadIconTexture(g_game,item,eh,gDialogHandle);
					end
					
				end
			
	     	    ih = Dialog_GetImage(gDialogHandle, GIFT_ICON_INV_NAME..i+1)
	    	    setGiftIcon(item, ih)

			end
		end
	
	elseif filter == BankInventoryID then

		local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
		if lbh ~= 0 then

			-- always remove all items		
			ListBox_RemoveAllItems(lbh)

			g_t_bank_items 	= {}

			g_b_loading_items = true

			local num_items = Event_DecodeNumber( event )
			for i = 0, num_items-1 do

				local GLID 		= Event_DecodeNumber( event )
				local quantity 		= Event_DecodeNumber( event )
				local gift = false
				local armed = false
				
				if quantity < 0 then
					quantity = -quantity -- if < 0 armed
					armed = true
				end					
				
				if GLID < 0 then
					GLID = -GLID -- if < 0 gift
					gift = true
				end 
				
				local name 		= Event_DecodeString( event )
				local use_type		= Event_DecodeNumber( event )
				local texture_name 	= Event_DecodeString( event )
				-- texture coords
				local left		= Event_DecodeNumber( event )
				local top		= Event_DecodeNumber( event )
				local right		= Event_DecodeNumber( event )
				local bottom		= Event_DecodeNumber( event )

				local t_item 			= {}
				t_item["GLID"]			= GLID
				t_item["quantity"] 		= quantity
				t_item["name"]			= name
				t_item["use_type"] 		= use_type
				t_item["texture_name"] 		= texture_name
				t_item["texture_coords"]	= {left, top, right, bottom}
				t_item["is_gift"] 		= gift
				t_item["isArmed"] 		= armed

				-- add to items table
				table.insert(g_t_bank_items, t_item)

			end

           	g_t_bank_items = sortTableByField(g_t_bank_items, "texture_name")
            
            for i=1, #g_t_bank_items do
            
				ListBox_AddItem(lbh, g_t_bank_items[i]["name"].."\nQty: "..tostring(g_t_bank_items[i]["quantity"]), g_t_bank_items[i]["GLID"])            
				
            end            

			g_b_loading_items = false

		end

		
		local num_items = 0
		-- determine how many icons need to be shown
		if lbh ~= 0 then
			num_items = ListBox_GetSize(lbh)
		end

        resetBankGiftIcons()
    
		for i = 0, 9 do

			if i < num_items then

				local item = g_t_bank_items[i+1]

				-- show list icons
				local icon_name = "imgBankIcon" .. tostring(i + 1)
				local ich = Dialog_GetImage(gDialogHandle, icon_name)

				if g_showIcons and ich ~= 0 then
				
					Control_SetVisible(ich, true)

					-- set the icon
					local eh = Image_GetDisplayElement(ich)
					if eh ~= 0 then
						
						local item = g_t_bank_items[i + 1]
						LoadIconTexture(g_game,item,eh,gDialogHandle);
						
					end
					
				end

    		    ih = Dialog_GetImage(gDialogHandle, GIFT_ICON_BANK_NAME..i+1)
			    setGiftIcon(item, ih)

			end
		end
		
	elseif filter == UpdateCashID then

		g_credits = Event_DecodeNumber( event )

		UpdateCredits()

	elseif filter == UpdateBankCashID then

		g_bank_credits = Event_DecodeNumber( event )

		UpdateCredits()

	elseif filter == AEGenericTypeID then
	
		if objectid == 92 then

			if g_t_transaction["type"] == "toInv" then

				local t_item = tcopy(g_t_bank_items[ g_t_transaction["index"] + 1])

				-- removed all of this type of item
				if g_t_transaction["new_quantity"] == t_item["quantity"] then

					table.remove(g_t_bank_items, g_t_transaction["index"] + 1)

                    local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
					ListBox_RemoveItem(lbh, g_t_transaction["index"])

					UpdateBankInventoryListBoxIcons(lbh)
					
					if ( ListBox_GetSize(lbh) < 10 ) then
						local ich = Dialog_GetImage(gDialogHandle, "imgBankIcon" .. tostring(ListBox_GetSize(lbh) + 1))
						
						if ich ~= 0 then
						   Control_SetVisible(ich, false)
		                end
					end
				else
					-- update the quantity
					local new_quantity = t_item["quantity"] - g_t_transaction["new_quantity"]

					g_t_bank_items[ g_t_transaction["index"] + 1]["quantity"] = new_quantity

                    local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
					ListBox_SetItemText(lbh, g_t_transaction["index"], g_t_bank_items[ g_t_transaction["index"] + 1]["name"] .. "\nQty: " .. tostring(new_quantity) )

				end

                -- the amount of this transaction
				t_item["quantity"] = g_t_transaction["new_quantity"]

				-- actually add the item now that the quantity has been set
				AddItemToInventory(t_item)

			elseif g_t_transaction["type"] == "toBank" then

				local t_item = tcopy(g_t_items[ g_t_transaction["index"] + 1])

				-- removed all of this type of item
				if g_t_transaction["new_quantity"] == t_item["quantity"] then

					table.remove(g_t_items, g_t_transaction["index"] + 1)

                    local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
					ListBox_RemoveItem(lbh, g_t_transaction["index"])

					UpdateInventoryListBoxIcons(lbh)
					
					if ( ListBox_GetSize(lbh) < 10 ) then
						local ich = Dialog_GetImage(gDialogHandle, "imgIcon" .. tostring(ListBox_GetSize(lbh) + 1))
						
						if ich ~= 0 then
						   Control_SetVisible(ich, false)
		                end
					end
				else
					-- update the quantity
					local new_quantity = t_item["quantity"] - g_t_transaction["new_quantity"]

					g_t_items[ g_t_transaction["index"] + 1]["quantity"] = new_quantity

                    local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
					ListBox_SetItemText(lbh, g_t_transaction["index"], g_t_items[ g_t_transaction["index"] + 1]["name"] .. "\nQty: " .. tostring(new_quantity) )

				end

                -- the amount of this transaction
				t_item["quantity"] = g_t_transaction["new_quantity"]

				-- actually add the item now that the quantity has been set
				AddItemToBank(t_item)            	

			end

			g_t_transaction = {}
			--EnableTransactionControls(true)
		end
	end


end

-- Turn off all gift icons
function resetInvGiftIcons()

	for i = 1,LIST_BOX_SIZE  do

	    ih = Dialog_GetImage(gDialogHandle, GIFT_ICON_INV_NAME..i)
		if ih ~= nil then
			Control_SetVisible(Image_GetControl(ih), false)
		end

	end

end

function resetBankGiftIcons()

	for i = 1,LIST_BOX_SIZE  do

	    ih = Dialog_GetImage(gDialogHandle, GIFT_ICON_BANK_NAME..i)
		if ih ~= nil then
			Control_SetVisible(Image_GetControl(ih), false)
		end

	end

end


function setGiftIcon(item, icon_handle)

	if icon_handle ~= nil then
	
		if item["is_gift"] then
			Control_SetVisible(Image_GetControl(icon_handle), true)
		else
			Control_SetVisible(Image_GetControl(icon_handle), false)
		end

	end

end

---------------------------------------------------------------------
--
-- Retrieve Credits
--
---------------------------------------------------------------------
function GetCredits()
	
	-- get the credits for both current inventory and bank
	if g_game ~= nil then

		local t = { ClientEngine_GetPlayerInfo(g_game) }
		if t ~= 0  then
			local num_args = #t

			-- index 8 = Cash
			g_credits = tonumber(t[8+1])

			-- index 9 = Bank Cash
			g_bank_credits = tonumber(t[9+1])
		end		
	end
end

---------------------------------------------------------------------
--
-- update Credit labels
--
---------------------------------------------------------------------
function UpdateCredits()
	
	local sh = Dialog_GetStatic(gDialogHandle, "lblCreditAmount")
	if sh ~= 0 then
		Static_SetText(sh, tostring(g_credits))
	end		

	sh = Dialog_GetStatic(gDialogHandle, "lblBankCreditAmount")
	if sh ~= 0 then
		Static_SetText(sh, tostring(g_bank_credits))
	end	
end

---------------------------------------------------------------------
--
-- Update ListBox Icons
--
---------------------------------------------------------------------
function UpdateListBoxIcons(listBoxHandle, lst_OnScrollBarChanged)

	if listBoxHandle ~= 0 then

		local sbh = ListBox_GetScrollBar(listBoxHandle)
		if sbh ~= 0 then
			local page_size = ScrollBar_GetPageSize(sbh)
			local track_pos = ScrollBar_GetTrackPos(sbh)
			lst_OnScrollBarChanged(sbh, listBoxHandle, page_size, track_pos)
		end
	end
end

---------------------------------------------------------------------
--
-- Setup look of listbox scrollbars
--
---------------------------------------------------------------------
function SetupListBoxScrollBars(listbox_name)

	local lbh = Dialog_GetListBox(gDialogHandle, listbox_name)
	if lbh ~= 0 then

		local sbh = ListBox_GetScrollBar(lbh)

		if sbh ~= 0 then

			local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then
				--Blue body
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				
				Element_SetCoords(eh, 361, 579, 390, 586)
			end

			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				--Grey lined
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 573, 409, 588)

			end

			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				--Up arrow button
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 547, 409, 561)

			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				--Down Arrow Button
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 642, 409, 656)

			end

		end

	end

end


---------------------------------------------------------------------
--
-- Keyboard Handler
--
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)

	-- ESC key
	if key == 27 then

		-- send request for inventory so other menus interested in the current inventory
		-- will be notified
		if g_game ~= nil then
			ClientEngine_RequestCharacterInventory(g_game)
		end

		-- close this menu
		DestroyMenu(gDialogHandle)

	end

end

---------------------------------------------------------------------
--
-- Inventory Scrollbar Changed Handler
--
---------------------------------------------------------------------
function lstInventory_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)

	g_last_inv_track_pos = track_pos

	if g_b_loading_items == false then

		local num_items = 0
		-- determine how many icons need to be shown
		if listBoxHandle ~= 0 then
			num_items = ListBox_GetSize(listBoxHandle)
		end

        resetInvGiftIcons()

		for i = 0, 9 do

			if i < num_items then

				local item = g_t_items[i + track_pos + 1]
	
				-- show list icons
				local icon_name = "imgIcon" .. tostring(i + 1)
				local ich = Dialog_GetImage(gDialogHandle, icon_name)
		
				if g_showIcons and ich ~= 0 then
	
					Control_SetVisible(ich, true)

					-- set the icon

					local eh = Image_GetDisplayElement(ich)
					if eh ~= 0 then
						
						local item = g_t_items[i + track_pos + 1]
						LoadIconTexture(g_game,item,eh,gDialogHandle);
					end

				else
					Control_SetVisible(ich, false)
				end

			    ih = Dialog_GetImage(gDialogHandle, GIFT_ICON_INV_NAME..i+1)
			    setGiftIcon(item, ih)
			end

		end

	end

end

---------------------------------------------------------------------
--
-- Bank Inventory Scrollbar Changed Handler
--
---------------------------------------------------------------------
function lstBankInventory_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)

	g_last_bank_track_pos = track_pos

	if g_b_loading_items == false then

		local num_items = 0
		-- determine how many icons need to be shown
		if listBoxHandle ~= 0 then
			num_items = ListBox_GetSize(listBoxHandle)
		end

		resetBankGiftIcons()

		for i = 0, 9 do

			if i < num_items then

				local item = g_t_bank_items[i + track_pos + 1]
						
				-- show list icons
				local icon_name = "imgBankIcon" .. tostring(i + 1)
				local ich = Dialog_GetImage(gDialogHandle, icon_name)

				if g_showIcons and ich ~= 0 then
		
					Control_SetVisible(ich, true)

					-- set the icon

					local eh = Image_GetDisplayElement(ich)
					if eh ~= 0 then
						
						local item = g_t_bank_items[i + track_pos + 1]
						LoadIconTexture(g_game,item,eh,gDialogHandle);
					end

				else
					Control_SetVisible(ich, false)
				end
				
    		    ih = Dialog_GetImage(gDialogHandle, GIFT_ICON_BANK_NAME..i+1)
			    setGiftIcon(item, ih)
			end
		end
	end
end

---------------------------------------------------------------------
--
-- Left Arrow clicked
--
---------------------------------------------------------------------
function btnLeftArrow_OnButtonClicked(buttonHandle)

	-- try to withdraw credits

	local ebh = Dialog_GetEditBox(gDialogHandle, "txtCredits")
	if ebh ~= 0 then
	
		local credits = EditBox_GetText(ebh)
		if credits ~= "" then

			credits  = tonumber(credits)
			if credits then

				-- if there are enough credits in bank, then withdraw them
				if credits <= g_bank_credits and credits > 0 then

					if g_game ~= nil then

						ClientEngine_WithdrawCashFromBank(g_game, credits)

					end

				end

			end
		end

	end

	-- try to withdraw item
	local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
	if lbh ~= 0 then

		local index = ListBox_GetSelectedItemIndex(lbh)
		if index ~= -1 then
			local GLID 		= g_t_bank_items[index+1]["GLID"]
			local tot_quantity 	= g_t_bank_items[index+1]["quantity"]
			
			local invType = GameGlobals.IT_NORMAL
			if g_t_bank_items[index+1]["is_gift"] then
				invType = GameGlobals.IT_GIFT
			end

			-- get the quantity to be withdrawn
			local quantity		= GetQuantityBank()

			-- if there is more than 0
			if tot_quantity >= 1 then

				-- do not attempt to move more than the total
				if quantity > tot_quantity then
					quantity = tot_quantity
				end				

				-- if there are enough units, then withdraw them
				if quantity <= tot_quantity and quantity > 0 then
					if g_game ~= nil then
                        --EnableTransactionControls(false)

						ClientEngine_WithdrawItemFromBank(g_game, GLID, quantity, invType )

                        g_t_transaction["type"]  	= "toInv"
						g_t_transaction["index"] 	= index
						g_t_transaction["new_quantity"]	= quantity
					end
				end
			end
		end
	end
	

end

---------------------------------------------------------------------
--
-- Right Arrow clicked
--
---------------------------------------------------------------------
function btnRightArrow_OnButtonClicked(buttonHandle)

	-- try to deposit credits

	local ebh = Dialog_GetEditBox(gDialogHandle, "txtCredits")
	if ebh ~= 0 then
	
		local credits = EditBox_GetText(ebh)
		if credits ~= "" then

			credits  = tonumber(credits)
			if credits then

				-- if there are enough credits, then deposit them
				if credits <= g_credits and credits > 0 then

					if g_game ~= nil then

						ClientEngine_DepositCashToBank(g_game, credits)
					end

				end

			end
		end

	end

	-- try to deposit item
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	if lbh ~= 0 then

		local index = ListBox_GetSelectedItemIndex(lbh)
		if index ~= -1 then
			local GLID 			= g_t_items[index+1]["GLID"]
			local tot_quantity 	= g_t_items[index+1]["quantity"]

			local invType = GameGlobals.IT_NORMAL
			if g_t_items[index+1]["is_gift"] then
				invType = GameGlobals.IT_GIFT
			end

			-- get the quantity to be deposited
			local quantity		= GetQuantity()

			-- if there is more than 1
			if tot_quantity >= 1 then

				if quantity > tot_quantity then
					quantity = tot_quantity
				end				

				-- if there are enough units, then deposit them
				if quantity <= tot_quantity and quantity > 0 then
					if g_game ~= nil then
                        --EnableTransactionControls(false)
						ClientEngine_DepositItemToBank(g_game, GLID, quantity, invType)

                        g_t_transaction["type"]  	= "toBank"
						g_t_transaction["index"] 	= index
						g_t_transaction["new_quantity"]	= quantity
					end
				end
			end
		end
	end

end

---------------------------------------------------------------------
--
-- Enable/Disable controls for a transaction
--
---------------------------------------------------------------------
function EnableTransactionControls( bEnable )

	local eh = Dialog_GetEditBox(gDialogHandle, "txtQuantity")
	if eh ~= 0 then
		Control_SetEnabled(eh, bEnable)
	end

  	eh = Dialog_GetEditBox(gDialogHandle, "txtQuantityBank")
	if eh ~= 0 then
		Control_SetEnabled(eh, bEnable)
	end

	local bh = Dialog_GetButton(gDialogHandle, "btnLeftArrow")
	if bh ~= 0 then
		Control_SetEnabled(bh, bEnable)
	end

	bh = Dialog_GetButton(gDialogHandle, "btnRightArrow")
	if bh ~= 0 then
		Control_SetEnabled(bh, bEnable)
	end
end


---------------------------------------------------------------------
--
-- Close Bank Window
--
---------------------------------------------------------------------
function btnClose_OnButtonClicked( buttonHandle )
	DestroyMenu(gDialogHandle)
end


---------------------------------------------------------------------
--
-- Get the value entered in the quantity field
--
---------------------------------------------------------------------
function GetQuantity()

	-- default quantity if not specified will be 1
	local quantity = 1

	local ebh = Dialog_GetEditBox(gDialogHandle, "txtQuantity")
	if ebh ~= 0 then
		quantity_text = EditBox_GetText(ebh)
		if quantity_text ~= "" then
			quantity  = tonumber(quantity_text)
			if not quantity then
				quantity = 0
			end
		end
	end

	return quantity
end

function GetQuantityBank()

	-- default quantity if not specified will be 1
	local quantity = 1

	local ebh = Dialog_GetEditBox(gDialogHandle, "txtQuantityBank")
	if ebh ~= 0 then
		quantity_text = EditBox_GetText(ebh)
		if quantity_text ~= "" then
			quantity  = tonumber(quantity_text)
			if not quantity then
				quantity = 0
			end
		end
	end

	return quantity
end

---------------------------------------------------------------------
--
-- Add Item to Bank
--
---------------------------------------------------------------------
function AddItemToBank(t_item)

	local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
	if t_item ~= 0 and t_item ~= nil and lbh ~= 0 then

		-- first check to see if that item exists
		local index = ListBox_FindItem(lbh, t_item["GLID"])
		if index ~= -1 then
		-- existing item

			local new_quantity = g_t_bank_items[index+1]["quantity"] + t_item["quantity"]

			-- update the existing quantity
			g_t_bank_items[index+1]["quantity"] = new_quantity

			-- update listbox text
			ListBox_SetItemText(lbh, index, g_t_bank_items[index+1]["name"] .. "\nQty: " .. tostring(new_quantity) )
		else
		-- new item, insert it

			table.insert(g_t_bank_items, t_item)

			ListBox_AddItem(lbh, t_item["name"] .. " \nQty: " .. tostring( t_item["quantity"] ), t_item["GLID"])

		end
	end
end

---------------------------------------------------------------------
--
-- Add Item to Inventory
--
---------------------------------------------------------------------
function AddItemToInventory(t_item)

	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	if t_item ~= 0 and t_item ~= nil and lbh ~= 0 then

		-- first check to see if that item exists
		local index = ListBox_FindItem(lbh, t_item["GLID"])
		if index ~= -1 then
		-- existing item

			local new_quantity = g_t_items[index+1]["quantity"] + t_item["quantity"]

			-- update the existing quantity
			g_t_items[index+1]["quantity"] = new_quantity

			-- update listbox text
			ListBox_SetItemText(lbh, index, g_t_items[index+1]["name"] .. "\nQty: " .. tostring(new_quantity) )
			
   		else
		-- new item, insert it

			table.insert(g_t_items, t_item)

			ListBox_AddItem(lbh, t_item["name"] .. "\nQty: " .. tostring( t_item["quantity"] ), t_item["GLID"])

		end
	end
	
end

---------------------------------------------------------------------
--
-- Update ListBox Icons
--
---------------------------------------------------------------------
function UpdateListBoxIcons(listBoxHandle, lst_OnScrollBarChanged)

	if listBoxHandle ~= 0 then

		local sbh = ListBox_GetScrollBar(listBoxHandle)
		if sbh ~= 0 then
			local page_size = ScrollBar_GetPageSize(sbh)
			local track_pos = ScrollBar_GetTrackPos(sbh)
			lst_OnScrollBarChanged(sbh, listBoxHandle, page_size, track_pos)
		end
	end
	
end

---------------------------------------------------------------------
--
-- Update Inventory ListBox Icons
--
---------------------------------------------------------------------
function UpdateInventoryListBoxIcons(listBoxHandle)
	UpdateListBoxIcons(listBoxHandle, lstInventory_OnScrollBarChanged)
end

---------------------------------------------------------------------
--
-- Update Bank ListBox Icons
--
---------------------------------------------------------------------
function UpdateBankInventoryListBoxIcons(listBoxHandle)
	UpdateListBoxIcons(listBoxHandle, lstBankInventory_OnScrollBarChanged)
end

---------------------------------------------------------------------
--
-- Inventory item selected
--
---------------------------------------------------------------------
function lstInventory_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	if bMouseDown == 0 then

		local GLID = ListBox_GetItemData(listBoxHandle, sel_index)
		if GLID == g_prev_inventory_sel_GLID and g_prev_inventory_index == sel_index then

			ListBox_ClearSelection(listBoxHandle)
			g_prev_inventory_sel_GLID = nil
			g_prev_inventory_index = nil
			
		else
			g_prev_inventory_sel_GLID = GLID
			g_prev_inventory_index = sel_index

		end
	
	end

	if bFromKeyBoard == 1 then
		g_prev_inventory_sel_GLID = ListBox_GetItemData(listBoxHandle, sel_index)
		g_prev_inventory_index = sel_index
	end
end

---------------------------------------------------------------------
--
-- Bank Inventory item selected
--
---------------------------------------------------------------------
function lstBankInventory_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	if bMouseDown == 0 then

		local GLID = ListBox_GetItemData(listBoxHandle, sel_index)
		if GLID == g_prev_bank_inventory_sel_GLID and g_prev_bank_inventory_index == sel_index then

			ListBox_ClearSelection(listBoxHandle)
			g_prev_bank_inventory_sel_GLID = nil
			g_prev_bank_inventory_index = nil
		else
			g_prev_bank_inventory_sel_GLID = GLID
			g_prev_bank_inventory_index = sel_index
		end
	
	end

	if bFromKeyBoard == 1 then
		g_prev_bank_inventory_sel_GLID = ListBox_GetItemData(listBoxHandle, sel_index)
		g_prev_bank_inventory_index = sel_index
	end
end

---------------------------------------------------------------------
--
-- makes a deep copy of a given table (the 2nd param is optional and for internal use)
-- circular dependencies are correctly copied.
--
---------------------------------------------------------------------
function tcopy(t, lookup_table)

	local copy = {}
	for i,v in pairs(t) do
		if type(v) ~= "table" then
			copy[i] = v
  		else
   			lookup_table = lookup_table or {}
   			lookup_table[t] = copy
  			if lookup_table[v] then
    				copy[i] = lookup_table[v] -- we already copied this table. reuse the copy.
   			else
    				copy[i] = tcopy(v,lookup_table) -- not yet copied. copy it.
   			end
  		end
 	end

	return copy
end


function imgIcon1_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	ListBox_SelectItem(lbh, 0 + g_last_inv_track_pos)
end 

function imgIcon2_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	ListBox_SelectItem(lbh, 1 + g_last_inv_track_pos)
end 

function imgIcon3_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	ListBox_SelectItem(lbh, 2 + g_last_inv_track_pos)
end 

function imgIcon4_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	ListBox_SelectItem(lbh, 3 + g_last_inv_track_pos)
end 

function imgIcon5_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	ListBox_SelectItem(lbh, 4 + g_last_inv_track_pos)
end 

function imgIcon6_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	ListBox_SelectItem(lbh, 5 + g_last_inv_track_pos)
end 

function imgIcon7_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	ListBox_SelectItem(lbh, 6 + g_last_inv_track_pos)
end 

function imgIcon8_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	ListBox_SelectItem(lbh, 7 + g_last_inv_track_pos)
end 

function imgIcon9_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	ListBox_SelectItem(lbh, 8 + g_last_inv_track_pos)
end 

function imgIcon10_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	ListBox_SelectItem(lbh, 9 + g_last_inv_track_pos)
end 


function imgBankIcon1_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
	ListBox_SelectItem(lbh, 0 + g_last_bank_track_pos)
end 

function imgBankIcon2_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
	ListBox_SelectItem(lbh, 1 + g_last_bank_track_pos)
end 

function imgBankIcon3_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
	ListBox_SelectItem(lbh, 2 + g_last_bank_track_pos)
end 

function imgBankIcon4_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
	ListBox_SelectItem(lbh, 3 + g_last_bank_track_pos)
end 

function imgBankIcon5_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
	ListBox_SelectItem(lbh, 4 + g_last_bank_track_pos)
end 

function imgBankIcon6_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
	ListBox_SelectItem(lbh, 5 + g_last_bank_track_pos)
end 

function imgBankIcon7_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
	ListBox_SelectItem(lbh, 6 + g_last_bank_track_pos)
end 

function imgBankIcon8_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
	ListBox_SelectItem(lbh, 7 + g_last_bank_track_pos)
end 

function imgBankIcon9_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
	ListBox_SelectItem(lbh, 8 + g_last_bank_track_pos)
end 

function imgBankIcon10_OnLButtonDown( dialogHandle, x, y )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstBankInventory")
	ListBox_SelectItem(lbh, 9 + g_last_bank_track_pos)
end 