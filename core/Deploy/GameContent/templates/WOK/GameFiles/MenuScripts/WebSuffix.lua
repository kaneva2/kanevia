----------------------------------------------
--
-- Web Query Suffixes
--
----------------------------------------------

USERID_SUFFIX = "kgp/userProfile.aspx?action=getUserIdFromAvatarName&avatar="
BALANCE_SUFFIX = "kgp/userProfile.aspx?action=getUserBalance&userId="

PROFILE_COUNTS_SUFFIX = "kgp/userProfile.aspx?action=getMyCounts"

GET_RAVES_SUFFIX = "kgp/ravePlace.aspx?action=GetRaves"
SET_RAVES_SUFFIX = "kgp/ravePlace.aspx?action=SetRave"

TRAVEL_LIST_SUFFIX = "kgp/travelzonelist.aspx?"

BUY_CREDITS_SUFFIX = "myKaneva/managecredits.aspx"
MY_KANEVA_SUFFIX = "default.aspx"
MY_PROFILE_SUFFIX = "channel/channelPage.aspx"

USERID_FRIENDS_SUFFIX = "kgp/friendsList.aspx?userId="
PROFILE_SUFFIX = "kgp/userProfile.aspx?action=getUserIdFromId&userId="
ABOUT_SUFFIX = "kgp/userProfile.aspx?action=getUserInterests&userId="
COUNTS_SUFFIX = "kgp/userProfile.aspx?action=getMyCounts&userId="
WEB_CHANNEL_PATH = "channel/"
RAVE_SORT_SUFFIX = "&sortbyraves=T"
RAVE_SUFFIX = "kgp/ravePlayer.aspx?userId="

BUY_ACCESS_PASS_SUFFIX = "myKaneva/buyaccess.aspx"

NEW_AVATAR_SUFFIX = "kgp/newAvatar.aspx?action=NewAvatar"
INTEREST_SUFFIX = "people/people.kaneva?int="