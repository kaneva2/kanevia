dofile("MenuHelper.lua")
dofile("CommonFunctions.lua")
dofile("..\\Scripts\\GameGlobals.lua")

-- prototype of a event handler method
-- function renderTextHandler(game, playerX, fromNetid, event, eventid, filter, objectid)
-- to access your dialog, use gDialogHandle
--
-- end 
PICTURE_FRAME_SELECTED = "PictureFrameSelectedEvent"

g_game = nil
g_dispatcher = nil

-- all inventory items
g_t_items = {}

RequestForInventoryAttachableObjectID 	= 51
USE_TYPE_ATTACHABLE_OBJECT 	= 11

-- boolean used to keep the handler, lstInventory_OnScrollBarChanged, from being called
-- when attribEventHandler() calls ListBox_AddItem()
g_b_loading_items = false

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	HelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel )	
	
	-- TODO register any of your handlers here like the following....
	-- ParentHandler_RegisterEventHandler( dispatcher, handler, "renderTextHandler", 1,0,0,0, "RenderTextEvent", KEP.HIGH_PRIO ) 
	ParentHandler_RegisterEventHandler( dispatcher, handler, "attribEventHandler", 1,0,0,0, "AttribEvent", KEP.MED_PRIO )
	
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	HelperInitializeKEPEvents( dispatcher, handler, debugLevel )

	-- TODO register any new event types here like the following...
	-- ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "LuaEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, PICTURE_FRAME_SELECTED, KEP.MED_PRIO )	
	
	g_dispatcher = dispatcher	

end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnInit)
--
---------------------------------------------------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	-- TODO init here
  g_game = Dispatcher_GetGame(KEP.dispatcher)
  
  if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
	end

	if g_game == 0 then
		g_game = nil
	end
	
	RequestInventory()
end

---------------------------------------------------------------------
--
-- Called from C to when the dialog is created (OnClose)
--
---------------------------------------------------------------------

function Dialog_OnDestroy(dialogHandle)
	-- TODO un-init here
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

---------------------------------------------------------------------
-- Attrib Event Handler
---------------------------------------------------------------------
function attribEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

  if filter == RequestForInventoryAttachableObjectID then
    --Dispatcher_LogMessage( g_dispatcher, KEP.ERROR_LOG_LEVEL, "in attrib event handler" )
    local lbh = Dialog_GetListBox(gDialogHandle, "lbPictureFrames")
		if lbh ~= 0 then
			-- always remove all items
			local lbh = Dialog_GetListBox(gDialogHandle, "lbPictureFrames")
			if lbh ~= 0 then
				ListBox_RemoveAllItems(lbh)
			end
			--Dispatcher_LogMessage( g_dispatcher, KEP.ERROR_LOG_LEVEL, "cleared" )

			g_t_items 	= {}

			g_b_loading_items = true

			g_t_items = decodeInventoryEvent( event )

			-- add to items table
			
			for i=1,#g_t_items do
				--Dispatcher_LogMessage( g_dispatcher, KEP.ERROR_LOG_LEVEL, "use_type="..use_type )
				if g_t_items[i]["use_type"] == USE_TYPE_ATTACHABLE_OBJECT then
					ListBox_AddItem(lbh, g_t_items[i]["name"] .. " [" .. tostring(g_t_items[i]["quantity"]) .. "]", g_t_items[i]["GLID"])
					--Dispatcher_LogMessage( g_dispatcher, KEP.ERROR_LOG_LEVEL, "added="..name )
				end
			end
			
			g_b_loading_items = false
			
		end
	end  	
end

---------------------------------------------------------------------
--
-- request inventory from server
--
---------------------------------------------------------------------
function RequestInventory()

	-- always remove all items (the list will repopulate when the request is answered)
	local lbh = Dialog_GetListBox(gDialogHandle, "lbPictureFrames")
	if lbh ~= 0 then
		ListBox_RemoveAllItems(lbh)
	end

	g_t_items 	= {}
    
	if g_game ~= nil then
    --Dispatcher_LogMessage( g_dispatcher, KEP.ERROR_LOG_LEVEL, "requesting inventory.." )
		ClientEngine_RequestCharacterInventoryAttachableObjects(g_game)		
	end
end

---------------------------------------------------------------------
--
-- Cancel Button - cancel and discard changes
--
---------------------------------------------------------------------
function btnCancel_OnButtonClicked( buttonHandle )    
  
	-- close this dialog
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------
--
-- Save Button - save changes
--
---------------------------------------------------------------------
function btnSelect_OnButtonClicked( buttonHandle )

  Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, "saving" ) 	
  
  local lbh = Dialog_GetListBox(gDialogHandle, "lbPictureFrames")
	if lbh ~= 0 then

		local index = ListBox_GetSelectedItemIndex(lbh)
		local GLID = ListBox_GetSelectedItemData(lbh)
		local invType = GameGlobals.IT_NORMAL 
		if g_t_items[index+1]["is_gift"] then
			invType = GameGlobals.IT_GIFT
		end
		if GLID ~= 0 then
			if g_game ~= nil then
				pictureFrameSelectedEvent = Dispatcher_MakeEvent( KEP.dispatcher, PICTURE_FRAME_SELECTED )	
		        Event_EncodeNumber( pictureFrameSelectedEvent, GLID )
		        Event_SetFilter( pictureFrameSelectedEvent, invType )
		        Dispatcher_QueueEvent( KEP.dispatcher, pictureFrameSelectedEvent )	
			end
		end
	end	
  	
  -- close this dialog
	DestroyMenu(gDialogHandle)
end

---------------------------------------------------------------------
-- Keyboard Handler
---------------------------------------------------------------------
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
      -- ESC key
      if key == 27 then
            -- close this menu
            DestroyMenu(gDialogHandle)
      end
end
