#import helper Python code
import KEP

# C functions
import Dispatcher
import Event
import GetSet
import ParentHandler
import GameGlobals
import InstanceId

gParent = 0
gDeedData = 0

ITEM = 0
ZONE = 1

NEW_DEED_TYPE = 203
CUSTOM_DEED_TYPE = 209

def logMsg( level, msg ):
    #needed for logging
    import ParentHandler
    ParentHandler.LogMessage( gParent, level, msg )
    #print msg

def loadDeedData():
    global gDeedData

    gDeedData = dict([
        (GameGlobals.UV_PORTAL_CASINO , [0, GameGlobals.ZONE_CASINO]),
        (GameGlobals.UV_PORTAL_THEATRE , [0, GameGlobals.ZONE_THEATRE]),    
        (GameGlobals.UV_APT_STUDIO , [GameGlobals.ITEM_APT_STUDIO, GameGlobals.ZONE_APT_02]),
        (GameGlobals.UV_APT_1BDR , [GameGlobals.ITEM_APT_1BDR, GameGlobals.ZONE_APT_03]),
        (GameGlobals.UV_APT_FLAT , [GameGlobals.ITEM_APT_FLAT, GameGlobals.ZONE_APT_04]),
        (GameGlobals.UV_APT_LARGE1BDR , [GameGlobals.ITEM_APT_LARGE1BDR, GameGlobals.ZONE_APT_01]),
        (GameGlobals.UV_APT_LOFT , [GameGlobals.ITEM_APT_LOFT, GameGlobals.ZONE_APT_LOFT]),
        (GameGlobals.UV_APT_GREEN_ACRES , [GameGlobals.ITEM_APT_GREEN_ACRES, GameGlobals.ZONE_GREEN_ACRES]),
        (GameGlobals.UV_BB_BBTHEATER , [GameGlobals.ITEM_BB_THEATER, GameGlobals.ZONE_BBTHEATER]),
        (GameGlobals.UV_BB_CAFE_LARGE , [GameGlobals.ITEM_BB_LG_CAFE, GameGlobals.ZONE_CAFE_LARGE]),
        (GameGlobals.UV_BB_CAFE_SMALL , [GameGlobals.ITEM_BB_SM_CAFE, GameGlobals.ZONE_CAFE_SMALL]),
        (GameGlobals.UV_BB_CONFERENCE , [GameGlobals.ITEM_BB_CONF_ROOM, GameGlobals.ZONE_CONFERENCE]),
        (GameGlobals.UV_BB_DANCEBAR , [GameGlobals.ITEM_BB_DANCE_BAR, GameGlobals.ZONE_DANCEBAR]),
        (GameGlobals.UV_BB_GREEN_ACRES , [GameGlobals.ITEM_BB_GREEN_ACRES, GameGlobals.ZONE_GREEN_ACRES]),
        (GameGlobals.UV_BB_NIGHTCLUB , [GameGlobals.ITEM_BB_NIGHT_CLUB, GameGlobals.ZONE_NIGHTCLUB]),
        (GameGlobals.UV_BB_THEATRE , [GameGlobals.ITEM_BB_THEATER, GameGlobals.ZONE_THEATRE]),
        (GameGlobals.UV_APT_32x64x64 , [GameGlobals.ITEM_APT_32x64x64, GameGlobals.ZONE_APT_32x64x64]),
        (GameGlobals.UV_APT_MANOR_HOME , [GameGlobals.ITEM_APT_MANOR_HOME, GameGlobals.ZONE_APT_MANOR_HOME]),
        (GameGlobals.UV_APT_CITY_LOFT , [GameGlobals.ITEM_APT_CITY_LOFT, GameGlobals.ZONE_APT_CITY_LOFT]),
        (GameGlobals.UV_APT_PREM_ISLAND , [GameGlobals.ITEM_APT_PREM_ISLAND, GameGlobals.ZONE_PREM_ISLAND]),
        (GameGlobals.UV_BB_PREM_ISLAND , [GameGlobals.ITEM_BB_PREM_ISLAND, GameGlobals.ZONE_PREM_ISLAND]),
        (GameGlobals.UV_APT_ISLAND , [GameGlobals.ITEM_APT_ISLAND, GameGlobals.ZONE_ISLAND]),
        (GameGlobals.UV_BB_ISLAND , [GameGlobals.ITEM_BB_ISLAND, GameGlobals.ZONE_ISLAND]),
        (GameGlobals.UV_BB_DELUXE_COFFEE_HOUSE , [GameGlobals.ITEM_BB_DELUXE_COFFEE_HOUSE, GameGlobals.ZONE_CITY_BLOCK]),
        (GameGlobals.UV_BB_CITY_BLOCK , [GameGlobals.ITEM_BB_CITY_BLOCK, GameGlobals.ZONE_CITY_BLOCK]),
        (GameGlobals.UV_APT_OUTDOOR_RETREAT , [GameGlobals.ITEM_APT_OUTDOOR_RETREAT, GameGlobals.ZONE_OUTDOOR_RETREAT]),
        (GameGlobals.UV_BB_OUTDOOR_RETREAT , [GameGlobals.ITEM_BB_OUTDOOR_RETREAT, GameGlobals.ZONE_OUTDOOR_RETREAT]),
        (GameGlobals.UV_APT_PREM_OUTDOOR_RETREAT , [GameGlobals.ITEM_APT_PREM_OUTDOOR_RETREAT, GameGlobals.ZONE_PREM_OUTDOOR_RETREAT]),
        (GameGlobals.UV_BB_PREM_OUTDOOR_RETREAT , [GameGlobals.ITEM_BB_PREM_OUTDOOR_RETREAT, GameGlobals.ZONE_PREM_OUTDOOR_RETREAT]),
        (GameGlobals.UV_APT_FASHIONISTA_FLAT , [GameGlobals.ITEM_APT_FASHIONISTA_FLAT, GameGlobals.ZONE_FASHIONISTA_FLAT]),
        (GameGlobals.UV_APT_TRADITIONAL_LIVING , [GameGlobals.ITEM_APT_TRADITIONAL_LIVING, GameGlobals.ZONE_TRADITIONAL_LIVING]),
        (GameGlobals.UV_APT_CONTEMPORARY_CONDO , [GameGlobals.ITEM_APT_CONTEMPORARY_CONDO, GameGlobals.ZONE_CONTEMPORARY_CONDO]),
        (GameGlobals.UV_APT_GAMERS_DEN , [GameGlobals.ITEM_APT_GAMERS_DEN, GameGlobals.ZONE_GAMERS_DEN]),
        (GameGlobals.UV_APT_SPORTS_ZONE , [GameGlobals.ITEM_APT_SPORTS_ZONE, GameGlobals.ZONE_SPORTS_ZONE]),
        (GameGlobals.UV_APT_BEACH_HOUSE , [GameGlobals.ITEM_APT_BEACH_HOUSE, GameGlobals.ZONE_BEACH_HOUSE]),
        (GameGlobals.UV_BB_BEACH_HOUSE , [GameGlobals.ITEM_BB_BEACH_HOUSE, GameGlobals.ZONE_BEACH_HOUSE])
        ])

def changeZone( dispatcher, idReference, inventoryItemId, apartmentId, upgradeType, invType ):
    e = Dispatcher.MakeEvent( dispatcher, "ZoneChangeEvent" )
    Event.EncodeNumber( e, idReference )
    Event.EncodeNumber( e, inventoryItemId )
    Event.EncodeNumber( e, apartmentId )
    Event.EncodeNumber( e, upgradeType )
    Event.EncodeNumber( e, invType )
    Event.EncodeNumber( e, 1 ) ## readd this item to inventory on fail
    
    Dispatcher.QueueEvent( dispatcher, e ) 

def handlePlayerUseDeed( dispatcher, fromNetId, event, eventid, filter, objectid):
    global gDeedData
    import Game

    deedFilter = int(filter)

    idReference = Event.DecodeNumber( event )
    idUseValue = Event.DecodeNumber( event )
    invType = Event.DecodeNumber( event ) # gift or not
    globalid = Event.DecodeNumber( event )
    useType = Event.DecodeNumber( event )

    logMsg( KEP.TRACE_LOG_LEVEL, "idReference: "+str(idReference)+" idUseValue: "+str(idUseValue)+" invType: "+str(invType)+" globalid: "+str(globalid)+" useType: "+str(useType))

    player1 = Game.GetPlayerByNetTraceId( Dispatcher.GetGame( dispatcher ), idReference, 0, 0 )
    if player1 == 0: # not found, or not in this arena, don't care
        return KEP.EPR_NOT_PROCESSED
   
    if deedFilter == CUSTOM_DEED_TYPE:
        #custom deed uses current zones with new globalid
        changeZone( dispatcher, idReference, globalid, int(idUseValue), KEP.ChannelUpgrade, invType )
        logMsg( KEP.TRACE_LOG_LEVEL, "Using custom deed! player: "+str(idReference)+ " deed: "+str(globalid))
    elif deedFilter == NEW_DEED_TYPE:
        if gDeedData.has_key(idUseValue):
            deed = gDeedData[idUseValue]
            #old deed mapping
            changeZone( dispatcher, idReference, deed[ITEM], deed[ZONE], KEP.ChannelUpgrade, invType )
        else:
            logMsg( KEP.WARN_LOG_LEVEL, "handlePlayerUseNew deed id not found in dict. Use value: "+str(idUseValue) )
        
    return KEP.EPR_CONSUMED
    
def useAnimationItem( dispatcher, fromNetId, event, eventid, filter, objectid):
    import Game

    #KEP.debugPrint( "We are in the handler" )

    idReference = Event.DecodeNumber( event )
    idUseValue = Event.DecodeNumber( event )
    invType = Event.DecodeNumber( event ) # gift or not

    Dispatcher.LogMessage( dispatcher, KEP.DEBUG_LOG_LEVEL, "Got animation event of " + str(idUseValue) )

    return KEP.EPR_OK
    
def useP2PAnimationItem( dispatcher, fromNetId, event, eventid, filter, objectid):
    import Game

    #KEP.debugPrint( "We are in the handler" )

    idReference = Event.DecodeNumber( event )
    idUseValue = Event.DecodeNumber( event )
    invType = Event.DecodeNumber( event ) # gift or not

    Dispatcher.LogMessage( dispatcher, KEP.DEBUG_LOG_LEVEL, "Got P2Panimation event of " + str(idUseValue) )

    return KEP.EPR_OK
    
def NPCItem( dispatcher, fromNetId, event, eventid, filter, objectid):
    import Game

    #KEP.debugPrint( "We are in the handler" )

    idReference = Event.DecodeNumber( event )
    idUseValue = Event.DecodeNumber( event )
    invType = Event.DecodeNumber( event ) # gift or not

    Dispatcher.LogMessage( dispatcher, KEP.DEBUG_LOG_LEVEL, "Got NPC event of " + str(idUseValue) )

    return KEP.EPR_OK
    
    
def InitializeKEPEventHandlers( dispatcher, parent, dbgLevel ):
    global gParent

    gParent = parent

    ParentHandler.RegisterFilteredEventHandler( dispatcher, parent, handlePlayerUseDeed, 1,0,0,0, "PlayerUseItem", NEW_DEED_TYPE, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.MED_PRIO ) 
    ParentHandler.RegisterFilteredEventHandler( dispatcher, parent, handlePlayerUseDeed, 1,0,0,0, "PlayerUseItem", CUSTOM_DEED_TYPE, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.MED_PRIO ) 

    loadDeedData()

    # test handlers
    # ParentHandler.RegisterFilteredEventHandler( dispatcher, parent, useAnimationItem, 1,0,0,0, "PlayerUseItem", KEP.USE_TYPE_ANIMATION, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.HIGH_PRIO ) 
    # ParentHandler.RegisterFilteredEventHandler( dispatcher, parent, useP2PAnimationItem, 1,0,0,0, "PlayerUseItem", KEP.USE_TYPE_P2P_ANIMATION, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.HIGH_PRIO ) 
    # ParentHandler.RegisterFilteredEventHandler( dispatcher, parent, NPCItem, 1,0,0,0, "PlayerUseItem", KEP.USE_TYPE_NPC, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.HIGH_PRIO ) 

    return  True 

#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    pass    

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    " called from C to give us a chance to cleanup before shutdown or re-init"
    return KEP.getDependentModules(__name__)
