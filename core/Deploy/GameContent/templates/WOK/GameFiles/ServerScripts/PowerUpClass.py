#import helper Python code
import KEP

# constants for GPB
import WoK

# C functions
import InstanceId
import Dispatcher
import Event
import GetSet
import ParentHandler
import random

import PlayerIds
import ServerItemIds

HEALTHPOWERUP = 1
WEAPONPOWERUP = 2
ITEMPOWERUP   = 3
CASHPOWERUP   = 4

HORNET_ID_MALE   = 61
HORNET_ID_FEMALE = 63

#supports the addition of many powerup types.  if a powerup has more than one
#location it will be randomly moved around the map.  puid is the aicfg that
#you are spawning, if you want to spawn more of the same type you need two bot
#configs.  Player race is still hardcoded here.

class PowerUpClass(object):
    __powerups      = list() 
    __powerdict     = dict()

    def __init__(self,dispatcher,game,channelInstance):
        self._game = game
        self._dispatcher = dispatcher
        self._channelInstance = channelInstance
        
    def AddPowerUp(self, channelInstance, puid, x, y, z, powertype, param1, param2, param3, rotation ):
        if puid not in self.__powerdict:
            self.__powerups.append( list() ) #add a slot
            self.__powerdict[puid] =  len(self.__powerups) - 1

        index = self.__powerdict[puid]
        self.__powerups[index].append( list([puid,x,y,z,powertype,param1,param2,param3,rotation]) )
       
    #find a random powerup and spawn it based on the puid
    def SpawnRandomPowerup( self, puid ):
        import Game
        index = self.__powerdict[puid]
        i = random.randint( 0, len(self.__powerups[index]) - 1)
        Game.SpawnStaticAIByNumber( self._game, self.__powerups[index][i][0], self.__powerups[index][i][1], self.__powerups[index][i][2], self.__powerups[index][i][3], self._channelInstance)

    #spawn a powerup by index i.e. the first one is 0
    def SpawnIndexedPowerup( self, puid, i ):
        import Game
        index = self.__powerdict[puid]
        Game.SpawnStaticAIByNumber( self._game, self.__powerups[index][i][0], self.__powerups[index][i][1], self.__powerups[index][i][2], self.__powerups[index][i][3], self._channelInstance)

    #specific code for the powertypes here
    def powerUpHit(self, player1, Player2Cfg, fromNetId ):
        import Game

        #debug
        #msg = "you just hit " + str(Player2Cfg) + " on channel " + str(self._channelInstance)
        #Game.SendTextMessage( self._game, player1, msg, 0 )

        ## Are we looking for this entity (npc)?
        if not self.__powerdict.has_key(Player2Cfg):
            return KEP.EPR_NOT_PROCESSED     

        index = self.__powerdict[Player2Cfg]
        powertype = self.__powerups[index][0][4] #the type of powerup, must all be the same for a list
        
        #health powerup param1 is max health
        if powertype == HEALTHPOWERUP: #health
            Game.DeSpawnStaticAIByNumber( self._game, Player2Cfg, self._channelInstance)
            newenergy = GetSet.GetNumber( player1, PlayerIds.ENERGY)
            maxenergy = GetSet.GetNumber( player1, PlayerIds.MAXENERGY)
            newenergy = newenergy + self.__powerups[index][0][5]
            if newenergy > maxenergy:
                newenergy = maxenergy
            GetSet.SetNumber( player1, PlayerIds.ENERGY, newenergy)
            Game.UpdatePlayerData( self._game, fromNetId )
            Game.SendTextMessage( self._game, player1, "You've been healed!", 0 )

            self.SpawnRandomPowerup( Player2Cfg )
            return KEP.EPR_CONSUMED

        #item and ammo powerup param1 is weapon for race 1, param2 is weapon for race 2, param3 is ammo
        if powertype == WEAPONPOWERUP: #item and ammo
            Game.DeSpawnStaticAIByNumber( self._game, Player2Cfg, self._channelInstance)
            Player1Race = GetSet.GetNumber( player1, PlayerIds.RACEID )
            ammoid = self.__powerups[index][0][7]
            wepid = self.__powerups[index][0][5]
            Game.AddInventoryItemToPlayer( self._game, player1, ammoid, 100, 0) 

            edb = GetSet.GetNumber( player1, PlayerIds.DBCFG )
            if edb == WoK.MALE_EDB:
                Game.AddInventoryItemToPlayer( self._game, player1, wepid-2, 1, 1)
            else:
                Game.AddInventoryItemToPlayer( self._game, player1, wepid, 1, 1)

            self.SpawnRandomPowerup( Player2Cfg )
            return KEP.EPR_CONSUMED

        #item only powerup param1 is item for race 1, param2 is item for race 2, param3 is qty
        if powertype == ITEMPOWERUP: #item only
            Game.DeSpawnStaticAIByNumber( self._game, Player2Cfg, self._channelInstance)
            Player1Race = GetSet.GetNumber( player1, PlayerIds.RACEID )
            qty = self.__powerups[index][0][7]
            itemid = self.__powerups[index][0][5]
            Game.AddInventoryItemToPlayer( self._game, player1, itemid, qty, 0)

            self.SpawnRandomPowerup( Player2Cfg )
            return KEP.EPR_CONSUMED

        #item only powerup param1 is item for race 1, param2 is item for race 2, param3 is qty
        if powertype == CASHPOWERUP: #cash only
            Game.DeSpawnStaticAIByNumber( self._game, Player2Cfg, self._channelInstance)

            return KEP.EPR_CONSUMED

        return KEP.EPR_OK

        
    

