
-- "include" files
dofile( "..\\Scripts\\PlayerIds.lua" )
dofile( "..\\Scripts\\KEP.lua" )
dofile( "..\\Scripts\\CMD.lua" )
dofile( "ServerItemIds.lua" )

TALK_RADIUS = 100
WHISPER_RADIUS = 10
SHOUT_RADIUS = 0

---------------------------------------------------------------------
--
-- test fn
--
---------------------------------------------------------------------
function test(a,b)

  print('Im in test now!!!')
  print( 'a = ', a );
  print( 'b = ', b );

  return 12, 13, 14
end

function debugStuffToCutAndPaste()

	if debug ~= nil then
		print('locals')
	   local a = 1
      while true do
        local name, value = debug.getlocal(1, a)
        if not name then break end
        print(name, value)
        a = a + 1
      end

		print( 'in the Lua text handler, type cont to continue' )
	--	debug.debug()
	end

end

---------------------------------------------------------------------
--
-- our event handler, called from C
--
---------------------------------------------------------------------
function textHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

	if Game_GetPlayerByNetId == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_SERVER_FN )
	end

	game = Dispatcher_GetGame(dispatcher)

	-- the contents of the text event is the users id and a string
	networkTraceId = Event_DecodeNumber( event )
	msg = Event_DecodeString( event )
	chatType = Event_DecodeNumber( event )
	radius = TALK_RADIUS
	origChatType = ChatType.Private 

	msg = string.gsub(msg, "<", " ")
	--msg = string.gsub(msg, ">", " ")

	if chatType == ChatType.Private or chatType == ChatType.S2S then
	   toDistribution = Event_DecodeString( event )
	   fromRecipient = Event_DecodeString( event )
	   origChatType = Event_DecodeNumber( event )
	end

	if chatType == ChatType.Private then
		chatType = ChatType.Private
		msg = " says to you > "..msg
	elseif chatType == ChatType.S2S then
		-- nothing
	elseif filter == CMD.TEAM or chatType == ChatType.Team then
		chatType = ChatType.Team
		msg = " says to team > "..msg
		radius = 0
	elseif filter == CMD.TRIBE or chatType == ChatType.Race then
		chatType = ChatType.Race
		msg = " says to tribe > "..msg
		radius = 0
	elseif chatType == ChatType.Clan then
		msg = " says to clan > "..msg
	elseif chatType == ChatType.Group then
		msg = " says to group > "..msg
	elseif filter == CMD.WHISPER then
		chatType = ChatType.Whisper
		msg = " whispers > "..msg
		radius = WHISPER_RADIUS
	elseif filter == CMD.SHOUT then
		chatType = ChatType.Shout
		msg = " shouts > "..msg
		radius = SHOUT_RADIUS
	elseif filter == CMD.DISMOUNT then
		Game_Demount( game, fromNetid )
		return KEP.EPR_CONSUMED
	elseif filter ~= 0 then
		-- if chattype not group or clan, we don't want it
		-- pass it off to default processing
		Game_TextHandler( game, filter, msg, fromNetid )

		return KEP.EPR_CONSUMED
	else
		chatType = ChatType.Talk
		msg = " says > "..msg
	end

	local charName = ""
	local x, y, z

	if chatType ~= ChatType.S2S then

		-- local netid

		player = Game_GetPlayerByNetId( game, fromNetid )

		if  player == 0 then
		    -- see if it's an NPC
		    obj = Game_GetItemObjectByNetTraceId( game, networkTraceId )

		    if obj == 0 then
				ParentHandler_LogMessage( KEP.parent, KEP.WARN_LOG_LEVEL,  "Didn't get player in TextHandler.lua netid="..tostring(fromNetId).." player net trace id ="..tostring(networkTraceId) )
				return KEP.EPR_NOT_PROCESSED
			end

			charName = GetSet_GetString( obj, ServerItemIds.DISPLAYEDNAME )
			channelId = GetSet_GetNumber( obj, ServerItemIds.CHANNEL )
			x, y, z =  GetSet_GetVector( obj, ServerItemIds.POSITION )
		else
			charName = GetSet_GetString( player, PlayerIds.CHARNAME )
			channelId = GetSet_GetNumber( player, PlayerIds.CURRENTCHANNEL )
			x, y, z = GetSet_GetVector( player, PlayerIds.N3DPOSITION )
		end
	end

	-- todo audit log here or in broadcast msg
	if chatType == ChatType.Private or chatType == ChatType.S2S then

		isRemote = 0.0
		if chatType == ChatType.Private then
			msg = charName..msg
		else
			isRemote = 1.0
			chatType = origChatType
		end

		if string.len(toDistribution) > 0 and string.len(fromRecipient) > 0 then 
		
			Game_BroadcastMessageToDistribution( game,
					msg,
					fromNetid,
					toDistribution,
					fromRecipient,
					chatType,
					isRemote )
		else -- broadcast from gm
			local channelId = InstanceId_MakeId( -1, 0, 0 ) -- invalid id == broadcast all
			Game_BroadcastMessage( game,
					0,
					msg,
					0,0,0,
					channelId,
					fromNetid,
					ChatType.System,
					1) -- not all instances of channel
		end
	else

		Game_BroadcastMessage( game,
					radius,
					charName..msg,
					x,y,z,
					channelId,
					fromNetid,
					chatType,
					0) -- not all instances of channel
	end

	return KEP.EPR_CONSUMED

end

---------------------------------------------------------------------
--
-- our event handler, called from C
--
---------------------------------------------------------------------
function testHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- the contents of the text event is the users id and a string
	num1 = Event_DecodeNumber( event )
	num2 = Event_DecodeNumber( event )
	s1 = Event_DecodeString( event )

	print ( 'Lua says: Values from event are ' , num1, num2, s1 )

	return KEP.EPR_OK
end


---------------------------------------------------------------------
--
-- function to make the version
--
---------------------------------------------------------------------
function makeVersion( a, b, c, d )
	return (a*16777216) + (b*65536)+ (c*256) +d
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

--		if debug ~= nill then
--			print( 'init: type cont to continue debugger' )
--			debug.debug()
--			test( 123, 456 )
--		end
	KEP.parent = handler
	KEP.dispatcher = dispatcher
	KEP.debugLevel = debugLevel

	-- call the c funtion to register our event handlers
	return ParentHandler_RegisterEventHandler( dispatcher, handler, "textHandler", 1,0,0,0, "TextEvent", KEP.MED_PRIO )
--	ParentHandler_RegisterEventHandler( dispatcher, handler, "testHandler", 1,0,0,0, "PythonHighEvent", KEP.MED_PRIO )
--	ParentHandler_RegisterEventHandler( dispatcher, handler, "testHandler", 1,0,0,0, "PythonMedEvent", KEP.MED_PRIO )
--	ParentHandler_RegisterEventHandler( dispatcher, handler, "testHandler", 1,0,0,0, "PythonLowEvent", KEP.MED_PRIO )
--	return ParentHandler_RegisterEventHandler( dispatcher, handler, "testHandler", 1,0,0,0, "LuaEvent", KEP.MED_PRIO )

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	-- call the c funtion to register our event, we have just one
	KEP.parent = handler
	KEP.dispatcher = dispatcher
	KEP.debugLevel = debugLevel

	ret = Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "LuaEvent", KEP.MED_PRIO )

end

