#python modules
import time, inspect

import pdb

#import helper Python code
import KEP
import CMD

#get set ids
import PlayerIds
import ChatType

#import the C methods from the exe
import ParentHandler
import Dispatcher
import Event
import GetSet
import InstanceId

gDispatcher = 0
gParent = 0
gBreakIntoDebugger = True


RETURN_TRUE = 1
RETURN_FALSE = 0

#-------------------------------------------------------------------
class TimeManager(object):
    "general purpose class that handles timer-related events"

    TIMER_NAME       = "GroupingTimerEvent"
    TIMER_INTERVAL   = 2 # seconds


    #-------------------------------------------------------------------
    def __init__(self):
        "constructor creates and initialized members"

        pass
        
    
    #-------------------------------------------------------------------
    # static event handler methods
    #-------------------------------------------------------------------

    #-------------------------------------------------------------------
    @staticmethod
    def handleTimerEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        "callback timer fires"

        cur_time = time.time()

        gInvitationManager.ClearExpiredInvitations( cur_time )
        gGroupManager.UpdateInfo( cur_time )

        return KEP.EPR_OK

#-------------------------------------------------------------------
class Group(object):
    "group class"

    # class-level variables

    MIN_MEMBERS = 2 # group must have at least 2 members

    #-------------------------------------------------------------------
    def __init__(self, player1_netid, player2_netid):
        "constructor creates and initialized members"

        # group must contain at least 2 members

        self._members  = list() # ordering matters -- first position is leader
        self._members.append( player1_netid )
        self._members.append( player2_netid )

    #-------------------------------------------------------------------
    # methods
    #-------------------------------------------------------------------

    #-------------------------------------------------------------------
    def AddMember( self, player_netid ):
        "add a player to this group"

        if not self.IsMember(player_netid):
            self._members.append( player_netid )
            return True

        return False

    #-------------------------------------------------------------------
    def RemoveMember( self, player_netid ):
        "remove a player from this group"

        if self.IsMember(player_netid):
            self._members.remove( player_netid )
            # print self._members
            return True

        return False
            

    #-------------------------------------------------------------------
    def IsMember( self, player_netid ):
        "checks if the given player is in this group"

        for member_id in self._members:
            if member_id == player_netid:
                return True

        return False

    #-------------------------------------------------------------------
    def IsLeader( self, player_netid ):
        "checks if the given player is this group leader"

        return self._members[0] == player_netid
 
    #-------------------------------------------------------------------
    def NumMembers( self ):
        "get number of members in the group"

        return len( self._members )

    #-------------------------------------------------------------------
    def MakeLeader( self, player_netid ):
        "make player the new leader"

        if self.IsMember( player_netid ):

            # print self._members

            # swap positions
            cur_leader              = self._members[0]
            index                   = self._members.index( player_netid )

            self._members[0]        = player_netid
            self._members[index]    = cur_leader
            
            # print self._members

            return True

        return False
            
    
#------------------------------------------------------------------- 

#-------------------------------------------------------------------
class GroupManager(object):
    "Group manager class to manage group related info"

    # class-level variables
    

    # event filters
    PLAYER_JOINED           = 1
    PLAYER_LEFT             = 2
    PLAYER_REMOVED          = 3
    NEW_LEADER              = 4
    UPDATE_INFO             = 5
    GROUP_ENDED             = 6
    REQUEST_DENIED          = 7

    GROUP_EVENT_NAME        = "GroupEvent"

    GROUP_UPDATE_INTERVAL   = 5     # seconds
    GROUP_UPDATE            = True  # whether to handle updates

    #-------------------------------------------------------------------
    def __init__(self):
        "constructor creates and initialized members"

        self._groups                = dict()
        self._last_update_timestamp = 0
        
    
    #-------------------------------------------------------------------
    # static event handler methods
    #-------------------------------------------------------------------

    #-------------------------------------------------------------------
    @staticmethod
    def handleInvitePlayerEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        "callback when a player is invited to join a group"

	import Game

	# the contents of the text event is the users id and a string
	player_id 	= Event.DecodeNumber( event )
	invitee_name	= Event.DecodeString( event )
	chatType 	= Event.DecodeNumber( event )

        game            = Dispatcher.GetGame(dispatcher)
        
        invitee_obj    	= Game.GetPlayerByName( game, invitee_name, 1, 1 )
        player_obj    	= Game.GetPlayerByNetTraceId( game, player_id, 1, 1 )

	if player_obj != 0:

            player_name     = GetSet.GetString( player_obj, PlayerIds.CHARNAME )

            if invitee_obj != 0:

                invitee_netid   = GetSet.GetNumber( invitee_obj, PlayerIds.CURRENTID )


                # verify here
                #  not yourself
                #  not already in a group

                if invitee_netid == fromNetId:
		    # notify yourself that you're not available (can't add yourself)
                    gInvitationManager.SendInvitationDeclined( -1, player_name, invitee_name, fromNetId, gInvitationManager.PLAYER_NOT_AVAILABLE_FILTER )


		elif not gGroupManager.IsGroupMember( invitee_netid ):

                    invitation_id = gInvitationManager.GetInvitationID( fromNetId, invitee_netid )
                    
                    if invitation_id == -1:
                        # add to invitations
                        invitation_id = gInvitationManager.AddInvitation( fromNetId, invitee_netid, player_name, invitee_name )
                        # print "added invitation_id=" + str(invitation_id)
                    else:
                        # update timestamp
                        invitation = gInvitationManager.GetInvitation( invitation_id )
                        if invitation != 0:
                            invitation.RefreshTimestamp()
                            # print "refreshed invitation_id=" + str(invitation_id)

                    # print invitation_id

                    gInvitationManager.SendInvitation( invitation_id, player_name, invitee_netid )

                    gInvitationManager.SendInvitationConfirmation( invitee_name, fromNetId )

                else:
                    # notify player that invitee is already in a group
                    gInvitationManager.SendInvitationDeclined( -1, player_name, invitee_name, fromNetId, gInvitationManager.PLAYER_IN_GROUP_FILTER )

            else:
                # could not find invitee
                gInvitationManager.SendInvitationDeclined( -1, player_name, invitee_name, fromNetId, gInvitationManager.PLAYER_NOT_AVAILABLE_FILTER )
                
        return KEP.EPR_CONSUMED

    #-------------------------------------------------------------------
    @staticmethod
    def handleDisconnectedEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        "callback when disconnects"

        player_netid = Event.DecodeNumber( event )
        # print "player disconnected " + str(player_netid) + " " + str(fromNetId)

        # remove player from groups if a member
        # check this first because if the player is in a group,
        # then there would not be any invitations for the player
        if not gGroupManager.RemovePlayer( player_netid ):
            gInvitationManager.ExpireInvitationsForPlayer( player_netid )

        return KEP.EPR_OK

    #-------------------------------------------------------------------
    @staticmethod
    def handleJoinGroupEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        "callback when an invitee accepts a group invitation"

	import Game

	# the contents of the text event is the users id and a string
	player_id 	= Event.DecodeNumber( event )
	invitation_id	= float( Event.DecodeString( event ) )

        game            = Dispatcher.GetGame(dispatcher)

        # print "join request from player_netid=" + str(fromNetId) + ", invitation_id=" + str(invitation_id)

        # match invitation id and invitee_netid

        # if the invitation exists and the player asking to join is the one invited
        invitation = gInvitationManager.GetInvitation( invitation_id )
        if invitation != 0 and invitation._invitee_netid == fromNetId:

            # not already in a group -- should never get here if already in group
            if not gGroupManager.IsGroupMember( invitation._invitee_netid ):

                # find existing group with the leader (player)
                group_id    = gGroupManager.GetGroupID( invitation._player_netid )
                if group_id != -1:
                    group = gGroupManager.GetGroup( group_id )
                    if group != 0:
                        if group.AddMember( invitation._invitee_netid ):
                            # if successfully added to a group
                            # print "added " + str(invitation._invitee_netid) + " to group_id=" + str(group_id)
                            
                            # remove the invitation that was just accepted
                            gInvitationManager.RemoveInvitation( invitation_id )
                            # print "removed invitation_id=" + str(invitation_id)

                            # notify invitee they have joined the group
                            # notify leader that invitee has joined the group
                            gGroupManager.NotifyGroupMemberJoined( group_id, invitation._invitee_name )

                            # expire all other outstanding invitations for the invitee
                            gInvitationManager.ExpireInvitationsForInvitee( invitation._invitee_netid )
                else:
                    # else, need to create a new group
                    group_id = gGroupManager.AddGroup( invitation._player_netid, invitation._invitee_netid )
                    if group_id != -1:
                        # if successfully added to a group
                        # print "added " + str(invitation._player_netid) + " and " + str(invitation._invitee_netid) + " to new group_id=" + str(group_id)

                        # remove the invitation that was just accepted
                        gInvitationManager.RemoveInvitation( invitation_id )
                        # print "removed invitation_id=" + str(invitation_id)

                        # notify leader that invitee has joined the group
                        # notify invitee they have joined the group
                        gGroupManager.NotifyGroupMemberJoined( group_id, invitation._invitee_name )
                        
                        # expire all other outstanding invitations for the leader (player)
                        gInvitationManager.ExpireInvitationsForInvitee( invitation._player_netid )

                        # expire all other outstanding invitations for the invitee
                        gInvitationManager.ExpireInvitationsForInvitee( invitation._invitee_netid )
        
        return KEP.EPR_CONSUMED

    #-------------------------------------------------------------------
    @staticmethod
    def handleLeaveGroupEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        "callback when a player wants to leave a group"

	# the contents of the text event is the users id and a string
	player_id 	= Event.DecodeNumber( event )

        # print "leave request from player_netid=" + str(fromNetId)

        if gGroupManager.RemovePlayer( fromNetId ):

            group_id = self.GetGroupID( fromNetId )
            if group_id != -1:
                
                group = self.GetGroup( group_id )
                if group != 0:

                    member_obj = Game.GetPlayerByNetId( game, fromNetId )
                    if member_obj != 0:
                        
                        member_name = GetSet.GetString( member_obj, PlayerIds.CHARNAME )

                        leader_obj = Game.GetPlayerByNetId( game, group._members[0] )
                        if leader_obj != 0:
                            group_name = GetSet.GetString( leader_obj, PlayerIds.CHARNAME )
                            
                            self.SendGroupEvent( group_name, member_name, fromNetId, GroupManager.PLAYER_LEFT )

        return KEP.EPR_CONSUMED

    #-------------------------------------------------------------------
    @staticmethod
    def handleRemovePlayerGroupEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        "callback when a group leader wants to remove a member from the group"

	import Game

	# the contents of the text event is the users id and a string
	player_id 	= Event.DecodeNumber( event )
	member_name	= Event.DecodeString( event )

        game            = Dispatcher.GetGame(dispatcher)

        # find existing group with the leader (player)
        group_id = gGroupManager.GetGroupID( fromNetId )
        if group_id != -1:
            
            group = gGroupManager.GetGroup( group_id )
            if group != 0:
                
                if group.IsLeader( fromNetId ):

                    # get the name of the leader (group)
                    group_name = ""
                    leader_obj = Game.GetPlayerByNetId( game, fromNetId )
                    if leader_obj != 0:
                        group_name = GetSet.GetString( leader_obj, PlayerIds.CHARNAME )

                    member_obj = Game.GetPlayerByName( game, member_name, 1, 1 )
                    if member_obj != 0:
                        
                        member_netid   = GetSet.GetNumber( member_obj, PlayerIds.CURRENTID )
                        if group.IsMember( member_netid ):

                            # print "group leader removes player_netid=" + str(member_netid)
                            gGroupManager.RemovePlayer( member_netid )

                            # notify member they were removed
                            event = Dispatcher.MakeEvent( gDispatcher, GroupManager.GROUP_EVENT_NAME )
                            if event != 0:
                                Event.SetFilter( event, GroupManager.PLAYER_REMOVED )
                                Event.EncodeString( event, group_name )
                                Event.AddTo( event, member_netid )
                                Dispatcher.QueueEvent( gDispatcher, event )
                else:
                    # notify member they do not have rights to do this
                    gGroupManager.SendRequestDenied( fromNetId )

        return KEP.EPR_CONSUMED

    #-------------------------------------------------------------------
    @staticmethod
    def handleChangeLeaderGroupEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        "callback when a group leader wants to make another member the new leader"

	import Game

	# the contents of the text event is the users id and a string
	player_id 	= Event.DecodeNumber( event )
	member_name	= Event.DecodeString( event )

        game            = Dispatcher.GetGame(dispatcher)

        # find existing group with the leader (player)
        group_id = gGroupManager.GetGroupID( fromNetId )
        if group_id != -1:
            
            group = gGroupManager.GetGroup( group_id )
            if group != 0:
                
                if group.IsLeader( fromNetId ):

                    member_obj = Game.GetPlayerByName( game, member_name, 1, 1 )
                    if member_obj != 0:
                        
                        member_netid   = GetSet.GetNumber( member_obj, PlayerIds.CURRENTID )
                        if group.IsMember( member_netid ):

                            # print "new group leader player_netid=" + str(member_netid)
                            
                            gGroupManager.ChangeLeader( group, member_netid )
                else:
                    # notify member they do not have rights to do this
                    gGroupManager.SendRequestDenied( fromNetId )
                

        return KEP.EPR_CONSUMED

    #-------------------------------------------------------------------
    @staticmethod
    def handleGroupChatEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        "callback when a group member sends chat message"

	import Game

	game            = Dispatcher.GetGame(dispatcher)

	# the contents of the text event is the users id and a string
	player_id 	= Event.DecodeNumber( event )
	msg	        = Event.DecodeString( event )
	chatType 	= Event.DecodeNumber( event )

	if ChatType.Group == chatType:

            # find existing group with the player
            group_id = gGroupManager.GetGroupID( fromNetId )
            if group_id != -1:
                
                group = gGroupManager.GetGroup( group_id )
                if group != 0:
                    
                    player_obj = Game.GetPlayerByNetId( game, fromNetId )
                    if player_obj != 0:
                        player_name = GetSet.GetString( player_obj, PlayerIds.CHARNAME )

                        msg = player_name + " says to group > " + msg

                        event = Dispatcher.MakeEvent( gDispatcher, "RenderTextEvent" )
                        if event != 0:
                            Event.EncodeString( event, msg )
                            Event.EncodeNumber( event, ChatType.Group )

                            for member_id in group._members:
                                Event.AddTo( event, member_id )

                            Dispatcher.QueueEvent( gDispatcher, event )

                            return KEP.EPR_CONSUMED

                

        return KEP.EPR_OK 

    #-------------------------------------------------------------------
    # methods
    #-------------------------------------------------------------------

    #-------------------------------------------------------------------
    def AddGroup( self, player1_netid, player2_netid ):
        "add a group"

        group = Group( player1_netid, player2_netid)

        group_id = id(group)
        self._groups[group_id] = group

        return group_id

    #-------------------------------------------------------------------
    def RemoveGroup( self, group_id ):
        "remove a group"

        if self._groups.has_key( group_id ):

            # notify the remaining member that the group has ended
            self.SendGroupEvent( "", "", self._groups[group_id]._members[0], GroupManager.GROUP_ENDED )
            
            del self._groups[group_id]

    #-------------------------------------------------------------------
    def IsGroupMember( self, player_netid ):
        "checks if a player is a member of any groups"

        for group_id in self._groups:

            if self._groups[group_id].IsMember( player_netid ):
                return True

        return False

    #-------------------------------------------------------------------
    def GetGroupID( self, player_netid ):
        "get group that contains the given player"

        for group_id in self._groups:

            if self._groups[group_id].IsMember( player_netid ):
                return group_id

        return -1

    #-------------------------------------------------------------------
    def GetGroup( self, group_id ):
        "get group"

        if self._groups.has_key( group_id ):
            return self._groups[group_id]

        return 0

    #-------------------------------------------------------------------
    def SendGroupEvent( self, group_name, player_name, to_netid, event_filter ):
        "send a group related event"

        import Game

        event = Dispatcher.MakeEvent( gDispatcher, GroupManager.GROUP_EVENT_NAME )
        if event != 0:
            Event.EncodeString( event, group_name )
            Event.EncodeString( event, player_name )
            Event.SetFilter( event, event_filter )
            Event.AddTo( event, to_netid )
            Dispatcher.QueueEvent( gDispatcher, event )

    #-------------------------------------------------------------------
    def NotifyGroup( self, group_id, member_name, event_filter ):
        "alert group members about an event"
        import Game

        game = Dispatcher.GetGame(gDispatcher)

        # for each member, send a notification

        group = self.GetGroup( group_id )
        if group != 0:
            leader_obj = Game.GetPlayerByNetId( game, group._members[0] )
            if leader_obj != 0:
                group_name = GetSet.GetString( leader_obj, PlayerIds.CHARNAME )
                
                for player_netid in group._members:
                    self.SendGroupEvent( group_name, member_name, player_netid, event_filter )

    #-------------------------------------------------------------------
    def NotifyGroupMemberJoined( self, group_id, member_name):
        "alert group members when a player has joined the group"

        self.NotifyGroup( group_id, member_name, GroupManager.PLAYER_JOINED )

    #-------------------------------------------------------------------
    def NotifyGroupMemberLeft( self, group_id, member_netid ):
        "alert group members when a player leaves the group"

        import Game
        game        = Dispatcher.GetGame(gDispatcher)
        member_name = ""

        member_obj = Game.GetPlayerByNetId( game, member_netid )
        if member_obj != 0:
            member_name = GetSet.GetString( member_obj, PlayerIds.CHARNAME )
        
        self.NotifyGroup( group_id, member_name, GroupManager.PLAYER_LEFT )

    #-------------------------------------------------------------------
    def NotifyGroupNewLeader( self, group_id, member_netid ):
        "alert group members about a new leader"
        
        import Game
        game        = Dispatcher.GetGame(gDispatcher)
        member_name = ""

        member_obj = Game.GetPlayerByNetId( game, member_netid )
        if member_obj != 0:
            member_name = GetSet.GetString( member_obj, PlayerIds.CHARNAME )
        
        self.NotifyGroup( group_id, member_name, GroupManager.NEW_LEADER )

    #-------------------------------------------------------------------
    def RemovePlayer( self, player_netid ):
        "remove player from the groups"

        group_id = self.GetGroupID( player_netid )
        if group_id != -1:
            group = self.GetGroup( group_id )
            if group != 0:

                is_leader = group.IsLeader( player_netid )

                # remove the member
                if group.RemoveMember( player_netid ):
                    # print "removed player_netid=" + str(player_netid) + " from group_id=" + str(group_id)

                    # notify group that the player left
                    self.NotifyGroupMemberLeft( group_id, player_netid )
                   
                    # remove the group if there aren't enough members left
                    if group.NumMembers() < group.MIN_MEMBERS:
                        self.RemoveGroup( group_id )
                        # print "removed group_id=" + str(group_id)
                        
                    # otherwise, group still exists, notify the members    
                    else:

                        # if the group member was the one to leave, then we have a new group leader
                        # notify the group of the new leader
                        if is_leader:
                            self.NotifyGroupNewLeader( group_id, group._members[0] )

                    return True

        return False

    #-------------------------------------------------------------------
    def UpdateInfo( self, cur_time ):
        "send group info to all players "

        # if it's time to update
        if GroupManager.GROUP_UPDATE and cur_time - gGroupManager._last_update_timestamp > GroupManager.GROUP_UPDATE_INTERVAL:

            # update the timestamp
            gGroupManager._last_update_timestamp = cur_time

            import Game
            game = Dispatcher.GetGame(gDispatcher)

            # notify members of every group
            for group_id in gGroupManager._groups:

                # decide what information should be given
                # player's name, health

                player_name = ""
                energy      = 0 # health


                # for each member, create event with info about all members in the group
                group = self.GetGroup( group_id )
                if group != 0:

                    event = Dispatcher.MakeEvent( gDispatcher, GroupManager.GROUP_EVENT_NAME )
                    if event != 0:

                        Event.SetFilter( event, GroupManager.UPDATE_INFO )

                        # to be nice, we'll make the number of members as the first encoded value
                        Event.EncodeNumber( event, group.NumMembers() )
                    
                        for member_netid in group._members:

                            member_obj = Game.GetPlayerByNetId( game, member_netid )
                            if member_obj != 0:
                                
                                player_name = GetSet.GetString( member_obj, PlayerIds.CHARNAME )
                                energy      = GetSet.GetNumber( member_obj, PlayerIds.ENERGY )

                                Event.EncodeString( event, player_name )
                                Event.EncodeNumber( event, energy )

                                # while we're at it, let's add this member to the list of receipients
                                Event.AddTo( event, member_netid )
                
                        Dispatcher.QueueEvent( gDispatcher, event )
             

    #-------------------------------------------------------------------
    def ChangeLeader( self, group, member_netid ):
        "change leader of a group"

        if group != 0:

            if group.MakeLeader( member_netid ):

                # notify group members of new leader
                self.NotifyGroupNewLeader( id(group), member_netid )

    #-------------------------------------------------------------------
    def SendRequestDenied( self, player_netid ):
        "send a request denied"

        event = Dispatcher.MakeEvent( gDispatcher, GroupManager.GROUP_EVENT_NAME )
        if event != 0:
            Event.SetFilter( event, GroupManager.REQUEST_DENIED )
            Event.AddTo( event, player_netid )
            Dispatcher.QueueEvent( gDispatcher, event )
        
#-------------------------------------------------------------------
class Invitation(object):
    "invitation class"

    # class-level variables

    #-------------------------------------------------------------------
    def __init__(self, player_netid, invitee_netid, player_name, invitee_name):
        "constructor creates and initialized members"

        self._player_netid  = player_netid
        self._invitee_netid = invitee_netid
        self._player_name   = player_name
        self._invitee_name  = invitee_name
        self._timestamp     = time.time()

    #-------------------------------------------------------------------
    # methods
    #-------------------------------------------------------------------
    
    #-------------------------------------------------------------------
    def Contains( self, player_netid, invitee_netid ):
        "determines whether the given values match this invitation"

        return self._player_netid == player_netid and self._invitee_netid == invitee_netid

    #-------------------------------------------------------------------
    def RefreshTimestamp( self ):
        "refreshes the timestamp to current time"

        self._timestamp     = time.time()

    #-------------------------------------------------------------------
    def Expire( self ):
        "expire the invitation by clearing the timestamp"

        self._timestamp     = 0
        
#------------------------------------------------------------------- 
        
#-------------------------------------------------------------------
class InvitationManager(object):
    "Invitation manager class to manage group invitations"

    # class-level variables
    invitations = dict()

    # event filters
    PLAYER_IN_GROUP_FILTER      = 1
    PLAYER_NOT_AVAILABLE_FILTER = 2
    PLAYER_NOT_ACCEPTED         = 3
    INVITATION_CONFIRMATION     = 4

    INVITATION_NAME             = "InvitationEvent"
    invitation_event            = 0

    INVITATION_DECLINED_NAME    = "InvitationDeclinedEvent"
    INVITATION_TIMEOUT          = 10 # seconds

    #-------------------------------------------------------------------
    def __init__(self):
        "constructor creates and initialized members"

        pass
        
    
    #-------------------------------------------------------------------
    # static event handler methods
    #-------------------------------------------------------------------

    #-------------------------------------------------------------------
    # methods
    #-------------------------------------------------------------------

    #-------------------------------------------------------------------
    def SendInvitation( self, invitation_id, player_name, invitee_netid ):
        "send an invitation"

        event = Dispatcher.MakeEvent( gDispatcher, InvitationManager.INVITATION_NAME )
        if event != 0:
            Event.EncodeNumber( event, invitation_id )
            Event.EncodeString( event, player_name )
            Event.AddTo( event, invitee_netid )
            Dispatcher.QueueEvent( gDispatcher, event )

    #-------------------------------------------------------------------
    def SendInvitationConfirmation( self, invitee_name, player_netid ):
        "send an invitation confirmation"

        event = Dispatcher.MakeEvent( gDispatcher, InvitationManager.INVITATION_NAME )
        if event != 0:
            Event.EncodeString( event, invitee_name )
            Event.SetFilter( event, InvitationManager.INVITATION_CONFIRMATION )
            Event.AddTo( event, player_netid )
            Dispatcher.QueueEvent( gDispatcher, event )

    #-------------------------------------------------------------------
    def SendInvitationDeclined( self, invitation_id, player_name, invitee_name, to_netid, event_filter ):
        "send a declined invitation notification"

        event = Dispatcher.MakeEvent( gDispatcher, InvitationManager.INVITATION_DECLINED_NAME )
        if event != 0:
            Event.EncodeNumber( event, invitation_id )
            Event.EncodeString( event, player_name )
            Event.EncodeString( event, invitee_name )
            Event.SetFilter( event, event_filter )
            Event.AddTo( event, to_netid )
            Dispatcher.QueueEvent( gDispatcher, event )
    
    #-------------------------------------------------------------------
    def AddInvitation( self, player_netid, invitee_netid, player_name, invitee_name ):
        "add an invitation"
        invitation = Invitation(player_netid, invitee_netid, player_name, invitee_name)
        invitation_id = id(invitation)
        InvitationManager.invitations[invitation_id] = invitation

        return invitation_id

    #-------------------------------------------------------------------
    def RemoveInvitation( self, invitation_id ):
        "remove an invitation"

        if InvitationManager.invitations.has_key(invitation_id):
            del InvitationManager.invitations[invitation_id]

    #-------------------------------------------------------------------
    def GetInvitationID( self, player_netid, invitee_netid ):
        "return the invitation ID involving these players if it exists"

        for invitation_id in InvitationManager.invitations:
            
            if InvitationManager.invitations[invitation_id].Contains(player_netid, invitee_netid):
                return invitation_id
 
        return -1

    #-------------------------------------------------------------------
    def GetInvitation( self, invitation_id ):
        "return the invitation involving these players if it exists"

        if InvitationManager.invitations.has_key(invitation_id):
            return InvitationManager.invitations[invitation_id]
 
        return 0

    #-------------------------------------------------------------------
    def ClearExpiredInvitations( self, cur_time ):
        "clear outstanding invitations that have not been accepted before timeout"

        expired_invitations = list()

        for invitation_id in InvitationManager.invitations:

            if cur_time - InvitationManager.invitations[invitation_id]._timestamp > InvitationManager.INVITATION_TIMEOUT:
                expired_invitations.append(invitation_id)

        for invitation_id in expired_invitations:
            # invitation declined event is sent to both player and invitee (they each handle it differently)

            player_netid    = InvitationManager.invitations[invitation_id]._player_netid
            invitee_netid   = InvitationManager.invitations[invitation_id]._invitee_netid

            player_name     = InvitationManager.invitations[invitation_id]._player_name
            invitee_name    = InvitationManager.invitations[invitation_id]._invitee_name
  
            # to player
            gInvitationManager.SendInvitationDeclined( invitation_id, player_name, invitee_name, player_netid, gInvitationManager.PLAYER_NOT_ACCEPTED )

            # to invitee
            gInvitationManager.SendInvitationDeclined( invitation_id, player_name, invitee_name, invitee_netid, gInvitationManager.PLAYER_NOT_ACCEPTED )
            
            del InvitationManager.invitations[invitation_id]
            # print "expired invitation_id=" + str(invitation_id)

    #-------------------------------------------------------------------
    def ExpireInvitationsForPlayer( self, remove_netid ):
        "expire any invitations that involve the given player"

        for invitation_id in InvitationManager.invitations:
            
            if InvitationManager.invitations[invitation_id]._player_netid == remove_netid or InvitationManager.invitations[invitation_id]._invitee_netid == remove_netid:
                InvitationManager.invitations[invitation_id].Expire()

    #-------------------------------------------------------------------
    def ExpireInvitationsForInvitee( self, remove_netid ):
        "expire the invitations that involve the given player as the invitee"

        for invitation_id in InvitationManager.invitations:
            
            if InvitationManager.invitations[invitation_id]._invitee_netid == remove_netid:
                InvitationManager.invitations[invitation_id].Expire()

               

#-------------------------------------------------------------------       
gGroupManager       = GroupManager()
gInvitationManager  = InvitationManager()
gTimeManager        = TimeManager()
        
#-------------------------------------------------------------------
#
# Called from C to initialize all the handlers in this script
#
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    
    global gParent
    global gDispatcher

    gParent = handler
    gDispatcher = dispatcher

    # get the game API 
    try:
        callable( Game.GetPlayerByNetId )
    except NameError:
        if not Dispatcher.GetFunctions( gDispatcher, KEP.GET_SERVER_FN ):
            Dispatcher.LogMessage( dispatcher, KEP.WARN_LOG_LEVEL,  "Python GroupHandler couldn't load server Game functions!!" )
            return False
        import Game

        # one more check        
        try:
            callable(Game.GetPlayerByNetId)
        except NameError:
            return False

    ParentHandler.RegisterEventHandler( dispatcher, handler, TimeManager.handleTimerEvent,                      1,0,0,0, TimeManager.TIMER_NAME, KEP.HIGH_PRIO )
    ParentHandler.RegisterEventHandler( dispatcher, handler, GroupManager.handleDisconnectedEvent,              1,0,0,0, "DisconnectedEvent", KEP.HIGH_PRIO )
        
    # filtered events

    ParentHandler.RegisterFilteredEventHandler( dispatcher, handler, GroupManager.handleInvitePlayerEvent,      1,0,0,0, "TextEvent", CMD.INVITE, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.MED_PRIO + 1)
    ParentHandler.RegisterFilteredEventHandler( dispatcher, handler, GroupManager.handleJoinGroupEvent,         1,0,0,0, "TextEvent", CMD.JOIN, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.MED_PRIO + 1)
    ParentHandler.RegisterFilteredEventHandler( dispatcher, handler, GroupManager.handleLeaveGroupEvent,        1,0,0,0, "TextEvent", CMD.LEAVE, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.MED_PRIO + 1)
    ParentHandler.RegisterFilteredEventHandler( dispatcher, handler, GroupManager.handleRemovePlayerGroupEvent, 1,0,0,0, "TextEvent", CMD.REMOVE, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.MED_PRIO + 1)
    ParentHandler.RegisterFilteredEventHandler( dispatcher, handler, GroupManager.handleChangeLeaderGroupEvent, 1,0,0,0, "TextEvent", CMD.LEADER, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.MED_PRIO + 1)
    ParentHandler.RegisterFilteredEventHandler( dispatcher, handler, GroupManager.handleGroupChatEvent,         1,0,0,0, "TextEvent", 0, KEP.NO_FILTER, KEP.NO_OBJID_FILTER, KEP.MED_PRIO + 1) 
    
    return True


#-------------------------------------------------------------------
#
# Called from C to initialize all the events
#
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):

    global gParent
    global gDispatcher

    import gc
    gc.set_debug( gc.DEBUG_LEAK )

    # call the c funtion to register our events
    gParent = parent
    gDispatcher = dispatcher
    KEP.debugLevel = dbgLevel

    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, InvitationManager.INVITATION_NAME, KEP.MED_PRIO )
    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, TimeManager.TIMER_NAME, KEP.HIGH_PRIO )
    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, InvitationManager.INVITATION_DECLINED_NAME, KEP.MED_PRIO )
    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, GroupManager.GROUP_EVENT_NAME, KEP.MED_PRIO )

    # create a new event
    timer_event = Dispatcher.MakeEvent( gDispatcher, TimeManager.TIMER_NAME )
    if timer_event != 0:
        Event.SetRecurrence( timer_event, TimeManager.TIMER_INTERVAL )
        Dispatcher.QueueEvent( gDispatcher, timer_event )
    

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    " called from C to give s a chance to cleanup before shutdown or re-init"
    
    ret = KEP.getDependentModules(__name__)

    return ret

        


