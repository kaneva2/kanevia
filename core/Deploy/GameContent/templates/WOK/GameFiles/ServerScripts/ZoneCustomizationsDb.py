import ServerGlobals
import KEP
import time
import DatabaseInterface

gDispatcher = 0
gParent = 0
gDBAccessor = None

def logMsg( level, msg ):
    #needed for logging
    import ParentHandler
    ParentHandler.LogMessage( gParent, level, msg )
    #print msg



dynamicObjectsSQL = "SELECT `obj_placement_id` as l, `object_id` as o, `texture_asset_id` as t, `player_id` as p, `friend_id` as f, " + \
		    " `position_x` as x, `position_y` as y , `position_z` as z, " + \
		    " `rotation_x` as rx, `rotation_y` as ry, `rotation_z` as rz, " + \
		    " `texture_url` as u, " + \
		    " swf_name as w, " + \
		    " `swf_parameter` as wp, " + \
		    " `global_id` as g " + \
		    " FROM " + ServerGlobals.DB_Name + ".`dynamic_objects` do" + \
		    " WHERE `zone_index` = @zoneIndex " + \
		    " AND `zone_instance_id` = @instanceId " + \
                    " AND (do.expired_date IS NULL OR do.expired_date > NOW())"
                    

worldObjectsSQL =   " SELECT w.world_object_id as o, w.asset_id as a, w.texture_url as u" + \
		    " FROM " + ServerGlobals.DB_Name + ".world_object_player_settings w " + \
		    " WHERE w.zone_instance_id = @instanceId AND w.zone_index = @zoneIndex AND w.asset_id > 0 ";



#-------------------------------------------------------------------

def getDynamicObjects( zoneIndex, instanceId, rowList ):
    "get the ALL playlist given the player_id (PlayerIds.HANDLE)"

    logMsg( KEP.DEBUG_LOG_LEVEL, "getDynamicObjects")

    sql = dynamicObjectsSQL.replace( "@zoneIndex", str(zoneIndex) )
    sql = sql.replace( "@instanceId", str(instanceId) )

    gDBAccessor.fetchRows( sql, rowList )    

#-------------------------------------------------------------------

def getWorldObjects( zoneIndex, instanceId, rowList ):
    "get the ALL playlist given the player_id (PlayerIds.HANDLE)"

    logMsg( KEP.DEBUG_LOG_LEVEL, "getWorldObjects")
   
    sql = worldObjectsSQL.replace( "@zoneIndex", str(zoneIndex) )
    sql = sql.replace( "@instanceId", str(instanceId) )
    
    gDBAccessor.fetchRows( sql, rowList )    

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    global gParent
    global gDispatcher
    global gDBAccessor

    # grab instances for debugging
    gParent = parent
    gDispatcher = dispatcher
    KEP.debugLevel = dbgLevel

    gDBAccessor = DatabaseInterface.DBAccess("ZoneCustomizationsDb")

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Manage resources for blade reload"""

    return KEP.getDependentModules(__name__)

    
#-------------------------------------------------------------------
# Self test
"""
if __name__ == "__main__":

    if False:
        dynobjlist = list()
        wolist = list()

        getDynamicObjects(805306376, 12271, dynobjlist)
        getWorldObjects(805306376, 12271, dynobjlist)
        
        print "/////////////////////////////////////////////"
        print "Got DynObj total of "+str(len(dynobjlist))
        for r in dynobjlist:
            print "---------------------------------------------"
            print "    OBJ_PLACEMENT_ID_DO " + str(r[KEP.OBJ_PLACEMENT_ID_DO])
            print "    OBJ_ID_DO  " + str(r[KEP.OBJ_ID_DO ]) 
            print "    TEX_ASSET_ID_DO  " + str(r[KEP.TEX_ASSET_ID_DO ])
            print "    PLAYER_ID_DO  " + str(r[KEP.PLAYER_ID_DO ])
            print "    FRIEND_ID_DO  " + str(r[KEP.FRIEND_ID_DO ])
            print "    POS_X_DO  " + str(r[KEP.POS_X_DO] )
            print "    POS_Y_DO  " + str(r[KEP.POS_Y_DO ])
            print "    POS_Z_DO  " + str(r[KEP.POS_Z_DO ])
            print "    ROT_X_DO  " + str(r[KEP.ROT_X_DO ])
            print "    ROT_Y_DO  " + str(r[KEP.ROT_Y_DO ])
            print "    ROT_Z_DO  " + str(r[KEP.ROT_Z_DO ])
            print "    SWF_NAME_DO  " + str(r[KEP.SWF_NAME_DO ])
            print "    SWF_PARAM_DO  " + str(r[KEP.SWF_PARAM_DO ])
            print "---------------------------------------------"
        print "/////////////////////////////////////////////"
        print "Got WorldObj total of "+str(len(wolist))
        for r in wolist:
            print "---------------------------------------------"
            print "    WORLD_OBJ_ID_WO " + str(r[KEP.WORLD_OBJ_ID_WO])
            print "    ASSET_ID_WO  " + str(r[KEP.ASSET_ID_WO ]) 
            print "    TEX_URL_WO  " + str(r[KEP.TEX_URL_WO ])
            print "---------------------------------------------"
"""        

        
