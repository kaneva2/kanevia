### Costume Configs ###
###
class CostumeObject(object):

    def __init__(self, glidList, defaultList):

        self.__numPieces = len(glidList)
        self.__glids = glidList
        self.__mainItem = glidList[0]
        self.__subItems = glidList[1:]
        self.__defaultItems = defaultList

    def getNumItems(self):

        return len(self.__glids)

    def getGlids(self):

        return self.__glids

    def getMainItem(self):

        return self.__mainItem

    def getSubItems(self):

        return self.__subItems

    def getDefaultItems(self):

        return self.__defaultItems

    def isSubItem(self, glid):

        if self.__subItems.count(glid) > 0:
            return True

        return False

    def hasGlid(self, glid):

        if self.__glids.count(glid) > 0:
            return True

        return False

#-------------------------------------------------------------------

Costumes = []

def isCostumePiece( glid ):
    """
    Search for costume and return reference if found, return None otherwise

    """

    for costume in Costumes:
        if costume.hasGlid(glid):
            return costume

    return None



### ADD COSTUMES ###
### NOTE:  PLEASE LIST ITEMS IN ORDER OF THEIR LOCATION ID
###        MAIN ITEM SHOULD ALWAYS BE SHIRT HENCE FIRST
###
### In Client C++ a glid of -1 will cause an instant return
EMPTY = -1    
SHIRT = 0
PANTS = 1
SHOES = 2

#default clothing items
Male_Defaults = [1172, 1173, 1174]

Female_Defaults = [1247, 1248, 1249]

### Costume GLIDS
#Male 
Vegas = [1478, 1479, EMPTY]
Pimped_Out = [2123, 2124, 2125]
Toga = [2218, 2219, EMPTY]
Vegas_Devil = [2224, 2225, EMPTY]
Pirate_Male = [2247, 2248, 2249]
Gorilla = [2250, 2251, 2252]
Warrior = [2364, 2365, EMPTY]
Santa = [2366, 2367, 2368]
Elf_Male = [2403, 2404, 2405]
Elf_Pimp_Male = [2406, 2407, 2408]

#Female
Pirate_Female = [1639, 1640, 1641]
Feline = [1683, 1684, 1685]
Senorita = [1884, 1885, 1886]
Light_Gray_Satin_Trim_Dress = [1607, 1608, EMPTY]
Black_Turq_Satin_Trim_Dress = [1609, 1610, EMPTY]
Black_Red_Satin_Trim_Dress = [1611, 1612, EMPTY]
Black_Pink_Satin_Trim_Dress = [1613,1614, EMPTY]
Red_Halter_Satin_Trim_Dress = [1603, 1604, EMPTY]
Black_Halter_Satin_Trim_Dress = [1605, 1606, EMPTY]
Red_Oring_Club_Dress = [1887, 1888, EMPTY]
Black_Oring_Club_Dress = [1889, 1890, EMPTY]
White_Bust_Garter_Gstring = [1891, 1892, 1893]
Red_Bust_Garter_Gstring = [1894, 1895, 1896]
Pink_Bust_Garter_Gstring = [1897, 1898, 1899]
Black_Bust_Garter_Gstring = [1900, 1901, 1902]
Grey_Plaid_Mini_Dress = [1903, 1904, EMPTY]
Red_Plaid_Mini_Dress = [2014, 2015, EMPTY]
Green_Plaid_Mini_Dress = [2016, 2017, EMPTY]
Dark_Grey_Plaid_Mini_Dress = [2018, 2019, EMPTY]
Blue_Plaid_Mini_Dress = [2020, 2021, EMPTY]
Purple_Plaid_Mini_Dress = [2022, 2023, EMPTY]
Black_Leather_Strapped_Club_Dress = [2037, 2038, EMPTY]
Purple_Pixie = [2120, 2121, 2122]
Lady_Law = [2220, 2221, 2222]
Fairy_Princess = [2226, 2227, 2228]
Wicked_Witch = [2230, 2231, 2232]
Belly_Dancer = [2236, 2237, 2238]
French_Maid = [2233, 2234, 2235]
Red_Skull_Minidress = [2291, 2292, EMPTY]
Blk_Wht_Lace_Minidress = [2293, 2294, EMPTY]
Drk_Grn_Wht_Stripe_MiniDress = [2295, 2296, EMPTY]
Blk_Red_Stripe_Minidress = [2297, 2298, EMPTY]
Maroon_Plaid_Minidress = [2299, 2300, EMPTY]
Blk_Blu_Stripe_Minidress = [2301, 2302, EMPTY]
Red_Diagonal_Dress = [2308, 2309, EMPTY]
Mrs_Claus = [2409, 2410, EMPTY]
Grn_Sexy_Elf = [2411, 2412, 2413]
Grn_Sexy_Snowflake_Elf = [2414, 2415, 2416]
Red_Sexy_Elf = [2417, 2418, 2419]
Red_Sexy_Snowflake_Elf = [2420, 2421, 2422]
Wedding_Gown = [2535, 2536, EMPTY]
Wedding_Sashed_Gown = [2537, 2538, EMPTY]
Green_Bridesmaids_Dress = [2539, 2540, EMPTY]
Black_Bridesmaids_Dress = [2541, 2542, EMPTY]
Sky_Blue_Bridesmaids_Dress = [2543, 2544, EMPTY]
White_Bridesmaids_Dress = [2545, 2546, EMPTY]
Dark_Red_Bridesmaids_Dress = [2547, 2548, EMPTY]
Brown_Bridesmaids_Dress = [2549, 2550, EMPTY]
Red_Bridesmaids_Dress = [2551, 2552, EMPTY]
Strapless_Wedding_Gown = [2553, 2554, EMPTY]
Black_Strapless_Long_Gown = [2555, 2556, EMPTY]
Gray_Strapless_Long_Gown = [2557, 2558, EMPTY]
Brown_Strapless_Long_Gown = [2559, 2560, EMPTY]
Blue_Strapless_Long_Gown = [2561, 2562, EMPTY]






### Add to costume list
#Male
Costumes.append(CostumeObject(Vegas, Male_Defaults))
Costumes.append(CostumeObject(Pimped_Out, Male_Defaults))
Costumes.append(CostumeObject(Toga, Male_Defaults))
Costumes.append(CostumeObject(Vegas_Devil, Male_Defaults))
Costumes.append(CostumeObject(Pirate_Male, Male_Defaults))
Costumes.append(CostumeObject(Gorilla, Male_Defaults))
Costumes.append(CostumeObject(Warrior, Male_Defaults))
Costumes.append(CostumeObject(Santa, Male_Defaults))
Costumes.append(CostumeObject(Elf_Male, Male_Defaults))
Costumes.append(CostumeObject(Elf_Pimp_Male, Male_Defaults))

#Female
Costumes.append(CostumeObject(Pirate_Female, Female_Defaults))
Costumes.append(CostumeObject(Feline, Female_Defaults))
Costumes.append(CostumeObject(Senorita, Female_Defaults))
Costumes.append(CostumeObject(Light_Gray_Satin_Trim_Dress, Female_Defaults))
Costumes.append(CostumeObject(Black_Turq_Satin_Trim_Dress, Female_Defaults))
Costumes.append(CostumeObject(Black_Red_Satin_Trim_Dress, Female_Defaults))
Costumes.append(CostumeObject(Black_Pink_Satin_Trim_Dress, Female_Defaults))
Costumes.append(CostumeObject(Red_Halter_Satin_Trim_Dress, Female_Defaults))
Costumes.append(CostumeObject(Black_Halter_Satin_Trim_Dress, Female_Defaults))
Costumes.append(CostumeObject(Red_Oring_Club_Dress, Female_Defaults))
Costumes.append(CostumeObject(Black_Oring_Club_Dress, Female_Defaults))
Costumes.append(CostumeObject(White_Bust_Garter_Gstring, Female_Defaults))
Costumes.append(CostumeObject(Red_Bust_Garter_Gstring, Female_Defaults))
Costumes.append(CostumeObject(Pink_Bust_Garter_Gstring, Female_Defaults))
Costumes.append(CostumeObject(Black_Bust_Garter_Gstring, Female_Defaults))
Costumes.append(CostumeObject(Grey_Plaid_Mini_Dress, Female_Defaults))
Costumes.append(CostumeObject(Red_Plaid_Mini_Dress, Female_Defaults))
Costumes.append(CostumeObject(Green_Plaid_Mini_Dress, Female_Defaults))
Costumes.append(CostumeObject(Dark_Grey_Plaid_Mini_Dress, Female_Defaults))
Costumes.append(CostumeObject(Blue_Plaid_Mini_Dress, Female_Defaults))
Costumes.append(CostumeObject(Purple_Plaid_Mini_Dress, Female_Defaults))
Costumes.append(CostumeObject(Black_Leather_Strapped_Club_Dress, Female_Defaults))
Costumes.append(CostumeObject(Purple_Pixie, Female_Defaults))
Costumes.append(CostumeObject(Lady_Law, Female_Defaults))
Costumes.append(CostumeObject(Fairy_Princess, Female_Defaults))
Costumes.append(CostumeObject(Wicked_Witch, Female_Defaults))
Costumes.append(CostumeObject(Belly_Dancer, Female_Defaults))
Costumes.append(CostumeObject(French_Maid, Female_Defaults))
Costumes.append(CostumeObject(Red_Skull_Minidress, Female_Defaults))
Costumes.append(CostumeObject(Blk_Wht_Lace_Minidress, Female_Defaults))
Costumes.append(CostumeObject(Drk_Grn_Wht_Stripe_MiniDress, Female_Defaults))
Costumes.append(CostumeObject(Blk_Red_Stripe_Minidress, Female_Defaults))
Costumes.append(CostumeObject(Maroon_Plaid_Minidress, Female_Defaults))
Costumes.append(CostumeObject(Blk_Blu_Stripe_Minidress, Female_Defaults))
Costumes.append(CostumeObject(Red_Diagonal_Dress, Female_Defaults))
Costumes.append(CostumeObject(Mrs_Claus, Female_Defaults))
Costumes.append(CostumeObject(Grn_Sexy_Elf, Female_Defaults))
Costumes.append(CostumeObject(Grn_Sexy_Snowflake_Elf, Female_Defaults))
Costumes.append(CostumeObject(Red_Sexy_Elf, Female_Defaults))
Costumes.append(CostumeObject(Red_Sexy_Snowflake_Elf, Female_Defaults))
Costumes.append(CostumeObject(Wedding_Gown, Female_Defaults))
Costumes.append(CostumeObject(Wedding_Sashed_Gown, Female_Defaults))
Costumes.append(CostumeObject(Green_Bridesmaids_Dress, Female_Defaults))
Costumes.append(CostumeObject(Black_Bridesmaids_Dress, Female_Defaults))
Costumes.append(CostumeObject(Sky_Blue_Bridesmaids_Dress, Female_Defaults))
Costumes.append(CostumeObject(White_Bridesmaids_Dress, Female_Defaults))
Costumes.append(CostumeObject(Dark_Red_Bridesmaids_Dress, Female_Defaults))
Costumes.append(CostumeObject(Brown_Bridesmaids_Dress, Female_Defaults))
Costumes.append(CostumeObject(Red_Bridesmaids_Dress, Female_Defaults))
Costumes.append(CostumeObject(Strapless_Wedding_Gown, Female_Defaults))
Costumes.append(CostumeObject(Black_Strapless_Long_Gown, Female_Defaults))
Costumes.append(CostumeObject(Gray_Strapless_Long_Gown, Female_Defaults))
Costumes.append(CostumeObject(Brown_Strapless_Long_Gown, Female_Defaults))
Costumes.append(CostumeObject(Blue_Strapless_Long_Gown, Female_Defaults))

