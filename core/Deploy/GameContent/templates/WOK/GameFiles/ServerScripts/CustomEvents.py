
#import helper Python code
import KEP
import CMD

import pdb

#get set ids
import PlayerIds
import ServerItemIds

#import the C methods from the exe
import ParentHandler
import Dispatcher
import Event
import GetSet
import InstanceId

#-------------------------------------------------------------------
#
# Called from C to initialize all the handlers in this script
#
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):

    KEP.parent = handler
    KEP.dispatcher = dispatcher
    KEP.debugLevel = dbgLevel

    try:
        callable( Game.GetPlayerByNetId )
    except NameError:
        Dispatcher.GetFunctions( KEP.dispatcher, KEP.GET_SERVER_FN )
        import Game
        
    # call the c funtion to register our event handlers, we have just one
    ParentHandler.RegisterEventHandler(  dispatcher, handler, RequestArenaList, 1,0,0,0, "RequestArenaList", KEP.MED_PRIO )
 
    return True

#-------------------------------------------------------------------
#
# handle a trigger event
#
#-------------------------------------------------------------------
def RequestArenaList(dispatcher, fromNetid, event, eventid, filter, objectid):

    import Game
    game = Dispatcher.GetGame( dispatcher )
    
    # get the player and info since we usually need it
    player = Game.GetPlayerByNetId( game, fromNetid )
   
    if  player == 0: 
        # TODO: log message
        return KEP.EPR_OK

    Game.RequestArenaList( game, player, 0 )
    
    return KEP.EPR_CONSUMED
    
#-------------------------------------------------------------------
#
# Called from C to initialize all the events
#
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    KEP.dispatcher = dispatcher
    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "RequestArenaList", KEP.MED_PRIO )
