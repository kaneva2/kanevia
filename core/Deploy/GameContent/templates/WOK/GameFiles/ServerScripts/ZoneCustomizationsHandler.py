#import helper Python code
import KEP
import xml.dom.minidom

# C functions
import InstanceId
import Dispatcher
import Event
import GetSet
import ParentHandler

import ZoneCustomizationsDb
import WorkerThread

CHUNK_SIZE = 25 ## Number of objects to send at once

gDispatcher = 0
gParent = 0
gBackgroundThread = None
gRootTag = None

def logMsg( level, msg ):
    #needed for logging
    import ParentHandler
    ParentHandler.LogMessage( gParent, level, msg )
    #print msg


#-------------------------------------------------------------------
def DynObjRequestHandler(dispatcher, fromNetId, event, eventid, filter, objectid):
    "Get the dynamic objects for the user -- event filter is the zoneIndex"
    import Game

    zoneIndex = filter
    instanceId = Event.DecodeNumber( event )

    zoneType = InstanceId.GetType(zoneIndex)

    if zoneType == KEP.CI_PERMANENT  or zoneType == KEP.CI_ARENA:
        instanceId = 1
        if zoneType == KEP.CI_ARENA:
            zoneIndex = InstanceId.MakeId(InstanceId.GetId(zoneIndex), 0, InstanceId.GetType(zoneIndex)) #TEMP

    logMsg(KEP.DEBUG_LOG_LEVEL, "Request for dynamic objects received from: "+str(event)+"\n In zoneIndex: "+str(filter)+" instanceId: "+str(instanceId) )

    gBackgroundThread.performWork(threadDynObjRequest, fromNetId, zoneIndex, instanceId)

    return KEP.EPR_CONSUMED 

#-------------------------------------------------------------------
def threadDynObjRequest(fromNetId, zoneIndex, instanceId):

    dynObjList = list()
    worldObjList = list()

    retrieveZoneCustomizations( zoneIndex, instanceId, dynObjList, worldObjList )

    logMsg(KEP.TRACE_LOG_LEVEL, str(dynObjList) )
    logMsg(KEP.TRACE_LOG_LEVEL, str(worldObjList) )

    splitAndSend(fromNetId, dynObjList, worldObjList)

##    finalXML = generateZoneXML( dynObjList, worldObjList )
##
##    logMsg( KEP.TRACE_LOG_LEVEL, finalXML )
##
##    status = sendXMLToClient( fromNetId, finalXML )

##    if status == False:
##        logMsg( KEP.DEBUG_LOG_LEVEL, "Reply event failed to send to client" )

#-------------------------------------------------------------------
def splitAndSend(fromNetId, dynObjList, worldObjList):
    """ Split up the xml into chunks to stay under the DPlay packet size limit """
    global gRootTag

    dynObjChunk = list()
    worldObjChunk = list()

    dynObjCount = len(dynObjList)
    worldObjCount = len(worldObjList)

    doc = None
    
    doc = initXMLDoc( doc )

    #print "roottag: "+str(gRootTag)
    #print "dynObjCount: "+str(dynObjCount)+"\nworldObjCount: "+str(worldObjCount)+"\n"

    ##
    ## Dynamic Objects
    ##
    start = 0
    end = CHUNK_SIZE

    if dynObjCount <= CHUNK_SIZE:
        generateDynObjXML( doc, gRootTag, dynObjList )
        sendXMLToClient( fromNetId, doc.toxml() )        
    else:
        while dynObjCount >= CHUNK_SIZE:
            dynObjChunk = dynObjList[start:end]
            #print "start count: "+str(start)+" end count: "+str(end)+"\n"
            generateDynObjXML( doc, gRootTag, dynObjChunk )
            #print doc.toxml()
            sendXMLToClient( fromNetId, doc.toxml() )
            del dynObjChunk
            del doc
            doc = None
            doc = initXMLDoc( doc )
            dynObjCount -= CHUNK_SIZE
            start = end
            end = end + CHUNK_SIZE

        #Send remainder of list
        if dynObjCount > 0:
            #print "Final batch: "+str(len(dynObjList[start:]))
            generateDynObjXML( doc, gRootTag, dynObjList[start:] )
            sendXMLToClient( fromNetId, doc.toxml() )       
            del doc
            doc = None
            

    ##
    ## World Objects
    ##
    start = 0
    end = CHUNK_SIZE 

    doc = initXMLDoc( doc )

    if worldObjCount <= CHUNK_SIZE:
        generateWorldObjXML( doc, gRootTag, worldObjList )
        sendXMLToClient( fromNetId, doc.toxml() )        
    else:
        while worldObjCount >= CHUNK_SIZE:
            worldObjChunk = worldObjList[start:end]
            #print "WO start count: "+str(start)+" end count: "+str(end)+"\n"
            generateWorldObjXML( doc, gRootTag, worldObjChunk )
            sendXMLToClient( fromNetId, doc.toxml() )
            del worldObjChunk
            del doc
            doc = None
            doc = initXMLDoc( doc )
            worldObjCount -= CHUNK_SIZE
            start = end
            end = end + CHUNK_SIZE
            
        #Send remainder of list
        if worldObjCount > 0:
            #print "Final batch: "+str(len(worldObjList[start:]))
            generateWorldObjXML( doc, gRootTag, worldObjList )
            sendXMLToClient( fromNetId, doc.toxml() )     
            del doc
            doc = None


#-------------------------------------------------------------------
def initXMLDoc( doc ):
    global gRootTag

    returnXML = "<Result></Result>"

    doc = xml.dom.minidom.parseString(returnXML)

    resultTags = doc.getElementsByTagName("Result")

    #initizalized above, so safe to access 0
    gRootTag = resultTags[0]    

    return doc

#-------------------------------------------------------------------
def retrieveZoneCustomizations( zoneIndex, instanceId, dynObjList, worldObjList ):
    "Get the dynamic objects from the database for this particular zone"

    zoneCustomizations = list()

    ZoneCustomizationsDb.getDynamicObjects( zoneIndex, instanceId, dynObjList )
    ZoneCustomizationsDb.getWorldObjects( zoneIndex, instanceId, worldObjList )

    pass


#-------------------------------------------------------------------
def generateZoneXML( dynObjList, worldObjList ):

    returnXML = "<Result></Result>"

    doc = xml.dom.minidom.parseString(returnXML)

    resultTags = doc.getElementsByTagName("Result")

    #initizalized above, so safe to access 0
    gRootTag = resultTags[0]

    generateDynObjXML( doc, gRootTag, dynObjList )

    logMsg( KEP.DEBUG_LOG_LEVEL, "The number of dyn obj's is "+str(len(dynObjList)) )

    generateWorldObjXML( doc, gRootTag, worldObjList )

    logMsg( KEP.DEBUG_LOG_LEVEL, "The number of world obj's is "+str(len(worldObjList)) )

    returnXML = doc.toxml()

    return returnXML



#-------------------------------------------------------------------
def generateDynObjXML( doc, rootTag, dynObjList ):

    #print "\nDyno list length: "+str(len(dynObjList))

    for dyno in dynObjList:

        logMsg( KEP.TRACE_LOG_LEVEL, str(dyno) )
        
        Dtag = doc.createElement("D")
        
        l_tag = doc.createElement("l")
        l_text = doc.createTextNode(str(dyno[KEP.OBJ_PLACEMENT_ID_DO]))
        l_tag.appendChild(l_text)
        Dtag.appendChild(l_tag)
        
        o_tag = doc.createElement("o")
        o_text = doc.createTextNode(str(dyno[KEP.OBJ_ID_DO]))
        o_tag.appendChild(o_text)
        Dtag.appendChild(o_tag)
        
        t_tag = doc.createElement("t")
        t_text = doc.createTextNode(str(dyno[KEP.TEX_ASSET_ID_DO]))
        t_tag.appendChild(t_text)
        Dtag.appendChild(t_tag)
        
        p_tag = doc.createElement("p")
        p_text = doc.createTextNode(str(dyno[KEP.PLAYER_ID_DO]))
        p_tag.appendChild(p_text)
        Dtag.appendChild(p_tag)
        
        f_tag = doc.createElement("f")
        f_text = doc.createTextNode(str(dyno[KEP.FRIEND_ID_DO]))
        f_tag.appendChild(f_text)
        Dtag.appendChild(f_tag)
        
        x_tag = doc.createElement("x")
        x_text = doc.createTextNode(str(dyno[KEP.POS_X_DO]))
        x_tag.appendChild(x_text)
        Dtag.appendChild(x_tag)
        
        y_tag = doc.createElement("y")
        y_text = doc.createTextNode(str(dyno[KEP.POS_Y_DO]))
        y_tag.appendChild(y_text)
        Dtag.appendChild(y_tag)
        
        z_tag = doc.createElement("z")
        z_text = doc.createTextNode(str(dyno[KEP.POS_Z_DO]))
        z_tag.appendChild(z_text)
        Dtag.appendChild(z_tag)
        
        rx_tag = doc.createElement("rx")
        rx_text = doc.createTextNode(str(dyno[KEP.ROT_X_DO]))
        rx_tag.appendChild(rx_text)
        Dtag.appendChild(rx_tag)
        
        ry_tag = doc.createElement("ry")
        ry_text = doc.createTextNode(str(dyno[KEP.ROT_Y_DO]))
        ry_tag.appendChild(ry_text)
        Dtag.appendChild(ry_tag)
        
        rz_tag = doc.createElement("rz")
        rz_text = doc.createTextNode(str(dyno[KEP.ROT_Z_DO]))
        rz_tag.appendChild(rz_text)
        Dtag.appendChild(rz_tag)
        
        u_tag = doc.createElement("u")
        u_text = doc.createTextNode(str(dyno[KEP.TEX_URL_DO]))
        u_tag.appendChild(u_text)
        Dtag.appendChild(u_tag)
        
        w_tag = doc.createElement("w")
        w_text = doc.createTextNode(str(dyno[KEP.SWF_NAME_DO]))
        w_tag.appendChild(w_text)
        Dtag.appendChild(w_tag)
        
        wp_tag = doc.createElement("wp")
        wp_text = doc.createTextNode(str(dyno[KEP.SWF_PARAM_DO]))
        wp_tag.appendChild(wp_text)
        Dtag.appendChild(wp_tag)

        g_tag = doc.createElement("g")
        g_text = doc.createTextNode(str(dyno[KEP.GLID_DO]))
        g_tag.appendChild(g_text)
        Dtag.appendChild(g_tag)
        
        rootTag.appendChild(Dtag)



#-------------------------------------------------------------------
def generateWorldObjXML( doc, rootTag, worldObjList ):


    for wobj in worldObjList:

        logMsg( KEP.TRACE_LOG_LEVEL, str(wobj) )
        
        Wtag = doc.createElement("W")
        
        o_tag = doc.createElement("o")
        o_text = doc.createTextNode(str(wobj[KEP.WORLD_OBJ_ID_WO]))
        o_tag.appendChild(o_text)
        Wtag.appendChild(o_tag)

        a_tag = doc.createElement("a")
        a_text = doc.createTextNode(str(wobj[KEP.ASSET_ID_WO]))
        a_tag.appendChild(a_text)
        Wtag.appendChild(a_tag)

        u_tag = doc.createElement("u")
        u_text = doc.createTextNode(str(wobj[KEP.TEX_URL_WO]))
        u_tag.appendChild(u_text)
        Wtag.appendChild(u_tag)
        
        rootTag.appendChild(Wtag)    



#-------------------------------------------------------------------
def sendXMLToClient( destination, xml ):

    ret = False

    e = Dispatcher.MakeEvent( gDispatcher, KEP.SERVERDYNOBJ_REPLY_EVENT_NAME )
    #print "destination: "+str(destination)+"\n"
    #print "e: "+str(e)+"\n"
    #print "gDispatcher: "+str(gDispatcher)+"\n"
    if e != 0:
        
        ret = True
        Event.EncodeString(e, xml)
        Event.AddTo(e, destination)
        Dispatcher.QueueEvent( gDispatcher, e )


    return True

#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, parent, dbgLevel ):

    global gParent
    global gDispatcher
    global gBackgroundThread

    gParent = parent
    gDispatcher = dispatcher


    ParentHandler.RegisterEventHandler( dispatcher, parent,  DynObjRequestHandler, 1,0,0,0, KEP.SERVERDYNOBJ_REQUEST_EVENT_NAME, KEP.HIGH_PRIO )

    gBackgroundThread = WorkerThread.WorkerThread("ZoneCustomizations")
    ParentHandler.LogMessage(parent, KEP.INFO_LOG_LEVEL, str(gBackgroundThread) + " tid: "+str(gBackgroundThread._get_my_tid()))

    return  True 


#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):

    global gParent
    global gDispatcher

    # grab instances for debugging
    gParent = parent
    gDispatcher = dispatcher
    KEP.debugLevel = dbgLevel

    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, KEP.SERVERDYNOBJ_REQUEST_EVENT_NAME, KEP.MED_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, KEP.SERVERDYNOBJ_REPLY_EVENT_NAME, KEP.MED_PRIO )

    


#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    " called from C to give us a chance to cleanup before shutdown or re-init"
    return KEP.getDependentModules(__name__)
        
