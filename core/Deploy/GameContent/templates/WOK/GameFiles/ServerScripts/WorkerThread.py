#thread modules
import threading
import Queue

#import the C methods from the exe
import ParentHandler
import Dispatcher

import KEP

#url module
import urllib2

#global vars
gParent = None 
gDispatcher = None

QUEUE_THRESHOLD = 1000


#-------------------------------------------------------------------
# Class WorkerThread
#-------------------------------------------------------------------
class WorkerThread(threading.Thread):
    """Worker thread to process data"""

    requestID = 0
    initTID = True
    active = True
    stopped = False

    #-------------------------------------------------------------------
    def __init__(self, threadName, queueThreshold = QUEUE_THRESHOLD, **kwds):
        """Worker Constructor"""

        threading.Thread.__init__(self, **kwds)
        self.setDaemon(1)
        self.workRequestQueue = Queue.Queue()
        self.resultQueue = Queue.Queue()
        self.setName(threadName)
        self.queueThreshold = queueThreshold
        self.start()
        ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, self.getName()+" worker thread - initialized -  work queue: "+str(self.workRequestQueue)+" result queue: "+str(self.resultQueue))

    #-------------------------------------------------------------------
    ### TODO: Need to find out if this is returning an OS thread handle or just a python handle        
    def _get_my_tid(self):
        """determines this (self's) thread id"""        

        if not self.isAlive():
            ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, self.getName()+" worker thread - NOT ACTIVE.  Could not retrieve thread id.")
            return 0

        # do we have it cached?
        if hasattr(self, "_thread_id"):
            return self._thread_id

        # no, look for it in the _active dict
        for tid, tobj in threading._active.items():
            if tobj is self:
                self._thread_id = tid
                return tid

        return 0

    #-------------------------------------------------------------------
    def performWork(self, entryPoint, *args, **kwds):
        """Called by main thread to queue work"""
        global QUEUE_THRESHOLD
        global gDispatcher

        self.requestID += 1
        self.workRequestQueue.put((self.requestID, entryPoint, args, kwds))

        if self.workRequestQueue.qsize() > self.queueThreshold:
            Dispatcher.LogAlertMessage( gDispatcher, str(self.getName())+" queue has exceeded threshold of "+str(self.queueThreshold)+", current size: "+str(self.workRequestQueue.qsize()) )

        return self.requestID

    #-------------------------------------------------------------------
    def stop(self):
        """Called by main thread to stop thread"""
        
        self.active = False

    def stopped(self):
        """Called by main thread to make sure thread isnt working"""

        return self.stopped


    #-------------------------------------------------------------------
    def run(self):
        """Process queued work"""

        while self.active:
            try:
                
                requestID, entryPoint, args, kwds = self.workRequestQueue.get()

                ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, "Worker thread - run - gRequestsQueue size: "+str(self.workRequestQueue.qsize()))

                ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, "Worker thread - run - requestID: "+str(requestID))
                ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, "Worker thread - run - entrypoint: "+str(entryPoint))
                ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, "Worker thread - run - args: "+str(args))
                ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, "Worker thread - run - kwds: "+str(kwds))

                entryPoint(*args, **kwds)
                
                if self.initTID:
                    ###ParentHandler.LogMessage( gParent, KEP.INFO_LOG_LEVEL, self.getName()+" worker thread - tid from c++: "+str(Dispatcher.GetCurrentThreadId()))
                    ParentHandler.LogMessage( gParent, KEP.INFO_LOG_LEVEL, self.getName()+" worker thread - tid: "+str(self._get_my_tid()))
                    self.initTID = False
                
                ###Determine who needs results
                ###self.resultQueue.put((requestID, entryPoint(*args, **kwds)))
                ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, "Worker thread - run - gRequestQueue size: "+str(self.workRequestQueue.qsize()))

            except urllib2.HTTPError, ehttp:
                ### This type of exception will kill the thread using the general execption handler
                ### It does not like ehttp.args
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **HTTP EXCEPTION** - class name: "+str(ehttp.__class__.__name__))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **HTTP EXCEPTION** - entry point: "+str(entryPoint))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **HTTP EXCEPTION** - entry point args: "+str(args))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **HTTP EXCEPTION** - gRquestsQueue size: "+str(self.workRequestQueue.qsize()))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **HTTP EXCEPTION** - exception object: "+str(ehttp))

            except Exception, e:

                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **EXCEPTION** - class name: "+str(e.__class__.__name__))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **EXCEPTION** - entry point: "+str(entryPoint))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **EXCEPTION** - entry point args: "+str(args))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **EXCEPTION** - exception args: "+str(e.args))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **EXCEPTION** - gRquestsQueue size: "+str(self.workRequestQueue.qsize()))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **EXCEPTION** - exception object: "+str(e))
                
        self.stopped = True                
    
#-------------------------------------------------------------------
# INIT EVENTS AND HANDLERS
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    """Called from C to initialize all the handlers in this script"""

    global gParent
    global gDispatcher

    gParent = handler
    gDispatcher = dispatcher


#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    #DO NOT LEAVE THIS TURNED ON IN RELEASE!!!
    #IT WILL LEAK MEMORY BECAUSE GARBAGE COLLECTION IS TURNED OFF!!! 
    #import gc
    #gc.set_debug( gc.DEBUG_LEAK )
    
    pass

