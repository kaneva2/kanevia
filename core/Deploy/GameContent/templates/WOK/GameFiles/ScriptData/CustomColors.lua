--colors for chat
--format hex number for alpha, red, green, blue

chatcolor         = {}
chatcolor.talk    = "FFFFFFFF"
chatcolor.clan    = "FF00FF00"
chatcolor.group   = "FF00FF00"
chatcolor.whisper = "C0C0C0C0"
chatcolor.shout   = "FFFF0000"
chatcolor.system  = "FFFFFF00"
chatcolor.private = "FF00FFFF"