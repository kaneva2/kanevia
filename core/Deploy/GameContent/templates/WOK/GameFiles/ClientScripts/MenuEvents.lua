dofile( "..\\Scripts\\KEP.lua" )

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	KEP.parent = handler
	KEP.dispatcher = dispatcher
	KEP.debugLevel = debugLevel

	ParentHandler_RegisterEventHandler( dispatcher, handler, "destroyMenuHandler", 1,0,0,0, "DestroyMenuEvent", KEP.MED_PRIO )

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	KEP.parent = handler
	KEP.dispatcher = dispatcher
	KEP.debugLevel = debugLevel

	ParentHandler_LogMessage( KEP.parent, KEP.DEBUG_LOG_LEVEL,  "MenuEvents.lua - Initialize KEP Events **" )

	-- call the c funtion to register our event handlers
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "DestroyMenuEvent", KEP.MED_PRIO )
	
end

---------------------------------------------------------------------
--
-- Destroy Menu Handler
--
---------------------------------------------------------------------
function destroyMenuHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

	if Dialog_Create == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
	end

 	local dialogHandle = Event_DecodeNumber( event )
  
	--Dialog_MsgBox( "values are "..tostring(dialogHandle)  )
	
	Dialog_Destroy(dialogHandle)
end

