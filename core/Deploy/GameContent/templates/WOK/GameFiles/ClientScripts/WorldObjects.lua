dofile("..\\Scripts\\KEP.lua")

handle = 0
gDispatcher = nil


WORLD_OBJ_MENU_EVENT = "WorldObjectMenuEvent"
CLOSE_WORLDOBJ_EVENT = "CloseWorldObjEvent"

X_COORD = 400
Y_COORD = 0

menuOpenEvent = 0

function loadMenu( handle, name, event )

	objId  = Event_DecodeNumber( event );  
	x  = Event_DecodeNumber( event );
	y  = Event_DecodeNumber( event );
	canHangPic = Event_DecodeNumber( event );

	if handle == 0 then
		handle = Dialog_CreateAtCoordinates( name, X_COORD, Y_COORD )
	end	
	
	local l_btnHangPic = Dialog_GetButton(handle, "btnHangPicture")
	if l_btnHangPic ~= 0 then
		Control_SetVisible(l_btnHangPic, canHangPic )
	end	
	
	--fire event to pass objId to the menu script
	menuOpenEvent = Dispatcher_MakeEvent( KEP.dispatcher, WORLD_OBJ_MENU_EVENT )
	
  	Event_EncodeNumber( menuOpenEvent, objId )
	Dispatcher_QueueEvent( KEP.dispatcher, menuOpenEvent )
	
	return handle
	
end

function unloadMenu( handle )
	if handle ~= 0 then
		Dialog_Destroy( handle )
		Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, "world obj menu closed" ) 
	end
	return 0
end 

function worldObjectSelectedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

  gDispatcher = dispatcher

  Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, " 1 " ) 
	if Dialog_CreateAtCoordinates == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
	end

	Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, " 2 " ) 
  if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end

  Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, " 3 " ) 
--[[
  if handle ~= 0 then 
	  Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, "object selected" ) 
	  handle = unloadMenu(handle); --close current menu
  end			
]]--
	local oldHandle = handle

    --Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "@@@ Old Handle: "..oldHandle )

	--Must set handle to 0 or load menu will get an invalid handle and try to get a button from it, not good
	handle = 0

    --Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, " 4 " )
	handle = loadMenu( handle, "WorldObjectMenu.xml", event)
	
 	--Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "@@@ New Handle: "..handle )
	if handle ~= oldHandle then
		sendCloseEvent( oldHandle )
	end

	return KEP.EPR_OK
end

function sendCloseEvent( dh )

	local ev = Dispatcher_MakeEvent( KEP.dispatcher, CLOSE_WORLDOBJ_EVENT )

	Event_EncodeNumber( ev, dh )

	Dispatcher_QueueEvent( KEP.dispatcher, ev )

end

---------------------------------------------------------------------
--
-- Close menu, if there is one
--
---------------------------------------------------------------------
function worldObjectUnSelectedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if Dialog_CreateAtCoordinates == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
	end

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end

	--Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, "object unselected" ) 
	--handle = unloadMenu(handle); --close current menu

	return KEP.EPR_OK
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	ParentHandler_RegisterEventHandler( dispatcher, handler, "worldObjectSelectedHandler", 1,0,0,0, "WorldObjectSelectedEvent", KEP.HIGH_PRIO ) 
	ParentHandler_RegisterEventHandler( dispatcher, handler, "worldObjectUnSelectedHandler", 1,0,0,0, "WorldObjectUnSelectedEvent", KEP.HIGH_PRIO ) 

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	--HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, WORLD_OBJ_MENU_EVENT, KEP.MED_PRIO )	
    Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, CLOSE_WORLDOBJ_EVENT, KEP.HIGH_PRIO )
    
	KEP.parent = handler

	-- the dispatcher 
	KEP.dispatcher = dispatcher

end

