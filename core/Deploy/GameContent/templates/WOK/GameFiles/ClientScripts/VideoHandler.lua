dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\CMD.lua")
dofile("..\\Scripts\\PlayerIds.lua")
dofile("..\\Scripts\\GameGlobals.lua")

URLADDTLDIR = ""

-- screen trace ids
SCREEN_1 = 385 --movie theater
SCREEN_2 = 386 --movie theater
SCREEN_3 = 387 --movie theater
SCREEN_4 = 388 --movie theater

SCREEN_5 = 2412
SCREEN_6 = 3421

SCREEN_7 = 76 --apartment L shape
SCREEN_8 = 57 --big apt tv screen

playingTraceId = nil

function videoTrigger( dispatcher, fromNetid, event, eventid, filter, objectid, screenTrace, flv )
	if Dialog_Create == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
	end

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end
	
	game = Dispatcher_GetGame( dispatcher )
	
  ret = 0
	
	if filter == KEP.TriggerEnter then
        ret = ClientEngine_StartPlayingFlashOnObject( game, screenTrace , "kepflvplayer.swf", "file=http://www.kaneva.com/wok_flash/" .. flv )
	elseif filter == KEP.TriggerExit then
		    ret = ClientEngine_StopPlayingFlashOnObject( game, screenTrace  )
	end
	
	return KEP.EPR_CONSUMED
end

function videoTriggerName(dispatcher, fromNetid, event, eventid, filter, objectid, screenName, flv )
	if Dialog_Create == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
	end

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end
	
	game = Dispatcher_GetGame( dispatcher )
    
    ret = 0
	
    -- extract the parameters of a trigger event
    --  (*OutBuffer()) << triggerIndex << radius << triggerName << x << y << z;
    if filter == KEP.TriggerEnter then --enter the trigger
        ret = ClientEngine_StartPlayingFlashOnNamedObject( game, screenName , "kepflvplayer.swf", "file=http://www.kaneva.com/wok_flash/" .. flv )
        playingTraceId = screenName
    elseif filter == KEP.TriggerExit then --exit the trigger
        ret = ClientEngine_StopPlayingFlashOnNamedObject( game, screenName  )
        playingTraceId = 0
    end
    
    return KEP.EPR_CONSUMED
end


--------------------------------------------------------------------------------------------------------------------
-- T H E A T E R
--------------------------------------------------------------------------------------------------------------------

function theater1(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "Projector_Screen01_2.KZM", 5321 )
end

function theater2(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "Projector_Screen02_2.KZM", 5331 )
end

function theater3(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "Projector_Screen03_2.KZM", 5341 )
end

function theater4(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "Projector_Screen04_2.KZM", 5351 )
end

function theaterLobby(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "N:22Video_Wall_Screen", 5311 )
end

--------------------------------------------------------------------------------------------------------------------
-- M I S C
--------------------------------------------------------------------------------------------------------------------
function penthouseVideo(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "N:056'TV_screen", 5301 )
end

function casinoVideo(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "TV_WALL_CASINO_SCREEN-01_0.KZM", 5471 )
end

function cafeVideo(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "N:056'TV_screen", 5461 )
end

--------------------------------------------------------------------------------------------------------------------
-- M A L L
--------------------------------------------------------------------------------------------------------------------

function mallVideo(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "N:6Large_56'TV_0", "amber_eye.swf", true )
end

function mallVideoNorth(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "N:6Large_56'TV_0", "amber_eye.swf", true )
end

function mallVideoSouth(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "N:6Large_56'TV_0", "amber_eye.swf", true )
end

function mallVideoEast(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "N:6Large_56'TV_0", "amber_eye.swf", true )
end

function mallVideoWest(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "N:6Large_56'TV_0", "amber_eye.swf", true )
end

--------------------------------------------------------------------------------------------------------------------
-- C O M P O U N D
--------------------------------------------------------------------------------------------------------------------
function rockBarVideo(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "N:056'TV_screen", 5451 )
end

function technoBarVideo(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "N:056'TV_screen", 5421 )
end

function countryBarVideo(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
    end
    return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "N:056'TV_screen", 5431 )
end

function hiphopBarVideo(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "N:056'TV_screen", 5441 )
end

function sportsbarVideo(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
    return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "N:056'TV_screen", 5411 )
end

--------------------------------------------------------------------------------------------------------------------
-- T U R N E R
--------------------------------------------------------------------------------------------------------------------
function turnerStar(dispatcher, fromNetid, event, eventid, filter, objectid)
--    if ClientEngine_GetPlayer == nil then
--		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
--	  end
--    return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "N:056'TV_screen", 5451 )
end

--Family Guy = FamilyGuy_TVscreen_0.KZM, Stewie_TV_Screen_0.KZM, Chris_TV_Screen_0.KZM, Meg_TV_Screen_0.KZM, Peter_TVscreen_0.KZM
function familyGuyVideo(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end
	  
	startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "Stewie_TV_Screen_0.KZM", "http://cnn-tbs-family-guy.wm.llnwd.net/cnn_tbs_family_guy?p=x.asx&x=t.wmv" )
	startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "Chris_TV_Screen_0.KZM", "http://cnn-tbs-family-guy.wm.llnwd.net/cnn_tbs_family_guy?p=x.asx&x=t.wmv" )
	startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "Meg_TV_Screen_0.KZM", "http://cnn-tbs-family-guy.wm.llnwd.net/cnn_tbs_family_guy?p=x.asx&x=t.wmv" )
	startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "Peter_TVscreen_0.KZM", "http://cnn-tbs-family-guy.wm.llnwd.net/cnn_tbs_family_guy?p=x.asx&x=t.wmv" )
	return startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "FamilyGuy_TVscreen_0.KZM", "http://cnn-tbs-family-guy.wm.llnwd.net/cnn_tbs_family_guy?p=x.asx&x=t.wmv" )
	
	--startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "Stewie_TV_Screen_0.KZM", 284030 )
	--startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "Chris_TV_Screen_0.KZM", 284030 )
	--startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "Meg_TV_Screen_0.KZM", 284030 )
	--startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "Peter_TVscreen_0.KZM", 284030 )
    --return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "FamilyGuy_TVscreen_0.KZM", 284030 )
end

--tbs = TBS_OutsideScreen_0.KZM, TBS_Lobby_BigScreen2_0.KZM
function tbsOutsideVideo(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	  end
	return startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "TBS_OutsideScreen_0.KZM", "http://cnn-tbs-10-items.wm.llnwd.net/cnn_tbs_10_items?p=x.asx&x=t.wmv" )
    --return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "TBS_OutsideScreen_0.KZM", 284029 )
end

function tbsLobbyVideo(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end
	return startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "TBS_Lobby_BigScreen2_0.KZM", "http://cnn-tbs-10-items.wm.llnwd.net/cnn_tbs_10_items?p=x.asx&x=t.wmv" )
    --return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "TBS_Lobby_BigScreen2_0.KZM", 284028 )
end

--TNT = TNT_Cinema_Screen_0.KZM, TNT_NYC_TV_Screen_0.KZM
function tntVideo(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end

	startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "TNT_NYC_TV_Screen_0.KZM", "http://cnn-tnt-generic.wm.llnwd.net/cnn_tnt_generic?p=x.asx&x=t.wmv" )
	return startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "TNT_Cinema_Screen_0.KZM", "http://cnn-tnt-generic.wm.llnwd.net/cnn_tnt_generic?p=x.asx&x=t.wmv" )

    --startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "TNT_NYC_TV_Screen_0.KZM", 284036 )
    --return startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "TNT_Cinema_Screen_0.KZM", 284036 )
end


--------------------------------------------------------------------------------------------------------------------
-- T U T O R I A L
--------------------------------------------------------------------------------------------------------------------
function tutVideo1(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
    end
    --First screen in sequence 120Inch Screen-05_0.KZM
	return startSwfOnNamedObject2( dispatcher, fromNetid, event, eventid, filter, objectid, "120Inch Screen-05_0.KZM", "file=http://streaming.kaneva.com/media/help/help-video1.flv", false )

end

function tutVideo2(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
    end
    --Second screen in sequence 120Inch Screen-03_0.KZM
	return startSwfOnNamedObject2( dispatcher, fromNetid, event, eventid, filter, objectid, "120Inch Screen-03_0.KZM", "file=http://streaming.kaneva.com/media/help/help-video2.flv", false )

end

function tutVideo3(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
    end
    --Third screen in sequence 120Inch Screen-04_0.KZM
	return startSwfOnNamedObject2( dispatcher, fromNetid, event, eventid, filter, objectid, "120Inch Screen-04_0.KZM", "file=http://streaming.kaneva.com/media/help/help-video3.flv", false )

end

function tutVideo4(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
    end
    --Fourth screen in sequence 120Inch Screen-01_0.KZM
	return startSwfOnNamedObject2( dispatcher, fromNetid, event, eventid, filter, objectid, "120Inch Screen-01_0.KZM", "file=http://streaming.kaneva.com/media/help/help-video4.flv", false )

end

function tutVideo5(dispatcher, fromNetid, event, eventid, filter, objectid)
    if ClientEngine_GetPlayer == nil then
		   Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
    end
    --Fifth screen in sequence 120Inch Screen-02_0.KZM
	return startSwfOnNamedObject2( dispatcher, fromNetid, event, eventid, filter, objectid, "120Inch Screen-02_0.KZM", "file=http://streaming.kaneva.com/media/help/help-video5.flv", false )

end


--------------------------------------------------------------------------------------------------------------------
-- CONFERENCE ROOM
--------------------------------------------------------------------------------------------------------------------
function conferenceRoomVidHandler (dispatcher, fromNetid, event, eventid, filter, objectid)

	startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "56'TV_screen-01_0.KZM", "http://www.youtube.com/v/7FNk9zMFzW8&autoplay=1", false )
	startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "56'TV_screen-02_0.KZM", "http://www.youtube.com/v/7FNk9zMFzW8&autoplay=1", false )
	startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "56'TV_screen-03_0.KZM", "http://www.youtube.com/v/7FNk9zMFzW8&autoplay=1", false )
	startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, "56'TV_screen-04_0.KZM", "http://www.youtube.com/v/7FNk9zMFzW8&autoplay=1", false )


    return KEP.EPR_CONSUMED

end


--------------------------------------------------------------------------------------------------------------------
function startPlaylistOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, screenName, plid )
   if Dialog_Create == nil then 
      Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
   end
   if ClientEngine_GetPlayer == nil then
      Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
   end
   game = Dispatcher_GetGame( dispatcher )
   ret = 0
   if filter == KEP.TriggerEnter then --enter the trigger
      ret = ClientEngine_StartPlayingFlashOnNamedObjectSecure( game, screenName , "KGPPlaylist.swf", "playlistUrl=" .. GameGlobals.WEB_SITE_PREFIX_KANEVA .. URLADDTLDIR .. "services/omm/request.aspx%3ftype%3d5%26plId%3d" .. tostring(plid) .. "%26game%3d1"  )
      playingTraceId = screenName
   elseif filter == KEP.TriggerExit then --exit the trigger
      ret = ClientEngine_StopPlayingFlashOnNamedObject( game, screenName  )
      playingTraceId = 0
   end
   return KEP.EPR_CONSUMED
end

function startSwfOnNamedObject2( dispatcher, fromNetid, event, eventid, filter, objectid, screenName, swf, secure )
   if Dialog_Create == nil then 
      Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
   end
   if ClientEngine_GetPlayer == nil then
      Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
   end
   game = Dispatcher_GetGame( dispatcher )
   ret = 0
   if filter == KEP.TriggerEnter then --enter the trigger
        if secure then
            ret = ClientEngine_StartPlayingFlashOnNamedObjectSecure( game, screenName , "kepflvplayer.swf", swf )
        else
            ret = ClientEngine_StartPlayingFlashOnNamedObject( game, screenName , "kepflvplayer.swf", swf )
        end
      playingTraceId = screenName
   elseif filter == KEP.TriggerExit then --exit the trigger
      ret = ClientEngine_StopPlayingFlashOnNamedObject( game, screenName  )
      playingTraceId = 0
   end
   return KEP.EPR_CONSUMED
end

function startSwfOnNamedObject( dispatcher, fromNetid, event, eventid, filter, objectid, screenName, swf, secure )
   if Dialog_Create == nil then 
      Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
   end
   if ClientEngine_GetPlayer == nil then
      Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
   end
   game = Dispatcher_GetGame( dispatcher )
   ret = 0
   if filter == KEP.TriggerEnter then --enter the trigger
        if secure then
            ret = ClientEngine_StartPlayingFlashOnNamedObjectSecure( game, screenName , swf, ""  )
        else
            ret = ClientEngine_StartPlayingFlashOnNamedObject( game, screenName , swf, ""  )
        end
      playingTraceId = screenName
   elseif filter == KEP.TriggerExit then --exit the trigger
      ret = ClientEngine_StopPlayingFlashOnNamedObject( game, screenName  )
      playingTraceId = 0
   end
   return KEP.EPR_CONSUMED
end




-------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
-------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, dbgLevel )
	
  KEP.parent     = handler
  KEP.dispatcher = dispatcher
  KEP.debugLevel = dbgLevel

    ParentHandler_RegisterEventHandler( dispatcher, handler, "conferenceRoomVidHandler",    1,0,0,0, "conf_rm_tv_trigger", KEP.MED_PRIO )

    return True
end

-------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
-------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, dbgLevel )
	KEP.parent = handler

	-- the dispatcher 
	KEP.dispatcher = dispatcher
end
