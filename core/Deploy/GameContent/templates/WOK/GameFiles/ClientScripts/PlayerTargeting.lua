dofile("..\\Scripts\\KEP.lua")

handle = 0

CLOSE_CONTEXT_EVENT = "CloseContextEvent"
PLAYER_TARGETED_MENU_EVENT = "PlayerTargetedMenuEvent"

menuOpenEvent = 0

function loadMenu( handle, name,  x, y, kanevaUserId, clickedName )

	if handle == 0 then
		handle = Dialog_CreateAtCoordinates( name, x, y )
	end
  Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "before dispatch player targeted" )	
	--fire event to pass placement id to the menu script.
	menuOpenEvent = Dispatcher_MakeEvent( KEP.dispatcher, PLAYER_TARGETED_MENU_EVENT )
	
	Event_EncodeNumber( menuOpenEvent, kanevaUserId )
	Event_EncodeString( menuOpenEvent, clickedName )	
	Dispatcher_QueueEvent( KEP.dispatcher, menuOpenEvent )
  Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "AFTER dispatch player targeted" )	
	return handle
	
end

function unloadMenu( handle )
	if handle ~= 0 then
		Dialog_Destroy( handle )
		Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "player targeting menu closed" ) 
	end
	return 0
end 

function playerSelectedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if Dialog_CreateAtCoordinates == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
	end

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end
--[[
  if handle ~= 0 then 
	  Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "object selected" ) 
	  handle = unloadMenu(handle) --close current menu
  end
]]--

	local oldHandle = handle

  	Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "targeting handle assign old handle: "..handle )

	handle = 0
		
	kanevaUserId  = Event_DecodeNumber( event )
	x  = Event_DecodeNumber( event )
	y  = Event_DecodeNumber( event )  
	myName = Event_DecodeString( event )
		
  	Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "open new player targeting menu" ) 
	handle = loadMenu( handle, "ContextMenu.xml", x, y, kanevaUserId, myName)

  	Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "targeting newly created handle: "..handle )

	if handle ~= oldHandle then
		sendCloseEvent( oldHandle )
	end

	--KEP.EPR_CONSUMED
	return KEP.EPR_OK
end

function sendCloseEvent( dh )

	local ev = Dispatcher_MakeEvent( KEP.dispatcher, CLOSE_CONTEXT_EVENT )

	Event_EncodeNumber( ev, dh )

	Dispatcher_QueueEvent( KEP.dispatcher, ev )

end

---------------------------------------------------------------------
--
-- Close menu, if there is one
--
---------------------------------------------------------------------
function playerUnSelectedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if Dialog_CreateAtCoordinates == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
	end

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end

	--Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "object unselected" ) 
	--handle = unloadMenu(handle); --close current menu

	return KEP.EPR_OK
end

---------------------------------------------------------------------
--
-- Reset the menu handle to 0 when new zone loaded
--
---------------------------------------------------------------------
function newZoneLoadedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Set handle back to 0 to ensure that handle isnt set to the hud or chat menu
	handle = 0

	return KEP.EPR_OK
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	ParentHandler_RegisterEventHandler( dispatcher, handler, "playerSelectedHandler", 1,0,0,0, "PlayerSelectedEvent", KEP.HIGH_PRIO) 
	ParentHandler_RegisterEventHandler( dispatcher, handler, "playerUnSelectedHandler", 1,0,0,0, "PlayerUnSelectedEvent", KEP.HIGH_PRIO ) 
	ParentHandler_RegisterEventHandler( dispatcher, handler, "newZoneLoadedHandler", 1,0,0,0, "ZoneLoadedEvent", KEP.HIGH_PRIO )

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	--HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, PLAYER_TARGETED_MENU_EVENT, KEP.HIGH_PRIO )	
    Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, CLOSE_CONTEXT_EVENT, KEP.HIGH_PRIO )
    
	KEP.parent = handler

	-- the dispatcher 
	KEP.dispatcher = dispatcher

end

