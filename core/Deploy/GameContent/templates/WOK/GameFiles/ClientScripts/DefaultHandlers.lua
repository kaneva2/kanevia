dofile("..\\Scripts\\KEP.lua")

---------------------------------------------------------------------
--
-- Fired when your weapon has no ammo and you attempt to fire it
--
---------------------------------------------------------------------
function outOfAmmoHandler (dispatcher, fromNetid, event, eventid, filter, objectid)

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end

	local SOUNDID = 11

	ClientEngine_SendChatMessage( Dispatcher_GetGame( dispatcher ), 7, "YOU ARE OUT OF AMMO.")
	ClientEngine_PlaySoundById( Dispatcher_GetGame( dispatcher ), SOUNDID, 0, 0, 0, 0, 0.03, 10.0 )
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	ParentHandler_RegisterEventHandler( dispatcher, handler, "outOfAmmoHandler", 1,0,0,0, "OutOfAmmoEvent", KEP.HIGH_PRIO ) 
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	KEP.parent = handler

	-- the dispatcher 
	KEP.dispatcher = dispatcher

	gScriptDir = "GameFiles\\ClientScripts\\"

end

