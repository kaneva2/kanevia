dofile("..\\Scripts\\KEP.lua")

g_kdphandle = 0
g_kdpScoreBoardHandle = 0
g_Dispatcher = nil

g_gameId = 0
g_objectId = 0
g_networkAssignedId = 0
g_tempRadius = 0
g_miscInt = 0
g_miscStr = nil
g_x = 0
g_y = 0
g_z = 0
g_xCenter = 0
g_yCenter = 0
g_zCenter = 0

-- Are dance events locked
g_kdpLock = false

-- Object id of the dance floor that acquired lock
g_kdpLockOwner = 0

YES_NO_BOX_EVENT = "YesNoBoxEvent"
YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"
KDP_MENU_CLOSE_EVENT = "KDPCloseEvent"

function loadMenu( handle, name, event )

	if handle == 0 then

        handle = Dialog_Create( name )
        local sh = Dialog_GetStatic(handle, "GamePlay")

        dialogx = Dialog_GetLocationX(handle)
        dialogy = Dialog_GetLocationY(handle)

        --gamePlayCtrlx = Control_GetLocationX(sh)
        --gamePlayCtrly = Control_GetLocationY(sh)
        gamePlayCtrlWidth = Dialog_GetWidth(handle)
        gamePlayCtrlHeight = Dialog_GetHeight(handle)

        --absx = gamePlayCtrlx + dialogx
        --absy = gamePlayCtrly + dialogy

        local playKDP1Game = Dispatcher_MakeEvent( KEP.dispatcher, KEP.KDP1_EVENT )

        Event_EncodeNumber( playKDP1Game, g_gameId )
        Event_EncodeNumber( playKDP1Game, g_objectId )
        Event_EncodeNumber( playKDP1Game, g_networkAssignedId )
        Event_EncodeNumber( playKDP1Game, g_tempRadius )
        Event_EncodeNumber( playKDP1Game, g_miscInt )
        Event_EncodeString( playKDP1Game, g_miscStr )
        Event_EncodeNumber( playKDP1Game, g_x )
        Event_EncodeNumber( playKDP1Game, g_y )
        Event_EncodeNumber( playKDP1Game, g_z )
        Event_EncodeNumber( playKDP1Game, g_xCenter )
        Event_EncodeNumber( playKDP1Game, g_yCenter )
        Event_EncodeNumber( playKDP1Game, g_zCenter )
        Event_EncodeNumber( playKDP1Game, dialogx )
        Event_EncodeNumber( playKDP1Game, dialogy )
        Event_EncodeNumber( playKDP1Game, gamePlayCtrlWidth )
        Event_EncodeNumber( playKDP1Game, gamePlayCtrlHeight )

		Event_SetFilter( playKDP1Game, KEP.FILTER_START_GAME)

        Dispatcher_QueueEvent( KEP.dispatcher, playKDP1Game )

	end

	return handle

end

function unloadMenu( handle )
	if handle ~= 0 then
		Dialog_Destroy( handle )
		Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, "kdp menu closed" )
	end
	return 0
end

function kdpMenuCloseHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local kdpHandle = Event_DecodeNumber( event )

    g_kdphandle = 0

end

function answerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local answer = Event_DecodeNumber( event )

	if answer ~= 0 then

        g_kdpScoreBoardHandle = Dialog_Create( "kdpScoreBoard.xml" )
        Dialog_BringToFront(g_kdpScoreBoardHandle)

        g_kdphandle = 0
        g_kdphandle = loadMenu( g_kdphandle, "kdp1.xml", gkdpevent)

 	else

		g_kdpLockOwner = 0
		g_kdpLock = false

 	end

end

function worldObjectTriggerHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

  --filter is global id of dance floor (type)

  g_Dispatcher = dispatcher

  action  = Event_DecodeNumber( event );

  if Dialog_CreateAtCoordinates == nil then
   	Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
  end

  if ClientEngine_GetPlayer == nil then
  	Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
  end

  local oldkdphandle = g_kdphandle

  if action == 1 then

	  if not g_kdpLock then

		  	g_kdpLock = true
		  	g_kdpLockOwner = objectid
			-- enter trigger
			dhyesno = Dialog_Create( "YesNoBox.xml" )
			if dhyesno ~= 0 then
				Dialog_SetModal(dhyesno, 1)
			    saveEventData( event, filter, objectid )
			    local yesnoEvent = Dispatcher_MakeEvent( KEP.dispatcher, YES_NO_BOX_EVENT )
                Event_SetFilter( yesnoEvent, KEP.FILTER_KDP_ANSWER_PLAY)
			    Event_EncodeString( yesnoEvent, "Do you want to play Kaneva Dance Party?" )
			    Event_EncodeString( yesnoEvent, "Confirm" )
			    Dispatcher_QueueEvent( KEP.dispatcher, yesnoEvent )
			end

   	  else

	  	return

	  end

  elseif action == 2 then

    if objectid == g_kdpLockOwner then

	    -- exit trigger
	    if g_kdphandle ~= 0 then
		    local endKDP1Game = Dispatcher_MakeEvent( KEP.dispatcher, KEP.KDP1_EVENT )
		    Event_EncodeNumber( endKDP1Game, filter )
		    Event_SetFilter( endKDP1Game, KEP.FILTER_END_GAME)
		    Dispatcher_QueueEvent( KEP.dispatcher, endKDP1Game )

			g_kdphandle = unloadMenu(g_kdphandle)
		end

		g_kdpScoreBoardHandle = unloadMenu(g_kdpScoreBoardHandle)

		g_kdpLockOwner = 0
		g_kdpLock = false
	else

		return

	end

  end

  return KEP.EPR_OK


end

function saveEventData( event, filter, objectid )

	g_networkAssignedId = Event_DecodeNumber( event )
	g_tempRadius = Event_DecodeNumber( event )
	g_miscInt = Event_DecodeNumber( event )
    g_miscStr = Event_DecodeString( event )
	g_x = Event_DecodeNumber( event )
	g_y = Event_DecodeNumber( event )
	g_z = Event_DecodeNumber( event )
	g_xCenter = Event_DecodeNumber( event )
	g_yCenter = Event_DecodeNumber( event )
	g_zCenter = Event_DecodeNumber( event )

    g_objectId = objectid
    g_gameId = filter

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	ParentHandler_RegisterFilteredEventHandler(dispatcher, handler, "answerEventHandler", 1,0,0,0, YES_NO_ANSWER_EVENT, KEP.FILTER_KDP_ANSWER_PLAY, 0, 0, KEP.HIGH_PRIO )
	ParentHandler_RegisterFilteredEventHandler(dispatcher, handler, "worldObjectTriggerHandler", 1,0,0,0, "TriggerEvent", KEP.KDP1ID, 0, 0, KEP.HIGH_PRIO )
	ParentHandler_RegisterFilteredEventHandler(dispatcher, handler, "worldObjectTriggerHandler", 1,0,0,0, "TriggerEvent", KEP.KDP2ID, 0, 0, KEP.HIGH_PRIO )
	ParentHandler_RegisterFilteredEventHandler(dispatcher, handler, "worldObjectTriggerHandler", 1,0,0,0, "TriggerEvent", KEP.KDP3ID, 0, 0, KEP.HIGH_PRIO )

	ParentHandler_RegisterEventHandler( dispatcher, handler, "kdpMenuCloseHandler", 1,0,0,0, KDP_MENU_CLOSE_EVENT, KEP.MED_PRIO )

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	--HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, YES_NO_BOX_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, KDP_MENU_CLOSE_EVENT, KEP.MED_PRIO )
	--Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, KEP.KDP1_EVENT, KEP.MED_PRIO )

	KEP.parent = handler

	-- the dispatcher
	KEP.dispatcher = dispatcher

end

