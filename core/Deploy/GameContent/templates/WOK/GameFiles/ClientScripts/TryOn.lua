dofile("..\\Scripts\\KEP.lua")

g_tryonhandle = 0
g_dispatcher = nil

function TryOnInventoryHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

  g_dispatcher = dispatcher

  glid = Event_DecodeNumber( event );
  usetype = Event_DecodeNumber( event );

  if Dialog_CreateAtCoordinates == nil then
   	Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
  end

  if ClientEngine_GetPlayer == nil then
  	Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
  end

  g_tryonhandle = Dialog_Create( "TryOnInventory.xml" )
  Dialog_BringToFront(g_tryonhandle)

  return KEP.EPR_OK

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	ParentHandler_RegisterFilteredEventHandler(dispatcher, handler, "TryOnInventoryHandler", 1,0,0,0, "TryOnInventoryEvent", KEP.FILTER_TRYONINVENTORY, 0, 0, KEP.MED_PRIO )

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	KEP.parent = handler

	KEP.dispatcher = dispatcher

end

