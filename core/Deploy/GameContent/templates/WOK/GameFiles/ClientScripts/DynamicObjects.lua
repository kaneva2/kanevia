dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\ClientEngineIds.lua")

V_2_0 = true

handle = 0

DYNAMIC_OBJ_MENU_EVENT = "DynamicObjectMenuEvent"
PICTURE_FRAME_MENU_EVENT = "PictureFrameMenuEvent"
CLOSE_DYNMENU_EVENT = "CloseDynMenuEvent"

-- Position to open menu at instead of right on top of object
X_COORD = 400
Y_COORD = 0

menuOpenEvent = 0

function loadMenu(owner, handle, placementId, friendId, x, y,  isAttachableObject, name, canPlayMovie, description )
  if name == "NOT QUITE WORKING Pacman Arcade" then
  
    if owner ~= 0 then
      --show edit / play
      handle = Dialog_Create( "FlashGame.xml" );
      if handle == 0 then
		    Dialog_MsgBox("Menu [" .. menu_path .. "] not found.")
	    end
      Dialog_SetModal(handle, 1)
    	menuOpenEvent = Dispatcher_MakeEvent( KEP.dispatcher, DYNAMIC_OBJ_MENU_EVENT )
    	Event_EncodeNumber( menuOpenEvent, placementId )
  	  Event_EncodeString( menuOpenEvent, name )	
    	Event_EncodeNumber( menuOpenEvent, canPlayMovie )
    	Dispatcher_QueueEvent( KEP.dispatcher, menuOpenEvent )
    else
      --play the game
      handle = Dialog_Create( "FlashGame.xml" );
      if handle == 0 then
		    Dialog_MsgBox("Menu [" .. menu_path .. "] not found.")
	    end
      Dialog_SetModal(handle, 1)
    	menuOpenEvent = Dispatcher_MakeEvent( KEP.dispatcher, DYNAMIC_OBJ_MENU_EVENT )
    	Event_EncodeNumber( menuOpenEvent, placementId )
  	  Event_EncodeString( menuOpenEvent, name )	
    	Event_EncodeNumber( menuOpenEvent, canPlayMovie )
    	Dispatcher_QueueEvent( KEP.dispatcher, menuOpenEvent )
    end
    return
  end
  
  if name == "NOT WORKING Pool Table" then
    return
  end
  
  if name == "NOT WORKING DartBoard" then
    return
  end
  
  if isAttachableObject == 0 or (isAttachableObject ~= 0 and canPlayMovie ~= 0) then
    -- show regular dynamic obj menu
  	if handle == 0 then
		if owner ~= 0 then
  			handle = Dialog_CreateAtCoordinates( "DynamicObjectMenu.xml", X_COORD, Y_COORD )
		else
			handle = Dialog_CreateAtCoordinates( "DynamicObjectMenuVisitor.xml", X_COORD, Y_COORD)
		end
  	end	
  	--fire event to pass placement id to the menu script
  	menuOpenEvent = Dispatcher_MakeEvent( KEP.dispatcher, DYNAMIC_OBJ_MENU_EVENT )

 	
  	Event_EncodeNumber( menuOpenEvent, placementId )
	Event_EncodeString( menuOpenEvent, name )	
  	Event_EncodeNumber( menuOpenEvent, canPlayMovie )
  	Event_EncodeNumber( menuOpenEvent, isAttachableObject )
    Event_EncodeString( menuOpenEvent, description )
  	Dispatcher_QueueEvent( KEP.dispatcher, menuOpenEvent )
  else
	-- show picture frame menu, which is the only attachable obj
	if handle == 0 then
		handle = Dialog_CreateAtCoordinates( "PictureFrameMenu.xml", X_COORD, Y_COORD )
	end	
	
	--fire event to pass placement id to the menu script
	menuOpenEvent = Dispatcher_MakeEvent( KEP.dispatcher, PICTURE_FRAME_MENU_EVENT )

	Event_EncodeNumber( menuOpenEvent, placementId )
	Event_EncodeNumber( menuOpenEvent, friendId )
	Event_EncodeString( menuOpenEvent, name )	
	Dispatcher_QueueEvent( KEP.dispatcher, menuOpenEvent )
  end 
	
	return handle
	
end

function unloadMenu( handle )
	if handle ~= 0 then
		Dialog_Destroy( handle )
		Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, "dynamic obj menu closed" ) 
	end
	return 0
end 

function dynamicObjectSelectedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if Dialog_CreateAtCoordinates == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
	end

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end
	local game = Dispatcher_GetGameAsGetSet( dispatcher )

	local oldHandle = handle
	handle = 0
	local placementId  = Event_DecodeNumber( event );
	local friendId = Event_DecodeNumber( event );
	local x  = Event_DecodeNumber( event );
	local y  = Event_DecodeNumber( event );  
	local isAttachableObject = Event_DecodeNumber( event );
	local name = Event_DecodeString( event );	
	local canPlayMovie = Event_DecodeNumber( event ); 
    if V_2_0 then
        local isInteractive = Event_DecodeNumber( event );
    end
    local description = Event_DecodeString( event );
	Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, "open new dynamic obj menu" ) 
	if GetSet_GetNumber(game, ClientEngineIds.ZONEPERMISSIONS) <= 2 then
		handle = loadMenu( 1, handle, placementId, friendId, x, y, isAttachableObject, name, canPlayMovie, description)
	else
		handle = loadMenu( 0, handle, placementId, friendId, x, y, isAttachableObject, name, canPlayMovie, description)
	end
	if handle ~= oldHandle then
    		sendCloseEvent( oldHandle )
	end
	return KEP.EPR_OK
end

function sendCloseEvent( dh )

	local ev = Dispatcher_MakeEvent( KEP.dispatcher, CLOSE_DYNMENU_EVENT )

	Event_EncodeNumber( ev, dh )

	Dispatcher_QueueEvent( KEP.dispatcher, ev )

end

---------------------------------------------------------------------
--
-- Close menu, if there is one
--
---------------------------------------------------------------------
function dynamicObjectUnSelectedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if Dialog_CreateAtCoordinates == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
	end

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end

	--Dispatcher_LogMessage( dispatcher, KEP.TRACE_LOG_LEVEL, "object unselected" ) 
	--handle = unloadMenu(handle); --close current menu

	return KEP.EPR_OK
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	ParentHandler_RegisterEventHandler( dispatcher, handler, "dynamicObjectSelectedHandler", 1,0,0,0, "DynamicObjectSelectedEvent", KEP.HIGH_PRIO ) 
	ParentHandler_RegisterEventHandler( dispatcher, handler, "dynamicObjectUnSelectedHandler", 1,0,0,0, "DynamicObjectUnSelectedEvent", KEP.HIGH_PRIO ) 

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	--HelperInitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, DYNAMIC_OBJ_MENU_EVENT, KEP.MED_PRIO )	
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, PICTURE_FRAME_MENU_EVENT, KEP.MED_PRIO )	
    Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, CLOSE_DYNMENU_EVENT, KEP.HIGH_PRIO )

	KEP.parent = handler

	-- the dispatcher 
	KEP.dispatcher = dispatcher

end

