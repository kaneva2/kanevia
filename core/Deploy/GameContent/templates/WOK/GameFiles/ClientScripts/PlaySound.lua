dofile("..\\Scripts\\MovementIds.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\CMD.lua")
dofile( "..\\Scripts\\KEP.lua" )
dofile( "..\\Scripts\\SoundGlobals.lua" )


-- Fired from server python to inform client which npc was hit
function  collisionHandler(dispatcher, fromNetId, event, eventid, filter, objectid)

    if ClientEngine_GetPlayer == nil then
        Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
    end

    npcId = objectid
    
    game = Dispatcher_GetGame( KEP.dispatcher )

    sound = SG.ItemSounds[npcId]
    
    if sound ~= nil then
        -- 0,0,0 is xyz of sound, it means play sound at players location
        -- TODO: Check directsound to confirm and rethink this function a little
        ClientEngine_PlaySoundById(game, sound[SG.INDEX], sound[SG.LOOPING], 0, 0, 0, sound[SG.SOUND_FACTOR], sound[SG.CLIP_RADIUS]) 
    else
        Dispatcher_LogMessage( dispatcher, KEP.INFO_LOG_LEVEL, "PlaySound.lua: Sound not found" )        
    end
    
    return KEP.EPR_OK

end


function InitializeKEPEventHandlers( dispatcher, handler, dbgLevel )

    KEP.parent     = handler
    KEP.dispatcher = dispatcher
    KEP.debugLevel = dbgLevel

 -- Fired from server python to inform client which npc was hit	
    ParentHandler_RegisterEventHandler( dispatcher, handler, "collisionHandler",   1,0,0,0, "NPCCollisionEvent", KEP.MED_PRIO )
    
end

function InitializeKEPEvents( dispatcher, handler, dbgLevel )
	KEP.parent = handler

	-- the dispatcher 
	KEP.dispatcher = dispatcher
 
    Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "NPCCollisionEvent", KEP.MED_PRIO )
    
end
