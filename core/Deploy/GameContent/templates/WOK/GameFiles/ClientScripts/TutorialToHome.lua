dofile("..\\Scripts\\MovementIds.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\CMD.lua")
dofile( "..\\Scripts\\KEP.lua" )

function tutorialToHome(dispatcher, fromNetid, event, eventid, filter, objectid)
 
    if ClientEngine_GetPlayer == nil then
        Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_XWIN_FN )
    end



    event = Dispatcher_MakeEvent( KEP.dispatcher, "TextEvent" )
    g_game = Dispatcher_GetGame(KEP.dispatcher)
    local player = ClientEngine_GetPlayer( g_game )
    local playerId = GetSet_GetNumber( player, MovementIds.NETWORKDEFINEDID )

    Event_EncodeNumber( event, playerId )
    Event_EncodeString( event, "")
    Event_EncodeNumber( event, ChatType.Talk )
    Event_SetFilter( event, CMD.GOHOME )
    Event_AddTo( event, KEP.SERVER_NETID )
    Dispatcher_QueueEvent( KEP.dispatcher, event )

end

function InitializeKEPEventHandlers( dispatcher, handler, dbgLevel )

    KEP.parent     = handler
    KEP.dispatcher = dispatcher
    KEP.debugLevel = dbgLevel
	
    ParentHandler_RegisterEventHandler( dispatcher, handler, "tutorialToHome",   1,0,0,0, "trg_tutorial_to_home", KEP.MED_PRIO )
    
end

function InitializeKEPEvents( dispatcher, handler, dbgLevel )
	KEP.parent = handler

	-- the dispatcher 
	KEP.dispatcher = dispatcher
end
