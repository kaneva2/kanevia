dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\ClientEngineIds.lua")


--Catch player spawned event before web call for dyn obj and send request to server
--filter is the zoneIndex
--decode instanceId
function ClientSpawnEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	
	if Dialog_Create == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
	end

    -- Dialog_MsgBox("In lua ClientSpawnEventHandler")

    local instanceID =  Event_DecodeNumber( event )
    local zoneIndex = filter

    -- requeue event to server
    local evt = Dispatcher_MakeEvent( dispatcher, KEP.SERVERDYNOBJ_REQUEST_EVENT_NAME )
    if evt ~= 0 then
        Event_AddTo( evt, KEP.SERVER_NETID )
        Event_SetFilter( evt, zoneIndex )
        Event_EncodeNumber( evt, instanceID )
        Dispatcher_QueueEvent( KEP.dispatcher, evt ) 
    end
    
    return KEP.EPR_CONSUMED
    
end 



function ServerDynObjReplyEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if Dialog_Create == nil then 
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_MENU_FN )
	end
  
    -- Dialog_MsgBox("In lua ServerDynObjReplyEventHandler")

    zoneXML = Event_DecodeString( event )
    
    -- Dialog_MsgBox("Decoded XML")
    
    extractDynObjData( zoneXML )
	
    extractWorldObjData( zoneXML )
    
	return KEP.EPR_CONSUMED
	
end 


-- Parse out xml for dynamic object data
-- 
--
function extractDynObjData( xml )

    first = 0       --start of substring
    last = 0        --end of substring
    strPos = 1      --posintion in string
    count = 0
    
    dynObj = ""
    
    first, strPos, dynObj = string.find( xml, "<D>(.-)</D>", strPos )    
    
    -- Dialog_MsgBox(dynObj)
    
    while dynObj ~= nil do
    
        count = count + 1
    
        first, last, placementId = string.find( dynObj, "<l>(.-)</l>", 1 )
        first, last, objectId = string.find( dynObj, "<o>(.-)</o>", 1 )
        first, last, texAssetId = string.find( dynObj, "<t>(.-)</t>", 1 )
        first, last, playerId = string.find( dynObj, "<p>(.-)</p>", 1)  
        first, last, friendId = string.find( dynObj, "<f>(.-)</f>", 1 )
        first, last, x = string.find( dynObj, "<x>(.-)</x>", 1 )
        first, last, y = string.find( dynObj, "<y>(.-)</y>", 1 )
        first, last, z = string.find( dynObj, "<z>(.-)</z>", 1 )
        first, last, rx = string.find( dynObj, "<rx>(.-)</rx>", 1 )
        first, last, ry = string.find( dynObj, "<ry>(.-)</ry>", 1 )
        first, last, rz = string.find( dynObj, "<rz>(.-)</rz>", 1 )
        first, last, url = string.find( dynObj, "<u>(.-)</u>", 1 )
        first, last, swf = string.find( dynObj, "<w>(.-)</w>", 1 )
        first, last, swfParams = string.find( dynObj, "<wp>(.-)</wp>", 1 )
        first, last, glid = string.find( dynObj, "<g>(.-)</g>", 1 )
        
        if url == nil then
            url = ""
        end

        if swf == nil then
            swf = ""
        end

        if swfParams == nil then
            swfParams = ""
        end
     
        placementId = tonumber(placementId)
        objectId = tonumber(objectId)
        glid = tonumber(glid)
        texAssetId = tonumber(texAssetId)
        playerId = tonumber(playerId)
        friendId = tonumber(friendId)
        x = tonumber(x)
        y = tonumber(y)
        z = tonumber(z)
        rx = tonumber(rx)
        ry = tonumber(ry)
        rz =tonumber(rz)
             
        local e = Dispatcher_MakeEvent( KEP.dispatcher, "AddDynamicObjectEvent" )
        if e ~= 0 then

            --The following encodings MUST happen in this order

            Event_SetObjectId( e, objectId )

            Event_SetFilter( e, playerId )

            Event_EncodeNumber( e, x )
            Event_EncodeNumber( e, y )
            Event_EncodeNumber( e, z )
            Event_EncodeNumber( e, rx )
            Event_EncodeNumber( e, ry )
            Event_EncodeNumber( e, rz )
            Event_EncodeNumber( e, placementId )
            Event_EncodeNumber( e, texAssetId )
            Event_EncodeString( e, url)
            Event_EncodeNumber( e, friendId )
            Event_EncodeString( e, swf )
            Event_EncodeString( e, swfParams )
            Event_EncodeNumber( e, glid )          
            
            addUnusedAttributes( e )
            
            Dispatcher_QueueEvent( KEP.dispatcher, e ) 
        end
    
        first, strPos, dynObj = string.find( xml, "<D>(.-)</D>", strPos + 1)
    
    end
    
    -- Dialog_MsgBox( "Dynamic Object count: "..tostring(count) ) 

end

function addUnusedAttributes( e )

    Event_EncodeNumber( e, 0 ) --attachable
    Event_EncodeNumber( e, 0 ) --interactive
    Event_EncodeNumber( e, 0 ) --playFlash
    Event_EncodeNumber( e, 0 ) --collision
    Event_EncodeNumber( e, 0 ) --min_x
    Event_EncodeNumber( e, 0 ) --min_y
    Event_EncodeNumber( e, 0 ) --min_z
    Event_EncodeNumber( e, 0 ) --max_x   
    Event_EncodeNumber( e, 0 ) --max_y
    Event_EncodeNumber( e, 0 ) --max_z
    Event_EncodeNumber( e, 0 ) --max_z
    Event_EncodeNumber( e, 0 ) --derivable
    Event_EncodeNumber( e, 256 ) --IT_NORMAL

end

function extractWorldObjData( xml )

    first = 0       --start of substring
    last = 0        --end of substring
    strPos = 1      --posintion in string
    count = 0
    
    wObj = ""
    
    first, strPos, wObj = string.find( xml, "<W>(.-)</W>", strPos )    
    
    --Dialog_MsgBox(wObj)
    
    while wObj ~= nil do
    
        count = count + 1
    
        first, last, objectId = string.find( wObj, "<o>(.-)</o>", 1 )
        first, last, assetId = string.find( wObj, "<a>(.-)</a>", 1 )
        first, last, texURL = string.find( wObj, "<u>(.-)</u>", 1)  
        
        texAssetId = tonumber(assetId)
        
        local e = Dispatcher_MakeEvent( KEP.dispatcher, "UpdateWorldObjectTextureEvent" )
        if e ~= 0 then

            --The following encodings MUST happen in this order

            Event_SetFilter( e, texAssetId )

            Event_EncodeString( e, objectId)
            Event_EncodeString( e, texURL )
            
            Dispatcher_QueueEvent( KEP.dispatcher, e ) 
        end
      
        first, strPos, wObj = string.find( xml, "<W>(.-)</W>", strPos +1 )
    
    end
    
    --Dialog_MsgBox( "World Object count: "..tostring(count) )

end


---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	ParentHandler_RegisterEventHandler( dispatcher, handler, "ClientSpawnEventHandler", 1,0,0,0, "ClientSpawnEvent", KEP.HIGH_PRIO ) 
	ParentHandler_RegisterEventHandler( dispatcher, handler, "ServerDynObjReplyEventHandler", 1,0,0,0, KEP.SERVERDYNOBJ_REPLY_EVENT_NAME , KEP.MED_PRIO ) 

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	KEP.parent = handler

	-- the dispatcher 
	KEP.dispatcher = dispatcher

    Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, KEP.SERVERDYNOBJ_REQUEST_EVENT_NAME, KEP.MED_PRIO )
    Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, KEP.SERVERDYNOBJ_REPLY_EVENT_NAME, KEP.MED_PRIO )

end

