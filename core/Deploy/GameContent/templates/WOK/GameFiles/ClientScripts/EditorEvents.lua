dofile( "..\\Scripts\\KEP.lua" )

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	KEP.parent = handler
	KEP.dispatcher = dispatcher
	KEP.debugLevel = debugLevel

end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	KEP.parent = handler
	KEP.dispatcher = dispatcher
	KEP.debugLevel = debugLevel

	ParentHandler_LogMessage( KEP.parent, KEP.DEBUG_LOG_LEVEL,  "Initialize KEP Events **" )

	-- call the c funtion to register our event handlers
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "MenuControlSelectedEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "MenuControlMovedEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "MenuControlResizedEvent", KEP.MED_PRIO )

	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "MenuSelectedEvent", MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "MenuMovedEvent", MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "MenuResizedEvent", MED_PRIO )
	
end

