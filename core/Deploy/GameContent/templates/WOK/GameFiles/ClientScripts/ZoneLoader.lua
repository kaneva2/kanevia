dofile("..\\Scripts\\KEP.lua")
dofile("..\\ClientScripts\\PatchHelper.lua")
dofile("..\\Scripts\\MovementCapsIds.lua")
dofile("..\\Scripts\\MovementIds.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\Progressive.lua")

first_zone_loaded = false

loginHandle = 0
gPatchHandle = 0
quickHandle = 0

function loadMenu( handle, name )

	if handle == 0 then
		handle = Dialog_Create( name )
	end
	
	return handle
	
end

function unloadMenu( handle )
	if handle ~= 0 then
		Dialog_Destroy( handle )
	end
	return 0
end 

function testPassListHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	
	if Dialog_Create == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
	end

	-- filter = 1 if zone PassList, sent on spawn
	-- filter = 2 if player PassList, then object id is player's networkassigned id, send on trade
	passCount =	Event_DecodeNumber( event )
	for i = 1, passCount do
		local passId = Event_DecodeNumber( event )
		-- TODO: something with this 
		Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "Player PassId: ("..tostring(i)..") "..tostring(passId) ) 
	end
	if filter == 1 then 
		passCount =	Event_DecodeNumber( event )
		for i = 1, passCount do
			local passId = Event_DecodeNumber( event )
			-- TODO: something with this 
			Dispatcher_LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "Zone PassId: ("..tostring(i)..") "..tostring(passId) ) 
		end
	end	
end 

ctSystem = 6

function PatchProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- if we get here, no one else consumed it
	-- we handle error cases, or create the progress menu
	-- TODO: are these the only cases we need to handle?
	local patchEnded = false
	
	if filter == MSG_COMPLETENM then
		gPatchHandle = 0 

	elseif filter == MSG_RECONNECT then
		local url = Event_DecodeString( event )

		textEvent = Dispatcher_MakeEvent( dispatcher, "RenderTextEvent" )
		if textEvent ~= nil then 
			Event_EncodeString( textEvent, "Lost connection during patch" )
			Event_EncodeNumber( textEvent, ctSystem )
			Dispatcher_QueueEvent( dispatcher, textEvent )
		end
		
		patchEnded = true
		
	elseif filter == MSG_VERBOSE_ERROR then
		local url = Event_DecodeString( event )
		local errNum = Event_DecodeNumber( event )
		local errMsg = Event_DecodeString( event )

		textEvent = Dispatcher_MakeEvent( dispatcher, "RenderTextEvent" )
		if textEvent ~= nil then 
			Event_EncodeString( textEvent, "Error patching "..url..". -- "..errMsg.." ("..tostring(errNum)..")"  )
			Event_EncodeNumber( textEvent, ctSystem )
			Dispatcher_QueueEvent( dispatcher, textEvent )
		end
		patchEnded = true

	elseif filter == MSG_ERROR then 
		local url = Event_DecodeString( event )
		local errNum = Event_DecodeNumber( event )
		local errMsg = "Unknown"
		if errNum == ERR_NOTCONNECTED then 
			errMsg = "Not connected"
		elseif errNum == ERR_NOVERSION then
			errMsg = "No version"
		elseif errNum == ERR_TIMEOUT then
			errMsg = "Timeout"
		end 
		
		textEvent = Dispatcher_MakeEvent( dispatcher, "RenderTextEvent" )
		if textEvent ~= nil then 
			Event_EncodeString( textEvent, "Error: "..errMsg )
			Event_EncodeNumber( textEvent, ctSystem )
			Dispatcher_QueueEvent( dispatcher, textEvent )
		end
		
		patchEnded = true

	end 

	if patchEnded then
		gPatchHandle = 0
	elseif gPatchHandle == 0 then
		gPatchHandle = Dialog_Create( "PatchProgress.xml" )
	end

	return KEP.EPR_OK
end

function pendingSpawnHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- we'll get this if the menu is not loaded, we load it a re-fire the same event for the menu 
	-- to handle
	if fromNetid == 0 then -- 0 if from server, 1 if local so we skip to avoid infinite loop

		local initialSpawn = Event_DecodeNumber( event )
		local coverCharge =  Event_DecodeNumber( event )
		
		local coverChargeHandle = 0
		if coverCharge > 0 then
			loadMenu( coverChargeHandle, "CoverCharge.xml" )
		else 			
			if PROGRESSIVE then 
				local downloadHandle = 0
				loadMenu( downloadHandle, "ZoneDownloader.xml" )
			else
				return KEP.EPR_OK -- let default handle it by spawning there
			end
		end 
			
		-- requeue event to menu
		local evt = Dispatcher_MakeEvent( dispatcher, KEP.PENDINGSPAWN_EVENT_NAME )
		-- Dialog_MsgBox( "pendingSpawnHandler evt="..tostring(evt).." event="..tostring(event).." from = "..fromNetid )	
		Event_SetFilter( evt, filter )
		Event_SetObjectId( evt, objectid )
		Event_EncodeNumber( evt, initialSpawn )
		Event_EncodeNumber( evt, coverCharge )
		Dispatcher_QueueEvent( dispatcher, evt ) 
		
	end 
	
	return KEP.EPR_CONSUMED
	
end 

function newZoneLoadedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	
	local g_game = nil
				
	if Dialog_Create == nil then 
		Dispatcher_GetFunctions( dispatcher, KEP.GET_MENU_FN )
    		g_game = Dispatcher_GetGame(KEP.dispatcher)
	end

	if ClientEngine_GetPlayer == nil then
		Dispatcher_GetFunctions( dispatcher, KEP.GET_XWIN_FN )
	end
	
	if KEPConfig_GetString == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end

	-- clear any pre-existing menus
	
	-- load menus for this scene, if Start.exg
	zoneName = string.lower(Event_DecodeString( event ))
	if zoneName == string.lower("loginmenu.exg") then 
	
		loginHandle = loadMenu( loginHandle, "LoginMenu.xml")	
	
	elseif zoneName == string.lower("pb_hangar_arena.exg") then 
		ClientEngine_SetMouseLook( Dispatcher_GetGame( dispatcher ), 1 )
	else
		ClientEngine_SetMouseLook( Dispatcher_GetGame( dispatcher ), 0 )		
	end

	return KEP.EPR_OK
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	ParentHandler_RegisterEventHandler( dispatcher, handler, "newZoneLoadedHandler", 1,0,0,0, "ZoneLoadedEvent", KEP.HIGH_PRIO ) 
	ParentHandler_RegisterEventHandler( dispatcher, handler, "PatchProgressEventHandler", 1,0,0,0, "PatchProgressEvent", KEP.LOW_PRIO ) 
	ParentHandler_RegisterEventHandler( dispatcher, handler, "pendingSpawnHandler", 1,0,0,0, KEP.PENDINGSPAWN_EVENT_NAME, KEP.MED_PRIO ) 
	ParentHandler_RegisterEventHandler( dispatcher, handler, "testPassListHandler", 1,0,0,0, "PassListEvent", KEP.MED_PRIO ) 
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )

	KEP.parent = handler

	-- the dispatcher 
	KEP.dispatcher = dispatcher

	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "PatchProgressEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "PassListEvent", KEP.MED_PRIO )

end

