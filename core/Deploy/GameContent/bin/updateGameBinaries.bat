@echo off
rem copy the binaries from the templates directory to the given game dir
if not exist ..\templates goto wrongdir
if %1. == . goto nodir
if not exist %1 goto nodir
echo This will copy the binaries from current install to  
echo     %1
pause
rem copy the files
xcopy CorePluginLibrary.dll %1 /y
xcopy CorePluginLibrary.pdb %1 /y
xcopy mysqlpp.dll %1 /y
xcopy mysqlpp.pdb %1 /y
xcopy SimpleAmqpClient.2.dll %1 /y
xcopy rabbitmq.4.dll %1 /y
xcopy CoreUtil.dll %1 /y
xcopy CoreUtil.pdb %1 /y
xcopy KEPMath.dll %1 /y
xcopy KEPMath.pdb %1 /y
xcopy KEPPhysics.dll %1 /y
xcopy KEPPhysics.pdb %1 /y
xcopy KEPUtil.dll %1 /y
xcopy KEPUtil.pdb %1 /y
xcopy CrashRpt1403.dll %1 /y
xcopy log4cplusU.dll %1 /y
xcopy nvtt.dll %1 /y
xcopy dbghelp.dll %1 /y
pushd ..\templates\shared
xcopy kepclient.exe %1 /y
xcopy kepclient.pdb %1 /y
xcopy ClientBlades\*.dll %1\ClientBlades\ /y
xcopy ClientBlades\*.pdb %1\ClientBlades\ /y
xcopy /s GameFiles\bladescripts %1\GameFiles\bladescripts\ /y
xcopy Crash*.dll %1 /y
xcopy Lua*.dll %1 /y
xcopy cjson*.dll %1 /y
xcopy f_in_box*.dll %1 /y
xcopy FreeImage*.dll %1 /y
xcopy libcollada14dom21.dll %1 /y
xcopy LzmaLib*.dll %1 /y
xcopy Zlib1*.dll %1 /y
xcopy jpeg62.dll %1 /y
popd
echo ended
goto end
:wrongdir
echo Must run from the installed bin directory
goto end
:nodir
echo Supply valid destination directory.  E.g. c:\newgame
goto end
:end 
