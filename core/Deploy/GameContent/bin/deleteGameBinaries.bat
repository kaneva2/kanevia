@echo off
if not exist %1 goto nodir
echo This will delete the build binaries from
echo     %1
pause
echo on
del /f %1\CoreUtil.dll
del /f %1\KEPMath.dll
del /f %1\KEPPhysics.dll
del /f %1\KEPUtil.dll
del /f %1\kepclient.exe
del /f %1\dll.pak
del /f %1\ClientBlades\*.dll
del /f %1\ClientBlades\dll.pak
del /f %1\GameFiles\bladescripts\*.dll
del /f %1\GameFiles\bladescripts\dll.pak
@echo off
echo ended
goto end
:nodir
echo Supply valid destination directory.  E.g. c:\newgame
goto end
:end 
