// Symbol file that defines constant values for the objects
// and counters that the provider provides. The counters should be
// grouped by object.

#define KEP_SERVER_OBJECT       	0
#define POPULATION                  2
#define QUEUE_COUNT					4
#define MSG_COUNT					6
#define THRUPUT						8
#define SQLCALLS					10

#define LAST_KEP_SERVER_OBJECT_COUNTER_OFFSET  SQLCALLS
