'==========================================================
'
' script for comparing the binaries in the templates
' directory of the editor with a game's content directory
'
'==========================================================
'on error resume next

dim copyFileList
' array or arrays
' each sub array has the path from <install>bin\ to the file as the first element
'    the second element is the path from <gamedir>\gamefiles
copyFileList = Array( _
    Array( "KEPUtil.dll",                                               	   "KEPUtil.dll" ), _
    Array( "dbghelp.dll",                                               	   "dbghelp.dll" ), _
    Array( "log4cplusU.dll",                                             	   "log4cplusU.dll" ), _
    Array( "python24.dll",                                              	   "python24.dll" ), _
    Array( "zlib.dll",                                                  	   "zlib.dll" ), _
    Array( "f_in_box.dll",                                                     "f_in_box.dll" ), _
    Array( "..\templates\shared\KEPClient.exe",                                "KEPClient.exe" ), _
    Array( "..\templates\shared\MiniDump.vbs",                                 "MiniDump.vbs" ), _
    Array( "..\templates\shared\CustomClientDist.wsf",                         "CustomClientDist.wsf" ), _
    Array( "..\templates\shared\CustomServerDist.wsf",                         "CustomServerDist.wsf" ), _
    Array( "..\templates\shared\extraClientFiles.xml",                         "extraClientFiles.xml" ), _
    Array( "..\templates\shared\extraServerFiles.xml",                         "extraServerFiles.xml" ), _
    Array( "..\templates\shared\luac.exe",                                 	   "luac.exe" ), _
    Array( "..\templates\shared\ClientBlades\ZMenu.dll",                       "ClientBlades\ZMenu.dll" ), _
    Array( "..\templates\shared\ClientBlades\wkgmesh.dll",                     "ClientBlades\wkgmesh.dll" ), _
    Array( "..\templates\shared\ClientBlades\wkgpsys.dll",                     "ClientBlades\wkgpsys.dll" ), _
    Array( "..\templates\shared\ClientBlades\targeting.dll",                   "ClientBlades\targeting.dll" ), _
    Array( "..\templates\shared\ClientBlades\effects\widget.fx",               "ClientBlades\effects\widget.fx" ), _
    Array( "..\templates\shared\ClientBlades\effects\material.fx",             "ClientBlades\effects\material.fx" ), _
    Array( "..\templates\shared\ClientBlades\effects\selection.fx",            "ClientBlades\effects\selection.fx" ), _
    Array( "..\templates\shared\ClientBlades\effects\skinned.fx",			   "ClientBlades\effects\skinned.fx" ), _
    Array( "..\templates\shared\GameFiles\BladeScripts\LuaEventHandler.dll",   "GameFiles\BladeScripts\LuaEventHandler.dll" ), _
    Array( "..\templates\shared\GameFiles\BladeScripts\PythonEventHandler.dll","GameFiles\BladeScripts\PythonEventHandler.dll" ), _
    Array( "..\templates\shared\GameFiles\BladeScripts\VwUrlEventHandler.dll", "GameFiles\BladeScripts\VwUrlEventHandler.dll" ), _
    Array( "..\templates\shared\GameFiles\BladeScripts\ScriptServerEngine.dll","GameFiles\BladeScripts\ScriptServerEngine.dll" ), _
    Array( "..\templates\shared\GameFiles\BladeScripts\PlayListMgr.dll",       "GameFiles\BladeScripts\PlayListMgr.dll" ), _
    Array( "libcollada14dom21.dll",                                            "libcollada14dom21.dll" ), _
    Array( "jpeg62.dll",                                                       "jpeg62.dll" ), _
    Array( "libpng12.dll",                                                     "libpng12.dll" ), _
    Array( "zlib1.dll",                                                        "zlib1.dll" ), _
    Array( "nvtt.dll",                                                         "nvtt.dll" ), _
    Array( "..\templates\shared\gtk\lib\charset.alias",                        "gtk\lib\charset.alias" ), _
    Array( "..\templates\shared\gtk\lib\gtk-2.0\modules\libgail.dll",          "gtk\lib\gtk-2.0\modules\libgail.dll" ), _
    Array( "..\templates\shared\gtk\share\themes\Default\gtk-2.0-key\gtkrc",   "gtk\share\themes\Default\gtk-2.0-key\gtkrc" ), _
    Array( "..\templates\shared\gtk\share\themes\Emacs\gtk-2.0-key\gtkrc",     "gtk\share\themes\Emacs\gtk-2.0-key\gtkrc" ), _
    Array( "..\templates\shared\gtk\share\themes\MS-Windows\gtk-2.0\gtkrc",    "gtk\share\themes\MS-Windows\gtk-2.0\gtkrc" ), _
    Array( "..\templates\shared\gtk\share\themes\Raleigh\gtk-2.0\gtkrc",       "gtk\share\themes\Raleigh\gtk-2.0\gtkrc" ), _
    Array( "..\templates\shared\gtk\CONTENTS",                                 "gtk\CONTENTS"), _
    Array( "..\templates\shared\pixmaps\emotes\default\theme",                 "pixmaps\emotes\default\theme" ), _
    Array( "..\templates\shared\pixmaps\emotes\none\theme",                    "pixmaps\emotes\none\theme" ), _
    Array( "..\templates\shared\pixmaps\emotes\small\theme",                   "pixmaps\emotes\small\theme" ), _
    Array( "..\templates\shared\pixmaps\tray\hicolor\index.theme",             "pixmaps\tray\hicolor\index.theme" ), _
    Array( "..\templates\shared\pixmaps\logo.png",                             "pixmaps\logo.png" ), _
    Array( "..\templates\shared\kanevatray.exe",                               "kanevatray.exe" ) _
	)

dim wildcardList
' first param must have wildcard, second must be only folder
wildcardList = Array ( _
    Array( "CrashRpt*.dll",                                             	   "."), _
    Array( "..\templates\shared\*.xml",                                        "."), _
    Array( "..\templates\shared\*.dll",                                        "."), _
    Array( "..\templates\shared\ca-certs\*.PEM",                               "ca-certs"), _
    Array( "..\templates\shared\gtk\bin\*.*",                                  "gtk\bin"), _
    Array( "..\templates\shared\gtk\etc\fonts\*.*",                            "gtk\etc\fonts"), _
    Array( "..\templates\shared\gtk\etc\gtk-2.0\*",                            "gtk\etc\gtk-2.0"), _
    Array( "..\templates\shared\gtk\etc\pango\*.*",                            "gtk\etc\pango"), _
    Array( "..\templates\shared\gtk\lib\gtk-2.0\2.10.0\engines\*.dll",         "gtk\lib\gtk-2.0\2.10.0\engines"), _
    Array( "..\templates\shared\gtk\manifest\*.mft",                           "gtk\manifest"), _
    Array( "..\templates\shared\gtk\share\themes\MyStyle\gtk-2.0\*",           "gtk\share\themes\MyStyle\gtk-2.0"), _
    Array( "..\templates\shared\plugins\*.dll",                                "plugins"), _
    Array( "..\templates\shared\sasl2\*.dll",                                  "sasl2"), _
    Array( "..\templates\shared\sounds\purple\*.wav",                          "sounds\purple"), _
    Array( "..\templates\shared\pixmaps\*.xpm",                                "pixmaps"), _
    Array( "..\templates\shared\pixmaps\animations\*.png",                     "pixmaps\animations"), _
    Array( "..\templates\shared\pixmaps\buttons\*.png",                        "pixmaps\buttons"), _
    Array( "..\templates\shared\pixmaps\dialogs\*.png",                        "pixmaps\dialogs"), _
    Array( "..\templates\shared\pixmaps\emblems\*.png",                        "pixmaps\emblems"), _
    Array( "..\templates\shared\pixmaps\animations\*.png",                     "pixmaps\animations"), _
    Array( "..\templates\shared\pixmaps\emotes\default\*.png",                 "pixmaps\emotes\default"), _
    Array( "..\templates\shared\pixmaps\emotes\small\*.png",                   "pixmaps\emotes\small"), _
    Array( "..\templates\shared\pixmaps\kaneva\*.png",                         "pixmaps\kaneva"), _
    Array( "..\templates\shared\pixmaps\protocols\*.png",                      "pixmaps\protocols"), _
    Array( "..\templates\shared\pixmaps\status\*.png",                         "pixmaps\status"), _
    Array( "..\templates\shared\pixmaps\toolbar\*.png",                        "pixmaps\toolbar"), _
    Array( "..\templates\shared\pixmaps\tray\*.ico",                           "pixmaps\tray"), _
    Array( "..\templates\shared\pixmaps\tray\hicolor\status\*.png",            "pixmaps\tray\hicolor\status"), _
    Array( "..\templates\shared\GameFiles\ServerScripts\*.py",                 "GameFiles\ServerScripts"), _
    Array( "..\templates\shared\GameFiles\*.xml",                              "GameFiles") _
    )

dim folderList
folderList = Array( _
	"\GameFiles", _
	"\GameFiles\Menus", _
	"\GameFiles\MenuScripts", _
    "\ClientBlades", _
    "\ClientBlades\effects", _
    "\ClientBlades\meshes", _
    "\GameFiles\BladeScripts", _
    "\ca-certs", _
    "\plugins", _
    "\sasl2", _
    "\sounds", _
    "\sounds\purple", _
    "\gtk", _
    "\gtk\bin", _
    "\gtk\etc", _
    "\gtk\etc\fonts", _
    "\gtk\etc\gtk-2.0", _
    "\gtk\etc\pango", _
    "\gtk\lib", _
    "\gtk\lib\gtk-2.0", _
    "\gtk\lib\gtk-2.0\2.10.0", _
    "\gtk\lib\gtk-2.0\2.10.0\engines", _
    "\gtk\lib\gtk-2.0\modules", _
    "\gtk\manifest", _
    "\gtk\share", _
    "\gtk\share\themes", _
    "\gtk\share\themes\Default", _
    "\gtk\share\themes\Emacs", _
    "\gtk\share\themes\MS-Windows", _
    "\gtk\share\themes\MyStyle", _
    "\gtk\share\themes\Raleigh", _
    "\gtk\share\themes\Default\gtk-2.0-key", _
    "\gtk\share\themes\Emacs\gtk-2.0-key", _
    "\gtk\share\themes\MS-Windows\gtk-2.0", _
    "\gtk\share\themes\MyStyle\gtk-2.0", _
    "\gtk\share\themes\Raleigh\gtk-2.0", _
    "\pixmaps", _
    "\pixmaps\animations", _
    "\pixmaps\buttons", _
    "\pixmaps\dialogs", _
    "\pixmaps\emblems", _
    "\pixmaps\emotes", _
    "\pixmaps\emotes\default", _
    "\pixmaps\emotes\none", _
    "\pixmaps\emotes\small", _
    "\pixmaps\kaneva", _
    "\pixmaps\protocols", _
    "\pixmaps\status", _
    "\pixmaps\toolbar", _
    "\pixmaps\tray", _
    "\pixmaps\tray\hicolor", _
    "\pixmaps\tray\hicolor\status", _
    "\GameFiles\ServerScripts" _
    )

dim title
title = "Kaneva Game Studio -- Comparing Game Files"

dim isWok
isWok = false

'==========================================================
'
' wrapper to suppress msgBox
'
'==========================================================
function showMsgBox( msg, flags, title, defaultValue )
    if isWok then
        showMsgBox = defaultValue
    else
        showMsgBox = MsgBox( msg, flags, title )
    end if
end function

'==========================================================
'
' return 0 if ok, -1 if aborted or error
'
'==========================================================
main
WScript.Quit 0


'==========================================================
'
' mainline logic
'
'==========================================================
sub main()
	set args = WScript.Arguments

	if args.Count < 4 then
		CALL showMsgBox( "Usage: CheckTemplate gameId ""destination"" ""bindir"" debug|""""" & vbCrlf & "This is usually called by KEPEdit", vbExclamation, title, vbNo )
		WScript.Quit -1
	end if

	dim gameDir
	dim binDir
	dim debugMode
	dim gameId
	debugMode = vbFalse

	gameId = args(0)
	gameDir = args(1)
	binDir = args(2)

	isWok = gameId = 3296 or gameId = 3298 or gameId = 5400 or gameId = 5354 ' prod, prev, rc, 3d

	if args(3) = "debug" then
		debugMode = vbTrue
	end if

	if isWok then ' WOK needs KDP so add two more entries into array
		ReDim Preserve copyFileList( UBound( copyFileList ) + 3 )
		copyFileList(UBound(copyFileList)-2) = Array( "..\templates\shared\GameFiles\BladeScripts\PlayListMgr.dll",       "GameFiles\BladeScripts\PlayListMgr.dll" )
		copyFileList(UBound(copyFileList)-1) = Array( "..\templates\shared\GameFiles\BladeScripts\KDP_S.dll",             "GameFiles\BladeScripts\KDP_S.dll" )
		copyFileList(UBound(copyFileList))   = Array( "..\templates\shared\ClientBlades\KDP.dll",                         "ClientBlades\KDP.dll" )
	end if

	' if debug, switch path names
	' initally small, now bigger
	if debugMode then
		for i = 0 To Ubound(copyFileList)
			copyFileList(i)(0) = Replace( copyFileList(i)(0), "ClientBlades", "ClientBladesd" )
			copyFileList(i)(1) = Replace( copyFileList(i)(1), "ClientBlades", "ClientBladesd" )

			copyFileList(i)(0) = Replace( copyFileList(i)(0), "BladeScripts", "BladeScriptsd" )
			copyFileList(i)(1) = Replace( copyFileList(i)(1), "BladeScripts", "BladeScriptsd" )

			copyFileList(i)(0) = Replace( copyFileList(i)(0), "log4cplusU", "log4cplusUD" )
			copyFileList(i)(1) = Replace( copyFileList(i)(1), "log4cplusU", "log4cplusUD" )

            ' now even debug uses release version of Python
			'copyFileList(i)(0) = Replace( copyFileList(i)(0), "python24", "python24_d" )
			'copyFileList(i)(1) = Replace( copyFileList(i)(1), "python24", "python24_d" )

			copyFileList(i)(0) = Replace( copyFileList(i)(0), "KEPUtil", "KEPUtild" )
			copyFileList(i)(1) = Replace( copyFileList(i)(1), "KEPUtil", "KEPUtild" )

			copyFileList(i)(0) = Replace( copyFileList(i)(0), "CrashReport", "CrashReportd" )
			copyFileList(i)(1) = Replace( copyFileList(i)(1), "CrashReport", "CrashReportd" )

			copyFileList(i)(0) = Replace( copyFileList(i)(0), "CrashRpt1401", "CrashRpt1401d" )
			copyFileList(i)(1) = Replace( copyFileList(i)(1), "CrashRpt1401", "CrashRpt1401d" )

			copyFileList(i)(0) = Replace( copyFileList(i)(0), "KEPClient", "KEPClientd" )
			copyFileList(i)(1) = Replace( copyFileList(i)(1), "KEPClient", "KEPClientd" )

			copyFileList(i)(0) = Replace( copyFileList(i)(0), "libcollada14dom21", "libcollada14dom21-d" )
			copyFileList(i)(1) = Replace( copyFileList(i)(1), "libcollada14dom21", "libcollada14dom21-d" )

			copyFileList(i)(0) = Replace( copyFileList(i)(0), "nvtt", "nvtt-d" )
			copyFileList(i)(1) = Replace( copyFileList(i)(1), "nvtt", "nvtt-d" )
		next
		for i = 0 To Ubound(folderList)
			folderList(i) = Replace( folderList(i), "ClientBlades", "ClientBladesd" )
			folderList(i) = Replace( folderList(i), "BladeScripts", "BladeScriptsd" )
		next
	end if

	if right(binDir,1) <> "\" then
		binDir = binDir & "\"
	end if

	if right(gameDir,1) <> "\" then
		gameDir = gameDir & "\"
	end if

	call CreateFolders( gameDir )
	call CompareAndCopy( copyFileList, gameDir, binDir, debugMode )

	call CopyWildCards( wildcardList, gameDir, binDir, debugMode )


end sub

'==========================================================
'
' for old distributions, try to create sub folders
'
'==========================================================
sub CreateFolders( gameDir )

	on error resume next

	set fso = WScript.CreateObject( "Scripting.FileSystemObject" )

	for each f in folderList
	    fso.CreateFolder( gameDir & f )
	next

	Err.Clear

end sub

'==========================================================
'
' cleanup some files that shouldn't be there
'
'==========================================================
sub CleanUp( ByRef fso, gameFolder )

	on error resume next

	call fso.DeleteFile( gameFolder & "\gamefiles\bladescripts\python24*.dll", vbTrue )
	call fso.DeleteFile( gameFolder & "\gamefiles\bladescriptsd\python24*.dll", vbTrue  )

	Err.Clear

end sub


'==========================================================
'
' copy wildcards always for now, other option is to do diff
' but doesn't add much value
'
'==========================================================
sub CopyWildCards( ByRef copyList, gameFolder, binFolder, debugMode )

	' read the XML file and get additional gamefile files
	dim additionalFiles
	dim xmlPath
	dim fso
	dim xd
	dim fc
	dim folder

	on error resume next

	set fso = WScript.CreateObject( "Scripting.FileSystemObject" )

	dim msg
	msg = ""

	' update them
	for each f in copyList

		' make all files r/w
		set folder = fso.GetFolder( gameFolder & f(1) )
		if Err = 0 then
			Set fc = folder.Files
			For Each f1 in fc
				' clear r/o bit
				dim flags
				flags = f1.Attributes
				' clear the r/o flag if set
				if (flags And 1) = 1 then
					f1.Attributes = flags And &Hfffffffe
				end if
			Next
		else
			Err.Clear
			set folder = fso.GetFolder( gameFolder )
			if Err = 0 then
				folder.SubFolders.Add f(1)
			end if
		end if

		' copy using mask
		call fso.CopyFile( binFolder & f(0), gameFolder & f(1) )
		if Err <> 0 then
			if showMsgBox( "Error copying files: " & binFolder & f(0) &  " -> " & gameFolder & f(1)  & vbCrlf & "Error: " & Err.Description & " (" & Err & "). " & vbCrLf & "Do you want to continue?", vbQuestion or vbYesNo, title, vbYes ) = vbNo then
				WScript.Quit -1
			end if
            Err.Clear
		end if
	next

end sub



'==========================================================
'
' creating the distribution
'
'==========================================================
sub CompareAndCopy( ByRef copyList, gameFolder, binFolder, debugMode )

	' read the XML file and get additional gamefile files
	dim additionalFiles
	dim xmlPath
	dim fso
	dim xd

	on error resume next

	set fso = WScript.CreateObject( "Scripting.FileSystemObject" )

	dim msg
	msg = ""

	call cleanUp( fso, gameFolder )

	for each f in copyList
		' compare the timestamp and sizes
		dim templateFile
		dim gameFile
		' WScript.Echo "Looking for "& binFolder & "\..\templates" & f(1)

		if Instr(f(0), "*") <> 0 then 'if wildcards, check folder dest
			set gameFile = fso.GetFolder( gameFolder & f(1) )
			if Err <> 0 then
				WScript.Echo "Got error" & Err.ErrNum
				'set gameFile = nothing
				msg = msg & vbCrlf & f(1) & " missing"
				Err.Clear
			end if
		else
			set gameFile = fso.GetFile( gameFolder & f(1) )
			if Err <> 0 then
				WScript.Echo "Got error" & Err.ErrNum
				'set gameFile = nothing
				msg = msg & vbCrlf & f(1) & " missing"
				Err.Clear
			else
				set templateFile = fso.GetFile( binFolder & f(0) )
				if Err <> 0 then
					call showMsgBox( "Error looking for template file: " & binFolder & f(0) & vbCrlf & "Error: " & Err.Description, vbExclamation, title, vbNo )
					WScript.Quit -1
				end if

				dim flags
				flags = gameFile.Attributes
				' clear the r/o flag if set
				if (flags And 1) = 1 then
					gameFile.Attributes = flags And &Hfffffffe
				end if

				if not templateFile is nothing and not gameFile is nothing then
					if templateFile.DateLastModified > gameFile.DateLastModified then
						msg = msg & vbCrlf & f(1) & " " & templateFile.DateLastModified & " >  " & gameFile.DateLastModified
					elseif templateFile.Size <> gameFile.Size then
						msg = msg & vbCrlf & f(1) & " " & templateFile.Size & " <>  " & gameFile.Size
					end if
				end if
			end if
		end if
	next
	on error goto 0
	if len(msg) > 0 then
		msg =  "The files in the game directory appear to be old for the reasons below" & vbCrlf & "Do you want to update them now?" & vbCrlf & vbCrlf & msg
		if showMsgBox( msg, vbYesNo or vbInformation, title, vbYes ) = vbYes then
			' update them
			for each f in copyList
				call fso.CopyFile( binFolder & f(0), gameFolder & f(1) )
				if Err <> 0 then
					if showMsgBox( "Error copying files: " & binFolder & f(0) &  " -> " & gameFolder & f(1)  & vbCrlf & "Error: " & Err.Description & " (" & Err & "). " & vbCrLf & "Do you want to continue?", vbQuestion or vbYesNo, title, vbYes ) = vbNo then
						WScript.Quit -1
					end if
                    Err.Clear
				end if
			next
			call showMsgBox( "Update Complete.  Load will now continue.", vbInformation, title, vbNo )
		else
			if debugMode then
				if showMsgBox( "Since you're in debug, do you want to continue loading?", vbYesNo Or vbDefaultButton2 Or vbQuestion, title, vbYes ) = vbYes then
					WScript.Quit 0
				end if
			end if
			WScript.Quit -1 ' they said no
		end if
	else
		if debugMode then
		' enough of this annoying message
		'	call MsgBox( "All binary files are current.", vbInformation, title )
		end if
	end if

end sub


