## Dependencies ##

* Microsoft SDKs
    * Windows SDK (Windows Kits 8.x)
    * MSVCRT 14.0
    * MFC ?
    * DirectX SDK 9.0 October 2004
* Third-party Libraries
    * MySQL 5.0.x SDK (server only, **GPL!!**)
    * OpenAL 1.1 SDK
    * AES (**obsolete**)
    * boost (**dependency to be removed**)
    * bullet3 (https://github.com/bulletphysics/bullet3)
    * colladadom 2.1.1 (https://sourceforge.net/projects/collada-dom/, to be upgraded if possible)
    * crashrpt (**obsolete**)
    * Erlang (**obsolete**)
    * f_in_box-4.4.0.0 (**proprietary**)
    * FreeImage (http://freeimage.sourceforge.net/)
    * GA (Google Analytics, **obsolete**)
    * GDIPlus (**obsolete**)
    * googletest
    * Incubator (**obsolete**)
    * Intel IPP (**obsolete**, replace with MS SIMD functions?)
    * jsTools (...)
    * KEPImageButton (**obsolete**)
    * KSwfPlayer (**obsolete**)
    * libcpuid (**obsolete**)
    * libjpeg-turbo (https://github.com/libjpeg-turbo/libjpeg-turbo)
    * libmad (**obsolete**)
    * libogg (https://www.xiph.org/downloads/)
    * libvorbis (https://www.xiph.org/downloads/)
    * log4cplus (https://github.com/log4cplus/log4cplus)
    * lua_cjson (https://github.com/mpx/lua-cjson)
    * luacoco (http://coco.luajit.org/)
    * lua 5.1 (https://www.lua.org/)
    * lzma (https://www.7-zip.org/sdk.html)
    * madlldlib (**obsolete**)
    * mongoose (https://github.com/cesanta/mongoose)
    * mysqlpp (https://tangentsoft.com/mysqlpp/home)
    * nasm (**obsolete**)
    * NLSTree (**obsolete**)
    * NSIS
    * NTService (**to be merged into core**)
    * nvtt (https://github.com/castano/nvidia-texture-tools)
    * pidgin-buildenv (**obsolete**)
    * python 2.4 (https://www.python.org/downloads/release/python-246/, **upgrade?**)
    * rabbitmq-c (https://github.com/alanxz/rabbitmq-c)
    * raknet (https://github.com/facebookarchive/RakNet)
    * SimpleAmqpClient (https://github.com/alanxz/SimpleAmqpClient)
    * SizeCBar (https://www.codeproject.com/Articles/6/CSizingControlBar-A-Resizable-Control-Bar, **obsolete or merge into core**)
    * SmartPropertyGrid (proprietary, **obsolete**)
    * syslog (**obsolete**)
    * TBB (Intel Thread Building Blocks, **GPL!!**, where do we use it?)
    * TinyXML (https://sourceforge.net/projects/tinyxml/, **project inactive, alternative?**)
    * TSWizards (https://www.codeproject.com/Articles/2911/TSWizard-a-wizard-framework-for-NET, **obsolete?**)
    * txbconv (ours, **probably obsolete**)
    * VisualLeakDetector (drop for now, reintegrate later)
    * vld (duplicate of VisualLeakDetector)
    * wkglib (merge into core for now, **to be replaced**)
    * Xceed (**obsolete**)
    * Xerces (**obsolete**)