-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `kaneva`.`marketing_share_referrals` (
  `existing_user_id` INTEGER UNSIGNED NOT NULL,
  `referred_user_id` INTEGER UNSIGNED NOT NULL,
  `registered_date` DATETIME NOT NULL,
  `completed_date` DATETIME NOT NULL,
  PRIMARY KEY (`referred_user_id`, `existing_user_id`)
)
ENGINE = InnoDB;


CREATE TABLE `kaneva`.`marketing_share_tracking` (
  `user_id` INTEGER UNSIGNED NOT NULL,
  `number_of_views` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `number_of_clics` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`user_id`)
)
ENGINE = InnoDB;


ALTER TABLE `kaneva`.`promotional_offers` ADD COLUMN `coupon_code` VARCHAR(45) AFTER `special_font_color`;

INSERT INTO promotional_offer_type (promotional_offer_type_id, promotional_offer_type, description)
VALUES (5, 'Referral', 'Referral Incentive Package');


