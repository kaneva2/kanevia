-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS `wok`.`report_login_cohort`;
DELIMITER ;;
CREATE PROCEDURE `wok`.`report_login_cohort`(__starting_week TINYINT unsigned, __starting_year SMALLINT)
BEGIN
DECLARE __ending_week TINYINT;
SET __ending_week = WEEK(NOW()) - 1;

CREATE TEMPORARY TABLE IF NOT EXISTS __login_cohort_temp
			(user_count SMALLINT,
			report_week TINYINT,
			started_week TINYINT);
			
WHILE __starting_week <= __ending_week 
	DO
	INSERT INTO __login_cohort_temp
			(user_count, report_week, started_week)
		SELECT count(distinct ll.user_id), WEEK(ll.created_date), __starting_week
			FROM wok.login_log ll
			INNER JOIN wok.game_users gu on ll.user_id = gu.user_id
			WHERE YEAR(gu.created_date) = __starting_year 
						and WEEK(gu.created_date) = __starting_week
						and YEAR(ll.created_date) = __starting_year
						and WEEK(ll.created_date) = (WEEK(NOW()) - 1)
			GROUP by WEEK(ll.created_date);
		
	SET __starting_week = __starting_week + 1;
	IF  __starting_week = 53
		THEN
		SET __starting_week = 1;
		SET __starting_year = __starting_year + 1;
		END IF;
	END WHILE;

SELECT * FROM __login_cohort_temp;

DROP TABLE IF EXISTS __login_cohort_temp;
END;;
DELIMITER ;
