/*      
MONyog - MySQL Monitor and Advisor
Copyright (c) 2009 Webyog Softworks Private Limited (http://www.webyog.com)
*/
obj = {
	Group       	: "Current Connections",
	Name        	: "Percentage of max allowed reached",
	Formula     	: "Max_used_connections / max_connections",
	GroupFunction	: "Avg",
	Value			: function ()
	{
	    if(MONyog.MySQL.Custom.Available != 1)
	            return "(n/a)";
	            
	    return (SafeDiv0(MONyog.MySQL.GlobalStatus.Max_used_connections,MONyog.MySQL.GlobalVariables.max_connections)*100).toFixed(2)+ "%";
    },
	Chart       	: "Yes",
	Barchart    	: "Yes",
	Warning     	: 105,
	Critical    	: 115,
	MailAlert   	: "Yes",
	AlertCondition     : function() 
	{ 
		return GetWarnStatusInt(this.Value, this.Critical, this.Warning, true);
	},
	DocText     	: "A high value indicates that you may run out of connections " +
	            	  "soon and new clients will be refused connection",
	AdviceText  	: "1. Make sure that you have a reasonable high value for max_connections.<br>" +
                	  "2. Design your application such that a MySQL connection is " + 
                	  "kept open for a very short period of time.<br>" +
                	  "3. Try pooling connections or switch to persistent connections " + 
                	  "to reduce the number of active MySQL connections. " + 
                	  "For example try using mysql_pconnect() instead of mysql_connect()"
}
_ALL_COUNTERS.push(obj);

obj = {
	Group       	: "Current Connections",
	Name        	: "",
	Value       	: ""
}
_ALL_COUNTERS.push(obj);
