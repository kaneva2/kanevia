-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS AwardFame;

DELIMITER ;;
CREATE PROCEDURE `AwardFame`(in packetId INT, in userId INT, OUT return_code INT, OUT levelId INT)
BEGIN

  -- Packet Info
  DECLARE _fame_type_id INT;
  DECLARE _points_earned INT;
  DECLARE _rewards INT;
  DECLARE _timeInterval INT;
  DECLARE _transaction_type SMALLINT;

  -- User Fame
  DECLARE _currentLevelId INT;
  DECLARE _currentPoints INT;

  -- Given reward
  DECLARE _returnCode INT;
  DECLARE _userBalance float;
  DECLARE _tranId INT;

  DECLARE _tempLevel INT;

  SET return_code = 0;
  SET levelId = 0;

  -- Get the Fame Packet
  SELECT fame_type_id, points_earned, rewards, time_interval, transaction_type 
	INTO _fame_type_id, _points_earned, _rewards, _timeInterval, _transaction_type 
	FROM packets WHERE packet_id = packetId LIMIT 1;

  -- Check for valid Packet
  IF _points_earned IS NULL THEN
      -- Packet was not found
      SET return_code = -3;

  ELSE
    -- Check thier fame level
    SELECT level_id, points INTO _currentLevelId, _currentPoints FROM user_fame WHERE user_id = userId AND fame_type_id = _fame_type_id LIMIT 1;

    -- Give them fame points
    IF _currentPoints IS NULL THEN
      INSERT INTO user_fame (user_id, fame_type_id, level_id, points) VALUES (userId, _fame_type_id, 0, _points_earned);
      SET _currentLevelId = 0;
    ELSE
      SET levelId = _currentLevelId;
      UPDATE user_fame SET points = points + _points_earned WHERE user_id = userId AND fame_type_id = _fame_type_id;
    END IF;

    -- Record to History Tables
    INSERT INTO packet_history (user_id, fame_type_id, packet_id, date_rewarded, points_awarded, rewards_awarded) VALUES (userId, _fame_type_id, packetId, NOW(), _points_earned, _rewards);

    IF _timeInterval = 1 THEN
      INSERT INTO onetime_history (user_id, fame_type_id, packet_id, date_rewarded) VALUES (userId, _fame_type_id, packetId, NOW());
    END IF;

    -- Add the rewards
     if _rewards > 0 THEN
       CALL kaneva.apply_transaction_to_user_balance(userId, _rewards, _transaction_type, 'GPOINT', @returnCode, @userBalance, @tranId);
     END IF;

    -- Set new level if they leveled up
    CALL LevelUp (userId, _fame_type_id, _currentLevelId, @levelId);
    SELECT @levelId INTO levelId;

  END IF;




END
;;
DELIMITER ;