-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

INSERT INTO `fame_type` (`fame_type_id`,`name`) 
VALUES 
(1,'World Fame'),
(2,'Dance Fame');


INSERT INTO `levels` (`fame_type_id`,`level_id`,`level_number`,`start_points`,`end_points`,`rewards`) 
VALUES 
(1,0,0,0,5000,0),
(1,1,1,5000,10550,0),
(1,2,2,10550,16698,0),
(1,3,3,16698,23495,0),
(1,4,4,23495,30993,0),
(1,5,5,30993,39249,0),
(1,6,6,39249,48320,0),
(1,7,7,48320,58266,0),
(1,8,8,58266,69150,0),
(1,9,9,69150,81035,0),
(1,10,10,81035,93992,0),
(1,11,11,93992,108094,0),
(1,12,12,108094,123418,0),
(1,13,13,123418,140041,0),
(1,14,14,140041,158044,0),
(1,15,15,158044,177509,0),
(1,16,16,177509,198520,0),
(1,17,17,198520,221160,0),
(1,18,18,221160,245517,0),
(1,19,19,245517,271676,0),
(1,20,20,271676,299724,0),
(1,21,21,299724,329747,0),
(1,22,22,329747,361829,0),
(1,23,23,361829,396054,0),
(1,24,24,396054,432504,0),
(1,25,25,432504,471258,0),
(1,26,26,471258,512390,0),
(1,27,27,512390,555975,0),
(1,28,28,555975,602079,0),
(1,29,29,602079,650764,0),
(1,30,30,650764,702088,0),
(1,31,31,702088,756102,0),
(1,32,32,756102,812848,0),
(1,33,33,812848,872364,0),
(1,34,34,872364,934677,0),
(1,35,35,934677,999807,0),
(1,36,36,999807,1067763,0),
(1,37,37,1067763,1138547,0),
(1,38,38,1138547,1212147,0),
(1,39,39,1212147,1288545,0),
(1,40,40,1288545,1367707,0),
(1,41,41,1367707,1449593,0),
(1,42,42,1449593,1534149,0),
(1,43,43,1534149,1621309,0),
(1,44,44,1621309,1710996,0),
(1,45,45,1710996,1803123,0),
(1,46,46,1803123,1897591,0),
(1,47,47,1897591,1994287,0),
(1,48,48,1994287,2093091,0),
(1,49,49,2093091,2193872,0),
(1,50,50,2193872,9000000,0);

INSERT INTO `packets` (`fame_type_id`,`packet_id`,`name`,`points_earned`,`rewards`,`time_interval`,`reward_interval`)
VALUES
(1,1,'WEB - add profile picture',1336,84,1,0),
(1,2,'WORLD - first time login after creating avatar',1503,95,1,0),
(1,3,'WORLD - go thru inworld tutorial',1411,89,1,0),
(1,4,'WEB - first time visit to level help page',1336,84,1,0),
(1,5,'WORLD - place object in home',1044,66,1,0),
(1,6,'WORLD - changing clothing ',847,53,1,0),
(1,7,'WORLD - change pattern on walls',847,53,1,0),
(1,8,'WORLD - change media on tv -first time',1411,89,1,0),
(1,9,'WEB - add profile theme background',1129,71,1,0),
(1,10,'WEB - Upload First You-Tube',1336,84,1,0),
(1,11,'BOTH � newbie tasks achievement',4500,283,1,0),
(1,12,'WEB - 10 Friend-invited Achievement',2500,210,1,10),
(1,13,'WEB - 25 Friend-invited Achievement',5500,525,1,25),
(1,14,'WEB - 50 Friend-invited Achievement',12000,1050,1,50),
(1,15,'WEB/WORLD - Add 5 Friends',1200,74,1,5),
(1,16,'WORLD - Use first hangout deed',2000,105,1,0),
(1,17,'DANCE FLOOR � Unique members visit (1,every 10)',300,6,2,10),
(1,18,'DANCE FLOOR - Unique members visit (1,every 50)',2500,53,2,50),
(1,19,'HOME - Unique members visit (1,every 10)',300,6,2,10),
(1,20,'HOME - Unique members visit (1,every 50)',2500,53,2,50),
(1,21,'HANGOUT - Unique members visited (1,every 10)',300,6,2,10),
(1,22,'HANGOUT - Unique members visited (1,every 50)',2500,53,2,50),
(1,23,'RAVED - Home (1,every 10x)',200,4,2,10),
(1,24,'RAVED � Profile',0,0,2,10),
(1,25,'RAVED � Hangout (1,every 10x)',200,4,2,10),
(1,26,'RAVED � Community (1,every 10x)',0,0,2,10),
(1,27,'Give a Gift',900,19,2,0),
(1,28,'RAVED - patterns (1,every 10x)',100,2,2,10),
(1,29,'RAVED - photo (1,every 10x)',100,2,2,10),
(1,30,'RAVED - video (1,every 10x)',100,2,2,10),
(1,31,'COMMUNITY � members join (1,every 5)',667,14,2,5),
(1,32,'INVITES - join onsite (1,every 5)',0,0,2,5),
(1,33,'INVITES - go inworld (1,every 5)',0,0,2,5),
(1,34,'INVITES � spend credits (1,every 5)',0,0,2,5),
(1,35,'Daily Signin World',500,11,3,0),
(1,36,'More than 10 minutes in world daily',500,11,3,10),
(1,37,'More than 3 hours in world daily',700,15,3,180),
(1,38,'Dance to level 10 daily',700,15,3,10),
(1,39,'Daily Signin Web',500,11,3,0),
(1,40,'Visit Kaneva Forum daily',300,6,3,0),
(1,41,'Read 3 Kaneva Forum Posts daily',400,8,3,3),
(1,42,'View 3 Media Details pages daily',400,8,3,3),
(1,43,'MyKaneva Slot Machine',0,0,3,0);

--Newbie checklist
INSERT INTO `nudge_checklists` (`nudge_checklist_id`,`name`,`reward_packet_id`)
VALUES
(1,'Newbie Checklist',11);

INSERT INTO `nudge_checklist_packets` (`nudge_checklist_id`,`packet_id`, `sort_order`)
VALUES
(1,1,1),
(1,2,2),
(1,3,3),
(1,4,4),
(1,5,5),
(1,6,6),
(1,7,7),
(1,8,8),
(1,9,9),
(1,10,10);