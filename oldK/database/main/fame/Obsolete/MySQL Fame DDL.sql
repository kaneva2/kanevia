-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

--
-- ER/Studio 7.5 SQL Code Generation
-- Company :      Kaneva
-- Project :      Fame.DM1
-- Author :       Kaneva
--
-- Date Created : Monday, March 03, 2008 10:38:24
-- Target DBMS : MySQL 5.x
--

DROP TABLE fame_type
;
DROP TABLE level_awards
;
DROP TABLE level_history
;
DROP TABLE levels
;
DROP TABLE nudge_checklist_packets
;
DROP TABLE nudge_checklists
;
DROP TABLE nudges
;
DROP TABLE onetime_history
;
DROP TABLE packet_history
;
DROP TABLE packets
;
DROP TABLE user_fame
;
-- 
-- TABLE: fame_type 
--

CREATE TABLE fame_type(
    fame_type_id    INT             AUTO_INCREMENT,
    name            VARCHAR(100)    NOT NULL,
    PRIMARY KEY (fame_type_id)
)ENGINE=INNODB
;



-- 
-- TABLE: level_awards 
--

CREATE TABLE level_awards(
    level_award_id    INT             NOT NULL,
    level_id          INT             NOT NULL,
    global_id         INT             NOT NULL,
    animation_id      INT             DEFAULT 0 NOT NULL,
    gender            CHAR(1)         DEFAULT 'U',
    quantity          INT,
    new_title         VARCHAR(100),
    PRIMARY KEY (level_award_id)
)ENGINE=INNODB
;



-- 
-- TABLE: level_history 
--

CREATE TABLE level_history(
    user_id             INT         NOT NULL,
    fame_type_id        INT         NOT NULL,
    level_id            INT         NOT NULL,
    level_date          DATETIME    NOT NULL,
    seconds_to_level    BIGINT      NOT NULL,
    rewards_awarded      INT         DEFAULT 0 NOT NULL,
    PRIMARY KEY (user_id, fame_type_id, level_id)
)ENGINE=INNODB
;



-- 
-- TABLE: levels 
--

CREATE TABLE levels(
    level_id        INT    AUTO_INCREMENT,
    fame_type_id    INT    NOT NULL,
    level_number    INT    NOT NULL,
    start_points    INT    NOT NULL,
    end_points      INT    NOT NULL,
    rewards         INT    DEFAULT 0 NOT NULL,
    PRIMARY KEY (level_id)
)ENGINE=INNODB
;



-- 
-- TABLE: nudge_checklist_packets 
--

CREATE TABLE nudge_checklist_packets(
    packet_id             INT    NOT NULL,
    nudge_checklist_id    INT    NOT NULL,
    sort_order            INT    NOT NULL,
    PRIMARY KEY (packet_id, nudge_checklist_id)
)ENGINE=INNODB
;



-- 
-- TABLE: nudge_checklists 
--

CREATE TABLE nudge_checklists(
    nudge_checklist_id    INT             AUTO_INCREMENT,
    name                  VARCHAR(100)    NOT NULL,
    reward_packet_id      INT             NOT NULL,
    PRIMARY KEY (nudge_checklist_id)
)ENGINE=INNODB
;



-- 
-- TABLE: nudges 
--

CREATE TABLE nudges(
    nudge_id     INT             AUTO_INCREMENT,
    packet_id    INT             NOT NULL,
    name         VARCHAR(100),
    impressions  INT             DEFAULT 0 NOT NULL,
    PRIMARY KEY (nudge_id)
)ENGINE=INNODB
;



-- 
-- TABLE: onetime_history 
--

CREATE TABLE onetime_history(
    user_id           INT         NOT NULL,
    fame_type_id      INT         NOT NULL,
    packet_id         INT         NOT NULL,
    date_rewarded    DATETIME    NOT NULL,
    PRIMARY KEY (user_id, fame_type_id, packet_id)
)ENGINE=INNODB
;



-- 
-- TABLE: packet_history 
--

CREATE TABLE packet_history(
    packet_history_id    BIGINT      AUTO_INCREMENT,
    user_id              INT         NOT NULL,
    packet_id            INT         NOT NULL,
    date_rewarded        DATETIME    NOT NULL,
    points_awarded       INT         DEFAULT 0 NOT NULL,
    rewards_awarded      INT         DEFAULT 0 NOT NULL,
    fame_type_id         INT         NOT NULL,
    PRIMARY KEY (packet_history_id)
)ENGINE=INNODB
;



-- 
-- TABLE: packets 
--

CREATE TABLE packets(
    packet_id          INT             AUTO_INCREMENT,
    fame_type_id       INT             NOT NULL,
    name               VARCHAR(100)    NOT NULL,
    points_earned      INT             NOT NULL,
    rewards            INT             NOT NULL,
    time_interval      INT             NOT NULL,
    reward_interval    INT             NOT NULL,
    PRIMARY KEY (packet_id)
)ENGINE=INNODB
;



-- 
-- TABLE: user_fame 
--

CREATE TABLE user_fame(
    user_id         INT    NOT NULL,
    fame_type_id    INT    NOT NULL,
    level_id        INT    NOT NULL,
    points          INT    DEFAULT 0 NOT NULL,
    PRIMARY KEY (user_id, fame_type_id)
)ENGINE=INNODB
;


-- 
-- TABLE: checklist_completions 
--

CREATE TABLE checklist_completions(
    user_id         INT    NOT NULL,
    nudge_checklist_id    INT    NOT NULL,
    date_completed        DATETIME    NOT NULL,
    PRIMARY KEY (user_id, nudge_checklist_id)
)ENGINE=INNODB
;


-- 
-- TABLE: level_awards 
--

ALTER TABLE level_awards ADD CONSTRAINT Reflevels16 
    FOREIGN KEY (level_id)
    REFERENCES levels(level_id)
;


-- 
-- TABLE: level_history 
--

ALTER TABLE level_history ADD CONSTRAINT Reflevels14 
    FOREIGN KEY (level_id)
    REFERENCES levels(level_id)
;

ALTER TABLE level_history ADD CONSTRAINT Refuser_fame15 
    FOREIGN KEY (user_id, fame_type_id)
    REFERENCES user_fame(user_id, fame_type_id)
;


-- 
-- TABLE: levels 
--

ALTER TABLE levels ADD CONSTRAINT Reffame_type33 
    FOREIGN KEY (fame_type_id)
    REFERENCES fame_type(fame_type_id)
;


-- 
-- TABLE: nudge_checklist_packets 
--

ALTER TABLE nudge_checklist_packets ADD CONSTRAINT Refnudge_checklists1 
    FOREIGN KEY (nudge_checklist_id)
    REFERENCES nudge_checklists(nudge_checklist_id)
;

ALTER TABLE nudge_checklist_packets ADD CONSTRAINT Refpackets2 
    FOREIGN KEY (packet_id)
    REFERENCES packets(packet_id)
;


-- 
-- TABLE: nudges 
--

ALTER TABLE nudges ADD CONSTRAINT Refpackets18 
    FOREIGN KEY (packet_id)
    REFERENCES packets(packet_id)
;


-- 
-- TABLE: onetime_history 
--

ALTER TABLE onetime_history ADD CONSTRAINT Refuser_fame21 
    FOREIGN KEY (user_id, fame_type_id)
    REFERENCES user_fame(user_id, fame_type_id)
;

ALTER TABLE onetime_history ADD CONSTRAINT Refpackets22 
    FOREIGN KEY (packet_id)
    REFERENCES packets(packet_id)
;


-- 
-- TABLE: packet_history 
--

ALTER TABLE packet_history ADD CONSTRAINT Refuser_fame6 
    FOREIGN KEY (user_id, fame_type_id)
    REFERENCES user_fame(user_id, fame_type_id)
;

ALTER TABLE packet_history ADD CONSTRAINT Refpackets9 
    FOREIGN KEY (packet_id)
    REFERENCES packets(packet_id)
;


-- 
-- TABLE: packets 
--

ALTER TABLE packets ADD CONSTRAINT Reffame_type32 
    FOREIGN KEY (fame_type_id)
    REFERENCES fame_type(fame_type_id)
;


-- 
-- TABLE: user_fame 
--

ALTER TABLE user_fame ADD CONSTRAINT Reflevels5 
    FOREIGN KEY (level_id)
    REFERENCES levels(level_id)
;

ALTER TABLE user_fame ADD CONSTRAINT Reffame_type29 
    FOREIGN KEY (fame_type_id)
    REFERENCES fame_type(fame_type_id)
;


-- More changes
ALTER TABLE `fame`.`nudge_checklist_packets` ADD COLUMN `name` VARCHAR(100) NOT NULL AFTER `sort_order`;