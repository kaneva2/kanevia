-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use fame;

-- move data
\. fame_SeedData_v50.sql

DROP PROCEDURE IF EXISTS fame_PromotePolicy_50;
DELIMITER //
CREATE PROCEDURE fame_PromotePolicy_50()
BEGIN
IF NOT EXISTS (SELECT 1 FROM schema_versions WHERE version = 50) THEN
	INSERT INTO `schema_versions` ( `version`, `description` ) 
		VALUES (50, '30-minutes, sprint 2, updating data in fame_type, levels and packets' );
END IF;

END //

DELIMITER ;

CALL fame_PromotePolicy_50;
DROP PROCEDURE IF EXISTS fame_PromotePolicy_50;

