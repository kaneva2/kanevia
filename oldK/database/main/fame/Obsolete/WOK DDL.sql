-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TABLE if exists wok.zone_unique_visits;

DROP TABLE IF EXISTS `wok`.`zone_unique_visits`;
CREATE TABLE  `wok`.`zone_unique_visits` (
  `kaneva_user_id` int(11) unsigned NOT NULL default '0' COMMENT 'User that visited zone',
  `zone_instance_id` int(11) unsigned NOT NULL default '0' COMMENT 'FK to channel_zones. Community_id or player_id',
  `zone_type` int(11) unsigned NOT NULL default '0' COMMENT 'FK to channel_zones',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`kaneva_user_id`,`zone_instance_id`, `zone_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `wok`.`channel_zones` ADD COLUMN `number_unique_visits` INTEGER UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Number of unique Visits to the zone' AFTER `last_update`;


DROP TABLE if exists wok.daily_dance_levels;
CREATE TABLE  `wok`.`daily_dance_levels` (
  `user_id` int(11) unsigned NOT NULL default '0' COMMENT 'User that danced to level',
  `level_id` int(11) unsigned NOT NULL default '0' COMMENT 'Level danced to',
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`user_id`,`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;