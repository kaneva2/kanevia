-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TABLE if exists kaneva.forum_visits;
DROP TABLE if exists kaneva.forum_post_views;

CREATE TABLE  `kaneva`.`forum_visits` (
  `user_id` int(11) unsigned NOT NULL default '0' COMMENT 'User that visited forum',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`user_id`,`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE  `kaneva`.`forum_post_views` (
  `user_id` int(11) unsigned NOT NULL default '0' COMMENT 'User that visited forum post',
  `number_of_views` int(11) unsigned NOT NULL default '0' COMMENT 'Number of posts viewed',
  `created_date` date NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`user_id`,`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
