-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS fame_SeedData_50;
DELIMITER //
CREATE PROCEDURE fame_SeedData_50()
BEGIN

-- add row to fame_type
IF NOT EXISTS (SELECT 1 FROM fame_type WHERE fame_type_id = 3) THEN
	INSERT INTO `fame_type` ( `fame_type_id`, `name` ) 
		VALUES (3,'Kaching Fame' );
END IF;

-- add kaching levels 
IF NOT EXISTS (SELECT 1 FROM levels WHERE level_id = 111) THEN
	INSERT INTO `levels` (`level_id`, `fame_type_id`, `level_number`, `start_points`, `end_points`, `rewards`) 
	VALUES('111', '3', '1', '21500', '59100', '0'),
		('121', '3', '2', '59100', '108900', '0'),
		('131', '3', '3', '108900', '168000', '0'),
		('141', '3', '4', '168000', '234200', '0'),
		('151', '3', '5', '234200', '305900', '0'),
		('161', '3', '6', '305900', '381900', '0'),
		('591', '3', '49', '24921800', '28844400', '0'),
		('581', '3', '48', '21602900', '24921800', '0'),
		('571', '3', '47', '18790900', '21602900', '0'),
		('561', '3', '46', '16404800', '18790900', '0'),
		('551', '3', '45', '14376800', '16404800', '0'),
		('541', '3', '44', '12650100', '14376800', '0'),
		('531', '3', '43', '11177000', '12650100', '0'),
		('521', '3', '42', '9917500', '11177000', '0'),
		('511', '3', '41', '8838000', '9917500', '0'),
		('501', '3', '40', '7910300', '8838000', '0'),
		('491', '3', '39', '7110800', '7910300', '0'),
		('481', '3', '38', '6419600', '7110800', '0'),
		('471', '3', '37', '5819900', '6419600', '0'),
		('461', '3', '36', '5297600', '5819900', '0'),
		('451', '3', '35', '4840800', '5297600', '0'),
		('441', '3', '34', '4439500', '4840800', '0'),
		('431', '3', '33', '4085200', '4439500', '0'),
		('421', '3', '32', '3770800', '4085200', '0'),
		('411', '3', '31', '3490300', '3770800', '0'),
		('401', '3', '30', '3238600', '3490300', '0'),
		('391', '3', '29', '3011400', '3238600', '0'),
		('381', '3', '28', '2805100', '3011400', '0'),
		('371', '3', '27', '2616600', '2805100', '0'),
		('361', '3', '26', '2443200', '2616600', '0'),
		('351', '3', '25', '2282700', '2443200', '0'),
		('341', '3', '24', '2133300', '2282700', '0'),
		('331', '3', '23', '1993300', '2133300', '0'),
		('321', '3', '22', '1861400', '1993300', '0'),
		('311', '3', '21', '1736500', '1861400', '0'),
		('301', '3', '20', '1617600', '1736500', '0'),
		('291', '3', '19', '1503800', '1617600', '0'),
		('281', '3', '18', '1394400', '1503800', '0'),
		('271', '3', '17', '1288900', '1394400', '0'),
		('261', '3', '16', '1186800', '1288900', '0'),
		('251', '3', '15', '1087700', '1186800', '0'),
		('241', '3', '14', '991300', '1087700', '0'),
		('231', '3', '13', '897300', '991300', '0'),
		('221', '3', '12', '805600', '897300', '0'),
		('211', '3', '11', '716100', '805600', '0'),
		('201', '3', '10', '628800', '716100', '0'),
		('191', '3', '9', '543800', '628800', '0'),
		('181', '3', '8', '461400', '543800', '0'),
		('171', '3', '7', '381900', '461400', '0'),
		('601', '3', '50', '28844400', '30000000', '0');
END IF;

UPDATE `levels` SET `fame_type_id`='1', `level_number`='7', `start_points`='315000', `end_points`='360000', `rewards`='100'  WHERE (`level_id` = 7) ;
UPDATE `levels` SET `fame_type_id`='1', `level_number`='6', `start_points`='270000', `end_points`='315000', `rewards`='100'  WHERE (`level_id` = 6) ;
UPDATE `levels` SET `fame_type_id`='1', `level_number`='5', `start_points`='225000', `end_points`='270000', `rewards`='100'  WHERE (`level_id` = 5) ;
UPDATE `levels` SET `fame_type_id`='1', `level_number`='4', `start_points`='180000', `end_points`='225000', `rewards`='100'  WHERE (`level_id` = 4) ;
UPDATE `levels` SET `fame_type_id`='1', `level_number`='3', `start_points`='135000', `end_points`='180000', `rewards`='100'  WHERE (`level_id` = 3) ;
UPDATE `levels` SET `fame_type_id`='1', `level_number`='2', `start_points`='90000', `end_points`='135000', `rewards`='100'  WHERE (`level_id` = 2) ;
UPDATE `levels` SET `fame_type_id`='1', `level_number`='1', `start_points`='45000', `end_points`='90000', `rewards`='100'  WHERE (`level_id` = 1) ;
UPDATE `levels` SET `fame_type_id`='1', `level_number`='8', `start_points`='360000', `end_points`='405000', `rewards`='100'  WHERE (`level_id` = 8) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='0', `start_points`='0', `end_points`='21500', `rewards`='0'  WHERE (`level_id` = 101) ;
UPDATE `levels` SET `fame_type_id`='1', `level_number`='0', `start_points`='0', `end_points`='45000', `rewards`='100'  WHERE (`level_id` = 0) ;

-- level changes (12/30/08 update)
IF NOT EXISTS (SELECT 1 FROM levels WHERE level_id = 101) THEN
	INSERT INTO `levels` (`level_id`, `fame_type_id`, `level_number`, `start_points`, `end_points`, `rewards`) 
		VALUES('101', '3', '0', '0', '21500', '0');
END IF;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='48', `start_points`='21602900', `end_points`='24921800', `rewards`='0'  WHERE (`level_id` = 581) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='47', `start_points`='18790900', `end_points`='21602900', `rewards`='0'  WHERE (`level_id` = 571) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='46', `start_points`='16404800', `end_points`='18790900', `rewards`='0'  WHERE (`level_id` = 561) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='45', `start_points`='14376800', `end_points`='16404800', `rewards`='0'  WHERE (`level_id` = 551) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='44', `start_points`='12650100', `end_points`='14376800', `rewards`='0'  WHERE (`level_id` = 541) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='43', `start_points`='11177000', `end_points`='12650100', `rewards`='0'  WHERE (`level_id` = 531) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='42', `start_points`='9917500', `end_points`='11177000', `rewards`='0'  WHERE (`level_id` = 521) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='41', `start_points`='8838000', `end_points`='9917500', `rewards`='0'  WHERE (`level_id` = 511) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='40', `start_points`='7910300', `end_points`='8838000', `rewards`='0'  WHERE (`level_id` = 501) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='39', `start_points`='7110800', `end_points`='7910300', `rewards`='0'  WHERE (`level_id` = 491) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='38', `start_points`='6419600', `end_points`='7110800', `rewards`='0'  WHERE (`level_id` = 481) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='37', `start_points`='5819900', `end_points`='6419600', `rewards`='0'  WHERE (`level_id` = 471) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='36', `start_points`='5297600', `end_points`='5819900', `rewards`='0'  WHERE (`level_id` = 461) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='35', `start_points`='4840800', `end_points`='5297600', `rewards`='0'  WHERE (`level_id` = 451) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='34', `start_points`='4439500', `end_points`='4840800', `rewards`='0'  WHERE (`level_id` = 441) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='33', `start_points`='4085200', `end_points`='4439500', `rewards`='0'  WHERE (`level_id` = 431) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='32', `start_points`='3770800', `end_points`='4085200', `rewards`='0'  WHERE (`level_id` = 421) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='31', `start_points`='3490300', `end_points`='3770800', `rewards`='0'  WHERE (`level_id` = 411) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='30', `start_points`='3238600', `end_points`='3490300', `rewards`='0'  WHERE (`level_id` = 401) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='29', `start_points`='3011400', `end_points`='3238600', `rewards`='0'  WHERE (`level_id` = 391) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='28', `start_points`='2805100', `end_points`='3011400', `rewards`='0'  WHERE (`level_id` = 381) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='27', `start_points`='2616600', `end_points`='2805100', `rewards`='0'  WHERE (`level_id` = 371) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='26', `start_points`='2443200', `end_points`='2616600', `rewards`='0'  WHERE (`level_id` = 361) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='25', `start_points`='2282700', `end_points`='2443200', `rewards`='0'  WHERE (`level_id` = 351) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='24', `start_points`='2133300', `end_points`='2282700', `rewards`='0'  WHERE (`level_id` = 341) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='23', `start_points`='1993300', `end_points`='2133300', `rewards`='0'  WHERE (`level_id` = 331) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='22', `start_points`='1861400', `end_points`='1993300', `rewards`='0'  WHERE (`level_id` = 321) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='21', `start_points`='1736500', `end_points`='1861400', `rewards`='0'  WHERE (`level_id` = 311) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='20', `start_points`='1617600', `end_points`='1736500', `rewards`='0'  WHERE (`level_id` = 301) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='19', `start_points`='1503800', `end_points`='1617600', `rewards`='0'  WHERE (`level_id` = 291) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='18', `start_points`='1394400', `end_points`='1503800', `rewards`='0'  WHERE (`level_id` = 281) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='17', `start_points`='1288900', `end_points`='1394400', `rewards`='0'  WHERE (`level_id` = 271) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='16', `start_points`='1186800', `end_points`='1288900', `rewards`='0'  WHERE (`level_id` = 261) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='15', `start_points`='1087700', `end_points`='1186800', `rewards`='0'  WHERE (`level_id` = 251) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='14', `start_points`='991300', `end_points`='1087700', `rewards`='0'  WHERE (`level_id` = 241) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='13', `start_points`='897300', `end_points`='991300', `rewards`='0'  WHERE (`level_id` = 231) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='12', `start_points`='805600', `end_points`='897300', `rewards`='0'  WHERE (`level_id` = 221) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='11', `start_points`='716100', `end_points`='805600', `rewards`='0'  WHERE (`level_id` = 211) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='10', `start_points`='628800', `end_points`='716100', `rewards`='0'  WHERE (`level_id` = 201) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='9', `start_points`='543800', `end_points`='628800', `rewards`='0'  WHERE (`level_id` = 191) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='4', `start_points`='168000', `end_points`='234200', `rewards`='0'  WHERE (`level_id` = 141) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='3', `start_points`='108900', `end_points`='168000', `rewards`='0'  WHERE (`level_id` = 131) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='2', `start_points`='59100', `end_points`='108900', `rewards`='0'  WHERE (`level_id` = 121) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='1', `start_points`='21500', `end_points`='59100', `rewards`='0'  WHERE (`level_id` = 111) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='50', `start_points`='28844400', `end_points`='30000000', `rewards`='0'  WHERE (`level_id` = 601) ;
UPDATE `levels` SET `fame_type_id`='3', `level_number`='49', `start_points`='24921800', `end_points`='28844400', `rewards`='0'  WHERE (`level_id` = 591) ;


-- new level_award rows (12/30/08 update)
/*
IF NOT EXISTS (SELECT 1 FROM level_awards WHERE level_award_id = 100) THEN
	INSERT INTO `level_awards` (`level_award_id`, `level_id`, `name`, `global_id`, `animation_id`, `gender`, `quantity`, `new_title`) 
		VALUES('100', '601', 'Ultimate Armor (Red)', '3761', '0', 'F', '1', 'Ka-ching! Famous'),
			('98', '501', 'Headshot Top', '3757', '0', 'F', '1', 'Ka-ching! Almost Famous'),
			('97', '501', 'Headshot Shirt', '3742', '0', 'M', '1', 'Ka-ching! Almost Famous'),
			('96', '401', '', '0', '0', 'F', '1', 'Ka-ching! Rising Star'),
			('95', '401', '', '0', '0', 'M', '1', 'Ka-ching! Rising Star'),
			('94', '351', 'Ka-ching! Bronze Trophy', '3779', '0', 'F', '1', NULL),
			('93', '351', 'Ka-ching! Bronze Trophy', '3779', '0', 'M', '1', NULL),
			('92', '301', 'Ka-ching This! Top', '3755', '0', 'F', '1', 'Ka-ching! Over-Achiever'),
			('91', '301', 'Ka-ching This! Shirt', '3740', '0', 'M', '1', 'Ka-ching! Over-Achiever'),
			('90', '201', 'Just Shoot Me Top', '3753', '0', 'F', '1', 'Ka-ching! Achiever'),
			('89', '201', 'Just Shoot Me Shirt', '3738', '0', 'M', '1', 'Ka-ching! Achiever'),
			('88', '151', '', '0', '0', 'F', '1', 'Ka-ching! Up-and-Coming'),
			('87', '151', '', '0', '0', 'M', '1', 'Ka-ching! Up-and-Coming'),
			('86', '121', 'Target Practice Top', '3751', '0', 'F', '1', NULL),
			('85', '121', 'Target Practice Shirt', '3736', '0', 'M', '1', NULL),
			('84', '111', '', '0', '0', 'F', '1', 'Ka-ching! Newb'),
			('83', '111', '', '0', '0', 'M', '1', 'Ka-ching! Newb'),
			('99', '601', 'Ultimate Armor (Blue)', '3747', '0', 'M', '1', 'Ka-ching! Famous');
END IF;
*/
-- update data in packets
IF NOT EXISTS (SELECT 1 FROM packets WHERE packet_id = 161) THEN
	INSERT INTO `packets` (`packet_id`, `fame_type_id`, `name`, `points_earned`, `rewards`, `time_interval`, `reward_interval`, `max_per_day`) 
	VALUES('161', '3', 'Daily Kaching - Win a game (up to 3)', '6700', '0', '3', '0', '3'),
		('71', '3', 'First Kaching Game', 						'10000', '50', '1', '0', '0'),
		('81', '3', 'First Kaching Win', 						'15000', '100', '1', '0', '0'),
		('91', '3', 'First Kaching Coin Scored', 				'5000', '100', '1', '0', '0'),
		('101', '3', 'First Kaching Check', 					'5000', '50', '1', '0', '0'),
		('111', '3', 'Every Kaching Win', 						'7500', '0', '2', '0', '0'),
		('121', '3', 'Every Kaching Game Played', 				'5000', '0', '2', '0', '0'),
		('131', '3', '5 Kaching Checks in One Game', 			'5000', '0', '2', '0', '0'),
		('151', '3', 'Daily Kaching - Play a Game (up to 5)', 	'3000', '0', '3', '0', '5'),
		('141', '3', '10 Kaching Coins Scored in One Game', 	'5000', '0', '2', '0', '0'),
		('61', '3', 'Kaching Game Played (TESTING--NOTUSED)', 	'5000', '0', '2', '0', '60');
END IF;

IF NOT EXISTS (SELECT 1 FROM packets WHERE packet_id = 46) THEN
	INSERT INTO `packets` (`packet_id`, `fame_type_id`, `name`, `points_earned`, `rewards`, `time_interval`, `reward_interval`, `max_per_day`) 
	VALUES( '46','1','Job Level 1','100','50','2','0','0' ),
		( '47','1','Job Level 2','100','55','2','0','0' ),
		( '48','1','Job Level 3','100','60','2','0','0' ),
		( '49','1','Job Level 4','100','65','2','0','0' ),
		( '50','1','Job Level 5','100','70','2','0','0' ),
		( '51','1','Job Level 6','100','75','2','0','0' ),
		( '52','1','Job Level 7','100','90','2','0','0' );
END IF;

UPDATE `packets` SET `max_per_day` = 0 WHERE `packet_id` IN (46, 47, 48, 49, 50, 51, 52);

UPDATE `packets` SET `fame_type_id`='1', `name`='WEB - Upload First You-Tube', `points_earned`='16000', `rewards`='50', `time_interval`='1', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 10) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='WORLD - change media on tv -first time', `points_earned`='14000', `rewards`='75', `time_interval`='1', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 8) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='WORLD - change pattern on walls', `points_earned`='75000', `rewards`='25', `time_interval`='1', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 7) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='WORLD - changing clothing ', `points_earned`='7500', `rewards`='25', `time_interval`='1', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 6) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='WORLD - place object in home', `points_earned`='10000', `rewards`='50', `time_interval`='1', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 5) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='DANCE FLOOR ??? Unique members visit (1,every 10)', `points_earned`='300', `rewards`='6', `time_interval`='2', `reward_interval`='10', `max_per_day`='5'  WHERE (`packet_id` = 17) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='WEB - first time visit to level help page', `points_earned`='10000', `rewards`='50', `time_interval`='1', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 4) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='HOME - Unique members visit (1,every 10)', `points_earned`='300', `rewards`='6', `time_interval`='2', `reward_interval`='10', `max_per_day`='5'  WHERE (`packet_id` = 19) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='WORLD - go thru inworld tutorial', `points_earned`='20000', `rewards`='100', `time_interval`='1', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 3) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='HANGOUT - Unique members visited (1,every 10)', `points_earned`='300', `rewards`='6', `time_interval`='2', `reward_interval`='10', `max_per_day`='5'  WHERE (`packet_id` = 21) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='WORLD - first time login after creating avatar', `points_earned`='40000', `rewards`='200', `time_interval`='1', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 2) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='BOTH ??? newbie tasks achievement', `points_earned`='60000', `rewards`='300', `time_interval`='1', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 11) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='View 3 Media Details pages daily', `points_earned`='400', `rewards`='8', `time_interval`='3', `reward_interval`='3', `max_per_day`='0'  WHERE (`packet_id` = 42) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='Daily Signin Web', `points_earned`='500', `rewards`='11', `time_interval`='3', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 39) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='Dance to level 10 daily', `points_earned`='700', `rewards`='15', `time_interval`='3', `reward_interval`='10', `max_per_day`='0'  WHERE (`packet_id` = 38) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='Give a Gift', `points_earned`='900', `rewards`='19', `time_interval`='2', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 27) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='More than 3 hours in world daily', `points_earned`='700', `rewards`='15', `time_interval`='3', `reward_interval`='180', `max_per_day`='0'  WHERE (`packet_id` = 37) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='WEB - add profile picture', `points_earned`='20000', `rewards`='50', `time_interval`='1', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 1) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='More than 10 minutes in world daily', `points_earned`='500', `rewards`='11', `time_interval`='3', `reward_interval`='10', `max_per_day`='0'  WHERE (`packet_id` = 36) ;
UPDATE `packets` SET `fame_type_id`='1', `name`='Daily Signin World', `points_earned`='500', `rewards`='11', `time_interval`='3', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 35) ;

-- packets changes (12/30/08 update)
UPDATE `packets` SET `fame_type_id`='3', `name`='Daily Kaching - Win a game (up to 3)', `points_earned`='6700', `rewards`='0', `time_interval`='2', `reward_interval`='0', `max_per_day`='4'  WHERE (`packet_id` = 161) ;
UPDATE `packets` SET `fame_type_id`='3', `name`='10 Kaching Coins Scored in One Game', `points_earned`='10000', `rewards`='0', `time_interval`='2', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 141) ;
UPDATE `packets` SET `fame_type_id`='3', `name`='5 Kaching Checks in One Game', `points_earned`='7500', `rewards`='0', `time_interval`='2', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 131) ;
UPDATE `packets` SET `fame_type_id`='3', `name`='Every Kaching Win', `points_earned`='10000', `rewards`='0', `time_interval`='2', `reward_interval`='0', `max_per_day`='0'  WHERE (`packet_id` = 111) ;
UPDATE `packets` SET `fame_type_id`='3', `name`='Daily Kaching - Play a Game (up to 5)', `points_earned`='3000', `rewards`='0', `time_interval`='2', `reward_interval`='0', `max_per_day`='6'  WHERE (`packet_id` = 151) ;

END //

DELIMITER ;

CALL fame_SeedData_50;
DROP PROCEDURE IF EXISTS fame_SeedData_50;


