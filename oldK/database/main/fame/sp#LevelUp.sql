-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS LevelUp;

DELIMITER ;;
CREATE PROCEDURE `LevelUp`(in userId INT, in fameTypeId INT, in currentLevelId INT, OUT levelId INT)
BEGIN

  -- Level Up
  DECLARE _newPoints INT;
  DECLARE _newLevelId INT;
  DECLARE _levelRewards INT;
  DECLARE _secondsToLevel INT;
  DECLARE _previousLevelDate DateTime;

  -- Set new level if they leveled up
  SELECT points INTO _newPoints FROM user_fame WHERE user_id = userId and fame_type_id = fameTypeId;
  SELECT level_id, rewards INTO _newLevelId, _levelRewards FROM levels WHERE fame_type_id = fameTypeId AND start_points < _newPoints AND end_points >= _newPoints LIMIT 1;

  SET levelId = currentLevelId;

  IF _newLevelId > currentLevelId THEN
      SET levelId = _newLevelId;
      UPDATE user_fame set level_id = _newLevelId WHERE user_id = userId AND fame_type_id = fameTypeId;

      -- Calc seconds to level
      SET _secondsToLevel = 0;

      -- Get the previous time they leveled up
      SELECT level_date INTO _previousLevelDate FROM level_history WHERE level_id = currentLevelId AND fame_type_id = fameTypeId LIMIT 1;

      IF _previousLevelDate IS NOT NULL THEN
         SELECT TIMESTAMPDIFF(SECOND, _previousLevelDate, NOW()) INTO _secondsToLevel;
      END IF;

      INSERT INTO level_history (user_id, fame_type_id, level_id, level_date, seconds_to_level, rewards_awarded) VALUES (userId, fameTypeId, _newLevelId, NOW(), _secondsToLevel, _levelRewards);

      -- Rewards for leveling
       if _levelRewards > 0 THEN
         CALL kaneva.apply_transaction_to_user_balance(userId, _levelRewards, 19, 'GPOINT', @returnCode, @userBalance, @tranId);
       END IF;
  END IF;


END
;;
DELIMITER ;