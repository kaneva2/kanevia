-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `checklist_completions` (
  `user_id` int(11) NOT NULL,
  `nudge_checklist_id` int(11) NOT NULL,
  `date_completed` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`nudge_checklist_id`)
) ENGINE=InnoDB;

CREATE TABLE `daily_history` (
  `user_id` int(11) NOT NULL,
  `fame_type_id` int(11) NOT NULL,
  `packet_id` int(11) NOT NULL,
  `date_rewarded` date NOT NULL,
  `packet_count` int(11) NOT NULL DEFAULT '0',
  `date_last_rewarded` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`fame_type_id`,`packet_id`,`date_rewarded`),
  KEY `IDX_date_rewarded` (`date_rewarded`)
) ENGINE=InnoDB;

CREATE TABLE `fame_type` (
  `fame_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`fame_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `levels` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `fame_type_id` int(11) NOT NULL,
  `level_number` int(11) NOT NULL,
  `start_points` int(11) NOT NULL,
  `end_points` int(11) NOT NULL,
  `rewards` int(11) NOT NULL DEFAULT '0',
  `send_kmail` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`level_id`),
  KEY `Reffame_type33` (`fame_type_id`),
  CONSTRAINT `Reffame_type33` FOREIGN KEY (`fame_type_id`) REFERENCES `fame_type` (`fame_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `level_awards` (
  `level_award_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `global_id` int(11) NOT NULL,
  `animation_id` int(11) NOT NULL DEFAULT '0',
  `gender` char(1) DEFAULT 'U',
  `quantity` int(11) DEFAULT NULL,
  `new_title_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`level_award_id`),
  KEY `Reflevels16` (`level_id`),
  CONSTRAINT `Reflevels16` FOREIGN KEY (`level_id`) REFERENCES `levels` (`level_id`)
) ENGINE=InnoDB;

CREATE TABLE `level_history` (
  `user_id` int(11) NOT NULL,
  `fame_type_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `level_date` datetime NOT NULL,
  `seconds_to_level` bigint(20) NOT NULL,
  `rewards_awarded` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`fame_type_id`,`level_id`),
  KEY `Reflevels14` (`level_id`),
  CONSTRAINT `Reflevels14` FOREIGN KEY (`level_id`) REFERENCES `levels` (`level_id`),
  CONSTRAINT `Refuser_fame15` FOREIGN KEY (`user_id`, `fame_type_id`) REFERENCES `user_fame` (`user_id`, `fame_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `maintenance_log` (
  `maintenance_log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_occurred` datetime NOT NULL,
  `database_name` varchar(50) DEFAULT NULL,
  `procedure_name` varchar(200) DEFAULT NULL,
  `message_text` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`maintenance_log_id`)
) ENGINE=InnoDB;

CREATE TABLE `nudges` (
  `nudge_id` int(11) NOT NULL AUTO_INCREMENT,
  `packet_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `impressions` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `url_text` varchar(100) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `checklist_text` varchar(1) NOT NULL DEFAULT '',
  `status_id` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nudge_id`),
  KEY `Refpackets18` (`packet_id`),
  CONSTRAINT `Refpackets18` FOREIGN KEY (`packet_id`) REFERENCES `packets` (`packet_id`)
) ENGINE=InnoDB;

CREATE TABLE `nudge_checklists` (
  `nudge_checklist_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `reward_packet_id` int(11) NOT NULL,
  PRIMARY KEY (`nudge_checklist_id`)
) ENGINE=InnoDB;

CREATE TABLE `nudge_checklist_packets` (
  `packet_id` int(11) NOT NULL,
  `nudge_checklist_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `percentage` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`packet_id`,`nudge_checklist_id`),
  KEY `Refnudge_checklists1` (`nudge_checklist_id`),
  CONSTRAINT `Refnudge_checklists1` FOREIGN KEY (`nudge_checklist_id`) REFERENCES `nudge_checklists` (`nudge_checklist_id`),
  CONSTRAINT `Refpackets2` FOREIGN KEY (`packet_id`) REFERENCES `packets` (`packet_id`)
) ENGINE=InnoDB;

CREATE TABLE `onetime_history` (
  `user_id` int(11) NOT NULL,
  `fame_type_id` int(11) NOT NULL,
  `packet_id` int(11) NOT NULL,
  `date_rewarded` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`fame_type_id`,`packet_id`),
  KEY `Refpackets22` (`packet_id`),
  CONSTRAINT `Refpackets22` FOREIGN KEY (`packet_id`) REFERENCES `packets` (`packet_id`),
  CONSTRAINT `Refuser_fame21` FOREIGN KEY (`user_id`, `fame_type_id`) REFERENCES `user_fame` (`user_id`, `fame_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `packets` (
  `packet_id` int(11) NOT NULL AUTO_INCREMENT,
  `fame_type_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `points_earned` int(11) NOT NULL,
  `rewards` int(11) NOT NULL,
  `time_interval` int(11) NOT NULL,
  `reward_interval` int(11) NOT NULL,
  `max_per_day` int(10) unsigned NOT NULL DEFAULT '0',
  `transaction_type` smallint(6) DEFAULT '15',
  `status_id` smallint(6) NOT NULL DEFAULT '1',
  `badge_id` int(11) NOT NULL DEFAULT '0',
  `is_server_only` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`packet_id`),
  KEY `Reffame_type32` (`fame_type_id`),
  CONSTRAINT `Reffame_type32` FOREIGN KEY (`fame_type_id`) REFERENCES `fame_type` (`fame_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `packet_history` (
  `packet_history_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `packet_id` int(11) NOT NULL,
  `date_rewarded` datetime NOT NULL,
  `points_awarded` int(11) NOT NULL DEFAULT '0',
  `rewards_awarded` int(11) NOT NULL DEFAULT '0',
  `fame_type_id` int(11) NOT NULL,
  PRIMARY KEY (`packet_history_id`),
  KEY `Refuser_fame6` (`user_id`,`fame_type_id`),
  KEY `Refpackets9` (`packet_id`),
  KEY `Index_date_rewarded` (`date_rewarded`) USING BTREE,
  CONSTRAINT `Refpackets9` FOREIGN KEY (`packet_id`) REFERENCES `packets` (`packet_id`),
  CONSTRAINT `Refuser_fame6` FOREIGN KEY (`user_id`, `fame_type_id`) REFERENCES `user_fame` (`user_id`, `fame_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `reward_interval_tracking` (
  `user_id` int(10) unsigned NOT NULL,
  `packet_id` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `packet_count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`packet_id`,`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `reward_interval_tracking_daily` (
  `user_id` int(11) NOT NULL,
  `packet_id` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `packet_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`packet_id`,`created_date`),
  KEY `IDX_date_rewarded` (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `schema_versions` (
  `version` int(11) unsigned NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

CREATE TABLE `user_fame` (
  `user_id` int(11) NOT NULL,
  `fame_type_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`fame_type_id`),
  KEY `Reflevels5` (`level_id`),
  KEY `Reffame_type29` (`fame_type_id`),
  CONSTRAINT `Reffame_type29` FOREIGN KEY (`fame_type_id`) REFERENCES `fame_type` (`fame_type_id`),
  CONSTRAINT `Reflevels5` FOREIGN KEY (`level_id`) REFERENCES `levels` (`level_id`)
) ENGINE=InnoDB;

