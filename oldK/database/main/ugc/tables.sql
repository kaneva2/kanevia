-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `countries_to_summarize` (
  `country` char(2) NOT NULL DEFAULT 'US' COMMENT 'country code of country',
  PRIMARY KEY (`country`)
) ENGINE=InnoDB COMMENT='Holds country codes of countries to summarize';

CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  `interface_change` enum('T','F') NOT NULL DEFAULT 'F',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

CREATE TABLE `sp_run_log` (
  `sp_run_log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `sp_name` varchar(256) NOT NULL COMMENT 'name of the stored procedure',
  `run_start_time` datetime NOT NULL COMMENT 'when started',
  `run_end_time` datetime NOT NULL COMMENT 'when ended',
  `duration_seconds` int(11) NOT NULL COMMENT 'duration in seconds',
  PRIMARY KEY (`sp_run_log_id`),
  KEY `IDX_start_time` (`run_start_time`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT COMMENT='run duration information for stored procedures';

CREATE TABLE `summary_3Dapps_raves_latest_month` (
  `community_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `place_type_id` int(11) NOT NULL DEFAULT '0' COMMENT 'communities place type id',
  `raves` int(11) NOT NULL,
  `game_name` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `owner_age` int(11) DEFAULT '18' COMMENT 'Owners Age',
  PRIMARY KEY (`community_id`),
  KEY `IDX_game_id` (`game_id`),
  KEY `IDX_place_type` (`place_type_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='summary raves for 3D apps';

CREATE TABLE `summary_3Dapps_raves_latest_month_filtered` (
  `community_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `place_type_id` int(11) NOT NULL DEFAULT '0' COMMENT 'communities place type id',
  `raves` int(11) NOT NULL,
  `game_name` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `owner_country` char(2) NOT NULL DEFAULT 'US' COMMENT 'country code of owner for filtering',
  `owner_age` int(11) NOT NULL DEFAULT '18' COMMENT 'age of the owner for filtering',
  PRIMARY KEY (`community_id`),
  KEY `IDX_game_id` (`game_id`),
  KEY `IDX_place_type` (`place_type_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='summary raves for 3D apps';

CREATE TABLE `summary_hangout_raves_all_time` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `place_type_id` int(11) NOT NULL DEFAULT '0' COMMENT 'communities place type id',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  `raves` int(11) NOT NULL,
  `zone_name` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `owner_age` int(11) DEFAULT '18' COMMENT 'Owners age',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `IDX_place_type_id` (`place_type_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_hangout_raves_all_time_filtered` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `place_type_id` int(11) NOT NULL DEFAULT '0' COMMENT 'communities place type id',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  `raves` int(11) NOT NULL,
  `zone_name` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `owner_country` char(2) NOT NULL DEFAULT 'US' COMMENT 'country code of owner for filtering',
  `owner_age` int(11) NOT NULL DEFAULT '18' COMMENT 'age of the owner for filtering',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `idx_country_age` (`owner_country`,`owner_age`),
  KEY `IDX_place_type_id` (`place_type_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_hangout_raves_latest_day` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `place_type_id` int(11) NOT NULL DEFAULT '0' COMMENT 'communities place type id',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  `raves` int(11) NOT NULL,
  `zone_name` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `owner_age` int(11) DEFAULT '18' COMMENT 'Owners Age',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `IDX_place_type_id` (`place_type_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_hangout_raves_latest_day_filtered` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `place_type_id` int(11) NOT NULL DEFAULT '0' COMMENT 'communities place type id',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  `raves` int(11) NOT NULL,
  `zone_name` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `owner_country` char(2) NOT NULL DEFAULT 'US' COMMENT 'country code of owner for filtering',
  `owner_age` int(11) NOT NULL DEFAULT '18' COMMENT 'age of the owner for filtering',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `idx_country_age` (`owner_country`,`owner_age`),
  KEY `IDX_place_type_id` (`place_type_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_hangout_raves_latest_month` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `place_type_id` int(11) NOT NULL DEFAULT '0' COMMENT 'communities place type id',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  `raves` int(11) NOT NULL,
  `zone_name` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `owner_age` int(11) DEFAULT '18' COMMENT 'Owners Age',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `IDX_place_type_id` (`place_type_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_hangout_raves_latest_month_filtered` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `place_type_id` int(11) NOT NULL DEFAULT '0' COMMENT 'communities place type id',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  `raves` int(11) NOT NULL,
  `zone_name` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `owner_country` char(2) NOT NULL DEFAULT 'US' COMMENT 'country code of owner for filtering',
  `owner_age` int(11) NOT NULL DEFAULT '18' COMMENT 'age of the owner for filtering',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `idx_country_age` (`owner_country`,`owner_age`),
  KEY `IDX_place_type_id` (`place_type_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_hangout_raves_latest_week` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `place_type_id` int(11) NOT NULL DEFAULT '0' COMMENT 'communities place type id',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  `raves` int(11) NOT NULL,
  `zone_name` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `owner_age` int(11) DEFAULT '18' COMMENT 'Owners Age',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `IDX_place_type_id` (`place_type_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_hangout_raves_latest_week_filtered` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `place_type_id` int(11) NOT NULL DEFAULT '0' COMMENT 'communities place type id',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  `raves` int(11) NOT NULL,
  `zone_name` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `owner_country` char(2) NOT NULL DEFAULT 'US' COMMENT 'country code of owner for filtering',
  `owner_age` int(11) NOT NULL DEFAULT '18' COMMENT 'age of the owner for filtering',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `idx_country_age` (`owner_country`,`owner_age`),
  KEY `IDX_place_type_id` (`place_type_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_home_raves_all_time` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `owner_age` int(11) DEFAULT '18' COMMENT 'Owners Age',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_home_raves_all_time_filtered` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `owner_country` char(2) NOT NULL DEFAULT 'US' COMMENT 'country code of owner for filtering',
  `owner_age` int(11) NOT NULL DEFAULT '18' COMMENT 'age of the owner for filtering',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `idx_country_age` (`owner_country`,`owner_age`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_home_raves_latest_day` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `owner_age` int(11) DEFAULT '18' COMMENT 'Owners Age',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_home_raves_latest_day_filtered` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `owner_country` char(2) NOT NULL DEFAULT 'US' COMMENT 'country code of owner for filtering',
  `owner_age` int(11) NOT NULL DEFAULT '18' COMMENT 'age of the owner for filtering',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `idx_country_age` (`owner_country`,`owner_age`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_home_raves_latest_month` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `owner_age` int(11) DEFAULT '18' COMMENT 'Owners Age',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_home_raves_latest_month_filtered` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `owner_country` char(2) NOT NULL DEFAULT 'US' COMMENT 'country code of owner for filtering',
  `owner_age` int(11) NOT NULL DEFAULT '18' COMMENT 'age of the owner for filtering',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `idx_country_age` (`owner_country`,`owner_age`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_home_raves_latest_week` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `owner_age` int(11) DEFAULT '18' COMMENT 'Owners Age',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `summary_home_raves_latest_week_filtered` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `owner_country` char(2) NOT NULL DEFAULT 'US' COMMENT 'country code of owner for filtering',
  `owner_age` int(11) NOT NULL DEFAULT '18' COMMENT 'age of the owner for filtering',
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pass group from pass_group_channel_zones',
  PRIMARY KEY (`zone_type`,`zone_instance_id`),
  KEY `idx_country_age` (`owner_country`,`owner_age`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='population count';

CREATE TABLE `table_in_maintenance` (
  `table_name` varchar(100) NOT NULL,
  `in_maintenance` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'table currently undergoing data maintenance (T/F)',
  PRIMARY KEY (`table_name`)
) ENGINE=InnoDB;

CREATE TABLE `tour_weekly_top_raved_hangouts` (
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `place_type_id` int(11) NOT NULL DEFAULT '0' COMMENT 'communities place type id',
  `zone_index_plain` int(11) NOT NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  PRIMARY KEY (`zone_instance_id`),
  KEY `IDX_place_type_id` (`place_type_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='tour_weekly_top_raved_hangouts';

