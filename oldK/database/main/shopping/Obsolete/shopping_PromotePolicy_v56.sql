-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

USE shopping;

SELECT 'Starting Promotion... ',NOW();


DROP PROCEDURE IF EXISTS PromotePolicy;
DELIMITER //
CREATE PROCEDURE PromotePolicy()
BEGIN
IF NOT EXISTS (SELECT 1	FROM information_schema.statistics WHERE table_schema = 'shopping' 
		AND table_name = 'item_categories' AND index_name = 'IDX_upload_type') THEN
	CREATE INDEX IDX_upload_type ON item_categories(`upload_type`);
END IF;
END
// 
DELIMITER ;

CALL PromotePolicy();
DROP PROCEDURE IF EXISTS PromotePolicy;

-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (56, 'Create Index on item_categories.upload_type', NOW()  );

SELECT 'Finished Promotion... ',NOW();