-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

/*
SQLyog Enterprise - MySQL GUI v7.14 
MySQL - 5.0.67-community-log : Database - shopping
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `active_item_templates` */

CREATE TABLE `active_item_templates` (
  `global_id` int(11) NOT NULL default '0',
  `sort_order` int(11) unsigned NOT NULL default '0',
  `height` int(11) NOT NULL default '0',
  `width` int(11) NOT NULL default '0',
  PRIMARY KEY  (`global_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `item_categories` */

CREATE TABLE `item_categories` (
  `item_category_id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  `parent_category_id` int(10) unsigned NOT NULL default '0',
  `sort_order` int(11) default '0',
  PRIMARY KEY  (`item_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1 COMMENT='Categories for an item';

/*Table structure for table `item_category_items` */

CREATE TABLE `item_category_items` (
  `item_category_id` int(10) unsigned NOT NULL,
  `global_id` int(11) NOT NULL,
  PRIMARY KEY  (`item_category_id`,`global_id`),
  KEY `gid` (`global_id`),
  KEY `ici` (`item_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Categories that an item is in';

/*Table structure for table `item_diggs` */

CREATE TABLE `item_diggs` (
  `digg_id` int(11) unsigned NOT NULL auto_increment,
  `user_id` int(11) NOT NULL default '0',
  `global_id` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`digg_id`),
  KEY `digger` (`user_id`,`global_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56252 DEFAULT CHARSET=latin1;

/*Table structure for table `item_promotions` */

CREATE TABLE `item_promotions` (
  `item_category_id` int(10) unsigned NOT NULL,
  `global_id` int(11) NOT NULL,
  PRIMARY KEY  (`item_category_id`,`global_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Items that are in a promotion';

/*Table structure for table `item_purchases` */

CREATE TABLE `item_purchases` (
  `purchase_id` int(10) unsigned NOT NULL auto_increment,
  `global_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `item_owner_id` int(11) unsigned NOT NULL default '0',
  `purchase_price` int(11) NOT NULL default '0',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'MPOINT' COMMENT 'Type of points used for purchase',
  `owner_price` int(10) unsigned NOT NULL default '0' COMMENT 'Price the owner recieved',
  `ip_address` bigint(20) unsigned NOT NULL,
  `purchase_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `quantity` int(10) unsigned NOT NULL default '1',
  PRIMARY KEY  (`purchase_id`),
  KEY `gid` (`global_id`),
  KEY `uid` (`user_id`),
  KEY `ioid` (`item_owner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=194313 DEFAULT CHARSET=latin1 COMMENT='Shopping item purchases';

/*Table structure for table `item_reviews` */

CREATE TABLE `item_reviews` (
  `review_id` int(10) unsigned NOT NULL auto_increment,
  `global_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text,
  `ip_address` bigint(20) unsigned NOT NULL,
  `last_updated_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`review_id`),
  KEY `gid` (`global_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4579 DEFAULT CHARSET=latin1 COMMENT='Reviews for items';

/*Table structure for table `items_web` */

CREATE TABLE `items_web` (
  `global_id` int(11) NOT NULL,
  `base_global_id` int(10) unsigned NOT NULL default '0' COMMENT 'Base global inventory ID',
  `template_path` varchar(100) default NULL COMMENT 'The relative path to the template',
  `template_path_encrypted` varchar(255) NOT NULL,
  `texture_path` varchar(100) default NULL COMMENT 'The custom texture path',
  `thumbnail_path` varchar(255) default NULL COMMENT 'Original thumbnail',
  `thumbnail_small_path` varchar(255) default NULL COMMENT 'Small version of thumb',
  `thumbnail_medium_path` varchar(255) default NULL COMMENT 'Medium version of thumb',
  `thumbnail_large_path` varchar(255) default NULL COMMENT 'large version of thumb',
  `thumbnail_assetdetails_path` varchar(255) default NULL COMMENT 'AD version of thumb',
  `designer_price` int(10) unsigned NOT NULL default '0' COMMENT 'Additional Price User has set',
  `keywords` varchar(100) default NULL COMMENT 'Keywords',
  `number_sold_on_web` int(10) unsigned NOT NULL default '0' COMMENT 'Number of time sold on web',
  `number_of_raves` int(10) unsigned NOT NULL default '0' COMMENT 'Number of raves',
  `number_of_views` int(10) unsigned NOT NULL default '0' COMMENT 'Number of views',
  `sales_total` int(10) unsigned NOT NULL default '0' COMMENT 'Total sales',
  `sales_designer_total` int(10) unsigned NOT NULL default '0' COMMENT 'Total sales designer recieved',
  PRIMARY KEY  (`global_id`),
  KEY `idxNumberRaves` (`number_of_raves`),
  KEY `idxNumberViews` (`number_of_views`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Additional Item Data';

/*Table structure for table `schema_versions` */

CREATE TABLE `schema_versions` (
  `version` int(11) unsigned NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL default CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY  (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
