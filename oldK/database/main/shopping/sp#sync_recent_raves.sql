-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS sync_recent_raves;

DELIMITER ;;
CREATE PROCEDURE `sync_recent_raves`(__cutoff_time DATETIME)
BEGIN
DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE _global_id INT;
DECLARE _raves INT;
DECLARE __message_text VARCHAR(500);
DECLARE sync_cursor CURSOR FOR 
		SELECT iw.global_id, COALESCE(COUNT(sid.digg_id),0) AS raves
			FROM shopping.items_web iw
			LEFT OUTER JOIN shopping.item_diggs sid ON iw.global_id = sid.global_id
			WHERE sid.created_date > ADDDATE(NOW(), INTERVAL -30 DAY) 
			GROUP BY iw.global_id;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;
SET i = 0;
CALL shard_info.add_maintenance_log ('kaneva','sync_recent_raves','sync started');
OPEN sync_cursor;
REPEAT
    FETCH sync_cursor INTO _global_id, _raves;
    IF NOT done THEN
	SET i=i+1;
	-- SELECT _global_id, _qty_sold;
	UPDATE items_web SET recent_raves = _raves WHERE global_id = _global_id;
    END IF;
    IF NOW() >= __cutoff_time THEN 
	SET done = 1; 
    END IF;
UNTIL done END REPEAT;
CALL shard_info.add_maintenance_log ('kaneva','sync_recent_raves','sync completed');
CLOSE sync_cursor;
END
;;
DELIMITER ;