-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `active_item_templates` (
  `global_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) unsigned NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `width` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`global_id`)
) ENGINE=InnoDB;

CREATE TABLE `checksum` (
  `db` char(64) NOT NULL,
  `tbl` char(64) NOT NULL,
  `chunk` int(11) NOT NULL,
  `boundaries` char(100) NOT NULL,
  `this_crc` char(40) NOT NULL,
  `this_cnt` int(11) NOT NULL,
  `master_crc` char(40) DEFAULT NULL,
  `master_cnt` int(11) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`db`,`tbl`,`chunk`)
) ENGINE=InnoDB;

CREATE TABLE `DBA_item_category_no_item_web` (
  `global_id` int(11) NOT NULL DEFAULT '0',
  `category_id1` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `category_id2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `category_id3` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`global_id`)
) ENGINE=InnoDB COMMENT='Data from ici with no items_web match';

CREATE TABLE `items_web` (
  `global_id` int(11) NOT NULL,
  `base_global_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Base global inventory ID',
  `template_path` varchar(100) DEFAULT NULL COMMENT 'The relative path to the template',
  `template_path_encrypted` varchar(255) NOT NULL,
  `texture_path` varchar(100) DEFAULT NULL COMMENT 'The custom texture path',
  `thumbnail_path` varchar(255) DEFAULT NULL COMMENT 'Original thumbnail',
  `thumbnail_small_path` varchar(255) DEFAULT NULL COMMENT 'Small version of thumb',
  `thumbnail_medium_path` varchar(255) DEFAULT NULL COMMENT 'Medium version of thumb',
  `thumbnail_large_path` varchar(255) DEFAULT NULL COMMENT 'large version of thumb',
  `thumbnail_assetdetails_path` varchar(255) DEFAULT NULL COMMENT 'AD version of thumb',
  `designer_price` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Additional Price User has set',
  `keywords` varchar(100) DEFAULT NULL COMMENT 'Keywords',
  `number_sold_on_web` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of time sold on web',
  `number_of_raves` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of raves',
  `number_of_views` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of views',
  `sales_total` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Total sales',
  `sales_designer_total` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Total sales designer recieved',
  `cache_duration` int(10) unsigned NOT NULL DEFAULT '1440' COMMENT 'Time to cache the texuture',
  `converted` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Has this item been converted',
  `category_id1` int(10) unsigned NOT NULL DEFAULT '0',
  `category_id2` int(10) unsigned NOT NULL DEFAULT '0',
  `category_id3` int(10) unsigned NOT NULL DEFAULT '0',
  `display_name` varchar(125) NOT NULL DEFAULT '',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `item_active` smallint(1) unsigned NOT NULL DEFAULT '0',
  `search_refresh_needed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'update to include item in inremental index',
  `creator_username` varchar(22) DEFAULT '' COMMENT 'copied from users to eliminate a join',
  `recent_qty_sold` int(11) DEFAULT '0',
  `recent_raves` int(11) DEFAULT '0',
  PRIMARY KEY (`global_id`),
  KEY `idxNumberRaves` (`number_of_raves`),
  KEY `idxNumberViews` (`number_of_views`),
  KEY `IDX_category_ids` (`category_id1`,`category_id2`,`category_id3`),
  KEY `IDX_search_refresh_needed` (`search_refresh_needed`)
) ENGINE=InnoDB COMMENT='Additional Item Data';

CREATE TABLE `item_categories` (
  `item_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  `parent_category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT '0',
  `marketing_path` varchar(500) NOT NULL DEFAULT ' ' COMMENT 'path for a flash ad or upsell image',
  `upload_type` int(10) NOT NULL DEFAULT '0' COMMENT 'Designates what upload types are eligible for the category.',
  `actor_group` int(10) NOT NULL DEFAULT '0' COMMENT 'Shows if a category is restricted to a gender.',
  PRIMARY KEY (`item_category_id`),
  KEY `IDX_upload_type` (`upload_type`)
) ENGINE=InnoDB COMMENT='Categories for an item';

CREATE TABLE `item_category_items` (
  `item_category_id` int(10) unsigned NOT NULL,
  `global_id` int(11) NOT NULL,
  PRIMARY KEY (`item_category_id`,`global_id`),
  KEY `gid` (`global_id`),
  KEY `ici` (`item_category_id`)
) ENGINE=InnoDB COMMENT='Categories that an item is in';

CREATE TABLE `item_diggs` (
  `digg_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `global_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`digg_id`),
  KEY `digger` (`user_id`,`global_id`),
  KEY `IDX_created_date` (`created_date`),
  KEY `IDX_global_id` (`global_id`)
) ENGINE=InnoDB;

CREATE TABLE `item_promotions` (
  `item_category_id` int(10) unsigned NOT NULL,
  `global_id` int(11) NOT NULL,
  PRIMARY KEY (`item_category_id`,`global_id`)
) ENGINE=InnoDB COMMENT='Items that are in a promotion';

CREATE TABLE `item_purchases` (
  `purchase_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `txn_type` smallint(2) NOT NULL DEFAULT '0',
  `global_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `item_owner_id` int(11) unsigned NOT NULL DEFAULT '0',
  `purchase_price` int(11) NOT NULL DEFAULT '0',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'MPOINT' COMMENT 'Type of points used for purchase',
  `owner_price` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Price the owner recieved',
  `ip_address` bigint(20) unsigned NOT NULL,
  `purchase_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `quantity` int(10) unsigned NOT NULL DEFAULT '1',
  `parent_purchase_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`purchase_id`),
  KEY `gid` (`global_id`),
  KEY `uid` (`user_id`),
  KEY `ioid` (`item_owner_id`),
  KEY `txn_type` (`txn_type`),
  KEY `IDX_purchase_date` (`purchase_date`),
  KEY `IDX_parent_purchase_id` (`parent_purchase_id`)
) ENGINE=InnoDB COMMENT='Shopping item purchases';

CREATE TABLE `item_reviews` (
  `review_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `global_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text,
  `ip_address` bigint(20) unsigned NOT NULL,
  `last_updated_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) NOT NULL DEFAULT '0',
  `status_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`review_id`),
  KEY `gid` (`global_id`)
) ENGINE=InnoDB COMMENT='Reviews for items';

CREATE TABLE `schema_versions` (
  `version` int(11) unsigned NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

