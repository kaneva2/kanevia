-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS util_flatten_item_category_items;

DELIMITER ;;
CREATE PROCEDURE `util_flatten_item_category_items`()
BEGIN
DECLARE _global_id		INT;
DECLARE _category_id		INT;
DECLARE	__table_RowCnt		INT;
DECLARE __new_rows_added	INT;
DECLARE __message_text VARCHAR(500);
DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE flat_cursor CURSOR FOR 
	SELECT global_id, item_category_id  
		FROM shopping.item_category_items 
		WHERE global_id > 0 
		ORDER BY global_id, item_category_id;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SELECT NOW() INTO @start_time;
SET done = 0;
SET i = 0;
SET __new_rows_added = 0;
SELECT MIN(global_id) INTO @last_global_id FROM shopping.item_category_items WHERE global_id > 0;
OPEN flat_cursor;
SET __table_RowCnt = (SELECT FOUND_ROWS());
SELECT CONCAT('Starting flattening ... ',CAST(__table_RowCnt AS CHAR),' rows to flatten. ') INTO __message_text;
-- select now(), __message_text;
SET @category_id1 = 0, @category_id2 = 0, @category_id3 = 0;
REPEAT
    FETCH flat_cursor INTO _global_id, _category_id;
    IF NOT done THEN
	IF _global_id <> @last_global_id THEN
		UPDATE items_web
			SET category_id1 = @category_id1, category_id2 = @category_id2, category_id3 = @category_id3
			WHERE global_id = @last_global_id;
		IF ROW_COUNT() <> 1 THEN
			INSERT INTO `shopping`.`DBA_item_category_no_item_web` (global_id, category_id1, category_id2, category_id3)
			VALUES (@last_global_id, @category_id1, @category_id2, @category_id3);
		END IF;
		SET @last_global_id = _global_id;
		SET @category_id1 = 0, @category_id2 = 0, @category_id3 = 0;
		SET i = 1;
		SET __new_rows_added = __new_rows_added + 1;
	END IF;
	IF i = 1 THEN SET @category_id1 = _category_id; END IF;
	IF i = 2 THEN SET @category_id2 = _category_id; END IF;
	IF i = 3 THEN SET @category_id3 = _category_id; END IF;
	SET i=i+1;
    END IF;	
UNTIL done END REPEAT;
CLOSE flat_cursor;
SELECT CONCAT(CAST(NOW() AS CHAR),' - util_flatten_item_category_items completed; ',CAST(__table_RowCnt AS CHAR), 
	' rows flattened to ',CAST(__new_rows_added AS CHAR),' rows; in ',
	CAST(TIME_TO_SEC(TIMEDIFF(NOW(),@start_time)) AS CHAR),' seconds.') AS `Results`;
END
;;
DELIMITER ;