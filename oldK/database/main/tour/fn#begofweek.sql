-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS begofweek;

DELIMITER ;;
CREATE FUNCTION `begofweek`(in_date DATETIME) RETURNS datetime
    DETERMINISTIC
BEGIN
-- CASE DAYOFWEEK(in_date)
--	WHEN 1 THEN SELECT CONCAT(DATE(DATE_SUB(in_date, INTERVAL 0 DAY)),' 00:00:00') INTO @begofweek;
--	WHEN 2 THEN SELECT CONCAT(DATE(DATE_SUB(in_date, INTERVAL 1 DAY)),' 00:00:00') INTO @begofweek;
--	WHEN 3 THEN SELECT CONCAT(DATE(DATE_SUB(in_date, INTERVAL 2 DAY)),' 00:00:00') INTO @begofweek;
--	WHEN 4 THEN SELECT CONCAT(DATE(DATE_SUB(in_date, INTERVAL 3 DAY)),' 00:00:00') INTO @begofweek;
--	WHEN 5 THEN SELECT CONCAT(DATE(DATE_SUB(in_date, INTERVAL 4 DAY)),' 00:00:00') INTO @begofweek;
--	WHEN 6 THEN SELECT CONCAT(DATE(DATE_SUB(in_date, INTERVAL 5 DAY)),' 00:00:00') INTO @begofweek;
--	WHEN 7 THEN SELECT CONCAT(DATE(DATE_SUB(in_date, INTERVAL 6 DAY)),' 00:00:00') INTO @begofweek;
--	END CASE;

	SELECT DATE_SUB(DATE(in_date), INTERVAL (DAYOFWEEK(in_date)-1) DAY) INTO @begofweek;	
	RETURN @begofweek;
END
;;
DELIMITER ;