-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_tour_zones_by_type;

DELIMITER ;;
CREATE PROCEDURE `get_tour_zones_by_type`( IN __place_type_id INT )
BEGIN
SELECT a.* -- ,c.is_public, c.is_adult, c.over_21_required, cz.access_rights ,pg.pass_group_id
	FROM 
		( SELECT zone_instance_id, zone_index, place_type_id, zone_index_plain, raves, `name` 
		FROM tour.tour_weekly_top_raved_hangouts t 
		WHERE place_type_id = __place_type_id 
		ORDER BY raves DESC 
		LIMIT 10) a 
	INNER JOIN kaneva.communities c ON a.zone_instance_id = c.community_id
	INNER JOIN wok.channel_zones cz ON cz.zone_type = 6 AND a.zone_instance_id = cz.zone_instance_id
	LEFT OUTER JOIN wok.pass_group_channel_zones pg ON a.zone_instance_id = pg.zone_instance_id AND pg.zone_type = 6
	WHERE c.is_adult = 'N'
	AND cz.cover_charge = 0
	AND cz.access_rights = 0
	AND ISNULL(pg.pass_group_id)
	AND c.is_public = 'Y'
	AND c.over_21_required = 'N'
	ORDER BY raves, a.name ASC ;
END
;;
DELIMITER ;