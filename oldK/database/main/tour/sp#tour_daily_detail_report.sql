-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS tour_daily_detail_report;

DELIMITER ;;
CREATE PROCEDURE `tour_daily_detail_report`()
BEGIN
SELECT ta.name AS tour_name, p.name, MAX(tph.packet_id)-56 AS last_zone_nbr , MIN(visited_at) AS tour_start_time
	FROM tour.tour_player_history tph
	INNER JOIN tour.tour_active ta ON tph.tour_id = ta.tour_id
	INNER JOIN wok.players p ON tph.player_id = p.player_id
	WHERE visited_at > DATE(NOW()) 
	GROUP BY ta.tour_id, p.player_id
	ORDER BY ta.tour_id, p.player_id;
	
END
;;
DELIMITER ;