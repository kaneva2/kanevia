-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS tourGetNextFamePacketForPlayerWithId;

DELIMITER ;;
CREATE FUNCTION `tourGetNextFamePacketForPlayerWithId`( _playerId INT ) RETURNS int(11)
    READS SQL DATA
BEGIN
   DECLARE __lastPacket INT DEFAULT 0;
   DECLARE __nextPacket int DEFAULT -1;
   
   SELECT  max(packet_id) INTO  __lastPacket
   FROM  tour.tour_packets_awarded 
   WHERE  player_id = _playerId AND date_awarded = curdate();
   
   IF isnull( __lastPacket ) THEN
      SET __nextPacket = 57;
   ELSE
      IF __lastPacket != -1 THEN
         IF __lastPacket < 66 THEN
            SET __nextPacket = __lastPacket + 1;
         END IF;
      END IF;
   END IF;

   RETURN __nextPacket;
END
;;
DELIMITER ;