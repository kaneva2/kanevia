-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS tourIncrementFamePacketForPlayerWithId;

DELIMITER ;;
CREATE PROCEDURE `tourIncrementFamePacketForPlayerWithId`( _playerId INT )
BEGIN
/*
 * Indicates that tour stage fame packet has been redeemed by incrementing the id of 
 * the last tour stage fame packet that was received.  Note that once the player has
 * redeemed their limit of tour stage packets, this procedure sets the id of the last
 * packet_id redeemed to -1.
*/
    DECLARE __lastPacketId INT;

    SELECT tourGetNextFamePacketForPlayerWithId(_playerId ) INTO __lastPacketId;

    IF __lastPacketId <= 66 THEN
       IF __lastPacketId != -1 THEN
         INSERT INTO tour.tour_packets_awarded (player_id,date_awarded,packet_id)
         VALUES( _playerId, curdate(), __lastPacketId )
         ON duplicate key UPDATE packet_id = __lastPacketId;
       END IF;
    END IF;
END
;;
DELIMITER ;