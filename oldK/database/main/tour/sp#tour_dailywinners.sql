-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS tour_dailywinners;

DELIMITER ;;
CREATE PROCEDURE `tour_dailywinners`()
BEGIN

DECLARE done INT;
DECLARE __tour_id INT;
DECLARE __running_tour_id INT;
DECLARE __created_date DATETIME;
DECLARE __player_id INT;
DECLARE __place_id INT;
DECLARE __zone_index INT;
DECLARE __zone_instance_id INT;
DECLARE __zone_name VARCHAR(255);
DECLARE __owner_name VARCHAR(255);
DECLARE __kaneva_user_id INT;
DECLARE __sort_order INT;
DECLARE __rowcount INT;
DECLARE __placeid INT;

DECLARE getTours CURSOR FOR
  SELECT tour_id, kaneva_user_id, sort_order, curr_datetime, running_tour_id, zone_index, zone_instance_id, zone_name, user_name FROM _temp_daily_awards;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

-- get a tour to reward
-- this process should only be run at the end of the day on the day of the awards!
SELECT th.tour_id tourid, tr.running_tour_id runningid
  INTO __tour_id, __running_tour_id 
  FROM tour.tour_history th
       INNER JOIN tour.tour_running tr ON th.tour_id = tr.tour_id
 WHERE awarded = 0 
   AND th.created_date BETWEEN CONCAT(DATE(NOW()),' 00:00:00') 
				AND  CONCAT(DATE(NOW()),' 23:59:59')
ORDER BY tourid DESC, runningid DESC 
LIMIT 1;

-- get all the data into the temp table for processing
DROP TEMPORARY TABLE IF EXISTS _temp_daily_awards;
CREATE TEMPORARY TABLE _temp_daily_awards 
  SELECT __tour_id AS tour_id, c.creator_id AS kaneva_user_id, tz.sort_order, NOW() AS curr_datetime,  __running_tour_id AS running_tour_id, cz.zone_index, 
	cz.zone_instance_id, cz.name AS zone_name, us.username AS user_name
    FROM tour.tour_zones tz  
    INNER JOIN wok.channel_zones cz ON tz.zone_index = cz.zone_index AND tz.zone_instance_id = cz.zone_instance_id
    INNER JOIN kaneva.communities c ON tz.zone_instance_id = c.community_id
    INNER JOIN kaneva.users us ON cz.kaneva_user_id = us.user_id
   WHERE tz.running_tour_id = __running_tour_id  AND tz.zone_instance_id > 0;


SET __rowcount = 0;
SET done = 0;
OPEN getTours;
SET __rowcount = (SELECT FOUND_ROWS());
START TRANSACTION;

REPEAT
	FETCH getTours INTO __tour_id, __kaneva_user_id, __sort_order, __created_date, __running_tour_id, __zone_index, __zone_instance_id, __zone_name, __owner_name;
	IF NOT done THEN
		SET __placeid = __rowcount - __sort_order + 1;
		INSERT INTO tour.tour_pending_awards ( running_tour_id, kaneva_user_id, place, awarded) VALUES (__running_tour_id, __kaneva_user_id, __placeid, 0);
		INSERT INTO tour.tour_winner_history ( tour_id, player_id, place_id, created_date, running_tour_id, zone_index, zone_instance, zone_name, owner_name)
          VALUES (__tour_id, __kaneva_user_id, __placeid, __created_date, __running_tour_id, __zone_index, __zone_instance_id, __zone_name, __owner_name);
	END IF;
UNTIL done END REPEAT;
CLOSE getTours;

-- mark all the tours for the day as processed, we really only look at the last one.
UPDATE tour.tour_history 
	SET awarded = 1 
	WHERE tour_id = __tour_id 
	AND created_date BETWEEN CONCAT(DATE(NOW()),' 00:00:00') AND  CONCAT(DATE(NOW()),' 23:59:59');

COMMIT;

DROP TEMPORARY TABLE IF EXISTS _temp_daily_awards;

SELECT 'Ok';

END
;;
DELIMITER ;