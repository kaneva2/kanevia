-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `schema_versions` (
  `version` decimal(11,4) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

CREATE TABLE `tour_active` (
  `tour_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `begin_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `tour_sql` text,
  PRIMARY KEY (`tour_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tour_active_backup` (
  `tour_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `begin_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `tour_sql` text CHARACTER SET utf8
) ENGINE=InnoDB;

CREATE TABLE `tour_active_test` (
  `tour_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `begin_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `tour_sql` text CHARACTER SET utf8
) ENGINE=InnoDB;

CREATE TABLE `tour_history` (
  `tour_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total_zones` int(11) DEFAULT NULL,
  `awarded` tinyint(1) DEFAULT '0',
  `running_tour_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`tour_id`,`created_date`) USING BTREE,
  KEY `IDX_created_date` (`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tour_packets_awarded` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_awarded` date NOT NULL,
  `player_id` int(11) NOT NULL,
  `packet_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `player_id` (`player_id`,`date_awarded`),
  KEY `date_awarded` (`date_awarded`)
) ENGINE=InnoDB;

CREATE TABLE `tour_pending_awards` (
  `running_tour_id` int(11) NOT NULL,
  `kaneva_user_id` int(11) NOT NULL,
  `place` int(11) NOT NULL,
  `awarded` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`running_tour_id`,`kaneva_user_id`,`place`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tour_player_history` (
  `player_id` int(11) NOT NULL,
  `zone_number` int(11) NOT NULL DEFAULT '0',
  `tour_id` int(11) NOT NULL,
  `packet_id` int(11) NOT NULL DEFAULT '0',
  `visited_at` datetime NOT NULL,
  PRIMARY KEY (`player_id`,`zone_number`,`tour_id`,`packet_id`,`visited_at`),
  KEY `IDX_visited_at` (`visited_at`)
) ENGINE=InnoDB;

CREATE TABLE `tour_running` (
  `running_tour_id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_id` int(11) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `current_zone_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`running_tour_id`),
  KEY `IDX_tour_id` (`tour_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tour_signups` (
  `running_tour_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `zone_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`running_tour_id`,`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tour_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `first_message_time` int(11) DEFAULT '30',
  `first_blast_text` varchar(255) DEFAULT NULL,
  `first_broadcast_text` varchar(255) DEFAULT NULL,
  `second_message_time` int(11) DEFAULT '1',
  `second_blast_text` varchar(255) DEFAULT NULL,
  `second_broadcast_text` varchar(255) DEFAULT NULL,
  `population_max` int(11) DEFAULT '100',
  `zone_duration` int(11) DEFAULT '5',
  `hourly_frequency` int(11) DEFAULT '8',
  `daystorun` char(7) DEFAULT '0',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tour_visitedzones` (
  `running_tour_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `zone_number` int(11) NOT NULL,
  PRIMARY KEY (`running_tour_id`,`player_id`,`zone_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tour_weekly_top_raved_hangouts` (
  `place_type_id` int(11) NOT NULL DEFAULT '0' COMMENT 'communities place type id',
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `zone_index_plain` int(11) NOT NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`zone_instance_id`),
  KEY `IDX_place_type_id` (`place_type_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='tour_weekly_top_raved_hangouts';

CREATE TABLE `tour_winner_history` (
  `tour_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `place_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `running_tour_id` int(11) DEFAULT NULL,
  `zone_index` int(11) DEFAULT NULL,
  `zone_instance` int(11) DEFAULT NULL,
  `zone_name` varchar(255) DEFAULT NULL,
  `owner_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tour_id`,`player_id`,`place_id`,`created_date`),
  KEY `IDX_created_date` (`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tour_winner_rewards` (
  `type_id` int(11) NOT NULL,
  `place_id` int(11) NOT NULL,
  `fame_packet_id` int(11) DEFAULT '0',
  PRIMARY KEY (`type_id`,`place_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tour_zones` (
  `running_tour_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `zone_instance_id` int(11) DEFAULT NULL,
  `zone_index` int(11) DEFAULT NULL,
  PRIMARY KEY (`running_tour_id`,`sort_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

