-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_tour_player_history;

DELIMITER ;;
CREATE PROCEDURE `add_tour_player_history`( IN _player_id INT, IN _zone_number INT, IN _tour_id INT, IN _packet_id INT )
BEGIN

INSERT INTO tour.tour_player_history
	(player_id, zone_number, tour_id, packet_id, visited_at)
	VALUES
	(_player_id, _zone_number, _tour_id, _packet_id, NOW())
	ON DUPLICATE KEY UPDATE visited_at = NOW();
END
;;
DELIMITER ;