-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS tour_manager;

DELIMITER ;;
CREATE PROCEDURE `tour_manager`()
BEGIN
DECLARE done INT DEFAULT 0;
DECLARE __begin_date DATETIME;
DECLARE __tour_id INT;
DECLARE __hourly_frequency INT;
DECLARE __first_message_time INT;
DECLARE __cur_datetime DATETIME;
DECLARE __next_run_date DATETIME;
DECLARE __estimated_end DATETIME;
DECLARE __time_delta INT;
DECLARE __tour_sql VARCHAR(2000);
DECLARE __running_tour_id INT;
DECLARE __sort_order INT;
DECLARE __zone_index INT;
DECLARE __zone_instance INT;
DECLARE __zone_index_plain INT;
DECLARE __zone_raves INT;
DECLARE __next_run_diff TIME;
DECLARE __running_start_time DATETIME;
DECLARE __next_run_minutes INT;
DECLARE __dow_pattern CHAR(7);
DECLARE __rows_processed INT;
DECLARE __begin_minutes DOUBLE;
DECLARE __current_minutes DOUBLE;
DECLARE __minutes_till_next_run INT;
DECLARE __freq_minutes INT;

DECLARE gettopzones CURSOR FOR select zone_index, zone_instance_id, zone_index_plain, raves from _tour_temp;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

SET __cur_datetime     = (SELECT now());
SET __tour_id          = 0;
SET __begin_date       = '0000-00-00';
SET __hourly_frequency = 0;
SET __first_message_time = 0;
SET __tour_sql         = '';
SET __sort_order       = 0;
SET __next_run_diff    = '00:00:00';
SET __next_run_minutes = 0;
SET __dow_pattern      = '_______';
SET __rows_processed   = 0;
SET __dow_pattern = left(concat( left('______', dayofweek(now())-1 ), '1', '______'),7);
-- determine the next active tour and get its records setup if its time
-- remember these only run hourly so we dont have to worry about per minute tours
SELECT tour_id,
	   begin_date, 
       hourly_frequency,
	   first_message_time,
	   tour_sql,
	   DATE_ADD(date(now()), INTERVAL (CEILING( ((HOUR(now())*60)+MINUTE(now())-((HOUR(begin_date)*60)+MINUTE(begin_date))) / (hourly_frequency*60) ) * (hourly_frequency*60))+((HOUR(begin_date)*60)+MINUTE(begin_date)) MINUTE) nextrun 
  INTO __tour_id, __begin_date, __hourly_frequency, __first_message_time, __tour_sql, __next_run_date
  FROM tour.tour_active ta,
       tour.tour_types  tt
 WHERE ta.type_id = tt.type_id 
   AND tt.daystorun like __dow_pattern
 ORDER BY time(nextrun)
 LIMIT 1;

SET __tour_sql = CONCAT('CREATE TEMPORARY TABLE _tour_temp ', __tour_sql);
IF NOT isnull(__tour_id) AND NOT __tour_id = 0 THEN
	-- SET __freq_minutes = __hourly_frequency*60;
	-- SET __begin_minutes = (HOUR(__begin_date)*60)+MINUTE(__begin_date);
	-- SET __current_minutes = (HOUR(__cur_datetime)*60)+MINUTE(__cur_datetime);
	-- SET __minutes_till_next_run = CEILING((__current_minutes - __begin_minutes) / __freq_minutes) * __freq_minutes;
	-- SET __next_run_date = DATE_ADD(DATE(__cur_datetime), INTERVAL __minutes_till_next_run MINUTE);
	-- SET __next_run_date = DATE_ADD(__next_run_date, INTERVAL __begin_minutes MINUTE);
	-- calculate the time in minutes for the return as well as the next section
	SET __next_run_diff = TIMEDIFF( __next_run_date, now() );
	SET __next_run_minutes = HOUR(__next_run_diff)*60 + MINUTE(__next_run_diff);
	IF now() < __next_run_date THEN
		SET __next_run_minutes = __next_run_minutes * -1;
	END IF;
	-- are we within 10 minutes of the start of the tour?
	IF __next_run_minutes >= -10 THEN
		SELECT running_tour_id, start_time into __running_tour_id, __running_start_time FROM tour.tour_running WHERE tour_id = __tour_id LIMIT 1;
		IF NOT isnull(__running_tour_id) THEN
			SET __estimated_end = DATE_ADD(__running_start_time, INTERVAL (__hourly_frequency*60)-15 MINUTE);
		ELSE
			SET __estimated_end = now();
		END IF;
		-- is the running tour not yet started, if so we need to skip
		IF isnull(__running_tour_id) OR now() > __estimated_end THEN
			-- clean up the old tour records for this tour_id, remember this system is designed only to run one tour_id at a time
			-- remove the population limits added for these zones
			DELETE FROM wok.occupancy_limits USING wok.occupancy_limits INNER JOIN tour.tour_zones ON wok.occupancy_limits.zone_index = tour.tour_zones.zone_index and wok.occupancy_limits.zone_instance_id = tour.tour_zones.zone_instance_id WHERE wok.occupancy_limits.zone_instance_id > 0;
			DELETE FROM tour.tour_zones where running_tour_id = __running_tour_id;
			DELETE FROM tour.tour_running WHERE running_tour_id = __running_tour_id;
			-- add the new running tour for this tour_id
			INSERT INTO tour.tour_running (tour_id, start_time, current_zone_id) VALUES (__tour_id, __next_run_date, 1);
			SET __running_tour_id = (select max(running_tour_id) from tour.tour_running);
			
			DROP TABLE IF EXISTS _tour_temp;
			SET @sql = __tour_sql; 
			PREPARE _statement FROM @sql;
            BEGIN
				EXECUTE _statement;
 				
				-- open the query to get the top zones for this tour and add to the tour_zones table
				SET done = 0;
				SET __sort_order = 0;
				OPEN gettopzones;
				REPEAT
					FETCH gettopzones INTO __zone_index, __zone_instance, __zone_index_plain, __zone_raves;
					IF NOT done THEN
						SET __sort_order = __sort_order + 1;
						-- add the tour_zone record
						INSERT INTO tour.tour_zones (running_tour_id, sort_order, zone_index, zone_instance_id) values (__running_tour_id, __sort_order, __zone_index, __zone_instance );
						-- add the occupancy limit records
						DELETE FROM wok.occupancy_limits where zone_index = __zone_index and zone_instance_id = __zone_instance;
						INSERT INTO wok.occupancy_limits ( zone_index, zone_instance_id, zone_index_plain, occupancy_limit_soft, occupancy_limit_hard) 
							values (__zone_index, __zone_instance, __zone_index_plain, 300, 350);						
						SET __rows_processed = __rows_processed + 1;
					END IF;
				UNTIL done END REPEAT;
				CLOSE gettopzones;
			
			END;
			DEALLOCATE PREPARE _statement;	
			DROP TEMPORARY TABLE IF EXISTS _tour_temp;
			IF __rows_processed > 0 THEN
				-- add the history record that we started a tour

				INSERT INTO tour.tour_history (tour_id, created_date, total_zones) values( __tour_id, __next_run_date, __sort_order );

				-- add the kaneva city to the end of the tour
				SET __sort_order = __sort_order + 1;
				INSERT INTO tour.tour_zones(running_tour_id, sort_order, zone_index, zone_instance_id) values (__running_tour_id, __sort_order, 1073741857, 0 );
			ELSE
				DELETE FROM tour.tour_visitedzones WHERE running_tour_id = __running_tour_id;
				DELETE FROM tour.tour_running WHERE running_tour_id = __running_tour_id;
			END IF;
		END IF;
	END IF;
	IF __rows_processed > 0 THEN
		SELECT __tour_id as TourId, __next_run_minutes as MinutesTillNextTour, __next_run_date as NxtTourTime, __begin_date as BeginDate, __hourly_frequency as HourlyFrequency, __next_run_diff as TimeDelta;
	ELSE
		SELECT -98 as TourId, __next_run_minutes as MinutesTillNextTour, __next_run_date as NxtTourTime, __begin_date as BeginDate, 0 as HourlyFrequency, 0 as TimeDelta;
	END IF;
ELSE
	SELECT -99 as TourId, 0 as MinutesTillNextTour, '0000-00-00' as NxtTourTime, __begin_date as BeginDate, 0 as HourlyFrequency, 0 as TimeDelta;
END IF;
END
;;
DELIMITER ;