-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS setLogLevel;

DELIMITER ;;
CREATE PROCEDURE `setLogLevel`(requested_log_level VARCHAR(10))
BEGIN
	-- DEPRECATED:  5/5/2014
	-- This procedure is being deprecated to avoid confusion.
	-- It should not have been called from within a stored procedure.
	-- The purpose was to make it easy to set the master log level, but
	-- I think most people will use some kind of GUI-driven tool for that.
END
;;
DELIMITER ;