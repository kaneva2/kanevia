-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS logMsg;

DELIMITER ;;
CREATE PROCEDURE `logMsg`(__log_level VARCHAR(10), __schema VARCHAR(50), __routine VARCHAR(50), __message VARCHAR(1000))
BEGIN
-- This is the wrapper procedure for the UDF udf_log_message function call.
-- We will also replace the existing logMessage() procedure with appropriate wrappings.
	DECLARE __master_log_level_value INTEGER;
	DECLARE __control_log_level_value INTEGER;
	DECLARE __log_level_value INTEGER;
	DECLARE __empty_result VARCHAR(10);

	-- get the master log level value
	SELECT value INTO __master_log_level_value FROM logging.log_levels ll WHERE ll.level =
		(SELECT value FROM logging.logging_settings ls WHERE ls.name = 'LOG_LEVEL');

	-- look for our control log level value, using the master where no control exists
	SELECT COALESCE(MIN(value), __master_log_level_value) INTO __control_log_level_value
	FROM logging.log_levels ll WHERE ll.level =
		(SELECT log_level FROM logging.logging_control WHERE `schema` = __schema AND routine = __routine);

	-- ONLY do logging if the controlling log level is greater then 0 (OFF)
	IF (__control_log_level_value > 0) THEN
		-- get the value of the requested log level
		SELECT COALESCE(MIN(value), 0) INTO __log_level_value FROM logging.log_levels WHERE level = __log_level;
		-- ONLY log if the requested level is in the acceptable range
		IF (__log_level_value BETWEEN 1 AND __control_log_level_value) THEN
             -- SELECT udf_log_message(CONNECTION_ID(), __log_level, __schema, __routine, __message) INTO __empty_result;
			 INSERT INTO logging.log_data 
			 VALUES(   NULL
					   , now()
					   , CONNECTION_ID()
					   , COALESCE(__log_level	, 'nil level')
					   , COALESCE(__schema		, 'nil schema')
					   , COALESCE(__routine		, 'nil routine')
					   , COALESCE(__message		, 'nil message' ) );
		END IF;
	END IF;
END
;;
DELIMITER ;
