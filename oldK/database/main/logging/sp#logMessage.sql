-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS logMessage;

DELIMITER ;;
CREATE PROCEDURE `logMessage`(__caller VARCHAR(200), __requested_log_level VARCHAR(10), __message VARCHAR(512))
BEGIN
	DECLARE __routine VARCHAR(50);

	-- check and set the calling routine
	-- This parameter is shortened in the new version of logging from the old version.  So, we have to
	-- check it for length and cut it down, if necessary.
	IF (length(__caller) > 50) THEN
		SET __routine := left(__caller, 50);
	ELSE
		SET __routine := __caller;
	END IF;

	-- pass all parameters along to the new logging format
	-- also provide the schema name, which probably will always be 'logging'
	CALL logMsg(__requested_log_level, SCHEMA(), __routine, __message);
END
;;
DELIMITER ;