-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use logging;
drop table if exists log_data;

create table if not exists log_data
(
	tstamp	varchar(50) not null,
	connection_id bigint not null,
	level varchar(10) not null,
	`schema` varchar(50) not null,
	routine varchar(50) not null,
	message varchar(1000) not null,
	primary key (tstamp, connection_id)
) engine=innodb;

