#include <my_global.h>
#include <my_sys.h>
#include <mysql.h>

#include <stdio.h>
#include <pthread.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

#define ERR_ARG "udf_log_message requires 5 parameters (int, string, string, string, string)"

extern "C" {
   my_bool udf_log_message_init(UDF_INIT *initid, UDF_ARGS *args, char *message);
   my_bool udf_log_message( UDF_INIT *initid, UDF_ARGS *args, char *result, unsigned long *length, char *is_null, char *error);
}

my_bool udf_log_message_init(UDF_INIT *initid, UDF_ARGS *args, char *message) {
	if (args->arg_count == 5) {
		args->arg_type[0] = INT_RESULT;		// connection id
		args->arg_type[1] = STRING_RESULT;	// log level
		args->arg_type[2] = STRING_RESULT;	// schema name
		args->arg_type[3] = STRING_RESULT;	// routine name
		args->arg_type[4] = STRING_RESULT;	// message
		initid->maybe_null = 0;
	} else {
		strcpy(message, ERR_ARG);
		return 1;
	}
   return 0;
}

my_bool udf_log_message(UDF_INIT *initid, UDF_ARGS *args, char *result, unsigned long *length, char *is_null, char *error) {
	// generate a MySQL compatible timestamp with microseconds appended
	struct timeval tv;
	struct tm* ptm;
	char time_string[20]; /* e.g. "2006-04-27 17:10:52" */
	char usec_time_string[30];
	time_t t;
	char output_buffer[1500];
	int buffer_size;
	char output_filename[50];

	/* Obtain the time of day, and convert it to a tm struct. */
	gettimeofday (&tv, NULL);
	t = (time_t)tv.tv_sec;
	ptm = localtime (&t);   

	/* Format the date and time, down to a single second.  */
	strftime (time_string, sizeof (time_string), "%Y-%m-%d %H:%M:%S", ptm);

	/* Print the formatted time, in seconds, followed by a decimal point and the microseconds.  */
	sprintf(usec_time_string, "%s.%06ld", time_string, tv.tv_usec);

	// convert the connection_id to integer form
	unsigned int connection_id = *((unsigned int *)args->args[0]);

	// create the output
	buffer_size = sprintf(output_buffer, "%s\t%u\t%s\t%s\t%s\t%s\n", usec_time_string, connection_id, args->args[1], args->args[2], args->args[3], args->args[4]);

	// create the proper file name
	sprintf(output_filename, "/tmp/logger_%d.dat", (unsigned int)pthread_self());

	// append log message to the log file
	FILE *logfile = fopen(output_filename, "a");
	fwrite(&output_buffer, 1, buffer_size, logfile);
	fclose(logfile);
	return 0;
}

