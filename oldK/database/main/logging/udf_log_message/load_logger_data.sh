#!/bin/bash

LOGGERDAT="/tmp/logger.dat"
LOGDATDAT="/tmp/logdat.dat"

cd /tmp
LOGFILECOUNT=`ls logger*.dat | wc | awk '{ print $1; }'`
if [ "${LOGFILECOUNT}" != "0" ]; then
	rename logger_ logdat_ logger_*.dat
	cat logdat_*.dat > ${LOGDATDAT}
	if [ -e $LOGDATDAT ]; then
		mysql logging -e "LOAD DATA INFILE '$LOGDATDAT' INTO TABLE log_data;"
	fi
	rm -f $LOGDATDAT
	rm -f logdat_*.dat
fi
