#include <my_global.h>
#include <my_sys.h>
#include <mysql.h>

#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

extern "C" {
   my_bool udf_log_message_init(UDF_INIT *initid, UDF_ARGS *args, char *message);
   my_bool udf_log_message( UDF_INIT *initid, UDF_ARGS *args, char *result, unsigned long *length, char *is_null, char *error);
}

my_bool udf_log_message_init(UDF_INIT *initid, UDF_ARGS *args, char *message) {
   return 0;
}

my_bool udf_log_message(UDF_INIT *initid, UDF_ARGS *args, char *result, unsigned long *length, char *is_null, char *error) {
	return 0;
}

