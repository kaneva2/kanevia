-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `logging_control` (
  `schema` varchar(50) NOT NULL COMMENT 'Schema to be controlled for logging.',
  `routine` varchar(50) NOT NULL COMMENT 'Routine to be controlled for logging.',
  `log_level` varchar(10) NOT NULL COMMENT 'Log level to log messages for this schema.',
  PRIMARY KEY (`schema`,`routine`),
  KEY `logging_control_log_level_fk` (`log_level`),
  CONSTRAINT `logging_control_log_level_fk` FOREIGN KEY (`log_level`) REFERENCES `log_levels` (`level`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Controls which messages get logged for each schema and log level';

CREATE TABLE `logging_settings` (
  `name` varchar(100) NOT NULL COMMENT 'Name of the logging setting variable.',
  `value` varchar(100) NOT NULL COMMENT 'Value of the logging setting variable, to be interpreted by the reader.',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB;

CREATE TABLE `log_data` (
  `tstamp` varchar(50) NOT NULL,
  `connection_id` bigint(20) NOT NULL,
  `level` varchar(10) NOT NULL,
  `schema` varchar(50) NOT NULL,
  `routine` varchar(50) NOT NULL,
  `message` varchar(1000) NOT NULL,
  PRIMARY KEY (`tstamp`,`connection_id`)
) ENGINE=InnoDB;

CREATE TABLE `log_levels` (
  `level` varchar(10) NOT NULL COMMENT 'Name of the logging level.',
  `value` int(11) NOT NULL COMMENT 'Value of the logging level.',
  PRIMARY KEY (`level`)
) ENGINE=InnoDB;

CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL,
  `description` varchar(250) NOT NULL,
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `interface_change` enum('T','F') DEFAULT 'F',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

CREATE TABLE `sp_log` (
  `tstamp` varchar(100) NOT NULL COMMENT 'Timestamp of the logging event',
  `connection_id` bigint(20) NOT NULL COMMENT 'Connection ID creating the logging event',
  `caller` varchar(200) NOT NULL COMMENT 'Name of the stored procedure creating the logging event',
  `level` varchar(10) NOT NULL DEFAULT 'OFF' COMMENT 'Logging level.',
  `message` varchar(512) NOT NULL COMMENT 'Message',
  PRIMARY KEY (`tstamp`,`connection_id`),
  KEY `sp_log_alt_level` (`level`)
) ENGINE=MyISAM;

