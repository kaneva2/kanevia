-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS flushLog;

DELIMITER ;;
CREATE PROCEDURE `flushLog`()
BEGIN
	-- DEPRECATED: 5/5/2014
	-- This procedure no longer serves any useful purpose.
	-- I'm stubbing it to save execution time and resources.
END
;;
DELIMITER ;