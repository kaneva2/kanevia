-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

USE logging;
SELECT 'Starting Promotion... ',NOW();


DROP TABLE IF EXISTS log_data;
CREATE TABLE log_data (
  log_id 			int(11) 		NOT NULL auto_increment
  , tstamp 			varchar(50) 	NOT NULL
  , connection_id 	bigint(20) 		NOT NULL
  , level 			varchar(10) 	NOT NULL
  , schema_name		varchar(50) 	NOT NULL
  , routine 		varchar(50) 	NOT NULL
  , message 		varchar(1000) 	NOT NULL
  , PRIMARY KEY (log_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- stored routines
--
\. sp#logMsg.sql


-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (5, 'Remove references to udf_log_message as there is no support for udfs in AWS mysql hosting.', NOW()  );

SELECT 'Finished Promotion... ',NOW();



