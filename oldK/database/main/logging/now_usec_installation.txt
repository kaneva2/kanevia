To build the shared object, you must have gcc and gcc-c++ install on the Linux machine.  You also need the appropriate MySQL-devel packages installed.

Compile:
	g++ -fPIC -Wall -I/usr/include/mysql -shared -o now_usec.so now_usec.cc

Copy the shared object to the machine's lib directory:
	cp now_usec.so /usr/lib64/mysql/plugins  (for MySQL 5.5+)
	cp now_usec.so /usr/lib64 (for MySQL 5.0/5.1)

Create the user defined function in the database:
	create function now_usec returns string soname 'now_usec.so';

