-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS purgeLog;

DELIMITER ;;
CREATE PROCEDURE `purgeLog`( purge_before_date DATE )
BEGIN
	DELETE FROM `logging`.`log_data` WHERE tstamp < purge_before_date;
END
;;
DELIMITER ;