-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

USE logging;
SELECT 'Starting Promotion... ',NOW();

-- stored routines
--
\. sp#logMsg.sql


-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (6, 'Add protection against null inputs.', NOW()  );

SELECT 'Finished Promotion... ',NOW();



