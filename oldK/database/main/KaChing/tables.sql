-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `arena` (
  `arena_id` int(11) NOT NULL AUTO_INCREMENT,
  `arena_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`arena_id`)
) ENGINE=InnoDB;

CREATE TABLE `game` (
  `game_id` int(11) NOT NULL AUTO_INCREMENT,
  `arena_id` int(11) NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `game_type_id` int(11) NOT NULL,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`game_id`),
  KEY `gametype_game_FK` (`game_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_type` (
  `game_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_type` varchar(100) NOT NULL,
  PRIMARY KEY (`game_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `player_game` (
  `player_game_id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `team` varchar(45) DEFAULT NULL,
  `quit_early` tinyint(1) DEFAULT NULL,
  `win_lose` tinyint(4) DEFAULT NULL,
  `num_kills` int(11) DEFAULT NULL,
  `num_deaths` int(11) DEFAULT NULL,
  PRIMARY KEY (`player_game_id`),
  KEY `game_playergame_FK` (`game_id`),
  KEY `IDX_player_id` (`player_id`),
  CONSTRAINT `game_playergame_FK` FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`)
) ENGINE=InnoDB AVG_ROW_LENGTH=65 ROW_FORMAT=COMPACT;

CREATE TABLE `player_game_coin_stats` (
  `player_game_coin_stats_id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `coin_pickups` int(11) DEFAULT '0',
  `coin_drops` int(11) DEFAULT '0',
  `coin_scores` int(11) DEFAULT NULL,
  PRIMARY KEY (`player_game_coin_stats_id`),
  KEY `game_playergamecoinstats_FK` (`game_id`),
  CONSTRAINT `game_playergamecoinstats_FK` FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`)
) ENGINE=InnoDB AVG_ROW_LENGTH=65 ROW_FORMAT=COMPACT;

CREATE TABLE `player_game_kill_stats` (
  `player_game_kill_stats_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL,
  `killer_id` int(11) NOT NULL,
  `killed_id` int(11) NOT NULL,
  `total_kills` int(11) DEFAULT NULL,
  PRIMARY KEY (`player_game_kill_stats_id`),
  KEY `game_playergamekillstats_FK` (`game_id`),
  CONSTRAINT `game_playergamekillstats_FK` FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`)
) ENGINE=InnoDB AVG_ROW_LENGTH=182 ROW_FORMAT=COMPACT;

