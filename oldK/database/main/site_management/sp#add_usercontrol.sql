-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_usercontrol;

DELIMITER ;;
CREATE PROCEDURE `add_usercontrol`(UserControlName VARCHAR(200), ControlTypeId INTEGER UNSIGNED)
BEGIN	

  START TRANSACTION;

    

    INSERT INTO user_controls (user_control_name, control_type_id) VALUES (UserControlName, ControlTypeId);

    

    SELECT LAST_INSERT_ID();

  COMMIT;

END
;;
DELIMITER ;