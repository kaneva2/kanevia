-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_new_privilege;

DELIMITER ;;
CREATE PROCEDURE `add_new_privilege`(privilegeName VARCHAR(50), privilegeDescription VARCHAR(100), adminOnly TINYINT)
BEGIN
 
  START TRANSACTION;
  
    
    /* update the records */
    INSERT INTO site_privileges (privilege_name, privilege_description, site_administrators_only) VALUES (privilegeName, privilegeDescription, adminOnly);

    /*return the auto id*/
    SELECT LAST_INSERT_ID();
    
    
  COMMIT;
END
;;
DELIMITER ;