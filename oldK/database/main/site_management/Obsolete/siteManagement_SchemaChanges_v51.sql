-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS siteManagement_SchemaChanges_v51;
DELIMITER //
CREATE PROCEDURE siteManagement_SchemaChanges_v51()
BEGIN

CREATE TABLE `site_management`.`user_control_type` (
  `control_type_id` INTEGER UNSIGNED NOT NULL DEFAULT 0 COMMENT 'manually create id',
  `control_type` VARCHAR(45) NOT NULL DEFAULT '' COMMENT 'display description',
  PRIMARY KEY(`control_type_id`)
)
ENGINE = MYISAM
COMMENT = 'second normal form table for use control types';

ALTER TABLE `site_management`.`user_controls` ADD COLUMN `control_type_id` INTEGER UNSIGNED NOT NULL DEFAULT 1 AFTER `user_control_path`;

ALTER TABLE `site_management`.`user_controls` ADD CONSTRAINT `FK_user_controls_1` FOREIGN KEY `FK_user_controls_1` (`control_type_id`)
    REFERENCES `user_control_type` (`control_type_id`)
    ON UPDATE CASCADE;
    
ALTER TABLE `site_management`.`user_controls` CHANGE COLUMN `user_control_path` `user_control_name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '';

DELIMITER ;

CALL siteManagement_SchemaChanges_v51;
DROP PROCEDURE IF EXISTS siteManagement_SchemaChanges_v51;

