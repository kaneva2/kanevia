-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- add new databbase site management
\. siteManagement_DBInstall_v50.sql


-- add site management procedures
\. sp#add_default_game_roles.sql
\. sp#add_new_privilege.sql
\. sp#add_new_privilege_to_role.sql
\. sp#add_new_role.sql
\. sp#delete_all_role_privileges.sql
\. sp#delete_privilege.sql
\. sp#delete_role.sql
\. sp#update_privilege.sql


-- add site management version number
DROP PROCEDURE IF EXISTS sitemanagement_SchemaChanges_v50;
DELIMITER //
CREATE PROCEDURE sitemanagement_SchemaChanges_v50()
BEGIN
IF NOT EXISTS (SELECT 1 FROM schema_versions WHERE version = 50.0) THEN
	INSERT INTO `schema_versions` ( `version`, `description` ) 
		VALUES (50.0, 'Invitations/security upgrade (initial version)' );
END IF;

END //

DELIMITER ;

CALL sitemanagement_SchemaChanges_v50;
DROP PROCEDURE IF EXISTS sitemanagement_SchemaChanges_v50;