-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- MySQL dump 10.10
--
-- Host: localhost    Database: site_management
-- ------------------------------------------------------
-- Server version	5.0.22-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE IF NOT EXISTS site_management;

USE site_management;

--
-- CREATE TABLE: schema_versions
--

CREATE TABLE IF NOT EXISTS `schema_versions`

(
	version INT NOT NULL COMMENT 'current schema version',
	description VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'description of update',
	date_applied TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
	CONSTRAINT PRIMARY KEY (version)
)
	ENGINE = InnoDB
	COLLATE latin1_swedish_ci
	ROW_FORMAT = COMPACT;


--
-- Table structure for table `access_levels`
--

DROP TABLE IF EXISTS `access_levels`;
CREATE TABLE `access_levels` (
  `access_level_id` smallint(5) unsigned NOT NULL default '0' COMMENT 'assigne value for level',
  `access_level` varchar(45) NOT NULL default '1' COMMENT 'description of access level',
  PRIMARY KEY  (`access_level_id`)
) ENGINE=InnoDB COMMENT '2nd normal table for privilege access levels' DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_levels`
--


/*!40000 ALTER TABLE `access_levels` DISABLE KEYS */;
LOCK TABLES `access_levels` WRITE;
INSERT INTO `access_levels` VALUES (1,'None'),(2,'Read Only'),(3,'Read/Write');
UNLOCK TABLES;
/*!40000 ALTER TABLE `access_levels` ENABLE KEYS */;

--
-- Table structure for table `roles_to_privileges`
--

DROP TABLE IF EXISTS `roles_to_privileges`;
CREATE TABLE `roles_to_privileges` (
  `site_role_id` int(10) unsigned NOT NULL default '1' COMMENT 'id of the security role across sites',
  `company_id` int(10) unsigned NOT NULL default '1' COMMENT 'id of company owning role',
  `privilege_id` int(10) unsigned NOT NULL default '1' COMMENT 'id of privilege',
  `access_level_id` smallint(5) unsigned NOT NULL default '1' COMMENT 'id of access level',
  PRIMARY KEY  (`company_id`,`privilege_id`,`site_role_id`),
  KEY `FK_accesslevel` (`access_level_id`),
  KEY `FK_siteRole` (`site_role_id`),
  CONSTRAINT `FK_accesslevel` FOREIGN KEY (`access_level_id`) REFERENCES `access_levels` (`access_level_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_siteRole` FOREIGN KEY (`site_role_id`) REFERENCES `site_roles` (`site_role_id`)
) ENGINE=InnoDB COMMENT 'many to many relating roles to privleges' DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles_to_privileges`
--


/*!40000 ALTER TABLE `roles_to_privileges` DISABLE KEYS */;
LOCK TABLES `roles_to_privileges` WRITE;
INSERT INTO `roles_to_privileges` VALUES (32,12,12,1),(33,12,12,1),(33,12,13,1),(33,12,15,1),(35,23,12,1),(36,23,12,1),(36,23,13,1),(36,23,15,1),(33,12,11,2),(32,12,13,2),(33,12,14,2),(32,12,15,2),(36,23,11,2),(35,23,13,2),(36,23,14,2),(35,23,15,2),(31,12,11,3),(32,12,11,3),(31,12,12,3),(31,12,13,3),(31,12,14,3),(32,12,14,3),(31,12,15,3),(34,23,11,3),(35,23,11,3),(34,23,12,3),(34,23,13,3),(34,23,14,3),(35,23,14,3),(34,23,15,3);
UNLOCK TABLES;
/*!40000 ALTER TABLE `roles_to_privileges` ENABLE KEYS */;

--
-- Table structure for table `site_privileges`
--

DROP TABLE IF EXISTS `site_privileges`;
CREATE TABLE `site_privileges` (
  `privilege_id` int(10) unsigned NOT NULL auto_increment COMMENT 'auto number for privilege',
  `privilege_name` varchar(50) NOT NULL default '' COMMENT 'name for the priviledge',
  `privilege_description` varchar(100) NOT NULL default '' COMMENT 'description of the privilege',
  `site_administrators_only` tinyint(3) unsigned NOT NULL default '1' COMMENT 'default to only site admins to prevent accidental deply to site users',
  PRIMARY KEY  (`privilege_id`)
) ENGINE=InnoDB COMMENT 'stores available privileges for managed sites' DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_privileges`
--


/*!40000 ALTER TABLE `site_privileges` DISABLE KEYS */;
LOCK TABLES `site_privileges` WRITE;
INSERT INTO `site_privileges` VALUES (11,'Team Members','List of team members',0),(12,'Company Account','Billing account information',0),(13,'Company Information','Company name and contact information',0),(14,'Company Stars','Create, edit, and delete Stars',0),(15,'Team Member Roles','Create, edit, and delete team member roles',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `site_privileges` ENABLE KEYS */;

--
-- Table structure for table `site_roles`
--

DROP TABLE IF EXISTS `site_roles`;
CREATE TABLE `site_roles` (
  `site_role_id` int(10) unsigned NOT NULL auto_increment COMMENT 'auto numbered id for role across sites',
  `role_id` smallint(5) unsigned NOT NULL default '0' COMMENT 'role id that is contextual within a site (mostly for use with kaneva.com)',
  `role_description` varchar(45) NOT NULL default '' COMMENT 'description of the role',
  `company_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`site_role_id`)
) ENGINE=InnoDB COMMENT 'stores roles for all sites' DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_roles`
--


/*!40000 ALTER TABLE `site_roles` DISABLE KEYS */;
LOCK TABLES `site_roles` WRITE;
INSERT INTO `site_roles` VALUES (1,1,'System',0),(2,2,'Site Member',0),(3,4,'Beta Tester',0),(4,8,'Site Administrator',0),(5,16,'Customer Service Rep.',0),(6,24,'SITEADMIN -CSR',0),(7,32,'Information Technology',0),(8,56,'SITEADMIN-CSR-IT',0),(31,1,'Administrator',12),(32,2,'Super User',12),(33,3,'Associate',12),(34,1,'Administrator',23),(35,2,'Super User',23),(36,3,'Associate',23);
UNLOCK TABLES;
/*!40000 ALTER TABLE `site_roles` ENABLE KEYS */;

--
-- Table structure for table `sm_available_websites`
--

DROP TABLE IF EXISTS `sm_available_websites`;
CREATE TABLE `sm_available_websites` (
  `website_id` smallint(5) unsigned NOT NULL auto_increment COMMENT 'auto id for available websites',
  `website_name` varchar(45) NOT NULL default '' COMMENT 'name of website (for display)',
  `connection_string` varchar(300) default NULL COMMENT 'connection string to websites database',
  `website_url` varchar(45) NOT NULL default '' COMMENT 'URL to the website',
  PRIMARY KEY  (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sm_available_websites`
--


/*!40000 ALTER TABLE `sm_available_websites` DISABLE KEYS */;
LOCK TABLES `sm_available_websites` WRITE;
INSERT INTO `sm_available_websites` VALUES (1,'Kaneva',NULL,'www.kaneva.com'),(2,'Developer',NULL,'developer.kaneva.com/'),(3,'Shopping',NULL,'shopping.kaneva.com'),(4,'Designer',NULL,'designer.kaneva.com'),(5,'Site Management',NULL,'');
UNLOCK TABLES;
/*!40000 ALTER TABLE `sm_available_websites` ENABLE KEYS */;

--
-- Table structure for table `sm_mainnav`
--

DROP TABLE IF EXISTS `sm_mainnav`;
CREATE TABLE `sm_mainnav` (
  `csr_mainnav_id` int(10) unsigned NOT NULL auto_increment COMMENT 'auto id for the navigation item',
  `csr_mainnav_title` varchar(60) NOT NULL default '' COMMENT 'display value for navigation item',
  `privilege_id` int(10) unsigned NOT NULL default '0' COMMENT 'id of privilege needed to access the navigation item',
  `website_id` smallint(5) unsigned NOT NULL default '0' COMMENT 'id of the website to which the navigation item belongs',
  PRIMARY KEY  (`csr_mainnav_id`),
  KEY `FK_csr_website_id` (`website_id`),
  KEY `FK_privilege_id_mainnav` (`privilege_id`),
  CONSTRAINT `FK_csr_website_id` FOREIGN KEY (`website_id`) REFERENCES `sm_available_websites` (`website_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_privilege_id_mainnav` FOREIGN KEY (`privilege_id`) REFERENCES `site_privileges` (`privilege_id`) ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT 'stores main navigation menu items for all websites' DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sm_mainnav`
--


/*!40000 ALTER TABLE `sm_mainnav` DISABLE KEYS */;
LOCK TABLES `sm_mainnav` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sm_mainnav` ENABLE KEYS */;

--
-- Table structure for table `sm_subnav`
--

DROP TABLE IF EXISTS `sm_subnav`;
CREATE TABLE `sm_subnav` (
  `csr_subnav_id` int(10) unsigned NOT NULL auto_increment COMMENT 'auto if for the sub navigation item',
  `csr_mainnav_id` int(10) unsigned NOT NULL default '0' COMMENT 'fk id to the main name which calls this sub nav',
  `csr_subnav_title` varchar(60) NOT NULL default '' COMMENT 'display value for the sub navigation item',
  `user_control_id` int(10) unsigned NOT NULL default '0' COMMENT 'fk id to the user control table to indicate which user control to load',
  `privilege_id` int(10) unsigned NOT NULL default '0' COMMENT 'fk id to indicate which privilege is needed to see this menu item',
  PRIMARY KEY  (`csr_subnav_id`),
  KEY `FK_privilege_id_subnav` (`privilege_id`),
  KEY `FK_csr_usercontrol_id` (`user_control_id`),
  KEY `FK_csr_mainMenu_id` (`csr_mainnav_id`),
  CONSTRAINT `FK_csr_mainMenu_id` FOREIGN KEY (`csr_mainnav_id`) REFERENCES `sm_mainnav` (`csr_mainnav_id`),
  CONSTRAINT `FK_csr_usercontrol_id` FOREIGN KEY (`user_control_id`) REFERENCES `user_controls` (`user_control_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_privilege_id_subnav` FOREIGN KEY (`privilege_id`) REFERENCES `site_privileges` (`privilege_id`) ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT 'stores sub navigation menu options for all websites' DEFAULT CHARSET=latin1 COMMENT='InnoDB free: 54272 kB; (`user_control_id`) REFER `customer_s';

--
-- Dumping data for table `sm_subnav`
--


/*!40000 ALTER TABLE `sm_subnav` DISABLE KEYS */;
LOCK TABLES `sm_subnav` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sm_subnav` ENABLE KEYS */;

--
-- Table structure for table `user_controls`
--

DROP TABLE IF EXISTS `user_controls`;
CREATE TABLE `user_controls` (
  `user_control_id` int(10) unsigned NOT NULL auto_increment COMMENT 'auto id for the user control',
  `user_control_path` varchar(200) NOT NULL default '' COMMENT 'path to the user control for loading',
  PRIMARY KEY  (`user_control_id`)
) ENGINE=InnoDB COMMENT 'stores the paths to all user controls for the site management tool' DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_controls`
--


/*!40000 ALTER TABLE `user_controls` DISABLE KEYS */;
LOCK TABLES `user_controls` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `user_controls` ENABLE KEYS */;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
DELIMITER ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

