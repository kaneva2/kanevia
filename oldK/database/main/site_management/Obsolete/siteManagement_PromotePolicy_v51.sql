-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 


-- add new tables and modify others
\. siteManagement_SchemaChanges_v51.sql
\. siteManagement_SeedData_v51.sql


-- add site management version number
DROP PROCEDURE IF EXISTS sitemanagement_SchemaChanges_v51;
DELIMITER //
CREATE PROCEDURE sitemanagement_SchemaChanges_v51()
BEGIN
IF NOT EXISTS (SELECT 1 FROM schema_versions WHERE version = 51.0) THEN
	INSERT INTO `schema_versions` ( `version`, `description` ) 
		VALUES (51.0, 'Invitations/security upgrade (initial version)' );
END IF;

END //

DELIMITER ;

CALL sitemanagement_SchemaChanges_v51;
DROP PROCEDURE IF EXISTS sitemanagement_SchemaChanges_v51;