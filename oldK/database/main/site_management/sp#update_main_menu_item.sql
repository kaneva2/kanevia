-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_main_menu_item;

DELIMITER ;;
CREATE PROCEDURE `update_main_menu_item`(NavigationTitle VARCHAR(60), PrivilegeId INTEGER UNSIGNED, WebsiteId SMALLINT UNSIGNED, NavigationId INTEGER UNSIGNED)
BEGIN

  START TRANSACTION;

    
    UPDATE sm_mainnav SET csr_mainnav_title = NavigationTitle, privilege_id = PrivilegeId, website_id = WebsiteId WHERE csr_mainnav_id = NavigationId;

    
    SELECT row_count();

  COMMIT;

END
;;
DELIMITER ;