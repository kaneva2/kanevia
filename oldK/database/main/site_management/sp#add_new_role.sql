-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_new_role;

DELIMITER ;;
CREATE PROCEDURE `add_new_role`(roleId INT UNSIGNED, roleDescription	VARCHAR(45), companyId	INT UNSIGNED)
BEGIN	
  START TRANSACTION;
  
    /*insert new record*/
    INSERT INTO site_roles (role_id, role_description, company_id) VALUES (roleId, roleDescription, companyId);

    /*return the auto id*/
    SELECT LAST_INSERT_ID();

  COMMIT;
END
;;
DELIMITER ;