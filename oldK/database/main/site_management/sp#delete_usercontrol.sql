-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_usercontrol;

DELIMITER ;;
CREATE PROCEDURE `delete_usercontrol`(UserControlId INTEGER UNSIGNED)
BEGIN

  START TRANSACTION;

    
    DELETE FROM user_controls WHERE user_control_id = UserControlId;

    
    SELECT row_count();

  COMMIT;

END
;;
DELIMITER ;