-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_default_game_roles;

DELIMITER ;;
CREATE PROCEDURE `add_default_game_roles`(companyId BIGINT UNSIGNED)
BEGIN	

  declare _adminRoleName varchar(45) default 'Administrator';
  declare _superUserRoleName varchar(45) default 'Super User';
  declare _associateRoleName varchar(45) default 'Associate';

  declare _adminSiteRoleId int default 0;
  declare _superUserSiteRoleId int default 0;
  declare _associateSiteRoleId int default 0;



  START TRANSACTION;

    /*add the default roles */
    call add_new_role(1, _adminRoleName, companyId);
    SELECT site_role_id INTO _adminSiteRoleId FROM site_roles WHERE role_id = 1 AND company_id = companyId;

    CALL add_new_role(2, _superUserRoleName, companyId);
    SELECT site_role_id INTO _superUserSiteRoleId FROM site_roles WHERE role_id = 2 AND company_id = companyId;

    CALL add_new_role(3, _associateRoleName, companyId);
    SELECT site_role_id INTO _associateSiteRoleId FROM site_roles WHERE role_id = 3 AND company_id = companyId;


    /*add the admin privileges*/
    CALL add_new_privilege_to_role(_adminSiteRoleId, companyId, 11, 3);
    CALL add_new_privilege_to_role(_adminSiteRoleId, companyId, 12, 3);
    CALL add_new_privilege_to_role(_adminSiteRoleId, companyId, 13, 3);
    CALL add_new_privilege_to_role(_adminSiteRoleId, companyId, 14, 3);
    CALL add_new_privilege_to_role(_adminSiteRoleId, companyId, 15, 3);

    /*add the superUser privileges*/
    CALL add_new_privilege_to_role(_superUserSiteRoleId, companyId, 11, 3);
    CALL add_new_privilege_to_role(_superUserSiteRoleId, companyId, 12, 1);
    CALL add_new_privilege_to_role(_superUserSiteRoleId, companyId, 13, 2);
    CALL add_new_privilege_to_role(_superUserSiteRoleId, companyId, 14, 3);
    CALL add_new_privilege_to_role(_superUserSiteRoleId, companyId, 15, 2);
    
    /*add the associate privileges*/
    CALL add_new_privilege_to_role(_associateSiteRoleId, companyId, 11, 2);
    CALL add_new_privilege_to_role(_associateSiteRoleId, companyId, 12, 1);
    CALL add_new_privilege_to_role(_associateSiteRoleId, companyId, 13, 1);
    CALL add_new_privilege_to_role(_associateSiteRoleId, companyId, 14, 2);
    CALL add_new_privilege_to_role(_associateSiteRoleId, companyId, 15, 1);    

  COMMIT;
END
;;
DELIMITER ;