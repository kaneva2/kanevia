-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_main_menu_item;

DELIMITER ;;
CREATE PROCEDURE `add_main_menu_item`(NavigationTitle VARCHAR(60), PrivilegeId INTEGER UNSIGNED, WebsiteId SMALLINT UNSIGNED)
BEGIN	

  START TRANSACTION;

    
    INSERT INTO sm_mainnav (csr_mainnav_title, privilege_id, website_id) VALUES (NavigationTitle, PrivilegeId, WebsiteId);

    
    SELECT LAST_INSERT_ID();

  COMMIT;

END
;;
DELIMITER ;