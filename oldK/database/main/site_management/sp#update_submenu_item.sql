-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_submenu_item;

DELIMITER ;;
CREATE PROCEDURE `update_submenu_item`(MainNavId INTEGER UNSIGNED, NavigationTitle VARCHAR(60), UserControlId INTEGER UNSIGNED, PrivilegeId INTEGER UNSIGNED, NavigationId INTEGER UNSIGNED)
BEGIN

  START TRANSACTION;

    
    UPDATE sm_subnav SET csr_mainnav_id = MainNavId, csr_subnav_title = NavigationTitle , user_control_id = UserControlId , privilege_id = PrivilegeId WHERE csr_subnav_id = NavigationId;

    
    SELECT row_count();

  COMMIT;

END
;;
DELIMITER ;