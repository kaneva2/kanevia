-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_role;

DELIMITER ;;
CREATE PROCEDURE `delete_role`(siteRoleId INT UNSIGNED, companyId INT UNSIGNED)
BEGIN
  
  START TRANSACTION;
  
    /* attempt to delete all privileges associted with this role */
    CALL delete_all_role_privileges(siteRoleId, companyId, 0);
      
    /* delete the records */
    DELETE FROM site_roles WHERE site_role_id = siteRoleId AND company_id = companyId;

    /*return the number of records affected*/
    SELECT row_count();

  COMMIT;
END
;;
DELIMITER ;