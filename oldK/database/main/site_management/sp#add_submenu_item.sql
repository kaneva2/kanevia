-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_submenu_item;

DELIMITER ;;
CREATE PROCEDURE `add_submenu_item`(MainNavId INTEGER UNSIGNED, NavigationTitle VARCHAR(60), UserControlId INTEGER UNSIGNED, PrivilegeId INTEGER UNSIGNED)
BEGIN	

  START TRANSACTION;

    

    INSERT INTO sm_subnav (csr_mainnav_id , csr_subnav_title, user_control_id, privilege_id) VALUES (MainNavId, NavigationTitle, UserControlId, PrivilegeId);

    

    SELECT LAST_INSERT_ID();

  COMMIT;

END
;;
DELIMITER ;