-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_available_website;

DELIMITER ;;
CREATE PROCEDURE `update_available_website`(websiteName VARCHAR(45), websiteConnStrng VARCHAR(300), websiteURL VARCHAR(45), websiteId SMALLINT UNSIGNED)
BEGIN

  START TRANSACTION;

    
    UPDATE sm_available_websites SET website_name = websiteName, connection_string = websiteConnStrng, website_url = websiteURL WHERE website_id = websiteId;

    
    SELECT row_count();

  COMMIT;

END
;;
DELIMITER ;