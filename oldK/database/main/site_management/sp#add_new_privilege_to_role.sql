-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_new_privilege_to_role;

DELIMITER ;;
CREATE PROCEDURE `add_new_privilege_to_role`(siteRoleId INT UNSIGNED, companyId INT UNSIGNED, privilegeId INT UNSIGNED, accessLevelId SMALLINT UNSIGNED)
BEGIN
 
  START TRANSACTION;
  
    
    /* update the records */
    INSERT INTO roles_to_privileges (site_role_id, company_id, privilege_id, access_level_id) VALUES (siteRoleId, companyId, privilegeId, accessLevelId);

    /*return the auto id*/
    SELECT LAST_INSERT_ID();
    
    
  COMMIT;
END
;;
DELIMITER ;