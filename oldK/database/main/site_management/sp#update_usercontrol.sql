-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_usercontrol;

DELIMITER ;;
CREATE PROCEDURE `update_usercontrol`(UserControlName VARCHAR(200), ControlTypeId INTEGER UNSIGNED, UserControlId INTEGER UNSIGNED)
BEGIN

  START TRANSACTION;

    
    UPDATE user_controls SET user_control_name = UserControlName, control_type_id = ControlTypeId WHERE user_control_id = UserControlId;

    
    SELECT row_count();

  COMMIT;

END
;;
DELIMITER ;