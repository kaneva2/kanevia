-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `access_levels` (
  `access_level_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'assigne value for level',
  `access_level` varchar(45) NOT NULL DEFAULT '1' COMMENT 'description of access level',
  PRIMARY KEY (`access_level_id`)
) ENGINE=InnoDB COMMENT='2nd normal table for privilege access levels';

CREATE TABLE `roles_to_privileges` (
  `site_role_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'id of the security role across sites',
  `company_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'id of company owning role',
  `privilege_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'id of privilege',
  `access_level_id` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'id of access level',
  PRIMARY KEY (`company_id`,`privilege_id`,`site_role_id`),
  KEY `FK_accesslevel` (`access_level_id`),
  KEY `FK_siteRole` (`site_role_id`),
  CONSTRAINT `FK_accesslevel` FOREIGN KEY (`access_level_id`) REFERENCES `access_levels` (`access_level_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_siteRole` FOREIGN KEY (`site_role_id`) REFERENCES `site_roles` (`site_role_id`)
) ENGINE=InnoDB COMMENT='many to many relating roles to privleges';

CREATE TABLE `schema_versions` (
  `version` int(11) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT;

CREATE TABLE `site_privileges` (
  `privilege_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto number for privilege',
  `privilege_name` varchar(50) NOT NULL DEFAULT '' COMMENT 'name for the priviledge',
  `privilege_description` varchar(100) NOT NULL DEFAULT '' COMMENT 'description of the privilege',
  `site_administrators_only` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'default to only site admins to prevent accidental deply to site users',
  PRIMARY KEY (`privilege_id`)
) ENGINE=InnoDB COMMENT='stores available privileges for managed sites';

CREATE TABLE `site_roles` (
  `site_role_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto numbered id for role across sites',
  `role_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'role id that is contextual within a site (mostly for use with kaneva.com)',
  `role_description` varchar(45) NOT NULL DEFAULT '' COMMENT 'description of the role',
  `company_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`site_role_id`)
) ENGINE=InnoDB COMMENT='stores roles for all sites';

CREATE TABLE `sm_available_websites` (
  `website_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto id for available websites',
  `website_name` varchar(45) NOT NULL DEFAULT '' COMMENT 'name of website (for display)',
  `connection_string` varchar(300) DEFAULT NULL COMMENT 'connection string to websites database',
  `website_url` varchar(255) NOT NULL,
  PRIMARY KEY (`website_id`)
) ENGINE=InnoDB;

CREATE TABLE `sm_mainnav` (
  `csr_mainnav_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto id for the navigation item',
  `csr_mainnav_title` varchar(60) NOT NULL DEFAULT '' COMMENT 'display value for navigation item',
  `privilege_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'id of privilege needed to access the navigation item',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'id of the website to which the navigation item belongs',
  PRIMARY KEY (`csr_mainnav_id`),
  KEY `FK_csr_website_id` (`website_id`),
  KEY `FK_privilege_id_mainnav` (`privilege_id`),
  CONSTRAINT `FK_csr_website_id` FOREIGN KEY (`website_id`) REFERENCES `sm_available_websites` (`website_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_privilege_id_mainnav` FOREIGN KEY (`privilege_id`) REFERENCES `site_privileges` (`privilege_id`) ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='stores main navigation menu items for all websites';

CREATE TABLE `sm_subnav` (
  `csr_subnav_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto if for the sub navigation item',
  `csr_mainnav_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'fk id to the main name which calls this sub nav',
  `csr_subnav_title` varchar(60) NOT NULL DEFAULT '' COMMENT 'display value for the sub navigation item',
  `user_control_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'fk id to the user control table to indicate which user control to load',
  `privilege_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'fk id to indicate which privilege is needed to see this menu item',
  PRIMARY KEY (`csr_subnav_id`),
  KEY `FK_privilege_id_subnav` (`privilege_id`),
  KEY `FK_csr_usercontrol_id` (`user_control_id`),
  KEY `FK_csr_mainMenu_id` (`csr_mainnav_id`),
  CONSTRAINT `FK_csr_mainMenu_id` FOREIGN KEY (`csr_mainnav_id`) REFERENCES `sm_mainnav` (`csr_mainnav_id`),
  CONSTRAINT `FK_csr_usercontrol_id` FOREIGN KEY (`user_control_id`) REFERENCES `user_controls` (`user_control_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_privilege_id_subnav` FOREIGN KEY (`privilege_id`) REFERENCES `site_privileges` (`privilege_id`) ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='InnoDB free: 54272 kB; (`user_control_id`) REFER `customer_s';

CREATE TABLE `user_controls` (
  `user_control_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto id for the user control',
  `user_control_name` varchar(200) NOT NULL,
  `control_type_id` varchar(200) NOT NULL,
  PRIMARY KEY (`user_control_id`)
) ENGINE=InnoDB COMMENT='stores the paths to all user controls for the site managemen';

CREATE TABLE `user_control_type` (
  `control_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `control_type` varchar(45) NOT NULL,
  PRIMARY KEY (`control_type_id`)
) ENGINE=InnoDB;

