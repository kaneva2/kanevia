-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_all_role_privileges;

DELIMITER ;;
CREATE PROCEDURE `delete_all_role_privileges`(siteRoleId INT UNSIGNED, companyId INT UNSIGNED, returnRowsAffected TINYINT)
BEGIN

  START TRANSACTION;
    
    /* delete the records */
    DELETE FROM roles_to_privileges WHERE site_role_id = siteRoleId AND company_id = companyId;

    IF returnRowsAffected = 1 THEN
    
    	/*return the number of records affected*/
    	SELECT row_count();
    
    END IF;
  COMMIT;
END
;;
DELIMITER ;