-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_available_website;

DELIMITER ;;
CREATE PROCEDURE `delete_available_website`(websiteId SMALLINT UNSIGNED)
BEGIN

  START TRANSACTION;

    
    DELETE FROM sm_subnav WHERE csr_mainnav_id IN (SELECT csr_mainnav_id FROM sm_mainnav WHERE website_id = websiteId);

    
    DELETE FROM sm_mainnav WHERE website_id = websiteId;

    
    DELETE FROM sm_available_websites WHERE website_id = websiteId;

    
    SELECT row_count();

  COMMIT;

END
;;
DELIMITER ;