-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_submenu_item;

DELIMITER ;;
CREATE PROCEDURE `delete_submenu_item`(NavigationId INTEGER UNSIGNED)
BEGIN

  START TRANSACTION;

    
    DELETE FROM sm_subnav WHERE csr_subnav_id = NavigationId;

    
    SELECT row_count();

  COMMIT;

END
;;
DELIMITER ;