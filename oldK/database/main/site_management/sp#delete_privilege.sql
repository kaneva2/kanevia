-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_privilege;

DELIMITER ;;
CREATE PROCEDURE `delete_privilege`(privilegeId INT UNSIGNED)
BEGIN
 
  START TRANSACTION;
  
    
    /* delete the records */
    DELETE FROM site_privileges WHERE privilege_id = privilegeId;

    /*return the number of records affected*/
    SELECT row_count();

  COMMIT;
END
;;
DELIMITER ;