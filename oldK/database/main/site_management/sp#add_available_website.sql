-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_available_website;

DELIMITER ;;
CREATE PROCEDURE `add_available_website`(websiteName VARCHAR(45), websiteConnStrng VARCHAR(300), websiteURL VARCHAR(45))
BEGIN	

  START TRANSACTION;

    
    INSERT INTO sm_available_websites (website_name, connection_string, website_url) VALUES (websiteName, websiteConnStrng, websiteURL);

    
    SELECT LAST_INSERT_ID();

  COMMIT;

END
;;
DELIMITER ;