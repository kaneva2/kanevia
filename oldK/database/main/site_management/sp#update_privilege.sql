-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_privilege;

DELIMITER ;;
CREATE PROCEDURE `update_privilege`(privilegeName VARCHAR(50), privilegeDescription VARCHAR(100), adminOnly TINYINT UNSIGNED, privilegeId INT UNSIGNED)
BEGIN
 
  START TRANSACTION;
  
    
    /* update the records */
    UPDATE site_privileges SET privilege_name = privilegeName, privilege_description = privilegeDescription, site_administrators_only = adminOnly WHERE privilege_id = privilegeId;

    /*return the number of records affected*/
    SELECT row_count();

  COMMIT;
END
;;
DELIMITER ;