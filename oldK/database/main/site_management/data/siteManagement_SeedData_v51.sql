-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- create the developer for the company
INSERT INTO site_management.user_control_type (control_type_id, control_type) VALUES (1, "Tool");
INSERT INTO site_management.user_control_type (control_type_id, control_type) VALUES (2, "Report");

 