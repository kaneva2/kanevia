-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS execute_prune_table;

DELIMITER ;;
CREATE PROCEDURE `execute_prune_table`(__archive_set_name VARCHAR(64), __source_schema VARCHAR(64), __destination_schema VARCHAR(64), __table_name VARCHAR(64), __control_column_name VARCHAR(64), __transactional_retention_period INT)
BEGIN
	DECLARE __min_date DATETIME;
	DECLARE __max_date DATETIME;
	DECLARE __prune_count LONG;
	DECLARE __table_exists INT;
	
	DECLARE __prune_1000s LONG;  -- prune_count divided by 1000 to partition data for deletion
	DECLARE __mins_per_segment INT; -- number of minutes worth of data per delete segment

	DECLARE __time_interval INT;  -- used to control which segment of rows to delete in a controlled manner

	DECLARE __retention_date DATETIME;
	DECLARE __daystoadjust INT;
	DECLARE __true_retention_period INT;

	
	-- find the beginning date to start pruning from
	SET @sql := CONCAT('SELECT MIN(',__control_column_name,') INTO @min_date FROM ', __source_schema, '.', __table_name,';') ;
	CALL logging.logMessage('execute_prune_table','DEBUG',@sql);
	CALL logging.flushLog();
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	IF (@min_date IS NULL) THEN
		CALL logging.logMessage('execute_prune_table','DEBUG','No rows found in table.');
		CALL logging.flushLog();
		INSERT INTO archiving.archive_prune_log 
			(archive_set_name, table_name, when_ts, from_ts, to_ts, number_of_rows_pruned, retention_period) 
			VALUES (__archive_set_name, __table_name, NOW(), NULL, NULL, 0, __transactional_retention_period);		
	ELSE
		SET __min_date = @min_date;
		CALL logging.logMessage('execute_prune_table','DEBUG',CONCAT('Min Date = ',__min_date));
		CALL logging.flushLog();

		-- calculate the date up to which we should prune.  leave enough rows that we have
		-- a full month of data for the date in which the retention period falls
		--
		SET __true_retention_period := __transactional_retention_period + 1;
		CALL logging.logMessage('execute_prune_table','DEBUG',CONCAT('true retention_period = ',__true_retention_period));
		CALL logging.flushLog();
		SET __retention_date := DATE(DATE_SUB(DATE(NOW()), INTERVAL __true_retention_period MONTH));
		CALL logging.logMessage('execute_prune_table','DEBUG',CONCAT('retention_date = ',__retention_date));
		CALL logging.flushLog();
		SET __max_date := DATE_SUB(DATE(__retention_date), INTERVAL 1 SECOND);
		CALL logging.logMessage('execute_prune_table','DEBUG',CONCAT('Max Date = ',__max_date));
		CALL logging.flushLog();

		-- count the number of rows from the source table to be archived
		SET @sql := CONCAT('SELECT COUNT(*) INTO @prune_count FROM ', __source_schema, '.', __table_name,' WHERE ',__control_column_name,' <= ''',__max_date,''';');
		CALL logging.logMessage('execute_prune_table','DEBUG',@sql);
		CALL logging.flushLog();
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
		SET __prune_count = @prune_count;
		CALL logging.logMessage('execute_prune_table','DEBUG',CONCAT('Rows to be pruned = ',__prune_count));
		CALL logging.flushLog();

		-- perform deletes in small batches in order to be replication-friendly
		REPEAT
			SET foreign_key_checks = 0;
			SET @sql := CONCAT('DELETE FROM ', __source_schema,'.',__table_name,' WHERE ',__control_column_name,' <= ''',__max_date,''' ORDER BY ',__control_column_name,' LIMIT 1000;');
			CALL logging.logMessage('execute_prune_table','DEBUG',@sql);
			CALL logging.flushLog();
			START TRANSACTION;
			PREPARE stmt FROM @sql;
			EXECUTE stmt;
			SELECT ROW_COUNT() INTO @num_deleted;
			DEALLOCATE PREPARE stmt;
			COMMIT;
			SET foreign_key_checks = 1;
			SELECT SLEEP(1);
		UNTIL @num_deleted = 0 END REPEAT;

		-- log the pruning for potential reporting purposes
		INSERT INTO archiving.archive_prune_log 
			(archive_set_name, table_name, when_ts, from_ts, to_ts, number_of_rows_pruned, retention_period) 
			VALUES (__archive_set_name, __table_name, NOW(), __min_date, __max_date, __prune_count, __transactional_retention_period);

	END IF;
	CALL logging.flushLog();
END
;;
DELIMITER ;