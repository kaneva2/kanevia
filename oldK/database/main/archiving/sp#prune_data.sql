-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS prune_data;

DELIMITER ;;
CREATE PROCEDURE `prune_data`()
begin
	DECLARE __done INT DEFAULT 0;
	DECLARE __archive_set_name VARCHAR(100);
	DECLARE __source_schema VARCHAR(64);
	DECLARE __destination_schema VARCHAR(64);
	DECLARE __transactional_retention_period INT;

	DECLARE archive_set_cursor CURSOR FOR
		SELECT archive_set_name, source_schema, destination_schema, transactional_retention_period
		FROM archiving.archive_sets
		WHERE prune_start_date >= DATE_ADD(archive_start_date, INTERVAL 1 MONTH)
			AND prune_start_date < NOW();

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1;
	
	CALL logging.logMessage('prune_data','DEBUG','Opening prune cursor.');
	CALL logging.flushLog;
	OPEN archive_set_cursor;
	REPEAT
		FETCH archive_set_cursor INTO __archive_set_name, __source_schema, __destination_schema, __transactional_retention_period;
		IF NOT __done THEN
			BEGIN
				CALL logging.logMessage('prune_data','DEBUG',CONCAT('Calling execute_prune_set with: ',__archive_set_name));
				CALL logging.flushLog;
				CALL execute_prune_set(__archive_set_name, __source_schema, __destination_schema, __transactional_retention_period);
			END;
		END IF;
	UNTIL __done END REPEAT;
	CALL logging.logMessage('prune_data','DEBUG','Closing prune cursor.');
	CALL logging.flushLog;
	CLOSE archive_set_cursor;
	SELECT 'ready' INTO OUTFILE '/tmp/pruning_tasks_complete.go';
end
;;
DELIMITER ;