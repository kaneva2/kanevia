#!/bin/bash

REPORTFILE=/tmp/archiving_tasks.report
GOFILE=/tmp/archiving_tasks_complete.go

print_report() {
	echo "${1}" >> ${REPORTFILE}
	echo -e "\n" >> ${REPORTFILE}
	cat ${2} >> ${REPORTFILE}
	echo -e "\n\n" >> ${REPORTFILE}
}

if [ -e ${GOFILE} ]; then
	echo -e "Nightly Archiving Report\n\n" > ${REPORTFILE}

	mysql -t archiving -e "select * from archive_log where date(when_ts) = date(now()) order by when_ts;" >> ${REPORTFILE}

	cat ${REPORTFILE} | mail -s "Nightly Archiving Report" dba@kaneva.com
	rm -f ${REPORTFILE} ${GOFILE}
fi

