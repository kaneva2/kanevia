-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- ONLY CREATE THIS EVENT ON THE SLAVE SERVER!!
--
USE archiving;

DROP EVENT IF EXISTS archive_event;

CREATE EVENT IF NOT EXISTS archive_event 
ON SCHEDULE 
EVERY 1 DAY
STARTS DATE_ADD( DATE(NOW()), INTERVAL 1 HOUR)
COMMENT 'Perform regular archiving of large tables.'
DO CALL archive_data;

ALTER EVENT archive_event ENABLE;
