-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS archive_data;

DELIMITER ;;
CREATE PROCEDURE `archive_data`()
BEGIN
	DECLARE __done INT DEFAULT 0;
	DECLARE __archive_set_name VARCHAR(100);
	DECLARE __source_schema VARCHAR(64);
	DECLARE __destination_schema VARCHAR(64);

	DECLARE archive_set_cursor CURSOR FOR
		SELECT archive_set_name, source_schema, destination_schema
		FROM archiving.archive_sets
		WHERE (prune_start_date >= DATE_ADD(archive_start_date, INTERVAL 1 MONTH) OR
				prune_start_date is NULL)
			AND archive_start_date <= NOW();

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1;
	
	CALL logging.logMessage('archiving','DEBUG','Opening archive_set_cursor.');
	CALL logging.flushLog;
	OPEN archive_set_cursor;
	REPEAT
		FETCH archive_set_cursor INTO __archive_set_name, __source_schema, __destination_schema;
		IF NOT __done THEN
			BEGIN
				CALL logging.logMessage('archiving','DEBUG',CONCAT('Calling execute_archive_set with: ',__archive_set_name));
				CALL logging.flushLog;
				CALL execute_archive_set(__archive_set_name, __source_schema, __destination_schema);
			END;
		END IF;
	UNTIL __done END REPEAT;
	CALL logging.logMessage('archiving','DEBUG','Closing archive_set_cursor.');
	CALL logging.flushLog;
	CLOSE archive_set_cursor;
	SELECT 'ready' INTO OUTFILE '/tmp/archiving_tasks_complete.go';
END
;;
DELIMITER ;