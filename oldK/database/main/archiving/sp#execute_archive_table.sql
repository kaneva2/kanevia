-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS execute_archive_table;

DELIMITER ;;
CREATE PROCEDURE `execute_archive_table`(__archive_set_name VARCHAR(64), __source_schema VARCHAR(64), __destination_schema VARCHAR(64), __table_name VARCHAR(64), __control_column_name VARCHAR(64))
BEGIN
	DECLARE __min_date DATETIME;
	DECLARE __max_date DATETIME;
	DECLARE __archive_count LONG;
	DECLARE __table_exists INT;

	START TRANSACTION;
	
	
	SET @sql := CONCAT('SELECT COUNT(*) INTO @table_exists FROM information_schema.tables WHERE TABLE_SCHEMA = ''',__destination_schema,''' AND TABLE_NAME = ''',__table_name,''';');
	CALL logging.logMessage('archiving','DEBUG',@sql);
	CALL logging.flushLog;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	CALL logging.logMessage('archiving','DEBUG',CONCAT('Table Exists = ',@table_exists));
	CALL logging.flushLog;
	
	IF @table_exists = 0 THEN
		SET @sql = CONCAT('CREATE TABLE ',__destination_schema,'.',__table_name,' LIKE ',__source_schema,'.',__table_name,';');
		CALL logging.logMessage('archiving','DEBUG',@sql);
		CALL logging.flushLog;
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
	END IF;

	
	SET @sql := CONCAT('SELECT MAX(',__control_column_name,') INTO @min_date FROM ', __destination_schema, '.', __table_name,';') ;
	CALL logging.logMessage('archiving','DEBUG',@sql);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	SET __min_date = @min_date;
	
	IF __min_date IS NULL THEN
		SET __min_date = '2000-01-01 00:00:00';
	END IF;
	SET __min_date := DATE_ADD(__min_date, INTERVAL 1 SECOND);
	CALL logging.logMessage('archiving','DEBUG',CONCAT('Min Date = ',__min_date));

	
	
	SET __max_date := DATE_SUB(DATE(NOW()), INTERVAL 1 SECOND);
	CALL logging.logMessage('archiving','DEBUG',CONCAT('Max Date = ',__max_date));

	
	SET @sql := CONCAT('SELECT COUNT(*) INTO @archive_count FROM ', __source_schema, '.', __table_name,' WHERE ',__control_column_name,' BETWEEN ''', __min_date,''' AND ''',__max_date,''';');
	CALL logging.logMessage('archiving','DEBUG',@sql);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	SET __archive_count = @archive_count;
	CALL logging.logMessage('archiving','DEBUG',CONCAT('Rows to be archived = ',__archive_count));

	
	SET @sql := CONCAT('INSERT INTO ', __destination_schema, '.', __table_name,' SELECT * FROM ', __source_schema, '.', __table_name,' WHERE ',__control_column_name,' BETWEEN ''', __min_date,''' AND ''',__max_date,''';');
	CALL logging.logMessage('archiving','DEBUG',@sql);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

	INSERT INTO archiving.archive_log 
		(archive_set_name, table_name, when_ts, from_ts, to_ts, number_of_rows_archived) 
		VALUES (__archive_set_name, __table_name, NOW(), __min_date, __max_date, __archive_count);

	COMMIT;
	CALL logging.flushLog();
END
;;
DELIMITER ;