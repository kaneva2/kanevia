-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- Note that this event is disabled on the slave machine.
-- Using SHOW EVENTS on the slave side should show a status of SLAVESIDE_DISABLED
--
USE archiving;

DROP EVENT IF EXISTS prune_event;

-- Since pruning always leaves a full month of data, pruning data on a daily basis from the month prior to the one that is desired.
-- The event is set up to start at 7am each day.
-- By default, the event is disabled on slaves.
--
CREATE EVENT IF NOT EXISTS prune_event 
	ON SCHEDULE 
	EVERY 1 DAY
	STARTS DATE_ADD( DATE(NOW()), INTERVAL 7 HOUR)
	ENABLE
	COMMENT 'Perform regular pruning.'
	DO CALL prune_data;
