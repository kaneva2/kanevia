#!/bin/bash

REPORTFILE=/tmp/pruning_tasks.report
GOFILE=/tmp/pruning_tasks_complete.go

if [ -e ${GOFILE} ]; then
	echo -e "Nightly Pruning Report\n\n" > ${REPORTFILE}

	mysql -t archiving -e "select * from archive_prune_log where date(when_ts) = date(now()) order by when_ts;" >> ${REPORTFILE}

	cat ${REPORTFILE} | mail -s "Nightly Pruning Report" dba@kaneva.com
	rm -f ${REPORTFILE} ${GOFILE}
fi

