-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `archive_log` (
  `archive_set_name` varchar(100) NOT NULL COMMENT 'Archive set that this archiving belongs to.',
  `table_name` varchar(64) NOT NULL COMMENT 'Table name that was archived.',
  `when_ts` datetime NOT NULL COMMENT 'When the archiving was done.',
  `from_ts` datetime NOT NULL COMMENT 'Beginning timestamp of the archiving.',
  `to_ts` datetime NOT NULL COMMENT 'Ending timestamp of the archiving',
  `number_of_rows_archived` int(11) NOT NULL COMMENT 'Number of rows archived to the archive table',
  PRIMARY KEY (`archive_set_name`,`table_name`,`when_ts`),
  CONSTRAINT `archive_log_tables_fk` FOREIGN KEY (`archive_set_name`, `table_name`) REFERENCES `archive_set_tables` (`archive_set_name`, `table_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `archive_prune_log` (
  `archive_set_name` varchar(100) NOT NULL COMMENT 'Archive set that this pruning belongs to.',
  `table_name` varchar(64) NOT NULL COMMENT 'Table name that was pruned.',
  `when_ts` datetime NOT NULL COMMENT 'When the pruning was done.',
  `from_ts` datetime DEFAULT NULL,
  `to_ts` datetime DEFAULT NULL,
  `number_of_rows_pruned` int(11) NOT NULL COMMENT 'Number of rows pruned from the table',
  `retention_period` int(11) NOT NULL COMMENT 'Transactional retention period in effect when the prune took place',
  PRIMARY KEY (`archive_set_name`,`table_name`,`when_ts`),
  CONSTRAINT `archive_prune_log_tables_fk` FOREIGN KEY (`archive_set_name`, `table_name`) REFERENCES `archive_set_tables` (`archive_set_name`, `table_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `archive_sets` (
  `archive_set_name` varchar(100) NOT NULL COMMENT 'Unique name of the archive set.',
  `source_schema` varchar(64) NOT NULL COMMENT 'Schema in which the archive data resides initially.',
  `destination_schema` varchar(64) NOT NULL COMMENT 'Schema into which the archive data will be placed.',
  `transactional_retention_period` int(11) NOT NULL COMMENT 'Number of months worth of transactional data to maintain in the source table.',
  `archive_start_date` date NOT NULL COMMENT 'Date that archiving is to begin.',
  `prune_start_date` date DEFAULT NULL COMMENT 'Date pruning is to begin.  Must be at least 1 month after archiving start.  If null, then tables in this set will not be pruned.',
  PRIMARY KEY (`archive_set_name`)
) ENGINE=InnoDB;

CREATE TABLE `archive_set_tables` (
  `archive_set_name` varchar(100) NOT NULL COMMENT 'References the archive_set_name in the archive_set table.',
  `table_name` varchar(64) NOT NULL COMMENT 'Name of the table to be archived.  The same name is used for the archived data in the destination schema.',
  `order_of_execution` int(11) NOT NULL COMMENT 'Order in which this table should be processed in the archive set.',
  `control_column_name` varchar(64) NOT NULL COMMENT 'Name of the date or timestamp column that should be used for archiving purposes.',
  PRIMARY KEY (`archive_set_name`,`table_name`),
  CONSTRAINT `archive_set_tables_sets_fk` FOREIGN KEY (`archive_set_name`) REFERENCES `archive_sets` (`archive_set_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  `interface_change` enum('T','F') NOT NULL DEFAULT 'F',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

