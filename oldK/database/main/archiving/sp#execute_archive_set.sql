-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS execute_archive_set;

DELIMITER ;;
CREATE PROCEDURE `execute_archive_set`(__archive_set_name VARCHAR(64), __source_schema VARCHAR(64), __destination_schema VARCHAR(64))
BEGIN
	DECLARE __done INT DEFAULT 0;
	DECLARE __table_name VARCHAR(64);
	DECLARE __control_column_name VARCHAR(64);

	DECLARE archive_tables_cursor CURSOR FOR
		SELECT table_name, control_column_name
		FROM archiving.archive_set_tables
		WHERE archive_set_name = __archive_set_name
		ORDER BY order_of_execution;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1;
	
	CALL logging.logMessage('archiving','DEBUG','opening cursor');
	CALL logging.flushLog();
	OPEN archive_tables_cursor;
	REPEAT
		FETCH archive_tables_cursor INTO __table_name, __control_column_name;
		IF NOT __done THEN
			BEGIN
				CALL logging.logMessage('archiving','DEBUG',CONCAT('Calling execute_archive_table with: ',__table_name));
				CALL logging.flushLog();
				CALL execute_archive_table(__archive_set_name, __source_schema, __destination_schema, __table_name, __control_column_name);
			END;
		END IF;
	UNTIL __done END REPEAT;
	CALL logging.logMessage('archiving','DEBUG','Closing archive_tables_cursor.');
	CALL logging.flushLog();
	CLOSE archive_tables_cursor;
END
;;
DELIMITER ;