-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TABLE IF EXISTS shard_info.`dynamic_objects`;
DROP TABLE IF EXISTS shard_info.`world_object_player_settings`;
DROP TABLE IF EXISTS shard_info.`schema_versions`;
