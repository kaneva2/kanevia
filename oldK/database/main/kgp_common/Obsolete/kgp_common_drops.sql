-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TABLE IF EXISTS kgp_common.`blocked_users`;
DROP TABLE IF EXISTS kgp_common.`friend_group_friends`;
DROP TABLE IF EXISTS kgp_common.`friend_groups`;
DROP TABLE IF EXISTS kgp_common.`friends`;
DROP TABLE IF EXISTS kgp_common.`transaction_log`;
DROP TABLE IF EXISTS kgp_common.`transaction_log_details`;
DROP TABLE IF EXISTS kgp_common.`user_balances`;
DROP TABLE IF EXISTS kgp_common.`users`;
DROP TABLE IF EXISTS kgp_common.`schema_versions`;

