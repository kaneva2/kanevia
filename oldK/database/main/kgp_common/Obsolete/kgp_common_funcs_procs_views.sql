-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- common functions, procs, views, triggers
use shard_info;
\. fn#get_dynamic_objects_id.sql
\. fn#get_world_object_player_settings_id.sql

