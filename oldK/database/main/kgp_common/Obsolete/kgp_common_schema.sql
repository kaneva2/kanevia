-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

--
-- Current Database: `kgp_common`
--

CREATE DATABASE IF NOT EXISTS `kgp_common` DEFAULT CHARACTER SET latin1;

USE `kgp_common`;

--
-- Table structure for table `blocked_users`
--

CREATE TABLE `blocked_users` (
  `user_id` int(11) NOT NULL default '0',
  `profile_id` int(11) NOT NULL default '0',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `zone_type` smallint(6) NOT NULL default '0' COMMENT 'Apartment or broadband',
  `zone_instance_id` int(11) NOT NULL default '0' COMMENT 'id of the zone being blocked if block_type is player or zone travel',
  `reason_description` varchar(80) NOT NULL default '0',
  `is_comm_blocked` tinyint(3) unsigned NOT NULL default '0' COMMENT 'Communication blocked',
  `is_follow_blocked` tinyint(3) unsigned NOT NULL default '0' COMMENT 'Apartment or follow person',
  `is_zone_blocked` tinyint(3) unsigned NOT NULL default '0' COMMENT 'Broadband zone blocked',
  `block_id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`block_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `friend_group_friends`
--

CREATE TABLE `friend_group_friends` (
  `friend_group_id` int(11) NOT NULL default '0',
  `friend_id` int(11) NOT NULL default '0' COMMENT 'alias to users.user_id',
  PRIMARY KEY  (`friend_group_id`,`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `friend_groups`
--

CREATE TABLE `friend_groups` (
  `friend_group_id` int(11) NOT NULL auto_increment,
  `owner_id` int(11) NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `friend_count` int(10) unsigned NOT NULL default '0',
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`friend_group_id`),
  KEY `oid` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `friends`
--

CREATE TABLE `friends` (
  `user_id` int(11) NOT NULL default '0',
  `friend_id` int(11) NOT NULL default '0' COMMENT 'alias to users.user_id',
  `glued_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`user_id`,`friend_id`),
  KEY `u_glue_stat` USING BTREE (`user_id`,`glued_date`),
  KEY `fid` (`friend_id`),
  KEY `uid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='link TP user to user';

--
-- Table structure for table `transaction_log`
--

CREATE TABLE `transaction_log` (
  `trans_id` bigint(20) NOT NULL auto_increment,
  `created_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL default '0',
  `balance_prev` float NOT NULL default '0',
  `balance` float NOT NULL default '0',
  `transaction_amount` double default '0',
  `kei_point_id` ENUM('GPOINT', 'KPOINT', 'MPOINT', 'DOLLAR', 'BANK_GPOINTS', 'BANK_KPOINTS') NOT NULL DEFAULT 'GPOINT' COMMENT 'type of currency',
  `transaction_type` smallint(5) unsigned NOT NULL default '1',
  PRIMARY KEY  (`trans_id`),
  KEY `user_id` (`user_id`),
  KEY `tamt` (`transaction_amount`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `transaction_log_details`
--

CREATE TABLE `transaction_log_details` (
  `trans_id` int(11) NOT NULL COMMENT 'FK to transaction table',
  `global_id` int(11) NOT NULL COMMENT 'FK to wok.items table',
  `quantity` int(11) NOT NULL COMMENT 'number of items',
  `use_value` int(11) NOT NULL default '0' COMMENT 'copied from wok.items',
  `name` varchar(255) NOT NULL default '' COMMENT 'copied from wok.items'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='details table for user_balance changes';

--
-- Table structure for table `user_balances`
--

CREATE TABLE `user_balances` (
  `user_id` int(11) NOT NULL default '0' COMMENT 'FK to users',
  `kei_point_id` ENUM('GPOINT', 'KPOINT', 'MPOINT', 'DOLLAR', 'BANK_GPOINTS', 'BANK_KPOINTS') NOT NULL DEFAULT 'GPOINT' COMMENT 'type of currency',
  `balance` decimal(12,3) NOT NULL default '0.000',
  PRIMARY KEY  (`user_id`,`kei_point_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='TP currency balance';

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL auto_increment COMMENT 'TP-specific unqiue id',
  `username` varchar(22) NOT NULL COMMENT 'no spaces allowed',
  `first_name` varchar(50) NOT NULL default '',
  `last_name` varchar(50) NOT NULL default '',
  `description` text,
  `gender` char(1) NOT NULL COMMENT 'M/F else unknown',
  `show_mature` binary(1) NOT NULL default '0' COMMENT '1 if should show mature content (and of age)',
  `birth_date` date NOT NULL default '2007-01-01' COMMENT 'when born, age, over_21 calculated from this',
  `age` tinyint(4) NOT NULL default '0' COMMENT 'calculated age',
  `over_21` enum('Y','N') NOT NULL default 'N' COMMENT 'calculated from age for performance',
  `active` enum('Y','N') NOT NULL default 'Y' COMMENT 'Y if allowed to access system',
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `status_id` int(11) NOT NULL default '1' COMMENT '1 = active',
  `mature_profile` binary(1) NOT NULL DEFAULT '0',
  PRIMARY KEY  (`user_id`),
  KEY `index_gender` (`gender`),
  KEY `username` (`username`),
  KEY `first_last` (`first_name`,`last_name`),
  KEY `country_status_id_user_id` (`user_id`),
  KEY `age` (`age`),
  KEY `active` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tracks TP-specific user data';

--
-- Table structure for table `schema_versions`
--

CREATE TABLE `schema_versions` (
  `version` int(11) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL default CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY  (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- set the initial schema version
INSERT INTO schema_versions( `version`, `description` ) VALUES (44, 'base version' );

