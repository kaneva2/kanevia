-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `preload_scripts` (
  `obj_placement_id` int(11) NOT NULL,
  `script` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`obj_placement_id`)
) ENGINE=InnoDB;

CREATE TABLE `preload_scripts_backup` (
  `obj_placement_id` int(11) NOT NULL,
  `script` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`obj_placement_id`)
) ENGINE=InnoDB;

CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  `interface_change` enum('T','F') DEFAULT 'F' COMMENT 'does this update require matching binaries',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

