-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS test_delete_game;

DELIMITER ;;
CREATE PROCEDURE `test_delete_game`( IN __game INT(11) , IN __modifier INT(11) )
BEGIN
Declare _now DateTime;
  
	set _now = NOW();
  
	SELECT __modifier as modifierId, __game as gameId, _now;
END
;;
DELIMITER ;