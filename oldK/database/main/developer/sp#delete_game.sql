-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_game;

DELIMITER ;;
CREATE PROCEDURE `delete_game`( IN __game INT(11) , IN __modifier INT(11) )
BEGIN
 
Declare _now DateTime;
 
 -- delete non audited data
 DELETE FROM game_views
   WHERE game_id = __game;
 
 DELETE FROM game_stats
   WHERE game_id = __game;
 
 DELETE FROM game_server_history
   WHERE server_id IN (SELECT server_id FROM game_servers WHERE game_id = __game );
 
 DELETE FROM game_roles
   WHERE game_id = __game;
 
 DELETE FROM game_raves
   WHERE game_id = __game;
 
 DELETE FROM game_patch_urls
   WHERE game_id = __game;
 
 -- get current date time
 set _now = NOW();
 
  -- delete audited data
 DELETE FROM game_servers
    WHERE game_id = __game;
 UPDATE audit_game_servers SET modifiers_id = __modifier WHERE game_id = __game AND date_modified >= _now AND operation = 'delete';
 
 DELETE FROM game_licenses 
   WHERE game_id = __game;
 UPDATE audit_game_licenses SET modifiers_id = __modifier WHERE game_id = __game AND date_modified >= _now AND operation = 'delete';
 
 INSERT into developer.audit_games
		(	game_id, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis, game_creation_date, game_access_id,
    	game_image_path, game_keywords, last_updated, modifiers_id, date_modified, operation )
    SELECT
     	__game, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis,
    	game_creation_date, game_access_id, game_image_path, game_keywords, last_updated,
      __modifier, Now(), 'delete'  
      FROM games WHERE game_id = __game;
 DELETE FROM games
   WHERE game_id = __game;
   
 END
;;
DELIMITER ;