-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS check_game_auth;

DELIMITER ;;
CREATE FUNCTION `check_game_auth`( __user_id INT(11), 
									__game_id INT(11), 
									__parent_game_id INT,
									__admin_only enum('T','F'),
									__members_only enum('T', 'F')) RETURNS tinyint(2)
    READS SQL DATA
BEGIN
-- will return the following possible values that map to the calling SP
-- SUCCESS 1
-- NO_GAME_ACCESS 4 (old rc, not member of community, used if no servers) 
-- GAME_FULL 9
-- NOT_AUTHORIZED 10 (mature, etc.)
-- ADMIN_ONLY = 12         (the server is admin_only and you're not an admin)
-- PROMPT_ALLOW = 13 (the user hasn't "allowed" this game access yet, prompt them)
-- 
	DECLARE __game_pop INT(11) DEFAULT NULL;
	DECLARE __game_pop_limit INT(11) DEFAULT 2000;
	DECLARE __privilege_id INT DEFAULT NULL;
	DECLARE __missing_passes INT DEFAULT 0;
	DECLARE __has_allowed INT DEFAULT NULL;
	
	
	SELECT sum(g.number_of_players) INTO __game_pop
	  FROM developer.game_servers g
	 WHERE game_id = __game_id 
	   AND server_status_id = 1;  
	   
	IF __game_pop IS NOT NULL THEN 
	
		SELECT max_game_users INTO __game_pop_limit
			FROM developer.game_licenses 
		WHERE game_id = __game_id;
			
		IF __game_pop > __game_pop_limit OR __admin_only = 'T' OR __members_only = 'T' THEN
		
			
			SET __privilege_id = wok.get_game_priv( __game_id, __user_id );

			
			IF __privilege_id NOT IN (1,2) THEN 
				IF __admin_only = 'T' THEN 
					return 12; 
				ELSEIF __members_only = 'T' AND __privilege_id <> 3 THEN
					return 16;
				ELSE
					return 9; 
				END IF;
			END IF;
		END IF;

		
		
		
		
		SELECT COUNT(*) INTO __missing_passes
		  FROM developer.game_pass_groups gpu 
	      LEFT OUTER JOIN wok.pass_group_users pgu ON gpu.pass_group_id = pgu.pass_group_id 
	           AND pgu.kaneva_user_id = __user_id
	     WHERE game_id = __game_id AND pgu.pass_group_id IS NULL           
	     ORDER BY 1
	     LIMIT 1;
	     
		IF __missing_passes > 0 THEN 
			return 10; 
		END IF;

		
		IF __parent_game_id = 0 THEN
			SET __has_allowed = __game_id; 
		ELSE			
			
			SELECT game_id INTO __has_allowed 
			  FROM player_game_allowed
			 WHERE kaneva_user_id = __user_id 
			   AND game_id = __game_id;
		END IF;			   

		IF __has_allowed IS NULL THEN
			CALL add_player_game_allowed(__user_id, __game_id);
		END IF;
    
			INSERT INTO developer.game_login_log (user_id, game_id, logon_time)
			 VALUES ( __user_id, __game_id, NOW() );
			return 1; 
					
	ELSE 
		return 4; 
	END IF;
END
;;
DELIMITER ;