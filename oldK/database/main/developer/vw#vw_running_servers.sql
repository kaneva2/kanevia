-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS vw_running_servers;

DELIMITER ;;

 CREATE VIEW `developer`.`vw_running_servers` AS SELECT `g`.`game_id` AS `game_id`,`gg`.`game_name` AS `game_name`,`g`.`server_id` AS `server_id`,`g`.`server_name` AS `server_name`,`gv`.`name` AS `visibility`,`g`.`ip_address` AS `ip_address`,`g`.`port` AS `port`,`g`.`number_of_players` AS `max_players`,`g`.`server_started_date` AS `server_started_date`,`g`.`last_ping_datetime` AS `last_ping_datetime` 
 FROM ( ( `developer`.`game_servers` `g` 
 JOIN `developer`.`game_server_visibility` `gv` on( ( `g`.`visibility_id` = `gv`.`visibility_id`) ) )  
 JOIN `developer`.`games` `gg` on( ( `gg`.`game_id` = `g`.`game_id`) ) )  
 WHERE ( `g`.`server_status_id` = 1) 
;;
DELIMITER ;