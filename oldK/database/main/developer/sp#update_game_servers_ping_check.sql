-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_game_servers_ping_check;

DELIMITER ;;
CREATE PROCEDURE `update_game_servers_ping_check`(__server_id INT(11),
	__actual_ip_address VARCHAR(50),
	__number_of_players INT(11),
	__unused INT(11),
	__server_status_id INT(11),
	__server_engine_admin_only ENUM('T','F')
	)
BEGIN
	-- this is called by the v5+ pingers which added __actual_ip_address and __ret
	DECLARE __ret INT DEFAULT 0; -- ok
	DECLARE __game_id INT DEFAULT 0;
	DECLARE __parent_game_id INT DEFAULT 0;
	DECLARE __protocol_version INT DEFAULT 0;
	DECLARE __asset_version INT DEFAULT 0;
	DECLARE __parent_protocol_version INT DEFAULT 0;
	DECLARE __parent_asset_version INT DEFAULT 0;
	DECLARE __game_access_id INT DEFAULT 0;
	DECLARE __owner_id INT DEFAULT 0;
	DECLARE __game_rating_id SMALLINT DEFAULT 0;
	DECLARE __game_status_id SMALLINT DEFAULT 0;
	DECLARE __game_name VARCHAR(100) DEFAULT '';
	DECLARE __game_description TEXT DEFAULT NULL; 
	DECLARE __game_synopsis VARCHAR(50) DEFAULT '';
	DECLARE __game_creation_date DATETIME DEFAULT '1990-01-01 00:00:00';
	DECLARE __game_image_path VARCHAR(255) DEFAULT '';
	DECLARE __game_keywords TEXT DEFAULT NULL;
	DECLARE __game_encryption_key VARCHAR(16) DEFAULT '';
	DECLARE __last_updated DATETIME DEFAULT '1990-01-01 00:00:00';
	DECLARE __modifiers_id INT DEFAULT 0;
	DECLARE LEGACY_PROTOCOL_VERSION INT DEFAULT 0x8026;
	DECLARE MIN_PROTOCOL_NEGO_VERSION INT DEFAULT 0x8027;
	
	UPDATE game_servers
		SET number_of_players = __number_of_players, 
        server_status_id = __server_status_id,
        last_ping_datetime = NOW(),
        server_engine_admin_only = __server_engine_admin_only,
				server_started_date = CASE WHEN __server_status_id = 3 THEN NOW() ELSE server_started_date END
    WHERE server_id = __server_id
      AND actual_ip_address = __actual_ip_address;

	IF ROW_COUNT() = 0 THEN
  
		-- this should rarely happen so ok to hit table again, figure out why it failed
		IF EXISTS (SELECT 1 FROM game_servers WHERE server_id = __server_id) THEN
			SET __ret = 1; -- actual ip mismatch
		ELSE
			SET __ret = 2; -- server_id not found
		END IF;
  
	ELSE
  
		-- check versions since ping can restart and a server can ping in without an initialize
		SELECT g.game_id, g.parent_game_id, g.game_access_id, gs.protocol_version, gs.asset_version ,
			g.owner_id, g.game_rating_id, g.game_status_id, g.game_name, g.game_description, 
			g.game_synopsis, g.game_creation_date, g.game_image_path, g.game_keywords, 
			g.game_encryption_key, g.last_updated, g.modifiers_id
		INTO __game_id, __parent_game_id, __game_access_id, __protocol_version, __asset_version,
			__owner_id, __game_rating_id, __game_status_id, __game_name, __game_description, 
			__game_synopsis, __game_creation_date, __game_image_path, __game_keywords, 
			__game_encryption_key, __last_updated, __modifiers_id
		FROM games g 
		INNER JOIN game_servers gs ON g.game_id = gs.game_id 
		WHERE gs.server_id = __server_id;
  
		-- if child game, make sure versions match 
		IF __parent_game_id <> 0 THEN
    
			SELECT max(protocol_version), max(asset_version) INTO __parent_protocol_version, __parent_asset_version
			FROM game_servers
			WHERE game_id = __parent_game_id
			AND server_type_id = 1 -- game server
			AND server_status_id = 1; -- running

			IF __parent_protocol_version IS NOT NULL THEN 
      
				IF __parent_asset_version <> __asset_version THEN
					SET __ret = 3;
				ELSEIF __parent_protocol_version <> __protocol_version AND 
						(__parent_protocol_version < MIN_PROTOCOL_NEGO_VERSION OR __protocol_version < LEGACY_PROTOCOL_VERSION) THEN 
					SET __ret = 4;
				END IF;

			ELSE

				SET __ret = 2; -- parent not found

			END IF;

			IF __ret <> 0 THEN
				INSERT into developer.audit_games
				(	game_id, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis, game_creation_date, game_access_id,
					game_image_path, game_keywords, last_updated, modifiers_id, date_modified, operation )
				VALUES
				(	__game_id, __owner_id, __game_rating_id, __game_status_id, __game_name, __game_description, __game_synopsis,
					__game_creation_date, 4, __game_image_path, __game_keywords, __last_updated,
					__modifiers_id, NOW(), 'update' ) ;

				UPDATE games
				SET game_access_id = 4 -- set to inaccessible
				WHERE game_id  = __game_id AND incubator_hosted = 0;
			END IF;
		END IF;
	END IF;

	IF __server_status_id = 2 OR __server_status_id = 0 THEN -- stopping or stopped
	    -- remove any users 
	    CALL server_login_cleanup( __server_id, 11 ); -- 11 = server restart
	END IF;
	
	SELECT __ret as ret;
	
END
;;
DELIMITER ;