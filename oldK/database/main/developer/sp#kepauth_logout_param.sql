-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS kepauth_logout_param;

DELIMITER ;;
CREATE PROCEDURE `kepauth_logout_param`( __user_id INT, __reason_code INT,
							                __server_id INT, __actual_ip VARCHAR(50),
							                OUT __ret INT )
BEGIN
	DECLARE __game_id INT DEFAULT NULL;
	DECLARE __ip_from_db VARCHAR(50) DEFAULT NULL;

	DECLARE __created_date DATETIME;
	DECLARE __deferred_sql VARCHAR(255);
	DECLARE __user_ip_address VARCHAR(50);
	DECLARE __q_ip_address VARCHAR(50);
	DECLARE __q_created_date VARCHAR(50);
	DECLARE __q_now VARCHAR(50);

	DECLARE __last_visit_zone_index INT(11);
	DECLARE __last_visit_zone_instance_id INT(11);
	DECLARE __last_visit_game_id INT(11);
	
	SET __ret = 1; 
	
	CALL logging.logMessage('kepauth_logout_param','TRACE','Entered kepauth_logout_param');

	
	SELECT game_id, actual_ip_address INTO __game_id, __ip_from_db
	  FROM game_servers 
	 WHERE server_id = __server_id;
	   
	IF __game_id IS NULL THEN
		SET __ret = 2; 
	ELSEIF __ip_from_db <> __actual_ip THEN
		SET __ret = 11; 
	ELSE
	
		SELECT created_date, ip_address, game_id INTO __created_date, __user_ip_address, __last_visit_game_id
		  FROM active_logins 
	     WHERE user_id = __user_id
		   AND server_id = __server_id;
		  
		IF __created_date IS NOT NULL THEN
			DELETE FROM active_logins
			 WHERE user_id = __user_id
			   AND server_id = __server_id;
			   
			CALL logging.logMessage('kepauth_logout_param','TRACE','Inserting into developer.login_log');
				
			INSERT IGNORE INTO developer.login_log
					(user_id, server_id, game_id, created_date, logout_date, logout_reason_code, ip_address)
				VALUES
					(__user_id, __server_id, __game_id, __created_date, NOW(), __reason_code, __user_ip_address);

			-- We need to determine where the user is coming from
			-- If we're logging out of WOK, we're coming from the zone in wok.player_zones and just need zone info
			IF wok.is_wok_game(__last_visit_game_id) = 1 THEN
				SET __last_visit_game_id = NULL;
				
				SELECT pz.current_zone_index, pz.current_zone_instance_id INTO __last_visit_zone_index, __last_visit_zone_instance_id
				FROM wok.player_zones pz
				INNER JOIN wok.players p ON pz.player_id = p.player_id
					AND p.kaneva_user_id = __user_id;
			ELSE
				-- If we're logging out of a non-WOK 3dApp, no index/instance is needed
				SET __last_visit_zone_index = NULL;
				SET __last_visit_zone_instance_id = NULL;
			END IF;

			-- Record the visit, only updating the previous record
			CALL developer.record_visit(__user_id, NULL, NULL, NULL, __last_visit_zone_index, __last_visit_zone_instance_id, __last_visit_game_id);
		ELSE
			SET __ret = 8; 
		END IF;
	
	END IF;				
		
	CALL logging.flushLog();

END
;;
DELIMITER ;