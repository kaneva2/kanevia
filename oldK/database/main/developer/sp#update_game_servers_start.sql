-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_game_servers_start;

DELIMITER ;;
CREATE PROCEDURE `update_game_servers_start`(__server_id INT(11),
	__unused INT(11),
	__server_status_id INT(11),
	__ip_address VARCHAR(50),
	__port	INT(11),
	__actual_ip_address VARCHAR(50),
	__server_type INT(11),
	__protocol_version INT(11),
	__schema_version INT(11),
	__server_engine_version VARCHAR(30),
	__asset_version INT(11),
	__parent_game_id INT(11)
	)
BEGIN
	DECLARE __ret INT(11) DEFAULT 0;
	DECLARE __parent_protocol_version INT(11);
	DECLARE __parent_asset_version INT(11);
  
	DECLARE __old_ip_address VARCHAR(50);
	DECLARE __game_id INT(11);
	DECLARE __patch_url VARCHAR(255);
	DECLARE __new_patch_url VARCHAR(255);
	DECLARE __ip_str VARCHAR(60);
	DECLARE done INT(11) DEFAULT 0;
	DECLARE LEGACY_PROTOCOL_VERSION INT DEFAULT 0x8026;
	DECLARE MIN_PROTOCOL_NEGO_VERSION INT DEFAULT 0x8027;
	DECLARE __patch_cursor CURSOR FOR SELECT patch_url FROM game_patch_urls WHERE game_id = __game_id;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
  
	-- if child game, make sure versions match 
	IF __parent_game_id <> 0 THEN
	
		SELECT max(protocol_version), max(asset_version) INTO __parent_protocol_version, __parent_asset_version
		  FROM game_servers
		 WHERE game_id = __parent_game_id
		   AND server_type_id = 1 -- game server
		   AND server_status_id = 1; -- running

		IF __parent_protocol_version IS NOT NULL THEN 
			IF __parent_asset_version <> __asset_version THEN
			   SET __ret = 3;
			ELSEIF __parent_protocol_version <> __protocol_version AND 
					(__parent_protocol_version < MIN_PROTOCOL_NEGO_VERSION OR __protocol_version < LEGACY_PROTOCOL_VERSION) THEN 
			   SET __ret = 4;
			END IF;			   
			
		ELSE
			SET __ret = 2; -- parent not found
		END IF;
		 
	END IF; 

	IF __ret <> 0 THEN 	  
		SET __server_status_id = 5; -- set to failed if error	
	END IF;

	SELECT actual_ip_address, game_id INTO __old_ip_address, __game_id
	FROM game_servers
	WHERE server_id = __server_id;

	IF __old_ip_address <> '' AND __old_ip_address <> __ip_address THEN

	OPEN __patch_cursor;

	REPEAT
	  FETCH __patch_cursor INTO __patch_url;
	  IF NOT done THEN
	  
		-- extract the ip address or hostname from patch url
		SELECT SUBSTRING_INDEX(__patch_url, '/', 3) INTO __ip_str;
		SELECT SUBSTRING_INDEX(__ip_str, '/', -1) INTO __ip_str;
	    
		IF __ip_str = __old_ip_address THEN
	   
		  SELECT REPLACE(__patch_url, __old_ip_address, __ip_address) INTO __new_patch_url;
		  UPDATE game_patch_urls 
			SET patch_url = __new_patch_url
			WHERE patch_url = __patch_url 
			  AND game_id = __game_id;  
		  SET done = 1;

		END IF;
	    
	  END IF;
	UNTIL done END REPEAT;

	CLOSE __patch_cursor;

	END IF;
    
	UPDATE game_servers
		SET number_of_players = 0, 
        server_status_id = __server_status_id,
        last_ping_datetime = NOW(),
        ip_address = __ip_address,
        port = __port,
		actual_ip_address = __actual_ip_address,
		server_type_id = __server_type,
        server_started_date = NOW(),
        protocol_version = __protocol_version,
        schema_version = __schema_version,
        server_engine_version = __server_engine_version,
        asset_version = __asset_version
    WHERE server_id = __server_id;

    
    -- remove any users 
    CALL server_login_cleanup( __server_id, 11 ); -- 11 = server restart

    select __ret as ret;
END
;;
DELIMITER ;