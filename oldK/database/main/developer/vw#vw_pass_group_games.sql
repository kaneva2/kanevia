-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS vw_pass_group_games;

DELIMITER ;;

 CREATE VIEW `developer`.`vw_pass_group_games` AS SELECT `pg`.`game_id` AS `game_id`,`pg`.`pass_group_id` AS `pass_group_id`,`sub`.`end_date` AS `end_date` 
 FROM ( ( ( ( `developer`.`game_pass_groups` `pg` 
 JOIN `developer`.`games` `g` on( ( `pg`.`game_id` = `g`.`game_id`) ) )  
 JOIN `developer`.`company_developers` `cd` on( ( `g`.`owner_id` = `cd`.`company_id`) ) )  
 JOIN `kaneva`.`subscriptions_to_passgroups` `sub_pass` on( ( `pg`.`pass_group_id` = `sub_pass`.`pass_group_id`) ) )  
 JOIN `kaneva`.`user_subscriptions` `sub` on( ( ( `sub`.`subscription_id` = `sub_pass`.`subscription_id`)  
 AND ( `cd`.`user_id` = `sub`.`user_id`) ) ) )  
 WHERE ( ( `sub`.`status_id` 
 IN ( 2,4) )  
 AND ( `sub`.`end_date` > now( ) ) ) 
;;
DELIMITER ;