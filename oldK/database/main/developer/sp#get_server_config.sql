-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_server_config;

DELIMITER ;;
CREATE PROCEDURE `get_server_config`( IN __type INT, IN __actual_ip VARCHAR(50) )
BEGIN
	-- get the server's configuration string as tab delimited values
	-- we prepend with rc\tdesc
	DECLARE __is_external INT;
	DECLARE __ret VARCHAR(512);
	
	SELECT is_external INTO __is_external
	  FROM game_servers 
	 WHERE actual_ip_address = __actual_ip
	   AND server_status_id in (1,3) -- running, starting
	 LIMIT 1;

	IF __is_external IS NOT NULL THEN
		IF __is_external = 1 THEN 
			SELECT string_value INTO __ret
			  FROM wok.getset_server_config_values
			 WHERE server_name = "external"
			   AND engine_type = __type
			   AND value_name = "RemoteConfigString";
		ELSE
			SELECT string_value INTO __ret
			  FROM wok.getset_server_config_values
			 WHERE server_name = "internal"
			   AND engine_type = __type
			   AND value_name = "RemoteConfigString";
		END IF;
		IF __ret IS NOT NULL THEN 
			SELECT CONCAT( '0\tok\t', __ret ) as ret;
		ELSE
			SELECT CONCAT( '2\tNo config data found for server') as ret;
		END IF;			
	ELSE
		SELECT CONCAT( '3\tServer not found or not running at: ', __actual_ip ) as ret;
	END IF;
END
;;
DELIMITER ;