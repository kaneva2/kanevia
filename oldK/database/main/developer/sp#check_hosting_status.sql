-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS check_hosting_status;

DELIMITER ;;
CREATE PROCEDURE `check_hosting_status`( 
    IN  __game_key    VARCHAR(100),
    OUT __reason_code INT,
    OUT __reason_desc VARCHAR(200)
)
    COMMENT '\n    --\n  '
this_proc:
BEGIN

    DECLARE __fixed_game_key INT DEFAULT  0;
    DECLARE __game_id INT DEFAULT  0;
    DECLARE __hosting_status_id INT DEFAULT -1;
    DECLARE __hosting_status VARCHAR(30);
    DECLARE __hosting_status_user_message VARCHAR(128);

    -- by default we allow access to the app
    DECLARE CONTINUE HANDLER FOR SQLSTATE '22007' SET __reason_code = 0;


  CALL logging.logMsg('TRACE', SCHEMA(), 'check_hosting_status', CONCAT('BEGIN (__game_key[', __game_key, '])')); 
  

    -- 
    -- __game_key is either game_id or game_name
    -- 
    SET __fixed_game_key = CAST(__game_key as SIGNED);    
    
    IF __fixed_game_key <> 0 THEN
      CALL logging.logMsg('TRACE', SCHEMA(), 'check_hosting_status', CONCAT('__game_key appears to be game_id [', __fixed_game_key, ']'));
    
        -- try game_key as game_id
        SELECT 
            gths.game_id,
            gths.hosting_status_id,
            hs.hosting_status, 
            hs.hosting_status_user_message
        INTO 
            __game_id,
            __hosting_status_id,
            __hosting_status,
            __hosting_status_user_message
        FROM 
            developer.games_to_hosting_status gths
            LEFT JOIN developer.hosting_status hs ON gths.hosting_status_id = hs.hosting_status_id
        WHERE 
            gths.game_id = __fixed_game_key;

    ELSE
      -- try game_key as game_name
      CALL logging.logMsg('TRACE', SCHEMA(), 'check_hosting_status', CONCAT( '__game_key appears to be game_name [', __game_key, ']'));

        
       SELECT 
            g.game_id, 
            gths.hosting_status_id,
            hs.hosting_status, 
            hs.hosting_status_user_message
        INTO 
            __game_id,
            __hosting_status_id,
            __hosting_status,
            __hosting_status_user_message
        FROM 
            developer.games g 
            INNER JOIN developer.games_to_hosting_status gths ON g.game_id = gths.game_id
            INNER JOIN developer.hosting_status hs ON hs.hosting_status_id = gths.hosting_status_id
        WHERE 
            g.game_name = __game_key;

    END IF;


  -- DEBUGING 
   -- SELECT __game_id, __hosting_status_id, __hosting_status, __hosting_status_user_message;
  -- DEBUGING 

    -- interpret the results 
    -- (If there are no records found then no login restrictrions)  
    IF __hosting_status_id = -1 THEN
        SET __reason_code = 0;
        SET __reason_desc = "";
    ELSE
        SET __reason_code = 1000 + __hosting_status_id;
        SET __reason_desc = __hosting_status_user_message;
    END IF;
  
  CALL logging.logMsg('TRACE', SCHEMA(), 'check_hosting_status', CONCAT('END (__game_key[', __game_key, '] __reason_code[',CAST(__reason_code AS CHAR),'] __reason_desc[',__reason_desc,'])')); 
     

END
;;
DELIMITER ;