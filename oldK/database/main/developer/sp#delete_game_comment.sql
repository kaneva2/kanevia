-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_game_comment;

DELIMITER ;;
CREATE PROCEDURE `delete_game_comment`(in p_id int)
BEGIN
	delete from game_comments where parent_comment_id=p_id;
	delete from game_comments where reply_to_comment_id=p_id;
	delete from game_comments where game_comment_id=p_id;
END
;;
DELIMITER ;