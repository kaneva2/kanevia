-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_game_info;

DELIMITER ;;
CREATE PROCEDURE `get_game_info`( 
  IN __userId     INT(11),
  IN __gameKey varchar(100),
  IN __serverType int(11),
  OUT __reason_code int,
  OUT __reason_desc varchar(200),
  OUT __server varchar(100),
  OUT __port int(11),
  OUT __game_id int(11),
  OUT __parent_game_id int,
  OUT __game_name varchar(100),
  OUT __server_hostname varchar(100)
)
this_proc:
BEGIN

  DECLARE __admin_only enum ('T', 'F') DEFAULT 'F';
  DECLARE __members_only enum ('T', 'F') DEFAULT 'F';
  DECLARE __access_id int DEFAULT 1; -- public

  DECLARE CONTINUE HANDLER FOR SQLSTATE '22007' SET __game_id = 0;

  SET __reason_code = 2; -- not found
  SET __game_id = 0;
  SET __parent_game_id = NULL;
  SET __server = NULL;


  CALL logging.logMsg('TRACE', SCHEMA(), 'get_game_info', CONCAT('BEGIN (__userId[', CAST(__userId AS CHAR), '] __gameKey[', __gameKey, '] __serverType[', CAST(__serverType AS CHAR), '])'));



  -- apply any hosting blocks (per app blocks)
  CALL check_hosting_status(__gameKey, __reason_code, __reason_desc);
  IF (__reason_code > 0) THEN
    LEAVE this_proc;
  END IF;

  -- reset reason code since we passed hosting blocks
  SET __reason_code = 2; -- not found

  -- CALL logging.logMessage('get_game_info', 'TRACE', CONCAT('no hosting blocks for gameKey[', __gameKey, ']'));
  -- CALL logging.flushLog();

  SET __game_id = CAST(__gameKey AS SIGNED);

  IF __game_id <> 0 THEN

    CALL logging.logMsg('TRACE', SCHEMA(), 'get_game_info', CONCAT('gameKey[', __gameKey, '] considered as gameId[', CAST(__game_id AS CHAR), ']'));


    SELECT
      g.game_name,
      g.parent_game_id,
      g.game_access_id INTO __game_name, __parent_game_id, __access_id
    FROM developer.games g
    WHERE g.game_id = __game_id;

    IF __game_name IS NOT NULL THEN

      SELECT
        ge.game_id,
        ge.ip_address,
        ge.port,
        ge.server_name,
        server_engine_admin_only INTO __game_id, __server, __port, __server_hostname, __admin_only
      FROM developer.game_servers ge
      WHERE ge.server_status_id = 1
      AND game_id = __game_id
      AND TIMESTAMPDIFF(MINUTE, last_ping_datetime, NOW()) < deadServerInterval()
      AND ge.visibility_id = 1
      AND ge.server_type_id = __serverType
      ORDER BY number_of_players LIMIT 1;
    END IF;
  ELSE
    SET __game_name = __gameKey;

    CALL logging.logMsg('TRACE', SCHEMA(), 'get_game_info', CONCAT('gameKey[', __gameKey, '] considered as gameName[', __game_name, ']'));


    IF __serverType <> 6 THEN -- 6 = tell servers

      SELECT
        g.game_id,
        g.parent_game_id,
        g.game_access_id INTO __game_id, __parent_game_id, __access_id
      FROM developer.games g
      WHERE g.game_name = __game_name;

      IF __game_id IS NOT NULL THEN
        SELECT
          ge.ip_address,
          ge.port,
          ge.server_name INTO __server, __port, __server_hostname
        FROM developer.game_servers ge
        WHERE ge.server_status_id = 1
        AND ge.game_id = __game_id
        AND TIMESTAMPDIFF(MINUTE, last_ping_datetime, NOW()) < deadServerInterval()
        AND ge.visibility_id = 1
        AND ge.server_type_id = __serverType
        ORDER BY number_of_players LIMIT 1;
      END IF;
    ELSE

      SELECT
        ge.game_id,
        ge.ip_address,
        ge.port,
        g.parent_game_id,
        ge.server_name INTO __game_id, __server, __port, __parent_game_id, __server_hostname
      FROM developer.game_servers ge
        INNER JOIN developer.games g
          ON ge.game_id = g.game_id
      WHERE ge.server_status_id = 1
      AND TIMESTAMPDIFF(MINUTE, last_ping_datetime, NOW()) < deadServerInterval()
      AND ge.visibility_id = 1
      AND ge.server_type_id = __serverType
      ORDER BY number_of_players LIMIT 1;


    END IF;

  END IF;


  IF __access_id = 3 THEN
    -- banned!
    SET __reason_code = 14;
  ELSEIF __access_id = 5 THEN
    -- deleted!
    SET __reason_code = 15;
  ELSEIF __access_id = 6 THEN
    -- purged!  same as deleted
    SET __reason_code = 15;
  ELSE

    IF __access_id = 2 THEN
      SET __admin_only = 'T';
	ELSEIF __access_id = 7 THEN
	  SET __members_only = 'T';
    END IF;

    IF __parent_game_id IS NULL THEN
      SET __reason_code = 2;
    ELSEIF __server IS NOT NULL THEN

      IF __game_id IS NOT NULL THEN

        IF __serverType <> 6 THEN
          SELECT
            check_game_auth(__userId, __game_id, __parent_game_id, __admin_only, __members_only) INTO __reason_code;
        ELSE
          SET __reason_code = 1;
        END IF;

      END IF;
    ELSE
      SET __reason_code = 4;
    END IF;

  END IF;

  IF __reason_code = 1 THEN
    SET __reason_desc = "Success";
  ELSEIF __reason_code = 2 THEN
    SET __reason_desc = "World Not Found";
  ELSEIF __reason_code = 4 THEN
    SET __reason_desc = "World Unavailable";
  ELSEIF __reason_code = 9 THEN
    SET __reason_desc = "World Overpopulated - Try Again Later";
  ELSEIF __reason_code = 10 THEN
    SET __reason_desc = "World Requires Access Pass";
  ELSEIF __reason_code = 12 THEN
    SET __reason_desc = "World For Administrators Only";
  ELSEIF __reason_code = 13 THEN
    SET __reason_desc = CONCAT('Do you want to allow access to World "', __game_name, '"?');
  ELSEIF __reason_code = 14 THEN
    SET __reason_desc = "World Banned";
  ELSEIF __reason_code = 15 THEN
    SET __reason_desc = "World Deleted";
  ELSEIF __reason_code = 16 THEN
    SET __reason_desc = "World For Members Only";
  ELSE
    SET __reason_desc = CONCAT("World Not Found ", __reason_code);
  END IF;
	
END
;;
DELIMITER ;