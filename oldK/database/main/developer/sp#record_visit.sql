-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS record_visit;

DELIMITER ;;
CREATE PROCEDURE `record_visit`( IN kanevaUserId INT(11),
	IN zoneIndex INT(11), 		IN zoneInstanceId INT(11), 	IN gameId INT(11),
	IN oldZoneIndex INT(11), 	IN oldInstanceId INT(11), 	IN oldGameId INT(11))
BEGIN
	DECLARE zoneType INT(11);
	DECLARE lastVisitLogId INT(11);

	-- visit_log records WOK records with a '0' game_id
	-- Just as a precaution, make sure inputs meet expectations
	IF wok.is_wok_game(gameId) = 1 THEN
		SET gameId = NULL;
	END IF;

	IF wok.is_wok_game(oldGameId) = 1 THEN
		SET oldGameId = NULL;
	END IF;

	-- Pull the last visit_log record for this user for their previous zone
	-- Should never be more than 1 record, but just in case...
	SELECT vl.visit_log_id INTO lastVisitLogId
	FROM visit_log vl
	WHERE vl.orphaned = 'N' 
		AND vl.visit_ended_at IS NULL
		AND vl.kaneva_user_id = kanevaUserId
		AND vl.zone_index = COALESCE(oldZoneIndex, 0)
		AND vl.zone_instance_id = COALESCE(oldInstanceId, 0)
		AND vl.game_id = COALESCE(oldGameId, 0)
	ORDER BY vl.visited_at DESC
	LIMIT 1;

	-- If the destination parameters are null, just update
	-- kepauth_logout_param calls this when logging out from a 3dApp
	IF zoneIndex IS NULL AND zoneInstanceId IS NULL AND gameId IS NULL AND lastVisitLogId IS NOT NULL THEN
		UPDATE visit_log
		SET visit_ended_at = NOW()
		WHERE visit_log_id = lastVisitLogId;

		-- Clean up any records that should be orphaned
		-- This must be done after the update
		UPDATE visit_log
		SET orphaned = 'Y'
		WHERE kaneva_user_id = kanevaUserId
			AND visit_ended_at IS NULL
			AND orphaned = 'N';

	ELSE
		-- If we have a log to enddate, update the log
		-- wok.playerZoning calls this when zoning between places in WOK
		IF lastVisitLogId IS NOT NULL THEN 
			UPDATE visit_log
			SET visit_ended_at = NOW()
			WHERE visit_log_id = lastVisitLogId;
		END IF;

		-- Clean up any records that should be orphaned
		-- This must be done after the update and before the insert
		UPDATE visit_log
		SET orphaned = 'Y'
		WHERE kaneva_user_id = kanevaUserId
			AND visit_ended_at IS NULL
			AND orphaned = 'N';

		-- Make sure we have valid data to insert
		IF gameId IS NOT NULL OR (zoneIndex IS NOT NULL AND zoneInstanceId IS NOT NULL) THEN

			IF COALESCE(zoneIndex, 0) = 0 THEN
				SET zoneType = NULL;
			ELSE	
				SET zoneType = wok.zoneType(zoneIndex);
			END IF;
				
			INSERT INTO visit_log (zone_type, zone_index, zone_instance_id, game_id, kaneva_user_id, visited_at)
				VALUES (COALESCE(zoneType, 0), COALESCE(zoneIndex, 0), COALESCE(zoneInstanceId, 0), COALESCE(gameId, 0), kanevaUserId, NOW());
		END IF;
	END IF;
END
;;
DELIMITER ;