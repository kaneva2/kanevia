-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_game_servers;

DELIMITER ;;
CREATE PROCEDURE `add_game_servers`( IN __server_name VARCHAR(100), IN __game_id INT(11), IN __visibility_id INT(11), 
                                      IN __server_started_date DATETIME, IN __ip_address VARCHAR(50), IN __port INT(11), 
                                      IN __number_of_players INT(11), IN __unused INT(11), IN __last_ping_datetime DATETIME, 
                                      IN __modifiers_id INT(10) unsigned,
                                      INOUT __server_id INT)
BEGIN	
 -- optional server ID. If 0, will use auto-increment value.
	call logging.logMessage( 'add_game_servers', 'DEBUG', concat( 'gameid=', __game_id, ';maxPlayers=', __unused ) );
	call logging.flushLog();

	IF __server_id=0 THEN
		-- Insert with auto-increment key
		INSERT INTO game_servers(
			server_name, game_id, visibility_id, server_started_date, ip_address, port, number_of_players, 
			last_ping_datetime, modifiers_id, is_external, actual_ip_address)
		VALUES (
			__server_name, __game_id, __visibility_id, __server_started_date, __ip_address, __port, __number_of_players, 
			__last_ping_datetime, __modifiers_id, NOT wok.is_wok_game(__game_id), '' );

		SET __server_id = @@IDENTITY;

	ELSE
		-- If __server_id is passed in with a valid value, do a full insert
		-- NOTE that this is for local ping synchronization only and should only be called on preview.
		INSERT IGNORE INTO game_servers(
			server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, number_of_players, 
			last_ping_datetime, modifiers_id, is_external, actual_ip_address)
		VALUES (
			__server_id, __server_name, __game_id, __visibility_id, __server_started_date, __ip_address, __port, __number_of_players, 
			__last_ping_datetime, __modifiers_id, NOT wok.is_wok_game(__game_id), '' );
	END IF;

	INSERT into developer.audit_game_servers(
		server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, number_of_players,
	    max_players, last_ping_datetime, server_type_id, modifiers_id, date_modified, operation )
    SELECT
    	server_id, __server_name, __game_id, __visibility_id, __server_started_date, __ip_address, __port, server_status_id, __number_of_players,
    	max_players, __last_ping_datetime, server_type_id, __modifiers_id, Now(), 'insert' 
	FROM developer.game_servers
	WHERE server_id = __server_id;

END
;;
DELIMITER ;