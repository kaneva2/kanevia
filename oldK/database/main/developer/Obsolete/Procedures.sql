-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

\. sp#add_game_server.sql
\. sp#add_game_servers.sql            
-- \. fn#check_game_auth.sql             
-- \. sp#db_update_and_maintenance.sql   
\. fn#deadServerInterval.sql          
\. sp#delete_game.sql                 
-- \. sp#delete_game_comment.sql         
\. sp#get_game_info.sql               
\. sp#remove_dead_servers.sql         
-- \. sp#remove_expired_sticky_blasts.sql
\. sp#remove_game_servers.sql         
-- \. sp#update_cache.sql                
\. sp#update_game_servers.sql         
\. sp#update_game_servers_ping.sql    
\. sp#update_game_servers_start.sql   
