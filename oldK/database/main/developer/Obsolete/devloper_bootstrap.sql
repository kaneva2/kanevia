-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 


insert into `client_platforms`(`client_platform_id`,`client_platform`) values (1,'PC Low Spec');
insert into `client_platforms`(`client_platform_id`,`client_platform`) values (2,'PC Current');
insert into `client_platforms`(`client_platform_id`,`client_platform`) values (3,'PC Next Gen');
insert into `client_platforms`(`client_platform_id`,`client_platform`) values (4,'Other');


insert into `game_access`(`game_access_id`,`game_access`) values (1,'public');
insert into `game_access`(`game_access_id`,`game_access`) values (2,'private');


insert into `game_categories`(`category_id`,`description`) values (1,'Action & Adventure');
insert into `game_categories`(`category_id`,`description`) values (2,'Aviation & Air Combat');
insert into `game_categories`(`category_id`,`description`) values (3,'Business');
insert into `game_categories`(`category_id`,`description`) values (4,'Casual Games');
insert into `game_categories`(`category_id`,`description`) values (5,'Driving & Racing');
insert into `game_categories`(`category_id`,`description`) values (6,'Education');
insert into `game_categories`(`category_id`,`description`) values (7,'Fantasy');
insert into `game_categories`(`category_id`,`description`) values (8,'Fighting');
insert into `game_categories`(`category_id`,`description`) values (9,'Future');
insert into `game_categories`(`category_id`,`description`) values (10,'Gambling');
insert into `game_categories`(`category_id`,`description`) values (11,'Modern');
insert into `game_categories`(`category_id`,`description`) values (12,'Puzzles');
insert into `game_categories`(`category_id`,`description`) values (13,'Roleplaying');
insert into `game_categories`(`category_id`,`description`) values (14,'Simulation');
insert into `game_categories`(`category_id`,`description`) values (15,'Sports');
insert into `game_categories`(`category_id`,`description`) values (16,'Strategy');


insert into `game_license_status`(`license_status_id`,`license_status`) values (1,'active');
insert into `game_license_status`(`license_status_id`,`license_status`) values (2,'deleted');
insert into `game_license_status`(`license_status_id`,`license_status`) values (3,'waiting_on_it');
insert into `game_license_status`(`license_status_id`,`license_status`) values (4,'failed_payment_authorization');
insert into `game_license_status`(`license_status_id`,`license_status`) values (5,'unsubscribe');
insert into `game_license_status`(`license_status_id`,`license_status`) values (6,'pending_auth');
insert into `game_license_status`(`license_status_id`,`license_status`) values (7,'waiting_on_it_takedown');


insert into `game_license_subscriptions`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (1,'Trial',1,0,'KPOINT',1,100000);
insert into `game_license_subscriptions`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (2,'Designer',1,10,'KPOINT',100,100000);
insert into `game_license_subscriptions`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (3,'Professional',1,20000,'KPOINT',2000,100000);


insert into `game_ratings`(`game_rating_id`,`game_rating`,`description`,`minimum_age`,`is_mature`) values (1,'General','All Audiences (G)',0,0);
insert into `game_ratings`(`game_rating_id`,`game_rating`,`description`,`minimum_age`,`is_mature`) values (11,'Teen','14 and Up (PG-14)',14,0);
insert into `game_ratings`(`game_rating_id`,`game_rating`,`description`,`minimum_age`,`is_mature`) values (21,'Adult','18 and older (R)',18,1);
insert into `game_ratings`(`game_rating_id`,`game_rating`,`description`,`minimum_age`,`is_mature`) values (31,'Mature','21 and older (NC-17)',14,1);


insert into `game_role_ids`(`game_role_id`,`game_role`) values (1,'owner');
insert into `game_role_ids`(`game_role_id`,`game_role`) values (2,'administrator');
insert into `game_role_ids`(`game_role_id`,`game_role`) values (3,'IT');
insert into `game_role_ids`(`game_role_id`,`game_role`) values (4,'CSR');


insert into `game_server_status`(`server_status_id`,`name`,`description`) values (0,'stopped','The server has stopped');
insert into `game_server_status`(`server_status_id`,`name`,`description`) values (1,'running','The server is running');
insert into `game_server_status`(`server_status_id`,`name`,`description`) values (2,'stopping','The server is stopping');
insert into `game_server_status`(`server_status_id`,`name`,`description`) values (3,'starting','The server is starting');
insert into `game_server_status`(`server_status_id`,`name`,`description`) values (4,'locked','The server has locked');
insert into `game_server_status`(`server_status_id`,`name`,`description`) values (5,'failed','The server has stopped');


insert into `game_server_types`(`server_type_id`,`name`,`description`) values (1,'Game','A Game Server Engine');
insert into `game_server_types`(`server_type_id`,`name`,`description`) values (2,'AI','A Game AI Engine');
insert into `game_server_types`(`server_type_id`,`name`,`description`) values (3,'Unknown','An Unknown Server');


insert into `game_server_visibility`(`visibility_id`,`name`,`description`) values (1,'Public','Anyone can access this server');
insert into `game_server_visibility`(`visibility_id`,`name`,`description`) values (2,'Private','Not in rotation');


insert into `game_status`(`game_status_id`,`game_status`) values (1,'active');
insert into `game_status`(`game_status_id`,`game_status`) values (2,'inactive');


insert into `project_startdates`(`project_startdate_id`,`project_startdate`) values (1,'In Progress');
insert into `project_startdates`(`project_startdate_id`,`project_startdate`) values (2,'1 Month');
insert into `project_startdates`(`project_startdate_id`,`project_startdate`) values (3,'3 Month');
insert into `project_startdates`(`project_startdate_id`,`project_startdate`) values (4,'6 Months');
insert into `project_startdates`(`project_startdate_id`,`project_startdate`) values (5,'More than 6 Months');
insert into `project_startdates`(`project_startdate_id`,`project_startdate`) values (6,'Don''t Know');


insert into `project_status`(`project_status_id`,`project_status`) values (1,'Planning');
insert into `project_status`(`project_status_id`,`project_status`) values (2,'In Development');


insert into `vworld_involvement`(`vworld_involvement_id`,`vworld_involvement`) values (1,'Studio');
insert into `vworld_involvement`(`vworld_involvement_id`,`vworld_involvement`) values (2,'3rd-party Developer');
insert into `vworld_involvement`(`vworld_involvement_id`,`vworld_involvement`) values (3,'Educational Institution');
insert into `vworld_involvement`(`vworld_involvement_id`,`vworld_involvement`) values (4,'Government');
insert into `vworld_involvement`(`vworld_involvement_id`,`vworld_involvement`) values (5,'Corporation');

