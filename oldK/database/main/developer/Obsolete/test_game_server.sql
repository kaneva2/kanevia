-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- string sqlString = "CALL add_game_servers (@server_name, @game_id, @visibility_id, @server_started_date, @ip_address,
--	@port, @number_of_players, @max_players, @last_ping_datetime, @modifiers_id) ";
SET @serverId = 0;
CALL add_game_servers ('jeffrtest', 7777, 1, '2008-09-19 12:00:00', 'localhost',	0, 0, 1000, '2008-09-19 12:00:00', 0, @serverId) ;

-- string sqlString = "CALL remove_game_servers (@serverID) ";
CALL remove_game_servers (801) ;

-- string sqlString = "CALL update_game_servers_start (@serverID, @maxcount, @serverstatusID, @ipaddress, @port, @protocolversion, @schemaversion, @serverengineversion )";
CALL update_game_servers_start (721, 75, 0, 'jreinhard', 7, 0, 0, 0 );

-- string sqlString = "CALL update_game_servers_ping (@serverid, @numberofplayers, @maxplayers, @serverstatusid, @serverengineadminonly )";
CALL update_game_servers_ping (721, 5, 100, 0, 'F' );

-- CALL update_game_servers (@serverid, @server_name, @game_id, @visibility_id, @server_started_date, @ip_address, @port, 
--			@number_of_players, @max_players, @last_ping_datetime, @modifiers_id);
CALL update_game_servers (811, 'jeffrtest',7777, 1, '2008-09-19 12:00:00', 'localhost', 0, 7, 1000, '2008-09-19 12:00:00', 0);
