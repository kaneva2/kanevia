-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

OBSOLETE!!!

CREATE TABLE `audit_game_licenses`(
	`game_license_id` int(11) NOT NULL  , 
	`game_id` int(11) NOT NULL  DEFAULT '0' COMMENT 'FK to games' , 
	`license_subscription_id` int(11) NULL  COMMENT 'FK to game_license_subcription, trial/consumer/pro' , 
	`game_key` varchar(100) COLLATE latin1_swedish_ci NOT NULL  COMMENT 'used by all servers for game' , 
	`start_date` datetime NULL  COMMENT 'for tracking trials' , 
	`end_date` datetime NULL  COMMENT 'for tracking trials' , 
	`purchase_date` datetime NULL  , 
	`license_status_id` int(11) NOT NULL  DEFAULT '0' COMMENT 'FK to game_license_status active, etc.' , 
	`modifiers_id` int(10) unsigned NOT NULL  , 
	`date_modified` datetime NOT NULL  DEFAULT '0000-00-00 00:00:00' , 
	`operation` varchar(20) COLLATE latin1_swedish_ci NOT NULL  
) ENGINE=InnoDB DEFAULT CHARSET='latin1';

CREATE TABLE `audit_game_servers`(
	`server_id` int(11) NOT NULL  , 
	`server_name` varchar(100) COLLATE latin1_swedish_ci NOT NULL  COMMENT 'hostname of server, preconfigured, must match when pinging' , 
	`game_id` int(11) NOT NULL  COMMENT 'FK to games' , 
	`visibility_id` int(11) NOT NULL  DEFAULT '0' COMMENT 'FK to game_server_visibility, public, etc.' , 
	`server_started_date` datetime NULL  COMMENT 'when last started' , 
	`ip_address` varchar(50) COLLATE latin1_swedish_ci NOT NULL  COMMENT 'external IP used by clients' , 
	`port` int(11) NOT NULL  DEFAULT '0' COMMENT 'port used by client' , 
	`server_status_id` int(11) NOT NULL  DEFAULT '0' COMMENT 'FK to game_server_status, running, etc.' , 
	`number_of_players` int(11) NULL  COMMENT 'current player count' , 
	`max_players` int(11) NOT NULL  DEFAULT '0' COMMENT 'as configured on server' , 
	`last_ping_datetime` datetime NULL  , 
	`server_type_id` int(11) NOT NULL  , 
	`modifiers_id` int(10) unsigned NOT NULL  , 
	`date_modified` datetime NOT NULL  DEFAULT '0000-00-00 00:00:00' , 
	`operation` varchar(20) COLLATE latin1_swedish_ci NOT NULL  
) ENGINE=InnoDB DEFAULT CHARSET='latin1';

CREATE TABLE `audit_games`(
	`game_id` int(10) unsigned NOT NULL  , 
	`owner_id` int(10) unsigned NOT NULL  DEFAULT '0' , 
	`game_rating_id` smallint(5) unsigned NOT NULL  DEFAULT '1' , 
	`game_status_id` smallint(5) unsigned NOT NULL  DEFAULT '1' , 
	`game_name` varchar(100) COLLATE latin1_swedish_ci NOT NULL  , 
	`game_description` text COLLATE latin1_swedish_ci NULL  , 
	`game_synopsis` varchar(50) COLLATE latin1_swedish_ci NULL  , 
	`game_creation_date` datetime NOT NULL  DEFAULT '0000-00-00 00:00:00' , 
	`game_access_id` varchar(45) COLLATE latin1_swedish_ci NOT NULL  DEFAULT '1' , 
	`game_image_path` varchar(255) COLLATE latin1_swedish_ci NULL  , 
	`game_keywords` text COLLATE latin1_swedish_ci NULL  , 
	`last_updated` datetime NULL  , 
	`modifiers_id` int(10) unsigned NOT NULL  DEFAULT '0' , 
	`date_modified` datetime NOT NULL  DEFAULT '0000-00-00 00:00:00' , 
	`operation` varchar(20) COLLATE latin1_swedish_ci NOT NULL  
) ENGINE=InnoDB DEFAULT CHARSET='latin1';

CREATE TABLE `client_platforms`(
	`client_platform_id` int(10) unsigned NOT NULL  DEFAULT '0' , 
	`client_platform` varchar(50) COLLATE latin1_swedish_ci NOT NULL  , 
	PRIMARY KEY (`client_platform_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='latin1';

CREATE TABLE `company_developers`(
	`company_id` int(10) unsigned NOT NULL  DEFAULT '0' , 
	`user_id` int(10) unsigned NOT NULL  DEFAULT '0' , 
	`role_id` int(10) unsigned NOT NULL  DEFAULT '0' , 
	PRIMARY KEY (`company_id`,`user_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='latin1';

CREATE TABLE `game_access`(
	`game_access_id` smallint(5) unsigned NOT NULL  , 
	`game_access` varchar(50) COLLATE latin1_swedish_ci NOT NULL  , 
	PRIMARY KEY (`game_access_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='latin1';

CREATE TABLE `game_categories`(
	`category_id` int(11) NOT NULL  auto_increment , 
	`description` varchar(100) COLLATE latin1_swedish_ci NOT NULL  , 
	PRIMARY KEY (`category_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='latin1';

CREATE TABLE `game_comments`(
	`game_comment_id` int(10) unsigned NOT NULL  auto_increment , 
	`user_id` int(10) unsigned NOT NULL  DEFAULT '0' , 
	`game_id` int(10) unsigned NOT NULL  DEFAULT '0' , 
	`parent_comment_id` int(10) unsigned NOT NULL  DEFAULT '0' , 
	`reply_to_comment_id` int(10) unsigned NOT NULL  DEFAULT '0' , 
	`comment` text COLLATE latin1_swedish_ci NULL  , 
	`ip_address` varchar(50) COLLATE latin1_swedish_ci NULL  , 
	`created_date` datetime NOT NULL  DEFAULT '0000-00-00 00:00:00' , 
	`last_updated_date` datetime NOT NULL  DEFAULT '0000-00-00 00:00:00' , 
	`last_updated_user_id` int(10) unsigned NOT NULL  DEFAULT '0' , 
	PRIMARY KEY (`game_comment_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='latin1';

CREATE TABLE `game_license_status`(
	`license_status_id` smallint(5) unsigned NOT NULL  , 
	`license_status` varchar(50) COLLATE latin1_swedish_ci NOT NULL  , 
	PRIMARY KEY (`license_status_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='latin1';

CREATE TABLE `game_server_status` (
  `server_status_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY  (`server_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `game_server_types` (
  `server_type_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY  (`server_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `game_license_subscriptions` (
  `license_subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `length_of_subscription` int(11) NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `amount_kei_point_id` varchar(50) NOT NULL DEFAULT '',
  `max_server_users` int(11) DEFAULT NULL,
  `disk_quota` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY  (`license_subscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `game_server_visibility` (
  `visibility_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY  (`visibility_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `game_server_history` (
  `server_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `server_id` int(11)   NOT NULL DEFAULT '0',
  `player_count` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_address` varchar(20) DEFAULT NULL,
  `port` int(11)   NULL,
  PRIMARY KEY  (`server_history_id`),
  KEY `FK_server_history_current_game_engines` (`server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `game_licenses` (
  `game_license_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to assets', 
  `license_type` int(11) DEFAULT NULL COMMENT 'FK to game_license_subcription, trial/consumer/pro',
  `game_key` varchar(100) NOT NULL DEFAULT '' COMMENT 'used by all servers for game',
  `start_date` datetime DEFAULT NULL COMMENT 'for tracking trials', 
  `end_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `purchase_date` datetime DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_license_status active, etc.', 
  PRIMARY KEY  (`game_license_id`),
  KEY `INDEX_asset_id` (`asset_id`),
  KEY `INDEX_game_key` (`game_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT = 'track extra data for a game';

CREATE TABLE `game_ratings`(
	`game_rating_id` int(10) unsigned NOT NULL  auto_increment , 
	`game_rating` varchar(50) COLLATE latin1_swedish_ci NOT NULL  , 
	`description` varchar(255) COLLATE latin1_swedish_ci NULL  , 
	`minimum_age` smallint(5) unsigned NOT NULL  DEFAULT '14' , 
	`is_mature` tinyint(3) unsigned NOT NULL  DEFAULT '0' , 
	PRIMARY KEY (`game_rating_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='latin1';

CREATE TABLE `game_raves`(
	`rave_id` int(10) unsigned NOT NULL  auto_increment , 
	`user_id` int(10) unsigned NOT NULL  DEFAULT '0' , 
	`game_id` int(10) unsigned NOT NULL  DEFAULT '0' , 
	`rave_date` datetime NOT NULL  DEFAULT '0000-00-00 00:00:00' , 
	PRIMARY KEY (`rave_id`) , 
	KEY `idx_uniqueRave`(`game_id`,`user_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='latin1';

CREATE TABLE `game_role_ids`(
	`game_role_id` smallint(6) NOT NULL  , 
	`game_role` varchar(50) COLLATE latin1_swedish_ci NULL  COMMENT 'descriptive name' , 
	PRIMARY KEY (`game_role_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='latin1' COMMENT='lookup table for asset_roles';

CREATE TABLE `game_roles`(
	`user_id` int(11) NOT NULL  COMMENT 'FK to users' , 
	`game_id` int(11) NOT NULL  COMMENT 'FK to games' , 
	`game_role_id` smallint(6) NOT NULL  COMMENT 'FK to game_roles' , 
	PRIMARY KEY (`user_id`,`game_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='latin1' COMMENT='track user permissions for a star except owners';

CREATE TABLE `game_servers` ( 
  `server_id` int(11) NOT NULL AUTO_INCREMENT,
  `server_name` varchar(100) NOT NULL COMMENT 'hostname of server, preconfigured, must match when pinging',
  `game_id` int(11) NOT NULL COMMENT 'FK to games',
  `visibility_id` INT(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_server_visibility, public, etc.',
  `server_started_date` datetime DEFAULT NULL COMMENT 'when last started',
  `ip_address` varchar(50) NOT NULL DEFAULT '0' COMMENT 'external IP used by clients',
  `port` int(11) NOT NULL DEFAULT '0' COMMENT 'port used by client',
  `server_status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_server_status, running, etc.',
  `number_of_players` int(11) DEFAULT NULL COMMENT 'current player count',
  `max_players` int(11) NOT NULL DEFAULT 0 COMMENT 'as configured on server',
  `last_ping_datetime` datetime DEFAULT NULL,
  `server_type_id` int(11)   NOT NULL DEFAULT '1',
  `modifiers_id` int(10) unsigned   NOT NULL,
  PRIMARY KEY  (`server_id`),
  KEY `INDEX_server_name` (`server_name`),
  KEY `fast_summary` (`game_id`,`server_status_id`,`last_ping_datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT = 'configure and track servers for a game';

CREATE TABLE game_patch_urls (
   patchURL_id int(10) unsigned   NOT NULL auto_increment,
   patch_id INT unsigned   NOT NULL DEFAULT '1' COMMENT 'simple id, passed in from patcher',
   game_id int(10) unsigned   NOT NULL DEFAULT '0',
	patch_url VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'semi-colon delimited urls',
  PRIMARY KEY (patchURL_id)
) ENGINE = InnoDB COMMENT = 'track patch urls for game' ROW_FORMAT = DEFAULT;

CREATE TABLE asset_roles (
    user_id INT(11) NOT NULL COMMENT 'FK to users',
    asset_id INT(11) NOT NULL COMMENT 'FK to assets',
    asset_role_id SMALLINT NOT NULL COMMENT 'FK to asset_roles',
    PRIMARY KEY (user_id, asset_id)
) ENGINE = InnoDB COMMENT = 'track user permissions for an asset except owners' ROW_FORMAT = DEFAULT;
 
CREATE TABLE asset_role_ids (
    asset_role_id SMALLINT NOT NULL,
    asset_role VARCHAR(50) COMMENT 'descriptive name',
    PRIMARY KEY (asset_role_id)
) ENGINE = InnoDB COMMENT = 'lookup table for asset_roles' ROW_FORMAT = DEFAULT;
 
CREATE TABLE `game_patcher_settings` (
  `game_id` int(11) NOT NULL default '0' COMMENT 'FK to games',
  `pane1_url` varchar(255) NOT NULL default '',
  `pane2_url` varchar(255) NOT NULL default '',
  `pane1_title` varchar(255) default NULL,
  `pane2_title` varchar(255) default NULL,
  `version_text` varchar(20) default NULL,
	`background_url` varchar(255)  NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='patcher customizations';

