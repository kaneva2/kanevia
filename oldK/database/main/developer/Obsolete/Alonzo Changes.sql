-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

OBSOLETE!!!


CREATE TABLE `developer`.`games` (
  `game_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `owner_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `game_rating_id` SMALLINT UNSIGNED NOT NULL DEFAULT 1,
  `game_status_id` SMALLINT UNSIGNED NOT NULL DEFAULT 1,
  `game_name` VARCHAR(100) NOT NULL DEFAULT '',
  `game_description` TEXT,
  `game_synopsis` VARCHAR(50),
  `game_creation_date` DATETIME NOT NULL DEFAULT 0,
  `game_access_id` VARCHAR(45) NOT NULL DEFAULT '1',
  `game_image_path` VARCHAR(255),
  `game_keywords` TEXT,
  `last_updated` DATETIME,
  `modifiers_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY(`game_id`),
  INDEX `idx_creationDate`(`game_creation_date`),
  INDEX `idx_owner`(`owner_id`),
  INDEX `idx_name`(`game_name`),
  INDEX `idx_rating`(`game_rating_id`),
  INDEX `idx_status`(`game_status_id`)
)
ENGINE = InnoDB;

CREATE TABLE `developer`.`games_to_categories` (
  `game_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `category_id` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY(`game_id`, `category_id`)
)
ENGINE = InnoDB;

CREATE TABLE `developer`.`game_categories` (
  `category_id` int(11) NOT NULL auto_increment,
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=542 DEFAULT CHARSET=latin1;

INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES
 (1,'Action & Adventure'),
 (2,'Aviation & Air Combat'),
 (3,'Business'),
 (4,'Casual Games'),
 (5,'Driving & Racing'),
 (6,'Education'),
 (7,'Fantasy'),
 (8,'Fighting'),
 (9,'Future'),
 (10,'Gambling'),
 (11,'Modern'),
 (12,'Puzzles'),
 (13,'Roleplaying'),
 (14,'Simulation'),
 (15,'Sports'),
 (16,'Strategy');
 
 CREATE TABLE `developer`.`game_raves` (
   `rave_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
   `user_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
   `game_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
   `rave_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
   PRIMARY KEY(`rave_id`),
   UNIQUE `idx_uniqueRave`(`game_id`, `user_id`)
 )
ENGINE = InnoDB;

CREATE TABLE `developer`.`game_views` (
  `view_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `game_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `view_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_address` INTEGER UNSIGNED,
  PRIMARY KEY(`view_id`),
  UNIQUE `idx_uniqueView`(`user_id`, `game_id`)
)
ENGINE = InnoDB;

CREATE TABLE `developer`.`game_stats` (
  `game_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `number_of_comments` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `number_of_raves` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `number_of_views` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP NOT NULL DEFAULT 0,
  PRIMARY KEY(`game_id`)
)
ENGINE = InnoDB;

CREATE TABLE `developer`.`game_ratings` (
  `game_rating_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `game_rating` VARCHAR(50) NOT NULL DEFAULT '',
  `description` VARCHAR(255),
  `minimum_age` SMALLINT UNSIGNED NOT NULL DEFAULT 14,
  `is_mature` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY(`game_rating_id`)
)
ENGINE = InnoDB;

INSERT INTO `developer`.`game_ratings` (`game_rating`,`description`, `minimum_age`, `is_mature`) VALUES
 ("General",'All Audiences (G)', 0, 0),
 ("Teen",'14 and Up (PG-14)', 14, 0),
 ("Adult",'18 and older (R)', 18, 1),
 ("Mature",'21 and older (NC-17)', 14, 1);
 
 CREATE TABLE `developer`.`audit_games` (
   `game_id` INTEGER UNSIGNED NOT NULL,
   `owner_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
   `game_rating_id` SMALLINT UNSIGNED NOT NULL DEFAULT 1,
   `game_status_id` SMALLINT UNSIGNED NOT NULL DEFAULT 1,
   `game_name` VARCHAR(100) NOT NULL DEFAULT '',
   `game_description` TEXT,
   `game_synopsis` VARCHAR(50),
   `game_creation_date` DATETIME NOT NULL DEFAULT 0,
   `game_access_id` VARCHAR(45) NOT NULL DEFAULT '1',
   `game_image_path` VARCHAR(255),
   `game_keywords` TEXT,
   `last_updated` DATETIME,
   `modifiers_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
   `date_modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
   `operation` VARCHAR(20) NOT NULL DEFAULT ''
 )
ENGINE = InnoDB;

DELIMITER //
CREATE TRIGGER developer.before_delete_games BEFORE DELETE on developer.games
FOR EACH ROW
BEGIN
    INSERT into developer.audit_games
    (
    	game_id, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis, game_creation_date, game_access_id,
    	game_image_path, game_keywords, last_updated, modifiers_id, date_modified, operation
    )
    VALUES
    (
    	
    	old.game_id, old.owner_id, old.game_rating_id, old.game_status_id, old.game_name, old.game_description, old.game_synopsis,
    	old.game_creation_date, old.game_access_id, old.game_image_path, old.game_keywords, old.last_updated,
      old.modifiers_id, Now(), 'delete'
    ) ;

	DELETE from developer.game_stats WHERE game_id = old.game_id;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER developer.before_update_games BEFORE update on developer.games
FOR EACH ROW
BEGIN
    INSERT into developer.audit_games
    (
    	game_id, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis, game_creation_date, game_access_id,
    	game_image_path, game_keywords, last_updated, modifiers_id, date_modified, operation
    )
    VALUES
    (
    	
    	old.game_id, old.owner_id, old.game_rating_id, old.game_status_id, old.game_name, old.game_description, old.game_synopsis,
    	old.game_creation_date, old.game_access_id, old.game_image_path, old.game_keywords, old.last_updated,
      old.modifiers_id, Now(), 'update'
    ) ;    
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER developer.after_insert_games AFTER insert on developer.games
FOR EACH ROW
BEGIN
    INSERT into developer.audit_games
    (
    	game_id, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis, game_creation_date, game_access_id,
    	game_image_path, game_keywords, last_updated, modifiers_id, date_modified, operation
    )
    VALUES
    (
    	
    	new.game_id, new.owner_id, new.game_rating_id, new.game_status_id, new.game_name, new.game_description, new.game_synopsis,
    	new.game_creation_date, new.game_access_id, new.game_image_path, new.game_keywords, new.last_updated,
        new.modifiers_id, Now(), 'insert'
    ) ;   
    
	INSERT into developer.game_stats
	(
		game_id, last_updated
	)
	VALUES
	(

		new.game_id, Now()
	) ;    

END //
DELIMITER ;



CREATE TABLE `developer`.`game_status` (
  `game_status_id` SMALLINT UNSIGNED NOT NULL,
  `game_status` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY(`game_status_id`)
)
ENGINE = InnoDB;

INSERT INTO `developer`.`game_status` (`game_status_id`,`game_status`) VALUES
 (1,'active'),
 (2,'inactive');
 
CREATE TABLE `developer`.`game_access` (
  `game_access_id` SMALLINT UNSIGNED NOT NULL,
  `game_access` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY(`game_access_id`)
)
ENGINE = InnoDB;

INSERT INTO `developer`.`game_access` (`game_access_id`,`game_access`) VALUES
 (1,'public'),
 (2,'private');
 
 CREATE TABLE `developer`.`game_server_history` (
   `server_history_id` int(11) NOT NULL auto_increment,
   `server_id` int(11) NOT NULL default '0',
   `player_count` int(11) NOT NULL default '0',
   `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
   `ip_address` varchar(20) default NULL,
   `port` INT(11),
   PRIMARY KEY  (`server_history_id`),
   KEY `FK_server_history_current_game_engines` (`server_id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=264402 DEFAULT CHARSET=latin1;
 
 CREATE TABLE `developer`.`game_server_status` (
   `server_status_id` int(11) NOT NULL default '0',
   `name` varchar(50) NOT NULL default '',
   `description` varchar(100) NOT NULL default '',
   PRIMARY KEY  (`server_status_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 
 INSERT INTO `developer`.`game_server_status` (`server_status_id`,`name`, `description`) VALUES
 (0,'stopped','The server has stopped'),
 (1,'running','The server is running'),
 (2,'stopping','The server is stopping'),
 (3,'starting','The server is starting'),  
 (4,'locked','The server has locked'),
 (5,'failed','The server has stopped');
 
 CREATE TABLE `developer`.`game_server_types` (
   `server_type_id` int(11) NOT NULL default '0',
   `name` varchar(50) NOT NULL default '',
   `description` varchar(50) NOT NULL default '',
   PRIMARY KEY  (`server_type_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 
  INSERT INTO `developer`.`game_server_types` (`server_type_id`,`name`, `description`) VALUES
  (1,'Game','A Game Server Engine'),
  (2,'AI','A Game AI Engine'),
  (3,'Unknown','An Unknown Server');

 
 CREATE TABLE `developer`.`game_license_subscriptions` (
   `license_subscription_id` SMALLINT NOT NULL auto_increment,
   `name` varchar(50) NOT NULL default '',
   `length_of_subscription` int(11) NOT NULL default '0',
   `amount` float NOT NULL default '0',
   `amount_kei_point_id` varchar(50) NOT NULL default '',
   `max_server_users` int(11) default NULL,
   `disk_quota` int(11) NOT NULL default '0',
   PRIMARY KEY  (`license_subscription_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

delete from `developer`.`game_license_subscriptions`;

insert into `developer`.`game_license_subscriptions`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values
(1,'Trial',1,0,'KPOINT',1,100000),
(2,'Designer',1,10,'KPOINT',100,100000),
(3,'Professional',1,20000,'KPOINT',2000,100000);

CREATE TABLE `developer`.`game_server_visibility` (
  `visibility_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY  (`visibility_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into `developer`.`game_server_visibility` (`visibility_id`,`name`,`description`) values
(1,'Public','Anyone can access this server'),
(2,'Private','Not in rotation');

CREATE TABLE `developer`.`game_licenses` (
  `game_license_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to games',
  `license_subscription_id` int(11) DEFAULT NULL COMMENT 'FK to game_license_subcription, trial/consumer/pro',
  `game_key` varchar(100) NOT NULL DEFAULT '' COMMENT 'used by all servers for game',
  `start_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `end_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `purchase_date` datetime DEFAULT NULL,
  `license_status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_license_status active, etc.',
  `modifiers_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY  (`game_license_id`),
  KEY `INDEX_game_id` (`game_id`),
  KEY `INDEX_game_key` (`game_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT = 'track extra data for a game';

CREATE TABLE `developer`.`audit_game_licenses` (
  `game_license_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to games',
  `license_subscription_id` int(11) DEFAULT NULL COMMENT 'FK to game_license_subcription, trial/consumer/pro',
  `game_key` varchar(100) NOT NULL DEFAULT '' COMMENT 'used by all servers for game',
  `start_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `end_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `purchase_date` datetime DEFAULT NULL,
  `license_status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_license_status active, etc.',
  `modifiers_id` INTEGER UNSIGNED NOT NULL,
  `date_modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `operation` VARCHAR(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB;

DELIMITER //
CREATE TRIGGER developer.before_delete_game_licenses BEFORE DELETE on developer.game_licenses
FOR EACH ROW
BEGIN
    INSERT into developer.audit_game_licenses
    (
    	game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, license_status_id, modifiers_id,
    	date_modified, operation
    )
    VALUES
    (
    	
    	old.game_license_id, old.game_id, old.license_subscription_id, old.game_key, old.start_date, old.end_date, old.purchase_date, old.license_status_id, 
    	old.modifiers_id, Now(), 'delete'
    ) ;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER developer.before_update_game_licenses BEFORE update on developer.game_licenses
FOR EACH ROW
BEGIN
    INSERT into developer.audit_game_licenses
    (
    	game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, license_status_id, modifiers_id,
    	date_modified, operation
    )
    VALUES
    (
    	
    	old.game_license_id, old.game_id, old.license_subscription_id, old.game_key, old.start_date, old.end_date, old.purchase_date, old.license_status_id, 
    	old.modifiers_id, Now(), 'update'
    ) ;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER developer.after_insert_game_licenses AFTER insert on developer.game_licenses
FOR EACH ROW
BEGIN
    INSERT into developer.audit_game_licenses
    (
    	game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, license_status_id, modifiers_id,
    	date_modified, operation
    )
    VALUES
    (
    	
    	new.game_license_id, new.game_id, new.license_subscription_id, new.game_key, new.start_date, new.end_date, new.purchase_date, new.license_status_id, 
    	new.modifiers_id, Now(), 'insert'
    ) ;
END //
DELIMITER ;

CREATE TABLE `developer`.`game_servers` (
  `server_id` int(11) NOT NULL AUTO_INCREMENT,
  `server_name` varchar(100) NOT NULL COMMENT 'hostname of server, preconfigured, must match when pinging',
  `game_id` int(11) NOT NULL COMMENT 'FK to games',
  `visibility_id` INT(11) NOT NULL DEFAULT 0 COMMENT 'FK to game_server_visibility, public, etc.',
  `server_started_date` datetime DEFAULT NULL COMMENT 'when last started',
  `ip_address` varchar(50) NOT NULL DEFAULT '' COMMENT 'external IP used by clients',
  `port` int(11) NOT NULL DEFAULT '0' COMMENT 'port used by client',
  `server_status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_server_status, running, etc.',
  `number_of_players` int(11) DEFAULT NULL COMMENT 'current player count',
  `max_players` int(11) NOT NULL DEFAULT 0 COMMENT 'as configured on server',
  `last_ping_datetime` datetime DEFAULT NULL,  
  `server_type_id` int(11) NOT NULL,
  `modifiers_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY  (`server_id`),
      KEY `INDEX_server_name` (`server_name`),
  KEY `fast_summary` (`game_id`,`server_status_id`,`last_ping_datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT = 'configure and track servers for a game';

CREATE TABLE `developer`.`audit_game_servers` (
  `server_id` int(11) NOT NULL,
  `server_name` varchar(100) NOT NULL COMMENT 'hostname of server, preconfigured, must match when pinging',
  `game_id` int(11) NOT NULL COMMENT 'FK to games',
  `visibility_id` INT(11) NOT NULL DEFAULT 0 COMMENT 'FK to game_server_visibility, public, etc.',
  `server_started_date` datetime DEFAULT NULL COMMENT 'when last started',
  `ip_address` varchar(50) NOT NULL DEFAULT '' COMMENT 'external IP used by clients',
  `port` int(11) NOT NULL DEFAULT '0' COMMENT 'port used by client',
  `server_status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_server_status, running, etc.',
  `number_of_players` int(11) DEFAULT NULL COMMENT 'current player count',
  `max_players` int(11) NOT NULL DEFAULT 0 COMMENT 'as configured on server',
  `last_ping_datetime` datetime DEFAULT NULL,  
  `server_type_id` int(11) NOT NULL,
  `modifiers_id` INTEGER UNSIGNED NOT NULL,
  `date_modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `operation` VARCHAR(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB; 

DELIMITER //
CREATE TRIGGER developer.before_delete_game_servers BEFORE DELETE on developer.game_servers
FOR EACH ROW
BEGIN
    INSERT into developer.audit_game_servers
    (
    	server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, number_of_players,
    	max_players, last_ping_datetime, server_type_id, modifiers_id, date_modified, operation
    )
    VALUES
    (
    	
    	old.server_id, old.server_name, old.game_id, old.visibility_id, old.server_started_date, old.ip_address, old.port, old.server_status_id, old.number_of_players,
    	old.max_players, old.last_ping_datetime, old.server_type_id, old.modifiers_id, Now(), 'delete'
    ) ;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER developer.before_update_game_servers BEFORE update on developer.game_servers
FOR EACH ROW
BEGIN
    INSERT into developer.audit_game_servers
    (
    	server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, number_of_players,
    	max_players, last_ping_datetime, server_type_id, modifiers_id, date_modified, operation
    )
    VALUES
    (
    	
    	old.server_id, old.server_name, old.game_id, old.visibility_id, old.server_started_date, old.ip_address, old.port, old.server_status_id, old.number_of_players,
    	old.max_players, old.last_ping_datetime, old.server_type_id, old.modifiers_id, Now(), 'update'
    ) ;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER developer.after_insert_game_servers AFTER insert on developer.game_servers
FOR EACH ROW
BEGIN
    INSERT into developer.audit_game_servers
    (
    	server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, number_of_players,
    	max_players, last_ping_datetime, server_type_id, modifiers_id, date_modified, operation
    )
    VALUES
    (
    	
    	new.server_id, new.server_name, new.game_id, new.visibility_id, new.server_started_date, new.ip_address, new.port, new.server_status_id, new.number_of_players,
    	new.max_players, new.last_ping_datetime, new.server_type_id, new.modifiers_id, Now(), 'insert'
    ) ;
END //
DELIMITER ;

CREATE TABLE `developer`.`game_patch_urls` (
  `patchURL_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `patch_id` INTEGER UNSIGNED NOT NULL DEFAULT 1,
  `game_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `patch_url` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY(`pitchURL_id`)
)
ENGINE = InnoDB;

CREATE TABLE developer.game_roles (
    user_id INT(11) NOT NULL COMMENT 'FK to users',
    game_id INT(11) NOT NULL COMMENT 'FK to games',
    game_role_id SMALLINT NOT NULL COMMENT 'FK to game_roles',
    PRIMARY KEY (user_id, game_id)
) ENGINE = InnoDB COMMENT = 'track user permissions for a star except owners' ROW_FORMAT = DEFAULT;

CREATE TABLE developer.game_role_ids (
    game_role_id SMALLINT NOT NULL,
    game_role VARCHAR(50) COMMENT 'descriptive name',
    PRIMARY KEY (game_role_id)
) ENGINE = InnoDB COMMENT = 'lookup table for asset_roles' ROW_FORMAT = DEFAULT;

INSERT INTO developer.game_role_ids (game_role_id, game_role)
VALUES (1, 'owner'), (2,'administrator'), (3,'IT'), (4,'CSR');


CREATE TABLE `developer`.`game_license_status` (
  `license_status_id` SMALLINT UNSIGNED NOT NULL,
  `license_status` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY(`license_status_id`)
)
ENGINE = InnoDB;

INSERT INTO `developer`.`game_license_status` (`license_status_id`,`license_status`) VALUES
 (1,'none'),
 (2,'validated'),
 (3,'suspended'),
 (4,'revoked');


CREATE TABLE `developer`.`prospective_star_developers` (
  `prospective_developer_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `contacts_firstname` VARCHAR(50) NOT NULL DEFAULT '',
  `contacts_lastname` VARCHAR(50) NOT NULL DEFAULT '',
  `contacts_email` VARCHAR(100) NOT NULL DEFAULT '',
  `contacts_phone` VARCHAR(20) NOT NULL DEFAULT '',
  `company_name` VARCHAR(70) NOT NULL DEFAULT '',
  `company_addressI` VARCHAR(80) NOT NULL DEFAULT '',
  `company_addressII` VARCHAR(80),
  `city` VARCHAR(50) NOT NULL DEFAULT '',
  `state_code` CHAR(2) NOT NULL DEFAULT 'GA',
  `province` VARCHAR(50) NOT NULL DEFAULT '',
  `country_id` CHAR(2) NOT NULL DEFAULT 'US',
  `zip_code` VARCHAR(13) NOT NULL DEFAULT '',
  `website_URL` VARCHAR(100),
  `annual_revenue` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `vworld_involvment_id` SMALLINT UNSIGNED NOT NULL DEFAULT 1,
  `project_name` VARCHAR(45) NOT NULL DEFAULT '',
  `project_description` VARCHAR(200),
  `project_startdate_id` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `budget` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `client_platform_id` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `project_status_id` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `current_team_size` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `projected_team_size` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `date_submitted` DATETIME NOT NULL DEFAULT 0, 
  PRIMARY KEY(`prospective_developer_id`)
)
ENGINE = InnoDB;

CREATE TABLE `developer`.`vworld_involvement` (
  `vworld_involvement_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `vworld_involvement` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY(`vworld_involvement_id`)
)
ENGINE = InnoDB;

INSERT INTO `developer`.`vworld_involvement` (`vworld_involvement_id`,`vworld_involvement`) VALUES
 (1,'Studio'),
 (2,'3rd-party Developer'),
 (3,'Educational Institution'),
 (4,'Government'),
 (5,'Corporation');


CREATE TABLE `developer`.`project_startdates` (
  `project_startdate_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `project_startdate` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY(`project_startdate_id`)
)
ENGINE = InnoDB;

INSERT INTO `developer`.`project_startdates` (`project_startdate_id`,`project_startdate` ) VALUES
 (1,'In Progress'),
 (2,'1 Month'),
 (3,'3 Month'),
 (4,'6 Months'),
 (5,'More than 6 Months'),
 (6,"Don't Know");

CREATE TABLE `developer`.`client_platforms` (
  `client_platform_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `client_platform` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY(`client_platform_id`)
)
ENGINE = InnoDB;

INSERT INTO `developer`.`client_platforms` (`client_platform_id` ,`client_platform`  ) VALUES
 (1,'PC Low Spec'),
 (2,'PC Current'),
 (3,'PC Next Gen'),
 (4,"Other");


CREATE TABLE `developer`.`project_status`  (
  `project_status_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `project_status` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY(`project_status_id`)
)
ENGINE = InnoDB;

INSERT INTO `developer`.`project_status` (`project_status_id` ,`project_status`  ) VALUES
 (1,'Planning'),
 (2,'In Development');