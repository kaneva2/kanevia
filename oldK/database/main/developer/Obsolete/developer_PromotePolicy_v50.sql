-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- add new developer table(s) 
\. developer_SchemaChanges_v50.sql


-- add developer version number
DROP PROCEDURE IF EXISTS developer_SchemaChanges_v50;
DELIMITER //
CREATE PROCEDURE developer_SchemaChanges_v50()
BEGIN
IF NOT EXISTS (SELECT 1 FROM schema_versions WHERE version = 50.0) THEN
	INSERT INTO `schema_versions` ( `version`, `description` ) 
		VALUES (50.0, 'Invitations/security upgrade (initial version)' );
END IF;

END //

DELIMITER ;

CALL developer_SchemaChanges_v50;
DROP PROCEDURE IF EXISTS developer_SchemaChanges_v50;
