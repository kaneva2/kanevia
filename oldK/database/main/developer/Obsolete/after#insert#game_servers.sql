-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

OBSOLETE!!!!

DROP TRIGGER IF EXISTS `after_insert_game_servers`;
DELIMITER //


CREATE TRIGGER `after_insert_game_servers`
   AFTER INSERT
   ON `game_servers`
   FOR EACH ROW
BEGIN
   INSERT INTO developer.audit_game_servers
          (server_id,
           server_name,
           game_id,
           visibility_id,
           server_started_date,
           ip_address,
           port,
           server_status_id,
           number_of_players,
           max_players,
           last_ping_datetime,
           server_type_id,
           modifiers_id,
           protocol_version,
           schema_version,
           server_engine_version,
           server_engine_admin_only,
           date_modified,
           operation)
   VALUES (
             new.server_id,
             new.server_name,
             new.game_id,
             new.visibility_id,
             new.server_started_date,
             new.ip_address,
             new.port,
             new.server_status_id,
             new.number_of_players,
             new.max_players,
             new.last_ping_datetime,
             new.server_type_id,
             new.modifiers_id,
             new.protocol_version,
             new.schema_version,
             new.server_engine_version,
             new.server_engine_admin_only,
             Now(),
             'insert'
          );
END //

DELIMITER ;