-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use developer;

CREATE TABLE `game_server_status` (
  `status_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into `game_server_status`(`status_id`,`name`,`description`) values (0,'stopped','The server has stopped');
insert into `game_server_status`(`status_id`,`name`,`description`) values (1,'running','The server is running');
insert into `game_server_status`(`status_id`,`name`,`description`) values (2,'stopping','The server is stopping');
insert into `game_server_status`(`status_id`,`name`,`description`) values (3,'starting','The server is starting');
insert into `game_server_status`(`status_id`,`name`,`description`) values (4,'locked','The server has locked');
insert into `game_server_status`(`status_id`,`name`,`description`) values (5,'failed','The server has failed');


CREATE TABLE `game_server_type` (
  `server_type_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY  (`server_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


insert into `game_server_type`(`server_type_id`,`name`,`description`) values (1,'Game','A Game Server Engine');
insert into `game_server_type`(`server_type_id`,`name`,`description`) values (2,'AI','A Game AI Engine');
insert into `game_server_type`(`server_type_id`,`name`,`description`) values (3,'Unknown','An Unknown server');

CREATE TABLE `game_license_subscription` (
  `license_subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `length_of_subscription` int(11) NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `amount_kei_point_id` varchar(50) NOT NULL DEFAULT '',
  `max_server_users` int(11) DEFAULT NULL,
  `disk_quota` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY  (`license_subscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into `game_license_subscription`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (1,'Trial',1,0,'KPOINT',1,100000);
insert into `game_license_subscription`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (2,'Designer',1,0,'KPOINT',100,100000);
insert into `game_license_subscription`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (3,'Professional',1,20000,'KPOINT',2000,100000);

CREATE TABLE `game_server_visibility` (
  `visibility_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY  (`visibility_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into `game_server_visibility` (`visibility_id`,`name`,`description`) values (1,'Public','Anyone can access this server');
insert into `game_server_visibility` (`visibility_id`,`name`,`description`) values (2,'Private','Not in rotation');

-- TODO: history/audit tables
CREATE TABLE `game_server_history` (
  `server_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `engine_id` int(11) NOT NULL DEFAULT '0',
  `player_count` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `game_key` varchar(100) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  PRIMARY KEY  (`server_history_id`),
  KEY `FK_engine_history_current_game_engines` (`engine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `game_license` (
  `game_license_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to assets', 
  `license_type` int(11) DEFAULT NULL COMMENT 'FK to game_license_subcription, trial/consumer/pro',
  `game_key` varchar(100) NOT NULL DEFAULT '' COMMENT 'used by all servers for game',
  `start_date` datetime DEFAULT NULL COMMENT 'for tracking trials', 
  `end_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `purchase_date` datetime DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_license_status active, etc.', 
  PRIMARY KEY  (`game_license_id`),
  KEY `INDEX_asset_id` (`asset_id`),
  KEY `INDEX_game_key` (`game_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT = 'track extra data for a game';

CREATE TABLE `game_servers` ( 
  `engine_id` int(11) NOT NULL AUTO_INCREMENT,
  `server_name` varchar(100) NOT NULL COMMENT 'hostname of server, preconfigured, must match when pinging',
  `game_asset_id` int(11) NOT NULL COMMENT 'FK to assets',
  `visibility_id` INT(11) NOT NULL DEFAULT 1 COMMENT 'FK to game_server_visibility, public, etc.',
  `engine_started_date` datetime DEFAULT NULL COMMENT 'when last started',
  `ip_address` varchar(50) NOT NULL DEFAULT '' COMMENT 'external IP used by clients',
  `port` int(11) NOT NULL DEFAULT '0' COMMENT 'port used by client',
  `server_status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_server_status, running, etc.',
  `number_of_players` int(11) DEFAULT NULL COMMENT 'current player count',
  `max_players` int(11) NOT NULL DEFAULT 0 COMMENT 'as configured on server',
  `last_ping_datetime` datetime DEFAULT NULL,
  PRIMARY KEY  (`engine_id`),
      KEY `INDEX_server_name` (`server_name`),
  KEY `fast_summary` (`game_asset_id`,`server_status_id`,`last_ping_datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT = 'configure and track servers for a game';


-- break out patch into separate tables
CREATE TABLE game_patch_urls (
   game_asset_id INT NOT NULL,
   patch_id INT NOT NULL COMMENT 'simple id, passed in from patcher',
   patch_url VARCHAR(255) NOT NULL COMMENT 'semi-colon delimited urls',
  PRIMARY KEY (game_asset_id, patch_id)
) ENGINE = InnoDB COMMENT = 'track patch urls for game' ROW_FORMAT = DEFAULT;

-- role tables
CREATE TABLE asset_roles (
    user_id INT(11) NOT NULL COMMENT 'FK to users',
    asset_id INT(11) NOT NULL COMMENT 'FK to assets',
    asset_role_id SMALLINT NOT NULL COMMENT 'FK to asset_roles',
    PRIMARY KEY (user_id, asset_id)
) ENGINE = InnoDB COMMENT = 'track user permissions for an asset except owners' ROW_FORMAT = DEFAULT;
 
CREATE TABLE asset_role_ids (
    asset_role_id SMALLINT NOT NULL,
    asset_role VARCHAR(50) COMMENT 'descriptive name',
    PRIMARY KEY (asset_role_id)
) ENGINE = InnoDB COMMENT = 'lookup table for asset_roles' ROW_FORMAT = DEFAULT;
 
INSERT INTO asset_role_ids (asset_role_id, asset_role)
VALUES (1, 'owner'), (2,'administrator'), (3,'IT'), (4,'CSR');

CREATE TABLE `game_patcher_settings` (
  `game_id` int(11) NOT NULL default '0' COMMENT 'FK to games',
  `pane1_url` varchar(255) NOT NULL default '',
  `pane2_url` varchar(255) NOT NULL default '',
  `pane1_title` varchar(255) default NULL,
  `pane2_title` varchar(255) default NULL,
  `version_text` varchar(20) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='patcher customizations';

