-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS SchemaChanges;

DELIMITER //

CREATE PROCEDURE SchemaChanges()
BEGIN

DROP TABLE IF EXISTS `developer`.`developer_emails`;
CREATE TABLE  `developer`.`developer_emails` (
  `email` varchar(100) NOT NULL DEFAULT '',
  `signup_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  UNIQUE KEY `uc_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


END//
DELIMITER ;

CALL SchemaChanges();

DROP PROCEDURE IF EXISTS SchemaChanges;
