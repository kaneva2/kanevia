-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

delimiter $$
--
-- CREATE TABLE IF NOT EXISTS  IF NOT EXISTS : schema_versions
--

CREATE TABLE IF NOT EXISTS  `schema_versions`
(
version INT NOT NULL COMMENT 'current schema version',
description VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'description of update',
date_applied TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
CONSTRAINT PRIMARY KEY (version)
)ENGINE = InnoDB COLLATE latin1_swedish_ci ROW_FORMAT = COMPACT;

IF NOT EXISTS (SELECT 1 FROM schema_versions WHERE version = 1) THEN
	INSERT INTO `schema_versions` ( `version`, `description` ) VALUES (1, 'Initial Schema' );
END IF;

CREATE TABLE IF NOT EXISTS `game_server_status` (
  `status_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

IF NOT EXISTS (SELECT 1 FROM game_server_status WHERE status_id = 0) THEN 
	insert into `game_server_status`(`status_id`,`name`,`description`) values (0,'stopped','The server has stopped');
END IF;
IF NOT EXISTS (SELECT 1 FROM game_server_status WHERE status_id = 1) THEN 
	insert into `game_server_status`(`status_id`,`name`,`description`) values (1,'running','The server is running');
END IF;
IF NOT EXISTS (SELECT 1 FROM game_server_status WHERE status_id = 2) THEN 
	insert into `game_server_status`(`status_id`,`name`,`description`) values (2,'stopping','The server is stopping');
END IF;
IF NOT EXISTS (SELECT 1 FROM game_server_status WHERE status_id = 3) THEN 
	insert into `game_server_status`(`status_id`,`name`,`description`) values (3,'starting','The server is starting');
END IF;
IF NOT EXISTS (SELECT 1 FROM game_server_status WHERE status_id = 4) THEN 
	insert into `game_server_status`(`status_id`,`name`,`description`) values (4,'locked','The server has locked');
END IF;
IF NOT EXISTS (SELECT 1 FROM game_server_status WHERE status_id = 5) THEN 
	insert into `game_server_status`(`status_id`,`name`,`description`) values (5,'failed','The server has failed');
END IF;


CREATE TABLE IF NOT EXISTS  `game_server_types` (
  `server_type_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY  (`server_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


IF NOT EXISTS (SELECT 1 FROM game_server_type WHERE server_type_id = 1) THEN 
	insert into `game_server_types`(`server_type_id`,`name`,`description`) values (1,'Game','A Game Server Engine');
END IF;
IF NOT EXISTS (SELECT 1 FROM game_server_type WHERE server_type_id = 2) THEN 
	insert into `game_server_types`(`server_type_id`,`name`,`description`) values (2,'AI','A Game AI Engine');
END;
IF NOT EXISTS (SELECT 1 FROM game_server_type WHERE server_type_id = 3) THEN 
	insert into `game_server_types`(`server_type_id`,`name`,`description`) values (3,'Unknown','An Unknown server');
END IF;


CREATE TABLE IF NOT EXISTS  `game_license_subscriptions` (
   `license_subscription_id` SMALLINT NOT NULL auto_increment,
   `name` varchar(50) NOT NULL default '',
   `length_of_subscription` int(11) NOT NULL default '0',
   `amount` float NOT NULL default '0',
   `amount_kei_point_id` varchar(50) NOT NULL default '',
   `max_server_users` int(11) default NULL,
   `disk_quota` int(11) NOT NULL default '0',
   PRIMARY KEY  (`license_subscription_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

IF NOT EXISTS (SELECT 1 FROM game_license_subscription WHERE license_subscription_id = 1) THEN 
	insert into `game_license_subscriptions`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (1,'Trial',1,0,'KPOINT',1,100000);
END IF;
IF NOT EXISTS (SELECT 1 FROM game_license_subscription WHERE license_subscription_id = 2) THEN 
	insert into `game_license_subscriptions`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (2,'Designer',1,0,'KPOINT',100,100000);
END;
IF NOT EXISTS (SELECT 1 FROM game_license_subscription WHERE license_subscription_id = 3) THEN 
	insert into `game_license_subscriptions`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (3,'Professional',1,20000,'KPOINT',2000,100000);
END IF;

CREATE TABLE IF NOT EXISTS `game_server_visibility` (
  `visibility_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY  (`visibility_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

IF NOT EXISTS (SELECT 1 FROM game_server_visibility WHERE visibility_id = 1) THEN 
	insert into `game_server_visibility` (`visibility_id`,`name`,`description`) values (1,'Public','Anyone can access this server');
END;
IF NOT EXISTS (SELECT 1 FROM game_server_visibility WHERE visibility_id = 2) THEN 
	insert into `game_server_visibility` (`visibility_id`,`name`,`description`) values (2,'Private','Not in rotation');
END;

-- TODO: history/audit tables
CREATE TABLE IF NOT EXISTS `game_server_history` (
  `server_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `engine_id` int(11) NOT NULL DEFAULT '0',
  `player_count` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `game_key` varchar(100) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  PRIMARY KEY  (`server_history_id`),
  KEY `FK_engine_history_current_game_engines` (`engine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `game_license` (
  `game_license_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to assets', 
  `license_type` int(11) DEFAULT NULL COMMENT 'FK to game_license_subcription, trial/consumer/pro',
  `game_key` varchar(100) NOT NULL DEFAULT '' COMMENT 'used by all servers for game',
  `start_date` datetime DEFAULT NULL COMMENT 'for tracking trials', 
  `end_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `purchase_date` datetime DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_license_status active, etc.', 
  PRIMARY KEY  (`game_license_id`),
  KEY `INDEX_asset_id` (`asset_id`),
  KEY `INDEX_game_key` (`game_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT = 'track extra data for a game';

-- break out patch into separate tables
CREATE TABLE IF NOT EXISTS game_patch_urls (
   game_asset_id INT NOT NULL,
   patch_id INT NOT NULL COMMENT 'simple id, passed in from patcher',
   patch_url VARCHAR(255) NOT NULL COMMENT 'semi-colon delimited urls',
  PRIMARY KEY (game_asset_id, patch_id)
) ENGINE = InnoDB COMMENT = 'track patch urls for game' ROW_FORMAT = DEFAULT;

-- role tables
CREATE TABLE IF NOT EXISTS asset_roles (
    user_id INT(11) NOT NULL COMMENT 'FK to users',
    asset_id INT(11) NOT NULL COMMENT 'FK to assets',
    asset_role_id SMALLINT NOT NULL COMMENT 'FK to asset_roles',
    PRIMARY KEY (user_id, asset_id)
) ENGINE = InnoDB COMMENT = 'track user permissions for an asset except owners' ROW_FORMAT = DEFAULT;
 
CREATE TABLE IF NOT EXISTS asset_role_ids (
    asset_role_id SMALLINT NOT NULL,
    asset_role VARCHAR(50) COMMENT 'descriptive name',
    PRIMARY KEY (asset_role_id)
) ENGINE = InnoDB COMMENT = 'lookup table for asset_roles' ROW_FORMAT = DEFAULT;

IF NOT EXISTS (SELECT 1 FROM asset_role_ids WHERE asset_role_id = 1) THEN  
	INSERT INTO asset_role_ids (asset_role_id, asset_role) VALUES (1, 'owner');
END IF;
IF NOT EXISTS (SELECT 1 FROM asset_role_ids WHERE asset_role_id = 2) THEN  
	INSERT INTO asset_role_ids (asset_role_id, asset_role) VALUES (2,'administrator');
END IF;
IF NOT EXISTS (SELECT 1 FROM asset_role_ids WHERE asset_role_id = 3) THEN  
	INSERT INTO asset_role_ids (asset_role_id, asset_role) VALUES (3,'IT');
END IF;
IF NOT EXISTS (SELECT 1 FROM asset_role_ids WHERE asset_role_id = 4) THEN  
	INSERT INTO asset_role_ids (asset_role_id, asset_role) VALUES (4,'CSR');
END IF;

CREATE TABLE IF NOT EXISTS `game_patcher_settings` (
  `game_id` int(11) NOT NULL default '0' COMMENT 'FK to games',
  `pane1_url` varchar(255) NOT NULL default '',
  `pane2_url` varchar(255) NOT NULL default '',
  `pane1_title` varchar(255) default NULL,
  `pane2_title` varchar(255) default NULL,
  `version_text` varchar(20) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='patcher customizations';

CREATE TABLE IF NOT EXISTS `developer`.`games` (
  `game_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `owner_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `game_rating_id` SMALLINT UNSIGNED NOT NULL DEFAULT 1,
  `game_status_id` SMALLINT UNSIGNED NOT NULL DEFAULT 1,
  `game_name` VARCHAR(100) NOT NULL DEFAULT '',
  `game_description` TEXT,
  `game_synopsis` VARCHAR(50),
  `game_creation_date` DATETIME NOT NULL DEFAULT 0,
  `game_access_id` VARCHAR(45) NOT NULL DEFAULT '1',
  `game_image_path` VARCHAR(255),
  `game_keywords` TEXT,
  `last_updated` DATETIME,
  `modifiers_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY(`game_id`),
  INDEX `idx_creationDate`(`game_creation_date`),
  INDEX `idx_owner`(`owner_id`),
  INDEX `idx_name`(`game_name`),
  INDEX `idx_rating`(`game_rating_id`),
  INDEX `idx_status`(`game_status_id`)
)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS`developer`.`games_to_categories` (
  `game_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `category_id` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY(`game_id`, `category_id`)
)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `developer`.`game_categories` (
  `category_id` int(11) NOT NULL auto_increment,
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=542 DEFAULT CHARSET=latin1;

 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 1) THEN 
	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (1,'Action & Adventure');
 END IF;
 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 2) THEN 
 	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (2,'Aviation & Air Combat');
 END IF;
 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 3) THEN 
 	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES 3,'Business');
 END IF;
 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 4) THEN 
 	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (4,'Casual Games');
 END IF;
 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 5) THEN 
 	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (5,'Driving & Racing');
 END IF;
 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 6) THEN 
 	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (6,'Education');
 END IF;
 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 7) THEN 
 	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (7,'Fantasy');
 END IF;
 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 8) THEN 
 	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (8,'Fighting');
 END IF;
 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 9) THEN 
	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (9,'Future');
 END IF;
 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 10) THEN 
	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (10,'Gambling');
 END IF;
 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 11) THEN 
	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (11,'Modern');
 END IF;
 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 12) THEN 
	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (12,'Puzzles');
 END IF;
 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 13) THEN 
	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (13,'Roleplaying');
 END IF;
 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 14) THEN 
	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (14,'Simulation');
END IF;
IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 15) THEN 
	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (15,'Sports');
 END;
 IF NOT EXISTS (SELECT 1 FROM game_categories WHERE category_id = 16) THEN 
	INSERT INTO `developer`.`game_categories` (`category_id`,`description`) VALUES (16,'Strategy');
 END IF;
 
 CREATE TABLE IF NOT EXISTS `developer`.`game_raves` (
   `rave_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
   `user_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
   `game_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
   `rave_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
   PRIMARY KEY(`rave_id`),
   UNIQUE `idx_uniqueRave`(`game_id`, `user_id`)
 )
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS`developer`.`game_views` (
  `view_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `game_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `view_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_address` INTEGER UNSIGNED,
  PRIMARY KEY(`view_id`),
  UNIQUE `idx_uniqueView`(`user_id`, `game_id`)
)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `developer`.`game_stats` (
  `game_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `number_of_comments` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `number_of_raves` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `number_of_views` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP NOT NULL DEFAULT 0,
  PRIMARY KEY(`game_id`)
)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `developer`.`game_ratings` (
  `game_rating_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `game_rating` VARCHAR(50) NOT NULL DEFAULT '',
  `description` VARCHAR(255),
  `minimum_age` SMALLINT UNSIGNED NOT NULL DEFAULT 14,
  `is_mature` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY(`game_rating_id`)
)
ENGINE = InnoDB;

IF NOT EXISTS (SELECT 1 FROM game_ratings WHERE game_rating = "General") THEN 
	INSERT INTO `developer`.`game_ratings` (`game_rating`,`description`, `minimum_age`, `is_mature`) VALUES ("General",'All Audiences (G)', 0, 0);
END IF;
IF NOT EXISTS (SELECT 1 FROM game_ratings WHERE game_rating = "Teen") THEN 
	INSERT INTO `developer`.`game_ratings` (`game_rating`,`description`, `minimum_age`, `is_mature`) VALUES ("Teen",'14 and Up (PG-14)', 14, 0);
END IF;
IF NOT EXISTS (SELECT 1 FROM game_ratings WHERE game_rating = "Adult") THEN 
	INSERT INTO `developer`.`game_ratings` (`game_rating`,`description`, `minimum_age`, `is_mature`) VALUES ("Adult",'18 and older (R)', 18, 1);
END IF;
IF NOT EXISTS (SELECT 1 FROM game_ratings WHERE game_rating = "Mature") THEN 
	INSERT INTO `developer`.`game_ratings` (`game_rating`,`description`, `minimum_age`, `is_mature`) VALUES ("Mature",'21 and older (NC-17)', 14, 1);
END IF;

 CREATE TABLE IF NOT EXISTS `developer`.`audit_games` (
   `game_id` INTEGER UNSIGNED NOT NULL,
   `owner_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
   `game_rating_id` SMALLINT UNSIGNED NOT NULL DEFAULT 1,
   `game_status_id` SMALLINT UNSIGNED NOT NULL DEFAULT 1,
   `game_name` VARCHAR(100) NOT NULL DEFAULT '',
   `game_description` TEXT,
   `game_synopsis` VARCHAR(50),
   `game_creation_date` DATETIME NOT NULL DEFAULT 0,
   `game_access_id` VARCHAR(45) NOT NULL DEFAULT '1',
   `game_image_path` VARCHAR(255),
   `game_keywords` TEXT,
   `last_updated` DATETIME,
   `modifiers_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
   `date_modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
   `operation` VARCHAR(20) NOT NULL DEFAULT ''
 )
ENGINE = InnoDB;

DROP TRIGGER IF EXISTS before_delete_games;
CREATE TRIGGER developer.before_delete_games BEFORE DELETE on developer.games
FOR EACH ROW
BEGIN
    INSERT into developer.audit_games
    (
    	game_id, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis, game_creation_date, game_access_id,
    	game_image_path, game_keywords, last_updated, modifiers_id, date_modified, operation
    )
    VALUES
    (
    	
    	old.game_id, old.owner_id, old.game_rating_id, old.game_status_id, old.game_name, old.game_description, old.game_synopsis,
    	old.game_creation_date, old.game_access_id, old.game_image_path, old.game_keywords, old.last_updated,
      old.modifiers_id, Now(), 'delete'
    ) ;

	DELETE from developer.game_stats WHERE game_id = old.game_id;
END ;


DROP TRIGGER IF EXISTS before_update_games;
CREATE TRIGGER developer.before_update_games BEFORE update on developer.games
FOR EACH ROW
BEGIN
    INSERT into developer.audit_games
    (
    	game_id, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis, game_creation_date, game_access_id,
    	game_image_path, game_keywords, last_updated, modifiers_id, date_modified, operation
    )
    VALUES
    (
    	
    	old.game_id, old.owner_id, old.game_rating_id, old.game_status_id, old.game_name, old.game_description, old.game_synopsis,
    	old.game_creation_date, old.game_access_id, old.game_image_path, old.game_keywords, old.last_updated,
      old.modifiers_id, Now(), 'update'
    ) ;    
END;

DROP TRIGGER IF EXISTS after_insert_games;
CREATE TRIGGER developer.after_insert_games AFTER insert on developer.games
FOR EACH ROW
BEGIN
    INSERT into developer.audit_games
    (
    	game_id, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis, game_creation_date, game_access_id,
    	game_image_path, game_keywords, last_updated, modifiers_id, date_modified, operation
    )
    VALUES
    (
    	
    	new.game_id, new.owner_id, new.game_rating_id, new.game_status_id, new.game_name, new.game_description, new.game_synopsis,
    	new.game_creation_date, new.game_access_id, new.game_image_path, new.game_keywords, new.last_updated,
        new.modifiers_id, Now(), 'insert'
    ) ;   
    
	INSERT into developer.game_stats
	(
		game_id, last_updated
	)
	VALUES
	(

		new.game_id, Now()
	) ;    

END;


CREATE TABLE IF NOT EXISTS `developer`.`game_status` (
  `game_status_id` SMALLINT UNSIGNED NOT NULL,
  `game_status` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY(`game_status_id`)
)
ENGINE = InnoDB;

IF NOT EXISTS (SELECT 1 FROM game_status WHERE game_status_id = 1) THEN 
	INSERT INTO `developer`.`game_status` (`game_status_id`,`game_status`) VALUES (1,'active');
 END IF;
IF NOT EXISTS (SELECT 1 FROM game_status WHERE game_status_id = 2) THEN 
	INSERT INTO `developer`.`game_status` (`game_status_id`,`game_status`) VALUES (2,'inactive');
 END IF;
 
CREATE TABLE IF NOT EXISTS`developer`.`game_access` (
  `game_access_id` SMALLINT UNSIGNED NOT NULL,
  `game_access` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY(`game_access_id`)
)
ENGINE = InnoDB;

IF NOT EXISTS (SELECT 1 FROM game_access WHERE game_access_id = 1) THEN 
	INSERT INTO `developer`.`game_access` (`game_access_id`,`game_access`) VALUES (1,'public');
END IF;
IF NOT EXISTS (SELECT 1 FROM game_access WHERE game_access_id = 1) THEN 
	INSERT INTO `developer`.`game_access` (`game_access_id`,`game_access`) VALUES (2,'private');
END IF;


CREATE TABLE IF NOT EXISTS  `developer`.`game_licenses` (
  `game_license_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to games',
  `license_subscription_id` int(11) DEFAULT NULL COMMENT 'FK to game_license_subcription, trial/consumer/pro',
  `game_key` varchar(100) NOT NULL DEFAULT '' COMMENT 'used by all servers for game',
  `start_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `end_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `purchase_date` datetime DEFAULT NULL,
  `license_status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_license_status active, etc.',
  `modifiers_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY  (`game_license_id`),
  KEY `INDEX_game_id` (`game_id`),
  KEY `INDEX_game_key` (`game_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT = 'track extra data for a game';

CREATE TABLE IF NOT EXISTS  `developer`.`audit_game_licenses` (
  `game_license_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to games',
  `license_subscription_id` int(11) DEFAULT NULL COMMENT 'FK to game_license_subcription, trial/consumer/pro',
  `game_key` varchar(100) NOT NULL DEFAULT '' COMMENT 'used by all servers for game',
  `start_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `end_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `purchase_date` datetime DEFAULT NULL,
  `license_status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_license_status active, etc.',
  `modifiers_id` INTEGER UNSIGNED NOT NULL,
  `date_modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `operation` VARCHAR(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB;


DROP TRIGGER IF EXISTS before_delete_game_licenses;
CREATE TRIGGER developer.before_delete_game_licenses BEFORE DELETE on developer.game_licenses
FOR EACH ROW
BEGIN
    INSERT into developer.audit_game_licenses
    (
    	game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, license_status_id, modifiers_id,
    	date_modified, operation
    )
    VALUES
    (
    	
    	old.game_license_id, old.game_id, old.license_subscription_id, old.game_key, old.start_date, old.end_date, old.purchase_date, old.license_status_id, 
    	old.modifiers_id, Now(), 'delete'
    ) ;
END;

DROP TRIGGER IF EXISTS before_update_game_licenses;
CREATE TRIGGER developer.before_update_game_licenses BEFORE update on developer.game_licenses
FOR EACH ROW
BEGIN
    INSERT into developer.audit_game_licenses
    (
    	game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, license_status_id, modifiers_id,
    	date_modified, operation
    )
    VALUES
    (
    	
    	old.game_license_id, old.game_id, old.license_subscription_id, old.game_key, old.start_date, old.end_date, old.purchase_date, old.license_status_id, 
    	old.modifiers_id, Now(), 'update'
    ) ;
END;

DROP TRIGGER IF EXISTS after_insert_game_licenses;
CREATE TRIGGER developer.after_insert_game_licenses AFTER insert on developer.game_licenses
FOR EACH ROW
BEGIN
    INSERT into developer.audit_game_licenses
    (
    	game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, license_status_id, modifiers_id,
    	date_modified, operation
    )
    VALUES
    (
    	
    	new.game_license_id, new.game_id, new.license_subscription_id, new.game_key, new.start_date, new.end_date, new.purchase_date, new.license_status_id, 
    	new.modifiers_id, Now(), 'insert'
    ) ;
END;

CREATE TABLE IF NOT EXISTS  `developer`.`game_servers` (
  `server_id` int(11) NOT NULL AUTO_INCREMENT,
  `server_name` varchar(100) NOT NULL COMMENT 'hostname of server, preconfigured, must match when pinging',
  `game_id` int(11) NOT NULL COMMENT 'FK to games',
  `visibility_id` INT(11) NOT NULL DEFAULT 0 COMMENT 'FK to game_server_visibility, public, etc.',
  `server_started_date` datetime DEFAULT NULL COMMENT 'when last started',
  `ip_address` varchar(50) NOT NULL DEFAULT '' COMMENT 'external IP used by clients',
  `port` int(11) NOT NULL DEFAULT '0' COMMENT 'port used by client',
  `server_status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_server_status, running, etc.',
  `number_of_players` int(11) DEFAULT NULL COMMENT 'current player count',
  `max_players` int(11) NOT NULL DEFAULT 0 COMMENT 'as configured on server',
  `last_ping_datetime` datetime DEFAULT NULL,  
  `server_type_id` int(11) NOT NULL,
  `modifiers_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY  (`server_id`),
      KEY `INDEX_server_name` (`server_name`),
  KEY `fast_summary` (`game_id`,`server_status_id`,`last_ping_datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT = 'configure and track servers for a game';

CREATE TABLE IF NOT EXISTS  `developer`.`audit_game_servers` (
  `server_id` int(11) NOT NULL,
  `server_name` varchar(100) NOT NULL COMMENT 'hostname of server, preconfigured, must match when pinging',
  `game_id` int(11) NOT NULL COMMENT 'FK to games',
  `visibility_id` INT(11) NOT NULL DEFAULT 0 COMMENT 'FK to game_server_visibility, public, etc.',
  `server_started_date` datetime DEFAULT NULL COMMENT 'when last started',
  `ip_address` varchar(50) NOT NULL DEFAULT '' COMMENT 'external IP used by clients',
  `port` int(11) NOT NULL DEFAULT '0' COMMENT 'port used by client',
  `server_status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_server_status, running, etc.',
  `number_of_players` int(11) DEFAULT NULL COMMENT 'current player count',
  `max_players` int(11) NOT NULL DEFAULT 0 COMMENT 'as configured on server',
  `last_ping_datetime` datetime DEFAULT NULL,  
  `server_type_id` int(11) NOT NULL,
  `modifiers_id` INTEGER UNSIGNED NOT NULL,
  `date_modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `operation` VARCHAR(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB; 


CREATE TRIGGER developer.before_delete_game_servers BEFORE DELETE on developer.game_servers
FOR EACH ROW
BEGIN
    INSERT into developer.audit_game_servers
    (
    	server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, number_of_players,
    	max_players, last_ping_datetime, server_type_id, modifiers_id, date_modified, operation
    )
    VALUES
    (
    	
    	old.server_id, old.server_name, old.game_id, old.visibility_id, old.server_started_date, old.ip_address, old.port, old.server_status_id, old.number_of_players,
    	old.max_players, old.last_ping_datetime, old.server_type_id, old.modifiers_id, Now(), 'delete'
    ) ;
END //
DELIMITER ;


CREATE TRIGGER developer.before_update_game_servers BEFORE update on developer.game_servers
FOR EACH ROW
BEGIN
    INSERT into developer.audit_game_servers
    (
    	server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, number_of_players,
    	max_players, last_ping_datetime, server_type_id, modifiers_id, date_modified, operation
    )
    VALUES
    (
    	
    	old.server_id, old.server_name, old.game_id, old.visibility_id, old.server_started_date, old.ip_address, old.port, old.server_status_id, old.number_of_players,
    	old.max_players, old.last_ping_datetime, old.server_type_id, old.modifiers_id, Now(), 'update'
    ) ;
END //
DELIMITER ;


CREATE TRIGGER developer.after_insert_game_servers AFTER insert on developer.game_servers
FOR EACH ROW
BEGIN
    INSERT into developer.audit_game_servers
    (
    	server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, number_of_players,
    	max_players, last_ping_datetime, server_type_id, modifiers_id, date_modified, operation
    )
    VALUES
    (
    	
    	new.server_id, new.server_name, new.game_id, new.visibility_id, new.server_started_date, new.ip_address, new.port, new.server_status_id, new.number_of_players,
    	new.max_players, new.last_ping_datetime, new.server_type_id, new.modifiers_id, Now(), 'insert'
    ) ;
END //
DELIMITER ;


CREATE TABLE IF NOT EXISTS  developer.game_roles (
    user_id INT(11) NOT NULL COMMENT 'FK to users',
    game_id INT(11) NOT NULL COMMENT 'FK to games',
    game_role_id SMALLINT NOT NULL COMMENT 'FK to game_roles',
    PRIMARY KEY (user_id, game_id)
) ENGINE = InnoDB COMMENT = 'track user permissions for a star except owners' ROW_FORMAT = DEFAULT;

CREATE TABLE IF NOT EXISTS  developer.game_role_ids (
    game_role_id SMALLINT NOT NULL,
    game_role VARCHAR(50) COMMENT 'descriptive name',
    PRIMARY KEY (game_role_id)
) ENGINE = InnoDB COMMENT = 'lookup table for asset_roles' ROW_FORMAT = DEFAULT;

IF NOT EXISTS (SELECT 1 FROM game_role_ids WHERE game_role_id = 1) THEN 
	INSERT INTO developer.game_role_ids (game_role_id, game_role) VALUES (1, 'owner');
END IF;
IF NOT EXISTS (SELECT 1 FROM game_role_ids WHERE game_role_id = 2) THEN 
	INSERT INTO developer.game_role_ids (game_role_id, game_role) VALUES (2,'administrator');
END IF;
IF NOT EXISTS (SELECT 1 FROM game_role_ids WHERE game_role_id = 3) THEN 
	INSERT INTO developer.game_role_ids (game_role_id, game_role) VALUES (3,'IT');
END IF;
IF NOT EXISTS (SELECT 1 FROM game_role_ids WHERE game_role_id = 4) THEN 
	INSERT INTO developer.game_role_ids (game_role_id, game_role) VALUES (4,'CSR');
END IF;

CREATE TABLE IF NOT EXISTS  `developer`.`game_license_status` (
  `license_status_id` SMALLINT UNSIGNED NOT NULL,
  `license_status` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY(`license_status_id`)
)
ENGINE = InnoDB;

IF NOT EXISTS (SELECT 1 FROM game_license_status WHERE license_status_id = 1) THEN 
	INSERT INTO `developer`.`game_license_status` (`license_status_id`,`license_status`) VALUES (1,'none');
END IF;
IF NOT EXISTS (SELECT 1 FROM game_license_status WHERE license_status_id = 1) THEN 
	INSERT INTO `developer`.`game_license_status` (`license_status_id`,`license_status`) VALUES (2,'validated');
END IF;
IF NOT EXISTS (SELECT 1 FROM game_license_status WHERE license_status_id = 1) THEN 
	INSERT INTO `developer`.`game_license_status` (`license_status_id`,`license_status`) VALUES (3,'suspended');
END IF;
IF NOT EXISTS (SELECT 1 FROM game_license_status WHERE license_status_id = 1) THEN 
	INSERT INTO `developer`.`game_license_status` (`license_status_id`,`license_status`) VALUES (4,'revoked');
END IF;

CREATE TABLE IF NOT EXISTS  `developer`.`prospective_star_developers` (
  `prospective_developer_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `contacts_firstname` VARCHAR(50) NOT NULL DEFAULT '',
  `contacts_lastname` VARCHAR(50) NOT NULL DEFAULT '',
  `contacts_email` VARCHAR(100) NOT NULL DEFAULT '',
  `contacts_phone` VARCHAR(20) NOT NULL DEFAULT '',
  `company_name` VARCHAR(70) NOT NULL DEFAULT '',
  `company_addressI` VARCHAR(80) NOT NULL DEFAULT '',
  `company_addressII` VARCHAR(80),
  `city` VARCHAR(50) NOT NULL DEFAULT '',
  `state_code` CHAR(2) NOT NULL DEFAULT 'GA',
  `province` VARCHAR(50) NOT NULL DEFAULT '',
  `country_id` CHAR(2) NOT NULL DEFAULT 'US',
  `zip_code` VARCHAR(13) NOT NULL DEFAULT '',
  `website_URL` VARCHAR(100),
  `annual_revenue` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `vworld_involvment_id` SMALLINT UNSIGNED NOT NULL DEFAULT 1,
  `project_name` VARCHAR(45) NOT NULL DEFAULT '',
  `project_description` VARCHAR(200),
  `project_startdate_id` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `budget` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `client_platform_id` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `project_status_id` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `current_team_size` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `projected_team_size` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `date_submitted` DATETIME NOT NULL DEFAULT 0, 
  PRIMARY KEY(`prospective_developer_id`)
)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS  `developer`.`vworld_involvement` (
  `vworld_involvement_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `vworld_involvement` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY(`vworld_involvement_id`)
)
ENGINE = InnoDB;

IF NOT EXISTS (SELECT 1 FROM vworld_involvement WHERE vworld_involvement_id = 1) THEN 
	INSERT INTO `developer`.`vworld_involvement` (`vworld_involvement_id`,`vworld_involvement`) VALUES (1,'Studio');
END IF;
IF NOT EXISTS (SELECT 1 FROM vworld_involvement WHERE vworld_involvement_id = 1) THEN 
	INSERT INTO `developer`.`vworld_involvement` (`vworld_involvement_id`,`vworld_involvement`) VALUES (2,'3rd-party Developer');
END IF;
IF NOT EXISTS (SELECT 1 FROM vworld_involvement WHERE vworld_involvement_id = 1) THEN 
	INSERT INTO `developer`.`vworld_involvement` (`vworld_involvement_id`,`vworld_involvement`) VALUES (3,'Educational Institution');
END IF;
IF NOT EXISTS (SELECT 1 FROM vworld_involvement WHERE vworld_involvement_id = 1) THEN 
	INSERT INTO `developer`.`vworld_involvement` (`vworld_involvement_id`,`vworld_involvement`) VALUES (4,'Government');
END IF;
IF NOT EXISTS (SELECT 1 FROM vworld_involvement WHERE vworld_involvement_id = 1) THEN 
	INSERT INTO `developer`.`vworld_involvement` (`vworld_involvement_id`,`vworld_involvement`) VALUES (5,'Corporation');
END IF;

CREATE TABLE IF NOT EXISTS  `developer`.`project_startdates` (
  `project_startdate_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `project_startdate` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY(`project_startdate_id`)
)
ENGINE = InnoDB;

IF NOT EXISTS (SELECT 1 FROM project_startdates WHERE project_startdate_id = 1) THEN 
	INSERT INTO `developer`.`project_startdates` (`project_startdate_id`,`project_startdate` ) VALUES (1,'In Progress');
END IF;
IF NOT EXISTS (SELECT 1 FROM project_startdates WHERE project_startdate_id = 2) THEN 
	INSERT INTO `developer`.`project_startdates` (`project_startdate_id`,`project_startdate` ) VALUES (2,'1 Month');
END IF;
IF NOT EXISTS (SELECT 1 FROM project_startdates WHERE project_startdate_id = 3) THEN 
	INSERT INTO `developer`.`project_startdates` (`project_startdate_id`,`project_startdate` ) VALUES (3,'3 Month');
END IF;
IF NOT EXISTS (SELECT 1 FROM project_startdates WHERE project_startdate_id = 4) THEN 
	INSERT INTO `developer`.`project_startdates` (`project_startdate_id`,`project_startdate` ) VALUES (4,'6 Months');
END IF;
IF NOT EXISTS (SELECT 1 FROM project_startdates WHERE project_startdate_id = 5) THEN 
	INSERT INTO `developer`.`project_startdates` (`project_startdate_id`,`project_startdate` ) VALUES (5,'More than 6 Months');
END IF;
IF NOT EXISTS (SELECT 1 FROM project_startdates WHERE project_startdate_id = 6) THEN 
	INSERT INTO `developer`.`project_startdates` (`project_startdate_id`,`project_startdate` ) VALUES (6,"Don't Know");
END IF;

CREATE TABLE IF NOT EXISTS  `developer`.`client_platforms` (
  `client_platform_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `client_platform` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY(`client_platform_id`)
)
ENGINE = InnoDB;

IF NOT EXISTS (SELECT 1 FROM client_platforms WHERE client_platform_id = 1) THEN 
	INSERT INTO `developer`.`client_platforms` (`client_platform_id` ,`client_platform`  ) VALUES (1,'PC Low Spec');
END IF;
IF NOT EXISTS (SELECT 1 FROM client_platforms WHERE client_platform_id = 2) THEN 
	INSERT INTO `developer`.`client_platforms` (`client_platform_id` ,`client_platform`  ) VALUES (2,'PC Current');
END IF;
IF NOT EXISTS (SELECT 1 FROM client_platforms WHERE client_platform_id = 3) THEN 
	INSERT INTO `developer`.`client_platforms` (`client_platform_id` ,`client_platform`  ) VALUES (3,'PC Next Gen');
END IF;
IF NOT EXISTS (SELECT 1 FROM client_platforms WHERE client_platform_id = 4) THEN 
	INSERT INTO `developer`.`client_platforms` (`client_platform_id` ,`client_platform`  ) VALUES (4,"Other");
END IF;

CREATE TABLE IF NOT EXISTS  `developer`.`project_status`  (
  `project_status_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `project_status` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY(`project_status_id`)
)
ENGINE = InnoDB;

IF NOT EXISTS (SELECT 1 FROM project_status WHERE project_status_id = 1) THEN 
	INSERT INTO `developer`.`project_status` (`project_status_id` ,`project_status`  ) VALUES (1,'Planning');
END IF;
IF NOT EXISTS (SELECT 1 FROM project_status WHERE project_status_id = 2) THEN 
	INSERT INTO `developer`.`project_status` (`project_status_id` ,`project_status`  ) VALUES (2,'In Development');
END IF;

CREATE TABLE IF NOT EXISTS  `developer`.`invitations` (
  `invitations_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `validation_key` VARCHAR(50) NOT NULL DEFAULT '',
  `email` VARCHAR(100) NOT NULL DEFAULT '',
  `company_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `invitation_date` DATETIME NOT NULL DEFAULT 0,
  `message` VARCHAR(300) NOT NULL DEFAULT '',
  `name` VARCHAR(100) NOT NULL DEFAULT '',
  `role_id` INTEGER UNSIGNED NOT NULL DEFAULT 0 
  PRIMARY KEY(`invitations_id`)
)
ENGINE = InnoDB;
