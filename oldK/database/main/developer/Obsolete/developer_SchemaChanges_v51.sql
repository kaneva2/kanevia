-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

﻿use developer;

DROP PROCEDURE IF EXISTS developer_SchemaChanges_v51;
DELIMITER //
CREATE PROCEDURE developer_SchemaChanges_v51()
BEGIN
-- changes for STAR Open Beta
IF NOT EXISTS (select 1 from information_schema.columns
		where table_schema = 'developer' and table_name = 'game_servers' 
		and column_name = 'protocol_version' LIMIT 1) THEN
	ALTER TABLE developer.game_servers
		ADD protocol_version INT(11) NOT NULL DEFAULT '0' COMMENT 'client-server protocol reported by server' AFTER server_type_id;
END IF;

IF NOT EXISTS (select 1 from information_schema.columns
		where table_schema = 'developer' and table_name = 'game_servers' 
		and column_name = 'schema_version' LIMIT 1) THEN
	ALTER TABLE developer.game_servers
 		ADD schema_version INT(11) NOT NULL DEFAULT '0' COMMENT 'server\'s schema version' AFTER protocol_version;
END IF;
		
IF NOT EXISTS (select 1 from information_schema.columns
		where table_schema = 'developer' and table_name = 'game_servers' 
		and column_name = 'server_engine_version' LIMIT 1) THEN
	ALTER TABLE developer.game_servers
		ADD server_engine_version VARCHAR(30) NOT NULL DEFAULT '' COMMENT 'file version of serverengine.dll' AFTER schema_version;
END IF;
		
IF NOT EXISTS (select 1 from information_schema.columns
		where table_schema = 'developer' and table_name = 'game_servers' 
		and column_name = 'server_engine_admin_only' LIMIT 1) THEN
	ALTER TABLE developer.game_servers
		ADD server_engine_admin_only ENUM('T','F') NOT NULL DEFAULT 'F' COMMENT 'is server is admin mode' AFTER server_engine_version;
END IF;

IF NOT EXISTS (select 1 from information_schema.columns
		where table_schema = 'developer' and table_name = 'audit_game_servers' 
		and column_name = 'protocol_version' LIMIT 1) THEN
	ALTER TABLE developer.audit_game_servers
 		ADD protocol_version INT(11) NOT NULL DEFAULT '0' COMMENT 'client-server protocol reported by server' AFTER server_type_id;
END IF;		
		
IF NOT EXISTS (select 1 from information_schema.columns
		where table_schema = 'developer' and table_name = 'audit_game_servers' 
		and column_name = 'schema_version' LIMIT 1) THEN
	ALTER TABLE developer.audit_game_servers
		ADD schema_version INT(11) NOT NULL DEFAULT '0' COMMENT 'server\'s schema version' AFTER protocol_version;
END IF;		

IF NOT EXISTS (select 1 from information_schema.columns
		where table_schema = 'developer' and table_name = 'audit_game_servers' 
		and column_name = 'server_engine_version' LIMIT 1) THEN
	ALTER TABLE developer.audit_game_servers
		ADD server_engine_version VARCHAR(30) NOT NULL DEFAULT '' COMMENT 'file version of serverengine.dll' AFTER schema_version;
END IF;

IF NOT EXISTS (select 1 from information_schema.columns
		where table_schema = 'developer' and table_name = 'audit_game_servers' 
		and column_name = 'server_engine_admin_only' LIMIT 1) THEN
	ALTER TABLE developer.audit_game_servers
		ADD server_engine_admin_only ENUM('T','F') NOT NULL DEFAULT 'F' COMMENT 'is server is admin mode' AFTER server_engine_version;
END IF;

-- history only updated once an hour so skip this for now
-- ALTER TABLE developer.game_server_history
--  ADD visibility_id INT AFTER port;

IF NOT EXISTS (select 1 from information_schema.columns
		where table_schema = 'developer' and table_name = 'game_licenses' 
		and column_name = 'max_game_users' LIMIT 1) THEN
	ALTER TABLE developer.game_licenses
		ADD max_game_users INT(11) NOT NULL DEFAULT '2000' COMMENT 'max for entire game, across servers' AFTER license_status_id;
END IF;

IF NOT EXISTS (select 1 from information_schema.columns
		where table_schema = 'developer' and table_name = 'audit_game_licenses' 
		and column_name = 'max_game_users' LIMIT 1) THEN
	ALTER TABLE developer.audit_game_licenses
		ADD max_game_users INT(11) NOT NULL DEFAULT '2000' COMMENT 'max for entire game, across servers' AFTER license_status_id;
END IF;

-- boot strap the values in the table
UPDATE developer.game_licenses gl, developer.game_license_subscriptions gls 
  SET gl.max_game_users = gls.max_server_users
   WHERE gl.license_subscription_id = gls.license_subscription_id;

CREATE TABLE IF NOT EXISTS developer.game_login_log (
   game_login_id INT AUTO_INCREMENT NOT NULL COMMENT 'PK',
   user_id INT(11) NOT NULL,
   game_id INT(11) NOT NULL,
   logon_time DATETIME NOT NULL COMMENT 'time of login',
  PRIMARY KEY (game_login_id)
) ENGINE = MyISAM COMMENT = 'track logins to all games (not just WOK)' ROW_FORMAT = DEFAULT;

END //

DELIMITER ;

CALL developer_SchemaChanges_v51;
DROP PROCEDURE IF EXISTS developer_SchemaChanges_v51;

