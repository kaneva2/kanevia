-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS after_insert_games;
DELIMITER ;;
CREATE TRIGGER `after_insert_games` AFTER INSERT ON `games` 
	FOR EACH ROW 
	BEGIN
   INSERT into developer.audit_games
    (	game_id, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis, game_creation_date, game_access_id,
    	game_image_path, game_keywords, last_updated, modifiers_id, date_modified, operation )
    VALUES
    (	new.game_id, new.owner_id, new.game_rating_id, new.game_status_id, new.game_name, new.game_description, new.game_synopsis,
    	new.game_creation_date, new.game_access_id, new.game_image_path, new.game_keywords, new.last_updated,
      new.modifiers_id, Now(), 'insert' ) ;   

	INSERT into developer.game_stats
	(	game_id, last_updated	)
	VALUES
	(	new.game_id, Now() ) ;

END ;;
DELIMITER ;
