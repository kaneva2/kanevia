-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

OBSOLETE!!!

DROP TRIGGER IF EXISTS `before_delete_game_servers`;
DELIMITER //

CREATE TRIGGER `before_delete_game_servers`
   BEFORE DELETE
   ON `game_servers`
   FOR EACH ROW
BEGIN
   INSERT INTO developer.audit_game_servers
          (server_id,
           server_name,
           game_id,
           visibility_id,
           server_started_date,
           ip_address,
           port,
           server_status_id,
           number_of_players,
           max_players,
           last_ping_datetime,
           server_type_id,
           modifiers_id,
           protocol_version,
           schema_version,
           server_engine_version,
           server_engine_admin_only,
           date_modified,
           operation)
   VALUES (
             old.server_id,
             old.server_name,
             old.game_id,
             old.visibility_id,
             old.server_started_date,
             old.ip_address,
             old.port,
             old.server_status_id,
             old.number_of_players,
             old.max_players,
             old.last_ping_datetime,
             old.server_type_id,
             old.modifiers_id,
             old.protocol_version,
             old.schema_version,
             old.server_engine_version,
             old.server_engine_admin_only,
             Now(),
             'delete'
          );
END 
//

DELIMITER ;