-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS check_game_auth;

DELIMITER //
-- will return the following possible values that map to the C# return codes
-- SUCCESS 1
-- NO_GAME_ACCESS 4 (old rc, not member of community, used if no servers) 
-- GAME_FULL 9
-- NOT_AUTHORIZED 10 (mature, etc.)
-- 
CREATE FUNCTION check_game_auth( __user_id INT(11), __game_id INT(11) ) 
									RETURNS TINYINT(2) READS SQL DATA
BEGIN
	DECLARE __game_pop INT(11) DEFAULT NULL;
	DECLARE __game_pop_limit INT(11) DEFAULT 2000;
	DECLARE __game_role SMALLINT(6) DEFAULT NULL;
	
	-- TODO: check to see if game is mature, or access pass, etc.
	-- for now can't set it on games
	-- IF .....
	-- 	return 10; -- NOT_AUTHORIZED
	-- END IF;

	-- check overall population of the game
	SELECT sum(g.number_of_players) INTO __game_pop
	  FROM developer.game_servers g
	 WHERE game_id = __game_id 
	   AND server_status_id = 1;  -- include private or public, only running servers
	   
	IF __game_pop IS NOT NULL THEN 
	
		SELECT max_game_users INTO __game_pop_limit
			FROM developer.game_licenses 
		WHERE game_id = __game_id;
			
		IF __game_pop <= __game_pop_limit THEN
			INSERT INTO developer.game_login_log (user_id, game_id, logon_time)
			 VALUES ( __user_id, __game_id, NOW() );
			return 1; -- SUCCESS
		ELSE
			-- check role to see if can override full game
			-- if you are owner or moderator for game you can get in
			IF get_game_priv( __game_id, __user_id) NOT in (1,2) THEN 
				return 9; -- GAME_FULL
			ELSE
				return 1; -- SUCCESS
			END IF;
		END IF;
	ELSE 
		return 4; -- NO_GAME_ACCESS (no servers running)
	END IF;
END;
//
DELIMITER ;
											
