-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use developer;

CREATE TABLE IF NOT EXISTS  `developer`.`invitations` (
  `invitations_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'auto number id',
  `validation_key` VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'unique key used to retrieve id',
  `email` VARCHAR(100) NOT NULL DEFAULT '' COMMENT 'email of the invited user',
  `company_id` INTEGER UNSIGNED NOT NULL DEFAULT 0 COMMENT 'id of the inviting company ',
  `invitation_date` DATETIME NOT NULL DEFAULT 0 COMMENT 'date of invite',
  `message` VARCHAR(300) NOT NULL DEFAULT '' COMMENT 'message to invitee',
  `name` VARCHAR(100) NOT NULL DEFAULT '' COMMENT 'display name of invitee',
  `role_id` INTEGER UNSIGNED NOT NULL DEFAULT 0 COMMENT 'preassigned role id of invitee',
  PRIMARY KEY(`invitations_id`)
)
ENGINE = InnoDB 
COMMENT = 'stores invitation to be a developer';

CREATE TABLE IF NOT EXISTS `schema_versions`

(
	version NUMERIC(11,2) NOT NULL COMMENT 'current schema version',
	description VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'description of update',
	date_applied TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
	CONSTRAINT PRIMARY KEY (version)
)
	ENGINE = InnoDB
	COLLATE latin1_swedish_ci
	ROW_FORMAT = COMPACT;