-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- changes for STAR Open Beta
ALTER TABLE developer.game_servers
 ADD protocol_version INT(11) NOT NULL DEFAULT '0' COMMENT 'client-server protocol reported by server' AFTER server_type_id,
 ADD schema_version INT(11) NOT NULL DEFAULT '0' COMMENT 'server\'s schema version' AFTER protocol_version,
 ADD server_engine_version VARCHAR(30) NOT NULL DEFAULT '' COMMENT 'file version of serverengine.dll' AFTER schema_version,
 ADD server_engine_admin_only ENUM('T','F') NOT NULL DEFAULT 'F' COMMENT 'is server is admin mode' AFTER server_engine_version;

ALTER TABLE developer.audit_game_servers
 ADD protocol_version INT(11) NOT NULL DEFAULT '0' COMMENT 'client-server protocol reported by server' AFTER server_type_id,
 ADD schema_version INT(11) NOT NULL DEFAULT '0' COMMENT 'server\'s schema version' AFTER protocol_version,
 ADD server_engine_version VARCHAR(30) NOT NULL DEFAULT '' COMMENT 'file version of serverengine.dll' AFTER schema_version,
 ADD server_engine_admin_only ENUM('T','F') NOT NULL DEFAULT 'F' COMMENT 'is server is admin mode' AFTER server_engine_version;

-- history only updated once an hour so skip this for now
-- ALTER TABLE developer.game_server_history
--  ADD visibility_id INT AFTER port;

ALTER TABLE developer.game_licenses
 ADD max_game_users INT(11) NOT NULL DEFAULT '2000' COMMENT 'max for entire game, across servers' AFTER license_status_id;

ALTER TABLE developer.audit_game_licenses
 ADD max_game_users INT(11) NOT NULL DEFAULT '2000' COMMENT 'max for entire game, across servers' AFTER license_status_id;

-- boot strap the values in the table
UPDATE developer.game_licenses gl, developer.game_license_subscriptions gls 
  SET gl.max_game_users = gls.max_server_users
   WHERE gl.license_subscription_id = gls.license_subscription_id;

CREATE TABLE developer.game_login_log (
   game_login_id INT AUTO_INCREMENT NOT NULL COMMENT 'PK',
   user_id INT(11) NOT NULL,
   game_id INT(11) NOT NULL,
   logon_time DATETIME NOT NULL COMMENT 'time of login',
  PRIMARY KEY (game_login_id)
) ENGINE = MyISAM COMMENT = 'track logins to all games (not just WOK)' ROW_FORMAT = DEFAULT;

CREATE TABLE `schema_versions` (
  `version` int(11) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL default CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY  (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- remove triggers no longer needed and update triggers for new audit cols
DROP TRIGGER IF EXISTS after_insert_game_servers;
DROP TRIGGER IF EXISTS before_update_game_servers;
DROP TRIGGER IF EXISTS before_delete_game_servers;
\. before#delete#game_licenses.sql
\. before#update#game_licenses.sql
\. after#insert#game_licenses.sql
\. sp#add_game_servers.sql
\. sp#remove_game_servers.sql
\. sp#update_game_servers.sql
\. sp#update_game_servers_ping.sql
\. sp#update_game_servers_start.sql

\. sp#check_game_auth_kaneva.sql
-- now only in developer
DROP PROCEDURE IF EXISTS wok.get_game_info; 
\. sp#get_game_info.sql

INSERT INTO schema_versions( `version`, `description` ) VALUES (48, 'STAR Open Beta changes' );
