-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

USE developer;

SELECT 'Starting Promotion... ',NOW();

\. developer_SchemaChanges_v96.sql

-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (96.0, 'Added developer_emails table', NOW());

SELECT 'Finished Promotion... ',NOW();