-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS z_dumpGame;
DROP PROCEDURE IF EXISTS z_dumpGameByName;
DROP PROCEDURE IF EXISTS z_dumpGameByServer;
DROP FUNCTION IF EXISTS format_version;

DELIMITER //

CREATE FUNCTION format_version( ver INT ) returns VARCHAR(30) deterministic
BEGIN
  return concat( (ver >> 24), '.', (ver & 0xffffff) >> 16, '.', (ver & 0xffff) >> 8, '.', ver & 0xff);
END;
//
CREATE PROCEDURE z_dumpGameByServer(IN __server_id INT)
BEGIN

  SELECT game_id INTO @gameId
    FROM game_servers 
   WHERE server_id = __server_id;
   
  IF @gameId IS NULL THEN 
    SELECT 'Game not found' as Error;
  ELSE
    CALL z_dumpGame( @gameId );
  END IF;
  
END;
//


CREATE PROCEDURE z_dumpGameByName(IN __game_name VARCHAR(100))
BEGIN
  DECLARE done INT DEFAULT 0;
  DECLARE __game_id INT;
  DECLARE __found INT;
	DECLARE cur1 CURSOR FOR   SELECT game_id 
    FROM games 
   WHERE game_name like __game_name;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN cur1;
	REPEAT	
		FETCH cur1 INTO __game_id;
    IF done THEN 
      SELECT 'No matches found' as 'Error';
		ELSE 
      SELECT FOUND_ROWS() INTO __found;
      
      IF __found = 1 THEN 
        CALL z_dumpGame( __game_id );
        SET done = 1;
      ELSE
        SELECT game_name as 'Found_mulitple_matches' FROM games 
        WHERE game_name like __game_name;
        SET done = 1;
      END IF;
		END IF;

	UNTIL done END REPEAT;
	
	CLOSE cur1;
  
END;
//

CREATE PROCEDURE z_dumpGame (IN __game_id INT)
BEGIN
  SELECT g.game_id, game_name, username as owner, game_key 
    FROM games g LEFT OUTER JOIN kaneva.users u ON g.owner_id = u.user_id
        LEFT OUTER JOIN game_licenses l ON g.game_id = l.game_id
   WHERE g.game_id = __game_id;

  SELECT patch_url FROM game_patch_urls WHERE game_id = __game_id;
  
  select server_id, server_name, gss.name as status, gsv.name as visibility, number_of_players, last_ping_datetime, server_started_date, ip_address, port, actual_ip_address, is_external, gst.name as server_type, hex(gs.protocol_version) as protocol_version, gs.schema_version, gs.server_engine_version, format_version(gs.asset_version) as asset_version
  from game_servers gs join game_server_status gss on gs.server_status_id = gss.server_status_id join game_server_visibility gsv on gs.visibility_id = gsv.visibility_id
  join game_server_types gst on gs.server_type_id = gst.server_type_id
  where gs.game_id = __game_id;

END;
//

DELIMITER ;