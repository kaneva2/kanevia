-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

﻿-- remove triggers no longer needed and update triggers for new audit cols
\. developer_SchemaChanges_v51.sql

DROP TRIGGER IF EXISTS after_insert_game_servers;
DROP TRIGGER IF EXISTS before_update_game_servers;
DROP TRIGGER IF EXISTS before_delete_game_servers;
\. before#delete#game_licenses.sql
\. before#update#game_licenses.sql
\. after#insert#game_licenses.sql
\. sp#add_game_servers.sql
\. sp#remove_game_servers.sql
\. sp#update_game_servers.sql
\. sp#update_game_servers_ping.sql
\. sp#update_game_servers_start.sql

\. sp#check_game_auth_kaneva.sql
-- now only in developer
DROP PROCEDURE IF EXISTS wok.get_game_info; 
\. sp#get_game_info.sql

DROP PROCEDURE IF EXISTS kaneva_PromotePolicy_v52;
DELIMITER //
CREATE PROCEDURE kaneva_PromotePolicy_v52()
BEGIN

IF NOT EXISTS (SELECT 1 FROM schema_versions WHERE version = 51) THEN
	INSERT INTO schema_versions( `version`, `description` ) VALUES (51, 'STAR Open Beta changes' );
END IF;

END //

DELIMITER ;

CALL kaneva_PromotePolicy_v52;
DROP PROCEDURE IF EXISTS kaneva_PromotePolicy_v52;
