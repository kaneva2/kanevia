-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- change the data for new security roles 
\. developer_SeedData_v52.sql

\. sp#update_game_servers_start.sql

-- add developer version number
DROP PROCEDURE IF EXISTS developer_SchemaChanges_v52;
DELIMITER //
CREATE PROCEDURE developer_SchemaChanges_v52()
BEGIN
IF NOT EXISTS (SELECT 1 FROM schema_versions WHERE version = 52.0) THEN
	INSERT INTO `schema_versions` ( `version`, `description` ) 
		VALUES (52.0, 'data change for production to go along with site update' );
END IF;

END //

DELIMITER ;

CALL developer_SchemaChanges_v52;
DROP PROCEDURE IF EXISTS developer_SchemaChanges_v52;
