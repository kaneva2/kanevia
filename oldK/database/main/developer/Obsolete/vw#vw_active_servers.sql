-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

drop view if exists vw_active_servers;

-- get a pretty list of servers that have pinged in at least once
create view vw_active_servers as
SELECT g.game_id, 
			 gg.game_name,
			 server_id,
       server_name,
       gv.name as visibility,
       ip_address,
       port,
       number_of_players
       max_players,
       server_started_date,
       last_ping_datetime, 
			 gs.name as status
  FROM game_servers g inner join game_server_status gs on g.server_status_id = gs.server_status_id
	     inner join game_server_visibility gv on g.visibility_id = gv.visibility_id
			 inner join games gg on gg.game_id = g.game_id
WHERE port <> 0			 
 