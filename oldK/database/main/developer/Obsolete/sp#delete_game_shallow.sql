-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_game_shallow;

DELIMITER //


CREATE PROCEDURE delete_game_shallow( IN __gameId INT , IN __owner_id INT )
BEGIN
 
  
 -- temporary plug for deleting a game: only hides the game from logins
 -- this will set the access_id flag to 5 = 'deleted'
 UPDATE games SET game_access_id = 5 WHERE game_id = __gameId and owner_id = __owner_id;

    
 END
//

DELIMITER ;
