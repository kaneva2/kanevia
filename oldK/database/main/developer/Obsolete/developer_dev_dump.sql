-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

OBSOLETE!!!

-- MySQL dump 10.11
--
-- Host: 10.11.11.6    Database: developer
-- ------------------------------------------------------
-- Server version	5.0.51-community-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `developer`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `developer` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `developer`;

--
-- Table structure for table `audit_game_licenses`
--

DROP TABLE IF EXISTS `audit_game_licenses`;
CREATE TABLE `audit_game_licenses` (
  `game_license_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL default '0' COMMENT 'FK to games',
  `license_subscription_id` int(11) default NULL COMMENT 'FK to game_license_subcription, trial/consumer/pro',
  `game_key` varchar(100) NOT NULL default '' COMMENT 'used by all servers for game',
  `start_date` datetime default NULL COMMENT 'for tracking trials',
  `end_date` datetime default NULL COMMENT 'for tracking trials',
  `purchase_date` datetime default NULL,
  `license_status_id` int(11) NOT NULL default '0' COMMENT 'FK to game_license_status active, etc.',
  `modifiers_id` int(10) unsigned NOT NULL,
  `date_modified` datetime NOT NULL default '0000-00-00 00:00:00',
  `operation` varchar(20) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `audit_game_servers`
--

DROP TABLE IF EXISTS `audit_game_servers`;
CREATE TABLE `audit_game_servers` (
  `server_id` int(11) NOT NULL,
  `server_name` varchar(100) NOT NULL COMMENT 'hostname of server, preconfigured, must match when pinging',
  `game_id` int(11) NOT NULL COMMENT 'FK to games',
  `visibility_id` int(11) NOT NULL default '0' COMMENT 'FK to game_server_visibility, public, etc.',
  `server_started_date` datetime default NULL COMMENT 'when last started',
  `ip_address` varchar(50) NOT NULL default '' COMMENT 'external IP used by clients',
  `port` int(11) NOT NULL default '0' COMMENT 'port used by client',
  `server_status_id` int(11) NOT NULL default '0' COMMENT 'FK to game_server_status, running, etc.',
  `number_of_players` int(11) default NULL COMMENT 'current player count',
  `max_players` int(11) NOT NULL default '0' COMMENT 'as configured on server',
  `last_ping_datetime` datetime default NULL,
  `server_type_id` int(11) NOT NULL,
  `modifiers_id` int(10) unsigned NOT NULL,
  `date_modified` datetime NOT NULL default '0000-00-00 00:00:00',
  `operation` varchar(20) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `audit_games`
--

DROP TABLE IF EXISTS `audit_games`;
CREATE TABLE `audit_games` (
  `game_id` int(10) unsigned NOT NULL,
  `owner_id` int(10) unsigned NOT NULL default '0',
  `game_rating_id` smallint(5) unsigned NOT NULL default '1',
  `game_status_id` smallint(5) unsigned NOT NULL default '1',
  `game_name` varchar(100) NOT NULL default '',
  `game_description` text,
  `game_synopsis` varchar(50) default NULL,
  `game_creation_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `game_access_id` varchar(45) NOT NULL default '1',
  `game_image_path` varchar(255) default NULL,
  `game_keywords` text,
  `last_updated` datetime default NULL,
  `modifiers_id` int(10) unsigned NOT NULL default '0',
  `date_modified` datetime NOT NULL default '0000-00-00 00:00:00',
  `operation` varchar(20) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `client_platforms`
--

DROP TABLE IF EXISTS `client_platforms`;
CREATE TABLE `client_platforms` (
  `client_platform_id` int(10) unsigned NOT NULL default '0',
  `client_platform` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`client_platform_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `game_access`
--

DROP TABLE IF EXISTS `game_access`;
CREATE TABLE `game_access` (
  `game_access_id` smallint(5) unsigned NOT NULL,
  `game_access` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`game_access_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `game_categories`
--

DROP TABLE IF EXISTS `game_categories`;
CREATE TABLE `game_categories` (
  `category_id` int(11) NOT NULL auto_increment,
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=542 DEFAULT CHARSET=latin1;

--
-- Table structure for table `game_license_status`
--

DROP TABLE IF EXISTS `game_license_status`;
CREATE TABLE `game_license_status` (
  `license_status_id` smallint(5) unsigned NOT NULL,
  `license_status` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`license_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `game_license_subscriptions`
--

DROP TABLE IF EXISTS `game_license_subscriptions`;
CREATE TABLE `game_license_subscriptions` (
  `license_subscription_id` smallint(6) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `length_of_subscription` int(11) NOT NULL default '0',
  `amount` float NOT NULL default '0',
  `amount_kei_point_id` varchar(50) NOT NULL default '',
  `max_server_users` int(11) default NULL,
  `disk_quota` int(11) NOT NULL default '0',
  PRIMARY KEY  (`license_subscription_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Table structure for table `game_licenses`
--

DROP TABLE IF EXISTS `game_licenses`;
CREATE TABLE `game_licenses` (
  `game_license_id` int(11) NOT NULL auto_increment,
  `game_id` int(11) NOT NULL default '0' COMMENT 'FK to games',
  `license_subscription_id` int(11) default NULL COMMENT 'FK to game_license_subcription, trial/consumer/pro',
  `game_key` varchar(100) NOT NULL default '' COMMENT 'used by all servers for game',
  `start_date` datetime default NULL COMMENT 'for tracking trials',
  `end_date` datetime default NULL COMMENT 'for tracking trials',
  `purchase_date` datetime default NULL,
  `license_status_id` int(11) NOT NULL default '1' COMMENT 'FK to game_license_status active, etc.',
  `modifiers_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`game_license_id`),
  KEY `INDEX_game_id` (`game_id`),
  KEY `INDEX_game_key` (`game_key`)
) ENGINE=InnoDB AUTO_INCREMENT=262 DEFAULT CHARSET=latin1 COMMENT='track extra data for a game';

/*!50003 SET @OLD_SQL_MODE=@@SQL_MODE*/;
DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */  /*!50003 TRIGGER `after_insert_game_licenses` AFTER INSERT ON `game_licenses` FOR EACH ROW BEGIN

    INSERT into developer.audit_game_licenses

    (

    	game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, license_status_id, modifiers_id,

    	date_modified, operation

    )

    VALUES

    (

    	

    	new.game_license_id, new.game_id, new.license_subscription_id, new.game_key, new.start_date, new.end_date, new.purchase_date, new.license_status_id, 

    	new.modifiers_id, Now(), 'insert'

    ) ;

END */;;

/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */  /*!50003 TRIGGER `before_update_game_licenses` BEFORE UPDATE ON `game_licenses` FOR EACH ROW BEGIN

    INSERT into developer.audit_game_licenses

    (

    	game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, license_status_id, modifiers_id,

    	date_modified, operation

    )

    VALUES

    (

    	

    	old.game_license_id, old.game_id, old.license_subscription_id, old.game_key, old.start_date, old.end_date, old.purchase_date, old.license_status_id, 

    	old.modifiers_id, Now(), 'update'

    ) ;

END */;;

/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */  /*!50003 TRIGGER `before_delete_game_licenses` BEFORE DELETE ON `game_licenses` FOR EACH ROW BEGIN

    INSERT into developer.audit_game_licenses

    (

    	game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, license_status_id, modifiers_id,

    	date_modified, operation

    )

    VALUES

    (

    	

    	old.game_license_id, old.game_id, old.license_subscription_id, old.game_key, old.start_date, old.end_date, old.purchase_date, old.license_status_id, 

    	old.modifiers_id, Now(), 'delete'

    ) ;

END */;;

DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;

-- manually added to dump file 7/08
CREATE TABLE developer.game_patcher_settings (
   game_id INT NOT NULL DEFAULT '0' COMMENT 'FK to games',
   pane1_url VARCHAR(255) NOT NULL DEFAULT '',
   pane2_url VARCHAR(255) NOT NULL DEFAULT '',
  `pane1_title` varchar(255) default NULL,
  `pane2_title` varchar(255) default NULL,
  `version_text` varchar(20) default NULL,
  background_url VARCHAR(255) default NULL
) ENGINE = InnoDB COMMENT = 'patcher customizations' ROW_FORMAT = DEFAULT;

--
-- Table structure for table `game_patch_urls`
--

DROP TABLE IF EXISTS `game_patch_urls`;
CREATE TABLE `game_patch_urls` (
  `patchURL_id` int(10) unsigned NOT NULL auto_increment,
  `patch_id` int(10) unsigned NOT NULL default '1',
  `game_id` int(10) unsigned NOT NULL default '0',
  `patch_url` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`patchURL_id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=latin1;

--
-- Table structure for table `game_ratings`
--

DROP TABLE IF EXISTS `game_ratings`;
CREATE TABLE `game_ratings` (
  `game_rating_id` int(10) unsigned NOT NULL auto_increment,
  `game_rating` varchar(50) NOT NULL default '',
  `description` varchar(255) default NULL,
  `minimum_age` smallint(5) unsigned NOT NULL default '14',
  `is_mature` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`game_rating_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Table structure for table `game_raves`
--

DROP TABLE IF EXISTS `game_raves`;
CREATE TABLE `game_raves` (
  `rave_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL default '0',
  `game_id` int(10) unsigned NOT NULL default '0',
  `rave_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`rave_id`),
  UNIQUE KEY `idx_uniqueRave` (`game_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `game_role_ids`
--

DROP TABLE IF EXISTS `game_role_ids`;
CREATE TABLE `game_role_ids` (
  `game_role_id` smallint(6) NOT NULL,
  `game_role` varchar(50) default NULL COMMENT 'descriptive name',
  PRIMARY KEY  (`game_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='lookup table for asset_roles';

--
-- Table structure for table `game_roles`
--

DROP TABLE IF EXISTS `game_roles`;
CREATE TABLE `game_roles` (
  `user_id` int(11) NOT NULL COMMENT 'FK to users',
  `game_id` int(11) NOT NULL COMMENT 'FK to games',
  `game_role_id` smallint(6) NOT NULL COMMENT 'FK to game_roles',
  PRIMARY KEY  (`user_id`,`game_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='track user permissions for a star except owners';

--
-- Table structure for table `game_server_history`
--

DROP TABLE IF EXISTS `game_server_history`;
CREATE TABLE `game_server_history` (
  `server_history_id` int(11) NOT NULL auto_increment,
  `server_id` int(11) NOT NULL default '0',
  `player_count` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `ip_address` varchar(20) default NULL,
  `port` int(11) default NULL,
  PRIMARY KEY  (`server_history_id`),
  KEY `FK_server_history_current_game_engines` (`server_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24802 DEFAULT CHARSET=latin1;

--
-- Table structure for table `game_server_status`
--

DROP TABLE IF EXISTS `game_server_status`;
CREATE TABLE `game_server_status` (
  `server_status_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`server_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `game_server_types`
--

DROP TABLE IF EXISTS `game_server_types`;
CREATE TABLE `game_server_types` (
  `server_type_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`server_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `game_server_visibility`
--

DROP TABLE IF EXISTS `game_server_visibility`;
CREATE TABLE `game_server_visibility` (
  `visibility_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`visibility_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `game_servers`
--

DROP TABLE IF EXISTS `game_servers`;
CREATE TABLE `game_servers` (
  `server_id` int(11) NOT NULL auto_increment,
  `server_name` varchar(100) NOT NULL COMMENT 'hostname of server, preconfigured, must match when pinging',
  `game_id` int(11) NOT NULL COMMENT 'FK to games',
  `visibility_id` int(11) NOT NULL default '0' COMMENT 'FK to game_server_visibility, public, etc.',
  `server_started_date` datetime default NULL COMMENT 'when last started',
  `ip_address` varchar(50) NOT NULL default '0' COMMENT 'external IP used by clients',
  `port` int(11) NOT NULL default '0' COMMENT 'port used by client',
  `server_status_id` int(11) NOT NULL default '0' COMMENT 'FK to game_server_status, running, etc.',
  `number_of_players` int(11) default NULL COMMENT 'current player count',
  `max_players` int(11) NOT NULL default '0' COMMENT 'as configured on server',
  `last_ping_datetime` datetime default NULL,
  `server_type_id` int(11) NOT NULL default '1',
  `modifiers_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`server_id`),
  KEY `INDEX_server_name` (`server_name`),
  KEY `fast_summary` (`game_id`,`server_status_id`,`last_ping_datetime`)
) ENGINE=InnoDB AUTO_INCREMENT=602 DEFAULT CHARSET=latin1 COMMENT='configure and track servers for a game';

/*!50003 SET @OLD_SQL_MODE=@@SQL_MODE*/;
DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */  /*!50003 TRIGGER `after_insert_game_servers` AFTER INSERT ON `game_servers` FOR EACH ROW BEGIN

    INSERT into developer.audit_game_servers

    (

    	server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, number_of_players,

    	max_players, last_ping_datetime, server_type_id, modifiers_id, date_modified, operation

    )

    VALUES

    (

    	

    	new.server_id, new.server_name, new.game_id, new.visibility_id, new.server_started_date, new.ip_address, new.port, new.server_status_id, new.number_of_players,

    	new.max_players, new.last_ping_datetime, new.server_type_id, new.modifiers_id, Now(), 'insert'

    ) ;

END */;;

/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */  /*!50003 TRIGGER `before_update_game_servers` BEFORE UPDATE ON `game_servers` FOR EACH ROW BEGIN

    INSERT into developer.audit_game_servers

    (

    	server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, number_of_players,

    	max_players, last_ping_datetime, server_type_id, modifiers_id, date_modified, operation

    )

    VALUES

    (

    	

    	old.server_id, old.server_name, old.game_id, old.visibility_id, old.server_started_date, old.ip_address, old.port, old.server_status_id, old.number_of_players,

    	old.max_players, old.last_ping_datetime, old.server_type_id, old.modifiers_id, Now(), 'update'

    ) ;

END */;;

/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */  /*!50003 TRIGGER `before_delete_game_servers` BEFORE DELETE ON `game_servers` FOR EACH ROW BEGIN

    INSERT into developer.audit_game_servers

    (

    	server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, number_of_players,

    	max_players, last_ping_datetime, server_type_id, modifiers_id, date_modified, operation

    )

    VALUES

    (

    	

    	old.server_id, old.server_name, old.game_id, old.visibility_id, old.server_started_date, old.ip_address, old.port, old.server_status_id, old.number_of_players,

    	old.max_players, old.last_ping_datetime, old.server_type_id, old.modifiers_id, Now(), 'delete'

    ) ;

END */;;

DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;

--
-- Table structure for table `game_stats`
--

DROP TABLE IF EXISTS `game_stats`;
CREATE TABLE `game_stats` (
  `game_id` int(10) unsigned NOT NULL default '0',
  `number_of_comments` int(10) unsigned NOT NULL default '0',
  `number_of_raves` int(10) unsigned NOT NULL default '0',
  `number_of_views` int(10) unsigned NOT NULL default '0',
  `last_updated` timestamp NOT NULL default '0000-00-00 00:00:00',
  `number_of_players` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  USING BTREE (`game_id`,`number_of_views`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `game_status`
--

DROP TABLE IF EXISTS `game_status`;
CREATE TABLE `game_status` (
  `game_status_id` smallint(5) unsigned NOT NULL,
  `game_status` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`game_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `game_views`
--

DROP TABLE IF EXISTS `game_views`;
CREATE TABLE `game_views` (
  `view_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL default '0',
  `game_id` int(10) unsigned NOT NULL default '0',
  `view_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `ip_address` int(10) unsigned default NULL,
  PRIMARY KEY  (`view_id`),
  UNIQUE KEY `idx_uniqueView` (`user_id`,`game_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
CREATE TABLE `games` (
  `game_id` int(10) NOT NULL auto_increment,
  `owner_id` int(10) unsigned NOT NULL default '0',
  `game_rating_id` smallint(5) unsigned NOT NULL default '1',
  `game_status_id` smallint(5) unsigned NOT NULL default '1',
  `game_name` varchar(100) NOT NULL default '',
  `game_description` text,
  `game_synopsis` varchar(50) default NULL,
  `game_creation_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `game_access_id` varchar(45) NOT NULL default '1',
  `game_image_path` varchar(255) default NULL,
  `game_keywords` text,
  `game_encryption_key` varchar(16) default NULL,
  `last_updated` datetime default NULL,
  `modifiers_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`game_id`),
  KEY `idx_creationDate` (`game_creation_date`),
  KEY `idx_owner` (`owner_id`),
  KEY `idx_name` (`game_name`),
  KEY `idx_rating` (`game_rating_id`),
  KEY `idx_status` (`game_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5182 DEFAULT CHARSET=latin1;

/*!50003 SET @OLD_SQL_MODE=@@SQL_MODE*/;
DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */  /*!50003 TRIGGER `after_insert_games` AFTER INSERT ON `games` FOR EACH ROW BEGIN

    INSERT into developer.audit_games

    (

    	game_id, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis, game_creation_date, game_access_id,

    	game_image_path, game_keywords, last_updated, modifiers_id, date_modified, operation

    )

    VALUES

    (

    	

    	new.game_id, new.owner_id, new.game_rating_id, new.game_status_id, new.game_name, new.game_description, new.game_synopsis,

    	new.game_creation_date, new.game_access_id, new.game_image_path, new.game_keywords, new.last_updated,

        new.modifiers_id, Now(), 'insert'

    ) ;   

    

	INSERT into developer.game_stats

	(

		game_id, last_updated

	)

	VALUES

	(



		new.game_id, Now()

	) ;



END */;;

/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */  /*!50003 TRIGGER `before_update_games` BEFORE UPDATE ON `games` FOR EACH ROW BEGIN

    INSERT into developer.audit_games

    (

    	game_id, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis, game_creation_date, game_access_id,

    	game_image_path, game_keywords, last_updated, modifiers_id, date_modified, operation

    )

    VALUES

    (

    	

    	old.game_id, old.owner_id, old.game_rating_id, old.game_status_id, old.game_name, old.game_description, old.game_synopsis,

    	old.game_creation_date, old.game_access_id, old.game_image_path, old.game_keywords, old.last_updated,

      old.modifiers_id, Now(), 'update'

    ) ;    

END */;;

/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */  /*!50003 TRIGGER `before_delete_games` BEFORE DELETE ON `games` FOR EACH ROW BEGIN

    INSERT into developer.audit_games

    (

    	game_id, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis, game_creation_date, game_access_id,

    	game_image_path, game_keywords, last_updated, modifiers_id, date_modified, operation

    )

    VALUES

    (

    	

    	old.game_id, old.owner_id, old.game_rating_id, old.game_status_id, old.game_name, old.game_description, old.game_synopsis,

    	old.game_creation_date, old.game_access_id, old.game_image_path, old.game_keywords, old.last_updated,

      old.modifiers_id, Now(), 'delete'

    ) ;



	DELETE from developer.game_stats WHERE game_id = old.game_id;

END */;;

DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;

--
-- Table structure for table `games_to_categories`
--

DROP TABLE IF EXISTS `games_to_categories`;
CREATE TABLE `games_to_categories` (
  `game_id` int(10) unsigned NOT NULL default '0',
  `category_id` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (`game_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `project_startdates`
--

DROP TABLE IF EXISTS `project_startdates`;
CREATE TABLE `project_startdates` (
  `project_startdate_id` int(10) unsigned NOT NULL default '0',
  `project_startdate` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`project_startdate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `project_status`
--

DROP TABLE IF EXISTS `project_status`;
CREATE TABLE `project_status` (
  `project_status_id` int(10) unsigned NOT NULL default '0',
  `project_status` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`project_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `prospective_star_developers`
--

DROP TABLE IF EXISTS `prospective_star_developers`;
CREATE TABLE `prospective_star_developers` (
  `prospective_developer_id` bigint(20) unsigned NOT NULL auto_increment,
  `contacts_firstname` varchar(50) NOT NULL default '',
  `contacts_lastname` varchar(50) NOT NULL default '',
  `contacts_email` varchar(100) NOT NULL default '',
  `contacts_phone` varchar(20) NOT NULL default '',
  `company_name` varchar(70) NOT NULL default '',
  `company_addressI` varchar(80) NOT NULL default '',
  `company_addressII` varchar(80) default NULL,
  `city` varchar(50) NOT NULL default '',
  `state_code` char(2) NOT NULL default 'GA',
  `province` varchar(50) NOT NULL default '',
  `country_id` char(2) NOT NULL default 'US',
  `zip_code` varchar(13) NOT NULL default '',
  `website_URL` varchar(100) default NULL,
  `annual_revenue` int(10) unsigned NOT NULL default '0',
  `vworld_involvment_id` smallint(5) unsigned NOT NULL default '1',
  `project_name` varchar(45) NOT NULL default '',
  `project_description` varchar(200) default NULL,
  `project_startdate_id` smallint(5) unsigned NOT NULL default '0',
  `budget` int(10) unsigned NOT NULL default '0',
  `client_platform_id` smallint(5) unsigned NOT NULL default '0',
  `project_status_id` smallint(5) unsigned NOT NULL default '0',
  `current_team_size` smallint(5) unsigned NOT NULL default '0',
  `projected_team_size` smallint(5) unsigned NOT NULL default '0',
  `date_submitted` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`prospective_developer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Table structure for table `vworld_involvement`
--

DROP TABLE IF EXISTS `vworld_involvement`;
CREATE TABLE `vworld_involvement` (
  `vworld_involvement_id` int(10) unsigned NOT NULL default '0',
  `vworld_involvement` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`vworld_involvement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping routines for database 'developer'
--
DELIMITER ;;
/*!50003 DROP PROCEDURE IF EXISTS `delete_game` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`ajones`@`%`*/ /*!50003 PROCEDURE `delete_game`( IN __game INT(11) , IN __modifier INT(11) )
BEGIN



Declare _now DateTime;

 

	-- delete non audited data

	DELETE FROM game_views

	  WHERE game_id = __game;



	DELETE FROM game_stats

	  WHERE game_id = __game;



	DELETE FROM game_server_history

	  WHERE server_id IN (SELECT server_id FROM game_servers WHERE game_id = __game );



	DELETE FROM game_roles

	  WHERE game_id = __game;



	DELETE FROM game_raves

	  WHERE game_id = __game;



	DELETE FROM game_patch_urls

	  WHERE game_id = __game;



	-- get current date time

	set _now = NOW();



  -- delete audited data

	DELETE FROM game_servers

	   WHERE game_id = __game;

	UPDATE audit_game_servers SET modifiers_id = __modifier WHERE game_id = __game AND date_modified >= _now AND operation = 'delete';

	

	DELETE FROM game_licenses	

	  WHERE game_id = __game;

	UPDATE audit_game_licenses SET modifiers_id = __modifier WHERE game_id = __game AND date_modified >= _now AND operation = 'delete';

	

	DELETE FROM games 

	  WHERE game_id = __game;

	UPDATE audit_games SET modifiers_id = __modifier WHERE game_id = __game AND date_modified >= _now AND operation = 'delete';



	  

END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
DELIMITER ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2008-06-11 11:11:21
