-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- string sqlString = "CALL update_game_servers_start (@serverID, @maxcount, @serverstatusID, @ipaddress, @port, @protocolversion, @schemaversion, @serverengineversion )";
CALL update_game_servers_start (721, 75, 0, 'jreinhard', 7, 0, 0, 0 );

-- string sqlString = "CALL update_game_servers_ping (@serverid, @numberofplayers, @maxplayers, @serverstatusid, @serverengineadminonly )";
CALL update_game_servers_ping (721, 5, 100, 0, 'F' );

/*
UPDATE game_servers " +
                        "SET number_of_players = 0, " +
                        "    max_players = " + maxCount + ", " +
                        "    server_status_id = " + (int)Constants.eGAME_SERVER_STATUS.STARTING + ", " +
                        "    last_ping_datetime = " + dbUtility.GetCurrentDateFunction() + ", " +
                        "    ip_address = '" + dbUtility.CleanText(ipAddress) + "', " +
                        "    port = " + port + ", " +
                        "    server_started_date = " + dbUtility.GetCurrentDateFunction()  + ", " +
                        "    protocol_version = " + protocolVersion.ToString() + ", " +
                        "    schema_version = " + schemaVersion.ToString() + ", " +
                        "    server_engine_version = '" + dbUtility.CleanText(serverVersion) + "'" +
                        " WHERE server_id = @serverId";
												
*/