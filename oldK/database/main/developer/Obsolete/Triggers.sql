-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

\. after#insert#game_licenses.sql
\. after#insert#game_rave.sql
\. after#insert#game_servers.sql
\. after#insert#games.sql
\. before#delete#game_licenses.sql
\. before#delete#game_servers.sql
\. before#delete#games.sql
\. before#update#game_licenses.sql
\. before#update#game_servers.sql
\. before#update#games.sql
