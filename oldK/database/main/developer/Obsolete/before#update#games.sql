-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS before_update_games;
DELIMITER ;;

CREATE TRIGGER `before_update_games` BEFORE UPDATE ON `games` 
	FOR EACH ROW 
	BEGIN
   INSERT into developer.audit_games
    (	game_id, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis, game_creation_date, game_access_id,
    	game_image_path, game_keywords, last_updated, modifiers_id, date_modified, operation )
    VALUES
    (	old.game_id, old.owner_id, old.game_rating_id, old.game_status_id, old.game_name, old.game_description, old.game_synopsis,
    	old.game_creation_date, old.game_access_id, old.game_image_path, old.game_keywords, old.last_updated,
      old.modifiers_id, Now(), 'update' ) ;    

END ;;

