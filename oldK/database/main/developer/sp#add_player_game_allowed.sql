-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_player_game_allowed;

DELIMITER ;;
CREATE PROCEDURE `add_player_game_allowed`( IN _kaneva_user_id INT, IN _game_id INT)
BEGIN 

INSERT INTO player_game_allowed 
	(kaneva_user_id, game_id, first_visit_date)
	VALUES
	(_kaneva_user_id, _game_id, NOW());

END
;;
DELIMITER ;