-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_game_servers;

DELIMITER ;;
CREATE PROCEDURE `update_game_servers`(__server_id					INT(11),
	__server_name 				VARCHAR(100),
	__game_id 						INT(11),
	__visibility_id 			INT(11),
	__server_started_date DATETIME,
	__ip_address					VARCHAR(50),
	__port 								INT(11),
	__number_of_players 	INT(11),
	__unused 				   INT(11),
	__last_ping_datetime 	DATETIME,
	__modifiers_id 				INT(10)
	)
BEGIN
START TRANSACTION;

INSERT into developer.audit_game_servers
    (	server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, number_of_players,
    	max_players, last_ping_datetime, server_type_id, modifiers_id, date_modified, operation )
    SELECT
     	server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, number_of_players,
    	max_players, last_ping_datetime, server_type_id, modifiers_id, Now(), 'update'
		FROM developer.game_servers
		WHERE server_id = __server_id;
		
UPDATE game_servers
		SET server_name = __server_name,
				game_id = __game_id,
				visibility_id = __visibility_id,
				server_started_date = __server_started_date,
        ip_address = __ip_address,
        port = __port,
				number_of_players = __number_of_players, 
        last_ping_datetime = __last_ping_datetime,
        modifiers_id = __modifiers_id
    WHERE server_id = __server_id;
		
COMMIT;

END
;;
DELIMITER ;