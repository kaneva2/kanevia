-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_new_game;

DELIMITER ;;
CREATE PROCEDURE `add_new_game`(ownerId INTEGER UNSIGNED, gameRatingId SMALLINT UNSIGNED, gameStatusId SMALLINT UNSIGNED,
            gameName VARCHAR(100), gameDescription TEXT, gameSynopsis VARCHAR(50), gameCreationDate DATETIME,
            gameAccessId VARCHAR(45), gameImagePath VARCHAR(255), gameKeywords TEXT, gameEncryptionKey VARCHAR(16),
            lastUpdated DATETIME, modifiersId INTEGER UNSIGNED, parentGameId INTEGER SIGNED, OUT _gameId INTEGER SIGNED)
BEGIN
            
    /*insert into table*/
    INSERT INTO games 
    (
	    owner_id, 
	    game_rating_id, 
	    game_status_id, 
	    game_name, 
	    game_description,
            game_synopsis, 
            game_creation_date, 
            game_access_id, 
            game_image_path, 
            game_keywords, 
            game_encryption_key,
            last_updated, 
            modifiers_id, 
            parent_game_id                
    )
    VALUES 
    (
	    ownerId, 
	    gameRatingId, 
	    gameStatusId, 
	    gameName, 
	    gameDescription,
            gameSynopsis, 
            gameCreationDate, 
            gameAccessId, 
            gameImagePath, 
            gameKeywords, 
            gameEncryptionKey,
            lastUpdated, 
            modifiersId, 
            parentGameId
    );
    
    -- get the community id of the new community
    SELECT LAST_INSERT_ID() INTO _gameId;
    
       INSERT into developer.audit_games
    (	game_id, owner_id, game_rating_id, game_status_id, game_name, game_description, game_synopsis, game_creation_date, game_access_id,
    	game_image_path, game_keywords, last_updated, modifiers_id, date_modified, operation )
    VALUES
    (	_gameId, ownerId, gameRatingId, gameStatusId, gameName, gameDescription, gameSynopsis,
    	gameCreationDate, gameAccessId, gameImagePath, gameKeywords, lastUpdated,
      modifiersId, Now(), 'insert' ) ;   
	INSERT into developer.game_stats
	(	game_id, last_updated	)
	VALUES
	(	_gameId, Now() ) ;
	
END
;;
DELIMITER ;