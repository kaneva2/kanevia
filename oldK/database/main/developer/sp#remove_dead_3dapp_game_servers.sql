-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS remove_dead_3dapp_game_servers;

DELIMITER ;;
CREATE PROCEDURE `remove_dead_3dapp_game_servers`()
BEGIN
-- called to remove players from a 3dapp server than hasn't pinged in for deadServerInterval minutes
	DECLARE __done INT DEFAULT 0;
	DECLARE __servers_updated INT(11) DEFAULT 0;
	DECLARE __server_id INT(11) DEFAULT 0;
	DECLARE __user_id INT(11) DEFAULT 0;
	DECLARE __game_id INT(11) DEFAULT 0;
	DECLARE __actual_ip_address VARCHAR(50) DEFAULT NULL;

	DECLARE __servers CURSOR FOR SELECT dgs.server_id, dgs.actual_ip_address, dgs.game_id, dal.user_id FROM developer.game_servers dgs, developer.active_logins dal
				     		   	     WHERE dgs.server_status_id <> 0 
									     AND dgs.server_id=dal.server_id
							   	     AND dgs.last_ping_datetime <= DATE_SUB(NOW(),INTERVAL deadServerInterval() MINUTE)
										  AND NOT wok.is_wok_game(dgs.game_id) FOR UPDATE;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1;

	START TRANSACTION;

 	OPEN __servers;

		REPEAT	
			FETCH __servers into __server_id, __actual_ip_address, __game_id, __user_id;

			IF NOT __done THEN 

				-- update deadServerInterval too
				
				UPDATE developer.game_servers
				   SET server_status_id = 0 -- stopped
				 WHERE server_id = __server_id
					AND server_status_id <> 0; -- already set to stopped with this cursor

				SELECT ROW_COUNT() INTO __servers_updated;

				IF __servers_updated <> 0 THEN
					CALL logging.logMessage( 'developer.remove_dead_3dapp_gameservers', 'INFO', concat('Removing dead 3dapp server ', cast( __server_id as char ) ) );
				END IF;

				-- DEAD_SERVER_DETECT = 13;
				CALL developer.kepauth_logout( __user_id, 13, __server_id, __actual_ip_address);

			 END IF;
			
		UNTIL __done END REPEAT;
		
	CLOSE __servers;

	CALL logging.flushLog();

	COMMIT;
  	
END
;;
DELIMITER ;