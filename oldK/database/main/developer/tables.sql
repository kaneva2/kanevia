-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `achievements` (
  `achievement_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `community_id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(100) NOT NULL,
  `points` int(10) unsigned NOT NULL,
  `image_url` varchar(200) NOT NULL,
  PRIMARY KEY (`achievement_id`),
  KEY `community_id` (`community_id`)
) ENGINE=InnoDB COMMENT='Holds achievements';

CREATE TABLE `achievements_earned` (
  `achievement_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`achievement_id`,`user_id`),
  CONSTRAINT `Refach_id` FOREIGN KEY (`achievement_id`) REFERENCES `achievements` (`achievement_id`)
) ENGINE=InnoDB COMMENT='Holds achievements earned';

CREATE TABLE `active_logins` (
  `user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `server_id` int(11) NOT NULL COMMENT 'FK to developer.game_servers',
  `game_id` int(11) NOT NULL COMMENT 'FK to developer.games',
  `created_date` datetime NOT NULL,
  `ip_address` varchar(30) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB COMMENT='actively logged on users';

CREATE TABLE `all_ports` (
  `port` int(11) NOT NULL,
  PRIMARY KEY (`port`)
) ENGINE=InnoDB COMMENT='Reference table to determine available ports';

CREATE TABLE `audit_games` (
  `game_id` int(10) unsigned NOT NULL,
  `owner_id` int(10) unsigned NOT NULL DEFAULT '0',
  `game_rating_id` smallint(5) unsigned NOT NULL DEFAULT '1',
  `game_status_id` smallint(5) unsigned NOT NULL DEFAULT '1',
  `game_name` varchar(100) NOT NULL DEFAULT '',
  `game_description` text,
  `game_synopsis` varchar(50) DEFAULT NULL,
  `game_creation_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `game_access_id` varchar(45) NOT NULL DEFAULT '1',
  `game_image_path` varchar(255) DEFAULT NULL,
  `game_keywords` text,
  `last_updated` datetime DEFAULT NULL,
  `modifiers_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `operation` varchar(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB;

CREATE TABLE `audit_game_licenses` (
  `game_license_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to games',
  `license_subscription_id` int(11) DEFAULT NULL COMMENT 'FK to game_license_subcription, trial/consumer/pro',
  `game_key` varchar(100) NOT NULL DEFAULT '' COMMENT 'used by all servers for game',
  `start_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `end_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `purchase_date` datetime DEFAULT NULL,
  `license_status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_license_status active, etc.',
  `max_game_users` int(11) NOT NULL DEFAULT '2000' COMMENT 'max for entire game, across servers',
  `modifiers_id` int(10) unsigned NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `operation` varchar(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB;

CREATE TABLE `audit_game_servers` (
  `server_id` int(11) NOT NULL,
  `server_name` varchar(100) NOT NULL COMMENT 'hostname of server, preconfigured, must match when pinging',
  `game_id` int(11) NOT NULL COMMENT 'FK to games',
  `visibility_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_server_visibility, public, etc.',
  `server_started_date` datetime DEFAULT NULL COMMENT 'when last started',
  `ip_address` varchar(50) NOT NULL DEFAULT '' COMMENT 'external IP used by clients',
  `port` int(11) NOT NULL DEFAULT '0' COMMENT 'port used by client',
  `server_status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_server_status, running, etc.',
  `number_of_players` int(11) DEFAULT NULL COMMENT 'current player count',
  `max_players` int(11) NOT NULL DEFAULT '0' COMMENT 'as configured on server',
  `last_ping_datetime` datetime DEFAULT NULL,
  `server_type_id` int(11) NOT NULL,
  `protocol_version` int(11) NOT NULL DEFAULT '0' COMMENT 'client-server protocol reported by server',
  `schema_version` int(11) NOT NULL DEFAULT '0' COMMENT 'server''s schema version',
  `server_engine_version` varchar(30) NOT NULL DEFAULT '' COMMENT 'file version of serverengine.dll',
  `server_engine_admin_only` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'is server is admin mode',
  `modifiers_id` int(10) unsigned NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `operation` varchar(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB;

CREATE TABLE `child_game_webcall_values` (
  `url_value` varchar(150) NOT NULL COMMENT 'value compared with each child game web call',
  PRIMARY KEY (`url_value`)
) ENGINE=InnoDB COMMENT='strings compared against child game web calls';

CREATE TABLE `client_platforms` (
  `client_platform_id` int(10) unsigned NOT NULL DEFAULT '0',
  `client_platform` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`client_platform_id`)
) ENGINE=InnoDB;

CREATE TABLE `company_developers` (
  `company_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `role_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`company_id`,`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `developer_emails` (
  `email` varchar(100) NOT NULL DEFAULT '',
  `signup_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  UNIQUE KEY `uc_unique` (`email`)
) ENGINE=InnoDB;

CREATE TABLE `development_companies` (
  `company_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `contacts_firstname` varchar(50) NOT NULL DEFAULT '',
  `contacts_lastname` varchar(50) NOT NULL DEFAULT '',
  `contacts_email` varchar(100) NOT NULL DEFAULT '',
  `contacts_phone` varchar(20) NOT NULL DEFAULT '',
  `company_name` varchar(70) NOT NULL DEFAULT '',
  `company_addressI` varchar(80) NOT NULL DEFAULT '',
  `company_addressII` varchar(80) DEFAULT NULL,
  `city` varchar(50) NOT NULL DEFAULT '',
  `state_code` char(2) NOT NULL DEFAULT 'GA',
  `province` varchar(50) NOT NULL DEFAULT '',
  `country_id` char(2) NOT NULL DEFAULT 'US',
  `zip_code` varchar(13) NOT NULL DEFAULT '',
  `website_URL` varchar(100) DEFAULT NULL,
  `annual_revenue` int(10) unsigned NOT NULL DEFAULT '0',
  `vworld_involvment_id` smallint(5) unsigned NOT NULL DEFAULT '1',
  `project_name` varchar(45) NOT NULL DEFAULT '',
  `project_description` varchar(200) DEFAULT NULL,
  `project_startdate_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `budget` int(10) unsigned NOT NULL DEFAULT '0',
  `client_platform_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `project_status_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `current_team_size` smallint(5) unsigned NOT NULL DEFAULT '0',
  `projected_team_size` smallint(5) unsigned NOT NULL DEFAULT '0',
  `date_submitted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `approved` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `authorized_tester` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`company_id`),
  KEY `date_submitted_idx` (`date_submitted`)
) ENGINE=InnoDB;

CREATE TABLE `download_log` (
  `download_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'simple auto number for unquie identifier ',
  `kaneva_user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'user id from kaneva table',
  `item_downloaded` varchar(100) NOT NULL COMMENT 'string identifying what was downloaded',
  `download_date` datetime NOT NULL COMMENT 'date and time the item was downloaded',
  PRIMARY KEY (`download_id`)
) ENGINE=InnoDB;

CREATE TABLE `faux_3d_app` (
  `game_id` int(11) NOT NULL DEFAULT '0',
  `zone_index` int(11) NOT NULL DEFAULT '0',
  `game_name` char(32) DEFAULT NULL,
  PRIMARY KEY (`game_id`,`zone_index`)
) ENGINE=InnoDB;

CREATE TABLE `games` (
  `game_id` int(10) NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) unsigned NOT NULL DEFAULT '0',
  `game_rating_id` smallint(5) unsigned NOT NULL DEFAULT '1',
  `game_status_id` smallint(5) unsigned NOT NULL DEFAULT '1',
  `game_name` varchar(100) NOT NULL DEFAULT '',
  `parent_game_id` int(11) NOT NULL DEFAULT '0' COMMENT 'if non-zero, this is a sub-game of the parent',
  `game_description` text,
  `game_synopsis` varchar(50) DEFAULT NULL,
  `game_creation_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `game_access_id` varchar(45) NOT NULL DEFAULT '1',
  `game_image_path` varchar(255) DEFAULT NULL,
  `game_keywords` text,
  `game_encryption_key` varchar(16) DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  `modifiers_id` int(10) unsigned NOT NULL,
  `ga_account` varchar(26) DEFAULT NULL COMMENT 'account id used to report to google analytics from the client',
  `incubator_hosted` smallint(1) NOT NULL DEFAULT '0' COMMENT 'Incubator-hosted flag to identify internally hosted apps from others',
  PRIMARY KEY (`game_id`),
  UNIQUE KEY `idx_name` (`game_name`),
  KEY `idx_creationDate` (`game_creation_date`),
  KEY `idx_owner` (`owner_id`),
  KEY `idx_rating` (`game_rating_id`),
  KEY `idx_status` (`game_status_id`)
) ENGINE=InnoDB;

CREATE TABLE `games_to_hosting_status` (
  `game_id` int(10) NOT NULL DEFAULT '0' COMMENT 'id of the game (FK to developer.games)',
  `hosting_status_id` int(10) NOT NULL DEFAULT '0' COMMENT 'id of the hosting status (FK to developer.hosting_status)',
  PRIMARY KEY (`game_id`,`hosting_status_id`),
  KEY `fk_GameId` (`game_id`),
  KEY `fk_HostingStatusId` (`hosting_status_id`),
  CONSTRAINT `fk_GameId2` FOREIGN KEY (`game_id`) REFERENCES `games` (`game_id`),
  CONSTRAINT `fk_HostingStatusId` FOREIGN KEY (`hosting_status_id`) REFERENCES `hosting_status` (`hosting_status_id`)
) ENGINE=InnoDB COMMENT='stores relationship between a game and hosting_status';

CREATE TABLE `game_access` (
  `game_access_id` smallint(5) unsigned NOT NULL,
  `game_access` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`game_access_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_balances` (
  `game_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ID of game within games table',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'KPOINT' COMMENT 'Type of currency KPOINT = Credits',
  `balance` decimal(12,3) NOT NULL DEFAULT '0.000' COMMENT 'Current balance of game',
  PRIMARY KEY (`game_id`,`kei_point_id`),
  KEY `KID` (`kei_point_id`)
) ENGINE=InnoDB COMMENT='Balances for all games';

CREATE TABLE `game_comments` (
  `game_comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `game_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_comment_id` int(10) unsigned NOT NULL DEFAULT '0',
  `reply_to_comment_id` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  `ip_address` varchar(50) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`game_comment_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_gifted_items` (
  `game_id` int(10) NOT NULL COMMENT 'id of the game that gifted the item (FK to developer.games)',
  `user_id` int(11) NOT NULL COMMENT 'id of the user that was gifted the item (FK to kaneva.users)',
  `global_id` int(11) NOT NULL COMMENT 'id of the item gifted to the user (FK to wok.items)',
  `created_date` datetime DEFAULT NULL COMMENT 'datetime that item was gifted',
  KEY `USER_ID_FK` (`user_id`),
  KEY `GLOBAL_ID_FK` (`global_id`),
  KEY `game_id` (`game_id`,`user_id`),
  CONSTRAINT `GAME_ID_FK` FOREIGN KEY (`game_id`) REFERENCES `games` (`game_id`),
  CONSTRAINT `GLOBAL_ID_FK` FOREIGN KEY (`global_id`) REFERENCES `wok`.`items` (`global_id`),
  CONSTRAINT `USER_ID_FK` FOREIGN KEY (`user_id`) REFERENCES `kaneva`.`users` (`user_id`)
) ENGINE=InnoDB COMMENT='stores lists of items that games have gifted to users';

CREATE TABLE `game_licenses` (
  `game_license_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to games',
  `license_subscription_id` int(11) DEFAULT NULL COMMENT 'FK to game_license_subcription, trial/consumer/pro',
  `game_key` varchar(100) NOT NULL DEFAULT '' COMMENT 'used by all servers for game',
  `start_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `end_date` datetime DEFAULT NULL COMMENT 'for tracking trials',
  `purchase_date` datetime DEFAULT NULL,
  `license_status_id` int(11) NOT NULL DEFAULT '1' COMMENT 'FK to game_license_status active, etc.',
  `max_game_users` int(11) NOT NULL DEFAULT '2000' COMMENT 'max for entire game, across servers',
  `modifiers_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`game_license_id`),
  UNIQUE KEY `INDEX_game_id` (`game_id`),
  KEY `INDEX_game_key` (`game_key`)
) ENGINE=InnoDB COMMENT='track extra data for a game';

CREATE TABLE `game_license_status` (
  `license_status_id` smallint(5) unsigned NOT NULL,
  `license_status` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`license_status_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_license_subscriptions` (
  `license_subscription_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `length_of_subscription` int(11) NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `amount_kei_point_id` varchar(50) NOT NULL DEFAULT '',
  `max_server_users` int(11) DEFAULT NULL,
  `disk_quota` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`license_subscription_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_login_log` (
  `game_login_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `logon_time` datetime NOT NULL COMMENT 'time of login',
  PRIMARY KEY (`game_login_id`),
  KEY `IDX_logon_time` (`logon_time`)
) ENGINE=InnoDB COMMENT='track logins to all games (not just WOK)';

CREATE TABLE `game_pass_groups` (
  `game_id` int(11) NOT NULL COMMENT 'FK to games',
  `pass_group_id` int(11) NOT NULL COMMENT 'FK to wok.pass_groups',
  `set_by_admin` char(1) NOT NULL DEFAULT 'N' COMMENT 'Indicates whether or not the pass group was set by a site administrator.',
  PRIMARY KEY (`game_id`,`pass_group_id`)
) ENGINE=InnoDB COMMENT='maintains the list of pass groups a game has';

CREATE TABLE `game_patcher_settings` (
  `game_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to games',
  `pane1_url` varchar(255) NOT NULL DEFAULT '',
  `pane2_url` varchar(255) NOT NULL DEFAULT '',
  `pane1_title` varchar(255) DEFAULT NULL,
  `pane2_title` varchar(255) DEFAULT NULL,
  `version_text` varchar(20) DEFAULT NULL,
  `background_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB COMMENT='patcher customizations';

CREATE TABLE `game_patch_urls` (
  `patchURL_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patch_id` int(10) unsigned NOT NULL DEFAULT '1',
  `game_id` int(10) unsigned NOT NULL DEFAULT '0',
  `patch_url` varchar(255) NOT NULL DEFAULT '',
  `patch_type` enum('','WOK','TEMPLATE','WORLD') NOT NULL DEFAULT '' COMMENT 'Patch type. Empty if legacy patch URLs',
  `client_path` varchar(100) NOT NULL DEFAULT '' COMMENT 'Parent folder for all the paths specified in versioninfo.dat',
  PRIMARY KEY (`patchURL_id`),
  KEY `urls_game_id` (`game_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_ratings` (
  `game_rating_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `game_rating` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `minimum_age` smallint(5) unsigned NOT NULL DEFAULT '14',
  `is_mature` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`game_rating_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_raves` (
  `rave_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `game_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rave_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`rave_id`),
  KEY `idx_uniqueRave` (`game_id`,`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_roles` (
  `user_id` int(11) NOT NULL COMMENT 'FK to users',
  `game_id` int(11) NOT NULL COMMENT 'FK to games',
  `game_role_id` smallint(6) NOT NULL COMMENT 'FK to game_roles',
  PRIMARY KEY (`user_id`,`game_id`)
) ENGINE=InnoDB COMMENT='track user permissions for a star except owners';

CREATE TABLE `game_role_ids` (
  `game_role_id` smallint(6) NOT NULL,
  `game_role` varchar(50) DEFAULT NULL COMMENT 'descriptive name',
  PRIMARY KEY (`game_role_id`)
) ENGINE=InnoDB COMMENT='lookup table for asset_roles';

CREATE TABLE `game_servers` (
  `server_id` int(11) NOT NULL AUTO_INCREMENT,
  `server_name` varchar(100) NOT NULL COMMENT 'hostname of server, preconfigured, must match when pinging',
  `game_id` int(11) NOT NULL COMMENT 'FK to games',
  `visibility_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_server_visibility, 1=public, 2=private',
  `is_external` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'denotes whether server is kaneva hosted, prevents event jabbers',
  `server_started_date` datetime DEFAULT NULL COMMENT 'when last started',
  `ip_address` varchar(50) NOT NULL DEFAULT '0' COMMENT 'external IP used by clients',
  `port` int(11) NOT NULL DEFAULT '0' COMMENT 'port used by client',
  `actual_ip_address` varchar(50) NOT NULL COMMENT 'ip address we detected',
  `server_status_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_server_status, 1=running, etc.',
  `number_of_players` int(11) DEFAULT NULL COMMENT 'current player count',
  `max_players` int(11) DEFAULT '2000',
  `affinity_mask` int(11) NOT NULL DEFAULT '255' COMMENT 'bit mask of allowed wok.zone_type_ids',
  `last_ping_datetime` datetime DEFAULT NULL,
  `server_type_id` int(11) NOT NULL DEFAULT '1' COMMENT 'FK to game_server_types: 1=game server, 7=script server, etc. ',
  `protocol_version` int(11) NOT NULL DEFAULT '0' COMMENT 'client-server protocol reported by server',
  `schema_version` int(11) NOT NULL DEFAULT '0' COMMENT 'server''s schema version',
  `asset_version` int(11) NOT NULL DEFAULT '0' COMMENT 'packed version from versioninfo.dat',
  `server_engine_version` varchar(30) NOT NULL DEFAULT '' COMMENT 'file version of serverengine.dll',
  `server_engine_admin_only` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'is server is admin mode',
  `modifiers_id` int(10) unsigned NOT NULL COMMENT 'kaneva user id who last modified this via website',
  `game_play_server` enum('F','T','A') NOT NULL DEFAULT 'A' COMMENT 'Server hosts zones with game play only, or A for all',
  PRIMARY KEY (`server_id`),
  KEY `INDEX_server_name` (`server_name`),
  KEY `fast_summary` (`game_id`,`server_status_id`,`last_ping_datetime`),
  KEY `INDEX_server_name_port` (`server_name`,`port`),
  KEY `idx_actual_ip_address` (`actual_ip_address`)
) ENGINE=InnoDB COMMENT='configure and track servers for a game';

CREATE TABLE `game_server_history` (
  `server_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `server_id` int(11) NOT NULL DEFAULT '0',
  `player_count` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_address` varchar(20) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  PRIMARY KEY (`server_history_id`),
  KEY `FK_server_history_current_game_engines` (`server_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_server_status` (
  `server_status_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`server_status_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_server_types` (
  `server_type_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`server_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_server_visibility` (
  `visibility_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`visibility_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_stats` (
  `game_id` int(10) unsigned NOT NULL DEFAULT '0',
  `number_of_comments` int(10) unsigned NOT NULL DEFAULT '0',
  `number_of_raves` int(10) unsigned NOT NULL DEFAULT '0',
  `number_of_visits` int(10) unsigned NOT NULL DEFAULT '0',
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `number_of_players` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`game_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_status` (
  `game_status_id` smallint(5) unsigned NOT NULL,
  `game_status` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`game_status_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_views` (
  `view_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `game_id` int(10) unsigned NOT NULL DEFAULT '0',
  `view_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_address` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`view_id`),
  UNIQUE KEY `idx_uniqueView` (`user_id`,`game_id`)
) ENGINE=InnoDB;

CREATE TABLE `hosting_status` (
  `hosting_status_id` int(10) NOT NULL COMMENT 'key',
  `hosting_status` varchar(30) DEFAULT NULL COMMENT 'description',
  `hosting_status_user_message` varchar(128) DEFAULT NULL COMMENT 'message displayed to user',
  PRIMARY KEY (`hosting_status_id`)
) ENGINE=InnoDB COMMENT='Allows control over app login process ';

CREATE TABLE `incubators` (
  `guid` char(36) NOT NULL COMMENT 'Global Id NOTA BENE: NOT A GLID!',
  `tid` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tuple id',
  `parent_game_id` int(10) unsigned DEFAULT NULL COMMENT 'Game to which this incubator is associated',
  `dflt_world_max_pop` int(10) unsigned NOT NULL DEFAULT '2000' COMMENT 'Default max pop for any given world identified by game_id',
  PRIMARY KEY (`guid`),
  KEY `tid` (`tid`)
) ENGINE=InnoDB;

CREATE TABLE `incubator_hosts` (
  `parent_game_id` int(11) NOT NULL DEFAULT '0',
  `host_name` varchar(100) NOT NULL DEFAULT '',
  `port_start` int(11) DEFAULT NULL,
  `port_end` int(11) DEFAULT NULL,
  `incubator_guid` char(64) NOT NULL,
  `incubator_tid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`parent_game_id`,`host_name`),
  KEY `incubator_hosts_ibfk_1` (`incubator_guid`),
  CONSTRAINT `incubator_hosts_ibfk_1` FOREIGN KEY (`incubator_guid`) REFERENCES `incubators` (`guid`) ON DELETE CASCADE
) ENGINE=InnoDB COMMENT='Incubator hosts metadata';

CREATE TABLE `invitations` (
  `invitations_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto number id',
  `validation_key` varchar(50) NOT NULL DEFAULT '' COMMENT 'unique key used to retrieve id',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT 'email of the invited user',
  `company_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'id of the inviting company ',
  `invitation_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'date of invite',
  `message` varchar(300) NOT NULL DEFAULT '' COMMENT 'message to invitee',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT 'display name of invitee',
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'preassigned role id of invitee',
  PRIMARY KEY (`invitations_id`)
) ENGINE=InnoDB COMMENT='stores invitation to be a developer';

CREATE TABLE `jason_temp` (
  `game_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  PRIMARY KEY (`game_id`)
) ENGINE=InnoDB;

CREATE TABLE `kim_login_log` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `created_date` datetime NOT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_reason_code` enum('NORMAL','LOST','TERMINATED','FORCE','TIMEOUT','BANNED','SECURITY_VIO','COMM_ASSIGN','UNKNOWN_NET','SERVER_SHUTDOWN','SERVER_RESTART','PLAYER_ZONED','DEAD_SERVER_DETECT') DEFAULT NULL,
  `server_name` varchar(100) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`login_id`),
  KEY `cd` (`created_date`),
  KEY `login_log_ld` (`logout_date`),
  KEY `uid` (`user_id`)
) ENGINE=InnoDB COMMENT='log of all logins for KIM';

CREATE TABLE `leaderboards` (
  `community_id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `sort_by` varchar(10) NOT NULL,
  `format_for_value` smallint(6) NOT NULL,
  `column_name` varchar(45) NOT NULL,
  PRIMARY KEY (`community_id`)
) ENGINE=InnoDB COMMENT='Holds leaderboards';

CREATE TABLE `leaderboards_values` (
  `community_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `current_value` int(11) NOT NULL DEFAULT '0',
  `changed_date` datetime NOT NULL,
  PRIMARY KEY (`community_id`,`user_id`),
  CONSTRAINT `Refcommunity_id` FOREIGN KEY (`community_id`) REFERENCES `leaderboards` (`community_id`)
) ENGINE=InnoDB COMMENT='Holds leaderboard values';

CREATE TABLE `levels_values` (
  `community_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `level_number` int(10) unsigned NOT NULL,
  `percent_complete` int(10) unsigned NOT NULL,
  `changed_date` datetime NOT NULL,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`community_id`,`user_id`)
) ENGINE=InnoDB COMMENT='Holds level values';

CREATE TABLE `login_log` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `game_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_reason_code` enum('NORMAL','LOST','TERMINATED','FORCE','TIMEOUT','BANNED','SECURITY_VIO','COMM_ASSIGN','UNKNOWN_NET','SERVER_SHUTDOWN','SERVER_RESTART','PLAYER_ZONED','DEAD_SERVER_DETECT') DEFAULT NULL,
  `server_id` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`login_id`),
  KEY `cd` (`created_date`),
  KEY `login_log_ld` (`logout_date`),
  KEY `uid` (`user_id`),
  KEY `gid` (`game_id`)
) ENGINE=InnoDB COMMENT='log of all logins for all games';

CREATE TABLE `maintenance_log` (
  `maintenance_log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_occurred` datetime NOT NULL,
  `database_name` varchar(50) DEFAULT NULL,
  `procedure_name` varchar(200) DEFAULT NULL,
  `message_text` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`maintenance_log_id`)
) ENGINE=InnoDB;

CREATE TABLE `player_game_allowed` (
  `kaneva_user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `game_id` int(11) NOT NULL COMMENT 'FK to games',
  `first_visit_date` datetime NOT NULL DEFAULT '2010-01-01 00:00:00' COMMENT 'when first visited',
  PRIMARY KEY (`kaneva_user_id`,`game_id`),
  KEY `IDX_first_visit` (`first_visit_date`)
) ENGINE=InnoDB COMMENT='track allowed games for players.';

CREATE TABLE `project_startdates` (
  `project_startdate_id` int(10) unsigned NOT NULL DEFAULT '0',
  `project_startdate` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`project_startdate_id`)
) ENGINE=InnoDB;

CREATE TABLE `project_status` (
  `project_status_id` int(10) unsigned NOT NULL DEFAULT '0',
  `project_status` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`project_status_id`)
) ENGINE=InnoDB;

CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT;

CREATE TABLE `server_affinity_flags` (
  `affinity_flag` int(11) NOT NULL COMMENT 'must be mutually exclusive bits',
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB COMMENT='fs for affinity_flag in wok.zone_weights and game_servers';

CREATE TABLE `visit_log` (
  `visit_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_type` tinyint(4) NOT NULL DEFAULT '0',
  `zone_index` int(11) NOT NULL DEFAULT '0',
  `zone_instance_id` int(11) NOT NULL DEFAULT '0',
  `game_id` int(11) NOT NULL DEFAULT '0',
  `kaneva_user_id` int(11) NOT NULL DEFAULT '0',
  `visited_at` datetime NOT NULL DEFAULT '2010-01-01 00:00:00',
  `visit_ended_at` datetime DEFAULT NULL,
  `orphaned` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`visit_log_id`),
  KEY `IDX_zone1` (`zone_index`),
  KEY `IDX_zone2` (`zone_instance_id`),
  KEY `IDX_game` (`game_id`),
  KEY `IDX_visited_at` (`visited_at`),
  KEY `IDX_user_visit_end` (`kaneva_user_id`,`visit_ended_at`)
) ENGINE=InnoDB;

CREATE TABLE `vworld_involvement` (
  `vworld_involvement_id` int(10) unsigned NOT NULL DEFAULT '0',
  `vworld_involvement` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`vworld_involvement_id`)
) ENGINE=InnoDB;

