-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS remove_game_servers;

DELIMITER ;;
CREATE PROCEDURE `remove_game_servers`(__server_id	INT(11))
BEGIN

START TRANSACTION;
INSERT into developer.audit_game_servers
    (	server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, number_of_players,
    	max_players, last_ping_datetime, server_type_id, modifiers_id, date_modified, operation )
    SELECT
    	server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, number_of_players,
    	max_players, last_ping_datetime, server_type_id, modifiers_id, Now(), 'delete' 
		FROM game_servers 
			WHERE server_id =  __server_id;

DELETE FROM game_servers 
			WHERE server_id =  __server_id;

COMMIT;
END
;;
DELIMITER ;