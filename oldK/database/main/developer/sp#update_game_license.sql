-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_game_license;

DELIMITER ;;
CREATE PROCEDURE `update_game_license`(gameLicenseId INTEGER SIGNED, gameId INTEGER SIGNED, licenseSubscriptionId INTEGER SIGNED, gameKey VARCHAR(100),
            startDate DATETIME, endDate DATETIME, purchaseDate DATETIME, licenseStatusId INTEGER SIGNED,
            maxGameUsers INTEGER SIGNED, modifiersId INTEGER UNSIGNED)
BEGIN


  START TRANSACTION;
	
    
    UPDATE 
    	game_licenses 
    SET 
    	game_id = gameId, 
    	license_subscription_id = licenseSubscriptionId, 
    	game_key = gameKey, 
    	start_date = startDate, 
    	end_date = endDate, 
    	purchase_date = purchaseDate, 
    	license_status_id = licenseStatusId, 
    	max_game_users = maxGameUsers, 
    	modifiers_id = modifiersId
    WHERE 
    	game_license_id = gameLicenseId;


    
   SELECT row_count();	
	
  COMMIT;

END
;;
DELIMITER ;