-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_game_server;

DELIMITER ;;
CREATE PROCEDURE `add_game_server`( IN __user_id INT(11), IN __key VARCHAR(100), IN __game_id INT(11), 
								  IN __host VARCHAR(50), IN __port INT(11), IN __patch_url VARCHAR(255),
								  IN __extra_patch_url VARCHAR(255), IN __patch_type VARCHAR(20), IN __client_path VARCHAR(100),
								  OUT __rc INT, OUT __ip_port INT, OUT __server_id INT)
BEGIN
-- will return rc values of 
-- 0 = ok
-- 2 = key or game_id not found
-- 3 = not authorized or game not found
-- 5 = access denied (or user id not found)

-- will lookup game_id if key passed in and game_id is 0
-- host or patch_url can be empty and it will insert only things that 
-- are not empty
	DECLARE __id INT(11) DEFAULT NULL;
 	DECLARE __existing_port INT(11) DEFAULT NULL;
	DECLARE __parent_game_id INT(11) DEFAULT 0;
	DECLARE __incubator_hosted SMALLINT(1) DEFAULT 0;
	DECLARE __port_start INT(11) DEFAULT NULL;
	DECLARE __port_end INT(11) DEFAULT NULL;

	
	-- TODO add more security checks 
	-- right now C# does none
	SET __ip_port = 0;
	SET __rc = 5;
	SET __server_id = 0;
	
	
	-- get game_id if not passed in
	IF __game_id = 0 THEN
		SELECT game_id INTO __game_id 
		  FROM game_licenses 
		 WHERE game_key = __key;
	END IF;

	IF __game_id IS NOT NULL THEN 

		-- get incubator_hosted flag
		SELECT parent_game_id, incubator_hosted INTO __parent_game_id, __incubator_hosted
		FROM games 
		WHERE game_id=__game_id;
		
		-- must be administrator
		SELECT account_type_id INTO __id
		FROM developer.vw_game_privileges
		WHERE user_id = __user_id
		 AND game_id = __game_id
		 AND account_type_id = 1; 
		
		IF __id IS NOT NULL THEN
	
			
			IF LENGTH(__host) > 0 THEN 
				SET __id = NULL;
				SELECT server_id INTO __id 
				  FROM game_servers
				 WHERE server_name = __host AND port = __port AND game_id = __game_id;

				IF __id IS NULL THEN 

					-- No previous record found matching input __host/__port combination for the requested 3dapp
					-- Assign a new port number and insert it into game_servers

					IF __incubator_hosted = 0 THEN

						-- regular game servers

						SET __existing_port = NULL;
 						SELECT MAX(port) INTO __existing_port 
						  FROM game_servers
						 WHERE server_name = __host;
         
						IF __existing_port IS NOT NULL THEN
						  -- Known server: assign a port by auto-increment. Disregard the port number parameter.
							SET __port = __existing_port + 1;
							SET __ip_port = __existing_port + 1;
						ELSE
							-- New server, assign by the port number parameter.
							SET __ip_port = __port;
						END IF;

					ELSE
						SELECT port_start, port_end INTO __port_start, __port_end
						FROM incubators i join incubator_hosts ih ON i.guid = ih.incubator_guid
						WHERE i.parent_game_id = __parent_game_id AND ih.host_name = __host;

						IF __port_start IS NULL OR __port_end IS NULL THEN
							-- if not defined, try catch-all settings
							SELECT port_start, port_end INTO __port_start, __port_end
							FROM incubators i join incubator_hosts ih ON i.guid = ih.incubator_guid
							WHERE i.parent_game_id = __parent_game_id AND ih.host_name = '*';
						END IF;

						-- delete existing server records
						DELETE FROM game_servers WHERE game_id=__game_id AND server_name=__host;

						-- allocate random free port
						SELECT ap.port INTO __ip_port
						FROM all_ports ap 
						LEFT OUTER JOIN game_servers gs ON gs.server_name=__host AND ap.port=gs.port
						WHERE gs.port IS NULL 
						  AND (ap.port>=__port_start OR __port_start IS NULL) 
						  AND (ap.port<=__port_end OR __port_end IS NULL)
						ORDER BY rand()
						LIMIT 1;

						IF __ip_port = NULL THEN
							SET __ip_port = 0;
						END IF;

						SET __port = __ip_port;

					END IF;

					IF __port<>0 THEN
						-- instead of INSERT call SP to insert, which also audits
						SET __server_id = 0;
						CALL add_game_servers(__host,
						                      __game_id, 
						                      1,             -- __visibility_id 1 = public 		
						                      NOW(),         -- __server_started_date          	
						                      '',            -- __ip_address
						                      __port,
						                      0,             -- __number_of_players
						                      0,             -- __unused
						                      NOW(),         -- __last_ping_datetime
						                      __user_id,
						                      __server_id ); -- INOUT __server_id
					END IF;

				ELSE
					-- Existing game_servers record found at [__host:__port] for this 3dapp
					-- no insertion, just return existing port
					SET __ip_port = __port;
					SET __server_id = __id;
				END IF;
			END IF;
	
			
			-- setting the URL?
			IF LENGTH(__patch_url) > 0 THEN
				SET __id = NULL;
				IF __patch_type = '' THEN
					-- Old behavior: check if URL exists with empty patch type
					SELECT patchURL_id INTO __id 
					  FROM game_patch_urls 
					 WHERE patch_url = __patch_url AND game_id = __game_id AND patch_type = '';
				ELSE
					-- Only allow one URL per patch_type / game_id
					SELECT patchURL_id INTO __id 
					  FROM game_patch_urls 
					 WHERE game_id = __game_id AND patch_type = __patch_type;
				END IF;

				IF __id IS NULL THEN 
					-- Insert a new one
					INSERT INTO game_patch_urls
						(patch_id, game_id, patch_url, patch_type, client_path) 
					VALUES (1, __game_id, __patch_url, __patch_type, __client_path);
				ELSE
					-- Modify existing
					UPDATE game_patch_urls
					   SET patch_url=__patch_url, client_path=__client_path
					 WHERE patchURL_id=__id;
				END IF;
			END IF;

			-- keep extra_patch_url for old style request only
			IF __patch_type = '' AND LENGTH(__extra_patch_url) > 0 THEN
				SET __id = NULL;
				SELECT patchURL_id INTO __id 
				  FROM game_patch_urls 
				 WHERE patch_url = __extra_patch_url AND game_id = __game_id AND patch_type = '';

				IF __id IS NULL THEN 
					INSERT INTO game_patch_urls
						(patch_id, game_id, patch_url, patch_type, client_path) 
					VALUES (1, __game_id, __extra_patch_url, __patch_type, __client_path);
				END IF;
			END IF;
		
			SET __rc = 0; -- ok  
			
		ELSE
			SET __rc = 3; -- not authorized or game not found 
		END IF;
	ELSE 		
			SET __rc = 2; -- key not found 
	END IF;                

END
;;
DELIMITER ;