-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS process_wok_peak_concurrent_players;

DELIMITER ;;
CREATE PROCEDURE `process_wok_peak_concurrent_players`( )
BEGIN

DECLARE _wokCount INT;

SELECT count(*) into _wokCount
FROM wok.players p  
INNER JOIN wok.player_zones pz ON p.player_id = pz.player_id 
                    INNER JOIN wok.supported_worlds sw ON wok.zoneIndex(pz.current_zone_index) = sw.zone_index 
                     INNER JOIN wok.channel_zones cz ON pz.current_zone_index = cz.zone_index and pz.current_zone_instance_id = cz.zone_instance_id 
                     LEFT OUTER JOIN developer.active_logins dal ON dal.user_id=p.kaneva_user_id
                     INNER JOIN developer.games dg ON dg.game_id=dal.game_id 
                     WHERE dal.user_id = p.kaneva_user_id AND ( ((wok.is_wok_game(dal.game_id)= 1) AND p.in_game = 'T') 
                     OR ((wok.is_wok_game(dal.game_id) = 0) AND p.in_game = 'F') );

CALL wok.update_peak_concurrent_players(_wokCount);

END
;;
DELIMITER ;