-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_communities_delete;

DELIMITER ;;
CREATE PROCEDURE `update_communities_delete`(
IN __req_community_id INT, 
IN __req_status_id INT, 
IN __req_mode VARCHAR(32)
)
    COMMENT '\r\n    Arguments\r\n     __req_community_id:    \r\n     __req_status_id:       \r\n     __req_mode:          gameonly | communityOnly\r\n  '
this_proc:
BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
      -- ERROR
      GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
      CALL logging.logMsg('ERROR', SCHEMA(), 'update_communities_delete', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
      RESIGNAL;
  END;

  CALL logging.logMsg('TRACE', SCHEMA(), 'update_communities_delete', CONCAT('BEGIN (__req_community_id[',CAST(__req_community_id AS CHAR),'] __req_status_id[',CAST(__req_status_id AS CHAR),'], __req_mode[',__req_mode,'])'));

  -- BEGIN Validate Args
  SELECT 
    `status_id`, `creator_id` INTO  @currentStatusId, @kanevaUserId
  FROM 
    `communities` 
  WHERE 
    `community_id` = __req_community_id;

  -- only proceed if the currect status is different then the requested
  IF (__req_status_id = @currentStatusId) THEN
    CALL logging.logMsg('WARN', SCHEMA(), 'update_communities_delete', CONCAT('__req_community_id[',CAST(__req_community_id AS CHAR), '] already in __req_status_id[',CAST(__req_status_id AS CHAR),'] -- all done'));
    LEAVE this_proc;
  END IF;
  --
  -- END Validate Args


  -- Actual logic
  -- 0) update status on community record
  UPDATE `communities`
    SET 
      `status_id`   = __req_status_id, 
      `last_update` = NOW()
    WHERE 
      `community_id` = __req_community_id;

  -- 1 if deleteing 
    IF __req_status_id = 3 THEN

    -- 1a) replenish inventory
    CALL logging.logMsg('TRACE', SCHEMA(), 'update_communities_delete', CONCAT('begin replenish wipa for __req_community_id[',CAST(__req_community_id AS CHAR),']'));

    INSERT INTO wok.inventory_pending_adds2 (`kaneva_user_id`, `global_id`, `inventory_type`, `inventory_sub_type`, `quantity`, `last_touch_datetime`) 
    (SELECT 
        @kanevaUserId, `global_id`, 'B', `inventory_sub_type`, @returnQty := COUNT(`global_id`) AS `returnQty`, NOW()
      FROM 
        wok.dynamic_objects
      WHERE 
        wok.zoneType(`zone_index`) = 6 AND
        `zone_instance_id` = __req_community_id AND 
        `expired_date`       IS NULL AND -- no try on items
        `inventory_sub_type` <> 1024     -- no unlimited items
      GROUP BY
        `global_id`, 
        `inventory_sub_type`
    )
    ON DUPLICATE KEY UPDATE `quantity` = `quantity` + @returnQty, `last_touch_datetime` = NOW();

    SELECT ROW_COUNT() INTO @numRecs;
    CALL logging.logMsg('TRACE', SCHEMA(), 'update_communities_delete', CONCAT('done replenish wipa for __req_community_id[',CAST(__req_community_id AS CHAR),'] with [', @numRecs ,'] changes'));


    -- 1a) delete all dynamic objects playlists
    CALL logging.logMsg('TRACE', SCHEMA(), 'update_communities_delete', 'delete dynamic_object_playlists');
    DELETE
      wok.dynamic_object_playlists.*
    FROM 
      wok.dynamic_object_playlists 
     LEFT JOIN wok.dynamic_object_parameters
        ON wok.dynamic_object_playlists.obj_placement_id = wok.dynamic_object_parameters.obj_placement_id
    WHERE
      wok.dynamic_object_parameters.obj_placement_id IS NULL AND
  		wok.dynamic_object_playlists.zone_instance_id = __req_community_id AND
  		wok.zoneType(wok.dynamic_object_playlists.zone_index) = 6;

	  -- 1b delete all dynamic objects (scripted + non scripted)
  	DELETE
  		wok.dynamic_objects.*, 
  		wok.dynamic_object_parameters.* 
  	FROM 
  		wok.dynamic_objects
  		LEFT JOIN wok.dynamic_object_parameters
  		ON wok.dynamic_object_parameters.obj_placement_id = wok.dynamic_objects.obj_placement_id
  	WHERE 
  		wok.dynamic_objects.zone_instance_id = __req_community_id AND
  		wok.zoneType(wok.dynamic_objects.zone_index) = 6;
  
  	IF (__req_mode <> 'communityOnly') THEN
      SELECT `game_id` INTO @game_id FROM kaneva.communities_game where community_id = __req_community_id;
      -- channel based worlds do not have game records
      IF (@game_id IS NOT NULL) THEN
  		  CALL developer.transition_game_access(@game_id, @kanevaUserId, 5, 'gameonly');
      END IF;
  	END IF;

	END IF;

  CALL logging.logMsg('TRACE', SCHEMA(), 'update_communities_delete', CONCAT('END (__req_community_id[',CAST(__req_community_id AS CHAR), '] __req_status_id[',CAST(__req_status_id AS CHAR),'])'));

END
;;
DELIMITER ;