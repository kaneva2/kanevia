-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS after_insert_game_rave;

DELIMITER ;;
CREATE TRIGGER `after_insert_game_rave` AFTER INSERT ON `game_raves` FOR EACH ROW BEGIN
  update game_stats set last_updated = Now(), number_of_raves = number_of_raves + 1 WHERE game_id = new.game_id;
END
;;
DELIMITER ;