-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_new_game_license;

DELIMITER ;;
CREATE PROCEDURE `add_new_game_license`(gameId INTEGER SIGNED, licenseSubscriptionId INTEGER SIGNED, gameKey VARCHAR(100),
            startDate DATETIME, endDate DATETIME, purchaseDate DATETIME, licenseStatusId INTEGER SIGNED,
            maxGameUsers INTEGER SIGNED, modifiersId INTEGER UNSIGNED, OUT __gameLicenseId INTEGER SIGNED)
BEGIN

    --  insert into table
    INSERT INTO game_licenses
    (
    	game_id, 
    	license_subscription_id, 
    	game_key, 
    	start_date, 
    	end_date, 
    	purchase_date, 
    	license_status_id, 
    	max_game_users, 
    	modifiers_id
    )
    VALUES 
    (
    	gameId, 
    	licenseSubscriptionId, 
    	gameKey, 
    	startDate, 
    	endDate, 
    	purchaseDate,
    	licenseStatusId, 
    	maxGameUsers, 
    	modifiersId
    );
    
    
    -- get the license id of the new game license
    SELECT LAST_INSERT_ID() INTO __gameLicenseId;
    
END
;;
DELIMITER ;