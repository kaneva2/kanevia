-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS apply_transaction_to_game_balance;

DELIMITER ;;
CREATE PROCEDURE `apply_transaction_to_game_balance`( IN game_id INT, 
                                                                IN transaction_amount FLOAT, 
							                                    IN v_kei_point_id VARCHAR(20), 
                                                                OUT return_code INT, 
                                                                OUT game_balance FLOAT)
BEGIN
	
	SET return_code = -1;
	SET game_balance = NULL;
	
	SELECT  gb.balance 
  INTO    game_balance 
	FROM    game_balances gb 
  WHERE   gb.game_id = game_id 
  AND     kei_point_id = v_kei_point_id;
	
	IF game_balance IS NOT NULL THEN
		IF (game_balance + transaction_amount) < 0 THEN 
			SET return_code = 1; 
		ELSE
			SET return_code = 0;
      
			UPDATE  game_balances gb 
      SET     gb.balance = gb.balance + transaction_amount 
      WHERE   gb.game_id = game_id 
      AND     gb.kei_point_id = v_kei_point_id;
		END IF;
	ELSE
		SET game_balance = 0;
		
		IF transaction_amount < 0 THEN
			SET return_code = 1; 
		ELSE
			SET return_code = 0;
			INSERT INTO game_balances ( game_id, kei_point_id, balance ) VALUES ( game_id, v_kei_point_id, transaction_amount );
		END IF;
	END IF;
  
	IF return_code = 0 THEN
		SET game_balance = game_balance + transaction_amount;
	END IF;
	
END
;;
DELIMITER ;