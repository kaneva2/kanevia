-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_game_servers_ping;

DELIMITER ;;
CREATE PROCEDURE `update_game_servers_ping`(__server_id INT(11),
	__number_of_players INT(11),
	__unused INT(11),
	__server_status_id INT(11),
	__server_engine_admin_only ENUM('T','F')
	)
BEGIN
	-- called by the older v3/v4 pingers
	UPDATE game_servers
		SET number_of_players = __number_of_players, 
        server_status_id = __server_status_id,
        last_ping_datetime = NOW(),
        server_engine_admin_only = __server_engine_admin_only,
				server_started_date = CASE WHEN __server_status_id = 3 THEN NOW() ELSE server_started_date END
    WHERE server_id = __server_id;

END
;;
DELIMITER ;