-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_game;

DELIMITER ;;
CREATE PROCEDURE `update_game`(
  gameId INTEGER SIGNED, 
  ownerId INTEGER UNSIGNED, 
  gameRatingId SMALLINT UNSIGNED, 
  gameStatusId SMALLINT UNSIGNED,
  gameName VARCHAR(100), 
  gameDescription TEXT, 
  gameSynopsis VARCHAR(50), 
  gameCreationDate DATETIME,
  gameAccessId VARCHAR(45), 
  gameImagePath VARCHAR(255), 
  gameKeywords TEXT, 
  gameEncryptionKey VARCHAR(16),
  lastUpdated DATETIME, 
  modifiersId INTEGER UNSIGNED, 
  parentGameId INTEGER SIGNED, 
  isAdult CHAR(1), 
  over21Required CHAR(1)
)
BEGIN
  START TRANSACTION;
  
  -- CALL logging.logMessage( 'update_game', 'TRACE', concat( 'begin (gameId[', gameId ,'] ownerId[', ownerId , '] gameName[', gameName, '] gameAccessId[', gameAccessId, ']'));
  -- CALL logging.flushLog();

  CALL developer.transition_game_access(gameId, modifiersId, gameAccessId, 'ignored');

    UPDATE
      games
    SET
      owner_id            = ownerId,
      game_rating_id      = gameRatingId,
      game_status_id      = gameStatusId,
      game_name           = gameName,
      game_description    = gameDescription,
      game_synopsis       = gameSynopsis,
      game_creation_date  = gameCreationDate,
      game_image_path     = gameImagePath,
      game_keywords       = gameKeywords,
      game_encryption_key = gameEncryptionKey,
      last_updated        = lastUpdated,
      modifiers_id        = modifiersId,
      parent_game_id      = parentGameId
    WHERE
      game_id = gameId;

    -- Only update community if this is not a wok game
    IF NOT wok.is_wok_game( gameId ) THEN
      -- Update relevant data in the community table

      UPDATE
        kaneva.communities c, kaneva.communities_game cg
      SET
        c.creator_id       = ownerId,
        c.name             = gameName,
        c.name_no_spaces   = REPLACE(gameName, ' ', ''),
        c.description      = gameDescription,
        c.keywords         = gameKeywords,
        c.last_update      = lastUpdated,
        c.is_adult         = isAdult,
        c.over_21_required = over21Required,
        c.creator_username = (select username from kaneva.users where user_id=ownerId)
      WHERE
        c.community_id = cg.community_id AND cg.game_id = gameId;
    END IF;
    
    SELECT row_count();

  COMMIT;

  -- CALL logging.logMessage( 'update_game', 'TRACE', concat( 'end (gameId[', gameId ,'] ownerId[', ownerId , '] gameName[', gameName, '] gameAccessId[', gameAccessId, ']'));
  -- CALL logging.flushLog();

END
;;
DELIMITER ;