-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_modifier_access;

DELIMITER ;;
CREATE PROCEDURE `add_modifier_access`(modifierId INTEGER UNSIGNED, gameId INTEGER SIGNED)
sproc:BEGIN
  
	INSERT IGNORE INTO player_game_allowed (kaneva_user_id, game_id) VALUES ( modifierId, gameId );
		  
END
;;
DELIMITER ;