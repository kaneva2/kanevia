-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS transition_game_access;

DELIMITER ;;
CREATE PROCEDURE `transition_game_access`(
  IN  __gameId INT(11), 
  IN  __modifierId INT(11), 
  IN  __requestedAccessId INT(11), 
  IN __mode VARCHAR(16)
)
    COMMENT '\n    -- this function handles game_access_id transitions to and from the following states\n      -- 1 PUBLIC\n      -- 2 PRIVATE\n      -- 3 BANNED\n      -- 4 INACCESSIBLE -- DEPRECATED\n      -- 5 DELETED\n      -- 6 PURGED\n      -- 7 MEMBERS\n    --\n  '
this_proc:
BEGIN

  DECLARE __currentAccessId INT DEFAULT -1;
  DECLARE __auditMessage    VARCHAR(20) default 'transition_game_access';
  DECLARE __communityId     INT DEFAULT -1;
  DECLARE __statusId        INT DEFAULT -1;

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
      -- ERROR
      GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
      CALL logging.logMsg('ERROR', SCHEMA(), 'transition_game_access', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
      RESIGNAL;
  END;

  CALL logging.logMsg('TRACE', SCHEMA(), 'transition_game_access', CONCAT( 'BEGIN (__gameId[',CAST(__gameId AS CHAR), '] __modifierId[',CAST(__modifierId AS CHAR),'] __requestedAccessId[',CAST(__requestedAccessId AS CHAR),'] __mode[',__mode,'])'));

  -- validate input
  IF __requestedAccessId < 1 OR __requestedAccessId > 7 THEN
    CALL logging.logMsg('ERROR', SCHEMA(), 'transition_game_access', CONCAT('EXITING __gameId[',CAST(__gameId AS CHAR), '] __requestedAccessId[',CAST(__requestedAccessId AS CHAR),'] out of range(1,7) -- exiting'));
    LEAVE this_proc;
  END IF;


  -- DEPRECATE 
  IF __requestedAccessId = 4 THEN
    CALL logging.logMsg('ERROR', SCHEMA(), 'transition_game_access', CONCAT('EXITING __gameId[',CAST(__gameId AS CHAR), '] __requestedAccessId[',CAST(__requestedAccessId AS CHAR), '] DEPRECATED -- exiting'));
    LEAVE this_proc;
  END IF;


  -- Get currentAccessId from db
  SELECT game_access_id INTO __currentAccessId FROM games WHERE game_id = __gameId;
  IF __currentAccessId = -1 THEN
    CALL logging.logMsg('ERROR', SCHEMA(), 'transition_game_access', CONCAT( 'EXITING __gameId[',CAST(__gameId AS CHAR), '] not found in games -- exiting'));
    LEAVE this_proc;
  END IF;

  -- reentry guard
  IF __currentAccessId = __requestedAccessId THEN
    CALL logging.logMsg('ERROR', SCHEMA(), 'transition_game_access', CONCAT( 'done __currentAccessId[',CAST(__currentAccessId AS CHAR), '] matches __requestedAccessId[',CAST(__requestedAccessId AS CHAR),'] -- exiting'));
    LEAVE this_proc;
  END IF;


  -- First look at the currentAccessId from top down
  IF __currentAccessId = 6 THEN
    -- PURGED 
    CALL logging.logMsg('ERROR', SCHEMA(), 'transition_game_access', CONCAT( 'EXITING __gameId[',CAST(__gameId AS CHAR),'] __currentAccessId[',CAST(__currentAccessId AS CHAR),'] PURGED, nothing left to do -- exiting'));
    LEAVE this_proc;

  ELSEIF __currentAccessId = 5 THEN
    -- DELETED 
    -- WE ONLY ALLOW GOING TO PURGE
      IF __requestedAccessId = 6 THEN
       UPDATE games SET game_access_id = __requestedAccessId, last_updated = NOW() WHERE game_id = __gameId;
       SET __auditMessage = CONCAT('access [',__currentAccessId,' ] -> [', __requestedAccessId, ']');
       CALL logging.logMsg('INFO', SCHEMA(), 'transition_game_access', __auditMessage);
      ELSE
       CALL logging.logMsg('ERROR', SCHEMA(), 'transition_game_access', CONCAT( 'EXITING __gameId[',CAST(__gameId AS CHAR),'] __currentAccessId[',CAST(__currentAccessId AS CHAR),'] DELETED can not transition to __requestedAccessId[',CAST(__requestedAccessId AS CHAR),'] -- exiting'));
       LEAVE this_proc;
      END IF;

  ELSE
    -- __currentAccessId = 7 3 2 1
    -- 7 MEMBERS / 3 BANNED / 2 PRIVATE  / 1 PUBLIC
    -- now we have to look at the __requestedAccessId to determine what to do
    -- for now free transition between states (list them individually in the stmt to simplyfy debuging

    IF __requestedAccessId = 1 THEN
     UPDATE games SET game_access_id = __requestedAccessId, last_updated = NOW() WHERE game_id = __gameId;
     SET __auditMessage = CONCAT('access [',__currentAccessId,' ] -> [', __requestedAccessId, ']');
     CALL logging.logMsg('INFO', SCHEMA(), 'transition_game_access', __auditMessage);

    ELSEIF __requestedAccessId = 2 THEN
      UPDATE games SET game_access_id = __requestedAccessId, last_updated = NOW() WHERE game_id = __gameId;
      SET __auditMessage = CONCAT('access [',__currentAccessId,' ] -> [', __requestedAccessId, ']');
      CALL logging.logMsg('INFO', SCHEMA(), 'transition_game_access', __auditMessage);

    ELSEIF __requestedAccessId = 3 THEN
      UPDATE games SET game_access_id = __requestedAccessId, last_updated = NOW() WHERE game_id = __gameId;
      SET __auditMessage = CONCAT('access [',__currentAccessId,' ] -> [', __requestedAccessId, ']');
      CALL logging.logMsg('INFO', SCHEMA(), 'transition_game_access', __auditMessage);

    ELSEIF __requestedAccessId = 4 THEN
      CALL logging.logMsg('ERROR', SCHEMA(), 'transition_game_access',  '__gameId[',CAST(__gameId AS CHAR), '] unreachable __requestedAccessId[',CAST(__requestedAccessId AS CHAR),'] -- exiting');

    ELSEIF __requestedAccessId = 5 THEN

      UPDATE games SET game_access_id = __requestedAccessId, last_updated = NOW() WHERE game_id = __gameId;
      SET __auditMessage = CONCAT('access [',__currentAccessId,' ] -> [', __requestedAccessId, ']');
      CALL logging.logMsg('INFO', SCHEMA(), 'transition_game_access', __auditMessage);


      -- handle community deletion
      if (__mode <> 'gameonly') THEN

        SELECT 
          c.community_id,
          c.status_id
        INTO
          __communityId,
          __statusId
        FROM 
            kaneva.communities c 
            INNER JOIN kaneva.communities_game cg
            ON c.community_id = cg.community_id
        WHERE 
          cg.game_id  = __gameId;

        CALL logging.logMsg('TRACE', SCHEMA(), 'transition_game_access', CONCAT('__gameId[',CAST(__gameId AS CHAR) , '] has associated community[',CAST(__communityId AS CHAR),'], status[',CAST(__statusId AS CHAR),']'));

        IF __communityId > 0 THEN -- we have a community
          IF __statusId <> 3 THEN -- and its not already deleted
            CALL logging.logMsg('TRACE', SCHEMA(), 'transition_game_access', CONCAT('__gameId[',CAST(__gameId AS CHAR), '] __communityId[',CAST(__communityId AS CHAR),'] needs to be deleted'));

            CALL kaneva.update_communities_delete(__communityId, 3, 'communityOnly'); -- 3 = eCOMMUNITY_STATUS.DELETED

            CALL logging.logMsg('TRACE', SCHEMA(), 'transition_game_access', CONCAT('__gameId[',CAST(__gameId AS CHAR), '] __communityId[',CAST(__communityId AS CHAR),'] was deleted'));

          END IF;
        ELSE
          CALL logging.logMsg('ERROR', SCHEMA(), 'transition_game_access', CONCAT('EXITING __gameId[',CAST(__gameId AS CHAR), '] could not resolve community id -- exiting'));
          LEAVE this_proc;
        END IF;
      END IF;

	ELSEIF __requestedAccessId = 7 THEN
      UPDATE games SET game_access_id = __requestedAccessId, last_updated = NOW() WHERE game_id = __gameId;
      SET __auditMessage = CONCAT('access [',__currentAccessId,' ] -> [', __requestedAccessId, ']');
      CALL logging.logMsg('INFO', SCHEMA(), 'transition_game_access', __auditMessage);

    ELSE 
      CALL logging.logMsg('WARN', SCHEMA(), 'transition_game_access', CONCAT('EXITING __gameId[',CAST(__gameId AS CHAR), '] request not processed __requestedAccessId[',CAST(__requestedAccessId AS CHAR),'] -- exiting'));
      LEAVE this_proc;
    END IF; -- __requestedAccessId

 END IF; -- currentAccessId
    
   -- if we got here then the game record changed
   INSERT into developer.audit_games
   (
     game_id,         owner_id,         game_rating_id, game_status_id, game_name,      game_description, game_synopsis, game_creation_date, 
     game_access_id,  game_image_path,  game_keywords,  last_updated,   modifiers_id,   date_modified,    operation
   )
   SELECT
     __gameId, owner_id, game_rating_id, game_status_id, game_name,    game_description, game_synopsis, game_creation_date, 
       game_access_id, game_image_path,  game_keywords,  last_updated, __modifierId,     Now(),        __auditMessage
   FROM 
       games 
   WHERE 
       game_id = __gameId;

  CALL logging.logMsg('TRACE', SCHEMA(), 'transition_game_access', CONCAT( 'END (__gameId[',CAST(__gameId AS CHAR), '] __modifierId[',CAST(__modifierId AS CHAR),'] __requestedAccessId[',CAST(__requestedAccessId AS CHAR) ,'] __auditMessage[',__auditMessage,'])'));

END
;;
DELIMITER ;