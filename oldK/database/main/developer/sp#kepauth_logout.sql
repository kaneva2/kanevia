-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS kepauth_logout;

DELIMITER ;;
CREATE PROCEDURE `kepauth_logout`( __user_id INT, __reason_code INT,
							        __server_id INT, __actual_ip VARCHAR(50) )
BEGIN
	DECLARE __ret INT DEFAULT 1; -- default to SUCCESS

	CALL kepauth_logout_param( __user_id, __reason_code, __server_id, __actual_ip, __ret );

	SELECT __ret AS ret;	
	
END
;;
DELIMITER ;