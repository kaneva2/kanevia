-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS before_delete_game_licenses;

DELIMITER ;;
CREATE TRIGGER `before_delete_game_licenses` BEFORE DELETE ON `game_licenses` FOR EACH ROW BEGIN
    INSERT into developer.audit_game_licenses
    (
    	game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, license_status_id, modifiers_id,
    	date_modified, operation
    )
    VALUES
    (
    	old.game_license_id, old.game_id, old.license_subscription_id, old.game_key, old.start_date, old.end_date, old.purchase_date, old.license_status_id, 
    	old.modifiers_id, Now(), 'delete'
    ) ;
	END
;;
DELIMITER ;