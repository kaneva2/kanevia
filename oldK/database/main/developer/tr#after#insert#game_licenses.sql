-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS after_insert_game_licenses;

DELIMITER ;;
CREATE TRIGGER `after_insert_game_licenses` AFTER INSERT ON `game_licenses` FOR EACH ROW BEGIN
    INSERT into developer.audit_game_licenses
    (
    	game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, license_status_id, modifiers_id,
    	date_modified, operation
    )
    VALUES
    (
	   	new.game_license_id, new.game_id, new.license_subscription_id, new.game_key, new.start_date, new.end_date, new.purchase_date, new.license_status_id, 
    	new.modifiers_id, Now(), 'insert'
    ) ;
END
;;
DELIMITER ;