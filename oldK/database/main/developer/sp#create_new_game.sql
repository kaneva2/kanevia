-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS create_new_game;

DELIMITER ;;
CREATE PROCEDURE `create_new_game`(ownerId INTEGER UNSIGNED, gameRatingId SMALLINT UNSIGNED, gameStatusId SMALLINT UNSIGNED,
            gameName VARCHAR(100), gameDescription TEXT, gameSynopsis VARCHAR(50), gameCreationDate DATETIME,
            gameAccessId VARCHAR(45), gameImagePath VARCHAR(255), gameKeywords TEXT, gameEncryptionKey VARCHAR(16),
            lastUpdated DATETIME, modifiersId INTEGER UNSIGNED, parentGameId INTEGER SIGNED, licenseSubscriptionId INTEGER SIGNED, 
            gameKey VARCHAR(100), startDate DATETIME, endDate DATETIME, purchaseDate DATETIME, licenseStatusId INTEGER SIGNED,
            maxGameUsers INTEGER SIGNED, canPublish INTEGER SIGNED, communityTypeId INTEGER UNSIGNED,
            backgroundImage VARCHAR(255), backgroundRGB VARCHAR(8), description VARCHAR(255),
            isAdult CHAR(1), isPublic CHAR(1), keywords TEXT, NAME VARCHAR(100), nameNoSpaces VARCHAR(100), 
            over21Required CHAR(1), placeTypeId INTEGER SIGNED, isPersonal INTEGER UNSIGNED, statusId INTEGER SIGNED, 
            email VARCHAR(100), creatorId INTEGER SIGNED, creatorUsername VARCHAR(22), url VARCHAR(50))
sproc:BEGIN

	 
	 DECLARE __communityId INT DEFAULT 0;
	 DECLARE __gameKey VARCHAR(100) DEFAULT '';
	 DECLARE __ownerId INT DEFAULT 1;
	 DECLARE __statusId INT DEFAULT 1;
	 DECLARE __accountTypeId INT DEFAULT 1;
	 DECLARE __done INT DEFAULT 0;


	 
	 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION, SQLWARNING, NOT FOUND SET __done = 1;

	  START TRANSACTION;
			   
	    
	    CALL add_new_game(ownerId, gameRatingId, gameStatusId, gameName, gameDescription, gameSynopsis, gameCreationDate, gameAccessId, gameImagePath, gameKeywords, gameEncryptionKey, lastUpdated, modifiersId, parentGameId, @gameId);
	    IF __done THEN ROLLBACK; LEAVE sproc; END IF;
		
	    
		SET __gameKey = CONCAT(ownerId,'O',@gameId,'G',gameKey); 
	    IF __done THEN ROLLBACK; LEAVE sproc; END IF;
	    
	    
	    CALL add_new_game_license (@gameId, licenseSubscriptionId, __gameKey, startDate, endDate, purchaseDate, licenseStatusId, maxGameUsers, modifiersId,@gameLicenseId);  
	    IF __done THEN ROLLBACK; LEAVE sproc; END IF;

	    
	    CALL kaneva.add_communities (placeTypeId, nameNoSpaces, url, creatorId, statusId, email, isPublic, isAdult, '', '', '', '', '', '', canPublish, 0, isPersonal, communityTypeId, 0, over21Required, 'N', NAME, description, backgroundRGB, backgroundImage, creatorUsername, keywords, @communityId);  
	    IF __done THEN ROLLBACK; LEAVE sproc; END IF;

	    
	    CALL kaneva.add_community_members (@communityId, creatorId, __accountTypeId, __statusId, 'Y', 0, 'Y', 'Y', NULL, 0, 0, 0,0, 0, 0, 0); 
	    IF __done THEN ROLLBACK; LEAVE sproc; END IF;
	            
	    
	    CALL kaneva.add_community_preferences(@communityId, 1, 1, 1); 
	    IF __done THEN ROLLBACK; LEAVE sproc; END IF;

	    
	    CALL kaneva.add_community_game_entry(@communityId,@gameId); 
	    IF __done THEN ROLLBACK; LEAVE sproc; END IF;

		CALL add_modifier_access(modifiersId, @gameId);
	    IF __done THEN ROLLBACK; LEAVE sproc; END IF;

	    
	    SELECT CAST(@gameId as UNSIGNED INT);

	COMMIT;
		  
END
;;
DELIMITER ;