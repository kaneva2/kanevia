-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS server_login_cleanup;

DELIMITER ;;
CREATE PROCEDURE `server_login_cleanup`(IN __server_id INT, IN __reason_code INT )
BEGIN

	DECLARE __done INT DEFAULT 0;
	DECLARE __game_id INT DEFAULT 0;
	DECLARE __user_id INT(11);
	DECLARE __zone_index INT(11);
	DECLARE __zone_instance_id INT(11);
	DECLARE __deferred_sql VARCHAR(255);
	DECLARE __q_now VARCHAR(50);
	DECLARE __created_date VARCHAR(50);
	DECLARE __ip_address VARCHAR(50);

	DECLARE __active_logins CURSOR FOR 
		SELECT al.user_id, al.created_date, al.ip_address, al.game_id, pz.current_zone_index, pz.current_zone_instance_id
		FROM active_logins al 
		LEFT OUTER JOIN wok.players p ON al.user_id = p.kaneva_user_id
		LEFT OUTER JOIN wok.player_zones pz ON p.player_id = pz.player_id
		WHERE al.server_id = __server_id;
	           
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1;

	SET __done = 0;
	OPEN __active_logins;
	REPEAT
		FETCH __active_logins INTO __user_id, __created_date, __ip_address, __game_id, __zone_index, __zone_instance_id;
		IF NOT __done THEN
			INSERT INTO developer.login_log(user_id,server_id,game_id,created_date,logout_date,logout_reason_code,ip_address) 
				VALUES (__user_id, __server_id,__game_id,__created_date,NOW(),__reason_code,__ip_address);

			IF wok.is_wok_game(__game_id) = 1 THEN
				SET __game_id = NULL;
			ELSE
				SET __zone_index = NULL;
				SET __zone_instance_id = NULL;
			END IF;

			CALL developer.record_visit(__user_id, NULL, NULL, NULL, __zone_index, __zone_instance_id, __game_id);
		END IF;
	UNTIL __done END REPEAT;
	
	DELETE FROM active_logins  WHERE server_id = __server_id;
END
;;
DELIMITER ;