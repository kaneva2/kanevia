-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS sync_mail_inbox_counts;

DELIMITER ;;
CREATE PROCEDURE `sync_mail_inbox_counts`(__cutoff_time DATETIME)
BEGIN
DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE _user_id INT;
DECLARE _number_of_inbox INT;
DECLARE _number_of_inbox_C INT;
DECLARE arch_cursor CURSOR FOR 
			SELECT user_id, number_inbox_messages 
			FROM kaneva.users_stats WHERE number_inbox_messages > 10000;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;
SET i = 0;
OPEN arch_cursor;
REPEAT
    FETCH arch_cursor INTO _user_id, _number_of_inbox_C;
    IF NOT done THEN
	SET i=i+1;
	SELECT COALESCE(COUNT(*),0) INTO _number_of_inbox
		FROM kaneva.messages 
		WHERE to_id = _user_id AND to_viewable != 'D';
	IF _number_of_comments <> _number_of_comments_C THEN
		SELECT _user_id, _number_of_inbox_C, _number_of_inbox;
		-- UPDATE users_stats SET number_inbox_messages = _number_inbox_messages_C WHERE user_id = _user_id;
	END IF;
	IF NOW() >= __cutoff_time THEN 
		SET done = 1; 
	END IF;
    END IF;	
UNTIL done END REPEAT;
CLOSE arch_cursor;
END
;;
DELIMITER ;