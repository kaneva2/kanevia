-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_game_license;

DELIMITER ;;
CREATE PROCEDURE `delete_game_license`(licenseId INTEGER SIGNED)
BEGIN

  START TRANSACTION;
	
    
    DELETE FROM game_licenses WHERE game_license_id = licenseId;
    
    SELECT row_count();

  COMMIT;

END
;;
DELIMITER ;