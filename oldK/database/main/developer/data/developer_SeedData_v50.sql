-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use developer;

-- add new development companies

INSERT INTO development_companies (contacts_firstname, contacts_lastname, contacts_email, contacts_phone, company_name, company_addressI,
company_addressII, city, state_code, province, country_id, zip_code, website_URL, annual_revenue, vworld_involvment_id, project_name,
project_description, project_startdate_id, budget, client_platform_id, project_status_id, current_team_size, projected_team_size,
date_submitted, approved, authorized_tester) VALUES
('Robin', 'Johnson', 'robin@trilogystudios.com', '555-555-5555', 'Triliogy Studio', 'add1', 'add2', 'city', 'GA', '', 'US', '55555',
'', 1, 1, 'closed beta', 'test', 1, 1, 1, 1, 1, 1, '2008-11-1 00:00:00', 1, 1),
('Matt', 'Scibilia', 'bwoolwine@criticalmassinteractive.com', '555-555-5555', 'Critical Mass', 'add1', 'add2', 'city', 'GA', '', 'US', '55555',
'', 1, 1, 'closed beta', 'test', 1, 1, 1, 1, 1, 1, '2008-11-1 00:00:00', 1, 1),
('Cameron', 'Conrad', 'mikale_c@hotmail.com', '555-555-5555', 'VVE', 'add1', 'add2', 'city', 'GA', '', 'US', '55555',
'', 1, 1, 'closed beta', 'test', 1, 1, 1, 1, 1, 1, '2008-11-1 00:00:00', 1, 1),
('Chris', 'Dodson', 'chrisdodson@mindspring.com', '555-555-5555', 'SCAD', 'add1', 'add2', 'city', 'GA', '', 'US', '55555',
'', 1, 1, 'closed beta', 'test', 1, 1, 1, 1, 3, 3, '2008-11-1 00:00:00', 1, 1),
('Curtis', 'Gorman', 'rayteku@msn.com', '555-555-5555', 'SmartKids', 'add1', 'add2', 'city', 'GA', '', 'US', '55555',
'', 1, 1, 'closed beta', 'test', 1, 1, 1, 1, 6, 6, '2008-11-1 00:00:00', 1, 1),
('Boyd', 'Kamer', 'mickey_kamer@hotmail.com', '555-555-5555', 'AETN', 'add1', 'add2', 'city', 'GA', '', 'US', '55555',
'', 1, 1, 'closed beta', 'test', 1, 1, 1, 1, 1, 1, '2008-11-1 00:00:00', 1, 1);

-- add developers to companies
-- add developer version number
DROP PROCEDURE IF EXISTS addCompanyDevelopers_v50;
DELIMITER //

CREATE PROCEDURE addCompanyDevelopers_v50()
BEGIN

declare _company1 BIGINT UNSIGNED;
declare _company2 BIGINT UNSIGNED;
declare _company3 BIGINT UNSIGNED;
declare _company4 BIGINT UNSIGNED;
declare _company5 BIGINT UNSIGNED;
declare _company6 BIGINT UNSIGNED;

declare _adminSiteRoleId1 BIGINT UNSIGNED;
declare _adminSiteRoleId2 BIGINT UNSIGNED;
declare _adminSiteRoleId3 BIGINT UNSIGNED;
declare _adminSiteRoleId4 BIGINT UNSIGNED;
declare _adminSiteRoleId5 BIGINT UNSIGNED;
declare _adminSiteRoleId6 BIGINT UNSIGNED;


-- retrieve the company id
SELECT company_id INTO _company1 FROM developer.development_companies WHERE company_name = "Triliogy Studio";

-- set up default security roles for the company
CALL site_management.add_default_game_roles(_company1);

-- set admin site role id
SELECT site_role_id INTO _adminSiteRoleId1 FROM site_management.site_roles WHERE role_id = 1 AND company_id = _company1;

-- create the developer for the company
INSERT INTO developer.company_developers (company_id, user_id, role_id) VALUES (_company1, 3880059, _adminSiteRoleId1);

 -- ---------------------------------------------------------------------------------------------------------

-- retrieve the company id
SELECT company_id INTO _company2 FROM developer.development_companies WHERE company_name = "Critical Mass";

-- set up default security roles for the company
CALL site_management.add_default_game_roles(_company2);

-- set admin site role id
SELECT site_role_id INTO _adminSiteRoleId2 FROM site_management.site_roles WHERE role_id = 1 AND company_id = _company2;

-- create the developer for the company
INSERT INTO developer.company_developers (company_id, user_id, role_id) VALUES (_company2, 5201, _adminSiteRoleId2);

 -- ---------------------------------------------------------------------------------------------------------

-- retrieve the company id
SELECT company_id INTO _company3 FROM developer.development_companies WHERE company_name = "VVE";

-- set up default security roles for the company
CALL site_management.add_default_game_roles(_company3);

-- set admin site role id
SELECT site_role_id INTO _adminSiteRoleId3 FROM site_management.site_roles WHERE role_id = 1 AND company_id = _company3;

-- create the developer for the company
INSERT INTO developer.company_developers (company_id, user_id, role_id) VALUES (_company3, 4145331, _adminSiteRoleId3);


   -- ---------------------------------------------------------------------------------------------------------

-- retrieve the company id
SELECT company_id INTO _company4 FROM developer.development_companies WHERE company_name = "SCAD";

-- set up default security roles for the company
CALL site_management.add_default_game_roles(_company4);

-- set admin site role id
SELECT site_role_id INTO _adminSiteRoleId4 FROM site_management.site_roles WHERE role_id = 1 AND company_id = _company4;

-- create the developer for the company
INSERT INTO developer.company_developers (company_id, user_id, role_id) VALUES (_company4, 4169478, _adminSiteRoleId4);
INSERT INTO developer.company_developers (company_id, user_id, role_id) VALUES (_company4, 11225, _adminSiteRoleId4);

   -- ---------------------------------------------------------------------------------------------------------

-- retrieve the company id
SELECT company_id INTO _company5 FROM developer.development_companies WHERE company_name = "SmartKids";

-- set up default security roles for the company
CALL site_management.add_default_game_roles(_company5);

-- set admin site role id
SELECT site_role_id INTO _adminSiteRoleId5 FROM site_management.site_roles WHERE role_id = 1 AND company_id = _company5;

-- create the developer for the company
INSERT INTO developer.company_developers (company_id, user_id, role_id) VALUES (_company5, 4102746, _adminSiteRoleId5);
INSERT INTO developer.company_developers (company_id, user_id, role_id) VALUES (_company5, 4115431, _adminSiteRoleId5);
INSERT INTO developer.company_developers (company_id, user_id, role_id) VALUES (_company5, 4114204, _adminSiteRoleId5);
INSERT INTO developer.company_developers (company_id, user_id, role_id) VALUES (_company5, 4139041, _adminSiteRoleId5);
INSERT INTO developer.company_developers (company_id, user_id, role_id) VALUES (_company5, 8196, _adminSiteRoleId5);

     -- ---------------------------------------------------------------------------------------------------------

-- retrieve the company id
SELECT company_id INTO _company6 FROM developer.development_companies WHERE company_name = "AETN";

-- set up default security roles for the company
CALL site_management.add_default_game_roles(_company6);

-- set admin site role id
SELECT site_role_id INTO _adminSiteRoleId6 FROM site_management.site_roles WHERE role_id = 1 AND company_id = _company6;

-- create the developer for the company
INSERT INTO developer.company_developers (company_id, user_id, role_id) VALUES (_company6, 4072984, _adminSiteRoleId6);

END //

DELIMITER ;

CALL addCompanyDevelopers_v50;
DROP PROCEDURE IF EXISTS addCompanyDevelopers_v50;
