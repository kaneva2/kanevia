-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

insert ignore into `game_server_status`(`server_status_id`,`name`,`description`) values (0,'stopped','The server has stopped');
insert ignore into `game_server_status`(`server_status_id`,`name`,`description`) values (1,'running','The server is running');
insert ignore into `game_server_status`(`server_status_id`,`name`,`description`) values (2,'stopping','The server is stopping');
insert ignore into `game_server_status`(`server_status_id`,`name`,`description`) values (3,'starting','The server is starting');
insert ignore into `game_server_status`(`server_status_id`,`name`,`description`) values (4,'locked','The server has locked');
insert ignore into `game_server_status`(`server_status_id`,`name`,`description`) values (5,'failed','The server has failed');

insert ignore into `game_server_types`(`server_type_id`,`name`,`description`) values (1,'Game','A Game Server Engine');
insert ignore into `game_server_types`(`server_type_id`,`name`,`description`) values (2,'AI','A Game AI Engine');
insert ignore into `game_server_types`(`server_type_id`,`name`,`description`) values (3,'Unknown','An Unknown server');

insert ignore into `game_license_subscriptions`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (1,'Trial',1,0,'KPOINT',1,100000);
insert ignore into `game_license_subscriptions`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (2,'Designer',1,0,'KPOINT',100,100000);
insert ignore into `game_license_subscriptions`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (3,'Professional',1,20000,'KPOINT',2000,100000);

insert ignore into `game_server_visibility` (`visibility_id`,`name`,`description`) values (1,'Public','Anyone can access this server');
insert ignore into `game_server_visibility` (`visibility_id`,`name`,`description`) values (2,'Private','Not in rotation');

-- insert ignore into asset_role_ids (asset_role_id, asset_role) VALUES (1, 'owner'), (2,'administrator'), (3,'IT'), (4,'CSR');


insert ignore into `client_platforms`(`client_platform_id`,`client_platform`) values (1,'PC Low Spec');
insert ignore into `client_platforms`(`client_platform_id`,`client_platform`) values (2,'PC Current');
insert ignore into `client_platforms`(`client_platform_id`,`client_platform`) values (3,'PC Next Gen');
insert ignore into `client_platforms`(`client_platform_id`,`client_platform`) values (4,'Other');


insert ignore into `game_access`(`game_access_id`,`game_access`) values (1,'public');
insert ignore into `game_access`(`game_access_id`,`game_access`) values (2,'private');


insert ignore into `game_categories`(`category_id`,`description`) values (1,'Action & Adventure');
insert ignore into `game_categories`(`category_id`,`description`) values (2,'Aviation & Air Combat');
insert ignore into `game_categories`(`category_id`,`description`) values (3,'Business');
insert ignore into `game_categories`(`category_id`,`description`) values (4,'Casual Games');
insert ignore into `game_categories`(`category_id`,`description`) values (5,'Driving & Racing');
insert ignore into `game_categories`(`category_id`,`description`) values (6,'Education');
insert ignore into `game_categories`(`category_id`,`description`) values (7,'Fantasy');
insert ignore into `game_categories`(`category_id`,`description`) values (8,'Fighting');
insert ignore into `game_categories`(`category_id`,`description`) values (9,'Future');
insert ignore into `game_categories`(`category_id`,`description`) values (10,'Gambling');
insert ignore into `game_categories`(`category_id`,`description`) values (11,'Modern');
insert ignore into `game_categories`(`category_id`,`description`) values (12,'Puzzles');
insert ignore into `game_categories`(`category_id`,`description`) values (13,'Roleplaying');
insert ignore into `game_categories`(`category_id`,`description`) values (14,'Simulation');
insert ignore into `game_categories`(`category_id`,`description`) values (15,'Sports');
insert ignore into `game_categories`(`category_id`,`description`) values (16,'Strategy');


insert ignore into `game_license_status`(`license_status_id`,`license_status`) values (1,'active');
insert ignore into `game_license_status`(`license_status_id`,`license_status`) values (2,'deleted');
insert ignore into `game_license_status`(`license_status_id`,`license_status`) values (3,'waiting_on_it');
insert ignore into `game_license_status`(`license_status_id`,`license_status`) values (4,'failed_payment_authorization');
insert ignore into `game_license_status`(`license_status_id`,`license_status`) values (5,'unsubscribe');
insert ignore into `game_license_status`(`license_status_id`,`license_status`) values (6,'pending_auth');
insert ignore into `game_license_status`(`license_status_id`,`license_status`) values (7,'waiting_on_it_takedown');


insert ignore into `game_license_subscriptions`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (1,'Trial',1,0,'KPOINT',1,100000);
insert ignore into `game_license_subscriptions`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (2,'Designer',1,10,'KPOINT',100,100000);
insert ignore into `game_license_subscriptions`(`license_subscription_id`,`name`,`length_of_subscription`,`amount`,`amount_kei_point_id`,`max_server_users`,`disk_quota`) values (3,'Professional',1,20000,'KPOINT',2000,100000);


insert ignore into `game_ratings`(`game_rating_id`,`game_rating`,`description`,`minimum_age`,`is_mature`) values (1,'General','All Audiences (G)',0,0);
insert ignore into `game_ratings`(`game_rating_id`,`game_rating`,`description`,`minimum_age`,`is_mature`) values (11,'Teen','14 and Up (PG-14)',14,0);
insert ignore into `game_ratings`(`game_rating_id`,`game_rating`,`description`,`minimum_age`,`is_mature`) values (21,'Adult','18 and older (R)',18,1);
insert ignore into `game_ratings`(`game_rating_id`,`game_rating`,`description`,`minimum_age`,`is_mature`) values (31,'Mature','21 and older (NC-17)',14,1);


insert ignore into `game_role_ids`(`game_role_id`,`game_role`) values (1,'owner');
insert ignore into `game_role_ids`(`game_role_id`,`game_role`) values (2,'administrator');
insert ignore into `game_role_ids`(`game_role_id`,`game_role`) values (3,'IT');
insert ignore into `game_role_ids`(`game_role_id`,`game_role`) values (4,'CSR');


insert ignore into `game_server_types`(`server_type_id`,`name`,`description`) values (1,'Game','A Game Server Engine');
insert ignore into `game_server_types`(`server_type_id`,`name`,`description`) values (2,'AI','A Game AI Engine');
insert ignore into `game_server_types`(`server_type_id`,`name`,`description`) values (3,'Unknown','An Unknown Server');


insert ignore into `game_server_visibility`(`visibility_id`,`name`,`description`) values (1,'Public','Anyone can access this server');
insert ignore into `game_server_visibility`(`visibility_id`,`name`,`description`) values (2,'Private','Not in rotation');


insert ignore into `game_status`(`game_status_id`,`game_status`) values (1,'active');
insert ignore into `game_status`(`game_status_id`,`game_status`) values (2,'inactive');


insert ignore into `project_startdates`(`project_startdate_id`,`project_startdate`) values (1,'In Progress');
insert ignore into `project_startdates`(`project_startdate_id`,`project_startdate`) values (2,'1 Month');
insert ignore into `project_startdates`(`project_startdate_id`,`project_startdate`) values (3,'3 Month');
insert ignore into `project_startdates`(`project_startdate_id`,`project_startdate`) values (4,'6 Months');
insert ignore into `project_startdates`(`project_startdate_id`,`project_startdate`) values (5,'More than 6 Months');
insert ignore into `project_startdates`(`project_startdate_id`,`project_startdate`) values (6,'Don''t Know');


insert ignore into `project_status`(`project_status_id`,`project_status`) values (1,'Planning');
insert ignore into `project_status`(`project_status_id`,`project_status`) values (2,'In Development');


insert ignore into `vworld_involvement`(`vworld_involvement_id`,`vworld_involvement`) values (1,'Studio');
insert ignore into `vworld_involvement`(`vworld_involvement_id`,`vworld_involvement`) values (2,'3rd-party Developer');
insert ignore into `vworld_involvement`(`vworld_involvement_id`,`vworld_involvement`) values (3,'Educational Institution');
insert ignore into `vworld_involvement`(`vworld_involvement_id`,`vworld_involvement`) values (4,'Government');
insert ignore into `vworld_involvement`(`vworld_involvement_id`,`vworld_involvement`) values (5,'Corporation');

