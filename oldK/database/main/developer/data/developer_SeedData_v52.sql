-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use developer;

DROP PROCEDURE IF EXISTS addCompanyDevelopers_v52;
DELIMITER //

CREATE PROCEDURE addCompanyDevelopers_v52()
BEGIN

declare _company1 BIGINT UNSIGNED;
declare _company2 BIGINT UNSIGNED;
declare _company3 BIGINT UNSIGNED;
declare _company4 BIGINT UNSIGNED;
declare _company5 BIGINT UNSIGNED;
declare _company6 BIGINT UNSIGNED;
declare _company7 BIGINT UNSIGNED;

declare _adminSiteRoleId1 BIGINT UNSIGNED;
declare _adminSiteRoleId2 BIGINT UNSIGNED;
declare _adminSiteRoleId3 BIGINT UNSIGNED;
declare _adminSiteRoleId4 BIGINT UNSIGNED;
declare _adminSiteRoleId5 BIGINT UNSIGNED;
declare _adminSiteRoleId6 BIGINT UNSIGNED;
declare _adminSiteRoleId7 BIGINT UNSIGNED;


-- set the kaneva company id
SET _company7 = 57;

-- set up default security roles for kaneva
CALL site_management.add_default_game_roles(_company7);

-- set admin site role id
SELECT site_role_id INTO _adminSiteRoleId7 FROM site_management.site_roles WHERE role_id = 1 AND company_id = _company7;

-- update the security roles for the kaneva developers
UPDATE developer.company_developers SET role_id = _adminSiteRoleId7 WHERE company_id = _company7;

 -- ---------------------------------------------------------------------------------------------------------

-- retrieve the company id
SELECT company_id INTO _company1 FROM developer.development_companies WHERE company_name = "Triliogy Studio";

-- set admin site role id
SELECT site_role_id INTO _adminSiteRoleId1 FROM site_management.site_roles WHERE role_id = 1 AND company_id = _company1;

-- update the security roles for the developers
UPDATE developer.company_developers SET role_id = _adminSiteRoleId1 WHERE company_id = _company1;

-- ----------------------------------------------------------------------------------------------------------

-- retrieve the company id
SELECT company_id INTO _company2 FROM developer.development_companies WHERE company_name = "Critical Mass Interactive";

-- set admin site role id
SELECT site_role_id INTO _adminSiteRoleId2 FROM site_management.site_roles WHERE role_id = 1 AND company_id = _company2;

-- update the security roles for the developers
UPDATE developer.company_developers SET role_id = _adminSiteRoleId2 WHERE company_id = _company2;

 -- ---------------------------------------------------------------------------------------------------------

-- retrieve the company id
SELECT company_id INTO _company3 FROM developer.development_companies WHERE company_name = "VVE";

-- set admin site role id
SELECT site_role_id INTO _adminSiteRoleId3 FROM site_management.site_roles WHERE role_id = 1 AND company_id = _company3;

-- update the security roles for the developers
UPDATE developer.company_developers SET role_id = _adminSiteRoleId3 WHERE company_id = _company3;


   -- ---------------------------------------------------------------------------------------------------------

-- retrieve the company id
SELECT company_id INTO _company4 FROM developer.development_companies WHERE company_name = "SCAD";

-- set admin site role id
SELECT site_role_id INTO _adminSiteRoleId4 FROM site_management.site_roles WHERE role_id = 1 AND company_id = _company4;

-- update the security roles for the developers
UPDATE developer.company_developers SET role_id = _adminSiteRoleId4 WHERE company_id = _company4;

   -- ---------------------------------------------------------------------------------------------------------

-- retrieve the company id
SELECT company_id INTO _company5 FROM developer.development_companies WHERE company_name = "SmartKids";

-- set admin site role id
SELECT site_role_id INTO _adminSiteRoleId5 FROM site_management.site_roles WHERE role_id = 1 AND company_id = _company5;

-- update the security roles for the developers
UPDATE developer.company_developers SET role_id = _adminSiteRoleId5 WHERE company_id = _company5;

     -- ---------------------------------------------------------------------------------------------------------

-- retrieve the company id
SELECT company_id INTO _company6 FROM developer.development_companies WHERE company_name = "AETN";

-- set admin site role id
SELECT site_role_id INTO _adminSiteRoleId6 FROM site_management.site_roles WHERE role_id = 1 AND company_id = _company6;

-- update the security roles for the developers
UPDATE developer.company_developers SET role_id = _adminSiteRoleId6 WHERE company_id = _company6;

END //

DELIMITER ;

CALL addCompanyDevelopers_v52;
DROP PROCEDURE IF EXISTS addCompanyDevelopers_v52;
