-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS kepauth_login;

DELIMITER ;;
CREATE PROCEDURE `kepauth_login`( __user_id INT, IN __user_ip VARCHAR(50),
								  __game_id INT, __server_id INT, __actual_ip VARCHAR(50))
BEGIN
	DECLARE __ret INT DEFAULT 1;
	DECLARE __dupe BOOL DEFAULT 0;
	DECLARE __current_game_id INT DEFAULT 0;
	DECLARE __current_server_id INT DEFAULT 0;
	DECLARE __current_gs_ip_address VARCHAR(50) DEFAULT NULL;
	
	DECLARE __game_pop INT(11) DEFAULT NULL;
	DECLARE __game_pop_limit INT(11) DEFAULT 2000;
	DECLARE __game_role SMALLINT(6) DEFAULT NULL;

	DECLARE __is_wok_game TINYINT;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '23000' SET __dupe = 1;
	
	
	SET __ret = 4; 
	SELECT wok.is_wok_game(__game_id) INTO __is_wok_game;

	
	SELECT SUM(g.number_of_players) INTO __game_pop
	  FROM developer.game_servers g
	 WHERE game_id = __game_id 
	   AND server_status_id = 1;  
	   
	IF __game_pop IS NOT NULL THEN 
	
		SELECT max_game_users INTO __game_pop_limit
			FROM developer.game_licenses 
		WHERE game_id = __game_id;
			
		
		SET __game_role = wok.get_game_priv( __game_id, __user_id );
           
		IF __game_pop <= __game_pop_limit OR __game_role IN (1,2) THEN 
			INSERT INTO developer.game_login_log (user_id, game_id, logon_time)
			 VALUES ( __user_id, __game_id, NOW() );
			SET __ret = 1; 
		ELSE
			SET __ret = 9; 
		END IF;
	ELSE 
		SET __ret = 4; 
	END IF;
	
	IF __ret = 1 THEN  
		INSERT INTO active_logins (user_id,created_date,server_id,ip_address,game_id) 
		 VALUES(__user_id, NOW(),__server_id,__user_ip,__game_id)
		  ON DUPLICATE KEY UPDATE server_id = __server_id, game_id=__game_id, created_date=NOW();

	 	IF __dupe THEN 
	 		SELECT al.game_id, al.server_id, gs.actual_ip_address INTO __current_game_id, __current_server_id, __current_gs_ip_address
	 		  FROM active_logins al, game_servers gs
	 		 WHERE al.user_id = __user_id AND
				    al.server_id = gs.server_id;

			
				
					
					
					CALL logging.logMessage( 'developer.kepauth_login', 'INFO', CONCAT('logging off dupe user ', __user_id, ' from server ', CAST( __current_server_id AS CHAR ) ) );
					CALL kepauth_logout_param( __user_id, 13,__current_server_id, __current_gs_ip_address, __ret ); 
					INSERT INTO active_logins (user_id,created_date,server_id,ip_address,game_id) 
					 VALUES(__user_id, NOW(),__server_id,__user_ip,__game_id);
				
					
				
			
		END IF;				
		IF __is_wok_game = 0 AND __ret = 1 AND __user_id != 4524881 THEN 
			-- Record the visit, non-wokgame only, don't update previous
			CALL developer.record_visit(__user_id, NULL, NULL, __game_id, NULL, NULL, NULL);

			UPDATE developer.game_stats SET number_of_visits = number_of_visits + 1 WHERE game_id = __game_id;
			
			SELECT community_id INTO @community_id FROM kaneva.communities_game WHERE game_id = __game_id;
			UPDATE kaneva.community_members SET last_visited_at = NOW() WHERE community_id = @community_id AND user_id = __user_id;
		END IF;
	END IF;
	SELECT __ret as ret, __game_role as role;
	CALL logging.flushLog();
END
;;
DELIMITER ;