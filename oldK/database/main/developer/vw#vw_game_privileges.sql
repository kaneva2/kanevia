-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS vw_game_privileges;

DELIMITER ;;

 CREATE VIEW `developer`.`vw_game_privileges` AS SELECT `g`.`game_id` AS `game_id`,`m`.`user_id` AS `user_id`,`m`.`account_type_id` AS `account_type_id` 
 FROM ( `kaneva`.`communities_game` `g` 
 JOIN `kaneva`.`community_members` `m` on( ( `g`.`community_id` = `m`.`community_id`) ) ) 
;;
DELIMITER ;