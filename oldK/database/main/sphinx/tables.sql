-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `people_delta` (
  `user_id` int(11) NOT NULL COMMENT 'user',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

CREATE TABLE `sphinx_control` (
  `index_name` varchar(64) NOT NULL COMMENT 'index name',
  `last_delta` datetime NOT NULL COMMENT 'when the last delta index was built',
  `last_full` datetime NOT NULL COMMENT 'when the last full index rebuild was performed',
  `minutes_between_full` int(11) NOT NULL COMMENT 'how often to do a full index rebuild',
  PRIMARY KEY (`index_name`)
) ENGINE=InnoDB COMMENT='information needed to control when to do full and delta inde';

