-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS purge_email_bounce_log;

DELIMITER ;;
CREATE PROCEDURE `purge_email_bounce_log`(__purge_before_date	DATE, __purge_cutoff_time DATETIME)
BEGIN
DECLARE	__oldest_date		datetime;
DECLARE	__batch_date		DATETIME;
DECLARE	__batch_date2		DATETIME;
DECLARE	__table_RowCnt		INT;
DECLARE __message_text VARCHAR(500);
SELECT	IFNULL(COUNT(bounce_log_id),0) INTO __table_RowCnt
	FROM	email_bounce_log
	WHERE	created_date < __purge_before_date;
SELECT IFNULL(min(created_date),'2009-01-01 00:00:00') INTO __oldest_date
	FROM	email_bounce_log;
SELECT CONCAT('Starting purge ... ',CAST(__table_RowCnt AS CHAR),' rows to purge. Purge before date: ',
		CAST(__purge_before_date AS CHAR),'; Purge cutoff time: ',
		CAST(__purge_cutoff_time AS CHAR)) INTO __message_text;
		
CALL add_maintenance_log (DATABASE(),'purge_email_bounce_log',__message_text);
select CONCAT(DATE(__oldest_date),' 00:00:00') into __batch_date;
select ADDDATE(__batch_date, INTERVAL 1 day) into __batch_date2;

-- Loop while anything to delete.
WHILE (__batch_date < __purge_before_date AND NOW() < __purge_cutoff_time)    DO
	DELETE FROM	email_bounce_log WHERE created_date between __batch_date and __batch_date2;
	SELECT IFNULL(ROW_COUNT(),0) INTO __table_RowCnt;
	SELECT  CONCAT('Purged ',CAST(__table_RowCnt AS CHAR),' rows from: ',cast(__batch_date as char)) INTO __message_text;
	CALL add_maintenance_log (DATABASE(),'purge_email_bounce_log',__message_text);
	SELECT ADDDATE(__batch_date, INTERVAL 1 DAY) INTO __batch_date;
	SELECT ADDDATE(__batch_date, INTERVAL 1 DAY) INTO __batch_date2;
	END WHILE;
SELECT 'Purge completed.' INTO __message_text;
CALL add_maintenance_log (DATABASE(),'purge_email_bounce_log',__message_text);
END
;;
DELIMITER ;