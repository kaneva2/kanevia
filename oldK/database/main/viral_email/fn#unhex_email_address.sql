-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS unhex_email_address;

DELIMITER ;;
CREATE FUNCTION `unhex_email_address`(in_hex varchar(510)) RETURNS varchar(255)
    DETERMINISTIC
BEGIN
  RETURN UNHEX(in_hex);
END
;;
DELIMITER ;