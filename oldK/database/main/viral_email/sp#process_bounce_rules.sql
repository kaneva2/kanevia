-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS process_bounce_rules;

DELIMITER ;;
CREATE PROCEDURE `process_bounce_rules`( IN _date_to_process DATE)
BEGIN
-- process the rules in bounce rules to act upon data in email_bounce_log
DECLARE _rule_limit SMALLINT DEFAULT 1;
DECLARE _day_range TINYINT DEFAULT 1;
DECLARE __done INT DEFAULT 0;

-- cursor to get distinct actions to takes
DECLARE rulesCursor CURSOR FOR
	select rule_limit, day_range
	from bounce_rules
	where `action` = 1
	group by rule_limit, day_range
	order by rule_limit, day_range;


DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1;
OPEN rulesCursor;

DROP TEMPORARY TABLE IF EXISTS tmp_rules;
CREATE TEMPORARY TABLE IF NOT EXISTS tmp_rules
	(bounce_type_id TINYINT NOT NULL,
	source_type_id TINYINT NOT NULL);

REPEAT
	FETCH rulesCursor into _rule_limit, _day_range;
	
	IF NOT __done THEN 
		-- SELECT 'cursor rule limit and day range: ', _rule_limit, _day_range;
		TRUNCATE TABLE tmp_rules;
		INSERT INTO tmp_rules 
			select bounce_type_id, source_type_id 
			from bounce_rules 
			where rule_limit = _rule_limit and day_range = _day_range and `action` = 1;

		INSERT IGNORE INTO blacklist (bounce_type_id, email, when_added, email_hash)
		SELECT d.bounce_type_id, d.email, now(), email_hash
			FROM summary_bounce_log_daily d
			INNER JOIN tmp_rules tr ON d.source_type_id = tr.source_type_id AND d.bounce_type_id = tr.bounce_type_id
			WHERE d.bounce_date = _date_to_process
			AND LENGTH(email) > 4
			AND d.bounce_count >= _rule_limit
			AND d.bounce_date >= date_add(_date_to_process, INTERVAL -_day_range DAY) AND d.bounce_date <= _date_to_process;
			
	END IF;
	UNTIL __done END REPEAT;
	
CLOSE rulesCursor;
DROP TEMPORARY TABLE IF EXISTS tmp_rules;
		
END
;;
DELIMITER ;