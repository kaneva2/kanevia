-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_newsletter_email_queue;

DELIMITER ;;
CREATE PROCEDURE `add_newsletter_email_queue`(IN _days_elapsed SMALLINT, 
		IN _type_id SMALLINT, IN _template_id SMALLINT, IN _limit INT, 
		IN _req_wokplayer SMALLINT, IN _desired_status SMALLINT)
BEGIN
DECLARE __done INT DEFAULT 0;
DECLARE _i INT;
DECLARE __email_to VARCHAR(200);
DECLARE __email_hash BINARY(16);
DECLARE cur2 CURSOR FOR 
    SELECT u.email, u.email_hash
            FROM viral_email.users_copy u 
            LEFT JOIN viral_email.email_send_log esl ON u.email_hash = esl.email_hash AND esl.email_type_id = 23 AND DATEDIFF(NOW(), date_sent) < _days_elapsed
            LEFT JOIN viral_email.email_queue eq ON u.email_hash = eq.email_hash AND eq.email_type_id = 23 
		WHERE newsletter = 'Y' 
                AND status_id = _desired_status 
                AND  wok_player_id >= _req_wokplayer 
                AND esl.email_hash IS NULL -- not in the send log
                AND eq.email_hash IS NULL -- not waiting in the queue
                LIMIT 1000;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1;
SET __done = 0;
SET _i = 0;
OPEN cur2;
REPEAT	
	FETCH cur2 INTO __email_to, __email_hash;
	IF NOT __done THEN 
		INSERT INTO email_queue (date_queued, email_type_id, email_template_id, email_to, email_hash)
		VALUES
		(NOW(),_type_id,_template_id,__email_to, __email_hash);
		SET _i=_i+1;
	END IF;
UNTIL __done END REPEAT;
CLOSE cur2;
SELECT _i AS number_queued;
END
;;
DELIMITER ;