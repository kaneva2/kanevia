-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS build_blacklisted_today;

DELIMITER ;;
CREATE PROCEDURE `build_blacklisted_today`( IN _date_to_process DATE)
BEGIN

TRUNCATE TABLE emails_blacklisted_today;

INSERT INTO emails_blacklisted_today
	(blacklist_id, bounce_type_id, email, email_hash)
	SELECT blacklist_id, bounce_type_id, email, email_hash
	FROM blacklist
	WHERE DATE(when_added) = _date_to_process
	AND bounce_type_id != 17 ;

END
;;
DELIMITER ;