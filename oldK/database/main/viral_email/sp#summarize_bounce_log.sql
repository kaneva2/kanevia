-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS summarize_bounce_log;

DELIMITER ;;
CREATE PROCEDURE `summarize_bounce_log`( IN _date_to_process DATE)
BEGIN
-- process the rules in bounce rules to act upon data in email_bounce_log
DECLARE _rule_limit SMALLINT DEFAULT 1;
DECLARE _day_range TINYINT DEFAULT 1;

IF  _date_to_process IS NULL THEN
	SET _date_to_process = (SELECT DATE_ADD(DATE(now()), INTERVAL -1 DAY));
END IF;

-- make sure there will be no conflicts
DELETE FROM summary_bounce_log_daily WHERE bounce_date = _date_to_process;

INSERT INTO summary_bounce_log_daily (bounce_date, source_type_id, bounce_type_id, email, bounce_count, email_hash)
	SELECT _date_to_process, source_type_id, bounce_type_id, email, SUM(bounce_count), email_hash
	FROM email_bounce_log
	WHERE DATE(created_date) = _date_to_process AND email LIKE '%@%.%' 
	GROUP BY source_type_id, bounce_type_id, email
	ORDER BY source_type_id, bounce_type_id, email;


END
;;
DELIMITER ;