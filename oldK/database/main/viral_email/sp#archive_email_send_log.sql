-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS archive_email_send_log;

DELIMITER ;;
CREATE PROCEDURE `archive_email_send_log`(__archive_before_date	DATETIME, __archive_cutoff_time DATETIME)
BEGIN
DECLARE	_email_id 	BIGINT;
DECLARE __sql_statement VARCHAR(512);
DECLARE	__table_RowCnt		INT;
DECLARE	__OrigRowCnt		INT;
DECLARE __message_text VARCHAR(500);
DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE arch_esl_cursor CURSOR FOR 
			SELECT email_id
			FROM	email_send_log
			WHERE date_sent < __archive_before_date;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;
SET i = 0;
OPEN arch_esl_cursor;
SET __table_RowCnt = (SELECT FOUND_ROWS());
SELECT CONCAT('Starting archive ... ',CAST(__table_RowCnt AS CHAR),' rows to archive. Archive before date: ',
					CAST(__archive_before_date AS CHAR),'; Cutoff time: ',
					CAST(__archive_cutoff_time AS CHAR)) INTO __message_text;
CALL add_maintenance_log (DATABASE(),'archive_email_send_log',__message_text);
-- Loop while anything to delete.
REPEAT
    FETCH arch_esl_cursor INTO _email_id;
	
	START TRANSACTION;
	SET i=i+1;
	IF _email_id IS NOT NULL THEN
		INSERT IGNORE INTO email_send_log_archive (email_id,email_type_id,email_template_id,email_to,date_sent,date_opened,date_clicked,invite_id,send_status,email_hash)
			SELECT email_id,email_type_id,email_template_id,email_to,date_sent,date_opened,date_clicked,invite_id,send_status,email_hash
			FROM email_send_log WHERE email_id = _email_id;
			
		DELETE FROM email_send_log WHERE email_id = _email_id;
	END IF;
	COMMIT;
	IF NOW() >= __archive_cutoff_time THEN 
		SET done = 1; 
	END IF;
		
UNTIL done END REPEAT;
CLOSE arch_esl_cursor;
SELECT CONCAT('Archive completed ..... ',CAST(i AS CHAR), ' rows moved to email_send_log_archive.') INTO __message_text;
CALL add_maintenance_log (DATABASE(),'archive_email_send_log',__message_text);
END
;;
DELIMITER ;