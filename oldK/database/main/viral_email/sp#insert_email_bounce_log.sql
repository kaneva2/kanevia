-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS insert_email_bounce_log;

DELIMITER ;;
CREATE PROCEDURE `insert_email_bounce_log`(__bounce_type	VARCHAR(5), __source_type VARCHAR(20), 
	__email_address VARCHAR(256), __email_address_hex VARCHAR(512))
BEGIN
	DECLARE _bounce_type_id TINYINT DEFAULT 0;
	DECLARE _source_type_id TINYINT DEFAULT 0;
	
	IF LENGTH(__email_address) <= 5 or __email_address is null THEN 
		SET __email_address = 'NULL or invalid';
	END IF;
	SET _bounce_type_id = (SELECT bounce_type_id FROM bounce_type WHERE bounce_type_code = __bounce_type);
	SET _source_type_id = (SELECT source_type_id FROM source_type WHERE source_type = __source_type);
 	INSERT INTO email_bounce_log (source_type_id, bounce_type_id, email, created_date, bounce_count, email_hash)
		VALUES (_source_type_id, _bounce_type_id, 
				IFNULL(__email_address,unhex_email_address(__email_address_hex)), 
				NOW(), 1,
				UNHEX(MD5(IFNULL(__email_address,unhex_email_address(__email_address_hex)))));
END
;;
DELIMITER ;