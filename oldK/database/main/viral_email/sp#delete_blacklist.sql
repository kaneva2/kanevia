-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_blacklist;

DELIMITER ;;
CREATE PROCEDURE `delete_blacklist`( IN _email VARCHAR(255))
BEGIN

INSERT INTO blacklist_audit
	(blacklist_id, bounce_type_id, email, when_added, when_deleted)
	SELECT blacklist_id, bounce_type_id, email, when_added, now()
	FROM blacklist
	WHERE email = _email;

DELETE FROM blacklist WHERE email = _email;
		
END
;;
DELIMITER ;