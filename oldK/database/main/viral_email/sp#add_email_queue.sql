-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_email_queue;

DELIMITER ;;
CREATE PROCEDURE `add_email_queue`(IN _days_elapsed SMALLINT, IN _type_id SMALLINT)
BEGIN
DECLARE __done INT DEFAULT 0;
DECLARE _i INT;
DECLARE _invite_id INT;
DECLARE _email_to VARCHAR(200);
DECLARE _email_hash BINARY(16);
DECLARE cur2 CURSOR FOR 
		SELECT i.invite_id, i.email, i.email_hash
		FROM kaneva.invites i
		LEFT JOIN viral_email.email_queue emq ON i.email_hash=emq.email_hash AND emq.email_type_id=_type_id
		LEFT JOIN viral_email.email_send_log esl ON i.email_hash=esl.email_hash AND esl.email_type_id=_type_id
		WHERE invite_status_id = 1 	AND clicked_accept_link = 0 	AND attempts > 0 AND user_id != 0
		AND reinvite_date = invited_date  
		AND invited_date BETWEEN CONCAT(DATE(ADDDATE(NOW(), INTERVAL -_days_elapsed DAY)),' 00:00:00') 
					AND CONCAT(DATE(ADDDATE(NOW(), INTERVAL -_days_elapsed+1 DAY)),' 00:00:00')
		AND i.email_hash NOT IN 
		(SELECT DISTINCT uc.email_hash 
			FROM viral_email.users_copy uc
			WHERE signup_date <= DATE_SUB(NOW(), INTERVAL _days_elapsed DAY))
		AND emq.email_queue_id IS NULL -- not in the queue
		AND esl.email_id IS NULL -- not in the send log
		LIMIT 1000;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1;
SET __done = 0;
SET _i = 0;
OPEN cur2;
REPEAT	
	FETCH cur2 INTO _invite_id, _email_to, _email_hash;
	IF NOT __done THEN 
		INSERT INTO email_queue (date_queued, email_type_id, email_template_id, email_to, invite_id, email_hash)
			SELECT NOW(), _type_id, 0, _email_to, _invite_id, _email_hash;
		SET _i=_i+1;
	END IF;
UNTIL __done END REPEAT;
CLOSE cur2;
SELECT _i AS number_queued;
END
;;
DELIMITER ;