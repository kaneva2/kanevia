-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS `archive_summary_bounce_log_daily`;
DELIMITER $$
CREATE PROCEDURE `archive_summary_bounce_log_daily`	(__archive_cutoff_time DATETIME) 
BEGIN

DECLARE	__table_RowCnt		BIGINT;
DECLARE __message_text 		VARCHAR(500);
DECLARE __min_bounce_date	DATE;
DECLARE	__batch_date		DATETIME;

SELECT (IFNULL(MAX(bounce_date),'2009-01-01')) INTO __min_bounce_date
	FROM	viral_email_archive.summary_bounce_log_daily;

SELECT	IFNULL(COUNT(*),0) INTO __table_RowCnt
	FROM	viral_email.summary_bounce_log_daily
	WHERE	bounce_date > __min_bounce_date;

SELECT CONCAT('Starting archive ... ',CAST(__table_RowCnt AS CHAR),' rows to copy to archive. ; Archive cutoff time: ',
				CAST(__archive_cutoff_time AS CHAR)) INTO __message_text;
CALL add_maintenance_log (DATABASE(),'archive_summary_bounce_log_daily',__message_text);

SELECT ADDDATE(__min_bounce_date, INTERVAL 1 DAY) INTO __batch_date;

-- Loop while anything to delete.
WHILE (__batch_date < DATE(NOW()) AND NOW() < __archive_cutoff_time)    DO

	INSERT INTO viral_email_archive.summary_bounce_log_daily 
		SELECT * FROM viral_email.summary_bounce_log_daily WHERE bounce_date = __batch_date;

	SELECT IFNULL(ROW_COUNT(),0) INTO __table_RowCnt;

	SELECT  CONCAT('Copied: ',CAST(__table_RowCnt AS CHAR),' rows from: ',CAST(__batch_date AS CHAR)) INTO __message_text;
	CALL add_maintenance_log (DATABASE(),'archive_summary_bounce_log_daily',__message_text);

	SELECT ADDDATE(__batch_date, INTERVAL 1 DAY) INTO __batch_date;
	END WHILE;

CALL add_maintenance_log (DATABASE(),'archive_summary_bounce_log_daily','Archive completed.');

END$$

DELIMITER ;
