-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use viral_email;

\. viral_email_SchemaChanges_v8.sql

-- add developer version number
DROP PROCEDURE IF EXISTS viral_email_PromotePolicy_v8;
DELIMITER //
CREATE PROCEDURE viral_email_PromotePolicy_v8()
BEGIN
IF NOT EXISTS (SELECT 1 FROM schema_versions WHERE version = 8.0) THEN
	INSERT INTO `schema_versions` ( `version`, `description` ) 
		VALUES (8.0, 'Auto Violations (initial version)' );
END IF;

END //

DELIMITER ;

CALL viral_email_PromotePolicy_v8;
DROP PROCEDURE IF EXISTS viral_email_PromotePolicy_v8;

