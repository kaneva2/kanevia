-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS SchemaChanges;

DELIMITER //

CREATE PROCEDURE SchemaChanges()
BEGIN

DROP TABLE IF EXISTS `viral_email`.`email_additional_message`;

CREATE TABLE IF NOT EXISTS `viral_email`.`email_overload_fields`( `overload_id` INT NOT NULL DEFAULT 0, `key` VARCHAR(255) , `value` VARCHAR(2000) )  ;                              


IF NOT EXISTS (select 1
		from information_schema.columns where table_schema = 'viral_email' and table_name = 'email_queue' 
		and column_name = 'overload_id') THEN
	ALTER TABLE `viral_email`.`email_queue` ADD COLUMN `overload_id` INT DEFAULT '0' NOT NULL AFTER `email_hash`;
END IF;

END //
DELIMITER ;

CALL SchemaChanges();

DROP PROCEDURE IF EXISTS SchemaChanges;
