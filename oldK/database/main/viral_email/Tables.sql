-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `blacklist` (
  `blacklist_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `bounce_type_id` tinyint(4) NOT NULL COMMENT 'PK',
  `email` varchar(255) NOT NULL COMMENT 'email',
  `email_hash` binary(16) NOT NULL,
  `when_added` datetime NOT NULL,
  PRIMARY KEY (`blacklist_id`),
  UNIQUE KEY `idx_email` (`email`),
  KEY `IDX_bounce_type_id_FK` (`bounce_type_id`),
  KEY `IDX_email_hash` (`email_hash`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT COMMENT='Places not to send mail';

CREATE TABLE `blacklist_audit` (
  `blacklist_id` int(11) NOT NULL COMMENT 'PK',
  `bounce_type_id` tinyint(4) NOT NULL,
  `email` varchar(255) NOT NULL COMMENT 'email',
  `when_added` datetime NOT NULL,
  `when_deleted` datetime NOT NULL,
  PRIMARY KEY (`blacklist_id`),
  UNIQUE KEY `idx_email` (`email`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT COMMENT='Auditing of deletes from blacklist';

CREATE TABLE `blacklist_match_log` (
  `blacklist_match_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `blacklist_id` int(11) NOT NULL COMMENT 'PK',
  `when_matched` datetime NOT NULL,
  PRIMARY KEY (`blacklist_match_log_id`),
  KEY `IDX_blacklist_id` (`blacklist_id`)
) ENGINE=InnoDB;

CREATE TABLE `bounce_rules` (
  `bounce_rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `source_type_id` tinyint(4) NOT NULL COMMENT 'PK',
  `bounce_type_id` tinyint(4) NOT NULL COMMENT 'PK',
  `rule_limit` smallint(6) NOT NULL DEFAULT '1',
  `day_range` tinyint(4) NOT NULL,
  `action` tinyint(4) NOT NULL,
  `definition` varchar(80) NOT NULL,
  PRIMARY KEY (`bounce_rule_id`),
  UNIQUE KEY `IDX_sourcetype_bouncetype` (`source_type_id`,`bounce_type_id`),
  KEY `IDX_source_type_id` (`source_type_id`),
  KEY `IDX_bounce_type_id` (`bounce_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `bounce_temp` (
  `sBounceType` varchar(255) DEFAULT NULL,
  `sEmailAddress` varchar(255) DEFAULT NULL,
  `sPOPAccount` varchar(255) DEFAULT NULL,
  `dtInserted` datetime DEFAULT NULL
) ENGINE=InnoDB;

CREATE TABLE `bounce_type` (
  `bounce_type_id` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `bounce_type_code` varchar(5) NOT NULL COMMENT 'bounce type',
  `bounce_type_desc` varchar(120) NOT NULL COMMENT 'decode btype',
  PRIMARY KEY (`bounce_type_id`),
  UNIQUE KEY `idx_btype` (`bounce_type_code`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT COMMENT='various codes for bounce types';

CREATE TABLE `emails_blacklisted_today` (
  `blacklist_id` int(11) NOT NULL COMMENT 'PK',
  `bounce_type_id` tinyint(4) NOT NULL,
  `email` varchar(255) NOT NULL COMMENT 'email',
  `email_hash` binary(16) NOT NULL,
  PRIMARY KEY (`blacklist_id`),
  UNIQUE KEY `idx_email` (`email`),
  KEY `IDX_email_hash` (`email_hash`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT COMMENT='List of emails added to the blacklist today';

CREATE TABLE `email_bounce_log` (
  `bounce_log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `source_type_id` tinyint(4) NOT NULL COMMENT 'PK',
  `bounce_type_id` tinyint(4) NOT NULL COMMENT 'PK',
  `email` varchar(255) NOT NULL COMMENT 'email',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bounce_count` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'number of times inserted from mail server',
  `email_hash` binary(16) NOT NULL,
  PRIMARY KEY (`bounce_log_id`),
  KEY `IDX_email` (`email`),
  KEY `IDX_source_type_id` (`source_type_id`),
  KEY `IDX_bounce_type_id` (`bounce_type_id`),
  KEY `IDX_email_hash` (`email_hash`),
  KEY `IDX_created_date` (`created_date`)
) ENGINE=InnoDB AVG_ROW_LENGTH=16384 ROW_FORMAT=COMPACT COMMENT='Places not to send mail';

CREATE TABLE `email_click_log` (
  `click_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `mailing_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `request` varchar(255) DEFAULT NULL,
  `ipaddress` varchar(255) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`click_id`)
) ENGINE=InnoDB;

CREATE TABLE `email_open_log` (
  `view_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `mailing_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `ipaddress` varchar(255) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`view_id`)
) ENGINE=InnoDB;

CREATE TABLE `email_overload_fields` (
  `overload_id` int(11) NOT NULL DEFAULT '0',
  `key` varchar(255) NOT NULL,
  `value` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`overload_id`,`key`)
) ENGINE=InnoDB;

CREATE TABLE `email_queue` (
  `email_queue_id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT 'queue id',
  `date_queued` datetime NOT NULL COMMENT 'date the email was queued',
  `email_type_id` smallint(5) unsigned NOT NULL COMMENT 'id from email_types',
  `email_template_id` smallint(5) unsigned NOT NULL COMMENT 'id from email_templates',
  `email_to` varchar(200) NOT NULL COMMENT 'email recipient',
  `invite_id` int(11) NOT NULL DEFAULT '0' COMMENT 'id from invites if associated with an invite',
  `email_hash` binary(16) NOT NULL,
  `overload_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`email_queue_id`),
  KEY `IDX_email_type` (`email_type_id`),
  KEY `IDX_email_hash` (`email_hash`)
) ENGINE=InnoDB COMMENT='Email Queue, removed once processed';

CREATE TABLE `email_send_log` (
  `email_id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT 'email id',
  `email_type_id` smallint(5) NOT NULL COMMENT 'id from email_type',
  `email_template_id` smallint(5) NOT NULL COMMENT 'id from email_template',
  `email_to` varchar(200) NOT NULL COMMENT 'address the email was sent to',
  `date_sent` datetime NOT NULL COMMENT 'date the email was sent',
  `date_opened` datetime NOT NULL COMMENT 'date the email was first opened',
  `date_clicked` datetime NOT NULL COMMENT 'date the link in the email was first clicked',
  `invite_id` int(11) NOT NULL DEFAULT '0' COMMENT 'id from invites if associated with an invite',
  `send_status` int(11) DEFAULT NULL COMMENT 'sending errors if any',
  `email_hash` binary(16) NOT NULL,
  PRIMARY KEY (`email_id`),
  KEY `IDX_type_tmpl_ver` (`email_type_id`,`email_template_id`),
  KEY `IDX_date_sent` (`date_sent`),
  KEY `IDX_email_hash` (`email_hash`)
) ENGINE=InnoDB COMMENT='record of all sent emails';

CREATE TABLE `email_template` (
  `email_template_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'template id',
  `email_type_id` smallint(5) NOT NULL DEFAULT '0' COMMENT 'id from types',
  `template_name` varchar(200) NOT NULL DEFAULT '' COMMENT 'template name',
  `template_version` int(20) NOT NULL DEFAULT '0' COMMENT 'template verstion',
  `template_subversion` int(20) NOT NULL DEFAULT '0',
  `reply_address` varchar(200) NOT NULL DEFAULT '' COMMENT 'reply email address',
  `reply_address_display_name` varchar(200) DEFAULT NULL,
  `template_text` text COMMENT 'text version',
  `template_html` text COMMENT 'html version',
  `email_subject` varchar(256) DEFAULT NULL COMMENT 'email subject',
  `language_code` varchar(12) DEFAULT NULL COMMENT 'language',
  `enabled` tinyint(3) NOT NULL DEFAULT '0' COMMENT 'if the template is enabled',
  `footer_template_id` int(11) NOT NULL DEFAULT '0' COMMENT 'what template to use as the footer, 0 for none',
  `header_template_id` int(20) NOT NULL DEFAULT '0' COMMENT 'what template to use as the header, 0 for none',
  `rotation_weight` tinyint(3) NOT NULL DEFAULT '0' COMMENT 'a relative value that determines the rotation of this template versus other templates of the same type.',
  `date_created` datetime NOT NULL COMMENT 'date this version was created',
  `description` varchar(256) DEFAULT NULL COMMENT 'a description of this templates function',
  `comments` varchar(256) DEFAULT NULL COMMENT 'notes about the efficacy of this template or reminders',
  `deleted` tinyint(3) DEFAULT '0' COMMENT 'shows if the template is marked as deleted',
  PRIMARY KEY (`email_template_id`),
  KEY `IDX_type_enabled` (`email_type_id`,`enabled`),
  KEY `IDX_header` (`header_template_id`),
  KEY `IDX_footer` (`footer_template_id`)
) ENGINE=InnoDB COMMENT='Email Templates';

CREATE TABLE `email_type` (
  `email_type_id` smallint(5) NOT NULL AUTO_INCREMENT COMMENT 'type id',
  `type_name` varchar(50) NOT NULL COMMENT 'name of this type',
  `enabled` tinyint(3) NOT NULL COMMENT 'if this type is enabled',
  `date_created` datetime NOT NULL COMMENT 'date this type was created',
  `description` varchar(256) DEFAULT NULL COMMENT 'description of this type',
  `tier` tinyint(3) NOT NULL COMMENT 'what tier will be used to send emails of this type',
  `list_query` text,
  `interval_que` int(10) DEFAULT NULL,
  `interval_chew` int(10) DEFAULT NULL,
  `deleted` tinyint(3) NOT NULL DEFAULT '0' COMMENT 'Marks if type is deleted',
  PRIMARY KEY (`email_type_id`)
) ENGINE=InnoDB COMMENT='Email Template types';

CREATE TABLE `filterlist` (
  `filterlist_id` int(11) NOT NULL AUTO_INCREMENT,
  `pattern_text` varchar(120) NOT NULL,
  PRIMARY KEY (`filterlist_id`),
  UNIQUE KEY `IDX_pattern_text` (`pattern_text`)
) ENGINE=InnoDB;

CREATE TABLE `filterlist_match_log` (
  `filterlist_match_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `filterlist_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `when_matched` datetime NOT NULL,
  PRIMARY KEY (`filterlist_match_log_id`),
  KEY `IDX_filterlist_id` (`filterlist_id`)
) ENGINE=InnoDB;

CREATE TABLE `kes_log` (
  `kes_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `level` varchar(50) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `exception` text,
  PRIMARY KEY (`kes_log_id`),
  KEY `IDX_date_created` (`date_created`)
) ENGINE=InnoDB;

CREATE TABLE `maintenance_log` (
  `maintenance_log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_occurred` datetime NOT NULL,
  `database_name` varchar(50) DEFAULT NULL,
  `procedure_name` varchar(200) DEFAULT NULL,
  `message_text` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`maintenance_log_id`)
) ENGINE=InnoDB;

CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  `interface_change` enum('T','F') NOT NULL DEFAULT 'F',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

CREATE TABLE `source_type` (
  `source_type_id` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `source_type` varchar(20) NOT NULL COMMENT 'source type',
  PRIMARY KEY (`source_type_id`),
  UNIQUE KEY `idx_source_type` (`source_type`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT COMMENT='various codes for source types';

CREATE TABLE `summary_bounce_log_daily` (
  `bounce_date` date NOT NULL,
  `source_type_id` tinyint(4) NOT NULL COMMENT 'PK',
  `bounce_type_id` tinyint(4) NOT NULL COMMENT 'PK',
  `email` varchar(255) NOT NULL,
  `bounce_count` int(11) NOT NULL,
  `email_hash` binary(16) NOT NULL,
  PRIMARY KEY (`bounce_date`,`source_type_id`,`bounce_type_id`,`email`),
  KEY `IDX_source_type_id` (`source_type_id`),
  KEY `IDX_bounce_type_id` (`bounce_type_id`),
  KEY `IDX_email_hash` (`email_hash`)
) ENGINE=InnoDB;

CREATE TABLE `tour_weekly_top_raved_hangouts` (
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `place_type_id` int(11) NOT NULL DEFAULT '0' COMMENT 'communities place type id',
  `zone_index_plain` int(11) NOT NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  PRIMARY KEY (`zone_instance_id`),
  KEY `IDX_place_type_id` (`place_type_id`),
  KEY `IDX_raves` (`raves`)
) ENGINE=InnoDB COMMENT='tour_weekly_top_raved_hangouts';

CREATE TABLE `traplist` (
  `traplist_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `email_hash` binary(16) NOT NULL,
  PRIMARY KEY (`traplist_id`),
  UNIQUE KEY `IDX_email` (`email`),
  KEY `IDX_email_hash` (`email_hash`)
) ENGINE=InnoDB;

CREATE TABLE `traplist_match_log` (
  `traplist_match_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `traplist_id` int(11) NOT NULL,
  `when_matched` datetime NOT NULL,
  PRIMARY KEY (`traplist_match_log_id`),
  KEY `IDX_traplist_id` (`traplist_id`)
) ENGINE=InnoDB;

CREATE TABLE `users_copy` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `signup_date` datetime NOT NULL DEFAULT '2000-00-00 00:00:00',
  `email` varchar(100) NOT NULL DEFAULT '',
  `email_hash` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `status_id` int(11) NOT NULL DEFAULT '0',
  `newsletter` char(1) NOT NULL DEFAULT '',
  `wok_player_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  KEY `IDX_signup_date` (`signup_date`),
  KEY `IDX_email_hash` (`email_hash`),
  KEY `IDX_wok_player_id` (`wok_player_id`)
) ENGINE=InnoDB;

CREATE TABLE `__users_copy` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `signup_date` datetime NOT NULL DEFAULT '2000-00-00 00:00:00',
  `email` varchar(100) NOT NULL DEFAULT '',
  `email_hash` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `status_id` int(11) NOT NULL DEFAULT '0',
  `newsletter` char(1) NOT NULL DEFAULT '',
  `wok_player_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

