-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS hex_email_address;

DELIMITER ;;
CREATE FUNCTION `hex_email_address`(in_email varchar(255)) RETURNS varchar(510)
    DETERMINISTIC
BEGIN
  RETURN HEX(in_email);
END
;;
DELIMITER ;