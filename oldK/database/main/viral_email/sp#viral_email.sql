-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS viral_email;

DELIMITER ;;
CREATE PROCEDURE `viral_email`()
BEGIN
-- process the rules in bounce rules to act upon data in email_bounce_log
CALL summarize_bounce_log(DATE(date_add(now(), INTERVAL -1 DAY)));
CALL process_bounce_rules(DATE(date_add(now(), INTERVAL -1 DAY)));
CALL build_blacklisted_today(DATE(now()));
		
END
;;
DELIMITER ;