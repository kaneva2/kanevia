-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 


/*Data for the table `email_source_type` */

insert ignore into `source_type`(`source_type_id`,`source_type`) values (1,'Scrubber1_Expl_Junk');
insert ignore into `source_type`(`source_type_id`,`source_type`) values (2,'Scrubber2_Kaneva@');
insert ignore into `source_type`(`source_type_id`,`source_type`) values (3,'Scrubber3_NDR');
insert ignore into `source_type`(`source_type_id`,`source_type`) values (4,'Website');
