-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 


/*Data for the table `email_bounce_type` */

insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (1,'NB','NON BOUNCE');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (2,'HB','HARD BOUNCE');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (3,'SB','SOFT BOUNCE - General');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (4,'SBDF','SOFT BOUNCE - Dns Failure');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (5,'SBMF','SOFT BOUNCE - Mailbox Full');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (6,'SBMS','SOFT BOUNCE - Message Size Too Large');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (7,'GB','General Bounce');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (8,'BN','BOUNCE WITH NO EMAIL ADDRESS');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (9,'MB','MAIL BLOCK - General');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (10,'MBKS','MAIL BLOCK - Known Spammer');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (11,'MBSD','MAIL BLOCK - Spam Detected');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (12,'MBAD','MAIL BLOCK - Attachment Detected');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (13,'MBRD','MAIL BLOCK - Relay Denied');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (14,'AR','AUTO REPLY');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (15,'TB','TRANSIENT BOUNCE');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (16,'SR','SUBSCRIBE REQUEST');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (17,'UR','UNSUBSCRIBE REQUEST');
insert  into `bounce_type`(`bounce_type_id`,`bounce_type_code`,`bounce_type_desc`) values (18,'CR','CHALLENGE RESPONSE');
