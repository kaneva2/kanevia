-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

/*
SQLyog Enterprise - MySQL GUI v7.14 
MySQL - 5.1.31-community-log : Database - viral_email
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Data for the table `bounce_rules` */

insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (1,14,1,1,1,1,'\'AUTO REPLY\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (2,14,3,1,1,0,'AUTO REPLY\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (3,14,2,1,1,0,'AUTO REPLY\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (4,8,1,1,1,0,'BOUNCE WITH NO EMAIL ADDRESS\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (5,8,3,1,1,0,'BOUNCE WITH NO EMAIL ADDRESS\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (6,8,2,1,1,0,'BOUNCE WITH NO EMAIL ADDRESS\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (7,18,1,1,1,1,'CHALLENGE RESPONSE\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (8,18,3,1,1,1,'CHALLENGE RESPONSE\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (9,18,2,1,1,1,'CHALLENGE RESPONSE\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (10,7,1,1,1,1,'General Bounce\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (11,7,3,40,6,0,'\'General Bounce\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (12,7,2,2,6,0,'\'General Bounce\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (13,2,1,1,1,1,'\'HARD BOUNCE\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (14,2,3,1,1,1,'\'HARD BOUNCE\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (15,2,2,1,1,1,'\'HARD BOUNCE\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (16,9,1,1,1,1,'\'MAIL BLOCK - General\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (17,9,3,1,1,1,'\'MAIL BLOCK - General\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (18,9,2,1,1,1,'\'MAIL BLOCK - General\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (19,12,1,1,1,1,'\'MAIL BLOCK - Attachment Detected\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (20,12,3,2,6,1,'\'MAIL BLOCK - Attachment Detected\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (21,12,2,2,6,1,'\'MAIL BLOCK - Attachment Detected\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (22,10,1,1,1,1,'\'MAIL BLOCK - Known Spammer\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (23,10,3,1,1,1,'\'MAIL BLOCK - Known Spammer\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (24,10,2,1,1,1,'\'MAIL BLOCK - Known Spammer\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (25,13,1,1,1,1,'\'MAIL BLOCK - Relay Denied\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (26,13,3,1,1,1,'\'MAIL BLOCK - Relay Denied\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (27,13,2,1,1,1,'\'MAIL BLOCK - Relay Denied\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (28,11,1,1,1,1,'\'MAIL BLOCK - Spam Detected\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (29,11,3,2,6,1,'\'MAIL BLOCK - Spam Detected\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (30,11,2,2,6,1,'\'MAIL BLOCK - Spam Detected\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (31,1,1,1,1,0,'\'NON BOUNCE\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (32,1,3,1,1,0,'\'NON BOUNCE\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (33,1,2,1,1,0,'\'NON BOUNCE\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (34,3,1,1,1,1,'\'SOFT BOUNCE - General\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (35,3,3,3,6,1,'\'SOFT BOUNCE - General\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (36,3,2,2,6,1,'\'SOFT BOUNCE - General\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (37,4,1,1,1,1,'\'SOFT BOUNCE - Dns Failure\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (38,4,3,2,6,1,'\'SOFT BOUNCE - Dns Failure\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (39,4,2,1,1,1,'\'SOFT BOUNCE - Dns Failure\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (40,5,1,1,1,1,'\'SOFT BOUNCE - Mailbox Full\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (41,5,3,6,6,1,'\'SOFT BOUNCE - Mailbox Full\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (42,5,2,6,6,1,'\'SOFT BOUNCE - Mailbox Full\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (43,6,1,1,1,1,'\'SOFT BOUNCE - Message Size Too Large\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (44,6,3,1,1,1,'\'SOFT BOUNCE - Message Size Too Large\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (45,6,2,1,1,1,'\'SOFT BOUNCE - Message Size Too Large\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (46,16,1,1,1,1,'\'SUBSCRIBE REQUEST\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (47,16,3,1,1,1,'\'SUBSCRIBE REQUEST\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (48,16,2,1,1,1,'\'SUBSCRIBE REQUEST\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (49,15,1,1,1,1,'\'TRANSIENT BOUNCE\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (50,15,3,10,6,1,'\'TRANSIENT BOUNCE\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (51,15,2,10,6,1,'\'TRANSIENT BOUNCE\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (52,17,1,1,1,1,'\'UNSUBSCRIBE REQUEST\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (53,17,3,1,1,1,'\'UNSUBSCRIBE REQUEST\'\r');
insert  into `bounce_rules`(`bounce_rule_id`,`bounce_type_id`,`source_type_id`,`rule_limit`,`day_range`,`action`,`definition`) values (54,17,2,1,1,1,'\'UNSUBSCRIBE REQUEST\'\r');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
