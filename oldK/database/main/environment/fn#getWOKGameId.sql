-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS getWOKGameId;

DELIMITER ;;
CREATE FUNCTION `getWOKGameId`() RETURNS int(11)
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Get the WOK game id for the current local environment.'
BEGIN
	DECLARE __wokGameId INTEGER;

	CALL validateEnvironmentHostname;

	SELECT wok_game_id INTO __wokGameId
	FROM environments
	WHERE environment_moniker = getLocalEnvironmentMoniker();

	RETURN __wokGameId;
END
;;
DELIMITER ;