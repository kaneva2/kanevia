-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS enableHostnameOverride;

DELIMITER ;;
CREATE PROCEDURE `enableHostnameOverride`()
    COMMENT 'Enable hostname override capability for the current local environment.'
BEGIN
	CALL setHostnameOverride('T');
END
;;
DELIMITER ;