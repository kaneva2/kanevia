-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

USE environment;
SELECT 'Starting Promotion... ',NOW();

-- Note that the format of this files name is a bit different since it contains
-- environment specific dml statements.  Specifically it stores the dns name
-- of the mysql host machine.
--
-- 
SET @dnsname='rc-newdb-01.kaneva-dc.net';



DROP TABLE IF EXISTS global_variables;
CREATE TABLE global_variables (
  variable_name 	varchar(64) 	NOT NULL,
  variable_value 	varchar(1024) 	DEFAULT NULL
) ENGINE=INNODB DEFAULT CHARSET=utf8;
CREATE UNIQUE INDEX global_variables_idx ON global_variables(variable_name);

INSERT INTO environment.global_variables VALUES( 'hostname',@dnsname);



-- stored routines
--
\. fn#getPhysicalHostname.sql


-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (2, 'Modify to make sp#getPhysicalHostName to reference new global_values table instead of a memory table no longer supported by MySql.', NOW()  );

SELECT 'Finished Promotion... ',NOW();



