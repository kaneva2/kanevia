-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS getPhysicalHostName;


DELIMITER ;;
CREATE  FUNCTION getPhysicalHostName() RETURNS varchar(255) CHARSET latin1
DETERMINISTIC
COMMENT 'Get the hostname of the physical database server.'
BEGIN
        DECLARE __hostname VARCHAR(255);
	    -- in the past this came from a memory table initialized by mysql that
		-- contained this value.  Support for this table was dropped
		-- 5.7?.  So this function now reads the value from a table
		-- that now must be configured accurately by us.  
		-- Further analysis of the environment schema may indicate
		-- that this function has no value (i.e. if it's simply being
		-- used to verify the configuration.
		--
        SELECT variable_value INTO __hostname
        FROM environment.global_variables
        WHERE variable_name = 'hostname';

        RETURN __hostname;
END
;;
DELIMITER ;