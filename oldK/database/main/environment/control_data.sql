-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

USE environment;

SELECT local_environment, environment_moniker, hostname_override
INTO @_le, @_em, @_ho
FROM local_environment
limit 1;

truncate local_environment;
truncate environments;

INSERT INTO environments ( environment_moniker, default_hostname, wok_game_id )
VALUES ('Test', 'wok-test.mysql.database.azure.com', 3296 );

INSERT INTO local_environment (local_environment, environment_moniker, hostname_override )
VALUES(@_le, @_em, @_ho);
