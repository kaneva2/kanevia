-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS getHostName;

DELIMITER ;;
CREATE FUNCTION `getHostName`() RETURNS varchar(255)
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Get the validated default hostname defined by the currently selected environment.'
BEGIN
	DECLARE __hostname VARCHAR(255);

	CALL validateEnvironmentHostname();

	SELECT default_hostname INTO __hostname
	FROM environments
	WHERE environment_moniker = getLocalEnvironmentMoniker();

	RETURN __hostname;
END
;;
DELIMITER ;