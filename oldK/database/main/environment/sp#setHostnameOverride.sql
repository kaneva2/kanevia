-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS setHostnameOverride;

DELIMITER ;;
CREATE PROCEDURE `setHostnameOverride`(IN __override ENUM('T','F'))
    COMMENT 'Set the hostname override capability for the current local environment.  Normally called by the enable/disable stored procedures.'
BEGIN
	UPDATE local_environment
	SET hostname_override = __override
	WHERE local_environment = 'local_environment';
END
;;
DELIMITER ;