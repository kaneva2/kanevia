-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `environments` (
  `environment_moniker` varchar(50) NOT NULL,
  `default_hostname` varchar(255) NOT NULL,
  `wok_game_id` int(11) NOT NULL,
  PRIMARY KEY (`environment_moniker`),
  UNIQUE KEY `environment_hostname_idx` (`default_hostname`),
  UNIQUE KEY `environment_wok_game_id_idx` (`wok_game_id`)
) ENGINE=InnoDB COMMENT='Contains a row for each potential Kaneva environment.';

CREATE TABLE `local_environment` (
  `local_environment` varchar(20) NOT NULL DEFAULT 'local_environment' COMMENT 'Contains the local_environment key to look up this servers environment data.',
  `environment_moniker` varchar(50) NOT NULL COMMENT 'Points to this server environment data.',
  `hostname_override` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'Override the default hostname for this environment to allow for testing.',
  PRIMARY KEY (`local_environment`),
  KEY `environment_moniker_fk` (`environment_moniker`),
  CONSTRAINT `environment_moniker_fk` FOREIGN KEY (`environment_moniker`) REFERENCES `environments` (`environment_moniker`)
) ENGINE=InnoDB COMMENT='Used to uniquely identify this servers environment data.';

CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT;

