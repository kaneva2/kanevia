-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS getHostnameOverride;

DELIMITER ;;
CREATE FUNCTION `getHostnameOverride`() RETURNS enum('T','F')
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Get the hostname override value for the current local environment.'
BEGIN
	DECLARE __override ENUM('T','F');

	SELECT hostname_override INTO __override
	FROM local_environment
	WHERE local_environment = 'local_environment';

	RETURN __override;
END
;;
DELIMITER ;