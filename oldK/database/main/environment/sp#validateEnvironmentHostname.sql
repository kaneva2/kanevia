-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS validateEnvironmentHostname;

DELIMITER ;;
CREATE PROCEDURE `validateEnvironmentHostname`()
    READS SQL DATA
    COMMENT 'Validate that the physical server hostname is the same as the environment default hostname unless override is enabled.'
BEGIN
	DECLARE invalidHostname CONDITION FOR SQLSTATE '45000';

	DECLARE __physicalHostname VARCHAR(255);
	DECLARE __defaultHostname VARCHAR(255);
	DECLARE __hostnameOverride ENUM('T','F');

	SELECT getPhysicalHostname() INTO __physicalHostname;

	SELECT hostname_override INTO __hostnameOverride
	FROM local_environment
	WHERE local_environment = 'local_environment';

	SELECT default_hostname INTO __defaultHostname
	FROM environments
	WHERE environment_moniker = getLocalEnvironmentMoniker();

	IF (__hostnameOverride = 'F') AND (__defaultHostname != __physicalHostname) THEN
		SIGNAL invalidHostname SET MESSAGE_TEXT = 'Default Hostname does not match Physical Hostname and override not permitted.';
	END IF;
END
;;
DELIMITER ;