-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS setLocalEnvironmentMoniker;

DELIMITER ;;
CREATE PROCEDURE `setLocalEnvironmentMoniker`(IN __moniker VARCHAR(20))
    COMMENT 'Set the current local environment moniker.'
BEGIN
	UPDATE local_environment 
	SET environment_moniker = __moniker
	WHERE local_environment = 'local_environment';
END
;;
DELIMITER ;