-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS getLocalEnvironmentMoniker;

DELIMITER ;;
CREATE FUNCTION `getLocalEnvironmentMoniker`() RETURNS varchar(20)
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Get the current local environment moniker.'
BEGIN
	DECLARE __moniker VARCHAR(20);

	SELECT environment_moniker INTO __moniker
	FROM local_environment
	WHERE local_environment = 'local_environment';

	RETURN __moniker;
END
;;
DELIMITER ;