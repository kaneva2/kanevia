-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `sphinx_indexer_control` (
  `index_id` int(11) NOT NULL AUTO_INCREMENT,
  `host_id` int(11) NOT NULL,
  `index_name` varchar(64) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `countdown_reset` int(11) NOT NULL DEFAULT '0',
  `countdown` int(11) NOT NULL DEFAULT '10',
  `last_item_count` int(10) unsigned NOT NULL DEFAULT '0',
  `last_indexed` int(10) unsigned NOT NULL DEFAULT '0',
  `op_interval_secs` int(10) unsigned NOT NULL DEFAULT '300',
  `next_index` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `host_id` (`host_id`,`index_name`)
) ENGINE=InnoDB;

CREATE TABLE `sphinx_indexer_hosts` (
  `host_id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(128) NOT NULL,
  `sphinx_end_point` varchar(128) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `poll_interval_secs` int(11) NOT NULL DEFAULT '300',
  `log_level` varchar(32) NOT NULL DEFAULT 'WARN',
  PRIMARY KEY (`host_id`),
  UNIQUE KEY `host` (`host`)
) ENGINE=InnoDB;

CREATE TABLE `sphinx_indexer_log` (
  `lid` int(11) NOT NULL AUTO_INCREMENT,
  `time` varchar(32) NOT NULL,
  `level` varchar(32) DEFAULT NULL,
  `class` varchar(32) NOT NULL,
  `message` varchar(512) NOT NULL,
  `host` varchar(128) DEFAULT NULL,
  `index_name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`lid`)
) ENGINE=InnoDB;

