-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS purge_sphinx_indexer_log;

DELIMITER ;;
CREATE PROCEDURE `purge_sphinx_indexer_log`(__purge_before_date DATE)
BEGIN

DELETE FROM `sphinx_indexer_log` WHERE `time` < __purge_before_date;

END
;;
DELIMITER ;