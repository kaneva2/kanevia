-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `countries_to_summarize` (
  `country` char(2) NOT NULL DEFAULT 'US' COMMENT 'country code of country',
  PRIMARY KEY (`country`)
) ENGINE=InnoDB COMMENT='Holds country codes of countries to summarize';

CREATE TABLE `gc_redemption_reports` (
  `import_date` date NOT NULL DEFAULT '0000-00-00',
  `vendor` varchar(255) NOT NULL DEFAULT '',
  `redemption_date` date NOT NULL DEFAULT '0000-00-00',
  `redemption_time` time NOT NULL DEFAULT '00:00:00',
  `product_description` varchar(255) NOT NULL DEFAULT '',
  `card_amount` int(11) NOT NULL DEFAULT '0',
  `pin_number` int(11) NOT NULL DEFAULT '0',
  `serial_number` varchar(255) NOT NULL DEFAULT '0',
  `refno` int(11) NOT NULL DEFAULT '0',
  `redeemed` enum('T','F') NOT NULL DEFAULT 'F',
  PRIMARY KEY (`import_date`,`refno`),
  KEY `Index_2` (`card_amount`),
  KEY `Index_3` (`redemption_date`)
) ENGINE=InnoDB;

CREATE TABLE `gc_sales_reports` (
  `import_date` date NOT NULL DEFAULT '0000-00-00',
  `merchant` varchar(255) NOT NULL DEFAULT '',
  `location` int(11) NOT NULL DEFAULT '0',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(5) NOT NULL DEFAULT '',
  `zip` int(11) NOT NULL DEFAULT '12345',
  `terminal` varchar(255) NOT NULL DEFAULT '',
  `product_description` varchar(255) NOT NULL DEFAULT '',
  `denomination` int(11) NOT NULL DEFAULT '0',
  `serial_number` varchar(255) NOT NULL DEFAULT '0',
  `sales_date` date NOT NULL DEFAULT '0000-00-00',
  `sales_time` time NOT NULL DEFAULT '00:00:00',
  `action` varchar(2) NOT NULL DEFAULT '',
  `sign` int(11) NOT NULL DEFAULT '0',
  `transactionamt` float NOT NULL DEFAULT '0',
  `refno` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`import_date`,`refno`),
  KEY `Index_2` (`transactionamt`),
  KEY `Index_3` (`sales_date`),
  KEY `Index_4` (`denomination`)
) ENGINE=InnoDB;

CREATE TABLE `maintenance_log` (
  `maintenance_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_occurred` datetime NOT NULL,
  `database_name` varchar(50) DEFAULT NULL,
  `procedure_name` varchar(200) DEFAULT NULL,
  `message_text` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`maintenance_log_id`)
) ENGINE=InnoDB;

CREATE TABLE `populated_worlds` (
  `snapshot_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `game_id` int(10) NOT NULL,
  `stpurl` varchar(300) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `game_population` int(11) NOT NULL,
  KEY `game_id_idx` (`game_id`),
  KEY `pw_st_idx` (`snapshot_timestamp`,`game_id`)
) ENGINE=InnoDB;

CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  `interface_change` enum('T','F') NOT NULL DEFAULT 'F',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_communities` (
  `community_count` int(11) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_placed_furniture` (
  `furniture_count` int(11) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_profiles` (
  `profile_count` int(11) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_purchased_clothing` (
  `clothing_purchase_count` int(11) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_site_members` (
  `member_count` int(11) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_user_active` (
  `user_count` int(11) NOT NULL,
  `play_time_avg` bigint(20) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_user_avatar_gender` (
  `user_count` int(11) NOT NULL DEFAULT '0',
  `gender` enum('M','F') NOT NULL DEFAULT 'M',
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`,`gender`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_user_balances` (
  `balance` double NOT NULL DEFAULT '0',
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL,
  PRIMARY KEY (`created_date`,`kei_point_id`),
  KEY `bal` (`balance`),
  KEY `kpi` (`kei_point_id`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_user_gender` (
  `user_count` int(11) NOT NULL,
  `gender` enum('M','F') NOT NULL DEFAULT 'M',
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`,`gender`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_user_inactive` (
  `user_count` int(11) NOT NULL,
  `play_time_avg` bigint(20) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_wok_user_average` (
  `play_time_avg` int(11) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `spender_activity` (
  `user_id` int(10) unsigned NOT NULL,
  `total_spent` double NOT NULL,
  `active_spender` enum('Y','N') NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `spender_activity_counts` (
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  `active_spender_counts` int(11) DEFAULT NULL,
  `inactive_spender_counts` int(11) DEFAULT NULL,
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `sp_run_log` (
  `sp_run_log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `sp_name` varchar(256) NOT NULL COMMENT 'name of the stored procedure',
  `run_start_time` datetime NOT NULL COMMENT 'when started',
  `run_end_time` datetime NOT NULL COMMENT 'when ended',
  `duration_seconds` int(11) NOT NULL COMMENT 'duration in seconds',
  PRIMARY KEY (`sp_run_log_id`),
  KEY `IDX_start_time` (`run_start_time`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT COMMENT='run duration information for stored procedures';

CREATE TABLE `summary_kaching_leader_by_allTime` (
  `user_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `user_name` varchar(80) NOT NULL,
  `wins` int(11) NOT NULL,
  `kills` int(11) NOT NULL,
  `deaths` int(11) NOT NULL,
  `coins` int(11) NOT NULL,
  `ratio_display_fraction` varchar(20) NOT NULL,
  `kills_to_deaths_ratio` decimal(11,4) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `IDX_wins` (`wins`),
  KEY `IDX_ratio` (`kills_to_deaths_ratio`),
  KEY `IDX_deaths` (`deaths`),
  KEY `IDX_coins` (`coins`),
  KEY `IDX_kills` (`kills`),
  KEY `IDX_player` (`player_id`)
) ENGINE=InnoDB;

CREATE TABLE `summary_kaching_leader_by_day` (
  `cur_date` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `user_name` varchar(80) NOT NULL,
  `wins` int(11) NOT NULL,
  `kills` int(11) NOT NULL,
  `deaths` int(11) NOT NULL,
  `coins` int(11) NOT NULL,
  `ratio_display_fraction` varchar(20) NOT NULL,
  `kills_to_deaths_ratio` decimal(11,4) NOT NULL,
  PRIMARY KEY (`cur_date`,`user_id`),
  KEY `IDX_wins` (`wins`),
  KEY `IDX_ratio` (`kills_to_deaths_ratio`),
  KEY `IDX_player` (`player_id`)
) ENGINE=InnoDB;

CREATE TABLE `summary_kaching_leader_by_hour` (
  `cur_date` date NOT NULL,
  `cur_hour` tinyint(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `user_name` varchar(80) NOT NULL,
  `wins` int(11) NOT NULL,
  `kills` int(11) NOT NULL,
  `deaths` int(11) NOT NULL,
  `coins` int(11) NOT NULL,
  PRIMARY KEY (`cur_date`,`cur_hour`,`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `summary_kaching_leader_by_last7days` (
  `user_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `user_name` varchar(80) NOT NULL,
  `wins` int(11) NOT NULL,
  `kills` int(11) NOT NULL,
  `deaths` int(11) NOT NULL,
  `coins` int(11) NOT NULL,
  `ratio_display_fraction` varchar(20) NOT NULL,
  `kills_to_deaths_ratio` decimal(11,4) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `IDX_wins` (`wins`),
  KEY `IDX_ratio` (`kills_to_deaths_ratio`),
  KEY `IDX_player` (`player_id`)
) ENGINE=InnoDB;

CREATE TABLE `summary_money_supply_by_user_day` (
  `user_id` int(11) NOT NULL,
  `record_date` date NOT NULL DEFAULT '0000-00-00',
  `balance` decimal(12,3) NOT NULL DEFAULT '0.000',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT',
  PRIMARY KEY (`user_id`,`record_date`,`kei_point_id`),
  KEY `IDX_record_date` (`record_date`)
) ENGINE=InnoDB;

CREATE TABLE `summary_ppt_by_day` (
  `user_id` int(10) unsigned NOT NULL,
  `dt_created` date NOT NULL,
  `total` double NOT NULL,
  `transaction_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`dt_created`)
) ENGINE=InnoDB;

CREATE TABLE `summary_ppt_completed_kpoint_transactions_by_day` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `transaction_date` date NOT NULL DEFAULT '0000-00-00',
  `total` double DEFAULT NULL,
  PRIMARY KEY (`user_id`,`transaction_date`),
  KEY `td` (`transaction_date`)
) ENGINE=InnoDB;

CREATE TABLE `summary_spent_credits_by_day_user` (
  `user_id` int(11) NOT NULL,
  `record_date` date NOT NULL,
  `spend_amount` decimal(12,3) NOT NULL DEFAULT '0.000',
  `kei_point_id` enum('KPOINT','GPOINT') NOT NULL DEFAULT 'GPOINT',
  PRIMARY KEY (`user_id`,`record_date`,`kei_point_id`),
  KEY `rd` (`record_date`),
  KEY `uid` (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `summary_transactions_by_day` (
  `transaction_date` date NOT NULL DEFAULT '0000-00-00',
  `payer_count` int(11) NOT NULL DEFAULT '0',
  `verified_transaction_count` int(11) NOT NULL DEFAULT '0',
  `failed_transaction_count` int(11) NOT NULL DEFAULT '0',
  `total_transaction_count` int(11) NOT NULL DEFAULT '0',
  `purchase_amount_debited` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`transaction_date`)
) ENGINE=InnoDB;

CREATE TABLE `summary_users_by_hour` (
  `cur_date` date NOT NULL DEFAULT '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL DEFAULT '0',
  `avg_logged_on_players` int(11) DEFAULT NULL,
  `max_logged_on_players` int(11) DEFAULT NULL,
  PRIMARY KEY (`cur_date`,`cur_hour`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `summary_users_by_minute` (
  `cur_date` date NOT NULL DEFAULT '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL DEFAULT '0',
  `cur_minute` tinyint(4) NOT NULL DEFAULT '0',
  `logged_on_players` int(11) DEFAULT NULL,
  PRIMARY KEY (`cur_date`,`cur_hour`,`cur_minute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `summary_users_by_server_by_minute` (
  `cur_date` date NOT NULL DEFAULT '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL DEFAULT '0',
  `cur_minute` tinyint(4) NOT NULL DEFAULT '0',
  `logged_on_players` int(11) DEFAULT NULL,
  `server_id` int(11) NOT NULL,
  PRIMARY KEY (`cur_date`,`cur_hour`,`cur_minute`,`server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `summary_user_transaction_count` (
  `kaneva_user_id` int(11) NOT NULL DEFAULT '0',
  `trans_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`kaneva_user_id`)
) ENGINE=InnoDB;

CREATE TABLE `table_in_maintenance` (
  `table_name` varchar(100) NOT NULL,
  `in_maintenance` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'table currently undergoing data maintenance (T/F)',
  PRIMARY KEY (`table_name`)
) ENGINE=InnoDB;

CREATE TABLE `world_population_trend` (
  `snapshot_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `wok_population` bigint(20) unsigned NOT NULL,
  `non_wok_population` bigint(20) unsigned NOT NULL,
  `hangout_population` bigint(20) unsigned NOT NULL,
  `home_population` bigint(20) unsigned NOT NULL,
  `permanent_zone_population` bigint(20) unsigned NOT NULL,
  KEY `wpt_st_idx` (`snapshot_timestamp`)
) ENGINE=InnoDB;

CREATE TABLE `__login_cohort_temp` (
  `user_count` smallint(6) DEFAULT NULL,
  `report_week` tinyint(4) DEFAULT NULL,
  `started_week` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB;

