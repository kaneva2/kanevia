-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS summary_kaching_leader_process_existing;

DELIMITER ;;
CREATE PROCEDURE `summary_kaching_leader_process_existing`()
BEGIN
DECLARE __begin_date DATETIME;
DECLARE __cur_date DATETIME;
DECLARE __end_date DATETIME;

SET __begin_date = (select max(cur_date) from summary_kaching_leader_by_day);
SET __cur_date = __begin_date;
SET __end_date = (SELECT DATE(NOW()));
WHILE __cur_date <= __end_date  DO
	CALL `report_kaneva`.`summary_kaching_leader_process_one_day` (__cur_date) ;
	SET __cur_date = (SELECT ADDDATE(__cur_date, INTERVAL 1 DAY));
END WHILE;
END
;;
DELIMITER ;