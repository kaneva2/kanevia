-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS summarize_transactions;

DELIMITER ;;
CREATE PROCEDURE `summarize_transactions`( start_date datetime, end_date datetime )
begin
SELECT NOW() INTO @start_time;

create table __payer_counts (
	transaction_date date not null default '0000-00-00',
	payer_count int not null default 0,
	purchase_amount_debited float not null default 0.00,
	primary key(transaction_date)
);

replace into report_kaneva.__payer_counts
select date(transaction_date),count(distinct user_id),sum(amount_debited) 
from kaneva.purchase_point_transactions where transaction_status=3 
group by date(transaction_date);

replace into report_kaneva.summary_transactions_by_day
select date(ppt.transaction_date),pc.payer_count,sum(if(ppt.transaction_status=3,1,0)),sum(if(ppt.transaction_status=12,1,0)),
count(ppt.point_transaction_id),pc.purchase_amount_debited
from kaneva.purchase_point_transactions ppt 
inner join report_kaneva.__payer_counts pc on date(ppt.transaction_date)=pc.transaction_date
where ppt.transaction_date between start_date and end_date
group by date(ppt.transaction_date);

drop table if exists report_kaneva.__payer_counts;

end
;;
DELIMITER ;