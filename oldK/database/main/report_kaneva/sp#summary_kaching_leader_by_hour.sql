-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS summary_kaching_leader_by_hour;

DELIMITER ;;
CREATE PROCEDURE `summary_kaching_leader_by_hour`(__cur_date	DATETIME, __cur_hour TINYINT)
BEGIN
SELECT NOW() INTO @start_time;

-- Delete any existing data in case this is to recalculate a bucket
DELETE FROM summary_kaching_leader_by_hour WHERE cur_date = __cur_date AND cur_hour = __cur_hour;

INSERT INTO summary_kaching_leader_by_hour 
			(cur_date,cur_hour,user_id,player_id,user_name,wins,kills,deaths,coins)
	SELECT __cur_date, __cur_hour, p.kaneva_user_id, pg.player_id, p.name
		,(SELECT count(win_lose) FROM kaching.player_game pg2 WHERE win_lose = 1 and pg2.player_id = pg.player_id)
		, sum(num_kills), sum(num_deaths)
		,(SELECT sum(c.coin_scores) FROM kaching.player_game_coin_stats c WHERE c.player_id = pg.player_id)
	FROM kaching.game g
		INNER JOIN kaching.player_game pg on g.game_id = pg.game_id
		INNER JOIN wok.players p on pg.player_id = p.player_id
	WHERE DATE(g.datetime) = __cur_date AND HOUR(g.datetime) = __cur_hour
	GROUP BY pg.player_id;

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('summary_kaching_leader_by_hour',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));

END
;;
DELIMITER ;