-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_communities;

DELIMITER ;;
CREATE PROCEDURE `ss_update_communities`(  )
begin
SELECT NOW() INTO @start_time;

replace into report_kaneva.snapshot_communities
	select count(c.community_id), @ymd
	from kaneva.communities_public c
	where c.status_id = 1 AND c.community_id NOT IN (101, 103, 102, 100, 99);

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('ss_update_communities',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));

end
;;
DELIMITER ;