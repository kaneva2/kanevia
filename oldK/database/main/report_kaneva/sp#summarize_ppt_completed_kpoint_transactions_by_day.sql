-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS summarize_ppt_completed_kpoint_transactions_by_day;

DELIMITER ;;
CREATE PROCEDURE `summarize_ppt_completed_kpoint_transactions_by_day`( start_date datetime, end_date datetime)
BEGIN
-- create a stored proc to run as the above one did to aggregate summarized data.
SELECT NOW() INTO @start_time;

replace into summary_ppt_completed_kpoint_transactions_by_day 
	select user_id,date(transaction_date) as d,sum(amount_debited) as debit_amount 
		from kaneva.purchase_point_transactions ppt 
		join kaneva.purchase_point_transaction_amounts ppta on ppt.point_transaction_id = ppta.point_transaction_id
		where ppt.transaction_status = 3
		and ppta.kei_point_id = 'KPOINT'
		and ppt.transaction_date BETWEEN start_date AND end_date 
		group by user_id,d;
		
INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('summarize_ppt_completed_kpoint_transactions_by_day',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));
		
END
;;
DELIMITER ;