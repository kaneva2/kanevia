-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_user_gender;

DELIMITER ;;
CREATE PROCEDURE `ss_update_user_gender`(  )
begin
SELECT NOW() INTO @start_time;

replace into report_kaneva.snapshot_user_gender
  select count(gu.user_id) as player_count,gender,@ymd
  FROM wok.game_users gu
	inner join kaneva.users ku on gu.kaneva_user_id=ku.user_id
	group by gender ;

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('ss_snapshot_user_gender',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));
end
;;
DELIMITER ;