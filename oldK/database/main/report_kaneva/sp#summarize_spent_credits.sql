-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS summarize_spent_credits;

DELIMITER ;;
CREATE PROCEDURE `summarize_spent_credits`( start_date datetime, end_date datetime)
BEGIN
SELECT NOW() INTO @start_time;

replace into report_kaneva.summary_spent_credits_by_day_user
	select user_id,date(created_date) as d,sum(transaction_amount) * -1 as debit_amount,kei_point_id 
	from kaneva.wok_transaction_log
	where created_date BETWEEN start_date AND end_date and transaction_amount<0 and transaction_type<>2 
	group by user_id,d,kei_point_id ;

END
;;
DELIMITER ;