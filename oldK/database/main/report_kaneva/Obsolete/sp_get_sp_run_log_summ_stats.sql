-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

SELECT sp_name, AVG(duration_seconds) AS AvgDuration, COUNT(*) AS HowManyRuns
FROM report_kaneva.sp_run_log 
WHERE run_start_time > ADDDATE(NOW(), INTERVAL -24 HOUR)
GROUP BY sp_name
ORDER BY sp_name;