-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS summarize_asset_info;
DELIMITER //
create procedure summarize_asset_info(in start_date datetime, in end_date datetime)
begin
-- Just move the asset summarization information thats part of update_report_stats to this procedure

SELECT NOW() INTO @start_time;
  Replace Into summary_assets_by_user_by_day
     select owner_id, date(created_date) as d, count(*) 
     from kaneva.assets WHERE created_date BETWEEN start_date AND end_date 
     group by owner_id, d;

  Replace Into summary_assets_by_day
     select date(created_date) as d, count(*) 
     from kaneva.assets WHERE created_date BETWEEN start_date AND end_date 
     group by d;

  Replace Into summary_assets_by_channel_by_day
  Select channel_id, date(ac.created_date) as d, count(*) 
  from kaneva.asset_channels ac
    Inner Join kaneva.assets a on a.asset_id = ac.asset_id
    WHERE ac.created_date BETWEEN start_date AND end_date AND
      a.asset_type_id=2 group by channel_id, d;

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('summarize_asset_info',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));
	  
END //
DELIMITER ;
