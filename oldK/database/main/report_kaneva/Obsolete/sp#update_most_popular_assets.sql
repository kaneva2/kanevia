-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DELIMITER $$

DROP PROCEDURE IF EXISTS `update_most_popular_assets`$$

CREATE PROCEDURE `update_most_popular_assets`()
BEGIN
SELECT NOW() INTO @start_time;

DELETE from summary_most_popular_assets;
COMMIT;
Insert into summary_most_popular_assets 
  SELECT SUM(sao.count) as ccount, sao.asset_id, a.asset_type_id  
  FROM kaneva.summary_assets_viewed sao
  INNER JOIN kaneva.vw_published_public_assets a on a.asset_id = sao.asset_id
  WHERE sao.dt_stamp >= ADDDATE(NOW(),INTERVAL -7 DAY) AND a.asset_type_id = 1 AND a.image_path IS NOT NULL AND a.image_path <> ''  
  GROUP BY sao.asset_id  
  ORDER BY ccount DESC  
  LIMIT 0, 12;

Insert into summary_most_popular_assets 
  SELECT SUM(sao.count) as ccount, sao.asset_id, a.asset_type_id  
  FROM kaneva.summary_assets_viewed sao
  INNER JOIN kaneva.vw_published_public_assets a on a.asset_id = sao.asset_id
  WHERE sao.dt_stamp >= ADDDATE(NOW(),INTERVAL -7 DAY) AND a.asset_type_id = 2 AND a.image_path IS NOT NULL AND a.image_path <> ''  
  GROUP BY sao.asset_id  
  ORDER BY ccount DESC  
  LIMIT 0, 12;

Insert into summary_most_popular_assets 
  SELECT SUM(sao.count) as ccount, sao.asset_id, a.asset_type_id  
  FROM kaneva.summary_assets_viewed sao
  INNER JOIN kaneva.vw_published_public_assets a on a.asset_id = sao.asset_id
  WHERE sao.dt_stamp >= ADDDATE(NOW(),INTERVAL -7 DAY) AND a.asset_type_id = 3 AND a.image_path IS NOT NULL AND a.image_path <> ''  
  GROUP BY sao.asset_id  
  ORDER BY ccount DESC  
  LIMIT 0, 12;

Insert into summary_most_popular_assets 
  SELECT SUM(sao.count) as ccount, sao.asset_id, a.asset_type_id  
  FROM kaneva.summary_assets_viewed sao
  INNER JOIN kaneva.vw_published_public_assets a on a.asset_id = sao.asset_id
  WHERE sao.dt_stamp >= ADDDATE(NOW(),INTERVAL -7 DAY) AND a.asset_type_id = 4 AND a.image_path IS NOT NULL AND a.image_path <> ''  
  GROUP BY sao.asset_id
  ORDER BY ccount DESC
  LIMIT 0, 12;

Insert into summary_most_popular_assets 
  SELECT SUM(sao.count) as ccount, sao.asset_id, a.asset_type_id
  FROM kaneva.summary_assets_viewed sao
  INNER JOIN kaneva.vw_published_public_assets a on a.asset_id = sao.asset_id
  WHERE sao.dt_stamp >= ADDDATE(NOW(),INTERVAL -7 DAY) AND a.asset_type_id = 5 AND a.image_path IS NOT NULL AND a.image_path <> ''
  GROUP BY sao.asset_id
  ORDER BY ccount DESC
  LIMIT 0, 12;

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('update_most_popular_assets',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));

END$$

DELIMITER ;