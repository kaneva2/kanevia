-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use report_kaneva;

--
-- CREATE TABLE: maintenance_log
--

CREATE TABLE IF NOT EXISTS maintenance_log
(
	maintenance_log_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	date_occurred DATETIME NOT NULL,
	database_name VARCHAR(50) NULL,
	procedure_name VARCHAR(200) NULL,
	message_text VARCHAR(500) NULL,
	CONSTRAINT PRIMARY KEY (maintenance_log_id)
)
	ENGINE = InnoDB;

-- new procedures
\. sp#add_maintenance_log.sql
\. report_kaneva#sp#purge_summary_money_supply_by_user_day.sql
\. kaneva#sp#summarize_most_popular.sql
\. summarize_new_channels.sql

DROP PROCEDURE IF EXISTS report_kaneva_SchemaChanges_v4;
DELIMITER //
CREATE PROCEDURE report_kaneva_SchemaChanges_v4()
BEGIN

IF NOT EXISTS (SELECT 1 FROM schema_versions WHERE version = 4) THEN
	INSERT INTO `schema_versions` ( `version`, `description` ) 
		VALUES (4, 'New purge, purge_summary_money_supply_by_user_day' );
END IF;

END //

DELIMITER ;

CALL report_kaneva_SchemaChanges_v4;
DROP PROCEDURE IF EXISTS report_kaneva_SchemaChanges_v4;
