-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use report_kaneva;

DELIMITER $$

DROP PROCEDURE IF EXISTS `report_kaneva`.`summarize_most_popular`$$

CREATE DEFINER=`admin`@`` PROCEDURE `summarize_most_popular`()
begin
DROP TABLE IF EXISTS `report_kaneva`.`__summary_most_popular`;
CREATE TABLE  `report_kaneva`.`__summary_most_popular` (
  `count` int(11) default NULL,
  `channel_id` int(11) NOT NULL,
  `username` varchar(80) default NULL,
  `gender` char(1) default NULL,
  `location` varchar(80) default NULL,
  `name_no_spaces` varchar(80) default NULL,
  `thumbnail_small_path` varchar(255) default NULL,
  `thumbnail_medium_path` varchar(255) default NULL,
  PRIMARY KEY  (`channel_id`),
  KEY `c` (`count`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;
	
	INSERT INTO report_kaneva.__summary_most_popular SELECT sum(count) AS count, cv.channel_id, x.username, x.gender, x.location, x.name_no_spaces, x.thumbnail_small_path, x.thumbnail_medium_path
	FROM kaneva.summary_channel_views cv
	INNER JOIN (SELECT community_id, u.gender, u.username, COALESCE( CONCAT( zc.city, ',', zc.state_code ) , c.country ) AS location, com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path
		FROM kaneva.communities com
		INNER JOIN kaneva.users_adult u ON u.user_id = com.creator_id
		INNER JOIN kaneva.countries c ON c.country_id = u.country
		Inner JOIN kaneva.zip_codes zc ON u.zip_code = zc.zip_code
		AND com.is_personal =1
		AND u.mature_profile =0
		AND com.thumbnail_path IS NOT NULL
	)x ON cv.channel_id = x.community_id
	WHERE cv.dt_stamp >= DATE(ADDDATE( NOW( ) , INTERVAL -7 DAY ))
	GROUP BY cv.channel_id
	ORDER BY count DESC
	LIMIT 0 , 12;
	
    
    DROP TABLE IF EXISTS report_kaneva.summary_most_popular;
    
    ALTER TABLE __summary_most_popular rename to summary_most_popular;
END$$

DELIMITER ;

DELIMITER $$

DROP PROCEDURE IF EXISTS `report_kaneva`.`summarize_new_channels`$$

CREATE DEFINER=`admin`@`` PROCEDURE `summarize_new_channels`()
begin
DROP TABLE IF EXISTS report_kaneva.__summary_new_channels;
CREATE TABLE  report_kaneva.__summary_new_channels (
  community_id int(11) NOT NULL,
  cx int(11) NOT NULL,
  community_name varchar(80) default NULL,
  name_no_spaces varchar(80) default NULL,
  description varchar(255) default NULL,
  is_public char(1) default NULL,
  is_adult char(1) default NULL,
  creator_id int(11) default NULL,
  username varchar(80) default NULL,
  thumbnail_small_path varchar(255) default NULL,
  thumbnail_medium_path varchar(255) default NULL,
  location varchar(80) default NULL,
  number_of_members int(11) default NULL,
  number_of_views int(11) default NULL,
  created_date datetime default NULL,
  is_personal tinyint(4) default NULL,
  PRIMARY KEY  (community_id),
  KEY cx (cx),
  KEY is_personal (is_personal),
  KEY created_date (created_date),
  KEY pub (is_public),
  KEY adult (is_adult)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
    Replace INTO report_kaneva.__summary_new_channels 
      Select * from
      (
         SELECT c.community_id, cs.number_of_diggs as cx, c.name as community_name, c.name_no_spaces, c.description, c.is_public, c.is_adult, c.creator_id, c.creator_username as username, c.thumbnail_small_path, c.thumbnail_medium_path,
                COALESCE(CONCAT(zc.city,',',zc.state_code),co.country) as location, cs.number_of_members, cs.number_of_views, c.created_date, c.is_personal 
         FROM kaneva.new_channels nc  
         INNER JOIN kaneva.communities c ON c.community_id = nc.channel_id  
         INNER JOIN kaneva.channel_stats cs ON cs.channel_id = c.community_id 
         INNER JOIN kaneva.users_adult u ON u.user_id = c.creator_id  
         LEFT OUTER JOIN kaneva.zip_codes zc ON u.zip_code = zc.zip_code  
         LEFT OUTER JOIN kaneva.countries co ON u.country = co.country_id  
         WHERE c.status_id = 1 AND c.thumbnail_path IS NOT NULL  AND 
               c.created_date < ADDDATE(NOW(),INTERVAL -2 DAY) AND 
               u.mature_profile = 0 AND 
               c.is_personal = 0 AND 
               c.over_21_required = 'N' AND 
               cs.number_of_members > 3  
         ORDER BY c.created_date DESC LIMIT 0, 100
      ) X group by X.creator_id order by created_date DESC limit 12;
  Replace INTO report_kaneva.__summary_new_channels 
    SELECT c.community_id, cs.number_of_diggs as cx, c.name as community_name, c.name_no_spaces, c.description, c.is_public, c.is_adult, c.creator_id, c.creator_username as username, c.thumbnail_small_path, c.thumbnail_medium_path,
           COALESCE(CONCAT(zc.city,',',zc.state_code),co.country) as location, cs.number_of_members, cs.number_of_views, c.created_date, c.is_personal 
    FROM kaneva.new_channels nc  
    INNER JOIN kaneva.communities c ON c.community_id = nc.channel_id  
    INNER JOIN kaneva.channel_stats cs ON cs.channel_id = c.community_id 
    INNER JOIN kaneva.users_adult u ON u.user_id = c.creator_id  
    LEFT OUTER JOIN kaneva.zip_codes zc ON u.zip_code = zc.zip_code  
    LEFT OUTER JOIN kaneva.countries co ON u.country = co.country_id  
    WHERE c.status_id = 1 AND
          c.thumbnail_path IS NOT NULL  AND 
          c.created_date < ADDDATE(NOW(),INTERVAL -2 DAY) AND 
          u.mature_profile = 0 AND 
          c.is_personal = 1 AND 
          number_of_diggs > 3 
    ORDER BY  c.created_date DESC LIMIT 0, 12;
    
    RENAME TABLE report_kaneva.summary_new_channels TO report_kaneva.__old__summary_new_channels, report_kaneva.__summary_new_channels to report_kaneva.summary_new_channels;
    DROP TABLE IF EXISTS report_kaneva.__old__summary_new_channels;
    
END$$

DELIMITER ;

DROP PROCEDURE IF EXISTS purge_summary_money_supply_by_user_day;

DELETE FROM `schema_versions` WHERE `version` = 4;

