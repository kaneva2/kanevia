-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DELIMITER $$

DROP PROCEDURE IF EXISTS `summarize_new_channels`$$

CREATE PROCEDURE `summarize_new_channels`()
begin
SELECT NOW() INTO @start_time;

DROP TABLE IF EXISTS __summary_new_channels;
CREATE TABLE  __summary_new_channels (
  community_id int(11) NOT NULL,
  cx int(11) NOT NULL,
  community_name varchar(80) default NULL,
  name_no_spaces varchar(80) default NULL,
  description varchar(255) default NULL,
  is_public char(1) default NULL,
  is_adult char(1) default NULL,
  creator_id int(11) default NULL,
  username varchar(80) default NULL,
  thumbnail_small_path varchar(255) default NULL,
  thumbnail_medium_path varchar(255) default NULL,
  location varchar(80) default NULL,
  number_of_members int(11) default NULL,
  number_of_views int(11) default NULL,
  created_date datetime default NULL,
  is_personal tinyint(4) default NULL,
  PRIMARY KEY  (community_id),
  KEY cx (cx),
  KEY is_personal (is_personal),
  KEY created_date (created_date),
  KEY pub (is_public),
  KEY adult (is_adult)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;


Replace INTO __summary_new_channels 
      Select * from
      (
         SELECT c.community_id, cs.number_of_diggs as cx, c.name as community_name, c.name_no_spaces, c.description, c.is_public, c.is_adult, 
				c.creator_id, c.creator_username as username, c.thumbnail_small_path, c.thumbnail_medium_path,
                COALESCE(CONCAT(zc.city,',',zc.state_code),co.country) as location, cs.number_of_members, cs.number_of_views, c.created_date, c.is_personal 
         FROM kaneva.new_channels nc  
         INNER JOIN kaneva.communities c ON c.community_id = nc.channel_id  
         INNER JOIN kaneva.channel_stats cs ON cs.channel_id = c.community_id 
         INNER JOIN kaneva.users u ON u.user_id = c.creator_id  
         LEFT OUTER JOIN kaneva.zip_codes zc ON u.zip_code = zc.zip_code  
         LEFT OUTER JOIN kaneva.countries co ON u.country = co.country_id  
         WHERE c.status_id = 1 AND c.thumbnail_path IS NOT NULL  AND 
               c.created_date < ADDDATE(NOW(),INTERVAL -2 DAY) AND 
               u.mature_profile = 0 AND u.age > 17 AND
               c.is_personal = 0 AND 
               c.over_21_required = 'N' AND 
               cs.number_of_members > 3  
         ORDER BY c.created_date DESC LIMIT 0, 100
      ) X group by X.creator_id order by created_date DESC limit 12;


Replace INTO report_kaneva.__summary_new_channels 
    SELECT c.community_id, cs.number_of_diggs as cx, c.name as community_name, c.name_no_spaces, c.description, c.is_public, c.is_adult, c.creator_id, 
			c.creator_username as username, c.thumbnail_small_path, c.thumbnail_medium_path,
           COALESCE(CONCAT(zc.city,',',zc.state_code),co.country) as location, cs.number_of_members, cs.number_of_views, c.created_date, c.is_personal 
    FROM kaneva.new_channels nc  
    INNER JOIN kaneva.communities c ON c.community_id = nc.channel_id  
    INNER JOIN kaneva.channel_stats cs ON cs.channel_id = c.community_id 
    INNER JOIN kaneva.users u ON u.user_id = c.creator_id  
    LEFT OUTER JOIN kaneva.zip_codes zc ON u.zip_code = zc.zip_code  
    LEFT OUTER JOIN kaneva.countries co ON u.country = co.country_id  
    WHERE c.status_id = 1 AND
          c.thumbnail_path IS NOT NULL  AND 
          c.created_date < ADDDATE(NOW(),INTERVAL -2 DAY) AND 
          u.mature_profile = 0 AND u.age > 17 AND
          c.is_personal = 1 AND 
          number_of_diggs > 3 
    ORDER BY  c.created_date DESC LIMIT 0, 12;
    
RENAME TABLE report_kaneva.summary_new_channels TO report_kaneva.__old__summary_new_channels, 
			report_kaneva.__summary_new_channels to report_kaneva.summary_new_channels;

DROP TABLE IF EXISTS report_kaneva.__old__summary_new_channels;
    
INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('summarize_new_channels',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));

END$$

DELIMITER ;