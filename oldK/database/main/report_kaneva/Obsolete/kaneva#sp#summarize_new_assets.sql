-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DELIMITER $$

DROP PROCEDURE IF EXISTS kaneva.summarize_new_assets$$
CREATE PROCEDURE  kaneva.summarize_new_assets()
begin

	DROP TABLE IF EXISTS kaneva.__summary_new_assets;
	CREATE TABLE  kaneva.__summary_new_assets (
		asset_id int(11) NOT NULL,
		asset_type_id int(11) default NULL,
		name varchar(80) default NULL,
		created_date datetime default NULL,
		asset_rating_id int(11) default NULL,
		publis_status_id int(11) default NULL,
		thumbnail_small_path varchar(255) default NULL,
		thumbnail_medium_path varchar(255) default NULL,
		thumbnail_gen tinyint(4) default NULL,
		user_id int(11) default NULL,
		username varchar(80) default NULL,
		status_id tinyint(4) default NULL,
		number_of_downloads int(11) default NULL,
		number_of_shares int(11) default NULL,
		number_of_comments int(11) default NULL,
		number_of_diggs int(11) default NULL,
		PRIMARY KEY  (asset_id)
	) ENGINE=MEMORY DEFAULT CHARSET=utf8;

Replace into __summary_new_assets
 SELECT a.asset_id, a.asset_type_id, aa.name, aa.created_date,  aa.asset_rating_id, aa.publish_status_id, aa.thumbnail_small_path, aa.thumbnail_medium_path, aa.thumbnail_gen,  aa.owner_id as user_id, aa.owner_username as username, aa.status_id, ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs 
  FROM new_assets_by_type a 
    INNER JOIN vw_published_public_assets aa ON aa.asset_id = a.asset_id 
    INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id   
    WHERE a.asset_type_id = 1  AND aa.image_path IS NOT null  AND ass.number_of_diggs >= 3 AND a.date_stamp < ADDDATE(NOW(),INTERVAL -2 DAY) GROUP BY a.asset_id ORDER BY a.date_stamp DESC  LIMIT 0, 8;

Replace into __summary_new_assets
 SELECT a.asset_id, a.asset_type_id, aa.name, aa.created_date,  aa.asset_rating_id, aa.publish_status_id, aa.thumbnail_small_path, aa.thumbnail_medium_path, aa.thumbnail_gen,  aa.owner_id as user_id, aa.owner_username as username, aa.status_id, ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs 
  FROM new_assets_by_type a 
    INNER JOIN vw_published_public_assets aa ON aa.asset_id = a.asset_id 
    INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id   
    WHERE a.asset_type_id = 2  AND aa.image_path IS NOT null  AND ass.number_of_diggs >= 3 AND a.date_stamp < ADDDATE(NOW(),INTERVAL -2 DAY) GROUP BY a.asset_id ORDER BY a.date_stamp DESC  LIMIT 0, 8;

Replace into __summary_new_assets
 SELECT a.asset_id, a.asset_type_id, aa.name, aa.created_date,  aa.asset_rating_id, aa.publish_status_id, aa.thumbnail_small_path, aa.thumbnail_medium_path, aa.thumbnail_gen,  aa.owner_id as user_id, aa.owner_username as username, aa.status_id, ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs 
  FROM new_assets_by_type a 
    INNER JOIN vw_published_public_assets aa ON aa.asset_id = a.asset_id 
    INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id   
    WHERE a.asset_type_id = 3  AND aa.image_path IS NOT null  AND ass.number_of_diggs >= 3 AND a.date_stamp < ADDDATE(NOW(),INTERVAL -2 DAY) GROUP BY a.asset_id ORDER BY a.date_stamp DESC  LIMIT 0, 8;

Replace into __summary_new_assets
 SELECT a.asset_id, a.asset_type_id, aa.name, aa.created_date,  aa.asset_rating_id, aa.publish_status_id, aa.thumbnail_small_path, aa.thumbnail_medium_path, aa.thumbnail_gen,  aa.owner_id as user_id, aa.owner_username as username, aa.status_id, ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs 
  FROM new_assets_by_type a 
    INNER JOIN vw_published_public_assets aa ON aa.asset_id = a.asset_id 
    INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id   
    WHERE a.asset_type_id = 4  AND aa.image_path IS NOT null  AND ass.number_of_diggs >= 3 AND a.date_stamp < ADDDATE(NOW(),INTERVAL -2 DAY) GROUP BY a.asset_id ORDER BY a.date_stamp DESC  LIMIT 0, 8;

Replace into __summary_new_assets
 SELECT a.asset_id, a.asset_type_id, aa.name, aa.created_date,  aa.asset_rating_id, aa.publish_status_id, aa.thumbnail_small_path, aa.thumbnail_medium_path, aa.thumbnail_gen,  aa.owner_id as user_id, aa.owner_username as username, aa.status_id, ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs 
  FROM new_assets_by_type a 
    INNER JOIN vw_published_public_assets aa ON aa.asset_id = a.asset_id 
    INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id   
    WHERE a.asset_type_id = 5  AND ass.number_of_diggs >= 3 AND a.date_stamp < ADDDATE(NOW(),INTERVAL -2 DAY) GROUP BY a.asset_id ORDER BY a.date_stamp DESC  LIMIT 0, 8;

		
    #Drop the existing popular keywords table
    DROP TABLE IF EXISTS kaneva.summary_new_assets;

    #RENAME new temp table to the original table so the new data is in place.
    ALTER TABLE __summary_new_assets rename to summary_new_assets;	

END $$

DELIMITER ;