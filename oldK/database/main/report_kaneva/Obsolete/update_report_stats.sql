-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_report_stats;
DELIMITER //
CREATE PROCEDURE  `update_report_stats`()
BEGIN
DECLARE start_date datetime;
DECLARE end_date datetime;
set start_date = concat(date(now()),' 00:00:00');
SET end_date = NOW();

  call update_wok_report_stats(start_date, end_date);
  call summarize_spent_credits( start_date, end_date);
  CALL update_community_req_counts();
  call summarize_transactions(start_date, end_date);
END //
DELIMITER ;
