-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS summarize_community_info;
DELIMITER //
create procedure summarize_community_info(in start_date datetime, in end_date datetime)
begin
-- Just move the community summarization information thats part of update_report_stats to this procedure
SELECT NOW() INTO @start_time;

  Replace Into summary_channel_diggs
     Select channel_id, DATE(created_date) as d, count(*) 
     from kaneva.channel_diggs WHERE created_date BETWEEN start_date AND end_date group by channel_id, d;

  Replace Into summary_comments_by_day
    Select channel_id, date(created_date) as d, count(*) as c 
    from kaneva.comments WHERE created_date BETWEEN start_date AND end_date group by channel_id, d;
      
  Replace Into summary_community_members
    Select cm.community_id, date(added_date) as d, count(*) as c 
    from kaneva.community_members cm
    Inner Join kaneva.communities com on com.community_id = cm.community_id
    WHERE com.is_personal=0 AND cm.added_date BETWEEN start_date AND end_date group by cm.community_id, d; 

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('summarize_community_info',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));
		  
END //
DELIMITER ;
