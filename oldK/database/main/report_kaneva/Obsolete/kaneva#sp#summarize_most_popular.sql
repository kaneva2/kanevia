-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS summarize_most_popular;
DELIMITER //
create procedure summarize_most_popular()
begin

DROP TABLE IF EXISTS `kaneva`.`__summary_most_popular`;
CREATE TABLE  `kaneva`.`__summary_most_popular` (
  `count` int(11) default NULL,
  `channel_id` int(11) NOT NULL,
  `username` varchar(80) default NULL,
  `gender` char(1) default NULL,
  `location` varchar(80) default NULL,
  `name_no_spaces` varchar(80) default NULL,
  `thumbnail_small_path` varchar(255) default NULL,
  `thumbnail_medium_path` varchar(255) default NULL,
  PRIMARY KEY  (`channel_id`),
  KEY `c` (`count`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

	# Move all of the located most popular summary information into the temporary table
	INSERT INTO kaneva.__summary_most_popular SELECT sum(count) AS count, cv.channel_id, x.username, x.gender, x.location, x.name_no_spaces, x.thumbnail_small_path, x.thumbnail_medium_path
	FROM summary_channel_views cv
	INNER JOIN (SELECT community_id, u.gender, u.username, COALESCE( CONCAT( zc.city, ',', zc.state_code ) , c.country ) AS location, com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path
		FROM communities com
		INNER JOIN users u ON u.user_id = com.creator_id
		INNER JOIN countries c ON c.country_id = u.country
		Inner JOIN zip_codes zc ON u.zip_code = zc.zip_code
		AND com.is_personal =1
		AND u.mature_profile =0
		AND DATE(NOW()) >= ADDDATE(birth_date, INTERVAL 18 YEAR) 
		AND com.thumbnail_path IS NOT NULL
	)x ON cv.channel_id = x.community_id
	WHERE cv.dt_stamp >= DATE(ADDDATE( NOW( ) , INTERVAL -7 DAY ))
	GROUP BY cv.channel_id
	ORDER BY count DESC
	LIMIT 0 , 12;
	
    #Drop the existing table
    DROP TABLE IF EXISTS kaneva.summary_most_popular;

    #RENAME new temp table to the original table so the new data is in place.
    ALTER TABLE __summary_most_popular rename to summary_most_popular;	

END //
DELIMITER ;