-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DELIMITER $$

DROP PROCEDURE IF EXISTS `wok`.`ss_update_user_gender`$$
CREATE DEFINER=`admin`@`` PROCEDURE  `wok`.`ss_update_user_gender`(  )
begin
  replace into wok.snapshot_user_gender
-- Split out men and women into one table with this query.  Replace your query below.
select count(gu.user_id) as player_count,gender,date(now())
  FROM wok.game_users gu
	inner join kaneva.users ku on gu.kaneva_user_id=ku.user_id
	group by gender ;
end $$

DELIMITER ;

