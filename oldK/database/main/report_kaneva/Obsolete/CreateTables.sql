-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

/*
SQLyog Enterprise - MySQL GUI v7.14 
MySQL - 5.0.67-community-log : Database - report_kaneva
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `__login_cohort_temp` */

CREATE TABLE `__login_cohort_temp` (
  `user_count` smallint(6) default NULL,
  `report_week` tinyint(4) default NULL,
  `started_week` tinyint(4) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `asset_owners` */

CREATE TABLE `asset_owners` (
  `user_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `cover_charge_change_log` */

CREATE TABLE `cover_charge_change_log` (
  `record_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `community_id` int(11) NOT NULL default '0',
  `cover_charge` float NOT NULL default '0',
  PRIMARY KEY  (`record_date`,`community_id`),
  KEY `rd` (`record_date`),
  KEY `c` (`community_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `data_for_rob` */

CREATE TABLE `data_for_rob` (
  `user_id` int(11) NOT NULL default '0' COMMENT 'unique id',
  `time_played` int(11) NOT NULL default '0' COMMENT 'number of minutes played',
  `dt` date default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `friend_counts` */

CREATE TABLE `friend_counts` (
  `user_id` int(11) NOT NULL default '0',
  `friend_count` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `gc_redemption_reports` */

CREATE TABLE `gc_redemption_reports` (
  `import_date` date NOT NULL default '0000-00-00',
  `vendor` varchar(255) NOT NULL default '',
  `redemption_date` date NOT NULL default '0000-00-00',
  `redemption_time` time NOT NULL default '00:00:00',
  `product_description` varchar(255) NOT NULL default '',
  `card_amount` int(11) NOT NULL default '0',
  `pin_number` int(11) NOT NULL default '0',
  `serial_number` varchar(255) NOT NULL default '0',
  `refno` int(11) NOT NULL default '0',
  `redeemed` enum('T','F') NOT NULL default 'F',
  PRIMARY KEY  (`import_date`,`refno`),
  KEY `Index_2` (`card_amount`),
  KEY `Index_3` (`redemption_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `gc_sales_reports` */

CREATE TABLE `gc_sales_reports` (
  `import_date` date NOT NULL default '0000-00-00',
  `merchant` varchar(255) NOT NULL default '',
  `location` int(11) NOT NULL default '0',
  `city` varchar(255) NOT NULL default '',
  `state` varchar(5) NOT NULL default '',
  `zip` int(11) NOT NULL default '12345',
  `terminal` varchar(255) NOT NULL default '',
  `product_description` varchar(255) NOT NULL default '',
  `denomination` int(11) NOT NULL default '0',
  `serial_number` varchar(255) NOT NULL default '0',
  `sales_date` date NOT NULL default '0000-00-00',
  `sales_time` time NOT NULL default '00:00:00',
  `action` varchar(2) NOT NULL default '',
  `sign` int(11) NOT NULL default '0',
  `transactionamt` float NOT NULL default '0',
  `refno` int(11) NOT NULL default '0',
  PRIMARY KEY  (`import_date`,`refno`),
  KEY `Index_2` (`transactionamt`),
  KEY `Index_3` (`sales_date`),
  KEY `Index_4` (`denomination`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `login_count` */

CREATE TABLE `login_count` (
  `user_id` int(11) NOT NULL default '0',
  `login_count` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `maintenance_log` */

CREATE TABLE `maintenance_log` (
  `maintenance_log_id` int(10) unsigned NOT NULL auto_increment,
  `date_occurred` datetime NOT NULL,
  `database_name` varchar(50) default NULL,
  `procedure_name` varchar(200) default NULL,
  `message_text` varchar(500) default NULL,
  PRIMARY KEY  (`maintenance_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=latin1;

/*Table structure for table `schema_versions` */

CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL default CURRENT_TIMESTAMP COMMENT 'when version was set',
  `interface_change` enum('T','F') NOT NULL default 'F',
  PRIMARY KEY  (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_communities` */

CREATE TABLE `snapshot_communities` (
  `community_count` int(11) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_placed_furniture` */

CREATE TABLE `snapshot_placed_furniture` (
  `furniture_count` int(11) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_profiles` */

CREATE TABLE `snapshot_profiles` (
  `profile_count` int(11) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_purchased_clothing` */

CREATE TABLE `snapshot_purchased_clothing` (
  `clothing_purchase_count` int(11) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_site_members` */

CREATE TABLE `snapshot_site_members` (
  `member_count` int(11) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_user_active` */

CREATE TABLE `snapshot_user_active` (
  `user_count` int(11) NOT NULL,
  `play_time_avg` bigint(20) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_user_avatar_gender` */

CREATE TABLE `snapshot_user_avatar_gender` (
  `user_count` int(11) NOT NULL default '0',
  `gender` enum('M','F') NOT NULL default 'M',
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`,`gender`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_user_balances` */

CREATE TABLE `snapshot_user_balances` (
  `balance` double NOT NULL default '0',
  `created_date` date NOT NULL default '0000-00-00',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL,
  PRIMARY KEY  (`created_date`,`kei_point_id`),
  KEY `bal` (`balance`),
  KEY `kpi` (`kei_point_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_user_gender` */

CREATE TABLE `snapshot_user_gender` (
  `user_count` int(11) NOT NULL,
  `gender` enum('M','F') NOT NULL default 'M',
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`,`gender`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_user_inactive` */

CREATE TABLE `snapshot_user_inactive` (
  `user_count` int(11) NOT NULL,
  `play_time_avg` bigint(20) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_wok_user_average` */

CREATE TABLE `snapshot_wok_user_average` (
  `play_time_avg` int(11) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `spender_activity` */

CREATE TABLE `spender_activity` (
  `user_id` int(10) unsigned NOT NULL,
  `total_spent` double NOT NULL,
  `active_spender` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `spender_activity_counts` */

CREATE TABLE `spender_activity_counts` (
  `created_date` date NOT NULL default '0000-00-00',
  `active_spender_counts` int(11) default NULL,
  `inactive_spender_counts` int(11) default NULL,
  PRIMARY KEY  (`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_assets_by_channel_by_day` */

CREATE TABLE `summary_assets_by_channel_by_day` (
  `channel_id` int(10) unsigned NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`channel_id`,`dt_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_assets_by_day` */

CREATE TABLE `summary_assets_by_day` (
  `date_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`date_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_assets_by_user_by_day` */

CREATE TABLE `summary_assets_by_user_by_day` (
  `user_id` int(10) unsigned NOT NULL,
  `date_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`date_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_channel_diggs` */

CREATE TABLE `summary_channel_diggs` (
  `channel_id` int(10) unsigned NOT NULL auto_increment,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`channel_id`,`dt_stamp`)
) ENGINE=InnoDB AUTO_INCREMENT=1002351492 DEFAULT CHARSET=latin1;

/*Table structure for table `summary_comments_by_day` */

CREATE TABLE `summary_comments_by_day` (
  `channel_id` int(10) unsigned NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`channel_id`,`dt_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_community_members` */

CREATE TABLE `summary_community_members` (
  `channel_id` int(10) unsigned NOT NULL auto_increment,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`channel_id`,`dt_stamp`)
) ENGINE=InnoDB AUTO_INCREMENT=1002318207 DEFAULT CHARSET=latin1;

/*Table structure for table `summary_cover_charge_by_community_by_day` */

CREATE TABLE `summary_cover_charge_by_community_by_day` (
  `record_date` date NOT NULL default '0000-00-00',
  `community_id` int(11) NOT NULL default '0',
  `amount_debited` float NOT NULL default '0',
  `pass_count` int(11) NOT NULL default '0',
  PRIMARY KEY  (`record_date`,`community_id`),
  KEY `rd` (`record_date`),
  KEY `c` (`community_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='details table for cover charge changes';

/*Table structure for table `summary_friends_by_day` */

CREATE TABLE `summary_friends_by_day` (
  `user_id` int(11) NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(11) default '0',
  PRIMARY KEY  (`user_id`,`dt_stamp`),
  KEY `dt` (`dt_stamp`),
  KEY `cnt` (`count`),
  KEY `uid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `summary_hangout_raves_all_time` */

CREATE TABLE `summary_hangout_raves_all_time` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) default NULL,
  `raves` int(11) NOT NULL,
  `zone_name` varchar(200) default NULL,
  `username` varchar(200) default NULL,
  PRIMARY KEY  (`zone_type`,`zone_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_hangout_raves_latest_day` */

CREATE TABLE `summary_hangout_raves_latest_day` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) default NULL,
  `raves` int(11) NOT NULL,
  `zone_name` varchar(200) default NULL,
  `username` varchar(200) default NULL,
  PRIMARY KEY  (`zone_type`,`zone_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_hangout_raves_latest_month` */

CREATE TABLE `summary_hangout_raves_latest_month` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) default NULL,
  `raves` int(11) NOT NULL,
  `zone_name` varchar(200) default NULL,
  `username` varchar(200) default NULL,
  PRIMARY KEY  (`zone_type`,`zone_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_hangout_raves_latest_week` */

CREATE TABLE `summary_hangout_raves_latest_week` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) default NULL,
  `raves` int(11) NOT NULL,
  `zone_name` varchar(200) default NULL,
  `username` varchar(200) default NULL,
  PRIMARY KEY  (`zone_type`,`zone_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_home_raves_all_time` */

CREATE TABLE `summary_home_raves_all_time` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) default NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(200) default NULL,
  PRIMARY KEY  (`zone_type`,`zone_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_home_raves_latest_day` */

CREATE TABLE `summary_home_raves_latest_day` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) default NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(200) default NULL,
  PRIMARY KEY  (`zone_type`,`zone_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_home_raves_latest_month` */

CREATE TABLE `summary_home_raves_latest_month` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) default NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(200) default NULL,
  PRIMARY KEY  (`zone_type`,`zone_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_home_raves_latest_week` */

CREATE TABLE `summary_home_raves_latest_week` (
  `zone_type` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `is_adult` char(1) default NULL,
  `raves` int(11) NOT NULL,
  `name` varchar(200) default NULL,
  PRIMARY KEY  (`zone_type`,`zone_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_kaching_leader_by_allTime` */

CREATE TABLE `summary_kaching_leader_by_allTime` (
  `user_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `user_name` varchar(80) NOT NULL,
  `wins` int(11) NOT NULL,
  `kills` int(11) NOT NULL,
  `deaths` int(11) NOT NULL,
  `coins` int(11) NOT NULL,
  `ratio_display_fraction` varchar(20) NOT NULL,
  `kills_to_deaths_ratio` decimal(11,4) NOT NULL,
  PRIMARY KEY  (`user_id`),
  KEY `IDX_wins` (`wins`),
  KEY `IDX_ratio` (`kills_to_deaths_ratio`),
  KEY `IDX_deaths` (`deaths`),
  KEY `IDX_coins` (`coins`),
  KEY `IDX_kills` (`kills`),
  KEY `IDX_player` (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_kaching_leader_by_day` */

CREATE TABLE `summary_kaching_leader_by_day` (
  `cur_date` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `user_name` varchar(80) NOT NULL,
  `wins` int(11) NOT NULL,
  `kills` int(11) NOT NULL,
  `deaths` int(11) NOT NULL,
  `coins` int(11) NOT NULL,
  `ratio_display_fraction` varchar(20) NOT NULL,
  `kills_to_deaths_ratio` decimal(11,4) NOT NULL,
  PRIMARY KEY  (`cur_date`,`user_id`),
  KEY `IDX_wins` (`wins`),
  KEY `IDX_ratio` (`kills_to_deaths_ratio`),
  KEY `IDX_player` (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_kaching_leader_by_hour` */

CREATE TABLE `summary_kaching_leader_by_hour` (
  `cur_date` date NOT NULL,
  `cur_hour` tinyint(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `user_name` varchar(80) NOT NULL,
  `wins` int(11) NOT NULL,
  `kills` int(11) NOT NULL,
  `deaths` int(11) NOT NULL,
  `coins` int(11) NOT NULL,
  PRIMARY KEY  (`cur_date`,`cur_hour`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_kaching_leader_by_last7days` */

CREATE TABLE `summary_kaching_leader_by_last7days` (
  `user_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `user_name` varchar(80) NOT NULL,
  `wins` int(11) NOT NULL,
  `kills` int(11) NOT NULL,
  `deaths` int(11) NOT NULL,
  `coins` int(11) NOT NULL,
  `ratio_display_fraction` varchar(20) NOT NULL,
  `kills_to_deaths_ratio` decimal(11,4) NOT NULL,
  PRIMARY KEY  (`user_id`),
  KEY `IDX_wins` (`wins`),
  KEY `IDX_ratio` (`kills_to_deaths_ratio`),
  KEY `IDX_player` (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_logons_by_day` */

CREATE TABLE `summary_logons_by_day` (
  `logon_date` date NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY  (`logon_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `summary_logons_by_hour` */

CREATE TABLE `summary_logons_by_hour` (
  `hour` int(11) NOT NULL,
  `logon_date` date NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY  USING BTREE (`logon_date`,`hour`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `summary_money_supply_by_user_day` */

CREATE TABLE `summary_money_supply_by_user_day` (
  `user_id` int(11) NOT NULL,
  `record_date` date NOT NULL default '0000-00-00',
  `balance` decimal(12,3) NOT NULL default '0.000',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  PRIMARY KEY  (`user_id`,`record_date`,`kei_point_id`),
  KEY `report_date` (`record_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `summary_most_popular` */

CREATE TABLE `summary_most_popular` (
  `count` int(11) default NULL,
  `channel_id` int(11) NOT NULL,
  `username` varchar(80) default NULL,
  `gender` char(1) default NULL,
  `location` varchar(80) default NULL,
  `name_no_spaces` varchar(80) default NULL,
  `thumbnail_small_path` varchar(255) default NULL,
  `thumbnail_medium_path` varchar(255) default NULL,
  PRIMARY KEY  (`channel_id`),
  KEY `c` (`count`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

/*Table structure for table `summary_most_popular_assets` */

CREATE TABLE `summary_most_popular_assets` (
  `count` int(10) unsigned NOT NULL,
  `asset_id` int(10) unsigned NOT NULL auto_increment,
  `asset_type_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`asset_id`),
  KEY `at` (`asset_type_id`),
  KEY `c` (`count`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

/*Table structure for table `summary_new_assets` */

CREATE TABLE `summary_new_assets` (
  `asset_id` int(11) NOT NULL,
  `asset_type_id` int(11) default NULL,
  `name` varchar(80) default NULL,
  `created_date` datetime default NULL,
  `asset_rating_id` int(11) default NULL,
  `publis_status_id` int(11) default NULL,
  `thumbnail_small_path` varchar(255) default NULL,
  `thumbnail_medium_path` varchar(255) default NULL,
  `thumbnail_gen` tinyint(4) default NULL,
  `user_id` int(11) default NULL,
  `username` varchar(80) default NULL,
  `status_id` tinyint(4) default NULL,
  `number_of_downloads` int(11) default NULL,
  `number_of_shares` int(11) default NULL,
  `number_of_comments` int(11) default NULL,
  `number_of_diggs` int(11) default NULL,
  PRIMARY KEY  (`asset_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

/*Table structure for table `summary_new_channels` */

CREATE TABLE `summary_new_channels` (
  `community_id` int(11) NOT NULL,
  `cx` int(11) NOT NULL,
  `community_name` varchar(80) default NULL,
  `name_no_spaces` varchar(80) default NULL,
  `description` varchar(255) default NULL,
  `is_public` char(1) default NULL,
  `is_adult` char(1) default NULL,
  `creator_id` int(11) default NULL,
  `username` varchar(80) default NULL,
  `thumbnail_small_path` varchar(255) default NULL,
  `thumbnail_medium_path` varchar(255) default NULL,
  `location` varchar(80) default NULL,
  `number_of_members` int(11) default NULL,
  `number_of_views` int(11) default NULL,
  `created_date` datetime default NULL,
  `is_personal` tinyint(4) default NULL,
  PRIMARY KEY  (`community_id`),
  KEY `cx` (`cx`),
  KEY `is_personal` (`is_personal`),
  KEY `created_date` (`created_date`),
  KEY `pub` (`is_public`),
  KEY `adult` (`is_adult`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_ppt_by_day` */

CREATE TABLE `summary_ppt_by_day` (
  `user_id` int(10) unsigned NOT NULL,
  `dt_created` date NOT NULL,
  `total` double NOT NULL,
  `transaction_count` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`dt_created`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_ppt_completed_kpoint_transactions_by_day` */

CREATE TABLE `summary_ppt_completed_kpoint_transactions_by_day` (
  `user_id` int(11) NOT NULL default '0',
  `transaction_date` date NOT NULL default '0000-00-00',
  `total` double default NULL,
  PRIMARY KEY  (`user_id`,`transaction_date`),
  KEY `td` (`transaction_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_signups_by_day` */

CREATE TABLE `summary_signups_by_day` (
  `date_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`date_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_spent_credits_by_day_user` */

CREATE TABLE `summary_spent_credits_by_day_user` (
  `user_id` int(11) NOT NULL,
  `record_date` date NOT NULL,
  `spend_amount` decimal(12,3) NOT NULL default '0.000',
  `kei_point_id` enum('KPOINT','GPOINT') NOT NULL default 'GPOINT',
  PRIMARY KEY  (`user_id`,`record_date`,`kei_point_id`),
  KEY `rd` (`record_date`),
  KEY `uid` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `summary_transaction_count` */

CREATE TABLE `summary_transaction_count` (
  `kaneva_user_id` int(11) NOT NULL default '0',
  `trans_count` int(11) NOT NULL default '0',
  PRIMARY KEY  (`kaneva_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `summary_transactions_by_day` */

CREATE TABLE `summary_transactions_by_day` (
  `transaction_date` date NOT NULL default '0000-00-00',
  `payer_count` int(11) NOT NULL default '0',
  `verified_transaction_count` int(11) NOT NULL default '0',
  `failed_transaction_count` int(11) NOT NULL default '0',
  `total_transaction_count` int(11) NOT NULL default '0',
  `purchase_amount_debited` float NOT NULL default '0',
  PRIMARY KEY  (`transaction_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `summary_user_transaction_count` */

CREATE TABLE `summary_user_transaction_count` (
  `kaneva_user_id` int(11) NOT NULL default '0',
  `trans_count` int(11) NOT NULL default '0',
  PRIMARY KEY  (`kaneva_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `summary_users_by_hour` */

CREATE TABLE `summary_users_by_hour` (
  `cur_date` date NOT NULL default '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL default '0',
  `avg_logged_on_players` int(11) default NULL,
  `max_logged_on_players` int(11) default NULL,
  PRIMARY KEY  (`cur_date`,`cur_hour`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `summary_users_by_minute` */

CREATE TABLE `summary_users_by_minute` (
  `cur_date` date NOT NULL default '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL default '0',
  `cur_minute` tinyint(4) NOT NULL default '0',
  `logged_on_players` int(11) default NULL,
  PRIMARY KEY  (`cur_date`,`cur_hour`,`cur_minute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `summary_users_by_server_by_hour` */

CREATE TABLE `summary_users_by_server_by_hour` (
  `cur_date` date NOT NULL default '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL default '0',
  `avg_logged_on_players` int(11) default NULL,
  `max_logged_on_players` int(11) default NULL,
  `server_id` int(11) NOT NULL,
  PRIMARY KEY  (`cur_date`,`cur_hour`,`server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `summary_users_by_server_by_minute` */

CREATE TABLE `summary_users_by_server_by_minute` (
  `cur_date` date NOT NULL default '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL default '0',
  `cur_minute` tinyint(4) NOT NULL default '0',
  `logged_on_players` int(11) default NULL,
  `server_id` int(11) NOT NULL,
  PRIMARY KEY  (`cur_date`,`cur_hour`,`cur_minute`,`server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `table_in_maintenance` */

CREATE TABLE `table_in_maintenance` (
  `table_name` varchar(100) NOT NULL,
  `in_maintenance` enum('T','F') NOT NULL default 'F' COMMENT 'table currently undergoing data maintenance (T/F)',
  PRIMARY KEY  (`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `topics_by_type_by_day` */

CREATE TABLE `topics_by_type_by_day` (
  `user_id` int(10) unsigned NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_download_count` */

CREATE TABLE `user_download_count` (
  `user_id` int(11) NOT NULL default '0',
  `download_count` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `user_downloads_of_wok` */

CREATE TABLE `user_downloads_of_wok` (
  `user_id` int(11) NOT NULL default '0',
  `username` varchar(128) NOT NULL default '0',
  `signup_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `time_played` int(11) default NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
