-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS summary_kaching_leader_by_allTime;

DELIMITER ;;
CREATE PROCEDURE `summary_kaching_leader_by_allTime`(__cur_date	DATETIME)
BEGIN

SELECT NOW() INTO @start_time;
UPDATE shard_info.table_in_maintenance 
	SET in_maintenance = 'T'
	WHERE table_name = 'summary_kaching_leader_by_allTime';
	
-- UPDATE players metrics if existing metrics for all time
UPDATE summary_kaching_leader_by_allTime a 
		INNER JOIN (SELECT user_id, player_id, user_name, sum(wins) as wins, sum(kills) as kills, 
							sum(deaths) as deaths, sum(coins) as coins
					FROM summary_kaching_leader_by_day 
					WHERE cur_date = __cur_date
					GROUP BY user_id) d ON a.user_id = d.user_id
	SET 
		a.wins = a.wins + d.wins, 
		a.kills = a.kills + d.kills, 
		a.deaths = a.deaths + d.deaths, 
		a.coins = a.coins + d.coins, 
		a.ratio_display_fraction = concat(a.kills + d.kills, '/',a.deaths + d.deaths), 
		a.kills_to_deaths_ratio =	CASE WHEN (a.deaths + d.deaths) = 0 AND (a.kills + d.kills) > 0 THEN (a.kills + d.kills) 
									WHEN (a.kills + d.kills) = 0 THEN 0 
									ELSE (a.kills + d.kills)/(a.deaths + d.deaths) END;

-- INSERT players metrics if first metrics for all time
INSERT INTO summary_kaching_leader_by_allTime
			(user_id,player_id,user_name,wins,kills,deaths,coins,ratio_display_fraction,kills_to_deaths_ratio)
	SELECT user_id, player_id, user_name, sum(wins), sum(kills), sum(deaths), sum(coins), 
			concat(sum(kills), '/',sum(deaths)), 
			CASE WHEN sum(deaths) = 0 AND sum(kills) > 0 THEN sum(kills) WHEN sum(kills) = 0 THEN 0 ELSE sum(kills)/sum(deaths) END
	FROM summary_kaching_leader_by_day
	WHERE cur_date = __cur_date
		AND user_id not in (SELECT distinct user_id FROM summary_kaching_leader_by_allTime)
	GROUP BY user_id;

UPDATE shard_info.table_in_maintenance 
	SET in_maintenance = 'F'
	WHERE table_name = 'summary_kaching_leader_by_allTime';

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('summary_kaching_leader_by_allTime',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));

END
;;
DELIMITER ;