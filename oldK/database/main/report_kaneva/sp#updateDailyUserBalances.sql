-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS updateDailyUserBalances;

DELIMITER ;;
CREATE PROCEDURE `updateDailyUserBalances`()
BEGIN
-- create procedure to update this table daily

-- populate the summary of money supply by users table daily
     SELECT NOW() INTO @start_time;

	INSERT INTO summary_money_supply_by_user_day 
	(user_id, record_date, balance, kei_point_id)
	Select user_id, DATE(now()) as cur_date, balance, kei_point_id
	from kaneva.user_balances ub 
	ON DUPLICATE KEY UPDATE balance = ub.balance;
	
END
;;
DELIMITER ;