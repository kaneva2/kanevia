-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_report_stats;

DELIMITER ;;
CREATE PROCEDURE `update_report_stats`()
BEGIN
DECLARE start_date DATETIME;
DECLARE end_date DATETIME;
SET start_date = CONCAT(DATE(NOW()),' 00:00:00');
SET end_date = NOW();

CALL update_wok_report_stats(start_date, end_date);

  
END
;;
DELIMITER ;