-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS summarize_ppt_day;

DELIMITER ;;
CREATE PROCEDURE `summarize_ppt_day`(start_date datetime, end_date datetime)
BEGIN
SELECT NOW() INTO @start_time;

replace into report_kaneva.summary_ppt_by_day
	select user_id,date(transaction_date) as d,sum(amount_debited) as debit_amount,count(point_transaction_id) 
	from kaneva.purchase_point_transactions
	where transaction_date BETWEEN start_date AND end_date and transaction_status=3 
	group by user_id,d ;

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('summarize_ppt_day',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));

END
;;
DELIMITER ;