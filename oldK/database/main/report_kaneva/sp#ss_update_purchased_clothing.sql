-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_purchased_clothing;

DELIMITER ;;
CREATE PROCEDURE `ss_update_purchased_clothing`(  )
begin
SELECT NOW() INTO @start_time;

replace into report_kaneva.snapshot_purchased_clothing
	select count(i.global_id) as purchase_count,@ymd as created_date
	from wok.inventories i
	where date(created_date)=@ymd
	and i.global_id not in (select distinct global_id from wok.dynamic_objects) 
	group by date(created_date);

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('ss_update_purchased_clothing',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));
end
;;
DELIMITER ;