-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_user_balances;

DELIMITER ;;
CREATE PROCEDURE `ss_update_user_balances`(  )
begin
SELECT NOW() INTO @start_time;

replace into report_kaneva.snapshot_user_balances
	select sum(balance), record_date, kei_point_id
	from report_kaneva.summary_money_supply_by_user_day
	where record_date=date(@ymd)
	group by record_date,kei_point_id;

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('ss_update_user_balances',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));
	
end
;;
DELIMITER ;