-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_snapshot_eod_data;

DELIMITER ;;
CREATE PROCEDURE `update_snapshot_eod_data`()
begin
set @ymd = date(now());
	call report_kaneva.ss_update_user_balances();
	call report_kaneva.ss_update_sitemembers();
	call report_kaneva.ss_update_user_gender();
	call report_kaneva.ss_update_user_avatar_gender();
	call report_kaneva.ss_update_user_active();
	call report_kaneva.ss_update_user_inactive();
	call report_kaneva.ss_update_wok_user_average();
	call report_kaneva.ss_update_communities();
	call report_kaneva.ss_update_placed_furniture();
	call report_kaneva.ss_update_purchased_clothing();
	
end
;;
DELIMITER ;