-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_user_avatar_gender;

DELIMITER ;;
CREATE PROCEDURE `ss_update_user_avatar_gender`(  )
begin
SELECT NOW() INTO @start_time;

replace into report_kaneva.snapshot_user_avatar_gender 
  select count(gu.user_id) as player_count,gender,@ymd
  from wok.game_users gu 
  inner join kaneva.users ku on gu.kaneva_user_id=ku.user_id 
  inner join wok.players p on gu.user_id=p.user_id 
  where date(p.created_date)=@ymd
  group by gender;
  
INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('ss_update_user_avatar_gender',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));
  
end
;;
DELIMITER ;