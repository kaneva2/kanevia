-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_user_active;

DELIMITER ;;
CREATE PROCEDURE `ss_update_user_active`(  )
begin
SELECT NOW() INTO @start_time;

replace into report_kaneva.snapshot_user_active
	select count(gu.user_id) as player_count,avg(time_played) as pta, @ymd
	from wok.game_users gu
	where time_played>=600;

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('ss_update_user_active',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));

end
;;
DELIMITER ;