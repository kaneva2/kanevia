-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS summary_kaching_leader_by_day;

DELIMITER ;;
CREATE PROCEDURE `summary_kaching_leader_by_day`(__cur_date	DATETIME)
BEGIN
SELECT NOW() INTO @start_time;

-- DELETE current day to recalculate
DELETE FROM summary_kaching_leader_by_day 
	WHERE cur_date = DATE(__cur_date);

-- INSERT players metrics if first metrics for the day
INSERT INTO summary_kaching_leader_by_day 
			(cur_date,user_id,player_id,user_name,wins,kills,deaths,coins,ratio_display_fraction,kills_to_deaths_ratio)
	SELECT cur_date, user_id, player_id, user_name, sum(wins), sum(kills), sum(deaths), sum(coins), 
			concat(sum(kills), '/',sum(deaths)), 
			CASE WHEN sum(deaths) = 0 AND sum(kills) > 0 THEN sum(kills) WHEN sum(kills) = 0 THEN 0 ELSE sum(kills)/sum(deaths) END
	FROM summary_kaching_leader_by_hour
	WHERE cur_date = DATE(__cur_date) 
	GROUP BY user_id, cur_date;

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('summary_kaching_leader_by_day',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));

END
;;
DELIMITER ;