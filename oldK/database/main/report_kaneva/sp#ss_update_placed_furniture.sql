-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_placed_furniture;

DELIMITER ;;
CREATE PROCEDURE `ss_update_placed_furniture`(  )
begin
SELECT NOW() INTO @start_time;

replace into report_kaneva.snapshot_placed_furniture
	select count(obj_placement_id) as placement_count,@ymd as created_date
	from wok.dynamic_objects 
	where date(created_date)=@ymd
	group by date(created_date);

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('ss_update_placed_furniture',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));
end
;;
DELIMITER ;