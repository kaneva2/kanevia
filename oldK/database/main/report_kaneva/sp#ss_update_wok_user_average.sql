-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_wok_user_average;

DELIMITER ;;
CREATE PROCEDURE `ss_update_wok_user_average`(  )
begin
SELECT NOW() INTO @start_time;

replace into report_kaneva.snapshot_wok_user_average
	select avg(gu.time_played) as play_avg, @ymd
	from wok.game_users gu;

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('ss_update_wok_user_average',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));
end
;;
DELIMITER ;