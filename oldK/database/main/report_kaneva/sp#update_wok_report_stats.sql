-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_wok_report_stats;

DELIMITER ;;
CREATE PROCEDURE `update_wok_report_stats`(IN start_date DATETIME, IN end_date DATETIME)
BEGIN
  CALL wok.updateMinuteUserCounts(start_date, end_date);
  call wok.updateUserHourStats(start_date, end_date);
  CALL report_kaneva.updateDailyUserBalances();
  call kaneva.summarize_ppt_day(start_date, end_date);
  call kaneva.summarize_ppt_completed_kpoint_transactions_by_day(start_date, end_date);
  call kaneva.update_spender_activity();
END
;;
DELIMITER ;