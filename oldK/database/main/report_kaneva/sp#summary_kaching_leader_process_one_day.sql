-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS summary_kaching_leader_process_one_day;

DELIMITER ;;
CREATE PROCEDURE `summary_kaching_leader_process_one_day`(__cur_date	DATETIME)
BEGIN
DECLARE __hour TINYINT DEFAULT 0;
WHILE __hour <= 23  DO
	CALL `report_kaneva`.`summary_kaching_leader_by_hour` (__cur_date, __hour) ;
	SET __hour = __hour + 1;
END WHILE;
CALL `report_kaneva`.`summary_kaching_leader_by_day` (__cur_date) ;
IF  __cur_date < DATE(NOW()) THEN
	TRUNCATE TABLE `report_kaneva`.`summary_kaching_leader_by_hour`;
END IF;
CALL `report_kaneva`.`summary_kaching_leader_by_allTime` (__cur_date) ;
END
;;
DELIMITER ;