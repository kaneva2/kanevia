-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `effects` (
  `effect` varchar(50) NOT NULL,
  `description` varchar(150) NOT NULL,
  PRIMARY KEY (`effect`)
) ENGINE=InnoDB COMMENT='Listing of possible effects that an experiment group may trigger.';

CREATE TABLE `experiments` (
  `experiment_id` varchar(50) NOT NULL COMMENT 'unique id of the experiment',
  `name` varchar(200) NOT NULL COMMENT 'name of the experiment',
  `description` text COMMENT 'description of the experiment',
  `status` enum('C','S','E','A') NOT NULL DEFAULT 'C' COMMENT 'status of the experiment: (C)reated -> (S)tarted -> (E)nded -> (A)rchived',
  `winning_group_id` varchar(50) DEFAULT NULL COMMENT 'id of the group that won the experiment (FK to experiment.experiment_groups)',
  `creation_date` datetime NOT NULL COMMENT 'datetime the experiment is created',
  `start_date` datetime DEFAULT NULL COMMENT 'datetime the experiment started',
  `end_date` datetime DEFAULT NULL COMMENT 'datetime the experiment ended',
  `assign_groups_on` enum('CreationOnly','Request') NOT NULL DEFAULT 'Request',
  `creator_id` int(11) NOT NULL COMMENT 'user_id of user who created the experiment.',
  `category` varchar(50) DEFAULT NULL,
  `assignment_target` enum('Users','Worlds') NOT NULL DEFAULT 'Users',
  PRIMARY KEY (`experiment_id`),
  KEY `GROUP_ID_FK` (`winning_group_id`),
  KEY `e_category_fk` (`category`),
  KEY `e_status_idx` (`status`,`assignment_target`),
  CONSTRAINT `experiments_ibfk_1` FOREIGN KEY (`winning_group_id`) REFERENCES `experiment_groups` (`experiment_group_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `e_category_fk` FOREIGN KEY (`category`) REFERENCES `experiment_categories` (`category`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='stores the experiment definition';

CREATE TABLE `experiment_categories` (
  `category` varchar(50) NOT NULL,
  `description` varchar(150) NOT NULL,
  PRIMARY KEY (`category`)
) ENGINE=InnoDB COMMENT='Category groupings for experiments.';

CREATE TABLE `experiment_groups` (
  `experiment_group_id` varchar(50) NOT NULL COMMENT 'unique id of the experiment_group',
  `experiment_id` varchar(50) NOT NULL COMMENT 'identifier of the experiment the group belongs to (FK to experiment.experiments)',
  `name` varchar(200) NOT NULL COMMENT 'name of the group',
  `description` text COMMENT 'description of the group',
  `creation_date` datetime NOT NULL COMMENT 'datetime the experiment is created',
  `label` enum('A','B','C','D','E','F','G','H','I','J') DEFAULT NULL COMMENT 'This can be used to identify a group as a letter like A or B',
  `assignment_percentage` int(2) NOT NULL DEFAULT '50' COMMENT 'Percentage of users that should be added to each group.',
  PRIMARY KEY (`experiment_group_id`),
  KEY `EXPERIMENT_ID_FK` (`experiment_id`),
  CONSTRAINT `experiment_groups_ibfk_1` FOREIGN KEY (`experiment_id`) REFERENCES `experiments` (`experiment_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='stores the different groups an experiment has';

CREATE TABLE `experiment_group_effects` (
  `experiment_group_id` varchar(50) NOT NULL,
  `effect` varchar(50) NOT NULL,
  PRIMARY KEY (`experiment_group_id`,`effect`),
  KEY `ege_effect_fk_idx` (`effect`),
  CONSTRAINT `ege_experiment_group_id_fk` FOREIGN KEY (`experiment_group_id`) REFERENCES `experiment_groups` (`experiment_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ege_effect_fk` FOREIGN KEY (`effect`) REFERENCES `effects` (`effect`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Join table tying experiment_groups to effects.';

CREATE TABLE `schema_versions` (
  `version` int(11) unsigned NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

CREATE TABLE `user_groups` (
  `user_group_id` varchar(50) NOT NULL COMMENT 'unique id of the user_group',
  `user_id` int(10) NOT NULL COMMENT 'id of the user participating in the experiment (FK to kaneva.users)',
  `experiment_group_id` varchar(50) NOT NULL COMMENT 'id of the group user receives (FK to experiment.experiment_groups)',
  `assignment_date` datetime NOT NULL,
  `conversion_date` datetime DEFAULT NULL,
  `assigned_via_url` char(1) NOT NULL DEFAULT 'F' COMMENT 'Indicates whether or not the user was assigned via our test url.',
  PRIMARY KEY (`user_group_id`),
  UNIQUE KEY `USER_ID_AND_GROUP_ID` (`user_id`,`experiment_group_id`),
  KEY `USER_ID_FK` (`user_id`),
  KEY `GROUP_ID_FK` (`experiment_group_id`),
  CONSTRAINT `user_groups_ibfk_2` FOREIGN KEY (`experiment_group_id`) REFERENCES `experiment_groups` (`experiment_group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Records user participation in the experiments.';

CREATE TABLE `world_groups` (
  `world_group_id` varchar(50) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `zone_type` int(11) NOT NULL,
  `experiment_group_id` varchar(50) NOT NULL,
  `assignment_date` datetime NOT NULL,
  `conversion_date` datetime DEFAULT NULL,
  PRIMARY KEY (`world_group_id`),
  UNIQUE KEY `world_experiment_group_unique` (`zone_instance_id`,`zone_type`,`experiment_group_id`),
  KEY `wg_experiment_group_fk_idx` (`experiment_group_id`),
  CONSTRAINT `wg_experiment_group_fk` FOREIGN KEY (`experiment_group_id`) REFERENCES `experiment_groups` (`experiment_group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Join table tying experiment_groups to worlds.';

CREATE TABLE `world_template_experiment_groups` (
  `template_id` int(10) unsigned NOT NULL,
  `experiment_group_id` varchar(50) NOT NULL,
  PRIMARY KEY (`template_id`,`experiment_group_id`),
  KEY `wteg_experiment_group_id_fk_idx` (`experiment_group_id`),
  CONSTRAINT `wteg_experiment_group_id_fk` FOREIGN KEY (`experiment_group_id`) REFERENCES `experiment_groups` (`experiment_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `wteg_template_id_fk` FOREIGN KEY (`template_id`) REFERENCES `wok`.`world_templates` (`template_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Join table linking templates to experiments and actions';

