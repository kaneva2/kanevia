-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `accept_types` (
  `accept_type_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`accept_type_id`)
) ENGINE=InnoDB COMMENT='Used to specify valid accept message types';

CREATE TABLE `bins` (
  `min_value` int(11) DEFAULT NULL,
  `max_value` int(11) DEFAULT NULL
) ENGINE=InnoDB;

CREATE TABLE `client_mtbf_log` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Report time',
  `user_id` int(11) NOT NULL COMMENT 'User Id',
  `total_runtime_sec` int(11) NOT NULL DEFAULT '0' COMMENT 'Total runtime (seconds) since last report',
  `total_runs` int(11) NOT NULL DEFAULT '0' COMMENT 'Total number of runs since last report',
  `total_crashes` int(11) NOT NULL DEFAULT '0' COMMENT 'Total number of crashes since last report.',
  `kep_client_version` varchar(256) NOT NULL DEFAULT '' COMMENT 'Kep Client version.',
  `runtime_id` varchar(50) DEFAULT NULL,
  UNIQUE KEY `primary_replaced` (`report_time`,`user_id`,`runtime_id`)
) ENGINE=InnoDB COMMENT='Tracking for user client runtime statistics, including total runtime, number of crashes, etc.';

CREATE TABLE `client_runtime_performance_log` (
  `runtime_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'UUID key for the runtime record.',
  `runtime_state` varchar(50) NOT NULL DEFAULT 'Running',
  `user_id` int(11) NOT NULL COMMENT 'ID of the user running the client.',
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Report time, used for archiving purposes.',
  `system_configuration_id` varchar(50) DEFAULT NULL COMMENT 'Link to a user_system_configuration record.',
  `kep_client_version` varchar(256) NOT NULL COMMENT 'Kep Client version.',
  `test_group` int(11) DEFAULT NULL COMMENT 'Test group, randomly assigned by the client.',
  `initial_ping` double unsigned DEFAULT NULL COMMENT 'Average of 3 test pings recorded at client launch. Measured in milliseconds.',
  `initial_disk_space` bigint(20) unsigned DEFAULT NULL COMMENT 'Total free hard disk space recorded at client launch. Measured in milliseconds.',
  `total_cpu_time` bigint(20) unsigned DEFAULT NULL COMMENT 'Total client CPU usage during this runtime. Measured in milliseconds.',
  `total_mem_checks` bigint(20) unsigned DEFAULT NULL COMMENT 'Total memory checks made by the client during this runtime.',
  `avg_mem_used` bigint(20) unsigned DEFAULT NULL COMMENT 'Average RAM used by the client during this runtime. Measured in bytes.',
  `min_mem_used` bigint(20) unsigned DEFAULT NULL COMMENT 'Minimum RAM used by the client during this runtime. Measured in bytes.',
  `max_mem_used` bigint(20) unsigned DEFAULT NULL COMMENT 'Maximum RAM used by the client during this runtime. Measured in bytes.',
  `total_tex_mem_checks` bigint(20) unsigned DEFAULT NULL COMMENT 'Total texture memory checks made by the client during this runtime.',
  `avg_tex_mem_avail` bigint(20) unsigned DEFAULT NULL COMMENT 'Average texture memory available during this runtime. Measured in bytes.',
  `min_tex_mem_avail` bigint(20) unsigned DEFAULT NULL COMMENT 'Minimum texture memory available during this runtime. Measured in bytes.',
  `max_tex_mem_avail` bigint(20) unsigned DEFAULT NULL COMMENT 'Maximum texture memory available during this runtime. Measured in bytes.',
  `avg_fps` double unsigned DEFAULT NULL COMMENT 'Average client frames per second during this runtime.',
  `ripper_detected` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`runtime_id`),
  KEY `crpl_system_config_idx` (`system_configuration_id`),
  CONSTRAINT `crpl_system_config_fk` FOREIGN KEY (`system_configuration_id`) REFERENCES `kaneva`.`user_system_configurations` (`system_configuration_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Measures performance data about a client during a given runtime.';

CREATE TABLE `client_version_history` (
  `kep_client_version` varchar(256) NOT NULL COMMENT 'Version number of the client.',
  `release_date` datetime NOT NULL COMMENT 'Date that the client was released.',
  `deprecated_date` datetime DEFAULT NULL COMMENT 'Date that the client should be considered replaced by its subsequent version.',
  `comment` varchar(256) DEFAULT NULL COMMENT 'Notes about the release.',
  PRIMARY KEY (`kep_client_version`)
) ENGINE=InnoDB COMMENT='Stores the history of client deployments.';

CREATE TABLE `client_webcall_performance_log` (
  `runtime_id` varchar(50) NOT NULL COMMENT 'Foreign Key to client_runtime_log.',
  `webcall_type` varchar(50) NOT NULL COMMENT 'Type of webcall being measured.',
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Report time, used for archiving purposes.',
  `total_calls_completed` int(11) unsigned DEFAULT NULL COMMENT 'Total number of calls of this type that completed during the given runtime.',
  `total_calls_errored` int(11) unsigned DEFAULT NULL COMMENT 'Total number of calls of this type that errored during the given runtime.',
  `avg_response_time` int(11) unsigned DEFAULT NULL COMMENT 'Average call response time during the runtime. Measured from when the call is queued to be sent until the response is received from the web.  Measured in milliseconds.',
  `avg_web_response_time` int(11) unsigned DEFAULT NULL COMMENT 'Average call response time during the runtime, including only the time from which the response is sent to when it is received. Measured in milliseconds.',
  `total_response_size` int(11) unsigned DEFAULT NULL COMMENT 'Total amount of data received in webcall response. Measured in bytes.',
  PRIMARY KEY (`runtime_id`,`webcall_type`),
  CONSTRAINT `fk_runtime_id` FOREIGN KEY (`runtime_id`) REFERENCES `client_runtime_performance_log` (`runtime_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB COMMENT='Measures performance data about a client''s webcall usage during a given runtime.';

CREATE TABLE `client_zone_performance_log` (
  `runtime_id` varchar(50) NOT NULL COMMENT 'Foreign key to client_runtime_log.',
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Report time, used for archiving purposes.',
  `game_id` int(11) NOT NULL COMMENT 'game_id of the zone.',
  `zone_index` int(11) NOT NULL COMMENT 'zone_index of the zone.',
  `zone_instance_id` int(11) NOT NULL COMMENT 'zone_instance_id of the zone.',
  `zone_type` int(11) NOT NULL COMMENT 'zone_type of the zone.',
  `ping` double unsigned DEFAULT NULL COMMENT 'Average of 3 test pings recorded at client launch. Measured in milliseconds.',
  `avg_fps` double unsigned DEFAULT NULL COMMENT 'Average client frames per second during the time the user was in this zone.',
  `avg_do` double unsigned DEFAULT NULL COMMENT 'Average dynamic objects present in the zone.',
  `min_do` double unsigned DEFAULT NULL COMMENT 'Min dynamic objects present in the zone.',
  `max_do` double unsigned DEFAULT NULL COMMENT 'Max dynamic objects present in the zone.',
  `avg_poly` double unsigned DEFAULT NULL COMMENT 'Average polygons present in the zone.',
  `min_poly` double unsigned DEFAULT NULL COMMENT 'Min polygons present in the zone.',
  `max_poly` double unsigned DEFAULT NULL COMMENT 'Max polygons present in the zone.',
  `zone_time` int(11) unsigned DEFAULT NULL COMMENT 'Time the user spent in the zone, in milliseconds.',
  PRIMARY KEY (`runtime_id`,`report_time`,`game_id`,`zone_index`,`zone_instance_id`,`zone_type`),
  CONSTRAINT `fk_zruntime_id` FOREIGN KEY (`runtime_id`) REFERENCES `client_runtime_performance_log` (`runtime_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB COMMENT='Measures performance data about a user''s use of a zone during a given runtime.';

CREATE TABLE `cl_counter_types` (
  `id` int(11) NOT NULL COMMENT 'code',
  `name` varchar(64) NOT NULL COMMENT 'decode',
  `description` varchar(256) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB COMMENT='Code list enumerating defined counter types';

CREATE TABLE `counters` (
  `counter_guid` varchar(50) NOT NULL COMMENT 'Unique identifier of the counter',
  `name` varchar(64) NOT NULL COMMENT 'User friendly name of the counter',
  `description` varchar(256) NOT NULL COMMENT 'Info about the counter',
  `counter_type_id` int(11) NOT NULL COMMENT 'FK into cl_counter_types',
  `default_scale` double DEFAULT '1',
  `enabled` char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`counter_guid`),
  UNIQUE KEY `ndx_CounterNames` (`name`),
  KEY `FK_counter_type` (`counter_type_id`),
  CONSTRAINT `FK_counter_type` FOREIGN KEY (`counter_type_id`) REFERENCES `cl_counter_types` (`id`)
) ENGINE=InnoDB COMMENT='Collection of counters being measured';

CREATE TABLE `counter_data` (
  `guid` varchar(50) NOT NULL,
  `ndx` int(11) NOT NULL AUTO_INCREMENT,
  `counter_guid` char(36) NOT NULL,
  `capture_dt` datetime NOT NULL,
  `formatted_value` varchar(128) DEFAULT NULL COMMENT 'unused',
  `a1` double DEFAULT NULL COMMENT 'Pair A Value 1',
  `a2` double DEFAULT NULL COMMENT 'Pair A Value 2',
  `b1` double DEFAULT NULL COMMENT 'Pair B Value 1',
  `b2` double DEFAULT NULL COMMENT 'Pair B Value 2',
  PRIMARY KEY (`guid`),
  UNIQUE KEY `ndx` (`ndx`),
  KEY `FK_counter` (`counter_guid`),
  KEY `capture_dt` (`capture_dt`),
  CONSTRAINT `FK_counter` FOREIGN KEY (`counter_guid`) REFERENCES `counters` (`counter_guid`)
) ENGINE=InnoDB COMMENT='Storage for captured metrics';

CREATE TABLE `kimstatus_population_samples` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sample_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kim_pop` bigint(20) unsigned NOT NULL,
  `wok_pop` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sample_time_idx` (`sample_time`)
) ENGINE=InnoDB;

CREATE TABLE `kim_error_log` (
  `error_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `kim_server` varchar(100) NOT NULL,
  `error_message` varchar(1000) NOT NULL,
  PRIMARY KEY (`error_timestamp`,`kim_server`)
) ENGINE=InnoDB;

CREATE TABLE `launcher_mtbf_log` (
  `runtime_id` varchar(50) NOT NULL,
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Report time',
  `total_runtime_sec` int(11) NOT NULL DEFAULT '0' COMMENT 'Total runtime (seconds) since last report',
  `total_runs` int(11) NOT NULL DEFAULT '0' COMMENT 'Total number of runs since last report',
  `total_crashes` int(11) NOT NULL DEFAULT '0' COMMENT 'Total number of crashes since last report.',
  PRIMARY KEY (`runtime_id`,`report_time`),
  CONSTRAINT `lmtbf_fk_runtime_id` FOREIGN KEY (`runtime_id`) REFERENCES `launcher_runtime_performance_log` (`runtime_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB COMMENT='Tracking for user launcher runtime statistics, including total runtime, number of crashes, etc.';

CREATE TABLE `launcher_patch_performance_log` (
  `runtime_id` varchar(50) NOT NULL COMMENT 'Foreign Key to launcher_runtime_log.',
  `dl_num_ok` bigint(20) unsigned NOT NULL DEFAULT '0',
  `dl_num_err` bigint(20) unsigned NOT NULL DEFAULT '0',
  `dl_ms` bigint(20) unsigned NOT NULL DEFAULT '0',
  `dl_bytes` bigint(20) unsigned NOT NULL DEFAULT '0',
  `dl_waits` bigint(20) unsigned NOT NULL DEFAULT '0',
  `dl_retries` bigint(20) unsigned NOT NULL DEFAULT '0',
  `dl_retries_err` bigint(20) unsigned NOT NULL DEFAULT '0',
  `zip_num_ok` bigint(20) unsigned NOT NULL DEFAULT '0',
  `zip_num_err` bigint(20) unsigned NOT NULL DEFAULT '0',
  `zip_ms` bigint(20) unsigned NOT NULL DEFAULT '0',
  `zip_bytes` bigint(20) unsigned NOT NULL DEFAULT '0',
  `pak_num_ok` bigint(20) unsigned NOT NULL DEFAULT '0',
  `pak_num_err` bigint(20) unsigned NOT NULL DEFAULT '0',
  `pak_ms` bigint(20) unsigned NOT NULL DEFAULT '0',
  `pak_bytes` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`runtime_id`),
  CONSTRAINT `lppl_fk_runtime_id` FOREIGN KEY (`runtime_id`) REFERENCES `launcher_runtime_performance_log` (`runtime_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB COMMENT='Measures performance data about the launcher''s patch performance during a given runtime.';

CREATE TABLE `launcher_runtime_performance_log` (
  `runtime_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'UUID key for the runtime record.',
  `runtime_state` varchar(10) NOT NULL DEFAULT 'Running',
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Report time, used for archiving purposes.',
  `system_configuration_id` varchar(50) DEFAULT NULL COMMENT 'Link to a user_system_configuration record.',
  `app_version` varchar(256) NOT NULL COMMENT 'App version.',
  `initial_disk_space` bigint(20) unsigned DEFAULT NULL COMMENT 'Total free hard disk space recorded at client launch. Measured in milliseconds.',
  `total_cpu_time` bigint(20) unsigned DEFAULT NULL COMMENT 'Total client CPU usage during this runtime. Measured in milliseconds.',
  `total_mem_checks` bigint(20) unsigned DEFAULT NULL COMMENT 'Total memory checks made by the client during this runtime.',
  `avg_mem_used` bigint(20) unsigned DEFAULT NULL COMMENT 'Average RAM used by the client during this runtime. Measured in bytes.',
  `min_mem_used` bigint(20) unsigned DEFAULT NULL COMMENT 'Minimum RAM used by the client during this runtime. Measured in bytes.',
  `max_mem_used` bigint(20) unsigned DEFAULT NULL COMMENT 'Maximum RAM used by the client during this runtime. Measured in bytes.',
  PRIMARY KEY (`runtime_id`),
  KEY `lrpl_system_config_idx` (`system_configuration_id`),
  KEY `lrpl_sys_config_state_idx` (`system_configuration_id`,`runtime_state`),
  CONSTRAINT `lrpl_system_config_fk` FOREIGN KEY (`system_configuration_id`) REFERENCES `kaneva`.`user_system_configurations` (`system_configuration_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Measures performance data about the launcher during a given runtime.';

CREATE TABLE `launcher_version_history` (
  `app_version` varchar(256) NOT NULL COMMENT 'Version number of the launcher.',
  `release_date` datetime NOT NULL COMMENT 'Date that the launcher was released.',
  `deprecated_date` datetime DEFAULT NULL COMMENT 'Date that the launcher should be considered replaced by its subsequent version.',
  `comment` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`app_version`)
) ENGINE=InnoDB COMMENT='Stores the history of launcher deployments.';

CREATE TABLE `launcher_webcall_performance_log` (
  `runtime_id` varchar(50) NOT NULL COMMENT 'Foreign Key to launcher_runtime_log.',
  `webcall_type` varchar(50) NOT NULL COMMENT 'Type of webcall being measured.',
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Report time, used for archiving purposes.',
  `total_calls_completed` int(11) unsigned DEFAULT NULL COMMENT 'Total number of calls of this type that completed during the given runtime.',
  `total_calls_errored` int(11) unsigned DEFAULT NULL COMMENT 'Total number of calls of this type that errored during the given runtime.',
  `avg_response_time` int(11) unsigned DEFAULT NULL COMMENT 'Average call response time during the runtime. Measured from when the call is queued to be sent until the response is received from the web.  Measured in milliseconds.',
  `avg_web_response_time` int(11) unsigned DEFAULT NULL COMMENT 'Average call response time during the runtime, including only the time from which the response is sent to when it is received. Measured in milliseconds.',
  `total_response_size` int(11) unsigned DEFAULT NULL COMMENT 'Total amount of data received in webcall response. Measured in bytes.',
  PRIMARY KEY (`runtime_id`,`webcall_type`),
  CONSTRAINT `lwpl_fk_runtime_id` FOREIGN KEY (`runtime_id`) REFERENCES `launcher_runtime_performance_log` (`runtime_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB COMMENT='Measures performance data about the launcher''s webcall usage during a given runtime.';

CREATE TABLE `message_types` (
  `message_type_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`message_type_id`)
) ENGINE=InnoDB COMMENT='Used to specify valid message types';

CREATE TABLE `new_user_funnel_progression` (
  `user_id` int(11) NOT NULL,
  `web_signup` datetime DEFAULT NULL COMMENT 'Date and time at which the user first joined Kaneva on Web.',
  `kim_gamelogin` datetime DEFAULT NULL COMMENT 'Date and time at which the user first authenticated with kim using gamelogin.aspx webcall, prior to connecting to XMPP server.  Considered first part of authentication.',
  `kim_validateuser` datetime DEFAULT NULL COMMENT 'Date and time at which the user first authenticated with kim using validateuser.aspx webcall, after connecting to XMPP server.  Considered second part of authentication.',
  `entered_avatar_select` datetime DEFAULT NULL COMMENT 'Date and time at which the user first entered the character select zone.',
  `completed_avatar_select` datetime DEFAULT NULL COMMENT 'Date and time at which the user successfully completed character select.',
  `entered_home` datetime DEFAULT NULL COMMENT 'Date and time at which the user first entered their home zone.',
  `completed_tutorial` datetime DEFAULT NULL COMMENT 'Date and time at which the user completed the new user tutorial.',
  `entered_game_world` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB COMMENT='Records user progression through key points in their Kaneva experience.';

CREATE TABLE `page_timing` (
  `idpage_timing` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_name` varchar(45) DEFAULT NULL,
  `time_in_ms` decimal(4,0) unsigned DEFAULT NULL,
  `datetime_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpage_timing`)
) ENGINE=InnoDB COMMENT='Page Time storage for metrics';

CREATE TABLE `patching_baseline` (
  `template_id` int(11) NOT NULL DEFAULT '0',
  `avg_time_ms` int(11) DEFAULT NULL,
  `sample_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB;

CREATE TABLE `progress_metrics` (
  `progress_id` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `time_ms` int(10) unsigned DEFAULT '0',
  `progress_tag` varchar(255) DEFAULT NULL,
  `progress_msg` varchar(255) DEFAULT NULL,
  `datetime_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_3dapp` enum('Y','N') DEFAULT 'N',
  `game_name` varchar(100) DEFAULT NULL,
  `game_id` int(11) DEFAULT NULL,
  `progress_guid` varchar(45) DEFAULT NULL,
  KEY `game_id` (`game_id`),
  KEY `game_name` (`game_name`),
  KEY `user_id` (`user_id`),
  KEY `progress_tag_idx` (`progress_tag`),
  KEY `datetime_created_idx` (`datetime_created`)
) ENGINE=InnoDB;

CREATE TABLE `requests` (
  `request_id` varchar(50) NOT NULL DEFAULT '',
  `request_type_id` int(11) NOT NULL,
  `user_id_sent` int(11) DEFAULT NULL,
  `game_id` int(11) DEFAULT NULL,
  `community_id` int(11) DEFAULT NULL,
  `request_date` datetime NOT NULL,
  PRIMARY KEY (`request_id`),
  KEY `fkRTID` (`request_type_id`),
  KEY `FKUserId` (`user_id_sent`),
  CONSTRAINT `fkRTID` FOREIGN KEY (`request_type_id`) REFERENCES `request_types` (`request_type_id`),
  CONSTRAINT `FKUserId` FOREIGN KEY (`user_id_sent`) REFERENCES `kaneva`.`users` (`user_id`)
) ENGINE=InnoDB COMMENT='Used to keep track of when a request is sent';

CREATE TABLE `request_types` (
  `request_type_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`request_type_id`)
) ENGINE=InnoDB COMMENT='Used to specify valid requests types';

CREATE TABLE `request_types_accepted` (
  `request_types_sent_id` varchar(50) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL,
  `accept_date` datetime NOT NULL,
  `accept_type_id` int(11) NOT NULL DEFAULT '0' COMMENT 'indication that player actually landed in zone after accepting an invite',
  `is_ie` char(1) NOT NULL DEFAULT 'N' COMMENT 'indication that browser performing the accept is Internet Explorer. Combine with user_agent',
  `user_agent` varchar(200) DEFAULT NULL COMMENT 'browser userAgent performing the accept',
  PRIMARY KEY (`request_types_sent_id`,`user_id`,`accept_date`,`accept_type_id`),
  KEY `FKat` (`accept_type_id`),
  CONSTRAINT `FKat` FOREIGN KEY (`accept_type_id`) REFERENCES `accept_types` (`accept_type_id`),
  CONSTRAINT `FKrta` FOREIGN KEY (`request_types_sent_id`) REFERENCES `request_types_sent` (`request_types_sent_id`)
) ENGINE=InnoDB COMMENT='Used to keep track of acceptance';

CREATE TABLE `request_types_sent` (
  `request_types_sent_id` varchar(50) NOT NULL,
  `request_id` varchar(50) NOT NULL,
  `message_type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'Used for 1 to 1 sending',
  `sent_date` datetime NOT NULL,
  PRIMARY KEY (`request_types_sent_id`),
  UNIQUE KEY `UKrm` (`request_id`,`message_type_id`),
  KEY `FKMT` (`message_type_id`),
  CONSTRAINT `FKMT` FOREIGN KEY (`message_type_id`) REFERENCES `message_types` (`message_type_id`),
  CONSTRAINT `FKRid` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`)
) ENGINE=InnoDB COMMENT='Used to keep track of message types sent (Email, Blast, PM, etc)';

CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  `interface_change` enum('T','F') DEFAULT 'F' COMMENT 'does this update require matching binaries',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

CREATE TABLE `sgitem_framework_activation_log` (
  `uuid` varchar(50) NOT NULL,
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `zone_instance_id` int(11) NOT NULL,
  `zone_type` int(11) NOT NULL,
  `username` varchar(22) NOT NULL,
  `activation_action` varchar(50) NOT NULL COMMENT 'Activate or Deactivate.',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB COMMENT='Records user activation/deactivation of the framework within a world.';

CREATE TABLE `sgitem_loot_log` (
  `uuid` varchar(50) NOT NULL,
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `zone_instance_id` int(11) NOT NULL,
  `zone_type` int(11) NOT NULL,
  `username` varchar(22) NOT NULL,
  `loot_source` varchar(50) NOT NULL COMMENT 'Source of the acquired loot.',
  `game_item_id` int(11) NOT NULL,
  `game_item_glid` int(11) DEFAULT NULL,
  `item_type` varchar(50) NOT NULL,
  `behavior` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB COMMENT='Records script_game_item looting by players in Player mode.';

CREATE TABLE `sgitem_paint_log` (
  `uuid` varchar(50) NOT NULL,
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `zone_instance_id` int(11) NOT NULL,
  `zone_type` int(11) NOT NULL,
  `username` varchar(22) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB COMMENT='Records user use of the Framework paint feature within a world.';

CREATE TABLE `sgitem_placement_log` (
  `uuid` varchar(50) NOT NULL,
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `zone_instance_id` int(11) NOT NULL,
  `zone_type` int(11) NOT NULL,
  `username` varchar(22) NOT NULL,
  `gameplay_mode` varchar(10) NOT NULL COMMENT 'Mode that the game item was in (Player or Creator)',
  `game_item_id` int(11) NOT NULL,
  `game_item_glid` int(11) DEFAULT NULL,
  `item_type` varchar(50) NOT NULL,
  `behavior` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB COMMENT='Records script_game_item placements by users.';

CREATE TABLE `tmp_game_template_ids` (
  `game_id` int(11) NOT NULL DEFAULT '0',
  `template_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`game_id`)
) ENGINE=InnoDB;

CREATE TABLE `v1_counter_data` (
  `guid` varchar(50) NOT NULL,
  `ndx` int(11) NOT NULL AUTO_INCREMENT,
  `counter_guid` varchar(50) NOT NULL,
  `key_guid` varchar(50) NOT NULL,
  `formatted_value` varchar(128) DEFAULT NULL COMMENT 'unused',
  `a1` double DEFAULT NULL COMMENT 'Pair A Value 1',
  `a2` double DEFAULT NULL COMMENT 'Pair A Value 2',
  `b1` double DEFAULT NULL COMMENT 'Pair B Value 1',
  `b2` double DEFAULT NULL COMMENT 'Pair B Value 2',
  `capture_dt` datetime NOT NULL,
  PRIMARY KEY (`guid`),
  UNIQUE KEY `NDX_v1` (`ndx`),
  KEY `FK_v1_cd` (`counter_guid`),
  KEY `FK_v1_ck` (`key_guid`),
  KEY `v1_cd_capture_dt_idx` (`capture_dt`),
  CONSTRAINT `FK_v1_cd` FOREIGN KEY (`counter_guid`) REFERENCES `counters` (`counter_guid`),
  CONSTRAINT `FK_v1_ck` FOREIGN KEY (`key_guid`) REFERENCES `v1_counter_keys` (`guid`)
) ENGINE=InnoDB COMMENT='Keyed storage for captured metrics';

CREATE TABLE `v1_counter_keys` (
  `guid` varchar(50) NOT NULL,
  `ndx` int(11) NOT NULL AUTO_INCREMENT,
  `k1` varchar(128) NOT NULL COMMENT 'Datum Key 1',
  `k2` varchar(128) NOT NULL COMMENT 'Datum Key 2',
  `k3` varchar(128) NOT NULL COMMENT 'Datum Key 3',
  `k4` varchar(128) NOT NULL COMMENT 'Datum Key 4',
  PRIMARY KEY (`guid`),
  UNIQUE KEY `ndx` (`ndx`),
  UNIQUE KEY `compound` (`k1`,`k2`,`k3`,`k4`)
) ENGINE=InnoDB COMMENT='External counter data keys';

CREATE TABLE `world_population_trend` (
  `wok_user_type` varchar(10) NOT NULL DEFAULT '',
  `snapshot_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `wok_population` bigint(20) unsigned NOT NULL,
  `non_wok_population` bigint(20) unsigned NOT NULL,
  `hangout_population` bigint(20) unsigned NOT NULL,
  `home_population` bigint(20) unsigned NOT NULL,
  `permanent_zone_population` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`wok_user_type`,`snapshot_timestamp`)
) ENGINE=InnoDB;

CREATE TABLE `world_template_population_trend` (
  `template_id` int(10) unsigned NOT NULL,
  `snapshot_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `new_wok_user_population` bigint(20) unsigned NOT NULL,
  `returning_wok_user_population` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`snapshot_timestamp`)
) ENGINE=InnoDB;

CREATE TABLE `x` (
  `id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

