-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS parse_progress_tag;

DELIMITER ;;
CREATE FUNCTION `parse_progress_tag`( progressTag VARCHAR(255) ) RETURNS varchar(255)
    DETERMINISTIC
BEGIN
	DECLARE _a1 VARCHAR(255);
	DECLARE _a2 VARCHAR(255);
	DECLARE _a3 VARCHAR(255);
	DECLARE _a4 VARCHAR(255);
	DECLARE _gameName VARCHAR(255);
	DECLARE _ofs1 INT;
	DECLARE _ofs2 INT;
	DECLARE _tmp VARCHAR(255);
	DECLARE _shortUrl VARCHAR(255);

	SELECT INSTR(progressTag, '//') INTO _ofs1;
	IF _ofs1<>0 THEN
		SET _ofs1 = _ofs1 + 2;
	END IF;
	SELECT SUBSTRING(progressTag, _ofs1) INTO _tmp;
	SELECT INSTR(_tmp, '/') INTO _ofs2;

	IF _ofs2<>0 THEN
		SELECT SUBSTRING(progressTag, 1, _ofs1+_ofs2-2) INTO _shortUrl;
	ELSE
		SET _shortUrl = progressTag;
	END IF;
	
	CALL wok.parse_vw_url(_shortUrl, _a1, _a2, _a3, _a4, _gameName );
	RETURN _gameName;
END
;;
DELIMITER ;