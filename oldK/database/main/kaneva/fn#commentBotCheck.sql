-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS commentBotCheck;

DELIMITER ;;
CREATE FUNCTION `commentBotCheck`(_userId  int(11)) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE _todaysPosts INT(11) DEFAULT 0;
  DECLARE _intialFlagValue INT;
  DECLARE _intervalFlag INT;
  DECLARE _temp INT;

  set _intialFlagValue = 20;
  set  _intervalFlag = 20;

   /* get the number of comments done this 24 hours by the user */
	SELECT comment_count INTO _todaysPosts FROM summary_comments_by_user_by_day WHERE Date(comment_date) = CURDATE() AND user_id = _userId;

   /* first check to see if the intial flag has been hit*/
	 IF _todaysPosts = _intialFlagValue THEN
      /*return true; */
      return 1;
   /* check to see if comments exceed the inital flag */
   ELSEIF _todaysPosts > _intialFlagValue  THEN
     set _temp =  _todaysPosts - _intialFlagValue;
     /* check to see if the interval flag has been hit above the inital flag */
     IF  (_temp %  _intervalFlag) = 0 then
       return 1;
     /* interval flag not reached */
     ELSE
       return -1;
     END IF;
   /* initial flag not hit */
   ELSE
       /*return false; */
       return -1;

	 END IF;
END
;;
DELIMITER ;