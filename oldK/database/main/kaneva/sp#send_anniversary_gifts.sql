-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS send_anniversary_gifts;

DELIMITER ;;
CREATE PROCEDURE `send_anniversary_gifts`()
BEGIN
DECLARE done INT;
DECLARE _from_id INT;
DECLARE _is_free INT;
DECLARE _user_id INT;
DECLARE _gift_id INT;
DECLARE _email_type INT;
DECLARE _email_template INT;
DECLARE _status INT;
DECLARE oneday CURSOR FOR 
	SELECT user_id 
		FROM kaneva.users 
		WHERE signup_date BETWEEN CONCAT(DATE(ADDDATE(NOW(), INTERVAL -2 DAY)),' 00:00:00') 
					AND  CONCAT(DATE(ADDDATE(NOW(), INTERVAL -1 DAY)),' 00:00:00')
		AND status_id IN (1,2);

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

SET _from_id        = 1;    
SET _is_free        = 1;    
SET _gift_id        = 3324; 
SET _email_type     = 20;   
SET _email_template = 0;    
SET _status         = 0;
SET done = 0;
OPEN oneday;
REPEAT
	SET _user_id = 0;
	FETCH oneday INTO _user_id;
	IF not done THEN
		IF _user_id > 0 THEN
			call wok.send_gift_to_player(_from_id, _user_id, _gift_id, _is_free, _email_type, _email_template);
        	SET _status = 1;
		END IF;
	END IF;
UNTIL done END REPEAT;


END
;;
DELIMITER ;