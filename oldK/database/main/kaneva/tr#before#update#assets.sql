-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS before_asset_update;

DELIMITER ;;
CREATE TRIGGER `before_asset_update` BEFORE UPDATE ON `assets` FOR EACH ROW BEGIN
SET new.published    = IF(( (new.publish_status_id=10) & ( (new.status_id=1) | (new.status_id=3)) ),'Y','N');
	
END
;;
DELIMITER ;