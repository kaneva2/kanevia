-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS sync_pending_friend_requests;

DELIMITER ;;
CREATE PROCEDURE `sync_pending_friend_requests`()
BEGIN
DECLARE __message_text VARCHAR(500);

CREATE TABLE IF NOT EXISTS kaneva._pending_friends
(	the_id	INT DEFAULT 0 NOT NULL,
	us_pending_friends INT DEFAULT 0 NOT NULL,
	fr_pending_friends INT DEFAULT 0 NOT NULL,
	us_pending_requests INT DEFAULT 0 NOT NULL,
	fr_pending_requests INT DEFAULT 0 NOT NULL,
	CONSTRAINT PRIMARY KEY (the_id)
);

CREATE TABLE IF NOT EXISTS kaneva._tmp_friend_requests
(	user_id INT DEFAULT 0 NOT NULL,
	fr_pending_requests INT DEFAULT 0 NOT NULL,
	CONSTRAINT PRIMARY KEY (user_id)
);

CREATE TABLE IF NOT EXISTS kaneva._tmp_friend_pending
(	user_id INT DEFAULT 0 NOT NULL,
	fr_pending_friends INT DEFAULT 0 NOT NULL,
	CONSTRAINT PRIMARY KEY (user_id)
);

TRUNCATE TABLE kaneva._pending_friends;	
TRUNCATE TABLE kaneva._tmp_friend_requests;	
TRUNCATE TABLE kaneva._tmp_friend_pending;	

INSERT INTO kaneva._pending_friends 
	SELECT user_id, number_of_pending, 0, number_of_requests, 0 	
	FROM kaneva.users_stats;

INSERT INTO kaneva._tmp_friend_requests
	SELECT friend_id, COUNT(user_id)
		FROM kaneva.friend_requests GROUP BY friend_id;
	
INSERT INTO kaneva._tmp_friend_pending
	SELECT user_id, COUNT(friend_id)
		FROM kaneva.friend_requests GROUP BY user_id;

INSERT INTO kaneva._pending_friends 
	SELECT user_id, 0, fr_pending_friends, 0, 0 FROM _tmp_friend_pending t
 	ON DUPLICATE KEY UPDATE fr_pending_friends = t.fr_pending_friends;

INSERT INTO kaneva._pending_friends
	SELECT user_id, 0, 0, 0, fr_pending_requests FROM _tmp_friend_requests t
 	ON DUPLICATE KEY UPDATE fr_pending_requests = t.fr_pending_requests;

-- SELECT * FROM kaneva._pending_friends WHERE us_pending_friends != fr_pending_friends OR us_pending_requests != fr_pending_requests;

UPDATE kaneva.users_stats us, kaneva._pending_friends pf
	SET us.number_of_pending = pf.fr_pending_friends
	WHERE us.user_id = pf.the_id AND (pf.us_pending_friends != pf.fr_pending_friends);

SELECT CONCAT('Sync step 1 of 2 completed ..... ',CAST(ROW_COUNT() AS CHAR), ' pending users_stats updated.') INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'sync_pending_friend_requests',__message_text);

UPDATE kaneva.users_stats us, kaneva._pending_friends pf
	SET us.number_of_requests = pf.fr_pending_requests
	WHERE us.user_id = pf.the_id AND (pf.us_pending_requests != pf.fr_pending_requests);

SELECT CONCAT('Sync step 2 of 2 completed ..... ',CAST(ROW_COUNT() AS CHAR), ' requests users_stats updated.') INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'sync_pending_friend_requests',__message_text);

END
;;
DELIMITER ;