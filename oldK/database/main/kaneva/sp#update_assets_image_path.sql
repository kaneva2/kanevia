-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_image_path;

DELIMITER ;;
CREATE PROCEDURE `update_assets_image_path`(IN __asset_id INT, IN __image_path VARCHAR(255),
		IN __image_type VARCHAR(50))
BEGIN

UPDATE assets 
	SET image_path = __image_path,
	image_type = __image_type
	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;