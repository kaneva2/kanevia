-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS insert_billing_subscription;

DELIMITER ;;
CREATE PROCEDURE `insert_billing_subscription`( IN userId INT(11),
		IN billingSubscriptionId VARCHAR(30) )
BEGIN
	DECLARE __seq_number SMALLINT;

	SELECT subscription_sequence_number INTO __seq_number FROM users_billing_subscriptions
		WHERE user_id = userId ORDER BY subscription_sequence_number DESC LIMIT 1;

	IF __seq_number IS NULL THEN
		SET __seq_number = 0;
	END IF;

	INSERT INTO users_billing_subscriptions
		       ( billing_subscription_id, user_id, subscription_sequence_number, billing_subscription_status, last_modified_date )
		VALUES ( billingSubscriptionId, userId, __seq_number+1, 1, NOW() );

END
;;
DELIMITER ;