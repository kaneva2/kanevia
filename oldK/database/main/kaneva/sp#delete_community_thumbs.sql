-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_community_thumbs;

DELIMITER ;;
CREATE PROCEDURE `delete_community_thumbs`(_communityId INTEGER UNSIGNED)
BEGIN

  START TRANSACTION;
	    
    
    UPDATE 
   	communities
    SET 
	thumbnail_path = '',
	thumbnail_type = '',
	thumbnail_small_path = '',
	thumbnail_medium_path = '',
	thumbnail_large_path = '',
	thumbnail_xlarge_path = '',
	has_thumbnail = 'N'
    WHERE 
   	community_id = _communityId;

    
    SELECT row_count();

  COMMIT;

END
;;
DELIMITER ;