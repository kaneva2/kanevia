-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_channel_diggs;

DELIMITER ;;
CREATE PROCEDURE `add_channel_diggs`(IN __user_id INT, IN __channel_id INT, IN __rave_count INT)
BEGIN

INSERT INTO channel_diggs 
	(user_id, channel_id, rave_count, created_date)
	VALUES
	(__user_id, __channel_id, IFNULL(__rave_count,0), NOW());

UPDATE channel_stats SET number_of_diggs = number_of_diggs + 1 WHERE channel_id = __channel_id;

END
;;
DELIMITER ;