-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_communities_name;

DELIMITER ;;
CREATE PROCEDURE `update_communities_name`(IN __community_id INT, IN __name VARCHAR(100), 
		IN __description VARCHAR(255))
BEGIN
DECLARE _zoneIndex INT(11) DEFAULT NULL;
DECLARE _channelZoneId INT(11) DEFAULT NULL;
DECLARE _old_name	VARCHAR(100);

SELECT `name` INTO _old_name
	FROM communities WHERE community_id = __community_id;

UPDATE communities 
	SET `name` = __name,
	-- should we also update name_no_spaces?
        `description` = __description
	WHERE community_id = __community_id;

IF _old_name <> __name THEN
	SELECT channel_zone_id, zone_index INTO _channelZoneId, _zoneIndex
	  FROM wok.channel_zones cz
	 WHERE cz.zone_instance_id = __community_id 
	   AND cz.zone_type = 6 
	FOR UPDATE;
	IF _zoneIndex IS NOT NULL THEN 
		UPDATE wok.channel_zones
		   SET NAME = wok.makeFriendlyName( __name, _zoneIndex ) 
		WHERE channel_zone_id = _channelZoneId;
	END IF;
END IF;
                      

END
;;
DELIMITER ;