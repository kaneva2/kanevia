-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS remove_associated_school;

DELIMITER ;;
CREATE PROCEDURE `remove_associated_school`(IN __user_id INT, IN __school_id INT)
BEGIN
  DELETE from kaneva.user_schools where user_id = __user_id AND school_id = __school_id;
END
;;
DELIMITER ;