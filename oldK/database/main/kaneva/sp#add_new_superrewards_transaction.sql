-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_new_superrewards_transaction;

DELIMITER ;;
CREATE PROCEDURE `add_new_superrewards_transaction`(srTransactionId INT UNSIGNED, creditsAwarded INT UNSIGNED, creditsAllTime INT UNSIGNED, userID INT UNSIGNED, offerID INT UNSIGNED, security_key VARCHAR(100), awardStatus SMALLINT UNSIGNED, wokTransactionLogID INT UNSIGNED, requestingIPAddress VARCHAR(100))
BEGIN

START TRANSACTION;

/* update the records */
    INSERT INTO 
    	superrewards_log (sr_transaction_id, credits_awarded, credits_all_time, user_id, sr_offer_id, security_key, award_status, award_date, requesting_ip, wok_transaction_log_id) 
    VALUES 
    	(srTransactionId, creditsAwarded, creditsAllTime, userID, offerID, security_key, awardStatus, now(), INET_ATON(requestingIPAddress), wokTransactionLogID);

    /*get the id of the new row*/
    SELECT LAST_INSERT_ID();
    
  COMMIT;
END
;;
DELIMITER ;