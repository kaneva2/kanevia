-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS removeDupPersonalCommunities;

DELIMITER ;;
CREATE PROCEDURE `removeDupPersonalCommunities`()
BEGIN
DECLARE done INT DEFAULT 0;
DECLARE _user_id  INT;
DECLARE _dup_com_cnt INT;
DECLARE dpcCursor CURSOR FOR
	SELECT creator_id, count(creator_id) 
		FROM communities c 
		WHERE is_personal = 1
		GROUP BY creator_id
		HAVING count(creator_id) > 1;
DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN SET done = 1; END;
CREATE TABLE IF NOT EXISTS _temp_dup_com (dup_com_id INT);
OPEN dpcCursor;
REPEAT
	FETCH dpcCursor into _user_id, _dup_com_cnt;
	IF NOT done THEN
		INSERT INTO _temp_dup_com (dup_com_id)
			SELECT community_id FROM communities 
			WHERE creator_id = _user_id and is_personal = 1
			AND community_id != (SELECT MIN(community_id) FROM communities WHERE creator_id = _user_id AND is_personal = 1);
	END IF;
UNTIL done END REPEAT;
CLOSE dpcCursor;

SELECT  'these will be deleted, please keep this list for future reference: ', community_id, username, now()
	FROM communities c
	INNER JOIN users u ON c.creator_id = u.user_id
	WHERE community_id IN (SELECT dup_com_id FROM _temp_dup_com);


DELETE FROM personal_communities WHERE community_id IN (SELECT dup_com_id FROM _temp_dup_com);
DELETE FROM summary_assets_by_type_by_channel WHERE channel_id IN (SELECT dup_com_id FROM _temp_dup_com);
DELETE FROM summary_assets_by_connection WHERE channel_id IN (SELECT dup_com_id FROM _temp_dup_com);
DELETE FROM new_channels WHERE channel_id IN (SELECT dup_com_id FROM _temp_dup_com);
DELETE FROM channel_stats WHERE channel_id IN (SELECT dup_com_id FROM _temp_dup_com);
DELETE FROM community_members WHERE community_id IN (SELECT dup_com_id FROM _temp_dup_com);
DELETE FROM communities WHERE community_id IN (SELECT dup_com_id FROM _temp_dup_com);
DROP TABLE IF EXISTS _temp_dup_com;
END
;;
DELIMITER ;