-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS rename_user;

DELIMITER ;;
CREATE PROCEDURE `rename_user`( IN __old_name VARCHAR(80), IN __new_name VARCHAR(80), OUT __ret INT )
BEGIN
	DECLARE __player_id INT(11) DEFAULT NULL;
	DECLARE __user_id INT(11) DEFAULT NULL;
	DECLARE __kaneva_user_id INT (11) DEFAULT NULL;
	DECLARE __existing_kaneva_user_id INT (11) DEFAULT NULL;
	DECLARE __zone_index INT (11) DEFAULT NULL;

	SET __ret = 0;

	
	
	
	
	
	
	START TRANSACTION;
	
		
		SELECT user_id INTO __kaneva_user_id
		  FROM kaneva.users
		 WHERE username = __new_name;
		 
		IF __kaneva_user_id IS NOT NULL AND __new_name <> __old_name THEN 
			SET __ret = 3; 
		ELSE
			SELECT user_id INTO __kaneva_user_id
			  FROM kaneva.users
			 WHERE username = __old_name; 

			IF __kaneva_user_id IS NOT NULL THEN 

				 
				 SELECT user_id INTO __user_id 
				   FROM wok.game_users
				  WHERE kaneva_user_id = __kaneva_user_id
				  FOR UPDATE;

				 IF __user_id IS NOT NULL THEN
				 	UPDATE wok.game_users
				 	  SET username = __new_name
 				    WHERE kaneva_user_id = __kaneva_user_id;
				 END IF;

				 
				UPDATE kaneva.users
				   SET username = __new_name
				 WHERE user_id = __kaneva_user_id; 

				
				UPDATE kaneva.communities
				  SET name = __new_name,
				  	   name_no_spaces = __new_name,
				  	  creator_username = __new_name,
 			           keywords = replace(keywords,__old_name,__new_name)
				 WHERE is_personal = 1
				   AND creator_id = __kaneva_user_id;
				   
				UPDATE kaneva.communities
				  SET creator_username = __new_name,
 			           keywords = replace(keywords,__old_name,__new_name)
				 WHERE is_personal = 0 
				   AND creator_id = __kaneva_user_id;

				UPDATE kaneva.assets
				  SET owner_username = __new_name
				 WHERE owner_id = __kaneva_user_id;

				UPDATE kaneva.topics
				  SET created_username = __new_name
				 WHERE created_user_id = __kaneva_user_id;

				 
				 SELECT player_id INTO __player_id 
				   FROM wok.players
				  WHERE kaneva_user_id = __kaneva_user_id
				  FOR UPDATE;

				 IF __player_id IS NOT NULL THEN
				 	UPDATE wok.players
				 	  SET name = __new_name
 				    WHERE kaneva_user_id = __kaneva_user_id;

 				    
 				    SELECT zone_index INTO __zone_index
 				      FROM wok.channel_zones 
 				    WHERE zone_type = 3
 				      AND zone_instance_id = __player_id
 				     FOR UPDATE;

					IF __zone_index IS NOT NULL THEN  				      
	 				    UPDATE wok.channel_zones 
	 				      SET name = wok.getFriendlyApartmentName( __new_name, __zone_index )
	 				    WHERE zone_type = 3
	 				      AND zone_instance_id = __player_id;
	 				END IF;
				 END IF;
			ELSE
				SET __ret = 2; 
			END IF;
		END IF;

	IF __ret = 0 THEN 		
		COMMIT;
	ELSE
		ROLLBACK;
	END IF;
	
END
;;
DELIMITER ;