-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_publish_file_properties;

DELIMITER ;;
CREATE PROCEDURE `update_assets_publish_file_properties`(IN __asset_id INT, IN __content_extension VARCHAR(255),
		IN __target_dir VARCHAR(255), IN __file_size BIGINT UNSIGNED, 
		IN __image_path VARCHAR(255), IN __image_type VARCHAR(50))
BEGIN

UPDATE assets 
	SET target_dir = __target_dir,
	content_extension = __content_extension,
	file_size = __file_size,
	image_path = __image_path,
	image_type = __image_type
	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;