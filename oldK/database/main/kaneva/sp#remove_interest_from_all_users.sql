-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS remove_interest_from_all_users;

DELIMITER ;;
CREATE PROCEDURE `remove_interest_from_all_users`(IN __interest_id INT)
BEGIN
-- remove interests from all users BE CAREFUL!  they go away for all of them, to be used by admins only.
  DECLARE __ic_id INT;
	Select ic_id INTO __ic_id from kaneva.interests where interest_id = __interest_id;
	
	DELETE from kaneva.user_interests where interest_id = __interest_id;
	
	-- Update the counts
	update kaneva.user_interest_category_counts SET `count`=`count` - 1 WHERE  ic_id = __ic_id;
	update kaneva.users_stats SET `interest_count`=`interest_count` - 1 WHERE  interest_id = __interest_id;
END
;;
DELIMITER ;