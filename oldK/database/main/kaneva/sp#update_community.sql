-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_community;

DELIMITER ;;
CREATE PROCEDURE `update_community`(_communityId INTEGER SIGNED, _canPublish INTEGER SIGNED, _backgroundImage VARCHAR(255), 
	_backgroundRGB VARCHAR(8), _description VARCHAR(255),_isAdult CHAR(1), _isPublic CHAR(1), 
	_keywords TEXT, _name VARCHAR(100), _nameNoSpaces VARCHAR(100), _over21Required CHAR(1), _placeTypeId INTEGER SIGNED,
	_isPersonal INTEGER UNSIGNED, _email VARCHAR(100), _url VARCHAR(50), _communityTypeId INTEGER UNSIGNED)
BEGIN

	UPDATE  communities 
	SET allow_publishing = _canPublish,
		background_image = _backgroundImage,
		background_rgb = _backgroundRGB,
		is_adult = _isAdult,
		keywords = _keywords,
		over_21_required = _over21Required,
		place_type_id = _placeTypeId,
		is_personal = _isPersonal,
		email = _email,
		last_edit = NOW(),
		url = _url,
		community_type_id = _communityTypeId,
		name_no_spaces = _nameNoSpaces
	WHERE   community_id = _communityId;
	SELECT ROW_COUNT();
	CALL update_communities_name(_communityId, _name, _description);
	CALL update_communities_is_public(_communityId, _isPublic);
	

END
;;
DELIMITER ;