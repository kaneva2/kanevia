-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_report_stats;

DELIMITER ;;
CREATE PROCEDURE `update_report_stats`(IN start_date DATETIME, IN
end_date DATETIME)
BEGIN

  call update_wok_report_stats(start_date, end_date);

 call summarize_asset_info(start_date, end_date);

 call summarize_community_info(start_date, end_date);

 
call summarize_new_channels();

call summarize_most_popular();


    CALL summarize_new_assets();

  CALL update_most_popular_assets();

  CALL update_community_req_counts();
  call summarize_transactions(start_date, end_date);



END
;;
DELIMITER ;