-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_community_members_all_pending;

DELIMITER ;;
CREATE PROCEDURE `update_community_members_all_pending`(IN __community_id INT)
BEGIN

DECLARE _account_type_id INT;
DECLARE _user_id INT;
DECLARE _how_many_to_activate INT DEFAULT 0;
DECLARE done INT DEFAULT 0;

DECLARE cm_cursor CURSOR FOR 
		SELECT user_id, account_type_id 
		FROM community_members WHERE community_id = __community_id AND status_id = 2;
			
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;

OPEN cm_cursor;
SET _how_many_to_activate = (SELECT FOUND_ROWS());

-- decrement pending members
UPDATE channel_stats 
	SET number_of_pending_members =CASE WHEN number_of_pending_members>_how_many_to_activate THEN number_of_pending_members-_how_many_to_activate ELSE 0 END
	WHERE channel_id = __community_id;
	
-- increment members
UPDATE channel_stats 
	SET number_of_members = number_of_members + _how_many_to_activate 
	WHERE channel_id = __community_id;

REPEAT
	FETCH cm_cursor INTO _user_id, _account_type_id;
	IF NOT done THEN 
		UPDATE community_members 
			SET status_id = 1
			WHERE community_id = __community_id AND user_id = _user_id;

		IF (_account_type_id = 2 OR _account_type_id = 1) THEN
		      UPDATE users SET own_mod = 'Y' WHERE user_id = _user_id;
		END IF;
	END IF;
UNTIL done END REPEAT;
CLOSE cm_cursor;

END
;;
DELIMITER ;