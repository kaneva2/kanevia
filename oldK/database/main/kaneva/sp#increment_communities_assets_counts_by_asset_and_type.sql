-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS increment_communities_assets_counts_by_asset_and_type;

DELIMITER ;;
CREATE PROCEDURE `increment_communities_assets_counts_by_asset_and_type`(IN __asset_id INT, IN __asset_type_id INT)
BEGIN
IF __asset_type_id = 1 THEN UPDATE communities_assets_counts cas,asset_channels ac 
		SET cas.games=cas.games+1 WHERE ac.asset_id=__asset_id AND cas.community_id=ac.channel_id;
    ELSEIF __asset_type_id = 2 THEN UPDATE communities_assets_counts cas,asset_channels ac 
		SET cas.videos=cas.videos+1 WHERE ac.asset_id=__asset_id AND cas.community_id=ac.channel_id;
    ELSEIF __asset_type_id = 4 THEN UPDATE communities_assets_counts cas,asset_channels ac 
		SET cas.music=cas.music+1 WHERE ac.asset_id=__asset_id AND cas.community_id=ac.channel_id;
    ELSEIF __asset_type_id = 5 THEN UPDATE communities_assets_counts cas,asset_channels ac 
		SET cas.photos=cas.photos+1 WHERE ac.asset_id=__asset_id AND cas.community_id=ac.channel_id;
    ELSEIF __asset_type_id = 6 THEN UPDATE communities_assets_counts cas,asset_channels ac 
		SET cas.patterns=cas.patterns+1 WHERE ac.asset_id=__asset_id AND cas.community_id=ac.channel_id;
    ELSEIF __asset_type_id = 7 THEN UPDATE communities_assets_counts cas,asset_channels ac 
		SET cas.TV=cas.TV+1 WHERE ac.asset_id=__asset_id AND cas.community_id=ac.channel_id;
    ELSEIF __asset_type_id = 8 THEN UPDATE communities_assets_counts cas,asset_channels ac 
		SET cas.widgets=cas.widgets+1 WHERE ac.asset_id=__asset_id AND cas.community_id=ac.channel_id;
    ELSEIF __asset_type_id = 99 THEN UPDATE communities_assets_counts cas,asset_channels ac 
		SET ccas.shares=cas.shares+1 WHERE ac.asset_id=__asset_id AND cas.community_id=ac.channel_id;
-- is inrementing shares globally for an asset feasible?
END IF;
END
;;
DELIMITER ;