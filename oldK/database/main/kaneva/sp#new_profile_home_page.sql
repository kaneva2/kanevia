-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS new_profile_home_page;

DELIMITER ;;
CREATE PROCEDURE `new_profile_home_page`(IN __begin_user_id INT(11), IN __end_user_id INT(11))
BEGIN
DECLARE __user_id INT;
DECLARE __channel_id INT;
DECLARE __done INT DEFAULT 0;
DECLARE __done2 INT DEFAULT 0;
DECLARE __done3 INT DEFAULT 0;
DECLARE __page_id INT;
DECLARE __username VARCHAR(100);
DECLARE __community_id INT;
DECLARE __last_id INT;
DECLARE __new_home_page_id INT;
DECLARE cur_users CURSOR FOR SELECT user_id, username
				from users
				WHERE user_id >= __begin_user_id AND user_id <= __end_user_id 
				and username REGEXP '[[:alnum:]]+';
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1;
OPEN cur_users;
REPEAT
				SET __done2 = 0;
				SET __done3 = 0;
                FETCH cur_users INTO __user_id, __username;
                IF NOT __done THEN
                BEGIN
                DECLARE cur_communities CURSOR FOR SELECT community_id from kaneva.communities where communities.is_personal = 1 and communities.creator_id = __user_id limit 1;
                DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done2 = 1;
                OPEN cur_communities;
					REPEAT
						 Fetch cur_communities into __community_id;
									IF NOT __done2 THEN
									BEGIN
										DECLARE cur_layout_pages CURSOR FOR SELECT page_id from kaneva.layout_pages WHERE kaneva.layout_pages.channel_id = __community_id;
										DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done3 = 1;
										open cur_layout_pages;
										REPEAT
										FETCH cur_layout_pages into __page_id;
										IF NOT __done3 THEN
										BEGIN
											BEGIN
												UPDATE layout_pages  SET layout_pages.name = 'Page 2', group_id = NULL, access_id = 0  
															WHERE layout_pages.name = 'Home' and layout_pages.page_id = __page_id;
												UPDATE layout_pages lp  SET sequence = sequence+1, lp.home_page = 0  WHERE page_id = __page_id;
											END;
									END;
									END IF;
						UNTIL __done3 END REPEAT;
                                   
				close cur_layout_pages;
				
				INSERT INTO layout_pages (name, group_id, access_id, home_page, channel_id, sequence)  VALUES ( 'Profile', NULL, 0, 1, __community_id, 0);
				SELECT LAST_INSERT_ID() INTO __new_home_page_id;

				
				INSERT INTO layout_module_title_text  (text, show_menu, banner_path)  VALUES(__username, 1, NULL);
				SELECT last_insert_id() INTO __last_id;

				INSERT INTO layout_page_modules (module_page_id, page_id, module_id, zone_id, sequence) VALUES (__last_id, __new_home_page_id, 1, 1, 1);
				INSERT INTO layout_module_control_panel  (title, show_gender, show_location, show_age)  VALUES(__username, 1, 1, 1);
				SELECT last_insert_id() INTO __last_id;
				INSERT INTO layout_page_modules (module_page_id, page_id, module_id, zone_id, sequence) VALUES (__last_id, __new_home_page_id, 23, 6, 1);
				INSERT INTO layout_module_fame  (title)  VALUES('Fame');
				SELECT last_insert_id() INTO __last_id;

				INSERT INTO layout_page_modules (module_page_id, page_id, module_id, zone_id, sequence) VALUES (__last_id, __new_home_page_id, 56, 6, 2);
				INSERT INTO layout_module_personal  (title)  VALUES('Personal');
				SELECT last_insert_id() INTO __last_id;
				INSERT INTO layout_page_modules (module_page_id, page_id, module_id, zone_id, sequence) VALUES (__last_id, __new_home_page_id, 54, 6, 3);
				INSERT INTO layout_module_friends  (title, people_per_page)  VALUES('Friends', 10);
				SELECT last_insert_id() INTO __last_id;
				INSERT INTO layout_page_modules (module_page_id, page_id, module_id, zone_id, sequence) VALUES (__last_id, __new_home_page_id, 4, 6, 4);
				INSERT INTO layout_module_multiple_pictures  (title, pictures_per_page, show_image_title, group_id, size)  
						VALUES('Multiple Photos', 10, 1, NULL, 3);
				SELECT last_insert_id() INTO __last_id;
				INSERT INTO layout_page_modules (module_page_id, page_id, module_id, zone_id, sequence) VALUES (__last_id, __new_home_page_id, 10, 2, 5);
				INSERT INTO layout_module_channels  (title, entries_per_page, filter)  VALUES('Community List', 10, 7);
				SELECT last_insert_id() INTO __last_id;
				INSERT INTO layout_page_modules (module_page_id, page_id, module_id, zone_id, sequence) VALUES (__last_id, __new_home_page_id, 12, 2, 6);
				INSERT INTO layout_module_interests  (title)  VALUES('My Interests');
				SELECT last_insert_id() INTO __last_id;
				INSERT INTO layout_page_modules (module_page_id, page_id, module_id, zone_id, sequence) VALUES (__last_id, __new_home_page_id, 53, 3, 1);
				INSERT INTO layout_module_blogs  (title, entries_per_page, show_dates, tags)  VALUES('Blogs', 20, 1, '');
				SELECT last_insert_id() INTO __last_id;

				INSERT INTO layout_page_modules (module_page_id, page_id, module_id, zone_id, sequence) VALUES (__last_id, __new_home_page_id, 2, 3, 2);
				INSERT INTO layout_module_omm_media  (title,asset_id,auto_start)  VALUES ('Video Player', 0, 0);
				SELECT last_insert_id() INTO __last_id;
				INSERT INTO layout_page_modules (module_page_id, page_id, module_id, zone_id, sequence) VALUES (__last_id, __new_home_page_id, 32, 3, 3);
				INSERT INTO layout_module_stores  (title,items_per_page,group_id)  VALUES ('Video Catalog', 10, NULL);
				SELECT last_insert_id() INTO __last_id;
				INSERT INTO layout_page_modules (module_page_id, page_id, module_id, zone_id, sequence) VALUES (__last_id, __new_home_page_id, 18, 3, 4);
				INSERT INTO layout_module_comments  (title,  max_comments, show_datetime, show_profile_pic, author_alignment, permission_friends, permission_everyone)  
							VALUES('Comments', 100, 1, 1, 'L', 1, 0);
				
				SELECT last_insert_id() INTO __last_id;
				INSERT INTO layout_page_modules (module_page_id, page_id, module_id, zone_id, sequence) VALUES (__last_id, __new_home_page_id, 5, 3, 5);
				
				END;
				END IF;
				UNTIL __done2 END REPEAT;
				CLOSE cur_communities;
				END;
				END IF;
				INSERT INTO `dba_users_with_new_profile_home_page` (`user_id`, `last_updated`, `add_count` )
						VALUES (__user_id, now(), 1)
						ON DUPLICATE KEY UPDATE `last_updated` = now(), `add_count` = `add_count` + 1;
				COMMIT;

                UNTIL __done END REPEAT;
close cur_users;
END
;;
DELIMITER ;