-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_snapshot_eod_data;

DELIMITER ;;
CREATE PROCEDURE `update_snapshot_eod_data`()
begin
	call wok.ss_update_user_balances();
	call kaneva.ss_update_sitemembers();
	call wok.ss_update_user_gender();
	call wok.ss_update_user_active();
	call wok.ss_update_user_inactive();
	call wok.ss_update_wok_user_average();
	call kaneva.ss_update_communities();
	call wok.ss_update_placed_furniture();
	call wok.ss_update_purchased_clothing();
end
;;
DELIMITER ;