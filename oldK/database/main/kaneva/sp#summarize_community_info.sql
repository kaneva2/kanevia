-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS summarize_community_info;

DELIMITER ;;
CREATE PROCEDURE `summarize_community_info`(in start_date datetime, in end_date datetime)
begin


  Replace Into summary_channel_diggs
     Select channel_id, DATE(created_date) as d, count(*) from channel_diggs WHERE created_date BETWEEN start_date AND end_date group by channel_id, d;

  Replace Into summary_comments_by_day
    Select channel_id, date(created_date) as d, count(*) as c from comments WHERE created_date BETWEEN start_date AND end_date group by channel_id, d;
      
      
  Replace Into summary_community_members
    Select cm.community_id, date(added_date) as d, count(*) as c from community_members cm
    Inner Join communities_base com on com.community_id = cm.community_id
    WHERE com.is_personal=0 AND cm.added_date BETWEEN start_date AND end_date group by cm.community_id, d;
	  
	  
END
;;
DELIMITER ;