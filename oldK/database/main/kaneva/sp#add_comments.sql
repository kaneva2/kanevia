-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_comments;

DELIMITER ;;
CREATE PROCEDURE `add_comments`(IN __asset_id INT, IN __channel_id INT, IN __user_id INT, 
		IN __parent_comment_id INT, IN __reply_to_comment_id INT, IN `__comment` TEXT, 
		IN __comment_type INT, IN __ip_address VARCHAR(50), IN __status_id INT)
BEGIN
DECLARE _comment_count INT;
DECLARE __sql_statement VARCHAR(512);

INSERT IGNORE INTO comments (asset_id, channel_id, user_id, parent_comment_id, reply_to_comment_id, 
			`comment`, comment_type, ip_address, created_date, last_updated_date, 
			last_updated_user_id, status_id) 
	VALUES (__asset_id, __channel_id, __user_id, __parent_comment_id, __reply_to_comment_id, 
		__comment, 0, __ip_address, NOW(), NOW(), 0, __status_id);
SELECT ROW_COUNT() INTO @rowcnt;

IF @rowcnt = 1 THEN
	UPDATE kaneva.users_stats SET number_of_comments = number_of_comments + 1 WHERE user_id = __user_id;
END IF;

/*get current count of comments for the user for the day*/
SET _comment_count = (SELECT comment_count FROM summary_comments_by_user_by_day WHERE comment_date = CURDATE() AND user_id = __user_id) ;

/*add or update comment count*/	
IF (_comment_count IS NOT NULL) AND (_comment_count > 0) THEN
	UPDATE summary_comments_by_user_by_day SET comment_count = comment_count + 1 WHERE user_id = __user_id AND comment_date = CURDATE();
ELSE
	INSERT INTO summary_comments_by_user_by_day (comment_date,comment_count,user_id) VALUES(CURDATE() , 1, __user_id);
END IF;

IF @rowcnt1 = 1 THEN
	UPDATE kaneva.assets_stats SET number_of_comments = number_of_comments + 1 WHERE asset_id = __asset_id;
END IF;

END
;;
DELIMITER ;