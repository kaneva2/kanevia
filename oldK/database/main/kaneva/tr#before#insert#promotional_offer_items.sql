-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS before_insert_promotional_offer_items;

DELIMITER ;;
CREATE TRIGGER `before_insert_promotional_offer_items` BEFORE INSERT ON `promotional_offer_items` FOR EACH ROW BEGIN
    INSERT into kaneva.audit_promotional_offer_items 
    (
    	wok_item_id, promotion_id, gift_card_id, community_id, 
    	item_description, quantity, market_price, 
        gender, promotion_offer_id, alternate_description, 
	modifiers_id, date_modified, operation
    )
    VALUES
    (
    	
    	new.wok_item_id, new.promotion_id, new.gift_card_id, new.community_id, 
    	new.item_description, new.quantity, new.market_price, 
        new.gender, new.promotion_offer_id, new.alternate_description, 
	new.modifiers_id, Now(), 'insert'
    ) ;  
   
END
;;
DELIMITER ;