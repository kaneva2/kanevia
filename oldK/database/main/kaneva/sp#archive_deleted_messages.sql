-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS archive_deleted_messages;

DELIMITER ;;
CREATE PROCEDURE `archive_deleted_messages`()
BEGIN
  replace into my_kaneva.messages_archive Select * from kaneva.messages where from_id_status IN (2,3) AND to_id_status IN (2,3) AND message_date <= CONCAT(DATE(DATE_SUB(NOW(), INTERVAL 10 DAY)),' 00:00:00');
  DELETE from kaneva.messages where from_id_status IN (2,3) AND to_id_status IN (2,3) AND message_date <= CONCAT(DATE(DATE_SUB(NOW(), INTERVAL 10 DAY)),' 00:00:00');
END
;;
DELIMITER ;