-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS b4_topics_update;

DELIMITER ;;
CREATE TRIGGER `b4_topics_update` BEFORE UPDATE ON `topics_base` FOR EACH ROW BEGIN
  set new.viewable = IF (new.status_id IN (1,3), 'Y','N');
END
;;
DELIMITER ;