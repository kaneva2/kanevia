-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_communities_over_21;

DELIMITER ;;
CREATE PROCEDURE `update_communities_over_21`(IN __community_id INT, IN __over_21_required ENUM('Y','N'))
BEGIN

UPDATE communities 
	SET over_21_required = __over_21_required
	WHERE community_id = __community_id;

END
;;
DELIMITER ;