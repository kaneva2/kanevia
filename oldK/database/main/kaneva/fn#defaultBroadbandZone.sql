-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS defaultBroadbandZone;

DELIMITER ;;
CREATE FUNCTION `defaultBroadbandZone`() RETURNS int(11)
    DETERMINISTIC
BEGIN
	return 1610612749; 
END
;;
DELIMITER ;