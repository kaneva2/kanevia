-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS create_new_contest;

DELIMITER ;;
CREATE PROCEDURE `create_new_contest`(IN userId INTEGER UNSIGNED, IN endDate DATETIME,
              IN title varchar(100), IN description TEXT, IN prizeAmount INTEGER UNSIGNED, IN transactionType smallint unsigned,
              IN keiPointId varchar(20), IN contestTransactionType smallint unsigned, IN createFee INT,
							OUT return_code INT, OUT user_balance float, OUT tranId INT, OUT contestId BIGINT unsigned)
BEGIN

    DECLARE EXIT HANDLER FOR SQLEXCEPTION, NOT FOUND, SQLWARNING ROLLBACK;

    SET contestId = 0;

    START TRANSACTION;

      INSERT INTO contests_ugc (user_id, created_date, end_date, title, description, prize_amount, contest_active, date_status_changed)
        VALUES (userId, NOW(), endDate, title, description, prizeAmount, 1, NOW());

      SELECT LAST_INSERT_ID() INTO contestId;

      IF contestId = 0 THEN ROLLBACK;
      ELSE

        CALL kaneva.apply_transaction_to_user_balance( userId, createFee, transactionType, keiPointId, return_code, user_balance, tranId );
 
        IF return_code = 0 AND tranId > 0 THEN
 		        INSERT INTO contest_transaction_details ( trans_id, contest_id, transaction_type, transaction_date )
             VALUES ( tranId, contestId, contestTransactionType, NOW() );
        ELSE
          ROLLBACK;
        END IF;

      END IF;

    COMMIT;

END
;;
DELIMITER ;