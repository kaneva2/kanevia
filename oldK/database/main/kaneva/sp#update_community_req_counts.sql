-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_community_req_counts;

DELIMITER ;;
CREATE PROCEDURE `update_community_req_counts`()
BEGIN
-- update the counts of the number of community requests 
-- for a users where the user is a owner or moderator, 
-- not including their personal community
	UPDATE kaneva.users_stats us
	       INNER JOIN
	       (SELECT sum(number_of_pending_members) AS __count, user_id
	          FROM kaneva.channel_stats cs
	               INNER JOIN
	               kaneva.community_members cm ON cs.channel_id = cm.community_id AND cm.account_type_id IN (1, 2)
	               INNER JOIN
	               kaneva.communities_public cb ON cb.community_id = cm.community_id
	                                         AND cb.status_id = 1
	         GROUP BY cm.user_id) AS x ON us.user_id = x.user_id
	   SET us.number_pending_community_requests = x.__count;
END
;;
DELIMITER ;