-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS aggregate_post_counts;

DELIMITER ;;
CREATE PROCEDURE `aggregate_post_counts`()
BEGIN
  DELETE from kaneva.posts_7_days where date_created <= DATE_SUB(NOW(), INTERVAL 7 DAY);

  update channel_stats zc
  Inner Join (
    Select count(log_id) as post_count, channel_id from posts_7_days group by channel_id
  )X ON zc.channel_id = X.channel_id
  SET zc.number_posts_7_days = X.post_count;

END
;;
DELIMITER ;