-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_name_teaser;

DELIMITER ;;
CREATE PROCEDURE `update_assets_name_teaser`(IN __asset_id INT, IN __name VARCHAR(100),
		IN __teaser TEXT)
BEGIN

UPDATE assets
	SET `name` = __name,
	teaser = __teaser
	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;