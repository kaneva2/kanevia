-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_assets;

DELIMITER ;;
CREATE PROCEDURE `add_assets`(
  IN __asset_type_id INT, IN __is_kaneva_game BINARY(1), IN __asset_sub_type_id INT,
  IN __asset_rating_id INT, IN __owner_id INT, IN __file_size BIGINT UNSIGNED,
  IN __run_time_seconds INT, IN __category1_id INT, IN __category2_id INT,
  IN `__category3_id` INT, IN __amount DECIMAL(12,2), IN __kei_point_id ENUM('GPOINT','KPOINT','MPOINT','DOLLAR'),
  IN __image_path VARCHAR(255), IN __image_full_path VARCHAR(255), IN __thumbnail_assetdetails_path VARCHAR(255),
  IN __thumbnail_xlarge_path VARCHAR(255), IN __thumbnail_large_path VARCHAR(255),
  IN __thumbnail_medium_path VARCHAR(255), IN __thumbnail_small_path VARCHAR(255),
  IN __thumbnail_gen TINYINT, IN __image_caption VARCHAR(100), IN __image_type VARCHAR(50),
  IN __status_id INT, IN __last_updated_user_id INT, IN __game_exe VARCHAR(100), IN __torrent_id INT,
  IN __license_cc VARCHAR(100), IN __license_URL VARCHAR(200), IN __license_type CHAR(1),
  IN __license_name VARCHAR(50), IN __license_additional VARCHAR(100), IN __publish_status_id INT,
  IN __percent_complete INT, IN __ip_address VARCHAR(30), IN __game_encryption_key VARCHAR(16),
  IN __icon_image_data LONGBLOB, IN __icon_image_path VARCHAR(100), IN __icon_image_type VARCHAR(50),
  IN __require_login SMALLINT, IN __content_extension VARCHAR(255), IN __target_dir VARCHAR(255),
  IN __media_path VARCHAR(255), IN __permission INT, IN __permission_group INT, IN __over_21_required ENUM('Y','N'),
  IN __asset_offsite_id VARCHAR(1000), IN __sort_order INT UNSIGNED, IN __name VARCHAR(100), IN __body_text TEXT,
  IN __short_description VARCHAR(50), IN __teaser TEXT, IN __owner_username VARCHAR(22),
  IN __keywords TEXT, IN __instructions TEXT, IN __company_name VARCHAR(100), OUT _asset_id INT)
BEGIN

DECLARE __sql_statement VARCHAR(512);

SET @mature       = IF(( (__asset_rating_id=3) | (__asset_rating_id=6) | (__asset_rating_id=9) ),'Y','N');
SET @published    = IF(( (__publish_status_id=10) & ( (__status_id=1) | (__status_id=3)) ),'Y','N');
SET @public       = IF (__permission <> 2,'Y','N');
SET @playlistable = IF(( (__asset_type_id=2) | (__asset_type_id=4) ),'Y','N');

INSERT INTO assets 
	(asset_type_id, is_kaneva_game, asset_sub_type_id, asset_rating_id, owner_id, file_size, 
	run_time_seconds, category1_id, category2_id, category3_id, amount, kei_point_id, image_path, 
	image_full_path, thumbnail_assetdetails_path, thumbnail_xlarge_path, thumbnail_large_path, 
	thumbnail_medium_path, thumbnail_small_path, thumbnail_gen, image_caption, image_type, status_id, 
	created_date, last_updated_date, last_updated_user_id, game_exe, torrent_id, license_cc, license_URL, 
	license_type, license_name, license_additional, publish_status_id, percent_complete, ip_address, 
	game_encryption_key, icon_image_data, icon_image_path, icon_image_type, require_login, content_extension, 
	target_dir, media_path, permission, permission_group, published, mature, public, playlistable, 
	over_21_required, asset_offsite_id, sort_order, last_update, `name`, body_text, short_description, 
	teaser, owner_username, keywords, instructions, company_name)
	VALUES
	(__asset_type_id, __is_kaneva_game, __asset_sub_type_id, __asset_rating_id, __owner_id, __file_size, 
	__run_time_seconds, __category1_id, __category2_id, __category3_id, __amount, __kei_point_id, __image_path, 
	__image_full_path, __thumbnail_assetdetails_path, __thumbnail_xlarge_path, __thumbnail_large_path, 
	__thumbnail_medium_path, __thumbnail_small_path, __thumbnail_gen, __image_caption, __image_type, __status_id, 
	NOW(), NOW(), __last_updated_user_id, __game_exe, __torrent_id, __license_cc, __license_URL, 
	__license_type, __license_name, __license_additional, __publish_status_id, __percent_complete, __ip_address, 
	__game_encryption_key, __icon_image_data, __icon_image_path, __icon_image_type, __require_login, __content_extension, 
	__target_dir, __media_path, __permission, __permission_group, @published, @mature, @public, @playlistable, 
	__over_21_required, __asset_offsite_id, __sort_order, NOW(), __name, __body_text, __short_description, 
	__teaser, __owner_username, __keywords, __instructions, __company_name);

SELECT LAST_INSERT_ID() INTO _asset_id;

UPDATE kaneva.summary_assets_by_type SET `count` = `count` + 1 WHERE asset_type_id = __asset_type_id;    
INSERT IGNORE INTO kaneva.assets_stats (asset_id) VALUES (_asset_id);

END
;;
DELIMITER ;