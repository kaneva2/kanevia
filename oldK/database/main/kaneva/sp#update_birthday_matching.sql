-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_birthday_matching;

DELIMITER ;;
CREATE PROCEDURE `update_birthday_matching`()
BEGIN

DECLARE _counter SMALLINT;
DECLARE _month TINYINT;
DECLARE _day TINYINT;
DECLARE _year SMALLINT;
DECLARE __done TINYINT;
DECLARE _birthday DATE;

SELECT MONTH(ADDDATE(NOW(), INTERVAL 1 DAY)) INTO _month;
SELECT DAY(ADDDATE(NOW(), INTERVAL 1 DAY)) INTO _day;
SELECT YEAR(ADDDATE(NOW(), INTERVAL 1 DAY)) INTO _year;
SET _counter = 1;
SET __done = 0;

DELETE FROM birthday_matching;

REPEAT	
	IF NOT __done THEN 
	
		SET _birthday = CONCAT(_year,'-',_month,'-',_day);
		INSERT INTO birthday_matching ( `birthday`)  VALUES (_birthday);

		SET _counter = _counter + 1;
		SET _year = _year - 1;
		IF _counter > 150 THEN SET __done = 1; END IF;
	END IF;

UNTIL __done END REPEAT;
END
;;
DELIMITER ;