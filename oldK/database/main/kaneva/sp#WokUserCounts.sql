-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS WokUserCounts;

DELIMITER ;;
CREATE PROCEDURE `WokUserCounts`( IN userId INT(11), 
			OUT numberofnewmessages INT, OUT numberofrequests INT, 
			OUT numberoffriends INT, OUT numberofpendingmembers INT, 
			OUT numberofviews INT, OUT numbergiftspending INT, 
			OUT numbergiftsreceived INT, OUT numbergiftsgiven INT, 
			OUT numberofdiggs INT )
BEGIN
	 -- get counts specific to this user
	 SELECT number_of_new_messages, number_of_requests,	number_of_friends,
	 		number_gifts_pending, number_gifts_received, number_gifts_given, 
	 		number_pending_community_requests, number_of_views, number_of_diggs 
	   INTO 
	        numberofnewmessages, numberofrequests, numberoffriends, 
	        numbergiftspending, numbergiftsreceived, numbergiftsgiven, 
	        numberofpendingmembers, numberofviews, numberofdiggs
	   FROM users_stats us 
			INNER JOIN communities_personal c ON c.creator_id = us.user_id 
	   		INNER JOIN channel_stats cs ON cs.channel_id = c.community_id 
	   WHERE user_id = userId 
		LIMIT 1;
END
;;
DELIMITER ;