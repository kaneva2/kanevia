-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_asset_view_aggregates;

DELIMITER ;;
CREATE PROCEDURE `update_asset_view_aggregates`()
BEGIN
SELECT NOW() INTO @start_time;

DROP TABLE IF EXISTS __tempViews_;
CREATE TABLE `__tempViews_`
(
  `asset_id` INT NOT NULL,
  `number_of_views` INT DEFAULT 0,
  PRIMARY KEY(`asset_id`)
)
ENGINE = MyISAM
CHARACTER SET utf8;

Insert into __tempViews_ Select asset_id, sum(count) from summary_assets_viewed WHERE dt_stamp >= DATE_SUB(NOW(), INTERVAL 7 DAY) group by asset_id;

DROP TABLE IF EXISTS summary_assets_viewed_7_days;
ALTER TABLE __tempViews_ RENAME TO summary_assets_viewed_7_days;



DROP TABLE IF EXISTS __tempViews_;
CREATE TABLE `__tempViews_`
(
  `asset_id` INT NOT NULL,
  `number_of_views` INT DEFAULT 0,
  PRIMARY KEY(`asset_id`)
)
ENGINE = MyISAM
CHARACTER SET utf8;

Insert into __tempViews_ Select asset_id, sum(count) from summary_assets_viewed WHERE dt_stamp >= DATE_SUB(NOW(), INTERVAL 30 DAY) group by asset_id;

DROP TABLE IF EXISTS summary_assets_viewed_30_days;
ALTER TABLE __tempViews_ RENAME TO summary_assets_viewed_30_days;

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('update_asset_view_aggregates',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));

END
;;
DELIMITER ;