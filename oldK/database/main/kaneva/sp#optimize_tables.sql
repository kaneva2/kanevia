-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS optimize_tables;

DELIMITER ;;
CREATE PROCEDURE `optimize_tables`()
BEGIN
  optimize table kaneva.this_weeks_community_views;
  optimize table kaneva.summary_asset_channels;
  optimize table kaneva.threads_base;
  optimize table kaneva.topics_base;
  
  
  
  
  
  
  
  

END
;;
DELIMITER ;