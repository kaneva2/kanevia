-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_message_gifts;

DELIMITER ;;
CREATE PROCEDURE `update_message_gifts`(IN __message_id INT, IN __gift_status ENUM('U','A','R'))
BEGIN
DECLARE _whoFrom INT(11) DEFAULT NULL;
DECLARE _whoTo INT(11) DEFAULT NULL;
DECLARE _old_gift_status ENUM('U','A','R');
DECLARE __sql_statement VARCHAR(512);
 
SELECT from_id, to_id INTO _whoFrom, _whoTo FROM messages WHERE message_id = __message_id LIMIT 1;

SELECT gift_status INTO _old_gift_status FROM message_gifts WHERE message_id = __message_id LIMIT 1;

UPDATE message_gifts 
	SET gift_status = __gift_status
	WHERE message_id = __message_id;

IF __gift_status = 'A' AND _old_gift_status = 'U' THEN 
	IF _whoTo > 0 THEN
		UPDATE kaneva.users_stats SET number_gifts_pending = number_gifts_pending - 1 WHERE user_id = _whoTo AND number_gifts_pending > 0;
		UPDATE kaneva.users_stats SET number_gifts_received = number_gifts_received + 1 WHERE user_id = _whoTo;
	END IF;
END IF;
	
IF __gift_status = 'R' AND _old_gift_status = 'U' THEN 
	IF _whoTo > 0 THEN
		UPDATE kaneva.users_stats SET number_gifts_pending = number_gifts_pending - 1 WHERE user_id = _whoTo AND number_gifts_pending > 0;
	END IF;
END IF;

END
;;
DELIMITER ;