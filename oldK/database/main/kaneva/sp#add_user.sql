-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_user;

DELIMITER ;;
CREATE PROCEDURE `add_user`( IN _username VARCHAR(22), IN _password VARCHAR(64), IN _salt VARCHAR(16),
		IN _role VARCHAR(32), IN _account_type INT, IN _first_name VARCHAR(50), IN _last_name VARCHAR(50), 
		IN _display_name VARCHAR(30), IN _gender CHAR, IN _homepage VARCHAR(200), IN _email VARCHAR(100), 
		IN _show_email VARCHAR(11), IN _key_value VARCHAR(20), IN _birth_date DATE, IN _country CHAR(2), 
		IN _zip_code VARCHAR(13), IN _registration_key VARCHAR(50), IN _ip_address INT UNSIGNED, 
		IN _last_ip_address INT UNSIGNED, IN _join_source_community_id INT, OUT _user_id INT)
BEGIN 
DECLARE __sql_statement VARCHAR(512);
DECLARE __country VARCHAR(100);
DECLARE __zip_code VARCHAR(13);
DECLARE __city VARCHAR(50);
DECLARE __state VARCHAR(4);
DECLARE __city_state VARCHAR(60);
DECLARE __location VARCHAR(80);


DECLARE __cur_datetime DATETIME;
SET __cur_datetime = (SELECT NOW());

SET __zip_code='';
SET __city='';
SET __state='';
SET __city_state='';

SET __country = (SELECT country FROM countries WHERE country_id = _country);
 
IF (__country IS NOT NULL) THEN
  IF (TRIM(_country) = 'US') THEN
    SET __zip_code =(SELECT zip_code FROM zip_codes WHERE zip_code = _zip_code);
    IF (__zip_code IS NOT NULL) AND (__zip_code <> '') AND (__zip_code <> '00000') THEN
      SET __city = (SELECT city FROM zip_codes WHERE zip_code = __zip_code);
      SET __state = (SELECT state_code FROM zip_codes WHERE zip_code = __zip_code);
      SET __city_state = CONCAT(__city,',',__state);
    ELSE
      SET __zip_code='00000';
    END IF;
  END IF;
END IF;

IF (__city_state = '') THEN
    IF (_country IS NOT NULL) THEN
      SET __location = __country;
    ELSE
      SET __location='';
    END IF;
ELSE
    SET __location = __city_state;
END IF;

INSERT INTO users
	(
	username, PASSWORD, salt, role, account_type, first_name, last_name, display_name, 
	gender, homepage, email, show_email, key_value, 
	last_login, second_to_last_login, signup_date, birth_date, 
	newsletter, email_notify, public_profile, country, zip_code, master_user_id, 
	status_id, registration_key, ip_address, last_ip_address, join_source_community_id, 
	age, 
	over_21,
	active, location,
	_signup_date
	)
	VALUES
	(
	_username, _password, _salt, _role, _account_type, _first_name, _last_name, _display_name, 
	_gender, _homepage, _email, _show_email, _key_value, 
	__cur_datetime, __cur_datetime, __cur_datetime, _birth_date, 
	'Y', 'N', 'Y',	_country, _zip_code, NULL, 
	2, _registration_key, _ip_address, _last_ip_address, _join_source_community_id, 
	DATE_FORMAT(FROM_DAYS(TO_DAYS(CONCAT(DATE(NOW()),' 23:59:59'))-TO_DAYS(_birth_date)), '%Y')+0,
	IF((DATE_FORMAT(FROM_DAYS(TO_DAYS(CONCAT(DATE(NOW()),' 23:59:59'))-TO_DAYS(_birth_date)), '%Y')+0)>=21, 'Y','N'),
	'Y', __location,
	(kaneva.master_date() - UNIX_TIMESTAMP(__cur_datetime))
	);

SELECT LAST_INSERT_ID() INTO _user_id;

INSERT IGNORE INTO kaneva.users_stats (user_id) VALUES (_user_id);

IF (_email IS NOT NULL) THEN
	INSERT INTO users_email (user_id, email_hash) VALUES (_user_id, COALESCE(UNHEX(MD5(_email)), ''));
END IF;

CALL kaneva.post_insert_users(_user_id);

END
;;
DELIMITER ;