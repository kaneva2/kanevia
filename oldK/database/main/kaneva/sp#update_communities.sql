-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_communities;

DELIMITER ;;
CREATE PROCEDURE `update_communities`(
  IN __req_community_id INT, 
  IN __place_type_id    INT,
  IN __name_no_spaces   VARCHAR(100), 
  IN __is_public        CHAR(1), 
  IN __is_adult         CHAR(1), 
  IN __background_rgb   VARCHAR(8),
  IN __background_image VARCHAR(255), 
  IN __allow_publishing TINYINT, 
  IN __req_status_id INT)
    COMMENT '\n    Arguments\n      __req_community_id\n      __place_type_id\n      __name_no_spaces\n      __is_public\n      __is_adult\n      __background_rgb\n      __background_image\n      __allow_publishing\n      __req_status_id\n  '
this_proc:
BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
      
      GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
      CALL logging.logMsg('ERROR', SCHEMA(), 'update_communities', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
      RESIGNAL;
  END;


    CALL logging.logMsg('TRACE', SCHEMA(), 'update_communities', CONCAT('BEGIN (__req_community_id[',CAST(__req_community_id AS CHAR), '] __req_status_id[',CAST(__req_status_id AS CHAR),'] ...)'));

  -- BEGIN Validate Args
  SELECT 
    `status_id`, `creator_id` INTO @currentStatusId, @kanevaUserId
  FROM 
    `communities` 
  WHERE 
    `community_id` = __req_community_id;


  UPDATE communities 
    SET 
      place_type_id    = __place_type_id,
      name_no_spaces   = __name_no_spaces,
      is_adult         = __is_adult,
      background_rgb   = __background_rgb,
      background_image = __background_image,
      allow_publishing = __allow_publishing,
      status_id        = __req_status_id,
      last_edit        = NOW()
  WHERE 
      `community_id` = __req_community_id;

  CALL update_communities_is_public(__req_community_id, __is_public);


  IF (__req_status_id = currentStatusId) THEN
    CALL logging.logMsg('WARN', SCHEMA(), 'update_communities_delete', CONCAT('__req_community_id[',CAST(__req_community_id AS CHAR), '] already in __req_status_id[',CAST(__req_status_id AS CHAR),'] -- all done'));
    LEAVE this_proc;
  END IF;

  -- 1 if deleteing 
  IF __req_status_id = 3 THEN
    -- 1a) replenish inventory
    CALL logging.logMsg('TRACE', SCHEMA(), 'update_communities', CONCAT('begin replenish wipa for __req_community_id[',CAST(__req_community_id AS CHAR),']'));

    INSERT INTO wok.inventory_pending_adds2 (`kaneva_user_id`, `global_id`, `inventory_type`, `inventory_sub_type`, `quantity`, `last_touch_datetime`) 
    (SELECT 
        @kanevaUserId, `global_id`, 'B', `inventory_sub_type`, @returnQty := COUNT(`global_id`) AS `returnQty`, NOW()
      FROM 
        `wok`.`dynamic_objects`
      WHERE 
        wok.zoneType(`zone_index`) = 6 AND
        `zone_instance_id` = __req_community_id AND 
        `expired_date`       IS NULL AND -- no try on items
        `inventory_sub_type` <> 1024  -- no unlimited items
      GROUP BY
        `global_id`, 
        `inventory_sub_type`
    )
    ON DUPLICATE KEY UPDATE `quantity` = `quantity` + @returnQty, `last_touch_datetime` = NOW();

    SELECT ROW_COUNT() INTO @numRecs;
    CALL logging.logMsg('TRACE', SCHEMA(), 'update_communities', CONCAT('done replenish wipa for __req_community_id[',CAST(__req_community_id AS CHAR),'] with [', @numRecs ,'] changes'));


    -- 1a) delete all dynamic objects playlists
    CALL logging.logMsg('TRACE', SCHEMA(), 'update_communities', 'delete dynamic_object_playlists');
    DELETE
      wok.dynamic_object_playlists.*
    FROM 
      wok.dynamic_object_playlists 
     LEFT JOIN wok.dynamic_object_parameters
        ON wok.dynamic_object_playlists.obj_placement_id = wok.dynamic_object_parameters.obj_placement_id
    WHERE
      wok.dynamic_object_parameters.obj_placement_id IS NULL AND
      wok.dynamic_object_playlists.zone_instance_id = __community_id AND
      wok.zoneType(wok.dynamic_object_playlists.zone_index) = 6;


    -- 1b) DELETE all dynamic objects (scripted + non scripted)
    DELETE
        wok.dynamic_objects.*, 
        wok.dynamic_object_parameters.* 
    FROM 
        wok.dynamic_objects
        LEFT JOIN wok.dynamic_object_parameters
        ON wok.dynamic_object_parameters.obj_placement_id = wok.dynamic_objects.obj_placement_id
    WHERE 
        wok.dynamic_objects.zone_instance_id = __community_id AND
        wok.zoneType(wok.dynamic_objects.zone_index) = 6;

  END IF;

  CALL logging.logMsg('TRACE', SCHEMA(), 'update_communities', CONCAT('END (__req_community_id[',CAST(__req_community_id AS CHAR),'] __req_status_id[',CAST(__req_status_id AS CHAR),'])'));

END
;;
DELIMITER ;