-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS gen_uid;

DELIMITER ;;
CREATE FUNCTION `gen_uid`() RETURNS varchar(64)
    NO SQL
BEGIN
/* New universal function for generating unique IDs */
  return gen_id(rand()*1048576,rand()*1048576);
END
;;
DELIMITER ;