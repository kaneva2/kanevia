-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS sync_playlist_counts;

DELIMITER ;;
CREATE PROCEDURE `sync_playlist_counts`(__cutoff_time DATETIME)
BEGIN
DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE _asset_group_id INT;
DECLARE _count INT;
DECLARE _count_C INT;
DECLARE _name VARCHAR(22);
DECLARE __message_text VARCHAR(500);
DECLARE arch_cursor CURSOR FOR 
		SELECT asset_group_id, asset_count, `name` FROM asset_groups;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;
SET i = 0;
OPEN arch_cursor;
REPEAT
    FETCH arch_cursor INTO _asset_group_id, _count_C, _name;
    IF NOT done THEN
	-- select 'top of loop', done, i, __cutoff_time, _asset_group_id, _name;
	SET i=i+1;
	SELECT COALESCE(COUNT(*),0) INTO _count
		FROM asset_group_assets WHERE asset_group_id  = _asset_group_id;
	IF _count <> _count_C THEN
		-- SELECT _asset_group_id, _name, _count, _count_C;
		UPDATE asset_groups SET asset_count = _count WHERE asset_group_id = _asset_group_id;
		SELECT  CONCAT('Synced asset_group_id: ',CAST(_asset_group_id AS CHAR),': ',_name,'. Old count: ',CAST(_count_C AS CHAR),
		 		'; new count: ',CAST(_count AS CHAR)) INTO __message_text;
		CALL shard_info.add_maintenance_log ('kaneva','sync_playlist_counts',__message_text);
	END IF;
	IF NOW() >= __cutoff_time THEN 
		SET done = 1; 
	END IF;
    END IF;	
UNTIL done END REPEAT;
CLOSE arch_cursor;
END
;;
DELIMITER ;