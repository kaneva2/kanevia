-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_my_searchable_interests;

DELIMITER ;;
CREATE PROCEDURE `update_my_searchable_interests`(IN __user_id INT)
BEGIN
  DECLARE done INT DEFAULT 0;
  DECLARE __interests TEXT;
  DECLARE __interest VARCHAR(255);
  DECLARE xc CURSOR for SELECT interest FROM user_interests ui Inner Join interests i on ui.interest_id = i.interest_id where ui.user_id = __user_id;
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	OPEN xc;
  
	SET __interests = '';
  REPEAT
    FETCH xc INTO __interest;
    if not done then
      SET __interests = CONCAT(__interests, __interest ,' ');
    end if;
  UNTIL done END REPEAT;
  CLOSE xc;
  update kaneva.users set last_update = now() where user_id = __user_id;
	
	Replace into kaneva.user_interests_searchable VALUES(__user_id,__interests);
END
;;
DELIMITER ;