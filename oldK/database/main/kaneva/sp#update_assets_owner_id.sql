-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_owner_id;

DELIMITER ;;
CREATE PROCEDURE `update_assets_owner_id`(IN __asset_id INT, IN __owner_id  INT,
		IN __updating_user_id INT)
BEGIN

DECLARE _old_owner_id INT; 
DECLARE _asset_type_id INT;

SELECT owner_id, asset_type_id INTO _old_owner_id, _asset_type_id FROM assets WHERE asset_id = __asset_id;

UPDATE assets 
	SET owner_id = __owner_id, 
	last_updated_date = NOW(),
	last_updated_user_id =__updating_user_id
	WHERE asset_id = __asset_id;

IF (__owner_id <> _old_owner_id) THEN
    UPDATE summary_assets_by_type_by_user SET `count`=`count`- 1 WHERE user_id = _old_owner_id AND asset_type_id = _asset_type_id;
    UPDATE summary_assets_by_type_by_user SET `count`=`count`+ 1 WHERE user_id = __owner_id AND asset_type_id = _asset_type_id;
END IF;

END
;;
DELIMITER ;