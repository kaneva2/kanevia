-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS after_blog_insert;

DELIMITER ;;
CREATE TRIGGER `after_blog_insert` AFTER INSERT ON `blogs` FOR EACH ROW BEGIN
    update users_stats set number_of_blogs = number_of_blogs + 1 WHERE user_id = new.created_user_id;
END
;;
DELIMITER ;