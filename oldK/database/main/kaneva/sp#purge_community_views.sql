-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS purge_community_views;

DELIMITER ;;
CREATE PROCEDURE `purge_community_views`(__purge_before_date	DATE, __purge_cutoff_time DATETIME)
BEGIN
DECLARE	__Max_view_id		bigint;
DECLARE	__community_view_RowCnt		bigint;
DECLARE __message_text varchar(500);
SELECT	IFNULL(COUNT(view_id),0) INTO __community_view_RowCnt
	FROM	community_views
	WHERE	created_date < __purge_before_date;
SELECT IFNULL(MAX(view_id),__purge_before_date) INTO __Max_view_id
	FROM	community_views
	WHERE	created_date < __purge_before_date;
SELECT CONCAT('Starting purge ... ',CAST(__community_view_RowCnt as CHAR),' rows to purge. Purge before date: ',
											CAST(__purge_before_date as CHAR),'; Purge cutoff time: ',
											CAST(__purge_cutoff_time AS CHAR)) INTO __message_text;
CALL shard_info.add_maintenance_log ('kaneva','purge_community_views',__message_text);

-- Loop while anything to delete.
WHILE (__community_view_RowCnt > 0 AND NOW() < __purge_cutoff_time)    DO
	DELETE FROM	community_views WHERE view_id <= __Max_view_id LIMIT 5000;
	SELECT IFNULL(ROW_COUNT(),0) INTO __community_view_RowCnt;
	SELECT  CONCAT('Purged ',CAST(__community_view_RowCnt AS CHAR),' rows.') INTO __message_text;
	CALL shard_info.add_maintenance_log ('kaneva','purge_community_views',__message_text);
	END WHILE;
SELECT 'Purge completed.' INTO __message_text;
CALL shard_info.add_maintenance_log ('kaneva','purge_community_views',__message_text);
END
;;
DELIMITER ;