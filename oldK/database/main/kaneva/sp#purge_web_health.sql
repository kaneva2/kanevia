-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS purge_web_health;

DELIMITER ;;
CREATE PROCEDURE `purge_web_health`(__purge_before_date	DATE, __purge_cutoff_time DATETIME)
BEGIN

DECLARE	__Max_dt_taken		datetime;
DECLARE	__web_health_RowCnt		int;
DECLARE __message_text varchar(500);

SELECT	IFNULL(COUNT(DISTINCT dt_taken),0) INTO __web_health_RowCnt
	FROM	web_health
	WHERE	DATE(dt_taken) < __purge_before_date;

SELECT IFNULL(MAX(dt_taken),__purge_before_date) INTO __Max_dt_taken
	FROM	web_health
	WHERE	DATE(dt_taken) < __purge_before_date;

SELECT CONCAT('Starting purge ... ',CAST(__web_health_RowCnt as CHAR),' rows to purge. Purge before date: ',
											CAST(__purge_before_date as CHAR),'; Purge cutoff time: ',
											CAST(__purge_cutoff_time AS CHAR)) INTO __message_text;
CALL shard_info.add_maintenance_log ('kaneva','purge_web_health',__message_text);

-- Loop while anything to delete.
WHILE (__web_health_RowCnt > 0 AND NOW() < __purge_cutoff_time)    DO

	DELETE FROM	web_health WHERE DATE(dt_taken) < __purge_before_date LIMIT 5000;

	SELECT IFNULL(ROW_COUNT(),0) INTO __web_health_RowCnt;

	SELECT  CONCAT('Purged ',CAST(__web_health_RowCnt AS CHAR),' rows.') INTO __message_text;
	CALL shard_info.add_maintenance_log ('kaneva','purge_web_health',__message_text);

	END WHILE;

SELECT 'Purge completed.' INTO __message_text;
CALL shard_info.add_maintenance_log ('kaneva','purge_web_health',__message_text);

END
;;
DELIMITER ;