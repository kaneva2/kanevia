-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_runtime;

DELIMITER ;;
CREATE PROCEDURE `update_assets_runtime`(IN __asset_id INT, IN __run_time INT)
BEGIN

UPDATE assets 
	SET run_time_seconds = __run_time,
	last_updated_date = NOW()
	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;