-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS after_topic_update;

DELIMITER ;;
CREATE TRIGGER `after_topic_update` AFTER UPDATE ON `topics_base` FOR EACH ROW BEGIN
 
 /* this is now moved to after_blogs_update
  if ( (new.status_id <> 1) AND (old.status_id = 1) ) AND ( new.topic_type IN (2,3) ) THEN
    update users_stats set number_of_blogs = number_of_blogs - 1 WHERE user_id = new.created_user_id;
  END IF; */
  
  /* Update the summary info for a topic update if a new thread is added to this topic */
  if (new.last_thread_id <> old.last_thread_id) THEN
	UPDATE forums SET last_post_date = NOW(), last_topic_id = new.topic_id, last_user_id = new.last_user_id WHERE forum_id = new.forum_id;
  END IF;
  
END
;;
DELIMITER ;