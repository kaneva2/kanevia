-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS process_asset_violation;

DELIMITER ;;
CREATE PROCEDURE `process_asset_violation`(assetID INT UNSIGNED, violationAction INT UNSIGNED, modifierID  INT UNSIGNED)
BEGIN

   
   DECLARE _MATURE INT DEFAULT 3;
   DECLARE _DELETED INT DEFAULT 2;
   DECLARE _MARK_DELETED INT DEFAULT 4;
   DECLARE _ACTIVE INT DEFAULT 1;
   DECLARE _REMOVE INT DEFAULT 4;
   DECLARE _AP INT DEFAULT 2;
   DECLARE _ADMIN_OK INT DEFAULT 5;
   DECLARE _GENERAL INT DEFAULT 1;

   DECLARE _last_action INT DEFAULT 0;

    
  START TRANSACTION;
    
	/*begin repetative processing*/
	IF violationAction = 2 THEN -- Mark as AP (adult)

		-- update assets restriction level 
		CALL update_asset_restriction(_MATURE, assetID, 'Y');

		-- update search boxes
		CALL search_kaneva.flag_search_asset(assetID);

		-- update violation action
		UPDATE 
			violation_reports 
		SET 
			violation_action_type_id = _AP, 
			last_updated = Now(),
			modifiers_id = modifierID
		WHERE
			asset_id = assetID;

	ELSEIF violationAction = 4 THEN -- Remove 
		-- delete the asset
		-- CALL delete_asset(assetID);

		-- mark as deleted
		CALL update_asset_status(_MARK_DELETED, assetID);

    		-- mark as deleted in community
    		CALL update_assets_status_in_channels(assetID, _DELETED);

		-- remove from search boxes
		CALL search_kaneva.delete_search_asset(assetID);

		-- update violation action
		UPDATE
			violation_reports
		SET
			violation_action_type_id = _REMOVE, 
			last_updated = Now(),
			modifiers_id = modifierID 
		WHERE
			asset_id = assetID;

	ELSEIF violationAction = 5 THEN -- administrative over ride to good status 
		-- get the last action done on the item
		SELECT 
			violation_action_type_id INTO _last_action 
		FROM 
			violation_reports 
		WHERE
			asset_id = assetID 
		ORDER BY
			violation_id DESC
		LIMIT 1;
		
		-- set the item back based on the last action done
		IF _last_action = 2 THEN -- remove the AP  setting
			
			-- update assets restriction level  
			CALL update_asset_restriction(_GENERAL, assetID, 'N');

			-- update search boxes
			CALL search_kaneva.flag_search_asset(assetID);
		
		ELSEIF _last_action = 4 THEN -- undelete it

       		   	-- mark as active
		   	CALL update_asset_status(_ACTIVE, assetID);

       			-- mark as undeleted in community
       			CALL update_assets_status_in_channels(assetID, _ACTIVE);

 			-- update search boxes
			CALL search_kaneva.flag_search_asset(assetID);

		END IF;

		-- update violation action
		UPDATE 
			violation_reports 
		SET 
			violation_action_type_id = _ADMIN_OK, 
			last_updated = Now(),
			modifiers_id = modifierID 
		WHERE 
			asset_id = assetID;
	END IF;
	
	-- return success = 1 if no exceptions thrown
	select 1;

  COMMIT;
  END
;;
DELIMITER ;