-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_sort_order;

DELIMITER ;;
CREATE PROCEDURE `update_assets_sort_order`(IN __asset_id INT, IN __sort_order  INT)
BEGIN

UPDATE assets 
	SET sort_order = __sort_order
	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;