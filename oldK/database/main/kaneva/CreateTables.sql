-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

/*
SQLyog Enterprise - MySQL GUI v7.14 
MySQL - 5.0.67-community-log : Database - kaneva
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `__balance_temp0` */

CREATE TABLE `__balance_temp0` (
  `balance` float NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `match_criteria` varchar(255) NOT NULL,
  KEY `Index_1` (`match_criteria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `__balances_temp` */

CREATE TABLE `__balances_temp` (
  `match_criteria` varchar(255) default NULL,
  KEY `Index_1` (`match_criteria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `__balances_temp2` */

CREATE TABLE `__balances_temp2` (
  `balance` float NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `__related_items_temp` */

CREATE TABLE `__related_items_temp` (
  `_order` double NOT NULL,
  `base_asset_id` int(11) NOT NULL,
  `related_asset_id` int(11) NOT NULL,
  `mature` enum('Y','N') default 'N',
  PRIMARY KEY  (`_order`),
  UNIQUE KEY `base_asset_id` (`base_asset_id`,`related_asset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `acquisition_source` */

CREATE TABLE `acquisition_source` (
  `acquisition_source_id` int(11) unsigned NOT NULL auto_increment COMMENT 'PK',
  `acquisition_source_type_id` int(11) unsigned NOT NULL COMMENT 'FK to acquisition_source_type table',
  `description` varchar(128) NOT NULL default '' COMMENT 'domain name',
  PRIMARY KEY  (`acquisition_source_id`),
  KEY `IDX_acquisition_source_type__id` (`acquisition_source_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1072 DEFAULT CHARSET=latin1 COMMENT='First time visit referring URL for users that join Kaneva';

/*Table structure for table `acquisition_source_type` */

CREATE TABLE `acquisition_source_type` (
  `acquisition_source_type_id` int(11) unsigned NOT NULL auto_increment COMMENT 'PK',
  `description` varchar(128) NOT NULL default '' COMMENT 'description of acquisition type',
  PRIMARY KEY  (`acquisition_source_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Source acquisition types';

/*Table structure for table `address` */

CREATE TABLE `address` (
  `address_id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL default '0',
  `name` varchar(200) NOT NULL default '',
  `phone_number` varchar(50) default NULL,
  `address1` varchar(100) default NULL,
  `address2` varchar(100) default NULL,
  `city` varchar(100) default NULL,
  `state_code` varchar(100) default NULL,
  `zip_code` varchar(25) NOT NULL default '',
  `country_id` char(2) default NULL,
  PRIMARY KEY  (`address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17934 DEFAULT CHARSET=latin1;

/*Table structure for table `asset_attribute_values` */

CREATE TABLE `asset_attribute_values` (
  `asset_id` int(11) NOT NULL default '0',
  `attribute_id` int(11) default NULL,
  `category_attribute_value_id` int(11) default NULL,
  `attribute_value` varchar(255) default NULL,
  KEY `FK_asset_attribute_values_assets` (`asset_id`),
  KEY `FK_asset_attribute_values_category_attribute_values` (`category_attribute_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `asset_categories` */

CREATE TABLE `asset_categories` (
  `category_id` int(11) NOT NULL auto_increment,
  `asset_type_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=284 DEFAULT CHARSET=latin1;

/*Table structure for table `asset_channels` */

CREATE TABLE `asset_channels` (
  `asset_channel_id` int(11) NOT NULL auto_increment,
  `channel_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `status_id` int(11) NOT NULL,
  PRIMARY KEY  (`asset_channel_id`),
  UNIQUE KEY `UNI` (`channel_id`,`asset_id`),
  KEY `asset_channels_idx` (`channel_id`),
  KEY `chnl_status` (`asset_id`,`channel_id`,`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14120806 DEFAULT CHARSET=latin1;

/*Table structure for table `asset_diggs` */

CREATE TABLE `asset_diggs` (
  `digg_id` int(11) unsigned NOT NULL auto_increment,
  `user_id` int(11) unsigned NOT NULL default '0',
  `asset_id` int(11) unsigned NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`digg_id`),
  KEY `digger` (`user_id`,`asset_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2274587 DEFAULT CHARSET=latin1;

/*Table structure for table `asset_group_assets` */

CREATE TABLE `asset_group_assets` (
  `asset_group_id` int(11) NOT NULL default '0',
  `asset_id` int(11) NOT NULL default '0',
  `sort_order` int(11) NOT NULL default '0',
  PRIMARY KEY  (`asset_group_id`,`asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `asset_groups` */

CREATE TABLE `asset_groups` (
  `asset_group_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `channel_id` int(11) NOT NULL default '0',
  `asset_count` int(10) unsigned NOT NULL default '0',
  `description` varchar(256) NOT NULL default '',
  PRIMARY KEY  (`asset_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=362125 DEFAULT CHARSET=latin1;

/*Table structure for table `asset_permissions` */

CREATE TABLE `asset_permissions` (
  `permission_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `asset_ratings` */

CREATE TABLE `asset_ratings` (
  `asset_rating_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `description` varchar(255) default NULL,
  `minium_age` int(11) default NULL,
  `display_order` int(11) NOT NULL default '0',
  `asset_type_id` int(11) default NULL,
  PRIMARY KEY  (`asset_rating_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Table structure for table `asset_share_connection` */

CREATE TABLE `asset_share_connection` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `asset_id` int(10) unsigned NOT NULL default '0',
  `user_id` int(10) unsigned default NULL,
  `user_ip` varchar(20) default NULL,
  `parent_id` int(10) unsigned NOT NULL default '0',
  `path` varchar(255) NOT NULL default '',
  `depth` int(10) unsigned NOT NULL default '0',
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=871625 DEFAULT CHARSET=latin1;

/*Table structure for table `asset_share_connection_map_data` */

CREATE TABLE `asset_share_connection_map_data` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `asset_share_connection_id` int(11) NOT NULL default '0',
  `map_data` text NOT NULL,
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `asset_share_connection_stats` */

CREATE TABLE `asset_share_connection_stats` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `asset_share_connection_id` int(11) NOT NULL default '0',
  `depth` int(11) NOT NULL default '0',
  `count` int(11) NOT NULL default '0',
  `created_datetime` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `asset_shares` */

CREATE TABLE `asset_shares` (
  `share_id` int(11) unsigned NOT NULL auto_increment,
  `from_user_id` int(10) unsigned default NULL,
  `asset_id` int(11) unsigned NOT NULL default '0',
  `to_email` text,
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `from_ip` varchar(20) default NULL,
  `to_user_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`share_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5674586 DEFAULT CHARSET=latin1;

/*Table structure for table `asset_subscriptions` */

CREATE TABLE `asset_subscriptions` (
  `asset_subscription_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `asset_id` int(11) NOT NULL default '0',
  `community_id` int(11) default NULL,
  `length_of_subscription` int(10) NOT NULL default '0',
  `amount` decimal(12,2) NOT NULL default '0.00',
  `amount_kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `status_id` int(11) NOT NULL default '0',
  `updated_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`asset_subscription_id`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=latin1;

/*Table structure for table `asset_subtypes` */

CREATE TABLE `asset_subtypes` (
  `asset_subtype_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `icon_path` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`asset_subtype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `asset_types` */

CREATE TABLE `asset_types` (
  `asset_type_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `icon_path` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`asset_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `asset_upload` */

CREATE TABLE `asset_upload` (
  `asset_upload_id` int(11) NOT NULL auto_increment,
  `asset_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `path` varchar(255) NOT NULL default '',
  `filename` varchar(255) default NULL,
  `size` bigint(20) default '0',
  `MD5_hash` varchar(100) default NULL,
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `completed_datetime` datetime default NULL,
  `type_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`asset_upload_id`),
  KEY `asset_id_join` (`asset_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1202209 DEFAULT CHARSET=latin1;

/*Table structure for table `asset_views` */

CREATE TABLE `asset_views` (
  `view_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL default '0',
  `asset_id` int(10) unsigned NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `ip_address` int(11) unsigned default NULL,
  PRIMARY KEY  (`view_id`),
  KEY `cv_hub` (`asset_id`),
  KEY `cid_user_id` (`asset_id`,`user_id`),
  KEY `cv_idx_1` USING BTREE (`created_date`,`asset_id`,`ip_address`),
  KEY `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70860078 DEFAULT CHARSET=latin1;

/*Table structure for table `assets` */

CREATE TABLE `assets` (
  `asset_id` int(11) NOT NULL auto_increment,
  `asset_type_id` int(11) NOT NULL default '0',
  `is_kaneva_game` binary(1) NOT NULL default '0',
  `asset_sub_type_id` int(11) NOT NULL default '0',
  `asset_rating_id` int(11) NOT NULL default '0',
  `owner_id` int(11) NOT NULL default '0',
  `file_size` bigint(20) unsigned NOT NULL default '0',
  `run_time_seconds` int(11) NOT NULL default '0',
  `category1_id` int(11) NOT NULL default '0',
  `category2_id` int(11) NOT NULL default '0',
  `category3_id` int(11) NOT NULL default '0',
  `amount` decimal(12,2) NOT NULL default '0.00',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `image_path` varchar(255) default NULL,
  `image_full_path` varchar(255) default NULL,
  `thumbnail_assetdetails_path` varchar(255) NOT NULL,
  `thumbnail_xlarge_path` varchar(255) NOT NULL,
  `thumbnail_large_path` varchar(255) NOT NULL,
  `thumbnail_medium_path` varchar(255) NOT NULL,
  `thumbnail_small_path` varchar(255) NOT NULL,
  `thumbnail_gen` tinyint(2) NOT NULL default '0',
  `image_caption` varchar(100) default NULL,
  `image_type` varchar(50) default NULL,
  `status_id` int(11) NOT NULL default '0',
  `created_date` datetime default NULL,
  `last_updated_date` datetime default NULL,
  `last_updated_user_id` int(11) default NULL,
  `game_exe` varchar(100) default NULL,
  `torrent_id` int(11) default NULL,
  `license_cc` varchar(100) NOT NULL default 'http://creativecommons.org/licenses/by/2.5/',
  `license_URL` varchar(200) default NULL,
  `license_type` char(1) NOT NULL default 'P',
  `license_name` varchar(50) default NULL,
  `license_additional` varchar(100) default NULL,
  `publish_status_id` int(11) NOT NULL default '0',
  `percent_complete` int(11) default '0',
  `ip_address` varchar(30) default NULL,
  `game_encryption_key` varchar(16) default NULL,
  `icon_image_data` longblob,
  `icon_image_path` varchar(100) default NULL,
  `icon_image_type` varchar(50) default NULL,
  `require_login` smallint(6) NOT NULL default '0',
  `content_extension` varchar(255) default NULL,
  `target_dir` varchar(255) default NULL,
  `media_path` varchar(255) default '',
  `permission` int(11) NOT NULL default '1',
  `permission_group` int(11) default NULL,
  `published` enum('Y','N') NOT NULL default 'Y',
  `mature` enum('Y','N') NOT NULL default 'N',
  `public` enum('Y','N') NOT NULL default 'Y',
  `playlistable` enum('Y','N') NOT NULL default 'Y',
  `over_21_required` enum('Y','N') default 'N',
  `asset_offsite_id` varchar(1000) NOT NULL default '',
  `sort_order` int(10) unsigned NOT NULL default '0',
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `name` varchar(100) NOT NULL default '',
  `body_text` text,
  `short_description` varchar(50) default NULL,
  `teaser` text,
  `owner_username` varchar(22) NOT NULL,
  `keywords` text,
  PRIMARY KEY  (`asset_id`),
  KEY `FK_assets_asset_ratings` (`asset_rating_id`),
  KEY `FK_Assets_AssetTypes` (`asset_type_id`),
  KEY `INDEX_status_id` (`status_id`),
  KEY `INDEX_publish_status_id` (`publish_status_id`),
  KEY `atid_psid_arid` (`asset_type_id`,`status_id`,`publish_status_id`,`asset_rating_id`,`image_path`),
  KEY `cd` (`created_date`),
  KEY `pubid_sid_arid_atid_p` (`publish_status_id`,`status_id`,`asset_rating_id`,`asset_type_id`,`permission`),
  KEY `__bunch_o_asset_fields` USING BTREE (`amount`,`permission`,`owner_id`,`asset_type_id`,`status_id`,`publish_status_id`,`asset_rating_id`),
  KEY `published` (`published`),
  KEY `mature` (`mature`),
  KEY `public` (`public`),
  KEY `playlistable` (`playlistable`),
  KEY `owner` (`owner_id`),
  KEY `IDX_sort_order` (`sort_order`)
) ENGINE=InnoDB AUTO_INCREMENT=5296068 DEFAULT CHARSET=latin1;

/*Table structure for table `assets_stats` */

CREATE TABLE `assets_stats` (
  `asset_id` int(11) NOT NULL default '0',
  `number_of_comments` int(11) NOT NULL default '0',
  `number_of_downloads` int(11) NOT NULL default '0',
  `number_of_streams` int(11) NOT NULL default '0',
  `number_of_stream_requests` int(11) NOT NULL default '0',
  `number_of_shares` int(10) unsigned NOT NULL default '0',
  `number_of_channels` int(10) unsigned NOT NULL default '0',
  `number_of_diggs` int(10) unsigned NOT NULL default '0',
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`asset_id`),
  KEY `downloads` (`number_of_downloads`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `audit_promotional_offer_items` */

CREATE TABLE `audit_promotional_offer_items` (
  `wok_item_id` int(10) unsigned NOT NULL default '0',
  `promotion_id` smallint(5) unsigned NOT NULL default '0',
  `gift_card_id` int(10) unsigned NOT NULL default '0',
  `community_id` int(10) unsigned NOT NULL default '0',
  `item_description` varchar(125) NOT NULL default '',
  `alternate_description` varchar(125) default NULL,
  `market_price` smallint(5) unsigned NOT NULL default '0',
  `quantity` smallint(5) unsigned NOT NULL default '0',
  `gender` enum('M','F','U') NOT NULL default 'U',
  `promotion_offer_id` smallint(5) unsigned NOT NULL default '0',
  `modifiers_id` int(10) unsigned NOT NULL default '0',
  `date_modified` datetime NOT NULL default '0000-00-00 00:00:00',
  `operation` varchar(20) NOT NULL default ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `audit_promotional_offers` */

CREATE TABLE `audit_promotional_offers` (
  `promotion_id` smallint(5) unsigned NOT NULL default '0',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `dollar_amount` decimal(12,2) NOT NULL default '0.00',
  `kei_point_amount` decimal(12,2) NOT NULL default '0.00',
  `free_points_awarded_amount` decimal(12,2) NOT NULL default '0.00',
  `free_kei_point_ID` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `is_special` tinyint(1) unsigned NOT NULL default '0',
  `promotion_description` varchar(100) default NULL,
  `promotional_offers_type_id` smallint(6) NOT NULL default '0',
  `wok_pass_group_id` int(11) NOT NULL,
  `sku` smallint(5) unsigned default '0',
  `bundle_title` varchar(100) NOT NULL default '',
  `bundle_subheading1` varchar(100) default NULL,
  `bundle_subheading2` varchar(100) default NULL,
  `promotion_start` datetime NOT NULL default '0000-00-00 00:00:00',
  `promotion_end` datetime NOT NULL default '0000-00-00 00:00:00',
  `promotion_list_heading` varchar(100) NOT NULL default '',
  `highlight_color` char(7) default NULL,
  `special_background_color` char(7) default NULL,
  `special_background_image` varchar(500) default NULL,
  `special_sticker_image` varchar(45) default NULL,
  `promotional_package_label` varchar(100) default NULL,
  `special_font_color` char(7) default NULL,
  `value_of_credits` decimal(12,2) default NULL,
  `modifiers_id` int(10) unsigned NOT NULL default '0',
  `date_modified` datetime NOT NULL default '0000-00-00 00:00:00',
  `operation` varchar(20) NOT NULL default ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `audit_users` */

CREATE TABLE `audit_users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(22) NOT NULL default '',
  `username_old` varchar(22) NOT NULL default '',
  `display_name` varchar(30) NOT NULL default '',
  `display_name_old` varchar(30) NOT NULL default '',
  `zip_code` varchar(13) NOT NULL default '0',
  `zip_code_old` varchar(13) NOT NULL default '0',
  `country` char(2) NOT NULL default '99',
  `country_old` char(2) NOT NULL default '99',
  `location` varchar(80) NOT NULL default '',
  `location_old` varchar(80) NOT NULL default '',
  `date_modified` datetime NOT NULL default '0000-00-00 00:00:00',
  KEY `ui` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `banned_ip_addresses` */

CREATE TABLE `banned_ip_addresses` (
  `ip_address` bigint(20) unsigned NOT NULL,
  PRIMARY KEY  (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `billing_information` */

CREATE TABLE `billing_information` (
  `billing_information_id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL default '0',
  `nickname` varchar(50) NOT NULL default '',
  `information_type` int(11) NOT NULL default '0',
  `name_on_card` varchar(100) default NULL,
  `card_number` varchar(40) default NULL,
  `card_type` varchar(50) default NULL,
  `exp_month` varchar(50) default NULL,
  `exp_day` varchar(50) default NULL,
  `exp_year` varchar(50) default NULL,
  `address_id` int(11) default '0',
  PRIMARY KEY  (`billing_information_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29434 DEFAULT CHARSET=latin1;

/*Table structure for table `blocked_users` */

CREATE TABLE `blocked_users` (
  `user_id` int(11) NOT NULL default '0',
  `profile_id` int(11) NOT NULL default '0',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `zone_type` smallint(6) NOT NULL default '0' COMMENT 'Apartment or broadband',
  `zone_instance_id` int(11) NOT NULL default '0' COMMENT 'id of the zone being blocked if block_type is player or zone travel',
  `reason_description` varchar(80) NOT NULL default '0',
  `is_comm_blocked` tinyint(4) NOT NULL default '0' COMMENT 'Communication blocked',
  `is_follow_blocked` tinyint(4) NOT NULL default '0' COMMENT 'Apartment or follow person',
  `is_zone_blocked` tinyint(4) NOT NULL default '0' COMMENT 'Broadband zone blocked',
  `block_id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`block_id`),
  KEY `puz` (`profile_id`,`user_id`,`zone_type`),
  KEY `pid` (`profile_id`),
  KEY `uid` (`user_id`),
  KEY `zt` (`zone_type`),
  KEY `zt_izb` (`zone_type`,`is_zone_blocked`)
) ENGINE=InnoDB AUTO_INCREMENT=137334 DEFAULT CHARSET=latin1;

/*Table structure for table `blog_categories` */

CREATE TABLE `blog_categories` (
  `blog_category_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  `thumbnail_image` varchar(50) default NULL,
  PRIMARY KEY  (`blog_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `blog_comments` */

CREATE TABLE `blog_comments` (
  `blog_comment_id` int(11) NOT NULL auto_increment,
  `blog_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `subject` varchar(100) default NULL,
  `body_text` text,
  `ip_address` varchar(30) default NULL,
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) default NULL,
  `status_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`blog_comment_id`),
  KEY `summary_index` (`blog_id`,`status_id`,`created_date`),
  KEY `bc_status_user` (`blog_id`,`user_id`,`status_id`),
  KEY `sort_by_update_date` (`last_updated_date`)
) ENGINE=InnoDB AUTO_INCREMENT=48105 DEFAULT CHARSET=latin1;

/*Table structure for table `blogs_base` */

CREATE TABLE `blogs_base` (
  `blog_id` int(11) NOT NULL auto_increment,
  `community_id` int(11) default NULL,
  `subject` varchar(100) NOT NULL default '',
  `body_text` text,
  `created_user_id` int(11) NOT NULL default '0',
  `number_of_comments` int(11) NOT NULL default '0',
  `number_of_views` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) default NULL,
  `ip_address` varchar(30) default NULL,
  `status_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`blog_id`),
  KEY `count_community_null` (`status_id`,`created_user_id`,`community_id`)
) ENGINE=InnoDB AUTO_INCREMENT=179590 DEFAULT CHARSET=latin1;

/*Table structure for table `bulletins` */

CREATE TABLE `bulletins` (
  `bulletin_id` int(11) NOT NULL auto_increment,
  `from_id` int(11) NOT NULL default '0',
  `from_status_id` int(11) NOT NULL default '0',
  `friend_group_id` int(11) default NULL,
  `subject` varchar(100) NOT NULL default '',
  `message` text NOT NULL,
  `message_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `channel_id` int(11) default NULL,
  PRIMARY KEY  (`bulletin_id`),
  KEY `friend_status_mesh` (`from_id`,`from_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=96242 DEFAULT CHARSET=latin1 COMMENT='Used for bulletins';

/*Table structure for table `categories` */

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL auto_increment,
  `asset_type_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  `parent_category_id` int(11) default NULL,
  `sort_order` int(11) default '0',
  PRIMARY KEY  (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1258 DEFAULT CHARSET=latin1;

/*Table structure for table `category_attribute_values` */

CREATE TABLE `category_attribute_values` (
  `category_attribute_value_id` int(11) NOT NULL auto_increment,
  `attribute_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`category_attribute_value_id`),
  KEY `FK_category_attribute_values_category_attributes` (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

/*Table structure for table `category_attributes` */

CREATE TABLE `category_attributes` (
  `attribute_id` int(11) NOT NULL auto_increment,
  `category_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  `type` varchar(11) NOT NULL default '1',
  PRIMARY KEY  (`attribute_id`),
  KEY `FK_category_attributes_categories` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

/*Table structure for table `channel_diggs` */

CREATE TABLE `channel_diggs` (
  `digg_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL default '0',
  `channel_id` int(10) unsigned NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`digg_id`),
  KEY `newindex` (`created_date`),
  KEY `uid_cid` (`user_id`,`channel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14512293 DEFAULT CHARSET=latin1;

/*Table structure for table `channel_stats` */

CREATE TABLE `channel_stats` (
  `channel_id` int(11) NOT NULL auto_increment,
  `number_of_views` int(11) NOT NULL default '0',
  `number_of_members` int(11) NOT NULL default '0',
  `number_of_diggs` int(11) NOT NULL default '0',
  `number_times_shared` int(11) NOT NULL default '0',
  `number_of_pending_members` int(11) NOT NULL default '0',
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `number_posts_7_days` int(10) unsigned NOT NULL default '0' COMMENT 'Posts last 7 days',
  PRIMARY KEY  (`channel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1002396972 DEFAULT CHARSET=latin1;

/*Table structure for table `comments` */

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `asset_id` int(11) NOT NULL default '0',
  `channel_id` int(11) NOT NULL default '0',
  `parent_comment_id` int(11) default '0',
  `reply_to_comment_id` int(11) default '0',
  `comment` text,
  `comment_type` int(11) NOT NULL default '0',
  `ip_address` varchar(50) default NULL,
  `last_updated_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`comment_id`),
  KEY `num_people` (`channel_id`,`status_id`),
  KEY `asset_finder` (`asset_id`,`user_id`,`status_id`),
  KEY `channel_id` (`channel_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `communities` */

CREATE TABLE `communities` (
  `community_id` int(11) NOT NULL auto_increment,
  `category_id` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `url` varchar(50) default NULL,
  `creator_id` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_edit` datetime NOT NULL default '0000-00-00 00:00:00',
  `status_id` int(11) NOT NULL default '0',
  `email` varchar(100) default NULL,
  `percent_royalty_paid` int(11) default NULL,
  `is_public` char(1) NOT NULL default '',
  `is_adult` char(1) default NULL,
  `show_server_uptime` char(1) default NULL,
  `show_server_ip` char(1) default NULL,
  `thumbnail_path` varchar(255) default NULL,
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `thumbnail_medium_path` varchar(255) NOT NULL default '',
  `thumbnail_large_path` varchar(255) NOT NULL default '',
  `thumbnail_xlarge_path` varchar(255) NOT NULL default '',
  `thumbnail_type` varchar(50) default NULL,
  `db_connection_string` varchar(200) default NULL,
  `allow_publishing` tinyint(4) NOT NULL default '0',
  `allow_member_events` tinyint(3) unsigned NOT NULL default '0',
  `is_personal` tinyint(3) unsigned NOT NULL default '0',
  `last_update` datetime NOT NULL default '0000-00-00 00:00:00',
  `template_id` int(10) unsigned NOT NULL default '0',
  `over_21_required` enum('Y','N') default 'N',
  `_created_date` int(10) unsigned NOT NULL default '4291869598',
  `has_thumbnail` enum('Y','N') NOT NULL default 'N',
  `name` varchar(100) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `creator_username` varchar(22) NOT NULL,
  `keywords` text,
  PRIMARY KEY  (`community_id`),
  KEY `index_creatorId` (`creator_id`),
  KEY `index_ispersonal` (`is_personal`),
  KEY `creator_id_is_personal` (`creator_id`,`is_personal`),
  KEY `nns_is_personal` (`name_no_spaces`,`is_personal`),
  KEY `newindex` (`thumbnail_path`,`is_personal`),
  KEY `u` (`url`(20)),
  KEY `_created_date` (`_created_date`),
  KEY `thumb_rec_adult_per` USING BTREE (`is_personal`,`has_thumbnail`,`is_adult`,`_created_date`)
) ENGINE=InnoDB AUTO_INCREMENT=1002396972 DEFAULT CHARSET=latin1;

/*Table structure for table `community_account_types` */

CREATE TABLE `community_account_types` (
  `account_type_id` int(11) NOT NULL default '0',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`account_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `community_categories` */

CREATE TABLE `community_categories` (
  `category_id` int(11) NOT NULL auto_increment,
  `category_name` varchar(100) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `number_of_communities` int(11) NOT NULL default '0',
  PRIMARY KEY  (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Table structure for table `community_member_group_members` */

CREATE TABLE `community_member_group_members` (
  `member_group_id` int(10) unsigned NOT NULL default '0',
  `member_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`member_group_id`,`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `community_member_groups` */

CREATE TABLE `community_member_groups` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `channel_id` int(10) unsigned NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2953 DEFAULT CHARSET=latin1;

/*Table structure for table `community_member_status` */

CREATE TABLE `community_member_status` (
  `status_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `community_member_subscriptions` */

CREATE TABLE `community_member_subscriptions` (
  `community_member_subscription_id` int(11) NOT NULL auto_increment,
  `community_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `community_subscription_id` int(11) NOT NULL default '0',
  `start_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `end_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `gross_point_amount` float NOT NULL default '0',
  `purchase_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `status_id` int(11) NOT NULL default '0',
  `refund_amount` float default NULL,
  PRIMARY KEY  (`community_member_subscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `community_member_subscriptions_status` */

CREATE TABLE `community_member_subscriptions_status` (
  `status_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `community_members` */

CREATE TABLE `community_members` (
  `community_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `account_type_id` int(11) NOT NULL default '0',
  `added_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `status_id` int(10) unsigned NOT NULL default '0',
  `newsletter` char(1) NOT NULL default '',
  `invited_by_user_id` int(11) default NULL,
  `allow_asset_uploads` char(1) default NULL,
  `allow_forum_use` char(1) default NULL,
  `keywords` varchar(100) default NULL,
  `notifications` int(11) NOT NULL default '0',
  `item_notify` int(11) NOT NULL default '0',
  `item_review_notify` int(11) NOT NULL default '0',
  `blog_notify` int(11) NOT NULL default '0',
  `blog_comment_notify` int(11) NOT NULL default '0',
  `post_notify` int(11) NOT NULL default '0',
  `reply_notify` int(11) NOT NULL default '0',
  `has_edit_rights` enum('Y','N') NOT NULL default 'N',
  `active_member` enum('Y','N') NOT NULL default 'Y',
  PRIMARY KEY  (`community_id`,`user_id`),
  KEY `user_stats` (`user_id`,`status_id`),
  KEY `has_edit_rights` (`has_edit_rights`),
  KEY `active_member` (`active_member`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `community_shares` */

CREATE TABLE `community_shares` (
  `share_id` int(11) unsigned NOT NULL auto_increment,
  `user_id` int(11) unsigned NOT NULL default '0',
  `sent_user_id` int(11) unsigned NOT NULL default '0',
  `community_id` int(11) unsigned NOT NULL default '0',
  `email` text,
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`share_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67723 DEFAULT CHARSET=latin1;

/*Table structure for table `community_status` */

CREATE TABLE `community_status` (
  `status_id` int(11) NOT NULL default '0',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `community_subscriptions` */

CREATE TABLE `community_subscriptions` (
  `community_subscription_id` int(11) NOT NULL auto_increment,
  `community_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `subscription_amount` decimal(12,2) NOT NULL default '0.00',
  `subscription_kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `length_of_subscription` int(11) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  `updated_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`community_subscription_id`),
  KEY `comid_state` (`community_id`,`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

/*Table structure for table `community_views` */

CREATE TABLE `community_views` (
  `view_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL default '0',
  `community_id` int(10) unsigned NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `anonymous` tinyint(3) unsigned NOT NULL default '0',
  `ip_address` int(11) unsigned default NULL,
  PRIMARY KEY  (`view_id`),
  KEY `cv_hub` (`community_id`),
  KEY `cid_anon_u` (`community_id`,`anonymous`,`user_id`),
  KEY `cid_anon_user_id` (`community_id`,`anonymous`,`user_id`),
  KEY `cv_idx_1` USING BTREE (`created_date`,`community_id`,`ip_address`)
) ENGINE=InnoDB AUTO_INCREMENT=66273689 DEFAULT CHARSET=latin1;

/*Table structure for table `configuration` */

CREATE TABLE `configuration` (
  `auto_debit_minium_amount` int(11) NOT NULL default '0',
  `threads_per_page` int(11) NOT NULL default '0',
  `topics_per_page` int(11) NOT NULL default '0',
  `max_number_login_attempts` int(11) NOT NULL default '0',
  `torrent_username` varchar(32) default NULL,
  `torrent_password` varchar(32) default NULL,
  `last_notifications_sent` datetime default NULL,
  `last_daily_notifications_sent` datetime default NULL,
  `file_upload_timeout_hours` int(11) NOT NULL default '336',
  `max_upload_filesize` bigint(20) unsigned NOT NULL default '10',
  `last_ranking_updated` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_weekly_notifications_sent` datetime default NULL,
  `last_daily_webstat_email_sent` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `contest_asset_votes` */

CREATE TABLE `contest_asset_votes` (
  `asset_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vote_date` datetime NOT NULL,
  `yes` tinyint(4) NOT NULL default '1',
  `ip_address` bigint(20) unsigned NOT NULL default '0',
  `valid` enum('Y','N') NOT NULL default 'Y',
  PRIMARY KEY  (`asset_id`,`contest_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `contest_assets` */

CREATE TABLE `contest_assets` (
  `asset_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `number_of_votes` int(11) NOT NULL,
  `phone_number` varchar(20) default NULL,
  PRIMARY KEY  (`asset_id`,`contest_id`),
  KEY `aid` (`asset_id`),
  KEY `cid` (`contest_id`),
  KEY `ad` (`added_date`),
  KEY `nov` (`number_of_votes`),
  KEY `caid` (`asset_id`,`contest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `contests` */

CREATE TABLE `contests` (
  `contest_id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `terms` text,
  `upload_start_date` datetime NOT NULL,
  `upload_end_date` datetime NOT NULL,
  `vote_start_date` datetime NOT NULL,
  `vote_end_date` datetime NOT NULL,
  `channel_id` int(11) NOT NULL,
  `contest_picture` varchar(100) default NULL,
  `vote_yes_image` varchar(100) default NULL,
  `vote_no_image` varchar(100) default NULL,
  PRIMARY KEY  (`contest_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `countries` */

CREATE TABLE `countries` (
  `country_id` char(2) NOT NULL default '',
  `country` varchar(100) NOT NULL default '',
  `number_of_users` int(10) unsigned NOT NULL default '0' COMMENT 'Number of users in this country',
  PRIMARY KEY  (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `countries_7_days` */

CREATE TABLE `countries_7_days` (
  `log_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL,
  `country_id` varchar(2) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY  USING BTREE (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `crew_peeps` */

CREATE TABLE `crew_peeps` (
  `crew_id` int(11) NOT NULL auto_increment COMMENT 'FK to crew',
  `user_id` int(11) NOT NULL default '0' COMMENT 'Peep in crew',
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00' COMMENT 'Date created',
  PRIMARY KEY  (`crew_id`,`user_id`),
  CONSTRAINT `FK_crew_friends_1` FOREIGN KEY (`crew_id`) REFERENCES `crews` (`crew_id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1 COMMENT='Peeps in your crew';

/*Table structure for table `crews` */

CREATE TABLE `crews` (
  `crew_id` int(11) NOT NULL auto_increment COMMENT 'PK',
  `user_id` int(11) NOT NULL default '0' COMMENT 'The users crew',
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00' COMMENT 'Date created',
  `crew_count` int(10) unsigned NOT NULL default '0' COMMENT 'Number of peeps in crew',
  PRIMARY KEY  (`crew_id`),
  UNIQUE KEY `uid` USING BTREE (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1 COMMENT='Crew info';

/*Table structure for table `csr_logs` */

CREATE TABLE `csr_logs` (
  `csr_log_id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL default '0',
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `activity` text NOT NULL,
  `asset_id` int(11) default NULL,
  `updated_user_id` int(11) default NULL,
  PRIMARY KEY  (`csr_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=236380 DEFAULT CHARSET=latin1;

/*Table structure for table `csr_logs_arch` */

CREATE TABLE `csr_logs_arch` (
  `csr_log_id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL default '0',
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `activity` text NOT NULL,
  `asset_id` int(11) default NULL,
  `updated_user_id` int(11) default NULL,
  PRIMARY KEY  (`csr_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13625 DEFAULT CHARSET=latin1;

/*Table structure for table `current_game_engines` */

CREATE TABLE `current_game_engines` (
  `engine_id` int(11) NOT NULL auto_increment,
  `server_name` varchar(100) default NULL,
  `game_asset_id` int(11) default NULL,
  `game_name` varchar(100) default NULL,
  `registration_key` varchar(50) default NULL,
  `type` int(11) NOT NULL default '0',
  `internally_hosted` char(1) NOT NULL default '',
  `engine_started_date` datetime default NULL,
  `ip_address` varchar(50) NOT NULL default '',
  `port` int(11) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  `number_of_players` int(11) default NULL,
  `max_players` int(11) default NULL,
  `player_statistic_type` int(11) default NULL,
  `last_ping_datetime` datetime default NULL,
  `last_ping_value` int(11) default NULL,
  PRIMARY KEY  (`engine_id`),
  KEY `fast_summary` (`game_asset_id`,`status_id`,`type`,`last_ping_datetime`)
) ENGINE=InnoDB AUTO_INCREMENT=376 DEFAULT CHARSET=latin1;

/*Table structure for table `data_copy_history` */

CREATE TABLE `data_copy_history` (
  `copy_history_id` int(10) unsigned NOT NULL auto_increment COMMENT 'auto number used for portential reporting',
  `table_name` varchar(100) NOT NULL default '' COMMENT 'database and table name whos data is being copied',
  `projected_id_copied` int(10) unsigned NOT NULL default '0' COMMENT 'holds what will be the highest id to be copied over for new inserts',
  `projected_modified_date` datetime NOT NULL default '0000-00-00 00:00:00' COMMENT 'holds the most recent date for data that will be copied - used for update copies as well as just notation',
  `last_id_copied` int(10) unsigned NOT NULL default '0' COMMENT 'holds what the last id that was actually copied over',
  `last_modified_date` datetime NOT NULL default '0000-00-00 00:00:00' COMMENT 'holds what last date that was actually copied over ',
  PRIMARY KEY  (`copy_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='keeps data copying record - MYSQL to SQL Server';

/*Table structure for table `db_health` */

CREATE TABLE `db_health` (
  `dt_taken` datetime NOT NULL,
  `cpu_load` float NOT NULL default '0',
  `io_load` float NOT NULL default '0',
  `overall_health` float NOT NULL default '0',
  `slow_queries` int(10) unsigned NOT NULL default '0',
  `queries` bigint(20) unsigned NOT NULL default '0',
  `server` varchar(10) NOT NULL,
  PRIMARY KEY  USING BTREE (`dt_taken`,`server`),
  KEY `server` (`server`),
  KEY `dt_taken` (`dt_taken`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `elite_source_watermarks` */

CREATE TABLE `elite_source_watermarks` (
  `user_id` int(11) NOT NULL default '0',
  `elite_name` varchar(100) NOT NULL default '',
  `watermark` varchar(50) NOT NULL default '',
  `comment` text,
  `revoked` tinyint(4) NOT NULL default '0',
  `revoked_date` datetime default NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `engine_history` */

CREATE TABLE `engine_history` (
  `engine_history_id` int(11) NOT NULL auto_increment,
  `engine_id` int(11) NOT NULL default '0',
  `player_count` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `game_key` varchar(100) default NULL,
  `ip_address` varchar(20) default NULL,
  PRIMARY KEY  (`engine_history_id`),
  KEY `FK_engine_history_current_game_engines` (`engine_id`)
) ENGINE=InnoDB AUTO_INCREMENT=160845 DEFAULT CHARSET=latin1;

/*Table structure for table `engine_status` */

CREATE TABLE `engine_status` (
  `status_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `engine_type` */

CREATE TABLE `engine_type` (
  `engine_type_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`engine_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `events` */

CREATE TABLE `events` (
  `event_id` int(11) NOT NULL auto_increment,
  `community_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `title` varchar(100) NOT NULL default '',
  `details` text NOT NULL,
  `location` text NOT NULL,
  `start_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `end_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `type_id` int(10) unsigned NOT NULL default '0',
  `time_zone_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`event_id`),
  KEY `communit_event_begin` (`community_id`,`start_time`)
) ENGINE=InnoDB AUTO_INCREMENT=9144 DEFAULT CHARSET=latin1;

/*Table structure for table `featured_assets` */

CREATE TABLE `featured_assets` (
  `featured_asset_id` int(11) NOT NULL auto_increment,
  `asset_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `start_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `end_datetime` datetime default NULL,
  `shown_count` int(11) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  `playlist_type` int(11) NOT NULL default '0',
  PRIMARY KEY  (`featured_asset_id`),
  KEY `date_range` (`status_id`,`start_datetime`,`end_datetime`)
) ENGINE=InnoDB AUTO_INCREMENT=842 DEFAULT CHARSET=latin1;

/*Table structure for table `feedback` */

CREATE TABLE `feedback` (
  `feedback_id` int(11) NOT NULL auto_increment,
  `subject` varchar(255) NOT NULL default '',
  `summary` text NOT NULL,
  `IP` varchar(50) NOT NULL default '',
  `file_link` varchar(200) default NULL,
  `type` tinyint(4) NOT NULL default '0',
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=99233 DEFAULT CHARSET=latin1;

/*Table structure for table `first_time_transactions` */

CREATE TABLE `first_time_transactions` (
  `user_id` int(10) unsigned NOT NULL,
  `dt_created` datetime NOT NULL,
  PRIMARY KEY  (`user_id`),
  KEY `uid` (`user_id`),
  KEY `dtc` (`dt_created`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `forum_categories` */

CREATE TABLE `forum_categories` (
  `forum_category_id` int(11) NOT NULL auto_increment,
  `community_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  `display_order` int(11) NOT NULL default '0',
  PRIMARY KEY  (`forum_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=392079 DEFAULT CHARSET=latin1;

/*Table structure for table `forum_post_views` */

CREATE TABLE `forum_post_views` (
  `user_id` int(11) unsigned NOT NULL default '0' COMMENT 'User that visited forum post',
  `number_of_views` int(11) unsigned NOT NULL default '0' COMMENT 'Number of posts viewed',
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`user_id`,`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `forum_status` */

CREATE TABLE `forum_status` (
  `status_id` int(11) NOT NULL default '0',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `forum_visits` */

CREATE TABLE `forum_visits` (
  `user_id` int(11) unsigned NOT NULL default '0' COMMENT 'User that visited forum',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`user_id`,`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `forums` */

CREATE TABLE `forums` (
  `forum_id` int(11) NOT NULL auto_increment,
  `forum_category_id` int(11) default NULL,
  `community_id` int(11) NOT NULL default '0',
  `forum_name` varchar(100) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `number_of_topics` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_post_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_topic_id` int(11) unsigned NOT NULL default '0',
  `last_user_id` int(11) unsigned NOT NULL default '0',
  `display_order` int(11) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`forum_id`),
  KEY `luid` (`last_user_id`),
  KEY `ltid` (`last_topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=395613 DEFAULT CHARSET=latin1;

/*Table structure for table `friend_group_friends` */

CREATE TABLE `friend_group_friends` (
  `friend_group_id` int(11) NOT NULL default '0',
  `friend_id` int(11) NOT NULL default '0',
  `owner_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`friend_group_id`,`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `friend_groups` */

CREATE TABLE `friend_groups` (
  `friend_group_id` int(11) NOT NULL auto_increment,
  `owner_id` int(11) NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `friend_count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`friend_group_id`),
  KEY `oid` (`owner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12887816 DEFAULT CHARSET=latin1;

/*Table structure for table `friend_requests` */

CREATE TABLE `friend_requests` (
  `user_id` int(11) NOT NULL default '0',
  `friend_id` int(11) NOT NULL default '0',
  `request_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`user_id`,`friend_id`),
  KEY `user_frined` (`user_id`,`friend_id`),
  KEY `u_glue` (`user_id`,`request_date`),
  KEY `u_glue_stat` (`user_id`,`request_date`),
  KEY `fid` (`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `friend_status` */

CREATE TABLE `friend_status` (
  `friend_status_id` int(11) NOT NULL default '0',
  `name` varchar(20) NOT NULL default '',
  `descirption` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`friend_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `friends` */

CREATE TABLE `friends` (
  `user_id` int(11) NOT NULL default '0',
  `friend_id` int(11) NOT NULL default '0',
  `glued_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`user_id`,`friend_id`),
  KEY `u_glue` (`user_id`,`glued_date`),
  KEY `u_glue_stat` USING BTREE (`user_id`,`glued_date`),
  KEY `fid` (`friend_id`),
  KEY `uid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `friends_declined` */

CREATE TABLE `friends_declined` (
  `user_id` int(10) unsigned NOT NULL auto_increment,
  `friend_id` int(10) unsigned NOT NULL,
  `date_declined` datetime NOT NULL,
  PRIMARY KEY  (`user_id`,`friend_id`,`date_declined`)
) ENGINE=InnoDB AUTO_INCREMENT=4342872 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `friends_deleted` */

CREATE TABLE `friends_deleted` (
  `user_id` int(10) unsigned NOT NULL,
  `friend_id` int(10) unsigned NOT NULL,
  `date_deleted` datetime NOT NULL,
  PRIMARY KEY  (`user_id`,`friend_id`,`date_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `game_license` */

CREATE TABLE `game_license` (
  `game_license_id` int(11) NOT NULL auto_increment,
  `asset_id` int(11) NOT NULL default '0',
  `license_type` int(11) default NULL,
  `game_key` varchar(100) NOT NULL default '',
  `patch_url` varchar(255) default NULL,
  `patch_url_v2` varchar(255) NOT NULL,
  `server_name` varchar(50) default NULL,
  `start_date` datetime default NULL,
  `end_date` datetime default NULL,
  `purchase_date` datetime default NULL,
  `user_cancelled` char(1) NOT NULL default 'N',
  `status_id` int(11) NOT NULL default '0',
  `publish_status_id` int(11) NOT NULL default '0',
  `IT_game_server` varchar(50) default NULL,
  `IT_game_directory` varchar(255) default NULL,
  `IT_game_temp_directory` varchar(255) default NULL,
  `IT_game_binaries_directory` varchar(255) default NULL,
  `admin_url` varchar(255) default NULL,
  `server_version` varchar(10) default NULL,
  `patch_url_v3` varchar(255) NOT NULL,
  PRIMARY KEY  (`game_license_id`),
  KEY `INDEX_asset_id` (`asset_id`)
) ENGINE=InnoDB AUTO_INCREMENT=449 DEFAULT CHARSET=latin1;

/*Table structure for table `game_license_status` */

CREATE TABLE `game_license_status` (
  `status_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `game_status` */

CREATE TABLE `game_status` (
  `status_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `game_upload` */

CREATE TABLE `game_upload` (
  `game_upload_id` int(11) NOT NULL auto_increment,
  `asset_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `path` varchar(255) NOT NULL default '',
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `completed_datetime` datetime default NULL,
  `publish_status_id` int(11) NOT NULL default '0',
  `ip_address` varchar(30) default NULL,
  PRIMARY KEY  (`game_upload_id`),
  KEY `p_status` (`publish_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=latin1;

/*Table structure for table `genres` */

CREATE TABLE `genres` (
  `genre_id` int(11) NOT NULL default '0',
  `asset_type_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `gift_cards` */

CREATE TABLE `gift_cards` (
  `gift_card_id` int(11) NOT NULL auto_increment,
  `dollar_amount` decimal(12,2) NOT NULL default '0.00',
  `upc` varchar(50) NOT NULL,
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `kei_point_amount` decimal(12,2) NOT NULL default '0.00',
  `description` varchar(256) NOT NULL default '',
  PRIMARY KEY  (`gift_card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `group_categories` */

CREATE TABLE `group_categories` (
  `group_category_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `channel_id` int(11) NOT NULL default '0',
  `description` varchar(256) NOT NULL default '',
  PRIMARY KEY  (`group_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `group_category_asset_groups` */

CREATE TABLE `group_category_asset_groups` (
  `group_category_id` int(11) NOT NULL default '0',
  `asset_group_id` int(11) NOT NULL default '0',
  `sort_order` int(11) NOT NULL default '0',
  PRIMARY KEY  (`group_category_id`,`asset_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `hotnew_stuff_time_periods` */

CREATE TABLE `hotnew_stuff_time_periods` (
  `time_period_id` smallint(5) unsigned NOT NULL,
  `period_in_days` smallint(5) unsigned NOT NULL default '0',
  `description` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`time_period_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `interest_categories` */

CREATE TABLE `interest_categories` (
  `ic_id` int(10) unsigned NOT NULL auto_increment,
  `category_text` varchar(45) NOT NULL,
  PRIMARY KEY  (`ic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Table structure for table `interests` */

CREATE TABLE `interests` (
  `interest_id` int(10) unsigned NOT NULL auto_increment,
  `ic_id` int(10) unsigned NOT NULL,
  `interest` varchar(255) NOT NULL,
  `user_count` int(10) unsigned NOT NULL default '0' COMMENT 'Number of users with interest',
  PRIMARY KEY  (`interest_id`),
  KEY `iic` (`interest_id`,`ic_id`),
  KEY `ic_id` (`ic_id`),
  KEY `interest` (`interest`(10))
) ENGINE=InnoDB AUTO_INCREMENT=149018 DEFAULT CHARSET=latin1;

/*Table structure for table `invite_ip_addresses` */

CREATE TABLE `invite_ip_addresses` (
  `ip_address` bigint(20) unsigned NOT NULL,
  `number_of_times` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Holds IP Addresses and how many times it was used for invite';

/*Table structure for table `invite_point_awards` */

CREATE TABLE `invite_point_awards` (
  `invite_point_id` int(10) NOT NULL auto_increment,
  `point_award_amount` float NOT NULL default '0',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `end_date` datetime default NULL,
  `ip_limit` smallint(5) unsigned NOT NULL default '5',
  `award_multiple` smallint(5) unsigned NOT NULL default '5',
  `description` varchar(100) NOT NULL,
  PRIMARY KEY  (`invite_point_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `invite_status` */

CREATE TABLE `invite_status` (
  `invite_status_id` int(11) NOT NULL auto_increment,
  `name` varchar(20) NOT NULL default '',
  `description` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`invite_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `invites` */

CREATE TABLE `invites` (
  `invite_id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL default '0',
  `email` varchar(200) NOT NULL default '',
  `key_value` varchar(20) NOT NULL default '',
  `invite_status_id` int(11) NOT NULL default '0',
  `invited_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `points_awarded` float default NULL,
  `kei_point_id_awarded` varchar(50) default NULL,
  `reinvite_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `channel_id` int(10) unsigned default NULL,
  `invited_user_id` int(11) default NULL,
  `attempts` int(10) unsigned NOT NULL default '1',
  PRIMARY KEY  (`invite_id`)
) ENGINE=InnoDB AUTO_INCREMENT=423630 DEFAULT CHARSET=latin1;

/*Table structure for table `ip_bans` */

CREATE TABLE `ip_bans` (
  `ip_address` int(10) unsigned NOT NULL,
  `ban_start_date` datetime NOT NULL,
  `ban_end_date` datetime NOT NULL,
  PRIMARY KEY  (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

/*Table structure for table `join_communities` */

CREATE TABLE `join_communities` (
  `join_id` int(11) NOT NULL auto_increment,
  `source_community_id` int(11) NOT NULL,
  `join_community_id` int(11) NOT NULL,
  PRIMARY KEY  (`join_id`),
  KEY `scid` (`source_community_id`),
  KEY `jcid` (`join_community_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `join_themes` */

CREATE TABLE `join_themes` (
  `join_id` int(11) NOT NULL auto_increment,
  `community_id` int(11) NOT NULL default '0',
  `css` text,
  `page_join_copy` text,
  `page_verify_copy` text,
  `page_complete_copy` text,
  `user_profile_theme_male_id` int(11) NOT NULL default '0',
  `user_profile_theme_female_id` int(11) NOT NULL default '0',
  `user_profile_video_id` int(11) NOT NULL default '0',
  `user_profile_male_avatar_path` varchar(255) default NULL,
  `user_profile_female_avatar_path` varchar(255) default NULL,
  `email_body_top` varchar(255) default NULL,
  `email_body_bottom` varchar(255) default NULL,
  `email_subject` varchar(255) default NULL,
  PRIMARY KEY  (`join_id`),
  KEY `cid` (`community_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `kei_points` */

CREATE TABLE `kei_points` (
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `conversion_rate_to_dollar` float NOT NULL default '0',
  `description` varchar(100) NOT NULL default '',
  `currency_symbol` varchar(50) NOT NULL default '',
  `user_purchaseable` char(1) NOT NULL default '',
  `display_format` varchar(100) default NULL,
  PRIMARY KEY  (`kei_point_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `layout_background_repeat_modes` */

CREATE TABLE `layout_background_repeat_modes` (
  `mode_id` int(11) NOT NULL auto_increment,
  `mode` varchar(20) NOT NULL default '',
  `html` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`mode_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Possible repeat modes for a background';

/*Table structure for table `layout_fonts` */

CREATE TABLE `layout_fonts` (
  `font_id` int(11) NOT NULL auto_increment,
  `name` varchar(40) NOT NULL default '',
  `pixel_sizes_csv` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`font_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_billboard` */

CREATE TABLE `layout_module_billboard` (
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  `title` text NOT NULL,
  `header` text NOT NULL,
  `footer` text NOT NULL,
  `show_mature` tinyint(1) unsigned NOT NULL default '0',
  `results_per_page` tinyint(4) unsigned NOT NULL default '20',
  `results_to_return` tinyint(4) unsigned NOT NULL default '10',
  `info_placement` tinyint(1) unsigned NOT NULL default '0',
  `image_size` tinyint(4) unsigned NOT NULL default '2',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=710 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_blogs` */

CREATE TABLE `layout_module_blogs` (
  `title` text,
  `entries_per_page` int(11) NOT NULL default '20',
  `show_dates` tinyint(4) NOT NULL default '0',
  `module_page_id` int(11) NOT NULL auto_increment,
  `tags` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4750728 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_channel_control_panel` */

CREATE TABLE `layout_module_channel_control_panel` (
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  `title` text NOT NULL,
  `show_menu` tinyint(4) unsigned NOT NULL default '1',
  `show_join_channel` tinyint(4) unsigned NOT NULL default '1',
  `show_quit_channel` tinyint(4) unsigned NOT NULL default '1',
  `show_send_message` tinyint(4) unsigned NOT NULL default '1',
  `show_rave_it` tinyint(4) unsigned NOT NULL default '1',
  `show_report_abuse` tinyint(4) unsigned NOT NULL default '1',
  `show_tell_it` tinyint(4) unsigned NOT NULL default '1',
  `show_submit_media` tinyint(4) unsigned NOT NULL default '1',
  `banner_path` varchar(255) default NULL,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=402067 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_channel_description` */

CREATE TABLE `layout_module_channel_description` (
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  `title` text NOT NULL,
  `num_moderators` int(10) unsigned NOT NULL default '0',
  `show_moderators` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=389614 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_channel_members` */

CREATE TABLE `layout_module_channel_members` (
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  `entries_per_page` int(10) unsigned NOT NULL default '0',
  `title` text NOT NULL,
  `show_join_date` tinyint(3) unsigned NOT NULL default '0',
  `show_role` tinyint(3) unsigned NOT NULL default '0',
  `show_digs` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=390922 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_channel_members_map` */

CREATE TABLE `layout_module_channel_members_map` (
  `module_page_id` int(11) NOT NULL auto_increment,
  `title` text NOT NULL,
  `number_of_top_members` int(11) NOT NULL default '0',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_channel_owner` */

CREATE TABLE `layout_module_channel_owner` (
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_channel_top_contributors` */

CREATE TABLE `layout_module_channel_top_contributors` (
  `module_page_id` int(11) NOT NULL auto_increment,
  `title` text NOT NULL,
  `number_of_top_contributors` int(11) NOT NULL default '0',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_channels` */

CREATE TABLE `layout_module_channels` (
  `entries_per_page` int(11) NOT NULL default '20',
  `module_page_id` int(11) NOT NULL auto_increment,
  `title` text,
  `filter` smallint(5) unsigned NOT NULL default '7',
  PRIMARY KEY  (`module_page_id`),
  KEY `FK_module_id` (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4345459 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_comments` */

CREATE TABLE `layout_module_comments` (
  `max_comments` int(11) NOT NULL default '5',
  `show_datetime` tinyint(4) NOT NULL default '1',
  `show_profile_pic` tinyint(4) NOT NULL default '1',
  `author_alignment` char(1) NOT NULL default 'L',
  `permission_friends` int(11) NOT NULL default '1',
  `permission_everyone` int(11) NOT NULL default '1',
  `module_page_id` int(11) NOT NULL auto_increment,
  `title` text,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4382021 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_contest_get_raved` */

CREATE TABLE `layout_module_contest_get_raved` (
  `module_page_id` int(11) NOT NULL auto_increment,
  `title` text,
  `contest_id` int(11) NOT NULL,
  `intro_text` text,
  `contest_url` varchar(200) NOT NULL,
  `banner_image` varchar(200) NOT NULL,
  `banner_image_not_logged_in` varchar(200) default NULL,
  `banner_text` varchar(100) NOT NULL,
  `login_message` varchar(100) NOT NULL,
  `no_assets_message` varchar(100) NOT NULL,
  `no_assets_html` text,
  `have_assets_html` text,
  `not_loggedin_html` text,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_contest_profile_header` */

CREATE TABLE `layout_module_contest_profile_header` (
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  `show_menu` tinyint(4) unsigned NOT NULL default '1',
  `banner` longblob,
  `text` text NOT NULL,
  `show_join_channel` tinyint(4) unsigned NOT NULL default '1',
  `show_quit_channel` tinyint(4) unsigned NOT NULL default '1',
  `show_send_message` tinyint(4) unsigned NOT NULL default '1',
  `show_rave_it` tinyint(4) unsigned NOT NULL default '1',
  `show_report_abuse` tinyint(4) unsigned NOT NULL default '1',
  `show_tell_it` tinyint(4) unsigned NOT NULL default '1',
  `show_submit_media` tinyint(4) unsigned NOT NULL default '1',
  `banner_path` varchar(255) default NULL,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6785 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_contest_submissions` */

CREATE TABLE `layout_module_contest_submissions` (
  `module_page_id` int(11) NOT NULL auto_increment,
  `title` text,
  `contest_id` int(11) NOT NULL,
  `assets_per_page` int(11) NOT NULL default '12',
  `intro_text_title` varchar(100) default NULL,
  `intro_text` text,
  `cloud_title` varchar(100) NOT NULL,
  `hide_cloud` tinyint(4) NOT NULL default '0',
  `assets_per_row` int(10) unsigned NOT NULL default '1',
  `info_placement` smallint(5) unsigned NOT NULL default '1',
  `show_Mature` smallint(5) unsigned NOT NULL default '1',
  `max_cloud_length` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_contest_topvotes` */

CREATE TABLE `layout_module_contest_topvotes` (
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  `contest_id` int(10) unsigned NOT NULL default '0',
  `title` text NOT NULL,
  `intro_text` text NOT NULL,
  `num_results_to_return` int(10) unsigned NOT NULL default '0',
  `info_placement` smallint(5) unsigned NOT NULL default '1',
  `show_Mature` smallint(5) unsigned NOT NULL default '1',
  `footer` text NOT NULL,
  `rankby_id` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_contest_upload` */

CREATE TABLE `layout_module_contest_upload` (
  `module_page_id` int(11) NOT NULL auto_increment,
  `title` text,
  `description_text` text,
  `terms_text` text,
  `results_url` text,
  `body_text` text,
  `media_upload_label` text,
  `artist_name_label` text NOT NULL,
  `media_title_label` text,
  `keyword_label` text,
  `photo_upload_label` text,
  `allow_video` tinyint(3) default '0',
  `allow_photos` tinyint(3) default '0',
  `allow_music` tinyint(3) default '0',
  `allow_games` tinyint(3) default '0',
  `contest_id` int(11) NOT NULL default '0',
  `not_logged_in_HTML` text,
  `submissions_end_HTML` text,
  `voting_ended_HTML` text,
  `show_media_description` tinyint(3) unsigned NOT NULL default '1',
  `media_description_label` text,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_control_panel` */

CREATE TABLE `layout_module_control_panel` (
  `module_page_id` int(11) NOT NULL auto_increment,
  `title` text,
  `show_gender` tinyint(3) unsigned default NULL,
  `show_location` tinyint(3) unsigned default NULL,
  `show_age` tinyint(3) unsigned default NULL,
  PRIMARY KEY  (`module_page_id`),
  KEY `FK_module_id` (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4365053 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_counter` */

CREATE TABLE `layout_module_counter` (
  `font_color` varchar(10) NOT NULL default '',
  `background_color` varchar(10) NOT NULL default '',
  `font_size` int(11) NOT NULL default '0',
  `title` varchar(255) default NULL,
  `font_style` varchar(10) NOT NULL default '',
  `module_page_id` int(11) NOT NULL auto_increment,
  `description` varchar(255) default NULL,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=406240 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_events` */

CREATE TABLE `layout_module_events` (
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  `show_add_event` tinyint(3) unsigned NOT NULL default '0',
  `title` text NOT NULL,
  `entries_per_page` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=388238 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_forum` */

CREATE TABLE `layout_module_forum` (
  `title` text NOT NULL,
  `entries_per_page` int(10) unsigned NOT NULL default '0',
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=390550 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_friends` */

CREATE TABLE `layout_module_friends` (
  `title` text,
  `people_per_page` int(11) NOT NULL default '10',
  `module_page_id` int(11) NOT NULL auto_increment,
  `friend_group_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4366149 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_groups` */

CREATE TABLE `layout_module_groups` (
  `module_group_id` int(11) NOT NULL auto_increment,
  `title` varchar(50) NOT NULL default '',
  `expanded` tinyint(4) NOT NULL default '0',
  `sequence` int(11) NOT NULL default '0',
  `visible` tinyint(3) unsigned NOT NULL default '1',
  PRIMARY KEY  (`module_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_hotnew_stuff` */

CREATE TABLE `layout_module_hotnew_stuff` (
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  `title` text NOT NULL,
  `header` text NOT NULL,
  `footer` text NOT NULL,
  `show_mature` tinyint(1) unsigned NOT NULL default '0',
  `time_period_id` tinyint(4) unsigned NOT NULL default '10',
  `media_per_page` tinyint(4) unsigned NOT NULL default '20',
  `media_per_row` tinyint(4) unsigned NOT NULL default '0',
  `media_location` tinyint(1) unsigned NOT NULL default '0',
  `image_size` tinyint(4) unsigned NOT NULL default '2',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=652 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_html` */

CREATE TABLE `layout_module_html` (
  `module_page_id` int(11) NOT NULL auto_increment,
  `body_text` text,
  `title_text` text,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=213267 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_interests` */

CREATE TABLE `layout_module_interests` (
  `title` text,
  `module_page_id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=MyISAM AUTO_INCREMENT=775589 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_leader_board` */

CREATE TABLE `layout_module_leader_board` (
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  `title` text NOT NULL,
  `header` text NOT NULL,
  `description` text NOT NULL,
  `leader_config_id` int(10) unsigned NOT NULL default '0',
  `number_of_results` int(10) unsigned NOT NULL default '10',
  `footer` text NOT NULL,
  `show_pimp_profile` tinyint(1) unsigned NOT NULL default '0',
  `show_pimp_page` tinyint(1) unsigned NOT NULL default '0',
  `show_channel_members` tinyint(1) unsigned NOT NULL default '0',
  `show_friend_accepts` tinyint(1) unsigned NOT NULL default '0',
  `show_raves_your_profile` tinyint(1) unsigned NOT NULL default '0',
  `show_raves_channel` tinyint(1) unsigned NOT NULL default '0',
  `show_join_wok` tinyint(1) unsigned NOT NULL default '0',
  `show_channels_created` tinyint(1) unsigned NOT NULL default '0',
  `show_raves_media` tinyint(1) unsigned NOT NULL default '0',
  `show_photos_uploaded` tinyint(1) unsigned NOT NULL default '0',
  `show_video_uploaded` tinyint(1) unsigned NOT NULL default '0',
  `show_music_uploaded` tinyint(1) unsigned NOT NULL default '0',
  `show_people_invited` tinyint(1) unsigned NOT NULL default '0',
  `show_user_image` tinyint(1) unsigned NOT NULL default '0',
  `time_period_id` smallint(5) unsigned NOT NULL default '1',
  `show_user_name` tinyint(1) unsigned NOT NULL default '0',
  `show_user_points` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_map` */

CREATE TABLE `layout_module_map` (
  `from_profile` tinyint(4) NOT NULL default '1',
  `zip` varchar(25) default '',
  `country_id` int(11) default '0',
  `city` varchar(100) default '',
  `module_page_id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_menu` */

CREATE TABLE `layout_module_menu` (
  `module_page_id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45429 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_mixed_media` */

CREATE TABLE `layout_module_mixed_media` (
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  `text` text NOT NULL,
  `view_type_id` int(10) unsigned NOT NULL default '0',
  `thumbnail_size` int(10) unsigned NOT NULL default '0',
  `num_showing` int(10) unsigned default NULL,
  `num_rows` int(10) unsigned default NULL,
  `num_columns` int(10) unsigned default NULL,
  `show_name` tinyint(3) unsigned NOT NULL default '0',
  `show_description` tinyint(3) unsigned NOT NULL default '0',
  `show_diggs` tinyint(3) unsigned NOT NULL default '0',
  `show_num_views` tinyint(3) unsigned NOT NULL default '0',
  `show_num_shares` tinyint(3) unsigned NOT NULL default '0',
  `show_price` tinyint(3) unsigned NOT NULL default '0',
  `show_type` tinyint(3) unsigned NOT NULL default '0',
  `asset_group_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35365 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_most_viewed` */

CREATE TABLE `layout_module_most_viewed` (
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  `title` text NOT NULL,
  `header` text NOT NULL,
  `footer` text NOT NULL,
  `show_mature` tinyint(1) unsigned NOT NULL default '0',
  `time_period_id` tinyint(4) unsigned NOT NULL default '10',
  `media_per_page` tinyint(4) unsigned NOT NULL default '20',
  `media_per_row` tinyint(4) unsigned NOT NULL default '0',
  `media_location` tinyint(1) unsigned NOT NULL default '0',
  `image_size` tinyint(4) unsigned NOT NULL default '2',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=515 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_multiple_pictures` */

CREATE TABLE `layout_module_multiple_pictures` (
  `module_page_id` int(11) NOT NULL auto_increment,
  `title` text,
  `pictures_per_page` int(11) NOT NULL default '10',
  `show_image_title` tinyint(3) unsigned NOT NULL default '1',
  `group_id` int(11) default NULL,
  `size` smallint(5) unsigned default NULL,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4359801 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_my_picture` */

CREATE TABLE `layout_module_my_picture` (
  `module_page_id` int(11) NOT NULL auto_increment,
  `title` text,
  `show_gender` tinyint(3) unsigned default NULL,
  `show_location` tinyint(3) unsigned default NULL,
  `show_age` tinyint(3) unsigned default NULL,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31173 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_new_people` */

CREATE TABLE `layout_module_new_people` (
  `module_page_id` int(11) NOT NULL auto_increment,
  `number_showing` int(11) NOT NULL default '0',
  `with_picture_only` tinyint(3) unsigned NOT NULL default '0',
  `title` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table for the "New People" widget';

/*Table structure for table `layout_module_newest_media` */

CREATE TABLE `layout_module_newest_media` (
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  `title` text NOT NULL,
  `header` text NOT NULL,
  `footer` text NOT NULL,
  `show_mature` tinyint(1) unsigned NOT NULL default '0',
  `time_period_id` tinyint(4) unsigned NOT NULL default '10',
  `media_per_page` tinyint(4) unsigned NOT NULL default '20',
  `media_per_row` tinyint(4) unsigned NOT NULL default '0',
  `media_location` tinyint(1) unsigned NOT NULL default '0',
  `image_size` tinyint(4) unsigned NOT NULL default '2',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=571 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_omm_media` */

CREATE TABLE `layout_module_omm_media` (
  `module_page_id` int(11) NOT NULL auto_increment,
  `title` text,
  `asset_id` int(11) NOT NULL default '0',
  `auto_start` tinyint(4) NOT NULL default '0' COMMENT 'autoplay the media on load',
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4058523 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_omm_playlist` */

CREATE TABLE `layout_module_omm_playlist` (
  `title` text,
  `items_per_page` int(11) NOT NULL default '10',
  `module_page_id` int(11) NOT NULL auto_increment,
  `group_id` int(11) default NULL,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65400 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_personal` */

CREATE TABLE `layout_module_personal` (
  `title` text,
  `module_page_id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=MyISAM AUTO_INCREMENT=772564 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_profile` */

CREATE TABLE `layout_module_profile` (
  `title` text,
  `module_page_id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3584755 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_single_picture` */

CREATE TABLE `layout_module_single_picture` (
  `module_page_id` int(11) NOT NULL auto_increment,
  `title` text,
  `asset_id` int(11) NOT NULL default '0',
  `alignment` smallint(5) unsigned default NULL COMMENT '0 = left, 1 = center, 2 = right',
  `size` smallint(5) unsigned default NULL,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44357 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_slide_show` */

CREATE TABLE `layout_module_slide_show` (
  `module_page_id` int(11) NOT NULL auto_increment,
  `title` text,
  `hide_caption` tinyint(3) unsigned NOT NULL default '0',
  `automated` tinyint(3) unsigned NOT NULL default '1',
  `size` smallint(5) unsigned default NULL,
  `group_id` int(11) default NULL,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3978253 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_stores` */

CREATE TABLE `layout_module_stores` (
  `title` text,
  `items_per_page` int(11) NOT NULL default '10',
  `module_page_id` int(11) NOT NULL auto_increment,
  `group_id` int(11) default NULL,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6011886 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_system_stats` */

CREATE TABLE `layout_module_system_stats` (
  `module_page_id` int(11) NOT NULL auto_increment,
  `title` text,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=82822 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_title_text` */

CREATE TABLE `layout_module_title_text` (
  `text` text,
  `show_menu` tinyint(4) NOT NULL default '1',
  `module_page_id` int(11) NOT NULL auto_increment,
  `banner` longblob,
  `banner_path` varchar(255) default NULL,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=282942 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_module_user_upload` */

CREATE TABLE `layout_module_user_upload` (
  `module_page_id` int(10) unsigned NOT NULL auto_increment,
  `title` text NOT NULL,
  `header` text NOT NULL,
  `asset_group_id` tinyint(4) unsigned NOT NULL default '1',
  `channel_owner_tags` text NOT NULL,
  `tag_instructions` text NOT NULL,
  PRIMARY KEY  (`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=507 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_modules` */

CREATE TABLE `layout_modules` (
  `module_id` int(11) NOT NULL auto_increment,
  `module_group_id` int(11) NOT NULL default '0',
  `title` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  `icon_url` varchar(100) NOT NULL default '',
  `map_table` varchar(100) default NULL,
  `sequence` int(11) NOT NULL default '0',
  `visible` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`module_id`),
  KEY `FK_module_group_id` (`module_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_page_access` */

CREATE TABLE `layout_page_access` (
  `access_id` int(11) NOT NULL auto_increment,
  `description` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`access_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_page_modules` */

CREATE TABLE `layout_page_modules` (
  `module_page_id` int(11) NOT NULL default '0',
  `page_id` int(11) NOT NULL default '0',
  `module_id` int(11) NOT NULL default '0',
  `zone_id` int(11) NOT NULL default '0',
  `id` int(11) NOT NULL auto_increment,
  `sequence` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `FK_zone_id` (`zone_id`),
  KEY `FK_page_id` (`page_id`),
  KEY `FK_module_id` (`module_id`),
  KEY `pid_mid` (`page_id`,`module_id`),
  KEY `pid_mid_mpid` (`page_id`,`module_id`,`module_page_id`),
  KEY `mid_mpid` (`module_id`,`module_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48928538 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_pages` */

CREATE TABLE `layout_pages` (
  `page_id` int(11) NOT NULL auto_increment,
  `home_page` tinyint(1) NOT NULL default '0',
  `name` varchar(80) NOT NULL default '',
  `template_id` int(11) NOT NULL default '0',
  `channel_id` int(11) default '0',
  `password` varchar(100) default '',
  `sequence` int(11) NOT NULL default '0',
  `access_id` int(11) NOT NULL default '0',
  `body_alignment` tinyint(4) NOT NULL default '0',
  `group_id` int(10) unsigned default NULL,
  `left_percentage` decimal(10,3) NOT NULL default '0.340',
  PRIMARY KEY  (`page_id`),
  KEY `FK_access_id` (`access_id`),
  KEY `FK_channel_id` (`channel_id`),
  KEY `FK_template_id` (`template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4799916 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_standard_templates` */

CREATE TABLE `layout_standard_templates` (
  `standard_template_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  `style_sheet` varchar(100) NOT NULL default '',
  `preview_url` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`standard_template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_template_font` */

CREATE TABLE `layout_template_font` (
  `rgb` varchar(8) default NULL,
  `underline` tinyint(4) default '0',
  `bold` tinyint(4) default '0',
  `italic` tinyint(4) default '0',
  `pixel_size` int(11) default NULL,
  `font_name_id` int(11) default '0',
  `style_id` int(11) default '0',
  `transparent` tinyint(4) default '0',
  `font_id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`font_id`),
  KEY `FK_style_id` (`style_id`),
  KEY `FK_font_name_id` (`font_name_id`)
) ENGINE=InnoDB AUTO_INCREMENT=413858 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_template_font_styles` */

CREATE TABLE `layout_template_font_styles` (
  `style_id` int(11) NOT NULL auto_increment,
  `description` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`style_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_templates` */

CREATE TABLE `layout_templates` (
  `template_id` int(11) NOT NULL auto_increment,
  `standard_template_id` int(11) NOT NULL default '1',
  `title` varchar(80) default NULL,
  `base_font` int(11) default NULL,
  `base_hover` int(11) default NULL,
  `base_link` int(11) default NULL,
  `base_visited` int(11) default NULL,
  `module_header_rgb` varchar(8) default NULL,
  `module_background_rgb` varchar(8) default NULL,
  `module_picture` varchar(255) default NULL,
  `module_border_rgb` varchar(8) default NULL,
  `module_border_width` varchar(10) default NULL,
  `module_border_style` varchar(12) default NULL,
  `module_opacity` int(11) default NULL,
  `module_border_between_main_side` int(11) default NULL,
  `background_rgb` varchar(8) default NULL,
  `background_picture` varchar(255) default NULL,
  `background_orientation` int(11) default NULL,
  `background_fixed` tinyint(4) default NULL,
  `background_repeat` int(11) default NULL,
  `user_defined` tinyint(4) NOT NULL default '0',
  `module_outer_border_rgb` varchar(8) default NULL,
  `module_outer_border_width` varchar(10) default NULL,
  `custom_css` text,
  `opacity` int(11) default NULL,
  PRIMARY KEY  (`template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2618618 DEFAULT CHARSET=latin1;

/*Table structure for table `layout_zones` */

CREATE TABLE `layout_zones` (
  `zone_id` int(11) NOT NULL auto_increment,
  `name` varchar(25) NOT NULL default '',
  `width` smallint(5) unsigned NOT NULL default '0',
  `height` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (`zone_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `leader_board_configuration_options` */

CREATE TABLE `leader_board_configuration_options` (
  `leader_config_id` int(10) unsigned NOT NULL default '0',
  `config_option_description` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`leader_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `leader_board_time_periods` */

CREATE TABLE `leader_board_time_periods` (
  `time_period_id` smallint(5) unsigned NOT NULL,
  `period_in_days` smallint(5) unsigned NOT NULL default '0',
  `description` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`time_period_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `leaderboard_excluded_channels` */

CREATE TABLE `leaderboard_excluded_channels` (
  `channel_id` int(11) NOT NULL,
  PRIMARY KEY  (`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `leaderboard_excluded_users` */

CREATE TABLE `leaderboard_excluded_users` (
  `user_id` int(11) NOT NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `license_subscription` */

CREATE TABLE `license_subscription` (
  `license_subscription_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `length_of_subscription` int(11) NOT NULL default '0',
  `amount` float NOT NULL default '0',
  `amount_kei_point_id` varchar(50) NOT NULL default '',
  `max_server_users` int(11) default NULL,
  `disk_quota` int(11) NOT NULL default '0',
  PRIMARY KEY  (`license_subscription_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `marketing_email_records_by_users` */

CREATE TABLE `marketing_email_records_by_users` (
  `user_id` int(11) NOT NULL,
  `email_type` int(11) NOT NULL,
  `date_sent` datetime NOT NULL,
  KEY `Index_2` (`email_type`),
  KEY `Index_3` (`date_sent`),
  KEY `Index_4` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

/*Table structure for table `marketing_email_tracking` */

CREATE TABLE `marketing_email_tracking` (
  `view_id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `mailing_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `ipaddress` varchar(255) default NULL,
  `useragent` varchar(255) default NULL,
  PRIMARY KEY  (`view_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93140 DEFAULT CHARSET=latin1;

/*Table structure for table `marketing_share_referrals` */

CREATE TABLE `marketing_share_referrals` (
  `existing_user_id` int(10) unsigned NOT NULL default '0' COMMENT 'User ID of referring user',
  `referred_user_id` int(10) unsigned NOT NULL default '0' COMMENT 'User ID of user that was referred',
  `registered_date` datetime NOT NULL COMMENT 'Date referred user registered',
  `completed_checklist_date` datetime default NULL COMMENT 'Date referred user completed checklist',
  PRIMARY KEY  (`referred_user_id`,`existing_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tracks referrers to referrals and the status of referrals.';

/*Table structure for table `marketing_share_tracking` */

CREATE TABLE `marketing_share_tracking` (
  `user_id` int(11) unsigned NOT NULL default '0' COMMENT 'User Id of member',
  `number_of_views` int(11) unsigned NOT NULL default '0' COMMENT 'Number of times banner was viewed',
  `number_of_clicks` int(11) unsigned NOT NULL default '0' COMMENT 'Nuber of times banner was clicked',
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tracks the views and clicks of banners from Share Kaneva.';

/*Table structure for table `message_gifts` */

CREATE TABLE `message_gifts` (
  `message_id` int(11) NOT NULL COMMENT 'FK to messages',
  `gift_id` int(11) unsigned NOT NULL COMMENT 'FK to gift_catalog_items',
  `gift_status` enum('U','A','R') NOT NULL default 'U' COMMENT 'Unaccpeted, Accepted, Rejected',
  `gift_type` enum('2D','3D') default '3D',
  PRIMARY KEY  (`message_id`,`gift_id`,`gift_status`),
  KEY `INDEX_giftmsg_type` (`gift_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='M2M between messages and gift_catalog_items for gifting';

/*Table structure for table `message_status` */

CREATE TABLE `message_status` (
  `status_id` int(11) NOT NULL default '0',
  `name` varchar(20) NOT NULL default '',
  `description` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `messages` */

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL default '0',
  `from_id` int(11) NOT NULL default '0',
  `from_id_status` int(11) NOT NULL default '0',
  `to_id` int(11) NOT NULL default '0',
  `to_id_status` int(11) NOT NULL default '0',
  `subject` varchar(100) NOT NULL default '',
  `message` text NOT NULL,
  `message_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `replied` smallint(6) NOT NULL default '0',
  `type` int(11) NOT NULL default '0',
  `channel_id` int(11) default NULL,
  `to_viewable` enum('Y','N') NOT NULL default 'Y',
  `from_viewable` enum('Y','N') NOT NULL default 'Y',
  PRIMARY KEY  (`message_id`),
  KEY `from_state` (`from_id`,`from_id_status`),
  KEY `all_the_tos` (`to_id`,`to_id_status`),
  KEY `tov` (`to_viewable`),
  KEY `frv` (`from_viewable`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `messages_non_friends` */

CREATE TABLE `messages_non_friends` (
  `user_id` int(11) NOT NULL,
  `number_of_messages_sent` int(11) NOT NULL default '0' COMMENT 'total number of messages sent to non-friends per date in last_message_sent_date column',
  `last_message_sent_date` datetime NOT NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

/*Table structure for table `new_assets_by_type` */

CREATE TABLE `new_assets_by_type` (
  `asset_id` int(10) unsigned NOT NULL,
  `asset_type_id` int(10) unsigned NOT NULL,
  `date_stamp` datetime NOT NULL,
  PRIMARY KEY  (`asset_id`),
  KEY `Index_2` (`asset_type_id`),
  KEY `Index_3` (`asset_id`),
  KEY `Index_4` (`date_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `new_channels` */

CREATE TABLE `new_channels` (
  `channel_id` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY  (`channel_id`),
  KEY `dt` (`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `new_channels_by_day` */

CREATE TABLE `new_channels_by_day` (
  `date_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`date_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `notification_signup` */

CREATE TABLE `notification_signup` (
  `notify_id` int(11) NOT NULL auto_increment,
  `email` varchar(100) NOT NULL default '',
  `create_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`notify_id`)
) ENGINE=InnoDB AUTO_INCREMENT=346 DEFAULT CHARSET=latin1;

/*Table structure for table `order_billing_information` */

CREATE TABLE `order_billing_information` (
  `order_billing_information_id` int(11) NOT NULL auto_increment,
  `name_on_card` varchar(100) NOT NULL default '',
  `card_number` varchar(40) NOT NULL default '',
  `card_type` varchar(50) NOT NULL default '',
  `exp_month` varchar(50) NOT NULL default '',
  `exp_day` varchar(50) NOT NULL default '',
  `exp_year` varchar(50) default NULL,
  `address_name` varchar(100) NOT NULL default '',
  `phone_number` varchar(50) NOT NULL default '',
  `address1` varchar(100) NOT NULL default '',
  `address2` varchar(100) default NULL,
  `city` varchar(100) NOT NULL default '',
  `state_code` varchar(100) NOT NULL default '',
  `zip_code` varchar(25) NOT NULL default '',
  `country_id` char(2) NOT NULL default '',
  PRIMARY KEY  (`order_billing_information_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29238 DEFAULT CHARSET=latin1;

/*Table structure for table `order_item_download` */

CREATE TABLE `order_item_download` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `order_item_id` int(10) unsigned NOT NULL default '0',
  `client_ip` varchar(45) default NULL,
  `datetime_downloaded` datetime NOT NULL default '0000-00-00 00:00:00',
  `download_type_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `order_items` */

CREATE TABLE `order_items` (
  `order_item_id` int(11) NOT NULL auto_increment,
  `order_id` int(11) NOT NULL default '0',
  `asset_id` int(11) default NULL,
  `user_asset_subscription_id` int(11) default NULL,
  `asset_subscription_id` int(11) default NULL,
  `community_member_subscription_id` int(11) default NULL,
  `featured_asset_id` int(11) default NULL,
  `game_license_id` int(11) default NULL,
  `point_amount` float NOT NULL default '0',
  `quantity` int(11) NOT NULL default '0',
  `royalty_amount` decimal(12,3) default NULL,
  `royalty_paid` char(1) default NULL,
  `royalty_user_id_paid` int(11) default NULL,
  `number_of_downloads` int(11) NOT NULL default '0',
  `number_of_downloads_allowed` int(11) NOT NULL default '5',
  `number_of_streams` int(11) NOT NULL default '0',
  `token` varchar(20) default NULL,
  `submitted_as_inventory` char(1) NOT NULL default 'N',
  PRIMARY KEY  (`order_item_id`),
  KEY `FK_order_items_orders` (`order_id`),
  KEY `FK_asset_id` (`asset_id`),
  KEY `royalty_user_id_paid` (`royalty_user_id_paid`),
  KEY `order_id_asset_id` (`order_id`,`asset_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1410638 DEFAULT CHARSET=latin1;

/*Table structure for table `order_transaction_amounts_paid` */

CREATE TABLE `order_transaction_amounts_paid` (
  `order_id` int(11) NOT NULL default '0',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `amount` float NOT NULL default '0',
  PRIMARY KEY  (`order_id`,`kei_point_id`),
  KEY `FK_order_transaction_amounts_paid_kei_points` (`kei_point_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `orders` */

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL auto_increment,
  `purchase_type` int(11) NOT NULL default '0',
  `description` varchar(100) default NULL,
  `user_id` int(11) NOT NULL default '0',
  `purchase_date` datetime default NULL,
  `transaction_status_id` int(11) NOT NULL default '0',
  `gross_point_amount` decimal(12,2) NOT NULL default '0.00',
  `download_end_date` datetime default NULL,
  `ip_address` varchar(30) default NULL,
  `error_description` varchar(100) default NULL,
  `download_stop_date` datetime default NULL,
  `point_transaction_id` int(11) default NULL,
  PRIMARY KEY  (`order_id`),
  KEY `FK_orders_purchase_transaction_status` (`transaction_status_id`),
  KEY `FK_orders_users` (`user_id`),
  KEY `INDEX_purchase_date` (`purchase_date`),
  KEY `utp` (`user_id`,`transaction_status_id`,`purchase_type`)
) ENGINE=InnoDB AUTO_INCREMENT=1496419 DEFAULT CHARSET=latin1;

/*Table structure for table `parsed_order_items_word_count` */

CREATE TABLE `parsed_order_items_word_count` (
  `order_item_id` int(11) unsigned NOT NULL default '0',
  `word_id` int(11) unsigned default NULL,
  `total` int(7) unsigned default NULL,
  UNIQUE KEY `id_index` (`order_item_id`,`word_id`),
  KEY `word_src` (`word_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `payment_methods` */

CREATE TABLE `payment_methods` (
  `payment_method_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(255) default NULL,
  `status_id` int(11) NOT NULL default '0',
  `adaptor` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`payment_method_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `personal_communities` */

CREATE TABLE `personal_communities` (
  `community_id` int(11) NOT NULL,
  PRIMARY KEY  (`community_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `point_system` */

CREATE TABLE `point_system` (
  `activity_code` int(10) unsigned NOT NULL auto_increment,
  `activity_description` varchar(45) NOT NULL default '',
  `points_awarded` int(10) unsigned NOT NULL default '0',
  `max_award` int(10) unsigned NOT NULL default '0',
  `activity_group` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`activity_code`)
) ENGINE=InnoDB AUTO_INCREMENT=231 DEFAULT CHARSET=latin1;

/*Table structure for table `poll_options` */

CREATE TABLE `poll_options` (
  `poll_option_id` int(11) NOT NULL default '0',
  `poll_id` int(11) NOT NULL default '0',
  `option_text` varchar(100) NOT NULL default '',
  `option_votes` int(11) default NULL,
  `display_order` int(11) default NULL,
  `status_id` int(11) default NULL,
  PRIMARY KEY  (`poll_option_id`,`poll_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `poll_votes` */

CREATE TABLE `poll_votes` (
  `poll_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `poll_option_id` int(11) NOT NULL default '0',
  `vote_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `status_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`poll_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `polls` */

CREATE TABLE `polls` (
  `poll_id` int(11) NOT NULL auto_increment,
  `poll_question` varchar(250) NOT NULL default '',
  `created_on_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `status_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`poll_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `popular_keywords` */

CREATE TABLE `popular_keywords` (
  `keyword` varchar(100) NOT NULL default '',
  `total_count` int(11) NOT NULL default '0',
  `asset_type_id` int(11) NOT NULL default '-1',
  KEY `key_word_search` (`keyword`,`asset_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `popular_keywords_bku` */

CREATE TABLE `popular_keywords_bku` (
  `keyword` varchar(100) NOT NULL default '',
  `total_count` int(11) NOT NULL default '0',
  `asset_type_id` int(11) NOT NULL default '0',
  KEY `key_word_search` (`keyword`,`asset_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `posts_7_days` */

CREATE TABLE `posts_7_days` (
  `log_id` int(10) unsigned NOT NULL auto_increment,
  `channel_id` int(10) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY  USING BTREE (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `potty_mouth` */

CREATE TABLE `potty_mouth` (
  `id` int(11) NOT NULL auto_increment,
  `bad_word` varchar(50) NOT NULL default '',
  `replace_with` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `promotional_offer_items` */

CREATE TABLE `promotional_offer_items` (
  `wok_item_id` int(10) unsigned NOT NULL default '0',
  `promotion_id` smallint(5) unsigned NOT NULL default '0',
  `gift_card_id` int(10) unsigned NOT NULL default '0',
  `community_id` int(10) unsigned NOT NULL default '0',
  `item_description` varchar(125) NOT NULL default '',
  `alternate_description` varchar(125) default NULL,
  `market_price` smallint(5) unsigned NOT NULL default '0',
  `quantity` smallint(5) unsigned NOT NULL default '0',
  `gender` enum('M','F','U') NOT NULL default 'U',
  `promotion_offer_id` smallint(5) unsigned NOT NULL auto_increment,
  `modifiers_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`wok_item_id`,`promotion_id`,`gift_card_id`,`community_id`),
  KEY `idx_promo_item_id` (`promotion_offer_id`),
  KEY `g` (`gender`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=latin1;

/*Table structure for table `promotional_offer_type` */

CREATE TABLE `promotional_offer_type` (
  `promotional_offer_type_id` smallint(5) unsigned NOT NULL default '0',
  `promotional_offer_type` varchar(50) NOT NULL default '',
  `description` varchar(100) default NULL,
  PRIMARY KEY  (`promotional_offer_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `promotional_offers` */

CREATE TABLE `promotional_offers` (
  `promotion_id` smallint(5) unsigned NOT NULL auto_increment,
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `dollar_amount` decimal(12,2) NOT NULL default '0.00',
  `kei_point_amount` decimal(12,2) NOT NULL default '0.00',
  `free_points_awarded_amount` decimal(12,2) NOT NULL default '0.00',
  `free_kei_point_ID` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `is_special` tinyint(1) unsigned NOT NULL default '0',
  `promotion_description` varchar(100) default NULL,
  `promotional_offers_type_id` smallint(6) NOT NULL default '0',
  `wok_pass_group_id` int(11) NOT NULL,
  `sku` smallint(5) unsigned default '0',
  `bundle_title` varchar(100) NOT NULL default '',
  `bundle_subheading1` varchar(100) default NULL,
  `bundle_subheading2` varchar(100) default NULL,
  `promotion_start` datetime NOT NULL default '0000-00-00 00:00:00',
  `promotion_end` datetime NOT NULL default '0000-00-00 00:00:00',
  `promotion_list_heading` varchar(100) NOT NULL default '',
  `highlight_color` char(7) default NULL,
  `special_background_color` char(7) default NULL,
  `special_background_image` varchar(500) default NULL,
  `special_sticker_image` varchar(500) default NULL,
  `promotional_package_label` varchar(100) default NULL,
  `special_font_color` char(7) default NULL,
  `coupon_code` varchar(45) default NULL COMMENT 'Code that is used to referrence this promotion.',
  `value_of_credits` decimal(12,2) default NULL,
  `modifiers_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`promotion_id`),
  KEY `idx_isSpecial` (`is_special`),
  KEY `idx_promotionStart` (`promotion_start`),
  KEY `idx_promotionEnd` (`promotion_end`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

/*Table structure for table `public_communities` */

CREATE TABLE `public_communities` (
  `community_id` int(11) NOT NULL,
  PRIMARY KEY  (`community_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `purchase_point_transaction_amounts` */

CREATE TABLE `purchase_point_transaction_amounts` (
  `point_transaction_id` int(11) NOT NULL default '0',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `amount` decimal(12,2) NOT NULL default '0.00',
  PRIMARY KEY  (`point_transaction_id`,`kei_point_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `purchase_point_transactions` */

CREATE TABLE `purchase_point_transactions` (
  `point_transaction_id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL default '0',
  `description` varchar(200) default NULL,
  `billing_information_id` int(11) default NULL,
  `address_id` int(11) default NULL,
  `order_billing_information_id` int(11) default NULL,
  `payment_method_id` int(11) NOT NULL default '0',
  `amount_debited` decimal(19,4) NOT NULL default '0.0000',
  `amount_credited` decimal(19,4) default NULL,
  `tax` decimal(19,4) default NULL,
  `order_id` int(11) default NULL,
  `point_bucket_id` int(11) default NULL,
  `gift_card_id` int(11) NOT NULL default '0',
  `authorization_id` varchar(10) default NULL,
  `transaction_id` varchar(100) default NULL,
  `transaction_status` int(20) default NULL,
  `transaction_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `error_description` varchar(255) default NULL,
  `user_license_subscription_id` int(11) default NULL,
  `ip_address` varchar(30) default NULL,
  `created_date` datetime default NULL,
  PRIMARY KEY  (`point_transaction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=68009 DEFAULT CHARSET=latin1;

/*Table structure for table `purchase_point_transactions_status` */

CREATE TABLE `purchase_point_transactions_status` (
  `transaction_status_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  `long_description` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`transaction_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `purchase_transaction_amounts` */

CREATE TABLE `purchase_transaction_amounts` (
  `purchase_transaction_id` int(11) NOT NULL default '0',
  `kei_point_id` varchar(50) NOT NULL default '',
  `amount` decimal(12,2) default NULL,
  PRIMARY KEY  (`purchase_transaction_id`,`kei_point_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `purchase_transaction_status` */

CREATE TABLE `purchase_transaction_status` (
  `transaction_status_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`transaction_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `purchase_transactions` */

CREATE TABLE `purchase_transactions` (
  `purchase_transaction_id` int(11) NOT NULL auto_increment,
  `purchase_type` varchar(50) NOT NULL default '',
  `description` varchar(100) default NULL,
  `user_id` int(11) NOT NULL default '0',
  `asset_id` int(11) default NULL,
  `download_end_date` datetime default NULL,
  `user_asset_subscription_id` int(11) default NULL,
  `community_member_subscription_id` int(11) default NULL,
  `purchase_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `community_id` int(11) default NULL,
  `royalty_sale` char(1) NOT NULL default '',
  `master_user_id_paid_royalties` int(11) default NULL,
  `transaction_status` int(11) default NULL,
  `gross_point_amount` decimal(12,2) default NULL,
  PRIMARY KEY  (`purchase_transaction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1381 DEFAULT CHARSET=latin1;

/*Table structure for table `rank_by_month` */

CREATE TABLE `rank_by_month` (
  `user_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `recent_community_views` */

CREATE TABLE `recent_community_views` (
  `view_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL default '0',
  `community_id` int(10) unsigned NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `anonymous` tinyint(3) unsigned NOT NULL default '0',
  `ip_address` int(11) unsigned default NULL,
  PRIMARY KEY  (`view_id`),
  KEY `cv_hub` (`community_id`),
  KEY `Index_3` (`created_date`),
  KEY `Index_4` (`community_id`,`created_date`)
) ENGINE=MEMORY AUTO_INCREMENT=66273690 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `related_assets` */

CREATE TABLE `related_assets` (
  `asset_id` int(10) unsigned NOT NULL auto_increment,
  `related_asset_id` int(10) unsigned NOT NULL,
  `downloads` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY  (`asset_id`,`related_asset_id`),
  KEY `dt` (`created_date`),
  KEY `raid` (`related_asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `related_items` */

CREATE TABLE `related_items` (
  `_order` double NOT NULL,
  `base_asset_id` int(11) NOT NULL,
  `related_asset_id` int(11) NOT NULL,
  `mature` enum('Y','N') default 'N',
  PRIMARY KEY  (`_order`),
  UNIQUE KEY `base_asset_id` (`base_asset_id`,`related_asset_id`),
  KEY `baid` (`base_asset_id`),
  KEY `baid_ord` (`_order`,`base_asset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `restricted_words` */

CREATE TABLE `restricted_words` (
  `restricted_words_id` int(11) NOT NULL auto_increment,
  `word` varchar(50) NOT NULL default '',
  `replace_with` varchar(50) default '',
  `restriction_type_id` int(11) NOT NULL,
  `match_type` enum('exact','anywhere','start') default NULL,
  PRIMARY KEY  (`restricted_words_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

/*Table structure for table `restriction_types` */

CREATE TABLE `restriction_types` (
  `restriction_type_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY  (`restriction_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `reward_events` */

CREATE TABLE `reward_events` (
  `reward_event_id` int(11) unsigned NOT NULL auto_increment COMMENT 'PK',
  `event_name` varchar(100) NOT NULL default '' COMMENT 'name of event',
  `transaction_type_id` smallint(5) unsigned NOT NULL COMMENT 'FK to transaction_type table',
  PRIMARY KEY  (`reward_event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COMMENT='Events users can get rewards for participating in';

/*Table structure for table `reward_log` */

CREATE TABLE `reward_log` (
  `reward_log_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `amount` float NOT NULL,
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL,
  `transaction_type` smallint(5) unsigned NOT NULL,
  `return_code` int(10) unsigned NOT NULL,
  `reward_event_id` int(11) unsigned NOT NULL default '0' COMMENT 'FK to reward_events table',
  `wok_transaction_log_id` bigint(20) NOT NULL default '0' COMMENT 'FK to wok_transaction_log table',
  PRIMARY KEY  (`reward_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=396868 DEFAULT CHARSET=latin1;

/*Table structure for table `roles` */

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL default '0',
  `description` varchar(80) default NULL,
  `role_value` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `royalty_payments` */

CREATE TABLE `royalty_payments` (
  `payment_id` int(11) NOT NULL default '0',
  `user_id_paid` int(11) NOT NULL default '0',
  `check_number` varchar(50) default NULL,
  `payment_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `period_start_date` datetime default NULL,
  `period_end_date` datetime default NULL,
  `total_amount` decimal(12,3) NOT NULL default '0.000',
  `amount_paid` decimal(12,3) NOT NULL default '0.000',
  `tax_withheld` decimal(12,3) NOT NULL default '0.000',
  `notes` varchar(200) default NULL,
  `user_id_created` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `rpt_new_file_upload` */

CREATE TABLE `rpt_new_file_upload` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `date_input` date NOT NULL default '0000-00-00',
  `num_files_uploaded` int(10) unsigned NOT NULL default '0',
  `asset_type_id` int(10) unsigned NOT NULL default '0',
  `datetime_calculated` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `rpt_new_user_signup` */

CREATE TABLE `rpt_new_user_signup` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `date_input` date NOT NULL default '0000-00-00',
  `num_new_users` int(10) unsigned NOT NULL default '0',
  `datetime_calculated` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `rpt_time_traffic` */

CREATE TABLE `rpt_time_traffic` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `date_input` date NOT NULL default '0000-00-00',
  `asset_id` int(10) unsigned NOT NULL default '0',
  `num_downloads` int(10) unsigned NOT NULL default '0',
  `datetime_calculated` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `rss_suggested_feeds` */

CREATE TABLE `rss_suggested_feeds` (
  `feed_id` int(11) NOT NULL auto_increment,
  `description` varchar(100) NOT NULL default '',
  `url` varchar(200) default '',
  PRIMARY KEY  (`feed_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `schema_versions` */

CREATE TABLE `schema_versions` (
  `version` decimal(11,4) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL default CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY  (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `schools` */

CREATE TABLE `schools` (
  `school_id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(80) NOT NULL,
  `city` varchar(45) NOT NULL,
  `state` char(2) NOT NULL,
  `user_count` int(10) unsigned NOT NULL default '0' COMMENT 'Number of users attending this school',
  PRIMARY KEY  (`school_id`),
  UNIQUE KEY `name` USING BTREE (`name`),
  KEY `city` (`city`(10)),
  KEY `state` (`state`)
) ENGINE=InnoDB AUTO_INCREMENT=5774 DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_communities` */

CREATE TABLE `snapshot_communities` (
  `community_count` int(11) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_profiles` */

CREATE TABLE `snapshot_profiles` (
  `profile_count` int(11) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_site_members` */

CREATE TABLE `snapshot_site_members` (
  `member_count` int(11) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `spender_activity` */

CREATE TABLE `spender_activity` (
  `user_id` int(10) unsigned NOT NULL,
  `total_spent` double NOT NULL,
  `active_spender` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `spender_activity_counts` */

CREATE TABLE `spender_activity_counts` (
  `created_date` date NOT NULL default '0000-00-00',
  `active_spender_counts` int(11) default NULL,
  `inactive_spender_counts` int(11) default NULL,
  PRIMARY KEY  (`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `states` */

CREATE TABLE `states` (
  `state_code` char(2) NOT NULL default '',
  `state` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`state_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `stats_converter` */

CREATE TABLE `stats_converter` (
  `id` int(11) NOT NULL auto_increment,
  `asset_id` int(11) NOT NULL default '0',
  `start_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `processing_time_seconds` int(11) NOT NULL default '0',
  `filepath` varchar(255) NOT NULL default '',
  `source_size` int(11) NOT NULL default '0',
  `target_size` int(11) NOT NULL default '0',
  `result` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=302007 DEFAULT CHARSET=latin1;

/*Table structure for table `stats_publishing_processor` */

CREATE TABLE `stats_publishing_processor` (
  `id` int(11) NOT NULL auto_increment,
  `start_time` datetime default '0000-00-00 00:00:00',
  `num_processed` int(11) NOT NULL default '0',
  `num_to_process` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1566068 DEFAULT CHARSET=latin1;

/*Table structure for table `subscription_billing_information` */

CREATE TABLE `subscription_billing_information` (
  `sub_billing_information_id` int(11) NOT NULL auto_increment,
  `name_on_card` varchar(100) NOT NULL default '',
  `card_number` varchar(40) NOT NULL default '',
  `card_type` varchar(50) NOT NULL default '',
  `exp_month` varchar(50) NOT NULL default '',
  `exp_day` varchar(50) NOT NULL default '',
  `exp_year` varchar(50) default NULL,
  `address_name` varchar(100) NOT NULL default '',
  `phone_number` varchar(50) NOT NULL default '',
  `address1` varchar(100) NOT NULL default '',
  `address2` varchar(100) default NULL,
  `city` varchar(100) NOT NULL default '',
  `state_code` varchar(100) NOT NULL default '',
  `zip_code` varchar(25) NOT NULL default '',
  `country_id` char(2) NOT NULL default '',
  PRIMARY KEY  (`sub_billing_information_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `summary_asset_channels` */

CREATE TABLE `summary_asset_channels` (
  `asset_id` int(11) NOT NULL default '0',
  `community_id` int(11) NOT NULL default '0',
  `is_adult` char(1) default NULL,
  `name` varchar(100) NOT NULL default '',
  `is_personal` tinyint(3) unsigned NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `creator_id` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '1980-01-01 00:00:00',
  PRIMARY KEY  (`asset_id`,`community_id`),
  KEY `ia` (`is_adult`),
  KEY `n` (`name`),
  KEY `is_personal` (`is_personal`),
  KEY `nns` (`name_no_spaces`),
  KEY `tn` (`thumbnail_small_path`),
  KEY `cid` (`creator_id`),
  KEY `cd` (`created_date`),
  KEY `atn` (`asset_id`,`thumbnail_small_path`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_asset_diggs` */

CREATE TABLE `summary_asset_diggs` (
  `asset_id` int(10) unsigned NOT NULL auto_increment,
  `date_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`asset_id`,`date_stamp`)
) ENGINE=InnoDB AUTO_INCREMENT=5296023 DEFAULT CHARSET=latin1;

/*Table structure for table `summary_asset_diggs_1_days` */

CREATE TABLE `summary_asset_diggs_1_days` (
  `asset_id` int(11) NOT NULL,
  `number_of_diggs` int(11) default '0',
  PRIMARY KEY  (`asset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `summary_asset_diggs_1_year` */

CREATE TABLE `summary_asset_diggs_1_year` (
  `asset_id` int(11) NOT NULL,
  `number_of_diggs` int(11) default '0',
  PRIMARY KEY  (`asset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `summary_asset_diggs_30_days` */

CREATE TABLE `summary_asset_diggs_30_days` (
  `asset_id` int(11) NOT NULL,
  `number_of_diggs` int(11) default '0',
  PRIMARY KEY  (`asset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `summary_asset_diggs_7_days` */

CREATE TABLE `summary_asset_diggs_7_days` (
  `asset_id` int(11) NOT NULL,
  `number_of_diggs` int(11) default '0',
  PRIMARY KEY  (`asset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `summary_assets_by_channel_by_day` */

CREATE TABLE `summary_assets_by_channel_by_day` (
  `channel_id` int(10) unsigned NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`channel_id`,`dt_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_assets_by_connection` */

CREATE TABLE `summary_assets_by_connection` (
  `channel_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_assets_by_day` */

CREATE TABLE `summary_assets_by_day` (
  `date_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`date_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_assets_by_type` */

CREATE TABLE `summary_assets_by_type` (
  `asset_type_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`asset_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_assets_by_type_by_channel` */

CREATE TABLE `summary_assets_by_type_by_channel` (
  `channel_id` int(10) unsigned NOT NULL,
  `asset_type_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`channel_id`,`asset_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_assets_by_type_by_user` */

CREATE TABLE `summary_assets_by_type_by_user` (
  `asset_type_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`asset_type_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_assets_by_user_by_day` */

CREATE TABLE `summary_assets_by_user_by_day` (
  `user_id` int(10) unsigned NOT NULL,
  `date_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`date_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_assets_viewed` */

CREATE TABLE `summary_assets_viewed` (
  `asset_id` int(10) unsigned NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`asset_id`,`dt_stamp`),
  KEY `c` (`count`),
  KEY `ds` (`dt_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_channel_diggs` */

CREATE TABLE `summary_channel_diggs` (
  `channel_id` int(10) unsigned NOT NULL auto_increment,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`channel_id`,`dt_stamp`)
) ENGINE=InnoDB AUTO_INCREMENT=1001314682 DEFAULT CHARSET=latin1;

/*Table structure for table `summary_channel_views` */

CREATE TABLE `summary_channel_views` (
  `channel_id` int(11) NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(11) NOT NULL default '0',
  PRIMARY KEY  (`channel_id`,`dt_stamp`),
  KEY `cid` (`channel_id`),
  KEY `dts` (`dt_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `summary_comments_by_day` */

CREATE TABLE `summary_comments_by_day` (
  `channel_id` int(10) unsigned NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`channel_id`,`dt_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_comments_by_user_by_day` */

CREATE TABLE `summary_comments_by_user_by_day` (
  `comment_date` date NOT NULL default '0000-00-00',
  `comment_count` smallint(5) unsigned NOT NULL default '0',
  `user_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`comment_date`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_community_members` */

CREATE TABLE `summary_community_members` (
  `channel_id` int(10) unsigned NOT NULL auto_increment,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`channel_id`,`dt_stamp`)
) ENGINE=InnoDB AUTO_INCREMENT=1001314642 DEFAULT CHARSET=latin1;

/*Table structure for table `summary_friends_by_day` */

CREATE TABLE `summary_friends_by_day` (
  `user_id` int(11) NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(11) default '0',
  PRIMARY KEY  (`user_id`,`dt_stamp`),
  KEY `dt` (`dt_stamp`),
  KEY `cnt` (`count`),
  KEY `uid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `summary_lb_channel2channel_day` */

CREATE TABLE `summary_lb_channel2channel_day` (
  `user_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL default '',
  `profile_picture` int(11) NOT NULL default '0',
  `raves_channel` int(11) NOT NULL default '0',
  `channels_created` int(11) NOT NULL default '0',
  `friend_accepts` int(11) NOT NULL default '0',
  `raves_media` int(11) NOT NULL default '0',
  `channel_members` int(11) NOT NULL default '0',
  `join_wok` int(11) NOT NULL default '0',
  `photos_uploaded` int(11) NOT NULL default '0',
  `video_uploaded` int(11) NOT NULL default '0',
  `music_uploaded` int(11) NOT NULL default '0',
  `people_invited` int(11) NOT NULL default '0',
  `page_pimped` int(11) NOT NULL default '0',
  `raves_your_profile` int(11) NOT NULL default '0',
  `total_points` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_lb_channel2channel_month` */

CREATE TABLE `summary_lb_channel2channel_month` (
  `user_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL default '',
  `profile_picture` int(11) NOT NULL default '0',
  `raves_channel` int(11) NOT NULL default '0',
  `channels_created` int(11) NOT NULL default '0',
  `friend_accepts` int(11) NOT NULL default '0',
  `raves_media` int(11) NOT NULL default '0',
  `channel_members` int(11) NOT NULL default '0',
  `join_wok` int(11) NOT NULL default '0',
  `photos_uploaded` int(11) NOT NULL default '0',
  `video_uploaded` int(11) NOT NULL default '0',
  `music_uploaded` int(11) NOT NULL default '0',
  `people_invited` int(11) NOT NULL default '0',
  `page_pimped` int(11) NOT NULL default '0',
  `raves_your_profile` int(11) NOT NULL default '0',
  `total_points` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_lb_channel2channel_week` */

CREATE TABLE `summary_lb_channel2channel_week` (
  `user_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL default '',
  `profile_picture` int(11) NOT NULL default '0',
  `raves_channel` int(11) NOT NULL default '0',
  `channels_created` int(11) NOT NULL default '0',
  `friend_accepts` int(11) NOT NULL default '0',
  `raves_media` int(11) NOT NULL default '0',
  `channel_members` int(11) NOT NULL default '0',
  `join_wok` int(11) NOT NULL default '0',
  `photos_uploaded` int(11) NOT NULL default '0',
  `video_uploaded` int(11) NOT NULL default '0',
  `music_uploaded` int(11) NOT NULL default '0',
  `people_invited` int(11) NOT NULL default '0',
  `page_pimped` int(11) NOT NULL default '0',
  `raves_your_profile` int(11) NOT NULL default '0',
  `total_points` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_lb_channel2channel_year` */

CREATE TABLE `summary_lb_channel2channel_year` (
  `user_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL default '',
  `profile_picture` int(11) NOT NULL default '0',
  `raves_channel` int(11) NOT NULL default '0',
  `channels_created` int(11) NOT NULL default '0',
  `friend_accepts` int(11) NOT NULL default '0',
  `raves_media` int(11) NOT NULL default '0',
  `channel_members` int(11) NOT NULL default '0',
  `join_wok` int(11) NOT NULL default '0',
  `photos_uploaded` int(11) NOT NULL default '0',
  `video_uploaded` int(11) NOT NULL default '0',
  `music_uploaded` int(11) NOT NULL default '0',
  `people_invited` int(11) NOT NULL default '0',
  `page_pimped` int(11) NOT NULL default '0',
  `raves_your_profile` int(11) NOT NULL default '0',
  `total_points` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_lb_temp` */

CREATE TABLE `summary_lb_temp` (
  `user_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `profile_picture` int(11) NOT NULL default '0',
  `raves_channel` int(11) NOT NULL default '0',
  `channels_created` int(11) NOT NULL default '0',
  `friend_accepts` int(11) NOT NULL default '0',
  `raves_media` int(11) NOT NULL default '0',
  `channel_members` int(11) NOT NULL default '0',
  `join_wok` int(11) NOT NULL default '0',
  `photos_uploaded` int(11) NOT NULL default '0',
  `video_uploaded` int(11) NOT NULL default '0',
  `music_uploaded` int(11) NOT NULL default '0',
  `people_invited` int(11) NOT NULL default '0',
  `page_pimped` int(11) NOT NULL default '0',
  `raves_your_profile` int(11) NOT NULL default '0',
  `total_points` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_lb_user2user_day` */

CREATE TABLE `summary_lb_user2user_day` (
  `user_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `profile_picture` int(11) NOT NULL default '0',
  `raves_channel` int(11) NOT NULL default '0',
  `channels_created` int(11) NOT NULL default '0',
  `friend_accepts` int(11) NOT NULL default '0',
  `raves_media` int(11) NOT NULL default '0',
  `channel_members` int(11) NOT NULL default '0',
  `join_wok` int(11) NOT NULL default '0',
  `photos_uploaded` int(11) NOT NULL default '0',
  `video_uploaded` int(11) NOT NULL default '0',
  `music_uploaded` int(11) NOT NULL default '0',
  `people_invited` int(11) NOT NULL default '0',
  `page_pimped` int(11) NOT NULL default '0',
  `raves_your_profile` int(11) NOT NULL default '0',
  `total_points` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_lb_user2user_month` */

CREATE TABLE `summary_lb_user2user_month` (
  `user_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `profile_picture` int(11) NOT NULL default '0',
  `raves_channel` int(11) NOT NULL default '0',
  `channels_created` int(11) NOT NULL default '0',
  `friend_accepts` int(11) NOT NULL default '0',
  `raves_media` int(11) NOT NULL default '0',
  `channel_members` int(11) NOT NULL default '0',
  `join_wok` int(11) NOT NULL default '0',
  `photos_uploaded` int(11) NOT NULL default '0',
  `video_uploaded` int(11) NOT NULL default '0',
  `music_uploaded` int(11) NOT NULL default '0',
  `people_invited` int(11) NOT NULL default '0',
  `page_pimped` int(11) NOT NULL default '0',
  `raves_your_profile` int(11) NOT NULL default '0',
  `total_points` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_lb_user2user_week` */

CREATE TABLE `summary_lb_user2user_week` (
  `user_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `profile_picture` int(11) NOT NULL default '0',
  `raves_channel` int(11) NOT NULL default '0',
  `channels_created` int(11) NOT NULL default '0',
  `friend_accepts` int(11) NOT NULL default '0',
  `raves_media` int(11) NOT NULL default '0',
  `channel_members` int(11) NOT NULL default '0',
  `join_wok` int(11) NOT NULL default '0',
  `photos_uploaded` int(11) NOT NULL default '0',
  `video_uploaded` int(11) NOT NULL default '0',
  `music_uploaded` int(11) NOT NULL default '0',
  `people_invited` int(11) NOT NULL default '0',
  `page_pimped` int(11) NOT NULL default '0',
  `raves_your_profile` int(11) NOT NULL default '0',
  `total_points` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_lb_user2user_year` */

CREATE TABLE `summary_lb_user2user_year` (
  `user_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `profile_picture` int(11) NOT NULL default '0',
  `raves_channel` int(11) NOT NULL default '0',
  `channels_created` int(11) NOT NULL default '0',
  `friend_accepts` int(11) NOT NULL default '0',
  `raves_media` int(11) NOT NULL default '0',
  `channel_members` int(11) NOT NULL default '0',
  `join_wok` int(11) NOT NULL default '0',
  `photos_uploaded` int(11) NOT NULL default '0',
  `video_uploaded` int(11) NOT NULL default '0',
  `music_uploaded` int(11) NOT NULL default '0',
  `people_invited` int(11) NOT NULL default '0',
  `page_pimped` int(11) NOT NULL default '0',
  `raves_your_profile` int(11) NOT NULL default '0',
  `total_points` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_lb_userNchannel_day` */

CREATE TABLE `summary_lb_userNchannel_day` (
  `user_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `profile_picture` int(11) NOT NULL default '0',
  `raves_channel` int(11) NOT NULL default '0',
  `channels_created` int(11) NOT NULL default '0',
  `friend_accepts` int(11) NOT NULL default '0',
  `raves_media` int(11) NOT NULL default '0',
  `channel_members` int(11) NOT NULL default '0',
  `join_wok` int(11) NOT NULL default '0',
  `photos_uploaded` int(11) NOT NULL default '0',
  `video_uploaded` int(11) NOT NULL default '0',
  `music_uploaded` int(11) NOT NULL default '0',
  `people_invited` int(11) NOT NULL default '0',
  `page_pimped` int(11) NOT NULL default '0',
  `raves_your_profile` int(11) NOT NULL default '0',
  `total_points` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_lb_userNchannel_month` */

CREATE TABLE `summary_lb_userNchannel_month` (
  `user_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `profile_picture` int(11) NOT NULL default '0',
  `raves_channel` int(11) NOT NULL default '0',
  `channels_created` int(11) NOT NULL default '0',
  `friend_accepts` int(11) NOT NULL default '0',
  `raves_media` int(11) NOT NULL default '0',
  `channel_members` int(11) NOT NULL default '0',
  `join_wok` int(11) NOT NULL default '0',
  `photos_uploaded` int(11) NOT NULL default '0',
  `video_uploaded` int(11) NOT NULL default '0',
  `music_uploaded` int(11) NOT NULL default '0',
  `people_invited` int(11) NOT NULL default '0',
  `page_pimped` int(11) NOT NULL default '0',
  `raves_your_profile` int(11) NOT NULL default '0',
  `total_points` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_lb_userNchannel_week` */

CREATE TABLE `summary_lb_userNchannel_week` (
  `user_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `profile_picture` int(11) NOT NULL default '0',
  `raves_channel` int(11) NOT NULL default '0',
  `channels_created` int(11) NOT NULL default '0',
  `friend_accepts` int(11) NOT NULL default '0',
  `raves_media` int(11) NOT NULL default '0',
  `channel_members` int(11) NOT NULL default '0',
  `join_wok` int(11) NOT NULL default '0',
  `photos_uploaded` int(11) NOT NULL default '0',
  `video_uploaded` int(11) NOT NULL default '0',
  `music_uploaded` int(11) NOT NULL default '0',
  `people_invited` int(11) NOT NULL default '0',
  `page_pimped` int(11) NOT NULL default '0',
  `raves_your_profile` int(11) NOT NULL default '0',
  `total_points` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_lb_userNchannel_year` */

CREATE TABLE `summary_lb_userNchannel_year` (
  `user_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `profile_picture` int(11) NOT NULL default '0',
  `raves_channel` int(11) NOT NULL default '0',
  `channels_created` int(11) NOT NULL default '0',
  `friend_accepts` int(11) NOT NULL default '0',
  `raves_media` int(11) NOT NULL default '0',
  `channel_members` int(11) NOT NULL default '0',
  `join_wok` int(11) NOT NULL default '0',
  `photos_uploaded` int(11) NOT NULL default '0',
  `video_uploaded` int(11) NOT NULL default '0',
  `music_uploaded` int(11) NOT NULL default '0',
  `people_invited` int(11) NOT NULL default '0',
  `page_pimped` int(11) NOT NULL default '0',
  `raves_your_profile` int(11) NOT NULL default '0',
  `total_points` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_most_popular` */

CREATE TABLE `summary_most_popular` (
  `count` int(11) default NULL,
  `channel_id` int(11) NOT NULL,
  `username` varchar(80) default NULL,
  `gender` char(1) default NULL,
  `location` varchar(80) default NULL,
  `name_no_spaces` varchar(80) default NULL,
  `thumbnail_small_path` varchar(255) default NULL,
  `thumbnail_medium_path` varchar(255) default NULL,
  PRIMARY KEY  (`channel_id`),
  KEY `c` (`count`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

/*Table structure for table `summary_most_popular_assets` */

CREATE TABLE `summary_most_popular_assets` (
  `count` int(10) unsigned NOT NULL,
  `asset_id` int(10) unsigned NOT NULL auto_increment,
  `asset_type_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`asset_id`),
  KEY `at` (`asset_type_id`),
  KEY `c` (`count`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

/*Table structure for table `summary_new_assets` */

CREATE TABLE `summary_new_assets` (
  `asset_id` int(11) NOT NULL,
  `asset_type_id` int(11) default NULL,
  `name` varchar(80) default NULL,
  `created_date` datetime default NULL,
  `asset_rating_id` int(11) default NULL,
  `publis_status_id` int(11) default NULL,
  `thumbnail_small_path` varchar(255) default NULL,
  `thumbnail_medium_path` varchar(255) default NULL,
  `thumbnail_gen` tinyint(4) default NULL,
  `user_id` int(11) default NULL,
  `username` varchar(80) default NULL,
  `status_id` tinyint(4) default NULL,
  `number_of_downloads` int(11) default NULL,
  `number_of_shares` int(11) default NULL,
  `number_of_comments` int(11) default NULL,
  `number_of_diggs` int(11) default NULL,
  PRIMARY KEY  (`asset_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

/*Table structure for table `summary_new_channels` */

CREATE TABLE `summary_new_channels` (
  `community_id` int(11) NOT NULL,
  `cx` int(11) NOT NULL,
  `community_name` varchar(80) default NULL,
  `name_no_spaces` varchar(80) default NULL,
  `description` varchar(255) default NULL,
  `is_public` char(1) default NULL,
  `is_adult` char(1) default NULL,
  `creator_id` int(11) default NULL,
  `username` varchar(80) default NULL,
  `thumbnail_small_path` varchar(255) default NULL,
  `thumbnail_medium_path` varchar(255) default NULL,
  `location` varchar(80) default NULL,
  `number_of_members` int(11) default NULL,
  `number_of_views` int(11) default NULL,
  `created_date` datetime default NULL,
  `is_personal` tinyint(4) default NULL,
  PRIMARY KEY  (`community_id`),
  KEY `cx` (`cx`),
  KEY `is_personal` (`is_personal`),
  KEY `created_date` (`created_date`),
  KEY `pub` (`is_public`),
  KEY `adult` (`is_adult`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

/*Table structure for table `summary_ppt_by_day` */

CREATE TABLE `summary_ppt_by_day` (
  `user_id` int(10) unsigned NOT NULL,
  `dt_created` date NOT NULL,
  `total` double NOT NULL,
  `transaction_count` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`dt_created`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_ppt_completed_kpoint_transactions_by_day` */

CREATE TABLE `summary_ppt_completed_kpoint_transactions_by_day` (
  `user_id` int(11) NOT NULL default '0',
  `transaction_date` date NOT NULL default '0000-00-00',
  `total` double default NULL,
  PRIMARY KEY  (`user_id`,`transaction_date`),
  KEY `td` (`transaction_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_public_assets_by_type_by_user` */

CREATE TABLE `summary_public_assets_by_type_by_user` (
  `asset_type_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`asset_type_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_signups_by_day` */

CREATE TABLE `summary_signups_by_day` (
  `date_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`date_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `summary_transactions_by_day` */

CREATE TABLE `summary_transactions_by_day` (
  `transaction_date` date NOT NULL default '0000-00-00',
  `payer_count` int(11) NOT NULL default '0',
  `verified_transaction_count` int(11) NOT NULL default '0',
  `failed_transaction_count` int(11) NOT NULL default '0',
  `total_transaction_count` int(11) NOT NULL default '0',
  `purchase_amount_debited` float NOT NULL default '0',
  PRIMARY KEY  (`transaction_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `this_weeks_community_views` */

CREATE TABLE `this_weeks_community_views` (
  `view_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL default '0',
  `community_id` int(10) unsigned NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `anonymous` tinyint(3) unsigned NOT NULL default '0',
  `ip_address` int(11) unsigned default NULL,
  PRIMARY KEY  (`view_id`),
  KEY `cv_hub` (`community_id`),
  KEY `Index_3` (`created_date`),
  KEY `Index_4` (`community_id`,`created_date`)
) ENGINE=InnoDB AUTO_INCREMENT=66273690 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `thread_type` */

CREATE TABLE `thread_type` (
  `thread_type_id` int(11) NOT NULL default '0',
  `name` varchar(20) NOT NULL default '',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`thread_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `threads_base` */

CREATE TABLE `threads_base` (
  `thread_id` int(11) NOT NULL auto_increment,
  `thread_type_id` int(11) NOT NULL default '0',
  `topic_id` int(11) NOT NULL default '0',
  `parent_thread_id` int(11) default NULL,
  `user_id` int(11) NOT NULL default '0',
  `rating` float default NULL,
  `ip_address` varchar(30) default NULL,
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) default NULL,
  `status_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`thread_id`),
  KEY `summary_index` (`topic_id`,`thread_type_id`,`status_id`,`created_date`),
  KEY `thead_statis_topic_user` (`topic_id`,`thread_type_id`,`user_id`,`status_id`),
  KEY `sort_by_update_date` (`last_updated_date`),
  KEY `lu_uid_tid` (`last_updated_user_id`,`topic_id`),
  KEY `cd_tt_uid_sid` (`created_date`,`thread_type_id`,`user_id`,`status_id`),
  KEY `tt_uid_sid` (`thread_type_id`,`user_id`,`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84183 DEFAULT CHARSET=latin1;

/*Table structure for table `topics_base` */

CREATE TABLE `topics_base` (
  `topic_id` int(11) NOT NULL auto_increment,
  `forum_id` int(11) default NULL,
  `community_id` int(11) default NULL,
  `created_user_id` int(11) NOT NULL default '0',
  `sticky` char(1) NOT NULL default '',
  `important` char(1) default NULL,
  `allow_comments` char(1) default NULL,
  `allow_ratings` char(1) default NULL,
  `number_of_replies` int(11) NOT NULL default '0',
  `number_of_views` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_reply_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_thread_id` int(11) unsigned NOT NULL default '0',
  `last_user_id` int(11) unsigned NOT NULL default '0',
  `last_updated_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) default NULL,
  `ip_address` varchar(30) default NULL,
  `poll_id` int(11) default NULL,
  `status_id` int(11) NOT NULL default '0',
  `viewable` enum('Y','N') NOT NULL default 'Y',
  PRIMARY KEY  (`topic_id`),
  KEY `count_community_null` (`status_id`,`created_user_id`,`community_id`),
  KEY `reply_date_sort` (`last_reply_date`),
  KEY `cd` (`created_date`),
  KEY `cuid` (`created_user_id`),
  KEY `luid` (`last_user_id`),
  KEY `ltid` (`last_thread_id`),
  KEY `v` (`viewable`)
) ENGINE=InnoDB AUTO_INCREMENT=70896 DEFAULT CHARSET=latin1;

/*Table structure for table `topics_by_type_by_day` */

CREATE TABLE `topics_by_type_by_day` (
  `user_id` int(10) unsigned NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `torrent_status` */

CREATE TABLE `torrent_status` (
  `status_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `torrents` */

CREATE TABLE `torrents` (
  `torrent_id` int(11) NOT NULL auto_increment,
  `torrent_name` varchar(255) NOT NULL default '',
  `user_id` int(11) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `display_name` varchar(100) NOT NULL default '',
  `info_hash` varchar(40) NOT NULL default '',
  `content_size` bigint(20) unsigned NOT NULL default '0',
  `content_extension` varchar(255) default NULL,
  `secured` int(11) NOT NULL default '0',
  `number_of_seeds` int(11) NOT NULL default '0',
  `short_description` varchar(255) default NULL,
  `asset_type_id` int(11) default NULL,
  `seed_updated_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `target_dir` varchar(255) default NULL,
  `content_hosted` int(11) NOT NULL default '0',
  `torrent_status` int(11) NOT NULL default '0',
  `folder` int(11) NOT NULL default '0',
  `percent_complete` int(11) NOT NULL default '0',
  `initial_info_hash` varchar(40) default NULL,
  PRIMARY KEY  (`torrent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4327 DEFAULT CHARSET=latin1;

/*Table structure for table `transaction_type` */

CREATE TABLE `transaction_type` (
  `transaction_type` smallint(5) unsigned NOT NULL default '1',
  `trans_desc` varchar(80) default NULL,
  `trans_text` varchar(40) NOT NULL,
  PRIMARY KEY  (`transaction_type`),
  KEY `td` (`trans_desc`),
  KEY `tt` (`trans_text`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `upd_summary` */

CREATE TABLE `upd_summary` (
  `user_id` int(10) unsigned NOT NULL,
  `activity_code` int(10) unsigned NOT NULL,
  `date_stamp` date NOT NULL,
  `points_rewarded` int(10) unsigned NOT NULL default '0',
  `channel_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`activity_code`,`channel_id`,`date_stamp`),
  KEY `Index_2` (`user_id`),
  KEY `Index_3` (`activity_code`),
  KEY `Index_4` (`date_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_acquisition_source` */

CREATE TABLE `user_acquisition_source` (
  `user_id` int(11) NOT NULL COMMENT 'FK to users table',
  `acquisition_source_id` int(11) unsigned NOT NULL COMMENT 'FK to acquisistion_source table',
  `full_url` text NOT NULL COMMENT 'complete url of referring location',
  `additional_information` varchar(512) NOT NULL default '' COMMENT 'any additional info about source',
  PRIMARY KEY  (`user_id`,`acquisition_source_id`),
  KEY `IDX_user_id` (`user_id`),
  KEY `IDX_acquisition_source_id` (`acquisition_source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='1to1 between users and acquisition_source';

/*Table structure for table `user_balances` */

CREATE TABLE `user_balances` (
  `user_id` int(11) NOT NULL default '0',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `balance` decimal(12,3) NOT NULL default '0.000',
  PRIMARY KEY  (`user_id`,`kei_point_id`),
  KEY `KID` (`kei_point_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

/*Table structure for table `user_balances_audit_log` */

CREATE TABLE `user_balances_audit_log` (
  `transaction_id` int(11) NOT NULL auto_increment,
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') default NULL,
  `user_id` int(11) NOT NULL default '0',
  `old_balance` float NOT NULL default '0',
  `new_balance` float NOT NULL default '0',
  `difference` float NOT NULL default '0',
  `transaction_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`transaction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Table structure for table `user_connection` */

CREATE TABLE `user_connection` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `channel_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `parent_id` int(11) default NULL,
  `path` varchar(255) NOT NULL default '',
  `depth` int(10) unsigned NOT NULL default '0',
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=latin1;

/*Table structure for table `user_connection_map_data` */

CREATE TABLE `user_connection_map_data` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user_connection_id` int(11) NOT NULL default '0',
  `map_data` text NOT NULL,
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_connection_stats` */

CREATE TABLE `user_connection_stats` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user_connection_id` int(11) NOT NULL default '0',
  `depth` int(11) NOT NULL default '0',
  `count` int(11) NOT NULL default '0',
  `created_datetime` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_editor_downloads` */

CREATE TABLE `user_editor_downloads` (
  `user_id` int(11) NOT NULL default '0',
  `downloaded_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `version` varchar(20) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_interest_category_counts` */

CREATE TABLE `user_interest_category_counts` (
  `user_id` int(10) unsigned NOT NULL,
  `ic_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`user_id`,`ic_id`),
  KEY `ic_id` (`ic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_interests` */

CREATE TABLE `user_interests` (
  `user_id` int(10) unsigned NOT NULL,
  `interest_id` int(10) unsigned NOT NULL,
  `ic_id` int(10) unsigned NOT NULL,
  `z_order` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`interest_id`),
  KEY `ic_id` (`ic_id`),
  KEY `user_id` (`user_id`),
  KEY `iid` (`interest_id`),
  KEY `zo` (`z_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_interests_searchable` */

CREATE TABLE `user_interests_searchable` (
  `user_id` int(10) unsigned NOT NULL,
  `interests` text NOT NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_login_issues` */

CREATE TABLE `user_login_issues` (
  `user_login_issue_id` int(11) NOT NULL auto_increment,
  `ip_address` varchar(50) NOT NULL default '',
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `user_id` int(11) default NULL,
  `description` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`user_login_issue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3349728 DEFAULT CHARSET=latin1;

/*Table structure for table `user_notes` */

CREATE TABLE `user_notes` (
  `note_id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL default '0',
  `note` text NOT NULL,
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `created_user_id` int(11) NOT NULL default '0',
  `wok_transaction_log_id` bigint(20) NOT NULL default '0' COMMENT 'FK to wok_transaction_log table',
  PRIMARY KEY  (`note_id`),
  KEY `user_id_key` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20147 DEFAULT CHARSET=latin1;

/*Table structure for table `user_point_distribution` */

CREATE TABLE `user_point_distribution` (
  `upd_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL,
  `activity_code` int(10) unsigned NOT NULL,
  `points_rewarded` int(10) unsigned NOT NULL default '0',
  `date_stamp` datetime NOT NULL,
  `channel_id` int(10) unsigned NOT NULL default '0',
  `extra` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`upd_id`),
  KEY `Index_2` (`user_id`),
  KEY `Index_3` (`activity_code`),
  KEY `Index_4` (`date_stamp`),
  KEY `Index_5` (`extra`)
) ENGINE=InnoDB AUTO_INCREMENT=20563026 DEFAULT CHARSET=latin1;

/*Table structure for table `user_profile_views` */

CREATE TABLE `user_profile_views` (
  `view_id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL default '0',
  `profile_id` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `anonymous` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`view_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_profiles` */

CREATE TABLE `user_profiles` (
  `user_id` int(11) NOT NULL default '0',
  `title_blast` text,
  `show_favorites` char(1) default 'M',
  `favorite_games` text,
  `favorite_movies` text,
  `favorite_music` text,
  `favorite_books` text,
  `favorite_tv` text,
  `like_to_meet` text,
  `interests` text,
  `show_prof` char(1) default 'M',
  `prof_title` varchar(100) default NULL,
  `prof_position` varchar(100) default NULL,
  `prof_industry` varchar(100) default NULL,
  `prof_company` varchar(100) default NULL,
  `prof_job` varchar(100) default NULL,
  `prof_skills` varchar(255) default NULL,
  `show_org` char(1) default NULL,
  `org_current_assoc` varchar(255) default NULL,
  `org_past_assoc` varchar(255) default NULL,
  `show_store` char(1) NOT NULL default 'M',
  `show_blog` char(1) NOT NULL default 'M',
  `show_games` char(1) NOT NULL default 'M',
  `show_bookmarks` char(1) NOT NULL default 'M',
  `relationship` enum('No Answer','Single','Married','Freaky','In a relationship','Divorced','Swinger') default 'No Answer',
  `orientation` enum('No Answer','Straight','Freak','Bi-sexual','Gay/Lesbian','Not Sure') default 'No Answer',
  `religion` enum('No Answer','Catholic','Christian (Other)','Buddhist','Jewish','Other','Agnostic','Atheist','Protestant','Wiccan','Taoist','Mormon','Scientologist','Muslim','Hindu') default 'No Answer',
  `ethnicity` enum('No Answer','White','Mixed','Asian','Native American','East Indian','Other','Black','Latino/Hispanic','European','Pacific Rim','Middle Eastern') default 'No Answer',
  `children` enum('No Answer','Undecided','One day','I am a parent','Don''t want any') default 'No Answer',
  `education` enum('No Answer','College graduate','Some college','High School','Masters Degree','Dropout','Doctorate','In college','Bachelors Degree') default 'No Answer',
  `income` tinyint(3) unsigned default '0',
  `height_feet` tinyint(4) default NULL,
  `height_inches` tinyint(4) default NULL,
  `smoke` enum('No Answer','Yes','No') default 'No Answer',
  `drink` enum('No Answer','Yes','No') default 'No Answer',
  `im_username_1` varchar(50) default NULL,
  `im_type_1` varchar(10) default 'Yahoo',
  `im_security_1` varchar(11) default NULL,
  `im_username_2` varchar(50) default NULL,
  `im_type_2` varchar(10) default 'MSN',
  `im_security_2` varchar(11) default NULL,
  `im_username_3` varchar(50) default NULL,
  `im_type_3` varchar(10) default 'ICQ',
  `im_security_3` varchar(11) default NULL,
  `im_username_4` varchar(50) default NULL,
  `im_type_4` varchar(10) default 'AIM',
  `im_security_4` varchar(11) default NULL,
  `hobbies` text,
  `favorite_sports` text,
  `favorite_gadgets` text,
  `favorite_fashion` text,
  `favorite_cars` text,
  `favorite_artists` text,
  `clubs` text,
  `favorite_brands` text,
  `role_models` text,
  `favorite_quotes` text,
  `hometown` varchar(100) default NULL,
  PRIMARY KEY  (`user_id`),
  KEY `user_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_schools` */

CREATE TABLE `user_schools` (
  `user_id` int(10) unsigned NOT NULL,
  `school_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`user_id`,`school_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_searches` */

CREATE TABLE `user_searches` (
  `log_id` int(11) NOT NULL auto_increment,
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `search_string` varchar(100) NOT NULL default '',
  `user_id` int(11) NOT NULL default '0',
  `search_type` int(11) NOT NULL default '0',
  PRIMARY KEY  (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39484098 DEFAULT CHARSET=latin1;

/*Table structure for table `user_status` */

CREATE TABLE `user_status` (
  `status_id` int(11) NOT NULL default '0',
  `name` varchar(20) NOT NULL default '',
  `description` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_suspensions` */

CREATE TABLE `user_suspensions` (
  `ban_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL,
  `ban_date_start` datetime NOT NULL,
  `ban_date_end` datetime default NULL,
  PRIMARY KEY  (`ban_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1166 DEFAULT CHARSET=latin1 COMMENT='Suspension Details';

/*Table structure for table `user_upload_descriptions` */

CREATE TABLE `user_upload_descriptions` (
  `description_id` int(10) unsigned NOT NULL auto_increment,
  `module_page_id` int(10) unsigned default NULL,
  `field_description` text NOT NULL,
  `required` tinyint(1) unsigned NOT NULL default '1',
  PRIMARY KEY  (`description_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1792 DEFAULT CHARSET=latin1;

/*Table structure for table `users` */

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL auto_increment,
  `username` varchar(22) NOT NULL,
  `password` varchar(64) NOT NULL default '',
  `salt` varchar(16) NOT NULL default '',
  `role` varchar(32) NOT NULL default '',
  `registration_key` varchar(50) default NULL,
  `account_type` int(11) NOT NULL default '0',
  `master_user_id` int(11) default NULL,
  `first_name` varchar(50) NOT NULL default '',
  `last_name` varchar(50) NOT NULL default '',
  `display_name` varchar(30) NOT NULL,
  `description` text,
  `gender` char(1) NOT NULL default '',
  `show_gender` char(1) default NULL,
  `homepage` varchar(200) default NULL,
  `email` varchar(100) NOT NULL default '',
  `show_email` varchar(11) NOT NULL default 'M',
  `status_id` int(11) NOT NULL default '0',
  `email_status` tinyint(3) NOT NULL default '0',
  `key_value` varchar(20) default NULL,
  `last_login` datetime NOT NULL default '0000-00-00 00:00:00',
  `signup_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `birth_date` date default NULL,
  `show_birth` char(1) default NULL,
  `newsletter` char(1) NOT NULL default '',
  `email_notify` char(1) default NULL,
  `public_profile` char(1) NOT NULL default '',
  `zip_code` varchar(13) NOT NULL default '0',
  `country` char(2) NOT NULL default '99',
  `ip_address` int(10) unsigned default '0',
  `last_ip_address` int(10) unsigned default '0',
  `message_notification` int(11) NOT NULL default '0',
  `show_mature` binary(1) NOT NULL default '0',
  `browse_anonymously` tinyint(4) NOT NULL default '0',
  `show_online` binary(1) NOT NULL default '1',
  `notify_blog_comments` binary(1) NOT NULL default '1',
  `notify_profile_comments` binary(1) NOT NULL default '0',
  `notify_friend_messages` binary(1) NOT NULL default '1',
  `notify_anyone_messages` binary(1) NOT NULL default '1',
  `notify_friend_requests` binary(1) NOT NULL default '1',
  `friends_can_comment` tinyint(4) NOT NULL default '1',
  `everyone_can_comment` tinyint(4) NOT NULL default '1',
  `mature_profile` binary(1) NOT NULL default '0',
  `online` tinyint(1) NOT NULL default '0',
  `second_to_last_login` datetime NOT NULL default '0000-00-00 00:00:00',
  `age` tinyint(4) NOT NULL default '0',
  `location` varchar(80) NOT NULL,
  `own_mod` enum('Y','N') NOT NULL default 'N',
  `active` enum('Y','N') NOT NULL default 'Y',
  `over_21` enum('Y','N') default 'N',
  `ustate` enum('On','Off','InWorld','OnInWorld') NOT NULL default 'Off',
  `wok_player_id` int(11) NOT NULL default '0',
  `_signup_date` int(10) unsigned NOT NULL default '4291869598',
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `blast_show_permissions` int(10) unsigned NOT NULL default '1023',
  `blast_privacy_permissions` int(10) unsigned NOT NULL default '1023',
  `join_source_community_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `email_rule` (`email`),
  KEY `index_gender` (`gender`),
  KEY `index_zipcode` (`zip_code`),
  KEY `username` (`username`),
  KEY `first_last` (`first_name`,`last_name`),
  KEY `country_index` (`country`),
  KEY `u_status` (`status_id`),
  KEY `date_report` (`signup_date`),
  KEY `country_status_id_user_id` (`user_id`,`status_id`,`country`),
  KEY `birth_date` (`birth_date`),
  KEY `age` (`age`),
  KEY `active` (`active`),
  KEY `_signup_date` (`_signup_date`)
) ENGINE=InnoDB AUTO_INCREMENT=4343339 DEFAULT CHARSET=latin1;

/*Table structure for table `users_adult` */

CREATE TABLE `users_adult` (
  `user_id` int(11) NOT NULL default '0',
  `username` varchar(22) NOT NULL,
  `password` varchar(64) NOT NULL default '',
  `salt` varchar(16) NOT NULL default '',
  `role` varchar(32) NOT NULL default '',
  `registration_key` varchar(50) default NULL,
  `account_type` int(11) NOT NULL default '0',
  `master_user_id` int(11) default NULL,
  `first_name` varchar(50) NOT NULL default '',
  `last_name` varchar(50) NOT NULL default '',
  `display_name` varchar(30) NOT NULL,
  `description` text,
  `gender` char(1) NOT NULL default '',
  `show_gender` char(1) default NULL,
  `homepage` varchar(200) default NULL,
  `email` varchar(100) NOT NULL default '',
  `show_email` varchar(11) NOT NULL default 'M',
  `status_id` int(11) NOT NULL default '0',
  `email_status` tinyint(3) NOT NULL default '0',
  `key_value` varchar(20) default NULL,
  `last_login` datetime NOT NULL default '0000-00-00 00:00:00',
  `signup_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `birth_date` date default NULL,
  `show_birth` char(1) default NULL,
  `newsletter` char(1) NOT NULL default '',
  `email_notify` char(1) default NULL,
  `public_profile` char(1) NOT NULL default '',
  `zip_code` varchar(25) default NULL,
  `country` char(2) default NULL,
  `ip_address` int(10) unsigned default '0',
  `last_ip_address` int(10) unsigned default '0',
  `message_notification` int(11) NOT NULL default '0',
  `show_mature` binary(1) NOT NULL default '0',
  `browse_anonymously` tinyint(4) NOT NULL default '0',
  `show_online` binary(1) NOT NULL default '1',
  `notify_blog_comments` binary(1) NOT NULL default '1',
  `notify_profile_comments` binary(1) NOT NULL default '0',
  `notify_friend_messages` binary(1) NOT NULL default '1',
  `notify_anyone_messages` binary(1) NOT NULL default '1',
  `notify_friend_requests` binary(1) NOT NULL default '0',
  `friends_can_comment` tinyint(4) NOT NULL default '1',
  `everyone_can_comment` tinyint(4) NOT NULL default '1',
  `mature_profile` binary(1) NOT NULL default '0',
  `online` tinyint(1) NOT NULL default '0',
  `second_to_last_login` datetime NOT NULL default '0000-00-00 00:00:00',
  `age` tinyint(4) NOT NULL default '0',
  `location` varchar(80) NOT NULL,
  `own_mod` enum('Y','N') NOT NULL default 'N',
  `active` enum('Y','N') NOT NULL default 'Y',
  `over_21` enum('Y','N') default 'N',
  `ustate` enum('On','Off','InWorld','OnInWorld') NOT NULL default 'Off',
  `wok_player_id` int(11) NOT NULL default '0',
  `_signup_date` int(10) unsigned NOT NULL default '4291869598',
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `blast_show_permissions` int(10) unsigned NOT NULL default '1023',
  `blast_privacy_permissions` int(10) unsigned NOT NULL default '1023',
  `join_source_community_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `email_rule` (`email`),
  KEY `index_gender` (`gender`),
  KEY `index_zipcode` (`zip_code`),
  KEY `username` (`username`),
  KEY `first_last` (`first_name`,`last_name`),
  KEY `country_index` (`country`),
  KEY `u_status` (`status_id`),
  KEY `date_report` (`signup_date`),
  KEY `country_status_id_user_id` (`user_id`,`status_id`,`country`),
  KEY `birth_date` (`birth_date`),
  KEY `age` (`age`),
  KEY `active` (`active`),
  KEY `_signup_date` (`_signup_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `users_minor` */

CREATE TABLE `users_minor` (
  `user_id` int(11) NOT NULL default '0',
  `username` varchar(22) NOT NULL,
  `password` varchar(64) NOT NULL default '',
  `salt` varchar(16) NOT NULL default '',
  `role` varchar(32) NOT NULL default '',
  `registration_key` varchar(50) default NULL,
  `account_type` int(11) NOT NULL default '0',
  `master_user_id` int(11) default NULL,
  `first_name` varchar(50) NOT NULL default '',
  `last_name` varchar(50) NOT NULL default '',
  `display_name` varchar(30) NOT NULL,
  `description` text,
  `gender` char(1) NOT NULL default '',
  `show_gender` char(1) default NULL,
  `homepage` varchar(200) default NULL,
  `email` varchar(100) NOT NULL default '',
  `show_email` varchar(11) NOT NULL default 'M',
  `status_id` int(11) NOT NULL default '0',
  `email_status` tinyint(3) NOT NULL default '0',
  `key_value` varchar(20) default NULL,
  `last_login` datetime NOT NULL default '0000-00-00 00:00:00',
  `signup_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `birth_date` date default NULL,
  `show_birth` char(1) default NULL,
  `newsletter` char(1) NOT NULL default '',
  `email_notify` char(1) default NULL,
  `public_profile` char(1) NOT NULL default '',
  `zip_code` varchar(25) default NULL,
  `country` char(2) default NULL,
  `ip_address` int(10) unsigned default '0',
  `last_ip_address` int(10) unsigned default '0',
  `message_notification` int(11) NOT NULL default '0',
  `show_mature` binary(1) NOT NULL default '0',
  `browse_anonymously` tinyint(4) NOT NULL default '0',
  `show_online` binary(1) NOT NULL default '1',
  `notify_blog_comments` binary(1) NOT NULL default '1',
  `notify_profile_comments` binary(1) NOT NULL default '0',
  `notify_friend_messages` binary(1) NOT NULL default '1',
  `notify_anyone_messages` binary(1) NOT NULL default '1',
  `notify_friend_requests` binary(1) NOT NULL default '0',
  `friends_can_comment` tinyint(4) NOT NULL default '1',
  `everyone_can_comment` tinyint(4) NOT NULL default '1',
  `mature_profile` binary(1) NOT NULL default '0',
  `online` tinyint(1) NOT NULL default '0',
  `second_to_last_login` datetime NOT NULL default '0000-00-00 00:00:00',
  `age` tinyint(4) NOT NULL default '0',
  `location` varchar(80) NOT NULL,
  `own_mod` enum('Y','N') NOT NULL default 'N',
  `active` enum('Y','N') NOT NULL default 'Y',
  `over_21` enum('Y','N') default 'N',
  `ustate` enum('On','Off','InWorld','OnInWorld') NOT NULL default 'Off',
  `wok_player_id` int(11) NOT NULL default '0',
  `_signup_date` int(10) unsigned NOT NULL default '4291869598',
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `blast_show_permissions` int(10) unsigned NOT NULL default '1023',
  `blast_privacy_permissions` int(10) unsigned NOT NULL default '1023',
  `join_source_community_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `email_rule` (`email`),
  KEY `index_gender` (`gender`),
  KEY `index_zipcode` (`zip_code`),
  KEY `username` (`username`),
  KEY `first_last` (`first_name`,`last_name`),
  KEY `country_index` (`country`),
  KEY `u_status` (`status_id`),
  KEY `date_report` (`signup_date`),
  KEY `country_status_id_user_id` (`user_id`,`status_id`,`country`),
  KEY `birth_date` (`birth_date`),
  KEY `age` (`age`),
  KEY `active` (`active`),
  KEY `_signup_date` (`_signup_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `users_stats` */

CREATE TABLE `users_stats` (
  `user_id` int(11) NOT NULL auto_increment,
  `number_of_logins` int(11) NOT NULL default '0',
  `number_of_failed_logins` int(11) NOT NULL default '0',
  `number_inbox_messages` int(11) NOT NULL default '0',
  `number_outbox_messages` int(11) NOT NULL default '0',
  `number_forum_posts` int(11) NOT NULL default '0',
  `number_of_friends` int(11) NOT NULL default '0' COMMENT 'total number of friends',
  `number_of_pending` int(11) NOT NULL default '0' COMMENT 'this user''s pending friend req to others',
  `number_of_requests` int(11) NOT NULL default '0' COMMENT 'other user''s req to be this one''s friend',
  `number_of_new_messages` int(11) NOT NULL default '0',
  `number_of_trashed_messages` int(11) NOT NULL default '0',
  `number_of_blogs` int(11) NOT NULL default '0',
  `number_of_comments` int(11) NOT NULL default '0',
  `number_of_asset_diggs` int(11) NOT NULL default '0',
  `i_own` int(11) NOT NULL default '0' COMMENT 'communities',
  `i_moderate` int(11) NOT NULL default '0' COMMENT 'communities',
  `i_belong_to` int(11) NOT NULL default '0' COMMENT 'communities',
  `number_pending_community_requests` int(11) NOT NULL default '0' COMMENT 'pending req to all owned/moderated communities',
  `number_gifts_given` int(11) NOT NULL default '0' COMMENT '3D+2D gifts',
  `number_gifts_received` int(11) NOT NULL default '0',
  `number_gifts_pending` int(11) NOT NULL default '0' COMMENT 'not accepted yet',
  `number_asset_shares` int(10) unsigned NOT NULL default '0',
  `number_commuity_shares` int(10) unsigned NOT NULL default '0',
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `interest_count` int(10) unsigned NOT NULL default '0' COMMENT 'Number of interests a user has',
  `number_invites_sent` int(10) unsigned NOT NULL default '0',
  `number_invites_accepted` int(10) unsigned NOT NULL default '0',
  `number_invites_in_world` int(10) unsigned NOT NULL default '0',
  `number_blasts_sent` int(10) unsigned NOT NULL default '0' COMMENT 'Number of blasts sent',
  `number_friend_invite_rewards` decimal(12,2) NOT NULL default '0.00' COMMENT 'Number of rewards from inviting friends',
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4343339 DEFAULT CHARSET=latin1;

/*Table structure for table `web_health` */

CREATE TABLE `web_health` (
  `dt_taken` datetime NOT NULL,
  `server_name` varchar(50) NOT NULL,
  `uptime` float NOT NULL,
  `processor_time` float NOT NULL,
  `processor_priv_time` float NOT NULL,
  `processor_interrupt_time` float NOT NULL,
  `system_processor_queue_length` float NOT NULL,
  `system_context_switches_sec` float NOT NULL,
  `memory_avail_mbytes` float NOT NULL,
  `memory_page_reads_sec` float NOT NULL,
  `memory_pages_sec` float NOT NULL,
  `server_pool_nonpaged_bytes` float NOT NULL,
  `server_pool_nonpaged_failures` float NOT NULL,
  `server_pool_paged_failures` float NOT NULL,
  `server_pool_nonpaged_peak` float NOT NULL,
  `memory_cache_bytes` float NOT NULL,
  `memory_cache_faults_sec` float NOT NULL,
  `cache_MDL_read_hits_percent` float NOT NULL,
  `disk_queue_length` float NOT NULL,
  `disk_avg_read_queue_length` float NOT NULL,
  `disk_avg_write_queue_length` float NOT NULL,
  `disk_avg_disk_sec_read` float NOT NULL,
  `nic1_bytes_total_sec` float NOT NULL,
  `nic2_byts_total_sec` float NOT NULL,
  `tcp_segments_sent_sec` float NOT NULL,
  `udp_datagrams_sent_sec` float NOT NULL,
  `net_exceptions` float NOT NULL,
  `net_exceptions_sec` float NOT NULL,
  `net_contention_rate_sec` float NOT NULL,
  `net_queue_length` float NOT NULL,
  `net_GC_percent` float NOT NULL,
  `net_requests_sec` float NOT NULL,
  `isapi_requests_sec` float NOT NULL,
  `net_requests_timed_out` float NOT NULL,
  `net_request_execution_time` float NOT NULL,
  `net_cache_total_entries` float NOT NULL,
  `net_cache_hit_ratio` float NOT NULL,
  `net_output_cache_entries` float NOT NULL,
  `net_output_cache_hit_ratio` float NOT NULL,
  `net_output_cache_turnover_rate` float NOT NULL,
  PRIMARY KEY  (`dt_taken`),
  KEY `sn` (`server_name`(15))
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Stores Website Stats';

/*Table structure for table `wok_downloads` */

CREATE TABLE `wok_downloads` (
  `wok_download_id` int(11) unsigned NOT NULL auto_increment,
  `user_id` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL,
  PRIMARY KEY  (`wok_download_id`),
  KEY `u` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1633048 DEFAULT CHARSET=latin1 COMMENT='Store WOK download attempts';

/*Table structure for table `wok_transaction_log` */

CREATE TABLE `wok_transaction_log` (
  `trans_id` bigint(20) NOT NULL auto_increment,
  `created_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL default '0',
  `balance_prev` float NOT NULL default '0',
  `balance` float NOT NULL default '0',
  `transaction_amount` double default '0',
  `kei_point_id` enum('KPOINT','MPOINT','GPOINT','DOLLAR') NOT NULL default 'KPOINT',
  `transaction_type` smallint(5) unsigned NOT NULL default '1',
  PRIMARY KEY  (`trans_id`),
  KEY `user_id` (`user_id`),
  KEY `tamt` (`transaction_amount`)
) ENGINE=InnoDB AUTO_INCREMENT=14931787 DEFAULT CHARSET=latin1;

/*Table structure for table `wok_transaction_log_community_details` */

CREATE TABLE `wok_transaction_log_community_details` (
  `trans_id` int(11) NOT NULL default '0' COMMENT 'FK to transaction table',
  `community_id` int(11) NOT NULL default '0' COMMENT 'FK to communities table',
  `cover_charge_amount` float NOT NULL default '0' COMMENT 'Cover charge amount',
  `charge_time` datetime NOT NULL default '0000-00-00 00:00:00' COMMENT 'Time of transaction',
  PRIMARY KEY  (`trans_id`),
  KEY `ci` (`community_id`),
  KEY `ct` (`charge_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='details table for cover charges paid';

/*Table structure for table `wok_transaction_log_details` */

CREATE TABLE `wok_transaction_log_details` (
  `trans_id` int(11) NOT NULL COMMENT 'FK to transaction table',
  `global_id` int(11) NOT NULL COMMENT 'FK to wok.items table',
  `quantity` int(11) NOT NULL COMMENT 'number of items',
  `use_value` int(11) NOT NULL default '0' COMMENT 'copied from wok.items',
  `name` varchar(255) NOT NULL default '' COMMENT 'copied from wok.items',
  PRIMARY KEY  (`trans_id`),
  KEY `gi` (`global_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='details table for user_balance changes';

/*Table structure for table `zip_codes` */

CREATE TABLE `zip_codes` (
  `zip_code` varchar(13) NOT NULL,
  `latitude` double default NULL,
  `longitude` double default NULL,
  `city` varchar(50) default NULL,
  `state_code` char(2) default NULL,
  `county` varchar(50) default NULL,
  `zip_class` varchar(50) default NULL,
  `number_of_users` int(10) unsigned NOT NULL default '0' COMMENT 'Number of users in this zip code',
  PRIMARY KEY  (`zip_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `zip_codes_7_days` */

CREATE TABLE `zip_codes_7_days` (
  `log_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL,
  `zip_code` varchar(11) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY  USING BTREE (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/* Function  structure for function  `commentBotCheck` */

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`` FUNCTION `commentBotCheck`(_userId  int(11)) RETURNS int(11)
    DETERMINISTIC
BEGIN

	DECLARE _todaysPosts INT(11) DEFAULT 0;

  DECLARE _intialFlagValue INT;

  DECLARE _intervalFlag INT;

  DECLARE _temp INT;



  set _intialFlagValue = 20;

  set  _intervalFlag = 20;



   /* get the number of comments done this 24 hours by the user */

	SELECT comment_count INTO _todaysPosts FROM summary_comments_by_user_by_day WHERE Date(comment_date) = CURDATE() AND user_id = _userId;



   /* first check to see if the intial flag has been hit*/

	 IF _todaysPosts = _intialFlagValue THEN

      /*return true; */

      return 1;

   /* check to see if comments exceed the inital flag */

   ELSEIF _todaysPosts > _intialFlagValue  THEN

     set _temp =  _todaysPosts - _intialFlagValue;

     /* check to see if the interval flag has been hit above the inital flag */

     IF  (_temp %  _intervalFlag) = 0 then

       return 1;

     /* interval flag not reached */

     ELSE

       return -1;

     END IF;

   /* initial flag not hit */

   ELSE

       /*return false; */

       return -1;



	 END IF;

END */$$
DELIMITER ;

/* Function  structure for function  `defaultBroadbandZone` */

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`localhost` FUNCTION `defaultBroadbandZone`() RETURNS int(11)
BEGIN
	return 1610612749; 
END */$$
DELIMITER ;

/* Function  structure for function  `gen_id` */

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`` FUNCTION `gen_id`(_prefix INT, _prefix2 INT) RETURNS varchar(64) CHARSET latin1
BEGIN

return CONCAT( hex(_prefix), '-', hex(_prefix2),'-',  hex(FLOOR( (rand() * UNIX_TIMESTAMP(NOW())) * (RAND() * (1048576*2)))));



END */$$
DELIMITER ;

/* Function  structure for function  `gen_uid` */

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`` FUNCTION `gen_uid`() RETURNS varchar(64) CHARSET latin1
BEGIN

  return gen_id(rand()*1048576,rand()*1048576);

END */$$
DELIMITER ;

/* Function  structure for function  `interest_exists` */

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`` FUNCTION `interest_exists`(__ic_id INT , __interest varchar(255)) RETURNS int(11)
BEGIN
  
  DECLARE __interest_id INTEGER;
  
  Select interest_id into __interest_id from interests WHERE ic_id = __ic_id AND interest = __interest;
  if (__interest_id IS NULL) THEN
    return 0;
  END IF;
  return __interest_id;
  
END */$$
DELIMITER ;

/* Function  structure for function  `master_date` */

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`` FUNCTION `master_date`() RETURNS bigint(20)
BEGIN

  -- This equals Select UNIX_TIMESTAMP('2037-12-31 23:59:59')*2;

	return 4291869598;

END */$$
DELIMITER ;

/* Function  structure for function  `reverse_max` */

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`` FUNCTION `reverse_max`() RETURNS int(11)
BEGIN

  return 2147483647;

END */$$
DELIMITER ;

/* Function  structure for function  `school_exists` */

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`` FUNCTION `school_exists`(__name varchar(80)) RETURNS int(11)
BEGIN
  DECLARE __school_id INTEGER;
  
  Select school_id into __school_id from schools WHERE name=__name;
  if (__school_id IS NULL) THEN
    return 0;
  END IF;
  return __school_id;
END */$$
DELIMITER ;

/*Table structure for table `blogs` */

DROP TABLE IF EXISTS `blogs`;

/*!50001 CREATE TABLE `blogs` (
  `blog_id` int(11) NOT NULL default '0',
  `community_id` int(11) default NULL,
  `subject` varchar(100) NOT NULL default '',
  `body_text` text,
  `created_user_id` int(11) NOT NULL default '0',
  `number_of_comments` int(11) NOT NULL default '0',
  `number_of_views` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) default NULL,
  `ip_address` varchar(30) default NULL,
  `status_id` int(11) NOT NULL default '0',
  `keywords` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1 */;

/*Table structure for table `communities_personal` */

DROP TABLE IF EXISTS `communities_personal`;

/*!50001 CREATE TABLE `communities_personal` (
  `community_id` int(11) NOT NULL default '0',
  `category_id` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `url` varchar(50) default NULL,
  `creator_id` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_edit` datetime NOT NULL default '0000-00-00 00:00:00',
  `status_id` int(11) NOT NULL default '0',
  `email` varchar(100) default NULL,
  `percent_royalty_paid` int(11) default NULL,
  `is_public` char(1) NOT NULL default '',
  `is_adult` char(1) default NULL,
  `show_server_uptime` char(1) default NULL,
  `show_server_ip` char(1) default NULL,
  `thumbnail_path` varchar(255) default NULL,
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `thumbnail_medium_path` varchar(255) NOT NULL default '',
  `thumbnail_large_path` varchar(255) NOT NULL default '',
  `thumbnail_xlarge_path` varchar(255) NOT NULL default '',
  `thumbnail_type` varchar(50) default NULL,
  `db_connection_string` varchar(200) default NULL,
  `allow_publishing` tinyint(4) NOT NULL default '0',
  `allow_member_events` tinyint(3) unsigned NOT NULL default '0',
  `is_personal` tinyint(3) unsigned NOT NULL default '0',
  `last_update` datetime NOT NULL default '0000-00-00 00:00:00',
  `template_id` int(10) unsigned NOT NULL default '0',
  `over_21_required` enum('Y','N') default NULL,
  `_created_date` int(10) unsigned NOT NULL default '0',
  `has_thumbnail` enum('Y','N') NOT NULL default 'Y',
  `name` varchar(100) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `creator_username` varchar(22) NOT NULL,
  `keywords` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1 */;

/*Table structure for table `communities_public` */

DROP TABLE IF EXISTS `communities_public`;

/*!50001 CREATE TABLE `communities_public` (
  `community_id` int(11) NOT NULL default '0',
  `category_id` int(11) NOT NULL default '0',
  `name_no_spaces` varchar(100) NOT NULL default '',
  `url` varchar(50) default NULL,
  `creator_id` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_edit` datetime NOT NULL default '0000-00-00 00:00:00',
  `status_id` int(11) NOT NULL default '0',
  `email` varchar(100) default NULL,
  `percent_royalty_paid` int(11) default NULL,
  `is_public` char(1) NOT NULL default '',
  `is_adult` char(1) default NULL,
  `show_server_uptime` char(1) default NULL,
  `show_server_ip` char(1) default NULL,
  `thumbnail_path` varchar(255) default NULL,
  `thumbnail_small_path` varchar(255) NOT NULL default '',
  `thumbnail_medium_path` varchar(255) NOT NULL default '',
  `thumbnail_large_path` varchar(255) NOT NULL default '',
  `thumbnail_xlarge_path` varchar(255) NOT NULL default '',
  `thumbnail_type` varchar(50) default NULL,
  `db_connection_string` varchar(200) default NULL,
  `allow_publishing` tinyint(4) NOT NULL default '0',
  `allow_member_events` tinyint(3) unsigned NOT NULL default '0',
  `is_personal` tinyint(3) unsigned NOT NULL default '0',
  `last_update` datetime NOT NULL default '0000-00-00 00:00:00',
  `template_id` int(10) unsigned NOT NULL default '0',
  `over_21_required` enum('Y','N') default NULL,
  `_created_date` int(10) unsigned NOT NULL default '0',
  `has_thumbnail` enum('Y','N') NOT NULL default 'Y',
  `name` varchar(100) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `creator_username` varchar(22) NOT NULL,
  `keywords` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1 */;

/*Table structure for table `kaneva_playlist_counts` */

DROP TABLE IF EXISTS `kaneva_playlist_counts`;

/*!50001 CREATE TABLE `kaneva_playlist_counts` (
  `asset_group_id` int(11) NOT NULL default '0',
  `name` varchar(15) character set utf8 NOT NULL default '',
  `asset_count` bigint(21) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 */;

/*Table structure for table `threads` */

DROP TABLE IF EXISTS `threads`;

/*!50001 CREATE TABLE `threads` (
  `thread_id` int(11) NOT NULL default '0',
  `thread_type_id` int(11) NOT NULL default '0',
  `topic_id` int(11) NOT NULL default '0',
  `parent_thread_id` int(11) default NULL,
  `user_id` int(11) NOT NULL default '0',
  `rating` float default NULL,
  `ip_address` varchar(30) default NULL,
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) default NULL,
  `status_id` int(11) NOT NULL default '0',
  `subject` varchar(100) default NULL,
  `body_text` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1 */;

/*Table structure for table `topics` */

DROP TABLE IF EXISTS `topics`;

/*!50001 CREATE TABLE `topics` (
  `topic_id` int(11) NOT NULL default '0',
  `forum_id` int(11) default NULL,
  `community_id` int(11) default NULL,
  `created_user_id` int(11) NOT NULL default '0',
  `sticky` char(1) NOT NULL default '',
  `important` char(1) default NULL,
  `allow_comments` char(1) default NULL,
  `allow_ratings` char(1) default NULL,
  `number_of_replies` int(11) NOT NULL default '0',
  `number_of_views` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_reply_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_thread_id` int(11) unsigned NOT NULL default '0',
  `last_user_id` int(11) unsigned NOT NULL default '0',
  `last_updated_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) default NULL,
  `ip_address` varchar(30) default NULL,
  `poll_id` int(11) default NULL,
  `status_id` int(11) NOT NULL default '0',
  `viewable` enum('Y','N') NOT NULL default 'Y',
  `created_username` varchar(22) NOT NULL,
  `subject` varchar(100) NOT NULL default '',
  `body_text` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1 */;

/*Table structure for table `vw_published_assets` */

DROP TABLE IF EXISTS `vw_published_assets`;

/*!50001 CREATE TABLE `vw_published_assets` (
  `asset_id` int(11) NOT NULL default '0',
  `asset_type_id` int(11) NOT NULL default '0',
  `is_kaneva_game` binary(1) NOT NULL default '\0',
  `asset_sub_type_id` int(11) NOT NULL default '0',
  `asset_rating_id` int(11) NOT NULL default '0',
  `owner_id` int(11) NOT NULL default '0',
  `file_size` bigint(20) unsigned NOT NULL default '0',
  `run_time_seconds` int(11) NOT NULL default '0',
  `category1_id` int(11) NOT NULL default '0',
  `category2_id` int(11) NOT NULL default '0',
  `category3_id` int(11) NOT NULL default '0',
  `amount` decimal(12,2) NOT NULL default '0.00',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `image_path` varchar(255) default NULL,
  `image_full_path` varchar(255) default NULL,
  `thumbnail_assetdetails_path` varchar(255) NOT NULL,
  `thumbnail_xlarge_path` varchar(255) NOT NULL,
  `thumbnail_large_path` varchar(255) NOT NULL,
  `thumbnail_medium_path` varchar(255) NOT NULL,
  `thumbnail_small_path` varchar(255) NOT NULL,
  `thumbnail_gen` tinyint(2) NOT NULL default '0',
  `image_caption` varchar(100) default NULL,
  `image_type` varchar(50) default NULL,
  `status_id` int(11) NOT NULL default '0',
  `created_date` datetime default NULL,
  `last_updated_date` datetime default NULL,
  `last_updated_user_id` int(11) default NULL,
  `game_exe` varchar(100) default NULL,
  `torrent_id` int(11) default NULL,
  `license_cc` varchar(100) NOT NULL default '',
  `license_URL` varchar(200) default NULL,
  `license_type` char(1) NOT NULL default '',
  `license_name` varchar(50) default NULL,
  `license_additional` varchar(100) default NULL,
  `publish_status_id` int(11) NOT NULL default '0',
  `percent_complete` int(11) default NULL,
  `ip_address` varchar(30) default NULL,
  `game_encryption_key` varchar(16) default NULL,
  `icon_image_data` longblob,
  `icon_image_path` varchar(100) default NULL,
  `icon_image_type` varchar(50) default NULL,
  `require_login` smallint(6) NOT NULL default '0',
  `content_extension` varchar(255) default NULL,
  `target_dir` varchar(255) default NULL,
  `media_path` varchar(255) default NULL,
  `permission` int(11) NOT NULL default '0',
  `permission_group` int(11) default NULL,
  `published` enum('Y','N') NOT NULL default 'Y',
  `mature` enum('Y','N') NOT NULL default 'Y',
  `public` enum('Y','N') NOT NULL default 'Y',
  `playlistable` enum('Y','N') NOT NULL default 'Y',
  `over_21_required` enum('Y','N') default NULL,
  `asset_offsite_id` varchar(1000) NOT NULL default '',
  `sort_order` int(10) unsigned NOT NULL default '0',
  `last_update` timestamp NOT NULL default '0000-00-00 00:00:00',
  `name` varchar(100) NOT NULL default '',
  `body_text` text,
  `short_description` varchar(50) default NULL,
  `teaser` text,
  `owner_username` varchar(22) NOT NULL,
  `keywords` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1 */;

/*Table structure for table `vw_published_mature_assets` */

DROP TABLE IF EXISTS `vw_published_mature_assets`;

/*!50001 CREATE TABLE `vw_published_mature_assets` (
  `asset_id` int(11) NOT NULL default '0',
  `asset_type_id` int(11) NOT NULL default '0',
  `is_kaneva_game` binary(1) NOT NULL default '\0',
  `asset_sub_type_id` int(11) NOT NULL default '0',
  `asset_rating_id` int(11) NOT NULL default '0',
  `owner_id` int(11) NOT NULL default '0',
  `file_size` bigint(20) unsigned NOT NULL default '0',
  `run_time_seconds` int(11) NOT NULL default '0',
  `category1_id` int(11) NOT NULL default '0',
  `category2_id` int(11) NOT NULL default '0',
  `category3_id` int(11) NOT NULL default '0',
  `amount` decimal(12,2) NOT NULL default '0.00',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `image_path` varchar(255) default NULL,
  `image_full_path` varchar(255) default NULL,
  `thumbnail_assetdetails_path` varchar(255) NOT NULL,
  `thumbnail_xlarge_path` varchar(255) NOT NULL,
  `thumbnail_large_path` varchar(255) NOT NULL,
  `thumbnail_medium_path` varchar(255) NOT NULL,
  `thumbnail_small_path` varchar(255) NOT NULL,
  `thumbnail_gen` tinyint(2) NOT NULL default '0',
  `image_caption` varchar(100) default NULL,
  `image_type` varchar(50) default NULL,
  `status_id` int(11) NOT NULL default '0',
  `created_date` datetime default NULL,
  `last_updated_date` datetime default NULL,
  `last_updated_user_id` int(11) default NULL,
  `game_exe` varchar(100) default NULL,
  `torrent_id` int(11) default NULL,
  `license_cc` varchar(100) NOT NULL default '',
  `license_URL` varchar(200) default NULL,
  `license_type` char(1) NOT NULL default '',
  `license_name` varchar(50) default NULL,
  `license_additional` varchar(100) default NULL,
  `publish_status_id` int(11) NOT NULL default '0',
  `percent_complete` int(11) default NULL,
  `ip_address` varchar(30) default NULL,
  `game_encryption_key` varchar(16) default NULL,
  `icon_image_data` longblob,
  `icon_image_path` varchar(100) default NULL,
  `icon_image_type` varchar(50) default NULL,
  `require_login` smallint(6) NOT NULL default '0',
  `content_extension` varchar(255) default NULL,
  `target_dir` varchar(255) default NULL,
  `media_path` varchar(255) default NULL,
  `permission` int(11) NOT NULL default '0',
  `permission_group` int(11) default NULL,
  `published` enum('Y','N') NOT NULL default 'Y',
  `mature` enum('Y','N') NOT NULL default 'Y',
  `public` enum('Y','N') NOT NULL default 'Y',
  `playlistable` enum('Y','N') NOT NULL default 'Y',
  `over_21_required` enum('Y','N') default NULL,
  `asset_offsite_id` varchar(1000) NOT NULL default '',
  `sort_order` int(10) unsigned NOT NULL default '0',
  `last_update` timestamp NOT NULL default '0000-00-00 00:00:00',
  `name` varchar(100) NOT NULL default '',
  `body_text` text,
  `short_description` varchar(50) default NULL,
  `teaser` text,
  `owner_username` varchar(22) NOT NULL,
  `keywords` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1 */;

/*Table structure for table `vw_published_public_assets` */

DROP TABLE IF EXISTS `vw_published_public_assets`;

/*!50001 CREATE TABLE `vw_published_public_assets` (
  `asset_id` int(11) NOT NULL default '0',
  `asset_type_id` int(11) NOT NULL default '0',
  `is_kaneva_game` binary(1) NOT NULL default '\0',
  `asset_sub_type_id` int(11) NOT NULL default '0',
  `asset_rating_id` int(11) NOT NULL default '0',
  `owner_id` int(11) NOT NULL default '0',
  `file_size` bigint(20) unsigned NOT NULL default '0',
  `run_time_seconds` int(11) NOT NULL default '0',
  `category1_id` int(11) NOT NULL default '0',
  `category2_id` int(11) NOT NULL default '0',
  `category3_id` int(11) NOT NULL default '0',
  `amount` decimal(12,2) NOT NULL default '0.00',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `image_path` varchar(255) default NULL,
  `image_full_path` varchar(255) default NULL,
  `thumbnail_assetdetails_path` varchar(255) NOT NULL,
  `thumbnail_xlarge_path` varchar(255) NOT NULL,
  `thumbnail_large_path` varchar(255) NOT NULL,
  `thumbnail_medium_path` varchar(255) NOT NULL,
  `thumbnail_small_path` varchar(255) NOT NULL,
  `thumbnail_gen` tinyint(2) NOT NULL default '0',
  `image_caption` varchar(100) default NULL,
  `image_type` varchar(50) default NULL,
  `status_id` int(11) NOT NULL default '0',
  `created_date` datetime default NULL,
  `last_updated_date` datetime default NULL,
  `last_updated_user_id` int(11) default NULL,
  `game_exe` varchar(100) default NULL,
  `torrent_id` int(11) default NULL,
  `license_cc` varchar(100) NOT NULL default '',
  `license_URL` varchar(200) default NULL,
  `license_type` char(1) NOT NULL default '',
  `license_name` varchar(50) default NULL,
  `license_additional` varchar(100) default NULL,
  `publish_status_id` int(11) NOT NULL default '0',
  `percent_complete` int(11) default NULL,
  `ip_address` varchar(30) default NULL,
  `game_encryption_key` varchar(16) default NULL,
  `icon_image_data` longblob,
  `icon_image_path` varchar(100) default NULL,
  `icon_image_type` varchar(50) default NULL,
  `require_login` smallint(6) NOT NULL default '0',
  `content_extension` varchar(255) default NULL,
  `target_dir` varchar(255) default NULL,
  `media_path` varchar(255) default NULL,
  `permission` int(11) NOT NULL default '0',
  `permission_group` int(11) default NULL,
  `published` enum('Y','N') NOT NULL default 'Y',
  `mature` enum('Y','N') NOT NULL default 'Y',
  `public` enum('Y','N') NOT NULL default 'Y',
  `playlistable` enum('Y','N') NOT NULL default 'Y',
  `over_21_required` enum('Y','N') default NULL,
  `asset_offsite_id` varchar(1000) NOT NULL default '',
  `sort_order` int(10) unsigned NOT NULL default '0',
  `last_update` timestamp NOT NULL default '0000-00-00 00:00:00',
  `name` varchar(100) NOT NULL default '',
  `body_text` text,
  `short_description` varchar(50) default NULL,
  `teaser` text,
  `owner_username` varchar(22) NOT NULL,
  `keywords` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1 */;

/*Table structure for table `vw_published_public_mature_assets` */

DROP TABLE IF EXISTS `vw_published_public_mature_assets`;

/*!50001 CREATE TABLE `vw_published_public_mature_assets` (
  `asset_id` int(11) NOT NULL default '0',
  `asset_type_id` int(11) NOT NULL default '0',
  `is_kaneva_game` binary(1) NOT NULL default '\0',
  `asset_sub_type_id` int(11) NOT NULL default '0',
  `asset_rating_id` int(11) NOT NULL default '0',
  `owner_id` int(11) NOT NULL default '0',
  `file_size` bigint(20) unsigned NOT NULL default '0',
  `run_time_seconds` int(11) NOT NULL default '0',
  `category1_id` int(11) NOT NULL default '0',
  `category2_id` int(11) NOT NULL default '0',
  `category3_id` int(11) NOT NULL default '0',
  `amount` decimal(12,2) NOT NULL default '0.00',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  `image_path` varchar(255) default NULL,
  `image_full_path` varchar(255) default NULL,
  `thumbnail_assetdetails_path` varchar(255) NOT NULL,
  `thumbnail_xlarge_path` varchar(255) NOT NULL,
  `thumbnail_large_path` varchar(255) NOT NULL,
  `thumbnail_medium_path` varchar(255) NOT NULL,
  `thumbnail_small_path` varchar(255) NOT NULL,
  `thumbnail_gen` tinyint(2) NOT NULL default '0',
  `image_caption` varchar(100) default NULL,
  `image_type` varchar(50) default NULL,
  `status_id` int(11) NOT NULL default '0',
  `created_date` datetime default NULL,
  `last_updated_date` datetime default NULL,
  `last_updated_user_id` int(11) default NULL,
  `game_exe` varchar(100) default NULL,
  `torrent_id` int(11) default NULL,
  `license_cc` varchar(100) NOT NULL default '',
  `license_URL` varchar(200) default NULL,
  `license_type` char(1) NOT NULL default '',
  `license_name` varchar(50) default NULL,
  `license_additional` varchar(100) default NULL,
  `publish_status_id` int(11) NOT NULL default '0',
  `percent_complete` int(11) default NULL,
  `ip_address` varchar(30) default NULL,
  `game_encryption_key` varchar(16) default NULL,
  `icon_image_data` longblob,
  `icon_image_path` varchar(100) default NULL,
  `icon_image_type` varchar(50) default NULL,
  `require_login` smallint(6) NOT NULL default '0',
  `content_extension` varchar(255) default NULL,
  `target_dir` varchar(255) default NULL,
  `media_path` varchar(255) default NULL,
  `permission` int(11) NOT NULL default '0',
  `permission_group` int(11) default NULL,
  `published` enum('Y','N') NOT NULL default 'Y',
  `mature` enum('Y','N') NOT NULL default 'Y',
  `public` enum('Y','N') NOT NULL default 'Y',
  `playlistable` enum('Y','N') NOT NULL default 'Y',
  `over_21_required` enum('Y','N') default NULL,
  `asset_offsite_id` varchar(1000) NOT NULL default '',
  `sort_order` int(10) unsigned NOT NULL default '0',
  `last_update` timestamp NOT NULL default '0000-00-00 00:00:00',
  `name` varchar(100) NOT NULL default '',
  `body_text` text,
  `short_description` varchar(50) default NULL,
  `teaser` text,
  `owner_username` varchar(22) NOT NULL,
  `keywords` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1 */;

/*View structure for view blogs */

/*!50001 DROP TABLE IF EXISTS `blogs` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`` SQL SECURITY DEFINER VIEW `blogs` 
AS 
select `b`.`blog_id` AS `blog_id`,`b`.`community_id` AS `community_id`,`b`.`subject` AS `subject`,`b`.`body_text` AS `body_text`,
`b`.`created_user_id` AS `created_user_id`,`b`.`number_of_comments` AS `number_of_comments`,`b`.`number_of_views` AS `number_of_views`,
`b`.`created_date` AS `created_date`,`b`.`last_updated_date` AS `last_updated_date`,`b`.`last_updated_user_id` AS `last_updated_user_id`,
`b`.`ip_address` AS `ip_address`,`b`.`status_id` AS `status_id`,`ftb`.`keywords` AS `keywords` from (`blogs_base` `b` join `my_kaneva`.
`ft_blogs` `ftb` on((`b`.`blog_id` = `ftb`.`blog_id`))) */;

/*View structure for view communities_personal */

/*!50001 DROP TABLE IF EXISTS `communities_personal` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`%` SQL SECURITY DEFINER VIEW `communities_personal` 
AS 
select `com`.`community_id` AS `community_id`,`com`.`category_id` AS `category_id`,`com`.`name_no_spaces` AS `name_no_spaces`,
`com`.`url` AS `url`,`com`.`creator_id` AS `creator_id`,`com`.`created_date` AS `created_date`,`com`.`last_edit` AS `last_edit`,
`com`.`status_id` AS `status_id`,`com`.`email` AS `email`,`com`.`percent_royalty_paid` AS `percent_royalty_paid`,
`com`.`is_public` AS `is_public`,`com`.`is_adult` AS `is_adult`,`com`.`show_server_uptime` AS `show_server_uptime`,
`com`.`show_server_ip` AS `show_server_ip`,`com`.`thumbnail_path` AS `thumbnail_path`,`com`.`thumbnail_small_path` AS `thumbnail_small_path`,
`com`.`thumbnail_medium_path` AS `thumbnail_medium_path`,`com`.`thumbnail_large_path` AS `thumbnail_large_path`,
`com`.`thumbnail_xlarge_path` AS `thumbnail_xlarge_path`,`com`.`thumbnail_type` AS `thumbnail_type`,`com`.
`db_connection_string` AS `db_connection_string`,`com`.`allow_publishing` AS `allow_publishing`,`com`.
`allow_member_events` AS `allow_member_events`,`com`.`is_personal` AS `is_personal`,`com`.`last_update` AS `last_update`,
`com`.`template_id` AS `template_id`,`com`.`over_21_required` AS `over_21_required`,`com`.`_created_date` AS `_created_date`,
`com`.`has_thumbnail` AS `has_thumbnail`,`com`.`name` AS `name`,`com`.`description` AS `description`,`com`.`creator_username` AS `creator_username`,
`com`.`keywords` AS `keywords` from `communities` `com` where (`com`.`is_personal` = 1) */;

/*View structure for view communities_public */

/*!50001 DROP TABLE IF EXISTS `communities_public` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`%` SQL SECURITY DEFINER VIEW `communities_public` 
AS 
select `com`.`community_id` AS `community_id`,`com`.`category_id` AS `category_id`,`com`.`name_no_spaces` AS `name_no_spaces`,
`com`.`url` AS `url`,`com`.`creator_id` AS `creator_id`,`com`.`created_date` AS `created_date`,`com`.`last_edit` AS `last_edit`,
`com`.`status_id` AS `status_id`,`com`.`email` AS `email`,`com`.`percent_royalty_paid` AS `percent_royalty_paid`,`com`.
`is_public` AS `is_public`,`com`.`is_adult` AS `is_adult`,`com`.`show_server_uptime` AS `show_server_uptime`,
`com`.`show_server_ip` AS `show_server_ip`,`com`.`thumbnail_path` AS `thumbnail_path`,`com`.`thumbnail_small_path` AS `thumbnail_small_path`,
`com`.`thumbnail_medium_path` AS `thumbnail_medium_path`,`com`.`thumbnail_large_path` AS `thumbnail_large_path`,
`com`.`thumbnail_xlarge_path` AS `thumbnail_xlarge_path`,`com`.`thumbnail_type` AS `thumbnail_type`,
`com`.`db_connection_string` AS `db_connection_string`,`com`.`allow_publishing` AS `allow_publishing`,
`com`.`allow_member_events` AS `allow_member_events`,`com`.`is_personal` AS `is_personal`,`com`.`last_update` AS `last_update`,
`com`.`template_id` AS `template_id`,`com`.`over_21_required` AS `over_21_required`,`com`.`_created_date` AS `_created_date`,
`com`.`has_thumbnail` AS `has_thumbnail`,`com`.`name` AS `name`,`com`.`description` AS `description`,`com`.`creator_username` AS `creator_username`,
`com`.`keywords` AS `keywords` from `communities` `com` where (`com`.`is_personal` = 0) */;

/*View structure for view kaneva_playlist_counts */

/*!50001 DROP TABLE IF EXISTS `kaneva_playlist_counts` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`localhost` SQL SECURITY DEFINER VIEW `kaneva_playlist_counts` 
AS 
select sql_no_cache `featured_assets`.`playlist_type` AS `asset_group_id`,
(case `featured_assets`.`playlist_type` when -(1) then _utf8'Cool items' when -(4) then _utf8'Home items' when -(5) then _utf8'Member items' 
when -(6) then _utf8'Broadband items' when -(7) then _utf8'Media items' else _utf8'Unknown' end) AS `name`,
count(0) AS `asset_count` 
from `featured_assets` 
where ((`featured_assets`.`start_datetime` < now()) and (`featured_assets`.`end_datetime` > now())) group by 1,2 */;

/*View structure for view threads */

/*!50001 DROP TABLE IF EXISTS `threads` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`` SQL SECURITY DEFINER VIEW `threads` 
AS 
select `t`.`thread_id` AS `thread_id`,`t`.`thread_type_id` AS `thread_type_id`,`t`.`topic_id` AS `topic_id`,
`t`.`parent_thread_id` AS `parent_thread_id`,`t`.`user_id` AS `user_id`,`t`.`rating` AS `rating`,`t`.`ip_address` AS `ip_address`,
`t`.`created_date` AS `created_date`,`t`.`last_updated_date` AS `last_updated_date`,`t`.`last_updated_user_id` AS `last_updated_user_id`,
`t`.`status_id` AS `status_id`,`ftt`.`subject` AS `subject`,`ftt`.`body_text` AS `body_text` 
from (`threads_base` `t` join `my_kaneva`.`ft_threads` `ftt` on((`t`.`thread_id` = `ftt`.`thread_id`))) */;

/*View structure for view topics */

/*!50001 DROP TABLE IF EXISTS `topics` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`localhost` SQL SECURITY DEFINER VIEW `topics` 
AS 
select `t`.`topic_id` AS `topic_id`,`t`.`forum_id` AS `forum_id`,`t`.`community_id` AS `community_id`,`t`.`created_user_id` AS `created_user_id`,
`t`.`sticky` AS `sticky`,`t`.`important` AS `important`,`t`.`allow_comments` AS `allow_comments`,`t`.`allow_ratings` AS `allow_ratings`,
`t`.`number_of_replies` AS `number_of_replies`,`t`.`number_of_views` AS `number_of_views`,`t`.`created_date` AS `created_date`,
`t`.`last_reply_date` AS `last_reply_date`,`t`.`last_thread_id` AS `last_thread_id`,`t`.`last_user_id` AS `last_user_id`,
`t`.`last_updated_date` AS `last_updated_date`,`t`.`last_updated_user_id` AS `last_updated_user_id`,`t`.`ip_address` AS `ip_address`,
`t`.`poll_id` AS `poll_id`,`t`.`status_id` AS `status_id`,`t`.`viewable` AS `viewable`,`ftt`.`created_username` AS `created_username`,
`ftt`.`subject` AS `subject`,`ftt`.`body_text` AS `body_text` 
from (`topics_base` `t` join `my_kaneva`.`ft_topics` `ftt` on((`t`.`topic_id` = `ftt`.`topic_id`))) */;

/*View structure for view vw_published_assets */

/*!50001 DROP TABLE IF EXISTS `vw_published_assets` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`%` SQL SECURITY DEFINER VIEW `vw_published_assets` 
AS 
select `a`.`asset_id` AS `asset_id`,`a`.`asset_type_id` AS `asset_type_id`,`a`.`is_kaneva_game` AS `is_kaneva_game`,
`a`.`asset_sub_type_id` AS `asset_sub_type_id`,`a`.`asset_rating_id` AS `asset_rating_id`,`a`.`owner_id` AS `owner_id`,
`a`.`file_size` AS `file_size`,`a`.`run_time_seconds` AS `run_time_seconds`,`a`.`category1_id` AS `category1_id`,
`a`.`category2_id` AS `category2_id`,`a`.`category3_id` AS `category3_id`,`a`.`amount` AS `amount`,`a`.`kei_point_id` AS `kei_point_id`,
`a`.`image_path` AS `image_path`,`a`.`image_full_path` AS `image_full_path`,`a`.`thumbnail_assetdetails_path` AS `thumbnail_assetdetails_path`,
`a`.`thumbnail_xlarge_path` AS `thumbnail_xlarge_path`,`a`.`thumbnail_large_path` AS `thumbnail_large_path`,
`a`.`thumbnail_medium_path` AS `thumbnail_medium_path`,`a`.`thumbnail_small_path` AS `thumbnail_small_path`,
`a`.`thumbnail_gen` AS `thumbnail_gen`,`a`.`image_caption` AS `image_caption`,`a`.`image_type` AS `image_type`,
`a`.`status_id` AS `status_id`,`a`.`created_date` AS `created_date`,`a`.`last_updated_date` AS `last_updated_date`,
`a`.`last_updated_user_id` AS `last_updated_user_id`,`a`.`game_exe` AS `game_exe`,`a`.`torrent_id` AS `torrent_id`,
`a`.`license_cc` AS `license_cc`,`a`.`license_URL` AS `license_URL`,`a`.`license_type` AS `license_type`,
`a`.`license_name` AS `license_name`,`a`.`license_additional` AS `license_additional`,`a`.`publish_status_id` AS `publish_status_id`,
`a`.`percent_complete` AS `percent_complete`,`a`.`ip_address` AS `ip_address`,`a`.`game_encryption_key` AS `game_encryption_key`,
`a`.`icon_image_data` AS `icon_image_data`,`a`.`icon_image_path` AS `icon_image_path`,`a`.`icon_image_type` AS `icon_image_type`,
`a`.`require_login` AS `require_login`,`a`.`content_extension` AS `content_extension`,`a`.`target_dir` AS `target_dir`,
`a`.`media_path` AS `media_path`,`a`.`permission` AS `permission`,`a`.`permission_group` AS `permission_group`,`a`.`published` AS `published`,
`a`.`mature` AS `mature`,`a`.`public` AS `public`,`a`.`playlistable` AS `playlistable`,`a`.`over_21_required` AS `over_21_required`,
`a`.`asset_offsite_id` AS `asset_offsite_id`,`a`.`sort_order` AS `sort_order`,`a`.`last_update` AS `last_update`,`a`.`name` AS `name`,
`a`.`body_text` AS `body_text`,`a`.`short_description` AS `short_description`,`a`.`teaser` AS `teaser`,`a`.`owner_username` AS `owner_username`,
`a`.`keywords` AS `keywords` 
from `assets` `a` where ((`a`.`published` = _latin1'Y') and (`a`.`mature` = _latin1'N')) */;

/*View structure for view vw_published_mature_assets */

/*!50001 DROP TABLE IF EXISTS `vw_published_mature_assets` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`%` SQL SECURITY DEFINER VIEW `vw_published_mature_assets` 
AS 
select `a`.`asset_id` AS `asset_id`,`a`.`asset_type_id` AS `asset_type_id`,`a`.`is_kaneva_game` AS `is_kaneva_game`,
`a`.`asset_sub_type_id` AS `asset_sub_type_id`,`a`.`asset_rating_id` AS `asset_rating_id`,`a`.`owner_id` AS `owner_id`,
`a`.`file_size` AS `file_size`,`a`.`run_time_seconds` AS `run_time_seconds`,`a`.`category1_id` AS `category1_id`,
`a`.`category2_id` AS `category2_id`,`a`.`category3_id` AS `category3_id`,`a`.`amount` AS `amount`,
`a`.`kei_point_id` AS `kei_point_id`,`a`.`image_path` AS `image_path`,`a`.`image_full_path` AS `image_full_path`,
`a`.`thumbnail_assetdetails_path` AS `thumbnail_assetdetails_path`,`a`.`thumbnail_xlarge_path` AS `thumbnail_xlarge_path`,
`a`.`thumbnail_large_path` AS `thumbnail_large_path`,`a`.`thumbnail_medium_path` AS `thumbnail_medium_path`,
`a`.`thumbnail_small_path` AS `thumbnail_small_path`,`a`.`thumbnail_gen` AS `thumbnail_gen`,
`a`.`image_caption` AS `image_caption`,`a`.`image_type` AS `image_type`,`a`.`status_id` AS `status_id`,
`a`.`created_date` AS `created_date`,`a`.`last_updated_date` AS `last_updated_date`,`a`.`last_updated_user_id` AS `last_updated_user_id`,
`a`.`game_exe` AS `game_exe`,`a`.`torrent_id` AS `torrent_id`,`a`.`license_cc` AS `license_cc`,`a`.`license_URL` AS `license_URL`,
`a`.`license_type` AS `license_type`,`a`.`license_name` AS `license_name`,`a`.`license_additional` AS `license_additional`,
`a`.`publish_status_id` AS `publish_status_id`,`a`.`percent_complete` AS `percent_complete`,`a`.`ip_address` AS `ip_address`,
`a`.`game_encryption_key` AS `game_encryption_key`,`a`.`icon_image_data` AS `icon_image_data`,`a`.`icon_image_path` AS `icon_image_path`,
`a`.`icon_image_type` AS `icon_image_type`,`a`.`require_login` AS `require_login`,`a`.`content_extension` AS `content_extension`,
`a`.`target_dir` AS `target_dir`,`a`.`media_path` AS `media_path`,`a`.`permission` AS `permission`,`a`.`permission_group` AS `permission_group`,
`a`.`published` AS `published`,`a`.`mature` AS `mature`,`a`.`public` AS `public`,`a`.`playlistable` AS `playlistable`,
`a`.`over_21_required` AS `over_21_required`,`a`.`asset_offsite_id` AS `asset_offsite_id`,`a`.`sort_order` AS `sort_order`,
`a`.`last_update` AS `last_update`,`a`.`name` AS `name`,`a`.`body_text` AS `body_text`,`a`.`short_description` AS `short_description`,
`a`.`teaser` AS `teaser`,`a`.`owner_username` AS `owner_username`,`a`.`keywords` AS `keywords` 
from `assets` `a` where (`a`.`published` = _latin1'Y') */;

/*View structure for view vw_published_public_assets */

/*!50001 DROP TABLE IF EXISTS `vw_published_public_assets` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`%` SQL SECURITY DEFINER VIEW `vw_published_public_assets` 
AS 
select `a`.`asset_id` AS `asset_id`,`a`.`asset_type_id` AS `asset_type_id`,`a`.`is_kaneva_game` AS `is_kaneva_game`,
`a`.`asset_sub_type_id` AS `asset_sub_type_id`,`a`.`asset_rating_id` AS `asset_rating_id`,`a`.`owner_id` AS `owner_id`,
`a`.`file_size` AS `file_size`,`a`.`run_time_seconds` AS `run_time_seconds`,`a`.`category1_id` AS `category1_id`,
`a`.`category2_id` AS `category2_id`,`a`.`category3_id` AS `category3_id`,`a`.`amount` AS `amount`,`a`.`kei_point_id` AS `kei_point_id`,
`a`.`image_path` AS `image_path`,`a`.`image_full_path` AS `image_full_path`,`a`.`thumbnail_assetdetails_path` AS `thumbnail_assetdetails_path`,
`a`.`thumbnail_xlarge_path` AS `thumbnail_xlarge_path`,`a`.`thumbnail_large_path` AS `thumbnail_large_path`,
`a`.`thumbnail_medium_path` AS `thumbnail_medium_path`,`a`.`thumbnail_small_path` AS `thumbnail_small_path`,
`a`.`thumbnail_gen` AS `thumbnail_gen`,`a`.`image_caption` AS `image_caption`,`a`.`image_type` AS `image_type`,
`a`.`status_id` AS `status_id`,`a`.`created_date` AS `created_date`,`a`.`last_updated_date` AS `last_updated_date`,
`a`.`last_updated_user_id` AS `last_updated_user_id`,`a`.`game_exe` AS `game_exe`,`a`.`torrent_id` AS `torrent_id`,
`a`.`license_cc` AS `license_cc`,`a`.`license_URL` AS `license_URL`,`a`.`license_type` AS `license_type`,
`a`.`license_name` AS `license_name`,`a`.`license_additional` AS `license_additional`,`a`.`publish_status_id` AS `publish_status_id`,
`a`.`percent_complete` AS `percent_complete`,`a`.`ip_address` AS `ip_address`,`a`.`game_encryption_key` AS `game_encryption_key`,
`a`.`icon_image_data` AS `icon_image_data`,`a`.`icon_image_path` AS `icon_image_path`,`a`.`icon_image_type` AS `icon_image_type`,
`a`.`require_login` AS `require_login`,`a`.`content_extension` AS `content_extension`,`a`.`target_dir` AS `target_dir`,
`a`.`media_path` AS `media_path`,`a`.`permission` AS `permission`,`a`.`permission_group` AS `permission_group`,
`a`.`published` AS `published`,`a`.`mature` AS `mature`,`a`.`public` AS `public`,`a`.`playlistable` AS `playlistable`,
`a`.`over_21_required` AS `over_21_required`,`a`.`asset_offsite_id` AS `asset_offsite_id`,`a`.`sort_order` AS `sort_order`,
`a`.`last_update` AS `last_update`,`a`.`name` AS `name`,`a`.`body_text` AS `body_text`,`a`.`short_description` AS `short_description`,
`a`.`teaser` AS `teaser`,`a`.`owner_username` AS `owner_username`,`a`.`keywords` AS `keywords` 
from `assets` `a` where ((`a`.`published` = _latin1'Y') and (`a`.`mature` = _latin1'N') and (`a`.`public` = _latin1'Y')) */;

/*View structure for view vw_published_public_mature_assets */

/*!50001 DROP TABLE IF EXISTS `vw_published_public_mature_assets` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`%` SQL SECURITY DEFINER VIEW `vw_published_public_mature_assets` 
AS 
select `a`.`asset_id` AS `asset_id`,`a`.`asset_type_id` AS `asset_type_id`,`a`.`is_kaneva_game` AS `is_kaneva_game`,
`a`.`asset_sub_type_id` AS `asset_sub_type_id`,`a`.`asset_rating_id` AS `asset_rating_id`,`a`.`owner_id` AS `owner_id`,
`a`.`file_size` AS `file_size`,`a`.`run_time_seconds` AS `run_time_seconds`,`a`.`category1_id` AS `category1_id`,
`a`.`category2_id` AS `category2_id`,`a`.`category3_id` AS `category3_id`,`a`.`amount` AS `amount`,`a`.`kei_point_id` AS `kei_point_id`,
`a`.`image_path` AS `image_path`,`a`.`image_full_path` AS `image_full_path`,`a`.`thumbnail_assetdetails_path` AS `thumbnail_assetdetails_path`,
`a`.`thumbnail_xlarge_path` AS `thumbnail_xlarge_path`,`a`.`thumbnail_large_path` AS `thumbnail_large_path`,
`a`.`thumbnail_medium_path` AS `thumbnail_medium_path`,`a`.`thumbnail_small_path` AS `thumbnail_small_path`,
`a`.`thumbnail_gen` AS `thumbnail_gen`,`a`.`image_caption` AS `image_caption`,`a`.`image_type` AS `image_type`,
`a`.`status_id` AS `status_id`,`a`.`created_date` AS `created_date`,`a`.`last_updated_date` AS `last_updated_date`,
`a`.`last_updated_user_id` AS `last_updated_user_id`,`a`.`game_exe` AS `game_exe`,`a`.`torrent_id` AS `torrent_id`,
`a`.`license_cc` AS `license_cc`,`a`.`license_URL` AS `license_URL`,`a`.`license_type` AS `license_type`,`a`.`license_name` AS `license_name`,
`a`.`license_additional` AS `license_additional`,`a`.`publish_status_id` AS `publish_status_id`,`a`.`percent_complete` AS `percent_complete`,
`a`.`ip_address` AS `ip_address`,`a`.`game_encryption_key` AS `game_encryption_key`,`a`.`icon_image_data` AS `icon_image_data`,
`a`.`icon_image_path` AS `icon_image_path`,`a`.`icon_image_type` AS `icon_image_type`,`a`.`require_login` AS `require_login`,
`a`.`content_extension` AS `content_extension`,`a`.`target_dir` AS `target_dir`,`a`.`media_path` AS `media_path`,`a`.`permission` AS `permission`,
`a`.`permission_group` AS `permission_group`,`a`.`published` AS `published`,`a`.`mature` AS `mature`,`a`.`public` AS `public`,
`a`.`playlistable` AS `playlistable`,`a`.`over_21_required` AS `over_21_required`,`a`.`asset_offsite_id` AS `asset_offsite_id`,
`a`.`sort_order` AS `sort_order`,`a`.`last_update` AS `last_update`,`a`.`name` AS `name`,`a`.`body_text` AS `body_text`,
`a`.`short_description` AS `short_description`,`a`.`teaser` AS `teaser`,`a`.`owner_username` AS `owner_username`,`a`.`keywords` AS `keywords` 
from `assets` `a` where ((`a`.`published` = _latin1'Y') and (`a`.`public` = _latin1'Y')) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
