-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS insert_email_bounce_log;

DELIMITER ;;
CREATE PROCEDURE `insert_email_bounce_log`( 
		IN __bounce_source VARCHAR(255),
		IN __bounce_type VARCHAR(5),
		IN __email_address VARCHAR(255),
		IN __pop_account VARCHAR(50)
		)
BEGIN

DECLARE _bounce_type_id TINYINT;
DECLARE _bounce_source_id TINYINT;

SELECT bounce_type_id INTO _bounce_type_id FROM bounce_type WHERE bounce_type_code = __bounce_type;
SELECT bounce_source_id INTO _bounce_source_id FROM bounce_source WHERE description = __bounce_source;

INSERT   INTO email_bounce_log
	(bounce_source_id, bounce_type_id, email_address, pop_account, added_at)
	VALUES (_bounce_source_id, _bounce_type_id,__email_address, COALESCE(__pop_account,''), NOW());
	
END
;;
DELIMITER ;