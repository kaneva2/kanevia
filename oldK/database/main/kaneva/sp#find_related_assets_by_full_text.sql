-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS find_related_assets_by_full_text;

DELIMITER ;;
CREATE PROCEDURE `find_related_assets_by_full_text`(IN _base_asset_id INT, IN _keywords TEXT)
BEGIN
  DECLARE done INT DEFAULT 0;
  DECLARE _related_asset_id INT;
  DEClARE _asset_rating_id INT;
  DECLARE _o DOUBLE;
  DECLARE _related_assets CURSOR for Select rand() as o, sar.asset_id, sar.asset_rating_id from search_kaneva.searchable_assets sa Inner Join search_kaneva.searchable_assets_results sar on sa.asset_id = sar.asset_id where MATCH (sa.keywords) AGAINST (_keywords) AND sa.asset_id <> _base_asset_id  AND sa.asset_type_id=2 order by o limit 500;
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
  SET done = 0;
  OPEN _related_assets;
 
  REPEAT
    FETCH  _related_assets into _o, _related_asset_id, _asset_rating_id;
    if (_related_asset_id IS NOT NULL) THEN
      if (_asset_rating_id IN (3,6,9)) THEN
        Replace into __related_items_temp VALUES(_o, _base_asset_id, _related_asset_id, 'Y');
      ELSE
        Replace into __related_items_temp VALUES(_o, _base_asset_id, _related_asset_id, 'N');
      END IF;
    END IF;
  UNTIL done END REPEAT;
END
;;
DELIMITER ;