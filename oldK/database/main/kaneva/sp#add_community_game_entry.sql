-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_community_game_entry;

DELIMITER ;;
CREATE PROCEDURE `add_community_game_entry`(communityId INTEGER SIGNED, gameId INTEGER SIGNED)
BEGIN
            
    
    INSERT INTO communities_game 
    (
	    community_id, 
	    game_id                
    )
    VALUES 
    (
	    communityId, 
	    gameId
    );

END
;;
DELIMITER ;