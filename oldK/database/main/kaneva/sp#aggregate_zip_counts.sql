-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS aggregate_zip_counts;

DELIMITER ;;
CREATE PROCEDURE `aggregate_zip_counts`()
BEGIN
  DELETE from kaneva.zip_codes_7_days where date_created <= DATE_SUB(NOW(), INTERVAL 7 DAY);

  update zip_codes zc
  Inner Join (
    Select count(log_id) as zip_count, zip_code from zip_codes_7_days group by zip_code
  )X ON zc.zip_code = X.zip_code
  SET zc.number_of_users = X.zip_count;

END
;;
DELIMITER ;