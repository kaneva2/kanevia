-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS threads;

DELIMITER ;;

 CREATE VIEW `kaneva`.`threads` AS SELECT `t`.`thread_id` AS `thread_id`,`t`.`thread_type_id` AS `thread_type_id`,`t`.`topic_id` AS `topic_id`,`t`.`parent_thread_id` AS `parent_thread_id`,`t`.`user_id` AS `user_id`,`t`.`rating` AS `rating`,`t`.`ip_address` AS `ip_address`,`t`.`created_date` AS `created_date`,`t`.`last_updated_date` AS `last_updated_date`,`t`.`last_updated_user_id` AS `last_updated_user_id`,`t`.`status_id` AS `status_id`,`ftt`.`subject` AS `subject`,`ftt`.`body_text` AS `body_text` 
 FROM ( `kaneva`.`threads_base` `t` 
 JOIN `my_kaneva`.`ft_threads` `ftt` on( ( `t`.`thread_id` = `ftt`.`thread_id`) ) ) 
;;
DELIMITER ;