-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_search_asset;

DELIMITER ;;
CREATE PROCEDURE `delete_search_asset`(assetId int)
begin
end
;;
DELIMITER ;