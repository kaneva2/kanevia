-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS assign_interest_to_user;

DELIMITER ;;
CREATE PROCEDURE `assign_interest_to_user`(IN __user_id INTEGER, IN __interest_id INTEGER, IN __ic_id INT, OUT __return_val INTEGER)
BEGIN
  DECLARE __ass_exists INT;
	DECLARE __existing_count INT;
-- assign an existing interest to an existing user
-- return value is -1 if its already in the table
-- return value is 0 if it is successful
  SELECT count(user_id) INTO __ass_exists from kaneva.user_interests WHERE user_id = __user_id AND interest_id = __interest_id;
  if (__ass_exists > 0) THEN
    SET __return_val = -1; -- Already in the table
  ELSE
    Select count(`count`) into __existing_count FROM kaneva.user_interest_category_counts WHERE user_id = __user_id AND ic_id = __ic_id;
    
    -- The default z_order for a given interest is the current count+1 of the interests they have for that category...
    Insert into kaneva.user_interests (user_id, interest_id, ic_id, z_order) VALUES (__user_id, __interest_id, __ic_id, (IFNULL(__existing_count+1,1)) );
		
		-- Update the counts
    IF (__existing_count IS NULL) OR (__existing_count = 0) THEN
      Insert into kaneva.user_interest_category_counts VALUES(__user_id,__ic_id, 0);
		END IF;
    update kaneva.user_interest_category_counts SET `count`=`count` + 1 WHERE  user_id = __user_id AND ic_id = __ic_id;
		update kaneva.users_stats SET `interest_count`=`interest_count` + 1 WHERE  user_id = __user_id;
		
		SET __return_val = 0; -- success
  END IF;
END
;;
DELIMITER ;