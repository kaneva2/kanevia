-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_thumbnail_path;

DELIMITER ;;
CREATE PROCEDURE `update_assets_thumbnail_path`(IN __asset_id INT, 
	IN __column ENUM('AD','XL','L','M','S'), IN __thumbnail_path VARCHAR(255))
BEGIN

IF  __column = 'AD' THEN 
	UPDATE assets SET thumbnail_assetdetails_path = __thumbnail_path WHERE asset_id = __asset_id;
	ELSEIF __column = 'S' THEN
	UPDATE assets SET thumbnail_small_path = __thumbnail_path WHERE asset_id = __asset_id;
	ELSEIF __column = 'M' THEN
	UPDATE assets SET thumbnail_medium_path = __thumbnail_path WHERE asset_id = __asset_id;
	ELSEIF __column = 'L' THEN
	UPDATE assets SET thumbnail_large_path = __thumbnail_path WHERE asset_id = __asset_id;
	ELSEIF __column = 'XL' THEN
	UPDATE assets SET thumbnail_xlarge_path = __thumbnail_path WHERE asset_id = __asset_id;
	END IF;	

END
;;
DELIMITER ;