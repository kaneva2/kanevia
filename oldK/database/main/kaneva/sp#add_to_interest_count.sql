-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_to_interest_count;

DELIMITER ;;
CREATE PROCEDURE `add_to_interest_count`(IN __interest_id INT)
BEGIN
  update kaneva.interests SET user_count=user_count + 1 WHERE interest_id=__interest_id;
END
;;
DELIMITER ;