-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_kaneva_game;

DELIMITER ;;
CREATE PROCEDURE `update_assets_kaneva_game`(IN __asset_id INT, IN __is_kaneva_game BINARY(1))
BEGIN

UPDATE assets 
	SET is_kaneva_game = __is_kaneva_game
	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;