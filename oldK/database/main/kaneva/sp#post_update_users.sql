-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS post_update_users;

DELIMITER ;;
CREATE PROCEDURE `post_update_users`(IN __new_user_id INT(11), __new_user_name VARCHAR(22),
					__old_user_name VARCHAR(22), __new_display_name VARCHAR(30),
					__old_display_name VARCHAR(30), __new_zip_code VARCHAR(13),
					__old_zip_code VARCHAR(13), __new_country CHAR(2), __old_country CHAR(2),
					__new_location VARCHAR(80), __old_location VARCHAR(80))
BEGIN
-- called from the update trigger on community_members
	  INSERT into kaneva.audit_users
    	(user_id,username,username_old,display_name,display_name_old,zip_code,zip_code_old,
        country,country_old,location,location_old,date_modified)
     	VALUES
    	(__new_user_id, __new_user_name, __old_user_name, __new_display_name, __old_display_name,
		__new_zip_code, __old_zip_code, __new_country, __old_country, __new_location, __old_location, NOW());
				
END
;;
DELIMITER ;