-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS delete_threads_base;

DELIMITER ;;
CREATE TRIGGER `delete_threads_base` BEFORE DELETE ON `threads_base` FOR EACH ROW BEGIN
    Insert into my_kaneva.arch_threads_base Select * from threads_base where thread_id = old.thread_id;
    DELETE from my_kaneva.ft_threads where thread_id = old.thread_id;
    UPDATE topics SET number_of_replies = (number_of_replies - 1) WHERE topic_id = old.topic_id;
END
;;
DELIMITER ;