-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_cat_rating;

DELIMITER ;;
CREATE PROCEDURE `update_assets_cat_rating`(IN __asset_id_list TINYTEXT, IN __asset_type_id INT,
		IN __category1_id INT, IN __category2_id INT, IN __category3_id INT,
		IN __asset_rating_id INT, IN __last_updated_user_id INT, IN __permission INT)
BEGIN
DECLARE _old_status_id INT;
DECLARE _old_asset_type_id INT;
DECLARE _old_permission INT;

DECLARE _new_mature ENUM('Y','N');
DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE _asset_id INT;
DECLARE asset_cursor CURSOR FOR 
		SELECT asset_id FROM	assets 	WHERE asset_id IN (__asset_id_list);
			
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

SET done = 0;
SET i = 0;

OPEN asset_cursor;

REPEAT
	FETCH asset_cursor INTO _asset_id;
	IF NOT done THEN SET i=i+1;
	
	SELECT status_id, asset_type_id, permission
		INTO _old_status_id, _old_asset_type_id, _old_permission
		FROM assets WHERE asset_id = _asset_id;

	SET _new_mature       = IF(( (__asset_rating_id=3) | (__asset_rating_id=6) | (__asset_rating_id=9) ),'Y','N');
	

	UPDATE assets 
		SET last_updated_date = NOW(),
		last_updated_user_id = __last_updated_user_id,
		category1_id = __category1_id,
		category2_id = __category2_id,
		category3_id = __category3_id,
		asset_rating_id = __asset_rating_id,
		mature = _new_mature,
		public = CASE WHEN __permission IS NULL THEN public ELSE IF (__permission <> 2,'Y','N') END,
		playlistable = CASE WHEN __asset_type_id IS NULL THEN playlistable ELSE IF(( (__asset_type_id=2) | (__asset_type_id=4) ),'Y','N') END,
		asset_type_id = CASE WHEN __asset_type_id IS NULL THEN asset_type_id ELSE __asset_type_id END,
		permission = CASE WHEN __permission IS NULL THEN permission ELSE __permission END
		WHERE asset_id = _asset_id;

	  
	IF (__permission IS NOT NULL) THEN
		IF ( ((__permission =2) OR (__permission = 3) ) AND ((_old_permission <> 2) AND (_old_permission <> 3)) ) THEN

			UPDATE summary_assets_by_type SET `count`=CASE WHEN `count`>0 THEN `count`-1 ELSE 0 END WHERE asset_type_id = _old_asset_type_id;

			CALL decrement_communities_assets_counts_by_asset_and_type(_asset_id,_old_asset_type_id);
			    
			IF (__permission<>2) AND (__permission <>3) THEN
				UPDATE summary_assets_by_type_by_user SET `count`=CASE WHEN `count`>0 THEN `count`-1 ELSE 0 END WHERE user_id= __owner_id AND asset_type_id = __asset_type_id;
			END IF;
			    
		ELSEIF ( ((__permission <>2) AND (__permission <> 3) ) AND ((_old_permission = 2) OR (_old_permission = 3)) ) THEN
					
			UPDATE summary_assets_by_type SET `count`=`count`+1 WHERE asset_type_id = _old_asset_type_id;
		    
			IF (__permission=2) OR (__permission=3) THEN
				UPDATE summary_assets_by_type_by_user SET `count`=`count`+ 1 WHERE user_id= __owner_id AND asset_type_id = __asset_type_id;
			END IF;
		END IF;
	END IF;

		  
	IF (__asset_type_id IS NOT NULL) THEN
		IF (_old_asset_type_id <> __asset_type_id) THEN
			UPDATE summary_assets_by_type SET `count`=`count`-1 WHERE asset_type_id = _old_asset_type_id;
			UPDATE summary_assets_by_type SET `count`=`count`+1 WHERE asset_type_id=__asset_type_id;
			UPDATE summary_assets_by_type_by_user SET `count`=`count`-1 WHERE asset_type_id=_old_asset_type_id AND user_id=_old_owner_id;
			UPDATE summary_assets_by_type_by_user SET `count`=`count`+1 WHERE asset_type_id=__asset_type_id AND user_id=__owner_id;

			CALL decrement_communities_assets_counts_by_asset_and_type(_asset_id,_old_asset_type_id);
			CALL increment_communities_assets_counts_by_asset_and_type(_asset_id,__asset_type_id);
		END IF;
	END IF;

END IF;
UNTIL done END REPEAT;
CLOSE asset_cursor;


END
;;
DELIMITER ;