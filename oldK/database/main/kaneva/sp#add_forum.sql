-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_forum;

DELIMITER ;;
CREATE PROCEDURE `add_forum`(_communityId INTEGER SIGNED, _forumName VARCHAR(100), _description VARCHAR(255), _forumCategoryId INTEGER SIGNED)
BEGIN

DECLARE _displayOrder INT DEFAULT 0;
DECLARE _forumStatus_Active INT DEFAULT 1;

  START TRANSACTION;

	
	SELECT 
		COALESCE(MAX(display_order)+1,1) INTO _displayOrder
	FROM 
		forums  
	WHERE 
		community_id = _communityId AND
		forum_category_id = _forumCategoryId;
		
		
    	
	INSERT INTO forums 
	(
	    forum_category_id,
	    community_id,
	    forum_name,
	    description,
	    number_of_topics,
	    created_date,
	    last_post_date,
	    display_order,
	    status_id
	)
	VALUES 
	(
	    _forumCategoryId,
	    _communityId,
	    _forumName,
	    _description,
	    0,
	    Now(),
	    Now(),
	    _displayOrder,
	    _forumStatus_Active
	);
    
    
    SELECT LAST_INSERT_ID();

  COMMIT;

END
;;
DELIMITER ;