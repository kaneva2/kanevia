-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_user_account;

DELIMITER ;;
CREATE PROCEDURE `update_user_account`( IN _user_id INT(11), 
		IN _first_name VARCHAR(50), IN _last_name VARCHAR(50), IN _email VARCHAR(100))
BEGIN 

DECLARE _old_birth_date DATE; 
DECLARE _old_email VARCHAR(100); 

DECLARE _new_age TINYINT(4);
DECLARE _new_over_21 ENUM('Y','N');

SELECT birth_date, email
	INTO _old_birth_date, _old_email 
	FROM users
	WHERE user_id = _user_id
	FOR UPDATE;

SET _new_age = DATE_FORMAT(FROM_DAYS(TO_DAYS(CONCAT(DATE(NOW()),' 23:59:59'))-TO_DAYS(_old_birth_date)), '%Y')+0;
SET _new_over_21 = IF(_new_age>=21, 'Y','N');

UPDATE users
	SET first_name = COALESCE(_first_name, first_name), 
		last_name = COALESCE(_last_name, last_name), 
		email = COALESCE(_email, email),
		age = COALESCE(_new_age, age),
		over_21 = COALESCE(_new_over_21, over_21)
	WHERE user_id = _user_id;

IF (_old_email <> _email AND _email IS NOT NULL) THEN
	REPLACE INTO users_email (user_id, email_hash) VALUES (_user_id, UNHEX(MD5(_email)));
END IF;
IF (_old_email <> _email AND _email IS NULL) THEN
	DELETE FROM users_email WHERE user_id = _user_id;
END IF;

END
;;
DELIMITER ;