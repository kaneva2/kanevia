-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS copy_data;

DELIMITER ;;
CREATE PROCEDURE `copy_data`(table_ VARCHAR(100), column_ VARCHAR(100), operation TINYINT)
BEGIN 
 
  /* create constants for comparison */
  declare _INSERTS TINYINT default 0;
  declare _UPDATES TINYINT default 1;
  
  declare _tableInHistory TINYINT default 0;
  declare _plannedMaxId BIGINT default 0;
  declare _plannedModDate DATETIME default Now();
 
  declare _copyHistoryId INTEGER default 0;
  declare _tableName VARCHAR(100) default '';
  declare _projectedIdCopied BIGINT default 0;
  declare _projectedModifiedDate DATETIME default Now();
  declare _lastIdCopied BIGINT default 0;
  declare _lastModifiedDate DATETIME default SUBDATE(Now(),1);
 
  -- make check to see if the table name entry is already in the data copy history
  SELECT COUNT(table_name) INTO _tableInHistory FROM data_copy_history WHERE table_name = table_ ;
 
  -- get data for the table if the table already has a record
  IF _tableInHistory > 0 THEN
   -- now see if this is a loop for the same day or a new insert of an existing table for the day
   SELECT COUNT(table_name) INTO _tableInHistory FROM data_copy_history WHERE table_name = table_ AND DATE(last_modified_date) = DATE(Now());
 
 -- if there is no record of this table for today add one as a copy of the last record
 IF _tableInHistory <= 0 THEN
 
  INSERT INTO
   data_copy_history (table_name, projected_id_copied, projected_modified_date, last_id_copied, last_modified_date)
  SELECT 
   table_name, projected_id_copied, Now() as projected_modified_date, last_id_copied, Now() as last_modified_date
  FROM
   data_copy_history
  WHERE
   table_name = table_
  ORDER BY 
   copy_history_id DESC LIMIT 0,1;    
 END IF;
 
 -- get the data for todays copy for the indicated table
 SELECT 
  copy_history_id, table_name, projected_id_copied, projected_modified_date, last_id_copied, last_modified_date
 INTO 
  _copyHistoryId, _tableName, _projectedIdCopied, _projectedModifiedDate, _lastIdCopied, _lastModifiedDate
 FROM
  data_copy_history
 WHERE
  table_name = table_
 AND 
  DATE(last_modified_date) = DATE(Now());
 
  ELSE 
       -- insert a new copy data record into the table
       INSERT INTO
   data_copy_history (table_name, projected_id_copied, projected_modified_date, last_id_copied, last_modified_date)
       VALUES (table_, _projectedIdCopied, _projectedModifiedDate, _lastIdCopied, _lastModifiedDate);
 
       SELECT LAST_INSERT_ID() INTO _copyHistoryId;
  END IF;
 
  -- if this is for new inserts pull data by id and return
  IF operation = _INSERTS THEN
 
    -- get the highest id at this moment
    SET @_stmt := CONCAT("SELECT MAX(",column_, ") INTO @_plannedMaxId FROM ", table_, " WHERE ", column_, " > ", _lastIdCopied, ";");
    PREPARE __stmt FROM @_stmt;
    EXECUTE __stmt;
    
   
    IF @_plannedMaxId IS NOT NULL THEN
    
 -- set the data retrieval size
 IF (@_plannedMaxId - _lastIdCopied) > 1000 THEN
 
  -- get the max id of the next set of 10000
     SET @_stmt := CONCAT("SELECT ",column_, " FROM ", table_, " WHERE ", column_, " > ", _lastIdCopied, " ORDER BY ", column_, " ASC LIMIT 0,1000");
     SET @_stmt := CONCAT("SELECT MAX(x.",column_, ") INTO @_plannedMaxId FROM (", @_stmt, ") x");
  PREPARE __stmt FROM @_stmt;
  EXECUTE __stmt;
 
 END IF;    
 
     -- update the data copy history table with planned id
     UPDATE data_copy_history SET projected_id_copied = @_plannedMaxId, projected_modified_date = _plannedModDate  WHERE copy_history_id = _copyHistoryId;
    ELSE
         -- Set the planned id to the last copied id
         SET @_plannedMaxId = _lastIdCopied;
     -- update the data copy history table with planned id
     UPDATE data_copy_history SET projected_id_copied = @_plannedMaxId WHERE copy_history_id = _copyHistoryId;
    END IF;
    
    -- return the data set
    SET @_stmt := CONCAT("SELECT * FROM ", table_, " WHERE ", column_, " > ", _lastIdCopied, " AND ", column_, " <= ", @_plannedMaxId, ";");
    PREPARE __stmt FROM @_stmt;
    EXECUTE __stmt;
  
  
  
    DEALLOCATE PREPARE __stmt;
 
  -- if this is for modifed items pull by date  
  ELSEIF operation = _UPDATES THEN
   
    -- update the data copy history table with planned id
    UPDATE data_copy_history SET projected_modified_date = _plannedModDate WHERE copy_history_id = _copyHistoryId;
 
    -- return the data set
    SET @_stmt := CONCAT("SELECT * FROM ", table_, " WHERE ", column_, " > '", _lastModifiedDate, "' AND ", column_, " <= '", _plannedModDate, "';");
    PREPARE __stmt FROM @_stmt;
    EXECUTE __stmt;
 
    DEALLOCATE PREPARE __stmt;
 
  END IF;
  
END
;;
DELIMITER ;