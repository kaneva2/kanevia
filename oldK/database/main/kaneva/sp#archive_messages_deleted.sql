-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS archive_messages_deleted;

DELIMITER ;;
CREATE PROCEDURE `archive_messages_deleted`(__archive_before_date	DATETIME, __archive_cutoff_time DATETIME)
BEGIN

DECLARE	_message_id		INT;
DECLARE _from_id INT;
DECLARE _to_id INT;
DECLARE _subject VARCHAR(100);
DECLARE _message TEXT;
DECLARE _message_date DATETIME;
DECLARE _replied SMALLINT;
DECLARE _type TINYINT;
DECLARE _channel_id INT;
DECLARE _to_viewable ENUM('U','R','D');
DECLARE _from_viewable ENUM('S','D');
DECLARE __sql_statement VARCHAR(512);

DECLARE	__table_RowCnt		INT;
DECLARE	__OrigRowCnt		INT;
DECLARE __message_text VARCHAR(500);
DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE arch_msg_del_cursor CURSOR FOR 
			SELECT message_id
			FROM	messages
			WHERE to_viewable = 'D'
				AND message_date < __archive_before_date;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

SET done = 0;
SET i = 0;

OPEN arch_msg_del_cursor;
SET __table_RowCnt = (SELECT FOUND_ROWS());
SELECT CONCAT('Starting archive ... ',CAST(__table_RowCnt AS CHAR),' rows to archive. Archive before date: ',
											CAST(__archive_before_date AS CHAR),'; Cutoff time: ',
											CAST(__archive_cutoff_time AS CHAR)) INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'archive_messages_deleted',__message_text);

REPEAT
    SET i=i+1;
	FETCH arch_msg_del_cursor INTO _message_id;

	IF _message_id IS NOT NULL THEN
		SELECT from_id,to_id,`subject`,message,message_date,replied,`type`,channel_id,to_viewable,from_viewable
		INTO  _from_id,_to_id,`_subject`,_message,_message_date,_replied,`_type`,_channel_id,_to_viewable,_from_viewable
		FROM messages WHERE message_id = _message_id;

		INSERT IGNORE INTO kaneva.messages_deleted (message_id,from_id,to_id,
					`subject`,message,message_date,replied,`type`,channel_id,to_viewable,from_viewable)
			VALUES (_message_id,_from_id,_to_id,`_subject`,_message,_message_date,_replied,`_type`,_channel_id,_to_viewable,_from_viewable);
			
		DELETE FROM messages WHERE message_id = _message_id;
		SELECT ROW_COUNT() INTO @rowcnt;

		IF (_from_viewable='S') AND (_type < 4) AND (@rowcnt = 1) THEN  
			UPDATE kaneva.users_stats SET number_outbox_messages = number_outbox_messages - 1 WHERE user_id = _from_id AND number_outbox_messages > 0;
		END IF;
	END IF;
	IF NOW() >= __archive_cutoff_time THEN 
		SET done = 1; 
	END IF;
		
UNTIL done END REPEAT;
CLOSE arch_msg_del_cursor;

SELECT CONCAT('Archive completed ..... ',CAST(i AS CHAR), ' rows moved to messages_deleted.') INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'archive_messages_deleted',__message_text);

END
;;
DELIMITER ;