-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS decrement_communities_assets_counts_by_type;

DELIMITER ;;
CREATE PROCEDURE `decrement_communities_assets_counts_by_type`(IN __channel_id INT, IN __asset_type_id INT)
BEGIN

IF __asset_type_id = 1 THEN
	UPDATE communities_assets_counts SET games = games-1 WHERE community_id=__channel_id AND games > 0;
ELSEIF __asset_type_id = 2 THEN
	UPDATE communities_assets_counts SET videos = videos-1 WHERE  community_id=__channel_id AND videos > 0;
ELSEIF __asset_type_id = 4 THEN
	UPDATE communities_assets_counts SET music = music-1 WHERE  community_id=__channel_id AND music > 0;
ELSEIF __asset_type_id = 5 THEN
	UPDATE communities_assets_counts SET photos = photos-1 WHERE  community_id=__channel_id AND photos > 0;
ELSEIF __asset_type_id = 6 THEN
	UPDATE communities_assets_counts SET patterns = patterns-1 WHERE  community_id=__channel_id AND patterns > 0;
ELSEIF __asset_type_id = 7 THEN
	UPDATE communities_assets_counts SET TV = TV-1 WHERE  community_id=__channel_id AND TV > 0;
ELSEIF __asset_type_id = 8 THEN
	UPDATE communities_assets_counts SET widgets = widgets-1 WHERE  community_id=__channel_id AND widgets > 0;
ELSEIF __asset_type_id = 99 THEN
	UPDATE communities_assets_counts SET shares = shares-1 WHERE  community_id=__channel_id AND shares > 0;
END IF;

END
;;
DELIMITER ;