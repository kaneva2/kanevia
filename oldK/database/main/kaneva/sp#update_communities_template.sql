-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_communities_template;

DELIMITER ;;
CREATE PROCEDURE `update_communities_template`(IN __community_id INT, IN __template_id INT)
BEGIN

UPDATE communities 
	SET template_id = __template_id
	WHERE community_id = __community_id;                      

END
;;
DELIMITER ;