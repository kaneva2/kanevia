-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_friend_request;

DELIMITER ;;
CREATE PROCEDURE `add_friend_request`(IN __user_id INT, IN __friend_id INT)
BEGIN
	DECLARE __sql_statement VARCHAR(512);

	INSERT INTO kaneva.friend_transactions( txn_type, user_id, friend_id, txn_time )
	VALUES ('REQ', __user_id, __friend_id, NOW() );

	INSERT IGNORE INTO kaneva.friend_requests (user_id, friend_id, request_date)
	VALUES	(__user_id, __friend_id, NOW());
	SELECT ROW_COUNT() INTO @rowcnt1;

	IF @rowcnt1 = 1 THEN
		UPDATE kaneva.users_stats SET number_of_pending  = number_of_pending + 1 WHERE user_id = __user_id;
		UPDATE kaneva.users_stats SET number_of_requests = number_of_requests + 1 WHERE user_id = __friend_id;
	END IF;

END
;;
DELIMITER ;