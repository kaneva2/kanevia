-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS purge_messages_inactive;

DELIMITER ;;
CREATE PROCEDURE `purge_messages_inactive`(__purge_before_date	DATE, __purge_cutoff_time DATETIME)
BEGIN
DECLARE	__Max_message_id		int;
DECLARE	__message_RowCnt		int;
DECLARE __message_text varchar(500);
SELECT CONCAT('Starting purge ... ? rows to purge. Purge before date: ',
						CAST(__purge_before_date as CHAR),'; Purge cutoff time: ',
						CAST(__purge_cutoff_time AS CHAR)) INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'purge_messages_inactive',__message_text);
CREATE TABLE IF NOT EXISTS _temp_inactive_msg
	(message_id INT NOT NULL primary key);
truncate table _temp_inactive_msg;
INSERT INTO _temp_inactive_msg
	SELECT message_id FROM messages
	WHERE from_id IN (SELECT user_id FROM users WHERE last_login < __purge_before_date)
	OR to_id  IN (SELECT user_id FROM users WHERE last_login < __purge_before_date);
SELECT	1 INTO __message_RowCnt;

-- Loop while anything to delete.
WHILE (__message_RowCnt > 0 AND NOW() < __purge_cutoff_time)    DO
	DELETE FROM	messages
		WHERE message_id IN (SELECT message_id FROM _temp_inactive_msg)
		LIMIT 5000;
	SELECT IFNULL(ROW_COUNT(),0) INTO __message_RowCnt;
	SELECT  CONCAT('Purged ',CAST(__message_RowCnt AS CHAR),' rows.') INTO __message_text;
	CALL shard_info.add_maintenance_log (DATABASE(),'purge_messages_inactive',__message_text);
	SELECT SLEEP( 1 ) INTO @ignore_me;
	COMMIT;
	END WHILE;
SELECT 'Purge completed.' INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'purge_messages_inactive',__message_text);
DROP TABLE IF EXISTS _temp_inactive_msg;
END
;;
DELIMITER ;