-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_communities;

DELIMITER ;;
CREATE PROCEDURE `add_communities`(
  IN __place_type_id INT, IN __name_no_spaces VARCHAR(100),
  IN __url VARCHAR(50), IN __creator_id INT, IN __status_id INT, IN __email VARCHAR(100),
  IN __is_public CHAR(1), IN __is_adult CHAR(1), IN __thumbnail_path VARCHAR(255),
  IN __thumbnail_small_path VARCHAR(255), IN __thumbnail_medium_path VARCHAR(255),
  IN __thumbnail_large_path VARCHAR(255), IN __thumbnail_xlarge_path VARCHAR(255),
  IN __thumbnail_type VARCHAR(50), IN __allow_publishing TINYINT, IN __allow_member_events TINYINT,
  IN __is_personal TINYINT, IN __community_type_id SMALLINT, IN __template_id INT,
  IN __over_21_required ENUM('Y','N'), IN __has_thumbnail ENUM('Y','N'), IN __name VARCHAR(100),
  IN __description VARCHAR(255), IN __background_rgb VARCHAR(8),
  IN __background_image VARCHAR(255), IN __creator_username VARCHAR(22), IN __keywords TEXT,
  OUT __community_id INT)
BEGIN

 

INSERT INTO communities 
	(place_type_id, name_no_spaces, url, creator_id, 
	created_date, last_edit, status_id, email, is_public, is_adult, thumbnail_path, 
	thumbnail_small_path, thumbnail_medium_path, thumbnail_large_path, thumbnail_xlarge_path, 
	thumbnail_type, allow_publishing, allow_member_events, is_personal, community_type_id, 
	last_update, template_id, over_21_required, _created_date, has_thumbnail, `name`, 
	`description`, background_rgb, background_image, creator_username, `keywords`)
	VALUES
	(__place_type_id, __name_no_spaces, __url, __creator_id, 
	NOW(), NOW(), __status_id, __email, __is_public, __is_adult, __thumbnail_path, 
	__thumbnail_small_path, __thumbnail_medium_path, __thumbnail_large_path, __thumbnail_xlarge_path, 
	__thumbnail_type, __allow_publishing, __allow_member_events, __is_personal, __community_type_id, 
	NOW(), __template_id, __over_21_required, (master_date() - UNIX_TIMESTAMP(NOW())), __has_thumbnail, __name, 
	__description, __background_rgb, __background_image, __creator_username, __keywords);

SELECT LAST_INSERT_ID() INTO __community_id;

INSERT INTO communities_assets_counts (community_id) VALUES (__community_id);

INSERT INTO channel_stats (channel_id) VALUES(__community_id);

 
 
 
 

 
 
 
 
 

END
;;
DELIMITER ;