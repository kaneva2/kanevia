-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_permanent_zone_roles_all;

DELIMITER ;;
CREATE PROCEDURE `add_permanent_zone_roles_all`( IN __kaneva_user_id INT, __max_zone_index INT, IN __role INT )
BEGIN
  
  DECLARE __idx INT DEFAULT 0;
  
  WHILE __idx <= __max_zone_index DO
  
    INSERT INTO permanent_zone_roles(`zone_index`,`kaneva_user_id`,`role`) VALUES (__idx, __kaneva_user_id, __role)
      ON DUPLICATE KEY UPDATE `role` = __role;
    
    SET __idx = __idx + 1;
  
  END WHILE;
    
  END
;;
DELIMITER ;