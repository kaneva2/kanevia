-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_communities_thumbnail;

DELIMITER ;;
CREATE PROCEDURE `update_communities_thumbnail`(IN __community_id INT, IN __thumbnail_path VARCHAR(255), 
		IN __thumbnail_type VARCHAR(50), IN __has_thumbnail ENUM('Y','N'))
BEGIN

UPDATE communities 
	SET thumbnail_path = __thumbnail_path,
	thumbnail_type = __thumbnail_type, 
	has_thumbnail = __has_thumbnail 
	WHERE community_id = __community_id;

END
;;
DELIMITER ;