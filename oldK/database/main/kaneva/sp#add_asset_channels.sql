-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_asset_channels;

DELIMITER ;;
CREATE PROCEDURE `add_asset_channels`(IN __asset_id INT, IN __channel_id INT, IN __status_id INT)
BEGIN

DECLARE _permission INT;
DECLARE _asset_type_id INT;
DECLARE _asset_owner_id INT;
DECLARE _ch_creator_id INT;
DECLARE _is_personal INT;
DECLARE _status_id INT;
DECLARE _is_adult ENUM('Y','N');
DECLARE __sql_statement VARCHAR(512);


INSERT INTO asset_channels 
	(channel_id, asset_id, created_date, status_id)
	VALUES
	(__channel_id, __asset_id, NOW(), __status_id);

SELECT permission, asset_type_id, owner_id 
	INTO _permission, _asset_type_id, _asset_owner_id 
	FROM assets WHERE asset_id = __asset_id;
	
SELECT creator_id, is_personal, is_adult, status_id 
	INTO _ch_creator_id, _is_personal, _is_adult, _status_id 
	FROM communities WHERE community_id = __channel_id;
	
IF (_is_adult = 'N' AND _status_id = 1) THEN
   REPLACE INTO summary_asset_channels
       SELECT __asset_id, c.community_id, c.is_adult, c.name, c.is_personal, c.name_no_spaces, c.thumbnail_small_path, c.creator_id, NOW()
       FROM communities c  WHERE c.community_id = __channel_id;
END IF;

CALL increment_communities_assets_counts_by_type(__channel_id, _asset_type_id);
 
IF (_asset_owner_id <> _ch_creator_id) THEN
   CALL increment_communities_assets_counts_by_type(__channel_id, 99);
END IF;

UPDATE kaneva.assets_stats SET number_of_channels = number_of_channels + 1 WHERE asset_id = __asset_id;

END
;;
DELIMITER ;