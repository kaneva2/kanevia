-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS copy_data_copy_history;

DELIMITER ;;
CREATE PROCEDURE `copy_data_copy_history`(table_ VARCHAR(100))
BEGIN
 
 SELECT * FROM kaneva.data_copy_history WHERE table_name = table_ ORDER BY copy_history_id DESC LIMIT 0,1;
  
END
;;
DELIMITER ;