-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS assemble_like_assets;

DELIMITER ;;
CREATE PROCEDURE `assemble_like_assets`()
BEGIN
  DECLARE done INT DEFAULT 0;

  DECLARE _base_asset_id INT;
  DECLARE _related_asset_id INT;
  DECLARE _mature_flat INT;
  DECLARE _keywords TEXT;
  DECLARE _cat1 INT;
  DECLARE _cat2 INT;
  DECLARE _cat3 INT;
  DECLARE assets_to_relate_by_full_text CURSOR FOR SELECT a.asset_id, keywords FROM assets a WHERE a.keywords <> '';
  DECLARE assets_to_relate_by_category  CURSOR FOR SELECT a.asset_id, category1_id, category2_id, category3_id from assets a where (a.keywords = '' OR a.keywords IS NULL);
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

 
  DROP TABLE IF EXISTS __related_items_temp;
 
  CREATE TABLE `__related_items_temp` (
  `_order` DOUBLE NOT NULL,
  `base_asset_id` INT NOT NULL,
  `related_asset_id` INT NOT NULL,
  `mature` ENUM('Y','N') DEFAULT 'N',

    PRIMARY KEY(`_order`),
    UNIQUE INDEX (`base_asset_id`, `related_asset_id`)
  )
  ENGINE = MyISAM
  CHARACTER SET utf8;


  SET done = 0;

  OPEN assets_to_relate_by_full_text;

  REPEAT
    FETCH assets_to_relate_by_full_text INTO _base_asset_id, _keywords;
    CALL find_related_assets_by_full_text(_base_asset_id, _keywords );
  UNTIL done END REPEAT;
  CLOSE assets_to_relate_by_full_text;

  OPEN assets_to_relate_by_category;
  REPEAT
    FETCH assets_to_relate_by_category INTO _base_asset_id,_cat1,_cat2,_cat3;
    CALL find_related_assets_by_category(_base_asset_id, _cat1,_cat2,_cat3);
  UNTIL done END REPEAT; 
  CLOSE assets_to_relate_by_category;

  ALTER TABLE __related_items_temp
     ADD INDEX baid(`base_asset_id`),
     ADD INDEX baid_ord(`_order`,`base_asset_id`);
    
  DROP TABLE IF EXISTS related_items;
 
  ALTER TABLE __related_items_temp RENAME TO related_items;

END
;;
DELIMITER ;