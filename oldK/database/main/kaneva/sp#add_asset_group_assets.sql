-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_asset_group_assets;

DELIMITER ;;
CREATE PROCEDURE `add_asset_group_assets`(IN __asset_group_id INT, IN __asset_id INT)
BEGIN
DECLARE __sql_statement VARCHAR(512);

INSERT INTO asset_group_assets (asset_group_id, asset_id) 
	VALUES	(__asset_group_id, __asset_id);

UPDATE kaneva.asset_groups SET asset_count = asset_count + 1 WHERE asset_group_id = __asset_group_id;

END
;;
DELIMITER ;