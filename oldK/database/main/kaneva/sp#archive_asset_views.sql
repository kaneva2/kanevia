-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS archive_asset_views;

DELIMITER ;;
CREATE PROCEDURE `archive_asset_views`( in startdate datetime, in enddate datetime )
begin
  replace into my_kaneva.asset_views_arch Select * from kaneva.asset_views where created_date BETWEEN startdate AND enddate;
	DELETE from kaneva.asset_views where created_date BETWEEN startdate AND enddate;
end
;;
DELIMITER ;