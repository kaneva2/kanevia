-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS master_date;

DELIMITER ;;
CREATE FUNCTION `master_date`() RETURNS bigint(20)
    NO SQL
    DETERMINISTIC
BEGIN
  -- This equals Select UNIX_TIMESTAMP('2037-12-31 23:59:59')*2;
	return 4291869598;
END
;;
DELIMITER ;