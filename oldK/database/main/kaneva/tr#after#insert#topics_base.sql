-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS after_topic_insert;

DELIMITER ;;
CREATE TRIGGER `after_topic_insert` AFTER INSERT ON `topics_base` FOR EACH ROW BEGIN
    UPDATE users_stats SET number_forum_posts = number_forum_posts +1 WHERE user_id = new.created_user_id;
    UPDATE forums SET number_of_topics = (number_of_topics + 1), last_post_date = NOW(), last_topic_id = new.topic_id, last_user_id = new.created_user_id WHERE forum_id = new.forum_id;
END
;;
DELIMITER ;