-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_forum_category;

DELIMITER ;;
CREATE PROCEDURE `add_forum_category`(_communityId INTEGER SIGNED, _name VARCHAR(100), _description VARCHAR(255))
BEGIN

DECLARE _displayOrder INT DEFAULT 0;

  START TRANSACTION;

	
	SELECT 
		COALESCE(MAX(display_order)+1,1) INTO _displayOrder
	FROM 
		forum_categories  
	WHERE 
		community_id = _communityId;
		
		
    	
	INSERT INTO forum_categories 
	(
	    community_id,
	    description,
	    name,
	    display_order
	)
	VALUES 
	(
	    _communityId,
	    _description,
	    _name,
	    _displayOrder
	);
    
    
    SELECT LAST_INSERT_ID();

  COMMIT;

END
;;
DELIMITER ;