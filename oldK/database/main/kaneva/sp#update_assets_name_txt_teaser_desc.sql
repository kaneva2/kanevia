-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_name_txt_teaser_desc;

DELIMITER ;;
CREATE PROCEDURE `update_assets_name_txt_teaser_desc`(IN __asset_id INT, IN __name VARCHAR(100),
		IN __teaser TEXT, IN __body_text TEXT, IN __short_description VARCHAR(50))
BEGIN

UPDATE assets
	SET `name` = __name,
	body_text = __body_text,
	teaser = __teaser,
	short_description = __short_description
	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;