-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS aggregate_country_counts;

DELIMITER ;;
CREATE PROCEDURE `aggregate_country_counts`()
BEGIN
  DELETE from kaneva.countries_7_days where date_created <= DATE_SUB(NOW(), INTERVAL 7 DAY);

  update countries zc
  Inner Join (
    Select count(log_id) as country_count, country_id from countries_7_days group by country_id
  )X ON zc.zip_code = X.zip_code
  SET zc.number_of_users = X.zip_count;

END
;;
DELIMITER ;