-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS b4_ppt_update;

DELIMITER ;;
CREATE TRIGGER `b4_ppt_update` BEFORE UPDATE ON `purchase_point_transactions` 
    FOR EACH ROW BEGIN
  DECLARE _count INT;
    IF ( new.transaction_status = 3) THEN
		SET _count = (SELECT COUNT(user_id) AS c FROM first_time_transactions WHERE user_id = new.user_id);
	
		IF (_count IS NULL) OR (_count = 0) THEN
			INSERT INTO first_time_transactions VALUES(new.user_id, new.transaction_date);
		END IF;
	END IF;
END
;;
DELIMITER ;