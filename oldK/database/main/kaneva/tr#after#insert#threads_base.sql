-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS after_threads_insert;

DELIMITER ;;
CREATE TRIGGER `after_threads_insert` AFTER INSERT ON `threads_base` FOR EACH ROW BEGIN
    UPDATE users_stats SET number_forum_posts = number_forum_posts +1 WHERE user_id = new.user_id;
    UPDATE topics SET number_of_replies = (number_of_replies + 1), last_reply_date = NOW(), last_user_id = new.user_id, last_thread_id = new.thread_id WHERE topic_id = new.topic_id;
END
;;
DELIMITER ;