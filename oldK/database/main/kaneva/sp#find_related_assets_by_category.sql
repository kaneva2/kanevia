-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS find_related_assets_by_category;

DELIMITER ;;
CREATE PROCEDURE `find_related_assets_by_category`(IN _base_asset_id INT, IN _c1 INT, IN _c2 INT, IN _c3 INT)
BEGIN
  DECLARE done INT DEFAULT 0;
  DECLARE _related_asset_id INT;
  DEClARE _mature ENUM('Y','N');
  DECLARE _o DOUBLE;
 
  DECLARE _related_assets_all_3 CURSOR for Select rand() as o, a.asset_id, mature from assets a where ( a.category1_id IN (_c1,_c2,_c3) OR a.category2_id IN (_c1,_c2,_c3) OR a.category3_id IN (_c1,_c2,_c3))  AND a.asset_type_id=2 AND a.published='Y' order by o limit 12;
  DECLARE _related_assets_all_2 CURSOR for Select rand() as o, a.asset_id, mature from assets a where ( a.category1_id IN (_c1,_c2) OR a.category2_id IN (_c1,_c2) OR a.category3_id IN (_c1,_c2))              AND a.asset_type_id=2 AND a.published='Y' order by o limit 12;
  DECLARE _related_assets_all_1 CURSOR for Select rand() as o, a.asset_id, mature from assets a where ( a.category1_id = _c1 OR a.category2_id = _c1 OR a.category3_id =_c1)                                    AND a.asset_type_id=2 AND a.published='Y' order by o limit 12;
 
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
 
  SET done = 0;
 
  if (_c3 IS NOT NULL) AND (_c3 <> 0) THEN
    OPEN _related_assets_all_3;
 
    REPEAT
      FETCH  _related_assets_all_3 into _o, _related_asset_id, _mature;
      if (_related_asset_id IS NOT NULL) THEN
          Replace into __related_items_temp VALUES(_o, _base_asset_id, _related_asset_id, _mature);     
      END IF;
    UNTIL done END REPEAT;
 
  ELSEIF (_c2 IS NOT NULL) AND (_c2 <> 0) THEN
    OPEN _related_assets_all_2;
 
    REPEAT
      FETCH  _related_assets_all_2 into _o, _related_asset_id, _mature;
      if (_related_asset_id IS NOT NULL) THEN
          Replace into __related_items_temp VALUES(_o, _base_asset_id, _related_asset_id, _mature);
      END IF;
    UNTIL done END REPEAT;
  ELSE
   OPEN _related_assets_all_1;
 
    REPEAT
      FETCH  _related_assets_all_1 into _o, _related_asset_id, _mature;
      if (_related_asset_id IS NOT NULL) THEN
          Replace into __related_items_temp VALUES(_o, _base_asset_id, _related_asset_id, _mature);
      END IF;
    UNTIL done END REPEAT;
  END IF;
END
;;
DELIMITER ;