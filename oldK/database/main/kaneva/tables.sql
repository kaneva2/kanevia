-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `acquisition_source` (
  `acquisition_source_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `acquisition_source_type_id` int(11) unsigned NOT NULL COMMENT 'FK to acquisition_source_type table',
  `description` varchar(128) NOT NULL DEFAULT '' COMMENT 'domain name',
  PRIMARY KEY (`acquisition_source_id`),
  KEY `IDX_acquisition_source_type__id` (`acquisition_source_type_id`)
) ENGINE=InnoDB COMMENT='First time visit referring URL for users that join Kaneva';

CREATE TABLE `acquisition_source_type` (
  `acquisition_source_type_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `description` varchar(128) NOT NULL DEFAULT '' COMMENT 'description of acquisition type',
  PRIMARY KEY (`acquisition_source_type_id`)
) ENGINE=InnoDB COMMENT='Source acquisition types';

CREATE TABLE `address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(200) NOT NULL DEFAULT '',
  `phone_number` varchar(50) DEFAULT NULL,
  `address1` varchar(100) DEFAULT NULL,
  `address2` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state_code` varchar(100) DEFAULT NULL,
  `zip_code` varchar(25) NOT NULL DEFAULT '',
  `country_id` char(2) DEFAULT NULL,
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB;

CREATE TABLE `api_authentication` (
  `community_id` int(11) NOT NULL DEFAULT '0',
  `consumer_key` varchar(100) NOT NULL DEFAULT 'key',
  `consumer_secret` varchar(200) NOT NULL DEFAULT 'secret',
  `send_blast` enum('Y','N') DEFAULT 'Y',
  `admin_override_blast` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`community_id`)
) ENGINE=InnoDB;

CREATE TABLE `assets` (
  `asset_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_type_id` int(11) NOT NULL DEFAULT '0',
  `is_kaneva_game` tinyint(4) NOT NULL DEFAULT '0',
  `asset_sub_type_id` int(11) NOT NULL DEFAULT '0',
  `asset_rating_id` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `file_size` bigint(20) unsigned NOT NULL DEFAULT '0',
  `run_time_seconds` int(11) NOT NULL DEFAULT '0',
  `category1_id` int(11) NOT NULL DEFAULT '0',
  `category2_id` int(11) NOT NULL DEFAULT '0',
  `category3_id` int(11) NOT NULL DEFAULT '0',
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT',
  `image_path` varchar(255) DEFAULT NULL,
  `image_full_path` varchar(255) DEFAULT NULL,
  `thumbnail_assetdetails_path` varchar(255) NOT NULL,
  `thumbnail_xlarge_path` varchar(255) NOT NULL,
  `thumbnail_large_path` varchar(255) NOT NULL,
  `thumbnail_medium_path` varchar(255) NOT NULL,
  `thumbnail_small_path` varchar(255) NOT NULL,
  `thumbnail_gen` tinyint(2) NOT NULL DEFAULT '0',
  `image_caption` varchar(100) DEFAULT NULL,
  `image_type` varchar(50) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `last_updated_user_id` int(11) DEFAULT NULL,
  `game_exe` varchar(100) DEFAULT NULL,
  `torrent_id` int(11) DEFAULT NULL,
  `license_cc` varchar(100) NOT NULL DEFAULT 'http://creativecommons.org/licenses/by/2.5/',
  `license_URL` varchar(200) DEFAULT NULL,
  `license_type` char(1) NOT NULL DEFAULT 'P',
  `license_name` varchar(50) DEFAULT NULL,
  `license_additional` varchar(100) DEFAULT NULL,
  `publish_status_id` int(11) NOT NULL DEFAULT '0',
  `percent_complete` int(11) DEFAULT '0',
  `ip_address` varchar(30) DEFAULT NULL,
  `game_encryption_key` varchar(16) DEFAULT NULL,
  `icon_image_data` longblob,
  `icon_image_path` varchar(100) DEFAULT NULL,
  `icon_image_type` varchar(50) DEFAULT NULL,
  `require_login` smallint(6) NOT NULL DEFAULT '0',
  `content_extension` varchar(255) DEFAULT NULL,
  `target_dir` varchar(255) DEFAULT NULL,
  `media_path` varchar(255) DEFAULT '',
  `permission` int(11) NOT NULL DEFAULT '1',
  `permission_group` int(11) DEFAULT NULL,
  `published` enum('Y','N') NOT NULL DEFAULT 'Y',
  `mature` enum('Y','N') NOT NULL DEFAULT 'N',
  `public` enum('Y','N') NOT NULL DEFAULT 'Y',
  `playlistable` enum('Y','N') NOT NULL DEFAULT 'Y',
  `over_21_required` enum('Y','N') DEFAULT 'N',
  `asset_offsite_id` varchar(1000) NOT NULL DEFAULT '',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(100) NOT NULL DEFAULT '',
  `body_text` text,
  `short_description` varchar(50) DEFAULT NULL,
  `teaser` text,
  `owner_username` varchar(22) NOT NULL,
  `keywords` text,
  `instructions` text NOT NULL COMMENT 'game instructions',
  `company_name` varchar(100) NOT NULL DEFAULT '' COMMENT 'name of company associated with asset',
  PRIMARY KEY (`asset_id`),
  KEY `FK_assets_asset_ratings` (`asset_rating_id`),
  KEY `FK_Assets_AssetTypes` (`asset_type_id`),
  KEY `INDEX_status_id` (`status_id`),
  KEY `INDEX_publish_status_id` (`publish_status_id`),
  KEY `atid_psid_arid` (`asset_type_id`,`status_id`,`publish_status_id`,`asset_rating_id`,`image_path`),
  KEY `cd` (`created_date`),
  KEY `pubid_sid_arid_atid_p` (`publish_status_id`,`status_id`,`asset_rating_id`,`asset_type_id`,`permission`),
  KEY `__bunch_o_asset_fields` (`amount`,`permission`,`owner_id`,`asset_type_id`,`status_id`,`publish_status_id`,`asset_rating_id`) USING BTREE,
  KEY `published` (`published`),
  KEY `mature` (`mature`),
  KEY `public` (`public`),
  KEY `playlistable` (`playlistable`),
  KEY `owner` (`owner_id`),
  KEY `IDX_sort_order` (`sort_order`),
  KEY `IDX_last_update` (`last_update`),
  KEY `IDX_combo` (`asset_id`,`owner_id`,`published`,`mature`,`asset_type_id`,`permission`),
  KEY `IDX_combo2` (`owner_id`,`published`,`mature`,`asset_type_id`,`permission`)
) ENGINE=InnoDB;

CREATE TABLE `assets_deleted` (
  `asset_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `asset_type_id` tinyint(4) NOT NULL,
  `target_dir` varchar(255) DEFAULT NULL,
  `removed_from_filestore` enum('Y','N') NOT NULL,
  PRIMARY KEY (`asset_id`),
  KEY `flag` (`removed_from_filestore`)
) ENGINE=InnoDB;

CREATE TABLE `assets_stats` (
  `asset_id` int(11) NOT NULL DEFAULT '0',
  `number_of_comments` int(11) NOT NULL DEFAULT '0',
  `number_of_downloads` int(11) NOT NULL DEFAULT '0',
  `number_of_streams` int(11) NOT NULL DEFAULT '0',
  `number_of_stream_requests` int(11) NOT NULL DEFAULT '0',
  `number_of_shares` int(10) unsigned NOT NULL DEFAULT '0',
  `number_of_channels` int(10) unsigned NOT NULL DEFAULT '0',
  `number_of_diggs` int(10) unsigned NOT NULL DEFAULT '0',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`asset_id`),
  KEY `downloads` (`number_of_downloads`),
  KEY `IDX_last_update` (`last_update`)
) ENGINE=InnoDB;

CREATE TABLE `asset_attribute_values` (
  `asset_id` int(11) NOT NULL DEFAULT '0',
  `attribute_id` int(11) DEFAULT NULL,
  `category_attribute_value_id` int(11) DEFAULT NULL,
  `attribute_value` varchar(255) DEFAULT NULL,
  KEY `FK_asset_attribute_values_assets` (`asset_id`),
  KEY `FK_asset_attribute_values_category_attribute_values` (`category_attribute_value_id`)
) ENGINE=InnoDB;

CREATE TABLE `asset_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_type_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB;

CREATE TABLE `asset_channels` (
  `asset_channel_id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`asset_channel_id`),
  UNIQUE KEY `UNI` (`channel_id`,`asset_id`),
  KEY `asset_channels_idx` (`channel_id`),
  KEY `chnl_status` (`asset_id`,`channel_id`,`status_id`),
  KEY `IDX_asset_id` (`asset_id`)
) ENGINE=InnoDB;

CREATE TABLE `asset_diggs` (
  `digg_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `asset_id` int(11) unsigned NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`digg_id`),
  KEY `digger` (`user_id`,`asset_id`),
  KEY `IDX_asset_id` (`asset_id`),
  KEY `IDX_created_date` (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `asset_duplicates` (
  `parent_asset_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  PRIMARY KEY (`parent_asset_id`,`asset_id`),
  KEY `parIdx` (`parent_asset_id`),
  KEY `assIdx` (`asset_id`)
) ENGINE=InnoDB COMMENT='Holds the duplicate relationships';

CREATE TABLE `asset_groups` (
  `asset_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `channel_id` int(11) NOT NULL DEFAULT '0',
  `asset_count` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`asset_group_id`),
  KEY `IDX_channel_id` (`channel_id`)
) ENGINE=InnoDB;

CREATE TABLE `asset_group_assets` (
  `asset_group_id` int(11) NOT NULL DEFAULT '0',
  `asset_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`asset_group_id`,`asset_id`),
  KEY `IDX_asset_id` (`asset_id`)
) ENGINE=InnoDB;

CREATE TABLE `asset_permissions` (
  `permission_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB;

CREATE TABLE `asset_ratings` (
  `asset_rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `minium_age` int(11) DEFAULT NULL,
  `display_order` int(11) NOT NULL DEFAULT '0',
  `asset_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`asset_rating_id`)
) ENGINE=InnoDB;

CREATE TABLE `asset_share_connection` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned DEFAULT NULL,
  `user_ip` varchar(20) DEFAULT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `depth` int(10) unsigned NOT NULL DEFAULT '0',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `asset_share_connection_map_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_share_connection_id` int(11) NOT NULL DEFAULT '0',
  `map_data` text NOT NULL,
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `asset_share_connection_stats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_share_connection_id` int(11) NOT NULL DEFAULT '0',
  `depth` int(11) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `created_datetime` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `asset_status` (
  `asset_status_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'this number correlates to status_is in the asset and asset_channels tables',
  `asset_status` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`asset_status_id`)
) ENGINE=InnoDB COMMENT='second normal forma table for asset statuses';

CREATE TABLE `asset_subscriptions` (
  `asset_subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `asset_id` int(11) NOT NULL DEFAULT '0',
  `community_id` int(11) DEFAULT NULL,
  `length_of_subscription` int(10) NOT NULL DEFAULT '0',
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `amount_kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT',
  `status_id` int(11) NOT NULL DEFAULT '0',
  `updated_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`asset_subscription_id`)
) ENGINE=InnoDB;

CREATE TABLE `asset_subtypes` (
  `asset_subtype_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `icon_path` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`asset_subtype_id`)
) ENGINE=InnoDB;

CREATE TABLE `asset_types` (
  `asset_type_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `icon_path` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`asset_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `asset_upload` (
  `asset_upload_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `filename` varchar(255) DEFAULT NULL,
  `size` bigint(20) DEFAULT '0',
  `MD5_hash` varchar(100) DEFAULT NULL,
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `completed_datetime` datetime DEFAULT NULL,
  `type_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`asset_upload_id`),
  KEY `asset_id_join` (`asset_id`)
) ENGINE=InnoDB;

CREATE TABLE `asset_youtube_TEMP` (
  `idasset_youtube` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `ERROR_CODE` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idasset_youtube`)
) ENGINE=InnoDB;

CREATE TABLE `audit_promotional_offers` (
  `promotion_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT',
  `dollar_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `kei_point_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `free_points_awarded_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `free_kei_point_ID` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT',
  `is_special` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `promotion_description` varchar(100) DEFAULT NULL,
  `promotional_offers_type_id` smallint(6) NOT NULL DEFAULT '0',
  `wok_pass_group_id` int(11) NOT NULL,
  `sku` smallint(5) unsigned DEFAULT '0',
  `bundle_title` varchar(100) NOT NULL DEFAULT '',
  `bundle_subheading1` varchar(100) DEFAULT NULL,
  `bundle_subheading2` varchar(100) DEFAULT NULL,
  `promotion_start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `promotion_end` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `promotion_list_heading` varchar(100) NOT NULL DEFAULT '',
  `highlight_color` char(7) DEFAULT NULL,
  `special_background_color` char(7) DEFAULT NULL,
  `special_background_image` varchar(500) DEFAULT NULL,
  `special_sticker_image` varchar(45) DEFAULT NULL,
  `promotional_package_label` varchar(100) DEFAULT NULL,
  `special_font_color` char(7) DEFAULT NULL,
  `value_of_credits` decimal(12,2) DEFAULT NULL,
  `modifiers_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `operation` varchar(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB;

CREATE TABLE `audit_promotional_offer_items` (
  `wok_item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `promotion_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `gift_card_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `item_description` varchar(125) NOT NULL DEFAULT '',
  `alternate_description` varchar(125) DEFAULT NULL,
  `market_price` smallint(5) unsigned NOT NULL DEFAULT '0',
  `quantity` smallint(5) unsigned NOT NULL DEFAULT '0',
  `gender` enum('M','F','U') NOT NULL DEFAULT 'U',
  `promotion_offer_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `modifiers_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `operation` varchar(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB;

CREATE TABLE `audit_users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(22) NOT NULL DEFAULT '',
  `username_old` varchar(22) NOT NULL DEFAULT '',
  `display_name` varchar(30) NOT NULL DEFAULT '',
  `display_name_old` varchar(30) NOT NULL DEFAULT '',
  `zip_code` varchar(13) NOT NULL DEFAULT '0',
  `zip_code_old` varchar(13) NOT NULL DEFAULT '0',
  `country` char(2) NOT NULL DEFAULT '99',
  `country_old` char(2) NOT NULL DEFAULT '99',
  `location` varchar(80) NOT NULL DEFAULT '',
  `location_old` varchar(80) NOT NULL DEFAULT '',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `ui` (`user_id`)
) ENGINE=InnoDB ROW_FORMAT=DYNAMIC;

CREATE TABLE `banned_ip_addresses` (
  `ip_address` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`ip_address`)
) ENGINE=InnoDB;

CREATE TABLE `billing_information` (
  `billing_information_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `nickname` varchar(50) NOT NULL DEFAULT '',
  `information_type` int(11) NOT NULL DEFAULT '0',
  `name_on_card` varchar(100) DEFAULT NULL,
  `card_number` varchar(40) DEFAULT NULL,
  `card_type` varchar(50) DEFAULT NULL,
  `exp_month` varchar(50) DEFAULT NULL,
  `exp_day` varchar(50) DEFAULT NULL,
  `exp_year` varchar(50) DEFAULT NULL,
  `address_id` int(11) DEFAULT '0',
  PRIMARY KEY (`billing_information_id`)
) ENGINE=InnoDB;

CREATE TABLE `birthday_matching` (
  `birthday` date NOT NULL,
  PRIMARY KEY (`birthday`)
) ENGINE=InnoDB;

CREATE TABLE `blacklist` (
  `blacklist_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `bounce_type_id` tinyint(4) NOT NULL COMMENT 'PK',
  `email` varchar(255) NOT NULL COMMENT 'email',
  `when_added` datetime NOT NULL,
  PRIMARY KEY (`blacklist_id`),
  UNIQUE KEY `idx_email` (`email`),
  KEY `IDX_bounce_type_id_FK` (`bounce_type_id`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT COMMENT='Places not to send mail';

CREATE TABLE `blocked_sources` (
  `blocked_source_id` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`blocked_source_id`)
) ENGINE=InnoDB;

CREATE TABLE `blocked_users` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `blocked_user_id` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `blocked_source_id` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`blocked_user_id`),
  KEY `fk_bu_buid` (`blocked_user_id`),
  KEY `fk_bu_bs` (`blocked_source_id`),
  CONSTRAINT `fk_bu_bs` FOREIGN KEY (`blocked_source_id`) REFERENCES `blocked_sources` (`blocked_source_id`),
  CONSTRAINT `fk_bu_buid` FOREIGN KEY (`blocked_user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `fk_bu_uid` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `blocked_users_old` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `profile_id` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `zone_type` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Apartment or broadband',
  `zone_instance_id` int(11) NOT NULL DEFAULT '0' COMMENT 'id of the zone being blocked if block_type is player or zone travel',
  `reason_description` varchar(80) NOT NULL DEFAULT '0',
  `is_comm_blocked` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Communication blocked',
  `is_follow_blocked` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Apartment or follow person',
  `is_zone_blocked` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Broadband zone blocked',
  `block_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`block_id`),
  KEY `puz` (`profile_id`,`user_id`,`zone_type`),
  KEY `pid` (`profile_id`),
  KEY `uid` (`user_id`),
  KEY `zt` (`zone_type`),
  KEY `zt_izb` (`zone_type`,`is_zone_blocked`)
) ENGINE=InnoDB;

CREATE TABLE `blogs` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `community_id` int(11) DEFAULT NULL,
  `subject` varchar(100) NOT NULL DEFAULT '',
  `body_text` text,
  `created_user_id` int(11) NOT NULL DEFAULT '0',
  `number_of_comments` int(11) NOT NULL DEFAULT '0',
  `number_of_views` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(30) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `keywords` text,
  PRIMARY KEY (`blog_id`),
  KEY `count_community_null` (`status_id`,`created_user_id`,`community_id`),
  KEY `IDX_community_id` (`community_id`)
) ENGINE=InnoDB;

CREATE TABLE `blog_categories` (
  `blog_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  `thumbnail_image` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`blog_category_id`)
) ENGINE=InnoDB;

CREATE TABLE `blog_comments` (
  `blog_comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `subject` varchar(100) DEFAULT NULL,
  `body_text` text,
  `ip_address` varchar(30) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`blog_comment_id`),
  KEY `summary_index` (`blog_id`,`status_id`,`created_date`),
  KEY `bc_status_user` (`blog_id`,`user_id`,`status_id`),
  KEY `sort_by_update_date` (`last_updated_date`)
) ENGINE=InnoDB;

CREATE TABLE `bulletins` (
  `bulletin_id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) NOT NULL DEFAULT '0',
  `from_status_id` int(11) NOT NULL DEFAULT '0',
  `friend_group_id` int(11) DEFAULT NULL,
  `subject` varchar(100) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `message_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `channel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `friend_status_mesh` (`from_id`,`from_status_id`)
) ENGINE=InnoDB COMMENT='Used for bulletins';

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_type_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  `parent_category_id` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB;

CREATE TABLE `category_attributes` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  `type` varchar(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`attribute_id`),
  KEY `FK_category_attributes_categories` (`category_id`)
) ENGINE=InnoDB;

CREATE TABLE `category_attribute_values` (
  `category_attribute_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`category_attribute_value_id`),
  KEY `FK_category_attribute_values_category_attributes` (`attribute_id`)
) ENGINE=InnoDB;

CREATE TABLE `channel_diggs` (
  `digg_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rave_count` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`digg_id`),
  KEY `newindex` (`created_date`),
  KEY `uid_cid` (`user_id`,`channel_id`),
  KEY `IDX_cd_channel_id` (`channel_id`)
) ENGINE=InnoDB;

CREATE TABLE `channel_diggs_with_zero_date` (
  `digg_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rave_count` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB;

CREATE TABLE `channel_stats` (
  `channel_id` int(11) NOT NULL AUTO_INCREMENT,
  `number_of_views` int(11) NOT NULL DEFAULT '0',
  `number_of_members` int(11) NOT NULL DEFAULT '0',
  `number_of_diggs` int(11) NOT NULL DEFAULT '0',
  `number_times_shared` int(11) NOT NULL DEFAULT '0',
  `number_of_pending_members` int(11) NOT NULL DEFAULT '0',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `number_posts_7_days` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Posts last 7 days',
  `number_of_blogs` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'community blog count',
  PRIMARY KEY (`channel_id`),
  KEY `IDX_last_update` (`last_update`)
) ENGINE=InnoDB;

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `asset_id` int(11) NOT NULL DEFAULT '0',
  `channel_id` int(11) NOT NULL DEFAULT '0',
  `parent_comment_id` int(11) DEFAULT '0',
  `reply_to_comment_id` int(11) DEFAULT '0',
  `comment` text,
  `comment_type` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(50) DEFAULT NULL,
  `last_updated_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) NOT NULL DEFAULT '0',
  `status_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`),
  KEY `num_people` (`channel_id`,`status_id`),
  KEY `asset_finder` (`asset_id`,`user_id`,`status_id`),
  KEY `channel_id` (`channel_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `communities` (
  `community_id` int(11) NOT NULL AUTO_INCREMENT,
  `place_type_id` int(11) NOT NULL DEFAULT '0',
  `name_no_spaces` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(50) DEFAULT NULL,
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_edit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(100) DEFAULT NULL,
  `is_public` char(1) NOT NULL DEFAULT '',
  `is_adult` char(1) DEFAULT NULL,
  `thumbnail_path` varchar(255) DEFAULT NULL,
  `thumbnail_small_path` varchar(255) NOT NULL DEFAULT '',
  `thumbnail_medium_path` varchar(255) NOT NULL DEFAULT '',
  `thumbnail_large_path` varchar(255) NOT NULL DEFAULT '',
  `thumbnail_xlarge_path` varchar(255) NOT NULL DEFAULT '',
  `thumbnail_square_path` varchar(255) NOT NULL DEFAULT '',
  `thumbnail_type` varchar(50) DEFAULT NULL,
  `allow_publishing` tinyint(4) NOT NULL DEFAULT '0',
  `allow_member_events` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_personal` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `community_type_id` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'indicates the type of community (e.g. profile vs community)',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `template_id` int(10) unsigned NOT NULL DEFAULT '0',
  `over_21_required` enum('Y','N') DEFAULT 'N',
  `_created_date` int(10) unsigned NOT NULL DEFAULT '4291869598',
  `has_thumbnail` enum('Y','N') NOT NULL DEFAULT 'N',
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `background_rgb` varchar(8) DEFAULT '#cccccc' COMMENT 'color for the background of the community page',
  `background_image` varchar(255) DEFAULT NULL COMMENT 'path for the background image of the community page',
  `creator_username` varchar(22) NOT NULL,
  `keywords` text,
  PRIMARY KEY (`community_id`),
  KEY `index_creatorId` (`creator_id`),
  KEY `index_ispersonal` (`is_personal`),
  KEY `creator_id_is_personal` (`creator_id`,`is_personal`),
  KEY `nns_is_personal` (`name_no_spaces`,`is_personal`),
  KEY `newindex` (`thumbnail_path`,`is_personal`),
  KEY `u` (`url`(20)),
  KEY `_created_date` (`_created_date`),
  KEY `thumb_rec_adult_per` (`is_personal`,`has_thumbnail`,`is_adult`,`_created_date`) USING BTREE,
  KEY `IDX_type_status` (`community_type_id`,`status_id`),
  KEY `IDX_last_edit` (`last_edit`),
  KEY `IDX_name` (`name`(10))
) ENGINE=InnoDB;

CREATE TABLE `communities_assets_counts` (
  `community_id` int(11) NOT NULL,
  `games` smallint(5) unsigned NOT NULL DEFAULT '0',
  `videos` smallint(5) unsigned NOT NULL DEFAULT '0',
  `music` smallint(5) unsigned NOT NULL DEFAULT '0',
  `photos` smallint(5) unsigned NOT NULL DEFAULT '0',
  `patterns` smallint(5) unsigned NOT NULL DEFAULT '0',
  `TV` smallint(5) unsigned NOT NULL DEFAULT '0',
  `widgets` smallint(5) unsigned NOT NULL DEFAULT '0',
  `shares` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`community_id`)
) ENGINE=InnoDB;

CREATE TABLE `communities_game` (
  `community_id` int(11) NOT NULL COMMENT 'FK to communities',
  `game_id` int(11) NOT NULL COMMENT 'FK to developer.games',
  PRIMARY KEY (`community_id`,`game_id`)
) ENGINE=InnoDB COMMENT='Associates a 3D game with a 3D App community';

CREATE TABLE `communities_to_categories` (
  `community_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`community_id`,`category_id`),
  KEY `fk_category_ids_idx` (`category_id`),
  CONSTRAINT `fk_category_ids` FOREIGN KEY (`category_id`) REFERENCES `community_categories` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_community_ids` FOREIGN KEY (`community_id`) REFERENCES `communities` (`community_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB COMMENT='Join table of communities to community categories.';

CREATE TABLE `community_account_types` (
  `account_type_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`account_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `community_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB;

CREATE TABLE `community_members` (
  `community_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `account_type_id` int(11) NOT NULL DEFAULT '0',
  `added_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_id` int(10) unsigned NOT NULL DEFAULT '0',
  `newsletter` char(1) NOT NULL DEFAULT '',
  `invited_by_user_id` int(11) DEFAULT NULL,
  `allow_asset_uploads` char(1) DEFAULT NULL,
  `allow_forum_use` char(1) DEFAULT NULL,
  `keywords` varchar(100) DEFAULT NULL,
  `notifications` int(11) NOT NULL DEFAULT '0',
  `item_notify` int(11) NOT NULL DEFAULT '0',
  `item_review_notify` int(11) NOT NULL DEFAULT '0',
  `blog_notify` int(11) NOT NULL DEFAULT '0',
  `blog_comment_notify` int(11) NOT NULL DEFAULT '0',
  `post_notify` int(11) NOT NULL DEFAULT '0',
  `reply_notify` int(11) NOT NULL DEFAULT '0',
  `has_edit_rights` enum('Y','N') NOT NULL DEFAULT 'N',
  `active_member` enum('Y','N') NOT NULL DEFAULT 'Y',
  `last_visited_at` datetime NOT NULL DEFAULT '2010-01-01 00:00:00',
  PRIMARY KEY (`community_id`,`user_id`),
  KEY `user_stats` (`user_id`,`status_id`),
  KEY `has_edit_rights` (`has_edit_rights`),
  KEY `active_member` (`active_member`),
  KEY `user_status_edit` (`user_id`,`status_id`,`has_edit_rights`)
) ENGINE=InnoDB;

CREATE TABLE `community_member_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `community_member_group_members` (
  `member_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_group_id`,`member_id`)
) ENGINE=InnoDB;

CREATE TABLE `community_member_status` (
  `status_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB;

CREATE TABLE `community_member_subscriptions` (
  `community_member_subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `community_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `community_subscription_id` int(11) NOT NULL DEFAULT '0',
  `start_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `gross_point_amount` float NOT NULL DEFAULT '0',
  `purchase_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_id` int(11) NOT NULL DEFAULT '0',
  `refund_amount` float DEFAULT NULL,
  PRIMARY KEY (`community_member_subscription_id`)
) ENGINE=InnoDB;

CREATE TABLE `community_member_subscriptions_status` (
  `status_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB;

CREATE TABLE `community_place_types` (
  `place_type_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`place_type_id`)
) ENGINE=InnoDB COMMENT='This is the type of place of the community hangout';

CREATE TABLE `community_preferences` (
  `community_preference_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto number id ',
  `community_id` int(10) unsigned NOT NULL COMMENT 'community id - used to tie prefernces to community',
  `show_gender` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'flag for showing gender on control panel',
  `show_location` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'flag for showing location on control panel',
  `show_age` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'flag for showing age on control panel',
  `about_text` text,
  PRIMARY KEY (`community_preference_id`),
  KEY `IDX_community_id` (`community_id`)
) ENGINE=InnoDB COMMENT='keeps preferences for a community';

CREATE TABLE `community_status` (
  `status_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB;

CREATE TABLE `community_subscriptions` (
  `community_subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `community_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `subscription_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `subscription_kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT',
  `length_of_subscription` int(11) NOT NULL DEFAULT '0',
  `status_id` int(11) NOT NULL DEFAULT '0',
  `updated_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`community_subscription_id`),
  KEY `comid_state` (`community_id`,`status_id`)
) ENGINE=InnoDB;

CREATE TABLE `community_tabs` (
  `community_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'this number correlates to community_id in the communities table',
  `tab_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'this number correlates to tab_id in the community_types_tabs table',
  `community_type_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'this number correlates to communty_type_id in the community_types_tabs table',
  `is_default` enum('Y','N') NOT NULL DEFAULT 'N',
  UNIQUE KEY `uc_unique` (`community_id`,`tab_id`,`community_type_id`),
  KEY `index_community` (`community_id`),
  KEY `fk1` (`community_type_id`,`tab_id`),
  CONSTRAINT `fk1` FOREIGN KEY (`community_type_id`, `tab_id`) REFERENCES `community_types_tabs` (`community_type_id`, `tab_id`)
) ENGINE=InnoDB;

CREATE TABLE `community_types` (
  `community_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'integer indicator for community type',
  `community_type` varchar(50) NOT NULL COMMENT 'description of community type',
  PRIMARY KEY (`community_type_id`)
) ENGINE=InnoDB COMMENT='2nd normal form (look up) table for community types';

CREATE TABLE `community_types_tabs` (
  `community_type_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'this number correlates to communty_type_id in the community_types table',
  `tab_id` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(45) NOT NULL DEFAULT '',
  `is_configurable` enum('Y','N') NOT NULL DEFAULT 'N',
  `display_order` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`community_type_id`,`tab_id`)
) ENGINE=InnoDB;

CREATE TABLE `community_views` (
  `view_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `anonymous` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ip_address` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`view_id`),
  KEY `cv_hub` (`community_id`),
  KEY `cid_anon_u` (`community_id`,`anonymous`,`user_id`),
  KEY `cid_anon_user_id` (`community_id`,`anonymous`,`user_id`),
  KEY `cv_idx_1` (`created_date`,`community_id`,`ip_address`) USING BTREE
) ENGINE=InnoDB;

CREATE TABLE `configuration` (
  `auto_debit_minium_amount` int(11) NOT NULL DEFAULT '0',
  `threads_per_page` int(11) NOT NULL DEFAULT '0',
  `topics_per_page` int(11) NOT NULL DEFAULT '0',
  `max_number_login_attempts` int(11) NOT NULL DEFAULT '0',
  `torrent_username` varchar(32) DEFAULT NULL,
  `torrent_password` varchar(32) DEFAULT NULL,
  `last_notifications_sent` datetime DEFAULT NULL,
  `last_daily_notifications_sent` datetime DEFAULT NULL,
  `file_upload_timeout_hours` int(11) NOT NULL DEFAULT '336',
  `max_upload_filesize` bigint(20) unsigned NOT NULL DEFAULT '10',
  `last_ranking_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_weekly_notifications_sent` datetime DEFAULT NULL,
  `last_daily_webstat_email_sent` datetime NOT NULL
) ENGINE=InnoDB;

CREATE TABLE `contests` (
  `contest_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `terms` text,
  `upload_start_date` datetime NOT NULL,
  `upload_end_date` datetime NOT NULL,
  `vote_start_date` datetime NOT NULL,
  `vote_end_date` datetime NOT NULL,
  `channel_id` int(11) NOT NULL,
  `contest_picture` varchar(100) DEFAULT NULL,
  `vote_yes_image` varchar(100) DEFAULT NULL,
  `vote_no_image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`contest_id`)
) ENGINE=InnoDB;

CREATE TABLE `contests_ugc` (
  `contest_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users, creator of contest',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text,
  `prize_amount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Prize amount',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'KPOINT',
  `contest_active` smallint(1) unsigned NOT NULL DEFAULT '0',
  `num_entries` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of contest entries',
  `num_votes` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of votes',
  `num_comments` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of comments',
  `popular_vote_winner_entry_id` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to kaneva.contest_entries',
  `owner_vote_winner_entry_id` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to kaneva.contest_entries',
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '0-Inactive, 1-Active, 2-Ended Waiting on Winner, 3-Ended Winner Selected, 4-Ended Winner Paid, 5-Ended Owner Refunded',
  `date_status_changed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `archived` smallint(1) unsigned NOT NULL DEFAULT '0',
  `status_changed_by_user_id` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to kaneva.users',
  PRIMARY KEY (`contest_id`),
  KEY `ed` (`end_date`),
  KEY `pa` (`prize_amount`),
  KEY `uid` (`user_id`),
  KEY `popular_vote_winner_entry_id` (`popular_vote_winner_entry_id`),
  KEY `owner_vote_winner_entry_id` (`owner_vote_winner_entry_id`),
  CONSTRAINT `contests_ugc_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `contests_ugc_ibfk_2` FOREIGN KEY (`popular_vote_winner_entry_id`) REFERENCES `contest_entries` (`contest_entry_id`),
  CONSTRAINT `contests_ugc_ibfk_3` FOREIGN KEY (`owner_vote_winner_entry_id`) REFERENCES `contest_entries` (`contest_entry_id`)
) ENGINE=InnoDB COMMENT='new contests for UGC';

CREATE TABLE `contest_assets` (
  `asset_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `number_of_votes` int(11) NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`asset_id`,`contest_id`),
  KEY `aid` (`asset_id`),
  KEY `cid` (`contest_id`),
  KEY `ad` (`added_date`),
  KEY `nov` (`number_of_votes`),
  KEY `caid` (`asset_id`,`contest_id`)
) ENGINE=InnoDB;

CREATE TABLE `contest_asset_votes` (
  `asset_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vote_date` datetime NOT NULL,
  `yes` tinyint(4) NOT NULL DEFAULT '1',
  `ip_address` bigint(20) unsigned NOT NULL DEFAULT '0',
  `valid` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`asset_id`,`contest_id`,`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `contest_comments` (
  `comment_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `contest_id` bigint(20) unsigned NOT NULL COMMENT 'FK to kaneva.contests_ugc',
  `contest_entry_id` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to kaneva.contest_entries',
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`comment_id`),
  KEY `cid` (`contest_id`),
  KEY `ceid` (`contest_entry_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `contest_comments_ibfk_1` FOREIGN KEY (`contest_id`) REFERENCES `contests_ugc` (`contest_id`),
  CONSTRAINT `contest_comments_ibfk_2` FOREIGN KEY (`contest_entry_id`) REFERENCES `contest_entries` (`contest_entry_id`),
  CONSTRAINT `contest_comments_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB COMMENT='comments on contests and contest entries';

CREATE TABLE `contest_entries` (
  `contest_entry_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `contest_id` bigint(20) unsigned NOT NULL COMMENT 'FK to kaneva.contests_ugc',
  `user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text,
  `num_votes` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of votes',
  `num_comments` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of comments',
  `entry_status` smallint(1) NOT NULL DEFAULT '1' COMMENT '0-Inactive, 1-Active, 2-Disabled',
  `date_status_changed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_changed_by_user_id` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to kaneva.users',
  PRIMARY KEY (`contest_entry_id`),
  KEY `uid` (`user_id`),
  KEY `contest_id` (`contest_id`),
  CONSTRAINT `contest_entries_ibfk_1` FOREIGN KEY (`contest_id`) REFERENCES `contests_ugc` (`contest_id`),
  CONSTRAINT `contest_entries_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB COMMENT='contest entries';

CREATE TABLE `contest_entry_votes` (
  `contest_entry_id` bigint(20) unsigned NOT NULL COMMENT 'FK to kaneva.contest_entries',
  `user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_address` int(10) unsigned DEFAULT '0' COMMENT 'IP of user submittin vote',
  PRIMARY KEY (`contest_entry_id`,`user_id`),
  KEY `uid` (`user_id`),
  CONSTRAINT `contest_entry_votes_ibfk_1` FOREIGN KEY (`contest_entry_id`) REFERENCES `contest_entries` (`contest_entry_id`),
  CONSTRAINT `contest_entry_votes_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB COMMENT='votes for contest entries';

CREATE TABLE `contest_followers` (
  `contest_id` bigint(20) unsigned NOT NULL COMMENT 'FK to kaneva.contests_ugc',
  `user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  PRIMARY KEY (`contest_id`,`user_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `contest_followers_ibfk_1` FOREIGN KEY (`contest_id`) REFERENCES `contests_ugc` (`contest_id`),
  CONSTRAINT `contest_followers_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB COMMENT='users following a contest';

CREATE TABLE `contest_transaction_details` (
  `trans_id` bigint(20) NOT NULL COMMENT 'FK to transaction table',
  `contest_id` bigint(20) unsigned NOT NULL COMMENT 'FK to kaneva.contests_ugc',
  `transaction_type` smallint(5) unsigned NOT NULL DEFAULT '0',
  `transaction_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`trans_id`,`contest_id`),
  KEY `cid` (`contest_id`),
  CONSTRAINT `contest_transactions_ibfk_1` FOREIGN KEY (`trans_id`) REFERENCES `wok_transaction_log` (`trans_id`),
  CONSTRAINT `contest_transactions_ibfk_2` FOREIGN KEY (`contest_id`) REFERENCES `contests_ugc` (`contest_id`)
) ENGINE=InnoDB COMMENT='details for contest payment transactions';

CREATE TABLE `countries` (
  `country_id` char(2) NOT NULL DEFAULT '',
  `country` varchar(100) NOT NULL DEFAULT '',
  `number_of_users` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of users in this country',
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB;

CREATE TABLE `countries_7_days` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `country_id` varchar(2) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE=InnoDB;

CREATE TABLE `countries_to_summarize` (
  `country` char(2) NOT NULL DEFAULT 'US' COMMENT 'country code of country',
  PRIMARY KEY (`country`)
) ENGINE=InnoDB COMMENT='Holds country codes of countries to summarize';

CREATE TABLE `crews` (
  `crew_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'The users crew',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date created',
  `crew_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of peeps in crew',
  PRIMARY KEY (`crew_id`),
  UNIQUE KEY `uid` (`user_id`) USING BTREE
) ENGINE=InnoDB COMMENT='Crew info';

CREATE TABLE `crew_peeps` (
  `crew_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'FK to crew',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Peep in crew',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date created',
  PRIMARY KEY (`crew_id`,`user_id`),
  CONSTRAINT `FK_crew_friends_1` FOREIGN KEY (`crew_id`) REFERENCES `crews` (`crew_id`)
) ENGINE=InnoDB COMMENT='Peeps in your crew';

CREATE TABLE `csr_logs` (
  `csr_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activity` text NOT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`csr_log_id`)
) ENGINE=InnoDB;

CREATE TABLE `csr_logs_arch` (
  `csr_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activity` text NOT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`csr_log_id`)
) ENGINE=InnoDB;

CREATE TABLE `data_copy_history` (
  `copy_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto number used for portential reporting',
  `table_name` varchar(100) NOT NULL DEFAULT '' COMMENT 'database and table name whos data is being copied',
  `projected_id_copied` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'holds what will be the highest id to be copied over for new inserts',
  `projected_modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'holds the most recent date for data that will be copied - used for update copies as well as just notation',
  `last_id_copied` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'holds what the last id that was actually copied over',
  `last_modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'holds what last date that was actually copied over ',
  PRIMARY KEY (`copy_history_id`)
) ENGINE=InnoDB COMMENT='keeps data copying record - MYSQL to SQL Server';

CREATE TABLE `dates` (
  `dates` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`dates`)
) ENGINE=InnoDB;

CREATE TABLE `emails_blacklisted_today` (
  `blacklist_id` int(11) NOT NULL COMMENT 'PK',
  `bounce_type_id` tinyint(4) NOT NULL,
  `email` varchar(255) NOT NULL COMMENT 'email',
  `email_hash` binary(16) NOT NULL,
  PRIMARY KEY (`blacklist_id`),
  UNIQUE KEY `idx_email` (`email`),
  KEY `IDX_email_hash` (`email_hash`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT COMMENT='List of emails added to the blacklist today';

CREATE TABLE `email_unsubscribe` (
  `unsubscribe_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Null if not a user otherwise refers to users table',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT 'Email Address we should not contact',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date unsubscribed',
  `note` varchar(255) DEFAULT NULL COMMENT 'Reason or method for unsubscribing.',
  PRIMARY KEY (`unsubscribe_id`),
  KEY `user_id_key` (`user_id`),
  KEY `email_key` (`email`)
) ENGINE=InnoDB COMMENT='Emails of people specfically requesting no email';

CREATE TABLE `events` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `community_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `details` text NOT NULL,
  `location` text NOT NULL,
  `zone_index` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'channel_zone_id from wok.channel_zones',
  `zone_instance_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'zone_instance_id from wok.channel_zones',
  `start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `premium` int(11) NOT NULL DEFAULT '0' COMMENT 'Denotes if the user has paid to promote the event',
  `privacy` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'who can see the event',
  `time_zone_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'when the event was created',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'when the event was updated',
  `is_AP_event` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'Is AP required',
  `tracking_request_GUID` varchar(50) DEFAULT NULL,
  `event_status_id` int(10) unsigned NOT NULL DEFAULT '1',
  `recurring_status_id` int(10) unsigned NOT NULL DEFAULT '0',
  `invite_friends` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'Friends were invited to event',
  `recurring_parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Event Id this recurring event was created from',
  PRIMARY KEY (`event_id`),
  KEY `communit_event_begin` (`community_id`,`start_time`),
  KEY `IDX_events_zone_multi` (`zone_instance_id`,`zone_index`),
  KEY `IDX_endtime_premium` (`end_time`,`premium`)
) ENGINE=InnoDB;

CREATE TABLE `events_archive` (
  `event_id` int(11) NOT NULL,
  `community_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `details` text NOT NULL,
  `location` text NOT NULL,
  `zone_index` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'zone_index from wok.channel_zones',
  `zone_instance_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'zone_instance_id from wok.channel_zones',
  `start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `premium` int(11) NOT NULL DEFAULT '0' COMMENT 'Denotes if the user has paid to promote the event',
  `privacy` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'who can see the event',
  `time_zone_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'when the event was created',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'when the event was updated',
  `is_AP_event` enum('Y','N') NOT NULL DEFAULT 'N',
  `tracking_request_GUID` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB;

CREATE TABLE `events_audit` (
  `event_audit_id` varchar(50) NOT NULL,
  `event_id` int(11) NOT NULL COMMENT 'FK to kaneva.events',
  `user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `description` varchar(200) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`event_audit_id`)
) ENGINE=InnoDB COMMENT='Event audit';

CREATE TABLE `events_rewards` (
  `events_rewards_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `reward_date` datetime DEFAULT NULL,
  `reward_amount` double DEFAULT NULL,
  PRIMARY KEY (`events_rewards_id`),
  KEY `FKeid` (`event_id`),
  KEY `FKuid` (`user_id`),
  CONSTRAINT `FKeid` FOREIGN KEY (`event_id`) REFERENCES `events` (`event_id`),
  CONSTRAINT `FKuid` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB COMMENT='Event Rewards';

CREATE TABLE `event_blasts` (
  `event_blast_id` varchar(50) NOT NULL,
  `event_id` int(11) NOT NULL COMMENT 'FK to kaneva.events',
  `user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment` varchar(256) DEFAULT NULL,
  `num_comments` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of comments',
  PRIMARY KEY (`event_blast_id`),
  UNIQUE KEY `ebid` (`event_blast_id`),
  KEY `uid` (`user_id`),
  KEY `eid` (`event_id`),
  CONSTRAINT `event_blasts_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `events` (`event_id`),
  CONSTRAINT `event_blasts_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB COMMENT='Event blasts';

CREATE TABLE `event_blast_comments` (
  `event_blast_comment_id` varchar(50) NOT NULL,
  `event_id` int(11) NOT NULL COMMENT 'FK to kaneva.events',
  `event_blast_id` varchar(50) NOT NULL COMMENT 'FK to kaneva.event_blasts',
  `comment` varchar(256) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`event_blast_comment_id`),
  UNIQUE KEY `ebcid` (`event_blast_comment_id`),
  KEY `eid` (`event_id`),
  KEY `ebid` (`event_blast_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `event_blast_comments_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `events` (`event_id`),
  CONSTRAINT `event_blast_comments_ibfk_2` FOREIGN KEY (`event_blast_id`) REFERENCES `event_blasts` (`event_blast_id`),
  CONSTRAINT `event_blast_comments_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB COMMENT='Event blast comments';

CREATE TABLE `event_invites` (
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `invite_status_id` tinyint(4) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `attended_event` char(1) DEFAULT NULL,
  PRIMARY KEY (`event_id`,`user_id`),
  KEY `fk_user_id` (`user_id`),
  CONSTRAINT `fk_event_id` FOREIGN KEY (`event_id`) REFERENCES `events` (`event_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB COMMENT='Event invites';

CREATE TABLE `event_recurring_status` (
  `recurring_status_id` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`recurring_status_id`)
) ENGINE=InnoDB COMMENT='Event Recurring Status Types';

CREATE TABLE `event_reminders` (
  `reminder_id` int(10) unsigned NOT NULL,
  `reminder_time_offset` smallint(6) NOT NULL COMMENT 'Number of minutes prior to event start time',
  `email_type_id` smallint(5) DEFAULT NULL COMMENT 'FK to viral_email.email_type',
  PRIMARY KEY (`reminder_id`),
  KEY `fk_email_type_id` (`email_type_id`),
  CONSTRAINT `fk_email_type_id` FOREIGN KEY (`email_type_id`) REFERENCES `viral_email`.`email_type` (`email_type_id`)
) ENGINE=InnoDB COMMENT='Event reminder types';

CREATE TABLE `event_reminder_notifications` (
  `event_id` int(11) NOT NULL,
  `reminder_id` int(10) unsigned NOT NULL,
  `reminder_sent_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`event_id`,`reminder_id`),
  KEY `fk_reminder_id` (`reminder_id`),
  CONSTRAINT `fk_reminder_event_id` FOREIGN KEY (`event_id`) REFERENCES `events` (`event_id`),
  CONSTRAINT `fk_reminder_id` FOREIGN KEY (`reminder_id`) REFERENCES `event_reminders` (`reminder_id`)
) ENGINE=InnoDB COMMENT='Event reminder notifications';

CREATE TABLE `event_status` (
  `event_status_id` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`event_status_id`)
) ENGINE=InnoDB COMMENT='Event Status Types';

CREATE TABLE `event_types` (
  `event_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'in the events table this id is type_id',
  `event_type` varchar(50) NOT NULL DEFAULT '' COMMENT 'textual representation of type',
  PRIMARY KEY (`event_type_id`)
) ENGINE=InnoDB COMMENT='2nd normal form table for events';

CREATE TABLE `featured_assets` (
  `featured_asset_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `start_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_datetime` datetime DEFAULT NULL,
  `shown_count` int(11) NOT NULL DEFAULT '0',
  `status_id` int(11) NOT NULL DEFAULT '0',
  `playlist_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`featured_asset_id`),
  KEY `date_range` (`status_id`,`start_datetime`,`end_datetime`)
) ENGINE=InnoDB;

CREATE TABLE `feedback` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL DEFAULT '',
  `summary` text NOT NULL,
  `IP` varchar(50) NOT NULL DEFAULT '',
  `file_link` varchar(200) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB;

CREATE TABLE `first_time_transactions` (
  `user_id` int(10) unsigned NOT NULL,
  `dt_created` datetime NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `uid` (`user_id`),
  KEY `dtc` (`dt_created`)
) ENGINE=InnoDB;

CREATE TABLE `forums` (
  `forum_id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_category_id` int(11) DEFAULT NULL,
  `community_id` int(11) NOT NULL DEFAULT '0',
  `forum_name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `number_of_topics` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_topic_id` int(11) unsigned NOT NULL DEFAULT '0',
  `last_user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `display_order` int(11) NOT NULL DEFAULT '0',
  `status_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`forum_id`),
  KEY `luid` (`last_user_id`),
  KEY `ltid` (`last_topic_id`),
  KEY `IDX_community_id` (`community_id`)
) ENGINE=InnoDB;

CREATE TABLE `forum_categories` (
  `forum_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `community_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  `display_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`forum_category_id`)
) ENGINE=InnoDB;

CREATE TABLE `forum_post_views` (
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'User that visited forum post',
  `number_of_views` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of posts viewed',
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`user_id`,`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `forum_status` (
  `status_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB;

CREATE TABLE `friends` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `friend_id` int(11) NOT NULL DEFAULT '0',
  `glued_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`friend_id`),
  KEY `u_glue` (`user_id`,`glued_date`),
  KEY `fid` (`friend_id`),
  KEY `uid` (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `friends_declined` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `friend_id` int(10) unsigned NOT NULL,
  `date_declined` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`friend_id`,`date_declined`)
) ENGINE=InnoDB ROW_FORMAT=DYNAMIC;

CREATE TABLE `friends_deleted` (
  `user_id` int(10) unsigned NOT NULL,
  `friend_id` int(10) unsigned NOT NULL,
  `date_deleted` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`friend_id`,`date_deleted`)
) ENGINE=InnoDB ROW_FORMAT=DYNAMIC;

CREATE TABLE `friend_groups` (
  `friend_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `friend_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`friend_group_id`),
  KEY `oid` (`owner_id`)
) ENGINE=InnoDB;

CREATE TABLE `friend_group_friends` (
  `friend_group_id` int(11) NOT NULL DEFAULT '0',
  `friend_id` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`friend_group_id`,`friend_id`),
  KEY `friend_owner_idx` (`friend_id`,`owner_id`)
) ENGINE=InnoDB;

CREATE TABLE `friend_requests` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `friend_id` int(11) NOT NULL DEFAULT '0',
  `request_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`friend_id`),
  KEY `user_frined` (`user_id`,`friend_id`),
  KEY `u_glue` (`user_id`,`request_date`),
  KEY `u_glue_stat` (`user_id`,`request_date`),
  KEY `fid` (`friend_id`)
) ENGINE=InnoDB;

CREATE TABLE `friend_status` (
  `friend_status_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(20) NOT NULL DEFAULT '',
  `descirption` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`friend_status_id`)
) ENGINE=InnoDB;

CREATE TABLE `friend_transactions` (
  `txn_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Transaction Serial Number',
  `txn_type` enum('REQ','ADD','DEC','DEL') NOT NULL COMMENT 'Transaction Type',
  `user_id` int(11) NOT NULL COMMENT 'Requester user ID',
  `friend_id` int(11) NOT NULL COMMENT 'Peer user ID',
  `txn_time` datetime NOT NULL COMMENT 'Transaction Time',
  PRIMARY KEY (`txn_id`)
) ENGINE=InnoDB COMMENT='Friend transactions log including friend request, approval, ';

CREATE TABLE `genres` (
  `genre_id` int(11) NOT NULL DEFAULT '0',
  `asset_type_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`genre_id`)
) ENGINE=InnoDB;

CREATE TABLE `gift_cards` (
  `gift_card_id` int(11) NOT NULL AUTO_INCREMENT,
  `dollar_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `upc` varchar(50) NOT NULL,
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT',
  `kei_point_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `description` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`gift_card_id`)
) ENGINE=InnoDB;

CREATE TABLE `group_categories` (
  `group_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `channel_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`group_category_id`)
) ENGINE=InnoDB;

CREATE TABLE `group_category_asset_groups` (
  `group_category_id` int(11) NOT NULL DEFAULT '0',
  `asset_group_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_category_id`,`asset_group_id`)
) ENGINE=InnoDB;

CREATE TABLE `hotnew_stuff_time_periods` (
  `time_period_id` smallint(5) unsigned NOT NULL,
  `period_in_days` smallint(5) unsigned NOT NULL DEFAULT '0',
  `description` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`time_period_id`)
) ENGINE=InnoDB;

CREATE TABLE `im_users` (
  `user_id` int(11) NOT NULL COMMENT 'Kaneva user ID',
  `online` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=offline, 1=online',
  `presence` varchar(100) NOT NULL DEFAULT '' COMMENT 'Additional presence message',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB COMMENT='IM user states';

CREATE TABLE `interests` (
  `interest_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ic_id` int(10) unsigned NOT NULL,
  `interest` varchar(255) NOT NULL,
  `user_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of users with interest',
  PRIMARY KEY (`interest_id`),
  KEY `iic` (`interest_id`,`ic_id`),
  KEY `ic_id` (`ic_id`),
  KEY `interest` (`interest`(10))
) ENGINE=InnoDB;

CREATE TABLE `interest_categories` (
  `ic_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_text` varchar(45) NOT NULL,
  PRIMARY KEY (`ic_id`)
) ENGINE=InnoDB;

CREATE TABLE `invites` (
  `invite_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(200) NOT NULL DEFAULT '',
  `email_hash` binary(16) NOT NULL,
  `to_name` varchar(100) NOT NULL DEFAULT '',
  `key_value` varchar(20) NOT NULL DEFAULT '',
  `invite_status_id` int(11) NOT NULL DEFAULT '0',
  `invited_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `points_awarded` float DEFAULT NULL,
  `kei_point_id_awarded` varchar(50) DEFAULT NULL,
  `reinvite_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `channel_id` int(10) unsigned DEFAULT NULL,
  `invited_user_id` int(11) DEFAULT NULL,
  `invites_source_id` smallint(6) DEFAULT NULL COMMENT 'sending errors if any',
  `attempts` int(10) unsigned NOT NULL DEFAULT '1',
  `opened_invite` binary(1) NOT NULL DEFAULT '0',
  `clicked_accept_link` binary(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`invite_id`),
  KEY `user_id` (`user_id`),
  KEY `invited_date` (`invited_date`),
  KEY `IDX_invited_user_id` (`invited_user_id`),
  KEY `IDX_email_hash` (`email_hash`)
) ENGINE=InnoDB;

CREATE TABLE `invites_archive` (
  `invite_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(200) NOT NULL DEFAULT '',
  `email_hash` binary(16) NOT NULL,
  `to_name` varchar(100) NOT NULL,
  `key_value` varchar(20) NOT NULL DEFAULT '',
  `invite_status_id` int(11) NOT NULL DEFAULT '0',
  `invited_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `points_awarded` float DEFAULT NULL,
  `kei_point_id_awarded` varchar(50) DEFAULT NULL,
  `reinvite_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `channel_id` int(10) unsigned DEFAULT NULL,
  `invited_user_id` int(11) DEFAULT NULL,
  `invites_source_id` int(10) unsigned NOT NULL,
  `attempts` int(10) unsigned NOT NULL DEFAULT '1',
  `opened_invite` binary(1) NOT NULL,
  `clicked_accept_link` binary(1) NOT NULL,
  PRIMARY KEY (`invite_id`),
  KEY `IDX_user_id` (`user_id`),
  KEY `IDX_invited_date` (`invited_date`),
  KEY `IDX_email_hash` (`email_hash`),
  KEY `IDX_invited_user_id` (`invited_user_id`)
) ENGINE=InnoDB;

CREATE TABLE `invites_source` (
  `invites_source_id` int(11) NOT NULL AUTO_INCREMENT,
  `invite_source_type_id` tinyint(3) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `from_email_hash` binary(16) NOT NULL,
  `from_domain` varchar(64) NOT NULL,
  `invite_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`invites_source_id`),
  KEY `IDX_created_date` (`created_date`),
  KEY `IDX_user_id` (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `invite_declines` (
  `invite_decline_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `invite_id` int(11) NOT NULL COMMENT 'FK to invites table',
  `invite_decline_reason_id` int(11) unsigned NOT NULL COMMENT 'FK to invite_decline_reason table',
  `additional_information` varchar(512) NOT NULL DEFAULT '' COMMENT 'any additional info about decline reason',
  `decline_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'datetime when user submitted invite decline',
  PRIMARY KEY (`invite_decline_id`)
) ENGINE=InnoDB COMMENT='Friend invite decline reasons and explanation';

CREATE TABLE `invite_decline_reasons` (
  `invite_decline_reason_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT 'brief name for decline reason',
  `description` varchar(100) NOT NULL DEFAULT '' COMMENT 'description explaining decline reason',
  PRIMARY KEY (`invite_decline_reason_id`)
) ENGINE=InnoDB COMMENT='Friend invite decline reasons';

CREATE TABLE `invite_import_audit` (
  `invite_import_audit_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email_entry_hash` binary(16) NOT NULL,
  `import_acct_email_rev` varchar(100) NOT NULL,
  `invite_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`invite_import_audit_id`),
  KEY `IDX_created_date` (`created_date`),
  KEY `IDX_user_id` (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `invite_ip_addresses` (
  `ip_address` bigint(20) unsigned NOT NULL,
  `number_of_times` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ip_address`)
) ENGINE=InnoDB COMMENT='Holds IP Addresses and how many times it was used for invite';

CREATE TABLE `invite_point_awards` (
  `invite_point_id` int(10) NOT NULL AUTO_INCREMENT,
  `point_award_amount` float NOT NULL DEFAULT '0',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT',
  `end_date` datetime DEFAULT NULL,
  `ip_limit` smallint(5) unsigned NOT NULL DEFAULT '5',
  `award_multiple` smallint(5) unsigned NOT NULL DEFAULT '5',
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`invite_point_id`)
) ENGINE=InnoDB;

CREATE TABLE `invite_source_type` (
  `invite_source_type_id` tinyint(3) NOT NULL,
  `source_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`invite_source_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `invite_status` (
  `invite_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `description` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`invite_status_id`)
) ENGINE=InnoDB;

CREATE TABLE `ip_bans` (
  `ip_address` int(10) unsigned NOT NULL,
  `ban_start_date` datetime NOT NULL,
  `ban_end_date` datetime NOT NULL,
  PRIMARY KEY (`ip_address`)
) ENGINE=InnoDB ROW_FORMAT=FIXED;

CREATE TABLE `join_communities` (
  `join_id` int(11) NOT NULL AUTO_INCREMENT,
  `source_community_id` int(11) NOT NULL,
  `join_community_id` int(11) NOT NULL,
  PRIMARY KEY (`join_id`),
  KEY `scid` (`source_community_id`),
  KEY `jcid` (`join_community_id`)
) ENGINE=InnoDB;

CREATE TABLE `join_themes` (
  `join_id` int(11) NOT NULL AUTO_INCREMENT,
  `community_id` int(11) NOT NULL DEFAULT '0',
  `css` text,
  `page_join_copy` text,
  `page_verify_copy` text,
  `page_complete_copy` text,
  `user_profile_theme_male_id` int(11) NOT NULL DEFAULT '0',
  `user_profile_theme_female_id` int(11) NOT NULL DEFAULT '0',
  `user_profile_video_id` int(11) NOT NULL DEFAULT '0',
  `user_profile_male_avatar_path` varchar(255) DEFAULT NULL,
  `user_profile_female_avatar_path` varchar(255) DEFAULT NULL,
  `email_body_top` varchar(255) DEFAULT NULL,
  `email_body_bottom` varchar(255) DEFAULT NULL,
  `email_subject` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`join_id`),
  KEY `cid` (`community_id`)
) ENGINE=InnoDB;

CREATE TABLE `kei_points` (
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT',
  `conversion_rate_to_dollar` float NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL DEFAULT '',
  `currency_symbol` varchar(50) NOT NULL DEFAULT '',
  `user_purchaseable` char(1) NOT NULL DEFAULT '',
  `display_format` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`kei_point_id`)
) ENGINE=InnoDB;

CREATE TABLE `leaderboard_excluded_channels` (
  `channel_id` int(11) NOT NULL,
  PRIMARY KEY (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `leaderboard_excluded_users` (
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `leader_board_configuration_options` (
  `leader_config_id` int(10) unsigned NOT NULL DEFAULT '0',
  `config_option_description` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`leader_config_id`)
) ENGINE=InnoDB;

CREATE TABLE `leader_board_time_periods` (
  `time_period_id` smallint(5) unsigned NOT NULL,
  `period_in_days` smallint(5) unsigned NOT NULL DEFAULT '0',
  `description` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`time_period_id`)
) ENGINE=InnoDB;

CREATE TABLE `license_subscription` (
  `license_subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `length_of_subscription` int(11) NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `amount_kei_point_id` varchar(50) NOT NULL DEFAULT '',
  `max_server_users` int(11) DEFAULT NULL,
  `disk_quota` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`license_subscription_id`)
) ENGINE=InnoDB;

CREATE TABLE `login_log` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `created_date` datetime NOT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_reason_code` enum('NORMAL','LOST','TERMINATED','FORCE','TIMEOUT','BANNED','SECURITY_VIO','UNKNOWN_NET','WEB_RESTART','SERVER_SHUTDOWN','DEAD_WEBSITE_DETECT') DEFAULT NULL,
  `server_name` varchar(100) NOT NULL DEFAULT '0',
  `ip_address` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`login_id`),
  KEY `IDX_created_date` (`created_date`),
  KEY `IDX_logout_date` (`logout_date`),
  KEY `IDX_user_id` (`user_id`),
  KEY `IDX_ip_address` (`ip_address`)
) ENGINE=InnoDB COMMENT='log of all logins for website';

CREATE TABLE `marketing_share_referrals` (
  `existing_user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User ID of referring user',
  `referred_user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User ID of user that was referred',
  `registered_date` datetime NOT NULL COMMENT 'Date referred user registered',
  `completed_checklist_date` datetime DEFAULT NULL COMMENT 'Date referred user completed checklist',
  PRIMARY KEY (`referred_user_id`,`existing_user_id`)
) ENGINE=InnoDB COMMENT='Tracks referrers to referrals and the status of referrals.';

CREATE TABLE `marketing_share_tracking` (
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'User Id of member',
  `number_of_views` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of times banner was viewed',
  `number_of_clicks` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Nuber of times banner was clicked',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB COMMENT='Tracks the views and clicks of banners from Share Kaneva.';

CREATE TABLE `mega_rave_history` (
  `mega_rave_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `rave_type_id` int(10) unsigned NOT NULL,
  `channel_id` int(11) NOT NULL DEFAULT '0',
  `zone_instance_id` int(11) NOT NULL DEFAULT '0',
  `zone_type` int(11) NOT NULL DEFAULT '0',
  `asset_id` int(11) NOT NULL DEFAULT '0',
  `rave_value` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'number of raves recieved - taken from value of rave_types.',
  `user_id_raver` int(11) NOT NULL,
  `price_paid` int(11) NOT NULL DEFAULT '0' COMMENT 'Price paid for the mega rave',
  `commission_received` int(11) NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL,
  PRIMARY KEY (`mega_rave_history_id`),
  KEY `fk_mega_rave_history_rave_types` (`rave_type_id`),
  CONSTRAINT `fk_mega_rave_history_rave_types` FOREIGN KEY (`rave_type_id`) REFERENCES `rave_types` (`rave_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB COMMENT='Holds history of mega raves given';

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) NOT NULL DEFAULT '0',
  `to_id` int(11) NOT NULL DEFAULT '0',
  `subject` varchar(100) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `message_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `replied` smallint(6) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `channel_id` int(11) DEFAULT NULL,
  `to_viewable` enum('U','R','D') NOT NULL COMMENT 'Unread, Read, Deleted',
  `from_viewable` enum('S','D') NOT NULL COMMENT 'Sent, Deleted',
  PRIMARY KEY (`message_id`),
  KEY `from_state` (`from_id`),
  KEY `all_the_tos` (`to_id`),
  KEY `IDX_fid_tid_tov_type` (`from_id`,`to_id`,`type`),
  KEY `IDX_toid_tovw_type` (`to_id`,`to_viewable`,`type`),
  KEY `IDX_to_viewable` (`to_viewable`),
  KEY `IDX_from_viewable` (`from_viewable`)
) ENGINE=InnoDB;

CREATE TABLE `messages_deleted` (
  `message_id` int(11) NOT NULL DEFAULT '0',
  `from_id` int(11) NOT NULL DEFAULT '0',
  `to_id` int(11) NOT NULL DEFAULT '0',
  `subject` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `message_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `replied` smallint(6) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `channel_id` int(11) DEFAULT NULL,
  `to_viewable` enum('U','R','D') NOT NULL COMMENT 'Unread, Read, Deleted',
  `from_viewable` enum('S','D') NOT NULL COMMENT 'Sent, Deleted',
  PRIMARY KEY (`message_id`),
  KEY `IDX_msg_date` (`message_date`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT;

CREATE TABLE `messages_non_friends` (
  `user_id` int(11) NOT NULL,
  `number_of_messages_sent` int(11) NOT NULL DEFAULT '0' COMMENT 'total number of messages sent to non-friends per date in last_message_sent_date column',
  `last_message_sent_date` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB ROW_FORMAT=FIXED;

CREATE TABLE `message_gifts` (
  `message_id` int(11) NOT NULL COMMENT 'FK to messages',
  `gift_id` int(11) unsigned NOT NULL COMMENT 'FK to gift_catalog_items',
  `gift_status` enum('U','A','R') NOT NULL DEFAULT 'U' COMMENT 'Unaccpeted, Accepted, Rejected',
  `gift_type` enum('2D','3D') DEFAULT '3D',
  PRIMARY KEY (`message_id`,`gift_id`,`gift_status`),
  KEY `INDEX_giftmsg_type` (`gift_type`)
) ENGINE=InnoDB COMMENT='M2M between messages and gift_catalog_items for gifting';

CREATE TABLE `meta_game_items` (
  `item_name` varchar(50) NOT NULL,
  `item_type` varchar(50) NOT NULL,
  `conversion_value_rewards` int(11) NOT NULL DEFAULT '0',
  `conversion_value_credits` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_name`),
  KEY `mgi_item_type_fk_idx` (`item_type`),
  CONSTRAINT `mgi_item_type_fk` FOREIGN KEY (`item_type`) REFERENCES `meta_game_item_types` (`item_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Enumeration of the meta game items that are available.';

CREATE TABLE `meta_game_item_conversions` (
  `conversion_id` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `item_type` varchar(50) NOT NULL,
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL,
  `num_items_converted` int(11) NOT NULL COMMENT 'Number of items converted',
  `currency_amount_received` int(11) NOT NULL COMMENT 'Currency amount received from conversion',
  `conversion_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_id` bigint(20) NOT NULL COMMENT 'FK to kaneva.wok_transaction_log',
  PRIMARY KEY (`conversion_id`),
  KEY `uid` (`user_id`),
  KEY `tid` (`trans_id`),
  KEY `wic_conversion_date_idx` (`conversion_date`),
  KEY `mgic_item_type_fk_idx` (`item_type`),
  CONSTRAINT `meta_game_item_conversions_ibfk_1` FOREIGN KEY (`trans_id`) REFERENCES `wok_transaction_log` (`trans_id`),
  CONSTRAINT `meta_game_item_conversions_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `mgic_item_type_fk` FOREIGN KEY (`item_type`) REFERENCES `meta_game_item_types` (`item_type`) ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='User Item Conversions';

CREATE TABLE `meta_game_item_types` (
  `item_type` varchar(50) NOT NULL,
  `redemption_rewards_limit` int(11) NOT NULL DEFAULT '0',
  `redemption_credits_limit` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_type`)
) ENGINE=InnoDB COMMENT='Enumeration of the types of meta game items that are available.';

CREATE TABLE `new_channels_by_day` (
  `date_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`date_stamp`)
) ENGINE=InnoDB;

CREATE TABLE `notifications` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `notified` tinyint(4) NOT NULL DEFAULT '0',
  `notify_date` datetime DEFAULT NULL,
  `packet_id` int(11) NOT NULL DEFAULT '0',
  `level_id` int(11) NOT NULL DEFAULT '0',
  `achievement_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`notification_id`),
  KEY `idx_userId` (`user_id`),
  KEY `idx_notified` (`notified`),
  KEY `idx_level` (`level_id`),
  KEY `idx_packet` (`packet_id`),
  KEY `idx_achievement` (`achievement_id`),
  KEY `idx_userid_notified` (`user_id`,`notified`)
) ENGINE=InnoDB;

CREATE TABLE `notifications_profile` (
  `idnotifications_profile` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `notification_type` smallint(6) unsigned NOT NULL DEFAULT '0',
  `xml_data` text,
  `notif_text` varchar(200) DEFAULT NULL,
  `param1` varchar(45) DEFAULT NULL,
  `param2` varchar(45) DEFAULT NULL,
  `param3` varchar(45) DEFAULT NULL,
  `param4` varchar(45) DEFAULT NULL,
  `added_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_dismissed` int(11) DEFAULT '0',
  PRIMARY KEY (`idnotifications_profile`),
  KEY `userIdx` (`user_id`),
  KEY `dismissedIdx` (`user_dismissed`)
) ENGINE=InnoDB;

CREATE TABLE `notification_requests` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `number_of_requests` int(11) NOT NULL,
  PRIMARY KEY (`notification_id`),
  UNIQUE KEY `userIdGameId` (`user_id`,`game_id`)
) ENGINE=InnoDB COMMENT='Used to keep track of app request notifications';

CREATE TABLE `notification_signup` (
  `notify_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL DEFAULT '',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`notify_id`)
) ENGINE=InnoDB;

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_type` int(11) NOT NULL DEFAULT '0',
  `description` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `purchase_date` datetime DEFAULT NULL,
  `transaction_status_id` int(11) NOT NULL DEFAULT '0',
  `gross_point_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `download_end_date` datetime DEFAULT NULL,
  `ip_address` varchar(30) DEFAULT NULL,
  `error_description` varchar(100) DEFAULT NULL,
  `download_stop_date` datetime DEFAULT NULL,
  `point_transaction_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `FK_orders_purchase_transaction_status` (`transaction_status_id`),
  KEY `FK_orders_users` (`user_id`),
  KEY `INDEX_purchase_date` (`purchase_date`),
  KEY `utp` (`user_id`,`transaction_status_id`,`purchase_type`)
) ENGINE=InnoDB;

CREATE TABLE `order_billing_information` (
  `order_billing_information_id` int(11) NOT NULL AUTO_INCREMENT,
  `name_on_card` varchar(100) NOT NULL DEFAULT '',
  `card_number` varchar(40) NOT NULL DEFAULT '',
  `card_type` varchar(50) NOT NULL DEFAULT '',
  `exp_month` varchar(50) NOT NULL DEFAULT '',
  `exp_day` varchar(50) NOT NULL DEFAULT '',
  `exp_year` varchar(50) DEFAULT NULL,
  `address_name` varchar(100) NOT NULL DEFAULT '',
  `phone_number` varchar(50) NOT NULL DEFAULT '',
  `address1` varchar(100) NOT NULL DEFAULT '',
  `address2` varchar(100) DEFAULT NULL,
  `city` varchar(100) NOT NULL DEFAULT '',
  `state_code` varchar(100) NOT NULL DEFAULT '',
  `zip_code` varchar(25) NOT NULL DEFAULT '',
  `country_id` char(2) NOT NULL DEFAULT '',
  PRIMARY KEY (`order_billing_information_id`)
) ENGINE=InnoDB;

CREATE TABLE `order_items` (
  `order_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `asset_id` int(11) DEFAULT NULL,
  `user_asset_subscription_id` int(11) DEFAULT NULL,
  `asset_subscription_id` int(11) DEFAULT NULL,
  `community_member_subscription_id` int(11) DEFAULT NULL,
  `featured_asset_id` int(11) DEFAULT NULL,
  `game_license_id` int(11) DEFAULT NULL,
  `point_amount` float NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `royalty_amount` decimal(12,3) DEFAULT NULL,
  `royalty_paid` char(1) DEFAULT NULL,
  `royalty_user_id_paid` int(11) DEFAULT NULL,
  `number_of_downloads` int(11) NOT NULL DEFAULT '0',
  `number_of_downloads_allowed` int(11) NOT NULL DEFAULT '5',
  `number_of_streams` int(11) NOT NULL DEFAULT '0',
  `token` varchar(20) DEFAULT NULL,
  `submitted_as_inventory` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`order_item_id`),
  KEY `FK_order_items_orders` (`order_id`),
  KEY `FK_asset_id` (`asset_id`),
  KEY `royalty_user_id_paid` (`royalty_user_id_paid`),
  KEY `order_id_asset_id` (`order_id`,`asset_id`)
) ENGINE=InnoDB;

CREATE TABLE `order_item_download` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `client_ip` varchar(45) DEFAULT NULL,
  `datetime_downloaded` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `download_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `order_transaction_amounts_paid` (
  `order_id` int(11) NOT NULL DEFAULT '0',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT',
  `amount` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`,`kei_point_id`),
  KEY `FK_order_transaction_amounts_paid_kei_points` (`kei_point_id`)
) ENGINE=InnoDB;

CREATE TABLE `parsed_order_items_word_count` (
  `order_item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `word_id` int(11) unsigned DEFAULT NULL,
  `total` int(7) unsigned DEFAULT NULL,
  UNIQUE KEY `id_index` (`order_item_id`,`word_id`),
  KEY `word_src` (`word_id`)
) ENGINE=InnoDB;

CREATE TABLE `payment_methods` (
  `payment_method_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `adaptor` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`payment_method_id`)
) ENGINE=InnoDB;

CREATE TABLE `polls` (
  `poll_id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_question` varchar(250) NOT NULL DEFAULT '',
  `created_on_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`poll_id`)
) ENGINE=InnoDB;

CREATE TABLE `poll_options` (
  `poll_option_id` int(11) NOT NULL DEFAULT '0',
  `poll_id` int(11) NOT NULL DEFAULT '0',
  `option_text` varchar(100) NOT NULL DEFAULT '',
  `option_votes` int(11) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`poll_option_id`,`poll_id`)
) ENGINE=InnoDB;

CREATE TABLE `poll_votes` (
  `poll_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `poll_option_id` int(11) NOT NULL DEFAULT '0',
  `vote_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`poll_id`,`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `popular_keywords` (
  `keyword` varchar(100) NOT NULL DEFAULT '',
  `total_count` int(11) NOT NULL DEFAULT '0',
  `asset_type_id` int(11) NOT NULL DEFAULT '-1',
  KEY `key_word_search` (`keyword`,`asset_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `popular_keywords_bku` (
  `keyword` varchar(100) NOT NULL DEFAULT '',
  `total_count` int(11) NOT NULL DEFAULT '0',
  `asset_type_id` int(11) NOT NULL DEFAULT '0',
  KEY `key_word_search` (`keyword`,`asset_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `posts_7_days` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(10) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE=InnoDB;

CREATE TABLE `potty_mouth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bad_word` varchar(50) NOT NULL DEFAULT '',
  `replace_with` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `premium_item_credit_redemption` (
  `purchase_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key to item_purchases table',
  `community_id` int(11) NOT NULL COMMENT 'Foreign Key to kaneva.communities table',
  `redemption_date` datetime DEFAULT NULL COMMENT 'Date the transaction credits were redeemed. Null if not redeemed',
  PRIMARY KEY (`purchase_id`),
  KEY `community_id` (`community_id`),
  CONSTRAINT `ipr_community_id_fk` FOREIGN KEY (`community_id`) REFERENCES `communities` (`community_id`),
  CONSTRAINT `ipr_purchase_id_fk` FOREIGN KEY (`purchase_id`) REFERENCES `shopping`.`item_purchases` (`purchase_id`)
) ENGINE=InnoDB COMMENT='Redemption date for Premium Item credit redemptions';

CREATE TABLE `promotional_offers` (
  `promotion_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT',
  `dollar_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `kei_point_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `free_points_awarded_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `free_kei_point_ID` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT',
  `is_special` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `promotion_description` varchar(100) DEFAULT NULL,
  `promotional_offers_type_id` smallint(6) NOT NULL DEFAULT '0',
  `wok_pass_group_id` int(11) NOT NULL,
  `sku` smallint(5) unsigned DEFAULT '0',
  `bundle_title` varchar(100) NOT NULL DEFAULT '',
  `bundle_subheading1` varchar(100) DEFAULT NULL,
  `bundle_subheading2` varchar(100) DEFAULT NULL,
  `promotion_start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `promotion_end` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `promotion_list_heading` varchar(100) NOT NULL DEFAULT '',
  `highlight_color` char(7) DEFAULT NULL,
  `special_background_color` char(7) DEFAULT NULL,
  `special_background_image` varchar(500) DEFAULT NULL,
  `special_sticker_image` varchar(500) DEFAULT NULL,
  `promotional_package_label` varchar(100) DEFAULT NULL,
  `special_font_color` char(7) DEFAULT NULL,
  `coupon_code` varchar(45) DEFAULT NULL COMMENT 'Code that is used to referrence this promotion.',
  `use_display_options` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'flag to signifiy if promotion should use display options or not',
  `original_price` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'this field is just to store a bogus price for marketing purposes',
  `adrotator_xml_path` varchar(500) NOT NULL DEFAULT '',
  `value_of_credits` decimal(12,2) DEFAULT NULL,
  `modifiers_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`promotion_id`),
  KEY `idx_isSpecial` (`is_special`),
  KEY `idx_promotionStart` (`promotion_start`),
  KEY `idx_promotionEnd` (`promotion_end`)
) ENGINE=InnoDB;

CREATE TABLE `promotional_offer_items` (
  `wok_item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `promotion_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `gift_card_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `item_description` varchar(125) NOT NULL DEFAULT '',
  `alternate_description` varchar(125) DEFAULT NULL,
  `market_price` smallint(5) unsigned NOT NULL DEFAULT '0',
  `quantity` smallint(5) unsigned NOT NULL DEFAULT '0',
  `gender` enum('M','F','U') NOT NULL DEFAULT 'U',
  `promotion_offer_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `modifiers_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`wok_item_id`,`promotion_id`,`gift_card_id`,`community_id`),
  KEY `idx_promo_item_id` (`promotion_offer_id`),
  KEY `g` (`gender`)
) ENGINE=InnoDB;

CREATE TABLE `promotional_offer_type` (
  `promotional_offer_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `promotional_offer_type` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`promotional_offer_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `promotions_to_subscriptions` (
  `promotion_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'primary key of the promotions table',
  `subscription_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'primary key of the subscriptions table',
  PRIMARY KEY (`promotion_id`,`subscription_id`)
) ENGINE=InnoDB COMMENT='many to many table to relating promotions to subscriptions';

CREATE TABLE `purchase_point_transactions` (
  `point_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(200) DEFAULT NULL,
  `billing_information_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `order_billing_information_id` int(11) DEFAULT NULL,
  `payment_method_id` int(11) NOT NULL DEFAULT '0',
  `amount_debited` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `amount_credited` decimal(19,4) DEFAULT NULL,
  `tax` decimal(19,4) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `point_bucket_id` int(11) DEFAULT NULL,
  `gift_card_id` int(11) NOT NULL DEFAULT '0',
  `authorization_id` varchar(10) DEFAULT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `transaction_status` int(20) DEFAULT NULL,
  `transaction_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `error_description` varchar(255) DEFAULT NULL,
  `user_license_subscription_id` int(11) DEFAULT NULL,
  `ip_address` varchar(30) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`point_transaction_id`),
  KEY `user_id` (`user_id`),
  KEY `IDX_transaction_date` (`transaction_date`),
  KEY `IDX_order_id` (`order_id`)
) ENGINE=InnoDB;

CREATE TABLE `purchase_point_transactions_status` (
  `transaction_status_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  `long_description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`transaction_status_id`)
) ENGINE=InnoDB;

CREATE TABLE `purchase_point_transaction_amounts` (
  `point_transaction_id` int(11) NOT NULL DEFAULT '0',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT',
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`point_transaction_id`,`kei_point_id`)
) ENGINE=InnoDB;

CREATE TABLE `purchase_transactions` (
  `purchase_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_type` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `asset_id` int(11) DEFAULT NULL,
  `download_end_date` datetime DEFAULT NULL,
  `user_asset_subscription_id` int(11) DEFAULT NULL,
  `community_member_subscription_id` int(11) DEFAULT NULL,
  `purchase_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `community_id` int(11) DEFAULT NULL,
  `royalty_sale` char(1) NOT NULL DEFAULT '',
  `master_user_id_paid_royalties` int(11) DEFAULT NULL,
  `transaction_status` int(11) DEFAULT NULL,
  `gross_point_amount` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`purchase_transaction_id`)
) ENGINE=InnoDB;

CREATE TABLE `purchase_transaction_amounts` (
  `purchase_transaction_id` int(11) NOT NULL DEFAULT '0',
  `kei_point_id` varchar(50) NOT NULL DEFAULT '',
  `amount` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`purchase_transaction_id`,`kei_point_id`)
) ENGINE=InnoDB;

CREATE TABLE `purchase_transaction_status` (
  `transaction_status_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`transaction_status_id`)
) ENGINE=InnoDB;

CREATE TABLE `rank_by_month` (
  `user_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `rave_types` (
  `rave_type_id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL COMMENT 'Name of the rave type',
  `description` varchar(100) NOT NULL COMMENT 'Description of the rave type',
  `mega_rave_value` int(10) unsigned NOT NULL DEFAULT '25' COMMENT 'The number of raves a mega rave is worth',
  `price_single_rave` int(10) unsigned DEFAULT NULL COMMENT 'Price of a rave in rewards/credits',
  `price_mega_rave` int(10) unsigned DEFAULT NULL COMMENT 'Price of a mega rave in rewards/credits',
  `number_allowed_free` int(10) unsigned DEFAULT '1' COMMENT 'Number of free raves allowed',
  `percent_commission` int(10) unsigned DEFAULT '10' COMMENT 'Percent commision recieved from pay rave.',
  PRIMARY KEY (`rave_type_id`)
) ENGINE=InnoDB COMMENT='Defines type of raves in the system';

CREATE TABLE `related_assets` (
  `asset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `related_asset_id` int(10) unsigned NOT NULL,
  `downloads` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`asset_id`,`related_asset_id`),
  KEY `dt` (`created_date`),
  KEY `raid` (`related_asset_id`)
) ENGINE=InnoDB;

CREATE TABLE `related_items` (
  `_order` double NOT NULL,
  `base_asset_id` int(11) NOT NULL,
  `related_asset_id` int(11) NOT NULL,
  `mature` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`_order`),
  UNIQUE KEY `base_asset_id` (`base_asset_id`,`related_asset_id`),
  KEY `baid` (`base_asset_id`),
  KEY `baid_ord` (`_order`,`base_asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `restricted_words` (
  `restricted_words_id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(50) NOT NULL DEFAULT '',
  `replace_with` varchar(50) DEFAULT '',
  `restriction_type_id` int(11) NOT NULL,
  `match_type` enum('exact','anywhere','start') DEFAULT NULL,
  PRIMARY KEY (`restricted_words_id`)
) ENGINE=InnoDB;

CREATE TABLE `restriction_types` (
  `restriction_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`restriction_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `reward_events` (
  `reward_event_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `event_name` varchar(100) NOT NULL DEFAULT '' COMMENT 'name of event',
  `transaction_type_id` smallint(5) unsigned NOT NULL COMMENT 'FK to transaction_type table',
  PRIMARY KEY (`reward_event_id`)
) ENGINE=InnoDB COMMENT='Events users can get rewards for participating in';

CREATE TABLE `reward_interval_tracking_daily` (
  `user_id` int(11) NOT NULL,
  `packet_id` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `packet_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`packet_id`,`created_date`),
  KEY `IDX_date_rewarded` (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `reward_log` (
  `reward_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `amount` float NOT NULL,
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL,
  `transaction_type` smallint(5) unsigned NOT NULL,
  `return_code` int(10) unsigned NOT NULL,
  `reward_event_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to reward_events table',
  `wok_transaction_log_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to wok_transaction_log table',
  PRIMARY KEY (`reward_log_id`)
) ENGINE=InnoDB;

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(80) DEFAULT NULL,
  `role_value` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB;

CREATE TABLE `royalty_payments` (
  `payment_id` int(11) NOT NULL DEFAULT '0',
  `user_id_paid` int(11) NOT NULL DEFAULT '0',
  `check_number` varchar(50) DEFAULT NULL,
  `payment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `period_start_date` datetime DEFAULT NULL,
  `period_end_date` datetime DEFAULT NULL,
  `total_amount` decimal(12,3) NOT NULL DEFAULT '0.000',
  `amount_paid` decimal(12,3) NOT NULL DEFAULT '0.000',
  `tax_withheld` decimal(12,3) NOT NULL DEFAULT '0.000',
  `notes` varchar(200) DEFAULT NULL,
  `user_id_created` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB;

CREATE TABLE `rpt_new_file_upload` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_input` date NOT NULL DEFAULT '0000-00-00',
  `num_files_uploaded` int(10) unsigned NOT NULL DEFAULT '0',
  `asset_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `datetime_calculated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `rpt_new_user_signup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_input` date NOT NULL DEFAULT '0000-00-00',
  `num_new_users` int(10) unsigned NOT NULL DEFAULT '0',
  `datetime_calculated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `rpt_time_traffic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_input` date NOT NULL DEFAULT '0000-00-00',
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0',
  `num_downloads` int(10) unsigned NOT NULL DEFAULT '0',
  `datetime_calculated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `rss_suggested_feeds` (
  `feed_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(200) DEFAULT '',
  PRIMARY KEY (`feed_id`)
) ENGINE=InnoDB;

CREATE TABLE `schema_versions` (
  `version` decimal(11,4) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

CREATE TABLE `schools` (
  `school_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `city` varchar(45) NOT NULL,
  `state` char(2) NOT NULL,
  `user_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of users attending this school',
  PRIMARY KEY (`school_id`),
  UNIQUE KEY `name` (`name`) USING BTREE,
  KEY `city` (`city`(10)),
  KEY `state` (`state`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_communities` (
  `community_count` int(11) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_profiles` (
  `profile_count` int(11) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_site_members` (
  `member_count` int(11) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `spender_activity` (
  `user_id` int(10) unsigned NOT NULL,
  `total_spent` double NOT NULL,
  `active_spender` enum('Y','N') NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `spender_activity_counts` (
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  `active_spender_counts` int(11) DEFAULT NULL,
  `inactive_spender_counts` int(11) DEFAULT NULL,
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `sp_run_log` (
  `sp_run_log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `sp_name` varchar(256) NOT NULL COMMENT 'name of the stored procedure',
  `run_start_time` datetime NOT NULL COMMENT 'when started',
  `run_end_time` datetime NOT NULL COMMENT 'when ended',
  `duration_seconds` int(11) NOT NULL COMMENT 'duration in seconds',
  PRIMARY KEY (`sp_run_log_id`),
  KEY `IDX_start_time` (`run_start_time`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT COMMENT='run duration information for stored procedures';

CREATE TABLE `states` (
  `state_code` char(2) NOT NULL DEFAULT '',
  `state` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`state_code`)
) ENGINE=InnoDB;

CREATE TABLE `stats_converter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL DEFAULT '0',
  `start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `processing_time_seconds` int(11) NOT NULL DEFAULT '0',
  `filepath` varchar(255) NOT NULL DEFAULT '',
  `source_size` int(11) NOT NULL DEFAULT '0',
  `target_size` int(11) NOT NULL DEFAULT '0',
  `result` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `stats_publishing_processor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_time` datetime DEFAULT '0000-00-00 00:00:00',
  `num_processed` int(11) NOT NULL DEFAULT '0',
  `num_to_process` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `learn_more_content` text COMMENT 'hold html based content for pass detials ',
  `discount_percent` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'stores any discount tha tis asscociated with this subscription',
  `keep_subscription_reasons` text NOT NULL COMMENT 'reasons to keep the subscription (for display)',
  `learn_more_image_path` varchar(200) DEFAULT NULL COMMENT 'image path for the passes details page',
  `upsell_image_path` varchar(200) DEFAULT NULL COMMENT 'image path for the upsell images',
  `name` varchar(45) NOT NULL COMMENT 'Name of the subscription',
  `price` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'Price of the subscription',
  `introductory_price` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'Initial price',
  `introductory_end_date` datetime DEFAULT NULL COMMENT 'Intro price end date',
  `grandfather_price` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'Is the price grandfathered for changes',
  `term` int(11) NOT NULL DEFAULT '0' COMMENT 'Term offered for the subscription',
  `monthly_allowance` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'amount of credits paid to the user monthly',
  `days_free` int(11) NOT NULL DEFAULT '0' COMMENT 'How long is the free trial in days',
  PRIMARY KEY (`subscription_id`)
) ENGINE=InnoDB COMMENT='Holds types of subscriptions (SKU in PRD)';

CREATE TABLE `subscriptions_to_passgroups` (
  `subscription_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'foreign key to the subscriptions table',
  `pass_group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'foreign key to the wok pass groups table',
  PRIMARY KEY (`subscription_id`,`pass_group_id`)
) ENGINE=InnoDB COMMENT='table correlating subscriptions to pass groups';

CREATE TABLE `subscription_billing_information` (
  `sub_billing_information_id` int(11) NOT NULL AUTO_INCREMENT,
  `name_on_card` varchar(100) NOT NULL DEFAULT '',
  `card_number` varchar(40) NOT NULL DEFAULT '',
  `card_type` varchar(50) NOT NULL DEFAULT '',
  `exp_month` varchar(50) NOT NULL DEFAULT '',
  `exp_day` varchar(50) NOT NULL DEFAULT '',
  `exp_year` varchar(50) DEFAULT NULL,
  `address_name` varchar(100) NOT NULL DEFAULT '',
  `phone_number` varchar(50) NOT NULL DEFAULT '',
  `address1` varchar(100) NOT NULL DEFAULT '',
  `address2` varchar(100) DEFAULT NULL,
  `city` varchar(100) NOT NULL DEFAULT '',
  `state_code` varchar(100) NOT NULL DEFAULT '',
  `zip_code` varchar(25) NOT NULL DEFAULT '',
  `country_id` char(2) NOT NULL DEFAULT '',
  PRIMARY KEY (`sub_billing_information_id`)
) ENGINE=InnoDB;

CREATE TABLE `subscription_date_history` (
  `subscription_date_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_subscription_id` int(10) unsigned DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL COMMENT 'Date this record was inserted/updated',
  `old_enddate` datetime DEFAULT NULL COMMENT 'previous end date of this subscription',
  `new_enddate` datetime DEFAULT NULL COMMENT 'new end date of this subscription',
  PRIMARY KEY (`subscription_date_history_id`),
  KEY `sdh_usi` (`user_subscription_id`)
) ENGINE=InnoDB;

CREATE TABLE `subscription_entitlements` (
  `entitlement_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `subscription_id` int(10) unsigned NOT NULL COMMENT 'Subscription ID for this entitlement',
  `points` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'Points you get',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT' COMMENT 'Type of points',
  `global_id` int(11) NOT NULL DEFAULT '0' COMMENT 'The glid you recieve',
  `quantity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quantity of glid recieved',
  `keep_on_cancel` enum('Y','N') NOT NULL DEFAULT 'Y' COMMENT 'Do you get to keep this entitle on subscriptoin cancel?',
  PRIMARY KEY (`entitlement_id`),
  KEY `test` (`subscription_id`)
) ENGINE=InnoDB COMMENT='Types of entitlements for a subscription';

CREATE TABLE `subscription_pending_allowance` (
  `sub_pending_allowance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `user_subscription_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The user subscription id',
  `award_attempt_date` datetime DEFAULT NULL COMMENT 'Date to attempt award',
  `processed` enum('Y','N') DEFAULT 'N' COMMENT 'Has this been processed yet',
  `success` enum('Y','N') DEFAULT 'N' COMMENT 'Has the award succeded',
  `order_id` int(11) NOT NULL DEFAULT '0' COMMENT 'The order id',
  PRIMARY KEY (`sub_pending_allowance_id`),
  KEY `idx_prossed` (`processed`)
) ENGINE=InnoDB;

CREATE TABLE `subscription_status` (
  `status_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `name` varchar(45) DEFAULT NULL COMMENT 'Name of status',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description of status',
  PRIMARY KEY (`status_id`) USING BTREE
) ENGINE=InnoDB COMMENT='Holds status of subscriptions';

CREATE TABLE `subscription_terms` (
  `subscription_term_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'id to indicate the subscription term length',
  `subscription_term` varchar(50) NOT NULL DEFAULT '' COMMENT 'description of what the term id represents',
  PRIMARY KEY (`subscription_term_id`)
) ENGINE=InnoDB COMMENT='2nd normal form table for subscription terms';

CREATE TABLE `summary_assets_by_channel_by_day` (
  `channel_id` int(10) unsigned NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`,`dt_stamp`)
) ENGINE=InnoDB;

CREATE TABLE `summary_assets_by_connection` (
  `channel_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`)
) ENGINE=InnoDB;

CREATE TABLE `summary_assets_by_day` (
  `date_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`date_stamp`)
) ENGINE=InnoDB;

CREATE TABLE `summary_assets_by_type` (
  `asset_type_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`asset_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `summary_assets_by_type_by_channel` (
  `channel_id` int(10) unsigned NOT NULL,
  `asset_type_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`,`asset_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `summary_assets_by_type_by_user` (
  `asset_type_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`asset_type_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `summary_assets_by_user_by_day` (
  `user_id` int(10) unsigned NOT NULL,
  `date_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`date_stamp`)
) ENGINE=InnoDB;

CREATE TABLE `summary_assets_shared` (
  `asset_id` int(10) unsigned NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`asset_id`,`dt_stamp`),
  KEY `c` (`count`),
  KEY `ds` (`dt_stamp`)
) ENGINE=InnoDB;

CREATE TABLE `summary_assets_viewed` (
  `asset_id` int(10) unsigned NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`asset_id`,`dt_stamp`),
  KEY `c` (`count`),
  KEY `ds` (`dt_stamp`)
) ENGINE=InnoDB;

CREATE TABLE `summary_asset_channels` (
  `asset_id` int(11) NOT NULL DEFAULT '0',
  `community_id` int(11) NOT NULL DEFAULT '0',
  `is_adult` char(1) DEFAULT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `is_personal` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name_no_spaces` varchar(100) NOT NULL DEFAULT '',
  `thumbnail_small_path` varchar(255) NOT NULL DEFAULT '',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '1980-01-01 00:00:00',
  PRIMARY KEY (`asset_id`,`community_id`),
  KEY `ia` (`is_adult`),
  KEY `n` (`name`),
  KEY `is_personal` (`is_personal`),
  KEY `nns` (`name_no_spaces`),
  KEY `tn` (`thumbnail_small_path`),
  KEY `cid` (`creator_id`),
  KEY `cd` (`created_date`),
  KEY `atn` (`asset_id`,`thumbnail_small_path`)
) ENGINE=InnoDB;

CREATE TABLE `summary_asset_diggs` (
  `asset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`asset_id`,`date_stamp`),
  KEY `IDX_date_stamp` (`date_stamp`)
) ENGINE=InnoDB;

CREATE TABLE `summary_channel_diggs` (
  `channel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`,`dt_stamp`)
) ENGINE=InnoDB;

CREATE TABLE `summary_comments_by_day` (
  `channel_id` int(10) unsigned NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`channel_id`,`dt_stamp`)
) ENGINE=InnoDB;

CREATE TABLE `summary_comments_by_user_by_day` (
  `comment_date` date NOT NULL DEFAULT '0000-00-00',
  `comment_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_date`,`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `summary_community_members` (
  `channel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`channel_id`,`dt_stamp`)
) ENGINE=InnoDB;

CREATE TABLE `summary_friends_by_day` (
  `user_id` int(11) NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(11) DEFAULT '0',
  PRIMARY KEY (`user_id`,`dt_stamp`),
  KEY `dt` (`dt_stamp`),
  KEY `cnt` (`count`),
  KEY `uid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `summary_lb_temp` (
  `user_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `profile_picture` int(11) NOT NULL DEFAULT '0',
  `raves_channel` int(11) NOT NULL DEFAULT '0',
  `channels_created` int(11) NOT NULL DEFAULT '0',
  `friend_accepts` int(11) NOT NULL DEFAULT '0',
  `raves_media` int(11) NOT NULL DEFAULT '0',
  `channel_members` int(11) NOT NULL DEFAULT '0',
  `join_wok` int(11) NOT NULL DEFAULT '0',
  `photos_uploaded` int(11) NOT NULL DEFAULT '0',
  `video_uploaded` int(11) NOT NULL DEFAULT '0',
  `music_uploaded` int(11) NOT NULL DEFAULT '0',
  `people_invited` int(11) NOT NULL DEFAULT '0',
  `page_pimped` int(11) NOT NULL DEFAULT '0',
  `raves_your_profile` int(11) NOT NULL DEFAULT '0',
  `total_points` int(11) NOT NULL DEFAULT '0',
  `name_no_spaces` varchar(100) NOT NULL DEFAULT '',
  `thumbnail_small_path` varchar(255) NOT NULL DEFAULT '',
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

CREATE TABLE `summary_most_popular` (
  `count` int(11) DEFAULT NULL,
  `channel_id` int(11) NOT NULL,
  `username` varchar(80) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `location` varchar(80) DEFAULT NULL,
  `name_no_spaces` varchar(80) DEFAULT NULL,
  `thumbnail_small_path` varchar(255) DEFAULT NULL,
  `thumbnail_medium_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`channel_id`),
  KEY `c` (`count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `summary_most_popular_assets` (
  `count` int(10) unsigned NOT NULL,
  `asset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_type_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`asset_id`),
  KEY `at` (`asset_type_id`),
  KEY `c` (`count`)
) ENGINE=InnoDB;

CREATE TABLE `summary_new_assets` (
  `asset_id` int(11) NOT NULL,
  `asset_type_id` int(11) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `asset_rating_id` int(11) DEFAULT NULL,
  `publis_status_id` int(11) DEFAULT NULL,
  `thumbnail_small_path` varchar(255) DEFAULT NULL,
  `thumbnail_medium_path` varchar(255) DEFAULT NULL,
  `thumbnail_gen` tinyint(4) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(80) DEFAULT NULL,
  `status_id` tinyint(4) DEFAULT NULL,
  `number_of_downloads` int(11) DEFAULT NULL,
  `number_of_shares` int(11) DEFAULT NULL,
  `number_of_comments` int(11) DEFAULT NULL,
  `number_of_diggs` int(11) DEFAULT NULL,
  PRIMARY KEY (`asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `summary_new_channels` (
  `community_id` int(11) NOT NULL,
  `cx` int(11) NOT NULL,
  `community_name` varchar(80) DEFAULT NULL,
  `name_no_spaces` varchar(80) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_public` char(1) DEFAULT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `username` varchar(80) DEFAULT NULL,
  `thumbnail_small_path` varchar(255) DEFAULT NULL,
  `thumbnail_medium_path` varchar(255) DEFAULT NULL,
  `location` varchar(80) DEFAULT NULL,
  `number_of_members` int(11) DEFAULT NULL,
  `number_of_views` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_personal` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`community_id`),
  KEY `cx` (`cx`),
  KEY `is_personal` (`is_personal`),
  KEY `created_date` (`created_date`),
  KEY `pub` (`is_public`),
  KEY `adult` (`is_adult`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

CREATE TABLE `summary_ppt_by_day` (
  `user_id` int(10) unsigned NOT NULL,
  `dt_created` date NOT NULL,
  `total` double NOT NULL,
  `transaction_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`dt_created`)
) ENGINE=InnoDB;

CREATE TABLE `summary_ppt_completed_kpoint_transactions_by_day` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `transaction_date` date NOT NULL DEFAULT '0000-00-00',
  `total` double DEFAULT NULL,
  PRIMARY KEY (`user_id`,`transaction_date`),
  KEY `td` (`transaction_date`)
) ENGINE=InnoDB;

CREATE TABLE `summary_signups_by_day` (
  `date_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`date_stamp`)
) ENGINE=InnoDB;

CREATE TABLE `summary_transactions_by_day` (
  `transaction_date` date NOT NULL DEFAULT '0000-00-00',
  `payer_count` int(11) NOT NULL DEFAULT '0',
  `verified_transaction_count` int(11) NOT NULL DEFAULT '0',
  `failed_transaction_count` int(11) NOT NULL DEFAULT '0',
  `total_transaction_count` int(11) NOT NULL DEFAULT '0',
  `purchase_amount_debited` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`transaction_date`)
) ENGINE=InnoDB;

CREATE TABLE `superrewards_log` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto number for various uses like reporting',
  `sr_transaction_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'super rewards transaction id',
  `wok_transaction_log_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'foreign key to the wok_transaction_log table',
  `credits_awarded` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'credits awarded this transaction',
  `credits_all_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'credits accumlated to date',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'kaneva user id',
  `sr_offer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'the specific offer id the user to advantage of',
  `security_key` varchar(100) NOT NULL DEFAULT '' COMMENT 'security used to verify the transaction',
  `award_status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'indication of if transactions was successful - 0 fail - 1 success',
  `award_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'date the award attempt took place',
  `requesting_ip` int(11) NOT NULL DEFAULT '0' COMMENT 'date the award attempt took place',
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB COMMENT='table to audit the super rewards program';

CREATE TABLE `threads_base` (
  `thread_id` int(11) NOT NULL AUTO_INCREMENT,
  `thread_type_id` int(11) NOT NULL DEFAULT '0',
  `topic_id` int(11) NOT NULL DEFAULT '0',
  `parent_thread_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `rating` float DEFAULT NULL,
  `ip_address` varchar(30) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`thread_id`),
  KEY `summary_index` (`topic_id`,`thread_type_id`,`status_id`,`created_date`),
  KEY `thead_statis_topic_user` (`topic_id`,`thread_type_id`,`user_id`,`status_id`),
  KEY `sort_by_update_date` (`last_updated_date`),
  KEY `lu_uid_tid` (`last_updated_user_id`,`topic_id`),
  KEY `cd_tt_uid_sid` (`created_date`,`thread_type_id`,`user_id`,`status_id`),
  KEY `tt_uid_sid` (`thread_type_id`,`user_id`,`status_id`)
) ENGINE=InnoDB;

CREATE TABLE `thread_type` (
  `thread_type_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(20) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`thread_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `tmp_channel_cnt` (
  `HowMany` bigint(21) NOT NULL DEFAULT '0',
  `channel_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB;

CREATE TABLE `tmp_channel_shares` (
  `channel_id` int(11) NOT NULL,
  `HowMany` bigint(21) NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`)
) ENGINE=InnoDB;

CREATE TABLE `topics_base` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) DEFAULT NULL,
  `community_id` int(11) DEFAULT NULL,
  `created_user_id` int(11) NOT NULL DEFAULT '0',
  `sticky` char(1) NOT NULL DEFAULT '',
  `important` char(1) DEFAULT NULL,
  `allow_comments` char(1) DEFAULT NULL,
  `allow_ratings` char(1) DEFAULT NULL,
  `number_of_replies` int(11) NOT NULL DEFAULT '0',
  `number_of_views` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_reply_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_thread_id` int(11) unsigned NOT NULL DEFAULT '0',
  `last_user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `last_updated_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated_user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(30) DEFAULT NULL,
  `poll_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `viewable` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`topic_id`),
  KEY `count_community_null` (`status_id`,`created_user_id`,`community_id`),
  KEY `reply_date_sort` (`last_reply_date`),
  KEY `cd` (`created_date`),
  KEY `cuid` (`created_user_id`),
  KEY `luid` (`last_user_id`),
  KEY `ltid` (`last_thread_id`),
  KEY `v` (`viewable`)
) ENGINE=InnoDB;

CREATE TABLE `topics_by_type_by_day` (
  `user_id` int(10) unsigned NOT NULL,
  `dt_stamp` date NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `torrents` (
  `torrent_id` int(11) NOT NULL AUTO_INCREMENT,
  `torrent_name` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `status_id` int(11) NOT NULL DEFAULT '0',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `display_name` varchar(100) NOT NULL DEFAULT '',
  `info_hash` varchar(40) NOT NULL DEFAULT '',
  `content_size` bigint(20) unsigned NOT NULL DEFAULT '0',
  `content_extension` varchar(255) DEFAULT NULL,
  `secured` int(11) NOT NULL DEFAULT '0',
  `number_of_seeds` int(11) NOT NULL DEFAULT '0',
  `short_description` varchar(255) DEFAULT NULL,
  `asset_type_id` int(11) DEFAULT NULL,
  `seed_updated_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `target_dir` varchar(255) DEFAULT NULL,
  `content_hosted` int(11) NOT NULL DEFAULT '0',
  `torrent_status` int(11) NOT NULL DEFAULT '0',
  `folder` int(11) NOT NULL DEFAULT '0',
  `percent_complete` int(11) NOT NULL DEFAULT '0',
  `initial_info_hash` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`torrent_id`)
) ENGINE=InnoDB;

CREATE TABLE `torrent_status` (
  `status_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB;

CREATE TABLE `transaction_type` (
  `transaction_type` smallint(5) unsigned NOT NULL DEFAULT '1',
  `trans_desc` varchar(80) DEFAULT NULL,
  `trans_text` varchar(40) NOT NULL,
  PRIMARY KEY (`transaction_type`),
  KEY `td` (`trans_desc`),
  KEY `tt` (`trans_text`)
) ENGINE=InnoDB;

CREATE TABLE `unified_worlds_home_migration_log` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `run_date` datetime NOT NULL COMMENT 'Date the conversion was run.',
  `number_of_communities_processed` int(11) NOT NULL COMMENT 'How many communities were processed.',
  `number_of_communities_left` int(11) NOT NULL COMMENT 'How many communities are left to process.',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB COMMENT='Temp table to indicate how many home community records were converted.';

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(22) NOT NULL,
  `password` varchar(64) NOT NULL DEFAULT '',
  `salt` varchar(16) NOT NULL DEFAULT '',
  `role` varchar(32) NOT NULL DEFAULT '',
  `registration_key` varchar(50) DEFAULT NULL,
  `account_type` int(11) NOT NULL DEFAULT '0',
  `master_user_id` int(11) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL DEFAULT '',
  `last_name` varchar(50) NOT NULL DEFAULT '',
  `display_name` varchar(30) NOT NULL,
  `description` text,
  `gender` char(1) NOT NULL DEFAULT '',
  `show_gender` char(1) DEFAULT NULL,
  `homepage` varchar(200) DEFAULT NULL,
  `email` varchar(100) NOT NULL DEFAULT '',
  `show_email` varchar(11) NOT NULL DEFAULT 'M',
  `status_id` int(11) NOT NULL DEFAULT '0',
  `email_status` tinyint(3) NOT NULL DEFAULT '0',
  `key_value` varchar(20) DEFAULT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `signup_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `birth_date` date DEFAULT NULL,
  `show_birth` char(1) DEFAULT NULL,
  `newsletter` char(1) NOT NULL DEFAULT '',
  `email_notify` char(1) DEFAULT NULL,
  `public_profile` char(1) NOT NULL DEFAULT '',
  `zip_code` varchar(13) NOT NULL DEFAULT '0',
  `country` char(2) NOT NULL DEFAULT '99',
  `ip_address` int(10) unsigned DEFAULT '0',
  `last_ip_address` int(10) unsigned DEFAULT '0',
  `message_notification` int(11) NOT NULL DEFAULT '0',
  `show_mature` tinyint(4) NOT NULL DEFAULT '0',
  `browse_anonymously` tinyint(4) NOT NULL DEFAULT '0',
  `show_online` tinyint(4) NOT NULL DEFAULT '1',
  `notify_blog_comments` tinyint(4) NOT NULL DEFAULT '1',
  `notify_profile_comments` tinyint(4) NOT NULL DEFAULT '0',
  `notify_friend_messages` tinyint(4) NOT NULL DEFAULT '1',
  `notify_anyone_messages` tinyint(4) NOT NULL DEFAULT '1',
  `notify_friend_requests` tinyint(4) NOT NULL DEFAULT '1',
  `notify_new_friends` tinyint(4) NOT NULL DEFAULT '1',
  `friends_can_comment` tinyint(4) NOT NULL DEFAULT '1',
  `everyone_can_comment` int(11) NOT NULL DEFAULT '1',
  `mature_profile` tinyint(4) NOT NULL DEFAULT '0',
  `online` tinyint(1) NOT NULL DEFAULT '0',
  `second_to_last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `age` tinyint(4) NOT NULL DEFAULT '0',
  `location` varchar(80) NOT NULL,
  `own_mod` enum('Y','N') NOT NULL DEFAULT 'N',
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `over_21` enum('Y','N') DEFAULT 'N',
  `ustate` enum('On','Off','InWorld','OnInWorld') NOT NULL DEFAULT 'Off',
  `wok_player_id` int(11) NOT NULL DEFAULT '0',
  `_signup_date` int(10) unsigned NOT NULL DEFAULT '4291869598',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `blast_show_permissions` int(10) unsigned NOT NULL DEFAULT '1023',
  `blast_privacy_permissions` int(10) unsigned NOT NULL DEFAULT '1023',
  `join_source_community_id` int(11) NOT NULL DEFAULT '0',
  `home_community_id` int(11) DEFAULT NULL COMMENT 'community_id of the user''s home (starting) world.',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_rule` (`email`),
  KEY `index_gender` (`gender`),
  KEY `index_zipcode` (`zip_code`),
  KEY `username` (`username`),
  KEY `first_last` (`first_name`,`last_name`),
  KEY `country_index` (`country`),
  KEY `u_status` (`status_id`),
  KEY `date_report` (`signup_date`),
  KEY `country_status_id_user_id` (`user_id`,`status_id`,`country`),
  KEY `birth_date` (`birth_date`),
  KEY `age` (`age`),
  KEY `active` (`active`),
  KEY `_signup_date` (`_signup_date`),
  KEY `IDX_last_update` (`last_update`),
  KEY `IDX_role` (`role`),
  KEY `IDX_last_login` (`last_login`),
  KEY `IDX_last_ip_address` (`last_ip_address`),
  KEY `idx_wok_player_id` (`wok_player_id`),
  KEY `fk_home_community_id_idx` (`home_community_id`),
  CONSTRAINT `fk_home_community_id` FOREIGN KEY (`home_community_id`) REFERENCES `communities` (`community_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `users_billing_subscriptions` (
  `user_id` int(11) NOT NULL COMMENT 'PK',
  `subscription_sequence_number` smallint(5) unsigned NOT NULL COMMENT 'Sequence number of subscription per user.  Each user will start at 1',
  `billing_subscription_id` varchar(30) NOT NULL DEFAULT '' COMMENT 'Cybersource profile identifier, PK',
  `billing_subscription_status` tinyint(3) unsigned NOT NULL COMMENT 'Status of subscription: 1-active, 2-deleted',
  `last_modified_date` datetime NOT NULL COMMENT 'datetime of last update to subscription status',
  PRIMARY KEY (`user_id`,`subscription_sequence_number`)
) ENGINE=InnoDB COMMENT='Cybersource billin subscription identifier';

CREATE TABLE `users_dates` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `birthday` smallint(4) NOT NULL DEFAULT '0',
  `anniversary` smallint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  KEY `IDX_birthday` (`birthday`),
  KEY `IDX_anniversary` (`anniversary`)
) ENGINE=InnoDB;

CREATE TABLE `users_email` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email_hash` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  PRIMARY KEY (`user_id`),
  KEY `IDX_email_hash` (`email_hash`)
) ENGINE=InnoDB;

CREATE TABLE `users_stats` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `number_of_logins` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `number_of_failed_logins` smallint(5) unsigned NOT NULL DEFAULT '0',
  `number_inbox_messages` smallint(5) unsigned NOT NULL DEFAULT '0',
  `number_outbox_messages` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `number_forum_posts` smallint(5) unsigned NOT NULL DEFAULT '0',
  `number_of_friends` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'total number of friends',
  `number_of_pending` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'this user''s pending friend req to others',
  `number_of_requests` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'other user''s req to be this one''s friend',
  `number_of_new_messages` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `number_of_trashed_messages` smallint(5) unsigned NOT NULL DEFAULT '0',
  `number_of_blogs` smallint(5) unsigned NOT NULL DEFAULT '0',
  `number_of_comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `number_of_asset_diggs` smallint(5) unsigned NOT NULL DEFAULT '0',
  `i_own` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'communities',
  `i_moderate` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'communities',
  `i_belong_to` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'communities',
  `number_pending_community_requests` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'pending req to all owned/moderated communities',
  `number_gifts_given` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '3D+2D gifts',
  `number_gifts_received` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `number_gifts_pending` mediumint(9) NOT NULL DEFAULT '0' COMMENT 'not accepted yet',
  `number_asset_shares` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `number_community_shares` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `interest_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of interests a user has',
  `number_invites_sent` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `number_invites_accepted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `number_invites_in_world` smallint(5) unsigned NOT NULL DEFAULT '0',
  `number_blasts_sent` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of blasts sent',
  `number_friend_invite_rewards` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'Number of rewards from inviting friends',
  `number_3DApp_requests` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `number_community_invites` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  KEY `IDX_last_update` (`last_update`)
) ENGINE=InnoDB;

CREATE TABLE `users_system_ids` (
  `user_id` int(11) NOT NULL,
  `system_id` varchar(50) NOT NULL,
  `date_first_used` datetime NOT NULL,
  `date_last_used` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`system_id`),
  KEY `usids_system_id_fk_idx` (`system_id`),
  CONSTRAINT `usids_system_id_fk` FOREIGN KEY (`system_id`) REFERENCES `user_systems` (`system_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usids_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `user_acquisition_source` (
  `user_id` int(11) NOT NULL COMMENT 'FK to users table',
  `acquisition_source_id` int(11) unsigned NOT NULL COMMENT 'FK to acquisistion_source table',
  `full_url` text NOT NULL COMMENT 'complete url of referring location',
  `additional_information` varchar(512) NOT NULL DEFAULT '' COMMENT 'any additional info about source',
  PRIMARY KEY (`user_id`,`acquisition_source_id`),
  KEY `IDX_user_id` (`user_id`),
  KEY `IDX_acquisition_source_id` (`acquisition_source_id`)
) ENGINE=InnoDB COMMENT='1to1 between users and acquisition_source';

CREATE TABLE `user_balances` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL DEFAULT 'GPOINT',
  `balance` decimal(12,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`user_id`,`kei_point_id`),
  KEY `KID` (`kei_point_id`)
) ENGINE=InnoDB ROW_FORMAT=FIXED;

CREATE TABLE `user_balances_audit_log` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `old_balance` float NOT NULL DEFAULT '0',
  `new_balance` float NOT NULL DEFAULT '0',
  `difference` float NOT NULL DEFAULT '0',
  `transaction_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB;

CREATE TABLE `user_connection` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `depth` int(10) unsigned NOT NULL DEFAULT '0',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `user_connection_map_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_connection_id` int(11) NOT NULL DEFAULT '0',
  `map_data` text NOT NULL,
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `user_connection_stats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_connection_id` int(11) NOT NULL DEFAULT '0',
  `depth` int(11) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `created_datetime` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `user_daily_gifts` (
  `created_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  PRIMARY KEY (`created_date`,`user_id`,`recipient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_editor_downloads` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `downloaded_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` varchar(20) DEFAULT NULL
) ENGINE=InnoDB;

CREATE TABLE `user_facebook_settings` (
  `user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `fb_user_id` bigint(20) NOT NULL COMMENT 'Users Facebook User Id',
  `access_token` text COMMENT 'Facebook access token',
  `use_facebook_profile_picture` smallint(1) unsigned NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  KEY `fbuid` (`fb_user_id`),
  CONSTRAINT `fb_settings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB COMMENT='user facebook settings';

CREATE TABLE `user_interests` (
  `user_id` int(10) unsigned NOT NULL,
  `interest_id` int(10) unsigned NOT NULL,
  `ic_id` int(10) unsigned NOT NULL,
  `z_order` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`interest_id`),
  KEY `ic_id` (`ic_id`),
  KEY `user_id` (`user_id`),
  KEY `iid` (`interest_id`),
  KEY `zo` (`z_order`)
) ENGINE=InnoDB;

CREATE TABLE `user_interests_searchable` (
  `user_id` int(10) unsigned NOT NULL,
  `interests` text NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `user_interest_category_counts` (
  `user_id` int(10) unsigned NOT NULL,
  `ic_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`ic_id`),
  KEY `ic_id` (`ic_id`)
) ENGINE=InnoDB;

CREATE TABLE `user_login_issues` (
  `user_login_issue_id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(50) NOT NULL DEFAULT '',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) DEFAULT NULL,
  `description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_login_issue_id`)
) ENGINE=InnoDB;

CREATE TABLE `user_meta_game_item_balances` (
  `user_id` int(11) NOT NULL,
  `meta_game_item_name` varchar(50) NOT NULL,
  `balance` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`meta_game_item_name`),
  KEY `umgib_game_item_fk_idx` (`meta_game_item_name`),
  CONSTRAINT `umgib_game_item_fk` FOREIGN KEY (`meta_game_item_name`) REFERENCES `meta_game_items` (`item_name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `umgib_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='User balances for each of the meta game items.';

CREATE TABLE `user_notes` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `note` text NOT NULL,
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(11) NOT NULL DEFAULT '0',
  `wok_transaction_log_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to wok_transaction_log table',
  PRIMARY KEY (`note_id`),
  KEY `user_id_key` (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `user_notification_preferences` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `notify_blast_comments` tinyint(4) NOT NULL DEFAULT '1',
  `notify_media_comments` tinyint(4) NOT NULL DEFAULT '1',
  `notify_shop_comments_purchases` tinyint(4) NOT NULL DEFAULT '1',
  `notify_event_invites` tinyint(4) NOT NULL DEFAULT '1',
  `notify_friend_birthdays` tinyint(4) NOT NULL DEFAULT '1',
  `notify_world_blasts` tinyint(4) NOT NULL DEFAULT '1',
  `notify_world_requests` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Whether or not the user wishes to receive API request emails.',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `user_preferences` (
  `user_id` int(11) NOT NULL COMMENT 'User_id of the preferences',
  `kei_point_id_purchase_type` enum('GPOINT','KPOINT','MPOINT','DOLLAR','NOTSET') DEFAULT 'NOTSET' COMMENT 'Preferences for purchases',
  `filter_by_country` enum('Y','N') DEFAULT 'N' COMMENT 'Filter by country preference',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB COMMENT='Preferences for a user';

CREATE TABLE `user_preferences_blast_blocks` (
  `user_id` int(11) NOT NULL COMMENT 'kaneva.user_id of the blocker',
  `blocked_user_id` int(11) NOT NULL COMMENT 'users.user_id of the blocked person',
  `blocked_community_id` int(11) NOT NULL COMMENT 'communities.community_id of the blocked community or app',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'date the blast was blocked',
  PRIMARY KEY (`user_id`,`blocked_user_id`,`blocked_community_id`),
  KEY `IDX_blocked_user` (`blocked_user_id`),
  KEY `IDX_blocked_comm` (`blocked_community_id`)
) ENGINE=InnoDB COMMENT='Blasts Blocked by a User';

CREATE TABLE `user_profiles` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `title_blast` text,
  `show_favorites` char(1) DEFAULT 'M',
  `favorite_games` text,
  `favorite_movies` text,
  `favorite_music` text,
  `favorite_books` text,
  `favorite_tv` text,
  `like_to_meet` text,
  `interests` text,
  `show_prof` char(1) DEFAULT 'M',
  `prof_title` varchar(100) DEFAULT NULL,
  `prof_position` varchar(100) DEFAULT NULL,
  `prof_industry` varchar(100) DEFAULT NULL,
  `prof_company` varchar(100) DEFAULT NULL,
  `prof_job` varchar(100) DEFAULT NULL,
  `prof_skills` varchar(255) DEFAULT NULL,
  `show_org` char(1) DEFAULT NULL,
  `org_current_assoc` varchar(255) DEFAULT NULL,
  `org_past_assoc` varchar(255) DEFAULT NULL,
  `show_store` char(1) NOT NULL DEFAULT 'M',
  `show_blog` char(1) NOT NULL DEFAULT 'M',
  `show_games` char(1) NOT NULL DEFAULT 'M',
  `show_bookmarks` char(1) NOT NULL DEFAULT 'M',
  `relationship` enum('No Answer','Single','Married','Freaky','In a relationship','Divorced','Swinger') DEFAULT 'No Answer',
  `orientation` enum('No Answer','Straight','Freak','Bi-sexual','Gay/Lesbian','Not Sure') DEFAULT 'No Answer',
  `religion` enum('No Answer','Catholic','Christian (Other)','Buddhist','Jewish','Other','Agnostic','Atheist','Protestant','Wiccan','Taoist','Mormon','Scientologist','Muslim','Hindu') DEFAULT 'No Answer',
  `ethnicity` enum('No Answer','White','Mixed','Asian','Native American','East Indian','Other','Black','Latino/Hispanic','European','Pacific Rim','Middle Eastern') DEFAULT 'No Answer',
  `children` enum('No Answer','Undecided','One day','I am a parent','Don''t want any') DEFAULT 'No Answer',
  `education` enum('No Answer','College graduate','Some college','High School','Masters Degree','Dropout','Doctorate','In college','Bachelors Degree') DEFAULT 'No Answer',
  `income` tinyint(3) unsigned DEFAULT '0',
  `height_feet` tinyint(4) DEFAULT NULL,
  `height_inches` tinyint(4) DEFAULT NULL,
  `smoke` enum('No Answer','Yes','No') DEFAULT 'No Answer',
  `drink` enum('No Answer','Yes','No') DEFAULT 'No Answer',
  `im_username_1` varchar(50) DEFAULT NULL,
  `im_type_1` varchar(10) DEFAULT 'Yahoo',
  `im_security_1` varchar(11) DEFAULT NULL,
  `im_username_2` varchar(50) DEFAULT NULL,
  `im_type_2` varchar(10) DEFAULT 'MSN',
  `im_security_2` varchar(11) DEFAULT NULL,
  `im_username_3` varchar(50) DEFAULT NULL,
  `im_type_3` varchar(10) DEFAULT 'ICQ',
  `im_security_3` varchar(11) DEFAULT NULL,
  `im_username_4` varchar(50) DEFAULT NULL,
  `im_type_4` varchar(10) DEFAULT 'AIM',
  `im_security_4` varchar(11) DEFAULT NULL,
  `hobbies` text,
  `favorite_sports` text,
  `favorite_gadgets` text,
  `favorite_fashion` text,
  `favorite_cars` text,
  `favorite_artists` text,
  `clubs` text,
  `favorite_brands` text,
  `role_models` text,
  `favorite_quotes` text,
  `hometown` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_index` (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `user_profile_views` (
  `view_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `profile_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `anonymous` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`view_id`)
) ENGINE=InnoDB;

CREATE TABLE `user_schools` (
  `user_id` int(10) unsigned NOT NULL,
  `school_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`school_id`)
) ENGINE=InnoDB;

CREATE TABLE `user_searches_archived` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `search_string` varchar(100) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `search_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`log_id`),
  KEY `IDX_created_datetime` (`created_datetime`)
) ENGINE=InnoDB;

CREATE TABLE `user_status` (
  `status_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(20) NOT NULL DEFAULT '',
  `description` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB;

CREATE TABLE `user_subscriptions` (
  `user_subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `subscription_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Subscription id',
  `promotion_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'foreign key to the promotional_offers table',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Users subscrription',
  `price` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'Amount of subscription',
  `monthly_allowance` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'amount of credits paid to the user monthly',
  `discount_percent` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'stores any discount tha tis asscociated with this subscription',
  `auto_renew` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'Is it renewable',
  `purchase_date` datetime NOT NULL COMMENT 'The date first purchased',
  `status_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The status of the subscription',
  `billing_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Is it PayPal or Cybersource',
  `subscription_identifier` varchar(100) DEFAULT NULL COMMENT 'The PayPal or Cybersource subscription identifier',
  `user_cancelled` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'Y if end user cancelled this subscription',
  `cancelled_date` datetime NOT NULL COMMENT 'Date the subscription was cancelled',
  `inital_term` int(11) NOT NULL DEFAULT '0',
  `renewal_term` int(11) NOT NULL DEFAULT '0',
  `end_date` datetime DEFAULT NULL COMMENT 'Current date the subscription will end',
  `free_trial_end_date` datetime DEFAULT NULL,
  `user_cancellation_reason` varchar(200) NOT NULL DEFAULT '' COMMENT 'stored reason for users cancellation',
  `is_legacy` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'Y if they subscription uses legacy sub engine',
  PRIMARY KEY (`user_subscription_id`),
  KEY `FK_game_subscriptions_2` (`subscription_id`),
  KEY `FK_game_subscriptions_4` (`status_id`),
  KEY `FK_sub_1` (`subscription_id`)
) ENGINE=InnoDB COMMENT='Hold the users subscriptions';

CREATE TABLE `user_subscription_orders` (
  `user_subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tthe user subcription',
  `order_id` int(11) NOT NULL COMMENT 'The order id',
  PRIMARY KEY (`user_subscription_id`,`order_id`)
) ENGINE=InnoDB COMMENT='Orders for a subscription';

CREATE TABLE `user_suspensions` (
  `ban_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ban_date_start` datetime NOT NULL,
  `ban_date_end` datetime DEFAULT NULL,
  PRIMARY KEY (`ban_id`),
  KEY `IDX_user_id` (`user_id`)
) ENGINE=InnoDB COMMENT='Suspension Details';

CREATE TABLE `user_systems` (
  `system_id` varchar(50) NOT NULL,
  `date_first_used` datetime NOT NULL,
  `date_last_used` datetime NOT NULL,
  PRIMARY KEY (`system_id`)
) ENGINE=InnoDB COMMENT='Unique identification records for systems used to login to Kaneva.';

CREATE TABLE `user_system_configurations` (
  `system_configuration_id` varchar(50) NOT NULL,
  `system_id` varchar(50) DEFAULT NULL,
  `os_version` varchar(20) DEFAULT NULL,
  `system_name` varchar(256) DEFAULT NULL,
  `cpu_count` tinyint(4) unsigned DEFAULT NULL,
  `cpu_version` varchar(10) DEFAULT NULL,
  `cpu_mhz` int(11) unsigned DEFAULT NULL,
  `system_memory_bytes` bigint(20) unsigned DEFAULT NULL,
  `disk_size_bytes` bigint(20) unsigned DEFAULT NULL,
  `gpu_description` varchar(256) DEFAULT NULL,
  `gpu_display_mode_width` int(11) unsigned DEFAULT NULL,
  `gpu_display_mode_height` int(11) unsigned DEFAULT NULL,
  `gpu_refresh_rate_hz` int(11) unsigned DEFAULT NULL,
  `gpu_display_mode_format` int(11) unsigned DEFAULT NULL,
  `dx_version` varchar(45) DEFAULT NULL,
  `net_interface_mac` varchar(45) DEFAULT NULL,
  `net_interface_hash` varchar(45) DEFAULT NULL,
  `net_interface_description` varchar(256) DEFAULT NULL,
  `net_interface_type` int(11) unsigned DEFAULT NULL,
  `date_first_used` datetime NOT NULL,
  `date_last_used` datetime NOT NULL,
  `user_id_last_used` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`system_configuration_id`),
  KEY `usc_system_id_fk_idx` (`system_id`),
  KEY `usc_net_int_hash_idx` (`net_interface_hash`),
  KEY `usc_date_last_used_ids` (`date_last_used`),
  CONSTRAINT `usc_system_id_fk` FOREIGN KEY (`system_id`) REFERENCES `user_systems` (`system_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Log of system configurations a user has used to play WoK.';

CREATE TABLE `user_upload_descriptions` (
  `description_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_page_id` int(10) unsigned DEFAULT NULL,
  `field_description` text NOT NULL,
  `required` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`description_id`)
) ENGINE=InnoDB;

CREATE TABLE `user_useragent_data` (
  `user_id` int(11) NOT NULL,
  `os` varchar(50) DEFAULT NULL,
  `browser` varchar(50) DEFAULT NULL,
  `user_agent` text,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB COMMENT='UserAgent information stored when user joins Kaneva';

CREATE TABLE `violation_actions` (
  `violation_action_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key for quick reference',
  `violation_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'id of the type of violation eg pornographic',
  `violation_action_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'id of the type of action to take',
  `violation_trigger_count` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'number of reports needed before taking the action',
  PRIMARY KEY (`violation_action_id`)
) ENGINE=InnoDB COMMENT='table sets the rules for actions to take on violations';

CREATE TABLE `violation_action_types` (
  `violation_action_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'id of the violation action type',
  `violation_action_description` varchar(100) NOT NULL DEFAULT '' COMMENT 'description of the violation action type',
  PRIMARY KEY (`violation_action_type_id`)
) ENGINE=InnoDB COMMENT='2nd normal form table for holding violation action types';

CREATE TABLE `violation_reports` (
  `violation_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto number for quick reference',
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'id of asset from asset table',
  `wok_item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'global it from wok items table',
  `owner_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'user_id of the items owner/creator',
  `reporter_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'user_id of the person reporting the violation',
  `violation_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'id of the type of violation eg pornographic',
  `violation_action_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'id of the type of action taken',
  `member_comments` text COMMENT 'reporting member comments',
  `csr_notes` text COMMENT 'customer service comments',
  `report_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'time of the orginal report',
  `last_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'last time the report was updated',
  `modifiers_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'id of the user who made the most recent changes',
  PRIMARY KEY (`violation_id`),
  UNIQUE KEY `Unique_Violation_Report` (`reporter_id`,`violation_type_id`,`asset_id`,`wok_item_id`),
  KEY `Unique_WOK_report` (`wok_item_id`,`reporter_id`,`violation_type_id`)
) ENGINE=InnoDB COMMENT='holds all a record of violation reports';

CREATE TABLE `violation_types` (
  `violation_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'id of the violation type',
  `violation_type_description` varchar(100) NOT NULL DEFAULT '' COMMENT 'description of the violation type',
  PRIMARY KEY (`violation_type_id`)
) ENGINE=InnoDB COMMENT='2nd normal form table for violation types';

CREATE TABLE `web_health` (
  `dt_taken` datetime NOT NULL,
  `server_name` varchar(50) NOT NULL,
  `uptime` float NOT NULL,
  `processor_time` float NOT NULL,
  `processor_priv_time` float NOT NULL,
  `processor_interrupt_time` float NOT NULL,
  `system_processor_queue_length` float NOT NULL,
  `system_context_switches_sec` float NOT NULL,
  `memory_avail_mbytes` float NOT NULL,
  `memory_page_reads_sec` float NOT NULL,
  `memory_pages_sec` float NOT NULL,
  `server_pool_nonpaged_bytes` float NOT NULL,
  `server_pool_nonpaged_failures` float NOT NULL,
  `server_pool_paged_failures` float NOT NULL,
  `server_pool_nonpaged_peak` float NOT NULL,
  `memory_cache_bytes` float NOT NULL,
  `memory_cache_faults_sec` float NOT NULL,
  `cache_MDL_read_hits_percent` float NOT NULL,
  `disk_queue_length` float NOT NULL,
  `disk_avg_read_queue_length` float NOT NULL,
  `disk_avg_write_queue_length` float NOT NULL,
  `disk_avg_disk_sec_read` float NOT NULL,
  `nic1_bytes_total_sec` float NOT NULL,
  `nic2_byts_total_sec` float NOT NULL,
  `tcp_segments_sent_sec` float NOT NULL,
  `udp_datagrams_sent_sec` float NOT NULL,
  `net_exceptions` float NOT NULL,
  `net_exceptions_sec` float NOT NULL,
  `net_contention_rate_sec` float NOT NULL,
  `net_queue_length` float NOT NULL,
  `net_GC_percent` float NOT NULL,
  `net_requests_sec` float NOT NULL,
  `isapi_requests_sec` float NOT NULL,
  `net_requests_timed_out` float NOT NULL,
  `net_request_execution_time` float NOT NULL,
  `net_cache_total_entries` float NOT NULL,
  `net_cache_hit_ratio` float NOT NULL,
  `net_output_cache_entries` float NOT NULL,
  `net_output_cache_hit_ratio` float NOT NULL,
  `net_output_cache_turnover_rate` float NOT NULL,
  PRIMARY KEY (`dt_taken`),
  KEY `sn` (`server_name`(15))
) ENGINE=InnoDB COMMENT='Stores Website Stats';

CREATE TABLE `web_options` (
  `option_group` char(10) NOT NULL,
  `option_key` char(40) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  `option_value` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`option_group`,`option_key`,`role`)
) ENGINE=InnoDB;

CREATE TABLE `web_option_templates` (
  `option_group` char(10) NOT NULL,
  `option_key` char(40) NOT NULL,
  `option_desc` varchar(250) NOT NULL,
  `enabled` smallint(1) NOT NULL DEFAULT '1',
  `support_complex_values` smallint(1) NOT NULL DEFAULT '0',
  `default_value` varchar(250) DEFAULT NULL,
  `help_examples` varchar(1000) DEFAULT NULL,
  `help_references` varchar(500) DEFAULT NULL,
  `help_see_also` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`option_group`,`option_key`)
) ENGINE=InnoDB COMMENT='Available web options';

CREATE TABLE `wok_downloads` (
  `wok_download_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`wok_download_id`),
  KEY `u` (`user_id`)
) ENGINE=InnoDB COMMENT='Store WOK download attempts';

CREATE TABLE `wok_transaction_log` (
  `trans_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `balance_prev` float NOT NULL DEFAULT '0',
  `balance` float NOT NULL DEFAULT '0',
  `transaction_amount` double DEFAULT '0',
  `kei_point_id` enum('KPOINT','MPOINT','GPOINT','DOLLAR') NOT NULL DEFAULT 'KPOINT',
  `transaction_type` smallint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`trans_id`),
  KEY `user_id` (`user_id`),
  KEY `tamt` (`transaction_amount`),
  KEY `IDX_created_date` (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `wok_transaction_log_community_details` (
  `trans_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to transaction table',
  `community_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to communities table',
  `cover_charge_amount` float NOT NULL DEFAULT '0' COMMENT 'Cover charge amount',
  `charge_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Time of transaction',
  PRIMARY KEY (`trans_id`),
  KEY `ci` (`community_id`),
  KEY `ct` (`charge_time`)
) ENGINE=InnoDB COMMENT='details table for cover charges paid';

CREATE TABLE `wok_transaction_log_details` (
  `trans_id` int(11) NOT NULL COMMENT 'FK to transaction table',
  `global_id` int(11) NOT NULL COMMENT 'FK to wok.items table',
  `quantity` int(11) NOT NULL COMMENT 'number of items',
  `use_value` int(11) NOT NULL DEFAULT '0' COMMENT 'copied from wok.items',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'copied from wok.items',
  PRIMARY KEY (`trans_id`),
  KEY `gi` (`global_id`)
) ENGINE=InnoDB COMMENT='details table for user_balance changes';

CREATE TABLE `xxx_marketing_email_records_by_users` (
  `user_id` int(11) NOT NULL,
  `email_type` int(11) NOT NULL,
  `date_sent` datetime NOT NULL,
  KEY `Index_2` (`email_type`),
  KEY `Index_3` (`date_sent`),
  KEY `Index_4` (`user_id`)
) ENGINE=InnoDB ROW_FORMAT=FIXED;

CREATE TABLE `xxx_marketing_email_tracking` (
  `view_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `mailing_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `ipaddress` varchar(255) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`view_id`)
) ENGINE=InnoDB;

CREATE TABLE `zip_codes` (
  `zip_code` varchar(13) NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state_code` char(2) DEFAULT NULL,
  `county` varchar(50) DEFAULT NULL,
  `zip_class` varchar(50) DEFAULT NULL,
  `number_of_users` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of users in this zip code',
  PRIMARY KEY (`zip_code`),
  KEY `IDX_lat_long` (`latitude`,`longitude`)
) ENGINE=InnoDB;

CREATE TABLE `zip_codes_7_days` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `zip_code` varchar(11) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE=InnoDB;

CREATE TABLE `_pending_friends` (
  `the_id` int(11) NOT NULL DEFAULT '0',
  `us_pending_friends` int(11) NOT NULL DEFAULT '0',
  `fr_pending_friends` int(11) NOT NULL DEFAULT '0',
  `us_pending_requests` int(11) NOT NULL DEFAULT '0',
  `fr_pending_requests` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`the_id`)
) ENGINE=InnoDB;

CREATE TABLE `_pending_members` (
  `the_id` int(11) NOT NULL DEFAULT '0',
  `cs_pending_members` int(11) NOT NULL DEFAULT '0',
  `cm_pending_members` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`the_id`)
) ENGINE=InnoDB;

CREATE TABLE `_temp_user_friends` (
  `user_id` int(11) NOT NULL,
  `friends` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `_temp_user_friends2` (
  `user_id` int(11) NOT NULL,
  `friends` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `_tmp_comm_mem` (
  `community_id` int(11) NOT NULL DEFAULT '0',
  `cm_pending_members` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`community_id`)
) ENGINE=InnoDB;

CREATE TABLE `_tmp_friend_pending` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `fr_pending_friends` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `_tmp_friend_requests` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `fr_pending_requests` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `_tmp_missed_asset_raves` (
  `asset_id` int(11) NOT NULL,
  `rave_count` int(11) NOT NULL,
  PRIMARY KEY (`asset_id`)
) ENGINE=InnoDB;

CREATE TABLE `__balances_temp` (
  `match_criteria` varchar(255) DEFAULT NULL,
  KEY `Index_1` (`match_criteria`)
) ENGINE=InnoDB;

CREATE TABLE `__balances_temp2` (
  `balance` float NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB;

CREATE TABLE `__balance_temp0` (
  `balance` float NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `match_criteria` varchar(255) NOT NULL,
  KEY `Index_1` (`match_criteria`)
) ENGINE=InnoDB;

CREATE TABLE `__related_items_temp` (
  `_order` double NOT NULL,
  `base_asset_id` int(11) NOT NULL,
  `related_asset_id` int(11) NOT NULL,
  `mature` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`_order`),
  UNIQUE KEY `base_asset_id` (`base_asset_id`,`related_asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `__users_dates` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `birthday` smallint(4) NOT NULL DEFAULT '0',
  `anniversary` smallint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  KEY `IDX_birthday` (`birthday`),
  KEY `IDX_anniversary` (`anniversary`)
) ENGINE=InnoDB;

CREATE TABLE `__users_no_age` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

