-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS gen_id;

DELIMITER ;;
CREATE FUNCTION `gen_id`(_prefix INT, _prefix2 INT) RETURNS varchar(64)
    NO SQL
BEGIN
return CONCAT( hex(_prefix), '-', hex(_prefix2),'-',  hex(FLOOR( (rand() * UNIX_TIMESTAMP(NOW())) * (RAND() * (1048576*2)))));

END
;;
DELIMITER ;