-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_communities_thumbnailX;

DELIMITER ;;
CREATE PROCEDURE `update_communities_thumbnailX`(IN __community_id INT, IN __thumbnail_path VARCHAR(255), 
		IN __column ENUM('Reg','S','M','L','XL'))
BEGIN

IF  __column = 'Reg' THEN 
	UPDATE communities SET thumbnail_path = __thumbnail_path WHERE community_id = __community_id;
	ELSEIF __column = 'S' THEN
	UPDATE communities SET thumbnail_small_path = __thumbnail_path WHERE community_id = __community_id;
	ELSEIF __column = 'M' THEN
	UPDATE communities SET thumbnail_medium_path = __thumbnail_path WHERE community_id = __community_id;
	ELSEIF __column = 'L' THEN
	UPDATE communities SET thumbnail_large_path = __thumbnail_path WHERE community_id = __community_id;
	ELSEIF __column = 'XL' THEN
	SELECT thumbnail_xlarge_path, is_personal, creator_id INTO @old_thumbnail_xlarge_path, @is_personal, @creator_id
		FROM communities WHERE community_id = __community_id;
	UPDATE communities SET thumbnail_xlarge_path = __thumbnail_path WHERE community_id = __community_id;
	IF @is_personal = 1 AND @old_thumbnail_xlarge_path <> __thumbnail_path THEN
		UPDATE wok.dynamic_objects
			SET texture_url = __thumbnail_path 
			WHERE friend_id = @creator_id;
	END IF;	
END IF;

END
;;
DELIMITER ;