-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS approveWokUser;

DELIMITER ;;
CREATE PROCEDURE `approveWokUser`( IN theusername VARCHAR(64) )
BEGIN
	DECLARE userid INT(11) DEFAULT NULL;
	DECLARE oldStatus INT(11) DEFAULT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
	SELECT user_id INTO userid
	  FROM users
	WHERE username = theusername;
	IF userid IS NOT NULL THEN
		UPDATE users
		   SET status_id = 1
		WHERE username = theusername;

		SELECT status_id INTO oldStatus
		   FROM community_members 
		WHERE community_id = 13818 AND user_id = userId;
		IF oldStatus IS NULL THEN
			INSERT INTO community_members ( community_id, user_id, account_type_id, added_date, status_id )
			   VALUES ( 13818, userid, 3, NOW(), 1 );
			SELECT concat( 'new member ok - ', theusername ) as Result;
		ELSE
			UPDATE community_members
			  SET status_id = 1
			WHERE community_id = 13818 AND user_id = userId;
			SELECT concat( 'existing member ok - ', theusername ) as Result;
		END IF;
		
	ELSE
		SELECT concat( 'user not found - ', theusername ) as Result;
	END IF;
END
;;
DELIMITER ;