-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS apply_transaction_and_details_to_user_balance;

DELIMITER ;;
CREATE PROCEDURE `apply_transaction_and_details_to_user_balance`( IN user_id int, IN transaction_amount float, 
							IN transaction_type smallint unsigned, 
							IN v_kei_point_id varchar(20), 
							IN v_glid INT,
							IN v_qty INT,
							OUT return_code INT, OUT user_balance float, OUT tranId INT )
BEGIN
-- Return code can be one of three values
-- 1 indicates the transaction is invalid due to insufficient funds
-- 0 indicates the transaction was valid and recorded
-- -1 indicates the transaction was invalid due to an error
	CALL kaneva.apply_transaction_to_user_balance( user_id, transaction_amount, transaction_type, 
							v_kei_point_id, return_code, user_balance, tranId );
	IF return_code = 0 AND v_glid > 0 THEN
		INSERT INTO kaneva.wok_transaction_log_details ( trans_id, global_id, quantity )
			VALUES ( tranId, v_glid, v_qty );
	END IF;
	
END
;;
DELIMITER ;