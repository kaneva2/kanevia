-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS remove_friends;

DELIMITER ;;
CREATE PROCEDURE `remove_friends`(IN __user_id INT, IN __friend_id INT)
BEGIN
DECLARE __sql_statement VARCHAR(512);

INSERT INTO kaneva.friend_transactions( txn_type, user_id, friend_id, txn_time )
VALUES ('DEL', __user_id, __friend_id, NOW() );

DELETE FROM kaneva.friends WHERE user_id = __user_id AND friend_id = __friend_id;
SELECT ROW_COUNT() INTO @rowcnt1;

DELETE FROM kaneva.friends WHERE user_id = __friend_id AND friend_id = __user_id;
SELECT ROW_COUNT() INTO @rowcnt2;

IF (@rowcnt1 = 1) THEN
	UPDATE kaneva.users_stats SET number_of_friends = number_of_friends - 1 WHERE user_id = __user_id AND number_of_friends > 0;
END IF;

IF (@rowcnt2 = 1) THEN
	UPDATE kaneva.users_stats SET number_of_friends = number_of_friends -1 WHERE user_id = __friend_id AND number_of_friends > 0;
END IF;

INSERT IGNORE INTO friends_deleted VALUES(__user_id,__friend_id,NOW());
      
END
;;
DELIMITER ;