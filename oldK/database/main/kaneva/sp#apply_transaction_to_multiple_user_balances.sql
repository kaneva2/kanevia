-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS apply_transaction_to_multiple_user_balances;

DELIMITER ;;
CREATE PROCEDURE `apply_transaction_to_multiple_user_balances`(in user_id_list text, in user_email_list text, in transaction_amount float,
							in transaction_type smallint unsigned, in v_kei_point_id varchar(20), OUT return_code INT, OUT user_balance float, OUT tranId INT, OUT _user_id INT )
BEGIN
  DECLARE done INT DEFAULT 0;
  DECLARE _count INT DEFAULT 0;
  DECLARE _userID INT;

  DECLARE user_id_cursor CURSOR FOR select user_id from __tempIDsTranscations;
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

  
  IF user_id_list is NOT NULL THEN
       Set @_stmt := CONCAT("Create table __tempIDsTranscations as select user_id from kaneva.users where user_id in (", user_id_list, ');');
   ELSE
       Set @_stmt := CONCAT("Create table __tempIDsTranscations as select user_id from kaneva.users where email in (", user_email_list, ');');
  END IF;

  
  PREPARE __stmt FROM @_stmt;
  EXECUTE __stmt;
  DEALLOCATE PREPARE __stmt;

  
  SET done = 0;

  OPEN user_id_cursor;

  REPEAT
    FETCH user_id_cursor INTO _userID;
      call kaneva.apply_transaction_to_user_balance( _userID, transaction_amount, transaction_type, v_kei_point_id, return_code, user_balance, tranId);
      
      set _count = _count + 1;

     
     SET _userID = -1;
     if return_code = -1 then
           SET done = 1;
      END IF;
  UNTIL done END REPEAT;
  CLOSE user_id_cursor;

  
  drop table if exists __tempIDsTranscations ;

  
  
  
END
;;
DELIMITER ;