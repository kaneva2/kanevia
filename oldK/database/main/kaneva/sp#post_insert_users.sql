-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS post_insert_users;

DELIMITER ;;
CREATE PROCEDURE `post_insert_users`(IN __new_user_id INT(11))
BEGIN
  Insert IGNORE into user_profiles (user_id) VALUES(__new_user_id);
END
;;
DELIMITER ;