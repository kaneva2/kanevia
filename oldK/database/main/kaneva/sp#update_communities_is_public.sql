-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_communities_is_public;

DELIMITER ;;
CREATE PROCEDURE `update_communities_is_public`(_communityId INTEGER SIGNED, _isPublic CHAR(1))
BEGIN
	DECLARE _gameId INT(11) DEFAULT NULL;
	DECLARE _gameAccessId INT(11) DEFAULT NULL;
	DECLARE _modifierId INT(11) DEFAULT NULL;
	
	SELECT game_id, creator_id INTO _gameId, _modifierId
	FROM communities_game cg
	INNER JOIN communities c ON c.community_id = cg.community_id
	WHERE cg.community_id = _communityId;

	-- If this community represents a 3dapp, we need to update its access record
	IF _gameId IS NOT NULL THEN
		IF _isPublic = 'Y' THEN
			SET _gameAccessId = 1;
		ELSEIF _isPublic = 'N' THEN
			SET _gameAccessId = 7;
		ELSE
			SET _gameAccessId = 2;
		END IF;

		CALL developer.transition_game_access(_gameId, _modifierId, _gameAccessId, 'gameonly');
	END IF;

	-- Update the community
	UPDATE communities
	SET is_public = _isPublic
	WHERE community_id = _communityId;

END
;;
DELIMITER ;