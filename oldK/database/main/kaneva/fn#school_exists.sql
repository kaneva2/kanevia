-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS school_exists;

DELIMITER ;;
CREATE FUNCTION `school_exists`(__name varchar(80)) RETURNS int(11)
    READS SQL DATA
BEGIN
  DECLARE __school_id INTEGER;
  
  Select school_id into __school_id from kaneva.schools WHERE name=__name;
  if (__school_id IS NULL) THEN
    return 0;
  END IF;
  return __school_id;
END
;;
DELIMITER ;