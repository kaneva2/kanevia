-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS remove_interest_from_user;

DELIMITER ;;
CREATE PROCEDURE `remove_interest_from_user`(IN __user_id INT, IN __interest_id INT)
BEGIN
-- remove a specified interest from a specified user
  DECLARE __ic_id INT;
	Select ic_id INTO __ic_id from kaneva.interests where interest_id = __interest_id;
	 
  DELETE from kaneva.user_interests where user_id = __user_id AND interest_id = __interest_id;
	
	-- Update the counts 
	update kaneva.user_interest_category_counts SET `count`=`count` - 1 WHERE  user_id = __user_id AND ic_id = __ic_id and `count` > 0;
	update kaneva.users_stats SET `interest_count`=`interest_count` - 1 WHERE  user_id = __user_id and `interest_count` > 0;
	
END
;;
DELIMITER ;