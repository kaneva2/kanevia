-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_cat_license_more;

DELIMITER ;;
CREATE PROCEDURE `update_assets_cat_license_more`(IN __asset_id INT, IN __asset_type_id INT,
		IN __category1_id INT, IN __category2_id INT, IN __category3_id INT,
		IN __amount DECIMAL(12,2), IN __kei_point_id ENUM('KPOINT','GPOINT','MPOINT','DOLLAR'),
		IN __image_caption VARCHAR(100), IN __asset_rating_id INT, IN __last_updated_user_id INT,
		IN _license_name VARCHAR(50), IN __license_additional VARCHAR(100), IN __license_cc VARCHAR(100),
		IN __license_URL VARCHAR(200), IN __license_type CHAR(1), IN __permission INT,
		IN __permission_group INT, IN __status_id INT, IN __instructions TEXT,
		IN __company_name VARCHAR(100))
BEGIN
DECLARE _old_status_id INT;
DECLARE _old_owner_id INT;
DECLARE _old_asset_type_id INT;
DECLARE _new_published ENUM('Y','N');
DECLARE _new_public ENUM('Y','N');
DECLARE _new_playlistable ENUM('Y','N');
DECLARE _new_mature ENUM('Y','N');
DECLARE _old_publish_status_id INT;
DECLARE _old_permission INT;
DECLARE _atid INT;

SELECT status_id, publish_status_id, permission, asset_type_id, owner_id
	INTO _old_status_id, _old_publish_status_id, _old_permission, _old_asset_type_id, _old_owner_id
	FROM assets WHERE asset_id = __asset_id;

SET _new_published = IF(( _old_publish_status_id=10 & ( __status_id=1 | __status_id=3)),'Y','N');
SET _new_public       = IF (__permission <> 2,'Y','N');
SET _new_playlistable = IF(( (__asset_type_id=2) | (__asset_type_id=4) ),'Y','N');
SET _new_mature       = IF(( (__asset_rating_id=3) | (__asset_rating_id=6) | (__asset_rating_id=9) ),'Y','N');

UPDATE assets 
	SET asset_type_id = __asset_type_id,
        category1_id = __category1_id,
        category2_id = __category2_id,
	category3_id = __category3_id,
        amount = __amount,
        kei_point_id = __kei_point_id,
        image_caption = __image_caption,
        asset_rating_id = __asset_rating_id,
        last_updated_date = NOW(),
        last_updated_user_id = __last_updated_user_id,
        license_name = _license_name,
        license_additional = __license_additional,
        license_cc  = __license_cc,
        license_URL  = __license_URL,
        license_type = __license_type,
	permission = __permission,
	permission_group = __permission_group,
        status_id = __status_id,
        instructions = __instructions,
        company_name = __company_name,
        published = _new_published,
        public = _new_public,
        playlistable = _new_playlistable,
        mature = _new_mature
	WHERE asset_id = __asset_id;

SET _atid = __asset_type_id;
 
  
IF ( ((__status_id <>1) AND (__status_id <> 3) ) AND ((_old_status_id =1) OR (_old_status_id = 3      )) ) OR
   ( ((__permission =2) OR (__permission = 3) ) AND ((_old_permission <> 2) OR (_old_permission <> 3)) ) THEN

	UPDATE summary_assets_by_type SET `count`=`count`-1 WHERE asset_type_id = _old_asset_type_id AND `count` > 0;

	CALL decrement_communities_assets_counts_by_asset_and_type(__asset_id,__asset_type_id);
	    
	IF (__permission<>2) AND (__permission <>3) THEN
		UPDATE summary_assets_by_type_by_user SET `count`=`count`- 1 WHERE user_id= _old_owner_id AND asset_type_id = __asset_type_id AND `count` > 0;
	END IF;
	    
	-- should this first AND be an OR?
	ELSEIF ( ((__status_id =1) AND (__status_id = 3) ) AND ((_old_status_id <>1) OR (_old_status_id <> 3      )) ) OR
     ( ((__permission <>2) OR (__permission <> 3) ) AND ((_old_permission = 2) OR (_old_permission = 3)) ) THEN
        		
        UPDATE summary_assets_by_type SET `count`=`count`+1 WHERE asset_type_id = _old_asset_type_id;
    
	IF (__permission=2) OR (__permission=3) THEN
		UPDATE summary_assets_by_type_by_user SET `count`=`count`+ 1 WHERE user_id= _old_owner_id AND asset_type_id = __asset_type_id;
	END IF;
END IF;

	  
IF (_old_asset_type_id <> __asset_type_id) THEN
	UPDATE summary_assets_by_type SET `count`=`count`-1 WHERE asset_type_id = _old_asset_type_id AND `count` > 0;
	UPDATE summary_assets_by_type SET `count`=`count`+1 WHERE asset_type_id=__asset_type_id;
	UPDATE summary_assets_by_type_by_user SET `count`=`count`-1 WHERE asset_type_id=_old_asset_type_id AND user_id=_old_owner_id AND `count` > 0;
	UPDATE summary_assets_by_type_by_user SET `count`=`count`+1 WHERE asset_type_id=__asset_type_id AND user_id=_old_owner_id;

	CALL decrement_communities_assets_counts_by_asset_and_type(__asset_id,_old_asset_type_id);
	CALL increment_communities_assets_counts_by_asset_and_type(__asset_id,__asset_type_id);
END IF;

END
;;
DELIMITER ;