-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS send_birthday_gifts;

DELIMITER ;;
CREATE PROCEDURE `send_birthday_gifts`()
BEGIN

DECLARE _from_id INT;
DECLARE _is_free INT;
DECLARE _user_id INT;
DECLARE done INT;
DECLARE _gift_id INT;
DECLARE _email_type INT;
DECLARE _email_template INT;
DECLARE _status INT;

DECLARE theList CURSOR FOR 
	SELECT user_id 
		FROM kaneva.users u
		INNER JOIN kaneva.birthday_matching bm ON u.birth_date = bm.birthday
		WHERE status_id = 1;


DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

SET _from_id        = 1;    
SET _is_free        = 1;    
SET _gift_id        = 1089; 
SET _email_type     = 19;   
SET _email_template = 0;    
SET _status         = 0;
SET done			= 0;

CALL update_birthday_matching();

OPEN theList;
REPEAT
	SET _user_id = 0;
	FETCH theList INTO _user_id;
	IF _user_id > 0 THEN
		CALL wok.send_gift_to_player(_from_id, _user_id, _gift_id, _is_free, _email_type, _email_template);
        SET _status = 1;
	END IF;
UNTIL done END REPEAT;

SELECT _status;

END
;;
DELIMITER ;