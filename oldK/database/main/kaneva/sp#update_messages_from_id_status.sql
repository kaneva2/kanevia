-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_messages_from_id_status;

DELIMITER ;;
CREATE PROCEDURE `update_messages_from_id_status`(IN __message_id INT, __from_id INT, 
		__from_viewable ENUM('S','D'))
BEGIN
DECLARE _OLD_from_viewable ENUM('S','D');
DECLARE _type TINYINT;
DECLARE __sql_statement VARCHAR(512);

SELECT from_viewable, `type` INTO _OLD_from_viewable, _type FROM messages WHERE message_id = __message_id;

UPDATE messages 
	SET from_viewable = __from_viewable
	WHERE message_id = __message_id;
 
IF (_OLD_from_viewable='S') AND ( __from_viewable = 'D') AND (_type < 4) THEN  -- deleted
	UPDATE kaneva.users_stats SET number_outbox_messages = number_outbox_messages - 1 WHERE user_id = __from_id AND number_outbox_messages > 0;
END IF;
 
END
;;
DELIMITER ;