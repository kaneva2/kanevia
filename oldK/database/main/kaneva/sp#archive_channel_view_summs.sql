-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS archive_channel_view_summs;

DELIMITER ;;
CREATE PROCEDURE `archive_channel_view_summs`( in startdate date, in enddate date )
begin
	replace into my_kaneva.summary_channel_views select channel_id,dt_stamp,count from kaneva.summary_channel_views where dt_stamp between startdate and enddate;
	delete from kaneva.summary_channel_views where dt_stamp between startdate and enddate;
end
;;
DELIMITER ;