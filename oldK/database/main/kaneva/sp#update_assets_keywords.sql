-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_keywords;

DELIMITER ;;
CREATE PROCEDURE `update_assets_keywords`(IN __asset_id INT, IN __keywords TEXT)
BEGIN

UPDATE assets 
	SET keywords = __keywords
	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;