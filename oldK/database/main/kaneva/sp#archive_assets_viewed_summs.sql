-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS archive_assets_viewed_summs;

DELIMITER ;;
CREATE PROCEDURE `archive_assets_viewed_summs`( in startdate date, in enddate date )
begin
	replace into my_kaneva.summary_assets_viewed select asset_id,dt_stamp,count from kaneva.summary_assets_viewed where dt_stamp between startdate and enddate;
	delete from kaneva.summary_assets_viewed where dt_stamp between startdate and enddate;
end
;;
DELIMITER ;