-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_ProfilePage;

DELIMITER ;;
CREATE PROCEDURE `delete_ProfilePage`(IN _page_id INT)
BEGIN
        DECLARE done INT DEFAULT 0;
        DECLARE _module_page_id INT;
        DECLARE _module_id INT;
        DECLARE _id INT;
      DECLARE layout_page_module_cursor CURSOR FOR SELECT module_page_id, module_id, id FROM layout_page_modules WHERE page_id = _page_id;
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
 
      
        SET done = 0;
 
        OPEN layout_page_module_cursor;

        REPEAT
          FETCH layout_page_module_cursor INTO _module_page_id, _module_id, _id;
          IF NOT done THEN

            
        CASE _module_id
                        WHEN 1 THEN DELETE FROM layout_module_title_text WHERE module_page_id = _module_page_id;
                        WHEN 2 THEN DELETE FROM layout_module_blogs WHERE module_page_id = _module_page_id;
                        WHEN 3 THEN DELETE FROM layout_module_profile WHERE module_page_id = _module_page_id;
                        WHEN 4 THEN DELETE FROM layout_module_friends WHERE module_page_id = _module_page_id;
                        WHEN 5 THEN DELETE FROM layout_module_comments WHERE module_page_id = _module_page_id;
                        
                        WHEN 7 THEN DELETE FROM layout_module_html WHERE module_page_id = _module_page_id;
                        WHEN 8 THEN DELETE FROM layout_module_menu WHERE module_page_id = _module_page_id;
                        WHEN 9 THEN DELETE FROM layout_module_my_picture WHERE module_page_id = _module_page_id;
                        WHEN 10 THEN DELETE FROM layout_module_multiple_pictures WHERE module_page_id = _module_page_id;
                        WHEN 11 THEN DELETE FROM layout_module_stores WHERE module_page_id = _module_page_id;
                        WHEN 12 THEN DELETE FROM layout_module_channels WHERE module_page_id = _module_page_id;
                        WHEN 13 THEN DELETE FROM layout_module_single_picture WHERE module_page_id = _module_page_id;
                        
                        
                        WHEN 16 THEN DELETE FROM layout_module_slide_show WHERE module_page_id = _module_page_id;
                        WHEN 17 THEN DELETE FROM layout_module_stores WHERE module_page_id = _module_page_id;
                        WHEN 18 THEN DELETE FROM layout_module_stores WHERE module_page_id = _module_page_id;
                        WHEN 19 THEN DELETE FROM layout_module_counter WHERE module_page_id = _module_page_id;
                        
                        WHEN 21 THEN DELETE FROM layout_module_system_stats WHERE module_page_id = _module_page_id;
                        WHEN 22 THEN DELETE FROM layout_module_new_people WHERE module_page_id = _module_page_id;
                        WHEN 23 THEN DELETE FROM layout_module_control_panel WHERE module_page_id = _module_page_id;
                        WHEN 24 THEN DELETE FROM layout_module_channel_owner WHERE module_page_id = _module_page_id;
                        WHEN 25 THEN DELETE FROM layout_module_channel_members WHERE module_page_id = _module_page_id;
                        WHEN 26 THEN DELETE FROM layout_module_events WHERE module_page_id = _module_page_id;
                        WHEN 27 THEN DELETE FROM layout_module_forum WHERE module_page_id = _module_page_id;
                        WHEN 28 THEN DELETE FROM layout_module_channel_control_panel WHERE module_page_id = _module_page_id;
                        WHEN 29 THEN DELETE FROM layout_module_channel_description WHERE module_page_id = _module_page_id;
                        WHEN 30 THEN DELETE FROM layout_module_mixed_media WHERE module_page_id = _module_page_id;
                        
                        WHEN 32 THEN DELETE FROM layout_module_omm_media WHERE module_page_id = _module_page_id;
                        WHEN 33 THEN DELETE FROM layout_module_omm_playlist WHERE module_page_id = _module_page_id;
                        WHEN 34 THEN DELETE FROM layout_module_omm_media WHERE module_page_id = _module_page_id;
                        WHEN 35 THEN DELETE FROM layout_module_omm_playlist WHERE module_page_id = _module_page_id;
                        WHEN 36 THEN DELETE FROM layout_module_channel_top_contributors WHERE module_page_id = _module_page_id;
                        WHEN 37 THEN DELETE FROM layout_module_contest_upload WHERE module_page_id = _module_page_id;
                        WHEN 38 THEN DELETE FROM layout_module_contest_submissions WHERE module_page_id = _module_page_id;
                        WHEN 39 THEN DELETE FROM layout_module_contest_get_raved WHERE module_page_id = _module_page_id;
                        WHEN 40 THEN DELETE FROM layout_module_user_gifts WHERE module_page_id = _module_page_id;
                        WHEN 41 THEN DELETE FROM layout_module_contest_profile_header WHERE module_page_id = _module_page_id;
                        WHEN 42 THEN DELETE FROM layout_module_contest_topvotes WHERE module_page_id = _module_page_id;
                        WHEN 43 THEN DELETE FROM layout_module_control_panel WHERE module_page_id = _module_page_id;
                        WHEN 44 THEN DELETE FROM layout_module_channel_portal WHERE module_page_id = _module_page_id;
                        WHEN 45 THEN DELETE FROM layout_module_leader_board WHERE module_page_id = _module_page_id;
                        WHEN 46 THEN DELETE FROM layout_module_hotnew_stuff WHERE module_page_id = _module_page_id;
                        WHEN 47 THEN DELETE FROM layout_module_newest_media WHERE module_page_id = _module_page_id;
                        WHEN 48 THEN DELETE FROM layout_module_billboard WHERE module_page_id = _module_page_id;
                        WHEN 49 THEN DELETE FROM layout_module_most_viewed WHERE module_page_id = _module_page_id;
                        WHEN 50 THEN DELETE FROM layout_module_user_upload WHERE module_page_id = _module_page_id;
                        WHEN 51 THEN DELETE FROM layout_module_leader_board WHERE module_page_id = _module_page_id;
                        WHEN 52 THEN DELETE FROM layout_module_contest_profile_header WHERE module_page_id = _module_page_id;
                        WHEN 53 THEN DELETE FROM layout_module_interests WHERE module_page_id = _module_page_id;
                        WHEN 54 THEN DELETE FROM layout_module_personal WHERE module_page_id = _module_page_id;
                        WHEN 55 THEN DELETE FROM layout_module_channel_info WHERE module_page_id = _module_page_id;
                        WHEN 56 THEN DELETE FROM layout_module_fame WHERE module_page_id = _module_page_id;
                        WHEN 57 THEN DELETE FROM layout_module_html2 WHERE module_page_id = _module_page_id;
                  ELSE
            BEGIN
            END;
      END CASE;
 
 
          END IF;
        UNTIL done END REPEAT;
 
        CLOSE layout_page_module_cursor; 
 
        
        DELETE FROM layout_page_modules WHERE page_id = _page_id;         
 
        
          DELETE FROM layout_pages WHERE page_id = _page_id;      
 
    END
;;
DELIMITER ;