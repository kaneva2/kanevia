-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS get_o;

DELIMITER ;;
CREATE FUNCTION `get_o`() RETURNS bigint(20)
    NO SQL
BEGIN
  -- This equals Select UNIX_TIMESTAMP('2037-12-31 23:59:59')*2;
	return kaneva.master_date() - UNIX_TIMESTAMP(NOW());
END
;;
DELIMITER ;