-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_community_members_delete_all;

DELIMITER ;;
CREATE PROCEDURE `update_community_members_delete_all`(IN __community_id INT)
BEGIN

DECLARE _account_type_id INT;
DECLARE _user_id INT;
DECLARE _status_id INT;
DECLARE _old_status_id INT;
DECLARE _own_mod_count INT;
DECLARE _how_many_to_delete INT DEFAULT 0;
DECLARE done INT DEFAULT 0;

DECLARE cm_cursor CURSOR FOR 
		SELECT user_id, account_type_id, status_id 
		FROM community_members WHERE community_id = __community_id;
			
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;

OPEN cm_cursor;
REPEAT
	FETCH cm_cursor INTO _user_id, _account_type_id, _status_id;
	IF NOT done THEN 

	SELECT status_id, account_type_id INTO _old_status_id, _account_type_id 
		FROM community_members  WHERE community_id = __community_id AND user_id = _user_id;

	UPDATE community_members 
		SET status_id = 4
		WHERE community_id = __community_id AND user_id = _user_id;

	-- decrement pending members
	IF (_old_status_id = 2) THEN
		UPDATE channel_stats 
			SET number_of_pending_members =CASE WHEN number_of_pending_members>0 THEN number_of_pending_members-1 ELSE 0 END
			WHERE channel_id = __community_id;
		IF (_account_type_id = 2 OR _account_type_id = 1) THEN
		      UPDATE users SET own_mod = 'Y' WHERE user_id = _user_id;
		END IF;
	END IF;

	-- decrement members
	IF (_old_status_id = 1) THEN
		UPDATE channel_stats 
			SET number_of_members = number_of_members - 1
			WHERE channel_id = __community_id;
		IF (_account_type_id = 2 OR _account_type_id = 1) THEN
			SET _own_mod_count = (SELECT COUNT(*) AS own_mod
				FROM community_members cm
				INNER JOIN communities c ON cm.community_id = c.community_id
				WHERE user_id = _user_id AND (account_type_id = 2 OR account_type_id = 1)
				AND c.status_id = 1 AND c.is_personal = 0);
		     IF (_own_mod_count = 0) OR (_own_mod_count IS NULL) THEN
		       UPDATE users SET own_mod='N' WHERE user_id = _user_id;
		     END IF;
		END IF;
	END IF;
END IF;
UNTIL done END REPEAT;
CLOSE cm_cursor;

END
;;
DELIMITER ;