-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_school;

DELIMITER ;;
CREATE PROCEDURE `add_school`(IN __name VARCHAR(80), IN __city varchar(45), IN __state varchar(45), OUT __return_val INTEGER)
BEGIN
  DECLARE __school_id INTEGER;
  
  SET __school_id = school_exists(__name);
  if (__school_id > 0) THEN
    SET __return_val = __school_id;
     ELSE
    INSERT into kaneva.schools (name,city,`state`) VALUES(__name,__city,__state);
    SELECT LAST_INSERT_ID() into __school_id ;
    SET __return_val = __school_id;
  END IF;
 
END
;;
DELIMITER ;