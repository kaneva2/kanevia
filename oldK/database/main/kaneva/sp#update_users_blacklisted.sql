-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_users_blacklisted;

DELIMITER ;;
CREATE PROCEDURE `update_users_blacklisted`()
BEGIN
/*
status_id  name                  description                                
---------  --------------------  -------------------------------------------
        1  Registered Validated  User has registered and validated          
        2  NOT Validatated       User has registered and has NOT validated  
        3  Deleted               User was deleted                           
        4  Deleted By Admin      User was deleted by Admin                  
        5  Locked                User was locked by Admin                   
        6  Locked Validated      User has validated and was locked by Admin 
        7  Deleted Validated     User has validated and was deleted by Admin
*/
UPDATE users u
	INNER JOIN users_email ue ON u.user_id = ue.user_id
	SET u.status_id = 2 
	WHERE u.status_id = 1 
	AND ue.email_hash IN (select distinct email_hash from emails_blacklisted_today);

UPDATE users u
	INNER JOIN users_email ue ON u.user_id = ue.user_id
	SET u.status_id = 5 
	WHERE u.status_id = 6 
	AND ue.email_hash IN (select distinct email_hash from emails_blacklisted_today);

UPDATE invites 
	SET invite_status_id = 5 
	WHERE email_hash IN (SELECT email_hash FROM emails_blacklisted_today)
	AND invite_status_id = 1 AND opened_invite = 0;

END
;;
DELIMITER ;