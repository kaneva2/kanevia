-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_percent_complete;

DELIMITER ;;
CREATE PROCEDURE `update_assets_percent_complete`(IN __asset_id INT, IN __percent_complete INT)
BEGIN

UPDATE assets 
	SET percent_complete = __percent_complete
	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;