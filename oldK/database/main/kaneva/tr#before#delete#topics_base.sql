-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS delete_topics_base;

DELIMITER ;;
CREATE TRIGGER `delete_topics_base` BEFORE DELETE ON `topics_base` FOR EACH ROW BEGIN
    Insert Into my_kaneva.arch_topics_base Select * from topics_base where topic_id = old.topic_id;  
    DELETE from my_kaneva.ft_topics where topic_id = old.topic_id;
    UPDATE forums SET number_of_topics = (number_of_topics - 1) WHERE forum_id = old.forum_id;
END
;;
DELIMITER ;