-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_communities_keywords;

DELIMITER ;;
CREATE PROCEDURE `update_communities_keywords`(IN __community_id INT, IN __keywords TEXT)
BEGIN

UPDATE communities 
	SET keywords = __keywords
	WHERE community_id = __community_id;                      

END
;;
DELIMITER ;