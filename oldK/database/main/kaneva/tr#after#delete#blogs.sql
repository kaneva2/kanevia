-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS delete_blogs_base;

DELIMITER ;;
CREATE TRIGGER `delete_blogs_base` AFTER DELETE ON `blogs` FOR EACH ROW BEGIN 
	DELETE from my_kaneva.ft_blogs where blog_id = old.blog_id;
END
;;
DELIMITER ;