-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS happy_birthday;

DELIMITER ;;
CREATE PROCEDURE `happy_birthday`()
BEGIN
  update kaneva.users set age=DATE_FORMAT(FROM_DAYS(TO_DAYS(CONCAT(DATE(NOW()),' 23:59:59'))-TO_DAYS(birth_date)), '%Y') where age != DATE_FORMAT(FROM_DAYS(TO_DAYS(CONCAT(DATE(NOW()),' 23:59:59'))-TO_DAYS(birth_date)), '%Y'); 
END
;;
DELIMITER ;