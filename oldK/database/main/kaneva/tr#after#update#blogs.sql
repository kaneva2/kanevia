-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS after_blogs_update;

DELIMITER ;;
CREATE TRIGGER `after_blogs_update` AFTER UPDATE ON `blogs` FOR EACH ROW BEGIN
  if ( (new.status_id <> 1) AND (old.status_id = 1) ) THEN
    update users_stats set number_of_blogs = number_of_blogs - 1 WHERE user_id = new.created_user_id;
  END IF;
END
;;
DELIMITER ;