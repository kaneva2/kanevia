-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_comment;

DELIMITER ;;
CREATE PROCEDURE `delete_comment`(in p_id int)
BEGIN
  if (p_id > 0) THEN

    
	  
	  delete from comments where comment_id=p_id;
  END IF;

  END
;;
DELIMITER ;