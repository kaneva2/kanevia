-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS view_archiver;

DELIMITER ;;
CREATE PROCEDURE `view_archiver`( in startdate datetime, in enddate datetime)
begin
	declare _current_date datetime;
	declare next_date datetime;

  set _current_date = startdate;
	while _current_date < enddate do
		set next_date = date_add(_current_date, interval 86399 second);
		if (next_date > enddate) then
		  SET next_date = enddate;
		END IF;
		call kaneva.archive_channel_views(_current_date,next_date);
		call kaneva.archive_asset_views(_current_date,next_date);
		set _current_date = date_add(next_date, interval 1 second);
	end while;
end
;;
DELIMITER ;