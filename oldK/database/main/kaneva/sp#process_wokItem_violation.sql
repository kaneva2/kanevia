-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS process_wokItem_violation;

DELIMITER ;;
CREATE PROCEDURE `process_wokItem_violation`(wokItemID INT UNSIGNED, violationAction INT UNSIGNED, modifierID  INT UNSIGNED)
BEGIN

   DECLARE _MATURE_PASS INT DEFAULT 1;
   DECLARE _GENERAL_PASS INT DEFAULT 1;
   DECLARE _REMOVE INT DEFAULT 4;
   DECLARE _AP INT DEFAULT 2;
   DECLARE _last_action INT DEFAULT 0;
   DECLARE _ACTIVE INT DEFAULT 1;
    
  START TRANSACTION;
    
	/*begin repetative processing*/
	IF violationAction = 2 THEN -- Mark as AP (adult), modifier id = 0 for kaneva
		CALL wok.add_pass_group_items(_MATURE_PASS,wokItemID,0);

		-- update violation action
		UPDATE
			violation_reports 
		SET 
			violation_action_type_id = _AP,
			last_updated = Now(),
			modifiers_id = modifierID  
		WHERE 
			wok_item_id = wokItemID;

	ELSEIF violationAction = 4 THEN -- Remove

		-- remove from shopping site 0 = deleted
		CALL wok.update_item_active_state(wokItemID, 0);

		-- remove from pending
		CALL wok.delete_item_from_pending_adds(wokItemID);

		-- remove from dynamic objects
		CALL wok.delete_item_from_dynamic_objects(wokItemID);

		-- remove from inventories
		CALL wok.delete_item_from_inventories(wokItemID);

		-- update violation action
		UPDATE
			violation_reports
		SET
			violation_action_type_id = _REMOVE,
			last_updated = Now(),
			modifiers_id = modifierID
		WHERE
			wok_item_id = wokItemID;

 	ELSEIF violationAction = 5 THEN -- administrative over ride to good status

		-- get the last action done on the item
		SELECT
			violation_action_type_id INTO _last_action
		FROM 
			violation_reports 
		WHERE
			wok_item_id = wokItemID
		ORDER BY
			violation_id DESC
		LIMIT 1;
		
		-- set the item back based on the last action done
		IF _last_action = 2 THEN -- remove the AP  setting
			
			-- update assets restriction level
      			CALL wok.delete_pass_group_items(_MATURE_PASS,wokItemID,modifierID);
		
		ELSEIF _last_action = 4 THEN -- undelete it

       			-- mark as deleted
		   	CALL update_asset_status(_ACTIVE, assetID);

		    	-- add back to shopping site
		    	CALL wok.update_item_active_state(wokItemID, _ACTIVE);

		END IF;

		-- update violation action
		UPDATE 
			violation_reports 
		SET
			violation_action_type_id = _ADMIN_OK, 
			last_updated = Now(),
			modifiers_id = modifierID 
		WHERE 
			wok_item_id = wokItemID;
	END IF;
	
	-- return success = 1 if no exceptions thrown
	select 1;

  COMMIT;
END
;;
DELIMITER ;