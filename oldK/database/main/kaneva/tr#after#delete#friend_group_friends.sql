-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS friend_groups_friends_delete;

DELIMITER ;;
CREATE TRIGGER `friend_groups_friends_delete` AFTER DELETE ON `friend_group_friends` FOR EACH ROW BEGIN
  update friend_groups set friend_count=friend_count-1 WHERE friend_group_id=old.friend_group_id;
END
;;
DELIMITER ;