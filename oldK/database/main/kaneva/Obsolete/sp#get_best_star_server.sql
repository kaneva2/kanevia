-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS kaneva.get_best_star_server;

DELIMITER //
CREATE PROCEDURE kaneva.get_best_star_server( IN _game_id INT(11), OUT _ip_address VARCHAR(50), OUT _port INT(11) )
BEGIN
	-- took query from getLoadBalancedServer
	-- this is load balancing logic (send to least populated)
	-- if should match getLoadBalancedServer
	SET _ip_address = NULL;
	SET _port = NULL;
	
	SELECT ip_address, port INTO _ip_address, _port
	  FROM current_game_engines ge
	 WHERE ge.status_id = 1 -- 1 is running
	   AND timestampdiff (minute, last_ping_datetime, NOW()) < wok.deadServerInterval() -- minute limit 
	   AND ge.internally_hosted = 'Y'
	   AND ge.number_of_players < (ge.max_players - 10 ) -- 10 is pad
	   AND ge.game_asset_id = _game_id
	 ORDER BY number_of_players LIMIT 1;
	 
	 IF _ip_address IS NULL THEN 
		SET _ip_address = '';
		SET _port = 0;
	END IF;

END //

DELIMITER ;

