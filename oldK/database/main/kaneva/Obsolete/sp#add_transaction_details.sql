-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- The following is a stored procedure that adds details to user balance transaction
DROP PROCEDURE IF EXISTS kaneva.add_transaction_details;

DELIMITER //
CREATE PROCEDURE kaneva.add_transaction_details( in transId int, in v_use_value int,
							in v_global_id int, in v_qty INT, in v_name varchar(125) )
BEGIN
	 
	INSERT INTO wok_transaction_detail_log  
	      ( trans_id, use_value,    global_id,   quantity, name )
    VALUES( transId,  v_user_value, v_global_id, v_qty,    v_name  );

END //

DELIMITER ;

