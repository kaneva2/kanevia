-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS post_update_community_members;

DELIMITER //

-- called from the update trigger on community_members
CREATE PROCEDURE post_update_community_members (IN __new_status_id INT(11), IN __old_status_id INT(11), 
			IN __new_community_id INT(11), IN __old_community_id INT(11), IN __new_user_id INT(11), 
			IN __old_user_id INT(11), IN __new_account_type_id INT(11))
BEGIN
  DECLARE _owner_id INT;
  DECLARE _number_of_members INT;
  DECLARE _points_allowed INT;
  DECLARE _points_attained INT;
  DECLARE _own_mod_count INT;
-- 1
  IF (__new_status_id <> 2) and (__old_status_id = 2) then
      update channel_stats set number_of_pending_members = number_of_pending_members - 1 WHERE channel_id = __new_community_id;
  END IF;
-- 2/3 
  IF (__new_status_id=1) AND (__old_status_id <> 1) then
    update channel_stats set number_of_members = number_of_members + 1 WHERE channel_id = __new_community_id;
    IF (__new_account_type_id = 2 OR __new_account_type_id = 1) THEN
      update users set own_mod = 'Y' WHERE user_id = __new_user_id;
    END IF;
  END IF;
 -- 4
  IF (__new_status_id<>1) AND (__old_status_id = 1) then
    update channel_stats set number_of_members = number_of_members - 1 WHERE channel_id = __new_community_id;
-- 5
    set _own_mod_count = (SELECT count(*) as own_mod
         FROM community_members cm
         INNER JOIN communities c ON cm.community_id = c.community_id
         WHERE
          user_id = __old_user_id
         AND (account_type_id = 2 OR account_type_id = 1)
         AND c.status_id = 1
         AND c.is_personal = 0);
-- 6
     if (_own_mod_count = 0) OR (_own_mod_count IS NULL) THEN
       update users set own_mod='N' WHERE user_id = __old_user_id;
     END IF;
  END IF; 

  SET _points_allowed=0;
-- 10
  Set _owner_id = (Select creator_id from communities where community_id = __new_community_id);
-- 11
  Set _number_of_members = (Select count(*) from community_members WHERE status_id=1 AND community_id = __old_community_id);
 
  if (_number_of_members >= 100) then
    SET _points_allowed = 3;
  ELSEIF (_number_of_members >=11) then
    SET _points_allowed = 2;
  ELSE
    SET _points_allowed = 1;
  END IF;

END //
DELIMITER ;