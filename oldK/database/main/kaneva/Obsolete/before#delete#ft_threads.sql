-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS delete_ft_threads;

DELIMITER //
CREATE TRIGGER delete_ft_threads BEFORE DELETE on ft_threads
FOR EACH ROW
BEGIN
  Insert into arch_ft_threads Select * from ft_threads where thread_id = old.thread_id;
END //
DELIMITER ;