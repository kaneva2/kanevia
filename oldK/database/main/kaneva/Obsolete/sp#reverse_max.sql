-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP function IF EXISTS kaneva.reverse_max;
DELIMITER //
CREATE function kaneva.reverse_max() RETURNS INT
	DETERMINISTIC
	NO SQL
BEGIN
  return 2147483647;
END //
DELIMITER ;