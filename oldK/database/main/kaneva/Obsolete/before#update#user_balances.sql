-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS kaneva.before_user_balances_update;
DELIMITER //
-- Trigger has the responsibility of updating the summary table for all negative balance changes per user daily
CREATE TRIGGER kaneva.before_user_balances_update BEFORE UPDATE ON kaneva.user_balances
FOR EACH ROW BEGIN
	declare spend_total decimal(12,3) default 0;

	IF NEW.kei_point_id = 'KPOINT' THEN
		IF NEW.balance < OLD.balance THEN
			SET spend_total = OLD.Balance - NEW.balance;
			INSERT INTO wok_transaction_log (created_date, user_id, debit, credit, balance_prev, balance) VALUES( now(), new.user_id, spend_total, 0.00, old.balance, new.balance );
		ELSE
			SET spend_total = NEW.balance - Old.Balance;
			INSERT INTO wok_transaction_log (created_date, user_id, debit, credit, balance_prev, balance) VALUES( now(), new.user_id, 0.00, spend_total, old.balance, new.balance );
		END IF;
    END IF;
END //

DELIMITER ;