-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

��D E L I M I T E R   / / 
 
 C R E A T E   T R I G G E R   i n s e r t _ a s   A F T E R   I N S E R T   o n   a s s e t _ s h a r e s 
 
 F O R   E A C H   R O W 
 
   B E G I N 
 
     D E C L A R E   a i d   I N T ; 
 
     s e t   a i d   =   n e w . a s s e t _ i d ; 
 
     U p d a t e   a s s e t s _ s t a t s   s e t   n u m b e r _ o f _ s h a r e s = n u m b e r _ o f _ s h a r e s + 1   w h e r e   a s s e t _ i d = a i d ; 
 
 E N D   / / 
 
 
 
 D E L I M I T E R   ; 