-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_asset_digg_aggregates;
DELIMITER //
CREATE PROCEDURE update_asset_digg_aggregates()
BEGIN
DECLARE __asset_group_id INT;

DROP TABLE IF EXISTS `__temp_`;
CREATE TABLE `__temp_`
(
  `asset_id` INT NOT NULL,
  `number_of_diggs` INT DEFAULT 0,
  PRIMARY KEY(`asset_id`)
)
ENGINE = MyISAM
CHARACTER SET utf8;

Insert into __temp_ Select asset_id, sum(count) from summary_asset_diggs WHERE date_stamp >= DATE_SUB(NOW(), INTERVAL 1 DAY) group by asset_id;

DROP TABLE IF EXISTS summary_asset_diggs_1_days;

ALTER TABLE __temp_ RENAME TO summary_asset_diggs_1_days;

-- refresh Top 10 Raved Videos playlist (if atleast 1 raved in the last 24 hours)
IF EXISTS (select sd.asset_id		FROM summary_asset_diggs_1_days sd
			INNER JOIN assets a 					ON sd.asset_id = a.asset_id
			INNER JOIN asset_types at 				ON a.asset_type_id = at.asset_type_id
			WHERE a.asset_type_id = 2 -- videos
			LIMIT 1) THEN

	SET __asset_group_id = (SELECT asset_group_id FROM kaneva.asset_groups WHERE channel_id = 1001736152 AND name = 'Top 10 Raved Videos');

	DELETE FROM asset_group_assets WHERE asset_group_id = __asset_group_id;

	SET @sort_order = -1;
	INSERT INTO asset_group_assets (asset_group_id, asset_id, sort_order)
	select __asset_group_id, sd.asset_id, @sort_order:=@sort_order+1 SortOrder
		FROM summary_asset_diggs_1_days sd
		INNER JOIN assets a 					ON sd.asset_id = a.asset_id
		INNER JOIN asset_types at 				ON a.asset_type_id = at.asset_type_id
		WHERE a.asset_type_id = 2 AND over_21_required = 'N' AND mature = 'N' 
		ORDER BY sd.number_of_diggs DESC
		LIMIT 10;

END IF;
/* --------------- */

DROP TABLE IF EXISTS __temp_;
CREATE TABLE `__temp_`
(
  `asset_id` INT NOT NULL,
  `number_of_diggs` INT DEFAULT 0,
  PRIMARY KEY(`asset_id`)
)
ENGINE = MyISAM
CHARACTER SET utf8;

Insert into __temp_ Select asset_id, sum(count) from summary_asset_diggs WHERE date_stamp >= DATE_SUB(NOW(), INTERVAL 7 DAY) group by asset_id;

DROP TABLE IF EXISTS summary_asset_diggs_7_days;

ALTER TABLE __temp_ RENAME TO summary_asset_diggs_7_days;

/* --------------- */

DROP TABLE IF EXISTS __temp_;
CREATE TABLE `__temp_`
(
  `asset_id` INT NOT NULL,
  `number_of_diggs` INT DEFAULT 0,
  PRIMARY KEY(`asset_id`)
)
ENGINE = MyISAM
CHARACTER SET utf8;

Insert into __temp_ Select asset_id, sum(count) from summary_asset_diggs WHERE date_stamp >= DATE_SUB(NOW(), INTERVAL 30 DAY) group by asset_id;

DROP TABLE IF EXISTS summary_asset_diggs_30_days;

ALTER TABLE __temp_ RENAME TO summary_asset_diggs_30_days;

/* --------------- */

DROP TABLE IF EXISTS __temp_;
CREATE TABLE `__temp_`
(
  `asset_id` INT NOT NULL,
  `number_of_diggs` INT DEFAULT 0,
  PRIMARY KEY(`asset_id`)
)
ENGINE = MyISAM
CHARACTER SET utf8;

Insert into __temp_ Select asset_id, sum(count) from summary_asset_diggs WHERE date_stamp >= DATE_SUB(NOW(), INTERVAL 1 YEAR) group by asset_id;

DROP TABLE IF EXISTS summary_asset_diggs_1_year;

ALTER TABLE __temp_ RENAME TO summary_asset_diggs_1_year;
END //
DELIMITER ;
