-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use kaneva;

\. kaneva_SchemaChanges_v52.sql

-- new or changed procedures
\. sp#add_background_sql_tasks.sql
\. sp#post_update_community_members.sql
\. sp#post_insert_users.sql
\. sp#post_update_users.sql
\. sp#archive_messages_deleted.sql
\. sp#purge_asset_views.sql
\. sp#purge_community_views.sql
\. sp#purge_messages_active.sql
\. sp#purge_messages_deleted.sql
\. sp#purge_messages_inactive.sql
\. sp#purge_web_health.sql

-- trigger changes
\. after#insert#community_members.sql
\. after#update#community_members.sql
\. after#insert#users.sql
\. after#update#users.sql

DROP PROCEDURE IF EXISTS kaneva_PromotePolicy_v52;
DELIMITER //
CREATE PROCEDURE kaneva_PromotePolicy_v52()
BEGIN
IF NOT EXISTS (SELECT 1 FROM schema_versions WHERE version = 52) THEN
	INSERT INTO `schema_versions` ( `version`, `description` ) 
		VALUES (52, 'DB Performance Changes Nov08' );
END IF;

END //

DELIMITER ;

CALL kaneva_PromotePolicy_v52;
DROP PROCEDURE IF EXISTS kaneva_PromotePolicy_v52;

