-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_users_not_validated;

DELIMITER //
CREATE PROCEDURE update_users_not_validated( IN _user_id INT)
BEGIN

UPDATE users SET status_id = 2 WHERE user_id = _user_id;
		
END//

DELIMITER ;
