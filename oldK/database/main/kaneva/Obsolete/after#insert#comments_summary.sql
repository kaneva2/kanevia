-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DELIMITER //
CREATE TRIGGER comment_count_insert after insert on comments
FOR EACH ROW
BEGIN
	DECLARE _comment_count INT;

	/*get current count of comments for the user for the day*/
	SET _comment_count = (SELECT comment_count FROM summary_comments_by_user_by_day WHERE comment_date = CURDATE() AND user_id = new.user_id) ;

	/*add or update comment count*/	
	if (_comment_count IS NOT NULL) AND (_comment_count > 0) then
		update summary_comments_by_user_by_day set comment_count = comment_count + 1 WHERE user_id = new.user_id AND comment_date = CURDATE();
	else
		Insert into summary_comments_by_user_by_day (comment_date,comment_count,user_id) VALUES(Date(new.created_date) ,1,new.user_id);
	end if;

END //
DELIMITER ;