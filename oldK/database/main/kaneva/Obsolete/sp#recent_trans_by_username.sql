-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS `kaneva`.`recent_trans_by_username`;
DELIMITER $$
CREATE PROCEDURE `kaneva`.`recent_trans_by_username`
	(__username	VARCHAR(60)) 
BEGIN

select t.trans_text, created_date, balance_prev, balance, transaction_amount, kei_point_id , 
		CASE WHEN kei_point_id = 'GPOINT' THEN 'Rewards' ELSE 'Credits' END  as CurrencyType -- GPOINTS = Rewards and KPOINTS = Credits
	from kaneva.wok_transaction_log tl, kaneva.transaction_type t
	where user_id = (select user_id from kaneva.users where username = __username)
	and tl.transaction_type = t.transaction_type
	and created_date > ADDDATE( NOW( ) , INTERVAL -60 DAY )
	order by created_date desc;

select * ,
	CASE WHEN kei_point_id = 'GPOINT' THEN 'Rewards' ELSE 'Credits' END  as CurrencyType -- GPOINTS = Rewards and KPOINTS = Credits
from kaneva.user_balances
where user_id = (select user_id from kaneva.users where username = __username);

END$$

DELIMITER ;

-- CALL kaneva.recent_trans_by_username ('reiny');
