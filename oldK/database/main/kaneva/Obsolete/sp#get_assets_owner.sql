-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_assets_owner;
DELIMITER ;;
CREATE PROCEDURE get_assets_owner(IN __asset_id INT)
BEGIN

SELECT owner_id FROM assets WHERE asset_id = __asset_id;

END ;;
DELIMITER ;
