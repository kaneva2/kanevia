-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

��D E L I M I T E R   / / 
 
 C R E A T E   T R I G G E R   a f t e r _ c o m m e n t s _ i n s e r t   A F T E R   I N S E R T   o n   c o m m e n t s 
 
 F O R   E A C H   R O W 
 
 B E G I N 
 
 
 
 	 D E C L A R E   _ c o m m e n t _ c o u n t   I N T ; 
 
 
 
 	 u p d a t e   u s e r s _ s t a t s   s e t   n u m b e r _ o f _ c o m m e n t s   =   n u m b e r _ o f _ c o m m e n t s   +   1   W H E R E   u s e r _ i d   =   n e w . u s e r _ i d ; 
 
 
 
 	 / * g e t   c u r r e n t   c o u n t   o f   c o m m e n t s   f o r   t h e   u s e r   f o r   t h e   d a y * / 
 
 	 S E T   _ c o m m e n t _ c o u n t   =   ( S E L E C T   c o m m e n t _ c o u n t   F R O M   s u m m a r y _ c o m m e n t s _ b y _ u s e r _ b y _ d a y   W H E R E   c o m m e n t _ d a t e   =   C U R D A T E ( )   A N D   u s e r _ i d   =   n e w . u s e r _ i d )   ; 
 
 
 
 	 / * a d d   o r   u p d a t e   c o m m e n t   c o u n t * / 	 
 
 	 i f   ( _ c o m m e n t _ c o u n t   I S   N O T   N U L L )   A N D   ( _ c o m m e n t _ c o u n t   >   0 )   t h e n 
 
 	 	 u p d a t e   s u m m a r y _ c o m m e n t s _ b y _ u s e r _ b y _ d a y   s e t   c o m m e n t _ c o u n t   =   c o m m e n t _ c o u n t   +   1   W H E R E   u s e r _ i d   =   n e w . u s e r _ i d   A N D   c o m m e n t _ d a t e   =   C U R D A T E ( ) ; 
 
 	 e l s e 
 
 	 	 I n s e r t   i n t o   s u m m a r y _ c o m m e n t s _ b y _ u s e r _ b y _ d a y   ( c o m m e n t _ d a t e , c o m m e n t _ c o u n t , u s e r _ i d )   V A L U E S ( D a t e ( n e w . c r e a t e d _ d a t e )   , 1 , n e w . u s e r _ i d ) ; 
 
 	 e n d   i f ; 
 
 
 
 
 
 E N D   / / 
 
 D E L I M I T E R   ; 
 
 