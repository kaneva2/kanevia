-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS delete_ft_topics;

DELIMITER //
CREATE TRIGGER delete_ft_topics BEFORE DELETE on ft_topics
FOR EACH ROW
BEGIN
  Insert into arch_ft_topics Select * from ft_topics where topic_id = old.topic_id;
END //
DELIMITER ;