-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

��C R E A T E   T R I G G E R   a f t e r _ a s s e t _ v i e w _ i n s e r t   A F T E R   I N S E R T   o n   a s s e t _ v i e w s 
 
 F O R   E A C H   R O W 
 
 B E G I N 
 
     D E C L A R E   n   D A T E ; 
 
     D E C L A R E   c   I N T ; 
 
     D E C L A R E   a i d   I N T ; 
 
     
 
     i f   ( n e w . a s s e t _ i d   I S   N O T   N U L L )   t h e n 
 
         s e t   a i d   =   n e w . a s s e t _ i d ; 
 
         s e t   n   =   D A T E ( N O W ( ) ) ; 
 
         s e t   c   =   ( s e l e c t   ` c o u n t `   f r o m   s u m m a r y _ a s s e t s _ v i e w e d   w h e r e   d t _ s t a m p = n   A N D   a s s e t _ i d = a i d ) ; 
 
 
 
         i f   c   i s   N U L L   o r   c   =   0   t h e n     
 
             I n s e r t   i n t o   s u m m a r y _ a s s e t s _ v i e w e d   V A L U E S ( a i d , n , 1 ) ; 
 
         e l s e 
 
             U p d a t e   s u m m a r y _ a s s e t s _ v i e w e d   s e t   c o u n t = c o u n t + 1   w h e r e   d t _ s t a m p = n   A N D   a s s e t _ i d = a i d ; 
 
         e n d   i f ; 
 
     e n d   i f ; 
 
     
 
 E N D   / / 
 
 D E L I M I T E R   ; 