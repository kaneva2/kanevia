-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

��D R O P   V I E W   I F   E X I S T S   c o m m u n i t i e s ; 
 
 C R E A T E   V I E W   c o m m u n i t i e s   A S   S e l e c t   c o m . * ,   ` n a m e ` , ` d e s c r i p t i o n ` , ` c r e a t o r _ u s e r n a m e ` , ` k e y w o r d s `   f r o m   c o m m u n i t i e s _ b a s e   c o m   I n n e r   J o i n   m y _ k a n e v a . f t _ c o m m u n i t i e s   f t _ c o m   o n   c o m . c o m m u n i t y _ i d   =   f t _ c o m . c o m m u n i t y _ i d ; 
 
 