-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DELIMITER $$

USE `kaneva`$$

DROP PROCEDURE IF EXISTS `_database_cleanup`$$

CREATE PROCEDURE `_database_cleanup`()
BEGIN

  DELETE FROM new_assets_by_type WHERE date_stamp <= DATE_SUB(NOW(),INTERVAL 30 DAY);
  DELETE FROM new_channels WHERE created_date <= DATE_SUB(NOW(), INTERVAL 30 DAY);

END$$

DELIMITER ;