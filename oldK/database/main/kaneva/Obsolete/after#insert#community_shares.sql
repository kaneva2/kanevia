-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

��D R O P   T R I G G E R   I F   E X I S T S   i n s e r t _ c s ; 
 
 
 
 D E L I M I T E R   / / 
 
 C R E A T E   T R I G G E R   i n s e r t _ c s   A F T E R   I N S E R T   o n   c o m m u n i t y _ s h a r e s 
 
 F O R   E A C H   R O W 
 
 B E G I N 
 
     D E C L A R E   c i d   I N T ; 
 
     s e t   c i d   =   n e w . c o m m u n i t y _ i d ; 
 
     U p d a t e   c h a n n e l _ s t a t s   s e t   n u m b e r _ t i m e s _ s h a r e d = n u m b e r _ t i m e s _ s h a r e d + 1   w h e r e   c h a n n e l _ i d = c i d ; 
 
     U p d a t e   u s e r s _ s t a t s   s e t     n u m b e r _ c o m m u i t y _ s h a r e s = n u m b e r _ c o m m u i t y _ s h a r e s + 1   W H E R E   u s e r _ i d   =   n e w . u s e r _ i d ; 
 
 E N D   / / 
 
 D E L I M I T E R   ; 