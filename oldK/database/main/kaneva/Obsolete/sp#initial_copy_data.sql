-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS `kaneva`.`initial_copy_data`;
delimiter $$
CREATE PROCEDURE `kaneva`.`initial_copy_data`(table_ VARCHAR(100), column_ VARCHAR(100))
BEGIN	

  /* create constants for comparison */
  declare _INSERTS TINYINT default 0;
  declare _UPDATES TINYINT default 1;
  
  declare _tableInHistory TINYINT default 0;
  declare _plannedMaxId BIGINT default 0;
  declare _plannedModDate DATETIME default Now();

  declare _copyHistoryId INTEGER default 0;
  declare _tableName VARCHAR(100) default '';
  declare _projectedIdCopied BIGINT default 0;
  declare _projectedModifiedDate DATETIME default '2000-01-01 00:00:00';
  declare _lastIdCopied BIGINT default 0;
  declare _lastModifiedDate DATETIME default '2000-01-01 00:00:00';


    -- make check to see if the table name entry is already in the data copy history
    SELECT COUNT(table_name) INTO _tableInHistory FROM data_copy_history WHERE table_name = table_;
  
    -- pull back the last data and place into the variables if table name exist
    IF _tableInHistory > 0 THEN
        	
    	SELECT 
    		copy_history_id, table_name, projected_id_copied, projected_modified_date, last_id_copied, last_modified_date
    	INTO 
    		_copyHistoryId, _tableName, _projectedIdCopied, _projectedModifiedDate, _lastIdCopied, _lastModifiedDate
    	FROM
    		data_copy_history
    	WHERE
    		table_name = table_
    	ORDER BY 
    		copy_history_id DESC
    	LIMIT 0,1;
  ELSE 
       -- insert a new copy data record into the table
       INSERT INTO
	  data_copy_history (table_name, projected_id_copied, projected_modified_date, last_id_copied, last_modified_date)
       VALUES (table_, _projectedIdCopied, _projectedModifiedDate, _lastIdCopied, _lastModifiedDate);

       SELECT LAST_INSERT_ID() INTO _copyHistoryId;
       
       -- 
  
  END IF;
  

-- get the highest id at this moment
SET @_stmt := CONCAT("SELECT MAX(",column_, ") INTO @_plannedMaxId FROM ", table_, " WHERE ", column_, " > ", _lastIdCopied, ";");
PREPARE __stmt FROM @_stmt;
EXECUTE __stmt;

IF @_plannedMaxId IS NOT NULL THEN

    -- set the data retrieval size
    IF (@_plannedMaxId - _lastIdCopied) > 10000 THEN
    
    	-- get the max id of the next set of 10000
        SET @_stmt := CONCAT("SELECT ",column_, " FROM ", table_, " WHERE ", column_, " > ", _lastIdCopied, " ORDER BY ", column_, " ASC LIMIT 0,10000");
        SET @_stmt := CONCAT("SELECT MAX(x.",column_, ") INTO @_plannedMaxId FROM (", @_stmt, ") x");
    	PREPARE __stmt FROM @_stmt;
    	EXECUTE __stmt;
    	
    END IF;
        
    -- update the data copy history table with planned id
    UPDATE data_copy_history SET projected_id_copied = @_plannedMaxId, projected_modified_date = _plannedModDate  WHERE copy_history_id = _copyHistoryId;
ELSE
    -- Set the planned id to the last copied id
    SET @_plannedMaxId = _lastIdCopied;
    -- update the data copy history table with planned id
    UPDATE data_copy_history SET projected_id_copied = @_plannedMaxId WHERE copy_history_id = _copyHistoryId;
END IF;

-- return the data set
SET @_stmt := CONCAT("SELECT * FROM ", table_, " WHERE ", column_, " > ", _lastIdCopied, " AND ", column_, " <= ", _projectedIdCopied, ";");
PREPARE __stmt FROM @_stmt;
EXECUTE __stmt;
		
		
		
    DEALLOCATE PREPARE __stmt;
  
END
DELIMITER;
