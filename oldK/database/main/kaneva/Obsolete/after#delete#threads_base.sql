-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

��# D E P R E C A T E D   N O   L O N G E R   U S E D ; 
 
 D E L I M I T E R   / / 
 
 C R E A T E   T R I G G E R   d e l e t e _ t h r e a d s _ b a s e   A F T E R   D E L E T E   o n   t h r e a d s _ b a s e 
 
 F O R   E A C H   R O W 
 
 B E G I N 
 
 	 U P D A T E   t o p i c s   S E T   n u m b e r _ o f _ r e p l i e s   =   ( n u m b e r _ o f _ r e p l i e s   -   1 )   W H E R E   t o p i c _ i d   =   o l d . t o p i c _ i d ; 
 
 E N D   / / 
 
 D E L I M I T E R   ; 