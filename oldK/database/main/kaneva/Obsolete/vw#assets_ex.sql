-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

��# N o   l o n g e r   i n   u s e 
 
 C R E A T E   V I E W   a s s e t s _ e x   A S 
 
 S e l e c t   a . * , 
 
     ` n a m e ` , ` t e a s e r ` , ` b o d y _ t e x t ` , ` o w n e r _ u s e r n a m e ` , ` s h o r t _ d e s c r i p t i o n ` , ` k e y w o r d s ` 
 
 f r o m   a s s e t s   a   I n n e r   J o i n   f t _ a s s e t s   f t a   o n   a . a s s e t _ i d   =   f t a . a s s e t _ i d ; 