-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS `insert_community_views`;

DELIMITER //

CREATE TRIGGER insert_community_views AFTER INSERT on community_views
FOR EACH ROW
BEGIN
  
  if (new.community_id IS NOT NULL) then
    update channel_stats set number_of_views=number_of_views+1 WHERE channel_id = new.community_id;
  end if;
 
  Insert into recent_community_views VALUES(new.view_id,new.user_id, new.community_id, new.created_date, new.anonymous, new.ip_address);
END; 
//

DELIMITER ;