-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TABLE IF EXISTS kaneva.messages_deleted;

--
-- CREATE TABLE: messages_deleted
--
CREATE TABLE IF NOT EXISTS kaneva.messages_deleted
(
	message_id INT NOT NULL DEFAULT 0,
	from_id INT NOT NULL DEFAULT 0,
	from_id_status INT NOT NULL DEFAULT 0 COMMENT 'NEW = 0, READ = 1, TRASH = 2, DELETED = 3',
	to_id INT NOT NULL DEFAULT 0,
	to_id_status INT NOT NULL DEFAULT 0 COMMENT 'NEW = 0, READ = 1, TRASH = 2, DELETED = 3',
	subject VARCHAR(100) NOT NULL,
	message TEXT NOT NULL,
	message_date DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	replied SMALLINT NOT NULL DEFAULT 0,
	type INT NOT NULL DEFAULT 0,
	channel_id INT NULL,
	to_viewable ENUM('N','Y') NOT NULL DEFAULT 'Y',
	from_viewable ENUM('N','Y') NOT NULL DEFAULT 'Y',
	CONSTRAINT PRIMARY KEY (message_id)
)
	ENGINE = InnoDB
	ROW_FORMAT = COMPACT;

--
-- CREATE INDEX: IDX_msg_date
--
CREATE INDEX IDX_msg_date ON kaneva.messages_deleted
(
	message_date
);
