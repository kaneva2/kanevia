-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_stats_converter;
DELIMITER ;;
CREATE PROCEDURE add_stats_converter(IN __asset_id INT, IN __result INT,
		IN __filepath VARCHAR(255), IN __source_size INT, IN __target_size INT,
		IN __processing_time_seconds INT)
BEGIN

INSERT INTO stats_converter (asset_id, start_time, result, filepath, source_size, target_size, processing_time_seconds) 
	VALUES (__asset_id, NOW(), __result, __filepath, __source_size, __target_size, __processing_time_seconds);

END ;;
DELIMITER ;
