-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS kaneva.after_user_balances_update;

DELIMITER //
-- Trigger has the responsibility of updating the summary table for all negative balance changes per user daily
CREATE TRIGGER kaneva.after_user_balances_update AFTER UPDATE ON kaneva.user_balances FOR EACH ROW BEGIN
    declare spend_total decimal(12,3) default 0;
    declare user_count int default 0;

    IF NEW.kei_point_id = 'KPOINT' THEN
        IF NEW.balance < OLD.balance THEN
            SET spend_total = OLD.Balance - NEW.balance;
            SELECT count(user_id) into user_count from wok.summary_spent_credits_by_day_user where user_id=new.user_id and record_date=DATE(NOW());
            IF user_count < 1  then
                INSERT INTO wok.summary_spent_credits_by_day_user
VALUES( new.user_id, date(now()),spend_total);
            ELSE
                UPDATE wok.summary_spent_credits_by_day_user SET spend_amount=spend_amount + spend_total where user_id=new.user_id and record_date=date(now());
            END IF;
        END IF;
    END IF;
END //

DELIMITER ;
