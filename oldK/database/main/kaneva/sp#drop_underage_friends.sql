-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS drop_underage_friends;

DELIMITER ;;
CREATE PROCEDURE `drop_underage_friends`(IN my_user_id INT)
BEGIN

  DROP TABLE IF EXISTS __friends2delete;
  
  Create Table __friends2delete (Select friend_id from friends f
    Inner Join users u on f.friend_id = u.user_id where f.user_id=my_user_id AND u.age < 18);
  
  delete from friends where user_id = my_user_id AND friend_id in (Select friend_id from __friends2delete);
  

  DROP TABLE IF EXISTS __friends2delete;
  
  Create Table __friends2delete (Select f.user_id from friends f
    Inner Join users u on f.user_id = u.user_id where f.friend_id=my_user_id AND u.age < 18);
  
  delete from friends where friend_id = my_user_id AND user_id in (Select user_id from __friends2delete);

  DROP TABLE IF EXISTS __friends2delete;

END
;;
DELIMITER ;