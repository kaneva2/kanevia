-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS topics;

DELIMITER ;;

 CREATE VIEW `kaneva`.`topics` AS SELECT `t`.`topic_id` AS `topic_id`,`t`.`forum_id` AS `forum_id`,`t`.`community_id` AS `community_id`,`t`.`created_user_id` AS `created_user_id`,`t`.`sticky` AS `sticky`,`t`.`important` AS `important`,`t`.`allow_comments` AS `allow_comments`,`t`.`allow_ratings` AS `allow_ratings`,`t`.`number_of_replies` AS `number_of_replies`,`t`.`number_of_views` AS `number_of_views`,`t`.`created_date` AS `created_date`,`t`.`last_reply_date` AS `last_reply_date`,`t`.`last_thread_id` AS `last_thread_id`,`t`.`last_user_id` AS `last_user_id`,`t`.`last_updated_date` AS `last_updated_date`,`t`.`last_updated_user_id` AS `last_updated_user_id`,`t`.`ip_address` AS `ip_address`,`t`.`poll_id` AS `poll_id`,`t`.`status_id` AS `status_id`,`t`.`viewable` AS `viewable`,`ftt`.`created_username` AS `created_username`,`ftt`.`subject` AS `subject`,`ftt`.`body_text` AS `body_text` 
 FROM ( `kaneva`.`topics_base` `t` 
 JOIN `my_kaneva`.`ft_topics` `ftt` on( ( `t`.`topic_id` = `ftt`.`topic_id`) ) ) 
;;
DELIMITER ;