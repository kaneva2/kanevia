-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS associate_school;

DELIMITER ;;
CREATE PROCEDURE `associate_school`(IN __user_id INT, IN __school_id INT, OUT __return_val INT)
BEGIN
  DECLARE __ass_exists INT;
 
  SELECT count(user_id) INTO __ass_exists from kaneva.user_schools WHERE user_id = __user_id AND school_id = __school_id;
  if (__ass_exists > 0) THEN
    SET __return_val = -1; -- Already in the table
  ELSE
    Insert into kaneva.user_schools (user_id, school_id) VALUES (__user_id, __school_id);
    SET __return_val = 0; -- success
  END IF;
END
;;
DELIMITER ;