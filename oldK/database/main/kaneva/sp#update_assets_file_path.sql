-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_file_path;

DELIMITER ;;
CREATE PROCEDURE `update_assets_file_path`(IN __asset_id INT, IN __content_extension VARCHAR(255),
	IN __target_dir VARCHAR(255), IN __file_size BIGINT, IN __media_path  VARCHAR(255))
BEGIN

UPDATE assets 
	SET content_extension = __content_extension,
	target_dir = __target_dir,
	file_size = __file_size, 
        media_path = __media_path 
        WHERE asset_id = __asset_id;

END
;;
DELIMITER ;