-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS cleanup_deleted_assets;

DELIMITER ;;
CREATE PROCEDURE `cleanup_deleted_assets`()
BEGIN

DECLARE	__table_RowCnt		BIGINT;
DECLARE _asset_id		INT;
DECLARE __message_text 		VARCHAR(500);

DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE arch_assets_cursor CURSOR FOR 
			SELECT asset_id
			FROM	assets 	WHERE status_id IN (2,4);
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

SET done = 0;
SET i = 0;

OPEN arch_assets_cursor;
SET __table_RowCnt = (SELECT FOUND_ROWS());
	
SELECT CONCAT('Starting cleanup ... ',CAST(__table_RowCnt AS CHAR),' rows to copy to cleanup. ') INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'cleanup_deleted_assets',__message_text);

FETCH arch_assets_cursor INTO _asset_id;
REPEAT
	SET i=i+1;

	INSERT IGNORE INTO assets_deleted
		SELECT asset_id, owner_id, asset_type_id, IFNULL(target_dir,image_path), 'N' FROM assets WHERE asset_id = _asset_id;

	DELETE FROM assets_stats WHERE asset_id = _asset_id;
	DELETE FROM asset_upload WHERE asset_id = _asset_id;
	DELETE FROM comments WHERE asset_id = _asset_id;
	DELETE FROM assets WHERE asset_id = _asset_id;

	FETCH arch_assets_cursor INTO _asset_id;

UNTIL done END REPEAT;
CLOSE arch_assets_cursor;

SELECT CONCAT('Cleanup completed ..... ',CAST(i AS CHAR), ' rows moved to assets_deleted and removed from assets.') INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'cleanup_deleted_assets',__message_text);

END
;;
DELIMITER ;