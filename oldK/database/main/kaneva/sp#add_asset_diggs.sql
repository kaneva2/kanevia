-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_asset_diggs;

DELIMITER ;;
CREATE PROCEDURE `add_asset_diggs`(IN __user_id INT SIGNED, IN __asset_id INT SIGNED, OUT __digg_id INT SIGNED)
BEGIN
 
  INSERT INTO asset_diggs (user_id, asset_id, created_date) VALUES (__user_id, __asset_id, NOW());

  SELECT LAST_INSERT_ID() INTO __digg_id;

END
;;
DELIMITER ;