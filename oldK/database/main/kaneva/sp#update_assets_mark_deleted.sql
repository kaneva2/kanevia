-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_mark_deleted;

DELIMITER ;;
CREATE PROCEDURE `update_assets_mark_deleted`(IN __asset_id INT, IN __user_updating INT)
BEGIN
/*
asset_status_id  asset_status       
---------------  -------------------
              1  ACTIVE             
              2  DELETED            
              3  NEW                
              4  MARKED_FOR_DELETION
*/
DECLARE _old_status_id INT;
DECLARE _new_status_id INT DEFAULT 4;
DECLARE _old_asset_type_id INT;

SELECT status_id, asset_type_id INTO _old_status_id, _old_asset_type_id FROM assets WHERE asset_id = __asset_id;

UPDATE assets 
	SET status_id = _new_status_id,
	last_updated_user_id = __user_updating,
	published = 'N',
	last_updated_date = NOW()
	WHERE asset_id = __asset_id AND status_id <> 2;

IF ( ((_new_status_id <>1) AND (_new_status_id <> 3) ) AND ((_old_status_id =1) OR (_old_status_id = 3 )) )    THEN
    
		UPDATE summary_assets_by_type SET `count`=`count`-1 WHERE asset_type_id = _old_asset_type_id;
	    
	ELSEIF ( ((_new_status_id =1) AND (_new_status_id = 3) ) AND ((_old_status_id <>1) OR (_old_status_id <> 3 )) )   THEN
        		
        UPDATE summary_assets_by_type SET `count`=`count`+1 WHERE asset_type_id = _old_asset_type_id;
    
	END IF;

END
;;
DELIMITER ;