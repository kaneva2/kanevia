-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS after_user_subscriptions_update;

DELIMITER ;;
CREATE TRIGGER `after_user_subscriptions_update` AFTER UPDATE ON `user_subscriptions` 
    FOR EACH ROW BEGIN
  IF new.end_date <> old.end_date THEN
	INSERT INTO subscription_date_history (user_subscription_id, date_updated, old_enddate, new_enddate) 
		VALUES (new.user_subscription_id, NOW(), old.end_date, new.end_date);
  END IF;
END
;;
DELIMITER ;