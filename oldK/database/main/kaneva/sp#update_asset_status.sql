-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_asset_status;

DELIMITER ;;
CREATE PROCEDURE `update_asset_status`(assetStatusID INT, assetID INT)
BEGIN
 
  START TRANSACTION;
  
    
    /* update the records */
    UPDATE 
    	assets 
    SET 
    	status_id = assetStatusID
    WHERE 
    	asset_id = assetID;
   	
    /*return the number of records affected*/
    SELECT row_count();

  COMMIT;
END
;;
DELIMITER ;