-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_community_preferences;

DELIMITER ;;
CREATE PROCEDURE `add_community_preferences`(_communityId INTEGER SIGNED, _showGender INTEGER SIGNED, _showLocation INTEGER SIGNED, _showAge INTEGER SIGNED)
BEGIN

    
    INSERT INTO community_preferences 
    (
            community_id,
            show_gender,
            show_location,
            show_age
    )
    VALUES 
    (
            _communityId,
            _showGender,
            _showLocation,
            _showAge
    );
            
    
END
;;
DELIMITER ;