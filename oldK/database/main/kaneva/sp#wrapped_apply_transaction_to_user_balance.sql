-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS wrapped_apply_transaction_to_user_balance;

DELIMITER ;;
CREATE PROCEDURE `wrapped_apply_transaction_to_user_balance`( in user_id int, in transaction_amount float, in transaction_type smallint unsigned, in v_kei_point_id varchar(20) )
begin
	declare rc int;
	declare ub float;
	declare ti int;
	call kaneva.apply_transaction_to_user_balance(user_id, transaction_amount, transaction_type, v_kei_point_id, rc, ub,ti);
end
;;
DELIMITER ;