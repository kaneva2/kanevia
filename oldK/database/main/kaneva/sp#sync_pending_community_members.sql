-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS sync_pending_community_members;

DELIMITER ;;
CREATE PROCEDURE `sync_pending_community_members`()
BEGIN
DECLARE __message_text VARCHAR(500);

CREATE TABLE IF NOT EXISTS kaneva._pending_members
(	the_id	INT DEFAULT 0 NOT NULL,
	cs_pending_members INT DEFAULT 0 NOT NULL,
	cm_pending_members INT DEFAULT 0 NOT NULL,
	CONSTRAINT PRIMARY KEY (the_id)
);

CREATE TABLE IF NOT EXISTS kaneva._tmp_comm_mem
(	community_id INT DEFAULT 0 NOT NULL,
	cm_pending_members INT DEFAULT 0 NOT NULL,
	CONSTRAINT PRIMARY KEY (community_id)
);

TRUNCATE TABLE kaneva._pending_members;	
TRUNCATE TABLE kaneva._tmp_comm_mem;	

INSERT INTO kaneva._pending_members 
	SELECT channel_id, number_of_pending_members, 0 	
	FROM channel_stats;

INSERT INTO kaneva._tmp_comm_mem
	SELECT community_id, COUNT(user_id)
		FROM community_members WHERE status_id = 2 GROUP BY community_id;
	
INSERT INTO kaneva._pending_members 
	SELECT community_id, 0, cm_pending_members FROM _tmp_comm_mem t
 	ON DUPLICATE KEY UPDATE cm_pending_members = t.cm_pending_members;

-- select * from kaneva._pending_members where cs_pending_members != cm_pending_members or cs_pending_members < 0;

UPDATE kaneva.channel_stats cs, kaneva._pending_members pm
	SET cs.number_of_pending_members = pm.cm_pending_members
	WHERE cs.channel_id = pm.the_id AND (pm.cs_pending_members != pm.cm_pending_members OR pm.cs_pending_members < 0);

SELECT CONCAT('Sync completed ..... ',CAST(ROW_COUNT() AS CHAR), ' communities updated.') INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'sync_pending_community_members',__message_text);

END
;;
DELIMITER ;