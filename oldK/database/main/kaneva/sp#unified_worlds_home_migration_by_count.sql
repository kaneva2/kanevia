-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS unified_worlds_home_migration_by_count;

DELIMITER ;;
CREATE PROCEDURE `unified_worlds_home_migration_by_count`(IN __updatelimit INT(11))
BEGIN

	DECLARE _home_community_name VARCHAR(100);
	DECLARE _username VARCHAR(22);
	DECLARE _user_id, _wok_player_id, _status_id, _home_community_id, _collision_count, _name_index, _zone_index, _zone_instance_id, _records_processed, _records_left INT(11);
	DECLARE _is_public, _is_adult CHAR(1);
	DECLARE _allow_publishing TINYINT(4);
	DECLARE _allow_member_events TINYINT(3) UNSIGNED;
	DECLARE _over_21_required ENUM('Y','N');
	
	DECLARE _done INT DEFAULT FALSE;
	DECLARE _user_cur CURSOR FOR 
		SELECT DISTINCT u.username, u.user_id, u.wok_player_id, cp.status_id, cp.is_public, cp.is_adult,
		cp.allow_publishing, cp.allow_member_events, cp.over_21_required
		FROM kaneva.users u
		INNER JOIN kaneva.communities_personal cp ON cp.creator_id = u.user_id
		WHERE u.home_community_id IS NULL
		LIMIT __updatelimit;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET _done = TRUE;
	
	SET _records_processed = 0;

	-- Loop through the cursor
	OPEN _user_cur;
	read_loop: LOOP

		FETCH _user_cur INTO _username, _user_id, _wok_player_id, _status_id, _is_public, _is_adult, _allow_publishing, _allow_member_events, _over_21_required;

		IF _done THEN
			LEAVE read_loop;
		END IF;

		-- Begin the transaction
		START TRANSACTION;

			-- Determine safe name for the community
			SET _name_index = NULL;

			name_loop: LOOP
			
				SET _home_community_name = CONCAT(_username, " World ", COALESCE(_name_index, ""));
				SET _collision_count = 0;

				SELECT COUNT(*) INTO _collision_count
				FROM kaneva.communities
				WHERE name_no_spaces = REPLACE(_home_community_name, " ", "")
				AND community_id <> 0;

				IF _collision_count > 0 THEN
					SET _name_index = COALESCE(_name_index, 1) + 1;
				ELSE
					SET _home_community_name = TRIM(_home_community_name);
					LEAVE name_loop;
				END IF;

			END LOOP name_loop;

			-- Create the community
			SET _home_community_id = 0;
			CALL kaneva.add_communities(
				0, REPLACE(_home_community_name, " ", ""),
				NULL, _user_id, _status_id, "", 
				_is_public, _is_adult, "",
				"", "",
				"", "",
				"", _allow_publishing, _allow_member_events,
				0, 5, 0,
				_over_21_required, 'N', _home_community_name,
				CONCAT(_username, " Home World"), "",
				"", _username, "", _home_community_id);

			-- If creation was successful, modify associated tables
			IF _home_community_id > 0 THEN

				CALL kaneva.add_community_members (
					_home_community_id, _user_id, 1, 1, "Y", 0, "Y", "Y", 
					NULL, 0, 0, 0, 0, 0, 0, 0);

				INSERT INTO community_preferences (community_id,show_gender,show_location,show_age)
				VALUES (_home_community_id, 1, 1, 1);

				UPDATE kaneva.users u
				SET u.home_community_id = _home_community_id
				WHERE u.user_id = _user_id;

				-- Raves
				INSERT INTO kaneva.channel_diggs (user_id, channel_id, rave_count, created_date)
				SELECT zr.kaneva_user_id, _home_community_id, zr.rave_count, zr.created_date
				FROM wok.zone_raves zr
				WHERE zr.zone_type = 3 AND zr.zone_instance_id = _wok_player_id;

				UPDATE kaneva.channel_stats cs
				INNER JOIN (
					SELECT channel_id, SUM(IF(rave_count <= 0, 1, rave_count)) AS rave_count
					FROM kaneva.channel_diggs
					WHERE channel_id = _home_community_id
					GROUP BY channel_id
				) AS digg_totals ON cs.channel_id = digg_totals.channel_id
				SET cs.number_of_diggs = digg_totals.rave_count;

				SET _records_processed = _records_processed + 1;
			END IF;

		COMMIT;

	END LOOP read_loop;
	CLOSE _user_cur;

	-- Determine how many records are yet to be processed
	SELECT COUNT(DISTINCT u.user_id) INTO _records_left
	FROM kaneva.users u
	INNER JOIN kaneva.communities_personal cp ON cp.creator_id = u.user_id
	WHERE u.home_community_id IS NULL;

	-- Log the execution
	INSERT INTO kaneva.unified_worlds_home_migration_log (run_date, number_of_communities_processed, number_of_communities_left)
	VALUES (NOW(), _records_processed, _records_left);

	SELECT 'ready' INTO OUTFILE '/tmp/communities_conversion_task.go';

END
;;
DELIMITER ;