-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS asset_group_assets_delete;

DELIMITER ;;
CREATE TRIGGER asset_group_assets_delete AFTER DELETE ON asset_group_assets
FOR EACH ROW
BEGIN
	UPDATE asset_groups SET asset_count = asset_count - 1 WHERE asset_group_id = old.asset_group_id AND asset_count > 0;
END
;;
DELIMITER ;