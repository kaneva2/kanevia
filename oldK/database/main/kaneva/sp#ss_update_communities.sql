-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_communities;

DELIMITER ;;
CREATE PROCEDURE `ss_update_communities`(  )
begin
	replace into kaneva.snapshot_communities
	select count(c.community_id),date(now()) 
	from kaneva.communities_public c
	where c.status_id = 1 AND c.community_id NOT IN (101, 103, 102, 100, 99);
	
	replace into kaneva.snapshot_profiles
	select count(c.community_id),date(now()) 
	from kaneva.personal_communities c;
end
;;
DELIMITER ;