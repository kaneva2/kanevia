-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_owner_name;

DELIMITER ;;
CREATE PROCEDURE `update_assets_owner_name`(IN __asset_id INT, IN __owner_username VARCHAR(22))
BEGIN

UPDATE assets
	SET owner_username = __owner_username
	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;