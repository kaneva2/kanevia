-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_messages_to_id_status;

DELIMITER ;;
CREATE PROCEDURE `update_messages_to_id_status`(IN __message_id INT, IN __to_viewable ENUM('U','R','D'), 
		IN __to_id INT)
BEGIN
DECLARE _OLD_to_viewable ENUM('U','R','D');
DECLARE _type TINYINT;
DECLARE __sql_statement VARCHAR(512);

SELECT to_viewable, `type` INTO _OLD_to_viewable, _type FROM messages WHERE message_id = __message_id;

UPDATE messages 	SET to_viewable = __to_viewable 	WHERE message_id = __message_id;
 
IF (_OLD_to_viewable='U') AND ( __to_viewable != 'U') AND (_type < 4) THEN -- read
	UPDATE kaneva.users_stats SET number_of_new_messages = number_of_new_messages -1 WHERE user_id = __to_id AND number_of_new_messages > 0;
END IF;

IF (_OLD_to_viewable != 'D') AND (__to_viewable = 'D') AND (_type < 4) THEN -- deleted
	UPDATE kaneva.users_stats SET number_inbox_messages = number_inbox_messages - 1 WHERE user_id = __to_id AND number_inbox_messages > 0;
END IF;
IF (_OLD_to_viewable = 'U') AND (__to_viewable != 'U') AND (_type = 5) THEN -- deleted or read
	UPDATE kaneva.users_stats SET number_3DApp_requests = number_3DApp_requests -1 WHERE user_id = __to_id AND number_3DApp_requests > 0;
END IF;
IF (_OLD_to_viewable = 'U') AND (__to_viewable != 'U') AND (_type = 6) THEN -- deleted or read
	UPDATE kaneva.users_stats SET number_community_invites = number_community_invites - 1 WHERE user_id = __to_id AND number_community_invites > 0;
END IF;

END
;;
DELIMITER ;