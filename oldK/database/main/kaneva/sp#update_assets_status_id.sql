-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_status_id;

DELIMITER ;;
CREATE PROCEDURE `update_assets_status_id`(IN __asset_id INT, IN __status_id  INT)
BEGIN

DECLARE _new_published ENUM('Y','N');
DECLARE _publish_status_id INT;
DECLARE _status_id INT;
DECLARE _asset_type_id INT;

SELECT publish_status_id, status_id, asset_type_id INTO _publish_status_id, _status_id, _asset_type_id
	FROM assets WHERE asset_id = __asset_id;

SET _new_published    = IF(( (_publish_status_id=10) & ( (__status_id=1) | (__status_id=3)) ),'Y','N');

UPDATE assets 
	SET status_id = __status_id,
	published = _new_published
        WHERE asset_id = __asset_id;

IF ( ((__status_id <>1) AND (__status_id <> 3) ) AND ((_status_id =1) OR (_status_id = 3 )) )    THEN
	UPDATE summary_assets_by_type SET `count`=`count`-1 WHERE asset_type_id = _asset_type_id;
	
	CALL decrement_communities_assets_counts_by_asset_and_type(__asset_id,_asset_type_id);
	    
ELSEIF ( ((__status_id =1) AND (__status_id = 3) ) AND ((_status_id <>1) OR (_status_id <> 3 )) )    THEN
        		
        UPDATE summary_assets_by_type SET `count`=`count`+1 WHERE asset_type_id = _asset_type_id;
    
END IF;


END
;;
DELIMITER ;