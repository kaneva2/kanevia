-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_asset_restriction;

DELIMITER ;;
CREATE PROCEDURE `update_asset_restriction`(assetRatingID INT, assetID INT, mature VARCHAR(1))
BEGIN
 
  START TRANSACTION;
  
    
    /* update the records */
    UPDATE 
    	assets 
    SET 
    	asset_rating_id = assetRatingID,
    	mature = mature
    WHERE 
    	asset_id = assetID;
   	
    /*return the number of records affected*/
    SELECT row_count();

  COMMIT;
END
;;
DELIMITER ;