-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_messages;

DELIMITER ;;
CREATE PROCEDURE `add_messages`(IN __from_id INT, IN __to_id INT, IN __subject VARCHAR(255), IN __message TEXT, 
	IN __replied INT, IN __type TINYINT, IN __channel_id INT, OUT __message_id INTEGER)
BEGIN
DECLARE __sql_statement VARCHAR(512);
 
  INSERT INTO messages (from_id, from_viewable, to_id, to_viewable, `subject`, `message`, 
		message_date, replied, `type`, channel_id)
  VALUES
	(__from_id, 'S', __to_id, 'U', __subject, __message, 
	NOW(), __replied, __type, __channel_id);

  SELECT LAST_INSERT_ID() INTO __message_id;

  IF  (__type < 4) THEN
		UPDATE kaneva.users_stats SET number_inbox_messages = number_inbox_messages + 1, number_of_new_messages = number_of_new_messages + 1 WHERE user_id = __to_id;
		UPDATE kaneva.users_stats SET number_outbox_messages = number_outbox_messages + 1 WHERE user_id = __from_id;
  END IF;
  IF  (__type = 5) THEN
		UPDATE kaneva.users_stats SET number_3DApp_requests = number_3DApp_requests + 1 WHERE user_id = __to_id;
  END IF;
  IF  (__type = 6) THEN
		UPDATE kaneva.users_stats SET number_community_invites = number_community_invites + 1 WHERE user_id = __to_id;
  END IF;

END
;;
DELIMITER ;