-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_thumbnail_gen;

DELIMITER ;;
CREATE PROCEDURE `update_assets_thumbnail_gen`(IN __asset_id INT)
BEGIN

UPDATE assets 	SET thumbnail_gen = 1 	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;