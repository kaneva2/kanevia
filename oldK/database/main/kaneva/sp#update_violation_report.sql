-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_violation_report;

DELIMITER ;;
CREATE PROCEDURE `update_violation_report`(violationID INT UNSIGNED, assetID INT UNSIGNED, wokItemID INT UNSIGNED, ownerID INT UNSIGNED, reporterID INT UNSIGNED, violationTypeID INT UNSIGNED, violationActionTypeID INT UNSIGNED, 
memberComments TEXT, csrNotes TEXT, reportDate DATETIME, lastUpdated DATETIME, modifierID  INT UNSIGNED)
BEGIN
 
 DECLARE _rows_affected int;
 
  START TRANSACTION;
  
    
    /* update the records */
    UPDATE 
    	violation_reports 
    SET 
    	asset_id = assetID, 
    	wok_item_id = wokItemID, 
    	owner_id = ownerID, 
    	reporter_id = reporterID, 
    	violation_type_id = violationTypeID, 
    	violation_action_type_id = violationActionTypeID, 
    	member_comments = memberComments, 
    	csr_notes = csrNotes, 
    	report_date = reportDate, 
    	last_updated = lastUpdated, 
    	modifiers_id = modifierID 
    WHERE 
    	violation_id = violationID;

    /*get the number of records affected*/
    SELECT row_count() INTO _rows_affected ;
    
    /* check auto violations */
    CALL autoprocess_violation(assetID, wokItemID, modifierID);

    /*return the number of records affected*/
    SELECT _rows_affected;
    
  COMMIT;
END
;;
DELIMITER ;