-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS purge_friend_transactions;

DELIMITER ;;
CREATE PROCEDURE `purge_friend_transactions`()
BEGIN
	DECLARE _hold_out_id INT;

	-- purge all friend transactions that are older than 7 days, but make sure to leave at least one row behind
	SELECT max(txn_id) INTO _hold_out_id FROM friend_transactions;
	DELETE FROM friend_transactions WHERE txn_time < ADDDATE(NOW(), INTERVAL -7 DAY) AND txn_id <> _hold_out_id;
END
;;
DELIMITER ;