-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS interest_exists;

DELIMITER ;;
CREATE FUNCTION `interest_exists`(__ic_id INT , __interest varchar(255)) RETURNS int(11)
    READS SQL DATA
BEGIN
  
  DECLARE __interest_id INTEGER;
  
  Select interest_id into __interest_id from kaneva.interests WHERE ic_id = __ic_id AND interest = __interest;
  if (__interest_id IS NULL) THEN
    return 0;
  END IF;
  return __interest_id;
  
END
;;
DELIMITER ;