-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS invites_insert;

DELIMITER ;;
CREATE TRIGGER `invites_insert` AFTER INSERT ON `invites` FOR EACH ROW BEGIN
update users_stats set number_invites_sent = number_invites_sent + 1 WHERE user_id = new.user_id;
END
;;
DELIMITER ;