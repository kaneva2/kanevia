-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS remove_friend_request;

DELIMITER ;;
CREATE PROCEDURE `remove_friend_request`(IN __user_id INT, IN __friend_id INT)
BEGIN
DECLARE __sql_statement VARCHAR(512);

-- INSERT INTO kaneva.friend_transactions( txn_type, user_id, friend_id, txn_time )
--	VALUES ('DEC', __user_id, __friend_id, NOW() );

DELETE FROM kaneva.friend_requests WHERE user_id = __user_id AND friend_id = __friend_id;
SELECT ROW_COUNT() INTO @rowcnt;

IF @rowcnt = 1 THEN
	UPDATE kaneva.users_stats SET number_of_pending  = number_of_pending - 1 WHERE user_id = __user_id AND number_of_pending > 0;
	UPDATE kaneva.users_stats SET number_of_requests = number_of_requests - 1 WHERE user_id = __friend_id AND number_of_requests > 0;
END IF;

END
;;
DELIMITER ;