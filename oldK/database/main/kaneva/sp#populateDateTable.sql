-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS populateDateTable;

DELIMITER ;;
CREATE PROCEDURE `populateDateTable`()
BEGIN
  declare i integer;
  set i = 0;
  while i <= 9495 DO
    insert into kaneva.dates values( date_add('2005/01/01 00:00:00',interval i day));
    set i = i + 1;
  end while;
END
;;
DELIMITER ;