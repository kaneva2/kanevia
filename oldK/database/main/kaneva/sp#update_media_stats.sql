-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_media_stats;

DELIMITER ;;
CREATE PROCEDURE `update_media_stats`()
begin
	/* ** */
	drop table if exists __summary_assets_by_type;
	CREATE TABLE __summary_assets_by_type (
	asset_type_id INTEGER UNSIGNED NOT NULL,
	count INTEGER UNSIGNED NOT NULL,
	PRIMARY KEY(asset_type_id)
	)
	ENGINE = InnoDB;

	/* Initial Population */
	Insert into __summary_assets_by_type
	Select asset_type_id, count(*) as c from assets WHERE permission NOT IN (2) AND publish_status_id = 10 AND status_id IN (1,3) group by asset_type_id order by asset_id;

	/* ** */
	DROP TABLE IF EXISTS kaneva.summary_assets_by_type;
	ALTER TABLE kaneva.__summary_assets_by_type RENAME TO kaneva.summary_assets_by_type;
end
;;
DELIMITER ;