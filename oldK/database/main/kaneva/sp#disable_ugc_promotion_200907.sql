-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS disable_ugc_promotion_200907;

DELIMITER ;;
CREATE PROCEDURE `disable_ugc_promotion_200907`()
BEGIN

	
	
	

	
	UPDATE kaneva.web_options wo INNER JOIN kaneva.web_options wo2 
		ON wo.option_group='UGC' AND wo2.option_group='UGC200907' AND wo.option_key=wo2.option_key AND wo.role=wo2.role
		SET wo.option_value = wo2.option_value;
	
	
	DELETE FROM kaneva.web_options WHERE option_group='UGC200907';

	
	
	

	
	UPDATE wok.ugc_price_tiers upt INNER JOIN wok.ugc_price_tiers upt2
		ON upt.kaneva_role_value=upt2.kaneva_role_value AND upt.ugc_usage_id=101 AND upt2.ugc_usage_id=200907 AND upt.tier_id=upt2.tier_id
		SET upt.base_price = upt2.base_price, upt.max_price = upt2.max_price
		WHERE upt.kaneva_role_value=2 AND upt.ugc_usage_id=101 AND upt.tier_id in (1120, 1121, 1122, 1123);
	
	
	DELETE FROM wok.ugc_price_tiers 
		WHERE kaneva_role_value=2 AND ugc_usage_id=200907;

END
;;
DELIMITER ;