-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS before_update_promotional_offer_items;

DELIMITER ;;
CREATE TRIGGER `before_update_promotional_offer_items` BEFORE UPDATE ON `promotional_offer_items` FOR EACH ROW BEGIN
    INSERT into kaneva.audit_promotional_offer_items 
    (
    	wok_item_id, promotion_id, gift_card_id, community_id, 
    	item_description, quantity, market_price, 
        gender, promotion_offer_id, alternate_description, 
	modifiers_id, date_modified, operation
    )
    VALUES
    (
    	
    	old.wok_item_id, old.promotion_id, old.gift_card_id, old.community_id, 
    	old.item_description, old.quantity, old.market_price, 
        old.gender, old.promotion_offer_id, old.alternate_description, 
	old.modifiers_id, Now(), 'update'
    ) ;   
   
END
;;
DELIMITER ;