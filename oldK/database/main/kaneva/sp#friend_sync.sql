-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS friend_sync;

DELIMITER ;;
CREATE PROCEDURE `friend_sync`()
BEGIN

DECLARE __message_text VARCHAR(500);
SELECT NOW() INTO @start_time;

DROP TABLE IF EXISTS _temp_user_friends;
DROP TABLE IF EXISTS _temp_user_friends2;

CREATE TABLE _temp_user_friends (
  user_id INT(11) NOT NULL,
  friends INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
  ) ENGINE=INNODB;

CREATE TABLE _temp_user_friends2 (
  user_id INT(11) NOT NULL,
  friends INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
  ) ENGINE=INNODB;

INSERT INTO _temp_user_friends (user_id, friends)
	SELECT u.user_id, COALESCE(COUNT(f.friend_id),0)
		FROM users u
		INNER JOIN communities_personal com ON u.user_id = com.creator_id
		INNER JOIN channel_stats cs ON com.community_id = cs.channel_id
		INNER JOIN friends f ON u.user_id = f.friend_id
		WHERE u.active = 'Y' 
		GROUP BY u.user_id;

INSERT INTO _temp_user_friends2 (user_id, friends)
	SELECT u.user_id, COALESCE(COUNT(f.user_id),0)
		FROM users u
		INNER JOIN communities_personal com ON u.user_id = com.creator_id
		INNER JOIN channel_stats cs ON com.community_id = cs.channel_id
		INNER JOIN friends f ON u.user_id = f.user_id
		WHERE u.active = 'Y' 
		GROUP BY u.user_id;
	
UPDATE users_stats us
	LEFT OUTER JOIN _temp_user_friends t1 ON us.user_id = t1.user_id
	LEFT OUTER JOIN _temp_user_friends2 t2 ON us.user_id = t2.user_id
	SET number_of_friends = COALESCE(t1.friends,0)+COALESCE(t2.friends,0)
	WHERE number_of_friends != COALESCE(t1.friends,0)+COALESCE(t2.friends,0)
	AND COALESCE(t1.friends,0)+COALESCE(t2.friends,0) > 0;

SELECT CONCAT('Friend sync completed: ',CAST(ROW_COUNT() AS CHAR), ' user_stats rows updated in ',CAST(TIME_TO_SEC(TIMEDIFF(NOW(),@start_time)) AS CHAR), ' seconds.') INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'friend_sync',__message_text);

END
;;
DELIMITER ;