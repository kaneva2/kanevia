-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_new_violation_report;

DELIMITER ;;
CREATE PROCEDURE `add_new_violation_report`(assetID INT UNSIGNED, wokItemID INT UNSIGNED, ownerID INT UNSIGNED, 
			reporterID INT UNSIGNED, violationTypeID INT UNSIGNED, violationActionTypeID INT UNSIGNED, 
			memberComments TEXT, csrNotes TEXT, reportDate DATETIME, lastUpdated DATETIME, modifierID  INT UNSIGNED)
BEGIN
 
DECLARE _duplicate INT;
DECLARE _newViolationId INT;

DECLARE duplicate_key CONDITION FOR 1062;

DECLARE CONTINUE HANDLER FOR duplicate_key
	BEGIN
		SET _duplicate = 1;
	END;

SET _newViolationID = 0;
SET _duplicate = 0;

START TRANSACTION;
  
/* update the records */
INSERT INTO 
	violation_reports (asset_id, wok_item_id, owner_id, reporter_id, violation_type_id, violation_action_type_id, 
	member_comments, csr_notes, report_date, last_updated, modifiers_id) 
VALUES 
	(assetID, wokItemID, ownerID, reporterID, violationTypeID, violationActionTypeID, memberComments, 
	csrNotes, reportDate, lastUpdated, modifierID);

IF _duplicate = 0 THEN
    /*get the id of the new row*/
    SELECT LAST_INSERT_ID() INTO _newViolationId;  
           
    /* check auto violations */
    CALL autoprocess_violation(assetID, wokItemID, modifierID);
ELSE
	SET _newViolationId = -1;
END IF;

SELECT _newViolationId;    
COMMIT;

END
;;
DELIMITER ;