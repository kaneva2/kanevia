-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_community_notifications;

DELIMITER ;;
CREATE PROCEDURE `update_community_notifications`(_communityId INTEGER SIGNED, _userId INTEGER SIGNED, _notifications INTEGER SIGNED)
BEGIN


  START TRANSACTION;
	
    /*update table*/
    UPDATE 
    	community_members 
    SET
            notifications = _notifications           
    WHERE
    	community_id = _communityId AND
    	user_id = _userId;

   
    SELECT row_count();

  COMMIT;

END
;;
DELIMITER ;