-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS sync_mail_new_inbox_counts;

DELIMITER ;;
CREATE PROCEDURE `sync_mail_new_inbox_counts`(__cutoff_time DATETIME)
BEGIN
DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE _user_id INT;
DECLARE _number_of_new_inbox INT;
DECLARE _number_of_new_inbox_C INT;
DECLARE _username VARCHAR(22);
DECLARE __message_text VARCHAR(500);
DECLARE arch_cursor CURSOR FOR 
			SELECT us.user_id, us.number_of_new_messages, username 
			FROM kaneva.users_stats us
			INNER JOIN kaneva.users u ON us.user_id = u.user_id
			WHERE number_of_new_messages > 1000
			OR last_login > ADDDATE(NOW(), INTERVAL -6 HOUR)
			ORDER BY u.user_id;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;
SET i = 0;
OPEN arch_cursor;
REPEAT
    FETCH arch_cursor INTO _user_id, _number_of_new_inbox_C, _username;
    IF NOT done THEN
	SET i=i+1;
	SELECT COALESCE(COUNT(*),0) INTO _number_of_new_inbox
		FROM kaneva.messages 
		WHERE to_id = _user_id AND to_viewable = 'U'  AND `type` IN (0,1);
	IF _number_of_new_inbox <> _number_of_new_inbox_C THEN
		-- SELECT _user_id, _number_of_inbox_C, _number_of_inbox;
		UPDATE users_stats SET number_of_new_messages = _number_of_new_inbox WHERE user_id = _user_id;
		SELECT  CONCAT('Synced user_id: ',CAST(_user_id AS CHAR),': ',_username,'. Old count: ',CAST(_number_of_new_inbox_C AS CHAR),'; new count: ',CAST(_number_of_new_inbox AS CHAR)) INTO __message_text;
		CALL shard_info.add_maintenance_log ('kaneva','sync_mail_new_inbox_counts',__message_text);
	END IF;
	IF NOW() >= __cutoff_time THEN 
		SET done = 1; 
	END IF;
    END IF;	
UNTIL done END REPEAT;
CLOSE arch_cursor;
END
;;
DELIMITER ;