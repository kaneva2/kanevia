-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_amount_status;

DELIMITER ;;
CREATE PROCEDURE `update_assets_amount_status`(IN __asset_id INT, IN __amount DECIMAL(12,2),
		IN __status_id INT)
BEGIN
DECLARE _old_status_id INT;
DECLARE _new_published ENUM('Y','N');
DECLARE _publish_status_id INT;

SELECT status_id, publish_status_id INTO _old_status_id, _publish_status_id 
	FROM assets WHERE asset_id = __asset_id;

SET _new_published = IF(( _publish_status_id=10 & ( __status_id=1 | __status_id=3)),'Y','N');

UPDATE assets 
	SET amount = __amount,
        status_id = __status_id,
        published = _new_published
        WHERE asset_id = __asset_id;

END
;;
DELIMITER ;