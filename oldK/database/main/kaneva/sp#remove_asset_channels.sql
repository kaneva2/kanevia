-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS remove_asset_channels;

DELIMITER ;;
CREATE PROCEDURE `remove_asset_channels`(IN __channel_id INT, IN __asset_id INT)
BEGIN

DECLARE __sql_statement VARCHAR(512);
DECLARE _permission INT;
DECLARE _asset_type_id INT;
DECLARE _asset_owner_id INT;

DECLARE _ch_creator_id INT;
DECLARE _is_personal INT;
 
SELECT permission, asset_type_id, owner_id INTO _permission, _asset_type_id, _asset_owner_id  FROM assets WHERE asset_id = __asset_id;
SELECT creator_id, is_personal INTO _ch_creator_id, _is_personal FROM communities WHERE community_id = __channel_id;

DELETE FROM asset_channels WHERE channel_id = __channel_id AND asset_id = __asset_id;

DELETE FROM summary_asset_channels WHERE asset_id = __asset_id AND community_id = __channel_id;
 
IF (_asset_owner_id <> _ch_creator_id) THEN
   UPDATE communities_assets_counts SET shares = shares-1 WHERE community_id=__channel_id AND shares > 0;
END IF;

CALL decrement_communities_assets_counts_by_type(__channel_id, _asset_type_id);

UPDATE assets_stats SET number_of_channels = number_of_channels-1 WHERE asset_id = __asset_id AND number_of_channels > 0;
      
END
;;
DELIMITER ;