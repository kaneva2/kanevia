-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_community_preferences;

DELIMITER ;;
CREATE PROCEDURE `update_community_preferences`(_communityPreferenceId INTEGER SIGNED, _communityId INTEGER SIGNED, _showGender INTEGER SIGNED, _showLocation INTEGER SIGNED, _showAge INTEGER SIGNED)
BEGIN


  START TRANSACTION;
	
    /*update table*/
	UPDATE 
		community_preferences 
	SET
		community_id = _communityId,
		show_gender = _showGender,
		show_location = _showLocation,
		show_age = _showAge
	WHERE
		community_preference_id = _communityPreferenceId;
		
	
  COMMIT;

END
;;
DELIMITER ;