-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_violation_report_by_itemid;

DELIMITER ;;
CREATE PROCEDURE `delete_violation_report_by_itemid`(itemID INT UNSIGNED)
BEGIN
 
  START TRANSACTION;
    
    /* delete the records */
    DELETE FROM violation_reports WHERE wok_item_id = itemID;

    /*return the number of records affected*/
    SELECT row_count();

  COMMIT;
END
;;
DELIMITER ;