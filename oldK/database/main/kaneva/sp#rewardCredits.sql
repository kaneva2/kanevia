-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS rewardCredits;

DELIMITER ;;
CREATE PROCEDURE `rewardCredits`( IN __transaction_amount FLOAT, IN __v_kei_point_id VARCHAR(20), 
					IN __start_date DATETIME, IN __end_date DATETIME )
BEGIN
	DECLARE __user_id,return_code,tranId INT;
	DECLARE __reward_event_id INT UNSIGNED DEFAULT 1;
	DECLARE balance FLOAT;
	DECLARE done INT DEFAULT 0;
	DECLARE __message_text VARCHAR(500);

	DECLARE curReward CURSOR FOR SELECT DISTINCT user_id FROM
    (SELECT DISTINCT(user_id) AS user_id 
			FROM developer.login_log  
			WHERE created_date BETWEEN DATE(__start_date) AND __end_date
			AND logout_date BETWEEN __start_date AND DATE_ADD(__end_date, INTERVAL 12 HOUR)
			)X;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	SELECT event_name INTO @event_name FROM reward_events WHERE reward_event_id = __reward_event_id;
	SELECT  CONCAT('Reward event of ',CAST(@event_name AS CHAR),' awarding: ',CAST(__transaction_amount AS CHAR),
		' ',CAST(__v_kei_point_id AS CHAR),'; for starting: ',CAST(__start_date AS CHAR),' and ending: ',CAST(__end_date AS CHAR)
		) INTO __message_text;

	CALL shard_info.add_maintenance_log (DATABASE(),'rewardCredits',__message_text); 
	OPEN curReward;

	REPEAT
		FETCH curReward INTO __user_id;
		IF NOT done AND __user_id IS NOT NULL THEN
			SELECT 'user_id :',__user_id;
			CALL kaneva.apply_transaction_to_user_balance( __user_id, __transaction_amount, 23, __v_kei_point_id, return_code, balance, tranId);
			INSERT INTO kaneva.reward_log (user_id,date_created,amount,kei_point_id,transaction_type, return_code,reward_event_id) 
							VALUES(__user_id, NOW(), __transaction_amount, __v_kei_point_id, 23, return_code,__reward_event_id);
		END IF;	
	UNTIL done END REPEAT;
	CLOSE curReward;
END
;;
DELIMITER ;