-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS kaneva_playlist_counts;

DELIMITER ;;

 CREATE VIEW `kaneva`.`kaneva_playlist_counts` AS SELECT SQL_NO_CACHE `kaneva`.`featured_assets`.`playlist_type` AS `asset_group_id`,( 
 CASE `kaneva`.`featured_assets`.`playlist_type` 
 WHEN -( 1)  
 THEN _utf8'Cool items' 
 WHEN -( 4)  
 THEN _utf8'Home items' 
 WHEN -( 5)  
 THEN _utf8'Member items' 
 WHEN -( 6)  
 THEN _utf8'Broadband items' 
 WHEN -( 7)  
 THEN _utf8'Media items' 
 ELSE _utf8'Unknown' 
 END)  AS `name`,count( 0)  AS `asset_count` 
 FROM `kaneva`.`featured_assets` 
 WHERE ( ( `kaneva`.`featured_assets`.`start_datetime` < now( ) )  
 AND ( `kaneva`.`featured_assets`.`end_datetime` > now( ) ) )  
 GROUP BY 1,2
;;
DELIMITER ;