-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS remove_channels_from_asset;

DELIMITER ;;
CREATE PROCEDURE `remove_channels_from_asset`(IN __asset_id INT)
BEGIN

DECLARE _channel_id INT;
DECLARE done INT DEFAULT 0;
DECLARE asset_cursor CURSOR FOR 
			SELECT channel_id FROM asset_channels WHERE asset_id = __asset_id;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

SET done = 0;

OPEN asset_cursor;

REPEAT
	FETCH asset_cursor INTO _channel_id;

	IF _channel_id IS NOT NULL THEN
		CALL remove_asset_channels(_channel_id, __asset_id);
	END IF;
	FETCH asset_cursor INTO _channel_id;
UNTIL done END REPEAT;
CLOSE asset_cursor;

END
;;
DELIMITER ;