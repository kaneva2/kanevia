-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS archive_events;

DELIMITER ;;
CREATE PROCEDURE `archive_events`(__archive_before_date	DATETIME, __archive_cutoff_time DATETIME)
BEGIN
DECLARE	_event_id 	BIGINT;
DECLARE __sql_statement VARCHAR(512);
DECLARE	__table_RowCnt		INT;
DECLARE	__OrigRowCnt		INT;
DECLARE __message_text VARCHAR(500);
DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE arch_event_cursor CURSOR FOR 
			SELECT event_id
			FROM	`events`
			WHERE end_time < __archive_before_date;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;
SET i = 0;
OPEN arch_event_cursor;
SET __table_RowCnt = (SELECT FOUND_ROWS());
SELECT CONCAT('Starting archive ... ',CAST(__table_RowCnt AS CHAR),' rows to archive. Archive before date: ',
					CAST(__archive_before_date AS CHAR),'; Cutoff time: ',
					CAST(__archive_cutoff_time AS CHAR)) INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'archive_events',__message_text);
REPEAT
    FETCH arch_event_cursor INTO _event_id;
	
	START TRANSACTION;
	SET i=i+1;
	IF _event_id IS NOT NULL THEN
	
		insert ignore into kaneva.events_archive (event_id, community_id, user_id, title, details, location, 
				zone_index, zone_instance_id, start_time, end_time, type_id, premium, privacy, 
				time_zone_id, created_date, last_update, is_AP_event)
			select 	event_id, community_id, user_id, title, details, location, 
				zone_index, zone_instance_id, start_time, end_time, type_id, premium, privacy, 
				time_zone_id, created_date, last_update, is_AP_event
			from kaneva.events 
			where event_id = _event_id;
		DELETE FROM `events` WHERE event_id = _event_id;
	END IF;
	COMMIT;
	IF NOW() >= __archive_cutoff_time THEN 
		SET done = 1; 
	END IF;
		
UNTIL done END REPEAT;
CLOSE arch_event_cursor;
SELECT CONCAT('Archive completed ..... ',CAST(i AS CHAR), ' rows moved to events_archive.') INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'archive_events',__message_text);
END
;;
DELIMITER ;