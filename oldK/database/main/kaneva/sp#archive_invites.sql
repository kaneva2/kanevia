-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS archive_invites;

DELIMITER ;;
CREATE PROCEDURE `archive_invites`(__archive_before_date	DATETIME, __archive_cutoff_time DATETIME)
BEGIN
DECLARE	_invite_id 	BIGINT;
DECLARE __sql_statement VARCHAR(512);
DECLARE	__table_RowCnt		INT;
DECLARE	__OrigRowCnt		INT;
DECLARE __message_text VARCHAR(500);
DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE arch_invite_cursor CURSOR FOR 
			SELECT invite_id
			FROM	invites
			WHERE invited_date < __archive_before_date;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;
SET i = 0;
OPEN arch_invite_cursor;
SET __table_RowCnt = (SELECT FOUND_ROWS());
SELECT CONCAT('Starting archive ... ',CAST(__table_RowCnt AS CHAR),' rows to archive. Archive before date: ',
					CAST(__archive_before_date AS CHAR),'; Cutoff time: ',
					CAST(__archive_cutoff_time AS CHAR)) INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'archive_invites',__message_text);
-- Loop while anything to delete.
REPEAT
    FETCH arch_invite_cursor INTO _invite_id;
	
	START TRANSACTION;
	SET i=i+1;
	IF _invite_id IS NOT NULL THEN
		INSERT IGNORE INTO kaneva.invites_archive (invite_id, user_id, email, email_hash, to_name, key_value, invite_status_id, 
				invited_date, points_awarded, kei_point_id_awarded, reinvite_date, channel_id, invited_user_id, 
				invites_source_id, attempts, opened_invite, clicked_accept_link)
			SELECT 	invite_id, user_id, email, email_hash, to_name, key_value, invite_status_id, invited_date, 
				points_awarded, kei_point_id_awarded, reinvite_date, channel_id, invited_user_id, COALESCE(invites_source_id,0), 
				attempts, opened_invite, clicked_accept_link	 
			FROM kaneva.invites WHERE invite_id = _invite_id;
			
		DELETE FROM kaneva.invites WHERE invite_id = _invite_id;
	END IF;
	COMMIT;
	IF NOW() >= __archive_cutoff_time THEN 
		SET done = 1; 
	END IF;
		
UNTIL done END REPEAT;
CLOSE arch_invite_cursor;
SELECT CONCAT('Archive completed ..... ',CAST(i AS CHAR), ' rows moved to invites_archive.') INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'archive_invites',__message_text);
END
;;
DELIMITER ;