-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS sync_both_friend_requests;

DELIMITER ;;
CREATE PROCEDURE `sync_both_friend_requests`(__cutoff_time DATETIME)
BEGIN
DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE _user_id INT;
DECLARE _number_of_requests INT;
DECLARE _number_of_requests_C INT;
DECLARE _number_of_pending INT;
DECLARE _number_of_pending_C INT;
DECLARE _username VARCHAR(22);
DECLARE __message_text VARCHAR(500);
DECLARE arch_cursor CURSOR FOR 
		SELECT us.user_id, us.number_of_pending, number_of_requests, username 
			FROM kaneva.users_stats us
			INNER JOIN kaneva.users u ON us.user_id = u.user_id
			WHERE us.number_of_pending > 35000
			OR u.last_login > ADDDATE(NOW(), INTERVAL -6 HOUR)
	UNION
		SELECT us.user_id, us.number_of_pending, number_of_requests, username 
			FROM kaneva.users_stats us
			INNER JOIN kaneva.users u ON us.user_id = u.user_id
			WHERE us.number_of_requests > 35000;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;
SET i = 0;
OPEN arch_cursor;
REPEAT
    FETCH arch_cursor INTO _user_id, _number_of_pending_C, _number_of_requests_C, _username;
    IF NOT done THEN
	-- select 'top of loop', done, i, __cutoff_time, _user_id, _username;
	SET i=i+1;
	SELECT COALESCE(COUNT(*),0) INTO _number_of_pending
		FROM kaneva.friend_requests
		WHERE user_id = _user_id;
	SELECT COALESCE(COUNT(*),0) INTO _number_of_requests
		FROM kaneva.friend_requests
		WHERE friend_id = _user_id;
	IF _number_of_pending <> _number_of_pending_C THEN
		-- SELECT _user_id, _username, _number_of_pending, _number_of_pending_C;
		UPDATE users_stats SET number_of_pending = _number_of_pending WHERE user_id = _user_id;
		SELECT  CONCAT('Synced user_id: ',CAST(_user_id AS CHAR),': ',_username,'. Old count: ',CAST(_number_of_pending_C AS CHAR),
		 		'; new count: ',CAST(_number_of_pending AS CHAR)) INTO __message_text;
		CALL shard_info.add_maintenance_log ('kaneva','sync_both_friend_requests.pend',__message_text);
	END IF;
	IF _number_of_requests <> _number_of_requests_C THEN
		-- SELECT _user_id, _username, _number_of_requests, _number_of_requests_C;
		UPDATE users_stats SET number_of_requests = _number_of_requests WHERE user_id = _user_id;
		SELECT  CONCAT('Synced user_id: ',CAST(_user_id AS CHAR),': ',_username,'. Old count: ',CAST(_number_of_requests_C AS CHAR),
				'; new count: ',CAST(_number_of_requests AS CHAR)) INTO __message_text;
		CALL shard_info.add_maintenance_log ('kaneva','sync_both_friend_requests.req',__message_text);
	END IF;
	IF NOW() >= __cutoff_time THEN 
		SET done = 1; 
	END IF;
    END IF;	
UNTIL done END REPEAT;
CLOSE arch_cursor;
END
;;
DELIMITER ;