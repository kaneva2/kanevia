-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_rating_id;

DELIMITER ;;
CREATE PROCEDURE `update_assets_rating_id`(IN __asset_id INT, IN __asset_rating_id  INT)
BEGIN

DECLARE _new_mature ENUM('Y','N');

SET _new_mature       = IF(( (__asset_rating_id=3) | (__asset_rating_id=6) | (__asset_rating_id=9) ),'Y','N');

UPDATE assets 
	SET asset_rating_id = __asset_rating_id,
	mature = _new_mature
        WHERE asset_id = __asset_id;

END
;;
DELIMITER ;