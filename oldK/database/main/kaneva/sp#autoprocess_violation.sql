-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS autoprocess_violation;

DELIMITER ;;
CREATE PROCEDURE `autoprocess_violation`(assetID INT UNSIGNED, wokItemID INT UNSIGNED, modifierID  INT UNSIGNED)
BEGIN
 
   DECLARE done INT DEFAULT 0;
   DECLARE _count INT DEFAULT 0;
   DECLARE _offenseCount INT UNSIGNED;
   DECLARE _violationAction INT UNSIGNED;
   DECLARE _assetID INT UNSIGNED;
   DECLARE _violationTriggerCount INT UNSIGNED;
   DECLARE _wokItemID INT UNSIGNED;
   DECLARE _REMOVE INT DEFAULT 4;
   DECLARE _ADMIN_OK INT DEFAULT 5;

   
   DECLARE asset_cursor CURSOR FOR  SELECT COUNT(vr.violation_id) as num_offenses, va.violation_action_type_id, vr.asset_id, va.violation_trigger_count
 				    FROM violation_reports vr INNER JOIN violation_actions va ON vr.violation_type_id = va.violation_type_id 
   				    WHERE vr.asset_id <> 0 AND vr.asset_id = assetID AND vr.violation_action_type_id <> _REMOVE AND vr.violation_action_type_id <> _ADMIN_OK 
   				    GROUP BY vr.asset_id, vr.violation_type_id HAVING num_offenses >= va.violation_trigger_count;
  
   DECLARE wokItem_cursor CURSOR FOR SELECT COUNT(vr.violation_id) as num_offenses, va.violation_action_type_id, vr.wok_item_id, va.violation_trigger_count
 				    FROM violation_reports vr INNER JOIN violation_actions va ON vr.violation_type_id = va.violation_type_id 
   				    WHERE vr.wok_item_id <> 0 AND vr.wok_item_id = wokItemID AND vr.violation_action_type_id <> _REMOVE AND vr.violation_action_type_id <> _ADMIN_OK 
   				    GROUP BY vr.wok_item_id, vr.violation_type_id HAVING num_offenses >= va.violation_trigger_count;
   				    
   DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
 
   SET done = 0;
 
  START TRANSACTION;
    
    /*if it is an asset follow this logic */
    IF (assetID > 0) THEN
    
       OPEN asset_cursor;
   
	   REPEAT
	     FETCH asset_cursor INTO _offenseCount, _violationAction, _assetID, _violationTriggerCount;

		IF done = 0 THEN
			/*begin repetative processing*/
			CALL process_asset_violation(_assetID, _violationAction, modifierID);
		END IF;
	
	   UNTIL done END REPEAT;
	   
	CLOSE asset_cursor;
  
    /*if it is a WOK items follow this logic*/
    ELSEIF(wokItemID > 0) THEN
    
       OPEN wokItem_cursor;
   
	   REPEAT
	     FETCH wokItem_cursor INTO _offenseCount, _violationAction, _wokItemID, _violationTriggerCount;

		IF done = 0 THEN
			/*begin repetative processing*/
			CALL process_wokItem_violation(_wokItemID, _violationAction, modifierID );
		END IF;
	
	   UNTIL done END REPEAT;
	   
	CLOSE wokItem_cursor;    
    END IF; 

  COMMIT;
END
;;
DELIMITER ;