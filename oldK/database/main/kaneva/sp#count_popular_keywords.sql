-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS count_popular_keywords;

DELIMITER ;;
CREATE PROCEDURE `count_popular_keywords`()
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE cur_position INT DEFAULT 1 ;
    DECLARE remainder TEXT;
    DECLARE cur_string VARCHAR(1000);
    DECLARE delimiter_length TINYINT UNSIGNED;
    DECLARE input TEXT;
    DECLARE asset_type INT DEFAULT 0;
    DECLARE xc CURSOR for Select asset_type_id,keyword from __keywords_temp;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    
    DROP TABLE IF EXISTS __keywords_temp;
    DROP TABLE IF EXISTS __individual_keywords_temp;
    DROP TABLE IF EXISTS __popular_keywords;
    CREATE TABLE __keywords_temp (
     `keyword_id` INT NOT NULL AUTO_INCREMENT,
     `asset_type_id` int not null,
     `keyword` text,
    PRIMARY KEY (`keyword_id`)
    )
    CHARACTER SET utf8;
    CREATE TABLE __individual_keywords_temp (
     `keyword_id` INT NOT NULL AUTO_INCREMENT,
     `asset_type_id` int not null,
     `keyword` text,
    PRIMARY KEY (`keyword_id`)
    );
  
  #To keep consistent with the popular_keywords table that already exists
  CREATE TABLE  __popular_keywords (
    `keyword` varchar(100) NOT NULL default '',
    `total_count` int(11) NOT NULL default '0',
    `asset_type_id` int(11) NOT NULL default '-1',
    KEY `key_word_search` (`keyword`,`asset_type_id`)
  ) ENGINE=InnoDB;
    Insert into __keywords_temp (asset_type_id, keyword) Select asset_type_id, keywords from kaneva. vw_published_public_assets where length(keywords) > 0;
	# This could have been the communities view but that is unnecessary
	Insert into __keywords_temp (asset_type_id, keyword ) select -1 as asset_type_id, ftcom.keywords from my_kaneva.ft_communities ftcom, kaneva.communities c where length(ftcom.keywords) > 0 and c.status_id = 1 and c.is_adult <> 'Y' and ftcom.community_id = c.community_id;
    OPEN xc;
    REPEAT
      FETCH xc INTO asset_type,input;
      SET cur_position = 1;
      SET remainder = input;
      SET delimiter_length = CHAR_LENGTH(' ');
      WHILE CHAR_LENGTH(remainder) > 0 AND cur_position > 0
      DO
          SET cur_position = INSTR(remainder, ' ');
          IF cur_position = 0 THEN
              SET cur_string = remainder;
          ELSE
              SET cur_string = LEFT(remainder, cur_position - 1);
          END IF;
          IF TRIM(cur_string) != '' THEN
              INSERT INTO __individual_keywords_temp (asset_type_id,keyword) VALUES (asset_type,cur_string);
          END IF;
          SET remainder = SUBSTRING(remainder, cur_position + delimiter_length);
      END WHILE;
    UNTIL done END REPEAT;
    CLOSE xc;
    
    #Aggregate counts for keywords now.
    Insert into __popular_keywords Select keyword, count(keyword) as count, asset_type_id from kaneva.__individual_keywords_temp group by asset_type_id,keyword;
    
    #Drop the existing popular keywords table
    DROP TABLE IF EXISTS kaneva.popular_keywords;
    
    #RENAME new temp table to the original table so the new data is in place.
    ALTER TABLE __popular_keywords rename to popular_keywords;
   
    DROP TABLE IF EXISTS __keywords_temp;
    DROP TABLE IF EXISTS __individual_keywords_temp;
    DROP TABLE IF EXISTS __popular_keywords;
END
;;
DELIMITER ;