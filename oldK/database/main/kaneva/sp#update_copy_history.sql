-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_copy_history;

DELIMITER ;;
CREATE PROCEDURE `update_copy_history`(table_ VARCHAR(100))
BEGIN
  /* create constants for comparison */
  declare _INSERTS TINYINT default 0;
  declare _UPDATES TINYINT default 1;
  declare _copyHistoryId INTEGER default 0;
  
  -- get the newest record for this table
  SELECT copy_history_id INTO _copyHistoryId FROM data_copy_history WHERE table_name = table_ ORDER BY copy_history_id DESC LIMIT 0,1;
  	
  -- update the proper records	
  UPDATE data_copy_history SET last_id_copied = projected_id_copied, last_modified_date = projected_modified_date WHERE table_name = table_
  AND copy_history_id = _copyHistoryId;
  
  SELECT 1;
END
;;
DELIMITER ;