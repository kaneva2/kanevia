-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_communities_thumbnail2;

DELIMITER ;;
CREATE PROCEDURE `update_communities_thumbnail2`(IN __community_id INT, IN __thumbnail_path VARCHAR(255), 
		IN __thumbnail_type VARCHAR(50), IN __has_thumbnail ENUM('Y','N'),
		IN __thumbnail_small_path VARCHAR(255), IN __thumbnail_medium_path VARCHAR(255),
		IN __thumbnail_large_path VARCHAR(255), IN __thumbnail_xlarge_path VARCHAR(255))
BEGIN

SELECT thumbnail_xlarge_path, is_personal, creator_id
	INTO @old_thumbnail_xlarge_path, @is_personal, @creator_id 
	FROM communities WHERE community_id = __community_id;

UPDATE communities 
	SET thumbnail_path = __thumbnail_path,
	thumbnail_type = __thumbnail_type, 
	thumbnail_small_path = __thumbnail_small_path,
	thumbnail_medium_path = __thumbnail_medium_path,
	thumbnail_large_path = __thumbnail_large_path,
	thumbnail_xlarge_path = __thumbnail_xlarge_path, 
	has_thumbnail = __has_thumbnail 
	WHERE community_id = __community_id;


IF @is_personal = 1 AND @old_thumbnail_xlarge_path <> __thumbnail_xlarge_path THEN
	UPDATE wok.dynamic_objects
		SET texture_url = __thumbnail_xlarge_path 
		WHERE friend_id = @creator_id;
END IF;

END
;;
DELIMITER ;