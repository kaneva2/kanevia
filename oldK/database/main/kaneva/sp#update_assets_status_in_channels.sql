-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_status_in_channels;

DELIMITER ;;
CREATE PROCEDURE `update_assets_status_in_channels`(assetID INT UNSIGNED, assetStatusID INT UNSIGNED)
BEGIN

  START TRANSACTION;
  
    UPDATE
    	asset_channels
    SET
    	status_id = assetStatusID
    WHERE 
    	asset_id = assetID;   	

    SELECT row_count();
    
  COMMIT;
END
;;
DELIMITER ;