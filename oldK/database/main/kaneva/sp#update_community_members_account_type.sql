-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_community_members_account_type;

DELIMITER ;;
CREATE PROCEDURE `update_community_members_account_type`(IN __community_id INT, IN __user_id INT, IN __account_type_id INT)
BEGIN
DECLARE _own_mod_count INT;

SET @has_edit_rights = IF (__account_type_id IN (1,2),'Y','N'),
    @active_member   = IF (__account_type_id IN (1,2,3), 'Y','N');
    
UPDATE community_members 
	SET account_type_id = __account_type_id ,
		has_edit_rights = @has_edit_rights,
		active_member = @active_member
	WHERE community_id = __community_id AND user_id = __user_id;

IF (__account_type_id = 2 OR __account_type_id = 1) THEN
    UPDATE users SET own_mod = 'Y' WHERE user_id = __user_id;
  ELSE
      SET _own_mod_count = (SELECT COUNT(*) AS own_mod
				 FROM community_members cm
				 INNER JOIN communities c ON cm.community_id = c.community_id
				 WHERE user_id = __user_id AND (account_type_id = 2 OR account_type_id = 1)
				 AND c.status_id = 1         AND c.is_personal = 0);
      IF (_own_mod_count = 0) OR (_own_mod_count IS NULL) THEN
         UPDATE users SET own_mod='N' WHERE user_id = __user_id;
      END IF;
  END IF;
END
;;
DELIMITER ;