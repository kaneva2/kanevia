-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS _test;

DELIMITER ;;
CREATE PROCEDURE `_test`()
BEGIN
DECLARE __asset_group_id INT;
SET @sort_order = -1;
	select sd.asset_id, @sort_order:=@sort_order+1 SortOrder, a.name, sd.number_of_diggs
		FROM summary_asset_diggs_1_days sd
		INNER JOIN assets a 					ON sd.asset_id = a.asset_id
		INNER JOIN asset_types at 				ON a.asset_type_id = at.asset_type_id
		WHERE a.asset_type_id = 2 AND over_21_required = 'N' AND mature = 'N'
		ORDER BY sd.number_of_diggs DESC
		LIMIT 20;
END
;;
DELIMITER ;