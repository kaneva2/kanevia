-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_user_status_id;

DELIMITER ;;
CREATE PROCEDURE `update_user_status_id`( IN _user_id INT(11), 
		IN _status_id INT(11))
BEGIN 

UPDATE users
	SET status_id = COALESCE(_status_id, status_id),
		active = IF(( (_status_id=1) | (_status_id=2) ),'Y','N')
	WHERE user_id = _user_id;

END
;;
DELIMITER ;