-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_publish_status;

DELIMITER ;;
CREATE PROCEDURE `update_assets_publish_status`(IN __asset_id INT, IN __run_time INT,
		IN __publish_status_id INT)
BEGIN
DECLARE _status_id INT;
DECLARE _new_published ENUM('Y','N');

SELECT status_id INTO _status_id FROM assets WHERE asset_id = __asset_id;
SET _new_published    = IF(( (__publish_status_id=10) & ( (_status_id=1) | (_status_id=3)) ),'Y','N');

UPDATE assets 
	SET run_time_seconds = __run_time,
	publish_status_id = __publish_status_id,
	published = _new_published,
	last_updated_date = NOW()
	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;