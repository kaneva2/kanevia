-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_user_account2jr;

DELIMITER ;;
CREATE PROCEDURE `update_user_account2jr`( IN _user_id INT(11), 
		IN _display_name VARCHAR(50), IN _email VARCHAR(100), IN _country CHAR(2), IN _zip_code VARCHAR(13))
BEGIN 
DECLARE __sql_statement VARCHAR(512);
DECLARE _old_display_name VARCHAR(50);
DECLARE _old_email VARCHAR(100); 
DECLARE _old_country VARCHAR(100); 
DECLARE _old_zip_code VARCHAR(13); 
DECLARE _old_birth_date DATE; 
DECLARE _old_location VARCHAR(80);
DECLARE _new_age TINYINT(4);
DECLARE _new_country VARCHAR(100); 
DECLARE _new_zip_code VARCHAR(13); 
DECLARE _new_over_21 ENUM('Y','N');
DECLARE _new_location VARCHAR(80);
DECLARE _city VARCHAR(50);
DECLARE _state VARCHAR(4);
DECLARE _city_state VARCHAR(60);
DECLARE _empty_string CHAR(1);
SET _empty_string = '';

SELECT display_name, email, country, zip_code, birth_date, location
	INTO _old_display_name, _old_email, _old_country, _old_zip_code, _old_birth_date, _old_location
	FROM users
	WHERE user_id = _user_id
	FOR UPDATE;
SET _new_age = DATE_FORMAT(FROM_DAYS(TO_DAYS(CONCAT(DATE(NOW()),' 23:59:59'))-TO_DAYS(_old_birth_date)), '%Y')+0;
SET _new_over_21 = IF(_new_age>=21, 'Y','N');
IF _country IS NOT NULL AND (_country <> _old_country) THEN
	set _new_country = (Select country from countries where country_id = _country);
END IF;
IF (_country IS NOT NULL) then
  IF (trim(_country) = 'US') then
    SET _new_zip_code =(Select zip_code from zip_codes where zip_code = _zip_code);
    if (_new_zip_code IS NOT NULL) AND (_new_zip_code <> '') AND (_new_zip_code <> '00000') THEN
      set _city = (Select city from zip_codes where zip_code = _new_zip_code);
      set _state = (Select state_code from zip_codes where zip_code = _new_zip_code);
      set _city_state = CONCAT(_city,',',_state);
    ELSE
      set _new_zip_code='00000';
    END IF;
  END IF;
END IF;
if (_city_state = '') then
    if (_new_country IS NOT NULL) then
      set _new_location = _new_country;
    else
      set _new_location='';
    END IF;
ELSE
    set _new_location = COALESCE(_city_state,_new_country);
END IF;

UPDATE users
	SET display_name = COALESCE(_display_name, _old_display_name), 
		email = COALESCE(_email, email), 
		country = COALESCE(_new_country, country), 
		zip_code = COALESCE(_new_zip_code, zip_code), 
		location = COALESCE(_new_location, location),
		age = COALESCE(_new_age, age),
		over_21 = COALESCE(_new_over_21, over_21)
	WHERE user_id = _user_id;
IF (_new_zip_code <> _old_zip_code OR
	   _new_country <> _old_country OR
	   _new_location <> _old_location) THEN
		SELECT _user_id, _empty_string, _old_display_name, _display_name, _new_zip_code, _old_zip_code, _new_country, _country,  _old_country, _new_location, _old_location;
		SET __sql_statement = CONCAT('CALL KGP%PARAM().post_update_users(',_user_id,
				',"',_empty_string,'","',_empty_string,'","',_old_display_name,'","',
		        _display_name,'","',COALESCE(_new_zip_code, _zip_code),'","',_old_zip_code,'","',COALESCE(_new_country, _country),
        		'","',_old_country,'","',_new_location,'","',_old_location,'");');
		CALL add_background_sql_tasks (__sql_statement, 'common', 'update_user_account2', 'SP');
END IF;
END
;;
DELIMITER ;