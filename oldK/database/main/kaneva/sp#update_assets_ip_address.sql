-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_ip_address;

DELIMITER ;;
CREATE PROCEDURE `update_assets_ip_address`(IN __asset_id INT, IN __ip_address VARCHAR(30))
BEGIN

UPDATE assets 
	SET ip_address = __ip_address
	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;