-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS clean_up_recent_asset_shares;

DELIMITER ;;
CREATE PROCEDURE `clean_up_recent_asset_shares`()
BEGIN
  Delete from kaneva.summary_asset_channels WHERE created_date <= DATE_SUB(NOW(), INTERVAL 7 DAY);
END
;;
DELIMITER ;