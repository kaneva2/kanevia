-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_message_gifts;

DELIMITER ;;
CREATE PROCEDURE `add_message_gifts`(IN __message_id INT, IN __gift_id INT, 
			IN __gift_status ENUM('U','A','R'), IN __gift_type ENUM('2D','3D'), OUT _rows_inserted TINYINT)
BEGIN
DECLARE _whoFrom INT(11) DEFAULT NULL;
DECLARE _whoTo INT(11) DEFAULT NULL;
DECLARE __sql_statement VARCHAR(512);
 
INSERT INTO message_gifts (message_id, gift_id, gift_status, gift_type) 
	VALUES (__message_id, __gift_id, __gift_status, __gift_type );
	
SELECT ROW_COUNT() INTO _rows_inserted;

SELECT from_id, to_id INTO _whoFrom, _whoTo FROM messages WHERE message_id = __message_id;

IF _whoFrom IS NOT NULL AND _whoTo IS NOT NULL THEN
	UPDATE kaneva.users_stats SET number_gifts_pending = number_gifts_pending + 1 WHERE user_id = _whoTo;
	UPDATE kaneva.users_stats SET number_gifts_given = number_gifts_given + 1 WHERE user_id = _whoFrom;
END IF;
	
END
;;
DELIMITER ;