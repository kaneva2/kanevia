-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_community_members;

DELIMITER ;;
CREATE PROCEDURE `add_community_members`(IN __community_id INT, IN __user_id INT, IN __account_type_id INT, IN __status_id INT, 
	IN __newsletter CHAR(1), IN __invited_by_user_id INT, IN __allow_asset_uploads CHAR(1), IN __allow_forum_use CHAR(1), 
	IN __keywords VARCHAR(100), IN __notifications INT, IN __item_notify INT, IN __item_review_notify INT, 
	IN __blog_notify INT, IN __blog_comment_notify INT, IN __post_notify INT, IN __reply_notify INT)
BEGIN
  DECLARE __sql_statement VARCHAR(512);
  
  SET @has_edit_rights = IF (__account_type_id IN (1,2),'Y','N'),
	@active_member = IF (__account_type_id IN (1,2,3), 'Y','N');

  INSERT INTO community_members
	(community_id, user_id, account_type_id, added_date, status_id, newsletter, invited_by_user_id, 
	allow_asset_uploads, allow_forum_use, keywords, notifications, item_notify, item_review_notify, 
	blog_notify, blog_comment_notify, post_notify, reply_notify, has_edit_rights, active_member)
  VALUES
	(__community_id, __user_id, __account_type_id, NOW(), __status_id, __newsletter, __invited_by_user_id, 
	__allow_asset_uploads, __allow_forum_use, __keywords, __notifications, __item_notify, __item_review_notify, 
	__blog_notify, __blog_comment_notify, __post_notify, __reply_notify, @has_edit_rights, @active_member);
 
  IF (__status_id = 2) THEN
	UPDATE kaneva.channel_stats SET number_of_pending_members = number_of_pending_members + 1 WHERE channel_id = __community_id;
  END IF;

  IF (__status_id = 1)  THEN
	UPDATE kaneva.channel_stats SET number_of_members = number_of_members + 1 WHERE channel_id = __community_id;
	IF (__account_type_id = 2 OR __account_type_id = 1) THEN
		UPDATE kaneva.users SET own_mod = 'Y' WHERE user_id = __user_id;
	END IF;
  END IF;


END
;;
DELIMITER ;