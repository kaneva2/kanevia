-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_user_username;

DELIMITER ;;
CREATE PROCEDURE `update_user_username`( IN _user_id INT(11), 
		IN _username VARCHAR(22))
BEGIN 
DECLARE __sql_statement VARCHAR(512);

DECLARE _old_username VARCHAR(22);
DECLARE _old_birth_date DATE; 

DECLARE _new_age TINYINT(4);
DECLARE _new_over_21 ENUM('Y','N');

DECLARE _empty_string CHAR(1);
SET _empty_string = '';

SELECT username, birth_date
	INTO _old_username, _old_birth_date
	FROM users
	WHERE user_id = _user_id
	FOR UPDATE;

SET _new_age = DATE_FORMAT(FROM_DAYS(TO_DAYS(CONCAT(DATE(NOW()),' 23:59:59'))-TO_DAYS(_old_birth_date)), '%Y')+0;
SET _new_over_21 = IF(_new_age>=21, 'Y','N');

UPDATE users
	SET username = COALESCE(_username, _old_username), 
		age = COALESCE(_new_age, age),
		over_21 = COALESCE(_new_over_21, over_21)
	WHERE user_id = _user_id;

IF (_username <> _old_username) THEN
	CALL kaneva.post_update_users(_user_id, _username, _old_username, _empty_string, _empty_string, _empty_string, _empty_string,
		_empty_string, _empty_string, _empty_string, _empty_string);
END IF;
END
;;
DELIMITER ;