-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_sitemembers;

DELIMITER ;;
CREATE PROCEDURE `ss_update_sitemembers`(  )
begin
  replace into kaneva.snapshot_site_members
  select count(user_id) as c,date(now())
  from kaneva.users;
end
;;
DELIMITER ;