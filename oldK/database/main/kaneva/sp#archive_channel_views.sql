-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS archive_channel_views;

DELIMITER ;;
CREATE PROCEDURE `archive_channel_views`( in startdate datetime, in enddate datetime )
begin
  replace into my_kaneva.community_views_arch Select * from kaneva.community_views where created_date BETWEEN startdate AND enddate;
	DELETE from kaneva.community_views where created_date BETWEEN startdate AND enddate;
end
;;
DELIMITER ;