-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS remove_interest;

DELIMITER ;;
CREATE PROCEDURE `remove_interest`(IN __interest_id INTEGER, OUT __return_val INTEGER)
BEGIN
-- REMOVES an interest only if no one else already has it associated to them
-- RETURN value is -1 if it cannot remove it because it already exists somewhere else
-- RETURN value is 0 if it is successful.
  DECLARE __item_count INT;
  Select count(user_id) into __item_count from kaneva.user_interests WHERE interest_id = __interest_id;
  if (__item_count > 0) THEN
    SET __return_val = -1;
  ELSE
    DELETE from kaneva.interests WHERE interest_id = __interest_id;
    SET __return_val = 0;
  END IF;
END
;;
DELIMITER ;