-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_delete_thumbs;

DELIMITER ;;
CREATE PROCEDURE `update_assets_delete_thumbs`(IN __asset_id INT, IN __image_path VARCHAR(255),
		IN __image_type VARCHAR(50), IN __thumbnail_small_path VARCHAR(255), 
		IN __thumbnail_medium_path VARCHAR(255), IN __thumbnail_large_path VARCHAR(255), 
		IN __thumbnail_xlarge_path VARCHAR(255), IN __thumbnail_assetdetails_path VARCHAR(255))
BEGIN

UPDATE assets 
	SET image_path = __image_path,
	image_type = __image_type,
	thumbnail_small_path = __thumbnail_small_path,
	thumbnail_medium_path = __thumbnail_medium_path,
	thumbnail_large_path = __thumbnail_large_path,
	thumbnail_xlarge_path = __thumbnail_xlarge_path, 
	thumbnail_assetdetails_path = __thumbnail_assetdetails_path 
	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;