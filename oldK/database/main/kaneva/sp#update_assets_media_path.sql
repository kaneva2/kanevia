-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_media_path;

DELIMITER ;;
CREATE PROCEDURE `update_assets_media_path`(IN __asset_id INT, IN __media_path  VARCHAR(255))
BEGIN

UPDATE assets 
	SET media_path = __media_path 
        WHERE asset_id = __asset_id;

END
;;
DELIMITER ;