-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS insertCSRLog;

DELIMITER ;;
CREATE PROCEDURE `insertCSRLog`( IN userId INT(11), IN activity TEXT, 
 									      IN assetId INT(11), IN updatedUserId INT(11),
 									      OUT csrLogId INT(11) )
BEGIN

	INSERT INTO csr_logs 
		       (user_id, activity, asset_id, updated_user_id, created_datetime ) 
		VALUES (userId,  activity, assetId,  updatedUserId,   NOW() );

	SET csrLogId = LAST_INSERT_ID();

END
;;
DELIMITER ;