-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_invites_email_hash;

DELIMITER ;;
CREATE PROCEDURE `update_invites_email_hash`(__update_after_date	DATETIME, __cutoff_time DATETIME)
BEGIN
DECLARE	__Max_invite_date		datetime;
DECLARE	__Min_invite_date		datetime;
DECLARE __batch_beg_date		datetime;
DECLARE __batch_end_date		datetime;
DECLARE	__table_RowCnt		int;
SELECT MAX(invited_date), MIN(invited_date) INTO __Max_invite_date, __Min_invite_date
	FROM	invites 	WHERE invited_date > __update_after_date;
SELECT CONCAT(DATE(__Min_invite_date),' 00:00:00') INTO __batch_beg_date;
SELECT CONCAT(DATE(adddate(__batch_beg_date, interval 1 day)),' 00:00:00') INTO __batch_end_date;
SET __table_RowCnt = 1;

WHILE ( __batch_beg_date < __Max_invite_date) AND (NOW() < __cutoff_time)    DO
	UPDATE kaneva.invites SET email_hash = UNHEX(MD5(email))
		WHERE invited_date between __batch_beg_date and __batch_end_date;
	SELECT IFNULL(ROW_COUNT(),0) as rowsUpdated, __batch_beg_date, __batch_end_date;
	SELECT CONCAT(DATE(adddate(__batch_beg_date, interval 1 day)),' 00:00:00') INTO __batch_beg_date;
	SELECT CONCAT(DATE(adddate(__batch_end_date, interval 1 day)),' 00:00:00') INTO __batch_end_date;
	END WHILE;
END
;;
DELIMITER ;