-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_community;

DELIMITER ;;
CREATE PROCEDURE `delete_community`(_communityId INTEGER UNSIGNED)
BEGIN

  START TRANSACTION;
	
    
    CALL delete_members_from_community(_communityId); 
    
    
    CALL delete_assets_from_community(_communityId);
    
    
    UPDATE 
   	communities
    SET 
   	status_id = 3,
        last_edit = Now()
    WHERE 
   	community_id = _communityId;

    
    SELECT row_count();

  COMMIT;

END
;;
DELIMITER ;