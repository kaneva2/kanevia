-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_spender_activity;

DELIMITER ;;
CREATE PROCEDURE `update_spender_activity`()
BEGIN
  Replace Into kaneva.spender_activity SELECT user_id,sum(total) as wok_spending_total, if(sum(total) >= 9.99,'Y','N') as active_spender
  FROM kaneva.summary_ppt_completed_kpoint_transactions_by_day ppt
  group by user_id;
  Replace into kaneva.spender_activity_counts select date(now()), sum(if(active_spender='Y',1,0)),sum(if(active_spender='N',1,0)) from spender_activity;
END
;;
DELIMITER ;