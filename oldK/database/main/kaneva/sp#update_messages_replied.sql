-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_messages_replied;

DELIMITER ;;
CREATE PROCEDURE `update_messages_replied`(IN __message_id INT)
BEGIN
  UPDATE kaneva.messages SET replied = 1 WHERE message_id = __message_id;
 
END
;;
DELIMITER ;