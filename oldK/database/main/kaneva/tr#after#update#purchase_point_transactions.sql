-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS after_ppt_update;

DELIMITER ;;
CREATE TRIGGER `after_ppt_update` AFTER UPDATE ON `purchase_point_transactions` 
    FOR EACH ROW BEGIN
DECLARE _count INT;
SET _count = (SELECT COUNT(user_id) AS c FROM first_time_transactions WHERE user_id = new.user_id);
      
IF (_count IS NULL) OR (_count = 0) THEN
	IF new.transaction_status = 3 AND new.amount_debited > 0 THEN
		INSERT INTO first_time_transactions VALUES(new.user_id, new.transaction_date);
	END IF;
END IF;
END
;;
DELIMITER ;