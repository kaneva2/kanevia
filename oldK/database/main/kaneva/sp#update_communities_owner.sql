-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_communities_owner;

DELIMITER ;;
CREATE PROCEDURE `update_communities_owner`(IN __community_id INT, IN __creator_id INT,
		IN __creator_username VARCHAR(22))
BEGIN

UPDATE communities 
	SET creator_id = __creator_id, 
        creator_username = __creator_username
	WHERE community_id = __community_id;                      

END
;;
DELIMITER ;