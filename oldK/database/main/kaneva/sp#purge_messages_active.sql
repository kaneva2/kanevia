-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS purge_messages_active;

DELIMITER ;;
CREATE PROCEDURE `purge_messages_active`(__purge_before_date	DATE, __purge_cutoff_time DATETIME)
BEGIN

DECLARE	__Max_message_id		int;
DECLARE	__message_RowCnt		int;
DECLARE __message_text varchar(500);

SELECT	IFNULL(COUNT(message_id),0) INTO __message_RowCnt
	FROM	messages
	WHERE	message_date < __purge_before_date;

SELECT IFNULL(MAX(message_id),__purge_before_date) INTO __Max_message_id
	FROM	messages
	WHERE	message_date < __purge_before_date;

SELECT CONCAT('Starting purge ... ',CAST(__message_RowCnt as CHAR),' rows to purge. Purge before date: ',
									CAST(__purge_before_date as CHAR),'; Purge cutoff time: ',
									CAST(__purge_cutoff_time AS CHAR)) INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'purge_messages_active',__message_text);

-- Loop while anything to delete.
WHILE (__message_RowCnt > 0 AND NOW() < __purge_cutoff_time)    DO

	DELETE FROM	messages WHERE message_id <= __Max_message_id LIMIT 5000;

	SELECT IFNULL(ROW_COUNT(),0) INTO __message_RowCnt;

	SELECT  CONCAT('Purged ',CAST(__message_RowCnt AS CHAR),' rows.') INTO __message_text;
	CALL shard_info.add_maintenance_log (DATABASE(),'purge_messages_active',__message_text);

	END WHILE;

SELECT 'Purge completed.' INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'purge_messages_active',__message_text);

END
;;
DELIMITER ;