-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_interest;

DELIMITER ;;
CREATE PROCEDURE `add_interest`(IN __ic_id INT, IN __interest varchar(255), OUT __return_val INTEGER)
BEGIN
-- CREATES an interest if it does not already exists.  It does a check itself internally
-- The return value is unique ID of the interest.
  DECLARE __interest_id INTEGER;
  
  SET __interest_id = interest_exists(__ic_id, __interest);
  if (__interest_id > 0) THEN
    SET __return_val = __interest_id;
	ELSE
    INSERT into kaneva.interests (ic_id,interest) VALUES(__ic_id,__interest);
    SELECT LAST_INSERT_ID() into __interest_id ;
    SET __return_val = __interest_id;
  END IF;
END
;;
DELIMITER ;