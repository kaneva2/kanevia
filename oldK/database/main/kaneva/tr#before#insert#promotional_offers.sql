-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS before_insert_promotional_offers;

DELIMITER ;;
CREATE TRIGGER `before_insert_promotional_offers` BEFORE INSERT ON `promotional_offers` FOR EACH ROW BEGIN
    INSERT into kaneva.audit_promotional_offers 
    (
    	bundle_title, bundle_subheading1, bundle_subheading2, dollar_amount, 
    	kei_point_amount, promotion_start, promotion_end, 
        is_special, promotion_list_heading, promotion_description, 
        highlight_color, special_background_color, special_background_image, 
        special_sticker_image, promotional_package_label, kei_point_id, 
        free_points_awarded_amount, free_kei_point_ID, promotional_offers_type_id,
        wok_pass_group_id, sku, special_font_color, value_of_credits, modifiers_id,
        date_modified, operation
    )
    VALUES
    (
    	
    	new.bundle_title, new.bundle_subheading1, new.bundle_subheading2, new.dollar_amount, 
    	new.kei_point_amount, new.promotion_start, new.promotion_end, 
        new.is_special, new.promotion_list_heading, new.promotion_description, 
        new.highlight_color, new.special_background_color, new.special_background_image, 
        new.special_sticker_image, new.promotional_package_label, new.kei_point_id, 
        new.free_points_awarded_amount, new.free_kei_point_ID, new.promotional_offers_type_id,
        new.wok_pass_group_id, new.sku, new.special_font_color, new.value_of_credits, new.modifiers_id,
        Now(), 'insert'
    ) ;   
END
;;
DELIMITER ;