-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_media_views;

DELIMITER ;;
CREATE PROCEDURE `update_media_views`( IN __asset_id INT(11))
BEGIN 
DECLARE __sql_statement VARCHAR(512);

UPDATE kaneva.assets_stats SET number_of_downloads = number_of_downloads + 1 WHERE asset_id = __asset_id;
INSERT INTO kaneva.summary_assets_viewed VALUES(__asset_id, DATE(NOW()), 1) ON DUPLICATE KEY UPDATE `count` = `count` + 1;

END
;;
DELIMITER ;