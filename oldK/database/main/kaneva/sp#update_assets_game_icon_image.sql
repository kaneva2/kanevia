-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_assets_game_icon_image;

DELIMITER ;;
CREATE PROCEDURE `update_assets_game_icon_image`(IN __asset_id INT, IN __icon_image_data LONGBLOB,
		IN __icon_image_path VARCHAR(100), IN __icon_image_type VARCHAR(50))
BEGIN

UPDATE assets 
	SET icon_image_path = __icon_image_path,
	icon_image_type = __icon_image_type,
	icon_image_data = __icon_image_data
	WHERE asset_id = __asset_id;

END
;;
DELIMITER ;