-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_user_identity_csr;

DELIMITER ;;
CREATE PROCEDURE `update_user_identity_csr`( IN _user_id INT(11), 
		IN _first_name VARCHAR(50), IN _last_name VARCHAR(50), IN _description TEXT,
		IN _gender CHAR, IN _homepage VARCHAR(200), IN _email VARCHAR(100), IN _show_email VARCHAR(11),
		IN _show_mature BINARY(1), IN _birth_date DATE, IN _country CHAR(2), IN _zip_code VARCHAR(13), IN _display_name VARCHAR(30))
BEGIN 
DECLARE __sql_statement VARCHAR(512);

DECLARE _old_first_name VARCHAR(50);
DECLARE _old_last_name VARCHAR(50);
DECLARE _old_username VARCHAR(22);
DECLARE _old_description TEXT;
DECLARE _old_gender CHAR; 
DECLARE _old_homepage VARCHAR(200); 
DECLARE _old_email VARCHAR(100); 
DECLARE _old_show_email VARCHAR(11);
DECLARE _old_show_mature BINARY(1); 
DECLARE _old_birth_date DATE; 
DECLARE _old_country CHAR(2); 
DECLARE _old_zip_code VARCHAR(13); 
DECLARE _old_display_name VARCHAR(30);
DECLARE _old_location VARCHAR(80);

DECLARE _new_age TINYINT(4);
DECLARE _new_country CHAR(2); 
DECLARE _new_zip_code VARCHAR(13); 
DECLARE _new_over_21 ENUM('Y','N');
DECLARE _new_location VARCHAR(80);

DECLARE _city VARCHAR(50);
DECLARE _state VARCHAR(4);
DECLARE _city_state VARCHAR(60);

IF _birth_date IS NOT NULL THEN
	set _new_age = DATE_FORMAT(FROM_DAYS(TO_DAYS(CONCAT(DATE(NOW()),' 23:59:59'))-TO_DAYS(_birth_date)), '%Y')+0;
	SET _new_over_21 = IF(_new_age>=21, 'Y','N');
END IF;

SELECT first_name, last_name, description, gender, homepage, email, show_email, username, show_mature, birth_date, country, zip_code, display_name, location
	INTO _old_first_name, _old_last_name, _old_description, _old_gender, _old_homepage, _old_email, _old_show_email, _old_username,
			_old_show_mature, _old_birth_date, _old_country, _old_zip_code, _old_display_name, _old_location
	FROM users
	WHERE user_id = _user_id
	FOR UPDATE;

IF _new_age IS NULL THEN
	set _new_age = DATE_FORMAT(FROM_DAYS(TO_DAYS(CONCAT(DATE(NOW()),' 23:59:59'))-TO_DAYS(_old_birth_date)), '%Y')+0;
	SET _new_over_21 = IF(_new_age>=21, 'Y','N');
END IF;

IF _country IS NOT NULL AND (_country <> _old_country) THEN
	set _new_country = (Select country from countries where country_id = _country);
END IF;

IF (_country IS NOT NULL) then
  IF (trim(_country) = 'US') then
    SET _new_zip_code =(Select zip_code from zip_codes where zip_code = _zip_code);
    if (_new_zip_code IS NOT NULL) AND (_new_zip_code <> '') AND (_new_zip_code <> '00000') THEN
      set _city = (Select city from zip_codes where zip_code = _new_zip_code);
      set _state = (Select state_code from zip_codes where zip_code = _new_zip_code);
      set _city_state = CONCAT(_city,',',_state);
    ELSE
      set _new_zip_code='00000';
    END IF;
  END IF;
END IF;

if (_city_state = '') then
    if (_new_country IS NOT NULL) then
      set _new_location = _new_country;
    else
      set _new_location='';
    END IF;
ELSE
    set _new_location = _city_state;
END IF;

UPDATE users
	SET first_name = COALESCE(_first_name, first_name), 
		last_name = COALESCE(_last_name, last_name), 
		description = COALESCE(_description, description), 
		gender = COALESCE(_gender, gender), 
		homepage = COALESCE(_homepage, homepage), 
		email = COALESCE(_email, email), 
		show_email = COALESCE(_show_email, show_email), 
		show_mature = COALESCE(_show_mature, show_mature), 
		birth_date = COALESCE(_birth_date, birth_date), 
		country = COALESCE(_new_country, country), 
		zip_code = COALESCE(_new_zip_code, zip_code), 
		display_name = COALESCE(_display_name, display_name),
		location = COALESCE(_new_location, location),
		age = COALESCE(_new_age, age),
		over_21 = COALESCE(_new_over_21, over_21)
	WHERE user_id = _user_id;

IF (_old_email <> _email AND _email IS NOT NULL) THEN
	REPLACE INTO users_email (user_id, email_hash) VALUES (_user_id, UNHEX(MD5(_email)));
END IF;
IF (_old_email <> _email AND _email IS NULL) THEN
	DELETE FROM users_email WHERE user_id = _user_id;
END IF;

IF (_display_name <> _old_display_name OR _new_zip_code <> _old_zip_code OR _new_country <> _old_country OR _new_location <> _old_location) THEN
	   CALL kaneva.post_update_users(_user_id, _old_username, _old_username, _new_display_name, _old_display_name, _new_zip_code, _old_zip_code,
			_new_country, _old_country, _new_location, _old_location);
END IF;
END
;;
DELIMITER ;