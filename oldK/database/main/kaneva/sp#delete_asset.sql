-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_asset;

DELIMITER ;;
CREATE PROCEDURE `delete_asset`(assetID INT UNSIGNED)
BEGIN
 
  START TRANSACTION;
    
    /* delete the records */
    DELETE FROM assets WHERE asset_id = assetID ;

    /*return the number of records affected*/
    SELECT row_count();

  COMMIT;
END
;;
DELIMITER ;