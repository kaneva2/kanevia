-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS GiftCounts;

DELIMITER ;;
CREATE PROCEDURE `GiftCounts`( IN userId INT(11), OUT maxGifts INT, OUT myGifted INT, OUT myGifting INT )
BEGIN
-- get the count of gifts for a user, and the total
SELECT count(*) INTO myGifted
  FROM messages m, message_gifts mg
 WHERE m.message_id = mg.message_id 
   AND m.to_id = userId 
	 AND mg.gift_status = 'A';
SELECT count(*) INTO myGifting
  FROM messages m, message_gifts mg
 WHERE m.message_id = mg.message_id
   AND m.from_id = userId
   AND mg.gift_status = 'A';
SELECT count(*) INTO maxGifts
  FROM messages m, message_gifts mg
 WHERE m.message_id = mg.message_id 
   AND mg.gift_status = 'A'
GROUP BY m.from_id
ORDER BY 1 DESC
  LIMIT 0,1;
END
;;
DELIMITER ;