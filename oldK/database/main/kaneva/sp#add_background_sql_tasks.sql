-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_background_sql_tasks;

DELIMITER ;;
CREATE PROCEDURE `add_background_sql_tasks`(__sql_statement	VARCHAR(512), __target_schema VARCHAR(20), 
	__trigger_event_table VARCHAR(80), __trigger_event_manipulation VARCHAR(12))
BEGIN
	DECLARE __processing_flag INT DEFAULT 0;
	
	IF INSTR(__sql_statement, 'select ') = 1 THEN 
		SET __processing_flag = -2;
	END IF;
	
	IF INSTR(__sql_statement, 'KGP%PARAM().') = 0 THEN 
		SET __processing_flag = -3;
	END IF;
		
	IF __target_schema = 'common' THEN 
		SET @SQL = REPLACE (__sql_statement, 'KGP%PARAM()', 'kaneva'); 
	ELSE
		SET @SQL = REPLACE (__sql_statement, 'KGP%PARAM()', DATABASE() ); 
	END IF;
	INSERT INTO shard_info.background_sql_tasks 
		(inserted_at, processing_flag, sql_statement, target_schema, 
		trigger_event_table, trigger_event_manipulation)
		VALUES 
		(NOW(), __processing_flag, @SQL, __target_schema, 
		__trigger_event_table, __trigger_event_manipulation);
END
;;
DELIMITER ;