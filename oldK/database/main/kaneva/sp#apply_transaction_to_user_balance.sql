-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS apply_transaction_to_user_balance;

DELIMITER ;;
CREATE PROCEDURE `apply_transaction_to_user_balance`( in user_id int, in transaction_amount float, 
							in transaction_type smallint unsigned, 
							in v_kei_point_id varchar(20), OUT return_code INT, OUT user_balance float, OUT tranId INT )
BEGIN
-- Return code can be one of three values
-- 1 indicates the transaction is invalid due to insufficient funds
-- 0 indicates the transaction was valid and recorded
-- -1 indicates the transaction was invalid due to an error
	-- Get user balance and determine if the transaction_amount can be supported.
	SET return_code = -1;
	SET tranId = 0;
	SET user_balance = NULL;
	
	SELECT ub.balance INTO user_balance 
	  FROM user_balances ub 
     WHERE ub.user_id = user_id AND kei_point_id = v_kei_point_id;
	
	IF user_balance is NOT NULL THEN
		IF (user_balance + transaction_amount) < 0 THEN 
			SET return_code = 1; -- overdrawn
		ELSE
			SET return_code = 0;
			UPDATE user_balances ub SET ub.balance = ub.balance + transaction_amount 
			 WHERE ub.user_id = user_id and kei_point_id = v_kei_point_id;
			 
		END IF;
	ELSE
		SET user_balance = 0;
		
		-- need to add it
		IF transaction_amount < 0 THEN
			SET return_code = 1; -- overdrawn
		ELSE
			SET return_code = 0;
			INSERT INTO user_balances ( user_id, kei_point_id, balance )
			       VALUES ( user_id, v_kei_point_id, transaction_amount );
		END IF;
	END IF;
	IF return_code = 0 THEN
		INSERT INTO wok_transaction_log  
		      ( created_date, user_id, transaction_amount, balance_prev, balance,                           transaction_type, kei_point_id ) 
	    VALUES( now(),        user_id, transaction_amount, user_balance, user_balance + transaction_amount, transaction_type, v_kei_point_id );
		
		--  set output values
	    SET tranId = LAST_INSERT_ID();
		SET user_balance = user_balance + transaction_amount;
	END IF;
	
END
;;
DELIMITER ;