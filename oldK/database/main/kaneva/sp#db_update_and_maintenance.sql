-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS db_update_and_maintenance;

DELIMITER ;;
CREATE PROCEDURE `db_update_and_maintenance`()
BEGIN
  CALL update_report_stats(DATE_SUB( CONCAT(DATE(NOW()),' 00:00:00'),INTERVAL 1 DAY), now());
  CALL _database_cleanup();
END
;;
DELIMITER ;