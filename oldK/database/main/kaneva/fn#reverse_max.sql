-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS reverse_max;

DELIMITER ;;
CREATE FUNCTION `reverse_max`() RETURNS int(11)
    NO SQL
    DETERMINISTIC
BEGIN
  return 2147483647;
END
;;
DELIMITER ;