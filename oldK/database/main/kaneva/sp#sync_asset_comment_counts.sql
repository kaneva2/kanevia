-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS sync_asset_comment_counts;

DELIMITER ;;
CREATE PROCEDURE `sync_asset_comment_counts`(__cutoff_time DATETIME)
BEGIN
DECLARE done INT DEFAULT 0;
DECLARE i INT;
declare _asset_id int;
declare _number_of_comments int;
DECLARE _number_of_comments_C INT;
DECLARE arch_cursor CURSOR FOR 
			sELECT asset_id, number_of_comments FROM assets_stats;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;
SET i = 0;
OPEN arch_cursor;
REPEAT
    FETCH arch_cursor INTO _asset_id, _number_of_comments;
    IF NOT done THEN
	SET i=i+1;
	SELECT COUNT(comment_id) into _number_of_comments_C
		FROM comments
		WHERE asset_id = _asset_id;
	IF _number_of_comments <> _number_of_comments_C THEN
		-- select _asset_id, _number_of_comments, _number_of_comments_C;
		
		update assets_stats set number_of_comments = _number_of_comments_C where asset_id = _asset_id;
	end if;
	IF NOW() >= __cutoff_time THEN 
		SET done = 1; 
	END IF;
    END IF;	
UNTIL done END REPEAT;
CLOSE arch_cursor;
END
;;
DELIMITER ;