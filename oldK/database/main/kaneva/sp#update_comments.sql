-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_comments;

DELIMITER ;;
CREATE PROCEDURE `update_comments`(IN __user_id INT, IN `__comment` TEXT, IN __comment_id INT)
BEGIN

UPDATE comments 
	SET `comment` = __comment, last_updated_date = NOW(), last_updated_user_id = __user_id 
	WHERE comment_id = __comment_id;

END
;;
DELIMITER ;