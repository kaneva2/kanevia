-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS remove_from_schoolscount;

DELIMITER ;;
CREATE PROCEDURE `remove_from_schoolscount`(IN __school_id INT)
BEGIN
  update kaneva.schools SET user_count=user_count - 1 WHERE school_id = __school_id;
END
;;
DELIMITER ;