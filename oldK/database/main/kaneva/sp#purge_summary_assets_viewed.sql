-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS purge_summary_assets_viewed;

DELIMITER ;;
CREATE PROCEDURE `purge_summary_assets_viewed`(__purge_before_date	DATE, __purge_cutoff_time DATETIME)
BEGIN
DECLARE	__Max_record_date		date;
DECLARE	__Min_record_date		date;
DECLARE __batch_date			date;
DECLARE	__table_RowCnt		int;
DECLARE __message_text varchar(500);
SELECT IFNULL(MAX(dt_stamp),__purge_before_date), IFNULL(MIN(dt_stamp),__purge_before_date) INTO __Max_record_date, __Min_record_date
	FROM	summary_assets_viewed
	WHERE	dt_stamp < __purge_before_date;
SELECT CONCAT('Starting purge ... ',CAST(__purge_before_date as CHAR),'; Purge cutoff time: ',
					CAST(__purge_cutoff_time AS CHAR)) INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'purge_summary_assets_viewed',__message_text);
SET __batch_date= __Min_record_date;
SET __table_RowCnt = 1;
-- Loop while anything to delete.
WHILE ( __batch_date < __purge_before_date) AND (NOW() < __purge_cutoff_time)    DO
	DELETE FROM summary_assets_viewed WHERE dt_stamp = __batch_date;
	SELECT IFNULL(ROW_COUNT(),0) INTO __table_RowCnt;
	SELECT  CONCAT('Purged ',CAST(__table_RowCnt AS CHAR),' rows; from date: ',CAST(__batch_date AS CHAR)) INTO __message_text;
	CALL shard_info.add_maintenance_log (DATABASE(),'purge_summary_assets_viewed',__message_text);
	SET __batch_date = ADDDATE(__batch_date, INTERVAL 1 DAY);
	END WHILE;
SELECT 'Purge completed.' INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'purge_summary_assets_viewed',__message_text);
END
;;
DELIMITER ;