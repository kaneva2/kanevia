-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS before_delete_promotional_offers;

DELIMITER ;;
CREATE TRIGGER `before_delete_promotional_offers` BEFORE DELETE ON `promotional_offers` FOR EACH ROW BEGIN
    INSERT into kaneva.audit_promotional_offers 
    (
    	bundle_title, bundle_subheading1, bundle_subheading2, dollar_amount, 
    	kei_point_amount, promotion_start, promotion_end, 
        is_special, promotion_list_heading, promotion_description, 
        highlight_color, special_background_color, special_background_image, 
        special_sticker_image, promotional_package_label, kei_point_id, 
        free_points_awarded_amount, free_kei_point_ID, promotional_offers_type_id,
        wok_pass_group_id, sku, special_font_color, value_of_credits, modifiers_id,
        date_modified, operation
    )
    VALUES
    (
    	
    	old.bundle_title, old.bundle_subheading1, old.bundle_subheading2, old.dollar_amount, 
    	old.kei_point_amount, old.promotion_start, old.promotion_end, 
        old.is_special, old.promotion_list_heading, old.promotion_description, 
        old.highlight_color, old.special_background_color, old.special_background_image, 
        old.special_sticker_image, old.promotional_package_label, old.kei_point_id, 
        old.free_points_awarded_amount, old.free_kei_point_ID, old.promotional_offers_type_id,
        old.wok_pass_group_id, old.sku, old.special_font_color, old.value_of_credits, old.modifiers_id,
        Now(), 'delete'
    ) ;   
   
END
;;
DELIMITER ;