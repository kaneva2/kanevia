-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS apply_transaction_to_multiple_user_balances2;

DELIMITER ;;
CREATE PROCEDURE `apply_transaction_to_multiple_user_balances2`(in user_id_list text, in user_email_list text, in transaction_amount float,
							in transaction_type smallint unsigned, in v_kei_point_id varchar(20), in reward_event_id int, OUT success_count INT, OUT failed_user_id_list text)
BEGIN
  DECLARE done INT DEFAULT 0;
  DECLARE _count INT DEFAULT 0;
  DECLARE _userID INT;
	DECLARE tranId INT DEFAULT 0;
	DECLARE return_code INT DEFAULT 0;
	DECLARE user_balance float DEFAULT 0;
	DECLARE failed_list text DEFAULT '';

  DECLARE user_id_cursor CURSOR FOR select user_id from __tempIDsTranscations;
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

  -- -- creates prepared statements statment based on which set is being used, multiple emails or multiple user ids -------------------------------------------------------------
  DROP TABLE IF EXISTS kaneva.__tempIDsTranscations;
	
	IF user_id_list is NOT NULL THEN
       Set @_stmt := CONCAT("Create table __tempIDsTranscations as select user_id from kaneva.users where user_id in (", user_id_list, ');');
   ELSE
       Set @_stmt := CONCAT("Create table __tempIDsTranscations as select user_id from kaneva.users where email in (", user_email_list, ');');
  END IF;

  -- -------------- executes prepared statement which creates table as a select of user_ids via provided list--------------------------------
  PREPARE __stmt FROM @_stmt;
  EXECUTE __stmt;
  DEALLOCATE PREPARE __stmt;

  -- ------------- Cursor that loops through id's and updates each account -------------------------------
  SET done = 0;

  OPEN user_id_cursor;

  REPEAT
    FETCH user_id_cursor INTO _userID;
      
			IF NOT done THEN
			
				call kaneva.apply_transaction_to_user_balance( _userID, transaction_amount, transaction_type, v_kei_point_id, return_code, user_balance, tranId);
      	
				set _count = _count + 1;

				Insert into kaneva.reward_log (user_id, date_created, amount, kei_point_id, transaction_type, return_code, reward_event_id, wok_transaction_log_id) VALUES (_userId, now(), transaction_amount, v_kei_point_id, transaction_type, return_code, reward_event_id, tranId);
			
			 	IF return_code <> 0 THEN
			 		SET failed_list = failed_list + ',' + _userID;
					SET return_code = 0;
      	END IF;
	
			END IF;

	UNTIL done END REPEAT;
  CLOSE user_id_cursor;

	SELECT failed_list INTO failed_user_id_list;
	
	-- put the count of successful userids into return code
  SELECT _count INTO success_count;
	-- SELECT 0 INTO success_count;     -- ***** Use this until new code is deployed that will handle count as return value

	
	
  -- -------removes the temp table --------------------
  drop table if exists __tempIDsTranscations ;
END
;;
DELIMITER ;