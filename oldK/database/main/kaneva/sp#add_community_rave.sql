-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_community_rave;

DELIMITER ;;
CREATE PROCEDURE `add_community_rave`(_CommunityId INTEGER UNSIGNED, _NumberOfRaves INTEGER UNSIGNED,  _RavingUserId INTEGER UNSIGNED)
BEGIN
  START TRANSACTION;
	
    
    INSERT INTO channel_diggs (user_id, channel_id, created_date, rave_count) VALUES (_RavingUserId, _CommunityId, Now(), _NumberOfRaves);
                
    
    UPDATE channel_stats SET number_of_diggs = (number_of_diggs + _NumberOfRaves) WHERE channel_id = _CommunityId;
    
    SELECT 1;
  COMMIT;
END
;;
DELIMITER ;