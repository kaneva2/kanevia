-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `asset_shares` (
  `share_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from_user_id` int(10) unsigned DEFAULT NULL,
  `asset_id` int(11) unsigned NOT NULL DEFAULT '0',
  `to_email` text,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `from_ip` varchar(20) DEFAULT NULL,
  `to_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`share_id`),
  KEY `IDX_created_date` (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `asset_views` (
  `view_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_address` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`view_id`),
  KEY `cv_hub` (`asset_id`),
  KEY `cid_user_id` (`asset_id`,`user_id`),
  KEY `cv_idx_1` (`created_date`,`asset_id`,`ip_address`),
  KEY `user` (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `audit_users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(22) NOT NULL DEFAULT '',
  `username_old` varchar(22) NOT NULL DEFAULT '',
  `display_name` varchar(30) NOT NULL DEFAULT '',
  `display_name_old` varchar(30) NOT NULL DEFAULT '',
  `zip_code` varchar(13) NOT NULL DEFAULT '0',
  `zip_code_old` varchar(13) NOT NULL DEFAULT '0',
  `country` char(2) NOT NULL DEFAULT '99',
  `country_old` char(2) NOT NULL DEFAULT '99',
  `location` varchar(80) NOT NULL DEFAULT '',
  `location_old` varchar(80) NOT NULL DEFAULT '',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `ui` (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `community_shares` (
  `share_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `sent_user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `community_id` int(11) unsigned NOT NULL DEFAULT '0',
  `email` text,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`share_id`)
) ENGINE=InnoDB;

CREATE TABLE `csr_logs` (
  `csr_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activity` text NOT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL COMMENT 'old_user_id',
  PRIMARY KEY (`csr_log_id`)
) ENGINE=InnoDB;

CREATE TABLE `forum_visits` (
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'User that visited forum',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `maintenance_log` (
  `maintenance_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `date_occurred` datetime NOT NULL COMMENT 'Time stamp of maintenance event',
  `database_name` varchar(50) DEFAULT NULL COMMENT 'The database the event used',
  `procedure_name` varchar(200) DEFAULT NULL COMMENT 'The procedure that performed the event',
  `message_text` varchar(500) DEFAULT NULL COMMENT 'The details of the maintenance event',
  PRIMARY KEY (`maintenance_log_id`)
) ENGINE=InnoDB COMMENT='Info about maintenance events to audit activity';

CREATE TABLE `schema_versions` (
  `version` decimal(11,4) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

CREATE TABLE `user_login_issues` (
  `user_login_issue_id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(50) NOT NULL DEFAULT '',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) DEFAULT NULL,
  `description` varchar(200) NOT NULL DEFAULT '',
  `error_type` varchar(45) DEFAULT NULL,
  `server_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_login_issue_id`)
) ENGINE=InnoDB;

CREATE TABLE `user_searches` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `search_string` varchar(100) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `search_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB;

