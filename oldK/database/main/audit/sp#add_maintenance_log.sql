-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_maintenance_log;

DELIMITER ;;
CREATE PROCEDURE `add_maintenance_log`(__database_name	varchar(50), __procedure_name varchar(200), __message_text varchar(500))
BEGIN
INSERT INTO maintenance_log (date_occurred, database_name, procedure_name, message_text)
	VALUES (NOW(), __database_name, __procedure_name, __message_text);
	
END
;;
DELIMITER ;