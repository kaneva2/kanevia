-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- add new developer table(s) 
-- schema initialization.  initializes schema_version.
--
\. kgp_volatile_schema.sql
