-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_inventory_item;

DELIMITER ;;
CREATE PROCEDURE `delete_inventory_item`(
  IN __player_id INT, 
	IN __global_id INT, 
	IN __inv_type enum('P','B','H'),		
	IN __inv_subType INT,
	IN __qty INT, 
	OUT __ret int
)
    COMMENT '\n    -- ok for ok, 2 if not found, 5 if not enough\n  '
this_proc:
BEGIN
  
  DECLARE __old_qty INT DEFAULT NULL;
  DECLARE __new_qty INT DEFAULT 1;

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- ERROR
      GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
      CALL logging.logMsg('ERROR', SCHEMA(), 'delete_inventory_item', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
      RESIGNAL;
  END;

  CALL logging.logMsg('TRACE', SCHEMA(), 'delete_inventory_item', CONCAT('BEGIN (__player_id[',CAST(__player_id AS CHAR),'] __global_id[',CAST(__global_id AS CHAR), '], __inv_type[', __inv_type, '] __inv_subType[',CAST(__inv_subType AS CHAR),'] __qty[',CAST(__qty AS CHAR),'])'));

	SET __ret = 0; -- ok

	SELECT 
    `quantity` INTO __old_qty
  FROM 
    `inventories`
  WHERE 
    `player_id`          = __player_id AND 
    `global_id`          = __global_id AND 
    `inventory_type`     = __inv_type  AND 
    `inventory_sub_type` = __inv_subType
  FOR UPDATE;

	IF __old_qty IS NOT NULL THEN 
		SET __new_qty = __old_qty - __qty;
		
		IF __new_qty = 0 THEN

			-- if using more than have, just remove all, server shouldn't let this happen
			DELETE FROM 
        `inventories`
			 WHERE 
        `player_id`          = __player_id AND 
        `global_id`          = __global_id AND 
        `inventory_type`     = __inv_type AND 
        `inventory_sub_type` = __inv_subType;

		ELSEIF __new_qty > 0 THEN 

			UPDATE 
        `inventories`
      SET 
        `quantity` = __new_qty,
				`last_touch_datetime` = NOW()
      WHERE 
        `player_id`          = __player_id   AND 
        `global_id`          = __global_id   AND 
        `inventory_type`     = __inv_type    AND 
        `inventory_sub_type` = __inv_subType;
		ELSE 
      -- must be < 0
      CALL logging.logMsg('WARN', SCHEMA(), 'delete_inventory_item', CONCAT('using more than have. player[', CAST(__player_id AS CHAR), '] glid[', CAST(__global_id AS CHAR), '] qty[', CAST( __qty AS CHAR), ']'));
			SET __ret = 5;	
		END IF;
	ELSE
    CALL logging.logMsg('WARN', SCHEMA(), 'delete_inventory_item', CONCAT('using item player doesnt have. player ', CAST(__player_id AS CHAR), '] glid[',CAST(__global_id AS CHAR), ' qty[', CAST(__qty AS CHAR), ']'));
		SET __ret = 2;
	END IF;


  CALL logging.logMsg('TRACE', SCHEMA(), 'delete_inventory_item', CONCAT('END (__player_id[',CAST(__player_id AS CHAR),'] __global_id[',CAST(__global_id AS CHAR), '], __inv_type[', __inv_type, '] __inv_subType[',CAST(__inv_subType AS CHAR),'] __qty[',CAST(__qty AS CHAR),'] __ret[',__ret,'])'));

END
;;
DELIMITER ;