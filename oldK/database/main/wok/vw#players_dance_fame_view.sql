-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS players_dance_fame_view;

DELIMITER ;;

 CREATE VIEW `players_dance_fame_view` AS SELECT `ps`.`player_id` AS `player_id`,`ps`.`current_level` AS `current_level`,`ps`.`current_experience` AS `current_experience` 
 FROM ( `skills` `s` 
 JOIN `player_skills` `ps`)  
 WHERE ( ( `s`.`skill_id` = `ps`.`skill_id`)  
 AND ( `s`.`name` = _latin1'Dance Fame') ) 
;;
DELIMITER ;