-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS logMsg;

DELIMITER ;;
CREATE PROCEDURE `logMsg`( IN procName VARCHAR(120), IN type ENUM( 'DBG', 'INFO', 'WARN', 'ERROR' ), IN message VARCHAR(500 ) )
BEGIN
	if (`type` = 'ERROR') THEN
    INSERT INTO sp_logs (caller, type, msg) VALUES (COALESCE(procName,'null caller'), type, COALESCE(message,'null msg'));
  END IF;
END
;;
DELIMITER ;