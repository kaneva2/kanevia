-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS fetch_image_url_and_mature;

DELIMITER ;;
CREATE PROCEDURE `fetch_image_url_and_mature`( IN __asset_id INT(11), IN __is_asset ENUM('T','F'),
											 OUT __texture_url varchar(1000), OUT __is_mature ENUM('Y','N') )
BEGIN											 
	-- get the url or an asset, and mature settings											 
	DECLARE __thumbnail_url VARCHAR(1000) DEFAULT NULL;
	DECLARE __file_size INT DEFAULT 0;
	IF __is_asset = 'T' THEN 
		SELECT image_full_path, thumbnail_xlarge_path, file_size, mature  INTO __texture_url, __thumbnail_url, __file_size, __is_mature
		  FROM kaneva.assets a
		 WHERE a.asset_id = __asset_id;
	ELSE 
		SELECT thumbnail_xlarge_path, IF(mature_profile=1,'Y','N')	INTO __texture_url, __is_mature
	      FROM kaneva.communities_personal cb 
	     INNER JOIN kaneva.users u on cb.creator_id = u.user_id
         WHERE cb.creator_id = __asset_id;
	END IF;
	IF __texture_url IS NULL THEN
		CALL logDebugMsg( 'fetch_image_url', concat( 'Couldnt get user image for asset: ', cast( __asset_id as char )  ) );
	ELSEIF 	__file_size	> 256000 THEN
		SET __texture_url =  __thumbnail_url;
	END IF;
	
END
;;
DELIMITER ;