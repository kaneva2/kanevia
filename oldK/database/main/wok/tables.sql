-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `access_type` (
  `access_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `access_type` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`access_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `apartment_cache` (
  `community_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `number_of_diggs` int(11) DEFAULT '0',
  `number_of_members` int(11) DEFAULT '0',
  `is_public` char(1) NOT NULL DEFAULT '',
  `is_adult` char(1) DEFAULT NULL,
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(64) NOT NULL COMMENT 'name shown in client',
  `zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'zoneIndex of their apt',
  `zone_instance_id` int(11) NOT NULL COMMENT 'FK to players',
  `count` bigint(11) NOT NULL DEFAULT '0',
  `wok_raves` bigint(11) NOT NULL DEFAULT '0',
  `creator_username` varchar(22) NOT NULL,
  `keywords` text,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `over_21_required` enum('Y','N') NOT NULL DEFAULT 'N',
  `pass_group_id` int(11) NOT NULL DEFAULT '0',
  KEY `comm_id` (`community_id`),
  KEY `name` (`name`(10)),
  KEY `descr` (`description`(10)),
  KEY `nd` (`number_of_diggs`),
  KEY `nm` (`number_of_members`),
  KEY `ip` (`is_public`),
  KEY `ia` (`is_adult`),
  KEY `cid` (`creator_id`),
  KEY `dn` (`display_name`(10)),
  KEY `zi` (`zone_index`),
  KEY `ziid` (`zone_instance_id`),
  KEY `c` (`count`),
  KEY `wr` (`wok_raves`),
  KEY `cun` (`creator_username`(10)),
  KEY `kw` (`keywords`(10)),
  KEY `st` (`status_id`),
  KEY `caid` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `apartment_templates` (
  `apartment_template_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Id for a specific apartment template',
  `name` varchar(256) DEFAULT NULL COMMENT 'Name of the apartment template',
  `description` text COMMENT 'Brief description of the apartment template',
  `image` varchar(256) DEFAULT NULL COMMENT 'Link to an image of the apartment template',
  PRIMARY KEY (`apartment_template_id`)
) ENGINE=InnoDB COMMENT='The template apartments the player can choose from';

CREATE TABLE `app_message_queue` (
  `id` varchar(50) NOT NULL COMMENT 'UUID for this db entry',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when db entry was made',
  `app_id` int(11) NOT NULL,
  `msg_type` varchar(64) NOT NULL COMMENT 'indicates type of message',
  `msg_source` text NOT NULL COMMENT 'url request to get message data',
  `client_response_id` varchar(50) DEFAULT NULL COMMENT 'client: UUID',
  `client_response_code` enum('none','success','failed','ignored','unknown') NOT NULL DEFAULT 'none',
  `client_processed_date` timestamp NULL DEFAULT NULL COMMENT 'client: when request was processed',
  `response_date` timestamp NULL DEFAULT NULL COMMENT 'when response entry was made',
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB;

CREATE TABLE `avatar_template` (
  `description` varchar(40) DEFAULT NULL,
  `file_name` varchar(40) DEFAULT NULL,
  `template_id` smallint(6) NOT NULL,
  `gender` char(1) DEFAULT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB;

CREATE TABLE `bad_trades` (
  `kaneva_user_id` int(1) NOT NULL DEFAULT '0',
  `trans_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'auto-inc PK',
  `player_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to players if on player',
  `trader_id` int(11) DEFAULT '0' COMMENT 'for trades, this will be who traded the item',
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `quantity_delta` int(11) NOT NULL DEFAULT '0' COMMENT 'change in quantity for this trans',
  `created_date` datetime NOT NULL
) ENGINE=InnoDB;

CREATE TABLE `balancer_inputs` (
  `server_id` int(10) unsigned NOT NULL COMMENT 'FK to servers',
  `zone_index` int(10) unsigned NOT NULL COMMENT 'zone id w/o instance',
  `zone_type` smallint(6) NOT NULL DEFAULT '0' COMMENT 'type (apt,channel,etc)',
  `zone_instance_id` int(10) unsigned NOT NULL COMMENT 'zone instance',
  `weight` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'current total in zone for server',
  PRIMARY KEY (`server_id`,`zone_instance_id`,`zone_index`),
  UNIQUE KEY `sap_zone_index_fields` (`zone_index`,`zone_type`,`zone_instance_id`)
) ENGINE=InnoDB COMMENT='table containing input data for the load balancing logic';

CREATE TABLE `bundle_items` (
  `bundle_global_id` int(11) NOT NULL DEFAULT '0' COMMENT 'bundle global id',
  `item_global_id` int(11) NOT NULL DEFAULT '0' COMMENT 'bundle item global id',
  `quantity` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'quantity of items',
  PRIMARY KEY (`bundle_global_id`,`item_global_id`),
  KEY `bi_item_global_id_fk` (`item_global_id`),
  CONSTRAINT `bi_bundle_global_id_fk` FOREIGN KEY (`bundle_global_id`) REFERENCES `items` (`global_id`),
  CONSTRAINT `bi_item_global_id_fk` FOREIGN KEY (`item_global_id`) REFERENCES `items` (`global_id`)
) ENGINE=InnoDB COMMENT='relation table for item bundles';

CREATE TABLE `catalogs_3d` (
  `catalog_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`catalog_id`)
) ENGINE=InnoDB COMMENT='catalog names for 3d gifts';

CREATE TABLE `channel_zones` (
  `channel_zone_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `kaneva_user_id` int(11) NOT NULL COMMENT 'owner. FK to gamer_users/kaneva.users',
  `zone_index` int(11) NOT NULL COMMENT 'index of zone, w/o instanceId',
  `zone_index_plain` int(11) NOT NULL COMMENT 'just the zone index FK to supported_worlds',
  `zone_type` int(11) NOT NULL DEFAULT '6' COMMENT 'type of zone ',
  `zone_instance_id` int(11) NOT NULL DEFAULT '0' COMMENT 'if zone is instanced, this is instanceId, usually Kaneva.community_id',
  `name` varchar(200) NOT NULL COMMENT 'zone name if Kaneva zone',
  `tied_to_server` enum('NOT_TIED','TIED','PERMANENTLY_TIED','PENDING_TIED') NOT NULL DEFAULT 'NOT_TIED' COMMENT 'used to control if server:port must be used',
  `country` char(2) NOT NULL DEFAULT 'US' COMMENT 'country code for filtering',
  `created_date` datetime NOT NULL,
  `raves` int(11) NOT NULL DEFAULT '0' COMMENT 'Raves for this place',
  `access_rights` int(11) NOT NULL DEFAULT '0' COMMENT 'Access rights to apartment or broadband zone',
  `access_friend_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'For access_rights equals friends(1), the friend group id',
  `server_id` int(11) NOT NULL COMMENT 'FK to current_game_engines.engine_id',
  `cover_charge` int(11) NOT NULL DEFAULT '0' COMMENT 'if non-zero amt charged to enter zone',
  `visibility` int(10) unsigned NOT NULL DEFAULT '100' COMMENT 'who sees this zone 0 = no one see or spawn, 50 = no one sees anyone spawn, 100 = everyone',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `number_unique_visits` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of unique Visits to the zone',
  `max_age` int(11) NOT NULL DEFAULT '100' COMMENT 'max age for players that can goto this zone',
  `script_server_id` int(11) NOT NULL,
  PRIMARY KEY (`channel_zone_id`),
  KEY `INDEX_cz_zoneindex` (`zone_index`),
  KEY `INDEX_cz_zoneinstance` (`zone_instance_id`),
  KEY `zone_type` (`zone_type`),
  KEY `newindex` (`kaneva_user_id`),
  KEY `IDX_zone_index` (`zone_index_plain`),
  KEY `cd` (`created_date`),
  KEY `tts` (`tied_to_server`),
  KEY `server_id` (`server_id`),
  KEY `zi_ziid_tts` (`zone_index`,`zone_instance_id`,`tied_to_server`),
  KEY `IDX_zonetype_raves` (`zone_type`,`raves`),
  KEY `IDX_type_instance` (`zone_type`,`zone_instance_id`),
  CONSTRAINT `cz_zone_type_fk` FOREIGN KEY (`zone_type`) REFERENCES `channel_zone_types` (`zone_type`)
) ENGINE=InnoDB COMMENT='Kaneva and channel zones';

CREATE TABLE `channel_zone_cover_charges` (
  `kaneva_user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `zone_index` int(11) NOT NULL COMMENT 'FK to channel_zones',
  `zone_instance_id` int(11) NOT NULL COMMENT 'FK to channel_zones',
  `expiration_date` datetime NOT NULL COMMENT 'when this cover charge expires',
  `created_date` datetime NOT NULL COMMENT 'when this cover charge was purchases',
  PRIMARY KEY (`kaneva_user_id`,`zone_index`,`zone_instance_id`),
  KEY `kaneva_user_id` (`kaneva_user_id`,`zone_index`,`zone_instance_id`,`expiration_date`)
) ENGINE=InnoDB COMMENT='track cover charges for players and zones';

CREATE TABLE `channel_zone_types` (
  `zone_type` int(11) NOT NULL COMMENT 'channel_zone type id number.',
  `name` varchar(50) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`zone_type`)
) ENGINE=InnoDB COMMENT='Type lookup for channel_zones.zone_type.';

CREATE TABLE `clans` (
  `clan_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clan_name` varchar(35) NOT NULL DEFAULT ' ',
  `owner_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to player_id',
  `housing_placement_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to housing_placements',
  `arena_points` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`clan_id`),
  UNIQUE KEY `owner_id` (`owner_id`),
  UNIQUE KEY `clan_name` (`clan_name`)
) ENGINE=InnoDB COMMENT='base clan table';

CREATE TABLE `clan_members` (
  `clan_id` int(11) unsigned NOT NULL DEFAULT '0',
  `player_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`player_id`),
  KEY `clan_id` (`clan_id`),
  CONSTRAINT `clan_members_clan_id_fk` FOREIGN KEY (`clan_id`) REFERENCES `clans` (`clan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='link players to clans';

CREATE TABLE `cod_items` (
  `cod_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `to_id` int(11) NOT NULL COMMENT 'denormalized from messages',
  `message_id` int(11) NOT NULL COMMENT 'FK to kaneva.messages',
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `status` enum('U','A','R') NOT NULL DEFAULT 'U' COMMENT 'Unaccpeted, Accepted, Rejected',
  `amount` int(11) NOT NULL DEFAULT '0' COMMENT 'amount requested for items',
  `quantity` int(11) NOT NULL DEFAULT '1' COMMENT 'number of items being given',
  `sent_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when the item was sent',
  PRIMARY KEY (`cod_id`)
) ENGINE=InnoDB COMMENT='M2M between wok.items and kaneva.messages for tracking C.O.D';

CREATE TABLE `colors` (
  `color_type` tinyint(3) unsigned NOT NULL COMMENT 'This is the color category',
  `color_name` varchar(255) DEFAULT NULL COMMENT 'Name of the color',
  `color_description` varchar(255) DEFAULT NULL COMMENT 'Describes what the color is used for',
  `color_r` tinyint(3) unsigned NOT NULL DEFAULT '255' COMMENT 'Red color component',
  `color_g` tinyint(3) unsigned NOT NULL DEFAULT '255' COMMENT 'Green color component',
  `color_b` tinyint(3) unsigned NOT NULL DEFAULT '255' COMMENT 'Blue color component',
  PRIMARY KEY (`color_type`)
) ENGINE=InnoDB COMMENT='The red, green, blue values for colors';

CREATE TABLE `combined_hangouts_3dapps_cache` (
  `stpurl` varchar(300) DEFAULT NULL,
  `game_id` int(10) DEFAULT NULL,
  `community_id` int(11) DEFAULT NULL,
  `zone_type` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_public` char(1) DEFAULT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `keywords` text,
  `over_21_required` enum('Y','N') DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `username` varchar(22) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `population` int(11) DEFAULT NULL,
  `name_no_spaces` varchar(100) DEFAULT NULL,
  `thumbnail_small_path` varchar(255) DEFAULT NULL,
  `creator_name_no_spaces` varchar(100) DEFAULT NULL,
  `creator_thumbnail_small_path` varchar(255) DEFAULT NULL,
  `number_of_members` int(11) DEFAULT NULL,
  `number_of_views` int(11) DEFAULT NULL,
  `number_of_times_shared` int(11) DEFAULT NULL,
  `number_of_diggs` int(11) DEFAULT NULL,
  KEY `i1` (`name`),
  KEY `i2` (`description`),
  KEY `i3` (`keywords`(10)),
  KEY `i4` (`username`),
  KEY `i5` (`is_adult`),
  KEY `i6` (`creator_id`),
  KEY `i7` (`zone_type`),
  KEY `i8` (`population`),
  KEY `i9` (`number_of_views`),
  KEY `i10` (`number_of_diggs`),
  KEY `i11` (`number_of_members`)
) ENGINE=InnoDB;

CREATE TABLE `currency_type` (
  `currency_type_id` smallint(5) unsigned NOT NULL,
  `currency_type` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`currency_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `custom_deed_usage` (
  `zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'index of zone without the instanceId',
  `zone_instance_id` int(11) NOT NULL DEFAULT '1' COMMENT 'instance id of a particular zone',
  `apartment_template_id` int(11) NOT NULL DEFAULT '0' COMMENT 'globalId of the deed used for this space',
  PRIMARY KEY (`zone_index`,`zone_instance_id`),
  CONSTRAINT `custom_deed_usage_ibfk_1` FOREIGN KEY (`zone_index`, `zone_instance_id`) REFERENCES `channel_zones` (`zone_index`, `zone_instance_id`)
) ENGINE=InnoDB COMMENT='Records which zones have custom deeds applied';

CREATE TABLE `daily_dance_levels` (
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'User that danced to level',
  `level_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Level danced to',
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`user_id`,`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `dance_level_actions` (
  `dance_level_action_id` int(11) NOT NULL AUTO_INCREMENT,
  `dance_level` int(11) NOT NULL DEFAULT '0',
  `action_id` int(11) NOT NULL DEFAULT '0',
  `perfect_action_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`dance_level_action_id`)
) ENGINE=InnoDB;

CREATE TABLE `ddr` (
  `game_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `seed` int(11) NOT NULL,
  `num_seconds_to_play` int(11) NOT NULL,
  `num_moves` int(11) NOT NULL,
  `num_misses` int(11) NOT NULL,
  `perfect_score` int(11) NOT NULL,
  `great_score` int(11) NOT NULL,
  `good_score` int(11) NOT NULL,
  `finish_bonus_score` int(11) NOT NULL,
  `level_increase_score` int(11) NOT NULL,
  `level_bonus_trigger_num` int(11) NOT NULL,
  `level_bonus_score` int(11) NOT NULL,
  `perfect_score_ceiling` int(11) NOT NULL,
  `great_score_ceiling` int(11) NOT NULL,
  `good_score_ceiling` int(11) NOT NULL,
  `num_sub_levels` int(11) NOT NULL,
  `last_level_reduction_ms` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`game_id`,`level_id`)
) ENGINE=InnoDB;

CREATE TABLE `ddr_animations` (
  `game_id` int(11) NOT NULL,
  `type` enum('FLARE','FINAL_FLARE','MISS','LEVELUP','PERFECT','IDLE') NOT NULL,
  `level_id` int(11) NOT NULL,
  `animation_id` int(11) NOT NULL,
  `play_order` int(11) NOT NULL,
  PRIMARY KEY (`game_id`,`level_id`,`animation_id`)
) ENGINE=InnoDB;

CREATE TABLE `ddr_globals` (
  `game_id` int(11) NOT NULL,
  `max_players` int(11) NOT NULL,
  `notify_inactive` int(11) NOT NULL,
  `purge_timeout` int(11) NOT NULL,
  PRIMARY KEY (`game_id`)
) ENGINE=InnoDB;

CREATE TABLE `ddr_high_scores` (
  `dynamic_object_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `high_score` int(11) NOT NULL,
  `player_name` varchar(64) NOT NULL,
  `created_date` datetime NOT NULL,
  `object_id` int(11) NOT NULL DEFAULT '0',
  `zone_index` int(11) NOT NULL DEFAULT '0',
  KEY `doi` (`dynamic_object_id`),
  KEY `oz` (`object_id`,`zone_index`)
) ENGINE=InnoDB;

CREATE TABLE `ddr_high_scores_archive` (
  `dynamic_object_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `high_score` int(11) NOT NULL,
  `player_name` varchar(64) NOT NULL,
  `created_date` datetime NOT NULL,
  `archive_date` datetime NOT NULL,
  `object_id` int(11) NOT NULL DEFAULT '0',
  `zone_index` int(11) NOT NULL DEFAULT '0',
  `dhsa_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`dhsa_id`),
  KEY `doi` (`dynamic_object_id`),
  KEY `object_id` (`object_id`,`zone_index`),
  KEY `oz` (`object_id`,`zone_index`)
) ENGINE=InnoDB;

CREATE TABLE `ddr_scores` (
  `game_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `instance_id` int(11) NOT NULL AUTO_INCREMENT,
  `score` int(11) NOT NULL,
  `num_perfects` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `num_misses` int(11) NOT NULL DEFAULT '0',
  `num_perfects_inarow` int(11) NOT NULL,
  `player_name` varchar(64) NOT NULL,
  `dynamic_object_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL DEFAULT '0',
  `zone_index` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`instance_id`),
  KEY `gpdoz` (`game_id`,`player_id`,`dynamic_object_id`,`object_id`,`zone_index`),
  KEY `IDX_created_date` (`created_date`),
  KEY `IDX_player_id` (`player_id`)
) ENGINE=InnoDB;

CREATE TABLE `ddr_unique_visits` (
  `kaneva_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'User that visited dance floor',
  `dynamic_object_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to dynamic_objects.obj_placement_id',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`kaneva_user_id`,`dynamic_object_id`),
  KEY `IDX_dynamic_object_id` (`dynamic_object_id`)
) ENGINE=InnoDB;

CREATE TABLE `default_home_template` (
  `default_key` varchar(10) NOT NULL DEFAULT 'default',
  `template_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`default_key`),
  KEY `wtd_template_id_fk` (`template_id`),
  CONSTRAINT `wtd_template_id_fk` FOREIGN KEY (`template_id`) REFERENCES `world_templates` (`template_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Default world template used when creating home worlds.';

CREATE TABLE `default_script_game_custom_data` (
  `attribute` varchar(255) NOT NULL,
  `value` text,
  PRIMARY KEY (`attribute`)
) ENGINE=InnoDB COMMENT='Default values for script game custom data, used when gaming is activated in a world.';

CREATE TABLE `default_script_game_items` (
  `game_item_id` int(11) NOT NULL,
  `game_item_glid` int(11) NOT NULL,
  `automatic_insertion_start` datetime DEFAULT NULL COMMENT 'Point in time after which this item should be automatically added to the user''s WoK inventory/game systems.',
  `automatic_insertion_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`game_item_id`,`game_item_glid`),
  UNIQUE KEY `game_item_id_UNIQUE` (`game_item_id`),
  KEY `dsgi_giglid_fk_idx` (`game_item_glid`),
  KEY `dsgi_automatic_insertion_start_idx` (`automatic_insertion_start`),
  CONSTRAINT `dsgi_giglid_fk` FOREIGN KEY (`game_item_glid`) REFERENCES `snapshot_script_game_items` (`game_item_glid`)
) ENGINE=InnoDB COMMENT='List of default, Kaneva-owned script game items.';

CREATE TABLE `default_script_game_item_recipients` (
  `user_id` int(11) NOT NULL,
  `date_last_received_defaults` datetime NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_dsgir_user_id` FOREIGN KEY (`user_id`) REFERENCES `kaneva`.`users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='List of users who have received the default set of script game items, and the datetime at which they last received the default set.';

CREATE TABLE `deformable_configs` (
  `sequence` int(11) NOT NULL COMMENT 'sort key to key cfgs in order',
  `player_id` int(11) NOT NULL COMMENT 'player id',
  `equip_id` int(11) NOT NULL DEFAULT '0' COMMENT 'zero for main body, else id of attached item',
  `deformable_config` int(11) NOT NULL COMMENT 'config id from the skeletal object',
  `custom_material` int(11) NOT NULL COMMENT 'customizable material',
  `custom_color_r` float NOT NULL COMMENT 'color of material',
  `custom_color_g` float NOT NULL,
  `custom_color_b` float NOT NULL,
  PRIMARY KEY (`player_id`,`sequence`)
) ENGINE=InnoDB COMMENT='players'' deformable configs';

CREATE TABLE `deformable_configs_inactive` (
  `sequence` int(11) NOT NULL COMMENT 'sort key to key cfgs in order',
  `player_id` int(11) NOT NULL COMMENT 'player id',
  `equip_id` int(11) NOT NULL DEFAULT '0' COMMENT 'zero for main body, else id of attached item',
  `deformable_config` int(11) NOT NULL COMMENT 'config id from the skeletal object',
  `custom_material` int(11) NOT NULL COMMENT 'customizable material',
  `custom_color_r` float NOT NULL COMMENT 'color of material',
  `custom_color_g` float NOT NULL,
  `custom_color_b` float NOT NULL,
  KEY `INDEX_defconfig_player` (`player_id`),
  KEY `INDEX_defconfig_seq` (`sequence`)
) ENGINE=InnoDB COMMENT='players'' deformable configs';

CREATE TABLE `dx_diag` (
  `diag_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '1234',
  `diag_date` datetime NOT NULL COMMENT '2007-07-06 00:00:00',
  `user_id` int(10) unsigned NOT NULL COMMENT '1234',
  `operating_system` varchar(60) NOT NULL COMMENT 'Microsoft Windows XP Pro version 5.1',
  `make` varchar(45) NOT NULL COMMENT 'Dell Inc.',
  `model` varchar(60) NOT NULL COMMENT 'Precision M65',
  `bios` varchar(80) NOT NULL COMMENT 'Phoenix RoM Bios PLUS Version 1.10 A07',
  `proc` varchar(60) NOT NULL COMMENT 'Intel(R) Core(TM)2 CPU T7600 @ 2.33Ghz (2CPUs)',
  `memory` varchar(15) NOT NULL COMMENT '2046MB RAM',
  `directx_version` varchar(45) NOT NULL COMMENT 'DirectX 9.0c (4.09.0000.0904)',
  `card_name` varchar(60) NOT NULL COMMENT 'NVIDIA Quadro FX 350M',
  `card_make` varchar(45) NOT NULL COMMENT 'NVIDIA',
  `card_chip_type` varchar(45) NOT NULL COMMENT 'Quadro FX 350M',
  `dac_type` varchar(45) NOT NULL COMMENT 'Integrated RAMDAC',
  `card_memory` varchar(15) NOT NULL COMMENT '512.0 MB',
  `display_mode` varchar(45) NOT NULL COMMENT '1650x1050 (32bit) (60Hz)',
  `downloaded_size` bigint(20) unsigned NOT NULL DEFAULT '0',
  `elapsed_time` bigint(20) unsigned NOT NULL DEFAULT '0',
  `download_completed` enum('Y','N') NOT NULL DEFAULT 'N',
  `driver_version` varchar(45) DEFAULT NULL COMMENT 'Driver 6.14.0010.6587 English',
  `driver_date_size` varchar(45) DEFAULT NULL COMMENT 'Driver Date Size',
  PRIMARY KEY (`diag_id`),
  KEY `uid` (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `dynamic_objects` (
  `obj_placement_id` int(11) NOT NULL COMMENT 'unique id sent to client',
  `player_id` int(11) NOT NULL DEFAULT '0' COMMENT 'player setting it',
  `zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'zone where set.  If housing, instance Id will have to be reset',
  `zone_instance_id` int(11) NOT NULL DEFAULT '0' COMMENT 'instance id of zone, if instanced',
  `object_id` int(11) NOT NULL DEFAULT '0' COMMENT 'id of dynamic object (e.g. couch)',
  `global_id` int(11) NOT NULL DEFAULT '0' COMMENT 'global inventory id of item that created this',
  `inventory_sub_type` int(11) NOT NULL DEFAULT '256' COMMENT 'mature = 1024, gift = 512, normal = 256',
  `position_x` float NOT NULL DEFAULT '0' COMMENT 'position in zone',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `rotation_x` float NOT NULL DEFAULT '0' COMMENT 'rotatation',
  `rotation_y` float NOT NULL DEFAULT '0',
  `rotation_z` float NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired_date` datetime DEFAULT NULL COMMENT 'for try on, when this item expires',
  `slide_x` float DEFAULT '0',
  `slide_y` float DEFAULT '0',
  `slide_z` float DEFAULT '0',
  PRIMARY KEY (`obj_placement_id`),
  KEY `INDEX_do_player` (`player_id`),
  KEY `zinid_zid` (`zone_instance_id`,`zone_index`) USING BTREE,
  KEY `IDX_created_date` (`created_date`),
  KEY `IDX_glid_agi_zi_zid` (`global_id`,`zone_index`,`zone_instance_id`),
  KEY `IDX_expired_date` (`expired_date`)
) ENGINE=InnoDB COMMENT='placement info for dynamic objects';

CREATE TABLE `dynamic_objects_inactive` (
  `obj_placement_id` int(11) NOT NULL COMMENT 'unique id sent to client',
  `player_id` int(11) NOT NULL DEFAULT '0' COMMENT 'player setting it',
  `zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'zone where set.  If housing, instance Id will have to be reset',
  `zone_instance_id` int(11) NOT NULL DEFAULT '0' COMMENT 'instance id of zone, if instanced',
  `object_id` int(11) NOT NULL DEFAULT '0' COMMENT 'id of dynamic object (e.g. couch)',
  `global_id` int(11) NOT NULL DEFAULT '0' COMMENT 'global inventory id of item that created this',
  `inventory_sub_type` int(11) NOT NULL DEFAULT '256' COMMENT 'mature = 1024, gift = 512, normal = 256',
  `position_x` float NOT NULL DEFAULT '0' COMMENT 'position in zone',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `rotation_x` float NOT NULL DEFAULT '0' COMMENT 'rotatation',
  `rotation_y` float NOT NULL DEFAULT '0',
  `rotation_z` float NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired_date` datetime DEFAULT NULL COMMENT 'for try on, when this item expires',
  `slide_x` float DEFAULT '0',
  `slide_y` float DEFAULT '0',
  `slide_z` float DEFAULT '0',
  PRIMARY KEY (`obj_placement_id`),
  KEY `INDEX_do_player` (`player_id`)
) ENGINE=InnoDB COMMENT='placement info for dynamic objects';

CREATE TABLE `dynamic_object_parameters` (
  `obj_placement_id` int(11) NOT NULL COMMENT 'FK to dynamic_objects',
  `param_type_id` int(11) NOT NULL COMMENT 'FK to parameters ',
  `VALUE` varchar(1000) NOT NULL DEFAULT '' COMMENT 'arbitrary value, meaning depends on type id',
  PRIMARY KEY (`obj_placement_id`,`param_type_id`)
) ENGINE=InnoDB COMMENT='parameters for placed objects';

CREATE TABLE `dynamic_object_parameters_inactive` (
  `obj_placement_id` int(11) NOT NULL COMMENT 'FK to dynamic_objects',
  `param_type_id` int(11) NOT NULL COMMENT 'FK to parameters ',
  `VALUE` varchar(1000) NOT NULL DEFAULT '' COMMENT 'arbitrary value, meaning depends on type id',
  PRIMARY KEY (`obj_placement_id`,`param_type_id`)
) ENGINE=InnoDB COMMENT='parameters for inactive placed objects';

CREATE TABLE `dynamic_object_playlists` (
  `zone_index` int(11) NOT NULL COMMENT 'zone of object',
  `zone_instance_id` int(11) NOT NULL COMMENT 'zone of object',
  `global_id` int(11) NOT NULL COMMENT 'glid of item',
  `obj_placement_id` int(11) NOT NULL COMMENT 'FK to dyn_obj, may have more in zone',
  `asset_group_id` int(11) NOT NULL COMMENT 'playlist id FK to kaneva.asset_groups',
  `video_range` float NOT NULL DEFAULT '0',
  `audio_range` float NOT NULL DEFAULT '0',
  `volume` float NOT NULL DEFAULT '100',
  PRIMARY KEY (`zone_index`,`zone_instance_id`,`obj_placement_id`),
  KEY `asset_group_id` (`asset_group_id`)
) ENGINE=InnoDB COMMENT='tracks one row per dyn object glid per zone';

CREATE TABLE `error_log` (
  `error_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `error_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'error_time is local user time',
  `error_code` int(11) NOT NULL DEFAULT '0',
  `error_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL DEFAULT '',
  `report_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`error_id`),
  KEY `idx_error_type` (`error_type_id`),
  KEY `ind_errorCode` (`error_code`),
  KEY `error_log_user_idx` (`user_id`),
  KEY `error_log_report_time_idx` (`report_time`)
) ENGINE=InnoDB;

CREATE TABLE `error_types` (
  `error_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `error_type` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`error_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `event_participation` (
  `event_date` date NOT NULL,
  `date_created` datetime NOT NULL,
  `kaneva_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`event_date`,`kaneva_user_id`),
  KEY `ukey` (`date_created`,`kaneva_user_id`)
) ENGINE=InnoDB;

CREATE TABLE `game_inventories_sync` (
  `game_id` int(11) NOT NULL COMMENT 'FK to developer.games',
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `inventory_type` enum('P','B','H') NOT NULL DEFAULT 'P' COMMENT 'type of inventory this is first letter of: Player/Bank/Housing',
  `inventory_sub_type` int(11) NOT NULL DEFAULT '256' COMMENT 'mature = 1024, gift = 512, normal = 256',
  `kaneva_user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to user_id in users table',
  `quantity` int(11) NOT NULL DEFAULT '0' COMMENT 'represents the quantity available for the player to use',
  `quantity_purchased` int(11) NOT NULL DEFAULT '0' COMMENT 'only WOK will able to modify this field',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`game_id`,`kaneva_user_id`,`inventory_type`,`global_id`,`inventory_sub_type`)
) ENGINE=InnoDB COMMENT='holds player inventory data specific to child games';

CREATE TABLE `game_items` (
  `game_id` int(11) NOT NULL COMMENT 'FK to developer.games',
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `approval_status` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT 'Pending = 0, Requested = 1, Approved = 2, Denied = 3',
  `last_update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`game_id`,`global_id`),
  KEY `gi_global_id_idx` (`global_id`)
) ENGINE=InnoDB COMMENT='holds the definition of what items belong to a game';

CREATE TABLE `game_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique id',
  `kaneva_user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Their Id in Kaneva (Kenava.users.user_id)',
  `username` varchar(80) NOT NULL COMMENT 'from Kaneva.users',
  `is_gm` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'game master (T/F)',
  `can_spawn` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'can spawn AI (T/F)',
  `is_admin` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'play is an admin (T/F)',
  `time_played` int(11) NOT NULL DEFAULT '0' COMMENT 'number of minutes played',
  `last_logon` timestamp NULL DEFAULT NULL COMMENT 'when they last logged onto the game',
  `second_to_last_logon` timestamp NULL DEFAULT NULL COMMENT 'second to last logon',
  `server_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to Kaneva current engines',
  `last_update_time` datetime NOT NULL COMMENT 'used to sync to game server',
  `logged_on` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'currently logged on (T/F)',
  `last_ip` varchar(30) NOT NULL DEFAULT '' COMMENT 'IP address from last logon',
  `created_date` datetime NOT NULL DEFAULT '2007-01-01 00:00:00' COMMENT 'when user first logged in',
  `login_count` int(11) DEFAULT '0' COMMENT 'Total number of logins',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  KEY `INDEX_users_logged_on` (`logged_on`),
  KEY `INDEX_users_kaneva_user_id` (`kaneva_user_id`),
  KEY `INDEX_users_serverId` (`server_id`),
  KEY `IDX_last_logon` (`last_logon`)
) ENGINE=InnoDB COMMENT='Kaneva users who have used the game';

CREATE TABLE `game_users_backup` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique id',
  `kaneva_user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Their Id in Kaneva (Kenava.users.user_id)',
  `username` varchar(80) NOT NULL COMMENT 'from Kaneva.users',
  `is_gm` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'game master (T/F)',
  `can_spawn` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'can spawn AI (T/F)',
  `is_admin` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'play is an admin (T/F)',
  `time_played` int(11) NOT NULL DEFAULT '0' COMMENT 'number of minutes played',
  `last_logon` timestamp NULL DEFAULT NULL COMMENT 'when they last logged onto the game',
  `server_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to Kaneva current engines',
  `last_update_time` datetime NOT NULL COMMENT 'used to sync to game server',
  `logged_on` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'currently logged on (T/F)',
  `last_ip` varchar(30) NOT NULL DEFAULT '' COMMENT 'IP address from last logon',
  `created_date` datetime NOT NULL DEFAULT '2007-01-01 00:00:00' COMMENT 'when user first logged in',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  KEY `INDEX_users_logged_on` (`logged_on`),
  KEY `INDEX_users_kaneva_user_id` (`kaneva_user_id`),
  KEY `INDEX_users_serverId` (`server_id`)
) ENGINE=InnoDB COMMENT='Kaneva users who have used the game';

CREATE TABLE `getset_player_values` (
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `name` char(64) NOT NULL COMMENT 'name of new value',
  `type` int(11) NOT NULL COMMENT 'type int, double, string (8/14/15)',
  `int_value` int(11) DEFAULT NULL,
  `double_value` double DEFAULT NULL,
  `string_value` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`player_id`,`name`),
  KEY `INDEX_gs_player` (`player_id`)
) ENGINE=InnoDB COMMENT='dyanmically added GetSet values';

CREATE TABLE `getset_server_config_values` (
  `server_name` varchar(128) NOT NULL DEFAULT '' COMMENT 'Hostname of the server.  Blank is for all server of that type.',
  `engine_type` int(11) NOT NULL COMMENT 'ServerEngine, DispatcherServer, TellServer, etc.',
  `server_instance_id` int(11) NOT NULL DEFAULT '1' COMMENT 'Instance ID of a particular server to allow > 1 server on one box',
  `engine_instance_id` int(11) NOT NULL DEFAULT '1' COMMENT 'instance id of engine within a server (instance_id)',
  `value_name` char(64) NOT NULL COMMENT 'name of new value',
  `type` int(11) NOT NULL COMMENT 'type int, double, string (8/14/15)',
  `int_value` int(11) DEFAULT NULL,
  `double_value` double DEFAULT NULL,
  `string_value` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`server_name`,`engine_type`,`server_instance_id`,`engine_instance_id`,`value_name`)
) ENGINE=InnoDB COMMENT='Server config values';

CREATE TABLE `getset_type_names` (
  `type` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB COMMENT='lookup table for getset types';

CREATE TABLE `gift_catalog_3d_items` (
  `gift_id` int(11) unsigned NOT NULL COMMENT 'FK to global_id in items table and gift_id in msg_gifts',
  `catalog_id` int(11) unsigned NOT NULL COMMENT 'catalog id for segmenting gifts per NPC, etc.',
  `name` varchar(125) NOT NULL COMMENT 'short name of gift for lists',
  `description` varchar(300) NOT NULL,
  `price` int(11) unsigned NOT NULL DEFAULT '1' COMMENT 'gift price in KPts',
  `avail_begin_date` datetime NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT 'begining date when item appears in the catalog',
  `avail_end_date` datetime NOT NULL DEFAULT '2020-01-01 00:00:00' COMMENT 'corresponding end date',
  `gifted_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'number of times this was given',
  PRIMARY KEY (`gift_id`),
  KEY `INDEX_gift_catalog` (`catalog_id`),
  KEY `INDEX_gift_name` (`name`),
  KEY `INDEX_gift_avail` (`avail_begin_date`,`avail_end_date`),
  KEY `INDEX_gift_count` (`gifted_count`)
) ENGINE=InnoDB COMMENT='items for 3D gifting';

CREATE TABLE `hangout_cache` (
  `display_name` varchar(64) NOT NULL COMMENT 'name shown in client',
  `community_id` int(11) NOT NULL DEFAULT '0',
  `place_type_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `number_of_members` int(11) DEFAULT '0',
  `is_public` char(1) NOT NULL DEFAULT '',
  `is_adult` char(1) DEFAULT NULL,
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(22) NOT NULL,
  `name_no_spaces` varchar(100) NOT NULL,
  `thumbnail_small_path` varchar(255) NOT NULL DEFAULT '',
  `creator_thumbnail_small_path` varchar(255) NOT NULL DEFAULT '',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `number_of_diggs` int(11) DEFAULT '0',
  `zone_index` int(11) DEFAULT NULL,
  `zone_instance_id` int(11) NOT NULL DEFAULT '0',
  `wok_raves` bigint(11) NOT NULL DEFAULT '0',
  `count` bigint(11) NOT NULL DEFAULT '0',
  `keywords` text,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `over_21_required` enum('Y','N') NOT NULL DEFAULT 'N',
  `pass_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`community_id`),
  KEY `dn` (`display_name`(10)),
  KEY `n` (`name`(10)),
  KEY `de` (`description`(10)),
  KEY `nm` (`number_of_members`),
  KEY `ip` (`is_public`),
  KEY `pt` (`place_type_id`),
  KEY `cid` (`creator_id`),
  KEY `un` (`username`(10)),
  KEY `cd` (`created_date`),
  KEY `nd` (`number_of_diggs`),
  KEY `zi` (`zone_index`),
  KEY `ziid` (`zone_instance_id`),
  KEY `wr` (`wok_raves`),
  KEY `c` (`count`),
  KEY `kw` (`keywords`(10)),
  KEY `sid` (`status_id`),
  KEY `caid` (`category_id`),
  KEY `ia` (`is_adult`),
  KEY `pgid` (`pass_group_id`)
) ENGINE=InnoDB ROW_FORMAT=FIXED;

CREATE TABLE `housing_placements` (
  `housing_placement_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL COMMENT 'ref to housing type',
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `position_x` float NOT NULL COMMENT 'where is this house in the zone',
  `position_y` float NOT NULL,
  `position_z` float NOT NULL,
  `is_open` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'T/F',
  `has_vault` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'T/F',
  `vault_item_capacity` int(11) NOT NULL COMMENT 'max items ',
  `vault_current_cash` int(11) NOT NULL COMMENT 'current cash amount in vault',
  `store_item_capacity` int(11) NOT NULL COMMENT 'max items',
  `store_current_cash` int(11) NOT NULL COMMENT 'current store cash',
  PRIMARY KEY (`housing_placement_id`)
) ENGINE=InnoDB;

CREATE TABLE `inactive_audit_log` (
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `when_added` datetime NOT NULL COMMENT 'timestamp of activity',
  `last_logon` datetime NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT 'If inactivating, when was the second to last logon',
  `action_type` enum('A','I') NOT NULL COMMENT 'Activating or Inactivating data',
  PRIMARY KEY (`player_id`,`when_added`)
) ENGINE=InnoDB COMMENT='audit log for activating and inactivating player data';

CREATE TABLE `inventories` (
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `inventory_type` enum('P','B','H') NOT NULL DEFAULT 'P' COMMENT 'type of inventory this is first letter of: Player/Bank/Housing',
  `inventory_sub_type` int(11) NOT NULL DEFAULT '256' COMMENT 'mature = 1024, gift = 512, normal = 256',
  `player_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to players if on player',
  `armed` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'is item currently armed (T/F)',
  `quantity` int(11) NOT NULL DEFAULT '0' COMMENT 'how many of this item',
  `charge_quantity` int(11) NOT NULL DEFAULT '0' COMMENT 'if ammo, amount of ammo',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_touch_datetime` datetime DEFAULT NULL COMMENT 'to track item placement or pickup',
  PRIMARY KEY (`player_id`,`inventory_type`,`global_id`,`inventory_sub_type`),
  KEY `INDEX_inv_player` (`player_id`),
  KEY `INDEX_inv_type` (`inventory_type`),
  KEY `player_id` (`player_id`),
  KEY `global_id` (`global_id`)
) ENGINE=InnoDB;

CREATE TABLE `inventories_inactive` (
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `inventory_type` enum('P','B','H') NOT NULL DEFAULT 'P' COMMENT 'type of inventory this is first letter of: Player/Bank/Housing',
  `inventory_sub_type` int(11) NOT NULL DEFAULT '256' COMMENT 'mature = 1024, gift = 512, normal = 256',
  `player_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to players if on player',
  `armed` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'is item currently armed (T/F)',
  `quantity` int(11) NOT NULL DEFAULT '0' COMMENT 'how many of this item',
  `charge_quantity` int(11) NOT NULL DEFAULT '0' COMMENT 'if ammo, amount of ammo',
  `gift_giver` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to players if a gift',
  `corpse_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to corpse if inventory on corpse',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`player_id`,`inventory_type`,`global_id`,`inventory_sub_type`),
  KEY `INDEX_inv_player` (`player_id`)
) ENGINE=InnoDB;

CREATE TABLE `inventories_transaction_log` (
  `trans_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'auto-inc PK',
  `created_date` datetime NOT NULL,
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `inventory_type` enum('P','B','H') NOT NULL DEFAULT 'P' COMMENT 'type of inventory this is first letter of: Player/Bank/Housing',
  `inventory_sub_type` int(11) NOT NULL DEFAULT '256' COMMENT 'mature = 1024, gift = 512, normal = 256',
  `player_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to players if on player',
  `armed` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'is item currently armed (T/F)',
  `quantity_delta` int(11) NOT NULL DEFAULT '0' COMMENT 'change in quantity for this trans',
  `quantity_new` int(11) NOT NULL DEFAULT '0' COMMENT 'quanity after this action',
  `charge_quantity` int(11) NOT NULL DEFAULT '0' COMMENT 'if ammo, amount of ammo',
  `trader_id` int(11) DEFAULT '0' COMMENT 'for trades, this will be who traded the item',
  PRIMARY KEY (`trans_id`),
  KEY `INDEX_inv_player` (`player_id`),
  KEY `INDEX_inv_type` (`inventory_type`),
  KEY `IDX_created_date` (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `inventory_pending_adds2` (
  `kaneva_user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'soft reference kaneva.users,game_users',
  `global_id` int(11) NOT NULL COMMENT 'soft reference items',
  `inventory_type` char(1) NOT NULL DEFAULT 'P' COMMENT 'type of inventory this is first letter of: Player/Bank/Housing/Return',
  `inventory_sub_type` int(11) NOT NULL DEFAULT '256' COMMENT 'normal = 256, gift = 512, infinite = 2048',
  `quantity` int(11) NOT NULL DEFAULT '0' COMMENT 'how many of this item',
  `last_touch_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`kaneva_user_id`,`global_id`,`inventory_sub_type`)
) ENGINE=InnoDB COMMENT='items that need to be put back into a users inventory';

CREATE TABLE `items` (
  `global_id` int(11) NOT NULL COMMENT 'global inventory id',
  `name` varchar(125) NOT NULL COMMENT 'inventory item''s name',
  `description` varchar(200) NOT NULL DEFAULT '',
  `market_cost` int(11) NOT NULL COMMENT 'cost to buy, in game credits',
  `selling_price` int(11) NOT NULL COMMENT 'buy-back price, in game credits',
  `required_skill` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to skills',
  `required_skill_level` int(11) NOT NULL DEFAULT '0' COMMENT 'if skill req''d the level',
  `use_type` int(11) NOT NULL DEFAULT '0' COMMENT 'type of item FK to use_types',
  `item_creator_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'kaneva_user_id of creator of item if UGC',
  `arm_anywhere` smallint(1) unsigned NOT NULL DEFAULT '1',
  `disarmable` smallint(1) unsigned NOT NULL DEFAULT '1',
  `stackable` smallint(1) unsigned NOT NULL DEFAULT '1',
  `destroy_when_used` smallint(1) unsigned NOT NULL DEFAULT '0',
  `inventory_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'mask, mature = 1024, gift = 512, normal = 256',
  `custom_texture` varchar(50) DEFAULT NULL COMMENT 'for UGC items',
  `base_global_id` int(11) NOT NULL DEFAULT '0' COMMENT 'for UGC, the glid of item this is based upon',
  `expired_duration` int(11) DEFAULT '0' COMMENT 'for tryon, if < now(), this is inactive',
  `permanent` smallint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'item fixed to character, can not put on or take off, e.g. spinning jewel',
  `is_default` smallint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'default item for a specific set of items, e.g. default male shirt given all male shirts',
  `visual_ref` int(11) NOT NULL DEFAULT '0',
  `web_controlled_edit` smallint(1) NOT NULL DEFAULT '0' COMMENT 'Denotes row has been edited by web. Editor export should not overwrite.',
  `is_derivable` smallint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'for UGC, can this be derived for new item',
  `derivation_level` smallint(1) NOT NULL DEFAULT '0',
  `actor_group` int(11) NOT NULL DEFAULT '0' COMMENT 'used to limit which actors can use this item',
  `search_refresh_needed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'update to include item in inremental index',
  PRIMARY KEY (`global_id`),
  KEY `INDEX_items_use_type` (`use_type`),
  KEY `base_global_id` (`base_global_id`),
  KEY `IDX_name` (`name`),
  KEY `IDX_creator` (`item_creator_id`),
  KEY `IDX_search_refresh_needed` (`search_refresh_needed`)
) ENGINE=InnoDB COMMENT='global inventory svd';

CREATE TABLE `item_animations` (
  `item_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ID of dynamic object containing animation',
  `animation_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ID of animation belonging to dynamic object',
  PRIMARY KEY (`item_id`,`animation_id`),
  KEY `IDX_animation` (`animation_id`)
) ENGINE=InnoDB COMMENT='Dynamic Objects to Animations relationships';

CREATE TABLE `item_approval_denials` (
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `reason_description` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`global_id`)
) ENGINE=InnoDB COMMENT='holds the explanation for item approval denials';

CREATE TABLE `item_asset_conversions` (
  `conv_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `old_path_type_id` int(11) NOT NULL,
  `new_path_type_id` int(11) NOT NULL,
  `global_id_min` int(11) NOT NULL DEFAULT '0',
  `global_id_max` int(11) NOT NULL DEFAULT '-1',
  `creation_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `completion_time` datetime NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`conv_id`),
  KEY `old_path_type_id` (`old_path_type_id`),
  KEY `new_path_type_id` (`new_path_type_id`),
  CONSTRAINT `item_asset_conversions_ibfk_1` FOREIGN KEY (`old_path_type_id`) REFERENCES `item_path_types` (`type_id`),
  CONSTRAINT `item_asset_conversions_ibfk_2` FOREIGN KEY (`new_path_type_id`) REFERENCES `item_path_types` (`type_id`)
) ENGINE=InnoDB;

CREATE TABLE `item_asset_conversion_logs` (
  `conv_id` int(11) NOT NULL,
  `global_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('COMPLETED','SKIPPED','FAILED') NOT NULL,
  `completion_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`conv_id`,`global_id`),
  KEY `global_id` (`global_id`),
  KEY `batch_id` (`batch_id`),
  CONSTRAINT `item_asset_conversion_logs_ibfk_1` FOREIGN KEY (`conv_id`) REFERENCES `item_asset_conversions` (`conv_id`),
  CONSTRAINT `item_asset_conversion_logs_ibfk_2` FOREIGN KEY (`global_id`) REFERENCES `items` (`global_id`)
) ENGINE=InnoDB;

CREATE TABLE `item_dynamic_objects` (
  `global_id` int(11) NOT NULL COMMENT 'Foreign key to items table',
  `attachable` smallint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Is item attachable',
  `interactive` smallint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Is item interactive',
  `playFlash` smallint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Can item play flash',
  `collision` smallint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Item has collision',
  `min_x` float NOT NULL DEFAULT '0' COMMENT 'Bounding Box Min coordinate',
  `min_y` float NOT NULL DEFAULT '0',
  `min_z` float NOT NULL DEFAULT '0',
  `max_x` float NOT NULL DEFAULT '0' COMMENT 'Bounding Box Max coordinate',
  `max_y` float NOT NULL DEFAULT '0',
  `max_z` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`global_id`)
) ENGINE=InnoDB COMMENT='Item related Dynamic Object data';

CREATE TABLE `item_external_add` (
  `player_id` int(11) NOT NULL COMMENT 'WOK playerId',
  `global_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL COMMENT 'FK to item_external_add_source',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when db entry was made',
  PRIMARY KEY (`player_id`,`global_id`)
) ENGINE=InnoDB;

CREATE TABLE `item_external_add_source` (
  `source_id` int(11) NOT NULL COMMENT 'Source id for item_external_add',
  `name` varchar(50) NOT NULL COMMENT 'Type of operation',
  `description` varchar(50) NOT NULL COMMENT 'Description of operation',
  PRIMARY KEY (`source_id`)
) ENGINE=InnoDB;

CREATE TABLE `item_parameters` (
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `param_type_id` int(11) NOT NULL COMMENT 'FK to parameters ',
  `VALUE` varchar(1000) NOT NULL DEFAULT '' COMMENT 'arbitrary value, meaning depends on type id',
  PRIMARY KEY (`global_id`,`param_type_id`),
  KEY `IDX_param_type` (`param_type_id`)
) ENGINE=InnoDB COMMENT='default parameters for items';

CREATE TABLE `item_paths` (
  `global_id` int(11) NOT NULL COMMENT 'Item global ID',
  `type_id` int(11) NOT NULL COMMENT 'Path type ID, see item_path_types table',
  `ordinal` bigint(20) NOT NULL DEFAULT '0',
  `path` varchar(250) NOT NULL COMMENT 'Relative path',
  `file_size` int(11) NOT NULL DEFAULT '0' COMMENT 'Asset file size for client-side verification. 0=ignore size/hash.',
  `file_hash` varchar(100) NOT NULL DEFAULT '' COMMENT 'Asset file MD5 checksum for client-side verification.',
  PRIMARY KEY (`global_id`,`type_id`,`ordinal`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `item_paths_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `item_path_types` (`type_id`)
) ENGINE=InnoDB COMMENT='Relative paths for item data file';

CREATE TABLE `item_paths_backup` (
  `global_id` int(11) NOT NULL COMMENT 'Item global ID',
  `type_id` int(11) NOT NULL COMMENT 'Path type ID, see item_path_types table',
  `ordinal` bigint(20) NOT NULL DEFAULT '0',
  `path` varchar(250) NOT NULL COMMENT 'Relative path',
  `file_size` int(11) NOT NULL DEFAULT '0' COMMENT 'Asset file size for client-side verification. 0=ignore size/hash.',
  `file_hash` varchar(100) NOT NULL DEFAULT '' COMMENT 'Asset file MD5 checksum for client-side verification.',
  PRIMARY KEY (`global_id`,`type_id`,`ordinal`),
  KEY `type_id` (`type_id`)
) ENGINE=InnoDB COMMENT='Relative paths for item data file';

CREATE TABLE `item_path_types` (
  `type_id` int(11) NOT NULL COMMENT 'Item path type ID',
  `type` varchar(20) NOT NULL COMMENT 'Item path type',
  `description` varchar(200) NOT NULL COMMENT 'Description',
  `xml_tag` varchar(40) NOT NULL COMMENT 'XML tag for client use',
  `asset_version` int(11) NOT NULL DEFAULT '0',
  `file_format` varchar(50) NOT NULL DEFAULT '',
  `path_regex` varchar(200) NOT NULL DEFAULT '',
  `path_format` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `path_type` (`type`)
) ENGINE=InnoDB COMMENT='Types of item paths -- see item_paths table';

CREATE TABLE `item_templates` (
  `global_id` int(11) NOT NULL,
  `sub_id` bigint(20) NOT NULL,
  `template_path` varchar(200) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  PRIMARY KEY (`global_id`,`sub_id`)
) ENGINE=InnoDB;

CREATE TABLE `item_use_types` (
  `use_type_id` smallint(5) unsigned NOT NULL,
  `use_type` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`use_type_id`)
) ENGINE=InnoDB COMMENT='FK to use_types in items';

CREATE TABLE `item_use_value` (
  `use_value_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `use_value` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`use_value_id`)
) ENGINE=InnoDB;

CREATE TABLE `kaneva_copied_items` (
  `old_global_id` int(11) NOT NULL COMMENT 'The global_id that was copied.',
  `new_global_id` int(11) NOT NULL COMMENT 'The newly-created global_id.',
  `copied_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When the copying occurred.',
  PRIMARY KEY (`old_global_id`,`new_global_id`)
) ENGINE=InnoDB COMMENT='UGC items that Kaneva has copied to take ownership of.';

CREATE TABLE `level_rewards` (
  `reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_id` int(11) NOT NULL DEFAULT '0',
  `level_id` int(11) NOT NULL DEFAULT '0',
  `reward_glid` int(11) NOT NULL DEFAULT '0',
  `reward_qty` int(11) NOT NULL DEFAULT '0',
  `reward_point_type` smallint(5) NOT NULL DEFAULT '0',
  `reward_amt` int(11) NOT NULL DEFAULT '0',
  `alt_reward_glid` int(11) NOT NULL DEFAULT '0',
  `alt_reward_qty` int(11) NOT NULL DEFAULT '0',
  `alt_reward_amt` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reward_id`,`skill_id`,`level_id`)
) ENGINE=InnoDB;

CREATE TABLE `maintenance_log` (
  `maintenance_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `date_occurred` datetime NOT NULL COMMENT 'Time stamp of maintenance event',
  `database_name` varchar(50) DEFAULT NULL COMMENT 'The database the event used',
  `procedure_name` varchar(200) DEFAULT NULL COMMENT 'The procedure that performed the event',
  `message_text` varchar(500) DEFAULT NULL COMMENT 'The details of the maintenance event',
  PRIMARY KEY (`maintenance_log_id`),
  KEY `IDX_date_occurred` (`date_occurred`),
  KEY `IDX_procedure_name` (`procedure_name`)
) ENGINE=InnoDB COMMENT='Info about maintenance events to audit activity';

CREATE TABLE `noterieties` (
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `murder_count` int(11) NOT NULL,
  `minutes_since_last_murder` int(11) NOT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=InnoDB COMMENT='1:1 with player, murder info';

CREATE TABLE `occupancy_limits` (
  `zone_index` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'type+index',
  `zone_instance_id` int(11) unsigned NOT NULL DEFAULT '0',
  `zone_index_plain` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'zoneIndex(zone_index)',
  `occupancy_limit_soft` int(11) unsigned NOT NULL COMMENT 'lower limit, still allow goto player',
  `occupancy_limit_hard` int(11) unsigned NOT NULL COMMENT 'only owner, gms allowed if hit',
  PRIMARY KEY (`zone_index`,`zone_instance_id`),
  KEY `IDX_OL_plain` (`zone_index_plain`),
  KEY `IDX_OL_limit` (`occupancy_limit_soft`)
) ENGINE=InnoDB COMMENT='soft and hard population occupancy limits';

CREATE TABLE `owner_items_95133` (
  `global_id` int(11) NOT NULL,
  PRIMARY KEY (`global_id`)
) ENGINE=InnoDB;

CREATE TABLE `p2p_animations` (
  `p2p_animation_id` int(11) NOT NULL DEFAULT '0' COMMENT 'This is the index for a p2p animation',
  `initiator_anim_index` int(11) NOT NULL DEFAULT '0' COMMENT 'Animation index for the person who starts the peer to peer animation',
  `participant_anim_index` int(11) NOT NULL DEFAULT '0' COMMENT 'Animation index of the person who agrees to the peer to peer animation',
  `name` varchar(100) DEFAULT NULL COMMENT 'Name of the animation',
  `runtime` int(11) NOT NULL DEFAULT '0' COMMENT 'Time in milliseconds the peer to peer animation runs',
  `looping` enum('T','F') DEFAULT 'F' COMMENT 'Does this animation loop?',
  PRIMARY KEY (`p2p_animation_id`)
) ENGINE=InnoDB COMMENT='Holds the animation indexes for peer to peer animations';

CREATE TABLE `parameters` (
  `param_type_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `NAME` varchar(30) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`param_type_id`)
) ENGINE=InnoDB COMMENT='descriptive table for item/dyn obj param_type_ids';

CREATE TABLE `pass_groups` (
  `pass_group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'PK',
  `flags` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Bit mask of pass group options.  See pass_group_flags',
  `name` varchar(50) NOT NULL COMMENT 'unique name of pass group',
  PRIMARY KEY (`pass_group_id`)
) ENGINE=InnoDB COMMENT='central table for pass system';

CREATE TABLE `pass_group_animations` (
  `animation_id` int(11) NOT NULL COMMENT 'id of animation from Editor',
  `pass_group_id` int(11) NOT NULL COMMENT 'FK to pass_groups',
  PRIMARY KEY (`animation_id`,`pass_group_id`)
) ENGINE=InnoDB COMMENT='track pass groups for animations';

CREATE TABLE `pass_group_channel_zones` (
  `zone_instance_id` int(11) NOT NULL COMMENT 'FK to channel zones',
  `zone_type` int(11) NOT NULL COMMENT 'FK to channel zones',
  `pass_group_id` int(11) NOT NULL COMMENT 'FK to pass_groups',
  `set_by_admin` char(1) NOT NULL DEFAULT 'N' COMMENT 'Indicates whether or not the pass group was set by a site administrator.',
  PRIMARY KEY (`zone_instance_id`,`zone_type`,`pass_group_id`)
) ENGINE=InnoDB COMMENT='m2m between pass_groups and apt or BB channel_zones';

CREATE TABLE `pass_group_flags` (
  `flag` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'must be mutually exclusive bit-wise with other flags',
  `name` varchar(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`flag`)
) ENGINE=InnoDB COMMENT='bit-mask flags for pass_groups.flags column';

CREATE TABLE `pass_group_items` (
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `pass_group_id` int(11) NOT NULL COMMENT 'FK to pass_groups',
  `auto_conversion` smallint(6) NOT NULL DEFAULT '0' COMMENT 'auto-conversion flag',
  PRIMARY KEY (`global_id`,`pass_group_id`)
) ENGINE=InnoDB COMMENT='m2m table between items and pass_groups';

CREATE TABLE `pass_group_items_audit` (
  `global_id` int(11) DEFAULT NULL,
  `pass_group_id` int(11) DEFAULT NULL,
  `modifier_kaneva_user_id` int(11) DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `operation` enum('INSERT','UPDATE','DELETE') DEFAULT NULL
) ENGINE=InnoDB;

CREATE TABLE `pass_group_perm_channels` (
  `zone_index_plain` int(11) NOT NULL COMMENT 'FK to channel zones',
  `pass_group_id` int(11) NOT NULL COMMENT 'FK to pass_groups',
  PRIMARY KEY (`zone_index_plain`,`pass_group_id`)
) ENGINE=InnoDB COMMENT='m2m between pass_groups and permanent zones in channel_zones';

CREATE TABLE `pass_group_users` (
  `kaneva_user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `pass_group_id` int(11) NOT NULL COMMENT 'FK to pass_groups',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`kaneva_user_id`,`pass_group_id`)
) ENGINE=InnoDB COMMENT='m2m between pass_groups and players';

CREATE TABLE `peak_concurrent_players` (
  `cur_date` date NOT NULL COMMENT 'current date',
  `cur_hour` tinyint(4) NOT NULL COMMENT 'current hour',
  `peak_count` int(11) NOT NULL COMMENT 'peak concurrent player count for the hour',
  PRIMARY KEY (`cur_date`,`cur_hour`)
) ENGINE=InnoDB COMMENT='peak concurrent player counts per hour';

CREATE TABLE `pending_players` (
  `kaneva_user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Players kaneva user id',
  `player_template_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Identifies which avatar template the player chose from the web',
  `apartment_template_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Identifies which apartment template the player chose',
  `date_created` datetime DEFAULT NULL COMMENT 'Date when the player chose an avatar template',
  `date_used` datetime DEFAULT NULL COMMENT 'Date when the player logged into world and the template was applied.  If this is NULL they have yet to log in.',
  PRIMARY KEY (`kaneva_user_id`)
) ENGINE=InnoDB COMMENT='This table holds avatar templates selected from the website';

CREATE TABLE `permanent_zone_ids` (
  `zone_index` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to supported world zone index',
  `zone_instance_id` int(11) unsigned NOT NULL COMMENT 'last used instance id',
  PRIMARY KEY (`zone_index`)
) ENGINE=InnoDB COMMENT='last used instance ids, used to serially get next id';

CREATE TABLE `permanent_zone_roles` (
  `zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'plain perm zone index',
  `kaneva_user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to game_users/player/kaneva.users',
  `role` int(11) NOT NULL DEFAULT '0' COMMENT 'from kaneva. 1 = owner, 2 = moderator',
  PRIMARY KEY (`zone_index`,`kaneva_user_id`)
) ENGINE=InnoDB COMMENT='roles of users for permanent zones';

CREATE TABLE `players` (
  `player_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique identifier for a player',
  `user_id` int(11) NOT NULL COMMENT 'Kaneva.users and game_players.user_id',
  `name` varchar(80) NOT NULL COMMENT 'name of avatar',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT 'title part of name',
  `spawn_config_index` int(11) NOT NULL DEFAULT '1' COMMENT 'initial spawn pt for new player',
  `original_EDB` int(11) NOT NULL DEFAULT '0' COMMENT 'players original entity form',
  `team_id` int(11) NOT NULL DEFAULT '1' COMMENT 'player''s team, mainly for arenas',
  `race_id` int(11) NOT NULL DEFAULT '1' COMMENT 'player''s race for using items',
  `last_update_time` timestamp NULL DEFAULT NULL COMMENT 'when this record was last updated',
  `in_game` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'is this player in the game?',
  `server_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK to Kaneva current engines',
  `kaneva_user_id` int(11) NOT NULL COMMENT 'denormalized from game_users',
  `created_date` datetime NOT NULL DEFAULT '2007-01-01 00:00:00' COMMENT 'when the avatar was created',
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y' COMMENT 'Is player data in active tables',
  PRIMARY KEY (`player_id`),
  UNIQUE KEY `name` (`name`),
  KEY `INDEX_players_in_game` (`in_game`),
  KEY `user_id` (`user_id`),
  KEY `IDX_player_server_id` (`server_id`),
  KEY `kuid` (`kaneva_user_id`)
) ENGINE=InnoDB COMMENT='avatars';

CREATE TABLE `player_presences` (
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `direction_x` float NOT NULL DEFAULT '0' COMMENT 'last direction',
  `direction_y` float NOT NULL DEFAULT '0',
  `direction_z` float NOT NULL DEFAULT '0',
  `position_x` float NOT NULL DEFAULT '0' COMMENT 'last position',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `energy` int(11) NOT NULL DEFAULT '0' COMMENT 'current hit points',
  `max_energy` int(11) NOT NULL DEFAULT '0' COMMENT 'max hit points',
  `housing_zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'zoneIndex of their apt',
  `housing_zone_index_plain` int(11) NOT NULL COMMENT 'just the zone index FK to supported_worlds',
  `respawn_zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'rebirth zone',
  `respawn_zone_instance_id` int(11) NOT NULL DEFAULT '0' COMMENT 'if respawn zone is instanced, this is instanceId',
  `respawn_position_x` float NOT NULL DEFAULT '0' COMMENT 'rebirth point',
  `respawn_position_y` float NOT NULL DEFAULT '0',
  `respawn_position_z` float NOT NULL DEFAULT '0',
  `scale_factor_x` float NOT NULL DEFAULT '1' COMMENT 'avatar scaling',
  `scale_factor_y` float NOT NULL DEFAULT '1',
  `scale_factor_z` float NOT NULL DEFAULT '1',
  `count_down_to_pardon` int(11) NOT NULL DEFAULT '0' COMMENT 'time until pardoned for murder',
  `arena_base_points` int(11) NOT NULL DEFAULT '0',
  `arena_total_kills` int(11) NOT NULL DEFAULT '0' COMMENT 'points scored in arena',
  `color_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'This references the type in the table colors',
  PRIMARY KEY (`player_id`),
  KEY `IDX_housing_zone_index` (`housing_zone_index_plain`),
  KEY `IDX_color_type` (`color_type`)
) ENGINE=InnoDB COMMENT='position and volatile player information';

CREATE TABLE `player_quests` (
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `quest_id` int(11) NOT NULL COMMENT 'FK to quests',
  `current_progression` int(11) NOT NULL DEFAULT '0' COMMENT 'where they are in quest sequence',
  `last_selection_index` int(11) NOT NULL DEFAULT '0' COMMENT 'track users position in progression',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'status of quest 1 = success, 0 = in progress',
  `max_drop_count` int(11) NOT NULL DEFAULT '0' COMMENT 'max number of items this quest can drop, on a per player basis',
  PRIMARY KEY (`player_id`,`quest_id`)
) ENGINE=InnoDB;

CREATE TABLE `player_skills` (
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `skill_id` int(11) NOT NULL COMMENT 'FK to skills',
  `current_level` int(11) NOT NULL DEFAULT '1' COMMENT 'skill level',
  `current_experience` double NOT NULL DEFAULT '0' COMMENT 'experience value ',
  PRIMARY KEY (`player_id`,`skill_id`),
  KEY `sid` (`skill_id`),
  KEY `pid` (`player_id`)
) ENGINE=InnoDB COMMENT='M2M between player and skills';

CREATE TABLE `player_spawn_points` (
  `player_id` int(11) NOT NULL,
  `zone_index` int(11) NOT NULL,
  `zone_instance_id` int(11) NOT NULL,
  `position_x` float NOT NULL DEFAULT '0',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `direction_x` float NOT NULL DEFAULT '0',
  `direction_y` float NOT NULL DEFAULT '0',
  `direction_z` float NOT NULL DEFAULT '0',
  `last_update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`player_id`,`zone_index`,`zone_instance_id`)
) ENGINE=InnoDB;

CREATE TABLE `player_stats` (
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `stat_id` int(11) NOT NULL COMMENT 'FK to stats',
  `current_value` float NOT NULL DEFAULT '0' COMMENT 'stat value',
  PRIMARY KEY (`player_id`,`stat_id`)
) ENGINE=InnoDB COMMENT='M2M between player and stats';

CREATE TABLE `player_templates` (
  `player_template_id` int(11) NOT NULL COMMENT 'Id for which avatar style the player selected',
  `name` varchar(256) DEFAULT NULL COMMENT 'Name of the avatar template',
  `description` text COMMENT 'Description of the avatar template',
  `gender` char(1) NOT NULL DEFAULT '' COMMENT 'Gender of the avatar',
  `image` varchar(256) DEFAULT NULL COMMENT 'Link to a screenshot of the avatar template',
  PRIMARY KEY (`player_template_id`)
) ENGINE=InnoDB COMMENT='Avatar template selected from the web';

CREATE TABLE `player_template_applications` (
  `player_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Player id from players table',
  `template_id` int(11) NOT NULL DEFAULT '0' COMMENT 'The template the player chose',
  `date_applied` datetime NOT NULL COMMENT 'When the template was applied',
  PRIMARY KEY (`player_id`),
  KEY `template_id` (`template_id`)
) ENGINE=InnoDB COMMENT='Keep track of when avatar templates are applied';

CREATE TABLE `player_titles` (
  `player_id` int(11) NOT NULL DEFAULT '0',
  `title_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0' COMMENT 'to track titles in 3DAPPS',
  PRIMARY KEY (`player_id`,`title_id`)
) ENGINE=InnoDB;

CREATE TABLE `player_zones` (
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `server_id` int(10) unsigned NOT NULL COMMENT 'FK to servers',
  `current_zone` varchar(80) NOT NULL DEFAULT '' COMMENT 'name of current zone',
  `current_zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'zoneId of current zone',
  `current_zone_type` smallint(6) NOT NULL DEFAULT '0' COMMENT 'denormalized type of zone from zone_index',
  `current_zone_instance_id` int(11) NOT NULL DEFAULT '0' COMMENT 'if zone is instanced, this is instanceId',
  PRIMARY KEY (`player_id`),
  KEY `IDX_pz_zone` (`current_zone`),
  KEY `IDX_pz_zoneIndex` (`current_zone_index`,`current_zone_instance_id`),
  KEY `server_id` (`server_id`),
  KEY `czii` (`current_zone_instance_id`)
) ENGINE=InnoDB COMMENT='zone information for a player';

CREATE TABLE `population_cache` (
  `STPURL` varchar(300) NOT NULL,
  `game_id` int(10) NOT NULL,
  `community_id` int(11) NOT NULL,
  `zone_type` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_public` char(1) NOT NULL,
  `is_adult` char(1) DEFAULT NULL,
  `keywords` text,
  `over_21_required` enum('Y','N') DEFAULT 'N',
  `creator_id` int(11) NOT NULL,
  `username` varchar(22) NOT NULL,
  `created_date` datetime NOT NULL,
  `name_no_spaces` varchar(100) NOT NULL,
  `thumbnail_small_path` varchar(255) NOT NULL,
  `game_image_path` varchar(255) DEFAULT NULL,
  `creator_name_no_spaces` varchar(100) NOT NULL,
  `creator_thumbnail_small_path` varchar(255) NOT NULL,
  `number_of_members` int(11) NOT NULL,
  `number_of_views` int(11) NOT NULL,
  `number_times_shared` int(11) NOT NULL,
  `number_of_diggs` int(11) NOT NULL,
  `number_of_diggs_past_7_days` int(11) NOT NULL,
  `game_population` int(11) NOT NULL,
  `last_ping_datetime` datetime DEFAULT NULL,
  `has_thumbnail` char(1) NOT NULL,
  PRIMARY KEY (`game_id`),
  KEY `idx_game_population` (`game_population`)
) ENGINE=InnoDB;

CREATE TABLE `premium_item_types` (
  `premium_item_type_id` int(11) NOT NULL COMMENT 'premium item type id number.',
  `name` varchar(50) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`premium_item_type_id`)
) ENGINE=InnoDB;

CREATE TABLE `product_availability` (
  `product_availability_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `product_availability` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`product_availability_id`)
) ENGINE=InnoDB;

CREATE TABLE `product_exclusivity` (
  `product_exclusivity_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `product_exclusivity` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`product_exclusivity_id`)
) ENGINE=InnoDB;

CREATE TABLE `quests` (
  `quest_id` int(11) NOT NULL COMMENT 'spawnGenId',
  `title` varchar(100) NOT NULL COMMENT 'short name of quest, shown in menu',
  `description` varchar(1000) NOT NULL COMMENT 'longer name, shown in menu',
  `completion_message` varchar(1000) NOT NULL COMMENT 'opt msg shown when complete',
  `npc_name` varchar(50) NOT NULL COMMENT 'name of npc attached to quest',
  `bonus_skill_id` int(11) NOT NULL COMMENT 'FK to skills, add this skill when kill MOB',
  `experience_bonus` float NOT NULL DEFAULT '0' COMMENT 'add this amount of skill when kill MOB',
  `max_exp_limit` float NOT NULL DEFAULT '0' COMMENT 'max skill can be added when kill MOB',
  `is_autocomplete` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'T/F if true and quest is complete when kill mob, quest it marked as complete',
  `optional_instruction` varchar(1000) NOT NULL,
  `mob_id_to_hunt` int(11) NOT NULL COMMENT 'xref aiCfgId,mob id to kill to complete quest',
  `drop_global_id` int(11) NOT NULL COMMENT 'FK to items, xref GLID of dropped item by MOB',
  `drop_percentage` int(11) NOT NULL COMMENT 'chance MOB will drop the item',
  `is_undeletable` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'T/F, if have > 10 (hardcoded) quests, will remove one that is note marked as this',
  KEY `quest_id` (`quest_id`)
) ENGINE=InnoDB COMMENT='quest svd';

CREATE TABLE `quest_progress_condition` (
  `prog_cond_id` int(11) NOT NULL,
  `datetime_begin` datetime DEFAULT NULL,
  `datetime_end` datetime DEFAULT NULL,
  `description` varchar(1000) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE `required_quest_items` (
  `quest_id` int(11) NOT NULL COMMENT 'FK to quests',
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `quantity` int(11) NOT NULL COMMENT 'number of them you must have',
  KEY `IDX_quest_required` (`quest_id`)
) ENGINE=InnoDB COMMENT='list of required items for a quest';

CREATE TABLE `required_skill` (
  `required_skill_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `required_skill` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`required_skill_id`)
) ENGINE=InnoDB;

CREATE TABLE `required_skill_level` (
  `required_skill_level_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `required_skill_level` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`required_skill_level_id`)
) ENGINE=InnoDB;

CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  `interface_change` enum('T','F') DEFAULT 'F' COMMENT 'does this update require matching binaries',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

CREATE TABLE `script_available_items` (
  `item_id` varchar(20) NOT NULL COMMENT 'Item id of the item the script server can generate',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB COMMENT='Holds the glids of items that the script server can generate';

CREATE TABLE `script_available_items_plain` (
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  PRIMARY KEY (`global_id`)
) ENGINE=InnoDB COMMENT='the list of items available for scripts to use';

CREATE TABLE `script_game_custom_data` (
  `zone_instance_id` int(11) NOT NULL COMMENT 'The zone_instance_id of the world to which this data belongs.',
  `zone_type` int(11) NOT NULL COMMENT 'The zone_type of the world to which this data belongs.',
  `attribute` varchar(255) NOT NULL DEFAULT 'default' COMMENT 'What this data is.',
  `value` text COMMENT 'Value of the data.',
  PRIMARY KEY (`zone_instance_id`,`zone_type`,`attribute`),
  KEY `sgcd_zone_instance_id_fk` (`zone_type`,`zone_instance_id`),
  CONSTRAINT `sgcd_zone_instance_id_fk` FOREIGN KEY (`zone_type`, `zone_instance_id`) REFERENCES `channel_zones` (`zone_type`, `zone_instance_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Table of custom data values for a world.';

CREATE TABLE `script_game_data` (
  `game_id` int(11) NOT NULL DEFAULT '0' COMMENT 'This is the zone index that matches supported_worlds table.  There is only one game type per zone index.',
  `attribute` varchar(16) NOT NULL DEFAULT 'default' COMMENT 'Key for a specific game value',
  `value` varchar(128) DEFAULT NULL COMMENT 'Value for an attribute',
  PRIMARY KEY (`game_id`,`attribute`)
) ENGINE=InnoDB COMMENT='Hold data relevant to a game';

CREATE TABLE `script_game_items` (
  `zone_instance_id` int(11) NOT NULL COMMENT 'The zone_instance_id of the world to which this item belongs.',
  `zone_type` int(11) NOT NULL COMMENT 'The zone_type of the world to which this item belongs.',
  `game_item_id` int(11) NOT NULL COMMENT 'Item id is controlled within the game code.',
  `game_item_glid` int(11) DEFAULT NULL COMMENT 'wok.items record that this game tem is associated with.',
  `glid` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL COMMENT 'Name of this item.',
  `item_type` varchar(50) NOT NULL COMMENT 'The type of this item.',
  `level` int(11) NOT NULL DEFAULT '1' COMMENT 'The game item''s level.',
  `rarity` varchar(50) NOT NULL DEFAULT 'Common' COMMENT 'The game item''s rarity.',
  PRIMARY KEY (`zone_instance_id`,`zone_type`,`game_item_id`),
  KEY `sgi_zone_instance_id_fk` (`zone_type`,`zone_instance_id`),
  KEY `sgi_item_type_fk` (`item_type`),
  KEY `sgi_game_item_glid_fk_idx` (`game_item_glid`),
  KEY `sgi_glid_fk_idx` (`glid`),
  CONSTRAINT `sgi_game_item_glid_fk` FOREIGN KEY (`game_item_glid`) REFERENCES `snapshot_script_game_items` (`game_item_glid`),
  CONSTRAINT `sgi_glid_fk` FOREIGN KEY (`glid`) REFERENCES `items` (`global_id`),
  CONSTRAINT `sgi_item_type_fk` FOREIGN KEY (`item_type`) REFERENCES `script_game_item_types` (`item_type`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sgi_zone_instance_id_fk` FOREIGN KEY (`zone_type`, `zone_instance_id`) REFERENCES `channel_zones` (`zone_type`, `zone_instance_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Table of game items that are owned by a world.';

CREATE TABLE `script_game_item_global_configs` (
  `property_name` varchar(50) NOT NULL,
  `property_value` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`property_name`)
) ENGINE=InnoDB COMMENT='Contains a list of configurable values that control script game item functionality within WoK.';

CREATE TABLE `script_game_item_properties` (
  `zone_instance_id` int(11) NOT NULL COMMENT 'The zone_instance_id of the world to which this item belongs.',
  `zone_type` int(11) NOT NULL COMMENT 'The zone_type of the world to which this item belongs.',
  `game_item_id` int(11) NOT NULL COMMENT 'Item id to which these properties belong.',
  `property_name` varchar(50) NOT NULL COMMENT 'Name of this property.',
  `property_value` varchar(1000) DEFAULT NULL COMMENT 'Value of this property.',
  PRIMARY KEY (`zone_instance_id`,`zone_type`,`game_item_id`,`property_name`),
  CONSTRAINT `sgip_game_item_fk` FOREIGN KEY (`zone_instance_id`, `zone_type`, `game_item_id`) REFERENCES `script_game_items` (`zone_instance_id`, `zone_type`, `game_item_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Table of variable properties that belong to the various items owned by a game.';

CREATE TABLE `script_game_item_types` (
  `item_type` varchar(50) NOT NULL COMMENT 'Item type name.',
  `description` varchar(1000) DEFAULT NULL COMMENT 'Optional description of the item type.',
  `inventory_compatible` char(1) NOT NULL DEFAULT 'F' COMMENT 'Whether or not this type of game item is compatible with the wok inventory.',
  PRIMARY KEY (`item_type`)
) ENGINE=InnoDB COMMENT='Item types constrained.';

CREATE TABLE `script_game_player_data` (
  `zone_instance_id` int(11) NOT NULL COMMENT 'The zone_instance_id of the world to which this data belongs.',
  `zone_type` int(11) NOT NULL COMMENT 'The zone_type of the world to which this data belongs.',
  `username` varchar(22) NOT NULL COMMENT 'username of the player that owns this data.',
  `attribute` varchar(255) NOT NULL DEFAULT 'default' COMMENT 'What this data is.',
  `value` text COMMENT 'Value of the data.',
  PRIMARY KEY (`zone_instance_id`,`zone_type`,`username`,`attribute`),
  KEY `sgpd_zone_instance_id_fk` (`zone_type`,`zone_instance_id`),
  KEY `sgpd_username_fk` (`username`),
  CONSTRAINT `sgpd_username_fk` FOREIGN KEY (`username`) REFERENCES `kaneva`.`users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sgpd_zone_instance_id_fk` FOREIGN KEY (`zone_type`, `zone_instance_id`) REFERENCES `channel_zones` (`zone_type`, `zone_instance_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Table of data values for players of a world.';

CREATE TABLE `script_player_data` (
  `player_id` int(11) NOT NULL DEFAULT '0' COMMENT 'player id from WOK',
  `game_id` int(11) NOT NULL DEFAULT '0' COMMENT 'This is the zone index that matches supported_worlds table.  There is only one game type per zone index.',
  `attribute` varchar(16) NOT NULL DEFAULT 'default' COMMENT 'Key for a specific player value for a specific game',
  `value` varchar(128) DEFAULT NULL COMMENT 'Value for an attribute',
  PRIMARY KEY (`player_id`,`game_id`,`attribute`)
) ENGINE=InnoDB COMMENT='Holds script server data for players ';

CREATE TABLE `skills` (
  `skill_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'db id of skill',
  `name` varchar(30) NOT NULL COMMENT 'name of skill',
  `type` int(11) NOT NULL COMMENT 'skill type',
  `gain_increment` float NOT NULL DEFAULT '0' COMMENT 'basis for gain (how fast a skill can be built)',
  `basic_ability_skill` char(1) NOT NULL DEFAULT 'F' COMMENT 'means that it only has one function like hiding (T/F)',
  `basic_ability_exp_mastered` float NOT NULL DEFAULT '0' COMMENT 'component of ability basic skill option',
  `basic_ability_function` int(11) NOT NULL DEFAULT '0' COMMENT 'component of ability basic-function resulting from successful throw',
  `skill_gains_when_failed` float NOT NULL DEFAULT '0' COMMENT '0.0 is no gain when failed if not 0.0 then it will gain till that exp point then not gain from trying anymore',
  `max_multiplier` float NOT NULL,
  `use_internal_timer` char(1) NOT NULL COMMENT 'T/F',
  `filter_eval_level` char(1) NOT NULL COMMENT 'T/F',
  PRIMARY KEY (`skill_id`),
  KEY `INDEX_skill_name` (`name`)
) ENGINE=InnoDB COMMENT='player skills svd';

CREATE TABLE `skill_actions` (
  `action_id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_id` int(11) NOT NULL DEFAULT '0',
  `gain_amt` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB;

CREATE TABLE `skill_levels` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_id` int(11) NOT NULL DEFAULT '0',
  `level_number` int(11) NOT NULL DEFAULT '0',
  `start_exp` float NOT NULL DEFAULT '0',
  `end_exp` float NOT NULL DEFAULT '0',
  `title_id` int(11) NOT NULL DEFAULT '0',
  `alt_title_id` int(11) NOT NULL DEFAULT '0',
  `gain_msg` varchar(200) NOT NULL DEFAULT '',
  `alt_gain_msg` varchar(200) NOT NULL DEFAULT '',
  `level_msg` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_placed_furniture` (
  `furniture_count` int(11) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_purchased_clothing` (
  `clothing_purchase_count` int(11) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_script_game_items` (
  `game_item_glid` int(11) NOT NULL,
  `glid` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `item_type` varchar(50) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `rarity` varchar(50) NOT NULL DEFAULT 'Common',
  PRIMARY KEY (`game_item_glid`),
  KEY `snsgi_glid_fk` (`glid`),
  KEY `snsgi_item_type_fk` (`item_type`),
  CONSTRAINT `snsgi_game_item_glid_fk` FOREIGN KEY (`game_item_glid`) REFERENCES `items` (`global_id`),
  CONSTRAINT `snsgi_glid_fk` FOREIGN KEY (`glid`) REFERENCES `items` (`global_id`),
  CONSTRAINT `snsgi_item_type_fk` FOREIGN KEY (`item_type`) REFERENCES `script_game_item_types` (`item_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Snapshot of a script game item instance. Used to integrate game items in shop and inventory.';

CREATE TABLE `snapshot_script_game_item_properties` (
  `game_item_glid` int(11) NOT NULL,
  `property_name` varchar(50) NOT NULL,
  `property_value` varchar(1000) NOT NULL,
  PRIMARY KEY (`game_item_glid`,`property_name`),
  CONSTRAINT `snsgip_game_item_glid_fk` FOREIGN KEY (`game_item_glid`) REFERENCES `snapshot_script_game_items` (`game_item_glid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Snapshot of a script game item instance''s properties.';

CREATE TABLE `snapshot_user_active` (
  `user_count` int(11) NOT NULL,
  `play_time_avg` bigint(20) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_user_avatar_gender` (
  `user_count` int(11) NOT NULL DEFAULT '0',
  `gender` enum('M','F') NOT NULL DEFAULT 'M',
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`,`gender`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_user_balances` (
  `balance` double NOT NULL DEFAULT '0',
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL,
  PRIMARY KEY (`created_date`),
  KEY `bal` (`balance`),
  KEY `kpi` (`kei_point_id`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_user_gender` (
  `user_count` int(11) NOT NULL,
  `gender` enum('M','F') NOT NULL DEFAULT 'M',
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`,`gender`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_user_inactive` (
  `user_count` int(11) NOT NULL,
  `play_time_avg` bigint(20) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `snapshot_wok_user_average` (
  `play_time_avg` int(11) NOT NULL,
  `created_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`created_date`)
) ENGINE=InnoDB;

CREATE TABLE `sound_customizations` (
  `obj_placement_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Unique id of placed object',
  `zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'location of object',
  `instance_id` int(11) NOT NULL DEFAULT '0' COMMENT 'instance of zone_index the object is located in',
  `pitch` float NOT NULL DEFAULT '1' COMMENT 'Frequency of the sound',
  `gain` float NOT NULL DEFAULT '1' COMMENT 'Volume of the sound',
  `max_distance` float NOT NULL DEFAULT '25' COMMENT 'Furthest distance from source the sound can be heard',
  `roll_off` float NOT NULL DEFAULT '1' COMMENT 'Rate at which the sound decays as max_distance is approached',
  `loop` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Does the sound loop or not',
  `loop_delay` int(11) NOT NULL DEFAULT '0' COMMENT 'How much time to wait between each sound loop',
  `dir_x` float NOT NULL DEFAULT '0' COMMENT 'X component of the vector definig the direction of the sound (zero vector means omni directional)',
  `dir_y` float NOT NULL DEFAULT '0' COMMENT 'Y component of the vector definig the direction of the sound (zero vector means omni directional)',
  `dir_z` float NOT NULL DEFAULT '0' COMMENT 'Z component of the vector definig the direction of the sound (zero vector means omni directional)',
  `cone_outer_gain` float NOT NULL DEFAULT '1' COMMENT 'Volume at the outer edge of the cone of a directional sound',
  `cone_inner_angle` smallint(6) NOT NULL DEFAULT '360' COMMENT 'Angle of inner cone where gain reaches maximum',
  `cone_outer_angle` smallint(6) NOT NULL DEFAULT '360' COMMENT 'Angle of outer cone where gain drops to cone_outer_gain',
  PRIMARY KEY (`obj_placement_id`),
  KEY `sound_customization_zi_ii` (`zone_index`,`instance_id`)
) ENGINE=InnoDB COMMENT='This table my change or go away after item rework';

CREATE TABLE `spawn_points` (
  `spawn_point_id` int(11) unsigned NOT NULL COMMENT 'PK',
  `zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'what zone this is for, type is optional',
  `position_x` float NOT NULL DEFAULT '0' COMMENT 'where in zone',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `rotation` int(11) DEFAULT '0' COMMENT 'in degrees',
  `name` varchar(50) NOT NULL COMMENT 'short desc',
  PRIMARY KEY (`spawn_point_id`),
  KEY `INDEX_sp_zone_index` (`zone_index`)
) ENGINE=InnoDB COMMENT='spawn points for zones';

CREATE TABLE `sp_logs` (
  `counter` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `caller` varchar(120) NOT NULL COMMENT 'stored proc that generated this msg',
  `type` enum('DBG','INFO','WARN','ERROR') NOT NULL DEFAULT 'DBG' COMMENT 'serverity type',
  `msg` varchar(500) NOT NULL,
  PRIMARY KEY (`counter`),
  KEY `idx_date` (`when`)
) ENGINE=InnoDB ROW_FORMAT=FIXED COMMENT='log message table for stored procs';

CREATE TABLE `sp_run_log` (
  `sp_run_log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `sp_name` varchar(256) NOT NULL COMMENT 'name of the stored procedure',
  `run_start_time` datetime NOT NULL COMMENT 'when started',
  `run_end_time` datetime NOT NULL COMMENT 'when ended',
  `duration_seconds` int(11) NOT NULL COMMENT 'duration in seconds',
  PRIMARY KEY (`sp_run_log_id`),
  KEY `IDX_start_time` (`run_start_time`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT COMMENT='run duration information for stored procedures';

CREATE TABLE `starting_balances` (
  `kei_point_id` varchar(50) NOT NULL DEFAULT 'KPOINT' COMMENT 'FK to kaneva.user_balanaces',
  `balance` decimal(12,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`kei_point_id`)
) ENGINE=InnoDB COMMENT='starting balance for new avatar';

CREATE TABLE `starting_deformable_configs` (
  `player_template_id` int(11) NOT NULL DEFAULT '1' COMMENT 'This maps to the player template selected on the web',
  `sequence` int(11) NOT NULL COMMENT 'sort key to key cfgs in order',
  `equip_id` int(11) NOT NULL DEFAULT '0' COMMENT 'zero for main body, else id of attached item',
  `deformable_config` int(11) NOT NULL COMMENT 'config id from the skeletal object',
  `custom_material` int(11) NOT NULL COMMENT 'customizable material',
  `custom_color_r` float NOT NULL COMMENT 'color of material',
  `custom_color_g` float NOT NULL,
  `custom_color_b` float NOT NULL,
  PRIMARY KEY (`player_template_id`,`sequence`),
  KEY `INDEX_defconfig_seq` (`sequence`)
) ENGINE=InnoDB COMMENT='This stores the config for the avatar templates from the web';

CREATE TABLE `starting_dynamic_objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique id',
  `zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'zone where set.',
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'is this an active item to be added',
  `object_id` int(11) NOT NULL DEFAULT '0' COMMENT 'id of dynamic object (e.g. couch)',
  `global_id` int(11) NOT NULL DEFAULT '0' COMMENT 'global inventory id of item that created this',
  `position_x` float NOT NULL DEFAULT '0' COMMENT 'position in zone',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `rotation_x` float NOT NULL DEFAULT '0' COMMENT 'rotatation',
  `rotation_y` float NOT NULL DEFAULT '0',
  `rotation_z` float NOT NULL DEFAULT '0',
  `apartment_template_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Maps dynamic objects to a specific apartment template',
  PRIMARY KEY (`id`),
  KEY `INDEX_sdo_active` (`active`),
  KEY `sdo_zone_index_idx` (`zone_index`),
  KEY `sdo_global_id_idx` (`global_id`),
  KEY `sdo_apartment_template_id_fk_idx` (`apartment_template_id`),
  CONSTRAINT `sdo_apartment_template_id_fk` FOREIGN KEY (`apartment_template_id`) REFERENCES `items` (`global_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='starting object in the new players apt';

CREATE TABLE `starting_dynamic_object_parameters` (
  `id` int(11) NOT NULL COMMENT 'FK to starting_dynamic_objects',
  `param_type_id` int(11) NOT NULL COMMENT 'FK to parameters ',
  `VALUE` varchar(1000) NOT NULL DEFAULT '' COMMENT 'arbitrary value, meaning depends on type id',
  PRIMARY KEY (`id`,`param_type_id`),
  KEY `sdop_param_type_id_idx` (`param_type_id`)
) ENGINE=InnoDB COMMENT='parameters for starting zone placed objects';

CREATE TABLE `starting_dynamic_object_playlists` (
  `id` int(11) NOT NULL,
  `global_id` int(11) NOT NULL,
  `asset_group_id` int(11) NOT NULL,
  `video_range` float NOT NULL DEFAULT '0',
  `audio_range` float NOT NULL DEFAULT '0',
  `volume` float NOT NULL DEFAULT '100',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='Snapshot of starting dynamic object playlists for templates.';

CREATE TABLE `starting_inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'is this an active item to be added',
  `original_EDB` int(11) NOT NULL DEFAULT '0' COMMENT 'players original entity form',
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `inventory_type` enum('P','B','H') NOT NULL DEFAULT 'P' COMMENT 'type of inventory this is first letter of: Player/Bank/Housing',
  `armed` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'is item currently armed (T/F)',
  `quantity` int(11) NOT NULL DEFAULT '0' COMMENT 'how many of this item',
  `charge_quantity` int(11) NOT NULL DEFAULT '0' COMMENT 'if ammo, amount of ammo',
  `sub_type` int(11) NOT NULL DEFAULT '512' COMMENT 'Inventory sub type (normal, gift, etc)',
  `player_template_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Maps items to a specific player template',
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_si_unique_multi` (`active`,`original_EDB`,`global_id`,`player_template_id`),
  KEY `INDEX_si_active` (`active`),
  KEY `INDEX_si_edb` (`original_EDB`)
) ENGINE=InnoDB COMMENT='starting inventory for new players by EDB';

CREATE TABLE `starting_player_info` (
  `player_template_id` int(11) NOT NULL DEFAULT '1',
  `team_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Which team does the player belong to',
  `original_edb` int(11) NOT NULL DEFAULT '0' COMMENT 'The actor used for this player template',
  `race_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Race used for the player template',
  `scale_factor_x` float NOT NULL DEFAULT '1' COMMENT 'x axis scaling for character',
  `scale_factor_y` float NOT NULL DEFAULT '1' COMMENT 'y axis scaling for character',
  `scale_factor_z` float NOT NULL DEFAULT '1' COMMENT 'z axis scaling for character',
  PRIMARY KEY (`player_template_id`)
) ENGINE=InnoDB COMMENT='Contains player information for template';

CREATE TABLE `starting_script_game_custom_data` (
  `template_glid` int(11) NOT NULL,
  `attribute` varchar(255) NOT NULL DEFAULT 'default',
  `value` text,
  PRIMARY KEY (`template_glid`,`attribute`),
  CONSTRAINT `stsgcd_template_glid_fk` FOREIGN KEY (`template_glid`) REFERENCES `items` (`global_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Snapshot of custom game data properties.';

CREATE TABLE `starting_script_game_items` (
  `template_glid` int(11) NOT NULL,
  `game_item_id` int(11) NOT NULL,
  `game_item_glid` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`template_glid`,`game_item_id`),
  KEY `stsgi_game_item_glid_fk` (`game_item_glid`),
  CONSTRAINT `stsgi_game_item_glid_fk` FOREIGN KEY (`game_item_glid`) REFERENCES `snapshot_script_game_items` (`game_item_glid`),
  CONSTRAINT `stsgi_template_glid_fk` FOREIGN KEY (`template_glid`) REFERENCES `items` (`global_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Snapshot of a script game item instance. Used to integrate game items in shop and inventory.';

CREATE TABLE `starting_sound_customizations` (
  `id` int(11) NOT NULL COMMENT 'Unique id of placed object',
  `pitch` float NOT NULL DEFAULT '1' COMMENT 'Frequency of the sound',
  `gain` float NOT NULL DEFAULT '1' COMMENT 'Volume of the sound',
  `max_distance` float NOT NULL DEFAULT '25' COMMENT 'Furthest distance from source the sound can be heard',
  `roll_off` float NOT NULL DEFAULT '1' COMMENT 'Rate at which the sound decays as max_distance is approached',
  `loop` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Does the sound loop or not',
  `loop_delay` int(11) NOT NULL DEFAULT '0' COMMENT 'How much time to wait between each sound loop',
  `dir_x` float NOT NULL DEFAULT '0' COMMENT 'X component of the vector definig the direction of the sound (zero vector means omni directional)',
  `dir_y` float NOT NULL DEFAULT '0' COMMENT 'Y component of the vector definig the direction of the sound (zero vector means omni directional)',
  `dir_z` float NOT NULL DEFAULT '0' COMMENT 'Z component of the vector definig the direction of the sound (zero vector means omni directional)',
  `cone_outer_gain` float NOT NULL DEFAULT '1' COMMENT 'Volume at the outer edge of the cone of a directional sound',
  `cone_inner_angle` smallint(6) NOT NULL DEFAULT '360' COMMENT 'Angle of inner cone where gain reaches maximum',
  `cone_outer_angle` smallint(6) NOT NULL DEFAULT '360' COMMENT 'Angle of outer cone where gain drops to cone_outer_gain',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='This table my change or go away after item rework';

CREATE TABLE `starting_world_object_player_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'zone where set.',
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'is this an active item to be added',
  `world_object_id` char(36) NOT NULL DEFAULT '0',
  `asset_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Kaneva.assets.asset_id',
  `texture_url` varchar(255) NOT NULL COMMENT 'from kaneva assets',
  `apartment_template_id` int(11) NOT NULL DEFAULT '0' COMMENT 'The apartment template this customization goes with.',
  PRIMARY KEY (`id`),
  KEY `INDEX_swo_active` (`active`),
  KEY `swop_apartment_template_id_fk_idx` (`apartment_template_id`)
) ENGINE=InnoDB COMMENT='player''s apartment customizations';

CREATE TABLE `stats` (
  `stat_id` int(11) NOT NULL COMMENT 'editor''s id',
  `name` varchar(50) NOT NULL COMMENT 'stat name (WIS/DEX...)',
  `cap_value` float NOT NULL COMMENT 'max value',
  `gain_basis` float NOT NULL,
  `benefit_type` int(11) NOT NULL COMMENT '0 = no benefit  1=health benefit  2=speed benefit',
  `bonus_basis` float NOT NULL,
  PRIMARY KEY (`stat_id`)
) ENGINE=InnoDB COMMENT='player stats svd';

CREATE TABLE `stock_animation_glids` (
  `owner_type` enum('ACTOR','EQUIP') NOT NULL,
  `owner_name` varchar(50) NOT NULL,
  `animation_index` int(11) NOT NULL,
  `animation_type` int(11) NOT NULL,
  `animation_version` int(11) NOT NULL,
  `animation_glid` int(11) NOT NULL,
  `animation_name` varchar(50) NOT NULL,
  PRIMARY KEY (`owner_type`,`owner_name`,`animation_index`)
) ENGINE=InnoDB COMMENT='Stock animation index/type to GLID mappings';

CREATE TABLE `stores` (
  `store_id` int(11) NOT NULL COMMENT 'PK',
  `game_id` int(11) NOT NULL DEFAULT '3296' COMMENT 'FK to developer.games',
  `name` varchar(64) NOT NULL COMMENT 'name of store (not NPC)',
  `channel_id` int(11) NOT NULL COMMENT 'channel where store exists',
  `web_controlled_edit` smallint(6) NOT NULL DEFAULT '1' COMMENT 'if 1, wont be exported from Editor',
  PRIMARY KEY (`store_id`)
) ENGINE=InnoDB COMMENT='in-game stores (commerce objects)';

CREATE TABLE `store_inventories` (
  `store_id` int(11) NOT NULL COMMENT 'FK to stores',
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  PRIMARY KEY (`store_id`,`global_id`)
) ENGINE=InnoDB COMMENT='store->items table';

CREATE TABLE `summary_active_permanent_population` (
  `zone_index` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'perm zone index',
  `count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'current population',
  PRIMARY KEY (`zone_index`)
) ENGINE=InnoDB COMMENT='track population of all instances of permanent zones';

CREATE TABLE `summary_active_population` (
  `server_id` int(10) unsigned NOT NULL COMMENT 'FK to servers',
  `zone_index` int(10) unsigned NOT NULL COMMENT 'zone id w/o instance',
  `zone_type` smallint(6) NOT NULL DEFAULT '0' COMMENT 'type (apt,channel,etc)',
  `zone_instance_id` int(10) unsigned NOT NULL COMMENT 'zone instance',
  `count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'current total in zone for server',
  PRIMARY KEY (`server_id`,`zone_instance_id`,`zone_index`),
  UNIQUE KEY `sap_zone_index_fields` (`zone_index`,`zone_type`,`zone_instance_id`),
  KEY `IDX_ap_type` (`zone_type`),
  KEY `IDX_ap_zone` (`zone_index`,`zone_instance_id`),
  KEY `IDX_ap_zone2` (`zone_instance_id`,`zone_type`)
) ENGINE=InnoDB COMMENT='summary table of players in zones on servers';

CREATE TABLE `summary_users_by_hour` (
  `cur_date` date NOT NULL DEFAULT '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL DEFAULT '0',
  `avg_logged_on_players` int(11) DEFAULT NULL,
  `max_logged_on_players` int(11) DEFAULT NULL,
  PRIMARY KEY (`cur_date`,`cur_hour`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `summary_users_by_minute` (
  `cur_date` date NOT NULL DEFAULT '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL DEFAULT '0',
  `cur_minute` tinyint(4) NOT NULL DEFAULT '0',
  `logged_on_players` int(11) DEFAULT NULL,
  PRIMARY KEY (`cur_date`,`cur_hour`,`cur_minute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `summary_users_by_server_by_hour` (
  `cur_date` date NOT NULL DEFAULT '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL DEFAULT '0',
  `avg_logged_on_players` int(11) DEFAULT NULL,
  `max_logged_on_players` int(11) DEFAULT NULL,
  `server_id` int(11) NOT NULL,
  PRIMARY KEY (`cur_date`,`cur_hour`,`server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `summary_users_by_server_by_minute` (
  `cur_date` date NOT NULL DEFAULT '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL DEFAULT '0',
  `cur_minute` tinyint(4) NOT NULL DEFAULT '0',
  `logged_on_players` int(11) DEFAULT NULL,
  `server_id` int(11) NOT NULL,
  PRIMARY KEY (`cur_date`,`cur_hour`,`cur_minute`,`server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `supported_worlds` (
  `zone_index` int(11) NOT NULL COMMENT 'index in editor''s array',
  `name` varchar(260) NOT NULL COMMENT 'zone name and extension',
  `channel_id` int(11) NOT NULL DEFAULT '1' COMMENT 'channel this zone uses for messages',
  `display_name` varchar(64) NOT NULL COMMENT 'name shown in client',
  `default_max_occupancy` int(11) unsigned NOT NULL DEFAULT '30' COMMENT 'default limit copied to channel_zones on create',
  PRIMARY KEY (`zone_index`)
) ENGINE=InnoDB COMMENT='supported worlds for the CMP';

CREATE TABLE `template_server_ties` (
  `server_ip_address` varchar(50) NOT NULL DEFAULT '0' COMMENT 'external IP used by clients',
  `template_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'id of the template',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date that the template_server_ties relationship was created.',
  PRIMARY KEY (`server_ip_address`),
  KEY `tst_template_id_fk` (`template_id`),
  CONSTRAINT `tst_template_id_fk` FOREIGN KEY (`template_id`) REFERENCES `world_templates` (`template_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='stores templates that are tied to specific servers';

CREATE TABLE `thumbnail_xl_sizes` (
  `asset_id` int(11) NOT NULL,
  `file_size` int(11) DEFAULT NULL,
  PRIMARY KEY (`asset_id`)
) ENGINE=InnoDB;

CREATE TABLE `time_played` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `time_played` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`date_created`),
  KEY `dtc` (`date_created`),
  KEY `tp` (`time_played`)
) ENGINE=InnoDB;

CREATE TABLE `titles` (
  `title_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`title_id`)
) ENGINE=InnoDB;

CREATE TABLE `tmp_kaneva_item_sizes` (
  `global_id` int(11) NOT NULL,
  `file_size` int(11) NOT NULL,
  PRIMARY KEY (`global_id`)
) ENGINE=InnoDB;

CREATE TABLE `ugc_price_tiers` (
  `kaneva_role_value` int(11) NOT NULL,
  `ugc_usage_id` int(5) NOT NULL,
  `tier_id` int(5) NOT NULL,
  `tier_type` char(3) NOT NULL,
  `desc_matched` varchar(200) NOT NULL DEFAULT '',
  `desc_nomatch` varchar(200) NOT NULL DEFAULT '',
  `base_price` int(11) NOT NULL,
  `max_price` int(11) NOT NULL,
  `upcharge_type` enum('NON','MIN','MAX','SUM') NOT NULL DEFAULT 'NON',
  PRIMARY KEY (`kaneva_role_value`,`ugc_usage_id`,`tier_id`)
) ENGINE=InnoDB;

CREATE TABLE `ugc_price_tier_upcharges` (
  `kaneva_role_value` int(11) NOT NULL,
  `ugc_usage_id` int(5) NOT NULL,
  `tier_id` int(5) NOT NULL,
  `property_level_id` int(5) NOT NULL,
  `property_id` int(11) NOT NULL,
  `unit` int(11) NOT NULL DEFAULT '1' COMMENT 'unit size as increments in property_value',
  `rounddown` smallint(1) NOT NULL DEFAULT '0' COMMENT '1 - round down, 0 - round up',
  `unit_upcharge` decimal(11,2) NOT NULL,
  `description` varchar(200) NOT NULL,
  `property_instance_id_start` int(11) NOT NULL DEFAULT '0',
  `property_instance_id_end` int(11) NOT NULL DEFAULT '99999',
  PRIMARY KEY (`kaneva_role_value`,`ugc_usage_id`,`tier_id`,`property_level_id`,`property_id`)
) ENGINE=InnoDB;

CREATE TABLE `ugc_property_levels` (
  `property_level_id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`property_level_id`)
) ENGINE=InnoDB;

CREATE TABLE `ugc_uploads` (
  `upload_id` int(20) NOT NULL AUTO_INCREMENT,
  `upload_type` int(3) NOT NULL,
  `master_upload_id` int(20) NOT NULL,
  `sub_id` bigint(20) NOT NULL,
  `player_id` int(11) NOT NULL,
  `bytes` int(11) NOT NULL,
  `hash` varchar(300) NOT NULL,
  `origName` varchar(100) DEFAULT NULL,
  `uploadName` varchar(300) DEFAULT NULL,
  `summary` varchar(200) DEFAULT NULL,
  `state` int(3) NOT NULL,
  `global_id` int(11) DEFAULT NULL,
  `creationTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL,
  `validation_request_id` int(11) NOT NULL DEFAULT '-1',
  `tier_id` int(5) NOT NULL DEFAULT '-1',
  `base_price` int(11) NOT NULL DEFAULT '0',
  `base_global_id` int(11) NOT NULL DEFAULT '0',
  `orig_size` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`upload_id`),
  KEY `IDX_player_master_upload` (`player_id`,`master_upload_id`,`upload_type`,`state`)
) ENGINE=InnoDB COMMENT='Uploaded user generated contents';

CREATE TABLE `ugc_usages` (
  `ugc_usage_id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`ugc_usage_id`)
) ENGINE=InnoDB;

CREATE TABLE `ugc_validation_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `return_code` int(11) NOT NULL,
  `return_msg` varchar(1000) NOT NULL,
  `price_tier` int(5) NOT NULL,
  `base_price` int(11) NOT NULL,
  `upload_id` int(11) NOT NULL,
  `log_time` datetime NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB;

CREATE TABLE `ugc_validation_properties` (
  `property_level_id` int(5) NOT NULL,
  `property_id` int(11) NOT NULL,
  `type` enum('I','F','S','B') NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`property_level_id`,`property_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB;

CREATE TABLE `ugc_validation_requests` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `kaneva_user_id` int(11) NOT NULL,
  `ugc_usage_id` int(5) NOT NULL,
  `request_time` datetime NOT NULL,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB;

CREATE TABLE `ugc_validation_request_details` (
  `request_id` int(11) NOT NULL,
  `property_level_id` int(5) NOT NULL,
  `property_id` int(11) NOT NULL,
  `property_path` varchar(100) NOT NULL,
  `property_value` varchar(100) NOT NULL,
  PRIMARY KEY (`request_id`,`property_level_id`,`property_id`)
) ENGINE=InnoDB;

CREATE TABLE `ugc_validation_server_rules` (
  `kaneva_role_value` int(11) NOT NULL,
  `ugc_usage_id` int(5) NOT NULL,
  `tier_id` int(5) NOT NULL,
  `rule_order` int(5) NOT NULL,
  `property_level_id` int(5) NOT NULL,
  `property_id` int(11) NOT NULL,
  `min_value` decimal(20,6) NOT NULL,
  `max_value` decimal(20,6) NOT NULL,
  `connector` enum('AND','OR') NOT NULL,
  `desc_valid` varchar(200) NOT NULL,
  `desc_failed` varchar(200) NOT NULL,
  `property_instance_id_start` int(11) NOT NULL DEFAULT '0',
  `property_instance_id_end` int(11) NOT NULL DEFAULT '99999',
  PRIMARY KEY (`kaneva_role_value`,`ugc_usage_id`,`tier_id`,`rule_order`)
) ENGINE=InnoDB;

CREATE TABLE `unified_world_ids` (
  `community_id` int(11) NOT NULL,
  `world_type` varchar(20) NOT NULL,
  `game_id` int(11) DEFAULT NULL,
  `zone_index` int(11) DEFAULT NULL,
  `zone_index_plain` int(11) DEFAULT NULL,
  `zone_instance_id` int(11) DEFAULT NULL,
  `zone_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`community_id`),
  KEY `uwi_zi_zt_idx` (`zone_type`,`zone_instance_id`),
  KEY `uwi_game_id_fk_idx` (`game_id`),
  CONSTRAINT `uwi_game_id_fk` FOREIGN KEY (`game_id`) REFERENCES `developer`.`games` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `uwi_comm_id_fk` FOREIGN KEY (`community_id`) REFERENCES `kaneva`.`communities` (`community_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Join table tying worlds to their various table ids, i.e., game_id, community_id, channel_zone_id.  Only for worlds that have kaneva.communities records.';

CREATE TABLE `visit_log_95133` (
  `visit_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_type` tinyint(4) NOT NULL DEFAULT '0',
  `zone_index` int(11) NOT NULL DEFAULT '0',
  `zone_instance_id` int(11) NOT NULL DEFAULT '0',
  `game_id` int(11) NOT NULL DEFAULT '0',
  `kaneva_user_id` int(11) NOT NULL DEFAULT '0',
  `visited_at` datetime NOT NULL DEFAULT '2010-01-01 00:00:00',
  PRIMARY KEY (`visit_log_id`),
  KEY `IDX_zone1` (`zone_index`),
  KEY `IDX_zone2` (`zone_instance_id`),
  KEY `IDX_game` (`game_id`),
  KEY `IDX_visited_at` (`visited_at`)
) ENGINE=InnoDB;

CREATE TABLE `worlds_cache` (
  `STPURL` varchar(300) NOT NULL DEFAULT '',
  `community_id` int(11) NOT NULL DEFAULT '0',
  `community_type_id` smallint(5) unsigned NOT NULL DEFAULT '1',
  `place_type_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `name_no_spaces` varchar(100) NOT NULL DEFAULT '',
  `display_name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `status_id` int(11) NOT NULL DEFAULT '0',
  `is_public` char(1) NOT NULL DEFAULT 'N',
  `is_adult` char(1) NOT NULL DEFAULT 'N',
  `pass_group_id` int(11) NOT NULL DEFAULT '0',
  `keywords` text,
  `over_21_required` char(1) NOT NULL DEFAULT 'N',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `has_thumbnail` char(1) NOT NULL DEFAULT 'N',
  `thumbnail_small_path` varchar(255) NOT NULL DEFAULT '',
  `thumbnail_medium_path` varchar(255) NOT NULL DEFAULT '',
  `thumbnail_large_path` varchar(255) NOT NULL DEFAULT '',
  `thumbnail_xlarge_path` varchar(255) NOT NULL DEFAULT '',
  `thumbnail_square_path` varchar(255) NOT NULL DEFAULT '',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `creator_username` varchar(100) NOT NULL DEFAULT '',
  `creator_name_no_spaces` varchar(100) NOT NULL DEFAULT '',
  `creator_thumbnail_small_path` varchar(255) NOT NULL DEFAULT '',
  `creator_thumbnail_medium_path` varchar(255) NOT NULL DEFAULT '',
  `creator_thumbnail_large_path` varchar(255) NOT NULL DEFAULT '',
  `creator_thumbnail_xlarge_path` varchar(255) NOT NULL DEFAULT '',
  `creator_thumbnail_square_path` varchar(255) NOT NULL DEFAULT '',
  `game_id` int(10) NOT NULL DEFAULT '0',
  `zone_index` int(11) NOT NULL DEFAULT '0',
  `zone_index_plain` int(11) NOT NULL DEFAULT '0',
  `zone_instance_id` int(11) NOT NULL DEFAULT '0',
  `zone_type` int(11) NOT NULL DEFAULT '0',
  `number_of_members` int(11) NOT NULL DEFAULT '0',
  `number_of_views` int(11) NOT NULL DEFAULT '0',
  `number_times_shared` int(11) NOT NULL DEFAULT '0',
  `wok_raves` bigint(11) NOT NULL DEFAULT '0',
  `number_of_diggs` int(11) NOT NULL DEFAULT '0',
  `number_of_diggs_past_7_days` int(11) NOT NULL DEFAULT '0',
  `population` bigint(11) NOT NULL DEFAULT '0',
  `owner_country` char(2) NOT NULL DEFAULT '99',
  `owner_birth_date` date DEFAULT NULL,
  `owner_under_18` smallint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`community_id`),
  KEY `dn` (`display_name`(10)),
  KEY `n` (`name`(10)),
  KEY `de` (`description`(10)),
  KEY `nm` (`number_of_members`),
  KEY `ip` (`is_public`),
  KEY `pt` (`place_type_id`),
  KEY `cid` (`creator_id`),
  KEY `un` (`creator_username`(10)),
  KEY `cd` (`created_date`),
  KEY `nd` (`number_of_diggs`),
  KEY `nd7` (`number_of_diggs_past_7_days`),
  KEY `gi` (`game_id`),
  KEY `zi` (`zone_index`),
  KEY `ziid` (`zone_instance_id`),
  KEY `wr` (`wok_raves`),
  KEY `c` (`population`),
  KEY `kw` (`keywords`(10)),
  KEY `sid` (`status_id`),
  KEY `ia` (`is_adult`),
  KEY `pgid` (`pass_group_id`),
  KEY `cidx` (`owner_country`),
  KEY `bdidx` (`owner_birth_date`),
  KEY `un18` (`owner_under_18`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `worlds_top_tour_cache` (
  `STPURL` varchar(300) NOT NULL DEFAULT '',
  `community_id` int(11) NOT NULL DEFAULT '0',
  `community_type_id` smallint(5) unsigned NOT NULL DEFAULT '1',
  `place_type_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `name_no_spaces` varchar(100) NOT NULL DEFAULT '',
  `display_name` varchar(64) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `status_id` int(11) NOT NULL DEFAULT '0',
  `is_public` char(1) NOT NULL DEFAULT 'N',
  `is_adult` char(1) NOT NULL DEFAULT 'N',
  `pass_group_id` int(11) NOT NULL DEFAULT '0',
  `keywords` text,
  `over_21_required` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `has_thumbnail` enum('Y','N') NOT NULL DEFAULT 'N',
  `thumbnail_small_path` varchar(255) NOT NULL DEFAULT '',
  `thumbnail_medium_path` varchar(255) NOT NULL DEFAULT '',
  `thumbnail_large_path` varchar(255) NOT NULL DEFAULT '',
  `thumbnail_xlarge_path` varchar(255) NOT NULL DEFAULT '',
  `thumbnail_square_path` varchar(255) NOT NULL DEFAULT '',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `creator_username` varchar(100) NOT NULL DEFAULT '',
  `creator_name_no_spaces` varchar(100) NOT NULL DEFAULT '',
  `creator_thumbnail_small_path` varchar(255) NOT NULL DEFAULT '',
  `creator_thumbnail_medium_path` varchar(255) NOT NULL DEFAULT '',
  `creator_thumbnail_large_path` varchar(255) NOT NULL DEFAULT '',
  `creator_thumbnail_xlarge_path` varchar(255) NOT NULL DEFAULT '',
  `creator_thumbnail_square_path` varchar(255) NOT NULL DEFAULT '',
  `game_id` int(10) NOT NULL DEFAULT '0',
  `zone_index` int(11) NOT NULL DEFAULT '0',
  `zone_index_plain` int(11) NOT NULL DEFAULT '0',
  `zone_instance_id` int(11) NOT NULL DEFAULT '0',
  `zone_type` int(11) NOT NULL DEFAULT '0',
  `number_of_diggs_past_7_days` int(11) NOT NULL DEFAULT '0',
  `loot_spawners` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`community_id`),
  KEY `dn` (`display_name`(10)),
  KEY `n` (`name`(10)),
  KEY `de` (`description`(10)),
  KEY `ip` (`is_public`),
  KEY `pt` (`place_type_id`),
  KEY `cid` (`creator_id`),
  KEY `un` (`creator_username`(10)),
  KEY `cd` (`created_date`),
  KEY `nd7` (`number_of_diggs_past_7_days`),
  KEY `gi` (`game_id`),
  KEY `zi` (`zone_index`),
  KEY `ziid` (`zone_instance_id`),
  KEY `kw` (`keywords`(10)),
  KEY `sid` (`status_id`),
  KEY `ia` (`is_adult`),
  KEY `pgid` (`pass_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `worlds_top_tour_redeems` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `zone_instance_id` int(11) NOT NULL DEFAULT '0',
  `zone_type` int(11) NOT NULL DEFAULT '0',
  `reward_date` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`zone_instance_id`,`zone_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `worlds_to_templates` (
  `community_id` int(10) NOT NULL DEFAULT '0' COMMENT 'id of the community',
  `template_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'id of the template',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date that the worlds_to_template relationship was created.',
  PRIMARY KEY (`community_id`,`template_id`),
  KEY `wtt_template_id_fk` (`template_id`),
  CONSTRAINT `wtt_community_id_fk` FOREIGN KEY (`community_id`) REFERENCES `kaneva`.`communities` (`community_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `wtt_template_id_fk` FOREIGN KEY (`template_id`) REFERENCES `world_templates` (`template_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='stores relationship between a world and template';

CREATE TABLE `world_objects` (
  `world_object_id` char(36) NOT NULL COMMENT 'GUID of item',
  `zone_index` int(11) NOT NULL COMMENT 'index of the zone',
  `trace_id` int(11) NOT NULL COMMENT 'unique id of world item',
  `name` varchar(50) NOT NULL COMMENT 'friendly name of zone item',
  `description` varchar(255) NOT NULL COMMENT 'description of what this is',
  PRIMARY KEY (`world_object_id`),
  KEY `INDEX_wo_zone_index` (`zone_index`)
) ENGINE=InnoDB COMMENT='customizable world objects';

CREATE TABLE `world_object_player_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `world_object_id` char(36) NOT NULL DEFAULT '0',
  `asset_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Kaneva.assets.asset_id',
  `texture_url` varchar(255) NOT NULL COMMENT 'from kaneva assets',
  `zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'Zone index of this obj, denormalized from world_objects',
  `zone_instance_id` int(11) NOT NULL DEFAULT '0' COMMENT 'for instances of zone, this is the id. (player, channel, etc.)',
  `last_updated_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `wobj_id_sidx` (`world_object_id`,`zone_index`,`zone_instance_id`),
  KEY `IDX_zone_index_zid` (`zone_index`,`zone_instance_id`),
  KEY `zone_instance_id` (`zone_instance_id`)
) ENGINE=InnoDB COMMENT='player''s apartment customizations';

CREATE TABLE `world_templates` (
  `template_id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `feature_list` text NOT NULL COMMENT 'Pipe delimited list of template features.',
  `default_category_id` int(11) DEFAULT NULL,
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  `status_id` int(10) unsigned NOT NULL DEFAULT '2',
  `preview_image_path_sm` varchar(255) NOT NULL DEFAULT '',
  `preview_image_path_me` varchar(255) NOT NULL DEFAULT '',
  `preview_image_path_lg` varchar(255) NOT NULL DEFAULT '',
  `kep_thumbnail_id` int(10) NOT NULL DEFAULT '0',
  `default_thumbnail` varchar(255) NOT NULL DEFAULT '',
  `first_public_release_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date that the template was made public.',
  `incubator_hosted` char(1) NOT NULL DEFAULT 'N' COMMENT 'Whether or not worlds of the template type are hosted on the incubator or WoK.',
  `pass_group_id` int(11) NOT NULL DEFAULT '0',
  `grandfather_date` datetime DEFAULT NULL,
  `population_multiplier` int(10) unsigned DEFAULT '1',
  `private_on_creation` char(1) NOT NULL DEFAULT 'N' COMMENT 'Indicates whether or not newly-created worlds based upon this template should be marked Private by default.',
  PRIMARY KEY (`template_id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `wt_default_category_id_fk` (`default_category_id`),
  KEY `wt_status_id_fk` (`status_id`),
  KEY `wt_pass_group_id_fk` (`pass_group_id`),
  CONSTRAINT `wt_default_category_id_fk` FOREIGN KEY (`default_category_id`) REFERENCES `kaneva`.`community_categories` (`category_id`),
  CONSTRAINT `wt_pass_group_id_fk` FOREIGN KEY (`pass_group_id`) REFERENCES `pass_groups` (`pass_group_id`),
  CONSTRAINT `wt_status_id_fk` FOREIGN KEY (`status_id`) REFERENCES `world_template_status` (`world_template_status_id`)
) ENGINE=InnoDB;

CREATE TABLE `world_template_default_items` (
  `template_id` int(10) unsigned NOT NULL,
  `global_id` int(11) NOT NULL,
  PRIMARY KEY (`template_id`,`global_id`),
  KEY `wtdi_global_id_fk` (`global_id`),
  CONSTRAINT `wtdi_global_id_fk` FOREIGN KEY (`global_id`) REFERENCES `items` (`global_id`),
  CONSTRAINT `wtdi_template_id_fk` FOREIGN KEY (`template_id`) REFERENCES `world_templates` (`template_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='list of default inventory items for world templates';

CREATE TABLE `world_template_global_id` (
  `template_id` int(10) unsigned NOT NULL,
  `global_id` int(11) NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `wtgi_global_id_fk` (`global_id`),
  CONSTRAINT `wtgi_global_id_fk` FOREIGN KEY (`global_id`) REFERENCES `items` (`global_id`),
  CONSTRAINT `wtgi_template_id_fk` FOREIGN KEY (`template_id`) REFERENCES `world_templates` (`template_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Ties a world template to the global_id of the UGC zone that creates it.';

CREATE TABLE `world_template_status` (
  `world_template_status_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Enumeration value, to be used internally by us.  Does not auto-increment, and will not be set by users.',
  `world_template_status` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`world_template_status_id`)
) ENGINE=InnoDB COMMENT='Enumeration of world template status, including ''Active'', ''Pending'', ''Archived'', etc.';

CREATE TABLE `zone_deed_item_lookup` (
  `zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'Zone index from the supported worlds table.',
  `global_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Globla_id of the deed for this zone in the items table.',
  PRIMARY KEY (`zone_index`),
  CONSTRAINT `zone_deed_item_lookup_ibfk_1` FOREIGN KEY (`zone_index`) REFERENCES `supported_worlds` (`zone_index`)
) ENGINE=InnoDB COMMENT='This table is used to look up the deed associated with a zon';

CREATE TABLE `zone_raves` (
  `zone_instance_id` int(11) NOT NULL COMMENT 'FK to channel_zones',
  `zone_type` int(11) NOT NULL COMMENT 'FK to channel_zones',
  `kaneva_user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'user that performed the rave',
  `rave_count` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`kaneva_user_id`,`created_date`),
  KEY `zone_index` (`zone_instance_id`,`zone_type`),
  KEY `IDX_zonetype_createddate` (`zone_type`,`created_date`),
  KEY `IDX_created_date` (`created_date`)
) ENGINE=InnoDB COMMENT='Holds zone raves given';

CREATE TABLE `zone_raves_old` (
  `zone_instance_id` int(11) NOT NULL COMMENT 'FK to channel_zones',
  `zone_type` int(11) NOT NULL COMMENT 'FK to channel_zones',
  `kaneva_user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'user that performed the rave',
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`zone_instance_id`,`zone_type`,`kaneva_user_id`)
) ENGINE=InnoDB;

CREATE TABLE `zone_unique_visits` (
  `kaneva_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'User that visited zone',
  `zone_instance_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to channel_zones. Community_id or player_id',
  `zone_type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to channel_zones',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`kaneva_user_id`,`zone_instance_id`,`zone_type`)
) ENGINE=InnoDB;

CREATE TABLE `zone_weights` (
  `zone_index` int(11) NOT NULL,
  `name` varchar(260) NOT NULL DEFAULT '' COMMENT 'zone name or desc',
  `weight` int(11) NOT NULL DEFAULT '1' COMMENT 'relative to other zones in this table',
  `affinity_flag` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'bit indicating type see developer.affinity_flag_ids',
  PRIMARY KEY (`zone_index`)
) ENGINE=InnoDB COMMENT='zones that should not be on same server, if possible';

CREATE TABLE `_temp_dup` (
  `dup_id` int(11) DEFAULT NULL
) ENGINE=InnoDB;

CREATE TABLE `__apartment_cache` (
  `community_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `number_of_diggs` int(11) DEFAULT '0',
  `number_of_members` int(11) DEFAULT '0',
  `is_public` char(1) NOT NULL DEFAULT '',
  `is_adult` char(1) DEFAULT NULL,
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(64) NOT NULL COMMENT 'name shown in client',
  `zone_index` int(11) NOT NULL DEFAULT '0' COMMENT 'zoneIndex of their apt',
  `zone_instance_id` int(11) NOT NULL COMMENT 'FK to players',
  `count` bigint(11) NOT NULL DEFAULT '0',
  `wok_raves` bigint(11) NOT NULL DEFAULT '0',
  `creator_username` varchar(22) NOT NULL,
  `keywords` text,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `over_21_required` enum('Y','N') NOT NULL DEFAULT 'N',
  `pass_group_id` int(11) NOT NULL DEFAULT '0',
  KEY `comm_id` (`community_id`),
  KEY `name` (`name`(10)),
  KEY `descr` (`description`(10)),
  KEY `nd` (`number_of_diggs`),
  KEY `nm` (`number_of_members`),
  KEY `ip` (`is_public`),
  KEY `ia` (`is_adult`),
  KEY `cid` (`creator_id`),
  KEY `dn` (`display_name`(10)),
  KEY `zi` (`zone_index`),
  KEY `ziid` (`zone_instance_id`),
  KEY `c` (`count`),
  KEY `wr` (`wok_raves`),
  KEY `cun` (`creator_username`(10)),
  KEY `kw` (`keywords`(10)),
  KEY `st` (`status_id`),
  KEY `caid` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

