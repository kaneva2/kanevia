-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS z_active_server_engines;

DELIMITER ;;

 CREATE VIEW `z_active_server_engines` AS SELECT `vw_game_servers`.`server_id` AS `server_id`,`vw_game_servers`.`ip_address` AS `ip_address`,`vw_game_servers`.`port` AS `port`,`vw_game_servers`.`number_of_players` AS `number_of_players`,`vw_game_servers`.`visibility_id` AS `visibility_id`,concat( 
 IF( ( `vw_game_servers`.`affinity_mask` & 1) ,_latin1'A',_latin1'') ,if( ( `vw_game_servers`.`affinity_mask` & 2) ,_latin1'H',_latin1'') ,if( ( `vw_game_servers`.`affinity_mask` & 4) ,_latin1'P',_latin1'') ,if( ( `vw_game_servers`.`affinity_mask` & 8) ,_latin1'M',_latin1'') ,if( ( `vw_game_servers`.`affinity_mask` & 16) ,_latin1'E',_latin1'') ,if( ( `vw_game_servers`.`affinity_mask` & 32) ,_latin1'K',_latin1'') )  AS `affinity`,`vw_game_servers`.`server_status_id` AS `server_status_id`,`vw_game_servers`.`last_ping_datetime` AS `last_ping_datetime`,`vw_game_servers`.`server_started_date` AS `server_started_date`,`vw_game_servers`.`max_players` AS `max_players` 
 FROM `wok`.`vw_game_servers` 
 ORDER BY `vw_game_servers`.`ip_address`
;;
DELIMITER ;