-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS expand_group;

DELIMITER ;;
CREATE PROCEDURE `expand_group`( IN __group_id INT(11) )
BEGIN
-- part of schema abstration to remove all Kaneva references from C++ 
-- and move them to SPs, which can be site-specific
	SELECT  players.player_id, players.name 
	  FROM  players, 
	        vw_friend_group_friends 
	 WHERE  vw_friend_group_friends.friend_group_id =  __group_id
	   AND  vw_friend_group_friends.friend_id =  players.vw_user_id 
	   AND  players.in_game =  'T';
END
;;
DELIMITER ;