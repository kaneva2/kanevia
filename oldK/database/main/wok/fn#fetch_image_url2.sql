-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS fetch_image_url2;

DELIMITER ;;
CREATE FUNCTION `fetch_image_url2`( __asset_id INT(11), 
																			__is_asset ENUM('T','F') ) RETURNS varchar(1000) CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE __ret VARCHAR(1000) DEFAULT NULL;
	DECLARE __thumbnail_url VARCHAR(1000) DEFAULT NULL;
	DECLARE __file_size INT DEFAULT 0;
	IF __is_asset = 'T' THEN 
		SELECT image_full_path, thumbnail_xlarge_path, file_size  INTO __ret, __thumbnail_url, __file_size
		  FROM kaneva.assets a
		 WHERE a.asset_id = __asset_id;
	ELSE 
		SELECT thumbnail_xlarge_path INTO __ret
	      FROM kaneva.communities_personal cb 
         WHERE cb.creator_id = __asset_id;
	END IF;
	IF __ret IS NULL THEN
		CALL logMsg( 'fetch_image_url', 'ERROR', concat( 'Couldnt get user image for asset: ', cast( __asset_id as char )  ) );
	ELSEIF 	__file_size	> 256000 THEN
		RETURN __thumbnail_url;
	END IF;
	
	RETURN __ret;
	
END
;;
DELIMITER ;