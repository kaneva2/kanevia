-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS check_for_zone_playlist;

DELIMITER ;;
CREATE PROCEDURE `check_for_zone_playlist`( IN __zone_index INT(11), IN __attachable INT,
	IN __zone_instance_id INT(11), IN __global_id INT(11), OUT __asset_group_id INT(11) )
BEGIN

IF __attachable = 0 THEN 
	-- if there are other objects in this zone of the same glid, playing a video, this one 
	-- should play id too
	SELECT CAST( p.`value` AS SIGNED ) INTO __asset_group_id
	  FROM dynamic_objects `do` 
	  INNER JOIN dynamic_object_parameters p ON `do`.obj_placement_id = p.obj_placement_id
	 WHERE zone_index = __zone_index
	   AND zone_instance_id = __zone_instance_id
	   AND global_id = __global_id
	   AND param_type_id = 16 -- asset_group_id
	   LIMIT 1;
END IF;	   

END
;;
DELIMITER ;