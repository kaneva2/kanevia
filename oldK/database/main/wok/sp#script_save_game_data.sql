-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS script_save_game_data;

DELIMITER ;;
CREATE PROCEDURE `script_save_game_data`( IN __game_id INT(11),
												IN __attribute VARCHAR(16),
												IN __value VARCHAR(128) )
BEGIN
-- called to insert or update game data for a given game
 INSERT INTO script_game_data(`game_id`, `attribute`, `value`) 
   VALUES (__game_id, __attribute, __value)
   ON DUPLICATE KEY UPDATE `attribute` = __attribute, `value` = __value;

END
;;
DELIMITER ;