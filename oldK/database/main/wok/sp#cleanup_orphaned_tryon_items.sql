-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS cleanup_orphaned_tryon_items;

DELIMITER ;;
CREATE PROCEDURE `cleanup_orphaned_tryon_items`()
BEGIN
DECLARE _player_id BIGINT;
DECLARE __table_RowCnt INT;
DECLARE __del_cnt INT;
DECLARE __message_text VARCHAR(200);
DECLARE i INT;
DECLARE done INT DEFAULT 0;
DECLARE arch_cursor CURSOR FOR 
	SELECT player_id 
		FROM wok.players p
		INNER JOIN wok.game_users gu ON p.user_id = gu.user_id
		WHERE last_logon > ADDDATE(NOW(), INTERVAL -2 DAY);
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;
SET i = 0;
OPEN arch_cursor;
SET __table_RowCnt = (SELECT FOUND_ROWS());
SELECT CONCAT('Starting cleanup ... ',CAST(__table_RowCnt AS CHAR),' players to process') INTO __message_text;
CALL add_maintenance_log (DATABASE(),'cleanup_orphaned_tryon_items',__message_text);
REPEAT
    FETCH arch_cursor INTO _player_id;
    IF NOT done THEN
                DELETE sc FROM sound_customizations sc 
				INNER JOIN dynamic_objects dyo ON sc.obj_placement_id = dyo.obj_placement_id
				WHERE dyo.player_id = _player_id AND dyo.expired_date < NOW();
                SET __del_cnt = COALESCE(ROW_COUNT(),0);
                IF __del_cnt > 0 THEN 
                                SELECT CONCAT('Deleted: ',CAST(__del_cnt AS CHAR),' orphaned sound_customizations from player id: ',CAST(_player_id AS CHAR)) INTO __message_text;
				CALL add_maintenance_log (DATABASE(),'cleanup_orphaned_tryon_items',__message_text); 
                                END IF;
                DELETE FROM wok.dynamic_objects WHERE player_id = _player_id AND expired_date < NOW();
                SET __del_cnt = COALESCE(ROW_COUNT(),0);
                IF __del_cnt > 0 THEN 
                                SELECT CONCAT('Deleted: ',CAST(__del_cnt AS CHAR),' orphaned dynamic_objects from player id: ',CAST(_player_id AS CHAR)) INTO __message_text;
				CALL add_maintenance_log (DATABASE(),'cleanup_orphaned_tryon_items',__message_text); 
                                SET i = i + __del_cnt;
                                END IF;
    END IF;             
UNTIL done END REPEAT;
CLOSE arch_cursor;
SELECT CONCAT('Deleted: ',CAST(i AS CHAR),' rows from dynamic_objects') INTO __message_text;
CALL add_maintenance_log (DATABASE(),'cleanup_orphaned_tryon_items',__message_text); 
END
;;
DELIMITER ;