-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_dyn_obj_media_asset;

DELIMITER ;;
CREATE PROCEDURE `update_dyn_obj_media_asset`( IN __asset_id INT(11), IN __offsite_url VARCHAR(1000), IN __asset_type_id INT(11), IN __mature CHAR, IN __obj_id INT(11), IN __zone_index INT(11), IN __instance_id INT(11), IN __stop_playing BINARY(1))
BEGIN
-- update the media parameters for a dynamic object 
--
-- params:
--      assetId = kaneva.assetId - in process of retiring the use of assetId
--		  offsite_url - kaneva.assets.asset_offsite_id = url - passed to proc - 3d game has no kaneva.assets
--      asset_type_id = passed to proc - 3d game has no kaneva.assets
-- 	  mature = passed to proc - 3d game has no kaneva.assets
-- 	  obj_id = dyn object id
--      zone_index =
--		  instance_id =
--  	  stop_playing	= used instead of asset_id=0 to stop playing the asset.
	DECLARE __old_global_id INT(11) DEFAULT 0;
	DECLARE __new_obj_placement_id INT(11) DEFAULT 0;
	DECLARE __parameters VARCHAR(1000) DEFAULT NULL;
	DECLARE __pass_group_id INT;
	SELECT `global_id` INTO __old_global_id
	  FROM `dynamic_objects` do LEFT OUTER JOIN dynamic_object_parameters dop ON do.obj_placement_id = dop.obj_placement_id AND dop.param_type_id = 20
	 WHERE do.`obj_placement_id` = __obj_id
    FOR UPDATE;
	SET __parameters = '';
	IF __old_global_id IS NOT NULL THEN 
		IF __stop_playing = 1 THEN
			SET __offsite_url = '';
			SET __parameters = '';
		ELSE
			IF __mature = 'Y' AND NOT is_mature_zone(__zone_index,__instance_id) THEN
				-- can't do it!
				SET __offsite_url = NULL;
				SELECT 12; -- need AP
			END IF;
		END IF;
		IF __offsite_url IS NOT NULL THEN
			IF __asset_type_id = 1 THEN
				
				
				INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
				VALUES (__obj_id, 18, __parameters) ON DUPLICATE KEY UPDATE value = __parameters;
				INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
				VALUES (__obj_id, 17, __offsite_url) ON DUPLICATE KEY UPDATE value = __offsite_url;
				
				INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
				VALUES (__obj_id, 14, __asset_id) ON DUPLICATE KEY UPDATE value = __asset_id;
				
				INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
				VALUES (__obj_id, 19, __asset_id) ON DUPLICATE KEY UPDATE value = __asset_id;
				DELETE FROM dynamic_object_parameters 
				  WHERE obj_placement_id = __obj_id 
				    AND param_type_id = 16; 
			ELSE
				INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
				VALUES (__obj_id, 18, __parameters) ON DUPLICATE KEY UPDATE value = __parameters;
				INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
				VALUES (__obj_id, 17, __offsite_url) ON DUPLICATE KEY UPDATE value = __offsite_url;
				
				INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
				VALUES (__obj_id, 19, __asset_id) ON DUPLICATE KEY UPDATE value = __asset_id;
				DELETE FROM dynamic_object_parameters 
				  WHERE obj_placement_id = __obj_id 
			    AND param_type_id = 16; -- asset_group_id
			END IF;

			-- Delete the playlist
			DELETE FROM `dynamic_object_playlists`
			WHERE `zone_index` = __zone_index
			  AND `zone_instance_id` = __instance_id
			  AND `obj_placement_id` = __obj_id;

			SELECT __offsite_url, __parameters, __new_obj_placement_id; 
		END IF;
	END IF;
END
;;
DELIMITER ;