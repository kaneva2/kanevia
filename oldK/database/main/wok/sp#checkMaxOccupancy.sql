-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS checkMaxOccupancy;

DELIMITER ;;
CREATE PROCEDURE `checkMaxOccupancy`( IN       playerId       INT
                                    , IN     kanevaUserId   INT
                                    , IN     zoneIndex      INT
                                    , INOUT  instanceId     INT
                                    , INOUT  isGm           CHAR
                                    , IN     country        CHAR(2)
                                    , IN     birthDate      DATE
                                    , IN     gotoPlayer     BOOLEAN
                                    , OUT    reasonCode     INT )
BEGIN

	DECLARE zoneType INT DEFAULT 0;
	DECLARE role INT; -- moderator(2), owner(1)
	DECLARE softLimit INT DEFAULT 30;
	DECLARE hardLimit INT DEFAULT 30;
	DECLARE curPopulation INT DEFAULT 0;

   DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;

	SET zoneType = zoneType( zoneIndex );
	SET reasonCode = 5; -- zone full, default return code
	
   CALL logging.logMsg( 'TRACE'
                        , SCHEMA()
                        , 'checkMaxOccupancy'
                        , concat( 'begin( ', playerId  , ') ' )
                        );

-- call logDebugMsg( 'checkMaxOccupancy', concat( 'zoneType is ', ifnull(cast(zoneType as char),'<null>'), ' and zi is ', ifnull(hex(zoneIndex),'<null>'), ' instanceId ', ifnull(cast(instanceId as char),'<null>'), ' goto is ', ifnull(cast( gotoPlayer as char ),'<null>'), ' isGm is ', ifnull(isGm,'<null>') ) );

	IF zoneType = 3 AND instanceId = playerId THEN -- 3 = apt
		-- owner
		SET isGm = 'T'; 
		SET reasonCode = 0;
	ELSEIF isGm = 'T' THEN
		SET reasonCode = 0;
	ELSEIF zoneType = 6 THEN -- BB
		CALL communityRole( kanevaUserId, instanceId, role );
		IF role = 1 OR role = 2 THEN
			SET reasonCode = 0;
		END IF;
		IF role = 1 THEN
			SET isGm = 'T';
		END IF;
	END IF;

	call logDebugMsg( 'checkMaxOccupancy', concat( '   after gm check, reasonCode is ', cast(reasonCode as char ) ) );
	
	IF reasonCode <> 0 THEN 


		CALL getZoneLimits( zoneIndex, instanceId, softLimit, hardLimit );

		IF gotoPlayer OR (zoneType = 3 OR zoneType = 6) THEN -- apt or BB
		
         CALL logging.logMsg( 'TRACE'
                              , SCHEMA()
                              , 'checkMaxOccupancy'
                              , concat( 'Check apartment(', playerId  , ') ' )
                              );

			SELECT `count` INTO curPopulation
			  FROM summary_active_population
		     WHERE zone_index = zoneIndex
		       AND zone_instance_id = instanceId;

			IF curPopulation < softLimit OR 
			   (gotoPlayer AND curPopulation < hardLimit ) THEN
			   SET reasonCode = 0;
			END IF;
			
         CALL logging.logMsg( 'DEBUG'
                              , SCHEMA()
                              , 'checkMaxOccupancy'
                              , concat( 'BB or APT pop: ', cast( curPopulation as char )
                                        , ' limits are ', cast( softLimit as char )
                                        , '/', cast(hardLimit as char )
                                        , ' reasonCode: ', cast(reasonCode as char ) )
                              );

	
		ELSEIF zoneType = 4 THEN

            CALL logging.logMsg( 'TRACE'
                                 , SCHEMA()
                                 , 'getBestPermanentZone'
                                 , concat( 'Check apartment(', playerId  , ') ' )
                                 );
			-- never get here if goto player
			CALL getBestPermanentZone( zoneIndex, softLimit, instanceId, curPopulation, playerId, country, birthDate );
			
			SET reasonCode = 0;
			
		ELSE
			IF zoneType = 0 THEN -- let these go thru
				SET reasonCode = 0;
            CALL logging.logMsg( 'DEBUG'
                                 , SCHEMA()
                                 , 'checkMaxOccupancy'
                                 , concat('Allowing public zone access for 0x', hex( zoneIndex ) )
                                 );
         ELSE

            CALL logging.logMsg( 'WARN'
                                 , SCHEMA()
                                 , 'checkMaxOccupancy'
                                 , concat('***Unhandled zone type ', cast( zoneType as char ) ) );
			END IF;
		END IF;
	ELSEIF zoneType = 4 AND instanceId = 0 THEN -- GM going to public space
		-- need to get instanceId of perm zone, pass in 50000 for soft limit
		-- to basically ignore it for gms
		CALL getBestPermanentZone( zoneIndex, 50000, instanceId, curPopulation, playerId, country, birthDate );
	END IF;
	
END
;;
DELIMITER ;