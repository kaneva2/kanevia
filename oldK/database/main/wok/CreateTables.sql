-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

/*
SQLyog Enterprise - MySQL GUI v7.14 
MySQL - 5.0.67-community-log : Database - wok
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `access_type` */

CREATE TABLE `access_type` (
  `access_type_id` smallint(5) unsigned NOT NULL auto_increment,
  `access_type` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`access_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `active_logins` (
	user_id int(11) not null, 
	server_id int(11) not null, 
	created_date datetime not null,
	ip_address varchar(30)
	)
	ENGINE = InnoDB;
	
create unique index active_logins_idx on active_logins(user_id,server_id);

/*Table structure for table `apartment_cache` */

CREATE TABLE `apartment_cache` (
  `community_id` int(11) NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `number_of_diggs` int(11) default '0',
  `number_of_members` int(11) default '0',
  `is_public` char(1) NOT NULL default '',
  `is_adult` char(1) default NULL,
  `creator_id` int(11) NOT NULL default '0',
  `display_name` varchar(64) NOT NULL COMMENT 'name shown in client',
  `zone_index` int(11) NOT NULL default '0' COMMENT 'zoneIndex of their apt',
  `zone_instance_id` int(11) NOT NULL COMMENT 'FK to players',
  `count` bigint(11) NOT NULL default '0',
  `wok_raves` bigint(11) NOT NULL default '0',
  `creator_username` varchar(22) NOT NULL,
  `keywords` text,
  `status_id` int(11) NOT NULL default '0',
  `category_id` int(11) NOT NULL default '0',
  `over_21_required` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`community_id`),
  KEY `name` (`name`(10)),
  KEY `descr` (`description`(10)),
  KEY `nd` (`number_of_diggs`),
  KEY `nm` (`number_of_members`),
  KEY `ip` (`is_public`),
  KEY `ia` (`is_adult`),
  KEY `cid` (`creator_id`),
  KEY `dn` (`display_name`(10)),
  KEY `zi` (`zone_index`),
  KEY `ziid` (`zone_instance_id`),
  KEY `c` (`count`),
  KEY `wr` (`wok_raves`),
  KEY `cun` (`creator_username`(10)),
  KEY `kw` (`keywords`(10)),
  KEY `st` (`status_id`),
  KEY `caid` (`category_id`),
  FULLTEXT KEY `ft` (`name`,`description`,`display_name`,`creator_username`,`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

/*Table structure for table `catalogs_3d` */

CREATE TABLE `catalogs_3d` (
  `catalog_id` int(11) unsigned NOT NULL auto_increment COMMENT 'PK',
  `name` varchar(80) NOT NULL,
  PRIMARY KEY  (`catalog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='catalog names for 3d gifts';

/*Table structure for table `channel_zone_cover_charges` */

CREATE TABLE `channel_zone_cover_charges` (
  `kaneva_user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `zone_index` int(11) NOT NULL COMMENT 'FK to channel_zones',
  `zone_instance_id` int(11) NOT NULL COMMENT 'FK to channel_zones',
  `expiration_date` datetime NOT NULL COMMENT 'when this cover charge expires',
  `created_date` datetime NOT NULL COMMENT 'when this cover charge was purchases',
  PRIMARY KEY  (`kaneva_user_id`,`zone_index`,`zone_instance_id`),
  KEY `kaneva_user_id` (`kaneva_user_id`,`zone_index`,`zone_instance_id`,`expiration_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='track cover charges for players and zones';

/*Table structure for table `channel_zones` */

CREATE TABLE `channel_zones` (
  `channel_zone_id` int(11) NOT NULL auto_increment COMMENT 'PK',
  `kaneva_user_id` int(11) NOT NULL COMMENT 'owner. FK to gamer_users/kaneva.users',
  `zone_index` int(11) NOT NULL COMMENT 'index of zone, w/o instanceId',
  `zone_index_plain` int(11) NOT NULL COMMENT 'just the zone index FK to supported_worlds',
  `zone_type` int(11) NOT NULL default '6' COMMENT 'type of zone ',
  `zone_instance_id` int(11) NOT NULL default '0' COMMENT 'if zone is instanced, this is instanceId, usually Kaneva.community_id',
  `name` varchar(200) NOT NULL COMMENT 'zone name if Kaneva zone',
  `tied_to_server` enum('NOT_TIED','TIED','PERMANENTLY_TIED','PENDING_TIED') NOT NULL default 'NOT_TIED' COMMENT 'used to control if server:port must be used',
  `created_date` datetime NOT NULL,
  `raves` int(11) NOT NULL default '0' COMMENT 'Raves for this place',
  `access_rights` int(11) NOT NULL default '0' COMMENT 'Access rights to apartment or broadband zone',
  `access_friend_group_id` int(11) NOT NULL default '0' COMMENT 'For access_rights equals friends(1), the friend group id',
  `server_id` int(11) NOT NULL COMMENT 'FK to current_game_engines.engine_id',
  `cover_charge` int(11) NOT NULL default '0' COMMENT 'if non-zero amt charged to enter zone',
  `visibility` int(10) unsigned NOT NULL default '100' COMMENT 'who sees this zone 0 = no one see or spawn, 50 = no one sees anyone spawn, 100 = everyone',
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `number_unique_visits` int(10) unsigned NOT NULL default '0' COMMENT 'Number of unique Visits to the zone',
  PRIMARY KEY  (`channel_zone_id`),
  KEY `INDEX_cz_zoneindex` (`zone_index`),
  KEY `INDEX_cz_zoneinstance` (`zone_instance_id`),
  KEY `zone_type` (`zone_type`),
  KEY `newindex` (`kaneva_user_id`),
  KEY `IDX_zone_index` (`zone_index_plain`),
  KEY `cd` (`created_date`),
  KEY `tts` (`tied_to_server`),
  KEY `server_id` (`server_id`),
  KEY `zi_ziid_tts` (`zone_index`,`zone_instance_id`,`tied_to_server`)
) ENGINE=InnoDB AUTO_INCREMENT=952396 DEFAULT CHARSET=latin1 COMMENT='Kaneva and channel zones';

/*Table structure for table `clan_members` */

CREATE TABLE `clan_members` (
  `clan_id` int(11) unsigned NOT NULL default '0',
  `player_id` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`player_id`),
  KEY `clan_id` (`clan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='link players to clans';

/*Table structure for table `clans` */

CREATE TABLE `clans` (
  `clan_id` int(11) unsigned NOT NULL auto_increment,
  `clan_name` varchar(35) NOT NULL default ' ',
  `owner_id` int(11) unsigned NOT NULL default '0' COMMENT 'FK to player_id',
  `housing_placement_id` int(11) unsigned NOT NULL default '0' COMMENT 'FK to housing_placements',
  `arena_points` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`clan_id`),
  UNIQUE KEY `owner_id` (`owner_id`),
  UNIQUE KEY `clan_name` (`clan_name`)
) ENGINE=InnoDB AUTO_INCREMENT=200655 DEFAULT CHARSET=latin1 COMMENT='base clan table';

/*Table structure for table `cod_items` */

CREATE TABLE `cod_items` (
  `cod_id` int(11) NOT NULL auto_increment COMMENT 'PK',
  `to_id` int(11) NOT NULL COMMENT 'denormalized from messages',
  `message_id` int(11) NOT NULL COMMENT 'FK to kaneva.messages',
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `status` enum('U','A','R') NOT NULL default 'U' COMMENT 'Unaccpeted, Accepted, Rejected',
  `amount` int(11) NOT NULL default '0' COMMENT 'amount requested for items',
  `quantity` int(11) NOT NULL default '1' COMMENT 'number of items being given',
  `sent_time` timestamp NOT NULL default CURRENT_TIMESTAMP COMMENT 'when the item was sent',
  PRIMARY KEY  (`cod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='M2M between wok.items and kaneva.messages for tracking C.O.D';

/*Table structure for table `currency_type` */

CREATE TABLE `currency_type` (
  `currency_type_id` smallint(5) unsigned NOT NULL,
  `currency_type` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`currency_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `daily_dance_levels` */

CREATE TABLE `daily_dance_levels` (
  `user_id` int(11) unsigned NOT NULL default '0' COMMENT 'User that danced to level',
  `level_id` int(11) unsigned NOT NULL default '0' COMMENT 'Level danced to',
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`user_id`,`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `dance_level_actions` */

CREATE TABLE `dance_level_actions` (
  `dance_level_action_id` int(11) NOT NULL auto_increment,
  `dance_level` int(11) NOT NULL default '0',
  `action_id` int(11) NOT NULL default '0',
  `perfect_action_id` int(11) NOT NULL default '0',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`dance_level_action_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Table structure for table `ddr` */

CREATE TABLE `ddr` (
  `game_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `seed` int(11) NOT NULL,
  `num_seconds_to_play` int(11) NOT NULL,
  `num_moves` int(11) NOT NULL,
  `num_misses` int(11) NOT NULL,
  `perfect_score` int(11) NOT NULL,
  `great_score` int(11) NOT NULL,
  `good_score` int(11) NOT NULL,
  `finish_bonus_score` int(11) NOT NULL,
  `level_increase_score` int(11) NOT NULL,
  `level_bonus_trigger_num` int(11) NOT NULL,
  `level_bonus_score` int(11) NOT NULL,
  `perfect_score_ceiling` int(11) NOT NULL,
  `great_score_ceiling` int(11) NOT NULL,
  `good_score_ceiling` int(11) NOT NULL,
  `num_sub_levels` int(11) NOT NULL,
  `last_level_reduction_ms` int(11) NOT NULL default '0',
  PRIMARY KEY  (`game_id`,`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `ddr_animations` */

CREATE TABLE `ddr_animations` (
  `game_id` int(11) NOT NULL,
  `type` enum('FLARE','FINAL_FLARE','MISS','LEVELUP','PERFECT','IDLE') NOT NULL,
  `level_id` int(11) NOT NULL,
  `animation_id` int(11) NOT NULL,
  `play_order` int(11) NOT NULL,
  PRIMARY KEY  (`game_id`,`level_id`,`animation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `ddr_globals` */

CREATE TABLE `ddr_globals` (
  `game_id` int(11) NOT NULL,
  `max_players` int(11) NOT NULL,
  `notify_inactive` int(11) NOT NULL,
  `purge_timeout` int(11) NOT NULL,
  PRIMARY KEY  (`game_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `ddr_high_scores` */

CREATE TABLE `ddr_high_scores` (
  `dynamic_object_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `high_score` int(11) NOT NULL,
  `player_name` varchar(64) NOT NULL,
  `created_date` datetime NOT NULL,
  `object_id` int(11) NOT NULL default '0',
  `zone_index` int(11) NOT NULL default '0',
  KEY `doi` (`dynamic_object_id`),
  KEY `oz` (`object_id`,`zone_index`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `ddr_high_scores_archive` */

CREATE TABLE `ddr_high_scores_archive` (
  `dynamic_object_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `high_score` int(11) NOT NULL,
  `player_name` varchar(64) NOT NULL,
  `created_date` datetime NOT NULL,
  `archive_date` datetime NOT NULL,
  `object_id` int(11) NOT NULL default '0',
  `zone_index` int(11) NOT NULL default '0',
  `dhsa_id` int(10) unsigned NOT NULL auto_increment,
  PRIMARY KEY  (`dhsa_id`),
  KEY `doi` (`dynamic_object_id`),
  KEY `object_id` (`object_id`,`zone_index`),
  KEY `oz` (`object_id`,`zone_index`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `ddr_scores` */

CREATE TABLE `ddr_scores` (
  `game_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `instance_id` int(11) NOT NULL auto_increment,
  `score` int(11) NOT NULL,
  `num_perfects` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `num_misses` int(11) NOT NULL default '0',
  `num_perfects_inarow` int(11) NOT NULL,
  `player_name` varchar(64) NOT NULL,
  `dynamic_object_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL default '0',
  `zone_index` int(11) NOT NULL default '0',
  PRIMARY KEY  (`instance_id`),
  KEY `gpdoz` (`game_id`,`player_id`,`dynamic_object_id`,`object_id`,`zone_index`)
) ENGINE=InnoDB AUTO_INCREMENT=3345563 DEFAULT CHARSET=latin1;

/*Table structure for table `ddr_unique_visits` */

CREATE TABLE `ddr_unique_visits` (
  `kaneva_user_id` int(11) unsigned NOT NULL default '0' COMMENT 'User that visited dance floor',
  `dynamic_object_id` int(11) unsigned NOT NULL default '0' COMMENT 'FK to dynamic_objects.obj_placement_id',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`kaneva_user_id`,`dynamic_object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `deformable_configs` */

CREATE TABLE `deformable_configs` (
  `deformable_id` int(11) NOT NULL auto_increment COMMENT 'PK ',
  `sequence` int(11) NOT NULL COMMENT 'sort key to key cfgs in order',
  `player_id` int(11) NOT NULL COMMENT 'player id',
  `equip_id` int(11) NOT NULL default '0' COMMENT 'zero for main body, else id of attached item',
  `deformable_config` int(11) NOT NULL COMMENT 'config id from the skeletal object',
  `custom_material` int(11) NOT NULL COMMENT 'customizable material',
  `custom_color_r` float NOT NULL COMMENT 'color of material',
  `custom_color_g` float NOT NULL,
  `custom_color_b` float NOT NULL,
  PRIMARY KEY  (`deformable_id`),
  KEY `INDEX_defconfig_player` (`player_id`),
  KEY `INDEX_defconfig_seq` (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=90478605 DEFAULT CHARSET=latin1 COMMENT='players'' deformable configs';

/*Table structure for table `dx_diag` */

CREATE TABLE `dx_diag` (
  `diag_id` int(10) unsigned NOT NULL auto_increment COMMENT '1234',
  `diag_date` datetime NOT NULL COMMENT '2007-07-06 00:00:00',
  `user_id` int(10) unsigned NOT NULL COMMENT '1234',
  `operating_system` varchar(60) NOT NULL COMMENT 'Microsoft Windows XP Pro version 5.1',
  `make` varchar(45) NOT NULL COMMENT 'Dell Inc.',
  `model` varchar(60) NOT NULL COMMENT 'Precision M65',
  `bios` varchar(80) NOT NULL COMMENT 'Phoenix RoM Bios PLUS Version 1.10 A07',
  `proc` varchar(60) NOT NULL COMMENT 'Intel(R) Core(TM)2 CPU T7600 @ 2.33Ghz (2CPUs)',
  `memory` varchar(15) NOT NULL COMMENT '2046MB RAM',
  `directx_version` varchar(45) NOT NULL COMMENT 'DirectX 9.0c (4.09.0000.0904)',
  `card_name` varchar(60) NOT NULL COMMENT 'NVIDIA Quadro FX 350M',
  `card_make` varchar(45) NOT NULL COMMENT 'NVIDIA',
  `card_chip_type` varchar(45) NOT NULL COMMENT 'Quadro FX 350M',
  `dac_type` varchar(45) NOT NULL COMMENT 'Integrated RAMDAC',
  `card_memory` varchar(15) NOT NULL COMMENT '512.0 MB',
  `display_mode` varchar(45) NOT NULL COMMENT '1650x1050 (32bit) (60Hz)',
  `downloaded_size` bigint(20) unsigned NOT NULL default '0',
  `elapsed_time` bigint(20) unsigned NOT NULL default '0',
  `download_completed` enum('Y','N') NOT NULL default 'N',
  `driver_version` varchar(45) default NULL COMMENT 'Driver 6.14.0010.6587 English',
  `driver_date_size` varchar(45) default NULL COMMENT 'Driver Date Size',
  PRIMARY KEY  (`diag_id`),
  KEY `uid` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=448021 DEFAULT CHARSET=latin1;

/*Table structure for table `dynamic_object_playlists` */

CREATE TABLE `dynamic_object_playlists` (
  `zone_index` int(11) NOT NULL COMMENT 'zone of object',
  `zone_instance_id` int(11) NOT NULL COMMENT 'zone of object',
  `global_id` int(11) NOT NULL COMMENT 'glid of item',
  `obj_placement_id` int(11) NOT NULL COMMENT 'FK to dyn_obj, may have more in zone',
  `asset_group_id` int(11) NOT NULL COMMENT 'playlist id FK to kaneva.asset_groups',
  PRIMARY KEY  (`zone_index`,`zone_instance_id`,`global_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tracks one row per dyn object glid per zone';

/*Table structure for table `dynamic_objects` */

CREATE TABLE `dynamic_objects` (
  `obj_placement_id` int(11) NOT NULL COMMENT 'unique id sent to client',
  `player_id` int(11) NOT NULL default '0' COMMENT 'player setting it',
  `zone_index` int(11) NOT NULL default '0' COMMENT 'zone where set.  If housing, instance Id will have to be reset',
  `zone_instance_id` int(11) NOT NULL default '0' COMMENT 'instance id of zone, if instanced',
  `object_id` int(11) NOT NULL default '0' COMMENT 'id of dynamic object (e.g. couch)',
  `global_id` int(11) NOT NULL default '0' COMMENT 'global inventory id of item that created this',
  `inventory_sub_type` int(11) NOT NULL default '256' COMMENT 'mature = 1024, gift = 512, normal = 256',
  `position_x` float NOT NULL default '0' COMMENT 'position in zone',
  `position_y` float NOT NULL default '0',
  `position_z` float NOT NULL default '0',
  `rotation_x` float NOT NULL default '0' COMMENT 'rotatation',
  `rotation_y` float NOT NULL default '0',
  `rotation_z` float NOT NULL default '0',
  `texture_asset_id` int(10) unsigned NOT NULL default '0' COMMENT 'optional material, xfer to kaneva assets',
  `texture_url` varchar(255) NOT NULL COMMENT 'from kaneva asset or personal community ',
  `asset_group_id` int(11) NOT NULL default '0' COMMENT 'FK to kaneva.asset_groups, basically playlist id ',
  `swf_name` varchar(1000) NOT NULL default '' COMMENT 'Local or URL',
  `swf_parameter` varchar(1000) NOT NULL default '' COMMENT 'optional playlist for object.',
  `friend_id` int(10) unsigned NOT NULL default '0' COMMENT 'optional friend id, used to show friend''s picture and activate related actions',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `expired_date` datetime default NULL COMMENT 'for try on, when this item expires',
  PRIMARY KEY  (`obj_placement_id`),
  KEY `INDEX_do_zone_index` (`zone_index`),
  KEY `INDEX_do_zone_instance` (`zone_instance_id`),
  KEY `INDEX_do_global_id` (`global_id`),
  KEY `INDEX_do_player` (`player_id`),
  KEY `objp_zidx` (`obj_placement_id`,`zone_index`),
  KEY `obj_id_zi_ziid` (`object_id`,`zone_index`,`zone_instance_id`),
  KEY `zinid_zid` USING BTREE (`zone_instance_id`,`zone_index`),
  KEY `fid` (`friend_id`),
  KEY `asset_group_id` (`asset_group_id`),
  KEY `asset_group_id_2` (`asset_group_id`),
  KEY `zi_ziid_gid` (`zone_index`,`zone_instance_id`,`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='placement info for dynamic objects';

/*Table structure for table `error_log` */

CREATE TABLE `error_log` (
  `error_id` bigint(20) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL default '0',
  `error_time` datetime NOT NULL default '0000-00-00 00:00:00' COMMENT 'error_time is local user time',
  `error_code` int(11) NOT NULL default '0',
  `error_type_id` smallint(5) unsigned NOT NULL default '0',
  `description` varchar(100) NOT NULL default '',
  `report_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`error_id`),
  KEY `idx_error_type` (`error_type_id`),
  KEY `ind_errorCode` (`error_code`)
) ENGINE=InnoDB AUTO_INCREMENT=192421 DEFAULT CHARSET=latin1;

/*Table structure for table `error_types` */

CREATE TABLE `error_types` (
  `error_type_id` smallint(5) unsigned NOT NULL default '0',
  `error_type` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`error_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `event_participation` */

CREATE TABLE `event_participation` (
  `event_date` date NOT NULL,
  `date_created` datetime NOT NULL,
  `kaneva_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`event_date`,`kaneva_user_id`),
  KEY `ukey` (`date_created`,`kaneva_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `game_users` */

CREATE TABLE `game_users` (
  `user_id` int(11) NOT NULL auto_increment COMMENT 'unique id',
  `kaneva_user_id` int(11) NOT NULL default '0' COMMENT 'Their Id in Kaneva (Kenava.users.user_id)',
  `username` varchar(80) NOT NULL COMMENT 'from Kaneva.users',
  `is_gm` enum('T','F') NOT NULL default 'F' COMMENT 'game master (T/F)',
  `can_spawn` enum('T','F') NOT NULL default 'F' COMMENT 'can spawn AI (T/F)',
  `is_admin` enum('T','F') NOT NULL default 'F' COMMENT 'play is an admin (T/F)',
  `time_played` int(11) NOT NULL default '0' COMMENT 'number of minutes played',
  `last_logon` timestamp NULL default NULL COMMENT 'when they last logged onto the game',
  `server_id` int(11) NOT NULL default '0' COMMENT 'FK to Kaneva current engines',
  `last_update_time` datetime NOT NULL COMMENT 'used to sync to game server',
  `logged_on` enum('T','F') NOT NULL default 'F' COMMENT 'currently logged on (T/F)',
  `last_ip` varchar(30) NOT NULL default '' COMMENT 'IP address from last logon',
  `created_date` datetime NOT NULL default '2007-01-01 00:00:00' COMMENT 'when user first logged in',
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `username` (`username`),
  KEY `INDEX_users_logged_on` (`logged_on`),
  KEY `INDEX_users_kaneva_user_id` (`kaneva_user_id`),
  KEY `INDEX_users_serverId` (`server_id`)
) ENGINE=InnoDB AUTO_INCREMENT=787746 DEFAULT CHARSET=latin1 COMMENT='Kaneva users who have used the game';

/*Table structure for table `getset_player_values` */

CREATE TABLE `getset_player_values` (
  `id` int(11) NOT NULL auto_increment COMMENT 'unique id',
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `name` char(64) NOT NULL COMMENT 'name of new value',
  `type` int(11) NOT NULL COMMENT 'type int, double, string (8/14/15)',
  `int_value` int(11) default NULL,
  `double_value` double default NULL,
  `string_value` varchar(512) default NULL,
  PRIMARY KEY  (`id`),
  KEY `INDEX_gs_player` (`player_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45009838 DEFAULT CHARSET=latin1 COMMENT='dyanmically added GetSet values';

/*Table structure for table `gift_catalog_3d_items` */

CREATE TABLE `gift_catalog_3d_items` (
  `gift_id` int(11) unsigned NOT NULL COMMENT 'FK to global_id in items table and gift_id in msg_gifts',
  `catalog_id` int(11) unsigned NOT NULL COMMENT 'catalog id for segmenting gifts per NPC, etc.',
  `name` varchar(125) NOT NULL COMMENT 'short name of gift for lists',
  `description` varchar(300) NOT NULL,
  `price` int(11) unsigned NOT NULL default '1' COMMENT 'gift price in KPts',
  `avail_begin_date` datetime NOT NULL default '2000-01-01 00:00:00' COMMENT 'begining date when item appears in the catalog',
  `avail_end_date` datetime NOT NULL default '2020-01-01 00:00:00' COMMENT 'corresponding end date',
  `gifted_count` int(11) unsigned NOT NULL default '0' COMMENT 'number of times this was given',
  PRIMARY KEY  (`gift_id`),
  KEY `INDEX_gift_catalog` (`catalog_id`),
  KEY `INDEX_gift_name` (`name`),
  KEY `INDEX_gift_avail` (`avail_begin_date`,`avail_end_date`),
  KEY `INDEX_gift_count` (`gifted_count`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='items for 3D gifting';

/*Table structure for table `hangout_cache` */

CREATE TABLE `hangout_cache` (
  `display_name` varchar(64) NOT NULL COMMENT 'name shown in client',
  `community_id` int(11) NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `number_of_members` int(11) default '0',
  `is_public` char(1) NOT NULL default '',
  `is_adult` char(1) default NULL,
  `creator_id` int(11) NOT NULL default '0',
  `username` varchar(22) NOT NULL,
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `number_of_diggs` int(11) default '0',
  `zone_index` int(11) default NULL,
  `zone_instance_id` int(11) NOT NULL default '0',
  `wok_raves` bigint(11) NOT NULL default '0',
  `count` bigint(11) NOT NULL default '0',
  `keywords` text,
  `status_id` int(11) NOT NULL default '0',
  `category_id` int(11) NOT NULL default '0',
  `over_21_required` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`community_id`),
  KEY `dn` (`display_name`(10)),
  KEY `n` (`name`(10)),
  KEY `de` (`description`(10)),
  KEY `nm` (`number_of_members`),
  KEY `ip` (`is_public`),
  KEY `cid` (`creator_id`),
  KEY `un` (`username`(10)),
  KEY `cd` (`created_date`),
  KEY `nd` (`number_of_diggs`),
  KEY `zi` (`zone_index`),
  KEY `ziid` (`zone_instance_id`),
  KEY `wr` (`wok_raves`),
  KEY `c` (`count`),
  KEY `kw` (`keywords`(10)),
  KEY `sid` (`status_id`),
  KEY `caid` (`category_id`),
  KEY `ia` (`is_adult`),
  FULLTEXT KEY `ft` (`name`,`description`,`username`,`display_name`,`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

/*Table structure for table `housing_placements` */

CREATE TABLE `housing_placements` (
  `housing_placement_id` int(11) NOT NULL auto_increment,
  `type` int(11) NOT NULL COMMENT 'ref to housing type',
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `position_x` float NOT NULL COMMENT 'where is this house in the zone',
  `position_y` float NOT NULL,
  `position_z` float NOT NULL,
  `is_open` enum('T','F') NOT NULL default 'F' COMMENT 'T/F',
  `has_vault` enum('T','F') NOT NULL default 'F' COMMENT 'T/F',
  `vault_item_capacity` int(11) NOT NULL COMMENT 'max items ',
  `vault_current_cash` int(11) NOT NULL COMMENT 'current cash amount in vault',
  `store_item_capacity` int(11) NOT NULL COMMENT 'max items',
  `store_current_cash` int(11) NOT NULL COMMENT 'current store cash',
  PRIMARY KEY  (`housing_placement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `inventories` */

CREATE TABLE `inventories` (
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `inventory_type` enum('P','B','H') NOT NULL default 'P' COMMENT 'type of inventory this is first letter of: Player/Bank/Housing',
  `inventory_sub_type` int(11) NOT NULL default '256' COMMENT 'mature = 1024, gift = 512, normal = 256',
  `player_id` int(11) NOT NULL default '0' COMMENT 'FK to players if on player',
  `armed` enum('T','F') NOT NULL default 'F' COMMENT 'is item currently armed (T/F)',
  `quantity` int(11) NOT NULL default '0' COMMENT 'how many of this item',
  `charge_quantity` int(11) NOT NULL default '0' COMMENT 'if ammo, amount of ammo',
  `gift_giver` int(11) NOT NULL default '0' COMMENT 'FK to players if a gift',
  `corpse_id` int(11) NOT NULL default '0' COMMENT 'FK to corpse if inventory on corpse',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`player_id`,`inventory_type`,`global_id`,`inventory_sub_type`),
  KEY `INDEX_inv_player` (`player_id`),
  KEY `INDEX_inv_type` (`inventory_type`),
  KEY `player_id` (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `inventories_transaction_log` */

CREATE TABLE `inventories_transaction_log` (
  `trans_id` bigint(20) NOT NULL auto_increment COMMENT 'auto-inc PK',
  `created_date` datetime NOT NULL,
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `inventory_type` enum('P','B','H') NOT NULL default 'P' COMMENT 'type of inventory this is first letter of: Player/Bank/Housing',
  `inventory_sub_type` int(11) NOT NULL default '256' COMMENT 'mature = 1024, gift = 512, normal = 256',
  `player_id` int(11) NOT NULL default '0' COMMENT 'FK to players if on player',
  `armed` enum('T','F') NOT NULL default 'F' COMMENT 'is item currently armed (T/F)',
  `quantity_delta` int(11) NOT NULL default '0' COMMENT 'change in quantity for this trans',
  `quantity_new` int(11) NOT NULL default '0' COMMENT 'quanity after this action',
	`charge_quantity` int(11)   NOT NULL DEFAULT '0' COMMENT 'if ammo, amount of ammo',
	`trader_id` INT(11) DEFAULT 0 COMMENT 'for trades, this will be who traded the item',
  PRIMARY KEY  (`trans_id`),
  KEY `INDEX_inv_player` (`player_id`),
  KEY `INDEX_inv_type` (`inventory_type`)
) ENGINE=InnoDB AUTO_INCREMENT=23427567 DEFAULT CHARSET=latin1;

/*Table structure for table `inventory_pending_adds` */

CREATE TABLE `inventory_pending_adds` (
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `inventory_type` enum('P','B','H') NOT NULL default 'P' COMMENT 'type of inventory this is first letter of: Player/Bank/Housing',
  `inventory_sub_type` int(11) NOT NULL default '256' COMMENT 'mature = 1024, gift = 512, normal = 256',
  `kaneva_user_id` int(11) NOT NULL default '0' COMMENT 'FK to kaneva.users,game_users,players',
  `quantity` int(11) NOT NULL default '0' COMMENT 'how many of this item',
  `gift_giver` int(11) NOT NULL default '0' COMMENT 'temp delete later',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `picked_up_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `active` enum('T','F') NOT NULL default 'T' COMMENT 'have these been picked up yet?',
  KEY `INDEX_inv_user` (`kaneva_user_id`),
  KEY `kaneva_user__id` (`kaneva_user_id`,`active`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `item_set_items` */

CREATE TABLE `item_set_items` (
  `item_set_id` smallint(5) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`item_set_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `item_sets` */

CREATE TABLE `item_sets` (
  `item_set_id` smallint(5) unsigned NOT NULL auto_increment,
  `set_name` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`item_set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `item_use_types` */

CREATE TABLE `item_use_types` (
  `use_type_id` smallint(5) unsigned NOT NULL auto_increment,
  `use_type` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`use_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `item_use_value` */

CREATE TABLE `item_use_value` (
  `use_value_id` int(10) unsigned NOT NULL auto_increment,
  `use_value` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`use_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `items` */

CREATE TABLE `items` (
  `global_id` int(11) NOT NULL COMMENT 'global inventory id',
  `name` varchar(125) NOT NULL COMMENT 'inventory item''s name',
  `description` varchar(200) NOT NULL default '',
  `market_cost` int(11) NOT NULL COMMENT 'cost to buy, in game credits',
  `selling_price` int(11) NOT NULL COMMENT 'buy-back price, in game credits',
  `required_skill` int(11) NOT NULL default '0' COMMENT 'skill req''d or -1 if none',
  `required_skill_level` int(11) NOT NULL default '0' COMMENT 'if skill req''d the level',
  `use_type` int(11) NOT NULL default '0' COMMENT 'how this item can be used',
  `use_value` int(11) NOT NULL default '0' COMMENT 'misc-int type value depends on use type',
  `display_name` varchar(125) NOT NULL default '',
  `access_type_id` smallint(5) unsigned NOT NULL default '0',
  `product_availability_id` smallint(5) unsigned NOT NULL default '0',
  `product_exclusivity_id` smallint(5) unsigned NOT NULL default '0',
  `currency_type_id` smallint(5) unsigned NOT NULL default '0',
  `product_amount` int(10) unsigned NOT NULL default '0',
  `date_added` datetime NOT NULL default '0000-00-00 00:00:00',
  `item_creator_id` int(10) unsigned NOT NULL default '0',
  `item_image_path` varchar(500) NOT NULL default '',
  `item_active` smallint(1) unsigned NOT NULL default '0',
  `arm_anywhere` smallint(1) unsigned NOT NULL default '1',
  `disarmable` smallint(1) unsigned NOT NULL default '1',
  `stackable` smallint(1) unsigned NOT NULL default '1',
  `destroy_when_used` smallint(1) unsigned NOT NULL default '0',
  `modifiers_id` int(10) unsigned NOT NULL default '0',
  `inventory_type` smallint(5) unsigned NOT NULL default '0' COMMENT 'mask of type 0x100=normal, 0x200 = gift',
  `custom_texture` varchar(50) default '' COMMENT 'texture name',
  `base_global_id` int(11) NOT NULL default '0' COMMENT 'glid this item is based upon',
  `expired_duration` int(11) default '0' COMMENT 'for try-on default expires from players inventory',
  `permanent` SMALLINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'item fixed to character, can not put on or take off, e.g. spinning jewel',
  `is_default` SMALLINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'default item for a specific set of items, e.g. default male shirt given all male shirts',
  `visual_ref` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY  (`global_id`),
  KEY `INDEX_items_use_type` (`use_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='global inventory svd';

/*Table structure for table `level_rewards` */

CREATE TABLE `level_rewards` (
  `reward_id` int(11) NOT NULL auto_increment,
  `skill_id` int(11) NOT NULL default '0',
  `level_id` int(11) NOT NULL default '0',
  `reward_glid` int(11) NOT NULL default '0',
  `reward_qty` int(11) NOT NULL default '0',
  `reward_point_type` smallint(5) NOT NULL default '0',
  `reward_amt` int(11) NOT NULL default '0',
  `alt_reward_glid` int(11) NOT NULL default '0',
  `alt_reward_qty` int(11) NOT NULL default '0',
  `alt_reward_amt` int(11) NOT NULL default '0',
  PRIMARY KEY  (`reward_id`,`skill_id`,`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `login_log` */

CREATE TABLE `login_log` (
  `login_id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `logout_date` datetime default NULL,
  `logout_reason_code` enum('NORMAL','LOST','TERMINATED','FORCE','TIMEOUT','BANNED','SECURITY_VIO','COMM_ASSIGN','UNKNOWN_NET','SERVER_SHUTDOWN','SERVER_RESTART','PLAYER_ZONED')  COLLATE utf8_general_ci NULL,
  `server_id` int(11) NOT NULL default '0',
  `ip_address` varchar(50)  COLLATE utf8_general_ci NULL,
  PRIMARY KEY  (`login_id`),
  KEY `cd` (`created_date`),
  KEY `uid` (`user_id`),
  KEY `login_log_ld` (`logout_date`)
) ENGINE=InnoDB COMMENT='', DEFAULT CHARSET='latin1';

/*Table structure for table `noterieties` */

CREATE TABLE `noterieties` (
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `murder_count` int(11) NOT NULL,
  `minutes_since_last_murder` int(11) NOT NULL,
  PRIMARY KEY  (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='1:1 with player, murder info';

/*Table structure for table `occupancy_limits` */

CREATE TABLE `occupancy_limits` (
  `zone_index` int(11) unsigned NOT NULL default '0' COMMENT 'type+index',
  `zone_instance_id` int(11) unsigned NOT NULL default '0',
  `zone_index_plain` int(11) unsigned NOT NULL default '0' COMMENT 'zoneIndex(zone_index)',
  `occupancy_limit_soft` int(11) unsigned NOT NULL COMMENT 'lower limit, still allow goto player',
  `occupancy_limit_hard` int(11) unsigned NOT NULL COMMENT 'only owner, gms allowed if hit',
  PRIMARY KEY  (`zone_index`,`zone_instance_id`),
  KEY `IDX_OL_plain` (`zone_index_plain`),
  KEY `IDX_OL_limit` (`occupancy_limit_soft`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='soft and hard population occupancy limits';


CREATE TABLE `p2p_animations` (
  `p2p_animation_id` int(11) NOT NULL default '0' COMMENT 'This is the index for a p2p animation',
  `initiator_anim_index` int(11) NOT NULL default '0' COMMENT 'Animation index for the person who starts the peer to peer animation',
  `participant_anim_index` int(11) NOT NULL default '0' COMMENT 'Animation index of the person who agrees to the peer to peer animation',
  `name` varchar(100) default NULL COMMENT 'Name of the animation',
  `runtime` int(11) NOT NULL default '0' COMMENT 'Time in milliseconds the peer to peer animation runs',
  `looping` enum('T','F') default 'F' COMMENT 'Does this animation loop?',
  `access_pass` int(11) NOT NULL default '0' COMMENT 'Access pass this item requires ( 0 no pass required )',
  PRIMARY KEY  (`p2p_animation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Holds the animation indexes for peer to peer animations';

/*Table structure for table `pass_group_channel_zones` */

CREATE TABLE `pass_group_channel_zones` (
  `zone_instance_id` int(11) NOT NULL COMMENT 'FK to channel zones',
  `zone_type` int(11) NOT NULL COMMENT 'FK to channel zones',
  `pass_group_id` int(11) NOT NULL COMMENT 'FK to pass_groups',
  PRIMARY KEY  (`zone_instance_id`,`zone_type`,`pass_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='m2m between pass_groups and apt or BB channel_zones';

/*Table structure for table `pass_group_items` */

CREATE TABLE `pass_group_items` (
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `pass_group_id` int(11) NOT NULL COMMENT 'FK to pass_groups',
  PRIMARY KEY  (`global_id`,`pass_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='m2m table between items and pass_groups';

/*Table structure for table `pass_group_items_audit` */

CREATE TABLE `pass_group_items_audit` (
  `global_id` int(11) default NULL,
  `pass_group_id` int(11) default NULL,
  `modifier_kaneva_user_id` int(11) default NULL,
  `change_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `operation` enum('INSERT','UPDATE','DELETE') default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `pass_group_perm_channels` */

CREATE TABLE `pass_group_perm_channels` (
  `zone_index_plain` int(11) NOT NULL COMMENT 'FK to channel zones',
  `pass_group_id` int(11) NOT NULL COMMENT 'FK to pass_groups',
  PRIMARY KEY  (`zone_index_plain`,`pass_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='m2m between pass_groups and permanent zones in channel_zones';

/*Table structure for table `pass_group_users` */

CREATE TABLE `pass_group_users` (
  `kaneva_user_id` int(11) NOT NULL COMMENT 'FK to kaneva.users',
  `pass_group_id` int(11) NOT NULL COMMENT 'FK to pass_groups',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`kaneva_user_id`,`pass_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='m2m between pass_groups and players';

/*Table structure for table `pass_groups` */

CREATE TABLE `pass_groups` (
  `pass_group_id` int(11) NOT NULL auto_increment COMMENT 'PK',
  `name` varchar(50) NOT NULL COMMENT 'unique name of pass group',
  `description` varchar(200) NOT NULL,
  `cost` int(11) NOT NULL default '0' COMMENT 'price',
  PRIMARY KEY  (`pass_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='central table for pass system';

/*Table structure for table `permanent_zone_ids` */

CREATE TABLE `permanent_zone_ids` (
  `zone_index` int(11) unsigned NOT NULL default '0' COMMENT 'FK to supported world zone index',
  `zone_instance_id` int(11) unsigned NOT NULL COMMENT 'last used instance id',
  PRIMARY KEY  (`zone_index`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='last used instance ids, used to serially get next id';

/*Table structure for table `permanent_zone_roles` */

CREATE TABLE `permanent_zone_roles` (
  `zone_index` int(11) NOT NULL default '0' COMMENT 'plain perm zone index',
  `kaneva_user_id` int(11) NOT NULL default '0' COMMENT 'FK to game_users/player/kaneva.users',
  `role` int(11) NOT NULL default '0' COMMENT 'from kaneva. 1 = owner, 2 = moderator',
  PRIMARY KEY  (`zone_index`,`kaneva_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='roles of users for permanent zones';

/*Table structure for table `player_presences` */

CREATE TABLE `player_presences` (
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `direction_x` float NOT NULL default '0' COMMENT 'last direction',
  `direction_y` float NOT NULL default '0',
  `direction_z` float NOT NULL default '0',
  `position_x` float NOT NULL default '0' COMMENT 'last position',
  `position_y` float NOT NULL default '0',
  `position_z` float NOT NULL default '0',
  `energy` int(11) NOT NULL default '0' COMMENT 'current hit points',
  `max_energy` int(11) NOT NULL default '0' COMMENT 'max hit points',
  `housing_zone_index` int(11) NOT NULL default '0' COMMENT 'zoneIndex of their apt',
  `housing_zone_index_plain` int(11) NOT NULL COMMENT 'just the zone index FK to supported_worlds',
  `respawn_zone_index` int(11) NOT NULL default '0' COMMENT 'rebirth zone',
  `respawn_zone_instance_id` int(11) NOT NULL default '0' COMMENT 'if respawn zone is instanced, this is instanceId',
  `respawn_position_x` float NOT NULL default '0' COMMENT 'rebirth point',
  `respawn_position_y` float NOT NULL default '0',
  `respawn_position_z` float NOT NULL default '0',
  `scale_factor_x` float NOT NULL default '1' COMMENT 'avatar scaling',
  `scale_factor_y` float NOT NULL default '1',
  `scale_factor_z` float NOT NULL default '1',
  `count_down_to_pardon` int(11) NOT NULL default '0' COMMENT 'time until pardoned for murder',
  `arena_base_points` int(11) NOT NULL default '0',
  `arena_total_kills` int(11) NOT NULL default '0' COMMENT 'points scored in arena',
  PRIMARY KEY  (`player_id`),
  KEY `IDX_housing_zone_index` (`housing_zone_index_plain`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='position and volatile player information';

/*Table structure for table `player_quests` */

CREATE TABLE `player_quests` (
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `quest_id` int(11) NOT NULL COMMENT 'FK to quests',
  `current_progression` int(11) NOT NULL default '0' COMMENT 'where they are in quest sequence',
  `last_selection_index` int(11) NOT NULL default '0' COMMENT 'track users position in progression',
  `status` int(11) NOT NULL default '0' COMMENT 'status of quest 1 = success, 0 = in progress',
  `max_drop_count` int(11) NOT NULL default '0' COMMENT 'max number of items this quest can drop, on a per player basis',
  PRIMARY KEY  (`player_id`,`quest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `player_skills` */

CREATE TABLE `player_skills` (
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `skill_id` int(11) NOT NULL COMMENT 'FK to skills',
  `current_level` int(11) NOT NULL default '1' COMMENT 'skill level',
  `current_experience` varchar(30) NOT NULL default '0' COMMENT 'experience value ',
  PRIMARY KEY  (`player_id`,`skill_id`),
  KEY `sid` (`skill_id`),
  KEY `pid` (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='M2M between player and skills';

/*Table structure for table `player_stats` */

CREATE TABLE `player_stats` (
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `stat_id` int(11) NOT NULL COMMENT 'FK to stats',
  `current_value` float NOT NULL default '0' COMMENT 'stat value',
  PRIMARY KEY  (`player_id`,`stat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='M2M between player and stats';

/*Table structure for table `player_titles` */

CREATE TABLE `player_titles` (
  `player_id` int(11) NOT NULL default '0',
  `title_id` int(11) NOT NULL default '0',
  `sort_order` int(11) NOT NULL default '0',
  PRIMARY KEY  (`player_id`,`title_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `player_zones` */

CREATE TABLE `player_zones` (
  `player_id` int(11) NOT NULL COMMENT 'FK to players',
  `server_id` int(10) unsigned NOT NULL COMMENT 'FK to servers',
  `current_zone` varchar(80) NOT NULL default '' COMMENT 'name of current zone',
  `current_zone_index` int(11) NOT NULL default '0' COMMENT 'zoneId of current zone',
  `current_zone_type` smallint(6) NOT NULL default '0' COMMENT 'denormalized type of zone from zone_index',
  `current_zone_instance_id` int(11) NOT NULL default '0' COMMENT 'if zone is instanced, this is instanceId',
  PRIMARY KEY  (`player_id`),
  KEY `IDX_pz_zone` (`current_zone`),
  KEY `IDX_pz_zoneIndex` (`current_zone_index`,`current_zone_instance_id`),
  KEY `server_id` (`server_id`),
  KEY `czii` (`current_zone_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='zone information for a player';

/*Table structure for table `players` */

CREATE TABLE `players` (
  `player_id` int(11) NOT NULL auto_increment COMMENT 'unique identifier for a player',
  `user_id` int(11) NOT NULL COMMENT 'Kaneva.users and game_players.user_id',
  `name` varchar(80) NOT NULL COMMENT 'name of avatar',
  `title` varchar(50) NOT NULL default '' COMMENT 'title part of name',
  `spawn_config_index` int(11) NOT NULL default '1' COMMENT 'initial spawn pt for new player',
  `original_EDB` int(11) NOT NULL default '0' COMMENT 'players original entity form',
  `team_id` int(11) NOT NULL default '1' COMMENT 'player''s team, mainly for arenas',
  `race_id` int(11) NOT NULL default '1' COMMENT 'player''s race for using items',
  `last_update_time` timestamp NULL default NULL COMMENT 'when this record was last updated',
  `in_game` enum('T','F') NOT NULL default 'F' COMMENT 'is this player in the game?',
  `server_id` int(11) NOT NULL default '0' COMMENT 'FK to Kaneva current engines',
  `kaneva_user_id` int(11) NOT NULL COMMENT 'denormalized from game_users',
  `created_date` datetime NOT NULL default '2007-01-01 00:00:00' COMMENT 'when the avatar was created',
  PRIMARY KEY  (`player_id`),
  UNIQUE KEY `name` (`name`),
  KEY `INDEX_players_in_game` (`in_game`),
  KEY `user_id` (`user_id`),
  KEY `IDX_player_server_id` (`server_id`),
  KEY `kuid` (`kaneva_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=810062 DEFAULT CHARSET=latin1 COMMENT='avatars';

/*Table structure for table `product_availability` */

CREATE TABLE `product_availability` (
  `product_availability_id` smallint(5) unsigned NOT NULL auto_increment,
  `product_availability` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`product_availability_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `product_exclusivity` */

CREATE TABLE `product_exclusivity` (
  `product_exclusivity_id` smallint(5) unsigned NOT NULL auto_increment,
  `product_exclusivity` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`product_exclusivity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `quest_progress_condition` */

CREATE TABLE `quest_progress_condition` (
  `prog_cond_id` int(11) NOT NULL,
  `datetime_begin` datetime default NULL,
  `datetime_end` datetime default NULL,
  `description` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `quests` */

CREATE TABLE `quests` (
  `quest_id` int(11) NOT NULL COMMENT 'spawnGenId',
  `title` varchar(100) NOT NULL COMMENT 'short name of quest, shown in menu',
  `description` varchar(1000) NOT NULL COMMENT 'longer name, shown in menu',
  `completion_message` varchar(1000) NOT NULL COMMENT 'opt msg shown when complete',
  `npc_name` varchar(50) NOT NULL COMMENT 'name of npc attached to quest',
  `bonus_skill_id` int(11) NOT NULL COMMENT 'FK to skills, add this skill when kill MOB',
  `experience_bonus` float NOT NULL default '0' COMMENT 'add this amount of skill when kill MOB',
  `max_exp_limit` float NOT NULL default '0' COMMENT 'max skill can be added when kill MOB',
  `is_autocomplete` enum('T','F') NOT NULL default 'F' COMMENT 'T/F if true and quest is complete when kill mob, quest it marked as complete',
  `optional_instruction` varchar(1000) NOT NULL,
  `mob_id_to_hunt` int(11) NOT NULL COMMENT 'xref aiCfgId,mob id to kill to complete quest',
  `drop_global_id` int(11) NOT NULL COMMENT 'FK to items, xref GLID of dropped item by MOB',
  `drop_percentage` int(11) NOT NULL COMMENT 'chance MOB will drop the item',
  `is_undeletable` enum('T','F') NOT NULL default 'F' COMMENT 'T/F, if have > 10 (hardcoded) quests, will remove one that is note marked as this',
  KEY `quest_id` (`quest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='quest svd';

/*Table structure for table `required_quest_items` */

CREATE TABLE `required_quest_items` (
  `quest_id` int(11) NOT NULL COMMENT 'FK to quests',
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `quantity` int(11) NOT NULL COMMENT 'number of them you must have',
  KEY `IDX_quest_required` (`quest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='list of required items for a quest';

/*Table structure for table `required_skill` */

CREATE TABLE `required_skill` (
  `required_skill_id` smallint(5) unsigned NOT NULL auto_increment,
  `required_skill` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`required_skill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `required_skill_level` */

CREATE TABLE `required_skill_level` (
  `required_skill_level_id` smallint(5) unsigned NOT NULL auto_increment,
  `required_skill_level` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`required_skill_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `schema_versions` */

CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL default CURRENT_TIMESTAMP COMMENT 'when version was set',
  `interface_change` ENUM('T','F') default 'F' COMMENT 'does this update require matching binaries',
  PRIMARY KEY  (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `skill_actions` */

CREATE TABLE `skill_actions` (
  `action_id` int(11) NOT NULL auto_increment,
  `skill_id` int(11) NOT NULL default '0',
  `gain_amt` float NOT NULL default '0',
  PRIMARY KEY  (`action_id`)
) ENGINE=InnoDB AUTO_INCREMENT=260 DEFAULT CHARSET=latin1;

/*Table structure for table `skill_levels` */

CREATE TABLE `skill_levels` (
  `level_id` int(11) NOT NULL auto_increment,
  `skill_id` int(11) NOT NULL default '0',
  `level_number` int(11) NOT NULL default '0',
  `start_exp` float NOT NULL default '0',
  `end_exp` float NOT NULL default '0',
  `title_id` int(11) NOT NULL default '0',
  `alt_title_id` int(11) NOT NULL default '0',
  `gain_msg` varchar(200) NOT NULL default '',
  `alt_gain_msg` varchar(200) NOT NULL default '',
  `level_msg` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=511 DEFAULT CHARSET=latin1;

/*Table structure for table `skills` */

CREATE TABLE `skills` (
  `skill_id` int(11) NOT NULL auto_increment COMMENT 'db id of skill',
  `name` varchar(30) NOT NULL COMMENT 'name of skill',
  `type` int(11) NOT NULL COMMENT 'skill type',
  `gain_increment` float NOT NULL default '0' COMMENT 'basis for gain (how fast a skill can be built)',
  `basic_ability_skill` char(1) NOT NULL default 'F' COMMENT 'means that it only has one function like hiding (T/F)',
  `basic_ability_exp_mastered` float NOT NULL default '0' COMMENT 'component of ability basic skill option',
  `basic_ability_function` int(11) NOT NULL default '0' COMMENT 'component of ability basic-function resulting from successful throw',
  `skill_gains_when_failed` float NOT NULL default '0' COMMENT '0.0 is no gain when failed if not 0.0 then it will gain till that exp point then not gain from trying anymore',
  `max_multiplier` float NOT NULL,
  `use_internal_timer` char(1) NOT NULL COMMENT 'T/F',
  `filter_eval_level` char(1) NOT NULL COMMENT 'T/F',
  PRIMARY KEY  (`skill_id`),
  KEY `INDEX_skill_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='player skills svd';

/*Table structure for table `snapshot_placed_furniture` */

CREATE TABLE `snapshot_placed_furniture` (
  `furniture_count` int(11) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_purchased_clothing` */

CREATE TABLE `snapshot_purchased_clothing` (
  `clothing_purchase_count` int(11) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_user_active` */

CREATE TABLE `snapshot_user_active` (
  `user_count` int(11) NOT NULL,
  `play_time_avg` bigint(20) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_user_avatar_gender` */

CREATE TABLE `snapshot_user_avatar_gender` (
  `user_count` int(11) NOT NULL default '0',
  `gender` enum('M','F') NOT NULL default 'M',
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`,`gender`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_user_balances` */

CREATE TABLE `snapshot_user_balances` (
  `balance` double NOT NULL default '0',
  `created_date` date NOT NULL default '0000-00-00',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL,
  PRIMARY KEY  (`created_date`),
  KEY `bal` (`balance`),
  KEY `kpi` (`kei_point_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_user_gender` */

CREATE TABLE `snapshot_user_gender` (
  `user_count` int(11) NOT NULL,
  `gender` enum('M','F') NOT NULL default 'M',
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`,`gender`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_user_inactive` */

CREATE TABLE `snapshot_user_inactive` (
  `user_count` int(11) NOT NULL,
  `play_time_avg` bigint(20) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `snapshot_wok_user_average` */

CREATE TABLE `snapshot_wok_user_average` (
  `play_time_avg` int(11) NOT NULL,
  `created_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`created_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `sp_logs` */

CREATE TABLE `sp_logs` (
  `counter` int(10) unsigned NOT NULL auto_increment,
  `when` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `caller` varchar(120) NOT NULL COMMENT 'stored proc that generated this msg',
  `type` enum('DBG','INFO','WARN','ERROR') NOT NULL default 'DBG' COMMENT 'serverity type',
  `msg` varchar(500) NOT NULL,
  PRIMARY KEY  (`counter`)
) ENGINE=InnoDB AUTO_INCREMENT=338656 DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED COMMENT='log message table for stored procs';

/*Table structure for table `spawn_points` */

CREATE TABLE `spawn_points` (
  `spawn_point_id` int(11) unsigned NOT NULL auto_increment COMMENT 'PK',
  `zone_index` int(11) NOT NULL default '0' COMMENT 'what zone this is for, type is optional',
  `position_x` float NOT NULL default '0' COMMENT 'where in zone',
  `position_y` float NOT NULL default '0',
  `position_z` float NOT NULL default '0',
  `rotation` int(11) default '0' COMMENT 'in degrees',
  `name` varchar(50) NOT NULL COMMENT 'short desc',
  PRIMARY KEY  (`spawn_point_id`),
  KEY `INDEX_sp_zone_index` (`zone_index`)
) ENGINE=InnoDB AUTO_INCREMENT=4161 DEFAULT CHARSET=latin1 COMMENT='spawn points for zones';

/*Table structure for table `starting_balances` */

CREATE TABLE `starting_balances` (
  `kei_point_id` varchar(50) NOT NULL default 'KPOINT' COMMENT 'FK to kaneva.user_balanaces',
  `balance` decimal(12,3) NOT NULL default '0.000',
  PRIMARY KEY  (`kei_point_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='starting balance for new avatar';

/*Table structure for table `starting_dynamic_objects` */

CREATE TABLE `starting_dynamic_objects` (
  `id` int(11) NOT NULL auto_increment COMMENT 'unique id',
  `zone_index` int(11) NOT NULL default '0' COMMENT 'zone where set.',
  `active` tinyint(1) NOT NULL default '0' COMMENT 'is this an active item to be added',
  `object_id` int(11) NOT NULL default '0' COMMENT 'id of dynamic object (e.g. couch)',
  `global_id` int(11) NOT NULL default '0' COMMENT 'global inventory id of item that created this',
  `position_x` float NOT NULL default '0' COMMENT 'position in zone',
  `position_y` float NOT NULL default '0',
  `position_z` float NOT NULL default '0',
  `rotation_x` float NOT NULL default '0' COMMENT 'rotatation',
  `rotation_y` float NOT NULL default '0',
  `rotation_z` float NOT NULL default '0',
  `texture_asset_id` int(10) unsigned NOT NULL default '0' COMMENT 'optional material, xfer to kaneva assets',
  `texture_url` varchar(255) NOT NULL COMMENT 'from kaneva asset or personal community ',
  `swf_name` varchar(1000) NOT NULL default '' COMMENT 'Local or URL',
  `swf_parameter` varchar(1000) NOT NULL COMMENT 'optional parameters for object.',
  PRIMARY KEY  (`id`),
  KEY `INDEX_sdo_active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1 COMMENT='starting object in the new players apt';

/*Table structure for table `starting_inventories` */

CREATE TABLE `starting_inventories` (
  `id` int(11) NOT NULL auto_increment COMMENT 'PK',
  `active` tinyint(1) NOT NULL default '0' COMMENT 'is this an active item to be added',
  `original_EDB` int(11) NOT NULL default '0' COMMENT 'players original entity form',
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  `inventory_type` enum('P','B','H') NOT NULL default 'P' COMMENT 'type of inventory this is first letter of: Player/Bank/Housing',
  `armed` enum('T','F') NOT NULL default 'F' COMMENT 'is item currently armed (T/F)',
  `quantity` int(11) NOT NULL default '0' COMMENT 'how many of this item',
  `charge_quantity` int(11) NOT NULL default '0' COMMENT 'if ammo, amount of ammo',
  PRIMARY KEY  (`id`),
  KEY `INDEX_si_active` (`active`),
  KEY `INDEX_si_edb` (`original_EDB`)
) ENGINE=InnoDB AUTO_INCREMENT=582 DEFAULT CHARSET=latin1 COMMENT='starting inventory for new players by EDB';

/*Table structure for table `starting_world_object_player_settings` */

CREATE TABLE `starting_world_object_player_settings` (
  `id` int(11) NOT NULL auto_increment,
  `zone_index` int(11) NOT NULL default '0' COMMENT 'zone where set.',
  `active` tinyint(1) NOT NULL default '0' COMMENT 'is this an active item to be added',
  `world_object_id` char(36) NOT NULL default '0',
  `asset_id` int(11) NOT NULL default '0' COMMENT 'Kaneva.assets.asset_id',
  `texture_url` varchar(255) NOT NULL COMMENT 'from kaneva assets',
  PRIMARY KEY  (`id`),
  KEY `INDEX_swo_active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='player''s apartment customizations';

/*Table structure for table `stats` */

CREATE TABLE `stats` (
  `stat_id` int(11) NOT NULL COMMENT 'editor''s id',
  `name` varchar(50) NOT NULL COMMENT 'stat name (WIS/DEX...)',
  `cap_value` float NOT NULL COMMENT 'max value',
  `gain_basis` float NOT NULL,
  `benefit_type` int(11) NOT NULL COMMENT '0 = no benefit  1=health benefit  2=speed benefit',
  `bonus_basis` float NOT NULL,
  PRIMARY KEY  (`stat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='player stats svd';

/*Table structure for table `store_inventories` */

CREATE TABLE `store_inventories` (
  `store_id` int(11) NOT NULL COMMENT 'FK to stores',
  `global_id` int(11) NOT NULL COMMENT 'FK to items',
  PRIMARY KEY  (`store_id`,`global_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='store->items table';

/*Table structure for table `stores` */

CREATE TABLE `stores` (
  `store_id` int(11) NOT NULL COMMENT 'PK',
  `name` varchar(64) NOT NULL COMMENT 'name of store (not NPC)',
  `channel_id` int(11) NOT NULL COMMENT 'channel where store exists',
  PRIMARY KEY  (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='in-game stores (commerce objects)';

/*Table structure for table `summary_active_permanent_population` */

CREATE TABLE `summary_active_permanent_population` (
  `zone_index` int(11) unsigned NOT NULL default '0' COMMENT 'perm zone index',
  `count` int(11) unsigned NOT NULL default '0' COMMENT 'current population',
  PRIMARY KEY  (`zone_index`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='track population of all instances of permanent zones';

/*Table structure for table `summary_active_population` */

CREATE TABLE `summary_active_population` (
  `server_id` int(10) unsigned NOT NULL COMMENT 'FK to servers',
  `zone_index` int(10) unsigned NOT NULL COMMENT 'zone id w/o instance',
  `zone_type` smallint(6) NOT NULL default '0' COMMENT 'type (apt,channel,etc)',
  `zone_instance_id` int(10) unsigned NOT NULL COMMENT 'zone instance',
  `count` int(10) unsigned NOT NULL default '0' COMMENT 'current total in zone for server',
  PRIMARY KEY  (`server_id`,`zone_instance_id`,`zone_index`),
  KEY `IDX_ap_count` (`count`),
  KEY `IDX_ap_type` (`zone_type`),
  KEY `IDX_ap_zone` (`zone_index`,`zone_instance_id`),
  KEY `IDX_ap_zone2` (`zone_instance_id`,`zone_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='summary table of players in zones on servers';

/*Table structure for table `summary_logons_by_day` */

CREATE TABLE `summary_logons_by_day` (
  `logon_date` date NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY  (`logon_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `summary_logons_by_hour` */

CREATE TABLE `summary_logons_by_hour` (
  `hour` int(11) NOT NULL,
  `logon_date` date NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY  USING BTREE (`logon_date`,`hour`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `summary_money_supply_by_user_day` */

CREATE TABLE `summary_money_supply_by_user_day` (
  `user_id` int(11) NOT NULL,
  `record_date` date NOT NULL default '0000-00-00',
  `balance` decimal(12,3) NOT NULL default '0.000',
  `kei_point_id` enum('GPOINT','KPOINT','MPOINT','DOLLAR') NOT NULL default 'GPOINT',
  PRIMARY KEY  (`user_id`,`record_date`,`kei_point_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `summary_spent_credits_by_day_user` */

CREATE TABLE `summary_spent_credits_by_day_user` (
  `user_id` int(11) NOT NULL,
  `record_date` date NOT NULL,
  `spend_amount` decimal(12,3) NOT NULL default '0.000',
  `kei_point_id` enum('KPOINT','GPOINT') NOT NULL default 'GPOINT',
  PRIMARY KEY  (`user_id`,`record_date`,`kei_point_id`),
  KEY `rd` (`record_date`),
  KEY `uid` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `summary_users_by_hour` */

CREATE TABLE `summary_users_by_hour` (
  `cur_date` date NOT NULL default '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL default '0',
  `avg_logged_on_players` int(11) default NULL,
  `max_logged_on_players` int(11) default NULL,
  PRIMARY KEY  (`cur_date`,`cur_hour`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `summary_users_by_minute` */

CREATE TABLE `summary_users_by_minute` (
  `cur_date` date NOT NULL default '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL default '0',
  `cur_minute` tinyint(4) NOT NULL default '0',
  `logged_on_players` int(11) default NULL,
  PRIMARY KEY  (`cur_date`,`cur_hour`,`cur_minute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `summary_users_by_server_by_hour` */

CREATE TABLE `summary_users_by_server_by_hour` (
  `cur_date` date NOT NULL default '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL default '0',
  `avg_logged_on_players` int(11) default NULL,
  `max_logged_on_players` int(11) default NULL,
  `server_id` int(11) NOT NULL,
  PRIMARY KEY  (`cur_date`,`cur_hour`,`server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `summary_users_by_server_by_minute` */

CREATE TABLE `summary_users_by_server_by_minute` (
  `cur_date` date NOT NULL default '0000-00-00',
  `cur_hour` tinyint(4) NOT NULL default '0',
  `cur_minute` tinyint(4) NOT NULL default '0',
  `logged_on_players` int(11) default NULL,
  `server_id` int(11) NOT NULL,
  PRIMARY KEY  (`cur_date`,`cur_hour`,`cur_minute`,`server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `supported_worlds` */

CREATE TABLE `supported_worlds` (
  `zone_index` int(11) NOT NULL COMMENT 'index in editor''s array',
  `name` varchar(260) NOT NULL COMMENT 'zone name and extension',
  `channel_id` int(11) NOT NULL default '1' COMMENT 'channel this zone uses for messages',
  `display_name` varchar(64) NOT NULL COMMENT 'name shown in client',
  `default_max_occupancy` int(11) unsigned NOT NULL default '30' COMMENT 'default limit copied to channel_zones on create',
  PRIMARY KEY  (`zone_index`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='supported worlds for the CMP';

/*Table structure for table `time_played` */

CREATE TABLE `time_played` (
  `user_id` int(10) unsigned NOT NULL auto_increment,
  `date_created` datetime NOT NULL,
  `time_played` smallint(5) unsigned NOT NULL,
  PRIMARY KEY  (`user_id`,`date_created`),
  KEY `dtc` (`date_created`),
  KEY `tp` (`time_played`)
) ENGINE=InnoDB AUTO_INCREMENT=4208991 DEFAULT CHARSET=latin1;

/*Table structure for table `titles` */

CREATE TABLE `titles` (
  `title_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`title_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Table structure for table `world_object_player_settings` */

CREATE TABLE `world_object_player_settings` (
  `id` int(11) NOT NULL,
  `world_object_id` char(36) NOT NULL default '0',
  `asset_id` int(11) NOT NULL default '0' COMMENT 'Kaneva.assets.asset_id',
  `texture_url` varchar(255) NOT NULL COMMENT 'from kaneva assets',
  `zone_index` int(11) NOT NULL default '0' COMMENT 'Zone index of this obj, denormalized from world_objects',
  `zone_instance_id` int(11) NOT NULL default '0' COMMENT 'for instances of zone, this is the id. (player, channel, etc.)',
  `last_updated_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`),
  KEY `INDEX_wops_world_object_id` (`world_object_id`),
  KEY `INDEX_wops_zone_instance` (`zone_instance_id`),
  KEY `INDEX_wops_asset_id` (`asset_id`),
  KEY `zid_asset_id` (`asset_id`,`zone_index`,`zone_instance_id`),
  KEY `wobj_id_sidx` (`world_object_id`,`zone_index`,`zone_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='player''s apartment customizations';

/*Table structure for table `world_objects` */

CREATE TABLE `world_objects` (
  `world_object_id` char(36) NOT NULL COMMENT 'GUID of item',
  `zone_index` int(11) NOT NULL COMMENT 'index of the zone',
  `trace_id` int(11) NOT NULL COMMENT 'unique id of world item',
  `name` varchar(50) NOT NULL COMMENT 'friendly name of zone item',
  `description` varchar(255) NOT NULL COMMENT 'description of what this is',
  PRIMARY KEY  (`world_object_id`),
  KEY `INDEX_wo_zone_index` (`zone_index`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='customizable world objects';

/*Table structure for table `zone_raves` */

CREATE TABLE `zone_raves` (
  `zone_instance_id` int(11) NOT NULL COMMENT 'FK to channel_zones',
  `zone_type` int(11) NOT NULL COMMENT 'FK to channel_zones',
  `kaneva_user_id` int(11) NOT NULL default '0' COMMENT 'user that performed the rave',
  `created_date` datetime NOT NULL,
  PRIMARY KEY  (`zone_instance_id`,`zone_type`,`kaneva_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `zone_unique_visits` */

CREATE TABLE `zone_unique_visits` (
  `kaneva_user_id` int(11) unsigned NOT NULL default '0' COMMENT 'User that visited zone',
  `zone_instance_id` int(11) unsigned NOT NULL default '0' COMMENT 'FK to channel_zones. Community_id or player_id',
  `zone_type` int(11) unsigned NOT NULL default '0' COMMENT 'FK to channel_zones',
  `created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`kaneva_user_id`,`zone_instance_id`,`zone_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- functions some views are dependant upon
\. fn#deadServerInterval.sql
\. fn#zoneIndex.sql

/* view list */

\. view#player_skills_view.sql
\. view#player_stats_view.sql
\. view#vw_blocked_users.sql
\. view#vw_friend_group_friends.sql
\. view#vw_friends.sql
\. view#vw_game_servers.sql
\. view#vw_local_users.sql
\. view#vw_transaction_log.sql
\. view#vw_user_balances.sql
\. view#vw_users.sql
\. view#z_active_server_engines.sql
\. view#z_list_players.sql
-- \. view#z_occupancy_limits.sql
\. view#z_online_players.sql
\. view#z_running_servers.sql
\. view#z_zone_populations.sql
\. view#dance_level_actions_view.sql
\. view#players_dance_fame_view.sql
