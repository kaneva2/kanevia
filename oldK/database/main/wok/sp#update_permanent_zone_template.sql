-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_permanent_zone_template;

DELIMITER ;;
CREATE PROCEDURE `update_permanent_zone_template`( IN __zone_index_plain INT(11),	IN __visibility INT(11) )
BEGIN
-- called to update the visibility (and possibly other attributes in the future) of a permanent zone
	UPDATE channel_zones
	SET visibility = __visibility
	WHERE zone_type = 4 AND 
				zone_index_plain = __zone_index_plain;


END
;;
DELIMITER ;