-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_schema;

DELIMITER ;;
CREATE PROCEDURE `update_schema`()
BEGIN
	DECLARE curVer INT;
	DECLARE changes INT;
	DECLARE workingVer INT;
	SET curVer = 0;
	SET changes = 0;
	SET workingVer = 19; 
	CREATE TABLE IF NOT EXISTS `schema_versions` (
	  `version` int(11) NOT NULL COMMENT 'current schema version',
	  `description` varchar(250) NOT NULL COMMENT 'description of update',
	  `date_applied` timestamp NOT NULL default CURRENT_TIMESTAMP COMMENT 'when version was set',
	  PRIMARY KEY  (`version`)
	) ENGINE=InnoDB;
	
	SELECT IFNULL(MAX(version),0) INTO curVer FROM schema_versions;
	SELECT CONCAT( "Current version is ", CAST( curVer AS char ) );
	
	
	SET workingVer = workingVer + 1;
	IF curVer < workingVer THEN 
		SELECT CONCAT( "Updating to version ", CAST( workingVer AS char ) );
		
		ALTER TABLE `channel_zones` ADD `max_occupancy_soft` INT( 11 ) UNSIGNED NOT NULL DEFAULT '30' COMMENT 'soft limit of avatars in zone. Goto player allowed if hit.',
					ADD `max_occupancy_hard` INT( 11 ) UNSIGNED NOT NULL DEFAULT '30' COMMENT 'hard limit. Only owner allowed when it.';
		ALTER TABLE `supported_worlds` ADD `default_max_occupancy` INT( 11 ) UNSIGNED NOT NULL DEFAULT '30' COMMENT 'default limit copied to channel_zones on create';
					
		INSERT INTO `schema_versions` ( `version`, `description` ) VALUES (workingVer, 'add limits for zones to table' );
		SET changes = changes + 1;
	END IF;
	
	
	
		
	SELECT CONCAT( "Made ", cast( changes as char), " schema updates" );
	
END
;;
DELIMITER ;