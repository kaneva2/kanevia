-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS script_save_player_data;

DELIMITER ;;
CREATE PROCEDURE `script_save_player_data`( IN __player_id INT(11),
                        IN __game_id INT(11),
												IN __attribute VARCHAR(16),
												IN __value VARCHAR(128) )
BEGIN
-- called to insert or update player data for a given game
  INSERT INTO script_player_data(`player_id`, `game_id`, `attribute`, `value`) 
    VALUES (__player_id, __game_id, __attribute, __value)
    ON DUPLICATE KEY UPDATE `attribute` = __attribute, `value` = __value;

END
;;
DELIMITER ;