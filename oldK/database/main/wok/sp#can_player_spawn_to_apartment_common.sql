-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS can_player_spawn_to_apartment_common;

DELIMITER ;;
CREATE PROCEDURE `can_player_spawn_to_apartment_common`(   IN    sourcePlayerId    INT
                                                         , IN  sourceKanevaId    INT
                                                         , IN  destKanevaId      INT
                                                         , IN  myServerId        INT(11)
                                                         , IN  isGm              CHAR
                                                         , IN  country           CHAR(2)
                                                         , IN  birthDate         DATE
                                                         , IN  showMature        BINARY(1)
                                                         , OUT playerId          INT(11)
                                                         , OUT housingZoneIndex  INT(11)
                                                         , OUT reasonCode        INT
                                                         , OUT serverId          INT(11)
                                                         , OUT scriptServerId    INT(11)
                                                         , OUT playerName        VARCHAR(80))
BEGIN
   DECLARE switchedServer BOOL DEFAULT FALSE;
   DECLARE isAllowed BOOL DEFAULT FALSE;
   DECLARE isBlocked BOOL DEFAULT FALSE;
   DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
	SET serverId = 0;
	
   CALL logging.logMsg( 'TRACE'
                        , SCHEMA()
                        , 'can_player_spawn_to_apartment_common'
                       , concat( 'begin(',sourceKanevaId,')' ) );
   IF sourceKanevaId = destKanevaId THEN
	   SET isGm = 'T'; 
	END IF;
	


   SET isAllowed = (isGm='F') AND (NOT isAccessAllowed( sourceKanevaId, destKanevaId, destKanevaId, 3));
   SET isBlocked = (isGm='F') AND isUserBlocked( sourceKanevaId, destKanevaId, destKanevaId, 2, NULL );

	IF isAllowed THEN
      CALL logging.logMsg( 'INFO'
                           , SCHEMA()
                           , 'can_player_spawn_to_apartment_common'
                           , concat( 'not allowed(',sourceKanevaId,')' ) );
      SET reasonCode = 4;
	ELSEIF isBlocked THEN
      CALL logging.logMsg( 'INFO'
                           , SCHEMA()
                           , 'can_player_spawn_to_apartment_common'
                           , concat( 'blocked(',sourceKanevaId,')' ) );
		SET reasonCode = 4; 
	ELSE
		SELECT pp.`housing_zone_index`, p.`player_id`
		  INTO housingzoneindex, playerid
		  FROM `players` p
				 INNER JOIN `player_presences` pp ON p.player_id = pp.player_id
		 WHERE p.kaneva_user_id = destkanevaid
		 LIMIT 1;
		
      CALL logging.logMsg( 'TRACE'
                           , SCHEMA()
                           , 'can_player_spawn_to_apartment_common'
                          , concat( 'hzi=' , housingzoneindex , ';playerId=', playerId) );

		IF housingZoneIndex IS NOT NULL THEN
			
			SET reasonCode = 0;
			
         CALL logging.logMsg( 'TRACE'
                              , SCHEMA()
                              , 'can_player_spawn_to_apartment_common'
                              , concat('CALL checkMaxOccupancy(' , sourcePlayerId, ',', sourceKanevaId, ',', housingZoneIndex, ',' , playerId , ')' )
                              );
			CALL checkMaxOccupancy( sourcePlayerId
                                 , sourceKanevaId
                                 , housingZoneIndex
                                 , playerId
                                 , isGm
                                 , country
                                 , birthDate
                                 , FALSE
                                 , reasonCode );
			IF reasonCode = 0 THEN
            CALL logging.logMsg( 'TRACE'
                                 , SCHEMA()
                                 , 'can_player_spawn_to_apartment_common'
                                 , concat( 'CALL can_player_spawn_to_zone(',sourceKanevaId,')' ) );

			
				CALL can_player_spawn_to_zone( sourceKanevaId
                                           , 3
                                           , housingZoneIndex
                                           , playerId
                                           , myServerId
                                           , isGm
                                           , country
                                           , birthDate
                                           , showMature
                                           , reasonCode
                                           , serverId
                                           , scriptServerId
                                           , playerName );
         ELSE
            CALL logging.logMsg( 'TRACE'
                                 , SCHEMA()
                                 , 'can_player_spawn_to_apartment_common'
                                , 'Me so full' );

			END IF;
		ELSEIF sourceKanevaId = destKanevaId THEN 
        CALL logging.logMsg( 'TRACE'
                                 , SCHEMA()
                                 , 'can_player_spawn_to_apartment_common'
                                 , concat( 'CALL get_load_balanced_server_for_player(',sourceKanevaId,')' ) );
			SET reasonCode = 0;
			SET housingZoneIndex = defaultApartmentZone();
			CALL get_load_balanced_server_for_player( housingZoneIndex
                                                   , sourceKanevaId
                                                   , playerId
                                                   , serverId
                                                   , scriptServerId
                                                   , switchedServer );
	 	ELSE
         CALL logging.logMsg( 'INFO'
                              , SCHEMA()
                              , 'can_player_spawn_to_apartment_common'
                              , 'code 2' );
			SET reasonCode = 2; 
		END IF;
	END IF;
	IF reasonCode <> 0 THEN 
		SET housingZoneIndex = 0;
		SET playerId = 0;
	END IF;
END
;;
DELIMITER ;