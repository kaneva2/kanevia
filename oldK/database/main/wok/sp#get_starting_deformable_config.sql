-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_starting_deformable_config;

DELIMITER ;;
CREATE PROCEDURE `get_starting_deformable_config`( IN __player_template_id INT(11) )
BEGIN

	SELECT equip_id,
		deformable_config,
		custom_material,
		custom_color_r,
		custom_color_g,
		custom_color_b
	FROM starting_deformable_configs
	WHERE player_template_id = __player_template_id
	ORDER BY sequence;

END
;;
DELIMITER ;