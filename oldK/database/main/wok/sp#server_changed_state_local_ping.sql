-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS server_changed_state_local_ping;

DELIMITER ;;
CREATE PROCEDURE `server_changed_state_local_ping`( IN __server_id INT(11), IN __server_name VARCHAR(64), 
										    IN __server_port INT(11), IN __new_state INT, IN __popCount INT(11),
										    IN __local_ping INT )
BEGIN
-- part of schema abstration to remove all Kaneva references from C++ 
-- and move them to SPs, which can be site-specific
-- called to do cleanup when server stops or starts
-- can be called also when a server crashes, and then never comes back up
	-- __new_state can be this, we only expect starting or stopping
	-- enum EServerState
	-- {
	-- 	ssStopped,
	-- 	ssRunning,
	-- 	ssStopping,
	-- 	ssStarting,
	-- 	ssLocked,
	-- 	ssFailed,
	-- };
	DECLARE __zone_index INT(11);
	DECLARE __zone_instance_id INT(11);
	DECLARE __done INT DEFAULT 0;
	DECLARE __user_id INT(11);
	DECLARE __deferred_sql VARCHAR(255);
	DECLARE __q_now VARCHAR(50);
	DECLARE __q_created_date VARCHAR(50);
	DECLARE __q_ip_address VARCHAR(50);
	DECLARE __q_logout_reason_code VARCHAR(50);
	
	
	DECLARE __perm_zones CURSOR FOR 
	    SELECT zone_index, zone_instance_id
	     FROM channel_zones
		WHERE `server_id` = __server_id
		  AND `zone_type` = 4 -- permanent
		  AND `zone_instance_id` > 1
		  AND `tied_to_server` <> 'PERMANENTLY_TIED';
			
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1;

	IF __local_ping = 1 THEN 
		CALL local_ping( __server_id, __server_name, __server_port, __new_state, __popCount );
	END IF;
	
	IF __new_state = 3 OR __new_state = 2 THEN -- stopping or starting, do cleanup
	
		-- this is a pasting of logoffAllUsers since it did more than that
		IF __server_id = 0 THEN
			-- get the server id from kaneva which should be there
			
			SELECT server_id INTO __server_id
			  FROM vw_game_servers
			 WHERE server_name = __server_name
			   AND port = __server_port;
					
		END IF;
		
		IF __server_id <> 0 THEN
		
			-- DELETE FROM player_zones
			UPDATE player_zones
			   SET server_id = 0
			 WHERE server_id = __server_id;

			UPDATE `game_users` 
			   SET `last_update_time` = NOW(), `logged_on` = 'F'
			 WHERE `server_id` = __server_id;
			 
			UPDATE `players` 
			   SET `in_game` = 'F'
			 WHERE `server_id` = __server_id;

			-- clean up any perm zones we had > 1
			-- similar to z_cleanup_perm_zones, only cursor restricts 
			-- to ones this server had
			OPEN __perm_zones;

			REPEAT
				FETCH __perm_zones into __zone_index, __zone_instance_id;

				IF NOT __done THEN 

					DELETE FROM channel_zones
					WHERE zone_index = __zone_index
						AND zone_instance_id = __zone_instance_id;
					
					DELETE FROM world_object_player_settings 
					WHERE zone_index = __zone_index
						AND zone_instance_id = __zone_instance_id;

					DELETE FROM dynamic_objects
					WHERE zone_index = __zone_index
						AND zone_instance_id = __zone_instance_id;

				END IF;

			UNTIL __done END REPEAT;

			-- set any zones we had to not tied
		   UPDATE channel_zones 
			  SET tied_to_server = 'NOT_TIED', 
			      `server_id` = 0
			WHERE `server_id` = __server_id
			  AND `tied_to_server` <> 'PERMANENTLY_TIED';

			if __new_state = 3 THEN
					SET __q_logout_reason_code = QUOTE('SERVER_RESTART');
			ELSE
					SET __q_logout_reason_code = QUOTE('SERVER_SHUTDOWN');	
			END IF;

			DELETE FROM summary_active_population
			 WHERE server_id = __server_id;
			
			
		END IF;
	END IF;
END
;;
DELIMITER ;