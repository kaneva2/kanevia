-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS haveRequiredPasses;

DELIMITER ;;
CREATE FUNCTION `haveRequiredPasses`( kanevaId INT, zi INT(11), instanceId INT) RETURNS tinyint(1)
    READS SQL DATA
BEGIN
	DECLARE mismatchedCount INT DEFAULT 0;

	IF zoneType( zi ) = 4 THEN -- permanent
		-- see if there are pass restrictions
		SET mismatchedCount = (SELECT count(cz.pass_group_id)
							     FROM pass_group_perm_channels cz
							    WHERE cz.zone_index_plain = zoneIndex(zi) AND 
							          cz.pass_group_id NOT IN 
							 	(SELECT u.pass_group_id FROM vw_pass_group_users u WHERE u.kaneva_user_id = kanevaId ));
	ELSE
		-- see if there are pass restrictions
		SET mismatchedCount = (SELECT count(cz.pass_group_id)
							     FROM pass_group_channel_zones cz
							    WHERE cz.zone_instance_id = instanceId AND 
							          cz.zone_type = zoneType(zi) AND 
							          cz.pass_group_id NOT IN 
							 	(SELECT u.pass_group_id FROM vw_pass_group_users u WHERE u.kaneva_user_id = kanevaId ));
	END IF;							 	
						 	
	IF mismatchedCount > 0 THEN
	    CALL logDebugMsg( 'haveRequiredPasses', concat( 'pass restriction failed for user count: ', cast( mismatchedCount as char )  ) );
		RETURN 0;
	END IF;

   return mismatchedCount = 0;

END
;;
DELIMITER ;