-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS getBestPermanentZone;

DELIMITER ;;
CREATE PROCEDURE `getBestPermanentZone`( IN zoneIndex INT, 
                                             IN softLimit INT, 
                                             OUT instanceId INT, 
                                             OUT curPopulation INT, 
                                             IN playerId INT,
										     IN __country CHAR(2),
										     IN birthDate DATE)
BEGIN
-- find the permanent zone for this zoneIndex()
-- that has the highest population < softLimit
	DECLARE usedId INT DEFAULT 0;
	DECLARE maxInstanceId INT DEFAULT 0;
	DECLARE newInstanceId INT DEFAULT 0;
	DECLARE done INT DEFAULT 0;
	DECLARE zoneName VARCHAR(64);
	DECLARE tiedToServer ENUM( 'NOT_TIED', 'TIED', 'PERMANENTLY_TIED', 'PENDING_TIED' );
	DECLARE foundOne INT DEFAULT 0;
	DECLARE workingServerId INT(11);
	DECLARE visible TINYINT UNSIGNED DEFAULT 100;
	DECLARE age INT DEFAULT TIMESTAMPDIFF(YEAR,birthDate,NOW());
	DECLARE zoneCountry CHAR(2) DEFAULT 'US';
	DECLARE zoneAge INT DEFAULT 0;

	-- cursor to get all zones that have < softlimit, or are inactive, on all servers
	DECLARE zonesCursor CURSOR FOR
	SELECT cz.name, cz.zone_instance_id, `count`, cz.server_id, cz.tied_to_server, cz.country, cz.max_age 
	  FROM channel_zones cz
	       LEFT OUTER JOIN
	       summary_active_population ap ON cz.zone_index = ap.zone_index AND cz.zone_instance_id = ap.zone_instance_id
	 WHERE cz.zone_index = zoneIndex
	 ORDER BY `count` DESC;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN SET done = 1; END;
    -- the following will catch case when checking used zone returns > 1 row, but
    -- it's better to let the error get thrown and bubble up
    -- DECLARE EXIT HANDLER FOR 1172 BEGIN 	CALL logMsg('getBestPermanentZone', 'WARN', concat( "> 1 row Zone 0x", hex(zoneIndex), ":", cast( instanceId as char ))) ; END;
    
	SET instanceId = NULL;
	
	-- softLimit of 0 means we want 1 person per instance
	IF softLimit = 0 THEN
	
		SELECT zone_instance_id into instanceId
		FROM channel_zones 
		WHERE zone_instance_id = playerId AND zone_index = zoneIndex; 
	
		SET curPopulation = 0;
		
		IF instanceId IS NOT NULL THEN
			SET foundOne = 1;
		END IF;
		
	ELSE			
		-- code after this select must be serialized
		-- across game servers, so use select for update to do it
		SELECT zone_instance_id INTO instanceId
			FROM permanent_zone_ids
		WHERE zone_index = zoneIndex(zoneIndex)
		FOR UPDATE;
	
		SET done = 0;
		OPEN zonesCursor;
	
		CALL logDebugMsg('getBestPermanentZone', concat( "Zone 0x", ifnull(hex(zoneIndex),'null'), ":", ifnull(cast( instanceId as char ),'<null>'), " limit ", ifnull(cast(softlimit as char),'null'), " country ", ifnull(__country,'null'), " age ", ifnull(cast( age as char ),'null'))) ;
	
		REPEAT
			FETCH zonesCursor into zoneName, instanceId, curPopulation, workingServerId, tiedToServer, zoneCountry, zoneAge;
	
			IF NOT done THEN 
				IF instanceId > maxInstanceId THEN
					SET maxInstanceId = instanceId;
				END IF;
	
				IF (curPopulation IS NULL OR curPopulation < softLimit) 
				    AND zoneCountry = __country 
				    AND zoneAge = calc_max_age_group(age)
				    AND NOT done THEN
	
					-- cursor sorts by count, so always get the highest population
					SET foundOne = 1;
					IF curPopulation IS NULL THEN
						-- no summary_active_population record
						SET curPopulation = 0;
					END IF;
	
				END IF;
				CALL logDebugMsg('getBestPermanentZone', concat('in loop, foundOne is ', cast(foundOne as char ), ' zone instance ', hex(zoneIndex), ' ', cast(instanceId as char), ' with pop of ', cast( curPopulation as char ), ' serverId ', cast(ifnull(workingServerId,0) as char) ) );
			END IF;
	
		UNTIL done OR foundOne END REPEAT;
	
		CLOSE zonesCursor;
		
	END IF;
	
	SET done = 0;


	IF foundOne = 0 THEN
		-- create a new instance on the fly since not one active

		SET curPopulation = 0;

		IF softLimit = 0 THEN
			SET instanceId = playerId;
		ELSE
			SET instanceId = maxInstanceId + 1;
		END IF;

		-- check to make sure not already used, ugly but necessary
		CALL logDebugMsg( 'getBestPermanentZone', concat( 'checking used zone ', hex(zoneIndex), ' instanceId = ', cast( instanceId as char ) ) );

		-- this should only return one row, but has returned > 1 which shouldn't happen
		-- see comment in the commented out handler above
		SELECT zone_instance_id INTO usedId
		  FROM channel_zones
		 WHERE zone_index = zoneIndex
		   AND zone_instance_id = instanceId;

		IF FOUND_ROWS() = 0 THEN
			-- ok
			REPLACE INTO permanent_zone_ids (zone_index, zone_instance_id)
			   VALUES( zoneIndex(zoneIndex), instanceId );

			SET zoneName = '';
			SELECT name, visibility INTO zoneName, visible
			  FROM channel_zones
			 WHERE zone_index = zoneIndex
			   AND zone_instance_id = 1;

			INSERT INTO channel_zones
			         (`kaneva_user_id`, `zone_index`, `zone_index_plain`,  `zone_type`,         `zone_instance_id`, `name`,  `server_id`,  `tied_to_server`, created_date, `visibility`, `country`, `max_age`, script_server_id )
			  VALUES ( 0,               zoneIndex,    zoneIndex(zoneIndex), zoneType(zoneIndex), instanceId,        zoneName, 0,           'NOT_TIED',       now(),         visible,    __country, calc_max_age_group(age), 0 );

		ELSE
			-- this is an error
			CALL logMsg( 'getBestPermanentZone', 'ERROR', concat( 'error next instanceId is used for zone index ', hex(zoneIndex), ' instanceId = ', cast( instanceId as char ) ) );
			SET instanceId = 0;
		END IF;

		CALL logDebugMsg('getBestPermanentZone', concat('created new instance zone ', hex(zoneIndex), ' ', cast(instanceId as char), " for ", __country, " with max age of ", cast( calc_max_age_group(age) as char) ) );
	ELSE
		CALL logDebugMsg('getBestPermanentZone', concat('found zone instance ', hex(zoneIndex), ' ', cast(instanceId as char), ' with pop of ', cast( curPopulation as char ) ) );
	END IF;
END
;;
DELIMITER ;