-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS updateDailyUserBalances;

DELIMITER ;;
CREATE PROCEDURE `updateDailyUserBalances`()
BEGIN
	Replace INTO summary_money_supply_by_user_day Select user_id,DATE(now()) as cur_date,balance,kei_point_id from kaneva.user_balances where kei_point_id='KPOINT';
	Replace INTO summary_money_supply_by_user_day Select user_id,DATE(now()) as cur_date,balance,kei_point_id from kaneva.user_balances where kei_point_id='GPOINT';	
END
;;
DELIMITER ;