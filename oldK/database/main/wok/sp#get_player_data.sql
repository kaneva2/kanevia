-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_player_data;

DELIMITER ;;
CREATE PROCEDURE `get_player_data`( IN __player_id INT, IN __kaneva_user_id INT )
BEGIN
	-- get all the datasets about a player, in the order C++ wants them

	DECLARE __color_type TINYINT DEFAULT 0; 
	DECLARE __direction_x FLOAT;
	DECLARE __direction_y FLOAT;
	DECLARE __direction_z FLOAT;
	DECLARE __position_x FLOAT;
	DECLARE __position_y FLOAT;
	DECLARE __position_z FLOAT;
	DECLARE __energy INT;
	DECLARE __max_energy INT;
	DECLARE __housing_zone_index INT;
	DECLARE __respawn_zone_index INT;
	DECLARE __respawn_zone_instance_id INT;
	DECLARE __respawn_position_x FLOAT;
	DECLARE __respawn_position_y FLOAT;
	DECLARE __respawn_position_z FLOAT;
	DECLARE __scale_factor_x FLOAT;
	DECLARE __scale_factor_y FLOAT;
	DECLARE __scale_factor_z FLOAT;
	DECLARE __count_down_to_pardon INT;
	DECLARE __arena_base_points INT;
	DECLARE __arena_total_kills INT;


  DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
      -- ERROR
      GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
      CALL logging.logMsg('ERROR', SCHEMA(), 'get_player_data', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
      RESIGNAL;
  END;


  CALL logging.logMsg('TRACE', SCHEMA(), 'get_player_data', CONCAT('BEGIN (__player_id[',CAST(__player_id AS CHAR), '] __kaneva_user_id[',CAST(__kaneva_user_id AS CHAR), '])'));


	
	CALL get_player_check_archive( __player_id, __kaneva_user_id );
	
	SELECT `direction_x`,`direction_y`, `direction_z`,
	 `position_x`,`position_y`,`position_z`, 
	 `energy`,  `max_energy`,
	 `housing_zone_index`,
	 `respawn_zone_index`,`respawn_zone_instance_id`,`respawn_position_x`,`respawn_position_y`,`respawn_position_z`,
	 `scale_factor_x`,`scale_factor_y`,`scale_factor_z`,
	 `count_down_to_pardon`,
	 `arena_base_points`, `arena_total_kills` , `color_type`
		INTO
		`__direction_x`,`__direction_y`, `__direction_z`,
	 `__position_x`,`__position_y`,`__position_z`, 
	 `__energy`,  `__max_energy`,
	 `__housing_zone_index`,
	 `__respawn_zone_index`,`__respawn_zone_instance_id`,`__respawn_position_x`,`__respawn_position_y`,`__respawn_position_z`,
	 `__scale_factor_x`,`__scale_factor_y`,`__scale_factor_z`,
	 `__count_down_to_pardon`,
	 `__arena_base_points`, `__arena_total_kills` , `__color_type`
	 FROM `player_presences` 
	 WHERE player_id = __player_id;

	SELECT 		`__direction_x`,`__direction_y`, `__direction_z`,
	 `__position_x`,`__position_y`,`__position_z`, 
	 `__energy`,  `__max_energy`,
	 `__housing_zone_index`,
	 `__respawn_zone_index`,`__respawn_zone_instance_id`,`__respawn_position_x`,`__respawn_position_y`,`__respawn_position_z`,
	 `__scale_factor_x`,`__scale_factor_y`,`__scale_factor_z`,
	 `__count_down_to_pardon`,
	 `__arena_base_points`, `__arena_total_kills`;

	SELECT `current_zone_index`, `current_zone_instance_id`, `current_zone` 
	 FROM `player_zones` 
	 WHERE player_id = __player_id;
	 		 
	CALL get_inventory( __player_id, __kaneva_user_id, '', 0 );

	SELECT `equip_id`, `deformable_config`, `custom_material`, `custom_color_r`, `custom_color_g`, `custom_color_b`
	 FROM `deformable_configs`
	 WHERE `player_id` = __player_id
	 ORDER BY `sequence`;

	CALL get_user_balance( __kaneva_user_id, 2 ); -- 2 = CREDITS_ON_PLAYER
	CALL get_user_balance( __kaneva_user_id, 1 ); -- 1 = GIFT_CREDITS_ON_PLAYER
	CALL get_user_balance( __kaneva_user_id, 6 ); -- 6 = CREDITS_IN_BANK

	SELECT `skill_id`, `name`, `type`, `gain_increment`, `basic_ability_skill`, `basic_ability_exp_mastered`, `basic_ability_function`, 
		`skill_gains_when_failed`, `max_multiplier`, `use_internal_timer`, `filter_eval_level`, `current_level`, `current_experience` 
	 FROM player_skills_view 
	 WHERE `player_id` = __player_id;

	SELECT `stat_id`, `name`, `cap_value`, `gain_basis`, `benefit_type`, `bonus_basis`, `current_value` 
	 FROM player_stats_view 
	 WHERE `player_id` = __player_id;

	SELECT `name`, `type`, `int_value`, `double_value`, `string_value` 
	 FROM `getset_player_values` 
	 WHERE player_id = __player_id;

	SELECT `murder_count`, `minutes_since_last_murder`
	  FROM `noterieties`
	 WHERE `player_id` = __player_id;

	SELECT clan_name, owner_id
	  FROM clan_members cm INNER JOIN clans c ON cm.clan_id = c.clan_id
	 WHERE player_id = __player_id;

	SELECT pass_group_id, end_date
	  FROM vw_pass_group_users
	 WHERE kaneva_user_id = __kaneva_user_id;
	 
	SELECT color_r, color_g, color_b
	  FROM colors
	 WHERE color_type = __color_type; 

	SELECT 
		0 AS is_players_inventory
        , base.*
		, ifnull(ip.value,0)       AS use_value
		, ifnull(ip2.value, '' )			AS exclusion_groups
		, inv.inventory_type
		, inv.inventory_sub_type
		, inv.armed
		, inv.quantity
		, inv.charge_quantity
	FROM 
		inventories inv
		JOIN items AS derivative ON inv.global_id				= derivative.global_id
		JOIN items AS base       ON derivative.base_global_id	= base.global_id
		LEFT OUTER JOIN item_parameters ip  ON base.global_id = ip.global_id  AND base.use_type = ip.param_type_id
		LEFT OUTER JOIN item_parameters ip2 ON base.global_id = ip2.global_id AND ip2.param_type_id = 31
	WHERE
		player_id=__player_id or player_id=0
	UNION		
	SELECT 
        1 
        , items.*
        , ifnull(ip.value,0)       AS use_value
        , ifnull(ip2.value, '' )   AS exclusion_groups
		, inv.inventory_type
		, inv.inventory_sub_type
		, inv.armed
		, inv.quantity
		, inv.charge_quantity
	FROM 
		inventories inv
        JOIN items ON inv.global_id				= items.global_id
    	LEFT OUTER JOIN item_parameters ip  ON inv.global_id = ip.global_id  AND items.use_type = ip.param_type_id
		LEFT OUTER JOIN item_parameters ip2 ON inv.global_id = ip2.global_id AND ip2.param_type_id = 31
	WHERE
		player_id=__player_id or player_id=0;
	
	-- Get the pass group ids for this users inventory items and their base items
	--	
		SELECT
		0 AS is_players_inventory 
		, pass.*
	FROM
		inventories inv
		join items                       AS derivative  ON inv.global_id              = derivative.global_id
		join items                       AS base        ON derivative.base_global_id  = base.global_id
		join pass_group_items AS pass        ON base.global_id             = pass.global_id
	WHERE
		player_id=__player_id or player_id=0
    UNION
	SELECT
		1 AS is_players_inventory
		, pass.*
	FROM
		inventories inv
		JOIN items                                      ON inv.global_id              = items.global_id
		JOIN pass_group_items AS pass        ON items.global_id            = pass.global_id
	WHERE
		player_id=__player_id or player_id=0;

  
  CALL logging.logMsg('TRACE', SCHEMA(), 'get_player_data', CONCAT('END (__player_id[',CAST(__player_id AS CHAR), '] __kaneva_user_id[',CAST(__kaneva_user_id AS CHAR), '])'));
  		
END
;;
DELIMITER ;