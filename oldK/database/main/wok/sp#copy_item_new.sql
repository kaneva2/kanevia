-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS copy_item_new;

DELIMITER ;;
CREATE PROCEDURE `copy_item_new`( IN __source_global_id INT(11) )
BEGIN
DECLARE custom_error CONDITION FOR SQLSTATE '45000';
   select @source_type:= s.use_type,
          @source_base_global_id:=s.base_global_id 
   FROM wok.items s
   where s.global_id = __source_global_id;
   
   set @target_base_global_id = @source_base_global_id;

if @source_type in (0,10,13,200,206,207) then

  select shard_info.get_items_custom_id() into @target_global_id;
  select __source_global_id as original, @target_global_id as newcopy;

  -- Don't change base_global_id for dynamic_objects.
  if @source_base_global_id >= 1000000 AND @source_type <> 10 then
                select shard_info.get_items_ugc_id() into @target_base_global_id;
  end if;

  -- Log the copy.
  insert into wok.kaneva_copied_items (old_global_id, new_global_id) VALUES (__source_global_id, @target_global_id);
  
  insert into wok.items ( global_id,name,description,market_cost,selling_price,required_skill,required_skill_level,use_type,item_creator_id,arm_anywhere,disarmable,stackable,destroy_when_used,inventory_type,custom_texture,base_global_id,expired_duration,permanent,is_default,visual_ref,web_controlled_edit,is_derivable,derivation_level,actor_group,search_refresh_needed )
     select @target_global_id, name,description, market_cost, selling_price, required_skill, required_skill_level, use_type,
                                0, arm_anywhere, disarmable, stackable, destroy_when_used, inventory_type, custom_texture, @target_base_global_id,
                    expired_duration, permanent, is_default, visual_ref, web_controlled_edit, is_derivable, derivation_level,
                                actor_group, now()
                from wok.items where global_id = __source_global_id;
  
  insert into shopping.items_web ( global_id,base_global_id,template_path,template_path_encrypted,texture_path,thumbnail_path,thumbnail_small_path,thumbnail_medium_path,thumbnail_large_path,thumbnail_assetdetails_path,designer_price,keywords,number_sold_on_web,number_of_raves,number_of_views,sales_total,sales_designer_total,cache_duration,converted,category_id1,category_id2,category_id3,display_name,date_added,item_active,search_refresh_needed,creator_username,recent_qty_sold,recent_raves) 
     select @target_global_id, @target_base_global_id, template_path, template_path_encrypted, texture_path, thumbnail_path, thumbnail_small_path,
        thumbnail_medium_path, thumbnail_large_path, thumbnail_assetdetails_path, 0, keywords, number_sold_on_web, number_of_raves,
        number_of_views, sales_total, sales_designer_total, cache_duration, converted, category_id1, category_id2, category_id3,
        display_name,now(), 1, now(), 'kaneva', 0, 0
     from shopping.items_web where global_id = __source_global_id;

  insert ignore into wok.item_paths 
                select if(ips.global_id=__source_global_id,@target_global_id,@target_base_global_id) as global_id, ips.type_id, ips.ordinal, ips.path, ips.file_size, ips.file_hash
                                                from wok.item_paths ips, wok.items i
                                where (i.base_global_id = ips.global_id or i.global_id = ips.global_id) and i.global_id  = __source_global_id;

  insert ignore into wok.item_parameters 
                select if(ips.global_id=__source_global_id,@target_global_id,@target_base_global_id) as global_id, ips.param_type_id, ips.value
                                                from wok.item_parameters ips, wok.items i
                                where (i.base_global_id = ips.global_id or i.global_id = ips.global_id) and i.global_id  = __source_global_id;

  -- Don't change base_global_id for dynamic_objects.								  
  if @source_base_global_id >= 1000000 AND @source_type <> 10 then

                insert into wok.items ( global_id,name,description,market_cost,selling_price,required_skill,required_skill_level,use_type,item_creator_id,arm_anywhere,disarmable,stackable,destroy_when_used,inventory_type,custom_texture,base_global_id,expired_duration,permanent,is_default,visual_ref,web_controlled_edit,is_derivable,derivation_level,actor_group,search_refresh_needed )
       select @target_base_global_id, name,description, market_cost, selling_price, required_skill, required_skill_level, use_type,
                                0, arm_anywhere, disarmable, stackable, destroy_when_used, inventory_type, custom_texture, base_global_id,
                    expired_duration, permanent, is_default, visual_ref, web_controlled_edit, is_derivable, derivation_level,
                                actor_group, now()
                   from wok.items where global_id = @source_base_global_id;
  
                insert into shopping.items_web ( global_id,base_global_id,template_path,template_path_encrypted,texture_path,thumbnail_path,thumbnail_small_path,thumbnail_medium_path,thumbnail_large_path,thumbnail_assetdetails_path,designer_price,keywords,number_sold_on_web,number_of_raves,number_of_views,sales_total,sales_designer_total,cache_duration,converted,category_id1,category_id2,category_id3,display_name,date_added,item_active,search_refresh_needed,creator_username,recent_qty_sold,recent_raves) 
       select @target_base_global_id, base_global_id, template_path, template_path_encrypted, texture_path, thumbnail_path, thumbnail_small_path,
         thumbnail_medium_path, thumbnail_large_path, thumbnail_assetdetails_path, 0, keywords, number_sold_on_web, number_of_raves,
         number_of_views, sales_total, sales_designer_total, cache_duration, converted, category_id1, category_id2, category_id3,
         display_name,now(), 1, now(), 'kaneva', 0, 0
       from shopping.items_web where global_id = @source_base_global_id;

  end if;

ELSE
   set @error_message:=concat("Unsupported Source use_type=", ifnull(@source_type,"null"));
   SIGNAL SQLSTATE '45000'
   SET MESSAGE_TEXT = @error_message;
END IF;

END
;;
DELIMITER ;