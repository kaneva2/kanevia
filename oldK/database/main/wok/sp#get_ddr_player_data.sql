-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_ddr_player_data;

DELIMITER ;;
CREATE PROCEDURE `get_ddr_player_data`( IN __game_id INT,
 										IN __player_id INT,
									    IN __dyn_object_id INT,
 										IN __zone_index INT, 
									    IN __object_id INT,
 										IN __want_high_score BOOLEAN )
BEGIN
	DECLARE __high_score INT DEFAULT NULL;
	DECLARE __skill_id INT DEFAULT NULL;
	DECLARE __name VARCHAR(30) DEFAULT NULL;
	DECLARE __type INT DEFAULT NULL;
	DECLARE __gain_increment FLOAT DEFAULT NULL;
	DECLARE __basic_ability_skill CHAR(1) DEFAULT NULL;
	DECLARE __basic_ability_exp_mastered FLOAT DEFAULT NULL;
	DECLARE __basic_ability_function INT DEFAULT NULL;
	DECLARE __skill_gains_when_failed FLOAT DEFAULT NULL;
	DECLARE __max_multiplier FLOAT DEFAULT NULL;
	DECLARE __use_internal_timer CHAR(1) DEFAULT NULL;
	DECLARE __filter_eval_level CHAR(1) DEFAULT NULL;
	DECLARE __current_level INT DEFAULT NULL;
	DECLARE __current_experience DOUBLE DEFAULT NULL;
	
	IF __zone_index = 1 AND __object_id = 1 THEN 
		
		SELECT zone_index, object_id INTO __zone_index, __object_id 
		  FROM dynamic_objects
		 WHERE obj_placement_id = __dyn_object_id;
		 
	END IF;
	
	IF __zone_index IS NOT NULL THEN 

		IF zoneType( __zone_index ) = 4 THEN  -- perm zone
			SELECT MAX(score) INTO __high_score
			 FROM ddr_scores  
			WHERE game_id = __game_id AND player_id = __player_id AND object_id = __object_id AND zone_index = __zone_index;
		ELSE
			SELECT MAX(score) INTO __high_score
			  FROM ddr_scores
			 WHERE game_id = __game_id  AND player_id = __player_id AND dynamic_object_id = __object_id;
		END IF;

		IF __high_score IS NULL THEN 
			SET __high_score = 0;
		END IF;
		
		-- first result set is rc, zi, oid, high score
		-- zi and oid may be same as param if params <> 1
		SELECT 0, __zone_index, __object_id, __high_score; 

		SELECT skill_id, name, type, gain_increment, basic_ability_skill, basic_ability_exp_mastered, basic_ability_function, 
		skill_gains_when_failed, max_multiplier, use_internal_timer, filter_eval_level, current_level, current_experience 
		 INTO __skill_id, __name, __type, __gain_increment, __basic_ability_skill, __basic_ability_exp_mastered, __basic_ability_function, 
			__skill_gains_when_failed, __max_multiplier, __use_internal_timer, __filter_eval_level, __current_level, __current_experience 
      FROM player_skills_view
      WHERE player_id = __player_id AND skill_id = 2; -- dance fame

		 IF __skill_id IS NULL THEN 
		    SELECT 1; 
		 ELSE
		    SELECT 0, __skill_id, __name, __type, __gain_increment, __basic_ability_skill, __basic_ability_exp_mastered, __basic_ability_function, __skill_gains_when_failed, __max_multiplier, __use_internal_timer, __filter_eval_level, __current_level, __current_experience;
		 END IF;

		IF __want_high_score THEN

			-- Check if the all time high score is zero and gather the data
			IF zoneType( __zone_index ) = 4 THEN  -- perm zone
				SELECT high_score, player_id, player_name
				  FROM ddr_high_scores  
				 WHERE object_id = __object_id AND zone_index = __zone_index;
			ELSE
				SELECT high_score, player_id, player_name
				  FROM ddr_high_scores 
				 WHERE dynamic_object_id = __object_id;
			END IF;
			
		END IF;
		
	ELSE
		SELECT 1; -- indicate zone_index not found
	END IF;
	
END
;;
DELIMITER ;