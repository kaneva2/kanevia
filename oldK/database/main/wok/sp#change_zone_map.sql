-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS change_zone_map;

DELIMITER ;;
CREATE PROCEDURE `change_zone_map`(
  IN __old_zone_index   INT,
  IN __zone_instance_id INT,
  IN __new_zone_index   INT,
  IN __player_name      VARCHAR(80),
  IN __template_id      INT,
  IN __game_name_no_spaces VARCHAR(100),
  OUT __ret INT,
  OUT __url VARCHAR(255)
)
    COMMENT '\n   Arguments\n   __old_zone_index:\n   __zone_instance_id:\n   __new_zone_index:\n   __player_name:\n   __template_id:\n   __game_name_no_spaces:\n  '
this_proc:
BEGIN


  DECLARE __kaneva_user_id int DEFAULT 0;
  DECLARE __count int DEFAULT 0;
  DECLARE __friendly_name varchar(128) DEFAULT ' - ';
  DECLARE __global_id int;
  DECLARE __object_id int;
  DECLARE __obj_placement_id int;
  DECLARE __x float;
  DECLARE __y float;
  DECLARE __z float;
  DECLARE __rx float;
  DECLARE __ry float;
  DECLARE __rz float;
  DECLARE __id int;
  DECLARE __playerId int;
  DECLARE __placement_playerId int;
  DECLARE __new_zi_plain int;
  DECLARE __zone_type int;
  DECLARE __game_item_id INT;
  DECLARE __game_item_glid INT;
  DECLARE __game_item_bundle_glid INT;
  DECLARE __inventory_compatible CHAR(1);
  DECLARE __game_item_type VARCHAR(50);
  DECLARE __activating_framework BOOLEAN DEFAULT FALSE;
  DECLARE __deactivating_framework BOOLEAN DEFAULT FALSE;
  DECLARE __done int DEFAULT 0;

  DECLARE dyo_cursor CURSOR FOR
  SELECT
    id,
    `object_id`,
    `global_id`,
    `position_x`,
    `position_y`,
    `position_z`,
    `rotation_x`,
    `rotation_y`,
    `rotation_z`
  FROM starting_dynamic_objects
  WHERE apartment_template_id = __template_id;

  DECLARE gi_cursor CURSOR FOR
  SELECT sgi.game_item_id, sgi.game_item_glid, ip.value AS game_item_bundle_glid, st.inventory_compatible, sn.item_type
  FROM starting_script_game_items sgi
  INNER JOIN snapshot_script_game_items sn ON sgi.game_item_glid = sn.game_item_glid
  INNER JOIN script_game_item_types st ON sn.item_type = st.item_type
  LEFT OUTER JOIN wok.item_parameters ip ON sgi.game_item_glid = ip.global_id 
	AND ip.param_type_id = 34
  WHERE sgi.template_glid = __template_id;

  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1;

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
      -- ERROR
      GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
      CALL logging.logMsg('ERROR', SCHEMA(), 'change_zone_map', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
      RESIGNAL;
  END;


  CALL logging.logMsg('TRACE', SCHEMA(), 'change_zone_map', CONCAT('BEGIN (__old_zone_index[',CAST(__old_zone_index AS CHAR), '], __zone_instance_id[',CAST(__zone_instance_id AS CHAR), '] __new_zone_index[',CAST(__new_zone_index AS CHAR), '] __player_name[', __player_name, '] __template_id[',CAST(__template_id AS CHAR), '] __game_name_no_spaces[', __game_name_no_spaces, '])'));

  SET __ret = 0;

  -- Validate Args	
  SELECT
    player_id INTO __playerId
  FROM players
  WHERE name = __player_name;
  IF __playerId = NULL THEN
    SET __ret = 9;
    LEAVE this_proc;
  END IF;


  IF zoneType(__old_zone_index) <> 3 AND zoneType(__old_zone_index) <> 6 THEN
    SET __ret = 7;
    CALL logging.logMsg('ERROR', SCHEMA(), 'change_zone_map', CONCAT('EXITING __ret[', __ret, '])'));
    LEAVE this_proc;
  END IF;


  SET __kaneva_user_id = get_zone_owner(__old_zone_index, __zone_instance_id);
  IF __kaneva_user_id = 0 THEN
    SET __ret = 5;
    CALL logging.logMsg('ERROR', SCHEMA(), 'change_zone_map', CONCAT('EXITING __ret[', __ret, '])'));
    LEAVE this_proc;
  END IF;


  SELECT
    COUNT(*) INTO __count
  FROM channel_zones
  WHERE `zone_index` = __old_zone_index AND `zone_instance_id` = __zone_instance_id;
  IF __count = 0 THEN
    SET __ret = 2;
    CALL logging.logMsg('ERROR', SCHEMA(), 'change_zone_map', CONCAT('EXITING __ret[', __ret, '])'));
    LEAVE this_proc;
  END IF;


  DELETE
    FROM `custom_deed_usage`
  WHERE `zone_index` = __old_zone_index AND `zone_instance_id` = __zone_instance_id;

  IF __template_id > 3000000 THEN

    SET __zone_type = zoneType(__old_zone_index);

    SELECT
      VALUE INTO __new_zi_plain
    FROM item_parameters
    WHERE global_id = __template_id;

    IF __new_zi_plain IS NOT NULL THEN
      SET __new_zone_index = ((__zone_type << 28) | __new_zi_plain);

      IF __zone_type = 3 THEN
        UPDATE player_presences
        SET housing_zone_index = __new_zone_index,
            housing_zone_index_plain = __new_zi_plain,
            respawn_zone_index = __new_zone_index
        WHERE player_id = __playerId;
      END IF;
    ELSE
      SET __ret = 2;
      CALL logging.logMsg('ERROR', SCHEMA(), 'change_zone_map', CONCAT('EXITING __ret[', __ret, '])'));
      LEAVE this_proc;
    END IF;

  END IF;



  IF zoneType(__old_zone_index) = 3 THEN
    SET __friendly_name = getFriendlyApartmentName(__player_name, __new_zone_index);
    UPDATE `channel_zones`
    SET `zone_index` = __new_zone_index,
        `zone_index_plain` = zoneIndex(__new_zone_index),
        `name` = __friendly_name
    WHERE `zone_index` = __old_zone_index AND `zone_instance_id` = __zone_instance_id;
  ELSEIF zoneType(__old_zone_index) = 6 THEN
    SET __friendly_name = getFriendlyHangoutName(__zone_instance_id, __new_zone_index);
    UPDATE `channel_zones`
    SET `zone_index` = __new_zone_index,
        `zone_index_plain` = zoneIndex(__new_zone_index),
        `name` = __friendly_name
    WHERE `zone_index` = __old_zone_index AND `zone_instance_id` = __zone_instance_id;
  END IF;

  UPDATE unified_world_ids
  SET zone_index = __new_zone_index,
	zone_index_plain = zoneIndex(__new_zone_index)
  WHERE zone_index       = __old_zone_index AND
	zone_instance_id = __zone_instance_id;

  DELETE
    FROM `world_object_player_settings`
  WHERE `zone_index` = __old_zone_index AND `zone_instance_id` = __zone_instance_id;


  DELETE
    FROM `occupancy_limits`
  WHERE `zone_index` = __old_zone_index AND `zone_instance_id` = __zone_instance_id;


  DELETE
    FROM `pass_group_channel_zones`
  WHERE `zone_type` = zoneType(__old_zone_index) AND `zone_instance_id` = __zone_instance_id;


  DELETE
    FROM `zone_raves`
  WHERE `zone_type` = zoneType(__old_zone_index) AND `zone_instance_id` = __zone_instance_id;

  DELETE
    FROM dynamic_object_playlists 
  WHERE zone_index = __old_zone_index AND zone_instance_id = __zone_instance_id;

  DELETE
    FROM sound_customizations 
  WHERE zone_index = __old_zone_index AND instance_id = __zone_instance_id;

  UPDATE `channel_zones`
  SET `raves` = 0
  WHERE `zone_type` = zoneType(__old_zone_index) AND `zone_instance_id` = __zone_instance_id;


  IF isCustomDeedUGC(__template_id) THEN
    INSERT INTO custom_deed_usage (`zone_index`, `zone_instance_id`, `apartment_template_id`)
      VALUES (__new_zone_index, __zone_instance_id, __template_id);
  END IF;

  SET __url = concat_vw_url(__game_name_no_spaces, SUBSTRING_INDEX(__friendly_name, ' - ', 1), zoneType(__old_zone_index));

  -- 1a 
  -- log framework deactivation if it was active in the zone
  SELECT IF(MIN(attribute) IS NULL, FALSE, TRUE) INTO __deactivating_framework
  FROM script_game_custom_data 
  WHERE zone_instance_id = __zone_instance_id 
	AND zone_type = zoneType(__old_zone_index)
	AND attribute = 'WorldSettings'
	AND value LIKE '%"frameworkEnabled":true%';

  IF __deactivating_framework THEN
	INSERT INTO metrics.sgitem_framework_activation_log (uuid, report_time, zone_instance_id, zone_type, username, activation_action)
    VALUES (UUID(), NOW(), __zone_instance_id, zoneType(__old_zone_index), __player_name, 'DeactivateImport');
  END IF;

  -- delete script game custom data
  DELETE FROM script_game_custom_data 
  WHERE zone_instance_id = __zone_instance_id AND zone_type = zoneType(__old_zone_index);

  -- 1b 
  -- log framework activation if zone to be imported has framework
  SELECT IF(MIN(attribute) IS NULL, FALSE, TRUE) INTO __activating_framework
  FROM starting_script_game_custom_data 
  WHERE template_glid = __template_id
	AND attribute = 'WorldSettings'
	AND value LIKE '%"frameworkEnabled":true%';

  IF __activating_framework THEN
	INSERT INTO metrics.sgitem_framework_activation_log (uuid, report_time, zone_instance_id, zone_type, username, activation_action)
    VALUES (UUID(), NOW(), __zone_instance_id, zoneType(__new_zone_index), __player_name, 'ActivateImport');
  END IF;

  -- insert script game custom data
  INSERT INTO `wok`.`script_game_custom_data` (`zone_instance_id`, `zone_type`, `attribute`, `value`)
  SELECT __zone_instance_id, zoneType(__new_zone_index), attribute, value
  FROM starting_script_game_custom_data
  WHERE template_glid = __template_id;

  -- 2a) delete all script game items
  DELETE
    sgi.*, 
    sgip.* 
  FROM 
    script_game_items sgi
    LEFT JOIN script_game_item_properties sgip
      ON sgip.zone_instance_id = sgi.zone_instance_id
	  AND sgip.zone_type = sgi.zone_type
	  AND sgip.game_item_id = sgi.game_item_id
  WHERE 
      sgi.zone_instance_id = __zone_instance_id
	  AND sgi.zone_type = zoneType(__old_zone_index);

  -- 2b
  -- copy over script game items
  SET __done = 0;
  OPEN gi_cursor;
  REPEAT
	FETCH gi_cursor INTO __game_item_id, __game_item_glid, __game_item_bundle_glid, __inventory_compatible, __game_item_type;
	IF NOT __done THEN

		-- Special case for quest items. 
		-- These items must always be inserted as a local copy due to their reliance on dynamic object placements.
		IF __game_item_type = 'quest' THEN
			
			INSERT INTO `wok`.`script_game_items` (`zone_instance_id`, `zone_type`, `game_item_id`, `game_item_glid`, `glid`, `name`, `item_type`, `level`, `rarity`)
			SELECT __zone_instance_id, zoneType(__new_zone_index), st.game_item_id, NULL, sn.glid, sn.name, sn.item_type, sn.level, sn.rarity
			FROM starting_script_game_items st
			INNER JOIN snapshot_script_game_items sn ON st.game_item_glid = sn.game_item_glid
			WHERE template_glid = __template_id AND game_item_id = __game_item_id;

			INSERT INTO wok.script_game_item_properties (zone_instance_id, zone_type, game_item_id, property_name, property_value)
			SELECT __zone_instance_id, zoneType(__new_zone_index), st.game_item_id, sn.property_name, sn.property_value
			FROM starting_script_game_items st
			INNER JOIN snapshot_script_game_item_properties sn ON st.game_item_glid = sn.game_item_glid
			WHERE template_glid = __template_id AND game_item_id = __game_item_id;

		ELSE

			INSERT INTO `wok`.`script_game_items` (`zone_instance_id`, `zone_type`, `game_item_id`, `game_item_glid`, `glid`, `name`, `item_type`, `level`, `rarity`)
			SELECT __zone_instance_id, zoneType(__new_zone_index), st.game_item_id, st.game_item_glid, sn.glid, sn.name, sn.item_type, sn.level, sn.rarity
			FROM starting_script_game_items st
			INNER JOIN snapshot_script_game_items sn ON st.game_item_glid = sn.game_item_glid
			WHERE template_glid = __template_id AND game_item_id = __game_item_id;

		END IF;

	END IF;
  UNTIL __done END REPEAT;
  CLOSE gi_cursor;

  -- 3a) delete all dynamic objects
  CALL logging.logMsg('TRACE', SCHEMA(), 'change_zone_map', CONCAT('begin (mt) delete dynamic_objects'));
  DELETE
    dynamic_objects.*, 
    dynamic_object_parameters.* 
  FROM 
    dynamic_objects
    LEFT JOIN dynamic_object_parameters
      ON dynamic_object_parameters.obj_placement_id = dynamic_objects.obj_placement_id
  WHERE 
      dynamic_objects.zone_index       = __old_zone_index AND 
      dynamic_objects.zone_instance_id = __zone_instance_id;

  -- 3b) insert new dynamic objects
  SET __done = 0;
  OPEN dyo_cursor;
  REPEAT
    FETCH dyo_cursor INTO __id, __object_id, __global_id, __x, __y, __z, __rx, __ry, __rz;
    IF NOT __done THEN
      SET __obj_placement_id = shard_info.get_dynamic_objects_id();

	  -- Temporary hack for home world template. Erven needs all items BUT the "safe" assigned to the "Kaneva" user for ownership reasons.
	  IF __template_id IN ( SELECT DISTINCT g.global_id
							FROM wok.world_templates wt
							INNER JOIN wok.world_template_global_id g ON wt.template_id = g.template_id
							WHERE wt.feature_list = 'Gameplay in the home'
						   ) AND __global_id <> 4588715 THEN
		SET __placement_playerId = 842177;
	  ELSE 
	    SET __placement_playerId = __playerId;
	  END IF;

      INSERT INTO dynamic_objects (`obj_placement_id`, `player_id`, `zone_index`, `zone_instance_id`, `object_id`, `global_id`, `inventory_sub_type`, `position_x`, `position_y`, `position_z`,
      `rotation_x`, `rotation_y`, `rotation_z`, `created_date`)
        VALUES (__obj_placement_id, __placement_playerId, __new_zone_index, __zone_instance_id, __object_id, __global_id, 2048, __x, __y, __z, __rx, __ry, __rz, NOW());

      INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value)
        SELECT
          __obj_placement_id,
          param_type_id,
          value
        FROM starting_dynamic_object_parameters
        WHERE id = __id;

	  INSERT INTO dynamic_object_playlists (zone_index, zone_instance_id, global_id, obj_placement_id, asset_group_id, video_range, audio_range, volume)
	  SELECT __new_zone_index, __zone_instance_id, global_id, __obj_placement_id, asset_group_id, video_range, audio_range, volume
	  FROM starting_dynamic_object_playlists
	  WHERE id = __id;

	  INSERT INTO sound_customizations (obj_placement_id, zone_index, instance_id, pitch, gain, max_distance, roll_off, `loop`, loop_delay, 
		dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle)
	  SELECT __obj_placement_id, __new_zone_index, __zone_instance_id, pitch, gain, max_distance, roll_off, `loop`, loop_delay, 
		dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle
	  FROM starting_sound_customizations
	  WHERE id = __id;

	  -- Update objectAssociations customdata
	  UPDATE wok.script_game_custom_data
	  SET value = REPLACE(
	  				REPLACE(
						REPLACE(
							REPLACE(
								REPLACE(value, ' ', ''), CONCAT('[', __id, ']'), CONCAT('[', __obj_placement_id, ']')
							), CONCAT('[', __id, ','), CONCAT('[', __obj_placement_id, ',')
						), CONCAT(',', __id, ','), CONCAT(',', __obj_placement_id, ',')
					), CONCAT(',', __id, ']'), CONCAT(',', __obj_placement_id, ']')
				 )
		WHERE zone_instance_id = __zone_instance_id 
			AND zone_type = zoneType(__new_zone_index)
			AND attribute LIKE 'objectAssociations%';

		-- Update waypointPID game item properties
		UPDATE wok.script_game_item_properties
		SET property_value = __obj_placement_id
		WHERE zone_instance_id = __zone_instance_id 
			AND zone_type = zoneType(__new_zone_index)
			AND property_name = 'waypointPID'
			AND property_value = __id;

    END IF;

  UNTIL __done END REPEAT;
  CLOSE dyo_cursor;

  -- 4a
  -- copy over starting objects into the inventory
  INSERT INTO wok.inventory_pending_adds2 (`kaneva_user_id`, `global_id`, `inventory_type`, `inventory_sub_type`, `quantity`, `last_touch_datetime`) 
    (SELECT 
        __kaneva_user_id, sdop.value, 'P', 512, 1, NOW()
      FROM 
        starting_dynamic_objects sdo
        JOIN starting_dynamic_object_parameters sdop
          ON sdo.id = sdop.id
      WHERE 
        sdo.apartment_template_id = __template_id AND sdop.param_type_id IN (32, 206)
      GROUP BY 
        sdop.value
     )
    ON DUPLICATE KEY UPDATE `quantity` = `quantity` + 1, `last_touch_datetime` = NOW();

  -- 4b
  -- copy over starting wops to zone
  INSERT INTO world_object_player_settings (`world_object_id`, `asset_id`, `texture_url`, `zone_index`, `zone_instance_id`, `last_updated_datetime`)
    SELECT
      `world_object_id`, `asset_id`, `texture_url`, __new_zone_index, __zone_instance_id, NOW()
    FROM 
      starting_world_object_player_settings
    WHERE 
       apartment_template_id = __template_id;

  -- 5 
  -- delete script game player data
  DELETE FROM script_game_player_data 
  WHERE zone_instance_id = __zone_instance_id AND zone_type = zoneType(__old_zone_index);

  CALL logging.logMsg('TRACE', SCHEMA(), 'change_zone_map', CONCAT('END (__ret[', __ret, '] __url[', __url, '])'));

END
;;
DELIMITER ;