-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS z_running_servers;

DELIMITER ;;

 CREATE VIEW `z_running_servers` AS SELECT `ge`.`server_id` AS `server_id`,`ge`.`ip_address` AS `ip_address`,`ge`.`port` AS `port`,`ge`.`number_of_players` AS `number_of_players`,`ge`.`visibility_id` AS `visibility_id`,concat( 
 IF( ( `ge`.`affinity_mask` & 1) ,_latin1'A',_latin1'') ,if( ( `ge`.`affinity_mask` & 2) ,_latin1'H',_latin1'') ,if( ( `ge`.`affinity_mask` & 4) ,_latin1'P',_latin1'') ,if( ( `ge`.`affinity_mask` & 8) ,_latin1'M',_latin1'') ,if( ( `ge`.`affinity_mask` & 16) ,_latin1'E',_latin1'') ,if( ( `ge`.`affinity_mask` & 32) ,_latin1'K',_latin1'') )  AS `affinity`,`ge`.`last_ping_datetime` AS `last_ping_datetime`,`ge`.`server_started_date` AS `server_started_date`,`ge`.`max_players` AS `max_players` 
 FROM `wok`.`vw_game_servers` `ge` 
 WHERE ( ( `ge`.`server_status_id` = 1)  
 AND ( timestampdiff( MINUTE,`ge`.`last_ping_datetime`,now( ) )  < `deadServerInterval`( ) ) )  
 ORDER BY `ge`.`ip_address`
;;
DELIMITER ;