-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_pass_group_items;

DELIMITER ;;
CREATE PROCEDURE `delete_pass_group_items`(passGroupId	INT(11), itemId	INT(11), modifierUserId	INT(11))
BEGIN
  START TRANSACTION;
     /*first insert into the audit table */
    INSERT INTO wok.pass_group_items_audit (global_id, pass_group_id, modifier_kaneva_user_id, change_date, operation) SELECT global_id, pass_group_id, modifierUserId, now(), 'DELETE' FROM wok.pass_group_items
    WHERE global_id = itemId AND pass_group_id = passGroupId;
    /*then removed from the pass_group_items table*/
    DELETE FROM wok.pass_group_items WHERE global_id = itemId AND pass_group_id = passGroupId;
  COMMIT;
END
;;
DELIMITER ;