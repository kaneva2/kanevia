-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS before_inventories_delete;

DELIMITER ;;
CREATE TRIGGER `before_inventories_delete` BEFORE DELETE ON `inventories` 
    FOR EACH ROW BEGIN
                INSERT INTO `inventories_transaction_log` 
                                                ( `created_date`, `global_id`,      `inventory_type`,   `player_id`,    `armed`,   `quantity_delta`,`quantity_new`, `charge_quantity`, `inventory_sub_type`, trader_id )
                VALUES  ( NOW(),          OLD.global_id, old.inventory_type, old.player_id, old.armed,  -old.quantity,   0,              old.charge_quantity, old.inventory_sub_type, @traderId );
END
;;
DELIMITER ;