-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_zone_customizations;

DELIMITER ;;
CREATE PROCEDURE `get_zone_customizations`( IN __zone_index INT, IN __zone_instance_id INT,
												IN __x FLOAT, IN __y FLOAT, IN __z FLOAT)
BEGIN
  
    SELECT concat(concat_ws('\t', 'd',
          do.obj_placement_id,
          `object_id`, 
          player_id, 
          `position_x`, 
          `position_y`, 
          `position_z`, 
          `rotation_x`, 
          `rotation_y`, 
          `rotation_z`,  
            `slide_x`, 
            `slide_y`, 
            `slide_z`,  
          do.global_id, 
          IFNULL(attachable,0), 
          IFNULL(interactive,0),  
          IFNULL(playFlash,0),  
          IFNULL(collision,0),  
          IFNULL(min_x,-1),  
          IFNULL(min_y,-1), 
          IFNULL(min_z,-1), 
          IFNULL(max_x,1),  
          IFNULL(max_y,1), 
          IFNULL(max_z,1),  
          IFNULL(is_derivable,0),  
          do.inventory_sub_type, 
          group_concat(CAST(dop.param_type_id AS CHAR), '\t', dop.value SEPARATOR '\t')), '\n') 
      FROM `vw_dynamic_objects_w_baseGLID` do
          LEFT OUTER JOIN `item_dynamic_objects` ido
              ON do.base_global_id = ido.global_id
          LEFT JOIN `dynamic_object_parameters` dop
             ON do.obj_placement_id = dop.obj_placement_id
             AND dop.param_type_id NOT IN (29, 30)
    WHERE `zone_index` = __zone_index
          AND`zone_instance_id` = __zone_instance_id
          AND(do.expired_date IS NULL OR do.expired_date > NOW()) 
      GROUP BY do.obj_placement_id
      ORDER BY (((position_x - __x) * (position_x - __x)) + ((position_y - __y) * (position_y - __y)) + ((position_z - __z) * (position_z - __Z))); 
         
    SELECT concat( 'w\t', w.world_object_id, '\t',  w.asset_id, '\t', w.texture_url, '\n'  ) as d
      FROM world_object_player_settings w 
     WHERE w.zone_instance_id = __zone_instance_id AND w.zone_index = __zone_index AND w.asset_id > 0;
  END
;;
DELIMITER ;