-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS defaultApartmentZone;

DELIMITER ;;
CREATE FUNCTION `defaultApartmentZone`() RETURNS int(11)
    DETERMINISTIC
BEGIN
-- sp to return the default apartment so it can be changed easily
		-- 3 = apt 5 = studio
        return 0x30000005; 
END
;;
DELIMITER ;