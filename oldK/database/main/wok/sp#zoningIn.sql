-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS zoningIn;

DELIMITER ;;
CREATE PROCEDURE `zoningIn`(    IN playerId INT
                              , IN currentZoneIndex INT
                              , IN currentZoneInstanceId INT
                              , IN serverId INT )
BEGIN
   -- before we lock everything up...update the balancer_inputs
   -- 
   call incrementBalancerInputs( currentZoneIndex, currentZoneInstanceId, serverId );


   INSERT INTO summary_active_population
             (server_id,zone_index,zone_type,zone_instance_id,`count`)
   VALUES (serverId,currentZoneIndex,zoneType(currentZoneIndex),currentZoneInstanceId,1)
       ON DUPLICATE KEY UPDATE count = count + 1, server_id = serverId;

END
;;
DELIMITER ;