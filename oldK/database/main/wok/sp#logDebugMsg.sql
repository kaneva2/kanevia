-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS logDebugMsg;

DELIMITER ;;
CREATE PROCEDURE `logDebugMsg`( IN procName VARCHAR(120), IN message VARCHAR(500 ) )
BEGIN
	-- comment out to turn off debug logging
--	INSERT INTO sp_logs (caller, type, msg) VALUES (COALESCE(procName,'null caller'), 'DBG', COALESCE(message,'null msg'));
END
;;
DELIMITER ;