-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS is_avatar_template_applied;

DELIMITER ;;
CREATE PROCEDURE `is_avatar_template_applied`( IN __player_id INT(11), OUT __ret INT(11) )
    READS SQL DATA
BEGIN
	DECLARE __template_id INT(11);
  
  SET __ret = 0;
  
	SELECT template_id INTO __template_id FROM player_template_applications WHERE player_id = __player_id;
  
  IF __template_id IS NOT NULL THEN
    SET __ret = 1;
  END IF; 
END
;;
DELIMITER ;