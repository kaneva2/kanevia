-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS updatePassList;

DELIMITER ;;
CREATE PROCEDURE `updatePassList`( IN isAdd INT, IN zoneIndex INT(11), IN instanceId INT(11), IN passId INT(11), OUT ret INT )
BEGIN
-- add or remove pass list from a space
-- server checks to make sure they can set it, we only need check on removal if any items
-- in the space
-- ret:  0 = ok,  1 = can't do to perm zone, 2 = items of pass exist in zone, must remove first
	DECLARE mismatchedCount INT DEFAULT 0;
	DECLARE id INT(11) DEFAULT NULL;
	SET ret = 0;
	IF zoneType( zoneIndex ) = 4 THEN
		SET ret = 1; -- never change perm zones here, manually edit table
	ELSE 
		IF isAdd THEN
			INSERT IGNORE INTO wok.pass_group_channel_zones 
			       (zone_instance_id, zone_type, pass_group_id) 
			VALUES (instanceId, zoneType(zoneIndex), passId);
		ELSE
			-- can not remove a pass from a zone if it contains any items 
			-- with the pass
			SELECT gi.global_id  INTO id
			  FROM pass_group_items gi INNER JOIN dynamic_objects do
			          ON gi.global_id = do.global_id
			 WHERE do.zone_index = zoneIndex AND do.zone_instance_id = instanceId AND gi.pass_group_id = passId
			 LIMIT 1;
			 IF id IS NULL THEN
				DELETE FROM wok.pass_group_channel_zones 
				WHERE zone_instance_id = instanceId 
				  AND zoneType(zoneIndex);
			ELSE
				SET ret = 2; -- can't remove pass from zone since have items left in it
			END IF;
		END IF;
	END IF;	
END
;;
DELIMITER ;