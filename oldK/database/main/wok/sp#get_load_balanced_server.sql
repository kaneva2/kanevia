-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_load_balanced_server;

DELIMITER ;;
CREATE PROCEDURE `get_load_balanced_server`(IN __zone_index INT, IN __instance_id INT(11), INOUT __cur_server_id INT(11), OUT __script_server_id INT(11), OUT __switched BOOL )
BEGIN
 	DECLARE __server_id INT(11) DEFAULT NULL;
 	DECLARE __dest_weight INT DEFAULT 0;
 	
	CALL logDebugMsg( 'get_load_balanced_server', concat( 'zone_index ', ifnull(hex( __zone_index ),'<null>'), 'instanceId ', ifnull(hex( __instance_id ),'<null>'), ' current ServerId is ', ifnull(cast( __cur_server_id as char ),'<null>') ) );

   set __server_id = selectCandidateServer(environment.getWOKGameId(),__zone_index, __instance_id);
			
	SELECT gs.server_id INTO __script_server_id
	  FROM developer.game_servers gs
		 WHERE gs.server_status_id = 1 
		   AND timestampdiff (minute, last_ping_datetime, NOW()) < deadServerInterval() 
		   AND gs.visibility_id = 1 
		   AND gs.number_of_players + 10 < gs.max_players 
			 AND gs.server_type_id = 7
			GROUP BY gs.server_id
			ORDER BY gs.number_of_players
			LIMIT 1;
			
	IF __script_server_id IS NULL THEN
		CALL logDebugMsg( 'get_load_balanced_server', concat( 'Script server id null since no servers match criteria' ) );
		SET __script_server_id = 0;
	ELSE
		CALL logDebugMsg( 'get_load_balanced_server', concat( 'Least-heavy server id is', cast(__script_server_id as char) ) );
	END IF;
			
	IF __server_id IS NULL THEN 
		-- can't find one!
		CALL logDebugMsg( 'get_load_balanced_server', concat( 'Server id null since no servers match criteria' ) );
		SET __server_id = 0;
	ELSE
		CALL logDebugMsg( 'get_load_balanced_server', concat( 'Least-heavy server id is ', cast( __server_id as char ), ' with weight of ', ifnull( cast(  __dest_weight as char ),'<null>') ) );
	END IF;

	SET __switched = FALSE;
		
	IF __cur_server_id = __server_id THEN
		SET __switched = FALSE; 
	ELSE		
		SET __switched = TRUE; -- so, we're switching you
		CALL logDebugMsg( 'get_load_balanced_server', concat( 'switching servers since __cur_server_id doesnt match ', ifnull(cast( __cur_server_id as char ),'<null>'), '<>', ifnull(cast( __server_id as char ),'<null>')  ) );
	END IF;
	
	SET __cur_server_id = __server_id;

END
;;
DELIMITER ;