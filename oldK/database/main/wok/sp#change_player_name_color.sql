-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS change_player_name_color;

DELIMITER ;;
CREATE PROCEDURE `change_player_name_color`(userID INT UNSIGNED, nameColor INT UNSIGNED)
BEGIN

  START TRANSACTION;

    
    UPDATE
    	player_presences
    SET
    	color_type = nameColor
    WHERE
      player_id = (select player_id from players where kaneva_user_id = userID);

    
    SELECT LAST_INSERT_ID();

  COMMIT;
END
;;
DELIMITER ;