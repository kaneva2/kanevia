-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS can_player_spawn_to_zone;

DELIMITER ;;
CREATE PROCEDURE `can_player_spawn_to_zone`( IN kanevaUserId INT,
												   IN zoneType INT, 
												   INOUT zoneIndex INT, 
												   IN instanceId INT,
												   IN myServerId INT(11),
												   IN isGm CHAR,
												   IN __country CHAR(2),
												   IN birthDate DATE,
												   IN showMatureBit BINARY(1),
												   OUT reasonCode INT,
												   OUT serverId INT(11),
													 OUT scriptServerId INT(11),
												   OUT zoneName VARCHAR(100))
BEGIN
	
DECLARE playersInZone INT;
	DECLARE tiedToServer ENUM( 'NOT_TIED', 'TIED', 'PERMANENTLY_TIED', 'PENDING_TIED' ) DEFAULT 'NOT_TIED';
	DECLARE channelZoneId INT(11) DEFAULT NULL;
	DECLARE switchedServer BOOL DEFAULT FALSE;
	DECLARE localZoneType INT(11) DEFAULT 0;
	DECLARE localZoneIndex INT(11) DEFAULT 0;
    DECLARE friendlyUserName VARCHAR(100) DEFAULT NULL;
    DECLARE friendlyZoneName VARCHAR(80) DEFAULT NULL;
    DECLARE friendlyName VARCHAR(200) DEFAULT NULL;
    DECLARE isAllowed BOOL DEFAULT FALSE; 
    DECLARE isBlocked BOOL DEFAULT FALSE;
    
	DECLARE over21 ENUM('Y','N') DEFAULT 'N';
	DECLARE age INT DEFAULT 0;
	DECLARE communityId INT(11) DEFAULT 0;
	
	DECLARE isAdult CHAR(1) DEFAULT 'N';
	DECLARE over21Required ENUM('Y','N') DEFAULT 'N';
	DECLARE rowCount INT DEFAULT 0;
	
	DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
	SET zoneName = '';
	
   CALL logging.logMsg( 'TRACE', SCHEMA(), 'can_player_spawn_to_zone', 'begin' );                                      CALL logDebugMsg( 'can_player_spawn_to_zone', CONCAT( 'starting for account: ', CAST( kanevaUserId AS CHAR ), ' for server ', CAST( myServerId AS CHAR ), ' ', HEX(zoneindex), ' ', CAST(instanceId AS CHAR), ' type=', CAST(zoneType AS CHAR) )  );
--	CALL logDebugMsg( 'can_player_spawn_to_zone', CONCAT( 'isGm: ', isGm, ' country: ', __country, ' birthDate: ', birthDate)  );
	SET age = TIMESTAMPDIFF(YEAR,birthDate,NOW());
	
	IF isGm='T' THEN
		
		SET showMatureBit = 1;
		SET over21 = 'Y';
	ELSEIF age > 20 THEN
		SET over21 = 'Y';
	END IF;
	CALL logDebugMsg( 'can_player_spawn_to_zone', CONCAT( 'permissions:  showMature = ', CAST( showMatureBit AS CHAR ), ' over21 ', CAST( over21 AS CHAR ) )  );
	SET reasonCode = 0; 
	IF isGm = 'F' AND NOT haveRequiredPasses( kanevaUserId, zoneIndex, instanceId ) THEN
		SET reasonCode = 7;
	END IF;
	IF reasonCode = 0 THEN 
		
		IF zoneType = 6 THEN 
            set isAllowed = (isGm='F') AND (NOT isAccessAllowed( kanevaUserId, (SELECT p.kaneva_user_id FROM players p WHERE player_id = instanceId ), instanceId, 6));
			set isBlocked = (isGm='F') AND isUserBlocked( kanevaUserId, (SELECT creator_id FROM kaneva.communities WHERE community_id = instanceId ), instanceId, 3, NULL) ;
			IF isAllowed THEN
				SET reasonCode = 3;
			ELSEIF isBlocked THEN
				SET reasonCode = 4; 
			ELSE
				
				
				
				SELECT c.is_adult, c.over_21_required, cz.channel_zone_id, cz.server_id, cz.script_server_id, tied_to_server, cz.zone_index, cz.zone_type, c.name
						INTO isAdult, over21Required, channelZoneId, serverId, scriptServerId, tiedToServer, localZoneIndex, localZoneType, zoneName
				  FROM kaneva.communities_public c 
				  	  LEFT OUTER JOIN channel_zones cz ON c.community_id = cz.zone_instance_id AND cz.zone_type = 6
				WHERE c.status_id = 1 
					AND c.community_id = instanceId
					AND NOT EXISTS (SELECT 1 FROM kaneva.communities_game cg WHERE c.community_id = cg.community_id)
					LIMIT 1;
				SET rowCount = FOUND_ROWS();
				CALL logDebugMsg( 'can_player_spawn_to_zone', CONCAT( 'type 6, serverId is ', IFNULL(serverId,'{null}'), ' foundRows is ', CAST(rowCount AS CHAR ) ) );
				
				IF rowCount = 0 THEN 
					CALL logDebugMsg( 'can_player_spawn_to_zone', 'zone not found' );
					SET reasonCode = 2;		
				
				
				
				
				
				ELSEIF serverId IS NULL THEN
					
					CALL logDebugMsg( 'can_player_spawn_to_zone', 'setting serverId to 0' );
					SET serverId = 0;
					IF localZoneType <> 6 THEN
						SET reasonCode = 2; 
					ELSE
				 		SET tiedToServer = 'NOT_TIED'; 
				 	END IF;
				ELSEIF localZoneIndex <> zoneIndex THEN
					CALL logMsg( 'can_player_spawn_to_zone', 'WARN', CONCAT( 'Channel zone index mismatch ', CAST( IFNULL(localZoneIndex,'null') AS CHAR), ' <> ', CAST( zoneIndex AS CHAR ) ) );
			 	END IF;
			END IF;
		ELSEIF zoneType = 3 THEN 
			set isAllowed =(isGm='F') AND (NOT isAccessAllowed( kanevaUserId, (SELECT p.kaneva_user_id FROM players p WHERE player_id = instanceId ), instanceId, 3));
            set isBlocked =  (isGm='F') AND isUserBlocked( kanevaUserId, (SELECT p.kaneva_user_id FROM players p WHERE player_id = instanceId ), instanceId, 2, NULL);
			IF isAllowed THEN
				SET reasonCode = 3;
			ELSEIF isBlocked THEN
				SET reasonCode = 4; 
			ELSE
				
				IF TRUE THEN 
					
					SELECT cz.channel_zone_id, cz.server_id, cz.script_server_id, cz.tied_to_server, u.username INTO channelZoneId, serverId, scriptServerId, tiedToServer, zoneName
					FROM players p 
							INNER JOIN vw_local_users u ON u.user_id = p.kaneva_user_id
							LEFT OUTER JOIN channel_zones cz ON p.player_id = cz.zone_instance_id AND cz.zone_type = 3
					WHERE p.player_id  = instanceId 
				 	LIMIT 1;
				 ELSE
					
					
					SELECT cz.channel_zone_id, cz.server_id, cz.script_server_id, cz.tied_to_server, u.username INTO channelZoneId, serverId, scriptServerId, tiedToServer, zoneName
					FROM players p 
							INNER JOIN vw_local_users u ON u.user_id = p.kaneva_user_id
							LEFT OUTER JOIN channel_zones cz ON p.player_id = cz.zone_instance_id AND cz.zone_type = 3
					WHERE p.player_id  = instanceId
				 	LIMIT 1;
				 END IF;
			 	IF FOUND_ROWS() = 0 THEN
					SET reasonCode = 2;		
				ELSEIF serverId IS NULL THEN
					SET serverId = 0;
			 		SET tiedToServer = 'NOT_TIED'; 
			 	END IF;
			 END IF;
		ELSEIF zoneType = 4 OR zoneType = 0 THEN 
			
			IF isGm = 'T' THEN 
				
				SELECT cz.channel_zone_id, cz.server_id, cz.script_server_id, tied_to_server, cz.name INTO channelZoneId, serverId, scriptServerId, tiedToServer, zoneName
				  FROM channel_zones cz
				 WHERE zone_instance_id = instanceId
				   AND zone_index = zoneIndex;
			ELSE
				SELECT cz.channel_zone_id, cz.server_id, cz.script_server_id, tied_to_server, cz.name INTO channelZoneId, serverId, scriptServerId, tiedToServer, zoneName
				  FROM channel_zones cz
				 WHERE cz.visibility >= 50 
				   AND zone_instance_id = instanceId
				   AND zone_index = zoneIndex;
			END IF;
			
			
			IF FOUND_ROWS() = 0 THEN
				SET reasonCode = 2; 
			END IF;
		 	CALL logDebugMsg( 'can_player_spawn_to_zone', CONCAT( 'type ', CAST( zoneType AS CHAR ), ' got ', CAST( reasonCode AS CHAR), ' ', CAST(IFNULL(serverId,0) AS CHAR ) ) );
		END IF;
	END IF ;
	
	CALL logDebugMsg( 'can_player_spawn_to_zone', CONCAT( 'first query ran, rc ', CAST( reasonCode AS CHAR ), ' tied to server ', CAST( tiedToServer AS CHAR ) ) );
	IF reasonCode = 0 THEN  
		IF tiedToServer <> 'NOT_TIED' THEN
			
			CALL logDebugMsg( 'can_player_spawn_to_zone', CONCAT( 'Tied server ', CAST(serverId AS CHAR), ' for zone ', HEX(zoneIndex), ':', CAST( instanceId AS CHAR ), ' tied somehow, checking server ...' ) );
			
			IF  NOT isServerRunning( serverId ) THEN 
				CALL get_load_balanced_server_for_player( zoneIndex
                                                      , kanevaUserId
                                                      , instanceId
                                                      , serverId
                                                      , scriptServerId
                                                      , switchedServer );
			END IF;
			CALL logDebugMsg( 'can_player_spawn_to_zone', CONCAT( '... output serverId is ', CAST(serverId AS CHAR) ) );
		ELSE
			
			CALL logDebugMsg( 'can_player_spawn_to_zone', CONCAT( 'about to call selectServer( ', CAST(channelZoneId AS CHAR), ', ', CAST(kanevaUserId AS CHAR ),  ', ', CAST(zoneType AS CHAR ), ', ', CAST(zoneIndex AS CHAR ), ', ', CAST(instanceId AS CHAR ), ',', CAST(myServerId AS CHAR ), ', ', CAST(serverId AS CHAR ), ');' ) );
			CALL selectServer( channelZoneId, kanevaUserId, zoneType, zoneIndex, instanceId, myServerId, serverId, scriptServerId );
			CALL logDebugMsg( 'can_player_spawn_to_zone', CONCAT( 'called select server and got ', CAST( COALESCE(serverId,'null') AS CHAR ) ) );
		END IF;
		CALL logDebugMsg( "can_player_spawn_to_zone", CONCAT( "channelZoneId is ", IFNULL(CAST(channelZoneId AS CHAR), '<null>'), ' scriptServerId is ', IFNULL( CAST( scriptServerId AS CHAR ), '<null>') ) );
	    IF channelZoneId IS NULL THEN
		    
		    SET friendlyName = '';
		    IF zoneType = 3 THEN
		      SELECT gu.username INTO friendlyUserName
		        FROM game_users gu
		        INNER JOIN players p ON p.user_id=gu.user_id
		  		  WHERE p.player_id=instanceId;
		      SELECT sw.display_name INTO friendlyZoneName
		         FROM supported_worlds sw
		         WHERE sw.zone_index=zoneIndex(zoneIndex);
		      IF friendlyUserName IS NOT NULL AND friendlyZoneName IS NOT NULL THEN
		         SET friendlyName = CONCAT(friendlyUserName, ' - ', friendlyZoneName);
		         SET zoneName = friendlyUserName;
		      END IF;
		    ELSEIF zoneType = 6 THEN 
		      SELECT c.name INTO friendlyUserName
		        FROM kaneva.communities c
		        WHERE c.community_id=instanceId;
		      SELECT sw.display_name INTO friendlyZoneName
		         FROM supported_worlds sw
		         WHERE sw.zone_index=zoneIndex(zoneIndex);
			  IF friendlyZoneName IS NOT NULL THEN
		         SET zoneName = friendlyUserName;
			      IF friendlyUserName IS NOT NULL THEN 
			         SET friendlyName = CONCAT(friendlyUserName, ' - ', friendlyZoneName);
			      END IF;
			  END IF;
		    END IF;
	        
	        IF serverId <> 0 AND myServerId <> 0 THEN
	        	SET tiedToServer = 'PENDING_TIED';
			ELSE
				SET tiedToServer = 'NOT_TIED';
			END IF;
			
	    	INSERT 	INTO channel_zones (`kaneva_user_id`, `zone_index`, `zone_index_plain`, `zone_type`,         `zone_instance_id`, `name`,           `server_id`,  `tied_to_server`, created_date, country, max_age, script_server_id )
	    				VALUES             ( kanevaUserId,     zoneIndex,   zoneIndex(zoneIndex), zoneType(zoneIndex), instanceId,        friendlyName,     serverId,    tiedToServer,  NOW(), __country, calc_max_age_group(age), scriptServerId);

			-- Grab the new zone's associated community_id if it's a home or hangout.
			IF zoneType = 6 THEN
				SET communityId = instanceId;
				INSERT INTO wok.unified_world_ids (community_id, world_type, zone_index, zone_index_plain, zone_type, zone_instance_id) VALUES (communityId, 'Community', zoneIndex, zoneIndex(zoneIndex), zoneType(zoneIndex), instanceId);
			ELSEIF zoneType = 3 THEN
				SELECT COALESCE(home_community_id, 0) INTO communityId FROM kaneva.users WHERE wok_player_id = instanceId;
				IF communityId <> 0 THEN
					INSERT INTO wok.unified_world_ids (community_id, world_type, zone_index, zone_index_plain, zone_type, zone_instance_id) VALUES (communityId, 'Home', zoneIndex, zoneIndex(zoneIndex), zoneType(zoneIndex), instanceId);
				END IF;
			END IF;
	             
	    ELSEIF serverId <> 0 AND myServerId <> 0 THEN
	    	IF tiedToServer <> 'PERMANENTLY_TIED' THEN
	    		SET tiedToServer = 'PENDING_TIED';
	    	END IF;
	    	
	        UPDATE channel_zones
	           SET `server_id` = serverId,
	          `tied_to_server` = tiedToServer,
		  	  `script_server_id` = scriptServerId
	         WHERE `channel_zone_id` = channelZoneId; 
	    END IF;
	END IF;
	IF serverId = 0 THEN 
		SET reasonCode = 6; 
	END IF;
	
	
	IF (zoneType = 4 OR zoneType = 6 OR zoneType = 3) AND reasonCode = 0 THEN
		IF zoneType = 6 THEN
			SET communityId = instanceId;
		ELSEIF zoneType = 3 THEN
			SELECT COALESCE(home_community_id, 0) INTO communityId FROM kaneva.users WHERE wok_player_id = instanceId;
		ELSE
			SELECT COALESCE(cg.community_id, 0) INTO communityId 
			FROM developer.faux_3d_app f3d
			INNER JOIN kaneva.communities_game cg ON f3d.game_id = cg.game_id
			WHERE f3d.zone_index = zoneIndex;
		END IF;

		IF communityId > 0 THEN
			UPDATE kaneva.community_members SET last_visited_at = NOW() WHERE community_id = communityId AND user_id = kanevaUserId;
		END IF;
	END IF;
	CALL logDebugMsg( 'can_player_spawn_to_zone', CONCAT( 'exiting with serverId of ', IFNULL(CAST(serverId AS CHAR ),'{null}'), ', scriptServerId of ', IFNULL(CAST(scriptServerId AS CHAR),'{null}'), ' rc of ', CAST( reasonCode AS CHAR ) ) );
END
;;
DELIMITER ;