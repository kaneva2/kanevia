-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS incrementBalancerInputs;

DELIMITER ;;
CREATE PROCEDURE `incrementBalancerInputs`( IN __zoneIndex          INT
                                          , IN __ZoneInstanceId   INT
                                          , IN __serverId         INT )
BEGIN
   
   INSERT INTO balancer_inputs
             ( zone_type
               , zone_index
               , zone_instance_id
               , server_id 
               , weight )
   VALUES    ( 
               zoneType(__zoneIndex)
               , __zoneIndex
               , __zoneInstanceId
               , __serverId  
               , 1 )
   ON DUPLICATE KEY 
  UPDATE weight = weight + 1, server_id = __serverId;

END
;;
DELIMITER ;