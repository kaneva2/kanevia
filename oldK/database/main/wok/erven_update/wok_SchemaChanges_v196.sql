-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

﻿DROP PROCEDURE IF EXISTS wok_SchemaChanges;

DELIMITER //

CREATE PROCEDURE wok_SchemaChanges ()
BEGIN

	-- Cursor values
	DECLARE __obj_placement_id, __object_id, __global_id, __inventory_sub_type INT;
	DECLARE __created_date, __expired_date DATETIME;
	DECLARE __position_x, __position_y, __position_z, __rotation_x, __rotation_y, __rotation_z, __slide_x, __slide_y, __slide_z FLOAT;

	-- Script variables
	DECLARE __done, __user_id, __player_id, __new_obj_placement_id, __wok_zone_index, __wok_zone_instance_id INT;
	DECLARE __wok_zone_type INT DEFAULT 6;
	DECLARE __3dapp_zone_index INT DEFAULT 1073742825;
	DECLARE __3dapp_zone_instance_id INT DEFAULT 1;
	DECLARE __3dapp_zone_index_plain INT DEFAULT 54;

	DECLARE dyo_cursor CURSOR FOR
	SELECT obj_placement_id, object_id, global_id, inventory_sub_type, position_x, position_y, position_z,
		rotation_x, rotation_y, rotation_z, created_date, expired_date, slide_x, slide_y, slide_z
	FROM erven.dynamic_objects do
	WHERE zone_instance_id = __3dapp_zone_instance_id AND zone_index = __3dapp_zone_index;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1;

	IF NOT EXISTS (SELECT 1 FROM wok.schema_versions WHERE version = 196) THEN

		-- Make sure spawn point exists
		-- INSERT INTO wok.spawn_points VALUES (116, __3dapp_zone_index_plain, -351.18, 4.17, -355.63, 217, 'CityZoneStart');

		-- Grab Smackdragon's player_id
		SELECT user_id, wok_player_id INTO __user_id, __player_id 
		FROM kaneva.users WHERE username = 'Smackdragon';

		-- Grab zone info for 'Erven City'
		SELECT wok.make_zone_index(__3dapp_zone_index_plain, __wok_zone_type), cz.zone_instance_id INTO __wok_zone_index, __wok_zone_instance_id
		FROM kaneva.communities c
		INNER JOIN wok.channel_zones cz ON c.community_id = cz.zone_instance_id AND cz.zone_type = __wok_zone_type
		WHERE c.name = 'Erven City v2';

		-- Update the wok channel_zone with the new zone index
		UPDATE wok.channel_zones
		SET zone_index_plain = __3dapp_zone_index_plain, zone_index = __wok_zone_index
		WHERE zone_instance_id = __wok_zone_instance_id and zone_type = __wok_zone_type;

		-- Copy over world object player settings
		INSERT INTO world_object_player_settings (`world_object_id`, `asset_id`, `texture_url`, `zone_index`, `zone_instance_id`, `last_updated_datetime`)
		SELECT `world_object_id`, `asset_id`, `texture_url`, __wok_zone_index, __wok_zone_instance_id, NOW()
		FROM erven.world_object_player_settings;

		-- Process dynamic objects and their parameters
		SET __done = 0;
		OPEN dyo_cursor;
		REPEAT
			FETCH dyo_cursor INTO __obj_placement_id, __object_id, __global_id, __inventory_sub_type, __position_x, __position_y, __position_z,
				__rotation_x, __rotation_y, __rotation_z, __created_date, __expired_date, __slide_x, __slide_y, __slide_z;
			IF NOT __done THEN
				START TRANSACTION;

					CALL `wok`.`insert_dynamic_object`(__player_id, __wok_zone_index, __wok_zone_instance_id, __object_id, 
						__global_id, __inventory_sub_type, __position_x, __position_y, __position_z, 
						__rotation_x, __rotation_y, __rotation_z, __slide_x, __slide_y, __slide_z, 0, 0, 
						0, 0, __new_obj_placement_id);

					INSERT INTO wok.dynamic_object_parameters (`obj_placement_id`, `param_type_id`, `value`)
					SELECT __new_obj_placement_id, `param_type_id`, `value`
					FROM erven.dynamic_object_parameters dop 
					WHERE obj_placement_id = __obj_placement_id
						AND dop.param_type_id NOT IN (29, 30);

					INSERT INTO wok.dynamic_object_playlists (zone_index, zone_instance_id, global_id, obj_placement_id, asset_group_id)
					SELECT __wok_zone_index, __wok_zone_instance_id, global_id, __new_obj_placement_id, asset_group_id
					FROM erven.dynamic_object_playlists dop 
					WHERE obj_placement_id = __obj_placement_id;

				COMMIT;
			END IF;
		UNTIL __done END REPEAT;
		CLOSE dyo_cursor;

	END IF;
END
//
DELIMITER ;

CALL wok_SchemaChanges();
DROP PROCEDURE IF EXISTS wok_SchemaChanges;

