-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_playlist;

DELIMITER ;;
CREATE PROCEDURE `get_playlist`( IN __zone_index INT(11), IN __zone_instance_id INT(11), IN __asset_group_id INT(11), IN __player_id INT(11))
BEGIN

	DECLARE __is_access_pass INT;
	
	SELECT is_mature_zone( __zone_index, __zone_instance_id) INTO __is_access_pass;

	IF __asset_group_id = -7 THEN
	-- for now all we get is the Cool items (-7) playlist
		IF __is_access_pass = 1 THEN
			SELECT a.asset_id, 0 as asset_offsite_id, asset_type_id, asset_sub_type_id, run_time_seconds, a.thumbnail_medium_path, mature, public, 'N' as over_21_required, number_of_diggs, a.media_path, a.name
			FROM kaneva.featured_assets fa, kaneva.vw_published_public_mature_assets a
			INNER JOIN kaneva.assets_stats ass ON a.asset_id = ass.asset_id
			INNER JOIN kaneva.communities_personal com ON a.owner_id = com.creator_id
			WHERE a.asset_id = fa.asset_id
			AND fa.status_id = 2 
			AND fa.start_datetime <=  NOW()
			AND run_time_seconds > 0
			AND asset_sub_type_id >= 0
			AND (fa.end_datetime >=  NOW()
			OR fa.end_datetime IS NULL);
		ELSE
		-- not in a mature zone, don't get any that are marked as mature
			SELECT a.asset_id, 0 as asset_offsite_id, asset_type_id, asset_sub_type_id, run_time_seconds, a.thumbnail_medium_path, mature, public, 'N' as over_21_required, number_of_diggs, a.media_path, a.name
			FROM kaneva.featured_assets fa, kaneva.vw_published_public_mature_assets a
			INNER JOIN kaneva.assets_stats ass ON a.asset_id = ass.asset_id
			INNER JOIN kaneva.communities_personal com ON a.owner_id = com.creator_id
			WHERE a.asset_id = fa.asset_id
			AND fa.status_id = 2 
			AND fa.start_datetime <=  NOW()
			AND (fa.end_datetime >=  NOW()
			OR fa.end_datetime IS NULL)
			AND run_time_seconds > 0
			AND asset_sub_type_id >= 0
			AND mature = 'N' ;
		END IF;
	ELSEIF __asset_group_id = -1 THEN
	-- get the ALL playlist given the player_id
		IF __is_access_pass = 1 THEN
			SELECT a.asset_id, a.asset_offsite_id, asset_type_id, asset_sub_type_id, run_time_seconds, a.thumbnail_medium_path, mature, public, a.over_21_required, number_of_diggs, a.media_path, a.name
			FROM kaneva.asset_channels ac
			INNER JOIN kaneva.assets a on ac.asset_id = a.asset_id and a.asset_type_id in (2,4)
			INNER JOIN kaneva.assets_stats d on a.asset_id = d.asset_id
			INNER JOIN kaneva.communities_personal cp on cp.community_id = ac.channel_id
			INNER JOIN wok.players p on cp.creator_id = p.kaneva_user_id
			WHERE p.player_id = __player_id AND ac.status_id = 1
			AND run_time_seconds > 0
			AND asset_sub_type_id >= 0;
		ELSE
		-- not in a mature zone, don't get any that are marked as mature
			SELECT a.asset_id, a.asset_offsite_id, asset_type_id, asset_sub_type_id, run_time_seconds, a.thumbnail_medium_path, mature, public, a.over_21_required, number_of_diggs, a.media_path, a.name
			FROM kaneva.asset_channels ac
			INNER JOIN kaneva.assets a on ac.asset_id = a.asset_id and a.asset_type_id in (2,4)
			INNER JOIN kaneva.assets_stats d on a.asset_id = d.asset_id
			INNER JOIN kaneva.communities_personal cp on cp.community_id = ac.channel_id
			INNER JOIN wok.players p on cp.creator_id = p.kaneva_user_id
			WHERE p.player_id = __player_id AND ac.status_id = 1
			AND run_time_seconds > 0
			AND asset_sub_type_id >= 0
			AND mature = 'N';
		END IF;
	ELSE
		IF __is_access_pass = 1 THEN
			SELECT ga.asset_id, a.asset_offsite_id, asset_type_id, asset_sub_type_id, run_time_seconds, thumbnail_medium_path, mature, public, over_21_required, number_of_diggs, a.media_path, a.name
			FROM kaneva.asset_group_assets ga
			INNER JOIN kaneva.assets a on ga.asset_id = a.asset_id
			INNER JOIN kaneva.assets_stats d on a.asset_id = d.asset_id 
			WHERE asset_group_id = __asset_group_id and run_time_seconds > 0
			AND asset_sub_type_id >= 0
			ORDER BY ga.sort_order, a.created_date desc;
		ELSE
		-- not in a mature zone, don't get any that are marked as mature
			SELECT ga.asset_id, a.asset_offsite_id, asset_type_id, asset_sub_type_id, run_time_seconds, thumbnail_medium_path, mature, public, over_21_required, number_of_diggs, a.media_path, a.name
			FROM kaneva.asset_group_assets ga
			INNER JOIN kaneva.assets a on ga.asset_id = a.asset_id
			INNER JOIN kaneva.assets_stats d on a.asset_id = d.asset_id 
			WHERE asset_group_id = __asset_group_id and run_time_seconds > 0
			AND asset_sub_type_id >= 0
			AND mature = 'N'
			ORDER BY ga.sort_order, a.created_date desc;
		END IF;
	END IF;

END
;;
DELIMITER ;