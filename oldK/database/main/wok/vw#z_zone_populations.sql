-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS z_zone_populations;

DELIMITER ;;

 CREATE VIEW `z_zone_populations` AS SELECT `cz`.`name` AS `name`,hex( `ap`.`zone_index`)  AS `zone_index`,`ap`.`zone_instance_id` AS `instance_id`,`cz`.`country` AS `country`,`cz`.`max_age` AS `max_age`,`ap`.`count` AS `count`,`ap`.`server_id` AS `server_id`,`ce`.`server_name` AS `server_name`,`ce`.`ip_address` AS `ip_address`,`ce`.`port` AS `port` 
 FROM ( ( `wok`.`summary_active_population` `ap` 
 JOIN `developer`.`game_servers` `ce` on( ( `ce`.`server_id` = `ap`.`server_id`) ) )  
 JOIN `wok`.`channel_zones` `cz` on( ( ( `ap`.`zone_index` = `cz`.`zone_index`)  
 AND ( `ap`.`zone_instance_id` = `cz`.`zone_instance_id`) ) ) )  
 GROUP BY `ap`.`zone_index`,`ap`.`zone_instance_id`,`ap`.`server_id` 
 ORDER BY `ap`.`count` desc,`cz`.`name`
;;
DELIMITER ;