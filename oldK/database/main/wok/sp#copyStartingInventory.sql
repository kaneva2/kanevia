-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS copyStartingInventory;

DELIMITER ;;
CREATE PROCEDURE `copyStartingInventory`( IN playerId INT )
BEGIN
-- helper SP, not called from code
-- 	copy the starting inventory from an existing player object
--  use this after you create the "model" player object
--  you need to do this for each type of player since it is by EDB
	DECLARE origEDB INT DEFAULT 0;
	SELECT p.original_edb INTO origEDB
	  FROM players p
	 WHERE player_id = playerId;
	IF origEDB <> 0 THEN
		
		UPDATE starting_inventories SET active = 0 WHERE original_edb = origEDB; -- turn off all others
		INSERT INTO starting_inventories ( `active`,`original_EDB`,`global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity` )
				SELECT 1, origEDB, `global_id`,`inventory_type`,`armed`,`quantity`,`charge_quantity` 
		  					FROM inventories WHERE player_id = playerId;
		SELECT concat( 'Inserted ', cast( ROW_COUNT() as char ), " for edb ", cast( origEDB as char ) ) as Result;
		
	ELSE
		SELECT concat( 'Player id not found: ', cast( playerId as char ) ) as Result;
	END IF;
	  					
END
;;
DELIMITER ;