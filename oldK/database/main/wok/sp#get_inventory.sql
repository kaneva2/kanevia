-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_inventory;

DELIMITER ;;
CREATE PROCEDURE `get_inventory`(
	IN __player_id            INT(11), 
	IN __kaneva_user_id       INT(11), 
	IN __inventory_type_req   CHAR, 
	IN __update_inventory_req INT
)
    COMMENT '\n   Arguments\n   __playerId:\n   __kaneva_user_id:\n   __inventory_type_req:\n   __update_inventory_req:\n  '
this_proc:
BEGIN

	DECLARE __inventory_type_req_valid INT DEFAULT 0;
	DECLARE __global_id int(11);
	DECLARE __inv_type char(1);
	DECLARE __inv_sub_type int(11);
	DECLARE __refreshCount int DEFAULT 0;

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
      -- ERROR
      GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
      CALL logging.logMsg('ERROR', SCHEMA(), 'get_inventory', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
      RESIGNAL;
  END;


  CALL logging.logMsg('TRACE', SCHEMA(), 'get_inventory', CONCAT('BEGIN (__player_id[',CAST(__player_id AS CHAR), '] __kaneva_user_id[',CAST(__kaneva_user_id AS CHAR), '] __inventory_type_req[', __inventory_type_req, '] __update_inventory_req[',CAST(__update_inventory_req AS CHAR),'])'));


	-- if a specific inventory type is requested, restrict by it.
  SET __inventory_type_req_valid = 0;
	IF (__inventory_type_req = 'P' OR __inventory_type_req = 'B' OR __inventory_type_req = 'H') THEN
		SET __inventory_type_req_valid = 1;
	END IF;

  CALL wok.refreshInventory(__player_id,__kaneva_user_id, __inventory_type_req, __refreshCount);

  CALL logging.logMsg('TRACE', SCHEMA(), 'get_inventory', CONCAT( 'refreshInventory returned __refreshCount[',CAST(__refreshCount AS  CHAR),'] records for _player_id[',CAST(__player_id AS CHAR), '] __kaneva_user_id[',CAST(__kaneva_user_id AS CHAR), ']'));

   IF __update_inventory_req = 1 THEN

		IF __refreshCount > 0 THEN

			IF  __inventory_type_req_valid = 1 THEN

				SELECT `player_id`, `inventory_type`, `global_id`, `inventory_sub_type`, `armed`, `quantity`, `charge_quantity`
				  FROM inventories
				 WHERE (player_id = __player_id or player_id = 0)
					AND inventory_type = __inventory_type_req;
			ELSE

				SELECT `player_id`, `inventory_type`, `global_id`, `inventory_sub_type`, `armed`, `quantity`, `charge_quantity`
				  FROM inventories
				 WHERE player_id = __player_id or player_id = 0;

			END IF;

		ELSE
			-- nothing added to inventory and a update was requested. Return on the add_count to signify nothing has changed
			-- so the caller's in memory inventory is still valid and doesn't need to change. Have to return somethin, mysql++ faults if not.
			SELECT 0 AS `player_id`, 'P' AS `inventory_type`, 0 AS `global_id`, 256 AS `inventory_sub_type`, 'F' AS `armed`, 0 AS `quantity`, 0 AS `charge_quantity`;

		END IF;

	ELSE

		IF  __inventory_type_req_valid = 1 THEN


			SELECT `player_id`, `inventory_type`, `global_id`, `inventory_sub_type`, `armed`, `quantity`, `charge_quantity`
			  FROM inventories
			 WHERE (player_id = __player_id or player_id = 0)
				AND inventory_type = __inventory_type_req;

		ELSE

			SELECT `player_id`, `inventory_type`, `global_id`, `inventory_sub_type`, `armed`, `quantity`, `charge_quantity`
			  FROM inventories
			 WHERE player_id = __player_id or player_id = 0;

		END IF;

	END IF; 

  CALL logging.logMsg('TRACE', SCHEMA(), 'get_inventory', CONCAT('END (__player_id[',CAST(__player_id AS CHAR), '] __kaneva_user_id[',CAST(__kaneva_user_id AS CHAR), '] __inventory_type_req[', __inventory_type_req, '] __update_inventory_req[',CAST(__update_inventory_req AS CHAR),'])'));


END
;;
DELIMITER ;