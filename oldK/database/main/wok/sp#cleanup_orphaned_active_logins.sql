-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS cleanup_orphaned_active_logins;

DELIMITER ;;
CREATE PROCEDURE `cleanup_orphaned_active_logins`()
BEGIN
DECLARE __message_text VARCHAR(500);
DECLARE	__table_RowCnt		INT;
DECLARE __user_id INT;
DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE arch_cursor CURSOR FOR 
		SELECT al.user_id
		FROM developer.active_logins `al`
		INNER JOIN wok.game_users gu ON al.user_id = gu.kaneva_user_id
		INNER JOIN wok.players p ON gu.user_id = p.user_id
		WHERE p.in_game = 'F'  AND `wok`.`is_wok_game`(al.game_id) = 1;
DECLARE arch_cursor2 CURSOR FOR 
		SELECT p.kaneva_user_id
	FROM wok.players p
	LEFT OUTER JOIN developer.active_logins al ON p.kaneva_user_id = al.user_id
	WHERE p.in_game = 'T' AND al.created_date IS NULL;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;
SET i = 0;
OPEN arch_cursor;
SET __table_RowCnt = (SELECT FOUND_ROWS());
SELECT CONCAT('Starting cleanup 1 ... ',CAST(__table_RowCnt AS CHAR),' rows to remove.') INTO __message_text;
CALL add_maintenance_log (DATABASE(),'cleanup_orphaned_active_logins',__message_text);
REPEAT
    FETCH arch_cursor INTO __user_id;
    IF NOT done THEN
	SET i=i+1;
	DELETE FROM developer.active_logins WHERE user_id = __user_id;
    END IF;	
UNTIL done END REPEAT;
CLOSE arch_cursor;
SELECT CONCAT('Cleanup completed 1 ..... ',CAST(i AS CHAR), ' rows removed from active_login.') INTO __message_text;
CALL add_maintenance_log (DATABASE(),'cleanup_orphaned_active_logins',__message_text);
SET done = 0;
SET i = 0;
OPEN arch_cursor2;
SET __table_RowCnt = (SELECT FOUND_ROWS());
SELECT CONCAT('Starting cleanup 2 ... ',CAST(__table_RowCnt AS CHAR),' rows to update.') INTO __message_text;
CALL add_maintenance_log (DATABASE(),'cleanup_orphaned_active_logins',__message_text);
REPEAT
    FETCH arch_cursor2 INTO __user_id;
    IF NOT done THEN
	SET i=i+1;
	UPDATE wok.players SET in_game = 'F' WHERE kaneva_user_id = __user_id;
    END IF;	
UNTIL done END REPEAT;
CLOSE arch_cursor2;
SELECT CONCAT('Cleanup completed 2 ..... ',CAST(i AS CHAR), ' rows updated in players.') INTO __message_text;
CALL add_maintenance_log (DATABASE(),'cleanup_orphaned_active_logins',__message_text);
END
;;
DELIMITER ;