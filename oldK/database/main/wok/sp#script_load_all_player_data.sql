-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS script_load_all_player_data;

DELIMITER ;;
CREATE PROCEDURE `script_load_all_player_data`( IN __player_id INT(11),
                        IN __game_id INT(11) )
BEGIN
-- called to load all player data for a given game
  SELECT `attribute`, `value` 
    FROM script_player_data
    WHERE player_id = __player_id AND
          game_id = __game_id;
            
END
;;
DELIMITER ;