-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS setPlayerBanStatus;

DELIMITER ;;
CREATE PROCEDURE `setPlayerBanStatus`( IN bannedUserName VARCHAR(80), IN msg VARCHAR(100), IN banning BOOL, IN bannersId INT(11), OUT banned BOOL  )
BEGIN
-- set the ban status of a player, copied from .net code
-- should be called in context of a transaction
	DECLARE bannedId INT(11) DEFAULT NULL;
	DECLARE status INT(11);
	DECLARE csrLogId INT(11);
	
	-- lookup the kaneva user id
	SELECT kaneva_user_id INTO bannedId 
	  FROM game_users gu
	 WHERE username = bannedUserName
	   AND gu.is_admin = 'F' 
	   AND gu.is_gm = 'F';

	IF bannedId IS NOT NULL THEN
	
		IF banning THEN -- banned
			SET status = 5; -- LOCKED = 5
			CALL kaneva.insertCSRLog( bannersId, concat( "Admin - Banning User ", cast( bannedId as char ), ' - ', msg ), 0, bannedId, csrLogId );
		 	DELETE FROM kaneva.friend_requests  WHERE user_id = bannedId OR friend_id = bannedId; 
		ELSE
			SET status = 1; -- VALIDATED = 1 -- REGNOTVALIDATED = 2,
			CALL kaneva.insertCSRLog( bannersId, concat( "Admin - Remove Ban from User ", cast( bannedId as char ), ' - ', msg ), 0, bannedId, csrLogId );
		END IF;

		UPDATE kaneva.users 
		   SET status_id = status
		 WHERE user_id = bannedId;
		
	END IF;

	SET banned = bannedId IS NOT NULL;	 
END
;;
DELIMITER ;