-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS refreshInventory;

DELIMITER ;;
CREATE PROCEDURE `refreshInventory`(
	IN __player_id int,
	IN __kaneva_user_id int,
	IN __inventory_type_req  CHAR, 
	OUT __retval int
)
    COMMENT '\n    Arguments\n      __player_id. \n      __kaneva_user_id\n\n    both are expected to be for the same user.  \n      __inventory_type_req \n     P layer or B ank \n\n  Return codes\n    retval\n      = (failure) -1 \n      = (success) numRecords \n  '
this_proc:
BEGIN

	DECLARE __pending_adds_inserted INT DEFAULT 0;
	DECLARE __sgi_defaults_inserted INT DEFAULT 0;
	DECLARE __sgi_defaults_bundle_items_inserted INT DEFAULT 0;
	DECLARE __inventory_type_req_valid INT DEFAULT 0;
	DECLARE __default_sgi_receipt_date DATETIME DEFAULT '2005-01-01 00:00:00';
	DECLARE __date_received_sgi_defaults DATETIME DEFAULT NULL;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		CALL logging.logMsg('ERROR', SCHEMA(), 'refreshInventory', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
		SET __retval = -1;
		RESIGNAL;
	END;

	-- Log proc start
	CALL logging.logMsg('TRACE', SCHEMA(), 'refreshInventory', CONCAT('BEGIN (__player_id[',CAST(__player_id AS CHAR),'] __kaneva_user_id[',CAST(__kaneva_user_id AS CHAR), '] __inventory_type_req[', __inventory_type_req, '])'));

	-- Init declared variables
	SET __retval = -1; 

	IF (__inventory_type_req = 'P' OR __inventory_type_req = 'B') THEN
		SET __inventory_type_req_valid = 1;
	ELSE
		SET __inventory_type_req_valid = 0;
	END IF;

	-- Note, there's only 1 date here. MAX is specified for coalesce to function in case of no records returned
	SELECT COALESCE(MAX(date_last_received_defaults), __default_sgi_receipt_date) INTO __date_received_sgi_defaults 
	FROM default_script_game_item_recipients 
	WHERE user_id = __kaneva_user_id;

	-- Log beginning of default game item processing
	CALL logging.logMsg('TRACE', SCHEMA(), 'refreshInventory', CONCAT('begin processing default game item adds for __player_id[',CAST(__player_id AS CHAR),']'));

	-- Insert pending adds for default game items
	SET @sgi_insert_now = NOW();
	INSERT INTO wok.inventory_pending_adds2 (`kaneva_user_id`, `global_id`, `inventory_type`, `inventory_sub_type`, `quantity`, `last_touch_datetime`)
	SELECT ordered_defaults.*, @sgi_insert_now := DATE_ADD(@sgi_insert_now, INTERVAL 1 SECOND) AS last_touch_datetime
	FROM (
		SELECT __kaneva_user_id, d.game_item_glid, 'P', 512, 1
		FROM default_script_game_items d
		INNER JOIN snapshot_script_game_items sn ON d.game_item_glid = sn.game_item_glid
		INNER JOIN script_game_item_types t ON sn.item_type = t.item_type
			AND t.inventory_compatible = 'T'
		WHERE d.automatic_insertion_start IS NOT NULL
			AND d.automatic_insertion_start > __date_received_sgi_defaults
		ORDER BY d.automatic_insertion_order ASC, d.automatic_insertion_start ASC
	) AS ordered_defaults
	ON DUPLICATE KEY UPDATE wok.inventory_pending_adds2.quantity = wok.inventory_pending_adds2.quantity + 1;

	SELECT ROW_COUNT() INTO __sgi_defaults_inserted;

	-- Insert pending adds for default game item bundles
	INSERT INTO wok.inventory_pending_adds2 (`kaneva_user_id`, `global_id`, `inventory_type`, `inventory_sub_type`, `quantity`, `last_touch_datetime`)
	SELECT __kaneva_user_id, bi.item_global_id, 'B', 512, 1, NOW()
	FROM default_script_game_items d
	INNER JOIN snapshot_script_game_items sn ON d.game_item_glid = sn.game_item_glid
	INNER JOIN script_game_item_types t ON sn.item_type = t.item_type
		AND t.inventory_compatible = 'T'
	INNER JOIN wok.item_parameters p ON d.game_item_glid = p.global_id
		AND p.param_type_id = 34
	INNER JOIN bundle_items bi ON bi.bundle_global_id = p.VALUE
	WHERE d.automatic_insertion_start IS NOT NULL
		AND d.automatic_insertion_start > __date_received_sgi_defaults
	ON DUPLICATE KEY UPDATE wok.inventory_pending_adds2.quantity = wok.inventory_pending_adds2.quantity + 1, `last_touch_datetime` = NOW();

	SELECT ROW_COUNT() INTO __sgi_defaults_bundle_items_inserted;

	-- Update date received defaults for user
	INSERT INTO default_script_game_item_recipients VALUES (__kaneva_user_id, NOW())
	ON DUPLICATE KEY UPDATE date_last_received_defaults = NOW();

	-- Log end of default game item processing
	CALL logging.logMsg('TRACE', SCHEMA(), 'refreshInventory', CONCAT('end processing default game item adds for __player_id[',CAST(__player_id AS CHAR),'] with [', __sgi_defaults_inserted ,'] game item changes and [',__sgi_defaults_bundle_items_inserted,'] bundle item changes'));

	-- Log beginning of pending add processing
	CALL logging.logMsg('TRACE', SCHEMA(), 'refreshInventory', CONCAT('begin processing pending adds for __player_id[',CAST(__player_id AS CHAR),']'));

	IF  __inventory_type_req_valid = 1 THEN

		-- Insert limited pending adds for specified inventory type
		INSERT INTO wok.inventories (`global_id`, `inventory_type`, `inventory_sub_type`, `player_id`, `quantity`, `created_date`, `last_touch_datetime`)
		SELECT wipa.global_id, wipa.inventory_type, wipa.inventory_sub_type, __player_id, wipa.quantity, NOW(),
			CASE WHEN d.game_item_id IS NOT NULL THEN wipa.last_touch_datetime ELSE NULL END AS last_touch_datetime
		FROM wok.inventory_pending_adds2 wipa
		LEFT JOIN default_script_game_items d ON wipa.global_id = d.game_item_glid
		WHERE wipa.kaneva_user_id = __kaneva_user_id 
			AND wipa.inventory_type = __inventory_type_req
			AND wipa.inventory_sub_type <> 2048
		ON DUPLICATE KEY UPDATE wok.inventories.quantity = wok.inventories.quantity + wipa.quantity, `last_touch_datetime` = NOW();

		SELECT ROW_COUNT() INTO __pending_adds_inserted;

		-- Insert unlimited pending adds for specified inventory type
		INSERT INTO wok.inventories (`global_id`, `inventory_type`, `inventory_sub_type`, `player_id`, `quantity`, `created_date`)
		SELECT wipa.global_id, wipa.inventory_type, 512, __player_id, wipa.quantity, NOW()
		FROM wok.inventory_pending_adds2 wipa
		LEFT JOIN wok.inventories wi ON wipa.global_id = wi.global_id 
			AND wi.player_id = __player_id
		WHERE wipa.kaneva_user_id = __kaneva_user_id
			AND wipa.inventory_type = __inventory_type_req
			AND wipa.inventory_sub_type = 2048 
			AND wi.global_id IS NULL;

		SELECT __pending_adds_inserted + ROW_COUNT() INTO __pending_adds_inserted;

		-- Clear pending adds for specified type
		DELETE FROM wok.inventory_pending_adds2 WHERE `kaneva_user_id` = __kaneva_user_id AND `inventory_type` = __inventory_type_req;

	ELSE

		-- Insert all limited pending adds
		INSERT INTO wok.inventories (`global_id`, `inventory_type`, `inventory_sub_type`, `player_id`, `quantity`, `created_date`, `last_touch_datetime`)
		SELECT wipa.global_id, wipa.inventory_type, wipa.inventory_sub_type, __player_id, wipa.quantity, NOW(),
			CASE WHEN d.game_item_id IS NOT NULL THEN wipa.last_touch_datetime ELSE NULL END AS last_touch_datetime
		FROM wok.inventory_pending_adds2 wipa
		LEFT JOIN default_script_game_items d ON wipa.global_id = d.game_item_glid
		WHERE wipa.kaneva_user_id = __kaneva_user_id 
			AND wipa.inventory_sub_type <> 2048
		ON DUPLICATE KEY UPDATE wok.inventories.quantity = wok.inventories.quantity + wipa.quantity, `last_touch_datetime` = NOW();

		SELECT ROW_COUNT() INTO __pending_adds_inserted;

		-- Insert all unlimited pending adds
		INSERT INTO wok.inventories (`global_id`, `inventory_type`, `inventory_sub_type`, `player_id`, `quantity`, `created_date`)
		SELECT wipa.global_id, wipa.inventory_type, 512, __player_id, wipa.quantity, NOW()
		FROM wok.inventory_pending_adds2 wipa
		LEFT JOIN wok.inventories wi ON wipa.global_id = wi.global_id 
			AND wi.player_id = __player_id
		WHERE wipa.kaneva_user_id  = __kaneva_user_id
			AND wipa.inventory_sub_type = 2048 
			AND wi.global_id IS NULL;

		SELECT __pending_adds_inserted + ROW_COUNT() INTO __pending_adds_inserted;

		-- Clear all pending adds
		DELETE FROM wok.inventory_pending_adds2 WHERE `kaneva_user_id` = __kaneva_user_id;

	END IF;

	-- Log end of pending add processing
	CALL logging.logMsg('TRACE', SCHEMA(), 'refreshInventory', CONCAT('end processing pending adds for __player_id[',CAST(__player_id AS CHAR),'] with [', __pending_adds_inserted ,'] changes'));

	-- Set and log return, then exit the proc
	SET __retval = __pending_adds_inserted;
	CALL logging.logMsg('TRACE', SCHEMA(), 'refreshInventory', CONCAT('END (__player_id[',CAST(__player_id AS CHAR),'] __kaneva_user_id[',CAST(__kaneva_user_id AS CHAR), '] __retval[',CAST(__retval AS CHAR),'])'));

END
;;
DELIMITER ;