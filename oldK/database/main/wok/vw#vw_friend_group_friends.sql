-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS vw_friend_group_friends;

DELIMITER ;;

 CREATE VIEW `vw_friend_group_friends` AS SELECT `kaneva`.`friend_group_friends`.`friend_group_id` AS `friend_group_id`,`kaneva`.`friend_group_friends`.`friend_id` AS `friend_id`,`kaneva`.`friend_group_friends`.`owner_id` AS `owner_id` 
 FROM `kaneva`.`friend_group_friends`
;;
DELIMITER ;