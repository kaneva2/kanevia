-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_inventories;

DELIMITER ;;
CREATE PROCEDURE `add_inventories`( 
  IN __global_id INT, 
  IN __inventory_type ENUM('P','B','H'), 
  IN __inventory_sub_type INT, 
  IN __player_id INT, 
  IN __armed ENUM('T','F'),
  IN __quantity INT, 
  IN __charge_quantity int
)
    COMMENT '\n    -- \n  '
this_proc:
BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- ERROR
      GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
      CALL logging.logMsg('ERROR', SCHEMA(), 'add_inventories', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
      RESIGNAL;
  END;


  CALL logging.logMsg('TRACE', SCHEMA(), 'add_inventories', CONCAT('BEGIN (__global_id[',CAST(__global_id AS CHAR), '], __inventory_type[', __inventory_type, '] __inventory_sub_type[',CAST(__inventory_sub_type AS CHAR),'] __playerId[',CAST(__playerId AS CHAR), '] __quantity[',CAST(__quantity AS CHAR),'])'));


	INSERT INTO inventories 
    (`global_id`,  `inventory_type`, `player_id`,  `armed`, `quantity`, `charge_quantity`, `inventory_sub_type`, `created_date`, `last_touch_datetime`)
	VALUES 
    (__global_id, __inventory_type, __player_id, __armed, __quantity,  __charge_quantity, __inventory_sub_type,   NOW(),          NOW())
  ON DUPLICATE KEY UPDATE quantity = __quantity, last_touch_datetime=NOW();


  CALL logging.logMsg('TRACE', SCHEMA(), 'add_inventories', CONCAT('END (__global_id[',CAST(__global_id AS CHAR), '], __inventory_type[', __inventory_type, '] __inventory_sub_type[',CAST(__inventory_sub_type AS CHAR),'] __playerId[',CAST(__playerId AS CHAR), '] __quantity[',CAST(__quantity AS CHAR),'])'));

END
;;
DELIMITER ;