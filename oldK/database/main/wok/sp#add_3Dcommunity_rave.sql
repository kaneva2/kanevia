-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_3Dcommunity_rave;

DELIMITER ;;
CREATE PROCEDURE `add_3Dcommunity_rave`(_ZoneId INTEGER UNSIGNED, _ZoneType INTEGER UNSIGNED, _NumberOfRaves INTEGER UNSIGNED,  _RavingUserId INTEGER UNSIGNED)
BEGIN
  START TRANSACTION;
	
    
    INSERT INTO zone_raves (zone_instance_id, zone_type, kaneva_user_id, created_date, rave_count) VALUES (_ZoneId, _ZoneType, _RavingUserId, Now(), _NumberOfRaves);
                
    
    UPDATE channel_zones SET raves = (raves + _NumberOfRaves) WHERE zone_instance_id = _ZoneId AND zone_type = _ZoneType;
    
    SELECT 1;
  COMMIT;
END
;;
DELIMITER ;