-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS remove_wok_dead_servers2;

DELIMITER ;;
CREATE PROCEDURE `remove_wok_dead_servers2`()
BEGIN
	
	
  	
	
	UPDATE game_users
	   SET logged_on = 'F'
	WHERE server_id IN 
  	(Select engine_id as pinged from kaneva.current_game_engines where last_ping_datetime <= DATE_SUB(NOW(),INTERVAL deadServerInterval() MINUTE));
	 
	DELETE FROM summary_active_population
	WHERE server_id IN 
  	(Select engine_id as pinged from kaneva.current_game_engines where last_ping_datetime <= DATE_SUB(NOW(),INTERVAL deadServerInterval() MINUTE));
  	
END
;;
DELIMITER ;