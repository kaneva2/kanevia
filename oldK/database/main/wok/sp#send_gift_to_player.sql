-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS send_gift_to_player;

DELIMITER ;;
CREATE PROCEDURE `send_gift_to_player`(
  IN _from_id INT, 
  IN _to_id INT, 
  IN _global_id INT, 
  IN _is_free INT, 
  IN _email_type INT, 
  IN _email_template INT
)
    COMMENT '\n    '
this_proc:
BEGIN

DECLARE __oktoproceed INTEGER;
DECLARE __gift_id     INTEGER;
DECLARE __from_name   VARCHAR(255);
DECLARE __gift_name   VARCHAR(255);
DECLARE __message     VARCHAR(255);
DECLARE __message_id  INTEGER;
DECLARE __id_system   INTEGER;
DECLARE __name_system VARCHAR(255);
DECLARE __to_email    VARCHAR(255);
DECLARE __email_hash  VARCHAR(255);
DECLARE __return      CHAR(10);
DECLARE __balance     INTEGER;
DECLARE __overloadid  INTEGER;
DECLARE __displayname VARCHAR(255);

SET __oktoproceed = 0;
SET __id_system   = 0; 
SET __name_system = 'Team Kaneva';
SET __return = '';
SET __email_hash = '';

SELECT gift_id, `name` INTO __gift_id, __gift_name FROM wok.gift_catalog_3d_items where gift_id = _global_id;
SELECT display_name INTO __displayname from kaneva.users where user_id = _from_id;

SET __oktoproceed = _is_free;


SELECT email INTO __to_email FROM kaneva.users WHERE user_id = _to_id;
SELECT email_hash INTO __email_hash FROM kaneva.users_email WHERE user_id = _to_id;
if not isnull(__displayname) and not isnull(__gift_id) and not isnull(__to_email) and not isnull(__email_hash) and __oktoproceed > 0 THEN

	START TRANSACTION;

  INSERT INTO wok.inventory_pending_adds2 (`kaneva_user_id`, `global_id`, `inventory_type`, `inventory_sub_type`, `quantity`, `last_touch_datetime`) 
  VALUES (_to_id, _global_id, 'P', 512, 1, NOW())
  ON DUPLICATE KEY UPDATE `quantity` = `quantity` + 1, last_touch_datetime = NOW();


	SET __message = concat('I hope you enjoy this ', __gift_name, '.  Have a great day!');
	call wok.insertMessage(_from_id, 0, _to_id ,0 ,'You have been sent a gift' , __message, 0, 0, null, __message_id);

    SET __overloadid = shard_info.get_email_overload_fields_id();

    INSERT INTO viral_email.email_overload_fields (overload_id, `key`, value) values (__overloadid, "#GiftFromName#", __displayname );
    INSERT INTO viral_email.email_overload_fields (overload_id, `key`, value) values (__overloadid, "#GiftName#", __gift_name );
    INSERT INTO viral_email.email_overload_fields (overload_id, `key`, value) values (__overloadid, "#GiftId#", __gift_id );

	INSERT INTO viral_email.email_queue (date_queued, email_type_id, email_template_id, email_to, invite_id, email_hash, overload_id)
         VALUES (now(), _email_type, _email_template, __to_email, 0, __email_hash, __overloadid);
	
	SELECT balance INTO __balance FROM kaneva.user_balances WHERE user_id = _to_id AND kei_point_id = 'KPOINT';
	IF isnull(__balance) THEN
		SET __balance = 0;
	END IF;
	
	insert into kaneva.wok_transaction_log (created_date, user_id, balance_prev, balance, transaction_amount, kei_point_id, transaction_type)
	values( now(),_to_id , __balance, __balance,0,"KPOINT",30);
	
	SET __return = 'Ok';

	COMMIT;
END IF;
SELECT __return as status;

END
;;
DELIMITER ;