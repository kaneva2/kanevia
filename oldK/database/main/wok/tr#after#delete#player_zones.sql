-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS pz_delete;

DELIMITER ;;
CREATE TRIGGER `pz_delete` AFTER DELETE ON `player_zones` FOR EACH ROW BEGIN
-- trigger to update counts in summary table
	CALL zoningOut( old.current_zone_index, old.current_zone_instance_id, old.server_id );  
END
;;
DELIMITER ;