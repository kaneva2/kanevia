-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS rewardCredits;

DELIMITER ;;
CREATE PROCEDURE `rewardCredits`( in transaction_amount float, in v_kei_point_id varchar(20), in start_date datetime, in end_date datetime )
begin
	declare user_id,return_code,tranId int;
	declare balance float;
	declare done int default 0;

	declare curReward cursor for Select distinct kaneva_user_id from
    (
      SELECT distinct(gu.kaneva_user_id) as user_id FROM wok.login_log wll
         Inner Join wok.game_users gu on wll.user_id = gu.user_id
         where
             created_date between date(start_date) and end_date
      and
             logout_date between start_date and date_add(end_date, Interval 12 hour)
      )X;
                DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

                open curReward;

                repeat
					fetch curReward into user_id;
					call kaneva.apply_transaction_to_user_balance( user_id, transaction_amount,12,v_kei_point_id, return_code, balance, tranId);
					Insert into kaneva.reward_log (user_id,date_created,amount,kei_point_id,transaction_type, return_code) VALUES(user_id, now(), transaction_amount, kei_point_id, 12, return_code);
                until done END REPEAT;
                close curReward;
end
;;
DELIMITER ;