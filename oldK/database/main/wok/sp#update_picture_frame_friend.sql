-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_picture_frame_friend;

DELIMITER ;;
CREATE PROCEDURE `update_picture_frame_friend`( IN __friendId INT(11), IN __objSerialNumber INT(11),
											  IN __zoneIndex INT(11), IN __instanceId INT(11), 
											  OUT __textureURL VARCHAR(1000),
											  OUT __rc INT )
BEGIN
		DECLARE __mature_asset enum('Y','N') DEFAULT 'N';

		SET __rc = 0;
		
		CALL fetch_image_url_and_mature( __friendId, 'F', __textureURL, __mature_asset );	

		IF __textureURL IS NULL THEN 
			SET __rc = 2; -- not found 
		ELSEIF __mature_asset = 'Y' THEN

			IF NOT is_mature_zone( __zoneIndex, __instanceId ) THEN 
      			-- can't do it!
      			SET __rc = 12; -- need AP
      		END IF;
      		
		END IF;

		-- remove any texture first
		DELETE FROM dynamic_object_parameters
		  WHERE obj_placement_id = __objSerialNumber 
		    AND param_type_id = 14; -- texture_asset_id
		    
		INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
		VALUES (__objSerialNumber, 20, __friendId) ON DUPLICATE KEY UPDATE value = __friendId;
		
		INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
		VALUES (__objSerialNumber, 15, __textureURL) ON DUPLICATE KEY UPDATE value = __textureURL;
		
END
;;
DELIMITER ;