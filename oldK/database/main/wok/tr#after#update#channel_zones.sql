-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS after_channel_zones_update;

DELIMITER ;;
CREATE TRIGGER `after_channel_zones_update` AFTER UPDATE ON `channel_zones` 
    FOR EACH ROW BEGIN
	IF OLD.zone_index <> NEW.zone_index THEN 
		DELETE FROM summary_active_population
		 WHERE zone_index = OLD.zone_index 
		   AND zone_instance_id = OLD.zone_instance_id;
	END IF;
END
;;
DELIMITER ;