-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_dyn_obj_playlist_id;

DELIMITER ;;
CREATE PROCEDURE `update_dyn_obj_playlist_id`( IN playlistId INT(11), IN objId INT(11),
											 IN zoneIndex INT(11), IN instanceId INT(11) )
BEGIN
-- update the url on a dynamic object 
--
-- params:
--      playlistId -- new url
--      objId -- dyn object id
--		zoneIndex/instanceId -- zone Info, used if not gm 
	DECLARE glid INT(11) DEFAULT NULL;
	DECLARE isGlobalPlayer INT DEFAULT 0;
	
	SELECT do.global_id, COALESCE(dopl.video_range, 0)=0 INTO glid, isGlobalPlayer
	   FROM `dynamic_objects` do 
	   LEFT OUTER JOIN `dynamic_object_playlists` dopl ON do.obj_placement_id=dopl.obj_placement_id
	   WHERE do.obj_placement_id = objId;
	   
	IF glid IS NOT NULL THEN 	   

		-- remove any swfs
		DELETE dop 
			FROM dynamic_objects `do` 
			INNER JOIN dynamic_object_parameters `dop` ON dop.obj_placement_id = `do`.obj_placement_id 
			WHERE zone_index = zoneIndex AND zone_instance_id = instanceId AND do.obj_placement_id = objId AND param_type_id IN (17,18); -- swf_param/name

		INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
 		 VALUES (objId, 16, playlistId) -- asset group id
		ON DUPLICATE KEY UPDATE value = playlistId;
		
		INSERT INTO dynamic_object_playlists
			(zone_index, zone_instance_id, global_id, obj_placement_id, asset_group_id) 
		VALUES (zoneIndex, instanceId, glid, objId, playlistId)
			ON DUPLICATE KEY UPDATE asset_group_id = playlistId;

		-- Sync up all global media players
		IF isGlobalPlayer=1 THEN
			UPDATE dynamic_object_playlists 
				SET asset_group_id = playlistId
				WHERE zone_index=zoneIndex AND zone_instance_id=instanceId AND video_range=0;

			SELECT obj_placement_id
				FROM dynamic_object_playlists 
				WHERE zone_index=zoneIndex AND zone_instance_id=instanceId AND video_range=0;

		ELSE
			SELECT objId;

		END IF;

	END IF;
		
END
;;
DELIMITER ;