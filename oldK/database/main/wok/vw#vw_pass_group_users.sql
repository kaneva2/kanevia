-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS vw_pass_group_users;

DELIMITER ;;

 CREATE VIEW `vw_pass_group_users` AS SELECT `pg`.`kaneva_user_id` AS `kaneva_user_id`,`pg`.`pass_group_id` AS `pass_group_id`,`pg`.`created_date` AS `created_date`,`sub`.`end_date` AS `end_date` 
 FROM ( ( `wok`.`pass_group_users` `pg` 
 JOIN `kaneva`.`subscriptions_to_passgroups` `sub_pass` on( ( `pg`.`pass_group_id` = `sub_pass`.`pass_group_id`) ) )  
 JOIN `kaneva`.`user_subscriptions` `sub` on( ( ( `sub`.`subscription_id` = `sub_pass`.`subscription_id`)  
 AND ( `pg`.`kaneva_user_id` = `sub`.`user_id`) ) ) )  
 WHERE ( ( `sub`.`status_id` 
 IN ( 2,4) )  
 AND ( `sub`.`end_date` > now( ) ) ) 
;;
DELIMITER ;