-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS selectServer;

DELIMITER ;;
CREATE PROCEDURE `selectServer`( IN channelZoneId INT(11), IN kanevaUserId INT(11),
                                            IN zoneType INT(11),IN zoneIndex INT(11), IN instanceId INT(11),
                                              IN myServerId INT(11),
                                              INOUT zoneServerId INT(11),
											  OUT scriptServerId INT(11))
BEGIN											  
-- select the server for the user/zone config
--    this function can be extended to do load balancing
--
-- params:
--      channelZoneId -- if not null existing row's id
--        kanevaUserId -- who is zoning
--        zoneIndex/instanceId -- zone Info
--        zoneServerId -- if not null, current server
--        myServerId -- id of current server where zoning user is
    DECLARE playersInZone INT DEFAULT 0;
    DECLARE switchedServer BOOL DEFAULT FALSE;
    DECLARE oldServerId INT(11);

	-- value so update db
    DECLARE newServerId INT(11) DEFAULT 0;

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
    
    SET oldServerId = zoneServerId;

    CALL logDebugMsg( 'selectServer', concat( 'in select server and got ', cast( kanevaUserId as char ),' my= ', cast(coalesce(myServerId,'null') as char ), ' cur= ', cast(coalesce(zoneServerId,'null') as CHAR ), ' zi=',cast(IFNULL(hex(zoneIndex),'null') as char), ' instance=',cast(IFNULL(instanceId,'null') as char ) ) );

    -- is anyone is this instance of this zone?
    SELECT `count` INTO playersInZone
    FROM summary_active_population
    WHERE zone_instance_id = instanceId
      AND zone_index = zoneIndex;

    CALL logDebugMsg( 'selectServer', concat( 'players in zone ', cast( IFNULL(playersInZone,0) as char ), ' values ', cast( instanceId as char), ' ', cast( zoneType as char ), ' ', cast( zoneIndex as char ) ) );
    IF playersInZone = 0 THEN
        -- no one in zone, use my server

        CALL logDebugMsg( 'selectServer', 'player in zone step 2' );

        IF myServerId <> 0 THEN
            -- no one here, use my server
            IF zoneServerId <> myServerId THEN
                CALL logDebugMsg( 'selectServer', concat('switched from ', zoneServerId, ' to ', myServerId ));
                SET zoneServerId = myServerId;
            END IF;
        -- ELSE
            -- CALL logDebugMsg( 'selectServer', concat('since serverId zero, cleared ', zoneServerId ));
            -- clear out current server to get a load balanced one
            -- SET zoneServerId = 0;
        END IF;

    END IF;

    -- if no one in zone, then set server to this server, if switching
   CALL get_load_balanced_server_for_player( zoneIndex
                                             , kanevaUserId
                                             , instanceId
                                             , zoneServerId
                                             , scriptServerId
                                             , switchedServer );

	IF zoneServerId = 0 THEN 
		IF myServerId <> 0 THEN -- is zero if called from web site
			SET newServerId = myServerId;
		END IF;
	ELSE
		SET newServerId = zoneServerId;
	END IF;
	
    CALL logDebugMsg( 'selectServer', concat( 'channelZoneId ', IFNULL(cast( channelZoneId as char ),'<NULL>'), ' old ', IFNULL(cast( oldServerId as char),'<NULL>'), ' new ', IFNULL(cast( newServerId as char),'<NULL>') ) );

END
;;
DELIMITER ;