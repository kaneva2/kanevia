-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_zone_playlist_media;

DELIMITER ;;
CREATE PROCEDURE `get_zone_playlist_media`(IN __zone_index INT, IN __zone_instance_id INT, IN __playlist_id_filter INT)
BEGIN
	DECLARE __is_access_pass INT;

	-- Based on get_playlist(), standard asset_group_id section
	SELECT is_mature_zone( __zone_index, __zone_instance_id) INTO __is_access_pass;

	SELECT ga.`asset_group_id`, ga.`asset_id`, a.`asset_offsite_id`, `asset_type_id`, `asset_sub_type_id`, 
		    `run_time_seconds`, `thumbnail_medium_path`, a.`media_path`, a.`name`
	  FROM `kaneva`.`asset_group_assets` ga
	 INNER JOIN `kaneva`.`assets` a on ga.`asset_id` = a.`asset_id`
	 INNER JOIN `kaneva`.`assets_stats` d on a.`asset_id` = d.`asset_id`	-- ??
	 INNER JOIN `dynamic_object_playlists` dopl ON dopl.`asset_group_id`=ga.`asset_group_id`
	 WHERE `zone_index` = __zone_index
	   AND `zone_instance_id` = __zone_instance_id
	   AND (__playlist_id_filter = 0 OR __playlist_id_filter = ga.`asset_group_id`)
	   AND `run_time_seconds` > 0
	   AND `asset_sub_type_id` >= 0											-- ??
	   AND (__is_access_pass = 1 OR mature = 'N')
	 GROUP BY ga.`asset_group_id`, ga.`asset_id`		-- suppress duplicated records
	 ORDER BY ga.sort_order, a.created_date desc;
END
;;
DELIMITER ;