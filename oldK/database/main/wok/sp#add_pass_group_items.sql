-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_pass_group_items;

DELIMITER ;;
CREATE PROCEDURE `add_pass_group_items`(passGroupId	INT(11), itemId	INT(11), modifierUserId	INT(11))
BEGIN
  START TRANSACTION;
    /*first insert into the pass_group_items table*/
    INSERT INTO wok.pass_group_items (global_id, pass_group_id) VALUES(itemId, passGroupId) ON DUPLICATE KEY UPDATE pass_group_id = passGroupId;
    /*then insert into the audit table*/
    INSERT INTO wok.pass_group_items_audit (global_id, pass_group_id, modifier_kaneva_user_id, change_date, operation) VALUES (itemId, passGroupId, modifierUserId, now(), 'INSERT');
  COMMIT;
END
;;
DELIMITER ;