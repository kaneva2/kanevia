-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS vw_clan_members;

DELIMITER ;;

 CREATE VIEW `vw_clan_members` AS SELECT `cm`.`clan_id` AS `clan_id`,`p`.`player_id` AS `player_id`,`p`.`name` AS `name` 
 FROM ( `clan_members` `cm` 
 JOIN `players` `p` on( ( `cm`.`player_id` = `p`.`player_id`) ) ) 
;;
DELIMITER ;