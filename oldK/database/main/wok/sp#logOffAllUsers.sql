-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS logOffAllUsers;

DELIMITER ;;
CREATE PROCEDURE `logOffAllUsers`( IN serverId INT(11), IN serverName VARCHAR(64), IN serverPort INT(11) )
BEGIN
		DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
		IF serverId = 0 THEN
			
			
			SELECT engine_id INTO serverId
			  FROM kaneva.current_game_engines ce
			 WHERE ce.server_name = serverName
			   AND ce.port = serverPort;
					
		END IF;
		
		IF serverId <> 0 THEN
		
			
			UPDATE player_zones
			   SET server_id = 0
			 WHERE server_id = serverId;
			DELETE FROM summary_active_population
			 WHERE server_id = serverId;
			
			UPDATE `game_users` 
			   SET `last_update_time` = NOW(), `logged_on` = 'F'
			 WHERE `server_id` = serverId;
		   UPDATE channel_zones 
			  SET tied_to_server = 'NOT_TIED', 
			      `server_id` = 0
			WHERE `server_id` = serverId
			  AND `tied_to_server` <> 'PERMANENTLY_TIED';
			  
			
			
			
			
			
		END IF;
				
END
;;
DELIMITER ;