-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_account_data;

DELIMITER ;;
CREATE PROCEDURE `get_account_data`( 	IN __my_server_id INT,
										IN __kaneva_user_id INT, 
										IN __username VARCHAR(80),
									   	IN __current_ip VARCHAR(30),
									   	IN __gender CHAR,
									   	IN __birth_date DATETIME,
									   	IN __show_mature INT )
BEGIN
	-- get all the datasets about a account, in the order C++ wants them
	DECLARE __user_id INT;
	DECLARE __is_gm ENUM('T','F');
	DECLARE __is_admin ENUM('T','F');
	DECLARE __can_spawn ENUM('T','F');
	DECLARE __server_id INT;
	DECLARE __logged_on ENUM('T','F');
	DECLARE __time_played INT;
	DECLARE __age TINYINT(4);
	DECLARE __show_mature BINARY(1);
	DECLARE __row_count INT DEFAULT 0;
	
	SELECT `user_id`, `is_gm`, `can_spawn`, `is_admin`, `server_id`, `logged_on`, `time_played`, `age`, `show_mature` 
	  INTO __user_id, __is_gm, __can_spawn, __is_admin, __server_id, __logged_on, __time_played, __age, __show_mature 
	 FROM  `vw_users`
	 WHERE kaneva_user_id = __kaneva_user_id;

	SET __row_count = FOUND_ROWS();
	IF __row_count  = 1 THEN 
		CALL logDebugMsg( 'get_account_data', concat( 'Found user kuid=', cast( __kaneva_user_id as char ), ' logged_on = ', __logged_on ) );
		IF __logged_on = 'T' AND __server_id <> __my_server_id THEN
			SELECT 2, __server_id; -- already logged on
		ELSE
			SELECT 0, __user_id, __kaneva_user_id, __is_gm, __can_spawn, __is_admin, __my_server_id, __logged_on, __time_played, __age, __show_mature;
		
			-- they exist, this will log them on and return players
			CALL log_on_user( __my_server_id, __current_ip, __user_id, __kaneva_user_id );
		END IF;
	ELSEIF __row_count = 0 THEN
		CALL logDebugMsg( 'get_account_data', concat( 'Didnt find user, creating user kuid=', cast( __kaneva_user_id as char ) ) );
		-- they don't exist, add them
		CALL create_game_user( __kaneva_user_id, __username, __my_server_id, __current_ip, __gender,
							   __birth_date, __show_mature, __user_id ); 
							   
		SELECT 1, __user_id; -- 1 = indicate to C++ we are creating a new one
	ELSE
		SELECT 3; -- too many rows!
	END IF;
	
END
;;
DELIMITER ;