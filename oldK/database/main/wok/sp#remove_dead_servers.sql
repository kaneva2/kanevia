-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS remove_dead_servers;

DELIMITER ;;
CREATE PROCEDURE `remove_dead_servers`()
BEGIN
-- called to remove players from a server than hasn't pinged in for deadServerInterval minutes
	/* 
		triggers will do the following
			1) game_users trigger sets players.in_game = 'F'
			2) players trigger deletes from player_zones
			3) player_zones trigger updates summary_active_population counts
	*/
	DECLARE done INT DEFAULT 0;
	DECLARE serverId INT(11) DEFAULT 0;
	DECLARE statusId INT(11) DEFAULT 0;
	DECLARE servers CURSOR FOR SELECT server_id, server_status_id FROM vw_game_servers 
							   	WHERE server_status_id <> 0 -- stopped
							   	  AND last_ping_datetime <= DATE_SUB(NOW(),INTERVAL deadServerInterval() MINUTE) FOR UPDATE;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

 	OPEN servers;

		REPEAT	
			FETCH servers into serverId, statusId;

			IF NOT done THEN 
				CALL logMsg( 'remove_dead_servers', 'ERROR', concat('Removing dead server ', cast( serverId as char ) ) );
				
				-- '',0 is server port, used if no serverId, new state (2=stopping), 0 = pop
				CALL server_changed_state( serverId, '', 0, 2, 0 ); 
				UPDATE vw_game_servers
				   SET server_status_id = 0 -- stoppped
				 WHERE server_id = serverId;
			 END IF;
			
		UNTIL done END REPEAT;
		
	CLOSE servers;
						   	
  	
END
;;
DELIMITER ;