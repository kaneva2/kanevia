-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS reClotheAvatars;

DELIMITER ;;
CREATE PROCEDURE `reClotheAvatars`()
BEGIN

	DECLARE playerId INT(11);
	DECLARE playerGender CHAR(1);
	DECLARE done INT DEFAULT 0;
	
	DECLARE folks CURSOR FOR 
			SELECT player_id, gender 
			  FROM players p inner join kaneva.users u on p.kaneva_user_id = u.user_id;


	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN folks;

	REPEAT	
		FETCH folks into playerId, playerGender;
		IF done = 0 THEN 

			IF playerGender = 'F' THEN 
				-- for any existing folks, give them clothes 
				insert into inventories (global_id,inventory_type,player_id,armed,quantity,charge_quantity,is_gift,pending_add,created_date)
				values ( 1247, 'P', playerId, 'T', 1, 0, 0, 'T', NOW() ),
				       ( 1248, 'P', playerId, 'T', 1, 0, 0, 'T', NOW() ),
				       ( 1249, 'P', playerId, 'T', 1, 0, 0, 'T', NOW() )
				ON DUPLICATE KEY UPDATE quantity = 1;
			ELSEIF playerGender = 'M' THEN 

				insert into inventories (global_id,inventory_type,player_id,armed,quantity,charge_quantity,is_gift,pending_add,created_date)
				values ( 1172, 'P', playerId, 'T', 1, 0, 0, 'T', NOW() ),
				       ( 1173, 'P', playerId, 'T', 1, 0, 0, 'T', NOW() ),
				       ( 1174, 'P', playerId, 'T', 1, 0, 0, 'T', NOW() )
				ON DUPLICATE KEY UPDATE quantity = 1;
			END IF;
		END IF;
		
	UNTIL done END REPEAT;
	
	CLOSE folks;
	

END
;;
DELIMITER ;