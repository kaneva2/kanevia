-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS deletePlayerById;

DELIMITER ;;
CREATE PROCEDURE `deletePlayerById`( IN playerId INT(11) )
BEGIN
-- admin helper proc
	delete from players WHERE player_id = playerId;
END
;;
DELIMITER ;