-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS accept3dGift;

DELIMITER ;;
CREATE PROCEDURE `accept3dGift`( IN userId INT, IN messageId INT )
BEGIN
-- accept a 3D gift, called from server
	DECLARE giftId INT DEFAULT 0;
	DECLARE fromId INT DEFAULT 0;
	DECLARE newMessageId INT DEFAULT 0;
	DECLARE toName VARCHAR(80);
	DECLARE giftName VARCHAR(125);
	DECLARE ACCEPT_SUBJ VARCHAR(200) DEFAULT '{0} has accepted your gift';
	DECLARE ACCEPT_MSG VARCHAR(500) DEFAULT '{0} has accepted the gift, "{1}," which you so generously gave.';
    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
			 			    
	SELECT mg.gift_id, m.from_id, i.name, u.username INTO giftId, fromId, giftName, toName
	  FROM kaneva.messages m
	       INNER JOIN
	       kaneva.message_gifts mg ON m.message_id = mg.message_id
	       INNER JOIN wok.items i ON mg.gift_id = i.global_id
	       INNER JOIN kaneva.users u ON u.user_id = m.to_id
	 WHERE mg.gift_type = '3D'
		AND gift_status = 'U'
	  AND m.to_id = userId
	  AND m.message_id = messageId
	  FOR UPDATE;

	IF giftId <> 0 THEN

		CALL kaneva.update_message_gifts( messageId, 'A');

		IF ROW_COUNT() = 1 THEN
		
			 -- send reply to giver
			 CALL insertMessage( userId, 0,  -- 0 = new msg
			 			    fromId, 0, -- 0 = new msg 
			 			    REPLACE( ACCEPT_SUBJ, '{0}', toName ),
			 			    REPLACE( REPLACE( ACCEPT_MSG, '{0}', toName ), '{1}', giftName ), 
			 			    0, -- replied
			 			    0, -- private msg
			 			    0,  -- channel id
			 			    newMessageId );
			 			    
			IF newMessageId = 0 THEN
				CALL logMsg( 'accepted3dGift', 'ERROR', concat('Error inserting reply for accept for messageId = ', cast( messageId as char )) );
			END IF;
			 
		END IF;
		 
	END IF;
	
	SELECT giftId, fromId; -- return values
END
;;
DELIMITER ;