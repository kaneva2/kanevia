-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_online_friends;

DELIMITER ;;
CREATE PROCEDURE `get_online_friends`( IN __user_id INT(11) )
BEGIN
-- part of schema abstration to remove all Kaneva references from C++ 
-- and move them to SPs, which can be site-specific
-- called when broadcasting to friends that you're logging in/out
	-- copied largely from UserUtility.GetFriends
	(SELECT wp.player_id, wp.name
	  FROM  vw_local_users ku 
	       INNER JOIN  vw_friends kf ON ku.user_id = kf.friend_id 
	       INNER JOIN  players wp ON wp.kaneva_user_id = ku.user_id 
	 WHERE kf.user_id = __user_id 
	    AND (ku.status_id = 1  --    (int) Constants.eUSER_STATUS.REGVALIDATED + 
	        OR ku.status_id = 2)  -- (int) Constants.eUSER_STATUS.REGNOTVALIDATED + ) 
	    AND wp.in_game = 'T' 
			) ;
END
;;
DELIMITER ;