-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS purge_inventories_transaction_log;

DELIMITER ;;
CREATE PROCEDURE `purge_inventories_transaction_log`(__purge_before_date	DATE, __purge_cutoff_time DATETIME)
BEGIN

DECLARE	__Max_trans_id		int;
DECLARE	__inv_tran_log_RowCnt		int;
DECLARE __message_text varchar(500);

SELECT	IFNULL(COUNT(trans_id),0) INTO __inv_tran_log_RowCnt
	FROM	inventories_transaction_log
	WHERE	created_date < __purge_before_date;

SELECT IFNULL(MAX(trans_id),__purge_before_date) INTO __Max_trans_id
	FROM	inventories_transaction_log
	WHERE	created_date < __purge_before_date;

SELECT CONCAT('Starting purge ... ',CAST(__inv_tran_log_RowCnt as CHAR),' rows to purge. Purge before date: ',
											CAST(__purge_before_date as CHAR),'; Purge cutoff time: ',CAST(__purge_cutoff_time AS CHAR)) INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'purge_inventories_transaction_log',__message_text);

-- Loop while anything to delete.
WHILE (__inv_tran_log_RowCnt > 0 AND NOW() < __purge_cutoff_time)    DO

	DELETE FROM	inventories_transaction_log WHERE trans_id <= __Max_trans_id LIMIT 5000;

	SELECT IFNULL(ROW_COUNT(),0) INTO __inv_tran_log_RowCnt;

	SELECT  CONCAT('Purged ',CAST(__inv_tran_log_RowCnt AS CHAR),' rows.') INTO __message_text;
	CALL shard_info.add_maintenance_log (DATABASE(),'purge_inventories_transaction_log',__message_text);

	END WHILE;

SELECT 'Purge completed.' INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'purge_inventories_transaction_log',__message_text);

END
;;
DELIMITER ;