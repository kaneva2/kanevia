-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_script_available_item;

DELIMITER ;;
CREATE PROCEDURE `add_script_available_item`( IN __item_id VARCHAR(20) )
BEGIN

  INSERT IGNORE INTO script_available_items(`item_id`) 
    VALUES (__item_id);

END
;;
DELIMITER ;