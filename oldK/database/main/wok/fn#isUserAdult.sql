-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS isUserAdult;

DELIMITER ;;
CREATE FUNCTION `isUserAdult`( kanevaUserId INT ) RETURNS binary(1)
    READS SQL DATA
BEGIN
	DECLARE ret BINARY(1);
	
	SELECT show_mature INTO ret FROM kaneva.users WHERE user_id = kanevaUserId;
	return ret;
END
;;
DELIMITER ;