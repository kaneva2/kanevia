-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS hasCoverCharge;

DELIMITER ;;
CREATE PROCEDURE `hasCoverCharge`( IN kanevaUserId INT(11), IN zoneIndex INT(11), 
				IN instanceId INT(11), OUT coverCharge INT(11), OUT ownerKanevaUserId INT(11) )
BEGIN
-- see if this player has pd cover charge, and if not return non-zero value
	DECLARE foundKanevaUserId INT(11) DEFAULT NULL;
	DECLARE role INT;
	
	SET coverCharge = 0;
	SET ownerKanevaUserId = 0;
	
	IF zoneType(zoneIndex) = 3 OR zoneType(zoneIndex) = 6 THEN -- only apt and hangouts
		
		SELECT cover_charge, cc.kaneva_user_id, cz.kaneva_user_id INTO coverCharge, foundKanevaUserId, ownerKanevaUserId
		FROM channel_zones cz
		       LEFT OUTER JOIN
		          channel_zone_cover_charges cc
		       ON cc.zone_index = cz.zone_index
		      AND cc.zone_instance_id = cz.zone_instance_id 
			  AND cc.kaneva_user_id = kanevaUserId
			  AND cc.expiration_date > now()
		 WHERE cz.zone_index = zoneIndex
		   AND cz.zone_instance_id = instanceId;
		IF coverCharge > 0 THEN 
			IF foundKanevaUserId IS NOT NULL THEN 
				SET coverCharge = 0; -- player has paid
			ELSEIF kanevaUserId = ownerKanevaUserId THEN
				SET coverCharge = 0; -- player is owner
			ELSEIF zoneType(zoneIndex) = 6 THEN
				CALL communityRole(kanevaUserId, instanceId, role );
				IF role <= 2 THEN -- 1 = owner, 2 = moderator
					SET coverCharge = 0; -- player is owner or moderator
				END IF;
			END IF ;
			
		END IF;
	END IF;	
	
END
;;
DELIMITER ;