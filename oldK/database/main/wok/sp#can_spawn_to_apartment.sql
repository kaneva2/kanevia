-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS can_spawn_to_apartment;

DELIMITER ;;
CREATE PROCEDURE `can_spawn_to_apartment`( IN sourcePlayerId INT, 
						IN sourceKanevaId INT, 
						IN destKanevaId INT, 
						IN myServerId INT(11), 
						IN isGm CHAR,
					    IN country CHAR(2),
					    IN birthDate DATE,
					    IN showMature BINARY(1),
						IN gameNameNoSpaces VARCHAR(100),
						OUT reasonCode INT,
						OUT housingZoneIndex INT(11),
						OUT serverId INT(11), 
						OUT scriptServerId INT(11), 
						OUT playerId INT(11), 
						OUT coverCharge INT(11),
						OUT zoneOwnerKanevaId INT(11),
						OUT url VARCHAR(255) )
BEGIN
-- only called from C++, directly or via canSpawnToURL SP
	DECLARE playerName VARCHAR(80) DEFAULT '';
	SET reasonCode = 2;  -- 0 = ok, 1 = not in game, 2 = not found, 3 = permission, 4 = blocked, 5 = zoneFull, 6 = server full, 7 = pass required
	SET coverCharge = 0;
	SET zoneOwnerKanevaId = 0;
	
	CALL can_player_spawn_to_apartment_common( sourcePlayerId, sourceKanevaId, destKanevaId, myServerId, isGm, country, birthDate, showMature, playerId, housingZoneIndex, reasonCode, serverId, scriptServerId, playerName );
	
	CALL hasCoverCharge( sourceKanevaId, housingZoneIndex, playerId, coverCharge, zoneOwnerKanevaId );
	-- return as result set
	SET serverId = IFNULL( serverId, 0 ); -- not switching servers
	SET url = concat_vw_url(gameNameNoSpaces,playerName,3);
END
;;
DELIMITER ;