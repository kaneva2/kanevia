-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_peak_concurrent_players;

DELIMITER ;;
CREATE PROCEDURE `update_peak_concurrent_players`( IN __peak_count INT(11) )
BEGIN

INSERT INTO peak_concurrent_players (cur_date, cur_hour, peak_count)
	VALUES (DATE(now()), HOUR(now()), __peak_count)
	ON DUPLICATE KEY UPDATE peak_count = CASE WHEN peak_count > __peak_count THEN peak_count ELSE __peak_count END;

END
;;
DELIMITER ;