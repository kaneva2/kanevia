-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS z_copy_dynamic_objects;

DELIMITER ;;
CREATE PROCEDURE `z_copy_dynamic_objects`(IN _srcZoneIndex INT, IN _srcInstanceId INT, 
	IN _destZoneIndex INT, IN _destInstanceId INT)
BEGIN 
  -- cursor parameters 
  DECLARE __done INT; 
  DECLARE __player_id INT; 
  DECLARE __object_id INT; 
  DECLARE __global_id  INT; 
  DECLARE __position_x DOUBLE; 
  DECLARE __position_y DOUBLE; 
  DECLARE __position_z DOUBLE; 
  DECLARE __rotation_x DOUBLE; 
  DECLARE __rotation_y DOUBLE; 
  DECLARE __rotation_z DOUBLE; 
  DECLARE __asset_group_id INT; 
  DECLARE __texture_asset_id INT; 
  DECLARE __texture_url VARCHAR(255); 
  DECLARE __swf_name VARCHAR(255); 
  DECLARE __swf_parameter varchar(255); 
  DECLARE __attachable INT; 
  DECLARE __obj_placement_id INT; 
  DECLARE __inventory_sub_type INT; 
  DECLARE __world_object_id INT; 
  DECLARE __obj_id varchar(255); 
  DECLARE __zone_index_plain INT; 

  -- query the current source objects 
  DECLARE __fromObjects CURSOR FOR 
    SELECT object_id, global_id, position_x, position_y, position_z, 
                     rotation_x, rotation_y, rotation_z, texture_asset_id, texture_url, 
                     swf_name, swf_parameter, asset_group_id, inventory_sub_type 
            FROM dynamic_objects 
     WHERE zone_index = _srcZoneIndex 
     AND zone_instance_id = _srcInstanceId; 
  DECLARE __fromWorldObjSettings CURSOR FOR 
    SELECT world_object_id, asset_id, texture_url 
      FROM world_object_player_settings 
     WHERE zone_index = _srcZoneIndex 
       AND zone_instance_id = _srcInstanceId; 
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1; 

  -- remove existing object from destination 
  DELETE from dynamic_objects WHERE zone_index = _destZoneIndex AND zone_instance_id = _destInstanceId; 
  DELETE from world_object_player_settings WHERE zone_index = _destZoneIndex AND zone_instance_id = _destInstanceId; 
  
  -- get the player id for the destination zone (if this fails then the destination is not valid) 
  SELECT player_id 
	FROM players p, channel_zones z 
	where p.kaneva_user_id = z.kaneva_user_id and z.zone_index = _destZoneIndex 
	AND z.zone_instance_id = _destInstanceId 
	INTO __player_id; 
  
  -- get the plain zone index from the source (if this fails the source was not valid) 
  SELECT zone_index_plain from channel_zones 
	where zone_index = _srcZoneIndex AND zone_instance_id = _srcInstanceId 
	INTO __zone_index_plain; 
  
  -- we must have these two, if not then one or both of the input parameters are incorrect 
  IF __player_id > 0 AND __zone_index_plain > 0 THEN 
  
  -- switch the instance and plain index on the destination 
  UPDATE channel_zones set zone_index_plain = __zone_index_plain, zone_index = _srcZoneIndex 
	where zone_index = _destZoneIndex and zone_instance_id = _destInstanceId; 
  SET _destZoneIndex = _srcZoneIndex; 
  
  -- do the dynamic objects 
  SET __done = 0; 
  OPEN __fromObjects; 
    REPEAT 
      FETCH __fromObjects into __object_id, __global_id, __position_x, __position_y, __position_z, 
		__rotation_x, __rotation_y, __rotation_z, __texture_asset_id, __texture_url, __swf_name, 
		__swf_parameter, __asset_group_id, __inventory_sub_type; 
        IF NOT __done THEN 
      SET __obj_placement_id = shard_info.get_dynamic_objects_id(); 
      INSERT INTO dynamic_objects (obj_placement_id, player_id, zone_index, zone_instance_id, object_id, 
		global_id, inventory_sub_type, position_x, position_y, position_z, rotation_x, rotation_y, 
		rotation_z, texture_asset_id, texture_url, swf_name, swf_parameter, asset_group_id, created_date) 
       VALUES ( __obj_placement_id, __player_id, _destZoneIndex, _destInstanceId, __object_id, __global_id, 
		__inventory_sub_type, __position_x, __position_y, __position_z, __rotation_x, __rotation_y, __rotation_z, 
		__texture_asset_id, __texture_url, __swf_name, __swf_parameter, __asset_group_id, NOW() ); 
    END IF; 
  UNTIL __done END REPEAT; 
  CLOSE __fromObjects; 
  
  SET __done = 0; 
  OPEN __fromWorldObjSettings; 
    REPEAT 
      FETCH __fromWorldObjSettings into __obj_id, __texture_asset_id, __texture_url; 
        IF NOT __done THEN 
      SET __world_object_id = shard_info.get_world_object_player_settings_id(); 
      INSERT INTO world_object_player_settings (id, world_object_id, asset_id, texture_url, zone_index, 
		zone_instance_id, last_updated_datetime) 
           VALUES ( __world_object_id, __obj_id, __texture_asset_id, __texture_url, _destZoneIndex, _destInstanceId, NOW()); 
    END IF; 
  UNTIL __done END REPEAT; 
  CLOSE __fromWorldObjSettings; 
  END IF; -- if player and index are ok 
END
;;
DELIMITER ;