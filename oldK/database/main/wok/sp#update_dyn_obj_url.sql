-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_dyn_obj_url;

DELIMITER ;;
CREATE PROCEDURE `update_dyn_obj_url`( IN listUrl VARCHAR(255), IN objId INT(11) )
BEGIN
-- update the url on a dynamic object 
--
-- params:
--      listUrl -- new url
--      objId -- dyn object id
	DECLARE playersInZone INT DEFAULT 0;
	DECLARE oldUrl VARCHAR(255) DEFAULT NULL;
	DECLARE swfName VARCHAR(1000) DEFAULT 'KGPPlaylist.swf';
	
	
	SELECT value INTO oldUrl
	  FROM `dynamic_object_parameters`
     WHERE `obj_placement_id` = objId
       AND param_type_id = 18
	   FOR UPDATE;
	
	IF oldUrl IS NOT NULL THEN 

		IF listUrl = '' THEN
			DELETE FROM dynamic_object_parameters
		     WHERE `obj_placement_id` = objId
		       AND param_type_id = 17; -- swf name
		ELSE		       
			INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
			VALUES (objId, 18, listUrl) ON DUPLICATE KEY UPDATE value = listUrl;
		END IF;
		
		IF oldUrl <> listUrl THEN 
			INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
			VALUES (objId, 17, swfName) ON DUPLICATE KEY UPDATE value = swfName;
		END IF;

		select 0 as reasonCode; -- return something only on success
	END IF;
		
END
;;
DELIMITER ;