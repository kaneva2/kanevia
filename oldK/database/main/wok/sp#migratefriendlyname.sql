-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS migratefriendlyname;

DELIMITER ;;
CREATE PROCEDURE `migratefriendlyname`()
BEGIN
   DECLARE friendlyUserName VARCHAR(64) DEFAULT NULL;
   DECLARE friendlyZoneName VARCHAR(64) DEFAULT NULL;
   DECLARE friendlyName VARCHAR(128) DEFAULT NULL;
	DECLARE zoneType INT(11);
	DECLARE zoneIndex INT(11);
	DECLARE instanceId INT(11);
	DECLARE done INT DEFAULT 0;
	DECLARE zones CURSOR FOR SELECT zone_instance_id, zone_type, zone_index
	  FROM channel_zones
	 WHERE zone_type=3 OR zone_type=6;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
   OPEN zones;
   REPEAT
      FETCH zones INTO instanceId, zoneType, zoneIndex;
      IF NOT done THEN
         IF zoneType = 3 THEN
            SELECT gu.username INTO friendlyUserName
              FROM channel_zones cz
              INNER JOIN players p ON p.player_id=cz.zone_instance_id AND cz.zone_type=zoneType
              INNER JOIN game_users gu ON p.user_id=gu.user_id
              WHERE cz.zone_instance_id=instanceId;
            SELECT sw.display_name INTO friendlyZoneName
               FROM supported_worlds sw
               INNER JOIN channel_zones cz ON sw.zone_index=zoneIndex(zoneIndex) AND cz.zone_type=zoneType AND cz.zone_instance_id=instanceId;
            IF friendlyUserName IS NOT NULL AND friendlyZoneName IS NOT NULL THEN
               SET friendlyName = concat(friendlyUserName, ' - ', friendlyZoneName);
               UPDATE channel_zones SET name=friendlyName WHERE zone_instance_id=instanceId AND zone_type=zoneType;
            END IF;
         ELSEIF zoneType = 6 THEN
             SELECT c.name INTO friendlyUserName
               FROM channel_zones cz
               INNER JOIN kaneva.communities c ON c.community_id=cz.zone_instance_id AND cz.zone_type=zoneType
               WHERE cz.zone_instance_id=instanceId;
             SELECT sw.display_name INTO friendlyZoneName
                FROM supported_worlds sw
                INNER JOIN channel_zones cz ON sw.zone_index=zoneIndex(zoneIndex) AND cz.zone_type=zoneType AND cz.zone_instance_id=instanceId;
             IF friendlyUserName IS NOT NULL AND friendlyZoneName IS NOT NULL THEN
                SET friendlyName = concat(friendlyUserName, ' - ', friendlyZoneName);
                UPDATE channel_zones SET name=friendlyName WHERE zone_instance_id=instanceId AND zone_type=zoneType;
             END IF;
         END IF;
         SET done = 0;
      END IF;
   UNTIL done END REPEAT;
   CLOSE zones;
END
;;
DELIMITER ;