-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS vw_users;

DELIMITER ;;

 CREATE VIEW `vw_users` AS SELECT `gu`.`user_id` AS `user_id`,`gu`.`kaneva_user_id` AS `kaneva_user_id`,`gu`.`username` AS `username`,`gu`.`is_gm` AS `is_gm`,`gu`.`can_spawn` AS `can_spawn`,`gu`.`is_admin` AS `is_admin`,`gu`.`time_played` AS `time_played`,`gu`.`last_logon` AS `last_logon`,`gu`.`server_id` AS `server_id`,`gu`.`last_update_time` AS `last_update_time`,`gu`.`logged_on` AS `logged_on`,`gu`.`last_ip` AS `last_ip`,`u`.`over_21` AS `over_21`,`u`.`age` AS `age`,`u`.`show_mature` AS `show_mature` 
 FROM ( `wok`.`game_users` `gu` 
 JOIN `kaneva`.`users` `u` on( ( `gu`.`kaneva_user_id` = `u`.`user_id`) ) ) 
;;
DELIMITER ;