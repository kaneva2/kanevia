-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_user_balance;

DELIMITER ;;
CREATE PROCEDURE `get_user_balance`( IN __kaneva_id INT(11), IN __point_id INT(11) )
BEGIN
-- part of schema abstration to remove all Kaneva references from C++ 
-- and move them to SPs, which can be site-specific
	SELECT `balance` 
	  FROM  `vw_user_balances` 
	 WHERE user_id =  __kaneva_id
	   AND `kei_point_id` =  __point_id;  

END
;;
DELIMITER ;