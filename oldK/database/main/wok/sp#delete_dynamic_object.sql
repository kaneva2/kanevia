-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_dynamic_object;

DELIMITER ;;
CREATE PROCEDURE `delete_dynamic_object`( 
  IN __zone_index INT(11) ,
  IN __instance_id INT(11),
  IN __dyn_obj_id INT(11),
  IN __kaneva_user_id INT(11),
  OUT __glid INT(11),
  OUT __inv_sub_type INT(11),
  OUT __expired_date DATETIME,
  OUT __new_playlist_dyn_obj_id INT(11),
  OUT __new_playlist_id INT(11)
)
    COMMENT '\n      __zone_index \n      __instance_id\n      __dyn_obj_id\n      __kaneva_user_id\n    '
this_proc:
BEGIN
	DECLARE __zone_type    INT(11) DEFAULT NULL;
	DECLARE __owner_user_id INT(11) DEFAULT NULL;
	DECLARE __owner_player_id INT(11) DEFAULT NULL;
	DECLARE __placed_player_id INT(11) DEFAULT NULL;

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
      -- ERROR
      GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
      CALL logging.logMsg('ERROR', SCHEMA(), 'delete_dynamic_object', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
      RESIGNAL;
  END;


  CALL logging.logMsg('TRACE', SCHEMA(), 'delete_dynamic_object', CONCAT('BEGIN (__zone_index[',CAST(__zone_index AS CHAR), '] __instance_id[',CAST(__instance_id AS CHAR), '] __dyn_obj_id[',CAST(__dyn_obj_id AS CHAR), '] __kaneva_user_id[',CAST(__kaneva_user_id AS CHAR),'])'));


	SET __new_playlist_dyn_obj_id = 0;
	SET __new_playlist_id = 0;

	-- WOK specific feature so all permanent zones and Arenas share customizations based of instance 1
	SET __zone_type = zoneType(__zone_index);
	IF __zone_type = 4 OR __zone_type = 5 THEN
		SET __instance_id = 1;
	END IF;

	IF __kaneva_user_id <> 0 THEN 

		-- Get zone owner info.
		SET __owner_user_id = get_zone_owner(__zone_index, __instance_id);
		SELECT wok_player_id INTO __owner_player_id FROM kaneva.users WHERE user_id = __owner_user_id;

		-- the item will be put back into the zone owners inventory
		-- C++ needs glid, invSubType returned, so get that first
		SELECT `global_id`, `inventory_sub_type`, `expired_date`, `player_id` INTO __glid, __inv_sub_type, __expired_date, __placed_player_id
		  FROM `dynamic_objects`
		  WHERE `zone_index` = __zone_index
		    AND `zone_instance_id` = __instance_id
		    AND `obj_placement_id` = __dyn_obj_id;
	END IF;

	IF __glid IS NOT NULL THEN 
		IF __expired_date IS NULL THEN
			IF (__inv_sub_type <> 1024 AND __placed_player_id = __owner_player_id) THEN
				-- add 1 item back into inventory  (inventory_type 'P')
				INSERT INTO wok.inventory_pending_adds2 (`kaneva_user_id`, `global_id`, `inventory_type`, `inventory_sub_type`, `quantity`, `last_touch_datetime`) 
				VALUES (__owner_user_id, __glid, 'P', __inv_sub_type, 1, NOW())
				ON DUPLICATE KEY UPDATE `quantity` = `quantity` + 1, `last_touch_datetime` = NOW();
			END IF;
		END IF;

		DELETE FROM dynamic_objects 
		 WHERE `zone_index` = __zone_index
		   AND `zone_instance_id` = __instance_id
		   AND `obj_placement_id` = __dyn_obj_id;

		DELETE FROM dynamic_object_parameters
		 WHERE `obj_placement_id` = __dyn_obj_id;

		DELETE FROM dynamic_object_playlists
		WHERE `zone_index` = __zone_index
		  AND `zone_instance_id` = __instance_id
		  AND `obj_placement_id` = __dyn_obj_id;
		
	ELSE
		SET __glid = 0;
		SET __inv_sub_type = 0;
	END IF;


  CALL logging.logMsg('TRACE', SCHEMA(), 'delete_dynamic_object', CONCAT('END (__dyn_obj_id[',CAST(__dyn_obj_id AS CHAR),'] __glid[',CAST(__glid AS CHAR),'] __inv_sub_type[',CAST(__inv_sub_type AS CHAR),'])'));


END
;;
DELIMITER ;