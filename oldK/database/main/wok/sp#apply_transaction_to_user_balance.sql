-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS apply_transaction_to_user_balance;

DELIMITER ;;
CREATE PROCEDURE `apply_transaction_to_user_balance`( in user_id int, in transaction_amount float, 
							in transaction_type smallint unsigned, 
							in v_kei_point_id enum('GPOINT','KPOINT','MPOINT','DOLLAR','BANK_GPOINTS','BANK_KPOINTS'), OUT return_code INT, OUT user_balance float, OUT tranId INT )
BEGIN
-- The following is a stored procedure that applys a transaction to a user balance
-- Return code can be one of three values
-- 1 indicates the transaction is invalid due to insufficient funds
-- 0 indicates the transaction was valid and recorded
-- -1 indicates the transaction was invalid due to an error
	-- wrapper around kaneva's SP for STAR insulation
	CALL kaneva.apply_transaction_to_user_balance( user_id, transaction_amount, 
							transaction_type, 
							v_kei_point_id, return_code, user_balance, tranId );
END
;;
DELIMITER ;