-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_user_balances;

DELIMITER ;;
CREATE PROCEDURE `ss_update_user_balances`(  )
begin
    replace into wok.snapshot_user_balances
    select sum(balance), record_date, kei_point_id
    from wok.summary_money_supply_by_user_day
    where record_date=date(now())
    group by record_date,kei_point_id;
end
;;
DELIMITER ;