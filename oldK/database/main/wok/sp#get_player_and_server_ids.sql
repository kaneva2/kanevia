-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_player_and_server_ids;

DELIMITER ;;
CREATE PROCEDURE `get_player_and_server_ids`( IN __player_list text )
BEGIN
	-- called from dispatcher server only to get a list of servers 
	-- based on player names
	-- this used to his the wok.players table, but with GStar, they 
	-- may be on other servers

	IF right(__player_list,1)=',' THEN 
		-- trim trailing commas since a bug in C++ passes them in some times
		REPEAT
			SET __player_list = left(__player_list,length(__player_list)-1);
		UNTIL right(__player_list,1) <> ','	END REPEAT;
	END IF;

	IF LENGTH(__player_list) > 0 THEN 
		-- using a regular DECLAREd variable, MySQL didn't like, this it likes	
		SET @__sql := CONCAT( "SELECT p.player_id, al.server_id
								  FROM players p inner join developer.active_logins al on p.kaneva_user_id = al.user_id
								 WHERE p.name IN (",__player_list,")");
		-- test SET @__sql := 'select * from players';        				 	
		PREPARE __stmt FROM @__sql;
		EXECUTE __stmt;
		DEALLOCATE PREPARE __stmt;
					
	END IF;
END
;;
DELIMITER ;