-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_community_account_type;

DELIMITER ;;
CREATE PROCEDURE `get_community_account_type`( IN __user_id INT(11), IN __community_id INT(11) )
BEGIN
-- part of schema abstration to remove all Kaneva references from C++ 
-- and move them to SPs, which can be site-specific
		SELECT account_type_id 
		 FROM kaneva.community_members 
		 WHERE  user_id = __user_id
		 AND account_type_id  in (1,2) 	-- OWNER_OR_MODERATOR 
		 AND status_id = 1 				-- ACTIVE_MEMBER
		 AND community_id = __community_id;
END
;;
DELIMITER ;