-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS isUserBlocked;

DELIMITER ;;
CREATE PROCEDURE `isUserBlocked`( IN sourceKanevaId INT, IN destKanevaId INT, IN instanceId INT, IN blockType INT, IN userName VARCHAR(80) )
BEGIN
	DECLARE i TINYINT;
	select isUserBlocked( sourceKanevaId , destKanevaId , instanceId , blockType , userName  ) INTO i; 
	if i <> 0 THEN 
		select 1;
	END IF;
END
;;
DELIMITER ;