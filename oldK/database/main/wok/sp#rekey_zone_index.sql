-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS rekey_zone_index;

DELIMITER ;;
CREATE PROCEDURE `rekey_zone_index`(
IN __playerId           INT,
IN __fromZoneIndex      INT,
IN __fromZoneInstanceId INT,
IN __toZoneIndex        INT,
OUT __retval            INT
)
    COMMENT '\n    Purpose: Handles rekey on zone_index for all tables \n             that have an associated zone_instance_id\n    Special: custom_deed_usage \n          has a db relationship on zone_index and updates are not possible\n          for this case we perform a delete/insert\n\n    -- wok.player_presences\n    -- wok.custom_deed_usage\n    -- wok.channel_zones\n    -- wok.permanent_zone_ids\n    -- wok.channel_zone_cover_charges\n    -- wok.world_object_player_settings\n    -- wok.dynamic_objects\n    -- wok.dynamic_object_playlists\n    -- wok.occupancy_limits\n\n    Return codes\n    retval\n      = -1 (failure) -- \n      =  0 (success) -- \n  '
this_proc:
BEGIN


  -- working variables
  DECLARE __fromZoneType int DEFAULT -1;
  DECLARE __toZoneIndexPlain int DEFAULT -1;
  DECLARE __apartment_template_id int DEFAULT -1;

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- ERROR
      GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
      CALL logging.logMsg('ERROR', SCHEMA(), 'rekey_zone_index', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
      SET __retval = -1;
  END;


  CALL logging.logMsg('TRACE', SCHEMA(), 'rekey_zone_index', CONCAT('BEGIN (__playerId[',CAST(__playerId AS CHAR),'] __fromZoneIndex[',CAST(__fromZoneIndex AS CHAR), '] __fromZoneInstanceId[',CAST(__fromZoneInstanceId AS CHAR), '] __toZoneIndex[',CAST(__toZoneIndex AS CHAR),'])'));

  SET __retval = -1; -- assume worst
  SET __fromZoneType     = wok.zoneType(__fromZoneIndex);
  SET __toZoneIndexPlain = wok.zoneIndex(__toZoneIndex);

  IF __fromZoneType = 3 THEN
    CALL logging.logMsg('TRACE', SCHEMA(), 'rekey_zone_index', 'update player_presences');
    UPDATE 
      `player_presences`
    SET 
      `housing_zone_index`       = __toZoneIndex,
      `housing_zone_index_plain` = __toZoneIndexPlain,
      `respawn_zone_index`       = __toZoneIndex
    WHERE 
      `player_id` = __playerId;
  END IF;

  CALL logging.logMsg('TRACE', SCHEMA(), 'rekey_zone_index', 'delete custom_deed_usage');
  -- keep track of this delete so we can reinsert a record at the end

  SELECT 
    `apartment_template_id` 
  FROM 
    `custom_deed_usage`
  WHERE 
    `zone_index`       = __fromZoneIndex AND 
    `zone_instance_id` = __fromZoneInstanceId
  INTO 
    __apartment_template_id;

  DELETE 
    FROM 
      `custom_deed_usage`
    WHERE 
      `zone_index`       = __fromZoneIndex AND 
      `zone_instance_id` = __fromZoneInstanceId;


  CALL logging.logMsg('TRACE', SCHEMA(), 'rekey_zone_index', 'update channel_zones');
  UPDATE 
    `channel_zones`
  SET 
    `zone_index`       = __toZoneIndex,
    `zone_index_plain` = __toZoneIndexPlain
  WHERE 
    `zone_index`       = __fromZoneIndex AND
    `zone_instance_id` = __fromZoneInstanceId;


  CALL logging.logMsg('TRACE', SCHEMA(), 'rekey_zone_index', 'update permanent_zone_ids');
  UPDATE 
    `permanent_zone_ids`
  SET 
    `zone_index` = __toZoneIndex
  WHERE 
    `zone_index`       = __fromZoneIndex AND
    `zone_instance_id` = __fromZoneInstanceId;


  CALL logging.logMsg('TRACE', SCHEMA(), 'rekey_zone_index', 'update channel_zone_cover_charges');
  UPDATE 
    `channel_zone_cover_charges`
  SET 
    `zone_index` = __toZoneIndex
  WHERE 
    `zone_index`       = __fromZoneIndex AND
    `zone_instance_id` = __fromZoneInstanceId;


  CALL logging.logMsg('TRACE', SCHEMA(), 'rekey_zone_index', 'update world_object_player_settings');
  UPDATE
    `world_object_player_settings`
  SET 
    `zone_index` = __toZoneIndex
  WHERE 
    `zone_index`       = __fromZoneIndex AND
    `zone_instance_id` = __fromZoneInstanceId;

  CALL logging.logMsg('TRACE', SCHEMA(), 'rekey_zone_index', 'update dynamic_objects');
  UPDATE 
    `dynamic_objects`
  SET 
    `zone_index` = __toZoneIndex
  WHERE 
    `zone_index`       = __fromZoneIndex AND
    `zone_instance_id` = __fromZoneInstanceId;


  CALL logging.logMsg('TRACE', SCHEMA(), 'rekey_zone_index', 'replace dynamic_object_playlists');
  REPLACE INTO `dynamic_object_playlists` (`zone_index`, `zone_instance_id`, `global_id`, `obj_placement_id` ,`asset_group_id`)
  SELECT  
    __toZoneIndex, `zone_instance_id`, `global_id`, `obj_placement_id` ,`asset_group_id`
    FROM 
      `dynamic_object_playlists` doPlayLists
    WHERE 
      doPlayLists.zone_index       = __fromZoneIndex AND
      doPlayLists.zone_instance_id = __fromZoneInstanceId;

  DELETE 
    FROM
      `dynamic_object_playlists`
    WHERE
      `zone_index`       = __fromZoneIndex AND
      `zone_instance_id` = __fromZoneInstanceId;


  CALL logging.logMsg('TRACE', SCHEMA(), 'rekey_zone_index', 'update occupancy_limits');
  UPDATE 
    `occupancy_limits`
  SET
    `zone_index` = __toZoneIndex
  WHERE
    `zone_index`       = __fromZoneIndex AND
    `zone_instance_id` = __fromZoneInstanceId;

  CALL logging.logMsg('TRACE', SCHEMA(), 'rekey_zone_index', 'update unified_world_ids');
  UPDATE unified_world_ids
  SET zone_index = __toZoneIndex,
	zone_index_plain = __toZoneIndexPlain
  WHERE zone_index       = __fromZoneIndex AND
	zone_instance_id = __fromZoneInstanceId;

  IF (__apartment_template_id IS NOT NULL AND __apartment_template_id != -1) THEN
    -- Rekey this table has a depency (so we reinsert record with new value)
    INSERT IGNORE INTO `custom_deed_usage` (`zone_index`, `zone_instance_id`, `apartment_template_id`) VALUES(__toZoneIndex, __fromZoneInstanceId, __apartment_template_id);
  END IF;

  -- succcess
  SET __retval = 0;
  CALL logging.logMsg('TRACE', SCHEMA(), 'rekey_zone_index', CONCAT('END (__playerId[',CAST(__playerId AS CHAR),'] __fromZoneIndex[',CAST(__fromZoneIndex AS CHAR), '] __fromZoneInstanceId[',CAST(__fromZoneInstanceId AS CHAR), '] __toZoneIndex[',CAST(__toZoneIndex AS CHAR),'])'));

END
;;
DELIMITER ;