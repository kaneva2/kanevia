-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS getZonePasses;

DELIMITER ;;
CREATE PROCEDURE `getZonePasses`( IN zi INT(11), IN instanceId INT(11) )
BEGIN
	IF zoneType( zi ) = 4 THEN -- perm
		SELECT pass_group_id
		  FROM wok.pass_group_perm_channels cz
		 WHERE cz.zone_index_plain = zoneIndex(zi);
	ELSE		 
		SELECT pass_group_id
		  FROM wok.pass_group_channel_zones cz
		 WHERE cz.zone_instance_id = instanceId AND cz.zone_type = zoneType(zi);
	END IF;
END
;;
DELIMITER ;