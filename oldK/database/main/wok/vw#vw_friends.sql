-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS vw_friends;

DELIMITER ;;

 CREATE VIEW `vw_friends` AS SELECT `kaneva`.`friends`.`user_id` AS `user_id`,`kaneva`.`friends`.`friend_id` AS `friend_id`,`kaneva`.`friends`.`glued_date` AS `glued_date` 
 FROM `kaneva`.`friends`
;;
DELIMITER ;