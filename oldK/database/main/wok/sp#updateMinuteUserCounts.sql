-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS updateMinuteUserCounts;

DELIMITER ;;
CREATE PROCEDURE `updateMinuteUserCounts`(in start_date datetime, in end_date datetime)
BEGIN
-- This procedure takes all of the users by server by minute summary information and builds hourly summary information about users per hour within a specified datetime frame.
-- This procedure would have a logic error based off of searching for particular hours within the datetime of start_date and end_date due to cur_date being assumed to be at 00:00:00.
-- This is repaired with the concat statement in the criteria that allows the actual hour and minute to be used in determining what to search for.  
SELECT NOW() INTO @start_time;

Replace INTO summary_users_by_server_by_minute
	Select DATE(NOW()) as this_date, HOUR(NOW()) as this_hour,
			MINUTE(NOW()) as minute, gs.number_of_players, gs.server_id
	from developer.game_servers gs 
	Inner Join developer.game_licenses gl on gs.game_id = gl.game_id
	WHERE gl.game_id = environment.getWOKGameId();

Replace into summary_users_by_minute 
	Select cur_date,cur_hour,cur_minute, sum(logged_on_players) 
	from summary_users_by_server_by_minute sm 
	where concat(cur_date,' ',IF(cur_hour<10,CONCAT('0',cur_hour),cur_hour),':',if(cur_minute<10,concat('0',cur_minute),cur_minute),':00') 
	between start_date and end_date 
	group by sm.cur_date, sm.cur_hour, sm.cur_minute;

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('updateMinuteUserCounts',@start_time,now(),TIME_TO_SEC(TIMEDIFF(now(),@start_time)));
	
END
;;
DELIMITER ;