-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_blocked_users;

DELIMITER ;;
CREATE PROCEDURE `get_blocked_users`( IN __blocked_kaneva_id INT(11), IN __zone_type INT(11), IN __comm_blocked INT )
BEGIN
-- part of schema abstration to remove all Kaneva references from C++ 
-- and move them to SPs, which can be site-specific
	SELECT user_id 
	  FROM kaneva.blocked_users
	 WHERE blocked_user_id = __blocked_kaneva_id;
END
;;
DELIMITER ;