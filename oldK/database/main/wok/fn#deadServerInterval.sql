-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS deadServerInterval;

DELIMITER ;;
CREATE FUNCTION `deadServerInterval`() RETURNS int(11)
    DETERMINISTIC
BEGIN
-- number of minutes a server doesn't ping in to be considered dead
-- this is a fn for easy changing
	return 6; 
END
;;
DELIMITER ;