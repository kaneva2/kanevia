-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS player_skills_view;

DELIMITER ;;

 CREATE VIEW `player_skills_view` AS SELECT `player_skills`.`player_id` AS `player_id`,`skills`.`skill_id` AS `skill_id`,`skills`.`name` AS `name`,`skills`.`type` AS `type`,`skills`.`gain_increment` AS `gain_increment`,`skills`.`basic_ability_skill` AS `basic_ability_skill`,`skills`.`basic_ability_exp_mastered` AS `basic_ability_exp_mastered`,`skills`.`basic_ability_function` AS `basic_ability_function`,`skills`.`skill_gains_when_failed` AS `skill_gains_when_failed`,`skills`.`max_multiplier` AS `max_multiplier`,`skills`.`use_internal_timer` AS `use_internal_timer`,`skills`.`filter_eval_level` AS `filter_eval_level`,`player_skills`.`current_level` AS `current_level`,`player_skills`.`current_experience` AS `current_experience` 
 FROM ( `skills` 
 JOIN `player_skills`)  
 WHERE ( `skills`.`skill_id` = `player_skills`.`skill_id`) 
;;
DELIMITER ;