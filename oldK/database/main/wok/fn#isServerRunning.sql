-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS isServerRunning;

DELIMITER ;;
CREATE FUNCTION `isServerRunning`( serverId INT(11) ) RETURNS tinyint(1)
    READS SQL DATA
BEGIN
	DECLARE isValid INT DEFAULT 0;
	IF serverId  <> 0 THEN -- serverId 0 never running
		SELECT COUNT(*) INTO isValid
		  FROM vw_game_servers gs
		 WHERE gs.server_status_id = 1
		   AND timestampdiff (minute, last_ping_datetime, NOW()) < 7 -- 7 minute limit used by Kaneva
		   AND gs.server_id = serverId;
		IF isValid = 0 THEN
			CALL logDebugMsg( 'isServerRunning', concat( 'serverId ', cast(serverId as char), ' is not running!' ) );
		END IF;
	END IF;
	
	RETURN isValid > 0;
END
;;
DELIMITER ;