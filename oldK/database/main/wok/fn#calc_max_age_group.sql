-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS calc_max_age_group;

DELIMITER ;;
CREATE FUNCTION `calc_max_age_group`( __age INT ) RETURNS int(11)
    DETERMINISTIC
BEGIN
-- return the max_age col value for a given players' age
	IF __age < 18 THEN
		RETURN 18;
	ELSE
		RETURN 100;
	END IF;
END
;;
DELIMITER ;