-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_load_balanced_server_for_player;

DELIMITER ;;
CREATE PROCEDURE `get_load_balanced_server_for_player`( IN       __zone_index         INT(11)
                                                      , IN     __kaneva_id          INT(11)
                                                      , IN     __instance_id        INT(11)
                                                      , INOUT  __cur_server_id      VARCHAR(64)
                                                      , OUT    __script_server_id   INT(11)
                                                      , OUT    __switched           BOOL )
BEGIN

	DECLARE currentServerId INT(11) DEFAULT NULL;
   DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;

   CALL logging.logMsg( 'TRACE'
                        , SCHEMA()
                        , 'get_load_balanced_server_for_player'
                        , concat( 'begin( zi=', __zone_index, ';kuid=' , __kaneva_id, ';iid=' , __instance_id, ')' ) );

   select tiedServerId(__instance_id) into __cur_server_id;

	IF __cur_server_id = 0 THEN -- logging in
		SELECT server_id INTO currentServerId
		  FROM game_users
	 	  WHERE kaneva_user_id = __kaneva_id
	 	    AND logged_on = 'T';

		IF currentServerId IS NOT NULL  THEN
			SET __cur_server_id = currentServerId;
		END IF;

      CALL get_load_balanced_server( __zone_index, __instance_id, __cur_server_id, __script_server_id, __switched );
   ELSE
      CALL logging.logMsg( 'TRACE'
                           , SCHEMA()
                           , 'get_load_balanced_server_for_player'
                           , concat( 'Zone with instance_id [', __instance_id, '] is PERMANENTLY_TIED to server [', __cur_server_id, ']' ) );

	END IF;

END
;;
DELIMITER ;