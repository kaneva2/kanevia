-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_item_active_state;

DELIMITER ;;
CREATE PROCEDURE `update_item_active_state`(globalId INT, itemActiveState INT)
BEGIN
 
  START TRANSACTION;
  
    
    /* update the records */
    UPDATE shopping.items_web
	  SET item_active = itemActiveState
    WHERE global_id = globalId;
   
    /*return the number of records affected*/
    SELECT ROW_COUNT();

  COMMIT;
END
;;
DELIMITER ;