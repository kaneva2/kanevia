-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS z_online_players;

DELIMITER ;;

 CREATE VIEW `z_online_players` AS SELECT `p`.`name` AS `name`,`gu`.`logged_on` AS `logged_on`,`p`.`in_game` AS `in_game`,concat( `cge`.`ip_address`,_latin1':',cast( `cge`.`port` as char 
 CHARSET latin1) )  AS `server`,`pz`.`current_zone` AS `current_zone`,concat( _utf8'0x',convert( hex( `pz`.`current_zone_index`)  using utf8) )  AS `zone_index`,`pz`.`current_zone_instance_id` AS `current_zone_instance_id`,`p`.`player_id` AS `player_id` 
 FROM ( ( ( `wok`.`players` `p` 
 JOIN `wok`.`game_users` `gu` on( ( ( `p`.`user_id` = `gu`.`user_id`)  
 AND ( `gu`.`logged_on` = _latin1'T') ) ) )  
 LEFT JOIN `wok`.`player_zones` `pz` on( ( `p`.`player_id` = `pz`.`player_id`) ) )  
 JOIN `developer`.`game_servers` `cge` on( ( ( `pz`.`server_id` = `cge`.`server_id`)  
 AND ( `cge`.`game_id` = `environment`.`getWOKGameId`( ) ) ) ) ) 
;;
DELIMITER ;