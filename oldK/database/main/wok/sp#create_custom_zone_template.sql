-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS create_custom_zone_template;

DELIMITER ;;
CREATE PROCEDURE `create_custom_zone_template`( IN __zone_index INT, IN __zone_instance_id INT, IN __template_global_id INT, IN __is_admin BOOLEAN )
BEGIN
	DECLARE __done, __obj_placement_id, __game_item_id, __last_sdo_id, __global_id INT;

	DECLARE dyo_cursor CURSOR FOR
		SELECT obj_placement_id, do.global_id
		FROM dynamic_objects do
		INNER JOIN shopping.items_web iw ON iw.global_id = do.global_id
		WHERE zone_instance_id = __zone_instance_id 
			AND zone_index = __zone_index 
			AND (iw.item_active = 1 OR (__is_admin AND iw.global_id IN (SELECT global_id FROM script_available_items_plain)));

	DECLARE gi_cursor CURSOR FOR
		SELECT game_item_id
		FROM script_game_items
		WHERE zone_instance_id = __zone_instance_id 
			AND zone_type = zoneType(__zone_index)
			AND game_item_glid IS NOT NULL;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1;

	-- Reset existing dos and wops
	UPDATE starting_dynamic_objects SET active = 0 WHERE zone_index = __zone_index AND apartment_template_id = __template_global_id; 
	UPDATE starting_world_object_player_settings SET active = 0 WHERE zone_index = __zone_index AND apartment_template_id = __template_global_id; 

	-- Process game items. Temporarily only if the user is a GM for step 1 of the templating process.
	IF __is_admin THEN
		SET __done = 0;
		OPEN gi_cursor;
		REPEAT
			FETCH gi_cursor INTO __game_item_id;
			IF NOT __done THEN
				START TRANSACTION;

					INSERT INTO `wok`.`starting_script_game_items` (`template_glid`, `game_item_id`, `game_item_glid`)
					SELECT __template_global_id, game_item_id, game_item_glid
					FROM script_game_items 
					WHERE zone_instance_id = __zone_instance_id AND zone_type = zoneType(__zone_index) AND game_item_id = __game_item_id;

				COMMIT;
			END IF;
		UNTIL __done END REPEAT;
		CLOSE gi_cursor;

		-- Process script game custom data
		INSERT INTO `wok`.`starting_script_game_custom_data` (`template_glid`, `attribute`, `value`)
		SELECT __template_global_id, attribute, value
		FROM script_game_custom_data
		WHERE zone_instance_id = __zone_instance_id AND zone_type = zoneType(__zone_index);

	END IF;

	-- Process dynamic objects and their parameters
	SET __done = 0;
	OPEN dyo_cursor;
	REPEAT
		FETCH dyo_cursor INTO __obj_placement_id, __global_id;
		IF NOT __done THEN
			START TRANSACTION;

				INSERT INTO starting_dynamic_objects ( `active`, `zone_index`, `object_id`,`global_id`,`position_x`,`position_y`,`position_z`,
				`rotation_x`,`rotation_y`,`rotation_z`, `apartment_template_id`)
				SELECT 1, __zone_index, `object_id`, `global_id`,`position_x`,`position_y`,`position_z`,
				`rotation_x`,`rotation_y`,`rotation_z`,	__template_global_id
				FROM dynamic_objects WHERE obj_placement_id = __obj_placement_id;

				SELECT LAST_INSERT_ID() INTO __last_sdo_id;

				-- Insert non-glid params
				INSERT INTO starting_dynamic_object_parameters (`id`, `param_type_id`, `value`)
				SELECT __last_sdo_id, `param_type_id`, `value`
				FROM dynamic_object_parameters dop 
				WHERE obj_placement_id = __obj_placement_id
					AND param_type_id NOT IN (32, 206);

				-- Insert glid params
				INSERT INTO starting_dynamic_object_parameters (`id`, `param_type_id`, `value`)
				SELECT __last_sdo_id, `param_type_id`, `value`
				FROM dynamic_object_parameters dop 
				INNER JOIN shopping.items_web iw ON dop.value = iw.global_id
					AND iw.item_active = 1
				WHERE obj_placement_id = __obj_placement_id
					AND param_type_id IN (32, 206);

				IF __is_admin THEN

					INSERT INTO starting_dynamic_object_playlists (id, global_id, asset_group_id)
					SELECT __last_sdo_id, global_id, asset_group_id
					FROM dynamic_object_playlists dopl
					WHERE zone_instance_id = __zone_instance_id 
						AND zone_index = __zone_index 
						AND global_id = __global_id
						AND obj_placement_id = __obj_placement_id;

					-- Update objectAssociations customdata
					UPDATE wok.starting_script_game_custom_data
					SET value = REPLACE(
									REPLACE(
										REPLACE(
											REPLACE(
												REPLACE(value, ' ', ''), CONCAT('[', __obj_placement_id, ']'), CONCAT('[', __last_sdo_id, ']')
											), CONCAT('[', __obj_placement_id, ','), CONCAT('[', __last_sdo_id, ',')
										), CONCAT(',', __obj_placement_id, ','), CONCAT(',', __last_sdo_id, ',')
									), CONCAT(',', __obj_placement_id, ']'), CONCAT(',', __last_sdo_id, ']')
								)
					WHERE template_glid = __template_global_id 
						AND attribute LIKE 'objectAssociations%';

				END IF;

			COMMIT;
		END IF;
	UNTIL __done END REPEAT;
	CLOSE dyo_cursor;

	-- Process world object player settings
	INSERT INTO starting_world_object_player_settings (  `active`, `zone_index`, `world_object_id`, `asset_id`, `texture_url`, `apartment_template_id` )
	SELECT 1,  __zone_index,  `world_object_id`, `asset_id`, `texture_url`, __template_global_id
	FROM world_object_player_settings WHERE zone_index = __zone_index AND zone_instance_id = __zone_instance_id;
	  					
END
;;
DELIMITER ;