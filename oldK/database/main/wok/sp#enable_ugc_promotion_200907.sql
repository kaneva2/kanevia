-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS enable_ugc_promotion_200907;

DELIMITER ;;
CREATE PROCEDURE `enable_ugc_promotion_200907`()
BEGIN

	
	
	

	
	INSERT INTO kaneva.web_options(option_group, option_key, role, option_value, option_desc) 
		SELECT 'UGC200907', option_key, role, option_value, option_desc FROM kaneva.web_options 
		WHERE option_group='UGC' AND option_key in ('UploadFeeMax', 'AutoPurchasedItemInvType', 'AutoPurchaseUploadedItem') AND role=0;

	
	UPDATE kaneva.web_options 
		SET option_value = '400' 
		WHERE option_group='UGC' AND option_key='UploadFeeMax' AND role=0;
	
	
	UPDATE kaneva.web_options 
		SET option_value = '512' 
		WHERE option_group='UGC' AND option_key='AutoPurchasedItemInvType' AND role=0;

	
	UPDATE kaneva.web_options 
		SET option_value = 'Y' 
		WHERE option_group='UGC' AND option_key='AutoPurchaseUploadedItem' AND role=0;

	
	
	

	
	INSERT IGNORE INTO wok.ugc_price_tiers ( kaneva_role_value, ugc_usage_id, tier_id, tier_type, base_price, max_price )
		SELECT kaneva_role_value, 200907, tier_id, tier_type, base_price, max_price FROM wok.ugc_price_tiers
		WHERE kaneva_role_value=2 AND ugc_usage_id=101 AND tier_id in (1120, 1121, 1122, 1123);
	
	
	UPDATE wok.ugc_price_tiers
		SET base_price = 200, max_price = 200
		WHERE kaneva_role_value=2 AND ugc_usage_id=101 AND tier_id in (1120, 1121, 1122, 1123);

END
;;
DELIMITER ;