-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS removeDupStartingInventory;

DELIMITER ;;
CREATE PROCEDURE `removeDupStartingInventory`()
BEGIN
DECLARE done INT DEFAULT 0;
DECLARE _glid  INT;
DECLARE _cnt INT;
DECLARE _active TINYINT;
DECLARE _orig_edb INT;

DECLARE dupCursor CURSOR FOR
	SELECT global_id, original_edb, active , count(id) 
		FROM wok.starting_inventories
		where active = 1
		group by global_id, original_edb, active
		having count(id) > 1;

DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN SET done = 1; END;

CREATE TABLE IF NOT EXISTS _temp_dup_si (id INT, glid INT, orig_edb INT, active TINYINT);

OPEN dupCursor;

REPEAT
	FETCH dupCursor into _glid, _orig_edb, _active, _cnt;
	IF NOT done THEN
		INSERT INTO _temp_dup_si (id, glid, orig_edb, active)
			SELECT id, global_id, original_edb,active 
			FROM wok.starting_inventories
			WHERE global_id = _glid AND original_edb = _orig_edb AND active = _active AND id NOT IN 
			(SELECT MIN(id) FROM starting_inventories WHERE global_id = _glid AND original_edb = _orig_edb AND active = _active);
	END IF;

UNTIL done END REPEAT;

CLOSE dupCursor;


SELECT  'these will be deleted, please keep this list for future reference: ', id, orig_edb, glid, active, now()
	FROM _temp_dup_si; 

DELETE FROM starting_inventories WHERE id IN (SELECT id FROM _temp_dup_si);

DROP TABLE IF EXISTS _temp_dup_si;

END
;;
DELIMITER ;