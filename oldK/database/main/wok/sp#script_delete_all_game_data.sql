-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS script_delete_all_game_data;

DELIMITER ;;
CREATE PROCEDURE `script_delete_all_game_data`( IN __game_id INT(11) )
BEGIN
-- called to delete game data for a given game
  DELETE FROM script_game_data
    WHERE game_id = __game_id;

END
;;
DELIMITER ;