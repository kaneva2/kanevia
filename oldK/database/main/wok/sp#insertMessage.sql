-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS insertMessage;

DELIMITER ;;
CREATE PROCEDURE `insertMessage`( IN _fromId INT, IN _fromIdStatus INT, IN _toId INT, IN _toIdStatus INT, 
	IN _subject VARCHAR(100), IN _message TEXT, IN _replied INT, IN _messageType INT, IN _channelId INT,
	OUT _messageId INT )
BEGIN
-- SP to insert a message into its tables
    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
	SET _messageId = 0;
	IF NOT wok.isUserBlocked(_fromId, _toId, 0, 1, NULL ) THEN 
	
	
	-- Send the message
	CALL kaneva.add_messages(_fromId, _toId, _subject, _message, _replied, _messageType, NULL, _messageId);
	-- can't send email if configured, like ASPX can
		
		
	END IF;
END
;;
DELIMITER ;