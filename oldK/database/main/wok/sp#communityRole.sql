-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS communityRole;

DELIMITER ;;
CREATE PROCEDURE `communityRole`( IN kanevaUserId INT, IN communityId INT, OUT role INT)
BEGIN
	
	/* from C#
		public enum eCOMMUNITY_MEMBER_STATUS
		{
			ACTIVE = 1,
			PENDING = 2,
			LOCKED = 3,
			DELETED = 4,
			REJECTED = 5,
			PENDINGPAYMENT = 6
		}

		public enum eCOMMUNITY_ACCOUNT_TYPE
		{
			OWNER = 1,
			MODERATOR = 2,
			SUBSCRIBER = 3
		}
	*/
	DECLARE statusId INT DEFAULT 2;
    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
	
	SELECT account_type_id, status_id INTO role, statusId
	  FROM kaneva.community_members 
	 WHERE user_id = kanevaUserId 
	   AND community_id = communityId;
	IF statusId <> 1 THEN
		SET role = 3;
	END IF;
	  
END
;;
DELIMITER ;