-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS can_player_spawn_to_player_common;

DELIMITER ;;
CREATE PROCEDURE `can_player_spawn_to_player_common`( IN playerId INT, IN sourceKanevaId INT, IN destKanevaId INT, 
										   IN myServerId INT(11), 
										   IN isGM CHAR,
										   IN country CHAR(2),
										   IN birthDate DATE,
										   IN showMature BINARY(1),
										   OUT reasonCode INT(11), 
										   OUT cZoneIndex INT(11), OUT cZoneInstance INT(11), 
										   OUT serverId INT(11),
										   OUT scriptServerId INT(11),
										   OUT x float, OUT y float, OUT z float,
										   OUT playerName VARCHAR(80) )
BEGIN
-- called from C and ASPX indirectly from wrapper SPs
	DECLARE cZoneType smallint(6);
	DECLARE playersServerId INT(11);
	DECLARE ingame char;
	DECLARE isBlocked BOOL DEFAULT FALSE;
    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
	SET serverId = 0;
	
	-- CALL logDebugMsg( "can_player_spawn_to_player_common", concat( ' playerId ', cast( playerId as char ), ' sourceKanevaId ', cast( sourceKanevaId as char ), ' destKanevaId is ', cast( destKanevaId as char ) ) );
	
	SET isBlocked = (isGm = 'F') AND isUserBlocked( sourceKanevaId, destKanevaId, destKanevaId, 2, NULL ) ;
	IF isBlocked THEN
		-- CALL logDebugMsg( "can_player_spawn_to_player_common", concat( ' user ', cast( sourceKanevaId as char ), ' is blocked by ', cast( destKanevaId as char ) ) );
		
		SET reasonCode = 4;
	ELSE
		
		-- get player's position
		SELECT pz.`current_zone_index`, pz.`current_zone_instance_id`,
		       pz.`current_zone_type`, pp.`position_x`, pp.`position_y`, pp.`position_z`,
		       p.`in_game`, p.`server_id`
		  INTO czoneindex, czoneinstance, czonetype, x, y, z, ingame, playersServerId
		  FROM `players` p
		       INNER JOIN
		       `player_presences` pp ON p.player_id = pp.player_id
					 INNER JOIN
					 `player_zones` pz ON p.player_id = pz.player_id 
		 WHERE p.kaneva_user_id = destKanevaId and p.in_game = 'T'
		 LIMIT 1;
 
		IF cZoneIndex IS NOT NULL THEN
			IF ingame = 'T' THEN 
				SET reasonCode = 0; -- found, in game
			ELSE
				SET reasonCode = 1; -- found, not in game
			END IF;
		ELSE
			SET reasonCode = 2; -- not found
		END IF;
		IF reasonCode = 0 THEN
			CALL checkMaxOccupancy( playerId, sourceKanevaId, cZoneIndex, cZoneInstance, isGm, country, birthDate, TRUE, reasonCode );
			IF reasonCode = 0 THEN
				-- can they get to that zone
				-- used to ignore tied server, but that could cause problems, so always honor it
				CALL can_player_spawn_to_zone( sourceKanevaId, cZoneType, cZoneIndex, cZoneInstance, myServerId, isGm, country, birthDate, showMature, reasonCode, playersServerId, scriptServerId, playerName );
			END IF;
		END IF;
		
		-- CALL logDebugMsg( "can_player_spawn_to_player_common", concat( 'exiting with rc=', cast( reasonCode as char ), ' server is ', cast( serverId as char ), ' playerserver is ', cast( playersServerId as char ), ' myServerId is ', cast( myServerId as char )) );
	END IF;
	
	IF reasonCode <> 0 THEN
		SET cZoneIndex = 0;
		SET cZoneInstance = 0;
	ELSE
		IF playersServerId <> myServerId  THEN 
			SET serverId = playersServerId;
		END IF;
	END IF;
	
END
;;
DELIMITER ;