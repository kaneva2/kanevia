-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS angleConversion;

DELIMITER ;;
CREATE PROCEDURE `angleConversion`( zoneInstanceId INT)
    READS SQL DATA
BEGIN
		DECLARE placementId INT(11) DEFAULT NULL;
		DECLARE rotationX DOUBLE DEFAULT NULL;
		DECLARE rotationY DOUBLE DEFAULT NULL;
		DECLARE rotationZ DOUBLE DEFAULT NULL;
		DECLARE roundedRotationX DOUBLE DEFAULT NULL;
		DECLARE roundedRotationY DOUBLE DEFAULT NULL;
		DECLARE roundedRotationZ DOUBLE DEFAULT NULL;		
		DECLARE vecLen DOUBLE DEFAULT NULL;
		DECLARE exactDegree DOUBLE DEFAULT NULL;
		DECLARE roundedDegree DOUBLE DEFAULT NULL;
		DECLARE roundedRadians DOUBLE DEFAULT NULL;
		DECLARE done INT DEFAULT 0;
		DECLARE itsPI DOUBLE DEFAULT 0;
		
		DECLARE placements CURSOR FOR 
			SELECT dynamic_objects.obj_placement_id, dynamic_objects.rotation_x, dynamic_objects.rotation_y, dynamic_objects.rotation_z
			FROM dynamic_objects
	 		WHERE dynamic_objects.zone_instance_id = zoneInstanceId;
			
		DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
		SELECT PI() INTO itsPI;
-- Loop through all Do placements in that zone		
		OPEN placements;
		REPEAT
			FETCH placements INTO placementId, rotationX, rotationY, rotationZ;
			IF NOT done THEN
-- normalize input vector			
				SELECT SQRT((rotationX * rotationX) + (rotationY * rotationY) + (rotationZ * rotationZ)) INTO vecLen;
-- Prevent division by 0				
				IF vecLen <> 0 THEN
					SET rotationX = rotationX / vecLen;
					SET rotationY = rotationY / vecLen;
					SET rotationZ = rotationZ / vecLen;
			  END IF;
				
-- obtain exact degree measurement of currently stored angle in DB.
				SELECT ASIN(rotationZ) INTO exactDegree;
				SET exactDegree = exactDegree * (180 / itsPI);
				SELECT FLOOR(exactDegree / 5.0 + 0.5) INTO roundedDegree;
				
-- this is the rounded degree value.  Must be stored back into DB as a vector				
				SET roundedDegree = roundedDegree * 5.0;
				
				IF roundedDegree <> 0 THEN
					SET roundedRadians = roundedDegree * (itsPI / 180);
-- C++ trig functions don't work for neg angles going to abs and manually restore quadrant like the C++					
					SELECT ABS(roundedRadians) INTO roundedRadians;
					SELECT ABS(COS(roundedRadians)) INTO roundedRotationX;
					SELECT ABS(SIN(roundedRadians)) INTO roundedRotationZ;
					IF rotationX <= 0 THEN
						SET roundedRotationX = roundedRotationX * -1;
					END IF;
					IF rotationZ <= 0 THEN
					  SET roundedRotationZ = roundedRotationZ * -1;
					END IF;
				ELSE
-- special cases for right angles
					IF rotationZ < -.9 THEN
						SET roundedRotationX = 0;
						SET roundedRotationZ = -1.0;
					ELSEIF rotationX < -.9 THEN
						SET roundedRotationX = -1.0;
						SET roundedRotationZ = 0;
					ELSEIF rotationZ > .9 THEN
						SET roundedRotationZ = 1.0;
						SET roundedRotationX = 0;
					ELSEIF rotationX > .9 THEN
						SET roundedRotationX = 1.0;
						SET roundedRotationZ = 0;
					ELSE
-- This should never ever happen
						SET roundedRotationX = 1.0;
						SET roundedRotationZ = 0;
					END IF;
				END IF;
				SET roundedRotationY = 0;

-- ReNormalize the result and put it back into the database
-- normalize input vector			
				SELECT SQRT((roundedRotationX * roundedRotationX) + (roundedRotationY * roundedRotationY) + (roundedRotationZ * roundedRotationZ)) INTO vecLen;				
				IF vecLen <> 0 THEN
					SET roundedRotationX = roundedRotationX * (1.0 / vecLen);
					SET roundedRotationY = roundedRotationY * (1.0 / vecLen);
					SET roundedRotationZ = roundedRotationZ * (1.0 / vecLen);
			  END IF;
-- Store rounded Vals back into the DB
				UPDATE dynamic_objects 
				SET dynamic_objects.rotation_x = roundedRotationX, dynamic_objects.rotation_y = roundedRotationY, dynamic_objects.rotation_z = roundedRotationZ 	WHERE dynamic_objects.obj_placement_id = placementId;
				SET done = 0;
			END IF;
		UNTIL done END REPEAT;	
		CLOSE placements;
END
;;
DELIMITER ;