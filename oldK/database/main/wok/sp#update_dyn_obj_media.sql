-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_dyn_obj_media;

DELIMITER ;;
CREATE PROCEDURE `update_dyn_obj_media`( IN mediaType INT(11),
											IN url VARCHAR(1000),
										    IN parameters VARCHAR(1000), 
										    IN objId INT(11) )
BEGIN
-- update the media parameters for a dynamic object 
--
-- params:
-- 	    mediatype -- 
--      url -- new url
--      parameters -- optional parameters
--      objId -- dyn object id
	DECLARE oldParameters VARCHAR(255) DEFAULT NULL;
	DECLARE swfName VARCHAR(1000) DEFAULT 'KGPPlaylist.swf';

	-- TODO: do something with the new media type
	-- 
	SELECT value INTO oldParameters
	  FROM `dynamic_object_parameters`
     WHERE `obj_placement_id` = objId
       AND `param_type_id` = 18
	   FOR UPDATE;

	IF oldParameters IS NOT NULL THEN 

		UPDATE `dynamic_object_parmeters`
		   SET value = parameters
	     WHERE `obj_placement_id` = objId
	       AND param_type_id = 18;

		UPDATE `dynamic_object_parmeters`
		   SET value = url
	     WHERE `obj_placement_id` = objId
		   AND param_type_id = 17;

		select 0 as reasonCode; -- return something only on success
	END IF;
		
END
;;
DELIMITER ;