-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS vw_user_balances;

DELIMITER ;;

 CREATE VIEW `vw_user_balances` AS SELECT `kaneva`.`user_balances`.`user_id` AS `user_id`,`kaneva`.`user_balances`.`kei_point_id` AS `kei_point_id`,`kaneva`.`user_balances`.`balance` AS `balance` 
 FROM `kaneva`.`user_balances`
;;
DELIMITER ;