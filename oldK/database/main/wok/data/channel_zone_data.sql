-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use wok;
INSERT INTO `channel_zones` VALUES (1, 0, 0, 0, 0, 'Mall', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (2, 0, 16, 0, 0, 'Cafe', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (3, 0, 3, 0, 0, 'Theater', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (4, 0, 12, 0, 0, 'Compound', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (5, 0, 10, 0, 0, 'Penthouse', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (6, 0, 2, 0, 0, 'Casino', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (11, 0, 17, 0, 0, 'Channel - Art Gallery', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (12, 0, 1073741824, 4, 1, 'Mall (perm instance)', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (13, 0, 4, 0, 0, 'Apartment - One Bedroom', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (14, 0, 5, 0, 0, 'Apartment - Studio', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (15, 0, 6, 0, 0, 'Apartment - One Bedroom Medium', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (16, 0, 7, 0, 0, 'Apartment - Loft', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (17, 0, 8, 0, 0, 'Apartment - One Bedroom Large', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (18, 0, 9, 0, 0, 'Channel - Night Club', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (19, 0, 13, 0, 0, 'Channel - Conference Room', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (29, 0, 14, 0, 0, 'Channel - Theater', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (30, 0, 18, 0, 0, 'Channel - The Beach', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (36, 0, 15, 0, 0, 'Channel - Cafe', '', 0, 'F');
INSERT INTO `channel_zones` VALUES (38, 0, 11, 0, 0, 'Channel - Hangar', '', 0, 'F');

