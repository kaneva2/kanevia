-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_wok_user_average;

DELIMITER ;;
CREATE PROCEDURE `ss_update_wok_user_average`(  )
begin
  replace into wok.snapshot_wok_user_average
	select avg(gu.time_played) as play_avg,date(now())
	from wok.game_users gu;
end
;;
DELIMITER ;