-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS import_channel;

DELIMITER ;;
CREATE PROCEDURE `import_channel`(
  IN __importing_player_id	INT,
  IN __dstZoneIndex			INT,
  IN __dstZoneInstanceId	INT,
  IN __baseZoneIndexPlain	INT,
  IN __srcZoneIndex			INT,
  IN __srcZoneInstanceId	INT,
  IN __dstPreserveFlag		INT,
  OUT __retval				INT,
  OUT __newZoneIndex		INT
)
    COMMENT '\n   Arguments\n   __importing_player_id:            playerId\n   __dstZoneIndex:        Instanced form for the destination zoneIndex (comes from KEP_GetCurrentZoneIndex)\n   __dstZoneInstanceId:   For zoneType 3 will be playerId and zoneType 6 will be communityId\n   __baseZoneIndexPlain:  Defines the terrain that will be imported into Destination Zone\n   __srcZoneIndex:        source  zoneIndex (db format)\n   __srcZoneInstanceId    source  instance id\n   __dstPreserveFlag:     (0= true| 1= false) preserve scripted objects in destination zone\n\n\n    Validations & Return codes\n    retval\n      = -1 (failure) -- \n      =  0 (success) -- \n      =  2 (invalid destination zone type) -- this procedure only handles channel_zones of type 3(home 6(channel)\n      =  3 (user does not own destination) -- \n      =  4 (could not find destination in database) -- \n      =  5 (rekey zone index failed) -- \n  '
this_proc:
BEGIN


  -- working variables
  DECLARE __dstZoneType INT;
  DECLARE __dbZoneIndex INT;
  DECLARE __newZoneIndexPlain INT;

  DECLARE __world_owner_user_id INT DEFAULT 0;
  DECLARE __world_owner_player_id INT DEFAULT 0;
  DECLARE __count          INT DEFAULT 0;
  DECLARE __template_id    INT DEFAULT 0;
  DECLARE __rc int;
  DECLARE __player_name    VARCHAR(80);
  DECLARE __activating_framework BOOLEAN DEFAULT FALSE;
  DECLARE __deactivating_framework BOOLEAN DEFAULT FALSE;

  -- cursor variable declares
  DECLARE _dstObjPlacementId INT;
  DECLARE _srcObjPlacementId INT;
  DECLARE _object_id INT;
  DECLARE _global_id INT;
  DECLARE _x  float;
  DECLARE _y  float;
  DECLARE _z  float;
  DECLARE _rx float;
  DECLARE _ry float;
  DECLARE _rz float;
  DECLARE __game_item_id INT;
  DECLARE __game_item_glid INT;
  DECLARE __game_item_bundle_glid INT;
  DECLARE __inventory_compatible CHAR(1);

  DECLARE _curDone INT DEFAULT 0;
  DECLARE _curSourceDynObjects CURSOR FOR 
    SELECT 
      `obj_placement_id`,
      `object_id`, 
      `global_id`, 
      `position_x`,
      `position_y`,
      `position_z`,
      `rotation_x`,
      `rotation_y`,
      `rotation_z` 
    FROM 
      `dynamic_objects` 
    WHERE 
      `zone_index`       = __srcZoneIndex AND
      `zone_instance_id` = __srcZoneInstanceId;

  DECLARE gi_cursor CURSOR FOR
  SELECT sgi.game_item_id, sgi.game_item_glid, CAST(ip.value AS UNSIGNED) AS game_item_bundle_glid, st.inventory_compatible
  FROM script_game_items sgi
  INNER JOIN script_game_item_types st ON sgi.item_type = st.item_type
  LEFT OUTER JOIN wok.item_parameters ip ON sgi.game_item_glid = ip.global_id 
	AND ip.param_type_id = 34
  WHERE sgi.zone_instance_id = __srcZoneInstanceId AND
		sgi.zone_type = zoneType(__srcZoneIndex);

  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET _curDone = 1;

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
      -- ERROR
      GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
      CALL logging.logMsg('ERROR', SCHEMA(), 'import_channel', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
      RESIGNAL;
  END;



  CALL logging.logMsg('TRACE', SCHEMA(), 'import_channel', CONCAT('BEGIN (__importing_player_id[',CAST(__importing_player_id AS CHAR), '] __dstZoneIndex[',CAST(__dstZoneIndex AS CHAR), '] __dstZoneInstanceId[',CAST(__dstZoneInstanceId AS CHAR), '] __baseZoneIndexPlain[',CAST(__baseZoneIndexPlain AS CHAR),'] __srcZoneIndex[',CAST(__srcZoneIndex AS CHAR), '] __srcZoneInstanceId[', CAST(__srcZoneInstanceId AS CHAR), '] __dstPreserveFlag[',CAST(__dstPreserveFlag AS CHAR), '])'));

  SET __retval = -1; -- assume worst

  -- BEGIN Validate Args
  -- 
  -- we only work with 'homes' and 'channels'
  SET __dstZoneType = zoneType(__dstZoneIndex);
  IF __dstZoneType <> 3 AND __dstZoneType <> 6 THEN
    SET __retval = 2;
    CALL logging.logMsg('ERROR', SCHEMA(), 'import_channel', CONCAT('EXITING __retval[', __retval, ']) -- reason: invalid __dstZoneType[',CAST(__dstZoneType AS CHAR), ']'));
    LEAVE this_proc;
  END IF;

  -- 
  -- make sure the user owns the destination zone
  SET __world_owner_user_id = get_zone_owner(__dstZoneIndex, __dstZoneInstanceId);
  IF __world_owner_user_id = 0 THEN
    SET __retval = 3;
    CALL logging.logMsg('ERROR', SCHEMA(), 'import_channel', CONCAT('EXITING __ret[', __retval, ']) -- reason: unable to find owner of destZone( __dstZoneIndex[',CAST(__dstZoneIndex AS CHAR), '] __dstZoneInstanceId[',CAST(__dstZoneInstanceId AS CHAR), '])'));
    LEAVE this_proc;
  END IF;

  -- Grab player_name for logging
  SELECT username, wok_player_id INTO __player_name, __world_owner_player_id
  FROM kaneva.users
  WHERE user_id = __world_owner_user_id;

  IF __world_owner_player_id <> __importing_player_id AND __world_owner_player_id <> 842177 THEN
    SET __retval = 3;
    CALL logging.logMsg('ERROR', SCHEMA(), 'import_channel', CONCAT('EXITING __ret[', __retval, ']) -- reason: __importing_player_id[',CAST(__importing_player_id AS CHAR), '] does not own destZone( __dstZoneIndex[',CAST(__dstZoneIndex AS CHAR), '] __dstZoneInstanceId[',CAST(__dstZoneInstanceId AS CHAR), '])'));
    LEAVE this_proc;
  END IF;

  -- 
  -- make sure the destination zone exists
  SELECT wok.make_zone_index(wok.zoneIndex(__dstZoneIndex), wok.zoneType(__dstZoneIndex)) INTO __dbZoneIndex;
  SELECT
    COUNT(*) INTO __count
  FROM 
    `channel_zones`
  WHERE 
    `zone_index`       = __dbZoneIndex AND
    `zone_instance_id` = __dstZoneInstanceId;
  IF __count = 0 THEN
    SET __retval = 4;
    CALL logging.logMsg('ERROR', SCHEMA(), 'import_channel', CONCAT('EXITING __retval[', __retval, '])-- reson: no channel_zones matching destZone(__dstZoneIndex[',CAST(__dstZoneIndex AS CHAR), '] __dstZoneInstanceId[',CAST(__dstZoneInstanceId AS CHAR), '] using __dbZoneIndex[',CAST(__dbZoneIndex AS CHAR),']'));
    LEAVE this_proc;
  END IF;

  -- Actual logic
  -- 0) compare base zone indexes to see if we need to rekey
  SET __newZoneIndex      = -1;
  SET __newZoneIndexPlain = -1;
  IF (wok.zoneIndex(__dbZoneIndex) <> __baseZoneIndexPlain) THEN
    -- The destination has a different baseZoneIndex.  so we need to rekey
    SET __newZoneIndexPlain = __baseZoneIndexPlain;
    SET __newZoneIndex = wok.make_zone_index(__newZoneIndexPlain, __dstZoneType);

    SET __rc = -1;
    CALL wok.rekey_zone_index(__importing_player_id, __dbZoneIndex, __dstZoneInstanceId, __newZoneIndex, __rc);
    IF (__rc = -1) THEN
      SET __retval = 5;
      CALL logging.logMsg('ERROR', SCHEMA(), 'import_channel', CONCAT('EXITING __retval[', __retval, '])-- reason: rekey_zone_index failed'));
      LEAVE this_proc;
    END IF;

  ELSE  
    -- no rekeys needed make alias for the zoneIndexes to clarify 'copy'
    SET __newZoneIndexPlain = wok.zoneIndex(__dbZoneIndex);
    SET __newZoneIndex = __dbZoneIndex;

  END IF;


  -- wops are terrain based and can not be copied over
  CALL logging.logMsg('TRACE', SCHEMA(), 'import_channel', 'delete world_object_player_settings');
  DELETE 
    FROM 
      `world_object_player_settings`
    WHERE 
      `zone_index`       = __newZoneIndex AND
      `zone_instance_id` = __dstZoneInstanceId;


  -- 2) Clear destination zone (should use full flags)
  CALL logging.logMsg('TRACE', SCHEMA(), 'import_channel', CONCAT('begin clearing destination zone[',CAST(__newZoneIndex AS CHAR),']'));

  -- Clear out custom deed usage
  DELETE
    FROM `custom_deed_usage`
  WHERE `zone_index` = __newZoneIndex AND `zone_instance_id` = __dstZoneInstanceId;

  -- all playlists are removed
  CALL logging.logMsg('TRACE', SCHEMA(), 'import_channel', 'delete dynamic_object_playlists');
  DELETE
    dynamic_object_playlists.*
  FROM 
    dynamic_object_playlists 
   LEFT JOIN dynamic_object_parameters
      ON dynamic_object_playlists.obj_placement_id = dynamic_object_parameters.obj_placement_id
  WHERE
    dynamic_object_parameters.obj_placement_id IS NULL AND
    dynamic_object_playlists.zone_index       = __newZoneIndex AND
    dynamic_object_playlists.zone_instance_id = __dstZoneInstanceId;

  -- delete sound customizations
  DELETE
    FROM sound_customizations 
  WHERE zone_index = __dbZoneIndex AND instance_id = __dstZoneInstanceId;

  -- Replenish user inventory
  INSERT INTO wok.inventory_pending_adds2 (kaneva_user_id, global_id, inventory_type, inventory_sub_type, quantity, last_touch_datetime) 
  (SELECT 
	__world_owner_user_id, global_id, 'B', inventory_sub_type, @returnQty := COUNT(global_id) AS returnQty, NOW()
	FROM 
	wok.dynamic_objects
	WHERE 
	zone_index = __newZoneIndex AND
	zone_instance_id = __dstZoneInstanceId AND 
	expired_date       IS NULL AND -- no try on items
	inventory_sub_type <> 1024 AND -- no unlimited items
	player_id = __importing_player_id
	GROUP BY
	global_id, 
	inventory_sub_type
  )
  ON DUPLICATE KEY UPDATE quantity = quantity + @returnQty, last_touch_datetime = NOW();

  -- Delete all dynamic objects
  CALL logging.logMsg('TRACE', SCHEMA(), 'import_channel', '(mt)delete all dynamic objects');

  DELETE
  dynamic_objects.*, 
  dynamic_object_parameters.* 
  FROM 
  dynamic_objects
  LEFT JOIN dynamic_object_parameters
    ON dynamic_object_parameters.obj_placement_id = dynamic_objects.obj_placement_id
  WHERE 
    dynamic_objects.zone_index       = __newZoneIndex AND 
    dynamic_objects.zone_instance_id = __dstZoneInstanceId ;

  CALL logging.logMsg('TRACE', SCHEMA(), 'import_channel', CONCAT('done  clearing destination zone[',CAST(__newZoneIndex AS CHAR),']'));

  -- Delete script game items
  DELETE
    sgi.*, 
    sgip.* 
  FROM 
    script_game_items sgi
    LEFT JOIN script_game_item_properties sgip
      ON sgip.zone_instance_id = sgi.zone_instance_id
	  AND sgip.zone_type = sgi.zone_type
	  AND sgip.game_item_id = sgi.game_item_id
  WHERE 
      sgi.zone_instance_id = __dstZoneInstanceId
	  AND sgi.zone_type = zoneType(__newZoneIndex);

  -- Log framework deactivation if it was active in the zone
  SELECT IF(MIN(attribute) IS NULL, FALSE, TRUE) INTO __deactivating_framework
  FROM script_game_custom_data 
  WHERE zone_instance_id = __dstZoneInstanceId 
	AND zone_type = zoneType(__newZoneIndex)
	AND attribute = 'WorldSettings'
	AND value LIKE '%"frameworkEnabled":true%';

  IF __deactivating_framework THEN
	INSERT INTO metrics.sgitem_framework_activation_log (uuid, report_time, zone_instance_id, zone_type, username, activation_action)
    VALUES (UUID(), NOW(), __dstZoneInstanceId, zoneType(__newZoneIndex), __player_name, 'DeactivateImport');
  END IF;

  -- Delete script game custom data
  DELETE FROM script_game_custom_data 
  WHERE zone_instance_id = __dstZoneInstanceId AND zone_type = zoneType(__newZoneIndex);

  -- Delete script game player data
  DELETE FROM script_game_player_data 
  WHERE zone_instance_id = __dstZoneInstanceId AND zone_type = zoneType(__newZoneIndex);

  -- 4 If we were doing free zone we are done
  IF (__srcZoneInstanceId = -1) THEN
    -- 4a) user requested free zone
    CALL logging.logMsg('TRACE', SCHEMA(), 'import_channel', CONCAT('all done -- free zone[',CAST(__newZoneIndex AS CHAR),']'));
  ELSE
    -- 4b) Copy from source to destination (should use flags)
	SELECT 
		`apartment_template_id` 
	FROM 
		`custom_deed_usage`
	WHERE 
		`zone_index`       = __srcZoneIndex AND 
		`zone_instance_id` = __srcZoneInstanceId
	INTO 
		__template_id;

	IF __template_id IS NOT NULL AND isCustomDeedUGC(__template_id) THEN
		INSERT INTO custom_deed_usage (`zone_index`, `zone_instance_id`, `apartment_template_id`)
		VALUES (__newZoneIndex, __dstZoneInstanceId, __template_id);
	END IF;
    
    -- 4b_1) wops  only copy if we have not rekeyed the zone
    IF (wok.zoneIndex(__srcZoneIndex) = __newZoneIndexPlain) THEN
      CALL logging.logMsg('TRACE', SCHEMA(), 'import_channel', CONCAT('begin copying wops from srcZoneIndex[',CAST(__srcZoneIndex AS CHAR),'] to dbZoneIndex[',CAST(__dbZoneIndex AS CHAR),']'));
      INSERT INTO `world_object_player_settings` (`world_object_id`, `asset_id`, `texture_url`, `zone_index`, `zone_instance_id`, `last_updated_datetime`)
          SELECT 
            `world_object_id`,
            `asset_id`, 
            `texture_url`, 
            __newZoneIndex, 
            __dstZoneInstanceId, 
            NOW()
          FROM 
            `world_object_player_settings`
          WHERE 
           `zone_index`       = __srcZoneIndex AND
           `zone_instance_id` = __srcZoneInstanceId;
      CALL logging.logMsg('TRACE', SCHEMA(), 'import_channel', CONCAT('done  copying wops from srcZoneIndex[',CAST(__srcZoneIndex AS CHAR),'] to dbZoneIndex[', CAST(__dbZoneIndex AS CHAR),']'));
    END IF;

	-- log framework activation if zone to be imported has framework
	SELECT IF(MIN(attribute) IS NULL, FALSE, TRUE) INTO __activating_framework
	FROM script_game_custom_data 
	WHERE zone_instance_id = __srcZoneInstanceId 
		AND zone_type = zoneType(__srcZoneIndex)
		AND attribute = 'WorldSettings'
		AND value LIKE '%"frameworkEnabled":true%';

	IF __activating_framework THEN
		INSERT INTO metrics.sgitem_framework_activation_log (uuid, report_time, zone_instance_id, zone_type, username, activation_action)
		VALUES (UUID(), NOW(), __dstZoneInstanceId, zoneType(__newZoneIndex), __player_name, 'ActivateImport');
	END IF;

	-- Script game custom data
	INSERT INTO `wok`.`script_game_custom_data` (`zone_instance_id`, `zone_type`, `attribute`, `value`)
	SELECT __dstZoneInstanceId, zoneType(__newZoneIndex), attribute, value
	FROM script_game_custom_data
	WHERE zone_instance_id = __srcZoneInstanceId AND zone_type = zoneType(__srcZoneIndex)
	AND attribute NOT LIKE 'DNC_%'
	AND attribute <> 'Metrics';

	-- 4b_2) copy over script game items
	SET _curDone = 0;
	OPEN gi_cursor;
	REPEAT
	FETCH gi_cursor INTO __game_item_id, __game_item_glid, __game_item_bundle_glid, __inventory_compatible;
	IF NOT _curDone THEN

		INSERT INTO `wok`.`script_game_items` (`zone_instance_id`, `zone_type`, `game_item_id`, `game_item_glid`, `glid`, `name`, `item_type`, `level`, `rarity`)
		SELECT __dstZoneInstanceId, zoneType(__newZoneIndex), game_item_id, game_item_glid, glid, name, item_type, level, rarity
		FROM script_game_items 
		WHERE zone_instance_id = __srcZoneInstanceId AND zone_type = zoneType(__srcZoneIndex) AND game_item_id = __game_item_id;

		IF __game_item_glid IS NULL THEN

			-- Local copies of the properties are only necessary if there is no game item glid
			INSERT INTO `wok`.`script_game_item_properties` (`zone_instance_id`, `zone_type`, `game_item_id`, `property_name`, `property_value`)
			SELECT __dstZoneInstanceId, zoneType(__newZoneIndex), game_item_id, property_name, property_value
			FROM script_game_item_properties
			WHERE zone_instance_id = __srcZoneInstanceId AND zone_type = zoneType(__srcZoneIndex) AND game_item_id = __game_item_id;

		END IF;

	END IF;
	UNTIL _curDone END REPEAT;
	CLOSE gi_cursor;

    -- 4b_3_4) dynamic_objects
	SET _curDone = 0;
    CALL logging.logMsg('TRACE', SCHEMA(), 'import_channel', CONCAT('begin copying do/dops from srcZoneIndex[',CAST(__srcZoneIndex AS CHAR),'] to newZoneIndex[',CAST(__newZoneIndex AS CHAR),']'));
    OPEN _curSourceDynObjects;
    REPEAT 
      FETCH _curSourceDynObjects INTO _srcObjPlacementId, _object_id, _global_id, _x, _y,_z,_rx,_ry,_rz;
      IF NOT _curDone THEN 
        SET _dstObjPlacementId = shard_info.get_dynamic_objects_id();

        INSERT INTO dynamic_objects(`obj_placement_id`, `player_id`, `zone_index`, `zone_instance_id`, `object_id`, `global_id`, `inventory_sub_type`, `position_x`,`position_y`,`position_z`, `rotation_x`,`rotation_y`,`rotation_z`, `created_date`)
        VALUES (_dstObjPlacementId, __importing_player_id, __newZoneIndex, __dstZoneInstanceId, _object_id, _global_id, 512,  _x, _y,_z, _rx,_ry,_rz, NOW());

        INSERT INTO 
          `dynamic_object_parameters` (`obj_placement_id`, `param_type_id`, `value`) 
          SELECT 
            _dstObjPlacementId, 
            `param_type_id`, 
            `value`
          FROM 
              `dynamic_object_parameters` 
          WHERE 
            `obj_placement_id` = _srcObjPlacementId;

		INSERT INTO sound_customizations (obj_placement_id, zone_index, instance_id, pitch, gain, max_distance, roll_off, `loop`, loop_delay, 
			dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle)
		SELECT _dstObjPlacementId, __newZoneIndex, __dstZoneInstanceId, pitch, gain, max_distance, roll_off, `loop`, loop_delay, 
			dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle
		FROM sound_customizations
		WHERE obj_placement_id = _srcObjPlacementId;

		-- Update objectAssociations customdata
	    UPDATE wok.script_game_custom_data
	    SET value = REPLACE(
	  	 			REPLACE(
						REPLACE(
							REPLACE(
								REPLACE(value, ' ', ''), CONCAT('[', _srcObjPlacementId, ']'), CONCAT('[', _dstObjPlacementId, ']')
							), CONCAT('[', _srcObjPlacementId, ','), CONCAT('[', _dstObjPlacementId, ',')
						), CONCAT(',', _srcObjPlacementId, ','), CONCAT(',', _dstObjPlacementId, ',')
					), CONCAT(',', _srcObjPlacementId, ']'), CONCAT(',', _dstObjPlacementId, ']')
				 )
		WHERE zone_instance_id = __dstZoneInstanceId 
			AND zone_type = zoneType(__newZoneIndex)
			AND attribute LIKE 'objectAssociations%';

		-- Update waypointPID game item properties
		UPDATE wok.script_game_item_properties
		SET property_value = _dstObjPlacementId
		WHERE zone_instance_id = __dstZoneInstanceId 
			AND zone_type = zoneType(__newZoneIndex)
			AND property_name = 'waypointPID'
			AND property_value = _srcObjPlacementId;
      END IF;
  
    UNTIL _curDone END REPEAT;
    CLOSE _curSourceDynObjects;
    CALL logging.logMsg('TRACE', SCHEMA(), 'import_channel', CONCAT('done copying do/dops from srcZoneIndex[',CAST(__srcZoneIndex AS CHAR),'] to to newZoneIndex[',CAST(__newZoneIndex AS CHAR),']'));
	
  END IF;

  SET __retval = 0; -- success
  CALL logging.logMsg('TRACE', SCHEMA(), 'import_channel', CONCAT('END (__importing_player_id[',CAST(__importing_player_id AS CHAR),'] __dstZoneIndex[',CAST(__dstZoneIndex AS CHAR), '] __dstZoneInstanceId[',CAST(__dstZoneInstanceId AS CHAR), '] __baseZoneIndexPlain[',CAST(__baseZoneIndexPlain AS CHAR),'] __srcZoneIndex[',CAST(__srcZoneIndex AS CHAR), '] __srcZoneInstanceId[', CAST(__srcZoneInstanceId AS CHAR), '] __dstPreserveFlag[',CAST(__dstPreserveFlag AS CHAR), '] __retval[', __retval, '] __newZoneIndex[', CAST(__newZoneIndex AS CHAR), '])'));

END
;;
DELIMITER ;