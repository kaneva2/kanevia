-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS z_dumpInventory;

DELIMITER ;;
CREATE PROCEDURE `z_dumpInventory`(IN playerid  varchar (80))
BEGIN
  SELECT i.global_id, ii.name, i.inventory_type, armed, quantity, inventory_sub_type,
         ii.market_cost, ii.selling_price, ii.use_type,
         ip.value
    FROM inventories i
            INNER JOIN
               items ii
            ON i.global_id = ii.global_id
         INNER JOIN
            players p
         ON i.player_id = p.player_id
         LEFT OUTER JOIN 
            item_parameters ip
         ON i.global_id = ip.global_id AND ip.param_type_id BETWEEN 8 AND 14 -- old use values
   WHERE p.name = playerid
   ORDER BY i.inventory_type, ii.name;
END
;;
DELIMITER ;