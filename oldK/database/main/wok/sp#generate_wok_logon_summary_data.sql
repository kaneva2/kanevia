-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS generate_wok_logon_summary_data;

DELIMITER ;;
CREATE PROCEDURE `generate_wok_logon_summary_data`(IN _start_date DATETIME, IN _end_date DATETIME)
BEGIN
SELECT NOW() INTO @start_time;
REPLACE INTO summary_logons_by_hour 
    SELECT HOUR(logon_time) AS h,DATE(logon_time) AS cd,COUNT(game_login_id) 
    FROM developer.game_login_log 
    WHERE logon_time >= _start_date AND logon_time <= _end_date 
    AND game_id = environment.getWOKGameId()
    GROUP BY cd,h;
    
REPLACE INTO summary_logons_by_day 
	SELECT DATE(logon_time) AS cd,COUNT(game_login_id) 
	FROM developer.game_login_log 
	WHERE logon_time >= _start_date AND logon_time <= _end_date 
	AND game_id = environment.getWOKGameId()
	GROUP BY cd; 
INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('generate_wok_logon_summary_data',@start_time,NOW(),TIME_TO_SEC(TIMEDIFF(NOW(),@start_time)));
	
END
;;
DELIMITER ;