-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS canPlayerSpawnToChannelZone2;

DELIMITER ;;
CREATE PROCEDURE `canPlayerSpawnToChannelZone2`( IN sourceKanevaId INT(11), 
													    IN country CHAR(2),
													    IN birthDate DATE,
													    IN showMature BINARY(1),
														IN instanceId INT(11), 
										   				OUT reasonCode INT(11), OUT server VARCHAR(64), OUT serverPort INT(11), OUT zoneIndex INT(11), OUT serverId INT(11)  )
BEGIN

	DECLARE sourcePlayerId INT DEFAULT 0;
	DECLARE isGm CHAR DEFAULT 'F';
	DECLARE unused VARCHAR( 100 );
	DECLARE reconnCode INT DEFAULT 0;
	DECLARE unusedScriptId INT DEFAULT 0;
	
    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;

	SET serverId = 0;
	SET reasonCode = 10; 
	CALL checkForReconnect( sourceKanevaId, isGM, sourcePlayerId, reconnCode, server, serverPort );

	
	SELECT zone_index INTO zoneIndex 
		FROM channel_zones cz
	WHERE cz.zone_instance_id = instanceId
		AND cz.zone_type = 6;

	IF zoneIndex IS NULL THEN 
		SET zoneIndex = defaultBroadbandZone();
	END IF;

	IF reconnCode = 0 THEN
	
		START TRANSACTION;
			CALL checkMaxOccupancy( sourcePlayerId, sourceKanevaId, zoneIndex, instanceId, isGm, country, birthDate, FALSE, reasonCode );
			IF reasonCode = 0 THEN
				CALL can_player_spawn_to_zone( sourceKanevaId, 6, zoneIndex, instanceId, 0, isGm, country, birthDate, showMature, reasonCode, serverId, unusedScriptId, unused );
				IF serverId IS NOT NULL AND serverId <> 0 THEN 
					SELECT ip_address, port INTO server, serverPort
						FROM vw_game_servers
						WHERE server_id = serverId;
				END IF;
				
				IF serverId IS NULL OR serverId = 0 THEN 
					SET server = '';
					SET serverPort = 0;
				END IF;	
			END IF;
		COMMIT;

	ELSEIF reconnCode = 1 THEN

		CALL logDebugMsg( 'canPlayerSpawnToChannelZone2', concat( ' server: ', cast( IFNULL(server,0) as char ), 
											' port: ', cast( IFNULL(serverPort,0) as char ) ) );
	
		
		SET reasonCode = 0;
	
	END IF;

END
;;
DELIMITER ;