-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS vw_game_servers;

DELIMITER ;;

 CREATE VIEW `vw_game_servers` AS SELECT `developer`.`game_servers`.`server_id` AS `server_id`,`developer`.`game_servers`.`server_name` AS `server_name`,`developer`.`game_servers`.`game_id` AS `game_id`,`developer`.`game_servers`.`visibility_id` AS `visibility_id`,`developer`.`game_servers`.`ip_address` AS `ip_address`,`developer`.`game_servers`.`port` AS `port`,`developer`.`game_servers`.`number_of_players` AS `number_of_players`,`developer`.`game_servers`.`max_players` AS `max_players`,`developer`.`game_servers`.`server_started_date` AS `server_started_date`,`developer`.`game_servers`.`last_ping_datetime` AS `last_ping_datetime`,`developer`.`game_servers`.`server_status_id` AS `server_status_id`,`developer`.`game_servers`.`affinity_mask` AS `affinity_mask` 
 FROM `developer`.`game_servers` 
 WHERE ( `developer`.`game_servers`.`game_id` = `environment`.`getWOKGameId`( ) ) 
;;
DELIMITER ;