-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_dynamic_object_particle;

DELIMITER ;;
CREATE PROCEDURE `update_dynamic_object_particle`( 
	IN __objSerialNumber INT(11), 
	IN __zoneIndex INT(11), 
	IN __instanceId INT(11), 
	IN __particleGLID INT(11), 
	OUT __rc INT)
BEGIN
	DECLARE __zone_type INT(11) DEFAULT NULL;

	-- WOK specific feature so all permanent zones share customizations based of instance 1
	SET __zone_type = zoneType(__zoneIndex);
	IF __zone_type = 4 OR __zone_type = 5 THEN

		SET __instanceId = 1;
	
	END IF;

	DELETE FROM dynamic_object_parameters
	  WHERE obj_placement_id = __objSerialNumber 
	    AND param_type_id = 206;					-- 206 = particle GLID

	-- If GLID = 0, we are removing the particle. Don't insert a new record.
	IF __particleGLID <> 0 THEN
		INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
		VALUES (__objSerialNumber, 206, __particleGLID) -- 206 = particle GLID
		ON DUPLICATE KEY UPDATE value = __particleGLID;
	END IF;
	
	SET __rc = 0;
END
;;
DELIMITER ;