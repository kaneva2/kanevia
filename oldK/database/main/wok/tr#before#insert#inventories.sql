-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS before_inventories_insert;

DELIMITER ;;
CREATE TRIGGER `before_inventories_insert` BEFORE INSERT ON `inventories` 
    FOR EACH ROW BEGIN
	
	-- old trigger
	SET new.created_date = NOW();
	
	-- new trigger
	INSERT INTO `inventories_transaction_log` 
			( created_date, `global_id`, 	`inventory_type`,   `player_id`,  `armed`,   `quantity_delta`, 	`quantity_new`, `charge_quantity`,  `inventory_sub_type`, trader_id )
	VALUES  ( NOW(),        NEW.global_id, NEW.inventory_type, NEW.player_id, NEW.armed,  NEW.quantity,     NEW.quantity,    NEW.charge_quantity, NEW.inventory_sub_type, @traderId );
END
;;
DELIMITER ;