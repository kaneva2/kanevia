-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS isUserBlocked;

DELIMITER ;;
CREATE FUNCTION `isUserBlocked`( sourceKanevaId INT, destKanevaId INT, instanceId INT, blockType INT, userName VARCHAR(80) ) RETURNS tinyint(1)
    READS SQL DATA
BEGIN
	DECLARE tempId INT(11) DEFAULT NULL;
	DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;

	IF destKanevaId <> 0 THEN
		SELECT user_id INTO tempId
		FROM kaneva.blocked_users
		WHERE user_id = destKanevaId
			AND blocked_user_id = sourceKanevaId;
	ELSE
		SELECT bu.user_id INTO tempId
		FROM kaneva.blocked_users bu
		INNER JOIN kaneva.users u ON bu.user_id = u.user_id
		WHERE u.username = userName
			AND bu.blocked_user_id = sourceKanevaId;
	END IF;

   CALL logDebugMsg( 'isUserBlocked', CONCAT( 'sourceKanevaId:', CAST( IFNULL(sourceKanevaId,'null') AS CHAR), ' destKanevaId:', CAST( IFNULL(destKanevaId,'null') AS CHAR), ' instanceId:', CAST( IFNULL(instanceId,'null') AS CHAR), ' blockType:', CAST( IFNULL(blockType,'null') AS CHAR), ' username:', IFNULL(username, 'null'), ' result:', IFNULL(tempId, 'false') ) );

   RETURN tempId IS NOT NULL;

END
;;
DELIMITER ;