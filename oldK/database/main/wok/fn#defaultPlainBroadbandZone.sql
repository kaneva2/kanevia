-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS defaultPlainBroadbandZone;

DELIMITER ;;
CREATE FUNCTION `defaultPlainBroadbandZone`() RETURNS int(11)
    DETERMINISTIC
BEGIN
-- sp to return the default broadband channel so it can be changed easily
        return 20; -- this is green acres
END
;;
DELIMITER ;