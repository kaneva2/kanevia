-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_dynamic_object_orientation;

DELIMITER ;;
CREATE PROCEDURE `get_dynamic_object_orientation`( IN __object_id INT(11), IN __zone_index INT(11), IN __zone_instance_id INT(11) )
BEGIN
        SELECT `position_x`, `position_y`, `position_z`, `rotation_x`, `rotation_y`, `rotation_z`, `slide_x`,  `slide_y`, `slide_z`
          FROM  `dynamic_objects` 
         WHERE `obj_placement_id` =  __object_id AND `zone_index` =  __zone_index AND `zone_instance_id` = __zone_instance_id;  
  END
;;
DELIMITER ;