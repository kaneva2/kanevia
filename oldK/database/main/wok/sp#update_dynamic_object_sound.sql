-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_dynamic_object_sound;

DELIMITER ;;
CREATE PROCEDURE `update_dynamic_object_sound`( 
	IN __objSerialNumber INT(11), 
	IN __soundGLID INT(11), 
	OUT __rc INT)
BEGIN

	DELETE FROM dynamic_object_parameters
	  WHERE obj_placement_id = __objSerialNumber 
	    AND param_type_id = 207; -- sound param id/use type
	    
	INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
	VALUES (__objSerialNumber, 207, __soundGLID) -- sound param id/use type
	ON DUPLICATE KEY UPDATE value = __soundGLID;
	
	SET __rc = 0;
END
;;
DELIMITER ;