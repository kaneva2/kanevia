-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_player_check_archive;

DELIMITER ;;
CREATE PROCEDURE `get_player_check_archive`( INOUT __player_id INT, INOUT __kaneva_user_id INT )
BEGIN

	IF __kaneva_user_id <> 0 THEN 	
		SELECT player_id, `name`, `title`,  `spawn_config_index`, `original_EDB`, `team_id`, `race_id` , is_active 
		INTO __player_id, @name, @title,  @spawn_config_index, @original_EDB, @team_id, @race_id, @is_active
		 FROM `players` 
		 WHERE kaneva_user_id = __kaneva_user_id;
	ELSEIF __player_id <> 0 THEN 
		SELECT kaneva_user_id, `name`, `title`,  `spawn_config_index`, `original_EDB`, `team_id`, `race_id` , is_active 
		INTO __kaneva_user_id, @name, @title,  @spawn_config_index, @original_EDB, @team_id, @race_id, @is_active
		 FROM `players` 
		 WHERE player_id = __player_id;
	END IF;

	-- see if some of users data has been inactivated and needs to be activated
	IF @is_active = 'N' THEN
		CALL activate_player_data(__player_id);
	END IF;

	SELECT @name as name, @title as title, @spawn_config_index as spawn_config_index, 
		@original_EDB as original_EDB, @team_id as team_id, @race_id as race_id, __player_id as player_id;

END
;;
DELIMITER ;