-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_deed_template_items;

DELIMITER ;;
CREATE PROCEDURE `get_deed_template_items`( IN __zoneIndex INT, IN __instanceId INT, IN __zone_index_plain INT )
BEGIN
  
	SELECT display_name, 2000 AS market_cost 
	FROM supported_worlds
	WHERE zone_index = __zone_index_plain;
  
	SELECT do.obj_placement_id AS obj_placement_id, 
		do.global_id AS global_id, 
		it.name AS name, 
		512 AS inventory_sub_type, 
		it.use_type AS use_type, 
		(  iw.designer_price
		+ IFNULL((SELECT iw2.designer_price FROM shopping.items_web iw2 WHERE iw.base_global_id = iw2.global_id), 0)) AS market_cost,
		IF(saip.global_id IS NOT NULL, 1, iw.item_active) AS item_active, 
		ifnull((SELECT pass_group_id FROM pass_group_items WHERE global_id = do.global_id), 0) AS pass_group_id,
		it.item_creator_id,
		COUNT(*) AS item_count
	FROM dynamic_objects do
	INNER JOIN items it
		ON it.global_id = do.global_id
	INNER JOIN shopping.items_web iw
		ON iw.global_id = do.global_id
	LEFT OUTER JOIN script_available_items_plain saip ON do.global_id = saip.global_id
	WHERE do.zone_index = __zoneIndex and do.zone_instance_id = __instanceId
		and do.expired_date is null
	GROUP BY global_id;

	SELECT do.obj_placement_id, 
		it.global_id AS global_id, 
		it.name AS name, 
		512 AS inventory_sub_type, 
		dop.param_type_id AS use_type,
		(  iw.designer_price + IFNULL((SELECT iw2.designer_price FROM shopping.items_web iw2 WHERE iw.base_global_id = iw2.global_id), 0)) AS market_cost,
		IF(saip.global_id IS NOT NULL, 1, iw.item_active) AS item_active, 
		ifnull((select pgi.pass_group_id from pass_group_items pgi where pgi.global_id = dop.VALUE),0) AS pass_group_id, 
		it.item_creator_id AS item_creator_id, COUNT(*) AS item_count
	FROM dynamic_object_parameters dop
	INNER JOIN dynamic_objects do
		ON do.obj_placement_id = dop.obj_placement_id 
	INNER JOIN items it
		ON it.global_id = dop.VALUE
	INNER JOIN shopping.items_web iw
		ON iw.global_id = dop.VALUE
	LEFT OUTER JOIN script_available_items_plain saip ON do.global_id = saip.global_id
	WHERE dop.param_type_id in (32, 206) 
		and do.zone_index = __zoneIndex and do.zone_instance_id = __instanceId and do.expired_date is null
	GROUP BY it.global_id;    
END
;;
DELIMITER ;