-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS activate_player_data;

DELIMITER ;;
CREATE PROCEDURE `activate_player_data`(_player_id INT)
BEGIN

DECLARE __message_text VARCHAR(500);

INSERT IGNORE INTO dynamic_objects (`obj_placement_id`,`player_id`,`zone_index`,`zone_instance_id`,
		`object_id`,`global_id`,`inventory_sub_type`,`position_x`,`position_y`,`position_z`,`rotation_x`,
		`rotation_y`,`rotation_z`, `slide_x`,  `slide_y`, `slide_z`,`created_date`,`expired_date`)
	SELECT `obj_placement_id`,`player_id`,`zone_index`,`zone_instance_id`,
		`object_id`,`global_id`,`inventory_sub_type`,`position_x`,`position_y`,`position_z`,`rotation_x`,
		`rotation_y`,`rotation_z`, `slide_x`,  `slide_y`, `slide_z`,`created_date`,`expired_date`
	FROM dynamic_objects_inactive WHERE player_id = _player_id;

INSERT IGNORE INTO dynamic_object_parameters (obj_placement_id, param_type_id, `value`)
	SELECT dopi.obj_placement_id, param_type_id, `value`
		FROM dynamic_object_parameters_inactive dopi
		INNER JOIN dynamic_objects_inactive dyoi ON dopi.obj_placement_id = dyoi.obj_placement_id
		WHERE dyoi.player_id = _player_id;
	
DELETE dopi
	FROM dynamic_object_parameters_inactive dopi
	INNER JOIN dynamic_objects_inactive dyoi ON dopi.obj_placement_id = dyoi.obj_placement_id
	WHERE player_id = _player_id;

DELETE FROM dynamic_objects_inactive WHERE player_id = _player_id;

INSERT IGNORE INTO deformable_configs (`sequence`,`player_id`,`equip_id`,`deformable_config`,
			`custom_material`,`custom_color_r`,`custom_color_g`,`custom_color_b`)
	SELECT `sequence`,`player_id`,`equip_id`,`deformable_config`,
			`custom_material`,`custom_color_r`,`custom_color_g`,`custom_color_b`
	FROM deformable_configs_inactive WHERE player_id = _player_id;

DELETE FROM deformable_configs_inactive WHERE player_id = _player_id;

INSERT IGNORE INTO inventories (`global_id`,`inventory_type`, `inventory_sub_type`,
		`player_id`, `armed`, `quantity`, `charge_quantity`, `created_date` )
	SELECT `global_id`,`inventory_type`, `inventory_sub_type`,
		`player_id`, `armed`, `quantity`, `charge_quantity`, `created_date`
	FROM inventories_inactive WHERE player_id = _player_id;

DELETE FROM inventories_inactive WHERE player_id = _player_id;

INSERT INTO `inactive_audit_log` (`player_id`,`when_added`,`last_logon`,`action_type`)
	SELECT _player_id, NOW(), IFNULL(gu.second_to_last_logon,IFNULL(gu.last_logon,'2000-01-01 00:00:00')), 'A'
	FROM players p
	INNER JOIN game_users gu ON p.user_id = gu.user_id
	WHERE p.player_id = _player_id;

UPDATE players SET is_active = 'Y' WHERE player_id = _player_id;

SELECT CONCAT('Activation completed ..... ',CAST(_player_id AS CHAR), ' player_id moved back to active tables.') INTO __message_text;
CALL add_maintenance_log (DATABASE(),'activate_player_data',__message_text);

END
;;
DELIMITER ;