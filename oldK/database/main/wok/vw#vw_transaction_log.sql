-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS vw_transaction_log;

DELIMITER ;;

 CREATE VIEW `vw_transaction_log` AS SELECT `kaneva`.`wok_transaction_log`.`trans_id` AS `trans_id`,`kaneva`.`wok_transaction_log`.`created_date` AS `created_date`,`kaneva`.`wok_transaction_log`.`user_id` AS `user_id`,`kaneva`.`wok_transaction_log`.`balance_prev` AS `balance_prev`,`kaneva`.`wok_transaction_log`.`balance` AS `balance`,`kaneva`.`wok_transaction_log`.`transaction_amount` AS `transaction_amount`,`kaneva`.`wok_transaction_log`.`kei_point_id` AS `kei_point_id`,`kaneva`.`wok_transaction_log`.`transaction_type` AS `transaction_type` 
 FROM `kaneva`.`wok_transaction_log`
;;
DELIMITER ;