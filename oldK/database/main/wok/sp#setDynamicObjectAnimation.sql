-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS setDynamicObjectAnimation;

DELIMITER ;;
CREATE PROCEDURE `setDynamicObjectAnimation`( IN __obj_placement_id INT, 
                IN __param_type_id INT, IN __value VARCHAR(1000), OUT __result INT)
BEGIN

DECLARE obj_id INT DEFAULT NULL;

SELECT obj_placement_id INTO obj_id 
                FROM dynamic_objects WHERE obj_placement_id = __obj_placement_id;

IF obj_id = __obj_placement_id THEN
                INSERT INTO dynamic_object_parameters ( obj_placement_id, param_type_id, `value`)
                                VALUES ( __obj_placement_id, __param_type_id, __value )
                                ON DUPLICATE KEY UPDATE `value` = __value;
        SET __result = 1;
    ELSE
        SET __result = 0;
        END IF;
END
;;
DELIMITER ;