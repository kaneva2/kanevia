-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_dynamic_object_texture;

DELIMITER ;;
CREATE PROCEDURE `update_dynamic_object_texture`( IN __assetId INT(11), IN __objSerialNumber INT(11),
												IN __zoneIndex INT(11), IN __instanceId INT(11), 
												IN __inTextureURL VARCHAR(1000), OUT __rc INT, OUT __textureURL VARCHAR(1000))
BEGIN
	DECLARE __global_id INT DEFAULT 0;
	DECLARE __mature_asset enum('Y','N') DEFAULT 'N';
	DECLARE __pass_group_id INT;
	DECLARE __zone_type INT(11) DEFAULT NULL;

	SET __rc = 0;
	
	-- WOK specific feature so all permanent zones share customizations based of instance 1
	SET __zone_type = zoneType(__zoneIndex);
	IF __zone_type = 4 OR __zone_type = 5 THEN

		SET __instanceId = 1;
	
	END IF;

	IF __assetId > 0 THEN

		IF __inTextureURL = '' THEN
				CALL fetch_image_url_and_mature( __assetId, 'T', __textureURL, __mature_asset );	

				IF __textureURL IS NULL THEN 
						SET __rc = 2; -- not found 
				ELSEIF __mature_asset = 'Y' AND NOT is_mature_zone(__zoneIndex,__instanceId) THEN
  					-- can't do it!
  				SET __rc = 12; -- need AP
   			END IF;
		ELSE
				SET __textureURL = __inTextureURL;
		END IF;

	ELSE
		SET __textureURL = '';
	END IF;

	IF __rc = 0 THEN
	
		-- need to get id first since checking ROW_COUNT
		-- may return 0 if we match thus sending back error code.
		SELECT global_id INTO __global_id
		  FROM dynamic_objects
		WHERE obj_placement_id = __objSerialNumber 
			AND	zone_index = __zoneIndex 
			AND	zone_instance_id = __instanceId;

		-- 1/09 can't change texture on UGC
		IF FOUND_ROWS() > 0 THEN
			DELETE FROM dynamic_object_parameters
			  WHERE obj_placement_id = __objSerialNumber 
			    AND param_type_id = 20; -- friend id
			    
			INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
			VALUES (__objSerialNumber, 14, __assetId) -- 14 = asset id
			ON DUPLICATE KEY UPDATE value = __assetId;
			
			INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) 
			VALUES (__objSerialNumber, 15, __textureURL) -- 15 = url
			ON DUPLICATE KEY UPDATE value = __textureURL;
			
			SET __rc = 0;
		ELSE
			SET __rc = 5; -- ugc
		END IF;
		
	END IF;
END
;;
DELIMITER ;