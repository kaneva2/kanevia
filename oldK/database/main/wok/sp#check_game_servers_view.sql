-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS check_game_servers_view;

DELIMITER ;;
CREATE PROCEDURE `check_game_servers_view`( IN __game_id INT )
BEGIN
	
	SET @__sql := CONCAT( "	CREATE OR REPLACE VIEW vw_game_servers 
							AS
							SELECT server_id,server_name,game_id,visibility_id ,ip_address,port,number_of_players,max_players,server_started_date,last_ping_datetime,server_status_id,affinity_mask
								FROM developer.game_servers
								WHERE server_type_id = 1
								 AND game_id = ", CAST( __game_id AS CHAR ) );

	PREPARE __stmt FROM @__sql;
	EXECUTE __stmt;
	DEALLOCATE PREPARE __stmt;
END
;;
DELIMITER ;