-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS insert_players;

DELIMITER ;;
CREATE PROCEDURE `insert_players`( IN __user_id INT(11), IN __kaneva_user_id INT(11),
			IN __name VARCHAR(80), IN __title VARCHAR(50), IN __spawn_config_index INT(11),
			IN __original_edb INT(11), IN __team_id INT(11), IN __race_id INT(11),
			OUT __ret INT(11))
BEGIN

  DECLARE __sql_statement VARCHAR(512);

	INSERT INTO players 
		(user_id, kaneva_user_id, `name`, title, spawn_config_index, original_edb,               
		team_id, race_id, last_update_time, created_date)   
		VALUES (__user_id, __kaneva_user_id, __name, __title, __spawn_config_index,               
		__original_edb, __team_id, __race_id, NOW(), NOW());
	
	SET __ret = LAST_INSERT_ID();
		
	UPDATE kaneva.users u SET wok_player_id = __ret  WHERE u.user_id = __kaneva_user_id;
END
;;
DELIMITER ;