-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS reiny_playlist;

DELIMITER ;;
CREATE PROCEDURE `reiny_playlist`()
BEGIN
set @zone_index = 805306404;
set @zone_instance_id = 78141; 
set @asset_group_id = 302066;
set @player_id = 78141;
	SELECT is_mature_zone( @zone_index, @zone_instance_id) INTO @is_access_pass;
	IF @asset_group_id = -7 THEN
	
		IF @is_access_pass = 1 THEN
			select '1';
			SELECT a.asset_id, 0 AS asset_offsite_id, asset_type_id, asset_sub_type_id, run_time_seconds, a.thumbnail_medium_path, 
			mature, public, 'N' AS over_21_required, number_of_diggs, a.media_path
			FROM kaneva.featured_assets fa, kaneva.vw_published_public_mature_assets a
			INNER JOIN kaneva.assets_stats ass ON a.asset_id = ass.asset_id
			INNER JOIN kaneva.communities_personal com ON a.owner_id = com.creator_id
			WHERE a.asset_id = fa.asset_id
			AND fa.status_id = 2 
			AND fa.start_datetime <=  NOW()
			AND run_time_seconds > 0
			AND asset_sub_type_id >= 0
			AND (fa.end_datetime >=  NOW()
			OR fa.end_datetime IS NULL);
		ELSE
			SELECT '2';
		
			SELECT a.asset_id, 0 AS asset_offsite_id, asset_type_id, asset_sub_type_id, run_time_seconds, a.thumbnail_medium_path, 
			mature, public, 'N' AS over_21_required, number_of_diggs, a.media_path
			FROM kaneva.featured_assets fa, kaneva.vw_published_public_mature_assets a
			INNER JOIN kaneva.assets_stats ass ON a.asset_id = ass.asset_id
			INNER JOIN kaneva.communities_personal com ON a.owner_id = com.creator_id
			WHERE a.asset_id = fa.asset_id
			AND fa.status_id = 2 
			AND fa.start_datetime <=  NOW()
			AND (fa.end_datetime >=  NOW()
			OR fa.end_datetime IS NULL)
			AND run_time_seconds > 0
			AND asset_sub_type_id >= 0
			AND mature = 'N' ;
		END IF;
	ELSEIF @asset_group_id = -1 THEN
	
		IF @is_access_pass = 1 THEN
			SELECT '3';
			SELECT a.asset_id, a.asset_offsite_id, asset_type_id, asset_sub_type_id, run_time_seconds, a.thumbnail_medium_path, 
			mature, public, a.over_21_required, number_of_diggs, a.media_path
			FROM kaneva.asset_channels ac
			INNER JOIN kaneva.assets a ON ac.asset_id = a.asset_id AND a.asset_type_id IN (2,4)
			INNER JOIN kaneva.assets_stats d ON a.asset_id = d.asset_id
			INNER JOIN kaneva.communities_personal cp ON cp.community_id = ac.channel_id
			INNER JOIN wok.players p ON cp.creator_id = p.kaneva_user_id
			WHERE p.player_id = __player_id AND ac.status_id = 1
			AND run_time_seconds > 0
			AND asset_sub_type_id >= 0;
		ELSE
			SELECT '4';
		
			SELECT a.asset_id, a.asset_offsite_id, asset_type_id, asset_sub_type_id, run_time_seconds, a.thumbnail_medium_path, 
			mature, public, a.over_21_required, number_of_diggs, a.media_path
			FROM kaneva.asset_channels ac
			INNER JOIN kaneva.assets a ON ac.asset_id = a.asset_id AND a.asset_type_id IN (2,4)
			INNER JOIN kaneva.assets_stats d ON a.asset_id = d.asset_id
			INNER JOIN kaneva.communities_personal cp ON cp.community_id = ac.channel_id
			INNER JOIN wok.players p ON cp.creator_id = p.kaneva_user_id
			WHERE p.player_id = @player_id AND ac.status_id = 1
			AND run_time_seconds > 0
			AND asset_sub_type_id >= 0
			AND mature = 'N';
		END IF;
	ELSE
		IF @is_access_pass = 1 THEN
			SELECT '5';
			SELECT ga.asset_id, a.asset_offsite_id, asset_type_id, asset_sub_type_id, run_time_seconds, thumbnail_medium_path, 
			mature, public, over_21_required, number_of_diggs, a.media_path
			FROM kaneva.asset_group_assets ga
			INNER JOIN kaneva.assets a ON ga.asset_id = a.asset_id
			INNER JOIN kaneva.assets_stats d ON a.asset_id = d.asset_id 
			WHERE asset_group_id = @asset_group_id AND run_time_seconds > 0
			AND asset_sub_type_id >= 0
			ORDER BY ga.sort_order, a.created_date DESC;
		ELSE
			SELECT '6';
		
			SELECT ga.asset_id, a.asset_offsite_id, asset_type_id, asset_sub_type_id, run_time_seconds, thumbnail_medium_path, 
			mature, public, over_21_required, number_of_diggs, a.media_path
			FROM kaneva.asset_group_assets ga
			INNER JOIN kaneva.assets a ON ga.asset_id = a.asset_id
			INNER JOIN kaneva.assets_stats d ON a.asset_id = d.asset_id 
			WHERE asset_group_id = @asset_group_id AND run_time_seconds > 0
			AND asset_sub_type_id >= 0
			AND mature = 'N'
			ORDER BY ga.sort_order, a.created_date DESC;
		END IF;
	END IF;
	
END
;;
DELIMITER ;