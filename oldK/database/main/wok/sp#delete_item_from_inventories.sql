-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_item_from_inventories;

DELIMITER ;;
CREATE PROCEDURE `delete_item_from_inventories`(globalId INT UNSIGNED)
    COMMENT '\n    -- \n  '
this_proc:
BEGIN
 
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- ERROR
      GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
      CALL logging.logMsg('ERROR', SCHEMA(), 'delete_item_from_inventories', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
      RESIGNAL;
    END;

  CALL logging.logMsg('TRACE', SCHEMA(), 'delete_item_from_inventories', CONCAT('BEGIN (globalId[',CAST(globalId AS CHAR),'])'));


  /* delete the records */
  DELETE FROM inventories WHERE global_id = globalId;

  /* return the number of records affected */
  SELECT row_count();

  CALL logging.logMsg('TRACE', SCHEMA(), 'delete_item_from_inventories', CONCAT('END (globalId[',CAST(globalId AS CHAR),'])'));


END
;;
DELIMITER ;