-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS inactivate_player_data;

DELIMITER ;;
CREATE PROCEDURE `inactivate_player_data`(__archive_before_date	DATE, __archive_cutoff_time DATETIME)
BEGIN

DECLARE	_player_id		BIGINT;
DECLARE	__table_RowCnt		INT;
DECLARE	__OrigRowCnt		INT;
DECLARE __message_text VARCHAR(500);
DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE arch_cursor CURSOR FOR 
			SELECT p.player_id
			FROM	players p
			INNER JOIN game_users gu ON p.user_id = gu.user_id
			WHERE is_active = 'Y' AND DATE(gu.last_logon) < __archive_before_date
			ORDER BY player_id
			LIMIT 50000
			FOR UPDATE;

DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

SET done = 0;
SET i = 0;

OPEN arch_cursor;
SET __table_RowCnt = (SELECT FOUND_ROWS());
SELECT CONCAT('Starting inactivation ... ',CAST(__table_RowCnt AS CHAR),' rows to inactivate. Inactivate before date: ',
						CAST(__archive_before_date AS CHAR),'; Cutoff time: ',
						CAST(__archive_cutoff_time AS CHAR)) INTO __message_text;
CALL add_maintenance_log (DATABASE(),'inactivate_player_data',__message_text);

-- remove orphaned/expired try on items
DELETE FROM dynamic_objects WHERE expired_date < NOW();

-- take away the green title if no active VIP pass for the player
UPDATE player_presences pp 
	INNER JOIN players p ON pp.player_id = p.player_id
	INNER JOIN game_users gu ON p.user_id = gu.user_id
	LEFT OUTER JOIN vw_pass_group_users v ON gu.kaneva_user_id = v.kaneva_user_id AND v.pass_group_id = 2
	SET pp.color_type = 0
	WHERE pp.color_type = 1 AND v.pass_group_id IS NULL;

REPEAT
    FETCH arch_cursor INTO _player_id;
    IF NOT done THEN
	SET i=i+1;
	START TRANSACTION;
	INSERT IGNORE INTO dynamic_objects_inactive (`obj_placement_id`,`player_id`, zone_index,`zone_instance_id`,
			`object_id`,`global_id`,`inventory_sub_type`,`position_x`,`position_y`,`position_z`,`rotation_x`,
			`rotation_y`,`rotation_z`, `slide_x`,  `slide_y`, `slide_z`,`created_date`,`expired_date`)
		SELECT `obj_placement_id`,`player_id`, dyo.zone_index, dyo.zone_instance_id,
			`object_id`,`global_id`,`inventory_sub_type`,`position_x`,`position_y`,`position_z`,`rotation_x`,
			`rotation_y`,`rotation_z`, `slide_x`,  `slide_y`, `slide_z`, dyo.created_date,`expired_date`
		FROM dynamic_objects dyo
		WHERE player_id = _player_id AND zoneType(zone_index) = 3 AND zone_instance_id = player_id;

	INSERT IGNORE INTO dynamic_object_parameters_inactive (obj_placement_id, param_type_id, `value`)
		SELECT dop.obj_placement_id, param_type_id, `value`
			FROM dynamic_object_parameters dop
			INNER JOIN dynamic_objects dyo ON dop.obj_placement_id = dyo.obj_placement_id
			WHERE player_id = _player_id AND zoneType(zone_index) = 3 AND zone_instance_id = player_id;
	
	DELETE dop
		FROM dynamic_object_parameters dop 
		INNER JOIN dynamic_objects dyo ON dop.obj_placement_id = dyo.obj_placement_id
		WHERE player_id = _player_id AND zoneType(zone_index) = 3 AND zone_instance_id = player_id;

	DELETE dyo 
		FROM dynamic_objects dyo
		WHERE player_id = _player_id AND zoneType(zone_index) = 3 AND zone_instance_id = player_id;

	INSERT IGNORE INTO deformable_configs_inactive (`sequence`,`player_id`,`equip_id`,`deformable_config`,
				`custom_material`,`custom_color_r`,`custom_color_g`,`custom_color_b`)
		SELECT `sequence`,`player_id`,`equip_id`,`deformable_config`,
				`custom_material`,`custom_color_r`,`custom_color_g`,`custom_color_b`
		FROM deformable_configs WHERE player_id = _player_id;

	DELETE FROM deformable_configs WHERE player_id = _player_id;

	INSERT IGNORE INTO inventories_inactive (`global_id`,`inventory_type`, `inventory_sub_type`,
			`player_id`, `armed`, `quantity`, `charge_quantity`, `created_date` )
		SELECT `global_id`,`inventory_type`, `inventory_sub_type`,
			`player_id`, `armed`, `quantity`, `charge_quantity`, `created_date`
		FROM inventories WHERE player_id = _player_id;

	DELETE FROM inventories WHERE player_id = _player_id;

	UPDATE players SET is_active = 'N' WHERE player_id = _player_id;

	INSERT INTO `inactive_audit_log` (`player_id`,`when_added`,`last_logon`,`action_type`)
		SELECT _player_id, NOW(), gu.last_logon, 'I'
		FROM players p
		INNER JOIN game_users gu ON p.user_id = gu.user_id
		WHERE p.player_id = _player_id;

	COMMIT;

	IF NOW() >= __archive_cutoff_time THEN 
		SET done = 1; 
	END IF;
    END IF;	
UNTIL done END REPEAT;
CLOSE arch_cursor;

SELECT CONCAT('Inactivation completed ..... ',CAST(i AS CHAR), ' players moved to inactive tables.') INTO __message_text;
CALL add_maintenance_log (DATABASE(),'inactivate_player_data',__message_text);

END
;;
DELIMITER ;