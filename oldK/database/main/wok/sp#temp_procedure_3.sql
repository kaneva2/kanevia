-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS temp_procedure_3;

DELIMITER ;;
CREATE PROCEDURE `temp_procedure_3`()
BEGIN
DECLARE done INT DEFAULT 0;
DECLARE _count INT DEFAULT 0;
DECLARE _itemID INT DEFAULT NULL;
DECLARE pass_items_cursor CURSOR FOR select i.global_id from wok.items i left join wok.pass_group_items p on p.global_id = i.global_id where i.access_type_id = 2 and p.global_id is null;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
   SET done = 0;
  OPEN pass_items_cursor;
 REPEAT
  FETCH pass_items_cursor INTO _itemID;
      insert into wok.pass_group_items( global_id, pass_group_id) values (_itemID,1);
      insert into wok.pass_group_items_audit (global_id, pass_group_id, modifier_kaneva_user_id, change_date, operation) VALUES (_itemID, 1, 3875959, now(), 'INSERT');
   UNTIL done END REPEAT;
  CLOSE pass_items_cursor;
END
;;
DELIMITER ;