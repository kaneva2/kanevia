-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS record_template_application;

DELIMITER ;;
CREATE PROCEDURE `record_template_application`( IN __player_id INT(11),
                                                IN __player_template_id INT(11))
BEGIN

  INSERT INTO player_template_applications (`player_id`, `template_id`, `date_applied`) VALUES ( __player_id, __player_template_id, NOW() );

END
;;
DELIMITER ;