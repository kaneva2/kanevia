-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS parse_userid;

DELIMITER ;;
CREATE FUNCTION `parse_userid`( __url VARCHAR(255) ) RETURNS int(11)
    DETERMINISTIC
BEGIN       
	DECLARE __firstSlash INT;
	DECLARE __secondSlash INT;
	
	SET __firstSlash = locate("/", __url);
	SET __secondSlash = locate("/", __url,__firstSlash+1);
	IF __secondSlash <> 0 THEN
		return convert( MID(__url, __firstSlash+1, __secondSlash-__firstSlash-1), signed);
	ELSE
		return convert( MID(__url, 1, __firstSlash-1), signed );
	END IF;
END
;;
DELIMITER ;