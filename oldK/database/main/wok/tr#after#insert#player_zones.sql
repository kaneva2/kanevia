-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS pz_insert;

DELIMITER ;;
CREATE TRIGGER `pz_insert` AFTER INSERT ON `player_zones` FOR EACH ROW BEGIN
-- trigger to update counts in summary table
	CALL zoningIn(  new.player_id, new.current_zone_index, new.current_zone_instance_id, new.server_id );
END
;;
DELIMITER ;