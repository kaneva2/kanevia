-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS updateUserHourStats;

DELIMITER ;;
CREATE PROCEDURE `updateUserHourStats`( in start_date datetime, in end_date datetime)
BEGIN

SELECT NOW() INTO @start_time;

Replace into summary_users_by_hour 
	Select sm.cur_date, sm.cur_hour, avg(logged_on_players), peak.peak_count
	from summary_users_by_minute sm 
	inner join wok.peak_concurrent_players peak ON sm.cur_date = peak.cur_date AND sm.cur_hour = peak.cur_hour
	where logged_on_players > 0 
	and concat(sm.cur_date,' ',IF(sm.cur_hour<10,CONCAT('0',sm.cur_hour),sm.cur_hour),':00:00') between start_date and end_date 
	group by sm.cur_date,sm.cur_hour;
    
END
;;
DELIMITER ;