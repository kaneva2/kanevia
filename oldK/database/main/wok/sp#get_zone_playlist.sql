-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_zone_playlist;

DELIMITER ;;
CREATE PROCEDURE `get_zone_playlist`(IN __zone_index INT, IN __zone_instance_id INT)
BEGIN
	
	SELECT `obj_placement_id`, `asset_group_id`, `global_id`, `video_range`, `audio_range`, `volume`
	  FROM `dynamic_object_playlists`
	 WHERE `zone_index` = __zone_index
	   AND `zone_instance_id` = __zone_instance_id;

	IF FOUND_ROWS() = 0 THEN 
		CALL logDebugMsg( 'get_zone_playlist', concat( 'Could not find playlist for zone_index:', cast(__zone_index as char), ' zone_instance_id:', cast( __zone_instance_id as char ) ) );
	END IF;
	
END
;;
DELIMITER ;