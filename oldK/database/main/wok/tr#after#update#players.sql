-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS logOffPlayer;

DELIMITER ;;
CREATE TRIGGER `logOffPlayer` AFTER UPDATE ON `players` 
    FOR EACH ROW BEGIN
	IF NEW.in_game = 'F' THEN 
		-- logging off
		-- DELETE FROM player_zones 
		UPDATE player_zones
		   SET server_id = 0
		 WHERE player_id = NEW.player_id;
	END IF;
END
;;
DELIMITER ;