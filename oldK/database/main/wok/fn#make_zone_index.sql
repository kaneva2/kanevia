-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS make_zone_index;

DELIMITER ;;
CREATE FUNCTION `make_zone_index`( __zi INT, __type INT ) RETURNS int(11)
    DETERMINISTIC
BEGIN
	RETURN (__type << 28) | __zi;
END
;;
DELIMITER ;