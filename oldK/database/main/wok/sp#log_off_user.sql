-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS log_off_user;

DELIMITER ;;
CREATE PROCEDURE `log_off_user`( IN __user_id INT(11), IN __kaneva_user_id INT, IN __server_id INT(11), IN __time_played INT(11), IN __logout_reason_code VARCHAR(50), IN __ip_address VARCHAR(50) )
BEGIN
-- called whenever a user logs off a game server (including zoning between servers)
	DECLARE __created_date DATETIME;
	DECLARE __deferred_sql VARCHAR(255);
	DECLARE __q_logout_reason_code VARCHAR(255);
	DECLARE __q_ip_address VARCHAR(50);
	DECLARE __q_created_date VARCHAR(50);
	DECLARE __q_now VARCHAR(50);
	
	UPDATE game_users 
	   SET logged_on = 'F', 
	       time_played = time_played + __time_played, 
		   login_count = login_count + IF(__logout_reason_code = 'NORMAL',1,0),
	       last_update_time = NOW() 
	 WHERE user_id = __user_id;
END
;;
DELIMITER ;