-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS deleteUser;

DELIMITER ;;
CREATE PROCEDURE `deleteUser`( _userName  VARCHAR(80) )
BEGIN
	DECLARE accountId INT(11) DEFAULT 0;
	SELECT user_id INTO accountId FROM game_users WHERE username = _userName;
	DELETE from game_users WHERE user_id = accountId;
END
;;
DELIMITER ;