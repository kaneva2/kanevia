-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS insert_starting_inventory;

DELIMITER ;;
CREATE PROCEDURE `insert_starting_inventory`( IN __player_id INT(11),
												IN __kaneva_user_id INT(11),
												IN __original_edb INT(11),
												IN __player_template_id INT(11))
BEGIN

	IF __player_template_id > 0 THEN

    SELECT global_id, armed, quantity, sub_type 
      FROM starting_inventories 
      WHERE player_template_id = __player_template_id AND active = 1; -- Return which items need to be added

  ELSE
  
		CALL logDebugMsg('insert_start_inv', concat('template id == 0', ' orig_edb: ', IFNULL(CAST(__original_edb as char),'NULL')));
        
    SELECT global_id, armed, quantity, sub_type 
      FROM starting_inventories 
      WHERE original_EDB = __original_edb AND active = 1 AND player_template_id = 0; -- Return which items need to be added
      
	END IF;
	  
END
;;
DELIMITER ;