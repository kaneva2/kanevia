-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS tiedServerId;

DELIMITER ;;
CREATE FUNCTION `tiedServerId`( __zone_instance INT ) RETURNS int(11)
    READS SQL DATA
BEGIN
   declare _serverId int default 0;

   select server_id
     into _serverId
     from wok.channel_zones cz
    where cz.zone_instance_id = __zone_instance
          and cz.tied_to_server = 'PERMANENTLY_TIED';
   return _serverId;
END
;;
DELIMITER ;