-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS insert_default_clothing;

DELIMITER ;;
CREATE PROCEDURE `insert_default_clothing`( IN __player_id INT(11), IN __gender CHAR, OUT __ret INT(11) )
BEGIN
  SET __ret = 0;
  
  IF __gender = 'M' THEN
  
		INSERT IGNORE INTO inventories
					(`global_id`, `inventory_type`, `player_id`, `armed`, `quantity`, 
					`charge_quantity`, `inventory_sub_type`, `created_date`)
		VALUES( 1172, 'P', __player_id, 'T', 1,	0, 512, NOW() ),
          ( 1173, 'P', __player_id, 'T', 1,	0, 512, NOW() ),
          ( 1174, 'P', __player_id, 'T', 1,	0, 512, NOW() );   
          
  ELSEIF __gender = 'F' THEN
  
		INSERT IGNORE INTO inventories
					(`global_id`, `inventory_type`, `player_id`, `armed`, `quantity`, 
					`charge_quantity`, `inventory_sub_type`, `created_date`)
		VALUES( 1247, 'P', __player_id, 'T', 1,	0, 512, NOW() ),
          ( 1248, 'P', __player_id, 'T', 1,	0, 512, NOW() ),
          ( 1249, 'P', __player_id, 'T', 1,	0, 512, NOW() ); 
          
  ELSE 
  
    SET __ret = 2;
  
  END IF;
END
;;
DELIMITER ;