-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS validate_ugc_request;

DELIMITER ;;
CREATE PROCEDURE `validate_ugc_request`(IN reqId INT, IN roleVal INT, OUT returnCode INT, OUT returnMsg VARCHAR(1000), OUT returnMsgDetail VARCHAR(2000), OUT rulesFound INT, OUT price INT)
BEGIN

	DECLARE userId INT DEFAULT NULL;
	DECLARE ugcUsage INT DEFAULT NULL;

	-- initialize return values
	SET rulesFound = 0;
	SET price = 999999999;
	SET returnMsg = '';
	SET returnMsgDetail = '';
	SET returnCode = 0;			-- default return code is SUCCEEDED
	
	-- get request information
	SELECT kaneva_user_id, ugc_usage_id 
		INTO userId, ugcUsage
		FROM ugc_validation_requests
		WHERE request_id = reqId;

	IF userId IS NOT NULL AND ugcUsage IS NOT NULL THEN

		-- request exists, continue to validate

		BEGIN
			DECLARE done INT DEFAULT 0;
			
			DECLARE passId INT DEFAULT -1;
			DECLARE tierId INT DEFAULT -1;
			DECLARE tierType CHAR(1) DEFAULT '';
			DECLARE basePrice INT DEFAULT 0;
			DECLARE maxPrice INT DEFAULT 999999999;
			DECLARE upchargeType CHAR(3) DEFAULT '';
			DECLARE tierDescMatched VARCHAR(200) DEFAULT '';
			DECLARE tierDescNoMatch VARCHAR(200) DEFAULT '';
			DECLARE propLevel INT DEFAULT 0;
			DECLARE propId INT DEFAULT 0;
			DECLARE minValue DECIMAL(20,6) DEFAULT -1;
			DECLARE aMaxValue DECIMAL(20,6) DEFAULT -1;
			DECLARE conn CHAR(5) DEFAULT '';
			DECLARE ruleDescValid VARCHAR(200) DEFAULT '';
			DECLARE ruleDescFailed VARCHAR(200) DEFAULT '';
			DECLARE propValue DECIMAL(20,6) DEFAULT NULL;
			DECLARE unit INT DEFAULT 1;
			DECLARE roundDown INT DEFAULT 0;
			DECLARE unitUpcharge DECIMAL(11,2) DEFAULT 0;
			DECLARE upchargeDesc VARCHAR(200) DEFAULT '';
			DECLARE propInstId INT DEFAULT -1;
			
			DECLARE nextPassId INT DEFAULT -1;
			DECLARE nextTierId INT DEFAULT -1;
			DECLARE nextTierType CHAR(1) DEFAULT '';
			DECLARE nextBasePrice INT DEFAULT 0;
			DECLARE nextMaxPrice INT DEFAULT 999999999;
			DECLARE nextUpchargeType CHAR(3) DEFAULT '';
			DECLARE nextTierDescMatched VARCHAR(200) DEFAULT '';
			DECLARE nextTierDescNoMatch VARCHAR(200) DEFAULT '';
			DECLARE nextPropLevel INT DEFAULT 0;
			DECLARE nextPropId INT DEFAULT 0;
			DECLARE nextMinValue DECIMAL(20,6) DEFAULT -1;
			DECLARE nextMaxValue DECIMAL(20,6) DEFAULT -1;
			DECLARE nextConn CHAR(5) DEFAULT '';
			DECLARE nextRuleDescValid VARCHAR(200) DEFAULT '';
			DECLARE nextRuleDescFailed VARCHAR(200) DEFAULT '';
			DECLARE nextPropValue DECIMAL(20,6) DEFAULT NULL;
			DECLARE nextUnit INT DEFAULT 1;
			DECLARE nextRoundDown INT DEFAULT 0;
			DECLARE nextUnitUpcharge DECIMAL(11,2) DEFAULT 0;
			DECLARE nextUpchargeDesc VARCHAR(200) DEFAULT '';
			DECLARE nextPropInstId INT DEFAULT -1;

			DECLARE priceId INT DEFAULT 0;

			DECLARE currUpcharge DECIMAL(11,2) DEFAULT 0;
			DECLARE tierUpcharge DECIMAL(11,2) DEFAULT 0;
			DECLARE tierPrice INT(11) DEFAULT 0;
			DECLARE currResult BOOL DEFAULT FALSE;
			
			DECLARE skipCurrTier BOOL DEFAULT FALSE;
			DECLARE skipCurrPropInstId BOOL DEFAULT FALSE;
			
			DECLARE strPrices VARCHAR(200) DEFAULT '';
			DECLARE currPrice INT(11) DEFAULT 0;
			DECLARE loopIdx INT DEFAULT 0;
			DECLARE tmpMsg VARCHAR(200) DEFAULT '';
			
			DECLARE tierRuleMsg VARCHAR(1000) DEFAULT '';
			DECLARE entryStr VARCHAR(1000) DEFAULT '';
			
			DECLARE curRules CURSOR FOR 

				SELECT floor(t.tier_id/1000), t.tier_id, t.tier_type, t.base_price, t.max_price, t.upcharge_type, t.desc_matched, t.desc_nomatch, 
					   r.property_level_id, r.property_id, r.min_value, r.max_value, r.connector, r.desc_valid, r.desc_failed, 
					   IFNULL(d.property_value,''), IFNULL(u.unit, 1), IFNULL(u.rounddown,0), IFNULL(u.unit_upcharge, 0), u.description, 
					   IFNULL(d.property_level_id,0)&0xFFFFFF00 as prop_inst_id

					FROM ugc_price_tiers t 

					INNER JOIN ugc_validation_server_rules r 
						ON t.kaneva_role_value=r.kaneva_role_value 
						AND t.ugc_usage_id=r.ugc_usage_id 
						AND t.tier_id=r.tier_id

					LEFT OUTER JOIN ugc_validation_request_details d
							ON r.property_level_id=(d.property_level_id & 255)
							AND r.property_id=d.property_id
							AND r.property_instance_id_start<=IFNULL(d.property_level_id,0)&0xFFFFFF00
							AND IFNULL(d.property_level_id,0)&0xFFFFFF00<=r.property_instance_id_end

					LEFT OUTER JOIN ugc_price_tier_upcharges u
							ON t.kaneva_role_value=u.kaneva_role_value
							AND t.ugc_usage_id=u.ugc_usage_id
							AND t.tier_id=u.tier_id
							AND r.property_level_id=u.property_level_id
							AND r.property_id=u.property_id
							AND u.property_instance_id_start<=IFNULL(d.property_level_id,0)&0xFFFFFF00
							AND IFNULL(d.property_level_id,0)&0xFFFFFF00<=u.property_instance_id_end

					WHERE t.kaneva_role_value = roleVal 
					  AND t.ugc_usage_id = ugcUsage
					  AND t.tier_type<>'X'
					  AND d.request_id = reqId
					ORDER BY floor(r.tier_id/1000), prop_inst_id, r.tier_id, r.rule_order;

			DECLARE curResult CURSOR FOR
					SELECT succeeded, message, tier_price, hit_count
						FROM _TMP_PRICING_LOG 
						WHERE request_id = reqId
						ORDER by tier_id;

			DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
			
			-- initialize all price components (#0 ~ #9) to zeros
			SET loopIdx = 0;
			WHILE loopIdx < 10 DO
				SET strPrices = CONCAT( strPrices, '00000000000|' );
				SET loopIdx = loopIdx + 1;
			END WHILE;
			
			-- create temporary table
			CREATE TEMPORARY TABLE IF NOT EXISTS _TMP_PRICING_LOG(
				request_id		int(11)			NOT NULL,					-- request ID to allow records of multiple request to coexist in the same table
				tier_id			int(5)			NOT NULL,					-- price tier this validation rule applies to
				succeeded		smallint(1)		NOT NULL,
				message			varchar(1000)	NOT NULL,
				tier_price		int(11),
				hit_count		int(5),
				PRIMARY KEY(request_id, tier_id, succeeded)
			);
			
			OPEN curRules;
				
			FETCH curRules into nextPassId, nextTierId, nextTierType, nextBasePrice, nextMaxPrice, nextUpchargeType, nextTierDescMatched, nextTierDescNoMatch, 
								nextPropLevel, nextPropId, nextMinValue, nextMaxValue, nextConn, nextRuleDescValid, nextRuleDescFailed,
								nextPropValue, nextUnit, nextRoundDown, nextUnitUpcharge, nextUpchargeDesc, nextPropInstId;

loop_rules:	REPEAT

				IF done THEN 
					LEAVE loop_rules;
				END IF;
				
				-- At least one rule found with matching role and usage
				SET rulesFound = 1;
				
				-- DEBUG OUTPUTS
				-- IF passId<>nextPassId THEN
				-- 	SET returnMsgDetail = concat( returnMsgDetail, "*** Pass ", floor(nextTierId/1000), " ***\r\n" );
				-- END IF;

				SELECT nextPassId, nextTierId, nextTierType, nextBasePrice, nextMaxPrice, nextUpchargeType, nextTierDescMatched, nextTierDescNoMatch, 
					   nextPropLevel, nextPropId, nextMinValue, nextMaxValue, nextConn, nextRuleDescValid, nextRuleDescFailed,
					   nextPropValue, nextUnit, nextRoundDown, nextUnitUpcharge, nextUpchargeDesc, nextPropInstId
				INTO passId, tierId, tierType, basePrice, maxPrice, upchargeType, tierDescMatched, tierDescNoMatch, 
					 propLevel, propId, minValue, aMaxValue, conn, ruleDescValid, ruleDescFailed, 
					 propValue, unit, roundDown, unitUpcharge, upchargeDesc, propInstId;
				
				SET priceId = floor( tierId % 1000 / 100 );	-- price component Id: 0~9 (e.g. 0 for base price, 1 for texture charge, etc. Price components will be sum once validation is completed)

				-- DEBUG OUTPUTS
				-- SELECT passId, tierId, tierType, basePrice, maxPrice, upchargeType, propLevel, propId, minValue, aMaxValue, conn, propValue, unitUpcharge;
			
				IF NOT skipCurrPropInstId AND NOT skipCurrTier THEN

					SET currResult = FALSE;
					IF propValue<>'' THEN
						SET currResult = ceil(propValue) > minValue AND ceil(propValue) <= aMaxValue;
					END IF;
					
					IF currResult THEN

						IF ruleDescValid<>'' THEN	
							SET ruleDescValid = replace( ruleDescValid, '{min_value}', format(minValue, 0) );
							SET ruleDescValid = replace( ruleDescValid, '{max_value}', format(aMaxValue, 0) );
							SET ruleDescValid = replace( ruleDescValid, '{property_value}', format(ceil(propValue), 0) );
							SET ruleDescValid = replace( ruleDescValid, '{diff_value}', format(ceil(propValue) - minValue, 0) );
							SET ruleDescValid = replace( ruleDescValid, '{current_message}', tierRuleMsg );
							SET tierRuleMsg = ruleDescValid;
						END IF;
					
						SET currUpcharge = 0;
						IF roundDown=0 THEN
							SET currUpcharge = unitUpcharge * (ceil((propValue - minValue) / unit));
						ELSE
							SET currUpcharge = unitUpcharge * (floor((propValue - minValue) / unit));
						END IF;
						
						IF currUpcharge < 0 THEN
							SET currUpcharge = 0;
						END IF;
						IF upchargeType='MIN' THEN
							SET tierUpcharge = IF( currUpcharge < tierUpcharge, currUpcharge, tierUpcharge );
						ELSEIF upchargeType='MAX' THEN
							SET tierUpcharge = IF( currUpcharge > tierUpcharge, currUpcharge, tierUpcharge );
						ELSEIF upchargeType='SUM' THEN
							SET tierUpcharge = tierUpcharge + currUpcharge;
						END IF;

						IF tierUpcharge>0 AND upchargeDesc<>'' THEN
							SET upchargeDesc = replace( upchargeDesc, '{unit_upcharge}', format(unitUpcharge, 0) );
							SET upchargeDesc = replace( upchargeDesc, '{tier_upcharge}', format(tierUpcharge, 0) );
						END IF;

						SET tierPrice = basePrice + tierUpcharge;
						SET tierPrice = IF( tierPrice < maxPrice, tierPrice, maxPrice );
						
					ELSE
					
						IF ruleDescFailed<>'' THEN
							SET ruleDescFailed = replace( ruleDescFailed, '{min_value}', format(minValue, 0) );
							SET ruleDescFailed = replace( ruleDescFailed, '{max_value}', format(aMaxValue, 0) );
							SET ruleDescFailed = replace( ruleDescFailed, '{property_value}', format(ceil(propValue), 0) );
							SET ruleDescFailed = replace( ruleDescFailed, '{diff_value}', format(ceil(propValue) - minValue, 0) );
							SET ruleDescFailed = replace( ruleDescFailed, '{current_message}', tierRuleMsg );
							SET tierRuleMsg = ruleDescFailed;
						END IF;
						
					END IF;
					
					-- check if we want to go thru more records in this tier
					IF conn='AND' AND NOT currResult OR conn='OR' AND currResult THEN
						-- shortcut remaining rules in current tier
						SET skipCurrTier = TRUE;
					END IF;

				END IF;

				-- -----------------------------------------------------------------
				-- Fetch and preview next record
				-- -----------------------------------------------------------------
				
				FETCH curRules into nextPassId, nextTierId, nextTierType, nextBasePrice, nextMaxPrice, nextUpchargeType, nextTierDescMatched, nextTierDescNoMatch, 
									nextPropLevel, nextPropId, nextMinValue, nextMaxValue, nextConn, nextRuleDescValid, nextRuleDescFailed,
									nextPropValue, nextUnit, nextRoundDown, nextUnitUpcharge, nextUpchargeDesc, nextPropInstId;

				IF done OR NOT skipCurrPropInstId AND tierId<>nextTierId THEN
					-- reset skipCurrTier flag
					SET skipCurrTier = FALSE;

					-- end-of-price-tier processing
					SET tmpMsg = '';
					IF currResult AND tierDescMatched<>'' THEN
						SET tmpMsg = tierDescMatched;
					ELSEIF NOT currResult AND tierDescNoMatch<>'' THEN
						SET tmpMsg = tierDescNoMatch;
					END IF;

					IF tmpMsg<>'' THEN
						SET tmpMsg = replace( tmpMsg, '{base_price}', format(basePrice, 0) );
						SET tmpMsg = replace( tmpMsg, '{max_price}', format(maxPrice, 0) );
						SET tmpMsg = replace( tmpMsg, '{tier_price}', format(tierPrice, 0) );
						
						IF currResult AND returnMsg='' THEN
							SET returnMsg = concat( returnMsg, tmpMsg, '\r\n' );
						END IF;
					END IF;
					
					IF NOT currResult AND tierType='V' THEN
						-- validation failed
						SET returnCode = 1;
						
						-- clear all pricing records
						DELETE FROM _TMP_PRICING_LOG WHERE request_id=reqId;
						
						-- log validation record
						INSERT INTO _TMP_PRICING_LOG(request_id, tier_id, succeeded, message, tier_price, hit_count)
							VALUES ( reqId, tierId, 0, tierRuleMsg, 999999999, 1 )
							ON DUPLICATE KEY UPDATE hit_count = hit_count + 1;
						
						SET returnMsg = concat( returnMsg, tierRuleMsg );
						LEAVE loop_rules;

					ELSEIF currResult THEN
					
						-- extract current price component from array
						SET currPrice = Convert( substring( strPrices, priceId*12+1, 11 ), SIGNED );
						
						-- DEBUG OUTPUTS
						-- SELECT currPrice as getprice;
					
						IF tierType='V' THEN
							-- validation succeeded: make sure current price component fall into range
							IF currPrice < basePrice THEN
								SET currPrice = basePrice;
								-- DEBUG OUTPUTS
								-- SELECT currPrice as clamped_at_lower_bound;
							ELSEIF currPrice > maxPrice THEN
								SET currPrice = maxPrice;
								-- DEBUG OUTPUTS
								-- SELECT currPrice as clamped_at_upper_bound;
							END IF;

						ELSEIF tierType='P' or tierType='D' THEN
							-- tier rules matched
					
							-- DEBUG OUTPUTS
							-- SELECT currPrice, tierPrice;
							
							-- Add tier price to corresponding price component based on priceId
							SET currPrice = currPrice + tierPrice;
							IF currPrice < 0 THEN
								SET tierPrice = tierPrice - currPrice;
								SET currPrice = 0;	-- total price shouldn't go beyond zero at any time point (make sure discount is always applied at the end)
							END IF;
						
							-- log pricing record
							INSERT INTO _TMP_PRICING_LOG(request_id, tier_id, succeeded, message, tier_price, hit_count)
								VALUES ( reqId, tierId, 1, tierRuleMsg, tierPrice, 1 )
								ON DUPLICATE KEY UPDATE hit_count = hit_count + 1, tier_price = tier_price + tierPrice;
						
							-- current pass completed, proceed to next pass
							SET skipCurrPropInstId = TRUE;

						ELSEIF tierType='M' THEN
						
							-- dump a message into pricing log
							INSERT IGNORE INTO _TMP_PRICING_LOG(request_id, tier_id, succeeded, message, tier_price, hit_count)
								VALUES ( reqId, tierId, 1, tierRuleMsg, NULL, NULL );
						
						END IF;

						-- now store current price component to array
						SET strPrices = concat( substring( strPrices, 1, priceId*12 ), LPad( currPrice, 11, '0' ), '|', substring( strPrices, (priceId+1)*12+1 ) );
					END IF;

					-- Reset data and get ready for next price tier
					SET currResult = FALSE;
					SET tierUpcharge = 0;
					SET tierPrice = 0;
					SET tierRuleMsg = '';

				END IF;
				
				IF done OR passId<>nextPassId OR propInstId<>nextPropInstId THEN
					-- reset skipCurrPropInstId flag
					SET skipCurrPropInstId = FALSE;
				END IF;

			UNTIL done END REPEAT loop_rules;

			CLOSE curRules;

			IF returnCode=0 THEN
				-- validation passed, now sum all price components
				SET price = 0;
				SET loopIdx = 0;
				WHILE loopIdx < 10 DO
					SET currPrice = Convert( substring( strPrices, loopIdx*12+1, 11 ), SIGNED );
					-- DEBUG OUTPUTS
					-- SELECT currPrice as price_comp;
					SET price = price + currPrice;
					SET loopIdx = loopIdx + 1;
				END WHILE;
			END IF;
			
			SET returnMsg = replace( returnMsg, '{total_price}', format(price, 0) );
			
			-- DEBUG
			-- SELECT CONCAT('<Entry>', '<Label>', label, '</Label>', '<Message>', IF(hit_count>1, CONCAT(message, ' x ', hit_count), message), '</Message>', '<Credits>', tier_price, '</Credits>', '</Entry>') as entry FROM _TMP_PRICING_LOG order by tier_id;
			-- UPDATE _TMP_PRICING_LOG SET label = replace( label, '{hit_count}', hit_count ), message = replace( message, '{hit_count}', hit_count );
	
			SET returnMsgDetail = '';
			SET done = 0;
			OPEN curResult;

			BEGIN
				DECLARE succeeded INT DEFAULT 0;
				DECLARE message VARCHAR(1000) DEFAULT "";
				DECLARE tier_price INT DEFAULT 999999999;
				DECLARE hit_count INT DEFAULT 0;
loop_entries:	REPEAT
					FETCH curResult into succeeded, message, tier_price, hit_count;
					IF NOT done THEN
						SET message = replace( message, '{occurrence}', IFNULL(hit_count, ''));
						SET message = replace( message, '{credits}', IFNULL(tier_price, ''));
						SET message = replace( message, '{total}', price );
						SET returnMsgDetail = CONCAT(returnMsgDetail, '<Entry><Message>', message, '</Message><Occurrence>', IFNULL(hit_count, ''), '</Occurrence><Credits>', IFNULL(tier_price, ''), '</Credits></Entry>\r\n' );
					END IF;
				UNTIL done END REPEAT loop_entries;
			END;

			-- clear all records			
			DELETE FROM _TMP_PRICING_LOG WHERE request_id=reqId;
		END;

	ELSE

		-- invalid request ID, return error
		SET returnCode = 2;
		SET returnMsg = 'Invalid request ID';
		SET returnMsgDetail = returnMsg;

	END IF;
	
	DROP TEMPORARY TABLE IF EXISTS _TMP_PRICING_LOG;
	
END
;;
DELIMITER ;