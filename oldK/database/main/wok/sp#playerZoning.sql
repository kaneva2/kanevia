-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS playerZoning;

DELIMITER ;;
CREATE PROCEDURE `playerZoning`( IN playerId INT(11), IN serverId INT(11), IN zoneIndex INT(11), IN zoneInstanceId INT(11), IN currentZone VARCHAR(80) )
BEGIN
	DECLARE oldServerId INT(11);
	DECLARE oldZoneIndex INT(11);
	DECLARE oldInstanceId INT(11);
	DECLARE kanevaUserId INT(11);
	DECLARE rowCount INT DEFAULT 0;

	SELECT pz.server_id, pz.current_zone_index, pz.current_zone_instance_id, p.kaneva_user_id INTO oldServerId, oldZoneIndex, oldInstanceId, kanevaUserId
	FROM players p
	LEFT OUTER JOIN player_zones pz ON p.player_id = pz.player_id
	WHERE p.player_id = playerId
	FOR UPDATE;

	IF oldServerId IS NULL THEN 
		INSERT INTO player_zones (	player_id, server_id, current_zone_index, current_zone_type, current_zone_instance_id, current_zone )
			              VALUES ( playerId,  serverId,  zoneIndex, zoneType(zoneIndex), zoneInstanceId, currentZone );
   		UPDATE players 
   		   SET server_id = serverId
   		 WHERE player_id = playerId;

		-- If there was no existing player_zones record, there would be no existing visit_log record. Go ahead and insert.
		CALL developer.record_visit( kanevaUserId, zoneIndex, zoneInstanceId, NULL, NULL, NULL, NULL );
	ELSEIF oldServerId <> serverId OR oldZoneIndex <> zoneIndex OR oldInstanceId <> zoneInstanceId THEN
		UPDATE player_zones
		  SET server_id = serverId, current_zone_index = zoneIndex, current_zone_type = zoneType(zoneIndex), current_zone_instance_id = zoneInstanceId, current_zone  = currentZone
		 WHERE player_id = playerId;

		IF oldServerId = 0 THEN
			-- If oldServerId = 0, we're either coming from a 3dApp or initially logging in.
			-- In either case, we have no old record to update. Just insert.
			CALL developer.record_visit( kanevaUserId, zoneIndex, zoneInstanceId, NULL, NULL, NULL, NULL );
		ELSEIF oldZoneIndex <> zoneIndex OR oldInstanceId <> zoneInstanceId THEN
			-- If either zoneIndex or zoneInstanceId is different, we need to update the old record
			CALL developer.record_visit( kanevaUserId, zoneIndex, zoneInstanceId, NULL, oldZoneIndex, oldInstanceId, NULL );
		END IF;
	END IF;
	
	
	
	  
	
	UPDATE channel_zones
	   SET tied_to_server = 'TIED'
	 WHERE zone_index = zoneIndex
 	  AND zone_instance_id = zoneInstanceId
 	  AND tied_to_server = 'PENDING_TIED';
 	  
   	
END
;;
DELIMITER ;