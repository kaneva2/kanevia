-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS defaultBroadbandZone;

DELIMITER ;;
CREATE FUNCTION `defaultBroadbandZone`() RETURNS int(11)
    DETERMINISTIC
BEGIN
-- sp to return the default broadband channel so it can be changed easily
        return 0x60000000 | defaultPlainBroadbandZone(); 
END
;;
DELIMITER ;