-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS getFriendlyApartmentName;

DELIMITER ;;
CREATE FUNCTION `getFriendlyApartmentName`( userName VARCHAR(64), zoneIndex INT ) RETURNS varchar(128) CHARSET latin1
    READS SQL DATA
BEGIN
   DECLARE friendlyZoneName VARCHAR(64) DEFAULT NULL;
     SELECT sw.display_name INTO friendlyZoneName
        FROM supported_worlds sw
        WHERE sw.zone_index=zoneIndex(zoneIndex);
    IF friendlyZoneName IS NOT NULL THEN
       RETURN concat(userName, ' - ', friendlyZoneName);
     ELSE 
     	CALL logMsg( 'getFriendlyHangoutName', 'WARN', concat('Failed to get apartment name for ', userName, ' ', hex(zoneIndex) ) );
     	RETURN concat( userName, '''s Apartment');
    END IF;
END
;;
DELIMITER ;