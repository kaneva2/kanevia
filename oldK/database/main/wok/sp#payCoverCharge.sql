-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS payCoverCharge;

DELIMITER ;;
CREATE PROCEDURE `payCoverCharge`( 
				IN kanevaUserId INT(11),
				IN ownerKanevaUserId INT(11),
				IN coverCharge INT(11),
				IN zoneIndex INT(11), 
				IN instanceId INT(11),
				OUT returnCode INT,
				OUT user1Balance FLOAT,
				OUT user2Balance FLOAT)
BEGIN
-- see if this player has pd cover charge, and if not return non-zero value
	DECLARE tranId INT DEFAULT 0;
	DECLARE netPaid INT DEFAULT 0;
	DECLARE commission INT DEFAULT 0;
	DECLARE commissionBalance FLOAT DEFAULT 0;

	SET returnCode = -1;

	-- for apply_transaction_to_user_balance
	-- 1 indicates the transaction is invalid due to insufficient funds
	-- 0 indicates the transaction was valid and recorded
	-- -1 indicates the transaction was invalid due to an error

	-- remove from payer
	CALL apply_transaction_to_user_balance( kanevaUserId, -coverCharge, 
							16, -- TT_COVER_CHARGE 
							"KPOINT", returnCode, user1Balance, tranId );
	IF returnCode = 0 THEN 

		SET commission = ROUND(coverCharge * 0.1);
		SET netPaid = coverCharge - commission;
		
		-- add to payee
		CALL apply_transaction_to_user_balance( ownerKanevaUserId, netPaid, 
							16, -- TT_COVER_CHARGE 
							"KPOINT", returnCode, user2Balance, tranId );

		IF returnCode = 0 THEN
			-- update commision in tran log
			CALL apply_transaction_to_user_balance( 1, commission, 
								17, -- TT_COMMISSION 
								"KPOINT", returnCode, commissionBalance, tranId );

			UPDATE vw_user_balances
			   SET balance = balance + commission
			 WHERE user_id = 1 -- special id for "Kaneva"
			   AND kei_point_id = 'KPOINT';
			   
			IF returnCode = 0 THEN
				INSERT INTO channel_zone_cover_charges
							(kaneva_user_id, zone_index, zone_instance_id, expiration_date, created_date) 
				VALUES (kanevaUserId, zoneIndex, instanceId, DATE_ADD(NOW(), INTERVAL 24 HOUR), NOW() )
				ON DUPLICATE KEY UPDATE  expiration_date =  DATE_ADD(NOW(), INTERVAL 24 HOUR), created_date = NOW();
			END IF;
		END IF;
	END IF;
	
END
;;
DELIMITER ;