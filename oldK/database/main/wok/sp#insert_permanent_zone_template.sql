-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS insert_permanent_zone_template;

DELIMITER ;;
CREATE PROCEDURE `insert_permanent_zone_template`( IN __zone_index_plain INT(11),	IN __visibility INT(11), IN __name VARCHAR(200) )
BEGIN
-- called to insert a new zone into channel_zones table 

      INSERT INTO channel_zones
            (kaneva_user_id,
            zone_index,
            zone_index_plain,
            zone_type,
            zone_instance_id,
            name,
            tied_to_server,
            created_date,
            raves,
            access_rights,
            access_friend_group_id,
            server_id,
            cover_charge,
            visibility,
            last_update, 
            number_unique_visits
            )
            VALUES
            (     0,
                  make_zone_index(__zone_index_plain, 4), 
                  __zone_index_plain,
                  4,
                  1, 
                  __name,
                  'NOT_TIED',
                  now(),
                  0,
                  0,
                  0,
                  0,
                  0,
                  __visibility,
                  now(),
                  0
            );

END
;;
DELIMITER ;