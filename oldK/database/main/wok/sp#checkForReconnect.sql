-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS checkForReconnect;

DELIMITER ;;
CREATE PROCEDURE `checkForReconnect`( IN __sourceKanevaId INT, 
										   INOUT __isGM CHAR, INOUT __playerId INT(11), OUT __reasonCode INT(11), 
											 OUT __server VARCHAR(64), OUT __serverPort INT(11) )
BEGIN
	-- WOK-specific SP!! only called from SPs that are only called from C#
	DECLARE __server_id INT(11) DEFAULT 0;
	DECLARE __logged_on CHAR DEFAULT 'F';
	DECLARE __game_id INT DEFAULT 0;

	SELECT is_gm, COALESCE(p.player_id,0), gu.server_id, gu.logged_on INTO __isGm, __playerId, __server_id, __logged_on
		FROM game_users gu LEFT OUTER JOIN players p ON gu.user_id = p.user_id
	 	WHERE gu.kaneva_user_id = __sourceKanevaId;
	 
	IF __logged_on = 'T' THEN  

	 	-- if already logged on to non-wok server, don't tell them to reconnect to it
	 	
		SELECT ip_address, port, game_id INTO __server, __serverPort, __game_id
			FROM developer.game_servers 
			WHERE server_id = __server_id;

		IF __server IS NOT NULL AND __serverPort IS NOT NULL THEN		
			
			IF is_wok_game( __game_id ) THEN 
		
				SET __reasonCode = 1;	-- Reconnecting and found the info
				CALL logDebugMsg( 'checkForReconnect', concat( 'reasonCode: ', cast( IFNULL(__reasonCode,0) as char ), 
													' server: ', cast( IFNULL(__server,0) as char ), ' port: ', 
													cast( IFNULL(__serverPort,0) as char ) ) );
			
			ELSE
				SET __server_id = 0; -- not a WOK server
				SET __reasonCode = 0; -- coming from another server, no reconnect ck
			END IF;		
		ELSE
			
			SET __reasonCode = 2; -- Reconnecting cannot find the info
			CALL logDebugMsg( 'checkForReconnect', concat( 'reasonCode: ', cast( IFNULL(__reasonCode,0) as char ) ) );
		
		END IF;
	
	ELSE
	
		SET __reasonCode = 0; -- Not reconnect just return normal values
		CALL logDebugMsg( 'checkForReconnect', concat( 'reasonCode: ', cast( IFNULL(__reasonCode,0) as char ) ) );
		
	END IF;
		
END
;;
DELIMITER ;