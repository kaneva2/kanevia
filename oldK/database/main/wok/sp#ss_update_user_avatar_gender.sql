-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_user_avatar_gender;

DELIMITER ;;
CREATE PROCEDURE `ss_update_user_avatar_gender`(  )
begin
  replace into snapshot_user_avatar_gender
    select count(distinct wll.user_id) as player_count,gender,date(max(wll.created_date)) as max_date
    from developer.login_log wll
    inner join game_users gu on wll.user_id=gu.user_id
    inner join kaneva.users ku on gu.kaneva_user_id=ku.user_id
    inner join players p on gu.user_id=p.user_id
    WHERE wll.created_date BETWEEN CONCAT( DATE(NOW()) , ' 00:00:00') AND CONCAT( DATE(NOW()) , ' 23:59:59')
    group by date(wll.created_date),gender ;
end
;;
DELIMITER ;