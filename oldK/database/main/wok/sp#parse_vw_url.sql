-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS parse_vw_url;

DELIMITER ;;
CREATE PROCEDURE `parse_vw_url`( IN __url VARCHAR(300), OUT __instance_name VARCHAR(100), 
                               OUT __type INT, OUT __name_location VARCHAR(100), 
							   OUT __instance_id INT,
							   OUT __game_name VARCHAR(100) )
BEGIN
	-- parse out the kaneva url
	-- format is kaneva://game/instance.type[?n=namedloc]
	
	DECLARE __slashPos INT;
	DECLARE __thirdSlashPos INT;
	DECLARE __proto_end INT;
	DECLARE __dotPos INT;
	DECLARE __eqPos INT;
	DECLARE __lbPos INT;
	DECLARE __secondDotPos INT;
	DECLARE __typeAsStr VARCHAR(100);
	
	SET __instance_id = 0; -- for admins can goto mall.zone.1

	-- possible values
	-- kaneva://game123 456/seekatar.people.1
	-- kaneva://wok/mall.zone.2#namedloc
	-- kaneva://World of Kaneva 2.0
	-- kaneva://World of Kaneva 2.0/peekatar.home.3
	-- kaneva://Cardboard World/gothere.channel.5

	-- high-level check
	IF locate(concat(vw_url_prefix(), '://'),__url) = 1 THEN 
	
		SET __proto_end = LENGTH(concat(vw_url_prefix(), '://'))+1;
		SET __slashPos = LOCATE("/", __url, __proto_end );
		SET __dotPos = LOCATE( '.', __url, __slashPos );
		
		IF LENGTH(__url) > __proto_end THEN 
		
			IF __slashPos > 0 AND __dotPos > __slashPos THEN 
			
				SET __instance_name = SUBSTRING( __url, __slashPos+1, (__dotPos-__slashPos)-1 );

				SET __game_name = SUBSTRING( __url, __proto_end, (__slashPos-__proto_end));
				
				SET __lbPos = locate( '#', __url, __dotPos );

				IF __lbPos > 0 THEN 			
					SET __typeAsStr = SUBSTRING( __url, __dotPos+1, (__lbPos-__dotPos)-1 );
					
					SET __name_location = SUBSTRING( __url, __lbPos+1  );
				ELSE
					SET __typeAsStr = SUBSTRING( __url, __dotPos+1 );
				END IF;

				SET __secondDotPos = locate('.',__typeAsStr);
				IF __secondDotPos > 0 THEN
					-- admin going to perm zone instance
					IF LEFT(__typeAsStr,__secondDotPos-1) IN ('zone','arena') THEN
						SET __instance_id = CONVERT( MID(__typeAsStr,__secondDotPos+1), SIGNED );
					END IF;

					-- tolerate if passed in for invalid zones.
					SET __typeAsStr = LEFT(__typeAsStr,__secondDotPos-1);

				END IF;
				
				SET __type = CASE __typeAsStr WHEN 'home' THEN 3 WHEN 'zone' THEN 4 WHEN 'arena' THEN 5 WHEN 'channel' THEN 6 WHEN 'people' THEN 99 ELSE 0 END;
				IF __type NOT IN (4,5) AND __instance_id <> 0 THEN 
					SET __instance_id = 0; -- only valid for zone, arena
				END IF;

				IF __type = 5 AND __instance_id = 0 THEN -- must have instance id for arenas, if not error out
					SET __instance_name = NULL; 
					SET __type = NULL;
				END IF;
				
			ELSEIF __slashPos = 0 THEN
				SET __game_name = SUBSTRING( __url, __proto_end );
			END IF;
		END IF;
	END IF;	
END
;;
DELIMITER ;