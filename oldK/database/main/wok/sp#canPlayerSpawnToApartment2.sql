-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS canPlayerSpawnToApartment2;

DELIMITER ;;
CREATE PROCEDURE `canPlayerSpawnToApartment2`( IN sourceKanevaId INT, 
				IN destKanevaId INT, 
			    IN country CHAR(2),
			    IN birthDate DATE,
			    IN showMature BINARY(1),
				OUT reasonCode INT, OUT server VARCHAR(64), OUT serverPort INT(11), OUT housingZoneIndex INT(11), OUT serverId INT(11) )
BEGIN
	DECLARE playerId INT(11);
	DECLARE isGm CHAR DEFAULT 'F';
	DECLARE sourcePlayerId INT DEFAULT 0;
	DECLARE unused VARCHAR(80);
	DECLARE reconnCode INT DEFAULT 0;
	DECLARE unusedScriptId INT(11);

    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;

	SET serverId = 0;
	SET reasonCode = 10; 
	CALL checkForReconnect( sourceKanevaId, isGM, playerId, reconnCode, server, serverPort );
	
	IF reconnCode = 1 THEN
	
		SELECT pp.`housing_zone_index`, p.`player_id`
		  INTO housingzoneindex, playerid
		  FROM `players` p
				 INNER JOIN `player_presences` pp ON p.player_id = pp.player_id
		 WHERE p.kaneva_user_id = destkanevaid
		 LIMIT 1;
	
		SET reasonCode = 0;
	
		CALL logDebugMsg( 'canPlayerSpawnToApartment2', concat( 'housingzoneindex: ', cast( IFNULL(housingzoneindex,0) as char ), 
									' playerid: ', cast( IFNULL(playerid,0) as char ) ) );
	
	ELSEIF reconnCode = 0 THEN
	
		START TRANSACTION;
			CALL can_player_spawn_to_apartment_common( sourcePlayerId, sourceKanevaId, destKanevaId, 0, isGm, country, birthDate, showMature, playerId, housingZoneIndex, reasonCode, serverId, unusedScriptId, unused );
	
		IF serverId IS NOT NULL AND serverId <> 0 THEN 
			SELECT ip_address, port INTO server, serverPort
			  FROM vw_game_servers
		     WHERE server_id = serverId;
		END IF;
		
		IF serverId IS NULL OR serverId = 0 THEN 
			SET server = '';
			SET serverPort = 0;
		END IF;	

		COMMIT;
	
	END IF;
	
END
;;
DELIMITER ;