-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS get_game_priv;

DELIMITER ;;
CREATE FUNCTION `get_game_priv`( __game_id INT, __kaneva_user_id INT ) RETURNS int(11)
    READS SQL DATA
BEGIN
	-- returns kaneva.community_account_type values 1 = owner, 2 = moderator, 3 = member or 0 = nothing
	DECLARE __account_type INT DEFAULT NULL;

	IF wok.is_wok_game( __game_id ) THEN 
		-- for wok games, must be gm
		SELECT 	IF(is_gm = 'T',1,0) INTO __account_type 
			FROM wok.game_users
			WHERE kaneva_user_id = __kaneva_user_id;
	ELSE
		-- for other games, use the view
		SELECT 	account_type_id INTO __account_type 
			FROM developer.vw_game_privileges
			WHERE user_id = __kaneva_user_id
			 AND game_id = __game_id;
	END IF;			 

	IF __account_type IS NULL THEN 
		SET __account_type = 0;
	END IF;
	
	RETURN __account_type;
END
;;
DELIMITER ;