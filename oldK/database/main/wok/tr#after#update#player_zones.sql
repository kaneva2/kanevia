-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS after_player_zones_update;

DELIMITER ;;
CREATE TRIGGER `after_player_zones_update` AFTER UPDATE ON `player_zones` FOR EACH ROW BEGIN
-- trigger to update counts in summary table

	IF OLD.server_id <> 0 THEN 
		CALL zoningOut( old.current_zone_index, old.current_zone_instance_id, old.server_id );	
	END IF;
	IF NEW.server_id <> 0 THEN
		CALL zoningIn(	new.player_id, new.current_zone_index, new.current_zone_instance_id, new.server_id );
	END IF;

END
;;
DELIMITER ;