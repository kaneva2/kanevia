-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS decrementBalancerInputs;

DELIMITER ;;
CREATE PROCEDURE `decrementBalancerInputs`( IN __zoneIndex          INT
                                          , IN __zoneInstanceId   INT )
BEGIN
   
   UPDATE 
            balancer_inputs 
   SET      
            weight = if( weight > 1, weight - 1, 0 )
   WHERE 
            zone_type            = ZoneType(__zoneIndex)
            AND zone_index         = __zoneIndex
            AND zone_instance_id   = __zoneInstanceId;

   delete from balancer_inputs 
   WHERE 
            zone_type            = ZoneType(__zoneIndex)
            AND zone_index         = __zoneIndex
            AND zone_instance_id   = __zoneInstanceId
            AND weight = 0;
END
;;
DELIMITER ;