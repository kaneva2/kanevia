-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS get_zone_owner;

DELIMITER ;;
CREATE FUNCTION `get_zone_owner`( __zone_index INT, __instance_id INT ) RETURNS int(11)
    READS SQL DATA
BEGIN
	DECLARE __ret INT DEFAULT 0;
	
	-- returns the kaneva user id
	IF zoneType( __zone_index ) = 3 THEN 
		SELECT IFNULL( kaneva_user_id, 0 ) INTO __ret FROM players where player_id = __instance_id;
	ELSEIF zoneType( __zone_index ) = 6 THEN 
		-- WOK specific
		SELECT IFNULL( creator_id, 0 ) INTO __ret FROM kaneva.communities where community_id = __instance_id;
	END IF;
	
	RETURN __ret;
END
;;
DELIMITER ;