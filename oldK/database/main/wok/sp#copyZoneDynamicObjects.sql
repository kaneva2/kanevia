-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS copyZoneDynamicObjects;

DELIMITER ;;
CREATE PROCEDURE `copyZoneDynamicObjects`(IN _source_instance_id INT,
		IN _source_zone_index INT, IN _dest_instance_id INT,
		IN _dest_zone_index INT, IN _dest_player_id INT)
BEGIN
DECLARE __obj_placement_id INT;
DECLARE __new_obj_placement_id INT;
DECLARE done INT DEFAULT 0;
DECLARE i INT;
DECLARE dyo_cursor CURSOR FOR
                        SELECT obj_placement_id
                        FROM dynamic_objects
			WHERE zone_instance_id = _source_instance_id AND zone_index = _source_zone_index;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;
SET i = 0;
OPEN dyo_cursor;
REPEAT
    FETCH dyo_cursor INTO __obj_placement_id;
    IF NOT done THEN
        SET i=i+1;
        START TRANSACTION;
	SET __new_obj_placement_id = shard_info.get_dynamic_objects_id();
	INSERT INTO dynamic_objects ( obj_placement_id, zone_index, zone_instance_id, object_id, player_id, global_id,
				       position_x, position_y, position_z, rotation_x, rotation_y, rotation_z, 
				       created_date, inventory_sub_type)
		SELECT __new_obj_placement_id, _dest_zone_index, _dest_instance_id, object_id, _dest_player_id, global_id, 
			position_x, position_y, position_z,rotation_x, rotation_y, rotation_z,
			NOW(), inventory_sub_type
		FROM dynamic_objects
		WHERE obj_placement_id = __obj_placement_id;
	
	INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, `value`)
		SELECT __new_obj_placement_id, param_type_id, `value`
		FROM dynamic_object_parameters WHERE obj_placement_id = __obj_placement_id;
        COMMIT;
    END IF;
UNTIL done END REPEAT;
CLOSE dyo_cursor;
IF  wok.zoneIndex(_source_zone_index) = wok.zoneIndex(_dest_zone_index) THEN
	INSERT INTO world_object_player_settings (world_object_id, asset_id, texture_url, zone_index, zone_instance_id, last_updated_datetime )
		SELECT world_object_id, asset_id, texture_url, _dest_zone_index, _dest_instance_id, NOW()
		FROM world_object_player_settings 
		WHERE zone_index = _source_zone_index AND zone_instance_id = _source_instance_id;
END IF;
END
;;
DELIMITER ;