-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_players;

DELIMITER ;;
CREATE PROCEDURE `delete_players`( IN __kaneva_user_id INT, IN __player_id INT)
BEGIN
	DECLARE _housing_zone_index INT(11);
	
	DELETE FROM inventory_pending_adds2 WHERE kaneva_user_id = __kaneva_user_id;
	DELETE FROM pending_players WHERE kaneva_user_id = __kaneva_user_id;
	
	SELECT housing_zone_index INTO _housing_zone_index FROM player_presences WHERE player_id = __player_id;
	DELETE FROM player_presences WHERE player_id = __player_id;
	DELETE FROM player_zones WHERE player_id = __player_id;
	DELETE FROM world_object_player_settings WHERE zone_instance_id = __player_id AND zone_index = _housing_zone_index;
	DELETE FROM player_stats WHERE player_id = __player_id;
	DELETE FROM player_skills WHERE player_id = __player_id;
	DELETE FROM noterieties WHERE player_id = __player_id;
	DELETE FROM inventories WHERE player_id = __player_id;
	DELETE FROM getset_player_values WHERE player_id = __player_id;
	DELETE FROM dynamic_objects WHERE player_id = __player_id;
	DELETE FROM deformable_configs WHERE player_id = __player_id;
	DELETE FROM player_quests WHERE player_id = __player_id;
	DELETE FROM cod_items WHERE to_id = __player_id;
	DELETE FROM housing_placements WHERE player_id = __player_id;

	
	
	DELETE FROM channel_zones WHERE zone_type = 3 AND zone_instance_id = __player_id;

	
	DELETE FROM apartment_cache WHERE zone_instance_id = __player_id;
	DELETE FROM clans WHERE owner_id = __player_id;
	DELETE FROM clan_members WHERE player_id = __player_id;

	DELETE FROM players WHERE player_id = __player_id;
END
;;
DELIMITER ;