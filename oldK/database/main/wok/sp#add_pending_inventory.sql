-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_pending_inventory;

DELIMITER ;;
CREATE PROCEDURE `add_pending_inventory`(
	IN __kaneva_user_id INT, 
	IN __inventory_type CHAR, 
	IN __global_id INT, 
	IN __quantity INT, 
	IN __inventory_sub_type INT,
	IN __suppress_inventory_fetch BOOLEAN
)
    COMMENT '\n    '
this_proc:
BEGIN

  DECLARE __user_game_id INT DEFAULT 0;
  DECLARE __player_id INT DEFAULT 0;

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- ERROR
      GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
      CALL logging.logMsg('ERROR', SCHEMA(), 'add_pending_inventory', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
      RESIGNAL;
  END;


  CALL logging.logMsg('TRACE', SCHEMA(), 'add_pending_inventory', CONCAT('BEGIN (__kaneva_user_id[',CAST(__kaneva_user_id AS CHAR), '], __inventory_type[', __inventory_type, '] __global_id[',CAST(__global_id AS CHAR), '] __quantity[',CAST(__quantity AS CHAR), '] __inventory_sub_type[', CAST(__inventory_sub_type AS CHAR), '])'));

  -- Retrieve player_id
  SELECT `player_id` INTO __player_id FROM `players` WHERE `kaneva_user_id` = __kaneva_user_id;

  -- 1a) replenish inventory
  INSERT INTO wok.inventory_pending_adds2
    (`kaneva_user_id`, `global_id`, `inventory_type`, `inventory_sub_type`, `quantity`, `last_touch_datetime`) 
  VALUES 
    (__kaneva_user_id, __global_id, __inventory_type, __inventory_sub_type, __quantity, NOW())
  ON DUPLICATE KEY UPDATE `quantity` = `quantity` + __quantity, `last_touch_datetime` = NOW();


  -- Check if user is in WOK
  SELECT `game_id` INTO __user_game_id FROM `developer`.`active_logins` WHERE `user_id` =__kaneva_user_id;

  -- Promote pending inventory if NOT in WOK
  IF NOT __suppress_inventory_fetch AND (__user_game_id IS NULL OR NOT is_wok_game(__user_game_id)) THEN
    -- Promote pending inventory only if player_id exists
     IF __player_id IS NOT NULL and __player_id <> 0 THEN
       CALL get_inventory(__player_id, __kaneva_user_id, __inventory_type, 0);
     END IF;
  END IF;

  CALL logging.logMsg('TRACE', SCHEMA(), 'add_pending_inventory', CONCAT('END (__kaneva_user_id[',CAST(__kaneva_user_id AS CHAR), '], __inventory_type[', __inventory_type, '] __global_id[',CAST(__global_id AS CHAR), '] __quantity[',CAST(__quantity AS CHAR), '] __inventory_sub_type[', CAST(__inventory_sub_type AS CHAR), '])'));

END
;;
DELIMITER ;