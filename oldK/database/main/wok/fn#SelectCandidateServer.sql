-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS SelectCandidateServer;

DELIMITER ;;
CREATE FUNCTION `SelectCandidateServer`( _gameId INT, _zoneIndex INT, _zoneInstanceId INT  ) RETURNS int(11)
    READS SQL DATA
BEGIN
  --  Combined lightest instance on lightest box
  declare _serverId int default 0;
  declare _weight int default 0;
  declare _lightestBoxAddress varchar(50) default '0';
  declare _lightestBoxWeight int default 0;
  declare _worldSettingsValue text default 0;
  declare _communityZone INT DEFAULT 6;
  declare _worldSettingsValueOffset int DEFAULT 0;

  DECLARE _isGameZone CHAR;

  -- if the zone has the frameworkEnabled, it is considered a game zone.
  -- game zones go against one bank of servers, all others go against another bank

  SET _isGameZone = 'F'; 

  SELECT value
     INTO _worldSettingsValue 
     FROM script_game_custom_data
     WHERE zone_instance_id = _zoneInstanceId
     AND zone_type=zoneType(_zoneIndex)
     AND attribute = 'WorldSettings';

  SELECT LOCATE('"frameworkEnabled":true', _worldSettingsValue) INTO _worldSettingsValueOffset;

  IF _worldSettingsValueOffset <> 0 THEN
     SET _isGameZone = 'T'; 
  END IF;

   SELECT
            gs.ip_address                    as ip_address
            , sum(coalesce(zw.weight,0))     as server_weight

      into _lightestBoxAddress, _lightestBoxWeight
         from
            developer.game_servers AS gs
               LEFT OUTER JOIN wok.balancer_inputs      AS bi  ON gs.server_id =bi.server_id  AND bi.weight > 0
               LEFT OUTER JOIN wok.zone_weights zw             ON bi.zone_index = zw.zone_index
            , wok.zone_weights AS zone_affinities

         where
            gs.server_status_id  = 1
            and gs.visibility_id = 1
            AND (gs.game_play_server = _isGameZone OR gs.game_play_server = 'A')
            and zone_affinities.zone_index = _zoneIndex
            AND gs.number_of_players + 10 < gs.max_players
            AND timestampdiff (minute, last_ping_datetime, NOW()) < wok.deadServerInterval()
            AND gs.game_id       = _gameId
         group by ip_address
         order by server_weight
         limit 1;

   if ( _lightestBoxAddress <> '0' ) then
   select
      gs.server_id
      , sum(coalesce(zw.weight,0))      as instance_weight
      into _serverId, _weight
   from
      developer.game_servers AS gs
            LEFT OUTER JOIN wok.balancer_inputs AS bi   ON  gs.server_id =bi.server_id
            LEFT OUTER JOIN wok.zone_weights    AS zw   ON  bi.zone_index = zw.zone_index
        , wok.zone_weights                      AS zone_affinities

   where
      gs.server_status_id  = 1
      and gs.ip_address = _lightestBoxAddress
      and gs.visibility_id = 1
      and zone_affinities.zone_index = _zoneIndex
      AND gs.number_of_players + 10 < gs.max_players
      AND gs.game_id       = _gameId
      AND timestampdiff (minute, last_ping_datetime, NOW()) < wok.deadServerInterval()
   group by gs.server_id
   order by instance_weight
   limit 1;
   end if;

   return _serverId;

END
;;
DELIMITER ;