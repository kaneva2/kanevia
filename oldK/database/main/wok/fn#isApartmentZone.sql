-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS isApartmentZone;

DELIMITER ;;
CREATE FUNCTION `isApartmentZone`( zi INT ) RETURNS tinyint(1)
    DETERMINISTIC
BEGIN
	RETURN zoneType(zi) = 3; -- housing
END
;;
DELIMITER ;