-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS dance_level_actions_view;

DELIMITER ;;

 CREATE VIEW `dance_level_actions_view` AS SELECT `dla`.`dance_level` AS `dance_level`,`sa1`.`skill_id` AS `skill_id`,`sa1`.`action_id` AS `action_id`,`sa2`.`action_id` AS `perfect_action_id`,`sa1`.`gain_amt` AS `gain_amt`,`sa2`.`gain_amt` AS `perfect_gain_amt` 
 FROM ( ( ( `skills` `s` 
 JOIN `skill_actions` `sa1`)  
 JOIN `skill_actions` `sa2`)  
 JOIN `dance_level_actions` `dla`)  
 WHERE ( ( `s`.`skill_id` = `sa1`.`skill_id`)  
 AND ( `s`.`skill_id` = `sa2`.`skill_id`)  
 AND ( `sa1`.`action_id` = `dla`.`action_id`)  
 AND ( `sa2`.`action_id` = `dla`.`perfect_action_id`)  
 AND ( `s`.`name` = _latin1'Dance Fame') ) 
;;
DELIMITER ;