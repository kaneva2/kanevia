-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_user_active;

DELIMITER ;;
CREATE PROCEDURE `ss_update_user_active`(  )
begin
  replace into wok.snapshot_user_active
	select count(gu.user_id) as player_count,avg(time_played) as pta, date(now())
	from wok.game_users gu
	where time_played>=600;
end
;;
DELIMITER ;