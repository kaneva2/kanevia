-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_placed_furniture;

DELIMITER ;;
CREATE PROCEDURE `ss_update_placed_furniture`(  )
begin
  replace into wok.snapshot_placed_furniture
	select count(obj_placement_id) as placement_count,date(now()) as created_date
	from wok.dynamic_objects 
	where date(created_date)=date(now())
	group by date(created_date);
end
;;
DELIMITER ;