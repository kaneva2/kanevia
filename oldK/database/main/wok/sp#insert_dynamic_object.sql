-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS insert_dynamic_object;

DELIMITER ;;
CREATE PROCEDURE `insert_dynamic_object`( IN __player_id INT(11), IN __zone_index INT(11),
	IN __zone_instance_id INT(11), IN __object_id INT(11), IN __global_id INT(11),
	IN __inventory_sub_type INT(11), IN __position_x FLOAT, IN __position_y FLOAT,
	IN __position_z FLOAT, IN __rotation_x FLOAT, IN __rotation_y FLOAT,
	IN __rotation_z FLOAT, IN __slide_x FLOAT, IN __slide_y FLOAT, IN __slide_z FLOAT, 
	IN __texture_asset_id INT(11), IN __attachable INT,
	IN __expired_duration INT, IN __asset_group_id INT(11), OUT __obj_placement_id INT(11) )
BEGIN
-- called to insert into dyn object table, calling and returning get_dynamic_objects_id()

DECLARE __zone_type INT(11) DEFAULT NULL;
DECLARE __expired_date DATETIME;
SET __expired_date = NULL;

-- WOK specific feature so all permanent zones and Arenas share customizations based of instance 1
SET __zone_type = zoneType(__zone_index);
IF __zone_type = 4 OR __zone_type = 5 THEN
	SET __zone_instance_id = 1;
END IF;

IF __asset_group_id IS NULL THEN
	SET __asset_group_id = 0;
END IF;
	
SET __obj_placement_id = shard_info.get_dynamic_objects_id();

IF __expired_duration <> 0 THEN
	SET __expired_date = DATE_ADD(NOW(), INTERVAL + __expired_duration MINUTE);
END IF;

INSERT   INTO dynamic_objects              
	(obj_placement_id, player_id, zone_index, zone_instance_id, object_id, global_id,               
	inventory_sub_type, position_x, position_y, position_z, rotation_x, rotation_y, 
	rotation_z, slide_x, slide_y, slide_z, created_date, expired_date)   
VALUES (__obj_placement_id, __player_id, __zone_index, __zone_instance_id, __object_id, __global_id,          
	__inventory_sub_type, __position_x, __position_y, __position_z, __rotation_x, __rotation_y, __rotation_z, 
	__slide_x, __slide_y, __slide_z, NOW(), __expired_date);

-- add any params
IF __texture_asset_id <> 0 THEN 
	INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, `value`) 
	VALUES (__obj_placement_id, 14, __texture_asset_id );
END IF;
IF __asset_group_id <> 0 THEN 
	INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, `value`) 
	VALUES (__obj_placement_id, 16, __asset_group_id );
END IF;

END
;;
DELIMITER ;