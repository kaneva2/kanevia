-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS z_server_affinities;

DELIMITER ;;

 CREATE VIEW `z_server_affinities` AS SELECT `gs`.`ip_address` AS `ip_address`,`gs`.`server_id` AS `server_id`,if( ( `gs`.`affinity_mask` & 1) ,_latin1'T',_latin1'F')  AS `apts`,if( ( `gs`.`affinity_mask` & 2) ,_latin1'T',_latin1'F')  AS `hang`,if( ( `gs`.`affinity_mask` & 4) ,_latin1'T',_latin1'F')  AS `perm`,if( ( `gs`.`affinity_mask` & 8) ,_latin1'T',_latin1'F')  AS `mall`,if( ( `gs`.`affinity_mask` & 16) ,_latin1'T',_latin1'F')  AS `empl`,if( ( `gs`.`affinity_mask` & 32) ,_latin1'T',_latin1'F')  AS `kaching`,`gss`.`name` AS `status`,`gsv`.`name` AS `visibility` 
 FROM ( ( `developer`.`game_servers` `gs` 
 JOIN `developer`.`game_server_status` `gss` on( ( `gs`.`server_status_id` = `gss`.`server_status_id`) ) )  
 JOIN `developer`.`game_server_visibility` `gsv` on( ( `gs`.`visibility_id` = `gsv`.`visibility_id`) ) )  
 WHERE ( `gs`.`game_id` = `environment`.`getWOKGameId`( ) )  
 ORDER BY 1
;;
DELIMITER ;