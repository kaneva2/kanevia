-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS z_server_weights;

DELIMITER ;;

 CREATE VIEW `z_server_weights` AS SELECT `gs`.`ip_address` AS `ip_address`,`gs`.`server_id` AS `server_id`,`gs`.`number_of_players` AS `number_of_players`,sum( `zw`.`weight`)  AS `total_weight` 
 FROM ( ( `wok`.`vw_game_servers` `gs` 
 JOIN `wok`.`summary_active_population` `cz` on( ( ( `gs`.`server_id` = `cz`.`server_id`)  
 AND ( `cz`.`count` > 0) ) ) )  
 JOIN `wok`.`zone_weights` `zw` on( ( `cz`.`zone_index` = `zw`.`zone_index`) ) )  
 GROUP BY 1 
 ORDER BY 1
;;
DELIMITER ;