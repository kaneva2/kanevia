-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS z_list_players;

DELIMITER ;;

 CREATE VIEW `z_list_players` AS SELECT `gu`.`username` AS `username`,`ku`.`email` AS `email`,`gu`.`user_id` AS `user_id`,`gu`.`kaneva_user_id` AS `kaneva_user_id`,`p`.`player_id` AS `player_id`,coalesce( `ub`.`balance`,0)  AS `kpoints`,coalesce( `ub2`.`balance`,0)  AS `gpoints` 
 FROM ( ( `wok`.`game_users` `gu` 
 JOIN `wok`.`players` `p`)  
 JOIN ( ( `kaneva`.`users` `ku` 
 LEFT JOIN `kaneva`.`user_balances` `ub` on( ( ( `ub`.`user_id` = `ku`.`user_id`)  
 AND ( `ub`.`kei_point_id` = _latin1'KPOINT') ) ) )  
 LEFT JOIN `kaneva`.`user_balances` `ub2` on( ( ( `ub2`.`user_id` = `ku`.`user_id`)  
 AND ( `ub2`.`kei_point_id` = _latin1'GPOINT') ) ) ) )  
 WHERE ( ( `gu`.`user_id` = `p`.`user_id`)  
 AND ( `gu`.`kaneva_user_id` = `ku`.`user_id`) )  
 ORDER BY 1
;;
DELIMITER ;