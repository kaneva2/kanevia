-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS tmp_player_name;

DELIMITER ;;

 CREATE VIEW `tmp_player_name` AS SELECT DISTINCT `p`.`player_id` AS `player_id`,`p`.`name` AS `player_name` 
 FROM ( `developer`.`login_log` `ll` 
 JOIN `wok`.`players` `p` on( ( `ll`.`user_id` = `p`.`kaneva_user_id`) ) )  
 WHERE ( ( `ll`.`game_id` 
 NOT IN ( 3296,3298,5310,5354) )  
 AND ( `ll`.`created_date` > _utf8'2010-03-18') )  
 ORDER BY `p`.`player_id`
;;
DELIMITER ;