-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS can_spawn_to_zone;

DELIMITER ;;
CREATE PROCEDURE `can_spawn_to_zone`( IN playerId INT(11), 
											IN kanevaUserId INT(11), 
											IN zoneType INT(11), 
											IN zoneIndex INT(11), 
										   	IN instanceId INT(11), 
										   	IN myServerId INT(11), 
										   	IN isGm CHAR, 
										   	IN country CHAR(2),
										   	IN birthDate DATE,
										   	IN showMature BINARY(1),
										   	IN gotoPlayer BOOLEAN,
										   	IN gameNameNoSpaces varchar(100),
											OUT reasonCode INT,
											OUT retInstanceId INT(11),
											OUT serverId INT(11), 
											OUT scriptServerId INT(11), 
											OUT coverCharge INT(11),
											OUT zoneOwnerKanevaId INT(11),
											OUT url VARCHAR(300) )
BEGIN
-- only called from C++
	DECLARE localZoneIndex INT(11); -- since INOUT
	DECLARE zoneName VARCHAR(100) DEFAULT '';
	
	SET zoneOwnerKanevaId = 0;
	SET coverCharge = 0;
	SET serverId = myServerId;
	SET reasonCode = 3; -- 0 = ok, 1 = not in game, 2 = not found, 3 = permission, 4 = blocked, 5 = zoneFull, 6 = server full, 7 = pass required
	SET localZoneIndex = zoneIndex;
	CALL hasCoverCharge( kanevaUserId, zoneIndex, instanceId, coverCharge, zoneOwnerKanevaId );
	
	CALL checkMaxOccupancy( playerId, kanevaUserId, localZoneIndex, instanceId, isGm, country, birthDate, gotoPlayer, reasonCode );
	IF reasonCode = 0 THEN
		SET retInstanceId = instanceId;
		CALL can_player_spawn_to_zone( kanevaUserId, zoneType, localZoneIndex, instanceId, myServerId, isGm, country, birthDate, showMature, reasonCode, serverId, scriptServerId, zoneName );
	END IF;
	
	-- CALL logDebugMsg( 'can_spawn_to_zone', concat( 'return from canPlayerSpawnToZone2. server: ', cast( IFNULL(serverId,0) as char ) ) );
	IF serverId IS NULL THEN 
		SET serverId = 0;
	END IF;
	
	SET url = concat_vw_url( gameNameNoSpaces, zoneName, zoneType ); 
	
	-- CALL logDebugMsg( 'canSpawnToZone2', concat( 'exit called other fn with server ', IFNULL(server,'') ) );
END
;;
DELIMITER ;