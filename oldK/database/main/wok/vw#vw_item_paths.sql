-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS vw_item_paths;

DELIMITER ;;

 CREATE VIEW `vw_item_paths` AS SELECT `ip`.`global_id` AS `global_id`,`ip`.`type_id` AS `type_id`,`ip`.`ordinal` AS `ordinal`,`ip`.`path` AS `path`,`ip`.`file_size` AS `file_size`,`ip`.`file_hash` AS `file_hash`,`ipt`.`type` AS `type` 
 FROM ( `item_paths` `ip` 
 JOIN `item_path_types` `ipt` on( ( `ip`.`type_id` = `ipt`.`type_id`) ) ) 
;;
DELIMITER ;