-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS log_on_user;

DELIMITER ;;
CREATE PROCEDURE `log_on_user`(IN __server_id INT, IN __ip_address VARCHAR(30), IN __user_id INT, IN __kaneva_user_id INT )
BEGIN

DECLARE _second_to_last_logon timestamp;
SET _second_to_last_logon = (SELECT last_logon FROM game_users WHERE user_id = __user_id);

		UPDATE `game_users` 
		SET `logged_on` = 'T',
			`second_to_last_logon` = _second_to_last_logon,
			`last_logon` = NOW(),
		    `last_update_time` = NOW(),
		    `server_id` = __server_id,
		    `last_ip` = __ip_address
		 WHERE `user_id` = __user_id;

		-- update the player list from the db
		SELECT `player_id`, `name`, `original_EDB` FROM `players` WHERE `user_id` = __user_id;

END
;;
DELIMITER ;