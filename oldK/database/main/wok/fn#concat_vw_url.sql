-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS concat_vw_url;

DELIMITER ;;
CREATE FUNCTION `concat_vw_url`( __game_name_no_spaces VARCHAR(100), 
							    __remainder VARCHAR(255),
							    __zone_type INT ) RETURNS varchar(300) CHARSET latin1
    DETERMINISTIC
BEGIN
	-- broke out to encapsulate the constants
	DECLARE __url VARCHAR(300) DEFAULT ''; -- if none of these types, default to nothing
	
	IF __zone_type = 0 and length(__remainder) = 0 THEN -- just logon zone
		SET __url = concat(vw_url_prefix(), '://',__game_name_no_spaces);
	ELSEIF __zone_type = 3 THEN -- apt
		SET __url = concat(vw_url_prefix(), '://',__game_name_no_spaces,'/',__remainder,'.home');
	ELSEIF __zone_type = 4 OR __zone_type = 0 THEN -- perm zone
		SET __url = concat(vw_url_prefix(), '://',__game_name_no_spaces,'/',__remainder,'.zone');
	ELSEIF __zone_type = 5 OR __zone_type = 0 THEN -- arena zone
		SET __url = concat(vw_url_prefix(), '://',__game_name_no_spaces,'/',__remainder,'.arena');
	ELSEIF __zone_type = 6 THEN -- channel
		SET __url = concat(vw_url_prefix(), '://',__game_name_no_spaces,'/',__remainder,'.channel');
	END IF;		
	
	RETURN __url;
END
;;
DELIMITER ;