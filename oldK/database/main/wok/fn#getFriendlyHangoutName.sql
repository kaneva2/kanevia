-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS getFriendlyHangoutName;

DELIMITER ;;
CREATE FUNCTION `getFriendlyHangoutName`( zoneInstanceId INT, zoneIndex INT ) RETURNS varchar(128) CHARSET latin1
    READS SQL DATA
BEGIN
   DECLARE friendlyCommunityName VARCHAR(64) DEFAULT NULL;
   DECLARE friendlyZoneName VARCHAR(64) DEFAULT NULL;
   DECLARE friendlyName VARCHAR(128) DEFAULT NULL;
     SELECT c.name INTO friendlyCommunityName
       FROM kaneva.communities c 
       WHERE c.community_id=zoneInstanceId;
     SELECT sw.display_name INTO friendlyZoneName
        FROM supported_worlds sw
        WHERE sw.zone_index=zoneIndex(zoneIndex);
     IF friendlyCommunityName IS NOT NULL AND friendlyZoneName IS NOT NULL THEN
        RETURN concat(friendlyCommunityName, ' - ', friendlyZoneName);
     ELSE 
     	CALL logMsg( 'getFriendlyHangoutName', 'WARN', concat('Failed to get hangout name for ', cast( zoneInstanceId as char ), ' ', hex(zoneIndex) ) );
     	RETURN 'Unknown';
     END IF;
END
;;
DELIMITER ;