-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS zoningOut;

DELIMITER ;;
CREATE PROCEDURE `zoningOut`( IN currentZoneIndex INT, IN currentZoneInstanceId INT,
                                                            IN serverId INT )
BEGIN

        SET @prevCount = 0;


        -- before we lock everything up...update the balancer_inputs
        -- 
        call decrementBalancerInputs( currentZoneIndex, currentZoneInstanceId );





        SELECT count INTO @prevCount
          FROM summary_active_population
         WHERE server_id = serverId
          AND zone_index = currentZoneIndex
          AND zone_instance_id = currentZoneInstanceId
          FOR UPDATE;

        IF @prevCount > 100000 THEN

                CALL logMsg( 'zoningOut', 'ERROR', concat( 'Underflow for zone ', hex(currentZoneIndex), ":", cast(currentZoneInstanceId as char ), ' on server ', cast( serverId as char ) ) );
                UPDATE summary_active_population
                   SET count = 0
                 WHERE server_id = serverId
                  AND zone_index = currentZoneIndex
                  AND zone_instance_id = currentZoneInstanceId;

            SET @prevCount = 0;

        ELSEIF @prevCount > 0 THEN

                UPDATE summary_active_population
                   SET count = @prevCount - 1
                 WHERE server_id = serverId
                  AND zone_index = currentZoneIndex
                  AND zone_instance_id = currentZoneInstanceId;
        END IF;

        IF @prevCount = 1 THEN

                call logDebugMsg( 'zoningOut', concat( 'Setting NOT_TIED on ', hex( currentZoneIndex ), ':', cast( currentZoneInstanceId as char ) ) );

                UPDATE channel_zones
                  SET  tied_to_server = 'NOT_TIED',
                       server_id = 0
                 WHERE zone_index = currentZoneIndex
                   AND zone_instance_id = currentZoneInstanceId
                   AND tied_to_server = 'TIED';
        END IF;

        SET @prevCount = 0;


        IF zoneType(currentZoneIndex) = 4 THEN

                SELECT count INTO @prevCount
                  FROM summary_active_permanent_population
                 WHERE zone_index = currentZoneIndex
                   FOR UPDATE;

                IF @prevCount > 0 THEN
                        UPDATE summary_active_permanent_population
                           SET count = @prevCount - 1
                         WHERE zone_index = currentZoneIndex;
                END IF;

                IF @prevCount = 0 OR @prevCount > 100000 THEN
                        CALL logMsg( 'zoningOut', 'ERROR', concat( 'Underflow for perm zone ', hex(currentZoneIndex), ":", cast(currentZoneInstanceId as char ), ' on server ', cast( serverId as char ) ) );
                        UPDATE summary_active_permanent_population
                           SET count = 0
                         WHERE zone_index = currentZoneIndex;
                END IF;

        END IF;

END
;;
DELIMITER ;