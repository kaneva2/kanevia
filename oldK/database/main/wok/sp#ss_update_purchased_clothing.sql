-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_purchased_clothing;

DELIMITER ;;
CREATE PROCEDURE `ss_update_purchased_clothing`(  )
begin
  replace into wok.snapshot_purchased_clothing
	select count(i.global_id) as purchase_count,date(now()) as created_date
	from wok.inventories i 
	where date(created_date)=date(now())
	and i.global_id not in (select distinct global_id from dynamic_objects) 
	group by date(created_date);
end
;;
DELIMITER ;