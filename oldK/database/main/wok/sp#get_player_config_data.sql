-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_player_config_data;

DELIMITER ;;
CREATE PROCEDURE `get_player_config_data`( 
  IN __kaneva_user_id INT, 
  IN __inventory_flags INT, 
  IN __game_id int
)
    COMMENT '\n  '
this_proc:

BEGIN
	
	-- called from C# in API
	-- get all the datasets about a user and player, in the order C# wants them	
	
	-- constants from MySqlEventProcessor.cpp
	DECLARE	GET_INV_ARMED		INT DEFAULT 1;
	DECLARE	GET_INV_ARMED_FURN	INT DEFAULT	3;
	DECLARE	GET_INV_ALL			INT DEFAULT 255;

	DECLARE __color_type TINYINT DEFAULT 0; 
	DECLARE __scale_x FLOAT DEFAULT 1.0; 
	DECLARE __scale_y FLOAT DEFAULT 1.0; 
	DECLARE __scale_z FLOAT DEFAULT 1.0; 
	DECLARE __player_id INT;
	DECLARE __housing_zone_index INT;
	DECLARE __account_type INT DEFAULT 3;

  DECLARE __retval INT DEFAULT -1;


   DECLARE EXIT HANDLER FOR SQLEXCEPTION
     BEGIN
      -- ERROR
       GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
       CALL logging.logMsg('ERROR', SCHEMA(), 'get_player_config_data', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
       SET __retval = -1;
       RESIGNAL;
     END;


  CALL logging.logMsg('TRACE', SCHEMA(), 'get_player_config_data', CONCAT('BEGIN (__kaneva_user_id[',CAST(__kaneva_user_id AS CHAR),'] __inventory_flags[',CAST(__inventory_flags AS CHAR), '] __game_id[',CAST(__game_id AS CHAR),'])'));


	SET __account_type = get_game_priv( __game_id, __kaneva_user_id );
	
	IF __account_type = 1 THEN -- owner can do everything
		SELECT `user_id`, 'T' as is_gm, 'T' as can_spawn, 'T' as is_admin, `server_id`, `logged_on`, `time_played`, `age`, `show_mature`
		FROM 	`vw_users`
		WHERE 	kaneva_user_id = __kaneva_user_id;
	ELSEIF __account_type = 2 THEN -- moderator is just a gm
		SELECT `user_id`, 'T' as is_gm, 'F' as can_spawn, 'F' as is_admin, `server_id`, `logged_on`, `time_played`, `age`, `show_mature`
		FROM 	`vw_users`
		WHERE 	kaneva_user_id = __kaneva_user_id;
	ELSE
		SELECT `user_id`, 'F' as is_gm, 'F' as can_spawn, 'F' as is_admin, `server_id`, `logged_on`, `time_played`, `age`, `show_mature`
		FROM 	`vw_users`
		WHERE 	kaneva_user_id = __kaneva_user_id;
	END IF;

	CALL get_player_check_archive( __player_id, __kaneva_user_id );

  SET __retval = -1;
  CALL refreshInventory(__player_id, __kaneva_user_id, '', __retval);
  IF (__retval = -1) THEN
    CALL logging.logMsg('TRACE', SCHEMA(), 'get_player_config_data', CONCAT(' refreshingInventory __player_id[',CAST(__player_id AS CHAR),'] __kaneva_user_id[',CAST(__kaneva_user_id AS CHAR), '] __inventory_type_req[', __inventory_type_req, '] returned -1'));
  END IF;


	
	SELECT `scale_factor_x`,`scale_factor_y`,`scale_factor_z`, `color_type`, `housing_zone_index`
	  INTO __scale_x, __scale_y, __scale_z, __color_type, __housing_zone_index 
	  FROM `player_presences` 
	 WHERE player_id = __player_id;
	 
	SELECT __scale_x as `scale_factor_x`,__scale_y as `scale_factor_y`, __scale_z as `scale_factor_z`, __housing_zone_index as `housing_zone_index`;
	
	
	
	IF __account_type in (1,2) THEN
		-- GM/full inventory access -> all clothing/accessories + builder items
		-- GM/limited inventory access -> armed + builder items
		SELECT 	inventory_type, global_id, inventory_sub_type, armed, SUM(quantity) as quantity
		FROM
			(SELECT 	i.inventory_type, 
						i.global_id, 
						inventory_sub_type, 
						armed, 
						quantity
			 FROM		inventories i 
			 INNER JOIN items it ON i.global_id = it.global_id
			 LEFT JOIN  pass_group_items pgi ON pgi.global_id = it.global_id   
			 WHERE   	player_id = __player_id
			 AND		(i.inventory_type = 'P' OR i.inventory_type = 'B')
			 AND   		(it.use_type in (0,13) AND (armed = 'T' OR __inventory_flags=GET_INV_ALL)	-- clothing/accessories :: armed or full inventory access enabled
			             OR it.use_type in (10,11,200,201,206,207)									
			             OR it.is_default = 1)														

			 UNION ALL   
			 
			 SELECT 	'P' AS inventory_type, 
						global_id, 
						inventory_sub_type, 
						'F' AS armed, 
						COUNT(*) AS quantity 
			 FROM 		wok.dynamic_objects 
			 WHERE		player_id = __player_id 
			 GROUP BY	inventory_type, global_id, inventory_sub_type, armed) AS x
		GROUP BY inventory_type, global_id, inventory_sub_type, armed;
	ELSEIF __account_type = 0 THEN
		-- Visitor/full inventory access -> all clothing/accessories
		-- Visitor/limited inventory access -> armed items only
		SELECT 
			i.inventory_type, 
			i.global_id, 
			inventory_sub_type, 
			armed, 
			quantity
		FROM 
			inventories i 
			INNER JOIN items it ON i.global_id = it.global_id
		WHERE 
			player_id = __player_id 
			AND (it.use_type in (0,13) AND (armed='T' OR __inventory_flags=GET_INV_ALL)		-- clothing/accessories :: armed or full inventory access enabled
				 OR it.is_default = 1);														-- default items (clothing)
	ELSE
		-- This section is obsolete now
		IF __inventory_flags = GET_INV_ARMED THEN -- armed only
			SELECT i.inventory_type, i.global_id, inventory_sub_type, armed, quantity
					FROM inventories i 
					INNER JOIN items it ON i.global_id = it.global_id
				WHERE player_id = __player_id 
					AND (armed = 'T' OR it.is_default = 1);
		ELSEIF __inventory_flags = GET_INV_ARMED_FURN THEN -- armed+furniture     
			SELECT i.inventory_type, i.global_id, inventory_sub_type, armed, quantity
					FROM inventories i 
					INNER JOIN items it ON i.global_id = it.global_id
				WHERE player_id = __player_id
					AND i.inventory_type = 'P'
					AND (armed = 'T'
						OR it.use_type in (10,11,13,200,201,206,207) OR it.is_default = 1);
		ELSEIF __inventory_flags = 5 THEN -- all personal inventory	   
			SELECT i.inventory_type, i.global_id, inventory_sub_type, armed, quantity
					FROM inventories i 
					INNER JOIN items it ON i.global_id = it.global_id
				WHERE player_id = __player_id
					AND i.inventory_type = 'P';
		ELSEIF __inventory_flags = GET_INV_ALL THEN -- everything, personal and banked inventory
			SELECT inventory_type, i.global_id, inventory_sub_type, armed, quantity
					FROM inventories i
				WHERE player_id = __player_id;
		END IF;
	END IF;
	
	SELECT `equip_id`, `deformable_config`, `custom_material`, `custom_color_r`, `custom_color_g`, `custom_color_b`
	  FROM `deformable_configs`
	 WHERE `player_id` = __player_id
	 ORDER BY `sequence`;
	
	-- 1 = GIFT_CREDITS_ON_PLAYER
	-- 2 = CREDITS_ON_PLAYER
	-- 6 = CREDITS_IN_BANK
	SELECT kei_point_id, `balance`
	  FROM `vw_user_balances` 
	 WHERE user_id =  __kaneva_user_id
	   AND `kei_point_id` IN (1,2,6);  
		 
	SELECT clan_name, owner_id
	  FROM clan_members cm INNER JOIN clans c ON cm.clan_id = c.clan_id
	 WHERE player_id = __player_id;
	 
	SELECT pass_group_id, end_date
	  FROM vw_pass_group_users u
	 WHERE u.kaneva_user_id = __kaneva_user_id;
	
	SELECT color_r, color_g, color_b
	  FROM colors
	 WHERE color_type = __color_type; 
	 
	 IF __game_id <> 0 THEN
		SELECT game_id, global_id, inventory_type, inventory_sub_type, quantity, quantity_purchased
		  FROM game_inventories_sync gis
		 WHERE gis.kaneva_user_id = __kaneva_user_id
		   AND gis.game_id = __game_id;
	 END IF;

  CALL logging.logMsg('TRACE', SCHEMA(), 'get_player_config_data', CONCAT('BEGIN (__kaneva_user_id[',CAST(__kaneva_user_id AS CHAR) ,'] __inventory_flags[',CAST(__inventory_flags AS CHAR), '] __game_id[',CAST(__game_id AS CHAR),'])'));

END
;;
DELIMITER ;