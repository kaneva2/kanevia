-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS z_cleanup_perm_zones;

DELIMITER ;;
CREATE PROCEDURE `z_cleanup_perm_zones`( IN __test BOOL )
BEGIN 
-- admin tool to cleanup unused zones
 DECLARE __zoneIndex INT(11); 
 DECLARE __zoneInstanceId INT(11); 
 DECLARE __done INT DEFAULT 0; 
 DECLARE __permZones CURSOR FOR 
 SELECT zone_index, zone_instance_id 
  FROM channel_zones 
 WHERE zone_type = 4 
  AND zone_instance_id > 1 
 AND tied_to_server = 'NOT_TIED'; 
  
 DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1; 
 
 IF __test THEN 
 
 SELECT zone_index, zone_instance_id 
  FROM channel_zones 
 WHERE zone_type = 4 
  AND zone_instance_id > 1 
 AND tied_to_server = 'NOT_TIED'; 
 
 ELSE 
 
 OPEN __permZones; 
 
 REPEAT 
 FETCH __permZones into __zoneIndex, __zoneInstanceId; 
 
 IF NOT __done THEN 
 
 DELETE FROM channel_zones 
  WHERE zone_index = __zoneIndex 
  AND zone_instance_id = __zoneInstanceId; 
 
 DELETE FROM 
  wok.world_object_player_settings 
  WHERE zone_index = __zoneIndex 
  AND zone_instance_id = __zoneInstanceId; 
 
 DELETE FROM 
  wok.dynamic_objects 
  WHERE zone_index = __zoneIndex 
  AND zone_instance_id = __zoneInstanceId; 
 
 END IF; 
 
 UNTIL __done END REPEAT; 
 
 CLOSE __permZones; 
 
 END IF; 
 
END
;;
DELIMITER ;