-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS vw_url_prefix;

DELIMITER ;;
CREATE FUNCTION `vw_url_prefix`() RETURNS varchar(20) CHARSET latin1
    DETERMINISTIC
BEGIN
-- name of url prefix, was "vwtp", "stp", now "kaneva"
-- this is a fn for easy changing
	return "kaneva"; 
END
;;
DELIMITER ;