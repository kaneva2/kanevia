-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS can_spawn_to_url;

DELIMITER ;;
CREATE PROCEDURE `can_spawn_to_url`( IN __source_player_id INT(11), IN __source_kaneva_id INT(11), 
		IN __in_url VARCHAR(255), IN __my_server_id INT(11), IN __is_gm CHAR, IN __country CHAR(2),
 		IN __birth_Date DATE, IN __show_mature BINARY(1), IN __game_name_no_spaces VARCHAR(100), IN __game_id INT(11),
		OUT __reason_code INT, OUT __zone_index INT(11), OUT __zone_instance INT(11), OUT __x FLOAT, OUT __y FLOAT,
		OUT __z FLOAT, OUT __r INT, OUT __spawnPtIndex INT(11), OUT __server_id INT(11), OUT __script_server_id INT(11),
		OUT __cover_charge INT(11), OUT __owner_id INT(11), OUT	__url VARCHAR(300), OUT __dest_kaneva_id INT(11) )
BEGIN
	
	DECLARE __admin_perm_instance_id INT(11) DEFAULT 0;
	DECLARE __instance_name VARCHAR(100) DEFAULT NULL;
	DECLARE __named_location VARCHAR(100) DEFAULT NULL;
	DECLARE __zone_type INT(11) DEFAULT NULL;

	DECLARE __url_game_name VARCHAR( 100 ) DEFAULT NULL;

   
   
   
   
   
   DECLARE __errant VARCHAR(1024);

   DECLARE EXIT handler FOR 1172
   BEGIN
    
    CALL logMsg( 'can_spawn_to_url', 'ERROR', CONCAT('ERROR 1172 [', __errant, ']') );

    
    
    
    PREPARE errst FROM @__errant;
    execute errst;
   END;



	
	SET __reason_code = 8; 
 	SET __x = NULL;
	SET __y = NULL;
	SET __z = NULL;
	SET __r = NULL;
	SET __server_id = 0;
	SET __cover_charge = 0;
	SET __owner_id = 0;
	SET __url = '';
	SET __zone_index = 0;
	SET __spawnPtIndex = -1;
   SET __dest_kaneva_id = 0;
   SET __errant = '';


	
	CALL parse_vw_url( __in_url, __instance_name, __zone_type, __named_location, __admin_perm_instance_id, __url_game_name );

	-- Special handling for "Home" communities. These need to be routed to the user's apartment.
	IF __instance_name IS NOT NULL AND __zone_type = 6 THEN
		SELECT COALESCE(c.creator_id, __dest_kaneva_id), COALESCE(c.creator_username, __instance_name) INTO __dest_kaneva_id, __instance_name
		FROM kaneva.communities c
		WHERE c.name = __instance_name AND c.community_type_id = 5;

		IF __dest_kaneva_id > 0 THEN
			SET __in_url = concat_vw_url(__game_name_no_spaces, __instance_name, 3);
			CALL parse_vw_url( __in_url, __instance_name, __zone_type, __named_location, __admin_perm_instance_id, __url_game_name );
		END IF;
	END IF;
	
	IF __instance_name IS NOT NULL AND __zone_type IS NOT NULL THEN 

		IF __url_game_name <> __game_name_no_spaces AND CAST(__url_game_name AS SIGNED) <> __game_id THEN 
			CALL logDebugMsg( 'can_spawn_to_url', concat('mismatched game in URL ',__in_url, ' with game of:', __url_game_name, ' <> ', __game_name_no_spaces, ' or ' , cast( __game_id as char ) ) );
			SET __reason_code = 9; 
		ELSEIF __zone_type = 3 OR __zone_type = 99 THEN 
			

			SET @__errant =  CONCAT('SELECT kaneva_user_id INTO __dest_kaneva_id FROM players WHERE name = ''' , __instance_name, '''' );
			SELECT kaneva_user_id INTO __dest_kaneva_id
			 FROM  players
			 WHERE `name` = __instance_name;

			IF __dest_kaneva_id IS NULL THEN 
				SET __reason_code = 2; 
			ELSE

				IF __zone_type = 3 THEN 
					CALL can_spawn_to_apartment( __source_player_id, __source_kaneva_id, __dest_kaneva_id, __my_server_id, 
							__is_gm, __country, __birth_date, __show_mature, __game_name_no_spaces, __reason_code,
				 			__zone_index, __server_id, __script_server_id, __zone_instance, __cover_charge, __owner_id, __url  );
          SET __dest_kaneva_id = 0; 
				ELSE
					CALL can_spawn_to_player( __source_player_id, __source_kaneva_id, __dest_kaneva_id, __my_server_id, 
				 			__is_gm, __country, __birth_date, __show_mature, __game_name_no_spaces, __reason_code,
				 			__zone_index, __zone_instance, __x, __y, __z, __server_id, __script_server_id, __cover_charge, __owner_id, __url );
				END IF;			 						  
			END IF;			 						   
		ELSEIF __zone_type = 4 THEN 
		
			SELECT zone_index INTO __zone_index
			FROM  developer.faux_3d_app f3d
			INNER JOIN kaneva.communities_game cg ON f3d.game_id = cg.game_id
			INNER JOIN kaneva.communities c ON c.community_id = cg.community_id
			WHERE c.name = __instance_name
			LIMIT 1;

			-- Fall back to old way if fail
			IF __zone_index IS NULL OR __zone_index <= 0 THEN 
				SELECT zone_index INTO __zone_index
				FROM  channel_zones
				WHERE `name` = __instance_name AND zone_type IN (0,4)
				LIMIT 1;
			END IF;

			IF __zone_index IS NULL THEN 
				SET __reason_code = 2; 
			ELSE

			 CALL can_spawn_to_zone( __source_player_id, __source_kaneva_id, __zone_type, __zone_index, __admin_perm_instance_id,  
			 			__my_server_id, __is_gm, __country, __birth_date, __show_mature, FALSE, 
			 			__game_name_no_spaces, __reason_code, __zone_instance, __server_id, __script_server_id, __cover_charge, __owner_id, __url );
			END IF;			 					  
			 
		ELSEIF __zone_type = 6 THEN 
		
         SET @__errant = concat(    'SELECT community_id, IFNULL(zone_index,defaultBroadbandZone()) as zone_index INTO @admin_perm_instance_id, @zone_index '
			                           , 'FROM kaneva.communities c LEFT OUTER JOIN '
			                           , ' channel_zones cz ON c.community_id = cz.zone_instance_id AND cz.zone_type = 6 '
			                           , 'WHERE c.name = ''', __instance_name, ''' AND is_personal = 0' );


			SELECT community_id, IFNULL(zone_index,defaultBroadbandZone()) as zone_index INTO __admin_perm_instance_id, __zone_index
				FROM kaneva.communities c 
				LEFT OUTER JOIN channel_zones cz ON c.community_id = cz.zone_instance_id AND cz.zone_type = 6
			WHERE c.name = __instance_name AND is_personal = 0;

			IF __admin_perm_instance_id IS NULL OR __zone_index IS NULL THEN 
				SET __reason_code = 2; 
			ELSE
			 CALL can_spawn_to_zone( __source_player_id, __source_kaneva_id, __zone_type, __zone_index, __admin_perm_instance_id,  
			 			__my_server_id, __is_gm, __country, __birth_date, __show_mature, FALSE, 
			 			__game_name_no_spaces, __reason_code, __zone_instance, __server_id, __script_server_id, __cover_charge, __owner_id, __url );
			END IF;			 					   

		ELSEIF __zone_type = 5 THEN 
			
			
			IF __is_gm = 'T' THEN
				SET __zone_index = NULL;

				SET @__errant =  CONCAT( 'SELECT 0x50000000+zone_index INTO __zone_index FROM supported_worlds WHERE display_name = ''', __instance_name,'''' );


				SELECT 0x50000000+zone_index INTO __zone_index 
				  FROM supported_worlds
				 WHERE display_name = __instance_name;
				IF __zone_index IS NOT NULL THEN 				 
					SET __server_id = __my_server_id;
					SET __script_server_id = 0;
					SET __url = concat_vw_url( __url_game_name, __instance_name, __zone_type );
					SET __zone_instance = __admin_perm_instance_id;
					SET __reason_code = 0;
				ELSE
					SET __reason_code = 2; 
				END IF;
			ELSE
				SET __reason_code = 3; 
			END IF;
		END IF;
	END IF;

	IF __reason_code = 0 THEN 	
		IF __named_location IS NOT NULL THEN 

         SET @__errant =  concat( 'SELECT position_x, position_y, position_z, rotation INTO __x, __y, __z, __r '
			                         , 'FROM spawn_points '
			                         , 'WHERE name = '''
                                  , __named_location 
			                         , ''' AND zone_index = '
                                  , zoneIndex(__zone_index) );


			SELECT position_x, position_y, position_z, rotation INTO __x, __y, __z, __r
			  FROM spawn_points
			 WHERE `name` = __named_location
			   AND zone_index = zoneIndex(__zone_index); 
			   
			IF __x IS NOT NULL THEN
				SET __url = concat( __url, '#', __named_location ); 
			END IF;
		END IF;

		IF __x IS NULL THEN
			SET __spawnPtIndex = 0; 
		END IF;
		
		SET __x = IFNULL(__x,0.0);
		SET __y = IFNULL(__y,0.0);
		SET __z = IFNULL(__z,0.0);
		SET __r = IFNULL(__r,0);
		SET __server_id = IFNULL(__server_id,0);
		
	END IF;
	SET __zone_index = IFNULL(__zone_index,0);
	
END
;;
DELIMITER ;