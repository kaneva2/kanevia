-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS copy_item;

DELIMITER ;;
CREATE PROCEDURE `copy_item`( IN __source_global_id INT(11), IN __target_global_id INT(11) )
BEGIN
 DECLARE custom_error CONDITION FOR SQLSTATE '45000';
   select @source_type:= s.use_type,
          @target_type:= t.use_type,
          @source_market_cost:= s.market_cost,
          @target_base_global_id:=t.base_global_id
   FROM wok.items s, wok.items t 
   where s.global_id = __source_global_id
         and t.global_id = __target_global_id;

IF @source_type = @target_type and @source_type is not null THEN
  update wok.items set market_cost = @source_market_cost, item_creator_id = 0
  where global_id = __target_global_id;
  
  update shopping.items_web set designer_price = 0, search_refresh_needed = now() where global_id = __target_global_id;
  
  if @target_base_global_id > 0 then
	update shopping.items_web set designer_price = 0, search_refresh_needed = now() where global_id = @target_base_global_id;
  end if;

  replace into wok.item_paths 
	select if(ips.global_id=__source_global_id,__target_global_id,@target_base_global_id) as global_id, ips.type_id, ips.ordinal, ips.path, ips.file_size, ips.file_hash
			from wok.item_paths ips, wok.items i
		where (i.base_global_id = ips.global_id or i.global_id = ips.global_id) and i.global_id  = __source_global_id;

ELSE
   set @error_message:=concat("Mismatch or missing (",__source_global_id,",",__target_global_id,") Source use_type=", ifnull(@source_type,"null"),"; Target use_type=", ifnull(@target_type,"null"));
   SIGNAL SQLSTATE '45000'
   SET MESSAGE_TEXT = @error_message;
END IF;

END
;;
DELIMITER ;