-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS isCustomDeedUGC;

DELIMITER ;;
CREATE FUNCTION `isCustomDeedUGC`( deedGlobalId INT ) RETURNS tinyint(1)
    READS SQL DATA
BEGIN
	DECLARE isUGC BOOLEAN DEFAULT FALSE;
	DECLARE itemCreatorId INT(11) DEFAULT NULL;
	DECLARE templateId INT(11) DEFAULT NULL;
	DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;

	-- Grab item creator, will be > 0 if a UGC custom deed. Older Kaneva-made items have an id of 0.
	SELECT i.item_creator_id INTO itemCreatorId
	FROM items i
	INNER JOIN item_use_types ut ON i.use_type = ut.use_type_id AND ut.use_type = 'USE_TYPE_CUSTOM_DEED'
	WHERE global_id = deedGlobalId;

	-- Grab template id, will be NULL if a UGC custom deed.
	SELECT template_id INTO templateId
	FROM world_template_global_id
	WHERE global_id = deedGlobalId;
	
    RETURN (templateId IS NULL AND itemCreatorId IS NOT NULL AND itemCreatorId > 0);

END
;;
DELIMITER ;