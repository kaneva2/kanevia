-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS can_spawn_to_player;

DELIMITER ;;
CREATE PROCEDURE `can_spawn_to_player`( IN sourcePlayerId INT, 
										IN sourceKanevaId INT, 
										IN destKanevaId INT, 
										IN myServerId INT(11), 
										IN isGm CHAR,
									    IN country CHAR(2),
									    IN birthDate DATE,
									    IN showMature BINARY(1),
									   	IN gameNameNoSpaces varchar(100),
										OUT reasonCode INT,
										OUT cZoneIndex INT(11),
										OUT cZoneInstance INT(11),
										OUT x float,
										OUT y float,
										OUT z float,									
										OUT serverId INT(11), 
										OUT scriptServerId INT(11),
										OUT coverCharge INT(11),
										OUT zoneOwnerKanevaId INT(11),
										OUT url VARCHAR(255) )
BEGIN
-- only called from C++
	DECLARE playerName VARCHAR(80) DEFAULT '';
	SET reasonCode = 2; -- 0 = ok, 1 = not in game, 2 = not found, 3 = permission, 4 = blocked, 5 = zoneFull, 6 = server full, 7 = pass required
	SET coverCharge = 0;
	SET zoneOwnerKanevaId = 0;
	
	CALL can_player_spawn_to_player_common( sourcePlayerId, sourceKanevaId, destKanevaId, myServerId, isGm, country, birthDate, showMature, reasonCode, cZoneIndex, cZoneInstance, serverId, scriptServerId, x, y, z, playerName );
	CALL hasCoverCharge( sourceKanevaId, cZoneIndex, cZoneInstance, coverCharge, zoneOwnerKanevaId );
	
	SET x = IFNULL( x, 0.0 );
	SET y = IFNULL( y, 0.0 );
	SET z = IFNULL( z, 0.0 );
	SET serverId = IFNULL( serverId, 0 );
	SET url = concat_vw_url(gameNameNoSpaces,playerName,zoneType(cZoneIndex));
	
END
;;
DELIMITER ;