-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS player_stats_view;

DELIMITER ;;

 CREATE VIEW `player_stats_view` AS SELECT `player_stats`.`player_id` AS `player_id`,`stats`.`stat_id` AS `stat_id`,`stats`.`name` AS `name`,`stats`.`cap_value` AS `cap_value`,`stats`.`gain_basis` AS `gain_basis`,`stats`.`benefit_type` AS `benefit_type`,`stats`.`bonus_basis` AS `bonus_basis`,`player_stats`.`current_value` AS `current_value` 
 FROM ( `stats` 
 JOIN `player_stats`)  
 WHERE ( `stats`.`stat_id` = `player_stats`.`stat_id`) 
;;
DELIMITER ;