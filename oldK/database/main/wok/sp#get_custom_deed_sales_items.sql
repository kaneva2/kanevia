-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_custom_deed_sales_items;

DELIMITER ;;
CREATE PROCEDURE `get_custom_deed_sales_items`( IN __deed_global_id INT  )
BEGIN

SELECT do.id AS id,
       do.global_id AS global_id,
       it.name AS name,
       iw.thumbnail_small_path AS thumbnail_small_path,
       ( iw.designer_price
        + IFNULL((SELECT iw2.designer_price
                    FROM shopping.items_web iw2
                   WHERE iw.base_global_id = iw2.global_id), 0))
          AS market_cost,
       ifnull((SELECT pgi.pass_group_id
                 FROM pass_group_items pgi
                WHERE pgi.global_id = do.global_id), 0)
          AS pass_group_id,
       it.item_creator_id,
       COUNT(*) AS item_count,
	   iw.item_active
  FROM starting_dynamic_objects do
       INNER JOIN items it
          ON it.global_id = do.global_id
       INNER JOIN shopping.items_web iw
          ON iw.global_id = do.global_id
WHERE do.apartment_template_id = __deed_global_id
GROUP BY global_id;


SELECT tdo.id,
       it.global_id AS global_id,
       it.name AS name,
       iw.thumbnail_small_path AS thumbnail_small_path,
       ( iw.designer_price
        + IFNULL((SELECT iw2.designer_price
                    FROM shopping.items_web iw2
                   WHERE iw.base_global_id = iw2.global_id), 0))
          AS market_cost,
       ifnull((select pgi.pass_group_id from pass_group_items pgi where pgi.global_id = dop.VALUE),0) AS pass_group_id, 
       it.item_creator_id AS item_creator_id,
       COUNT(*) AS item_count,
	   iw.item_active
  FROM starting_dynamic_object_parameters dop
       INNER JOIN (SELECT id
                     FROM starting_dynamic_objects
                    WHERE apartment_template_id = __deed_global_id
                   GROUP BY global_id) AS tdo
          ON tdo.id = dop.id
       INNER JOIN items it
          ON it.global_id = dop.VALUE
       INNER JOIN shopping.items_web iw
          ON iw.global_id = dop.VALUE
WHERE dop.param_type_id in (32, 206) # To only get animations and particles
GROUP BY it.global_id;
END
;;
DELIMITER ;