-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS isAnimSupported;

DELIMITER ;;
CREATE PROCEDURE `isAnimSupported`( placementId INT, animGlid INT)
    READS SQL DATA
BEGIN
	DECLARE tempId INT(11) DEFAULT NULL;
	DECLARE adoGlid INT(11) DEFAULT NULL;

	SELECT global_id INTO adoGlid
		FROM dynamic_objects
		WHERE obj_placement_id = placementId;

	SELECT item_id INTO tempId
	  FROM item_animations
	  WHERE item_id = adoGlid
	  AND animation_id = animGlid;
	IF tempId <> 0 THEN 
	  SELECT 1;
	ELSE
	  SELECT 0;
	END IF;
END
;;
DELIMITER ;