-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS insert_player_presences;

DELIMITER ;;
CREATE PROCEDURE `insert_player_presences`( IN __player_id INT(11), IN __direction_x FLOAT,
						IN __direction_y FLOAT, IN __direction_z FLOAT, IN __position_x FLOAT,
						IN __position_y FLOAT, IN __position_z FLOAT, IN __energy INT(11),
						IN __max_energy INT(11), IN __housing_zone_index INT(11),
						IN __housing_zone_index_plain INT(11), IN __respawn_zone_index INT(11),
						IN __respawn_zone_instance_id INT(11), IN __respawn_position_x FLOAT,
						IN __respawn_position_y FLOAT, IN __respawn_position_z FLOAT,
						IN __scale_factor_x FLOAT, IN __scale_factor_y FLOAT, IN __scale_factor_z FLOAT,
						IN __count_down_to_pardon INT(11), IN __arena_base_points INT(11), IN __arena_total_kills INT(11) )
BEGIN

  DECLARE done INT DEFAULT 0;
	DECLARE objPlacementId INT(11);
	DECLARE startingDynamicObjId INT(11);
	DECLARE zoneIndex INT(11);
	DECLARE objectId INT(11);
	DECLARE globalId INT(11);
	DECLARE positionX FLOAT;
	DECLARE positionY FLOAT;
	DECLARE positionZ FLOAT;
	DECLARE rotationX FLOAT;
	DECLARE rotationY FLOAT;
	DECLARE rotationZ FLOAT;

	DECLARE startingDynamicObjects CURSOR FOR
		SELECT shard_info.`get_dynamic_objects_id`(), id, zone_index, object_id, global_id, 
				position_x, position_y, position_z, rotation_x, rotation_y, rotation_z
		FROM	 starting_dynamic_objects
		WHERE  active = 1 AND  zone_index = __housing_zone_index;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	IF __housing_zone_index = 4095 THEN 
		SET __housing_zone_index = 0x30000031;
		SET __housing_zone_index_plain = 0x31;
	END IF;

	INSERT   INTO player_presences              
			(player_id, direction_x, direction_y, direction_z, position_x, position_y, position_z,               
			 energy, max_energy, housing_zone_index, housing_zone_index_plain, respawn_zone_index,               
			 respawn_zone_instance_id, respawn_position_x, respawn_position_y, respawn_position_z,
			 scale_factor_x, scale_factor_y, scale_factor_z, count_down_to_pardon, arena_base_points,
			 arena_total_kills)   
			 VALUES (          
			 __player_id, __direction_x, __direction_y, __direction_z, __position_x, __position_y, __position_z,               
			 __energy, __max_energy, __housing_zone_index, __housing_zone_index_plain, __respawn_zone_index,               
			 __respawn_zone_instance_id, __respawn_position_x, __respawn_position_y, __respawn_position_z,
			 __scale_factor_x, __scale_factor_y, __scale_factor_z, __count_down_to_pardon, __arena_base_points,
			 __arena_total_kills);
	
	OPEN startingDynamicObjects;
	REPEAT
		FETCH startingDynamicObjects INTO objPlacementId,	startingDynamicObjId,	zoneIndex, 	objectId,	
								globalId,	positionX,	positionY,	positionZ,	rotationX,	rotationY,	rotationZ;
		IF NOT done THEN
			INSERT INTO dynamic_objects( obj_placement_id, player_id, zone_index, zone_instance_id, 
						object_id, global_id, position_x, position_y, position_z, rotation_x, 
						rotation_y, rotation_z, created_date) 
					VALUES ( objPlacementId, __player_id, __housing_zone_index, __player_id, 
						objectId, globalId,	positionX, positionY, positionZ, rotationX,	
						rotationY, rotationZ, NOW());
			
			INSERT INTO dynamic_object_parameters (	obj_placement_id, param_type_id, value ) 
			 (  SELECT 	objPlacementId, param_type_id, value 
						FROM 	starting_dynamic_object_parameters 
					 WHERE 	id = startingDynamicObjId);
			
		END IF;
	UNTIL done END REPEAT;
	CLOSE startingDynamicObjects;
	
	INSERT INTO world_object_player_settings (`world_object_id`, `asset_id`, `texture_url`, 
				`zone_index`, `zone_instance_id`, `last_updated_datetime`	)
	(SELECT `world_object_id`, `asset_id`, `texture_url`,
			__housing_zone_index, __player_id, NOW()
			FROM starting_world_object_player_settings
			WHERE active = 1 AND zone_index = __housing_zone_index);
	
	
END
;;
DELIMITER ;