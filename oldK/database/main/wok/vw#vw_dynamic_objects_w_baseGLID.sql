-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS vw_dynamic_objects_w_baseGLID;

DELIMITER ;;

 CREATE VIEW `vw_dynamic_objects_w_baseGLID` AS SELECT `do`.`obj_placement_id` AS `obj_placement_id`,`do`.`player_id` AS `player_id`,`do`.`zone_index` AS `zone_index`,`do`.`zone_instance_id` AS `zone_instance_id`,`do`.`object_id` AS `object_id`,`do`.`inventory_sub_type` AS `inventory_sub_type`,`do`.`global_id` AS `global_id`,`do`.`position_x` AS `position_x`,`do`.`position_y` AS `position_y`,`do`.`position_z` AS `position_z`,`do`.`rotation_x` AS `rotation_x`,`do`.`rotation_y` AS `rotation_y`,`do`.`rotation_z` AS `rotation_z`,`do`.`slide_x` AS `slide_x`,`do`.`slide_y` AS `slide_y`,`do`.`slide_z` AS `slide_z`,`do`.`created_date` AS `created_date`,`do`.`expired_date` AS `expired_date`,if( ( `itm`.`base_global_id` = 0) ,`do`.`global_id`,`itm`.`base_global_id`)  AS `base_global_id`,`itm`.`is_derivable` AS `is_derivable` 
 FROM ( `dynamic_objects` `do` 
 JOIN `items` `itm` on( ( `do`.`global_id` = `itm`.`global_id`) ) ) 
;;
DELIMITER ;