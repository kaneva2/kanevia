-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_user_gender;

DELIMITER ;;
CREATE PROCEDURE `ss_update_user_gender`(  )
begin
  replace into snapshot_user_gender
-- Split out men and women into one table with this query.  Replace your query below.
select count(wll.user_id) as player_count,gender,date(max(wll.created_date))
	from developer.login_log wll
	inner join game_users gu on wll.user_id=gu.user_id
	inner join kaneva.users ku on gu.kaneva_user_id=ku.user_id
	group by gender ;
end
;;
DELIMITER ;