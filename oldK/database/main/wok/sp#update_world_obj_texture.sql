-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS update_world_obj_texture;

DELIMITER ;;
CREATE PROCEDURE `update_world_obj_texture`( IN __worldObjId VARCHAR(255), IN __assetId INT(11),
			  IN __zoneIndex INT(11), IN __instanceId INT(11),	
			  IN __inTextureURL VARCHAR(1000), OUT __ret INT(11), OUT __textureURL VARCHAR(1000))
sproc:BEGIN
	DECLARE __acct_id INT(11) DEFAULT NULL;
	DECLARE __zone_type INT(11) DEFAULT NULL;
	
	-- __ret return values - 2 not found to be consistent with other return codes
	-- 0 == default
	-- 2 == not found
	-- 3 == update happened
	-- 4 == delete happened
	-- 5 == insert happened
	SET __ret = 0;
	-- WOK specific feature so all permanent zones share customizations based of instance 1
	SET __zone_type = zoneType(__zoneIndex);
	IF __zone_type = 4 OR __zone_type = 5 THEN
	
		SET __instanceId = 1;
	
	END IF;
	IF __assetId > 0 THEN
		IF __inTextureURL = '' THEN
			SET __textureURL = fetch_image_url( __assetId, 'T' );
		ELSE
			SET  __textureURL = __inTextureURL;
		END IF;
		
	END IF;
 	SELECT id INTO __acct_id
		FROM world_object_player_settings 
			WHERE world_object_id = __worldObjId AND zone_index = __zoneIndex  AND zone_instance_id = __instanceId;
	IF __acct_id IS NULL THEN
	
		SET __acct_id = 0;
		
	END IF;
	
	IF __acct_id > 0 THEN
	
		IF __assetId > 0 THEN
			UPDATE world_object_player_settings 
				SET asset_id = __assetId, 
						texture_url = COALESCE(__textureURL,''), 
						last_updated_datetime = NOW() 
				WHERE id = __acct_id AND world_object_id = __worldObjId;	
						
			SET __ret = 3;
				
		ELSE
		
			DELETE FROM world_object_player_settings 
				WHERE id = __acct_id AND world_object_id = __worldObjId;			
				
				
			SET __ret = 4;
			
		END IF;
	
	ELSEIF __assetId > 0 THEN	
	
		INSERT INTO world_object_player_settings (world_object_id, asset_id, texture_url, zone_index, zone_instance_id, last_updated_datetime) 
			VALUES (__worldObjId, __assetId, COALESCE(__textureURL,''), __zoneIndex, __instanceId, NOW());	
		SET __ret = 5;
	
	ELSE
	
		SET __ret = 2;
	
	END IF;
			
	
END
;;
DELIMITER ;