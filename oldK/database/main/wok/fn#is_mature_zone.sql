-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS is_mature_zone;

DELIMITER ;;
CREATE FUNCTION `is_mature_zone`( __zoneIndex INT, __instanceId INT ) RETURNS tinyint(1)
    DETERMINISTIC
BEGIN
	DECLARE __pass_group_id INT;
	
	-- make sure in mature area (one w/pass_group of 1)
	IF zoneType(__zoneIndex) = 4 THEN
		SELECT pass_group_id INTO __pass_group_id
		  FROM pass_group_perm_channels
         WHERE zone_index_plain = zoneIndex(__zoneIndex)
		       AND pass_group_id = 1; -- access pass
    ELSE
		SELECT pass_group_id INTO __pass_group_id
		  FROM pass_group_channel_zones
         WHERE zone_type = zoneType(__zoneIndex)
		       AND zone_instance_id = __instanceId
		       AND pass_group_id = 1; -- access pass
	END IF;

	return __pass_group_id IS NOT NULL;
	
END
;;
DELIMITER ;