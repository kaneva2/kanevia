-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS makePlayerGMorAdvisor;

DELIMITER ;;
CREATE PROCEDURE `makePlayerGMorAdvisor`( IN __playerName VARCHAR(80), IN __mode ENUM('GM','Advisor','Normal'))
mpgm: BEGIN
-- SP to change a player to a GM or advisor
DECLARE __playerid INT;
DECLARE __kanevauserid INT;
DECLARE __zoneIndex INT;
DECLARE __loggedOn CHAR(10);

-- get all the data we need
SELECT player_id INTO __playerid FROM wok.players WHERE NAME = __playername;
SELECT kaneva_user_id, logged_on INTO __kanevauserid, __loggedOn FROM wok.game_users WHERE username = __playername;

-- make sure the player is logged out and the other variables have values
IF  __playerid IS NULL OR __kanevauserid IS NULL THEN
    LEAVE mpgm;
END IF;

-- set the color and add the title
IF __mode = 'GM' THEN
	UPDATE wok.player_presences SET color_type = 3 WHERE player_id = __playerid;
	INSERT IGNORE INTO wok.player_titles VALUES (__playerid, 25, 0,0);
    UPDATE wok.game_users set is_gm = 'T', is_admin = 'T', can_spawn = 'T' where kaneva_user_id = __kanevauserid;
END IF;
IF __mode = 'Advisor' THEN
	UPDATE wok.player_presences SET color_type = 2 WHERE player_id = __playerid;
	INSERT IGNORE INTO wok.player_titles VALUES (__playerid, 26, 0,0);
END IF;
IF __mode = 'Normal' THEN
	UPDATE wok.player_presences SET color_type = 0 WHERE player_id = __playerid;
    UPDATE wok.game_users set is_gm = 'F', is_admin = 'F', can_spawn = 'F' where kaneva_user_id = __kanevauserid;
	DELETE FROM wok.player_titles WHERE player_id = __playerid AND title_id IN (25,26,27,35);
    DELETE FROM permanent_zone_roles WHERE kaneva_user_id = __kanevauserid;
END IF;

-- add the entries into the permanent zone roles table, this gives the GM the ability to customize public zones and to kick people
DELETE FROM permanent_zone_roles WHERE kaneva_user_id = __kanevauserid;
IF __mode = 'GM' OR __mode = 'Advisor' THEN
	INSERT INTO permanent_zone_roles ( zone_index, kaneva_user_id, role)  
		SELECT zone_index, __kanevauserid, 1 
		FROM supported_worlds;
END IF;

-- update the game_users table to set their account as a GM account, this gives them ban and build abilities
IF __mode = 'GM' THEN
	UPDATE wok.game_users SET is_gm='T', is_admin='T', can_spawn='T' WHERE kaneva_user_id = __kanevauserid;
END IF;
IF __mode = 'Normal' THEN
	UPDATE wok.game_users SET is_gm='F', is_admin='F', can_spawn='F' WHERE kaneva_user_id = __kanevauserid;
END IF;
END
;;
DELIMITER ;