-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS fnc_own_home;

DELIMITER ;;
CREATE FUNCTION `fnc_own_home`(__kaneva_user_id INT ) RETURNS int(11)
    READS SQL DATA
BEGIN
  DECLARE __home_instance_id INT;
  SELECT coalesce(pz.current_zone_instance_id,0) INTO __home_instance_id
		FROM wok.player_zones pz
		INNER JOIN wok.players p ON pz.player_id = p.player_id
		INNER JOIN wok.game_users gu ON p.user_id = gu.user_id
		WHERE gu.kaneva_user_id = __kaneva_user_id;
  RETURN __home_instance_id;
  
END
;;
DELIMITER ;