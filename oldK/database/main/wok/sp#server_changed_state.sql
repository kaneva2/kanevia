-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS server_changed_state;

DELIMITER ;;
CREATE PROCEDURE `server_changed_state`( IN __server_id INT(11), IN __server_name VARCHAR(64), IN __server_port INT(11), IN __new_state INT, IN __popCount INT(11) )
BEGIN
-- part of schema abstration to remove all Kaneva references from C++ 
-- and move them to SPs, which can be site-specific
-- called to do cleanup when server stops or starts
-- can be called also when a server crashes, and then never comes back up
	CALL server_changed_state_local_ping( __server_id, __server_name, __server_port, 
										  __new_state, __popCount, 0 );
END
;;
DELIMITER ;