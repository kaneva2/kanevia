-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS copyStartingZoneCustomizations;

DELIMITER ;;
CREATE PROCEDURE `copyStartingZoneCustomizations`( IN __playerName VARCHAR(80), IN __templateId INT )
cszm: BEGIN
	
	
	
	
	DECLARE __playerId INT;
	DECLARE __zoneIndex INT;
	DECLARE __instanceId INT;
	
	SELECT player_id INTO __playerId
	FROM players WHERE name = __playerName;

	IF __playerId IS NULL THEN
		SELECT concat( "Player not found ", __playerName ) as error;
		LEAVE cszm;
	END IF;
	
	SELECT housing_zone_index, player_id INTO __zoneIndex, __instanceId
	FROM player_presences
	WHERE player_id = __playerId;

	IF __zoneIndex IS NULL THEN
		SELECT concat( "Housing zone for player not found ", __playerName ) as error;
		LEAVE cszm;
	END IF;

	UPDATE starting_dynamic_objects SET active = 0 WHERE zone_index = __zoneIndex AND apartment_template_id = __templateId; -- turn off all others
	UPDATE starting_world_object_player_settings SET active = 0 WHERE zone_index = __zoneIndex AND apartment_template_id = __templateId; -- turn off all others

	-- do dynamic objects first
	INSERT INTO starting_dynamic_objects ( `active`, `zone_index`, `object_id`,`global_id`,`position_x`,`position_y`,`position_z`,
											`rotation_x`,`rotation_y`,`rotation_z`,`texture_asset_id`, `texture_url`, 
											`swf_name`, `swf_parameter`, `apartment_template_id`)
			SELECT 1, __zoneIndex, `object_id`, `global_id`,`position_x`,`position_y`,`position_z`,
											`rotation_x`,`rotation_y`,`rotation_z`,
											`texture_asset_id`, `texture_url`,
											`swf_name`, `swf_parameter`, __templateId
	  					FROM dynamic_objects WHERE zone_index = __zoneIndex AND zone_instance_id = __instanceId;

	SELECT concat( 'Inserted ', cast( ROW_COUNT() as char ) ) as DynObjects;
	
	-- now do textures
	INSERT INTO starting_world_object_player_settings (  `active`, `zone_index`, `world_object_id`, `asset_id`, `texture_url`, `apartment_template_id` )
			SELECT 1,  __zoneIndex,  `world_object_id`, `asset_id`, `texture_url`, __templateId
	  					FROM world_object_player_settings WHERE zone_index = __zoneIndex AND zone_instance_id = __instanceId;

	SELECT concat( 'Inserted ', cast( ROW_COUNT() as char ) ) as Textures;
	  					
END
;;
DELIMITER ;