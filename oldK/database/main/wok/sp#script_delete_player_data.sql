-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS script_delete_player_data;

DELIMITER ;;
CREATE PROCEDURE `script_delete_player_data`( IN __player_id INT(11),
                        IN __game_id INT(11),
                        IN __attribute VARCHAR(16) )
BEGIN
-- called to delete player data for a given game
  DELETE FROM script_player_data
    WHERE player_id = __player_id AND
      game_id = __game_id AND
      attribute = __attribute;
            
END
;;
DELIMITER ;