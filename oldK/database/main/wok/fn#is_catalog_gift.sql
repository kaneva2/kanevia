-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS is_catalog_gift;

DELIMITER ;;
CREATE FUNCTION `is_catalog_gift`( __glid INT ) RETURNS tinyint(1)
    DETERMINISTIC
BEGIN
	return __glid IN (1089,1090,1091,1092,1093,1094,1095,1096,1098,1099,1100,1101,1102); -- these are the current canned catalog gift glids, copied from inventory.lua gift table
END
;;
DELIMITER ;