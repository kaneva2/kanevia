-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS ss_update_user_inactive;

DELIMITER ;;
CREATE PROCEDURE `ss_update_user_inactive`(  )
begin
  replace into wok.snapshot_user_inactive
	select count(gu.user_id) as player_count,avg(time_played) as pta,date(now())
	from wok.game_users gu
	where time_played<120;
end
;;
DELIMITER ;