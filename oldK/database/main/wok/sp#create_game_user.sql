-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS create_game_user;

DELIMITER ;;
CREATE PROCEDURE `create_game_user`( IN __kaneva_user_id INT(11), IN __user_name VARCHAR(80), 
									       IN __server_id INT(11), IN __last_ip VARCHAR(30),
									       IN __gender CHAR, IN __birth_date DATETIME, 
									       IN __show_mature INT, OUT __user_id INT(11) )
BEGIN
    DECLARE prev_balance DECIMAL(12,3) DEFAULT NULL;
    DECLARE new_balance DECIMAL(12,3) DEFAULT NULL;
    DECLARE pointType VARCHAR( 50 ) DEFAULT NULL;
	DECLARE done INT DEFAULT 0;
        
	DECLARE cur1 CURSOR FOR SELECT balance, kei_point_id FROM starting_balances;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	INSERT INTO `game_users` ( `kaneva_user_id`, `username`, `is_gm`, `can_spawn`, `is_admin`, `time_played`, `last_logon`, `server_id`, `last_update_time`, `logged_on`, `created_date`, `last_ip` )
    	              VALUES ( __kaneva_user_id, __user_name, 'F',    'F',         'F',        0,             NOW(),         __server_id, NOW(),             'T',         NOW(),          __last_ip );
	
	SET __user_id = LAST_INSERT_ID();
	
	OPEN cur1;
	REPEAT	
		FETCH cur1 into new_balance, pointType;
		IF NOT done THEN 
			SET prev_balance = NULL;
	        SELECT balance INTO prev_balance
	        FROM vw_user_balances
	        WHERE user_id = __kaneva_user_id
	          AND kei_point_id = pointType
	         FOR UPDATE;
	        IF prev_balance IS NULL THEN
	                INSERT INTO vw_user_balances (user_id, kei_point_id, balance )
	                VALUES ( __kaneva_user_id, pointType, new_balance );
					SET prev_balance = 0;
				
	        ELSEIF prev_balance < new_balance THEN  -- 6/08 change from 10000 to new_balance
	        
	                UPDATE vw_user_balances 
	                  SET balance = new_balance
	                WHERE user_id = __kaneva_user_id
	                  AND kei_point_id = pointType;
	                  
	        END IF;	
	        INSERT INTO vw_transaction_log 
	        			( created_date, user_id,          balance_prev, balance,     transaction_amount,         kei_point_id, transaction_type )
	             VALUES ( NOW(),        __kaneva_user_id, prev_balance, new_balance, new_balance - prev_balance, pointType,    8); --  'TT_OPENING_BALANCE' );
	      	SET done = 0;
		END IF;
	UNTIL done END REPEAT;
	
	CLOSE cur1;

END
;;
DELIMITER ;