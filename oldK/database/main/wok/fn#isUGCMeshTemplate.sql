-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS isUGCMeshTemplate;

DELIMITER ;;
CREATE FUNCTION `isUGCMeshTemplate`( globalId INT ) RETURNS smallint(1) unsigned
    NO SQL
    DETERMINISTIC
BEGIN
  DECLARE ret SMALLINT(1) DEFAULT 0;

  if globalId >= 1000000000 then
	SET ret = 1;
  end if;
  return ret;
END
;;
DELIMITER ;