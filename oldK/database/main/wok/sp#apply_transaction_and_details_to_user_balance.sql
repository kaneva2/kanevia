-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS apply_transaction_and_details_to_user_balance;

DELIMITER ;;
CREATE PROCEDURE `apply_transaction_and_details_to_user_balance`( IN user_id int, IN transaction_amount float, 
							IN transaction_type smallint unsigned, 
							IN v_kei_point_id INT, 
							IN v_glid INT,
							IN v_qty INT,
							OUT return_code INT, OUT user_balance float, OUT tranId INT )
BEGIN
-- The following is a stored procedure that applys a transaction to a user balance
-- Return code can be one of three values
-- 1 indicates the transaction is invalid due to insufficient funds
-- 0 indicates the transaction was valid and recorded
-- -1 indicates the transaction was invalid due to an error
	DECLARE __point_id VARCHAR(20) DEFAULT NULL;
	IF v_kei_point_id = 1 THEN 
		SET __point_id = 'GPOINT';
	ELSEIF v_kei_point_id = 2 THEN 
		SET __point_id = 'KPOINT';
	ELSE
		CALL logMsg( 'apply_transaction_and_details_to_user_balance', 'ERROR', CONCAT( 'Incorrect value for point id ', cast( v_kei_point_id as char ) ) );
		SET return_code = -1;
	END IF;
	IF __point_id IS NOT NULL THEN 
		-- this still takes string values
		CALL kaneva.apply_transaction_and_details_to_user_balance( user_id, transaction_amount, transaction_type, 
							__point_id, 
							v_glid, v_qty,
							return_code, user_balance, tranId );
	END IF;							
END
;;
DELIMITER ;