-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS bad_trade_fix;

DELIMITER ;;
CREATE PROCEDURE `bad_trade_fix`()
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE __player_id INT;
	DECLARE __trader_id INT;
	DECLARE __global_id INT;
	DECLARE __qty INT;
	DECLARE __tid INT;
	DECLARE __when datetime;
	DECLARE cur1 CURSOR FOR SELECT trans_id, player_id, trader_id, global_id, quantity_delta, created_date
								FROM bad_trades WHERE quantity_delta < 0 order by trans_id;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	OPEN cur1;
	REPEAT	
		FETCH cur1 into __tid, __player_id, __trader_id, __global_id, __qty, __when;
		
		IF NOT done THEN 
			
			
			delete from bad_trades 
			  where player_id = __trader_id
			    and global_id = __global_id
			    and trader_id = __player_id
			    and quantity_delta = -__qty
			    and created_date between TIMESTAMPADD(SECOND, -5, __when) AND TIMESTAMPADD(SECOND, 5, __when);
		END IF;
		IF row_count() = 1 THEN
			
			delete from bad_trades 
			  where trans_id = __tid;
		END IF;
	UNTIL done END REPEAT;
	
	CLOSE cur1;
	
	
END
;;
DELIMITER ;