-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS getInventory;

DELIMITER ;;
CREATE PROCEDURE `getInventory`( IN playerId INT(11), IN kanevaUserId INT(11) )
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE globalId int(11);
	DECLARE invType ENUM('P','B','H');
	DECLARE qty int(11);
	DECLARE invSubType int(11);
	DECLARE giftGiver int(11);
	DECLARE addCount int DEFAULT 0;
	
	DECLARE pendingAdds CURSOR FOR
		SELECT global_id, inventory_type, quantity, inventory_sub_type, gift_giver
		  FROM inventory_pending_adds
		 WHERE active = 'T' 
		   AND kaneva_user_id = kanevaUserId;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- ERROR
      GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
      CALL logging.logMsg('ERROR', SCHEMA(), 'getInventory', CONCAT('MYSQL_ERRNO[' , CAST(@errno AS CHAR), '] RETURNED_SQLSTATE[', @sqlstate, '] MESSAGE_TEXT[', @text, ']'));
      RESIGNAL;
  END;


 	OPEN pendingAdds;
	REPEAT	
		FETCH pendingAdds into globalId, invType, qty, invSubType, giftGiver;
		IF NOT done THEN 
		
			SET addCount = addCount + 1;
			INSERT INTO inventories
					( global_id,inventory_type,player_id, armed, quantity, charge_quantity, corpse_id, gift_giver, created_date, inventory_sub_type )
 			 VALUES (globalId, invType,       playerId,  'F',    qty,      0,              0,          giftGiver,  NOW(),       invSubType )
			 ON DUPLICATE KEY UPDATE quantity = quantity + qty;
			
		END IF;
	UNTIL done END REPEAT;
		
	CLOSE pendingAdds;
	CALL logDebugMsg( 'getInventory', concat( 'pending adds converted for ', cast( playerId as char ), ' is ', cast( addCount as char ) ) );
	
	IF addCount > 0 THEN
		UPDATE inventory_pending_adds
		   SET active = 'F',
		       picked_up_date = NOW()
		 WHERE active = 'T' 
		   AND kaneva_user_id = kanevaUserId;
	END IF;
	
	SELECT `player_id`, `inventory_type`, `global_id`, `inventory_sub_type`, `armed`, `quantity`, `charge_quantity`, `corpse_id` 
	  FROM inventories
	 WHERE player_id = playerId;
	 
END
;;
DELIMITER ;