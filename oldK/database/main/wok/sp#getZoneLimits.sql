-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS getZoneLimits;

DELIMITER ;;
CREATE PROCEDURE `getZoneLimits`(  IN    zoneIndex   INT
                                 , IN  instanceId  INT
                                 , OUT softLimit   INT
                                 , OUT hardLimit   INT )
BEGIN
    DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;

	-- default values
	SET softLimit = 10;
	SET hardLimit = 15;

   CALL logging.logMsg( 'TRACE'
                        , SCHEMA()
                        , 'getZoneLimits'
                        , concat('Begin (' , zoneIndex, ',' , instanceId , ')' ) 
                        );
   
	-- first try zoneIndex+instance
	SELECT occupancy_limit_soft, occupancy_limit_hard INTO softLimit, hardLimit 
	  FROM occupancy_limits ol
	 WHERE ol.zone_index = zoneIndex
	   AND ol.zone_instance_id = instanceId
	 LIMIT 1;

	IF FOUND_ROWS() = 0 THEN
		-- now try zoneindex  (order by??)
		SELECT occupancy_limit_soft, occupancy_limit_hard INTO softLimit, hardLimit 
		  FROM occupancy_limits ol
		 WHERE ol.zone_index = zoneIndex
		   AND zone_instance_id = 0
		 ORDER BY occupancy_limit_soft DESC
		 LIMIT 1;
		 
		IF FOUND_ROWS() = 0 THEN
			-- now just zone (order by??)
			SELECT occupancy_limit_soft, occupancy_limit_hard INTO softLimit, hardLimit 
			  FROM occupancy_limits ol
			 WHERE ol.zone_index_plain = zoneIndex(zoneIndex)
			   AND zone_instance_id = 0
		 	 ORDER BY occupancy_limit_soft DESC
			 LIMIT 1;

			 IF FOUND_ROWS() = 0 THEN
			 	-- nothing!, add default value from supported worlds
			 	SELECT default_max_occupancy INTO softLimit
			 	  FROM supported_worlds
			 	 WHERE zone_index = zoneIndex(zoneIndex);

				SET hardLimit = softLimit * 1.2;
				
			 	INSERT INTO occupancy_limits (`zone_index`, `zone_instance_id`, `zone_index_plain`, `occupancy_limit_soft`, `occupancy_limit_hard`)
			 	  VALUES ( zoneIndex, 0, zoneIndex(zoneIndex), softLimit, hardLimit );
			 END IF;
		END IF;
	 END IF;
END
;;
DELIMITER ;