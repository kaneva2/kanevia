-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS isServerAvailable;

DELIMITER ;;
CREATE FUNCTION `isServerAvailable`( serverId INT(11) ) RETURNS tinyint(1)
    READS SQL DATA
BEGIN
	DECLARE statusId INT DEFAULT NULL;
	
	IF serverId  <> 0 THEN 
	
		SELECT status_id INTO statusId
		  FROM vw_game_engines
		 WHERE server_id = serverId;
		
	END IF;
	RETURN statusId = 1; -- 1 = running
END
;;
DELIMITER ;