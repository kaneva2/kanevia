-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS move_dynamic_object;

DELIMITER ;;
CREATE PROCEDURE `move_dynamic_object`( IN __x float, IN __y float, IN __z float, IN __rotX float, IN __rotY float, IN __rotZ float,
												IN __slideX float, IN __slideY float, IN __slideZ float,
											IN __objPlacementId INT(11), IN __zoneIndex INT(11), IN __instanceId INT(11) )
BEGIN
	DECLARE __zone_type INT(11) DEFAULT NULL;
	
	-- WOK specific feature so all permanent zones share customizations based of instance 1
	SET __zone_type = zoneType(__zoneIndex);
	IF __zone_type = 4 OR __zone_type = 5 THEN
	
		SET __instanceId = 1;
	
	END IF;

	UPDATE dynamic_objects SET
		position_x = __x,
		position_y = __y,
		position_z = __z,
		rotation_x = __rotX,
		rotation_y = __rotY, 
  		rotation_z = __rotZ,
			slide_x = __slideX,
			slide_y = __slideY,
			slide_z = __slideZ
	WHERE obj_placement_id = __objPlacementId
		AND zone_index = __zoneIndex
		AND zone_instance_id = __instanceId;

END
;;
DELIMITER ;