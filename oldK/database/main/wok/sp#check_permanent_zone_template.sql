-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS check_permanent_zone_template;

DELIMITER ;;
CREATE PROCEDURE `check_permanent_zone_template`( IN __zone_index_plain INT(11),	IN __visibility INT(11), IN __name VARCHAR(200) )
BEGIN
-- called to see if premanent zone db entry needs to be updated or added

	DECLARE zoneIndexPlain INT(11) DEFAULT NULL;
	DECLARE zoneVisibility INT(11) DEFAULT NULL;

	SELECT zone_index_plain, visibility INTO zoneIndexPlain, zoneVisibility
	FROM channel_zones
	WHERE zone_index_plain = __zone_index_plain and
				zone_type = 4 and -- only for permanent zones
				zone_instance_id < 10000 -- no player instances
				LIMIT 1;
				
				
	IF zoneIndexPlain IS NOT NULL THEN
	
		IF zoneVisibility <> __visibility THEN
		
			CALL update_permanent_zone_template( __zone_index_plain, __visibility );
			
		END IF;
	
	ELSE
	
		CALL insert_permanent_zone_template( __zone_index_plain,	__visibility, __name );
	
	END IF;
				
				
END
;;
DELIMITER ;