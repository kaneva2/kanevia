-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS is_wok_game;

DELIMITER ;;
CREATE FUNCTION `is_wok_game`( __game_id INT ) RETURNS tinyint(1)
    DETERMINISTIC
BEGIN
	return __game_id = environment.getWOKGameId();
END
;;
DELIMITER ;