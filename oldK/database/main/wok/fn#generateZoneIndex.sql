-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS generateZoneIndex;

DELIMITER ;;
CREATE FUNCTION `generateZoneIndex`() RETURNS int(10) unsigned
    DETERMINISTIC
BEGIN
	return TRUNCATE(rand() * UNIX_TIMESTAMP(now()),0);
END
;;
DELIMITER ;