-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS deletePlayer;

DELIMITER ;;
CREATE PROCEDURE `deletePlayer`( IN playerName VARCHAR(80) )
BEGIN
-- admin helper proc
	DECLARE playerId INT(11) DEFAULT NULL;
	SELECT player_id INTO playerId
	  FROM players
	 WHERE name = playerName;
	 IF playerId IS NOT NULL THEN 
	 	CALL deletePlayerById( playerId );
	 END IF;
END
;;
DELIMITER ;