-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS _tmp_cleanup_orphaned_items;

DELIMITER ;;
CREATE PROCEDURE `_tmp_cleanup_orphaned_items`()
BEGIN
DECLARE	_player_id		BIGINT;
DECLARE	__table_RowCnt		INT;
DECLARE __del_cnt		INT;
DECLARE i 			int;
DECLARE done INT DEFAULT 0;
DECLARE arch_cursor CURSOR FOR SELECT DISTINCT player_id FROM wok.dynamic_objects;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
SET done = 0;
SET i = 0;
OPEN arch_cursor;
SET __table_RowCnt = (SELECT FOUND_ROWS());
SELECT CONCAT('Starting cleanup ... ',CAST(__table_RowCnt AS CHAR),' players to process');
REPEAT
    FETCH arch_cursor INTO _player_id;
    IF NOT done THEN
	DELETE FROM wok.dynamic_objects WHERE player_id = _player_id AND expired_date < NOW();
	
	SET __del_cnt = COALESCE(ROW_COUNT(),0);
	IF __del_cnt > 0 THEN 
		SELECT CONCAT('Deleted: ',CAST(__del_cnt AS CHAR),' orphaned rows from player id: ',CAST(_player_id AS CHAR)); 
		set i = i + __del_cnt;
		END IF;
    END IF;	
UNTIL done END REPEAT;
CLOSE arch_cursor;
SELECT CONCAT('Deleted: ',CAST(i AS CHAR),' rows from dynamic_objects'); 
END
;;
DELIMITER ;