-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_starting_player_info;

DELIMITER ;;
CREATE PROCEDURE `get_starting_player_info`( IN __template_id INT(11), IN __player_id INT(11),
                                        OUT __xScale FLOAT, OUT __yScale FLOAT, OUT __zScale FLOAT )
BEGIN
	DECLARE __edb INT(11);
	DECLARE __race_id INT(11);
	DECLARE __team_id INT(11);
  
  SET __xScale = 1;
  SET __yScale = 1;
  SET __zScale = 1;

	SELECT team_id, original_edb, race_id, scale_factor_x, scale_factor_y, scale_factor_z
	INTO __team_id, __edb, __race_id, __xScale, __yScale, __zScale
	FROM starting_player_info
	WHERE player_template_id = __template_id;
  
	UPDATE players 
		SET original_edb = __edb,
        team_id = __team_id,
        race_id = __race_id,
        last_update_time = NOW()  
		WHERE player_id = __player_id;  
	
	UPDATE player_presences 
		SET scale_factor_x = __xScale,
        scale_factor_y = __yScale,
        scale_factor_z = __zScale
		WHERE player_id = __player_id; 
    
END
;;
DELIMITER ;