-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS local_ping;

DELIMITER ;;
CREATE PROCEDURE `local_ping`( IN __server_id INT(11), IN __server_name VARCHAR(64), IN __server_port INT, IN __new_state INT(11), IN __pop_count INT(11) )
BEGIN

	IF __server_id <> 0 THEN -- running
		UPDATE developer.game_servers
		   SET last_ping_datetime = NOW(),
     	       number_of_players = __pop_count,
     	       server_status_id = __new_state
		 WHERE server_id = __server_id;
	END IF;

END
;;
DELIMITER ;