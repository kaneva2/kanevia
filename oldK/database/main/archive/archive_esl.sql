-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DELIMITER $$

USE `archive`$$

DROP PROCEDURE IF EXISTS `archive_email_send_log`$$

CREATE DEFINER=`admin`@`%` PROCEDURE `archive_email_send_log`( __archive_cutoff_time DATETIME)
BEGIN
DECLARE	__Max_id		BIGINT;
DECLARE	__table_RowCnt		BIGINT;
DECLARE __message_text 		VARCHAR(500);

SELECT IFNULL(MAX(email_id),0) INTO __Max_id
	FROM	viral_email_archive.email_send_log;
	
SELECT	IFNULL(COUNT(email_id),0) INTO __table_RowCnt
	FROM	viral_email.email_send_log
	WHERE	email_id > __Max_id;
	
SELECT CONCAT('Starting archive ... ',CAST(__table_RowCnt AS CHAR),' rows to copy to archive. ; Archive cutoff time: ',
				CAST(__archive_cutoff_time AS CHAR),'; Starting with id: ',
				CAST(__Max_id AS CHAR)) INTO __message_text;
CALL add_maintenance_log (DATABASE(),'archive_email_send_log',__message_text);
-- Loop while anything to delete.
WHILE (__table_RowCnt > 0 AND NOW() < __archive_cutoff_time)    DO
	INSERT INTO viral_email_archive.email_send_log SELECT * FROM viral_email.email_send_log WHERE email_id > __Max_id LIMIT 75000;
	SELECT IFNULL(ROW_COUNT(),0) INTO __table_RowCnt;
	SELECT  CONCAT('Copied: ',CAST(__table_RowCnt AS CHAR),' rows.') INTO __message_text;
	CALL add_maintenance_log (DATABASE(),'archive_email_send_log',__message_text);
	SELECT IFNULL(MAX(email_id),0) INTO __Max_id FROM viral_email_archive.email_send_log;
	END WHILE;
CALL add_maintenance_log (DATABASE(),'archive_email_send_log','Archive completed.');
END$$

DELIMITER ;