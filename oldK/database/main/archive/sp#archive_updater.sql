-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS archive_updater;

DELIMITER //
CREATE PROCEDURE archive_updater()
BEGIN

-- CALL developer_archive.archive_game_login_log(ADDDATE(NOW(), INTERVAL 10 MINUTE));

-- CALL kaneva_archive.archive_user_searches_archived(ADDDATE(NOW(), INTERVAL 10 MINUTE));
CALL archive.archive_wok_transaction_log(ADDDATE(NOW(), INTERVAL 10 MINUTE));
CALL archive.archive_messages(ADDDATE(NOW(), INTERVAL 5 MINUTE));
-- CALL kaneva_archive.archive_wok_transaction_log_details(ADDDATE(NOW(), INTERVAL 10 MINUTE));

-- CALL fame_archive.archive_fame_packet_history(ADDDATE(NOW(), INTERVAL 10 MINUTE));

-- CALL viral_email_archive.archive_email_bounce_log(ADDDATE(NOW(), INTERVAL 10 MINUTE));
CALL archive.archive_email_send_log(ADDDATE(NOW(), INTERVAL 10 MINUTE));
-- CALL viral_email_archive.archive_summary_bounce_log_daily(ADDDATE(NOW(), INTERVAL 10 MINUTE));

-- CALL wok_archive.archive_ddr_scores(ADDDATE(NOW(), INTERVAL 10 MINUTE));
CALL archive.archive_inventories_transaction_log(ADDDATE(NOW(), INTERVAL 10 MINUTE));
-- CALL wok_archive.archive_login_log(ADDDATE(NOW(), INTERVAL 10 MINUTE));

END
//

DELIMITER ;
-- CALL archive.archive_updater;
