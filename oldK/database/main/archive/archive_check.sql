-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 


CALL developer_archive.archive_game_login_log(ADDDATE(NOW(), INTERVAL 10 MINUTE));
SELECT COUNT(*), 'developer.game_login_log' FROM developer.game_login_log UNION
SELECT COUNT(*), 'developer_archive.game_login_log' FROM developer_archive.game_login_log;

CALL kaneva_archive.archive_user_searches_archived(ADDDATE(NOW(), INTERVAL 10 MINUTE));
SELECT COUNT(*), 'kaneva.user_searches_archived' FROM kaneva.user_searches_archived UNION
SELECT COUNT(*), 'kaneva_archive.user_searches_archived' FROM kaneva_archive.user_searches_archived;

CALL kaneva_archive.archive_wok_transaction_log(ADDDATE(NOW(), INTERVAL 10 MINUTE));
SELECT COUNT(*), 'kaneva.wok_transaction_log' FROM kaneva.wok_transaction_log UNION
SELECT COUNT(*), 'kaneva_archive.wok_transaction_log' FROM kaneva_archive.wok_transaction_log;

CALL kaneva_archive.archive_wok_transaction_log_details(ADDDATE(NOW(), INTERVAL 10 MINUTE));
SELECT COUNT(*), 'kaneva.wok_transaction_log_details' FROM kaneva.wok_transaction_log_details UNION
SELECT COUNT(*), 'kaneva_archive.wok_transaction_log_details' FROM kaneva_archive.wok_transaction_log_details;

CALL fame_archive.archive_fame_packet_history(ADDDATE(NOW(), INTERVAL 10 MINUTE));
SELECT COUNT(*), 'fame.packet_history' FROM fame.packet_history UNION
SELECT COUNT(*), 'fame_archive.packet_history' FROM fame_archive.packet_history;
