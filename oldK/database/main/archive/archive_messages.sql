-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DELIMITER $$

USE `archive`$$

DROP PROCEDURE IF EXISTS `archive_messages`$$

CREATE DEFINER=`admin`@`%` PROCEDURE `archive_messages`( __archive_cutoff_time DATETIME)
BEGIN
DECLARE	__Max_id		BIGINT;
DECLARE	__table_RowCnt		BIGINT;
DECLARE __message_text 		VARCHAR(500);

SELECT IFNULL(MAX(message_id),0) INTO __Max_id
	FROM	kaneva_archive.messages;
	
SELECT	IFNULL(COUNT(message_id),0) INTO __table_RowCnt
	FROM	kaneva.messages
	WHERE	message_id > __Max_id;
	
SELECT CONCAT('Starting archive ... ',CAST(__table_RowCnt AS CHAR),' rows to copy to archive. ; Archive cutoff time: ',
				CAST(__archive_cutoff_time AS CHAR),'; Starting with id: ',
				CAST(__Max_id AS CHAR)) INTO __message_text;
CALL add_maintenance_log (DATABASE(),'archive_messages',__message_text);
-- Loop while anything to delete.
WHILE (__table_RowCnt > 0 AND NOW() < __archive_cutoff_time)    DO
	INSERT INTO kaneva_archive.messages SELECT * FROM kaneva.messages WHERE message_id > __Max_id LIMIT 75000;
	SELECT IFNULL(ROW_COUNT(),0) INTO __table_RowCnt;
	SELECT  CONCAT('Copied: ',CAST(__table_RowCnt AS CHAR),' rows.') INTO __message_text;
	CALL add_maintenance_log (DATABASE(),'archive_messages',__message_text);
	SELECT IFNULL(MAX(message_id),0) INTO __Max_id FROM kaneva_archive.messages;
	END WHILE;
CALL add_maintenance_log (DATABASE(),'archive_messages','Archive completed.');
END$$

DELIMITER ;