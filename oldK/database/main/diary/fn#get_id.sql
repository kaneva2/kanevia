-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS get_id;

DELIMITER ;;
CREATE FUNCTION `get_id`() RETURNS bigint(20)
    READS SQL DATA
BEGIN
  UPDATE diary.diary_id SET id=@_diary_id:=id+1;
  return @_diary_id;
END
;;
DELIMITER ;