-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `active_blasts` (
  `entry_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `sender_id` int(20) unsigned NOT NULL,
  `sender_name` varchar(80) NOT NULL,
  `subj` varchar(512) DEFAULT NULL,
  `diary_entry` text,
  `highlight_css` varchar(45) NOT NULL,
  `blast_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_name` varchar(80) NOT NULL DEFAULT '' COMMENT 'Community Name',
  `is_community_personal` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Is community a profile',
  `number_of_replies` int(11) NOT NULL DEFAULT '0',
  `display_thumbnail` varchar(255) NOT NULL DEFAULT '',
  `recipient_id` int(20) unsigned NOT NULL DEFAULT '0' COMMENT 'User Id if the blast is to a specific user',
  `originator_id` int(20) unsigned DEFAULT NULL,
  `originator_name` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`entry_id`,`_o`,`sender_id`,`community_id`,`recipient_id`),
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`),
  KEY `__o` (`_o`) USING BTREE,
  KEY `nr` (`number_of_replies`),
  KEY `sen` (`sender_id`) USING BTREE,
  KEY `IDX_community` (`community_id`)
) ENGINE=InnoDB;

CREATE TABLE `active_blasts_archive` (
  `entry_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `sender_id` int(20) unsigned NOT NULL,
  `sender_name` varchar(80) NOT NULL,
  `subj` varchar(512) DEFAULT NULL,
  `diary_entry` text,
  `highlight_css` varchar(45) NOT NULL,
  `blast_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `community_name` varchar(80) NOT NULL DEFAULT '' COMMENT 'Community Name',
  `is_community_personal` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Is community a profile',
  `number_of_replies` int(11) NOT NULL DEFAULT '0',
  `display_thumbnail` varchar(255) NOT NULL DEFAULT '',
  `recipient_id` int(20) unsigned NOT NULL DEFAULT '0' COMMENT 'User Id if the blast is to a specific user',
  `originator_id` int(20) unsigned DEFAULT NULL,
  `originator_name` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`entry_id`,`_o`,`sender_id`,`community_id`,`recipient_id`),
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`),
  KEY `__o` (`_o`) USING BTREE,
  KEY `nr` (`number_of_replies`),
  KEY `sen` (`sender_id`) USING BTREE
) ENGINE=InnoDB;

CREATE TABLE `active_blasts_replies` (
  `diary_reply_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `reply_to_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `message` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `_date_created` bigint(20) unsigned NOT NULL DEFAULT '4291869598',
  PRIMARY KEY (`_date_created`,`user_id`,`diary_reply_id`) USING BTREE,
  KEY `dtc` (`date_created`),
  KEY `us` (`user_id`),
  KEY `dc` (`_date_created`),
  KEY `abr_reply_to_id_idx` (`reply_to_id`)
) ENGINE=InnoDB;

CREATE TABLE `active_blasts_replies_archive` (
  `diary_reply_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `reply_to_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `message` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `_date_created` bigint(20) unsigned NOT NULL DEFAULT '4291869598',
  PRIMARY KEY (`_date_created`,`user_id`,`diary_reply_id`) USING BTREE,
  KEY `dtc` (`date_created`),
  KEY `us` (`user_id`),
  KEY `_dc` (`_date_created`)
) ENGINE=InnoDB;

CREATE TABLE `diary_id` (
  `id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB ROW_FORMAT=FIXED;

CREATE TABLE `maintenance_log` (
  `maintenance_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_occurred` datetime NOT NULL,
  `database_name` varchar(50) DEFAULT NULL,
  `procedure_name` varchar(200) DEFAULT NULL,
  `message_text` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`maintenance_log_id`)
) ENGINE=InnoDB;

CREATE TABLE `passive_blasts` (
  `entry_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `sender_id` int(20) unsigned NOT NULL,
  `sender_name` varchar(80) NOT NULL,
  `subj` varchar(512) DEFAULT NULL,
  `diary_entry` text,
  `blast_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`_o`,`sender_id`,`entry_id`) USING BTREE,
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`),
  KEY `__o` (`_o`) USING BTREE,
  KEY `sen` (`sender_id`) USING BTREE
) ENGINE=InnoDB;

CREATE TABLE `passive_blasts_archive` (
  `entry_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `sender_id` int(20) unsigned NOT NULL,
  `sender_name` varchar(80) NOT NULL,
  `subj` varchar(512) DEFAULT NULL,
  `diary_entry` text,
  `blast_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`_o`,`sender_id`,`entry_id`) USING BTREE,
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`),
  KEY `__o` (`_o`) USING BTREE,
  KEY `sen` (`sender_id`) USING BTREE
) ENGINE=InnoDB;

CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  `interface_change` enum('T','F') NOT NULL DEFAULT 'F',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

CREATE TABLE `sp_run_log` (
  `sp_run_log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `sp_name` varchar(256) NOT NULL COMMENT 'name of the stored procedure',
  `run_start_time` datetime NOT NULL COMMENT 'when started',
  `run_end_time` datetime NOT NULL COMMENT 'when ended',
  `duration_seconds` int(11) NOT NULL COMMENT 'duration in seconds',
  PRIMARY KEY (`sp_run_log_id`),
  KEY `IDX_start_time` (`run_start_time`)
) ENGINE=InnoDB ROW_FORMAT=COMPACT COMMENT='run duration information for stored procedures';

CREATE TABLE `sticky_blasts` (
  `entry_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `date_expires` datetime NOT NULL,
  `subj` varchar(512) DEFAULT NULL,
  `diary_entry` text,
  `sticky` enum('Y','N') DEFAULT NULL,
  `highlight_css` varchar(45) NOT NULL,
  `sender` int(10) unsigned NOT NULL DEFAULT '0',
  `display_thumbnail` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`_o`,`sender`,`entry_id`) USING BTREE,
  KEY `dt` (`date_created`),
  KEY `de` (`date_expires`),
  KEY `ac` (`sticky`) USING BTREE,
  KEY `__o` (`_o`) USING BTREE
) ENGINE=InnoDB;

CREATE TABLE `sticky_blasts_archive` (
  `entry_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `date_expires` datetime NOT NULL,
  `subj` varchar(512) DEFAULT NULL,
  `diary_entry` text,
  `sticky` enum('Y','N') DEFAULT NULL,
  `highlight_css` varchar(45) NOT NULL,
  `sender` int(10) unsigned NOT NULL,
  `display_thumbnail` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`_o`,`sender`,`entry_id`) USING BTREE,
  KEY `dt` (`date_created`),
  KEY `de` (`date_expires`),
  KEY `ac` (`sticky`) USING BTREE,
  KEY `sender` (`sender`)
) ENGINE=InnoDB;

CREATE TABLE `user_blast_counts` (
  `counts` int(10) unsigned DEFAULT '0',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

CREATE TABLE `user_diary` (
  `entry_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `sender` int(20) unsigned NOT NULL,
  `subj` varchar(512) DEFAULT NULL,
  `diary_entry` text,
  `highlight_css` varchar(45) NOT NULL,
  `blast_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `number_of_replies` int(11) NOT NULL DEFAULT '0',
  `display_thumbnail` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`_o`,`sender`,`community_id`,`entry_id`) USING BTREE,
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`),
  KEY `__o` (`_o`) USING BTREE,
  KEY `nr` (`number_of_replies`),
  KEY `sen` (`sender`) USING BTREE,
  KEY `eid2` (`entry_id`)
) ENGINE=InnoDB COMMENT='InnoDB free: 3072 kB';

CREATE TABLE `user_diary_archive` (
  `entry_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `sender` int(20) unsigned NOT NULL,
  `subj` varchar(512) DEFAULT NULL,
  `diary_entry` text,
  `highlight_css` varchar(45) NOT NULL,
  `blast_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `number_of_replies` int(11) NOT NULL,
  `display_thumbnail` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`_o`,`sender`,`community_id`,`entry_id`) USING BTREE,
  KEY `re` (`sender`),
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`),
  KEY `eid2` (`entry_id`)
) ENGINE=InnoDB;

CREATE TABLE `user_diary_replies` (
  `diary_reply_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `reply_to_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `message` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `_date_created` bigint(20) unsigned NOT NULL DEFAULT '4291869598',
  PRIMARY KEY (`_date_created`,`user_id`,`diary_reply_id`) USING BTREE,
  KEY `dtc` (`date_created`),
  KEY `us` (`user_id`),
  KEY `dc` (`_date_created`)
) ENGINE=InnoDB;

CREATE TABLE `user_events` (
  `entry_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `date_event` datetime NOT NULL,
  `sender` int(10) unsigned NOT NULL,
  `subj` varchar(512) DEFAULT NULL,
  `diary_entry` text,
  `highlight_css` varchar(45) NOT NULL,
  `blast_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `location` text,
  `event_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`_o`,`sender`,`community_id`,`entry_id`) USING BTREE,
  KEY `re` (`sender`),
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`),
  KEY `IDX_date_event` (`date_event`)
) ENGINE=InnoDB;

CREATE TABLE `user_events_archive` (
  `entry_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `date_event` datetime NOT NULL,
  `sender` int(10) unsigned NOT NULL,
  `subj` varchar(512) DEFAULT NULL,
  `diary_entry` text,
  `highlight_css` varchar(45) NOT NULL,
  `blast_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `community_id` int(10) unsigned NOT NULL DEFAULT '0',
  `location` text,
  `event_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`_o`,`sender`,`community_id`,`entry_id`) USING BTREE,
  KEY `re` (`sender`),
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`)
) ENGINE=InnoDB;

