-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP TRIGGER IF EXISTS b4_active_blasts_replies_update;

DELIMITER ;;
CREATE TRIGGER `b4_active_blasts_replies_update` BEFORE UPDATE ON `active_blasts_replies` FOR EACH ROW BEGIN
	 SET new._date_created = kaneva.master_date() - UNIX_TIMESTAMP(new.date_created);
END
;;
DELIMITER ;