-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS delete_admin_blast;

DELIMITER ;;
CREATE PROCEDURE `delete_admin_blast`(IN __entry_id BIGINT)
BEGIN

DELETE FROM sticky_blasts WHERE entry_id = __entry_id;

DELETE FROM user_events WHERE entry_id = __entry_id;


END
;;
DELIMITER ;