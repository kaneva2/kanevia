-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_channel_diary_reply;

DELIMITER ;;
CREATE PROCEDURE `add_channel_diary_reply`(IN __entry_id BIGINT, IN __text TEXT)
BEGIN
  DECLARE __this_entry_id BIGINT;
	set __this_entry_id = diary.get_id();
	
  Insert into diary.channel_diary_replies VALUES(__this_entry_id ,__entry_id,NOW(),__text);
END
;;
DELIMITER ;