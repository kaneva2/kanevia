-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use diary;

DROP PROCEDURE IF EXISTS purge_user_diary_archive;

DELETE FROM `schema_versions` WHERE `version` = 1;

