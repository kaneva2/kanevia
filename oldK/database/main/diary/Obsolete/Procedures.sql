-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

\. diary#sp#purge_user_diary_archive.sql
\. sp#add_maintenance_log.sql