-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `active_blasts`
--

DROP TABLE IF EXISTS `active_blasts`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `active_blasts` (
  `entry_id` bigint(20) unsigned NOT NULL default '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `sender_id` int(20) unsigned NOT NULL,
  `sender_name` varchar(80) NOT NULL,
  `subj` varchar(512) default NULL,
  `diary_entry` text,
  `highlight_css` varchar(45) NOT NULL,
  `blast_type` tinyint(3) unsigned NOT NULL default '0',
  `community_id` int(10) unsigned NOT NULL default '0',
  `number_of_replies` int(11) NOT NULL default '0',
  `display_thumbnail` varchar(255) NOT NULL default '',
  PRIMARY KEY  USING BTREE (`_o`,`sender_id`,`community_id`,`entry_id`),
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`),
  KEY `__o` USING BTREE (`_o`),
  KEY `nr` (`number_of_replies`),
  KEY `sen` USING BTREE (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `active_blasts_archive`
--

DROP TABLE IF EXISTS `active_blasts_archive`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `active_blasts_archive` (
  `entry_id` bigint(20) unsigned NOT NULL default '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `sender_id` int(20) unsigned NOT NULL,
  `sender_name` varchar(80) NOT NULL,
  `subj` varchar(512) default NULL,
  `diary_entry` text,
  `highlight_css` varchar(45) NOT NULL,
  `blast_type` tinyint(3) unsigned NOT NULL default '0',
  `community_id` int(10) unsigned NOT NULL default '0',
  `number_of_replies` int(11) NOT NULL default '0',
  `display_thumbnail` varchar(255) NOT NULL default '',
  PRIMARY KEY  USING BTREE (`_o`,`sender_id`,`community_id`,`entry_id`),
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`),
  KEY `__o` USING BTREE (`_o`),
  KEY `nr` (`number_of_replies`),
  KEY `sen` USING BTREE (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `active_blasts_replies`
--

DROP TABLE IF EXISTS `active_blasts_replies`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `active_blasts_replies` (
  `diary_reply_id` bigint(20) unsigned NOT NULL default '0',
  `reply_to_id` bigint(20) unsigned NOT NULL default '0',
  `date_created` datetime NOT NULL,
  `message` text NOT NULL,
  `user_id` int(11) NOT NULL default '0',
  `_date_created` bigint(20) unsigned NOT NULL default '4291869598',
  PRIMARY KEY  USING BTREE (`_date_created`,`user_id`,`diary_reply_id`),
  KEY `dtc` (`date_created`),
  KEY `us` (`user_id`),
  KEY `dc` (`_date_created`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `diary_id`
--

DROP TABLE IF EXISTS `diary_id`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `diary_id` (
  `id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY  USING BTREE (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `maintenance_log`
--

DROP TABLE IF EXISTS `maintenance_log`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `maintenance_log` (
  `maintenance_log_id` int(10) unsigned NOT NULL auto_increment,
  `date_occurred` datetime NOT NULL,
  `database_name` varchar(50) default NULL,
  `procedure_name` varchar(200) default NULL,
  `message_text` varchar(500) default NULL,
  PRIMARY KEY  (`maintenance_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `passive_blasts`
--

DROP TABLE IF EXISTS `passive_blasts`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `passive_blasts` (
  `entry_id` bigint(20) unsigned NOT NULL default '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `sender_id` int(20) unsigned NOT NULL,
  `sender_name` varchar(80) NOT NULL,
  `subj` varchar(512) default NULL,
  `diary_entry` text,
  `blast_type` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  USING BTREE (`_o`,`sender_id`,`entry_id`),
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`),
  KEY `__o` USING BTREE (`_o`),
  KEY `sen` USING BTREE (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `passive_blasts_archive`
--

DROP TABLE IF EXISTS `passive_blasts_archive`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `passive_blasts_archive` (
  `entry_id` bigint(20) unsigned NOT NULL default '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `sender_id` int(20) unsigned NOT NULL,
  `sender_name` varchar(80) NOT NULL,
  `subj` varchar(512) default NULL,
  `diary_entry` text,
  `blast_type` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  USING BTREE (`_o`,`sender_id`,`entry_id`),
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`),
  KEY `__o` USING BTREE (`_o`),
  KEY `sen` USING BTREE (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `schema_versions`
--

DROP TABLE IF EXISTS `schema_versions`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `schema_versions` (
  `version` decimal(11,2) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL default CURRENT_TIMESTAMP COMMENT 'when version was set',
  `interface_change` enum('T','F') NOT NULL default 'F',
  PRIMARY KEY  (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `sticky_blasts`
--

DROP TABLE IF EXISTS `sticky_blasts`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sticky_blasts` (
  `entry_id` bigint(20) unsigned NOT NULL default '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `date_expires` datetime NOT NULL,
  `subj` varchar(512) default NULL,
  `diary_entry` text,
  `sticky` enum('Y','N') default NULL,
  `highlight_css` varchar(45) NOT NULL,
  `sender` int(10) unsigned NOT NULL default '0',
  `display_thumbnail` varchar(255) NOT NULL default '',
  PRIMARY KEY  USING BTREE (`_o`,`sender`,`entry_id`),
  KEY `dt` (`date_created`),
  KEY `de` (`date_expires`),
  KEY `ac` USING BTREE (`sticky`),
  KEY `__o` USING BTREE (`_o`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `sticky_blasts_archive`
--

DROP TABLE IF EXISTS `sticky_blasts_archive`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sticky_blasts_archive` (
  `entry_id` bigint(20) unsigned NOT NULL default '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `date_expires` datetime NOT NULL,
  `subj` varchar(512) default NULL,
  `diary_entry` text,
  `sticky` enum('Y','N') default NULL,
  `highlight_css` varchar(45) NOT NULL,
  `sender` int(10) unsigned NOT NULL,
  `display_thumbnail` varchar(255) NOT NULL default '',
  PRIMARY KEY  USING BTREE (`_o`,`sender`,`entry_id`),
  KEY `dt` (`date_created`),
  KEY `de` (`date_expires`),
  KEY `ac` USING BTREE (`sticky`),
  KEY `sender` (`sender`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user_blast_counts`
--

DROP TABLE IF EXISTS `user_blast_counts`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user_blast_counts` (
  `counts` int(10) unsigned default '0',
  `user_id` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user_diary`
--

DROP TABLE IF EXISTS `user_diary`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user_diary` (
  `entry_id` bigint(20) unsigned NOT NULL default '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `sender` int(20) unsigned NOT NULL,
  `subj` varchar(512) default NULL,
  `diary_entry` text,
  `highlight_css` varchar(45) NOT NULL,
  `blast_type` tinyint(3) unsigned NOT NULL default '0',
  `community_id` int(10) unsigned NOT NULL default '0',
  `number_of_replies` int(11) NOT NULL default '0',
  `display_thumbnail` varchar(255) NOT NULL default '',
  PRIMARY KEY  USING BTREE (`_o`,`sender`,`community_id`,`entry_id`),
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`),
  KEY `__o` USING BTREE (`_o`),
  KEY `nr` (`number_of_replies`),
  KEY `sen` USING BTREE (`sender`),
  KEY `eid2` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='InnoDB free: 3072 kB';
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user_diary_archive`
--

DROP TABLE IF EXISTS `user_diary_archive`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user_diary_archive` (
  `entry_id` bigint(20) unsigned NOT NULL default '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `sender` int(20) unsigned NOT NULL,
  `subj` varchar(512) default NULL,
  `diary_entry` text,
  `highlight_css` varchar(45) NOT NULL,
  `blast_type` tinyint(3) unsigned NOT NULL default '0',
  `community_id` int(10) unsigned NOT NULL default '0',
  `number_of_replies` int(11) NOT NULL,
  `display_thumbnail` varchar(255) NOT NULL default '',
  PRIMARY KEY  USING BTREE (`_o`,`sender`,`community_id`,`entry_id`),
  KEY `re` (`sender`),
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`),
  KEY `eid2` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user_diary_replies`
--

DROP TABLE IF EXISTS `user_diary_replies`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user_diary_replies` (
  `diary_reply_id` bigint(20) unsigned NOT NULL default '0',
  `reply_to_id` bigint(20) unsigned NOT NULL default '0',
  `date_created` datetime NOT NULL,
  `message` text NOT NULL,
  `user_id` int(11) NOT NULL default '0',
  `_date_created` bigint(20) unsigned NOT NULL default '4291869598',
  PRIMARY KEY  USING BTREE (`_date_created`,`user_id`,`diary_reply_id`),
  KEY `dtc` (`date_created`),
  KEY `us` (`user_id`),
  KEY `dc` (`_date_created`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

/*!50003 SET @SAVE_SQL_MODE=@@SQL_MODE*/;

--
-- Table structure for table `user_events`
--

DROP TABLE IF EXISTS `user_events`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user_events` (
  `entry_id` bigint(20) unsigned NOT NULL default '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `date_event` datetime NOT NULL,
  `sender` int(10) unsigned NOT NULL,
  `subj` varchar(512) default NULL,
  `diary_entry` text,
  `highlight_css` varchar(45) NOT NULL,
  `blast_type` tinyint(3) unsigned NOT NULL default '0',
  `community_id` int(10) unsigned NOT NULL default '0',
  `location` text,
  `event_name` varchar(100) default NULL,
  PRIMARY KEY  USING BTREE (`_o`,`sender`,`community_id`,`entry_id`),
  KEY `re` (`sender`),
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user_events_archive`
--

DROP TABLE IF EXISTS `user_events_archive`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user_events_archive` (
  `entry_id` bigint(20) unsigned NOT NULL default '0',
  `date_created` datetime NOT NULL,
  `_o` bigint(20) unsigned NOT NULL,
  `date_event` datetime NOT NULL,
  `sender` int(10) unsigned NOT NULL,
  `subj` varchar(512) default NULL,
  `diary_entry` text,
  `highlight_css` varchar(45) NOT NULL,
  `blast_type` tinyint(3) unsigned NOT NULL default '0',
  `community_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  USING BTREE (`_o`,`sender`,`community_id`,`entry_id`),
  KEY `re` (`sender`),
  KEY `dt` (`date_created`),
  KEY `bt` (`blast_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;
