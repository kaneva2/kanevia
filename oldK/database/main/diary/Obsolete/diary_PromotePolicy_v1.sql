-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use diary;

--
-- CREATE TABLE: schema_versions
--
DROP TABLE IF EXISTS `schema_versions`;
CREATE TABLE IF NOT EXISTS `schema_versions` (
	version NUMERIC(11,2) NOT NULL COMMENT 'current schema version',
	description VARCHAR(250) NOT NULL COMMENT 'description of update',
	date_applied TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
	interface_change ENUM('T','F') NOT NULL DEFAULT 'F',
	PRIMARY KEY (version)
)
	ENGINE = InnoDB;

--
-- CREATE TABLE: maintenance_log
--

CREATE TABLE IF NOT EXISTS maintenance_log
(
	maintenance_log_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	date_occurred DATETIME NOT NULL,
	database_name VARCHAR(50) NULL,
	procedure_name VARCHAR(200) NULL,
	message_text VARCHAR(500) NULL,
	CONSTRAINT PRIMARY KEY (maintenance_log_id)
)
	ENGINE = InnoDB;

-- new procedures
\. sp#add_maintenance_log.sql
\. diary#sp#purge_user_diary_archive.sql

DROP PROCEDURE IF EXISTS diary_PromotePolicy_1;
DELIMITER //
CREATE PROCEDURE diary_PromotePolicy_1()
BEGIN
IF NOT EXISTS (SELECT 1 FROM schema_versions WHERE version = 1) THEN
	INSERT INTO `schema_versions` ( `version`, `description` ) 
		VALUES (1, 'add purge for user_diary_archive' );
END IF;

END //

DELIMITER ;

CALL diary_PromotePolicy_1;
DROP PROCEDURE IF EXISTS diary_PromotePolicy_1;

