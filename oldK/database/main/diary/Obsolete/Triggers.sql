-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

\. diary#before#update#user_diary_replies.sql
\. diary#before#insert#user_diary_replies.sql