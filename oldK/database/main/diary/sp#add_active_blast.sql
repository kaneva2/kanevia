-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_active_blast;

DELIMITER ;;
CREATE PROCEDURE `add_active_blast`(IN __thumbnail_path VARCHAR(255), IN __blast_type TINYINT, IN __highlight_css VARCHAR(45), 
	IN __sender_id INT, IN __sender_name VARCHAR(80), IN __subject VARCHAR(512), IN __diary_entry TEXT, IN __recipient_id INT)
BEGIN

DECLARE __entry_id BIGINT;
SET __entry_id = diary.get_id();
CALL add_active_blast2(__blast_type, __highlight_css, __entry_id, __sender_id, __sender_name, __subject, __diary_entry, __thumbnail_path, __recipient_id);
	
END
;;
DELIMITER ;