-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_channel_diary_entry;

DELIMITER ;;
CREATE PROCEDURE `add_channel_diary_entry`(IN __thumbnail_path varchar(255), IN __sender INT, IN __blast_type TINYINT, IN __highlight_css varchar(45), IN __community_id INT, IN __subject varchar(512), IN __diary_entry TEXT)
BEGIN
  
	
  DECLARE __entry_id BIGINT;
	set __entry_id = diary.get_id();
	CALL add_channel_diary_entry2(__sender, __blast_type, __highlight_css, __entry_id, __community_id,__subject,__diary_entry,__thumbnail_path);
	
END
;;
DELIMITER ;