-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_channel_diary_entry2;

DELIMITER ;;
CREATE PROCEDURE `add_channel_diary_entry2`(IN __sender INT, IN __blast_type TINYINT, IN __highlight_css varchar(45), IN __entry_id BIGINT, IN __community_id INT, IN __subject varchar(512), IN __diary_entry TEXT, IN __thumbnail_path varchar(255))
BEGIN
/* Same as above but lets you specify the serial number (ID) for the entry */
	DECLARE __blast_time DATETIME;
	set __blast_time = NOW();
  
  INSERT into diary.user_diary VALUES(__entry_id,__blast_time,kaneva.get_o(),__sender, __subject, __diary_entry, __highlight_css, __blast_type,__community_id,0,__thumbnail_path);
END
;;
DELIMITER ;