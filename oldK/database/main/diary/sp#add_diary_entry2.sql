-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_diary_entry2;

DELIMITER ;;
CREATE PROCEDURE `add_diary_entry2`(in __blast_type tinyint,in __highlight_css varchar(45),IN __entry_id BIGINT, IN __sender INT, IN __subject varchar(512), IN __diary_entry TEXT, IN __thumbnail_path varchar(255))
BEGIN
/* Same as above but lets you specify the serial number (ID) for the entry */
	
  -- PRODUCT KEY for this test is 1001
	-- RECORD TYPE for this test is 9119
	DECLARE __blast_time DATETIME;
	set __blast_time = NOW();
  INSERT into diary.user_diary VALUES(__entry_id,__blast_time,kaneva.get_o(),__sender, __subject, __diary_entry, __highlight_css, __blast_type,0,0, __thumbnail_path);
  
END
;;
DELIMITER ;