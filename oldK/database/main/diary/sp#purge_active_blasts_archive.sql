-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS purge_active_blasts_archive;

DELIMITER ;;
CREATE PROCEDURE `purge_active_blasts_archive`(__purge_before_date	DATE, __purge_cutoff_time DATETIME)
BEGIN

DECLARE	__Max_entry_id		INT;
DECLARE	__table_RowCnt		INT;
DECLARE	__table_RowCnt2		INT;
DECLARE __message_text VARCHAR(500);
DECLARE __min_entry_id		BIGINT;

SELECT	IFNULL(COUNT(entry_id),0) INTO __table_RowCnt
	FROM	active_blasts_archive
	WHERE	date_created < __purge_before_date;

SELECT IFNULL(MAX(entry_id),0) INTO __Max_entry_id
	FROM	active_blasts_archive
	WHERE	date_created < __purge_before_date;

SELECT CONCAT('Starting purge ... ',CAST(__table_RowCnt AS CHAR),' rows to purge. Purge before date: ',
				CAST(__purge_before_date AS CHAR),'; Purge cutoff time: ',
				CAST(__purge_cutoff_time AS CHAR),'; Max entry id: ',
				CAST(__Max_entry_id AS CHAR)) INTO __message_text;
CALL add_maintenance_log (DATABASE(),'purge_active_blasts_archive',__message_text);

-- Loop while anything to delete.
WHILE (__table_RowCnt > 0 AND NOW() < __purge_cutoff_time)    DO

	DELETE FROM	active_blasts_archive WHERE entry_id <= __Max_entry_id LIMIT 5000;

	SELECT IFNULL(ROW_COUNT(),0) INTO __table_RowCnt;

	SELECT  CONCAT('Purged active_blasts_archive: ',CAST(__table_RowCnt AS CHAR),' rows.') INTO __message_text;
	CALL add_maintenance_log (DATABASE(),'purge_active_blasts_archive',__message_text);

	SELECT MIN(entry_id) INTO __min_entry_id FROM active_blasts_archive;
	DELETE FROM diary.active_blasts_replies_archive WHERE reply_to_id < __min_entry_id;

	SELECT IFNULL(ROW_COUNT(),0) INTO __table_RowCnt2;

	SELECT  CONCAT('Purged active_blasts_replies_archive: ',CAST(__table_RowCnt2 AS CHAR),' rows.') INTO __message_text;
	CALL add_maintenance_log (DATABASE(),'purge_active_blasts_archive',__message_text);

	END WHILE;

SELECT 'Purge completed.' INTO __message_text;
CALL add_maintenance_log (DATABASE(),'purge_active_blasts_archive',__message_text);

END
;;
DELIMITER ;