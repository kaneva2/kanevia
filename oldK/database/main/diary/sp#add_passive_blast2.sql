-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_passive_blast2;

DELIMITER ;;
CREATE PROCEDURE `add_passive_blast2`(in __blast_type tinyint, IN __entry_id BIGINT, IN __sender_id INT, IN __sender_name varchar(80), IN __subject varchar(512), IN __diary_entry TEXT)
BEGIN
  

	DECLARE __blast_time DATETIME;
	set __blast_time = NOW();
  INSERT into diary.passive_blasts VALUES(__entry_id, __blast_time, kaneva.get_o(), __sender_id, __sender_name, __subject, __diary_entry, __blast_type);
  
END
;;
DELIMITER ;