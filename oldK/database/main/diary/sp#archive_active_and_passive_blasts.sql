-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS archive_active_and_passive_blasts;

DELIMITER ;;
CREATE PROCEDURE `archive_active_and_passive_blasts`(IN __expirey_date DATETIME)
BEGIN
DECLARE	__table_RowCnt		INT;
DECLARE __message_text VARCHAR(500);
DECLARE __min_entry_id		BIGINT;

SELECT NOW() INTO @start_time;
SELECT CONCAT('Starting archive of data before: ',CAST(__expirey_date AS CHAR)) INTO __message_text;
CALL add_maintenance_log (DATABASE(),'archive_active_and_passive_blasts',__message_text);

  START TRANSACTION;
	REPLACE INTO diary.active_blasts_archive SELECT * FROM diary.active_blasts WHERE date_created < __expirey_date;
	DELETE FROM diary.active_blasts WHERE date_created < __expirey_date;

	SELECT IFNULL(ROW_COUNT(),0) INTO __table_RowCnt;
	SELECT  CONCAT('Archived for active_blasts: ',CAST(__table_RowCnt AS CHAR),' rows.') INTO __message_text;
	CALL add_maintenance_log (DATABASE(),'archive_active_and_passive_blasts',__message_text);

	SELECT COALESCE(MIN(entry_id),744073709551615) INTO __min_entry_id FROM active_blasts;
	REPLACE INTO diary.active_blasts_replies_archive SELECT * FROM diary.active_blasts_replies WHERE reply_to_id < __min_entry_id;
	DELETE FROM diary.active_blasts_replies WHERE reply_to_id < __min_entry_id;

	SELECT IFNULL(ROW_COUNT(),0) INTO __table_RowCnt;
	SELECT  CONCAT('Archived for active_blasts_replies: ',CAST(__table_RowCnt AS CHAR),' rows.') INTO __message_text;
	CALL add_maintenance_log (DATABASE(),'archive_active_and_passive_blasts',__message_text);

	REPLACE INTO diary.passive_blasts_archive SELECT * FROM diary.passive_blasts WHERE date_created < __expirey_date;
	DELETE FROM diary.passive_blasts WHERE date_created < __expirey_date;

	SELECT IFNULL(ROW_COUNT(),0) INTO __table_RowCnt;
	SELECT  CONCAT('Archived for passive_blasts: ',CAST(__table_RowCnt AS CHAR),' rows.') INTO __message_text;
	CALL add_maintenance_log (DATABASE(),'archive_active_and_passive_blasts',__message_text);
	COMMIT;
	
SELECT 'Archive completed.' INTO __message_text;
CALL add_maintenance_log (DATABASE(),'archive_active_and_passive_blasts',__message_text);

INSERT INTO sp_run_log (sp_name, run_start_time, run_end_time, duration_seconds) VALUES
	('archive_active_and_passive_blasts',@start_time,NOW(),TIME_TO_SEC(TIMEDIFF(NOW(),@start_time)));

END
;;
DELIMITER ;