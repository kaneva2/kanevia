-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_passive_blast;

DELIMITER ;;
CREATE PROCEDURE `add_passive_blast`(IN __blast_type TINYINT, IN __sender_id INT, IN __sender_name varchar(80), IN __subject varchar(512), IN __diary_entry TEXT)
BEGIN

  DECLARE __entry_id BIGINT;
	set __entry_id = diary.get_id();
	CALL add_passive_blast2(__blast_type, __entry_id, __sender_id, __sender_name, __subject, __diary_entry);
	
END
;;
DELIMITER ;