-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS send_sticky_blast;

DELIMITER ;;
CREATE PROCEDURE `send_sticky_blast`(IN __entry_id BIGINT, IN __thumbnail_path varchar(255), IN __sender INT, IN __highlight_css varchar(45), IN __date_expires DATETIME, IN __subject varchar(512), IN __diary_entry TEXT, IN __is_sticky ENUM('Y','N'))
BEGIN
  Insert into diary.sticky_blasts VALUES(__entry_id, now(), kaneva.get_o(), __date_expires, __subject, __diary_entry, __is_sticky, __highlight_css, __sender, __thumbnail_path);
END
;;
DELIMITER ;