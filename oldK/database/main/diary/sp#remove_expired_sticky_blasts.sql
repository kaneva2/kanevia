-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS remove_expired_sticky_blasts;

DELIMITER ;;
CREATE PROCEDURE `remove_expired_sticky_blasts`()
BEGIN
  -- update sticky_blasts SET active='N' WHERE date_expires <= NOW();
  Replace into sticky_blasts_archive Select * from sticky_blasts WHERE date_expires <= NOW();
  DELETE from sticky_blasts WHERE date_expires <= NOW();
END
;;
DELIMITER ;