-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS send_blast_to_channel;

DELIMITER ;;
CREATE PROCEDURE `send_blast_to_channel`(IN __blast_type TINYINT, IN __highlight_css varchar(45), IN __channel_id INT, IN __blast_time DATETIME, IN __subject varchar(512), IN __diary_entry TEXT)
BEGIN
/* Sends a blast message to a channel and its members */
  -- PRODUCT KEY for this test is 1001
	-- RECORD TYPE for this test is 9119
	
  DECLARE __entry_id BIGINT;
	DECLARE done INT DEFAULT 0;
  DECLARE __member_id INT;
	DECLARE xc CURSOR for Select user_id from kaneva.community_members where community_id = __channel_id AND status_id=1;
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	
	OPEN xc;
	REPEAT
  FETCH xc INTO __member_id;
	
    set __entry_id = diary.get_id();		
		INSERT into diary.user_diary VALUES(__entry_id, __blast_time ,__member_id, __subject, __diary_entry, __highlight_css, __blast_type);
  UNTIL done END REPEAT;
CLOSE xc;
END
;;
DELIMITER ;