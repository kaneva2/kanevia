-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS purge_passive_blasts_archive;

DELIMITER ;;
CREATE PROCEDURE `purge_passive_blasts_archive`(__purge_before_date	DATE, __purge_cutoff_time DATETIME)
BEGIN
DECLARE	__Max_entry_id		int;
DECLARE	__table_RowCnt		int;
DECLARE __message_text varchar(500);
SELECT	IFNULL(COUNT(entry_id),0) INTO __table_RowCnt
	FROM	passive_blasts_archive
	WHERE	date_created < __purge_before_date;
SELECT IFNULL(MAX(entry_id),0) INTO __Max_entry_id
	FROM	passive_blasts_archive
	WHERE	date_created < __purge_before_date;
SELECT CONCAT('Starting purge ... ',CAST(__table_RowCnt as CHAR),' rows to purge. Purge before date: ',
											CAST(__purge_before_date as CHAR),'; Purge cutoff time: ',
											CAST(__purge_cutoff_time AS CHAR),'; Max entry id: ',
											CAST(__Max_entry_id AS CHAR)) INTO __message_text;
CALL add_maintenance_log (DATABASE(),'purge_passive_blasts_archive',__message_text);
-- Loop while anything to delete.
WHILE (__table_RowCnt > 0 AND NOW() < __purge_cutoff_time)    DO
	DELETE FROM	passive_blasts_archive WHERE entry_id <= __Max_entry_id LIMIT 5000;
	SELECT IFNULL(ROW_COUNT(),0) INTO __table_RowCnt;
	SELECT  CONCAT('Purged ',CAST(__table_RowCnt AS CHAR),' rows.') INTO __message_text;
	CALL add_maintenance_log (DATABASE(),'purge_passive_blasts_archive',__message_text);
	END WHILE;
SELECT 'Purge completed.' INTO __message_text;
CALL add_maintenance_log (DATABASE(),'purge_passive_blasts_archive',__message_text);
END
;;
DELIMITER ;