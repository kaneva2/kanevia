-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS archive_old_diary_entries;

DELIMITER ;;
CREATE PROCEDURE `archive_old_diary_entries`(IN __expirey_date DATETIME)
BEGIN
  START TRANSACTION;
	Replace into diary.active_blasts_archive Select * from diary.active_blasts where date_created < __expirey_date;
	DELETE from diary.active_blasts where date_created < __expirey_date;

	Replace into diary.passive_blasts_archive Select * from diary.passive_blasts where date_created < __expirey_date;
	DELETE from diary.passive_blasts where date_created < __expirey_date;
	COMMIT;
END
;;
DELIMITER ;