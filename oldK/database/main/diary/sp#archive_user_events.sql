-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS archive_user_events;

DELIMITER ;;
CREATE PROCEDURE `archive_user_events`(__purge_before_date	DATE, __cutoff_time DATETIME)
BEGIN

DECLARE	__Max_date		date;
DECLARE	__Min_date		date;
DECLARE __batch_beg_date		datetime;
DECLARE __batch_end_date		datetime;
DECLARE	__table_RowCnt		int;
DECLARE __message_text varchar(500);

SELECT IFNULL(MAX(date_event),__purge_before_date), IFNULL(MIN(date_event),__purge_before_date) INTO __Max_date, __Min_date
	FROM	user_events
	WHERE	date_event < __purge_before_date;

SELECT CONCAT('Starting purge ... ',CAST(__purge_before_date as CHAR),'; Purge cutoff time: ',
					CAST(__cutoff_time AS CHAR)) INTO __message_text;
CALL add_maintenance_log (DATABASE(),'archive_user_events',__message_text);

SELECT CONCAT(DATE(__Min_date),' 00:00:00') INTO __batch_beg_date;
SELECT CONCAT(DATE(adddate(__batch_beg_date, interval 1 day)),' 00:00:00') INTO __batch_end_date;

SET __table_RowCnt = 1;

-- Loop while anything to delete.
WHILE ( __batch_beg_date < __Max_date) AND (NOW() < __cutoff_time)    DO

	INSERT IGNORE INTO user_events_archive SELECT * FROM user_events WHERE date_event between __batch_beg_date and __batch_end_date;

	DELETE FROM user_events WHERE date_event between __batch_beg_date and __batch_end_date;

	SELECT IFNULL(ROW_COUNT(),0) INTO __table_RowCnt;

	SELECT  CONCAT('Archived ',CAST(__table_RowCnt AS CHAR),' rows; from date: ',CAST(__batch_beg_date AS CHAR)) INTO __message_text;
	CALL add_maintenance_log (DATABASE(),'archive_user_events',__message_text);

	SELECT CONCAT(DATE(adddate(__batch_beg_date, interval 1 day)),' 00:00:00') INTO __batch_beg_date;
	SELECT CONCAT(DATE(adddate(__batch_end_date, interval 1 day)),' 00:00:00') INTO __batch_end_date;
	END WHILE;

SELECT 'Archive completed.' INTO __message_text;
CALL add_maintenance_log (DATABASE(),'archive_user_events',__message_text);

END
;;
DELIMITER ;