-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_diary_entry;

DELIMITER ;;
CREATE PROCEDURE `add_diary_entry`(IN __thumbnail_path varchar(255), IN __blast_type TINYINT, IN __highlight_css varchar(45), IN __sender INT, IN __subject varchar(512), IN __diary_entry TEXT)
BEGIN
  -- PRODUCT KEY for this test is 1001
	-- RECORD TYPE for this test is 9119
  DECLARE __entry_id BIGINT;
	set __entry_id = diary.get_id();
	CALL add_diary_entry2(__blast_type, __highlight_css, __entry_id, __sender,__subject,__diary_entry, __thumbnail_path);
	
END
;;
DELIMITER ;