-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS add_channel_blast_entry2;

DELIMITER ;;
CREATE PROCEDURE `add_channel_blast_entry2`(IN __sender_id INT, IN __sender_name VARCHAR(80), IN __blast_type TINYINT, 
	IN __highlight_css VARCHAR(45), IN __entry_id BIGINT, IN __community_id INT, 
	IN __subject VARCHAR (512), IN __diary_entry TEXT, IN __thumbnail_path VARCHAR(255),
	IN __community_name VARCHAR(80), IN __is_community_personal INTEGER UNSIGNED, IN __recipient_id INT)
BEGIN
	
INSERT INTO diary.active_blasts 
	(entry_id, date_created, _o, 
	sender_id, sender_name, subj, diary_entry, highlight_css, blast_type, 
	community_id, community_name, is_community_personal, number_of_replies, display_thumbnail, recipient_id)
	VALUES(__entry_id, NOW(), kaneva.get_o(), 
		__sender_id, __sender_name, __subject,__diary_entry, __highlight_css, __blast_type, 
		__community_id, __community_name, __is_community_personal, 0, __thumbnail_path, __recipient_id);
	
END
;;
DELIMITER ;