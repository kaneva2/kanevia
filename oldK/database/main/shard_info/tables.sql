-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

CREATE TABLE `dynamic_objects` (
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `email_overload_fields` (
  `static_pk` tinyint(4) NOT NULL DEFAULT '1',
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`static_pk`)
) ENGINE=InnoDB;

CREATE TABLE `items_custom` (
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `items_ugc` (
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='UGC items numbered from 1,000,000,000';

CREATE TABLE `kaneva_comments` (
  `static_pk` tinyint(4) NOT NULL DEFAULT '1',
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`static_pk`)
) ENGINE=InnoDB;

CREATE TABLE `kaneva_messages` (
  `static_pk` tinyint(4) NOT NULL DEFAULT '1',
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`static_pk`)
) ENGINE=InnoDB;

CREATE TABLE `maintenance_log` (
  `maintenance_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_occurred` datetime NOT NULL,
  `database_name` varchar(50) DEFAULT NULL,
  `procedure_name` varchar(200) DEFAULT NULL,
  `message_text` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`maintenance_log_id`),
  KEY `IDX_date_occurred` (`date_occurred`)
) ENGINE=InnoDB;

CREATE TABLE `schemas_to_count` (
  `schema_name` varchar(64) NOT NULL,
  PRIMARY KEY (`schema_name`)
) ENGINE=InnoDB;

CREATE TABLE `schema_versions` (
  `version` int(11) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;

