-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS get_items_ugc_id;

DELIMITER ;;
CREATE FUNCTION `get_items_ugc_id`() RETURNS int(11) unsigned
    READS SQL DATA
BEGIN
-- procedure to auto increment UGC items GLID
  UPDATE shard_info.items_ugc SET id=@_item_id:=id+1;
  return @_item_id;
END
;;
DELIMITER ;