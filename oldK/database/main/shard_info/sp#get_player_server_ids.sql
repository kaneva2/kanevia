-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_player_server_ids;

DELIMITER ;;
CREATE PROCEDURE `get_player_server_ids`( IN __player_list text )
BEGIN
	IF right(__player_list,1)=',' THEN 
		
		REPEAT
			SET __player_list = left(__player_list,length(__player_list)-1);
		UNTIL right(__player_list,1) <> ','	END REPEAT;
	END IF;
	IF LENGTH(__player_list) > 0 THEN 
		
		SET @__sql := CONCAT( "SELECT p.player_id, p.server_id ",
								  "FROM players p "
								 "WHERE p.player_id IN (",__player_list,") AND p.in_game = 'T'");
		
		PREPARE __stmt FROM @__sql;
		EXECUTE __stmt;
		DEALLOCATE PREPARE __stmt;
					
	END IF;
END
;;
DELIMITER ;