-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

--
-- CREATE TABLE: background_sql_tasks
--
DROP TABLE IF EXISTS background_sql_tasks;

CREATE TABLE IF NOT EXISTS background_sql_tasks
(
	background_sql_task_id BIGINT UNSIGNED NOT NULL,
	inserted_at DATETIME NOT NULL,
	started_at DATETIME NULL,
	completed_at DATETIME NULL,
	processing_flag TINYINT NOT NULL,
	sql_statement VARCHAR(512) NOT NULL,
	target_schema VARCHAR(20) NOT NULL,
	trigger_event_table VARCHAR(80) NULL,
	trigger_event_manipulation VARCHAR(12) NULL,
	CONSTRAINT PRIMARY KEY (background_sql_task_id)
)
	ENGINE = InnoDB;

--
-- CREATE TABLE: maintenance_log
--

CREATE TABLE IF NOT EXISTS maintenance_log
(
	maintenance_log_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	date_occurred DATETIME NOT NULL,
	database_name VARCHAR(50) NULL,
	procedure_name VARCHAR(200) NULL,
	message_text VARCHAR(500) NULL,
	CONSTRAINT PRIMARY KEY (maintenance_log_id)
)
	ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS background_sql_tasks_id
(
	id BIGINT UNSIGNED NOT NULL DEFAULT 1);