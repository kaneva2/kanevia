-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use shard_info;

\. shard_info_SchemaChanges_v49.sql

\. fnc#get_items_ugc_id.sql


REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
VALUES (49, 'UGC_DO database changes', now() );

