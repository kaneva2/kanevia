-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

--
-- Current Database: `shard_info`
--

CREATE DATABASE IF NOT EXISTS `shard_info` DEFAULT CHARACTER SET latin1;

USE `shard_info`;

--
-- Table structure for table `dynamic_objects`
--

CREATE TABLE `dynamic_objects` (
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- CREATE TABLE: maintenance_log
--

CREATE TABLE IF NOT EXISTS maintenance_log
(
	maintenance_log_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	date_occurred DATETIME NOT NULL,
	database_name VARCHAR(50) NULL,
	procedure_name VARCHAR(200) NULL,
	message_text VARCHAR(500) NULL,
	CONSTRAINT PRIMARY KEY (maintenance_log_id)
)
	ENGINE = InnoDB;

--
-- Table structure for table `world_object_player_settings`
--

CREATE TABLE `world_object_player_settings` (
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `schema_versions`
--

CREATE TABLE `schema_versions` (
  `version` int(11) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL default CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY  (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- set the initial schema version
INSERT INTO schema_versions( `version`, `description` ) VALUES (47, 'base version' );

