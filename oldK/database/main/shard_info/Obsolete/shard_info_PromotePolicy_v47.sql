-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use shard_info;

-- add new table background_sql_tasks
\. shard_info_SchemaChanges_v47.sql

-- new stored procedures
\. sp#run_background_sql_tasks.sql
\. sp#process_background_sql_tasks.sql
\. sp#monitor_background_sql_tasks.sql
\. sp#report_background_sql_tasks.sql
\. sp#purge_background_sql_tasks.sql
\. sp#add_maintenance_log.sql
\. fnc#get_background_sql_tasks_id.sql

DROP PROCEDURE IF EXISTS shard_info_SchemaChanges_v47;
DELIMITER //
CREATE PROCEDURE shard_info_SchemaChanges_v47()
BEGIN

insert into background_sql_tasks_id VALUES (1);

IF NOT EXISTS (SELECT 1 FROM schema_versions WHERE version = 47) THEN
	INSERT INTO `schema_versions` ( `version`, `description` ) 
		VALUES (47, 'DB Performance Changes Nov08' );
END IF;

END //

DELIMITER ;

CALL shard_info_SchemaChanges_v47;
DROP PROCEDURE IF EXISTS shard_info_SchemaChanges_v47;
