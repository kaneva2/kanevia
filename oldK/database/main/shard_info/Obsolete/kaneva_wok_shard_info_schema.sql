-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- run this file after running shard_info_schema.sql to do kaneva-only stuff
USE `shard_info`;

--
-- Table structure for table `kaneva_comments`
--

DROP TABLE IF EXISTS `kaneva_comments`;
CREATE TABLE `kaneva_comments` (
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `kaneva_messages`
--

DROP TABLE IF EXISTS `kaneva_messages`;
CREATE TABLE `kaneva_messages` (
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- bootstrap values, if separate servers this will have to be 
-- done manually
INSERT INTO kaneva_comments
	SELECT MAX(comment_id)+1 FROM kaneva.comments;

INSERT INTO kaneva_messages
	SELECT MAX(message_id)+1 FROM kaneva.messages;

-- bootstrap values, if separate servers this will have to be 
-- done manually
INSERT INTO dynamic_objects 
	SELECT MAX(obj_placement_id)+1 FROM wok.dynamic_objects;

INSERT INTO world_object_player_settings
	SELECT MAX(id)+1 FROM wok.world_object_player_settings;

