-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use shard_info;

DROP PROCEDURE IF EXISTS run_background_sql_tasks;
DROP PROCEDURE IF EXISTS process_background_sql_tasks;
DROP PROCEDURE IF EXISTS monitor_background_sql_tasks;
DROP PROCEDURE IF EXISTS report_background_sql_tasks;
DROP PROCEDURE IF EXISTS purge_background_sql_tasks;
DROP PROCEDURE IF EXISTS add_maintenance_log;

DROP TABLE IF EXISTS shard_info.background_sql_tasks;

DELETE FROM `schema_versions` WHERE `version` = 47;

