-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS SchemaChanges;

DELIMITER //

CREATE PROCEDURE SchemaChanges()
BEGIN

IF NOT EXISTS (SELECT 1 FROM information_schema.statistics WHERE table_schema = 'shard_info' 
		and table_name = 'email_overload_fields') THEN
		
CREATE TABLE `shard_info`.`email_overload_fields` (
  `static_pk` TINYINT(4) NOT NULL DEFAULT '1',
  `id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY  (`static_pk`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

END IF;
END //
DELIMITER ;

CALL SchemaChanges();

DROP PROCEDURE IF EXISTS SchemaChanges;
