-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

use shard_info;

-- add new function(s) 
\. fnc#get_email_overload_fields_id.sql

\. shard_info_SchemaChanges_v53.sql

-- add developer version number
DROP PROCEDURE IF EXISTS shard_info_PromotePolicy_v53;
DELIMITER //
CREATE PROCEDURE shard_info_PromotePolicy_v53()
BEGIN

IF NOT EXISTS (SELECT 1 FROM shard_info.email_overload_fields WHERE id > 0) THEN
	insert into email_overload_fields (`static_pk`, `id`) values (1,1);
END IF;

IF NOT EXISTS (SELECT 1 FROM schema_versions WHERE version = 53.0) THEN
	INSERT INTO `schema_versions` ( `version`, `description` ) 
		VALUES (53.0, 'Auto Violations (initial version)' );
END IF;

END //

DELIMITER ;

CALL shard_info_PromotePolicy_v53;
DROP PROCEDURE IF EXISTS shard_info_PromotePolicy_v53;

