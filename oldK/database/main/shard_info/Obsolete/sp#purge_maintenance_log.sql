-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS `shard_info`.`purge_maintenance_log`;
DELIMITER $$
CREATE PROCEDURE `shard_info`.`purge_maintenance_log`
	(__purge_before_date	DATE, __purge_cutoff_time DATETIME) 
BEGIN

DECLARE	__Max_log_id		int;
DECLARE	__maint_log_RowCnt		int;
DECLARE	__OrigRowCnt		int;
DECLARE __message_text varchar(500);

SELECT	IFNULL(COUNT(maintenance_log_id),0) INTO __maint_log_RowCnt
	FROM	maintenance_log
	WHERE	date_occurred < __purge_before_date;

SELECT IFNULL(MAX(maintenance_log_id),__purge_before_date) INTO __Max_log_id
	FROM	maintenance_log
	WHERE	date_occurred < __purge_before_date;

SELECT CONCAT('Starting purge ... ',CAST(__maint_log_RowCnt as CHAR),' rows to purge. Purge before date: ',
											CAST(__purge_before_date as CHAR),'; Purge cutoff time: ',CAST(__purge_cutoff_time AS CHAR)) INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'purge_maintenance_log',__message_text);

-- Loop while anything to delete.
SELECT __maint_log_RowCnt INTO __OrigRowCnt;
WHILE (__maint_log_RowCnt > 0 AND NOW() < __purge_cutoff_time)    DO

	DELETE FROM	maintenance_log WHERE maintenance_log_id <= __Max_log_id ORDER BY maintenance_log_id LIMIT 50000;

	SELECT IFNULL(ROW_COUNT(),0) INTO __maint_log_RowCnt;

	SELECT  CONCAT('Purged ',CAST(__maint_log_RowCnt AS CHAR),' rows.') INTO __message_text;
	CALL shard_info.add_maintenance_log (DATABASE(),'purge_maintenance_log',__message_text);

	END WHILE;

SELECT 'Purge completed.' INTO __message_text;
CALL shard_info.add_maintenance_log (DATABASE(),'purge_maintenance_log',__message_text);

END$$

DELIMITER ;

