-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS wok_SchemaChanges;
DELIMITER //
CREATE PROCEDURE wok_SchemaChanges()

BEGIN

DECLARE recCount INT DEFAULT 0;

CREATE TABLE IF NOT EXISTS `items_ugc` (
    `id`	int(10) unsigned	PRIMARY KEY
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='UGC items numbered from 1,000,000,000';

SELECT count(*) INTO recCount FROM `items_ugc`;
IF recCount = 0 THEN
	INSERT INTO `items_ugc` (`id`) VALUES (1000000000);
END IF;

END //
DELIMITER ;

CALL wok_SchemaChanges;
DROP PROCEDURE IF EXISTS wok_SchemaChanges;
