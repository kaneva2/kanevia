-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- Jason's shard info changes
USE `shard_info`;

DELIMITER //
CREATE PROCEDURE v46_upgrade()
BEGIN
	DECLARE __prev_ver INT(11) DEFAULT NULL;

	SELECT IFNULL(MAX(version),0) INTO __prev_ver 
	  FROM schema_versions;

	IF __prev_ver < 46 THEN 	  
		CREATE TABLE  `items_custom` 
		(`id` int(10) unsigned NOT NULL,PRIMARY KEY  (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;

		insert into `items_custom` (id) values (3000000);

		INSERT INTO schema_versions( `version`, `description` ) VALUES (46, 'fame, organic, login log changes' );
	END IF;
END;
//

DELIMITER ;

CALL v46_upgrade();

DROP PROCEDURE v46_upgrade;

-- renames of tables to remove "wokness"
RENAME TABLE wok_dynamic_objects TO dynamic_objects;
RENAME TABLE wok_world_object_player_settings TO world_object_player_settings;

-- drop old names of these
DROP FUNCTION IF EXISTS `get_wok_world_object_player_settings_id`;
DROP FUNCTION IF EXISTS `get_wok_dynamic_objects_id`;

\. sp#get_items_custom_id.sql
\. sp#get_world_object_player_settings_id.sql
\. sp#get_dynamic_objects_id.sql


