-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS get_world_object_player_settings_id;

DELIMITER ;;
CREATE FUNCTION `get_world_object_player_settings_id`() RETURNS int(11)
    READS SQL DATA
BEGIN
  UPDATE shard_info.world_object_player_settings SET id=@_world_object_player_settings_id:=id+1;
  return @_world_object_player_settings_id;
END
;;
DELIMITER ;