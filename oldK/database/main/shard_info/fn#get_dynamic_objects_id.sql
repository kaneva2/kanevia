-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS get_dynamic_objects_id;

DELIMITER ;;
CREATE FUNCTION `get_dynamic_objects_id`() RETURNS int(11)
    READS SQL DATA
BEGIN
  UPDATE shard_info.dynamic_objects SET id=@_dynamic_objects_id:=id+1;
  return @_dynamic_objects_id;
END
;;
DELIMITER ;