-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS get_kaneva_comments_id;

DELIMITER ;;
CREATE FUNCTION `get_kaneva_comments_id`() RETURNS int(11)
    READS SQL DATA
BEGIN
  UPDATE shard_info.kaneva_comments SET id=@_kaneva_comment_id:=id+1 WHERE static_pk = 1;
  return @_kaneva_comment_id;
END
;;
DELIMITER ;