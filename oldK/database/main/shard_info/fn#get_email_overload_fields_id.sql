-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS get_email_overload_fields_id;

DELIMITER ;;
CREATE FUNCTION `get_email_overload_fields_id`() RETURNS int(11)
    READS SQL DATA
BEGIN
  UPDATE shard_info.email_overload_fields SET id=@_email_overload_fields_id:=id+1 WHERE static_pk = 1;
  RETURN @_email_overload_fields_id;
END
;;
DELIMITER ;