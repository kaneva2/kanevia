-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS get_tables_to_compare;

DELIMITER ;;
CREATE PROCEDURE `get_tables_to_compare`()
BEGIN
SELECT CONCAT(table_schema,'.', table_name) AS `table_name`
FROM information_schema.tables t
INNER JOIN shard_info.schemas_to_count c ON t.TABLE_SCHEMA = c.schema_name
WHERE t.TABLE_NAME NOT LIKE 'xxx%' AND t.TABLE_NAME NOT LIKE '\_%'
AND table_type = 'BASE TABLE'
ORDER BY 1;
END
;;
DELIMITER ;