@echo off
REM  This batch file takes two parameters. The first is an sql promote script
REM  The second is a target directory. It searches for the "\. filename" line 
REM  in the promote script and copies the related files into the target.
REM
REM  This script assumes that all files paths are relative to the current
REM  folder

if !%1==! goto Usage
if !%2==! goto Usage

for /f "tokens=2 delims= " %%v in ('find "\." %1') do (
	xcopy %%v %2 /i /y > nul
)

Goto ExitLine

:Usage
echo USAGE: %0 promotefile.sql targetdir
:ExitLine
