::
:: This script recreates every crontab for every system.
::

xcopy template_root.crontab db-blast_root.crontab /Y
xcopy template_root.crontab db-search2_root.crontab /Y
xcopy template_root.crontab db-search_root.crontab /Y
xcopy template_root.crontab db-stats_root.crontab /Y
xcopy template_root.crontab db-ugc_root.crontab /Y
xcopy template_root.crontab db1_root.crontab /Y
xcopy template_root.crontab db2_root.crontab /Y
xcopy template_root.crontab db3_root.crontab /Y
xcopy template_root.crontab pv-db-blast_root.crontab /Y
xcopy template_root.crontab pv-db-search_root.crontab /Y
xcopy template_root.crontab pv-db-search_root.crontab /Y
xcopy template_root.crontab pv-db-stats_root.crontab /Y
xcopy template_root.crontab pv-db-ugc_root.crontab /Y
xcopy template_root.crontab pv-db1_root.crontab /Y
xcopy template_root.crontab pv-db2_root.crontab /Y
xcopy template_root.crontab pv-db3_root.crontab /Y
xcopy template_root.crontab forums_root.crontab /Y

php -f createcron.php template_mysql_utils.crontab

move /Y preview.crontab preview_mysql_utils.crontab
move /Y production.crontab production_mysql_utils.crontab 
move /Y dev.crontab dev_mysql_utils.crontab 
move /Y 3d.crontab 3d_mysql_utils.crontab 
move /Y rc.crontab rc_mysql_utils.crontab 

php -f createcron.php template.crontab
