<!--
 Copyright Kaneva 2001-2020
 This file is licensed under the PolyForm Noncommercial License 1.0.0
 https://polyformproject.org/licenses/noncommercial/1.0.0/ 
-->

<?php
$template_file = "template.crontab";

if($argc>1){
   $template_file = $argv[1];
}
echo "Using template file: $template_file\n";

$config["dev"] = array(
	"%CRON_USER%" => "mysql_cron@kanevia.com",
	"%ALERT_EMAIL%" => "alerts@kanevia.com",
    "%USERNAME%" => "TBD",
    "%PASSWORD%" => "TBD",
    "%DB1%" => "db.kanevia.com",
    "%DB2%" => "db.kanevia.com",
    "%DB3%" => "db.kanevia.com",
    "%DB-BLAST%" => "db.kanevia.com",
    "%DB-SEARCH%" => "db.kanevia.com",
    "%DB-STATS%" => "db.kanevia.com",
    "%DB-REPORT%" => "db.kanevia.com",
    "%DB-UGC%" => "db.kanevia.com",
    "%DB-SPHINX%" => "db.kanevia.com",
    "%PRODONLY%" => "#",
    "%PURGESHORT%" => "2",
    "%PURGELONG%" => "2",
    "%CACHELOADTIME%" => "*/20 * * * *",
    "%POPULATIONCACHELOADTIME%" => "*/10 * * * *",
    "%POPULATIONCACHEQUERYTIME%" => "*/10 * * * *",
    "%SEARCHLOADTIME%" => "0,30 * * * *",
    "%METRICSCOUNT%" => "200",
    "%AGGDAILYTIME%" => "2 * * * *",
    "%AGGHOURLYTIME%" => "0,30 * * * *",
    "%MONITOR%" => "#",
    "%STAGEDEV%" => "#",
    "%STAGE3D%" => "",
    "%STAGERC%" => "",
    "%STAGEPV%" => "",
    "%SHAREDDB%" => "#",
    "%SYSTEMMONIKER%" => "techpreview",
);

if(file_exists($template_file)){
    $templatestring = file_get_contents($template_file);
}

foreach($config as $system => $configset){
    $outstring = $templatestring;
    foreach ($configset as $from => $to){
        echo "Processing $system::$from\n";
        $outstring = str_replace($from,$to,$outstring);
    }
    if(($fileptr=fopen("$system.crontab","wb"))!=null){
        fwrite($fileptr,$outstring);
        fclose($fileptr);    
    }
}
?>
