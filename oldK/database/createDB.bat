@echo off
REM  This batch file takes three parameters. 
REM  1) server-host-name-or-ip
REM  2) db-name - to create this version of the schema
REM  3) db-user-name 
REM
REM  This script assumes C:\Program Files\MySQL\MySQL Server 5.0\bin is in your path
REM  Be prepared to enter your password several times (to avoid passing it in on the command line)

if !%1==! goto Usage
if !%2==! goto Usage
if !%3==! goto Usage

REM create the database
ECHO CREATE DATABASE IF NOT EXISTS `%2` DEFAULT CHARACTER SET latin1; > createdatabase.tmp
mysql.exe -u %3 -p -h %1 -v wok < createdatabase.tmp > createDB_log.txt

REM create the tables and views
mysql.exe -u %3 -p -h %1 -v %2 < CreateTables.sql >> createDB_log.txt

REM create the triggers
mysql.exe -u %3 -p -h %1 -v %2 < Triggers.sql >> createDB_log.txt

REM create the stored procedures and functions
mysql.exe -u %3 -p -h %1 -v %2 < Procedures.sql >> createDB_log.txt

REM add the seed data
mysql.exe -u %3 -p -h %1 -v %2 < SeedData.sql >> createDB_log.txt

Goto ExitLine

:Usage
echo USAGE: %0 server-host-name-or-ip database-name db-user-name 
echo This script assumes C:\Program Files\MySQL\MySQL Server 5.0\bin is in your path
echo Be prepared to enter your password several times (to avoid passing it in on the command line)
:ExitLine
del createdatabase.tmp
echo COMPLETED at %date%  %time% ......... output in: createDB_log.txt
