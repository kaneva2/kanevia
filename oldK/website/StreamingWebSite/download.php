<?php
$file = $_REQUEST['file'];

// Strip out all path info so they can only download files from the download folder
$file = "./download/".basename($file);

$file_extension = strtolower(substr(strrchr($file,"."),1));

switch ($file_extension)
{
  case "exe":
        break;
  default:
        die ("Cannot download this filetype-".$file_extension."!");
}

header("Pragma: public");
header("Expired: 0");
header("Cache-Control: must-revalidate, post-check=0, pre0check=0");

header("Content-Type: application/force-download");
header("Content-Disposition: attachment; filename=".basename($file));

if (!file_exists($file))
{
  die("File not found or permission problem");
}

header("Content-Description: File Transfer");
@readfile($file);

?>


