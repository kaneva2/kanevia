<?php
$cfg['host']			= 'kanevadev';
$cfg['username']        = 'root';
$cfg['password']        = 'glue2004';
$cfg['db']				= 'kaneva20';

// Production
//$cfg['host']			= '10.10.252.12';
//$cfg['username']      = 'streamer';
//$cfg['password']      = 'istream2004';
//$cfg['db']			= 'kaneva';

//header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
//header('Pragma: no-cache');
//header ("Expires: Mon, 21 Jul 1997 05:00:00 GMT"); // Date in the past

$tId = $_GET["tId"];

if ($tId) {

    // connect to database server:
	$link = mysql_connect ($cfg['host'], $cfg['username'], $cfg['password']);
	if (!$link) {
		die ('Could not connect:'. mysql_error ());
	}

    @mysql_select_db ($cfg['db']);

	// Get published asset
    $query = sprintf("SELECT a.target_dir, a.content_extension, a.amount, a.asset_id ".
			 " FROM assets a ".
			 " WHERE a.asset_id = %d".
			 " AND a.published = 'Y'",
			 $tId);
			 
    $result = mysql_query ($query);
    
    if (!$result) {
	  //die ('Could not query directory data :'.mysql_error ());
	  die ('No Asset Found');
	}
	else
	{
		if (mysql_num_rows($result) == 0)
		{
			die ('Not a valid asset!');
		} 
	}

    $targetDir = mysql_result ($result, 0, "target_dir");
    $contentExtension = mysql_result ($result, 0, "content_extension");
    $amount = mysql_result ($result, 0, "amount");
    $assetId = mysql_result ($result, 0, "asset_id");
    
    // Set the mime type
    $mime_type = strtolower(strrchr($contentExtension,'.')); 
    $mime_type_array = array( 
        '.asf'  => 'application/vnd.ms-asf', 
        '.avi'  => 'video/x-msvideo', 
        '.gif'  => 'image/gif', 
        '.jpg'  => 'image/jpeg', 
        '.mov'  => 'video/quicktime', 
        '.mpe'  => 'video/mpeg', 
        '.mpeg' => 'video/mpeg', 
        '.mpg'  => 'video/mpeg', 
        '.ra'   => 'audio/x-pn-realaudio', 
        '.ram'  => 'audio/x-pn-realaudio', 
        '.rm'   => 'audio/x-pn-realaudio', 
        '.wmv'  => 'audio/x-ms-wmv',
	  '.flv'  => 'video/x-flv' 
        );

  //  die ($mime_type."-".$mime_type_array[$mime_type]);
    if (in_array($mime_type,array_keys($mime_type_array)))
    {
       header("Content-Type: ".$mime_type_array[$mime_type]);
    }
    
    // Update the stream count on the asset
    $query = sprintf("UPDATE assets_stats a SET ".
		" a.number_of_stream_requests = (a.number_of_stream_requests + 1) ".
		" WHERE a.asset_id = %d",
		$assetId);;
		
	$result = mysql_query ($query);
	
	if (!$result) {
		die ('Could not update number_of_streams :' . mysql_error ());
	}
	
	$v_diskfile = $targetDir.'\\'.$contentExtension;
   
    mysql_close ($link);

	// Unix fix
	$v_diskfile = str_replace("\\","/",$v_diskfile);

	if (file_exists ($v_diskfile ) ) {
		// Add size
		header ("Content-Disposition: filename=\"$contentExtension\"");
		header ("Content-Length: " . filesize($v_diskfile));
		
		// Send the file
		readfile_chunked ($v_diskfile );
	}
	else
	{
		echo ('Could not find file or no permission.');
	}

}
else
{
	die ('No asset id');
}

function readfile_chunked($filename,$retbytes=true) {
   $chunksize = 1*(1024*1024); // how many bytes per chunk
   $buffer = '';
   $cnt =0;

   $handle = fopen($filename, 'rb');
   if ($handle === false) {
       return false;
   }
   while (!feof($handle)) {
       $buffer = fread($handle, $chunksize);
       echo $buffer;
       flush();
       if ($retbytes) {
           $cnt += strlen($buffer);
       }
   }
       $status = fclose($handle);
   if ($retbytes && $status) {
       return $cnt; // return num. bytes delivered like readfile() does.
   }
   return $status;

}
?>
