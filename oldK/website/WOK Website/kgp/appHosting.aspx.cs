///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;


namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class _3DappHosting : KgpBasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {

            if (IsPostBack)
            {
                return;
            }

            if (CheckUserId(true)) // true = can be logged on to web also for this request
            {
                if (Request.Params["action"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                string actionreq = Request.Params["action"];
                GameFacade gameFacade = new GameFacade();
                CommunityFacade communityFacade = new CommunityFacade();

                if (actionreq.Equals("CreateApp"))
                {
                    if ((Request.Params["name"] == null) || (Request.Params["desc"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>name or desc not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    string name = Request.Params["name"].ToString ();
                    string desc = Request.Params["desc"].ToString ();
                    int templateId = 0;

                    // Validate the name
                    if (!Regex.IsMatch(name.Trim(), "^" + Constants.VALIDATION_REGEX_CHANNEL_NAME + "$"))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-3</ReturnCode>\r\n  <ResultDescription>" + Constants.VALIDATION_REGEX_GAME_NAME_ERROR_MESSAGE + "</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    // 3dapp limits
                    if (!IsAdministrator())
                    {
                        int numberOf3DApps = communityFacade.GetNumberOf3DApps(KanevaWebGlobals.CurrentUser.UserId, (int)Constants.eCOMMUNITY_STATUS.ACTIVE);

                        if (numberOf3DApps >= Configuration.MaxNumberOf3DAppsPerUser)
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-2</ReturnCode>\r\n  <ResultDescription>Non-VIP Users are restricted to a maximum of " + Configuration.MaxNumberOf3DAppsPerUser + " 3D Apps</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    }



                    if (Request.Params["template"] != null)
                    {
                        try
                        {
                            templateId = Convert.ToInt32(Request.Params["template"]);
                        }
                        catch (Exception) { }
                    }

                    Response.Write(gameFacade.CreateApp (name, desc, m_userId, templateId));
                }

                if (actionreq.Equals("IsTaskCompleted"))
                {
                    if ((Request.Params["taskid"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>taskid not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    Int64 taskid = Int64.Parse(Request.Params["taskid"].ToString());

                    Response.Write(gameFacade.IsTaskCompleted(taskid));
                }

                if (actionreq.Equals("GoToApp"))
                {
                    //if ((Request.Params["id"] == null))
                    //{
                    //    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>id not specified</ResultDescription>\r\n</Result>";
                    //    Response.Write(errorStr);
                    //    return;

                    //}
                    int id = 0;

                    if (Request.Params["id"] != null)
                    {
                        id = Int32.Parse(Request.Params["id"].ToString());
                    }
                    else if (Request.Params["name"] != null)
                    {
                        string gameName = "";
                        StpUrl.ExtractGameName((string) Request.Params["name"].ToString(), out gameName);

                        Hashtable parameters = new Hashtable();
                        string wokDb = KanevaGlobals.DbNameKGP;
                        string kanevaDb = KanevaGlobals.DbNameKaneva;

                        string sql = "CALL get_game_info( @user_id, @game_name, @server_type, @reasonCode, @reasonDesc, @server, @port, @gameId, @parentGameId, @newGameName, @serverHostName ); select CAST(@reasonCode as UNSIGNED INT) as reasonCode, @reasonDesc, @server, CAST(@port as UNSIGNED INT) as port, CAST(@gameId as UNSIGNED INT) as gameId, CAST(@parentGameId as UNSIGNED INT) as parentGameId, @newGameName, @serverHostName; ";
                        parameters.Add("@user_id", 0);
                        parameters.Add("@game_name", gameName);
                        parameters.Add("@server_type", 0);

                        DataRow dr = KanevaGlobals.GetDatabaseUtilityDeveloper().GetDataRow(sql, parameters, false, "USE developer");

                        if (dr != null)
                        {
                            try
                            {
                                id = Convert.ToInt32(dr["gameId"]);
                            }
                            catch (Exception) { }

                        }
                    }


                    Response.Write(gameFacade.GoToApp(id));
                }

            }

        }

      
    }
}