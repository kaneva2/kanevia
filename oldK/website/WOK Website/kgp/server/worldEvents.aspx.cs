///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class worldEvents : KgpBasePage
    {
        private int _zoneInstanceId;
        private int _zoneType;

        protected void Page_Load(object sender, EventArgs e)
        {
            GetRequestParams();

            GetWorldEvents();
        }

        /// <summary>
        /// Gets all active (current/future) events for world
        /// </summary>
        private void GetWorldEvents()
        {
            if (_zoneInstanceId == 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid zoneInstanceId</ResultDescription>\r\n</Result>");
                return;
            }

            if (_zoneType == 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid zoneType</ResultDescription>\r\n</Result>");
                return;
            }

            int _communityId = GetCommunityFacade.GetCommunityIdFromWorldIds(0, 0, 0, _zoneInstanceId, _zoneType);

            if (_communityId == 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>community not found</ResultDescription>\r\n</Result>");
                return;
            }

            List<Event> worldEvents = GetEventFacade.GetWorldEventsWithEventFlags(_communityId);

            // Return a list of events for the world
            if (worldEvents != null)
            {
                XmlDocument doc = new XmlDocument();

                XmlNode root = doc.CreateElement("Result");
                doc.AppendChild(root);

                XmlElement elem = doc.CreateElement("ReturnCode");
                elem.InnerText = "0";
                root.AppendChild(elem);

                elem = doc.CreateElement("ReturnDescription");
                elem.InnerText = "success";
                root.AppendChild(elem);

                Response.Write(BuildWorldEventsXML(worldEvents));

                return;
            }
            else
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>error finding events for world</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }
        }

        /// <summary>
        /// Returns the xml for the meta game item balances.
        /// </summary>
        private string BuildWorldEventsXML(List<Event> events)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("Result"));
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";
            root.AppendChild(elem);

            elem = doc.CreateElement("ResultDescription");
            elem.InnerText = "success";
            root.AppendChild(elem);

            elem = doc.CreateElement("Events");
            foreach (Event e in events)
            {
                XmlElement itemElem = doc.CreateElement("Event");
                itemElem.InnerXml += string.Format("<EventId>{0}</EventId>", e.EventId);
                itemElem.InnerXml += string.Format("<Title>{0}</Title>", e.Title);
                itemElem.InnerXml += string.Format("<Details>{0}</Details>", e.Details);
                itemElem.InnerXml += string.Format("<StartTime>{0}</StartTime>", e.StartTime);
                itemElem.InnerXml += string.Format("<EndTime>{0}</EndTime>", e.EndTime);
                itemElem.InnerXml += string.Format("<ObjPlacementId>{0}</ObjPlacementId>", e.ObjPlacementId);
                elem.AppendChild(itemElem);
            }
            root.AppendChild(elem);

            return root.OuterXml;
        }
        
        ///<summary>
        /// Returns an xml document that will be sent back to the caller containing all experiment participant data.
        /// </summary>
        private XmlDocument WorldEventsToXML(List<Event> events)
        {
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(events.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);

            x.Serialize(writer, events);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());

            return doc;
        }

        /// <summary>
        /// Reads all request parameters from query string and provides appropriate defaults.
        /// </summary>
        private void GetRequestParams()
        {
            int iOut;

            _zoneInstanceId = Int32.TryParse(Request.Params["zoneInstanceId"], out iOut) ? iOut : 0;
            _zoneType = Int32.TryParse(Request.Params["zoneType"], out iOut) ? iOut : 0;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}