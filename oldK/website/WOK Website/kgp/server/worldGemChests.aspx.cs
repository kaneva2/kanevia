///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;
using Kaneva.BusinessLayer.Facade;
using System;
using System.Data;
using System.Xml;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class worldGemChests : KgpBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["action"] == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            string actionreq = Request.Params["action"];

            // Dev <add key='WokGameId' value='5354' />  Tied to community id  1118
            // http://dev-www.kaneva.com/channel/VirtualWorldofKaneva.channel

            // PV  <add key='WokGameId' value='3298' /> Tied to community id  372    (f2bed117K3df6K4d9eKb361K62bd04bdb98b)
            // http://preview.kaneva.com/channel/VirtualWorldofKaneva.channel

            // PROD <add key='WokGameId' value='3296' /> Tied to community id  372  (76321c71K3f69K4d58K999bKbd7254153b96)


            if (Request.Params["sToken"] == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-100</ReturnCode>\r\n  <ResultDescription>sToken not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            // Check Consumer Secret, only Wok Server is allowed
            APIAuthentication apiAuth = GetGameFacade.GetAPIAuth(KanevaGlobals.WokGameId);

            if (!apiAuth.ConsumerSecret.Equals(Request.Params["sToken"].ToString()))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-99</ReturnCode>\r\n  <ResultDescription>Invalid sToken</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            if (actionreq.Equals("CanWorldDropGems"))
            {
                if ((Request.Params["zoneInstanceId"] == null) || (Request.Params["zoneType"] == null))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>zoneInstanceId, zoneType or username not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                int zoneInstanceId = 0;
                int zoneType = 0;

                try
                {
                    zoneInstanceId = int.Parse(Request.Params["zoneInstanceId"]);
                    zoneType = int.Parse(Request.Params["zoneType"]);
                }
                catch (Exception)
                {
                    Response.Write("<Result>\r\n  <ReturnCode>-3</ReturnCode>\r\n  <ResultDescription>Invalid Parameter format</ResultDescription>\r\n</Result>");
                    return;
                }

                int communityId = GetCommunityFacade.GetCommunityIdFromWorldIds(KanevaGlobals.WokGameId, 0, 0, zoneInstanceId, zoneType);

                User owner = GetUserFacade.GetCommunityOwnerByCommunityId(communityId);
                if (owner.UserId == 0)
                {
                    Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid world owner</ResultDescription>\r\n</Result>");
                    return;
                }

                bool canDropGems = GetGameFacade.CanWorldDropGemChests(owner.UserId, communityId);

                Response.Write(BuildXMLResponse(canDropGems));
            }
            else
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invalid action</ResultDescription>\r\n</Result>");
            }
        }

        private string BuildXMLResponse(bool canDrop)
        {
            XmlDocument doc = new XmlDocument();
            DataSet ds = new DataSet();
            ds.DataSetName = "Result";

            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("CanDropGems");
            elem.InnerText = canDrop.ToString().ToLower();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            return root.OuterXml;
        }
    }
}