///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Web;
using System.Web.UI;
using System.Data;
using log4net;
using System.Linq;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects.API;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class TopWorldsTourServer : KgpBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["action"] == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            string actionreq = Request.Params["action"];
            GameFacade gameFadace = new GameFacade ();

            // Dev <add key='WokGameId' value='5354' />  Tied to community id  1118
            // http://dev-www.kaneva.com/channel/VirtualWorldofKaneva.channel

            // PV  <add key='WokGameId' value='3298' /> Tied to community id  372    (f2bed117K3df6K4d9eKb361K62bd04bdb98b)
            // http://preview.kaneva.com/channel/VirtualWorldofKaneva.channel

            // PROD <add key='WokGameId' value='3296' /> Tied to community id  372  (76321c71K3f69K4d58K999bKbd7254153b96)

           
            if (Request.Params["sToken"] == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-100</ReturnCode>\r\n  <ResultDescription>sToken not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

             // Check Consumer Secret, only Wok Server is allowed
            APIAuthentication apiAuth = gameFadace.GetAPIAuth(KanevaGlobals.WokGameId);

            if (!apiAuth.ConsumerSecret.Equals(Request.Params["sToken"].ToString()))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-99</ReturnCode>\r\n  <ResultDescription>Invalid sToken</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }


            if (actionreq.Equals("GetTourWorld"))
            {

                if ((Request.Params["start"] == null) || (Request.Params["max"] == null || (Request.Params["username"] == null)))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start, max or username not specified</ResultDescription>\r\n\r\n  </Result>";
                    Response.Write(errorStr);
                    return;
                }

                int page = Int32.Parse(Request.Params["start"].ToString());
                int items_per_page = Int32.Parse(Request.Params["max"].ToString());
                string username = Request.Params["username"].ToString();

                int userId = GetUserFacade.GetUserIdFromUsername(username);
                if (userId == 0)
                {
                    Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid username</ResultDescription>\r\n</Result>");
                    return;
                }

                int totalNumRecords = 0;

                GameFacade gameFacade = new GameFacade();

                DataTable dtGames = gameFacade.TopWorldsTour(m_userId, false, false, ref totalNumRecords, page, items_per_page, KanevaGlobals.WokGameId);
                Response.Write(BuildXMLResponse(dtGames, "Result", "3DApps", totalNumRecords, username));


            }
            else if (actionreq.Equals("RedeemTourWorldRewards"))
            {
                if ((Request.Params["zoneInstanceId"] == null) || (Request.Params["zoneType"] == null) || (Request.Params["username"] == null))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>zoneInstanceId, zoneType or username not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                int userId = 0;
                int zoneInstanceId = 0;
                int zoneType = 0;
                string username = "";

                try
                {
                    zoneInstanceId = int.Parse(Request.Params["zoneInstanceId"]);
                    zoneType = int.Parse (Request.Params["zoneType"]);
                    username = Request.Params["username"].ToString();
                }
                catch (Exception)
                {
                    Response.Write("<Result>\r\n  <ReturnCode>-3</ReturnCode>\r\n  <ResultDescription>Invalid Parameter format</ResultDescription>\r\n</Result>");
                    return;
                }

                userId = GetUserFacade.GetUserIdFromUsername(username);
                if (userId == 0)
                {
                    Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid username</ResultDescription>\r\n</Result>");
                    return;
                }


                int result = gameFadace.ReedemTopWorldReward (userId, zoneInstanceId, zoneType, Configuration.TourRewardsLooter);


                if (result == -2)
                {
                    Response.Write("<Result>\r\n  <ReturnCode>-2</ReturnCode>\r\n  <ResultDescription>Already redeemed</ResultDescription>\r\n</Result>");
                    return;
                }
                else if (result == 0)
                {
                    Response.Write("<Result>\r\n  <ReturnCode>-99</ReturnCode>\r\n  <ResultDescription>Unknown Error, Contact Web Dev</ResultDescription>\r\n</Result>");
                    return;
                }

                Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Success</ResultDescription>\r\n</Result>");

            }
            else
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invalid action</ResultDescription>\r\n</Result>");
            }
        }

        /// <summary>
        /// BuildXMLResponse
        /// </summary>
        private string BuildXMLResponse(DataTable pdt, string dataSetName, string tableName, int totalNumRecords, string username)
        {
            //DataSet ds = new DataSet();
            DataSet ds = pdt.DataSet;
            ds.DataSetName = dataSetName;

            pdt.TableName = tableName;
            // ds.Tables.Add(pdt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("Username");
            elem.InnerText = username;

            root.InsertBefore(elem, root.FirstChild);

            int numRecords = pdt.Rows.Count;
            elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            InsertPassthroughTag(ref doc);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            return root.OuterXml;
        }

    }
}