///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for newAvatar.
    /// </summary>
    public class newAvatar : KgpBasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(false))
            {
                string responseStr = null;

                if (Request.Params["action"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                string actionreq = Request.Params["action"];

                if (actionreq.Equals("NewAvatar"))
                {
                    // Award user World Fame for creating an Avatar
                    FameFacade fameFacade = new FameFacade();
                    fameFacade.RedeemPacket(m_userId, (int)PacketId.WORLD_OT__FIRST_LOGIN, (int)FameTypes.World);

                    // See if this user was invited by someone, note, status must not be max ip reached, this means someone was cheating the system
                    DataRow drInvite = UsersUtility.GetInviteByUserInvited(m_userId);

                    // Was this person an invitie?
                    if (drInvite != null)
                    {
                        // Is it a valid invite?
                        if (Convert.ToInt32(drInvite["invite_status_id"]).Equals ((int) Constants.eINVITE_STATUS.REGISTERED))
                        {
                            // Call logic to determine awards
                            UsersUtility.AwardsInvitesInWorld(Convert.ToInt32(drInvite["user_id"]), Common.GetVisitorIPAddress(), Convert.ToInt32(drInvite["invite_id"]));
                        }

                    }
                    responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Success</ResultDescription>\r\n</Result>";
                    Response.Write(responseStr);
                    return;

                }                
                else
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action request unknown</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
