///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class eventCreate : KgpBasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            m_logger.Debug("CreateEvent: Starting Create.");
            if (CheckUserId(false))
            {
                if (Request.Params["action"] == null)
                {
                    m_logger.Debug("CreateEvent: No action");
                    SendResponse(3, "Invalid event action.");
                    return;
                }

                switch (Request.Params["action"])
                {
                    case "createEvent":
                        CreateEvent();
                        break;
                    case "autoTesterCreateEvent":
                        CreateAutoTesterEvent();
                        break;
                }

                return;
            }
        }

        public void CreateEvent()
        {
            EventFacade eventFacade = new EventFacade();
            int userId = KanevaWebGlobals.CurrentUser.UserId;

            m_logger.Debug("CreateEvent: Looking for fields.");
            if (Request.Params["eventName"] == null || Request.Params["description"] == null || Request.Params["typeId"] == null || Request.Params["premium"] == null || Request.Params["duration"] == null || Request.Params["zoneIndex"] == null || Request.Params["zoneInstanceId"] == null)
            {
                m_logger.Debug("CreateEvent: Missing Field");
                SendResponse(2, "The event parameters are incorrect.");
                return;
            }


            string eventName = Request.Params["eventName"];
            string description = Request.Params["description"];

            bool isAPEvent = false;

            if (Request.Params["APEvent"] != null)
            {
                isAPEvent = Request.Params["APEvent"].Equals("Y");
            }

            //check for injections scripts
            if (KanevaWebGlobals.ContainsInjectScripts(eventName) || KanevaWebGlobals.ContainsInjectScripts(description))
            {
                SendResponse(3, "Your input contains invalid scripting, please remove script code and try again.");
                m_logger.Warn("User " + userId + " tried to script from IP " + Common.GetVisitorIPAddress());
                return;
            }

            //check for bad or restricted words
            if (KanevaWebGlobals.isTextRestricted(eventName, Constants.eRESTRICTION_TYPE.ALL))
            {
                SendResponse(4, "The event name contains an offensive word. To continue, change the word.");
                m_logger.Debug("CreateEvent: Poth mouth word in event name");
                return;
            }
            if (KanevaWebGlobals.isTextRestricted(description, Constants.eRESTRICTION_TYPE.ALL))
            {
                SendResponse(4, "The event description contains an offensive word. To continue, change the word.");
                m_logger.Debug("CreateEvent: Poth mouth word in event description");
                return;
            }



            int zoneIndex = Int32.Parse(Request.Params["zoneIndex"].ToString());
            int zoneInstanceId = Int32.Parse(Request.Params["zoneInstanceId"].ToString());

            string location = "";
            int communityId = 0; //TO DO: get id of the community or apartment
            try
            {
                DataRow drEventLocation = EventsUtility.GetLocationDetail2(zoneIndex, zoneInstanceId, 0);
                location = drEventLocation["name"].ToString();
            }
            catch
            {
                SendResponse(2, "An error has occured determining the event location." + zoneIndex + " " + zoneInstanceId);
                return;
            }

            DateTime startTime = DateTime.Now;
            int duration = Int32.Parse(Request.Params["duration"].ToString());
            DateTime endTime = startTime.AddMinutes(duration);     //  int Request.Params["duration"];
            int typeId = Int32.Parse(Request.Params["typeId"].ToString());
            int timeZoneId = 0;

            //
            // Does this user have an event happening now?
            m_logger.Debug("CreateEvent: Looking for Events.");
            try
            {
                // Concurrency Check
                int ConcurrentCheck = eventFacade.ConcurrencyCheck(0, userId, communityId, startTime, endTime, zoneIndex, zoneInstanceId);

                m_logger.Debug("ConcurrentCheck: " + ConcurrentCheck + "userId" + userId + "communityId" + communityId + "startTime" + startTime + "endTime" + endTime + "zoneIndex" + zoneIndex + "zoneInstanceId" + zoneInstanceId);

                if (ConcurrentCheck.Equals(2))
                {
                    m_logger.Error("Concurrency rule check caused error: UserId=" + userId.ToString() + " : communityId=" + communityId);
                    SendResponse(2, "Unable to create event.");
                    return;
                }

                if (ConcurrentCheck.Equals(0))
                {
                    //m_logger.Error("Concurrency rule check failed: UserId=" + userId.ToString() + " : communityId=" + communityId + " : ");
                    SendResponse(2, "There is already an event in this location.");
                    return;
                }

            }
            catch
            {
                m_logger.Debug("CreateEvent: Error getting user's exisiting events.");
                SendResponse(2, "Error checking for existing events.");
                return;
            }


            m_logger.Debug("CreateEvent: Looking for Premium info...");
            // default to not premium
            int premium = 0;
            try
            {
                if (Request.Params["premium"] != null)
                {
                    if (Request.Params["premium"] == "GPOINTS" || Request.Params["premium"] == "KPOINTS")
                    {
                        //int premium = Request.Params["premium"] == "T" ? 1 : 0;

                        premium = EventsUtility.ChargeForPremiumEvent(userId, Request.Params["premium"]);

                        if (premium != 1)
                        {
                            if (premium == -1)
                            {
                                SendResponse(1, "You do not enough credits or rewards.");
                            }
                            else
                            {
                                SendResponse(2, "An error has occured creating the event while doing premium transaction.");
                            }
                            return;
                        }
                    }
                }
            }
            catch
            {
                SendResponse(2, "An error has occured creating the event while doing premium transaction.");
                return;
            }

            int privacy = 0;


            // Get the current zone's permissions and set event's privacy
            /*
            int zoneAccess = 0;
            bool isGM = false;
            int zone_instance_id = 0;

            UsersUtility.GetUserAccessRightsToCurrentZone(KanevaWebGlobals.CurrentUser.UserId, ref zoneAccess, ref isGM, ref zone_instance_id);
            SendResponse(0, zone_instance_id.ToString());
            return;
            */

            // Metrics tracking for accepts
            string requestId = GetUserFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_INVITE, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
            string requestTypeSentIdEvent = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_WEBSITE, 0);

            // Create the event
            int eventId = eventFacade.InsertEvent(communityId, userId, eventName, description,
                location, zoneIndex, zoneInstanceId, startTime, endTime,
                typeId, timeZoneId, premium, privacy, isAPEvent, requestTypeSentIdEvent);

            //
            // Blast Event  
            try
            {
                string blastTitle = eventName;
                string share = ResolveUrl("~/mykaneva/events/eventDetail.aspx?event=" + eventId);
                string blast = description;

                BlastFacade blastFacade = new BlastFacade();
                //if (blastFacade.SendEventBlast(GetUserId(), communityId, KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.NameNoSpaces, title, location, "", desc, startTime.ToString(), startTime, "", KanevaWebGlobals.CurrentUser.ThumbnailSmallPath, false).Equals(1))

                string cssHighLight = "";
                if (premium == 1)
                {
                    cssHighLight = "highlight1";
                }

                bool isCommunityBlast = false;
                m_logger.Debug("Blasting event_id: " + eventId);
                if (blastFacade.SendEventBlast(GetUserId(), communityId, KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.NameNoSpaces, blastTitle, location, "", blast, startTime.ToString() + " EST", startTime, cssHighLight, KanevaWebGlobals.CurrentUser.ThumbnailSmallPath, isCommunityBlast).Equals(1))
                {

                }
                else
                {
                    m_logger.Debug("Failure to blast event_id: " + eventId);
                }

            }
            catch
            {
                m_logger.Debug("Error sending blast event_id: " + eventId);
            }

            // Return result

            // General error
            // SendResponse(0, "There was an error creating the event.");


            //m_logger.Debug("CreateEvent: Success");
            SendResponse(0, "Your event has been created!");
            return;

        }

        public void CreateAutoTesterEvent()
        {
            try
            {
                int eventId = 0;

                // Confirm user permissions.
                if (!GetUserFacade.IsUserAutomatedTestAccount(KanevaWebGlobals.CurrentUser))
                {
                    SendResponse(2, "Unauthorized user.");
                    return;
                }

                // Confirm that this call is active on this environment.
                if (!Configuration.AutomatedTestUserGenerationEnabled)
                {
                    SendResponse(2, "Unauthorized action.");
                    return;
                }

                // Check required params.
                if (string.IsNullOrWhiteSpace(Request.Params["title"])
                    || string.IsNullOrWhiteSpace(Request.Params["description"])
                    || string.IsNullOrWhiteSpace(Request.Params["worldName"])
                    || string.IsNullOrWhiteSpace(Request.Params["username"]))
                {
                    m_logger.Debug("CreateAutoTesterEvent: Missing Field");
                    SendResponse(2, "The event parameters are incorrect.");
                    return;
                }

                // Validate username.
                int userId = GetUserFacade.GetUserIdFromUsername(Request.Params["username"]);
                if (userId <= 0)
                {
                    SendResponse(2, "Invalid username.");
                    return;
                }

                // Validate worldName.
                int communityId = GetCommunityFacade.GetCommunityIdFromName(Request.Params["worldName"], false);
                if (communityId <= 0)
                {
                    SendResponse(2, "Invalid worldName.");
                    return;
                }

                // Check for JS injection scripts.
                if (KanevaWebGlobals.ContainsInjectScripts(Request.Params["title"]) ||
                    KanevaWebGlobals.ContainsInjectScripts(Request.Params["description"]))
                {
                    SendResponse(2, "Your input contains invalid scripting, please remove script code and try again.");
                    return;
                }

                // Default to event starting now, unless specified.
                DateTime startTime = DateTime.Now;
                if (!string.IsNullOrWhiteSpace(Request.Params["startTime"]) 
                    && !DateTime.TryParse(Request.Params["startTime"], out startTime))
                {
                    SendResponse(2, "Invalid startTime.");
                    return;               
                }
                DateTime endTime = startTime.AddHours(1);

                // Check for date conflicts.
                DateTime conflictDate = new DateTime();
                int concurrentCheck = GetEventFacade.ConcurrencyCheckRecurring(eventId, KanevaWebGlobals.CurrentUser.UserId, communityId, startTime, endTime, false, 0, ref conflictDate);
                switch (concurrentCheck)
                {
                    case ((int)Event.eCONCURRENCY_ERROR.GENERAL):
                        SendResponse(2, "There can only be one event at a time in a particular zone.");
                        return;
                    case ((int)Event.eCONCURRENCY_ERROR.TIME_OVERLAP):
                        SendResponse(2, "Only 1 event per World per hour can be scheduled.");
                        return;
                    case ((int)Event.eCONCURRENCY_ERROR.EXCEED_MAX_EVENTS_PER_DAY):
                        SendResponse(2, "Only 2 events per day are allowed per World.");
                        return;
                    case ((int)Event.eCONCURRENCY_ERROR.EXCEED_MAX_WORLD_EVENTS_PER_DAY):
                        SendResponse(2, "This world has reached it's maximum number of events for the day.");
                        return;
                }

                // Create the event.
                Event evt = new Event
                {
                    CommunityId = communityId,
                    UserId = userId,
                    Title = Server.HtmlEncode(Request.Params["title"]),
                    Details = Server.HtmlEncode(Request.Params["description"]),
                    Location = Server.HtmlEncode(Request.Params["worldName"]),
                    StartTime = startTime,
                    EndTime = endTime,
                    Priority = (int)Event.ePRIORITY.PREMIUM,
                    TypeId = 2,
                    IsPrivate = !CommunityUtility.IsCommunityPublic(communityId),
                    InviteFriends = false,
                    RecurringStatusId = (int)Event.eRECURRING_STATUS_TYPE.NONE
                };

                // Insert the event.
                eventId = GetEventFacade.InsertEvent(evt, string.Empty);
                if (eventId > 0)
                    SendResponse(0, "Event created successfully.");
                else
                    SendResponse(2, "Error inserting event record.");

                return;
            }
            catch (Exception exc)
            {
                SendResponse(2, "An error occurred while trying to save the event. Error: " + exc.Message);
                return;
            }
        }

        public void SendResponse(int returnCode, string message)
        {
            string responseStr = "<Result>\r\n  <ReturnCode>" + returnCode + "</ReturnCode>\r\n  <ReturnDescription>" + message + "</ReturnDescription>\r\n</Result>";
            Response.Write(responseStr);
            return;
        }

        protected string responseStr;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
