///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for tradeItems.
	/// </summary>
	public class tradeItems : KgpBasePage
	{
        private int _start;
        private int _max;
        double _amount;
        private string _action;
        private string _username;
        private string[] _requestedGlids;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
                GetRequestParams();

                // Process based on action.
                switch (_action)
                {
                    case "giftCreditsToUser":
                        ProcessGiftCreditsToUser();
                        break;
                    default:
                        ProcessGetUserTradeItems();
                        break;
                }
			}
		}

        /// <summary>
        /// Reads all request parameters from query string and provides appropriate defaults.
        /// </summary>
        private void GetRequestParams()
        {
            int iOut;
            double dOut;

            _action = Request.Params["action"];
            _username = Request.Params["username"];
            _requestedGlids = (Request.Params["GLIDS"] ?? string.Empty).Split(',');

            _start = Int32.TryParse(Request.Params["start"], out iOut) ? iOut : 0;
            _max = Int32.TryParse(Request.Params["max"], out iOut) ? iOut : 0;
            _amount = Double.TryParse(Request.Params["amount"], out dOut) ? dOut : 0;
        }

        /// <summary>
        /// Gives credits from the logged-in user to another user.
        /// </summary>
        private void ProcessGiftCreditsToUser()
        {
            if (_amount < 1)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>amount not specified or invalid</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }
            
            if (string.IsNullOrWhiteSpace(_username))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>username not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            int receivingUserId = GetUserFacade.GetUserIdFromUsername(_username);
            if (receivingUserId == 0)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid username</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            try
            {
                GetUserFacade.AdjustUserBalance(m_userId, Currency.CurrencyType.CREDITS, -(_amount), (ushort)TransactionType.eTRANSACTION_TYPES.CASH_TT_TRADE);
                GetUserFacade.AdjustUserBalance(receivingUserId, Currency.CurrencyType.CREDITS, _amount, (ushort)TransactionType.eTRANSACTION_TYPES.CASH_TT_TRADE);
            }
            catch (Exception ex)
            {
                string errorStr;
                if (ex.Message.Equals(((int)UserFacade.eAdjustUserBalanceReturnCode.InsufficientFunds).ToString()))
                {
                    errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>insufficient funds</ResultDescription>\r\n</Result>";
                    
                }
                else
                {
                    errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>error</ResultDescription>\r\n</Result>";
                }

                Response.Write(errorStr);
                return;
            }

            GetUserFacade.SendCreditGiftNotificationToPlayer(m_userId, receivingUserId, _amount);
            Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>");
        }

        /// <summary>
        /// Get user trade items.
        /// </summary>
        private void ProcessGetUserTradeItems()
        {
            if (_start < 1 || _max < 1)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>page number and items per page were invalid or not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }
            
            if (_requestedGlids == null || _requestedGlids.Length == 0)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>GLIDS not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            DataSet ds = new DataSet();
            ds.DataSetName = "Result";

            PagedDataTable pdt = UsersUtility.GetWokTradeItemNames(m_playerId, _requestedGlids, "global_id ASC", _start, _max);

            pdt.TableName = "TradeItems";
            ds.Tables.Add(pdt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            int numRecords = pdt.Rows.Count;
            XmlElement elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = pdt.TotalCount;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
