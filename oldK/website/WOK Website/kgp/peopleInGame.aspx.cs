///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.Caching;
using log4net;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for peopleInGame
	/// </summary>
	public class peopleInGame : KgpBasePage
	{
        const int MIN_USERNAME_LEN = 3;

		private void Page_Load(object sender, System.EventArgs e)
		{
            string action = "";
            int zoneInstanceId = 0;
            int currentzoneIndex = 0;

            m_userId = GetUserId();
            m_playerId = GetPlayerId();

			if ( CheckUserId( false ) )
			{
   				if ( (Request.Params["start"] == null) || (Request.Params["max"] == null) )
				{
					string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>page number and items per page not specified</ResultDescription>\r\n</Result>";
					Response.Write(errorStr);
					return;
				}

                if (Request.Params["action"] != null)
                {
                    action = Request.Params["action"].ToString ();

                    if (action.Equals("getZone"))
                    {
                        if ((Request.Params["zid"] == null || (Request.Params["czi"] == null)))
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>zid or czi not specified</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }

                        zoneInstanceId = Int32.Parse(Request.Params["zid"].ToString());
                        currentzoneIndex = Int32.Parse(Request.Params["czi"].ToString());
                    }

                    if( action.Equals("getUrl"))
                    {
                        if (Request.Params["user"] == null)
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>user not specified</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    }
                }

				int page = Int32.Parse(Request.Params["start"].ToString());
				int items_per_page = Int32.Parse(Request.Params["max"].ToString());
   				string sortByName = Request.Params["sortbyname"];
   				string userName = Request.Params["search"];
                string newSortOpt = Request.Params["sort"];

				DataSet ds = new DataSet();

				ds.DataSetName = "Result";

				PagedDataTable pdt;

                if (action.Equals("getZone"))
                {
                    string orderby = "";

                    if (userName == null)
                    {
                        if (sortByName != null || newSortOpt == "n")
                        {
                            // by name +
                            orderby = " p.name asc ";
                        }
                        else if (newSortOpt == "N")
                        {
                            // by name -
                            orderby = " p.name desc ";
                        }
                        else if (newSortOpt == "z" || newSortOpt == "zn")
                        {
                            // zone+, name+
                            orderby = " pz.current_zone asc, p.name asc ";
                        }
                        else if (newSortOpt == "Z" || newSortOpt == "Zn")
                        {
                            // zone-, name+
                            orderby = " pz.current_zone desc, p.name asc ";
                        }
                        else if (newSortOpt == "zN")
                        {
                            // zone+, name-
                            orderby = " pz.current_zone asc, p.name desc ";
                        }
                        else if (newSortOpt == "ZN")
                        {
                            // zone-, name-
                            orderby = " pz.current_zone desc, p.name desc ";
                        }
                        else if (newSortOpt == "r")
                        {
                            // random
                            orderby = " rand() ";
                        }
                        else if (newSortOpt == "login")
                        {
                            orderby = " dal.created_date desc ";
                        }
                        else
                        {
                            // default: zone+, name+
                            orderby = " pz.current_zone asc, p.name asc ";
                        }
                    }

                    // Get users in a zone
                    pdt = UsersUtility.GetUsersInZone (m_userId, zoneInstanceId, currentzoneIndex, orderby, page, items_per_page, userName);
                    pdt.TableName = "GameUser";
                    ds.Tables.Add(pdt);
                }
                else if (action.Equals("getUrl"))
                {
                    // Get URL for a single player
                    string user = Request["user"];
                    int userId, playerId;

                    if (!ParseUserName(user, out userId, out playerId))
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>user not found</ResultDescription>\r\n</Result>");
                        return;
                    }

                    string inGameId = KanevaGlobals.WokGameId.ToString();
                    DataTable dt = UsersUtility.GetUserLoggedInGameId(userId);
                    if (dt!=null && dt.Rows.Count>0)
                    {
                        inGameId = dt.Rows[0]["game_id"].ToString();
                    }
                    
                    string stpUrl;

                    if (inGameId == KanevaGlobals.WokGameId.ToString())
                    {
                        // in WOK - kaneva://WokGameId/user.people
                        stpUrl = StpUrl.MakeUrlPrefix(inGameId) + "/" + StpUrl.MakePersonUrlPath(HttpUtility.UrlEncode(user));
                    }
                    else if (inGameId != "0")
                    {
                        // in 3d APP - kaneva://3dAppGameId/
                        stpUrl = StpUrl.MakeUrlPrefix(inGameId) + "/";
                    }
                    else
                    {
                        // not in world - kaneva://WokGameId/user.home
                        stpUrl = StpUrl.MakeUrlPrefix(KanevaGlobals.WokGameId.ToString()) + "/" + StpUrl.MakeAptUrlPath(HttpUtility.UrlEncode(user));
                    }

                    Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription><Url>" + stpUrl + "</Url>\r\n</Result>");
                    return;
                }
                else
                {
                    string orderby = "";

                    if (userName == null)
                    {
                        if (sortByName != null || newSortOpt == "n")
                        {
                            // by name +
                            orderby = " name asc ";
                        }
                        else if (newSortOpt == "N")
                        {
                            // by name -
                            orderby = " name desc ";
                        }
                        else if (newSortOpt == "z" || newSortOpt == "zn")
                        {
                            // zone+, name+
                            orderby = " current_zone asc, name asc ";
                        }
                        else if (newSortOpt == "Z" || newSortOpt == "Zn")
                        {
                            // zone-, name+
                            orderby = " current_zone desc, name asc ";
                        }
                        else if (newSortOpt == "zN")
                        {
                            // zone+, name-
                            orderby = " current_zone asc, name desc ";
                        }
                        else if (newSortOpt == "ZN")
                        {
                            // zone-, name-
                            orderby = " current_zone desc, name desc ";
                        }
                        else if (newSortOpt == "r")
                        {
                            // random
                            orderby = " rand() ";
                        }
                        else if (newSortOpt == "login")
                        {
                            orderby = " last_logon desc ";
                        }
                        else
                        {
                            // default: zone+, name+
                            orderby = " current_zone asc, name asc ";
                        }
                    }

                    // Get all people in game
                    pdt = UsersUtility.GetUsersInGame(m_userId, orderby, page, items_per_page, userName);
                    pdt.TableName = "GameUser";
                    ds.Tables.Add(pdt);

                    // Add this per Jim request
                    // UsersUtility.GetUsersInGame returns TotalNumber of records.  
                    // That�s the number we want to use to call the SP.  
                    // I�m not sure what the best way to keep global data in the cs file, 
                    // but I�d set some global variables to the peak since last call to SP.  
                    // If the current value is higher that last, save it off.  
                    // Then occasionally (every 5 min?) call the SP with the saved peak.  
                    // You can reset the peak in memory after every SP call.

                    // There may be a slight skew since if Jason gets a peak at 2:58 then call the SP 
                    // at 3:01 the peak would be registered in the 3 hour instead of 2, but it still gets registered.  
                    // I was thinking Jason could track the hour also, 
                    // but it doesn�t seem to really make a difference as long as the peak is registered.
                    string uiMaxUsers = (string)Cache[cCACHE_MAX_USERS];

                    if (uiMaxUsers == null)
                    {
                        //m_logger.Debug("Adding to cache peak users count " + pdt.TotalCount.ToString());

                        // Add the number to the cache
                        Cache.Insert(cCACHE_MAX_USERS, pdt.TotalCount.ToString(), null, DateTime.Now.AddMinutes(5),
                            System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, new System.Web.Caching.CacheItemRemovedCallback(this.CacheExpired));
                    }
                    else if (pdt.TotalCount > Convert.ToInt32(uiMaxUsers))
                    {
                        //m_logger.Debug("Replacing cache peak users count " + pdt.TotalCount.ToString());

                        // Update the cache with the greater number
                        //Cache[cCACHE_MAX_USERS] = pdt.TotalCount.ToString();
                        Cache.Insert(cCACHE_MAX_USERS, pdt.TotalCount.ToString(), null, DateTime.Now.AddMinutes(1),
                            System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, new System.Web.Caching.CacheItemRemovedCallback(this.CacheExpired));
                    }
                }

				XmlDocument doc = new XmlDocument();
				doc.LoadXml(ds.GetXml());
				XmlNode root = doc.DocumentElement;

				int numRecords = pdt.Rows.Count;
				XmlElement elem = doc.CreateElement("NumberRecords");
				elem.InnerText= numRecords.ToString();

				root.InsertBefore( elem, root.FirstChild);

				int totalNumRecords = pdt.TotalCount;
				elem = doc.CreateElement("TotalNumberRecords");
				elem.InnerText= totalNumRecords.ToString();

				root.InsertBefore( elem, root.FirstChild);

				elem = doc.CreateElement("ReturnDescription");
				elem.InnerText="success";

				root.InsertBefore( elem, root.FirstChild);

				elem = doc.CreateElement("ReturnCode");
				elem.InnerText="0";

				root.InsertBefore( elem, root.FirstChild);

				Response.Write(root.OuterXml);
			}
		}

        /// <summary>
        /// CacheExpired
        /// </summary>
        protected void CacheExpired (string key, object item, CacheItemRemovedReason reason)
        {
            //m_logger.Debug("CacheExpired key = " + key.ToString () + " Reason " + reason.ToString ());

            try
            {
                if ((reason.Equals(CacheItemRemovedReason.Expired) || reason.Equals(CacheItemRemovedReason.Underused))
                    && key.Equals(cCACHE_MAX_USERS))
                {
                    //m_logger.Debug("Updating peak to " + item.ToString ());

                    // Call the SP
                    UsersUtility.UpdatePeakConcurrentPlayers(Convert.ToInt32(item));
                }
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error updating peak", exc);
            }
        }

        private bool ParseUserName(string userName, out int userId, out int playerId)
        {
            UserFacade userFacade = new UserFacade();

            userId = 0;
            playerId = 0;

            if (userName.Length >= MIN_USERNAME_LEN)
            {
                DataRow dr = userFacade.GetUserIdPlayerIdFromUsername(userName);
                if (dr != null)
                {
                    try
                    {
                        userId = Convert.ToInt32(dr["user_id"]);
                        playerId = Convert.ToInt32(dr["player_id"]);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        // DB error
                        m_logger.Error("Error parsing data row from GetUserIdPlayerIdFromUsername", ex);
                        userId = 0;
                        playerId = 0;
                    }
                }
            }

            return false;
        }

        private const string cCACHE_MAX_USERS = "cacheMaxUsers";

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
