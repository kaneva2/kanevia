///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
  /// <summary>
  /// Summary provides access to single field updates on wok.inventory.
  /// </summary>
  public class inventoryUpdate : KgpBasePage
    {
        #region Declarations

        const string SUCCESS = "0";
        const string ERROR = "1";

        #endregion

    private void Page_Load(object sender, System.EventArgs e)
    {
        string authenticationError  = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n <ResultDescription>Authentication failed.</ResultDescription>\r\n</Result>";
        string parameterError       = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n <ResultDescription>Invalid usage: missing required gid</ResultDescription>\r\n</Result>";
        string actionError          = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n <ResultDescription>Invalid usage: unkown action</ResultDescription>\r\n</Result>";
        string usageError           = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n <ResultDescription>Invalid usage: action?armed&glid=n&inventoryType=str&inventorySubType=n&val=[T|F]</ResultDescription>\r\n</Result>";
        string successMsg           = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>rows affected {0}</ResultDescription>\r\n</Result>";

        #region authenticate the request
          if (Request.Params["gid"] == null) {
            Response.Write(parameterError);
            return;
          }
          int argGameId = Int32.Parse(Request.Params["gid"]);
          string argIP = Common.GetVisitorIPAddress();

          if (!ServerUtility.IsKnownServer(argIP, argGameId)) {
            Response.Write(authenticationError);
            return;
          }
        #endregion

        // convert from web args to native types
        // the action flag is used to determine the format of remaining user args
      if ((Request.Params["action"] != null)) {
          string action = Request.Params["action"];
          switch (action) {
            case "armed":

              bool badArgs = Request.Params["playerId"]         == null || 
                             Request.Params["glid"]             == null || 
                             Request.Params["inventoryType"]    == null || 
                             Request.Params["inventorySubType"] == null ||
                             Request.Params["val"]              == null;

              if (badArgs) {
                Response.Write(usageError);
                return;
              }
              // convert form args to database fields.
              Hashtable keys = new Hashtable();
                keys.Add("global_id",          Int32.Parse(Request.Params["glid"].ToString()));
                keys.Add("inventory_type",     Request.Params["inventoryType"].ToString());
                keys.Add("inventory_sub_type", Int32.Parse(Request.Params["inventorySubType"].ToString()));
                keys.Add("player_id",          Int32.Parse(Request.Params["playerId"].ToString()));

              Hashtable fields = new Hashtable();
              fields.Add(action, Request.Params["val"].ToString());

              int rowsAffected = UsersUtility.UpdateInventoryFields(keys, fields);
              successMsg = successMsg.Replace("{0}", rowsAffected.ToString());
              Response.Write(successMsg);
              break;

            default:
                Response.Write(actionError);
                break;
          }
        }
        else {
          string resp = "<Result><ReturnCode>-1</ReturnCode><ResultDescription>unkown action</ResultDescription></Result>";
          Response.Write(resp);
        }
     }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
      //
      // CODEGEN: This call is required by the ASP.NET Web Form Designer.
      //
      InitializeComponent();
      base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.Load += new System.EventHandler(this.Page_Load);
    }
    #endregion
  }
}
