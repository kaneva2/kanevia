///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Xml;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for Fame.
    /// </summary>
    public class _recordNewUserFunnel : KgpBasePage
    {
        #region Declarations

        private int _page;
        private int _itemsPerPage;
        private string _funnelStep;

        private string _successResult = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ReturnDescription>{0}</ReturnDescription>\r\n </Result>";
        private string _errorResult = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>{0}</ReturnDescription>\r\n </Result>";

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(true))
            {
                // Get parameters
                GetRequestParams();

                // Process actions
                ProcessRecordFunnelProgression();
            }
        }

        /// <summary>
        /// Reads all request parameters from query string and provides appropriate defaults
        /// </summary>
        private void GetRequestParams()
        {
            int iOut;

            _page = (Int32.TryParse(Request.Params["start"], out iOut) ? iOut : 0);
            _itemsPerPage = (Int32.TryParse(Request.Params["max"], out iOut) ? iOut : 0);
            _funnelStep = Request.Params["funnelStep"];
        }

        /// <summary>
        /// Records user progression through funnel
        /// </summary>
        private void ProcessRecordFunnelProgression()
        {
            try
            {
                GetMetricsFacade.RecordNewUserFunnelProgression(KanevaWebGlobals.CurrentUser.UserId, _funnelStep);
            }
            catch (ArgumentException e)
            {
                Response.Write(string.Format(_errorResult, e.Message));
                return;
            }

            Response.Write(string.Format(_successResult, "success"));
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
