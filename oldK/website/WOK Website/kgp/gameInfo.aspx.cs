///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class gameInfo : KgpBasePage
    {
        const string NOT_FOUND_RESP = "<game_status_id>-1 Game not found</game_status_id>";
        const string BAD_PARAM_RESP = "<game_status_id>-1 Bad parameter</game_status_id>";
        const string ACTIVE_SERVER_STATUS = "<game_status_id>1</game_status_id>";

        const string DEFAULT_PANE1_URL = "http://streaming.kaneva.com/media/WOK/launcher/patch_notes.html";
        const string DEFAULT_PANE2_URL = "http://streaming.kaneva.com/media/WOK/launcher/specials.html";


        /// <summary>
        /// get the server for a STAR will return resp above, or 0 server port
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // if (CheckUserId(false))
            {
                String response = "<server><settings>";

                Int32 gameId = 0;
                if ((Request.Params["id"] == null) || !Int32.TryParse(Request.Params["id"],out gameId))
                {
                    response += BAD_PARAM_RESP;
                }
                else
                {
                    DataTable dt = ServerUtility.GetGamePatcherSettings(gameId);
                    if (dt.Rows.Count > 0)
                    {
                        DataSet ds = new DataSet();

                        ds.DataSetName = "server";

                        dt.TableName = "settings";
                        ds.Tables.Add(dt);

                        Response.Write(ds.GetXml());

                        return;
                    }
                    else
                    {
                        // no entry found, return defaults for WOK
                        response += NOT_FOUND_RESP;
                    }
                }

                response += "</settings></server>";

                Response.Write(response);
            }

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
