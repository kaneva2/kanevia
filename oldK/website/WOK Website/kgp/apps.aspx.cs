///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Xml;
using System.Data;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class apps : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string actionreq = "GetMostPopulated";
            int limit = 50;

            if (Request.Params["limit"] != null)
            {
                limit = Convert.ToInt32 (Request.Params["limit"]);
            }

            if (Request.Params["action"] != null)
            {
                actionreq = Request.Params["action"];
            }

            if (actionreq.Equals("GetMostPopulated"))
            {
               string strSelect = " SELECT pc.game_id,  pc.name " +
                   " FROM wok.worlds_cache pc " +
                   " WHERE pc.population > 0 " +
                   " AND pc.has_thumbnail = 'Y' " +
                   " AND pc.is_public = 'Y' " +
                   " AND pc.parent_community_id IS NULL " +
                   " ORDER BY pc.population DESC, pc.number_of_diggs DESC" +
                   " limit " + limit;


               DataTable dtResult = KanevaGlobals.GetDatabaseUtility().GetDataTable(strSelect);

               DataSet ds = new DataSet();

               ds.DataSetName = "Most Populated Apps";
               dtResult.TableName = "Apps";
               ds.Tables.Add(dtResult);

               XmlDocument doc = new XmlDocument();
               doc.LoadXml(ds.GetXml());
               XmlNode root = doc.DocumentElement;

               XmlElement elem = doc.CreateElement("ReturnDescription");
               elem.InnerText = "success";

               root.InsertBefore(elem, root.FirstChild);

               elem = doc.CreateElement("ReturnCode");
               elem.InnerText = "0";

               root.InsertBefore(elem, root.FirstChild);

               Response.Write(root.OuterXml);
            }
            else if (actionreq.Equals("GetMostRaved"))
            {
                GameFacade gameFacade = new GameFacade ();
                int totalCount = 0;
                DataTable dtRaved = (DataTable) gameFacade.GetMostRaved3DApps (false, false, ref totalCount, 1, limit, KanevaGlobals.WokGameId);

                DataTable dtTemp = new DataTable ();

                dtTemp.Columns.Add (new DataColumn ("game_id", System.Type.GetType ("System.String")));
                dtTemp.Columns.Add (new DataColumn ("name", System.Type.GetType ("System.String")));

                if (dtRaved.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtRaved.Rows)
                    {
                        DataRow newRow = dtTemp.NewRow ();
                        newRow["game_id"] = dr["game_id"].ToString ();
                        newRow["name"] = dr["name"].ToString ();
                        dtTemp.Rows.Add (newRow);
                    }
                }

                DataSet ds = new DataSet ();

                ds.DataSetName = "Most Raved Apps";
                dtTemp.TableName = "Apps";
                ds.Tables.Add (dtTemp);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(ds.GetXml());
                XmlNode root = doc.DocumentElement;

                XmlElement elem = doc.CreateElement("ReturnDescription");
                elem.InnerText = "success";

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("ReturnCode");
                elem.InnerText = "0";

                root.InsertBefore(elem, root.FirstChild);

                Response.Write(root.OuterXml);
            }
            else if (actionreq.Equals("GetMostRequested"))
            {
                string strSelect = " SELECT pc.game_id,  pc.name, COUNT(pc.game_id) as cnt " +
                    " FROM wok.worlds_cache pc, kaneva.notification_requests nr " +
                    " WHERE pc.game_id = nr.game_id " +
                    " AND pc.parent_community_id IS NULL " +
                    " GROUP BY pc.game_id " +
                    " ORDER BY cnt DESC " +
                    " limit " + limit;

                DataTable dtResult = KanevaGlobals.GetDatabaseUtility().GetDataTable(strSelect);

                DataSet ds = new DataSet();

                ds.DataSetName = "Most Requested Apps";
                dtResult.TableName = "Apps";
                ds.Tables.Add(dtResult);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(ds.GetXml());
                XmlNode root = doc.DocumentElement;

                XmlElement elem = doc.CreateElement("ReturnDescription");
                elem.InnerText = "success";

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("ReturnCode");
                elem.InnerText = "0";

                root.InsertBefore(elem, root.FirstChild);

                Response.Write(root.OuterXml);
            }
            else if (actionreq.Equals("GetMostVisited"))
            {
                GameFacade gameFacade = new GameFacade();
                int totalCount = 0;
                DataTable dtCommunities = gameFacade.Top3Dapps(false, false, 1, limit, ref totalCount, KanevaGlobals.WokGameId);


                DataTable dtTemp = new DataTable();

                dtTemp.Columns.Add(new DataColumn("game_id", System.Type.GetType("System.String")));
                dtTemp.Columns.Add(new DataColumn("name", System.Type.GetType("System.String")));


                if (dtCommunities.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtCommunities.Rows)
                    {
                        DataRow newRow = dtTemp.NewRow();
                        //newRow.ItemArray = dr.ItemArray;
                        newRow["game_id"] = dr ["game_id"].ToString();
                        newRow["name"] = dr ["name"].ToString();
                        dtTemp.Rows.Add (newRow);
                    }
                }


                DataSet ds = new DataSet();

                ds.DataSetName = "Most Visited Apps";
                dtTemp.TableName = "Apps";
                ds.Tables.Add(dtTemp);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(ds.GetXml());
                XmlNode root = doc.DocumentElement;

                XmlElement elem = doc.CreateElement("ReturnDescription");
                elem.InnerText = "success";

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("ReturnCode");
                elem.InnerText = "0";

                root.InsertBefore(elem, root.FirstChild);

                Response.Write(root.OuterXml);
            }
            else if (actionreq.Equals ("GetByCategory"))
            {
                string category = "";

                if (Request.Params["categoryname"] != null)
                {
                    category = Request.Params["categoryname"];
                }

                GameFacade gameFacade = new GameFacade ();
                int totalCount = 0;
                DataTable dtGames = gameFacade.GetWorldsByCategory(false, false, ref totalCount, 1, limit, KanevaGlobals.WokGameId, category);

                DataTable dtTemp = new DataTable ();

                dtTemp.Columns.Add (new DataColumn ("game_id", System.Type.GetType ("System.String")));
                dtTemp.Columns.Add (new DataColumn ("name", System.Type.GetType ("System.String")));

                if (dtGames.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtGames.Rows)
                    {
                        DataRow newRow = dtTemp.NewRow ();
                        //newRow.ItemArray = dr.ItemArray;
                        newRow["game_id"] = dr["game_id"].ToString ();
                        newRow["name"] = dr["name"].ToString ();
                        dtTemp.Rows.Add (newRow);
                    }
                }

                DataSet ds = new DataSet ();

                ds.DataSetName = "Apps By Category";
                dtTemp.TableName = "Apps";
                ds.Tables.Add (dtTemp);

                XmlDocument doc = new XmlDocument ();
                doc.LoadXml (ds.GetXml ());
                XmlNode root = doc.DocumentElement;

                XmlAttribute catAttr = doc.CreateAttribute ("categoryname");
                catAttr.Value = category;
                root.Attributes.Append (catAttr);

                XmlElement elem = doc.CreateElement ("ReturnDescription");
                elem.InnerText = "success";

                root.InsertBefore (elem, root.FirstChild);

                elem = doc.CreateElement ("ReturnCode");
                elem.InnerText = "0";

                root.InsertBefore (elem, root.FirstChild);

                Response.Write (root.OuterXml);
            }
        }
    }
}