///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;

using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for dynamicObject.
	/// </summary>
	public class dynamicObject : KgpBasePage
	{
        const Int32 ACCESS_PASS_ID = 1;

		private void Page_Load(object sender, System.EventArgs e)
		{
            bool inGame = false;
            bool inAccessPassZone = false;

            if (CheckUserId(true, true, ref inGame, ref inAccessPassZone))
            {
                if (Request.Params["placementId"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>placementId not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                int placementId = Int32.Parse(Request.Params["placementId"].ToString());

                if (Request.Params["type"] != null && Request.Params["type"] == "g") // called from game menu
                {
                    if (inGame)
                    {
                        // unfortunately have hit dyn obj table twice, once to get texture id
                        // then again with outer join to get details about do and asset
                        PagedDataTable pdt;
                        Hashtable parameters = new Hashtable();

                        string selectList = " dop.VALUE AS texture_asset_id ";
                        string tableList = " wok.dynamic_objects do INNER JOIN wok.players p ON do.player_id = p.player_id LEFT OUTER JOIN wok.dynamic_object_parameters dop ON dop.obj_placement_id = do.obj_placement_id INNER JOIN wok.parameters param ON dop.param_type_id = param.param_type_id ";
                        string whereClause = " do.obj_placement_id = @placementId AND param.NAME = 'texture_asset_id' ";
                        string orderbyclause = " dop.VALUE ";

                        parameters.Add("@placementId", placementId);
                        pdt = KanevaGlobals.GetDatabaseUtility().GetPagedDataTable(selectList, tableList, whereClause, orderbyclause, parameters, 0, 1);

                        if (pdt != null && pdt.Rows.Count > 0)
                        {
                            PagedDataTable pdt2;
                            Hashtable parameters2 = new Hashtable();

                            selectList  = " (SELECT value FROM wok.dynamic_object_parameters dop WHERE dop.obj_placement_id = @placementId AND param_type_id = 17) AS swf_name, ";
                            selectList += " (SELECT value FROM wok.dynamic_object_parameters dop WHERE dop.obj_placement_id = @placementId AND param_type_id = 18) AS swf_parameter, ";
                            selectList += " a.asset_id AS texture_asset_id, a.thumbnail_large_path, a.name, a.instructions, a.company_name, u.username, ";
                            selectList += " CONCAT(\"" + Configuration.StreamingServer + "/\", a.media_path) AS media_path, ";
                            selectList += " IFNULL(ad.digg_id, 0) AS digg_id, a.mature ";
                            tableList = " kaneva.assets a JOIN kaneva.users u ON a.owner_id = u.user_id LEFT OUTER JOIN kaneva.asset_diggs ad ON ad.asset_id = a.asset_id AND ad.user_id = @userId ";
                            whereClause = " a.asset_id = (SELECT value FROM wok.dynamic_object_parameters dop WHERE dop.obj_placement_id = @placementId AND param_type_id = 14) ";
                            orderbyclause = " a.asset_id ";

                            parameters2.Add("@assetId", Convert.ToInt32(pdt.Rows[0]["texture_asset_id"]));
                            parameters2.Add("@userId", m_userId);
                            parameters2.Add("@placementId", placementId);
                            pdt2 = KanevaGlobals.GetDatabaseUtility().GetPagedDataTable(selectList, tableList, whereClause, orderbyclause, parameters2, 0, 1);

                            if (pdt2 != null && pdt2.Rows.Count > 0)
                            {
                                if (Request.Params["type"] != null && Request.Params["rave"] == "1")
                                {
                                    StoreUtility.InsertDigg(Convert.ToInt32(m_userId), Convert.ToInt32(pdt.Rows[0]["texture_asset_id"]));
                                }

                                if (!inAccessPassZone && !pdt2.Rows[0]["mature"].Equals(DBNull.Value) && Convert.ToString(pdt2.Rows[0]["mature"]) == "Y")
                                {
                                    pdt2.Rows[0]["media_path"] = Configuration.NonAPSwfGameReplacement;
                                }
                                returnResult(pdt2);
                            }
                            else
                            {
                                string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>asset not found</ResultDescription>\r\n</Result>";
                                Response.Write(errorStr);
                                return;
                            }
                        }
                        else
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>placementId not found</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>not in game</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
                else
                {
                    PagedDataTable pdt;
                    Hashtable parameters = new Hashtable();

                    // get the details about the dynamic object from the items (global inventory) table 
                    string selectList = " i.global_id, i.market_cost, floor((i.market_cost + ifnull(iw.designer_price, 0) + ifnull(iw2.designer_price, 0))*IF(i.base_global_id>0, 1.1, 1)) as price ";
                    string tableList = " wok.items i" +
                                       " LEFT JOIN shopping.items_web iw ON i.global_id=iw.global_id" +
                                       " LEFT JOIN shopping.items_web iw2 ON i.base_global_id=iw2.global_id" +
                                       " LEFT JOIN wok.dynamic_objects d on i.global_id = d.global_id ";
                    string whereClause = " d.obj_placement_id = @placementId ";
                    string orderbyclause = "";

                    parameters.Add("@placementId", placementId);

                    pdt = KanevaGlobals.GetDatabaseUtility().GetPagedDataTable(selectList, tableList, whereClause, orderbyclause, parameters, 0, 1);

                    if (pdt != null)
                    {
                        returnResult(pdt);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>placementId not found</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
            }
		}

        private void returnResult(PagedDataTable pdt)
        {
            DataSet ds = new DataSet();

            ds.DataSetName = "Result";

            pdt.TableName = "DynamicObject";
            ds.Tables.Add(pdt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            int numRecords = pdt.Rows.Count;
            XmlElement elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = pdt.TotalCount;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
