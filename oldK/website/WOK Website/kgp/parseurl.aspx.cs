///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.framework.utils;

/*
Request

kgp/parseurl.aspx?requrl=(youtubeurl/flikerurl/tvurl/...)&reqid=requestid

Response

<ReturnCode>0=unknown, 1=video, 2=picture, 3=sound, ...</ReturnCode>
<RequestId>reqId</RequestId>
<ReturnUrl>usable url for playback</ReturnUrl>
*/

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class parseurl : KgpBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["requrl"] != null && Request.Params["reqid"] != null)
            {
                string returnCode = "0";
                string requestUrl = Request.Params["requrl"];
                string requestId = Request.Params["reqid"];
                string providerStr = null;

                int urlType = ExternalURLHelper.ClassifyURL(requestUrl, ref providerStr);
                if (urlType != (int)ExternalURLHelper.UrlType.URLTYPE_UNKNOWN)
                    returnCode = "1";

                Response.Write("<ReturnCode>" + returnCode + "</ReturnCode>");
                Response.Write("<RequestUrl>" + requestUrl + "</RequestUrl>");
                Response.Write("<RequestId>" + requestId + "</RequestId>");
                Response.Write("<UrlType>" + urlType + "</UrlType>");
                if (providerStr == null)
                    return;

                Response.Write("<UrlProvider>" + providerStr + "</UrlProvider>");

                switch( urlType )
                {
                    case (int)ExternalURLHelper.UrlType.URLTYPE_VIDEO:
                    {
                        if (providerStr == ExternalURLHelper.PROVIDER_YOUTUBE)
                        {
                            string returnUrl, extAssetId;
                            if (ExternalURLHelper.ParseYoutubeUrl(requestUrl, out returnUrl, out extAssetId))
                            {
                                Response.Write("<ReturnUrl>" + returnUrl + "</ReturnUrl>");
                                Response.Write("<OffsiteAssetId>" + extAssetId + "</OffsiteAssetId>");
                            }
                        }
                    }
                    break;
                }
            }
        }
    }
}
