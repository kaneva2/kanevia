///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Linq;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// userExperiment inserts new experiment participant records and returns participant data for the logged in user
	/// </summary>
	public class userExperiment : KgpBasePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
                if (Request.Params["experimentId"] != null && Request.Params["groupId"] != null && Request.Params["assignmentDate"] != null )
                {   
                    // if called with an experiment id, group id and assignment date, create a new group record 
                    // returns an error: if a group record already exists
                    //                   if experiment id or group id is not defined
                    //                   if date is not formatted correctly
                    DateTime dateValue;
                    if (DateTime.TryParse(Request.Params["assignmentDate"], out dateValue))
                    {
                        bool status = GetExperimentFacade.SaveUserExperimentGroup(new UserExperimentGroup(string.Empty, m_userId, Request.Params["groupId"].ToString(), DateTime.Now, null, false, string.Empty));
                        if (!status)
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>error inserting experiment data into the DB</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                        else
                        {
                            string responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
                            Response.Write(responseStr);
                            return;
                        }
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>error parsing assignment date before insert</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
                else if (Request.Params["experimentId"] != null)
                {
                    // if called with experiment id parameter, return the group the user is in for that experiment
                    // returns an error: if the experiment id is not defined
                    UserExperimentGroup participantData = GetExperimentFacade.GetUserExperimentGroupByExperimentId(m_userId, Request.Params["experimentId"].ToString());

                    if (participantData != null)
                    {
                        if (Request.Params["convert"] != null)
                        {
                            if (participantData.ConversionDate == null)
                            {
                                participantData.ConversionDate = DateTime.Now;
                                GetExperimentFacade.SaveUserExperimentGroup(participantData);
                            }
                        }
                        else if (Request.Params["groupId"] != null && GetUserFacade.IsUserAutomatedTestAccount(KanevaWebGlobals.CurrentUser))
                        {
                            // Allow automated test users to switch their group assignment.
                            participantData.GroupId = Request.Params["groupId"].ToString();
                            GetExperimentFacade.SaveUserExperimentGroup(participantData);
                        }

                        XmlDocument doc = participantToXML(participantData);
                        XmlNode root = doc.DocumentElement;

                        XmlElement elem = doc.CreateElement("ReturnDescription");
                        elem.InnerText = "success";

                        root.InsertBefore(elem, root.FirstChild);

                        elem = doc.CreateElement("ReturnCode");
                        elem.InnerText = "0";

                        root.InsertBefore(elem, root.FirstChild);

                        Response.Write(root.OuterXml);
                        return;
                    }
                    else
                    {
                        // the user is not participating in the experiment
                        if (Request.Params["auto_assign"] == "Y")
                        {
                            // has it moved past the created state?
                            string experimentId = Request.Params["experimentId"].ToString();
                            Experiment experimentData = GetExperimentFacade.GetExperiment(experimentId);
                            if (experimentData != null && (experimentData.Status == EXPERIMENT_STATUS_TYPE.STARTED || experimentData.Status == EXPERIMENT_STATUS_TYPE.ENDED))
                            {
                                string assignedGroup = GetExperimentFacade.AssignParticipant(experimentData);
                                if (!string.IsNullOrWhiteSpace(assignedGroup))
                                {
                                    bool status = GetExperimentFacade.SaveUserExperimentGroup(new UserExperimentGroup(string.Empty, m_userId, assignedGroup, DateTime.Now, null, false, string.Empty));
                                    if (status)
                                    {
                                        participantData = GetExperimentFacade.GetUserExperimentGroupByExperimentId(m_userId, experimentId);
                                        if (participantData != null)
                                        {
                                            XmlDocument doc = participantToXML(participantData);
                                            XmlNode root = doc.DocumentElement;

                                            XmlElement elem = doc.CreateElement("ReturnDescription");
                                            elem.InnerText = "success";

                                            root.InsertBefore(elem, root.FirstChild);

                                            elem = doc.CreateElement("ReturnCode");
                                            elem.InnerText = "0";

                                            root.InsertBefore(elem, root.FirstChild);

                                            Response.Write(root.OuterXml);
                                            return;
                                        }
                                        else
                                        {
                                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>error inserting experiment data into the DB</ResultDescription>\r\n</Result>";
                                            Response.Write(errorStr);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>error inserting experiment data into the DB</ResultDescription>\r\n</Result>";
                                        Response.Write(errorStr);
                                        return;
                                    }
                                }
                                else
                                {
                                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>error determining group user should be in</ResultDescription>\r\n</Result>";
                                    Response.Write(errorStr);
                                    return;
                                }
                            }
                            else
                            {
                                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>error assigning user to a group because the experiment is not in a started or ended state</ResultDescription>\r\n</Result>";
                                Response.Write(errorStr);
                                return;
                            }
                        }
                        else
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>error finding participant in experiment</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    }
                }
                else
                {
                    UserFacade userFacade = new UserFacade();
                    if (Request.Params["auto_assign"] == "Y")
                    {
                        // assign a group to the user for every experiment that has started (or ended) but is not participating in
                        List<Experiment> experiments = GetExperimentFacade.GetAvailableExperimentsByUserContext(m_userId, Experiment.ASSIGN_ON_REQUEST).ToList();
                        
                        foreach ( Experiment experiment in experiments )
                        {
                            string assignedGroup = GetExperimentFacade.AssignParticipant(experiment);
                            if (!string.IsNullOrWhiteSpace(assignedGroup))
                            {
                                GetExperimentFacade.SaveUserExperimentGroup(new UserExperimentGroup(string.Empty, m_userId, assignedGroup, DateTime.Now, null, false, string.Empty));
                            }
                        }
                    }

                    // if called without experimentId, return a list of experiments the user is participating in
                    List<UserExperimentGroup> activeExperiments = GetExperimentFacade.GetActiveUserExperimentGroups(m_userId, false).ToList();
                    if (activeExperiments != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        
                        XmlNode root = doc.CreateElement("Result");
                        doc.AppendChild(root);

                        XmlElement elem = doc.CreateElement("ReturnCode");
                        elem.InnerText = "0";
                        root.AppendChild(elem);

                        elem = doc.CreateElement("ReturnDescription");
                        elem.InnerText = "success";
                        root.AppendChild(elem);

                        XmlDocument experimentsXml = participantsToXML(activeExperiments);
                        XmlNode experimentsRoot = doc.ImportNode(experimentsXml.DocumentElement, true);
                        root.AppendChild(experimentsRoot);
                        
                        Response.Write(doc.OuterXml);

                        return;
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>error finding experiments for user</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
            }
		}

        // returns an xml document that will be sent back to the caller containing all experiment participant data
        private XmlDocument participantToXML(UserExperimentGroup participant)
        {
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(participant.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);

            x.Serialize(writer, participant);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());

            return doc;
        }

        // returns an xml document that will be sent back to the caller containing all experiment participant data
        private XmlDocument participantsToXML(List<UserExperimentGroup> participants)
        {
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(participants.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);

            x.Serialize(writer, participants);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());

            return doc;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
