///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for reportAbuse.
	/// </summary>
	public class reportAbuse : KgpBasePage
	{
        //static private string REPORT_CATEGORY = "Kaneva Web Site";
        //static private string REPORT_SUMMARY  = "Kaneva WOK Feedback";

		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
   				if ( (Request.Params["subject"] == null) || (Request.Params["message"] == null) )
				{
					string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>subject or message not specified</ResultDescription>\r\n</Result>";
					Response.Write(errorStr);
					return;
				}

				string subject = Request.Params["subject"];
				string message = Request.Params["message"];

   				string strChecked = "";

				if ( subject.Equals("ReportBug") )
				{
					strChecked += "\n\tReport a bug";
				}
				else if ( subject.Equals("InappropriateContent") )
				{
					strChecked += "\n\tViolation of Inappropriate Content";
				}
				else if ( subject.Equals("CopyRightViolation") )
				{
					strChecked += "\n\tViolation of Copyright Content";
				}
				else if ( subject.Equals("Mature") )
				{
					strChecked += "\n\tPlace contains material that is racist, violent, or pornographic";
				}
                else if (subject.Equals("ReportAbuse"))
                {
                    strChecked += "\n\tReports user abuse";
                }
				else if ( subject.Equals("Other") )
				{
					strChecked += "\n\tOther issues";
				}
				else
				{
					string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>subject unknown</ResultDescription>\r\n</Result>";
					Response.Write(errorStr);
					return;
				}

				string username = UsersUtility.GetUserNameFromId(m_userId);
				string emailaddress = UsersUtility.GetUserEmailFromId(m_userId);

				string strComments = "User comment = '" + message + "'\n" +
					"Items checked = " + strChecked + "\n" +
					"Submitted by username = '" + username + "'\n" +
					"World of Kaneva Version = '" + KanevaGlobals.Version () + "'";

				strComments += "\nEmail = " + emailaddress;

                // Replace Mantis with Parature
				//int issueId = StoreUtility.AddMantisIssue( REPORT_CATEGORY, REPORT_SUMMARY, strComments );

                try
                {
                    string paratureURL = Configuration.ParatureAPIURL;
                    string paratureToken = Configuration.ParatureAPIToken;
                    string paratureCustomerId = "";

                    WebClient client = new WebClient();
                    client.Encoding = System.Text.Encoding.UTF8;
                    string response = "";
                    string xmlRequest = "";
                    System.Text.UTF8Encoding objUTF8 = new System.Text.UTF8Encoding();
                    byte[] postByteArray;
                    byte[] responseArray;

                    // Look up the user in parataure
                    responseArray = client.DownloadData(paratureURL + "Customer?email=" + KanevaWebGlobals.CurrentUser.Email + "&" + paratureToken);
                    response = objUTF8.GetString(responseArray).Trim();

                    XmlDocument xmlResponse = new XmlDocument();
                    xmlResponse.LoadXml(response);

                    XmlNodeList enlCustomer = xmlResponse.GetElementsByTagName("Customer");

                    if (enlCustomer.Count > 0)
                    {
                        XmlAttribute attId = enlCustomer[0].Attributes["id"];
                        paratureCustomerId = attId.Value;
                    }

                    // Did we find an existing user?
                    if (paratureCustomerId.Length.Equals(0))
                    {
                        // Create the user in parature
                        xmlRequest = "<Customer>" +
                            "<Email>" + KanevaWebGlobals.CurrentUser.Email + "</Email>" +
                            "<First_Name>" + KanevaWebGlobals.CurrentUser.FirstName + "</First_Name>" +
                            "<Last_Name>" + KanevaWebGlobals.CurrentUser.LastName + "</Last_Name>" +
                            "<Password>password</Password><Password_Confirm>password</Password_Confirm>" +
                            "<Sla> <Sla id=\"4857\"/> </Sla>" +
                            "<Status><Status id=\"2\"/></Status>" +
                            "<User_Name>" + KanevaWebGlobals.CurrentUser.Username + "</User_Name>" +
                            "</Customer>";

                        postByteArray = System.Text.Encoding.ASCII.GetBytes(xmlRequest);
                        responseArray = client.UploadData(paratureURL + "Customer?" + paratureToken, "POST", postByteArray);
                        response = objUTF8.GetString(responseArray).Trim();

                        xmlResponse = new XmlDocument();
                        xmlResponse.LoadXml(response);

                        enlCustomer = xmlResponse.GetElementsByTagName("Customer");

                        if (enlCustomer.Count > 0)
                        {
                            XmlAttribute attId = enlCustomer[0].Attributes["id"];
                            paratureCustomerId = attId.Value;
                        }
                    }

                    // Create the Ticket
                    xmlRequest = "<Ticket>" +
                            "<Ticket_Customer>" +
                                "<Customer id=\"" + paratureCustomerId + "\"></Customer>" +
                            "</Ticket_Customer>" +
                            "<Custom_Field id=\"98090\"> <Option id=\"200726\" selected=\"true\" /> </Custom_Field>" +
                            "<Custom_Field id=\"98091\"> <Option id=\"200731\" selected=\"true\" /> </Custom_Field>" +
                            "<Custom_Field id=\"98099\">" + KanevaGlobals.CleanParatureText("Report Abuse from WOK") + "</Custom_Field>" +
                            "<Custom_Field id=\"98100\">" + KanevaGlobals.CleanParatureText(strComments) + "</Custom_Field>" +
                        "</Ticket>";

                    postByteArray = System.Text.Encoding.ASCII.GetBytes(xmlRequest);
                    responseArray = client.UploadData(paratureURL + "Ticket?" + paratureToken, "POST", postByteArray);
                    response = objUTF8.GetString(responseArray);
                }
                catch (Exception)
                {
                    string error = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Error communicating to Parature</ResultDescription>\r\n</Result>";
                    Response.Write(error);
                }

				string responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
				Response.Write(responseStr);


			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
