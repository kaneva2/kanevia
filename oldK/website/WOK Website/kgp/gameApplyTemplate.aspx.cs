///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.IO;
using log4net;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class gameApplyTemplate : KgpBasePage
    {
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        const string SUCCESS            = "0";
        const string BAD_PARAMETERS     = "1";
        const string USER_NOT_VALIDATED = "2";
        const string USER_NOT_OWNER     = "3";

        const string NODE_ACHIEVEMENT = "achievement";
        const string NODE_PREMIUMITEM = "premiumitem";
        const string NODE_LEADERBOARD = "leaderboard";

        const int MAX_TEMPLATE_ITEM_COUNT = 100;    // Set a maximum item count to prevent hacking
        const int MAX_TEMPLATE_XML_LENGTH = 4096;

        private void Page_Load(object sender, System.EventArgs e)
        {
            string userEmail = Request["user"];
            string passWord  = Request["pw"];
            int gameId     = -1;
            int templateId = -1;

            try
            {
                if (Request["gameId"] != null)
                {
                    gameId = Convert.ToInt32(Request["gameId"]);
                }

                if (Request["templateId"] != null)
                {
                  templateId = Convert.ToInt32(Request["templateId"]);
                }
            }
            catch (Exception) { }

            if (userEmail == null || userEmail == ""  || passWord  == null || passWord  == ""  ||
                gameId == -1      || templateId == -1 || Request.Files.Count != 1)
            {
                string errorString = "<Result><ReturnCode>" + BAD_PARAMETERS + "</ReturnCode><ReturnDescription>Error parsing request parameters</ReturnDescription></Result>";
                Response.Write(errorString);
                return;
            }

            HttpPostedFile xmlFile = Request.Files[0]; 
            string templateConfig = "";
            if( xmlFile.ContentLength<=MAX_TEMPLATE_XML_LENGTH )
            {
                byte[] transferBuffer = new byte[xmlFile.ContentLength];
                int bytesRead = xmlFile.InputStream.Read(transferBuffer, 0, transferBuffer.Length);
                if (bytesRead > 0)
                {
                    templateConfig = System.Text.Encoding.Default.GetString(transferBuffer);
                }
            }

            int roleMemberShip = 0;
            if (!ValidateUser(true, userEmail, passWord, gameId, ref roleMemberShip))
            {
                string errorString = "<Result><ReturnCode>" + USER_NOT_VALIDATED + "</ReturnCode><ReturnDescription>The user is not validated</ReturnDescription></Result>";
                Response.Write(errorString);
                m_logger.Error("User with email " + userEmail + " is not validated");
                return;
            }

            int userId = GetUserFacade.GetUserByEmail(userEmail).UserId;
            int ownerId = GetGameFacade.GetGameOwner(gameId);
            // only owners of the 3d app or incubator owner can apply templates
            if (!(ownerId.Equals(userId) || userEmail!=null && userEmail!="" && userEmail==KanevaGlobals.IncubationOwner))
            {
                string errorString = "<Result><ReturnCode>" + USER_NOT_OWNER + "</ReturnCode><ReturnDescription>The user is not the owner of this 3D App</ReturnDescription></Result>";
                Response.Write(errorString);
                m_logger.Error("User " + userId + " is not the owner of the 3D App " + gameId);
                return;
            }

            if (gameId != -1 && templateId != -1)
            {
                int communityId = GetCommunityFacade.GetCommunityIdFromGameId(gameId);

                if (communityId > 0)
                {
                    GetGameFacade.InsertWorldsToTemplate(communityId, templateId);
                }
            }



            int itemCount = 0;
            int leaderBoardCount = 0;   //Should be <=1
            XmlDocument docInput = new XmlDocument();
            Hashtable outAchievements = new Hashtable();
            Hashtable outPremiumItems = new Hashtable();
            bool resLeaderBoardSetup = false;

            try
            {
                docInput.LoadXml(templateConfig);
                XmlNode templateRoot = docInput.DocumentElement;

                for (XmlNode node = templateRoot.FirstChild; node != null && ++itemCount < MAX_TEMPLATE_ITEM_COUNT; node = node.NextSibling)
                {
                    if (node.Name == NODE_ACHIEVEMENT)
                    {
                        //Process achievement
                        ProcessAchievementNode(userId, gameId, node, ref outAchievements);
                    }
                    else if (node.Name == NODE_PREMIUMITEM)
                    {
                        //Process premium item
                        ProcessPremiumItemNode(userId, gameId, node, ref outPremiumItems);
                    }
                    else if (node.Name == NODE_LEADERBOARD)
                    {
                        leaderBoardCount++;
                        if (leaderBoardCount <= 1)
                        {
                            //Process leader board
                            resLeaderBoardSetup = ProcessLeaderBoardNode(userId, gameId, node);
                        }
                    }
                    else
                    {
                        //Unknown entry
                        m_logger.Warn(String.Format("Unknown template config item: {0}", node.Name));
                    }
                }
            }
            catch (XmlException)
            {
                //Malformed XML, hack possible 
                string errorString = "<Result><ReturnCode>" + BAD_PARAMETERS + "</ReturnCode><ReturnDescription>Invalid parameter</ReturnDescription></Result>";
                Response.Write(errorString);
                m_logger.Error(String.Format("Malformed input XML: user {0} from IP {1}, {2}", userId, Common.GetVisitorIPAddress(), templateConfig));
                return;
            }

            if (itemCount > MAX_TEMPLATE_ITEM_COUNT)
            {
                m_logger.Error(String.Format("Too many items in template config: user {0} from IP {1}, {2}", userId, Common.GetVisitorIPAddress(), templateConfig));
            }
            if (leaderBoardCount > 1)
            {
                m_logger.Error(String.Format("More than one leaderboard item: user {0} from IP {1}, {2}", userId, Common.GetVisitorIPAddress(), templateConfig));
            }

            //Generate output XML
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<Result><ReturnCode>0</ReturnCode><ReturnDescription>success</ReturnDescription></Result>");
            XmlNode root = doc.DocumentElement;

            foreach (string key in outAchievements.Keys)
            {
                XmlElement elem = doc.CreateElement("achievement");

                XmlAttribute attr = doc.CreateAttribute("name");
                attr.Value = key;
                elem.Attributes.Append(attr);

                elem.InnerText = outAchievements[key].ToString();

                root.AppendChild(elem);
            }

            foreach (string key in outPremiumItems.Keys)
            {
                XmlElement elem = doc.CreateElement("premiumitem");

                XmlAttribute attr = doc.CreateAttribute("name");
                attr.Value = key;
                elem.Attributes.Append(attr);

                elem.InnerText = outPremiumItems[key].ToString();

                root.AppendChild(elem);
            }

            {
                XmlElement elem = doc.CreateElement("leaderboard");
                elem.InnerText = resLeaderBoardSetup ? "1" : "0";
                root.AppendChild(elem);
            }

            Response.Write(root.OuterXml);
        }

        private void ProcessAchievementNode(int userId, int gameId, XmlNode node, ref Hashtable outAchievements)
        {
            XmlAttribute attrName = node.Attributes["name"];
            XmlAttribute attrDesc = node.Attributes["desc"];
            XmlAttribute attrPoints = node.Attributes["points"];
            XmlAttribute attrAssetId = node.Attributes["assetId"];

            if (attrName == null || attrName.Value == null || 
                attrDesc == null || attrDesc.Value==null || 
                attrPoints == null || attrPoints.Value==null || attrPoints.Value=="")
            {
                throw new ArgumentException("Invalid achievement node", "achievement");
            }

            if (outAchievements.ContainsKey(attrName))
            {
                throw new ArgumentException("Duplicated achievement node", "achievement");
            }

            UInt32 points = 0;
            try
            {
                points = Convert.ToUInt32(attrPoints.Value);
            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid points parameter for achievement", "achievement.points");
            }

            int imageAssetId = 0;
            try
            {
                if (attrAssetId != null && attrAssetId.Value != null && attrAssetId.Value != "")
                {
                    imageAssetId = Convert.ToInt32(attrAssetId.Value);
                }
            }
            catch (Exception) { }

            uint badgeId = AddGameAchievement(userId, gameId, attrName.Value, attrDesc.Value, points, imageAssetId);
            outAchievements.Add(attrName.Value, badgeId);
        }

        private void ProcessPremiumItemNode(int userId, int gameId, XmlNode node, ref Hashtable outPremiumItems)
        {
            XmlAttribute attrName = node.Attributes["name"];
            XmlAttribute attrDesc = node.Attributes["desc"];
            XmlAttribute attrPrice = node.Attributes["price"];
            XmlAttribute attrAssetId = node.Attributes["assetId"];

            if (attrName == null || attrName.Value == null ||
                attrDesc == null || attrDesc.Value == null ||
                attrPrice == null || attrPrice.Value == null || attrPrice.Value == "")
            {
                throw new ArgumentException("Invalid premiumitem node", "premiumitem");
            }

            if (outPremiumItems.ContainsKey(attrName))
            {
                throw new ArgumentException("Duplicated premiumitem node", "premiumitem");
            }

            UInt32 price = 0;
            try
            {
                price = Convert.ToUInt32(attrPrice.Value);
            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid price parameter for premium item", "premiumitem.price");
            }

            int imageAssetId = 0;
            try
            {
                if (attrAssetId != null && attrAssetId.Value != null && attrAssetId.Value != "")
                {
                    imageAssetId = Convert.ToInt32(attrAssetId.Value);
                }
            }
            catch (Exception) { }

            int globalId = AddGamePremiumItem(userId, gameId, attrName.Value, attrDesc.Value, price, imageAssetId);
            outPremiumItems.Add(attrName.Value, globalId);
        }

        private bool ProcessLeaderBoardNode(int userId, int gameId, XmlNode node)
        {
            XmlAttribute attrName = node.Attributes["name"];
            XmlAttribute attrLabel = node.Attributes["label"];
            XmlAttribute attrType = node.Attributes["type"];
            XmlAttribute attrSort = node.Attributes["sort"];

            if (attrName == null || attrName.Value == null ||
                attrLabel == null || attrLabel.Value == null ||
                attrType == null || attrType.Value == null || attrType.Value == "" ||
                attrSort == null || attrSort.Value == null) 
            {
                throw new ArgumentException("Invalid leaderboard node", "leaderboard");
            }

            int type = 0;
            try
            {
                type = Convert.ToInt32(attrType.Value);
            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid type parameter for leaderboard", "leaderboard.type");
            }

            return SetupGameLeaderBoard(userId, gameId, attrName.Value, attrLabel.Value, type, attrSort.Value);
        }
    }
}
