///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;
using log4net;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for Metrics.
    /// </summary>
    public class Metrics : KgpBasePage    {

        int? TryParseInt(string s)       {
            int i;
            if (!int.TryParse(s, out i)) return null;
            return i;
        }

        double? TryParseDouble(string s)
        {
            double d;
            if (!double.TryParse(s, out d)) return null;
            return d;
        }


        private void Page_Load(object sender, System.EventArgs e)        {
            try { m_logger.Info ("BEGIN - Metrics.PageLoad()"); }
            catch { }

            Response.ContentType = "text/xml;charset=UTF-8";

            string  sVersion    = Request.Params["version"];
            int?    nVersion    = 0;                                // 
            if (sVersion != null)                          nVersion = TryParseInt(sVersion);
            


            switch (nVersion)            {
                case 0:     loadVersion0(sender, e); break;
                case 1:     loadVersion1(sender, e); break;
                default: 
                    string errorStr = "<Result>\r\n  <ReturnCode>-2</ReturnCode>\r\n  <ResultDescription>unsupported version</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
            };
        }


        private void loadVersion0(object sender, System.EventArgs e)
        {
            try { m_logger.Info("BEGIN - Metrics.PageLoad()"); }
            catch { }

            Response.ContentType = "text/xml;charset=UTF-8";

            string action = Request.Params["action"];
            if (action == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            if (action.Equals("report"))
            {
                string counterName = Request.Params["countername"];
                if (counterName == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>countername not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

// v0 is being obsolesced.  disable call to stop all traffic on v0 until wok deployment.
//                MetricsFacade metricsFacade = new MetricsFacade();
//                metricsFacade.InsertMetricsCounterData_v0(  counterName
//                                                            , TryParseDouble(Request.Params["a1"])
//                                                            , TryParseDouble(Request.Params["a2"])
//                                                            , TryParseDouble(Request.Params["b1"])
//                                                            , TryParseDouble(Request.Params["b2"])
//                                                        );
                string retCode = "0";
                string retDesc = "Success";

                string returnStr = "<Result>"
                                        + "<ReturnCode>" + retCode + "</ReturnCode>"
                                        + "<ResultDescription>" + retDesc + "</ResultDescription>"
                                    + "</Result>";
                Response.Write(returnStr);
            }
            else
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid action specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            m_logger.Debug("END - Metrics.PageLoad()");
        }

        private void loadVersion1(object sender, System.EventArgs e)
        {
            try { m_logger.Info("BEGIN - Metrics.PageLoad()"); }
            catch { }

            Response.ContentType = "text/xml;charset=UTF-8";

            string action = Request.Params["action"];
            if (action == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }


            if (action.Equals("report"))
            {
                string counterName = Request.Params["countername"];
                if (counterName == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>countername not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }


                MetricsFacade metricsFacade = new MetricsFacade();

                metricsFacade.InsertMetricsCounterData_v1(  counterName
                                                            , Request.Params["k1"]
                                                            , Request.Params["k2"]
                                                            , Request.Params["k3"]
                                                            , Request.Params["k4"]
                                                            , TryParseDouble(Request.Params["a1"])
                                                            , TryParseDouble(Request.Params["a2"])
                                                            , TryParseDouble(Request.Params["b1"])
                                                            , TryParseDouble(Request.Params["b2"])
                                                        );
                string retCode      = "0";
                string retDesc      = "Success";

                string returnStr    = "<Result>"
                                        + "<ReturnCode>" + retCode + "</ReturnCode>"
                                        + "<ResultDescription>" + retDesc + "</ResultDescription>"
                                    + "</Result>";
                Response.Write(returnStr);
            }
            else
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid action specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            m_logger.Debug("END - Metrics.PageLoad()");
        }



        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
