///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Web;
using System.Web.UI;
using System.Data;
using log4net;
using System.Linq;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class TopWorldsTour : KgpBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["action"] == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            string actionreq = Request.Params["action"];
            GameFacade gameFadace = new GameFacade ();

            if (actionreq.Equals("GetTourWorld"))
            {

                if ((Request.Params["start"] == null) || (Request.Params["max"] == null || (Request.Params["username"] == null)))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start, max or username not specified</ResultDescription>\r\n\r\n  </Result>";
                    Response.Write(errorStr);
                    return;
                }

                int page = Int32.Parse(Request.Params["start"].ToString());
                int items_per_page = Int32.Parse(Request.Params["max"].ToString());
                string username =Request.Params["username"].ToString();

                int userId = GetUserFacade.GetUserIdFromUsername(username);
                if (userId == 0)
                {
                    Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid username</ResultDescription>\r\n</Result>");
                    return;
                }

                int totalNumRecords = 0;

                GameFacade gameFacade = new GameFacade();

                DataTable dtGames = gameFacade.TopWorldsTour(userId, false, false, ref totalNumRecords, page, items_per_page, KanevaGlobals.WokGameId, Configuration.TourRewardsLooter);
                Response.Write(BuildXMLResponse(dtGames, "Result", "3DApps", totalNumRecords, username));
            }
            else
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invalid action</ResultDescription>\r\n</Result>");
            }
        }

        /// <summary>
        /// BuildXMLResponse
        /// </summary>
        private string BuildXMLResponse(DataTable pdt, string dataSetName, string tableName, int totalNumRecords, string username)
        {
            //DataSet ds = new DataSet();
            DataSet ds = pdt.DataSet;
            ds.DataSetName = dataSetName;

            pdt.TableName = tableName;
            // ds.Tables.Add(pdt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("Username");
            elem.InnerText = username;

            root.InsertBefore(elem, root.FirstChild);

            int numRecords = pdt.Rows.Count;
            elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            InsertPassthroughTag(ref doc);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            return root.OuterXml;
        }

    }
}