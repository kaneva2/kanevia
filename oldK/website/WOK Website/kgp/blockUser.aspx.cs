///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for texturesList.
	/// </summary>
	public class blockUser : KgpBasePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
				int page = 0;
				int items_per_page = 0;
				int blockedUserId = 0;
				string responseStr = null;

				if ( Request.Params["action"] == null)
				{
					string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
					Response.Write(errorStr);
					return;
				}

				string actionreq = Request.Params["action"];

                // Temporary tweak in case we move without KIM updates
                string comm = (Request.Params["comm"] ?? string.Empty);
                string follow = (Request.Params["follow"] ?? string.Empty);

                bool newKimBlock = (actionreq.Equals("Block") && string.IsNullOrWhiteSpace(comm) && string.IsNullOrWhiteSpace(follow));
                bool oldKimBlock = (actionreq.Equals("Block") && comm.Equals("1") && follow.Equals("1"));
                bool oldKimUnblock = (actionreq.Equals("Block") && comm.Equals("0") && follow.Equals("0"));


                if (actionreq.Equals ("List"))      
				{
       				if (Request.Params["start"] == null)
                    {
                        page = 1;
                    }
                    else
                    {
                        page = Int32.Parse(Request.Params["start"].ToString());
                    }

                    if (Request.Params["max"] == null)
                    {
                        items_per_page = Int32.MaxValue;
                    }
                    else
                    {
                        items_per_page = Int32.Parse(Request.Params["max"].ToString());
                    }
       				
					DataSet ds = new DataSet();

					ds.DataSetName = "Result";

                    global::Kaneva.DataLayer.DataObjects.PagedDataTable pdt;
                                       
                    pdt = GetUserFacade.GetBlockedUsers (m_userId, "username", page, items_per_page);

					pdt.TableName = "Block";
					ds.Tables.Add(pdt);

					XmlDocument doc = new XmlDocument();
					doc.LoadXml(ds.GetXml());
					XmlNode root = doc.DocumentElement;

					int numRecords = pdt.Rows.Count;
					XmlElement elem = doc.CreateElement("NumberRecords");
					elem.InnerText= numRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					uint totalNumRecords = pdt.TotalCount; 
					elem = doc.CreateElement("TotalNumberRecords");
					elem.InnerText= totalNumRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnDescription");
					elem.InnerText="success";

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnCode");
					elem.InnerText="0";

					root.InsertBefore( elem, root.FirstChild);

					Response.Write(root.OuterXml);
				}
                else if (actionreq.Equals ("ListCommBlocks"))    
                {   
                    string sql = "";
                    Hashtable parameters = null;

                    sql = "SELECT distinct u.username as Name FROM kaneva.blocked_users bu " +
                            "INNER JOIN kaneva.users u ON bu.blocked_user_id=u.user_id " +
                            "WHERE bu.user_id=@userId"; 

                    parameters = new Hashtable();
                    parameters.Add("@userId", m_userId);
                    DataTable dt = KanevaGlobals.GetDatabaseUtility().GetDataTable(sql, parameters);

                    DataSet ds = new DataSet("result");
                    if (dt!=null)
                    {
                        dt.TableName = "Record";
                        ds.Tables.Add(dt);
                    }

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(ds.GetXml());
                    XmlNode root = doc.DocumentElement;

                    XmlElement elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = "success";
                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = "0";
                    root.InsertBefore(elem, root.FirstChild);

                    Response.Write(root.OuterXml);
                }
                else if (actionreq.Equals ("GetUserBlockData"))     
                {
                    bool commBlocked = false;
                    bool followBlocked = false;
                    bool blockedFromCurrentLocation = false;
                    string reasonDescription = null;
                    int blockingUserId = 0;

                    if (Request.Params["userId"] == null && Request.Params["username"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>user id not specified for action</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    if (Request.Params["userId"] != null)
                    {
                        blockedUserId = Int32.Parse(Request.Params["userId"].ToString());
                    }
                    else
                    {
                        blockedUserId = UsersUtility.GetUserIdFromUsername(Convert.ToString(Request.Params["username"]));
                    }

                    // If the blocking user is specified, use it. If not, use the logged-in user
                    if (Request.Params["blockingUserId"] != null)
                    {
                        blockingUserId = Int32.Parse(Request.Params["blockingUserId"].ToString());
                    }
                    else
                    {
                        blockingUserId = m_userId;
                    }

                    if (blockedUserId == 0)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid user id or user name</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    DataSet ds = new DataSet();
                    ds.DataSetName = "Result";

                    int zoneAccess = 0;
                    int userCurrentZone = 0;
                    bool isGM = false;

                    DataRow dr = UsersUtility.GetBlockedUser(blockingUserId, blockedUserId, ref commBlocked, ref followBlocked, ref zoneAccess, ref isGM, ref blockedFromCurrentLocation, ref userCurrentZone, ref reasonDescription);

                    if (dr != null)
                    {
                        DataTable dt = dr.Table;

                        dt.TableName = "User";

                        ds.Tables.Add (dt);

                        XmlDocument doc = new XmlDocument ();
                        doc.LoadXml (ds.GetXml ());
                        XmlNode root = doc.DocumentElement;

                        XmlElement elem = doc.CreateElement ("Owner");

                        if (zoneAccess == (int) CommunityMember.CommunityMemberAccountType.OWNER)
                        {
                            elem.InnerText = "1";
                        }
                        else
                        {
                            elem.InnerText = "0";
                        }

                        root.InsertBefore (elem, root.FirstChild);

                        elem = doc.CreateElement ("Moderator");

                        if (zoneAccess == (int) CommunityMember.CommunityMemberAccountType.MODERATOR)
                        {
                            elem.InnerText = "1";
                        }
                        else
                        {
                            elem.InnerText = "0";
                        }

                        root.InsertBefore (elem, root.FirstChild);

                        elem = doc.CreateElement ("GM");

                        if (isGM)
                        {
                            elem.InnerText = "1";
                        }
                        else
                        {
                            elem.InnerText = "0";
                        }

                        root.InsertBefore (elem, root.FirstChild);

                        int numRecords = 1;
                        elem = doc.CreateElement ("NumberRecords");
                        elem.InnerText = numRecords.ToString ();

                        root.InsertBefore (elem, root.FirstChild);

                        int totalNumRecords = 1;
                        elem = doc.CreateElement ("TotalNumberRecords");
                        elem.InnerText = totalNumRecords.ToString ();

                        root.InsertBefore (elem, root.FirstChild);

                        elem = doc.CreateElement ("ReturnDescription");
                        elem.InnerText = "success";

                        root.InsertBefore (elem, root.FirstChild);

                        elem = doc.CreateElement ("ReturnCode");
                        elem.InnerText = "0";

                        root.InsertBefore (elem, root.FirstChild);

                        Response.Write (root.OuterXml);
                    }
                    else
                    {
                        XmlDocument doc = new XmlDocument ();
                        XmlNode root = doc.CreateNode (XmlNodeType.Element, "Result", "");

                        XmlElement elem = doc.CreateElement ("Owner");

                        if (zoneAccess == (int) CommunityMember.CommunityMemberAccountType.OWNER)
                        {
                            elem.InnerText = "1";
                        }
                        else
                        {
                            elem.InnerText = "0";
                        }

                        root.InsertBefore (elem, root.FirstChild);

                        elem = doc.CreateElement ("Moderator");

                        if (zoneAccess == (int) CommunityMember.CommunityMemberAccountType.MODERATOR)
                        {
                            elem.InnerText = "1";
                        }
                        else
                        {
                            elem.InnerText = "0";
                        }

                        root.InsertBefore (elem, root.FirstChild);

                        elem = doc.CreateElement ("GM");

                        if (isGM)
                        {
                            elem.InnerText = "1";
                        }
                        else
                        {
                            elem.InnerText = "0";
                        }

                        root.InsertBefore (elem, root.FirstChild);

                        elem = doc.CreateElement ("ReturnDescription");
                        elem.InnerText = "userid not found";

                        root.InsertBefore (elem, root.FirstChild);

                        elem = doc.CreateElement ("ReturnCode");
                        elem.InnerText = "-1";

                        root.InsertBefore (elem, root.FirstChild);

                        Response.Write (root.OuterXml);
                    }
                }
                else if (actionreq.Equals("Block") && (newKimBlock || oldKimBlock))
                {
                    if (Request.Params["userId"] == null && Request.Params["username"] == null) 
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>user id not specified for block</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int sourceId = (int) Constants.eUSER_BLOCK_SOURCE.UNKNOWN;
                    if (Request.Params["source"] != null)
                    {
                        if (Request.Params["source"].ToString ().ToLower () == "wok")
                        {
                            sourceId = (int) Constants.eUSER_BLOCK_SOURCE.WOK;
                        }
                        else if (Request.Params["source"].ToString ().ToLower () == "kim")
                        {
                            sourceId = (int) Constants.eUSER_BLOCK_SOURCE.KIM;
                        }
                    }

                    if (Request.Params["userId"] != null)
                    {
                        blockedUserId = Int32.Parse(Request.Params["userId"].ToString());
                    }
                    else
                    {
                        blockedUserId = UsersUtility.GetUserIdFromUsername (Convert.ToString (Request.Params["username"]));
                    }

                    if (blockedUserId == 0)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid user id for block</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int ret = GetUserFacade.BlockUser (m_userId, blockedUserId, sourceId);

                    if (ret == 0)
                    {
                        responseStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>update error</ResultDescription>\r\n</Result>";
                    }
                    else
                    {
                        responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
                    }

                    Response.Write(responseStr);

                    return;
                }
                else if (actionreq.Equals("UnBlock") || oldKimUnblock)
                {
                    if (Request.Params["userId"] == null && Request.Params["username"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>user id unblock type not specified</ResultDescription>\r\n</Result>";
                        Response.Write (errorStr);
                        return;
                    }

                    if (Request.Params["userId"] != null)
                    {
                        blockedUserId = Int32.Parse (Request.Params["userId"].ToString ());
                    }
                    else
                    {
                        blockedUserId = UsersUtility.GetUserIdFromUsername (Convert.ToString (Request.Params["username"]));
                    }

                    if (blockedUserId == 0)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid user id for unblock</ResultDescription>\r\n</Result>";
                        Response.Write (errorStr);
                        return;
                    }

                    int sourceId = (int)Constants.eUSER_BLOCK_SOURCE.UNKNOWN;
                    if (Request.Params["source"] != null)
                    {
                        if (Request.Params["source"].ToString().ToLower() == "wok")
                        {
                            sourceId = (int)Constants.eUSER_BLOCK_SOURCE.WOK;
                        }
                        else if (Request.Params["source"].ToString().ToLower() == "kim")
                        {
                            sourceId = (int)Constants.eUSER_BLOCK_SOURCE.KIM;
                        }
                    }

                    int ret = GetUserFacade.UnBlockUser (m_userId, blockedUserId, sourceId);

                    if (ret == 0)
                    {
                        responseStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>update error</ResultDescription>\r\n</Result>";
                    }
                    else
                    {
                        responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
                    }

                    Response.Write (responseStr);

                    return;
                }
                else
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action request unknown</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
