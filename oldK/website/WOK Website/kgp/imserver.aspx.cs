///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;
using System.Web.Security;

namespace KlausEnt.KEP.Kaneva.kgp
{
	public class imserver : System.Web.UI.Page
	{
		const int MAX_TXN_RECORDS_PER_QUERY = 1000;
        const int MIN_USERNAME_LEN = 3;
        const int MISSING_USER_PARAMETER = -1;
        const int INVALID_USERNAME = -2;
        const string REQ_GET_FRIEND_TRANSACTIONS = "friendtxns";
        const string REQ_GET_USER_INFO = "userinfo";
        const string REQ_GET_FRIENDS_LIST = "friends";
        const string REQ_SET_USER_PRESENCE = "setpresence";
        const string REQ_GET_ZONE_ACCESS = "zoneinfo";
        const string REQ_GET_BLOCK_USERS_LIST = "blockedusers";

        // TO BE MOVED to web.config
        static string AUTH_USER = "imserver";
        static string AUTH_PASS = "fK092jNYmv";
        static string AUTH_SALT = "hihuEa/HQY4J";

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private UserFacade userFacade = new UserFacade();
        private static string m_credentialHash = CalcCredential();
        
		protected void Page_Load(object sender, EventArgs e)
		{
            try
            {
                DataTable dt;
                string dtXml;

                AuthenticateCaller();

                ProcessRequest(Request.QueryString["request"], out dt, out dtXml);

                XmlDocument doc = new XmlDocument();
                if (dt != null)
                {
                    dt.TableName = "Record";
                    DataSet ds = new DataSet("result");
                    ds.Tables.Add(dt);
                    doc.LoadXml(ds.GetXml());
                }
                else if (dtXml != null)
                {
                    doc.LoadXml(dtXml);
                }
                else
                {
                    doc.LoadXml("<result></result>");
                }
                XmlNode root = doc.DocumentElement;

                XmlElement elem = doc.CreateElement("ReturnDescription");
                elem.InnerText = "success";
                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("ReturnCode");
                elem.InnerText = "0";
                root.InsertBefore(elem, root.FirstChild);

                Response.Write(root.OuterXml);
            }
            catch (IMServerException ex)
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml("<result></result>");
                XmlElement root = doc.DocumentElement;

                XmlElement elem = doc.CreateElement("ReturnDescription");
                elem.InnerText = ex.Message;
                root.AppendChild(elem);

                elem = doc.CreateElement("ReturnCode");
                elem.InnerText = Convert.ToString(ex.ErrorCode);
                root.InsertBefore(elem, root.FirstChild);

                Response.Write(root.OuterXml);
            }
		}

        private void ProcessRequest(string requestType, out DataTable dt, out string dtXml)
        {
            dt = null;
            dtXml = null;

            if (requestType == null)
            {
                throw new IMServerException(-1, "Missing request parameter");
            }

            string userName = Request.QueryString["user"];
            int userId = MISSING_USER_PARAMETER;
            int playerId = MISSING_USER_PARAMETER;

            if (userName != null)
            {
                ParseUserName(userName, out userId, out playerId);
            }

            if (requestType == REQ_GET_FRIEND_TRANSACTIONS)
            {
                string startStr = Request.QueryString["start"];
                string maxStr = Request.QueryString["max"];

                int start = -1, max = MAX_TXN_RECORDS_PER_QUERY;
                if (startStr != null)
                {
                    try
                    {
                        start = Convert.ToInt32(startStr);
                    }
                    catch (Exception) { }
                }
                if (maxStr != null)
                {
                    try
                    {
                        max = Convert.ToInt32(maxStr);
                    }
                    catch (Exception) { }
                }

                dt = GetFriendsTxns(start, max);
            }

            else if (requestType == REQ_GET_USER_INFO)
            {
                VerifyUserId(userId);
                dtXml = GetUserInfo(userId);
            }
            else if (requestType == REQ_GET_FRIENDS_LIST)
            {
                VerifyUserId(userId);
                dt = GetFriends(userId);
            }
            else if (requestType == REQ_GET_BLOCK_USERS_LIST)
            {
                VerifyUserId(userId);
                dt = GetBlockedUsers(userId);
            }
            else if (requestType == REQ_SET_USER_PRESENCE)
            {
                VerifyUserId(userId);

                int online = 0;
                try
                {
                    online = Convert.ToInt32(Request.QueryString["online"]);
                }
                catch (Exception)
                {
                    throw new IMServerException(-1, "Invalid online flag");
                }

                string statusMsg = "";
                try
                {
                    statusMsg = Convert.ToString(Request.QueryString["status"]);
                }
                catch (Exception)
                {
                    throw new IMServerException(-1, "Invalid status message");
                }

                SetPresence(userId, online, statusMsg);
            }
            else if (requestType == REQ_GET_ZONE_ACCESS)
            {
                VerifyUserId(userId);

                string zone = null;
                try
                {
                    zone = Convert.ToString(Request.QueryString["zone"]);
                }
                catch (Exception)
                {
                }

                if (zone == null)
                {
                    throw new IMServerException(-1, "Invalid zone parameter");
                }

                dt = GetUserZoneAccess(userId, zone);
            }
            else
            {
                throw new IMServerException(-1, "Unknown request '" + requestType + "'");
            }
        }

        private bool ParseUserName(string userName, out int userId, out int playerId)
        {
            userId = INVALID_USERNAME;
            playerId = INVALID_USERNAME;

            if (userName.Length >= MIN_USERNAME_LEN)
            {
                DataRow dr = userFacade.GetUserIdPlayerIdFromUsername(userName);
                if (dr != null)
                {
                    try
                    {
                        userId = Convert.ToInt32(dr["user_id"]);
                        playerId = Convert.ToInt32(dr["player_id"]);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        // DB error
                        m_logger.Error("Error parsing data row from GetUserIdPlayerIdFromUsername", ex);
                        userId = INVALID_USERNAME;
                        playerId = INVALID_USERNAME;
                    }
                }
            }

            return false;
        }

        void VerifyUserId(int userId)
        {
            if (userId == MISSING_USER_PARAMETER)
            {
                throw new IMServerException(-1, "Missing user parameter");
            }

            if (userId == INVALID_USERNAME)
            {
                throw new IMServerException(-1, "Invalid user name");
            }
        }

        private DataTable GetFriendsTxns(int start, int max)
        {
            string sql = "";
            Hashtable parameters = null;
            DataTable dtFriendsTxns = null;

            if (start == -1 && max == -1)
            {
                // Requesting max txn ID
                sql = "SELECT max(txn_id) as TxnId, 'max' as Event, '_' as Sender, '_' as Receiver FROM kaneva.friend_transactions";
                parameters = new Hashtable();
                dtFriendsTxns = KanevaGlobals.GetDatabaseUtility().GetDataTable(sql, parameters);
            }
            else
            {
                // Is it in cache?
                string cacheKey = CentralCache.keyFriendTransactions (start, max);
                dtFriendsTxns = (DataTable) CentralCache.Get(cacheKey);

                if (dtFriendsTxns == null)
                {
                    sql = "SELECT txn.txn_id as TxnId, lower(txn.txn_type) as Event, u1.username as Sender, u2.username as Receiver " +
                        "FROM kaneva.friend_transactions txn " +
                        "INNER JOIN kaneva.users u1 ON txn.user_id=u1.user_id " +
                        "INNER JOIN kaneva.users u2 ON txn.friend_id=u2.user_id " +
                        "WHERE txn_id>@lastTxnId limit @maxRows";

                    parameters = new Hashtable();
                    parameters.Add("@lastTxnId", start);
                    parameters.Add("@maxRows", max);
                    dtFriendsTxns = KanevaGlobals.GetDatabaseUtility().GetDataTable(sql, parameters);

                    // Add result to cache
                    CentralCache.Store (cacheKey, dtFriendsTxns, TimeSpan.FromMinutes (15));

                    // keep track of the keys we cache so they can be cleaned
                    // Clean all on any insert to friend_transaction table
                    string friendTransactionsCachedKey = CentralCache.keyFriendTransactionsCached();
                    string ftCached = (string)CentralCache.Get(friendTransactionsCachedKey);

                    if (ftCached != null)
                    {
                        if ( !ftCached.Contains( friendTransactionsCachedKey ) )
                        {
                            ftCached += ";" + cacheKey; 
                            CentralCache.Store(friendTransactionsCachedKey, ftCached, TimeSpan.FromMinutes(15));
                        }
                    }
                    else
                    {
                        // No combinations are cached yet
                        CentralCache.Store(friendTransactionsCachedKey, cacheKey, TimeSpan.FromMinutes(15));
                    }


                }
            }

            return dtFriendsTxns;
        }

        private string GetUserInfo(int userId)
        {
            User user = userFacade.GetUser(userId);
            if (user == null || user.UserId == 0)
            {
                // Not found
                m_logger.Error("UserFacade.GetUser failed with a valid user id: " + userId.ToString());
                throw new IMServerException(-1, "Database Error");
            }

            bool isAdmin, isGm;
            DateTime secondToLastLogin;
            userFacade.GetPlayerAdmin(userId, out isAdmin, out isGm, out secondToLastLogin);

            XmlDocument docUserInfo = new XmlDocument();
            XmlNode root, elem;

            docUserInfo.LoadXml("<result></result>");
            root = docUserInfo.DocumentElement;
            elem = docUserInfo.CreateElement("Record");
            root.AppendChild(elem);

            XmlNode col = docUserInfo.CreateElement("Name");
            col.InnerText = Convert.ToString(user.Username);
            elem.AppendChild(col);
            col = docUserInfo.CreateElement("FullName");
            col.InnerText = Convert.ToString(user.FirstName) + " " + Convert.ToString(user.LastName);
            elem.AppendChild(col);
            col = docUserInfo.CreateElement("GM");
            col.InnerText = isGm ? "1" : "0";
            elem.AppendChild(col);
            col = docUserInfo.CreateElement("Admin");
            col.InnerText = isAdmin ? "1" : "0";
            elem.AppendChild(col);

            return root.OuterXml;
        }

        private DataTable GetFriends(int userId)
        {
            string sql = "";
            Hashtable parameters = null;

            sql = "SELECT u.username as Name, t_max_txn_id.max_txn_id as MaxTxnId FROM kaneva.friends f " +
                    "INNER JOIN kaneva.users u ON f.friend_id=u.user_id " +
                    "LEFT OUTER JOIN wok.game_users gu ON u.user_id=gu.kaneva_user_id, " +
                    "(SELECT max(txn_id) as max_txn_id FROM kaneva.friend_transactions) t_max_txn_id " +
                    "WHERE f.user_id=@userId and u.active='Y' " + 
                    "ORDER BY gu.last_logon DESC, u.last_login DESC " +   // Order by gu.last_logon, if null then by u.last_login.
                    "LIMIT 1500";   // Set max friend list length to 1500 for now

            parameters = new Hashtable();
            parameters.Add("@userId", userId);
            return KanevaGlobals.GetDatabaseUtility().GetDataTable(sql, parameters);
        }

        private DataTable GetBlockedUsers(int userId)
        {   //**BLOCK -- remove after test
            string sql = "";
            Hashtable parameters = null;

            sql = "SELECT distinct u.username as Name FROM kaneva.blocked_users bu " + 
                    "INNER JOIN kaneva.users u ON bu.blocked_user_id=u.user_id " + 
                    "WHERE bu.user_id=@userId";

            parameters = new Hashtable();
            parameters.Add("@userId", userId);
            return KanevaGlobals.GetDatabaseUtility().GetDataTable(sql, parameters);
        }

        private void SetPresence(int userId, int online, string statusMsg)
        {
            string sql = "";
            Hashtable parameters = null;
            int rowsAffected = 0;

            sql = "REPLACE INTO im_users(user_id, online, presence) VALUES (@userId, @online, @statusMsg)";
            parameters = new Hashtable();
            parameters.Add("@userId", userId);
            parameters.Add("@online", online);
            parameters.Add("@statusMsg", statusMsg);

            rowsAffected = KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sql, parameters);

//            sql = "INSERT IGNORE INTO developer.active_logins(user_id, server_id, game_id, created_date) VALUES (@userId, 0, 5354, NOW())";
//            KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sql, parameters);

            if (rowsAffected == 0)
            {
                throw new IMServerException(-1, "No matching record found");
            }
        }

        private DataTable GetUserZoneAccess(int userId, string zone)
        {
            //Remove trailing zone instance Id from chat room name
            int idxQue = zone.LastIndexOf('?');
            if (idxQue > 0)
            {
                zone = zone.Substring(0, idxQue);
            }

            DataTable dt = UsersUtility.GetCurrentPlaceInfo(userId);
            if (dt == null || dt.Rows.Count == 0)
            {
                throw new IMServerException(-1, "User not in world");
            }

            DataRow dr = dt.Rows[0];
            string currentZone = "";   // e.g. user.home

            int zoneType = 0;
            string zoneName = null;
            try
            {
                zoneType = Convert.ToInt32(dr["current_zone_type"]);
                zoneName = Convert.ToString(dr["name"]);
            }
            catch (Exception)
            {
                throw new IMServerException(-1, "Server Error");
            }

            bool checkPostfixOnly = false;
            switch (zoneType)
            {
                case Constants.APARTMENT_ZONE:
                    currentZone = StpUrl.MakeAptUrlPath(zoneName);
                    break;
                case Constants.PERMANENT_ZONE:
                    currentZone = StpUrl.MakeZoneUrlPath(zoneName);
                    break;
                case Constants.BROADBAND_ZONE:
                    currentZone = StpUrl.MakeCommunityUrlPath(zoneName);
                    break;
                case 0:
                    //3dapp zone: lobby.zone.appname.app or xxxx.arena.appname.app - note that appname might include space and other non-alpha-num chars
                    //TODO: support appname escaping
                    currentZone = "." + zoneName + ".app";
                    checkPostfixOnly = true;
                    break;
                default:
                    throw new IMServerException(-1, "Unsupported zone type: " + zoneType.ToString());
            }

            if (!checkPostfixOnly && currentZone.ToLower() != zone.ToLower() || checkPostfixOnly && !(zone.Length>currentZone.Length && zone.ToLower().EndsWith(currentZone.ToLower())))
            {
                throw new IMServerException(-1, "User not in zone: " + zone);
            }

            return dt;
        }

        private void AuthenticateCaller()
        {
            string conn = Request["conn"];
            if (conn == null)
            {
                throw new IMServerException(-2, "No credential provided");
            }

            if (conn.CompareTo( m_credentialHash )!=0 )
            {
                throw new IMServerException(-2, "Invalid credential");
            }
        }

        private static string CalcCredential()
        {
            // Get the hashed password
            return FormsAuthentication.HashPasswordForStoringInConfigFile(MakeHash(AUTH_PASS + AUTH_USER.ToLower()) + AUTH_SALT, "MD5");
        }

        /// <summary>
        /// MakeHash copied from UserFacade
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private static string MakeHash(string s)
        {
            // This is one implementation of the abstract class MD5.
            System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();

            System.Text.Encoder encoder = System.Text.ASCIIEncoding.UTF8.GetEncoder();
            int byteCount = encoder.GetByteCount(s.ToCharArray(), 0, s.Length, true);
            byte[] bytes = new byte[byteCount];
            encoder.GetBytes(s.ToCharArray(), 0, s.Length, bytes, 0, true);
            byte[] result = md5.ComputeHash(bytes);

            string ret = "";
            for (int i = 0; i < result.Length; i++)
            {
                ret = ret + result[i].ToString("x2");
            }
            return ret;
        }
	}

    class IMServerException: Exception
    {
        private int _errorCode;

        public IMServerException(int errCode, string errMessage)
            : base( errMessage )
        {
            _errorCode = errCode;
        }

        public int ErrorCode
        {
            get { return _errorCode; }
        }
    }
}
