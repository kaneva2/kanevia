///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for eventTest.
    /// </summary>
    public class eventTest : KgpBasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(true))
            {
                if (UsersUtility.IsUserCSR())
                {
                    if (Request.Params["test"] == "ChangeItem")
                    {
                        // for now just take three things 
                        // objectType 
                        // actionType
                        // id

                        int ot = 0;
                        int ct = 0;
                        int id = 0;

                        if ((Request.Params["ot"] == null) || (Request.Params["ct"] == null) || (Request.Params["id"] == null) ||
                            !Int32.TryParse(Request.Params["ot"], out ot) || !Int32.TryParse(Request.Params["ct"], out ct) || !Int32.TryParse(Request.Params["id"], out id))
                        {
                            Response.Write("Invalid parameters, supply ot, ct and id");
                            return;
                        }

                        RemoteEventSender rs = new RemoteEventSender();
                        rs.BroadcastItemChangeEvent((RemoteEventSender.ObjectType)ot,
                                                    (RemoteEventSender.ChangeType)ct,
                                                     id);

                        Response.Write("Ok");
                    }
                    else if (Request.Params["test"] == "Rave")
                    {
                        RaveFacade rf = new RaveFacade();
                        rf.SendRavedEventToPlayer(m_userId, Convert.ToInt32(Request.Params["userId"].ToString()), RaveType.eRAVE_TYPE.SINGLE );
                    }
                    else
                    {
                        Response.Write("Must supply test=ChangeItem");
                    }
                }
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
