///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Xml;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;
using log4net;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for Fame.
    /// </summary>
    public class _worldTemplates : KgpBasePage
    {
        #region Declarations

        private int _page;
        private int _itemsPerPage;
        private string _action;

        private string _errorResult = "<Result>\r\n  <ReturnCode>{0}</ReturnCode>\r\n  <ReturnDescription>{1}</ReturnDescription>\r\n </Result>";
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(true))
            {
                // Get parameters
                GetRequestParams();

                // Process actions
                switch (_action)
                {
                    case "getTemplateList":
                        ProcessGetTemplateList();
                        break;
                    case "getDefaultHomeTemplate":
                        ProcessGetDefaultHomeTemplate();
                        break;
                    default:
                        Response.Write(string.Format(_errorResult, -1, "Invalid action"));
                        break;
                }
            }
        }

        /// <summary>
        /// Reads all request parameters from query string and provides appropriate defaults
        /// </summary>
        private void GetRequestParams()
        {
            int iOut;

            _page = (Int32.TryParse(Request.Params["start"], out iOut) ? iOut : 0);
            _itemsPerPage = (Int32.TryParse(Request.Params["max"], out iOut) ? iOut : 0);
            _action = Request.Params["action"];
        }

        /// <summary>
        /// Processes the "getTemplateList" webcall action
        /// </summary>
        private void ProcessGetTemplateList()
        {
            // Validate parameters
            if (_page <= 0 || _itemsPerPage <= 0)
            {
                Response.Write(string.Format(_errorResult, -1, "Invalid page number and/or items per page"));
                return;
            }

            int[] template_statuses;
            int[] templatePassGroups;

            if (IsAdministrator())
            {
                template_statuses = new int[] { Convert.ToInt32(WorldTemplate.WorldTemplateStatus.ACTIVE), Convert.ToInt32(WorldTemplate.WorldTemplateStatus.ADMIN_ONLY) };
                templatePassGroups = new int[] { 0, Configuration.AccessPassGroupID, Configuration.VipPassGroupID };
            }
            else
            {
                template_statuses = new int[] { Convert.ToInt32(WorldTemplate.WorldTemplateStatus.ACTIVE) };

                if (KanevaWebGlobals.CurrentUser.HasVIPPass)
                {
                    templatePassGroups = new int[] { 0, Configuration.AccessPassGroupID, Configuration.VipPassGroupID };
                }
                else if (KanevaWebGlobals.CurrentUser.HasAccessPass)
                {
                    templatePassGroups = new int[] { 0, Configuration.AccessPassGroupID };
                }
                else
                {
                    templatePassGroups = new int[] { 0 };
                }
            }

            PagedList<WorldTemplate> templateList = GetGameFacade.GetWorldTemplates(template_statuses, templatePassGroups, KanevaWebGlobals.CurrentUser.UserId, KanevaWebGlobals.CurrentUser.SignupDate, _page, _itemsPerPage);
            Response.Write(BuildWorldTemplateListXML(templateList, _page));
        }

        private void ProcessGetDefaultHomeTemplate()
        {
            WorldTemplate defaultHomeTemplate = GetGameFacade.GetDefaultHomeWorldTemplate(KanevaWebGlobals.CurrentUser.UserId);

            if (defaultHomeTemplate.GlobalId <= 0 || defaultHomeTemplate.ZoneIndexPlain <= 0)
            {
                Response.Write(string.Format(_errorResult, -1, "Unable to find default home world template."));
                return;
            }

            // Insert "Worlds to Templates" record
            if (KanevaWebGlobals.CurrentUser.HomeCommunityId > 0)
            {
                GetGameFacade.InsertWorldsToTemplate(KanevaWebGlobals.CurrentUser.HomeCommunityId, defaultHomeTemplate.TemplateId);

                // Insert A/B test associations for the new home world
                IList<Experiment> activeExperiments = GetExperimentFacade.GetAvailableExperimentsByWorldContext(KanevaWebGlobals.CurrentUser.WokPlayerId, 3, Experiment.ASSIGN_ON_CREATION_ONLY, true);

                foreach (Experiment experiment in activeExperiments)
                {
                    string assignedGroup = GetExperimentFacade.AssignParticipant(experiment);
                    if (!string.IsNullOrWhiteSpace(assignedGroup))
                    {
                        GetExperimentFacade.SaveWorldExperimentGroup(new WorldExperimentGroup(string.Empty, KanevaWebGlobals.CurrentUser.WokPlayerId, 3, assignedGroup, DateTime.Now, null));
                    }
                }

                // If home world needs to be linked to a parent world (ie.K-town), insert record 
                try
                {
                    string id = GetScriptGameItemFacade.GetSGIGlobalConfigValue("HomeChildWorldTemplateId");
                    
                    if (id != string.Empty)
                    {
                        int templateId = Convert.ToInt32(id);

                        if (defaultHomeTemplate.TemplateId == templateId)
                        {
                            int ret = GetCommunityFacade.InsertHomeCommunityIdAsChildWorld(KanevaWebGlobals.CurrentUser.HomeCommunityId);

                            if (ret <= 0)
                            {
                                m_logger.Error("Error inserting user home world as child world. User: " + KanevaWebGlobals.CurrentUser.UserId + ", CommunityId; " + KanevaWebGlobals.CurrentUser.HomeCommunityId);
                            }
                        }
                    }
                    else
                    {
                        m_logger.Error("HomeWorldTemplateId not found in script_game_item_global_configs");
                    }
                }
                catch (Exception ex)
                {
                    m_logger.Error("Error adding user home world as child world. User: " + KanevaWebGlobals.CurrentUser.UserId + ", CommunityId; " + KanevaWebGlobals.CurrentUser.HomeCommunityId + " : " + ex.ToString());
                }
            }

            Response.Write(string.Format("<Result>\r\n  <ReturnCode>{0}</ReturnCode>\r\n  <ReturnDescription>{1}</ReturnDescription>\r\n <GlobalId>{2}</GlobalId>\r\n <ZoneIndexPlain>{3}</ZoneIndexPlain>\r\n </Result>", 0, "success", defaultHomeTemplate.GlobalId, defaultHomeTemplate.ZoneIndexPlain));
        }

        /// <summary>
        /// Called to build the XML string for a paged list of WorldTemplates
        /// </summary>
        /// <param name="templateList">The list to serialize</param>
        /// <param name="page">The current page</param>
        /// <returns>XML</returns>
        private string BuildWorldTemplateListXML(PagedList<WorldTemplate> templateList, int page)
        {
            // Serialize the template list
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(templateList.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);

            x.Serialize(writer, templateList);

            XmlDocument listDoc = new XmlDocument();
            listDoc.LoadXml(sb.ToString());

            // Replace the serialized XML root with something prettier
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("Result");
            doc.AppendChild(root);
            root.InnerXml = listDoc.DocumentElement.InnerXml;

            // Append metadata
            int numRecords = templateList.Count;
            XmlElement elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = (int)templateList.TotalCount;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("Page");
            elem.InnerText = page.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            return root.OuterXml;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
