///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for inventory.
	/// </summary>
	public class metricRequest : KgpBasePage
    {
        #region Declarations

        const string SUCCESS = "0";
        const string ERROR = "1";

        #endregion

		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
				if ( (Request.Params["gameId"] == null) || (Request.Params["requestType"] == null) )
				{
					string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>gameId and requestType not specified</ResultDescription>\r\n</Result>";
					Response.Write(errorStr);
					return;
				}

                int gameId = Convert.ToInt32(Request.Params["gameId"]);
                int requestType = Convert.ToInt32(Request.Params["requestType"]);

                // Business logic for routing, framework API so can be used by developer API

                // Call framework API for inserting metric, get GUID tracking as a return value.

                // Give tracking GUID back to caller, i.e. client
            }

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
