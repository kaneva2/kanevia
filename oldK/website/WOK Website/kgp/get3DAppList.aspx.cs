///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class _3DAppList : KgpBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CheckUserId(false))
            {
                if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max per page not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }


                int page = Int32.Parse(Request.Params["start"].ToString());
                int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                int[] communityTypeIds;

                // If they want all world types, give it to them. otherwise, 3dapps only
                if (!string.IsNullOrEmpty(Request.Params["allWorldTypes"]) && Request.Params["allWorldTypes"].Equals("true"))
                {
                    communityTypeIds = new int[]{ Convert.ToInt32 (CommunityType.APP_3D), Convert.ToInt32 (CommunityType.COMMUNITY), Convert.ToInt32 (CommunityType.HOME) };
                }
                else
                {
                    communityTypeIds = new int[] { Convert.ToInt32(CommunityType.APP_3D) };
                }

                
                PagedList<Community> plCommunities = GetCommunityFacade.GetUserCommunities(KanevaWebGlobals.CurrentUser.UserId, communityTypeIds, "game_server_is_on_sum DESC, game_access_id ASC", "cm.account_type_id IN (1,2) AND place_type_id < 99", page, items_per_page, true);

                // Build the XML
                XmlDocument doc = new XmlDocument();
                XmlElement rootNode = doc.CreateElement("Result");
                doc.AppendChild(rootNode);

                XmlElement My3DAppsNode = doc.CreateElement("My3DApps");
                rootNode.AppendChild(My3DAppsNode);

                foreach (Community community in plCommunities)
                {
                    XmlElement AppNode = doc.CreateElement("3DApp");
                    AppNode.SetAttribute("Name", community.Name);
                    AppNode.SetAttribute("Description", community.Description);
                    AppNode.SetAttribute("GameId", community.WOK3App.GameId.ToString ());
                    AppNode.SetAttribute("CommunityId", community.CommunityId.ToString ());
                    AppNode.SetAttribute("ThumbnailURL", community.ThumbnailSmallPath);
                    AppNode.SetAttribute("OwnerUsername", community.CreatorUsername);
                    AppNode.SetAttribute("IsPublic", community.IsPublic);
                    AppNode.SetAttribute("ServerStatusId", community.WOK3App.ServerStatusId.ToString ());
                    AppNode.SetAttribute("GameStatusId", community.WOK3App.GameStatusId.ToString());
                    AppNode.SetAttribute("GameAccessId", community.WOK3App.GameAccessId.ToString());
                    AppNode.SetAttribute("IncubatorHosted", community.WOK3App.IsIncubatorHosted ? "1" : "0");
                    My3DAppsNode.AppendChild(AppNode);
                }

                XmlNode root = doc.DocumentElement;

                XmlElement elem = doc.CreateElement("NumberRecords");
                elem.InnerText = plCommunities.Count.ToString();

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("TotalNumberRecords");
                elem.InnerText = plCommunities.TotalCount.ToString();

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("ReturnDescription");
                elem.InnerText = "success";

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("ReturnCode");
                elem.InnerText = "0";

                root.InsertBefore(elem, root.FirstChild);

                Response.Write(root.OuterXml);

            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}