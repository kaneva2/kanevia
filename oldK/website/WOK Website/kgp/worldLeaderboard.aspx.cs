///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class worldLeaderboard : KgpBasePage
    {
        #region Declarations

        // Actions
        const string ACTION_GET_LEADERBOARD = "getLeaderboard";
        const string ACTION_GET_LEADERBOARD_FINAL = "getLeaderboardFinal";
        const string ACTION_ADD_RAVE = "rave";
        const string ACTION_ADD_VISIT = "visit";
        const string ACTION_GET_TEAM_MEMBERS = "teamGet";
        const string ACTION_UPDATE_TEAM_MEMBERS = "teamUpdate";
        const string ACTION_GET_USER_AWARD_TO_REDEEM = "getAward";
        const string ACTION_REDEEM_USER_AWARD = "redeemAward";
        const string ACTION_SET_OBJECT_NAME = "setObjectName";

        // Params
        private string _action = null;
        private string _name = null;
        private string[] _memberUsernames = null;
        private int _objPlacementId = 0;
        private int _zoneInstanceId = 0;
        private int _zoneType = 0;
        private int _userId = 0;
        private int _worldLeaderboardId = 0;

        // Response types
        private string _resultTemplate = "<Result>\r\n  <ReturnCode>{0}</ReturnCode>\r\n  <ReturnDescription>{1}</ReturnDescription>\r\n </Result>";

        #endregion Declerations
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (CheckUserId(true))
                {
                    // Get parameters
                    GetRequestParams();

                    // Make sure we at least have an action to branch
                    if (string.IsNullOrWhiteSpace(_action))
                    {
                        Response.Write(string.Format(_resultTemplate, -1, "action must be specified"));
                        return;
                    }

                    // Branch via report type
                    switch (_action)
                    {
                        case ACTION_GET_LEADERBOARD:
                            ProcessGetLeaderboardAction();  
                            break;
                        case ACTION_GET_LEADERBOARD_FINAL:
                            ProcessGetLeaderboardFinalAction();
                            break;
                        case ACTION_ADD_RAVE:
                        case ACTION_ADD_VISIT:
                            ProcessObjectInteractionAction();
                            break;
                        case ACTION_GET_TEAM_MEMBERS:
                            ProcessGetTeamMembersAction();
                            break;
                        case ACTION_UPDATE_TEAM_MEMBERS:
                            ProcessUpdateTeamMembersAction();
                            break;
                        case ACTION_GET_USER_AWARD_TO_REDEEM:
                            ProcessGetUserAwardToRedeemAction();
                            break;
                        case ACTION_REDEEM_USER_AWARD:
                            ProcessRedeemUserAwardAction();
                            break;
                        case ACTION_SET_OBJECT_NAME:
                            ProcessSetObjectName();
                            break;
                        default:
                            Response.Write(string.Format(_resultTemplate, -1, "invalid action"));
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Sets page variables using either passed in values or appropriate defaults
        /// </summary>
        private void GetRequestParams()
        {
            int intOut = 0;

            _action = Request.Params["action"];
            _name = Request.Params["name"];
            _userId = Int32.TryParse(Request.Params["userId"], out intOut) ? intOut : 0;
            _zoneInstanceId = Int32.TryParse(Request.Params["zoneInstanceId"], out intOut) ? intOut : 0;
            _zoneType = Int32.TryParse(Request.Params["zoneType"], out intOut) ? intOut : 0;
            _objPlacementId = Int32.TryParse(Request.Params["objPlacementId"], out intOut) ? intOut : 0;
            _worldLeaderboardId = Int32.TryParse(Request.Params["worldLeaderboardId"], out intOut) ? intOut : 0;

            if (Request.Params["usernames"] != null && Request.Params["usernames"] != "")
            {
                try
                {
                    _memberUsernames = Request.Params["usernames"].ToString().Split('|');
                }
                catch { }
            }
        }

        /// <summary>
        /// Retrieves the leaderboard for a world
        /// </summary>
        private void ProcessGetLeaderboardAction()
        {
            // ?action=getLeaderboard&zoneInstanceId=244571&zoneType=3
            // Validate parameters
            if (_zoneInstanceId <= 0 || _zoneType <= 0)
            {
                Response.Write(string.Format(_resultTemplate, -1, "invalid input parameter"));
                return;
            }

            DataTable dtGameObjects = GetGameFacade.GetLeaderboard(_zoneInstanceId, _zoneType, Configuration.WorldLeaderboardRavePointValue, Configuration.WorldLeaderboardVisitPointValue);

            List<int> objPlacementIds = new List<int>();
            if (dtGameObjects != null)
            {
                foreach (DataRow row in dtGameObjects.Rows)
                {
                    objPlacementIds.Add(Convert.ToInt32(row["obj_placement_id"]));
                }
            }

            DataTable dtTeamMembers = GetGameFacade.GetTeamMembers(objPlacementIds);
            
            dtGameObjects.TableName = "current_leaderboard";
            BuildCurrentLeaderboardXML(dtGameObjects, dtTeamMembers);
        }

        /// <summary>
        /// Retrieves the leaderboard for a world
        /// </summary>
        private void ProcessGetLeaderboardFinalAction()
        {
            // ?action=getLeaderboardFinal&zoneInstanceId=98765&zoneType=3
            // Validate parameters
            if (_zoneInstanceId <= 0 || _zoneType <= 0)
            {
                Response.Write(string.Format(_resultTemplate, -1, "invalid input parameter"));
                return;
            }

            DataTable dtGameObjects = GetGameFacade.GetFinalLeaderboard(_zoneInstanceId, _zoneType);
            DataTable dtTeamMembers = GetGameFacade.GetFinalLeaderboardTeamMembers(_zoneInstanceId, _zoneType);

            dtGameObjects.TableName = "final_leaderboard";
            BuildFinalLeaderboardXML(dtGameObjects, dtTeamMembers);
        }

        /// <summary>
        /// Adds interaction with world object
        /// </summary>
        private void ProcessObjectInteractionAction()
        {
            // RAVE --  ?action=rave&zoneInstanceId=1610612751&zoneType=3&objPlacementId=244571&name=My%20Test%20Land%20Flag
            // VISIT -- ?action=visit&zoneInstanceId=1610612751&zoneType=3&objPlacementId=244571&name=My%20Test%20Land%20Flag
            // Validate parameters
            if ( _name == null || _name == "" || _zoneInstanceId <= 0 || _zoneType <= 0 || _objPlacementId <= 0 )
            {
                Response.Write(string.Format(_resultTemplate, -1, "invalid input parameter"));
                return;
            }

            int ret = GetGameFacade.InsertObjectUserInteraction(KanevaWebGlobals.CurrentUser.UserId, _zoneInstanceId, _zoneType, _objPlacementId, Server.HtmlEncode(_name), _action.ToUpper());
            string msg = "";

            if (ret == 0)
            {
                msg = "success";
            }
            else if (ret == -1)
            {
                msg = "User has already " + (_action == "rave" ? "raved" : "visited") + " this item";
            }
            else if (ret == -2)
            {
                msg = "An error occurred when attempting to insert " + _action.ToLower() + " into the database";
            }
            else
            {
                ret = -3;
                msg = "Unknown error occurred.";
            }

            Response.Write(string.Format(_resultTemplate, ret, msg));
        }

        /// <summary>
        /// Process the update team member action for object
        /// </summary>
        private void ProcessUpdateTeamMembersAction()
        {
            // ?action=teamUpdate&objPlacementId=244571&userIds=111|222|333|444|555
            // Validate parameters
            if (_memberUsernames == null || _memberUsernames.Length == 0 || _objPlacementId <= 0) 
            {
                Response.Write(string.Format(_resultTemplate, -1, "invalid input parameter"));
                return;
            }

            if (_memberUsernames.Length > 5)
            {
                Response.Write(string.Format(_resultTemplate, -1, "more than 5 team members"));
                return;
            }

            List<int> _memberUserIds = new List<int>(5);
            int _userId = 0;
            for (int i = 0; i < _memberUsernames.Length; i++)
            {
                _userId = 0;
                _userId = GetUserFacade.GetUserIdFromUsername(_memberUsernames[i]);

                if (_userId > 0)
                {
                    _memberUserIds.Add(_userId);
                }
            }

            if (_memberUserIds.Count > 0)
            {
                if (_memberUserIds.Count != _memberUsernames.Length)
                {
                    Response.Write(string.Format(_resultTemplate, -1, "userId not found for 1 or more usernames"));
                    return;
                }

                int ret = GetGameFacade.UpdateTeamMembers(_memberUserIds, _objPlacementId);
                string msg = "";

                if (ret > 0)
                {
                    ret = 0;
                    msg = "success";
                }
                else
                {
                    ret = -1;
                    msg = "an error occurred";
                }

                Response.Write(string.Format(_resultTemplate, ret, msg));
            }
            else
            {
                Response.Write(string.Format(_resultTemplate, -1, "error getting team member user ids"));
                return;
            }
        }

        /// <summary>
        /// Process the get team member list action
        /// </summary>
        private void ProcessGetTeamMembersAction()
        {
            // ?action=teamGet&objPlacementId=12345
            // Validate parameters
            if (_objPlacementId <= 0)
            {
                Response.Write(string.Format(_resultTemplate, -1, "invalid input parameter"));
                return;
            }

            DataTable dt = GetGameFacade.GetTeamMembers(_objPlacementId);

            if (dt == null ||dt.Rows.Count == 0)
            {
                Response.Write(string.Format(_resultTemplate, -1, "no team members found"));
                return;
            }

            dt.TableName = "team_members";
            BuildOutputXML(dt);
        }

        /// <summary>
        /// Get user's awards that have not been redeemed
        /// </summary>
        private void ProcessGetUserAwardToRedeemAction()
        {
            // ?action=getAward&zoneInstanceId=1610612751&zoneType=3
            // Validate parameters
            if (_zoneInstanceId <= 0 || _zoneType <= 0)
            {
                Response.Write(string.Format(_resultTemplate, -1, "invalid input parameter"));
                return;
            }

            DataTable dt = GetGameFacade.GetUserLeaderboardAwards(_zoneInstanceId, _zoneType, KanevaWebGlobals.CurrentUser.UserId, KanevaWebGlobals.CurrentUser.Username);

            if (dt == null)
            {
                Response.Write(string.Format(_resultTemplate, -1, "unknown error retrieving awards"));
                return;
            }

            dt.TableName = "user_awards";
            BuildOutputXML(dt);
        }

        /// <summary>
        /// Redeem user's award
        /// </summary>
        private void ProcessRedeemUserAwardAction()
        {
            // ?action=redeemAward&worldLeaderboardId=1
            // Validate parameters
            if (_worldLeaderboardId <= 0)
            {
                Response.Write(string.Format(_resultTemplate, -1, "invalid input parameter"));
                return;
            }

            if (!GetGameFacade.RedeemUserLeaderboardAward(KanevaWebGlobals.CurrentUser.UserId, _worldLeaderboardId))
            {
                Response.Write(string.Format(_resultTemplate, -1, "award not redeemed"));
                return;
            }

            BuildOutputXML(null);
        }

        /// <summary>
        /// Set a leaderboard object name.
        /// </summary>
        private void ProcessSetObjectName()
        {
            // ?action=setObjectName&objPlacementId=1&name=example
            // Validate parameters
            if (_objPlacementId <= 0 || string.IsNullOrWhiteSpace(_name))
            {
                Response.Write(string.Format(_resultTemplate, -1, "invalid input parameter"));
                return;
            }

            if (GetGameFacade.UpdateLeaderboardObjectName(_objPlacementId,_name) <= 0)
            {
                Response.Write(string.Format(_resultTemplate, -1, "name not changed"));
                return;
            }

            Response.Write(string.Format(_resultTemplate, 0, "name changed"));
        }

        /// <summary>
        /// Build the output XML
        /// </summary>
        private void BuildOutputXML(DataTable dt)
        {
            DataSet ds = new DataSet("Result");
            if (dt != null)
            {
                ds.Tables.Add(dt.Copy());
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem;

            if (_objPlacementId > 0)
            {
                elem = doc.CreateElement("objPlacementId");
                elem.InnerText = _objPlacementId.ToString();
                root.InsertBefore(elem, root.FirstChild);
            }

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Build the output XML for the Current Leaderboard
        /// </summary>
        private void BuildCurrentLeaderboardXML(DataTable dtGameObjects, DataTable dtTeamMembers)
        {
            if (dtGameObjects == null)
            {
                Response.Write(string.Format(_resultTemplate, -1, "unable to generate leaderboard"));
                return;
            }

            DataSet ds = new DataSet("Result");

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem;
            string outXml = "";

            foreach (DataRow r in dtGameObjects.Rows)
            {
                elem = doc.CreateElement("current_leaderboard");
                outXml = "<name>" + r["name"] + "</name>" +
                    "<obj_placement_id>" + r["obj_placement_id"] + "</obj_placement_id>" +
                    "<user_id>" + r["user_id"] + "</user_id>" +
                    "<points>" + r["points"] + "</points>" +
                    "<team_members>";

                if (dtTeamMembers != null)
                {
                    DataRow[] drMembers = dtTeamMembers.Select("obj_placement_id = " + r["obj_placement_id"]);
                    foreach (DataRow member in drMembers)
                    {
                        outXml += "<team_member>" +
                                      "<user_id>" + member["kaneva_user_id"] + "</user_id>" +
                                      "<username>" + member["username"] + "</username>" +
                                      "<thumbnail_small_path>" + member["thumbnail_small_path"] + "</thumbnail_small_path>" +
                                      "<thumbnail_square_path>" + member["thumbnail_square_path"] + "</thumbnail_square_path>" +
                                  "</team_member>";
                    }
                }

                outXml += "</team_members>";

                elem.InnerXml = outXml;
                root.InsertAfter(elem, root.LastChild);
            }

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Build the output XML for the Final Leaderboard
        /// </summary>
        private void BuildFinalLeaderboardXML(DataTable dtGameObjects, DataTable dtTeamMembers)
        {
            if (dtGameObjects == null)
            {
                Response.Write(string.Format(_resultTemplate, -1, "unable to generate leaderboard"));
                return;
            }

            DataSet ds = new DataSet("Result");

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem;
            string outXml = "";

            foreach (DataRow r in dtGameObjects.Rows)
            {
                elem = doc.CreateElement("final_leaderboard");
                outXml = "<ranking>" + r["ranking"] + "</ranking>" +
                    "<points>" + r["points"] + "</points>" +
                    "<obj_placement_id>" + r["obj_placement_id"] + "</obj_placement_id>" +
                    "<name>" + r["name"] + "</name>" +
                    "<created_date>" + r["created_date"] + "</created_date>" +
                    "<team_members>";

                if (dtTeamMembers != null)
                {
                    DataRow[] drMembers = dtTeamMembers.Select("world_leaderboard_id = " + r["world_leaderboard_id"]);
                    foreach (DataRow member in drMembers)
                    {
                        outXml += "<team_member>" +
                                      "<user_id>" + member["kaneva_user_id"] + "</user_id>" +
                                      "<username>" + member["username"] + "</username>" +
                                      "<thumbnail_small_path>" + member["thumbnail_small_path"] + "</thumbnail_small_path>" +
                                      "<thumbnail_square_path>" + member["thumbnail_square_path"] + "</thumbnail_square_path>" +
                                  "</team_member>";
                    }
                }

                outXml += "</team_members>";

                elem.InnerXml = outXml;
                root.InsertAfter(elem, root.LastChild);
            }

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }
    }
}