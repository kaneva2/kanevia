///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for texturesList.
    /// </summary>
    public class visitPlace : KgpBasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(false))
            {
                if (Request.Params["action"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                string actionreq = Request.Params["action"];


                if (actionreq.Equals("visitPlace"))
                {
                    if ((Request.Params["zoneInstanceId"] == null) || (Request.Params["zoneType"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>zoneInstanceId or zoneType not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int zoneInstanceId = Int32.Parse(Request.Params["zoneInstanceId"].ToString());
                    int zoneType = Int32.Parse(Request.Params["zoneType"].ToString());

                    if (!zoneType.Equals(Constants.BROADBAND_ZONE) && !zoneType.Equals(Constants.APARTMENT_ZONE))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Only Zone types home and apartment supported</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int ret = UsersUtility.UpdatePlaceVisit(m_userId, zoneInstanceId, zoneType);

                    XmlDocument doc = new XmlDocument();
                    XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");

                    XmlElement elem = doc.CreateElement("ReturnCode");

                    elem.InnerText = ret.ToString();

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnDescription");

                    if (ret == -1)
                    {
                        elem.InnerText = "Already a unique Visit.";
                    }
                    else
                    {
                        elem.InnerText = "success";
                    }

                    root.InsertBefore(elem, root.FirstChild);
                    Response.Write(root.OuterXml);
                }
                else if (actionreq.Equals("visitDanceFloor"))
                {
                    if (Request.Params["objectId"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>objectId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int objectId = Int32.Parse(Request.Params["objectId"].ToString());

                    int ret = UsersUtility.UpdateDanceFloorVisit(m_userId, objectId);

                    XmlDocument doc = new XmlDocument();
                    XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");

                    XmlElement elem = doc.CreateElement("ReturnCode");

                    elem.InnerText = ret.ToString();

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnDescription");

                    if (ret == -1)
                    {
                        elem.InnerText = "Already a unique Visit.";
                    }
                    else
                    {
                        elem.InnerText = "success";
                    }

                    root.InsertBefore(elem, root.FirstChild);
                    Response.Write(root.OuterXml);
                }
                else
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action request unknown</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
