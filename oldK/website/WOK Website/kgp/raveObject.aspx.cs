///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// WOK Web call that gives the user the ability to rave an object within WOK
    /// </summary>

    public class raveObject : KgpBasePage
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (CheckUserId(false))
            {
                if (Request.Params["action"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Action not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                if (Request.Params["globalId"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Global ID not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                string actionreq = Request.Params["action"];
                int globalId = Convert.ToInt32(Request.Params["globalId"]);

                if (actionreq.Equals("SetRave"))
                {
                    // Currently, an item can only be normally raved.  i.e. no rave plus or mega-rave
                    RaveType.eRAVE_TYPE rt = RaveType.eRAVE_TYPE.SINGLE;
                    int numRaves = 0;
                    
                    // Do the rave
                    int ret = UsersUtility.UpdateItemRave(m_userId, globalId, rt, ref numRaves);

                    XmlDocument doc = new XmlDocument();
                    XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");

                    // 0 is success for the return code
                    XmlElement elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = ret.ToString();
                    root.InsertBefore(elem, root.FirstChild);

                    // create the return description based on what user utility returned
                    elem = doc.CreateElement("ReturnDescription");
                    if (ret == 0)
                    {
                        elem.InnerText = "success";
                    }
                    else if (ret == -1)
                    {
                        elem.InnerText = "User has already raved this item.";
                    }
                    else if (ret == -2)
                    {
                        elem.InnerText = "An error occurred when attempting to insert rave into the database.";
                    }
                    else
                    {
                        elem.InnerText = "Unknown error occurred.";
                    }
                    root.InsertBefore(elem, root.FirstChild);

                    // Informational rave count returned to caller, includes the rave that was just applied
                    elem = doc.CreateElement("NumRaves");
                    elem.InnerText = numRaves.ToString();
                    root.InsertBefore(elem, root.FirstChild);

                    Response.Write(root.OuterXml);
                }
                else if (actionreq.Equals("GetRaves"))
                {
                    XmlDocument doc = new XmlDocument();
                    XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");

                    int numRavesTotal = UsersUtility.GetItemRaveCount(globalId);
                    int numRavesByUser = UsersUtility.GetItemRaveCountByUser(m_userId, globalId);

                    // 0 is success for the return code
                    XmlElement elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = "0";
                    root.InsertAfter(elem, root.LastChild);

                    // create the return description based on what user utility returned
                    elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = "success";
                    root.InsertAfter(elem, root.LastChild);

                    elem = doc.CreateElement("rave_count");
                    elem.InnerText = numRavesTotal.ToString();
                    root.InsertAfter(elem, root.LastChild);

                    elem = doc.CreateElement("player_rave_count");
                    elem.InnerText = numRavesByUser.ToString();
                    root.InsertAfter(elem, root.LastChild);

                    Response.Write(root.OuterXml);
                }
                else
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-3</ReturnCode>\r\n  <ResultDescription>action request unknown</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }
            }
            else
            {
                string errorStr = "<Result>\r\n<ReturnCode>-4</ReturnCode>\r\n<ResultDescription>User must be in world to use this web call</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
