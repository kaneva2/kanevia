///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class getItemAnimations : KgpBasePage
    {
        private void Page_Load (object sender, System.EventArgs e)
        {
            string wokDb = KanevaGlobals.DbNameKGP;

            Response.Cache.SetNoServerCaching ();
            Response.Cache.SetNoStore ();

            if (true) // TODO need to validate which STAR is requesting this data, then validate this player KEPAuth'd to it
            {
                if (Request.Params["id"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>provide correct id</ReturnDescription>\r\n</Result>";
                    Response.Write (errorStr);
                    return;
                }

                int id = Int32.Parse (Request.Params["id"].ToString ());
   //             int ugcId = 0;
   //             int templateId = 0;
   //             if (Request.Params["ugcId"] != null && Request.Params["templateId"] != null)
   //             {
   //                 ugcId = Int32.Parse (Request.Params["ugcId"].ToString ());
   //                 templateId = Int32.Parse (Request.Params["templateId"].ToString ());
   //                 if (ugcId < WOKItem.UGC_BASE_GLID - 1 || templateId < WOKItem.UGC_BASE_TEMPLATE_GLID - 1)
   //                 {
   //                     string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>invalid range</ReturnDescription>\r\n</Result>";
   //                     Response.Write (errorStr);
   //                     return;
    //                }
    //            }
   //             int count = 1;
   //             if (Request.Params["count"] != null)
   //             {
   //                 Int32.TryParse (Request.Params["count"], out count);
   //             }

                ShoppingFacade shoppingFacade = new ShoppingFacade ();

                DataTable dt = shoppingFacade.GetItemAnimations (id, true);
                dt.TableName = "items";

                // result set
                //DataSet ds = new DataSet();
                DataSet ds = dt.DataSet;
                ds.DataSetName = "Result";
                //ds.Tables.Add(dt);

                // setup the result in the header
                XmlDocument doc = new XmlDocument ();
                doc.LoadXml (ds.GetXml ());
                XmlNode root = doc.DocumentElement;

                XmlElement elem = doc.CreateElement ("ReturnDescription");
                elem.InnerText = "success";

                root.InsertBefore (elem, root.FirstChild);

                elem = doc.CreateElement ("ReturnCode");
                elem.InnerText = "0";

                root.InsertBefore (elem, root.FirstChild);

                Response.Write (root.OuterXml);
            }
        }
    }
}