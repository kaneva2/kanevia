///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for texturesList.
	/// </summary>
	public class ravePlace : KgpBasePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
				if ( Request.Params["action"] == null)
				{
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>action not specified</ReturnDescription>\r\n</Result>";
					Response.Write(errorStr);
					return;
				}

				string actionreq = Request.Params["action"];

				if ( actionreq.Equals("GetPlaceInformation") )
				{
                    DataTable placeDataTable = UsersUtility.GetCurrentPlaceInfo(m_userId);
                    if (placeDataTable != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");

                        XmlElement elem = doc.CreateElement("ReturnCode");
                        doc.InsertAfter(root, doc.LastChild);
                        elem.InnerText = "0";
                        root.InsertAfter(elem, root.LastChild);
                        
                        elem = doc.CreateElement("ReturnDescription");
                        elem.InnerText = "success";
                        root.InsertAfter(elem, root.LastChild);

                        // Create the dataset from the datatable
                        DataSet ds = new DataSet();
                        ds.DataSetName = "Records";
                        placeDataTable.TableName = "Place";

                        // Add modified datatable to dataset
                        ds.Tables.Add(placeDataTable);
                        
                        // Build the XML document fragment to add to the root node
                        XmlDocumentFragment docFrag = doc.CreateDocumentFragment();
                        docFrag.InnerXml = ds.GetXml();
                        root.AppendChild(docFrag);

                        Response.Write(doc.InnerXml);
                    }
                    else
                    {
                        XmlDocument doc = new XmlDocument();
                        XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");
                        XmlElement elem = doc.CreateElement("ReturnCode");
                        doc.InsertAfter(root, doc.LastChild);

                        elem.InnerText = "-1";
                        root.InsertBefore(elem, root.LastChild);

                        Response.Write(doc.InnerXml);
                    }
			    }
				else if ( actionreq.Equals("SetRave") )
				{
                    if (UsersUtility.UpdatePlaceRave(m_userId, 1, WebCache.GetRaveType(RaveType.eRAVE_TYPE.SINGLE)) != 0)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>User has already raved this place</ReturnDescription>\r\n</Result>";
                        Response.Write(errorStr);
                    }
                    else
                    {
                        XmlDocument doc = new XmlDocument();
                        XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");
                        doc.InsertAfter(root, doc.LastChild);
                        
                        XmlElement elem = doc.CreateElement("ReturnCode");
                        elem.InnerText = "0";
                        root.InsertAfter(elem, root.LastChild);
                        
                        elem = doc.CreateElement("ReturnDescription");
                        elem.InnerText = "success";
                        root.InsertAfter(elem, root.LastChild);

                        Response.Write(doc.InnerXml);
                    }
				}
                else if (actionreq.Equals("RequestAccess"))
                {
                    int communityId = Convert.ToInt32(string.IsNullOrWhiteSpace(Request.Params["communityId"]) ? "0" : Request.Params["communityId"]);

                    if (communityId <= 0)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>CommunityId was invalid.</ReturnDescription>\r\n</Result>";
                        Response.Write(errorStr);
                    }
                    else
                    {
                        GetCommunityFacade.JoinCommunity(communityId, m_userId);

                        XmlDocument doc = new XmlDocument();
                        XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");
                        doc.InsertAfter(root, doc.LastChild);

                        XmlElement elem = doc.CreateElement("ReturnCode");
                        elem.InnerText = "0";
                        root.InsertAfter(elem, root.LastChild);

                        elem = doc.CreateElement("ReturnDescription");
                        elem.InnerText = "success";
                        root.InsertAfter(elem, root.LastChild);

                        Response.Write(doc.InnerXml);
                    }
                }
                else if (actionreq.Equals("AcceptTestUserAccess"))
                {
                    // Confirm user permissions.
                    if (!GetUserFacade.IsUserAutomatedTestAccount(KanevaWebGlobals.CurrentUser))
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>Unauthorized user.</ReturnDescription>\r\n</Result>");
                        return;
                    }

                    // Confirm that this call is active on this environment.
                    if (!Configuration.AutomatedTestUserGenerationEnabled)
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>Unauthorized action.</ReturnDescription>\r\n</Result>");
                        return;
                    }

                    bool revoke = (!string.IsNullOrWhiteSpace(Request.Params["revoke"]) && Request.Params["revoke"] == "1");

                    int communityId = GetCommunityFacade.GetCommunityIdFromName(string.IsNullOrWhiteSpace(Request.Params["worldName"]) ? "Testinvalidname" : Request.Params["worldName"], false);

                    if (communityId <= 0)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>CommunityId was invalid.</ReturnDescription>\r\n</Result>";
                        Response.Write(errorStr);
                    }
                    else
                    {
                        if (revoke)
                        {
                            if (GetCommunityFacade.GetCommunityMember(communityId, m_userId).StatusId != (uint)CommunityMember.CommunityMemberStatus.ACTIVE)
                            {
                                string errorStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ReturnDescription>Success</ReturnDescription>\r\n</Result>";
                                Response.Write(errorStr);
                            }
                            else
                            {
                                GetCommunityFacade.UpdateCommunityMember(communityId, m_userId, (int)CommunityMember.CommunityMemberStatus.DELETED);
                                if (GetCommunityFacade.GetCommunityMember(communityId, m_userId).StatusId != (uint)CommunityMember.CommunityMemberStatus.ACTIVE)
                                {
                                    string errorStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ReturnDescription>Success</ReturnDescription>\r\n</Result>";
                                    Response.Write(errorStr);
                                }
                                else
                                {
                                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>Unable to update community member.</ReturnDescription>\r\n</Result>";
                                    Response.Write(errorStr);
                                }
                            }
                        }
                        else
                        {
                            if (GetCommunityFacade.GetCommunityMember(communityId, m_userId).StatusId == (uint)CommunityMember.CommunityMemberStatus.ACTIVE)
                            {
                                string errorStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ReturnDescription>Success</ReturnDescription>\r\n</Result>";
                                Response.Write(errorStr);
                            }
                            else
                            {
                                GetCommunityFacade.JoinCommunity(communityId, m_userId);
                                GetCommunityFacade.UpdateCommunityMember(communityId, m_userId, (int)CommunityMember.CommunityMemberStatus.ACTIVE);
                                if (GetCommunityFacade.GetCommunityMember(communityId, m_userId).StatusId == (uint)CommunityMember.CommunityMemberStatus.ACTIVE)
                                {
                                    string errorStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ReturnDescription>Success</ReturnDescription>\r\n</Result>";
                                    Response.Write(errorStr);
                                }
                                else
                                {
                                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>Unable to update community member.</ReturnDescription>\r\n</Result>";
                                    Response.Write(errorStr);
                                }
                            }
                        }
                    }
                }
                else if (actionreq.Equals("MakeTestUserModerator"))
                {
                    // Confirm user permissions.
                    if (!GetUserFacade.IsUserAutomatedTestAccount(KanevaWebGlobals.CurrentUser))
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>Unauthorized user.</ReturnDescription>\r\n</Result>");
                        return;
                    }

                    // Confirm that this call is active on this environment.
                    if (!Configuration.AutomatedTestUserGenerationEnabled)
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>Unauthorized action.</ReturnDescription>\r\n</Result>");
                        return;
                    }

                    bool revoke = (!string.IsNullOrWhiteSpace(Request.Params["revoke"]) && Request.Params["revoke"] == "1");

                    int communityId = GetCommunityFacade.GetCommunityIdFromName(string.IsNullOrWhiteSpace(Request.Params["worldName"]) ? "Testinvalidname" : Request.Params["worldName"], false);

                    if (communityId <= 0)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>CommunityId was invalid.</ReturnDescription>\r\n</Result>";
                        Response.Write(errorStr);
                    }
                    else
                    {
                        if (revoke)
                        {
                            if (GetCommunityFacade.GetCommunityMember(communityId, m_userId).AccountTypeId != (uint)CommunityMember.CommunityMemberAccountType.MODERATOR)
                            {
                                string errorStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ReturnDescription>Success</ReturnDescription>\r\n</Result>";
                                Response.Write(errorStr);
                            }
                            else
                            {
                                GetCommunityFacade.UpdateCommunityMember(communityId, m_userId, CommunityMember.CommunityMemberAccountType.SUBSCRIBER);
                                if (GetCommunityFacade.GetCommunityMember(communityId, m_userId).AccountTypeId != (uint)CommunityMember.CommunityMemberAccountType.MODERATOR)
                                {
                                    string errorStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ReturnDescription>Success</ReturnDescription>\r\n</Result>";
                                    Response.Write(errorStr);
                                }
                                else
                                {
                                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>Unable to update community member.</ReturnDescription>\r\n</Result>";
                                    Response.Write(errorStr);
                                }
                            }
                        }
                        else
                        {
                            if (GetCommunityFacade.GetCommunityMember(communityId, m_userId).AccountTypeId == (uint)CommunityMember.CommunityMemberAccountType.MODERATOR)
                            {
                                string errorStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ReturnDescription>Success</ReturnDescription>\r\n</Result>";
                                Response.Write(errorStr);
                            }
                            else
                            {
                                GetCommunityFacade.JoinCommunity(communityId, m_userId);
                                GetCommunityFacade.UpdateCommunityMember(communityId, m_userId, CommunityMember.CommunityMemberAccountType.MODERATOR);
                                if (GetCommunityFacade.GetCommunityMember(communityId, m_userId).AccountTypeId == (uint)CommunityMember.CommunityMemberAccountType.MODERATOR)
                                {
                                    string errorStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ReturnDescription>Success</ReturnDescription>\r\n</Result>";
                                    Response.Write(errorStr);
                                }
                                else
                                {
                                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>Unable to update community member.</ReturnDescription>\r\n</Result>";
                                    Response.Write(errorStr);
                                }
                            }
                        }
                    }
                }
                else
				{
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>action request unknown</ReturnDescription>\r\n</Result>";
					Response.Write(errorStr);
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
