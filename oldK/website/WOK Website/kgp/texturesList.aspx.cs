///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for texturesList.
    /// </summary>
    public class texturesList : KgpBasePage
    {
        #region Declarations

        private string _action = null;
        private string _searchString;
        private int _start;
        private int _max;
        private bool _inAccessPassZone;

        // Response types
        private string _errorResult = "<Result>\r\n  <ReturnCode>{0}</ReturnCode>\r\n  <ResultDescription>{1}</ResultDescription>\r\n </Result>";

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                bool inGame = false;
                _inAccessPassZone = false;
                if (CheckUserId(false, true, ref inGame, ref _inAccessPassZone))
                {
                    // Get parameters
                    GetRequestParams();

                    // Branch via report type
                    switch (_action)
                    {
                        case "searchMedia":
                            ProcessSearchMediaAction();
                            break;
                        default:
                            ProcessDefaultAction();
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Sets page variables using either passed in values or appropriate defaults.
        /// </summary>
        private void GetRequestParams()
        {
            int intOut = 0;

            _action         = Request.Params["action"];
            _searchString   = Request.Params["ss"] ?? string.Empty;
            _start          = Int32.TryParse(Request.Params["start"], out intOut) ? intOut : 0;
            _max            = Int32.TryParse(Request.Params["max"], out intOut) ? intOut : 0;
        }

        /// <summary>
        /// Processes the searchMedia action.
        /// </summary>
        private void ProcessSearchMediaAction()
        {
            // Parse params that aren't shared.
            bool bOut;
            bool thumbnailRequired = Boolean.TryParse(Request.Params["thumbnailRequired"], out bOut) ? bOut : false;


            // Validate parameters.
            if (_start <= 0 || _max <= 0)
            {
                Response.Write(string.Format(_errorResult, -1, "page number and items per page not specified"));
                return;
            }

            // If showing the first page, the first slot should be blank. 
            _max = _start == 1 ? _max - 1 : _max;

            MediaFacade mediaFacade = new MediaFacade();
            PagedList<Asset> searchResults;

            if (string.IsNullOrWhiteSpace(_searchString))
            {
                searchResults = mediaFacade.GetUserUploadedAssets(KanevaWebGlobals.CurrentUser.UserId,
                    new List<int> { (int)Asset.eASSET_TYPES.PATTERN, (int)Asset.eASSET_TYPES.PICTURE },
                    "a.created_date desc", _start, _max);
            }
            else
            {
                // We're only looking for images uploaded by the user.
                List<int> uploads = mediaFacade.GetUserUploadedAssetIds(KanevaWebGlobals.CurrentUser.UserId);

                searchResults = mediaFacade.SearchMedia(KanevaWebGlobals.CurrentUser.HasAccessPass,
                    new List<int> { (int)Asset.eASSET_TYPES.PATTERN, (int)Asset.eASSET_TYPES.PICTURE },
                    false, thumbnailRequired, Server.HtmlEncode(_searchString),
                    string.Empty, 0, string.Empty, _start, _max, false, uploads);
            }

            if (_start == 1 && searchResults.Count > 0)
            {
                searchResults.Insert(0, new Asset());
            }

            PagedDataTable pdt = ConvertPLToPDT(searchResults);
            pdt.TableName = "Texture";

            DataSet ds = new DataSet();
            ds.DataSetName = "Result";
            ds.Tables.Add(pdt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            int numRecords = pdt.Rows.Count;
            XmlElement elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = pdt.TotalCount;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Processes the default webcall action if no other is specified.
        /// </summary>
        private void ProcessDefaultAction()
        {
            if (_start <= 0 || _max <= 0)
            {
                Response.Write(string.Format(_errorResult, -1, "page number and items per page not specified"));
                return;
            }

            // Only show patterns by default. Show pics only on command.
            bool wantPatterns = (Request.Params["wantPics"] == null || !Request.Params["wantPics"].Equals("true"));
            bool wantPictures = (Request.Params["wantPics"] != null && Request.Params["wantPics"].Equals("true"));

            int channelId = KanevaWebGlobals.CurrentUser.CommunityId;
            int playListId = -1;
            int assetId = -1;
            int categoryId = -1;
            if (Request.Params["type"] != null)
            {
                try
                {
                    /*
					if ( Int32.Parse(Request.Params["type"]) == (int)Constants.eASSET_TYPE.PICTURE )
						wantPatterns = false;
					else if ( Int32.Parse(Request.Params["type"]) == (int)Constants.eASSET_TYPE.PATTERN )
						wantPictures = false;
                    */
                    if (Request.Params["type"] == "f")
                    {
                        channelId = GetChannelId(Request.Params["channelId"]);
                    }
                    else if (Request.Params["type"] == "s")
                    {
                        if (Request.Params["channelId"] == "-1")
                        {
                            channelId = KanevaWebGlobals.CurrentUser.CommunityId;
                        }
                        else
                        {
                            channelId = -1;
                        }

                        if (Request.Params["pId"] != null)
                        {
                            playListId = Convert.ToInt32(Request.Params["pId"]);
                        }

                    }
                    else if (Request.Params["type"] == "c")
                    {
                        string catName = (Request.Params["category"] ?? "N/A");
                        MediaFacade mediaFacade = new MediaFacade();
                        categoryId = mediaFacade.GetAssetCategoryIdByName((int)Constants.eASSET_TYPE.PATTERN, catName);
                    }
                    else if (Request.Params["type"] == "a")
                    {
                        assetId = Int32.Parse(Request.Params["assetId"]);
                        StoreUtility.InsertAssetChannel(assetId, KanevaWebGlobals.CurrentUser.CommunityId);
                    }
                }
                catch (FormatException)
                {
                }
            }

            if (assetId == -1)
            {
                DataSet ds = new DataSet();

                ds.DataSetName = "Result";

                PagedDataTable pdt;

                string orderby = "created_date" + " " + "ASC";
                //get jpg only
                string filter = "a.content_extension LIKE '%.JPG' AND a.status_id <> " +
                    (int)Constants.eASSET_STATUS.MARKED_FOR_DELETION;

                if (Request.Params["type"] == "c")
                {
                    pdt = getCategoryItems(_start, _max, categoryId);
                }
                else if (_searchString.Length == 0)
                {
                    orderby = "sort_order asc, ac.created_date desc";

                    pdt = StoreUtility.GetAssetsInChannelForGame(channelId, _inAccessPassZone,
                        wantPictures, wantPatterns, filter, orderby, _start, _max);
                }
                else
                {
                    pdt = getSearchItems(_start, _max, _searchString, channelId, playListId, wantPictures);
                }


                pdt.TableName = "Texture";
                ds.Tables.Add(pdt);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(ds.GetXml());
                XmlNode root = doc.DocumentElement;

                int numRecords = pdt.Rows.Count;
                XmlElement elem = doc.CreateElement("NumberRecords");
                elem.InnerText = numRecords.ToString();

                root.InsertBefore(elem, root.FirstChild);

                int totalNumRecords = pdt.TotalCount;
                elem = doc.CreateElement("TotalNumberRecords");
                elem.InnerText = totalNumRecords.ToString();

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("ReturnDescription");
                elem.InnerText = "success";

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("ReturnCode");
                elem.InnerText = "0";

                root.InsertBefore(elem, root.FirstChild);

                Response.Write(root.OuterXml);
            }
        }

        private PagedDataTable getCategoryItems(int page, int items_per_page, int categoryId)
        {
            string catIds = (categoryId > 0 ? categoryId.ToString() : "");

            MediaFacade mediaFacade = new MediaFacade();
            PagedList<Asset> plAsset = mediaFacade.BrowseMedia("asset_raves_srch_idx", KanevaWebGlobals.CurrentUser.HasAccessPass, (int)Constants.eASSET_TYPE.PATTERN,
                        false, true, catIds, 30, "raves DESC", page, items_per_page, false);

            return ConvertPLToPDT(plAsset);
        }

        private PagedDataTable getSearchItems(int page, int items_per_page, string searchString, int channelId, int playListId, bool wantPictures)
        {
            MediaFacade mediaFacade = new MediaFacade();
            PagedList<Asset> plAsset = new PagedList<Asset>();

            int assetTypeId = (wantPictures ? (int) Constants.eASSET_TYPE.PICTURE : (int) Constants.eASSET_TYPE.PATTERN);

            if (channelId == -1)
            {
                plAsset = mediaFacade.SearchMedia(KanevaWebGlobals.CurrentUser.HasAccessPass, assetTypeId,
                   false, false, Server.HtmlEncode(searchString),
                   "", 0, "", page, items_per_page, false, null);
            }
            else
            {
                string sql = " select asset_id from kaneva.asset_channels ac where channel_id = " + channelId;

                if (playListId > 0)
                {
                    sql = " SELECT asset_id FROM kaneva.asset_group_assets where asset_group_id = " + playListId;
                }

                DataTable dt = KanevaGlobals.GetDatabaseUtility().GetDataTable(sql);

                List<int> iAssetList = new List<int>(dt.Rows.Count);

                //string assetClause = "";
                if (dt.Rows.Count > 0)
                {
                   // assetClause = " IN (";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        iAssetList.Add((int)Convert.ToInt32(dt.Rows[i]["asset_id"]));
                       // assetClause += dt.Rows[i]["asset_id"] + ", ";
                    }
                    //assetClause = assetClause.Substring(0, assetClause.Length - 2) + ") ";
                }

                plAsset = mediaFacade.SearchMedia(KanevaWebGlobals.CurrentUser.HasAccessPass, assetTypeId,
                    true, false, Server.HtmlEncode(searchString),
                    "", 0, "", page, items_per_page, false, iAssetList);

            }

            return ConvertPLToPDT(plAsset);
        }

        private DataRow ConvertAssetToDataRow(PagedDataTable pdt, Asset asset)
        {
            DataRow drAsset = pdt.NewRow();
            drAsset["file_size"] = asset.FileSize;
            drAsset["content_extension"] = asset.ContentExtension;
            drAsset["asset_id"] = asset.AssetId;
            drAsset["asset_type_id"] = asset.AssetTypeId;
            drAsset["name"] = asset.Name;
            drAsset["short_description"] = asset.ShortDescription;
            drAsset["created_date"] = asset.CreatedDate;
            drAsset["thumbnail_medium_path"] = asset.ThumbnailMediumPath;
            drAsset["image_full_path"] = asset.ImageFullPath;
            drAsset["username"] = asset.OwnerUsername;
            return drAsset;
        }

        private PagedDataTable ConvertPLToPDT(PagedList<Asset> plAsset)
        {
            PagedDataTable pdtResult = new PagedDataTable();
            pdtResult.TotalCount = (int)plAsset.TotalCount;
            pdtResult.Columns.Add(new DataColumn("file_size"));
            pdtResult.Columns.Add(new DataColumn("content_extension"));
            pdtResult.Columns.Add(new DataColumn("asset_id"));
            pdtResult.Columns.Add(new DataColumn("asset_type_id"));
            pdtResult.Columns.Add(new DataColumn("name"));
            pdtResult.Columns.Add(new DataColumn("short_description"));
            pdtResult.Columns.Add(new DataColumn("created_date"));
            pdtResult.Columns.Add(new DataColumn("thumbnail_medium_path"));
            pdtResult.Columns.Add(new DataColumn("image_full_path"));
            pdtResult.Columns.Add(new DataColumn("username"));

            foreach (Asset asset in plAsset)
            {
                pdtResult.Rows.Add(ConvertAssetToDataRow(pdtResult, asset));
            }

            return pdtResult;
        }

        private int GetChannelId(string nameNoSpaces)
        {
            string sqlSelect = "SELECT community_id " +
                               " FROM communities " +
                               " WHERE name_no_spaces = @name";

            Hashtable parameters = new Hashtable();
            parameters.Add("@name", nameNoSpaces);
            DataRow drResult = KanevaGlobals.GetDatabaseUtility().GetDataRow(sqlSelect, parameters, false);

            if (drResult != null && !drResult["community_id"].Equals(DBNull.Value))
            {
                return Convert.ToInt32(drResult["community_id"]);
            }
            return 0;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
