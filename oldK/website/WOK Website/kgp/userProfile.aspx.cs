///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;
using log4net;
using System.Security.Cryptography;
using System.Web.Security;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for userProfile.
    /// </summary>
    public class userProfile : KgpBasePage
    {
        #region Declarations

        // Shared report params
        private string _action = null;

        // Response types
        private string _errorResult = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>{0}</ResultDescription>\r\n</Result>";

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            if ((Request.Params["action"] ?? string.Empty).Equals("generateTestUser") || CheckUserId(false))
            {
                // Get parameters
                GetRequestParams();

                // Branch by action.
                switch (_action)
                {
                    case "getUser":
                        ProcessGetUser();
                        break;
                    case "getUserLocation":
                        ProcessGetUserLocation();
                        break;
                    case "getUserBalance":
                        ProcessGetUserBalance();
                        break;
                    case "getUserBalanceLastLogin":
                        ProcessGetUserBalanceLastLogin();
                        break;
                    case "getOwnershipSummary":
                        ProcessGetOwnershipSummary();
                        break;
                    case "getUserIdFromAvatarName":
                        ProcessGetUserIdFromAvatarName();
                        break;
                    case "getUserLoginInfoFromUsername":
                        ProcessGetUserLoginInfoFromUsername();
                        break;
                    case "getUserIdFromId":
                        ProcessGetUserIdFromId();
                        break;
                    case "getUserProfile":
                        ProcessGetUserProfile();
                        break;
                    case "getUserInterests":
                        ProcessGetUserInterests();
                        break;
                    case "sendDanceFameBlast":
                        ProcessSendDanceFameBlast();
                        break;
                    case "danceToLevel10":
                        ProcessDanceToLevel10();
                        break;
                    case "getMyCounts":
                        ProcessGetMyCounts();
                        break;
                    case "getTotalBlastsFromNow":
                        ProcessGetTotalBlastsFromNow();
                        break;
                    case "getCrewPeeps":
                        ProcessGetCrewPeeps();
                        break;
                    case "getUserPreferences":
                        ProcessGetUserPreferences();
                        break;
                    case "setUserPreferences":
                        ProcessSetUserPreferences();
                        break;
                    case "getDailyBonusData":
                        ProcessGetDailyBonusData();
                        break;
                    case "resetDailyBonus":
                        ProcessResetDailyBonus();
                        break;
                    case "progressMetrics":
                        ProcessProgressMetrics();
                        break;
                    case "generateTestUser":
                        ProcessGenerateTestUser();
                        break;
                    case "setTestUserToken":
                        ProcessSetTestUserToken();
                        break;
                    default:
                        Response.Write(string.Format(_errorResult, "action not specified"));
                        break;
                }
            }

            m_logger.Debug ("END - userProfile.PageLoad() for UserId = " + m_userId);
        }

        /// <summary>
        /// Sets page variables using either passed in values or appropriate defaults.
        /// </summary>
        private void GetRequestParams()
        {
            _action = Request.Params["action"];
        }

        #region Actions

        /// <summary>
        /// Processes the getUser action.
        /// </summary>
        private void ProcessGetUser()
        {
            if (Request.Params["userId"] == null)
            {
                DataSet ds = new DataSet();
                ds.DataSetName = "Result";

                DataRow dr = GetUserFacade.GetUserObsoleteForWok(m_userId);

                DataTable dt = new DataTable();
                dt = dr.Table.Clone();
                dt.ImportRow(dr);

                dt.TableName = "User";

                ds.Tables.Add(dt);

                Double balance = UsersUtility.getUserBalance(m_userId, Constants.CURR_KPOINT);

                Double giftbalance = UsersUtility.getUserBalance(m_userId, Constants.CURR_GPOINT);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(ds.GetXml());
                XmlNode root = doc.DocumentElement;

                XmlElement elem = doc.CreateElement("Balance");
                elem.InnerText = balance.ToString();

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("GiftBalance");
                elem.InnerText = giftbalance.ToString();

                root.InsertBefore(elem, root.FirstChild);

                // Add GM Info
                int isGM = 0;
                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    WokUserInfo userInfo = (WokUserInfo)HttpContext.Current.Items["WokUserInfo"];
                    isGM = userInfo.IsGm ? 1 : 0;
                }

                elem = doc.CreateElement("GM");
                elem.InnerText = isGM.ToString();
                root.InsertBefore(elem, root.FirstChild);


                int numRecords = 1;
                elem = doc.CreateElement("NumberRecords");
                elem.InnerText = numRecords.ToString();

                root.InsertBefore(elem, root.FirstChild);

                int totalNumRecords = 1;
                elem = doc.CreateElement("TotalNumberRecords");
                elem.InnerText = totalNumRecords.ToString();

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("ReturnDescription");
                elem.InnerText = "success";

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("ReturnCode");
                elem.InnerText = "0";

                root.InsertBefore(elem, root.FirstChild);

                Response.Write(root.OuterXml);
            }
            else
            {
                int userId = Int32.Parse(Request.Params["userId"].ToString());

                DataRow dr = GetUserFacade.GetUserObsoleteForWok(userId);

                if (dr == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userid not found</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                XmlDocument doc = new XmlDocument();
                XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");
                doc.InsertAfter(root, doc.LastChild);

                XmlElement elem = doc.CreateElement("ReturnCode");
                elem.InnerText = "0";
                root.InsertAfter(elem, root.LastChild);

                elem = doc.CreateElement("ReturnDescription");
                elem.InnerText = "success";
                root.InsertAfter(elem, root.LastChild);

                elem = doc.CreateElement("RequestedUserID");
                elem.InnerText = userId.ToString();
                root.InsertAfter(elem, root.LastChild);

                // get profile options for showing gender, location and age
                CommunityFacade cf = new CommunityFacade();
                if (cf != null)
                {
                    int communityId = Convert.ToInt32(dr["community_id"]);
                    Community community = cf.GetCommunity(communityId);

                    if (community != null)
                    {
                        elem = doc.CreateElement("ShowAge");
                        elem.InnerText = community.Preferences.ShowAge.ToString();
                        root.AppendChild(elem);

                        elem = doc.CreateElement("ShowLocation");
                        elem.InnerText = community.Preferences.ShowLocation.ToString();
                        root.AppendChild(elem);

                        elem = doc.CreateElement("ShowGender");
                        elem.InnerText = community.Preferences.ShowGender.ToString();
                        root.AppendChild(elem);

                        if (community.Preferences.ShowGender.ToString() == "True")
                        {
                            elem = doc.CreateElement("gender");
                            elem.InnerText = Convert.ToString(dr["gender"]);
                            root.AppendChild(elem);
                        }

                        if (community.Preferences.ShowAge.ToString() == "True")
                        {
                            elem = doc.CreateElement("age");
                            elem.InnerText = Convert.ToString(dr["age"]);
                            root.AppendChild(elem);
                        }

                        if (community.Preferences.ShowLocation.ToString() == "True")
                        {
                            elem = doc.CreateElement("location");
                            elem.InnerText = Convert.ToString(dr["location"]);
                            root.AppendChild(elem);
                        }

                    }
                }

                elem = doc.CreateElement("under18");
                elem.InnerText = Convert.ToInt32(dr["age"]) < 18 ? "true" : "false";
                root.AppendChild(elem);

                elem = doc.CreateElement("display_name");
                elem.InnerText = Convert.ToString(dr["display_name"]);
                root.AppendChild(elem);

                elem = doc.CreateElement("signup_date");
                elem.InnerText = Convert.ToString(dr["signup_date"]);
                root.AppendChild(elem);

                Response.Write(doc.InnerXml);
            }
        }

        /// <summary>
        /// Processes the getUserLocation action.
        /// </summary>
        private void ProcessGetUserLocation()
        {
            if (Request.Params["userId"] == null)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Missing param userId</ResultDescription><Result>");
            }

            string response = "";
            int userId = Convert.ToInt32(Request.Params["userId"]);

            // Is user in World
            if (UsersUtility.GetCurrentPlaceInfo(userId) != null)
            {
                response = "<InWorld>True</InWorld>";
            }
            else
            {
                response = "<InWorld>False</InWorld>";
            }


            // Is user in KIM?
            response += "<InKIM>False</InKIM>";


            Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Success</ResultDescription>" + response + "</Result>");
        }

        /// <summary>
        /// Processes the getUserBalance action.
        /// </summary>
        private void ProcessGetUserBalance()
        {
            DataSet ds = new DataSet();
            ds.DataSetName = "Result";

            Double balance = UsersUtility.getUserBalance(m_userId, Constants.CURR_KPOINT);

            Double giftbalance = UsersUtility.getUserBalance(m_userId, Constants.CURR_GPOINT);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("Balance");
            elem.InnerText = balance.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("GiftBalance");
            elem.InnerText = giftbalance.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int numRecords = 1;
            elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = 1;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Processes the getUserBalanceLastLogin action.
        /// </summary>
        private void ProcessGetUserBalanceLastLogin()
        {
            DataSet ds = new DataSet();
            ds.DataSetName = "Result";

            Double creditsBalance = UsersUtility.getUserBalanceSinceLastLogin(m_userId, Constants.CURR_KPOINT);

            Double rewardsBalance = UsersUtility.getUserBalanceSinceLastLogin(m_userId, Constants.CURR_GPOINT);

            DateTime secondLastLogon;
            bool ignoreGm, ignoreAdmin;
            UserFacade uf = new UserFacade();
            uf.GetPlayerAdmin(m_userId, out ignoreAdmin, out ignoreGm, out secondLastLogon);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("RewardsSinceLastLogin");
            elem.InnerText = rewardsBalance.ToString();

            root.InsertAfter(elem, root.FirstChild);

            elem = doc.CreateElement("CreditsSinceLastLogin");
            elem.InnerText = creditsBalance.ToString();

            root.InsertAfter(elem, root.FirstChild);

            elem = doc.CreateElement("LastLogon");
            elem.InnerText = secondLastLogon.ToString();

            root.InsertAfter(elem, root.FirstChild);

            int numRecords = 1;
            elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);
            /*
            int totalNumRecords = 1;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);
            */
            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Processes the getOwnershipSummary action.
        /// </summary>
        private void ProcessGetOwnershipSummary()
        {
            DataSet ds = new DataSet();
            ds.DataSetName = "Result";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            // Add GM Info
            int isGM = 0;
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                WokUserInfo userInfo = (WokUserInfo)HttpContext.Current.Items["WokUserInfo"];
                isGM = userInfo.IsGm ? 1 : 0;
            }

            XmlElement elem = doc.CreateElement("GM");
            elem.InnerText = isGM.ToString();

            root.InsertAfter(elem, root.FirstChild);

            int iOwnModerate = 0;

            DataRow drResult = KanevaGlobals.GetDatabaseUtility().GetDataRow("select 1 from community_members cm, communities_public cp where cm.community_id = cp.community_id and account_type_id in (1,2) and cm.status_id = 1 and cm.user_id = " + m_userId + " limit 1;", false);
            if (drResult != null)
            {
                iOwnModerate = 1;
            }

            elem = doc.CreateElement("i_own_moderate");
            elem.InnerText = iOwnModerate.ToString();

            root.InsertAfter(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Processes the getUserIdFromAvatarName action.
        /// </summary>
        private void ProcessGetUserIdFromAvatarName()
        {
            if (Request.Params["avatar"] == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>avatar not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            string avatarname = Request.Params["avatar"];

            DataSet ds = new DataSet();
            ds.DataSetName = "Result";

            DataRow dr = GetUserFacade.GetUserIdPlayerIdFromUsername(avatarname);

            if (dr == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>avatar name not found</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            DataTable dt = new DataTable();
            dt = dr.Table.Clone();
            dt.ImportRow(dr);

            dt.TableName = "User";

            ds.Tables.Add(dt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("RequestedAvatarName");
            elem.InnerText = avatarname;

            root.InsertBefore(elem, root.FirstChild);

            int numRecords = 1;
            elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = 1;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Processes the getUserLoginInfoFromUsername action.
        /// </summary>
        private void ProcessGetUserLoginInfoFromUsername()
        {
            if (Request.Params["avatar"] == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>avatar not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            string avatarname = Request.Params["avatar"];

            DataSet ds = new DataSet();
            ds.DataSetName = "Result";

            DataRow dr = UsersUtility.GetUserLoginInfoFromUsername(avatarname);

            if (dr == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>avatar name not found</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            DataTable dt = dr.Table;

            dt.TableName = "User";

            ds.Tables.Add(dt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            int numRecords = 1;
            XmlElement elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = 1;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Processes the getUserIdFromId action.
        /// </summary>
        private void ProcessGetUserIdFromId()
        {
            if (Request.Params["userId"] == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            int userId = Int32.Parse(Request.Params["userId"].ToString());

            DataSet ds = new DataSet();
            ds.DataSetName = "Result";

            DataRow dr = UsersUtility.GetUser(userId, true);

            if (dr == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userid not found</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            string profile_is_mature = (string)dr["mature_profile"];

            if (!KanevaWebGlobals.CurrentUser.HasAccessPass && profile_is_mature.Equals("1"))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>profile is mature</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            DataTable dt = dr.Table;

            dt.TableName = "User";

            ds.Tables.Add(dt);

            Double balance = UsersUtility.getUserBalance(userId, Constants.CURR_KPOINT);

            Double giftbalance = UsersUtility.getUserBalance(m_userId, Constants.CURR_GPOINT);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("Balance");
            elem.InnerText = balance.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("GiftBalance");
            elem.InnerText = giftbalance.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int numRecords = 1;
            elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = 1;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Processes the getUserProfile action.
        /// </summary>
        private void ProcessGetUserProfile()
        {
            if (Request.Params["userId"] == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            int userId = Int32.Parse(Request.Params["userId"].ToString());

            //DataRow drUser = UsersUtility.GetUser(userId);
            User user = GetUserFacade.GetUser(userId);

            if (user.UserId.Equals(0))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userid not found</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            if (!KanevaWebGlobals.CurrentUser.HasAccessPass && user.MatureProfile)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>profile is mature</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            DataSet ds = new DataSet();
            ds.DataSetName = "Result";

            DataRow dr = UsersUtility.GetUserProfile(userId);

            if (dr == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userid not found</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            DataTable dt = dr.Table;

            dt.TableName = "User";

            ds.Tables.Add(dt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            int numRecords = 1;
            XmlElement elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = 1;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Processes the getUserInterests action.
        /// </summary>
        private void ProcessGetUserInterests()
        {
            if (Request.Params["userId"] == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            int userId = Int32.Parse(Request.Params["userId"].ToString());

            User user = GetUserFacade.GetUser(userId);
            //DataRow drUser = UsersUtility.GetUser(userId);

            if (user.UserId.Equals(0))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userid not found</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            if (!KanevaWebGlobals.CurrentUser.HasAccessPass && user.MatureProfile)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>profile is mature</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            // Get the user's info they requested
            DataTable dtInterests = UsersUtility.GetUserInterests(userId);
            if (dtInterests == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userid not found</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            // Get the common interests of the user requesting
            DataTable dtCommon = null;
            if (userId != m_userId)
            {
                dtCommon = UsersUtility.GetCommonInterests(userId, m_userId);
            }

            // Build the XML
            XmlDocument doc = new XmlDocument();
            XmlElement rootNode = doc.CreateElement("Result");
            doc.AppendChild(rootNode);

            XmlElement userNode = doc.CreateElement("User");
            rootNode.AppendChild(userNode);

            // Add about me section
            if (user.Description.Length > 0)
            {
                XmlElement aboutMeNode = doc.CreateElement("About_Me");
                aboutMeNode.InnerText = user.Description.ToString();
                userNode.AppendChild(aboutMeNode);
            }

            // Interests
            XmlElement interestsNode = doc.CreateElement("Interests");
            userNode.AppendChild(interestsNode);

            XmlElement categoryNode;
            XmlElement interestNode;

            if (dtInterests != null && dtInterests.Rows.Count > 0)
            {
                string category = string.Empty;
                bool match = false;
                int j = 0;

                for (int i = 0; i < dtInterests.Rows.Count; i++)
                {
                    if (category != dtInterests.Rows[i]["category_text"].ToString())
                    {
                        category = dtInterests.Rows[i]["category_text"].ToString();

                        match = false;

                        categoryNode = doc.CreateElement("Category");
                        categoryNode.SetAttribute("name", category);
                        interestsNode.AppendChild(categoryNode);

                        // check to see if current item is also an interest of the
                        // user that is viewing the profile
                        if (dtCommon != null && dtCommon.Rows.Count > 0 && j < dtCommon.Rows.Count &&
                            Convert.ToInt32(dtCommon.Rows[j]["interest_id"]) == Convert.ToInt32(dtInterests.Rows[i]["interest_id"]))
                        {
                            match = true;
                            j++;
                        }

                        if (dtInterests.Rows[i]["interest"] != null && dtInterests.Rows[i]["interest"].ToString().Length > 0)
                        {
                            interestNode = doc.CreateElement("Interest");
                            interestNode.SetAttribute("common", match ? "Y" : "N");
                            interestNode.InnerText = dtInterests.Rows[i]["interest"].ToString();
                            categoryNode.AppendChild(interestNode);
                        }

                        // get all intersts for a particular category
                        while (++i < dtInterests.Rows.Count)
                        {
                            match = false;

                            if (category == dtInterests.Rows[i]["category_text"].ToString())
                            {
                                if (dtCommon != null && dtCommon.Rows.Count > 0 && j < dtCommon.Rows.Count &&
                                    Convert.ToInt32(dtCommon.Rows[j]["interest_id"]) == Convert.ToInt32(dtInterests.Rows[i]["interest_id"]))
                                {
                                    match = true;
                                    j++;
                                }

                                interestNode = doc.CreateElement("Interest");
                                interestNode.SetAttribute("common", match ? "Y" : "N");
                                interestNode.InnerText = dtInterests.Rows[i]["interest"].ToString();
                                categoryNode.AppendChild(interestNode);
                            }
                            else
                            {
                                i--;
                                break;
                            }
                        }

                        if (i == dtInterests.Rows.Count) i--;
                    }
                }
            }

            XmlNode root = doc.DocumentElement;

            int numRecords = 1;
            XmlElement elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = 1;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Processes the sendDanceFameBlast action.
        /// </summary>
        private void ProcessSendDanceFameBlast()
        {
            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(m_userId);

            FameFacade fameFacade = new FameFacade();
            UserFame userFame = fameFacade.GetUserFame(user.UserId, (int)FameTypes.Dance);

            BlastFacade blastFacade = new BlastFacade();
            string strText = user.Username + " reached Dance Fame Level " + userFame.LevelId;

            if (Request.Params["text"] != null)
            {
                strText = Request.Params["text"].ToString();
            }

            //int result = blastFacade.SendBasicBlast(user.UserId, user.Username, user.NameNoSpaces, strText, user.ThumbnailSmallPath);
            int result = blastFacade.SendFameBlast(user.UserId, user.Username, "New Dance Fame Title", strText);

            XmlDocument doc = new XmlDocument();
            XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");

            XmlElement elem = doc.CreateElement("ReturnCode");

            elem.InnerText = result.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = result.ToString();
            root.InsertBefore(elem, root.FirstChild);
            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Processes the danceToLevel10 action.
        /// </summary>
        private void ProcessDanceToLevel10()
        {
            FameFacade fameFacade = new FameFacade();
            int result = fameFacade.InsertDanceToLevel10(m_userId);

            XmlDocument doc = new XmlDocument();
            XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");

            XmlElement elem = doc.CreateElement("ReturnCode");

            elem.InnerText = result.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = result.ToString();
            root.InsertBefore(elem, root.FirstChild);
            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Processes the getMyCounts action.
        /// </summary>
        private void ProcessGetMyCounts()
        {
            int userId = 0;

            if (Request.Params["userId"] != null)
            {
                userId = Int32.Parse(Request.Params["userId"].ToString());
            }
            else
            {
                userId = m_userId;
            }

            DataRow dr = UsersUtility.GetUserCounts(userId);

            //already raved or not
            RaveFacade raveFacade = new RaveFacade();
            bool alreadyRaved = !raveFacade.IsCommunityRaveFree(m_userId, GetUserFacade.GetPersonalChannelId(userId), RaveType.eRAVE_TYPE.SINGLE);

            int status = 0;
            if (alreadyRaved)
            {
                status = 1;
            }

            string returnStr = "<Result>\r\n  <ReturnCode>";
            if (dr != null)
            {
                returnStr += "0</ReturnCode>  <ResultDescription>success</ResultDescription>";
                returnStr += "<NewMessages>" + dr["numberofnewmessages"] + "</NewMessages>";
                returnStr += "<FriendReqs>" + dr["numberofrequests"] + "</FriendReqs>";
                returnStr += "<FriendCount>" + dr["numberoffriends"] + "</FriendCount>";
                returnStr += "<ChannelReqs>" + dr["numberofpendingmembers"] + "</ChannelReqs>";
                returnStr += "<NumberOfViews>" + dr["numberofviews"] + "</NumberOfViews>";
                returnStr += "<NewGifts>" + dr["numbergiftspending"] + "</NewGifts>";
                returnStr += "<GiftsToMe>" + dr["numbergiftsreceived"] + "</GiftsToMe>";
                returnStr += "<GiftsFromMe>" + dr["numbergiftsgiven"] + "</GiftsFromMe>";
                returnStr += "<Raves>" + dr["numberofdiggs"] + "</Raves>";
                returnStr += "<AlreadyRaved>" + status.ToString() + "</AlreadyRaved>";
            }
            else
            {
                returnStr = "2</ReturnCode>\r\n  <ResultDescription>error</ResultDescription>\r\n";
            }
            returnStr += "</Result>";
            Response.Write(returnStr);
        }

        /// <summary>
        /// Processes the getTotalBlastsFromNow action.
        /// </summary>
        private void ProcessGetTotalBlastsFromNow()
        {
            // Now returns total number of active blasts
            // This is done so we may use the new optimized version of the query, and 
            // not have the added overhead of filtering by date. Client code will
            // keep last total count instead of last date, and do a simple substraction
            // to get number of new since last query.
            BlastFacade blastFacade = new BlastFacade();
            PagedList<Blast> plBlasts = blastFacade.GetActiveBlasts(m_userId, KanevaWebGlobals.CurrentUser.UserId, 1, 1);

            string returnStr = "<Result>\r\n  <ReturnCode>";
            returnStr += "0</ReturnCode>  <ResultDescription>success</ResultDescription>";
            returnStr += "<BlastCount>" + plBlasts.TotalCount.ToString() + "</BlastCount>";
            returnStr += "</Result>";
            Response.Write(returnStr);
        }

        /// <summary>
        /// Processes the getCrewPeeps action.
        /// </summary>
        private void ProcessGetCrewPeeps()
        {
            int userId = 0;

            if (Request.Params["userId"] != null)
            {
                userId = Int32.Parse(Request.Params["userId"].ToString());
            }
            else
            {
                userId = m_userId;
            }

            UserFacade userFacade = new UserFacade();
            Crew crew = userFacade.GetCrew(userId);

            if (crew != null)
            {
                CrewResponse response = new CrewResponse();
                response.crew = crew;
                Response.Write(response);

                XmlSerializer s = new XmlSerializer(typeof(CrewResponse));
                Response.Clear();
                s.Serialize(Response.OutputStream, response);
            }
            else
            {
                string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>Crew info not found</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }
        }

        /// <summary>
        /// Processes the getUserPreferences action.
        /// </summary>
        private void ProcessGetUserPreferences()
        {
            DataSet ds = new DataSet();
            ds.DataSetName = "Result";

            Preferences preferences = new UserFacade().GetUserPreferences(m_userId);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("AlwaysPurchaseWithCurrencyType");
            elem.InnerText = preferences.AlwaysPurchaseWithCurrencyType;

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Processes the setUserPreferences action.
        /// </summary>
        private void ProcessSetUserPreferences()
        {
            if (Request.Params["currency"] == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>currency not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            string currency = Request.Params["currency"].ToString();
            UserFacade userFacade = new UserFacade();
            Preferences preferences = userFacade.GetUserPreferences(m_userId);

            if (preferences.UserId.Equals(0)) { preferences.UserId = m_userId; }

            if (currency.Equals(Currency.CurrencyType.CREDITS)) //KPOINT
            {
                preferences.AlwaysPurchaseWithCurrencyType = Currency.CurrencyType.CREDITS;
            }
            else if (currency.Equals(Currency.CurrencyType.REWARDS)) //GPOINT
            {
                preferences.AlwaysPurchaseWithCurrencyType = Currency.CurrencyType.REWARDS;
            }
            else if (currency.Equals("NONE"))
            {
                preferences.AlwaysPurchaseWithCurrencyType = "NOTSET";
            }
            else
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>currency not valid type</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            int status = -1;
            string responseStr = null;

            status = userFacade.UpdateUserPreferences(preferences);

            if (status == 1)
            {
                responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
            }
            else
            {
                responseStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>User preferences NOT updated</ResultDescription>\r\n</Result>";
            }

            Response.Write(responseStr);
        }

        /// <summary>
        /// Processes the getDailyBonusData action.
        /// </summary>
        private void ProcessGetDailyBonusData()
        {
            int loginCount = 0;
            UInt32 visitorRewards = 0;
            int consecutiveLogins = 0;
            int returnCode = 0;
            int gameCount = 0;
            string strResult = "";

            try
            {
                m_logger.Debug("in getDailyBonusData for UserId = " + m_userId);

                // Get last time user redeemed daily login packet so we know how far to go back
                // to check for visitors to their worlds
                DataRow dr = GetFameFacade.GetUserLastDailyLoginPacketRedemption(m_userId);

                string date = ((dr != null && dr["date_last_rewarded"] != null) ? dr["date_last_rewarded"].ToString() : "null");
                m_logger.Debug("GetUserLastDailyLoginPacketRedemption() for UserId = " + m_userId + " Date = " + date);

                // Add this packet to Daily History table.  It will be used to calculate the
                // number of consecutive days a user has logged into WoK.
                GetFameFacade.UpdateDailyHistory(m_userId, (int)PacketId.WORLD_DAILY_SIGNIN, (int)FameTypes.World);

                m_logger.Debug("UpdateDailyHistory() for UserId = " + m_userId);

                // Get Consecutive Day Login Bonuses
                returnCode = GetFameFacade.RedeemPacketLoginRelated(m_userId, (int)FameTypes.World, out consecutiveLogins);

                m_logger.Debug("RedeemPacketLoginRelated() for UserId = " + m_userId);

                // Get Number of Worlds
                int[] communityTypeIds = new int[] { Convert.ToInt32(CommunityType.APP_3D), Convert.ToInt32(CommunityType.COMMUNITY), Convert.ToInt32(CommunityType.HOME) };
                PagedList<Community> plCommunities = GetCommunityFacade.GetUserCommunities(m_userId, communityTypeIds, "c.name ASC", "c.creator_id=" + m_userId + " AND place_type_id<99", 1, 1, false);

                gameCount = (int)plCommunities.TotalCount;

                m_logger.Debug("GetGamesByOwner() for UserId = " + m_userId);

                // 0 = success
                if (returnCode == 0)
                {
                    m_logger.Debug("returnCode == 0 for UserId = " + m_userId);
                    if (gameCount > 0)
                    {
                        m_logger.Debug("usersGames.Count > 0 for UserId = " + m_userId);

                        int count = 0;
                        if (dr != null && dr["date_last_rewarded"] != null)
                        {
                            // Get Visitor Rewards for visits to user's worlds since last login
                            count = GetGameFacade.GetNumberOfVisitorsForGamesByOwner(m_userId, Convert.ToDateTime(dr["date_last_rewarded"]));
                        }

                        m_logger.Debug("GetNumberOfVisitorsForGamesByOwner() for UserId = " + m_userId);

                        // Number of Rewards given per visit is 5
                        visitorRewards = (UInt32)count * 5;

                        if (visitorRewards > 0)
                        {
                            // Add new trans type and test
                            GetUserFacade.AdjustUserBalance(m_userId, Constants.CURR_REWARDS, visitorRewards, (int)TransactionType.eTRANSACTION_TYPES.CASH_TT_WORLD_VISITORS_BONUS);
                            m_logger.Debug("AdjustUserBalance() for UserId = " + m_userId + ", Visitor Reward Amount = " + visitorRewards);
                        }
                    }

                    strResult = "<ResultDescription>Success</ResultDescription>";
                }
                else
                {
                    m_logger.Debug("returnCode != 0 for UserId = " + m_userId);

                    switch (returnCode)
                    {
                        case -2:
                            strResult = "<ResultDescription>Packet Already redeemed</ResultDescription>";
                            break;
                        case -3:
                            strResult = "<ResultDescription>Packet Not Found</ResultDescription>";
                            break;
                        case -4:
                            strResult = "<ResultDescription>No Action taken, did not meet packet requirements</ResultDescription>";
                            break;
                        case -5:
                            strResult = "<ResultDescription>Packet Id not redeemable</ResultDescription>";
                            break;
                        case -6:
                            strResult = "<ResultDescription>User not able to use fame</ResultDescription>";
                            break;
                        case -7:
                            strResult = "<ResultDescription>Error</ResultDescription>";
                            break;
                        case -8:
                            strResult = "<ResultDescription>User is at max redemption</ResultDescription>";
                            break;
                        case -9:
                            strResult = "<ResultDescription>Packet is not active</ResultDescription>";
                            break;
                    }
                }

                // Get users login count
                loginCount = GetUserFacade.GetPlayerLoginCount(m_userId);

                m_logger.Debug("GetPlayerLoginCount() for UserId = " + m_userId);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error in getDailyBonus - ", ex);
                returnCode = -7;
                strResult = "<ResultDescription>Error</ResultDescription>";
            }

            string returnStr = "<Result>\r\n  <ReturnCode>" + returnCode;
            returnStr += "</ReturnCode>  " + strResult;
            returnStr += "<ConsecutiveLogins>" + consecutiveLogins + "</ConsecutiveLogins>";
            returnStr += "<NumberOfWorlds>" + gameCount + "</NumberOfWorlds>";
            returnStr += "<VisitorRewards>" + visitorRewards + "</VisitorRewards>";
            returnStr += "<LoginCount>" + loginCount + "</LoginCount>";
            returnStr += "</Result>";


            Response.Write(returnStr);
        }

        /// <summary>
        /// Processes the resetDailyBonus action.
        /// </summary>
        private void ProcessResetDailyBonus()
        {
            // This is for testing purposes only so the daily bonus redemptions can be reset manually
            // Check to see if web.config contains param.  Just a measure to prevent this from 
            // accidentally being called on prod.
            string retCode = "-1";
            string retDesc = "Not Authorized";

            if (KanevaWebGlobals.AllowDailyBonusReset)
            {
                CentralCache.Remove(CentralCache.keyFameDailyRewardTracking + "." + m_userId + "." + (int)PacketId.WORLD_DAILY_SIGNIN);
                CentralCache.Remove(CentralCache.keyFameDailyRewardTracking + "." + m_userId + "." + (int)PacketId.LOGIN_WOK_1_DAY_IN_A_ROW);
                CentralCache.Remove(CentralCache.keyFameDailyRewardTracking + "." + m_userId + "." + (int)PacketId.LOGIN_WOK_2_DAYS_IN_A_ROW);
                CentralCache.Remove(CentralCache.keyFameDailyRewardTracking + "." + m_userId + "." + (int)PacketId.LOGIN_WOK_3_DAYS_IN_A_ROW);
                retCode = "0";
                retDesc = "Success";
            }

            string returnStr = "<Result>\r\n  <ReturnCode>" + retCode + "</ReturnCode> ";
            returnStr += "<ResultDescription>" + retDesc + "</ResultDescription>";
            returnStr += "</Result>";
            Response.Write(returnStr);
        }

        /// <summary>
        /// Processes the progressMetrics action.
        /// </summary>
        private void ProcessProgressMetrics()
        {
            string progressId = "";
            int userId = 0;
            double timeMs = 0;
            string progressTag = "";
            string progressMsg = "";
            string progressGuid = "";

            if (Request.Params["progressId"] != null)
            {
                progressId = Request.Params["progressId"].ToString();
            }

            if (Request.Params["userId"] != null)
            {
                try
                {
                    userId = Convert.ToInt32(Request.Params["userId"]);
                }
                catch (Exception) { }
            }

            if (userId == 0)
            {
                userId = m_userId;
            }


            if (Request.Params["timeMs"] != null)
            {
                try
                {
                    timeMs = Convert.ToDouble(Request.Params["timeMs"]);
                }
                catch (Exception) { }
            }


            if (Request.Params["progressTag"] != null)
            {
                progressTag = Request.Params["progressTag"].ToString();
            }


            if (Request.Params["progressMsg"] != null)
            {
                progressMsg = Request.Params["progressMsg"].ToString();
            }

            if (Request.Params["progressGuid"] != null)
            {
                progressGuid = Request.Params["progressGuid"].ToString();
            }


            string is3dapp = "N";

            if (progressTag.Length > 0 && !progressTag.EndsWith(".home") && !progressTag.EndsWith(".hangout"))
            {
                try
                {

                    // Get Name or Game ID
                    string gameName = "";
                    StpUrl.ExtractGameName(progressTag, out gameName);

                    if (Common.IsNumeric(gameName))
                    {
                        if (GetGameFacade.GetGameByGameId(Convert.ToInt32(gameName)) != null)
                        {
                            is3dapp = "Y";
                        }
                    }
                    else
                    {
                        if (GetGameFacade.GetGameByGameName(gameName) != null)
                        {
                            is3dapp = "Y";
                        }
                    }
                }
                catch (Exception) { }
            }

            Hashtable parameters = new Hashtable();
            parameters.Add("@progressId", progressId);
            parameters.Add("@userId", userId);
            parameters.Add("@timeMs", timeMs);
            parameters.Add("@progressTag", progressTag);
            parameters.Add("@progressMsg", progressMsg);
            parameters.Add("@is3dapp", is3dapp);
            parameters.Add("@progressGuid", progressGuid);
            KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery("INSERT INTO metrics.progress_metrics (progress_id, user_id, time_ms, progress_tag, progress_msg, is_3dapp, progress_guid) VALUES (@progressId, @userId, @timeMs, @progressTag, @progressMsg, @is3dapp, @progressGuid) ", parameters);

            string returnStr = "<Result>\r\n  <ReturnCode>0</ReturnCode> ";
            returnStr += "<ResultDescription>Success</ResultDescription>";
            returnStr += "</Result>";
            Response.Write(returnStr);
        }

        /// <summary>
        /// Processes the generateTestUser action.
        /// </summary>
        private void ProcessGenerateTestUser()
        {
            // Check the key.
            if (string.IsNullOrWhiteSpace(Request.Params["tugKey"]) ||
                !Request.Params["tugKey"].Equals(Configuration.AutomatedTestUserGenerationKey))
            {
                Response.Write(string.Format(_errorResult, "Invalid key."));
                return;
            }

            // Confirm that this call is active on this environment.
            if (!Configuration.AutomatedTestUserGenerationEnabled)
            {
                Response.Write(string.Format(_errorResult, "Unauthorized action."));
                return;
            }

            string username = Request.Params["username"];
            string password = Request.Params["password"];
            string country = Request.Params["country"];
            string postalCode = Request.Params["postalCode"];
            string gender = Request.Params["gender"];
            string strDOB = Request.Params["dateOfBirth"];
            DateTime dateOfBirth = DateTime.Now.Subtract(new TimeSpan(356 * 18, 0, 0, 0, 0));
            string strAutoValidateEmail = Request.Params["autoValidateEmail"];
            bool autoValidateEmail = true;

            // Validate user input.
            if (string.IsNullOrWhiteSpace(username))
            {
                Response.Write(string.Format(_errorResult, "username not specified or invalid."));
                return;
            }
            if (string.IsNullOrWhiteSpace(password))
            {
                Response.Write(string.Format(_errorResult, "password not specified or invalid."));
                return;
            }
            if (!string.IsNullOrWhiteSpace(gender) && gender != "M" && gender != "F")
            {
                Response.Write(string.Format(_errorResult, "gender invalid, must be 'M' or 'F'."));
                return;
            }
            if (!string.IsNullOrWhiteSpace(strDOB) && !DateTime.TryParse(strDOB, out dateOfBirth))
            {
                Response.Write(string.Format(_errorResult, "dateOfBirth invalid, must be a valid datetime string."));
                return;
            }
            if (!string.IsNullOrWhiteSpace(strAutoValidateEmail) && !Boolean.TryParse(strAutoValidateEmail, out autoValidateEmail))
            {
                Response.Write(string.Format(_errorResult, "autoValidateEmail invalid, must be either 'true' or 'false'."));
                return;
            }

            // Set parameter defaults.
            username += Common.ConvertToUnixTimestamp(DateTime.Now);
            country = (string.IsNullOrWhiteSpace(country) ? "US" : country);
            gender = (string.IsNullOrWhiteSpace(gender) ? "M" : gender);
            postalCode = (string.IsNullOrWhiteSpace(postalCode) ? "12345" : postalCode);
            string email = username + "@kaneva.com";

            // Hash password
            byte[] salt = new byte[9];
            (new RNGCryptoServiceProvider()).GetBytes(salt);
            string hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(UsersUtility.MakeHash(password + username.ToLower()) + Convert.ToBase64String(salt), "MD5");

            // Generate a unique key_code for registration purposes
            string keyCode = KanevaGlobals.GenerateUniqueString(20);

            // Create a registration Key
            string regKey = KanevaGlobals.GenerateUniqueString(50);

            // Create the user.
            int userId = GetUserFacade.InsertUser(username, hashPassword, Convert.ToBase64String(salt), (int)SiteRole.KanevaRoles.Automated_Test_Account, 1,
                Server.HtmlEncode(username), Server.HtmlEncode("TestUser"), username, Server.HtmlEncode(gender), "", Server.HtmlEncode(email), dateOfBirth, keyCode,
                Server.HtmlEncode(country), Server.HtmlEncode(postalCode), regKey, Common.GetVisitorIPAddress(), (int)Kaneva.Constants.eUSER_STATUS.REGNOTVALIDATED, 0);

            // Did we get a success?
            if (userId > 0)
            {
                // Set up user profile.
                if (!GetUserFacade.CreateDefaultUserHomePage(username, userId))
                {
                    GetUserFacade.CleanUpRegistrationIssue(username);
                    Response.Write(string.Format(_errorResult, "Unable to create user profile."));
                    return;
                }
                
                // Set up user home world.
                if (!GetUserFacade.CreateDefaultUserHomeWorld(username, userId))
                {
                    GetUserFacade.CleanUpRegistrationIssue(username);
                    Response.Write(string.Format(_errorResult, "Unable to create user home world."));
                    return;
                }

                // Automatically set thier status to validated if necessary.
                if (autoValidateEmail)
                {
                    GetUserFacade.UpdateUserStatus(userId, (int)Constants.eUSER_STATUS.REGVALIDATED);
                }

                // Send the new user credentials as response.
                string successResponse = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription><username>{0}</username><email>{1}</email></ResultDescription>\r\n </Result>";
                Response.Write(string.Format(successResponse, username, email));
            }
        }

        /// <summary>
        /// Processes the setTestUserToken action.
        /// </summary>
        private void ProcessSetTestUserToken()
        {
            // Confirm user permissions.
            if (!GetUserFacade.IsUserAutomatedTestAccount(KanevaWebGlobals.CurrentUser))
            {
                Response.Write(string.Format(_errorResult, "Unauthorized user."));
                return;
            }

            // Confirm that this call is active on this environment.
            if (!Configuration.AutomatedTestUserGenerationEnabled)
            {
                Response.Write(string.Format(_errorResult, "Unauthorized action."));
                return;
            }

            // Generate a new Guid.
            Guid token = Guid.NewGuid();
            User cacheUser = new User
            {
                UserId = KanevaWebGlobals.CurrentUser.UserId,
                Username = KanevaWebGlobals.CurrentUser.Username,
                Email = KanevaWebGlobals.CurrentUser.Email,
                Role = KanevaWebGlobals.CurrentUser.Role
            };
            bool tokenSaved = GetUserFacade.SetSmokeTestUserLoginKey(token.ToString(), cacheUser);

            // Send the new token as response.
            if (tokenSaved)
            {
                string successResponse = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription><token>{0}</token></ResultDescription>\r\n </Result>";
                Response.Write(string.Format(successResponse, token));
            }
            else
            {
                Response.Write(string.Format(_errorResult, "Failed saving token."));
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
