///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class instantBuy : KgpBasePage
    {
        #region Declarations
        
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(false))
            {
                if (Request.Params["operation"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>operation not specified</ResultDescription>\r\n</Result>";
                    m_logger.Error("Error In instantbuy.aspx: operation not specified");
                    Response.Write(errorStr);
                    return;
                }

                //try and get the opertation. If it fails
                string operationId = "none";
                try
                {
                    operationId = Request.Params["operation"].ToString();
                }
                catch (FormatException fe)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>invalid operation id</ResultDescription>\r\n</Result>";
                    m_logger.Error("Error In instantbuy.aspx: invalid operation id", fe);
                    Response.Write(errorStr);
                    return;
                }

                //get user information for return 
                int userId = GetUserId();
                //string userGender = UsersUtility.GetUserGender(userId);
                // Optimize take out the db query
                string userGender = KanevaWebGlobals.CurrentUser.Gender;
                
                //perform the indicated operation
                switch (operationId)
                {
                    case "object":
                      #region object
                        //parse the object id 
                        try
                        {
                            if (Request.Params["objectId"] != null || Request.Params["globalId"] != null)
                            {
                                WOKItem item = null;

                                // Fix for instantBuy in worlds, where placementId has no meaning.
                                if (Request.Params["globalId"] != null)
                                {
                                    item = GetShoppingFacade.GetItem (Convert.ToInt32 (Request.Params["globalId"]));
                                }
                                else
                                {
                                    item = WOKStoreUtility.GetWOKItemsByObjPlacementId (Convert.ToInt32 (Request.Params["objectId"]));
                                }

                                if (item != null)
                                {
                                    // Build the return message and include whether the user has raved the item or not
                                    int playerRaveCount = UsersUtility.GetItemRaveCountByUser (userId, item.GlobalId);

                                    string creatorName, displayName;
                                    if (!item.IsKanevaOwned)
                                    {
                                        creatorName = UsersUtility.GetUserNameFromId (Convert.ToInt32 (item.ItemCreatorId));
                                        displayName = item.DisplayName;
                                    }
                                    else
                                    {
                                        creatorName = "Kaneva";
                                        displayName = item.Name;
                                    }

                                    XmlDocument doc = new XmlDocument ();
                                    XmlElement elem;
                                    XmlText text;

                                    elem = doc.CreateElement ("Result");
                                    doc.AppendChild (elem);

                                    elem = doc.CreateElement ("ReturnCode");
                                    text = doc.CreateTextNode ("0");
                                    elem.AppendChild (text);
                                    doc.LastChild.AppendChild (elem);

                                    elem = doc.CreateElement ("ResultDescription");
                                    text = doc.CreateTextNode ("success");
                                    elem.AppendChild (text);
                                    doc.LastChild.AppendChild (elem);

                                    elem = doc.CreateElement ("display_name");
                                    text = doc.CreateTextNode (displayName);
                                    elem.AppendChild (text);
                                    doc.LastChild.AppendChild (elem);

                                    elem = doc.CreateElement ("creator");
                                    text = doc.CreateTextNode (creatorName);
                                    elem.AppendChild (text);
                                    doc.LastChild.AppendChild (elem);

                                    elem = doc.CreateElement ("rave_count");
                                    text = doc.CreateTextNode (Convert.ToString (item.NumberOfRaves));
                                    elem.AppendChild (text);
                                    doc.LastChild.AppendChild (elem);

                                    elem = doc.CreateElement ("player_rave_count");
                                    text = doc.CreateTextNode (Convert.ToString (playerRaveCount));
                                    elem.AppendChild (text);
                                    doc.LastChild.AppendChild (elem);

                                    // Return VIP price if user has VIP 
                                    elem = doc.CreateElement ("web_price");
                                    if (KanevaWebGlobals.CurrentUser.HasVIPPass)
                                    {
                                        text = doc.CreateTextNode (Convert.ToString (item.WebPriceVIP));
                                    }
                                    else
                                    {
                                        text = doc.CreateTextNode (Convert.ToString (item.WebPrice));
                                    }
                                    elem.AppendChild (text);
                                    doc.LastChild.AppendChild (elem);

                                    elem = doc.CreateElement ("inventory_type");
                                    text = doc.CreateTextNode (Convert.ToString (item.InventoryType));
                                    elem.AppendChild (text);
                                    doc.LastChild.AppendChild (elem);

                                    Response.Write (doc.OuterXml);
                                }
                                else
                                {
                                    string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>Can't retreive items</ResultDescription>\r\n</Result>";
                                    m_logger.Error ("Error In instantbuy.aspx: Can't retreive items");
                                    Response.Write (errorStr);
                                    return;
                                }
                            }
                            else
                            {
                                string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>no user object was specified</ResultDescription>\r\n</Result>";
                                m_logger.Error ("Error In instantbuy.aspx: object id not specified");
                                Response.Write (errorStr);
                                return;
                            }
                        }
                        catch (FormatException fe)
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>invalid player id</ResultDescription>\r\n</Result>";
                            m_logger.Error ("Error In instantbuy.aspx: invalid player id", fe);
                            Response.Write (errorStr);
                            return;
                        }
                        catch (Exception ex)
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>Can't retreive items</ResultDescription>\r\n</Result>";
                            m_logger.Error ("Error In instantbuy.aspx: Can't retreive items", ex);
                            Response.Write (errorStr);
                            return;
                        }
                      #endregion
                          break;
                    case "list"://view the armed inventory  
                      #region list
                      //parse the id down to the player id integer
                        try
                        {
                            if (Request.Params["username"] != null)
                            {
                                string username = Request.Params["username"].ToString();
                                //get playerId from username
                                DataRow userInfo = GetUserFacade.GetUserIdPlayerIdFromUsername(username);
                                int playerId = Convert.ToInt32(userInfo["player_id"]);

                                //create new dataset
                                DataSet ds = new DataSet();

                                //get the users current armed items and all needed data
                                DataTable dt = UsersUtility.GetWokUsersArmedItems(playerId, KanevaWebGlobals.CurrentUser.HasVIPPass);
                                dt.TableName = "Armed Items";

                                //add to the data set
                                ds.Tables.Add(dt);
                                ds.DataSetName = "Result";

                                //generate the XML out put
                                XmlDocument doc = new XmlDocument();
                                doc.LoadXml(ds.GetXml());
                                XmlNode root = doc.DocumentElement;

                                int numRecords = dt.Rows.Count;
                                XmlElement elem = doc.CreateElement("NumberRecords");
                                elem.InnerText = numRecords.ToString();

                                root.InsertBefore(elem, root.FirstChild);

                                int totalNumRecords = dt.Rows.Count;
                                elem = doc.CreateElement("TotalNumberRecords");
                                elem.InnerText = totalNumRecords.ToString();

                                root.InsertBefore(elem, root.FirstChild);

                                elem = doc.CreateElement("ReturnDescription");
                                elem.InnerText = "success";

                                root.InsertBefore(elem, root.FirstChild);

                                elem = doc.CreateElement("ReturnCode");
                                elem.InnerText = "0";

                                root.InsertBefore(elem, root.FirstChild);

                                elem = doc.CreateElement("UsersGender");
                                elem.InnerText = userGender;
                                root.InsertBefore(elem, root.FirstChild);

                                //return the info to WOK
                                Response.Write(root.OuterXml);
                            }
                            else
                            {
                                string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>no user was specified</ResultDescription>\r\n</Result>";
                                m_logger.Error("Error In instantbuy.aspx: username not specified");
                                Response.Write(errorStr);
                                return;
                            }
                        }
                        catch (FormatException fe)
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>invalid player id</ResultDescription>\r\n</Result>";
                            m_logger.Error("Error In instantbuy.aspx: invalid player id",fe);
                            Response.Write(errorStr);
                            return;
                        }
                        catch (Exception ex)
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>Can't retreive items</ResultDescription>\r\n</Result>";
                            m_logger.Error("Error In instantbuy.aspx: Can't retreive items", ex);
                            Response.Write(errorStr);
                            return;
                          }
                      #endregion
                          break;
                    case "buy"://by the indicated item(s)
                      #region buy
                          try
                          {
                              string errorStr = "";

                              //do a check to see if the needed parameters are there
                              if (Request.Params["credit"] == null)
                              {
                                  errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>Type of credit to use was not specified.</ResultDescription>\r\n</Result>";
                                  m_logger.Error ("Error In instantbuy.aspx: Type of credit to use was not specified.");
                                  Response.Write (errorStr);
                                  return;
                              }
                              if (Request.Params["items"] == null)
                              {
                                  errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>No items were selected for purchase.</ResultDescription>\r\n</Result>";
                                  m_logger.Error ("Error In instantbuy.aspx: No items were selected for purchase.");
                                  Response.Write (errorStr);
                                  return;
                              }
                              if (Request.Params["qnty"] == null)
                              {
                                  errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>No quantity was specified for the purchase.</ResultDescription>\r\n</Result>";
                                  m_logger.Error ("Error In instantbuy.aspx: No quantity was specified for the purchase.");
                                  Response.Write (errorStr);
                                  return;
                              }

                              string succeedList = "";
                              string failList = "";
                              string failureReason = "";

                              bool premiumPurchase = false;

                              if (Request.Params["premium"] != null)
                              {
                                  premiumPurchase = true;
                              }

                              //get the credit type to use
                              string creditType = Request.Params["credit"].ToString ();

                              //get the list of items and parse
                              string items = ((Request.Params["items"] != null) ? Request.Params["items"].ToString () : "");

                              //get the list of quantities for the  items. if no quantities given it will default to one for everything
                              string quantities = ((Request.Params["qnty"] != null) ? Request.Params["qnty"].ToString () : "");

                              //parse out the global ids and quantities
                              string[] itemList = items.Trim ().Split (',');
                              string[] qtyList = quantities.Trim ().Split (',');

                              //create an array list to hold a 1 x 1 array to store the integer based items and quantities
                              ArrayList order = new ArrayList ();

                              //clear the items to prep it for suffucient funds check query
                              items = "";

                              //process the item ids and quantities
                              for (int i = 0; i < itemList.Length; i++)
                              {
                                  //handles format exception of non-integer. non-integers will be ignored and logged
                                  try
                                  {
                                      //create order item array
                                      int[,] orderItem = new int[1, 2];

                                      //pull item number
                                      orderItem[0, 0] = Convert.ToInt32 (itemList[i]);

                                      //build query string for checking suffucient funds
                                      items += itemList[i] + ",";

                                      //catch error in case quantities dont match number of items
                                      //default to one so purchasing may continue
                                      try
                                      {
                                          orderItem[0, 1] = Convert.ToInt32 (qtyList[i]);
                                      }
                                      catch (Exception)
                                      {
                                          orderItem[0, 1] = 1;
                                      }

                                      //add to the final order arraylist
                                      order.Add (orderItem);
                                  }
                                  catch (FormatException fe)
                                  {
                                      m_logger.Warn ("Error In instantbuy.aspx: error during id/quantity processing.", fe);
                                  }
                              }

                              //to a preventative check to make sure they have enough money for all the items in their list
                              //before purchasing                             
                              int totalCost = WOKStoreUtility.GetTotalOrderCost (items.Substring (0, items.Length - 1), order, KanevaWebGlobals.CurrentUser.HasVIPPass);

                            if (GetUserFacade.IsUserAutomatedTestAccount(KanevaWebGlobals.CurrentUser))
                            {
                                GetUserFacade.AdjustUserBalance(KanevaWebGlobals.CurrentUser.UserId, creditType, totalCost + 10, (int)TransactionType.eTRANSACTION_TYPES.TT_AUTO_TEST_USER_BALANCE_ADJUST);
                            }

                            double availableCredits = UsersUtility.getUserBalance(userId, creditType);

                            if (totalCost > availableCredits)
                              {
                                  errorStr = "<Result>\r\n  <ReturnCode>" + (creditType.Equals (Constants.CURR_GPOINT) ? 1 : 2) + "</ReturnCode>\r\n  <ResultDescription>Not Enough " + (creditType.Equals (Constants.CURR_GPOINT) ? Constants.CURR_GPOINT : Constants.CURR_KPOINT) + "</ResultDescription>\r\n</Result>";
                                  m_logger.Error ("Error In instantbuy.aspx: User doesn't have enough " + (creditType.Equals (Constants.CURR_GPOINT) ? Constants.CURR_GPOINT : Constants.CURR_KPOINT) + " for purchase.");
                                  Response.Write (errorStr);
                                  return;
                              }

                              //attempt to purchase all items
                              foreach (int[,] orderItem in order)
                              {
                                  //just to handle format exception of noninteger so processing of other
                                  //potentially good items can continue
                                  try
                                  {
                                      string result;

                                      if (premiumPurchase)
                                      {
                                          if (Request.Params["communityId"] == null)
                                          {
                                              errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>Community Id was not specified for the purchase.</ResultDescription>\r\n</Result>";
                                              m_logger.Error ("Error In instantbuy.aspx: Community Id was NOT specified for the premium item purchase.");
                                              Response.Write (errorStr);
                                              return;
                                          }

                                          int communityId = Convert.ToInt32 (Request.Params["communityId"]);
                                          if (communityId == 0)
                                          {
                                              errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>Community Id could not be 0.</ResultDescription>\r\n</Result>";
                                              m_logger.Error ("Error In instantbuy.aspx: Community Id can not be 0 for a premium item purchase.");
                                              Response.Write (errorStr);
                                              return;
                                          }

                                          result = BuyPremiumItem (creditType, orderItem[0, 0], orderItem[0, 1], userId, communityId);
                                      }
                                      else
                                      {
                                          result = BuyItem (creditType, orderItem[0, 0], orderItem[0, 1], userId, false);
                                      }

                                      if (result == null)
                                      {
                                          succeedList += orderItem[0, 0] + ",";

                                          try
                                          {
                                              if (orderItem[0, 0] > 0 && !premiumPurchase)
                                              {
                                                  // return server user is currently on
                                                  DataTable dt = new GameFacade().GetGameUserIn(KanevaWebGlobals.CurrentUser.UserId);
                                                  if (dt != null && dt.Rows.Count > 0)
                                                  {
                                                      // Check to see if user is GM and send event to server which will make the item available to the appserver
                                                      int gameId = Convert.ToInt32(dt.Rows[0]["game_id"]);
                                                      int serverId = Convert.ToInt32(dt.Rows[0]["server_id"]);
                                                      (new RemoteEventSender()).broadcastItemChangeEventToServer(RemoteEventSender.ObjectType.PlayerInventory, RemoteEventSender.ChangeType.AddObject, KanevaWebGlobals.CurrentUser.UserId, serverId);
                                                  }
                                              }
                                          }
                                          catch (Exception exc)
                                          {
                                              m_logger.Error ("Tickler failure buying item", exc);
                                          }
                                      }
                                      else //else append to failed list
                                      {
                                          failList += orderItem[0, 0] + ",";
                                          failureReason += result + ",";
                                      }
                                  }
                                  catch (FormatException fe)
                                  {
                                      m_logger.Warn ("Error In instantbuy.aspx: error during order processing.", fe);
                                  }
                              }

                              string resultStr = "<Result>\r\n <ReturnCode>" + (succeedList.Length > 0 ? "0" : "3") + "</ReturnCode>\r\n ";

                              //return results
                              if (succeedList != "")
                              {
                                  succeedList = succeedList.Substring (0, succeedList.Length - 1);
                                  resultStr += "<ItemsSucceded>" + succeedList + "</ItemsSucceded>\r\n";
                              }
                              if (failList != "")
                              {
                                  failList = failList.Substring (0, failList.Length - 1);
                                  failureReason = failureReason.Substring (0, failureReason.Length - 1);
                                  resultStr += "<ItemsFailed>" + failList + "</ItemsFailed>\r\n<ResultDescription>" + failureReason + "</ResultDescription>";
                                  m_logger.Warn ("Error In instantbuy.aspx: The following items failed to purchase: " + failList + " for the following reasons " + failureReason);
                              }
                              resultStr += "</Result>";

                              Response.Write (resultStr);

                          }
                          catch (FormatException fe)
                          {
                              string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>invalid player id</ResultDescription>\r\n</Result>";
                              m_logger.Warn ("Error In instantbuy.aspx: invalid player id.", fe);
                              Response.Write (errorStr);
                              return;

                          }
                          catch (Exception ex)
                          {
                              string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>Can't retreive items</ResultDescription>\r\n</Result>";
                              m_logger.Warn ("Error In instantbuy.aspx: Can't retreive items.", ex);
                              Response.Write (errorStr);
                              return;

                          }
                      #endregion 
                      break;
                    case "info"://
                      #region info                       
                      try
                      {
                          //do a check to see if the needed parameters are there
                          if (Request.Params["items"] == null)
                          {
                              string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>No items were selected.</ResultDescription>\r\n</Result>";
                              m_logger.Error ("Error In instantbuy.aspx: No items were selected.");
                              Response.Write (errorStr);
                              return;
                          }

                          //get the list of items and parse
                          string items = ((Request.Params["items"] != null) ? Request.Params["items"].ToString () : "");

                          //create new dataset
                          DataSet ds = new DataSet ();

                          if (Request.Params["getScriptBundle"] != null)
                          {
                              List<WOKItem> bundleList = GetShoppingFacade.GetBundlesItemBelongsTo (Convert.ToInt32 (items), string.Empty);
                              if (bundleList.Count <= 0 || bundleList.Count > 1)
                              {
                                  string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>Unable to find bundle for items.</ResultDescription>\r\n</Result>";
                                  m_logger.Error ("Error In instantbuy.aspx: Unable to find bundle for items.");
                                  Response.Write (errorStr);
                                  return;
                              }

                              WOKItem bundle = bundleList[0];
                              items = bundle.GlobalId.ToString ();
                          }

                          //get the users current armed items and all needed data
                          DataTable dt = WOKStoreUtility.GetWOKItems ("i.global_id in (" + items + ")", KanevaWebGlobals.CurrentUser.HasVIPPass);
                          dt.TableName = "Item Info";

                          //add to the data set
                          ds.Tables.Add (dt);
                          ds.DataSetName = "Result";

                          //generate the XML out put
                          XmlDocument doc = new XmlDocument ();
                          doc.LoadXml (ds.GetXml ());
                          XmlNode root = doc.DocumentElement;

                          int numRecords = dt.Rows.Count;
                          XmlElement elem = doc.CreateElement ("NumberRecords");
                          elem.InnerText = numRecords.ToString ();

                          root.InsertBefore (elem, root.FirstChild);

                          int totalNumRecords = dt.Rows.Count;
                          elem = doc.CreateElement ("TotalNumberRecords");
                          elem.InnerText = totalNumRecords.ToString ();

                          root.InsertBefore (elem, root.FirstChild);

                          elem = doc.CreateElement ("ReturnDescription");
                          elem.InnerText = "success";

                          root.InsertBefore (elem, root.FirstChild);

                          elem = doc.CreateElement ("ReturnCode");
                          elem.InnerText = "0";

                          root.InsertBefore (elem, root.FirstChild);

                          elem = doc.CreateElement ("UsersGender");
                          elem.InnerText = userGender;
                          root.InsertBefore (elem, root.FirstChild);

                          //return the info to WOK
                          Response.Write (root.OuterXml);
                      }
                      catch (Exception ex)
                      {
                          string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>Can't retreive items</ResultDescription>\r\n</Result>";
                          m_logger.Warn ("Error In instantbuy.aspx: Can't retreive items.", ex);
                          Response.Write (errorStr);
                          return;
                      }
                      #endregion                            
                      break;

                  case "try"://
                      #region try
                      try
                      {
                          int glid = 0;
                          if (Request.Params["glid"] != null)
                          {
                              try
                              {
                                  glid = Convert.ToInt32(Request.Params["glid"]);
                              }
                              catch (System.Exception) {}
                          }

                          if( glid==0 )
                          {
                              string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>No items were selected.</ResultDescription>\r\n</Result>";
                              m_logger.Error("Error In instantbuy.aspx: No items were selected.");
                              Response.Write(errorStr);
                              return;
                          }

                          //create new dataset
                          DataSet ds = new DataSet();

                          //get the users current armed items and all needed data
                          DataTable dt = WOKStoreUtility.GetWOKItems("i.global_id=" + glid.ToString());
                          dt.TableName = "Item Info";

                          //add to the data set
                          ds.Tables.Add(dt);
                          ds.DataSetName = "Result";

                          //generate the XML out put
                          XmlDocument doc = new XmlDocument();
                          doc.LoadXml(ds.GetXml());
                          XmlNode root = doc.DocumentElement;

                          int numRecords = dt.Rows.Count;
                          XmlElement elem = doc.CreateElement("NumberRecords");
                          elem.InnerText = numRecords.ToString();

                          root.InsertBefore(elem, root.FirstChild);

                          int totalNumRecords = dt.Rows.Count;
                          elem = doc.CreateElement("TotalNumberRecords");
                          elem.InnerText = totalNumRecords.ToString();

                          root.InsertBefore(elem, root.FirstChild);

                          elem = doc.CreateElement("ReturnDescription");
                          elem.InnerText = "success";

                          root.InsertBefore(elem, root.FirstChild);

                          elem = doc.CreateElement("ReturnCode");
                          elem.InnerText = "0";

                          root.InsertBefore(elem, root.FirstChild);

                          elem = doc.CreateElement("UsersGender");
                          elem.InnerText = userGender;
                          root.InsertBefore(elem, root.FirstChild);

                          DataTable dtPass = WOKStoreUtility.GetItemPassGroups(glid);

                          if (dtPass != null)
                          {
                              // Insert the pass group ids into the appropriate glid section of the doc
                              for (int ii = 0; ii < dtPass.Rows.Count; ii++)
                              {
                                  string passGlid = dtPass.Rows[ii]["global_id"].ToString();
                                  string passGroupId = dtPass.Rows[ii]["pass_group_id"].ToString();

                                  // XPath to specific glid
                                  XmlNode guidNode = root.SelectSingleNode("//Result/Item_x0020_Info/global_id[.  ='" + passGlid + "']");

                                  XmlElement passElement = doc.CreateElement("Pass");
                                  XmlNode passNode = guidNode.ParentNode.InsertAfter(passElement, guidNode.ParentNode.LastChild);

                                  XmlElement passId = doc.CreateElement("ID");
                                  passId.InnerText = passGroupId;
                                  passNode.AppendChild(passId);
                              }
                          }

                          //return the info to WOK
                          Response.Write(root.OuterXml);
                      }
                      catch (Exception ex)
                      {
                          string errorStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>Can't retreive items</ResultDescription>\r\n</Result>";
                          m_logger.Warn("Error In instantbuy.aspx: Can't retreive items.", ex);
                          Response.Write(errorStr);
                          return;
                      }
                      #endregion
                      break;
                    case "testTryOn":
                        string _successResult = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>{0}</ResultDescription>\r\n  <URL>{1}</URL>\r\n  <EXPIRATION>{2}</EXPIRATION>\r\n</Result>";
                        string _errorResult = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>{0}</ResultDescription>\r\n</Result>";

                        // Confirm user permissions.
                        if (!GetUserFacade.IsUserAutomatedTestAccount(KanevaWebGlobals.CurrentUser))
                        {
                            Response.Write(string.Format(_errorResult, "Unauthorized user."));
                            return;
                        }

                        // Confirm that this call is active on this environment.
                        if (!Configuration.AutomatedTestUserGenerationEnabled)
                        {
                            Response.Write(string.Format(_errorResult, "Unauthorized action."));
                            return;
                        }

                        int tryGlid;
                        if (Request.Params["glid"] == null || !int.TryParse(Request.Params["glid"], out tryGlid))
                        {
                            Response.Write(string.Format(_errorResult, "Invalid glid."));
                            return;
                        }

                        WOKItem tryItem = GetShoppingFacade.GetItem(tryGlid);
                        if (tryItem == null || tryItem.GlobalId <= 0)
                        {
                            Response.Write(string.Format(_errorResult, "Invalid glid."));
                            return;
                        }

                        // Construct the url.
                        string stpUrl;
                        if (tryItem.UseType == WOKItem.USE_TYPE_CUSTOM_DEED)
                        {
                            // Deeds return just the stpURL of the Try On zone.
                            int communityId = GetShoppingFacade.GetCommunityIdFromDeedTemplateId(tryGlid);
                            if (communityId > 0)
                            {
                                Community c = GetCommunityFacade.GetCommunity(communityId);
                                stpUrl = StpUrl.MakeUrlPrefix(KanevaGlobals.WokGameId.ToString());
                                stpUrl += "/" + StpUrl.MakeCommunityUrlPath(c.Name);
                                stpUrl = System.Web.HttpUtility.UrlPathEncode(stpUrl);
                                stpUrl += "?";
                            }         
                            else
                            {
                                Response.Write(string.Format(_errorResult, "Unable to find Try On world for glid."));
                                return;
                            }                     
                        }
                        else
                        {
                            stpUrl = StpUrl.MakeUrlPrefix(KanevaGlobals.WokGameId.ToString());
                            stpUrl += "?try=" + tryGlid + ':' + tryItem.UseType;
                        }

                        // Append credentials
                        stpUrl += "&userid=" + KanevaWebGlobals.CurrentUser.Email;

                        string singleSignOnToken = GetUserFacade.GetSingleSignOnToken(KanevaWebGlobals.CurrentUser.UserId);
                        if (!string.IsNullOrWhiteSpace(singleSignOnToken))
                            stpUrl += "&" + singleSignOnToken;

                        Response.Write(string.Format(_successResult, "success", stpUrl, tryItem.ExpiredDuration));
                        break;
                }

            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
