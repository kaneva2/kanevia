///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class setMessageQueue : KgpBasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            //setMessageQueue.aspx ? id={} & respId  ={} & respCode = {} & procDate = {}
            if (CheckUserId (false))
            {
                if ((Request.Params["id"] == null))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Must supply id</ResultDescription>\r\n</Result>";
                    Response.Write (errorStr);
                    return;
                }

                if ((Request.Params["respId"] == null))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Must supply respId</ResultDescription>\r\n</Result>";
                    Response.Write (errorStr);
                    return;
                }

                if ((Request.Params["respCode"] == null))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Must supply respCode</ResultDescription>\r\n</Result>";
                    Response.Write (errorStr);
                    return;
                }

                if ((Request.Params["procDate"] == null))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Must supply procDate</ResultDescription>\r\n</Result>";
                    Response.Write (errorStr);
                    return;
                }

                string id = Request.Params["id"].ToString ();
                string respId = Request.Params["respId"].ToString ();
                string respCode = Request.Params["respCode"].ToString ();
                DateTime respDate = Convert.ToDateTime (Request.Params["procDate"]);

                string resp = "";
                if (!GetGameFacade.UpdateAppMessageQueue (id, respId, respCode, respDate))
                {
                    resp = "<Result><ReturnCode>-1</ReturnCode><ResultDescription>Update App Message Queue Message Failed</ResultDescription></Result>";
                    Response.Write (resp);
                }

                resp = "<Result><ReturnCode>0</ReturnCode><ResultDescription>Ok</ResultDescription></Result>";
                Response.Write (resp);
            }
        }
    }
}