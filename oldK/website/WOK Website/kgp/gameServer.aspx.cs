///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class gameServer : KgpBasePage
    {
        const string NOT_FOUND_RESP = "2 Game not found";
        const string BAD_PARAM_RESP = "7 Bad parameter";

        /// <summary>
        /// get the server for a STAR will return resp above, or 0 server port
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CheckUserId(false))
            {
                if ((Request.Params["gameId"] == null) )
                {
                    string errorStr = BAD_PARAM_RESP;
                    Response.Write(errorStr);
                    return;
                }
                int gameId = Int32.Parse(Request.Params["gameId"].ToString());

                DataRow dr = ServerUtility.GetGameServer(gameId);

                string response = null;
                if (dr == null)
                {
                    response = NOT_FOUND_RESP;
                }
                else
                {
                    response = "0 " + dr["ip_address"].ToString() + " " + dr["port"].ToString();
                }

                Response.Write(response);
            }

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
