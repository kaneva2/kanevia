///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class premiumitems : KgpBasePage
    {
        #region Declarations

        private int _type;
        private string _action;

        private string _errorResult = "<Result>\r\n  <ReturnCode>{0}</ReturnCode>\r\n  <ResultDescription>{1}</ResultDescription>\r\n </Result>";

        #endregion

        private void Page_Load(object sender, EventArgs e)
        {
            if (CheckUserId (false))
            {
                GetRequestParams();

                switch (_action)
                {
                    case "getAvailableCreditRedemptions":
                        ProcessGetAvailableCreditRedemptions();
                        break;
                    default:
                        ProcessDefaultAction();
                        break;
                }
            }
        }

        /// <summary>
        /// Reads all request parameters from query string and provides appropriate defaults
        /// </summary>
        private void GetRequestParams()
        {
            int iOut;

            _type = (Int32.TryParse(Request.Params["type"], out iOut) ? iOut : 0);
            _action = Request.Params["action"];
        }

        /// <summary>
        /// Called to process the default action for the page if an action param was not specified
        /// </summary>
        private void ProcessDefaultAction()
        {
            if (_type <= 0)
            {
                Response.Write(string.Format(_errorResult, -1, "Premium Item type not specified"));
                return;
            }

            // Donation = 1, Key = 2  (WOKItem.PremiumItemType)
            DataSet ds = GetGameFacade.GetPremiumItems(Convert.ToInt32(Request.Params["type"]));

            ds.DataSetName = "Result";

            // setup the result in the header
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Called to return the credit redemption balance for a given user
        /// </summary>
        private void ProcessGetAvailableCreditRedemptions()
        {
            PremiumItemRedeemAmounts redeemAmounts = GetTransactionFacade.GetTotalTransactionCreditAmountsByUser(KanevaWebGlobals.CurrentUser.UserId, KanevaGlobals.PremiumItemDaysPendingBeforeRedeem);

            Response.Write(string.Format("<Result>\r\n  <ReturnCode>{0}</ReturnCode>\r\n  <ResultDescription>{1}</ResultDescription>\r\n <AvailableCreditRedemptions>{2}</AvailableCreditRedemptions>\r\n </Result>", 0, "success", redeemAmounts.EarnedAvailable));
        }
    }
}
