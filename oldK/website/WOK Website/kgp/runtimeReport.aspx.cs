///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class runtimeReport : KgpBasePage
    {
        #region Declarations

        private enum ReportTypes
        {
            Running, // report sent on state change to running
            Stopped, // report sent on state change to stopped
            Crashed, // report sent on state change to crashed
            ZoneRuntime   // report sent on application demand
        }

        // Shared report params
        private uint? _type = null;
        private double _runtime = 0;
        private ushort _runs = 0;
        private ushort _crashes = 0;
        private ulong _ulRuntimeId = 0;
        private string _runtimeId = null;
        private string _systemId = null;
        private string _appName = null;
        private string _appVer = null;
        private string _ipAddress = null;

        // Static system configuration params
        private uint? _cpus = null;
        private uint? _cpuMhz = null;
        private ulong? _sysMem = null;
        private ulong? _diskSize = null;
        private uint? _gpuX = null;
        private uint? _gpuY = null;
        private uint? _gpuHz = null;
        private uint? _gpuFmt = null;
        private string _dxVer = null;
        private string _cpuVer = null;
        private string _sysVer = null;
        private string _sysName = null;
        private string _net = null;
        private string _netMAC = null;
        private string _netType = null;
        private string _netHash = null;
        private string _gpu = null;
        private UserSystemConfiguration _userSystemConfiguration = null;

        // Client runtime params
        private int _userId = 0;
        private uint? _testGroup = 0;
        private uint? _ripperDetected = 0;
        private ulong? _diskAvail = null;
        private double? _appCpuTime = null;
        private uint? _appMemNum = null;
        private ulong? _appMemAvg = null;
        private ulong? _appMemMin = null;
        private ulong? _appMemMax = null;
        private ulong? _texMemNum = null;
        private ulong? _texMemAvg = null;
        private ulong? _texMemMin = null;
        private ulong? _texMemMax = null;
        private double? _pingMs = null;
        private double _fps = 0;
        private string _shutdownType = null;
        private double? _msgMoveEnabledTimeSec = null;

        // New CPU params
        private uint? _cpuPhysCore = null;
        private string _cpuVendor = null;
        private string _cpuBrand = null;
        private uint? _cpuFamily = null;
        private uint? _cpuModel = null;
        private uint? _cpuStepping = null;
        private uint? _cpuSSE3 = null;
        private uint? _cpuSSSE3 = null;
        private uint? _cpuSSE4_1 = null;
        private uint? _cpuSSE4_2 = null;
        private uint? _cpuSSE4_A = null;
        private uint? _cpuSSE5 = null;
        private uint? _cpuAVX = null;
        private uint? _cpuAVX2 = null;
        private uint? _cpuFMA3 = null;
        private uint? _cpuFMA4 = null;
        private string _cpuCodeName = null;

        // Launcher runtime params
        private ushort? _patchRv = null;
        private ushort _DL_NumOk;
        private ushort _DL_NumErr;
        private double _DL_Ms;
        private uint _DL_Bytes;
        private ushort _DL_Waits;
        private ushort _DL_Retries;
        private ushort _DL_RetriesErr;
        private ushort _ZIP_NumOk;
        private ushort _ZIP_NumErr;
        private double _ZIP_Ms;
        private uint _ZIP_Bytes;
        private ushort _PAK_NumOk;
        private ushort _PAK_NumErr;
        private double _PAK_Ms;
        private uint _PAK_Bytes;
        private string _playNowGuid = null;
        private string _patchUrl = null;
        private double? _patchMs = null;

        // Client runtime webcall params
        private string _webCallCompletedKey = "webCalls";
        private string _webCallsMsKey = "webCallsMs";
        private string _webCallsResponseMsKey = "webCallsResponseMs";
        private string _webCallsBytesKey = "webCallsBytes";
        private string _webCallsErroredKey = "webCallsErrored";
        private string _webCallRequestKey = "{0}.{1}";
        
        // Client zone runtime params
        private uint _gameId = 0;
        private uint _zoneIndex = 0;
        private uint _zoneInstanceId = 0;
        private uint _zoneType = 0;
        private uint _mediaCount = 0;
        private uint _texMem = 0;
        private double? _zoneTimeMs = null;
        private double? _avgDo = null;
        private double? _minDo = null;
        private double? _maxDo = null;
        private double? _avgPoly = null;
        private double? _minPoly = null;
        private double? _maxPoly = null;    

        // KanevaMediaFlash properties
        private int? _flashCrashes = null;
        private int? _flashPeakInstances = null;
 
        // Only valid apps should be processed.
        private HashSet<string> _validRuntimeAppNames = new HashSet<string>
        {
            "KEPClient",
            "KEPServer",
            "KanevaLauncher",
            "KanevaTray",
            "KanevaMediaFlash"
        };

        // Response types
        private string _successResult = "<Result><RuntimeId>{0}</RuntimeId>{1}</Result>";
        private string _errorResult = "<Result>\r\n  <ReturnCode>{0}</ReturnCode>\r\n  <ReturnDescription>{1}</ReturnDescription>\r\n </Result>";

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Get parameters
                GetRequestParams();

                // Make sure we at least have a report type to branch
                if (_type == null)
                {
                    Response.Write(string.Format(_errorResult, -1, "type must be specified"));
                    return;
                }

                // Branch via app name
                if (string.IsNullOrWhiteSpace(_appName))
                {
                    Response.Write(string.Format(_errorResult, -1, "appName must be specified"));
                }
                else if (_appName == "KEPClient")
                {
                    // Branch via report type
                    switch (_type)
                    {
                        case (uint)ReportTypes.Running:
                            ProcessClientRunningReport();
                            break;
                        case (uint)ReportTypes.Stopped:
                            ProcessClientStoppedReport();
                            break;
                        case (uint)ReportTypes.Crashed:
                            ProcessClientCrashedReport();
                            break;
                        case (uint)ReportTypes.ZoneRuntime:
                            ProcessZonePerformanceReport();
                            break;
                    }
                }
                else if (_validRuntimeAppNames.Contains(_appName))
                {
                    // Branch via report type
                    switch (_type)
                    {
                        case (uint)ReportTypes.Running:
                            ProcessGenericRunningReport();
                            break;
                        case (uint)ReportTypes.Stopped:
                            ProcessGenericStoppedReport("Stopped");
                            break;
                        case (uint)ReportTypes.Crashed:
                            ProcessGenericStoppedReport("Crashed");
                            break;
                    }
                }
                else
                {
                    Response.Write(string.Format(_errorResult, -1, "appName invalid"));
                }
            }
        }

        /// <summary>
        /// Sets page variables using either passed in values or appropriate defaults
        /// </summary>
        private void GetRequestParams()
        {
            int intOut = 0;
            uint uIntOut = 0;
            ulong uLongOut = 0;
            ushort uShortOut = 0;
            double dOut = 0;

            _gameId             = UInt32.TryParse(Request.Params["gameId"], out uIntOut) ? uIntOut : (uint)KanevaGlobals.WokGameId;

            // Just for clarity, if in a 3dApp, leave zone info defaulted to 0
            if (_gameId == (uint)KanevaGlobals.WokGameId)
            {
                _zoneIndex = UInt32.TryParse(Request.Params["zoneIndex"], out uIntOut) ? uIntOut : 0;
                _zoneInstanceId = UInt32.TryParse(Request.Params["zoneInstanceId"], out uIntOut) ? uIntOut : 0;
                _zoneType = UInt32.TryParse(Request.Params["zoneType"], out uIntOut) ? uIntOut : 0;
            }

            _userId             = Int32.TryParse(Request.Params["userId"], out intOut) ? intOut : 0;
            _flashCrashes       = Int32.TryParse(Request.Params["flashCrashes"], out intOut) ? (int?)intOut : null;
            _flashPeakInstances = Int32.TryParse(Request.Params["flashPeakInstances"], out intOut) ? (int?)intOut : null;
            _runtime            = Double.TryParse(Request.Params["runtime"], out dOut) ? dOut : 0;
            _runs               = UInt16.TryParse(Request.Params["runs"], out uShortOut) ? uShortOut : (ushort)0;
            _crashes            = UInt16.TryParse(Request.Params["crashes"], out uShortOut) ? uShortOut : (ushort)0;
            _type               = UInt32.TryParse(Request.Params["type"], out uIntOut) ? (uint?)uIntOut : null;
            _testGroup          = UInt32.TryParse(Request.Params["testGroup"], out uIntOut) ? (uint?)uIntOut : 0;
            _ripperDetected     = UInt32.TryParse(Request.Params["ripperDetected"], out uIntOut) ? (uint?)uIntOut : 0;
            _diskAvail          = UInt64.TryParse(Request.Params["diskAvail"], out uLongOut) ? (ulong?)uLongOut : null;
            _pingMs             = Double.TryParse(Request.Params["pingMs"], out dOut) ? (double?)dOut : null;
            _cpus               = UInt32.TryParse(Request.Params["cpus"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuMhz             = UInt32.TryParse(Request.Params["cpuMhz"], out uIntOut) ? (uint?)uIntOut : null;
            _sysMem             = UInt64.TryParse(Request.Params["sysMem"], out uLongOut) ? (ulong?)uLongOut : null;
            _diskSize           = UInt64.TryParse(Request.Params["diskSize"], out uLongOut) ? (ulong?)uLongOut : null;
            _gpuX               = UInt32.TryParse(Request.Params["gpuX"], out uIntOut) ? (uint?)uIntOut : null;
            _gpuY               = UInt32.TryParse(Request.Params["gpuY"], out uIntOut) ? (uint?)uIntOut : null;
            _gpuHz              = UInt32.TryParse(Request.Params["gpuHz"], out uIntOut) ? (uint?)uIntOut : null;
            _gpuFmt             = UInt32.TryParse(Request.Params["gpuFmt"], out uIntOut) ? (uint?)uIntOut : null;
            _appCpuTime         = Double.TryParse(Request.Params["appCpuTime"], out dOut) ? (double?)dOut : null;
            _appMemNum          = UInt32.TryParse(Request.Params["appMemNum"], out uIntOut) ? (uint?)uIntOut : null;
            _appMemAvg          = UInt64.TryParse(Request.Params["appMemAvg"], out uLongOut) ? (ulong?)uLongOut : null;
            _appMemMin          = UInt64.TryParse(Request.Params["appMemMin"], out uLongOut) ? (ulong?)uLongOut : null;
            _appMemMax          = UInt64.TryParse(Request.Params["appMemMax"], out uLongOut) ? (ulong?)uLongOut : null;
            _texMemNum          = UInt64.TryParse(Request.Params["texMemNum"], out uLongOut) ? (ulong?)uLongOut : null;
            _texMemAvg          = UInt64.TryParse(Request.Params["texMemAvg"], out uLongOut) ? (ulong?)uLongOut : null;
            _texMemMin          = UInt64.TryParse(Request.Params["texMemMin"], out uLongOut) ? (ulong?)uLongOut : null;
            _texMemMax          = UInt64.TryParse(Request.Params["texMemMax"], out uLongOut) ? (ulong?)uLongOut : null;
            _patchRv            = UInt16.TryParse(Request.Params["patchRv"], out uShortOut) ? uShortOut : (ushort)0;
            _DL_NumOk           = UInt16.TryParse(Request.Params["DL.NumOk"], out uShortOut) ? uShortOut : (ushort)0;
            _DL_NumErr          = UInt16.TryParse(Request.Params["DL.NumErr"], out uShortOut) ? uShortOut : (ushort)0;
            _DL_Ms              = Double.TryParse(Request.Params["DL.Ms"], out dOut) ? dOut : 0;
            _DL_Bytes           = UInt32.TryParse(Request.Params["DL.Bytes"], out uIntOut) ? (uint)uIntOut : 0;
            _DL_Waits           = UInt16.TryParse(Request.Params["DL.Waits"], out uShortOut) ? uShortOut : (ushort)0;
            _DL_Retries         = UInt16.TryParse(Request.Params["DL.Retries"], out uShortOut) ? uShortOut : (ushort)0;
            _DL_RetriesErr      = UInt16.TryParse(Request.Params["DL.RetriesErr"], out uShortOut) ? uShortOut : (ushort)0;
            _ZIP_NumOk          = UInt16.TryParse(Request.Params["ZIP.NumOk"], out uShortOut) ? uShortOut : (ushort)0;
            _ZIP_NumErr         = UInt16.TryParse(Request.Params["ZIP.NumErr"], out uShortOut) ? uShortOut : (ushort)0;
            _ZIP_Ms             = Double.TryParse(Request.Params["ZIP.Ms"], out dOut) ? dOut : 0;
            _ZIP_Bytes          = UInt32.TryParse(Request.Params["ZIP.Bytes"], out uIntOut) ? (uint)uIntOut : 0;
            _PAK_NumOk          = UInt16.TryParse(Request.Params["PAK.NumOk"], out uShortOut) ? uShortOut : (ushort)0;
            _PAK_NumErr         = UInt16.TryParse(Request.Params["PAK.NumErr"], out uShortOut) ? uShortOut : (ushort)0;
            _PAK_Ms             = Double.TryParse(Request.Params["PAK.Ms"], out dOut) ? dOut : 0;
            _PAK_Bytes          = UInt32.TryParse(Request.Params["PAK.Bytes"], out uIntOut) ? (uint)uIntOut : 0;
            _fps                = Double.TryParse(Request.Params["fps"], out dOut) ? dOut : 0;
            _zoneTimeMs         = Double.TryParse(Request.Params["zoneTimeMs"], out dOut) ? (double?)dOut : null;
            _msgMoveEnabledTimeSec = Double.TryParse(Request.Params["msgMoveEnabledTimeSec"], out dOut) ? (double?)dOut : null;
            _mediaCount         = UInt32.TryParse(Request.Params["mediaCount"], out uIntOut) ? (uint)uIntOut : 0;
            _ulRuntimeId        = UInt64.TryParse(Request.Params["runtimeId"], out uLongOut) ? uLongOut : 0;
            _avgDo              = Double.TryParse(Request.Params["avgDo"], out dOut) ? (double?)dOut : null;
            _minDo              = Double.TryParse(Request.Params["minDo"], out dOut) ? (double?)dOut : null;
            _maxDo              = Double.TryParse(Request.Params["maxDo"], out dOut) ? (double?)dOut : null;
            _avgPoly            = Double.TryParse(Request.Params["avgPoly"], out dOut) ? (double?)dOut : null;
            _minPoly            = Double.TryParse(Request.Params["minPoly"], out dOut) ? (double?)dOut : null;
            _maxPoly            = Double.TryParse(Request.Params["maxPoly"], out dOut) ? (double?)dOut : null;
            _patchMs            = Double.TryParse(Request.Params["patchMs"], out dOut) ? (double?)dOut : null;
            _texMem             = UInt32.TryParse(Request.Params["texMem"], out uIntOut) ? (uint)uIntOut : 0;
            _dxVer              = Request.Params["dxVer"];
            _appName            = Request.Params["appName"];
            _appVer             = Request.Params["appVer"];
            _cpuVer             = Request.Params["cpuVer"];
            _sysVer             = Request.Params["sysVer"];
            _sysName            = Request.Params["sysName"];
            _net                = Request.Params["net"];
            _netMAC             = Request.Params["netMAC"];
            _netType            = Request.Params["netType"];
            _netHash            = Request.Params["netHash"];
            _gpu                = Request.Params["gpu"];
            _runtimeId          = Request.Params["runtimeId"];
            _systemId           = Request.Params["systemId"] ?? string.Empty;   // Default to empty, save some null checks
            _playNowGuid        = Request.Params["playNowGuid"];
            _ipAddress          = Common.GetVisitorIPAddress();
            _patchUrl           = Request.Params["patchUrl"];
            // New CPU params.
            _cpuPhysCore        = UInt32.TryParse(Request.Params["cpuPhysCore"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuVendor          = Request.Params["cpuVendor"];
            _cpuBrand           = Request.Params["cpuBrand"];
            _cpuFamily          = UInt32.TryParse(Request.Params["cpuFamily"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuModel           = UInt32.TryParse(Request.Params["cpuModel"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuStepping        = UInt32.TryParse(Request.Params["cpuStepping"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuSSE3            = UInt32.TryParse(Request.Params["cpuSSE3"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuSSSE3           = UInt32.TryParse(Request.Params["cpuSSSE3"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuSSE4_1          = UInt32.TryParse(Request.Params["cpuSSE4_1"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuSSE4_2          = UInt32.TryParse(Request.Params["cpuSSE4_2"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuSSE4_A          = UInt32.TryParse(Request.Params["cpuSSE4_A"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuSSE5            = UInt32.TryParse(Request.Params["cpuSSE5"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuAVX             = UInt32.TryParse(Request.Params["cpuAVX"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuAVX2            = UInt32.TryParse(Request.Params["cpuAVX2"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuFMA3            = UInt32.TryParse(Request.Params["cpuFMA3"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuFMA4            = UInt32.TryParse(Request.Params["cpuFMA4"], out uIntOut) ? (uint?)uIntOut : null;
            _cpuCodeName = Request.Params["cpuCodeName"];
        }

        private string GetSystemIdTag(string systemId)
        {
            string tag;
            if (string.IsNullOrWhiteSpace(systemId))
                tag = string.Empty;
            else
                tag = string.Format("<SystemId>{0}</SystemId>", systemId);

            return tag;
        }

        /// <summary>
        /// Retrieves the user's stored system configuration, inserting or updating as necessary
        /// </summary>
        private void ProcessSystemConfigurationReport()
        {
            // Record and retrieve the system info
            _userSystemConfiguration = GetUserFacade.InsertUserSystemConfiguration(_systemId, _cpus, _cpuMhz, _sysMem, _diskSize, _gpuX, _gpuY,
                _gpuHz, _gpuFmt, _dxVer, _cpuVer, _sysVer, _sysName, _net, _netMAC, _netType, _netHash, _gpu,
                _cpuPhysCore, _cpuVendor, _cpuBrand, _cpuFamily, _cpuModel, _cpuStepping, _cpuSSE3, _cpuSSSE3, _cpuSSE4_1, _cpuSSE4_2, _cpuSSE4_A,
                _cpuSSE5, _cpuAVX, _cpuAVX2, _cpuFMA3, _cpuFMA4, _cpuCodeName);
        }

        #region Client Reports

        /// <summary>
        /// Inserts a new record used in calculating Mean Time Between Failure
        /// </summary>
        private void ProcessClientMtbfReport()
        {
            GetMetricsFacade.InsertClientMtbfLog(_userId, _runtime, _runs, _crashes, _appVer, _runtimeId);
        }

        /// <summary>
        /// Runs the set of reports that are generated upon app startup
        /// </summary>
        private void ProcessClientRunningReport()
        {
            // Process system info
            ProcessSystemConfigurationReport();

            // Insert runtime log
            _runtimeId = GetMetricsFacade.InsertClientRuntimePerformanceLog(_runtimeId, _userId, _userSystemConfiguration.SystemConfigurationId, _appVer, _testGroup, _diskAvail, _pingMs, _ripperDetected);

            // Write the response
            if (string.IsNullOrWhiteSpace(_runtimeId))
            {
                Response.Write(string.Format(_errorResult, -1, "Unable to process Running Report."));
            }
            else
            {
                // Process mtbf
                ProcessClientMtbfReport();

                // Record IP
                GetMetricsFacade.InsertClientRuntimeIP(_runtimeId, _ipAddress);

                // Write "SystemId" from configuration here.  May differ from passed _systemId.
                Response.Write(string.Format(_successResult, _runtimeId, GetSystemIdTag(_userSystemConfiguration.SystemId)));
            }
        }

        /// <summary>
        /// Runs the set of reports that are generated upon app stop
        /// </summary>
        private void ProcessClientStoppedReport()
        {
            // Set shutdown type
            _shutdownType   = "Stopped";

            // Write the response
            if (string.IsNullOrWhiteSpace(_runtimeId))
            {
                Response.Write(string.Format(_errorResult, -1, "Unable to process Stopped Report."));
            }
            else
            {
                // Process mtbf
                ProcessClientMtbfReport();

                // Record IP
                GetMetricsFacade.InsertClientRuntimeIP(_runtimeId, _ipAddress);

                // Update the log
                GetMetricsFacade.UpdateClientRuntimePerformanceLog(_runtimeId, _userId, _appCpuTime, _appMemNum, _appMemAvg, _appMemMin, _appMemMax, _texMemNum,
                    _texMemAvg, _texMemMin, _texMemMax, _fps, _shutdownType, _ripperDetected, _msgMoveEnabledTimeSec);

                // Update system configuration last user
                GetUserFacade.UpdateSystemConfigurationLastUserId(_runtimeId);

                // Process web calls
                ProcessClientWebCallReport();

                Response.Write(string.Format(_successResult, _runtimeId, GetSystemIdTag(_systemId)));
            }
        }

        /// <summary>
        /// Runs the set of reports that are generated upon app crash
        /// </summary>
        private void ProcessClientCrashedReport()
        {
            // Set shutdown type
            _shutdownType = "Crashed";

            // Write the response
            if (string.IsNullOrWhiteSpace(_runtimeId))
            {
                Response.Write(string.Format(_errorResult, -1, "Unable to process Crashed Report."));
            }
            else
            {
                // Process mtbf
                ProcessClientMtbfReport();

                // Record IP
                GetMetricsFacade.InsertClientRuntimeIP(_runtimeId, _ipAddress);

                // Update the log
                GetMetricsFacade.UpdateClientRuntimePerformanceLog(_runtimeId, _userId, _appCpuTime, _appMemNum, _appMemAvg, _appMemMin, _appMemMax, _texMemNum,
                    _texMemAvg, _texMemMin, _texMemMax, _fps, _shutdownType, _ripperDetected, _msgMoveEnabledTimeSec);

                // Update system configuration last user
                GetUserFacade.UpdateSystemConfigurationLastUserId(_runtimeId);

                // Process web calls
                ProcessClientWebCallReport();

                Response.Write(string.Format(_successResult, _runtimeId, GetSystemIdTag(_systemId)));
            }
        }

        /// <summary>
        /// Processes webcall reports
        /// </summary>
        private void ProcessClientWebCallReport()
        {
            if (!string.IsNullOrWhiteSpace(_runtimeId))
            {
                var webCallTypes = Request.Params.Cast<string>()
                    .Where(key => key.Contains(_webCallCompletedKey) || key.Contains(_webCallsMsKey) || key.Contains(_webCallsResponseMsKey) || key.Contains(_webCallsBytesKey) || key.Contains(_webCallsErroredKey))
                    .Select(key => key.Substring(0, key.IndexOf('.')))
                    .Distinct();

                foreach (string webCallType in webCallTypes)
                {
                    double doubleOut = 0;
                    uint uIntOut = 0;

                    uint webCalls = UInt32.TryParse(Request.Params[string.Format(_webCallRequestKey, webCallType, _webCallCompletedKey)], out uIntOut) ? uIntOut : 0;
                    double webCallsMs = Double.TryParse(Request.Params[string.Format(_webCallRequestKey, webCallType, _webCallsMsKey)], out doubleOut) ? doubleOut : 0;
                    double webCallsResponseMs = Double.TryParse(Request.Params[string.Format(_webCallRequestKey, webCallType, _webCallsResponseMsKey)], out doubleOut) ? doubleOut : 0;
                    uint webCallsBytes = UInt32.TryParse(Request.Params[string.Format(_webCallRequestKey, webCallType, _webCallsBytesKey)], out uIntOut) ? uIntOut : 0;
                    uint webCallsErrored = UInt32.TryParse(Request.Params[string.Format(_webCallRequestKey, webCallType, _webCallsErroredKey)], out uIntOut) ? uIntOut : 0;

                    GetMetricsFacade.InsertClientWebCallPerformanceLog(_runtimeId, webCallType, webCalls, webCallsMs, webCallsResponseMs, webCallsBytes, webCallsErrored);
                }
            }
        }

        /// <summary>
        /// Process zone performance reports
        /// </summary>
        private void ProcessZonePerformanceReport()
        {
            if (!string.IsNullOrWhiteSpace(_runtimeId))
            {
                if ((_gameId > 0 && _gameId != KanevaGlobals.WokGameId) || (_zoneIndex > 0 && _zoneInstanceId > 0 && _zoneType > 0))
                {
                    GetMetricsFacade.InsertClientZonePerformanceLog(_runtimeId, _gameId, _zoneIndex, _zoneInstanceId, _zoneType, _pingMs, _fps, _zoneTimeMs,
                        _avgDo, _minDo, _maxDo, _avgPoly, _minPoly, _maxPoly, _mediaCount, _texMem);
                }

                if (_userId > 0)
                {
                    GetMetricsFacade.UpdateClientRuntimePerformanceLogUserId(_runtimeId, _userId);
                    GetUserFacade.UpdateSystemConfigurationLastUserId(_runtimeId);
                }

                Response.Write(string.Format(_successResult, _runtimeId, GetSystemIdTag(_systemId)));
            }
            else
            {
                Response.Write(string.Format(_errorResult, -1, "Unable to process Zone Performance Report."));
            }
        }

        #endregion

        #region Generic Reports

        /// <summary>
        /// Records a generic running report.
        /// </summary>
        private void ProcessGenericRunningReport()
        {
            // Validation
            if (string.IsNullOrWhiteSpace(_appName))
            {
                Response.Write(string.Format(_errorResult, -1, "appName not specified or invalid."));
                return;
            }

            // Record that the launcher was started from a Play Now button (if it was)
            if (_appName == "KanevaLauncher" && !string.IsNullOrWhiteSpace(_playNowGuid))
            {
                GetGameFacade.SetPlayNowStartedKey(_playNowGuid);
            }

            // Grab the runtimeId
            _runtimeId = Guid.NewGuid().ToString();

            // Process system info
            ProcessSystemConfigurationReport();

            // Prepare data values for MongoDB
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("runtimeId", _runtimeId);
            data.Add("shutdownType", "Running");
            data.Add("systemConfigurationId", _userSystemConfiguration.SystemConfigurationId);
            data.Add("appVer", _appVer);
            data.Add("diskAvail", (_diskAvail ?? (object)"").ToString());
            data.Add("ipAddress", _ipAddress);
            data.Add("mtbfRuntime", _runtime.ToString());
            data.Add("mtbfRuns", _runs.ToString());
            data.Add("mtbfCrashes", _crashes.ToString());

            // Insert the placement record
            GetMetricsFacade.InsertGenericMetricsLog(_appName + "_runtime_reports", data, Global.RequestRabbitMQChannel());

            // Write "SystemId" from configuration here.  May differ from passed _systemId.
            Response.Write(string.Format(_successResult, _runtimeId, GetSystemIdTag(_userSystemConfiguration.SystemId)));
        }

        /// <summary>
        /// Records a generic running report.
        /// </summary>
        private void ProcessGenericStoppedReport(string shutdownType)
        {
            // Validation
            if (string.IsNullOrWhiteSpace(_appName))
            {
                Response.Write(string.Format(_errorResult, -1, "appName not specified or invalid."));
                return;
            }

            if (string.IsNullOrWhiteSpace(_runtimeId))
            {
                Response.Write(string.Format(_errorResult, -1, "Unable to process " + shutdownType + " Report."));
            }
            else
            {
                // Prepare data values for MongoDB
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("runtimeId", _runtimeId);
                data.Add("shutdownType", shutdownType);
                data.Add("appVer", _appVer);
                data.Add("appCpuTime", (_appCpuTime ?? (object)"").ToString());
                data.Add("appMemNum", (_appMemNum ?? (object)"").ToString());
                data.Add("appMemAvg", (_appMemAvg ?? (object)"").ToString());
                data.Add("appMemMin", (_appMemMin ?? (object)"").ToString());
                data.Add("appMemMax", (_appMemMax ?? (object)"").ToString());
                data.Add("flashCrashes", (_flashCrashes ?? (object)"").ToString());
                data.Add("flashPeakInstances", (_flashPeakInstances ?? (object)"").ToString());
                data.Add("ipAddress", _ipAddress);
                data.Add("mtbfRuntime", _runtime.ToString());
                data.Add("mtbfRuns", _runs.ToString());
                data.Add("mtbfCrashes", _crashes.ToString());

                GetMetricsFacade.InsertGenericMetricsLog(_appName + "_runtime_reports", data, Global.RequestRabbitMQChannel());

                Response.Write(string.Format(_successResult, _runtimeId, GetSystemIdTag(_systemId)));
            }
        }

        #endregion
    }
}
