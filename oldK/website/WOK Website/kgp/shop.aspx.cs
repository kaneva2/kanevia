///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;
using log4net;
using System.Security.Cryptography;
using System.Web.Security;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for shop.
    /// </summary>
    public class shop : KgpBasePage
    {
        #region Declarations

        // Shared report params
        private string _action = null;

        // Response types
        private string _errorResult = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>{0}</ResultDescription>\r\n</Result>";

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(true))
            {
                // Get parameters
                GetRequestParams();

                // Branch by action.
                switch (_action)
                {
                    case "finalizeUGCUpload":
                        ProcessFinalizeUGCUpload();
                        break;
                    default:
                        Response.Write(string.Format(_errorResult, "action not specified"));
                        break;
                }
            }

            m_logger.Debug ("END - shop.PageLoad() for UserId = " + m_userId);
        }

        /// <summary>
        /// Sets page variables using either passed in values or appropriate defaults.
        /// </summary>
        private void GetRequestParams()
        {
            _action = Request.Params["action"];
        }

        #region Actions

        /// <summary>
        /// Processes the finalizeUGCUpload action.
        /// </summary>
        private void ProcessFinalizeUGCUpload()
        {
            // Confirm user permissions. For now, this is ony open to automated testing users.
            if (!GetUserFacade.IsUserAutomatedTestAccount(KanevaWebGlobals.CurrentUser))
            {
                Response.Write(string.Format(_errorResult, "Unauthorized user."));
                return;
            }

            // Get request params.
            string itemName = Request.Params["itemName"];
            string itemDesc = Request.Params["itemDesc"];
            string itemKW = Request.Params["itemKW"];

            string currency = Request.Params["currency"] ?? Currency.CurrencyType.REWARDS;
            if (!currency.Equals(Currency.CurrencyType.CREDITS, StringComparison.InvariantCultureIgnoreCase) &&
                !currency.Equals(Currency.CurrencyType.REWARDS, StringComparison.InvariantCultureIgnoreCase))
            {
                Response.Write(string.Format(_errorResult, "Invalid value for param currency. Should be CREDITS or REWARDS."));
                return;
            }

            bool derivable;
            if (!bool.TryParse(Request.Params["derivable"] ?? "true", out derivable))
            {
                Response.Write(string.Format(_errorResult, "Invalid value for param derivable. Should be true or false."));
                return;
            }

            bool blast;
            if (!bool.TryParse(Request.Params["blast"] ?? "false", out blast))
            {
                Response.Write(string.Format(_errorResult, "Invalid value for param blast. Should be true or false."));
                return;
            }

            uint designerPrice;
            if (!uint.TryParse(Request.Params["designerPrice"] ?? "0", out designerPrice))
            {
                Response.Write(string.Format(_errorResult, "Invalid value for param designerPrice. Should be an unsigned integer."));
                return;
            }

            int itemPassType;
            if (!int.TryParse(Request.Params["itemPassType"] ?? "0", out itemPassType))
            {
                Response.Write(string.Format(_errorResult, "Invalid value for param itemPassType. Should be 1 for AP, 0 otherwise."));
                return;
            }

            int uploadId;
            if (!int.TryParse(Request.Params["uploadId"] ?? string.Empty, out uploadId))
            {
                Response.Write(string.Format(_errorResult, "Invalid value for param uploadId. Should be an unsigned integer."));
                return;
            }

            int categoryId = 0;
            if (!int.TryParse(Request.Params["category"] ?? "0", out categoryId))
            {
                Response.Write(string.Format(_errorResult, "Invalid value for param category. Should be an unsigned integer."));
                return;
            }

            // Get the upload item from the uploadId.
            UGCUpload uploadItem;
            IList<UGCUpload> uploadList =  UGCUtility.GetUploadedUGCList(KanevaWebGlobals.CurrentUser.UserId, uploadId, true) ?? new List<UGCUpload>();
            uploadItem = uploadList.SingleOrDefault(u => u.ID == uploadId);
            if (uploadItem == null)
            {
                Response.Write(string.Format(_errorResult, "No uploads found matching uploadId {0}.", uploadId));
                return;
            }

            uploadItem.AnimTargetActorGLID = UGCUtility.GetTargetActorGlid(uploadItem);     
            uploadItem.DefaultGlid = Request["defaultGlid"] ?? string.Empty;

            // Since this is only for test users atm, force uploads to private.
            WOKItem.ItemActiveStates itemStatus = WOKItem.ItemActiveStates.Private;

            // Complete the upload
            string errMsg;
            UGCUtility.FinalizeUGCUploadToShop(KanevaWebGlobals.CurrentUser, uploadItem, derivable, itemName, itemDesc,
                itemKW, Common.GetVisitorIPAddress(), categoryId, itemPassType,
                itemStatus, designerPrice, currency, blast, out errMsg);

            // Send the new token as response.
            if (string.IsNullOrWhiteSpace(errMsg))
            {
                string successResponse = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Upload successful.</ResultDescription>\r\n </Result>";
                Response.Write(string.Format(successResponse));
            }
            else
            {
                Response.Write(string.Format(_errorResult, "Upload failed. " + errMsg));
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
