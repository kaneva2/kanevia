///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class getAppMessageQueue : KgpBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string wokDb = KanevaGlobals.DbNameKGP;

            if (Request.Params["gameId"] == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>provide  gameId</ReturnDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }


            int gameId = Int32.Parse(Request.Params["gameId"].ToString());

            ////// Make sure they are the owner
            ////// Check they own the zone
            ////if (!WOKStoreUtility.IsZoneOwner(KanevaWebGlobals.CurrentUser.UserId, gameId))
            ////{
            ////    string errorStr = "<Result>\r\n  <ReturnCode>-2</ReturnCode>\r\n  <ReturnDescription>You must be the zone owner to continue</ReturnDescription>\r\n</Result>";
            ////    Response.Write(errorStr);
            ////    return;
            ////}



            GameFacade gameFacade = new GameFacade();

            DataTable dt = gameFacade.GetAppMessageQueue(gameId);
            dt.TableName = "Message";

            // result set
            DataSet ds = new DataSet("Result");
            ds.Tables.Add(dt.Copy());

            // setup the result in the header
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = dt.Rows.Count.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);


        }
    }
}