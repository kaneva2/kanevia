///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Xml;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;
using System.Collections.Generic;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /*
    <Result>
        <ReturnCode>0</ReturnCode>
        <ReturnDescription>success</ReturnDescription>
        <TotalNumberRecords>21</TotalNumberRecords>
        <table>
            <filename>TutWelcome_1503.xml</filename>
            <attributes>
                <camera>false</camera>
                <jump>false</jump>
                <modal>true</modal>
                <move>false</move>
                <timer>1</timer>
            </attributes>
        </table>
        <table>
            <filename>TutMove_1503.xml</filename>
            <attributes>
                <camera>false</camera>
                <jump>false</jump>
                <modal>false</modal>
                <move>true</move>
                <timer>3</timer>
            </attributes>
        </table>
    </Result>
  */
    public partial class tutorial : KgpBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CheckUserId(false))
            {
                try
                {
                    string actionreq = "GetTutorial";

                    if (Request.Params["action"] != null)
                    {
                        actionreq = Request.Params["action"];
                    }

                    if (actionreq.Equals("GetTutorial"))
                    {
                        int userId = KanevaWebGlobals.CurrentUser.UserId;

                        // For A/B test - 
                        string experimentId = "";

                        // Check to see if there is a currently active tutorial experiment
                        PagedList<Experiment> userExperiment = GetExperimentFacade.SearchExperiments("", false, "Tutorial", EXPERIMENT_STATUS_TYPE.STARTED, "", 1, 5, false);

                        // If there is an experiment, then get the experiment Id
                        if (userExperiment.Count > 0)
                        {
                            experimentId = userExperiment[0].ExperimentId;
                        }

                        // Using the experiment Id, get the test group the user belongs to
                        UserExperimentGroup userExperimentGroup = GetExperimentFacade.GetUserExperimentGroupCommon(KanevaWebGlobals.CurrentUser.UserId, experimentId);
                        string groupId = userExperimentGroup != null ? userExperimentGroup.GroupId : "";

                        DataTable dtTutorialPages = GetGameFacade.GetTutorial(groupId);

                        GetTutorialXml(dtTutorialPages);
                    }
                }
                catch (Exception exc)
                {
                    m_logger.Warn("Error getting tutorial: " + exc);
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Error retrieving tutorial</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }
            }
            else
            {
                string errorStr = "<Result>\r\n<ReturnCode>-4</ReturnCode>\r\n<ResultDescription>User must be in world to use this web call</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }
        }

        private void GetTutorialXml(DataTable dtTutorialPages)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode root = doc.CreateElement("Result");

            XmlElement elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";
            root.AppendChild(elem);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";
            root.AppendChild(elem);

            XmlElement tableElem = null;
            XmlElement attrValElem = null;
            string prevFilename = "";
            string filename = "";
            int i = 0;

            foreach (DataRow row in dtTutorialPages.Rows)
            {
                filename = row["file_name"].ToString();

                if (prevFilename != filename)
                {
                    i++;
                    if (prevFilename != "")
                    {
                        root.AppendChild(tableElem);
                    }

                    tableElem = doc.CreateElement("table");
                    elem = doc.CreateElement("filename");

                    elem.InnerText = filename;
                    tableElem.AppendChild(elem);

                    // Create Attributes node
                    attrValElem = doc.CreateElement(row["attribute_name"].ToString());
                    attrValElem.InnerText = row["attribute_value"].ToString();
                    tableElem.AppendChild(attrValElem);
                }
                else
                {
                    attrValElem = doc.CreateElement(row["attribute_name"].ToString());
                    attrValElem.InnerText = row["attribute_value"].ToString();
                    tableElem.AppendChild(attrValElem);
                }

                prevFilename = filename;
            }

            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = i.ToString();
            root.InsertAfter(elem, root.FirstChild.NextSibling);

            root.AppendChild(tableElem);

            Response.Write(root.OuterXml);
        }

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    }


}