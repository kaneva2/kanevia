///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;

using log4net;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Diagnostics;
using System.Collections.Generic;

/* Use a view to solve base global ID problem for UGC items
 * 
create view vw_dynamic_objects_w_baseGLID as
select obj_placement_id, player_id, zone_index, zone_instance_id, object_id, inventory_sub_type, 
	do.global_id as global_id, position_x, position_y, position_z, rotation_x, rotation_y, rotation_z, 
	texture_asset_id, texture_url, asset_group_id, swf_name, swf_parameter, friend_id, 
	created_date, expired_date, if(ifnull(itm.base_global_id, 0)=0, do.global_id, itm.base_global_id) as base_global_id
from dynamic_objects do left join items itm
on do.global_id=itm.global_id;
 */

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// get lists of dynamic objects for a zone
    /// </summary>
    public class zoneCustomizations : KgpBasePage
    {
        const int MESH_PATH_TYPE_ID = 1004;
        const int TEX_PATH_TYPE_ID = 2002;

        private void Page_Load(object sender, System.EventArgs e)
        {
            string wokDb = KanevaGlobals.DbNameKGP;

            Response.Cache.SetNoServerCaching();
            Response.Cache.SetNoStore();

            if (Request.Params["zoneIndex"] == null || Request.Params["instanceId"] == null)
            {
                Response.Write("r\t-1\tzoneIndex or instanceId not specified\n");
                return;
            }

            ZoneIndex zoneIndex = new ZoneIndex(Int32.Parse(Request.Params["zoneIndex"].ToString()));
            int instanceId = Int32.Parse(Request.Params["instanceId"].ToString());

            // if permanent zone or arena, always set instanceId to 1 
            if (zoneIndex.IsPermanent || zoneIndex.IsArena)
                instanceId = 1;

            float x = 0, y = 0, z = 0;
            float _x = 0, _y = 0, _z = 0;
            if (Request.Params["x"] != null && float.TryParse(Request.Params["x"], out _x) &&
                Request.Params["y"] != null && float.TryParse(Request.Params["y"], out _y) &&
                Request.Params["z"] != null && float.TryParse(Request.Params["z"], out _z))
            {
                x = _x;
                y = _y;
                z = _z;
            }
            getZoneCustomizations(zoneIndex, instanceId, x, y, z);
            return;
        }

        private void getZoneDownloadSizes(ZoneIndex zoneIndex, int instanceId, out uint textureSize, out uint meshSize, out uint meshInstSize)
        {
            textureSize = meshSize = meshInstSize = 0;

            IDictionary<uint, uint[]> downloadSizeDict = GetGameFacade.GetZoneDownloadSizes(zoneIndex.ZoneIdx, instanceId, MESH_PATH_TYPE_ID, TEX_PATH_TYPE_ID);
            if (downloadSizeDict != null)
            {
                if (downloadSizeDict.ContainsKey(MESH_PATH_TYPE_ID))
                {
                    meshSize = downloadSizeDict[MESH_PATH_TYPE_ID][0];
                    meshInstSize = downloadSizeDict[MESH_PATH_TYPE_ID][1];
                }
                if (downloadSizeDict.ContainsKey(TEX_PATH_TYPE_ID))
                {
                    textureSize = downloadSizeDict[TEX_PATH_TYPE_ID][0];
                }
            }
        }

        private void getZoneCustomizations(ZoneIndex zoneIndex, int instanceId, float x, float y, float z)
        {
            // Zone download size - textures, meshes and mesh instances
            uint textureSize, meshSize, meshInstSize;
            getZoneDownloadSizes(zoneIndex, instanceId, out textureSize, out meshSize, out meshInstSize);

            // Zone customization data
            DataSet ds = GetGameFacade.GetZoneCustomizations(zoneIndex.ZoneIdx, instanceId, x, y, z);
            if (ds != null)
            {
                if (ds.Tables.Count != 2)
                {
                    m_logger.Error("GetZoneCustomizations returned " + ds.Tables.Count.ToString() + " tables, expected: 2");
                }

                // table 0: dynamic objects
                int numDynamicObjects = 0;
                if (ds.Tables.Count > 0)
                {
                    numDynamicObjects = ds.Tables[0].Rows.Count;
                }

                // table 1: world object settings
                int numWorldObjectSettings = 0;
                if (ds.Tables.Count > 1)
                {
                    numWorldObjectSettings = ds.Tables[1].Rows.Count;
                }

                bool frameworkEnabled = GetGameFacade.IsFrameworkEnabled(instanceId, zoneIndex.ZoneType);

                // r = response
                Response.Write("r\t0\tok\t" + 
                    numDynamicObjects.ToString() + "\t" + 
                    numWorldObjectSettings.ToString() + "\t" + 
                    (frameworkEnabled ? "1" : "0") + "\t" +
                    textureSize.ToString() + "\t" +
                    meshSize.ToString() + "\t" + 
                    meshInstSize.ToString() + "\n"
                );

                foreach (DataTable t in ds.Tables)
                {
                    foreach (DataRow r in t.Rows)
                    {
                        if (r[0].GetType().FullName.Equals("System.Byte[]"))
                        {
                            Response.Write(System.Text.ASCIIEncoding.ASCII.GetString(((byte [])r[0]))); // will write p = parameter, d = dyn, w = world
                        }
                        else if (r[0].GetType().FullName.Equals("System.String"))
                        {
                            Response.Write(r[0].ToString());
                        }
                        else
                        {
                            m_logger.Error("Unknown Database type returned from SP " + r[0].GetType().FullName);
                        }
                    }
                }
            }
            else
            {
                Response.Write("r\t2\tdatabase error\n");
            }

            Response.Write("z\n"); // end 
        }

        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
