///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// get the details about a deed, a.k.a. apartment_template from the starting_dynamic_object,
    /// starting_dynamic_object_parameters, and starting_world_object_settings tables.
    /// </summary>
    public class getSpace : KgpBasePage
    {
		private void Page_Load(object sender, System.EventArgs e)
		{
			string wokDb = KanevaGlobals.DbNameKGP;

			if (Request.Params["zoneIndex"] == null || Request.Params["instanceId"] == null || Request.Params["playerId"] == null)
			{
				string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>provide correct zoneIndex, instanceId, and playerId</ReturnDescription>\r\n</Result>";
				Response.Write(errorStr);
				return;
			}

			int zoneIndex = Int32.Parse(Request.Params["zoneIndex"].ToString());
			int instanceId = Int32.Parse(Request.Params["instanceId"].ToString());
			int playerId = Int32.Parse(Request.Params["playerId"].ToString());

			ShoppingFacade shoppingFacade = new ShoppingFacade();

			DataTable dt = shoppingFacade.GetSpaceDynamicObjects(playerId, zoneIndex, instanceId);
			dt.TableName = "item";

			// result set
			DataSet ds = new DataSet ("Result");
			ds.Tables.Add (dt.Copy());

			// Get the ids so we can check for associated parameters
			string ids = "";
			for (int i = 0; i < dt.Rows.Count; i++) {
				ids += dt.Rows[i]["obj_placement_id"].ToString () + ",";   
			}
			// If we have item glids, then check for associated parameters
			if (ids.Length > 0) {
				ids = ids.Remove (ids.Length - 1);
				DataTable dtDynParams = shoppingFacade.GetSpaceDynamicObjectParams(ids);
				dtDynParams.TableName = "parameter";

				// add the animation datatable to dataset
				ds.Tables.Add (dtDynParams.Copy ());

				// Get the primary key column from the master table
				DataColumn primarykey = ds.Tables["item"].Columns["obj_placement_id"];

				// Get the primary key column from the master table
				DataColumn foreignkey = ds.Tables["parameter"].Columns["obj_placement_id"];

				//Assign relation
				DataRelation relation = ds.Relations.Add (primarykey, foreignkey);

				// Get ADO.NET to generate nexted XML nodes
				relation.Nested = true;
			}


			DataTable dtWorldObjectSettings = shoppingFacade.GetSpaceWorldObjectSettings(playerId, zoneIndex, instanceId);
			dtWorldObjectSettings.TableName = "WorldObjectSetting";
			ds.Tables.Add (dtWorldObjectSettings.Copy());

			// setup the result in the header
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(ds.GetXml());
			XmlNode root = doc.DocumentElement;

			XmlElement elem = doc.CreateElement("ReturnDescription");
			elem.InnerText = "success";

			root.InsertBefore(elem, root.FirstChild);

			elem = doc.CreateElement("ReturnCode");
			elem.InnerText = "0";

			root.InsertBefore(elem, root.FirstChild);

			Response.Write(root.OuterXml);
		}

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
