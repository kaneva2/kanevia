///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Xml.Serialization;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Represents a response to the client fame. 
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(NotificationsProfile))]
    public class ProfileNotificationsResponse
    {

        /// <summary>
        /// NotificationsProfile
        /// </summary>
        public List<NotificationsProfile> np;
    }
}
