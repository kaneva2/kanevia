﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Web;
using System.Web.UI;
using System.Data;
using log4net;
using System.Linq;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class buyGameItem : KgpBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             if (Request.Params["action"] == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            string actionreq = Request.Params["action"];
            UserFacade userFacade = new UserFacade();

            if (actionreq.Equals("BuyGameItem"))
            {
                if ((Request.Params["KEPPointId"] == null) || (Request.Params["zoneInstanceId"] == null) || (Request.Params["zoneType"] == null) || (Request.Params["amount"] == null))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-2</ReturnCode>\r\n  <ResultDescription>KEPPointId, zoneInstanceId, zoneType or amount not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                int userId = GetUserId();
                int zoneInstanceId = 0;
                int zoneType = 0;
                int ownerId = 0;
                double amount = 0.0;
                string KEPPointId = "";
                int successfulPayment = -1;

                try
                {
                    zoneInstanceId = int.Parse(Request.Params["zoneInstanceId"]);
                    zoneType = int.Parse(Request.Params["zoneType"]);
                    amount = double.Parse(Request.Params["amount"]);
                    KEPPointId = Request.Params["KEPPointId"].ToString ();
                }
                catch (Exception)
                {
                    Response.Write("<Result>\r\n  <ReturnCode>-3</ReturnCode>\r\n  <ResultDescription>Invalid Parameter format</ResultDescription>\r\n</Result>");
                    return;
                }

                // Look up world owner (Home is 3)
                DataRow drChannelZone = WOKStoreUtility.GetHangoutChannelZone(zoneInstanceId, zoneType.Equals(Constants.APARTMENT_ZONE));

                if (drChannelZone == null)
                {
                    Response.Write("<Result>\r\n  <ReturnCode>-4</ReturnCode>\r\n  <ResultDescription>Could not find Zone</ResultDescription>\r\n</Result>");
                    return;
                }

                ownerId = Convert.ToInt32 (drChannelZone ["kaneva_user_id"]);

                // Give out the splits
                //1.       A web call to remove credits from a player, 
                //              perform a percentage split on it (10% to the world, 90% to Kaneva (sink)), 
                //              enforce a max pull per call (40,000), 
                //              give these credits to the world’s “credit redemption queue” system and log this action so we can run reports on it
                //2.       A web call to remove rewards from a player, 
                //              perform a percentage split on it (70% to the world owner, 30% to Kaneva (sink)), 
                //              enforce a max pull per call (40,000), 
                //              add these rewards directly to the owner’s reward pool and log this action so we can run reports on it

                if (KEPPointId.Equals(Constants.CURR_KPOINT))
                {
                    // Require community id and global id
                    int globalId = 3006826;
                    int communityId = 0;

                    if (zoneType.Equals(Constants.APARTMENT_ZONE))
                    {
                        User owner = GetUserFacade.GetUser(ownerId);
                        communityId = owner.HomeCommunityId;
                    }
                    else
                    { 
                        communityId = Convert.ToInt32 (drChannelZone["zone_instance_id"]);
                    }

                    //if ((Request.Params["communityId"] == null))
                    //{
                    //    string errorStr = "<Result>\r\n  <ReturnCode>-2</ReturnCode>\r\n  <ResultDescriptioncommunityId is required for type credit</ResultDescription>\r\n</Result>";
                    //    Response.Write(errorStr);
                    //    return;
                    //}
                    //else
                    //{
                    //    communityId = int.Parse(Request.Params["communityId"]);
                    //}

                    // Credits
                    successfulPayment = userFacade.AdjustUserBalance(userId, Constants.CURR_KPOINT, -amount, Constants.TT_GAME_ITEM);

                    if (successfulPayment == 0)  //0 is success according to AdjustUserBalance notes
                    {
                        // Dont' do this immediately
                        //userFacade.AdjustUserBalance(ownerId, Constants.CURR_KPOINT, Math.Round(amount * .1), Constants.TT_GAME_ITEM_COMMISION);

                        // Add it to their history
                        int purchaseId = GetShoppingFacade.AddItemPurchase(globalId, (int)ItemPurchase.eItemTxnType.PURCHASE, userId, (uint)ownerId, (int) amount, (uint)Math.Round(amount * .7), Common.GetVisitorIPAddress(), Constants.CURR_KPOINT, 1, 0);

                        // Add record for redit redemption
                        if (purchaseId > 0)
                        {
                            GetShoppingFacade.InsertPremiumItemCreditRedemption(purchaseId, communityId);
                        }
                    }
                    else
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>-6</ReturnCode>\r\n  <ResultDescription>Payment failure</ResultDescription>\r\n</Result>");
                        return;
                    }
                }
                else if (KEPPointId.Equals(Constants.CURR_GPOINT))
                {
                    // Rewards
                    successfulPayment = userFacade.AdjustUserBalance(userId, Constants.CURR_GPOINT, -amount, Constants.TT_GAME_ITEM);

                    if (successfulPayment == 0)  //0 is success according to AdjustUserBalance notes
                    {
                        userFacade.AdjustUserBalance(ownerId, Constants.CURR_GPOINT, Math.Round(amount * .1), Constants.TT_GAME_ITEM_COMMISION);
                    }
                    else
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>-6</ReturnCode>\r\n  <ResultDescription>Payment failure</ResultDescription>\r\n</Result>");
                        return;
                    }
                }
                else
                {
                    Response.Write("<Result>\r\n  <ReturnCode>-5</ReturnCode>\r\n  <ResultDescription>Unknown or Unsupported KEI Point ID</ResultDescription>\r\n</Result>");
                    return;
                }

                Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Success</ResultDescription>\r\n</Result>");
                return;
            }


        }
    }
}