///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for friendsList.
	/// </summary>
	public class updateFriend : KgpBasePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
				if ( (Request.Params["userId"] == null && Request.Params["username"] == null ) || (Request.Params["action"] == null) )
				{
					string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>user id and action not specified</ResultDescription>\r\n</Result>";
					Response.Write(errorStr);
					return;
				}

				string action = Request.Params["action"];
				int friendId;

				if (Request.Params["userId"] != null)
				{
					friendId = Int32.Parse(Request.Params["userId"].ToString());
				}
				else
				{              
                    friendId = GetUserFacade.GetUserIdFromUsername (Request.Params["username"].ToString ());
				}

				int status = -1;
				string responseStr = null;

				if ( action != null && action.Equals("Add") )
				{
                    if (!GetUserFacade.IsUserBlocked (friendId, m_userId))
                    {
                        status = GetUserFacade.InsertFriendRequest (m_userId, friendId);

                        if (status == 1)
                        {
                            // even though 1 is success, 0 is our standard for success in wok web. 
                            // that's why the return code looks flipped here.
                            responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";

                        }
                        else if (status == 2)
                        {
                            responseStr = "<Result>\r\n  <ReturnCode>1</ReturnCode>\r\n  <ResultDescription>already requested to be friends</ResultDescription>\r\n</Result>";
                        }
                        else if (status == 0)
                        {
                            responseStr = "<Result>\r\n  <ReturnCode>1</ReturnCode>\r\n  <ResultDescription>already friends</ResultDescription>\r\n</Result>";
                        }
                        else
                        {
                            responseStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Unknown error</ResultDescription>\r\n</Result>";
                        }
                    }
                    else
                    {
                        responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
                    }
				}
				else if ( action != null && action.Equals("Remove") )
				{
                    status = GetUserFacade.DeleteFriend(m_userId, friendId);

					if ( status == 1)
					{
						responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
					}
					else
					{
						responseStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Not friends or unknown error</ResultDescription>\r\n</Result>";
					}
				}
				else
				{
					responseStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid action specified</ResultDescription>\r\n</Result>";
					Response.Write(responseStr);
					return;
				}

				Response.Write(responseStr);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
