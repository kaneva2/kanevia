///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class gameList : KgpBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CheckUserId(false))
            {
                if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>page number, or items per page not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }
                int page = Int32.Parse(Request.Params["start"].ToString());
                int items_per_page = Int32.Parse(Request.Params["max"].ToString());

				string orderBy = "";
				string orderByDirection = "";
				string orderbyclause = "name";

				if ( Request.Params["orderBy"] != null)
				{
					orderBy = Request.Params["orderBy"];

                    if (Request.Params["orderByDirection"] != null)
                    {
                        orderByDirection = Request.Params["orderByDirection"];

                        if (orderByDirection.Equals("DESC"))
                        {
                            orderByDirection = " DESC ";
                        }
                        else
                        {
                            orderByDirection = " ASC ";
                        }
                    }
                    if (orderBy == "raves")
                        orderbyclause = "raves" + orderByDirection;
                    else if ( orderBy == "population" )
                        orderbyclause = "population" + orderByDirection;
                    else if ( orderBy == "name" )
                        orderbyclause = "name" + orderByDirection;
                }

                GameFacade gamefacade = new GameFacade();
                global::Kaneva.DataLayer.DataObjects.PagedDataTable pdt = null;
                pdt = gamefacade.GetGames(page, items_per_page, orderbyclause);

                DataSet ds = new DataSet();

                ds.DataSetName = "Result";

                pdt.TableName = "game";
                ds.Tables.Add(pdt);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(ds.GetXml());
                XmlNode root = doc.DocumentElement;

                int numRecords = pdt.Rows.Count;
                XmlElement elem = doc.CreateElement("NumberRecords");
                elem.InnerText = numRecords.ToString();

                root.InsertBefore(elem, root.FirstChild);

                uint totalNumRecords = pdt.TotalCount;
                elem = doc.CreateElement("TotalNumberRecords");
                elem.InnerText = totalNumRecords.ToString();

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("ReturnDescription");
                elem.InnerText = "success";

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("ReturnCode");
                elem.InnerText = "0";

                root.InsertBefore(elem, root.FirstChild);

                Response.Write(root.OuterXml);


            }

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
