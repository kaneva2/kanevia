///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;


namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for friendsList.
	/// </summary>
	public class ravePlayer : KgpBasePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
                string errorStr = "";
                string actionreq = Request.Params["action"];
                UserFacade userFacade = new UserFacade();

                if (Request.Params["action"] == null)
                {
                    actionreq = "SetRave"; // default no action to "SetRave" for backwards compatibility
                }

                if (actionreq.Equals("SetRave"))
                {

                    if (Request.Params["userId"] == null)
                    {
                        errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>user id not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                    if (Request.Params["raveType"] == null)
                    {
                        errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>rave type not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                    if (Request.Params["currency"] == null)
                    {
                        errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>currency not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int toRaveUserId = Int32.Parse(Request.Params["userId"].ToString());
                    string raveType = Request.Params["raveType"].ToString();
                    string currency = Request.Params["currency"].ToString().ToUpper();
                    

                    if (toRaveUserId.Equals(0) && Request.Params["username"] != null)
                    {
                        DataRow dr = userFacade.GetUserIdPlayerIdFromUsername(Request.Params["username"].ToString ());

                        if (dr != null)
                        {
                            try
                            {
                                toRaveUserId = Convert.ToInt32(dr["user_id"]);
                            }
                            catch (Exception)
                            {
                                errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Error looking up username</ResultDescription>\r\n</Result>";
                                Response.Write(errorStr);
                                return;
                            }
                        }
                        else
                        {
                            errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>username not found</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    }

                    // Get the currency
                    if (Currency.CurrencyType.CREDITS != currency && Currency.CurrencyType.REWARDS != currency && !currency.Equals("FREE"))
                    {
                        // Error - Non-supported currency 
                        errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>unsupported currency type</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    
                    int status = RaveProfile(m_userId, userFacade.GetPersonalChannelId(toRaveUserId), raveType, currency);
                    
                    string responseStr = null;

                    if (status == 0)
                    {
                        m_logger.Debug("ravePlayer.aspx:Page_Load: Sent player raved success. RavedUserID=" + toRaveUserId);
                        responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
                    }
                    else if (status == 1 || status == 2)
                    {
                        responseStr = "<Result>\r\n  <ReturnCode>1</ReturnCode>\r\n  <ResultDescription>not enough rewards</ResultDescription>\r\n</Result>";
                    }
                    else if (status == 3 || status == 4)
                    {
                        responseStr = "<Result>\r\n  <ReturnCode>3</ReturnCode>\r\n  <ResultDescription>not enough credits</ResultDescription>\r\n</Result>";
                    }
                    else
                    {
                        responseStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Unknown error</ResultDescription>\r\n</Result>";
                    }

                    Response.Write(responseStr);
                }
                else if (actionreq.Equals("GetRaveInformation"))
                {
                    XmlDocument doc = new XmlDocument();
                    XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");
                    XmlElement elem = doc.CreateElement("ReturnCode");
                    
                    IList<RaveType> raveTypes = GetRaveInformation();

                    if (raveTypes != null)
                    {
                        elem.InnerText = "0";
                        root.InsertBefore(elem, root.FirstChild);
                       
                        elem = doc.CreateElement("ResultDescription");
                        elem.InnerText = "success";
                        root.InsertAfter(elem, root.LastChild);

                        foreach (RaveType rt in raveTypes)
                        {
                            XmlNode raveNode = doc.CreateNode(XmlNodeType.Element, "RaveType", "");
                            XmlElement subElement = doc.CreateElement("Name");
                            subElement.InnerText = rt.Name;
                            raveNode.InsertAfter(subElement, raveNode.LastChild);

                            subElement = doc.CreateElement("Description");
                            subElement.InnerText = rt.Description;
                            raveNode.InsertAfter(subElement, raveNode.LastChild);

                            subElement = doc.CreateElement("PriceSingleRave");
                            subElement.InnerText = rt.PriceSingleRave.ToString();
                            raveNode.InsertAfter(subElement, raveNode.LastChild);

                            subElement = doc.CreateElement("PriceMegaRave");
                            subElement.InnerText = rt.PriceMegaRave.ToString();
                            raveNode.InsertAfter(subElement, raveNode.LastChild);
                            
                            root.InsertAfter(raveNode, root.LastChild);
                        }
                    }
                    else
                    {
                        elem.InnerText = "-1";
                        root.InsertAfter(elem, root.LastChild);

                        elem = doc.CreateElement("ReturnDescription");
                        elem.InnerText = "No rave information exists.";
                        root.InsertAfter(elem, root.LastChild);
                    }

                    Response.Write(root.OuterXml);
                }
                else
                {
                    errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action request unknown</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
	}
}
