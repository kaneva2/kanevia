///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Collections;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;
using System.Diagnostics;

#region Calling Convention
/*
Must login with a Admin account

1) Return path type information
URL: ugcconvhelper.aspx?action=type&path_type={pathtype}
 
2) Return a list of assets for processing. Only assets that are not yet converted will be returned

URL: ugcconvhelper.aspx?action=query&cur_path_type={currpathtype}&new_path_type={newpathtype}creator_id={creatorid}&min_glid={minglid}&min_ord={minord}&max_glid={maxglid}&max_count={maxcount}
 * optionally a `conv_id' parameter can be provided to help filter out completed items - see item_asset_conversions table
 * optionally a `reconvert={oldconvid}' parameter can be provided for querying previously converted items

Use pseudo path type: texture_iw to access items_web.template_path_encrypted column

<Result>
	<UGCRecord>
		<global_id>{global_id}</global_id>
		<use_type>{usetype}</use_type>
		<path_type>{pathtype}</path_type>
		<path>{path}</path>
        <file_size>{file_size}</file_size>
        <file_hash>{file_hash}</file_hash>
		<creator_id>{creator_id}</creator_id>
		<base_global_id>{base_global_id}</base_global_id>
	</UGCRecord>
	...
</Result>

3) Update an item (also copy records for UGC DO items if a template item is being committed)
 * optionally a conv_id parameter can be provided to record the result in the DB log

URL: ugcconvhelper.aspx?action=update&glid={glid}&path_type={pathtype}&path={path}&size={size}&hash={hash}&image={imagetype}&ordinal={ordinal}
 * optional parameters: media={0/1}&collision={0/1}&minx={minx}&miny={miny}&minz={minz}&maxx={maxx}&maxy={maxy}&maxz={maxz}

<result>
	<ReturnCode>0</ReturnCode>		<!-- Return code: 0 = OK, else = failed. -->
	<ResultDescription>Success</ResultDescription>
</result>

4) Update dynamic object metadata only (for testing)
URL: ugcconvhelper.aspx?action=updatedynobj&glid={glid}&media={0/1}&collision={0/1}&minx={minx}&miny={miny}&minz={minz}&maxx={maxx}&maxy={maxy}&maxz={maxz}

5) Update texture metadata only (for testing)
URL: ugcconvhelper.aspx?action=updatedynobj&glid={glid}&ordinal={ordinal}&width={width}&height={height}&opacity={opacity}&color={color}
 */
#endregion

namespace KlausEnt.KEP.Kaneva.kgp
{
	public class ugcConvHelper : KgpBasePage
	{
		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string PATH_TYPE_MESH = "mesh";
        private const string PATH_TYPE_NONE = "none";
        private const uint MIN_UGCDO_BASEGLID = 1000000000;
        private const uint MIN_UGC_GLID = 3000000;
        private const string STATUS_COMPLETED = "COMPLETED";
        private const string STATUS_SKIPPED = "SKIPPED";
        private enum MetaPathType { NONE, MESH, SOUND, ANIMATION, EQUIP, PARTICLE, TEXTURE_TEMPLATE, TEXTURE };

        private string resultXml;

        private MetaPathType getMetaPathType(string pathType)
        {
            if (pathType.StartsWith("mesh"))
                return MetaPathType.MESH;
            if (pathType.StartsWith("sound"))
                return MetaPathType.SOUND;
            if (pathType.StartsWith("anim"))
                return MetaPathType.ANIMATION;
            if (pathType.StartsWith("equip"))
                return MetaPathType.EQUIP;
            if (pathType.StartsWith("particle"))
                return MetaPathType.PARTICLE;
            if (pathType.StartsWith("template") || pathType.StartsWith("tem_"))
                return MetaPathType.TEXTURE_TEMPLATE;
            if (pathType.StartsWith("texture") || pathType.StartsWith("tex_"))
                return MetaPathType.TEXTURE;
            return MetaPathType.NONE;
        }

        protected string EncodeResult(int code, string description)
        {
            return "<Result><ReturnCode>" + code.ToString() + "</ReturnCode><ResultDescription>" + description + "</ResultDescription></Result>";
        }

        protected void Page_Load(object sender, EventArgs e)
		{
            // Administrators only (kaneva.users.role bitand 8)
			if (!CheckUserId(true) || !IsAdministrator())
				return;

            Response.ContentType = "text/xml";

            try
            {
                string action;
                getParameter("action", out action);

                switch (action)
                {
                    case "type":
                        // Return path type info
                        QueryPathType();
                        break;
                    case "query":
                        // Return a list of conversion candidates
                        QueryConversionList();
                        break;
                    case "unique":
                        // Allocate and return a new (or existing) unique ID based on SHA256 hash
                        GetUniqueIdOrPath();
                        break;
                    case "update":
                        // Update item tables after conversion
                        UpdateConversionResult();
                        break;
                    case "updatedynobj":
                        // For testing only - update item_dynamic_objects table - included in "update"
                        UpdateDynamicObjectMetadata();
                        break;
                    case "updateanimation":
                        // For testing only - update item_parameters table - included in "update"
                        UpdateAnimationMetadata();
                        break;
                    case "updatesound":
                        // For testing only - update item_parameters table - included in "update"
                        UpdateSoundMetadata();
                        break;
                    case "updatetexture":
                        // For testing only - update item_path_textures table - included in "update"
                        UpdateTextureMetadata();
                        break;
                }
            }
            catch (ArgumentException ex)
            {
                resultXml = EncodeResult(-1, ex.Message);
            }

            Response.Write(resultXml);
        }

        protected void QueryPathType()
        {
            string sPathType;

            if (!getParameter("path_type", false, out sPathType))
            {
                resultXml = EncodeResult(-1, "Missing path type parameter");
                return;
            }

            ItemPathType info = GetPathTypeInfo(sPathType);
            if (info == null)
            {
                resultXml = EncodeResult(-1, "Invalid path type: " + sPathType);
                return;
            }

            resultXml = info.toXml("Result");
        }

        protected void QueryConversionList()
		{
			string creatorFilter = null;
            string zoneFilter = null;
            string filestoreJoin = "";
            string filestoreFilter = null;
			string minGlidFilter = null;
			string maxGlidFilter = null;
			string maxCountFilter = null;
            string convLogJoin = "";
            string convLogFilter = null;
            string existingItemPathJoin = "";
            string existingItemPathFilter = null;
            
            int convId;
            try
            {
                // Optional `conv_id' parameter
                if (!getParameter("conv_id", false, out convId))
                {
                    convId = -1;
                }
            }
            catch (ArgumentException ex)
            {
                resultXml = EncodeResult(-1, ex.Message);
                return;
            }

			string sCurPathType, sNewPathType;
            if (!getParameter("cur_path_type", false, out sCurPathType) || !getParameter("new_path_type", false, out sNewPathType))
            {
                resultXml = EncodeResult(-1, "Missing path type parameter (both current and new path types are required)");
                return;
            }

            ItemPathType curPathType = GetPathTypeInfo(sCurPathType);
            if (curPathType == null)
            {
                resultXml = EncodeResult(-1, "Invalid path type : " + sCurPathType);
                return;
            }

            ItemPathType newPathType = GetPathTypeInfo(sNewPathType);
            if (newPathType == null)
            {
                resultXml = EncodeResult(-1, "Invalid path type : " + sNewPathType);
                return;
            }

            int oldConvId;
            string filestore = null;
            try
            {
                // New convert v.s. reconvert
                if (!getParameter("reconvert", false, out oldConvId))
                {
                    oldConvId = -1;    // default value
                }

                // Determine conversion log join clause based on conv_id and reconvert flag
                if (oldConvId == -1)
                {
                    // New convert
                    if (convId != -1)
                    {
                        // Negative join conversion log to filter out any item that have been converted
                        convLogJoin = "LEFT OUTER JOIN item_asset_conversion_logs cl ON cl.conv_id=@ConvId and cl.global_id = ip.global_id AND cl.type_id=@NewPathTypeId AND cl.ordinal=ip.ordinal ";
                        convLogFilter = "cl.global_id IS NULL ";        // filter out items that have been converted
                    }

                    // Negative join item_paths
                    existingItemPathJoin = "LEFT OUTER JOIN item_paths ip2 ON ip.global_id=ip2.global_id AND ip2.type_id=@NewPathTypeId AND ip.ordinal=ip2.ordinal ";
                    existingItemPathFilter = "ip2.type_id IS NULL";
                }
                else
                {
                    if (convId != -1)
                    {
                        resultXml = EncodeResult(-1, "Found both conv_id and reconvert parameters. Use only one at a time.");
                        return;
                    }

                    // Inner join with conversion log
                    convLogJoin = "INNER JOIN item_asset_conversion_logs cl ON cl.conv_id=@OldConvId and cl.global_id = ip.global_id AND cl.type_id=@NewPathTypeId AND cl.ordinal=ip.ordinal ";

                    // No join with existing item path records
                    existingItemPathJoin = "";
                }

                // Creator filter
                uint creatorId;
                if (getParameter("creator_id", false, out creatorId))
				{
					creatorFilter = "i.item_creator_id = " + creatorId.ToString();
				}

                // Zone filter
                uint zoneIndex, instanceId;
                if (getParameter("zone_index", false, out zoneIndex) && getParameter("instance_id", false, out instanceId))
                {
                    zoneFilter = "ip.global_id IN (SELECT distinct global_id FROM dynamic_objects WHERE zone_index = " + zoneIndex.ToString() + " AND zone_instance_id = " + instanceId.ToString() + ")";
                }

                // Filestore filter (for preview)
                if (getParameter("filestore", false, out filestore) && filestore != null)
                {
                    filestoreJoin = "INNER JOIN item_path_filestores fs ON ip.global_id=fs.global_id AND ip.type_id=fs.item_path_type_id ";
                    filestoreFilter = "fs.filestore = @Filestore";
                }

                // GLID filter
                uint minGlid;
                if (!getParameter("min_glid", false, out minGlid))
                {
                    // default value
                    minGlid = curPathType.isDynamicObjectMesh() ? MIN_UGCDO_BASEGLID : MIN_UGC_GLID;
                }
                if (curPathType.isDynamicObjectMesh() && minGlid < MIN_UGCDO_BASEGLID)
                {
                    resultXml = EncodeResult(-1, "Minimum GLID parameter for dynamic objects must be at least " + MIN_UGCDO_BASEGLID.ToString());
                    return;
                }

                Int64 minOrdinal;
                if (getParameter("min_ord", false, out minOrdinal))
                {
                    minGlidFilter = "(ip.global_id > " + minGlid.ToString() + " OR ip.global_id = " + minGlid.ToString() + " AND ip.ordinal >= " + minOrdinal.ToString() + ")";
                }
                else
                {
                    minGlidFilter = "ip.global_id >= " + minGlid.ToString();
                }

                uint maxGlid;
                if (getParameter("max_glid", false, out maxGlid))
                {
                    maxGlidFilter = "ip.global_id <= " + maxGlid.ToString();
                }

                // Maximum return count
                uint maxCount;
                if (getParameter("max_count", false, out maxCount))
                {
                    maxCountFilter = "LIMIT " + maxCount.ToString();
                }
            }
			catch (ArgumentException ex) 
            {
                resultXml = EncodeResult(-1, ex.Message);
                return;
            }

            string sql = null;
            sql = "SELECT ip.global_id, i.use_type, ipt.`type` path_type, ip.ordinal, ip.path, ip.file_size, ip.file_hash, i.item_creator_id, i.base_global_id " + 
                        "FROM item_paths ip " +
                        "INNER JOIN items i ON i.global_id = ip.global_id " +
                        "INNER JOIN item_path_types ipt ON ip.type_id = ipt.type_id " +
                        existingItemPathJoin + 
                        convLogJoin +
                        filestoreJoin +
                        "WHERE ip.type_id = @CurPathTypeId ";

            if (convLogFilter != null)
            {
                sql = sql + " AND " + convLogFilter;
            }

            if (existingItemPathFilter != null)
            {
                sql = sql + " AND " + existingItemPathFilter;
            }

            if (creatorFilter != null)
            {
                sql = sql + " AND " + creatorFilter;
            }

            if (zoneFilter != null)
            {
                sql = sql + " AND " + zoneFilter;
            }

            if (filestoreFilter != null)
            {
                sql = sql + " AND " + filestoreFilter;
            }

            if (minGlidFilter != null)
            {
                sql = sql + " AND " + minGlidFilter;
            }

            if (maxGlidFilter != null)
            {
                sql = sql + " AND " + maxGlidFilter;
            }

            sql = sql + " GROUP BY ip.global_id, ip.type_id, ip.ordinal";
            sql = sql + " ORDER BY ip.global_id, ip.ordinal";

            if (maxCountFilter != null)
            {
                sql = sql + " " + maxCountFilter;
            }

            Hashtable parameters = new Hashtable();
            parameters.Add("@CurPathTypeId", curPathType.TypeId);
            parameters.Add("@NewPathTypeId", newPathType.TypeId);
            if (convId != -1)
            {
                parameters.Add("@ConvId", convId);
            }
            if (oldConvId != -1)
            {
                parameters.Add("@OldConvId", oldConvId);
            }
            if (filestoreFilter != null)
            {
                Debug.Assert(filestore != null);
                parameters.Add("@Filestore", filestore);
            }

            DataTable dt = KanevaGlobals.GetDatabaseUtilityKGP().GetDataTable(sql, parameters);
            if (dt == null)
            {
                // No data
                resultXml = "<Result />";
                return;
            }

            try
            {
                dt.TableName = "UGCRecord";
                DataSet ds = new DataSet();
                ds.DataSetName = "Result";
                ds.Tables.Add(dt);
                resultXml = ds.GetXml();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error encoding results: " + ex.ToString());
                resultXml = EncodeResult(-1, "Error encoding results");
            }
		}

        protected void GetUniqueIdOrPath()
        {
            string sSHA256Hash;
            string sPathType;
            int size, compressedSize;

            getParameter("path_type", true, out sPathType);
            ItemPathType pathTypeInfo = GetPathTypeInfo(sPathType);
            if (pathTypeInfo == null)
            {
                throw new ArgumentException("Invalid path type: [" + sPathType + "]", "path_type");
            }

            uint uniqueAssetId;
            bool isNew = false;
            getParameter("unique_id", false, out uniqueAssetId);

            if (uniqueAssetId == 0)
            {
                // Unique ID not provided. Allocate a new one
                if (pathTypeInfo.StoreType != ItemPathType.AssetStoreType.UNIQUE)
                {
                    throw new ArgumentException("Asset store type is NOT unique. Path type: [" + sPathType + "]", "path_type");
                }

                getParameter("sha256", true, out sSHA256Hash);
                var sha256Hash = parseSHA256String(sSHA256Hash);
                if (sha256Hash == null)
                {
                    throw new ArgumentException("Error parsing SHA256 hash: [" + sSHA256Hash + "]", "sha256");
                }

                getParameter("size", true, out size);
                if (size == 0)
                {
                    throw new ArgumentException("Invalid size: [" + size + "]", "size");
                }

                getParameter("compressed_size", true, out compressedSize);
                if (compressedSize == 0)
                {
                    throw new ArgumentException("Invalid compressed size: [" + size + "]", "compressed_size");
                }

                uniqueAssetId = GetShoppingFacade.AllocUniqueAssetIdBySHA256(pathTypeInfo, sha256Hash, size, compressedSize, out isNew);
                if (uniqueAssetId == 0)
                {
                    resultXml = EncodeResult(-1, "Error allocating unique asset ID. Path type: [" + pathTypeInfo.Type + "], hash: [" + sSHA256Hash + "]");
                    return;
                }
            }

            string path = pathTypeInfo.getAssetPathUnique(uniqueAssetId);
            if (path == null || path == "")
            {
                resultXml = EncodeResult(-1, "Error generating relative path. Path type: [" + pathTypeInfo.Type + "], unique ID: [" + uniqueAssetId + "]");
                return;
            }

            path = UGCUtility.GetDeployFilestore(pathTypeInfo) + path;
            resultXml = "<Result><UniqueId>" + uniqueAssetId.ToString() + "</UniqueId><IsNew>" + (isNew?"1":"0") + "</IsNew><Path>" + path + "</Path></Result>";
        }

        protected void UpdateConversionResult()
		{
            try
            {
                uint glid;
                getParameter("glid", out glid);                 // required

                string sPathType;
                getParameter("path_type", out sPathType);       // required

                Int64 ordinal;
                if (!getParameter("ordinal", false, out ordinal))
                {
                    ordinal = -1;   // default value
                }

                int convId;
                if (!getParameter("conv_id", false, out convId))
                {
                    convId = -1;    // default value
                }

                // Reconvert: overwrite previous record (previous record must exist)
                int oldConvId;
                if (!getParameter("reconvert", false, out oldConvId))
                {
                    oldConvId = -1; // default value
                }

                int batchId;
                getParameter("batch_id", false, out batchId);

                ItemPathType pathTypeInfo;
                if (!ValidateGlidAndType(glid, sPathType, ordinal, oldConvId, out pathTypeInfo))
                {
                    return;
                }

                string sPath;
                getParameter("path", out sPath);                // required

                uint uniqueAssetId = 0;
                if (pathTypeInfo.StoreType == ItemPathType.AssetStoreType.UNIQUE || pathTypeInfo.StoreType == ItemPathType.AssetStoreType.AUX)
                {
                    getParameter("unique_id", out uniqueAssetId);   // required if unique asset store
                }

                string sHash = Request["hash"];                 // required, allow empty value
                if (sHash == null)
                {
                    throw new ArgumentException("Missing parameter.", "hash");
                }

                int dataSize;
                getParameter("size", out dataSize);             // required

                int compressedSize;
                getParameter("compressed_size", out compressedSize);    // required

                if (!ValidateConversionId(convId))
                {
                    return;
                }

                string sql = null;
                sql = "INSERT INTO `item_paths`(`global_id`, `type_id`, `ordinal`, `path`, `file_size`, `file_hash`, `compressed_size`, `unique_id`) " +
                      "VALUES (@globalId, @pathTypeId, @ordinal, @path, @fileSize, @fileHash, @compressedSize, @uniqueAssetId)";
                if (oldConvId != -1)
                {
                    sql = sql + " ON DUPLICATE KEY UPDATE `path`=@path, `file_size`=@fileSize, `file_hash`=@fileHash, `compressed_size`=@compressedSize, `unique_id`=@uniqueAssetId";

                    // QUOTE MySQL manual: "With ON DUPLICATE KEY UPDATE, the affected-rows value per row is 1 
                    // if the row is inserted as a new row and 2 if an existing row is updated.". In addition, 
                    // it's 0 if no update occurred (row unchanged).
                }

			    Hashtable parameters = new Hashtable();
			    parameters.Add("@globalId", glid);
                parameters.Add("@pathTypeId", pathTypeInfo.TypeId);
			    parameters.Add("@ordinal", ordinal);
			    parameters.Add("@path", sPath);
			    parameters.Add("@fileSize", dataSize);
			    parameters.Add("@fileHash", sHash);
                parameters.Add("@compressedSize", compressedSize);
                parameters.Add("@uniqueAssetId", uniqueAssetId);

                // Affected rows should be 1 for INSERT, 1 or 2 for REPLACE.
                int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sql, parameters);
			    if (rowsAffected == 0 && oldConvId == -1)
			    {
                    // If rowsAffected is 0 with plain INSERT, the insertion failed.
                    m_logger.Error("UpdateConversionResult failed, glid = " + glid.ToString() + ", path type = " + pathTypeInfo.TypeId + ", rows affected = " + rowsAffected.ToString());
                    resultXml = EncodeResult(-1, "Update failed, rows affected = " + rowsAffected.ToString());
                    return;
			    }

                try
			    {
				    if (getMetaPathType(sPathType) == MetaPathType.MESH && glid>=MIN_UGCDO_BASEGLID)
				    {
                        PropagateItemPathRecords(convId, batchId, glid, pathTypeInfo.TypeId, ordinal, sPath, dataSize, sHash, compressedSize, uniqueAssetId);
				    }
			    }
			    catch (Exception ex)
			    {
				    m_logger.Error("UpdateConversionResult: copy DO rows from base item failed: " + ex.ToString());
                    resultXml = EncodeResult(-1, "Copy DO rows from base item failed: " + ex.ToString());
                    return;
			    }

                if (convId != -1)
                {
                    // Write conversion log
                    if (!WriteConversionLog(convId, glid, pathTypeInfo.TypeId, ordinal, batchId, STATUS_COMPLETED))
                    {
                        m_logger.Error("Error writing conversion log for [" + glid.ToString() + "]: already logged?");
                        resultXml = EncodeResult(-1, "Error writing conversion log: already logged?");
                        return;
                    }
                }

                if (getMetaPathType(sPathType) == MetaPathType.MESH && !UpdateDynamicObjectMetadata())
                {
                    return;
                }

                if (getMetaPathType(sPathType) == MetaPathType.ANIMATION && !UpdateAnimationMetadata())
                {
                    return;
                }

                if (getMetaPathType(sPathType) == MetaPathType.SOUND && !UpdateSoundMetadata())
                {
                    return;
                }

                // Texture metadata is optional
                if (getMetaPathType(sPathType) == MetaPathType.TEXTURE && Request["metadata"]=="1" && !UpdateTextureMetadata())
                {
                    return;
                }
            }
            catch (ArgumentException ex)
            {
                resultXml = EncodeResult(-1, ex.Message);
                return;
            }

            resultXml = EncodeResult(0, "Success");
        }

        public void PropagateItemPathRecords(int convId, int batchId, uint glid, int pathTypeId, Int64 ordinal, string sPath, int fileSize, string sHash, int compressedSize, uint uniqueAssetId)
        {
            // Copy item_path records from base item to DO items
            string sqlList = "SELECT i.global_id FROM items i INNER JOIN items bi ON i.base_global_id=bi.global_id WHERE i.use_type=10 AND i.base_global_id=@globalId";
            Hashtable paramsList = new Hashtable();
            paramsList.Add("@globalId", glid);

            DataTable dtList = KanevaGlobals.GetDatabaseUtilityKGP().GetDataTable(sqlList, paramsList);
            if (dtList == null)
            {
                m_logger.Warn("UpdateConversionResult: DB return null DataTable while querying for derived items with glid = " + glid.ToString());
                return;
            }

            uint numCastingFailed = 0;

            foreach (DataRow row in dtList.Rows)
            {
                string sql =
                    "INSERT INTO `item_paths`(`global_id`, `type_id`, `ordinal`, `path`, `file_size`, `file_hash`, `compressed_size`, `unique_id`) " +
                    "VALUES (@globalId, @pathTypeId, @ordinal, @path, @fileSize, @fileHash, @compressedSize, @uniqueAssetId) " +
                    "ON DUPLICATE KEY UPDATE `path`=@path, `file_size`=@fileSize, `file_hash`=@fileHash, `compressed_size`=@compressedSize, `unique_id`=@uniqueAssetId";

                try
                {
                    uint toGlid = Convert.ToUInt32(row["global_id"]);

                    Hashtable parameters = new Hashtable();
                    parameters.Add("@globalId", toGlid);
                    parameters.Add("@pathTypeId", pathTypeId);
                    parameters.Add("@ordinal", ordinal);
                    parameters.Add("@path", sPath);
                    parameters.Add("@fileSize", fileSize);
                    parameters.Add("@fileHash", sHash);
                    parameters.Add("@compressedSize", compressedSize);
                    parameters.Add("@uniqueAssetId", uniqueAssetId);

                    int dynObjRowsInserted = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sql, parameters);
                    if (dynObjRowsInserted < 1)
                    {
                        m_logger.Info("PropagateItemPathRecords DB record not updated: from " + glid.ToString() + ", path type " + pathTypeId.ToString() + ", to " + toGlid.ToString());
                    }
                    else
                    {
                        WriteConversionLog(convId, toGlid, pathTypeId, ordinal, batchId, STATUS_COMPLETED);
                    }
                }
                catch (FormatException) {
                    numCastingFailed++;
                }
            }

            if (numCastingFailed > 0)
            {
                m_logger.Warn("PropagateItemPathRecords error casting returned global ID to number for " + numCastingFailed.ToString() + " times");
            }
        }

        protected bool ValidateGlidAndType(uint glid, string sPathType, Int64 ordinal, int oldConvId, out ItemPathType pathTypeInfo)
		{
            pathTypeInfo = null;

            // Obtain item_path_types.type_id
            if (sPathType != "" && sPathType != null)
            {
                pathTypeInfo = GetPathTypeInfo(sPathType);
            }

            if (pathTypeInfo == null)
            {
                resultXml = EncodeResult(-1, "Invalid path type: " + sPathType);
                return false;
            }

            if (glid < MIN_UGC_GLID || getMetaPathType(sPathType) == MetaPathType.MESH  && glid < MIN_UGCDO_BASEGLID)
			{
				resultXml = EncodeResult(-1, "Invalid global ID: " + glid.ToString() + " with type: " + sPathType);
                return false;
			}

            // check if this glid has been converted
            string sql =
                "SELECT i.global_id, ip.path, l.status FROM items i " +
                "LEFT JOIN item_paths ip ON i.global_id = ip.global_id AND ip.type_id = @pathTypeId AND ip.ordinal=@ordinal " + 
                "LEFT JOIN item_asset_conversion_logs l on l.conv_id=@oldConvId AND l.global_id=i.global_id AND l.type_id=@pathTypeId AND l.ordinal=@ordinal " +
                "WHERE i.global_id=@globalId;";

			Hashtable parameters = new Hashtable();
            parameters.Add("@pathTypeId", pathTypeInfo.TypeId);
			parameters.Add("@globalId", glid);
			parameters.Add("@ordinal", ordinal);
            parameters.Add("@oldConvId", oldConvId);        // l.status will be NULL if oldConvId is -1 (not provided)

			DataRow dr = KanevaGlobals.GetDatabaseUtilityKGP().GetDataRow(sql, parameters, false);
			if (dr == null)
			{
				resultXml = EncodeResult(-1, "Global ID not found: " + glid.ToString());
                return false;
			}

            if (oldConvId == -1 && !Convert.IsDBNull(dr["path"]))
            {
				resultXml = EncodeResult(-1, "Already converted: " + glid.ToString());
                return false;
			}

            if (oldConvId != -1 && Convert.IsDBNull(dr["status"]))
            {
                resultXml = EncodeResult(-1, "Reconvert: previous record not found for " + glid.ToString());
                return false;
            }

			return true;
		}

		protected bool ValidateConversionId(int convId)
        {
            if (convId != -1)
            {
                string sql = "SELECT conv_id FROM item_asset_conversions WHERE conv_id=@ConvId";
                Hashtable parameters = new Hashtable();
                parameters.Add("@ConvId", convId);

                DataRow dr = KanevaGlobals.GetDatabaseUtilityKGP().GetDataRow(sql, parameters, false);
                if (dr == null)
                {
                    resultXml = EncodeResult(-1, "Conversion ID not found: " + convId.ToString());
                    return false;
                }
            }

            return true;
        }

        protected bool WriteConversionLog(int convId, uint glid, int pathTypeId, Int64 ordinal, int batchId, string status)
        {
            try
            {
                string sql = "INSERT INTO item_asset_conversion_logs(conv_id, global_id, type_id, ordinal, batch_id, status) VALUES (@convId, @globalId, @pathTypeId, @ordinal, @batchId, @status)";
            
                Hashtable parameters = new Hashtable();
                parameters.Add("@convId", convId);
                parameters.Add("@globalId", glid);
                parameters.Add("@pathTypeId", pathTypeId);
                parameters.Add("@ordinal", ordinal);
                parameters.Add("@batchId", batchId);
                parameters.Add("@status", status);

                int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sql, parameters);
                return rowsAffected == 1;
            }
            catch (Exception ex) 
            {
                m_logger.Error("Error inserting into item_asset_conversion_log: " + ex.ToString());
                return false;
            }
        }

        protected bool UpdateDynamicObjectMetadata()
        {
            // Global ID
            uint glid = 0;
            try
            {
                getParameter("glid", out glid);
            }
            catch (Exception ex)
            {
                resultXml = EncodeResult(-1, "Error updating dynamic object metadata: " + ex.Message);
                return false;
            }

            // Check if record exists
            try
            {
                string sql = "SELECT 1 FROM item_dynamic_objects WHERE global_id=@globalId";
                Hashtable parameters = new Hashtable();
                parameters.Add("@globalId", glid);

                DataRow dr = KanevaGlobals.GetDatabaseUtilityKGP().GetDataRow(sql, parameters, false);
                if (dr == null)
                {
                    // Missing old record
                    // Insert an empty record
                    string sqlInsert = "INSERT INTO item_dynamic_objects(global_id) VALUES (@globalId)";
                    Hashtable parametersInsert = new Hashtable();
                    parametersInsert.Add("@globalId", glid);

                    KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sqlInsert, parametersInsert);
                }
            }
            catch (Exception ex)
            {
                m_logger.Error("UpdateDynamicObjectMetadata[" + glid.ToString() + "]: Exception occurred while verifying existing record - " + ex.ToString());
                resultXml = EncodeResult(-1, "Error querying existing dynamic object metadata: DB error");
                return false;
            }

            string updateStmt = "";

            try
            {
                // Media/playflash flag
                bool supportMedia;
                if (getParameter("media", false, out supportMedia))
                {
                    updateStmt = updateStmt + "playFlash=" + (supportMedia ? "1" : "0") + ",";
                }

                // Collision flag
                bool hasCollision;
                if (getParameter("collision", false, out hasCollision))
                {
                    updateStmt = updateStmt + "collision=" + (hasCollision ? "1" : "0") + ",";
                }

                // Bounding box
                string[,] boxParamNames = new string[2, 3] { { "min_x", "min_y", "min_z" }, { "max_x", "max_y", "max_z" } };
                bool missing = false, found = false;
                for (int i = 0; i < 2; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        string paramName = boxParamNames[i, j];
                        float paramValue;
                        if (getParameter(paramName, false, out paramValue))
                        {
                            found = true;
                            updateStmt = updateStmt + paramName + "=" + paramValue.ToString() + ",";
                        }
                        else
                        {
                            missing = true;
                        }
                    }
                }

                // If any of the boundbox parameters are provided, all of them are required
                if (found && missing)
                {
                    resultXml = EncodeResult(-1, "Error updating dynamic object metadata: Incomplete bound box parameters");
                    return false;
                }
            }
            catch (ArgumentException ex)
            {
                resultXml = EncodeResult(-1, "Error updating dynamic object metadata: " + ex.Message);
                return false;
            }

            if (updateStmt.Length > 0)
            {
                // Remove trailing comma
                if (updateStmt.EndsWith(","))
                {
                    updateStmt = updateStmt.Substring(0, updateStmt.Length - 1);
                }

                // Update record
                try
                {
                    string sql = "UPDATE item_dynamic_objects SET " + updateStmt + " WHERE global_id=@globalId";
                    Hashtable parameters = new Hashtable();
                    parameters.Add("@globalId", glid);

                    KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sql, parameters);
                }
                catch (Exception ex)
                {
                    m_logger.Error("UpdateDynamicObjectMetadata[" + glid.ToString() + "]: Exception occurred - " + ex.ToString());
                    resultXml = EncodeResult(-1, "Error updating dynamic object metadata: DB error");
                    return false;
                }
            }
            else
            {
                m_logger.Warn("UpdateDynamicObjectMetadata[" + glid.ToString() + "]: nothing to update");
            }

            resultXml = EncodeResult(0, "Success");
            return true;
        }

        protected bool UpdateEffectDuration()
        {
            try
            {
                uint glid;
                getParameter("glid", out glid);

                uint duration;
                getParameter("duration", out duration);

                string sql =
                    "INSERT INTO `item_parameters`(`global_id`, `param_type_id`, `value`) VALUES " +
                    "(@globalId, @paramTypeId, @duration) " +
                    "ON DUPLICATE KEY UPDATE `value`=@duration";

                Hashtable parameters = new Hashtable();
                parameters.Add("@globalId", glid);
                parameters.Add("@paramTypeId", ItemParameter.PARAM_TYPE_EFFECT_DURATION);
                parameters.Add("@duration", duration);

                KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sql, parameters);
            }
            catch (Exception ex)
            {
                resultXml = EncodeResult(-1, "Error updating animation metadata: " + ex.Message);
                return false;
            }

            resultXml = EncodeResult(0, "Success");
            return true;
        }

        protected bool UpdateAnimationMetadata()
        {
            return UpdateEffectDuration();
        }

        protected bool UpdateSoundMetadata()
        {
            return UpdateEffectDuration();
        }

        protected bool UpdateTextureMetadata()
        {
            try
            {
                uint glid;
                getParameter("glid", out glid);

                Int64 ordinal;
                getParameter("ordinal", out ordinal);

                uint width, height, opacity, color;
                getParameter("width", out width);
                getParameter("height", out height);
                getParameter("opacity", out opacity);
                getParameter("color", out color);

                string sql =
                    "INSERT INTO `item_path_textures`(`global_id`, `ordinal`, `width`, `height`, `opacity`, `color`) VALUES " +
                    "(@globalId, @ordinal, @width, @height, @opacity, @color) " +
                    "ON DUPLICATE KEY UPDATE `width`=@width, `height`=@height, `opacity`=@opacity, `color`=@color";

                Hashtable parameters = new Hashtable();
                parameters.Add("@globalId", glid);
                parameters.Add("@ordinal", ordinal);
                parameters.Add("@width", width);
                parameters.Add("@height", height);
                parameters.Add("@opacity", opacity);
                parameters.Add("@color", color);

			    KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sql, parameters);
            }
            catch (Exception ex)
            {
                resultXml = EncodeResult(-1, "Error updating texture metadata: " + ex.Message);
                return false;
            }

            resultXml = EncodeResult(0, "Success");
            return true;
        }

        #region getParameter helpers
        protected bool getParameter(string name, out string val)
        {
            return getParameter(name, true, out val);
        }

        protected bool getParameter(string name, bool required, out string val)
        {
            val = Request[name];
            if ((val == null || val == "") && required)
            {
                throw new ArgumentException("Missing parameter.", name);
            }
            return val != null;
        }

        protected bool getParameter(string name, out int val)
        {
            return getParameter(name, true, out val);
        }

        protected bool getParameter(string name, bool required, out int val)
        {
            string sVal;
            if (!getParameter(name, required, out sVal))
            {
                val = 0;
                return false;
            }

            try
            {
                val = Convert.ToInt32(sVal);
                return true;
            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid parameter.", name);
            }
        }

        protected bool getParameter(string name, out uint val)
        {
            return getParameter(name, true, out val);
        }

        protected bool getParameter(string name, bool required, out uint val)
        {
            string sVal;
            if (!getParameter(name, required, out sVal))
            {
                val = 0;
                return false;
            }

            try
            {
                val = Convert.ToUInt32(sVal);
                return true;
            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid parameter.", name);
            }
        }

        protected bool getParameter(string name, out Int64 val)
        {
            return getParameter(name, true, out val);
        }

        protected bool getParameter(string name, bool required, out Int64 val)
        {
            string sVal;
            if (!getParameter(name, required, out sVal))
            {
                val = 0;
                return false;
            }

            try
            {
                val = Convert.ToInt64(sVal);
                return true;
            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid parameter.", name);
            }
        }

        protected bool getParameter(string name, out bool val)
        {
            return getParameter(name, true, out val);
        }

        protected bool getParameter(string name, bool required, out bool val)
        {
            string sVal;
            if (!getParameter(name, required, out sVal))
            {
                val = false;
                return false;
            }

            if (sVal == "0")
            {
                val = false;
            }
            else if (sVal == "1")
            {
                val = true;
            }
            else
            {
                throw new ArgumentException("Invalid parameter.", name);
            }
            return true;
        }

        protected bool getParameter(string name, bool required, out float val)
        {
            string sVal;
            if (!getParameter(name, required, out sVal))
            {
                val = 0;
                return false;
            }

            try
            {
                val = Convert.ToSingle(sVal);
                return true;
            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid parameter.", name);
            }
        }
        #endregion

        protected ItemPathType GetPathTypeInfo(string pathType)
        {
            if (pathType == PATH_TYPE_NONE)
            {
                ItemPathType info = new ItemPathType();
                info.Type = pathType;
                return info;
            }

            try
            {
                return GetShoppingFacade.GetItemPathType(pathType);
            }
            catch (Exception)
            {
                return null;
            }
        }

        protected byte[] parseSHA256String(string sSHA256String)
        {
            if (sSHA256String.Length != 64)
            {
                return null;
            }

            byte[] sha256Hash = new byte[sSHA256String.Length / 2];
            for (int i = 0; i<sSHA256String.Length/2; i++)
            {
                try
                {
                    sha256Hash[i] = (byte)Convert.ToInt32(sSHA256String.Substring(i*2, 2), 16);
                }
                catch (Exception)
                {
                    return null;
                }
            }

            return sha256Hash;
        }
    }
}
