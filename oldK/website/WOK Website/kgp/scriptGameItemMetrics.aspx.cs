///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Text.RegularExpressions;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class scriptGameItemMetrics : KgpBasePage
    {
        #region Declarations

        // Shared report params
        private string _action = null;
        private string _username = null;
        private string _gameplayMode = null;
        private string _activationAction = null;
        private string _itemType = null;
        private string _behavior = null;
        private string _lootSource = null;
        private string _reportName = null;
        private int? _zoneInstanceId = null;
        private int? _zoneType = null;
        private int? _gameItemId = null;
        private int? _gameItemGlid = null;
        private Dictionary<string, string> _reportParams = null;

        // Response types
        private string _successResult = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ReturnDescription>success</ReturnDescription>\r\n </Result>";
        private string _errorResult = "<Result>\r\n  <ReturnCode>{0}</ReturnCode>\r\n  <ReturnDescription>{1}</ReturnDescription>\r\n </Result>";

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Get parameters
                GetRequestParams();

                // Branch via report type
                switch (_action)
                {
                    case "logItemPlacement":
                        ProcessLogItemPlacement();
                        break;
                    case "logLootAcquisition":
                        ProcessLogLootAcquisition();
                        break;
                    case "logFrameworkActivation":
                        ProcessLogFrameworkActivation();
                        break;
                    case "logPaintAction":
                        ProcessLogPaintAction();
                        break;
                    default:
                        ProcessGenericMetricAction();
                        break;
                }
            }
        }

        /// <summary>
        /// Sets page variables using either passed in values or appropriate defaults
        /// </summary>
        private void GetRequestParams()
        {
            int intOut = 0;

            _action = Request.Params["action"];
            _username = Request.Params["username"];
            _gameplayMode = Request.Params["gameplayMode"];
            _activationAction = Request.Params["activationAction"];
            _itemType = Request.Params["itemType"];
            _behavior = Request.Params["behavior"];
            _lootSource = Request.Params["lootSource"];
            _reportName = Request.Params["reportName"];
            _zoneInstanceId = Int32.TryParse(Request.Params["zoneInstanceId"], out intOut) ? intOut : 0;
            _zoneType = Int32.TryParse(Request.Params["zoneType"], out intOut) ? intOut : 0;
            _gameItemId = Int32.TryParse(Request.Params["gameItemId"], out intOut) ? intOut : 0;
            _gameItemGlid = Int32.TryParse(Request.Params["gameItemGlid"], out intOut) ? intOut : 0;

            if (!string.IsNullOrWhiteSpace(Request.Params["reportParams"]))
            {
                _reportParams = new Dictionary<string, string>();

                string[] rParamTuples = Request.Params["reportParams"].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                char[] tupleDelimiter = new char[] { ',' };

                foreach (string rParamTuple in rParamTuples)
                {
                    string[] tupleParts = rParamTuple.Split(tupleDelimiter, StringSplitOptions.RemoveEmptyEntries);

                    if (tupleParts.Length == 2)
                    {
                        _reportParams.Add(tupleParts[0], tupleParts[1]);
                    }
                    else
                    {
                        _reportParams.Clear();
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Records the log placement action
        /// </summary>
        private void ProcessLogItemPlacement()
        {
            // Validation
            if (string.IsNullOrWhiteSpace(_username)) {
                Response.Write(string.Format(_errorResult, -1, "username not specified or invalid."));
                return;
            } else if (string.IsNullOrWhiteSpace(_gameplayMode) || !(_gameplayMode.Equals("Player") || _gameplayMode.Equals("Creator"))) {
                Response.Write(string.Format(_errorResult, -1, "gameplayMode not specified or invalid."));
                return;
            } else if (string.IsNullOrWhiteSpace(_itemType)) {
                Response.Write(string.Format(_errorResult, -1, "itemType not specified or invalid."));
                return;
            } else if (_zoneType != 3 && _zoneType != 6) {
                Response.Write(string.Format(_errorResult, -1, "zoneType not specified or invalid."));
                return;
            } else if (_zoneInstanceId <= 0) {
                Response.Write(string.Format(_errorResult, -1, "zoneInstanceId not specified or invalid."));
                return;
            } else if (_gameItemId <= 0)
            {
                Response.Write(string.Format(_errorResult, -1, "gameItemId not specified or invalid."));
                return;
            }

            // Insert the placement record
            GetMetricsFacade.InsertScriptGameItemPlacementLog((int)_zoneInstanceId, (int)_zoneType, _username, _gameplayMode, (int)_gameItemId, (int)_gameItemGlid, _itemType, _behavior);

            // Indicate success
            Response.Write(_successResult);
        }

        /// <summary>
        /// Records the log loot acquisition action
        /// </summary>
        private void ProcessLogLootAcquisition()
        {
            // Validation
            if (string.IsNullOrWhiteSpace(_username))
            {
                Response.Write(string.Format(_errorResult, -1, "username not specified or invalid."));
                return;
            }
            else if (string.IsNullOrWhiteSpace(_lootSource))
            {
                Response.Write(string.Format(_errorResult, -1, "lootSource not specified or invalid."));
                return;
            }
            else if (string.IsNullOrWhiteSpace(_itemType))
            {
                Response.Write(string.Format(_errorResult, -1, "itemType not specified or invalid."));
                return;
            }
            else if (_zoneType != 3 && _zoneType != 6)
            {
                Response.Write(string.Format(_errorResult, -1, "zoneType not specified or invalid."));
                return;
            }
            else if (_zoneInstanceId <= 0)
            {
                Response.Write(string.Format(_errorResult, -1, "zoneInstanceId not specified or invalid."));
                return;
            }
            else if (_gameItemId <= 0)
            {
                Response.Write(string.Format(_errorResult, -1, "gameItemId not specified or invalid."));
                return;
            }

            // Insert the placement record
            GetMetricsFacade.InsertScriptGameItemLootingLog((int)_zoneInstanceId, (int)_zoneType, _username, _lootSource, (int)_gameItemId, (int)_gameItemGlid, _itemType, _behavior);

            // Indicate success
            Response.Write(_successResult);
        }

        /// <summary>
        /// Records the log framework activation action
        /// </summary>
        private void ProcessLogFrameworkActivation()
        {
            // Validation
            if (string.IsNullOrWhiteSpace(_username))
            {
                Response.Write(string.Format(_errorResult, -1, "username not specified or invalid."));
                return;
            }
            else if (string.IsNullOrWhiteSpace(_activationAction) || 
                !(_activationAction.Equals("Activate") || 
                  _activationAction.Equals("Deactivate") ||
                  _activationAction.Equals("ActivateImport") ||
                  _activationAction.Equals("DeactivateImport")))
            {
                Response.Write(string.Format(_errorResult, -1, "activationAction not specified or invalid. Must equal either 'Activate' or 'Deactivate'."));
                return;
            }
            else if (_zoneType != 3 && _zoneType != 6)
            {
                Response.Write(string.Format(_errorResult, -1, "zoneType not specified or invalid."));
                return;
            }
            else if (_zoneInstanceId <= 0)
            {
                Response.Write(string.Format(_errorResult, -1, "zoneInstanceId not specified or invalid."));
                return;
            }

            // Insert the placement record
            GetMetricsFacade.InsertFrameworkActivationLog((int)_zoneInstanceId, (int)_zoneType, _username, _activationAction);

            // Indicate success
            Response.Write(_successResult);
        }

        /// <summary>
        /// Records the log paint action
        /// </summary>
        private void ProcessLogPaintAction()
        {
            Response.Write(string.Format(_errorResult, -1, "Deprecated action."));
        }

        /// <summary>
        /// Records a generic metrics action.
        /// </summary>
        private void ProcessGenericMetricAction()
        {
            // Validation
            if (string.IsNullOrWhiteSpace(_reportName))
            {
                Response.Write(string.Format(_errorResult, -1, "reportName not specified or invalid."));
                return;
            }
            else if (_reportParams == null || _reportParams.Count < 1)
            {
                Response.Write(string.Format(_errorResult, -1, "reportParams not specified or invalid."));
                return;
            }

            // Insert the placement record
            GetMetricsFacade.InsertGenericMetricsLog(_reportName, _reportParams, Global.RequestRabbitMQChannel());

            // Indicate success
            Response.Write(_successResult);
        }
    }
}
