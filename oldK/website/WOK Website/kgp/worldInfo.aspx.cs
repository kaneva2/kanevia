///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class worldInfo : KgpBasePage
    {
        #region Declarations

        // Actions
        const string ACTION_GET_COMMUNITY_ID = "GetCommunityId";
        const string ACTION_GET_WORLD_ZONES = "GetWorldZones";
        const string ACTION_BUY_ZONE_SLOT = "BuyZoneSlot";
        const string ACTION_DELETE_WORLD = "DeleteWorld";
        const string ACTION_GET_WORLD_TEMPLATE = "GetWorldTemplate";
        const string ACTION_CAN_WORLD_DROP_GEMS = "CanWorldDropGems";
        const string ACTION_CAN_WORLD_URL = "GetWorldUrl";

        // Params
        private string _action = null;
        private int _gameId = 0;
        private int _zoneIndex = 0;
        private int _zoneInstanceId = 0;
        private int _zoneType = 0;
        private int _zoneSlotPrice = 1000;
        private string _currencyType = Currency.CurrencyType.CREDITS;
        private string _worldName = null;

        // Response types
        private string _resultTemplate = "<Result>\r\n  <ReturnCode>{0}</ReturnCode>\r\n  <ReturnDescription>{1}</ReturnDescription>\r\n {2}\r\n </Result>";

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Get parameters
                GetRequestParams();

                // Make sure we at least have an action to branch
                if (string.IsNullOrWhiteSpace(_action))
                {
                    Response.Write(string.Format(_resultTemplate, -1, "action must be specified", string.Empty));
                    return;
                }

                // Branch via report type
                switch (_action)
                {
                    case ACTION_GET_COMMUNITY_ID:
                        ProcessGetCommunityIdAction();
                        break;
                    case ACTION_GET_WORLD_ZONES:
                        ProcessGetWorldZonesAction();
                        break;
                    case ACTION_BUY_ZONE_SLOT:
                        BuyZoneSlot();
                        break;
                    case ACTION_DELETE_WORLD:
                        DeleteWorld();
                        break;
                    case ACTION_GET_WORLD_TEMPLATE:
                        ProcessGetWorldTemplateAction();
                        break;
                    case ACTION_CAN_WORLD_DROP_GEMS:
                        ProcessCanWorldDropGems();
                        break;
                    case ACTION_CAN_WORLD_URL:
                        ProcessGetWorldUrl();
                        break;
                    default:
                        Response.Write(string.Format(_resultTemplate, -1, "invalid action", string.Empty));
                        break;
                }
            }
        }

        /// <summary>
        /// Sets page variables using either passed in values or appropriate defaults
        /// </summary>
        private void GetRequestParams()
        {
            int intOut = 0;

            _action = Request.Params["action"];
            _gameId = Int32.TryParse(Request.Params["gameId"], out intOut) ? intOut : 0;
            _zoneIndex = Int32.TryParse(Request.Params["zoneIndex"], out intOut) ? intOut : 0;
            _zoneInstanceId = Int32.TryParse(Request.Params["zoneInstanceId"], out intOut) ? intOut : 0;
            _zoneType = Int32.TryParse(Request.Params["zoneType"], out intOut) ? intOut : 0;
            _worldName = Request.Params["worldName"] ?? string.Empty;

            if (Request.Params["currencyType"] != null && Request.Params["currencyType"] != string.Empty)
            {
                _currencyType = Request.Params["currencyType"];
            }
        }

        /// <summary>
        /// Deletes world based on variables passed in
        /// </summary>
        private void DeleteWorld()
        {
            int communityId = GetCommunityFacade.GetCommunityIdFromWorldIds(KanevaGlobals.WokGameId, _gameId, _zoneIndex, _zoneInstanceId, _zoneType);
            int retVal = 0;

            if (GetCommunityFacade.IsCommunityOwner(communityId, KanevaWebGlobals.CurrentUser.UserId))
            {
                // Delete 
                retVal = CommunityUtility.DeleteCommunity(communityId, KanevaWebGlobals.CurrentUser.UserId);
            }
            else
            {
                m_logger.Error("User id " + KanevaWebGlobals.CurrentUser.UserId + " tried to delete CommunityId " + communityId +
                    " and user is not owner of this world");
                Response.Write(string.Format(_resultTemplate, -1, "User is not world owner", string.Empty));
                return;
            }

            if (retVal == 0)
            {
                Response.Write(string.Format(_resultTemplate, 0, "success", string.Empty));
                return;
            }

            Response.Write(string.Format(_resultTemplate, -1, "Delete world failed", string.Empty));
            return;
        }

        /// <summary>
        /// Purchase a slot for the world to add a linked zone
        /// </summary>
        private void BuyZoneSlot()
        {
            int userId = GetUserId();
            int successfulPayment = -1;

            // Verify we have all data needed
            if (_zoneInstanceId < 1 || _zoneType < 1)
            {
                Response.Write(string.Format(_resultTemplate, -1, "Invalid input parameters", string.Empty));
                return;
            }

            // Make sure this is a valid world
            int communityId = GetCommunityFacade.GetCommunityIdFromWorldIds(KanevaGlobals.WokGameId, _gameId, _zoneIndex, _zoneInstanceId, _zoneType);
            if (communityId == 0)
            {
                Response.Write(string.Format(_resultTemplate, -1, "Invalid world", string.Empty));
                return;
            }

            // Verify user has not reaches number of worlds limit
            int numberOfCommunities = GetCommunityFacade.GetNumberOfCommunities(userId, 1, new int[] { (int)CommunityType.COMMUNITY, (int)CommunityType.APP_3D, (int)CommunityType.HOME });
            if (!IsAdministrator() && !UsersUtility.IsUserBetatester() && userId != Configuration.AutomatedTestUserDefaultId)
            {
                if (numberOfCommunities >= Configuration.MaxNumberOfCommunitiesPerUser)
                {
                    Response.Write(string.Format(_resultTemplate, -3, "Users are restricted to a maximum of " + Configuration.MaxNumberOfCommunitiesPerUser + " Worlds", string.Empty));
                    return;
                }
            }

            // Verify user has enough Credits to purchase slot
            double availableCredits = UsersUtility.getUserBalance(userId, _currencyType);
            if (_zoneSlotPrice > availableCredits)
            {
                m_logger.Error("Error In worldInfo.aspx: User doesn't have enough Credits to purchase linked zone slot. UserId=" + userId);
                Response.Write(string.Format(_resultTemplate, -1, "User Does Not Have Enough Credits", string.Empty));
                return;
            }

            // Purchase zone slot
            try
            {
                successfulPayment = GetUserFacade.AdjustUserBalance(userId, _currencyType, -Convert.ToDouble(_zoneSlotPrice), Convert.ToUInt16(TransactionType.eTRANSACTION_TYPES.CASH_TT_LINKED_WORLD_SLOT));
            }
            catch (Exception)
            {
                m_logger.Error("Error In worldInfo.aspx: User doesn't have enough Credits to make purchase of linked zone slot. UserId=" + userId);
                Response.Write(string.Format(_resultTemplate, -1, "User Does Not Have Enough Credits", string.Empty));
                return;
            }

            if (successfulPayment == 0)  //0 is success according to AdjustUserBalance notes
            {
                Dictionary<string, string> metricData = new Dictionary<string, string>
                    {
                        {"communityId", communityId.ToString()},
                        {"userId", userId.ToString()}
                    };
                
                // Insert the purchase record
                GetMetricsFacade.InsertGenericMetricsLog("zone_slot_purchase_log", metricData, Global.RequestRabbitMQChannel());
                m_logger.Info("userId " + userId + " deleted communityId " + communityId);
                Response.Write(string.Format(_resultTemplate, 0, "success", string.Empty));
            }
            else
            {
                m_logger.Error("userId " + userId + " deleted communityId " + communityId + " but purchase failed");
                Response.Write(string.Format(_resultTemplate, -1, "Linked Zone Slot Purchase Failed", string.Empty));
            }
        }

        /// <summary>
        /// Retrieves info for the current world and any linked zones that belong to that world
        /// </summary>
        private void ProcessGetWorldZonesAction()
        {
            int communityId = GetCommunityFacade.GetCommunityIdFromWorldIds(KanevaGlobals.WokGameId, _gameId, _zoneIndex, _zoneInstanceId, _zoneType);

            if (communityId > 0)
            {
                DataTable dtLinkedZones = GetGameFacade.GetLinkedZones(communityId, _zoneInstanceId, _zoneType, KanevaGlobals.WokGameId);

                DataSet dsLinkedZones = new DataSet("Result");
                dsLinkedZones.Tables.Add(dtLinkedZones.Copy());

                XmlDocument zonesDoc = new XmlDocument();
                zonesDoc.LoadXml(dsLinkedZones.GetXml());
                XmlNode passesRoot = zonesDoc.DocumentElement;

                int numrecs = dtLinkedZones.Rows.Count;
                XmlElement passesElem = zonesDoc.CreateElement("TotalNumberRecords");
                passesElem.InnerText = numrecs.ToString();

                passesRoot.InsertBefore(passesElem, passesRoot.FirstChild);

                passesElem = zonesDoc.CreateElement("ReturnDescription");
                passesElem.InnerText = "success";

                passesRoot.InsertBefore(passesElem, passesRoot.FirstChild);

                passesElem = zonesDoc.CreateElement("ReturnCode");
                passesElem.InnerText = "0";

                passesRoot.InsertBefore(passesElem, passesRoot.FirstChild);

                Response.Write(passesRoot.OuterXml);
            }
            else
            {
                m_logger.Error("Invalid Community. communityId=" + communityId +
                    ", zoneInstanceId=" + _zoneInstanceId + ", zoneType=" + _zoneType);
                Response.Write(string.Format(_resultTemplate, -1, "Invalid World", string.Empty));
            }
        }

        /// <summary>
        /// Retrieves the community_id of a specified world
        /// </summary>
        private void ProcessGetCommunityIdAction()
        {
            int communityId = GetCommunityFacade.GetCommunityIdFromWorldIds(KanevaGlobals.WokGameId, _gameId, _zoneIndex, _zoneInstanceId, _zoneType);

            if (communityId > 0)
            {
                Response.Write(string.Format(_resultTemplate, 0, "success", string.Format("<community_id>{0}</community_id>", communityId)));
            }
            else
            {
                Response.Write(string.Format(_resultTemplate, -1, "unable to find community_id", string.Empty));
            }
        }

        /// <summary>
        /// Retrieves the world template id for a given world.
        /// </summary>
        private void ProcessGetWorldTemplateAction()
        {
            if (string.IsNullOrWhiteSpace(_worldName) && _zoneInstanceId <= 0 && _zoneType <= 0)
            {
                m_logger.Error("Invalid World. worldName=" + _worldName + " zoneInstanceId=" + _zoneInstanceId + " zoneType=" + _zoneType);
                Response.Write(string.Format(_resultTemplate, -1, "Invalid World", string.Empty));
            }
            else
            {
                // Default to world name if passed in, fallback to ids.
                int communityId = 0;
                if (!string.IsNullOrWhiteSpace(_worldName))
                    communityId = GetCommunityFacade.GetCommunityIdFromName(_worldName, false);
                else if (_zoneInstanceId > 0 && _zoneType > 0)
                    communityId = GetCommunityFacade.GetCommunityIdFromWorldIds(Configuration.WokGameId, 0, 0, _zoneInstanceId, _zoneType);

                if (communityId > 0)
                {
                    Community comm = GetCommunityFacade.GetCommunity(communityId, true);
                    Response.Write(string.Format("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ReturnDescription>success</ReturnDescription>\r\n  <TemplateId>{0}</TemplateId>\r\n </Result>", comm.TemplateId));
                }
                else
                {
                    m_logger.Error("Invalid World. worldName=" + _worldName);
                    Response.Write(string.Format(_resultTemplate, -1, "Invalid World", string.Empty));
                }
            }
        }

        private void ProcessCanWorldDropGems()
        {
            if (string.IsNullOrWhiteSpace(_worldName))
            {
                m_logger.Error("Invalid World. worldName=" + _worldName);
                Response.Write(string.Format(_resultTemplate, -1, "Invalid World", string.Empty));
            }
            else
            {
                Community community = GetCommunityFacade.GetCommunity(GetCommunityFacade.GetCommunityIdFromName(_worldName, false));
                if (community.CommunityId > 0)
                {
                    bool canDrop = GetGameFacade.CanWorldDropGemChests(community.CreatorId, community.CommunityId);
                    Response.Write(string.Format("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ReturnDescription>success</ReturnDescription>\r\n  <CanDropGems>{0}</CanDropGems>\r\n</Result>", canDrop.ToString().ToLower()));
                }
                else
                {
                    m_logger.Error("Invalid World. worldName=" + _worldName);
                    Response.Write(string.Format(_resultTemplate, -1, "Invalid World", string.Empty));
                }
            }
        }

        /// <summary>
        /// Retrieves the STPUrl for a given world.
        /// </summary>
        private void ProcessGetWorldUrl()
        {
            if (string.IsNullOrWhiteSpace(_worldName) && _zoneInstanceId <= 0 && _zoneType <= 0)
            {
                m_logger.Error("Invalid World. worldName=" + _worldName + " zoneInstanceId=" + _zoneInstanceId + " zoneType=" + _zoneType);
                Response.Write(string.Format(_resultTemplate, -1, "Invalid World", string.Empty));
            }
            else
            {
                // Default to world name if passed in, fallback to ids.
                int communityId = 0;
                if (!string.IsNullOrWhiteSpace(_worldName))
                    communityId = GetCommunityFacade.GetCommunityIdFromName(_worldName, false);
                else if (_zoneInstanceId > 0 && _zoneType > 0)
                    communityId = GetCommunityFacade.GetCommunityIdFromWorldIds(Configuration.WokGameId, 0, 0, _zoneInstanceId, _zoneType);

                if (communityId > 0)
                {
                    Community comm = GetCommunityFacade.GetCommunity(communityId, false);

                    string stpUrl = StpUrl.MakeUrlPrefix(KanevaGlobals.WokGameId.ToString());
                    if (comm.CommunityTypeId == (int)CommunityType.HOME)
                    { 
                        stpUrl += "/" + StpUrl.MakeAptUrlPath(comm.Name);
                    }
                    else if (comm.CommunityTypeId == (int)CommunityType.COMMUNITY
                        || comm.CommunityTypeId == (int)CommunityType.TRY_ON)
                    {
                        stpUrl += "/" + StpUrl.MakeCommunityUrlPath(comm.Name);
                    }
                    else
                    {   
                        stpUrl += "/" + StpUrl.MakeZoneUrlPath(comm.Name);
                    }

                    Response.Write(string.Format("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ReturnDescription>success</ReturnDescription>\r\n  <STPUrl>{0}</STPUrl>\r\n </Result>", stpUrl));
                }
                else
                {
                    m_logger.Error("Invalid World.");
                    Response.Write(string.Format(_resultTemplate, -1, "Invalid World", string.Empty));
                }
            }
        }
    }
}