///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace KlausEnt.KEP.Kaneva._testing
{
	/// <summary>
	/// Summary description for travellist.
	/// </summary>
	public class performanceMetrics : BasePage 
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
      SaveData();
		}

    private void SaveData()
    {
      SqlConnection cn = new SqlConnection("user id=client_loader;password=client.0605;server=sql.kanevia.com\\SQLEXPRESS;Trusted_Connection=no;database=Client_Perf;connection timeout=30");
      
      try
      {
          cn.Open();
      }
      catch(Exception)
      {
        //Response.Write(e.ToString());
        Response.Write("Error 1");
      }
      
      


        string sqlString = "INSERT INTO Client_Perf_raw ([timestamp],[client_version], [user_id], [zone_instance_id], [zone_index], " +
          " [loc_x], [loc_y], [loc_z], [facing_x], [facing_y], [facing_z], [viewport_res_x], [viewport_res_y], [fps_avg], " +
          " [num_frames], [num_stalls], [num_freezes], [msec_duration],[msec_in_zone],[msec_frame_avg], " +
          " [msec_render_avg],[msec_swap_avg],[num_tri_entity],[num_tri_wldobj],[num_tri_dynobj],[num_tri_total],[num_batch_entity], " +
          " [num_batch_wldobj],[num_batch_dynobj],[num_batch_total],[num_zone_entity],[num_zone_wldobj],[num_zone_dynobj],[num_zone_total] " +
          " ,[num_draw_entity],[num_draw_wldobj],[num_draw_dynobj],[num_draw_total],[cfg_settings_hash],[cfg_anim_quality],[cfg_lod_bias] " +
          " ,[cfg_avatar_names],[cfg_dynobj_quality],[cfg_flash_enabled],[cfg_env_anim],[cfg_spot_shadows],[sys_num_cpu],[sys_cpu_ratedfreq], " +
          " [sys_cpu_freq],[sys_cpu_vendor],[sys_cpu_string],[sys_ram_total],[sys_gfx_string],[sys_gfx_driver_file],[sys_gfx_driver_ver] " +
          " ,[sys_gfx_texmem],[sys_gfx_hwtnl],[sys_info_hash] " +
          " ) " +
          " VALUES (@timestamp, @client_version, @user_id, @zone_instance_id, @zone_index, @loc_x, @loc_y, @loc_z, " +
          " @facing_x, @facing_y, @facing_z, @viewport_res_x, @viewport_res_y, @fps_avg, @num_frames, @num_stalls, @num_freezes, " +
          " @msec_duration, @msec_in_zone, @msec_frame_avg,  " +
          " @msec_render_avg, @msec_swap_avg, @num_tri_entity, @num_tri_wldobj, @num_tri_dynobj, @num_tri_total, @num_batch_entity,  " +
          " @num_batch_wldobj, @num_batch_dynobj, @num_batch_total, @num_zone_entity, @num_zone_wldobj, @num_zone_dynobj, @num_zone_total  " +

          " ,@num_draw_entity, @num_draw_wldobj, @num_draw_dynobj, @num_draw_total, @cfg_settings_hash, @cfg_anim_quality, @cfg_lod_bias  " +
          " ,@cfg_avatar_names, @cfg_dynobj_quality, @cfg_flash_enabled, @cfg_env_anim, @cfg_spot_shadows, @sys_num_cpu, @sys_cpu_ratedfreq,  " +
          " @sys_cpu_freq, @sys_cpu_vendor, @sys_cpu_string, @sys_ram_total, @sys_gfx_string, @sys_gfx_driver_file, @sys_gfx_driver_ver  " +
          " ,@sys_gfx_texmem , @sys_gfx_hwtnl, @sys_info_hash " +
          " )";

        SqlCommand comm = new SqlCommand(sqlString, cn);


        // Params
        try
        {
        SqlParameter pTimeStamp = new SqlParameter("@timestamp", SqlDbType.DateTime);
        pTimeStamp.Value = DateTime.Now;
        comm.Parameters.Add(pTimeStamp);

        SqlParameter pClientVersion = new SqlParameter("@client_version", SqlDbType.VarChar);
        pClientVersion.Value = Request["client_version"] != null ? Request["client_version"].ToString() : "";
        comm.Parameters.Add(pClientVersion);

        SqlParameter user_id = new SqlParameter("@user_id", SqlDbType.Int);
        user_id.Value = Request["user_id"] != null ? Request["user_id"].ToString() : "";
        comm.Parameters.Add(user_id);

        SqlParameter zone_instance_id = new SqlParameter("@zone_instance_id", SqlDbType.Int);
        zone_instance_id.Value = Request["zone_instance_id"] != null ? Request["zone_instance_id"].ToString() : "";
        comm.Parameters.Add(zone_instance_id);

        SqlParameter zone_index = new SqlParameter("@zone_index", SqlDbType.Int);
        zone_index.Value = Request["zone_index"] != null ? Request["zone_index"].ToString() : "";
        comm.Parameters.Add(zone_index);

        SqlParameter loc_x = new SqlParameter("@loc_x", SqlDbType.Float);
        loc_x.Value = Request["loc_x"] != null ? Request["loc_x"].ToString() : "";
        comm.Parameters.Add(loc_x);

        SqlParameter loc_y = new SqlParameter("@loc_y", SqlDbType.Float);
        loc_y.Value = Request["loc_y"] != null ? Request["loc_y"].ToString() : "";
        comm.Parameters.Add(loc_y);

        SqlParameter loc_z = new SqlParameter("@loc_z", SqlDbType.Float);
        loc_z.Value = Request["loc_z"] != null ? Request["loc_z"].ToString() : "";
        comm.Parameters.Add(loc_z);

        SqlParameter facing_x = new SqlParameter("@facing_x", SqlDbType.Float);
        facing_x.Value = Request["facing_x"] != null ? Request["facing_x"].ToString() : "";
        comm.Parameters.Add(facing_x);

        SqlParameter facing_y = new SqlParameter("@facing_y", SqlDbType.Float);
        facing_y.Value = Request["facing_y"] != null ? Request["facing_y"].ToString() : "";
        comm.Parameters.Add(facing_y);

        SqlParameter facing_z = new SqlParameter("@facing_z", SqlDbType.Float);
        facing_z.Value = Request["facing_z"] != null ? Request["facing_z"].ToString() : "";
        comm.Parameters.Add(facing_z);

        SqlParameter viewport_res_x = new SqlParameter("@viewport_res_x", SqlDbType.Int);
        viewport_res_x.Value = Request["viewport_res_x"] != null ? Request["viewport_res_x"].ToString() : "";
        comm.Parameters.Add(viewport_res_x);

        SqlParameter viewport_res_y = new SqlParameter("@viewport_res_y", SqlDbType.Int);
        viewport_res_y.Value = Request["viewport_res_y"] != null ? Request["viewport_res_y"].ToString() : "";
        comm.Parameters.Add(viewport_res_y);

        SqlParameter fps_avg = new SqlParameter("@fps_avg", SqlDbType.Float);
        fps_avg.Value = Request["fps_avg"] != null ? Request["fps_avg"].ToString() : "";
        comm.Parameters.Add(fps_avg);

        SqlParameter num_frames = new SqlParameter("@num_frames", SqlDbType.Int);
        num_frames.Value = Request["num_frames"] != null ? Request["num_frames"].ToString() : "";
        comm.Parameters.Add(num_frames);

        SqlParameter num_stalls = new SqlParameter("@num_stalls", SqlDbType.Int);
        num_stalls.Value = Request["num_stalls"] != null ? Request["num_stalls"].ToString() : "";
        comm.Parameters.Add(num_stalls);

        SqlParameter num_freezes = new SqlParameter("@num_freezes", SqlDbType.Int);
        num_freezes.Value = Request["num_freezes"] != null ? Request["num_freezes"].ToString() : "";
        comm.Parameters.Add(num_freezes);

        SqlParameter msec_duration = new SqlParameter("@msec_duration", SqlDbType.Int);
        msec_duration.Value = Request["msec_duration"] != null ? Request["msec_duration"].ToString() : "";
        comm.Parameters.Add(msec_duration);

        SqlParameter msec_in_zone = new SqlParameter("@msec_in_zone", SqlDbType.Int);
        msec_in_zone.Value = Request["msec_in_zone"] != null ? Request["msec_in_zone"].ToString() : "";
        comm.Parameters.Add(msec_in_zone);

        SqlParameter msec_frame_avg = new SqlParameter("@msec_frame_avg", SqlDbType.Int);
        msec_frame_avg.Value = Request["msec_frame_avg"] != null ? Request["msec_frame_avg"].ToString() : "";
        comm.Parameters.Add(msec_frame_avg);

        SqlParameter msec_render_avg = new SqlParameter("@msec_render_avg", SqlDbType.Int);
        msec_render_avg.Value = Request["msec_render_avg"] != null ? Request["msec_render_avg"].ToString() : "";
        comm.Parameters.Add(msec_render_avg);

        SqlParameter msec_swap_avg = new SqlParameter("@msec_swap_avg", SqlDbType.Int);
        msec_swap_avg.Value = Request["msec_swap_avg"] != null ? Request["msec_swap_avg"].ToString() : "";
        comm.Parameters.Add(msec_swap_avg);

        SqlParameter num_tri_entity = new SqlParameter("@num_tri_entity", SqlDbType.Int);
        num_tri_entity.Value = Request["num_tri_entity"] != null ? Request["num_tri_entity"].ToString() : "";
        comm.Parameters.Add(num_tri_entity);

        SqlParameter num_tri_wldobj = new SqlParameter("@num_tri_wldobj", SqlDbType.Int);
        num_tri_wldobj.Value = Request["num_tri_wldobj"] != null ? Request["num_tri_wldobj"].ToString() : "";
        comm.Parameters.Add(num_tri_wldobj);

        SqlParameter num_tri_dynobj = new SqlParameter("@num_tri_dynobj", SqlDbType.Int);
        num_tri_dynobj.Value = Request["num_tri_dynobj"] != null ? Request["num_tri_dynobj"].ToString() : "";
        comm.Parameters.Add(num_tri_dynobj);

        SqlParameter num_tri_total = new SqlParameter("@num_tri_total", SqlDbType.Int);
        num_tri_total.Value = Request["num_tri_total"] != null ? Request["num_tri_total"].ToString() : "";
        comm.Parameters.Add(num_tri_total);

        SqlParameter num_batch_entity = new SqlParameter("@num_batch_entity", SqlDbType.Int);
        num_batch_entity.Value = Request["num_batch_entity"] != null ? Request["num_batch_entity"].ToString() : "";
        comm.Parameters.Add(num_batch_entity);

        SqlParameter num_draw_entity = new SqlParameter("@num_draw_entity", SqlDbType.Int);
        num_draw_entity.Value = Request["num_draw_entity"] != null ? Request["num_draw_entity"].ToString() : "";
        comm.Parameters.Add(num_draw_entity);

        SqlParameter num_draw_wldobj = new SqlParameter("@num_draw_wldobj", SqlDbType.Int);
        num_draw_wldobj.Value = Request["num_draw_wldobj"] != null ? Request["num_draw_wldobj"].ToString() : "";
        comm.Parameters.Add(num_draw_wldobj);

        SqlParameter num_draw_dynobj = new SqlParameter("@num_draw_dynobj", SqlDbType.Int);
        num_draw_dynobj.Value = Request["num_draw_dynobj"] != null ? Request["num_draw_dynobj"].ToString() : "";
        comm.Parameters.Add(num_draw_dynobj);

        SqlParameter num_draw_total = new SqlParameter("@num_draw_total", SqlDbType.Int);
        num_draw_total.Value = Request["num_draw_total"] != null ? Request["num_draw_total"].ToString() : "";
        comm.Parameters.Add(num_draw_total);

        SqlParameter cfg_settings_hash = new SqlParameter("@cfg_settings_hash", SqlDbType.Int);
        cfg_settings_hash.Value = Request["cfg_settings_hash"] != null ? Request["cfg_settings_hash"].ToString() : "";
        comm.Parameters.Add(cfg_settings_hash);

        SqlParameter cfg_anim_quality = new SqlParameter("@cfg_anim_quality", SqlDbType.Int);
        cfg_anim_quality.Value = Request["cfg_anim_quality"] != null ? Request["cfg_anim_quality"].ToString() : "";
        comm.Parameters.Add(cfg_anim_quality);

        SqlParameter num_batch_wldobj = new SqlParameter("@num_batch_wldobj", SqlDbType.Int);
        num_batch_wldobj.Value = Request["num_batch_wldobj"] != null ? Request["num_batch_wldobj"].ToString() : "";
        comm.Parameters.Add(num_batch_wldobj);

        SqlParameter num_batch_dynobj = new SqlParameter("@num_batch_dynobj", SqlDbType.Int);
        num_batch_dynobj.Value = Request["num_batch_dynobj"] != null ? Request["num_batch_dynobj"].ToString() : "";
        comm.Parameters.Add(num_batch_dynobj);

        SqlParameter num_batch_total = new SqlParameter("@num_batch_total", SqlDbType.Int);
        num_batch_total.Value = Request["num_batch_total"] != null ? Request["num_batch_total"].ToString() : "";
        comm.Parameters.Add(num_batch_total);

        SqlParameter num_zone_entity = new SqlParameter("@num_zone_entity", SqlDbType.Int);
        num_zone_entity.Value = Request["num_zone_entity"] != null ? Request["num_zone_entity"].ToString() : "";
        comm.Parameters.Add(num_zone_entity);

        SqlParameter num_zone_wldobj = new SqlParameter("@num_zone_wldobj", SqlDbType.Int);
        num_zone_wldobj.Value = Request["num_zone_wldobj"] != null ? Request["num_zone_wldobj"].ToString() : "";
        comm.Parameters.Add(num_zone_wldobj);

        SqlParameter num_zone_dynobj = new SqlParameter("@num_zone_dynobj", SqlDbType.Int);
        num_zone_dynobj.Value = Request["num_zone_dynobj"] != null ? Request["num_zone_dynobj"].ToString() : "";
        comm.Parameters.Add(num_zone_dynobj);

        SqlParameter num_zone_total = new SqlParameter("@num_zone_total", SqlDbType.Int);
        num_zone_total.Value = Request["num_zone_total"] != null ? Request["num_zone_total"].ToString() : "";
        comm.Parameters.Add(num_zone_total);


        SqlParameter cfg_lod_bias = new SqlParameter("@cfg_lod_bias", SqlDbType.Int);
        cfg_lod_bias.Value = Request["cfg_lod_bias"] != null ? Request["cfg_lod_bias"].ToString() : "";
        comm.Parameters.Add(cfg_lod_bias);

        SqlParameter cfg_avatar_names = new SqlParameter("@cfg_avatar_names", SqlDbType.Int);
        cfg_avatar_names.Value = Request["cfg_avatar_names"] != null ? Request["cfg_avatar_names"].ToString() : "";
        comm.Parameters.Add(cfg_avatar_names);

        SqlParameter cfg_dynobj_quality = new SqlParameter("@cfg_dynobj_quality", SqlDbType.Int);
        cfg_dynobj_quality.Value = Request["cfg_dynobj_quality"] != null ? Request["cfg_dynobj_quality"].ToString() : "";
        comm.Parameters.Add(cfg_dynobj_quality);

        SqlParameter cfg_flash_enabled = new SqlParameter("@cfg_flash_enabled", SqlDbType.Int);
        cfg_flash_enabled.Value = Request["cfg_flash_enabled"] != null ? Request["cfg_flash_enabled"].ToString() : "";
        comm.Parameters.Add(cfg_flash_enabled);

        SqlParameter cfg_env_anim = new SqlParameter("@cfg_env_anim", SqlDbType.Int);
        cfg_env_anim.Value = Request["cfg_env_anim"] != null ? Request["cfg_env_anim"].ToString() : "";
        comm.Parameters.Add(cfg_env_anim);

        SqlParameter cfg_spot_shadows = new SqlParameter("@cfg_spot_shadows", SqlDbType.Int);
        cfg_spot_shadows.Value = Request["cfg_spot_shadows"] != null ? Request["cfg_spot_shadows"].ToString() : "";
        comm.Parameters.Add(cfg_spot_shadows);

        SqlParameter sys_num_cpu = new SqlParameter("@sys_num_cpu", SqlDbType.Int);
        sys_num_cpu.Value = Request["sys_num_cpu"] != null ? Request["sys_num_cpu"].ToString() : "";
        comm.Parameters.Add(sys_num_cpu);

        SqlParameter sys_cpu_ratedfreq = new SqlParameter("@sys_cpu_ratedfreq", SqlDbType.Int);
        sys_cpu_ratedfreq.Value = Request["sys_cpu_ratedfreq"] != null ? Request["sys_cpu_ratedfreq"].ToString() : "";
        comm.Parameters.Add(sys_cpu_ratedfreq);

        SqlParameter sys_cpu_freq = new SqlParameter("@sys_cpu_freq", SqlDbType.Int);
        sys_cpu_freq.Value = Request["sys_cpu_freq"] != null ? Request["sys_cpu_freq"].ToString() : "";
        comm.Parameters.Add(sys_cpu_freq);

        SqlParameter sys_cpu_vendor = new SqlParameter("@sys_cpu_vendor", SqlDbType.VarChar);
        sys_cpu_vendor.Value = Request["sys_cpu_vendor"] != null ? Request["sys_cpu_vendor"].ToString() : "";
        comm.Parameters.Add(sys_cpu_vendor);

        SqlParameter sys_cpu_string = new SqlParameter("@sys_cpu_string", SqlDbType.VarChar);
        sys_cpu_string.Value = Request["sys_cpu_string"] != null ? Request["sys_cpu_string"].ToString() : "";
        comm.Parameters.Add(sys_cpu_string);

        SqlParameter sys_ram_total = new SqlParameter("@sys_ram_total", SqlDbType.Int);
        sys_ram_total.Value = Request["sys_ram_total"] != null ? Request["sys_ram_total"].ToString() : "";
        comm.Parameters.Add(sys_ram_total);

        SqlParameter sys_gfx_string = new SqlParameter("@sys_gfx_string", SqlDbType.VarChar);
        sys_gfx_string.Value = Request["sys_gfx_string"] != null ? Request["sys_gfx_string"].ToString() : "";
        comm.Parameters.Add(sys_gfx_string);

        SqlParameter sys_gfx_driver_file = new SqlParameter("@sys_gfx_driver_file", SqlDbType.VarChar);
        sys_gfx_driver_file.Value = Request["sys_gfx_driver_file"] != null ? Request["sys_gfx_driver_file"].ToString() : "";
        comm.Parameters.Add(sys_gfx_driver_file);

        SqlParameter sys_gfx_driver_ver = new SqlParameter("@sys_gfx_driver_ver", SqlDbType.VarChar);
        sys_gfx_driver_ver.Value = Request["sys_gfx_driver_ver"] != null ? Request["sys_gfx_driver_ver"].ToString() : "";
        comm.Parameters.Add(sys_gfx_driver_ver);


        SqlParameter sys_gfx_texmem = new SqlParameter("@sys_gfx_texmem", SqlDbType.Int);
        sys_gfx_texmem.Value = Request["sys_gfx_texmem"] != null ? Request["sys_gfx_texmem"].ToString() : "";
        comm.Parameters.Add(sys_gfx_texmem);


        SqlParameter sys_gfx_hwtnl = new SqlParameter("@sys_gfx_hwtnl", SqlDbType.Bit);
        sys_gfx_hwtnl.Value = 0;
        if (Request["sys_gfx_hwtnl"] != null)
        {
          if (Request["sys_gfx_hwtnl"].ToString() == "1")
          {
            sys_gfx_hwtnl.Value = 1;
          }
        }
        comm.Parameters.Add(sys_gfx_hwtnl);


        SqlParameter sys_info_hash = new SqlParameter("@sys_info_hash", SqlDbType.Int);
        sys_info_hash.Value = Request["sys_info_hash"] != null ? Request["sys_info_hash"].ToString() : "";
        comm.Parameters.Add(sys_info_hash);
      }
      catch (Exception)
      {
        Response.Write("Error 2");
      }
      
      // Execute
      try
      {
        //Response.Write(sqlString);
        
        Response.Write(comm.ExecuteNonQuery());
      }
      catch (Exception)
      {
        //Response.Write(e.ToString());
        Response.Write("Error 3");
      }

      // Close    
      try
      {
          cn.Close();
      }
      catch(Exception)
      {
        //Response.Write(e.ToString());
        Response.Write("Error 4");
      }


    }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
