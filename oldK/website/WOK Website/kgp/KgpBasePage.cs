///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;
using Kaneva.BusinessLayer.Facade;
using log4net;
using System.Xml;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// base page for kgp pages, with some helpers
	/// </summary>
	public class KgpBasePage : BasePage
	{
		protected bool m_fromWeb = false;
		protected Int32 m_userId = 0;
        protected Int32 m_playerId = 0;

        private UserFacade userFacade;
        private ShoppingFacade shoppingFacade;
		private CommunityFacade communityFacade;
        private FameFacade fameFacade;
        private GameDeveloperFacade gameDeveloperFacade;
        private DevelopmentCompanyFacade developmentCompanyFacade;
        private SiteSecurityFacade siteSecurityFacade;
        private GameFacade gameFacade;
        private BlastFacade blastFacade;
        private RaveFacade raveFacade;
        private ScriptGameItemFacade scriptGameItemFacade;
        private ExperimentFacade experimentFacade;
        private EventFacade eventFacade;

        const int CURRENCY_TYPE_CREDITS = 256;
        const int CURRENCY_TYPE_REWARDS = 512;
        const int CURRENCY_TYPE_BOTH_R_C = 768;
        string WOK_INVENTORY_TYPE_PERSONAL = "P";
        string WOK_INVENTORY_TYPE_BANK = "B";


        const int MAX_PURCHASE_AMOUNT_FOR_ZERO_PRICE_ITEMS = 100;

		protected bool fromWeb() { return m_fromWeb; }

		public static string KanevaWebServerName 
		{
			get 
			{
				return System.Configuration.ConfigurationManager.AppSettings ["KanevaWebServer"];
			}
        }

        #region Helper functions
        
        protected bool UserInGame( Int32 userId )
		{
            GameFacade gf = new GameFacade();
            return gf.UserInGame( userId, GameFacade.ANY_GAME );
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zoneType"></param>
        /// <param name="zoneIndexPlain"></param>
        /// <param name="zoneInstanceId"></param>
        /// <returns></returns>
        public bool IsAccessPassZone( Int32 zoneType, Int32 zoneIndexPlain, Int32 zoneInstanceId )
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            string dbName = KanevaGlobals.DbNameKGP;
            string selectSql = "";
            Hashtable parameters = new Hashtable();

            // have to hit different tables if in perm (4) or non-perm zone
            if (zoneType == Constants.PERMANENT_ZONE)
            {
                selectSql = "SELECT pass_group_id FROM " + dbName + ".pass_group_perm_channels" + 
                            " WHERE zone_index_plain = @zoneIndexPlain" +
                            "   AND pass_group_id = 1"; // 1 = access pass
                parameters.Clear();
                parameters.Add("@zoneIndexPlain", zoneIndexPlain);

                return dbUtility.ExecuteScalar( selectSql, parameters ) > 0;
            }
            else
            {
                selectSql = "SELECT pass_group_id FROM " + dbName + ".pass_group_channel_zones" +
                            " WHERE zone_type = @zoneType" +
                            "   AND zone_instance_id = @zoneInstanceId" +               
                            "   AND pass_group_id = 1"; // 1 = access pass
                parameters.Clear();
                parameters.Add("@zoneType", zoneType );
                parameters.Add("@zoneInstanceId", zoneInstanceId);

                return dbUtility.ExecuteScalar(selectSql, parameters) > 0;
            }
        }

        /// <summary>
        /// return if the user is in the game, and if they are in a zone
        /// that has AccessPass pass_group == 1
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="accessPassZone">true if AP</param>
        /// <returns>true if in game</returns>
        protected bool UserInGame(Int32 userId, ref bool accessPassZone)
        {
            accessPassZone = false;

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            string dbName = KanevaGlobals.DbNameKGP;

            string selectSql = "SELECT " + dbName + ".zoneType(current_zone_index) as zone_type, " + dbName + ".zoneIndex(current_zone_index) as zone_index_plain, current_zone_index, current_zone_instance_id " +
                                    " FROM " + dbName + ".`players` p INNER JOIN " + dbName + ".`player_zones` pz on p.player_id = pz.player_id" +
                                    " WHERE kaneva_user_id = @userId" +
                                    " AND in_game = 'T'";

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);

            DataRow playerInfo = dbUtility.GetDataRow(selectSql, parameters,false);
            if ( playerInfo != null )
            {
                accessPassZone = IsAccessPassZone(Convert.ToInt32(playerInfo["zone_type"]), 
                                                  Convert.ToInt32(playerInfo["zone_index_plain"]),
                                                  Convert.ToInt32(playerInfo["current_zone_instance_id"]));
                return true;
            }
            else
                return false;
        }

		protected void SendReply( string reply )
		{
			Response.Write("<html><body onload=\"document.location='kgp:" + HttpUtility.HtmlEncode( reply ) + "'\" /></html>" );
		}

        /// <summary>
        /// ValidateUser -  function that provides ability for a two attempt login. First uses normal CheckUserId
        /// then if that fails uses username and password to attempt validation if they are provided
        /// </summary>
        /// <param name="canBeLoggedIn">true if the user can be logged into WOK for this page to work</param>
        /// <param name="userName">true if the user can be logged into WOK for this page to work</param>
        /// <param name="passWord">true if the user can be logged into WOK for this page to work</param>
        /// <returns>bool</returns>
        protected bool ValidateUser(bool canBeLoggedIn, string userEmail, string passWord, int gameId)
        {
            int roleMemberShip = 0;

            return ValidateUser(canBeLoggedIn, userEmail, passWord, gameId, ref roleMemberShip);
        }

        /// <summary>
        /// ValidateUser -  function that provides ability for a two attempt login. First uses normal CheckUserId
        /// then if that fails uses username and password to attempt validation if they are provided
        /// </summary>
        /// <param name="canBeLoggedIn">true if the user can be logged into WOK for this page to work</param>
        /// <param name="userName">username</param>
        /// <param name="passWord">password</param>
        /// <param name="roleMemberShip">role membership id</param>
        /// <returns>bool</returns>
        protected bool ValidateUser(bool canBeLoggedIn, string userEmail, string passWord, int gameId, ref int roleMemberShip)
        {
            bool userIsValidated = false;

            //first try normal CheckUserId
            userIsValidated = Request.IsAuthenticated;

            //if the previosu fails try Authorizing the user if username and password are provided
            if (!userIsValidated && (userEmail != null) && (passWord != null) && (userEmail != "") && (passWord != ""))
            {
                int result = UsersUtility.Authorize(userEmail, passWord, gameId, ref roleMemberShip, "", false);

                //if result is success validate user. all other results ignore
                userIsValidated = (result == (int)Constants.eLOGIN_RESULTS.SUCCESS) ? true : false;
            }

            return userIsValidated;
        }

        protected bool IsRequestFromWebCaller()
        {
            return Request.UserAgent.Equals("WebCaller");
        }

		/// <summary>
		/// Ensure the userid is ok. (logged in, etc.)
		/// </summary>
		protected bool CheckUserId( bool canBeLoggedIn )
		{
            if (Request["game"] == null && Request.UserAgent != "WebCaller")
            {
                m_fromWeb = true;
            }

            // must be logged in always
            if (!Request.IsAuthenticated)
            {
                if (m_fromWeb)
                    Response.Redirect(GetErrorURL());
                else
                {
                    Response.StatusCode = 490;
                    Response.StatusDescription = "Not logged in";
                }

                return false;
            }

            m_userId = GetUserId();
            m_playerId = GetPlayerId();

            if (m_fromWeb && !canBeLoggedIn && UserInGame(m_userId))
            {
                // redirect
                Response.Redirect(ResolveUrl("~/kgp/userInGame.aspx"));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Ensure the userid is ok. (logged in, etc.)
        /// </summary>
        protected bool CheckUserId(bool canBeLoggedIn, bool checkAccessPass, ref bool inGame, ref bool accessPassZone)
        {
            inGame = false;
            accessPassZone = false;

			if ( Request["game"] == null && Request.UserAgent != "WebCaller")
			{
				m_fromWeb = true;
			}

			// must be logged in always
			if (!Request.IsAuthenticated)
			{
				if ( m_fromWeb )
					Response.Redirect (GetErrorURL ());
				else
				{
					Response.StatusCode = 490;
					Response.StatusDescription = "Not logged in";
				}

				return false;
			}

			m_userId = GetUserId();
            m_playerId = GetPlayerId();

            if (checkAccessPass)
                inGame = UserInGame(m_userId, ref accessPassZone);
            else
                inGame = UserInGame(m_userId);

            if (m_fromWeb && !canBeLoggedIn && inGame)
			{
			    // redirect
			    Response.Redirect ( ResolveUrl("~/kgp/userInGame.aspx") );
			    return false;
			}

			return true;
        }

        public ShoppingFacade GetShoppingFacade
        {
            get
            {
                if (shoppingFacade == null)
                {
                    shoppingFacade = new ShoppingFacade();
                }
                return shoppingFacade;
            }
        }

		public CommunityFacade GetCommunityFacade
		{
			get
			{
				if (communityFacade == null)
				{
					communityFacade = new CommunityFacade();
				}
				return communityFacade;
			}
		}

        public new UserFacade GetUserFacade
        {
            get
            {
                if (userFacade == null)
                {
                    userFacade = new UserFacade();
                }
                return userFacade;
            }
        }

        public FameFacade GetFameFacade
        {
            get
            {
                if (fameFacade == null)
                {
                    fameFacade = new FameFacade();
                }
                return fameFacade;
            }
        }

        public GameFacade GetGameFacade
        {
            get
            {
                if (gameFacade == null)
                {
                    gameFacade = new GameFacade();
                }
                return gameFacade;
            }
        }

        public GameDeveloperFacade GetGameDeveloperFacade
        {
            get
            {
                if (gameDeveloperFacade == null)
                {
                    gameDeveloperFacade = new GameDeveloperFacade();
                }
                return gameDeveloperFacade;
            }
        }

        public DevelopmentCompanyFacade GetDevelopmentCompanyFacade
        {
            get
            {
                if (developmentCompanyFacade == null)
                {
                    developmentCompanyFacade = new DevelopmentCompanyFacade();
                }
                return developmentCompanyFacade;
            }
        }

        public SiteSecurityFacade GetSiteSecurityFacade
        {
            get
            {
                if (siteSecurityFacade == null)
                {
                    siteSecurityFacade = new SiteSecurityFacade();
                }
                return siteSecurityFacade;
            }
        }

        public BlastFacade GetBlastFacade
        {
            get
            {
                if (blastFacade == null)
                {
                    blastFacade = new BlastFacade ();
                }
                return blastFacade;
            }
        }

        public RaveFacade GetRaveFacade
        {
            get
            {
                if (raveFacade == null)
                {
                    raveFacade = new RaveFacade();
                }
                return raveFacade;
            }
        }

        public ScriptGameItemFacade GetScriptGameItemFacade
        {
            get
            {
                if (scriptGameItemFacade == null)
                {
                    scriptGameItemFacade = new ScriptGameItemFacade();
                }
                return scriptGameItemFacade;
            }
        }

        public ExperimentFacade GetExperimentFacade
        {
            get
            {
                if (experimentFacade == null)
                {
                    experimentFacade = new ExperimentFacade();
                }
                return experimentFacade;
            }
        }

        public EventFacade GetEventFacade
        {
            get
            {
                if (eventFacade == null)
                {
                    eventFacade = new EventFacade();
                }
                return eventFacade;
            }
        }


        #endregion

        #region WOB to Web Blasts

        protected bool SendBlast(Blast blast, bool Admin, bool isCommunityBlast)
        {
            return SendBlast(blast, Admin, isCommunityBlast, "", -1);
        }

        protected bool SendBlast (Blast blast, bool Admin, bool isCommunityBlast, string url)
        {
            return SendBlast (blast, Admin, isCommunityBlast, url, -1);
        }

        //function throws no errors simply returns if the blast was sent or not
        protected bool SendBlast (Blast blast, bool Admin, bool isCommunityBlast, string url, int assetId)
        {
            int result = 0;

            if (blast != null)
            {
                //make a new instance of the blast Facade
                BlastFacade blastFacade = new BlastFacade();

                //determine the blas type for processing
                int type = blast.BlastType;

                switch (type)
                {
                    case (int)BlastTypes.BASIC:
                        if (Admin)
                        {
                            result = blastFacade.SendAdminTextBlast((int)blast.SenderId, blast.DateEvent, blast.SenderName, blast.NameNoSpaces, blast.DiaryEntry, blast.HighlightCss, blast.ThumbnailSmallPath, isCommunityBlast);
                        }
                        else
                        {
                            result = blastFacade.SendTextBlast ((int) blast.SenderId, (int) blast.CommunityId, blast.CommunityName, blast.IsCommunityPersonal, blast.SenderName, blast.NameNoSpaces, blast.DiaryEntry, blast.HighlightCss, blast.ThumbnailSmallPath, isCommunityBlast);
                        }
                        break;
                    case (int)BlastTypes.CAMERA_SCREENSHOT:
                        if (blast.CommunityId == int.MaxValue)
                        {   // This case is to handle public places that do not have communities or game ids
                            result = blastFacade.SendCameraScreenshotBlast ((int) blast.SenderId, blast.CommunityName, url, blast.DiaryEntry, assetId);
                        }
                        else
                        {   
                            result = blastFacade.SendCameraScreenshotBlast ((int) blast.SenderId, (int) blast.CommunityId, url, blast.DiaryEntry, assetId);
                        }
                        break;
                    case (int)BlastTypes.EVENT:
                        if (Admin)
                        {
                            //blastFacade.SendAdminEventBlast(blast.SenderId, blast.DateEvent, blast.SenderName, blast.NameNoSpaces, blast.EventName, blast.Location, blast. blast.DiaryEntry, blast.HighlightCss, blast.ThumbnailSmallPath, isCommunityBlast);
                        }
                        else
                        {
                            //blastFacade.SendEventBlast(blast.SenderId, blast.CommunityId, blast.SenderName, blast.NameNoSpaces, blast.EventName, blast.Location, blast.DiaryEntry, blast.HighlightCss, blast.ThumbnailSmallPath);
                        }
                        break;
                    case (int)BlastTypes.LINK:
                        if (Admin)
                        {
                            //blastFacade.SendAdminLinkBlast();
                        }
                        else
                        {
                            //blastFacade.SendLinkBlast();
                        }
                        break;
                    case (int)BlastTypes.PHOTO:
                        if (Admin)
                        {
                            result = blastFacade.SendAdminPhotoBlast((int)blast.SenderId, blast.DateEvent, blast.SenderName, blast.NameNoSpaces, url, blast.DiaryEntry, blast.HighlightCss, blast.ThumbnailSmallPath, isCommunityBlast);
                        }
                        else
                        {
                            result = blastFacade.SendPhotoBlast((int)blast.SenderId, (int)blast.CommunityId, blast.CommunityName, blast.IsCommunityPersonal, blast.SenderName, blast.NameNoSpaces, url, blast.DiaryEntry, blast.HighlightCss, blast.ThumbnailSmallPath, isCommunityBlast);
                        }
                        break;
                    case (int)BlastTypes.PLACE:
                        if (Admin)
                        {
                            //blastFacade.SendAdminPlaceBlast();
                        }
                        else
                        {
                            //blastFacade.SendPlaceBlast();
                        }
                        break;
                    case (int)BlastTypes.VIDEO:
                        if (Admin)
                        {
                            //blastFacade.SendAdminVideoBlast();
                        }
                        else
                        {
                            //blastFacade.SendVideoBlast();
                        }
                        break;
                    case (int)BlastTypes.ADD_FRIEND:
                        break;
                    case (int)BlastTypes.ADMIN:
                        break;
                    case (int)BlastTypes.AWARD:
                        break;
                    case (int)BlastTypes.BLOG:
                        break;
                    case (int)BlastTypes.FAME:
                        break;
                    case (int)BlastTypes.FORUM_POST:
                        break;
                    case (int)BlastTypes.JOIN_COMMUNITY:
                        break;
                    case (int)BlastTypes.MEDIA_COMMENT:
                        break;
                    case (int)BlastTypes.PROFILE_UPDATE:
                        break;
                    case (int)BlastTypes.RAVE_3D_HANGOUT:
                        break;
                    case (int)BlastTypes.RAVE_3D_HOME:
                        break;
                    case (int)BlastTypes.SEND_GIFT:
                        break;
                }
            }
            return (result == 1);
        }

        #endregion


        #region Purchase On Web

        //this function throws and exception to the calling function 
        //calling function should have logic to catch and return the message
        //returns null on success error message on failure
        protected string BuyItem(string creditType, int itemId, int quantity, int userId, bool giveFirstTimeShopFame)
        {
            WOKItem item = GetShoppingFacade.GetItem(itemId);
            int successfulPayment = -1;

            // See if user gets VIP pricing
            int iOrderTotal = item.WebPrice * quantity; 
            if (KanevaWebGlobals.CurrentUser.HasVIPPass)
            {
                iOrderTotal = item.WebPriceVIP * quantity;
            }

            // Make sure it is a valid quantity
            if (quantity < 1)
            {
                return "Quantity must be greater than 0";
            }

            // Make sure the items is Available for purchase
            if (item.ItemActive.Equals((int)WOKItem.ItemActiveStates.Deleted))
            {
                return "This item is not available for purchase.";
            }

            // Make sure only only privates can update it
            if (item.ItemActive.Equals((int)WOKItem.ItemActiveStates.Private))
            {
                if (!(item.ItemCreatorId.Equals(Convert.ToUInt32(KanevaWebGlobals.CurrentUser.UserId))))
                {
                    return "This item is not available for purchase.";
                }
            }

            // Is it available for purchase?
            if (item.InventoryType.Equals(0))
            {
                return "This item is not available for purchase.";
            }

            // Is it access pass?
            if (GetShoppingFacade.IsItemAccessPass(itemId) && !KanevaWebGlobals.CurrentUser.HasAccessPass)
            {
                return "This item requires Access Pass to purchase! Please purchase an Access Pass first.";
            }

            // Make sure this item can be purchased with rewards
            if (creditType.Equals(Constants.CURR_GPOINT))
            {
                // Make sure they have enough rewards
                UInt64 iRewards = Convert.ToUInt64(UsersUtility.getUserBalance(userId, Constants.CURR_GPOINT).ToString());

                if ((iRewards < Convert.ToUInt64(iOrderTotal)))
                {
                    return "Not enough rewards to purchase this item!";
                }

                // Make sure it is correct currency type for rewards.
                if (!item.InventoryType.Equals(CURRENCY_TYPE_REWARDS) && !item.InventoryType.Equals(CURRENCY_TYPE_BOTH_R_C))
                {
                    return "This item cannot be purchased with Rewards";
                }
            }
            else
            {
                // Does the user have enough credits?
                UInt64 iCredits = Convert.ToUInt64(UsersUtility.GetUserPointTotal(userId).ToString());

                if ((iCredits < Convert.ToUInt64(iOrderTotal)))
                {
                    return "Not enough credits to purchase this item!";
                }
            }


            try
            {
                if (creditType.Equals(Constants.CURR_GPOINT))
                {
                    successfulPayment = GetUserFacade.AdjustUserBalanceWithDetails(userId, Constants.CURR_GPOINT, -Convert.ToDouble(iOrderTotal), Constants.CASH_TT_INSTANT_BUY, itemId, quantity);
                }
                else
                {
                    successfulPayment = GetUserFacade.AdjustUserBalanceWithDetails(userId, Constants.CURR_KPOINT, -Convert.ToDouble(iOrderTotal), Constants.CASH_TT_INSTANT_BUY, itemId, quantity);
                }
            }
            catch (Exception)
            {
                return "Not enough credits to purchase this item!";
            }

            if (successfulPayment == 0)  //0 is success according to AdjustUserBalance notes
            {
                if (item.UseType == (int)WOKItem.USE_TYPE_BUNDLE)
                {
                    ProcessBundlePurchase(userId, item, quantity, creditType.Equals(Constants.CURR_GPOINT), iOrderTotal, giveFirstTimeShopFame, 0);
                }
                else if (item.UseType == (int)WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
                {
                    ProcessScriptGameItemPurchase(userId, item, quantity, creditType.Equals(Constants.CURR_GPOINT), iOrderTotal, giveFirstTimeShopFame);
                }
                else
                {
                    ProcessItemPurchase(userId, item, quantity, creditType.Equals(Constants.CURR_GPOINT), iOrderTotal, 0, true, true, false, giveFirstTimeShopFame);
                }
            }

            return null;
        }

        /// <summary>
        /// PurchaseItem
        /// </summary>
        private int ProcessItemPurchase(int userId, WOKItem item, int quantity, bool bRewards, int iOrderTotal, int parentPurchaseId, bool bAllowOwnerCommission, bool bAddToInventory, bool bAddToBank, bool giveFirstTimeShopFame)
        {
            UInt32 iDesignerCommision = Convert.ToUInt32(item.DesignerPrice * quantity);

            UInt32 iTemplateCreatorId = 0;
            UInt32 iTemplateCreatorsComission;
            GetUGCTemplateInfo(item, out iTemplateCreatorId, out iTemplateCreatorsComission);
            iTemplateCreatorsComission *= Convert.ToUInt32(quantity);
            int purchaseId = 0;
            int txnType = (int)(KanevaWebGlobals.CurrentUser.HasVIPPass ? ItemPurchase.eItemTxnType.VIP_PURCHASE : ItemPurchase.eItemTxnType.PURCHASE);

            // Give them the item
            if (bRewards)
            {
                if (bAddToInventory)
                {
                    GetUserFacade.AddItemToPendingInventory(userId, WOK_INVENTORY_TYPE_PERSONAL, item.GlobalId, quantity, 512);
                }
                else if (bAddToBank)
                {
                    GetUserFacade.AddItemToPendingInventory(userId, WOK_INVENTORY_TYPE_BANK, item.GlobalId, quantity, 512);
                }

                // Add it to thier history
                purchaseId = GetShoppingFacade.AddItemPurchase(item.GlobalId, txnType, userId, item.ItemCreatorId, iOrderTotal, 0, Common.GetVisitorIPAddress(), Constants.CURR_GPOINT, quantity, parentPurchaseId);
            }
            else
            {
                if (bAddToInventory)
                {
                    GetUserFacade.AddItemToPendingInventory(userId, WOK_INVENTORY_TYPE_PERSONAL, item.GlobalId, quantity, 256);
                }
                else if (bAddToBank)
                {
                    GetUserFacade.AddItemToPendingInventory(userId, WOK_INVENTORY_TYPE_BANK, item.GlobalId, quantity, 256);
                }

                // Add it to thier history
                purchaseId = GetShoppingFacade.AddItemPurchase(item.GlobalId, txnType, userId, item.ItemCreatorId, iOrderTotal, iDesignerCommision, Common.GetVisitorIPAddress(), Constants.CURR_KPOINT, quantity, parentPurchaseId);
            }

            // Credit the owner if it is a custom item
            // Animesh email titled "UGC credits/Rewards and upload changes" - UGC commission will be based on credits purchases only             
            if (iDesignerCommision > 0 && bAllowOwnerCommission)
            {
                if (!bRewards)
                {
                    GetUserFacade.AdjustUserBalance(Convert.ToInt32(item.ItemCreatorId), Constants.CURR_KPOINT, Convert.ToDouble(iDesignerCommision), Constants.CASH_TT_ROYALITY);
                }
            }

            if (!bRewards)
            {
                GetFameFacade.RedeemPacket(Convert.ToInt32(item.ItemCreatorId), (int)PacketIdFashion.PURCHASE_WITH_CREDS, (int)FameTypes.Fashion);
                GetFameFacade.RedeemPacket(Convert.ToInt32(item.ItemCreatorId), (int)PacketId.FASHION_PURCHASE_WITH_CREDS, (int)FameTypes.World);
            }
            else if (bRewards)
            {
                GetFameFacade.RedeemPacket(Convert.ToInt32(item.ItemCreatorId), (int)PacketIdFashion.PURCHASE_WITH_REWARDS, (int)FameTypes.Fashion);
            }

            // Credit template designer if it is based on a custom template
            if (iTemplateCreatorsComission > 0 && bAllowOwnerCommission)
            {
                if (!bRewards)
                {
                    GetUserFacade.AdjustUserBalance(Convert.ToInt32(iTemplateCreatorId), Constants.CURR_KPOINT, Convert.ToDouble(iTemplateCreatorsComission), Constants.CASH_TT_ROYALITY);

                    // Get base item global ID (the first derivative from the same template created by template designer)
                    int baseItemGlid = GetShoppingFacade.GetBaseItemGlobalId(item.GlobalId);

                    // Add template commission transaction to shopping history
                    GetShoppingFacade.AddItemPurchase(baseItemGlid, (int)ItemPurchase.eItemTxnType.COMMISSION, userId, iTemplateCreatorId, 0, iTemplateCreatorsComission, Common.GetVisitorIPAddress(), Constants.CURR_KPOINT, quantity, parentPurchaseId);
                }
            }

            if (!bRewards)
            {
                GetFameFacade.RedeemPacket(Convert.ToInt32(iTemplateCreatorId), (int)PacketIdFashion.PURCHASE_WITH_CREDS, (int)FameTypes.Fashion);
                GetFameFacade.RedeemPacket(Convert.ToInt32(iTemplateCreatorId), (int)PacketId.FASHION_PURCHASE_WITH_CREDS, (int)FameTypes.World);						
            }
            else if (bRewards)
            {
                GetFameFacade.RedeemPacket(Convert.ToInt32(iTemplateCreatorId), (int)PacketIdFashion.PURCHASE_WITH_REWARDS, (int)FameTypes.Fashion);
            }

            //Only give first time Shop Kaneva fame if not from instant buy
            if (giveFirstTimeShopFame)
            {
                GetFameFacade.RedeemPacket(KanevaWebGlobals.CurrentUser.UserId, (int)PacketId.BUY_SHOP_1_ITEM, (int)FameTypes.World);
            }

            // Give purchaser fashion fame
            GetFameFacade.RedeemPacket(userId, (int)PacketIdFashion.PURCHASED_ITEM, (int)FameTypes.Fashion);
            GetFameFacade.RedeemPacket(userId, (int)PacketId.BUY_AN_ITEM, (int)FameTypes.World);
            GetFameFacade.RedeemPacket(userId, (int)PacketId.BUY_SHOP_3_ITEMS, (int)FameTypes.World);
            GetFameFacade.RedeemPacket(userId, (int)PacketId.BUY_SHOP_5_ITEMS, (int)FameTypes.World);
            GetFameFacade.RedeemPacket(userId, (int)PacketId.BUY_SHOP_10_ITEMS, (int)FameTypes.World);
            GetFameFacade.RedeemPacket(userId, (int)PacketId.BUY_SHOP_20_ITEMS, (int)FameTypes.World);
            GetFameFacade.RedeemPacket(userId, (int)PacketId.BUY_SHOP_50_ITEMS, (int)FameTypes.World);
            GetFameFacade.RedeemPacket(userId, (int)PacketId.BUY_SHOP_100_ITEMS, (int)FameTypes.World);
            GetFameFacade.RedeemPacket(userId, (int)PacketId.BUY_SHOP_250_ITEMS, (int)FameTypes.World);
            GetFameFacade.RedeemPacket(userId, (int)PacketId.BUY_SHOP_500_ITEMS, (int)FameTypes.World);
            GetFameFacade.RedeemPacket(userId, (int)PacketId.BUY_SHOP_1000_ITEMS, (int)FameTypes.World);

            if (item.UseType == WOKItem.USE_TYPE_ANIMATION)
            {
                GetFameFacade.RedeemPacket(userId, (int)PacketId.BUY_ANIMATION, (int)FameTypes.World);
            }

            return purchaseId;
        }

        /// <summary>
        /// PurchaseBundle
        /// </summary>
        private void ProcessBundlePurchase(int userId, WOKItem bundle, int quantity, bool bRewards, int iOrderTotal, bool giveFirstTimeShopFame, int parentPurchaseId, bool addOnlyToBank = false)
        {
            UInt32 iDesignerCommision = Convert.ToUInt32(bundle.DesignerPrice * quantity);
            int purchaseId = 0;
            int txnType = (int)(KanevaWebGlobals.CurrentUser.HasVIPPass ? ItemPurchase.eItemTxnType.VIP_PURCHASE : ItemPurchase.eItemTxnType.PURCHASE);

            // add the bundle purchase 
            if (bRewards)
            {
                purchaseId = GetShoppingFacade.AddItemPurchase(bundle.GlobalId, txnType, KanevaWebGlobals.CurrentUser.UserId, bundle.ItemCreatorId, iOrderTotal, 0, Common.GetVisitorIPAddress(), Constants.CURR_GPOINT, quantity, parentPurchaseId);
            }
            else
            {
                purchaseId = GetShoppingFacade.AddItemPurchase(bundle.GlobalId, txnType, KanevaWebGlobals.CurrentUser.UserId, bundle.ItemCreatorId, iOrderTotal, 0, Common.GetVisitorIPAddress(), Constants.CURR_KPOINT, quantity, parentPurchaseId);
            }

            // Give bundle commission
            if (iDesignerCommision > 0)
            {
                if (!bRewards)
                {
                    GetUserFacade.AdjustUserBalance(Convert.ToInt32(bundle.ItemCreatorId), Constants.CURR_KPOINT, Convert.ToDouble(iDesignerCommision), Constants.CASH_TT_ROYALITY);
                }
            }

            bool bAllowOwnerCommission = true;

            // add items to users inventory
            foreach (WOKItem item in bundle.BundleItems)
            {
                bAllowOwnerCommission = !(bundle.ItemCreatorId.Equals(item.ItemCreatorId));

                ProcessItemPurchase(userId, item, quantity, bRewards, item.WebPrice, purchaseId, bAllowOwnerCommission, !addOnlyToBank, addOnlyToBank, giveFirstTimeShopFame);
            }

        }

        /// <summary>
        /// ProcessScriptGameItemPurchase
        /// </summary>
        private void ProcessScriptGameItemPurchase(int userId, WOKItem wokItem, int quantity, bool bRewards, int iOrderTotal, bool giveFirstTimeShopFame)
        {
            UInt32 iDesignerCommision = Convert.ToUInt32(wokItem.DesignerPrice * quantity);

            int purchaseId = ProcessItemPurchase(userId, wokItem, quantity, bRewards, wokItem.WebPrice, 0, true, true, false, giveFirstTimeShopFame);

            ScriptGameItem sgItem = GetScriptGameItemFacade.GetSnapshotScriptGameItem(wokItem.GlobalId);

            if (sgItem.BundleGlid > 0)
            {
                WOKItem bundle = GetShoppingFacade.GetItem(sgItem.BundleGlid);
                ProcessBundlePurchase(userId, bundle, quantity, bRewards, iOrderTotal, giveFirstTimeShopFame, purchaseId, true);
            }
        }

        protected string BuyPremiumItem(string creditType, int itemId, int quantity, int userId, int communityId)
        {
            //  Premium items can only be purchased with credits ... for now
            if (creditType.Equals(Constants.CURR_CREDITS))
            {
                if (quantity > 0)
                {
                    int successfulPayment = -1;
                    WOKItem item = GetShoppingFacade.GetItem(itemId, true);
                    
                    // Make sure the item is available for purchase && a premium item
                    if (!item.ItemActive.Equals ((int) WOKItem.ItemActiveStates.Deleted) && item.UseType == WOKItem.USE_TYPE_PREMIUM)
                    {
                        // Does the user have enough credits?
                        UInt64 credits = Convert.ToUInt64(UsersUtility.GetUserPointTotal(userId).ToString());
                        UInt32 iOrderTotal = Convert.ToUInt32(item.DesignerPrice * quantity);
                        
                        if ((credits >= Convert.ToUInt64(iOrderTotal)))
                        {
                            try
                            {
                                successfulPayment = GetUserFacade.AdjustUserBalanceWithDetails(userId, Constants.CURR_KPOINT, -Convert.ToDouble(iOrderTotal), Constants.CASH_TT_PREMIUM_ITEM, itemId, quantity);
                            }
                            catch (Exception)
                            {
                                return "Not enough credits to purchase this item";
                            }

                            if (successfulPayment == 0)
                            {
                                // Add it to their history
                                int purchaseId = GetShoppingFacade.AddItemPurchase(itemId, (int)ItemPurchase.eItemTxnType.PURCHASE, userId, item.ItemCreatorId, Convert.ToInt32(iOrderTotal), iOrderTotal, Common.GetVisitorIPAddress(), Constants.CURR_KPOINT, quantity, 0);

                                // Add record for premium item credit redemption
                                if (purchaseId > 0)
                                {
                                    GetShoppingFacade.InsertPremiumItemCreditRedemption (purchaseId, communityId);
                                }
                                           
                                return null;
                            }
                            else
                            {
                                return "Unknown error occurred when attempting to adjust the user's balance";
                            }
                        }
                        else
                        {
                            return "Not enough credits to purchase this item";
                        }
                    }
                    else
                    {
                        return "This item is not available for purchase";
                    }
                }
                else
                {
                    return "Quantity must be greater than 0";
                }
            }
            else
            {
                return "This item can only be purchased with Credits";
            }
        }
        
        bool GetUGCTemplateInfo(WOKItem item, out UInt32 templateCreatorId, out UInt32 commission)
        {
            templateCreatorId = 0;
            commission = 0;

            // obtain template info (if any)
            WOKItem baseItem = null;
            if (item.BaseGlobalId != 0)
                baseItem = GetShoppingFacade.GetItem(Convert.ToInt32(item.BaseGlobalId), true);

            if (baseItem != null && !baseItem.IsKanevaOwned)    // has template and not designed by Kaneva
            {
                templateCreatorId = baseItem.ItemCreatorId;
                commission = baseItem.DesignerPrice;
                return true;
            }

            return false;
        }

        #endregion


        #region Raves

        protected int RaveProfile (int userId, int communityId, string raveType, string currency)
        {
            try
            {
                // Verify user has enough credits/rewards to purchase rave
                RaveFacade raveFacade = new RaveFacade ();
                UserBalances ub = new UserFacade ().GetUserBalances (userId);
                RaveType.eRAVE_TYPE eRT = (raveType == "mega" ? RaveType.eRAVE_TYPE.MEGA : RaveType.eRAVE_TYPE.SINGLE);
                RaveType rt = WebCache.GetRaveType (eRT);
            
                // If rave is not free, then verify user has enough rewards/credits to pay for it
                if (!raveFacade.IsCommunityRaveFree (userId, communityId, eRT))
                {
                    int ret = raveFacade.UserHasFundsToBuyRave (rt, currency, ub);
                    if (!ret.Equals (0))
                    {
                        return ret;
                    }
                }

                // Rave the profile
                //new RaveFacade ().RaveCommunity (userId, communityId, RaveType.eRAVE_TYPE.SINGLE, currency);
                new RaveFacade ().RaveCommunity (userId, communityId, rt, currency);
            }
            catch 
            {
                return -1;    
            }
            return 0;
        }

        protected IList<RaveType> GetRaveInformation()
        {
            RaveFacade raveFacade = new RaveFacade();
            return WebCache.GetAllRaveTypes();
        }

        #endregion Raves


        #region Helper function for 3dapp achievements, premium items and leaderboards

        /// <summary>
        /// AddGameAchievement
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="gameId"></param>
        /// <param name="achievementName"></param>
        /// <param name="achievementDescription"></param>
        /// <param name="achievementPoints"></param>
        /// <param name="imageAssetId"></param>
        /// <returns>Achievement ID</returns>
        public uint AddGameAchievement(int userId, int gameId, string achievementName, string achievementDescription, UInt32 achievementPoints, int imageAssetId)
        {
            // check for valid name and description
            if (achievementName == "" || achievementDescription == "" ||
                KanevaWebGlobals.isTextRestricted(achievementName, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted(achievementDescription, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                throw new ArgumentException( "Achievement name/description is invalid", "namedesc" );
            }

            if (KanevaWebGlobals.ContainsInjectScripts(achievementName) ||
                KanevaWebGlobals.ContainsInjectScripts(achievementDescription))
            {
                throw new ArgumentException( "User " + userId + " tried to script from IP " + Common.GetVisitorIPAddress(), "namedesc" );
            }

            // check if there are enough points
            int availablePoints = KanevaGlobals.MaxAchievementPoints - GetGameFacade.GetTotalAchievementPoints(GetCommunityFacade.GetCommunityIdFromGameId(gameId), "");
            if (availablePoints <= 0)
            {
                throw new ArgumentException("There are no more points to create any new achievements", "availpoints");
            }

            if (achievementPoints > availablePoints)
            {
                throw new ArgumentException("Not enough available points to create new achievement", "points");
            }

            int communityId = GetCommunityFacade.GetCommunityIdFromGameId(gameId);

            //Search existing record for matching names.
            try
            {
                PagedList<Achievement> achievementList = GetGameFacade.AchievementForApp(communityId, "", 0, 999999);
                for (int i = 0; achievementList!=null && i<achievementList.Count; i++)
                {
                    Achievement ach = achievementList[i];
                    if (ach.Name == achievementName)
                    {
                        return ach.AchievementsId;
                    }
                }
            }
            catch (Exception)
            {
            }

            Achievement achievement = new Achievement();

            // build the achievement
            achievement.CommunityId = communityId;
            achievement.Name = achievementName;
            achievement.Description = achievementDescription;
            achievement.Points = achievementPoints;

            // save the achievement
            uint achievementId = GetGameFacade.SaveAchievement(achievement);

            // optional 
            if (imageAssetId > 0)
            {
                CreateAchievementImage(imageAssetId, userId, achievementId);
            }

            return achievementId;
        }

        /// <summary>
        /// AddGamePremiumItem
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="gameId"></param>
        /// <param name="itemName"></param>
        /// <param name="itemDescription"></param>
        /// <param name="itemPrice"></param>
        /// <param name="imageAssetId"></param>
        /// <returns>Achievement ID</returns>
        public int AddGamePremiumItem(int userId, int gameId, string itemName, string itemDescription, UInt32 itemPrice, int imageAssetId)
        {
            // check for valid name and description
            if (itemName == "" || itemDescription == "" ||
                KanevaWebGlobals.isTextRestricted(itemName, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted(itemDescription, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                throw new ArgumentException("Item name/description is invalid", "namedesc");
            }

            if (KanevaWebGlobals.ContainsInjectScripts(itemName) || KanevaWebGlobals.ContainsInjectScripts(itemDescription))
            {
                throw new ArgumentException("User " + userId + " tried to script from IP " + Common.GetVisitorIPAddress(), "namedesc");
            }

            WOKItem item = new WOKItem();
            item.BaseGlobalId = 0;
            item.ItemCreatorId = Convert.ToUInt32(GetUserId());
            item.Name = Server.HtmlEncode(itemName.Trim());
            item.DisplayName = Server.HtmlEncode(itemName.Trim());
            item.Description = Server.HtmlEncode(itemDescription.Trim());
            item.Keywords = "";
            item.ArmAnywhere = 1;
            item.DestroyWhenUsed = 0;
            item.Disarmable = 1;
            item.InventoryType = 256;
            item.ItemActive = (int)WOKItem.ItemActiveStates.Public;
            item.MarketCost = 0;
            item.RequiredSkill = 0;
            item.RequiredSkillLevel = 0;
            item.SellingPrice = 0;
            item.Stackable = 1;
            item.UseType = WOKItem.USE_TYPE_PREMIUM;
            item.DerivationLevel = WOKItem.DRL_KANEVAITEM;
            item.ApprovalStatus = WOKItem.DEFAULT_PREMIUM_ITEM_STATUS;
            item.DesignerPrice = itemPrice;

            //Search existing record for matching names.
            try
            {
                PagedList<WOKItem> existingItems = GetShoppingFacade.GetPremiumItems(gameId, 999999, 0, "", true);
                for (int i = 0; existingItems != null && i < existingItems.Count; i++)
                {
                    WOKItem existingItem = existingItems[i];
                    if (existingItem.Name == item.Name)
                    {
                        return existingItem.GlobalId;
                    }
                }
            }
            catch (Exception)
            {
            }

            //create the template and get the base id for item
            int baseGlobalId = GetShoppingFacade.CreateItemTemplate(item);
            if (baseGlobalId <= 0)
            {
                throw new ArgumentException("CreateItemTemplate returned baseGlobalId of " + baseGlobalId, "baseglid");
            }

            item.BaseGlobalId = Convert.ToUInt32(baseGlobalId);

            int globalId = GetShoppingFacade.AddCustomItem(item);
            if (globalId <= 0)
            {
                throw new ArgumentException("AddCustomItem returned globalId of " + globalId, "glid");
            }

            //add the item to the game
            GetShoppingFacade.AddItemToGame(globalId, gameId, (int)item.ApprovalStatus);

            // optional 
            if (imageAssetId > 0)
            {
                CreatePremiumItemImage(imageAssetId, userId, globalId);
            }

            return globalId;
        }

        /// <summary>
        /// SetupGameLeaderBoard
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="gameId"></param>
        /// <param name="leaderBoardName"></param>
        /// <param name="leaderBoardLabel"></param>
        /// <param name="leaderBoardType"></param>
        /// <param name="leaderBoardSort"></param>
        /// <returns>Achievement ID</returns>
        public bool SetupGameLeaderBoard(int userId, int gameId, string leaderBoardName, string leaderBoardLabel, int leaderBoardType, string leaderBoardSort)
        {
            // check for valid name and description
            if (leaderBoardName == "" || leaderBoardLabel == "" ||
                KanevaWebGlobals.isTextRestricted(leaderBoardName, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted(leaderBoardLabel, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                throw new ArgumentException("leaderboard name/description is invalid", "namedesc");
            }

            if (KanevaWebGlobals.ContainsInjectScripts(leaderBoardName) || KanevaWebGlobals.ContainsInjectScripts(leaderBoardLabel))
            {
                throw new ArgumentException("User " + userId + " tried to script from IP " + Common.GetVisitorIPAddress(), "namedesc");
            }

            if (leaderBoardType != (int)Leaderboard.FormatType.General && leaderBoardType != (int)Leaderboard.FormatType.Currency && leaderBoardType != (int)Leaderboard.FormatType.Time)
            {
                throw new ArgumentException("leaderboard's type is invalid", "type");
            }

            if (leaderBoardSort.ToUpper() != Leaderboard.SORTBY_ASC && leaderBoardSort.ToUpper() != Leaderboard.SORTBY_DESC)
            {
                throw new ArgumentException("leaderboard's sort is invalid", "sort");
            }

            Leaderboard leaderboard = new Leaderboard();

            //build the Leaderboard - fill in object values
            leaderboard.Name = leaderBoardName.Trim();
            leaderboard.ColumnName = leaderBoardLabel.Trim();
            leaderboard.FormatForValue = Convert.ToInt32(leaderBoardType);
            leaderboard.SortBy = leaderBoardSort;
            leaderboard.CommunityId = GetCommunityFacade.GetCommunityIdFromGameId(gameId);

            // Save the changes
            GetGameFacade.SaveLeaderBoard(leaderboard);

            return true;
        }

        public void CreateAchievementImage(int assetId, int userId, uint achievementId)
        {
            ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataRow drImageData = StoreUtility.GetAssetImage(assetId);

            if (drImageData != null && drImageData["asset_type_id"]!=null && Convert.ToInt32(drImageData["asset_type_id"]).Equals((int)Constants.eASSET_TYPE.PICTURE))
            {
                // create new image file from assetId's image file
                string fileName = drImageData["content_extension"].ToString();
                string imagePath = drImageData["target_dir"].ToString().Replace("/", "\\") + "\\" + fileName;
                string newPath = Path.Combine(Path.Combine(KanevaGlobals.ContentServerPath, userId.ToString()), "ach" + achievementId.ToString());

                //copy file to patch location or notify server to do this
                bool copySuccessful = false;
                try
                {
                    FileInfo fileInfo = new FileInfo(newPath + Path.DirectorySeparatorChar);
                    if (!fileInfo.Directory.Exists)
                    {
                        fileInfo.Directory.Create();
                    }

                    File.Copy(imagePath, Path.Combine(newPath, fileName));
                    copySuccessful = true;
                }
                catch (Exception ex)
                {
                    logger.Error("Error creating image file ", ex);
                }

                if (copySuccessful)
                {
                    ImageHelper.GenerateAchievementThumbs(fileName, Path.Combine(newPath, fileName), KanevaGlobals.ContentServerPath, userId, (int)achievementId);
                }
            }
        }

        public void CreatePremiumItemImage(int assetId, int userId, int globalId)
        {
            ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataRow drImageData = StoreUtility.GetAssetImage(assetId);

            if (drImageData!=null && drImageData["asset_type_id"]!=null && Convert.ToInt32(drImageData["asset_type_id"]).Equals((int)Constants.eASSET_TYPE.PICTURE))
            {
                // create new image file from assetId's image file
                string fileName = drImageData["content_extension"].ToString();
                string imagePath = drImageData["target_dir"].ToString().Replace("/", "\\") + "\\" + fileName;
                string newPath = Path.Combine(Path.Combine(KanevaGlobals.TexturePath, userId.ToString()), globalId.ToString());

                //copy file to patch location or notify server to do this
                bool copySuccessful = false;
                try
                {
                    FileInfo fileInfo = new FileInfo(newPath + Path.DirectorySeparatorChar);
                    if (!fileInfo.Directory.Exists)
                    {
                        fileInfo.Directory.Create();
                    }

                    File.Copy(imagePath, Path.Combine(newPath, fileName));
                    copySuccessful = true;
                }
                catch (Exception ex)
                {
                    logger.Error("Error In gameAddPremiumItem.aspx: error creating image file ", ex);
                }

                if (copySuccessful)
                {
                    ImageHelper.GenerateWOKItemThumbs(fileName, Path.Combine(newPath, fileName), KanevaGlobals.TexturePath, userId, globalId);
                }
            }
        }

        #endregion

        #region Passthrough

        /// <summary>
        /// Passthrough value to be returned in XML results.  Possible use includes as an
        /// identifier to distinquish multiple calls made to this page.
        /// </summary>
        public string PassthroughValue
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Request.Params["passthrough"]))
                {
                    return Request.Params["passthrough"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Shorthand property for returning a string representation of the Passthrough XML tag to be used in
        /// results.
        /// </summary>
        public string PassthroughTag
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(PassthroughValue))
                {
                    return "<Passthrough>" + PassthroughValue + "</Passthrough>";
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Called to insert the passthrough tag in an existing XmlDocument.
        /// </summary>
        public void InsertPassthroughTag(ref XmlDocument doc)
        {
            if (!string.IsNullOrWhiteSpace(PassthroughValue))
            {
                XmlNode root = doc.DocumentElement;
                XmlElement elem = doc.CreateElement("Passthrough");
                elem.InnerText = PassthroughValue;
                root.InsertBefore(elem, root.FirstChild);
            }
        }

        /// <summary>
        /// Called to replace any Passthrough tags in an existing xml response string with the new values.
        /// This is important for caching. Passthrough value should be immune to caching.
        /// </summary>
        public void ReplacePassthroughTag(ref string xmlResponse)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlResponse);

            XmlNode root = doc.DocumentElement;
            XmlNodeList nodeList = root.SelectNodes("descendant::Passthrough");

            foreach (XmlNode node in nodeList)
            {
                if (!string.IsNullOrWhiteSpace(PassthroughValue))
                {
                    XmlElement elem = doc.CreateElement("Passthrough");
                    elem.InnerText = PassthroughValue;
                    root.ReplaceChild(elem, node);
                }
                else
                {
                    root.RemoveChild(node);
                }
            }

            xmlResponse = root.OuterXml;
        }

        #endregion
    }
}
