///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;
using System.Collections.Generic;
using System.Linq;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class deedInfo : KgpBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.Params["zoneIndex"] == null) || (Request.Params["instanceId"] == null) || (Request.Params["zone_index_plain"] == null))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>zoneIndex or instanceId or zone_index_plain not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            try
            {
                
                int zoneIndex = Convert.ToInt32 (Request.Params["zoneIndex"]);
                int instanceId = Convert.ToInt32 (Request.Params["instanceId"]);
                int zone_index_plain = Convert.ToInt32 (Request.Params["zone_index_plain"]);

                string actionreq = "GetTemplateItems";
                
                if (Request.Params["action"] != null)
                {
                    actionreq = Request.Params["action"];
                }

                if (actionreq.Equals("GetTemplateItems"))
                {
                    int userId = KanevaWebGlobals.CurrentUser.UserId;

                    List<DeedItem> allItems = GetShoppingFacade.GetInitialDeedItemList(userId, zoneIndex, instanceId);
                    bool frameworkEnabled = GetGameFacade.IsFrameworkEnabled(instanceId, (new ZoneIndex(zoneIndex)).ZoneType);
                    DeedPrice bp = GetShoppingFacade.CalculateInitialDeedPrice(allItems, 0, (uint)userId, frameworkEnabled);

                    XmlDocument doc = new XmlDocument();
                    XmlNode root = doc.CreateElement("Result");

                    XmlElement elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = "0";
                    root.AppendChild(elem);
                        
                    elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = "success";
                    root.AppendChild(elem);

                    elem = doc.CreateElement("DeedPrice");
                    elem.InnerText = bp.TotalDeed.ToString();
                    root.AppendChild(elem);

                    elem = doc.CreateElement("IncludedItemCount");
                    elem.InnerText = allItems.Count.ToString();
                    root.AppendChild(elem);

                    if (allItems.Count > 0)
                    {
                        elem = doc.CreateElement("DeedItems");

                        foreach (DeedItem deedItem in allItems)
                        {
                            elem.AppendChild(BuildDeedItemXML(doc, deedItem));
                        }

                        root.AppendChild(elem);
                    }

                    Response.Write(root.OuterXml);
                }

                else if (actionreq.Equals("DeedInfo"))
                {
                    List<DeedItem> deedItems = GetShoppingFacade.GetInitialDeedItemList(KanevaWebGlobals.CurrentUser.UserId, zoneIndex, instanceId);
                    int MaxObjectCount = (IsAdministrator() ? Int32.MaxValue : Configuration.MaxObjectsAllowedInZone);

                    Response.Write("<Result><ReturnCode>0<ReturnCode/><ReturnDescription>success</ReturnDescription><ZoneObjectCount>" + deedItems.Count + "</ZoneObjectCount><MaxObjectCount>" + MaxObjectCount + "</MaxObjectCount></Result>");

                }

            }
            catch (Exception exc)
            {
                m_logger.Warn("Error getting deed info or items: " + exc);
                Response.Write("<Result>Error getting deed info or items</Result>");
            }


            
        }

        private XmlElement BuildDeedItemXML(XmlDocument doc, DeedItem deedItem)
        {
            XmlElement diElem = doc.CreateElement("DeedItem");

            XmlElement elem = doc.CreateElement("Name");
            elem.InnerText = deedItem.DisplayName;
            diElem.AppendChild(elem);

            elem = doc.CreateElement("Quantity");
            elem.InnerText = deedItem.Quantity.ToString();
            diElem.AppendChild(elem);

            elem = doc.CreateElement("DeedItemPrice");
            elem.InnerText = deedItem.DeedItemPrice.ToString();
            diElem.AppendChild(elem);

            elem = doc.CreateElement("UseType");
            elem.InnerText = deedItem.UseType.ToString();
            diElem.AppendChild(elem);

            if (deedItem.BundleItems.Count > 0)
            {
                elem = doc.CreateElement("BundledItems");
                
                foreach(DeedItem bundledItem in deedItem.BundleItems)
                {
                    elem.AppendChild(BuildDeedItemXML(doc, bundledItem));
                }

                diElem.AppendChild(elem);
            }

            return diElem;
        }

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    }

    
}