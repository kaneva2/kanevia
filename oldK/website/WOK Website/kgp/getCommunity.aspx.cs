///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class getCommunity : KgpBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string action = "";

            if (CheckUserId(false))
            {
                if (Request.Params["action"] != null)
                {
                    action = Request.Params["action"].ToString();

                    if (action.Equals("getCommunityByNames"))
                    {
                        if (Request.Params["cNames"] != null)
                        {
                            PagedDataTable dtComms = (PagedDataTable)GetCommunityByNames(Request.Params["cNames"].ToString());
                            Response.Write(BuildXMLResponse(dtComms, "Result", "Communities"));
                        }
                    }
                    else if (action.Equals("getCommunityByIds"))
                    {
                        if (Request.Params["cIds"] != null)
                        {
                            PagedDataTable dtComms = (PagedDataTable)GetCommunityByIds(Request.Params["cIds"].ToString());
                            Response.Write(BuildXMLResponse(dtComms, "Result", "Communities"));
                        }
                    }
                }
            
            }
        }

        private PagedDataTable GetCommunityByNames(string comNames)
        {
            Hashtable parameters = new Hashtable();

            char[] splitter = { ',' };
            string[] arCommunities = null;
            string sqlCommParams = "";

            // Did they enter multiples?
            arCommunities = comNames.Split(splitter);

            if (arCommunities.Length > 1)
            {
                for (int j = 0; j < arCommunities.Length; j++)
                {
                    if (j < arCommunities.Length - 1)
                    {
                        sqlCommParams += "@communityName" + j.ToString () + ",";
                        parameters.Add("@communityName" + j.ToString(), arCommunities[j].ToString());
                    }
                    else
                    {
                        sqlCommParams += "@communityName" + j.ToString() + "";
                        parameters.Add("@communityName" + j.ToString(), arCommunities[j].ToString());
                    }
                }
            }
            else
            {
                sqlCommParams = "@communityName";
                parameters.Add("@communityName", comNames);
            }

            string sqlString = " SELECT community_id, name, thumbnail_small_path " +
                " FROM kaneva.communities c " +
                " WHERE c.name IN (" + sqlCommParams + ") " +
                " AND c.is_personal = 0 ";
                ;
            return KanevaGlobals.GetDatabaseUtilityReadOnly ().GetPagedDataTableUnion (sqlString, "", parameters, 1, 100 );
        }

        private PagedDataTable GetCommunityByIds(string comIds)
        {
            Hashtable parameters = new Hashtable();

            char[] splitter = { ',' };
            string[] arCommunities = null;
            string sqlCommParams = "";

            // Did they enter multiples?
            arCommunities = comIds.Split(splitter);

            if (arCommunities.Length > 1)
            {
                for (int j = 0; j < arCommunities.Length; j++)
                {
                    if (j < arCommunities.Length - 1)
                    {
                        sqlCommParams += "@communityName" + j.ToString() + ",";
                        parameters.Add("@communityName" + j.ToString(), Convert.ToInt32 (arCommunities[j]));
                    }
                    else
                    {
                        sqlCommParams += "@communityName" + j.ToString() + "";
                        parameters.Add("@communityName" + j.ToString(), Convert.ToInt32(arCommunities[j]));
                    }
                }
            }
            else
            {
                sqlCommParams = "@communityName";
                parameters.Add("@communityName", comIds);
            }

            string sqlString = " SELECT community_id, name, thumbnail_small_path " +
                " FROM kaneva.communities c " +
                " WHERE c.community_id IN (" + sqlCommParams + ") " +
                " AND c.is_personal = 0 ";
            ;
            return KanevaGlobals.GetDatabaseUtilityReadOnly().GetPagedDataTableUnion(sqlString, "", parameters, 1, 100);
        }

        /// <summary>
        /// BuildXMLResponse
        /// </summary>
        private string BuildXMLResponse(PagedDataTable pdt, string dataSetName, string tableName)
        {
            DataSet ds = new DataSet();
            ds.DataSetName = dataSetName;

            pdt.TableName = tableName;
            ds.Tables.Add(pdt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            int numRecords = pdt.Rows.Count;
            XmlElement elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = pdt.TotalCount;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            return root.OuterXml;
        }

    }
}