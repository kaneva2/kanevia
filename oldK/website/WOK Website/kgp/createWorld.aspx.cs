///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

using log4net;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class createWorld : KgpBasePage
    {

        #region Declarations

        private const UInt32 ERR_PARSING_FAILED = 0xFFFFFFFF;

        private const string GENERIC_ERROR = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>Internal error</ReturnDescription>\r\n</Result>";

        private string returnXML = "<Result>\r\n  <ReturnCode>{0}</ReturnCode>\r\n  <ReturnDescription>{1}</ReturnDescription>\r\n   <STPURL>{2}</STPURL>\r\n</Result>";
        private string returnXMLAsync = "<Result>\r\n  <ReturnCode>{0}</ReturnCode>\r\n  <ReturnDescription>{1}</ReturnDescription>\r\n   <token>{2}</token>\r\n</Result>";

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Possible return codes
        //   0  "Success"
        //  -1, "Invalid arguments"
        //  -2, "You must first validate your email to create a World"
        //  -3, "VIP Users are restricted to a maximum of [n] Worlds"
        //  -4, "Non-VIP Users are restricted to a maximum of [n] Worlds"
        //  -5, "World <originalName>worldName</originalName> already exists, please change the name. We recommend <recommendedName>worldName</recommendedName>"
        //  -6, "World host server busy, please try again later"
        //  -7, Constants.VALIDATION_REGEX_CHANNEL_NAME_ERROR_MESSAGE // Name should be at least four characters and can contain only letters, numbers, spaces, dashes or underscore

        //  -8, Waiting on async.
        //  -9, Service error.

        // -10, "Error creating World" --> Actual[Error returned from IsTaskCompleted]
        // -11, "Error creating World" --> Actual[3Dapp could not be found]
        // -12, "Error creating World" --> Actual[Unable to get response from 3dapp host server"]
        // -13, "Error creating World" --> Actual[Error connecting to 3DApp host server]
        // -14, "Error creating World" --> Actual[Bad response from 3dapp host server]
        // -15, "Error creating World" --> Actual[Error returned from App Create]

        // -20  "Internal error"
        // Possible error codes


        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(false))
            {
                string action = Request.Params["action"] ?? "";
                switch (action)
                {
                    case "checkCreationStatus":
                        ProcessCheckCreationStatus();
                        break;
                    default:
                        ProcessCreateWorldAction();
                        break;
                }
            }
        }

        private void ProcessCreateWorldAction()
        {
            // vaildate url params
            if (Request.Params["worldname"] == null || Request.Params["worlddesc"] == null || Request.Params["templateid"] == null)
            {
                Response.Write(BuildXMLResponse(-1, "Invalid arguments"));
                return;
            }

            string worldName = Request.Params["worldname"].ToString().Trim();
            string worldDesc = Request.Params["worlddesc"].ToString().Trim();

            WorldTemplate template = null;
            try
            {
                template = GetGameFacade.GetWorldTemplate(Convert.ToInt32(Request.Params["templateid"]), true);

                if (template.TemplateId > 0)
                {
                    // If this is a child zone then parent id should have been passed in
                    int intOut = 0;
                    int parentId = Int32.TryParse(Request.Params["parentid"], out intOut) ? intOut : 0;

                    // Branch on feature toggle. If the client requests async creation and the feature is enabled, allow it.
                    // Otherwise, return the synchronous result as normal.
                    UserExperimentGroup userExperimentGroup = GetExperimentFacade.GetUserExperimentGroupCommon(m_userId, "376e2bf4-9167-11e6-84f2-a3cf223dc93b");
                    if (!string.IsNullOrWhiteSpace(Request.Params["async"])
                        && userExperimentGroup != null
                        && userExperimentGroup.GroupId.Equals("6bf3b789-9167-11e6-84f2-a3cf223dc93b"))
                        CreateWorldAsync(worldName, worldDesc, template.TemplateId, parentId);
                    else
                        CreateWorld(worldName, worldDesc, template, parentId);
                }
                else
                {
                    Response.Write(BuildXMLResponse(-1, "Invalid arguments"));
                }
            }
            catch (Exception ex)
            {
                Response.Write(BuildXMLResponse(-1, "Invalid arguments"));
            }
        }

        private void CreateWorld(string worldName, string worldDesc, WorldTemplate template, int parentId)
        {
            int errorCode = 0;
            string errorMsg = string.Empty;
            string title = Server.HtmlEncode(worldName.Trim());
            string urlSafeTitle = Server.UrlEncode(title);
            string description = Server.HtmlEncode(worldDesc.Trim());
            string urlSafeDescription = Server.UrlEncode(description);

            int communityId = GetGameFacade.CreateWorldFromTemplate(template, title, urlSafeTitle, description, urlSafeDescription, KanevaWebGlobals.CurrentUser,
                IsAdministrator(), UsersUtility.IsUserBetatester(), KanevaGlobals.WokGameName, out errorCode, out errorMsg);

            if (communityId > 0)
            {
                Community community = GetCommunityFacade.GetCommunity(communityId);

                // Set parent id if this is a child zone
                if (parentId > 0)
                {
                    GetCommunityFacade.UpdateCommunityParentId(community.CommunityId, parentId);
                }

                // Build the world url
                string stpUrl = StpUrl.MakeUrlPrefix(KanevaGlobals.WokGameId.ToString());

                if (community.WOK3App.GameId > 0)
                {
                    stpUrl = StpUrl.MakeUrlPrefix(community.WOK3App.GameId.ToString());
                }
                else
                {
                    stpUrl += "/" + StpUrl.MakeCommunityUrlPath(community.Name);
                }

                // return success
                Response.Write(BuildXMLResponse(0, "Success", stpUrl));
            }
            else
            {
                Response.Write(BuildXMLResponse(errorCode, errorMsg));
            }
        }

        /// <summary>
        /// Creates a new world asynchronously, returning a token that can be used to check completion.
        /// </summary>
        private void CreateWorldAsync(string worldName, string worldDesc, int templateId, int parentId)
        {
            string title = Server.HtmlEncode(worldName.Trim());
            string description = Server.HtmlEncode(worldDesc.Trim());

            string token = GetGameFacade.CreateWorldFromTemplateAsync(m_userId, title, description, templateId, parentId, Global.RequestRabbitMQChannel());

            if (!string.IsNullOrWhiteSpace(token))
            {
                // return success
                Response.Write(BuildAsyncXMLResponse(0, "Success", token));
            }
            else
            {
                Response.Write(BuildAsyncXMLResponse(-1, "Failed to create world.", token));
            }
        }

        private void ProcessCheckCreationStatus()
        {
            // vaildate url params
            if (Request.Params["token"] == null)
            {
                Response.Write(BuildXMLResponse(-1, "Invalid arguments"));
                return;
            }

            var results = (new Common()).CheckAsyncTaskCompleteKey(Request.Params["token"]);
            if (string.IsNullOrWhiteSpace(results.TaskKey))
            {
                Response.Write(BuildXMLResponse(-8, "Still waiting..."));
                return;
            }

            try
            {
                if (results.Status.Equals("Success"))
                {
                    Response.Write(BuildXMLResponse(0, "Success", results.Message));
                }
                else
                {
                    Response.Write(BuildXMLResponse(-9, results.Message));
                }
            }
            catch (Exception)
            {
                Response.Write(BuildXMLResponse(-9, "Unable to process status token."));
            }
        }

        /// <summary>
        /// BuildXMLResponse
        /// </summary>
        private string BuildXMLResponse(int returnCode, string returnDescription, string worldURL = "")
        {
            string retval = GENERIC_ERROR;

            try
            {
                retval = string.Format(returnXML, returnCode, returnDescription, worldURL);
            }
            catch (Exception ex)
            {
                m_logger.Error("BuildXMLResponse unexpected error " + ex);
            }

            return retval;
        }

        private string BuildAsyncXMLResponse(int returnCode, string returnDescription, string token)
        {
            string retval = GENERIC_ERROR;

            try
            {
                retval = string.Format(returnXMLAsync, returnCode, returnDescription, token);
            }
            catch (Exception ex)
            {
                m_logger.Error("BuildAsyncXMLResponse unexpected error " + ex);
            }

            return retval;
        }



        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
