///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for friendsList.
	/// </summary>
	public class friendsList : KgpBasePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
				if ( (Request.Params["start"] == null) || (Request.Params["max"] == null) )
				{
					string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>page number and items per page not specified</ResultDescription>\r\n</Result>";
					Response.Write(errorStr);
					return;
				}

				int page = Int32.Parse(Request.Params["start"].ToString());
				int items_per_page = Int32.Parse(Request.Params["max"].ToString());
				int userId = 0;

				if( Request.Params["userId"] != null)
				{
					userId = Int32.Parse(Request.Params["userId"].ToString());
				}

				string friendName = Request.Params["search"];
				string onlineOnly = Request.Params["online"];
				string sortLogon = Request.Params["sortbylogon"];
				string sortRaves = Request.Params["sortbyraves"];
				string mustHaveThumbnailImage = Request.Params["mustHaveThumbnail"];
				bool bonline = false;
				bool bsortbylastlogon = false;
				bool bsortbyraves = false;
				bool bMustHaveThumbnailImage = false;

				if ( onlineOnly != null && onlineOnly.Equals("T") )
				{
					bonline = true;
				}

				if ( mustHaveThumbnailImage != null && mustHaveThumbnailImage.Equals("T") )
				{
					bMustHaveThumbnailImage = true;
				}

				if ( sortLogon != null && sortLogon.Equals("T") )
				{
					bsortbylastlogon = true;
				}
				else if ( sortRaves != null && sortRaves.Equals("T") )
				{
					bsortbyraves = true;
				}

				DataSet ds = new DataSet();

				ds.DataSetName = "Result";

				PagedDataTable pdt;

				if ( userId == 0)
				{
					userId = m_userId;
				}

				pdt = UsersUtility.GetFriends (userId, "", page, items_per_page, friendName, bonline, bsortbylastlogon, bsortbyraves, bMustHaveThumbnailImage);
				pdt.TableName = "Friend";
				ds.Tables.Add(pdt);

				XmlDocument doc = new XmlDocument();
				doc.LoadXml(ds.GetXml());
				XmlNode root = doc.DocumentElement;

				int numRecords = pdt.Rows.Count;
				XmlElement elem = doc.CreateElement("NumberRecords");
				elem.InnerText= numRecords.ToString();

				root.InsertBefore( elem, root.FirstChild);

				int totalNumRecords = pdt.TotalCount;
				elem = doc.CreateElement("TotalNumberRecords");
				elem.InnerText= totalNumRecords.ToString();

				root.InsertBefore( elem, root.FirstChild);

				elem = doc.CreateElement("ReturnDescription");
				elem.InnerText="success";

				root.InsertBefore( elem, root.FirstChild);

				elem = doc.CreateElement("ReturnCode");
				elem.InnerText="0";

				root.InsertBefore( elem, root.FirstChild);

				Response.Write(root.OuterXml);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
