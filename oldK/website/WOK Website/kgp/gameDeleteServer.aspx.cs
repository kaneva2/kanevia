///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class gameDeleteServer : KgpBasePage
    {
        const string RESP_OK = "0";
        const string RESP_BAD_PARAM = "1";
        const string RESP_NOT_AUTHORIZED = "3";
        const string RESP_NOT_VALIDATED = "4";

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
        {
            String resp = "<Result><ReturnCode>";
            resp += RESP_BAD_PARAM; // bad param
            resp += "</ReturnCode><ReturnDescription>Bad Parameter</ReturnDescription></Result>";

            bool userValidated = false;
            bool userAuthorized = false;
            int userId = 0;

            Int32 gameId = 0;
            Int32 temp = 0;
            if (Request.Params["gameId"] != null &&
                 Int32.TryParse(Request.Params["gameId"], out temp))
            {
                gameId = temp;
            }

            String hostName = Request.Params["hostName"];

            int port = 0;
            temp = 0;
            if (Request.Params["port"] != null && 
                Int32.TryParse(Request.Params["port"], out temp))
            {
                port = temp;
            }

            //Authenticate and authorize
            if (Request.Params["user"] != null &&
                 Request.Params["pw"] != null && 
                 gameId != 0 && hostName!=null && port!=0)
            {
                int roleMemberShip = 0;
                DataRow dr = UsersUtility.GetUserByEmail(Request.Params["user"]);

                if (dr != null)
                {
                    // if they pass user(email)/pw then authenticate them
                    if (UsersUtility.Authorize(Request.Params["user"], Request["pw"], gameId, ref roleMemberShip, "", false) == (int)Constants.eLOGIN_RESULTS.SUCCESS)
                    {
                        userValidated = true;

                        Game game = GetGameFacade.GetGameByGameId(gameId);

                        if (game != null && game.IsIncubatorHosted && Request.Params["user"] == KanevaGlobals.IncubationOwner)
                        {
                            //Impersonate as the real owner
                            userId = game.OwnerId;
                        }
                        else
                        {
                            userId = Convert.ToInt32(dr["user_id"]);
                        }

                        userAuthorized = userId==game.OwnerId;
                    }
                }

                if (!userValidated)
                {
                    resp = "<Result><ReturnCode>";
                    resp += RESP_NOT_VALIDATED; 
                    resp += "</ReturnCode><ReturnDescription>Not Validated</ReturnDescription></Result>";
                    Response.Write(resp);
                    return;
                }

                if (!userAuthorized)
                {
                    resp = "<Result><ReturnCode>";
                    resp += RESP_NOT_AUTHORIZED; 
                    resp += "</ReturnCode><ReturnDescription>Not authorized or game not found</ReturnDescription></Result>";
                    Response.Write(resp);
                    return;
                }

                IList<GameServer> servers = GetGameFacade.GetServerListByGameId(gameId, "", "");

                int returnCode = 0;
                bool found = false;

                if (servers.Count > 1)
                {
                    //Log warnings if there are more than one server entry.
                    m_logger.Warn("More than one server record found for game " + gameId.ToString());
                }

                //Delete matching entry
                foreach (GameServer server in servers)
                {
                    if (server.ServerName.ToLower() == hostName.ToLower() && server.Port == port)
                    {
                        found = true;

                        try
                        {
                            GetGameFacade.DeleteGameServer(server.ServerId);
                            m_logger.Info("Deleted game server " + server.ServerId.ToString() + ", name=" + server.ServerName + ", port=" + server.Port.ToString());
                        }
                        catch (Exception ex)
                        {
                            m_logger.Error("Error deleting game server " + server.ServerId.ToString() + ": " + ex.ToString());
                            returnCode = -1;
                        }

                        break;
                    }
                }

                if (!found)
                {
                    //Log warnings if no server found for this game
                    m_logger.Warn("No matching server record found: gameId=" + gameId.ToString() + ", hostName=" + hostName + ", port=" + port.ToString());
                }

                String returnDescription;

                switch (returnCode)
                {
                    case 0:
                        returnDescription = "OK";
                        break;
                    case 2:
                        returnDescription = "Key or game id not found";
                        break;
                    case 3:
                        returnDescription = "Not authorized or game not found";
                        break;
                    case 5:
                        returnDescription = "Access denied or user id not found";
                        break;
                    default:
                        returnDescription = "Undefined";
                        break;
                }

                resp = "<Result><ReturnCode>";
                resp += returnCode.ToString();
                resp += "</ReturnCode><ReturnDescription>";
                resp += returnDescription;
                resp += "</ReturnDescription></Result>";
            }

            Response.Write(resp);
        }



        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
