///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for Fame.
    /// </summary>
    public class AppInfo : KgpBasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            string kanevaDb = KanevaGlobals.DbNameKaneva;

            if (IsPostBack)
            {
                return;
            }

            if (CheckUserId(true)) // true = can be logged on to web also for this request
            {
                if (Request.Params["action"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                string actionreq = Request.Params["action"];
                GameFacade gameFacade = new GameFacade();

                if (actionreq.Equals("GetAppInfo"))
                {
                    int gameId = 0;
                    if (Request["gid"] == null || !Int32.TryParse(Request["gid"], out gameId))
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>No gid supplied</ResultDescription>\r\n</Result>");
                        return;
                    }

                    GameFacade gf = new GameFacade();
                    Game game = gf.GetGameByGameId(gameId);
                    if (game != null)
                    {
                        // Get owner
                        User user = GetUserFacade.GetUser(game.OwnerId);
                        Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Success</ResultDescription>\r\n<Game><Name>" + game.GameName + "</Name>\r\n<Description>" + game.GameDescription + "</Description>\r\n<Thumbnail>" + KanevaGlobals.GameImageServer + "/" + (game.GameImagePath).Replace("\\", "/") + "</Thumbnail>\r\n<Owner>" + user.Username + "</Owner>\r\n</Game>\r\n</Result>");
                        return;
                    }
                    else
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>-2</ReturnCode>\r\n  <ResultDescription>Game not found</ResultDescription>\r\n</Result>");
                        return;
                    }
                }
                if (actionreq.Equals("GetBadgeAwards"))
                {
                    if ((Request.Params["userId"] == null) || (Request.Params["achievementId"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId or achievementId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int userId = Int32.Parse(Request.Params["userId"].ToString());
                    uint achievementId = uint.Parse(Request.Params["achievementId"].ToString());

                    XmlDocument doc = new XmlDocument();
                    XmlElement rootNode = doc.CreateElement("Result");
                    doc.AppendChild(rootNode);

                    XmlElement summaryNode = doc.CreateElement("Summary");
                    rootNode.AppendChild(summaryNode);

                    summaryNode.AppendChild(GetBadgesAwardsNode(doc, achievementId, userId));

                    XmlNode root = doc.DocumentElement;

                    XmlElement elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = "success";

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = "0";

                    root.InsertBefore(elem, root.FirstChild);

                    Response.Write(root.OuterXml);

                }
                if (actionreq.Equals("GetBadges"))
                {
                    if ((Request.Params["userId"] == null) || (Request.Params["communityId"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId or communityId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    int userId = Int32.Parse(Request.Params["userId"].ToString());
                    int communityId = Int32.Parse(Request.Params["communityId"].ToString());

                    XmlDocument doc = new XmlDocument();
                    XmlElement rootNode = doc.CreateElement("Result");
                    doc.AppendChild(rootNode);

                    XmlElement summaryNode = doc.CreateElement("Summary");
                    rootNode.AppendChild(summaryNode);

                    summaryNode.AppendChild(GetBadgesNode(doc, communityId, userId));

                    XmlNode root = doc.DocumentElement;

                    XmlElement elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = "success";

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = "0";

                    root.InsertBefore(elem, root.FirstChild);

                    Response.Write(root.OuterXml);


                }

                if (actionreq.Equals("GetVisited3DApps"))
                {
                    if ((Request.Params["userId"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    int userId = Int32.Parse(Request.Params["userId"].ToString());

                    XmlDocument doc = new XmlDocument();
                    XmlElement rootNode = doc.CreateElement("Result");
                    doc.AppendChild(rootNode);

                    XmlElement summaryNode = doc.CreateElement("Summary");
                    rootNode.AppendChild(summaryNode);

                    // Get Summary Data
                    Dictionary<string, int> dSum = GetGameFacade.GetAchievementsSummaryForAllApps(userId);

                    if (dSum.Count > 0)
                    {
                        int val = 0;

                        // Get users total points 
                        if (dSum.TryGetValue("UserTotalPoints", out val))
                        {
                            summaryNode.SetAttribute("UserTotalPoints", val.ToString());
                        }

                        // Get total points of all achievements
                        if (dSum.TryGetValue("TotalPoints", out val))
                        {
                            summaryNode.SetAttribute("TotalPoints", val.ToString());
                        }

                        // Get users total number of achievements
                        if (dSum.TryGetValue("UserTotalAchievements", out val))
                        {
                            summaryNode.SetAttribute("UserTotalAchievements", val.ToString());
                        }

                        // Get total number of achievements for 3D App
                        if (dSum.TryGetValue("TotalAchievements", out val))
                        {
                            summaryNode.SetAttribute("TotalAchievements", val.ToString());
                        }
                    }
                    else
                    {
                        summaryNode.SetAttribute("UserTotalPoints", "0");
                        summaryNode.SetAttribute("TotalPoints", "0");
                        summaryNode.SetAttribute("UserTotalAchievements", "0");
                        summaryNode.SetAttribute("TotalAchievements", "0");
                    }

                    // Get a list of apps...
                    PagedList<Community> communities = GetGameFacade.GetGamesInWhichUserHasFame(userId, page, items_per_page, "name");

                    XmlElement appsNode = doc.CreateElement("Apps");
                    rootNode.AppendChild(appsNode);

                    foreach (Community community in communities)
                    {
                        // Add the packet node
                        appsNode.AppendChild(GetAppNode(doc, userId, community));
                    }

                    XmlNode root = doc.DocumentElement;

                    XmlElement elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = "success";

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = "0";

                    root.InsertBefore(elem, root.FirstChild);

                    Response.Write(root.OuterXml);
                    //}
                    //else
                    //{
                    //    string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>App info not found</ResultDescription>\r\n</Result>";
                    //    Response.Write(errorStr);
                    //    return;
                    //}

                }

                if (actionreq.Equals("GetAppInfo"))
                {
                    int gameId = Int32.Parse(Request.Params["gameId"].ToString());
                    string accountId = "";

                    GameFacade gf = new GameFacade();
                    Game game = gf.GetGameByGameId(gameId);

                    if (game != null)
                    {
                        accountId = game.GAAccountID;
                    }

                    XmlDocument doc = new XmlDocument();
                    XmlElement rootNode = doc.CreateElement("Result");
                    doc.AppendChild(rootNode);

                    XmlNode root = doc.DocumentElement;

                    XmlElement elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = "success";

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = "0";

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("game_id");
                    elem.InnerText = gameId.ToString();

                    root.InsertAfter(elem, root.LastChild);

                    elem = doc.CreateElement("ga_account");
                    elem.InnerText = accountId;

                    root.InsertAfter(elem, root.LastChild);

                    Response.Write(root.OuterXml);
                }

            }

        }

        private XmlNode GetAppNode(XmlDocument doc, int userId, Community community)
        {
            GameFacade gameFacade = new GameFacade();

            // Add packet info
            XmlElement appNode = doc.CreateElement("App");

            appNode.SetAttribute("Name", community.Name);
            appNode.SetAttribute("Thumbnail", community.ThumbnailSmallPath);
            appNode.SetAttribute("Owner", community.CreatorUsername);
            appNode.SetAttribute("GameId", gameFacade.GetGameIdByCommunityId(community.CommunityId).ToString());
            appNode.SetAttribute("CommunityId", community.CommunityId.ToString());

            // Badges
            appNode.AppendChild(GetBadgesNode(doc, community.CommunityId, userId));

            // Leaderboard
            Dictionary<string, int> DictSummary = gameFacade.GetAchievementsSummaryForApp(community.CommunityId, userId);

            if (DictSummary.Count > 0)
            {
                int val = 0;

                // Get users user achievement count 
                if (DictSummary.TryGetValue("UserTotalPoints", out val))
                {
                    appNode.SetAttribute("UserTotalPoints", val.ToString());
                }
                // Get users total achievement count 
                if (DictSummary.TryGetValue("TotalPoints", out val))
                {
                    appNode.SetAttribute("TotalPoints", val.ToString());
                }
                if (DictSummary.TryGetValue("UserTotalAchievements", out val))
                {
                    appNode.SetAttribute("UserTotalAchievements", val.ToString());
                }
                if (DictSummary.TryGetValue("TotalAchievements", out val))
                {
                    appNode.SetAttribute("TotalAchievements", val.ToString());
                }
            }
            else
            {
                appNode.SetAttribute("UserTotalPoints", "0");
                appNode.SetAttribute("TotalPoints", "0");
                appNode.SetAttribute("UserTotalAchievements", "0");
                appNode.SetAttribute("TotalAchievements", "0");
            }

            return appNode;
        }

        private XmlNode GetBadgesAwardsNode(XmlDocument doc, uint achievementId, int userId)
        {
            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

            List<AchievementAward> achievementAwards = GetGameFacade.GetAchievementAwards(achievementId, user.Gender);

            XmlElement badgeNodes = doc.CreateElement("BadgeAwards");

            if (achievementAwards.Count > 0)
            {
                foreach (AchievementAward aa in achievementAwards)
                {
                    XmlElement badgeAwardNode = doc.CreateElement("BadgeAward");
                    badgeNodes.AppendChild(badgeAwardNode);

                    // Add the award node
                    badgeAwardNode.SetAttribute("BadgeAwardId", aa.AchievementAwardId.ToString());
                    badgeAwardNode.SetAttribute("Name", aa.Name);
                    badgeAwardNode.SetAttribute("GlobalId", aa.GlobalId.ToString());
                    badgeAwardNode.SetAttribute("AnimationId", aa.AnimationId.ToString());
                    badgeAwardNode.SetAttribute("Gender", aa.Gender);
                    badgeAwardNode.SetAttribute("Quantity", aa.Quantity.ToString());
                    badgeAwardNode.SetAttribute("NewTitleName", aa.NewTitleName);
                    badgeAwardNode.SetAttribute("Rewards", aa.Rewards.ToString());
                }
            }

            return badgeNodes;
        }

        /// <summary>
        /// Get Badges
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="communityId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private XmlNode GetBadgesNode(XmlDocument doc, int communityId, int userId)
        {
            PagedList<UserAchievementData> uads = GetGameFacade.GetUserAchievementsForApp(communityId, userId, "created_date DESC", 1, 100);

            XmlElement badgeNodes = doc.CreateElement("Badges");

            if (uads.Count > 0)
            {
                foreach (UserAchievementData uad in uads)
                {
                    XmlElement badgeNode = doc.CreateElement("Badge");
                    badgeNodes.AppendChild(badgeNode);

                    // Add the packet node
                    badgeNode.SetAttribute("BadgeId", uad.AchievementsId.ToString());
                    badgeNode.SetAttribute("CommunityId", uad.CommunityId.ToString());
                    badgeNode.SetAttribute("Name", uad.Name);
                    badgeNode.SetAttribute("Description", uad.Description);
                    badgeNode.SetAttribute("Points", uad.Points.ToString());
                    badgeNode.SetAttribute("ImageUrl", uad.ImageUrl);

                    if (uad.CreatedDate.Equals(DateTime.MinValue))
                    {
                        badgeNode.SetAttribute("Completed", "0");
                    }
                    else
                    {
                        badgeNode.SetAttribute("Completed", "1");
                    }

                    // Get the badge award and add to badge info
                    List<AchievementAward> achievementAwards = GetGameFacade.GetAchievementAwards(uad.AchievementsId, KanevaWebGlobals.CurrentUser.Gender);

                    // A of this release 12/8/15 we only support a reward amount and 1 title associated with
                    // a badge. If, in the future, support for multiple titles, etc, is needed then we will need
                    // to update the code here and on the client side to handle that. 
                    if (achievementAwards.Count > 0)
                    {
                        string gameBadges_ExperimentId = "9bb4ed18-6623-11e5-96ff-002219919c2a";
                        string gameBadges_GroupC = "3312c53b-6624-11e5-96ff-002219919c2a";
                        foreach (AchievementAward aa in achievementAwards)
                        {
                            UserExperimentGroup userExperimentGroup = GetExperimentFacade.GetUserExperimentGroupCommon(KanevaWebGlobals.CurrentUser.UserId, gameBadges_ExperimentId);
                            if (userExperimentGroup != null)
                            {
                                // Only Group C should get & see rewards
                                if (userExperimentGroup.GroupId == gameBadges_GroupC)
                                {
                                    badgeNode.SetAttribute("Rewards", aa.Rewards.ToString());
                                }
                                else
                                {
                                    badgeNode.SetAttribute("Rewards", "0");
                                }
                            }
                            else
                            {
                                badgeNode.SetAttribute("Rewards", "0");
                            }

                            badgeNode.SetAttribute("NewTitleName", aa.NewTitleName);
                        }
                    }
                }
            }

            return badgeNodes;
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
