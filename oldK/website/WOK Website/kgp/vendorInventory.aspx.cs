///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.kgp
{
	 public enum GetWokUserVendorInventorySorts
	 {
		 alfa = 1, 
		 beta = 2,
		 gamma = 3
	 }

	/// <summary>
	/// Summary description for vendorInventory.
	/// </summary>
	public class vendorInventory : KgpBasePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
				if (Request.Params["storeId"] == null)
				{
					string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>store id not specified</ResultDescription>\r\n</Result>";
					Response.Write(errorStr);
					return;
				}

				int storeId = Int32.Parse(Request.Params["storeId"].ToString());
				DataSet ds = new DataSet();

				ds.DataSetName = "Result";

				DataTable dt;

                dt = WOKStoreUtility.GetStoreContents( storeId);

				dt.TableName = "StoreInventory";
				ds.Tables.Add(dt);

				XmlDocument doc = new XmlDocument();
				doc.LoadXml(ds.GetXml());
				XmlNode root = doc.DocumentElement;

				int numRecords = dt.Rows.Count;
				XmlElement elem = doc.CreateElement("NumberRecords");
				elem.InnerText= numRecords.ToString();

				root.InsertBefore( elem, root.FirstChild);

				elem = doc.CreateElement("ReturnDescription");
				elem.InnerText="success";

				root.InsertBefore( elem, root.FirstChild);

				elem = doc.CreateElement("ReturnCode");
				elem.InnerText="0";

				root.InsertBefore( elem, root.FirstChild);

				Response.Write(root.OuterXml);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
