///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for serverlist.
	/// </summary>
    public class serverlist : KgpBasePage
	{
        private string m_defaultGameExe = null;

        // 4/08 moved from Kaneva project
		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( Request["id"] != null)
			{
				int gameId = Convert.ToInt32 (Request ["id"]);
                int patchVersion = 1;
                int implVersion = 0;       // Impl version is 0 if v parameter not available

				// only do commercial
                string filter = " gl.license_subscription_id = 3 "; // +(int)Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL;

                if (Request["v"] != null)
                {
                    try
                    {
                        // If v=T is passed in, it will throw an exception. For newer version of client/patcher, actual 'v' value will be used.
                        implVersion = Convert.ToInt32(Request["v"]);
                    }
                    catch (Exception) 
                    {
                        // Impl version is 1 if v=T
                        implVersion = 1;
                    }

                    if (Request["ver"] != null)
                    {
                        try
                        {
                            patchVersion = Convert.ToInt32(Request["ver"]);
                        }
                        catch (Exception) { }
                    }
                }

                if (implVersion <= 1)
                {
                    // v=T, v=1 or no v parameter
                    m_defaultGameExe = "KEPClient.exe";
                }
                else
                {
                    // v=2 or higher
                    m_defaultGameExe = "KanevaTray.exe";
                }

                DataTable dt = ServerUtility.GetGameServers(gameId, patchVersion, filter);

                if (dt.Rows.Count == 0)
                {
                    //Workarounds for incubator hosted apps
                    Game game = GetGameFacade.GetGameByGameId(gameId);
                    if (game!=null && game.IsIncubatorHosted)
                    {
                        if (HandleInactiveIncubatorHostedApp(game))
                        {
                            return;
                        }
                    }
                }

				int server_id = 0;
				string serverName = null;
				string gameName = null;
				string ip_address = null;
				int port = 0;
				string game_exe = null;
				string patch_url = null;
				int num_players = 0;
				int max_players = 0;
				int server_status = 0;
                string patch_type;
                string client_path;

                bool newPatchUrlsFound = false;
				for ( int ii = 0; ii < dt.Rows.Count; ii++)
				{
					server_id = Convert.ToInt32(dt.Rows[ii]["server_id"]);
					serverName = (string) dt.Rows[ii]["server_name"];
					gameName = (string) dt.Rows[ii]["game_name"];
					ip_address = (string) dt.Rows[ii]["ip_address"];
					port = Convert.ToInt32(dt.Rows[ii]["port"]);
					patch_url = (string) dt.Rows[ii]["patch_url"];
					num_players = Convert.ToInt32(dt.Rows[ii]["number_of_players"]);
					max_players = Convert.ToInt32(dt.Rows[ii]["max_players"]);
					server_status = Convert.ToInt32(dt.Rows[ii]["server_status_id"]);
                    patch_type = (string)dt.Rows[ii]["patch_type"];
                    client_path = (string)dt.Rows[ii]["client_path"];

                    // Return either only new patch URLs or old ones if there is no new URLs.
                    // The resultset is a join between servers and patch URLs, ordered by patch_type DESC.
                    if (patch_type != "")
                    {
                        // New patch URL with correct patch_type
                        newPatchUrlsFound = true;
                    }
                    else if (newPatchUrlsFound)
                    {
                        // New patch URLs already fetched, skip all old URLs
                        break;
                    }

                    if (Convert.IsDBNull(dt.Rows[ii]["game_exe"]))
                    {
                        //game_exe not defined in DB, use hard-coded ones as legacy impl
                        game_exe = m_defaultGameExe;
                    }
                    else
                    {
                        game_exe = (string)dt.Rows[ii]["game_exe"];
                    }

                    Response.Write(server_id + ";" + serverName + ";" + gameName + ";" + ip_address + ";" + port + ";" + game_exe + ";" + patch_url + ";" + num_players + ";" + max_players + ";" + server_status + ";" + patch_type + ";" + client_path + "\r\n");
				}
			}
		}

        //Workarounds for incubator hosted apps
        private bool HandleInactiveIncubatorHostedApp(Game game)
        {
            DataTable dtServers = GetGameFacade.GetServerByGameId(game.GameId, "", "");
            DataTable dtPatchUrls = GetGameFacade.GetPatchURLsByGameId(game.GameId, "", "patch_type DESC");

            if (dtServers == null || dtPatchUrls == null || dtServers.Rows.Count==0 || dtPatchUrls.Rows.Count==0)
            {
                return false;
            }

			int server_id = 0;
			string serverName = null;
			string gameName = null;
			string ip_address = null;
			int port = 0;
			string game_exe = null;
			string patch_url = null;
			int num_players = 0;
			int max_players = 0;
			int server_status = 0;
            string patch_type;
            string client_path;

            bool newPatchUrlsFound = false;
            for (int ii = 0; ii < dtServers.Rows.Count; ii++)
            {
                server_id = (int)dtServers.Rows[ii]["server_id"];
                serverName = (string)dtServers.Rows[ii]["server_name"];
                gameName = game.GameName;
                ip_address = (string)dtServers.Rows[ii]["ip_address"];
                port = (int)dtServers.Rows[ii]["port"];
                patch_url = (string)dtPatchUrls.Rows[ii]["patch_url"];
                game_exe = m_defaultGameExe;
                patch_type = (string)dtPatchUrls.Rows[ii]["patch_type"];
                client_path = (string)dtPatchUrls.Rows[ii]["client_path"];

                // Return either only new patch URLs or old ones if there is no new URLs.
                // The resultset is a join between servers and patch URLs, ordered by patch_type DESC.
                if (patch_type != "")
                {
                    // New patch URL with correct patch_type
                    newPatchUrlsFound = true;
                }
                else if (newPatchUrlsFound)
                {
                    // New patch URLs already fetched, skip all old URLs
                    break;
                }

                Response.Write(server_id + ";" + serverName + ";" + gameName + ";" + ip_address + ";" + port + ";" + game_exe + ";" + patch_url + ";" + num_players + ";" + max_players + ";" + server_status + ";" + patch_type + ";" + client_path + "\r\n");
            }
            return true;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
