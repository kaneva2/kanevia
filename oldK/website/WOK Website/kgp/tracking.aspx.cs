///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using Kaneva.DataLayer.DataObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class tracking : System.Web.UI.Page
    {
        #region Declarations

        private string _action = null;
        private string _requestId = null;
        private string _requestTypeSentId = null;
        private bool _ingame = false;
        private bool _logincomplete = false;
        private bool _routereq = false;
        private bool _zonestart = false;
        private int? _requestTypeId = null;
        private int? _messageTypeId = null;
        private int? _gameId = null;
        private int? _communityId = null;
        private int? _userId = null;

        private string _errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>{0}</ResultDescription>\r\n</Result>";

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            GetRequestParams();

            switch(_action)
            {
                case "InsertTrackingTypeSent":
                    ProcessInsertTrackingTypeSent();
                    break;
                case "AcceptTrackingRequest":
                    ProcessAcceptTrackingRequest();
                    break;
                default:
                    ProcessInsertTrackingRequest();
                    break;
            }
        }

        /// <summary>
        /// Sets page variables using either passed in values or appropriate defaults
        /// </summary>
        private void GetRequestParams()
        {
            int intOut = 0;
            bool boolOut = false;

            _action = Request.Params["action"];
            _requestId = Request.Params["requestId"];
            _requestTypeSentId = Request.Params["requestTypeSentId"];

            _ingame = Boolean.TryParse(Request.Params["ingame"], out boolOut) ? boolOut : false;
            _logincomplete = Boolean.TryParse(Request.Params["logincomplete"], out boolOut) ? boolOut : false;
            _routereq = Boolean.TryParse(Request.Params["route"], out boolOut) ? boolOut : false;
            _zonestart = Boolean.TryParse(Request.Params["zonestart"], out boolOut) ? boolOut : false;

            _requestTypeId = Int32.TryParse(Request.Params["requestTypeId"], out intOut) ? (int?)intOut : null;
            _messageTypeId = Int32.TryParse(Request.Params["messageTypeId"], out intOut) ? (int?)intOut : null;
            _gameId = Int32.TryParse(Request.Params["gameId"], out intOut) ? (int?)intOut : null;
            _communityId = Int32.TryParse(Request.Params["communityId"], out intOut) ? (int?)intOut : null;
            _userId = Int32.TryParse(Request.Params["userId"], out intOut) ? (int?)intOut : null;
        }

        /// <summary>
        /// Insert a new tracking request.
        /// </summary>
        private void ProcessInsertTrackingRequest()
        {
            UserFacade userFacade = new UserFacade();

            if (_requestTypeId == null)
            {
                Response.Write(string.Format(_errorStr, "requestTypeId not specified"));
                return;
            }

            if (KanevaWebGlobals.CurrentUser.UserId == 0)
            {
                Response.Write(string.Format(_errorStr, "User not logged in."));
                return;
            }

            // Make sure optional values have sensible defaults.
            _gameId = _gameId ?? 0;
            _communityId = _communityId ?? 0;

            string result = userFacade.InsertTrackingRequest((int)_requestTypeId, KanevaWebGlobals.CurrentUser.UserId, (int)_gameId, (int)_communityId);

            Response.Write("<Result><RequestId>" + result + "</RequestId></Result>");
        }

        /// <summary>
        /// Insert a new log of a tracking type sent.
        /// </summary>
        private void ProcessInsertTrackingTypeSent()
        {
            UserFacade userFacade = new UserFacade();

            if (string.IsNullOrWhiteSpace(_requestId) || _messageTypeId == null)
            {
                Response.Write(string.Format(_errorStr, "requestId or messageTypeId not specified"));
                return;
            }

            // Make sure optional values have sensible defaults.
            _userId = _userId ?? 0;

            if (_routereq)
            {
                // Route the request to either an inworld request or to kmail if the receipient is offline
                bool userInWorld = false;
                bool userInKIM = false;

                if (UsersUtility.GetCurrentPlaceInfo((int)_userId) != null)
                {
                    userInWorld = true;
                }
                else
                {
                    // Determine if the user is in KIM
                    userInKIM = UsersUtility.IsUserOnlineInKIM((int)_userId);
                }

                if (userInKIM)
                {
                    // Send them a KIM invite
                    _messageTypeId = 6; // KIM invite
                }
                else if (!userInWorld)
                {
                    // Send them a kmail instead of an inworld invite
                    _messageTypeId = 1; // PM
                }
            }

            string result = userFacade.InsertTrackingTypeSent(_requestId, (int)_messageTypeId, (int)_userId);
            Response.Write("<Result><RequestTypeSentId>" + result + "</RequestTypeSentId></Result>");
        }

        /// <summary>
        /// Mark a tracking request as accepted.
        /// </summary>
        private void ProcessAcceptTrackingRequest()
        {
            UserFacade userFacade = new UserFacade();

            if (string.IsNullOrWhiteSpace(_requestTypeSentId))
            {
                Response.Write(string.Format(_errorStr, "requestTypeSentId not specified"));
                return;
            }

            _userId = _userId ?? KanevaWebGlobals.CurrentUser.UserId;

            int acceptTypeId = 2;
            
            if (_ingame)
            {
                acceptTypeId = 1;
            }

            if (_logincomplete)
            {
                acceptTypeId = 3;
            }

            if (_zonestart)
            {
                acceptTypeId = 4;
            }

            int iResult = userFacade.AcceptTrackingRequest(_requestTypeSentId, (int)_userId, acceptTypeId);
            Response.Write("<Result><Result>" + iResult.ToString() + "</Result></Result>");
        }
    }
}