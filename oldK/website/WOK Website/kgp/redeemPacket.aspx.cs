///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for texturesList.
    /// </summary>
    public class redeemPacket : KgpBasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(false))
            {
                if (Request.Params["action"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                string actionreq = Request.Params["action"];

                if (actionreq.Equals("redeemPacket"))
                {
                    if ((Request.Params["userId"] == null) || (Request.Params["packetId"] == null) || (Request.Params["fameTypeId"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId or packetId or fameTypeId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    FameFacade fameFacade = new FameFacade();
                    int userId = Int32.Parse(Request.Params["userId"].ToString());
                    int fameTypeId = Int32.Parse(Request.Params["fameTypeId"].ToString());

                    string[] strPacketList = Request.Params["packetId"].ToString().Trim().Split(',');
                    Packet[] iPacketList = new Packet[strPacketList.Length];

                    // Do a SQL injection test for the string by converting to Int32, and test each param for valid packets
                    for (int i = 0; i < strPacketList.Length; i++)
                    {
                        try
                        {
                            iPacketList[i] = fameFacade.GetPacket(Convert.ToInt32(strPacketList[i]));
                        }
                        catch (Exception)
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invalid Packet Id</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }

                        // Only support World Fame and Creator at this time
                        if (!(iPacketList[i].FameTypeId.Equals((int)FameTypes.World) || iPacketList[i].FameTypeId.Equals((int)FameTypes.Creator) || iPacketList[i].FameTypeId.Equals((int)FameTypes.CoreHelp) || iPacketList[i].FameTypeId.Equals((int)FameTypes.Builder)))
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Not a World Fame, Creator or Builder packet</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }


                        // Per Jeff P, disallow certain packet (Tour) from client
                        if (iPacketList[i].IsServerOnly)
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Packet not allowed from Client</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    }

                    int ret = 0;

                    // Now redeem the packets
                    foreach (Packet packet in iPacketList)
                    {
                        ret = fameFacade.RedeemPacket(userId, packet.PacketId, packet.FameTypeId);
                    }

                    XmlDocument doc = new XmlDocument();
                    XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");

                    XmlElement elem = doc.CreateElement("ReturnCode");

                    elem.InnerText = ret.ToString();

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = ret.ToString ();

                    root.InsertBefore(elem, root.FirstChild);
                    Response.Write(root.OuterXml);
                }
                else
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action request unknown</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
