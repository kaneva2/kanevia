<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="allowAccess.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.kgp.allowAccess" %>


<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<style type="text/css">
body {
	background:#fff;
	margin: 0px;
	padding: 0px;
	border-style: none;
	font: 12px/16px Verdana, Arial, Helvetica, sans-serif;
}

a:link, a:visited {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color: #018aaa;
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
	color: #018aaa;
	background: transparent;
	cursor:hand;
}

img {border-width:0;border-style:none;}
.info {margin-bottom:12px;clear:both;}
.container {background-color:#ffffff;}
.innerContainer {width:500px;height:300px;border:solid 2px #5594b5;text-align:left;}
.bold {font-weight:bold;}
.name {font-size:13px;}
.imgContainer {float:left;margin:0 15px 0 0;}
.imgContainer img {width:120px;}
.descContainer {float:left;margin:0;}
.desc {margin-top:4px;height:80px;overflow:hidden;width:328px;color:#9c9c9c;}
.dataContainer {padding:6px 16px;}
.learnMore {margin:25px 0 10px 0;text-align:center;}
.titleBar {padding:4px 10px 0px 10px;width:100%;height:26px;background:transparent url('http://images.kaneva.com/header_tile.gif') repeat-x fixed 'center left';}
.title {float:left;color:#fff;}
.closeX {float:right;}
.close {background-image:url('http://images.kaneva.com/close_button_16x16.gif');height:16px;width:16px;}
.disclaimer {color:#9c9c9c;clear:both;margin-top:8px;font-size:11px;}

.btnAllow {background-image:url('http://images.kaneva.com/btn_allow.gif');height:24px;width:71px;}
.buttons {text-align:center;margin-top:35px;padding-left:160px;}
.or {padding:3px 10px;float:left;}
#btnOK {float:left;}
#btnCancel {float:left;padding-top:3px;}
</style>
</head>
<body scroll="no">
    <form id="form1" runat="server">
    <div class="container">
        
        <center>
        <div class="innerContainer">
        <div class="titleBar">
            <div class="title">Access Permission</div> 
            <div class="closeX"><a id="btnClose" href="bogus://close"><span class="close"></span></a></div> 
        </div>
        
        <div class="dataContainer">
            <div class="imgContainer">
                <img id="imgThumbnail" runat="server" border="0"/>
            </div>
            <div class="descContainer">
                <div class="bold name"><asp:Literal ID="litName2" runat="server"></asp:Literal></div>
                <div class="desc"><asp:Literal ID="litDesc" runat="server"></asp:Literal></div>
           </div>
			<div class="learnMore"><a id="aCommunityLink" runat="server" target="_blank"></a> is requesting 
					permission to access the basic profile information that you�ve already shared with everyone.</div>
            <div class="disclaimer" style="display:none;">
                By proceeding, you are allowing <span class="bold"><asp:Literal ID="litName1" runat="server"></asp:Literal></span>
                 access to public information that is provided within your Kaneva profile.  The 3D App
                  and you agree to the Kaneva terms and conditions that govern the use of all Kaneva and 3D Apps features.  This includes but is not limited to the Kaneva Privacy Policy.
            </div>
            <div class="buttons"><a id="btnOK" href="bogus://ok"><span class="btnAllow"></span></a> <div class="or">or</div> <a id="btnCancel" href="bogus://cancel">Cancel</a></div>
        </div>
        
        </div>
        </center>
        
    </div> 
    </form>
</body>
</html>

