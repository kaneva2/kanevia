///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WOK.classes;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;

namespace WOK.kgp
{
    /// <summary>
    ///  A page handler for testing the nascent components of TAZ.  Most if not all of this endpoint will be removed prior to release.
    /// </summary>
    public partial class getSessionToken : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["ExposeWoKWebSessionToken"] != "true")
            {
                Response.Clear();
                return;
            }

            Response.ContentType = "text/xml";
            if (Request.Params["action"] == null )
            {
                SessionTokenProvider.TokenInfo token = Global.tokenProvider().getToken();
                Global.tokenProvider().clearTrace();
//                Response.Clear();
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
//                Response.ContentType = "text/xml";
                Response.Write("<kaneva>\r\n");
                Response.Write("<result token=\"");
                Response.Write(token.token());
                Response.Write("\" expiry=\"");
                Response.Write(token.expiry().ToString());
                Response.Write("\" servedat=\"");
                Response.Write(DateTime.Now.ToString());
                Response.Write("\" />\r\n");
                Response.Write("</kaneva>");
                Response.End();
                return;
            }

            if (Request.Params["action"] == "getconfig")
            {
                Response.Write(Global.tokenProvider().ToString());
                Response.End();
            }

            if (Request.Params["action"] == "reset")
            {
                string addr = Common.GetVisitorIPAddress();
                Global.tokenProvider().reset();
                Response.Write("<p>Token system reset!</p>");
                Response.End();
                return;
            }
        }
    }
}