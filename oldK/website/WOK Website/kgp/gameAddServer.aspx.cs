///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class gameAddServer : KgpBasePage
    {
        const string RESP_BAD_PARAM = "1";
        const string RESP_NOT_VALIDATED = "4";
        const string CLIENT_PATH_WORLDS_ROOT = "GameFiles\\Worlds\\";
        const string CLIENT_PATH_TEMPLATES_ROOT = "GameFiles\\Templates\\";

        protected void Page_Load(object sender, EventArgs e)
        {
            Int32 temp = 0;

            String resp = "<Result><ReturnCode>";
            resp += RESP_BAD_PARAM; // bad param
            resp += "</ReturnCode><ReturnDescription>Bad Parameter</ReturnDescription></Result>";

            bool userValidated = false;
            int userId = 0;

            Int32 version = 0;
            if (Int32.TryParse(Request.Params["version"], out temp))
            {
                version = temp;
            }

            if ( version == 0)
            {
                resp = RESP_BAD_PARAM;
            }

            bool requestFromIncubator = false;
            Int32 gameId = 0;
            if (Request.Params["gameId"] != null &&
                 Int32.TryParse(Request.Params["gameId"], out temp))
            {
                gameId = temp;
            }

            if (Request.Params["user"] != null &&
                 Request.Params["pw"] != null && 
                 gameId != 0)
            {
                int roleMemberShip = 0;
                DataRow dr = UsersUtility.GetUserByEmail(Request.Params["user"]);

                if (dr != null)
                {
                    // if they pass user(email)/pw then authenticate them
                    if (UsersUtility.Authorize(Request.Params["user"], Request["pw"], gameId, ref roleMemberShip, "", false) == (int)Constants.eLOGIN_RESULTS.SUCCESS)
                    {
                        userValidated = true;

                        Game game = GetGameFacade.GetGameByGameId(gameId);

                        if (game!=null && game.IsIncubatorHosted && Request.Params["user"] == KanevaGlobals.IncubationOwner)
                        {
                            //Impersonate as the real owner
                            userId = game.OwnerId;
                            requestFromIncubator = true;
                        }
                        else
                        {
                            userId = Convert.ToInt32(dr["user_id"]);
                        }
                    }
                }

                if( !userValidated )
                {
                    if ( version == 0)
                    {
                        Response.Write(RESP_NOT_VALIDATED);
                    }
                    else
                    {
                        resp = "<Result><ReturnCode>";
                        resp += RESP_NOT_VALIDATED; 
                        resp += "</ReturnCode><ReturnDescription>Not Validated</ReturnDescription></Result>";
                        Response.Write(resp);
                    }
                    return;
                }
            }

            if ( userValidated || CheckUserId(true))
            {
                if (userId == 0)
                    userId = m_userId;

                String licenseKey = Request.Params["licenseKey"];
                String hostName = Request.Params["hostName"];
                String patchUrl = Request.Params["patchUrl"];
                String patchType = Request.Params["patchType"];         // Valid patch types are TEMPLATE/WORLD/WOK (see game_patch_urls for updates)
                String clientPath = Request.Params["clientPath"];       // Sub directory if type is TEMPLATE. E.g. 4.1.2.3\2-adventure, Live\3-community

                if (!ValidatePatchTypeAndClientPath(ref patchType, ref clientPath, gameId, requestFromIncubator))
                {
                    // Invalid input
                    if (version == 0)
                    {
                        resp = RESP_BAD_PARAM;
                    }
                    else
                    {
                        resp = "<Result><ReturnCode>";
                        resp += RESP_BAD_PARAM;
                        resp += "</ReturnCode><ReturnDescription>Invalid patch type or client path</ReturnDescription></Result>";
                    }
                    Response.Write(resp);
                    return;
                }

                // must have some identifier for game and 
                // must want us to do something.  can pass both
                if ((licenseKey != null || Request.Params["gameId"] != null) && 
                    (hostName != null || patchUrl != null))
                {

                    Int32 port = 25857;
                    if ( Request.Params["port"] != null &&
                         Int32.TryParse(Request.Params["port"], out temp) )
                    {
                        port = temp;
                    }

                    // if they pass in //AUTO/ for server name, use the IP
                    int index = patchUrl.IndexOf("//AUTO/");
                    string alternatePatchUrl = "";
                    if (index > 0 )
                    {
                        // AUTO usually used for NAT issues, so they may not be able to use
                        // the UserHostAddress so we'll add their local hostname as alternate
                        alternatePatchUrl = patchUrl;
                        alternatePatchUrl = patchUrl.Replace("//AUTO/", "//" + hostName + "/");

                        patchUrl = patchUrl.Replace("//AUTO/", "//" + Common.GetVisitorIPAddress() + "/");
                    }
                    if ( !patchUrl.EndsWith("/" ) )
                        patchUrl += "/"; // patcher wants trailing slash

                    int webAssignedPort = 0;
                    int serverId;

                    // will return rc values of 
                    // 0 = ok
                    // 2 = key or game_id not found
                    // 3 = not authorized of game not found
                    // 5 = access denied (or user id not found)
                    int returnCode = new GameFacade().AddNewServerAndPatch(userId, licenseKey, gameId, hostName, port, patchUrl, alternatePatchUrl, patchType, clientPath, ref webAssignedPort, out serverId);

                    String returnDescription;

                    switch ( returnCode)
                    {
                        case 0:
                            returnDescription = "OK";
                            break;
                        case 2:
                            returnDescription = "Key or game id not found";
                            break;
                        case 3:
                            returnDescription = "Not authorized or game not found";
                            break;
                        case 5:
                            returnDescription = "Access denied or user id not found";
                            break;
                        default:
                            returnDescription = "Undefined";
                            break;
                    }

                    if ( version == 0)
                    {
                        resp = returnCode.ToString();
                    }
                    else
                    {
                        resp = "<Result><ReturnCode>";
                        resp += returnCode.ToString();
                        resp += "</ReturnCode><ReturnDescription>";
                        resp += returnDescription;
                        resp += "</ReturnDescription><IpPort>";
                        resp += webAssignedPort.ToString();
                        resp += "</IpPort><ServerId>";
                        resp += serverId.ToString();
                        resp += "</ServerId></Result>";
                    }
                }
            }
            else
            {
                if ( version == 0)
                {
                    resp = RESP_NOT_VALIDATED;
                }
                else
                {
                    resp = "<Result><ReturnCode>";
                    resp += RESP_NOT_VALIDATED; 
                    resp += "</ReturnCode><ReturnDescription>Not Validated</ReturnDescription></Result>";
                }
            }
            Response.Write(resp);
        }

        public bool ValidatePatchTypeAndClientPath( ref string patchType, ref string clientPath, int gameId, bool requestFromIncubator )
        {
            // Allow omission of new parameters for old client/editor
            if (patchType == null) patchType = "";
            if (clientPath == null) clientPath = "";

            patchType = patchType.ToUpper();

            // Validate client path
            if (clientPath != "")
            {
                bool clientPathIsValid = true;
                if (patchType != "TEMPLATE")
                {
                    // Only TEMPLATE patch can specify subdirectory (usually version\name)
                    clientPathIsValid = false;
                }
                else if (clientPath.IndexOf("..\\") != -1 || clientPath.IndexOf("../") != -1)
                {
                    // Should not include parent directory access
                    clientPathIsValid = false;
                }
                else
                {
                    // Should not include non-ascii/control/invalid file name characters or '%' and '&' (to prevent html/url escaping)
                    // Also prohibit semicolon as it's used as delimiter in serverlist.aspx result
                    for (int i = 0; i < clientPath.Length; i++)
                    {
                        String invalidCharacters = "<>:\"/|?*%&;";
                        if (clientPath[i] < ' ' || clientPath[i] > '\x7F' || clientPath.IndexOfAny(invalidCharacters.ToCharArray()) != -1)
                        {
                            clientPathIsValid = false;
                            break;
                        }
                    }
                }

                if (!clientPathIsValid)
                {
                    return false;
                }
            }

            // Only allow "WORLD" patch in general. If request is sent from incubator, also allow "TEMPLATE" patch.
            if (patchType == "")
            {
                // old style request: no prefix but patcher will validate all paths in versioninfo.dat
                clientPath = "";
            }
            else if (patchType == "WORLD")
            {
                // World patch - store in Worlds\<game_id>
                clientPath = CLIENT_PATH_WORLDS_ROOT + gameId;
            }
            else if (patchType == "TEMPLATE" && requestFromIncubator)
            {
                // Template patch - store in Templates\<version>\<name> (or other format determined by incubator)
                clientPath = CLIENT_PATH_TEMPLATES_ROOT + clientPath;
            }
            else
            {
                // Invalid patch type
                return false;
            }

            return true;
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
