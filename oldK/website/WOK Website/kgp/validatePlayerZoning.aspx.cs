///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for texturesList.
    /// </summary>
    public class validatePlayerZoning : KgpBasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(false))
            {
                if (Request.Params["action"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>Action not specified.</ReturnDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }
                string actionreq = Request.Params["action"];

                if (actionreq.Equals("ValidateURLPermissions"))
                {
                    if (Request.Params["stpurl"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>Url was not specified.</ReturnDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                    string url = Request.Params["stpurl"].Trim();
                    string origUrl = url;

                    // Remove any non-standard hash tags at the end of the urls
                    // Original url is preserved and sent back, but this fixes issues for "Mall", "Plaza", etc.
                    int hashIndex = url.LastIndexOf('#');
                    if (hashIndex > 0)
                    {
                        url = url.Remove(hashIndex);
                    }

                    // Ka-Ching is a special case
                    if (url.ToLower().Contains("ka-ching! lobby.zone"))
                    {
                        string kachingResult = "<Result><AllowRequestAccess>0</AllowRequestAccess><ThumbnailMediumPath></ThumbnailMediumPath><CommunityId>0</CommunityId><STPURL>" + url + "</STPURL><ReturnDescription>Access allowed.</ReturnDescription><ReturnCode>0</ReturnCode></Result>";
                        Response.Write(kachingResult);
                        return;
                    }

                    if (!StpUrl.ValidateUrl(url))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>Url is invalid.</ReturnDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    string gameName = string.Empty;
                    string placeName = string.Empty;
                    string accessMessage = string.Empty;
                    int placeType = 0;
                    int gameId = 0;
                    int communityId = 0;
                    int accessReason = 0;
                    bool accessAllowed = false;
                    bool userBlocked = false;
                    Community community;

                    StpUrl.ExtractGameName(url, out gameName);
                    StpUrl.ExtractPlaceName(url, out placeName, true);
                    StpUrl.ExtractPlaceType(url, out placeType);

                    if (placeType == (int)Constants.APARTMENT_ZONE)
                    {
                        // Zoning to a user's home
                        int personId = GetUserFacade.GetUserIdFromUsername(placeName);
                        communityId = GetUserFacade.GetUserHomeCommunityIdByUserId(personId);
                    }
                    else if (placeType == (int)Constants.ZONE_TO_PERSON)
                    {
                        // Zoning to a person
                        int personId = GetUserFacade.GetUserIdFromUsername(placeName);

                        userBlocked = GetUserFacade.IsUserBlocked(personId, KanevaWebGlobals.CurrentUser.UserId);

                        // Don't bother with place info if the user has been blocked
                        if (!userBlocked)
                        {
                            DataTable dtPlaceInfo = UsersUtility.GetCurrentPlaceInfo(personId);

                            if (dtPlaceInfo != null && dtPlaceInfo.Rows.Count > 0)
                            {
                                communityId = (int)dtPlaceInfo.Rows[0]["community_id"];
                            }
                        }
                    }
                    else if (placeType == (int)Constants.BROADBAND_ZONE)
                    {
                        // Zoning to a community
                        communityId = GetCommunityFacade.GetCommunityIdFromName(placeName, false);
                    }
                    else if (Int32.TryParse(gameName, out gameId) && gameId != KanevaGlobals.WokGameId)
                    {
                        // Zoning to a game by game_id, OR a permanent zone. No need to check WoK.
                        communityId = GetCommunityFacade.GetCommunityIdFromGameId(gameId);
                    }
                    else if(!gameName.ToLower().Replace(" ", "").Equals(KanevaGlobals.WokGameName.ToLower().Replace(" ", "")))
                    {
                        // Zoning to a game by game_name, OR a permanent zone. No need to check WoK.
                        communityId = GetCommunityFacade.GetCommunityIdFromName(gameName, false);
                    }

                    // We can only validate if we have a valid community or the user was blocked. Otherwise, let them through and let the client handle it.
                    if (userBlocked)
                    {
                        community = new Community();
                        accessAllowed = false;
                        accessReason = (int)CommunityAccessReason.USER_BLOCKED;
                        accessMessage = "This World is not available.";
                    }
                    else if (communityId > 0 && !GetUserFacade.IsUserGM(m_userId))
                    {
                        community = GetCommunityFacade.GetCommunity(communityId);
                        accessAllowed = (community.CommunityId == 0 || GetCommunityFacade.CanUserAccessCommunity(community, KanevaWebGlobals.CurrentUser.UserId, KanevaWebGlobals.CurrentUser.HasAccessPass, KanevaWebGlobals.CurrentUser.Over21, out accessReason, out accessMessage));
                    }
                    else
                    {
                        community = new Community();
                        accessAllowed = true;
                    }

                    XmlDocument doc = new XmlDocument();
                    XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");

                    XmlElement elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = accessReason.ToString();
                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = accessMessage.ToString();
                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("STPURL");
                    elem.InnerText = origUrl;
                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("CommunityId");
                    elem.InnerText = community.CommunityId.ToString();
                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ThumbnailMediumPath");
                    elem.InnerText = community.ThumbnailMediumPath;
                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("AllowRequestAccess");
                    elem.InnerText = (accessReason == (int)CommunityAccessReason.MEMBERSHIP_REQUIRED ? 1 : 0).ToString();
                    root.InsertBefore(elem, root.FirstChild);

                    root.InsertBefore(elem, root.FirstChild);
                    Response.Write(root.OuterXml);
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
