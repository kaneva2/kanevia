///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for getImageUrl.
	/// </summary>
	public class getImageUrl : KgpBasePage
	{
		private const string WRONG_TYPE = "1";
		private const string NOT_FOUND = "2";
		private const string BAD_PARAM = "7";

		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
				bool wantThumbnail = Request.Params["thumb"] != null;
				int userId = 0;
				string userName = null;
				int assetId = 0;

				if( Request.Params["userId"] != null)
				{
					userId = Int32.Parse(Request.Params["userId"].ToString());
				}

				if( Request.Params["assetId"] != null)
				{
					assetId = Int32.Parse(Request.Params["assetId"].ToString());
				}

				if (Request.Params["username"] != null)
				{
					userName = Request.Params["username"].ToString();
				}

				if ( userId == 0 && assetId == 0 && userName==null )
				{
					string errorStr = BAD_PARAM;
					Response.Write(errorStr);
					return;
				}

				string response = NOT_FOUND; // not found
				if ( assetId > 0 )
				{
					DataRow dr = StoreUtility.GetAsset( assetId );
					if ( dr != null )
					{
						if ( wantThumbnail )
						{
							if ( !dr["thumbnail_medium_path"].Equals (DBNull.Value) )
								response = KanevaGlobals.ImageServer + "/" + dr["thumbnail_medium_path"].ToString();
						}
						else
						{
                            // Return full if under 250KB (i.e. 256000B)
                            if ( !dr["file_size"].Equals (DBNull.Value) )
                            {
                                if (Convert.ToInt32 (dr["file_size"]) > 256000)
                                {
                                    if (!dr["thumbnail_xlarge_path"].Equals(DBNull.Value))
                                        response = KanevaGlobals.ImageServer + "/" + dr["thumbnail_xlarge_path"].ToString();
                                }
                                else
                                {
                                    if (!dr["image_full_path"].Equals(DBNull.Value))
                                        response = KanevaGlobals.ImageServer + "/" + dr["image_full_path"].ToString();
                                }
                            }
                            else
                            {
                                // Leave this case alone (why check for image_full_path null then return thumbnail_xlarge_path?)
							    if ( !dr["image_full_path"].Equals (DBNull.Value) )
								    response = KanevaGlobals.ImageServer + "/" + dr["thumbnail_xlarge_path"].ToString();
								    // don't get huge ones! response = KanevaGlobals.ImageServer + "/" + dr["image_full_path"].ToString();
                            }


						}
					}
				}
				else if ( userId > 0 || userName!=null )
				{
					User user;
					if (userId > 0)
						user = GetUserFacade.GetUser(userId);
					else
						user = GetUserFacade.GetUser(userName);

                    if (user != null)
					{
						response = "";

						if ( wantThumbnail )
						{
							if ( user.ThumbnailSquarePath.Length > 0 )
                                response = KanevaGlobals.ImageServer + "/" + user.ThumbnailSquarePath;
						}
						else
						{
                            if (user.ThumbnailXlargePath.Length > 0)
                                response = KanevaGlobals.ImageServer + "/" + user.ThumbnailXlargePath;
						}

						if ( response.ToLower().EndsWith( ".gif" ) || response.Length == 0 ) 
						{
							// send down default
							if(user.Gender.Length > 0)
							{
								if(user.Gender.ToUpper().Equals("M"))
								{
									response = KanevaGlobals.ImageServer + "/" + DEFAULT_MEMBER_CHANNEL_IMAGE_MALE_SQUARE_JPG;
								}
                                else if (user.Gender.ToUpper().Equals("F"))
								{
                                    response = KanevaGlobals.ImageServer + "/" + DEFAULT_MEMBER_CHANNEL_IMAGE_FEMALE_SQUARE_JPG;
								}
							}
							else
							{
                                response = KanevaGlobals.ImageServer + "/" + DEFAULT_MEMBER_CHANNEL_IMAGE_NOTSET_SQUARE_JPG;
							}
						}
					}
					else
					{
						// send down default
                        response = KanevaGlobals.ImageServer + "/" + DEFAULT_MEMBER_CHANNEL_IMAGE_NOTSET_SQUARE_JPG;
					}

				}
				
				if ( response.ToLower().EndsWith( ".gif" ) )
					response = WRONG_TYPE;
				Response.Write(response);
			}
		}

        //private const string DEFAULT_MEMBER_CHANNEL_IMAGE_MALE_JPG = "/images/KanevaIconMale.jpg";
        private const string DEFAULT_MEMBER_CHANNEL_IMAGE_MALE_SQUARE_JPG = "images/KanevaIconMaleSquare.jpg";
        //private const string DEFAULT_MEMBER_CHANNEL_IMAGE_FEMALE_JPG = "/images/KanevaIconFemale.jpg";
        private const string DEFAULT_MEMBER_CHANNEL_IMAGE_FEMALE_SQUARE_JPG = "images/KanevaIconFemaleSquare.jpg";
        //private const string DEFAULT_MEMBER_CHANNEL_IMAGE_NOTSET_JPG = "/images/KanevaIconUnknownGender.jpg";
        private const string DEFAULT_MEMBER_CHANNEL_IMAGE_NOTSET_SQUARE_JPG = "images/KanevaIconUnknownGenderSquare.jpg";

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
