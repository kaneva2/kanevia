///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;
using System.Diagnostics;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Provide content metadata (see ContentService class in KEPClient)
	/// </summary>
	public class ContentMetadata : KgpBasePage
	{
        const int CACHE_DURATION_10_DAY = 14400;
        const int TEXTURE_ORDINAL_AND_COLOR_RADIX = 10;

		private static string GetURLPrefixByItemUseTypeAndPathType(int itemUseType, string itemPathType)
		{
			string urlPrefix = "";
            if (itemPathType.StartsWith("template_") || itemPathType.StartsWith("texture_") || itemPathType.StartsWith("tem_") || itemPathType.StartsWith("tex_"))
            {
				urlPrefix = KanevaGlobals.TextureServer;
            }
            else
            {
                switch (itemUseType) {
                    case WOKItem.USE_TYPE_ADD_DYN_OBJ:
                        urlPrefix = KanevaGlobals.MeshServer;
                        break;
                    case WOKItem.USE_TYPE_SOUND:
                        urlPrefix = KanevaGlobals.SoundServer;
                        break;
                    case WOKItem.USE_TYPE_ANIMATION:
                        urlPrefix = KanevaGlobals.AnimationServer;
                        break;
                    case WOKItem.USE_TYPE_EQUIP:
                        urlPrefix = KanevaGlobals.EquippableServer;
                        break;
                    case WOKItem.USE_TYPE_PARTICLE:
                        urlPrefix = KanevaGlobals.MeshServer;
                        break;
                }
			}

			return urlPrefix;
		}

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Login is not required for UGC metadata

            if (Request.Params["preload"] != null)
            {
                getPreloadMetadata();
            }
            else if (Request.Params["glid"] != null)
            {
                getItemMetadata();
            }
            else
            {
                string errorStr = "<Result><ReturnCode>-1</ReturnCode><ResultDescription>`glid' not specified</ResultDescription></Result>";
                Response.Write(errorStr);
                Response.End();
            }
       }

        private void getPreloadMetadata()
        {
            if (Request.Params["curver"] == null)
            {
                string errorStr = "<Result><ReturnCode>-1</ReturnCode><ResultDescription>`curver' not specified</ResultDescription></Result>";
                Response.Write(errorStr);
                Response.End();
            }

            int currentCachedVersion = Convert.ToInt32(Request.Params["curver"]);
            int updateVersion = GetShoppingFacade.GetLatestItemPreloadListVersion();
            if (updateVersion < 0)
            {
                // Something is wrong in item_preloads table
                Response.Write("<Result><ReturnCode>-1</ReturnCode><ResultDescription>Internal error</ResultDescription></Result>");
                Response.End();
                return;
            }

            Debug.Assert(updateVersion >= currentCachedVersion);

            if (updateVersion <= currentCachedVersion)
            {
                // Nothing to update
                Response.Write("<Result><ReturnCode>0</ReturnCode><ResultDescription>Already up-to-date</ResultDescription></Result>");
                Response.End();
                return;
            }

            Dictionary<string, string> attrs = new Dictionary<string, string>();
            attrs.Add("preloadListVersion", updateVersion.ToString());

            if (!getItemMetadataByFilter("INNER JOIN wok.`item_preloads` ipl ON ipl.`ver`=" + updateVersion.ToString() + " INNER JOIN wok.`item_preload_items` ipi ON ipi.`list_id` = ipl.`list_id` AND ipi.`global_id` = i.`global_id`", "TRUE", attrs))
            {
                // Nothing to update
                Response.Write("<Result preloadListVersion=\"" + updateVersion.ToString() + "\"><ReturnCode>0</ReturnCode><ResultDescription>No records</ResultDescription></Result>");
                Response.End();
                return;
            }
        }

        private void getItemMetadata()
        {
            string kanevaDb = KanevaGlobals.DbNameKaneva;
            string glids = Request.Params["glid"].ToString().Trim(',');

            // param validation
            foreach (char ch in glids)
            {
                if (!(ch == ',' || ch >= '0' && ch <= '9'))
                {
                    // bad param
                    string errorStr = "<Result><ReturnCode>-1</ReturnCode><ResultDescription>bad glid parameter</ResultDescription></Result>";
                    Response.Write(errorStr);
                    return;
                }
            }

            if (!getItemMetadataByFilter("", "i.global_id IN (" + glids + ")", null))
            {
                string errorStr = "<Result><ReturnCode>2</ReturnCode><ResultDescription>glid not found</ResultDescription></Result>";
                Response.Clear();
                Response.Write(errorStr);
                Response.End();
            }
        }

        // Return false if no records found
        private bool getItemMetadataByFilter(string additionalJoin, string filter, IDictionary<string, string> additionalAttrs)
        {
            IDictionary<string, string> tagOverrides = new Dictionary<string, string>();
            string strTagOverrides = Request["tag_override"];
            if (strTagOverrides != null)
            {
                string[] tagTokens = strTagOverrides.Split(',');
                foreach (string tagToken in tagTokens)
                {
                    string[] keyVal = tagToken.Split('!');
                    if (keyVal.Length == 2 && keyVal[0] != "")
                    {
                        tagOverrides.Add(keyVal[0], keyVal[1]);
                    }
                    else
                    {
                        // bad param
                        string errorStr = "<Result><ReturnCode>-1</ReturnCode><ResultDescription>bad tag_override parameter</ResultDescription></Result>";
                        Response.Write(errorStr);
                        Response.End();
                        return true;
                    }
                }
            }

			Hashtable parameters = new Hashtable();

			string sql = 
                "SELECT i.global_id, i.name, i.description, COALESCE(iw.base_global_id, 0) AS base_global_id, iw.texture_path, iw.template_path_encrypted, " +
                "  iw.thumbnail_assetdetails_path, COALESCE(iw.cache_duration, 1440) as cache_duration, i.use_type, i.item_creator_id, " +
				"  ipt.type as ip_type, ipt.xml_tag as ip_xml_tag, ip.ordinal as ip_ordinal, ip.path as ip_path, i.use_type, " +
                "  IF(i.use_type = " + WOKItem.USE_TYPE_ANIMATION + ", IF(iw.category_id3 = 73 AND (i.actor_group = 0 OR i.actor_group = 1), 1, i.actor_group), IFNULL(iprm.value, 0)) as use_value, " +
                "  IFNULL(iprm2.value, '') as exclusion_groups, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as pass_group_id, " +
                "  ip.file_size, ip.file_hash, ipt.store_type+0 as store_type, ip.unique_id, iptex.color, COALESCE(fs.filestore, '') as filestore " +
                "FROM wok.items i " +
				"  LEFT JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
				"  LEFT JOIN wok.item_parameters iprm ON i.global_id=iprm.global_id AND iprm.param_type_id=i.use_type " +
				"  LEFT JOIN wok.item_parameters iprm2 ON i.global_id=iprm2.global_id AND iprm2.param_type_id=" + ItemParameter.PARAM_TYPE_EXCLUSION_GROUPS.ToString() + 
				"  LEFT JOIN wok.item_paths ip ON i.global_id = ip.global_id " +
                "  LEFT JOIN wok.item_path_filestores fs ON fs.global_id=ip.global_id AND fs.item_path_type_id=ip.type_id " +
                "  LEFT JOIN wok.item_path_textures iptex ON ip.global_id=iptex.global_id AND ip.ordinal=iptex.ordinal " +
                "  LEFT JOIN wok.item_path_types ipt ON ip.type_id = ipt.type_id " +
                "  LEFT JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id " +
                additionalJoin + " " +
				"WHERE " + filter + " " +
				"ORDER BY i.global_id, ip.type_id DESC, ip.ordinal ";

			DataTable dt = KanevaGlobals.GetDatabaseUtility().GetDataTable(sql, parameters);

            if (dt != null && dt.Rows.Count > 0)
			{
				// Build the XML
				XmlDocument doc = new XmlDocument();
				XmlElement rootNode = doc.CreateElement("Result");
				doc.AppendChild(rootNode);

                rootNode.SetAttribute("textureNumberRadix", Convert.ToInt32(TEXTURE_ORDINAL_AND_COLOR_RADIX).ToString());

                if (additionalAttrs != null)
                {
                    foreach (var pair in additionalAttrs)
                    {
                        rootNode.SetAttribute(pair.Key, pair.Value);
                    }
                }

				XmlElement metadataNode = null;
                XmlElement elemTextures = null;     // Individual texture information
                Dictionary<string, XmlElement> elemTextureMap = new Dictionary<string, XmlElement>();

				string texturePath = "";

				int prevGlobalId = 0;
                HashSet<string> itemPathTagFilter = new HashSet<string>();
                uint numRecords = 0;

				for (int i = 0; i < dt.Rows.Count; i++)
				{
					int globalId = 0;
					try
					{
						globalId = Convert.ToInt32(dt.Rows[i]["global_id"]);
					}
					catch (System.Exception) {}

                    if (globalId != prevGlobalId)
					{
                        prevGlobalId = globalId;
                        itemPathTagFilter.Clear();

                        // Create new metadata element
                        metadataNode = doc.CreateElement("Record");
                        metadataNode.SetAttribute("cacheDuration", CACHE_DURATION_10_DAY.ToString());
						rootNode.AppendChild(metadataNode);
                        numRecords++;

                        // Reset <textures> child element
                        elemTextures = null;
                        elemTextureMap.Clear();

                        XmlElement childNode;
                        childNode = doc.CreateElement("global_id");
                        childNode.InnerText = dt.Rows[i]["global_id"].ToString();
                        metadataNode.AppendChild(childNode);

                        childNode = doc.CreateElement("global_id");
                        childNode.InnerText = dt.Rows[i]["global_id"].ToString();
                        metadataNode.AppendChild(childNode);

                        childNode = doc.CreateElement("base_global_id");
                        childNode.InnerText = dt.Rows[i]["base_global_id"].ToString();
                        metadataNode.AppendChild(childNode);

                        childNode = doc.CreateElement("use_type");
                        childNode.InnerText = dt.Rows[i]["use_type"].ToString();
                        metadataNode.AppendChild(childNode);

                        childNode = doc.CreateElement("use_value");
                        childNode.InnerText = dt.Rows[i]["use_value"].ToString();
                        metadataNode.AppendChild(childNode);

                        string exclusionGroups = dt.Rows[i]["exclusion_groups"].ToString();
                        if (exclusionGroups != "")
                        {
                            childNode = doc.CreateElement("exclusion_groups");
                            childNode.InnerText = exclusionGroups;
                            metadataNode.AppendChild(childNode);
                        }

                        // Use encrypted version?
                        if (dt.Rows[i]["template_path_encrypted"].ToString().Length > 0)
                        {
                            texturePath = dt.Rows[i]["template_path_encrypted"].ToString().Replace("\\", "/");
                        }
                        else
                        {
                            texturePath = dt.Rows[i]["texture_path"].ToString().Replace("\\", "/");
                        }

                        if (texturePath != "")
                        {
                            childNode = doc.CreateElement("texture_url");
                            childNode.InnerText = texturePath.Length == 0 ? "" : KanevaGlobals.TextureServer + "/" + texturePath;
                            metadataNode.AppendChild(childNode);
                        }

                        childNode = doc.CreateElement("name");
                        childNode.InnerText = dt.Rows[i]["name"].ToString();
                        metadataNode.AppendChild(childNode);

                        childNode = doc.CreateElement("description");
                        childNode.InnerText = dt.Rows[i]["description"].ToString();
                        metadataNode.AppendChild(childNode);

                        childNode = doc.CreateElement("thumbnailXLarge");
                        childNode.InnerText = StoreUtility.GetItemImageURL(dt.Rows[i]["thumbnail_assetdetails_path"].ToString(), "ad");
                        metadataNode.AppendChild(childNode);

                        childNode = doc.CreateElement("pass_group_id");
                        childNode.InnerText = dt.Rows[i]["pass_group_id"].ToString();
                        metadataNode.AppendChild(childNode);
					}

					//additional file paths from wok.item_paths table
                    int itemUseType = -1;
					try
					{
                        itemUseType = Convert.ToInt32(dt.Rows[i]["use_type"]);
                    }
                    catch (System.Exception) { }

                    string itemPathType = null;
                    string itemPathXmlTag = null;
                    string itemPath = null;
                    string itemPathOrdinal = null;
                    int itemFileSize = 0;
                    string itemFileHash = "0";
                    int assetStoreType = 0;
                    uint assetUniqueId = 0;
                    try
                    {
                        // Columns from item_paths table
                        // If left join fails exception will throw here and all following variables will stay with default value above.
                        itemPathType = Convert.ToString(dt.Rows[i]["ip_type"]);
						itemPathXmlTag = Convert.ToString(dt.Rows[i]["ip_xml_tag"]);
						itemPath = Convert.ToString(dt.Rows[i]["ip_path"]);
						itemPathOrdinal = Convert.ToString(dt.Rows[i]["ip_ordinal"]);
                        itemFileSize = Convert.ToInt32(dt.Rows[i]["file_size"]);
                        itemFileHash = Convert.ToString(dt.Rows[i]["file_hash"]);
                        assetStoreType = Convert.ToInt32(dt.Rows[i]["store_type"]);
                        assetUniqueId = Convert.ToUInt32(dt.Rows[i]["unique_id"]);
                    }
                    catch (System.Exception) {}

                    string itemPathFileStore = "";
                    itemPathFileStore = Convert.ToString(dt.Rows[i]["filestore"]);

                    // Override tags for any asset experiments (use network.cfg)
                    if (tagOverrides.ContainsKey(itemPathXmlTag))
                    {
                        itemPathXmlTag = tagOverrides[itemPathXmlTag];
                    }

					if (metadataNode!=null && itemPathType != null && itemPathXmlTag != null && itemPathXmlTag != "" && itemPath != null)
					{
                        // By filtering with xml tag, we allow an item to have multiple item_paths records that uses same xml_tag. 
                        // Only the one with highest type_id will be sent in the result. (The query is ordered by type_id DESC)
                        string itemPathTagKey = itemPathXmlTag;
                        if (itemPathOrdinal != null && itemPathOrdinal != "" && itemPathOrdinal != "-1")
                        {
                            itemPathTagKey = itemPathTagKey + "/" + itemPathOrdinal;
                        }

                        if (!itemPathTagFilter.Contains(itemPathTagKey))
                        {
                            itemPathTagFilter.Add(itemPathTagKey);

                            if (itemPathXmlTag == "textures")
                            {
                                if (assetStoreType == (int)ItemPathType.AssetStoreType.UNIQUE)
                                {
                                    var elemTexture = getTextureElement(doc, metadataNode, ref elemTextures, elemTextureMap, itemPathOrdinal);
                                    if (itemPathFileStore != "")
                                    {
                                        elemTextures.SetAttribute("fs", itemPathFileStore);
                                    }
                                    elemTexture.SetAttribute("u", assetUniqueId.ToString());
                                }
                            }
                            else
                            {
                                XmlElement addlURL = doc.CreateElement(itemPathXmlTag);
                                if (itemFileSize != 0)
                                {
                                    addlURL.SetAttribute("size", Convert.ToString(itemFileSize));
                                }
                                if (itemFileHash != "")
                                {
                                    addlURL.SetAttribute("hash", itemFileHash);
                                }
                                if (itemPathOrdinal != "-1")
                                {
                                    addlURL.SetAttribute("ordinal", itemPathOrdinal);
                                }
                                if (assetStoreType == (int)ItemPathType.AssetStoreType.UNIQUE)
                                {
                                    addlURL.SetAttribute("u", assetUniqueId.ToString());
                                }

                                string urlPrefix = GetURLPrefixByItemUseTypeAndPathType(itemUseType, itemPathType);
                                addlURL.InnerText = urlPrefix + "/" + itemPath;
                                metadataNode.AppendChild(addlURL);
                            }
                        }
					}

                    // Texture colors
                    if (!Convert.IsDBNull(dt.Rows[i]["color"]))
                    {
                        string itemTexColor = Convert.ToString(dt.Rows[i]["color"]);
                        if (itemPathOrdinal != null && itemPathOrdinal != "" && itemPathOrdinal != "-1" && itemTexColor != null && itemTexColor != "")
                        {
                            // Texture color mapping
                            var elemTexture = getTextureElement(doc, metadataNode, ref elemTextures, elemTextureMap, itemPathOrdinal);
                            elemTexture.SetAttribute("c", itemTexColor);    // Color might be updated multiple times for each unique <t> element
                        }
                    }
                }

				XmlElement elem = doc.CreateElement("NumberRecords");
                elem.InnerText = numRecords.ToString();

				rootNode.InsertBefore(elem, rootNode.FirstChild);

				elem = doc.CreateElement("TotalNumberRecords");
                elem.InnerText = numRecords.ToString();

				rootNode.InsertBefore(elem, rootNode.FirstChild);

                elem = doc.CreateElement("ResultDescription");
				elem.InnerText = "success";

				rootNode.InsertBefore(elem, rootNode.FirstChild);

				elem = doc.CreateElement("ReturnCode");
				elem.InnerText = "0";

				rootNode.InsertBefore(elem, rootNode.FirstChild);

				Response.Clear();
				Response.Write(rootNode.OuterXml);
				Response.End();
                return true;
			}
			else
			{
                return false;
			}
		}

        // Helper to retrieve <textures><t> element based on texture key
        XmlElement getTextureElement(XmlDocument doc, XmlElement metadataNode, ref XmlElement elemTextures, Dictionary<string, XmlElement> elemTextureMap, string textureKey)
        {
            if (elemTextures == null)
            {
                elemTextures = doc.CreateElement("textures");
                metadataNode.AppendChild(elemTextures);
            }

            XmlElement elem;
            if (elemTextureMap.ContainsKey(textureKey))
            {
                elem = elemTextureMap[textureKey];
            }
            else
            {
                elem = doc.CreateElement("t");
                elem.SetAttribute("o", textureKey);
                elemTextureMap.Add(textureKey, elem);
                elemTextures.AppendChild(elem);
            }

            return elem;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
