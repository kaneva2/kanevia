///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// get all the details about a player in the game
    /// </summary>
    public class getItems : KgpBasePage
    {
         private void Page_Load(object sender, System.EventArgs e)
        {
            string wokDb = KanevaGlobals.DbNameKGP;

            Response.Cache.SetNoServerCaching();
            Response.Cache.SetNoStore();

            if (true) // TODO need to validate which STAR is requesting this data, then validate this player KEPAuth'd to it
            {
                if (Request.Params["id"] == null )
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>provide correct ids</ReturnDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                int id = Int32.Parse(Request.Params["id"].ToString());
                int ugcId = 0;
                int templateId = 0;
                if (Request.Params["ugcId"] != null && Request.Params["templateId"] != null)
                {
                    ugcId = Int32.Parse(Request.Params["ugcId"].ToString());
                    templateId = Int32.Parse(Request.Params["templateId"].ToString());
                    if (ugcId < WOKItem.UGC_BASE_GLID - 1 || templateId < WOKItem.UGC_BASE_TEMPLATE_GLID - 1)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>invalid range</ReturnDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
                int count = 1;
                if (Request.Params["count"] != null  )
                {
                    Int32.TryParse(Request.Params["count"], out count);
                }

                ShoppingFacade shoppingFacade = new ShoppingFacade();

                DataTable dt = shoppingFacade.GetItems(id, ugcId, templateId, count);
                dt.TableName = "items";

                // result set
                DataSet ds = new DataSet ("Result");
                ds.Tables.Add (dt.Copy());

                // Get the item Glids so we can check for associated animations
                string glids = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    glids += dt.Rows[i]["global_id"].ToString () + ",";   
                }
                // If we have item glids, then check for associated animations
                if (glids.Length > 0)
                {
                    glids = glids.Remove (glids.Length - 1);
                    DataTable dtAnim = shoppingFacade.GetAnimations (glids);
                    dtAnim.TableName = "animation";

                    // add the animation datatable to dataset
                    ds.Tables.Add (dtAnim.Copy ());

                    // Get the primary key column from the master table
                    DataColumn primarykey = ds.Tables["items"].Columns["global_id"];

                    // Get the primary key column from the master table
                    DataColumn foreignkey = ds.Tables["animation"].Columns["global_id"];

                    //Assign relation
                    DataRelation relation = ds.Relations.Add (primarykey, foreignkey);

                    // Get ADO.NET to generate nexted XML nodes
                    relation.Nested = true;

                    DataTable dtPass = shoppingFacade.GetPasses(glids);
                    dtPass.TableName = "passes";

                    // add the animation datatable to dataset
                    ds.Tables.Add(dtPass.Copy());

                    // Get the primary key column from the master table
                    DataColumn foreignkeyPass = ds.Tables["passes"].Columns["global_id"];

                    //Assign relation
                    DataRelation relationPass = ds.Relations.Add(primarykey, foreignkeyPass);

                    // Get ADO.NET to generate nexted XML nodes
                    relationPass.Nested = true;

                    DataTable dtExGroup = shoppingFacade.GetExclusionGroupIds(glids);
                    dtExGroup.TableName = "exclusiongroups";

                    // add the animation datatable to dataset
                    ds.Tables.Add(dtExGroup.Copy());

                    // Get the primary key column from the master table
                    DataColumn foreignkeyExGroup = ds.Tables["exclusiongroups"].Columns["global_id"];

                    //Assign relation
                    DataRelation relationExGroup = ds.Relations.Add(primarykey, foreignkeyExGroup);

                    // Get ADO.NET to generate nexted XML nodes
                    relationExGroup.Nested = true;
                }

                // setup the result in the header
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(ds.GetXml());
                XmlNode root = doc.DocumentElement;

                XmlElement elem = doc.CreateElement("ReturnDescription");
                elem.InnerText = "success";

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("ReturnCode");
                elem.InnerText = "0";

                root.InsertBefore(elem, root.FirstChild);

                Response.Write(root.OuterXml);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
