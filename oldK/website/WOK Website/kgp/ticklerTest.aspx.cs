///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Xml;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for Fame.
    /// </summary>
    public class ticklerTest : KgpBasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(true))
            {
                // Check pagination
                if ((Request.Params["action"] == null))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                string action = Request.Params["action"];
                string sentXml = string.Empty;

                if (action.Equals("PassList")) 
                {
                    (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.PlayerPassList, RemoteEventSender.ChangeType.UpdateObject, KanevaWebGlobals.CurrentUser.UserId, out sentXml);
                }
                else if (action.Equals("Inventory"))
                {
                    (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.PlayerInventory, RemoteEventSender.ChangeType.UpdateObject, KanevaWebGlobals.CurrentUser.UserId, out sentXml);
                }

                if (string.IsNullOrWhiteSpace(sentXml))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>The tickler event was not sent.</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                }
                else
                {
                    Response.Write(sentXml);
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
