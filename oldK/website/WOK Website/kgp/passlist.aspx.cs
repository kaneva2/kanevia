///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Linq;
using System.Xml;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;
using Kaneva.DataLayer.DataObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for travellist.
	/// </summary>
	public class passlist : KgpBasePage
	{
        #region Declarations

        // Shared report params
        private string _action = null;
        private int _passGroupId;

        // Response types
        private string _errorResult = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>{0}</ResultDescription>\r\n</Result>";

        protected const String KANEVA_NAME = "Kaneva";

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
                GetRequestParams();
				switch(_action)
                {
                    case "setTestUserPass":
                        ProcessSetTestUserPass();
                        break;
                    case "removeTestUserPass":
                        ProcessRemoveTestUserPass();
                        break;
                    default:
                        ProcessDefaultAction();
                        break;
                }
			}
		}

        /// <summary>
        /// Sets page variables using either passed in values or appropriate defaults.
        /// </summary>
        private void GetRequestParams()
        {
            int iOut;

            _action = Request.Params["action"];
            _passGroupId = (int.TryParse(Request.Params["passGroupId"], out iOut) ? iOut : 0);
        }

        private void ProcessDefaultAction()
        {
            if ((Request.Params["type"] == null))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>type not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            GameFacade gameFacade = new GameFacade();
            gameFacade = new GameFacade();

            string requestType = Request.Params["type"].ToString();

            DataSet ds = new DataSet();

            ds.DataSetName = "Result";

            DataTable dt;

            switch (requestType)
            {
                case "z": // zones
                    {
                        if ((Request.Params["zi"] == null) || (Request.Params["ii"] == null))
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>zi or ii not specified</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                        int zoneIndex = Int32.Parse(Request.Params["zi"].ToString());
                        int instanceId = Int32.Parse(Request.Params["ii"].ToString());
                        dt = CommunityUtility.GetZonesPassIds(zoneIndex, instanceId);
                    }
                    break;

                case "u": // users
                    if (string.IsNullOrWhiteSpace(Request.Params["un"]))
                    {
                        dt = UsersUtility.GetPassIds(m_userId);
                    }
                    else
                    {
                        // If specific user is specified, use new bitmask return format
                        int userId = GetUserFacade.GetUserIdFromUsername(Request.Params["un"]);
                        dt = UsersUtility.GetPassIds(userId);

                        int passMask = 0;
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                passMask = passMask | Convert.ToInt32(row["pass_group_id"]); ;
                            }
                        }

                        Response.Write(string.Format("<PassGroups>{0}</PassGroups>", passMask));
                        return;
                    }
                    break;
                case "g": // games
                    int gameId = Int32.Parse(Request.Params["gi"].ToString());

                    DataTable passesTable = gameFacade.GetGamePasses(gameId);

                    DataSet passesSet = new DataSet("Result");
                    passesSet.Tables.Add(passesTable.Copy());

                    XmlDocument passesDoc = new XmlDocument();
                    passesDoc.LoadXml(passesSet.GetXml());
                    XmlNode passesRoot = passesDoc.DocumentElement;

                    int numrecs = passesTable.Rows.Count;
                    XmlElement passesElem = passesDoc.CreateElement("NumberRecords");
                    passesElem.InnerText = numrecs.ToString();

                    passesRoot.InsertBefore(passesElem, passesRoot.FirstChild);

                    int totalRecs = passesTable.Rows.Count;
                    passesElem = passesDoc.CreateElement("TotalNumberRecords");
                    passesElem.InnerText = totalRecs.ToString();

                    passesRoot.InsertBefore(passesElem, passesRoot.FirstChild);

                    passesElem = passesDoc.CreateElement("ReturnDescription");
                    passesElem.InnerText = "success";

                    passesRoot.InsertBefore(passesElem, passesRoot.FirstChild);

                    passesElem = passesDoc.CreateElement("ReturnCode");
                    passesElem.InnerText = "0";

                    passesRoot.InsertBefore(passesElem, passesRoot.FirstChild);

                    Response.Write(passesRoot.OuterXml);
                    return;

                case "ga": // add game pass
                    int addGameId = Int32.Parse(Request.Params["gi"].ToString());
                    int addPassId = Int32.Parse(Request.Params["pi"].ToString());

                    bool addSuccess = gameFacade.AddGamePass(m_userId, IsAdmin, addGameId, addPassId);

                    if (addSuccess && addPassId.Equals(Configuration.AccessPassGroupID))
                    {
                        // Update community
                        Game game = gameFacade.GetGameByGameId(addGameId);
                        game.IsAdult = true;
                        gameFacade.SaveGame(game);
                    }

                    XmlDocument addDoc = new XmlDocument();
                    addDoc.LoadXml(ds.GetXml());
                    XmlNode addRoot = addDoc.DocumentElement;

                    XmlElement addElem = addDoc.CreateElement("ReturnDescription");
                    addElem.InnerText = addSuccess ? "success" : "failure";

                    addRoot.InsertBefore(addElem, addRoot.FirstChild);

                    addElem = addDoc.CreateElement("ReturnCode");
                    addElem.InnerText = addSuccess ? "0" : "-1";

                    addRoot.InsertBefore(addElem, addRoot.FirstChild);

                    Response.Write(addRoot.OuterXml);
                    return;

                case "gd": // delete game pass
                    int deleteGameId = Int32.Parse(Request.Params["gi"].ToString());
                    int deletePassId = Int32.Parse(Request.Params["pi"].ToString());

                    bool deleteSuccess = gameFacade.DeleteGamePass(m_userId, IsAdmin, deleteGameId, deletePassId);

                    if (deleteSuccess && deletePassId.Equals(Configuration.AccessPassGroupID))
                    {
                        // Update community
                        Game game = gameFacade.GetGameByGameId(deleteGameId);
                        game.IsAdult = false;
                        gameFacade.SaveGame(game);
                    }

                    XmlDocument deleteDoc = new XmlDocument();
                    deleteDoc.LoadXml(ds.GetXml());
                    XmlNode deleteRoot = deleteDoc.DocumentElement;

                    XmlElement deleteElem = deleteDoc.CreateElement("ReturnDescription");
                    deleteElem.InnerText = deleteSuccess ? "success" : "failure";

                    deleteRoot.InsertBefore(deleteElem, deleteRoot.FirstChild);

                    deleteElem = deleteDoc.CreateElement("ReturnCode");
                    deleteElem.InnerText = deleteSuccess ? "0" : "-1";

                    deleteRoot.InsertBefore(deleteElem, deleteRoot.FirstChild);

                    Response.Write(deleteRoot.OuterXml);
                    return;

                default:
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invalid zone type specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
            }

            dt.TableName = "PassId";
            ds.Tables.Add(dt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            int numRecords = dt.Rows.Count;
            XmlElement elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = dt.Rows.Count;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Processes the setTestUserPass action.
        /// </summary>
        private void ProcessSetTestUserPass()
        {
            // Confirm user permissions.
            if (!GetUserFacade.IsUserAutomatedTestAccount(KanevaWebGlobals.CurrentUser))
            {
                Response.Write(string.Format(_errorResult, "Unauthorized user."));
                return;
            }

            // Confirm that this call is active on this environment.
            if (!Configuration.AutomatedTestUserGenerationEnabled)
            {
                Response.Write(string.Format(_errorResult, "Unauthorized action."));
                return;
            }

            // Check pass groups
            if (_passGroupId != Configuration.AccessPassGroupID && _passGroupId != Configuration.VipPassGroupID)
            {
                Response.Write(string.Format(_errorResult, "Invalid pass type."));
                return;
            }

            bool success = false;
            SubscriptionFacade subFacade = new SubscriptionFacade();
            if (subFacade.DoesUserAlreadyHaveThisPass((uint)_passGroupId, (uint)KanevaWebGlobals.CurrentUser.UserId))
            {
                success = true;
            }
            else
            {
                // Set up initial Dates
                DateTime dtNow = KanevaGlobals.GetCurrentDateTime();
                DateTime dtTodayEOD = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 23, 59, 59);

                // Get the subscription
                int promotionType = (_passGroupId == Configuration.AccessPassGroupID ? 
                    (int)Promotion.ePROMOTION_TYPE.MATURE_SUBSCRIPTION_PASS : (int)Promotion.ePROMOTION_TYPE.SUBSCRIPTION_PASS);
                Promotion promotion = GetPromotionsFacade.GetActivePromotions(promotionType.ToString(), Constants.CURR_KPOINT).FirstOrDefault();           
                Subscription relatedSubscription = subFacade.GetSubscriptionByPromotionId(promotion.PromotionId);

                // Build the user subscription
                UserSubscription userSubscription = new UserSubscription();
                
                // They get a free trial if there is one
                if (relatedSubscription.DaysFree > 0)
                {
                    userSubscription.EndDate = dtTodayEOD.AddDays(relatedSubscription.DaysFree);
                }
                else
                {
                    userSubscription.EndDate = subFacade.GetNextEndDate(dtNow, dtNow, relatedSubscription.Term);
                }

                userSubscription.FreeTrialEndDate = dtTodayEOD.AddDays(relatedSubscription.DaysFree);
                userSubscription.AutoRenew = false;
                userSubscription.PromotionId = (uint)promotion.PromotionId;
                userSubscription.BillingType = (int)Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE;
                userSubscription.CancelledDate = DateTime.Now;
                userSubscription.InitalTerm = relatedSubscription.Term;
                userSubscription.Price = (double)relatedSubscription.Price;
                userSubscription.PurchaseDate = DateTime.Now;
                userSubscription.RenewalTerm = relatedSubscription.Term;
                userSubscription.StatusId = Subscription.SubscriptionStatus.Active;
                userSubscription.SubscriptionId = relatedSubscription.SubscriptionId;
                userSubscription.SubscriptionIdentifier = "-1";
                userSubscription.UserCancelled = false;
                userSubscription.UserId = KanevaWebGlobals.CurrentUser.UserId;
                userSubscription.MontlyAllowance = relatedSubscription.MonthlyAllowance;
                userSubscription.DiscountPercent = relatedSubscription.DiscountPercent;

                subFacade.InsertUserSubscription(userSubscription);

                success = UsersUtility.AddUserToPassGroup(userSubscription.UserId, _passGroupId) > 0;

                CentralCache.Remove(CentralCache.keyKanevaUserSubscriptionValidByPassGroup + userSubscription.UserId.ToString() + _passGroupId.ToString());

                // Tickle the wok server
                try
                {
                    (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.PlayerPassList, RemoteEventSender.ChangeType.UpdateObject, userSubscription.UserId);
                }
                catch (Exception) { }
            }

            // Send the new token as response.
            if (success)
            {
                Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n </Result>");
            }
            else
            {
                Response.Write(string.Format(_errorResult, "Failed adding user to pass group."));
            }
        }

        /// <summary>
        /// Processes the setTestUserPass action.
        /// </summary>
        private void ProcessRemoveTestUserPass()
        {
            // Confirm user permissions.
            if (!GetUserFacade.IsUserAutomatedTestAccount(KanevaWebGlobals.CurrentUser))
            {
                Response.Write(string.Format(_errorResult, "Unauthorized user."));
                return;
            }

            // Confirm that this call is active on this environment.
            if (!Configuration.AutomatedTestUserGenerationEnabled)
            {
                Response.Write(string.Format(_errorResult, "Unauthorized action."));
                return;
            }

            // Check pass groups
            if (_passGroupId != Configuration.AccessPassGroupID && _passGroupId != Configuration.VipPassGroupID)
            {
                Response.Write(string.Format(_errorResult, "Invalid pass type."));
                return;
            }

            bool success = true;
            SubscriptionFacade subFacade = new SubscriptionFacade();

            // Get the related subscription
            int promotionType = (_passGroupId == Configuration.AccessPassGroupID ?
                (int)Promotion.ePROMOTION_TYPE.MATURE_SUBSCRIPTION_PASS : (int)Promotion.ePROMOTION_TYPE.SUBSCRIPTION_PASS);
            Promotion promotion = GetPromotionsFacade.GetActivePromotions(promotionType.ToString(), Constants.CURR_KPOINT).FirstOrDefault();
            Subscription relatedSubscription = subFacade.GetSubscriptionByPromotionId(promotion.PromotionId);

            // End all matching subscriptions
            List<UserSubscription> subscriptions = subFacade.GetActiveUserSubscriptions(KanevaWebGlobals.CurrentUser.UserId);
            foreach (UserSubscription userSubscription in subscriptions)
            {
                if (userSubscription.SubscriptionId == relatedSubscription.SubscriptionId)
                {
                    userSubscription.UserCancelled = true;
                    userSubscription.UserCancellationReason = "Test Cancellation";
                    userSubscription.CancelledDate = DateTime.Now;
                    userSubscription.EndDate = DateTime.Now;
                    userSubscription.StatusId = Subscription.SubscriptionStatus.Cancelled;

                    subFacade.UpdateUserSubscription(userSubscription);
                    UsersUtility.DeleteUserFromPassGroup(KanevaWebGlobals.CurrentUser.UserId, _passGroupId);

                    CentralCache.Remove(CentralCache.keyKanevaUserSubscriptionValidByPassGroup + userSubscription.UserId.ToString() + _passGroupId.ToString());

                    // Tickle the wok server
                    try
                    {
                        (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.PlayerPassList, RemoteEventSender.ChangeType.UpdateObject, userSubscription.UserId);
                    }
                    catch (Exception) { }
                }
            }

            // Send the new token as response.
            if (success)
            {
                Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n </Result>");
            }
            else
            {
                Response.Write(string.Format(_errorResult, "Failed removing user from pass group."));
            }
        }

        private bool IsAdmin
        {
            get { return (KanevaWebGlobals.CheckUserAccess((int)SitePrivilege.ePRIVILEGE.STAR_ADMIN) == (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL); }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}