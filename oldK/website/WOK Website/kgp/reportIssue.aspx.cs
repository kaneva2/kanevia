///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.IO;
using System.Xml;
using System.Net;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace WOK.kgp
{
    public partial class reportIssue : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string actionreq = "InsertIssue";
            int returnCode = 0;

            if (Request.Params["action"] != null)
            {
                actionreq = Request.Params["action"];
            }

            if (actionreq.Equals("InsertIssue"))
            {
                string GUID = "No Crash Report GUID";
                string IssueText = "";
                string result = "";
                string strAttachments = "";
                string strScreenshots = "";

                if (Request.Params["IssueText"] == null)
                {
                    string errorStr = "<Result><ReturnCode>-1</ReturnCode><ResultDescription>IssueText is not specified</ResultDescription></Result>";
                    Response.Write(errorStr);
                    return;
                }

                if (Request.Params["IssueText"] != null)
                {
                    IssueText = Request.Params["IssueText"].ToString();
                }

                if (Request.Params["GUID"] != null)
                {
                    GUID = Request.Params["GUID"].ToString();
                }

                // Add an photo to issue from our asset db
                if (Request.Params["screenshots"] != null)
                {
                    strScreenshots = "\n\nScreenshots:\n";

                    string[] screenshots = (Request.Params["screenshots"].ToString()).Split('|');

                    for (int i = 0; i < screenshots.Length; i++)
                    {
                        if (screenshots[i].ToUpper().StartsWith ("HTTP://"))
                        {
                            strScreenshots += screenshots[i] + "\n";
                        }
                        else
                        {
                            strScreenshots += KanevaGlobals.ImageServer + "/" + screenshots[i] + "\n";
                        }
                    }
                }


                // Process uploaded attachements
                if (Request.Files.Count > 0)
                {
                    string filename ="";
                    string strFilePathAttachement = "";
                    HttpFileCollection hfc = Request.Files;

                    foreach (String h in hfc.AllKeys)
                    {
                        if (hfc[h].ContentLength > 0)
                        {
                            filename = Server.HtmlEncode(hfc[h].FileName);

                            // Danger code, only allow images, we don't want anyone getting .exes on our systems
                            // Make sure they are the correct file types!!!
                            string strExtensionUpperCase = Path.GetExtension (hfc[h].FileName).ToUpper ();

                            if (!strExtensionUpperCase.Equals (".JPG") && !strExtensionUpperCase.Equals (".JPEG"))
                            {
                                string errorStr = "<Result><ReturnCode>-4</ReturnCode><ResultDescription>Invalid filetype, only JPG or JPEG allowed</ResultDescription></Result>";
                                Response.Write(errorStr);
                                return;
                            }

                            // Save to image servers to include in the ticket
                            strFilePathAttachement = Path.Combine(KanevaGlobals.ImageServer, KanevaWebGlobals.CurrentUser.UserId.ToString());
                            ImageHelper.UploadImageFromUser(ref filename, hfc[h], strFilePathAttachement, KanevaGlobals.MaxUploadedImageSize);

                            // Image locations to include into to issue
                            strAttachments += strFilePathAttachement.Replace ("\\", "/") + "/" + filename;
                        }
                    }


                    

                }


                // Call Parature API Here
                string paratureURL = Configuration.ParatureAPIURL;
                string paratureToken = Configuration.ParatureAPIToken;
                string paratureCustomerId = "";

                WebClient client = new WebClient();
                client.Encoding = System.Text.Encoding.UTF8;
                string response = "";
                string xmlRequest = "";
                System.Text.UTF8Encoding objUTF8 = new System.Text.UTF8Encoding();
                byte[] postByteArray;
                byte[] responseArray;

                string userEmail = KanevaWebGlobals.CurrentUser.Email;

                if (userEmail.Length.Equals(0))
                {
                    userEmail = "customersupport@kaneva.com";
                }

                // Look up the user in parataure
                responseArray = client.DownloadData(paratureURL + "Customer?email=" + userEmail + "&" + paratureToken);

                string strResponse = Encoding.UTF8.GetString(responseArray);

                XmlDocument xmlResponse = new XmlDocument();

                string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                if (strResponse.StartsWith(_byteOrderMarkUtf8))
                {
                    strResponse = strResponse.Remove(0, _byteOrderMarkUtf8.Length);
                }

                xmlResponse.LoadXml(strResponse);

                XmlNodeList enlCustomer = xmlResponse.GetElementsByTagName("Customer");

                if (enlCustomer.Count > 0)
                {
                    XmlAttribute attId = enlCustomer[0].Attributes["id"];
                    paratureCustomerId = attId.Value;
                }

                // Did we find an existing user?
                if (paratureCustomerId.Length.Equals(0))
                {
                    // Create the user in parature
                    xmlRequest = "<Customer>" +
                        "<Email>" + KanevaWebGlobals.CurrentUser.Email + "</Email>" +
                        "<First_Name>" + KanevaWebGlobals.CurrentUser.FirstName + "</First_Name>" +
                        "<Last_Name>" + KanevaWebGlobals.CurrentUser.LastName + "</Last_Name>" +
                        "<Password>password</Password><Password_Confirm>password</Password_Confirm>" +
                        "<Sla> <Sla id=\"4857\"/> </Sla>" +
                        "<Status><Status id=\"2\"/></Status>" +
                        "<User_Name>" + KanevaWebGlobals.CurrentUser.Username + "</User_Name>" +
                        "</Customer>";

                    postByteArray = System.Text.Encoding.ASCII.GetBytes(xmlRequest);
                    responseArray = client.UploadData(paratureURL + "Customer?" + paratureToken, "POST", postByteArray);
                    response = objUTF8.GetString(responseArray).Trim();

                    xmlResponse = new XmlDocument();

                    strResponse = Encoding.UTF8.GetString(responseArray);

                    if (strResponse.StartsWith(_byteOrderMarkUtf8))
                    {
                        strResponse = strResponse.Remove(0, _byteOrderMarkUtf8.Length);
                    }

                    xmlResponse.LoadXml(strResponse);

                    enlCustomer = xmlResponse.GetElementsByTagName("Customer");

                    if (enlCustomer.Count > 0)
                    {
                        XmlAttribute attId = enlCustomer[0].Attributes["id"];
                        paratureCustomerId = attId.Value;
                    }
                }

                // Per JP, add user info
                if (Request.IsAuthenticated)
                {
                    IssueText = "Reported by Username " + KanevaWebGlobals.CurrentUser.Username + " with Email " + KanevaWebGlobals.CurrentUser.Email + "\n\n" + IssueText;
                }

                // Create the Ticket
                xmlRequest = "<Ticket>" +
                        "<Ticket_Customer>" +
                            "<Customer id=\"" + paratureCustomerId + "\"></Customer>" +
                        "</Ticket_Customer>" +
                        "<Custom_Field id=\"98090\"> <Option id=\"200726\" selected=\"true\" /> </Custom_Field>" +
                        "<Custom_Field id=\"98091\"> <Option id=\"200731\" selected=\"true\" /> </Custom_Field>" +
                        "<Custom_Field id=\"98099\">Reported Issue (" + GUID + ")</Custom_Field>" +
                        "<Custom_Field id=\"98100\">" + IssueText + strAttachments + strScreenshots + "</Custom_Field>" +
                    "</Ticket>";

                postByteArray = System.Text.Encoding.ASCII.GetBytes(xmlRequest);
                responseArray = client.UploadData(paratureURL + "Ticket?" + paratureToken, "POST", postByteArray);
                response = objUTF8.GetString(responseArray);


                // Posible errors
                //<Result><ReturnCode>-2</ReturnCode><ResultDescription>Unable to connect to Parature</ResultDescription></Result>
                //<Result><ReturnCode>-3</ReturnCode><ResultDescription>Unable to add issue to Parature</ResultDescription></Result>


                result = "Sucess";
                returnCode = 0;

                Response.Write("<Result><ReturnCode>" + returnCode + "</ReturnCode><ResultDescription>" + result + "</ResultDescription></Result>");
            }

            
        }
    }
}