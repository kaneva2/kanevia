///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class gameEditInfo : KgpBasePage
    {
        const string RESP_BAD_PARAM = "1";
        const string RESP_NOT_VALIDATED = "4";
        const string RESP_INTERNAL_ERROR = "5";

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
        {
            Int32 temp = 0;

            String resp = "<Result><ReturnCode>";
            resp += RESP_BAD_PARAM; // bad param
            resp += "</ReturnCode><ReturnDescription>Bad Parameter</ReturnDescription></Result>";

            bool userValidated = false;
            int userId = 0;

            Int32 gameId = 0;
            if (Request.Params["gameId"] != null &&
                 Int32.TryParse(Request.Params["gameId"], out temp))
            {
                gameId = temp;
            }

            if (Request.Params["user"] != null &&
                 Request.Params["pw"] != null &&
                 gameId != 0)
            {
                int roleMemberShip = 0;
                DataRow dr = UsersUtility.GetUserByEmail(Request.Params["user"]);

                if (dr != null)
                {
                    userId = Convert.ToInt32(dr["user_id"]);
                    // if they pass user(email)/pw then authenticate them
                    if (UsersUtility.Authorize(Request.Params["user"], Request["pw"], gameId, ref roleMemberShip, "", false) == (int)Constants.eLOGIN_RESULTS.SUCCESS)
                    {
                        userValidated = true;
                    }
                    else
                    {
                        resp = "<Result><ReturnCode>";
                        resp += RESP_NOT_VALIDATED;
                        resp += "</ReturnCode><ReturnDescription>Not Validated</ReturnDescription></Result>";
                        Response.Write(resp);
                        return;
                    }
                }
                else
                {
                    resp = "<Result><ReturnCode>";
                    resp += RESP_NOT_VALIDATED;
                    resp += "</ReturnCode><ReturnDescription>Not Validated</ReturnDescription></Result>";
                    Response.Write(resp);
                    return;
                }
            }

            if (userValidated || CheckUserId(true))
            {
                if (userId == 0)
                    userId = m_userId;

                String newGameName = Request.Params["name"];
                String newGameDesc = Request.Params["desc"];
                String sOwnerId = Request.Params["ownerId"];
                String sGameAccess = Request.Params["access"];
                String sGameStatus = Request.Params["status"];
                int newOwnerId = 0;
                if( sOwnerId!=null )
                {
                    if( !Int32.TryParse(sOwnerId, out newOwnerId) )
                    {
                        newOwnerId = 0;
                    }
                }

                eGAME_ACCESS newGameAccessId = eGAME_ACCESS.NOT_FOUND;
                if (sGameAccess!=null)
                {
                    if (!Enum.TryParse(sGameAccess, out newGameAccessId) || !Enum.IsDefined(typeof(eGAME_ACCESS), newGameAccessId) || newGameAccessId == eGAME_ACCESS.NOT_FOUND)
                    {
                        resp = "<Result><ReturnCode>";
                        resp += RESP_BAD_PARAM;
                        resp += "</ReturnCode><ReturnDescription>Invalid access parameter</ReturnDescription></Result>";
                        Response.Write(resp);
                        return;
                    }
                }

                eGAME_STATUS newGameStatusId = eGAME_STATUS.NOT_FOUND;
                if (sGameStatus != null)
                {
                    if (!Enum.TryParse(sGameStatus, out newGameStatusId) || !Enum.IsDefined(typeof(eGAME_STATUS), newGameStatusId) || newGameStatusId==eGAME_STATUS.NOT_FOUND)
                    {
                        resp = "<Result><ReturnCode>";
                        resp += RESP_BAD_PARAM;
                        resp += "</ReturnCode><ReturnDescription>Invalid status parameter</ReturnDescription></Result>";
                        Response.Write(resp);
                        return;
                    }
                }
                
                // must have some identifier for game and 
                if (gameId!=0)
                {
                    Game game = GetGameFacade.GetGameByGameId( gameId );

                    if (game != null && game.ParentGameId != 0 && (game.OwnerId == userId && userId != 0 || game.IsIncubatorHosted && Request.Params["user"] == KanevaGlobals.IncubationOwner))
                    {
                        // Option 1: Update game profile
                        if (newGameName != null && newGameDesc != null && newOwnerId != 0)
                        {
                            //Firstly, create developer account and company for the new owner, if not yet a developer

                            //Validate user
                            User gameOwner = GetUserFacade.GetUser(newOwnerId);
                            if (gameOwner.UserId <= 0)
                            {
                                resp = "<Result><ReturnCode>";
                                resp += RESP_BAD_PARAM;
                                resp += "</ReturnCode><ReturnDescription>Invalid owner parameter</ReturnDescription></Result>";
                                Response.Write(resp);
                                return;
                            }

                            //get the user's game developer information
                            //GetGameDeveloperByEmail may throw an exception so handle it
                            GameDeveloper gameDeveloper = null;
                            try
                            {
                                gameDeveloper = GetGameDeveloperFacade.GetGameDeveloperByEmail(gameOwner.Email);
                            }
                            catch (Exception) { }

                            //If user is not already registered on Developer and auto register them
                            if (gameDeveloper == null || gameDeveloper.UserId <= 0)
                            {
                                //create the default company for the new user
                                int companyId = GetDevelopmentCompanyFacade.CreateDefaultCompanyForUser(newOwnerId);

                                //if the company was successfully created add the game developer to the company, otherwise report an error
                                if (companyId <= 0)
                                {
                                    resp = "<Result><ReturnCode>";
                                    resp += RESP_INTERNAL_ERROR;
                                    resp += "</ReturnCode><ReturnDescription>Error creating company record for developer</ReturnDescription></Result>";
                                    Response.Write(resp);
                                    return;
                                }

                                //get the administrators role id - this potentional can throw exception
                                SiteRole role = new SiteRole();
                                try
                                {
                                    role = GetSiteSecurityFacade.GetCompanyRole(companyId, GameDeveloper.ADMIN_ROLEID);
                                }
                                catch (Exception) { }

                                if (role.RoleId <= 0)
                                {
                                    resp = "<Result><ReturnCode>";
                                    resp += RESP_INTERNAL_ERROR;
                                    resp += "</ReturnCode><ReturnDescription>Unable to determine new company's admin role</ReturnDescription></Result>";
                                    Response.Write(resp);
                                    return;
                                }

                                //set the values for the game developer
                                gameDeveloper = new GameDeveloper();
                                gameDeveloper.CompanyId = companyId;
                                gameDeveloper.DeveloperRoleId = role.SiteRoleId;
                                gameDeveloper.UserId = newOwnerId;

                                // add the game developer to the company
                                if (GetGameDeveloperFacade.InsertGameDeveloper(gameDeveloper) <= 0)
                                {
                                    resp = "<Result><ReturnCode>";
                                    resp += RESP_INTERNAL_ERROR;
                                    resp += "</ReturnCode><ReturnDescription>Unable to add new developer to the company.</ReturnDescription></Result>";
                                    Response.Write(resp);
                                    return;
                                }
                            }

                            //Now update game name and transfer ownership (SP would handle both game and community table)
                            game.OwnerId = newOwnerId;
                            game.GameName = newGameName;
                            game.GameDescription = newGameDesc;
                            game.GameKeyWords = newGameName;
                            game.GameCreationDate = DateTime.Now;
                            if (newGameAccessId != eGAME_ACCESS.NOT_FOUND)
                            {
                                game.GameAccessId = (int)newGameAccessId;
                            }
                            if (newGameStatusId != eGAME_STATUS.NOT_FOUND)
                            {
                                game.GameStatusId = (int)newGameStatusId;
                            }

                            int returnCode = 0;
                            string returnDesc = "Success";

                            try
                            {
                                GetGameFacade.UpdateGame(game);
                            }
                            catch (Exception ex)
                            {
                                //Catch any DB exception
                                returnCode = 1;
                                returnDesc = ex.ToString();
                            }

                            if (returnCode == 0)
                            {
                                //Add owner to community
                                int communityId = 0;
                                try
                                {
                                    communityId = GetCommunityFacade.GetCommunityIdFromGameId(gameId);
                                }
                                catch { }

                                if (communityId > 0)
                                {
                                    CommunityMember communityMember = GetCommunityFacade.GetCommunityMember(communityId, newOwnerId);
                                    if (communityMember.UserId != newOwnerId)
                                    {
                                        //not a memeber yet
                                        communityMember = new CommunityMember();
                                        communityMember.CommunityId = communityId;
                                        communityMember.UserId = newOwnerId;
                                        communityMember.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.OWNER;
                                        communityMember.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;
                                        communityMember.Newsletter = "Y";
                                        communityMember.InvitedByUserId = 0;
                                        communityMember.AllowAssetUploads = "Y";
                                        communityMember.AllowForumUse = "Y";
                                        GetCommunityFacade.InsertCommunityMember(communityMember);
                                    }
                                    else
                                    {
                                        m_logger.Warn("Unable to transfer ownership: new owner " + newOwnerId.ToString() + " is already a member of the community, Id" + communityId.ToString());
                                    }
                                }
                                else
                                {
                                    m_logger.Warn("Failed getting community Id from game Id " + gameId.ToString());
                                }

                                //Allow access
                                try
                                {
                                    if (!GetGameFacade.UserAllowedGame(gameId, newOwnerId))
                                    {
                                        m_logger.Warn("Failed adding new owner game access for game Id " + gameId.ToString());
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //Minor issue, just log it then return success
                                    m_logger.Warn("Exception occurred while adding new owner game access for game Id " + gameId.ToString() + ": " + ex.ToString());
                                }

                                //Remove Incubator Owner
                                if (game.IsIncubatorHosted && KanevaGlobals.IncubationOwner != null)
                                {
                                    User incubatorOwner = GetUserFacade.GetUserByEmail(KanevaGlobals.IncubationOwner);
                                    if (incubatorOwner != null)
                                    {
                                        CommunityUtility.UpdateCommunityMember(communityId, incubatorOwner.UserId, CommunityMember.CommunityMemberAccountType.SUBSCRIBER);
                                        GetCommunityFacade.UpdateCommunityMember(communityId, incubatorOwner.UserId, (UInt32)CommunityMember.CommunityMemberStatus.DELETED);
                                    }
                                }
                            }

                            resp = "<Result>";
                            resp += "<ReturnCode>" + returnCode.ToString() + "</ReturnCode>";
                            resp += "<ReturnDescription>" + returnDesc + "</ReturnDescription>";
                            resp += "</Result>";
                        }
                        // Option 2: Update game flags
                        else if (sGameAccess != null || sGameStatus != null)
                        {
                            //Advanced settings: only accessible by incubator owner
                            if (KanevaGlobals.IncubationOwner != null && Request.Params["user"] == KanevaGlobals.IncubationOwner)
                            {
                                // By allowing incubator to change these two flags, we can simply scrap a 3dapp by making it inaccessible. 
                                // This will prevent such scrapped app from being launched again on incubator. - YC 04242012

                                bool changed = false;
                                if (newGameAccessId != eGAME_ACCESS.NOT_FOUND && (int)newGameAccessId != game.GameAccessId)
                                {
                                    game.GameAccessId = (int)newGameAccessId;
                                    changed = true;
                                }

                                if (newGameStatusId != eGAME_STATUS.NOT_FOUND && (int)newGameStatusId != game.GameStatusId)
                                {
                                    game.GameStatusId = (int)newGameStatusId;
                                    changed = true;
                                }

                                int returnCode = 0;
                                string returnDesc = "Success";

                                if (changed)
                                {
                                    try
                                    {
                                        GetGameFacade.UpdateGame(game);
                                    }
                                    catch (Exception ex)
                                    {
                                        //Catch any DB exception
                                        returnCode = 1;
                                        returnDesc = ex.ToString();
                                    }
                                }

                                resp = "<Result>";
                                resp += "<ReturnCode>" + returnCode.ToString() + "</ReturnCode>";
                                resp += "<ReturnDescription>" + returnDesc + "</ReturnDescription>";
                                resp += "</Result>";
                            }
                        }
                    }
                }
            }
            else
            {
                resp = "<Result><ReturnCode>";
                resp += RESP_NOT_VALIDATED;
                resp += "</ReturnCode><ReturnDescription>Not Validated</ReturnDescription></Result>";
            }
            Response.Write(resp);
        }
    }
}