///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.IO;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

using log4net;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class ugcLink : KgpBasePage
    {
        private const string INTERNAL_ERROR = "-1";
        private const string UNKNOWN_TYPE = "-2";
        private const string VALIDATION_FAILED = "-3";
		private const string MISSING_METADATA = "-4";
        private const string TOO_MANY_FILES = "9999";
        private const string NO_UPLOADS = "0";
        private const string SUCCEEDED = "1";

        private const string PARAM_ACTION = "action";
        private const string PARAM_UPLOADID = "uploadid";
        private const string PARAM_UPLOADTYPE = "uploadtype";
        private const string PARAM_UPLOADSIZE = "uploadsize";
        private const string PARAM_MASTERID = "masterid";
        private const string PARAM_SUBID = "subid";
        private const string PARAM_ORIGNAME = "origname";
        private const string PARAM_PROPS = "props";
        private const string PARAM_SUMMARY = "summary";
        private const string PARAM_BASE_GLID = "baseglid";
		private const string PARAM_ORIGSIZE = "origsize";
		private const string PARAM_ORIGHASH = "orighash";

        private const string XML_TAG_RETURNCODE = "ReturnCode";
        private const string XML_TAG_UPLOADID = "UploadId";
        private const string XML_TAG_GLOBALID = "GlobalId";
        private const string XML_TAG_BASEPRICE = "BasePrice";
        private const string XML_TAG_STATE = "State";
        private const string XML_TAG_VALIDATIONMSG = "ValidationMessage";
        private const string XML_TAG_VALIDATIONLOG = "ValidationLog";
        private const string XML_TAG_ASSETID = "AssetId";
		private const string XML_TAG_TEXTUREURL = "TextureUrl";
		private const string XML_TAG_OPTIONS = "UGCOptions";
		private const string XML_TAG_OPTION = "Option";
		private const string XML_TAG_OPTIONNAME = "OptionName";
		private const string XML_TAG_OPTIONVALUE = "OptionValue";

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest( context );
        }

        protected void Page_Load(object sender, EventArgs e)
        {
			if (!CheckUserId(false))
				return;

			if (Request.Params[PARAM_ACTION] == null && Request.Form[PARAM_ACTION] == null)
			{
				Response.Write(EncodeXMLElement(XML_TAG_RETURNCODE, INTERNAL_ERROR));
				return;
			}

            string sAction, sUploadId, sUploadType, sUploadSize, sMasterId, sSubId, sProps;

            if (Request.Params[PARAM_ACTION] != null)
                sAction = Request.Params[PARAM_ACTION];
            else
                sAction = Request.Form[PARAM_ACTION];

			if (sAction=="config")
			{
				// config client UGC module with settings from DB
				string configXml = ProcessConfigRequest();
				if (configXml==null)
					Response.Write(EncodeXMLElement(XML_TAG_RETURNCODE, INTERNAL_ERROR));
				else
					Response.Write(configXml);
				return;
			}

			if (Request.Params[PARAM_UPLOADTYPE] != null)
				sUploadType = Request.Params[PARAM_UPLOADTYPE];
			else
				sUploadType = Request.Form[PARAM_UPLOADTYPE];

			if (sUploadType == null)	// Must provide an upload type unless "config"
			{
				Response.Write(EncodeXMLElement(XML_TAG_RETURNCODE, INTERNAL_ERROR));
				return;
			}

			string response = EncodeXMLElement(XML_TAG_RETURNCODE, NO_UPLOADS);
			UGCUpload.eType uploadType = UGCUpload.ParseUploadType(sUploadType);
			int uploadId = -1;

			if (sAction == "new")
			{
				//////////////////////////////////////////////
				// New item creation

				// master upload ID: optional
				if (Request.Params[PARAM_MASTERID] != null)
					sMasterId = Request.Params[PARAM_MASTERID];
				else
					sMasterId = Request.Form[PARAM_MASTERID];

				int masterId = -1;
				if (sMasterId != null)
				{
					try
					{
						masterId = Convert.ToInt32(sMasterId);
					}
					catch (System.Exception) { }
				}

				if (Request.Params[PARAM_SUBID] != null)
					sSubId = Request.Params[PARAM_SUBID];
				else
					sSubId = Request.Form[PARAM_SUBID];

				UInt32 subId = UGCUtility.INVALID_SUB_ID;
				if (sSubId != null)
				{
					try
					{
						subId = Convert.ToUInt32(sSubId);
					}
					catch (System.Exception) { }
				}

				if (Request.Params[PARAM_PROPS] != null)
					sProps = Request.Params[PARAM_PROPS];
				else
					sProps = Request.Form[PARAM_PROPS];

				Dictionary<int, string> properties = new Dictionary<int, string>();
				if (sProps != null)
				{
					DecodeProperties(sProps, ref properties);
				}

				if (Request.Params[PARAM_UPLOADSIZE] != null)
					sUploadSize = Request.Params[PARAM_UPLOADSIZE];
				else
					sUploadSize = Request.Form[PARAM_UPLOADSIZE];

				int uploadSize = 0;
				if (sUploadSize != null)
				{
					try
					{
						uploadSize = Convert.ToInt32(sUploadSize);
						if (uploadSize < 0)
							uploadSize = 0;
					}
					catch (System.Exception) { }
				}

				string uploadSummary = "";
				if (Request.Params[PARAM_SUMMARY] != null)
					uploadSummary = Request.Params[PARAM_SUMMARY];
				else if (Request.Form[PARAM_SUMMARY] != null)
					uploadSummary = Request.Form[PARAM_SUMMARY];

				string msg = null;
				string msgDetail = null;
				float basePrice = 1000000000.0f;

				if (UGCUtility.PrepareUGCUpload(GetUserId(), uploadType, masterId, subId, properties, uploadSize, uploadSummary, ref uploadId, ref basePrice, ref msg, ref msgDetail))
				{
					response = EncodeXMLElement(XML_TAG_RETURNCODE, SUCCEEDED);
					response = response + EncodeXMLElement(XML_TAG_UPLOADID, uploadId.ToString());
					response = response + EncodeXMLElement(XML_TAG_BASEPRICE, basePrice.ToString("F"));
					response = response + EncodeXMLElement(XML_TAG_VALIDATIONMSG, msg == null ? "" : msg);
					response = response + EncodeXMLElement(XML_TAG_VALIDATIONLOG, msgDetail == null ? "" : msgDetail);
				}
				else
				{
					response = EncodeXMLElement(XML_TAG_RETURNCODE, VALIDATION_FAILED);
					response = response + EncodeXMLElement(XML_TAG_VALIDATIONMSG, msg == null ? "" : msg);
					response = response + EncodeXMLElement(XML_TAG_VALIDATIONLOG, msgDetail == null ? "" : msgDetail);
				}
			}
			else if (sAction == "derive")
			{
				////////////////////////////////////////
				// Item Derivation

				string sBaseGlid;

				if (Request.Params[PARAM_BASE_GLID] != null)
					sBaseGlid = Request.Params[PARAM_BASE_GLID];
				else
					sBaseGlid = Request.Form[PARAM_BASE_GLID];

				int baseGlid = -1;
				try
				{
					baseGlid = Convert.ToInt32(sBaseGlid);
				}
				catch (System.Exception ex)
				{
					baseGlid = -1;
					m_logger.Warn("Derive request with bad base GLID parameter: " + sBaseGlid, ex);
					response = EncodeXMLElement(XML_TAG_RETURNCODE, INTERNAL_ERROR);
				}

				// Get original name
				string originalName = "";
				if (Request.Params[PARAM_ORIGNAME] != null)
					originalName = Request.Params[PARAM_ORIGNAME];
				else if (Request.Form[PARAM_ORIGNAME] != null)
					originalName = Request.Form[PARAM_ORIGNAME];

				if (baseGlid != -1)
				{
					if (Request.Params[PARAM_PROPS] != null)
						sProps = Request.Params[PARAM_PROPS];
					else
						sProps = Request.Form[PARAM_PROPS];

					Dictionary<int, string> properties = new Dictionary<int, string>();
					if (sProps != null)
					{
						DecodeProperties(sProps, ref properties);
					}

					string msg = null;
					string msgDetail = null;
					float basePrice = 1000000000.0f;
					Int64[] subIds = null;

					if (UGCUtility.PrepareUGCDerivation(GetUserId(), uploadType, baseGlid, properties, ref uploadId, ref basePrice, ref msg, ref msgDetail, ref subIds))
					{
						// Set original name
						UGCUtility.SetUGCOriginalName(GetUserId(), uploadType, uploadId, originalName);

						response = EncodeXMLElement(XML_TAG_RETURNCODE, SUCCEEDED);
						response = response + EncodeXMLElement(XML_TAG_UPLOADID, uploadId.ToString());
						response = response + EncodeXMLElement(XML_TAG_BASEPRICE, basePrice.ToString("F"));
						response = response + EncodeXMLElement(XML_TAG_VALIDATIONMSG, msg == null ? "" : msg);
						response = response + EncodeXMLElement(XML_TAG_VALIDATIONLOG, msgDetail == null ? "" : msgDetail);
					}
					else
					{
						response = EncodeXMLElement(XML_TAG_RETURNCODE, VALIDATION_FAILED);
						response = response + EncodeXMLElement(XML_TAG_VALIDATIONMSG, msg == null ? "" : msg);
						response = response + EncodeXMLElement(XML_TAG_VALIDATIONLOG, msgDetail == null ? "" : msgDetail);
					}
				}
			}
			else
			{
				//////////////////////////////////////////////
				// Misc requests: data, cancel, query, deploy, ...

				sUploadId = "";

				if (Request.Params[PARAM_UPLOADID] == null && Request.Form[PARAM_UPLOADID] == null)
				{
					response = EncodeXMLElement(XML_TAG_RETURNCODE, INTERNAL_ERROR);
				}
				else
				{
					if (Request.Params[PARAM_UPLOADTYPE] != null)
						sUploadId = Request.Params[PARAM_UPLOADID];
					else
						sUploadId = Request.Form[PARAM_UPLOADID];

					try
					{
						uploadId = Convert.ToInt32(sUploadId);
					}
					catch (System.Exception ex)
					{
						uploadId = -1;
						m_logger.Warn(sAction + " request with bad upload ID parameter: " + sUploadId, ex);
						response = EncodeXMLElement(XML_TAG_RETURNCODE, INTERNAL_ERROR);
					}
				}

				if (uploadId != -1)
				{
					if (sAction == "data")
					{
						if (Request.Files.Count == 0)
						{
							response = EncodeXMLElement(XML_TAG_RETURNCODE, NO_UPLOADS);
						}
						else if (Request.Files.Count > 99)
						{
							response = EncodeXMLElement(XML_TAG_RETURNCODE, TOO_MANY_FILES);
						}
						else
						{
							bool ret = false;
							response = EncodeXMLElement(XML_TAG_RETURNCODE, SUCCEEDED);

							try
							{
								// Get original name
								string originalName = Request.Files[0].FileName;
								// Check if there is any overrides
								if (Request.Params[PARAM_ORIGNAME] != null)
									originalName = Request.Params[PARAM_ORIGNAME];
								else if (Request.Form[PARAM_ORIGNAME] != null)
									originalName = Request.Form[PARAM_ORIGNAME];

								string originalHash = null;
								if (Request.Params[PARAM_ORIGHASH] != null)
									originalHash = Request.Params[PARAM_ORIGHASH];
								else if (Request.Form[PARAM_ORIGHASH] != null)
									originalHash = Request.Form[PARAM_ORIGHASH];

								int originalSize = 0;
								try
								{
									if (Request.Params[PARAM_ORIGSIZE] != null)
										originalSize = Convert.ToInt32(Request.Params[PARAM_ORIGSIZE]);
									else if (Request.Form[PARAM_ORIGSIZE] != null)
										originalSize = Convert.ToInt32(Request.Form[PARAM_ORIGSIZE]);
								}
								catch (Exception) { }

								if (UGCUtility.IsSupportedDataUploadType(uploadType))
									ret = UGCUtility.ProcessUGCUpload(GetUserId(), uploadType, uploadId, Request.Files, originalName, originalHash, originalSize);
								else
									response = EncodeXMLElement(XML_TAG_RETURNCODE, UNKNOWN_TYPE);
							}
							catch (System.Exception ex)
							{
								m_logger.Error("Process UGC upload failed", ex);
								ret = false;
							}

							if (!ret)
								response = EncodeXMLElement(XML_TAG_RETURNCODE, INTERNAL_ERROR);
						}
					}
					else if (sAction == "cancel")
					{
						if (UGCUtility.CancelUGCUpload(GetUserId(), uploadType, uploadId))
							response = EncodeXMLElement(XML_TAG_RETURNCODE, SUCCEEDED);
						else
							response = EncodeXMLElement(XML_TAG_RETURNCODE, INTERNAL_ERROR);
					}
					else if (sAction == "query")
					{
						DataRow dr = UGCUtility.GetUGCUpload(GetUserId(), uploadType, uploadId);
						if (dr != null)
						{
							if (dr["state"] != null)
							{
								response = EncodeXMLElement(XML_TAG_RETURNCODE, SUCCEEDED);

								string sState = Convert.ToString(dr["state"]);
								if (sState != null)
								{
									response = response + EncodeXMLElement(XML_TAG_UPLOADID, sUploadId) + EncodeXMLElement(XML_TAG_STATE, sState);
								}
                                UGCUpload.eState state = UGCUpload.eState.FAILED;
								try
								{
									state = (UGCUpload.eState)Convert.ToInt32(sState);
								}
								catch (System.Exception) { }

								if (state == UGCUpload.eState.COMPLETED && dr["global_id"] != null)
								{
									response = response + EncodeXMLElement(XML_TAG_GLOBALID, Convert.ToString(dr["global_id"]));
								}
							}
							else
							{
								response = EncodeXMLElement(XML_TAG_RETURNCODE, INTERNAL_ERROR);
							}
						}
						else
						{
							response = EncodeXMLElement(XML_TAG_RETURNCODE, NO_UPLOADS);
						}
					}
					else if (sAction == "deploy")
					{
						bool ret = false;
						response = EncodeXMLElement(XML_TAG_RETURNCODE, SUCCEEDED);

						switch (uploadType)
						{
							case UGCUpload.eType.MEDIA:
								int assetId;
								string textureUrl;
								ret = UGCUtility.DeployUploadedMedia(GetUserId(), GetPersonalChannelId(), uploadType, uploadId, Common.GetVisitorIPAddress(), out assetId, out textureUrl);
								if (ret)
									response = response + EncodeXMLElement(XML_TAG_ASSETID, assetId.ToString()) + EncodeXMLElement(XML_TAG_TEXTUREURL, textureUrl.ToString());
								break;

							default:
								response = EncodeXMLElement(XML_TAG_RETURNCODE, UNKNOWN_TYPE);
								break;
						}

						if (!ret)
							response = EncodeXMLElement(XML_TAG_RETURNCODE, INTERNAL_ERROR);
					}
				}
            }

			Response.Write(response);
		}

        protected void DecodeProperties( string propStr, ref Dictionary<int, string> properties )
        {
            string[] propertyItems = propStr.Split(',');
            foreach (string s in propertyItems)
            {
                string[] propDef = s.Split('!');
                if( propDef.Length==2 )
                {
                    try
                    {
                        int propId = Convert.ToInt32(propDef[0]);
                        string propVal = propDef[1];
                        properties.Add(propId, propVal);
                    }
                    catch (System.Exception) {}
                }
            }
        }

        protected string EncodeXMLElement( string tag, string value )
        {
            return "<" + tag + ">" + value + "</" + tag + ">";
        }

		protected string ProcessConfigRequest()
		{
            User user = (User)HttpContext.Current.Items["User"];
			if (user == null)
				return null;

			Dictionary<string, OptionValueMap> result = Configuration.GetCachedWebOptionsByGroup("UGC-Client", user.Role, true);
			if (result == null)
				return null;
			
			string xml = EncodeXMLElement(XML_TAG_RETURNCODE, SUCCEEDED);

			string optionsXml = "";
			int index = 0;
			foreach (KeyValuePair<string, OptionValueMap> pair in result)
			{
				string optionElementText = EncodeXMLElement(XML_TAG_OPTIONNAME, pair.Key) + EncodeXMLElement(XML_TAG_OPTIONVALUE, pair.Value.String);
				optionsXml = optionsXml + EncodeXMLElement(XML_TAG_OPTION, optionElementText);
				index ++;
			}

			xml = xml + EncodeXMLElement(XML_TAG_OPTIONS, optionsXml);

			return xml;
		}
    }
}
