///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Xml;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;

namespace KlausEnt.KEP.Kaneva.kgp
{
	 public enum GetWokUserInventorySorts
	 {
		 alfa = 1, 
		 beta = 2,
		 gamma = 3
	 }

	/// <summary>
	/// Summary description for inventory.
	/// </summary>
	public class inventory : KgpBasePage
    {
        #region Declarations

        XmlNode root = null;
        XmlElement elem = null;
        XmlDocument doc = null;

        const string SUCCESS = "0";
        const string ERROR = "1";

        #endregion

		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
				if ( (Request.Params["start"] == null) || (Request.Params["max"] == null) )
				{
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>page number and items per page not specified</ResultDescription>\r\n" + PassthroughTag + "</Result>";
					Response.Write(errorStr);
					return;
				}

				int page = Int32.Parse(Request.Params["start"].ToString());
				int itemsPerPage = Int32.Parse(Request.Params["max"].ToString());
				int playerId = m_playerId;
                int userId = m_userId;

				string orderByClause = "inv.created_date DESC";
                bool avatarInventory = false;
                bool buildingInventory = false;
                bool gamingInventory = false;
                string wokInventoryType = Constants.WOK_INVENTORY_TYPE_PERSONAL;
                List<string> sortGlids = new List<string>();

                if (!string.IsNullOrWhiteSpace(Request.Params["wokInventoryType"]))
                {
                    wokInventoryType = Request.Params["wokInventoryType"];
                }

   				if ( (Request.Params["type"] != null) )
				{
    				string type = Request.Params["type"];

                    switch (type)
                    {
                        case "avatar":
                            avatarInventory = true;
                            break;

                        case "building":
                            buildingInventory = true;
                            break;

                        case "gaming":
                            gamingInventory = true;
                            break;

                        default:
                            {
                                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invalid type specified. avatar or building needed.</ResultDescription>\r\n" + PassthroughTag + "</Result>";
                                Response.Write(errorStr);
                                return;
                            }
                    }
				}

                if ( (Request.Params["sort"] != null) )
                {
                    // Prevalidate sort order requested.

    				string sort = Request.Params["sort"];

                    switch (sort)
                    {
                        case "new":
                            if ( avatarInventory || buildingInventory || gamingInventory)
                            {
                                orderByClause = "inv.armed ASC, COALESCE(inv.last_touch_datetime, inv.created_date) DESC, inv.created_date DESC";
                            }
                            else
                            {
                                orderByClause = "inv.created_date DESC";
                            }
                            break;

                        case "old":
                            if ( avatarInventory || buildingInventory || gamingInventory)
                            {
                                orderByClause = "inv.armed ASC, inv.last_touch_datetime ASC, inv.created_date ASC";
                            }
                            else
                            {
                                orderByClause = "inv.created_date ASC";
                            }
                            break;

                        case "manual":
                            if ((Request.Params["sort_glids"] != null))
                            {
                                string[] unsortedGlids = (Request.Params["sort_glids"].ToString()).Split('|');
                                foreach (string sortGlid in unsortedGlids)
                                {
                                    int iout;
                                    if (!Int32.TryParse(sortGlid, out iout))
                                    {
                                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invalid sort glids specified</ResultDescription>\r\n" + PassthroughTag + "</Result>";
                                        Response.Write(errorStr);
                                        return;
                                    }
                                }
                                sortGlids.AddRange(unsortedGlids);
                                sortGlids.Reverse();
                            }

                            if (avatarInventory || buildingInventory || gamingInventory)
                            {
                                orderByClause = "COALESCE(inv.last_touch_datetime, inv.created_date) DESC, inv.created_date DESC";
                            }
                            else
                            {
                                orderByClause = "inv.created_date DESC";
                            }

                            if (sortGlids.Count > 0)
                            {
                                orderByClause = "FIELD(inv.global_id, " + string.Join(", ", sortGlids) + ") DESC, " + orderByClause;
                            }
                            break;

                        default:
                            {
                                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invalid sort type specified</ResultDescription>\r\n" + PassthroughTag + "</Result>";
                                Response.Write(errorStr);
                                return;
                            }
                    }
				}

                string searchString = "";

                if ( (Request.Params["search"] != null) )
                {
    				searchString = Request.Params["search"];
                }

                string category = "";

                if ( (Request.Params["category"] != null) )
                {
    				category = Request.Params["category"];
                }

                List<ItemCategory> itemCats = new List<ItemCategory>();
                if (!string.IsNullOrWhiteSpace(Request.Params["categoryPairs"]))
                {
                    string[] tuples = Request.Params["categoryPairs"].Split(new char[] { '|' });
                    char[] pairDelimiter = new char[] { ',' };
                    List<KeyValuePair<string, string>> pairs = new List<KeyValuePair<string, string>>();

                    foreach (string tuple in tuples)
                    {
                        string[] tupleParts = tuple.Split(pairDelimiter, StringSplitOptions.RemoveEmptyEntries);
                        if (tupleParts.Length > 0 && tupleParts.Length <= 2)
                        {
                            pairs.Add(new KeyValuePair<string, string>(tupleParts[0], tupleParts[1]));
                        }
                        else
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>categoryPairs incorrectly formatted.</ResultDescription>\r\n" + PassthroughTag + "</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    }

                    itemCats = GetShoppingFacade.GetItemCategories(pairs);
                    if (itemCats == null || itemCats.Count != pairs.Count || !string.IsNullOrWhiteSpace(category))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invalid item category.</ResultDescription>\r\n" + PassthroughTag + "</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }

                bool gifts = false;

                if ( (Request.Params["gifts"] != null) )
                {
    				if ( Request.Params["gifts"] == "true" )
                    {
                        gifts = true;
                    }
                }

                bool canTrade = false; 
                
                if ((Request.Params["can_trade"] != null))
                {
                    if (Request.Params["can_trade"] == "true")
                    {
                        canTrade = true;
                    }
                }

                bool mature = false;

                if ( (Request.Params["mature"] != null) )
                {
    				if ( Request.Params["mature"] == "true" )
                    {
                        mature = true;
                    }
                }

                bool categories = false;
                int glid = 0;

                if ( (Request.Params["categories"] != null) )
                {
    				if ( Request.Params["categories"] == "true" )
                    {
                        categories = true;

                        if ( (Request.Params["glid"] != null) )
                        {
                            glid = Int32.Parse(Request.Params["glid"].ToString());
                            orderByClause = "ic.parent_category_id ASC";
                        }
                        else
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Categories require specification of a global id. categories=true&glid=n.</ResultDescription>\r\n" + PassthroughTag + "</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    }
                }

                bool quantities= false;
                string[] glids = new string[0];

                if ((Request.Params["quantities"] != null))
                {
                    if (Request.Params["quantities"] == "true")
                    {
                        quantities = true;

                        if ((Request.Params["glids"] != null))
                        {
                            glids = (Request.Params["glids"].ToString()).Split('|');
                            orderByClause = "inv.global_id ASC";
                        }
                        else
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Quantities requires specification of one or more glids, delimited by |. quantities=true&glids=n.</ResultDescription>\r\n" + PassthroughTag + "</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    }
                }

                List<int> useTypes = new List<int>();
                if (!string.IsNullOrWhiteSpace(Request.Params["use_type"]))
                {
                    string[] arrUseTypes = (Request.Params["use_type"]).Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                    int iOut;
                    foreach (string useType in arrUseTypes)
                    {
                        if (Int32.TryParse(useType, out iOut))
                        {
                            useTypes.Add(iOut);
                        }
                    }
                }

                List<ScriptGameItemCategory> gameItemTypes = new List<ScriptGameItemCategory>();

                if ((Request.Params["gameItemTypes"] != null))
                {
                    // gameItemTypes is passed as a series of pipe-delimited tuples, each of which is comprised of 1 or two comma-delimited values.               

                    string[] giTypeTuples = (Request.Params["gameItemTypes"].ToString()).Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                    char[] tupleDelimiter = new char[] { ',' };

                    foreach(string tuple in giTypeTuples)
                    {
                        string[] tupleParts = tuple.Split(tupleDelimiter, StringSplitOptions.RemoveEmptyEntries);

                        if(tupleParts.Length > 0 && tupleParts.Length <= 2)
                        {
                            gameItemTypes.Add(new ScriptGameItemCategory(tupleParts[0], (tupleParts.Length > 1 ? tupleParts[1] : null)));
                        }
                        else
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>gameItemTypes is incorrectly formatted.</ResultDescription>\r\n" + PassthroughTag + "</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    }
                }

                bool countGINameUse = (Request.Params["countGINameUse"] ?? "false").Equals("true");

				DataSet ds = new DataSet();

				ds.DataSetName = "Result";

				PagedDataTable pdt = null;

                if (Request.Params["action"] == "Player")
                {
                    if ( categories && glid != 0 )
                    {
                        pdt = UsersUtility.GetWokUserItemCategories (playerId, glid, orderByClause, page, itemsPerPage);
                    }
                    else if ( quantities && glids.Length > 0 )
                    {
                        pdt = UsersUtility.GetWokUserItemQuantities(playerId, glids, orderByClause, page, itemsPerPage);
                    }
                    else if ( avatarInventory && !gifts) // ignore if gifts specified
                    {
                        pdt = UsersUtility.GetWokUserAvatarInventory (playerId, true, searchString, wokInventoryType, category, itemCats, mature, orderByClause, page, itemsPerPage);
                    }
                    else if ( buildingInventory && !gifts) // ignore if gifts specified
                    {
                        pdt = UsersUtility.GetWokUserBuildingInventory (playerId, true, searchString, wokInventoryType, category, mature, orderByClause, page, itemsPerPage);
                    }
                    else if (useTypes.Count > 0)
                    {
                        // Special case for script game items here. They require different result data than normal inventory calls.
                        if (useTypes.Count == 1 && useTypes[0] == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
                        {
                            if (countGINameUse)
                            {
                                string suggestedName = GetScriptGameItemFacade.GetSuggestedGINameSuffixNum(playerId, wokInventoryType, searchString, gameItemTypes);
                                Response.Write(string.Format("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n  <SuggestedName>{0}</SuggestedName>\r\n </Result>", suggestedName));
                            }
                            else
                            {
                                PagedList<ScriptGameItem> scriptGameItems = GetScriptGameItemFacade.GetInventoryScriptGameItems(playerId, wokInventoryType, searchString, gameItemTypes,
                                    orderByClause, page, itemsPerPage, true);

                                WriteScriptGameItemResponseXml(scriptGameItems, page, searchString);
                            }

                            return;
                        }
                        else
                        {
                            pdt = UsersUtility.GetWokUserInventoryByUseType(playerId, false, useTypes.ToArray(), searchString, wokInventoryType, category, mature, orderByClause, page, itemsPerPage);
                        }
                    }
                    else if ( gifts)
                    {
                        pdt = UsersUtility.GetWokUserGiftInventory (playerId, true, wokInventoryType, orderByClause, page, itemsPerPage);
                    }
                    else if (canTrade)
                    {
                        pdt = UsersUtility.GetWokUserInventoryCanTrade (playerId, true, searchString, wokInventoryType, category, mature, gifts, orderByClause, page, itemsPerPage);
                    }
                    else
                    {
                        pdt = UsersUtility.GetWokUserInventory (playerId, true, searchString, wokInventoryType, category, mature, gifts, orderByClause, page, itemsPerPage);
                    }
                }
                else if (Request.Params["action"] == "Bank")
                {
       				pdt = UsersUtility.GetWokUserInventory (playerId, true, searchString, Constants.WOK_INVENTORY_TYPE_BANK, category, mature, gifts, orderByClause, page, itemsPerPage);
                }
                else if (Request.Params["action"] == "add")
                {
                    glid = -1;

                    if ( Request.Params["glid"] != null)
                    {
                        glid = Convert.ToInt32(Request.Params["glid"]);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Action type of add requires corresponding glid specification</ResultDescription>\r\n" + PassthroughTag + "</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int inventoryType = 256;

                    if ( Request.Params["inventorySubType"] != null)
                    {
                        inventoryType = Convert.ToInt32(Request.Params["inventorySubType"]);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Action type of add requires corresponding inventoryType specification</ResultDescription>\r\n" + PassthroughTag + "</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int gameId = 0;

                    if ( Request.Params["gameId"] != null)
                    {
                        gameId = Convert.ToInt32(Request.Params["gameId"]);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Action type of add requires corresponding gameId specification</ResultDescription>\r\n" + PassthroughTag + "</Result>";
                        Response.Write(errorStr);
                        return;
                    }

       				UsersUtility.AddWokUserInventoryIfNew(gameId, userId, playerId, glid, inventoryType);

                    root = null;

                    //generate the XML out put
                    doc = new XmlDocument ();
                    doc.LoadXml (ds.GetXml ());
                    root = doc.DocumentElement;

                    InsertPassthroughTag(ref doc);

                    elem = doc.CreateElement("ReturnDescription");

                    elem.InnerText = "success";
                    root.InsertBefore (elem, root.FirstChild);

                    elem = doc.CreateElement ("ReturnCode");
                    elem.InnerText = SUCCESS;

                    root.InsertBefore (elem, root.FirstChild);

                    Response.Write(root.OuterXml);

                    return;
                }
                else
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Incorrect inventory type specified</ResultDescription>\r\n" + PassthroughTag + "</Result>";
                    Response.Write(errorStr);
                    return;
                }
                
				pdt.TableName = "Inventory";
				ds.Tables.Add(pdt);

				doc = new XmlDocument();
				doc.LoadXml(ds.GetXml());
				root = doc.DocumentElement;

				int numRecords = pdt.Rows.Count;
				elem = doc.CreateElement("NumberRecords");
				elem.InnerText= numRecords.ToString();

				root.InsertBefore( elem, root.FirstChild);

				int totalNumRecords = pdt.TotalCount;
				elem = doc.CreateElement("TotalNumberRecords");
				elem.InnerText= totalNumRecords.ToString();

				root.InsertBefore( elem, root.FirstChild);

                elem = doc.CreateElement("Page");
                elem.InnerText = page.ToString();

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("Search");
                elem.InnerText = searchString.ToString();

                root.InsertBefore(elem, root.FirstChild);

                InsertPassthroughTag(ref doc);

				elem = doc.CreateElement("ReturnDescription");
				elem.InnerText="success";

				root.InsertBefore( elem, root.FirstChild);

				elem = doc.CreateElement("ReturnCode");
				elem.InnerText="0";

				root.InsertBefore( elem, root.FirstChild);

				// Retrieve any pass lists for the retrieved set of glids
                string[] glidSet = new string[pdt.Rows.Count];
                
                for (int ii = 0; ii < pdt.Rows.Count; ii++)
                {
                    glidSet[ii] = pdt.Rows[ii]["global_id"].ToString();
                }
         
				DataTable dt = UsersUtility.GetPassGroupsForGlids(glidSet);

                if (dt != null)
                {
                    // Insert the pass group ids into the appropriate glid section of the doc
                    for (int ii = 0; ii < dt.Rows.Count; ii++)
                    {
                        string passGlid = dt.Rows[ii]["global_id"].ToString();
                        string passGroupId = dt.Rows[ii]["pass_group_id"].ToString();
                        string passGroupName = dt.Rows[ii]["name"].ToString();

                        // XPath to specific glid
                        XmlNode guidNode = root.SelectSingleNode("//Result/Inventory/global_id[.  ='" + passGlid + "']");

                        XmlElement passElement = doc.CreateElement("Pass");
                        XmlNode passNode = guidNode.ParentNode.InsertAfter(passElement, guidNode.ParentNode.LastChild);

                        XmlElement passId = doc.CreateElement("ID");
                        passId.InnerText = passGroupId;
                        passNode.AppendChild(passId);

                        XmlElement passName = doc.CreateElement("Name");
                        passName.InnerText = passGroupName;
                        passNode.AppendChild(passName);
                    }
                }

				Response.Write(root.OuterXml);
			}
		}

        /// <summary>
        /// Helper function used to write the response xml for script game items in inventory
        /// </summary>
        private void WriteScriptGameItemResponseXml(PagedList<ScriptGameItem> gameItems, int page, string searchString)
        {
            doc = new XmlDocument();
            root = doc.CreateElement("Result");
            doc.AppendChild(root);

            foreach (ScriptGameItem item in gameItems)
            {
                elem = doc.CreateElement("ScriptGameItem");
                elem.InnerXml += string.Format("<game_item_glid>{0}</game_item_glid>", item.GIGlid);
                elem.InnerXml += string.Format("<glid>{0}</glid>", item.Glid);
                elem.InnerXml += string.Format("<name>{0}</name>", item.Name);
                elem.InnerXml += string.Format("<item_type>{0}</item_type>", item.ItemType);
                elem.InnerXml += string.Format("<level>{0}</level>", item.Level);
                elem.InnerXml += string.Format("<rarity>{0}</rarity>", item.Rarity);
                elem.InnerXml += "<inventoryCompatible>true</inventoryCompatible>";
                elem.InnerXml += string.Format("<exists_on_shop>{0}</exists_on_shop>", item.ExistsOnShop).ToLower();

                foreach (KeyValuePair<string, string> property in item.Properties)
                {
                    if (property.Value.StartsWith("{") && property.Value.EndsWith("}"))
                    {
                        XNode jsonXml = JsonConvert.DeserializeXNode(property.Value, property.Key);
                        elem.InnerXml += jsonXml.ToString();
                    }
                    else
                    {
                        elem.InnerXml += string.Format("<{0}>{1}</{0}>", property.Key, property.Value);
                    }
                }

                root.AppendChild(elem);
            }

            int numRecords = gameItems.Count;
            elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            uint totalNumRecords = gameItems.TotalCount;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("Page");
            elem.InnerText = page.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("Search");
            elem.InnerText = searchString.ToString();

            root.InsertBefore(elem, root.FirstChild);

            InsertPassthroughTag(ref doc);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
