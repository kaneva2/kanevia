///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;
using System.Text.RegularExpressions;
using System.IO;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class sendBlasts : KgpBasePage
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static bool _userIsAdmin = false;

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(false))
            {
                if (Request.Params["diarytext"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>blast text not provided</ResultDescription>\r\n</Result>";
                    m_logger.Error("Error In sendBlast.aspx: blast text not specified");
                    Response.Write(errorStr);
                    return;
                }

                if (Request.Params["blasttype"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>blast type not provided</ResultDescription>\r\n</Result>";
                    m_logger.Error("Error In sendBlast.aspx: blast type not specified");
                    Response.Write(errorStr);
                    return;
                }

                //create new blast object
                Blast blast = new Blast();

                int userId = 0;
                bool userIsFemale = false;

                User user; 

                //get user information 
                try
                {
                    user = (new UserFacade()).GetUser(GetUserId());

                    if ((user != null) && (user.UserId != 0))
                    {
                        userId = user.UserId;
                        _userIsAdmin = IsAdministrator(userId);
                        userIsFemale = user.Gender.ToUpper () == "F" ? true : false;

                        //store user related info
                        blast.SenderId = (uint)userId;
                        blast.SenderName = user.Username;
                        blast.NameNoSpaces = user.NameNoSpaces;
                        blast.ThumbnailSmallPath = user.ThumbnailSmallPath;
                    }
                    else
                    {
                        m_logger.Error("Error In sendBlast.aspx: error processing user");
                        string errorStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>An error occurred while processing user information. </ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
                catch (Exception)
                {
                    m_logger.Error("Error In sendBlast.aspx: error processing user");
                    string  errorStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>An error occurred while processing user information. </ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                //set parameter values
                bool isAdmin = false;
                bool isCommunityBlast = false;

                //check for optional information and set defaults if no values provided

                //store the message 
                string message = Request.Params["diarytext"].ToString();
                //check for injection scripts
                if (KanevaWebGlobals.ContainsInjectScripts(message))
                {
                    m_logger.Warn("User " + userId.ToString() + " tried to script from IP " + Common.GetVisitorIPAddress());
                    string  errorStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Invalid script values were entered. </ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (message, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {                                                                                       //Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE
                    string errorStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Restricted words were entered. </ResultDescription>\r\n</Result>";
                    Response.Write (errorStr);
                    return;
                }

                blast.DiaryEntry = message;

                //is this a blas from a site administrator
                if ((Request.Params["admin"] != null) && _userIsAdmin)
                {
                    try
                    {
                        isAdmin = Convert.ToBoolean(Request.Params["admin"]);
                    }
                    catch (FormatException)
                    {
                    }
                }

                //is this a blasts aimed at a community
                if (Request.Params["comBlast"] != null)
                {
                    try
                    {
                        isCommunityBlast = Convert.ToBoolean(Request.Params["comBlast"]);
                    }
                    catch (FormatException)
                    {
                    }
                }

                //fill the blast object type
                try
                {
                    switch(Request.Params["blasttype"].ToUpper())
                    {
                        case "TEXT":
                            blast.BlastType = (int)BlastTypes.BASIC;
                            break;
                        case "EVENT":
                            blast.BlastType = (int)BlastTypes.EVENT;
                            break;
                        case "LINK":
                            blast.BlastType = (int)BlastTypes.LINK;
                            break;
                        case "PHOTO":
                            blast.BlastType = (int)BlastTypes.PHOTO;
                            break;
                        case "PLACE":
                            blast.BlastType = (int)BlastTypes.PLACE;
                            break;
                        case "VIDEO":
                            blast.BlastType = (int)BlastTypes.VIDEO;
                            break;
                        case "CAMERA_SCREENSHOT":
                            blast.BlastType = (int)BlastTypes.CAMERA_SCREENSHOT;
                            break;
                    }
                }
                catch (FormatException)
                {
                    string errorStr = "<Result>\r\n<ReturnCode>0</ReturnCode>\r\n  <ResultDescription>blast type is invalid</ResultDescription>\r\n</Result>";
                    m_logger.Error("Error In sendBlast.aspx: invalid blast type specified");
                    Response.Write(errorStr);
                    return;
                }

                //what is the id of the community for the blast
                if (Request.Params["commID"] != null)
                {
                    try
                    {
                        blast.CommunityId = Convert.ToUInt32(Request.Params["commID"]);
                    }
                    catch (FormatException)
                    {
                        blast.CommunityId = 0;  
                    }
                }
                else
                {
                    blast.CommunityId = 0;
                }

                //what is the highlighting for the blast if any
                if (Request.Params["highlight"] != null)
                {
                    try
                    {
                        blast.HighlightCss = Request.Params["highlight"].ToString();
                    }
                    catch (FormatException)
                    {
                        blast.HighlightCss = "";
                    }
                }
                else
                {
                    blast.HighlightCss = "";
                }

                //what is the expiration date for the admin blast if any
                if (Request.Params["expireDate"] != null && isAdmin)
                {
                    try
                    {
                        blast.DateEvent = Convert.ToDateTime(Request.Params["expireDate"]);
                    }
                    catch (FormatException)
                    {
                        blast.DateEvent = DateTime.Now.AddDays(1.0);
                    }
                }
                else
                {
                    blast.DateEvent = DateTime.Now.AddDays(1.0);
                }

                if (blast.CommunityId > 0)
                {
                    try
                    {
                        Community community = new CommunityFacade ().GetCommunity ((int)blast.CommunityId);
                        blast.CommunityName = community.Name;
                        blast.IsCommunityPersonal = !community.IsPersonal.Equals (0);
                    }
                    catch
                    {
                        blast.CommunityName = "";
                        blast.IsCommunityPersonal = false;
                    }
                }

                bool sentFromApp = false;

                if (Request.Params["fromApp"] != null)
                {
                    sentFromApp = (Convert.ToInt32(Request.Params["fromApp"]) == 1);
                }

                // This code is used to insert the path for the default profile pic for a user if they
                // have not uploaded a profile pic yet.  Blast does not store the user's gender thus
                // why we have to prepopulate the thumbnail_path field with default profile image
                if ((blast.IsCommunityPersonal || (!blast.IsCommunityPersonal && !isCommunityBlast))
                    && blast.ThumbnailSmallPath.Length.Equals (0))
                {
                    if (userIsFemale)
                    {
                        blast.ThumbnailSmallPath = "/KanevaIconFemale_sm.gif";
                    }
                    else
                    {
                        blast.ThumbnailSmallPath = "/KanevaIconMale_sm.gif";
                    }
                }

                string url = "";
                int assetId = -1;
                int zoneIndex = 0;

                if (blast.BlastType == (int)BlastTypes.PHOTO || blast.BlastType == (int)BlastTypes.CAMERA_SCREENSHOT)
                {
                    if (blast.BlastType == (int) BlastTypes.CAMERA_SCREENSHOT && blast.CommunityId == 0)
                    {
                        if (Request.Params["zone_index"] != null)
                        {
                            try
                            {
                                zoneIndex = Convert.ToInt32 (Request.Params["zone_index"]);

                                // Check to see if this is in the faux_3d_apps table
                                if (zoneIndex > 0)
                                {
                                    blast.CommunityId = GetCommunityFacade.GetCommunityIdFromFaux3DAppZoneIndex (zoneIndex);

                                    // Not a faux app, now check to see if a public place
                                    if (blast.CommunityId == 0)
                                    {
                                        blast.CommunityName = GetGameFacade.GetZoneNameFromZoneIndex (zoneIndex);
                                        if (blast.CommunityName != "")
                                        {
                                            blast.CommunityId = int.MaxValue;
                                        }
                                    }
                                }
                            }
                            catch (FormatException)
                            { }
                        }

                        if (blast.CommunityId == 0 && blast.CommunityName == "")
                        {
                            string errorStr = "<Result>\r\n<ReturnCode>0</ReturnCode>\r\n  <ResultDescription>community id (0) is invalid for camera blast</ResultDescription>\r\n</Result>";
                            m_logger.Error ("Error In sendBlast.aspx: invalid community id = 0 for camera screenshot");
                            Response.Write (errorStr);
                            return;
                        }
                    }
                    
                    //store the message 
                    url = Request.Params["url"].ToString();

                    // Try to pull the image url out of the input string
                    Match match = Regex.Match(url, "(http\\:\\/\\/)?[a-zA-Z0-9\\-\\.]+[a-zA-Z0-9\\-\\.]+\\.[a-zA-Z]{2,3}(?:\\/\\S*)?(?:[a-zA-Z0-9_])+\\.(?:jpg|jpeg|gif|png)", RegexOptions.IgnoreCase);

                    if (match.Length > 0)
                    {
                        url = Server.HtmlEncode(match.Value);
                        if (!url.StartsWith("http://"))
                        {
                            url = "http://" + url;
                        }

                        url = "<a href=\"" + url + "\" target=\"_blank\"><img src=\"" + url + "\"/></a>";
                    }
                    else
                    {
                        // don't pass through to the send blast code if it does not match our regex
                        url = "";

                        // check for an asset id instead of a url
                        try
                        {
                            assetId = Convert.ToInt32(Request.Params["assetid"]);
                        }
                        catch (Exception){}
                        if (assetId > -1)
                        {
                            // Grab the url from the db for this asset
                            Asset imageAsset = new MediaFacade().GetNewAssetInformation(assetId);
                            url = KanevaGlobals.ImageServer + '/' + imageAsset.ImageFullPath;

                            if (blast.BlastType == (int) BlastTypes.CAMERA_SCREENSHOT)
                            {
                                url = "<img src=\"" + url + "\"/>";
                            }
                            else
                            {
                                url = "<a href=\"" + url + "\" target=\"_blank\"><img src=\"" + url + "\"/></a>";
                            }

                            // Check for set as profile picture if requested. User must be owner or moderator of community.
                            bool setAsThumbnail = Convert.ToBoolean(Request.Params["setasthumbnail"] ?? "false");

                            if (setAsThumbnail && blast.CommunityId > 0 && CommunityUtility.IsCommunityModerator((int)blast.CommunityId, (int)blast.SenderId, true))
                            {
                                if (KanevaGlobals.ContentServerPath != null)
                                {
                                    try
                                    {
                                        // We only support GIF and JPEG images. Image type will be one of the two.
                                        string imageType = (Path.GetExtension(imageAsset.MediaPath).ToUpper().Equals(".GIF") ? "image/gif" : "image/jpeg");

                                        // Update the community
                                        GetCommunityFacade.UpdateCommunity((int)blast.CommunityId, 0, false, imageAsset.MediaPath, imageType, true);

                                        // Create any and all thumbnails needed. Leaving in ability to generate user profile thumbs in case it's ever wanted.
                                        if (blast.IsCommunityPersonal)
                                        {
                                            ImageHelper.GenerateAllChannelThumbnails(imageAsset.ContentExtension, imageAsset.MediaPath, KanevaGlobals.ContentServerPath, (int)blast.SenderId, (int)blast.CommunityId);
                                        }
                                        else
                                        {
                                            ImageHelper.GenerateAllCommuntyThumbnails(imageAsset.ContentExtension, imageAsset.MediaPath, KanevaGlobals.ContentServerPath, (int)blast.SenderId, (int)blast.CommunityId);
                                        }

                                        // Update the game, if this community is a game
                                        Community community = GetCommunityFacade.GetCommunity((int)blast.CommunityId);
                                        if (community.WOK3App != null && community.WOK3App.GameId > 0)
                                        {
                                            Game game = GetGameFacade.GetGameByGameId(community.WOK3App.GameId);
                                            if (game != null)
                                            {
                                                game.GameImagePath = community.ThumbnailSmallPath;
                                                GetGameFacade.SaveGame(game);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        string errorStr = "<Result>\r\n<ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Unable to set photo as thumbnail. " + ex.Message + "</ResultDescription>\r\n</Result>";
                                        m_logger.Error("Error In sendBlast.aspx: " + ex.Message);
                                        Response.Write(errorStr);
                                        return;
                                    }
                                }
                                else
                                {
                                    string errorStr = "<Result>\r\n<ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Unable to set photo as thumbnail. ContentServerPath is undefined</ResultDescription>\r\n</Result>";
                                    m_logger.Error("Error In sendBlast.aspx: Unable to set photo as thumbnail. ContentServerPath is undefined");
                                    Response.Write(errorStr);
                                    return;
                                }
                            }
                        }
                    }
                }

                //send the blast
                if (SendBlast(blast, isAdmin, isCommunityBlast, url, assetId))
                {
                    if (sentFromApp)
                    {
                        GetFameFacade.RedeemPacket(GetUserId(), (int)PacketId.SEND_BLAST_FROM_APP, (int)FameTypes.World);
                    }
                    string errorStr = "<Result>\r\n<ReturnCode>1</ReturnCode>\r\n  <ResultDescription>Blast was sent successfully!</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                }
                else
                {
                    string errorStr = "<Result>\r\n<ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Blast could not be sent.</ResultDescription>\r\n</Result>";
                    m_logger.Error("Error In sendBlast.aspx: blast send has failed");
                    Response.Write(errorStr);
                }
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
