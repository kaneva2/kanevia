///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;
using log4net;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for memCache.
    /// </summary>
    public class memCache : KgpBasePage    {
        private void Page_Load(object sender, System.EventArgs e)        {
            try { m_logger.Info ("BEGIN - memCache.PageLoad()"); }
            catch { }

            Response.ContentType = "text/xml;charset=UTF-8";

            string action = Request.Params["action"];
            if ( action == null)                {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }


            if (action.Equals ("dropCompound"))                 {
                string key = Request.Params["key"];

                if ( key == null)                               {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>no key provided</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                string      cachedItem = (string)CentralCache.Get(key);
                if (cachedItem == null)                         {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>primary key [" + key + "] not found!</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                // currently, only supports semicolon delimiters
                //
                string[]    subKeys;
                char[]      splitter = { ';' };
                subKeys = cachedItem.Split(splitter);

                string dropped = "[" + key + "]";
                foreach (string subKey in subKeys)
                {
                    dropped += "[" + subKey + "]";
                    CentralCache.Remove(subKey);
                }
                CentralCache.Remove(key);

                IList<string> relatedKeys = CentralCache.GetRelatedKeys(key);
                if (relatedKeys != null)
                {
                    foreach (string rk in relatedKeys)
                    {
                        CentralCache.Remove(rk);
                    }
                }

                string retCode = "0";
                string retDesc = "Success";

                string returnStr = "<Result>"
                                        + "<ReturnCode>" + retCode + "</ReturnCode>"
                                        + "<ResultDescription>" + retDesc + "</ResultDescription>"
                                        + dropped
                                    +"</Result>";
                Response.Write (returnStr);
            }

            m_logger.Debug("END - memCache.PageLoad()");
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
