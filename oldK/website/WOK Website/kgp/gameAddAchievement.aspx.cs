///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.IO;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class gameAddAchievement : KgpBasePage
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        XmlNode root = null;

        private string userEmail = null;
        private string passWord = null;
        private int gameId = 0;
        private string achievementName = null;
        private string achievementDescription = null;
        private UInt32 achievementPoints = 0;
        private int imageAssetId = 0;

        const string SUCCESS = "0";
        const string BAD_PARAMETERS = "1";
        const string USER_NOT_VALIDATED = "2";
        const string USER_NOT_OWNER = "3";
        const string ACHIEVEMENT_NAMEDESC_INVALID = "4";
        const string NO_MORE_AVAILABLE_POINTS = "5";
        const string NOT_ENOUGH_AVAILABLE_POINTS = "6";

        #endregion

        #region Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            // name, description, price, gameId, user and pw are all required; imageAssetId is optional
            if (!GetRequestParams())
            {
                string errorString = "<Result><ReturnCode>" + BAD_PARAMETERS + "</ReturnCode><ReturnDescription>Error parsing request parameters</ReturnDescription></Result>";
                Response.Write(errorString);
                return;
            }

            int roleMemberShip = 0;
            if (!ValidateUser(true, userEmail, passWord, gameId, ref roleMemberShip))
            {
                string errorString = "<Result><ReturnCode>" + USER_NOT_VALIDATED + "</ReturnCode><ReturnDescription>The user is not validated</ReturnDescription></Result>";
                Response.Write(errorString);
                m_logger.Error("Error In gameAddAchievement.aspx: user with email " + userEmail + " is not validated");
                return;
            }

            int userId = GetUserFacade.GetUserByEmail(userEmail).UserId;
            int ownerId = GetGameFacade.GetGameOwner(gameId);
            // only owners of the 3d app can create an achievement
            if (!ownerId.Equals(userId))
            {
                string errorString = "<Result><ReturnCode>" + USER_NOT_OWNER + "</ReturnCode><ReturnDescription>The user is not the owner of this 3D App</ReturnDescription></Result>";
                Response.Write(errorString);
                m_logger.Error("Error In gameAddAchievement.aspx: user " + userId + " is not the owner of the 3D App " + gameId);
                return;
            }

            uint achievementId = 0;

            try
            {
                achievementId = AddGameAchievement(userId, gameId, achievementName, achievementDescription, achievementPoints, imageAssetId);
            }
            catch (ArgumentException ex)
            {
                if (ex.ParamName == "namedesc")
                {
                    string errorString = "<Result><ReturnCode>" + ACHIEVEMENT_NAMEDESC_INVALID + "</ReturnCode><ReturnDescription>The achievement's name and/or description is invalid</ReturnDescription></Result>";
                    Response.Write(errorString);
                    m_logger.Error("Error In gameAddAchievement.aspx: " + ex.Message);
                }
                else if (ex.ParamName == "availpoints")
                {
                    string errorString = "<Result><ReturnCode>" + NO_MORE_AVAILABLE_POINTS + "</ReturnCode><ReturnDescription>There are no more points to create any new achievements</ReturnDescription></Result>";
                    Response.Write(errorString);
                    m_logger.Error("Error In gameAddAchievement.aspx: " + ex.Message);
                }
                else if (ex.ParamName == "points")
                {
                    string errorString = "<Result><ReturnCode>" + NOT_ENOUGH_AVAILABLE_POINTS + "</ReturnCode><ReturnDescription>Not enough available points to create new achievement</ReturnDescription></Result>";
                    Response.Write(errorString);
                    m_logger.Error("Error In gameAddAchievement.aspx: " + ex.Message);
                }
                else
                {
                    //Other argument exceptions
                    string errorString = "<Result><ReturnCode>" + BAD_PARAMETERS + "</ReturnCode><ReturnDescription>Operation failed</ReturnDescription></Result>";
                    Response.Write(errorString);
                    m_logger.Error("Error In gameAddAchievement.aspx: " + ex.Message);
                }
                return;
            }

            //generate the XML out put
            DataSet ds = new DataSet();
            ds.DataSetName = "Result";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("AchievementId");
            elem.InnerText = achievementId.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);
            Response.Write(root.OuterXml);
        }
        #endregion

        #region Helper Functions
        
        private bool GetRequestParams()
        {
            try
            {
                achievementName = Server.UrlDecode(Request["name"].ToString());
                achievementDescription = Server.UrlDecode(Request["description"].ToString());
                achievementPoints = Convert.ToUInt32(Server.UrlDecode(Request["points"].ToString()));

                imageAssetId = Convert.ToInt32(Request["assetId"].ToString());

                gameId = Convert.ToInt32(Request["gameId"].ToString());

                userEmail = Request["user"].ToString();
                passWord = Request["pw"].ToString();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error In gameAddAchievement.aspx: error parsing request parameters ", ex);
                return false;
            }

            // optional
            try
            {
                imageAssetId = Convert.ToInt32(Request["imageAssetId"].ToString());
            }
            catch (Exception) {}

            return true;
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
