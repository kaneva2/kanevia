///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Linq;
using System.Text;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// userExperiment inserts new experiment participant records and returns participant data for the logged in user
	/// </summary>
    public class testAssignmentTester : KgpBasePage
	{
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(false))
            {
                // Initialize the lookup
                Dictionary<string, int> assignmentCounts = new Dictionary<string, int>();
                assignmentCounts.Add("481f0941-0ac0-11e5-aa91-002219919c2a", 0);
                assignmentCounts.Add("3cdafe54-0a2a-11e5-aa91-002219919c2a", 0);
                assignmentCounts.Add("6b9c3e29-0ac7-11e5-aa91-002219919c2a", 0);

                Experiment experiment = GetExperimentFacade.GetExperiment("555");

                // Perform the assignment 1000 times
                for(int i = 0; i < 1000; i++)
                {
                    string groupId = GetExperimentFacade.AssignParticipant(experiment);

                    int currentCount = 0;
                    if (assignmentCounts.TryGetValue(groupId, out currentCount))
                    {
                        assignmentCounts[groupId]++;
                    }
                }

                StringBuilder sb = new StringBuilder();
                foreach(KeyValuePair<string, int> pair in assignmentCounts)
                {
                    sb.Append(string.Format("{0} - {1} of {2} = {3}%<br/>", pair.Key, pair.Value, 1000, ((double)pair.Value) / 1000));
                }

                Response.Write(sb.ToString());
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
