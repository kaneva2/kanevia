///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// get all the details about a player in the game
    /// </summary>
    public class getPlayerInfo : KgpBasePage
    {
        #region Declarations

        // Shared report params
        private string _action = null;
        private string _username = null;
        private int _userId = 0;
        private int _inventoryFlags = 0;
        private int _gameId = 0;
        private int _view = 0;

        // Response types
        private string _errorResult = "<Result><ReturnCode>{0}</ReturnCode><ReturnDescription>{1}</ReturnDescription></Result>";

        // Pass group bitmask enum (mirrors PassGroups.h in KEPClient)
        private enum PassGroupMask
        {
            None        = 0x00,
            AP          = 0x01,
            VIP         = 0x02,
            GM          = 0x08,
            Owner       = 0x10, 
            Moderator   = 0x20,
            Invalid     = 0x80
        }

        private enum GraphView
        {
            Nodes,  // Usernames
            Edges,  // SystemIds
            Tree    // Tree traversal starting from specified user
        }

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            Response.Cache.SetNoServerCaching();
            Response.Cache.SetNoStore();

            if (true) // TODO need to validate which STAR is requesting this data, then validate this player KEPAuth'd to it
            {
                if (!IsPostBack)
                {
                    // Get parameters
                    GetRequestParams();

                    // Branch via report type
                    switch (_action)
                    {
                        case "entityCfg":
                            ProcessEntityCfgAction();
                            break;
                        case "alts":
                            ProcessAltsAction();
                            break;
                        default:
                            ProcessDefaultAction();
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Sets page variables using either passed in values or appropriate defaults
        /// </summary>
        private void GetRequestParams()
        {
            int intOut = 0;

            _action         = Request.Params["action"];
            _username       = Request.Params["username"] ?? Request.Params["userName"];
            _userId         = Int32.TryParse(Request.Params["userId"], out intOut) ? intOut : 0;
            _inventoryFlags = Int32.TryParse(Request.Params["flags"], out intOut) ? intOut : 1;
            _gameId         = Int32.TryParse(Request.Params["gid"], out intOut) ? intOut : 0;
            _view           = Int32.TryParse(Request.Params["view"], out intOut) ? intOut : 0;
        }

        /// <summary>
        /// Processes the default action if none is specified
        /// </summary>
        private void ProcessDefaultAction()
        {
            // Validation
            if (_userId <= 0)
            {
                Response.Write(string.Format(_errorResult, -1, "userId"));
                return;
            }

            // Get player data
            DataSet ds = GetUserFacade.GetPlayerData(_userId, _inventoryFlags, _gameId);
            ds.DataSetName = "Result";

            // Format and return result
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Processes the EntityCfg action
        /// </summary>
        private void ProcessEntityCfgAction()
        {
            int passGroupsUser  = (int)PassGroupMask.None;
            int nameColorR      = 0;
            int nameColorG      = 0;
            int nameColorB      = 0;
            int gameId          = 0;
            int zoneIndex       = 0;
            int zoneInstanceId  = 0;
            int zoneType        = 0;
            int passGroupsWorld = (int)PassGroupMask.None;
            int communityId     = 0;
            int resultCount     = 9;

            int[] resultData = null;

            string successResult = "<Result>" +
                                    "<ReturnCode>0</ReturnCode><ReturnDescription>success</ReturnDescription>" +
                                    "<PassGroupsUser>{0}</PassGroupsUser><NameColorR>{1}</NameColorR><NameColorG>{2}</NameColorG><NameColorB>{3}</NameColorB>" +
                                    "<GameId>{4}</GameId><ZoneIndex>{5}</ZoneIndex><ZoneInstanceId>{6}</ZoneInstanceId><ZoneType>{7}</ZoneType><PassGroupsWorld>{8}</PassGroupsWorld>" +
                                   "</Result>";

            // Validation
            if (string.IsNullOrWhiteSpace(_username))
            {
                Response.Write(string.Format(_errorResult, -1, "username."));
                return;
            }

            // Get userId for subsequent lookups
            _userId = GetUserFacade.GetUserIdFromUsername(_username);

            // Get info about the world that user is currently logged-in to (if any)
            DataRow drLoggedInWorld = GetUserFacade.GetUserLoggedInWorld(_userId);
            if (drLoggedInWorld != null)
            {
                gameId = Convert.ToInt32(drLoggedInWorld["game_id"]);
                zoneIndex = Convert.ToInt32(drLoggedInWorld["zone_index"]);
                zoneInstanceId = Convert.ToInt32(drLoggedInWorld["zone_instance_id"]);
                zoneType = Convert.ToInt32(drLoggedInWorld["zone_type"]);
            }

            // Check to see if results are already cached
            string cacheKey = CentralCache.keyEntityCfg(_userId);
            resultData = (int[])CentralCache.Get(cacheKey);

            // If cache is invalid, refresh
            if (resultData == null ||
                resultData.Length != resultCount ||
                gameId != resultData[4] ||
                zoneIndex != resultData[5] ||
                zoneInstanceId != resultData[6])
            {
                // User pass Groups
                DataTable dtPassGroups = UsersUtility.GetPassIds(_userId);
                if (dtPassGroups != null)
                {
                    foreach (DataRow row in dtPassGroups.Rows)
                    {
                        passGroupsUser = passGroupsUser | Convert.ToInt32(row["pass_group_id"]);
                    }
                }

                // User GM status
                if (GetUserFacade.IsUserGM(_userId))
                {
                    passGroupsUser = passGroupsUser | (int)PassGroupMask.GM;
                }

                // Name color
                DataRow drNameColorRGB = GetUserFacade.GetUsersNameColorRGB(_userId);
                if (drNameColorRGB != null)
                {
                    nameColorR = Convert.ToInt32(drNameColorRGB["color_r"]);
                    nameColorG = Convert.ToInt32(drNameColorRGB["color_g"]);
                    nameColorB = Convert.ToInt32(drNameColorRGB["color_b"]);
                }

                // Get info about the world that user is currently logged-in to
                if (drLoggedInWorld != null)
                {
                    communityId = GetCommunityFacade.GetCommunityIdFromWorldIds(KanevaGlobals.WokGameId, gameId, zoneIndex, zoneInstanceId, zoneType);

                    // Process passes
                    DataTable dtWorldPassGroups = GetCommunityFacade.GetWorldPasses(KanevaGlobals.WokGameId, gameId, zoneInstanceId, zoneType);
                    if (dtWorldPassGroups != null)
                    {
                        foreach (DataRow row in dtWorldPassGroups.Rows)
                        {
                            passGroupsWorld = passGroupsWorld | Convert.ToInt32(row["pass_group_id"]);
                        }
                    }

                    // Process community membership
                    CommunityMember communityMember = GetCommunityFacade.GetCommunityMember(communityId, _userId);
                    if (communityMember.AccountTypeId == (int)CommunityMember.CommunityMemberAccountType.OWNER)
                    {
                        passGroupsUser = passGroupsUser | (int)PassGroupMask.Owner;
                    }
                    else if (communityMember.AccountTypeId == (int)CommunityMember.CommunityMemberAccountType.MODERATOR)
                    {
                        passGroupsUser = passGroupsUser | (int)PassGroupMask.Moderator;
                    }
                }

                // Reset resultData aray
                resultData = new int[resultCount];
                resultData[0] = passGroupsUser;
                resultData[1] = nameColorR;
                resultData[2] = nameColorG;
                resultData[3] = nameColorB;
                resultData[4] = gameId;
                resultData[5] = zoneIndex;
                resultData[6] = zoneInstanceId;
                resultData[7] = zoneType;
                resultData[8] = passGroupsWorld;
                
                // Cache results
                CentralCache.Store(cacheKey, resultData, TimeSpan.FromMinutes(15));
            }

            // Return success response
            Response.Write(string.Format(successResult, resultData[0], resultData[1], resultData[2], resultData[3], resultData[4], resultData[5], resultData[6], resultData[7], resultData[8]));
        }

        /// <summary>
        /// Processes the alts action
        /// </summary>
        private void ProcessAltsAction()
        {
            // Limit this call to GMs Only
            if (CheckUserId(false) && GetUserFacade.IsUserGM(KanevaWebGlobals.CurrentUser.UserId))
            {
                // Validation
                if (_userId <= 0)
                {
                    if (string.IsNullOrWhiteSpace(_username))
                    {
                        // Neither username or userId was passed
                        Response.Write(string.Format(_errorResult, -1, "username or userId must be specified"));
                        return;
                    }

                    _userId = GetUserFacade.GetUserIdFromUsername(_username);

                    if (_userId <= 0)
                    {
                        // Username was passed, but invalid
                        Response.Write(string.Format(_errorResult, -1, "invalid username"));
                        return;
                    }
                }

                // Build graph of users and systems
                Graph<int, User> graph = GetUserFacade.GetUserAltsGraph(_userId);

                string innerXML = string.Empty;
                if (graph != null && graph.Count > 0)
                {
                    switch ((GraphView)_view)
                    {
                        case GraphView.Edges:
                            innerXML = BuildEdgeListXML(graph);
                            break;
                        case GraphView.Tree:
                            innerXML = BuildTreeXML(graph, _userId);
                            break;
                        default:
                            innerXML = BuildNodeListXML(graph);
                            break;
                    }
                }

                Response.Write(string.Format("<Result><ReturnCode>0</ReturnCode><ReturnDescription>success</ReturnDescription>{0}</Result>", innerXML));
            }
            else
            {
                Response.Write(string.Format(_errorResult, -1, "unauthorized user"));
            }
        }

        private XmlElement BuildUserElement(XmlDocument doc, int userId, string username,
            Dictionary<int, List<UserSuspension>> suspensions, Dictionary<int, bool> onlineStatuses)
        {
            XmlElement userElement = doc.CreateElement("User");
            userElement.SetAttribute("Name", username);

            List<UserSuspension> susOut;
            bool bOut;
            if (suspensions.TryGetValue(userId, out susOut))
            {
                if (susOut.Where(s => s.IsPermanent).Count() > 0)
                    userElement.SetAttribute("Suspension", "Permanent");
                else
                    userElement.SetAttribute("Suspension", susOut.Max(s => s.BanLengthDays) + " Days");
            }

            if (onlineStatuses.TryGetValue(userId, out bOut) && bOut)
            {
                userElement.SetAttribute("Online", "Online");
            }

            return userElement;
        }

        private string BuildNodeListXML(Graph<int, User> graph)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("Users"));
            XmlElement root = doc.DocumentElement;
            
            // Get user suspensions
            int[] userIds = graph.Select(n => n.Key).ToArray();
            Dictionary<int, List<UserSuspension>> suspensions = GetUserFacade.GetUserSuspensions(userIds, true);

            // Get use online status
            Dictionary<int, bool> onlineStatuses = GetUserFacade.AreUsersOnlineInWoK(userIds);

            // Order alphabetically
            List<GraphNode<int, User>> orderedList = graph.OrderBy(n => n.Value.Username).ToList();
            foreach (GraphNode<int, User> node in orderedList)
            {
                XmlElement userElement = BuildUserElement(doc, node.Key, node.Value.Username, suspensions, onlineStatuses);
                root.AppendChild(userElement);
            }

            return doc.InnerXml;
        }

        private XmlElement BuildSystemElement(XmlDocument doc, string systemId)
        {
            XmlElement systemElement = doc.CreateElement("System");
            systemElement.SetAttribute("Id", systemId);

            UserSystemConfiguration config = GetUserFacade.GetMostRecentConfigForSystem(systemId);
            
            // Add system name
            if (!string.IsNullOrWhiteSpace(config.SystemName))
            {
                systemElement.SetAttribute("SysName", config.SystemName);
            }
            else
            {
                systemElement.SetAttribute("SysName", "Unknown");
            }

            // Add system version
            if (!string.IsNullOrWhiteSpace(config.SystemVersion))
            {
                systemElement.SetAttribute("SysVer", config.SystemVersion);
            }

            // Add online status
            if (config.IsRunning)
            {
                systemElement.SetAttribute("Online", "Online");
            }

            return systemElement;
        }

        private string BuildEdgeListXML(Graph<int, User> graph)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("Systems"));
            XmlElement root = doc.DocumentElement;

            // Grab labels for every edge
            List<string> edges = graph.GetEdgeLabels();

            // Order alphabetically
            List<string> orderedList = edges.OrderBy(e => e).ToList();
            foreach (string edge in orderedList)
            {
                XmlElement systemElement = BuildSystemElement(doc, edge);
                root.AppendChild(systemElement);
            }

            return doc.InnerXml;
        }

        private string BuildTreeXML(Graph<int, User> graph, int rootUserId)
        {
            // Prep the data structures
            HashSet<int> visited = new HashSet<int>();
            Queue<GraphNode<int, User>> userList = new Queue<GraphNode<int, User>>();
            Queue<XmlElement> xmlList = new Queue<XmlElement>();
            GraphNode<int, User> rootGraphNode = graph.GetNode(rootUserId);
            
            // Make sure user is in graph
            if (rootGraphNode == null)
            {
                return string.Empty;
            }

            // Get user suspensions
            int[] userIds = graph.Select(n => n.Key).ToArray();
            Dictionary<int, List<UserSuspension>> suspensions = GetUserFacade.GetUserSuspensions(userIds, true);

            // Get use online status
            Dictionary<int, bool> onlineStatuses = GetUserFacade.AreUsersOnlineInWoK(userIds);

            // Init the document and root node
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("Tree"));

            XmlElement userElement = BuildUserElement(doc, rootUserId, rootGraphNode.Value.Username, suspensions, onlineStatuses);
            doc.DocumentElement.AppendChild(userElement);

            // Init the queues
            visited.Add(rootUserId);
            userList.Enqueue(rootGraphNode);
            xmlList.Enqueue(userElement);

            // Traverse the graph
            while (userList.Count != 0)
            {
                // Grab the current nodes
                GraphNode<int, User> userNode = userList.Dequeue();
                XmlElement xmlNode = xmlList.Dequeue();

                foreach (GraphNode<int, User> neighbor in userNode.Neighbors)
                {
                    if (!visited.Contains(neighbor.Key))
                    {
                        // Grab the connecting "System" element, or create one if none exists
                        string systemId = graph.GetEdgeLabel(userNode, neighbor);
                        XmlElement systemElement = (XmlElement)xmlNode.SelectSingleNode("descendant::System[@Id='" + systemId + "']");
                        if (systemElement == null)
                        {
                            systemElement = BuildSystemElement(doc, systemId);
                            xmlNode.AppendChild(systemElement);
                        }

                        // Add the neighbor to the system
                        userElement = BuildUserElement(doc, neighbor.Key, neighbor.Value.Username, suspensions, onlineStatuses);
                        systemElement.AppendChild(userElement);

                        // Add to queues
                        visited.Add(neighbor.Key);
                        userList.Enqueue(neighbor);
                        xmlList.Enqueue(userElement);
                    }
                    else if (userNode.Value == neighbor.Value)
                    {
                        // If the path is a path to the node itself, just add the system
                        string systemId = graph.GetEdgeLabel(userNode, neighbor);
                        XmlElement systemElement = (XmlElement)xmlNode.SelectSingleNode("descendant::System[@Id='" + systemId + "']");
                        if (systemElement == null)
                        {
                            systemElement = BuildSystemElement(doc, systemId);
                            xmlNode.AppendChild(systemElement);
                        }
                    }
                }
            }

            // Process predecessor list
            return doc.InnerXml;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
