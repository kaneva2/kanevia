///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;

using Kaneva.BusinessLayer.BusinessObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class snapshotGameItems : KgpBasePage
    {
        #region Declarations

        private bool _AddToInventory = true;
        private List<ScriptGameItem> _GameItems = new List<ScriptGameItem>();

        // Response types
        private string _ErrorResult = "<Result>\r\n  <ReturnCode>{0}</ReturnCode>\r\n  <ReturnDescription>{1}</ReturnDescription>\r\n </Result>";

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (CheckUserId(true))
            {
                if (!IsPostBack)
                {
                    GetRequestParams();

                    if (ValidateGameItemUpload())
                    {
                        bool success = GetScriptGameItemFacade.InsertSnapshotScriptGameItems(ref _GameItems, KanevaWebGlobals.CurrentUser.UserId, _AddToInventory);

                        if (success)
                        {
                            Response.Write(BuildSuccessResponse());
                        }
                        else
                        {
                            Response.Write(string.Format(_ErrorResult, -1, "There was an error adding game items."));
                        }
                    }
                }
            }
        }

        private string BuildSuccessResponse()
        {
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("Result");
            XmlElement elem = doc.CreateElement("GameItems");
            
            foreach (ScriptGameItem item in _GameItems)
            {
                XmlElement gIElem = doc.CreateElement("GameItem");
                gIElem.InnerXml = string.Format("<GIId>{0}</GIId>\n<GIGLID>{1}</GIGLID>", item.GIId, item.GIGlid);

                elem.AppendChild(gIElem);
            }

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("AffectedItemCount");
            elem.InnerText = _GameItems.Count.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "Game items were added successfully.";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            return root.OuterXml;
        }

        private bool ValidateGameItemUpload()
        {
            // Make sure some game items were sent
            if (_GameItems.Count == 0)
            {
                Response.Write(string.Format(_ErrorResult, -1, "No valid game items were specified."));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Sets page variables using either passed in values or appropriate defaults
        /// </summary>
        private void GetRequestParams()
        {
            string strOut;

            if (!Boolean.TryParse(Request.Params["addToInventory"], out _AddToInventory))
            {
                _AddToInventory = true;
            }
            
            string gameItemJson = Request.Params["gameItems"];
            if (!string.IsNullOrWhiteSpace(gameItemJson))
            {
                // Anything in the Json that's NOT in this list will go in the "Properties" dictionary of a script game item.
                Dictionary<string, string> sgiPropNames = typeof(ScriptGameItem)
                    .GetProperties()
                    .ToDictionary(p => p.Name, p => p.Name, StringComparer.InvariantCultureIgnoreCase);

                List<JObject> items = JsonConvert.DeserializeObject<List<JObject>>(gameItemJson);
                foreach (JObject item in items)
                {
                    ScriptGameItem gItem = item.ToObject<ScriptGameItem>();

                    // Make sure that only eligible game items are considered
                    if (gItem.InventoryCompatible)
                    {
                        // Parse properties
                        gItem.Properties = item.Properties()
                            .Where(p => !sgiPropNames.TryGetValue(p.Name, out strOut))
                            .ToDictionary(p => p.Name, p => p.Value.ToString());

                        // Reset GIGLID
                        gItem.GIGlid = 0;

                        // Add it to the insertion list
                        _GameItems.Add(gItem);
                    }
                }
            }
        }
    }
}
