///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for texturesList.
	/// </summary>
	public class messagecenter : KgpBasePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
				int page = 0;
				int items_per_page = 0;
				string responseStr = null;

				if ( Request.Params["action"] == null)
				{
					string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
					Response.Write(errorStr);
					return;
				}

				string actionreq = Request.Params["action"];

				if ( actionreq.Equals("ListPendingGifts") )
				{
       				if ( (Request.Params["start"] == null) || (Request.Params["max"] == null) )
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>page number and items per page not specified</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}

					// default to 3D
					bool get2DGifts = Request.Params["type"] != null &&  Request.Params["type"].ToString() == "2";

					page = Int32.Parse(Request.Params["start"].ToString());
					items_per_page = Int32.Parse(Request.Params["max"].ToString());

					DataSet ds = new DataSet();

					ds.DataSetName = "Result";

					PagedDataTable pdt;

					string orderby = " m.message_date desc ";

					pdt = UsersUtility.GetPendingGifts( m_userId, "", orderby, page, items_per_page, get2DGifts );

					pdt.TableName = "PendingGifts";
					ds.Tables.Add(pdt);

					XmlDocument doc = new XmlDocument();
					doc.LoadXml(ds.GetXml());
					XmlNode root = doc.DocumentElement;

					int numRecords = pdt.Rows.Count;
					XmlElement elem = doc.CreateElement("NumberRecords");
					elem.InnerText= numRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					int totalNumRecords = pdt.TotalCount;
					elem = doc.CreateElement("TotalNumberRecords");
					elem.InnerText= totalNumRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnDescription");
					elem.InnerText="success";

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnCode");
					elem.InnerText="0";

					root.InsertBefore( elem, root.FirstChild);

					Response.Write(root.OuterXml);
				}
				else if ( actionreq.Equals("Accept2DGift") )
				{
					// 2D only, 3D uses server
					if ( Request.Params["messageId"] == null || Request.Params["type"] == null || Request.Params["type"].ToString() != "2" )
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>message id not specified for action</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}

					int messageId = Int32.Parse(Request.Params["messageId"].ToString());

					bool ret = UsersUtility.Accept2DGift( m_userId, messageId );

                    if ( ret)
					{
						UsersUtility.DeleteMessage ( m_userId, messageId);
						responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
					}
					else
					{
						responseStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>error accepting gift</ResultDescription>\r\n</Result>";
					}

       				Response.Write(responseStr);

					return;
				}
				else if ( actionreq.Equals("RejectGift") )
				{
					// same for 2D/3D
       				if ( Request.Params["messageId"] == null)
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>message id not specified for action</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}

       				int messageId = Int32.Parse(Request.Params["messageId"].ToString());
					bool gift2D = Request.Params["type"] != null &&  Request.Params["type"].ToString() == "2";

					bool ret = UsersUtility.RejectGift( m_userId, messageId,gift2D);

                    if ( ret)
					{
                        UsersUtility.DeleteMessage (m_userId, messageId);
						responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
					}
					else
					{
						responseStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>error rejecting gift</ResultDescription>\r\n</Result>";
					}

       				Response.Write(responseStr);

					return;
				}
				else if ( actionreq.Equals("ReadGiftMessage") )
				{
					// same for 2D/3D
					if ( Request.Params["messageId"] == null)
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>message id not specified for action</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}

       				int messageId = Int32.Parse(Request.Params["messageId"].ToString());
					bool gift2D = Request.Params["type"] != null &&  Request.Params["type"].ToString() == "2";

					DataSet ds = new DataSet();

					ds.DataSetName = "Result";

					DataRow dr = UsersUtility.GetGiftMessage( m_userId, messageId, gift2D);

					if ( dr == null)
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>gift message not found</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}

					DataTable dt = dr.Table;

					dt.TableName = "Message";

					ds.Tables.Add(dt);

					XmlDocument doc = new XmlDocument();
					doc.LoadXml(ds.GetXml());
					XmlNode root = doc.DocumentElement;

					int numRecords = 1;
					XmlElement elem = doc.CreateElement("NumberRecords");
					elem.InnerText= numRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					int totalNumRecords = 1;
					elem = doc.CreateElement("TotalNumberRecords");
					elem.InnerText= totalNumRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnDescription");
					elem.InnerText="success";

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnCode");
					elem.InnerText="0";

					root.InsertBefore( elem, root.FirstChild);

					Response.Write(root.OuterXml);
				}
				else if ( actionreq.Equals("GetGiftCatalog") )
				{
					// works for 2D and 3D with extra param to get 2D type=2
       				if ( (Request.Params["start"] == null) || (Request.Params["max"] == null) )
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>page number and items per page not specified</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}

					bool get2DCatalog = Request.Params["type"] != null &&  Request.Params["type"].ToString() == "2";

       				page = Int32.Parse(Request.Params["start"].ToString());
					items_per_page = Int32.Parse(Request.Params["max"].ToString());

					// catId for 3D only
					int catId = 0;
					if( !get2DCatalog && Request.Params["catId"] == null )
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>catId not specified</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}
					else
						catId = Int32.Parse(Request.Params["catId"].ToString() );

					DataSet ds = new DataSet();

					ds.DataSetName = "Result";

					PagedDataTable pdt;

					string orderby = " name ";

					if ( get2DCatalog )
						pdt = StoreUtility.Get2DGiftCatalog( orderby, page, items_per_page );
					else
						pdt = StoreUtility.Get3DGiftCatalog( catId, orderby, page, items_per_page );

					pdt.TableName = "Gift";
					ds.Tables.Add(pdt);

					XmlDocument doc = new XmlDocument();
					doc.LoadXml(ds.GetXml());
					XmlNode root = doc.DocumentElement;

					int numRecords = pdt.Rows.Count;
					XmlElement elem = doc.CreateElement("NumberRecords");
					elem.InnerText= numRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					int totalNumRecords = pdt.TotalCount;
					elem = doc.CreateElement("TotalNumberRecords");
					elem.InnerText= totalNumRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnDescription");
					elem.InnerText="success";

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnCode");
					elem.InnerText="0";

					root.InsertBefore( elem, root.FirstChild);

					Response.Write(root.OuterXml);
				}
				else if ( actionreq.Equals("GetGiftCatalogs") )
				{
					// works for 2D and 3D with extra param to get 2D type=2
					bool get2DCatalog = Request.Params["type"] != null &&  Request.Params["type"].ToString() == "2";

					DataSet ds = new DataSet();

					ds.DataSetName = "Result";

					PagedDataTable pdt;

					string tableList = "";
					if ( get2DCatalog )
						tableList = "catalogs_2d"; // todo this table doesn't exist
					else
						tableList = KanevaGlobals.DbNameKGP + ".catalogs_3d"; 

					pdt = KanevaGlobals.GetDatabaseUtility().GetPagedDataTable( "catalog_id, name", tableList, "", "name", new Hashtable(), 1, 999 );

					pdt.TableName = "GiftCatalogs";
					ds.Tables.Add(pdt);

					XmlDocument doc = new XmlDocument();
					doc.LoadXml(ds.GetXml());
					XmlNode root = doc.DocumentElement;

					int numRecords = pdt.Rows.Count;
					XmlElement elem = doc.CreateElement("NumberRecords");
					elem.InnerText= numRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					int totalNumRecords = pdt.TotalCount;
					elem = doc.CreateElement("TotalNumberRecords");
					elem.InnerText= totalNumRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnDescription");
					elem.InnerText="success";

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnCode");
					elem.InnerText="0";

					root.InsertBefore( elem, root.FirstChild);

					Response.Write(root.OuterXml);
				}
				else if ( actionreq.Equals("GiveGift") )
				{
					// works for 2D and 3D, but if rectivate 2D need to distinquish between two 
					// since giftId will be from different tables
       				if ( (Request.Params["toId"] == null) || (Request.Params["subject"] == null) || (Request.Params["message"] == null) || (Request.Params["giftId"] == null) )
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>toId, subject, message, or giftId not specified for action</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}

					int toId = Int32.Parse(Request.Params["toId"].ToString());
					string subject = Request.Params["subject"];
					string message = Request.Params["message"];
					int giftId = Int32.Parse(Request.Params["giftId"].ToString());
					bool gift2D = Request.Params["type"] != null &&  Request.Params["type"].ToString() == "2";

					int ret = UsersUtility.GiveGift( m_userId, toId, subject, message, giftId, gift2D );

                    if ( ret > 0)
					{
                        // Per Animesh on 5/15 Remove give a gift fame
                        //// Award user World Fame
                        //FameFacade fameFacade = new FameFacade();
                        //fameFacade.RedeemPacket(m_userId, (int)PacketId.WORLD_GIFT, (int)FameTypes.World);

						responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
					}
					else
					{
						responseStr = "<Result>\r\n  <ReturnCode>" + ret + "</ReturnCode>\r\n  <ResultDescription>error sending gift</ResultDescription>\r\n</Result>";
					}

       				Response.Write(responseStr);

					return;
				}
				else if ( actionreq.Equals("GetFriendRequests") )
				{
       				if ( (Request.Params["start"] == null) || (Request.Params["max"] == null) )
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>page number and items per page not specified</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}

       				page = Int32.Parse(Request.Params["start"].ToString());
					items_per_page = Int32.Parse(Request.Params["max"].ToString());

					DataSet ds = new DataSet();

					ds.DataSetName = "Result";

					PagedDataTable pdt;

					string orderby = " request_date desc ";

					pdt = UsersUtility.GetIncomingPendingFriends ( m_userId, "", orderby, true, page, items_per_page);

					pdt.TableName = "PendingFriend";
					ds.Tables.Add(pdt);

					XmlDocument doc = new XmlDocument();
					doc.LoadXml(ds.GetXml());
					XmlNode root = doc.DocumentElement;

					int numRecords = pdt.Rows.Count;
					XmlElement elem = doc.CreateElement("NumberRecords");
					elem.InnerText= numRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					int totalNumRecords = pdt.TotalCount;
					elem = doc.CreateElement("TotalNumberRecords");
					elem.InnerText= totalNumRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnDescription");
					elem.InnerText="success";

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnCode");
					elem.InnerText="0";

					root.InsertBefore( elem, root.FirstChild);

					Response.Write(root.OuterXml);
				}
				else if ( actionreq.Equals("DenyFriend") )
				{
       				if ( Request.Params["friendId"] == null)
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>friend id not specified for action</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}

       				int friendId = Int32.Parse(Request.Params["friendId"].ToString());

                    GetUserFacade.DenyFriend(m_userId, friendId);

					responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";

       				Response.Write(responseStr);

					return;
				}
				else if ( actionreq.Equals("AcceptFriend") )
				{
       				if ( Request.Params["friendId"] == null)
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>friend id not specified for action</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}

       				int friendId = Int32.Parse(Request.Params["friendId"].ToString());

                    int ret = GetUserFacade.AcceptFriend(m_userId, friendId);

                    if (ret == 1)
                    {
                        UserFacade userFacade = new UserFacade ();

                        // if the user that sent the friend request wants to be notified 
                        // of new friends, then send them the 'you have a new friend' email
                        if (userFacade.GetUser (friendId).NotifyNewFriends)                             
                        {
                            MailUtilityWeb.SendNewFriendNotification (m_userId, friendId);
                        }
                    }
                    
                    responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";

       				Response.Write(responseStr);

					return;
				}
				else if ( actionreq.Equals("GetInboxMessages") )
				{
       				if ( (Request.Params["start"] == null) || (Request.Params["max"] == null) )
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>page number and items per page not specified</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}

       				page = Int32.Parse(Request.Params["start"].ToString());
					items_per_page = Int32.Parse(Request.Params["max"].ToString());

					DataSet ds = new DataSet();

					ds.DataSetName = "Result";

					PagedDataTable pdt;

					string orderby = " m.message_date desc ";

					pdt = UsersUtility.GetUserMessages ( m_userId, "m.type = " + (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, orderby, page, items_per_page);

					pdt.TableName = "Message";
					ds.Tables.Add(pdt);

					XmlDocument doc = new XmlDocument();
					doc.LoadXml(ds.GetXml());
					XmlNode root = doc.DocumentElement;

					int numRecords = pdt.Rows.Count;
					XmlElement elem = doc.CreateElement("NumberRecords");
					elem.InnerText= numRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					int totalNumRecords = pdt.TotalCount;
					elem = doc.CreateElement("TotalNumberRecords");
					elem.InnerText= totalNumRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnDescription");
					elem.InnerText="success";

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnCode");
					elem.InnerText="0";

					root.InsertBefore( elem, root.FirstChild);

					Response.Write(root.OuterXml);
				}
				else if ( actionreq.Equals("ReadInboxMessage") )
				{
       				if ( Request.Params["messageId"] == null)
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>message id not specified for action</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}

       				int messageId = Int32.Parse(Request.Params["messageId"].ToString());

					DataSet ds = new DataSet();

					ds.DataSetName = "Result";

					DataRow dr = UsersUtility.GetUserMessage( m_userId, messageId);

					if ( dr == null)
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>gift message not found</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}

					DataTable dt = dr.Table;

					dt.TableName = "Message";

					ds.Tables.Add(dt);

					XmlDocument doc = new XmlDocument();
					doc.LoadXml(ds.GetXml());
					XmlNode root = doc.DocumentElement;

					int numRecords = 1;
					XmlElement elem = doc.CreateElement("NumberRecords");
					elem.InnerText= numRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					int totalNumRecords = 1;
					elem = doc.CreateElement("TotalNumberRecords");
					elem.InnerText= totalNumRecords.ToString();

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnDescription");
					elem.InnerText="success";

					root.InsertBefore( elem, root.FirstChild);

					elem = doc.CreateElement("ReturnCode");
					elem.InnerText="0";

					root.InsertBefore( elem, root.FirstChild);

					Response.Write(root.OuterXml);
				}
				else if ( actionreq.Equals("DeleteInboxMessage") )
				{
       				if ( Request.Params["messageId"] == null)
					{
						string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>message id not specified for action</ResultDescription>\r\n</Result>";
						Response.Write(errorStr);
						return;
					}

       				int messageId = Int32.Parse(Request.Params["messageId"].ToString());

					bool ret = true;

                    UsersUtility.DeleteMessage (m_userId, messageId);

                    if ( ret)
					{
						responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
					}
					else
					{
						responseStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>error trashing message</ResultDescription>\r\n</Result>";
					}

       				Response.Write(responseStr);

					return;
				}
                else if (actionreq.Equals("SendInWorldInvite"))
                {
                    if ((Request.Params["message"] == null) || (Request.Params["username"] == null) || (Request.Params["kanevaURL"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>message, url or username not specified for action</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    string fromUsername = UsersUtility.GetUserNameFromId(m_userId);
                    string username = Request.Params["username"];
                    string message = Request.Params["message"];
                    string kanevaURL = Request.Params["kanevaURL"]; // kaneva:// url of the place you are inviting them too.

                    string trackingMetric = "";

                    if (Request.Params["trackingMetric"] != null)
                    {
                        trackingMetric = Request.Params["trackingMetric"];
                    }

                    string zoneName = "";

                    if (Request.Params["trackingMetric"] != null)
                    {
                        zoneName = Request.Params["zoneName"];
                    }

					int gameId = 0;

					if ((Request.Params["gameId"] != null))
					{
						gameId = Convert.ToInt32(Request.Params["gameId"]);
					}

                    UserFacade userFacade = new UserFacade();

					string routereq = "false";

					if (Request.Params["route"] != null)
					{
						routereq = Request.Params["route"];
					}

                    bool userInWorld = false;
                    bool userInKIM = false;
                    responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>ERROR</ResultDescription>\r\n</Result>";

					if ( routereq == "true")
					{
					    // Route the request to either an inworld request or to kmail if the receipient is offline

                        // Figure out what kind of zone we're in.  We need
                        string placeName = string.Empty;
                        StpUrl.ExtractPlaceName(kanevaURL, out placeName);

                        int subStart = placeName.LastIndexOf('.');
                        string placeSuffix = placeName.Substring(subStart, placeName.Length - subStart);

                        placeName = placeName.Substring(0, placeName.Length - placeSuffix.Length);

                        string placeTypeParam = string.Empty;
                        switch (placeSuffix)
                        {
                            case ".channel":
                                placeTypeParam = "C" + GetCommunityFacade.GetCommunityIdFromName(placeName, false);
                                break;
                            case ".home":
                                placeTypeParam = "A" + GetUserFacade.GetUserIdFromUsername(placeName);
                                break;
                            case ".people":
                                placeTypeParam = "P" + GetUserFacade.GetUserIdFromUsername(placeName);
                                break;
                            default:
                                placeTypeParam = "U" + gameId;
                                break;
                        }

					   int toUserId = UsersUtility.GetUserIdFromUsername(username);

					   if ( toUserId > 0 )
					   {
						   if (UsersUtility.GetCurrentPlaceInfo(toUserId) != null)
						   {
							   userInWorld = true;
						   }
						   else
						   {
							   // Determine if the user is in KIM
							   
							   userInKIM = UsersUtility.IsUserOnlineInKIM(toUserId);
						   }

                           if (userInKIM)
						   {
							   message = fromUsername + " has invited you to join them in <br/>" + zoneName;
                               string appUrl = @"<a href=""http://" + KanevaWebServerName + "/kgp/playwok.aspx?goto=" + placeTypeParam +
                                   "&amp;ILC=MM3D&amp;link=private&amp;utm_medium=email&amp;utm_campaign=WorldSendRequest&amp;utm_source=viral&amp;utm_term=&amp;utm_content=1&amp;RTSID=" +
                                   trackingMetric + @"&amp;nopromo=true""><br/><br/><font color=""#3189fe"">CLICK HERE TO ACCEPT &amp; GO</font></a>";
																																			  
							   bool ret = userFacade.SendKIMInviteToPlayer(fromUsername, username, message, appUrl);

							   if (ret)
							   {
								   responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
							   }
							   else
							   {
								   responseStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invite could not be sent to KIM.</ResultDescription>\r\n</Result>";
							   }
						   }
						   else if ( ! userInWorld )
						   {
							   // Construct the goto URL for PlayWok
                               string appUrl = @"<A href=""http://" + KanevaWebServerName + "/kgp/playwok.aspx?goto=" + placeTypeParam +
                                   "&ILC=MM3D&link=private&utm_medium=email&utm_campaign=WorldSendRequest&utm_source=viral&utm_term=&utm_content=1&RTSID=" +
                                   trackingMetric + @""">Go There Now!</a>";

							   string subject = string.Format ("{0} has invited you to join them!", fromUsername);
							   Message CMessage = new Message(0, m_userId, toUserId, subject, appUrl, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");
    						   userFacade.InsertMessage (CMessage);
                               responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
						   }
					   }
					}

                    if (userInWorld)
                    {
                        int ret = userFacade.SendInWorldInviteToPlayer(fromUsername, username, message, kanevaURL, trackingMetric);

                        if (ret == 0)
                        {
                            responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
                        }
                        else
                        {
                            responseStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invite could not be sent.</ResultDescription>\r\n</Result>";
                        }
                    }

                    Response.Write(responseStr);

                    return;
                }
                else if (actionreq.Equals("SendMessage"))
                {
                    if ((Request.Params["type"] == null) || (Request.Params["subject"] == null) || (Request.Params["message"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>type, subject, or message not specified for action</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    string type = Request.Params["type"];
                    string subject = Request.Params["subject"];
                    string message = Request.Params["message"];

                    if (type.Equals("Email"))
                    {
                        string emailaddress = Request.Params["address"];

                        if (emailaddress == null)
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>address not specified for type Email</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }

                        DataRow dr = UsersUtility.GetUserFromEmail(emailaddress);

                        if (dr != null)
                        {
                            int user_id = 0;

                            user_id = Convert.ToInt32(dr["user_id"]);


                            UserFacade userFacade = new UserFacade();

                            // If they are not friends make sure sender is above fame level 24
                            if (!userFacade.AreFriends(m_userId, user_id))
                            {
                                // Check to see if sender is above fame level 24
                                FameFacade fameFacade = new FameFacade();
                                UserFame userFame = fameFacade.GetUserFame(user_id, (int)FameTypes.World);

                                if (userFame.CurrentLevel.LevelNumber < 24)
                                {
                                    // Allowed to send a PM
                                    responseStr = "<Result>\r\n  <ReturnCode>-2</ReturnCode>\r\n  <ResultDescription>Not friends or world fame level 24</ResultDescription>\r\n</Result>";
                                    return;
                                }
                            }


                            // Insert the message
                            Message CMessage = new Message(0, m_userId, user_id, subject,
                                message, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");


                            int ret = userFacade.InsertMessage(CMessage);

                            if (ret > 0)
                            {
                                responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
                            }
                            else
                            {
                                responseStr = "<Result>\r\n  <ReturnCode>" + ret + "</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
                            }

                            Response.Write(responseStr);
                            return;
                        }
                        else
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Email address not found</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }

                    }
                    else if (type.Equals("Member"))
                    {
                        string username = Request.Params["username"];

                        if (username == null)
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>username not specified for type Member</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }

                        int user_id = UsersUtility.GetUserIdFromUsername(username);

                        if (user_id != 0)
                        {
                            UserFacade userFacade = new UserFacade();

                            // If they are not friends make sure sender is above fame level 24
                            if (!userFacade.AreFriends(m_userId, user_id))
                            {
                                // Check to see if sender is above fame level 24
                                FameFacade fameFacade = new FameFacade();
                                UserFame userFame = fameFacade.GetUserFame(user_id, (int)FameTypes.World);

                                if (userFame.CurrentLevel.LevelNumber < 24)
                                {
                                    // Allowed to send a PM
                                    responseStr = "<Result>\r\n  <ReturnCode>-2</ReturnCode>\r\n  <ResultDescription>Not friends or world fame level 24</ResultDescription>\r\n</Result>";
                                    return;
                                }
                            }

                            // Insert the message
                            Message CMessage = new Message(0, m_userId, user_id, subject,
                                message, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                            int ret = userFacade.InsertMessage(CMessage);

                            if (ret > 0)
                            {
                                responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
                            }
                            else
                            {
                                responseStr = "<Result>\r\n  <ReturnCode>" + ret + "</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
                            }

                            Response.Write(responseStr);
                            return;
                        }
                        else
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Username not found</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    }
                    else if (type.Equals("Friends"))
                    {
                        if (Request.Params["toId1"] == null)
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>At least one toId must be specified for Friends</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }

                        int ii = 1;

                        responseStr = "<Result>\r\n  ";

                        do
                        {
                            int toId = Int32.Parse(Request.Params["toId" + ii].ToString());

                            UserFacade userFacade = new UserFacade();

                             // Insert the message
                            Message CMessage = new Message(0, m_userId, toId, subject,
                                message, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                            int ret = userFacade.InsertMessage(CMessage);

                            if (ret > 0)
                            {
                                responseStr += "<ReturnCode" + toId + ">0</ReturnCode" + toId + ">\r\n  <ResultDescription" + toId + ">success</ResultDescription" + toId + ">\r\n";
                            }
                            else
                            {
                                responseStr += "<ReturnCode" + toId + ">" + ret + "</ReturnCode" + toId + ">\r\n  <ResultDescription" + toId + ">error sending messsage</ResultDescription" + toId + ">\r\n";
                            }

                            ii++;

                        } while (Request.Params["toId" + ii] == null);

                        responseStr += "\r\n</Result>";

                        Response.Write(responseStr);

                        return;
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>type specified undefined</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
