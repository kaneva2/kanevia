///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using System.IO;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for travellist.
    /// </summary>
    public class travelzonelist : KgpBasePage
    {

        public static WokUserInfo GetWokUserInfo()
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                WokUserInfo userInfo = (WokUserInfo)HttpContext.Current.Items["WokUserInfo"];
                return userInfo;
            }
            else
            {
                return null;
            }
        }

        protected const String DEFAULT_BROADBAND_ZONE = "DefaultBroadbandZone()"; // indicate to server, use default
        protected const String DEFAULT_PLAIN_BROADBAND_ZONE = "DefaultPlainBroadbandZone()"; // indicate to server, use default

        protected const String KANEVA_NAME = "Kaneva";

        protected const String DB_FALSE = "'F'";
        protected const String DB_TRUE = "'T'";

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(false))
            {

                string actionreq = "default";

                if (Request.Params["action"] != null)
                {
                    actionreq = Request.Params["action"];
                }

                #region Web Call Actions

                #region ClearAppNotification
                if (actionreq.Equals("ClearAppNotification"))
                {

                    if (Request.Params["gameId"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>gameId not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    GameFacade gameFacade = new GameFacade();
                    gameFacade.ClearNotificationAppRequest (GetUserId(), Int32.Parse(Request.Params["gameId"].ToString()));
                    gameFacade.ClearNotificationAppRequest (GetUserId(), 3296);  // This one is a global notification on Places Menu 'NEW'

                    Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Success</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>");
                    return;
                }
                #endregion

                #region GameNotificationCount
                if (actionreq.Equals("GameNotificationCount"))
                {
                    int gameId = 3296;
                    GameFacade gameFacade = new GameFacade();

                    if (Request.Params["gameId"] != null)
                    {
                        gameId = Int32.Parse(Request.Params["gameId"].ToString());
                    }

                    Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Success</ResultDescription>\r\n  " + PassthroughTag + "\r\n  <RequestCount>" + gameFacade.GetNumberOfNotificationAppRequest(GetUserId(), gameId).ToString() + "</RequestCount>\r\n</Result>");
                    return;
                }
                #endregion

                #region default
                if (actionreq.Equals("default"))
                {

                    if ((Request.Params["type"] == null) || (Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>zone type, page number, or items per page not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());
                    int zoneType = Int32.Parse(Request.Params["type"].ToString());
                    string searchZoneName = Request.Params["search"];

                    int placeType = 0;

                    if (Request.Params["placeType"] != null && KanevaGlobals.IsNumeric(Request.Params["placeType"].ToString ()))
                    {
                        placeType = Convert.ToInt32(Request.Params["placeType"]);
                    }

                    string orderBy = null;
                    string orderByDirection = null;
                    string orderbyclause = null;

                    if (Request.Params["orderBy"] != null)
                    {
                        orderBy = Request.Params["orderBy"];

                        if (Request.Params["orderByDirection"] != null)
                        {
                            orderByDirection = Request.Params["orderByDirection"];

                            if (orderByDirection.Equals("DESC"))
                            {
                                orderByDirection = " DESC ";
                            }
                            else
                            {
                                orderByDirection = " ASC ";
                            }
                        }

                        if (orderBy.Equals("Name"))
                        {
                            if (zoneType == Constants.BROADBAND_ZONE)
                            {
                                orderbyclause = " display_name_srt ";
                                orderbyclause += orderByDirection;
                            }
                            else if (zoneType == Constants.APARTMENT_ZONE)
                            {
                                orderbyclause = " name_srt ";
                                orderbyclause += orderByDirection;
                            }
                            else if (zoneType == Constants.PERMANENT_ZONE)
                            {
                                orderbyclause = " cz.name ";
                                orderbyclause += orderByDirection;
                            }
                        }
                        else if (orderBy.Equals("Type"))
                        {
                            orderbyclause = " display_name_srt ";
                            orderbyclause += orderByDirection;
                        }
                        else if ((orderBy.Equals("Owner")) && (zoneType == Constants.BROADBAND_ZONE))
                        {
                            orderbyclause = " username_srt ";
                            orderbyclause += orderByDirection;
                        }
                        else if ((orderBy.Equals("Population")) && (zoneType == Constants.BROADBAND_ZONE))
                        {
                            orderbyclause = " pop_count ";
                            orderbyclause += orderByDirection;
                        }
                        else if (orderBy.Equals("Population"))
                        {
                            orderbyclause = " count ";
                            orderbyclause += orderByDirection;
                        }
                        else if ((orderBy.Equals("Raves")) && (zoneType != Constants.PERMANENT_ZONE))
                        {
                            orderbyclause = " wok_raves ";
                            orderbyclause += orderByDirection;
                        }
                    }

                    string xmlResponse = "";

                    switch (zoneType)
                    {
                        case Constants.APARTMENT_ZONE:
                            xmlResponse = GetApartmentsXML(page, items_per_page, searchZoneName, orderbyclause);
                            break;

                        case Constants.BROADBAND_ZONE:
                            xmlResponse = GetBroadbandXML(page, items_per_page, searchZoneName, placeType, orderbyclause);
                            break;

                        case Constants.PERMANENT_ZONE:
                            xmlResponse = GetKanevaXML(page, items_per_page, searchZoneName, orderbyclause);
                            break;

                        default:
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invalid zone type specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                            Response.Write(errorStr);
                            return;
                    }

                    Response.Write(xmlResponse);
                }
                #endregion

                #region getTop
                else if (actionreq.Equals ("getTop"))
                {
                    if (Request.Params["type"] == null || (Request.Params["time"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>zone type or time not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int zoneType = Int32.Parse(Request.Params["type"].ToString());
                    string time = Request.Params["time"].ToString(); // D, W, M, AT
                    string placeType = "";
                    if (Request.Params["placeType"] != null)
                    {
                        placeType = Request.Params["placeType"].ToString();
                    }

                    string orderBy = null;
                    string orderByDirection = null;
                    string orderbyclause = null;

                    if (Request.Params["orderBy"] != null)
                    {
                        orderBy = Request.Params["orderBy"];

                        if (Request.Params["orderByDirection"] != null)
                        {
                            orderByDirection = Request.Params["orderByDirection"];

                            if (orderByDirection.Equals("DESC"))
                            {
                                orderByDirection = " DESC ";
                            }
                            else
                            {
                                orderByDirection = " ASC ";
                            }
                        }

                        if (orderBy.Equals("Name"))
                        {
                            orderbyclause = " name ";
                            orderbyclause += orderByDirection;
                        }
                        else if (orderBy.Equals("Type"))
                        {
                            orderbyclause = " display_name ";
                            orderbyclause += orderByDirection;
                        }
                        else if ((orderBy.Equals("Owner")) && (zoneType == Constants.BROADBAND_ZONE))
                        {
                            orderbyclause = " username ";
                            orderbyclause += orderByDirection;
                        }
                        else if ((orderBy.Equals("Raves")) && (zoneType != Constants.PERMANENT_ZONE))
                        {
                            orderbyclause = " raves ";
                            orderbyclause += orderByDirection;
                        }
                    }
                    // Filtered by Age and Country now
                    string strCountry = "";

                    // Is user in the list of countries we populate?
                    DataRow [] drCountry = GetCountries().Select("country = '" + KanevaWebGlobals.CurrentUser.Country + "'");

                    // And they have the preference
                    if (drCountry.Length > 0 && KanevaWebGlobals.CurrentUser.Preference.FilterByCountry)
                    {
                        strCountry = drCountry[0]["country"].ToString();
                    }

                    string xmlResponse = "";

                    switch (zoneType)
                    {
                        case Constants.APARTMENT_ZONE:
                            xmlResponse = GetTopApartmentsXML(orderbyclause, time, strCountry, KanevaWebGlobals.CurrentUser.Age);
                            break;

                        case Constants.BROADBAND_ZONE:
                            xmlResponse = GetTopBroadbandXML(orderbyclause, time, placeType, strCountry, KanevaWebGlobals.CurrentUser.Age);
                            break;

                        default:
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invalid zone type specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                            Response.Write(errorStr);
                            return;
                    }

                    Response.Write(xmlResponse);
                }
                #endregion

                #region getTopPlaces
                else if (actionreq.Equals("getTopPlaces"))
                {
                    SubscriptionFacade subscriptionFacade = new SubscriptionFacade();

                    User user = KanevaWebGlobals.CurrentUser;

                    // For now 2 groups, under 18, and 18 and up
                    bool is18AndUp = (user.Age >= 18);
                    bool bHasAPSubscription = user.HasAccessPass;
                    bool bShowOnlyAP = (ShowOnlyAP && bHasAPSubscription);

                    // 3dapps
                    GameFacade gamefacade = new GameFacade();
                    PagedDataTable pdt = null;
                    //pdt = gamefacade.GetTopGames(1, 10);

                    pdt = GetTop3Dapps30Days(bHasAPSubscription, bShowOnlyAP, "", is18AndUp);

                    if (pdt == null)
                    {
                        pdt = new PagedDataTable();
                    }

                    if (user.Stats.NumberOfFriends > 0)
                    {
                        PagedDataTable pdtOnline3dAPP = GetOnlineFriends3DApp(user.UserId);

                        if (pdtOnline3dAPP.Rows.Count > 0)
                        {
                            foreach (DataRow dr in pdtOnline3dAPP.Rows)
                            {
                                if (pdt.Rows.Count > 9)
                                {
                                    pdt.Rows.RemoveAt(pdt.Rows.Count - 1);
                                }

                                DataRow newRow = pdt.NewRow();
                                newRow.ItemArray = dr.ItemArray;
                                pdt.Rows.InsertAt(newRow, 0);
                            }
                        }
                    }

                    DataSet ds = new DataSet();

                    ds.DataSetName = "Top Places";

                    pdt.TableName = "Top 3DApps";
                    ds.Tables.Add(pdt);


                    PagedDataTable pdt2 = null;
                    pdt2 = GetTopHangout30Days(bHasAPSubscription, bShowOnlyAP, "", is18AndUp);

                    if (pdt2 == null)
                    {
                        pdt2 = new PagedDataTable();
                    }
                    
                    pdt2.TableName = "Top Communities";
                    

                   if (user.Stats.NumberOfFriends > 0)
                    {
                       PagedDataTable pdtOnlineWok = GetOnlineFriendsWOK (user.UserId);

                       if (pdtOnlineWok.Rows.Count > 0)
                       {
                           foreach (DataRow dr in pdtOnlineWok.Rows)
                           {
                               if (pdt2.Rows.Count > 9)
                               {
                                pdt2.Rows.RemoveAt(pdt2.Rows.Count - 1);
                               }

                               DataRow newRow = pdt2.NewRow();
                               newRow.ItemArray = dr.ItemArray;
                               pdt2.Rows.InsertAt(newRow, 0);

                           }
                       }
                   }

                   ds.Tables.Add(pdt2);

                    // Friends Picks
                    PagedDataTable pdt3 = new PagedDataTable ();
                    
                    if (user.Stats.NumberOfFriends > 0)
                    {
                        pdt3 = GetTopFriendPicks(user.UserId);
                    }
                    pdt3.TableName = "Top Friend Picks";
                    ds.Tables.Add(pdt3);


                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(ds.GetXml());
                    XmlNode root = doc.DocumentElement;

                    int numRecords = pdt3.Rows.Count;
                    XmlElement elem = doc.CreateElement("NumberFriendPickRecords");
                    elem.InnerText = numRecords.ToString();
                    root.InsertBefore(elem, root.FirstChild);

                    numRecords = pdt2.Rows.Count;
                    elem = doc.CreateElement("NumberCommunityRecords");
                    elem.InnerText = numRecords.ToString();
                    root.InsertBefore(elem, root.FirstChild);

                    numRecords = pdt.Rows.Count;
                    elem = doc.CreateElement("Number3DappRecords");
                    elem.InnerText = numRecords.ToString();
                    root.InsertBefore(elem, root.FirstChild);

                    //uint totalNumRecords = pdt.TotalCount;
                    //elem = doc.CreateElement("TotalNumberRecords");
                    //elem.InnerText = totalNumRecords.ToString();

                    //root.InsertBefore(elem, root.FirstChild);

                    InsertPassthroughTag(ref doc);

                    elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = "success";

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = "0";

                    root.InsertBefore(elem, root.FirstChild);

                    Response.Write(root.OuterXml);
                }
                #endregion

                #region myCommunity
                else if(actionreq.Equals("myCommunity"))
                {
                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>zone type, page number, or items per page not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    // Check to see if we need to include items purchased in shop that have a status of import
                    bool includeImports = false;
                    if (Request.Params["importList"] != null)
                    {
                        includeImports = Convert.ToBoolean (Request.Params["importList"]);
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    string orderByDirection = null;
                    string orderbyclause = null;

                    orderByDirection = Request.Params["orderByDirection"];

                    if (orderByDirection != null)
                    {
                        orderByDirection = orderByDirection.ToUpper();

                        if (orderByDirection.Equals("DESC"))
                        {
                            orderByDirection = " DESC ";
                        }
                        else
                        {
                            orderByDirection = " ASC ";
                        }
                    }
                    else
                    {
                        orderByDirection = " DESC ";
                    }

                    orderbyclause = Request.Params["orderBy"];

                    if (orderbyclause != null)
                    {
                        orderbyclause = orderbyclause.ToLower();

                        if (orderbyclause.Equals("name"))
                        {
                            orderbyclause = "name ";
                        }
                        else if (orderbyclause.Equals("type"))
                        {
                            orderbyclause = "cm.account_type_id ";
                        }
                        else if (orderbyclause.Equals("totalmembers"))
                        {
                            orderbyclause = "ha.number_of_members ";
                        }
                        else if (orderbyclause.Equals("raves"))
                        {
                            orderbyclause = "ha.wok_raves ";
                        }
                        else if (orderbyclause.Equals("population"))
                        {
                            orderbyclause = "ha.count ";
                        }
                        orderbyclause += orderByDirection;
                    }
                    else
                    {
                        // By default sort by population
                        orderbyclause = "ha.population ";
                        orderbyclause += orderByDirection;
                    }

                    string APonly = "";

                    if (ShowOnlyAP)
                    {
                        APonly = "ha.pass_group_id = 1";
                    }


                    bool onlyCommunitiesOwn = false;

                    string ownerOnly = null;

                    ownerOnly = Request.Params["ownerOnly"];

                    if (ownerOnly != null)
                    {
                        if (ownerOnly.Equals("true"))
                        {
                            onlyCommunitiesOwn = true;
                        }
                        else if (ownerOnly.Equals("false"))
                        {
                            onlyCommunitiesOwn = false;
                        }
                    }

                    PagedDataTable dtMyCommunities = UsersUtility.GetUserCommunitiesInWorld(GetUserId(), orderbyclause, APonly, onlyCommunitiesOwn, page, items_per_page);

                    if (onlyCommunitiesOwn)
                    {
                        DataSet ds = new DataSet();
                        ds.DataSetName = "Result";

                        // Hack for homes
                        DataRow drZone = WOKStoreUtility.GetOwnerChannelZone(m_userId);

                        PagedDataTable dtMyImportList = new PagedDataTable();
                        if (includeImports)
                        {
                            dtMyImportList = UsersUtility.GetUserZonesForImport (GetUserId (), "c.name", APonly, onlyCommunitiesOwn, page, items_per_page);
                        }

                        dtMyCommunities.TableName = "Zone";
                        if (dtMyImportList.Rows.Count > 0)
                        {
                            // import the rows
                            for (int i = 0; i < dtMyImportList.Rows.Count; i++)
                            {
                                dtMyCommunities.ImportRow (dtMyImportList.Rows[i]);
                            }
                            dtMyCommunities.TotalCount = dtMyCommunities.TotalCount + dtMyImportList.Rows.Count;
                        }
                        ds.Tables.Add (dtMyCommunities);
                        
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(ds.GetXml());
                        XmlNode root = doc.DocumentElement;

                        int numRecords = dtMyCommunities.Rows.Count;
                        XmlElement elem = doc.CreateElement("NumberRecords");
                        elem.InnerText = numRecords.ToString();

                        root.InsertBefore(elem, root.FirstChild);

                        int totalNumRecords = dtMyCommunities.TotalCount + dtMyImportList.TotalCount;
                        elem = doc.CreateElement("TotalNumberRecords");
                        elem.InnerText = totalNumRecords.ToString();

                        root.InsertBefore(elem, root.FirstChild);

                        InsertPassthroughTag(ref doc);

                        elem = doc.CreateElement("ReturnDescription");
                        elem.InnerText = "success";

                        root.InsertBefore(elem, root.FirstChild);

                        elem = doc.CreateElement("ReturnCode");
                        elem.InnerText = "0";

                        root.InsertBefore(elem, root.FirstChild);

                        Response.Write (root.OuterXml);
                    }
                    else
                    {
                       Response.Write(BuildXMLResponse(dtMyCommunities, "Result", "Zone"));
                    }



                }
                #endregion

                #region MostRaved
                else if (actionreq.Equals("MostRaved"))
                {
                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    int totalCount = 0;
                    string orderBy = "raves";
                    string orderByDirection = "desc";
                    string orderbyclause = orderBy + " " + orderByDirection;

                    GameFacade gameFacade = new GameFacade();
                    DataTable dtRaved = (DataTable)gameFacade.GetMostRaved3DApps(ShowOnlyAP, HideAP, ref totalCount, page, items_per_page, KanevaGlobals.WokGameId);
                    Response.Write(BuildXMLResponse(dtRaved, "Result", "MostRaved", totalCount));
                }
                #endregion


                #region MostPopulatedCombinedWithRequests

                else if (actionreq.Equals("MostRequested"))
                {
                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    GameFacade gameFacade = new GameFacade();

                    int totalNumRecords = 0;
                    DataTable dtCommunities = gameFacade.GetMostRequested (ShowOnlyAP, HideAP, GetUserId(), ref totalNumRecords, page, items_per_page, KanevaGlobals.WokGameId);

                    Response.Write(BuildXMLResponse(dtCommunities, "Result", "MostRequested", totalNumRecords));

                }

                else if (actionreq.Equals("MostPopulatedCombined"))
                {
                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    string countryFilterBy = "";
                    string countrySortBy = "";

                    if (Request.Params["country"] != null)
                    {
                        if (Request.Params["country"].Equals ("MineFirst"))
                        {
                            countrySortBy = KanevaWebGlobals.CurrentUser.Country;
                        }
                        else if (Request.Params["country"].Equals ("MineOnly"))
                        {
                            countryFilterBy  = KanevaWebGlobals.CurrentUser.Country;
                        }
                    }

                    int ageFilterBy = 0;
                    int ageSortBy = 0;

                    if (Request.Params["age"] != null)
                    {
                        if (Request.Params["age"].Equals("MineFirst"))
                        {
                            ageSortBy = KanevaWebGlobals.CurrentUser.Age;
                        }
                        else if (Request.Params["age"].Equals("MineOnly"))
                        {
                            ageFilterBy = KanevaWebGlobals.CurrentUser.Age;
                        }
                    }


                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    // Allow client to pass offset, this overrides pagenumber
                    int offset = -1;

                    if (Request.Params["offset"] != null)
                    {
                        offset = Int32.Parse(Request.Params["offset"].ToString());
                    }

                    GameFacade gameFacade = new GameFacade();

                    int totalNumRecords = 0;
                    DataTable dtCommunities = gameFacade.MostPopulatedCombined(ShowOnlyAP, HideAP, GetUserId(), ref totalNumRecords, page, items_per_page, KanevaGlobals.WokGameId, countryFilterBy, countrySortBy, ageFilterBy, ageSortBy,offset);

                    IncludeCanDropGems(ref dtCommunities);

                    Response.Write(BuildXMLResponse(dtCommunities, "Result", "MostPopulatedCombined", totalNumRecords));
                }
                #endregion

                #region MostPopulated
                else if (actionreq.Equals("MostPopulated"))
                {
                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int totalCount = 0;
                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());


                    GameFacade gameFacade = new GameFacade();
                    DataTable dtPopulated = (DataTable)gameFacade.GetMostPopulated3DApps(ShowOnlyAP, HideAP, ref totalCount, page, items_per_page, KanevaGlobals.WokGameId);

                    IncludeCanDropGems(ref dtPopulated);

                    Response.Write(BuildXMLResponse(dtPopulated, "Result", "MostPopulated", totalCount));
                }
                #endregion

                #region getMostVisited
                else if (actionreq.Equals("getMostVisited"))
                {
                    if ((Request.Params["max"] == null) || (Request.Params["constrainCount"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>max or constrainCount not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());
                    int constrainCount = Int32.Parse(Request.Params["constrainCount"].ToString());

                    GameFacade gameFacade = new GameFacade();
                    DataTable dtVisits = (DataTable)gameFacade.GetMostVisited(GetUserId(), HideAP, 1, items_per_page, constrainCount, KanevaGlobals.WokGameId);

                    IncludeCanDropGems(ref dtVisits);

                    Response.Write(BuildXMLResponse(dtVisits, "Result", "Vists"));
                }
                #endregion

                #region getRandomOnlineFriends
                else if (actionreq.Equals("getRandomOnlineFriends"))
                {
                    if (Request.Params["max"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>max not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());
                    PagedDataTable dtRandomFrieds = GetRandomFriends(GetUserId(), items_per_page);
                    Response.Write(BuildXMLResponse(dtRandomFrieds, "Result", "Friends"));
                }
                #endregion



                #region searchCommunities Action
                else if (actionreq.Equals("SearchConvert"))
                {
                    if ((Request.Params["SearchLogId"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>SearchLogId not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    GetUserFacade.SearchConvert(Convert.ToInt32(Request.Params["SearchLogId"]));

                    Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Sucess</ResultDescription>\r\n\r\n  </Result>");
                    return;
                }
                    

                else if (actionreq.Equals("searchCommunities"))
                {

                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    string searchZoneName = "";
                    if (Request.Params["search"] != null)
                    {
                        searchZoneName = Request.Params["search"].ToString();
                        int searchLogId = GetUserFacade.LogSearch(GetUserId(), searchZoneName, (int)Constants.eSEARCH_TYPE.WORLD);
                    }

                    int placeTypeId = -1;
                    if (Request.Params["placeType"] != null)
                    {
                        try
                        {
                            placeTypeId = Convert.ToInt32(Request.Params["placeType"]);
                        }
                        catch (Exception) { };
                    }

                    bool myCommunitiesOnly = false;
                    if (Request.Params["myCommunitiesOnly"] != null)
                    {
                        try
                        {
                            myCommunitiesOnly = Convert.ToBoolean(Request.Params["myCommunitiesOnly"]);
                        }
                        catch (Exception) { }
                    }

                    string countryFilter = "";
                    if (Request.Params["country"] != null)
                    {
                        try
                        {
                            if (Request.Params["country"].ToString().Equals("T"))
                            {
                                countryFilter = KanevaWebGlobals.CurrentUser.Country;
                            }
                        }
                        catch (Exception) { }
                    }


                    int minAge = 0;
                    if (Request.Params["minAge"] != null)
                    {
                        try
                        {
                            minAge = Convert.ToInt32(Request.Params["minAge"]);
                        }
                        catch (Exception) { }
                    }

                    int maxAge = 0;
                    if (Request.Params["maxAge"] != null)
                    {
                        try
                        {
                            maxAge = Convert.ToInt32(Request.Params["maxAge"]);
                        }
                        catch (Exception) { }
                    }



                    // Get cached version
                    if ((myCommunitiesOnly.Equals(false)) && (placeTypeId < 1) && (searchZoneName.Length.Equals(0)))
                    {
                        // Build a unique cache key
                        //string cacheKey = "SearchCom" + page + "p" + items_per_page + "A" + ShowOnlyAP;
                        //string xmlResponse = (string)Cache[cacheKey];

                        // New requirement, don't mix AP and non AP
                        bool bShowAP = ShowOnlyAP & !HideAP;

                        string cacheKey = "SearchComA" + bShowAP + "c" + countryFilter + "min" + minAge.ToString() + "max" + maxAge.ToString();
                        PagedDataTable dtCachedCommunities = (PagedDataTable)Cache[cacheKey];

                        if (dtCachedCommunities == null)
                        {
                            Random _rand = new Random();
                            int primaryKey = 0;

                            // Get top 20 by population
                            int totalCount = 0;
                            GameFacade gameFacade = new GameFacade();
                            DataTable dtCommunities = gameFacade.SearchHangouts(bShowAP, true, true, false, 1, 200, "", placeTypeId, false, 0, "pop_count DESC", KanevaGlobals.WokGameId, countryFilter, minAge, maxAge, ref totalCount);

                            dtCommunities.Columns.Add(new DataColumn("rndSortId", typeof(int)));
                            dtCommunities.Columns.Add(new DataColumn("id", typeof(int)));

                            for (int i = 0; i < dtCommunities.Rows.Count; i++)
                            {
                                if (i < 10)
                                {
                                    dtCommunities.Rows[i]["rndSortId"] = _rand.Next(40000, 50000);
                                }
                                else if (i < 20)
                                {
                                    dtCommunities.Rows[i]["rndSortId"] = _rand.Next(30000, 40000);
                                }
                                else if (i < 30)
                                {
                                    dtCommunities.Rows[i]["rndSortId"] = _rand.Next(20000, 30000);
                                }
                                else
                                {
                                    dtCommunities.Rows[i]["rndSortId"] = _rand.Next(10000);
                                }
                            }

                            dtCachedCommunities = new PagedDataTable();

                            // Copy column structure
                            //dtCachedCommunities = (PagedDataTable)dtCommunities.Clone();
                            for (int i = 0; i < dtCommunities.Columns.Count; i++)
                            {
                                dtCachedCommunities.Columns.Add(dtCommunities.Columns[i].ColumnName, dtCommunities.Columns[i].DataType);
                            }

                            DataRow[] drResults = dtCommunities.Select("", "rndSortId DESC", DataViewRowState.CurrentRows);

                            dtCachedCommunities.TotalCount = drResults.Length;

                            for (int i = 0; i < drResults.Length; i++)
                            {
                                DataRow newRow = dtCachedCommunities.NewRow();
                                drResults[i]["id"] = primaryKey;
                                primaryKey++;
                                newRow.ItemArray = drResults[i].ItemArray;
                                dtCachedCommunities.Rows.InsertAt(newRow, 0);
                            }

                            // Now add extra by raves with no population - http://wiki.kaneva.com/mediawiki/index.php/2012_Roadmap_for_Consumers#Dashboard_2.0
                            // 3.Not populated, then by raves. Again, randomized most raved, so not in most to least order. 
                            //PagedDataTable pdtFullResult = CommunityUtility.SearchHangouts(ShowOnlyAP, true, true, "", "", 0, "sap.count IS NULL", "number_of_diggs desc", 1, 30, placeTypeId, false, 0);
                            DataTable pdtFullResult = gameFacade.SearchHangouts(bShowAP, true, false, true, 1, 30, "", placeTypeId, false, 0, "number_of_diggs DESC", KanevaGlobals.WokGameId, countryFilter, minAge, maxAge, ref totalCount);
                            pdtFullResult.Columns.Add(new DataColumn("rndSortId", typeof(int)));
                            pdtFullResult.Columns.Add(new DataColumn("id", typeof(int)));

                            //// Copy column structure
                            ////dtCachedCommunities = (PagedDataTable)dtCommunities.Clone();
                            //for (int i = 0; i < dtCachedCommunities.Columns.Count; i++)
                            //{
                            //    pdtFullResult.Columns.Add(dtCachedCommunities.Columns[i].ColumnName, dtCachedCommunities.Columns[i].DataType);
                            //}

                            if (pdtFullResult.Rows.Count > 0)
                            {
                                foreach (DataRow dr in pdtFullResult.Rows)
                                {
                                    dr["rndSortId"] = 100;
                                    dr["id"] = primaryKey;
                                    primaryKey++;
                                    DataRow newRow = dtCachedCommunities.NewRow();
                                    newRow.ItemArray = dr.ItemArray;
                                    dtCachedCommunities.Rows.InsertAt(newRow, 0);

                                    dtCachedCommunities.TotalCount++;
                                }
                            }

                            Cache.Insert(cacheKey, dtCachedCommunities, null, DateTime.Now.AddSeconds(60), System.Web.Caching.Cache.NoSlidingExpiration);

                        }

                        // Now get the correct page
                        PagedDataTable pdtPagedResults = new PagedDataTable();
                        pdtPagedResults = (PagedDataTable)dtCachedCommunities.Clone();
                        pdtPagedResults.TotalCount = dtCachedCommunities.TotalCount;

                        int FromID = ((page - 1) * items_per_page);
                        int ToID = page * items_per_page - 1;

                        DataRow[] drPaged = dtCachedCommunities.Select("id >= " + FromID + " AND id <= " + ToID + "", "", DataViewRowState.CurrentRows);

                        for (int i = 0; i < drPaged.Length; i++)
                        {
                            DataRow newRow = pdtPagedResults.NewRow();
                            newRow.ItemArray = drPaged[i].ItemArray;
                            pdtPagedResults.Rows.InsertAt(newRow, 0);
                        }

                        //Response.Write(xmlResponse);
                        Response.Write(BuildXMLResponse(pdtPagedResults, "Result", "Communities"));
                    }
                    else
                    {
                        int totalCount = 0;
                        GameFacade gameFacade = new GameFacade();
                        DataTable dtCommunities = gameFacade.SearchHangouts(IncludeAPCommunities, ShowOnlyAP, false, false, page, items_per_page, searchZoneName, placeTypeId, myCommunitiesOnly, GetUserId(), "", KanevaGlobals.WokGameId, countryFilter, minAge, maxAge, ref totalCount);
                        Response.Write(BuildXMLResponse(dtCommunities, "Result", "Communities", totalCount));
                    }
                }
                #endregion

                #region Browse3DAppsByRequest
                else if (actionreq.Equals("Browse3DAppsByRequest"))
                {
                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    GameFacade gameFacade = new GameFacade();

                    int totalNumRecords = 0;
                    DataTable dtCommunities = gameFacade.Browse3DAppsByRequest(ShowOnlyAP, HideAP, GetUserId(), ref totalNumRecords, page, items_per_page, KanevaGlobals.WokGameId);

                    Response.Write(BuildXMLResponse(dtCommunities, "Result", "3DApps", totalNumRecords));
                }
                #endregion


                // A/B Test variant
                #region Browse3DAppsByRequestB
                else if (actionreq.Equals("Browse3DAppsByRequestB"))
                {
                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    GameFacade gameFacade = new GameFacade();

                    int totalNumRecords = 0;
                    DataTable dtCommunities = gameFacade.Browse3DAppsByRequestB(ShowOnlyAP, GetUserId(), ref totalNumRecords, page, items_per_page, KanevaGlobals.WokGameId);

                    Response.Write(BuildXMLResponse(dtCommunities, "Result", "3DApps", totalNumRecords));
                }
                #endregion

                #region search3DApps OR MostVisited
                else if (actionreq.Equals("search3DApps") || actionreq.Equals("MostVisited"))
                {
                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    int searchLogId = 0;
                    string searchZoneName = "";
                    if (Request.Params["search"] != null)
                    {
                        searchZoneName = Request.Params["search"].ToString();
                        searchLogId = GetUserFacade.LogSearch(GetUserId(), searchZoneName, (int)Constants.eSEARCH_TYPE.WORLD);
                    }


                    bool myCommunitiesOnly = false;
                    bool onlyOwned = false;
                    if (Request.Params["myCommunitiesOnly"] != null)
                    {
                        try
                        {
                            myCommunitiesOnly = Convert.ToBoolean(Request.Params["myCommunitiesOnly"]);

                            if (Request.Params["onlyOwned"] != null)
                            {
                                onlyOwned = Convert.ToBoolean(Request.Params["onlyOwned"]);
                            }
                        }
                        catch (Exception) { }
                    }

                    string orderBy = "raves";
                    string orderByDirection = "desc";
                    string orderbyclause = orderBy + " " + orderByDirection;

                    // Get cached version
                    if ((myCommunitiesOnly.Equals(false)) && (searchZoneName.Length.Equals(0)) && !ShowOnlyAP)
                    {
                        // Build a unique cache key
                        string cacheKey = "Search3D" + page + "p" + items_per_page + "A" + ShowOnlyAP + "H"+ HideAP;
                        string xmlResponse = (string)Cache[cacheKey];

                        if (xmlResponse == null)
                        {
                            // Add to the cache
                            //PagedDataTable dtCommunities = CommunityUtility.Search3Dapps(ShowOnlyAP, true, true, searchZoneName, "", 0, "", orderbyclause, page, items_per_page, -1, myCommunitiesOnly, GetUserId());
                            GameFacade gameFacade = new GameFacade();
                            int totalCount = 0;
                            DataTable dtCommunities = gameFacade.Top3Dapps(ShowOnlyAP, HideAP, page, items_per_page, ref totalCount, KanevaGlobals.WokGameId, true);

                            IncludeCanDropGems(ref dtCommunities);

                            PagedDataTable dtTemp = new PagedDataTable();
                            //dtTemp = dtCommunities.Clone();
                            //dtTemp = (PagedDataTable) dtCommunities.Copy();

                            if (dtCommunities.Columns.Count > 0)
                            {
                                foreach (DataColumn dc in dtCommunities.Columns)
                                {
                                    dtTemp.Columns.Add(dc.ColumnName, dc.DataType);
                                }
                            }

                            if (dtCommunities.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dtCommunities.Rows)
                                {
                                    DataRow newRow = dtTemp.NewRow();
                                    newRow.ItemArray = dr.ItemArray;
                                    dtTemp.Rows.Add(newRow);
                                }
                            }


                            dtTemp.TotalCount = totalCount;

                            xmlResponse = BuildXMLResponse(dtTemp, "Result", "3DApps");
                            Cache.Insert(cacheKey, xmlResponse, null, DateTime.Now.AddSeconds(60), System.Web.Caching.Cache.NoSlidingExpiration);
                        }
                        else
                        {
                            // Passthrough replacement for cached results
                            ReplacePassthroughTag(ref xmlResponse);
                        }

                        Response.Write(xmlResponse);

                    }
                    else
                    {
                        int totalNumRecords = 0;

                        // Filtering by community type
                        int[] communityTypes;
                        int communityType = 0;

                        if (!string.IsNullOrWhiteSpace(Request.Params["communityType"]) && Int32.TryParse(Request.Params["communityType"], out communityType))
                        {
                            // Group "Homes" with normal communities
                            if (communityType == (int)CommunityType.COMMUNITY)
                            {
                                communityTypes = new int[] { (int)CommunityType.COMMUNITY, (int)CommunityType.HOME };
                            }
                            else
                            {
                                communityTypes = new int[] { communityType };
                            }
                        }
                        else
                        {
                            communityTypes = new int[] { (int)CommunityType.APP_3D, (int)CommunityType.COMMUNITY, (int)CommunityType.HOME };
                        }

                        string searchOrderBy = (string.IsNullOrWhiteSpace(searchZoneName) ? "created_date DESC" : string.Empty);

                        GameFacade gameFacade = new GameFacade();
                        DataTable dtCommunities = gameFacade.Search3DApps(ShowOnlyAP, !HideAP, searchZoneName,
                            false, "", communityTypes, searchOrderBy, page, items_per_page, -1, myCommunitiesOnly, onlyOwned, GetUserId(), KanevaGlobals.WokGameId, ref totalNumRecords);

                        // Special case for new users who are checking this before sphinx can update
                        if (myCommunitiesOnly && searchZoneName.Length.Equals(0) && totalNumRecords == 0)
                        {
                            dtCommunities = gameFacade.GetUserHomeWorldForSearch(GetUserId(), KanevaGlobals.WokGameId, ref totalNumRecords);
                        }

                        IncludeCanDropGems(ref dtCommunities);

                        // PagedDataTable dtCommunities = CommunityUtility.Search3Dapps(ShowOnlyAP, true, true, searchZoneName, "", 0, "", orderbyclause, page, items_per_page, -1, myCommunitiesOnly, GetUserId());
                        Response.Write(BuildXMLResponse(dtCommunities, "Result", "3DApps", totalNumRecords, searchZoneName, searchLogId));
                    }


                }
                #endregion

                #region Browse3DAppsByCategory
                else if (actionreq.Equals("Browse3DAppsByCategory"))
                {
                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null) || (Request.Params["categoryName"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start, max, or categoryName must not specified</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());
                    string category = Request.Params["categoryName"].ToString();
                    int totalNumRecords = 0;

                    GameFacade gameFacade = new GameFacade();

                    DataTable dtGames = gameFacade.GetWorldsByCategory(ShowOnlyAP, HideAP, ref totalNumRecords, page, items_per_page, KanevaGlobals.WokGameId, category);
                    Response.Write(BuildXMLResponse(dtGames, "Result", "3DApps", totalNumRecords));
                }
                #endregion

                #region TopWorldsTour
                else if (actionreq.Equals("TopWorldsTour"))
                {
                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start, max</ResultDescription>\r\n" + PassthroughTag + "\r\n  </Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());
                    int totalNumRecords = 0;

                    GameFacade gameFacade = new GameFacade();

                    DataTable dtGames = gameFacade.TopWorldsTour(m_userId, ShowOnlyAP, HideAP, ref totalNumRecords, page, items_per_page, KanevaGlobals.WokGameId);
                    IncludeCanDropGems(ref dtGames);

                    Response.Write(BuildTourResponse(dtGames, "Result", "3DApps", totalNumRecords, KanevaWebGlobals.CurrentUser.Username, Configuration.TourRewardsLooter, Configuration.TourRewardsWorldOwner));
                }
                #endregion

                #endregion
            }
        }

        private void IncludeCanDropGems(ref DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                bool gem_drop = false;
                dt.Columns.Add("can_drop_gems", System.Type.GetType("System.String"));

                foreach (DataRow row in dt.Rows)
                {
                    try
                    {
                        gem_drop = false;
                        if (row["community_id"] != null && row["creator_id"] != null)
                        {
                            gem_drop = GetGameFacade.CanWorldDropGemChests(Convert.ToInt32(row["creator_id"]), Convert.ToInt32(row["community_id"]));
                        }

                        row["can_drop_gems"] = gem_drop.ToString();
                    }
                    catch { }
                }
            }
        }

        #region Xml Manipulation

        /// <summary>
        /// BuildXMLResponse
        /// </summary>
        private string BuildHackedXMLResponse(PagedDataTable pdt, string dataSetName, string tableName)
        {
            DataSet ds = new DataSet();
            ds.DataSetName = dataSetName;

            pdt.TableName = tableName;
            ds.Tables.Add(pdt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            int numRecords = pdt.Rows.Count;
            XmlElement elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = pdt.TotalCount;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            InsertPassthroughTag(ref doc);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            return root.OuterXml;
        }

        /// <summary>
        /// BuildXMLResponse
        /// </summary>
        private string BuildXMLResponse(PagedDataTable pdt, string dataSetName, string tableName)
        {
            DataSet ds = new DataSet();
            ds.DataSetName = dataSetName;

            pdt.TableName = tableName;
            ds.Tables.Add(pdt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            int numRecords = pdt.Rows.Count;
            XmlElement elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = pdt.TotalCount;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            InsertPassthroughTag(ref doc);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            return root.OuterXml;
        }

         /// <summary>
        /// BuildXMLResponse
        /// </summary>
        private string BuildXMLResponse(DataTable pdt, string dataSetName, string tableName)
        {
            return BuildXMLResponse(pdt, dataSetName, tableName, pdt.Rows.Count);
        }

        /// <summary>
        /// BuildXMLResponse
        /// </summary>
        private string BuildXMLResponse(DataTable pdt, string dataSetName, string tableName, int totalNumRecords)
        {
            return BuildXMLResponse(pdt, dataSetName, tableName, totalNumRecords, string.Empty, 0);
        }

        /// <summary>
        /// BuildXMLResponse
        /// </summary>
        private string BuildXMLResponse(DataTable pdt, string dataSetName, string tableName, int totalNumRecords, string searchString, int searchLogId)
        {
            //DataSet ds = new DataSet();
            DataSet ds = pdt.DataSet;
            ds.DataSetName = dataSetName;

            pdt.TableName = tableName;
            // ds.Tables.Add(pdt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("Search");
            elem.InnerText = searchString;

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("SearchLogId");
            elem.InnerText = searchLogId.ToString ();

            root.InsertBefore(elem, root.FirstChild);

            int numRecords = pdt.Rows.Count;
            elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            InsertPassthroughTag(ref doc);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            return root.OuterXml;
        }

        /// <summary>
        /// BuildXMLResponse
        /// </summary>
        private string BuildTourResponse(DataTable pdt, string dataSetName, string tableName, int totalNumRecords, string username, double TourRewardsLooter, double TourRewardsWorldOwner)
        {
            //DataSet ds = new DataSet();
            DataSet ds = pdt.DataSet;
            ds.DataSetName = dataSetName;

            pdt.TableName = tableName;
            // ds.Tables.Add(pdt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("Username");
            elem.InnerText = username;

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("TourRewardsLooter");
            elem.InnerText = TourRewardsLooter.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("TourRewardsWorldOwner");
            elem.InnerText = TourRewardsWorldOwner.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int numRecords = pdt.Rows.Count;
            elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            InsertPassthroughTag(ref doc);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            return root.OuterXml;
        }

        /// <summary>
        /// GetApartmentsXML
        /// </summary>
        private string GetApartmentsXML(Int32 pageNumber, Int32 pageSize, String search, String orderBy)
        {
            SubscriptionFacade subscriptionFacade = new SubscriptionFacade();
            GameFacade gameFacade = new GameFacade();

            bool bHasAPSubscription = KanevaWebGlobals.CurrentUser.HasAccessPass;
            bool bShowOnlyAP = (ShowOnlyAP && bHasAPSubscription);
            int totalCount = 0;

            // Cache all variations here besides a search, and only pages 1-5
            if (search != null && search.Length > 0 || pageNumber < 5)
            {

                DataTable dtResult = gameFacade.SearchApartments(bHasAPSubscription, bShowOnlyAP, pageNumber, pageSize, search, orderBy, ref totalCount);
                return BuildXMLResponse(dtResult, "Result", "Zone", totalCount);
            }
            else
            {
                // Build a unique cache key
                string cacheKey = "APTp" + pageNumber + "p" + pageSize + "o" + orderBy + "M" + bHasAPSubscription + "A" + bShowOnlyAP;
                string xmlResponse = (string)Cache[cacheKey];

                if (xmlResponse == null)
                {
                    // Add to the cache
                    DataTable dtResult = gameFacade.SearchApartments(bHasAPSubscription, bShowOnlyAP, pageNumber, pageSize, search, orderBy, ref totalCount);
                    xmlResponse = BuildXMLResponse(dtResult, "Result", "Zone", totalCount);
                    Cache.Insert(cacheKey, xmlResponse, null, DateTime.Now.AddSeconds(30), System.Web.Caching.Cache.NoSlidingExpiration);
                }
                else
                {
                    ReplacePassthroughTag(ref xmlResponse);
                }

                return xmlResponse;
            }
        }

        /// <summary>
        /// GetBroadbandXML
        /// </summary>
        private string GetBroadbandXML(Int32 pageNumber, Int32 pageSize, String search, int placeType, String orderBy)
        {
            int totalCount = 0;
            SubscriptionFacade subscriptionFacade = new SubscriptionFacade();
            GameFacade gameFacade = new GameFacade();

            bool bHasAPSubscription = KanevaWebGlobals.CurrentUser.HasAccessPass;
            bool bShowOnlyAP = (ShowOnlyAP && bHasAPSubscription);

            // If user does not belong to any communities, use cache
            // Cache pages 1-5 only, not member specific part
            //if (KanevaWebGlobals.CurrentUser.Stats.CommunitiesIBelongTo
            //KanevaWebGlobals.CurrentUser.com
            // XXX

            if (search == null)
            {
                search = "";
            }

            if (orderBy == null)
            {
                orderBy = "";
            }

            DataTable dtResult = gameFacade.SearchHangouts(bHasAPSubscription, bShowOnlyAP, false, false, pageNumber, pageSize, search, placeType, false, 0, orderBy,  KanevaGlobals.WokGameId, "", 0,0, ref totalCount);
            return BuildXMLResponse(dtResult, "Result", "Zone", totalCount);
        }

        /// <summary>
        /// GetKanevaXML
        /// </summary>
        private string GetKanevaXML(Int32 pageNumber, Int32 pageSize, String search, String orderBy)
        {
            // Cache all variations here besides a search
            if (search != null && search.Length > 0)
            {
                return BuildXMLResponse(GetKaneva(pageNumber, pageSize, search, orderBy), "Result", "Zone");
            }
            else
            {
                // Build a unique cache key
                string cacheKey = "PERMp" + pageNumber + "p" + pageSize + "o" + orderBy;
                string xmlResponse = (string)Cache[cacheKey];

                if (xmlResponse == null)
                {
                    // Add to the cache
                    xmlResponse = BuildXMLResponse(GetKaneva(pageNumber, pageSize, search, orderBy), "Result", "Zone");
                    Cache.Insert(cacheKey, xmlResponse, null, DateTime.Now.AddSeconds(30), System.Web.Caching.Cache.NoSlidingExpiration);
                }
                else
                {
                    ReplacePassthroughTag(ref xmlResponse);
                }

                return xmlResponse;
            }
        }

        /// <summary>
        /// XML
        /// </summary>
        private string GetTopApartmentsXML(string orderBy, string time, string strCountry, int iAge)
        {
            SubscriptionFacade subscriptionFacade = new SubscriptionFacade();

            // For now 2 groups, under 18, and 18 and up
            bool is18AndUp = (iAge >= 18);
            bool bHasAPSubscription = KanevaWebGlobals.CurrentUser.HasAccessPass;
            bool bShowOnlyAP = (ShowOnlyAP && bHasAPSubscription);

            // Cache all variations here

            // Build a unique cache key
            string cacheKey = "APTtop" + orderBy + "M" + bHasAPSubscription + "T" + time + "C" + strCountry + "A" + is18AndUp.ToString() + "M" + bShowOnlyAP;
            string xmlResponse = (string)Cache[cacheKey];

            if (xmlResponse == null)
            {
                // Add to the cache
                xmlResponse = BuildXMLResponse(GetTopApartments(bHasAPSubscription, bShowOnlyAP, orderBy, time, strCountry, is18AndUp), "Result", "Zone");
                Cache.Insert(cacheKey, xmlResponse, null, DateTime.Now.AddMinutes(5), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            else
            {
                ReplacePassthroughTag(ref xmlResponse);
            }

            return xmlResponse;
        }

        /// <summary>
        /// GetTopBroadbandXML
        /// </summary>
        private string GetTopBroadbandXML(string orderBy, string time, string placeType, string strCountry, int iAge)
        {
            SubscriptionFacade subscriptionFacade = new SubscriptionFacade();

            // For now 2 groups, under 18, and 18 and up
            bool is18AndUp = (iAge >= 18);
            bool bHasAPSubscription = KanevaWebGlobals.CurrentUser.HasAccessPass;
            bool bShowOnlyAP = (ShowOnlyAP && bHasAPSubscription);

            // Cache all variations here
            // Build a unique cache key
            string cacheKey = "BRDtop" + orderBy + "M" + bHasAPSubscription + "T" + time + "P" + placeType + "C" + strCountry + "A" + is18AndUp.ToString() + "M" + bShowOnlyAP;
            string xmlResponse = (string)Cache[cacheKey];

            if (xmlResponse == null)
            {
                // Add to the cache
                xmlResponse = BuildXMLResponse(GetTopBroadband(bHasAPSubscription, bShowOnlyAP, orderBy, time, placeType, strCountry, is18AndUp), "Result", "Zone");
                Cache.Insert(cacheKey, xmlResponse, null, DateTime.Now.AddMinutes(5), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            else
            {
                ReplacePassthroughTag(ref xmlResponse);
            }

            return xmlResponse;
        }

        #endregion Xml Manipulation

        #region DataTable Queries

        /// <summary>
        /// get list of Kaneva (permanent) zones
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        private PagedDataTable GetKaneva(Int32 pageNumber, Int32 pageSize, String search, String orderBy)
        {
            string dbName = KanevaGlobals.DbNameKGP;
            /*
            SELECT distinct cz.name, cz.zone_index,
                ifnull(server_name, '') AS server_name,
                ifnull(ap.`count`, 0) AS `count`
            FROM wok.channel_zones cz
                INNER JOIN wok.supported_worlds sw
                ON cz.zone_index_plain = sw.zone_index
                LEFT OUTER JOIN
                wok.summary_active_permanent_population ap ON ap.zone_index = cz.zone_index
            WHERE kaneva_user_id = 0
            order by count desc, name;
            */

            string selectList = " distinct cz.name, cz.zone_index, " +
                " sw.display_name, " +
                " ifnull(ap.`count`, 0) AS `count` ";

            string tableList = "wok.channel_zones cz " +
                                " INNER JOIN wok.supported_worlds sw" +
                                " ON cz.zone_index_plain = sw.zone_index" +
                                " LEFT OUTER JOIN" +
                                " wok.summary_active_permanent_population ap ON ap.zone_index = cz.zone_index";

            string whereClause = " zone_type = " + Constants.PERMANENT_ZONE.ToString() + " AND visibility >= " + Constants.SHOW_PERM_ZONE;





            string orderbyclause = null;

            if (orderBy != null && orderBy.Length > 0)
            {
                orderbyclause = orderBy;
            }
            else
            {
                // Default order by

                orderbyclause = "count desc, cz.name";
            }

            Hashtable parameters = new Hashtable();

            return KanevaGlobals.GetDatabaseUtilityReadOnly().GetPagedDataTable(selectList, tableList, whereClause, orderbyclause, parameters, pageNumber, pageSize);
        }

         /// <summary>
         /// get list of top apartments
         /// </summary>
        private PagedDataTable GetTopApartments(bool bHasAPSubscription, bool bShowOnlyAP, string orderBy, string strTime, string strCountry, bool is18AndUp)
         {
             Hashtable parameters = new Hashtable();
             string sqlString = " SELECT sumTop.zone_type, sumTop.zone_instance_id, sumTop.zone_index, sumTop.is_adult, sumTop.pass_group_id, sumTop.raves, sumTop.name, COALESCE(sap.count,0) as pop_count ";

             if (strTime.Equals("D"))
             {
                 if (strCountry.Length.Equals(0))
                 {
                     sqlString += " FROM ugc.summary_home_raves_latest_day sumTop ";
                 }
                 else
                 {
                     sqlString += " FROM ugc.summary_home_raves_latest_day_filtered sumTop ";
                 }
             }
             else if (strTime.Equals("W"))
             {
                 if (strCountry.Length.Equals(0))
                 {
                     sqlString += " FROM ugc.summary_home_raves_latest_week sumTop ";
                 }
                 else
                 {
                     sqlString += " FROM ugc.summary_home_raves_latest_week_filtered sumTop ";
                 }
             }
             else if (strTime.Equals("M"))
             {
                 if (strCountry.Length.Equals(0))
                 {
                     sqlString += " FROM ugc.summary_home_raves_latest_month sumTop ";
                 }
                 else
                 {
                     sqlString += " FROM ugc.summary_home_raves_latest_month_filtered sumTop ";
                 }
             }
             else
             {
                 if (strCountry.Length.Equals (0))
                 {
                     sqlString += " FROM ugc.summary_home_raves_all_time sumTop ";
                }
                else
                {
                    sqlString += " FROM ugc.summary_home_raves_all_time_filtered sumTop ";
                }
             }

            // Get the pop count
             sqlString += " LEFT JOIN wok.summary_active_population sap ON sumTop.zone_index = sap.zone_index AND sumTop.zone_instance_id = sap.zone_instance_id ";

             // Age
             if (is18AndUp)
             {
                 sqlString += " WHERE owner_age > 17 ";
             }
             else
             {
                 sqlString += " WHERE owner_age < 18 ";
             }

             // Filter by country
             if (strCountry.Length > 0)
             {
                 sqlString += " AND owner_country = @country ";
                 parameters.Add("@country", strCountry);
             }

             if (!bHasAPSubscription)
             {
                 sqlString += " AND pass_group_id <> 1 ";
             }
             else
             {
                 if (bShowOnlyAP)
                 {
                     sqlString += " AND pass_group_id = 1 ";
                 }
             }

             string orderbyclause = null;

             if (orderBy != null && orderBy.Length > 0)
             {
                 orderbyclause = orderBy;
             }
             else
             {
                 // Default order by
                 orderbyclause = "raves desc";
             }

             return KanevaGlobals.GetDatabaseUtilityUGC().GetPagedDataTableUnion(sqlString, orderbyclause, parameters, 1, 10, 4);
         }

         /// <summary>
         /// get list of top broadband
         /// </summary>
        private PagedDataTable GetTopBroadband(bool bHasAPSubscription, bool bShowOnlyAP, string orderBy, string strTime, string placeType, string strCountry, bool is18AndUp)
         {
             Hashtable parameters = new Hashtable();
             string sqlString = " SELECT sumTop.zone_type, sumTop.zone_instance_id, sumTop.place_type_id, sumTop.zone_index, sumTop.is_adult, sumTop.pass_group_id, sumTop.raves, sumTop.zone_name as name, sumTop.username, COALESCE(sap.count,0) as pop_count ";

             if (strTime.Equals("D"))
             {
                 if (strCountry.Length.Equals(0))
                 {
                     sqlString += " FROM ugc.summary_hangout_raves_latest_day sumTop ";
                 }
                else
                {
                    sqlString += " FROM ugc.summary_hangout_raves_latest_day_filtered sumTop ";
                }
             }
             else if (strTime.Equals("W"))
             {
                 if (strCountry.Length.Equals(0))
                 {
                     sqlString += " FROM ugc.summary_hangout_raves_latest_week sumTop ";
                }
                else
                {
                    sqlString += " FROM ugc.summary_hangout_raves_latest_week_filtered sumTop ";
                }
             }
             else if (strTime.Equals("M"))
             {
                 if (strCountry.Length.Equals(0))
                 {
                     sqlString += " FROM ugc.summary_hangout_raves_latest_month sumTop ";
                 }
                 else
                 {
                     sqlString += " FROM ugc.summary_hangout_raves_latest_month_filtered sumTop ";
                 }
             }
             else
             {
                 if (strCountry.Length.Equals(0))
                 {
                     sqlString += " FROM ugc.summary_hangout_raves_all_time sumTop ";
                 }
                else
                {
                    sqlString += " FROM ugc.summary_hangout_raves_all_time_filtered sumTop ";
                }
             }

             // Get the pop count
             sqlString += " LEFT JOIN wok.summary_active_population sap ON sumTop.zone_index = sap.zone_index AND sumTop.zone_instance_id = sap.zone_instance_id ";

             // Age
             if (is18AndUp)
             {
                 sqlString += " WHERE owner_age > 17 ";
             }
             else
             {
                 sqlString += " WHERE owner_age < 18 ";
             }

             // Filter by country
             if (strCountry.Length > 0)
             {
                 sqlString += " AND owner_country = @country ";
                 parameters.Add("@country", strCountry);
             }

             if (!bHasAPSubscription)
             {
                 sqlString += " AND pass_group_id <> 1 ";
             }
             else
             {
                 if (bShowOnlyAP)
                 {
                     sqlString += " AND pass_group_id = 1 ";
                 }
             }

             //Filter Place Type
             if (placeType != "" && placeType != null)
             {
                 sqlString += " AND place_type_id = @placeTypeId ";
                 parameters.Add("@placeTypeId", placeType);
             }

             string orderbyclause = null;

             if (orderBy != null && orderBy.Length > 0)
             {
                 orderbyclause = orderBy;
             }
             else
             {
                 // Default order by
                 orderbyclause = "raves desc";
             }

             return KanevaGlobals.GetDatabaseUtilityUGC().GetPagedDataTableUnion(sqlString, orderbyclause, parameters, 1, 10, 4);
         }

        private PagedDataTable GetRandomFriends(int userId, int pageSize)
        {
            Hashtable parameters = new Hashtable();

            // Per spec only hangouts and 3dapps...

            string sqlString = "(SELECT DISTINCT(cZone.name) as location, " +
               " wok.concat_vw_url('" + KanevaGlobals.WokGameId.ToString() + "',cZone.name,6) as STPURL, 0 as game_id, " +
               " u.username, u.user_id, u.display_name, 1 as online, com.thumbnail_small_path, com.thumbnail_medium_path, rand() as RandomOrder1 " +
               " FROM wok.players p  " +
                   " INNER JOIN wok.player_zones pz ON p.player_id = pz.player_id " +
                   " INNER JOIN wok.supported_worlds sw ON wok.zoneIndex(pz.current_zone_index) = sw.zone_index " +
                   " INNER JOIN wok.channel_zones cz ON pz.current_zone_index = cz.zone_index and pz.current_zone_instance_id = cz.zone_instance_id " +
                   " INNER JOIN kaneva.friends f ON f.friend_id = p.kaneva_user_id " +
                   " INNER JOIN kaneva.users u ON f.friend_id = u.user_id " +
                   " INNER JOIN kaneva.communities_personal com ON com.creator_id = u.user_id " +
                   " INNER JOIN kaneva.communities cZone ON cZone.community_id = cz.zone_instance_id " +
                   " WHERE cz.zone_type = 6 " +
                   " AND cZone.is_adult = 'N' " +
                   " AND cZone.is_public = 'Y' " +
                   " AND f.user_id = @userId " +
                   " AND p.in_game = 'T') " +
                " UNION " +
                    " (SELECT IF(length(c.name) = 0, 'unknown', c.name)  AS location,  wok.concat_vw_url(c.name,'',0) as STPURL,  al.game_id, " +
                    " u.username, u.user_id, u.display_name, 1 as online, com.thumbnail_small_path, com.thumbnail_medium_path, rand() as RandomOrder1 " +
                    " FROM developer.active_logins al " +
                    " INNER JOIN developer.games g ON al.game_id = g.game_id " +
                    " INNER JOIN wok.players p ON p.kaneva_user_id = al.user_id " +
                    " INNER JOIN kaneva.friends f ON f.friend_id = p.kaneva_user_id " +
                    " INNER JOIN kaneva.users u ON f.friend_id = u.user_id " +
                    " INNER JOIN kaneva.communities_personal com ON com.creator_id = u.user_id " +
                    " INNER JOIN kaneva.communities_game cg ON cg.game_id = g.game_id " +
                    " INNER JOIN kaneva.communities c ON c.community_id = cg.community_id " +
                    " WHERE f.user_id = @userId " +
                    " AND al.game_id <> " + KanevaGlobals.WokGameId.ToString() +
                    " AND g.game_access_id = 1 " +
                    " GROUP by al.game_id) ";

            parameters.Add("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityUGC().GetPagedDataTableUnion(sqlString, "RandomOrder1", parameters, 1, pageSize, 4);
        }

        private PagedDataTable GetOnlineFriendsWOK (int userId)
        {
            Hashtable parameters = new Hashtable();

            string sqlString = "SELECT DISTINCT(cz.name) as name, " +
                  " wok.concat_vw_url('" + KanevaGlobals.WokGameId.ToString() + "',cz.name,6) as STPURL, " +
                  " 1 as is_friend_playing " +
                  " FROM wok.players p  " +
                      " INNER JOIN wok.player_zones pz ON p.player_id = pz.player_id " +
                      " INNER JOIN wok.supported_worlds sw ON wok.zoneIndex(pz.current_zone_index) = sw.zone_index " +
                      " INNER JOIN wok.channel_zones cz ON pz.current_zone_index = cz.zone_index and pz.current_zone_instance_id = cz.zone_instance_id " +
                      " INNER JOIN kaneva.friends f ON f.friend_id = p.kaneva_user_id " +
                      " WHERE f.user_id = @userId " +
                      " AND p.in_game = 'T' " +
                      " AND cz.zone_type = 6 ";

            parameters.Add("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityUGC().GetPagedDataTableUnion(sqlString, "", parameters, 1, 10, 4);
        }

        private PagedDataTable GetOnlineFriends3DApp (int userId)
        {
            Hashtable parameters = new Hashtable();

            string sqlString = " SELECT IF(length(c.name) = 0, 'unknown', c.name)  AS name, " +
                " wok.concat_vw_url(c.name,'',0) as STPURL, 1 as is_friend_playing, " +
                " al.game_id, " +
                " IF(length(g.game_description) = 0, 'unknown', g.game_description)  AS description " +
                " FROM developer.active_logins al " +
                " INNER JOIN developer.games g ON al.game_id = g.game_id " +
                " INNER JOIN kaneva.communities_game cg ON cg.game_id = g.game_id " +
                " INNER JOIN kaneva.communities c ON c.community_id = cg.community_id " +
                " INNER JOIN wok.players p ON p.kaneva_user_id = al.user_id " +
                " INNER JOIN kaneva.friends f ON f.friend_id = p.kaneva_user_id " +
                " WHERE f.user_id = @userId " +
                " AND al.game_id <> " + KanevaGlobals.WokGameId.ToString() +
                " GROUP by al.game_id ";

            // Test with some dev data
            //string sqlString = " SELECT wok.concat_vw_url(g.game_name,'',0) as STPURL, 1 as is_friend_playing, " +
            //   " IF(length(g.game_name) = 0, 'unknown', g.game_name)  AS name, al.game_id, " +
            //   " IF(length(g.game_description) = 0, 'unknown', g.game_description)  AS description " +
            //   " FROM developer.active_logins al " +
            //   " INNER JOIN developer.games g ON al.game_id = g.game_id " +
            //   " INNER JOIN wok.players p ON p.kaneva_user_id = al.user_id " +
            //   " WHERE al.game_id <> " + KanevaGlobals.WokGameId.ToString() +
            //   " GROUP by al.game_id ";

            parameters.Add("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityUGC().GetPagedDataTableUnion(sqlString, "", parameters, 1, 10, 4);
        }

        /// <summary>
        /// GetTopFriendPicks
        /// </summary>
        private PagedDataTable GetTopFriendPicks (int userId)
        {
            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);

            //string sqlString = " SELECT c.name, wok.concat_vw_url('',c.name,0) as STPURL " +
            //    " FROM kaneva.channel_diggs cd " +
            //    " INNER JOIN kaneva.communities c ON cd.channel_id = c.community_id " +
            //    " INNER JOIN kaneva.friends f ON cd.user_id = f.friend_id " +
            //    " INNER JOIN kaneva.communities_game cg ON cg.community_id = c.community_id " +
            //    " INNER JOIN developer.games g ON cg.game_id = g.game_id " +
            //    " INNER JOIN developer.game_servers gs ON gs.game_id = g.game_id " +
            //    " WHERE f.user_id = @userId " +
            //    " AND cd.created_date > DATE_ADD(NOW(), INTERVAL -30 DAY) " +
            //    " AND c.community_type_id = 3 " +
            //    " AND g.game_status_id = 1  " +
            //    " AND gs.server_status_id = 1   " +
            //    " AND timestampdiff(minute, last_ping_datetime, NOW()) < wok.deadServerInterval() " +  
            //    " AND gs.visibility_id = 1 " +
            //    " AND g.game_access_id = 1 " +
            //    " AND gs.is_external = 1 LIMIT 8";

            //PagedDataTable pdtFriendPick3DApps = KanevaGlobals.GetDatabaseUtilityUGC().GetPagedDataTableUnion(sqlString, "", parameters, 1, 10, 1);

            //sqlString = " SELECT cz.name, wok.concat_vw_url('" + KanevaGlobals.WokGameId.ToString() + "',cz.name,6) as STPURL " +
            //   " FROM kaneva.channel_diggs cd " +
            //   " INNER JOIN wok.channel_zones cz ON cd.channel_id = cz.zone_instance_id " +
            //   " INNER JOIN kaneva.friends f ON cd.user_id = f.friend_id " +
            //   " WHERE f.user_id = @userId  " +
            //   " AND cd.created_date > DATE_ADD(NOW(), INTERVAL -30 DAY) " +
            //   " AND cz.zone_type = 6 LIMIT 10 ";
            //PagedDataTable pdtFriendPickCommunities = KanevaGlobals.GetDatabaseUtilityUGC().GetPagedDataTableUnion(sqlString, "", parameters, 1, 10, 1);

            string sqlString = "(" +
                " SELECT c.name, wok.concat_vw_url(c.name,'',0) as STPURL, 0 as is_friend_playing, g.game_id " +
                 " FROM kaneva.channel_diggs cd " +
                 " INNER JOIN kaneva.communities c ON cd.channel_id = c.community_id " +
                 " INNER JOIN kaneva.friends f ON cd.user_id = f.friend_id " +
                 " INNER JOIN kaneva.communities_game cg ON cg.community_id = c.community_id " +
                 " INNER JOIN developer.games g ON cg.game_id = g.game_id " +
                 " INNER JOIN developer.game_servers gs ON gs.game_id = g.game_id " +
                 " WHERE f.user_id = @userId " +
                 " AND cd.created_date > DATE_ADD(NOW(), INTERVAL -30 DAY) " +
                 " AND c.community_type_id = 3 " +
                 " AND g.game_status_id = 1  " +
                 " AND gs.server_status_id = 1   " +
                 " AND timestampdiff(minute, last_ping_datetime, NOW()) < wok.deadServerInterval() " +
                 " AND gs.visibility_id = 1 " +
                 " AND g.game_access_id = 1 " +
                 " AND gs.is_external = 1 " +
                 " group by g.game_id " +
                " ) " +
              " UNION " +
             " ( " +
             " SELECT cz.name, wok.concat_vw_url('" + KanevaGlobals.WokGameId.ToString() + "',cz.name,6) as STPURL, 0 as is_friend_playing, 0 as game_id " +
                " FROM kaneva.channel_diggs cd " +
                " INNER JOIN wok.channel_zones cz ON cd.channel_id = cz.zone_instance_id " +
                " INNER JOIN kaneva.friends f ON cd.user_id = f.friend_id " +
                " INNER JOIN kaneva.communities c ON cz.zone_instance_id = c.community_id " +
                " WHERE f.user_id = @userId  " +
                " AND cd.created_date > DATE_ADD(NOW(), INTERVAL -30 DAY) " +
                " AND cz.zone_type = 6 " +
                " AND c.is_public = 'Y' " +
                " AND c.community_type_id = 2 " +
             " ) ";

           return KanevaGlobals.GetDatabaseUtilityUGC().GetPagedDataTableUnion(sqlString, "", parameters, 1, 10, 1);
        }

        /// <summary>
        /// get list of top broadband
        /// </summary>
        private PagedDataTable GetTopHangout30Days (bool bHasAPSubscription, bool bShowOnlyAP, string strCountry, bool is18AndUp)
        {
            Hashtable parameters = new Hashtable();
            string sqlString = " SELECT sumTop.zone_name as name, " +
                " wok.concat_vw_url('" + KanevaGlobals.WokGameId.ToString() + "',sumTop.zone_name,6) as STPURL, " +
                " 0 as is_friend_playing ";

            if (strCountry.Length.Equals(0))
            {
                sqlString += " FROM ugc.summary_hangout_raves_latest_month sumTop ";
                //sqlString += " FROM ugc.summary_hangout_raves_all_time sumTop ";
            }
            else
            {
                sqlString += " FROM ugc.summary_hangout_raves_latest_month_filtered sumTop ";
            }
           
            // Age
            if (is18AndUp)
            {
                sqlString += " WHERE owner_age > 17 ";
            }
            else
            {
                sqlString += " WHERE owner_age < 18 ";
            }

            // Filter by country
            if (strCountry.Length > 0)
            {
                sqlString += " AND owner_country = @country ";
                parameters.Add("@country", strCountry);
            }

            if (!bHasAPSubscription)
            {
                sqlString += " AND pass_group_id <> 1 ";
            }
            else
            {
                if (bShowOnlyAP)
                {
                    sqlString += " AND pass_group_id = 1 ";
                }
            }

            // Default order by
            string orderbyclause = "raves desc";

            return KanevaGlobals.GetDatabaseUtilityUGC().GetPagedDataTableUnion(sqlString, orderbyclause, parameters, 1, 10, 4);
        }

        /// <summary>
        /// get list of top broadband
        /// </summary>
        private PagedDataTable GetTop3Dapps30Days(bool bHasAPSubscription, bool bShowOnlyAP, string strCountry, bool is18AndUp)
        {

            Hashtable parameters = new Hashtable();
            string sqlString = " SELECT IF(length(g.game_name) = 0, 'unknown', g.game_name)  AS name, " +
                " wok.concat_vw_url(sumTop.game_name,'',0) as STPURL, 0 as is_friend_playing, " +
                " g.game_id, " +
                " IF(length(g.game_description) = 0, 'unknown', g.game_description)  AS description ";

            if (strCountry.Length.Equals(0))
            {
                sqlString += " FROM ugc.summary_3Dapps_raves_latest_month sumTop ";
            }
            else
            {
                sqlString += " FROM ugc.summary_3Dapps_raves_latest_month_filtered sumTop ";
            }

            sqlString += "      INNER JOIN developer.games g on sumTop.game_id = g.game_id " +
                        "       INNER JOIN developer.game_servers gs on sumTop.game_id = gs.game_id " +
                        "       WHERE g.game_status_id = 1  " + //-- active
                        "       AND gs.server_status_id = 1  " + //-- 1 is running
                        "       AND timestampdiff (minute, last_ping_datetime, NOW()) < wok.deadServerInterval() " + // -- minute limit  
                        "       AND gs.visibility_id = 1 " + //-- server is public  
                        "       AND g.game_access_id = 1 " + //-- game is public
                        "       AND gs.is_external = 1 ";  //--exclude WOK games


            // Age
            if (is18AndUp)
            {
                sqlString += " AND owner_age > 17 ";
            }
            else
            {
                sqlString += " AND owner_age < 18 ";
            }

            // Filter by country
            if (strCountry.Length > 0)
            {
                sqlString += " AND owner_country = @country ";
                parameters.Add("@country", strCountry);
            }
            //if (!bHasAPSubscription)
            //{
            //    sqlString += " AND pass_group_id <> 1 ";
            //}
            //else
            //{
            //    if (bShowOnlyAP)
            //    {
            //        sqlString += " AND pass_group_id = 1 ";
            //    }
            //}

            // Default order by
            string orderbyclause = "raves desc";

            return KanevaGlobals.GetDatabaseUtilityUGC().GetPagedDataTableUnion(sqlString, orderbyclause, parameters, 1, 10, 4);
        }

        #endregion DataTable Queries

        #region Properties

        /// <summary>
        /// Get Countries
        /// </summary>
        private DataTable GetCountries()
        {
            // Build a unique cache key
            string cacheKey = "SummaryCountries";
            DataTable dtCountries = (DataTable)Cache[cacheKey];

            if (dtCountries == null)
            {
                // Add to the cache
                dtCountries = KanevaGlobals.GetDatabaseUtilityReadOnly().GetDataTable("SELECT * FROM kaneva.countries_to_summarize c");
                Cache.Insert(cacheKey, dtCountries, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtCountries;
        }

        /// <summary>
        /// Property to show only AP Items
        /// </summary>
        /// <returns></returns>
        private bool ShowOnlyAP
        {
            get
            {
                if (Request.Params["onlyAP"] != null)
                {
                    return (Request.Params["onlyAP"].Equals("T"));
                }
                else
                {
                    return false;
                }
            }
        }

        private bool HideAP
        {
            get
            {
                if (Request.Params["hideAP"] != null)
                {
                    return (Request.Params["hideAP"].Equals("T"));
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Property to include AP Items for communities
        /// </summary>
        /// <returns>True or False if explicitly set, value of ShowOnlyAP if not set.</returns>
        private bool IncludeAPCommunities
        {
            get
            {
                if (Request.Params["includeAPCommunities"] != null)
                {
                    return (Request.Params["includeAPCommunities"].Equals("T"));
                }
                else
                {
                    return ShowOnlyAP;
                }
            }
        }

        #endregion Properties

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
