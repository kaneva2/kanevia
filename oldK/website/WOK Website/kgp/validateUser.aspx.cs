///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

using WOK.classes;
using System.Collections.Generic;
using System.Text;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for validateUser.
	/// </summary>
	public class validateUser : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
            _tracer = new Queue<string>();
            UserFacade userFacade = new UserFacade();

            int version = 0;
            if (System.Configuration.ConfigurationManager.AppSettings["DisableValidateUserV1"] != "true")
            {
                if (Request["version"] != null)
                    version = Convert.ToInt32(Request["version"]);
            }
            Response.ContentType = "text/xml";

            if (Request["stats"] != null)
            {
                Response.Write("<resp><auth count=\"" + Global.authCount + "\" optcount=\"" + Global.authOptimizeCount + "\"/></resp>");
                return;
            }

			if ( Request["user"] != null && 
				Request["pw"] != null )
			{
                int gameId = KanevaGlobals.WokGameId; // default to WOK
                int tempGameId = 0;
                if ( Request["gid"] != null && Int32.TryParse(Request["gid"], out tempGameId ) )
                    gameId = tempGameId;

                bool allowGame = Request["a"] != null && Request["a"] == "1";

                // Before we potentially kick off the load of the user object, decompile the auth token using the
                // users sessiontoken and wokwebs sessiontoken
                //
                if ( version == 1 
                      && Request["authtoken"] != null 
                        && Request["authtoken"].Length > 0
                        && Request["sessid"] != null 
                        && Request["sessid"].Length > 0 
                        && isAuthentic(Request["authtoken"], Request["sessid"], Global.tokenProvider().getToken().token() ) )
                {
                    Global.authOptimizeCount++;
                    Response.Write("<resp><result>ok</result>"
                                    + "<authtoken>" + Request["authtoken"] + "</authtoken>"     // disable 
                                    //  + "<trace>" + traceString() + "</trace>"                    // disable
                                    + "</resp>");
                    return;
                }

                Global.authCount++;

                // get the e-mail
                UserFacade uf = new UserFacade();
                User u = uf.GetUser(Request["user"]);

                if (u.UserId != 0)
                {
                    int validLogin = UsersUtility.Authorize(Request["user"], Request["pw"], gameId, u.Password, u.Salt, u.StatusId, allowGame);

                    if ( validLogin ==
                        (int)Constants.eLOGIN_RESULTS.SUCCESS)
                    {
                        // Log successfull KIM login in new table
                        if (Request["ims"] != null)
                        {
                            string ipAddress = "";

                            if (Request["IP"] != null)
                            {
                                ipAddress = Request["IP"].ToString();
                            }

                            userFacade.InsertKIMLoginLog(u.UserId, Request["ims"].ToString(), ipAddress);

                            (new MetricsFacade()).RecordNewUserFunnelProgression(u.UserId, "kim_validateuser");
                        }


                        // Check to see if users IP address has been banned
                        try
                        {
                            DataRow drIPBan = UsersUtility.GetIPBan(Common.GetVisitorIPAddress());
                            if (drIPBan != null && drIPBan.Table.Rows.Count > 0)
                            {
                                // Now check to see if account is still banned
                                if (Convert.ToDateTime(drIPBan["ban_end_date"]) < Convert.ToDateTime(drIPBan["curr_date"]))
                                {   // Ban has expired, un-ban ip and continue
                                    UsersUtility.RemoveIPBan(Common.GetVisitorIPAddress());
                                }
                                else
                                {   // Ban still active, show
                                    Response.Write("<resp>error</resp>");
                                    return;
                                }
                            }
                        }
                        catch (Exception) { }

                        // don't do anything if fails since shouldn't ever happen (unless change pw while in game)
                        System.Web.Security.FormsAuthentication.SetAuthCookie(u.UserId.ToString(), false);

                        if (Request["redir"] != null)
                        {
                            Response.Redirect(Request["redir"]);
                        }
                        else
                        {
                            // Good to go.  If a sessid was provided, sign it with the WoKWebSessionToken
                            // Calc an auth token
                            //
                            if (version == 1)
                            {
                                try
                                {
                                    string authtoken = generateAuthToken(Request["sessid"]);
                                    _tracer.Enqueue("AESSign(" + Request["sessid"] + "," + Global.tokenProvider().getToken().token() + ") ==>" + authtoken);
                                    // And write the authtoken into the stream
                                    Response.Write( "<resp>"
                                                    + "<result>ok</result>"
                                                    + "<authtoken>" + authtoken + "</authtoken>"
                                                    //  + "<trace>" + traceString() + "</trace>"
                                                    + "</resp>");
                                }
                                catch (Exception exc)
                                {
                                    // Log this.  However, their credentials passed, let em go.  just no auth token
                                    m_logger.Error("validateUser.PageLoad() - Error generating authtoken [" + exc.ToString() + "]" );
                                    Response.Write("<resp>ok</resp>");
                                }
                            }
                            else {
                                Response.Write("<resp>ok</resp>");
                            }
                        }
                        return;
                    }
                    {

                        string strErrorPrefix = "wok";

                        if (Request["ims"] != null)
                        {
                            strErrorPrefix = "kim";
                        }
                        



                        // Log various failures...
                        switch (validLogin)
                        {
                            case (int)Constants.eLOGIN_RESULTS.USER_NOT_FOUND:
                                {
                                    userFacade.InsertUserLoginIssue(0, Common.GetVisitorIPAddress(), strErrorPrefix + " - Email address " + u.Email + " not found", "INVALID_EMAIL", strErrorPrefix.ToUpper());
                                    break;
                                }
                            case (int)Constants.eLOGIN_RESULTS.INVALID_PASSWORD:
                                {
                                    userFacade.InsertUserLoginIssue(0, Common.GetVisitorIPAddress(), strErrorPrefix + " - Invalid password for username " + u.Username, "INVALID_PASSWORD", strErrorPrefix.ToUpper());
                                    break;
                                }
                            case (int)Constants.eLOGIN_RESULTS.ACCOUNT_LOCKED:
                                {
                                    userFacade.InsertUserLoginIssue(0, Common.GetVisitorIPAddress(), strErrorPrefix + " - Locked account username '" + u.Email + "' tried to sign in", "BANNED", strErrorPrefix.ToUpper());
                                    break;
                                }
                        }

                    }

                }
            }
            if (version == 1)
            {
                Response.Write( "<resp><result>error<result>" 
                                //  + "<trace>" + traceString() + "</trace>"
                                +"</resp>");
            }
            else
            {
                Response.Write("<resp>error</resp>");
            }

            System.Web.Security.FormsAuthentication.SignOut();
		}

        private string generateAuthToken(string sessid )
        {
            if (sessid == null)
                throw new ArgumentNullException();
            return Common.AESSign(sessid, Global.tokenProvider().getToken().token());
        }

        private bool isAuthentic(string authtoken,string sessid, string signature )
        {
            if (sessid.Length == 0)
                throw new ArgumentException();

            if (authtoken.Length == 0)
                throw new ArgumentException();

            if (signature == null)
                throw new ArgumentNullException();

            return sessid == Common.AESUnsign( authtoken, signature );
        }


        private string traceString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<table>")
              .Append("<table>")
              .Append("<tr><td>Trace Data</td></tr>");

            foreach (string data in _tracer)
            {
                sb.Append("<tr><td>")
                  .Append(data)
                  .Append("</tr></td>");
            }

            sb.Append("</table></p>");
            return sb.ToString();
        }


        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Queue<String> _tracer;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
    }
}
