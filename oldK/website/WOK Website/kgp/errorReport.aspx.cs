///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class errorReport : KgpBasePage
    {
        #region Declarations

        private const int DISCONNECT_ERROR = 1;
        private const int CRASH_ERROR = 2;
        private string resultStr = "";

        private int _errorType = 0;
        private string _errorDescription = "";
        private DateTime _errorTime;
        private int _userId = 0;
        private int _errorCode = 0;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int result = 0;
                string message = "success";
                if (GetRequestParams())
                {
                    ErrorLoggingFacade errorFacade = new ErrorLoggingFacade();
                    result = errorFacade.InsertDisconnectError(_errorType, _errorDescription, _errorTime, _userId, _errorCode);
                }
                if (result == 0)
                {
                    message = "Error: unable to write your log to the Database";
                }
                Response.Write("<Result>\r\n  <ReturnCode>" + result + "</ReturnCode>\r\n  <ResultDescription>" + message + "</ResultDescription>\r\n</Result>");
            }
        }
        
        /// <summary>
		/// parse request params, returns false if any params are invliad
		/// </summary>
		/// <returns></returns>
		private bool GetRequestParams()
		{
			bool retVal = false;
			try
			{
				_errorType = Convert.ToInt32(Request.Params["errorType"]);
                _errorDescription = Request.Params["description"].ToString();
                _errorTime = Convert.ToDateTime(Request.Params["time"]).ToLocalTime(); //assumes time sent to page is UTC(GMT)
                _userId = Convert.ToInt32(Request.Params["link"]);
                _errorCode = Convert.ToInt32(Request.Params["errorCode"]);
                retVal = true;
			}
            catch(FormatException fe)
            {
                 resultStr = "<Result>\r\n  <ReturnCode>-2</ReturnCode>\r\n  <ResultDescription>" + fe.Message + "</ResultDescription>\r\n</Result>";
           }
            catch(Exception ex)
			{
                resultStr = "<Result>\r\n  <ReturnCode>-3</ReturnCode>\r\n  <ResultDescription>" + ex.Message + "</ResultDescription>\r\n</Result>";
			}

			return retVal;
		}
    }
}
