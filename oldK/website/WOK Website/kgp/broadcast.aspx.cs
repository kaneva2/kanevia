///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for Broadcast.
    /// </summary>
    public class Broadcast : KgpBasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(true))
            {
                if (UsersUtility.IsUserCSR())
                {
                    int serverId = 0;
                    int userId = 0;

                    if ((Request.Params["msg"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Must supply msg</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    if (Request.Params["sid"] != null)
                    {
                        if (!Int32.TryParse(Request.Params["sid"].ToString(), out serverId))
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Bad server id</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    }
                    else if (Request.Params["uid"] != null)
                    {
                        if (!Int32.TryParse(Request.Params["uid"].ToString(), out userId))
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Bad user id</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    }

                    string msg = Request.Params["msg"].ToString();

                    RemoteEventSender rs = new RemoteEventSender();
                    if (serverId != 0)
                        rs.BroadcastMessageToServer(msg, serverId);
                    else if (userId != 0)
                        rs.BroadcastMessageToPlayer(msg, userId);
                    else
                        rs.BroadcastMessageToAll(msg);

                    string resp = "<Result><ReturnCode>0</ReturnCode><ResultDescription>Ok</ResultDescription></Result>";
                    Response.Write(resp);
                }
                else
                {
                    string resp = "<Result><ReturnCode>-1</ReturnCode><ResultDescription>Insufficient rights</ResultDescription></Result>";
                    Response.Write(resp);
                }
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
