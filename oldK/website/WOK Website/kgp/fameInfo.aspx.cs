///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for Fame.
    /// </summary>
    public class FameInfo : KgpBasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            string kanevaDb = KanevaGlobals.DbNameKaneva;

            if (IsPostBack)
            {
                return;
            }

            if (CheckUserId(true)) // true = can be logged on to web also for this request
            {
                if (Request.Params["action"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                string actionreq = Request.Params["action"];
                FameFacade fameFacade = new FameFacade();

                if (actionreq.Equals("GetUserFame"))
                {
                    if ((Request.Params["userId"] == null) || (Request.Params["fameTypeId"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId or fameTypeId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int userId = Int32.Parse(Request.Params["userId"].ToString());
                    int fameTypeId = Int32.Parse(Request.Params["fameTypeId"].ToString());

                    UserFame userFame = fameFacade.GetUserFame(userId, fameTypeId);

                    if (userFame != null)
                    {
                        UserFameResponse response = new UserFameResponse();
                        response.userFame = userFame;
                        Response.Write(response);

                        XmlSerializer s = new XmlSerializer(typeof(UserFameResponse));
                        Response.Clear();
                        s.Serialize(Response.OutputStream, response);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>User Fame not found</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
                else if (actionreq.Equals("GetPacketForUser"))
                {
                    if ((Request.Params["userId"] == null) || (Request.Params["packetId"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId or packetId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int userId = Int32.Parse(Request.Params["userId"].ToString());
                    int packetId = Int32.Parse(Request.Params["packetId"].ToString());

                   
                    XmlDocument doc = new XmlDocument();
                    XmlElement rootNode = doc.CreateElement("Result");
                    doc.AppendChild(rootNode);

                    Packet packet = fameFacade.GetPacket (packetId);

                    // Check for packet not found
                    if (packet.PacketId.Equals (0))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Packet not found</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    // Add the packet node
                    rootNode.AppendChild(GetPacketNode(doc, userId, packet));

                    XmlNode root = doc.DocumentElement;

                    XmlElement elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = "success";

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = "0";

                    root.InsertBefore(elem, root.FirstChild);

                    Response.Write(root.OuterXml);


                }
                else if (actionreq.Equals("GetPacketsForUser"))
                {
                    if ((Request.Params["userId"] == null) || (Request.Params["fameTypeId"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId or fameTypeId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    int userId = Int32.Parse(Request.Params["userId"].ToString());
                    int fameTypeId = Int32.Parse(Request.Params["fameTypeId"].ToString());

                    PagedList<Packet> plPacket = fameFacade.GetPackets(fameTypeId, "", "packet_id ASC", "", page, items_per_page);

                    // Build the XML
                    XmlDocument doc = new XmlDocument();
                    XmlElement rootNode = doc.CreateElement("Result");
                    doc.AppendChild(rootNode);

                    XmlElement userNode = doc.CreateElement("UserPacketHistory");
                    rootNode.AppendChild(userNode);

                    XmlElement packetsNode = doc.CreateElement("Packets");
                    userNode.AppendChild(packetsNode);

                    foreach (Packet packet in plPacket)
                    {
                        if (!packet.PacketId.Equals((int)PacketId.BUY_VIP_PASS) && !packet.PacketId.Equals((int)PacketId.BUY_CREDITS))
                        {
                            // Add the packet node
                            packetsNode.AppendChild(GetPacketNode(doc, userId, packet));
                        }
                    }

                    XmlNode root = doc.DocumentElement;

                    XmlElement elem = doc.CreateElement("NumberRecords");
                    elem.InnerText = plPacket.Count.ToString ();

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("TotalNumberRecords");
                    elem.InnerText = plPacket.TotalCount.ToString ();

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = "success";

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = "0";

                    root.InsertBefore(elem, root.FirstChild);

                    Response.Write(root.OuterXml);

                }

                else if (actionreq.Equals("GetLevel"))
                {
                    if ((Request.Params["levelId"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>levelId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int levelId = Int32.Parse(Request.Params["levelId"].ToString());
                    Level level = fameFacade.GetLevel(levelId);


                    if (level != null)
                    {
                        LevelResponse response = new LevelResponse();
                        response.level = level;
                        Response.Write(response);

                        XmlSerializer s = new XmlSerializer(typeof(LevelResponse));
                        Response.Clear();
                        s.Serialize(Response.OutputStream, response);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>Level not found</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
                else if (actionreq.Equals("GetPacketHistory"))
                {
                    if ((Request.Params["userId"] == null) || (Request.Params["fameTypeId"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId or fameTypeId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int userId = Int32.Parse(Request.Params["userId"].ToString());
                    int fameTypeId = Int32.Parse(Request.Params["fameTypeId"].ToString());

                    PagedList<PacketHistory> packetHistory = fameFacade.GetPacketHistory(userId, fameTypeId, "", "packet_history_id DESC", 1, 3);

                    if (packetHistory != null)
                    {
                        PacketHistoryResponse response = new PacketHistoryResponse();
                        response.packetHistory = packetHistory;
                        Response.Write(response);

                        XmlSerializer s = new XmlSerializer(typeof(PacketHistoryResponse));
                        Response.Clear();
                        s.Serialize(Response.OutputStream, response);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>No Packet History</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
                else if (actionreq.Equals("GetLevelAwards"))
                {
                    if ((Request.Params["levelId"] == null) && (Request.Params["gender"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>levelId or gender not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int levelId = Int32.Parse(Request.Params["levelId"].ToString());
                    string gender = Request.Params["gender"].ToString();

                    List<LevelAwards> levelAwards = fameFacade.GetLevelAwards(levelId, gender);

                    if (levelAwards != null)
                    {
                        LevelAwardsResponse response = new LevelAwardsResponse();
                        response.levelawards = levelAwards;
                        Response.Write(response);

                        XmlSerializer s = new XmlSerializer(typeof(LevelAwardsResponse));
                        Response.Clear();
                        s.Serialize(Response.OutputStream, response);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>No Level Awards found</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
                else if (actionreq.Equals("GetAllLevelAwards"))
                {
                    if (Request.Params["fameTypeId"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>fameTypeId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                    
                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    int fameTypeId = Int32.Parse(Request.Params["fameTypeId"].ToString());

                    List<LevelAwards> plLevelAwards = fameFacade.GetAllLevelAwards (fameTypeId);

                    if (plLevelAwards != null)
                    {
                        LevelAwardsResponse response = new LevelAwardsResponse();
                        response.levelawards = plLevelAwards;
                        //Response.Write(response);

                        XmlSerializer s = new XmlSerializer(typeof(LevelAwardsResponse));
                        Response.Clear();
                        s.Serialize(Response.OutputStream, response);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>No Levels</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
                else if (actionreq.Equals("GetAllLevels"))
                {
                    if (Request.Params["fameTypeId"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>fameTypeId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    int fameTypeId = Int32.Parse(Request.Params["fameTypeId"].ToString());

                    PagedList<Level> plLevel = fameFacade.GetLevels(fameTypeId, "", "level_number ASC", page, items_per_page);

                    if (plLevel != null)
                    {
                        LevelsResponse response = new LevelsResponse();
                        response.level = plLevel;
                        //Response.Write(response);

                        XmlSerializer s = new XmlSerializer(typeof(LevelsResponse));
                        Response.Clear();
                        s.Serialize(Response.OutputStream, response);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>No Levels</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
                else if (actionreq.Equals("GetAllPackets"))
                {
                    if (Request.Params["fameTypeId"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>fameTypeId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                     if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    int fameTypeId = Int32.Parse(Request.Params["fameTypeId"].ToString());

                    PagedList<Packet> plPacket = fameFacade.GetPackets (fameTypeId, "", "packet_id ASC", "", page, items_per_page);

                    if (plPacket != null)
                    {
                        PacketResponse response = new PacketResponse();
                        response.packet = plPacket;
                        //Response.Write(response);

                        XmlSerializer s = new XmlSerializer(typeof(PacketResponse));
                        Response.Clear();
                        s.Serialize(Response.OutputStream, response);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>No Packets</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
                else if (actionreq.Equals("GetChecklist"))
                {
                    if ((Request.Params["fameTypeId"] == null) || (Request.Params["checklistId"] == null) || (Request.Params["userId"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId, fameTypeId or checklistId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    int fameTypeId = Int32.Parse(Request.Params["fameTypeId"].ToString());
                    int checklistId = Int32.Parse(Request.Params["checklistId"].ToString());
                    int userId = Int32.Parse(Request.Params["userId"].ToString());

                    PagedList<Nudge> plNudge = fameFacade.GetChecklist(userId, checklistId, "", "sort_order ASC", page, items_per_page);
       
                    if (plNudge != null)
                    {
                        NudgeResponse response = new NudgeResponse();
                        response.nudge = plNudge;
                        //Response.Write(response);

                        XmlSerializer s = new XmlSerializer(typeof(NudgeResponse));
                        Response.Clear();
                        s.Serialize(Response.OutputStream, response);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>No Nudges found</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }


                }
                else if (actionreq.Equals("GetNudges"))
                {
                    if ((Request.Params["fameTypeId"] == null) || (Request.Params["userId"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId, fameTypeId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>start or max not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int page = Int32.Parse(Request.Params["start"].ToString());
                    int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                    int fameTypeId = Int32.Parse(Request.Params["fameTypeId"].ToString());
                    int userId = Int32.Parse(Request.Params["userId"].ToString());

                    PagedList<Nudge> plNudge = fameFacade.GetNudges(userId, "", "", page, items_per_page);


                    if (plNudge != null)
                    {
                        NudgeResponse response = new NudgeResponse();
                        response.nudge = plNudge;
                        //Response.Write(response);

                        XmlSerializer s = new XmlSerializer(typeof(NudgeResponse));
                        Response.Clear();
                        s.Serialize(Response.OutputStream, response);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>No Nudges found</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                }
                else if (actionreq.Equals("GetContextualHelp"))
                {
                    if ((Request.Params["helpListId"] == null) || (Request.Params["userId"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId, helpListId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int userId = Int32.Parse(Request.Params["userId"].ToString());
                    int checkListId = Int32.Parse(Request.Params["helpListId"].ToString());

                    PagedList<Nudge> plNudge = fameFacade.GetNudgesForHelp(userId, checkListId);


                    if (plNudge != null)
                    {
                        NudgeResponse response = new NudgeResponse();
                        response.nudge = plNudge;
                        //Response.Write(response);

                        XmlSerializer s = new XmlSerializer(typeof(NudgeResponse));
                        Response.Clear();
                        s.Serialize(Response.OutputStream, response);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>No Nudges found</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                }
                else if (actionreq.Equals("GetNotficationProfileForUser"))
                {
                    if (Request.Params["userId"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int userId = Int32.Parse(Request.Params["userId"].ToString());
                    
                    UserFacade userFacade = new UserFacade();
                    List<NotificationsProfile> plNP = userFacade.GetNotificationsProfile (userId);

                    if (plNP != null)
                    {
                        ProfileNotificationsResponse response = new ProfileNotificationsResponse();
                        response.np = plNP;
                        //Response.Write(response);

                        XmlSerializer s = new XmlSerializer(typeof(ProfileNotificationsResponse));
                        Response.Clear();
                        s.Serialize(Response.OutputStream, response);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>2</ReturnCode>\r\n  <ResultDescription>No NotficationProfile found</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }

                else if (actionreq.Equals("AddNotficationProfile"))
                {
                    if (Request.Params["notificationTypeId"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>notificationTypeId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    if (Request.Params["xml"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>xml not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int notificationTypeId = Int32.Parse(Request.Params["notificationTypeId"].ToString());
                    string xml = Request.Params["xml"].ToString();

                    UserFacade userFacade = new UserFacade();
                    NotificationsProfile np = new NotificationsProfile(0, GetUserId(), notificationTypeId, xml, "");

                    int result = userFacade.InsertUserNotificationProfile(np);

                    if (result.Equals (1))
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Success</ResultDescription>\r\n</Result>");
                        return;
                    }
                    else
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Failure adding profile notifcation, see wok web logs</ResultDescription>\r\n</Result>");
                        return;
                    }


                }

                else if (actionreq.Equals("DismissNotficationProfile"))
                {
                    if (Request.Params["notificationId"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>notificationId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    uint notificationId = uint.Parse(Request.Params["notificationId"].ToString());

                    UserFacade userFacade = new UserFacade();
                    userFacade.DismissNotficationProfile(GetUserId(), notificationId);


                    Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Success</ResultDescription>\r\n</Result>");
                    return;


                }
                else if (actionreq.Equals("setFameLevel"))
                {
                    // Confirm user permissions.
                    if (!GetUserFacade.IsUserAutomatedTestAccount(KanevaWebGlobals.CurrentUser))
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Unauthorized user.</ResultDescription>\r\n</Result>");
                        return;
                    }

                    // Confirm that this call is active on this environment.
                    if (!Configuration.AutomatedTestUserGenerationEnabled)
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Unauthorized action.</ResultDescription>\r\n</Result>");
                        return;
                    }

                    if (string.IsNullOrWhiteSpace(Request.Params["fameTypeId"]))
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>fameTypeId not specified</ResultDescription>\r\n</Result>");
                        return;
                    }

                    if (string.IsNullOrWhiteSpace(Request.Params["level"]))
                    {
                        Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>level not specified</ResultDescription>\r\n</Result>");
                        return;
                    }

                    int fameTypeId = int.Parse(Request.Params["fameTypeId"]);
                    int level = int.Parse(Request.Params["level"]);

                    // Set the player's new level.
                    int levelId = GetFameFacade.SetLevel(GetUserId(), fameTypeId, level);

                    if (levelId > 0)
                    {
                        int levelNotificationId = GetFameFacade.SendLevelUpNotification(KanevaWebGlobals.CurrentUser, fameTypeId, levelId);
                        GetFameFacade.SendFameEarnedEventToPlayer(KanevaWebGlobals.CurrentUser, fameTypeId, Packet.GetDummyPacket(fameTypeId), true, levelId, 0, levelNotificationId, 0, 0);
                        Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Success</ResultDescription>\r\n</Result>");
                        return;
                    }

                    Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Failed to set level.</ResultDescription>\r\n</Result>");
                }
            }
        }

       private XmlNode GetPacketNode (XmlDocument doc, int userId, Packet packet)
        {
            FameFacade fameFacade = new FameFacade();

            // Add packet info
            XmlElement packetNode = doc.CreateElement("Packet");
            packetNode.SetAttribute("PacketId", packet.PacketId.ToString());
            packetNode.SetAttribute("Name", packet.Name);
            packetNode.SetAttribute("Status", Convert.ToInt32(packet.Status).ToString());
            int available = fameFacade.IsPacketAvailable(packet, userId);
            packetNode.SetAttribute("Available", available.ToString());
            
            XmlElement ripNode = doc.CreateElement("RewardIntervalProgress");
            ripNode.SetAttribute("RewardInterval", packet.RewardInterval.ToString());
            
            int progress = fameFacade.GetRewardProgress(packet, userId);
            ripNode.SetAttribute("Progress", progress.ToString());

            int numberOfDailyRedeptions = fameFacade.GetRewardIntervalDaily(userId, packet.PacketId);
            ripNode.SetAttribute("RemainingPerDay", (packet.MaxPerDay - numberOfDailyRedeptions).ToString());
            packetNode.AppendChild(ripNode);

            return packetNode;
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
