///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.framework.utils;
using log4net;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Text.RegularExpressions;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>    
    /// Summary description for friendsList.     
    /// </summary>
    public class playList : KgpBasePage
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string m_playListURL = "changePlaylist?url=http://" + KanevaWebServerName + "/services/omm/request.aspx" +
                                    HttpUtility.UrlEncode("?type=5&plId=ASSET_GROUP_ID&game=1");

        #endregion

        /// <summary>
        /// Event handler for Page load event. Main method of execution for this WoKWeb call.
        /// </summary>
        private void Page_Load(object sender, System.EventArgs e)
        {
            if ( (Request.Params["type"] != null && Request.Params["type"] == "getPlayList") ||
                 (CheckUserId(false)) )
            {
                // Pull request data and set defaults
                int assetId         = Int32.Parse(Request.Params["aId"] ?? "-1");
                int assetTypeId     = Int32.Parse(Request.Params["assetTypeId"] ?? ((int)Constants.eASSET_TYPE.ALL).ToString());
                int items_per_page  = Int32.Parse(Request.Params["max"] ?? "-1");
                int page            = Int32.Parse(Request.Params["start"] ?? "-1");
                int playerId        = Int32.Parse(Request.Params["playerId"] ?? "-1");
                int playListId      = Int32.Parse(Request.Params["pid"] ?? "-1");
                int zoneIndex       = Int32.Parse(Request.Params["zoneIndex"] ?? "-1");
                int zoneInstanceId  = Int32.Parse(Request.Params["zoneInstanceId"] ?? "-1");
                int gcId            = Int32.Parse(Request.Params["gcId"] ?? "-1");
                int tvPlayListId    = Int32.Parse(Request.Params["plId"] ?? "-1");
                string playlistName = (Request.Params["p"] ?? string.Empty);
                string action       = (Request.Params["type"] ?? "u");
                string searchStr    = (Request.Params["ss"] ?? string.Empty);
                string tvStr        = (Request.Params["tv"] ?? string.Empty); 
                
                // Make sure that we have page info if needed for this action
                if ((page < 1 || items_per_page < 1) && !Regex.IsMatch(action, "t|u|getPlayList"))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>page number and items per page not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                // Make sure we're not double-encoding
                playlistName = Server.HtmlDecode(playlistName);
                playlistName = Server.HtmlEncode(playlistName);

                switch (action)
                {
                    case("getPlayList"): // Zone playlist
                        getPlayList(zoneIndex, zoneInstanceId, playListId, playerId);
                        break;
                    case("k"): // kaneva playlists
                        getKanevaChannels(page, items_per_page);
                        break;
                    case("tpl"): // user textures
                        getAssetsInPlaylist(page, items_per_page, playListId, (Constants.eASSET_TYPE)assetTypeId);
                        break;
                    case("c"): // playlists by category
                        getCategoryPlaylists(page, items_per_page, gcId);
                        break;
                    case("m"): // search media
                        getSearchItems(page, items_per_page, searchStr, false, new Constants.eASSET_TYPE[] { Constants.eASSET_TYPE.VIDEO });
                        break;
                    case("y"): // search my media
                        getSearchItems(page, items_per_page, searchStr, true, new Constants.eASSET_TYPE[] { Constants.eASSET_TYPE.VIDEO });
                        break;
                    case("p"): // add to playlist
                        addItemToInWorldPlaylist(page, items_per_page, assetId, playlistName);
                        break;
                    case("s"): // swfs
                        m_playListURL = "changePlaylist?assetId=ASSET_ID&game=1";
                        getSearchItems(page, items_per_page, searchStr, true, new Constants.eASSET_TYPE[] { Constants.eASSET_TYPE.WIDGET });
                        break;
                    case("g"): // games
                        m_playListURL = "changePlaylist?assetId=ASSET_ID&game=1";
                        getSearchItems(page, items_per_page, searchStr, true, new Constants.eASSET_TYPE[] { Constants.eASSET_TYPE.GAME });
                        break;
                    case("a"): // asx
                        m_playListURL = "changePlaylist?assetId=ASSET_ID&game=1";
                        getSearchItems(page, items_per_page, searchStr, true, new Constants.eASSET_TYPE[] { Constants.eASSET_TYPE.TV });
                        break;
                    case("t"): // send first tv in plalist down
                        m_playListURL = "changePlaylist?assetId=ASSET_ID&game=1";
                        getFirstAssetFromPlaylist(tvPlayListId, Constants.eASSET_TYPE.TV);
                        break;
                    case("d"):  // in-client medium drop
                        addDropBoxMedia(page, items_per_page, playlistName);
                        break;
                    default:
                        getUserChannels();
                        break;
                }
            }
        }

        #region Helper Methods

        private void HtmlDecodePlaylistNames(ref PagedDataTable pdtAssetGroups)
        {
            foreach (DataRow row in pdtAssetGroups.Rows)
            {
                row["name"] = Server.HtmlDecode(row["name"].ToString());
            }
        }

        #endregion

        #region Action Methods

        private void addItemToInWorldPlaylist(int page, int items_per_page, int assetId, string playlistName)
        {
            MediaFacade mediaFacade = new MediaFacade();
            int channelId = KanevaWebGlobals.CurrentUser.CommunityId;

            PagedDataTable pdtAssetGroups = StoreUtility.GetAssetGroups(channelId, "", 1, Int32.MaxValue, playlistName);

            string interaction = "modifiedPlaylistInWoK";
            if (pdtAssetGroups.Rows.Count == 0)
            {
                mediaFacade.InsertAssetGroup(channelId, playlistName, "", 0);
                pdtAssetGroups = StoreUtility.GetAssetGroups(channelId, "", 1, Int32.MaxValue, playlistName);
                interaction = "createdPlaylistInWoK";
            }

            if (pdtAssetGroups.Rows.Count > 0)
            {
                StoreUtility.InsertAssetChannel(assetId, channelId);
                int assetGroupId = Int32.Parse(pdtAssetGroups.Rows[0]["asset_group_id"].ToString());
                mediaFacade.InsertAssetInGroup(channelId, assetGroupId, assetId);
                mediaFacade.MoveAssetToBottomOfGroup(assetGroupId, assetId);
                HtmlDecodePlaylistNames(ref pdtAssetGroups);
                returnResult(pdtAssetGroups);

                // Record metrics 
                if (!string.IsNullOrWhiteSpace(Request.Params["fromObjPlacementId"]) &&
                    !string.IsNullOrWhiteSpace(Request.Params["fromZoneInstanceId"]) &&
                    !string.IsNullOrWhiteSpace(Request.Params["fromZoneType"]))
                {
                    Dictionary<string, string> metricData = new Dictionary<string, string>
                    {
                        {"interaction", interaction },
                        {"username", KanevaWebGlobals.CurrentUser.Username},
                        {"objPlacementId", Request.Params["fromObjPlacementId"]},
                        {"frameworkEnabled", (!string.IsNullOrWhiteSpace(Request.Params["fromGameplayMode"])).ToString()},
                        {"playerModeActive", (Request.Params["fromGameplayMode"].Equals("player", StringComparison.InvariantCultureIgnoreCase)).ToString()},
                        {"zoneInstanceId", Request.Params["fromZoneInstanceId"]},
                        {"zoneType", Request.Params["fromZoneType"]},
                    };
                    (new MetricsFacade()).InsertGenericMetricsLog("media_player_interaction_log", metricData, Global.RequestRabbitMQChannel());
                }
            }
        }

        private void getSearchItems(int page, int items_per_page, string searchString, bool myItemsOnly, Constants.eASSET_TYPE[] assetTypes)
        {
            MediaFacade mediaFacade = new MediaFacade();
            List<int> iAssetList = null;

            if (myItemsOnly)
            {
                int channelId = UsersUtility.GetCommunityIdFromUserId(m_userId);
                iAssetList = new List<int>();

                foreach (int assetTypeId in assetTypes)
                {
                    DataTable dt = StoreUtility.GetAssetsInChannel(channelId, true, true, assetTypeId, "", "", 1, Int32.MaxValue);

                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            iAssetList.Add((int)Convert.ToInt32(dt.Rows[i]["asset_id"]));
                        }
                    }
                }
            }

            PagedDataTable pdtResult = new PagedDataTable();
            pdtResult.Columns.Add(new DataColumn("asset_id"));
            pdtResult.Columns.Add(new DataColumn("name"));
            pdtResult.Columns.Add(new DataColumn("asset_type_id"));
            pdtResult.Columns.Add(new DataColumn("mature"));
            pdtResult.Columns.Add(new DataColumn("asset_offsite_id"));

            foreach (int assetTypeId in assetTypes)
            {
                PagedList<Asset> assetList = mediaFacade.SearchMedia(KanevaWebGlobals.CurrentUser.HasAccessPass, assetTypeId,
                    myItemsOnly, false, Server.HtmlEncode(searchString),
                    "", 0, "", page, items_per_page, false, iAssetList);

                pdtResult.TotalCount += (int)assetList.TotalCount;

                DataRow drAsset;
                foreach (Asset asset in assetList)
                {
                    drAsset = pdtResult.NewRow();
                    drAsset["asset_id"] = asset.AssetId;
                    drAsset["name"] = asset.Name;
                    drAsset["asset_type_id"] = asset.AssetTypeId;
                    drAsset["mature"] = (asset.Mature ? "Y" : "N");
                    drAsset["asset_offsite_id"] = asset.AssetOffsiteId;
                    pdtResult.Rows.Add(drAsset);
                }
            }
                   
            returnResult(pdtResult);
        }

        private void getCategoryPlaylists(int page, int items_per_page, int groupCategoryId)
        {
            MediaFacade mediaFacade = new MediaFacade();

            PagedDataTable pdt = new PagedDataTable();
            pdt.Columns.Add(new DataColumn("group_category_id"));
            pdt.Columns.Add(new DataColumn("asset_group_id"));
            pdt.Columns.Add(new DataColumn("asset_type_id"));
            pdt.Columns.Add(new DataColumn("sort_order"));
            pdt.Columns.Add(new DataColumn("name"));

            PagedList<AssetGroup> groupList = mediaFacade.GetGroupCategoryAssetGroups(groupCategoryId, page, items_per_page);

            pdt.TotalCount = (int)groupList.TotalCount;

            DataRow drAsset;
            foreach (AssetGroup group in groupList)
            {
                drAsset = pdt.NewRow();
                drAsset["group_category_id"] = group.GroupCategoryId;
                drAsset["asset_group_id"] = group.AssetGroupId;
                drAsset["sort_order"] = group.GroupCategorySortOrder;
                drAsset["name"] = Server.HtmlDecode(group.Name);
                pdt.Rows.Add(drAsset);
            }

            returnResult(pdt);
        }

        private void getPlayList(int zoneIndex, int zoneInstanceId, int playListId, int playerId)
        {
            UserFacade userFacade = new UserFacade();

            DataSet ds = userFacade.GetPlayListData(zoneIndex, zoneInstanceId, playListId, playerId);

            returnResult(ds);
        }

        private void getKanevaChannels(int page, int items_per_page)
        {
            PagedDataTable pdt;

            string selectList = "group_category_id, name";
            string tableList = "group_categories";
            string whereClause = "";
            string orderbyclause = "group_category_id";

            pdt = KanevaGlobals.GetDatabaseUtility().GetPagedDataTable(selectList, tableList, whereClause, orderbyclause, new Hashtable(), page, items_per_page);

            returnResult(pdt);
        }

        private void getUserChannels()
        {
            PagedDataTable pdt;
            pdt = StoreUtility.GetAssetGroups(KanevaWebGlobals.CurrentUser.CommunityId, "name", 1, 9999, "");
            HtmlDecodePlaylistNames(ref pdt);
            returnResult(pdt);
        }

        private void getAssetsInPlaylist(int page, int items_per_page, int playListId, Constants.eASSET_TYPE assetTypeId)
        {
            PagedDataTable pdt;
            pdt = StoreUtility.GetAssetsInGroup(playListId, KanevaWebGlobals.CurrentUser.CommunityId, (int)assetTypeId, "aga.sort_order asc, ach.created_date desc", "", page, items_per_page);
            returnResult(pdt);
        }

        private void getFirstAssetFromPlaylist(int playlistId, Constants.eASSET_TYPE assetTypeId)
        {
            PagedDataTable pdt;
            pdt = StoreUtility.GetAssetsInGroup(playlistId, KanevaWebGlobals.CurrentUser.CommunityId, (int)assetTypeId, "asset_id asc", "", 1, 1);
            pdt.TotalCount = 1;

            returnResult(pdt);
        }

        private void returnResult(PagedDataTable pdt)
        {
            DataSet ds = new DataSet();

            ds.DataSetName = "Result";

            pdt.TableName = "PlayList";
            ds.Tables.Add(pdt);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("PlayListURL");
            elem.InnerText = m_playListURL.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int numRecords = pdt.Rows.Count;
            elem = doc.CreateElement("NumberRecords");
            elem.InnerText = numRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            int totalNumRecords = pdt.TotalCount;
            elem = doc.CreateElement("TotalNumberRecords");
            elem.InnerText = totalNumRecords.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Hashtable addlXMLElements = (Hashtable)ViewState["addlXMLElements"];
            if( addlXMLElements!=null )
            {
                foreach( string key in addlXMLElements.Keys)
                {
                    string value = (string)addlXMLElements[key];
                    elem = doc.CreateElement(key);
                    elem.InnerText = value;
                    root.InsertAfter(elem, root.FirstChild);
                }
            }

            Response.Write(root.OuterXml);
        }

        private void returnResult(DataSet ds)
        {
            ds.DataSetName = "Result";

            // setup the result in the header
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        private void addDropBoxMedia(int page, int items_per_page, string playlistName )
        {
            int assetType = Int32.Parse(Request.Params["at"]);
            int assetSubType = Int32.Parse(Request.Params["as"]);
            string extAssetID = Convert.ToString(Request.Params["xa"]);
            string title = Convert.ToString(Request.Params["t"]);
            int isPrivate = Int32.Parse(Request.Params["pv"]);
            int assetRatingId = Int32.Parse(Request.Params["r"]);
            string teaser = Convert.ToString(Request.Params["ts"]);
            int categoryId = Int32.Parse(Request.Params["c"]);

            // some parameters to be passed back to client
            string targetType = Convert.ToString(Request.Params["tt"]);
            string targetId = Convert.ToString(Request.Params["ti"]);

            // Step1: create offset asset
            int userId = GetUserId();
            //int assetId = StoreUtility.InsertOffSiteAsset(extAssetID, assetType, assetSubType, title,
            //    userId, UsersUtility.GetUserName(userId),
            //    (int)Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE,
            //    isPrivate == 0 ? (int)Constants.eASSET_PERMISSION.PUBLIC : (int)Constants.eASSET_PERMISSION.PRIVATE,
            //    assetRatingId, teaser, categoryId);

            int assetId = StoreUtility.InsertAsset(assetType, assetSubType, title, userId, UsersUtility.GetUserName(userId),
                (int)Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE,
                isPrivate == 0 ? (int)Constants.eASSET_PERMISSION.PUBLIC : (int)Constants.eASSET_PERMISSION.PRIVATE,
                assetRatingId, categoryId, teaser, "", "", extAssetID);

            if (assetId != 0)
            {
                // Step2: scrape the thumbnails and other video data
                if (ExternalURLHelper.GetYouTubeInfo(extAssetID, userId, assetId, m_logger) > 0)
                {
                    m_logger.Error("An error occurred during video metadata grab for Youtube ID: " + extAssetID);
                }

                // Step3: add asset to playlist and return playback XML
                if (!string.IsNullOrWhiteSpace(playlistName))
                {
                    Hashtable addlXMLElements = new Hashtable();
                    addlXMLElements.Add("targetType", targetType);
                    addlXMLElements.Add("targetId", targetId);
                    //getUserExternalMediaByAssetId(assetId, addlXMLElements);
                    ViewState["addlXMLElements"] = addlXMLElements;
                    addItemToInWorldPlaylist(page, items_per_page, assetId, playlistName);
                }
                else
                {
                    m_logger.Error("Missing playlist while processing dropbox media: " + extAssetID);
                }
            }
            else
            {
                m_logger.Error("InsertOffSiteAsset failed for Youtube ID: " + extAssetID);
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
