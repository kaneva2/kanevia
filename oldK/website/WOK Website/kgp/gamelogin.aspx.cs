///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Xml;
using System.Collections.Generic;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for gamelogin.
    /// </summary>
    public class gamelogin : KgpBasePage
    {
        const string CLIENT_PATH_WORLDS_ROOT = "GameFiles\\Worlds\\";   // TODO: merge with gameAddServer.CLIENTPATH_WORLDS_ROOT

        private const string GOTO_PLAYER  = "p";
        private const string GOTO_CHANNEL = "c";
        private const string GOTO_APT     = "a";
        private const string GOTO_URL     = "u";

        // 11/15 o Added one new result code below to identify the error category for all 3dapps. 
        //       o Added another column after kimPort to identify the actual error code. 
        // This is to address the conflicts among error codes from different sources (developer.get_game_info, wok.can_spawn_... and etc).
        // The result code in column#0 will now only represent the name spaces. Client should lookup detailed error code in the new column in desired.

        // game login resultCodes, must be one numeric char
        private const int GL_UNDEFINED           = -1;
        private const int GL_OK                  = 0; // Succeeded
        private const int GL_INVALID_PARAM       = 1; // Bad input
        private const int GL_USER_NOT_FOUND      = 2; // The email provided cannot be located in kaneva DB or the account has been deleted. If the user does not exist, error code is 0. Otherwise, error code is user status. 
        private const int GL_PROMPT_ALLOW        = 3; // The player must be prompted before going on to patch 3dApp
        private const int GL_CANT_SPAWN_TO_WOK   = 4; // Cannot send the user to the (WOK) destination - see detail error code column for "can_spawn" error codes from DB
        private const int GL_CANT_SPAWN_TO_3DAPP = 5; // Cannot send the user to the 3DApp - see detail error code column for an eLOGIN_RESULTS code from developer.get_game_info SP.
        private const int GL_VALIDATION_ERROR    = 9; // This is problematic.  db logic can return values in these gaps.  So adding cases to the sp requires that 
                                                      // developer be aware of call vector and insure no error code collisions.  A masked code value would address this.
				private const int GL_WORLD_FULL = 14;         // world is full


        private const int NO_ERROR = 0;

        private string formatResponseString(int lr, string msg, string format)
        {
            return formatResponseString(lr, msg, 0, "", "", 0, 0, "", 0, "", 0, false, "", 0, NO_ERROR, "", "", "", format);
        }

        private string formatResponseString(int lr, string msg, int errorCode, string format)
        {
            return formatResponseString(lr, msg, 0, "", "", 0, 0, "", 0, "", 0, false, "", 0, errorCode, "", "", "", format);
        }

        private string formatResponseString(int lr, string msg, int userid, string username, string server, int port, int zoneIndex, string gender, int gameId, string gameName, int parentGameId, bool IsIncubatorHosted, string serverVersion, string format)
        {
            return formatResponseString(lr, msg, userid, username, server, port, zoneIndex, gender, gameId, gameName, parentGameId, IsIncubatorHosted, null, 0, NO_ERROR, "", "", serverVersion, format);
        }

        private string formatResponseString(int lr, string msg, int userid, string username, string server, int port, int zoneIndex, string gender, int gameId, string gameName, int parentGameId, bool IsIncubatorHosted, int errorCode, string serverVersion, string format)
        {
            return formatResponseString(lr, msg, userid, username, server, port, zoneIndex, gender, gameId, gameName, parentGameId, IsIncubatorHosted, null, 0, errorCode, "", "", serverVersion, format);
        }

        private string formatResponseString(int lr, string msg, int userid, string username, string server, int port, int zoneIndex, string gender, int gameId, string gameName, int parentGameId, bool IsIncubatorHosted, string kimServer, int kimPort, int errorCode, string templateClientPath, string worldClientPath, string serverVersion, string format)
        {
            if (kimServer == null)
            {
                kimServer = Configuration.KIM_Server;
                kimPort = Configuration.KIM_Port;
            }

            string ret;

            if (format.Equals("xml"))
            {
                ret = formatXmlResponseString(lr, msg, userid, username, server, port, zoneIndex, gender, gameId, gameName, parentGameId, IsIncubatorHosted, kimServer, kimPort, errorCode, templateClientPath, worldClientPath, serverVersion);
            }
            else
            {
                ret = formatTDResponseString(lr, msg, userid, username, server, port, zoneIndex, gender, gameId, gameName, parentGameId, IsIncubatorHosted, kimServer, kimPort, errorCode, templateClientPath, worldClientPath);
            }

            return ret;
        }

        private string formatTDResponseString(int lr, string msg, int userid, string username, string server, int port, int zoneIndex, string gender, int gameId, string gameName, int parentGameId, bool IsIncubatorHosted, string kimServer, int kimPort, int errorCode, string templateClientPath, string worldClientPath)
        {
            return lr.ToString() + "\t" + msg + "\t" + userid + "\t" + username + "\t" + server + "\t" + port.ToString() + "\t" +
                    zoneIndex.ToString() + "\t" + gender + "\t" + gameId + "\t" + gameName + "\t" + parentGameId.ToString() + "\t" +
                    (IsIncubatorHosted ? "1" : "0").ToString() + "\t" +
                    kimServer + "\t" + kimPort.ToString() + "\t" +
                    errorCode.ToString() + "\t" +
                    templateClientPath + "\t" + worldClientPath;
        }

        private string formatXmlResponseString(int lr, string msg, int userid, string username, string server, int port, int zoneIndex, string gender, int gameId, string gameName, int parentGameId, bool IsIncubatorHosted, string kimServer, int kimPort, int errorCode, string templateClientPath, string worldClientPath, string serverVersion)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode root = doc.CreateElement("Result");
            doc.AppendChild(root);

            XmlElement elem = doc.CreateElement("ServerVersion");
            elem.InnerText = serverVersion;
            root.InsertBefore(elem, root.FirstChild);
                
            elem = doc.CreateElement("WorldClientPath");
            elem.InnerText = worldClientPath;
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("TemplateClientPath");
            elem.InnerText = templateClientPath.ToString();
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ErrorCode");
            elem.InnerText = errorCode.ToString();
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("KimPort");
            elem.InnerText = kimPort.ToString();
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("KimServer");
            elem.InnerText = kimServer;
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("IsIncubatorHosted");
            elem.InnerText = (IsIncubatorHosted ? "1" : "0").ToString();
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ParentGameId");
            elem.InnerText = parentGameId.ToString();
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("GameName");
            elem.InnerText = gameName;
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("GameId");
            elem.InnerText = gameId.ToString();
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("Gender");
            elem.InnerText = gender;
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ZoneIndex");
            elem.InnerText = zoneIndex.ToString();
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("Port");
            elem.InnerText = port.ToString();
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("Server");
            elem.InnerText = server;
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("UserName");
            elem.InnerText = username;
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("UserId");
            elem.InnerText = userid.ToString();
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ResultDescription");
            elem.InnerText = msg;
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ResultCode");
            elem.InnerText = lr.ToString();
            root.InsertBefore(elem, root.FirstChild);

            return root.OuterXml;
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Note: Use positive result code returns

            string format = (string.IsNullOrWhiteSpace(Request.Params["format"]) ? "" : Request.Params["format"]);

            if (Request.Params["user"] == null)
            {
                Response.Write(formatResponseString(GL_INVALID_PARAM, "User id not specified in request", format));
                return;
            }

            bool simpleResult = false;

            if (Request.Params["get"] != null) {
                simpleResult = (Request.Params["get"] == "user");
            }

            int server_type = (int)Constants.eGAME_SERVER_TYPE.ENGINE_SERVER;

            if (Request.Params["serverType"] != null) {
                server_type = Int32.Parse(Request.Params["serverType"]);
            }

            string emailaddress = Request.Params["user"];
            string webcallSource = Request.Params["source"];
            string runtimeId = Request.Params["runtimeId"];

            UserFacade uf = new UserFacade();
            User u = uf.GetUserByEmail(emailaddress);

            string responseStr = null;

            if (u.UserId != 0)
            {
                int port = 0;
                int zoneIndex = 0;
                string server = "";     // external name
                string gameName = null;
                int gameId = KanevaGlobals.WokGameId;
                int parentGameId = 0;
                bool isWokGame = true;
                bool IsIncubatorHosted = false;
                string templateClientPath = "";
                string worldClientPath = "";
                string serverVersion = "";

                if (!string.IsNullOrWhiteSpace(runtimeId))
                {
                    // Get system id for runtime
                    string systemId = GetMetricsFacade.GetClientRuntimeSystemId(runtimeId);

                    if (!string.IsNullOrWhiteSpace(systemId))
                    {
                        // Associate userId with userSystemId
                        GetUserFacade.InsertUsersSystemIds(u.UserId, systemId);

                        // If this system is suspended, make sure this user account is suspended too.
                        // Should only be one system suspension active at a given time.
                        List<UserSystemSuspension> suspensions = GetUserFacade.GetUserSystemSuspensions(systemId, true);
                        if (suspensions.Count > 0)
                        {
                            Dictionary<int, List<UserSuspension>> userSuspensions = GetUserFacade.GetUserSuspensionsBySystemBanId(suspensions[0].SystemBanId, true);
                            if(!userSuspensions.ContainsKey(u.UserId))
                            {
                                string banNote = "User suspended due to suspension for system id: " + suspensions[0].SystemId;
                                GetUserFacade.SaveUserSuspension(u.StatusId, new UserSuspension(0, u.UserId, suspensions[0].BanStartDate, suspensions[0].BanEndDate, suspensions[0].SystemBanId), 0, banNote);

                                if (suspensions[0].IsPermanent)
                                {
                                    try
                                    {
                                        (new CybersourceAdaptor()).CancelAllUserSubscriptions(u.UserId, "User has been banned.");
                                    }
                                    catch(Exception ex)
                                    {
                                        m_logger.Error("Error cancelling user subscription as part of system ban.", ex);
                                    }
                                }
                            }
                        }
                    }
                }

                switch ((Constants.eUSER_STATUS)u.StatusId)
                {
                    case Constants.eUSER_STATUS.REGNOTVALIDATED:
                    case Constants.eUSER_STATUS.REGVALIDATED:
                    case Constants.eUSER_STATUS.LOCKED:
                    case Constants.eUSER_STATUS.LOCKEDVALIDATED:
                        {
                            try
                            {
                                // 10/08 for installer's call added simpler result
                                if (simpleResult) {
                                    Response.Write(u.Username);
                                    return;
                                }

                                if (Request["goto"] == GOTO_URL) {
                                    if (Request["id"] == null)
                                    {
                                        Response.Write(formatResponseString(GL_VALIDATION_ERROR, "Bad parameter.", format));
                                        return;
                                    }

                                    // are we going to WOK?
                                    // url format stp://<gamename>/... 
                                    string url = Request["id"];
                                    int startIndex = url.IndexOf("://");
                                    if (startIndex > 1)
                                    {
                                        string[] s = url.Substring(startIndex + 3).Split(new Char[] { '/' });
                                        if (s.Length > 0)
                                        {
                                            gameName = s[0];
                                        }
                                    }
                                    // note tried this but spaces and dashes make it barf
                                    // Uri uri = new Uri(Request["id"]);
                                    //gameName = uri.Host;
                                }

                                isWokGame = gameName == null || gameName == KanevaGlobals.WokGameId.ToString() || String.Compare(gameName, KanevaGlobals.WokGameName, true) == 0;

                                // if user has not completed avatar select, send them to wok regardless
                                if (!u.Username.StartsWith("apphost") && !GetUserFacade.HasUserCompletedAvatarSelect(u.WokPlayerId))
                                {
                                    isWokGame = true;
                                }

								// assume WOK if not passed in, call fn to get wok info
                                if (isWokGame &&  server_type == (int)Constants.eGAME_SERVER_TYPE.ENGINE_SERVER)
                                {
                                    Constants.eCANSPAWN_RESULTS ret = getServerParameters(u.UserId, Request["goto"], Request["id"], u, ref server, ref serverVersion, ref port, ref zoneIndex, ref gameName, ref IsIncubatorHosted);
                                    if (ret!=Constants.eCANSPAWN_RESULTS.SUCCESS) {
                                        // gameName is WOK
                                        // try to send to their apt if didn't work
                                        ret = getServerParameters(u.UserId, null, null, u, ref server, ref serverVersion, ref port, ref zoneIndex, ref gameName, ref IsIncubatorHosted);
                                        if (ret != Constants.eCANSPAWN_RESULTS.SUCCESS)
                                        {
                                            responseStr = formatResponseString(GL_CANT_SPAWN_TO_WOK, "Error determining WOK server.", u.UserId, u.Username, server, port, zoneIndex, u.Gender, KanevaGlobals.WokGameId, gameName, 0, IsIncubatorHosted, (int)ret, serverVersion, format);
                                            m_logger.Info("gameLogin.PageLoad() - Error determining WOK server. responseStr = " + responseStr);
                                        }
                                    }

                                    // If successfully logging in to WOK from KIM call, record user progression
                                    if (ret == Constants.eCANSPAWN_RESULTS.SUCCESS && webcallSource == "KIM")
                                    {
                                        GetMetricsFacade.RecordNewUserFunnelProgression(u.UserId, "kim_gamelogin");
                                    }
                                }
                                else
                                {
                                    string serverHostName = ""; // internal name can be used for non-wok servers
                                    string reasonDesc = "";
                                    Constants.eLOGIN_RESULTS ret = getGameServerParameters(u.UserId, ref gameName, server_type, ref reasonDesc, ref server, ref serverVersion, ref port, ref gameId, ref parentGameId, ref serverHostName, ref IsIncubatorHosted);

                                    // if coming from IP that matches IP, send down host name.  This fixes NAT issues
                                    if (server == Common.GetVisitorIPAddress())
                                        server = serverHostName;

                                    // Send clientPath for TEMPLATE and WORLD from game_patch_urls table
                                    if (gameId != 0 && parentGameId != 0)   // Check if request is valid
                                    {

                                        // Pull all patch URLs for this game
                                        DataTable dtPatchUrls = GetGameFacade.GetPatchURLsByGameId(gameId, "patch_type!=''", "");

                                        for (int i = 0; i < dtPatchUrls.Rows.Count; i++)
                                        {
                                            DataRow dr = dtPatchUrls.Rows[i];
                                            string patchType = Convert.ToString(dr["patch_type"]);
                                            string clientPath = Convert.ToString(dr["client_path"]);

                                            if (patchType == "TEMPLATE")
                                            {
                                                templateClientPath = clientPath;
                                            }
                                            else if (patchType == "WORLD")
                                            {
                                                worldClientPath = clientPath;
                                            }
                                        }

                                        if (templateClientPath != "" && worldClientPath == "")
                                        {
                                            // If based on a template but no custom patch yet, provide a path anyway for developer mode
                                            // Formula below should match the one in gameAddServer.aspx
                                            worldClientPath = CLIENT_PATH_WORLDS_ROOT + gameId;
                                        }
                                    }

                                    if (ret != Constants.eLOGIN_RESULTS.SUCCESS)
                                    {
                                        if ( ret == Constants.eLOGIN_RESULTS.PROMPT_ALLOW )
                                            responseStr = formatResponseString(GL_PROMPT_ALLOW, reasonDesc, u.UserId, u.Username, server, port, zoneIndex, u.Gender, gameId, gameName, parentGameId, IsIncubatorHosted, serverVersion, format);
                                        else
                                            responseStr = formatResponseString(GL_CANT_SPAWN_TO_3DAPP, reasonDesc, u.UserId, u.Username, server, port, zoneIndex, u.Gender, gameId, gameName, parentGameId, IsIncubatorHosted, (int)ret, serverVersion, format);
                                    }
                                }
                            }
                            catch (FormatException)
                            {
                                responseStr = formatResponseString( GL_VALIDATION_ERROR,  "Bad parameter.", format);
                            }
                        }
                        break;


                    case Constants.eUSER_STATUS.DELETED:
                    case Constants.eUSER_STATUS.DELETEDVALIDATED:
                    case Constants.eUSER_STATUS.DELETEDBYUS:
                        responseStr = formatResponseString( GL_USER_NOT_FOUND, "Account has been deleted.", u.StatusId, format);
                        break;
                }

                if (responseStr == null) {
                    if (!gameHasCapacity(gameId)) {
                        responseStr = formatResponseString(GL_WORLD_FULL, "world is full", u.UserId, u.Username, "", 0, zoneIndex, u.Gender, gameId, gameName, parentGameId, IsIncubatorHosted, serverVersion, format );
                    }
                }

                if (responseStr == null)
                {
                    responseStr = formatResponseString(GL_OK, "success", u.UserId, u.Username, server, port, zoneIndex, u.Gender, gameId, gameName, parentGameId, IsIncubatorHosted, null, 0, NO_ERROR, templateClientPath, worldClientPath, serverVersion, format);
                }
            }
            else
            {
                responseStr = formatResponseString( GL_USER_NOT_FOUND,  "E-mail address not found", format);
            }

            if (simpleResult)
                Response.Write(responseStr.Substring(0, 1));    // used by starpublish.nsi (obsolete?) Simple result will only contain user name if succeed, or an error code if fail.
            else
                Response.Write(responseStr);
        }

        /// <summary>
        /// get info about another game
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="gameName"></param>
        /// <param name="server"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        private Constants.eLOGIN_RESULTS getGameServerParameters(int user_id, ref string gameName, int server_type, ref string reasonDesc, ref string server, ref string serverVersion, ref int port, ref int gameId, ref int parentGameId, ref string serverHostName, ref bool IsIncubatorHosted)
        {
            Constants.eLOGIN_RESULTS ret = Constants.eLOGIN_RESULTS.FAILED;

            Hashtable parameters = new Hashtable();
            string wokDb = KanevaGlobals.DbNameKGP;
            string kanevaDb = KanevaGlobals.DbNameKaneva;

            string sql = "CALL get_game_info( @user_id, @game_name, @server_type, @reasonCode, @reasonDesc, @server, @port, @gameId, @parentGameId, @newGameName, @serverHostName ); select CAST(@reasonCode as UNSIGNED INT) as reasonCode, @reasonDesc, @server, CAST(@port as UNSIGNED INT) as port, CAST(@gameId as UNSIGNED INT) as gameId, CAST(@parentGameId as UNSIGNED INT) as parentGameId, @newGameName, @serverHostName; ";
            parameters.Add("@user_id", user_id);
            parameters.Add("@game_name", gameName);
            parameters.Add("@server_type", server_type);

            DataRow dr = KanevaGlobals.GetDatabaseUtilityDeveloper().GetDataRow(sql, parameters, false, "USE developer");

            if (dr != null && dr["reasonCode"] != null && dr["@reasonDesc"] != null )
            {
                ret = (Constants.eLOGIN_RESULTS)Convert.ToInt32(dr["reasonCode"]);
                reasonDesc = dr["@reasonDesc"].ToString();
                if (ret == Constants.eLOGIN_RESULTS.SUCCESS || ret == Constants.eLOGIN_RESULTS.PROMPT_ALLOW)
                {
                    System.Diagnostics.Debug.WriteLine(">>>" + dr["reasonCode"].ToString() + ", " + dr["@server"].ToString() + ", " + dr["port"].ToString());

                    gameId = Convert.ToInt32(dr["gameId"]);
                    parentGameId = Convert.ToInt32(dr["parentGameId"]);
                    gameName = dr["@newGameName"].ToString();

                    if (ret == Constants.eLOGIN_RESULTS.SUCCESS)
                    {
                        server = dr["@server"].ToString();
                        port = Convert.ToInt32(dr["port"]);
                        serverHostName = dr["@serverHostName"].ToString();
                        serverVersion = GetGameFacade.GetServerVersion(gameId, serverHostName, port, server_type);
                    }
                }

            }

            // Get incubator hosted flag
            if (gameId > 0)
            {
                string newGameName = null;
                try
                {
                    if (dr!=null && !Convert.IsDBNull(dr["@newGameName"]))
                    {
                        newGameName = Convert.ToString(dr["@newGameName"]);
                        if( newGameName==null || newGameName=="" )
                        {
                            newGameName = gameName;
                        }
                    }
                }
                catch( Exception )
                {
                    newGameName = gameName;
                }

                sql = "select game_name, incubator_hosted from games where game_name = @gameName";
                parameters.Clear();
                parameters.Add("@gameName", newGameName);

                dr = KanevaGlobals.GetDatabaseUtilityDeveloper().GetDataRow(sql, parameters, false, "USE developer");

                if (dr != null)
                {
                    IsIncubatorHosted = Convert.ToInt32(dr["incubator_hosted"]).Equals(1) ? true : false;
                }
            }

            

            return ret;
        }

        /// <summary>
        /// get the server/port where they need to go for WOK
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="gotoWhere"></param>
        /// <param name="idToGoto"></param>
        /// <param name="server"></param>
        /// <param name="port"></param>
        /// <param name="zoneIndex"></param>
        /// <returns></returns>
        private Constants.eCANSPAWN_RESULTS getServerParameters(int user_id, string gotoWhere, string idToGoto, User user, ref string server, ref string serverVersion, ref int port, ref int zoneIndex, ref string gameName, ref bool IsIncubatorHosted)
        {
            Constants.eCANSPAWN_RESULTS ret = Constants.eCANSPAWN_RESULTS.FAILED;

            Hashtable parameters = new Hashtable();
            zoneIndex = 0; // only used for channels
            int serverId = 0;

            string sql = "";

            string wokDb = KanevaGlobals.DbNameKGP;
            string kanevaDb = KanevaGlobals.DbNameKaneva;

            // get the game name first to return as ref param
            sql = "select game_name, incubator_hosted from games where game_id = @game_id";
            parameters.Add("@game_id", KanevaGlobals.WokGameId);

            DataRow dr = KanevaGlobals.GetDatabaseUtilityDeveloper().GetDataRow(sql, parameters, false, "USE developer");

            if (dr != null)
            {
                gameName = dr["game_name"].ToString();
                IsIncubatorHosted = Convert.ToInt32(dr["incubator_hosted"]).Equals(1) ? true : false;
            }

            parameters.Clear();

            if (gotoWhere == GOTO_PLAYER)
            {
                if (idToGoto == null)
                    return ret;

                // canPlayerSpawnToPlayer2( IN sourceKanevaId INT, IN destKanevaId INT, 
                //						   OUT reasonCode INT(11), OUT server VARCHAR(64), OUT port INT(11),
                //						   OUT x float, OUT y float, OUT z float)
                sql = "CALL  " + wokDb + ".canPlayerSpawnToPlayer2( @user_id, @player_id, @country, @birth_date, @show_mature, @reasonCode, @server, @port, @x, @y, @z, @zoneIndex, @serverId ); select CAST(@reasonCode as unsigned INT) as reasonCode, @server, CAST(@port as unsigned INT) as port, CAST(@zoneIndex as unsigned INT) as zoneIndex, CAST(@serverId as unsigned INT) AS serverId; ";
                parameters.Add("@user_id", user_id);
                parameters.Add("@player_id", idToGoto);

                m_logger.Info("gameLogin.getServerParameters() - Calling canPlayerSpawnToPlayer2 with userId = " + user_id + " and idToGoto = " + idToGoto);

            }
            else if (gotoWhere == GOTO_CHANNEL)
            {
                if (idToGoto == null)
                    return ret;

                // canPlayerSpawnToChannelZone2( IN accountId INT(11), IN instanceId INT(11), 
                //							   OUT reasonCode INT(11), OUT server VARCHAR(64), OUT port INT(11), OUT zoneIndex INT(11)  )
                sql = "CALL  " + wokDb + ".canPlayerSpawnToChannelZone2( @user_id, @country, @birth_date, @show_mature, @channel_id, @reasonCode, @server, @port, @zoneIndex, @serverId );  select CAST(@reasonCode as unsigned INT) as reasonCode, @server, CAST(@port as unsigned INT) as port, CAST(@zoneIndex as unsigned INT) as zoneIndex, CAST(@serverId as unsigned INT) AS serverId; ";
                parameters.Add("@user_id", user_id);
                parameters.Add("@channel_id", idToGoto);

                m_logger.Info("gameLogin.getServerParameters() - Calling canPlayerSpawnToChannelZone2 with userId = " + user_id + " and idToGoto = " + idToGoto);
            }
            else // default to apartment
            {
                int apt_id = user_id;
                if (idToGoto != null)
                {
                    int temp = 0;
                    if (Int32.TryParse(idToGoto, out temp))
                        apt_id = temp;
                }

                // canPlayerSpawnToApartment2( IN sourceKanevaId INT, IN destKanevaId INT, 
                //								OUT reasonCode INT, OUT server VARCHAR(64), OUT port INT(11) )
                sql = "CALL " + wokDb + ".canPlayerSpawnToApartment2( @user_id, @apt_id, @country, @birth_date, @show_mature, @reasonCode, @server, @port, @zoneIndex, @serverId );  select CAST(@reasonCode as unsigned INT) as reasonCode, @server, CAST(@port as unsigned INT) as port, CAST(@zoneIndex as unsigned INT) as zoneIndex, CAST(@serverId as unsigned INT) AS serverId; ";
                parameters.Add("@user_id", user_id);
                parameters.Add("@apt_id", apt_id);

                m_logger.Info("gameLogin.getServerParameters() - Calling canPlayerSpawnToApartment2 with userId = " + user_id + " and idToGoto = " + idToGoto);
            }
            DateTime bd = Convert.ToDateTime(user.BirthDate);
            string birthDateStr = bd.ToString("yyyy-MM-dd", System.Globalization.DateTimeFormatInfo.InvariantInfo); // get back to MySQL format
            parameters.Add("@country", user.Country);
            parameters.Add("@birth_date", birthDateStr);
            parameters.Add("@show_mature", user.ShowMature ? 1 : 0);

            dr = KanevaGlobals.GetDatabaseUtility().GetDataRow(sql, parameters, false, "USE " + kanevaDb);

            if (dr != null && dr["reasonCode"] != null)
            {
                System.Diagnostics.Debug.WriteLine(">>>" + dr["reasonCode"].ToString() + ", " + dr["@server"].ToString() + ", " + dr["port"].ToString());

                try
                {
                    ret = (Constants.eCANSPAWN_RESULTS)Convert.ToInt32(dr["reasonCode"]);
                }
                catch (System.Exception)
                {
                    ret = Constants.eCANSPAWN_RESULTS.CRITICAL_ERROR;
                }

                if (ret == Constants.eCANSPAWN_RESULTS.SUCCESS)
                {
                    server = dr["@server"].ToString();
                    port = Convert.ToInt32(dr["port"]);
                    zoneIndex = Convert.ToInt32(dr["zoneIndex"]);
                    serverId = Convert.ToInt32(dr["serverId"]);
                    serverVersion = GetGameFacade.GetServerVersion(serverId);
                }
            }

            return ret;
        }

        /**
         * We need to insure that in the event that the world is hosted, i.e. Not ours, but hosted in an incubator, 
         * then we support
         */
        private bool gameHasCapacity(int gameId) {

            Hashtable parameters = new Hashtable();

            // this query is designed To work on non-hosted apps as well.
            // Note the outer join
            string sql = @"select 
                              coalesce(gs.max_players, i.dflt_world_max_pop, 0 )   as max_pop
                                , sum(number_of_players)  as curr_pop 
                            from 
                                developer.games as g 
                                    join developer.game_servers as gs on g.game_id = gs.game_id 
                                    left outer join developer.incubators   as i    on   g.parent_game_id = i.parent_game_id 
                            where 
                                g.game_id =@game_id"; 

            parameters.Add("@game_id", gameId );


            DataRow dr = KanevaGlobals.GetDatabaseUtilityDeveloper().GetDataRow(sql, parameters, false, "USE developer");

            // if this actually failed for some reason, it should be an infrastructure exception
            if ( dr                 == null ) return false;
            if ( dr["max_pop"]      == null ) return false;
            if ( dr["curr_pop"]     == null ) return false;

            int max_pop     = Convert.ToInt32(dr["max_pop"]);
            int curr_pop    = Convert.ToInt32(dr["curr_pop"]);
            return curr_pop < max_pop;
        }


        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
