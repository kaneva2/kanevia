///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class getCurrentTime : KgpBasePage
    {
        // Response types
        private string _errorResult = "<Result><ReturnCode>{0}</ReturnCode><ReturnDescription>{1}</ReturnDescription></Result>";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dtNow = GetCurrentTime();

            if (dtNow == null || dtNow.Rows.Count != 1)
            {
                Response.Write(string.Format(_errorResult, -1, "Failed to retrieve Current Time"));
                return;
            }
            
            DataSet ds = new DataSet();
            ds.DataSetName = "Result";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ds.GetXml());
            XmlNode root = doc.DocumentElement;

            XmlElement elem = doc.CreateElement("CurrentTime");
            elem.InnerText = dtNow.Rows[0]["CurrentTime"].ToString();
            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = "success";

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";

            root.InsertBefore(elem, root.FirstChild);

            Response.Write(root.OuterXml);
        }

        /// <summary>
        /// Get the current time
        /// </summary>
        private DataTable GetCurrentTime()
        {
            return KanevaGlobals.GetDatabaseUtilityReadOnly().GetDataTable("SELECT NOW() AS CurrentTime");
        }
    }
}