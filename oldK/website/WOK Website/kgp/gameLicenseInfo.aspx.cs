///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class gameLicenseInfo : KgpBasePage
    {
        protected string CalcSecondsRemaining(String endDate)
        {
            //
            // format Day/Month/Year Hour:Min:Sec XM
            //

            string sep = " :/";
            string []s = endDate.Split(sep.ToCharArray());
            int year = Convert.ToInt32(s[2]);
            int month = Convert.ToInt32(s[0]);
            int day = Convert.ToInt32(s[1]);
            int hour = Convert.ToInt32(s[3]);
            int minute = Convert.ToInt32(s[4]);
            int second = Convert.ToInt32(s[5]);
            if (hour == 12)
            {
                hour = 0;
            }
            if (s[6] == "PM")
            {
                hour += 12;
            }

            DateTime endDateTime = new DateTime(year, month, day, hour, minute, second);
            long secondsLeft = (long) (endDateTime - DateTime.Now).TotalSeconds;
            if (secondsLeft < 0)
            {
                secondsLeft = 0;
            }


            return Convert.ToString(secondsLeft);


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            String response;
            String licenseKey = Request.Params["licenseKey"];
            String hostName = Request.Params["hostName"];
            if (licenseKey == null || hostName == null)
            {
                response = "3";
            } else
            {
                DataRow dr = ServerUtility.GetGameLicenseInfo(licenseKey,hostName);
                if (dr != null)
                {
                    string returnCode = "0";
                    string secondsRemaining = CalcSecondsRemaining(dr["end_date"].ToString());
                    string serverId = dr["server_id"].ToString();
                    string licenseStatus = dr["license_status_id"].ToString();
                    if (serverId == "")
                    {
                        returnCode = "1";
                    }
                        
                    response = returnCode + " " + dr["name"].ToString() + " " + licenseStatus + " " + secondsRemaining;
                } else {
                    response = "2";
                }
            }
            Response.Write(response);
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
