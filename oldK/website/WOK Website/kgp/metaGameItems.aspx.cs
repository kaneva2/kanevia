///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Web;
using System.Web.UI;
using System.Data;
using log4net;
using System.Linq;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class metaGameItems : KgpBasePage
    {
        private string _action;
        private string _itemType;
        private int _zoneInstanceId;
        private int _zoneType;
        private string _gameItemName;
        private string _gameItemType;
        private List<KeyValuePair<string, int>> _itemAmounts;

        protected void Page_Load (object sender, EventArgs e)
        {
            if (CheckUserId (false))
            {
                GetRequestParams();

                // Process based on action.
                switch (_action)
                {
                    case "getBalances":
                        ProcessGetBalances();
                        break;
                    case "convertToRewards":
                        ProcessConvertToRewards();
                        break;
                    case "spendItems":
                        ProcessSpendItems();
                        break;
                    default:
                        Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid action</ResultDescription>\r\n</Result>");
                        break;
                }
            }
        }

        /// <summary>
        /// Returns the xml for the meta game item balances.
        /// </summary>
        private string BuildMetaGameItemBalanceXML(List<MetaGameItem> items, UserMetaGameItemBalances balances, int dailyTotal, int amtAvailableToConvert, double rewards = -1)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("Result"));
            XmlNode root = doc.DocumentElement;
            
            XmlElement elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";
            root.AppendChild(elem);

            elem = doc.CreateElement("ResultDescription");
            elem.InnerText = "success";
            root.AppendChild(elem);

            elem = doc.CreateElement("DailyConversionsTotal");
            elem.InnerText = dailyTotal.ToString();
            root.AppendChild(elem);

            elem = doc.CreateElement("DailyConversionsRemaining");
            elem.InnerText = amtAvailableToConvert.ToString();
            root.AppendChild(elem);

            elem = doc.CreateElement("ItemBalances");
            foreach (MetaGameItem item in items)
            {
                KeyValuePair<string, int> itemBalance = balances.SingleOrDefault(i => i.Key.Equals(item.ItemName));

                XmlElement itemElem = doc.CreateElement("Item");
                itemElem.InnerXml += string.Format("<Name>{0}</Name>", item.ItemName);
                itemElem.InnerXml += string.Format("<Balance>{0}</Balance>", itemBalance.Value);
                itemElem.InnerXml += string.Format("<ConversionValueRewards>{0}</ConversionValueRewards>", item.GetConversionValue(Currency.CurrencyType.REWARDS));
                elem.AppendChild(itemElem);
            }
            root.AppendChild(elem);

            if (rewards > -1)
            {
                elem = doc.CreateElement("UserRewardBalance");
                elem.InnerText = rewards.ToString();
                root.AppendChild(elem);
            }
            
            return root.OuterXml;
        }

        /// <summary>
        /// Returns UserMetaGameItemBalances for the specified item type.
        /// </summary>
        private void ProcessGetBalances()
        {
            if (string.IsNullOrWhiteSpace(_itemType))
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid itemType</ResultDescription>\r\n</Result>");
                return;
            }

            List<MetaGameItem> items = GetGameFacade.GetMetaGameItems(_itemType);
            UserMetaGameItemBalances balances = GetUserFacade.GetUserMetaGameItemBalances(m_userId);
            int dailyTotal = GetUserFacade.GetUserDailyTotalMGIConversionAmount(m_userId, _itemType, Currency.CurrencyType.REWARDS);
            int amtAvailableToConvert = (items.Count > 0 ? items[0].GetAmountAvailableToConvert(Currency.CurrencyType.REWARDS, dailyTotal) : 0);

            Response.Write(BuildMetaGameItemBalanceXML(items, balances, dailyTotal, amtAvailableToConvert));
        }

        /// <summary>
        /// Converts the specified meta game items to rewards.
        /// Returns the new balances if the update was successful.
        /// </summary>
        private void ProcessConvertToRewards()
        {
            int dailyTotal;
            int amtAvailableToConvert;

            // Per discussion with Paul, the only item type we will allow the client-side version of this call to redeem is "Gem"
            if (string.IsNullOrWhiteSpace(_itemType) || !_itemType.Equals("Gem"))
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid itemType</ResultDescription>\r\n</Result>");
                return;
            }

            if (_itemAmounts.Count == 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid itemAmounts</ResultDescription>\r\n</Result>");
                return;
            }

            try
            {
                bool conversionSucceeded = GetUserFacade.ConvertMetaGameItemsToCurrency(m_userId, _itemType, Currency.CurrencyType.REWARDS, _itemAmounts, out dailyTotal, out amtAvailableToConvert);

                if (!conversionSucceeded)
                {
                    Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>unable to convert items to rewards.</ResultDescription>\r\n</Result>");
                    return;
                }
            }
            catch (ArgumentException e)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>unable to convert items to rewards. " + e.Message + "</ResultDescription>\r\n</Result>");
                return;
            }

            // Return user balances
            List<MetaGameItem> items = GetGameFacade.GetMetaGameItems(_itemType);
            UserMetaGameItemBalances balances = GetUserFacade.GetUserMetaGameItemBalances(m_userId);
            UserBalances userBalances = GetUserFacade.GetUserBalances(m_userId);

            Response.Write(BuildMetaGameItemBalanceXML(items, balances, dailyTotal, amtAvailableToConvert, userBalances.Rewards));
        }

        /// <summary>
        /// Debits the user's meta game item balances by the specified amount.
        /// Returns the new balances if the update was successful.
        /// </summary>
        private void ProcessSpendItems()
        {
            // Per discussion with Paul, the only item type we will allow the client-side version of this call to redeem is "Gem"
            if (string.IsNullOrWhiteSpace(_itemType) || !_itemType.Equals("Gem"))
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid itemType</ResultDescription>\r\n</Result>");
                return;
            }

            if (_itemAmounts.Count == 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid itemAmounts</ResultDescription>\r\n</Result>");
                return;
            }

            if (_zoneInstanceId <= 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid zoneInstanceId</ResultDescription>\r\n</Result>");
                return;
            }

            if (_zoneType <= 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid zoneType</ResultDescription>\r\n</Result>");
                return;
            }

            if (string.IsNullOrWhiteSpace(_gameItemName))
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid gameItemName</ResultDescription>\r\n</Result>");
                return;
            }

            if (string.IsNullOrWhiteSpace(_gameItemType))
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid gameItemType</ResultDescription>\r\n</Result>");
                return;
            }

            try
            {
                bool purchaseSucceeded = GetUserFacade.SpendMetaGameItems(KanevaWebGlobals.CurrentUser.UserId, _zoneInstanceId, _zoneType, _itemType, _itemAmounts,
                    _gameItemName, _gameItemType);

                if (!purchaseSucceeded)
                {
                    Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>unable to complete transaction.</ResultDescription>\r\n</Result>");
                    return;
                }
            }
            catch (ArgumentException e)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>unable to complete transaction. " + e.Message + "</ResultDescription>\r\n</Result>");
                return;
            }

            // If we get here, we're good!
            Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>");
            return;
        }

        /// <summary>
        /// Reads all request parameters from query string and provides appropriate defaults.
        /// </summary>
        private void GetRequestParams()
        {
            int iOut;

            _action = Request.Params["action"];
            _itemType = Request.Params["itemType"];
            _zoneInstanceId = int.TryParse(Request.Params["zoneInstanceId"], out iOut) ? iOut : 0;
            _zoneType = int.TryParse(Request.Params["zoneType"], out iOut) ? iOut : 0;
            _gameItemName = Request.Params["gameItemName"];
            _gameItemType = Request.Params["gameItemType"];
            _itemAmounts = new List<KeyValuePair<string, int>>();

            // Item amounts should be specified as a list of pipe-delmited tuples.
            // Example: itemAmounts=World Coin,10|Emerald,15.
            if (!string.IsNullOrWhiteSpace(Request.Params["itemAmounts"]))
            {
                try
                {
                    string[] itemList = Request.Params["itemAmounts"].Split('|');
                    foreach (string item in itemList)
                    {
                        string[] itemValues = item.Split(',');

                        // Quick check to make sure that key and value aren't swapped
                        if (Int32.TryParse(itemValues[0], out iOut) || !Int32.TryParse(itemValues[1], out iOut))
                        {
                            throw new ArgumentException();
                        }

                        _itemAmounts.Add(new KeyValuePair<string, int>(itemValues[0], Int32.Parse(itemValues[1])));
                    }
                }
                catch (Exception) 
                {
                    _itemAmounts.Clear();
                }
            }
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
    }
}