///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for travellist.
	/// </summary>
	public class peopleInZone : KgpBasePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
				if ( (Request.Params["start"] == null) || (Request.Params["max"] == null) )
				{
					string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>page number or items per page not specified</ResultDescription>\r\n</Result>";
					Response.Write(errorStr);
					return;
				}

				int page = Int32.Parse(Request.Params["start"].ToString());
				int items_per_page = Int32.Parse(Request.Params["max"].ToString());

				DataSet ds = new DataSet();

				ds.DataSetName = "Result";

				PagedDataTable pdt;

				pdt = GetPlayersInMyZone(m_userId, page, items_per_page);

				if ( pdt == null)
				{
					string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>current player not in zone</ResultDescription>\r\n</Result>";
					Response.Write(errorStr);
					return;
				}

				pdt.TableName = "InZone";
				ds.Tables.Add(pdt);

				XmlDocument doc = new XmlDocument();
				doc.LoadXml(ds.GetXml());
				XmlNode root = doc.DocumentElement;

				int numRecords = pdt.Rows.Count;
				XmlElement elem = doc.CreateElement("NumberRecords");
				elem.InnerText= numRecords.ToString();

				root.InsertBefore( elem, root.FirstChild);

				int totalNumRecords = pdt.TotalCount;
				elem = doc.CreateElement("TotalNumberRecords");
				elem.InnerText= totalNumRecords.ToString();

				root.InsertBefore( elem, root.FirstChild);

				elem = doc.CreateElement("ReturnDescription");
				elem.InnerText="success";

				root.InsertBefore( elem, root.FirstChild);

				elem = doc.CreateElement("ReturnCode");
				elem.InnerText="0";

				root.InsertBefore( elem, root.FirstChild);

				Response.Write(root.OuterXml);
			}
		}

		/// <summary>
		/// get list of players in the same zone as userId
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		private PagedDataTable GetPlayersInMyZone( Int32 userId, Int32 pageNumber, Int32 pageSize)
		{
			string dbName = KanevaGlobals.DbNameKGP;

			string sql = "SELECT p.player_id, pz.current_zone_index, pz.current_zone_instance_id, p.server_id ";

			sql += "FROM " + dbName + ".players p , " + dbName + ".player_zones pz ";

			sql += "WHERE p.player_id = pz.player_id AND p.kaneva_user_id=@userId";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@userId", userId);

			DataRow drMyUser = KanevaGlobals.GetDatabaseUtility().GetDataRow (sql, parameters, false);

			if ( drMyUser != null)
			{
				int current_zone_index = 0;
				int current_zone_instance_id = 0;
				int server_id = 0;
				int player_id = 0;

				current_zone_index = Convert.ToInt32 (drMyUser ["current_zone_index"]);
				current_zone_instance_id = Convert.ToInt32 (drMyUser ["current_zone_instance_id"]);
				server_id = Convert.ToInt32 (drMyUser ["server_id"]);
				player_id = Convert.ToInt32 (drMyUser ["player_id"]);

				string selectList = "p.name, p.player_id, p.kaneva_user_id as user_id, IFNULL(p.title,'') as title ";

				string tableList = " " + dbName + ".players p, " + dbName + ".player_zones pz ";

				string whereClause = " p.player_id <> @playerId AND p.player_id = pz.player_id AND pz.current_zone_index=" + current_zone_index + 
									 " AND pz.current_zone_instance_id=" + current_zone_instance_id + " AND p.server_id=" + server_id;

				parameters.Clear();

				parameters["@playerId"] = player_id;

				string orderby = "p.name";

				return  KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
			}

			return null;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}