///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class allowAccess : KgpBasePage // System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int gameId = 0;
            if ( Request["gid"] == null || !Int32.TryParse( Request["gid"], out gameId ) )
            {
                Response.Write( "No gid supplied" );
                return;
            }
            
            GameFacade gf = new GameFacade();
            Game game = gf.GetGameByGameId( gameId );
            if ( game != null )
            {
                litName1.Text = game.GameName;
                litName2.Text = game.GameName;
                litDesc.Text = game.GameDescription; // is HTML Detailed Desc in UI, .GameSynopsis is Short Desc;

                // http://preview.kaneva.com/channel/TexasHoldemPreview.channel
                aCommunityLink.HRef = "http://" + KanevaGlobals.SiteName + "/channel/" + game.GameName.Replace(" ","") + ".channel";
                aCommunityLink.InnerText = game.GameName;

                imgThumbnail.Src = KanevaGlobals.GameImageServer + "/" + (game.GameImagePath).Replace ("\\", "/");
            }
        }

    }
}
