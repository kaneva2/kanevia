///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Summary description for texturesList.
	/// </summary>
	public class userAccess : KgpBasePage
	{
        private const int OWNER_ONLY = 3;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( CheckUserId( false ) )
			{
				string responseStr = null;

                try
                {
                    if (Request.Params["action"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                    else if (Request.Params["gameId"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>gameId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    string actionreq = Request.Params["action"];

                    if (actionreq.Equals("GetAccessRights"))
                    {
                        DateTime secondLastLogon;
                        bool isGM, isAdmin;

                        // Select the world's community_id value
                        int intOut = 0;
                        int gameId = Int32.TryParse(Request.Params["gameId"], out intOut) ? intOut : 0;
                        int zoneIndex = Int32.TryParse(Request.Params["zoneIndex"], out intOut) ? intOut : 0;
                        int zoneInstanceId = Int32.TryParse(Request.Params["zoneInstanceId"], out intOut) ? intOut : 0;
                        int zoneType = Int32.TryParse(Request.Params["zoneType"], out intOut) ? intOut : 0;

                        int communityId = GetCommunityFacade.GetCommunityIdFromWorldIds(KanevaGlobals.WokGameId, gameId, zoneIndex, zoneInstanceId, zoneType);
                        Community community = GetCommunityFacade.GetCommunity(communityId);

                        // Get admin
                        GetUserFacade.GetPlayerAdmin(m_userId, out isGM, out isAdmin, out secondLastLogon);

                        // Get Privs
                        CommunityMember communityMember = GetCommunityFacade.GetCommunityMember(communityId, m_userId);

                        // Build result
                        XmlDocument doc = new XmlDocument();
                        XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");

                        XmlElement elem = doc.CreateElement("Owner");

                        if (communityMember.AccountTypeId == (int)CommunityMember.CommunityMemberAccountType.OWNER)
                        {
                            elem.InnerText = "1";
                        }
                        else
                        {
                            elem.InnerText = "0";
                        }

                        root.InsertBefore(elem, root.FirstChild);

                        elem = doc.CreateElement("Moderator");

                        if (communityMember.AccountTypeId == (int)CommunityMember.CommunityMemberAccountType.MODERATOR)
                        {
                            elem.InnerText = "1";
                        }
                        else
                        {
                            elem.InnerText = "0";
                        }

                        root.InsertBefore(elem, root.FirstChild);

                        elem = doc.CreateElement("GM");

                        if (isGM)
                        {
                            elem.InnerText = "1";
                        }
                        else
                        {
                            elem.InnerText = "0";
                        }

                        root.InsertBefore(elem, root.FirstChild);

                        elem = doc.CreateElement("IsAdmin");

                        if (isAdmin)
                        {
                            elem.InnerText = "1";
                        }
                        else
                        {
                            elem.InnerText = "0";
                        }

                        root.InsertBefore(elem, root.FirstChild);

                        elem = doc.CreateElement("isPrivate");

                        if (community.IsPublic == "N")
                        {
                            elem.InnerText = "1"; // Private
                        }
                        else if (community.IsPublic == "I")
                        {
                            elem.InnerText = "2"; // Owner only
                        }
                        else
                        {
                            elem.InnerText = "0"; // Public
                        }

                        root.InsertBefore(elem, root.FirstChild);

                        // Check Access Pass
                        elem = doc.CreateElement("isAP");
                        elem.InnerText = "0";
                        DataTable dtWorldPassGroups = GetCommunityFacade.GetCommunityPasses(community.CommunityId, community.CommunityTypeId);
                        if (dtWorldPassGroups != null && dtWorldPassGroups.Rows.Count > 0)
                        {
                            foreach (DataRow row in dtWorldPassGroups.Rows)
                            {
                                if (Convert.ToInt32(row["pass_group_id"]) == KanevaGlobals.AccessPassGroupID)
                                {
                                    elem.InnerText = "1";
                                    break;
                                }
                            }
                        }
                        root.InsertBefore(elem, root.FirstChild);

                        int numRecords = 1;
                        elem = doc.CreateElement("NumberRecords");
                        elem.InnerText = numRecords.ToString();

                        root.InsertBefore(elem, root.FirstChild);

                        int totalNumRecords = 1;
                        elem = doc.CreateElement("TotalNumberRecords");
                        elem.InnerText = totalNumRecords.ToString();

                        root.InsertBefore(elem, root.FirstChild);

                        elem = doc.CreateElement("ReturnDescription");
                        elem.InnerText = "success";

                        root.InsertBefore(elem, root.FirstChild);

                        elem = doc.CreateElement("ReturnCode");
                        elem.InnerText = "0";

                        root.InsertBefore(elem, root.FirstChild);

                        Response.Write(root.OuterXml);
                    }
                    else if (actionreq.Equals("SetAccess"))
                    {
                        if (Request.Params["access"] == null)
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>access not specified</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }

                        int access = Int32.Parse(Request.Params["access"].ToString());

                        int ret = -1;

                        // Select the world's community_id value
                        int intOut = 0;
                        int gameId = Int32.TryParse(Request.Params["gameId"], out intOut) ? intOut : 0;
                        int zoneIndex = Int32.TryParse(Request.Params["zoneIndex"], out intOut) ? intOut : 0;
                        int zoneInstanceId = Int32.TryParse(Request.Params["zoneInstanceId"], out intOut) ? intOut : 0;
                        int zoneType = Int32.TryParse(Request.Params["zoneType"], out intOut) ? intOut : 0;
                        int? isAP = Int32.TryParse(Request.Params["isAP"], out intOut) ? (int?)intOut : null;

                        int communityId = GetCommunityFacade.GetCommunityIdFromWorldIds(KanevaGlobals.WokGameId, gameId, zoneIndex, zoneInstanceId, zoneType);

                        if (communityId > 0)
                        {
                            // Get user info
                            DateTime secondLastLogon;
                            bool isGM, isAdmin;
                            GetUserFacade.GetPlayerAdmin(m_userId, out isGM, out isAdmin, out secondLastLogon);
                            CommunityMember communityMember = GetCommunityFacade.GetCommunityMember(communityId, m_userId);

                            // check access rights
                            if (isGM ||
                                    communityMember.AccountTypeId == (int)CommunityMember.CommunityMemberAccountType.OWNER ||
                                    communityMember.AccountTypeId == (int)CommunityMember.CommunityMemberAccountType.MODERATOR)
                            {
                                Community community = GetCommunityFacade.GetCommunity(communityId);

                                if ((access == (int)eGAME_ACCESS.PUBLIC))
                                {
                                    community.IsPublic = "Y";
                                    ret = GetCommunityFacade.UpdateCommunity(community);
                                }
                                else if ((access == (int)eGAME_ACCESS.PRIVATE))
                                {
                                    community.IsPublic = "N";
                                    ret = GetCommunityFacade.UpdateCommunity(community);
                                }
                                else if ((access == OWNER_ONLY))
                                {
                                    community.IsPublic = "I";
                                    ret = GetCommunityFacade.UpdateCommunity(community);
                                }

                                if (isAP != null)
                                {
                                    bool alreadyAP = false;
                                    DataTable dtWorldPassGroups = GetCommunityFacade.GetCommunityPasses(community.CommunityId, community.CommunityTypeId);
                                    if (dtWorldPassGroups != null && dtWorldPassGroups.Rows.Count > 0)
                                    {
                                        foreach (DataRow row in dtWorldPassGroups.Rows)
                                        {
                                            if (Convert.ToInt32(row["pass_group_id"]) == KanevaGlobals.AccessPassGroupID)
                                            {
                                                alreadyAP = true;
                                                break;
                                            }
                                        }
                                    }

                                    bool passUpdateOk = true;
                                    if (isAP == 1 && !alreadyAP)
                                    {
                                        passUpdateOk = GetCommunityFacade.AddCommunityPass(m_userId, isAdmin, community.CommunityId, community.CommunityTypeId, KanevaGlobals.AccessPassGroupID);
                                    }
                                    else if (isAP == 0 && alreadyAP)
                                    {
                                        passUpdateOk = GetCommunityFacade.DeleteCommunityPass(m_userId, isAdmin, community.CommunityId, community.CommunityTypeId, KanevaGlobals.AccessPassGroupID);
                                    }

                                    // If this failed, assume ownership issues.
                                    if (!passUpdateOk)
                                        ret = -2;
                                }
                            }
                        }

                        if (ret == -1)
                        {
                            responseStr = "<Result>\r\n  <ReturnCode>-2</ReturnCode>\r\n  <ResultDescription>Access rights undetermined. Unable to identify World.</ResultDescription>\r\n</Result>";
                        }
                        else if (ret == -2)
                        {
                            responseStr = "<Result>\r\n  <ReturnCode>-3</ReturnCode>\r\n  <ResultDescription>User does not control World.</ResultDescription>\r\n</Result>";
                        }
                        else if (ret == -3)
                        {
                            responseStr = "<Result>\r\n  <ReturnCode>-3</ReturnCode>\r\n  <ResultDescription>Illegal access type specified for World.</ResultDescription>\r\n</Result>";
                        }
                        else
                        {
                            responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
                        }

                        Response.Write(responseStr);
                    }
                    else
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action request unknown</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
                catch (System.OverflowException)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Numeric Parameter Overflow</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }
                catch (System.FormatException)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Bad Numeric Parameter</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
