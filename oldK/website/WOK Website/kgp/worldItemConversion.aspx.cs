///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Web;
using System.Web.UI;
using System.Data;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class worldItemConversion : KgpBasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            int numToConvert = 0;
            int dailyTotal = 0;
            int amtAvailableToConvert = 0;
            string errorStr = "";
            string currencyType = Currency.CurrencyType.REWARDS;
            bool conversionSucceeded = false;
            MetaGameItem metaGameItem = null;

            if (CheckUserId (false))
            {
                // Get amount parameter
                if (Request.Params["amt"] != null)
                {
                    try
                    {
                        if (Convert.ToInt32 (Request.Params["amt"]) > 0)
                        {
                            numToConvert = Convert.ToInt32 (Request.Params["amt"]);
                        }
                    }
                    catch
                    {
                        errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid amount</ResultDescription>\r\n</Result>";
                        Response.Write (errorStr);
                        return;
                    }
                }

                // Grab the MetaGameItem to convert.
                metaGameItem = GetGameFacade.GetMetaGameItem("World Coin");
                if (string.IsNullOrWhiteSpace(metaGameItem.ItemName))
                {
                    errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid meta game item</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                // If an amount was sent in, then do conversion, else we just return daily available amount
                if (numToConvert > 0)
                {
                    conversionSucceeded = GetUserFacade.ConvertMetaGameItemToCurrency(m_userId, metaGameItem, currencyType, numToConvert, out dailyTotal, out amtAvailableToConvert);

                    if (!conversionSucceeded)
                    {
                        if (dailyTotal > 0 && amtAvailableToConvert == 0)
                        {
                            m_logger.Warn("User " + m_userId + " tried to convert more coins than avaialable. Available=" +
                                amtAvailableToConvert.ToString() + ", AmountToConvert=" + numToConvert);
                        }

                        errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>conversion failed</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }
                }
                else
                {
                    dailyTotal = GetUserFacade.GetUserDailyTotalMGIConversionAmount(m_userId, metaGameItem.ItemType, currencyType);
                    amtAvailableToConvert = metaGameItem.GetAmountAvailableToConvert(currencyType, dailyTotal);
                }

                DataSet ds = new DataSet ();
                ds.DataSetName = "Result";

                XmlDocument doc = new XmlDocument ();
                doc.LoadXml (ds.GetXml ());
                XmlNode root = doc.DocumentElement;

                XmlElement elem = doc.CreateElement ("DailyConversionsTotal");
                elem.InnerText = dailyTotal.ToString ();

                root.InsertBefore (elem, root.FirstChild);

                elem = doc.CreateElement ("DailyConversionsRemaining");
                elem.InnerText = amtAvailableToConvert.ToString ();

                root.InsertBefore (elem, root.FirstChild);

                elem = doc.CreateElement ("ReturnDescription");
                elem.InnerText = "success";

                root.InsertBefore (elem, root.FirstChild);

                elem = doc.CreateElement ("ReturnCode");
                elem.InnerText = "0";

                root.InsertBefore (elem, root.FirstChild);

                Response.Write (root.OuterXml);

            }


        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

    }
}