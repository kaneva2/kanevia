///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for Fame.
    /// </summary>
    public class Notify : KgpBasePage
    {
		private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private void Page_Load(object sender, System.EventArgs e)
        {
            string kanevaDb = KanevaGlobals.DbNameKaneva;

            if (IsPostBack)
            {
                return;
            }

            if (CheckUserId(true)) // true = can be logged on to web also for this request
            {
                if (Request.Params["action"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                string actionreq = Request.Params["action"];
                FameFacade fameFacade = new FameFacade();


                if (actionreq.Equals("TravelCompleted"))
                {
                    if (Request.Params["gameId"] == null && (Request.Params["zoneInstanceId"] == null || Request.Params["zoneType"] == null))
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>World info not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int gameId = Int32.Parse(Request.Params["gameId"] ?? "0");
                    int zoneInstanceId = Int32.Parse(Request.Params["zoneInstanceId"] ?? "0");
                    int zoneType = Int32.Parse(Request.Params["zoneType"] ?? "0");

                    int communityId = 0;
                    if (gameId > 0 && (gameId != 3296 && gameId != 3298 && gameId != 5316 && gameId != 5310))
                    {
                        communityId = GetCommunityFacade.GetCommunityIdFromGameId(gameId);
                    }
                    else if(zoneInstanceId > 0 && zoneType == Constants.BROADBAND_ZONE)
                    {
                        communityId = zoneInstanceId;
                    }
                    else if (zoneInstanceId > 0 && zoneType == Constants.APARTMENT_ZONE)
                    {
                        Community aptComm = GetCommunityFacade.GetApartmentCommunity(zoneInstanceId);
                        communityId = aptComm.CommunityId;
                    }

                    if (communityId > 0)
                    {
                        // Reward for going to an event that you DO NOT own
                        EventFacade eventFacade = new EventFacade();
                        if (eventFacade.DoesCommunityHaveActiveEvent(communityId))
                        {
                            PagedList<Event> worldEvents = eventFacade.GetWorldEvents(communityId, (int)Event.eSTART_TIME_FILTER.NOW, string.Empty, 1, Int32.MaxValue);

                            // If we find an event happening in the world that is not owned by the user, give the rewards
                            foreach (Event evt in worldEvents)
                            {
                                if (evt.UserId != m_userId)
                                {
                                    eventFacade.RewardUserForEvent(m_userId, evt.EventId);
                                    eventFacade.UpdateEventInviteeAttendance(evt.EventId, m_userId);
                                    break;
                                }
                            }
                        }
                    }

                    string response = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Success</ResultDescription>\r\n</Result>";
                    Response.Write(response);
                    return;
                }

                if (actionreq.Equals("ReportFameEventNotified"))
                {
                    if (Request.Params["notificationId"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>notificationId not specified</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    int notificationId = Int32.Parse(Request.Params["notificationId"].ToString());

                    UsersUtility.UpdateNotifcation(m_userId, notificationId);

                    string response = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Success</ResultDescription>\r\n</Result>";
                    Response.Write(response);
                    return;
                }
                else if (actionreq.Equals("GetAllNewFameEvents"))
                {
                    DataSet dsAch = new DataSet();
                    dsAch.DataSetName = "Achievements";

                    DataSet dsPackets = new DataSet();
                    dsPackets.DataSetName = "Packets";

                    DataSet dsLevels = new DataSet();
                    dsLevels.DataSetName = "Levels";

                    DataTable dtAchievements = UsersUtility.GetAllNewFameEventsAchivements (m_userId);
                    dtAchievements.TableName = "Achievement";
                    dsAch.Tables.Add(dtAchievements);

                    DataTable dtPackets = UsersUtility.GetAllNewFameEventsPacket(m_userId);
                    dtPackets.TableName = "Packet";
                    dsPackets.Tables.Add(dtPackets);

                    DataTable dtLevels = UsersUtility.GetAllNewFameEventsLevel (m_userId);
                    dtLevels.TableName = "Level";
                    dsLevels.Tables.Add(dtLevels);

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml("<NewFameEvent>" + dsAch.GetXml() + dsPackets.GetXml() + dsLevels.GetXml()  + "</NewFameEvent>");
                    XmlNode root = doc.DocumentElement;

                    XmlElement elem = doc.CreateElement("NumberAchievements");
                    elem.InnerText = dtAchievements.Rows.Count.ToString ();
                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("NumberPackets");
                    elem.InnerText = dtPackets.Rows.Count.ToString();
                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("NumberLevels");
                    elem.InnerText = dtLevels.Rows.Count.ToString();
                    root.InsertBefore(elem, root.FirstChild);



                    elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = "success";

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = "0";

                    root.InsertBefore(elem, root.FirstChild);

					// Broadcast level up notifications to players in the same zone
					if (dtLevels.Rows.Count > 0)
					{
						sendLevelUpNotifications(fameFacade, dtLevels);
					}

					// Broadcast achievement notifications to players in the same zone
					if (dtAchievements.Rows.Count > 0)
					{
						sendAchievementNotifications(fameFacade, dtAchievements);
					}

                    Response.Write(root.OuterXml);
                }
              
            }

        }

		private void sendLevelUpNotifications( FameFacade fameFacade, DataTable dtLevels )
		{
			Dictionary<int, int> levelUps = new Dictionary<int,int>();
			for( int rowIdx = 0; rowIdx < dtLevels.Rows.Count; rowIdx ++ )
			{
				try
				{
					int fameTypeId = Convert.ToInt32(dtLevels.Rows[rowIdx]["fame_type_id"]);
					int levelNumber = Convert.ToInt32(dtLevels.Rows[rowIdx]["level_number"]);

					if( !levelUps.ContainsKey( fameTypeId ) || levelUps[fameTypeId]<levelNumber )
					{
						levelUps[fameTypeId] = levelNumber;
					}
				}
				catch (Exception e)
				{
					m_logger.Error("Error parsing level up record for notification", e);
				}
			}

			foreach( var pair in levelUps )
			{
				fameFacade.sendLevelNotificationToPlayerZone(KanevaWebGlobals.CurrentUser, pair.Key, pair.Value);
			}
		}

		private void sendAchievementNotifications(FameFacade fameFacade, DataTable dtAchievements)
		{
			for (int rowIdx = 0; rowIdx < dtAchievements.Rows.Count; rowIdx++)
			{
				try
				{
					int achievementId = Convert.ToInt32(dtAchievements.Rows[rowIdx]["achievement_id"]);
					fameFacade.sendAchievementNotificationToPlayerZone(KanevaWebGlobals.CurrentUser, achievementId);
				}
				catch (Exception e)
				{
					m_logger.Error("Error parsing achievement record for notification", e);
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
