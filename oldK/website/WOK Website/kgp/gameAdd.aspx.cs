///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public partial class gameAdd : KgpBasePage
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string name = null;
        private string description = "";
        private int type = 0;
        private string passWord = null;
        private string userEmail = null;
        private bool canBeLoggedIn = true;
        private int gameId = 0;
        private eGAME_ACCESS gameAccessId = eGAME_ACCESS.PUBLIC;

        string errorStr = null;
        XmlNode root = null;

        const string SUCCESS = "0";
        const string USER_NOT_FOUND = "1";
        const string COMPANY_NOT_CREATED = "2";
        const string ROLE_NOT_FOUND = "3";
        const string USER_NOT_VALIDATED = "4";
        const string ADD_TO_COMPANY_FAIL = "5";
        const string GAME_ADD_FAIL = "6";
        const string MISSING_PARAMETERS = "7";
        const string GENERIC_FAILURE = "8";

        const int GAME_RATING_MATURE = 1;
        const int GAME_STAR = 1;

        #endregion

        #region Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            //get the parameters first
            GetRequestParams();

            if (errorStr == null)
            {
                //check to see if required parameters have been provided
                if ((name != null) && (name != "") && (type > 0))
                {
                    //validate user and proceed with processing if successful
                    if (ValidateUser (canBeLoggedIn, userEmail, passWord, gameId))
                    {
                        GameDeveloper gameDeveloper = new GameDeveloper ();
                        DevelopmentCompany company = new DevelopmentCompany ();
                        User user = new User ();

                        //get the user's game developer information
                        //GetGameDeveloperByEmail may throw an exception so handle it
                        try
                        {
                            gameDeveloper = GetGameDeveloperFacade.GetGameDeveloperByEmail (userEmail);
                        }
                        catch (Exception) { }

                        //if user id is zero or less from above error or just not one found assume
                        //user is not already registered on Developer and auto register them
                        if (gameDeveloper.UserId <= 0)
                        {
                            //get the user id 
                            user = GetUserFacade.GetUserByEmail (userEmail);

                            //if the user was found proceed, otherwise report an error
                            if (user.UserId > 0)
                            {
                                //sets the errorStr if there are any errors
                                AutoCreateCompany (user.UserId, ref gameDeveloper);
                            }
                            else
                            {
                                errorStr = "<Result><ReturnCode>" + USER_NOT_FOUND + "</ReturnCode><ReturnDescription>The user could not be found.</ReturnDescription>\r\n</Result>";
                                m_logger.Error ("Error In gameAdd.aspx: unable to find user (object) with provided email");
                            }
                        }

                        //if no errors continue
                        if (errorStr == null)
                        {
                            //get the game developer's company
                            try
                            {
                                company = GetDevelopmentCompanyFacade.GetDevelopmentCompany (gameDeveloper.CompanyId);
                            }
                            catch (Exception) { }

                            //check to see if the user has a company. If not create one.
                            //this might not be necessary can not think of a situation where you have no
                            //company but you are a game developer - leaving this for now
                            if (company.CompanyId <= 0)
                            {
                                company.CompanyId = AutoCreateCompany (user.UserId, ref gameDeveloper);
                            }

                            //create the new game
                            gameId = CreateNewGame (gameDeveloper, company);

                            //return the results of the effort
                            if ((gameId > 0) && (errorStr == null))
                            {
                                Game game = GetGameFacade.GetGameByGameId(gameId);
                                GameLicense license = GetGameFacade.GetGameLicenseByGameId(gameId);
                                string consumerKey = game.GameName.Replace(" ", "");
                                string consumerSecret = Guid.NewGuid().ToString().Replace("-", "K");
                                GetCommunityFacade.InsertAPIAuth(GetCommunityFacade.GetCommunityIdFromGameId(gameId), consumerKey, consumerSecret);

                                APIAuthentication apiAuth = GetGameFacade.GetAPIAuth(gameId);

                                //create new dataset
                                DataSet ds = new DataSet ();
                                ds.DataSetName = "Result";

                                //generate the XML out put
                                XmlDocument doc = new XmlDocument ();
                                doc.LoadXml (ds.GetXml ());
                                root = doc.DocumentElement;

                                XmlElement elem = doc.CreateElement ("GameId");
                                elem.InnerText = gameId.ToString ();

                                root.InsertBefore (elem, root.FirstChild);

                                elem = doc.CreateElement("ConsumerSecret");
                                elem.InnerText = apiAuth.ConsumerSecret;
                                root.InsertBefore(elem, root.FirstChild);

                                elem = doc.CreateElement("ConsumerKey");
                                elem.InnerText = apiAuth.ConsumerKey;
                                root.InsertBefore(elem, root.FirstChild);

                                elem = doc.CreateElement("GameKey");
                                elem.InnerText = license.GameKey;
                                root.InsertBefore(elem, root.FirstChild);

                                elem = doc.CreateElement("ReturnDescription");
                                elem.InnerText = "success";

                                root.InsertBefore (elem, root.FirstChild);

                                elem = doc.CreateElement ("ReturnCode");
                                elem.InnerText = SUCCESS;

                                root.InsertBefore (elem, root.FirstChild);
                            }
                        }
                    }
                    //user could not be validated
                    else
                    {
                        errorStr = "<Result><ReturnCode>" + USER_NOT_VALIDATED + "</ReturnCode><ReturnDescription>The user could not be validated.</ReturnDescription>\r\n</Result>";
                        m_logger.Error ("Error In gameAdd.aspx: call by non-validated user");
                    }
                }
                else
                {
                    errorStr = "<Result><ReturnCode>" + MISSING_PARAMETERS + "</ReturnCode><ReturnDescription>The required parameters have not been provided.</ReturnDescription>\r\n</Result>";
                    m_logger.Error ("Error In gameAdd.aspx: required parameters not provided");
                }
            }
            //return result to caller
            if ((errorStr == null) && (root != null))
            {
                Response.Write(root.OuterXml);
            }
            else
            {
                string genericError = "<Result><ReturnCode>" + GENERIC_FAILURE + "</ReturnCode><ReturnDescription>Error:Please contact administrator.</ReturnDescription>\r\n</Result>";
                Response.Write((errorStr == null ? genericError : errorStr));
            }
        }

        #endregion

        #region Helper Functions

        private int CreateNewGame(GameDeveloper developer, DevelopmentCompany company)
        {
            int gameId = 0;
            int daysLeft = 0;

            try
            {
                Game game = new Game();
                GameLicense license = new GameLicense();
                Community community = new Community();

                //set the game data
                if (type == GAME_STAR)
                {
                    game.ParentGameId = KanevaGlobals.WokGameId;
                }
                game.GameId = -1;
                game.GameAccessId = (int)gameAccessId;
                game.GameDescription = description;
                game.GameImagePath = ""; //setting to null will throw an exception
                game.GameKeyWords = name;
                game.GameModifiersId = developer.UserId;
                game.GameName = name;
                game.GameRatingId = GAME_RATING_MATURE;
                game.GameStatusId = (int)eGAME_STATUS.ACTIVE;
                game.GameSynopsis = description;
                game.OwnerId = developer.UserId;
                game.GameEncryptionKey = KanevaGlobals.GenerateUniqueString(16);

                //set the game license data
                //temporarily hard coded to be professional for closed beta
                //license.LicenseSubscriptionId = Convert.ToInt32(ddlLicenseTypes.SelectedValue);
                license.LicenseSubscriptionId = 3;
                license.GameId = gameId;
                //moved in to stored proc
                //license.GameKey = developer.UserId + "U" + gameId + "G" + KanevaGlobals.GenerateUniqueString(15);
                license.GameKey = KanevaGlobals.GenerateUniqueString(15);
                license.ModifierId = developer.UserId;
                license.LicenseStatusId = 1; // DeveloperCommonFunctions.DEFAULT_LICENSE_STATUS_ID;
                daysLeft = KanevaWebGlobals.DaysLeftGameLicense;

                //set the community data
                community.StatusId = (int)CommunityStatus.ACTIVE;
                community.NameNoSpaces = name.Replace(" ", "");
                community.Name = name;
                community.Keywords = name;
                community.IsPublic = "Y";
                community.AllowPublishing = 0;
                community.Description = description;
                community.IsAdult = "N";
                community.Over21Required = false;
                community.IsPersonal = 0; //0 is false
                community.CommunityTypeId = (int)CommunityType.APP_3D;
                community.Email = company.ContactsEmail;
                community.CreatorId = developer.UserId;

                //catch error so that the add may proceed
                try
                {
                    community.CreatorUsername = GetUserFacade.GetUserName(developer.UserId);
                }
                catch (Exception ex)
                {
                    m_logger.Error("Error In gameAdd.aspx: unable to retrieve username.", ex);
                }

				// check if there is a commuinity with the same name
				if (GetCommunityFacade.GetCommunityIdFromName(name, false) > 0 )
				{
					errorStr = "<Result><ReturnCode>" + GAME_ADD_FAIL + "</ReturnCode><ReturnDescription>Name in use, unable to create new game.</ReturnDescription>\r\n</Result>";
					m_logger.Error("Error In gameAdd.aspx: name in use, unable to create new game.");
					return gameId;
				}

                //create the new game and related data
                gameId = GetGameFacade.TransactionalGameCreation(game, license, community, daysLeft);
                if (gameId <= 0)
                {
                    errorStr = "<Result><ReturnCode>" + GAME_ADD_FAIL + "</ReturnCode><ReturnDescription>Unable to create new game.</ReturnDescription>\r\n</Result>";
                    m_logger.Error("Error In gameAdd.aspx: unable to create new game.");
                }

                // Mark it as incubatorHosted?
                if (Request["incubatorHosted"] != null)
                {
                    if (Request["incubatorHosted"].ToString().Equals("T"))
                    {
                        // Set flag to 1
                        string sql = "UPDATE developer.games SET incubator_hosted = 1 WHERE game_id = @game_id";
                        Hashtable parameters = new Hashtable();
                        parameters.Add("@game_id", gameId);
                        KanevaGlobals.GetDatabaseUtilityDeveloper().ExecuteNonQuery (sql, parameters);

                    }
                }

            }
            catch (Exception ex) 
            {
                errorStr = "<Result><ReturnCode>" + GAME_ADD_FAIL + "</ReturnCode><ReturnDescription>Unable to create new game.</ReturnDescription>\r\n</Result>";
                m_logger.Error("Error In gameAdd.aspx: unable to create new game.",ex);
            }

            return gameId;
        }


        private int AutoCreateCompany(int userId, ref GameDeveloper gameDeveloper)
        {
            //create the default company for the new user
            int companyId = GetDevelopmentCompanyFacade.CreateDefaultCompanyForUser(userId);

            //if the company was successfully created add the game developer to the company, otherwise report an error
            if (companyId > 0)
            {
                //get the administrators role id - this potentional can throw exception
                SiteRole role = new SiteRole();
                try
                {
                    role = GetSiteSecurityFacade.GetCompanyRole(companyId, GameDeveloper.ADMIN_ROLEID);
                }
                catch (Exception) { }

                if (role.RoleId > 0)
                {
                    //set the values for the game developer
                    gameDeveloper.CompanyId = companyId;
                    gameDeveloper.DeveloperRoleId = role.SiteRoleId;
                    gameDeveloper.UserId = userId;

                    // add the game developer to the company
                    if (GetGameDeveloperFacade.InsertGameDeveloper(gameDeveloper) <= 0)
                    {
                        errorStr = "<Result><ReturnCode>" + ADD_TO_COMPANY_FAIL + "</ReturnCode><ReturnDescription>Unable to add new user to the company.</ReturnDescription>\r\n</Result>";
                        m_logger.Error("Error In gameAdd.aspx: unable to add new user to the company.");
                    }
                }
                else
                {
                    errorStr = "<Result><ReturnCode>" + ROLE_NOT_FOUND + "</ReturnCode><ReturnDescription>Unable to determine new company's admin role.</ReturnDescription>\r\n</Result>";
                    m_logger.Error("Error In gameAdd.aspx: unable to determine new company's admin role.");
                }
            }
            else
            {
                errorStr = "<Result><ReturnCode>" + MISSING_PARAMETERS + "</ReturnCode><ReturnDescription>Company creation for the new developer has failed.</ReturnDescription>\r\n</Result>";
                m_logger.Error("Error In gameAdd.aspx: unable to create the new company for the new user");
            }

            return companyId;
        }

        private void GetRequestParams()
        {
            //check to see if there is a name for the game (required)
            try
            {
                name = Server.UrlDecode(Request["name"].ToString());

                if (KanevaWebGlobals.isTextRestricted (name, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    errorStr = "<Result><ReturnCode>" + MISSING_PARAMETERS + "</ReturnCode><ReturnDescription>Name contains restricted text.</ReturnDescription>\r\n</Result>";
                    m_logger.Error ("Error In gameAdd.aspx: name includes restricted text."); 
                    return;
                }
                else if (KanevaWebGlobals.ContainsInjectScripts (name))
                {
                    errorStr = "<Result><ReturnCode>" + MISSING_PARAMETERS + "</ReturnCode><ReturnDescription>Name contains scripting.</ReturnDescription>\r\n</Result>";
                    m_logger.Error ("Error In gameAdd.aspx: name contains scripting text."); 
                    return;
                }
                else if (!Regex.IsMatch (name, @"^[a-zA-Z0-9\s_]{4,50}$")) // only allow letters, numbers, spaces, and underline. Must be 4-50 characters in length
                {
                    errorStr = "<Result><ReturnCode>" + MISSING_PARAMETERS + "</ReturnCode><ReturnDescription>" + Constants.VALIDATION_REGEX_GAME_NAME_ERROR_MESSAGE + "</ReturnDescription>\r\n</Result>";
                    m_logger.Error ("Error In gameAdd.aspx: name contains invalid characters.");
                    return;
                }
            }
            catch (Exception)
            {
            }
            //check for game type (required)
            try
            {
                type = Convert.ToInt32(Request["type"].ToString());
            }
            catch (Exception)
            {
            }
            //check to see if there is a description for the game (optional)
            try
            {
                description = Server.UrlDecode(Request["desc"].ToString());

                if (KanevaWebGlobals.isTextRestricted (description, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    errorStr = "<Result><ReturnCode>" + MISSING_PARAMETERS + "</ReturnCode><ReturnDescription>Description contains restricted text.</ReturnDescription>\r\n</Result>";
                    m_logger.Error ("Error In gameAdd.aspx: description includes restricted text.");
                    return;
                }
                else if (KanevaWebGlobals.ContainsInjectScripts (description))
                {
                    errorStr = "<Result><ReturnCode>" + MISSING_PARAMETERS + "</ReturnCode><ReturnDescription>Description contains scripting.</ReturnDescription>\r\n</Result>";
                    m_logger.Error ("Error In gameAdd.aspx: description contains scripting text.");
                    return;
                }
            }
            catch (Exception)
            {
            }
            
            //check to see if a username and password was provided. You must have both,
            //but providing them at all is optional.
            try
            {
                userEmail = Request["user"].ToString();
                passWord = Request["pw"].ToString();
            }
            catch (Exception)
            {
            }

            //game access
            if (Request["access"]!=null)
            {
                if (!Enum.TryParse(Request["access"], out gameAccessId) || !Enum.IsDefined(typeof(eGAME_ACCESS), gameAccessId) || gameAccessId == (int)eGAME_ACCESS.NOT_FOUND)
                {
                    errorStr = "<Result><ReturnCode>" + MISSING_PARAMETERS + "</ReturnCode><ReturnDescription>Invalid game access ID</ReturnDescription>\r\n</Result>";
                    m_logger.Error("Error In gameAdd.aspx: invalid game access ID parameter.");
                    return;
                }
            }
        }

        
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
