///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Xml;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;
using System.Text;

namespace KlausEnt.KEP.Kaneva.kgp
{
    /// <summary>
    /// Summary description for Fame.
    /// </summary>
    public class _scriptGameItems : KgpBasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(true))
            {
                // Check action
                if (string.IsNullOrWhiteSpace(Request.Params["action"]))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                // Process action
                switch (Request.Params["action"])
                {
                    case "getGlobalConfig":
                        ProcessGetGlobalConfig();
                        break;
                    case "getTemplateVendorList":
                        ProcessGetTemplateVendorList();
                        break;
                    case "getTemplateQuests":
                        ProcessGetTemplateQuests();
                        break;
                    case "getTemplateCraftingItems":
                        ProcessGetTemplateCraftingItems();
                        break;
                    case "getTemplateItemSpawners":
                        ProcessGetTemplateItemSpawners();
                        break;
                    default:
                        string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid action</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        break;
                }
            }
        }

        #region Action - getGlobalConfig

        private void ProcessGetGlobalConfig()
        {
            // Check config property name
            if (string.IsNullOrWhiteSpace(Request.Params["propertyName"]))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>desired property not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            string propertyName = Request.Params["propertyName"];
            string propertyValue = GetScriptGameItemFacade.GetSGIGlobalConfigValue(propertyName);

            if (string.IsNullOrWhiteSpace(propertyValue))
            {
                string errorStr = string.Format("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>property {0} not found</ResultDescription>\r\n</Result>", propertyName);
                Response.Write(errorStr);
                return;
            }

            string successStr = string.Format("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n  <PropertyName>{0}</PropertyName>\r\n  <PropertyValue>{1}</PropertyValue>\r\n  </Result>", propertyName, propertyValue);
            Response.Write(successStr);
        }

        private void ProcessGetTemplateVendorList()
        {
            // Check config property name
            if (!IsAdministrator())
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid user</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            if (string.IsNullOrWhiteSpace(Request.Params["templateId"]))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid template id</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            StringBuilder responseBuilder = new StringBuilder("<table>");
            responseBuilder.Append("<tr>");
            responseBuilder.Append("<th>Vendor Name</th>");
            responseBuilder.Append("<th>Num Placed</th>");
            responseBuilder.Append("<th>Input Item</th>");
            responseBuilder.Append("<th>Input Item Level</th>");
            responseBuilder.Append("<th>Input Quantity</th>");
            responseBuilder.Append("<th>Output Item</th>");
            responseBuilder.Append("<th>Output Item Level</th>");
            responseBuilder.Append("<th>Output Quantity</th>");
            responseBuilder.Append("<th>Processing Time</th>");
            responseBuilder.Append("</tr>");

            int templateId = Int32.Parse(Request.Params["templateId"]);
            List<ScriptGameItem> gameItems = GetScriptGameItemFacade.GetTemplateScriptGameItemVendors(templateId);

            foreach(ScriptGameItem gameItem in gameItems)
            {
                if (gameItem.VendorTrades.Count > 0)
                {
                    foreach (SGIVendorItem trade in gameItem.VendorTrades)
                    {
                        responseBuilder.Append("<tr>");
                        responseBuilder.Append("<td>" + gameItem.Name + "</td>");
                        responseBuilder.Append("<td>" + gameItem.NumPlacedInTemplate + "</td>");
                        responseBuilder.Append("<td>" + trade.Input.Name + "</td>");
                        responseBuilder.Append("<td>" + trade.Input.Level + "</td>");
                        responseBuilder.Append("<td>" + trade.Cost +"</td>");
                        responseBuilder.Append("<td>" + trade.Output.Name +"</td>");
                        responseBuilder.Append("<td>" + trade.Output.Level + "</td>");
                        responseBuilder.Append("<td>" + trade.OutputCount +"</td>");
                        responseBuilder.Append("<td>" + trade.Time + "</td>");
                        responseBuilder.Append("</tr>");
                    }
                }
                else 
                {
                    responseBuilder.Append("<tr>");
                    responseBuilder.Append("<td>" + gameItem.Name + "</td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("</tr>");
                }
            }

            Response.Write(responseBuilder.ToString());
        }

        private void ProcessGetTemplateCraftingItems()
        {
            // Check config property name
            if (!IsAdministrator())
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid user</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            if (string.IsNullOrWhiteSpace(Request.Params["templateId"]))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid template id</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            StringBuilder responseBuilder = new StringBuilder("<table>");
            responseBuilder.Append("<tr>");
            responseBuilder.Append("<th>Crafting Item</th>");
            responseBuilder.Append("<th>Output</th>");
            responseBuilder.Append("<th>Required Item</th>");
            responseBuilder.Append("<th>Required Quantity</th>");
            responseBuilder.Append("</tr>");

            int templateId = Int32.Parse(Request.Params["templateId"]);
            List<SGICraftingItem> gameItems = GetScriptGameItemFacade.GetTemplateCraftingItems(Int32.Parse(Request.Params["templateId"]));

            foreach (SGICraftingItem gameItem in gameItems)
            {
                if (gameItem.CraftingItem != null)
                {
                    responseBuilder.Append("<tr>");
                    responseBuilder.Append("<td>" + gameItem.CraftingItem.Name + "</td>");
                    responseBuilder.Append("<td>" + (gameItem.Output ?? new ScriptGameItem()).Name + "</td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("</tr>");

                    foreach (SGICraftingInput input in gameItem.Inputs)
                    {
                        responseBuilder.Append("<tr>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td>" + (input.Item ?? new ScriptGameItem()).Name + "</td>");
                        responseBuilder.Append("<td>" + (input.Item != null ? input.Count.ToString() : "") + "</td>");
                        responseBuilder.Append("</tr>");
                    }
                }
            }

            Response.Write(responseBuilder.ToString());
        }

        private void ProcessGetTemplateQuests()
        {
            // Check config property name
            if (!IsAdministrator())
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid user</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            if (string.IsNullOrWhiteSpace(Request.Params["templateId"]))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid template id</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            StringBuilder responseBuilder = new StringBuilder("<table>");
            responseBuilder.Append("<tr>");
            responseBuilder.Append("<th>Quest Giver</th>");
            responseBuilder.Append("<th>Num Placed</th>");
            responseBuilder.Append("<th>Quest</th>");
            responseBuilder.Append("<th>Required Item</th>");
            responseBuilder.Append("<th>Required Quantity</th>");
            responseBuilder.Append("<th>Prerequisite Quest</th>");
            responseBuilder.Append("<th>Reward Item</th>");
            responseBuilder.Append("<th>Reward Quantity</th>");
            responseBuilder.Append("<th>Repeatable</th>");
            responseBuilder.Append("<th>Repeat Timer</th>");
            responseBuilder.Append("</tr>");

            int templateId = Int32.Parse(Request.Params["templateId"]);
            List<ScriptGameItem> gameItems = GetScriptGameItemFacade.GetTemplateQuestGivers(templateId);

            foreach (ScriptGameItem gameItem in gameItems)
            {
                if (gameItem.SpawnedItems.Count > 0)
                {
                    foreach (SGISpawnedItem spawnedItem in gameItem.SpawnedItems)
                    {
                        responseBuilder.Append("<tr>");
                        responseBuilder.Append("<td>" + gameItem.Name + "</td>");
                        responseBuilder.Append("<td>" + gameItem.NumPlacedInTemplate + "</td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("</tr>");

                        foreach (SGIQuest questItem in spawnedItem.SpawnedGameItem.Quests)
                        {
                            responseBuilder.Append("<tr>");
                            responseBuilder.Append("<td></td>");
                            responseBuilder.Append("<td></td>");
                            responseBuilder.Append("<td>" + (questItem.QuestGameItem ?? new ScriptGameItem()).Name + "</td>");
                            responseBuilder.Append("<td>" + (questItem.RequiredItem ?? new ScriptGameItem()).Name + "</td>");
                            responseBuilder.Append("<td>" + questItem.RequiredItemQuantity + "</td>");
                            responseBuilder.Append("<td>" + (questItem.PrerequisiteQuest ?? new ScriptGameItem()).Name + "</td>");
                            responseBuilder.Append("<td>" + (questItem.RewardItem ?? new ScriptGameItem()).Name + "</td>");
                            responseBuilder.Append("<td>" + questItem.RewardItemQuantity + "</td>");
                            responseBuilder.Append("<td>" + questItem.Repeatable + "</td>");
                            responseBuilder.Append("<td>" + ( questItem.Repeatable ? questItem.RepeatTime.ToString() : "") + "</td>");
                            responseBuilder.Append("</tr>");
                        }
                    }
                }
                else
                {
                    responseBuilder.Append("<tr>");
                    responseBuilder.Append("<td>" + gameItem.Name + "</td>");
                    responseBuilder.Append("<td>" + gameItem.NumPlacedInTemplate + "</td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("</tr>");

                    foreach (SGIQuest questItem in gameItem.Quests)
                    {
                        responseBuilder.Append("<tr>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td>" + (questItem.QuestGameItem ?? new ScriptGameItem()).Name + "</td>");
                        responseBuilder.Append("<td>" + (questItem.RequiredItem ?? new ScriptGameItem()).Name + "</td>");
                        responseBuilder.Append("<td>" + questItem.RequiredItemQuantity + "</td>");
                        responseBuilder.Append("<td>" + (questItem.PrerequisiteQuest ?? new ScriptGameItem()).Name + "</td>");
                        responseBuilder.Append("<td>" + (questItem.RewardItem ?? new ScriptGameItem()).Name + "</td>");
                        responseBuilder.Append("<td>" + questItem.RewardItemQuantity + "</td>");
                        responseBuilder.Append("<td>" + questItem.Repeatable + "</td>");
                        responseBuilder.Append("<td>" + (questItem.Repeatable ? questItem.RepeatTime.ToString() : "") + "</td>");
                        responseBuilder.Append("</tr>");
                    }
                }
            }

            Response.Write(responseBuilder.ToString());
        }

        private void ProcessGetTemplateItemSpawners()
        {
            // Check config property name
            if (!IsAdministrator())
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid user</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            if (string.IsNullOrWhiteSpace(Request.Params["templateId"]))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid template id</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            if (string.IsNullOrWhiteSpace(Request.Params["itemType"]))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid itemType</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            if (string.IsNullOrWhiteSpace(Request.Params["behavior"]))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid behavior</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            StringBuilder responseBuilder = new StringBuilder("<table>");
            responseBuilder.Append("<tr>");
            responseBuilder.Append("<th>Spawner Name</th>");
            responseBuilder.Append("<th>Num Spawners Placed</th>");
            responseBuilder.Append("<th>Spawned Item</th>");
            responseBuilder.Append("<th>Respawn Time</th>");
            responseBuilder.Append("<th>Max Spawns</th>");
            responseBuilder.Append("<th>Spawns Random Generic</th>");
            responseBuilder.Append("<th>Spawns Random Armor</th>");
            responseBuilder.Append("<th>Spawns Random Ammo</th>");
            responseBuilder.Append("<th>Spawns Random Consumable</th>");
            responseBuilder.Append("<th>Spawns Random Weapon</th>");
            responseBuilder.Append("<th>Specific Loot Items</th>");
            responseBuilder.Append("<th>Rarity</th>");
            responseBuilder.Append("<th>Level</th>");
            responseBuilder.Append("<th>Quantity</th>");
            responseBuilder.Append("<th>Drop Chance</th>");
            responseBuilder.Append("</tr>");

            int templateId = Int32.Parse(Request.Params["templateId"]);
            List<ScriptGameItem> gameItems = GetScriptGameItemFacade.GetTemplateSGISpawners(templateId, Request.Params["itemType"], Request.Params["behavior"]);

            foreach (ScriptGameItem gameItem in gameItems)
            {
                if (gameItem.SpawnedItems.Count > 0)
                {
                    foreach (SGISpawnedItem spawnedItem in gameItem.SpawnedItems)
                    {
                        responseBuilder.Append("<tr>");
                        responseBuilder.Append("<td>" + gameItem.Name + "</td>");
                        responseBuilder.Append("<td>" + gameItem.NumPlacedInTemplate + "</td>");
                        responseBuilder.Append("<td>" + spawnedItem.SpawnedGameItem.Name + "</td>");
                        responseBuilder.Append("<td>" + spawnedItem.RespawnTime + "</td>");
                        responseBuilder.Append("<td>" + spawnedItem.MaxSpawns + "</td>");
                        responseBuilder.Append("<td>" + spawnedItem.SpawnedGameItem.RandomLootSettings.CanSpawnGeneric + "</td>");
                        responseBuilder.Append("<td>" + spawnedItem.SpawnedGameItem.RandomLootSettings.CanSpawnArmor + "</td>");
                        responseBuilder.Append("<td>" + spawnedItem.SpawnedGameItem.RandomLootSettings.CanSpawnAmmo + "</td>");
                        responseBuilder.Append("<td>" + spawnedItem.SpawnedGameItem.RandomLootSettings.CanSpawnConsumable + "</td>");
                        responseBuilder.Append("<td>" + spawnedItem.SpawnedGameItem.RandomLootSettings.CanSpawnWeapon + "</td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("<td></td>");
                        responseBuilder.Append("</tr>");

                        foreach (SGILootItem lootItem in spawnedItem.SpawnedGameItem.LootItems)
                        {
                            responseBuilder.Append("<tr>");
                            responseBuilder.Append("<td></td>");
                            responseBuilder.Append("<td></td>");
                            responseBuilder.Append("<td></td>");
                            responseBuilder.Append("<td></td>");
                            responseBuilder.Append("<td></td>");
                            responseBuilder.Append("<td></td>");
                            responseBuilder.Append("<td></td>");
                            responseBuilder.Append("<td></td>");
                            responseBuilder.Append("<td></td>");
                            responseBuilder.Append("<td></td>");
                            responseBuilder.Append("<td>" + lootItem.Item.Name + "</td>");
                            responseBuilder.Append("<td>" + lootItem.Item.Rarity + "</td>");
                            responseBuilder.Append("<td>" + lootItem.Item.Level + "</td>");
                            responseBuilder.Append("<td>" + lootItem.QuantityRange + "</td>");
                            responseBuilder.Append("<td>" + lootItem.DropChance + "%</td>");
                            responseBuilder.Append("</tr>");
                        }
                    }
                }
                else
                {
                    responseBuilder.Append("<tr>");
                    responseBuilder.Append("<td>" + gameItem.Name + "</td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("<td></td>");
                    responseBuilder.Append("</tr>");
                }
            }

            Response.Write(responseBuilder.ToString());
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
