///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.kgp
{
    public class childgamewebcallvalues : KgpBasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckUserId(false))
            {
                GameFacade gameFacade = new GameFacade();
                DataSet ds = gameFacade.GetChildGameWebCallValues();
                ds.DataSetName = "Result";

                // setup the result in the header
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(ds.GetXml());
                XmlNode root = doc.DocumentElement;

                XmlElement elem = doc.CreateElement("ReturnDescription");
                elem.InnerText = "success";

                root.InsertBefore(elem, root.FirstChild);

                elem = doc.CreateElement("ReturnCode");
                elem.InnerText = "0";

                root.InsertBefore(elem, root.FirstChild);

                Response.Write(root.OuterXml);
            }
        }
    }
}
