///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
  /// <summary>
  /// Summary description for eventList.
  /// </summary>
  public partial class eventList : KgpBasePage 
  {
    private void Page_Load(object sender, System.EventArgs e)
    {
        // Possible querystring params:
        // start            - start record for return list
        // max              - max records to return
        // action           - AcceptedEvent, DeclineEvent, delete
        // eventId          
        // nextHours        - Events occurring within the next x hours
        // onlyPremium      - return only Premium events
        // onlyAP           - return only AP events

        //m_logger.Debug("Starting Event List");
        if (CheckUserId (false))
        {
            EventFacade eventFacade = new EventFacade();
            string actionreq = "";

            if (Request.Params["action"] != null)
            {
                actionreq = Request.Params["action"];
            }

            int userId = 0;
            userId = KanevaWebGlobals.CurrentUser.UserId;

            int eventId = 0;
            if (!actionreq.Equals("")) // eventId only needed for change of status actions
            {
                if (Request.Params["eventId"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>eventId not specified</ReturnDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }
                else
                {
                    eventId = Convert.ToInt32(Request.Params["eventId"]);
                }
            }

            // Which action do we need to perform?
            if (actionreq.Equals("AcceptEvent") || actionreq.Equals("DeclineEvent") || actionreq.Equals(""))
            {
                // Require start and max, as all actions will return event listings
                if ((Request.Params["start"] == null) || (Request.Params["max"] == null))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ReturnDescription>page number and items per page not specified</ReturnDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                Event theEvent = new Event();
                theEvent = eventFacade.GetEvent(eventId);

                if (actionreq.Equals("AcceptEvent") || actionreq.Equals("DeclineEvent"))
                {
                    int inviteStatus = (int)EventInvitee.Invite_Status.DECLINED;
                    if (actionreq.Equals("AcceptEvent"))
                    {
                        inviteStatus = (int)EventInvitee.Invite_Status.ACCEPTED;
                    }

                    eventFacade.UpdateEventInviteStatus(eventId, KanevaWebGlobals.CurrentUser.UserId, inviteStatus, theEvent.IsPremium);
                }

                // Begin processing event listing for return
                int page = Int32.Parse(Request.Params["start"].ToString());
                int items_per_page = Int32.Parse(Request.Params["max"].ToString());

                string orderby = "start_time ASC";

                int nextHours = -1;
                if (Request.Params["nextHours"] != null)
                {
                    nextHours = Int32.Parse(Request.Params["nextHours"].ToString());
                }

                bool onlyPremium = false;
                if (Request.Params["onlyPremium"] != null)
                {
                    onlyPremium = Request.Params["onlyPremium"].ToString().Equals("Y");
                }

                DataSet ds = new DataSet();
                ds.DataSetName = "Result";
                PagedDataTable pdt = new PagedDataTable();

                try
                {
                    SubscriptionFacade subscriptionFacade = new SubscriptionFacade();
                    bool bHasAPSubscription = KanevaWebGlobals.CurrentUser.HasAccessPass;
                    bool bShowOnlyAP = (ShowOnlyAP && bHasAPSubscription);

                    if (onlyPremium)
                    {
                            // This order by was used in the A/B test. Billy asked to keep it since we may add it also
                            //orderby = "start_time, number_of_members DESC, friends_invited DESC";
                            pdt = EventsUtility.GetPremuimEventsMyKaneva(KanevaWebGlobals.CurrentUser.UserId, nextHours, true, true, orderby, page, items_per_page);
                    }
                    else
                    {
                            // This order by was used in the A/B test. Billy asked to keep it since we may add it also
                            //orderby = "start_time, invite_count DESC";
                            pdt = EventsUtility.GetUsersEventsMyKaneva(KanevaWebGlobals.CurrentUser.UserId, nextHours, true, orderby, page, items_per_page);
                    }

                    // Need to decode the name before sending
                    foreach (DataRow row in pdt.Rows)
                    {
                        row.SetField("Name", Server.HtmlDecode(row["Name"].ToString()));
                        row.SetField("title", Server.HtmlDecode(row["title"].ToString()));
                    }

                    pdt.Columns.Add(new DataColumn("invite_status"));

                    // Get attending
                    EventInvitee eventInvitee;

                    for (int i = pdt.Rows.Count - 1; i > -1; i--)
                    {
                        // Add invite status
                        eventInvitee = eventFacade.GetEventInvitee(userId, Convert.ToInt32(pdt.Rows[i]["event_id"]));
                        pdt.Rows[i]["invite_status"] = eventInvitee.InviteStatus;
                    }

                    pdt.TableName = "Event";
                    ds.Tables.Add(pdt);

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(ds.GetXml());
                    XmlNode root = doc.DocumentElement;

                    int numRecords = pdt.Rows.Count;
                    XmlElement elem = doc.CreateElement("NumberRecords");
                    elem.InnerText = numRecords.ToString();

                    root.InsertBefore(elem, root.FirstChild);

                    int totalNumRecords = pdt.TotalCount;
                    elem = doc.CreateElement("TotalNumberRecords");
                    elem.InnerText = totalNumRecords.ToString();

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = "success";

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = "0";

                    root.InsertBefore(elem, root.FirstChild);

                    Response.Write(root.OuterXml);
                }
                catch (Exception exc)
                {
                    m_logger.Warn("Error getting events: " + exc);
                }
            }
            else if (actionreq.Equals("cancel"))
            {
                string responseStr = string.Empty;

                if (eventFacade.UpdateEventStatus(eventId, (int)Event.eSTATUS_TYPE.CANCELLED, KanevaWebGlobals.CurrentUser.UserId) == 1)
                {
                    responseStr = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>";
                }
                else
                {
                    responseStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>update error</ResultDescription>\r\n</Result>";
                }

                Response.Write(responseStr);
                return;
            }
        }
    }

    /// <summary>
    /// Property to show only AP Items
    /// </summary>
    /// <returns></returns>
    private bool ShowOnlyAP
    {
        get
        {
            if (Request.Params["onlyAP"] != null)
            {
                return (Request.Params["onlyAP"].Equals("T"));
            }
            else
            {
                return false;
            }
        }
    }


    // Logger
    private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    protected const int AP_SUBSCRIPTION = 1;

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
      //
      // CODEGEN: This call is required by the ASP.NET Web Form Designer.
      //
      InitializeComponent();
      base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.Load += new System.EventHandler(this.Page_Load);
    }
    #endregion
  }
}
