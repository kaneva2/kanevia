///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data;
using System.IO;
using System.Drawing;
using System.Web.Hosting;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for MailUtilityWeb.
	/// </summary>
	public class MailUtilityWeb
	{
		public MailUtilityWeb()
		{
		}

        private static string getStandardHeader()
        {
            string header = (string)Global.Cache()["mailHeader"];
            if (header == null || header.Length.Equals(0))
            {
                using (StreamReader hdr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "header.txt"))
                {
                    header = hdr.ReadToEnd();
                    Global.Cache().Insert("mailHeader", header, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                    hdr.Close();
                }
            }
            return header;
        }

        private static string getStandardFooter()
        {
            string footer = (string)Global.Cache()["mailFooter"];
            if (footer == null || footer.Length.Equals(0))
            {
                using (StreamReader ftr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "footer.txt"))
                {
                    footer = ftr.ReadToEnd();
                    Global.Cache().Insert("mailFooter", footer, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                    ftr.Close();
                }

            }
            return footer;
        }


        /// <summary>
        /// SendRegistrationEmail
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        public static void SendRegistrationEmail(string username, string email, string activationKey)
        {
            SendRegistrationEmail(username, email, activationKey, "signup");
        }

        /// <summary>
        /// SendRegistrationEmail
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        /// <param name="returnPage"></param>
        public static void SendRegistrationEmail(string username, string email, string activationKey, string returnPage)
        {
            try
            {
                string header = getStandardHeader();
                string footer = getStandardFooter();

                string subject = (string)Global.Cache()["mailRegistrationEmailSubject"];
                string message = (string)Global.Cache()["mailRegistrationEmailBody"];

                if (subject == null || subject.Length.Equals(0))
                {
                    using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "activate-account.txt"))
                    {
                        subject = sr.ReadLine();
                        subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                        sr.ReadLine();
                        message = sr.ReadToEnd();
                        Global.Cache().Insert("mailRegistrationEmailSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                        Global.Cache().Insert("mailRegistrationEmailBody", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                        sr.Close();
                    }
                }

                message = message.Replace("#header#", header);
                message = message.Replace("#footer#", footer);
                message = message.Replace("#username#", username);
                message = message.Replace("#sitename#", KanevaGlobals.SiteName);
                message = message.Replace("#activationkey#", activationKey);
                message = message.Replace("#returnpage#", returnPage);
                message = message.Replace("#toemail#", email);

                subject = subject.Replace("#username#", username);

                SendEmail(KanevaGlobals.FromEmail, email, subject, message, true, true, 1);
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, SendRegistrationEmail, username =" + username, exc);                
            }
        }

        /// <summary>
        /// SendRegistrationEmailCoBrand
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        /// <param name="returnPage"></param>
        public static void SendRegistrationEmailCoBrand(string body_top, string body_bottom, string subject, string email, string username, string activationKey, string returnPage)
        {
            try
            {
                string header = getStandardHeader();
                string footer = getStandardFooter();

                string message = (string)Global.Cache()["mailRegistrationEmailCoBrandBody"];

                if (message == null || message.Length.Equals(0))
                {
                    using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "activate-account-cobrand.txt"))
                    {
                        sr.ReadLine();
                        sr.ReadLine();
                        message = sr.ReadToEnd();
                        Global.Cache().Insert("mailRegistrationEmailCoBrandBody", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                        sr.Close();
                    }

                }

                message = message.Replace("#header#", header);
                message = message.Replace("#footer#", footer);
                message = message.Replace("#username#", username);
                message = message.Replace("#sitename#", KanevaGlobals.SiteName);
                message = message.Replace("#activationkey#", activationKey);
                message = message.Replace("#returnpage#", returnPage);
                message = message.Replace("#toemail#", email);
                message = message.Replace("#body_top#", body_top);
                message = message.Replace("#body_bottom#", body_bottom);

                subject = subject.Replace("#username#", username);

                SendEmail(KanevaGlobals.FromEmail, email, subject, message, true, true, 1);
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, SendRegistrationEmailCoBrand, username =" + username, exc);
            }
        }

        /// <summary>
        /// SendEmailChange
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        /// <param name="returnPage"></param>
        public static void SendEmailChange(string username, string email, string activationKey, string returnPage)
        {
            try
            {
                string header = getStandardHeader();
                string footer = getStandardFooter();

                string subject = (string)Global.Cache()["mailEmailChangeSubject"];
                string message = (string)Global.Cache()["mailEmailChangeBody"];

                if (subject == null || subject.Length.Equals(0))
                {
                    using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "email-change.txt"))
                    {
                        subject = sr.ReadLine();
                        subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                        sr.ReadLine();
                        message = sr.ReadToEnd();
                        Global.Cache().Insert("mailEmailChangeSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                        Global.Cache().Insert("mailEmailChangeBody", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                        sr.Close();
                    }
                }

                message = message.Replace("#header#", header);
                message = message.Replace("#footer#", footer);
                message = message.Replace("#username#", username);
                message = message.Replace("#sitename#", KanevaGlobals.SiteName);
                message = message.Replace("#activationkey#", activationKey);
                message = message.Replace("#returnpage#", returnPage);
                message = message.Replace("#toemail#", email);

                subject = subject.Replace("#username#", username);

                SendEmail(KanevaGlobals.FromEmail, email, subject, message, true, true, 1);
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, SendEmailChange, username =" + username, exc);
            }
        }




        /// <summary>
        /// SendPasswordRequest
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        /// <param name="returnPage"></param>
        public static void SendPasswordRequest(string username, string email, string keyvalue)
        {
            try
            {
                string header = getStandardHeader();
                string footer = getStandardFooter();

                string subject = (string)Global.Cache()["mailPasswordRequestSubject"];
                string message = (string)Global.Cache()["mailPasswordRequest"];

                if (message == null || message.Length.Equals(0))
                {
                    using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "forgot-password.txt"))
                    {
                        subject = sr.ReadLine();
                        subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                        sr.ReadLine();
                        message = sr.ReadToEnd();
                        Global.Cache().Insert("mailPasswordRequestSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                        Global.Cache().Insert("mailPasswordRequest", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                        sr.Close();
                    }


                }

                message = message.Replace("#header#", header);
                message = message.Replace("#footer#", footer);
                message = message.Replace("#username#", username);
                message = message.Replace("#sitename#", KanevaGlobals.SiteName);
                message = message.Replace("#key_value#", keyvalue);
                message = message.Replace("#toemail#", email);

                SendEmail(KanevaGlobals.FromEmail, email, subject, message, true, true, 1);
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, username =" + username, exc);
            }
        }

        /// <summary>
        /// SendUsernameResetEmail
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        /// <param name="returnPage"></param>
        public static void SendUsernameResetEmail (string username, string password, string email)
        {
            try
            {
                string header = getStandardHeader ();
                string footer = getStandardFooter ();

                string subject = (string) Global.Cache ()["mailUsernameResetSubject"];
                string message = (string) Global.Cache ()["mailUsernameReset"];

                if (message == null || message.Length.Equals (0))
                {
                    using (StreamReader sr = File.OpenText (System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString () + "username-reset.txt"))
                    {
                        subject = sr.ReadLine ();
                        subject = subject.Substring (subject.IndexOf (":") + 1).Trim ();
                        sr.ReadLine ();
                        message = sr.ReadToEnd ();
                        Global.Cache ().Insert ("mailUsernameResetSubject", subject, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
                        Global.Cache ().Insert ("mailUsernameReset", message, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
                        sr.Close ();
                    }
                }

                message = message.Replace ("#header#", header);
                message = message.Replace ("#footer#", footer);
                message = message.Replace ("#temp_username#", username);
                message = message.Replace ("#temp_password#", password);
                message = message.Replace ("#sitename#", KanevaGlobals.SiteName);

                SendEmail (KanevaGlobals.FromSupportEmail, email, subject, message, true, true, 1);
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error sending system email, username =" + username, exc);
            }
        }

        /// <summary>
        /// SendNewFriendNotification
        /// </summary>
        /// <param name="email"></param>
        /// <param name="drUser"></param>
        /// <param name="userId"></param>
        public static void SendNewFriendNotification (int userIdTo, int newFriendUserId)   
        {
            UserFacade userFacade = new UserFacade ();
            User userNewFriend = userFacade.GetUser (newFriendUserId);
            User userTo = userFacade.GetUser (userIdTo);

            string newFriendUserlink = KanevaGlobals.GetPersonalChannelUrl (userNewFriend.NameNoSpaces);
            string newFriendImg = UsersUtility.GetProfileImageURL (userNewFriend.ThumbnailSmallPath, "sm", userNewFriend.Gender);

            string header = getStandardHeader ();
            string footer = getStandardFooter ();

            string subject = (string) Global.Cache ()["mailNewFriendSubject"];
            string message = (string) Global.Cache ()["mailNewFriend"];

            if (message == null || message.Length.Equals (0))
            {
                using (StreamReader sr = File.OpenText (System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString () + "new-friend.txt"))
                {
                    subject = sr.ReadLine ();
                    subject = subject.Substring (subject.IndexOf (":") + 1).Trim ();
                    sr.ReadLine ();
                    message = sr.ReadToEnd ();
                    Global.Cache ().Insert ("mailNewFriendSubject", subject, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
                    Global.Cache ().Insert ("mailNewFriend", message, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
                    sr.Close ();
                }
            }

            subject = subject.Replace ("#firstname_lastname#", userNewFriend.FirstName + " " + userNewFriend.LastName);

            message = message.Replace ("#header#", header);
            message = message.Replace ("#to_firstname#", userTo.FirstName);
            message = message.Replace ("#firstname_lastname#", userNewFriend.FirstName + " " + userNewFriend.LastName);
            message = message.Replace ("#firstname#", userNewFriend.FirstName);
            message = message.Replace ("#profilepic#", newFriendImg);
            message = message.Replace ("#profilelink#", newFriendUserlink);
            message = message.Replace ("#displayname#", userNewFriend.DisplayName);
            message = message.Replace ("#meet3dlink#", KanevaGlobals.SiteName + "/kgp/playwok.aspx?goto=P" + userNewFriend.UserId.ToString () + "&ILC=MM3D&link=mmnowin");

            message = message.Replace ("#footer#", footer);
            message = message.Replace ("#toemail#", userTo.Email);
            message = message.Replace ("#sitename#", KanevaGlobals.SiteName);

            SendEmail (KanevaGlobals.FromEmail, userTo.Email, subject, message, true,false, 2);
        }


        /// <summary>
        /// SendCommentEmail
        /// </summary>
        /// <param name="email"></param>
        /// <param name="drUser"></param>
        /// <param name="userId"></param>
        public static void SendCommentEmail(string email, User userTo, int userIdFrom, string commentType)
        {
            UserFacade userFacade = new UserFacade();
            User userFrom = userFacade.GetUser(userIdFrom);

            string userlink = KanevaGlobals.GetPersonalChannelUrl(userFrom.NameNoSpaces);
            string tolink = KanevaGlobals.GetPersonalChannelUrl(userTo.NameNoSpaces);
            string username = userFrom.Username;

            string header = getStandardHeader();
            string footer = getStandardFooter();

            string subject = (string)Global.Cache()["mailCommentSubject"];
            string message = (string)Global.Cache()["mailComment"];

            if (message == null || message.Length.Equals(0))
            {
                using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "new-comment.txt"))
                {
                    subject = sr.ReadLine();
                    subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                    sr.ReadLine();
                    message = sr.ReadToEnd();
                    Global.Cache().Insert("mailCommentSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                    Global.Cache().Insert("mailComment", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                    sr.Close();
                }
            }

            subject = subject.Replace("#username#", username);
            message = message.Replace("#header#", header);
            message = message.Replace("#footer#", footer);
            message = message.Replace("#username#", username);
            message = message.Replace("#userlink#", userlink);
            message = message.Replace("#tolink#", tolink); // link to the recipients profile to view the message.
            message = message.Replace("#sitename#", KanevaGlobals.SiteName);
            message = message.Replace("#toemail#", email);

            subject = subject.Replace("#username#", username);

            SendEmail(KanevaGlobals.FromEmail, email, subject + " " + commentType, message, true, false, 2);
        }

        /// <summary>
        /// ShareAssetWithFriend
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        public static void ShareAssetWithFriend(int userId, string recipient, int assetId, string sender, string message, DataRow drAsset, string assetType, System.Web.UI.Page page)
        {
            string senderURL = sender;
            string assetLink = "http://" + KanevaGlobals.SiteName + "/asset/" + assetId + ".storeItem";

            if (userId > 0)
            {
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(userId);

                sender = user.Username;
                senderURL = "member <a target='_resource' href='" + KanevaGlobals.GetPersonalChannelUrl(user.NameNoSpaces) + "' >" + sender + "</a>";
            }

            string subject = sender + " has sent you a Kaneva " + assetType;
            string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>";
            strMessage += "<br> Kaneva " + senderURL + " has shared the " + assetType + " <a target='_resource' href='" + assetLink + "'>" + drAsset["name"].ToString() + "</a> with you.";

            strMessage += "<br><br>" + assetType + " link: ";
            strMessage += "<br> <a target='_resource' href='" + assetLink + "'>" + assetLink + "</a>";

            // Attach optional personal message?
            if (message.Trim().Length > 0)
            {
                strMessage += "<br>\r\nPersonal message from " + sender + ":";
                strMessage += "<br><br>" + message;
            }

            strMessage += "<br><br>";
            strMessage += "<br><span style='font-size:11px;'>This email was sent by Kaneva, 270 Carpenter Drive, Sandy Springs, Georgia, 30328 USA.</span>";

            strMessage += "</body></html>";
            SendEmail(KanevaGlobals.FromEmail, recipient, subject, strMessage, true, true, 2);
        }

        /// <summary>
        /// ShareChannelWithFriend
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        public static void ShareChannelWithFriend(int userId, string recipient, int channelId, string sender, string additionalmessage, System.Web.UI.Page page)
        {
            string header = getStandardHeader();
            string footer = getStandardFooter();

            string subject = (string)Global.Cache()["mailShareChannelSubject"];
            string message = (string)Global.Cache()["mailShareChannel"];

            if (message == null || message.Length.Equals(0))
            {
                using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "tell-others-community.txt"))
                {
                    subject = sr.ReadLine();
                    subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                    sr.ReadLine();
                    message = sr.ReadToEnd();
                    Global.Cache().Insert("mailShareChannelSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                    Global.Cache().Insert("mailShareChannel", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                    sr.Close();
                }
            }

            DataRow drChannel = CommunityUtility.GetCommunity(channelId);
            string username = sender; // username of the person sending the email
            string communitylink = KanevaGlobals.GetBroadcastChannelUrl(drChannel["name_no_spaces"].ToString()); // link of the profile being shared
            string communityimage = CommunityUtility.GetBroadcastChannelImageURL(drChannel["thumbnail_small_path"].ToString(), "sm");
            string communityname = drChannel["name"].ToString();
            string description = drChannel["description"].ToString();
            string userlink = KanevaGlobals.SiteName;

            if (userId > 0)
            {
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(userId);

                username = user.Username;
                userlink = KanevaGlobals.GetPersonalChannelUrl(user.NameNoSpaces);
            }

            message = message.Replace("#header#", header);
            message = message.Replace("#footer#", footer);

            message = message.Replace("#username#", username);
            message = message.Replace("#userlink#", userlink);
            message = message.Replace("#communitylink#", communitylink);
            message = message.Replace("#communityimage#", communityimage);
            message = message.Replace("#communityname#", communityname);
            message = message.Replace("#description#", description);
            message = message.Replace("#sitename#", KanevaGlobals.SiteName);
            message = message.Replace("#additionalmessage#", additionalmessage);
            message = message.Replace("#toemail#", recipient);

            subject = subject.Replace("#username#", username);

            SendEmail(KanevaGlobals.FromEmail, recipient, subject, message, true, true, 2);
        }

        /// <summary>
        /// ShareProfileWithFriend
        /// </summary>
        public static void ShareProfileWithFriend(int userId, string recipient, int profileId, string sender, string additionalmessage, System.Web.UI.Page page)
        {
            string header = getStandardHeader();
            string footer = getStandardFooter();

            string subject = (string)Global.Cache()["mailShareProfileSubject"];
            string message = (string)Global.Cache()["mailShareProfile"];

            if (message == null || message.Length.Equals(0))
            {
                using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "tell-others-profile.txt"))
                {
                    subject = sr.ReadLine();
                    subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                    sr.ReadLine();
                    message = sr.ReadToEnd();
                    Global.Cache().Insert("mailShareProfileSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                    Global.Cache().Insert("mailShareProfile", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                    sr.Close();
                }

            }

            UserFacade userFacade = new UserFacade();
            User userChannel = userFacade.GetUser(profileId);

            string username = sender; // username of the person sending the email
            string profilelink = KanevaGlobals.GetPersonalChannelUrl(userChannel.NameNoSpaces); // link of the profile being shared
            string profileimage = UsersUtility.GetProfileImageURL(userChannel.ThumbnailSmallPath, "sm", userChannel.Gender);
            string profileusername = userChannel.Username;
            string location = userChannel.Location;
            string userlink = KanevaGlobals.SiteName;

            if (userId > 0)
            {
                User user = userFacade.GetUser(userId);

                username = user.Username;
                userlink = KanevaGlobals.GetPersonalChannelUrl(user.NameNoSpaces);
            }
          
            message = message.Replace("#header#", header);
            message = message.Replace("#footer#", footer);

            message = message.Replace("#username#", username);
            message = message.Replace("#userlink#", userlink);
            message = message.Replace("#profilelink#", profilelink);
            message = message.Replace("#profileimage#", profileimage);
            message = message.Replace("#profileusername#", profileusername);
            message = message.Replace("#location#", location); 
            message = message.Replace("#sitename#", KanevaGlobals.SiteName);
            message = message.Replace("#additionalmessage#", additionalmessage);
            message = message.Replace("#toemail#", recipient);

            subject = subject.Replace("#username#", username);

            SendEmail(KanevaGlobals.FromEmail, recipient, subject, message, true, true, 2);
        }


		/// <summary>
		/// Sends a single email. 
		/// </summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="subject"></param>
		/// <param name="body"></param>
		/// <param name="bodyFormat"></param>
		/// <param name="smtpServer"></param>
		public static void SendEmail (string from,	string to, string subject, string body, bool isHtml, bool bSendBCCToKaneva, int tier) 
		{
      MailUtility.SendEmail(from, to, subject, body, isHtml, bSendBCCToKaneva, tier);        
		}

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

	}
}
