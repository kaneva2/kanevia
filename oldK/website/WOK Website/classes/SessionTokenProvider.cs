///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using log4net;
using log4net.Config;
using WOK;

// todo: inifinite ttl
// todo: disabled (Note that this might entail a NullTokenProvider
namespace WOK.classes
{
    /// <summary>
    /// Mechanism for providing a guid or "token" to be used in Tokenized Authorization and Zoning (TAZ).  When the time comes
    /// we can extract an interface that simply defines the getToken() method but for right now this is the only implementation.
    /// It functions by using memcache to synchronize this token between all web servers while minimizing both 1) network IO 
    /// accessing memcache and 2) the potential for a cache stampede.
    /// </summary>
    public class SessionTokenProvider 
    {
        public class AcquisitionFailureException : Exception
        {
            public AcquisitionFailureException()
            {
            }

            public AcquisitionFailureException(string message)
                : base(message)
            {
            }

            public AcquisitionFailureException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }

        public class TokenInfo
        {
            public TokenInfo()
            {
                _token = null;
                _expiry = new DateTime(); // new System.DateTime();
            }

            public TokenInfo(string token, DateTime expiry)
            {
                _token = token;
                _expiry = expiry;
            }

            public TokenInfo setToken(string token)
            {
                _token = token;
                return this;
            }

            public string token()
            {
                return _token;
            }

            public TokenInfo setExpiry(DateTime expiry)
            {
                _expiry = expiry;
                return this;
            }

            public DateTime expiry()
            {
                return _expiry;
            }

            private string      _token;
            private DateTime    _expiry;
        }

        /// <summary>
        /// Default constructor for a SessionTokenProvider
        /// </summary>
        public SessionTokenProvider() 
        {
            _ttl        = new TimeSpan();      // Zero timespan indicates no rollover
            _tokenInfo  = new TokenInfo();
            _tracer     = new Queue<string>();
        }

        public override string ToString()
        {
            return _ttl.ToString();
        }

        /// <summary>
        /// Initializes the token provider.  Specifically, specifies the token's time to live 
        /// and adds a dummy entry to the local web cache.  Note. that use of the local cache
        /// may not be necessary anymore since the provider is now stored in the HttpApplication
        /// instance.
        /// </summary>
        /// <param name="ttl"></param>
        /// <returns></returns>
        public SessionTokenProvider init(TimeSpan ttl)
        {
            _logger.Debug(System.Reflection.MethodBase.GetCurrentMethod());
            _ttl = ttl;
            return this;
        }

        /// <summary>
        /// Resets the shared token stored in memcached, thus forcing the regeneration of a new key.
        /// </summary>
        public void reset()
        {
            _logger.Debug(System.Reflection.MethodBase.GetCurrentMethod());
            Kaneva.DataLayer.DataObjects.CentralCache.Remove(keySessionSemaphore); 
            _tokenInfo = new TokenInfo();
        }

        /// <summary>
        /// Acquire the current WoKWeb Session Token
        /// </summary>
        /// <returns>The current token</returns>
        public TokenInfo getToken() 
        {
            // todo: make retry count configurable.  At the very least, make it NOT a magic number
            //
            for (int n = 0; n < 3; n++)
            {
                try
                {

                    _logger.Debug(System.Reflection.MethodBase.GetCurrentMethod());
                    _tracer.Enqueue(System.Reflection.MethodBase.GetCurrentMethod().Name);

                    // If we haven't reached the expected expiration of the semaphore
                    // simply return the current value

                    // Has this been initialized yet?
                    //
                    _tracer.Enqueue("Has local token data been initialized?");
                    if (_tokenInfo.token() != null)
                    {
                        _tracer.Enqueue("Local token data has been initialized!");

                        // Here is one problem.  These two will always be equal...
                        DateTime dt = DateTime.Now;
                        _tracer.Enqueue("Has the token data expired? [" + _tokenInfo.expiry().ToString() + " > " + dt.ToString() + "]");
                        if (_tokenInfo.expiry() > dt)
                        {
                            _logger.Debug(System.Reflection.MethodBase.GetCurrentMethod() + ": Returning existing key [" + _tokenInfo.token() + "]");
                            _tracer.Enqueue(System.Reflection.MethodBase.GetCurrentMethod() + ": Returning existing key [" + _tokenInfo.token() + "]");
                            return _tokenInfo;
                        }
                        else
                        {
                            _tracer.Enqueue("Local token data is stale!");
                        }
                    }
                    else
                    {
                        _tracer.Enqueue("Local token data has NOT been initialized!");
                    }

                    _tracer.Enqueue("Updating SessionToken!");
                    updateSessionToken(_tokenInfo);
                    return _tokenInfo;
                }
                catch (AcquisitionFailureException)
                {
                    continue;
                }
            }
            throw new AcquisitionFailureException();
        }

        private SessionTokenProvider updateSessionToken(TokenInfo localToken)
        {
            _logger.Debug(System.Reflection.MethodBase.GetCurrentMethod());
            _tracer.Enqueue(System.Reflection.MethodBase.GetCurrentMethod().Name);
            DateTime dt = DateTime.Now + _ttl;

            _tracer.Enqueue("Attempting to acquire semaphore");
            if (Kaneva.DataLayer.DataObjects.CentralCache.Add(keySessionSemaphore
                                                                , dt.ToString()
                                                                , _ttl))
            {
                _tracer.Enqueue("Semaphore successfully acquired.");

                // Generate a guid.  In C# this is done as follows:
                localToken.setToken(System.Guid.NewGuid().ToString());
                localToken.setExpiry(dt);
                _tracer.Enqueue("Storing new token.");
                Kaneva.DataLayer.DataObjects.CentralCache.Store(keySessionToken
                                                                    , localToken.token()
                                                                    , new TimeSpan(0));
                _logger.Debug(System.Reflection.MethodBase.GetCurrentMethod() + " ^" + localToken.token());
                return this;
            }

            _tracer.Enqueue("Failed to acquire semaphore");
            acquireSessionToken(localToken);
            return this;
        }

        private void acquireSessionToken(TokenInfo localToken)     {
            _logger.Debug( System.Reflection.MethodBase.GetCurrentMethod());
            _tracer.Enqueue(System.Reflection.MethodBase.GetCurrentMethod().Name);

            // Attempt to get the token.  Note that currently CentralCache does not support a wait timeout
            // for this method.  It is not clear if the time out mean time to comm, or wait time if value
            // is not set.  We'll clarify here.
            //
            string sExpiry = (string)Kaneva.DataLayer.DataObjects.CentralCache.Get(keySessionSemaphore);
            if (sExpiry == null)
            {
                _tracer.Enqueue("acquireSessionToken(): Failed to get [" + keySessionSemaphore + "]");
                throw new AcquisitionFailureException();
            }

            string sessionToken = (string)Kaneva.DataLayer.DataObjects.CentralCache.Get(keySessionToken); //, _timeoutMillis ); //
            if (sessionToken == null)
            {
                _tracer.Enqueue("acquireSessionToken(): Failed to get [" + keySessionToken + "]");
                throw new AcquisitionFailureException();
            }


            DateTime predictedExpiry = DateTime.Parse(sExpiry);
            if ( sessionToken.Equals(localToken.token()) ) 
            {
                _logger.Debug( System.Reflection.MethodBase.GetCurrentMethod() + ": Latent [" + localToken.token() + "]" );
            }
            else 
            {
                localToken.setToken( sessionToken );
                localToken.setExpiry( predictedExpiry );
                _logger.Debug( System.Reflection.MethodBase.GetCurrentMethod() + " +" + localToken.token()  );
            }
        }

        public void clearTrace()
        {
            _tracer.Clear();
        }

        public Queue<String> getTrace()
        {
            return _tracer;
        }

        private static readonly ILog    _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string            LocalTokenKey          = "SessionTokenProvider";
        private const string            keySessionSemaphore    = "WoKServer.Session.Semaphore";
        private const string            keySessionToken        = "WoKServer.Session.Token";
        private TimeSpan                _ttl;
        private TokenInfo               _tokenInfo;
        private Queue<String>           _tracer;
    }
}