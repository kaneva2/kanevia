///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web.Caching;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for Cache.
	/// </summary>
	public class WebCache
	{
		public WebCache ()
		{
		}

		/// <summary>
		/// GetCountries
		/// </summary>
        public static IList<Country> GetCountries()
		{
			string cacheItemsName = "countries";

			// Load countries
            IList<Country> dtCountries = (IList<Country>) Global.Cache()[cacheItemsName];

			if (dtCountries == null)
			{
				// Get and add to cache
                UserFacade userFacade = new UserFacade();
				dtCountries = userFacade.GetCountries ();
				Global.Cache ().Insert (cacheItemsName, dtCountries, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			return dtCountries;
		}

        /// <summary>
        /// GetStates
        /// </summary>
        public static IList<State> GetStates()
        {
            string cacheItemsName = "states";

            // Load states
            IList<State> dtStates = (IList<State>)Global.Cache()[cacheItemsName];

            if (dtStates == null)
            {
                // Get and add to cache
                UserFacade userFacade = new UserFacade();
                dtStates = userFacade.GetStates();
                Global.Cache().Insert(cacheItemsName, dtStates, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtStates;
        }

        /// <summary>
        /// GetAssetTypes
        /// </summary>
        public static DataTable GetAssetTypes()
        {
            string cacheAssetTypes = "assetTypes";

            // Load asset perms
            DataTable dtAssetTypes = (DataTable)Global.Cache()[cacheAssetTypes];
            if (dtAssetTypes == null)
            {
                // Add the community list to the cache
                dtAssetTypes = StoreUtility.GetAssetTypes(); ;
                Global.Cache().Insert(cacheAssetTypes, dtAssetTypes, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtAssetTypes;
        }

        /// <summary>
        /// Get the Asset Types
        /// </summary>
        /// <returns></returns>
        public static DataView GetAssetTypes (string filter)
        {
            return new DataView (GetAssetTypes(), filter, "name", DataViewRowState.CurrentRows);
        }

        /// <summary>
        /// GetAssetPermissions
        /// </summary>
        public static DataTable GetAssetPermissions()
        {
            string cacheItemsName = "assetPerms";

            // Load asset perms
            DataTable dtAssetPerms = (DataTable)Global.Cache()[cacheItemsName];
            if (dtAssetPerms == null)
            {
                // Add the community list to the cache
                dtAssetPerms = StoreUtility.GetAssetPermissions(); ;
                Global.Cache().Insert(cacheItemsName, dtAssetPerms, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtAssetPerms;
        }

		/// <summary>
		/// GetAssetCategories
		/// </summary>
		public static DataTable GetAssetCategories (int assetTypeId)
		{
			DataTable dtAssetCategories = null;
			Cache cache = Global.Cache ();
			int cacheTime = 15;

			switch (assetTypeId)
			{
				case (int) Constants.eASSET_TYPE.ASSET:
				{
					dtAssetCategories = (DataTable) cache [cASSET_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
						cache.Insert (cASSET_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes (cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				case (int) Constants.eASSET_TYPE.GAME:
				{
					dtAssetCategories = (DataTable) cache [cGAME_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
						cache.Insert (cGAME_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes (cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				case (int) Constants.eASSET_TYPE.VIDEO:
				{
					dtAssetCategories = (DataTable) cache [cMEDIA_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
						cache.Insert (cMEDIA_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes (cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				case (int) Constants.eASSET_TYPE.MUSIC:
				{
					dtAssetCategories = (DataTable) cache [cMUSIC_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
						cache.Insert (cMUSIC_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes (cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				case (int) Constants.eASSET_TYPE.PICTURE:
				{
					dtAssetCategories = (DataTable) cache [cPHOTO_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
						cache.Insert (cPHOTO_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes (cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				case (int) Constants.eASSET_TYPE.PATTERN:
				{
					dtAssetCategories = (DataTable) cache [cPATTERN_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
						cache.Insert (cPATTERN_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes (cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				case (int) Constants.eASSET_TYPE.TV:
				{
					dtAssetCategories = (DataTable) cache [cTV_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
						cache.Insert (cTV_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes (cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				case (int) Constants.eASSET_TYPE.WIDGET:
				{
					dtAssetCategories = (DataTable) cache [cWIDGET_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
                        cache.Insert(cWIDGET_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				default:
				{
					throw new Exception ("This media type (assetTypeId) is not supported in the cache");
				}
			}			


			return dtAssetCategories;
		}

		/// <summary>
		/// GetMediaPageOMMList
		/// </summary>
		public static DataTable GetMediaPageOMMList (int pageSize, string cacheKey)
		{
			PagedDataTable dtAssets = null;
			Cache cache = Global.Cache ();

			string cacheString = "MediaPageOMMList" + cacheKey;
			
			dtAssets = (PagedDataTable) cache [cacheString];
			if (dtAssets == null)
			{
				dtAssets = StoreUtility.GetCoolAssets( (int)Constants.eASSET_TYPE.ALL,
					" fa.playlist_type = " + (int) Constants.COOL_ITEM_PLAYLIST_ID_MEDIA + " AND a.playlistable = 'Y' ", 1, pageSize);

				cache.Insert (cacheString, dtAssets, null, DateTime.Now.AddMinutes (5), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			return dtAssets;
		}
		
        /// <summary>
        /// GetRestrictedWords
        /// </summary>
        public static DataTable GetRestrictedWords()
        {
            DataTable dtRestrictedWords = (DataTable)Global.Cache()[cRESTWORDS];
            if (dtRestrictedWords == null)
            {
                // Add to the cache
                dtRestrictedWords = StoreUtility.GetRestrictedWords();
                Global.Cache().Insert(cRESTWORDS, dtRestrictedWords, null, DateTime.Now.AddMinutes(15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtRestrictedWords;
        }

        /// <summary>
        /// GetRestrictedWordsList - This method caches and gets a specified string of words based on the restricion type
        /// and the match type.  The string is generated with | (pipe) seperator which is the format used by the REGEX object
        /// for the regular expression matching.
        /// </summary>
        public static string GetRestrictedWordsList(Constants.eRESTRICTION_TYPE rest_type, Constants.eMATCH_TYPE match_type)
        {
            string cacheID = string.Empty;

            // Set the cacheID so we can see if string is already cached
            if (rest_type == Constants.eRESTRICTION_TYPE.POTTY_MOUTH)
            {
                cacheID = (match_type == Constants.eMATCH_TYPE.EXACT ? cPMEXACT : cPMANY);
            }
            else if (rest_type == Constants.eRESTRICTION_TYPE.RESERVED)
            {
                cacheID = (match_type == Constants.eMATCH_TYPE.EXACT ? cRESTEXACT : cRESTANY);
            }

            // Try to get string from cache
            string strWordList = (string)Global.Cache()[cacheID];
            if (strWordList == null)
            {
                strWordList = string.Empty;

                // Set the match type param for the query string
                string matchTypeClause = "match_type=" + (match_type == Constants.eMATCH_TYPE.EXACT ? "'exact'" : "'anywhere'");

                // Set the restriction type param for the query string
                string restTypeClause = "restriction_type_id=" + (rest_type == Constants.eRESTRICTION_TYPE.RESERVED ?
                    (int)Constants.eRESTRICTION_TYPE.RESERVED : (int)Constants.eRESTRICTION_TYPE.POTTY_MOUTH);

                // Get the word list
                DataRow[] drRestWords = WebCache.GetRestrictedWords().Select(restTypeClause + " AND " + matchTypeClause);
                if (drRestWords.Length > 0)
                {
                    // Let's build the list of words we need to search for
                    for (int i = 0; i < drRestWords.Length; i++)
                    {
                        strWordList += drRestWords[i]["word"].ToString() + "|";
                    }

                    // Remove the trailing pipe |
                    if (strWordList.EndsWith("|"))
                    {
                        strWordList = strWordList.Substring(0, strWordList.Length - 1);
                    }
                }

                // Add to the cache
                Global.Cache().Insert(cacheID, strWordList, null, DateTime.Now.AddMinutes(15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return strWordList;
        }
        


		/// <summary>
		/// GetTotalAssets
		/// </summary>
		public static double GetTotalAssets ()
		{
			if ( Global.Cache () [cTOTAL_ASSETS] == null )
			{
				// add to cache
				DataRow drMediaCounts = StoreUtility.GetTotalMediaCounts ();
				double total_assets = Convert.ToDouble (drMediaCounts ["sum_total"]);
				Global.Cache ().Insert (cTOTAL_ASSETS, total_assets, null, DateTime.Now.AddMinutes (5), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			return (double) Global.Cache () [cTOTAL_ASSETS];
		}

		/// <summary>
		/// GetTotalPersonalChannels
		/// </summary>
		public static UInt64 GetTotalPersonalChannels()
		{
			if ( Global.Cache () [cTOTAL_PERSONAL_CHANNELS] == null )
			{
				// add to cache
				UInt64 total_channels = UsersUtility.GetUsersCount ();
				Global.Cache ().Insert (cTOTAL_PERSONAL_CHANNELS, total_channels, null, DateTime.Now.AddMinutes (5), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			return (UInt64) Global.Cache () [cTOTAL_PERSONAL_CHANNELS];
		}

		/// <summary>
		/// GetTotalBroadcastChannels
		/// </summary>
		public static double GetTotalBroadcastChannels ()
		{
			if ( Global.Cache () [cTOTAL_BROADCAST_CHANNELS] == null )
			{
				double total_channels = CommunityUtility.GetChannelCount (); 
				Global.Cache ().Insert (cTOTAL_BROADCAST_CHANNELS, total_channels, null, DateTime.Now.AddMinutes (5), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			return (double) Global.Cache () [cTOTAL_BROADCAST_CHANNELS];
		}

		/// <summary>
		/// GetLeaderBoard_User2User
		/// </summary>
		public static PagedDataTable GetLeaderBoard_User2User (int NumberOfResults, int TimePeriodInDays)
		{
			PagedDataTable dtLbUser2User = null;
			Cache cache = Global.Cache ();

			string cacheString = "dtLbUser2User" + NumberOfResults + TimePeriodInDays;
			
			dtLbUser2User = (PagedDataTable) cache [cacheString];
			if (dtLbUser2User == null)
			{
				dtLbUser2User = StoreUtility.GetLeaderBoard_User2User (NumberOfResults, TimePeriodInDays, null, 1); 
				cache.Insert (cacheString, dtLbUser2User, null, DateTime.Now.AddMinutes (15), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			return dtLbUser2User;
		}

        public static IList<RaveType> GetAllRaveTypes ()
        {
            string cacheItemsName = "raveTypes";

            // Load countries
            IList<RaveType> dtRaveTypes = (IList<RaveType>) Global.Cache ()[cacheItemsName];

            if (dtRaveTypes == null)
            {
                // Get and add to cache
                RaveFacade raveFacade = new RaveFacade ();
                dtRaveTypes = raveFacade.GetRaveTypes ();
                Global.Cache ().Insert (cacheItemsName, dtRaveTypes, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtRaveTypes;
        }

        public static RaveType GetRaveType (RaveType.eRAVE_TYPE eRT)
        {
            string cacheItemsName = "raveType" + eRT.ToString ();

            // Load countries
            RaveType raveType = (RaveType) Global.Cache ()[cacheItemsName];

            if (raveType == null)
            {
                // Get and add to cache
                RaveFacade raveFacade = new RaveFacade ();
                raveType = raveFacade.GetRaveType (Convert.ToUInt32 (eRT));
                Global.Cache ().Insert (cacheItemsName, raveType, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return raveType;
        }
        
        private const string cCOMMUNITY_CATEGORIES = "commCategories";

		private const string cMEDIA_CATEGORIES = "medCats";
		private const string cASSET_CATEGORIES = "assCats";
		private const string cGAME_CATEGORIES = "gamCats";
		private const string cMUSIC_CATEGORIES = "musicCats";
		private const string cPHOTO_CATEGORIES = "photoCats";
		private const string cPATTERN_CATEGORIES = "patternCats";
		private const string cTV_CATEGORIES = "tvCats";
		private const string cWIDGET_CATEGORIES = "widgetCats";

		private const string cALL_SUB_CATEGORIES = "allSubCats";

		// Featured item cache constants
		public const string FEATURED_ITEMS = "featItems";
		public const string FEATURED_ITEMS_WATCH = "fiW";
		public const string FEATURED_ITEMS_PLAY = "fiP";
		public const string FEATURED_ITEMS_CREATE = "fiC";

        public const string cRESTWORDS = "RESTRICTED_WORDS";    // Used for DataTable of restricted words
        public const string cPMEXACT = "POTTY_MOUTH_EXACT";     // Used for Potty Mouth Exact Match word list
        public const string cPMANY = "POTTY_MOUTH_ANYWHERE";    // Used for Potty Mouth Anywhere Match word list
        public const string cRESTEXACT = "RESERVED_EXACT";      // Used for Reserved Exact Match word list
        public const string cRESTANY = "RESERVED_ANYWHERE";     // Used for Reserved Anywhere Match word list

		private const string cSMUT = "POTTY_MOUTH";

		private const string cMODULE_TITLES = "MODULE_TITLES";

		private const string cTOTAL_ASSETS = "TOTAL_ASSETS";
		private const string cTOTAL_PERSONAL_CHANNELS = "TOTAL_PERSONAL_CHANNELS";
		private const string cTOTAL_BROADCAST_CHANNELS = "TOTAL_BROADCAST_CHANNELS";
		private const string cTOTAL_HANGOUTS = "TOTAL_HANGOUTS";
		private const string cWOK_POPULATION = "WOK_POPULATION";
	}
}
