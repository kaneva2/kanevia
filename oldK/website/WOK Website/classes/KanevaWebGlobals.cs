///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using System.Reflection;
using System.Resources;
using System.Globalization;
using System.Xml;
using CuteEditor;
using System.Text.RegularExpressions;


using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Specialized;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for KanevaWebGlobals.
	/// </summary>
	public class KanevaWebGlobals
	{
		private static ResourceManager resmgr = new ResourceManager ("KlausEnt.KEP.Kaneva.Kaneva", Assembly.GetExecutingAssembly());

		/// <summary>
		/// KanevaWebGlobals
		/// </summary>
		public KanevaWebGlobals ()
		{
		}

        /// <summary>
        /// GetJoinLink
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetJoinLink(Page page)
        {
            return page.ResolveUrl(KanevaGlobals.JoinLocation);
        }



		/// <summary>
		/// GetResourceString
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static string GetResourceString (string name)
		{
			return resmgr.GetString (name, System.Globalization.CultureInfo.CurrentCulture);
		}

        /// <summary>
        /// Retrieves the WOK Game id since it is different on dev/pview/rc/prod  prod is 3296
        /// </summary>
        public static int DaysLeftGameLicense
        {
            get
            {
                int days_Left = 50;
                string daysLeft = System.Configuration.ConfigurationManager.AppSettings["DaysLeftGameLicense"];
                if (daysLeft != null)
                {
                    try
                    {
                        Convert.ToInt32(daysLeft);
                    }
                    catch (Exception)
                    {
                    }
                }
                return (days_Left * 365);
            }
        }

		/// <summary>
		/// GetResourceString
		/// </summary>
		public static string GetResourceString (string name, params string [] args)
		{
			string [] newParams = new string [args.Length];
			
			for (int i = 0; i < args.Length; i++)
			{
				newParams [i] = GetResourceString (args [i]);
			}

			return String.Format (GetResourceString (name), newParams);
		}

		/// <summary>
		/// Get the user info from the current logged in user
		/// </summary>
		/// <returns></returns>
		public static User CurrentUser
		{
            get
            {
                User user = (User)HttpContext.Current.Items["User"];
                return user;

                //if (HttpContext.Current.Request.IsAuthenticated)
                //{
                //    User user = (User)HttpContext.Current.Items["User"];
                //    return user;
                //}
                //else
                //{
                //    return null;
                //}
            }
		}

		/// <summary>
		/// Get the user id from the current logged in user
		/// </summary>
		/// <returns></returns>
		public static int GetUserId ()
		{
			if (HttpContext.Current.Request.IsAuthenticated)
			{
				User userInfo = (User) HttpContext.Current.Items ["User"];
				return userInfo.UserId;
			}
			else
			{
				return 0;
			}
		}	

		/// <summary>
		/// Get the WOK player id from the current logged in user
		/// </summary>
		/// <returns></returns>
		public static int GetPlayerId ()
		{
			if (HttpContext.Current.Request.IsAuthenticated)
			{
				User userInfo = (User) HttpContext.Current.Items ["User"];
				return userInfo.WokPlayerId;
			}
			else
			{
				return 0;
			}
		}	

		/// <summary>
		/// Get the user id from the current logged in user
		/// </summary>
		/// <returns></returns>
		public static string GetUserState (string ustate)
		{
			switch (ustate)
			{
				case Constants.ONLINE_USTATE_OFF:
					return "";
				case Constants.ONLINE_USTATE_ON:		// logged into web site
					return "on-web";
				case Constants.ONLINE_USTATE_INWORLD:	// logged into WOK
					return "in-world";
				case Constants.ONLINE_USTATE_ONINWORLD:	// logged into web site and WOK
					return "in-world";
				default:
					return "";
			}
		}	

		/// <summary>
		/// Is the user in the Virtual World?
		/// </summary>
		/// <param name="ustate"></param>
		/// <returns></returns>
		public static bool IsInWorld (string ustate)
		{
			return (ustate.Equals (Constants.ONLINE_USTATE_INWORLD) || ustate.Equals (Constants.ONLINE_USTATE_ONINWORLD));
		}

		/// <summary>
		/// RemoveScripts
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static bool ContainsInjectScripts (string text)
		{
			bool bInvalid = false;

			try
			{
				XmlDocument doc = EditorUtility.ConvertToXmlDocument (text);
				bInvalid = ContainsScriptsRecursive (doc.DocumentElement);
			}
			catch (Exception) {}

			return bInvalid;
		}

		/// <summary>
		/// RemoveScriptsRecursive
		/// </summary>
		/// <param name="element"></param>
		private static bool ContainsScriptsRecursive (XmlElement element)
		{
			if (ShouldRemove (element))
			{
				return true;
			}
			foreach(XmlElement child in element.SelectNodes("*"))
			{
				return (ContainsScriptsRecursive (child));
			}

			return false;
		}
		
		/// <summary>
		/// GetAssetTypeName
		/// </summary>
		public static string GetAssetTypeName (int assetTypeId)   
		{
			string typeName = "Media";

			switch (assetTypeId)
			{
				case (int) Constants.eASSET_TYPE.GAME:
					typeName = "Game";
					break;

				case (int) Constants.eASSET_TYPE.VIDEO:
					typeName = "Video";
					break;

				case (int) Constants.eASSET_TYPE.MUSIC:
					typeName = "Audio";
					break;

				case (int) Constants.eASSET_TYPE.PICTURE:
					typeName = "Photo";
					break;

				case (int) Constants.eASSET_TYPE.PATTERN:
					typeName = "Pattern";
					break;

				case (int) Constants.eASSET_TYPE.WIDGET:
					typeName = "Widget";
					break;

				case (int) Constants.eASSET_TYPE.TV:
					typeName = "Streaming TV";
					break;
			}

			return typeName;
		}

//		/// <summary>
//		/// RemoveScripts
//		/// </summary>
//		/// <param name="text"></param>
//		/// <returns></returns>
//		public static string RemoveScripts (string text)
//		{
//			XmlDocument doc = EditorUtility.ConvertToXmlDocument (text);
//			RemoveScriptsRecursive(doc.DocumentElement);
//			//return doc.OuterXml;
//			return doc.DocumentElement.InnerXml;
//		}
//
//		/// <summary>
//		/// RemoveScriptsRecursive
//		/// </summary>
//		/// <param name="element"></param>
//		private static void RemoveScriptsRecursive (XmlElement element)
//		{
//			if(ShouldRemove(element))
//			{
//				element.ParentNode.RemoveChild(element);
//				return;
//			}
//			foreach(XmlElement child in element.SelectNodes("*"))
//			{
//				RemoveScriptsRecursive(child);
//			}
//		}

		/// <summary>
		/// ShouldRemove
		/// </summary>
		/// <param name="element"></param>
		/// <returns></returns>
		private static bool ShouldRemove (XmlElement element)
		{
			string name=element.LocalName.ToLower();
			//check the element name
			switch(name)
			{
				case "link"://maybe link to another css that contains scripts(behavior,expression)
				case "script":
					return true;
			}
			//check the attribute
			foreach(XmlAttribute attr in element.Attributes)
			{
				string attrname=attr.LocalName.ToLower();
				//<img onclick=....
				if(attrname.StartsWith("on"))
					return true;
				string val=attr.Value.ToLower();
				//<a href="javascript:scriptcode()"..
				if(val.IndexOf("script")!=-1)
					return true;
				//<a style="behavior:url(http://another/code.htc)"
				if(attrname=="style")
				{
					if(val.IndexOf("behavior")!=-1)
						return true;
					if(val.IndexOf("expression")!=-1)
						return true;
				}
    
			}
			return false;
		}

        public static bool isTextRestricted(string text, Constants.eRESTRICTION_TYPE restriction_type)
        {
            return isTextRestricted(text, restriction_type, false);
        }

        /// <summary>
        /// isTextRestricted
        /// </summary>
        /// <param name="text"></param>
        /// <param name="restriction_type"></param>
        /// <param name="replace"></param>
        /// <returns>boolean</returns>
        public static bool isTextRestricted(string text, Constants.eRESTRICTION_TYPE restriction_type, bool replace)
        {
            try
            {
                string pattern = "";
                text = text.ToUpper();
                Regex regx = null;

                // Only check potty mouth words if potty mouth or all type is requested
                if (restriction_type == Constants.eRESTRICTION_TYPE.POTTY_MOUTH ||
                    restriction_type == Constants.eRESTRICTION_TYPE.ALL)
                {
                    // Add potty mouth exact match word list to the REGEX expression
                    if (WebCache.GetRestrictedWordsList(Constants.eRESTRICTION_TYPE.POTTY_MOUTH, Constants.eMATCH_TYPE.EXACT).Length > 0)
                    {
                        pattern = @"\b(" + WebCache.GetRestrictedWordsList(Constants.eRESTRICTION_TYPE.POTTY_MOUTH, Constants.eMATCH_TYPE.EXACT) + ")" + @"\b";
                    }

                    // If there are potty mouth match anywhere words, add them to the REGEX expression
                    if (WebCache.GetRestrictedWordsList(Constants.eRESTRICTION_TYPE.POTTY_MOUTH, Constants.eMATCH_TYPE.ANYWHERE).Length > 0)
                    {
                        if (pattern.Length > 0) pattern += "|";
                        pattern += "(" + WebCache.GetRestrictedWordsList(Constants.eRESTRICTION_TYPE.POTTY_MOUTH, Constants.eMATCH_TYPE.ANYWHERE) + ")";
                    }

                    // create the Regex obj
                    regx = new Regex(pattern);

                    // If we have a match, return true.  No need to continue.
                    if (regx.IsMatch(text, 0))
                    {
                        return true;
                    }
                }


                // Only check reserved words if reserved or all type is requested              
                if (restriction_type == Constants.eRESTRICTION_TYPE.RESERVED ||
                    restriction_type == Constants.eRESTRICTION_TYPE.ALL)
                {
                    // Add reserved exact match word list to the REGEX expression
                    if (WebCache.GetRestrictedWordsList(Constants.eRESTRICTION_TYPE.RESERVED, Constants.eMATCH_TYPE.EXACT).Length > 0)
                    {
                        pattern = @"\b(" + WebCache.GetRestrictedWordsList(Constants.eRESTRICTION_TYPE.RESERVED, Constants.eMATCH_TYPE.EXACT) + ")" + @"\b";
                    }

                    // If there are reserved match anywhere words, add them to the REGEX expression
                    if (WebCache.GetRestrictedWordsList(Constants.eRESTRICTION_TYPE.RESERVED, Constants.eMATCH_TYPE.ANYWHERE).Length > 0)
                    {
                        if (pattern.Length > 0) pattern += "|";
                        pattern += "(" + WebCache.GetRestrictedWordsList(Constants.eRESTRICTION_TYPE.RESERVED, Constants.eMATCH_TYPE.ANYWHERE) + ")";
                    }

                    // create the Regex obj
                    regx = new Regex(pattern);

                    // return result of match
                    return regx.IsMatch(text, 0);
                }
            }
            catch (Exception)
            {

            }
            return false;
        }

        public static bool AllowDailyBonusReset
        {
            get
            {
                bool allowReset = false;
                string canReset = System.Configuration.ConfigurationManager.AppSettings["AllowDailyBonusResetForTestingOnly"];
                if (canReset != null)
                {
                    try
                    {
                        allowReset = Convert.ToBoolean (canReset);
                    }
                    catch (Exception)
                    {
                    }
                }
                return (allowReset);
            }
        }

        // ***********************************************
        // Security Functions
        // ***********************************************
        #region Security Functions

        /// <summary>
        /// Return the users privilege settings. All privileges available and their corresponding 
        /// access levels.
        /// </summary>
        /// <returns></returns>
        public static int CheckUserAccess(int privilegeId)
        {
            int accessLevel = (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE;
            try
            {
                //users privileges
                NameValueCollection privileges = CurrentUser.SitePrivileges;

                //find their access level for the privilege being checked
                accessLevel = Convert.ToInt32(privileges[privilegeId.ToString()]);
            }
            catch { }

            return accessLevel;
        }

        #endregion
	}
}
