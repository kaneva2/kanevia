///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.IO;
using System.Diagnostics;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.Metrics;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for BasePage.
	/// </summary>
	public class BasePage : System.Web.UI.Page
	{

        /// <summary>
        /// OnPreRender
        /// </summary>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (Configuration.MetricPageTimingEnabled)
            {
                // Metric Timing
                Stopwatch stopwatch = (Stopwatch)this.Context.Items["MetricPageTiming"];
                stopwatch.Stop();

                TimeSpan ts = stopwatch.Elapsed;

                string pageName = Path.GetFileName(Page.Request.Url.AbsolutePath).ToUpper();

                // Is this a page to record metrics on
                if (Configuration.MetricsPageTiming.ContainsKey(pageName))
                {
                    PageTiming pt = (PageTiming)Configuration.MetricsPageTiming[pageName];
                    MetricsFacade metricsFacade = new MetricsFacade();

                    if (metricsFacade.CanLogPageMetrics(pt.URL.ToUpper()))
                    {
                        metricsFacade.InsertMetricsPageTiming(pt.Name, ts.TotalMilliseconds);
                    }
                }
            }
        }


		/// <summary>
		/// OnInit
		/// </summary>
		/// <param name="e"></param>
		protected override void OnInit (System.EventArgs e) 
		{
            if (Configuration.MetricPageTimingEnabled)
            {
                // Metric Timing
                Stopwatch stopwatch = new Stopwatch();
                this.Context.Items["MetricPageTiming"] = stopwatch;
                stopwatch.Start();
            }


            //// If a new session and they are logged in, they automatically logged in with a cookie
            //// Update last login and number of logins
            //if (Session.IsNewSession && Request.IsAuthenticated)
            //{
            //    UsersUtility.UpdateLastLogin (GetUserId (), Common.GetVisitorIPAddress());
            //    Session ["userId"] = GetUserId ();  // Used to remove from list of active users on session end
            //}

			//base.OnInit (e);

            /* 
             * Removed because we are no longer using hbx, however technique may be valid for omniture.
			
			if (MagicAjax.MagicAjaxContext.Current.IsAjaxCallForPage(Page))
			{	
				MagicAjax.AjaxCallHelper.Write("_hbPageView(location.pathname,''); if(document.getElementById('goog') != null)$('goog').innerHTML=$('goog').innerHTML;");
			}

            */

        }

		// ***********************************************
		// Formatting functions
		// ***********************************************
		#region Formatting functions

		/// <summary>
		/// KPointsToDollars
		/// </summary>
		/// <param name="dKPoints"></param>
		/// <returns></returns>
		protected Double ConvertKPointsToDollars (Double dKPoints)
		{
			return StoreUtility.ConvertKPointsToDollars (dKPoints);
		}

		/// <summary>
		/// Format currency
		/// </summary>
		/// <param name="dblCurrency"></param>
		/// <returns></returns>
		protected string FormatCurrencyForTextBox (Object dblCurrency)
		{
			return KanevaGlobals.FormatCurrencyForTextBox (dblCurrency);
		}

		/// <summary>
		/// Format the date into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public string FormatDate (Object dtDate)
		{
			if (dtDate.Equals (DBNull.Value))
				return "";
			return KanevaGlobals.FormatDate ((DateTime) dtDate);
		}

		public string FormatDateNumbersOnly (Object dtDate)
		{
			if (dtDate.Equals (DBNull.Value))
				return "";
			return KanevaGlobals.FormatDateNumbersOnly ((DateTime) dtDate);
		}

		/// <summary>
		/// Format the date into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public string FormatDateTime (Object dtDate)
		{
			if (dtDate.Equals (DBNull.Value))
				return "";
			return KanevaGlobals.FormatDateTime((DateTime) dtDate);
		}
		/// <summary>
		/// Format date as a time span
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		protected string FormatDateTimeSpan(Object dtDate)
		{
			return KanevaGlobals.FormatDateTimeSpan(dtDate);
		}
		/// <summary>
		/// Format image size
		/// </summary>
		/// <param name="imageSize"></param>
		/// <returns></returns>
		public string FormatImageSize (Object imageSize)
		{
			if (imageSize.Equals (DBNull.Value))
				return "";
			return KanevaGlobals.FormatImageSize(Convert.ToInt64 (imageSize));
		}

		/// <summary>
		/// Format K-Points
		/// </summary>
		/// <param name="amount"></param>
		/// <returns></returns>
		public string FormatKpoints (Object amount)
		{
			return FormatKpoints (amount, true);
		}

		/// <summary>
		/// Format K-Points
		/// </summary>
		/// <param name="amount"></param>
		/// <returns></returns>
		public string FormatKpoints (Object amount, bool bShowFreeText)
		{
			if (amount.Equals (DBNull.Value))
				return "K-0";

			return KanevaGlobals.FormatKPoints (Convert.ToDouble (amount), true, bShowFreeText);
		}

		/// <summary>
		/// FormatCurrency
		/// </summary>
		/// <param name="amount"></param>
		/// <returns></returns>
		public string FormatCurrency (Object amount)
		{
			return KanevaGlobals.FormatCurrency (Convert.ToDouble (amount));
		}

		/// <summary>
		/// Format the date into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public string FormatDateTimeSpan (Object dtDate, Object dtDateNow)
		{
			if (dtDate.Equals (DBNull.Value))
				return "";
			return KanevaGlobals.FormatDateTimeSpan (dtDate, dtDateNow);
		}

		//		/// <summary>
		//		/// Unformat the user entered date format into a DateTime
		//		/// </summary>
		//		/// <param name="strDate"></param>
		//		/// <returns></returns>
		//		public DateTime UnformateDate Time (string strDate)
		//		{
		//			return new DateTime ();
		//		}

		/// <summary>
		/// FormatBlogRating
		/// </summary>
		public string FormatBlogRating (Object amount)
		{
			return KanevaGlobals.FormatBlogRating (amount);
		}

		/// <summary>
		/// TruncateWithEllipsis
		/// </summary>
		public string TruncateWithEllipsis (string text, int length)
		{
			return KanevaGlobals.TruncateWithEllipsis (text, length);
		}

		/// <summary>
		/// TruncateWithEllipsis
		/// </summary>
		public string FormatSecondsAsTime (Object seconds)
		{
			if (seconds.Equals (DBNull.Value))
				return "00:00:00";

			return KanevaGlobals.FormatSecondsAsTime (Convert.ToInt32(seconds));
		}

		#endregion

        // ***********************************************
        // Get Facade Functions
        // ***********************************************
        #region Get Facades

        protected PromotionsFacade GetPromotionsFacade
        {
            get
            {
                return (new PromotionsFacade());
            }
        }

        protected TransactionFacade GetTransactionFacade
        {
            get
            {
                return (new TransactionFacade());
            }
        }

        protected UserFacade GetUserFacade
        {
            get
            {
                return (new UserFacade());
            }
        }

        protected MetricsFacade GetMetricsFacade
        {
            get
            {
                return (new MetricsFacade());
            }
        }

        #endregion

		// ***********************************************
		// Helper functions
		// ***********************************************
		#region Helper functions

		protected ListItem CreateListItem (string name, string theValue)
		{
			return CreateListItem (name, theValue, false);
		}

		protected ListItem CreateListItem (string name, string theValue, bool bold)
		{
			ListItem liNew = new ListItem (name, theValue);
			if (bold)
			{
				liNew.Attributes.Add ("style", "font-weight: bold;");
			}
			return liNew;
		}

		protected bool IsCommunityPublic (string IsPublic)
		{
			return (CommunityUtility.IsCommunityPublic (IsPublic));
		}

		/// <summary>
		/// GetOnlineText
		/// </summary>
		protected string GetOnlineText (string ustate)
		{
			//implement catch here to prevent error page from being displayed for a simple online stauts check
			//default to no status is error occurs
			try
			{
				string strUserState = KanevaWebGlobals.GetUserState (ustate);
				if(strUserState.Length > 0)
				{
					return "<p class=\"online\">" + strUserState + "</p>";
				}
				else
				{
					return "<p >&nbsp;</p>";
				}
			}
			catch(InvalidCastException)
			{
			}

			return "";
		}

		protected bool IsMature (int assetRatingId)
		{
			return StoreUtility.IsMatureRating (assetRatingId);
		}

		/// <summary>
		/// ShowCreditCardNumber
		/// </summary>
		/// <param name="number"></param>
		/// <returns></returns>
		protected string ShowCreditCardNumber (string number)
		{
			int showLast = 4;

			number = KanevaGlobals.Decrypt (number);

			if (number.Length > showLast)
			{
				return ("***********" + number.Substring (number.Length - showLast));
			}
			else
			{
				return number;
			}
		}

		/// <summary>
		/// AddKeepAlive - Avoid timeout issues
		/// </summary>
		public void AddKeepAlive ()
		{
			// Set timer 30 seconds before the timeout will expire
			int int_MilliSecondsTimeOut = (this.Session.Timeout * 60000) - 30000;

			string str_Script = "<script type='text/javascript'>\n" +
				"//Number of Reconnects\n" +
				"var count=0;\n" +
				//Maximum reconnects setting\n" +
				"var max = 5;\n\n" +
				"function Reconnect()\n" +
				"{\n" +
				"	count++;\n" +
				"	if (count < max)\n" +
				"	{\n" +
				"		window.status = 'Session refreshed ' + count.toString()+' time(s)' ;\n" +
				"		var img = new Image(1,1);\n" +
				"		img.src = '" + ResolveUrl ("~/sessionKeepAlive.aspx") + "';\n" +
				"	}\n" +
				"}\n\n" +
				"window.setInterval('Reconnect()'," + int_MilliSecondsTimeOut.ToString()+ @");" +
				"</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(this.GetType(), "Reconnect"))
			{
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Reconnect", str_Script);
			}
		}

		/// <summary>
		/// If they are in a community, get the communityId string
		/// </summary>
		/// <returns></returns>
		public string GetCommunityIdQueryString ()
		{
			string commId = "";
			if (Request ["communityId"] != null)
			{
				commId = "&communityId=" + Request ["communityId"].ToString ();
			}
			return commId;
		}

		/// <summary>
		/// SetDropDownIndex
		/// </summary>
		protected void SetDropDownIndex (DropDownList drp, string theValue)
		{
			try
			{
				drp.SelectedValue = theValue;
			}
			catch (Exception)
			{}
		}

		/// <summary>
		/// SetDropDownIndex
		/// </summary>
		protected void SetDropDownIndex (DropDownList drp, string theValue, bool bAddValue)
		{
			SetDropDownIndex (drp, theValue, bAddValue, theValue);
		}

		/// <summary>
		/// SetDropDownIndex
		/// </summary>
		protected void SetDropDownIndex (DropDownList drp, string theValue, bool bAddValue, string theDisplayName)
		{
			if (bAddValue)
			{
				ListItem li = drp.Items.FindByValue (theValue);
				if (li == null)
				{
					drp.Items.Insert (drp.Items.Count, new ListItem (theDisplayName, theValue));
				}
			}
			SetDropDownIndex (drp, theValue);
		}

		/// <summary>
		/// Show an error message on startup
		/// </summary>
		/// <param name="errorMessage"></param>
		protected void ShowErrorOnStartup (string errorMessage)
		{
			ShowErrorOnStartup (errorMessage, true);
		}

		/// <summary>
		/// Show an error message on startup
		/// </summary>
		/// <param name="errorMessage"></param>
		protected void ShowErrorOnStartup (string errorMessage, bool bShowPleaseMessage)
		{
			string scriptString = "<script language=JavaScript>";
			if (bShowPleaseMessage)
			{
				scriptString += "alert ('Please correct the following errors:\\n\\n" + errorMessage + "');";
			}
			else
			{
				scriptString += "alert ('" + errorMessage + "');";
			}
			scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(this.GetType(), "ShowError"))
			{
                ClientScript.RegisterStartupScript (this.GetType (), "ShowError", scriptString);
			}
		}

		/// <summary>
		/// Get the results text
		/// </summary>
		/// <returns></returns>
		public string GetResultsText (int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
		{
			return KanevaGlobals.GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
		}


		/// <summary>
		/// Get the results text
		/// </summary>
		/// <returns></returns>
		public string GetResultsText (int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
		{
			return KanevaGlobals.GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
		}


		/// <summary>
        /// GetErrorURL
		/// </summary>
		/// <returns></returns>
        public string GetErrorURL()
		{
			return ResolveUrl ("~/error.aspx");
		}

		/// <summary>
		/// GetCurrentURL
		/// </summary>
		/// <returns></returns>
		public string GetCurrentURL ()
		{
			return Request.CurrentExecutionFilePath + "?" + Request.QueryString.ToString ();
		}

		/// <summary>
		/// removeHTML
		/// </summary>
		/// <param name="strMessageInput"></param>
		/// <returns></returns>
		public string RemoveHTML (string strMessageInput)
		{
			int lngMessagePosition = 0;		// Holds the message position
			int intHTMLTagLength = 0;		// Holds the length of the HTML tags
			string strHTMLMessage = "";			// Holds the HTML message
			string strTempMessageInput = "";	// Temp store for the message input

			// Get out quick?
			if (strMessageInput.Length == 0)
			{
				return "";
			}

			// Place the message input into a temp store
			strTempMessageInput = strMessageInput;

			// If an HTML tag is found then jump to the end so we can strip it
			lngMessagePosition = strMessageInput.IndexOf ("<", lngMessagePosition);

			// Loop through each character in the post message
			while (lngMessagePosition > -1)
			{
				// Get the length of the HTML tag
				intHTMLTagLength = (strMessageInput.IndexOf (">", lngMessagePosition) - lngMessagePosition);
			
				if (intHTMLTagLength > 0)
				{
					// Place the HTML tag back into the temporary message store
					strHTMLMessage = strMessageInput.Substring (lngMessagePosition, intHTMLTagLength + 1);

					// Strip the HTML from the temp message store
					strTempMessageInput = strTempMessageInput.Replace (strHTMLMessage, "");
				}
				
				// If an HTML tag is found then jump to the end so we can strip it
				lngMessagePosition = strMessageInput.IndexOf ("<", lngMessagePosition + 1);
			}
		
			// Replace a few characters in the remaining text
			strTempMessageInput = strTempMessageInput.Replace ("<", "&lt;");
			strTempMessageInput = strTempMessageInput.Replace ( ">", "&gt;");
			strTempMessageInput = strTempMessageInput.Replace ( "'", "&#039;");
			strTempMessageInput = strTempMessageInput.Replace ( "\"", "&#034;");
			strTempMessageInput = strTempMessageInput.Replace ( "&nbsp;", " ");

			//Return the function
			return strTempMessageInput;
		}

		#endregion

		// ***********************************************
		// Security Functions
		// ***********************************************
		#region Security Functions

		/// <summary>
		/// Return the current user id
		/// </summary>
		/// <returns></returns>
		protected int GetUserId ()
		{
			return KanevaWebGlobals.GetUserId ();
		}

        /// <summary>
        /// Return the current player id for the current user
        /// </summary>
        /// <returns></returns>
        protected int GetPlayerId()
        {
            return KanevaWebGlobals.GetPlayerId();
        }

		/// <summary>
		/// Return the personal channel id of current logged in user
		/// </summary>
		/// <returns></returns>
		protected int GetPersonalChannelId ()
		{
			return KanevaWebGlobals.CurrentUser.CommunityId;
		}

		/// <summary>
		/// Is the current session user a site administrator?
		/// </summary>
		/// <returns></returns>
		public bool IsAdministrator ()
		{
			return UsersUtility.IsUserAdministrator ();
		}

		/// <summary>
		/// Is this user a site administrator?
		/// </summary>
		/// <returns></returns>
		public bool IsAdministrator (int userId)
		{
			return UsersUtility.IsUserAdministrator (userId);
		}

		/// <summary>
		/// Is this user a site administrator?
		/// </summary>
		/// <returns></returns>
		public bool IsAdministratorByUserRole (int role)
		{
			return UsersUtility.IsUserAdministratorByUserRole (role);
		}

		/// <summary>
		/// Is the current session user a CSR?
		/// </summary>
		/// <returns></returns>
		public bool IsCSR ()
		{
			return UsersUtility.IsUserCSR ();
		}

		/// <summary>
		/// Is this user a CSR?
		/// </summary>
		/// <returns></returns>
		public bool IsCSR (int userId)
		{
			return UsersUtility.IsUserCSR (userId);
		}

		/// <summary>
		/// Is this user a CSR?
		/// </summary>
		/// <returns></returns>
		public bool IsCSRByUserRole (int role)
		{
			return UsersUtility.IsUserCSRByUserRole (role);
		}

		/// <summary>
		/// Is the current session user a IT?
		/// </summary>
		/// <returns></returns>
		public bool IsIT ()
		{
			return UsersUtility.IsUserIT ();
		}

		/// <summary>
		/// Is this user a IT?
		/// </summary>
		/// <returns></returns>
		public bool IsIT (int userId)
		{
			return UsersUtility.IsUserIT (userId);
		}

		/// <summary>
		/// Is this user a IT?
		/// </summary>
		/// <returns></returns>
		public bool IsITByUserRole (int role)
		{
			return UsersUtility.IsUserITByUserRole (role);
		}

		/// <summary>
		/// determines if the user is a minor? 
		/// throws it exceptions
		/// </summary>
		/// <param name="age">age</param>
		/// <returns>bool</returns>
		public bool IsUserAMinor (int age)
		{
			//check age by comparing to cut off
			if(age >= KanevaGlobals.MinorCutOffAge)
			{
				return false;
			}
			//this return is for minor found
			return true;
		}

		/// <summary>
		/// determines if the user is a minor? 
		/// throws its exceptions
		/// </summary>
		/// <param name="birthday">birthday</param>
		/// <returns>bool</returns>
		public bool IsUserAMinor (DateTime birthday)
		{
			return IsUserAMinor (KanevaGlobals.GetAgeInYears (birthday));
		}

		#endregion

		// ***********************************************
		// Asset Functions
		// ***********************************************
		#region Asset Functions

		/// <summary>
		/// GetRaveToolTip
		/// </summary>
		public string GetRaveToolTip ()
		{
			return KanevaWebGlobals.GetResourceString ("raveInfo", "channel", "rave");
		}

		/// <summary>
		/// Get correct rave icon to display
		/// </summary>
		public string GetRaveIcon (int diggId)
		{
			return StoreUtility.GetRaveIcon(diggId, Page);
		}

		/// <summary>
		/// Get Asset Details Link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetAssetEmbedLink (int assetId, int height, int width, bool fullUrlRequired )
		{
			return StoreUtility.GetAssetEmbedLink (assetId, height, width, Page, false, fullUrlRequired );
		}


		/// <summary>
		/// GetShareLink
		/// </summary>
		public string GetShareLink (int assetId)
		{ 
			return ResolveUrl("~/mykaneva/newMessage.aspx?assetId=" + assetId + "&URL=" + GetAssetDetailsLink (assetId));
		}

		/// <summary>
		/// GetStatusText
		/// </summary>
		protected string GetStatusText (int publishStatus, int statusId, int assetId)
		{
			return StoreUtility.GetStatusText (publishStatus, statusId, assetId);
		}

		/// <summary>
		/// Get Asset Details Link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetAssetDetailsLink (int assetId)
		{
			return StoreUtility.GetAssetDetailsLink (assetId, Page);
		}

		/// <summary>
		/// Get asset edit Link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetAssetEditLink (int assetId)
		{
			return Page.ResolveUrl ("~/asset/assetEdit.aspx?assetId=" + assetId); 
		}   

		/// <summary>
		/// GetCopyrightInfo
		/// </summary>
		public void GetCopyrightInfo (DataRow drAsset, Label lblLicense, Label lblLicenseURL)
		{
			string assetName = Server.HtmlDecode (drAsset ["name"].ToString ());

			switch (drAsset["license_type"].ToString ())
			{
				case "D":
					// Public domain
					lblLicense.Text = "Public Domain (Entire copyright is granted to the public, all use is permitted)";
					break;

				case "C":
					// Creative commons
					lblLicense.Text = "<br><a border=\"0\" target=\"_resource\" alt=\"Creative Commons License\" href=\"" + drAsset["license_cc"].ToString () + "\"><img border=\"0\" src=\"http://creativecommons.org/images/public/somerights20.gif\"/></a><br>" +
						"The item '" + assetName + "' is licensed under a:<br><a target=\"_resource\" class=\"adminLinks\" href=\"" + drAsset["license_cc"].ToString () + "\">Creative Commons License</a>";

					try
					{
						// Add rtf data (May be used by searches later)
						lblLicense.Text += "<!--" +
							"<rdf:RDF xmlns=\"http://web.resource.org/cc/\"" +
							"	xmlns:dc=\"http://purl.org/dc/elements/1.1/\"" +
							"	xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">" +
							"<Work rdf:about=\"" + GetCurrentURL () + "\">" +
							"<dc:title>" + assetName + "</dc:title>" +
							"<license rdf:resource=\"" + drAsset["license_cc"].ToString () + "\" />" +
							"</Work>" +
							"</rdf:RDF>" +
							"-->";
					}
					catch (Exception){}

					break;
				case "I":
					// Comerical copyright
					lblLicense.Text = "May be used for commercial use.<BR>" + drAsset["license_additional"].ToString ();
					break;
				case "O":
				{
					// Other
					lblLicense.Text = drAsset["license_name"].ToString ();
					if (!drAsset["license_URL"].Equals (DBNull.Value))
					{
						lblLicenseURL.Text = "<br><a class=\"dateStamp\" target=\"_resource\" href=\"http://" + drAsset["license_URL"].ToString () + "\">copyright info </a>";
					}
					break;
				}
				default:
					// Default to personal
					lblLicense.Text = "Copyright - Personal Usage Only<br>" + drAsset["license_additional"].ToString ();
					break;
			}
		}

		#endregion

		// ***********************************************
		// Channel Functions
		// ***********************************************
		#region Channel Functions

		/// <summary>
		/// GetChannelShareLink
		/// </summary>
		protected string GetChannelShareLink (int channelId)
		{
			return (ResolveUrl ("~/myKaneva/newMessage.aspx?communityId=" + channelId));
		}

		/// <summary>
		/// Get the link to go to community members
		/// </summary>
		/// <param name="communityId"></param>
		/// <returns></returns>
		public string GetCommunityMembersHREF (string communityName)
		{
			return ResolveUrl ("~/community/" + communityName + ".members");
		}

		/// <summary>
		/// Get the action URL
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="bJoin"></param>
		/// <returns></returns>
		private string GetChannelActionURL (int communityId, bool bJoin)
		{
			return ResolveUrl ("~/community/commJoin.aspx?communityId=" + communityId + "&join=" + ((bJoin)?"Y":"N"));
		}

		/// <summary>
		/// Get the join or quit image
		/// </summary>
		/// <param name="bJoin"></param>
		/// <returns></returns>
		private string GetChannelActionImage (bool bJoin)
		{
			return ResolveUrl ("~/images/button_" + (bJoin?"join":"quit") + ".gif");
		}

		/// <summary>
		/// Get the channel action to perform
		/// </summary>
		protected string GetChannelAction (object accountTypeId, int communityId, object memberStatusId)
		{
			string strJoinMessage = "Join this World?";

			if (!CommunityUtility.IsCommunityPublic (communityId))
			{
				// It is private
				strJoinMessage = "Apply for membership to this World?";
			}
			
			if (accountTypeId == DBNull.Value)
			{
				return "<a title=\"Join\" onclick=\"return confirm('" + strJoinMessage + "')\" href=\"" + GetChannelActionURL (communityId, true) + "\"><img title=\"Join\" src=\"" + GetChannelActionImage (true) + "\" border=\"0\" /></a>";
			}
			else
			{
				// If they are an owner they cannot quit here
				if (Convert.ToInt32 (accountTypeId).Equals ((int) CommunityMember.CommunityMemberAccountType.OWNER))
				{
					return "<span class=\"pending\">*owner</span>";
				}

				// Check their status here
				switch (Convert.ToUInt32 (memberStatusId))
				{
					case ((UInt32) CommunityMember.CommunityMemberStatus.PENDING):
					{
						return "<span class=\"pending\">*pending</span>";
					}
					case ((UInt32) CommunityMember.CommunityMemberStatus.ACTIVE):
					{
						return "<a title=\"Quit\" onclick=\"return confirm('Are you sure you want to quit this World?')\" href=\"" + GetChannelActionURL (communityId, false) + "\"><img title=\"quit\" src=\"" + GetChannelActionImage (false) + "\" border=\"0\" /></a>";
					}
					case ((UInt32) CommunityMember.CommunityMemberStatus.DELETED):
					{
						return "<a title=\"Join\" onclick=\"return confirm('" + strJoinMessage + "')\" href=\"" + GetChannelActionURL (communityId, true) + "\"><img title=\"Join\" src=\"" + GetChannelActionImage (true) + "\" border=\"0\" /></a>";
					}
					case ((UInt32) CommunityMember.CommunityMemberStatus.LOCKED):
					{
						return "<span class=\"pending\">*locked</span>";
					}
					case ((UInt32) CommunityMember.CommunityMemberStatus.REJECTED):
					{
						return "<span class=\"pending\">*denied</span>";
					}
				}
			}

			return "";
		}

		/// <summary>
		/// Get the Channel status
		/// </summary>
		protected string GetChannelStatus (object isPublic)
		{
			if (CommunityUtility.IsCommunityPublic (Convert.ToString (isPublic)))
			{
				return "Public";
			}
			else
			{
				return "Private";
			}
		}

		/// <summary>
		/// Get the channel content type
		/// </summary>
		public string GetChannelContentType (string isAdult)
		{
			if (Convert.ToString (isAdult).Equals ("Y"))
			{
				return "Restricted";
			}
			else if (Convert.ToString (isAdult).Equals ("N"))
			{
				return "Everyone";
			}
			else
			{
				// adf070321: (mantis 3632) Legacy code adjusted from General, Teen, Mature.
				return "Everyone";
			}
		}

		#endregion

		// ***********************************************
		// User Functions
		// ***********************************************
		#region User Functions

		/// <summary>
		/// Is the current user adult?
		/// </summary>
		/// <returns></returns>
		public bool IsCurrentUserAdult ()
		{
			// Now we are showing all if they are not logged in.
			return IsCurrentUserAdult (true);
		}

		/// <summary>
		/// Is the current user adult?
		/// </summary>
		/// <returns></returns>
		public bool IsCurrentUserAdult (bool defaultForNotLoggedIn)
		{
			// If they are logged in and over 18, we can show the adult communities
			bool bIsAdult = false;

			if (Request.IsAuthenticated)
			{
                bIsAdult = (KanevaWebGlobals.CurrentUser.Age >= 17);
			}
			else
			{
				bIsAdult = defaultForNotLoggedIn;
			}

			return bIsAdult;
		}

		/// <summary>
		/// Get the correct Media Type
		/// </summary>
		protected string GetMediaImageURL (string imagePath, string size, int assetId, int assetTypeId)
		{
			return GetMediaImageURL (imagePath, size, assetId, assetTypeId, 0);
		}

		/// <summary>
		/// Get the correct Media Type
		/// </summary>
		protected string GetMediaImageURL (string imagePath, string size, int assetId, int assetTypeId, int thumbGenerated)
		{
			return StoreUtility.GetMediaImageURL (imagePath, size, assetId, assetTypeId, thumbGenerated, Page);
		}

		/// <summary>
		/// Return the photo image URL
		/// </summary>
		public string GetPhotoImageURL (string imagePath, string size)
		{
			return StoreUtility.GetPhotoImageURL (imagePath, size);
		}

		/// <summary>
		/// Return the music image URL
		/// </summary>
		public string GetMusicImageURL (string imagePath, string size)
		{
			return StoreUtility.GetMusicImageURL (imagePath, size);
		}

		/// <summary>
		/// Return the game image URL
		/// </summary>
		public string GetGameImageURL (string imagePath, string defaultSize)
		{
            // copied from developer helper class
            if (imagePath.Length.Equals(0))
            {
                return KanevaGlobals.GameImageServer + "/KanevaGameIcon_" + defaultSize + ".gif";
            }
            else
            {
                if ((defaultSize == null) || (defaultSize == ""))
                {
                    defaultSize = "me";
                }
                return KanevaGlobals.GameImageServer + "/" + Path.GetDirectoryName(imagePath).Replace("\\", "/") + "/" + Path.GetFileNameWithoutExtension(imagePath) + "_" + defaultSize + Path.GetExtension(imagePath);
            }
        }

		/// <summary>
		/// Return the video image URL
		/// </summary>
		public string GetVideoImageURL (string imagePath, string size)
		{
			return StoreUtility.GetVideoImageURL (imagePath, size);
		}

		/// <summary>
		/// Return the user image URL
		/// </summary>
		public string GetProfileImageURL (string imagePath)
		{
			return UsersUtility.GetProfileImageURL (imagePath);
		}

		/// <summary>
		/// Return the photo image URL
		/// </summary>
		public string GetBroadcastChannelImageURL (string imagePath, string defaultSize)
		{
			return CommunityUtility.GetBroadcastChannelImageURL (imagePath, defaultSize);
		}

		/// <summary>
		/// Return the gift image URL
		/// </summary>
		public string GetGiftImageURL(string giftId)
		{
			return StoreUtility.GetGiftImageURL (giftId);
		}

		/// <summary>
		/// Return the gift icon URL
		/// </summary>
		public string GetGiftIcon()
		{
			return StoreUtility.GetGiftIcon();
		}

		/// <summary>
		/// Return the user image URL
		/// </summary>
		public string GetProfileImageURL (string imagePath, string defaultSize, string gender)
		{
			return UsersUtility.GetProfileImageURL (imagePath, defaultSize, gender);
		}

		/// <summary>
		/// Redirect to the current user's home page
		/// </summary>
		public void RedirectToHomePage ()
		{
			Response.Redirect (GetPersonalChannelUrl (KanevaWebGlobals.CurrentUser.NameNoSpaces));
		}

		/// <summary>
		/// Return the personal channel link
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static string GetPersonalChannelUrl (string nameNoSpaces)
		{
			return KanevaGlobals.GetPersonalChannelUrl (nameNoSpaces);
		}

		/// <summary>
		/// Return the broadcast channel link
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static string GetBroadcastChannelUrl (string nameNoSpaces)
		{
			return KanevaGlobals.GetBroadcastChannelUrl (nameNoSpaces);
		}

		/// <summary>
		/// Return the appropriate channel link based on supplied  arguement
		/// specialized function for cases where control may need to be able to 
		/// display both - Global for reuse
		///
		/// </summary>
		/// <param name="nameNoSpaces"></param>
		/// <param name="choice"></param>
		/// <returns></returns>
		public static string GetUrlPrsnlBroadAsset (string id, int choice)
		{
			string URL = "";
			switch(choice)
			{
				case (int) Constants.eURL_TYPE.BROADBAND:
					URL = KanevaGlobals.GetBroadcastChannelUrl (id);
					break;
				case (int) Constants.eURL_TYPE.MEDIA:
					//handles exception at this level since this is most often used in .ascx page
					//default URL on exception still to be determined
					try 
					{
						//to be completed lated due to time constraint
						//URL = StoreUtility.GetAssetDetailsLink (Convert.ToInt32(id), Page);
					}
					catch(Exception)
					{
					}
					break;
				case (int) Constants.eURL_TYPE.PERSONAL:
				default:
					URL = KanevaGlobals.GetPersonalChannelUrl (id);
					break;
			}
			return URL;
		}

		#endregion

		// ***********************************************
		// Checkout Functions
		// ***********************************************
		#region Checkout Functions

		/// <summary>
		/// Generate a token
		/// </summary>
		/// <returns></returns>
		public string GenerateToken (int orderId)
		{
			return StoreUtility.GenerateToken (orderId);
		}

		/// <summary>
		/// Get the order link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetOrderLink (int assetId)
		{
			return StoreUtility.GetOrderLink (assetId, Page);
		}

		/// <summary>
		/// GetTransactionStatus
		/// </summary>
		protected string GetTransactionStatus (int transactionStatus, string statusDescription, DateTime transactionDate)
		{
			if (transactionStatus.Equals ((int) Constants.eTRANSACTION_STATUS.WAITING_VERIFICATION))
			{
				// If it is more than a day old, show it as expired
				DateTime dtCurrentTime = KanevaGlobals.GetCurrentDateTime ();
				TimeSpan tsDifference;

				if (!transactionDate.Equals (DBNull.Value))
				{
					tsDifference = dtCurrentTime - (DateTime) transactionDate;
					if (tsDifference.Days > 1)
					{
						return "<span style=\"color=red\">Expired</span>";
					}
				}
			}

			return statusDescription;
		}

		/// <summary>
		/// GetTransactionStatusLink
		/// </summary>
		/// <returns></returns>
		protected string GetTransactionStatusLink (int transactionStatus, string statusDescription, DateTime transactionDate)
		{
			if (transactionStatus.Equals ((int) Constants.eTRANSACTION_STATUS.WAITING_VERIFICATION))
			{
				// If it is more than a day old, show it as expired
				DateTime dtCurrentTime = KanevaGlobals.GetCurrentDateTime ();
				TimeSpan tsDifference;

				if (!transactionDate.Equals (DBNull.Value))
				{
					tsDifference = dtCurrentTime - (DateTime) transactionDate;
					if (tsDifference.Days > 1)
					{
						transactionStatus = (int) Constants.eTRANSACTION_STATUS.EXPIRED;
					}
				}
			}

			return "javascript:window.open('" + ResolveUrl ("~/mykaneva/transactionStatus.aspx#" + transactionStatus + "','add','toolbar=no,width=600,height=300,menubar=no,scrollbars=yes,status=yes').focus();");
		}

		#endregion

		// ***********************************************
		// Bread Crumbs
		// ***********************************************
		#region Bread Crumbs

		/// <summary>
		/// GetBreadCrumbs
		/// </summary>
		/// <returns></returns>
		public BreadCrumbs GetBreadCrumbs ()
		{
			if (Session ["breadCrumb"] == null)
			{
				BreadCrumbs breadCrumbs = new BreadCrumbs ();
				Session ["breadCrumb"] = breadCrumbs;
				return breadCrumbs;
			}

			return (BreadCrumbs) Session ["breadCrumb"];
		}

		/// <summary>
		/// ResetBreadCrumb
		/// </summary>
		public void ResetBreadCrumb ()
		{
			Session ["breadCrumb"] = null;
		}

		public void AddBreadCrumb (BreadCrumb newBreadCrumb)
		{
			BreadCrumbs breadCrumbs = GetBreadCrumbs ();
			BreadCrumb breadCrumb;

			// See if this already exists, if it does, remove the rest
			for (int i = 0; i < breadCrumbs.Count; i++)
			{
				breadCrumb = (BreadCrumb) breadCrumbs.Item (i);

				if (breadCrumb.Text.Equals (newBreadCrumb.Text))
				{
					// Remove the rest and get out
					for (int j = breadCrumbs.Count - 1; j > i; j--)
					{
						breadCrumbs.Remove (j);
					}
					return;
				}
			}

			breadCrumbs.Add (newBreadCrumb);
		}

		/// <summary>
		/// DisplayBreadCrumb
		/// </summary>
		[Obsolete("This is for backward compatibilities, will be removed in next version")]
		public void DisplayBreadCrumb (PlaceHolder phBreadCrumb)
		{
		}

		/// <summary>
		/// NavigateBackToBreadCrumb
		/// </summary>
		public void NavigateBackToBreadCrumb (int backCount)
		{
			// Make sure it exists first
			BreadCrumbs breadCrumbs = GetBreadCrumbs ();
			string qs = "";

			if (breadCrumbs.Count >= backCount)
			{
				BreadCrumb bcPage = (BreadCrumb) breadCrumbs.Item (breadCrumbs.Count - backCount);
				if (bcPage.PageNumber > 0)
				{	
					qs += "&" + Constants.PARAM_PAGE_NUMBER + "=" + bcPage.PageNumber;
				}
				if (bcPage.Sort.Length > 0)
				{
					qs += "&" + Constants.PARAM_SORT + "=" + bcPage.Sort;
				}
				if (bcPage.SortOrder.Length > 0)
				{	
					qs += "&" + Constants.PARAM_SORT_ORDER + "=" + bcPage.SortOrder;
				}
				if (bcPage.Filter.Length > 0)
				{	
					qs += "&" + Constants.PARAM_FILTER + "=" + bcPage.Filter;
				}
				Response.Redirect (ResolveUrl (bcPage.Hyperlink) + qs);
			}
			else
			{
				// Go home
                Response.Redirect(ResolveUrl("~/free-virtual-world.kaneva"));				
			}
		}

		#endregion

		// ***********************************************
		// Tag Functions
		// ***********************************************
		#region Tag Functions

		/// <summary>
		/// GetAssetTags
		/// </summary>
		/// <param name="keywords"></param>
		/// <returns></returns>
		public string GetAssetTags (string keywords)
		{
			return StoreUtility.GetAssetTags (keywords, Page);
		}

		/// <summary>
		/// GetAssetTags
		/// </summary>
		/// <param name="keywords"></param>
		/// <returns></returns>
		public string GetAssetTags (string keywords, int type)
		{
			return StoreUtility.GetAssetTags (keywords, Page, type);
		}

		/// <summary>
		/// GetAssetCategoryList
		/// </summary>
		/// <param name="categories"></param>
		/// <returns></returns>
		public string GetAssetCategoryList (string categories)
		{
			return StoreUtility.GetAssetCategoryList (categories, Page);
		}

        /// <summary>
        /// GetAssetTypeName
        /// </summary>
        public string GetAssetTypeName(int assetTypeId)
        {
            try
            {
                DataTable dtAssetTypes = WebCache.GetAssetTypes();
                DataRow [] drAssetType = dtAssetTypes.Select("asset_type_id = " + assetTypeId, "");

                if (drAssetType != null && drAssetType.Length > 0)
                {
                    return drAssetType [0]["name"].ToString();
                }
            }
            catch (Exception) { };

            return "Unknown";
        }

        /// <summary>
        /// GetAssetTypeIcon
        /// </summary>
        public string GetAssetTypeIcon(int assetTypeId)
        {
            try
            {
                DataTable dtAssetTypes = WebCache.GetAssetTypes();
                DataRow[] drAssetType = dtAssetTypes.Select("asset_type_id = " + assetTypeId, "");

                if (drAssetType != null && drAssetType.Length > 0)
                {
                    return drAssetType[0]["icon_path"].ToString();
                }
            }
            catch (Exception) { };

            return "";
        }
		
		
		/// <summary>
		/// GetChannelTags
		/// </summary>
		/// <param name="keywords"></param>
		/// <returns></returns>
		public string GetChannelTags (string keywords)
		{
			Hashtable htKeywords = new Hashtable ();
			StoreUtility.AddToHash (htKeywords, keywords);

			string strKeywords = "";

			IDictionaryEnumerator en = htKeywords.GetEnumerator ();
			while (en.MoveNext ())
			{
				strKeywords += "<a href=\"" + ResolveUrl ("~/community/commDrillDown.aspx?tag=" + en.Value) + "\" class=\"Text12\">" + en.Value + "</a>&nbsp; ";
			}

			if (strKeywords.Length == 0)
			{
				strKeywords = "<span class=\"Text12\">Currently not tagged.</span>";
			}

			return strKeywords;
		}

		#endregion

		// ***********************************************
		// Forum Functions
		// ***********************************************
		#region Forum Functions

		/// <summary>
		/// Get the forum Threads URL
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetThreadURL (int topicId, int threadId)
		{
			if (threadId > 0)
			{
				return Page.ResolveUrl ("~/forum/forumThreads.aspx?topicId=" + topicId + GetCommunityIdQueryString () + "#" + threadId);
			}
			else
			{
				return Page.ResolveUrl ("~/forum/forumThreads.aspx?topicId=" + topicId + GetCommunityIdQueryString ());
			}
		}

		/// <summary>
		/// Get the forum Threads URL
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetThreadURL (int topicId)
		{
			return GetThreadURL (topicId, 0);
		}

		
		#endregion

		// ***********************************************
		// Links
		// ***********************************************
		#region Links

		/// <summary>
		/// Get Blog Details Link
		/// </summary>
		public string GetBlogDetailsLink (int topicId)
		{
			return KanevaGlobals.GeBlogUrl (topicId);
		}

		/// <summary>
		/// Get Blog Details Link
		/// </summary>
		public string GetBlogDetailsLink (int topicId, int communityId)
		{
			return GetBlogDetailsLink (topicId);
		}

		/// <summary>
		/// Get the License status link
		/// </summary>
		/// <param name="torrentStatus"></param>
		/// <returns></returns>
		protected string GetGameLicenseStatusLink (int statusId)
		{
			return "javascript:window.open('" + ResolveUrl ("~/asset/gameLicenseStatus.aspx#" + statusId + "','add','toolbar=no,width=600,height=250,menubar=no,scrollbars=yes,status=yes').focus();return false;");
		}

		/// <summary>
		/// GetPingHistoryLink
		/// </summary>
		protected string GetPingHistoryLink (int engineId)
		{
			return ResolveUrl ("~/reports/pingHistory.aspx?engineId=" + engineId);
		}

		#endregion

		// ***********************************************
		// Private Messages
		// ***********************************************
		#region Private Messages

		protected string GetMessageCssClass (string toViewable)
		{
            if (toViewable.Equals("U"))
			{
				return "messageUnread";
			}

			return "message";
		}

		/// <summary>
		/// GetTypeImage for messages
		/// </summary>
		/// <param name="theType"></param>
		/// <param name="statusId"></param>
		/// <returns></returns>
		protected string GetTypeImage (int theType, string toViewable)
		{
            return GetTypeImage(theType, toViewable, 0);
		}
		
		/// <summary>
		/// GetTypeImage for messages
		/// </summary>
		/// <param name="theType"></param>
		/// <param name="statusId"></param>
		/// <returns></returns>
		protected string GetTypeImage (int theType, string toViewable, int replied)
		{
			switch (theType)
			{
				case (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE:
				{
					if (toViewable.Equals ("U"))
					{
						return ResolveUrl ("~/images/forum/iconMail.gif");
					}
					else
					{
						if (replied > 0)
						{
							return ResolveUrl ("~/images/forum/icnMailReply.gif");
						}

						return ResolveUrl ("~/images/forum/iconMailOpened.gif");
					}
				}
				case (int) Constants.eMESSAGE_TYPE.ALERT:
				{
					return ResolveUrl ("~/images/forum/iconImportant.gif");
				}
					//				case (int) Constants.eMESSAGE_TYPE.FRIEND_REQUEST:
					//				{
					//					return ResolveUrl ("~/images/toothy2.gif");
					//				}
					//				case (int) Constants.eMESSAGE_TYPE.MEMBER_REQUEST:
					//				{
					//					return ResolveUrl ("~/images/toothy2.gif");
					//				}
				case (int) Constants.eMESSAGE_TYPE.GIFT:
				{
                    if (toViewable.Equals("U"))
					{
						return ResolveUrl ("~/images/forum/iconMailGift.gif");
					}
					else
					{
						return ResolveUrl ("~/images/forum/iconMailGiftOpened.gif");
					}
				}
				default:
				{
					return ResolveUrl ("~/images/forum/iconMailOpened.gif");
				}
			}
		}

		#endregion

	}


}
