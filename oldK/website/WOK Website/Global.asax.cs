///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Security.Principal;
using System.Data;
using System.Configuration;

// Import log4net classes.
using log4net;
using log4net.Config;

using Facade = Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;
using WOK;
using WOK.classes;

namespace KlausEnt.KEP.Kaneva 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
	
		public Global()
		{
			InitializeComponent();
		}

        /// <summary>
        /// Get the current cache
        /// </summary>
        /// <returns></returns>
        public static System.Web.Caching.Cache Cache()
        {
            return HttpRuntime.Cache;
        }

		/// <summary>
		/// Application_Start
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Application_Start(Object sender, EventArgs e)
		{
			// Change to log4net to be consistent with other modules
            XmlConfigurator.Configure(new System.IO.FileInfo(Server.MapPath("~") + "\\" + System.Configuration.ConfigurationManager.AppSettings["LogConfigFile"]));

            // Initialize the WoKWebSessionTokenProvider. todo: Allow for configuration of the ttl (in seconds? minutes? hours?)
            int ttlSecs = 0;
            try
            {
                string sttlSecs = System.Configuration.ConfigurationManager.AppSettings["WoKSessionTokenTTLSecs"];
                if (sttlSecs != null)
                {
                    ttlSecs = Convert.ToInt32(sttlSecs);
                }
                else
                {
                    m_logger.Warn("No value specified for WokSessionTokenTTLSecs. Defaulting to no TTL");
                }
            }
            catch (FormatException)
            {
                m_logger.Warn("Invalid value specified for WokSessionTokenTTLSecs. Defaulting to no TTL");
            }

            provider.init(new TimeSpan(0,0,0,Convert.ToInt32(ttlSecs)));

            // Setup application RabbitMQ connection.
            try
            {
                var factory = new RabbitMQ.Client.ConnectionFactory
                {
                    HostName = Facade.Configuration.RabbitMQHost,
                    Port = Facade.Configuration.RabbitMQPort,
                    UserName = Facade.Configuration.RabbitMQUsername,
                    Password = Facade.Configuration.RabbitMQPassword,
                    VirtualHost = Facade.Configuration.RabbitMQVHost,
                    AutomaticRecoveryEnabled = Facade.Configuration.RabbitMQAutoRecoveryEnabled,
                    NetworkRecoveryInterval = Facade.Configuration.RabbitMQReconnectInterval
                };

                rabbitMQConnection = factory.CreateConnection();
            }
            catch(Exception exc)
            {
                m_logger.Error("Unable to establish connection to RabbitMQ.", exc);
            }
        }
 
		protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{
	
		}

		/// <summary>
		/// Application_EndRequest
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Application_EndRequest (Object sender, EventArgs e)
		{
            // Clean up per-request RabbitMQ channel.
            try
            {
                RabbitMQ.Client.IModel channel = RequestRabbitMQChannel(false);
                if (channel != null && channel.IsOpen)
                {
                    channel.Close();
                }
            }
            catch(Exception exc) 
            {
                m_logger.Error("Error when attempting to close RabbitMQ channel.", exc);
            }
		}

		/// <summary>
		/// Application_AuthenticateRequest
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Application_AuthenticateRequest (Object sender, EventArgs e)
		{
            if (Request.IsAuthenticated)
            {
                // Load user data
                Facade.UserFacade userFacade = new Facade.UserFacade();
                User user = userFacade.GetUser(Convert.ToInt32(User.Identity.Name));
                HttpContext.Current.Items["User"] = user;

                // The only time this shoud happen is for wok user just created due to
                // wok_player_id is set from wok server via SP so it does not invalidate the cache.
                // Fixes issue with inventory not showing for up to 5 minutes.
                // http://bugz.kaneva.com/mantis/view.php?id=7773
                if (user.WokPlayerId.Equals(0))
                {
                    userFacade.InvalidateKanevaUserCache(Convert.ToInt32(User.Identity.Name));
                    user = userFacade.GetUser(Convert.ToInt32(User.Identity.Name));
                }

                ArrayList ar = UsersUtility.GetUserRoleArray(user.Role);
                string[] roles = (string[])ar.ToArray(Type.GetType("System.String"));

                HttpContext.Current.User = new GenericPrincipal(User.Identity, roles);
            }
            else
            {
                User user = new User();
                HttpContext.Current.Items["User"] = user;
            }

            WokUserInfo wokUserInfo = new WokUserInfo();
            HttpContext.Current.Items["WokUserInfo"] = wokUserInfo;

            if (Request.IsAuthenticated)
            {
                // Load user data
                wokUserInfo.LoadData(Convert.ToInt32(User.Identity.Name));
            }
        }

		/// <summary>
		/// Application_Error
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Application_Error (Object sender, EventArgs e)
		{
			if (Server.GetLastError () is HttpException)
			{
				m_logger.Warn ("HttpException from " + Request.Url.ToString (), Server.GetLastError ());
			}
			else
			{
				m_logger.Error ("Unhandled application error in " + Request.Url.ToString (), Server.GetLastError ());
			}
		}

		/// <summary>
		/// Session_End
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Session_End (Object sender, EventArgs e)
		{
		
		}

		/// <summary>
		/// Application_End
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Application_End (Object sender, EventArgs e)
		{
            // Cleanup RabbitMQ connection.
            try
            {
                if (rabbitMQConnection != null && rabbitMQConnection.IsOpen)
                {
                    rabbitMQConnection.Close();
                }
            }
            catch(Exception exc)
            {
                m_logger.Warn("Error when attempting to close RabbitMQ connection.", exc);
            }
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Logger
		/// </summary>
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        /// <summary>
        ///  A TokenProvider that is used for TAZ.
        /// </summary>
        private static SessionTokenProvider provider = new SessionTokenProvider();

        /// <summary>
        /// Obtain a reference to the application's token provider
        /// </summary>
        /// <returns>The application's token provider</returns>
        public static SessionTokenProvider tokenProvider()
        {
            return provider;
        }

        private static RabbitMQ.Client.IConnection rabbitMQConnection;

        /// <summary>
        /// Returns the RabbitMQ channel for the current request context.
        /// </summary>
        /// <param name="reconnectIfClosed">If true, re-open the request's channel if closed.</param>
        /// <returns>A RabbitMQ channel or null if an error occurs.</returns>
        public static RabbitMQ.Client.IModel RequestRabbitMQChannel(bool reconnectIfClosed = true)
        {
            RabbitMQ.Client.IModel channel = null;

            if (rabbitMQConnection != null && rabbitMQConnection.IsOpen)
            {
                if (HttpContext.Current.Items["rabbitMQChannel"] != null)
                    channel = (RabbitMQ.Client.IModel)HttpContext.Current.Items["rabbitMQChannel"];

                if ((channel == null || channel.IsClosed) && reconnectIfClosed)
                {
                    try
                    {
                        channel = rabbitMQConnection.CreateModel();
                        HttpContext.Current.Items["rabbitMQChannel"] = channel;
                    }
                    catch(Exception) { }
                }
            }

            return channel;     
        }

        public static int authCount         = 0; 
        public static int authOptimizeCount = 0;


		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

