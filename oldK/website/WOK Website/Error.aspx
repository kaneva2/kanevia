<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.Error" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Wok Web Error</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Whoops, we experienced a problem completing your request. <br/><br/>
        <b>Remember to be sure you have supplied correct credentials.</b><br/>
        If you continue to see this error please contact <a href="http://support.kaneva.com">support</a>
    </div>
    </form>
</body>
</html>
