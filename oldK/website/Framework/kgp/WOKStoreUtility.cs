///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Text;


namespace KlausEnt.KEP.Kaneva.framework.kgp
{
	/// <summary>
	/// Summary description for WOKStoreUtility.
	/// </summary>
	public class WOKStoreUtility
	{
		/// <summary>
		/// get list of stores
		/// </summary>
		/// <returns></returns>
		public static DataTable GetStores()
		{
			string dbName = KanevaGlobals.DbNameKGP;

			string sqlString = "SELECT s.name as storeName, concat( s.name, ' in the ', left( sw.name, INSTR(sw.name, '.exg')-1), ' (', cast(count(*) as char), ')' ) as name, s.store_id " + 
								"  FROM " + dbName + ".stores s " + 
								"       INNER JOIN " + 
								       dbName + ".supported_worlds sw ON (s.channel_id = sw.channel_id) " + 
							    "     INNER JOIN " +
										dbName + ".store_inventories si ON (s.store_id = si.store_id) " + 
								" INNER JOIN " + 
								      dbName + ".items i ON (si.global_id = i.global_id) " + 
							    " group by storeName ";

			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			
			return dbUtility.GetDataTable( sqlString );
		}

        /// <summary>
		/// get pass group info from table
		/// </summary>
		/// <returns>DataTable</returns>
		public static DataTable GetPassGroups()
		{
			string dbName = KanevaGlobals.DbNameKGP;

            string sql = "SELECT pass_group_id, name" +
                        "  FROM " + dbName + ".pass_groups ";

			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			
			return dbUtility.GetDataTable( sql );
		}

		/// <summary>
		/// get the contents of a WOK store
		/// </summary>
		/// <param name="storeId"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public static PagedDataTable GetStoreContents( int storeId, int pageNumber, int pageSize )
		{
			string dbName = KanevaGlobals.DbNameKGP;

			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			
			string selectList = " i.name, i.market_cost, i.global_id, i.selling_price ";
			string tableList = dbName + ".store_inventories si INNER JOIN " + dbName + ".items i ON (si.global_id = i.global_id) ";
			string whereClause = "si.store_id = @storeId";
			string orderByList = "name ASC";
			Hashtable theParams = new Hashtable();
			theParams.Add( "@storeId", storeId );

			return dbUtility.GetPagedDataTable( selectList, tableList, whereClause, orderByList, theParams, pageNumber, pageSize );

		}
        /// <summary>
        /// get all the currently available in world items (3 parameters)
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        public static PagedDataTable GetWOKItems(string filter, int pageNumber, int pageSize)
        {
            return GetWOKItems(-1, filter, pageNumber, pageSize);
        }


        /// <summary>
        /// get all the currently available in world items (4 parameters)
        /// created to allow for singular item retrieval if so desired by specifying the items id number
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        public static PagedDataTable GetWOKItems(int itemID, string filter, int pageNumber, int pageSize)
        {
            string dbName = KanevaGlobals.DbNameKGP;
            string whereClause = "";
            Hashtable theParams = new Hashtable();

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string selectList = " i.global_id, i.name, i.market_cost, i.selling_price, i.required_skill, i.required_skill_level, i.use_type, i.use_value ";
            string tableList = dbName + ".items i ";

            //limit by item id if one is provided
            if (itemID > 0)
            {
                whereClause += "i.global_id = @itemId ";
                theParams.Add("@itemId", itemID);
            }

            //append the where clause if a filter is provided
            if ((filter != null) && (filter != ""))
            {
                if (whereClause.Length > 0)
                {
                    whereClause += "AND ";
                }
                whereClause += filter;
            }

            string orderByList = " name ASC";


            return dbUtility.GetPagedDataTable(selectList, tableList, whereClause, orderByList, theParams, pageNumber, pageSize);
        }

        /// <summary>
        /// get all the currently available in world items (3 parameters)
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        public static DataTable GetWOKItems(string filter)
        {
            return GetWOKItems(-1, filter);
        }


        /// <summary>
        /// get all the currently available in world items (4 parameters)
        /// created to allow for singular item retrieval if so desired by specifying the items id number
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        public static DataTable GetWOKItems(int itemID, string filter)
        {
            string dbName = KanevaGlobals.DbNameKGP;

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            Hashtable theParams = new Hashtable();

            string sql = "SELECT i.global_id, i.name, i.market_cost, i.selling_price, i.required_skill, i.required_skill_level, i.use_type, i.use_value " +
            " FROM " +  dbName + ".items i ";

            //limit by item id if one is provided
            if (itemID > 0)
            {
                sql += " WHERE i.global_id = @itemId ";
                theParams.Add("@itemId", itemID);
            }
            
            //append the where clause if a filter is provided
            if ((filter != null) && (filter != ""))
            {
                if (itemID > 0)
                {
                    sql += " AND " + filter;
                }
                else
                {
                    sql += " WHERE " + filter;
                }
            }

            sql += " ORDER BY name ASC";


            return dbUtility.GetDataTable(sql,theParams);

        }

	}
}
