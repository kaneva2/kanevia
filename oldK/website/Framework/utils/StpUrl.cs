///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using Kaneva.BusinessLayer.Facade;
using System.Text.RegularExpressions;

/// <summary>
/// class of helper fns for manipulating stp urls to contain these contants, in case things change
/// NOTE this is a port of C++'s StpUrl.h 
/// </summary>
namespace KlausEnt.KEP.Kaneva
{
    public class StpUrl
    {
        // NOTE the only other place these suffixes exist are in *vw_url* Stored Procs and GotoPlaceEvent.h
        // If changes are made, they must be made in both places
        const string HOME_SUFFIX = ".home";
        const string CHANNEL_SUFFIX = ".channel";
        const string ZONE_SUFFIX = ".zone";
        const string PEOPLE_SUFFIX = ".people";
        const string ARENA_SUFFIX = ".arena";
        const string URL_PREFIX = "kaneva://";

        // will be xxxx://<gamename>/ (currently xxxx is kaneva, but we won't look for that
        public static void ExtractGameName( string url, out string gameName )
        {
            gameName = "";
        	
            int start = url.IndexOf( "://" );
            if ( start != -1 )
            {
	            start += 3; // skip ://
	            int end = url.IndexOf("/",start);
	            if ( end != -1 )
	            {
		            gameName = url.Substring( start, end-start );
	            }
	            else
	            {
		            gameName = url.Substring( start );

	            }
            }
        }

        // will be xxxx://<gamename>[/<placename>] (currently xxxx is kaneva, but we won't look for that
        public static void ExtractPlaceName( string url, out string placeName, bool excludeSuffix = false )
        {
            placeName = "";
        	
            int start = url.IndexOf( "://" );
            if ( start != -1 )
            {
	            start += 3; // skip ://
	            int end = url.IndexOf("/",start);
	            if ( end != -1 )
	            {
		            if ( url.Length > end )
			            placeName = url.Substring( end+1, url.Length-end-1 );
		            // else just <prefix>://gamename/
	            }
                // else just <prefix>://gamename
            }

            if (excludeSuffix)
            {
                int placeType = 0;
                ExtractPlaceType(url, out placeType);

                switch (placeType)
                {
                    case (int)Constants.APARTMENT_ZONE:
                        placeName = placeName.Replace(HOME_SUFFIX, string.Empty);
                        break;
                    case (int)Constants.BROADBAND_ZONE:
                        placeName = placeName.Replace(CHANNEL_SUFFIX, string.Empty);
                        break;
                    case (int)Constants.PERMANENT_ZONE:
                        placeName = placeName.Replace(ZONE_SUFFIX, string.Empty);
                        break;
                    case (int)Constants.ZONE_TO_PERSON:
                        placeName = placeName.Replace(PEOPLE_SUFFIX, string.Empty);
                        break;
                    case (int)Constants.ARENA_ZONE:
                        placeName = placeName.Replace(ARENA_SUFFIX, string.Empty);
                        break;
                }
            }
        }

        // will be xxxx://<gamename>[/<placename>].<type> (currently xxxx is kaneva, but we won't look for that
        public static void ExtractPlaceType(string url, out int placeType)
        {
            placeType = 0;
            int dotIndex = url.LastIndexOf('.');

            switch (url.Substring(dotIndex < 0 ? 0 : dotIndex))
            {
                case HOME_SUFFIX:
                    placeType = Constants.APARTMENT_ZONE;
                    break;
                case CHANNEL_SUFFIX:
                    placeType = Constants.BROADBAND_ZONE;
                    break;
                case ZONE_SUFFIX:
                    placeType = Constants.PERMANENT_ZONE;
                    break;
                case PEOPLE_SUFFIX:
                    placeType = Constants.ZONE_TO_PERSON;
                    break;
                case ARENA_SUFFIX:
                    placeType = Constants.ARENA_ZONE;
                    break;
            }
        }

        public static bool ValidateUrl(string url)
        {
            Regex validPattern = new Regex ("^" + URL_PREFIX + "[\\d\\s_\\-a-zA-Z0-9!]{1,50}" +
                "(/|(/[\\d\\s_\\-a-zA-Z0-9]{1,50}" +
                "(\\" + HOME_SUFFIX + "|\\" + CHANNEL_SUFFIX + "|\\" + ZONE_SUFFIX + "|\\" + PEOPLE_SUFFIX + "|\\" + ARENA_SUFFIX + ")))?$");

            if (validPattern.IsMatch(url))
            {
                string urlGameName = string.Empty;
                ExtractGameName(url, out urlGameName);
                urlGameName = urlGameName.Replace(" ", "");

                string urlPlaceName = string.Empty;
                ExtractPlaceName(url, out urlPlaceName);
                urlPlaceName = urlPlaceName.Replace(" ", "");

                int urlPlaceType = 0;
                ExtractPlaceType(url, out urlPlaceType);

                // A well-formed url must conform to the following rules:
                // GameName section may not be null
                // Must either have both a valid PlaceName and PlaceType OR
                // PlaceName and PlaceType must not be specified.
                return (!string.IsNullOrWhiteSpace(urlGameName) &&
                        ((string.IsNullOrWhiteSpace(urlPlaceName) && urlPlaceType == 0) ||
                            (!string.IsNullOrWhiteSpace(urlPlaceName) && urlPlaceType > 0)));
            }

            return false;
        }

        public static string MakeUrlPrefix( string gameName )
        {
            string ret = URL_PREFIX;
            ret += gameName;

            return ret;
        }

        public static string MakePersonUrlPath( string userName )
        {
            string ret = userName;
            ret += PEOPLE_SUFFIX;
        		
            return ret;
        }

        public static string MakeAptUrlPath( string userName )
        {
            string ret = userName;
            ret += HOME_SUFFIX;
        		
            return ret;
        }

        public static string MakeZoneUrlPath( string zoneName )
        {
            string ret = zoneName;
            ret += ZONE_SUFFIX;
        		
            return ret;
        }

        public static string MakeCommunityUrlPath( string communityName )
        {
            string ret = communityName;
            ret += CHANNEL_SUFFIX;
        		
            return ret;
        }

        public static string GetPluginJSForUser(bool IsAuthenticated, bool HasWOKAccount, string userName, string requestTrackingGUID, int currentUserId)
        {
            return GetPluginJS (IsAuthenticated, HasWOKAccount, 0, 0, "", userName, requestTrackingGUID, currentUserId);
        }

        public static string GetPluginJS (bool IsAuthenticated, bool HasWOKAccount, int gameId, int communityId, string communityName, string requestTrackingGUID, int currentUserId)
        {
            return GetPluginJS(IsAuthenticated, HasWOKAccount, gameId, communityId, communityName, "", requestTrackingGUID, currentUserId);
        }

        public static string GetPluginJS (bool IsAuthenticated, bool HasWOKAccount, int gameId, int communityId, string communityName, string userName, string requestTrackingGUID, int currentUserId, bool showNotification = true)
        {
            string strShowNotification = showNotification.ToString().ToLower();
            string stpUrl = StpUrl.MakeUrlPrefix(KanevaGlobals.WokGameId.ToString()); // default stp url
            string requestTrackingJS = "";
            string requestTrackingParams = "";
            string strCheckBanJS = "";

            string playNowGuid = Guid.NewGuid().ToString();

            // Tracking GUID exists?
            if (requestTrackingGUID.Length > 0)
            {
                requestTrackingJS = "trackMetrics('http://" + KanevaGlobals.CurrentSiteName + "/requestTracking.aspx', '" + Constants.cREQUEST_TRACKING_URL_PARAM + "','" + requestTrackingGUID.ToString() + "','" + Constants.cREQUEST_TRACKING_URL_IE_PARAM + "','" + Constants.cREQUEST_TRACKING_URL_USERAGENT_PARAM + "');";

                // Add tracking param so we can see if they made it in world after accepting a request that puts them in world, this is passed to
                // wok so they can mark a new accept as made it into world
                requestTrackingParams = Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTrackingGUID.ToString();
            }

            //check to see if browsing user is logged in/a member of kaneva
            if (IsAuthenticated)
            {
                //browsing user is a member of WOK
                if (HasWOKAccount)
                {

                    // Is the user blocked???????????
                    // Rewrite this to do a AJAX call on click, instead of checking for every one when we draw the URL's
                    ////if ((gameId > 0 || communityId > 0) && new UserFacade().IsUserBlocked(currentUserId, gameId, communityId))
                    ////{
                    ////    return "javascript:return showNotAvailable();";
                    ////}

                    if (gameId > 0 || communityId > 0)
                    {
                        strCheckBanJS = "if(IsBlocked('http://" + KanevaGlobals.CurrentSiteName + "/isBlocked.aspx'," + currentUserId + "," + gameId + "," + communityId + ")){return showNotAvailable();};";
                    }

                    if (gameId > 0)
                    { // this community is a 3D App
                        try
                        {
                            // This section is added to handle "fake" 3d apps (Kaneva City, KaChing, Emplyment Agency, etc)
                            // These spaces are not true games and the game id will not work in the url so they have
                            // to be referenced by the name
                            DataRow dr = new GameFacade ().GetGameFaux3DApp (gameId);
                            if (dr != null && dr["game_id"] != null && Convert.ToInt32 (dr["game_id"]) > 0)
                            {
                                stpUrl += "/" + StpUrl.MakeZoneUrlPath(dr["game_name"].ToString()) + "/?";
                            }
                            else
                            {
                                stpUrl = StpUrl.MakeUrlPrefix (gameId.ToString ()) + "/?" + requestTrackingParams;
                            }
                        }
                        catch
                        {
                            stpUrl = StpUrl.MakeUrlPrefix (gameId.ToString ()) + "/?" + requestTrackingParams;
                        }
                    }
                    else if (communityId > 0 && gameId == -1)
                    { // this is a users home
                        stpUrl += "/" + StpUrl.MakeAptUrlPath(communityName) + "/?";
                    }
                    else if (communityId > 0)
                    { // this is a regular community
                        stpUrl += "/" + StpUrl.MakeCommunityUrlPath(communityName) + "/?";
                    }
                    else if (gameId == 0 && communityId == 0 && communityName.Length > 0)
                    {   // this handles a public place which has not game id nor community id
                        stpUrl += "/" + StpUrl.MakeZoneUrlPath(communityName) + "/?";
                    }
                    else if (userName.Length > 0)
                    {
                        stpUrl += "/" + StpUrl.MakePersonUrlPath(userName) + "/?";
                    }
                    else
                    {
                        // shouldn't ever happen, only if caller bad
                        // Response.Write("<script type=\"text/javascript\"></script>");
                        return "alert('Invalid destination for Community Url: " + communityId + "' );";
                    }

                    stpUrl = System.Web.HttpUtility.UrlPathEncode(stpUrl);
                    return "javascript:" + strCheckBanJS + "DetectPlugin(" + KanevaGlobals.CurrentPatcherVersion.ToString() + ", " + KanevaGlobals.WokGameId.ToString() + ",'" + stpUrl + "', '', '', " + strShowNotification + ", '" + playNowGuid + "');callGAEvent('World', 'Go3DClick', 'Go3D');" + requestTrackingJS + "return false;";
                }
                //browsing user is not a memeber of WOK (profile owner status unknown) send user to download page
                else
                {
                    stpUrl = System.Web.HttpUtility.UrlPathEncode(stpUrl);
                    return "javascript:DetectPlugin(" + KanevaGlobals.CurrentPatcherVersion.ToString() + ", " + KanevaGlobals.WokGameId.ToString() + ",'" + stpUrl + "/?', '', '', " + strShowNotification + ", '" + playNowGuid + "');callGAEvent('World', 'Go3DClick', 'Go3D');" + requestTrackingJS + "return false;";
                }
            }
            //browsingUser is not logged in show only login/join kaneva (no one's WOK status know)
            else
            {
                stpUrl = System.Web.HttpUtility.UrlPathEncode(stpUrl);
                return "javascript:DetectPlugin(" + KanevaGlobals.CurrentPatcherVersion.ToString() + ", " + KanevaGlobals.WokGameId.ToString() + ",'" + stpUrl + "/?link=joinsignin&'" + ", '', '', " + strShowNotification + ", '" + playNowGuid + "');callGAEvent('World', 'Go3DClick', 'Go3D');" + requestTrackingJS + "return false;";
            }
        }

    }
}