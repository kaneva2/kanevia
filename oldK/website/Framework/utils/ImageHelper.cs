///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.IO;
using log4net;
using System.Drawing;


using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.Facade;

using System.Text;
using System.Security.Cryptography;
using System.Net;

using FreeImageAPI;

using Amazon.S3;
using Amazon.S3.Transfer;

namespace KlausEnt.KEP.Kaneva
{
    /// <summary>
    /// Summary description for ImageHelper.
    /// </summary>
    public class ImageHelper
    {
        public const int COMMUNITY_IMAGE = 1;
        public const int CHANNEL_IMAGE = 2;
        public const int ASSET_IMAGE = 3;
        public const int GAME_IMAGE = 4;
        public const int WOK_IMAGE = 5;
        public const int WOK_ITEM_IMAGE = 6;
        public const int TEMP_IMAGE = 7;
        public const int ACHIEVEMENT_IMAGE = 8;

        public ImageHelper()
        {
        }

        private static AmazonS3Client GetAmazonS3Client()
        {
            return new AmazonS3Client(Configuration.AmazonS3AccessKeyId,
                           Configuration.AmazonS3SecretAccessKey,
                           Amazon.RegionEndpoint.USEast1);
        }

        /// <summary>
        /// UploadImageFromUser - throws exceptions that must be handled by the calling function
        /// </summary> 
        ///<param name="userId">id of user</param>
        ///<param name="itemId">item id</param>
        ///<param name="browseTHUMB">html input file</param>
        public static void UploadImageFromUser(int userId, int itemId, HtmlInputFile browseTHUMB, string imagesServerPath, int imageUse)
        {
            // Files are now stored in content repository
            if (imagesServerPath != null)
            {
                //declare the file name of the image
                string filename = null;

                //create PostedFile
                HttpPostedFile File = browseTHUMB.PostedFile;

                try
                {
                    //generate newpath
                    string newPath = Path.Combine(imagesServerPath, userId.ToString());

                    //upload image from client to image repository
                    UploadImageFromUser(ref filename, File, newPath, KanevaGlobals.MaxUploadedAvatarSize);

                    //generate newpath and channel id
                    string imagePath = newPath + Path.DirectorySeparatorChar + filename;

                    //create any and all thumbnails needed
                    GenerateAllThumbnails(imageUse, filename, imagePath, imagesServerPath, userId, itemId, null);

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            else
            {
                throw new Exception("No image server path (content repository)");
            }

        }

        /// <summary>
        /// UploadImageFromUser - throws exceptions that must be handled by the calling function
        /// </summary> 
        ///<param name="File">file to be uploaded</param>
        ///<param name="uploadLocation">directory to which the image is saved</param>
        ///<param name="MaxImageSize">max size allowed of this image - enter -1 to skip this limitation</param>
        public static void UploadImageFromUser(ref string filename, HttpPostedFile File, string uploadLocation, int MaxImageSize)
        {
            if (File != null && File.ContentLength > 0)
            {
                // Make sure the image is not too big
                if (MaxImageSize > 0)
                {
                    if (File.ContentLength > MaxImageSize)
                    {
                        throw new Exception("The photo is too large, please select a photo smaller than " + KanevaGlobals.FormatImageSize(MaxImageSize) + ".");
                    }
                }

                // Make sure it is a valid file type
                string strExtensionUpperCase = System.IO.Path.GetExtension(File.FileName).ToUpper();
                if (!strExtensionUpperCase.Equals(".JPG") && !strExtensionUpperCase.Equals(".JPEG") && !strExtensionUpperCase.Equals(".GIF"))
                {
                    throw new Exception("Photo must be a jpg, jpeg, or gif.");
                }

                //try catch is just to log error and close out resources
                System.IO.MemoryStream msImage = null;
                try
                {
                    //get file name from uploaded file and remove any bad characters from it 
                    filename = StoreUtility.CleanImageFilename(Path.GetFileName(File.FileName));

                    //Create byte Array with file length 
                    byte[] bImage = new Byte[File.ContentLength];

                    //force the control to load data into the array
                    File.InputStream.Read(bImage, 0, File.ContentLength);

                    //read image into a memory stream
                    msImage = new System.IO.MemoryStream(bImage);

                    //write it to the image storage location
                    WriteImage(uploadLocation, filename, msImage);
                }
                catch (Exception exc)
                {
                    m_logger.Error("ImageHelperError - error uploading image from client machine ", exc);
                    //throw new exception so calling function will know something went wrong
                    throw new Exception("An error has occured during image processing");
                }
                finally
                {
                    msImage.Close();
                }
            }
        }

        /// <summary>
        /// UploadImageFromServer - throws exceptions that must be handled by the calling function.
        /// Intended to be used when re-uploading images that have already been stored on the server.
        /// </summary> 
        /// <param name="filename">name of the file</param>
        ///<param name="imageData">file to be uploaded</param>
        ///<param name="uploadLocation">directory to which the image is saved</param>
        public static void UploadImageFromServer(string filename, string imageHttpUrl, string uploadLocation)
        {
            byte[] imageData = Common.DownloadFile(imageHttpUrl);

            if (imageData != null && imageData.Length > 0)
            {
                try
                {
                    //read image into a memory stream
                    using (System.IO.MemoryStream msImage = new System.IO.MemoryStream(imageData))
                    {
                        //write it to the image storage location
                        WriteImage(uploadLocation, filename, msImage);
                    }
                }
                catch (Exception exc)
                {
                    m_logger.Error("ImageHelperError - error uploading image from byte server ", exc);
                    //throw new exception so calling function will know something went wrong
                    throw new Exception("An error has occured during image processing");
                }
            }
            else
            {
                throw new Exception("An error has occured during image processing");
            }
        }

        /// <summary>
        /// WriteImage
        /// </summary>
        public static void WriteImage(string newPath, string filename, System.IO.MemoryStream msImage)
        {
            // Make sure the directory exists
            FileInfo fileInfo = new FileInfo(newPath + Path.DirectorySeparatorChar);

            if (Configuration.UseAmazonS3Storage)
            {
                try
                {
                    AmazonS3Client azS3client = GetAmazonS3Client();
                    TransferUtility fileTransferUtility = new TransferUtility(azS3client);

                    // Upload data from a type of System.IO.Stream.
                    fileTransferUtility.Upload(msImage, Configuration.AmazonS3Bucket, newPath.Replace("\\", "/") + "/" + filename);
                }
                catch (AmazonS3Exception s3Exception)
                {
                    m_logger.Error("S3 Error " + s3Exception.Message, s3Exception);
                }
            }
            else
            {

                if (!fileInfo.Directory.Exists)
                {
                    fileInfo.Directory.Create();
                }

                System.IO.FileStream fImage = null;
                BinaryReader binReader = null;

                try
                {
                    fImage = new System.IO.FileStream(newPath + Path.DirectorySeparatorChar + filename, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                    binReader = new BinaryReader(msImage);

                    int size = 4096;
                    byte[] bytes = new byte[size];
                    int numBytes;
                    while ((numBytes = binReader.Read(bytes, 0, size)) > 0)
                    {
                        fImage.Write(bytes, 0, numBytes);
                    }
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in writing file" + filename, exc);
                }
                finally
                {
                    if (binReader != null)
                    {
                        binReader.Close();
                    }
                    if (fImage != null)
                    {
                        fImage.Close();
                    }
                }
            }
        }


        /// <summary>
        /// GenerateAllChannelThumbnails
        /// </summary>
        public static void GenerateAllCommuntyThumbnails(string filename, string imagePath, string contentRepository, int userId, int communityId)
        {
            GenerateAllThumbnails(COMMUNITY_IMAGE, filename, imagePath, contentRepository, userId, communityId, null);
        }

        /// <summary>
        /// GenerateAllChannelThumbnails
        /// </summary>
        public static void GenerateAllChannelThumbnails(string filename, string imagePath, string contentRepository, int userId, int channelId)
        {
            GenerateAllThumbnails(CHANNEL_IMAGE, filename, imagePath, contentRepository, userId, channelId, null);
        }

        public static void GenerateWOKItemThumbs(string filename, string imagePath, string wokItemThumbnailRepository, int userId, int itemId)
        {
            GenerateWOKItemThumbs(filename, imagePath, wokItemThumbnailRepository, userId, itemId, null);
        }

        public static void GenerateWOKItemThumbs(string filename, string imagePath, string wokItemThumbnailRepository, int userId, int itemId, string fileExtOverride)
        {
            GenerateAllThumbnails(WOK_ITEM_IMAGE, filename, imagePath, wokItemThumbnailRepository, userId, itemId, null, fileExtOverride);
        }

        public static void GenerateWOKPhotoThumbs(string filename, string imagePath, string wokImageRepository, int userId, int itemId)
        {
            GenerateAllThumbnails(WOK_IMAGE, filename, imagePath, wokImageRepository, userId, itemId, null);
        }

        public static void GenerateAchievementThumbs(string filename, string imagePath, string wokItemThumbnailRepository, int userId, int itemId)
        {
            GenerateAchievementThumbs(filename, imagePath, wokItemThumbnailRepository, userId, itemId, null);
        }

        public static void GenerateAchievementThumbs(string filename, string imagePath, string wokItemThumbnailRepository, int userId, int itemId, string fileExtOverride)
        {
            GenerateAllThumbnails(ACHIEVEMENT_IMAGE, filename, imagePath, wokItemThumbnailRepository, userId, itemId, null, fileExtOverride);
        }

        /// <summary>
        /// GenerateAllAssetThumbnails
        /// </summary>
        public static void GenerateAllAssetThumbnails(string filename, string originalImagePath, string imagePath, string contentRepository, int userId, int assetId)
        {
            GenerateAllThumbnails(ASSET_IMAGE, filename, imagePath, contentRepository, userId, assetId, originalImagePath);
        }

        /// <summary>
        /// GenerateThumbnails
        /// </summary>
        private static void GenerateAllThumbnails(int imageUse, string filename, string imagePath, string contentRepository, int userId, int ID, string originalImagePath)
        {
            GenerateAllThumbnails(imageUse, filename, imagePath, contentRepository, userId, ID, originalImagePath, null);
        }

        /// <summary>
        /// GenerateThumbnails
        /// </summary>
        private static void GenerateAllThumbnails(int imageUse, string filename, string imagePath, string contentRepository, int userId, int ID, string originalImagePath, string fileExtOverride)
        {
            System.Drawing.Image imgOriginal = null;

            try
            {
                if (Configuration.UseAmazonS3Storage)
                {
                    string originalImage = (Configuration.ImageServer + "/" + imagePath).Replace("\\", "/");

                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(originalImage);
                    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    System.IO.Stream receiveStream = httpWebResponse.GetResponseStream();
                    imgOriginal = System.Drawing.Image.FromStream(receiveStream);
                }
                else
                {
                    imgOriginal = System.Drawing.Image.FromFile(imagePath);
                }


                switch (imageUse)
                {
                    case COMMUNITY_IMAGE:
                        // Small
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.CHANNEL_THUMB_SMALL_HEIGHT, (int)Constants.CHANNEL_THUMB_SMALL_WIDTH, "_sm", "thumbnail_small_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Medium
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.CHANNEL_THUMB_MEDIUM_HEIGHT, (int)Constants.CHANNEL_THUMB_MEDIUM_WIDTH, "_me", "thumbnail_medium_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Large
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.CHANNEL_THUMB_LARGE_HEIGHT, (int)Constants.CHANNEL_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);
                        break;
                    case CHANNEL_IMAGE:
                        // Small
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.PEOPLE_THUMB_SMALL_HEIGHT, (int)Constants.PEOPLE_THUMB_SMALL_WIDTH, "_sm", "thumbnail_small_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Medium
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.PEOPLE_THUMB_MEDIUM_HEIGHT, (int)Constants.PEOPLE_THUMB_MEDIUM_WIDTH, "_me", "thumbnail_medium_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Large
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.PEOPLE_THUMB_LARGE_HEIGHT, (int)Constants.PEOPLE_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // X-Large
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.PEOPLE_THUMB_XLARGE_HEIGHT, (int)Constants.PEOPLE_THUMB_XLARGE_WIDTH, "_xl", "thumbnail_xlarge_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Facebook Size
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.PEOPLE_THUMB_SQ_HEIGHT, (int)Constants.PEOPLE_THUMB_SQ_WIDTH, "_sq", "thumbnail_square_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);


                        break;
                    case ASSET_IMAGE:
                        // Small
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.MEDIA_THUMB_SMALL_HEIGHT, (int)Constants.MEDIA_THUMB_SMALL_WIDTH, "_sm", "thumbnail_small_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Medium
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.MEDIA_THUMB_MEDIUM_HEIGHT, (int)Constants.MEDIA_THUMB_MEDIUM_WIDTH, "_me", "thumbnail_medium_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Large
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.MEDIA_THUMB_LARGE_HEIGHT, (int)Constants.MEDIA_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);
                        break;
                    case GAME_IMAGE:
                    case WOK_IMAGE:
                        // Small
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.PHOTO_THUMB_SMALL_HEIGHT, (int)Constants.PHOTO_THUMB_SMALL_WIDTH, "_sm", null,
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Medium
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.PHOTO_THUMB_MEDIUM_HEIGHT, (int)Constants.PHOTO_THUMB_MEDIUM_WIDTH, "_me", null,
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Large
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.PHOTO_THUMB_LARGE_HEIGHT, (int)Constants.PHOTO_THUMB_LARGE_WIDTH, "_la", null,
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // XLarge
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.PHOTO_THUMB_XLARGE_HEIGHT, (int)Constants.PHOTO_THUMB_XLARGE_WIDTH, "_xl", null,
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Asset Details
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.PHOTO_THUMB_ASSETDETAILS_HEIGHT, (int)Constants.PHOTO_THUMB_ASSETDETAILS_WIDTH, "_ad", null,
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);
                        break;

                    case WOK_ITEM_IMAGE:
                        // Small
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.WOK_ITEM_THUMB_SMALL_HEIGHT, (int)Constants.WOK_ITEM_THUMB_SMALL_WIDTH, "_sm", "thumbnail_small_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Medium
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.WOK_ITEM_THUMB_MEDIUM_HEIGHT, (int)Constants.WOK_ITEM_THUMB_MEDIUM_WIDTH, "_me", "thumbnail_medium_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Large
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.WOK_ITEM_THUMB_LARGE_HEIGHT, (int)Constants.WOK_ITEM_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Asset Details
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.WOK_ITEM_THUMB_ASSETDETAILS_HEIGHT, (int)Constants.WOK_ITEM_THUMB_ASSETDETAILS_WIDTH, "_ad", "thumbnail_assetdetails_path",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        break;

                    case ACHIEVEMENT_IMAGE:
                        // Small
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.ACHIEVEMENT_THUMB_SMALL_HEIGHT, (int)Constants.ACHIEVEMENT_THUMB_SMALL_WIDTH, "_sm", "null",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Medium
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.ACHIEVEMENT_THUMB_MEDIUM_HEIGHT, (int)Constants.ACHIEVEMENT_THUMB_MEDIUM_WIDTH, "_me", "null",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        // Large
                        SaveThumbnail(imageUse, imgOriginal, (int)Constants.ACHIEVEMENT_THUMB_LARGE_HEIGHT, (int)Constants.ACHIEVEMENT_THUMB_LARGE_WIDTH, "_la", "null",
                            filename, imagePath, contentRepository, userId, ID, fileExtOverride);

                        break;
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error reading image path is " + imagePath, exc);
            }
            finally
            {
                if (imgOriginal != null)
                {
                    imgOriginal.Dispose();
                }
            }
        }

        /// <summary>
        /// SaveChannelThumbnail
        /// </summary>
        public static bool SaveChannelThumbnail(System.Drawing.Image imgOriginal, int MaxHeight, int MaxWidth, string postfix, string dbColumn,
            string filename, string imagePath, string contentRepository, int userId, int communityId)
        {
            return SaveThumbnail(CHANNEL_IMAGE, imgOriginal, MaxHeight, MaxWidth, postfix, dbColumn, filename, imagePath, contentRepository, userId, communityId);
        }

        /// <summary>
        /// SaveAssetThumbnail
        /// </summary>
        public static bool SaveAssetThumbnail(System.Drawing.Image imgOriginal, int MaxHeight, int MaxWidth, string postfix, string dbColumn,
            string filename, string imagePath, string contentRepository, int userId, int assetId)
        {
            return SaveThumbnail(ASSET_IMAGE, imgOriginal, MaxHeight, MaxWidth, postfix, dbColumn, filename, imagePath, contentRepository, userId, assetId);
        }

        /// <summary>
        /// SaveWOKItemImage
        /// </summary>
        public static bool SaveWOKItemImage(System.Drawing.Image imgOriginal, int MaxHeight, int MaxWidth, string postfix,
            string filename, string imagePath, string contentRepository, int userId, int wokId)
        {
            return SaveThumbnail(WOK_IMAGE, imgOriginal, MaxHeight, MaxWidth, postfix, null, filename, imagePath, contentRepository, userId, wokId);
        }

        /// <summary>
        /// SaveThumbnail
        /// </summary>
        public static bool SaveThumbnail(int imageUse, System.Drawing.Image imgOriginal, int MaxHeight, int MaxWidth, string postfix, string dbColumn,
            string filename, string imagePath, string contentRepository, int userId, int ID)
        {
            return SaveThumbnail(imageUse, imgOriginal, MaxHeight, MaxWidth, postfix, dbColumn, filename, imagePath, contentRepository, userId, ID, null);
        }

        /// <summary>
        /// SaveThumbnail
        /// </summary>
        public static bool SaveThumbnail(int imageUse, System.Drawing.Image imgOriginal, int MaxHeight, int MaxWidth, string postfix, string dbColumn,
            string filename, string imagePath, string contentRepository, int userId, int ID, string fileExtOverride)
        {

            string fileStoreHack = "";
            string newPath = "";
            string idType = "";

            string extension = Path.GetExtension(imagePath);
            if (fileExtOverride != null)
                extension = fileExtOverride;

            string newFilename = Path.GetFileNameWithoutExtension(filename) + postfix + extension;
            string updateFilename = Path.GetFileNameWithoutExtension(filename) + extension;

            System.Drawing.Image imgThumbnail = null;
            System.Drawing.Graphics graphic = null;

            //select the proper fileStoreHack
            switch (imageUse)
            {
                case COMMUNITY_IMAGE:
                    if (KanevaGlobals.FileStoreHack != "")
                    {
                        fileStoreHack = KanevaGlobals.FileStoreHack;
                    }
                    newPath = Path.Combine(contentRepository, userId.ToString());
                    idType = "community id";
                    break;
                case CHANNEL_IMAGE:
                    if (KanevaGlobals.FileStoreHack != "")
                    {
                        fileStoreHack = KanevaGlobals.FileStoreHack;
                    }
                    newPath = Path.Combine(contentRepository, userId.ToString());
                    idType = "channel id";
                    break;
                case ASSET_IMAGE:
                    if (KanevaGlobals.FileStoreHack != "")
                    {
                        fileStoreHack = KanevaGlobals.FileStoreHack;
                    }
                    newPath = Path.Combine(Path.Combine(contentRepository, userId.ToString()), ID.ToString());
                    idType = "asset id";
                    break;
                case GAME_IMAGE:
                    if (KanevaGlobals.GameFileStoreHack != "")
                    {
                        fileStoreHack = KanevaGlobals.GameFileStoreHack;
                    }
                    newPath = Path.Combine(Path.Combine(contentRepository, userId.ToString()), ID.ToString());
                    idType = "game id";
                    break;
                case WOK_IMAGE:
                    if (KanevaGlobals.WOKFileStoreHack != "")
                    {
                        fileStoreHack = KanevaGlobals.WOKFileStoreHack;
                    }
                    newPath = Path.Combine(Path.Combine(contentRepository, userId.ToString()), ID.ToString());
                    idType = "item id";
                    break;
                case WOK_ITEM_IMAGE:
                    fileStoreHack = KanevaGlobals.TextureFilestore;
                    newPath = Path.Combine(Path.Combine(contentRepository, userId.ToString()), ID.ToString());
                    idType = "item id";
                    break;
                case ACHIEVEMENT_IMAGE:
                    if (KanevaGlobals.FileStoreHack != "")
                    {
                        fileStoreHack = KanevaGlobals.FileStoreHack;
                    }
                    newPath = Path.Combine(Path.Combine(contentRepository, userId.ToString()), "ach" + ID.ToString());
                    idType = "achievement id";
                    break;
                case TEMP_IMAGE:
                    fileStoreHack = "";
                    newPath = Path.Combine(Path.Combine(contentRepository, userId.ToString()), "uploadTemp");
                    idType = "upload id";
                    break;
            }

            try
            {
                // Make sure the directory exists
                if (!System.IO.Directory.Exists(newPath))
                {
                    if (!Configuration.UseAmazonS3Storage)
                    {
                        System.IO.Directory.CreateDirectory(newPath);
                    }
                }

                // Max height, Max Width
                int NewWidth = imgOriginal.Width;
                int NewHeight = imgOriginal.Height;

                if (MaxWidth > 0 && NewWidth > MaxWidth)
                {
                    // Resize to max width
                    NewWidth = MaxWidth;
                    NewHeight = imgOriginal.Height * NewWidth / imgOriginal.Width;
                }

                if (MaxHeight > 0 && NewHeight > MaxHeight)
                {
                    // Resize
                    NewHeight = MaxHeight;
                    NewWidth = imgOriginal.Width * NewHeight / imgOriginal.Height;
                }

                //Create a new FrameDimension object from this image
                System.Drawing.Imaging.FrameDimension FrameDimensions = new System.Drawing.Imaging.FrameDimension(imgOriginal.FrameDimensionsList[0]);

                //Determine the number of frames in the image
                //Note that all images contain at least 1 frame, but an animated GIF
                //will contain more than 1 frame.
                int NumberOfFrames = imgOriginal.GetFrameCount(FrameDimensions);

                if (NumberOfFrames > 1 && newFilename.ToUpper().EndsWith(".GIF"))
                {
                    m_logger.Debug("Multiple Frames for " + idType + ID + " thumbnail_path = '" + imagePath + "'<br>");

                    imgThumbnail = new Bitmap(NewWidth, NewHeight);
                    graphic = System.Drawing.Graphics.FromImage(imgThumbnail);

                    graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    graphic.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    graphic.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;

                    graphic.DrawImage(imgOriginal, 0, 0, NewWidth, NewHeight);

                    ImageManipulation.OctreeQuantizer quantizer = new ImageManipulation.OctreeQuantizer(255, 8);
                    Bitmap quantized = quantizer.Quantize(imgThumbnail);

                    if (Configuration.UseAmazonS3Storage)
                    {
                        try
                        {
                            AmazonS3Client azS3client = GetAmazonS3Client();
                            TransferUtility fileTransferUtility = new TransferUtility(azS3client);

                            // Save it to Amazon s3
                            using (MemoryStream qStream = new MemoryStream())
                            {
                                quantized.Save(qStream, System.Drawing.Imaging.ImageFormat.Gif);
                                fileTransferUtility.Upload(qStream, Configuration.AmazonS3Bucket, newPath.Replace("\\", "/") + "/" + newFilename);
                            }
                        }
                        catch (AmazonS3Exception s3Exception)
                        {
                            m_logger.Error("S3 Error " + s3Exception.Message, s3Exception);
                        }


                    }
                    else
                    {
                        quantized.Save(newPath + Path.DirectorySeparatorChar + newFilename, System.Drawing.Imaging.ImageFormat.Gif);
                    }
                }
                else
                {
                    // Not multiple frames here
                    imgThumbnail = new Bitmap(NewWidth, NewHeight);
                    graphic = System.Drawing.Graphics.FromImage(imgThumbnail);

                    graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    graphic.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    graphic.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;

                    graphic.DrawImage(imgOriginal, 0, 0, NewWidth, NewHeight);

                    if (newFilename.ToUpper().EndsWith(".GIF"))
                    {
                        ImageManipulation.OctreeQuantizer quantizer = new ImageManipulation.OctreeQuantizer(255, 8);
                        Bitmap quantized = quantizer.Quantize(imgThumbnail);

                        if (Configuration.UseAmazonS3Storage)
                        {
                            try
                            {
                                AmazonS3Client azS3client = GetAmazonS3Client();
                                TransferUtility fileTransferUtility = new TransferUtility(azS3client);

                                // Save it to Amazon s3
                                using (MemoryStream qStream = new MemoryStream())
                                {
                                    quantized.Save(qStream, System.Drawing.Imaging.ImageFormat.Gif);
                                    fileTransferUtility.Upload(qStream, Configuration.AmazonS3Bucket, newPath.Replace("\\", "/") + "/" + newFilename);
                                }
                            }
                            catch (AmazonS3Exception s3Exception)
                            {
                                m_logger.Error("S3 Error " + s3Exception.Message, s3Exception);
                            }
                        }
                        else
                        {
                            quantized.Save(newPath + Path.DirectorySeparatorChar + newFilename, System.Drawing.Imaging.ImageFormat.Gif);
                        }
                    }
                    else
                    {
                        // for jpg
                        System.Drawing.Imaging.ImageCodecInfo jpgCodecEncoder = GetEncoder("image/jpeg");

                        if (jpgCodecEncoder == null)
                        {
                            m_logger.Error("jpeg Encoder is null");
                        }
                        else
                        {
                            System.Drawing.Imaging.EncoderParameters encoderParameters;
                            encoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
                            encoderParameters.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);

                            if (Configuration.UseAmazonS3Storage)
                            {
                                try
                                {
                                    AmazonS3Client azS3client = GetAmazonS3Client();
                                    TransferUtility fileTransferUtility = new TransferUtility(azS3client);

                                    // Save it to Amazon s3
                                    using (MemoryStream qStream = new MemoryStream())
                                    {
                                        imgThumbnail.Save(qStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                        fileTransferUtility.Upload(qStream, Configuration.AmazonS3Bucket, newPath.Replace("\\", "/") + "/" + newFilename);
                                    }
                                }
                                catch (AmazonS3Exception s3Exception)
                                {
                                    m_logger.Error("S3 Error " + s3Exception.Message, s3Exception);
                                }


                            }
                            else
                            {
                                imgThumbnail.Save(newPath + Path.DirectorySeparatorChar + newFilename, jpgCodecEncoder, encoderParameters);
                            }
                        }
                    }
                    //select the correct save
                    switch (imageUse)
                    {
                        case COMMUNITY_IMAGE:
                        case CHANNEL_IMAGE:
                            CommunityUtility.UpdateCommunityThumb(ID, fileStoreHack + userId.ToString() + "/" + newFilename, dbColumn);
                            break;
                        case ASSET_IMAGE:
                            StoreUtility.UpdateAssetThumb(ID, fileStoreHack + userId.ToString() + "/" + ID.ToString() + "/" + newFilename, dbColumn);
                            break;
                        case GAME_IMAGE:
                            (new GameFacade()).UpdateGameThumbnail(ID, fileStoreHack + userId.ToString() + "/" + ID.ToString() + "/" + updateFilename);
                            break;
                        case WOK_IMAGE:
                            WOKStoreUtility.UpdateCatalogItemThumb(ID, fileStoreHack + userId.ToString() + "/" + ID.ToString() + "/" + updateFilename);
                            break;
                        case WOK_ITEM_IMAGE:
                            WOKStoreUtility.UpdateWOKItemThumb(ID, fileStoreHack + userId.ToString() + "/" + ID.ToString() + "/" + newFilename, dbColumn);
                            break;
                        case ACHIEVEMENT_IMAGE:
                            (new GameFacade()).UpdateAchievementThumbnail((uint)ID, fileStoreHack + userId.ToString() + "/ach" + ID.ToString() + "/" + updateFilename);
                            break;
                    }
                }
            }
            catch (System.ArgumentException ioexc)
            {
                m_logger.Error("Error User Thumbs Generation " + filename + ", " + ioexc.ToString());
            }
            catch (System.IO.IOException ioexc2)
            {
                m_logger.Error("Error User Thumbs Generation " + filename + ", " + ioexc2.ToString());
            }
            catch (Exception ioexc3)
            {
                m_logger.Error("Error3 User Thumbs Generation " + filename + ", " + ioexc3.ToString());
            }

            if (imgThumbnail != null)
            {
                imgThumbnail.Dispose();
            }
            if (graphic != null)
            {
                graphic.Dispose();
            }

            return true;
        }

        /// <summary>
        /// GetEncoder
        /// </summary>
        private static System.Drawing.Imaging.ImageCodecInfo GetEncoder(string mimeType)
        {
            System.Drawing.Imaging.ImageCodecInfo[] codecs = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();

            for (int i = 0; i < codecs.Length; i++)
            {
                if (codecs[i].MimeType.Equals(mimeType))
                {
                    return codecs[i];
                }
            }

            return null;
        }


        /// <summary>
        /// EncryptImage
        /// </summary>
        public static bool EncryptImageTest(string fileName, string newFileName)
        {
            // Create a string to encrypt.
            string FileNameToEncrypt = "\\\\10.11.14.8\\nfs\\DevContentStore\\test.jpg";
            string FileName = "\\\\10.11.14.8\\nfs\\DevContentStore\\test.dat";

            // Encrypt text to a file using the file name, key, and IV.
            return EncryptTextureToFile(FileNameToEncrypt, FileName);
        }

        /// <summary>
        /// EncryptTextureToFile
        /// </summary>
        public static bool EncryptTextureToFile(String FileNameToEncrypt, String FileNameOutput)
        {
            bool bResult = true;

            try
            {
                // Create a new Rijndael object.
                Rijndael RijndaelAlg = Rijndael.Create();

                RijndaelAlg.BlockSize = 128;
                RijndaelAlg.KeySize = 128;
                RijndaelAlg.Padding = PaddingMode.Zeros;
                RijndaelAlg.Mode = CipherMode.CBC;
                RijndaelAlg.IV = new byte[] { 0x0, 0x0, 0x0, 0x0, 
                    0x0,0x0,0x0,0x0,
                    0x0,0x0,0x0,0x0,
                    0x0,0x0,0x0,0x0};

                string key = "51ubB3rD3GuLL!0n";
                RijndaelAlg.Key = Encoding.ASCII.GetBytes(key);


                if (Configuration.UseAmazonS3Storage)
                {
                    string originalImage = (Configuration.TextureServer + "/" + FileNameToEncrypt).Replace("\\", "/");

                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(originalImage);
                    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    System.IO.Stream sIn = httpWebResponse.GetResponseStream();

                    using (MemoryStream fiStream = new MemoryStream())
                    {
                        AmazonS3Client azS3client = GetAmazonS3Client();
                        TransferUtility fileTransferUtility = new TransferUtility(azS3client);

                        //FreeImage.SaveToStream(bmp, fiStream, FreeImageAPI.FREE_IMAGE_FORMAT.FIF_BMP, (FreeImageAPI.FREE_IMAGE_SAVE_FLAGS)quality); // FreeImage supports raw quality number (0-100) for JPEG
                        // Create a CryptoStream using the FileStream and the passed key and initialization vector (IV).
                        CryptoStream cStream = new CryptoStream(fiStream, RijndaelAlg.CreateEncryptor(), CryptoStreamMode.Write);

                        try
                        {
                            int size = 256;
                            byte[] bytes = new byte[size];
                            int numBytes;

                            while ((numBytes = sIn.Read(bytes, 0, size)) > 0)
                            {
                                cStream.Write(bytes, 0, numBytes);
                            }

                            // Upload data from a type of System.IO.Stream.
                            fileTransferUtility.Upload(fiStream, Configuration.AmazonS3Bucket, FileNameOutput.Replace("\\", "/"));
                        }
                        catch (Exception e)
                        {
                            bResult = false;
                            m_logger.Error("An error error occurred", e);
                        }
                        finally
                        {
                            if (sIn != null)
                            {
                                sIn.Dispose();
                            }

                            // Close the streams and
                            cStream.Close();
                        }



                    }


                }
                else
                {
                    // Read file to be encrypted
                    FileStream fsIn = new FileStream(FileNameToEncrypt, FileMode.Open, FileAccess.Read);

                    // Create or open the specified file.
                    FileStream fStream = File.Open(FileNameOutput, FileMode.OpenOrCreate);



                    // Create a CryptoStream using the FileStream and the passed key and initialization vector (IV).
                    CryptoStream cStream = new CryptoStream(fStream, RijndaelAlg.CreateEncryptor(), CryptoStreamMode.Write);

                    try
                    {
                        int size = 256;
                        byte[] bytes = new byte[size];
                        int numBytes;

                        while ((numBytes = fsIn.Read(bytes, 0, size)) > 0)
                        {
                            cStream.Write(bytes, 0, numBytes);
                        }
                    }
                    catch (Exception e)
                    {
                        bResult = false;
                        m_logger.Error("An error error occurred", e);
                    }
                    finally
                    {
                        if (fsIn != null)
                        {
                            fsIn.Dispose();
                        }

                        // Close the streams and
                        cStream.Close();
                        fStream.Close();
                    }
                }
            }
            catch (CryptographicException e)
            {
                bResult = false;
                m_logger.Error("A Cryptographic error occurred", e);
            }
            catch (UnauthorizedAccessException e)
            {
                bResult = false;
                m_logger.Error("A file access error occurred", e);
            }
            catch (Exception e)
            {
                bResult = false;
                m_logger.Error("A general error encrypt error occurred", e);
            }

            return bResult;
        }

        /// <summary>
        /// DencryptTextureToFile
        /// </summary>
        public static bool DencryptTextureToFile(String FileNameToEncrypt, String FileNameOutput)
        {
            bool bResult = true;

            try
            {
                // Read file to be encrypted
                FileStream fsIn = new FileStream(FileNameToEncrypt, FileMode.Open, FileAccess.Read);

                // Create or open the specified file.
                FileStream fStream = File.Open(FileNameOutput, FileMode.OpenOrCreate);

                // Create a new Rijndael object.
                Rijndael RijndaelAlg = Rijndael.Create();

                RijndaelAlg.BlockSize = 128;
                RijndaelAlg.KeySize = 128;
                RijndaelAlg.Padding = PaddingMode.Zeros;
                RijndaelAlg.Mode = CipherMode.CBC;
                RijndaelAlg.IV = new byte[] { 0x0, 0x0, 0x0, 0x0, 
                    0x0,0x0,0x0,0x0,
                    0x0,0x0,0x0,0x0,
                    0x0,0x0,0x0,0x0};

                string key = "51ubB3rD3GuLL!0n";
                RijndaelAlg.Key = Encoding.ASCII.GetBytes(key);

                // Create a CryptoStream using the FileStream and the passed key and initialization vector (IV).
                CryptoStream cStream = new CryptoStream(fStream, RijndaelAlg.CreateDecryptor(), CryptoStreamMode.Write);

                try
                {
                    int size = 256;
                    byte[] bytes = new byte[size];
                    int numBytes;

                    while ((numBytes = fsIn.Read(bytes, 0, size)) > 0)
                    {
                        cStream.Write(bytes, 0, numBytes);
                    }
                }
                catch (Exception e)
                {
                    bResult = false;
                    m_logger.Error("An error error occurred", e);
                }
                finally
                {
                    if (fsIn != null)
                    {
                        fsIn.Dispose();
                    }

                    // Close the streams and
                    cStream.Close();
                    fStream.Close();
                }
            }
            catch (CryptographicException e)
            {
                bResult = false;
                m_logger.Error("A Cryptographic error occurred", e);
            }
            catch (UnauthorizedAccessException e)
            {
                bResult = false;
                m_logger.Error("A file access error occurred", e);
            }
            catch (Exception e)
            {
                bResult = false;
                m_logger.Error("A general error encrypt error occurred", e);
            }

            return bResult;
        }


        public static bool GenerateJPGTexturesFromDDS(string fileName, Stream stream, IDictionary<int, string> JPGFullPaths)
        {
            FIBITMAP bmp = FreeImage.LoadFromStream(stream);
            if (bmp.IsNull)
            {
                m_logger.Error("GenerateJPGTexturesFromDDS: Loading DDS from " + fileName + " failed.");
                return false;
            }

            uint width = FreeImage.GetWidth(bmp);
            uint height = FreeImage.GetHeight(bmp);
            m_logger.Debug("GenerateJPGTexturesFromDDS: " + fileName + " " + width.ToString() + " x " + height.ToString());

            try
            {
                foreach (KeyValuePair<int, string> keyVal in JPGFullPaths)
                {
                    int quality = keyVal.Key;
                    string outFileName = keyVal.Value;

                    if (Configuration.UseAmazonS3Storage)
                    {
                        try
                        {
                            AmazonS3Client azS3client = GetAmazonS3Client();
                            TransferUtility fileTransferUtility = new TransferUtility(azS3client);

                            // Save it to Amazon s3
                            using (MemoryStream fiStream = new MemoryStream())
                            {
                                FreeImage.SaveToStream(bmp, fiStream, FreeImageAPI.FREE_IMAGE_FORMAT.FIF_BMP, (FreeImageAPI.FREE_IMAGE_SAVE_FLAGS)quality); // FreeImage supports raw quality number (0-100) for JPEG

                                // Upload data from a type of System.IO.Stream.
                                fileTransferUtility.Upload(fiStream, Configuration.AmazonS3Bucket, outFileName.Replace("\\", "/"));
                            }

                        }
                        catch (AmazonS3Exception s3Exception)
                        {
                            m_logger.Error("S3 Error " + s3Exception.Message, s3Exception);
                        }
                    }
                    else
                    {
                        FreeImage.SaveEx(bmp, outFileName, (FreeImageAPI.FREE_IMAGE_SAVE_FLAGS)quality); // FreeImage supports raw quality number (0-100) for JPEG
                    }

                    m_logger.Debug("GenerateJPGTexturesFromDDS: saved " + outFileName);
                }
            }
            catch (Exception ex)
            {
                m_logger.Error("GenerateJPGTexturesFromDDS: exception occurred while saving JPG files: " + ex.ToString());
                return false;
            }

            FreeImage.UnloadEx(ref bmp);
            return true;
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    }
}