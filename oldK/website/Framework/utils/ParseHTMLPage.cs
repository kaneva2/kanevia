///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
    public class ParseHTMLPage
    {
        #region Declarations

        private string _parentURL = "";
        private string _parentDomain = "";
        private string _result = "";
        private string _images = "";
        private string _movies = "";
        private string _links = "";
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Constructors
        /// <summary>
        /// basic constructor doesnt intialize anything
        /// </summary>
        public ParseHTMLPage()
        {
        }

        /// <summary>
        /// constructor that intializes variables and auto processes the page
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="OrgDomain"></param>
        public ParseHTMLPage(string URL, string OrgDomain)
        {
            _parentURL = URL;
            _parentDomain = OrgDomain;

            //recreate source page
            ParseWebPage();
        }

        #endregion

        #region Parsing Functions

        public void ParseWebPage()
        {
            WebResponse objResponse = null;
            try
            {
                if (_parentURL != "")
                {
                    WebRequest objRequest = System.Net.HttpWebRequest.Create(_parentURL);
                    objResponse = objRequest.GetResponse();
                    using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                    {
                        _result = sr.ReadToEnd();
                        // Close and clean up the StreamReader
                        sr.Close();
                    }
                    objResponse.Close();

                    //clean up standardize all tag names and attriubutes
                    _result = Standardize(_result);
                    //repair any link that are relative path
                    if (_parentDomain != "")
                    {
                        _result = ReconstructURL(_result);
                    }
                    //get all image srcs
                    _images = FindAllImages(_result);
                    //get all movie srcs
                    _movies = FindAllMovies_Widgets(_result);
                    //get all link src
                    _links = FindAllLinks(_result);
                }
            }
            catch (Exception ex)
            {
                m_logger.Error("Generation and parsing of web page failed - " + ex);
            }
            finally
            {
                if (objResponse != null)
                {
                    objResponse.Close();
                }
            }

        }

        private string RemoveHeaderScripts(string webPageCode)
        {
            //replace
            webPageCode = webPageCode.Replace("<HEAD>", "<head>");
            webPageCode = webPageCode.Replace("</HEAD>", "</head>");
            webPageCode = webPageCode.Replace("<TITLE>", "<title>");
            webPageCode = webPageCode.Replace("</TITLE>", "</title>");

            int headSTART = webPageCode.IndexOf("<head>");
            int titleSTART = webPageCode.IndexOf("<title>");
            webPageCode = webPageCode.Replace(webPageCode.Substring(headSTART + 6, titleSTART - (headSTART + 6)), "");

            int headEND = webPageCode.IndexOf("</head>");
            int titleEND = webPageCode.IndexOf("</title>");
            webPageCode = webPageCode.Replace(webPageCode.Substring(titleEND + 8, headEND - (titleEND + 8)), "");

            return webPageCode;
        }

        //makes an upper case tags or attributes lower case
        private string Standardize(string webPageCode)
        {
            webPageCode = webPageCode.Replace("<IMG ", "<img ");
            webPageCode = webPageCode.Replace("</IMG", "</img");
            webPageCode = webPageCode.Replace("<A ", "<a ");
            webPageCode = webPageCode.Replace("</A", "</a");
            webPageCode = webPageCode.Replace("HREF", "href");
            webPageCode = webPageCode.Replace("<EMBED ", "<embed ");
            webPageCode = webPageCode.Replace("<OBJECT ", "<object ");
            webPageCode = webPageCode.Replace("</EMBED>", "</embed>");
            webPageCode = webPageCode.Replace("</OBJECT>", "</object>");
            webPageCode = webPageCode.Replace("SRC=", "src=");
            webPageCode = webPageCode.Replace("NAME=", "name=");
            webPageCode = webPageCode.Replace("MOVIE", "movie");
            webPageCode = webPageCode.Replace("HTTP", "http");
            webPageCode = webPageCode.Replace(".SWF", ".swf");

            return webPageCode;
        }

        //parses page to 
        private string FindAllImages(string webPageCode)
        {
            string images = "";

            int cursorPosition = 0;
            while (cursorPosition < webPageCode.Length)
            {
                int imgSTART = webPageCode.IndexOf("<img ", cursorPosition);
                int imgEND = 0;
                if (imgSTART >= 0)
                {
                    cursorPosition = imgSTART;

                    //this is a check to make sure a closing bracket and new <img isnt encountered
                    //before the </img> ia found
                    int closeImg = webPageCode.IndexOf(">", cursorPosition);
                    int nextImg = webPageCode.IndexOf("<img ", closeImg);
                    int closeTag = webPageCode.IndexOf("</img>", cursorPosition);

                    if ((closeTag != -1) && (closeTag < nextImg))
                    {
                        imgEND = closeTag + 6;
                    }
                    else
                    {
                        imgEND = closeImg + 1;
                    }

                    //find the src of the image
                    int srcSTART = -1;
                    int srcEND = -1;
                    srcSTART = webPageCode.IndexOf("src=", cursorPosition);
                    if (srcSTART != -1)
                    {
                        //account for moving cursor past src tag
                        if (webPageCode[srcSTART + 4].Equals('"'))
                        {
                            srcSTART += 5;
                        }
                        else
                        {
                            srcSTART += 4;
                        }

                        srcEND = webPageCode.IndexOf("\"", srcSTART);

                        //assign the image to the container
                        if (srcEND != -1)
                        {
                            images += webPageCode.Substring(srcSTART, srcEND - srcSTART).Replace("\"", "") + "|";
                        }
                    }
                   //images += webPageCode.Substring(imgSTART, imgEND - imgSTART) + "|";
                   
                    //move cursor
                    cursorPosition = imgEND;
                }
                else
                {
                    cursorPosition = webPageCode.Length;
                }

            }
            return images.Length > 0 ? images.Substring(0,images.Length -1): "";
        }

        private string FindAllLinks(string webPageCode)
        {
            string links = "";

            int cursorPosition = 0;
            while (cursorPosition < webPageCode.Length)
            {
                int linkSTART = webPageCode.IndexOf("<a ", cursorPosition);
                int linkEND = 0;
                if (linkSTART >= 0)
                {
                    cursorPosition = linkSTART;

                    //this is a check to make sure a closing bracket and new <a isnt encountered
                    //before the </a> ia found
                    int closeLink = webPageCode.IndexOf(">", cursorPosition);
                    int nextLink = webPageCode.IndexOf("<a ", closeLink);
                    int closeTag = webPageCode.IndexOf("</a>", cursorPosition);

                    if ((closeTag != -1) && (closeTag < nextLink))
                    {
                        linkEND = closeTag + 4;
                    }
                    else
                    {
                        linkEND = closeLink + 1;
                    }

                    //assign the link to the container
                    //find the href of the link
                    int hrefSTART = -1;
                    int hrefEND = -1;
                    hrefSTART = webPageCode.IndexOf("href=", cursorPosition);
                    if (hrefSTART != -1)
                    {
                        //account for moving cursor past src tag
                        if (webPageCode[hrefSTART + 5].Equals('"'))
                        {
                            hrefSTART += 6;
                        }
                        else
                        {
                            hrefSTART += 5;
                        }

                        hrefEND = webPageCode.IndexOf("\"", hrefSTART);

                        //assign the image to the container
                        if (hrefEND != -1)
                        {
                            links += webPageCode.Substring(hrefSTART, hrefEND - hrefSTART).Replace("\"", "") + "|";
                        }
                    }
                    
                    //links += webPageCode.Substring(linkSTART, linkEND - linkSTART) + "|";
                    //move cursor
                    cursorPosition = linkEND;
                }
                else
                {
                    cursorPosition = webPageCode.Length;
                }

            }
            return links.Length > 0 ? links.Substring(0, links.Length - 1) : "";
        }

        private string FindAllMovies_Widgets(string webPageCode)
        {
            string movies = "";

            int cursorPosition = 0;
            while (cursorPosition < webPageCode.Length)
            {
                int embedindex = webPageCode.IndexOf("<embed", cursorPosition);
                int objectindex = webPageCode.IndexOf("<object", cursorPosition);
                int endindex = -1;
                int startindex = -1;

                //find either the embed tag and src location or the object tag and movie location
                if (embedindex >= 0)
                {
                    startindex = embedindex;
                    endindex = webPageCode.IndexOf("</embed>", embedindex);
                    if (endindex != -1)
                    {
                        endindex += 8;
                    }
                }
                else if (objectindex >= 0)
                {
                    startindex = objectindex;
                    endindex = webPageCode.IndexOf("</object>", objectindex);
                    if (endindex != -1)
                    {
                        endindex += 9;
                    }
                }

                //if a source was found continue processing
                if (endindex > 0)
                {
                    movies += webPageCode.Substring(startindex, (endindex - startindex)) + "|";
                }

                //cursor placement
                //if embed index is larger you want to start next position there
                if ((startindex >= 0) && (endindex >=0))
                {
                    cursorPosition = endindex;
                }
                else if (startindex < 0)
                {
                    cursorPosition = webPageCode.Length;
                }
                else
                {
                    cursorPosition = startindex + 1;
                }

            }
            return movies.Length > 0 ? movies.Substring(0, movies.Length - 1) : "";
        }


        //repair any link that are relative path
        private string ReconstructURL(string webPageCode)
        {
            //replace image links
            webPageCode = webPageCode.Replace("src=\"../", "src=\"http://" + _parentDomain + "/");
            webPageCode = webPageCode.Replace("src=\"/", "src=\"http://" + _parentDomain + "/");

            int i = 0;
            while (i < webPageCode.Length)
            {
                int position1 = webPageCode.IndexOf("src=\"", i);

                if (position1 != -1)
                {
                    string item1 = webPageCode.Substring(position1, 9);
                    if (item1 != "src=\"http")
                    {
                        string original = item1.Substring(0, 6);
                        string newSRC = "src=\"http://" + _parentDomain + "/" + original.Substring(5, 1);
                        webPageCode = webPageCode.Replace(original, newSRC);
                    }

                    //advance cursor position
                    i = position1 + 1;
                }
                else
                {
                    i = webPageCode.Length;
                }
            }

            //replace hyperlinks 
            webPageCode = webPageCode.Replace("href=\"../", "href=\"http://" + _parentDomain + "/");
            webPageCode = webPageCode.Replace("href=\"/", "href=\"http://" + _parentDomain + "/");
            webPageCode = webPageCode.Replace("localhost", _parentDomain);

            int j = 0;
            while (j < webPageCode.Length)
            {
               int position2 = webPageCode.IndexOf("href=\"", j);
               if (position2 != -1)
                {
                    string item2 = webPageCode.Substring(position2, 10);
                    if (item2 != "href=\"http")
                    {
                        string original = item2.Substring(0, 7);
                        string newSRC = "href=\"http://" + _parentDomain + "/" + original.Substring(6, 1);
                        webPageCode = webPageCode.Replace(original, newSRC);
                    }

                    //advance cursor position
                    j = position2 + 1;
                }
                else
                {
                    j = webPageCode.Length;
                }

            }
            return webPageCode;
        }


        #endregion

        #region Accessor Functions

        public string Getlinks()
        {
            return _links;
        }

        public string GetMovies()
        {
            return _movies;
        }

        public string GetPhotos()
        {
            return _images;
        }

        #endregion
    }
}
