///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

/*------------------------------------------------------------------
 All code written by Peter L. Blum. 
 Copyright 2003 Peter L. Blum, All Rights Reserved 

 Please visit www.Kaneva.com for communications with the programmer.
 PLEASE READ THE LICENSE AGREEMENT INCLUDED WITH THIS PRODUCT FOR IMPORTANT
 RESTRICTIONS IN USING THE SOURCE CODE.
 This source code cannot be distributed without the express written consent of Peter L. Blum.
 
 See the file "Documentation.pdf" that came with this product for extensive documentation.
 This file's documentation is intended for developers of the source code to build the
 project and extend its code.
 

 ---- OVERVIEW -----------------------------------------------------
 This code is a substitute for the Page methods: IsClientScriptBlockRegistered,
 IsStartupScriptRegistered, RegisterClientScriptBlock and RegisterStartupScript.
 These methods have a bug. As you add to the ClientScriptBlock and 
 StartupScriptBlock, the entries are added to the page in the order they are added.
 This is predictable and useful behavior, especially when one initialization
 script runs and is dependent on another.
 Microsoft's functions use a System.Collections.Specialized.HybridDictionary
 which changes from a ListDictionary to a HashTable at a certain point. In this
 case, they switch at 9. Once in a Hashtable, the ordering is lost and the scripts
 are written to the page in a random order.
 
 When you write javascripts out, sometimes one is dependent on another during the initialization
 process of a page. For example, you might write a global variable with one call
 to RegisterClientScriptBlock or RegisterStartupScript followed by another call to 
 a function that initializes the global. You depend on the order.
 Even if you only use two calls to these methods, when they are on a page,
 other controls and code will add calls to the same methods. Its predictable that at some
 point, the combination of all calls to these methods will exceed 8 calls and your code
 will break.
 
 This set of classes delivers a tool that initially emulates the four Page methods
 mentioned above without being affected by the bug. It can be used on a page where other 
 controls and code continues to use Microsoft�s methods In fact,
 it eventually uses RegisterClientScriptBlock and RegisterStartupScript to write to the page.
 However, it only makes one call, after formatting all your registered scripts into a single
 string.
 
 It provides two important enhancements through overloaded methods:
 * The user can optionally have the start and end script tags added. This means that:
    - The user doesn't have to declare them in the script passed in. 
    - It simplifies their code to just the javascript
    - It limits errors when the user defines the tags (such as forgetting to put <!-- //--> tags around it)
    - They will be defined consistently for all usages
    - All back-to-back calls to RegisterXXXScript that request this feature will
      be enclosed in a common set of script tags. It reduces the page size and makes the code more readable.
 * Allows defining groups to control the ordering. By default, all entries are ordered
   in the order they were added. The user can provide grouping by giving a numeric position.
   For instance, all scripts with the group number 1 will be output together and appear below
   group 0 (the default group). With this, the user can be very precise about the order.
 
 Design
 ------
 1. The class RegisterScripts is used to maintain entries in a sorted order for both
    ClientScriptBlock and StartupScriptBlock.
 2. Entries into RegisterScripts use the RegisteredScript class which has several properties,
    Name, script (both strings), AddScriptTag (bool), and Group (int).
 3. RegisterScripts contains a second list, Keys, sorted by the RegisteredScript.Names property.
    It is used for checking for existing items.
 4. Methods on RegisterScripts include: IsRegistered and Register which parallel their
    counterparts on the Page class.
 5. HttpContent.Current.Items["<SystemName>_ClientScripts"] holds the ClientScriptBlock ArrayList.
 6. HttpContent.Current.Items["<SystemName>_StartupScripts"] holds the StartupScriptBlock ArrayList.
 7. When the Page.OnPreRender method has run, it's time to convert these lists into 
    Page.RegisterClientScriptBlock and Page.RegisterStartupScript calls (one for each list).
    Since there is no event handler on the Page to hookup that gets called between OnPreRender and Render,
    we use a hack. The first call to IsRegistered or Registrer will add an OnPreRender event handler 
    to the last control on the Page's control list (which is usually a LiteralControl containing </body></html>)
    This event handler will call the static method TransferToPage.
  
---- USING THIS SYSTEM -------------------------------------------
At the simplest level, substitute calls to these Page class methods:
IsClientScriptBlockRegister, IsStartupScriptRegistered
RegisterClientScriptBlock, RegisterStartupScript
with the same static methods in RegisterScripts.
The methods in RegisterScripts always take the Page as the first parameter.
Otherwise, they offer the same parameters.

ORIGINAL:
  if (!Page.IsClientScriptBlock("key"))
     Page.RegisterClientScriptBlock("key", "<script language='javascript'>script();</script>");
  if (!Page.IsStartupScriptRegistered("key2"))
     Page.RegisterStartupScript("key2", "<script language='javascript'>script2();</script>");
NEW:
  if (!RegisterScripts.IsClientScriptBlockRegistered(Page, "key"))
     RegisterScripts.RegisterClientScriptBlock(Page, "key", "<script language='javascript'>script();</script>");
  if (!RegisterScripts.IsStartupScriptRegistered(Page, "key2"))
     RegisterScripts.RegisterStartupScript(Page, "key2", "<script language='javascript'>script2();</script>");

You can allow the scripts to omit the <script> tags so that you only pass the script code.
Yet, your code still needs to be enclosed in <script> tags. To automatically assign those
tags, RegisterClientScriptBlock and RegisterStartupScript have been overloaded to take an additional
parameter, AddScriptTag, which you set to true:

  if (!RegisterScripts.IsClientScriptBlockRegistered(Page, "key"))
     RegisterScripts.RegisterClientScriptBlock(Page, "key", "script();", true);
  if (!RegisterScripts.IsStartupScriptRegistered(Page, "key2"))
     RegisterScripts.RegisterStartupScript(Page, "key2", "script2();", true);

Scripts are ordered in entry order by default. You can group scripts so that one or 
more always appear above or below others. Groups are identified by a number where
0 is the default group. The lower the number, the higher it appears on the page.
All items that share a group number will be ordered in entry order.

The previously shown RegisterClientScriptBlock and RegisterStartupScript methods don't take
a group number and assign the default group (0).
To assign group numbers, RegisterClientScriptBlock and RegisterStartupScript have been overloaded
with the group number as the last parameter. (These methods also have the AddScriptTag parameter.)

  if (!RegisterScripts.IsClientScriptBlockRegistered(Page, "key"))
     RegisterScripts.RegisterClientScriptBlock(Page, "key", "script();", true, 5);
  if (!RegisterScripts.IsStartupScriptRegistered(Page, "key2"))
     RegisterScripts.RegisterStartupScript(Page, "key2", "script2();", true, 10);

You can customize the behavior of this system with a number of static properties
on the RegisterScripts class. You should assign them in your Application_Start method (Global.aspx).
The properties are:
* Enabled
* ScriptSeparator,
* ScriptScriptTag
* EndScriptTag
* DefaultGroup
See their descriptions in the code below.

--- CREATING A PROJECT -------------------------------------------
If you do not have a project file, here's how to build it:
1. You need the following files.
   RegisterScripts.cs - this file
   CollectionsLibrary.cs - available from Kaneva.com as AutoSortArrayList.
     You will need to edit its namespace to be Kaneva.CollectionsLibrary.
2. Create a C# Class Library. Remove the default Class1.cs.
3. Add the two files (shown above)
4. Add a reference to System.Web.
5. Define the assembly name to be unique so that it doesn't conflict with
   the original Assembly, Kaneva.RegisterScripts.
6. Change the namespace of both source files from "PeterBlum.xxx" to 
   "yournamespace.xxx" to avoid conflicts with other assemblies based on this code.
This is a copyrighted program. Please respect the copyrights.
------------------------------------------------------------------*/

using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Reflection;
using Kaneva.CollectionsLibrary;

namespace Kaneva.RegisterScriptsClasses
{
// ---- CLASS RegisterScripts -------------------------------------------------------
/// <remarks>
/// RegisterScripts is a collection of RegisteredScript objects. Users
/// add to the collection by calling IsRegistered to see if the item exists
/// and Register to add.
/// It contains a number of static methods to operate on two instances of this class:
/// for ClientScriptBlocks and StartupScriptBlocks.
/// Most of the time, the user should call the static methods:
/// IsClientScriptBlockRegistered, IsStartupScriptRegistered
/// RegisterClientScriptBlock, RegisterStartupScript.
/// </remarks>
   public class RegisterScripts : AutoSortArrayList
   {
/// <summary>
/// Enabled determines if scripts are collected or transferred to the page immediately.
/// When true, they are collected until the page has prerendered its controls.
/// When false, they are transferred to the page right away.
/// This is kind of an on/off switch so you can write your code one way
/// and turn off the added layer as needed.
/// Since it is static, it affects all threads running this object. So don't turn it on/off
/// for each page being generated. Only set it during Application_Start
/// Defaults to true.
/// </summary>
      public static bool Enabled 
      {
         get { return fEnabled; }
         set { fEnabled = value; }
      }
      protected static bool fEnabled = true;

/// <summary>
/// HttpContext.Current.Items holds several elements associated with the page.
/// The strings for these must be unique. SystemName defines a prefix to the names
/// we use. It can generally remain unchanged unless you want to group information
/// separately between different systems. (Such as two separate control products that
/// don't prefer to intermix their code.)
/// </summary>
      public static string SystemName
      {
         get { return fSystemName; }
         set { fSystemName = value; }
      }
      protected static string fSystemName = "PBDC";

/// <summary>
/// ScriptSeparator determines the text written after each script to provide additional formatting.
/// It defaults to a carriage return.
/// </summary>
      public static string ScriptSeparator
      {
         get { return fScriptSeparator; }
         set { fScriptSeparator = value; }
      }
      protected static string fScriptSeparator = "\n";

/// <summary>
/// When using the AddScriptTag property on RegisteredScript, these are the strings
/// written to the page for the start and end tag. They are separated out so you can
/// enhance them. Perhaps you'd prefer to write the type=text/javascript tag instead of language=.
/// </summary>
      public static string StartScriptTag
      {
         get { return fStartScriptTag; }
         set { fStartScriptTag = value; }
      }
      protected static string fStartScriptTag = "<script language='JavaScript'>\n<!--\n";

      public static string EndScriptTag
      {
         get { return fEndScriptTag; }
         set { fEndScriptTag = value; }
      }
      protected static string fEndScriptTag = "// -->\n</script>\n";

/// <summary>
/// fDefaultGroup is the value for the default group, which is used when the user doesn't
/// supply a group.
/// </summary>
      public static int DefaultGroup
      {
         get { return fDefaultGroup; }
         set { fDefaultGroup = value; }
      }
      protected static int fDefaultGroup = 0;


      public RegisterScripts() : base()
      {
         xIComparer = new RegisterScriptGroupOrder();
      }

// ---- PROPERTIES ------------------------------------------------------
/// <summary>
/// Keys maintains the same list of RegisteredScripts as the RegisterScripts itself
/// but they are kept in sorted order by the Key field. Used for lookups on IsRegistered.
/// </summary>
      public AutoSortArrayList Keys
      {
         get 
         {
            if (fAutoSortArrayList == null)
               fAutoSortArrayList = new AutoSortArrayList();
            return fAutoSortArrayList;
         }
      }
      protected AutoSortArrayList fAutoSortArrayList = null;

/// <summary>
/// fNextOrder provides a sequential ordering of RegisteredScripts. Each time
/// a RegisteredScript instance is added to the list, increment this and use its
/// value in the RegisteredScript.Order property.
/// </summary>
      protected int fNextOrder = 0;

// ---- METHODS ---------------------------------------------------------
/// <summary>
/// IsRegistered tests the Key passed in to determine if that key is already registered.
/// If not, you can call Register.
/// Returns true when already registered.
/// </summary>
/// <param name="pKey">The key name.</param>
/// <returns></returns>
      public virtual bool IsRegistered(string pKey)
      {
         if (pKey.Trim().Length == 0)
            throw new ArgumentException("The Key is an empty string. It must be assigned some text.");
         return Keys.Contains(new RegisteredScript(pKey));
      }  // IsRegistered

/// <summary>
/// Adds the Script with the specified Key into the collection.
/// Any script blocks with the same pKey parameter values are considered duplicates.
/// Call IsRegistered first and call this only when that method returns false.
/// to avoid unnecessarily running code that creates the scripts.
/// </summary>
/// <param name="pKey">The Key name.</param>
/// <param name="pScript">The script.</param>
/// <param name="pAddScriptTags">When true, the user wants script tags to be added automatically.</param>
/// <param name="pGroup">Provide additional ordering. The higher then number, the lower the script appears on the page.
/// The default group is 0.</param>
      public virtual void Register(string pKey, string pScript, bool pAddScriptTags, int pGroup)
      {
         if (!IsRegistered(pKey))
         {
            RegisteredScript vRS = new RegisteredScript(pKey, pScript, pAddScriptTags, pGroup,
               ++fNextOrder);
            Keys.Add(vRS);
            Add(vRS);
         }
      }  // Register

      public override int Add(object pItem)
      {
         if (pItem is RegisteredScript)
            return base.Add(pItem);
         else
            throw new ArgumentException("The Item must be a RegisteredScript instance.");
      }  // Add

/// <summary>
/// WriteScripts asks this instance to transfer its scripts into pScriptBlock
/// in the order they were added. No formatting is added except a carriage return
/// if the last character of a script is not one. All formatting comes from the user.
/// </summary>
/// <param name="pScriptBlock"></param>
      public virtual void WriteScripts(StringBuilder pScriptBlock)
      {
         bool vNeedsScriptTags = false;
         for (int vI = 0; vI < Count; vI++)
         {
            RegisteredScript vRS = (RegisteredScript) this[vI];

            if (!vNeedsScriptTags && vRS.AddScriptTags)  // start script tags
               pScriptBlock.Append(fStartScriptTag);
            else if (vNeedsScriptTags && !vRS.AddScriptTags)   // end script tags
               pScriptBlock.Append(fEndScriptTag);
            vNeedsScriptTags = vRS.AddScriptTags;

            pScriptBlock.Append(vRS.Script);
            if (vRS.Script.Length > 0) 
            {
               char vLast = vRS.Script[vRS.Script.Length - 1];
               if ((vLast != '\n') && (vLast != '\r'))
                  pScriptBlock.Append("\n");
            }
         }  // for
         if (vNeedsScriptTags)
            pScriptBlock.Append(fEndScriptTag);
      }  // WriteScripts

// ---- STATIC METHODS ------------------------------------------------------
/// <summary>
/// GetClientScripts gets the instance of this class for ClientScriptBlocks.
/// It keeps the collection in HttpContext.Current.Items["[SystemName]_ClientScripts"]
/// When first used, the collection is created and the LinkToPage method is called.
/// </summary>
/// <returns></returns>
      public static RegisterScripts GetClientScripts(Page pPage)
      {
         RegisterScripts vRS = (RegisterScripts) HttpContext.Current.Items[fSystemName + "_ClientScripts"];
         if (vRS == null)
         {
            vRS = new RegisterScripts();
            HttpContext.Current.Items.Add(fSystemName + "_ClientScripts", vRS);
            LinkToPage(pPage);
         }
         return vRS;
      }  // GetClientScripts

/// <summary>
/// GetStartupScripts gets the instance of this class for StartupScriptBlocks.
/// It keeps the collection in HttpContext.Current.Items["[systemname]_StartupScripts"]
/// When first used, the collection is created and the LinkToPage method is called.
/// </summary>
/// <returns></returns>
      public static RegisterScripts GetStartupScripts(Page pPage)
      {
         RegisterScripts vRS = (RegisterScripts) HttpContext.Current.Items[fSystemName + "_StartupScripts"];
         if (vRS == null)
         {
            vRS = new RegisterScripts();
            HttpContext.Current.Items.Add(fSystemName + "_StartupScripts", vRS);
            LinkToPage(pPage);
         }
         return vRS;
      }  // GetStartupScripts

/// <summary>
/// LinkToPage hooks up the TransferToPage event handler to the last control
/// on the page's Control list's OnPreRender event. This assures us that TransferToPage
/// is run during OnPreRender but after all controls WITHIN the <form> have run.
/// May be called multiple times. Uses HttpContext.Current.Items["[systemname]_ScriptsLinked"]
/// to avoid multiple initializations.
/// WARNING: This system has a flaw because it depends on a hack to get around the fact
/// that Microsoft didn't provide an event between PreRender and Render. 
/// It depends on the structure of controls already on the page to find the last control 
/// whose OnPreRender event it can use.
/// Unfortunately, the last control can be the control whose OnPreRender event is calling
/// the Register method or one of its child controls AFTER its own OnPreRender calls the
/// ancestor (meaning the child controls have had their PreRender event run.)
/// This case will only happen when the closing </form> tag has no static text before it.
/// For example, if the control <myns:control> calls Register in its OnPreRender, if it
/// appears on the page like this, it will cause this problem:
/// <myns:control id=c1 runat=server /></form>
/// But it will work with just one space between the two tags:
/// <myns:control id=c1 runat=server /> </form>
/// This is because the space forms a LiteralControl and it becomes the last control within the form.
/// The case of no spaces or other formatting before the form tag is rare and easy to fix.
/// </summary>
      public static void LinkToPage(Page pPage)
      {
         if (HttpContext.Current.Items[fSystemName + "_ScriptsLinked"] == null)
         {
            if (pPage.Controls.Count == 0)
               throw new ArgumentException("The Page must have a <form> tag to be used with the RegisterScripts system.");
/*
            Control vLastControl = pPage.Controls[pPage.Controls.Count - 1];  // must be at least one if our controls are on the page.

            // if it's not the closing page tags, first attempt to add a PlaceHolder to the page's Control
            // list. This takes a hack which demands ReflectionSecurity isn't disabled.
            // If it is, search for the last child node of the tree.
            if (!(vLastControl is LiteralControl)) 
            {
               vLastControl = null;
               try
               {
                  // set the pPage.Controls.ReadOnly property. This requires setting
                  // the private field _readOnlyErrorMsg to String.Empty
                  FieldInfo vFieldInfo = typeof(ControlCollection).GetField("_readOnlyErrorMsg",BindingFlags.Public|BindingFlags.Instance|BindingFlags.NonPublic);
                  vFieldInfo.SetValue(pPage.Controls, null); // will throw exception when ReflectionSecurity prevents this

                  vLastControl = new PlaceHolder();
                  pPage.Controls.Add(vLastControl);
               }
               catch (Exception e)
               {
                  // eat the ReflectionSecurity error. Simply let the FindLastControl method do the job
                  vLastControl = null;
               }
            }
            if (vLastControl == null)
               vLastControl = FindLastControl(pPage, false);
*/
            Control vLastControl = FindLastControl(pPage, false);
            vLastControl.PreRender += new EventHandler(TransferToPage);
            HttpContext.Current.Items.Add(fSystemName + "_ScriptsLinked", "");
         }
      }  // LinkToPage

/// <summary>
/// This recursive method finds the last control on the page that isn't an HtmlForm.
/// We hope to find one after HtmlForm in the same Controls list. This often happens
/// where a LiteralControl containing "</body></html>" is after the form. However,
/// when the header has scripting or asp tags (<% %>), there is no LiteralControl.
/// If the <body> tag has runat=server, it may be the last on the page and we cannot attach
/// to that since its PreRender is run FIRST.
/// Rule: We cannot use the PreRender event of the HtmlForm or any controls list with
/// only one control unless its below the HtmlForm or on the HtmlGenericControl with tag = "body".
/// </summary>
/// <param name="pParent"></param>
/// <returns></returns>
      protected static Control FindLastControl(Control pParent, bool pFormIsParent)
      {
         if (pParent.Controls.Count == 0)
            throw new ArgumentException("The Page lacks any controls between the <form> tags. It cannot be used with the RegisterScripts system.");
         Control vLastControl = pParent.Controls[pParent.Controls.Count - 1];  // must be at least one if our controls are on the page.
         
/* !bf v1.0.1
         if (pFormIsParent || (pParent.Controls.Count > 1))
         {
*/         
            if (vLastControl is HtmlForm)
               return FindLastControl(vLastControl, true);
            else if ((vLastControl is HtmlGenericControl) &&
               (((HtmlGenericControl)vLastControl).TagName.ToUpper() == "BODY"))
               return FindLastControl(vLastControl, false);
            else if (vLastControl.Controls.Count > 0)  // looking for the last node in the tree within the <form> tag
               return FindLastControl(vLastControl, pFormIsParent);
            else  // last node
               return vLastControl;
/* !bf v1.0.1
      }
         else
            return FindLastControl(vLastControl, pFormIsParent || vLastControl is HtmlForm);
*/            
      }  // FindLastControl

/// <summary>
/// PrepareControl is designed for custom controls to call before using the Register methods.
/// If LinkToPage has yet to be called, this will add an extra PlaceHolder in the pControl's
/// child list when there is no LiteralControl at the end of the page. 
/// The effect is to allow LinkToPage to have a child control that runs
/// its PreRender event for TransferToPage after the pControl PreRender event is run.
/// Custom control must call this prior to the Register methods if they intend to benefit from it.
/// </summary>
/// <param name="pControl"></param>
      public static void PrepareControl(Control pControl)
      {
         if (HttpContext.Current.Items[fSystemName + "_ScriptsLinked"] == null)
         {
            // don't modify pControl if we already have something at the very end of the page to use
            if (pControl.Page.Controls[pControl.Page.Controls.Count - 1] is LiteralControl)
               return;

            PlaceHolder vPH = new PlaceHolder();
            pControl.Controls.Add(vPH);
         }
      }  // PrepareRegisterScripts

/// <summary>
/// TransferToPage is called to write the scripts to the page using
/// the Page.RegisterClientScriptBlock and Page.RegisterStartupScript methods.
/// These functions will get ALL scripts in a single call so they maintain their order.
/// When the hack for establishing this method fails to find a control, the web page
/// developer can override their page's OnPreRender method to call this:
/// protected override void OnPreRender(EventArgs pE)
/// {
///   base.OnPreRender(pE);
///   RegisterScripts.TransferToPage(this, pE);
/// }
/// </summary>
/// <param name="pSender">The last control in the page's Controls list.</param>
/// <param name="pE"></param>
      public static void TransferToPage(object pSender, EventArgs pE)
      {
         if (HttpContext.Current.Items[fSystemName + "_ScriptsRan"] == null)
         {
            Page vPage = ((Control)pSender).Page;
            RegisterScripts vRCS = (RegisterScripts) HttpContext.Current.Items[fSystemName + "_ClientScripts"];
            if ((vRCS != null) && (!vPage.ClientScript.IsClientScriptBlockRegistered(vPage.GetType(), fSystemName + "_ClientScripts")))
            {
               StringBuilder vScriptBlock = new StringBuilder(2000);
               vRCS.WriteScripts(vScriptBlock);
               vPage.ClientScript.RegisterClientScriptBlock(vPage.GetType (), fSystemName + "_ClientScripts", vScriptBlock.ToString());
               if (ScriptSeparator.Length > 0)
                  vScriptBlock.Append(ScriptSeparator);
            }

            RegisterScripts vRSS = (RegisterScripts) HttpContext.Current.Items[fSystemName + "_StartupScripts"];
            if ((vRSS != null) && (!vPage.ClientScript.IsStartupScriptRegistered(vPage.GetType(), fSystemName + "_StartupScripts")))
            {
               StringBuilder vScriptBlock = new StringBuilder(2000);
               vRSS.WriteScripts(vScriptBlock);
               vPage.ClientScript.RegisterStartupScript(vPage.GetType(), fSystemName + "_StartupScripts", vScriptBlock.ToString());
               if (ScriptSeparator.Length > 0)
                  vScriptBlock.Append(ScriptSeparator);
            }

            HttpContext.Current.Items.Add(fSystemName + "_ScriptsRan", "");   // a flag. Any Register calls after this are sent directly to the page
         }
      }  // TransferToPage

/// <summary>
/// IsClientScriptBlockRegistered is used to check that a script with the key name
/// specified is not already registered in the ClientScriptBlocks collection. 
/// Returns true when it's already registered. Only use RegisterClientScriptBlock
/// after this returns false.
/// </summary>
/// <param name="pPage">The page instance.</param>
/// <param name="pKey">The key to check.</param>
/// <returns>True when the key is already present. False when it is not. 
/// Only call RegisterClientScriptBlock when this returns false.</returns>
      public static bool IsClientScriptBlockRegistered(Page pPage, string pKey)
      {
         if (Enabled && (HttpContext.Current.Items[fSystemName + "_ScriptsRan"] == null))
         {
            RegisterScripts vRS = GetClientScripts(pPage);
            return vRS.IsRegistered(pKey);   // may throw ArgumentException
         }
         else
            return pPage.ClientScript.IsClientScriptBlockRegistered(pPage.GetType(), pKey);
      }  // IsClientScriptBlockRegistered

/// <summary>
/// RegisterClientScriptBlock adds a script to the ClientScriptBlock collection.
/// Any script blocks with the same pKey parameter values are considered duplicates.
/// Call IsClientScriptBlockRegistered first and call this only when that method returns false.
/// to avoid unnecessarily running code that creates the scripts.
/// </summary>
/// <param name="pPage">The page instance.</param>
/// <param name="pKey">The key to uniquely identify this script.</param>
/// <param name="pScript">The script. It can have any thing valid for an HTML page.
/// It will be added at the top of the <form> block.</param>
      public static void RegisterClientScriptBlock(Page pPage, string pKey, string pScript)
      {
         RegisterClientScriptBlock(pPage, pKey, pScript, false, fDefaultGroup);
      }  // RegisterClientScriptBlock

/// <summary>
/// RegisterClientScriptBlock adds a script to the ClientScriptBlock collection.
/// Any script blocks with the same pKey parameter values are considered duplicates.
/// Call IsClientScriptBlockRegistered first and call this only when that method returns false.
/// to avoid unnecessarily running code that creates the scripts.
/// </summary>
/// <param name="pPage">The page instance.</param>
/// <param name="pKey">The key to uniquely identify this script.</param>
/// <param name="pAddScriptTags">When true, add script tags automatically.</param>
/// <param name="pScript">The script. It can have any thing valid for an HTML page.
/// It will be added at the top of the <form> block.</param>
      public static void RegisterClientScriptBlock(Page pPage, string pKey, string pScript,
         bool pAddScriptTags)
      {
         RegisterClientScriptBlock(pPage, pKey, pScript, pAddScriptTags, fDefaultGroup);
      }  // RegisterClientScriptBlock

/// <summary>
/// RegisterClientScriptBlock adds a script to the ClientScriptBlock collection.
/// Any script blocks with the same pKey parameter values are considered duplicates.
/// Call IsClientScriptBlockRegistered first and call this only when that method returns false.
/// to avoid unnecessarily running code that creates the scripts.
/// </summary>
/// <param name="pPage">The page instance.</param>
/// <param name="pKey">The key to uniquely identify this script.</param>
/// <param name="pAddScriptTags">When true, add script tags automatically.</param>
/// <param name="pScript">The script. It can have any thing valid for an HTML page.
/// <param name="pGroup">The group number to further order items. All items within a group
/// are ordered by entry order.</param>
/// It will be added at the top of the <form> block.</param>
      public static void RegisterClientScriptBlock(Page pPage, string pKey, string pScript,
         bool pAddScriptTags, int pGroup)
      {
         if (Enabled && (HttpContext.Current.Items[fSystemName + "_ScriptsRan"] == null))
         {
            RegisterScripts vRS = GetClientScripts(pPage);
            vRS.Register(pKey, pScript, pAddScriptTags, pGroup);
         }
         else
         {
            if (pAddScriptTags)
               pScript = fStartScriptTag + pScript + fEndScriptTag;
           pPage.ClientScript.RegisterClientScriptBlock(pPage.GetType(), pKey, pScript);
         }

      }  // RegisterClientScriptBlock

/// <summary>
/// IsStartupScriptRegistered is used to check that a script with the key name
/// specified is not already registered in the StartupScriptBlocks collection. 
/// Returns true when it's already registered. Only use RegisterStartupScript
/// after this returns false.
/// </summary>
/// <param name="pPage">The page instance.</param>
/// <param name="pKey">The key to check.</param>
/// <returns>True when the key is already present. False when it is not. 
/// Only call RegisterStartupScript when this returns false.</returns>
      public static bool IsStartupScriptRegistered(Page pPage, string pKey)
      {
         if (Enabled && (HttpContext.Current.Items[fSystemName + "_ScriptsRan"] == null))
         {
            RegisterScripts vRS = GetStartupScripts(pPage);
            return vRS.IsRegistered(pKey);   // may throw ArgumentException
         }
         else
             return pPage.ClientScript.IsStartupScriptRegistered(pPage.GetType (), pKey);
      }  // IsStartupScriptRegistered

/// <summary>
/// RegisterStartupScript adds a script to the StartupScriptBlock collection.
/// Any script blocks with the same pKey parameter values are considered duplicates.
/// Call IsStartupScriptRegistered first and call this only when that method returns false.
/// to avoid unnecessarily running code that creates the scripts.
/// </summary>
/// <param name="pPage">The page instance.</param>
/// <param name="pKey">The key to uniquely identify this script.</param>
/// <param name="pScript">The script. It can have any thing valid for an HTML page.
/// It will be added at the top of the <form> block.</param>
      public static void RegisterStartupScript(Page pPage, string pKey, string pScript)
      {
         RegisterScripts vRS = GetStartupScripts(pPage);
         vRS.Register(pKey, pScript, false, fDefaultGroup);
      }  // RegisterStartupScript

/// <summary>
/// RegisterStartupScript adds a script to the StartupScriptBlock collection.
/// Any script blocks with the same pKey parameter values are considered duplicates.
/// Call IsStartupScriptRegistered first and call this only when that method returns false.
/// to avoid unnecessarily running code that creates the scripts.
/// </summary>
/// <param name="pPage">The page instance.</param>
/// <param name="pKey">The key to uniquely identify this script.</param>
/// <param name="pAddScriptTags">When true, add script tags automatically.</param>
/// <param name="pScript">The script. It can have any thing valid for an HTML page.
/// It will be added at the top of the <form> block.</param>
      public static void RegisterStartupScript(Page pPage, string pKey, string pScript,
         bool pAddScriptTags)
      {
         if (Enabled && (HttpContext.Current.Items[fSystemName + "_ScriptsRan"] == null))
         {
            RegisterScripts vRS = GetStartupScripts(pPage);
            vRS.Register(pKey, pScript, pAddScriptTags, fDefaultGroup);
         }
         else
         {
            if (pAddScriptTags)
               pScript = fStartScriptTag + pScript + fEndScriptTag;
           pPage.ClientScript.RegisterStartupScript(pPage.GetType (), pKey, pScript);
         }
      }  // RegisterStartupScript

/// <summary>
/// RegisterStartupScript adds a script to the StartupScriptBlock collection.
/// Any script blocks with the same pKey parameter values are considered duplicates.
/// Call IsStartupScriptRegistered first and call this only when that method returns false.
/// to avoid unnecessarily running code that creates the scripts.
/// </summary>
/// <param name="pPage">The page instance.</param>
/// <param name="pKey">The key to uniquely identify this script.</param>
/// <param name="pAddScriptTags">When true, add script tags automatically.</param>
/// <param name="pScript">The script. It can have any thing valid for an HTML page.
/// <param name="pGroup">The group number to further order items. All items within a group
/// It will be added at the top of the <form> block.</param>
      public static void RegisterStartupScript(Page pPage, string pKey, string pScript,
         bool pAddScriptTags, int pGroup)
      {
         RegisterScripts vRS = GetStartupScripts(pPage);
         vRS.Register(pKey, pScript, pAddScriptTags, pGroup);
      }  // RegisterStartupScript

   }  // class RegisterScripts

/// <remarks>
/// RegisteredScript holds one registered script. It defines two pieces of information:
/// Key - a name which uniquely identifies this script. Users should not attempt to add
/// multiple scripts with the same key.
/// Script - a string containing the entire block to be written to the page. If needed,
/// the user should include the <script> </script> tags.
/// </remarks>
   internal class RegisteredScript : IComparable
   {
      public string Key
      {
         get { return fKey; }
         set { fKey = value; }
      }
      protected string fKey = "";

      public string Script
      {
         get { return fScript; }
         set { fScript = value; }
      }
      protected string fScript = "";

/// <summary>
/// AddScriptTags determines if the script you've passed lacks script tags and needs
/// the system to add them. They will be added surrounding each sequence
/// of RegisteredScript objects for which this is true.
/// </summary>
      public bool AddScriptTags
      {
         get { return fAddScriptTags; }
         set { fAddScriptTags = value; }
      }
      protected bool fAddScriptTags = false;

/// <summary>
/// Group provides the ability to group related scripts so one group
/// can appear above or below another. Within a group, items are sorted
/// by the Order property which is the order of entry by default.
/// The lower the value, the higher it appears on the page. Zero is the default group
/// and is usually considered the top. However, negative values are legal.
/// </summary>
      public int Group
      {
         get { return fGroup; }
         set { fGroup = value; }
      }
      protected int fGroup = 0;

/// <summary>
/// Order is the order within a Group. By default, it is the order entered into the list.
/// The lower the value, the higher it appears on the page. Zero is the default
/// and is usually considered the top. However, negative values are legal.
/// </summary>
      public int Order
      {
         get { return fOrder; }
         set { fOrder = value; }
      }
      protected int fOrder = 0;

      public RegisteredScript(string pKey, string pScript, bool pAddScriptTags, int pGroup,
         int pOrder)
      {
         Key = pKey;
         Script = pScript;
         AddScriptTags = pAddScriptTags;
         Group = pGroup;
         Order = pOrder;
      }

/// <summary>
/// Used for finding a key.
/// </summary>
/// <param name="pKey"></param>
      public RegisteredScript(string pKey)
      {
         Key = pKey;
      }

      public virtual int CompareTo(object pObj)
      {
         return String.Compare(Key, ((RegisteredScript)pObj).Key, true);   // case insensitive
      }  // CompareTo

   }  // class RegisteredScript

   internal class RegisterScriptGroupOrder : IComparer
   {
      public int Compare(object pRS1, object pRS2)
      {
         RegisteredScript vRS1 = (RegisteredScript) pRS1;
         RegisteredScript vRS2 = (RegisteredScript) pRS2;
         if (vRS1.Group < vRS2.Group)
            return -1;
         else if (vRS1.Group > vRS2.Group)
            return 1;
         else if (vRS1.Order < vRS2.Order)
            return -1;
         else if (vRS1.Order > vRS2.Order)
            return 1;
         else
            return 0;
      }
   }

}  // namespace Kaneva.RegisterScriptsClasses
