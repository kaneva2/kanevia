///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Text.RegularExpressions;

namespace KlausEnt.KEP.Kaneva.framework.utils
{
	/// <summary>
	/// Summary description for JavascriptStripper.
	/// </summary>
	public class JavascriptStripper 
	{ 
		private static Regex myRe = new Regex( String.Format( 
			@"(?inx) 
\<(\w+)\s+ 
((?'attribute'(?'attributeName'{0})\s*=\s*(?'delim'['""]?)(?'attributeValue'[^'"">]+)(\3)) 
|(?'attribute'(?'attributeName'href)\s*=\s*(?'delim'['""]?)(?'attributeValue'javascript[^'"">]+)(\3)) 
| 
[^>] 
)* 
\>" 
			,@"on(blur|c(hange|lick)|dblclick|beforeprint|afterprint|focus|keypress|(key|mouse)(down|up)|(un)?load|mouse(move|o(ut|ver))|reset|s(elect|ubmit))") 
			); 

		public static string Strip(string page) 
		{ 
			string commentPattern = @"(?'comment'<!--.*?--[ \n\r]*>)" ; 
			string embeddedScriptComments = @"(\/\*.*?\*\/|\/\/.*?[\n\r])" ; 
			string scriptPattern = String.Format(@"(?'script'<[ \n\r]*script[^>]*>(.*?{0}?)*<[ \n\r]*/script[^>]*>)", embeddedScriptComments ) ; 
			string pattern = String.Format(@"(?s)({0}|{1})", commentPattern, scriptPattern) ; 
			page = (new Regex( pattern ) ).Replace( page, "" ); 
			page = myRe.Replace( page, new MatchEvaluator( StripAttributesHandler ) ) ; 
			return page; 
		} 

		private static string StripAttributesHandler( Match m ) 
		{ 
			if( !m.Groups["attribute"].Success) return m.Value; 
			string tempVal = m.Value.Replace( m.Groups["attribute"].Value, ""); 
			return StripAttributesHandler(myRe.Match(tempVal)); 
		} 
	} 

	
}
