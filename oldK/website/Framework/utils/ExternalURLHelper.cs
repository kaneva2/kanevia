///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using log4net;

using Newtonsoft.Json;
using System.Collections.Specialized;

using Kaneva.BusinessLayer.Facade;
using Amazon.S3;
using Amazon.S3.Transfer;

namespace KlausEnt.KEP.Kaneva.framework.utils
{
    public class ExternalURLHelper
    {
        public enum UrlType { URLTYPE_UNKNOWN = 0, URLTYPE_VIDEO = 1 };
        static string[] YOUTUBE_PREFIXS = { 
            "http://www.youtube.com/watch?", 
            "http://youtube.com/watch?", 
            "http://www.youtube.com/v/",
            "http://youtube.com/v/",
            "https://www.youtube.com/watch?", 
            "https://youtube.com/watch?", 
            "https://www.youtube.com/v/",
            "https://youtube.com/v/",
        };

        public const string PROVIDER_YOUTUBE = "youtube";

        public static int ClassifyURL(string url, ref string providerStr)
        {
            for (int i = 0; i < YOUTUBE_PREFIXS.Length; i++)
            {
                if (url.ToLower().StartsWith(YOUTUBE_PREFIXS[i]))
                {
                    providerStr = PROVIDER_YOUTUBE;
                    return (int)UrlType.URLTYPE_VIDEO;
                }
            }

            return (int)UrlType.URLTYPE_UNKNOWN;
        }

        #region Youtube Helpers

        public static Boolean ParseYoutubeUrl(string url, out string resultUrl, out string extAssetId)
        {
            string assetId = "";
            string providerStr = null;
            resultUrl = "";
            extAssetId = "";

            if (ClassifyURL(url, ref providerStr) != (int)UrlType.URLTYPE_VIDEO || providerStr!=PROVIDER_YOUTUBE)
                return false;

            //if an embed code string provided search for assetID
            string youTubeEmbedCode = url;
            if ((youTubeEmbedCode.Length > 0) && ((youTubeEmbedCode.ToUpper()).IndexOf("YOUTUBE") >= 0))
            {
                int endIndexOfassetId = -1;
                //check for embed tag
                int startIndexOfassetId = youTubeEmbedCode.IndexOf("v/");
                //if embed tag not found look for URL tag
                if (startIndexOfassetId < 0)
                {
                    startIndexOfassetId = youTubeEmbedCode.IndexOf("v=");

                    //if no URL tag found display error
                    if (startIndexOfassetId < 0)
                    {
                        startIndexOfassetId = youTubeEmbedCode.IndexOf("embed/");

                        if (startIndexOfassetId < 0)
                        {
                            return false;
                        }
                        else
                        {
                            endIndexOfassetId = youTubeEmbedCode.IndexOf("\"", startIndexOfassetId);
                            //if end index not found display error
                            if (endIndexOfassetId < 0)
                            {
                                return false;
                            }
                  
                            //pull out assetID from embed code
                            assetId = youTubeEmbedCode.Substring(startIndexOfassetId + 6, endIndexOfassetId - (startIndexOfassetId + 6));
                            
                        }
                    }
                    else
                    {
                        //if URL tag found pull out the assetID
                        assetId = youTubeEmbedCode.Substring(startIndexOfassetId + 2);
                    }
                }
                //if embed tag found pull out assetID
                else
                {
                    //find end of asset id in embed string with end index
                    endIndexOfassetId = youTubeEmbedCode.IndexOf("\"", startIndexOfassetId);
                    //if end index not found display error
                    if (endIndexOfassetId < 0)
                    {
                        return false;
                    }
                    //pull out assetID from embed code
                    assetId = youTubeEmbedCode.Substring(startIndexOfassetId + 2, endIndexOfassetId - (startIndexOfassetId + 2));
                }

            }

            if (assetId == "")
                return false;

            //added logic to account for YouTube url string Changes
            //modify as necessary
            int amplocation = assetId.IndexOf("&");
            if (amplocation >= 0)
            {
                assetId = assetId.Substring(0, amplocation);
            }

            amplocation = assetId.IndexOf("?");
            if (amplocation >= 0)
            {
                assetId = assetId.Substring(0, amplocation);
            }

            amplocation = assetId.IndexOf("http");
            if (amplocation >= 0)
            {
                assetId = assetId.Substring(0, amplocation);
            }

			resultUrl = "http://www.youtube.com/v/" + assetId + "&autoplay=1";
            extAssetId = assetId;
            return true;
        }





        private class YouTubeVideo
        {
            public string kind { get; set; }
            public List<Items> items { get; set; }
            public PageInfo pageInfo { get; set; }
        }

        private class Items
        {
            public string kind { get; set; }
            public Snippet snippet { get; set; }
            public ContentDetails contentDetails { get; set; }
            public Status status { get; set; }
        }

        private class Snippet
        {
            public Thumbnails thumbnails { get; set; }
        }

        private class Thumbnails
        {
            [JsonProperty("default")]
            public DefaultImage defaultImage { get; set; }

        }
        private class DefaultImage
        {
            public string url { get; set; }
            public string with { get; set; }
            public string height { get; set; }
        }

        private class Status
        {
            public string uploadStatus { get; set; }
            public string rejectionReason { get; set; }
            public string privacyStatus { get; set; }
            public string license { get; set; }
            public string embeddable { get; set; }
            public string publicStatsViewable { get; set; }
        }

        private class PageInfo
        {
            public string totalResults { get; set; }
            public string resultsPerPage { get; set; }
        }


        private class ContentDetails
        {
            public string duration { get; set; }
        }


        /// <summary>
        /// Call Youtube to get video information Moved from mykaneva\upload.aspx.cs.
        /// </summary>
        public static int GetYouTubeInfo(string YouTubeVideoId, int userId, int assetId, ILog logger)
        {
            HttpWebRequest httpWebRequest = null;
            HttpWebResponse httpWebResponse = null;
            System.Drawing.Image imgthumb = null;
            int result = 0;

            try
            {
                // Get the DEV Id
               string devId = "AIzaSyBx0byFAHcPR7YOHJ6eHIYTWsWY8_5TgFE";
               string url = "https://www.googleapis.com/youtube/v3/videos";

                if (System.Configuration.ConfigurationManager.AppSettings["YouTubeDevId"] != null)
                {
                    devId = System.Configuration.ConfigurationManager.AppSettings["YouTubeDevId"];
                }

                if (System.Configuration.ConfigurationManager.AppSettings["YouTubeURL"] != null)
                {
                    url = System.Configuration.ConfigurationManager.AppSettings["YouTubeURL"];
                }
            
                // Load data   
                int iRuntime = 0;
                string strThumbnail = "";

                // Clean it for the call
                YouTubeVideoId  = YouTubeVideoId.Replace("?rel=0", "");
                YouTubeVideoId = YouTubeVideoId.Replace("?feature=player_detailpage", "");
            
                using (WebClient client = new WebClient())
                {
                    byte[] responseArray = client.DownloadData(url + "?part=contentDetails,snippet,status&id=" + YouTubeVideoId + "&key=" + devId);

                    // Decode and display the response.
                    //string resultXXX = "\nResponse received was :\n" + Encoding.ASCII.GetString(responseArray);

                    YouTubeVideo youTubeVideo = JsonConvert.DeserializeObject<YouTubeVideo>(Encoding.ASCII.GetString(responseArray));

                    // Check for invalid videos
                    // https://kaneva.atlassian.net/browse/ED-3588
                    // Skip a few cases....videoseries, vivo, list, impropery filtered id with ?
                    if (!YouTubeVideoId.Contains("videoseries") && !YouTubeVideoId.Contains("list") && !YouTubeVideoId.Contains("?"))
                    {
                        if (youTubeVideo.pageInfo.totalResults.Equals("0"))
                        {
                            // Video Not found
                            return -9;
                        }
                        else if (youTubeVideo.items.Count > 0)
                        {
                            if (youTubeVideo.items[0].status.uploadStatus.Equals("rejected"))
                            {
                                // Rejected
                                return -8;
                            }
                        }
                    }

                    if (youTubeVideo.items.Count > 0)
                    {
                        strThumbnail = youTubeVideo.items[0].snippet.thumbnails.defaultImage.url;

                        // Duration is  ISO 8601 duration. For example, for a video that is at least one minute long and less than one hour long, the duration is in the format PT#M#S
                        //https://developers.google.com/youtube/v3/docs/videos
                        
                        string duration = youTubeVideo.items[0].contentDetails.duration;
                        duration = duration.Replace("PT", "").Replace("H", ":").Replace("M", ":").Replace("S", "");

                        char [] splitter  = {':'};
                        string[] durationValues = duration.Split(splitter);

                        int hours = 0;
                        int minutes = 0;
                        int seconds = 0;

                        if (durationValues.Length > 2)
                        {
                            hours = int.Parse (durationValues[0]);
                            minutes = int.Parse (durationValues[1]);
                            seconds = int.Parse (durationValues[2]);
                        }
                        else if (durationValues.Length > 1)
                        {
                            minutes = int.Parse (durationValues[0]);
                            seconds = int.Parse (durationValues[1]);
                        }
                        else
                        {
                            seconds = int.Parse(durationValues[0]);
                        }


                        iRuntime = hours * 3600 + minutes * 60 + seconds;
                    }

                }

                // Update the runtime
                StoreUtility.UpdateAssetRuntime(assetId, Convert.ToUInt64(iRuntime));

                // Generate the thumbnail
                httpWebRequest = (HttpWebRequest)WebRequest.Create(strThumbnail);
                httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                System.IO.Stream receiveStream = httpWebResponse.GetResponseStream();

                string contentRepository = "";
                if (KanevaGlobals.ContentServerPath != null)
                {
                    contentRepository = KanevaGlobals.ContentServerPath;
                }

                string newPath = Path.Combine(Path.Combine(contentRepository, userId.ToString()), assetId.ToString());
                string filename = System.IO.Path.GetFileName(strThumbnail);

                if (Configuration.UseAmazonS3Storage)
                {
                    string strOriginalImagePath = newPath + Path.DirectorySeparatorChar + filename;

                    try
                    {
                        imgthumb = System.Drawing.Image.FromStream(receiveStream);

                        try
                        {
                            string azS3Bucket = Configuration.AmazonS3Bucket;

                            AmazonS3Client azS3client = new AmazonS3Client(Configuration.AmazonS3AccessKeyId,
                                    Configuration.AmazonS3SecretAccessKey,
                                    Amazon.RegionEndpoint.USEast1);

                            TransferUtility fileTransferUtility = new TransferUtility(azS3client);

                              // Save it to Amazon s3
                            using (MemoryStream qStream = new MemoryStream())
                            {
                                if (filename.ToUpper ().EndsWith (".GIF") )
                                {
                                    imgthumb.Save(qStream, System.Drawing.Imaging.ImageFormat.Gif);
                                }
                                else
                                {
                                    imgthumb.Save(qStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                }
                                fileTransferUtility.Upload(qStream, azS3Bucket, newPath.Replace("\\", "/") + "/" + filename);
                            }
                        }
                        catch (AmazonS3Exception s3Exception)
                        {
                            //m_logger.Error("S3 Error " + s3Exception.Message, s3Exception);
                        }

                        StoreUtility.UpdateAssetImage(assetId, strOriginalImagePath, "");

                        // Small
                        ImageHelper.SaveAssetThumbnail(imgthumb, (int)Constants.MEDIA_THUMB_SMALL_HEIGHT, (int)Constants.MEDIA_THUMB_SMALL_WIDTH, "_sm", "thumbnail_small_path",
                            filename, strOriginalImagePath, contentRepository, userId, assetId);

                        // Medium
                        ImageHelper.SaveAssetThumbnail(imgthumb, (int)Constants.MEDIA_THUMB_MEDIUM_HEIGHT, (int)Constants.MEDIA_THUMB_MEDIUM_WIDTH, "_me", "thumbnail_medium_path",
                            filename, strOriginalImagePath, contentRepository, userId, assetId);

                        // Large
                        ImageHelper.SaveAssetThumbnail(imgthumb, (int)Constants.MEDIA_THUMB_LARGE_HEIGHT, (int)Constants.MEDIA_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
                            filename, strOriginalImagePath, contentRepository, userId, assetId);
                    }
                    catch (Exception exc)
                    {
                        if (logger != null)
                            logger.Error("Error reading YouTube generated image, path is " + strOriginalImagePath, exc);
                    }
                    finally
                    {
                        if (imgthumb != null)
                        {
                            imgthumb.Dispose();
                        }
                    }


                    StoreUtility.MarkAssetThumbAsGenerated(assetId);
                }
                else
                {
                    // Make sure the directory exists
                    if (!System.IO.Directory.Exists(newPath))
                    {
                        System.IO.Directory.CreateDirectory(newPath);
                    }

                    string strOriginalImagePath = newPath + Path.DirectorySeparatorChar + filename;

                    imgthumb = System.Drawing.Image.FromStream(receiveStream);
                    imgthumb.Save(strOriginalImagePath);
                    imgthumb.Dispose();

                    StoreUtility.UpdateAssetImage(assetId, strOriginalImagePath, "");

                    // Save the asset thumb sizes
                    System.Drawing.Image imgOriginal = null;

                    try
                    {
                        imgOriginal = System.Drawing.Image.FromFile(strOriginalImagePath);

                        // Small
                        ImageHelper.SaveAssetThumbnail(imgOriginal, (int)Constants.MEDIA_THUMB_SMALL_HEIGHT, (int)Constants.MEDIA_THUMB_SMALL_WIDTH, "_sm", "thumbnail_small_path",
                            filename, strOriginalImagePath, contentRepository, userId, assetId);

                        // Medium
                        ImageHelper.SaveAssetThumbnail(imgOriginal, (int)Constants.MEDIA_THUMB_MEDIUM_HEIGHT, (int)Constants.MEDIA_THUMB_MEDIUM_WIDTH, "_me", "thumbnail_medium_path",
                            filename, strOriginalImagePath, contentRepository, userId, assetId);

                        // Large
                        ImageHelper.SaveAssetThumbnail(imgOriginal, (int)Constants.MEDIA_THUMB_LARGE_HEIGHT, (int)Constants.MEDIA_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
                            filename, strOriginalImagePath, contentRepository, userId, assetId);
                    }
                    catch (Exception exc)
                    {
                        if (logger != null)
                            logger.Error("Error reading YouTube generated image, path is " + strOriginalImagePath, exc);
                    }
                    finally
                    {
                        if (imgOriginal != null)
                        {
                            imgOriginal.Dispose();
                        }
                    }

                    StoreUtility.MarkAssetThumbAsGenerated(assetId);
                }

             
            }
            catch (Exception exc)
            {
                logger.Error("Error getting YouTube runtime or thumbnail", exc);

                // Log any errors
                result = 1;
            }
            finally
            {
                if (httpWebResponse != null)
                {
                    httpWebResponse.Close();
                }

                if (imgthumb != null)
                {
                    imgthumb.Dispose();
                }

            }

            return result;
        }


        #endregion

        #region Google Analytics Reporting Helper

        public const string GOOGLE_ANALYTICS_AUTH_URL = "https://www.google.com/accounts/ClientLogin";

        private static string GetGoogleAnalyticsAuthToken(string email, string password)
        {
            WebRequest request = WebRequest.Create(GOOGLE_ANALYTICS_AUTH_URL);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Timeout = 1000000;

            ASCIIEncoding encoding = new ASCIIEncoding();
            string stringData = "accountType=GOOGLE";
            stringData += "&Email=" + email;
            stringData += "&Passwd=" + password;
            stringData += "&service=analytics";
            stringData += "&Source=www.kaneva.com";

            byte[] data = encoding.GetBytes(stringData);
            request.ContentLength = data.Length;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(data, 0, data.Length);
            requestStream.Close();

            WebResponse response = request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            string responseFromServer = reader.ReadToEnd();

            return responseFromServer.Substring(responseFromServer.LastIndexOf("Auth=") + 5);
        }

        public static Hashtable GoogleAnalyticsReportInfo(int gameId, DateTime startDate, DateTime endDate, ILog logger)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsEmail"] != null &&
                System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsPassword"] != null)
            {
                string email = System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsEmail"];
                string password = System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsPassword"];

                string authToken = GetGoogleAnalyticsAuthToken(email, password);
                if (authToken.Length == 0)
                {
                    logger.Error("ExternalURLHelper GoogleAnalyticsReportInfo - GA did not return a valid Auth token");
                    return null;
                }

                string url = "https://www.google.com/analytics/feeds/data";
                url += "?ids=ga%3A20359574";
                url += "&dimensions=ga%3Aweek";
                url += "&metrics=ga%3AuniquePageviews";
                url += "&filters=ga%3APagePath%3D%40%2Fkgp%2Fplaywok.aspx%3Fgoto%3DU" + gameId;
                url += "&start-date=" + startDate.ToString("yyyy-MM-dd");
                url += "&end-date=" + endDate.ToString("yyyy-MM-dd");
                url += "&max-results=50";

                WebRequest request = WebRequest.Create(url);
                request.Method = "GET";
                request.ContentType = "application/x-www-form-urlencoded";
                request.Headers.Add("Authorization", "GoogleLogin Auth=" + authToken);
                request.Timeout = 1000000;

                WebResponse response = request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader stReader = new StreamReader(responseStream);
                string responseFromServer = stReader.ReadToEnd();

                Hashtable returnTable = new Hashtable();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(responseFromServer);
                XmlNodeList entries = doc.DocumentElement.GetElementsByTagName("entry");
                foreach (XmlNode node in entries)
                {
                    if (node.Name == "entry")
                    {
                        string dimension = node["dxp:dimension"].Attributes["value"].Value;
                        string metric = node["dxp:metric"].Attributes["value"].Value;
                        returnTable.Add(dimension, Convert.ToInt32(metric));
                    }
                }
               
                return returnTable;
            }
            else
            {
                logger.Error("ExternalURLHelper GoogleAnalyticsReportInfo - no valid GA email or password");
                return null;
            }
        }

        #endregion
    }
}
