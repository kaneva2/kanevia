///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using System.Data;

namespace KlausEnt.KEP.Kaneva
{
    public class UserBalances
    {
 		/// <summary>
		/// Default Constructor
		/// </summary>
		public UserBalances ()
		{
		}

        public UserBalances (int userId)
		{
			PopulateBalances (UsersUtility.GetUserBalances(userId));
		}

        private void PopulateBalances (DataTable dtBalances)
		{
            if (dtBalances == null)
			{
				return;
			}

            for (int i=0; i < dtBalances.Rows.Count; i++)
            {
                try
                {
                    double balance = Convert.ToDouble (dtBalances.Rows[i]["balance"]);
                    string pointId = dtBalances.Rows[i]["kei_point_id"].ToString ();
                    SetBalance (balance, pointId);
                }
                catch (Exception exc)
                {
                    string s = exc.ToString ();
                }
            }
		}

        private void SetBalance (double balance, string pointId)
        {
            switch (pointId.ToUpper())
            {
                case Constants.CURR_GPOINT:
                    m_gpoint = balance;
                    break;

                case Constants.CURR_KPOINT:
                    m_kpoint = balance;
                    break;

                case Constants.CURR_MPOINT:
                    m_mpoint = balance;
                    break;

                case Constants.CURR_DOLLAR:
                    m_dollar = m_dollar;
                    break;
            }
        }
        
            
        public double GPoint
        {
            get { return m_gpoint; }
            set { m_gpoint = value; }
        }

        public double KPoint
        {
            get { return m_kpoint; }
            set { m_kpoint = value; }
        }

        public double MPoint
        {
            get { return m_mpoint; }
            set { m_mpoint = value; }
        }

        public double Dollar
        {
            get { return m_dollar; }
            set { m_dollar = value; }
        }

        public double Rewards
        {
            get { return m_gpoint; }
        }

        public double Credits
        {
            get { return m_kpoint + m_mpoint; }
        }

        private double m_gpoint = 0.0;
        private double m_kpoint = 0.0;
        private double m_mpoint = 0.0;
        private double m_dollar = 0.0;
    }
}
