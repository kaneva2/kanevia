///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using System.Data;

using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for WokUserInfo.
	/// </summary>
	public class WokUserInfo
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		public WokUserInfo ()
		{
		}

		public WokUserInfo (int userId)
		{
            PopulateWokUserInfo(userId);
		}

		/// <summary>
		/// Load data
		/// </summary>
		/// <param name="userName"></param>
		public void LoadData (int userId)
		{
			if (!HttpContext.Current.Request.IsAuthenticated)
			{
				// Keep the default values
				return;
			}

			// Read from database
            PopulateWokUserInfo(userId);			
		}


		private void PopulateWokUserInfo(int userId)
		{
            UserFacade uf = new UserFacade();
            uf.GetPlayerAdmin(userId, out m_admin, out m_gm, out m_secondToLastLogin);
		}

		public bool IsGm 
		{
			get { return m_gm; }
		}

		public bool IsAdmin 
		{
			get { return m_admin; }
		}

        public DateTime SecondToLastLogin
        {
            get { return m_secondToLastLogin; }
        }
		private bool m_gm = false;
		private bool m_admin = false;
        private DateTime m_secondToLastLogin = new DateTime();
	}
}
