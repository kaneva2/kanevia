///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

    using System;
    using System.Text;
    using System.Web.Security;
    using System.Web;
    using System.Web.Handlers;
    using System.Collections;
    using System.Data;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using log4net;
using System.Linq;

    using KlausEnt.KEP.Kaneva.framework.widgets;

    using Kaneva.BusinessLayer.Facade;
    using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;

namespace KlausEnt.KEP.Kaneva
{
    /// <summary>
    /// Summary description for UsersUtility.
    /// </summary>
    public class UsersUtility
    {
        private static readonly ILog log = LogManager.GetLogger (typeof (UsersUtility));

        private const string GIFT_ACCEPT_SUBJECT = "{0} has accepted your gift";
        private const string GIFT_ACCEPT_MESSAGE = "{0} has accepted the gift, \"{1},\" which you so generously gave.";
        private const string GIFT_REJECT_SUBJECT = "{0} has rejected your gift";
        private const string GIFT_REJECT_MESSAGE = "Sadly, {0} has rejected the gift, \"{1},\" which you so generously gave.";

        private static readonly Hashtable htRoles = new Hashtable (7);

        private const string ROLE_SYSTEM = "System";
        private const string ROLE_MEMBER = "Member";
        private const string ROLE_CERTIFIED_BETATESTER = "Certified Betatester";
        private const string ROLE_ADMIN = "Administrator User";
        private const string ROLE_CSR = "CSR";
        private const string ROLE_IT = "IT";
        private const string ROLE_FUTURE_USE = "Not Used Yet";

        static UsersUtility ()
        {
            // These are in a static HashTable for speed. No need to get these from DB unless they start changing.
            htRoles.Add ("1", ROLE_SYSTEM);
            htRoles.Add ("2", ROLE_MEMBER);
            htRoles.Add ("4", ROLE_CERTIFIED_BETATESTER);
            htRoles.Add ("8", ROLE_ADMIN);
            htRoles.Add ("16", ROLE_CSR);
            htRoles.Add ("32", ROLE_IT);
            htRoles.Add ("64", ROLE_FUTURE_USE);
        }

        /// <summary>
        /// Return a list of user status
        /// </summary>
        /// <returns></returns>
        public static DataTable GetUserStatus ()
        {
            string sqlSelectString = "SELECT status_id, name, description FROM user_status";
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelectString);
        }

        /// <summary>
        /// Get the user roles
        /// </summary>
        public static string[] GetUserRolesById (int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable roleHT = new Hashtable ();
            int userRole = 0;

            string sqlSelectString = "SELECT role FROM users where user_id = @userId"; ;
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            DataRow drUser = dbUtility.GetDataRow (sqlSelectString, parameters, false);

            try
            {
                userRole = Convert.ToInt32 (drUser["role"]);
            }
            catch (Exception e)
            {
                m_logger.Error (e.ToString ());
            }

            ArrayList ar = GetUserRoleArray (userRole);
            return (string[]) ar.ToArray (Type.GetType ("System.String"));
        }

        /// <summary>
        /// Return the list of roles user is in
        /// </summary>
        public static ArrayList GetUserRoleArray (int roleMembership)
        {
            ArrayList results = new ArrayList ();

            if (roleMembership != 0)
            {
                string roleDescription = null;

                ulong role = (ulong) roleMembership;

                int bitIndex = 1;

                for (int ii = 0; ii < 32; ii++)
                {
                    if ((role & 0x01) == 0x01)
                    {
                        roleDescription = (string) htRoles[Convert.ToString (bitIndex)];
                        //results += roleDescription + "\n";
                        results.Add (roleDescription);
                    }

                    bitIndex = bitIndex + bitIndex;

                    role = role >> 1;
                }
            }

            return results;
        }

        /// <summary>
        /// Is the user a site administrator
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static bool IsUserAdministrator ()
        {
            return HttpContext.Current.User.IsInRole (ROLE_ADMIN);
        }

        /// <summary>
        /// Is the user a site administrator
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static bool IsUserAdministrator (int userId)
        {
            return IsUserInRole (userId, ROLE_ADMIN);
        }

        /// <summary>
        /// Is the user a site administrator
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static bool IsUserAdministratorByUserRole (int role)
        {
            return IsUserInRole (ROLE_ADMIN, role);
        }

        /// <summary>
        /// Is the user a csr
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static bool IsUserCSR ()
        {
            return HttpContext.Current.User.IsInRole (ROLE_CSR);
        }

        /// <summary>
        /// Is the user a csr
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static bool IsUserCSR (int userId)
        {
            return IsUserInRole (userId, ROLE_CSR);
        }

        /// <summary>
        /// Is the user a csr
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static bool IsUserCSRByUserRole (int role)
        {
            return IsUserInRole (ROLE_CSR, role);
        }

        /// <summary>
        /// Is the user a IT
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static bool IsUserIT ()
        {
            return HttpContext.Current.User.IsInRole (ROLE_IT);
        }

        /// <summary>
        /// Is the user a IT
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static bool IsUserIT (int userId)
        {
            return IsUserInRole (userId, ROLE_IT);
        }

        /// <summary>
        /// Is the user a IT
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static bool IsUserITByUserRole (int role)
        {
            return IsUserInRole (ROLE_IT, role);
        }

        /// <summary>
        /// Is the user a Betatestor
        /// </summary>
        public static bool IsUserBetatester ()
        {
            return HttpContext.Current.User.IsInRole (ROLE_CERTIFIED_BETATESTER);
        }

        /// <summary>
        /// Is the user a Betatestor
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static bool IsUserBetatesterByUserRole (int role)
        {
            return IsUserInRole (ROLE_CERTIFIED_BETATESTER, role);
        }

        /// <summary>
        /// IsUserInRole
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="role"></param>
        private static bool IsUserInRole (string strRoleName, int role)
        {
            ArrayList ar = GetUserRoleArray (role);
            string[] sa = (string[]) ar.ToArray (Type.GetType ("System.String"));

            foreach (string desc in sa)
            {
                if (desc.IndexOf (strRoleName) != -1)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// IsUserInRole
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="role"></param>
        private static bool IsUserInRole (int userId, string strRoleName)
        {
            string[] sa = GetUserRolesById (userId);
            foreach (string desc in sa)
            {
                if (desc.IndexOf (strRoleName) != -1)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// GetUserPointTotal
        /// </summary>
        /// <returns></returns>
        public static Double GetUserPointTotal (int userId)
        {
            return getUserBalance (userId, Constants.CURR_KPOINT) + getUserBalance (userId, Constants.CURR_MPOINT);
        }

        /// <summary>
        /// Get user balance
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="keiPointId"></param>
        /// <returns></returns>
        public static Double getUserBalance (int userId, string keiPointId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT COALESCE(ub.balance, 0) as balance " +
                " FROM user_balances ub " +
                " WHERE ub.user_id = @userId " +
                " AND ub.kei_point_id = @keiPointId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@keiPointId", keiPointId);

            DataRow dtBalance = dbUtility.GetDataRow (sqlSelect, parameters, false);
            if (dtBalance == null)
                return 0.0;
            else
                return Convert.ToDouble (dtBalance["balance"]);
        }

        /// <summary>
        /// Get user balance since last login
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="keiPointId"></param>
        /// <returns></returns>
        public static Double getUserBalanceSinceLastLogin(int userId, string keiPointId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            Double currentUserBalance = getUserBalance(userId, keiPointId);

            string sqlSelect = "SELECT balance FROM kaneva.wok_transaction_log tl" +
                " INNER JOIN wok.game_users gu on tl.user_id = gu.kaneva_user_id" +
                " WHERE gu.kaneva_user_id = @userId AND kei_point_id = @keiPointId AND tl.created_date < second_to_last_logon" +
                " ORDER BY tl.created_date DESC, tl.trans_id DESC LIMIT 1;";
            
            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);
            parameters.Add("@keiPointId", keiPointId);

            DataRow dtBalance = dbUtility.GetDataRow(sqlSelect, parameters, false);
            if (dtBalance == null)
                return 0.0;
            else
                return currentUserBalance - Convert.ToDouble(dtBalance["balance"]);
        }

        /// <summary>
        /// Get user balances
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="keiPointId"></param>
        /// <returns></returns>
        public static DataTable GetUserBalances (int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT COALESCE(ub.balance, 0) as balance, kei_point_id " +
                " FROM user_balances ub " +
                " WHERE ub.user_id = @userId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return dbUtility.GetDataTable (sqlSelect, parameters);
        }

        /// <summary>
        /// Inserts info record into the kaneva.reward_log table
        /// </summary>
        public static void InsertRewardLog (int userId, double amount, string keiPointId, int transTypeId, int rewardEventId, int wokTransLogId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sql = "INSERT INTO kaneva.reward_log " +
                "(user_id, date_created, amount, kei_point_id, transaction_type, reward_event_id, wok_transaction_log_id, return_code" +
                ") VALUES (" +
                "@userId, DATE(NOW()), @transactionAmount, @keiPointId, @transTypeId, @rewardEventId, @wokTransLogId, 0)";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@transactionAmount", amount);
            parameters.Add ("@keiPointId", keiPointId);
            parameters.Add ("@transTypeId", transTypeId);
            parameters.Add ("@rewardEventId", rewardEventId);
            parameters.Add ("@wokTransLogId", wokTransLogId);
            
            dbUtility.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// UpdatePeakConcurrentPlayers
        /// </summary>
        public static void UpdatePeakConcurrentPlayers(int peakCount)
        {
            string sqlCallSP = "CALL wok.update_peak_concurrent_players (@peakCount);";

            Hashtable parameters = new Hashtable();
            parameters.Add("@peakCount", peakCount);
            KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sqlCallSP, parameters);
        }

        /// <summary>
        /// UnsubscribeEmail
        /// </summary>
        public static void UnsubscribeEmail(string email)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityEmailViral();

            string sqlInsert = "CALL  insert_email_bounce_log('UR','Website', @email, null)";
            Hashtable parameters = new Hashtable();
            parameters.Add("@email", email);
            dbUtility.ExecuteNonQuery(sqlInsert, parameters);
        }
          
      
        public static void ClearUserProfilePic (int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlUpdate = "update kaneva.communities set thumbnail_path=NULL, thumbnail_small_path='', " +
                " thumbnail_medium_path='', thumbnail_large_path='', thumbnail_xlarge_path='', thumbnail_type=NULL, has_thumbnail='N' " +
                " WHERE creator_id=@userId " +
                " AND is_personal=1;";

            Hashtable parameters = new Hashtable();
            parameters.Add ("@userId", userId);
            dbUtility.ExecuteNonQuery(sqlUpdate, parameters);
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user_id"></param>
        public static void UpdateUserProfile (int userId, string showFavorites, string showProf, string showOrg, string favoriteGames,
            string favoriteMovies, string favoriteMusic, string favoriteBooks, string favoriteTV, string interests, string profTitle, string profPosition, string profIndustry, string profCompany,
            string jobDesc, string profSkills, string currAssoc, string pastAssoc,
            string showStore, string showBlog, string showGames, string showBookmarks)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            // If there is not a record there add one
            string sqlSelect = "SELECT COUNT(*) FROM user_profiles WHERE user_id = @userId";
            if (dbUtility.ExecuteScalar (sqlSelect, parameters) == 0)
            {
                dbUtility.ExecuteNonQuery ("INSERT INTO user_profiles (user_id) VALUES (@userId)", parameters);
            }

            string sqlUpdate = "UPDATE user_profiles " +
                " SET show_favorites = @showFavorites," +
                " show_prof = @showProf," +
                " show_org = @showOrg," +
                " favorite_games = @favoriteGames," +
                " favorite_movies = @favoriteMovies," +
                " favorite_music = @favoriteMusic," +
                " favorite_books = @favoriteBooks," +
                " favorite_tv = @favoriteTV," +
                " interests = @interests," +
                " prof_title = @profTitle," +
                " prof_position = @profPosition," +
                " prof_industry = @profIndustry," +
                " prof_company = @profCompany," +
                " prof_job = @profJob," +
                " prof_skills = @profSkills," +
                " org_current_assoc = @currAssoc," +
                " org_past_assoc = @pastAssoc," +
                " show_store = @showStore," +
                " show_blog = @showBlog," +
                " show_games = @showGames," +
                " show_bookmarks = @showBookmarks" +
                " WHERE user_id = @userId";

            // Reset params
            parameters = new Hashtable ();
            parameters.Add ("@showFavorites", showFavorites);
            parameters.Add ("@showProf", showProf);
            parameters.Add ("@showOrg", showOrg);
            parameters.Add ("@favoriteGames", favoriteGames);
            parameters.Add ("@favoriteMovies", favoriteMovies);
            parameters.Add ("@favoriteMusic", favoriteMusic);
            parameters.Add ("@favoriteBooks", favoriteBooks);
            parameters.Add ("@favoriteTV", favoriteTV);
            parameters.Add ("@profJob", jobDesc);
            parameters.Add ("@profSkills", profSkills);
            parameters.Add ("@profTitle", profTitle);
            parameters.Add ("@profPosition", profPosition);
            parameters.Add ("@profIndustry", profIndustry);
            parameters.Add ("@profCompany", profCompany);
            parameters.Add ("@currAssoc", currAssoc);
            parameters.Add ("@pastAssoc", pastAssoc);
            parameters.Add ("@interests", interests);
            parameters.Add ("@showBlog", showBlog);
            parameters.Add ("@showStore", showStore);
            parameters.Add ("@showGames", showGames);
            parameters.Add ("@showBookmarks", showBookmarks);
            parameters.Add ("@userId", userId);

            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user_id"></param>
        public static void UpdateUserProfile (int userId,
            string relationship, string orientation, string religion, string ethnicity, string children, string education, string income,
            int heightFeet, int heightInches, string smoke, string drink)
        {
            UpdateUserProfile (userId, relationship, orientation, religion, ethnicity, children, education, income,
                heightFeet, heightInches, smoke, drink, "false!");
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user_id"></param>
        public static void UpdateUserProfile (int userId,
            string relationship, string orientation, string religion, string ethnicity, string children, string education, string income,
            int heightFeet, int heightInches, string smoke, string drink, string hometown)
        {
            bool includeHometown = (hometown == "false!" ? false : true);

            string sHeightFeet = heightFeet.ToString ();
            string sHeightInches = heightInches.ToString ();

            if (heightFeet == 0)
            {
                sHeightFeet = "NULL";
                sHeightInches = "NULL";
            }
            else
            {
                if (heightInches == -1)
                {
                    sHeightInches = "0";
                }
            }

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            // If there is not a record there add one
            string sqlSelect = "SELECT COUNT(*) FROM user_profiles WHERE user_id = @userId";
            if (dbUtility.ExecuteScalar (sqlSelect, parameters) == 0)
            {
                dbUtility.ExecuteNonQuery ("INSERT INTO user_profiles (user_id) VALUES (@userId)", parameters);
            }

            parameters = new Hashtable ();

            string sqlUpdate = "UPDATE user_profiles " +
                " SET " +
                " relationship = @relationship," +
                " orientation = @orientation," +
                " religion = @religion," +
                " ethnicity = @ethnicity," +
                " children = @children," +
                " education = @education," +
                " income = @income," +
                " height_feet = " + sHeightFeet + "," +
                " height_inches =  " + sHeightInches + "," +
                " smoke = @smoke," +
                " drink = @drink";

            if (includeHometown)
            {
                sqlUpdate += ", hometown = @hometown";
                parameters.Add ("@hometown", hometown);
            }

            sqlUpdate += " WHERE user_id = @userId";

            // Reset params
            parameters.Add ("@relationship", relationship);
            parameters.Add ("@orientation", orientation);
            parameters.Add ("@religion", religion);
            parameters.Add ("@ethnicity", ethnicity);
            parameters.Add ("@children", children);
            parameters.Add ("@education", education);
            parameters.Add ("@income", income);
            parameters.Add ("@smoke", smoke);
            parameters.Add ("@drink", drink);
            parameters.Add ("@userId", userId);

            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user_id"></param>
        public static void UpdateUserProfile (int userId,
            string favoriteGames, string favoriteMovies, string favoriteMusic, string favoriteBooks, string favoriteTV)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            // If there is not a record there add one
            string sqlSelect = "SELECT COUNT(*) FROM user_profiles WHERE user_id = @userId";
            if (dbUtility.ExecuteScalar (sqlSelect, parameters) == 0)
            {
                dbUtility.ExecuteNonQuery ("INSERT INTO user_profiles (user_id) VALUES (@userId)", parameters);
            }

            string sqlUpdate = "UPDATE user_profiles " +
                " SET " +
                " favorite_games = @favoriteGames," +
                " favorite_movies = @favoriteMovies," +
                " favorite_music = @favoriteMusic," +
                " favorite_books = @favoriteBooks," +
                " favorite_tv = @favoriteTV" +
                " WHERE user_id = @userId";

            // Reset params
            parameters = new Hashtable ();
            parameters.Add ("@favoriteGames", favoriteGames);
            parameters.Add ("@favoriteMovies", favoriteMovies);
            parameters.Add ("@favoriteMusic", favoriteMusic);
            parameters.Add ("@favoriteBooks", favoriteBooks);
            parameters.Add ("@favoriteTV", favoriteTV);
            parameters.Add ("@userId", userId);

            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        public static void AddBillingSubscriptionId(int userId, string billingSubscriptionId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlString = "CALL insert_billing_subscription(@userId, @billingSubscriptionId);";

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);
            parameters.Add("@billingSubscriptionId", billingSubscriptionId);

            dbUtility.ExecuteNonQuery(sqlString, parameters);
        }


        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user_id"></param>
        public static void UpdateUserInstantMessages (int userId,
            string imUsername1, string imType1, string imSecurity1,
            string imUsername2, string imType2, string imSecurity2,
            string imUsername3, string imType3, string imSecurity3,
            string imUsername4, string imType4, string imSecurity4)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            // If there is not a record there add one
            string sqlSelect = "SELECT COUNT(*) FROM user_profiles WHERE user_id = @userId";
            if (dbUtility.ExecuteScalar (sqlSelect, parameters) == 0)
            {
                dbUtility.ExecuteNonQuery ("INSERT INTO user_profiles (user_id) VALUES (@userId)", parameters);
            }

            string sqlUpdate = "UPDATE user_profiles " +
                " SET " +
                " im_username_1 = @imUsername1," +
                " im_type_1 = @imType1," +
                " im_security_1 = @imSecurity1," +
                " im_username_2 = @imUsername2," +
                " im_type_2 = @imType2," +
                " im_security_2 = @imSecurity2," +
                " im_username_3 = @imUsername3," +
                " im_type_3 = @imType3," +
                " im_security_3 = @imSecurity3," +
                " im_username_4 = @imUsername4," +
                " im_type_4 = @imType4," +
                " im_security_4 = @imSecurity4" +
                " WHERE user_id = @userId";

            // Reset params
            parameters = new Hashtable ();
            parameters.Add ("@imUsername1", imUsername1);
            parameters.Add ("@imType1", imType1);
            parameters.Add ("@imSecurity1", imSecurity1);
            parameters.Add ("@imUsername2", imUsername2);
            parameters.Add ("@imType2", imType2);
            parameters.Add ("@imSecurity2", imSecurity2);
            parameters.Add ("@imUsername3", imUsername3);
            parameters.Add ("@imType3", imType3);
            parameters.Add ("@imSecurity3", imSecurity3);
            parameters.Add ("@imUsername4", imUsername4);
            parameters.Add ("@imType4", imType4);
            parameters.Add ("@imSecurity4", imSecurity4);
            parameters.Add ("@userId", userId);

            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        
        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user_id"></param>
        public static void UpdateUserProfile (int userId,
            string titleBlast, string likeToMeet, string favoriteGames, string favoriteMovies, string favoriteMusic, string favoriteBooks, string favoriteTV,
            string hobbies, string favoriteSports, string favoriteGadgets, string favoriteFashion, string favoriteCars, string favoriteArtists,
            string clubs, string favoriteBrands, string roleModels, string favoriteQuotes)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            // If there is not a record there add one
            string sqlSelect = "SELECT COUNT(*) FROM user_profiles WHERE user_id = @userId";
            if (dbUtility.ExecuteScalar (sqlSelect, parameters) == 0)
            {
                dbUtility.ExecuteNonQuery ("INSERT INTO user_profiles (user_id) VALUES (@userId)", parameters);
            }

            string sqlUpdate = "UPDATE user_profiles " +
                " SET " +
                " favorite_games = @favoriteGames," +
                " favorite_movies = @favoriteMovies," +
                " favorite_music = @favoriteMusic," +
                " favorite_books = @favoriteBooks," +
                " favorite_tv = @favoriteTV, " +
                " hobbies = @hobbies, " +
                " favorite_sports = @favoriteSports, " +
                " favorite_gadgets = @favoriteGadgets, " +
                " favorite_fashion = @favoriteFashion, " +
                " favorite_cars = @favoriteCars, " +
                " favorite_artists = @favoriteArtists, " +
                " like_to_meet = @likeToMeet, " +
                " title_blast = @titleBlast, " +
                " clubs = @clubs, " +
                " favorite_brands = @favoriteBrands, " +
                " role_models = @roleModels, " +
                " favorite_quotes = @favoriteQuotes " +
                " WHERE user_id = @userId";

            // Reset params
            parameters = new Hashtable ();
            parameters.Add ("@favoriteGames", favoriteGames);
            parameters.Add ("@favoriteMovies", favoriteMovies);
            parameters.Add ("@favoriteMusic", favoriteMusic);
            parameters.Add ("@favoriteBooks", favoriteBooks);
            parameters.Add ("@favoriteTV", favoriteTV);
            parameters.Add ("@hobbies", hobbies);
            parameters.Add ("@favoriteSports", favoriteSports);
            parameters.Add ("@favoriteGadgets", favoriteGadgets);
            parameters.Add ("@favoriteFashion", favoriteFashion);
            parameters.Add ("@favoriteCars", favoriteCars);
            parameters.Add ("@favoriteArtists", favoriteArtists);
            parameters.Add ("@likeToMeet", likeToMeet);
            parameters.Add ("@titleBlast", titleBlast);
            parameters.Add ("@clubs", clubs);
            parameters.Add ("@favoriteBrands", favoriteBrands);
            parameters.Add ("@roleModels", roleModels);
            parameters.Add ("@favoriteQuotes", favoriteQuotes);
            parameters.Add ("@userId", userId);

            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user_id"></param>
        public static void UpdateUserProfile (int userId, string profTitle, string profPosition, string profIndustry, string profCompany,
            string jobDesc, string profSkills, string currAssoc, string pastAssoc)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            // If there is not a record there add one
            string sqlSelect = "SELECT COUNT(*) FROM user_profiles WHERE user_id = @userId";
            if (dbUtility.ExecuteScalar (sqlSelect, parameters) == 0)
            {
                dbUtility.ExecuteNonQuery ("INSERT INTO user_profiles (user_id) VALUES (@userId)", parameters);
            }

            string sqlUpdate = "UPDATE user_profiles " +
                " SET " +
                " prof_title = @profTitle," +
                " prof_position = @profPosition," +
                " prof_industry = @profIndustry," +
                " prof_company = @profCompany," +
                " prof_job = @profJob," +
                " prof_skills = @profSkills," +
                " org_current_assoc = @currAssoc," +
                " org_past_assoc = @pastAssoc" +
                " WHERE user_id = @userId";

            // Reset params
            parameters = new Hashtable ();
            parameters.Add ("@profJob", jobDesc);
            parameters.Add ("@profSkills", profSkills);
            parameters.Add ("@profTitle", profTitle);
            parameters.Add ("@profPosition", profPosition);
            parameters.Add ("@profIndustry", profIndustry);
            parameters.Add ("@profCompany", profCompany);
            parameters.Add ("@currAssoc", currAssoc);
            parameters.Add ("@pastAssoc", pastAssoc);
            parameters.Add ("@userId", userId);

            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// Get user billing subscription id
        /// </summary>
        /// <returns></returns>
        public static string GetUserBillingSubscriptionId (int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlSelectString = "SELECT billing_subscription_id " +
                " FROM users_billing_subscriptions " +
                " WHERE user_id = @userId " + 
                " AND billing_subscription_status = 1";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            DataRow drBillSubscription = dbUtility.GetDataRow (sqlSelectString, parameters, false);
            if (drBillSubscription != null)
            {
                return (string) drBillSubscription["billing_subscription_id"];
            }
            return "";
        }

        /// <summary>
        /// InsertUserEditorDownload
        /// </summary>
        /// <param name="userId"></param>
        public static void InsertUserEditorDownload (int userId, string version)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "INSERT INTO user_editor_downloads " +
                " (user_id, downloaded_datetime, version) " +
                " VALUES (@userId," + dbUtility.GetCurrentDateFunction () + ", @version)";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@version", version);
            dbUtility.ExecuteNonQuery (sqlSelect, parameters);
        }

        /// <summary>
        /// Get user Id
        /// </summary>
        /// <returns></returns>
        public static DataRow GetUserFromEmail (string email)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlSelectString = "SELECT user_id, username, gender, status_id, birth_date, show_mature, country FROM users WHERE email = @email";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@email", email);

            return dbUtility.GetDataRow (sqlSelectString, parameters, false);
        }

        /// <summary>
        /// Get user email from Id
        /// </summary>
        /// <returns></returns>
        public static string GetUserEmailFromId (int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlSelectString = "SELECT email FROM users WHERE user_id = @userId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            DataRow drUser = dbUtility.GetDataRow (sqlSelectString, parameters, false);
            if (drUser != null)
            {
                return (string) drUser["email"];
            }
            return null;
        }

        /// <summary>
        /// Get user Id
        /// </summary>
        /// <returns></returns>
        public static int GetUserIdFromUsername (string username)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlSelectString = "SELECT user_id FROM users WHERE username = @username";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@username", username);

            DataRow drUser = dbUtility.GetDataRow (sqlSelectString, parameters, false);
            if (drUser != null)
            {
                return (int) drUser["user_id"];
            }
            return 0;
        }

        /// <summary>
        /// get the player (avatar) details about a kaneva userid
        /// will be null if they don't have an avatar yet
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataRow GetPlayerFromUserId (int userId)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlSelectString = " SELECT p.`player_id`, p.`name`, p.`title`, p.`team_id`, p.`race_id`, p.`clan_id`, p.`in_game` " +
                                     " FROM " + dbNameForGame + ".`players` p " +
                                    " WHERE p.kaneva_user_id = @userId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return dbUtility.GetDataRow (sqlSelectString, parameters, false);
        }

        /// <summary>
        /// kaneva userid from the player id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataRow GetUserFromPlayerId (int playerId)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlSelectString = " SELECT p.kaneva_user_id, com.name, com.name_no_spaces " +
                " FROM " + dbNameForGame + ".players p INNER JOIN " + dbNameForKaneva + ".communities_personal com ON com.creator_id = p.kaneva_user_id " +
                " WHERE p.player_id = @playerId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@playerId", playerId);
            return dbUtility.GetDataRow (sqlSelectString, parameters, false);
        }

    /// <summary>
    /// Get User Login Info
    /// </summary>
    /// <returns></returns>
    public static DataRow GetUserLoginInfoFromUsername(string username)
    {
      string dbNameForGame = KanevaGlobals.DbNameKGP;


      DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

      string sqlSelectString = "SELECT gu.user_id, gu.login_count, gender, age, UNIX_TIMESTAMP(signup_date) as signup_date, UNIX_TIMESTAMP(gu.created_date) as created_date, " +
        " UNIX_TIMESTAMP(last_logon) as last_logon, UNIX_TIMESTAMP(second_to_last_logon) as second_to_last_logon, " +
        " DATEDIFF(last_logon, created_date) AS days_old,  COALESCE(acs.description, \"-\") as referrer, gu.kaneva_user_id, DATE_FORMAT(signup_date, '%c/%e/%Y') AS formatted_signup_date  " +
        " FROM " + dbNameForGame + ".game_users gu JOIN users ku ON gu.kaneva_user_id = ku.user_id " +
        " LEFT JOIN user_acquisition_source uas ON ku.user_id = uas.user_id " + 
        " LEFT JOIN acquisition_source acs ON uas.acquisition_source_id = acs.acquisition_source_id " + 
        " WHERE gu.username = @username";

      Hashtable parameters = new Hashtable();
      parameters.Add("@username", username);

      return dbUtility.GetDataRow(sqlSelectString, parameters, false);
    }

        /// <summary>
        /// Get username from Id
        /// </summary>
        /// <returns></returns>
        public static string GetUserNameFromId (int id)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlSelectString = "SELECT username FROM users WHERE user_id = @userid";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userid", id);

            DataRow drUser = dbUtility.GetDataRow (sqlSelectString, parameters, false);
            if (drUser != null)
            {
                return (string) drUser["username"];
            }
            return null;
        }

        /// <summary>
        /// Get a user with basic info
        /// </summary>
        public static bool GetUser (string email, ref string username, ref string password, ref string salt, ref int role, ref int userId, ref int userStatus)
        {
            bool ret = false;

            // List of parameters for sql statement
            ArrayList paramList = new ArrayList ();
            dbParms userName = new dbParms (email, 80, dbParms.dataTypes.VarChar);
            paramList.Add (userName);

            string sqlSelectString = "SELECT username, password, salt, role, user_id, status_id FROM users WHERE email= @param1";

            DataRow dtUser = KanevaGlobals.GetDatabaseUtility ().GetDataRowSafe (sqlSelectString, paramList, false);

            try
            {
                username = (string) dtUser["username"];
                password = (string) dtUser["password"];
                salt = (string) dtUser["salt"];
                role = Convert.ToInt32 (dtUser["role"]);
                userId = (int) dtUser["user_id"];
                userStatus = (int) dtUser["status_id"];

                ret = true;
            }
            catch (Exception)
            {
                ret = false;
                //Log.getInstance().writeLog (Log.Error,"Error", e.Message);
            }

            return ret;
        }

        ///// <summary>
        ///// Get the user info
        ///// </summary>
        ///// <param name="userId"></param>
        ///// <returns></returns>
        //[Obsolete("Use new business layer")]
        //public static DataRow GetUser (int userId)
        //{
        //    return new UserFacade().GetUserObsoleteForWok(userId);
        //}

        /// <summary>
        /// Get the community id from a user id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int GetCommunityIdFromUserId (int userId)
        {
            string sqlSelect = " SELECT community_id from communities_personal com WHERE com.creator_id = @userId ";


            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().ExecuteScalar(sqlSelect, parameters);
        }

        /// <summary>
        /// Get the user info
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataRow GetUser (int userId, bool fromGame)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;

            string sqlSelect = "SELECT COALESCE(p.player_id, 0) as player_id, COALESCE(pp.housing_zone_index, 0) as housing_zone_index, u.user_id, u.username, u.first_name, u.last_name, " +
                " u.description, u.gender, u.signup_date, " +
                " u.birth_date, u.country, u.zip_code, u.location, " +
                " u.last_login, u.second_to_last_login, " +
                " u.show_online, u.online, " +
                " u.friends_can_comment, u.everyone_can_comment, u.mature_profile, " +
                " us.number_of_logins, u.blast_privacy_permissions, u.blast_show_permissions, " +
                " com.thumbnail_path, com.community_id, com.template_id, com.name_no_spaces, com.url, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, " +
                " cs.number_of_diggs, cs.number_of_views, cs.number_times_shared " +
                " FROM " + dbNameForKaneva + ".users u " +
                " INNER JOIN " + dbNameForKaneva + ".communities_personal com ON com.creator_id = u.user_id " +
                " INNER JOIN " + dbNameForKaneva + ".users_stats us ON u.user_id = us.user_id " +
                " INNER JOIN " + dbNameForKaneva + ".channel_stats cs ON cs.channel_id = com.community_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".players p ON p.kaneva_user_id=@userId " +
                " LEFT OUTER JOIN " + dbNameForGame + ".player_presences pp ON p.player_id=pp.player_id " +
                " WHERE u.user_id = @userId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        public static string GetUserName (int userId)
        {
            string sqlSelect = "SELECT username " +
                " FROM users u " +
                " WHERE user_id = @userId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            DataRow row = KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
            return row == null ? null : row["username"].ToString ();
        }

        public static string GetUserGender (int userId)
        {
            string sqlSelect = "SELECT gender " +
                " FROM users u " +
                " WHERE user_id = @userId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            DataRow row = KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
            return row == null ? null : row["gender"].ToString ();
        }

        public static string GetUsersBlastPreferences (int userId)
        {
            string sqlSelect = "SELECT blast_privacy_permissions " +
                " FROM users u " +
                " WHERE user_id = @userId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            DataRow row = KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
            return row == null ? null : row["blast_privacy_permissions"].ToString ();
        }

        /// <summary>
        /// Get the user info
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataRow GetUser (string userName)
        {
            string sqlSelect = "SELECT u.user_id, u.username, u.age, u.role, u.account_type, u.first_name, u.last_name, u.registration_key, " +
                " u.description, u.gender, u.homepage, u.email, u.show_mature, u.status_id, u.signup_date, " +
                " u.birth_date, u.newsletter, u.public_profile, u.wok_player_id, u.over_21, u.blast_privacy_permissions, u.blast_show_permissions, " +
                " us.number_of_logins, us.number_inbox_messages, us.number_of_new_messages, us.number_outbox_messages, us.number_forum_posts, " +
                " browse_anonymously, u.country, zip_code, u.key_value, u.location, us.i_own, us.i_moderate, own_mod, " +
                " com.community_id, com.name_no_spaces, com.thumbnail_small_path " +
                " , u.salt, u.password " + // 4/08 added
                " FROM users u " +
                " INNER JOIN users_stats us ON u.user_id = us.user_id " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                " WHERE username = @userName ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userName", userName);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// Get the user info
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataRow GetUser (string email, string keyValue)
        {
            string sqlSelect = "SELECT user_id, username " +
                " FROM users u " +
                " WHERE email = @email " +
                " AND key_value = @keyValue ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@email", email);
            parameters.Add ("@keyValue", keyValue);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
        }

        /// <summary>
        /// returns user id and name
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        public static DataRow GetUserByPageId (int pageId)
        {
            string sqlSelect = " SELECT u.user_id, u.username " +
                    " FROM users u " +
                    " INNER JOIN communities c ON c.creator_id = u.user_id " +
                    " INNER JOIN layout_pages lp ON lp.channel_id = c.community_id " +
                    " WHERE lp.page_id = @page_id ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@page_id", pageId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);

        }

        /// <summary>
        /// Get total disk usage
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static double GetDiskUsage (int userId)
        {
            string sqlSelect = "SELECT COALESCE(SUM(file_size),0) " +
                " FROM assets WHERE owner_id = @userId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly ().ExecuteScalarDouble (sqlSelect, parameters);
        }

        /// <summary>
        /// Get the user info
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataRow GetUserByEmail (string email)
        {
            string sqlSelect = "SELECT u.user_id, username, role, account_type, first_name, last_name, registration_key, " +
                " description, gender, homepage, email, status_id, signup_date, key_value, " +
                " birth_date, newsletter, public_profile, " +
                " us.number_of_logins, us.number_inbox_messages, us.number_outbox_messages, us.number_forum_posts, " +
                " u.country, u.location " +
                " FROM users u " +
                " INNER JOIN users_stats us ON u.user_id = us.user_id " +
                " WHERE email = @email ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@email", email);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
        }

        /// <summary>
        /// Confirm this information is for a valid user
        /// </summary>
        /// <param name="email"></param>
        /// <param name="dtBirthate"></param>
        /// <param name="country"></param>
        /// <param name="zipCode"></param>
        /// <returns></returns>
        public static bool ConfirmUser (string email, DateTime dtBirthate, string country, string zipCode)
        {
            string sqlSelect = "SELECT user_id " +
                " FROM users u " +
                " WHERE email = @email " +
                " AND u.country = @country " +
                " AND u.zip_code = @zipCode " +
                " AND u.birth_date = @birthDate";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@email", email);
            parameters.Add ("@country", country);
            parameters.Add ("@zipCode", zipCode);
            parameters.Add ("@birthDate", dtBirthate);

            DataRow drUser = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
            return (drUser != null);
        }

        /// <summary>
        /// GetUsers
        /// </summary>
        public static PagedDataTable GetUsers(int userStatus, bool showMature, string filter, string orderby, int pageNumber, int pageSize)
        {
            string selectList = "u.user_id, u.username, u.role, u.first_name, u.last_name, u.gender, u.birth_date, " +
                " u.description, u.gender, u.homepage, u.email, u.status_id, u.signup_date, u.last_login, u.second_to_last_login, " +
                " u.country, u.zip_code, uss.name as user_status_name, u.display_name, " +
                " (u.online & u.show_online) as online, u.location, " +
                " us.number_of_logins, us.number_inbox_messages, us.number_outbox_messages, us.number_forum_posts, " +
                " com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, " +
                " cs.number_of_diggs, cs.number_of_views, cs.number_times_shared ";

            string tableList = " communities_personal com " +
                " INNER JOIN channel_stats cs ON cs.channel_id = com.community_id, " +
                " user_status uss, users u " +
                " INNER JOIN users_stats us ON u.user_id = us.user_id ";

            string whereClause = " u.status_id = uss.status_id " +
                " AND u.user_id = com.creator_id ";

            if (!showMature)
            {
                whereClause += " AND u.mature_profile = 0 ";
            }

            if (userStatus > 0)
            {
                whereClause += " AND u.status_id = " + userStatus;
            }

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            return KanevaGlobals.GetDatabaseUtilityReadOnly().GetPagedDataTable(selectList, tableList, whereClause, orderby, new Hashtable(), pageNumber, pageSize);
        }

        /// <summary>
        /// GetUsers
        /// </summary>
        public static PagedDataTable GetUsers (string username, string email, string firstName, string lastName, int userStatus, string filter, string orderby, int pageNumber, int pageSize)
        {
            Hashtable parameters = new Hashtable ();

            string selectList = "u.user_id, u.username, u.role, u.first_name, u.last_name, u.gender, u.birth_date, " +
                " u.description, u.gender, u.homepage, u.email, u.status_id, u.signup_date, u.last_login, u.second_to_last_login, " +
                " us.number_of_logins, us.number_inbox_messages, us.number_outbox_messages, us.number_forum_posts, " +
                " u.country, zip_code, uss.name as user_status_name, " +
                " (SELECT COUNT(*) from user_notes un WHERE un.user_id = u.user_id) as note_count ";

            string tableList = " user_status uss, users u " +
                " INNER JOIN users_stats us ON u.user_id = us.user_id ";

            string whereClause = " u.status_id = uss.status_id ";

            if (userStatus > 0)
            {
                whereClause += " AND u.status_id = " + userStatus;
            }

            if (username.Trim ().Length > 0)
            {
                parameters.Add ("@username", username.Trim () + "%");
                whereClause += " AND u.username like @username";
            }

            if (email.Trim ().Length > 0)
            {
                parameters.Add ("@email", email.Trim () + "%");
                whereClause += " AND u.email like @email";
            }

            if (firstName.Trim ().Length > 0)
            {
                parameters.Add ("@firstName", firstName.Trim () + "%");
                whereClause += " AND u.first_name like @firstName";
            }

            if (lastName.Trim ().Length > 0)
            {
                parameters.Add ("@lastName", lastName.Trim () + "%");
                whereClause += " AND u.last_name like @lastName";
            }

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            return KanevaGlobals.GetDatabaseUtilityReadOnly ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// GetUsers
        /// </summary>
        public static PagedDataTable GetUsers (DateTime dtStartDate, DateTime dtEndDate, string filter, string orderby, int pageNumber, int pageSize)
        {
            Hashtable parameters = new Hashtable ();

            string selectList = "u.user_id, u.username, u.role, u.first_name, u.last_name, u.gender, u.birth_date, " +
                " u.description, u.gender, u.homepage, u.email, u.status_id, u.signup_date, u.last_login, u.second_to_last_login, " +
                " us.number_of_logins, us.number_inbox_messages, us.number_outbox_messages, us.number_forum_posts, " +
                " u.country, zip_code, uss.name as user_status_name ";

            string tableList = " user_status uss, users u " +
                " INNER JOIN users_stats us ON u.user_id = us.user_id ";

            string whereClause = " u.status_id = uss.status_id " +
                " AND u.signup_date >= @startDate" +
                " AND u.signup_date <= @endDate";

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            parameters.Add ("@startDate", dtStartDate);
            parameters.Add ("@endDate", dtEndDate);
            return KanevaGlobals.GetDatabaseUtilityReadOnly ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// Get a count of all the users
        /// </summary>
        public static UInt64 GetUsersCount ()
        {
            UInt64 iCount = 0;
            string sqlSelect = "SELECT COUNT(*) as userCount from users";
            DataRow drCount = KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, false);

            if (drCount != null)
            {
                return Convert.ToUInt64 (drCount["userCount"]);
            }

            return iCount;
        }

        /// <summary>
        /// get users to migrate
        /// </summary>
        /// <returns></returns>
        public static DataTable GetUsersToMigrateImages ()
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sb = "SELECT u.user_id, u.avatar_path, u.avatar_custom, u.avatar_type, u.avatar, com.community_id " +
                " FROM users u, communities_personal com " +
                " WHERE u.avatar IS NOT NULL " +
                " AND u.user_id = com.creator_id ";

            Hashtable parameters = new Hashtable ();
            return dbUtility.GetDataTable (sb, parameters);
        }

        // Returns set of users signed up via a specific ip address within a specific period of time
        public static DataTable GetUsersByIp(string ipaddress, int minutes)
        {
            string sql = "";

            DateTime dtFrom = DateTime.Now.AddMinutes(-1 * minutes);

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilitySearch();
            Hashtable theParams = new Hashtable();

            sql = "SELECT user_id, username FROM kaneva.users u ";
            sql += " WHERE signup_date > @dtFrom  ";
            sql += " AND ( ip_address = INET_ATON(@ipaddress)  OR last_ip_address = INET_ATON(@ipaddress)) ";
            
            theParams.Add("@dtFrom", dtFrom);
            theParams.Add("@ipaddress", ipaddress);
                       
            return dbUtility.GetDataTable(sql, theParams);
        }


        /// <summary>
        /// Get all Users - used in migration
        /// </summary>
        public static DataTable GetUsersList ()
        {
            return GetUsersList (-1, null);
        }

        /// <summary>
        /// get simple user list based on a userID
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="filter"></param>
        public static DataTable GetUsersList (int userID)
        {
            return GetUsersList (userID, null);
        }

        /// <summary>
        /// get simple user list based on a user ID and filter
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="filter"></param>
        public static DataTable GetUsersList (int userID, string filter)
        {
            string sql = "";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();
            Hashtable theParams = new Hashtable ();

            //attach primary key to columns and attach items table to columns to form SQL string
            sql = "SELECT user_id, username from users u ";

            //limit by item id if one is provided
            if (userID > 0)
            {
                sql += " WHERE u.user_id = @userID ";
                theParams.Add ("@userID", userID);
            }

            //append the where clause if a filter is provided
            if ((filter != null) && (filter != ""))
            {
                if (userID > 0)
                {
                    sql += " AND " + filter;
                }
                else
                {
                    sql += " WHERE " + filter;
                }
            }

            sql += " ORDER BY username ASC";


            return dbUtility.GetDataTable (sql, theParams);

        }

        /// <summary>
        /// Get Users who haven't been in Wok by the Marketing Email Date - used to send automatic marketing emails
        /// </summary>
        public static DataTable GetUsersNoWokByMarketingEmailDate (int daysJoined, int daysSinceLastEmail, int limit)
        {
            string sqlSelect = " SELECT  u.user_id, u.email, coalesce(mer.date_sent, 0) as date_sent " +
                               " FROM kaneva.users u " +
                               " LEFT JOIN kaneva.marketing_email_records_by_users mer ON u.user_id = mer.user_id " +
                // validated email 
                               " WHERE u.status_id = 1  " +
                // singed up for newsletter 
                               " AND u.newsletter = 'Y' " +
                // have not been in WoK
                               " AND u.wok_player_id = 0 " +
                // for this email template
                               " AND (mer.email_type = 1 or mer.email_type is null) " +
                // has been at least N days since they joined
                               " AND u.signup_date < date_sub(DATE(NOW()),  INTERVAL @daysJoined DAY) " +
                // have not recieved this email in at least N days
                               " AND (mer.date_sent < date_sub(DATE(NOW()),  INTERVAL @daysSinceLastEmail DAY) or mer.date_sent is null) " +
                               " LIMIT @limit ;";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@daysJoined", daysJoined);
            parameters.Add ("@daysSinceLastEmail", daysSinceLastEmail);
            parameters.Add ("@limit", limit);

            return GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
        }

        /// <summary>
        /// Get Marketing Email Test Users
        /// </summary>
        public static DataTable GetUsersMarketingTest ()
        {

            string sqlSelect = "SELECT u.user_id, u.email, coalesce(mer.date_sent, 0) as date_sent FROM users u " +
                " LEFT JOIN kaneva.marketing_email_records_by_users mer ON u.user_id = mer.user_id  " +
                " WHERE email like 'cmullins@kaneva.com' " +
                /*
                " or email like 'jcobb@kaneva.com' " +                                              
                " or email like 'angie@dagibbyclan.com' " +         
                " or email like 'mmarcantel@kaneva.com' " +                
                " or email like 'cklaus@kaneva.com' " +
                " or email like 'gframe@kaneva.com' " +
                " or email like 'pwhite@kaneva.com' " +
                " or email like 'mnorwood@kaneva.com' " +
                " or email like 'asaha@kaneva.com' " +
                " or email like 'mwyner@kaneva.com' "+
                " or email like 'shubbard@kaneva.com' " +
                " or email like 'angiecgibson@yahoo.com' " +
                " or email like 'angiecgibson@gmail.com' " +
                " or email like 'angie.c.gibson@hotmail.com' " +
                " or email like 'jlongoria@kaneva.com' " +
                */
                " or email like 'rrogers@kaneva.com' " + 
                " or email like 'agibson@kaneva.com' GROUP BY user_id";


            Hashtable parameters = new Hashtable ();

            return GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
        }


        
        /// <summary>
        /// Get Users who haven't spent credits by the Marketing Email Date - used to send automatic marketing emails
        /// </summary>
        public static DataTable GetUsersNotSpentByMarketingEmailDate (int daysJoined, int daysSinceLastEmail, int limit)
        {
            string sqlSelect = " Select distinct wtl.user_id, email, coalesce(mer.date_sent, 0) as date_sent from kaneva.wok_transaction_log wtl " +
                    " Join users u on u.user_id = wtl.user_id " +
                    " LEFT JOIN kaneva.marketing_email_records_by_users mer ON u.user_id = mer.user_id   " +
                // No negative transactions
                    " where wtl.user_id NOT in (Select distinct user_id from kaneva.wok_transaction_log where transaction_amount < 0) " +
                // validated email 
                    " AND u.status_id = 1 " +
                // singed up for newsletter 
                    " AND u.newsletter = 'Y' " +
                // for this email template
                    " AND (mer.email_type = 2 or mer.email_type is null) " +
                // has been at least N days since they joined
                    " AND u.signup_date < date_sub(DATE(NOW()), INTERVAL @daysJoined DAY) " +
                // have not recieved this email in at least N days
                    " AND (mer.date_sent < date_sub(DATE(NOW()), INTERVAL  @daysSinceLastEmail DAY) or mer.date_sent is null)" +
                    " LIMIT @limit ;";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@daysJoined", daysJoined);
            parameters.Add ("@daysSinceLastEmail", daysSinceLastEmail);
            parameters.Add ("@limit", limit);

            return GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
        }


        /// <summary>
        /// Get Users for the newsletter
        /// </summary>
        public static DataTable GetUsersForNewsletter(int daysJoined, int daysSinceLastEmail, int limit)
        {
            string sqlSelect = " Select distinct u.user_id, email, coalesce(mer.date_sent, 0) as date_sent from  users u " +
                    " LEFT JOIN kaneva.marketing_email_records_by_users mer ON u.user_id = mer.user_id   " +
                // validated email 
                    " WHERE u.status_id = 1 " +
                // not a bounced email
                    " AND u.email_status = 0 " + 
                // and is for a specific subset of emails
                //    " AND u.email like '%yahoo.%' " +
                //# singed up for newsletter 
                    " AND u.newsletter = 'Y'  " +
                    " AND u.user_id NOT IN ( " +
                    " SELECT user_id FROM kaneva.marketing_email_records_by_users mer " +
                    " WHERE  (mer.email_type = " + (int)Constants.eMARKETING_EMAIL_TYPES.NEWSLETTER + " " +
                    " AND mer.date_sent > date_sub(DATE(NOW()), INTERVAL @daysSinceLastEmail DAY) or mer.date_sent is null) ) " + 
                    " GROUP BY u.user_id LIMIT @limit ;";

            Hashtable parameters = new Hashtable();
            parameters.Add("@daysSinceLastEmail", daysSinceLastEmail);
            parameters.Add("@limit", limit);

            return GetDatabaseUtility().GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// Get Users who have valideated and have newsletter = 'Y'
        /// </summary>
        public static DataTable GetUsersForPeopleNearbyEmail (int daysJoined, int daysSinceLastEmail, int limit)
        {

            string sqlSelect = "SELECT u.user_id, u.email, coalesce(mer.date_sent, 0) as date_sent FROM users u " +
                        " LEFT JOIN kaneva.marketing_email_records_by_users mer ON u.user_id = mer.user_id    " +
                        " INNER JOIN wok.game_users gu on u.user_id = gu.kaneva_user_id " +
                //# validated email 
                        " WHERE u.status_id = 1     " +
                //# singed up for newsletter 
                        " AND u.newsletter = 'Y'    " +
                //# for this email template
                        " AND (mer.email_type = 2 or mer.email_type is null)    " +
                //# has been at least N days since they joined
                        " AND u.signup_date < date_sub(DATE(NOW()), INTERVAL  @daysJoined DAY)    " +
                //# have not recieved this email in at least N days
                        " AND (mer.date_sent < date_sub(DATE(NOW()), INTERVAL  @daysSinceLastEmail DAY) or mer.date_sent is null) " +
                //# only regular users
                        " AND gu.time_played > 1000 Limit 4000 ;";


            Hashtable parameters = new Hashtable ();
            parameters.Add ("@daysJoined", daysJoined);
            parameters.Add ("@daysSinceLastEmail", daysSinceLastEmail);
            parameters.Add ("@limit", limit);

            return GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
        }

        public static PagedDataTable GetUserChanges (DateTime startDate, DateTime endDate, string filter, string orderby, int pageNumber, int pageSize)
        {
            string selectList = " user_id, username, username_old, display_name, display_name_old, zip_code, zip_code_old, " +
                "country, country_old, location, location_old, date_modified ";

            string tableList = "audit_users ";

            string whereClause = " date_modified >= @startDate" +
                " AND date_modified <= @endDate";

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@startDate", startDate);
            parameters.Add ("@endDate", endDate);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2 ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        public static PagedDataTable GetUserChanges (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            string selectList = " user_id, username, username_old, display_name, display_name_old, zip_code, zip_code_old, " +
                "country, country_old, location, location_old, date_modified ";

            string tableList = "audit_users ";

            string whereClause = " user_id = @userId";

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// GetUsers
        /// </summary>
        public static PagedDataTable GetUserNotes (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            string selectList = " un.note_id, un.user_id, un.note, un.created_datetime, un.created_user_id, u.username ";

            string tableList = "user_notes un, users u ";

            string whereClause = " un.user_id = @userId " +
                " AND un.created_user_id = u.user_id ";

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// Insert a user note
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static void InsertUserNote (int userId, int createdUserId, string note)
        {
            InsertUserNote (userId, createdUserId, note, 0);
        }
        public static void InsertUserNote (int userId, int createdUserId, string note, int wokTransLogId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            // Send the message
            string sql = "INSERT INTO user_notes " +
                "(user_id, note, created_datetime, created_user_id, wok_transaction_log_id " +
                ") VALUES (" +
                "@userId, @note, " + dbUtility.GetCurrentDateFunction () +
                ",@createdUserId, @wokTransLogId)";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@note", note);
            parameters.Add ("@createdUserId", createdUserId);
            parameters.Add ("@wokTransLogId", wokTransLogId);
            dbUtility.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// Get the user profile info
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataRow GetUserProfile (int userId)
        {
            string sqlSelect = "SELECT up.title_blast, u.description, up.hobbies, up.clubs, up.favorite_movies,up.favorite_tv, " +
                " up.favorite_music, up.favorite_sports, up.favorite_games, up.favorite_books, up.favorite_artists,	up.favorite_cars, " +
                " up.favorite_gadgets,   up.favorite_fashion, up.favorite_brands, up.role_models, up.like_to_meet, up.favorite_quotes, " +
                " up.show_favorites, up.show_prof, up.show_org, up.interests, up.prof_title, " +
                " up.prof_position, up.prof_industry, up.prof_company, up.prof_job, up.prof_skills, up.org_current_assoc, up.org_past_assoc, " +
                " up.relationship, up.orientation, up.religion, up.ethnicity, up.children, up.education, up.income, up.height_feet, up.height_inches, up.smoke, up.drink, " +
                " up.show_store, up.show_blog, up.show_games, up.show_bookmarks, up.hometown " +
                " FROM user_profiles up " +
                " INNER JOIN users u ON u.user_id = up.user_id " +
                " WHERE up.user_id = @userId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, true);
        }

        /// <summary>
        /// Get the user personal info
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataRow GetUserPersonalInfo (int userId)
        {
            string sqlSelect = "SELECT up.relationship, up.orientation, up.religion, up.ethnicity, " +
                " up.children, up.education, up.income, up.height_feet, up.height_inches, up.smoke, up.drink, up.hometown " +
                " FROM user_profiles up " +
                " INNER JOIN users u ON u.user_id = up.user_id " +
                " WHERE up.user_id = @userId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, true);
        }

        /// <summary>
        /// Get the user instant messages
        /// </summary>
        public static DataRow GetUserInstantMessages (int userId)
        {
            string sqlSelect = "SELECT im_username_1, im_type_1, im_security_1, " +
                " im_username_2, im_type_2, im_security_2, " +
                " im_username_3, im_type_3, im_security_3, " +
                " im_username_4, im_type_4, im_security_4 " +
                " FROM user_profiles " +
                " WHERE user_id = @userId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, true);
        }

        /// <summary>
        /// GetUserRatings
        /// </summary>
        public static PagedDataTable GetUserRatings (int userId, string orderby, int pageNumber, int pageSize)
        {
            string selectList = " uar.asset_rating_id , uar.rating, uar.created_date, uar.asset_id, a.name ";

            string tableList = " user_asset_ratings uar, assets a ";

            string whereClause = " a.asset_id = uar.asset_id " +
                " AND uar.user_id = @userId " +
                " AND uar.asset_rating_type = " + (int) Constants.eASSET_RATING_TYPE.RATING;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        public static int Authorize (string email, string password, int gameId, ref int roleMembership, string ipAddress, bool bSendTrackerMessage)
        {
            return Authorize (email, password, gameId, ref roleMembership, ipAddress, bSendTrackerMessage, false);
        }

        /// <summary>
        /// Authorize a user
        /// </summary>
        public static int Authorize (string email, string password, int gameId, ref int roleMembership, string ipAddress, bool bSendTrackerMessage, bool isTokenAuthorized)
        {
            string salt = null;
            string hashPassword = null;
            int userId = 0;
            int userStatus = 0;
            string userName = "";

            // Get the user from the databse
            if (!UsersUtility.GetUser (email, ref userName, ref hashPassword, ref salt, ref roleMembership, ref userId, ref userStatus))
            {
                return (int) Constants.eLOGIN_RESULTS.USER_NOT_FOUND;
            }

            // Set this value if user has logged in via Facebook and we have verified they have a Kaneva
            // Account and that account has already been connected to their FB account
            if (isTokenAuthorized) { hashPassword = "FaCeBoOkAuThOrIzE_OK"; }

            int ret = Authorize (userName, password, gameId, hashPassword, salt, userStatus);
            if (ret == (int) Constants.eLOGIN_RESULTS.INVALID_PASSWORD)
            {
                roleMembership = 0;
            }
            return ret;
        }

        /// <summary>
        /// more granular authorization called if already have user details (4/08)
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="hashPassword"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public static int Authorize(string userName, string password, int gameId, string hashPassword, string salt, int userStatus)
        {
            return Authorize(userName, password, gameId, hashPassword, salt, userStatus, false);
        }

        public static int Authorize(string userName, string password, int gameId, string hashPassword, string salt, int userStatus, bool allowGame )
        {
            // Get the hashed password
            string calcHashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(MakeHash(password + userName.ToLower()) + salt, "MD5");

            return Authorize(userName, calcHashPassword, gameId, hashPassword, userStatus, allowGame );
        }

        public static int Authorize(string userName, string calcHashPassword, int gameId, string hashPassword, int userStatus, bool allowGame )
        {
            UserFacade userFacade = new UserFacade();
            Constants.eLOGIN_RESULTS ret = Constants.eLOGIN_RESULTS.ACCOUNT_LOCKED;
            int userId = 0;

            // Was the correct password supplied?
            if (calcHashPassword == hashPassword || hashPassword == "FaCeBoOkAuThOrIzE_OK")
            {
                // if they are trying to authenticate against a game, check they have a subscription or bought it
                //if (gameId > 0 && !getDAO ().getGameAuthorization (userId, gameId))

                if (gameId > 0)
                {
                    // TODO: for STARs may need to do something for validating access, we now pass in gameId
                    //    return (int) Constants.eLOGIN_RESULTS.NO_GAME_ACCESS;
                }

        // Has this account been deleted?
                if (userStatus == (int)Constants.eUSER_STATUS.DELETED || userStatus == (int)Constants.eUSER_STATUS.DELETEDBYUS || userStatus == (int)Constants.eUSER_STATUS.DELETEDVALIDATED)
        {
                    return (int) Constants.eLOGIN_RESULTS.ACCOUNT_DELETED;
        }

                // check to see if the user has a record in the suspension table.

                // Has this account been locked?
                if (userStatus == (int)Constants.eUSER_STATUS.LOCKED || userStatus == (int)Constants.eUSER_STATUS.LOCKEDVALIDATED)
        {
                    // check to see if the user has a record in the suspension table.
                    userId = GetUserIdFromUsername(userName);
                    
                    List<UserSuspension> dtBans = userFacade.GetUserSuspensions(userId);

                    DateTime maxEndDate = DateTime.Now.AddDays(-1);

                    foreach (UserSuspension da in dtBans)
                    {
                        // do they have a record with a null end date?
                        // if so they are perma banned.
                        if (da.IsPermanent)
                        {
                            return (int)Constants.eLOGIN_RESULTS.ACCOUNT_LOCKED;
                        }

                        // keep track of the maximum end date so that we can use it later.
                        if (Convert.ToDateTime(da.BanEndDate) > maxEndDate)
                        {
                            maxEndDate = Convert.ToDateTime(da.BanEndDate);
                        }
                    }

                    // pessimistic
                    ret = Constants.eLOGIN_RESULTS.ACCOUNT_LOCKED;

                    // Only do the following if the user has an entry in the suspension table
                    // This way everyone who was locked/banned before this system was implemented
                    // will fall through correctly.
                    if (dtBans.Count > 0)
                    {
                        if (maxEndDate < DateTime.Now)
                        {
                            // The user has had a suspension, but not perma banned.
                            // The suspension has expired. Update their status and return success.
                            if (userStatus == 6 || userStatus == 7 || userStatus == 1)
                            {
                                // Set validated 
                                userFacade.UpdateUserStatus(userId, (int)Constants.eUSER_STATUS.REGVALIDATED);
                            }
                            else
                            {
                                // set to not validated
                                userFacade.UpdateUserStatus(userId, (int)Constants.eUSER_STATUS.REGNOTVALIDATED);
                            }

                            ret = Constants.eLOGIN_RESULTS.SUCCESS;
                        }
                    }
        }
                else
                    ret = Constants.eLOGIN_RESULTS.SUCCESS;
            }
            else
            {
                ret = Constants.eLOGIN_RESULTS.INVALID_PASSWORD;
            }

            FameFacade fameFacade = new FameFacade();
            GameFacade gameFacade = new GameFacade();
            Game owningGame = gameFacade.GetGameByGameId(gameId);

            // if they are allowing this game access, update the db to indicate
            // they shouldn't be prompted again
            if (ret == Constants.eLOGIN_RESULTS.SUCCESS && gameId > 0 && allowGame)
            {
                if (userId == 0)
                    userId = GetUserIdFromUsername(userName);

                if (!gameFacade.UserAllowedGame(gameId, userId))
                {
                    m_logger.Error("Failed to insert record into user_game_allowed table userName: " + userName + " userId: " + userId + " gameId: " + gameId);
                }            
            }
            else if (ret == Constants.eLOGIN_RESULTS.SUCCESS && gameId > 0)
            {
                if (owningGame != null && userId != owningGame.OwnerId && owningGame.ParentGameId != 0)
                {
                    fameFacade.RedeemPacket(owningGame.OwnerId, (int)PacketId.DEVELOPER_EVERY_10_VISITORS, (int)FameTypes.Creator);
                }
            }
            return (int)ret;
        }

        ///// <summary>
        ///// Get the user image
        ///// </summary>
        ///// <param name="userId"></param>
        ///// <returns></returns>
        //public static DataRow GetUserImage (int userId)
        //{
        //    string sqlSelect = "SELECT com.thumbnail_path, com.thumbnail_type, u.gender " +
        //        " FROM communities_personal com " +
        //        " INNER JOIN users u ON u.user_id = com.creator_id " +
        //        " WHERE com.creator_id = @userId ";

        //    Hashtable parameters = new Hashtable ();
        //    parameters.Add ("@userId", userId);

        //    DataTable dtResult = KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);

        //    if (dtResult.Rows.Count > 0)
        //    {
        //        return dtResult.Rows[0];
        //    }

        //    // Existing row was not found, get a new one
        //    return null;
        //}

        /// <summary>
        /// GetInterests
        /// </summary>
        /// <returns></returns>
        public static DataTable GetInterests (string startsWith, int category, int pageSize)
        {
            Hashtable parameters = new Hashtable ();
            string sqlSelect = "SELECT interest_id, ic_id, interest FROM interests WHERE interest LIKE @startsWith ";

            if (category > 0)
            {
                sqlSelect += " AND ic_id = @category";
                parameters.Add ("@category", category);
            }

            parameters.Add ("@startsWith", startsWith + "%");
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTableUnion(sqlSelect, " interest DESC ", parameters, 1, pageSize);
        }

        /// <summary>
        /// GetSchools
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSchools (string startsWith, int pageSize)
        {
            Hashtable parameters = new Hashtable ();
            string sqlSelect = "SELECT name FROM schools WHERE name LIKE @startsWith ";

            parameters.Add ("@startsWith", startsWith + "%");
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTableUnion(sqlSelect, " name DESC ", parameters, 1, pageSize);
        }

        /// <summary>
        /// GetFriends
        /// </summary>
        /// <returns></returns>
        public static DataTable GetFriends (string startsWith, int pageSize)
        {
            Hashtable parameters = new Hashtable ();
            string sqlSelect = "SELECT display_name FROM users WHERE name LIKE @startsWith ";

            parameters.Add ("@startsWith", startsWith + "%");
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTableUnion(sqlSelect, " name DESC ", parameters, 1, pageSize);
        }

        public static string MakeHash (string s)
        {
            // This is one implementation of the abstract class MD5.
            System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider ();

            System.Text.Encoder encoder = System.Text.ASCIIEncoding.UTF8.GetEncoder ();
            int byteCount = encoder.GetByteCount (s.ToCharArray (), 0, s.Length, true);
            byte[] bytes = new byte[byteCount];
            encoder.GetBytes (s.ToCharArray (), 0, s.Length, bytes, 0, true);
            byte[] result = md5.ComputeHash (bytes);

            string ret = "";
            for (int i = 0; i < result.Length; i++)
            {
                ret = ret + result[i].ToString ("x2");
            }
            return ret;
        }

        /// <summary>
        /// Get the communites the user belongs to, including the number of media files in the channel
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static PagedDataTable GetUserCommunitiesSummary (int userId, string orderby,
            string filter, int pageNumber, int pageSize)
        {
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            string selectList = " c.community_id, c.name, c.name_no_spaces, c.description, " +
                " c.is_public, c.thumbnail_small_path, " +
                " c.is_adult, c.over_21_required, c.creator_id, c.creator_username as username, c.keywords, " +
                " COUNT(a.asset_id) AS num_assets, cs.number_of_members ";

            string tableList = " community_members cm " +
                " INNER JOIN communities_public c ON c.community_id = cm.community_id " +
                " INNER JOIN channel_stats cs ON cs.channel_id = c.community_id " +
                " LEFT JOIN asset_channels assc ON assc.channel_id = c.community_id " +
                " LEFT JOIN vw_published_public_mature_assets a ON a.asset_id = assc.asset_id ";

            string whereClause = " cm.user_id = @userId " +
                " AND cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE +
                " AND c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE;

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }
            whereClause += " GROUP BY c.name, c.name_no_spaces, c.description, cs.number_of_members, " +
                " c.is_public, c.is_adult, c.creator_id, c.creator_username, c.keywords ";



            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// Get the communites the user belongs to
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static PagedDataTable GetUserCommunitiesInWorld (int userId, string orderby, string filter, bool onlyOwned, int pageNumber, int pageSize)
        {
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            string selectList = "DISTINCT cm.notifications, cm.item_notify, cm.item_review_notify, cm.blog_notify, cm.blog_comment_notify, cm.post_notify, cm.reply_notify, " +
                " cm.user_id as cm_user_id, cm.account_type_id, cm.status_id as cm_status_id, cm.added_date, " +
                " c.community_id, c.name, c.name_no_spaces, c.description, c.is_public, c.is_adult, c.creator_id, c.over_21_required, " +
                " c.creator_username as username, " +
                " c.keywords, c.keywords as community_keywords, c.is_personal, c.thumbnail_small_path, " +
                " cs.number_of_members, cs.number_times_shared, cs.number_of_diggs, " +
                " cs.number_of_views, cs.number_posts_7_days, 0 as number_of_topics, " +
                " ha.population as count, ha.zone_index, ha.zone_instance_id, ha.wok_raves, " +
                " ha.number_of_members AS wok_number_of_members, ha.status_id AS wok_status_id, ha.pass_group_id ";

            string tableList = " wok.worlds_cache ha, community_members cm, communities_public c " +
                " INNER JOIN channel_stats cs ON cs.channel_id = c.community_id ";

            string whereClause = "c.community_id = cm.community_id " +
                " AND ha.zone_type IN (3, 6) " +
                " AND ha.community_id = c.community_id " +
                " AND cm.user_id = @userId " +
                " AND cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE +
                " AND c.place_type_id < 99 ";

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            if ( onlyOwned)
            {
                whereClause += " AND c.creator_id = @userId ";
            }

            return KanevaGlobals.GetDatabaseUtility().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        public static PagedDataTable GetUserZonesForImport (int userId, string orderby, string filter, bool onlyOwned, int pageNumber, int pageSize)
        {
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            string selectList = "DISTINCT cm.notifications, cm.item_notify, cm.item_review_notify, cm.blog_notify, cm.blog_comment_notify, cm.post_notify, cm.reply_notify, " +
                " cm.user_id as cm_user_id, cm.account_type_id, cm.status_id as cm_status_id, cm.added_date, " +
                " c.community_id, c.name, c.name_no_spaces, c.description, c.is_public, c.is_adult, c.creator_id, c.over_21_required, " +
                " c.creator_username as username, " +
                " c.keywords, c.keywords as community_keywords, c.is_personal, c.thumbnail_small_path, " +
                " cs.number_of_members, cs.number_times_shared, cs.number_of_diggs, " +
                " cs.number_of_views, cs.number_posts_7_days, 0 as number_of_topics, " +
                " 0 as count,cz.zone_index, c.community_id as zone_instance_id, 0 as wok_raves, cs.number_of_members as wok_number_of_members, " +
                " c.status_id as wok_status_id, 0 as pass_group_id ";

            string tableList = " wok.channel_zones cz, community_members cm, communities_public c " +
                " INNER JOIN channel_stats cs ON cs.channel_id = c.community_id ";

            string whereClause = "c.community_id = cm.community_id " +
                " AND cz.zone_instance_id = c.community_id " +
                " AND cm.user_id = @userId " +
                " AND cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE +
                " AND c.place_type_id = 99 ";

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            if (onlyOwned)
            {
                whereClause += " AND c.creator_id = @userId ";
            }

            return KanevaGlobals.GetDatabaseUtilityReadOnly ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// Get the communites the user belongs to
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static PagedDataTable GetUserCommunities(int userId, string orderby, string filter, int pageNumber, int pageSize)
        {
            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);

            string selectList = "cm.notifications, cm.item_notify, cm.item_review_notify, cm.blog_notify, cm.blog_comment_notify, cm.post_notify, cm.reply_notify, " +
                " cm.user_id as cm_user_id, cm.account_type_id, cm.status_id as cm_status_id, cm.added_date, " +
                " c.community_id, c.name, c.name_no_spaces, c.description, c.is_public, c.is_adult, c.creator_id, c.over_21_required, " +
                " c.creator_username as username, c.place_type_id, " +
                " c.keywords, c.keywords as community_keywords, c.is_personal, c.thumbnail_small_path, " +
                " cs.number_of_members, cs.number_times_shared, cs.number_of_diggs, " +
                " cs.number_of_views, cs.number_posts_7_days, 0 as number_of_topics ";

            string tableList = " community_members cm, communities_public c " +
                " INNER JOIN channel_stats cs ON cs.channel_id = c.community_id ";

            string whereClause = "c.community_id = cm.community_id " +
                " AND cm.user_id = @userId " +
                " AND cm.status_id = " + (UInt32)CommunityMember.CommunityMemberStatus.ACTIVE +
                " AND c.status_id = " + (int)Constants.eCOMMUNITY_STATUS.ACTIVE;

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            return KanevaGlobals.GetDatabaseUtility().GetPagedDataTable(selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        public static DataTable GetCurrentPlaceInfo(int userId)
        {
            // Check if the user is in a 3D App first
            string sql =    "SELECT             c.name, " +
                            "                   c.creator_username, " +
                            "                   COALESCE(cs.number_of_diggs, 0) AS rave_count, " +
                            "                   (SELECT COUNT(*) FROM kaneva.channel_diggs cd WHERE cd.channel_id = c.community_id and cd.user_id = al.user_id) AS player_rave_count, " +
                            "                   (SELECT SUM(number_of_players) FROM developer.game_servers gs WHERE gs.game_id = al.game_id) AS population_count, " +
                            "                   c.thumbnail_medium_path, " +
                            "                   c.thumbnail_small_path, " +
                            "                   c.community_id, " +
                            "                   0 AS current_zone_type, " +
                            "                   c.is_adult, " +
                            "                   0 AS current_zone_index " +
                            "FROM               developer.active_logins al " +
                            "INNER JOIN         kaneva.communities_game cg " +
                            "ON                 al.game_id = cg.game_id " +
                            "INNER JOIN         kaneva.communities c " +
                            "ON                 cg.community_id = c.community_id " +
                            "LEFT OUTER JOIN    kaneva.channel_stats cs " +
                            "ON                 cs.channel_id = c.community_id " +
                            "WHERE       al.user_id = @userId AND al.game_id NOT IN (5316, 3298, 3296, 5310) ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);

            DataTable returnTable = KanevaGlobals.GetDatabaseUtility().GetDataTable(sql, parameters);

            if (returnTable != null && returnTable.Rows.Count > 0)
            {
                return returnTable;
            }
            else
            {
                sql =   "SELECT current_zone_type " +
                        "FROM wok.player_zones pz " + 
                        "INNER JOIN wok.players p " +
                        "ON p.player_id = pz.player_id " +
                        "WHERE p.kaneva_user_id = @userId and pz.server_id<>0";    // server_id==0 -> user offline -> no current place available

                parameters = new Hashtable();
                parameters.Add("@userId", userId);

                DataTable dt = KanevaGlobals.GetDatabaseUtility().GetDataTable(sql, parameters);

                if (dt != null && dt.Rows.Count > 0)
                {
                    int currentZoneType = Convert.ToInt32(dt.Rows[0]["current_zone_type"]);

                    switch (currentZoneType)
                    {
                        case Constants.BROADBAND_ZONE:
                            sql =   "SELECT          c.name, " +
                                    "                c.creator_username, " +
                                    "                COALESCE(cs.number_of_diggs, 0) AS rave_count, " +
                                    "                (SELECT COUNT(*) FROM kaneva.channel_diggs cd WHERE cd.channel_id = c.community_id and cd.user_id = p.kaneva_user_id) AS player_rave_count, " +
                                    "                IFNULL(sap.count,0) AS population_count, " +
                                    "                c.thumbnail_medium_path, " +
                                    "                c.thumbnail_small_path, " +
                                    "                c.community_id, " +
                                    "                pz.current_zone_type,  " +
                                    "                c.is_adult, " +
                                    "                pz.current_zone_index " +
                                    "FROM            wok.player_zones pz " +
                                    "INNER JOIN      wok.players p " +
                                    "ON              p.player_id = pz.player_id " +
                                    "INNER JOIN      wok.channel_zones cz " +
                                    "ON              cz.zone_index = pz.current_zone_index " +
                                    "AND             cz.zone_instance_id = pz.current_zone_instance_id " +
                                    "INNER JOIN      kaneva.communities c " +
                                    "ON              cz.zone_instance_id = c.community_id " +
                                    "LEFT OUTER JOIN kaneva.channel_stats cs " +
                                    "ON              cs.channel_id = c.community_id " +
                                    "LEFT OUTER JOIN wok.summary_active_population sap " +
                                    "ON              pz.current_zone_index = sap.zone_index " +
                                    "AND             pz.current_zone_instance_id = sap.zone_instance_id " +
                                    "WHERE           p.kaneva_user_id = @userId " +
                                    "AND             pz.current_zone_type = " + Constants.BROADBAND_ZONE;
                            break;
                        case Constants.PERMANENT_ZONE:
                            sql =   "SELECT          cz.name, " +
                                    "                '' AS creator_username, " +
                                    "                COALESCE(cs.number_of_diggs, 0) AS rave_count, " +
                                    "                (SELECT COUNT(*) FROM kaneva.channel_diggs cd WHERE cd.channel_id = cg.community_id and cd.user_id = p.kaneva_user_id) AS player_rave_count, " +
                                    "                IFNULL(sap.count,0) AS population_count, " +
                                    "                '' AS thumbnail_medium_path, " +
                                    "                '' AS thumbnail_small_path, " +
                                    "                COALESCE(cg.community_id, 0) AS community_id, " +
                                    "                pz.current_zone_type,  " +
                                    "                'N' AS is_adult, " +
                                    "                pz.current_zone_index " +
                                    "FROM            wok.player_zones pz " +
                                    "INNER JOIN      wok.players p " +
                                    "ON              p.player_id = pz.player_id " +
                                    "INNER JOIN      wok.channel_zones cz " +
                                    "ON              cz.zone_index = pz.current_zone_index " +
                                    "AND             cz.zone_instance_id = pz.current_zone_instance_id " +
                                    "LEFT OUTER JOIN developer.faux_3d_app f3da " +
                                    "ON              f3da.zone_index = pz.current_zone_index " +
                                    "LEFT OUTER JOIN kaneva.communities_game cg " +
                                    "ON              f3da.game_id = cg.game_id " +
                                    "LEFT OUTER JOIN kaneva.channel_stats cs " +
                                    "ON              cs.channel_id = cg.community_id " +
                                    "LEFT OUTER JOIN wok.summary_active_population sap " +
                                    "ON              pz.current_zone_index = sap.zone_index " +
                                    "AND             pz.current_zone_instance_id = sap.zone_instance_id " +
                                    "WHERE           p.kaneva_user_id = @userId " +
                                    "AND             pz.current_zone_type = " + Constants.PERMANENT_ZONE;
                            break;
                        // Temporarily uncommenting this case until apartments are converted to hangouts
                        case Constants.APARTMENT_ZONE:
                            sql = "SELECT          c.name, " +
                                    "                c.creator_username, " +
                                    "                COALESCE(cs.number_of_diggs, 0) AS rave_count, " +
                                    "                (SELECT COUNT(*) FROM kaneva.channel_diggs cd WHERE cd.channel_id = c.community_id and cd.user_id = p.kaneva_user_id) AS player_rave_count, " +
                                    "                IFNULL(sap.count,0) AS population_count, " +
                                    "                c.thumbnail_medium_path, " +
                                    "                c.thumbnail_small_path, " +
                                    "                c.community_id, " +
                                    "                pz.current_zone_type,  " +
                                    "                c.is_adult, " +
                                    "                pz.current_zone_index " +
                                    "FROM            wok.player_zones pz " +
                                    "INNER JOIN      wok.players p " +
                                    "ON              p.player_id = pz.player_id " +
                                    "INNER JOIN      wok.channel_zones cz " +
                                    "ON              cz.zone_index = pz.current_zone_index " +
                                    "AND             cz.zone_instance_id = pz.current_zone_instance_id " +
                                    "INNER JOIN      kaneva.communities c " +
                                    "ON              c.creator_id = cz.kaneva_user_id " +
                                    "AND             c.community_type_id = 5 " +
                                    "LEFT OUTER JOIN kaneva.channel_stats cs " +
                                    "ON              cs.channel_id = c.community_id " +
                                    "LEFT OUTER JOIN wok.summary_active_population sap " +
                                    "ON              pz.current_zone_index = sap.zone_index " +
                                    "AND             pz.current_zone_instance_id = sap.zone_instance_id " +
                                    "WHERE           p.kaneva_user_id = @userId " +
                                    "AND             pz.current_zone_type = " + Constants.APARTMENT_ZONE;
                            break;
                        default:
                            sql = null;
                            break;
                    }

                    if (sql != null)
                    {
                        parameters = new Hashtable();
                        parameters.Add("@userId", userId);

                        return KanevaGlobals.GetDatabaseUtility().GetDataTable(sql, parameters);
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public static bool IsUserOnlineInKIM (int userId)
        {
            string sqlSelect = "SELECT user_id " +
                " FROM im_users " +
                " WHERE user_id = @userId " +
                " AND online = 1";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            DataRow drUser = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
            return (drUser != null);
        }


        /// <summary>
        /// UpdatePlaceRave
        /// </summary>
        public static int UpdatePlaceRave(int userId, int numRaves, RaveType raveType)
        {
            DataTable returnTable = GetCurrentPlaceInfo(userId);

            if (returnTable != null && returnTable.Rows.Count > 0)
            {
                DataRow dr = returnTable.Rows[0];

                if (dr != null)
                {
                    int returnVal = -1;
                    RaveFacade raveFacade = new RaveFacade();

                    if (raveFacade.GetCommunityRaveCountByUser(userId, Convert.ToInt32(dr["community_id"])) == 0)
                    {
                        returnVal = raveFacade.RaveCommunity(userId, Convert.ToInt32(dr["community_id"]), raveType, string.Empty);
                    }
     
                    return returnVal;
                }
            }

            return -1;
        }

        /// <summary>
        /// UpdateChannelPreferences
        /// </summary>
        public static int UpdateChannelPreferences (int userId, int channelId, int notifications,
            int itemNotify, int itemReviewNotify, int blogNotify, int blogCommentNotify, int postNotify, int replyNotify)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlUpdate = "UPDATE community_members " +
                " SET notifications = @notifications, " +
                " item_notify = @itemNotify, " +
                " item_review_notify = @itemReviewNotify, " +
                " blog_notify = @blogNotify, " +
                " blog_comment_notify = @blogCommentNotify, " +
                " post_notify = @postNotify, " +
                " reply_notify = @replyNotify " +
                " WHERE user_id = @userId " +
                " AND community_id = @channelId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@channelId", channelId);
            parameters.Add ("@notifications", notifications);
            parameters.Add ("@itemNotify", itemNotify);
            parameters.Add ("@itemReviewNotify", itemReviewNotify);
            parameters.Add ("@blogNotify", blogNotify);
            parameters.Add ("@blogCommentNotify", blogCommentNotify);
            parameters.Add ("@postNotify", postNotify);
            parameters.Add ("@replyNotify", replyNotify);

            return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// GetUserMessages
        /// </summary>
        public static PagedDataTable GetUserMessages (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            string selectList = "com.thumbnail_small_path, com.thumbnail_square_path, com.name_no_spaces, m.message_id, m.from_id, m.to_id, m.subject, " +
                " m.message, m.message_date, m.replied, m.type, m.to_viewable, m.from_viewable, u.username, " +
                " m.channel_id, NOW() as CurrentDate, COALESCE(ufs.fb_user_id, 0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture, 0) AS use_facebook_profile_picture ";

            string tableList = "messages m, users u " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id, communities_personal com ";

            string whereClause = "m.from_id = u.user_id " +
                " AND m.to_id = @userId " +
                " AND m.to_viewable != 'D' " +
                " AND u.user_id = com.creator_id ";

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// GetUserMessages
        /// </summary>
        public static PagedDataTable GetSentMessages (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            string selectList = "com.thumbnail_small_path, com.thumbnail_square_path, com.name_no_spaces, m.message_id, m.from_id, m.to_id, m.subject, " +
                " m.message, m.message_date, m.replied, m.type, m.to_viewable, m.from_viewable, u.username, NOW() as CurrentDate, " +
                " COALESCE(ufs.fb_user_id, 0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture, 0) AS use_facebook_profile_picture ";

            string tableList = "messages m, users u " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id, communities_personal com ";

            string whereClause = "m.to_id = u.user_id " +
                " AND m.from_id = @userId " +
                " AND m.from_viewable = 'S' " +
                " AND u.user_id = com.creator_id ";

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// GetUserMessage
        /// </summary>
        public static DataRow GetUserMessage (int userId, int messageId)
        {
            string sqlSelect = "SELECT com.thumbnail_small_path, com.name_no_spaces, m.message_id, m.from_id, m.to_id, m.subject, " +
                " m.message, m.message_date, m.replied, m.type, m.to_viewable, m.from_viewable, u.username ";

            sqlSelect += "FROM messages m, users u, communities_personal com ";

            sqlSelect += "WHERE m.from_id = u.user_id " +
                " AND (" +
                        " (" +
                            " m.to_id = @userId " +
                            " AND m.to_viewable != 'D'" +
                        ")" +
                    " OR (" +
                            " m.from_id = @userId " +
                            " AND m.from_viewable != 'D'" +
                        ")" +
                    ")" +

                " AND m.message_id = @messageId" +
                " AND u.user_id = com.creator_id ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@messageId", messageId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, true);
        }

        /// <summary>
        /// GetSentNonFriendMessageCount
        /// </summary>
        public static DataRow GetSentNonFriendMessageCount (int userId)
        {
            string sqlSelect = "SELECT number_of_messages_sent, last_message_sent_date " +
                " FROM messages_non_friends " +
                " WHERE user_id = @userId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, true);
        }

        /// <summary>
        /// UpdateNumberNonFriendMessagesSent
        /// </summary>
        public static void UpdateNumberNonFriendMessagesSent (int userId, bool resetCount)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            
            string sqlString = "UPDATE messages_non_friends " +
                " SET number_of_messages_sent = " + (resetCount == true ? "" : "number_of_messages_sent + ") + " 1, " +
                " last_message_sent_date = " + dbUtility.GetCurrentDateFunction () +
                " WHERE user_id = @userId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// InsertNonFriendMessageSentCount
        /// </summary>
        public static void InsertNonFriendMessageSentCount (int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            // Send the message
            string sql = "INSERT INTO messages_non_friends " +
                "(user_id, number_of_messages_sent, last_message_sent_date " +
                ") VALUES (" +
                "@userId, 1, " + dbUtility.GetCurrentDateFunction () +
                ")";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            dbUtility.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// ConfirmCanSendNonFriendPM
        /// </summary>
        public static int ConfirmCanSendNonFriendPM (int userId, int toId)
        {
            UserFacade userFacade = new UserFacade();

            try
            {
                if (!userFacade.AreFriends(userId, toId))
                {
                    ////// Check to see if sender is above fame level 24
                    ////FameFacade fameFacade = new FameFacade();
                    ////UserFame userFame = fameFacade.GetUserFame(userId, (int)FameTypes.World);

                    ////if (userFame.CurrentLevel.LevelNumber > 23)
                    ////{
                    ////    // Allowed to send a PM
                    ////    return 0;
                    ////}


                    // check to see how many non-friend messages sent today
                    DataRow drMsgCount = UsersUtility.GetSentNonFriendMessageCount (userId);

                    if (drMsgCount != null && drMsgCount.Table.Rows.Count == 1)
                    {
                        // check to see if user has sent any other messages today
                        if (((DateTime) drMsgCount["last_message_sent_date"]).Date == DateTime.Today)
                        {
                            // is user over limit of non-friend messages sent?
                            if (Convert.ToInt32 (drMsgCount["number_of_messages_sent"]) + 1 > KanevaGlobals.MaxNumberNonFriendMessagesPerDay)
                            {
                                // can not send any more non-friend messages today, user exceeded limit
                                return Constants.ERR_USER_EXCEEDED_NON_FRIEND_MESSAGE_COUNT;
                            }
                            else
                            {   // increment counter
                                UsersUtility.UpdateNumberNonFriendMessagesSent (userId, false);
                            }
                        }
                        else
                        {   // reset the counter to 1 since this is a new day
                            UsersUtility.UpdateNumberNonFriendMessagesSent (userId, true);
                        }
                    }
                    else
                    {   // insert row for user
                        UsersUtility.InsertNonFriendMessageSentCount (userId);
                    }
                }

                return 0;
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error checking Non-Friend Message Sent Count: userId=" + userId.ToString() + " : ", ex);
                return -1;
            }
        }

        /// <summary>
        /// GetBulletin
        /// </summary>
        public static DataRow GetBulletin (int userId, int bulletinId)
        {
            string sqlSelect = "SELECT com.thumbnail_small_path, com.name_no_spaces, b.bulletin_id, b.from_id, b.from_status_id, b.subject, " +
                " b.message, b.message_date, u.username ";

            sqlSelect += "FROM bulletins b, users u, communities_personal com ";

            sqlSelect += "WHERE b.from_id = u.user_id " +
                " AND b.bulletin_id = @bulletinId" +
                " AND u.user_id = com.creator_id ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@bulletinId", bulletinId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, true);
        }

        public const int USER_IS_BLOCKED = -99;
        public const int INSUFFICIENT_FUNDS = -98;
        public const int GIVER_IS_RECEIVER = -97;

        /// <summary>
        /// Update a user sent private message
        /// </summary>
        public static int DeleteSentMessage (int userId, int messageId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            string sqlString = "CALL update_messages_from_id_status (@messageId, @fromId, @fromViewable) ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@messageId", messageId);
            parameters.Add("@fromId", userId);
            parameters.Add("@fromViewable", 'D');
            return dbUtility.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// Update a user sent private message
        /// </summary>
        public static int ReadMessage (int userId, int messageId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            string sqlString = "CALL update_messages_to_id_status (@messageId, @toViewable, @toId) ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@messageId", messageId);
            parameters.Add("@toId", userId);
            parameters.Add("@toViewable", 'R');
            return dbUtility.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// Update a user sent private message
        /// </summary>
        public static int DeleteMessage(int userId, int messageId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            string sqlString = "CALL update_messages_to_id_status (@messageId, @toViewable, @toId) ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@messageId", messageId);
            parameters.Add("@toId", userId);
            parameters.Add("@toViewable", 'D');
            return dbUtility.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// Update a user private message
        /// </summary>
        public static int UpdateMessageReply (int userId, int messageId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            string sqlString = "CALL update_messages_replied (@messageId) ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@messageId", messageId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// DeleteMessagePermenent - NOTE, we should only do this if both from_status_id and to_status_id is DELETED
        /// </summary>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public static int DeleteMessagePermenent (int messageId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "DELETE FROM messages where message_id = @messageId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@messageId", messageId);

            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// IsMessageRecipient
        /// </summary>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public static bool IsMessageRecipient (int messageId, int userId)
        {
            string sqlSelect = "SELECT message_id " +
                " FROM messages " +
                " WHERE message_id = @messageId " +
                " AND (to_id = @userId OR from_id = @userId)";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@messageId", messageId);
            parameters.Add ("@userId", userId);

            DataRow drUser = KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
            return (drUser != null);
        }

        /// <summary>
        /// Get awards data for inviting a user
        /// </summary>
        /// <returns></returns>
        public static DataRow GetInvitePointAwards ()
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            // Get Invite Point awards
            string sqlSelect = "SELECT invite_point_id, point_award_amount, kei_point_id, end_date " +
                " FROM invite_point_awards " +
                " WHERE (end_date >= " + dbUtility.GetCurrentDateFunction () + " OR end_date IS NULL)";

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, false);
        }

        /// <summary>
        /// Get awards data for inviting a user
        /// </summary>
        /// <returns></returns>
        public static DataRow GetInvitePointAwards (Constants.eINVITE_POINT_AWARD_TYPE invitePointId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            // Get Invite Point awards
            string sqlSelect = "SELECT invite_point_id, point_award_amount, kei_point_id, ip_limit, award_multiple, description " +
                " FROM invite_point_awards " +
                " WHERE invite_point_id = @invitePointId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@invitePointId", (int) invitePointId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
        }

        public static int InsertInvite (int userId, string toEmail, string keyValue, Double pointsAwarded, string KeiPointIdAwarded)
        {
            return InsertInvite (userId, toEmail, "", keyValue, pointsAwarded, KeiPointIdAwarded, -1, 0);
        }
        public static int InsertInvite (int userId, string toEmail, string keyValue, Double pointsAwarded, string KeiPointIdAwarded, int inviteSourceId)
        {
            return InsertInvite (userId, toEmail, "", keyValue, pointsAwarded, KeiPointIdAwarded, -1, inviteSourceId);
        }
        public static int InsertInvite (int userId, string toEmail, string toName, string keyValue, Double pointsAwarded, string KeiPointIdAwarded)
        {
            return InsertInvite (userId, toEmail, toName, keyValue, pointsAwarded, KeiPointIdAwarded, -1, 0);
        }
        public static int InsertInvite (int userId, string toEmail, string toName, string keyValue, Double pointsAwarded, string KeiPointIdAwarded, int channelId)
        {
            return InsertInvite (userId, toEmail, toName, keyValue, pointsAwarded, KeiPointIdAwarded, channelId, 0);
        }
        /// <summary>
        /// Insert a Invite
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int InsertInvite (int userId, string toEmail, string toName, string keyValue, Double pointsAwarded, string KeiPointIdAwarded, int channelId, int inviteSourceId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlInsert = "INSERT INTO invites " +
                "(user_id, email, to_name, channel_id, key_value, invite_status_id, " +
                " invited_date, points_awarded, kei_point_id_awarded, reinvite_date, email_hash ";
            string sqlValues = ") VALUES (" +
                "@userId, @email, @toName, @channel_id, @keyValue, " + (int) Constants.eINVITE_STATUS.UNREGISTER + "," +
                dbUtility.GetCurrentDateFunction () + ", @pointsAwarded, @KeiPointIdAwarded," + dbUtility.GetCurrentDateFunction () + ", UNHEX(MD5(@email)) ";

            if (inviteSourceId > 0)
            {
                sqlInsert += ", invites_source_id ";
                sqlValues += ", @inviteSourceId";
                parameters.Add ("@inviteSourceId", inviteSourceId);
            }

            string sql = sqlInsert + sqlValues + ")";

            parameters.Add ("@userId", userId);
            parameters.Add ("@email", toEmail);
            parameters.Add ("@toName", toName);
            parameters.Add ("@keyValue", keyValue);
            parameters.Add ("@pointsAwarded", pointsAwarded);
            parameters.Add ("@KeiPointIdAwarded", KeiPointIdAwarded);
            parameters.Add ("@channel_id", channelId > 0 ? channelId : 0);

            int inviteId = 0;
            dbUtility.ExecuteIdentityInsert (sql, parameters, ref inviteId);

            return inviteId;
        }

        /// <summary>
        /// Get Invite
        /// </summary>
        /// <param name="inviteId"></param>
        /// <returns></returns>
        public static DataRow GetInvite (string email, int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();
            Hashtable parameters = new Hashtable ();

            string sqlSelect = "SELECT invite_id, user_id, email, to_name, channel_id, key_value, invite_status_id, " +
                " invited_date, points_awarded, kei_point_id_awarded, reinvite_date " +
                " FROM invites " +
                " WHERE user_id = @userId " +
                " AND email = @email";

            parameters.Add ("@email", email);
            parameters.Add ("@userId", userId);
            return dbUtility.GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// Get Invite
        /// </summary>
        /// <param name="inviteId"></param>
        /// <returns></returns>
        public static DataRow GetInvite (int inviteId)
        {
            return GetInvite (inviteId, "");
        }


        /// <summary>
        /// Get Invite
        /// </summary>
        public static DataRow GetInvite (int inviteId, string keyValue)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();
            Hashtable parameters = new Hashtable ();

            string sqlSelect = "SELECT invite_id, user_id, email, to_name, channel_id, key_value, invite_status_id, " +
                " invited_date, points_awarded, kei_point_id_awarded, reinvite_date " +
                " FROM invites " +
                " WHERE invite_id = @inviteId ";

            if (keyValue.Length > 0)
            {
                parameters.Add ("@keyValue", keyValue);
                sqlSelect += " AND key_value = @keyValue";
            }

            parameters.Add ("@inviteId", inviteId);
            return dbUtility.GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// GetInviteDeclineReasons
        /// </summary>
        /// <returns></returns>
        public static DataTable GetInviteDeclineReasons ()
        {
            string sqlSelect = "SELECT invite_decline_reason_id, name, description " +
                " FROM invite_decline_reasons " +
                " ORDER BY name";

            Hashtable parameters = new Hashtable ();
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// Get Invite
        /// </summary>
        public static DataRow GetInviteByUserInvited (int invitedUserId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();
            Hashtable parameters = new Hashtable ();

            string sqlSelect = "SELECT invite_id, user_id, email, invite_status_id " +
                " FROM invites " +
                " WHERE invited_user_id = @invitedUserId ";

            parameters.Add ("@invitedUserId", invitedUserId);
            return dbUtility.GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// Get Invite
        /// </summary>
        public static DataRow GetDeclinedInviteInfo (int inviteId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();
            Hashtable parameters = new Hashtable ();

            string sqlSelect = "SELECT invite_id, invite_decline_reason_id, decline_date, additional_information " +
                " FROM invite_declines " +
                " WHERE invite_id = @inviteId ";

            parameters.Add ("@inviteId", inviteId);
            return dbUtility.GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// Get all invites for a user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static PagedDataTable GetAllInvites (int userId, string filter, string orderBy, int pageNumber, int pageSize)
        {
            string strQuery = "";

            strQuery = "SELECT invite_id, user_id, email, channel_id, key_value, invite_status_id, " +
                " invited_date, points_awarded, kei_point_id_awarded, reinvite_date, attempts, NOW() as CurrentDate ";

            strQuery += " FROM invites ";

            strQuery += " WHERE user_id = @userId";

            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTableUnion(strQuery, orderBy, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// update status and invited user id in an invite record
        /// </summary>
        /// <param name="inviteId"></param>
        /// <param name="status"></param>
        /// <param name="invitedUserId"></param>
        /// <returns></returns>
        public static DataRow UpdateInvite (int inviteId, int status, int invitedUserId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlSelect = "UPDATE invites " +
                " SET invited_user_id = @invitedUserId, " +
                " invite_status_id = @status " +
                " WHERE invite_id = @invite_id ";

            parameters.Add ("@invite_id", inviteId);
            parameters.Add ("@status", status);
            parameters.Add ("@invitedUserId", invitedUserId > 0 ? invitedUserId : 0);
            return dbUtility.GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// update status and invited user id in an invite record
        /// </summary>
        /// <param name="inviteId"></param>
        /// <param name="status"></param>
        /// <param name="invitedUserId"></param>
        /// <returns></returns>
        public static int UpdateInvite (int inviteId, int status)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlUpdate = "UPDATE invites " +
                " SET invite_status_id = @status " +
                " WHERE invite_id = @invite_id ";

            parameters.Add ("@invite_id", inviteId);
            parameters.Add ("@status", status);
            return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// update opened invite field to show user opened invite email
        /// </summary>
        /// <param name="inviteId"></param>
        /// <returns></returns>
        public static int UpdateInviteClick (int inviteId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlUpdate = "UPDATE invites " +
                " SET clicked_accept_link = 1 " +
                " WHERE invite_id = @invite_id ";

            parameters.Add ("@invite_id", inviteId);
            return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// Inserts an invite decline into invite_declines table
        /// </summary>
        /// <param name="inviteId"></param>
        /// <param name="declineReasonId"></param>
        /// <param name="additionalInfo"></param>
        /// <returns></returns>
        public static int InsertInviteDecline (int inviteId, int declineReasonId, string additionalInfo)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sql = "INSERT INTO invite_declines " +
                "(invite_id, invite_decline_reason_id, decline_date, additional_information " +
                ") VALUES (" +
                "@inviteId, @declineReasonId, " + dbUtility.GetCurrentDateFunction () + ", @additionalInfo)";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@inviteId", inviteId);
            parameters.Add ("@declineReasonId", declineReasonId);
            parameters.Add ("@additionalInfo", additionalInfo);

            int declineId = 0;
            dbUtility.ExecuteIdentityInsert (sql, parameters, ref declineId);

            return declineId;
        }

        /// <summary>
        /// update status and invited user id in an invite record
        /// </summary>
        /// <param name="inviteId"></param>
        /// <param name="status"></param>
        /// <param name="invitedUserId"></param>
        /// <returns></returns>
        public static int ResendInvite (int inviteId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlUpdate = "UPDATE invites " +
                " SET reinvite_date = " + dbUtility.GetCurrentDateFunction () +
                " ,attempts = attempts+1 " +
                " WHERE invite_id = @inviteId ";

            parameters.Add ("@inviteId", inviteId);
            return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// InsertInviteIP
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        private static int InsertInviteIP (string ipAddress)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            // Does this IP already exist?
            string sqlSelect = "SELECT ip_address " +
                " FROM invite_ip_addresses " +
                " WHERE ip_address = INET_ATON(@ipAddress) ";

            parameters.Add ("@ipAddress", ipAddress);
            DataRow drInviteIpAddresses = dbUtility.GetDataRow (sqlSelect, parameters, false);

            if (drInviteIpAddresses == null)
            {
                // Insert new record
                string sql = "INSERT INTO invite_ip_addresses " +
               "(ip_address, number_of_times " +
               ") VALUES (" +
               " INET_ATON(@ipAddress), 0 )";

                dbUtility.ExecuteNonQuery (sql, parameters);
            }
            else
            {
                // Increment count
                string sqlUpdate = "UPDATE invite_ip_addresses " +
                " SET number_of_times = number_of_times + 1 " +
                " WHERE ip_address = INET_ATON(@ipAddress) ";

                return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
            }

            return 0;
        }

        /// <summary>
        /// IsOverMaxInviteIPCount
        /// </summary>
        private static bool IsOverMaxInviteIPCount (int maxCount, string ipAddress)
        {
            string sqlSelect = "SELECT number_of_times " +
                " FROM invite_ip_addresses " +
                " WHERE ip_address = INET_ATON(@ipAddress) ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@ipAddress", ipAddress);
            int number_of_times = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlSelect, parameters);

            return (number_of_times > maxCount);
        }

        /// <summary>
        /// UpdateNumberInvitesAccepted
        /// </summary>
        /// <param name="userId"></param>
        private static void UpdateNumberInvitesAccepted (int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            string sqlUpdate = "UPDATE users_stats " +
                    " SET number_invites_accepted = number_invites_accepted + 1 " +
                    " WHERE user_id = @userId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// UpdateNumberInvitesInWorld
        /// </summary>
        /// <param name="userId"></param>
        private static void UpdateNumberInvitesInWorld (int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            string sqlUpdate = "UPDATE users_stats " +
                    " SET number_invites_in_world = number_invites_in_world + 1 " +
                    " WHERE user_id = @userId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// UpdateNumberFriendInviteRewards
        /// </summary>
        /// <param name="userId"></param>
        public static void UpdateNumberFriendInviteRewards (int userId, double rewards)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            string sqlUpdate = "UPDATE users_stats " +
                    " SET number_friend_invite_rewards = number_friend_invite_rewards + @rewards " +
                    " WHERE user_id = @userId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@rewards", rewards);
            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// Perform logic for Invite Awards when user registers
        /// </summary>
        public static bool AwardInviteRegister(int inviteSentByUserId, string ipAddress, int inviteId, int userIdRegistered)
        {
            // Get the reward settings
            DataRow drInvitePointAwards = UsersUtility.GetInvitePointAwards (Constants.eINVITE_POINT_AWARD_TYPE.REGISTRATION);

            // Make sure they are not over the IP limit - only do for registrations
            UsersUtility.InsertInviteIP (ipAddress);

            if (drInvitePointAwards == null)
            {
                m_logger.Error ("Award type eINVITE_POINT_AWARD_TYPE.REGISTRATION not found");
                return false;
            }
            else
            {
                if (UsersUtility.IsOverMaxInviteIPCount (Convert.ToInt32 (drInvitePointAwards["ip_limit"]), ipAddress))
                {
                    // Mark status as max IP
                    UsersUtility.UpdateInvite (inviteId, (int) Constants.eINVITE_STATUS.MAX_IP);
                }
                else
                {
                    // Update number of invited accepted
                    UsersUtility.UpdateNumberInvitesAccepted (inviteSentByUserId);

                    // See if it is correct multiple
                    UserFacade userFacade = new UserFacade ();
                    User userInviteSentBy = userFacade.GetUser (inviteSentByUserId);

                    uint iNumberInvitesAccepted = userInviteSentBy.Stats.NumberInvitesAccepted;
                    uint iAwardMultiple = Convert.ToUInt32 (drInvitePointAwards["award_multiple"]);

                    if (iNumberInvitesAccepted % iAwardMultiple == 0)
                    {
                        // Award the creds
                        int orderId = StoreUtility.CreateOrder (inviteSentByUserId, ipAddress, (int) Constants.eTRANSACTION_STATUS.CHECKOUT);
                        StoreUtility.SetPurchaseType (orderId, Constants.ePURCHASE_TYPE.INVITE_AWARD);

                        int transactionId = StoreUtility.PurchasePoints (inviteSentByUserId, (int) Constants.eTRANSACTION_STATUS.VERIFIED, drInvitePointAwards["description"].ToString (), 0, (int) Constants.ePAYMENT_METHODS.INVITE_USER, 0.0, 0.0, Convert.ToDouble (drInvitePointAwards["point_award_amount"]), 0, ipAddress);
                        Double awardAmount = Convert.ToDouble (drInvitePointAwards["point_award_amount"]);
                        (new UserFacade()).AdjustUserBalance(inviteSentByUserId, drInvitePointAwards["kei_point_id"].ToString(), awardAmount, Constants.CASH_TT_SPECIAL);

                        // Update New Rewards Balance in User Stats
                        UsersUtility.UpdateNumberFriendInviteRewards (inviteSentByUserId, awardAmount);

                        // Set the order id on the point purchase transaction
                        StoreUtility.UpdatePointTransaction (transactionId, orderId);
                        StoreUtility.UpdateOrderPointTranasactionId (orderId, transactionId);
                        StoreUtility.UpdateOrderToComplete (orderId, inviteSentByUserId);
                    }

                    // Add them to your crew
                    userFacade.AddCrewPeep(inviteSentByUserId, userIdRegistered);
                    // Also do the reverse
                    userFacade.AddCrewPeep(userIdRegistered, inviteSentByUserId);
                }
            }

            return true;
        }

        /// <summary>
        /// Perform logic for Invite Awards when invity logs in to WOK
        /// </summary>
        public static bool AwardsInvitesInWorld (int inviteSentByUserId, string ipAddress, int inviteId)
        {
            // Get the reward settings
            DataRow drInvitePointAwards = UsersUtility.GetInvitePointAwards (Constants.eINVITE_POINT_AWARD_TYPE.WOK_LOGIN);

            if (drInvitePointAwards == null)
            {
                m_logger.Error ("Award type eINVITE_POINT_AWARD_TYPE.WOK_LOGIN not found");
                return false;
            }
            else
            {
                // Update number of invited accepted
                UsersUtility.UpdateNumberInvitesInWorld (inviteSentByUserId);

                // See if it is correct multiple
                UserFacade userFacade = new UserFacade ();
                User userInviteSentBy = userFacade.GetUser (inviteSentByUserId);

                uint iNumberInvitesInWorld = userInviteSentBy.Stats.NumberInvitesInWorld;
                uint iAwardMultiple = Convert.ToUInt32 (drInvitePointAwards["award_multiple"]);

                if (iNumberInvitesInWorld % iAwardMultiple == 0)
                {
                    // Award the creds
                    int orderId = StoreUtility.CreateOrder (inviteSentByUserId, ipAddress, (int) Constants.eTRANSACTION_STATUS.CHECKOUT);
                    StoreUtility.SetPurchaseType (orderId, Constants.ePURCHASE_TYPE.INVITE_AWARD);

                    int transactionId = StoreUtility.PurchasePoints (inviteSentByUserId, (int) Constants.eTRANSACTION_STATUS.VERIFIED, drInvitePointAwards["description"].ToString (), 0, (int) Constants.ePAYMENT_METHODS.INVITE_USER, 0.0, 0.0, Convert.ToDouble (drInvitePointAwards["point_award_amount"]), 0, ipAddress);
                    (new UserFacade()).AdjustUserBalance(inviteSentByUserId, drInvitePointAwards["kei_point_id"].ToString(), Convert.ToDouble(drInvitePointAwards["point_award_amount"]), Constants.CASH_TT_SPECIAL);

                    // Set the order id on the point purchase transaction
                    StoreUtility.UpdatePointTransaction (transactionId, orderId);
                    StoreUtility.UpdateOrderPointTranasactionId (orderId, transactionId);
                    StoreUtility.UpdateOrderToComplete (orderId, inviteSentByUserId);
                }
            }

            // Go ahead and update the invite so it cannot be reused to get credit.
            UsersUtility.UpdateInvite (inviteId, (int) Constants.eINVITE_STATUS.WOK_LOGIN);

            return true;
        }

        /// <summary>
        /// GetUserCommentCount
        /// </summary>
        public static double GetTotalTraffic (int userId)
        {
            string sqlSelect = "SELECT COALESCE(SUM(a.file_size),0) " +
                " FROM order_items oi, orders o, assets a " +
                " WHERE royalty_user_id_paid = @userId" +
                " AND oi.order_id = o.order_id " +
                " AND a.asset_id = oi.asset_id " +
                " AND o.purchase_type = " + (int) Constants.ePURCHASE_TYPE.ASSET +
                " AND o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().ExecuteScalarDouble(sqlSelect, parameters);
        }

        /// <summary>
        /// InsertAssetRequest - inserts a record that the user has requested a specific
        /// asset. Used in metric reporting.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static int InsertAssetRequest (int assetId, int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "INSERT INTO metric_assets ( " +
                " user_id, asset_id, download_date" +
                " ) VALUES (" +
                " @userId, @assetId, " + dbUtility.GetCurrentDateFunction () + ")";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@userId", userId);
            dbUtility.ExecuteNonQuery (sqlString, parameters);

            return 0;
        }

        /// <summary>
        /// GetAssetRequests - number of distinct assets requested by given user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int GetAssetRequests (int assetId, int userId)
        {
            string sqlSelect = "SELECT count(DISTINCT asset_id)" +
                " FROM metric_assets " +
                " WHERE user_id = @userId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().ExecuteScalar(sqlSelect, parameters);
        }

        /// <summary>
        /// HasRightsToViewProfile
        /// </summary>
        /// <param name="showValue"></param>
        /// <param name="profileId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static bool HasRightsToViewProfile (string showValue, int profileId, int userId)
        {
            UserFacade userFacade = new UserFacade();

            switch (showValue)
            {
                // Everyone
                case "Y":
                    {
                        return true;
                    }
                // Kaneva members
                case "K":
                    {
                        return (userId > 0);
                    }
                // All Friends
                case "A":
                    {
                        return (userFacade.AreFriends (profileId, userId));
                    }
                // Only Me
                case "M":
                    {
                        return (profileId.Equals (userId));
                    }
                default:
                    {
                        try
                        {
                            // Is this a friend group?
                            if (KanevaGlobals.IsNumeric (showValue))
                            {
                                return IsFriendInGroup (Convert.ToInt32 (showValue), userId);
                            }
                        }
                        catch (Exception) { }

                        return false;
                    }
            }
        }

        /// <summary>
        /// UpdateChannelTag
        /// </summary>
        /// <returns></returns>
        public static int UpdateChannelTag (int userId, int channelId, string tag)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            // tag here is a comma seperated list
            tag = tag.ToLower ().Trim ();
            tag = tag.Replace ("  ", " ");

            // A tag here is a set of keywords,
            // make sure there are not duplicates to skew the results.
            Hashtable keywords = new Hashtable ();
            StoreUtility.AddToHash (keywords, tag);

            string newTags = "";
            IDictionaryEnumerator en = keywords.GetEnumerator ();

            while (en.MoveNext ())
            {
                newTags = en.Value + " " + newTags;
            }

            // Update the tags stored in community table
            CommunityUtility.UpdateChannelTags (channelId, newTags.Trim ());

            return 0;
        }

        /// <summary>
        /// GetAddresses
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataTable GetAddresses (int userId)
        {
            string sqlSelect = "SELECT name, address_id, user_id, phone_number, " +
                " address1, address2, city, a.state_code, s.state as state_name, zip_code, a.country_id, c.country as country_name " +
                " FROM countries c, address a LEFT OUTER JOIN states s ON a.state_code = s.state_code " +
                " WHERE user_id = @userId " +
                " AND a.country_id = c.country_id " +
                " ORDER BY address_id";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// GetAddress
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataRow GetAddress (int userId, int addressId)
        {
            string sqlSelect = "SELECT name, address_id, user_id, phone_number, " +
                " address1, address2, city, a.state_code, s.state as state_name, zip_code, a.country_id, c.country as country_name " +
                " FROM countries c, address a LEFT OUTER JOIN states s ON a.state_code = s.state_code " +
                " WHERE user_id = @userId " +
                " AND address_id = @addressId " +
                " AND a.country_id = c.country_id " +
                " ORDER BY address_id";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@addressId", addressId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
        }

        public static int GetLastAddressId(int userId)
        {
            string sqlSelect = "SELECT address_id " +
                " FROM address " +
                " WHERE user_id = @userId " +
                " ORDER BY address_id DESC LIMIT 1;";

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().ExecuteScalar(sqlSelect, parameters);
        }

        /// <summary>
        /// InsertAddress
        /// </summary>
        public static int InsertAddress (int userId, string name, string address1, string address2,
            string city, string state, string zipCode, string phoneNumber, string countryId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "INSERT INTO address " +
                " (user_id, name, address1, address2, city, " +
                " state_code, zip_code, country_id, phone_number) " +
                " VALUES (@userId, @name, @address1, @address2, @city, " +
                " @state, @zipCode, @countryId, @phoneNumber)";

            int addressId = 0;
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@name", name);
            parameters.Add ("@address1", address1);
            parameters.Add ("@address2", address2);
            parameters.Add ("@city", city);
            parameters.Add ("@state", state);
            parameters.Add ("@zipCode", zipCode);
            parameters.Add ("@countryId", countryId);
            parameters.Add ("@phoneNumber", phoneNumber);
            dbUtility.ExecuteIdentityInsert (sqlSelect, parameters, ref addressId);
            return addressId;
        }

        /// <summary>
        /// UpdateAddress
        /// </summary>
        public static int UpdateAddress (int userId, int addressId, string name, string address1, string address2,
            string city, string state, string zipCode, string phoneNumber, string countryId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlUpdate = "UPDATE address " +
                " SET name = @name, " +
                " address1 = @address1, " +
                " address2 = @address2, " +
                " city = @city, " +
                " state_code = @state, " +
                " zip_code = @zipCode, " +
                " country_id = @countryId, " +
                " phone_number = @phoneNumber " +
                " WHERE user_id = @userId " +
                " AND address_id = @addressId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@addressId", addressId);
            parameters.Add ("@name", name);
            parameters.Add ("@address1", address1);
            parameters.Add ("@address2", address2);
            parameters.Add ("@city", city);
            parameters.Add ("@state", state);
            parameters.Add ("@zipCode", zipCode);
            parameters.Add ("@countryId", countryId);
            parameters.Add ("@phoneNumber", phoneNumber);
            return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// DeleteAddress
        /// </summary>
        public static int DeleteAddress (int addressId, int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "DELETE FROM address " +
                " WHERE user_id = @userId" +
                " AND address_id = @addressId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@addressId", addressId);
            parameters.Add ("@userId", userId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// GetCreditCards
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataTable GetCreditCards (int userId, Constants.eBILLING_INFO_TYPE informationType)
        {
            string sqlSelect = "SELECT billing_information_id, user_id, nickname, information_type, " +
                " name_on_card, card_number, card_type, exp_month, exp_day, exp_year, address_id " +
                " FROM billing_information " +
                " WHERE user_id = @userId " +
                " AND information_type = @informationType " +
                " ORDER BY billing_information_id";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@informationType", (int) informationType);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// GetCreditCard
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataRow GetCreditCard (int userId, int billingInformationId)
        {
            string sqlSelect = "SELECT billing_information_id, user_id, nickname, information_type, " +
                " name_on_card, card_number, card_type, exp_month, exp_day, exp_year, address_id " +
                " FROM billing_information " +
                " WHERE user_id = @userId " +
                " AND billing_information_id = @billingInformationId " +
                " ORDER BY billing_information_id";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@billingInformationId", billingInformationId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
        }

        /// <summary>
        /// InsertBillingInfo
        /// </summary>
        public static int InsertBillingInfo (int userId, Constants.eBILLING_INFO_TYPE informationType, string nickname, string nameOnCard,
            string cardNumber, string cardType, string expMonth, string expDay, string expYear, int addressId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "INSERT INTO billing_information " +
                " (user_id, nickname, information_type, name_on_card, " +
                " card_number, card_type, exp_month, exp_day, exp_year, address_id) " +
                " VALUES (@userId, @nickname, @informationType, @nameOnCard, " +
                "@cardNumber, @cardType, @expMonth, @expDay, @expYear, @addressId)";

            int billingInfoId = 0;
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@nickname", nickname);
            parameters.Add ("@informationType", (int) informationType);
            parameters.Add ("@nameOnCard", nameOnCard);
            parameters.Add ("@cardNumber", cardNumber);
            parameters.Add ("@cardType", cardType);
            parameters.Add ("@expMonth", expMonth);
            parameters.Add ("@expDay", expDay);
            parameters.Add ("@expYear", expYear);
            parameters.Add ("@addressId", addressId);
            dbUtility.ExecuteIdentityInsert (sqlSelect, parameters, ref billingInfoId);
            return billingInfoId;
        }

        /// <summary>
        /// UpdateBillingInfo
        /// </summary>
        public static int UpdateBillingInfo (int userId, int billingInfoId, string expMonth, string expYear)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlUpdate = "UPDATE billing_information " +
                " SET exp_month = @expMonth, " +
                " exp_year = @expYear " +
                " WHERE user_id = @userId " +
                " AND billing_information_id = @billingInfoId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@billingInfoId", billingInfoId);
            parameters.Add ("@expMonth", expMonth);
            parameters.Add ("@expYear", expYear);
            return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// UpdateBillingInfo
        /// </summary>
        public static int UpdateBillingInfo (int userId, int billingInfoId, string nameOnCard, string cardType, string expMonth, string expYear)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlUpdate = "UPDATE billing_information " +
                " SET exp_month = @expMonth, " +
                " exp_year = @expYear, " +
                " card_type = @cardType, " +
                " name_on_card = @nameOnCard " +
                " WHERE user_id = @userId " +
                " AND billing_information_id = @billingInfoId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@billingInfoId", billingInfoId);
            parameters.Add ("@expMonth", expMonth);
            parameters.Add ("@expYear", expYear);
            parameters.Add ("@cardType", cardType);
            parameters.Add ("@nameOnCard", nameOnCard);
            return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// UpdateBillingInfo
        /// </summary>
        public static int ClearBillingSubscriptionId(int userId, string billingSubscriptionId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlUpdate = "UPDATE users_billing_subscriptions " +
                " SET billing_subscription_status = 2, " +
                " last_modified_date = Now() " +
                " WHERE user_id = @userId " +
                " AND billing_subscription_id = @billingSubscriptionId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);
            parameters.Add("@billingSubscriptionId", billingSubscriptionId);
            return dbUtility.ExecuteNonQuery(sqlUpdate, parameters);
        }

        /// <summary>
        /// DeleteBillingInfo
        /// </summary>
        public static int DeleteBillingInfo (int billingInfoId, int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "DELETE FROM billing_information " +
                " WHERE user_id = @userId" +
                " AND billing_information_id = @billingInfoId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@billingInfoId", billingInfoId);
            parameters.Add ("@userId", userId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        public static int InsertUserHardware (int userId, string operatingSystem, string systemManufacturer, string systemModel, string bios,
            string processor, string memory, string directXVersion, string cardName, string manufacturer, string chipType, string dacType,
            string displayMemory, string currentMode)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "INSERT INTO wok.dx_diag " +
                " (user_id, diag_date, " +
                " operating_system, make, model, bios, " +
                " proc, memory, directx_version, card_name, " +
                " card_make, card_chip_type, dac_type, card_memory, display_mode " +
                ") " +
                " VALUES (@userId, " + dbUtility.GetCurrentDateFunction () + "," +
                " @operatingSystem, @systemManufacturer, @systemModel, @bios, " +
                " @processor, @memory, @directXVersion, @cardName, " +
                " @manufacturer, @chipType, @dacType, @displayMemory, @currentMode " +
                ")";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@operatingSystem", operatingSystem);
            parameters.Add ("@systemManufacturer", systemManufacturer);
            parameters.Add ("@systemModel", systemModel);
            parameters.Add ("@bios", bios);
            parameters.Add ("@processor", processor);
            parameters.Add ("@memory", memory);
            parameters.Add ("@directXVersion", directXVersion);
            parameters.Add ("@cardName", cardName);
            parameters.Add ("@manufacturer", manufacturer);
            parameters.Add ("@chipType", chipType);
            parameters.Add ("@dacType", dacType);
            parameters.Add ("@displayMemory", displayMemory);
            parameters.Add ("@currentMode", currentMode);
            return dbUtility.ExecuteNonQuery (sqlSelect, parameters);
        }

        public static int InsertUserHardware (int userId, string operatingSystem, string systemManufacturer, string systemModel, string bios,
           string processor, string memory, string directXVersion, string cardName, string manufacturer, string chipType, string dacType,
           string displayMemory, string currentMode, UInt64 downloadedSize, UInt64 elapsedTime, bool downloadCompleted)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "INSERT INTO wok.dx_diag " +
                " (user_id, diag_date, " +
                " operating_system, make, model, bios, " +
                " proc, memory, directx_version, card_name, " +
                " card_make, card_chip_type, dac_type, card_memory, display_mode,  " +
                " downloaded_size, elapsed_time, download_completed " +
                ") " +
                " VALUES (@userId, " + dbUtility.GetCurrentDateFunction () + "," +
                " @operatingSystem, @systemManufacturer, @systemModel, @bios, " +
                " @processor, @memory, @directXVersion, @cardName, " +
                " @manufacturer, @chipType, @dacType, @displayMemory, @currentMode, @downloadedSize, @elapsedTime, @downloadCompleted " +
                ")";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@operatingSystem", operatingSystem);
            parameters.Add ("@systemManufacturer", systemManufacturer);
            parameters.Add ("@systemModel", systemModel);
            parameters.Add ("@bios", bios);
            parameters.Add ("@processor", processor);
            parameters.Add ("@memory", memory);
            parameters.Add ("@directXVersion", directXVersion);
            parameters.Add ("@cardName", cardName);
            parameters.Add ("@manufacturer", manufacturer);
            parameters.Add ("@chipType", chipType);
            parameters.Add ("@dacType", dacType);
            parameters.Add ("@displayMemory", displayMemory);
            parameters.Add ("@currentMode", currentMode);
            parameters.Add ("@downloadedSize", downloadedSize);
            parameters.Add ("@elapsedTime", elapsedTime);
            parameters.Add ("@downloadCompleted", downloadCompleted ? 'Y' : 'N');
            return dbUtility.ExecuteNonQuery (sqlSelect, parameters);
        }

        public static int InsertUserHardware (int userId, string operatingSystem, string systemManufacturer, string systemModel, string bios,
           string processor, string memory, string directXVersion, string cardName, string manufacturer, string chipType, string dacType,
           string displayMemory, string currentMode, UInt64 downloadedSize, UInt64 elapsedTime, bool downloadCompleted, string driverVersion, string driverDateSize)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "INSERT INTO wok.dx_diag " +
                " (user_id, diag_date, " +
                " operating_system, make, model, bios, " +
                " proc, memory, directx_version, card_name, " +
                " card_make, card_chip_type, dac_type, card_memory, display_mode,  " +
                " downloaded_size, elapsed_time, download_completed, driver_version, driver_date_size " +
                ") " +
                " VALUES (@userId, " + dbUtility.GetCurrentDateFunction () + "," +
                " @operatingSystem, @systemManufacturer, @systemModel, @bios, " +
                " @processor, @memory, @directXVersion, @cardName, " +
                " @manufacturer, @chipType, @dacType, @displayMemory, @currentMode, @downloadedSize, @elapsedTime, @downloadCompleted, @driverVersion, @driverDateSize " +
                ")";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@operatingSystem", operatingSystem);
            parameters.Add ("@systemManufacturer", systemManufacturer);
            parameters.Add ("@systemModel", systemModel);
            parameters.Add ("@bios", bios);
            parameters.Add ("@processor", processor);
            parameters.Add ("@memory", memory);
            parameters.Add ("@directXVersion", directXVersion);
            parameters.Add ("@cardName", cardName);
            parameters.Add ("@manufacturer", manufacturer);
            parameters.Add ("@chipType", chipType);
            parameters.Add ("@dacType", dacType);
            parameters.Add ("@displayMemory", displayMemory);
            parameters.Add ("@currentMode", currentMode);
            parameters.Add ("@downloadedSize", downloadedSize);
            parameters.Add ("@elapsedTime", elapsedTime);
            parameters.Add ("@downloadCompleted", downloadCompleted ? 'Y' : 'N');
            parameters.Add ("@driverVersion", driverVersion);
            parameters.Add ("@driverDateSize", driverDateSize);
            return dbUtility.ExecuteNonQuery (sqlSelect, parameters);
        }

        #region Interests Functions

        /// <summary>
        /// Get the user interests info
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataTable GetUserInterests (int userId)
        {
            return GetUserInterests (userId, "", "");
        }

        /// <summary>
        /// Get the user interests info
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataTable GetUserInterests (int userId, string filter, string orderBy)
        {
            string sqlSelect = "SELECT ui.interest_id, i.interest, i.user_count, ui.ic_id, ic.category_text, ui.z_order " +
                " FROM user_interests ui " +
                " INNER JOIN interests i ON i.interest_id = ui.interest_id " +
                " INNER JOIN interest_categories ic ON ic.ic_id = ui.ic_id " +
                " WHERE ui.user_id = @userId " +
                " AND ic.ic_id NOT IN (21) ";

            if (filter.Length > 0)
            {
                sqlSelect += " AND " + filter;
            }

            if (orderBy.Length > 0)
            {
                sqlSelect += " ORDER BY " + orderBy + ";";
            }
            else
            {
                sqlSelect += " ORDER BY category_text, interest; ";
            }

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// Get the user interests info
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataTable GetUserInterestsByCategory (int userId, int icId)
        {
            string sqlSelect = "SELECT ui.interest_id, i.interest, i.user_count, ui.ic_id, ic.category_text, ui.z_order " +
                " FROM user_interests ui " +
                " INNER JOIN interests i ON i.interest_id = ui.interest_id " +
                " INNER JOIN interest_categories ic ON ic.ic_id = ui.ic_id " +
                " WHERE ui.user_id = @userId " +
                " AND ic.ic_id = @icId " +
                " ORDER BY category_text, interest; ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@icId", icId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// Get the user interests info
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataTable GetInterests (int userId)
        {
            string sqlSelect = "SELECT ic.ic_id, ic.category_text, X.interest, X.interest_id, X.user_count, X.z_order " +
                " FROM interest_categories ic " +
                " LEFT JOIN " +
                " (SELECT ui.ic_id, i.interest, i.interest_id, i.user_count, ui.z_order FROM interests i " +
                " INNER JOIN user_interests ui on ui.interest_id = i.interest_id AND ui.user_id = @userId " +
                " ) X on X.ic_id = ic.ic_id " +
                " WHERE ic.ic_id NOT IN (21) " +
                " ORDER BY category_text, X.interest; ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// Get the user interests info
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataTable GetInterestByCategory (int userId, int icId)
        {
            string sqlSelect = "SELECT ic.ic_id, ic.category_text, X.interest, X.interest_id, X.user_count, X.z_order " +
                " FROM interest_categories ic " +
                " LEFT JOIN " +
                " (SELECT ui.ic_id, i.interest, i.interest_id, i.user_count, ui.z_order FROM interests i " +
                " INNER JOIN user_interests ui on ui.interest_id = i.interest_id AND ui.user_id = @userId " +
                " ) X on X.ic_id = ic.ic_id " +
                " WHERE ic.ic_id = @icId " +
                " ORDER BY X.interest; ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@icId", icId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// Add a new interest
        /// </summary>
        /// <param name="idId"></param>
        /// <param name="interest"></param>
        /// <returns></returns>
        public static int AddInterest (int icId, string interest)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "CALL add_interest(@icId, @interest, @rc); SELECT CAST(@rc as UNSIGNED INT) as rc;";
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@interest", interest);
            parameters.Add ("@icId", icId);

            DataRow dr = dbUtility.GetDataRow (sqlSelect, parameters, false);

            return Convert.ToInt32 (dr["rc"]);
        }

        /// <summary>
        /// Assing an interest to a user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="interestId"></param>
        /// <param name="idId"></param>
        /// <returns></returns>
        public static int AssignInterestToUser (int userId, int interestId, int icId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "CALL assign_interest_to_user(@userId, @interestId, @icId, @rc); Select CAST(@rc as UNSIGNED INT) as rc;";
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@interestId", interestId);
            parameters.Add ("@icId", icId);

            DataRow dr = dbUtility.GetDataRow (sqlSelect, parameters, false);

            int rc = Convert.ToInt32 (dr["rc"]); // 0 = ok, -1 = interest already exists for this user

            if (rc == 0)
            {
                // update the users searchable interests 
                UpdateUsersSearchableInterests (userId);

                // increment interest count
                string sqlAddProc = "CALL add_to_interest_count(@interestId)";
                Hashtable add_parameters = new Hashtable ();
                add_parameters.Add ("@interestId", interestId);

                DataRow drPlus = dbUtility.GetDataRow (sqlAddProc, add_parameters, false);
            }

            return rc;
        }

        /// <summary>
        /// Remove an interest from a user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="interestId"></param>
        /// <returns></returns>
        public static int RemoveInterestFromUser (int userId, int interestId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "CALL remove_interest_from_user (@userId, @interestId);";
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@interestId", interestId);
            int rc = dbUtility.ExecuteNonQuery (sqlString, parameters);

            if (rc == 1)
            {
                // update the users searchable interests   
                UpdateUsersSearchableInterests (userId);

                // decrement interest count
                string sqlSubtractProc = "CALL remove_from_interest_count(@interestId)";
                Hashtable subtract_parameters = new Hashtable ();
                subtract_parameters.Add ("@interestId", interestId);

                DataRow drSubtract = dbUtility.GetDataRow (sqlSubtractProc, subtract_parameters, false);
            }

            return rc;
        }

        /// <summary>
        /// Remove an interest from all users and remove from interest table
        /// </summary>
        /// <param name="interestId"></param>
        /// <returns></returns>
        public static int RemoveInterestFromAllUsers (int interestId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "CALL remove_interest_from_all_users (@interestId);";
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@interestId", interestId);

            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Checks to see if an interest exists
        /// </summary>
        /// <param name="idId"></param>
        /// <param name="interest"></param>
        /// <returns>Interest id if exists, 0 if not exists</returns>
        public static int InterestExists (int icId, string interest)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlSelect = "SELECT interest_exists(@icId, @interest);";
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@icId", icId);
            parameters.Add ("@interest", interest);

            DataRow drInterest = dbUtility.GetDataRow (sqlSelect, parameters, false);

            return Convert.ToInt32 (drInterest[0]); // 0 = does not exist, > 0 is interest id 
        }

        /// <summary>
        /// Gets common interests among 2 users
        /// </summary>
        /// <param name="idId"></param>
        /// <param name="interest"></param>
        /// <returns>Interest id if exists, 0 if not exists</returns>
        public static DataTable GetCommonInterests (int userId, int userId2)
        {
            string sqlSelect = "SELECT ui.interest_id, i.interest, ic.category_text " +
                " FROM kaneva.user_interests ui " +
                " INNER JOIN interests i ON i.interest_id = ui.interest_id " +
                " INNER JOIN interest_categories ic ON ic.ic_id = ui.ic_id " +
                " WHERE user_id = @userId " +
                " AND (ui.interest_id IN (SELECT interest_id FROM kaneva.user_interests WHERE user_id = @userId2)) " +
                " AND ic.ic_id NOT IN (21) " +
                " ORDER BY category_text, interest;";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@userId2", userId2);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// Gets common interests among 2 users
        /// </summary>
        /// <param name="idId"></param>
        /// <param name="interest"></param>
        /// <returns>Interest id if exists, 0 if not exists</returns>
        public static DataTable GetCommonInterestsByCategory (int userId, int userId2, int icId)
        {
            string sqlSelect = "SELECT ui.interest_id, i.interest, ic.category_text " +
                " FROM kaneva.user_interests ui " +
                " INNER JOIN interests i ON i.interest_id = ui.interest_id " +
                " INNER JOIN interest_categories ic ON ic.ic_id = ui.ic_id " +
                " WHERE user_id = @userId " +
                " AND (ui.interest_id IN (SELECT interest_id FROM kaneva.user_interests WHERE user_id = @userId2)) " +
                " AND ic.ic_id = @icId " +
                " ORDER BY category_text, interest;";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@userId2", userId2);
            parameters.Add ("@icId", icId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }

        public static int UpdateUsersSearchableInterests (int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "CALL update_my_searchable_interests(@userId);";
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        #endregion

        #region School Functions

        /// <summary>
        /// Get list of schools for a user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataTable GetUserSchools (int userId)
        {
            return GetUserSchools (userId, "", "");
        }
        /// <summary>
        /// Get list of schools for a user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataTable GetUserSchools (int userId, string filter, string orderBy)
        {
            string sqlSelect = "SELECT s.school_id, s.name, s.city, s.state " +
                " FROM schools s " +
                " INNER JOIN user_schools us ON us.school_id = s.school_id " +
                " WHERE us.user_id = @userId ";

            if (filter.Length > 0)
            {
                sqlSelect += " AND " + filter;
            }

            if (orderBy.Length > 0)
            {
                sqlSelect += " ORDER BY " + orderBy + ";";
            }
            else
            {
                sqlSelect += " ORDER BY s.name; ";
            }

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// Add a new school
        /// </summary>
        /// <param name="name"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public static int AddSchool (string name, string city, string state)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "CALL add_school(@name, @city, @state, @rc); SELECT CAST(@rc as UNSIGNED INT) as rc;";
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@name", name);
            parameters.Add ("@city", city);
            parameters.Add ("@state", state);

            DataRow dr = dbUtility.GetDataRow (sqlSelect, parameters, false);

            return Convert.ToInt32 (dr["rc"]);
        }

        /// <summary>
        /// Associate a school to a user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public static int AssociateSchoolToUser (int userId, int schoolId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "CALL associate_school(@userId, @schoolId, @rc); Select CAST(@rc as UNSIGNED INT) as rc;";
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@schoolId", schoolId);

            DataRow dr = dbUtility.GetDataRow (sqlSelect, parameters, false);

            int rc = Convert.ToInt32 (dr["rc"]); // 0 = ok, -1 = interest already exists for this user

            if (rc == 0)
            {
                // update the users searchable interests 
                //               UpdateUsersSearchableSchools (userId);
                UpdateUsersSearchableInterests (userId);

                // increment school count
                string sqlAddSelect = "CALL add_to_schoolscount(@schoolId)";
                Hashtable add_parameters = new Hashtable ();
                add_parameters.Add ("@schoolId", schoolId);

                DataRow drPlus = dbUtility.GetDataRow (sqlAddSelect, add_parameters, false);
            }

            return rc;
        }

        /// <summary>
        /// Remove aa school from a user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public static int RemoveSchoolFromUser (int userId, int schoolId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "CALL remove_associated_school (@userId, @schoolId);";
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@schoolId", schoolId);
            int rc = dbUtility.ExecuteNonQuery (sqlString, parameters);

            if (rc == 1)
            {
                // update the users searchable interests   
                //          UpdateUsersSearchableInterests (userId);

                // decrement school count
                string sqlSubtractProc = "CALL remove_from_schoolscount(@schoolId)";
                Hashtable subtract_parameters = new Hashtable ();
                subtract_parameters.Add ("@schoolId", schoolId);

                DataRow drSubtract = dbUtility.GetDataRow (sqlSubtractProc, subtract_parameters, false);
            }

            return rc;
        }

        /// <summary>
        /// Remove an school from all users and delete school
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public static int _RemoveSchoolFromAllUsers (int schoolId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            //   string sqlString = "CALL remove_interest_from_user (@userId);";
            //   Hashtable parameters = new Hashtable ();
            //   parameters.Add ("@userId", userId);
            return 1;// dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Check to see if a school already exists
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static int SchoolExists (string name)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlSelect = "SELECT school_exists(@name);";
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@name", name);

            DataRow drSchool = dbUtility.GetDataRow (sqlSelect, parameters, false);

            return Convert.ToInt32 (drSchool[0]); // 0 = does not exist, > 0 is interest id 
        }

        /// <summary>
        /// Get common schools between two users
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userId2"></param>
        /// <returns></returns>
        public static DataTable GetCommonSchools (int userId, int userId2)
        {
            string sqlSelect = "SELECT us.school_id, s.name " +
                " FROM user_schools us " +
                " INNER JOIN user_schools us2 ON us.school_id = us2.school_id " +
                " INNER JOIN schools s ON us.school_id = s.school_id " +
                " WHERE us.user_id=@userId AND us2.user_id=@userId2 " +
                " ORDER BY s.name;";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@userId2", userId2);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }

        #endregion

        // **********************************************************************************************
        // Friends
        // **********************************************************************************************
        #region Friend Functions

        /// <summary>
        /// Get the pending friends for a user
        /// </summary>
        public static PagedDataTable GetIncomingPendingFriends(int userId, string filter, string orderby,
            bool bGetPending, int pageNumber, int pageSize)
        {
            string selectList = "com.thumbnail_small_path, com.name_no_spaces, u.username, u.age, u.user_id, u.gender, " +
                " u.birth_date, u.zip_code, u.location, u.display_name, " +
                " com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, " +
                " fr.request_date, NOW() as CurrentTime, com.thumbnail_square_path, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture ";

            string tableList = "users u " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id, " +
                " friend_requests fr, communities_personal com ";

            string whereClause = " fr.friend_id = @userId AND u.user_id = fr.user_id " +
                " AND u.user_id = com.creator_id ";

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// Get the friends a user requested but not confirmed
        /// </summary>
        public static PagedDataTable GetOutgoingPendingFriends(int userId, string filter, string orderby,
            bool bGetPending, int pageNumber, int pageSize)
        {
            string selectList = "u.username, u.user_id, u.gender, u.age, u.location, " +
                " u.birth_date, u.zip_code, " +
                " com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, " +
                " fr.request_date, NOW() as CurrentDate ";

            string tableList = "users u, friend_requests fr, communities_personal com";

            string whereClause = " fr.user_id = @userId AND u.user_id = fr.friend_id " +
                " AND u.user_id = com.creator_id ";

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }


        /// <summary>
        /// Get the friends for a user considering game data (virtual world) as well
        /// </summary>
        public static PagedDataTable GetFriends(int userId, string orderby, int pageNumber, int pageSize, string friendName, bool onlineOnly, bool sortbyLastLogon, bool sortbyraves, bool mustHaveImage)
        {   //**BLOCK -- remove after test
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;
            string dbNameForDeveloper = KanevaGlobals.DbNameDeveloper;
            Hashtable parameters = new Hashtable();

            //** REWORK THIS METHOD 


            string select = "SELECT COALESCE(gu.logged_on, 'F') as online, " +
                " IFNULL(imu.online, 0) as im_online, IFNULL(imu.presence, '') as im_presence, " +
                " COALESCE(al.game_id,0) as game_id, COALESCE(gu.logged_on, 'F') as online_in_wok, ku.ustate AS onweb, ku.username, ku.user_id, ku.mature_profile, COALESCE(wp.player_id, 0) as player_id, kf.glued_date, " +
                " kc.name_no_spaces, COALESCE(al.created_date,COALESCE(gu.last_logon,0),0) as last_logon, COALESCE(TIMESTAMPDIFF(HOUR, al.created_date, NOW()),COALESCE(TIMESTAMPDIFF(HOUR, gu.last_logon, NOW()), 0)) as hours_since_last_logon, " +
                " kcs.number_of_diggs, kc.thumbnail_medium_path, " +
  /* ?? */      " IF(bu.blocked_user_id IS NOT NULL, 1, 0) AS comm_blocked, " +
  /* ?? */      " IF(bu.blocked_user_id IS NOT NULL, 1, 0) AS follow_blocked, " +
                " bu.created_date AS blocked_date, " +
                " ku.age, ku.gender, ku.display_name, ku.ustate, ku.location, " +
                " kc.thumbnail_small_path, kc.thumbnail_large_path, kcs.number_of_views, ku.status_id, gu.is_gm, COALESCE(cz.name, '') AS zone_display_name " +
                " FROM kaneva.users ku " +
                " INNER JOIN kaneva.communities_personal kc ON ku.user_id = kc.creator_id " +
                " INNER JOIN kaneva.channel_stats kcs ON kc.community_id = kcs.channel_id " +
                " INNER JOIN kaneva.friends kf ON kf.user_id = @userId AND ku.user_id = kf.friend_id " +
                " LEFT OUTER JOIN developer.active_logins al ON ku.user_id = al.user_id " +
                " LEFT OUTER JOIN kaneva.im_users imu ON ku.user_id = imu.user_id " +
                " LEFT OUTER JOIN wok.game_users gu ON gu.kaneva_user_id = kf.friend_id " +
                " LEFT OUTER JOIN wok.players wp ON gu.user_id = wp.user_id " +
                " LEFT OUTER JOIN kaneva.blocked_users bu ON kf.friend_id = bu.blocked_user_id AND bu.user_id = @userId " +
                " LEFT OUTER JOIN wok.player_zones pz ON wp.player_id = pz.player_id " +
                " LEFT OUTER JOIN wok.channel_zones cz ON pz.current_zone_index = cz.zone_index AND pz.current_zone_instance_id = cz.zone_instance_id " +
                " WHERE ku.active = 'Y' ";

            if (onlineOnly)
            {
                select += " AND (gu.logged_on='T' OR imu.online=1)";
            }

            if (mustHaveImage)
            {
                select += " AND (kc.thumbnail_path IS NOT NULL AND kc.thumbnail_path <> '')";
            }

            string theFriendName = null;

            if (friendName != null)
            {
                theFriendName = friendName + "%";
                select += " AND ku.username like @friendName ";
                parameters.Add("@friendName", theFriendName);
            }

            if (sortbyLastLogon)
            {
                select += " ORDER BY last_logon desc";
            }
            else if (sortbyraves)
            {
                select += " ORDER BY number_of_diggs desc";
            }
            else
            {
                select += " ORDER BY username";
            }

            parameters.Add("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTableUnion(select, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// IsFriendInGroup
        /// </summary>
        /// <param name="friendGroupId"></param>
        /// <param name="friendId"></param>
        /// <returns></returns>
        public static bool IsFriendInGroup (int friendGroupId, int friendId)
        {
            string sqlSelect = "SELECT friend_group_id " +
                " FROM friend_group_friends " +
                " WHERE friend_group_id = @friendGroupId" +
                " AND friend_id = @friendId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@friendGroupId", friendGroupId);
            parameters.Add ("@friendId", friendId);
            DataRow drFriendGroup = KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);

            return (drFriendGroup != null);
        }

        /// <summary>
        /// Insert a friend group
        /// </summary>
        /// <returns></returns>
        public static int InsertFriendGroup(int userId, string groupName)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sql = "INSERT INTO friend_groups " +
                "(owner_id, name, created_datetime " +
                ") VALUES (" +
                "@userId, @groupName, " + dbUtility.GetCurrentDateFunction() + ")";

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);
            parameters.Add("@groupName", groupName);
            return dbUtility.ExecuteNonQuery(sql, parameters);
        }

        ///// <summary>
        ///// Insert a friend group
        ///// </summary>
        ///// <returns></returns>
        //public static int UpdateFriendGroup (int userId, int friendGroupId, string groupName)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

        //    string sql = "UPDATE friend_groups " +
        //        " SET name = @groupName " +
        //        " WHERE owner_id = @userId " +
        //        " AND friend_group_id = @friendGroupId ";

        //    Hashtable parameters = new Hashtable ();
        //    parameters.Add ("@groupName", groupName);
        //    parameters.Add ("@userId", userId);
        //    parameters.Add ("@friendGroupId", friendGroupId);
        //    return dbUtility.ExecuteNonQuery (sql, parameters);
        //}

        /// <summary>
        /// Delete a friend group
        /// </summary>
        /// <returns></returns>
        public static int DeleteFriendGroup(int userId, int friendGroupId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            // Delete all friends from group
            DeleteAllFriendsFromGroup(friendGroupId);

            // Delete the friend group
            string sql = "DELETE FROM friend_groups " +
                " WHERE owner_id = @userId " +
                " AND friend_group_id = @friendGroupId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);
            parameters.Add("@friendGroupId", friendGroupId);
            return dbUtility.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// DeleteAllFriendsFromGroup
        /// </summary>
        /// <returns></returns>
        public static int DeleteAllFriendsFromGroup(int friendGroupId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            // Delete any friends in the group
            string sql = "DELETE FROM friend_group_friends " +
                " WHERE friend_group_id = @friendGroupId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@friendGroupId", friendGroupId);
            return dbUtility.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Insert a friend into a group
        /// </summary>
        /// <returns></returns>
        public static int InsertFriendInGroup(int ownerId, int friendGroupId, int friendId)
        {
            // Make sure they are not already in the group
            if (IsFriendInGroup(friendGroupId, friendId))
            {
                return 0;
            }

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sql = "INSERT INTO friend_group_friends " +
                "(friend_group_id, friend_id, owner_id" +
                ") VALUES (" +
                "@friendGroupId, @friendId, @ownerId)";

            Hashtable parameters = new Hashtable();
            parameters.Add("@friendId", friendId);
            parameters.Add("@friendGroupId", friendGroupId);
            parameters.Add("@ownerId", ownerId);
            return dbUtility.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Remove a friend from a group
        /// </summary>
        /// <returns></returns>
        public static int RemoveFriendFromGroup(int ownerId, int friendGroupId, int friendId)
        {
            // Make sure they are not already in the group
            if (!IsFriendInGroup(friendGroupId, friendId))
            {
                return 0;
            }

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sql = "DELETE FROM friend_group_friends " +
                " WHERE friend_id = @friendId " +
                " AND friend_group_id = @friendGroupId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@friendId", friendId);
            parameters.Add("@friendGroupId", friendGroupId);
            return dbUtility.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Remove a friend from all groups
        /// </summary>
        /// <returns></returns>
        public static int RemoveFriendFromAllGroups(int ownerId, int friendId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sql = "DELETE FROM friend_group_friends " +
                " WHERE friend_id = @friendId " +
                " AND owner_id = @ownerId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@friendId", friendId);
            parameters.Add("@ownerId", ownerId);
            return dbUtility.ExecuteNonQuery(sql, parameters);
        }


        /// <summary>
        /// Get the friend groups for a user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static PagedDataTable GetFriendGroups(int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            string selectList = " friend_group_id, name, friend_count ";

            string tableList = " friend_groups fg ";

            string whereClause = " owner_id = @userId ";		// Friends invited by my I confirmed

            // Filter it?
            if ((filter != null) && (filter.Length > 0))
            {
                whereClause += " AND " + filter;
            }

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        #endregion

        #region Block Functions
        /// <summary>
        /// Block a user
        /// </summary>
        /// <returns></returns>
        public static int BlockUser (int userId, int blockedUserId)
        {   //**BLOCK -- remove after test
            // Make sure they are not already blocked
            UserFacade userFacade = new UserFacade ();

            if (userFacade.IsUserBlocked(userId, blockedUserId))
            {
                return 0;
            }

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();
            
            string sql = "INSERT INTO blocked_users " +
                    "(user_id, blocked_user_id, created_date" +
                    ") VALUES (" +
                    "@userId, @blockedUserId, NOW())";

                parameters.Add ("@userId", userId);
                parameters.Add ("@blockedUserId", blockedUserId);

            return dbUtility.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// UnBlock a user
        /// </summary>
        /// <returns></returns>
        public static int UnBlockUser (int userId, int blockedUserId)
        {
            //**BLOCK -- remove after test
            string sql = "DELETE FROM blocked_users " +
                    "WHERE user_id = @userId " +
                    " AND blocked_user_id = @blockedUserId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@blockedUserId", blockedUserId);

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            return dbUtility.ExecuteNonQuery (sql, parameters);
        }

     
        /// <summary>
        /// Lists blocked users whose communication or follow travel has been blocked by the given userId
        /// Also, list any users that are blocked from the current userId's zone
        /// </summary>
        /// <param name="userId">blocking owner id</param>
        /// <returns></returns>
        public static PagedDataTable ListBlockedUsers (int userId, int pageNumber, int pageSize)
        {   //**BLOCK -- remove after test...
            string strQuery = "";

            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;

            int zoneAccess = 0;
            bool isGM = false;
            int zone_instance_id = 0;

            GetUserAccessRightsToCurrentZone (userId, ref zoneAccess, ref isGM, ref zone_instance_id);

            // GetUserAccessRightsToCurrentZone gets user if their in a broadband
            // zone only

            if ((zone_instance_id != 0) &&
                 (zoneAccess != (int) CommunityMember.CommunityMemberAccountType.OWNER) &&
                 (zoneAccess != (int) CommunityMember.CommunityMemberAccountType.MODERATOR) &&
                 (!isGM))
            {
                PagedDataTable pdtResults = new PagedDataTable ();
                return pdtResults;
            }

            strQuery = "SELECT bu.user_id AS block_owner, blocked_user_id AS profile_id, " +
                " created_date AS blocked_date, u.username AS username, " +
                " 1 AS comm_blocked, 1 AS follow_blocked, '' AS bu.reason_description, 1 AS zone_blocked " +
                " FROM " + dbNameForKaneva + ".blocked_users bu, " + dbNameForKaneva + ".users u " +
                " WHERE bu.user_id = @userId " +
                " AND profile_id=u.user_id;";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            string orderby = "username";

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTableUnion(strQuery, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// Determines the access rights to the user's current broadband zone. zoneAccess will match
        /// CommunityMember.CommunityMemberAccountType.OWNER) || CommunityMember.CommunityMemberAccountType.MODERATOR) to
        /// determine administrative access to the zone. Additionally, isGM will return true if the player
        /// is a GM of the game.
        /// </summary>
        /// <param name="userId">userId to check/param>
        /// <param name="isGM">returns true if userId is a GM</param>
        /// <param name="zone_instance_id">returns userId's current zone instance</param>
        public static void GetUserAccessRightsToCurrentZone (int userId, ref int zoneAccess, ref bool isGM, ref int zone_instance_id)
        {
            isGM = false;
            zoneAccess = 0;

            zone_instance_id = 0;

            string strQuery = "";

            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;


            strQuery = "SELECT pz.current_zone_type, pz.current_zone_instance_id ";
            strQuery += "FROM " + dbNameForGame + ".player_zones pz, " + dbNameForGame + ".players p ";

            strQuery += " WHERE p.kaneva_user_id = @userId AND p.player_id=pz.player_id AND pz.current_zone_type = " + (int) Constants.BROADBAND_ZONE;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            DataRow dr = KanevaGlobals.GetDatabaseUtility ().GetDataRow (strQuery, parameters, false);

            if (dr != null)
            {
                // Get the broadband id the userid currently resides in.
                zone_instance_id = (int)dr["current_zone_instance_id"];

                GetUserAccessRightsToSpecificZone(userId, ref zoneAccess, ref isGM, zone_instance_id);
            }
            else
            {
                IsUserGM(userId, ref isGM);
            }
        }

        /// <summary>
        /// Determines the access rights to passed in zone. zoneAccess will match
        /// CommunityMember.CommunityMemberAccountType.OWNER) || CommunityMember.CommunityMemberAccountType.MODERATOR) to
        /// determine administrative access to the zone. Additionally, isGM will return true if the player
        /// is a GM of the game.
        /// </summary>
        /// <param name="userId">userId to check/param>
        /// <param name="isGM">returns true if userId is a GM</param>
        /// <param name="zone_instance_id">specific zone instance to check</param>
        public static void GetUserAccessRightsToSpecificZone (int userId, ref int zoneAccess, ref bool isGM, int zone_instance_id)
        {
            isGM = false;
            zoneAccess = 0;

            string strQuery = "";

            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;


            strQuery = "SELECT account_type_id ";
            strQuery += "FROM " + dbNameForKaneva + ".community_members cm ";
            strQuery += "WHERE  cm.user_id = @userId AND cm.community_id = @zoneId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@zoneId", zone_instance_id);

            DataRow dr = KanevaGlobals.GetDatabaseUtility ().GetDataRow (strQuery, parameters, false);

            if (dr != null)
            {
                zoneAccess = (int) dr["account_type_id"];
            }

            IsUserGM(userId, ref isGM);
        }

        public static void IsUserGM(int userId, ref bool isGM)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;

            string strQuery = "SELECT is_gm ";
            strQuery += "FROM " + dbNameForGame + ".game_users gu ";
            strQuery += "WHERE  gu.kaneva_user_id = @userId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);

            DataRow dr2 = KanevaGlobals.GetDatabaseUtility().GetDataRow(strQuery, parameters, false);

            isGM = false;
            if (dr2 != null)
            {
                string isGMstr = (string)dr2["is_gm"];

                if (isGMstr.Equals("T"))
                {
                    isGM = true;
                }
            }
        }

        /// <summary>
        /// Lists blocked users whose communication or follow travel has been blocked by the given userId
        /// Also, list any users that are blocked from the current userId's zone where userId is an owner/moderator of that zone
        /// </summary>
        /// <param name="userId">blocking owner id</param>
        /// <param name="blockedUserId">the user being blocked</param>
        /// <returns></returns>
        public static DataRow GetBlockedUser (int userId, int blockedUserId, ref bool comm_blocked, ref bool follow_blocked, ref int zoneAccess, ref bool isGM, ref bool blockedFromCurrentLocation, ref int userCurrentZone, ref string reasonDescription)
        {   //**BLOCK -- remove after test...
            comm_blocked = false;
            follow_blocked = false;
            zoneAccess = 0;
            isGM = false;
            blockedFromCurrentLocation = false;
            userCurrentZone = 0;

            string strQuery = "";

            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;

            /* Old Block Query
//            strQuery = "SELECT bu.user_id AS block_owner, bu.blocked_user_id AS profile_id, bu.created_date AS blocked_date, u.username AS username, " +
                "bu.is_comm_blocked AS comm_blocked, " +
                "bu.is_follow_blocked AS follow_blocked";
            strQuery += " FROM " + dbNameForKaneva + ".blocked_users bu, " + dbNameForKaneva + ".users u ";
//            strQuery += " WHERE bu.user_id = @userId AND bu.blocked_user_id = @blockUserId AND u.user_id = @blockUserId ";
            */

            strQuery = "SELECT bu.user_id AS block_owner, bu.blocked_user_id AS profile_id, bu.created_date AS blocked_date, u.username AS username, " +
                " 1 AS comm_blocked, 1 AS follow_blocked" +
                " FROM " + dbNameForKaneva + ".blocked_users bu, " + dbNameForKaneva + ".users u " +
                " WHERE bu.user_id = @userId AND bu.blocked_user_id = @blockUserId AND u.user_id = @blockUserId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@blockUserId", blockedUserId);

            DataRow dr = KanevaGlobals.GetDatabaseUtilityReadOnly2 ().GetDataRow (strQuery, parameters, false);

            if (dr != null)
            {
                comm_blocked = true;
                follow_blocked = true;
            }

            // Determine if the user is the owner of this zone
            GetUserAccessRightsToCurrentZone (userId, ref zoneAccess, ref isGM, ref userCurrentZone);

            // If the blocker is the owner of the current community then blockee is not allowed in current zone
            if ((zoneAccess == (int) CommunityMember.CommunityMemberAccountType.OWNER))
            {
                blockedFromCurrentLocation = true;
            }

            return dr;
        }

        /// <summary>
        /// Blocks user from communication, following user, or from a specific zone.
        /// </summary>
        /// <param name="userId">blocking owner id</param>
        /// <param name="profileId">the user being blocked</param>
        /// <param name="block_comm">block communications</param>
        /// <param name="block_follow">block following userId</param>
        /// <param name="block_zone">block userId's current zone</param>
        /// <param name="block_reason_str">reason userId is blocking the profileId</param>
        /// <returns>positive value if ok</returns>
        /// <returns>-1 - userId does not control zone</returns>
        /// <returns>-2 - profileId has owner/moderator/GM access to zone</returns>
        public static int BlockUser (int userId, int blockedUserId, int block_comm, int block_follow, int block_zone, string block_reason_str)
        {   //**BLOCK -- remove after test
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;

            int ret = 1;

            bool comm_blocked = false;                  //*****************
            bool follow_blocked = false;                //** REWORK THIS METHOD      
            int userAccess = 0;                               
            bool isGM = false;
            bool blockedFromCurrentLocation = false;
            string reasonDescription = null;
            int userCurrentZone = 0;
 //           int zoneOwnerId = 0;

//            DataRow dr = GetBlockedUser (userId, blockedUserId, ref comm_blocked, ref follow_blocked, ref userAccess, ref isGM, ref blockedFromCurrentLocation, ref userCurrentZone, ref reasonDescription, ref zoneOwnerId);
            DataRow dr = GetBlockedUser (userId, blockedUserId, ref comm_blocked, ref follow_blocked, ref userAccess, ref isGM, ref blockedFromCurrentLocation, ref userCurrentZone, ref reasonDescription);

            // Determine zone blocking error conditions first
 /* zone does not matter -> per user basis now
            if ((block_zone == 1) && (!blockedFromCurrentLocation) &&
                 (userAccess != (int) CommunityMember.CommunityMemberAccountType.OWNER) &&
                 (userAccess != (int) CommunityMember.CommunityMemberAccountType.MODERATOR) &&
                 (!isGM))
            {
                // User has requested to block from current zone they do not control. Error.

                return -1;
            }
            else if ((block_zone == 0) && (blockedFromCurrentLocation) &&
                      (userAccess != (int) CommunityMember.CommunityMemberAccountType.OWNER) &&
                      (userAccess != (int) CommunityMember.CommunityMemberAccountType.MODERATOR) &&
                      (!isGM))
            {
                // User has requested to unblock from current zone they do not control. Error.
                return -1;
            }
 */

             int blockeeZoneAccess = 0;

            // Determine if the user being blocked is an owner/moderator/GM and disallow blocking.
             GetUserAccessRightsToSpecificZone (blockedUserId, ref blockeeZoneAccess, ref isGM, userCurrentZone);

 /* not valid - can block anyone 
            if (((block_zone == 1) && (!blockedFromCurrentLocation)) ||
                 ((block_zone == 0) && (blockedFromCurrentLocation)))
            {
                if ((blockeeZoneAccess == (int) CommunityMember.CommunityMemberAccountType.OWNER) ||
                     (blockeeZoneAccess == (int) CommunityMember.CommunityMemberAccountType.MODERATOR) ||
                     (isGM))
                {
                    // blockee is owner/moderator/GM of user's zone
                    return -2;
                }
            }

            if (((block_comm == 1) || (block_follow == 1)) && isGM)
            {
                // blockee is GM
                return -2;
            }
*/

            Hashtable parameters = new Hashtable ();
            string sql = null;

            if (dr != null)
            {
                // Don't need this section, user already in blocked list
                /*
                if ((block_comm == 1) || (block_follow == 1))
                {
                    // Update the row

                    sql = "UPDATE " + dbNameForKaneva + ".blocked_users " +
                        "SET is_comm_blocked = @commBlocked, is_follow_blocked = @followBlocked " +
                        "WHERE zone_type = " + (int) Constants.APARTMENT_ZONE + " AND user_id = @userId AND profile_id = @profileId";

                    parameters.Add ("@commBlocked", block_comm);
                    parameters.Add ("@followBlocked", block_follow);
                    parameters.Add ("@userId", userId);
                    parameters.Add ("@profileId", profileId);

                    ret = KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sql, parameters);
                }
                else
                {
                    // Delete the row

                    sql = "DELETE FROM " + dbNameForKaneva + ".blocked_users " +
                        "WHERE zone_type = " + (int) Constants.APARTMENT_ZONE + " AND user_id = @userId AND profile_id = @profileId";

                    parameters.Add ("@userId", userId);
                    parameters.Add ("@profileId", profileId);

                    ret = KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sql, parameters);
                }
                */

                if ((block_comm != 1) && (block_follow != 1))
                {
                    sql = "DELETE FROM " + dbNameForKaneva + ".blocked_users " +
                           "WHERE user_id = @userId AND blocked_user_id = @blockedUserId";

                    parameters.Add ("@userId", userId);
                    parameters.Add ("@blockedUserId", blockedUserId);

                    ret = KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sql, parameters);
                }
            }
            else if ((block_comm == 1) || (block_follow == 1))
            {
                // Insert
                sql = "INSERT INTO " + dbNameForKaneva + ".blocked_users " +
                    "(user_id, blocked_user_id, created_date, block_source)" +
                    " VALUES " +
                    "(@userId, @blockedUserId, NOW(), @blockSource)";

                parameters.Add ("@userId", userId);
                parameters.Add ("@blockedUserId", blockedUserId);
                parameters.Add ("@blockSource", (int)Constants.eUSER_BLOCK_SOURCE.WOK);          

                ret = KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sql, parameters);
            }

            parameters.Clear ();      //** working here **








            if (ret != 0)
            {
                if ((block_zone == 1) && (!blockedFromCurrentLocation))
                {
                    // Insert
                    /*
                    sql = "INSERT INTO " + dbNameForKaneva + ".blocked_users " +
                          "(user_id, profile_id, created_date, zone_type, zone_instance_id, is_zone_blocked, reason_description)" +
                          " VALUES " +
                          "(@userId, @profileId, NOW(), " + (int) Constants.BROADBAND_ZONE + ", @zoneId, @zoneBlocked, @reasonDescription)";

                    parameters.Add ("@userId", userId);
                    parameters.Add ("@blockedUserId", blockedUserId);
                    parameters.Add ("@zoneId", userCurrentZone);
                    parameters.Add ("@zoneBlocked", 1);
                    parameters.Add ("@reasonDescription", block_reason_str);
                    */

                    sql = "INSERT INTO " + dbNameForKaneva + ".blocked_users " +
                        "(user_id, blocked_user_id, created_date, block_source)" +
                        " VALUES " +
                        "(@userId, @blockedUserId, NOW(), @blockSource)";

                    parameters.Add ("@userId", userId);
                    parameters.Add ("@blockedUserId", blockedUserId);
                    parameters.Add ("@blockSource", (int) Constants.eUSER_BLOCK_SOURCE.WOK);          

                    ret = KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sql, parameters);
                }
                    /*
                else if ((block_zone == 1) && (blockedFromCurrentLocation) && (reasonDescription != block_reason_str))
                {
                    // Changed the reason description

                    sql = "UPDATE " + dbNameForKaneva + ".blocked_users " +
                          "SET reason_description = @reasonDescription " +
                          "WHERE profile_id = @profileId AND zone_type = " + (int) Constants.BROADBAND_ZONE + " AND zone_instance_id = @zoneId";

                    parameters.Add ("@reasonDescription", block_reason_str);
                    parameters.Add ("@blockedUserId", blockedUserId);
                    parameters.Add ("@zoneId", userCurrentZone);

                    ret = KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sql, parameters);
                }
                    * */
                else if ((block_zone == 0) && (blockedFromCurrentLocation))
                {
                    // Delete
                    /*
                    sql = "DELETE FROM " + dbNameForKaneva + ".blocked_users " +
                          "WHERE zone_type = " + (int) Constants.BROADBAND_ZONE + " AND profile_id = @profileId AND zone_instance_id = @zoneId";

                    parameters.Add ("@blockedUserId", blockedUserId);
                    parameters.Add ("@zoneId", userCurrentZone);
                    */

                    sql = "DELETE FROM " + dbNameForKaneva + ".blocked_users " +
                        "WHERE user_id = @userId AND blocked_user_id = @blockedUserId";

                    parameters.Add ("@userId", userId);
                    parameters.Add ("@blockedUserId", blockedUserId);

                    ret = KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sql, parameters);
                }
                else
                {
                    // Leave it alone
                    ;
                }
            }

            return ret;
        }

        #endregion

        #region User Access Functions WOK

        /// <summary>
        /// AddUserToPassGroup
        /// </summary>
        public static int AddUserToPassGroup (int userId, int wokPassGroupId)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            string sqlSelect = " INSERT IGNORE INTO " + dbNameForGame + ".pass_group_users " +
                "        (kaneva_user_id, pass_group_id, created_date ) " +
                " VALUES (@userId, @wokPassGroupId, NOW() ); ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@wokPassGroupId", wokPassGroupId);
            return KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlSelect, parameters);
        }

        /// <summary>
        /// AddUserToPassGroup
        /// </summary>
        public static int DeleteUserFromPassGroup(int userId, int wokPassGroupId)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            string sqlSelect = " DELETE FROM " + dbNameForGame + ".pass_group_users " +
                " WHERE kaneva_user_id = @userId AND pass_group_id = @wokPassGroupId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);
            parameters.Add("@wokPassGroupId", wokPassGroupId);
            return KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sqlSelect, parameters);
        }

        /// <summary>
        /// GetPassGroup
        /// </summary>
        public static DataRow GetPassGroup (int userId, int wokPassGroupId)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            string sqlSelect = " SELECT kaneva_user_id, pass_group_id, created_date " +
                " FROM " + dbNameForGame + ".pass_group_users " +
                " WHERE kaneva_user_id = @userId " +
                " AND pass_group_id = @wokPassGroupId; ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@wokPassGroupId", wokPassGroupId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /**
        *   Get any pass group ids for a given set of glids
        */
        public static DataTable GetPassGroupsForGlids(string[] glids)
    {
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            if (glids.Length == 0)
            {
                return null;
            }

            string sql = "SELECT pgi.global_id, pgi.pass_group_id, pg.name" +
                " FROM " + dbNameForGame + ".pass_group_items pgi " +
                " INNER JOIN " + dbNameForGame + ".pass_groups pg ON pgi.pass_group_id=pg.pass_group_id " +
                " WHERE pgi.global_id IN (";

            bool ftt = true;

            int ii = 0;
            foreach (string glid in glids)
            {
                if (!ftt)
                {
                    sql += ",";
                }
                ftt = false;
                sql += "@" + ii + "_" + glid;
                ii++;
            }

            sql += ")";

            Hashtable parameters = new Hashtable ();

            ii = 0;
            foreach (string glid in glids)
            {
                parameters.Add ("@" + ii + "_" + glid, glid);
                ii++;
            }

            return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sql, parameters);
    }

        #endregion

        #region Rave Functions WOK
         
        /// <summary>
        /// UpdatePlaceVisit
        /// </summary>
        public static int UpdatePlaceVisit (int userId, int zoneInstanceId, int zoneType)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            // Has this person ever visited before?
            string sql = "SELECT zone_instance_id " +
                " FROM " + dbNameForGame + ".zone_unique_visits " +
                " WHERE kaneva_user_id = @userId " +
                " AND zone_instance_id = @zoneInstanceId " +
                " AND zone_type = @zoneType ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@zoneInstanceId", zoneInstanceId);
            parameters.Add ("@zoneType", zoneType);
            DataRow dr = dbUtility.GetDataRow (sql, parameters, false);

            // If not, insert visit
            if (dr == null)
            {
                // Insert Visit
                sql = "INSERT INTO " + dbNameForGame + ".zone_unique_visits " +
                "(kaneva_user_id, zone_instance_id, zone_type, created_date)" +
                " VALUES " +
                "(@userId, @zoneInstanceId, @zoneType, " + dbUtility.GetCurrentDateFunction () + ")";
                parameters.Clear ();
                parameters.Add ("@userId", userId);
                parameters.Add ("@zoneInstanceId", zoneInstanceId);
                parameters.Add ("@zoneType", zoneType);
                dbUtility.ExecuteNonQuery (sql, parameters);

                // Update number_unique_visits on channel_zones
                sql = "UPDATE " + dbNameForGame + ".channel_zones " +
               " SET number_unique_visits = number_unique_visits + 1 " +
               " WHERE zone_instance_id = @zoneInstanceId " +
               " AND zone_type = @zoneType ";
                parameters.Clear ();
                parameters.Add ("@zoneInstanceId", zoneInstanceId);
                parameters.Add ("@zoneType", zoneType);
                dbUtility.ExecuteNonQuery (sql, parameters);

                /*
                // Get number of unique visits
                sql = "SELECT number_unique_visits " +
                    "FROM " + dbNameForGame + ".channel_zones " +
                   " WHERE zone_instance_id = @zoneInstanceId " +
                   " AND zone_type = @zoneType ";
                parameters.Clear ();
                parameters.Add ("@zoneInstanceId", zoneInstanceId);
                parameters.Add ("@zoneType", zoneType);
                int iNumberUniqueVisits = dbUtility.ExecuteScalar (sql, parameters);
                */
            }
                // See if we need to award fame
                FameFacade fameFacade = new FameFacade ();

            try
            {
                if (zoneType == (int) Constants.BROADBAND_ZONE)
                {
                    CommunityFacade communityFacade = new CommunityFacade ();
                    Community community = communityFacade.GetCommunity (zoneInstanceId);
                    /*
                    if (iNumberUniqueVisits > 0 && iNumberUniqueVisits % 10 == 0)
                    {
                        fameFacade.RedeemPacket (community.CreatorId, (int) PacketId.WORLD_HANGOUT_VISIT_10, (int) FameTypes.World);
                    }

                    if (iNumberUniqueVisits > 0 && iNumberUniqueVisits % 50 == 0)
                    {
                        fameFacade.RedeemPacket (community.CreatorId, (int) PacketId.WORLD_HANGOUT_VISIT_50, (int) FameTypes.World);
                    }
                    */
                    if (userId != community.CreatorId)
                    {
                        fameFacade.RedeemPacket(community.CreatorId, (int)PacketId.EVERY_10_VISITORS_COMMUNITY, (int)FameTypes.World);
                        fameFacade.RedeemPacket(community.CreatorId, (int)PacketId.EVERY_35_VISITORS_COMMUNITY, (int)FameTypes.World);
                    }
                }
                else if (zoneType == (int) Constants.APARTMENT_ZONE)
                {
                    DataRow drUser = GetUserFromPlayerId (zoneInstanceId);
                    int userIdOfHome = Convert.ToInt32 (drUser["kaneva_user_id"]);
                    /*
                    if (iNumberUniqueVisits > 0 && iNumberUniqueVisits % 10 == 0)
                    {
                        fameFacade.RedeemPacket (userIdOfHome, (int) PacketId.WORLD_HOME_VISIT_10, (int) FameTypes.World);
                    }

                    if (iNumberUniqueVisits > 0 && iNumberUniqueVisits % 50 == 0)
                    {
                        fameFacade.RedeemPacket (userIdOfHome, (int) PacketId.WORLD_HOME_VISIT_50, (int) FameTypes.World);
                    }
                    */
                    if (userId != userIdOfHome) //you should not earn credit for visiting your own home
                    {
                        fameFacade.RedeemPacket(userIdOfHome, (int)PacketId.FIRST_VISITOR_HOME, (int)FameTypes.World);
                        fameFacade.RedeemPacket(userIdOfHome, (int)PacketId.FIRST_10_VISITORS_HOME, (int)FameTypes.World);
                        fameFacade.RedeemPacket(userIdOfHome, (int)PacketId.FIRST_20_VISITORS_HOME, (int)FameTypes.World);
                        fameFacade.RedeemPacket(userIdOfHome, (int)PacketId.FIRST_50_VISITORS_HOME, (int)FameTypes.World);
                        fameFacade.RedeemPacket(userIdOfHome, (int)PacketId.FIRST_100_VISITORS_HOME, (int)FameTypes.World);
                        fameFacade.RedeemPacket(userIdOfHome, (int)PacketId.FIRST_250_VISITORS_HOME, (int)FameTypes.World);
                        fameFacade.RedeemPacket(userIdOfHome, (int)PacketId.FIRST_500_VISITORS_HOME, (int)FameTypes.World);
                        fameFacade.RedeemPacket(userIdOfHome, (int)PacketId.FIRST_1000_VISITORS_HOME, (int)FameTypes.World);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error awarding place visit fame - user=" + userId + ",zII=" + zoneInstanceId + ",zt=" + zoneType, exc);
            }

            return 0;
        }

        /// <summary>
        /// UpdateDanceFloorVisit
        /// </summary>
        public static int UpdateDanceFloorVisit (int userId, int objectId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            // Has this person ever visited before?
            string sql = "SELECT dynamic_object_id " +
                " FROM " + dbNameForGame + ".ddr_unique_visits " +
                " WHERE kaneva_user_id = @userId " +
                " AND dynamic_object_id = @objectId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@objectId", objectId);
            DataRow dr = dbUtility.GetDataRow (sql, parameters, false);

            // If not, insert visit
            if (dr == null)
            {
                // Insert Visit
                sql = "INSERT INTO " + dbNameForGame + ".ddr_unique_visits " +
                "(kaneva_user_id, dynamic_object_id, created_date)" +
                " VALUES " +
                "(@userId, @objectId, " + dbUtility.GetCurrentDateFunction () + ")";
                parameters.Clear ();
                parameters.Add ("@userId", userId);
                parameters.Add ("@objectId", objectId);
                dbUtility.ExecuteNonQuery (sql, parameters);

                // Get number of unique visits
                sql = "SELECT count(*) cnt " +
                    "FROM (select distinct kaneva_user_id FROM " + dbNameForGame + ".ddr_unique_visits " +
                   " WHERE dynamic_object_id = @objectId) users";
                parameters.Clear ();
                parameters.Add ("@objectId", objectId);

                int iNumberUniqueVisits = dbUtility.ExecuteScalar (sql, parameters);

                sql = "SELECT p.kaneva_user_id " +
                    " FROM " + dbNameForGame + ".dynamic_objects do, " + dbNameForGame + ".players p " +
                    " WHERE do.player_id = p.player_id AND do.obj_placement_id = @objectId ";
                parameters.Clear ();
                parameters.Add ("@objectId", objectId);
                DataRow dynamicObjectDR = dbUtility.GetDataRow (sql, parameters, false);

                if (dynamicObjectDR != null)
                {
                    int userIdOfDanceFloor = Convert.ToInt32 (dynamicObjectDR["kaneva_user_id"]);

                    // See if we need to award fame
                    FameFacade fameFacade = new FameFacade ();

                    try
                    {
                        if (iNumberUniqueVisits > 0 && iNumberUniqueVisits % 10 == 0)
                        {
                            fameFacade.RedeemPacket (userIdOfDanceFloor, (int) PacketId.DANCE_UNIQUE_VISIT_10, (int) FameTypes.World);
                        }

                        if (iNumberUniqueVisits > 0 && iNumberUniqueVisits % 50 == 0)
                        {
                            fameFacade.RedeemPacket (userIdOfDanceFloor, (int) PacketId.DANCE_UNIQUE_VISIT_50, (int) FameTypes.World);
                        }
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error ("Error awarding fame", exc);
                    }
                }
            }

            return 0;
        }

        public static int GetItemRaveCount(int globalId)
        {
            RaveFacade raveFacade = new RaveFacade();
            return raveFacade.GetUGCRaveCount(globalId);
        }

        public static int GetItemRaveCountByUser(int userId, int globalId)
        {
            RaveFacade raveFacade = new RaveFacade();
            return raveFacade.GetUGCRaveCountByUser(userId, globalId);
        }

        public static int UpdateItemRave(int userId, int globalId, RaveType.eRAVE_TYPE raveType, ref int numRaves)
        {
            RaveFacade raveFacade = new RaveFacade();
            
            // Get the number of raves for this item 
            numRaves = raveFacade.GetUGCRaveCount(globalId);

            // Check if this user has already raved this item
            int numRavesByUser = GetItemRaveCountByUser(userId, globalId);
            
            // If the user has not, then apply the rave
            if (numRavesByUser == 0)
            {
                // raveFacade call returns 0 if successful
                int retValue = raveFacade.RaveUGCItem(userId, globalId, RaveType.eRAVE_TYPE.SINGLE, "");

                if (retValue == 0)
                {
                    // Successful insert of the rave, update the number of rave reference value
                    numRaves += 1;
                    return 0;
                }
                else
                {
                    // DB insert failed
                    return -2;
                }
            }
            else
            {
                // User has already raved this item
                return -1;
            }
        }

        #endregion

        // **********************************************************************************************
        // CSR Functions
        // **********************************************************************************************
        #region CSR Functions

        /// <summary>
        /// GetEditorDownloads
        /// </summary>
        public static PagedDataTable GetEditorDownloads (DateTime dtStartDate, DateTime dtEndDate, string orderby, int pageNumber, int pageSize)
        {
            string selectList = " u.username, u.user_id, u.first_name, u.last_name, u.email, " +
                " ed.downloaded_datetime, ed.version ";

            string tableList = " users u, user_editor_downloads ed ";

            string whereClause = " u.user_id = ed.user_id " +
                " AND ed.downloaded_datetime >= @startDate" +
                " AND ed.downloaded_datetime <= @endDate"; ;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@startDate", dtStartDate);
            parameters.Add ("@endDate", dtEndDate);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// return a list of new users to the website
        /// </summary>
        /// <param name="numToShow"></param>
        /// <param name="withPictureOnly">true to only return people with a picture</param>
        /// <returns></returns>
        public static DataTable GetNewPeople (int numToShow, bool withPictureOnly)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlString = "SELECT u.user_id, u.username, c.community_id, c.name_no_spaces, c.thumbnail_small_path " +
                " FROM users u " +
                " INNER JOIN communities_personal c ON c.creator_id = u.user_id " +
                " WHERE @with_pic_only = 0 OR c.has_thumbnail='Y' " +
                " ORDER BY _signup_date " +
                " LIMIT @num_to_show ";

            parameters.Add ("@with_pic_only", withPictureOnly ? 1 : 0);
            parameters.Add ("@num_to_show", numToShow);

            return dbUtility.GetDataTable (sqlString, parameters);
        }

        /// <summary>
        /// return a list of users dx diag entries
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="orderby"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static PagedDataTable GetUserDXDiag (int userId, string orderby, int pageNumber, int pageSize)
        {
            string selectList = " diag_id, diag_date, user_id, operating_system, make, model, bios, proc, memory, " +
                " directx_version, card_name, card_make, card_chip_type, dac_type, card_memory, display_mode, " +
                " downloaded_size, elapsed_time, download_completed, driver_version, driver_date_size ";

            string tableList = KanevaGlobals.DbNameKGP + ".`dx_diag` ";

            string whereClause = " user_id = @userId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// return a list of reward events for a specified transaction type
        /// </summary>
        /// <param name="transactionTypeId"></param>
        /// <returns></returns>
        public static DataTable GetKanevaEventsByTransactionType (int transactionTypeId)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlString = "SELECT reward_event_id, event_name, transaction_type_id " +
                " FROM reward_events " +
                " WHERE transaction_type_id = @transactionTypeId " +
                " ORDER BY event_name ";

            parameters.Add ("@transactionTypeId", transactionTypeId);
            return dbUtility.GetDataTable (sqlString, parameters);
        }

        /// <summary>
        /// return a list of reward events
        /// </summary>
        /// <returns></returns>
        public static DataTable GetKanevaEvents ()
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlString = "SELECT reward_event_id, event_name, transaction_type_id, trans_desc " +
                " FROM reward_events re " +
                " INNER JOIN transaction_type tt ON tt.transaction_type = re.transaction_type_id " +
                " ORDER BY event_name ";

            return dbUtility.GetDataTable (sqlString, parameters);
        }

        /// <summary>
        /// inserts a new reward event
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="transactionTypeId"></param>
        /// <returns></returns>
        public static int AddKanevaRewardEvent (string eventName, int transactionTypeId)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = " INSERT INTO reward_events (event_name, transaction_type_id) " +
                               " VALUES (@eventName, @transactionTypeId)";

            parameters.Add ("@eventName", eventName);
            parameters.Add ("@transactionTypeId", transactionTypeId);

            int returnValue = dbUtility.ExecuteNonQuery (sqlString, parameters);
            return returnValue;
        }

        /// <summary>
        /// updates a reward event
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="eventName"></param>
        /// <param name="transactionTypeId"></param>
        /// <returns></returns>
        public static int UpdateKanevaRewardEvent (int eventId, string eventName, int transactionTypeId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlUpdate = "UPDATE reward_events " +
                " SET event_name = @eventName, " +
                " transaction_type_id = @transactionTypeId " +
                " WHERE reward_event_id = @eventId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@eventId", eventId);
            parameters.Add ("@eventName", eventName);
            parameters.Add ("@transactionTypeId", transactionTypeId);
            return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// Gets listo of Transaction Types
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public static DataTable GetTransactionTypes (string filter, string orderBy)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlString = "SELECT transaction_type, trans_desc, trans_text " +
                " FROM transaction_type ";

            if (filter.Length > 0)
            {
                sqlString += " WHERE " + filter;
            }

            if (orderBy.Length > 0)
            {
                sqlString += " ORDER BY " + orderBy;
            }

            return dbUtility.GetDataTable (sqlString, parameters);
        }

        /// <summary>
        /// Call stored proc to reward credits to user account
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="keiPointId"></param>
        /// <param name="rewardEventId"></param>
        /// <param name="transTypeId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static int RewardCredits (double amount, string keiPointId, int rewardEventId, int transTypeId, DateTime startDate, DateTime endDate)
        {
            Hashtable parameters = new Hashtable ();

            string sql = "CALL kaneva.rewardCredits (@amount, @keiPointId, @startDate, @endDate, @rewardEventId, @transTypeId)";
            parameters.Add ("@amount", amount);
            parameters.Add ("@keiPointId", keiPointId);
            parameters.Add ("@startDate", startDate);
            parameters.Add ("@endDate", endDate);
            parameters.Add ("@rewardEventId", rewardEventId);
            parameters.Add ("@transTypeId", transTypeId);

            return KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// Call stored proc to reward credits to multiple users
        /// </summary>
        /// <param name="userIdList"></param>
        /// <param name="userEmailList"></param>
        /// <param name="amount"></param>
        /// <param name="keiPointId"></param>
        /// <param name="rewardEventId"></param>
        /// <param name="transTypeId"></param>
        /// <param name="failedUserIdList"></param>
        /// <returns></returns>
        public static int RewardCreditsToMultipleUsers (string userIdList, string userEmailList, double amount, int transTypeId, string keiPointId, int rewardEventId, ref string failedUserIdList)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sql = "CALL kaneva.apply_transaction_to_multiple_user_balances2 (@userIdList, @userEmailList, " +
                "@amount, @transTypeId, @keiPointId, @rewardEventId, @success_count, @_failed_user_id_list); select CAST(@success_count as UNSIGNED INT) as success_count, @failed_user_id_list;";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userIdList", userIdList);
            parameters.Add ("@userEmailList", userEmailList);                   
            parameters.Add ("@amount", amount);                                 
            parameters.Add ("@transTypeId", transTypeId);
            parameters.Add ("@keiPointId", keiPointId);
            parameters.Add ("@rewardEventId", rewardEventId);

            DataRow dr = dbUtility.GetDataRow (sql, parameters, false);

            if (dr["@failed_user_id_list"] != null)
            {
                failedUserIdList = dr["@failed_user_id_list"].ToString ();
            }

            return Convert.ToInt32 (dr["success_count"]);
        }

        #endregion


        /// <summary>
        /// Return the user image URL
        /// </summary>
        ///
        public static string GetProfileImageURL (string imagePath)
        {
            return GetProfileImageURL (imagePath, "sm", "M");
        }


        public static string GetProfileImageURL (string imagePath, string defaultSize, string gender)
        {
            return GetProfileImageURL (imagePath, defaultSize, gender, false);
        }

        /// <summary>
        /// GetProfileImageURL
        /// </summary>
        /// <param name="imagePath">The relative path to the image on the NAS. Example - 4/user.gif</param>
        /// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la', 'xl'</param>
        /// <param name="gender">The gender of the user, used to display the correct default picture. Example - 'M', 'F'</param>
        /// <returns></returns>
        public static string GetProfileImageURL (string imagePath, string defaultSize, string gender, bool checkForFacebookImg)
        {
            if (imagePath.Length.Equals (0))
            {
                if (gender.ToUpper ().Equals ("M"))
                {
                    return KanevaGlobals.ImageServer + "/KanevaIconMale_" + defaultSize + ".gif";
                }
                else
                {
                    return KanevaGlobals.ImageServer + "/KanevaIconFemale_" + defaultSize + ".gif";
                }
            }
            else
            {
                if (checkForFacebookImg)
                {
                    if (imagePath.Contains ("graph.facebook"))
                    {
                        return imagePath;
                    }
                }
                return KanevaGlobals.ImageServer + "/" + imagePath;
            }
        }

        /// <summary>
        /// returns true if user is the channel owner
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public static bool IsUserChannelOwner (int userId, int channelId)
        {
            string sqlSelect = "SELECT COUNT(*) " +
                " FROM communities " +
                " WHERE creator_id = @creator_id " +
                " AND community_id = @community_id ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@creator_id", userId);
            parameters.Add ("@community_id", channelId);
            int result = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlSelect, parameters);
            return (result > 0);
        }

        /// <summary>
        /// return a list of usernames
        /// </summary>
        public static DataTable CheckDb ()
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "SELECT username " +
                " FROM users " +
                " LIMIT 10 ";

            return dbUtility.GetDataTable (sqlString, parameters);
        }

        /// <summary>
        /// Insert a new WOK download attempt
        /// </summary>
        public static int InsertWOKDownload (int userId)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "INSERT INTO wok_downloads (user_id, created_date) VALUES (@userId, NOW())";

            parameters.Add ("@userId", userId);

            int retVal = 0;
            dbUtility.ExecuteIdentityInsert (sqlString, parameters, ref retVal);
            return retVal;
        }

        #region used for db migration
        /// <summary>
        /// return all channels and their owners
        /// </summary>
        /// <returns></returns>
        public static DataTable GetLevel0ChannelMembers ()
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string query = " SELECT creator_id, community_id FROM communities_public " +
                " WHERE creator_id > 0 ";
            log.Debug ("Executing " + query);

            return dbUtility.GetDataTable (query, parameters);
        }

        /// <summary>
        /// returns all the members that are not invited by members other than the channel owner
        /// </summary>
        /// <returns></returns>
        public static DataTable GetLevel1ChannelMembers ()
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            StringBuilder sb = new StringBuilder ();
            sb.Append (" SELECT DISTINCT com.creator_id, c.community_id, c.user_id ");
            sb.Append (" FROM community_members c ");
            sb.Append (" INNER JOIN communities_public com ON c.community_id = com.community_id ");
            sb.Append (" WHERE c.status_id = ").Append ((UInt32) CommunityMember.CommunityMemberStatus.ACTIVE);
            sb.Append (" AND c.community_id > 0 AND com.creator_id <> c.user_id ");
            sb.Append (" AND NOT EXISTS ");
            sb.Append (" ( SELECT invited_user_id FROM invites i ");
            sb.Append (" WHERE i.invited_user_id = c.user_id AND ");
            sb.Append (" i.channel_id = c.community_id AND i.user_id <> com.creator_id ) ");

            log.Debug ("Executing " + sb.ToString ());
            return dbUtility.GetDataTable (sb.ToString (), parameters);
        }

        /// <summary>
        /// return members invited by <c>level</c>-1 members
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public static DataTable GetChannelMembersByLevel (int level)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            StringBuilder sb = new StringBuilder ();

            sb.Append (" SELECT i.user_id AS parent_id, i.channel_id AS community_id, ");
            sb.Append (" i.invited_user_id AS user_id, uc.path AS parent_path ");
            sb.Append (" FROM invites i ");
            sb.Append (" INNER JOIN community_members cm ON cm.community_id = i.channel_id ");
            sb.Append (" AND cm.user_id = i.invited_user_id ");
            sb.Append (" INNER JOIN user_connection uc ON uc.user_id = i.user_id ");
            sb.Append (" AND uc.channel_id = i.channel_id ");
            sb.Append (" WHERE uc.depth = @depth");

            log.Debug ("Executing " + sb.ToString ());
            parameters.Add ("@depth", level - 1); //parent's level is inserting member's level minus 1
            return dbUtility.GetDataTable (sb.ToString (), parameters);
        }
        #endregion

        #region user connection (forward track)

        /// <summary>
        /// Note that this method does not update stats, path and depth are calculated based on parent
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="userId"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public static int InsertUserConnection (int channelId, int userId,
            int parentId)
        {
            string path = "";
            int depth = 0;
            if (parentId > 0)
            {
                DataRow drParent = GetUserConnectionByUserAndChannel (parentId, channelId);
                if (drParent != null)
                {
                    if (drParent["path"] != DBNull.Value &&
                        drParent["path"].ToString ().Trim ().Length > 0)
                    {
                        path = drParent["path"].ToString () + "," + parentId;
                    }
                    else
                    {
                        path = parentId.ToString ();
                    }
                    depth = Convert.ToInt32 (drParent["depth"]) + 1;
                }
            }
            //SELECT u.id, u.channel_id, u.user_id, u.parent_id, u.path, u.depth,
            //u.created_datetime FROM user_connection u;
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            StringBuilder sb = new StringBuilder ();
            sb.Append (" INSERT INTO user_connection (");
            sb.Append (" channel_id, user_id, parent_id, path, ");
            sb.Append (" depth, created_datetime) VALUES ( ");
            sb.Append (" @channel_id, @user_id, @parent_id, @path, ");
            sb.Append (" @depth, @created_datetime) ");

            parameters.Add ("@channel_id", channelId > 0 ? channelId : 0);
            parameters.Add ("@user_id", userId);
            parameters.Add ("@parent_id", parentId > 0 ? parentId : 0);
            parameters.Add ("@path", path);
            parameters.Add ("@depth", depth);
            parameters.Add ("@created_datetime", dbUtility.GetCurrentDateTime ());

            //log.Debug("Executing " + sb.ToString());
            int retVal = 0;
            dbUtility.ExecuteIdentityInsert (sb.ToString (), parameters, ref retVal);

            return retVal;
        }

        public static DataTable GetAllUserConnections ()
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string query = " SELECT id, channel_id, user_id, parent_id, path, depth FROM user_connection ";

            return dbUtility.GetDataTable (query, parameters);
        }

        public static DataTable GetUserConnectionsByUserId (int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string query = " SELECT id, channel_id, user_id, path FROM user_connection " +
                " WHERE user_id = @userId";
            parameters.Add ("@userId", userId);

            return dbUtility.GetDataTable (query, parameters);
        }

        public static DataRow GetUserConnectionByUserAndChannel (int userId, int channelId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string query = " SELECT c.id, c.channel_id, c.user_id, c.path, c.depth, u.username, u.zip_code " +
                " FROM user_connection c " +
                " INNER JOIN users u ON u.user_id = c.user_id " +
                " WHERE u.user_id = @userId AND channel_id = @channelId";
            parameters.Add ("@userId", userId);
            parameters.Add ("@channelId", channelId);

            return dbUtility.GetDataRow (query, parameters, false);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="parentId">pass in 0 to return root level users</param>
        /// <param name="channelId">pass in 0 to return all channels</param>
        /// <returns></returns>
        public static DataTable GetUserConnectionsByParentId (int parentId, int channelId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string query = " SELECT uc.id, uc.channel_id, uc.user_id, uc.path, u.zip_code " +
                " FROM user_connection uc " +
                " INNER JOIN users u ON u.user_id = uc.user_id";

            if (parentId <= 0)
            {
                query += " WHERE (parent_id IS NULL OR parent_id = 0)";
            }
            else
            {
                query += " WHERE parent_id = @parent_id";
                parameters.Add ("@parent_id", parentId);
            }
            if (channelId > 0)
            {
                query += " AND channel_id = @channel_id";
                parameters.Add ("@channel_id", channelId);
            }

            return dbUtility.GetDataTable (query, parameters);
        }

        /// <summary>
        /// return level 0 user for a channel
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public static DataRow GetRootUserConnection (int channelId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string query = " SELECT id, channel_id, user_id, parent_id, path FROM user_connection "
                + " WHERE channel_id = @channelId AND (parent_id IS NULL OR parent_id = 0)";
            parameters.Add ("@channelId", channelId);

            return dbUtility.GetDataRow (query, parameters, false);
        }

        public static DataRow GetUserConnection (int userConnectionId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string query = " SELECT uc.id, uc.channel_id, uc.user_id, uc.parent_id, uc.path, uc.depth, u.username "
                + " FROM user_connection uc"
                + " INNER JOIN users u ON u.user_id = uc.user_id "
                + " WHERE id = @id";
            parameters.Add ("@id", userConnectionId);

            return dbUtility.GetDataRow (query, parameters, false);
        }

        public static DataRow GetParentUserConnection (int userConnectionId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string query = " SELECT uc.id, uc.channel_id, uc.user_id, uc.parent_id, uc.path " +
                        " FROM user_connection uc " +
                        " INNER JOIN user_connection uc2 ON uc2.parent_id = uc.user_id AND uc2.channel_id = uc.channel_id" +
                        " WHERE uc2.id = @id ";
            parameters.Add ("@id", userConnectionId);

            return dbUtility.GetDataRow (query, parameters, false);
        }

        /// <summary>
        /// populate a user's invited user count at level <c>depth</c>
        /// </summary>
        /// <param name="userConnectionId"></param>
        /// <param name="depth"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static int InsertUserConnectionStats (int userConnectionId, int depth, int count)
        {
            //SELECT u.id, u.user_connection_id, u.depth, u.count,
            //u.created_datetime FROM user_connection_stats u;
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            StringBuilder sb = new StringBuilder ();
            sb.Append (" INSERT INTO user_connection_stats (");
            sb.Append (" user_connection_id, depth, count, created_datetime) ");
            sb.Append (" VALUES ( ");
            sb.Append (" @user_connection_id, @depth, @count, @created_datetime) ");

            parameters.Add ("@user_connection_id", userConnectionId);
            parameters.Add ("@depth", depth);
            parameters.Add ("@count", count);
            parameters.Add ("@created_datetime", dbUtility.GetCurrentDateTime ());

            //log.Debug("Executing " + sb.ToString());
            int retVal = 0;
            dbUtility.ExecuteIdentityInsert (sb.ToString (), parameters, ref retVal);

            return retVal;
        }

        public static int GetUserConnectionStats (int userConnectionId, int depth)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string query = "SELECT id FROM user_connection_stats " +
                " WHERE user_connection_id = @userConnectionId AND depth = @depth";

            parameters.Add ("@userConnectionId", userConnectionId);
            parameters.Add ("@depth", depth);

            DataTable dt = dbUtility.GetDataTable (query, parameters);
            if (dt.Rows.Count > 0)
            {
                return Convert.ToInt32 (dt.Rows[0]["id"]);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// return the number of degrees in a channel
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public static int GetUserConnectionsDegreeByChannelId (int channelId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string query = "SELECT MAX( ucs.depth ) FROM user_connection_stats ucs " +
                " INNER JOIN user_connection uc ON uc.id = ucs.user_connection_id" +
                " WHERE uc.channel_id = @channelId";

            parameters.Add ("@channelId", channelId);

            return dbUtility.ExecuteScalar (query, parameters);
        }

        /// <summary>
        /// returns all user connections stats for a user, ordered by depth
        /// </summary>
        /// <param name="userConnectionId"></param>
        /// <returns></returns>
        public static DataTable GetUserConnectionStatsByUserConnectionId (int userConnectionId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string query = "SELECT DISTINCT ucs.id, ucs.depth, ucs.count FROM user_connection_stats ucs " +
                " WHERE ucs.user_connection_id = @user_connection_id ORDER BY depth";

            parameters.Add ("@user_connection_id", userConnectionId);

            return dbUtility.GetDataTable (query, parameters);
        }

        /// <summary>
        /// returns all user connections stats for a channel, ordered by depth
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public static DataTable GetUserConnectionStatsByChannelId (int channelId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string query = "SELECT DISTINCT ucs.id, ucs.depth, ucs.count FROM user_connection_stats ucs " +
                " INNER JOIN user_connection uc ON uc.id = ucs.user_connection_id" +
                " WHERE uc.channel_id = @channelId ORDER BY depth";

            parameters.Add ("@channelId", channelId);

            return dbUtility.GetDataTable (query, parameters);
        }

        /// <summary>
        /// return the number of members at each degree
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public static DataTable GetUserConnectionsCountByDegree (int channelId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            StringBuilder sb = new StringBuilder ();
            sb.Append (" SELECT COUNT(user_id) AS count, depth ");
            sb.Append (" FROM user_connection ");
            sb.Append (" WHERE channel_id = @channelId ");
            sb.Append (" GROUP BY depth ");
            sb.Append (" HAVING depth > 0 ");
            sb.Append (" ORDER BY depth ");

            parameters.Add ("@channelId", channelId);

            return dbUtility.GetDataTable (sb.ToString (), parameters);
        }

        public static DataTable GetAllUserConnectionStats (int userConnectionId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string query = "SELECT id, depth, count FROM user_connection_stats " +
                " WHERE user_connection_id = @userConnectionId" +
                " ORDER BY depth ";

            parameters.Add ("@userConnectionId", userConnectionId);

            return dbUtility.GetDataTable (query, parameters);
        }

        /// <summary>
        /// warning! calling this will clear all counts to 0 in user_connection_stats table
        /// It's meant to be called from the daily update service only to recalculate all the counts
        /// </summary>
        /// <returns></returns>
        public static void ClearAllUserConnectionStats ()
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string query = "UPDATE user_connection_stats SET count = 0";

            dbUtility.ExecuteNonQuery (query);
        }

        /// <summary>
        /// increment children nodes count of a user at certain level
        /// </summary>
        /// <param name="userConnectionId"></param>
        /// <param name="depth"></param>
        /// <param name="val"></param>
        public static void IncrementUserConnectionStatsCount (int userConnectionId, int depth, int val)
        {
            int count = val;
            if (GetUserConnectionStats (userConnectionId, depth) > 0)
            {
                //update
                DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
                Hashtable parameters = new Hashtable ();

                StringBuilder sb = new StringBuilder ();
                sb.Append (" SELECT count FROM user_connection_stats ");
                sb.Append (" WHERE user_connection_id = @userConnectionId AND depth = @depth ");

                parameters.Add ("@userConnectionId", userConnectionId);
                parameters.Add ("@depth", depth);

                //log.Debug("Executing " + sb.ToString());
                int currCount = dbUtility.ExecuteScalar (sb.ToString (), parameters);
                count += currCount;
            }

            UpdateUserConnectionStatsCount (userConnectionId, depth, count, true);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userConnectionId"></param>
        /// <param name="depth"></param>
        /// <param name="count"></param>
        /// /// <param name="createNew">true to create a new row if there isn't an existing one</param>
        public static void UpdateUserConnectionStatsCount (int userConnectionId, int depth, int count, bool createNew)
        {
            if (GetUserConnectionStats (userConnectionId, depth) > 0)
            {
                //update
                DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
                Hashtable parameters = new Hashtable ();

                StringBuilder sb = new StringBuilder ();
                sb.Append (" UPDATE user_connection_stats ");
                sb.Append (" SET count = @count ");
                sb.Append (" WHERE user_connection_id = @userConnectionId AND depth = @depth ");

                parameters.Add ("@userConnectionId", userConnectionId);
                parameters.Add ("@depth", depth);
                parameters.Add ("@count", count);

                //log.Debug("Executing " + sb.ToString());
                dbUtility.ExecuteNonQuery (sb.ToString (), parameters);
            }
            else if (createNew)
            {
                //insert
                InsertUserConnectionStats (userConnectionId, depth, count);
            }
            else
            {
                //return if no row is created or updated
                return;
            }

            if (depth != 0)
            {
                //increase the total count too
                IncrementUserConnectionStatsCount (userConnectionId, 0, count);
                //increase the total count for parent at the level
                IncrementParentUserConnectionStatsCount (userConnectionId, depth, count);
            }
        }

        /// <summary>
        /// increment children nodes count of a parent user at certain level
        /// </summary>
        /// <param name="userConnectionId"></param>
        /// <param name="depth"></param>
        /// <param name="val"></param>
        public static void IncrementParentUserConnectionStatsCount (int userConnectionId, int depth, int val)
        {
            DataRow drUserConn = GetParentUserConnection (userConnectionId);
            if (drUserConn != null)
            {
                int id = Convert.ToInt32 (drUserConn["id"]);
                if (id > 0)
                {
                    IncrementUserConnectionStatsCount (id, depth, val);
                }
            }
        }

        public static DataRow GetUserConnectionMapDataByUserConnectionId (int userConnectionId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string query = "SELECT id, user_connection_id, map_data, created_datetime " +
                " FROM user_connection_map_data " +
                " WHERE user_connection_id = @userConnectionId ";

            parameters.Add ("@userConnectionId", userConnectionId);

            return dbUtility.GetDataRow (query, parameters, false);
        }

        public static void UpdateUserConnectionMapData (int id, int userConnectionId, string mapData)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            StringBuilder sb = new StringBuilder ();
            sb.Append (" UPDATE user_connection_map_data ");
            sb.Append (" SET user_connection_id = @user_connection_id, ");
            sb.Append (" map_data = @map_data ");
            sb.Append (" WHERE id = @id ");

            parameters.Add ("@id", id);
            parameters.Add ("@map_data", mapData);
            parameters.Add ("@user_connection_id", userConnectionId);

            dbUtility.ExecuteNonQuery (sb.ToString (), parameters);
        }

        /// <summary>
        /// populate a user's map data
        /// </summary>
        /// <param name="userConnectionId"></param>
        /// <param name="mapData"></param>
        /// <returns></returns>
        public static int InsertUserConnectionMapData (int userConnectionId, string mapData)
        {
            //SELECT u.id, u.user_connection_id, u.map_data,
            //u.created_datetime FROM user_connection_map_data u;
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            StringBuilder sb = new StringBuilder ();
            sb.Append (" INSERT INTO user_connection_map_data (");
            sb.Append (" user_connection_id, map_data, created_datetime) ");
            sb.Append (" VALUES ( ");
            sb.Append (" @user_connection_id, @map_data, @created_datetime) ");

            parameters.Add ("@user_connection_id", userConnectionId);
            parameters.Add ("@map_data", mapData);
            parameters.Add ("@created_datetime", dbUtility.GetCurrentDateTime ());

            //log.Debug("Executing " + sb.ToString());
            int retVal = 0;
            dbUtility.ExecuteIdentityInsert (sb.ToString (), parameters, ref retVal);

            return retVal;
        }

        /// <summary>
        /// generate user connection mapdata in a dictionary
        /// </summary>
        /// <param name="userCollectionId"></param>
        /// <returns></returns>
        public static IDictionary GetUserConnectionMapData (int userCollectionId)
        {
            IDictionary retVal = null;
            DataRow drUserCollection = GetUserConnection (userCollectionId);
            if (drUserCollection != null)
            {
                retVal = new SortedList ();
                CollectUserConnectionMapMetric (Convert.ToInt32 (drUserCollection["user_id"]),
                    Convert.ToInt32 (drUserCollection["channel_id"]),
                    1, retVal);
            }
            return retVal;
        }

        /// <summary>
        /// recursive method that collects map data at each level
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="channelId"></param>
        /// <param name="depth"></param>
        /// <param name="degrees"></param>
        private static void CollectUserConnectionMapMetric (int userId,
            int channelId, int depth, IDictionary degrees)
        {
            DataTable dtChildren = UsersUtility.GetUserConnectionsByParentId (userId, channelId);
            if (dtChildren.Rows.Count > 0)
            {
                if (!degrees.Contains (depth))
                {
                    degrees.Add (depth, new Hashtable ());
                }
                IDictionary zipCounts = (IDictionary) degrees[depth];
                foreach (DataRow drChild in dtChildren.Rows)
                {
                    string zip = drChild["zip_code"] != DBNull.Value ?
                        drChild["zip_code"].ToString () : "";
                    int childUserConnId = Convert.ToInt32 (drChild["id"]);
                    int childUserId = Convert.ToInt32 (drChild["user_id"]);
                    string zipAreaCode = GetZipAreaCode (zip);

                    if (zipAreaCode != null)
                    {
                        if (!zipCounts.Contains (zipAreaCode))
                        {
                            zipCounts.Add (zipAreaCode, 0);
                        }
                        zipCounts[zipAreaCode] = Convert.ToInt32 (zipCounts[zipAreaCode]) + 1;
                    }
                    CollectUserConnectionMapMetric (childUserId, channelId, depth + 1,
                        degrees);
                }
            }
        }

        public static string GetZipAreaCode (string zip)
        {
            string retVal = null;
            try
            {
                //if this is a valid number, then returns first 3 digits
                int zipNum = Convert.ToInt32 (zip);
                if (zipNum > 10000)
                {
                    retVal = zip.Substring (0, 3);
                }
            }
            catch (Exception) { }
            return retVal;
        }

        /// <summary>
        /// get members that invited the most new members to the channel
        /// the result does not include the channel owner
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public static DataTable GetChannelTopMembers (int channelId, int number)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            StringBuilder sb = new StringBuilder ();
            sb.Append (" SELECT c.id, c.user_id, u.count, us.username FROM user_connection_stats u ");
            sb.Append (" INNER JOIN user_connection c ON c.id = u.user_connection_id ");
            sb.Append (" INNER JOIN users us ON us.user_id = c.user_id ");
            sb.Append (" INNER JOIN communities com ON com.community_id = c.channel_id ");
            sb.Append (" WHERE c.channel_id = @channelId AND u.depth = 0 ");
            sb.Append (" AND us.user_id <> com.creator_id AND u.count > 0");
            sb.Append (" ORDER BY u.count DESC LIMIT @number ");

            parameters.Add ("@channelId", channelId);
            parameters.Add ("@number", number);

            return dbUtility.GetDataTable (sb.ToString (), parameters);
        }

        #endregion

        #region gifts

        public static PagedDataTable GetUserGifts (int userId, string orderby, int pageNumber, int pageSize)
        {
            string selectList = "g.name, g.description, g.price, g.gift_id, u.username, u.user_id, m.message_date, m.message_id, com.name_no_spaces ";

            string tableList = "messages m, users u, message_gifts mg, gift_catalog_items g, communities_personal com ";

            string whereClause = "m.from_id = u.user_id " +
                " AND m.to_id = @userId " +
                " AND u.user_id = m.from_id " +
                " AND m.message_id = mg.message_id " +
                " AND mg.gift_id = g.gift_id" +
                " AND mg.gift_status = '" + Constants.GIFT_STATUS_ACCEPTED + "' " +
                " AND com.creator_id = u.user_id ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        ///  call a sp to get gifts counts
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataRow GetGiftCounts (int userId)
        {
            Hashtable parameters = new Hashtable ();

            string sql = "CALL GiftCounts( @user_id, @maxGifts, @giftedToMe, @giftedFromMe); select CAST(@maxGifts as UNSIGNED INT) as maxGifts, CAST(@giftedToMe as UNSIGNED INT) as giftedToMe, CAST(@giftedFromMe as UNSIGNED INT) as giftedFromMe; ";
            parameters.Add ("@user_id", userId);

            DataRow dr = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sql, parameters, false);

            return dr;
        }

        public static DataRow GetUserCounts (int userId)
        {
            Hashtable parameters = new Hashtable ();

            string sql = "CALL WokUserCounts( @user_id, @numberofnewmessages, @numberofrequests, @numberoffriends, @numberofpendingmembers, @numberofviews, @numbergiftspending, @numbergiftsreceived, @numbergiftsgiven, @numberofdiggs); select CAST(@numberofnewmessages as UNSIGNED INT) as numberofnewmessages, CAST(@numberofrequests as UNSIGNED INT) as numberofrequests, CAST(@numberoffriends as UNSIGNED INT) as numberoffriends, CAST(@numberofpendingmembers as UNSIGNED INT) as numberofpendingmembers, CAST(@numberofviews as UNSIGNED INT) as numberofviews, CAST(@numbergiftspending as UNSIGNED INT) as numbergiftspending, CAST(@numbergiftsreceived as UNSIGNED INT) as numbergiftsreceived, CAST(@numbergiftsgiven as UNSIGNED INT) as numbergiftsgiven, CAST(@numberofdiggs as UNSIGNED INT) as numberofdiggs; ";
            parameters.Add ("@user_id", userId);

            DataRow dr = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sql, parameters, false);

            return dr;
        }

        /// <summary>
        /// return number of pending gifts for a usser
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="filter"></param>
        /// <param name="orderby"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static PagedDataTable GetPendingGifts (int userId, string filter, string orderby,
            int pageNumber, int pageSize, bool get2DGifts)
        {
            string selectList = "com.thumbnail_small_path, com.name_no_spaces, m.message_id, m.from_id, m.to_id, m.subject, " +
                " m.message, m.message_date, m.replied, m.type, u.username ";

            string tableList = "messages m, users u, communities_personal com, message_gifts g ";

            string whereClause = "m.from_id = u.user_id " +
                " AND m.to_id = @userId " +
                " AND u.user_id = com.creator_id " +
                " AND m.message_id = g.message_id" +
                " AND g.gift_status = '" + Constants.GIFT_STATUS_UNACCEPTED + "' " +
                " AND g.gift_type = '" + (get2DGifts ? Constants.GIFT_2D : Constants.GIFT_3D) + "'";

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// GetGiftMessage
        /// </summary>
        public static DataRow GetGiftMessage (int userId, int messageId, bool gift2D)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            string sqlSelect = "SELECT com.thumbnail_small_path, com.name_no_spaces, m.message_id, m.from_id, m.to_id, m.subject, " +
                " m.message, m.message_date, m.replied, m.type, u.username, g.name, " +
                " g.description, g.gift_id, mg.gift_status ";

            sqlSelect += "FROM messages m, users u, communities_personal com, message_gifts mg, ";

            if (gift2D)
                sqlSelect += " gift_catalog_items g ";
            else
                sqlSelect += dbNameForGame + ".gift_catalog_3d_items  g ";

            sqlSelect += "WHERE m.from_id = u.user_id " +
                " AND (" +
                " (" +
                " m.to_id = @userId " +
                " AND m.to_viewable != 'D'" +
                ")" +
                " OR (" +
                " m.from_id = @userId " +
                " AND m.to_viewable != 'D' " +
                ")" +
                ")" +

                " AND m.message_id = @messageId" +
                " AND u.user_id = com.creator_id " +
                " AND m.message_id = mg.message_id" +
                " AND mg.gift_id = g.gift_id";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@messageId", messageId);

            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// helper to send a gift reply
        /// </summary>
        /// <param name="subjectFormat"></param>
        /// <param name="messageFormat"></param>
        private static void sendGiftReply (int userId, int messageId, string subjectFormat, string messageFormat, bool gift2D)
        {
            DataRow drMessage = GetGiftMessage (userId, messageId, gift2D);
            if (drMessage != null)
            {
                string receiverName = UsersUtility.GetUserName (userId);
                string subject = string.Format (subjectFormat, receiverName);
                string message = string.Format (messageFormat, receiverName, drMessage["name"]);

                // Insert a private message
                UserFacade userFacade = new UserFacade ();
                Message CMessage = new Message (0, userId, Convert.ToInt32 (drMessage["from_Id"]), subject,
                    message, new DateTime(), 0, 0, 0, "U", "S");

                userFacade.InsertMessage (CMessage);
            }
        }
        /// <summary>
        /// accept a gift from a user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public static bool Accept2DGift (int userId, int messageId)
        {
            // make sure they own it
            string sqlSelect = "SELECT COUNT(*) FROM messages " +
                               "WHERE to_id = @userId " +
                               "AND message_id = @messageId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@messageId", messageId);

            int count = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlSelect, parameters);
            if (count == 1)
            {
                string sqlUpdate = "CALL update_message_gifts( @messageId, '" + Constants.GIFT_STATUS_ACCEPTED + "')";

                parameters = new Hashtable ();
                parameters.Add ("@messageId", messageId);

                count = KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlUpdate, parameters);
            }

            if (count == 1)
            {
                UsersUtility.sendGiftReply (userId, messageId, UsersUtility.GIFT_ACCEPT_SUBJECT, UsersUtility.GIFT_ACCEPT_MESSAGE, true);
            }

            return count == 1;
        }

        /// <summary>
        /// accept a gift from a user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public static bool Accept2DGift (int userId, int messageId, bool gift2D)
        {
            // make sure they own it
            string sqlSelect = "SELECT COUNT(*) FROM messages " +
                               "WHERE to_id = @userId " +
                               "AND message_id = @messageId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@messageId", messageId);

            int count = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlSelect, parameters);
            if (count == 1)
            {
                string sqlUpdate = "CALL update_message_gifts( @messageId, '" + Constants.GIFT_STATUS_ACCEPTED + "')";

                parameters = new Hashtable ();
                parameters.Add ("@messageId", messageId);

                count = KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlUpdate, parameters);
            }

            if (count == 1)
            {
                UsersUtility.sendGiftReply (userId, messageId, UsersUtility.GIFT_ACCEPT_SUBJECT, UsersUtility.GIFT_ACCEPT_MESSAGE, gift2D);
            }

            return count == 1;
        }

        /// <summary>
        /// reject a gift when the item is deleted
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public static bool RejectGift (int userId, int messageId, bool gift2D)
        {
            // make sure they own it
            string sqlSelect = "SELECT COUNT(*) FROM messages " +
                "WHERE to_id = @userId " +
                "AND message_id = @messageId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@messageId", messageId);

            int count = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlSelect, parameters);
            if (count == 1)
            {
                string sqlUpdate = "CALL update_message_gifts( @messageId, '" + Constants.GIFT_STATUS_REJECTED + "')";

                parameters = new Hashtable ();
                parameters.Add ("@messageId", messageId);

                count = KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlUpdate, parameters);
            }

            if (count == 1)
            {
                UsersUtility.sendGiftReply (userId, messageId, UsersUtility.GIFT_REJECT_SUBJECT, UsersUtility.GIFT_REJECT_MESSAGE, gift2D);

            }
            return count == 1;
        }

        /// <summary>
        /// give a give, create the message, check kpoints, etc.
        /// </summary>
        /// <param name="fromId"></param>
        /// <param name="toId"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <param name="giftId"></param>
        /// <returns></returns>
        public static int GiveGift (int fromId, int toId,
            string subject, string message, int giftId, bool gift2D)
        {
            int price = 1;

            // do they have enough kpoints?
            Double curTotal = getUserBalance (fromId, Constants.CURR_KPOINT);
            if (curTotal > 0)
            {
                // make sure they buy it
                string sqlSelect = "SELECT price FROM ";
                if (gift2D)
                    sqlSelect += " gift_catalog_items ";
                else
                {
                    string dbNameForGame = KanevaGlobals.DbNameKGP;

                    sqlSelect += dbNameForGame + ".`gift_catalog_3d_items` ";
                }

                sqlSelect += " WHERE gift_id = @giftId ";

                Hashtable parameters = new Hashtable ();
                parameters.Add ("@giftId", giftId);

                price = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlSelect, parameters);
                if (price > curTotal)
                {
                    return INSUFFICIENT_FUNDS;
                }

            }
            else
            {
                return INSUFFICIENT_FUNDS;
            }

            // Insert a private message
            UserFacade userFacade = new UserFacade ();
            Message CMessage = new Message(0, fromId, toId, subject, message, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.GIFT, 0, "U", "S");

            userFacade.InsertMessage (CMessage);

            if (CMessage.MessageId > 0)
            {
                // add the new gift
                // make sure they buy it
                string sqlSelect = "CALL add_message_gifts( @messageId, @giftId, '" + Constants.GIFT_STATUS_UNACCEPTED + "', '" + (gift2D ? Constants.GIFT_2D : Constants.GIFT_3D) + "', @result); SELECT CAST(@result as UNSIGNED INT) as result";

                Hashtable parameters = new Hashtable ();
                parameters.Add ("@messageId", CMessage.MessageId);
                parameters.Add ("@giftId", giftId);

                int ok = 0;
                DataRow dr = KanevaGlobals.GetDatabaseUtility().GetDataRow(sqlSelect, parameters, false);
                if (dr != null)
                {
                    ok = Convert.ToInt32(dr["result"]);
                }

                if (ok > 0)
                {
                    // deduct kpoints
                    try
                    {
                        (new UserFacade()).AdjustUserBalance (fromId, Constants.CURR_KPOINT, -price, Constants.CASH_TT_BOUGHT_GIFT);
                    }
                    catch (Exception)
                    {
                        return INSUFFICIENT_FUNDS;
                    }

                    // finally increment the usage count
                    if (gift2D)
                        sqlSelect = "UPDATE gift_catalog_items SET gifted_count = gifted_count + 1 " +
                            " WHERE gift_id = @giftId";
                    else
                        sqlSelect = "UPDATE " + KanevaGlobals.DbNameKGP + ".gift_catalog_3d_items SET gifted_count = gifted_count + 1 " +
                            " WHERE gift_id = @giftId";

                    parameters = new Hashtable ();
                    parameters.Add ("@giftId", giftId);

                    KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlSelect, parameters);
                }

            }

            return CMessage.MessageId;
        }

        public static PagedDataTable GetUserInvetoryByCategory (int userId, int categoryId, string orderby, int pageNumber, int pageSize)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForShopping = KanevaGlobals.DbNameShop;

            string categoryIdSel = ", " + categoryId + " as item_category_id ";
            string categoryIdWhere = " AND (iw.category_id1 = @categoryId  OR iw.category_id2 = @categoryId OR iw.category_id3 = @categoryId) "; 

            if (categoryId == 0)
            {
                categoryIdSel = ", 0 as item_category_id ";
                categoryIdWhere = ""; 
            }

            string query1 = "(SELECT 'In Inv' as inv_location,i.global_id, ii.name, i.inventory_type, armed, quantity, " +
               " inventory_sub_type, ii.market_cost, ii.selling_price, ii.use_type, 0 as use_value, ii.description, COALESCE(iw.item_active, 0) as item_active, COALESCE(pg.pass_group_id, 0) AS pass_group_id " +
               categoryIdSel +
               " FROM " + dbNameForGame + ".inventories i " +
               " INNER JOIN " + dbNameForGame + ".items ii ON i.global_id = ii.global_id " +
               " LEFT OUTER JOIN " + dbNameForShopping + ".items_web iw ON iw.global_id = ii.global_id " +
               " LEFT OUTER JOIN " + dbNameForGame + ".pass_group_items pg ON pg.global_id = ii.global_id " +
               " WHERE i.player_id = (Select player_id from wok.players where user_id = " +
               " (Select user_id from wok.game_users WHERE kaneva_user_id = @userId))" +
               categoryIdWhere +
               " AND ii.permanent = 0)" +
               " UNION ALL " +
               " (SELECT 'Pnd Inv' as inv_location,i.global_id, ii.name, i.inventory_type, 'F' as armed, quantity, inventory_sub_type, " +
               " ii.market_cost, ii.selling_price, ii.use_type, 0 as use_value, ii.description, COALESCE(iw.item_active, 0) as item_active, COALESCE(pg.pass_group_id, 0) AS pass_group_id " +
               categoryIdSel +
               " FROM " + dbNameForGame + ".inventory_pending_adds2 i " +
               " INNER JOIN " +
               " " + dbNameForGame + ".items ii ON i.global_id = ii.global_id " +
               " LEFT OUTER JOIN " + dbNameForShopping + ".items_web iw ON iw.global_id = ii.global_id " +
               " LEFT OUTER JOIN " + dbNameForGame + ".pass_group_items pg ON pg.global_id = ii.global_id " +
               " WHERE kaneva_user_id = @userId " +
               " AND ii.permanent = 0 " +
               categoryIdWhere + ")";


            string orderByList = orderby;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            if (categoryId > 0)
            {
                parameters.Add ("@categoryId", categoryId);
            }

            return KanevaGlobals.GetDatabaseUtilityReadOnly ().GetPagedDataTableUnion (query1, orderByList, parameters, pageNumber, pageSize);
        }

        public static PagedDataTable SearchUserInvetory (int userId, string searchString, string orderby, int pageNumber, int pageSize)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForShopping = KanevaGlobals.DbNameShop;

            string query = "SELECT 'In Inv' as inv_location, iw.global_id, it.name, it.description, i.inventory_type, " +
                " market_cost, selling_price, use_type, 0 as use_value, display_name, quantity " +
                "FROM shopping.items_web iw " +
                " INNER JOIN wok.inventories i ON i.global_id = iw.global_id " +
                " INNER JOIN wok.items it ON it.global_id = iw.global_id " +
                " WHERE i.player_id = (SELECT player_id FROM wok.players WHERE kaneva_user_id = @userId) " +
                " AND it.name like @searchString " +
                "UNION ALL " +
                " SELECT 'Pnd Inv' as inv_location, iw.global_id, it.name, it.description, i.inventory_type, " +
                "   market_cost, selling_price, use_type, 0 as use_value, display_name, quantity  " +
                " FROM shopping.items_web iw " +
                " INNER JOIN wok.items it ON it.global_id = iw.global_id " +
                " INNER JOIN wok.inventory_pending_adds2 i ON i.global_id = iw.global_id " +
                " WHERE kaneva_user_id = @userId " +
                " AND it.name like @searchString ";

            string orderByList = orderby;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@searchString", searchString + "%");

            return KanevaGlobals.GetDatabaseUtilitySearch ().GetPagedDataTableUnion (query, orderByList, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// Get a user's Inventory
        /// </summary>
        /// <returns></returns>
        public static PagedDataTable GetUserInvetory (int userId, string orderby, int pageNumber, int pageSize)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            string query = "(SELECT 'In Inv' as inv_location,i.global_id, ii.name, i.inventory_type, armed, quantity, inventory_sub_type, ii.market_cost, ii.selling_price, ii.use_type," +
                "0 as use_value FROM " + dbNameForGame + ".inventories i " +
                "INNER JOIN " + dbNameForGame + ".items ii ON i.global_id = ii.global_id " +
                "WHERE i.player_id = (Select player_id from wok.players where user_id = " +
                "(Select user_id from wok.game_users WHERE kaneva_user_id=@userId)))" +
                " Union All" +
                " (SELECT 'Pnd Inv' as inv_location,i.global_id, ii.name, i.inventory_type, 'F' as armed, quantity, inventory_sub_type, " +
                " ii.market_cost, ii.selling_price, ii.use_type," +
                " 0 as use_value " +
                " FROM " + dbNameForGame + ".inventory_pending_adds2 i " +
                " INNER JOIN " +
                " " + dbNameForGame + ".items ii ON i.global_id = ii.global_id " +
                " WHERE kaneva_user_id =@userId)";

            string orderByList = orderby;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly ().GetPagedDataTableUnion (query, orderByList, parameters, pageNumber, pageSize);
        }

        /**
        *   Get a game user's inventory items.
        *   - 07/29/13 - DRF - Added canTrade option
        */
        public static PagedDataTable __GetWokUserInventory (int playerId, bool bDiscardPermanent, string searchString, string inventory_type, string category, bool mature, bool gifts, bool canTrade, string orderBy, int pageNumber, int pageSize)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForShopping = KanevaGlobals.DbNameShop;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;

            string selectList = "inv.global_id, inv.quantity, inv.armed, inv.inventory_sub_type, it.base_global_id, it.name, it.description, " +
                "it.selling_price, it.use_type as it_use_type, it.disarmable, it.inventory_type, it.visual_ref, it.is_default, it.actor_group, " +
                "iw.texture_path, iw.template_path_encrypted, iw.thumbnail_medium_path, iw.thumbnail_large_path, " +
                "iw.thumbnail_assetdetails_path, iut.use_type, IFNULL(NULLIF(ku.username, ''), 'Kaneva') AS creator_name, pgi.pass_group_id, " +
                "COALESCE(COALESCE(ido.playFlash, ido2.playFlash), 0) playFlash";

            string tableList = dbNameForGame + ".inventories inv " +
                " LEFT OUTER JOIN " + dbNameForGame + ".items it ON inv.global_id=it.global_id " +
                " LEFT OUTER JOIN " + dbNameForKaneva + ".users ku ON ku.user_id=it.item_creator_id " +
                " LEFT OUTER JOIN " + dbNameForShopping + ".items_web iw ON inv.global_id=iw.global_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".item_use_types iut ON it.use_type=iut.use_type_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".item_dynamic_objects ido ON ido.global_id=it.global_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".item_dynamic_objects ido2 ON ido2.global_id=it.base_global_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".pass_group_items pgi ON inv.global_id=pgi.global_id ";

            // We may have multiple categories, let's treat them as an array
            string[] catList = (category.Length != 0 ? category.Split('|') : new string[0]);

            if (catList.Length > 0)
            {
                tableList += " LEFT OUTER JOIN " + dbNameForShopping + ".item_categories ic ON ic.item_category_id=iw.category_id1 OR ic.item_category_id=iw.category_id2 OR ic.item_category_id=iw.category_id3 ";
            }

            string whereClause = " inv.player_id = @playerId ";

            if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
            {
                whereClause += " AND inv.inventory_type= @inventory_type ";
            }

            if (catList.Length > 0)
            {
                whereClause += " AND (";
                for (int i = 0, lastIndex = catList.Length - 1; i <= lastIndex; i++)
                {
                    whereClause += " ic.description = @category" + i;
                    whereClause += (i < lastIndex ? " OR " : "");
                }
                whereClause += ") ";
            }

            if (mature)
            {
                whereClause += " AND pass_group_id = 1";
            }
            else if (gifts)
            {
                whereClause += " AND " + dbNameForGame + ".is_catalog_gift(inv.global_id)";
            }

            // DRF - Added For PlayerTrade Menu Optimization
            if (canTrade)
            {
                whereClause += " AND (inv.armed='F') AND (inv.inventory_sub_type=256) AND (it.is_default!=1) ";
            }

            if ( bDiscardPermanent)
            {
                whereClause += " AND it.permanent=0 ";
            }

            if (searchString.Length != 0)
            {
                whereClause += " AND it.name like @searchTerm";
            }

            string orderByList = orderBy;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@playerId", playerId);

            if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
            {
                parameters.Add("@inventory_type", inventory_type);
            }

            if (searchString.Length != 0)
            {
                searchString = searchString.Insert(0, "%");
                searchString += "%";
                parameters.Add("@searchTerm", searchString);
            }

            if (catList.Length > 0)
            {
                for (int i = 0; i < catList.Length; i++)
                {
                    parameters.Add("@category" + i, catList[i]);
                }
            }

            return KanevaGlobals.GetDatabaseUtility().GetPagedDataTable(selectList, tableList, whereClause, orderByList, parameters, pageNumber, pageSize);
        }

        /**
        *   Get a game user's inventory items.
        */
        public static PagedDataTable GetWokUserInventory(int playerId, bool bDiscardPermanent, string searchString, string inventory_type, string category, bool mature, bool gifts, string orderBy, int pageNumber, int pageSize)
        {
            bool canTrade = false;
            return __GetWokUserInventory(playerId, bDiscardPermanent, searchString, inventory_type, category, mature, gifts, canTrade, orderBy, pageNumber, pageSize);
        }

        /**
        *   DRF - Get a game user's inventory items that can be traded.
        */
        public static PagedDataTable GetWokUserInventoryCanTrade(int playerId, bool bDiscardPermanent, string searchString, string inventory_type, string category, bool mature, bool gifts, string orderBy, int pageNumber, int pageSize)
        {
            bool canTrade = true;
            return __GetWokUserInventory(playerId, bDiscardPermanent, searchString, inventory_type, category, mature, gifts, canTrade, orderBy, pageNumber, pageSize);
        }

        /**
        *   Get a game user's inventory items, avatar items only.
        */
        public static PagedDataTable GetWokUserAvatarInventory (int playerId, bool bDiscardPermanent, string searchString, string inventory_type, string category, List<ItemCategory> itemCats, bool mature, string orderBy, int pageNumber, int pageSize)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForShopping = KanevaGlobals.DbNameShop;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;

            string selectList = "inv.global_id, inv.quantity, inv.armed, inv.inventory_sub_type, it.base_global_id, it.name, it.description, " +
        "it.selling_price, it.use_type as it_use_type, it.disarmable, it.inventory_type, it.visual_ref, it.is_default, it.actor_group, " +
                "iw.texture_path, iw.template_path_encrypted, iw.thumbnail_medium_path, iw.thumbnail_large_path, " +
                "iw.thumbnail_assetdetails_path, iut.use_type, IFNULL(NULLIF(ku.username, ''), 'Kaneva') AS creator_name, pgi.pass_group_id ";


            string tableList = dbNameForGame + ".inventories inv " +
                " LEFT OUTER JOIN " + dbNameForGame + ".items it ON inv.global_id=it.global_id " +
                " LEFT OUTER JOIN " + dbNameForKaneva + ".users ku ON ku.user_id=it.item_creator_id " +
                " LEFT OUTER JOIN " + dbNameForShopping + ".items_web iw ON inv.global_id=iw.global_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".item_use_types iut ON it.use_type=iut.use_type_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".pass_group_items pgi ON inv.global_id=pgi.global_id ";

            // We may have multiple categories, let's treat them as an array
            string[] catList = (category.Length != 0 ? category.Split('|') : new string[0]);

            if ( catList.Length > 0 || itemCats.Count > 0 )
            {
                tableList += " LEFT OUTER JOIN " + dbNameForShopping + ".item_categories ic ON ic.item_category_id=iw.category_id1 OR ic.item_category_id=iw.category_id2 OR ic.item_category_id=iw.category_id3 ";
            }

            string whereClause = "iut.use_type <> 'USE_TYPE_DEED' AND "; // always take out deeds

            if ( catList.Length > 0 )
            {
                whereClause += " (";
                for (int i = 0, lastIndex = catList.Length - 1; i <= lastIndex; i++)
                {
                    whereClause += " ic.description = @category" + i;
                    whereClause += (i < lastIndex ? " OR " : "");
                }
                whereClause += ") AND ";
            }
            else if (itemCats != null && itemCats.Count > 0)
            {
                whereClause += " ic.item_category_id IN (" + string.Join(", ", itemCats.Select(i => i.ItemCategoryId)) + ") AND ";
            }

            if ( mature)
            {
                whereClause += "pass_group_id = 1 AND ";
            }

            whereClause += " ( ( inv.player_id = @playerId1 ";

            if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
            {
                whereClause += " AND inv.inventory_type= @inventory_type1 ";
            }

            if ( bDiscardPermanent)
            {
                whereClause += " AND it.permanent=0 ";
            }

            whereClause += " AND (iut.use_type <> 'USE_TYPE_ANIMATION' AND iut.use_type <> 'USE_TYPE_ADD_DYN_OBJ' " +
            "AND iut.use_type <> 'USE_TYPE_ADD_ATTACH_OBJ' AND iut.use_type <> 'USE_TYPE_MENU' AND iut.use_type <> 'USE_TYPE_SOUND' AND iut.use_type <> 'USE_TYPE_PARTICLE' AND iut.use_type <> 'USE_TYPE_ACTION_ITEM' AND iut.use_type <> 'USE_TYPE_SCRIPT_GAME_ITEM') ) " +
            "OR ( inv.player_id = @playerId2 ";

            if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
            {
                whereClause += " AND inv.inventory_type= @inventory_type2 ";
            }

            if ( bDiscardPermanent)
            {
                whereClause += " AND it.permanent=0 ";
            }

            whereClause += " AND (iut.use_type = 'USE_TYPE_ANIMATION' AND it.actor_group > 0) )";

            whereClause += " OR ( inv.player_id = @playerId3 ";

            if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
            {
                whereClause += " AND inv.inventory_type= @inventory_type3 ";
            }

            if ( bDiscardPermanent)
            {
                whereClause += " AND it.permanent=0 ";
            }

            whereClause += " AND (iut.use_type = 'USE_TYPE_MENU')) )";

            if ( searchString.Length != 0)
            {
                whereClause += " AND it.name like @searchTerm";
            }

            string orderByList = orderBy;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@playerId1", playerId);
            parameters.Add ("@playerId2", playerId);
            parameters.Add ("@playerId3", playerId);      

            if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
            {
                parameters.Add("@inventory_type1", inventory_type);
                parameters.Add("@inventory_type2", inventory_type);
                parameters.Add("@inventory_type3", inventory_type);
            }

            if (searchString.Length != 0)
            {
                searchString = searchString.Insert(0,"%");
                searchString += "%";
                parameters.Add("@searchTerm", searchString);
            }

            if (catList.Length > 0)
            {
                for (int i = 0; i < catList.Length; i++)
                {
                    parameters.Add("@category" + i, catList[i]);
                }
            }

            return KanevaGlobals.GetDatabaseUtility().GetPagedDataTable(selectList, tableList, whereClause, orderByList, parameters, pageNumber, pageSize);
        }

        /**
        *   Get a game user's inventory items, building items only.
        */
        public static PagedDataTable GetWokUserBuildingInventory (int playerId, bool bDiscardPermanent, string searchString, string inventory_type, string category, bool mature, string orderBy, int pageNumber, int pageSize)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForShopping = KanevaGlobals.DbNameShop;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;

            string selectList = "inv.global_id, inv.quantity, inv.armed, inv.inventory_sub_type, it.base_global_id, it.name, it.description, " +
                "it.selling_price, it.use_type as it_use_type, it.disarmable, it.inventory_type, it.visual_ref, it.is_default, it.actor_group, " +
                "iw.texture_path, iw.template_path_encrypted, iw.thumbnail_medium_path, iw.thumbnail_large_path, " +
                "iw.thumbnail_assetdetails_path, iut.use_type, IFNULL(NULLIF(ku.username, ''), 'Kaneva') AS creator_name, " +
                "COALESCE(COALESCE(ido.playFlash, ido2.playFlash), 0) playFlash";

            string tableList = dbNameForGame + ".inventories inv " +
                " LEFT OUTER JOIN " + dbNameForGame + ".items it ON inv.global_id=it.global_id " +
                " LEFT OUTER JOIN " + dbNameForKaneva + ".users ku ON ku.user_id=it.item_creator_id " +
                " LEFT OUTER JOIN " + dbNameForShopping + ".items_web iw ON inv.global_id=iw.global_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".item_use_types iut ON it.use_type=iut.use_type_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".item_dynamic_objects ido ON ido.global_id=it.global_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".item_dynamic_objects ido2 ON ido2.global_id=it.base_global_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".pass_group_items pgi ON inv.global_id=pgi.global_id ";

            // We may have multiple categories, let's treat them as an array
            string[] catList = (category.Length != 0 ? category.Split('|') : new string[0]);

            if (catList.Length > 0 && (category != "Deeds") )
            {
                // "Particle Effects" category join could return duplicates, as there are multiple categories with this name assigned to these items
                if (category == "Particle Effects")
                {
                    selectList = "DISTINCT " + selectList;
                }

                tableList += " LEFT OUTER JOIN " + dbNameForShopping + ".item_categories ic ON ic.item_category_id=iw.category_id1 OR ic.item_category_id=iw.category_id2 OR ic.item_category_id=iw.category_id3 ";
            }

            string whereClause = "";

            if (catList.Length > 0)
            {
                // Special case for deeds
                if (category != "Deeds")
                {
                    whereClause += " (";
                    for (int i = 0, lastIndex = catList.Length - 1; i <= lastIndex; i++)
                    {
                        whereClause += " ic.description = @category" + i;
                        whereClause += (i < lastIndex ? " OR " : "");
                    }
                    whereClause += ") AND iut.use_type <> 'USE_TYPE_DEED' AND ";
                }
            }

            if ( mature)
            {
                whereClause += "pass_group_id = 1 AND ";
            }

            if (catList.Length > 0 && (category == "Deeds")) 
            {
                // Special case for deeds
                whereClause += " inv.player_id = @playerId1 AND iut.use_type = 'USE_TYPE_DEED' ";

                if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
                {
                    whereClause += " AND inv.inventory_type= @inventory_type1 ";
                }

                if ( bDiscardPermanent)
                {
                    whereClause += " AND it.permanent=0 ";
                }
            }
            else
            {
                whereClause += " ( ( inv.player_id = @playerId1 ";

                if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
                {
                    whereClause += " AND inv.inventory_type= @inventory_type1 ";
                }

                if ( bDiscardPermanent)
                {
                    whereClause += " AND it.permanent=0 ";
                }

                whereClause += " AND (iut.use_type = 'USE_TYPE_ADD_DYN_OBJ' OR iut.use_type = 'USE_TYPE_ADD_ATTACH_OBJ' OR iut.use_type = 'USE_TYPE_SOUND' OR iut.use_type = 'USE_TYPE_PARTICLE') ) " +
                    "OR ( inv.player_id = @playerId2 ";

                if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
                {
                    whereClause += " AND inv.inventory_type= @inventory_type2 ";
                }

                if ( bDiscardPermanent)
                {
                    whereClause += " AND it.permanent=0 ";
                }

                whereClause += " AND (iut.use_type = 'USE_TYPE_MENU' OR iut.use_type = 'USE_TYPE_DEED')) )";
            }
       
            if ( searchString.Length != 0)
            {
                whereClause += " AND it.name like @searchTerm";
            }

            string orderByList = orderBy;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@playerId1", playerId);

            if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
            {
                parameters.Add("@inventory_type1", inventory_type);
            }

            if ( (category.Length == 0) ||
                 ( (category.Length != 0) && (category != "Deeds") ) )
            {
                parameters.Add ("@playerId2", playerId);

                if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
                {
                    parameters.Add("@inventory_type2", inventory_type);
                }
            }

            if (searchString.Length != 0)
            {
                searchString = searchString.Insert(0,"%");
                searchString += "%";
                parameters.Add("@searchTerm", searchString);
            }

            if ((catList.Length > 0) && (category != "Deeds"))
            {
                for (int i = 0; i < catList.Length; i++)
                {
                    parameters.Add("@category" + i, catList[i]);
                }
            }

            return KanevaGlobals.GetDatabaseUtility().GetPagedDataTable(selectList, tableList, whereClause, orderByList, parameters, pageNumber, pageSize);
        }

        /**
        *   Get a game user's inventory items by use type.
        */
        public static PagedDataTable GetWokUserInventoryByUseType(int playerId, bool bDiscardPermanent, int[] useTypes, string searchString, string inventory_type, string category, bool mature, string orderBy, int pageNumber, int pageSize)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForShopping = KanevaGlobals.DbNameShop;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;

            // Make sure use type of some kind exists
            if (useTypes.Length == 0)
            {
                useTypes = new int[] { -1 };
            }

            string selectList = "inv.global_id, inv.quantity, inv.armed, inv.inventory_sub_type, it.base_global_id, it.name, it.description, " +
                "it.selling_price, it.use_type as it_use_type, it.disarmable, it.inventory_type, it.visual_ref, it.is_default, it.actor_group, " +
                "iw.texture_path, iw.template_path_encrypted, iw.thumbnail_medium_path, iw.thumbnail_large_path, " +
                "iw.thumbnail_assetdetails_path, iut.use_type, IFNULL(NULLIF(ku.username, ''), 'Kaneva') AS creator_name, " +
                "COALESCE(COALESCE(ido.playFlash, ido2.playFlash), 0) playFlash";

            string tableList = dbNameForGame + ".inventories inv " +
                " INNER JOIN " + dbNameForGame + ".items it ON inv.global_id=it.global_id AND it.use_type IN (" + string.Join(", ", useTypes) + ") " +
                " INNER JOIN " + dbNameForGame + ".item_use_types iut ON it.use_type=iut.use_type_id " +
                " LEFT OUTER JOIN " + dbNameForKaneva + ".users ku ON ku.user_id=it.item_creator_id " +
                " LEFT OUTER JOIN " + dbNameForShopping + ".items_web iw ON inv.global_id=iw.global_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".item_dynamic_objects ido ON ido.global_id=it.global_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".item_dynamic_objects ido2 ON ido2.global_id=it.base_global_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".pass_group_items pgi ON inv.global_id=pgi.global_id ";

            // Workaround for categorizing animations for action items given that most don't have their own categories.
            bool excludingEmotes = (category.Length != 0 && category == "Object Animations");

            // We may have multiple categories, let's treat them as an array
            string[] catList = (category.Length != 0 ? category.Split('|') : new string[0]);

            if (catList.Length > 0 && !excludingEmotes)
            {
                tableList += " LEFT OUTER JOIN " + dbNameForShopping + ".item_categories ic ON ic.item_category_id=iw.category_id1 OR ic.item_category_id=iw.category_id2 OR ic.item_category_id=iw.category_id3 ";
            }

            string whereClause = "inv.player_id = @playerId ";

            if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
            {
                whereClause += " AND inv.inventory_type= @inventory_type ";
            }

            if (bDiscardPermanent)
            {
                whereClause += " AND it.permanent=0 ";
            }

            if (catList.Length > 0)
            {
                if (excludingEmotes)
                {
                    whereClause += " AND NOT EXISTS ("+
                                    "SELECT 1 FROM " + dbNameForShopping + ".item_categories ic "+ 
                                    "WHERE ic.name IN ('Emotes', 'Emotes For Men', 'Emotes For Women') "+
                                    "AND (ic.item_category_id=iw.category_id1 OR ic.item_category_id=iw.category_id2 OR ic.item_category_id=iw.category_id3)"+
                                    ") ";
                }
                else
                {
                    whereClause += " AND (";
                    for(int i = 0, lastIndex = catList.Length - 1; i <= lastIndex; i++)
                    {
                        whereClause += " ic.name = @category" + i;
                        whereClause += (i < lastIndex ? " OR " : "");
                    }
                    whereClause += ") ";
                }
            }

            if (mature)
            {
                whereClause += " AND pass_group_id = 1 ";
            }

            if (searchString.Length != 0)
            {
                whereClause += " AND it.name like @searchTerm";
            }

            string orderByList = orderBy;

            Hashtable parameters = new Hashtable();
            parameters.Add("@playerId", playerId);

            if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
            {
                parameters.Add("@inventory_type", inventory_type);
            }

            if (searchString.Length != 0)
            {
                searchString = searchString.Insert(0, "%");
                searchString += "%";
                parameters.Add("@searchTerm", searchString);
            }

            if (catList.Length > 0 && !excludingEmotes)
            {
                for (int i = 0; i < catList.Length; i++)
                {
                    parameters.Add("@category" + i, catList[i]);
                }
            }

            return KanevaGlobals.GetDatabaseUtility().GetPagedDataTable(selectList, tableList, whereClause, orderByList, parameters, pageNumber, pageSize);
        }

        /**
        *   Get a game user's gift items.
        */
        public static PagedDataTable GetWokUserGiftInventory (int playerId, bool bDiscardPermanent, string inventory_type, string orderBy, int pageNumber, int pageSize)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForShopping = KanevaGlobals.DbNameShop;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;

            string selectList = "inv.global_id, inv.quantity, inv.armed, inv.inventory_sub_type, it.base_global_id, it.name, it.description, " +
        "it.selling_price, it.use_type as it_use_type, it.disarmable, it.inventory_type, it.visual_ref, it.is_default, it.actor_group, " +
                "iw.texture_path, iw.template_path_encrypted, iw.thumbnail_medium_path, iw.thumbnail_large_path, " +
                "iw.thumbnail_assetdetails_path, iut.use_type, IFNULL(NULLIF(ku.username, ''), 'Kaneva') AS creator_name ";

            string tableList = dbNameForGame + ".inventories inv " +
                " LEFT OUTER JOIN " + dbNameForGame + ".items it ON inv.global_id=it.global_id " +
                " LEFT OUTER JOIN " + dbNameForKaneva + ".users ku ON ku.user_id=it.item_creator_id " +
                " LEFT OUTER JOIN " + dbNameForShopping + ".items_web iw ON inv.global_id=iw.global_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".item_use_types iut ON it.use_type=iut.use_type_id ";

            string whereClause = "inv.player_id = @playerId1 ";

            if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
            {
                whereClause += " AND inv.inventory_type= @inventory_type1 ";
            }

            if ( bDiscardPermanent)
            {
                whereClause += " AND it.permanent=0 ";
            }

            whereClause += " AND " + dbNameForGame + ".is_catalog_gift(inv.global_id)";

            string orderByList = orderBy;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@playerId1", playerId);

            if (inventory_type != Constants.WOK_INVENTORY_TYPE_ALL)
            {
                parameters.Add("@inventory_type1", inventory_type);
            }

            return KanevaGlobals.GetDatabaseUtility().GetPagedDataTable(selectList, tableList, whereClause, orderByList, parameters, pageNumber, pageSize);
        }

        /**
        *   Get an item's categories
        */
        public static PagedDataTable GetWokUserItemCategories (int playerId, int glid, string orderBy, int pageNumber, int pageSize)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForShopping = KanevaGlobals.DbNameShop;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;

            string selectList = "inv.global_id, inv.quantity, IFNULL(NULLIF(ic.name,''),'Uncategorized') AS name, IFNULL(NULLIF(ku.username, ''), 'Kanvea') AS creator_name ";

            string tableList = dbNameForGame + ".inventories inv " +
                " LEFT OUTER JOIN " + dbNameForShopping + ".items_web iw ON inv.global_id=iw.global_id " +
                " LEFT OUTER JOIN " + dbNameForGame + ".items it ON inv.global_id=it.global_id " +
                " LEFT OUTER JOIN " + dbNameForKaneva + ".users ku ON ku.user_id=it.item_creator_id " +
                " LEFT OUTER JOIN " + dbNameForShopping + ".item_categories ic ON ic.item_category_id=iw.category_id1 OR ic.item_category_id=iw.category_id2 OR ic.item_category_id=iw.category_id3 ";

            string whereClause = "inv.player_id = @playerId1 AND inv.global_id = @globalId ";

            string orderByList = orderBy;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@playerId1", playerId);
            parameters.Add ("@globalId", glid);

            return KanevaGlobals.GetDatabaseUtility().GetPagedDataTable(selectList, tableList, whereClause, orderByList, parameters, pageNumber, pageSize);
        }

        /**
        *   Get the quantity of one or more items in a user's inventory
        */
        public static PagedDataTable GetWokUserItemQuantities(int playerId, string[] glids, string orderBy, int pageNumber, int pageSize)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForShopping = KanevaGlobals.DbNameShop;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;

            string selectList = "inv.global_id, inv.quantity, inv.inventory_sub_type ";

            string tableList = dbNameForGame + ".inventories inv ";

            string whereClause = "inv.player_id = @playerId AND inv.global_id IN (";
            for (int i = 0, lastIndex = glids.Length - 1; i <= lastIndex; i++)
            {
                whereClause += " @globalId" + i;
                whereClause += (i < lastIndex ? ", " : "");
            }
            whereClause += ")";

            string orderByList = orderBy;

            Hashtable parameters = new Hashtable();
            parameters.Add("@playerId", playerId);

            for (int i = 0; i < glids.Length; i++)
            {
                parameters.Add("@globalId" + i, glids[i]);
            }

            return KanevaGlobals.GetDatabaseUtility().GetPagedDataTable(selectList, tableList, whereClause, orderByList, parameters, pageNumber, pageSize);
        }

        public static bool AddWokUserInventory (int playerId, int glid)
        {
            return true;
        }

        /// <summary>
        /// Provides update access to inventory
        /// </summary>
        /// <param name="user_id"></param>
        public static int UpdateInventoryFields(Hashtable keys, Hashtable fields)
        {

					// delims in query
					string setDelim = ",";
					string andDelim = " AND ";

					// append some fields that always get updated
					fields.Add("last_touch_datetime", DateTime.Now);

					// Format Query
					StringBuilder sqlStmt = new StringBuilder();
					Hashtable parameters = new Hashtable();
						sqlStmt.Append("UPDATE " + KanevaGlobals.DbNameKGP + ".inventories");

						// format fields to be updated
						// and trim trailing delim afterwards
						sqlStmt.Append(" SET ");
						foreach (DictionaryEntry pair in fields) {
							sqlStmt.Append(pair.Key + "=@" + pair.Key + setDelim);
							parameters.Add("@" + pair.Key, pair.Value);
						}
						sqlStmt.Length -= setDelim.Length;

						// append where condition
						// and trim trailing delim afterwards
						sqlStmt.Append(" WHERE ");
						foreach (DictionaryEntry pair in keys) {
							sqlStmt.Append(pair.Key + "=@" + pair.Key + andDelim);
							parameters.Add("@" + pair.Key, pair.Value);
						}
						sqlStmt.Length -= andDelim.Length;

          return KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sqlStmt.ToString(), parameters);
        }



        /**
        *   Get name information about a set of glids that are being traded by the game
        *   client. Note that only the name is retrieved about this item. Additional
        *   columns could allow a client to exploit the item table.
        */
        public static PagedDataTable GetWokTradeItemNames(int playerId, string[] glids, string orderBy, int pageNumber, int pageSize)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            if (glids.Length == 0)
            {
                return null;
            }

            string selectList = "global_id, name";

            string tableList = dbNameForGame + ".items ";

            string whereClause = " global_id IN (";

            bool ftt = true;

            int ii = 0;
            foreach (string glid in glids)
            {
                if (!ftt)
                {
                    whereClause += ",";
                }
                ftt = false;
                whereClause += "@" + ii + "_" + glid;
                ii++;
            }

            whereClause += ")";

            Hashtable parameters = new Hashtable ();

            ii = 0;
            foreach (string glid in glids)
            {
                parameters.Add ("@" + ii + "_" + glid, glid);
                ii++;
            }

            string orderByList = orderBy;

            return KanevaGlobals.GetDatabaseUtility().GetPagedDataTable(selectList, tableList, whereClause, orderByList, parameters, pageNumber, pageSize);
        }

        public static DataTable GetWokUsersArmedItems(int playerId, bool isVIPPurchase)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForShopping = KanevaGlobals.DbNameShop;

            string selectList = "SELECT inv.global_id, it.name, it.description, iw.texture_path, iw.template_path_encrypted," +
                " iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, " +
                " IF(it.item_creator_id IN (0,4230696), 512, 256) AS inventory_type, it.item_creator_id," +
                " it.selling_price, iw.number_of_raves, IF(it.item_creator_id = 0,'kaneva', p.name) as creator_name, " +
                " (SELECT COUNT(*) FROM " + dbNameForShopping + ".item_diggs id WHERE id.global_id=inv.global_id AND id.user_id=(SELECT kaneva_user_id FROM " + dbNameForGame + ".players WHERE player_id=@playerId)) as player_rave_count, ";

            if (isVIPPurchase)
            {
                // _DesignerPrice + _TemplateDesignerPrice + _MarketCost * .90 <-- From WokItem.cs
                selectList += " CAST((IFNULL(iw.designer_price, 0) +  IFNULL(iw2.designer_price, 0) + (it.market_cost * .90)) AS UNSIGNED) AS market_cost ";
            }
            else
            {
                selectList += " IF(it.item_creator_id > 0, CAST(((it.market_cost + iw.designer_price) * 1.1) as UNSIGNED) ,market_cost) AS market_cost ";
            }

            selectList += " FROM " + dbNameForGame + ".inventories inv" +
                " LEFT OUTER JOIN " + dbNameForGame + ".items it ON inv.global_id = it.global_id" +
                " LEFT OUTER JOIN " + dbNameForShopping + ".items_web iw ON inv.global_id = iw.global_id" +
                " LEFT OUTER JOIN " + dbNameForShopping + ".items_web iw2 ON iw.base_global_id = iw2.global_id" +  //get base item info
                " LEFT OUTER JOIN " + dbNameForGame + ".players p ON p.kaneva_user_id = it.item_creator_id" +
                " WHERE inv.player_id = @playerId AND inv.inventory_type='P' AND inv.armed = 'T' AND iw.item_active = 1 AND it.permanent != 1" + 
                " ORDER BY it.name ASC";

            Hashtable parameters = new Hashtable ();
            parameters.Add("@playerId", playerId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly().GetDataTable(selectList, parameters);
        }

        /// <summary>
        /// Get a user's transaction history
        /// </summary>
        /// <returns></returns>
        public static PagedDataTable GetUserTransactions (int userId, string orderby, int pageNumber, int pageSize)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            string query = " SELECT wtl.trans_id as transaction_id, 'WTL' as tablename, username as name, created_date, trans_desc, kei_point_id as currency_type, transaction_amount, balance, quantity, null as qty_new, i.name as item_name, i.global_id as glid " +
                            " FROM kaneva.wok_transaction_log wtl " +
                            " INNER JOIN transaction_type tt on tt.transaction_type = wtl.transaction_type " +
                            " INNER JOIN users u on wtl.user_id = u.user_id " +
                            " LEFT JOIN kaneva.wok_transaction_log_details wtld on wtl.trans_id = wtld.trans_id " +
                            " LEFT JOIN wok.items i on wtld.global_id = i.global_id " +
                            " WHERE u.user_id=  @userId " +
                            " UNION ALL " +
                            " (SELECT trans_id as transaction_id, 'ITL' as tablename,    p.name,  itl.created_date, 'Inventory Action' as trans_desc, null as currency_type, null as transaction_amount, null as balance, quantity_delta as quantity, quantity_new as qty_new, i.name as item_name, i.global_id as glid " +
                            " FROM wok.inventories_transaction_log itl " +
                            " INNER JOIN wok.players p on itl.player_id = p.player_id " +
                            " LEFT JOIN wok.items i on itl.global_id = i.global_id " +
                            " WHERE kaneva_user_id = @userId) ";

            string orderByList = orderby;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly ().GetPagedDataTableUnion (query, orderByList, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// Get a user's login history
        /// </summary>
        /// <returns></returns>
        public static PagedDataTable GetUserLoginHistory (int userId, string orderby, int pageNumber, int pageSize)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            /*
             old query joining game_users for any good reason?
            string query = " SELECT 'login' as action_type, ll.user_id as kaneva_user_id, ll.created_date, null as time_played " +
                           "  FROM wok.login_log ll " +
                           "  WHERE user_id = @userId " +
                           "  UNION ALL " +
                           "  SELECT 'logout' as action_type, gu.kaneva_user_id, date_created as created_date, tp.time_played " +
                           "  FROM wok.time_played tp " +
                           "  INNER JOIN wok.game_users gu on tp.user_id = gu.kaneva_user_id " +
                           "  WHERE gu.kaneva_user_id = @userId ";
             */

            string query = " SELECT 'login' as action_type, ll.user_id as kaneva_user_id, ll.created_date, null as time_played " +
                            "  FROM wok.login_log ll " +
                            "  WHERE user_id = @userId " +
                            "  UNION ALL " +
                            "  SELECT 'logout' as action_type, user_id as kaneva_user_id, date_created as created_date, tp.time_played " +
                            "  FROM wok.time_played tp " +
                            "  WHERE user_id  = @userId ";

            string orderByList = orderby;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly ().GetPagedDataTableUnion (query, orderByList, parameters, pageNumber, pageSize);
        }


        /// <summary>
        /// add an item to a users inventory
        /// </summary>
        /// <returns></returns>
        public static void AddItemToUserInventory (int userId, string inventory_type, int global_id, int quantity)
        {
            AddItemToUserInventory (userId, inventory_type, global_id, quantity, 256);
        }
        public static void AddItemToUserInventory (int userId, string inventory_type, int global_id, int quantity, int inventorySubType)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;


            string sqlSelect = " INSERT INTO " + dbNameForGame + ".inventory_pending_adds2 " +
                "        (kaneva_user_id,  global_id,  inventory_type,  inventory_sub_type,  quantity,  last_touch_datetime ) " +
                " VALUES (@userId,        @global_id, @inventory_type, @inventorySubType,   @quantity,  NOW() ); ";

            Hashtable parameters = new Hashtable ();
            parameters.Add("@userId", userId);
            parameters.Add("@global_id", global_id);
            parameters.Add("@inventory_type", inventory_type);
            parameters.Add("@inventorySubType", inventorySubType);
            parameters.Add("@quantity", quantity);

            int ok = KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlSelect, parameters);
        }


        // Only add the inventory item for the user if it is 'new' to them and they have access to it.
        // An example of this use case is when a deed is used in a 3D App. The individual items
        // that make up the deed are not in the user's inventory. When they pick up one of the items,
        // we will add it to their inventory only if they didn't already have one of the items and they
        // have purchased it (it is available in one of their zones).
    public static void AddWokUserInventoryIfNew(int gameId, int userId, int playerId, int globalId, int inventory_sub_type)
    {
      string dbNameForGame = KanevaGlobals.DbNameKGP;
      string dbNameForDeveloper = KanevaGlobals.DbNameDeveloper;

      // Get the appId's owner.

      Constants.eITEM_EXTERNAL_ADD_SOURCE ownerSource = Constants.eITEM_EXTERNAL_ADD_SOURCE.NONE;

      string sql = "SELECT g.owner_id, p.player_id FROM " + dbNameForGame + ".players p, " + dbNameForDeveloper + ".games g WHERE game_id = @gameId AND g.owner_id = p.kaneva_user_id";

      Hashtable parameters = new Hashtable ();
      parameters.Add ("@gameId", gameId);

      DataRow dr = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sql, parameters, false);

      if (dr != null)
      {
                int owner = Convert.ToInt32 (dr["owner_id"]);
                int player = Convert.ToInt32 (dr["player_id"]);

                if ( owner != userId)
                {
                    ownerSource = Constants.eITEM_EXTERNAL_ADD_SOURCE.MODERATOR;
                    // Switch from moderator's userid/playerid to the owner's.
                    userId = owner;
                    playerId = player;
                }
                else
                {
                    ownerSource = Constants.eITEM_EXTERNAL_ADD_SOURCE.OWNER;
                }

        sql = "SELECT global_id FROM " + dbNameForGame + ".item_external_add WHERE player_id = @playerId AND global_id = @globalId";

        parameters.Clear();
        parameters.Add ("@globalId", globalId);
        parameters.Add ("@playerId", playerId);

        dr = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sql, parameters, false);

                // Check if previously picked up in a 3DApp. Prevents infinite pickups.

        if (dr == null)
        {
          // Check that the user has access to this item somewhere before blindly adding it. e.g. in a deed or ugc zone they
          // have purchased.

          sql = "SELECT global_id FROM " + dbNameForGame + ".dynamic_objects WHERE player_id = @playerId AND global_id = @globalId LIMIT 1";

          parameters.Clear();
          parameters.Add ("@globalId", globalId);
          parameters.Add ("@playerId", playerId);

                    dr = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sql, parameters, false);

          if (dr != null)
          {
            // They have access to this object somewhere. Let them proceed.

            sql = "SELECT global_id FROM " + dbNameForGame + ".inventories WHERE player_id = @playerId AND global_id = @globalId LIMIT 1";

            parameters.Clear();
            parameters.Add ("@playerId", playerId);
            parameters.Add ("@globalId", globalId);

            dr = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sql, parameters, false);

            if (dr == null)
            {
              // New to the user. Not in inventory or previously picked up in a 3DApp
              string inventory_type = "P";
              AddItemToUserInventory(userId, inventory_type, globalId, 1, inventory_sub_type);

                            // Tracked that they previously picked it up
              sql = "INSERT INTO " + dbNameForGame + ".item_external_add (player_id, global_id, source_id, created_date) VALUES ("+
                  "@playerId, @globalId, @owner, NOW())";

              parameters.Clear();
              parameters.Add ("@playerId", playerId);
              parameters.Add ("@globalId", globalId);
              parameters.Add ("@owner", (int) ownerSource);

                            KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sql, parameters);
            }
          }
        }
      }
    }

        #endregion

        #region gameonly

        /// <summary>
        /// Get the users that are in game
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="orderby"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="userName"> if searching for a specific user/param>
        /// <returns></returns>
        public static PagedDataTable GetUsersInGame (int userId, string orderby, int pageNumber, int pageSize, string userName)
    {
            int wokGameId = Configuration.WokGameId;

            string select = " SELECT p.name, p.player_id, p.kaneva_user_id, pz.current_zone, cz.name AS display_name, IF(bu.blocked_user_id IS NOT NULL, 1, 0) AS comm_blocked, " +
                                " IF(bu.blocked_user_id IS NOT NULL, 1, 0) AS follow_blocked, bu.created_date AS blocked_date,  " +
                                " 'T' AS online_in_wok, @wokGameId AS game_id, @wokGameId AS game_name, 'T' AS online, " +
                                " 1 AS im_online, '' AS im_presence, gu.is_gm, COALESCE(al.created_date, COALESCE(gu.last_logon, 0), 0) as last_logon " +
                            " FROM wok.players p " +
                            " INNER JOIN wok.game_users gu ON p.kaneva_user_id = gu.kaneva_user_id " +
                            " INNER JOIN wok.player_zones pz ON p.player_id = pz.player_id " +
                            " INNER JOIN wok.channel_zones cz ON pz.current_zone_index = cz.zone_index AND pz.current_zone_instance_id = cz.zone_instance_id " +
                            " INNER JOIN developer.active_logins al ON p.kaneva_user_id = al.user_id " +
                            " LEFT OUTER JOIN kaneva.blocked_users bu ON bu.blocked_user_id = p.kaneva_user_id AND bu.user_id = @userId " +
                            " WHERE p.in_game = 'T' ";

            Hashtable parameters = new Hashtable ();
            parameters.Clear ();

            parameters.Add ("@userId", userId);
            parameters.Add("@wokGameId", wokGameId);

            if (userName != null)
            {
                // Modify username to put percents on each side so that parameter substitution will be performed
                // correctly.

                string theName = null;

                theName = "%" + userName + "%";

                select += " AND p.name like @userName";
                parameters.Add ("@userName", theName);
            }

            return KanevaGlobals.GetDatabaseUtility ().GetPagedDataTableUnion (select, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// Get the users that are in game
        /// </summary>
        public static PagedDataTable GetUsersInGame(int gameId)
        {
            string dbNameForDeveloper = KanevaGlobals.DbNameDeveloper;

            string select = "SELECT * " +
                    " FROM " + dbNameForDeveloper + ".active_logins dal " +
                    " WHERE dal.game_id = @gameId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@gameId", gameId);
            return KanevaGlobals.GetDatabaseUtility().GetPagedDataTableUnion (select, "", parameters, 1, Int32.MaxValue);
        }

        /// <summary>
        /// Get the users that are in a GetUsersInZone
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="orderby"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="userName"> if searching for a specific user/param>
        /// <returns></returns>
        public static PagedDataTable GetUsersInZone(int userId, int zoneInstanceId, int currentzoneIndex, string orderby, int pageNumber, int pageSize, string userName)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;

            string select = "SELECT p.name, p.player_id, p.kaneva_user_id, pz.current_zone, cz.name as display_name " +
                " , IF(bu.blocked_user_id IS NOT NULL, 1, 0) AS comm_blocked, " +
                " IF(bu.blocked_user_id IS NOT NULL, 1, 0) AS follow_blocked, " +
                " bu.created_date AS blocked_date, gu.is_gm " +
                " FROM " + dbNameForGame + ".players p  " +
                    " INNER JOIN " + dbNameForGame + ".player_zones pz ON p.player_id = pz.player_id " +
                    " INNER JOIN " + dbNameForGame + ".supported_worlds sw ON " + dbNameForGame + ".zoneIndex(pz.current_zone_index) = sw.zone_index " +
                    " INNER JOIN " + dbNameForGame + ".channel_zones cz ON pz.current_zone_index = cz.zone_index and pz.current_zone_instance_id = cz.zone_instance_id " +
                    " INNER JOIN " + dbNameForGame + ".game_users gu ON p.kaneva_user_id = gu.kaneva_user_id " +
                    " LEFT OUTER JOIN " + dbNameForKaneva + ".blocked_users bu ON bu.blocked_user_id=p.kaneva_user_id AND bu.user_id = @userId " +
                    " WHERE p.in_game = 'T'" +
                    " AND pz.current_zone_index = @currentzoneIndex " +
                    " AND pz.current_zone_instance_id = @zoneInstanceId ";

            Hashtable parameters = new Hashtable ();
            parameters.Clear ();

            parameters.Add ("@userId", userId);
            parameters.Add("@currentzoneIndex", currentzoneIndex);
            parameters.Add ("@zoneInstanceId", zoneInstanceId);

            if (userName != null)
            {
                // Modify username to put percents on right side so that parameter substitution will be performed correctly.
                string theName = null;
                theName = userName + "%";

                select += " AND p.name like @userName";
                parameters.Add ("@userName", theName);
            }

            return KanevaGlobals.GetDatabaseUtility ().GetPagedDataTableUnion (select, orderby, parameters, pageNumber, pageSize);
        }

        public static DataTable GetPassIds (int userId)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            string sql = " SELECT u.pass_group_id " +
                         " FROM " + dbNameForGame + ".vw_pass_group_users u " +
                         " WHERE kaneva_user_id = @userId ";

            Hashtable parameters = new Hashtable ();
            parameters.Clear ();

            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sql, parameters);
        }

        /// <summary>
        /// Get the user gameId and location (wok or 3dapp) if logged in.
        /// </summary>
        public static DataTable GetUserLoggedInGameId(int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string dbNameForGame = KanevaGlobals.DbNameKGP;
            string dbNameForKaneva = KanevaGlobals.DbNameKaneva;
            string dbNameForDeveloper = KanevaGlobals.DbNameDeveloper;

            Hashtable parameters = new Hashtable();

            string sqlSelect = "SELECT COALESCE(gu.logged_on, 'F') as online, COALESCE(al.game_id,0) as game_id, COALESCE(gu.logged_on, 'F') as online_in_wok, ku.ustate AS onweb, ku.username, ku.user_id " +
                "FROM " + dbNameForKaneva + ".users ku LEFT OUTER JOIN " + dbNameForDeveloper + ".active_logins al ON ku.user_id=al.user_id " +
                "INNER JOIN " + dbNameForGame + ".game_users gu ON ku.user_id=gu.kaneva_user_id " +
                " WHERE ku.active = 'Y' " +
                " AND ku.user_id = @userId ";

            parameters.Add("@userId", userId);

            return dbUtility.GetDataTable (sqlSelect, parameters);
        }

        #endregion

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        public static int RemoveIPBan (string ip_address)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = " DELETE FROM ip_bans " +
                               " WHERE INET_NTOA(ip_address) = @ip_address";

            parameters.Add ("@ip_address", ip_address);

            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        public static int AddIPBan (string ip_address, string ban_start_date, string ban_end_date)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = " INSERT INTO ip_bans (ip_address, ban_start_date, ban_end_date) " +
                               " VALUES (INET_ATON(@ip_address), @ban_start_date, @ban_end_date)";

            parameters.Add ("@ip_address", ip_address);
            parameters.Add ("@ban_start_date", Convert.ToDateTime (ban_start_date));

            if (ban_end_date != null)
            {
                parameters.Add ("@ban_end_date", Convert.ToDateTime (ban_end_date));
            }
            else
            {
                parameters.Add ("@ban_end_date", ban_end_date);
            }

            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        public static DataRow GetIPsByUser (int userId)
        {
            string sqlSelect = "SELECT u.user_id, INET_NTOA(u.ip_address) as ip_address, " +
                " INET_NTOA(u.last_ip_address) as last_ip_address, ipb.ban_start_date, ipb.ban_end_date " +
                " FROM users u " +
                " LEFT JOIN ip_bans ipb ON ipb.ip_address = u.ip_address " +
                " OR ipb.ip_address = u.last_ip_address " +
                " WHERE user_id = @userId;";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
        }

        public static DataTable GetRelatedIPsByUser (int userId)
        {
            string sqlSelect = "SELECT user_id, username, display_name, " +
                " INET_NTOA(ip_address) AS ip_address, INET_NTOA(last_ip_address) AS last_ip_address " +
                " FROM users " +
                " WHERE last_ip_address = (SELECT last_ip_address FROM users WHERE user_id = @userId) " +
                " OR last_ip_address = (SELECT ip_address FROM users WHERE user_id = @userId) " +
                " ORDER BY username LIMIT 20;";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }

        public static bool IsIPBanned (string ipAddress)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT INET_NTOA(ip_address) AS ip_address " +
                " FROM ip_bans " +
                " WHERE ip_address = INET_ATON(@ipAddress);";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@ipAddress", ipAddress);

            DataRow drIP = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, true);
            return (drIP.Table.Rows.Count > 0);
        }

        public static PagedDataTable GetBannedIPs (DateTime startDate, DateTime endDate, string filter, string orderby, int pageNumber, int pageSize)
        {
            string selectList = " INET_NTOA(ip_address) AS ip_address, ban_start_date, ban_end_date ";

            string tableList = "ip_bans ";

            string whereClause = " ban_start_date >= @startDate" +
                " AND ban_end_date <= @endDate ";

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@startDate", startDate);
            parameters.Add ("@endDate", endDate);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        public static DataRow GetIPBan (string ipAddress)
        {
            string sqlSelect = "SELECT INET_NTOA(ip_address) AS ip_address, ban_start_date, ban_end_date, NOW() as curr_date " +
                " FROM ip_bans " +
                " WHERE ip_address = INET_ATON(@ipAddress);";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@ipAddress", ipAddress);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
        }


        /// <summary>
        /// Get a list of Permanent Zones not moderated by User
        /// intended for pulldown
        /// </summary>
        /// <returns></returns>
        public static DataTable GetPermanentZonesNotModeratedByUser (int userId)
        {
            //zone_type 4 = permanent 

            string sqlSelect = "SELECT distinct zone_index_plain, name FROM wok.channel_zones WHERE zone_type = @zoneType " +
                               " AND zone_index_plain not in (SELECT pzr.zone_index FROM wok.permanent_zone_roles pzr " +
                               " WHERE pzr.kaneva_user_id = @userID)";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@zoneType", "4");
            parameters.Add ("@userId", userId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }


        public static DataTable GetPermanentZoneRoles (int userId)
        {
            string sqlSelect = " SELECT distinct name, role, pzr.zone_index FROM wok.permanent_zone_roles pzr " +
                               " LEFT JOIN wok.channel_zones cz on pzr.zone_index = cz.zone_index_plain " +
                               " WHERE pzr.kaneva_user_id = @userId " +
                               " AND zone_type = 4  ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);

        }

        // Insert Permanent Zone Role
        public static int AddPermanentZoneRole (int userId, int zone_index, int role)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = " INSERT INTO wok.permanent_zone_roles (zone_index, kaneva_user_id, role) " +
                               " VALUES (@zone_index, @userId, @role)";

            parameters.Add ("@userId", userId);
            parameters.Add ("@zone_index", zone_index);
            parameters.Add ("@role", role);

            int returnValue = dbUtility.ExecuteNonQuery (sqlString, parameters);

            return returnValue;
        }

        /// <summary>
        /// Delete Zone Role
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="friendId"></param>
        /// <returns></returns>
        public static int DeletePermanentRole (int userId, int zone_index)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sql = " DELETE FROM wok.permanent_zone_roles " +
                         " WHERE kaneva_user_id = @userId AND zone_index = @zone_index;";

            parameters.Add ("@userId", userId);
            parameters.Add ("@zone_index", zone_index);
            return dbUtility.ExecuteNonQuery (sql, parameters);
        }

        public static DataTable GetWOKItems()
        {
            Hashtable parameters = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlSelect = "SELECT global_id, name, concat(global_id, ' - ',  name) as glidName " +
                               "FROM wok.items WHERE item_creator_id = 0 " +
                               "ORDER BY global_id ASC";

            return dbUtility.GetDataTable(sqlSelect, parameters);
        }

        public static DataTable GetWOKItemsIncludingUgc()
        {
            Hashtable parameters = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlSelect = "SELECT global_id, name, concat(global_id, ' - ',  name) as glidName " +
                               "FROM wok.items " +
                               "ORDER BY global_id ASC";

            return dbUtility.GetDataTable(sqlSelect, parameters);
        }

        public static DataTable GetWOKItems(string filter)
        {
            Hashtable parameters = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlSelect = "SELECT global_id, name, concat(global_id, ' - ',  name) as glidName " +
                               "FROM wok.items " +
                               "WHERE name like '%" + filter + "%'" + " or global_id like '%" + filter + "%' " +
                               "ORDER BY global_id ASC";

            return dbUtility.GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// AddWOKItem
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Obsolete("AddWOKItem is deprecated and shouldn't be called any more. It's currently only used by Activity_AddItem.aspx.cs.")]
        public static int AddWOKItem (int userId, int globalId, int qty, bool isGiftItem, bool isBankItem)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string invType = "P";
            if (isBankItem) invType = "B";

            int invSubType = 256;
            if (isGiftItem) invSubType = 512;

            string sqlString = "INSERT INTO wok.inventory_pending_adds2 VALUES (@userId, @globalID, @invType, @invSubType, @qty, NOW())";

            parameters.Add("@userId", userId);
            parameters.Add ("@globalId", globalId);
            parameters.Add ("@invType", invType);
            parameters.Add ("@invSubType", invSubType);
            parameters.Add ("@qty", qty);

            int returnValue = dbUtility.ExecuteNonQuery (sqlString, parameters);

            return returnValue;
        }

        public static DataTable GetZipCodes ()
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlString = "SELECT zip_code " +
                " FROM zip_codes " +
                " ORDER BY zip_code ASC; ";

            return dbUtility.GetDataTable (sqlString, parameters);
        }

        /// Get Clan Name
        /// 
        public static string GetClanName (int userId)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlSelectString = String.Format ("SELECT clan_name FROM users u INNER JOIN wok.players wp on u.user_id = kaneva_user_id INNER JOIN wok.clan_members wcm on wp.player_id = wcm.player_id INNER JOIN wok.clans wc on wcm.clan_id = wc.clan_id WHERE u.user_id = @userId;");

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            DataRow drClanName = dbUtility.GetDataRow (sqlSelectString, parameters, false);
            if (drClanName != null)
            {
                return (string) drClanName["clan_name"];
            }
            return null;

        }

        /// Get Clan Name
        /// 
        public static int GetClanId (int userId)
        {
            string dbNameForGame = KanevaGlobals.DbNameKGP;

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlSelectString = String.Format ("SELECT wcm.clan_id FROM users u INNER JOIN wok.players wp on u.user_id = kaneva_user_id INNER JOIN wok.clan_members wcm on wp.player_id = wcm.player_id INNER JOIN wok.clans wc on wcm.clan_id = wc.clan_id WHERE u.user_id = @userId;");

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);

            DataRow drClanId = dbUtility.GetDataRow (sqlSelectString, parameters, false);
            if (drClanId != null)
            {
                return Convert.ToInt32 (drClanId["clan_id"]);
            }
            return 0;

        }

        /// Disband Clan
        ///
        public static int ClanDisband (int userId)
        {
            int clanId = GetClanId (userId);

            // Remove members
            ClanRemoveMembers (clanId);

            string sqlString = "DELETE FROM wok.clans where clan_id = @clanId";
            Hashtable parms = new Hashtable ();
            parms.Add ("@clanId", clanId);
            return KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parms);
        }


        /// Remove clan members
        ///
        public static int ClanRemoveMembers (int clanId)
        {
            string sqlString = "DELETE FROM wok.clan_members where clan_id = @clanId";
            Hashtable parms = new Hashtable ();
            parms.Add ("@clanId", clanId);
            return KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parms);
        }


        /// <summary>
        /// UpdateNotifcation
        /// </summary>
        public static int UpdateNotifcation (int userId, int notificationId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            Hashtable parameters = new Hashtable();

            string sql = " UPDATE notifications " +
                         " SET notify_date = NOW(), notified = 1  " +
                         " WHERE user_id = @userId " +
                         " AND notification_id = @notificationId ";

            parameters.Add("@userId", userId);
            parameters.Add("@notificationId", notificationId);
            return dbUtility.ExecuteNonQuery(sql, parameters);
        }


        public static DataTable GetAllNewFameEvents(int userId)
        {
            Hashtable parameters = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlString = "SELECT notification_id " +
                " FROM notifications " +
                " ORDER BY notification_id ASC ";

            return dbUtility.GetDataTable(sqlString, parameters);
        }

        public static DataTable GetAllNewFameEventsPacket (int userId)
        {
            Hashtable parameters = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlString = "SELECT notification_id, n.created_date, p.packet_id, p.name, p.points_earned, p.rewards " +
                " FROM notifications n INNER JOIN fame.packets p ON n.packet_id = p.packet_id " +
                " WHERE n.notified = 0 " +
                " AND n.user_id = @userId " +
                " ORDER BY notification_id ASC ";

            parameters.Add("@userId", userId);
            return dbUtility.GetDataTable(sqlString, parameters);
        }

        public static DataTable GetAllNewFameEventsLevel (int userId)
        {
            Hashtable parameters = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlString = "SELECT notification_id, n.created_date, l.level_id, l.level_number, l.rewards, l.fame_type_id " +
                " FROM notifications n INNER JOIN fame.levels l ON n.level_id = l.level_id " +
                " WHERE n.notified = 0 " +
                " AND n.user_id = @userId " +
                " ORDER BY notification_id ASC ";

            parameters.Add("@userId", userId);
            return dbUtility.GetDataTable(sqlString, parameters);
        }

        public static DataTable GetAllNewFameEventsAchivements(int userId)
        {
            Hashtable parameters = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sqlString = "SELECT notification_id, n.created_date, a.achievement_id, a.name, a.points, a.image_url " +
                " FROM notifications n INNER JOIN developer.achievements a ON n.achievement_id = a.achievement_id " +
                " WHERE n.notified = 0 " +
                " AND n.user_id = @userId " +
                " ORDER BY notification_id ASC ";

            parameters.Add("@userId", userId);
            return dbUtility.GetDataTable(sqlString, parameters);
        }


        /// <summary>
        /// Get the database utility
        /// </summary>
        /// <returns></returns>
        private static DatabaseUtility GetDatabaseUtility ()
        {
            if (m_DatabaseUtilty == null)
            {
                switch (KanevaGlobals.DatabaseType)
                {
                    case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
                        {
                            m_DatabaseUtilty = new SQLServerUtility (KanevaGlobals.ConnectionStringStats);
                            break;
                        }
                    case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
                        {
                            m_DatabaseUtilty = new MySQLUtility (KanevaGlobals.ConnectionStringStats);
                            break;
                        }
                    default:
                        {
                            throw new Exception ("This database type is not supported");
                        }
                }
            }
            return m_DatabaseUtilty;
        }

        /// <summary>
        /// DataBase Utility
        /// </summary>
        private static DatabaseUtility m_DatabaseUtilty;

        /// <summary>
        /// IsReservedName
        /// </summary>
        public static bool IsReservedName (string username)
        {
            string usernameUppercase = username.ToUpper ();

            if (usernameUppercase.ToUpper ().StartsWith ("ADMIN") || (usernameUppercase.IndexOf ("GAMEMASTER") > -1) || (usernameUppercase.IndexOf ("ADVISOR") > -1)
                || (usernameUppercase.IndexOf ("KANEVA") > -1) || usernameUppercase.Equals ("MODERATOR") || usernameUppercase.Equals ("SITEADMIN")
                || usernameUppercase.Equals ("KANEVAADMIN") || (usernameUppercase.IndexOf ("ADMINISTRATOR") > -1) || usernameUppercase.Equals ("KLAUS")
                || usernameUppercase.Equals ("GORILLAPAINTBALL") || usernameUppercase.Equals ("IRIS") || usernameUppercase.Equals ("MODERATOR")
                || usernameUppercase.Equals ("KLAUSENTERTAINMENT")
                || KanevaGlobals.IsPottyMouth (username)
                )
            {
                return true;
            }
            return false;
        }
    }
}



