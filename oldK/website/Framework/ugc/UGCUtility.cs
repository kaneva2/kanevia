///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Web;
using System.Net;
using SevenZip.Sdk;
using log4net;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Linq;

using Amazon.S3;
using Amazon.S3.Transfer;

namespace KlausEnt.KEP.Kaneva
{
    public class UGCUtility
    {
        public const string ugcUploadTable = "ugc_uploads";
        public const string dynObjHeaderTable = "item_dynamic_objects";

        // ===========================
        private static Dictionary<string, string> DeployExtensionMap = new Dictionary<string, string>()
        {
            { "dob.gz",   "\\.000\\.gz" },      // *.nnn.gz
            { "dob.lzma", "\\.000\\.lzma" },    // *.nnn.lzma
            { "anm.gz",   "\\.dat\\.gz" },      // *.dat.gz
            { "anm.lzma", "\\.000\\.lzma" },    // *.nnn.lzma
            { "eqp.gz",   "\\.dat\\.gz" },      // *.dat.gz
            { "eqp.lzma", "\\.000\\.lzma" },    // *.nnn.lzma
            { "ogg.krx",  "\\.krx" },           // *.krx
            { "ogg",      "\\.ogg" },           // *.ogg
            { "par",      "\\.dat" },           // *.dat
            { "dds.gz",   "\\.dds\\.gz" },      // *.dds.gz
            { "dds.lzma", "\\.dds\\.lzma" },    // *.dds.lzma
        };

        private static Dictionary<UGCUpload.eType, string> DeployPathTypeMap = new Dictionary<UGCUpload.eType, string>()
        {
            { UGCUpload.eType.DOB_GZ,      ItemPathType.TYPE_UNIQUE_MESH },
            { UGCUpload.eType.SND_OGG_KRX, ItemPathType.TYPE_SOUND },
            { UGCUpload.eType.ANM_GZ,      ItemPathType.TYPE_ANIM },
            { UGCUpload.eType.EQP_GZ,      ItemPathType.TYPE_EQUIP },
            { UGCUpload.eType.PARTICLE,    ItemPathType.TYPE_PARTICLE },
            { UGCUpload.eType.ACTIONITEM,  ItemPathType.TYPE_SCRIPT },
            { UGCUpload.eType.TEX_DDS_GZ,  ItemPathType.TYPE_UNIQUE_TEX_DDS },
        };

        private static Dictionary<UGCUpload.eType, UGCUpload.eUsage> UGCUsageMap = new Dictionary<UGCUpload.eType, UGCUpload.eUsage>()
        {
            { UGCUpload.eType.DOB_GZ,      UGCUpload.eUsage.UGC_DYNAMIC_OBJ },
            { UGCUpload.eType.SND_OGG_KRX, UGCUpload.eUsage.UGC_SOUND },
            { UGCUpload.eType.ANM_GZ,      UGCUpload.eUsage.UGC_CHARANIM },
            { UGCUpload.eType.EQP_GZ,      UGCUpload.eUsage.UGC_EQUIPPABLE },
            { UGCUpload.eType.PARTICLE,    UGCUpload.eUsage.UGC_PARTICLE_EFFECT },
        };

        private static Dictionary<string, string> DeployFileNamePrefixMap = new Dictionary<string, string>()
        {
            { ItemPathType.TYPE_MESH,     "d" },
            { ItemPathType.TYPE_SOUND,    "s" },
            { ItemPathType.TYPE_ANIM,     "a" },
            { ItemPathType.TYPE_EQUIP,    "e" },
            { ItemPathType.TYPE_PARTICLE, "" },
            { ItemPathType.TYPE_SCRIPT,   "" },
            { ItemPathType.TYPE_ITEM_TEX_DDS, "" },
            { ItemPathType.TYPE_UNIQUE_TEX_DDS, "" },
            { ItemPathType.TYPE_UNIQUE_MESH, "" },
        };

        private static Dictionary<string, string> DeployRootPathMap = new Dictionary<string, string>()
        {
            { ItemPathType.TYPE_MESH, KanevaGlobals.MeshPath },
            { ItemPathType.TYPE_SOUND, KanevaGlobals.SoundPath },
            { ItemPathType.TYPE_ANIM, KanevaGlobals.AnimationPath },
            { ItemPathType.TYPE_EQUIP, KanevaGlobals.EquippablePath },
            { ItemPathType.TYPE_PARTICLE, KanevaGlobals.MeshPath },
            { ItemPathType.TYPE_SCRIPT, KanevaGlobals.ScriptsPath },
            { ItemPathType.TYPE_ITEM_TEX_DDS, KanevaGlobals.TexturePath },
        };

        private delegate string RootPathFunc(string relPath);

        private static Dictionary<string, RootPathFunc> DeployRootPathFuncs = new Dictionary<string, RootPathFunc>()
        {
            { ItemPathType.TYPE_UNIQUE_TEX_DDS, KanevaGlobals.GetUniqueTexturePath },
            { ItemPathType.TYPE_UNIQUE_MESH, KanevaGlobals.GetUniqueMeshPath },
        };

        private static Dictionary<string, string> DeployFilestoreMap = new Dictionary<string, string>()
        {
            { ItemPathType.TYPE_MESH, KanevaGlobals.MeshFilestore },
            { ItemPathType.TYPE_SOUND, KanevaGlobals.SoundFilestore },
            { ItemPathType.TYPE_ANIM, KanevaGlobals.AnimationFilestore },
            { ItemPathType.TYPE_EQUIP, KanevaGlobals.EquippableFilestore },
            { ItemPathType.TYPE_PARTICLE, KanevaGlobals.MeshFilestore },
            { ItemPathType.TYPE_SCRIPT, KanevaGlobals.ScriptsFilestore },
            { ItemPathType.TYPE_ITEM_TEX_DDS, KanevaGlobals.TextureFilestore },
            { ItemPathType.TYPE_UNIQUE_TEX_DDS, KanevaGlobals.UniqueTextureFilestore },
            { ItemPathType.TYPE_UNIQUE_MESH, KanevaGlobals.UniqueMeshFilestore },
        };

        private const string TEXTURE_JPG_EXT = ".jpg";
        private const string DEFAULT_EXT = ".dat";

        private const string ugcTblStdCols = "player_id, upload_type, upload_id, master_upload_id, sub_id, bytes, origName, uploadName, ifnull(summary, '') as summary, state, global_id, creationTime, updateTime, base_price, base_global_id, validation_request_id, hash, orig_size";

        public const UInt32 INVALID_SUB_ID = UInt32.MaxValue;
        private static UGCUpload.eType[] SupportedMasterUploadTypes = new UGCUpload.eType[] { UGCUpload.eType.DOB_GZ, UGCUpload.eType.MEDIA, UGCUpload.eType.ANM_GZ, UGCUpload.eType.SND_OGG_KRX, UGCUpload.eType.EQP_GZ, UGCUpload.eType.PARTICLE, UGCUpload.eType.ACTIONITEM, UGCUpload.eType.APPASSET };
        private static UGCUpload.eType[] SupportedWokItemUploadTypes = new UGCUpload.eType[] { UGCUpload.eType.DOB_GZ, UGCUpload.eType.ANM_GZ, UGCUpload.eType.SND_OGG_KRX, UGCUpload.eType.EQP_GZ, UGCUpload.eType.PARTICLE, UGCUpload.eType.ACTIONITEM };
        private static UGCUpload.eType[] SupportedDataUploadTypes = new UGCUpload.eType[] { UGCUpload.eType.DOB_GZ, UGCUpload.eType.TEX_DDS_GZ, UGCUpload.eType.CUSTOMTEX, UGCUpload.eType.ICON, UGCUpload.eType.PATTERN, UGCUpload.eType.PICTURE, UGCUpload.eType.ANM_GZ, UGCUpload.eType.SND_OGG_KRX, UGCUpload.eType.EQP_GZ, UGCUpload.eType.PARTICLE, UGCUpload.eType.ACTIONITEM, UGCUpload.eType.APPASSET };
        private static int[] BannedExclusionGroups = new int[] { 12 /*Weapon*/, 1 /*Pants*/, -1 /*Shirt*/ };

        private const UInt32 MAX_UGC_FILE_SIZE = 20 * 1024 * 1024;

        private static string TEXTURE_SUBID_TOKEN = "." + ItemPathType.MACRO_SUB_ID;
        private static string TEXTURE_QUALITY_TOKEN = "." + ItemPathType.MACRO_QUALITY;

        private static AmazonS3Client GetAmazonS3Client()
        {
            return new AmazonS3Client(Configuration.AmazonS3AccessKeyId,
                          Configuration.AmazonS3SecretAccessKey,
                          Amazon.RegionEndpoint.USEast1);
        }

        public static bool AssetFileExists(string strAssetFullPath)
        {
            if (Configuration.UseAmazonS3Storage)
            {
                AmazonS3Client azS3client = GetAmazonS3Client();
                Amazon.S3.IO.S3FileInfo s3FileInfo = new Amazon.S3.IO.S3FileInfo(azS3client, Configuration.AmazonS3Bucket, strAssetFullPath.Replace("\\", "/"));
                return s3FileInfo.Exists;
            }
            else
            {
                FileInfo fileInfo = new FileInfo(strAssetFullPath);
                return fileInfo.Exists;
            }
        }

        /// <summary>
        /// PrepareUGCUpload - validate submission request and create corresponding upload records if request is valid
        /// </summary>
        public static Boolean PrepareUGCUpload(int userId, UGCUpload.eType uploadType, int masterUploadId, UInt32 subId, Dictionary<int, string> properties, int bytes, string summary, ref int uploadId, ref float basePrice, ref string validationMsg, ref string validationMsgDetail)
        {
            bool uploadRecordPreCreated = false;

            if (masterUploadId != -1)	// A child item
            {
                // check if subId is valid
                if (subId == INVALID_SUB_ID)
                    return false;

                // check if parent record exists
                DataRow masterDr = GetUGCUpload(userId, UGCUpload.eType.UNKNOWN, masterUploadId);
                if (masterDr == null)
                    return false;   // return false if master record not found

                bool isDerivation = false;
                try
                {
                    isDerivation = Convert.ToInt32(masterDr["state"]) == (int)UGCUpload.eState.DERIVATION;
                }
                catch (System.Exception)
                {
                    return false;
                }

                if (isDerivation)
                    uploadRecordPreCreated = true;  // sub upload records have been pre-created if this is a derivation -- will simply return existing upload_id instead
            }
            else				// A master item
            {
                // check if uploadType is allowed without a masteruploadid
                if (!IsSupportedMasterUploadType(uploadType))
                {
                    // not a valid master upload type
                    return false;
                }
            }

            int requestId = -1;
            int priceTier = -1;

            if (!ValidateUGCProperties(userId, uploadType, masterUploadId, subId, properties, ref requestId, ref validationMsg, ref validationMsgDetail, ref priceTier, ref basePrice))
            {
                return false;
            }

            // Under special circumstances upload records are pre-created (mostly derivation)
            // If so, update them instead of creating new
            if (uploadRecordPreCreated)
            {
                // upload record already created (along with master record creation)
                DataRow dr = GetUGCUpload(userId, uploadType, masterUploadId, subId);
                if (dr == null)
                    return false;

                try
                {
                    uploadId = Convert.ToInt32(dr["upload_id"]);
                }
                catch (System.Exception)
                {
                    return false;
                }

                return true;
            }
            else
            {
                // create a new upload record
                return InsertUploadRecord(userId, uploadType, masterUploadId, masterUploadId != -1 ? (Int64)subId : -1, bytes, UGCUpload.eState.WAITING, requestId, priceTier, basePrice, summary, 0, ref uploadId);
            }
        }

        /// <summary>
        /// CancelUGCUpload - change upload state to cancelled
        /// </summary>
        public static Boolean CancelUGCUpload(int userId, UGCUpload.eType uploadType, int uploadId)
        {
            return SetUGCUploadState(userId, uploadType, uploadId, UGCUpload.eState.DONTCARE, UGCUpload.eState.CANCELLED);
        }

        /// <summary>
        /// CompleteUGCUpload - change upload state to completed
        /// </summary>
        public static Boolean CompleteUGCUpload(int userId, UGCUpload.eType uploadType, int uploadId, int globalId, UGCUpload.eState uploadedStateId)
        {
            Boolean ret = SetUGCUploadGlobalId(userId, uploadType, uploadId, globalId);
            if (!ret)
                return false;

            return SetUGCUploadState(userId, uploadType, uploadId, uploadedStateId, UGCUpload.eState.COMPLETED);
        }

        /// <summary>
        /// CancelUGCUploadByMasterId - change upload state to cancelled
        /// </summary>
        public static Boolean CancelUGCUploadByMasterId(int userId, UGCUpload.eType uploadType, int masterId)
        {
            return SetUGCUploadStateByMasterId(userId, uploadType, masterId, UGCUpload.eState.DONTCARE, UGCUpload.eState.CANCELLED);
        }

        /// <summary>
        /// CompleteUGCUploadByMasterId - change upload state to completed
        /// </summary>
        public static Boolean CompleteUGCUploadByMasterId(int userId, UGCUpload.eType uploadType, int masterId, int globalId, UGCUpload.eState uploadedStateId)
        {
            Boolean ret = SetUGCUploadGlobalIdByMasterId(userId, uploadType, masterId, globalId);
            if (!ret)
                return false;

            return SetUGCUploadStateByMasterId(userId, uploadType, masterId, uploadedStateId, UGCUpload.eState.COMPLETED);
        }

        /// <summary>
        /// CreateUGCTemplateItem - create an template item for newly created UGC_DO (for derivation purposes)
        /// </summary>
        public static bool CreateUGCTemplateItem(int userId, int masterId, WOKItem item)
        {
            // validate master Id
            DataRow dr = GetUGCUpload(userId, UGCUpload.eType.DOB_GZ, masterId);
            if (dr == null)
                return false;

            UGCUpload.eState state = UGCUpload.eState.FAILED;
            try
            {
                state = (UGCUpload.eState)Convert.ToInt32(dr["state"]);
            }
            catch (System.Exception)
            {
                return false;
            }

            if (state != UGCUpload.eState.UPLOADED)
                return false;

            // validate item record
            //@@TBD

            // create template
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            shoppingFacade.CreateItemTemplate(item);

            return false;
        }

        /// <summary>
        /// DeployUGCDynamicObject - deploy necessary files for a newly created UGC_DO
        /// </summary>
        public static bool DeployUploadedFile(int userId, UGCUpload.eType uploadType, int masterId, int globalId, string pathType, out string deployRelativePath, out int size, out int compressedSize, out uint uniqueAssetId)
        {
            deployRelativePath = null;
            size = 0;
            compressedSize = 0;
            uniqueAssetId = 0;

            ShoppingFacade shoppingFacade = new ShoppingFacade();
            ItemPathType pathTypeInfo = shoppingFacade.GetItemPathType(pathType);
            if (pathTypeInfo == null)
            {
                m_logger.Error("DeployUploadedFile: error obtaining path type information for `" + pathType + "'");
                return false;
            }

            // validate master Id
            DataRow dr = GetUGCUpload(userId, uploadType, masterId);
            if (dr == null)
            {
                m_logger.Warn("DeployUploadedFile: error retrieving upload record, masterUploadId=" + masterId + ", uploadType = " + uploadType + ", userId = " + userId);
                return false;
            }

            string inPath = Convert.ToString(dr["uploadName"]);

            UGCUpload.eState state = UGCUpload.eState.FAILED;
            try
            {
                state = (UGCUpload.eState)Convert.ToInt32(dr["state"]);
            }
            catch (System.Exception)
            {
                m_logger.Warn("DeployUploadedFile: error getting upload state, masterUploadId=" + masterId);
                return false;
            }

            if (state != UGCUpload.eState.UPLOADED)
            {
                m_logger.Warn("DeployUploadedFile: invalid upload state: expecting UPLOADED, got " + state + ", masterUploadId=" + masterId);
                return false;
            }

            bool uniqueFileStore = pathTypeInfo.StoreType == ItemPathType.AssetStoreType.UNIQUE;

            //check if deployment path type is compatible with uploaded format - convert file format if necessary
            string uploadFileFormat = UGCUpload.GetUploadFileFormat(uploadType);
            byte[] hashSHA256;
            if (!CheckAndConvertUploadedFile(ref inPath, uploadFileFormat, pathTypeInfo.FileFormat, uniqueFileStore ? true : false, out hashSHA256, out size, out compressedSize))
            {
                m_logger.Warn("DeployUploadedFile: uploaded file is in an incompatible format: expecting [" + pathTypeInfo.FileFormat + "], got [" + uploadFileFormat + "], globalId = " + globalId + ", masterUploadId=" + masterId);
                return false;
            }

            string deployRelativePathNoFS;
            bool isNewAsset = true;
            if (uniqueFileStore)
            {
                // Check if asset already exists in file store
                if (hashSHA256 == null)
                {
                    m_logger.Error("DeployUploadedFile: error hashing asset file for unique file store: path type ID = " + pathTypeInfo.TypeId + ", globalId = " + globalId + ", masterUploadId=" + masterId);
                    return false;
                }

                if (pathTypeInfo.SHA256Table == "")
                {
                    m_logger.Error("DeployUploadedFile: missing unique asset table name: path type ID = " + pathTypeInfo.TypeId + ", globalId = " + globalId + ", masterUploadId=" + masterId);
                    return false;
                }

                uniqueAssetId = shoppingFacade.AllocUniqueAssetIdBySHA256(pathTypeInfo, hashSHA256, size, compressedSize, out isNewAsset);
                deployRelativePathNoFS = GetDeployRelativePathUnique(uniqueAssetId, pathTypeInfo); // c/00/00/12.dob.lzma

                // Write per-item file store prefix if defined. This is particularly useful for preview and RC environment where two separate file store prefixes are used.
                string strFileStorePrefix = GetDeployFilestore(pathTypeInfo);
                if (strFileStorePrefix != "")
                {
                    shoppingFacade.AddItemPathFileStore(globalId, pathTypeInfo, strFileStorePrefix);
                }
            }
            else
            {
                deployRelativePathNoFS = GetDeployRelativePathByItem(userId, globalId, pathTypeInfo); // 4135960/3315465.002.lzma
            }

            // Path.Combine works even if one of the string is empty (e.g., empty file store name, will return the other string)
            deployRelativePath = Path.Combine(GetDeployFilestore(pathTypeInfo), deployRelativePathNoFS); // meshes/c/00/00/12.dob.lzma or meshes/4135960/3315465.002.lzma
            string deployPathBase = GetDeployRootPath(pathType, deployRelativePathNoFS); // \\...\meshes
            string deployPath = Path.Combine(deployPathBase, deployRelativePathNoFS.Replace('/', '\\')); // \\...\meshes\c\00\00\12.dob.lzma

            m_logger.Debug("DeployUploadedFile(" + globalId.ToString() + "): use [" + deployRelativePath + "] for item_paths");
            m_logger.Debug("DeployUploadedFile(" + globalId.ToString() + "): use [" + deployPath + "] for deployment");

            if (!isNewAsset)
            {
                // Already deployed - check if asset file actually exists
                if (AssetFileExists(deployPath))
                {
                    // No deployment required
                    return true;
                }
                else
                {
                    // Re-deploy
                    m_logger.Info("DeployUploadedFile(" + globalId.ToString() + "): unique mesh record exists but asset file not found - re-deploy to [" + deployPath + "]");
                }
            }

            if (Configuration.UseAmazonS3Storage)
            {
                try
                {
                    AmazonS3Client azS3client = GetAmazonS3Client();
                   
                    Amazon.S3.IO.S3FileInfo s3FileInfo = new Amazon.S3.IO.S3FileInfo(azS3client, Configuration.AmazonS3Bucket, inPath.Replace("\\", "/"));
                    Amazon.S3.IO.S3FileInfo s3FileDeployInfo = new Amazon.S3.IO.S3FileInfo(azS3client, Configuration.AmazonS3Bucket, deployPath.Replace("\\", "/"));

                    m_logger.Debug("DeployUploadedFile(" + globalId.ToString() + "): S3 copy from [" + s3FileInfo.FullName + "] to [" + s3FileDeployInfo.FullName + "], bucket: [" + Configuration.AmazonS3Bucket + "]");
                    s3FileInfo.CopyTo(s3FileDeployInfo);
                }
                catch (AmazonS3Exception s3Exception)
                {
                    m_logger.Error("DeployUploadedFile(" + globalId.ToString() + "): S3 Error " + s3Exception.Message, s3Exception);
                }
            }
            else
            {
                try
                {
                    FileInfo fileInfo = new FileInfo(deployPath);
                    if (!fileInfo.Directory.Exists)
                    {
                        fileInfo.Directory.Create();
                    }
                }
                catch (System.Exception ex)
                {
                    m_logger.Warn("DeployUploadedFile: " + ex.ToString() + " while creating folder for [" + deployPath + "], globalId = " + globalId + ", masterUploadId=" + masterId);
                    return false;
                }

                //copy file to patch location or notify server to do this
                try
                {
                    File.Copy(inPath, deployPath);
                }
                catch (System.Exception ex)
                {
                    m_logger.Warn("DeployUploadedFile: " + ex.ToString() + " while copying file [" + inPath + "] to [" + deployPath + "], globalId = " + globalId + ", masterUploadId=" + masterId);
                    return false;
                }
            }



            return true;
        }

        /// <summary>
        /// SetupUGCDynamicObjectHeader - setup data in items_dynamic_objects table for a UGC_DO
        /// </summary>
        public static Boolean SetupUGCDynamicObjectHeader(int userId, int masterId, int globalId, int validationReqId)
        {
            //@@Todo: get dynamic object header information from somewhere
            //Currently write a record of default data into items_dynamic_object
            // validate master Id
            DataRow dr = GetUGCUpload(userId, UGCUpload.eType.DOB_GZ, masterId);
            if (dr == null)
                return false;

            UGCUpload.eState state = UGCUpload.eState.FAILED;
            try
            {
                state = (UGCUpload.eState)Convert.ToInt32(dr["state"]);
            }
            catch (System.Exception)
            {
                return false;
            }

            if (state != UGCUpload.eState.UPLOADED)
                return false;

            IDictionary<int, string> properties = GetValidationRequestDetails(validationReqId);

            bool mediaSupport = false;
            if (properties.ContainsKey(UGCProperty.PROP_MEDIA_PLAYBACK))
            {
                mediaSupport = properties[UGCProperty.PROP_MEDIA_PLAYBACK] == "1";
            }

            bool canCollide = true;
            if (properties.ContainsKey(UGCProperty.PROP_COLLISION))
            {
                canCollide = properties[UGCProperty.PROP_COLLISION] == "1";
            }

            float minX = -1;
            if (properties.ContainsKey(UGCProperty.PROP_BOUNDBOX_MINX))
            {
                minX = Convert.ToSingle(properties[UGCProperty.PROP_BOUNDBOX_MINX]);
            }

            float minY = -1;
            if (properties.ContainsKey(UGCProperty.PROP_BOUNDBOX_MINY))
            {
                minY = Convert.ToSingle(properties[UGCProperty.PROP_BOUNDBOX_MINY]);
            }

            float minZ = -1;
            if (properties.ContainsKey(UGCProperty.PROP_BOUNDBOX_MINZ))
            {
                minZ = Convert.ToSingle(properties[UGCProperty.PROP_BOUNDBOX_MINZ]);
            }

            float maxX = 1;
            if (properties.ContainsKey(UGCProperty.PROP_BOUNDBOX_MAXX))
            {
                maxX = Convert.ToSingle(properties[UGCProperty.PROP_BOUNDBOX_MAXX]);
            }

            float maxY = 1;
            if (properties.ContainsKey(UGCProperty.PROP_BOUNDBOX_MAXY))
            {
                maxY = Convert.ToSingle(properties[UGCProperty.PROP_BOUNDBOX_MAXY]);
            }

            float maxZ = 1;
            if (properties.ContainsKey(UGCProperty.PROP_BOUNDBOX_MAXZ))
            {
                maxZ = Convert.ToSingle(properties[UGCProperty.PROP_BOUNDBOX_MAXZ]);
            }

            return InsertDynamicObjectHeader(globalId, false, false, mediaSupport, canCollide, minX, minY, minZ, maxX, maxY, maxZ);
        }

        /// <summary>
        /// ValidateUGCProperties - validate UGC submission request based on asset properties sent from client
        /// </summary>
        private static Boolean ValidateUGCProperties(int userId, UGCUpload.eType uploadType, int masterUploadId, UInt32 subId, Dictionary<int, string> properties, ref int requestId, ref string msg, ref string msgDetail, ref int priceTier, ref float basePrice)
        {
            int userRole = 0;
            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);
            try
            {
                userRole = user.Role;
            }
            catch (Exception) { }

            Boolean bValidated = false;

            uint roleValue = 1;
            bool rulesFound = false;    // true if validation rules found for at least one of the roles
            for (int bitShift = 0; bitShift < 32; bitShift++, roleValue <<= 1)
            {
                if ((userRole & roleValue) != 0)
                {
                    bool rulesFoundForCurrentRole = false;
                    switch (uploadType)
                    {
                        case UGCUpload.eType.DOB_GZ:
                        case UGCUpload.eType.ANM_GZ:
                        case UGCUpload.eType.SND_OGG_KRX:
                        case UGCUpload.eType.EQP_GZ:
                        case UGCUpload.eType.PARTICLE:
                            bValidated = DoValidateUGCProperties(userId, roleValue, masterUploadId, subId, GetUsageTypeByUploadType(uploadType), properties, ref requestId, ref msg, ref msgDetail, ref rulesFoundForCurrentRole, ref basePrice);
                            break;

                        case UGCUpload.eType.TEX_DDS_GZ:
                        case UGCUpload.eType.ICON:
                            bValidated = true;
                            break;

                        case UGCUpload.eType.MEDIA:
                        case UGCUpload.eType.PATTERN:
                        case UGCUpload.eType.PICTURE:
                        case UGCUpload.eType.APPASSET:
                            //@@Todo: validate custom texture
                            bValidated = true;
                            break;

                        case UGCUpload.eType.ACTIONITEM:
                            basePrice = 0;
                            bValidated = DoInsertValidationRequest(userId, GetUsageTypeByUploadType(uploadType), properties, ref requestId);
                            break;

                        default:
                            break;
                    }

                    rulesFound = rulesFound | rulesFoundForCurrentRole;
                    if (bValidated)
                        break;
                }
            }

            if (!bValidated && !rulesFound)
            {
                // No matching rules, reject submission
                basePrice = 999999999;
                msg = "Requested submission type is not supported";
                msgDetail = "";
            }

            return bValidated;
        }

        /// <summary>
        /// PrepareUGCDerivation - Validate derivation request and create corresponding upload records if request is valid
        /// </summary>
        public static Boolean PrepareUGCDerivation(int userId, UGCUpload.eType uploadType, int baseGlid, Dictionary<int, string> properties, ref int uploadId, ref float basePrice, ref string validationMsg, ref string validationMsgDetail, ref Int64[] subIds)
        {
            if (!IsItemDerivable(baseGlid))
                return false;

            string baseItemName = null, creatorName = null;
            if (!GetDerivableTemplateInfo(baseGlid, ref baseItemName, ref creatorName, ref subIds, ref basePrice))
                return false;

            UGCUpload.eType subType = GetSubUploadType(uploadType);
            if (subType == UGCUpload.eType.UNKNOWN)
                return false;

            string summary = "Derivation Work";
            if (baseItemName != null)
                summary = "Derived from [" + baseItemName + "]";
            if (creatorName != null)
                summary = summary + " created by " + creatorName;

            // Create main upload entry
            if (!InsertUploadRecord(userId, uploadType, -1, -1, 0, UGCUpload.eState.DERIVATION, -1, -1, basePrice, summary, baseGlid, ref uploadId))
                return false;

            // Pre-create upload entries for textures
            foreach (Int64 subId in subIds)
            {
                int subUploadId = 0;
                if (!InsertUploadRecord(userId, subType, uploadId, subId, -1, UGCUpload.eState.WAITING, -1, -1, -1, "Texture", baseGlid, ref subUploadId))
                    return false;
            }

            // Pre-create upload entry for icon
            int iconUploadId = 0;
            if (!InsertUploadRecord(userId, UGCUpload.eType.ICON, uploadId, 0, -1, UGCUpload.eState.WAITING, -1, -1, -1, "Icon", baseGlid, ref iconUploadId))
                return false;

            return true;
        }

        /// <summary>
        /// ProcessUGCUpload
        /// </summary>
        public static Boolean ProcessUGCUpload(int userId, UGCUpload.eType uploadType, int uploadId, HttpFileCollection files, string originalName, string originalHash, int originalSize)
        {
            if (files == null || files.Count == 0)
                return false;

            // Actual bytes uploaded
            int totalSize = 0;
            for (int i = 0; i < files.Count; i++)
            {
                totalSize += files[i].ContentLength;
            }

            if (totalSize > MAX_UGC_FILE_SIZE || !ValidateUGCUpload(userId, uploadType, uploadId, totalSize))
            {
                // No matching records
                SetUGCUploadState(userId, uploadType, uploadId, UGCUpload.eState.DONTCARE, UGCUpload.eState.FAILED);
                return false;
            }
            else
            {
                // set original hash/size and bytes uploaded
                SetUGCFileMetadata(userId, uploadType, uploadId, originalHash, originalSize);
                SetUGCUploadSize(userId, uploadType, uploadId, totalSize);

                // Set original name
                SetUGCOriginalName(userId, uploadType, uploadId, originalName);

                bool res = true;
                string fullPath = "";
                for (int i = 0; i < files.Count; i++)
                    res = res && SaveUGCUpload(userId, uploadType, uploadId, files[i], i, out fullPath);
                if (res)
                {
                    SetUGCUploadName(userId, uploadType, uploadId, fullPath);
                    SetUGCUploadState(userId, uploadType, uploadId, UGCUpload.eState.WAITING, UGCUpload.eState.UPLOADED);
                }
                else
                {
                    SetUGCUploadState(userId, uploadType, uploadId, UGCUpload.eState.DONTCARE, UGCUpload.eState.FAILED);
                }

                return res;
            }
        }

        /// <summary>
        /// DeployUploadedMedium
        /// </summary>
        public static Boolean DeployUploadedMedia(int userId, int channelId, UGCUpload.eType uploadType, int uploadId, string userHostAddress, out int assetId, out string textureUrl)
        {
            assetId = -1;
            textureUrl = "";

            UGCUpload.eType mediumUploadType = UGCUpload.eType.PATTERN;
            DataTable dt = GetUGCUploads(userId, UGCUpload.eType.PATTERN, uploadId);
            if (dt == null)
            {
                mediumUploadType = UGCUpload.eType.PICTURE;
                dt = GetUGCUploads(userId, UGCUpload.eType.PICTURE, uploadId);
                if (dt == null)
                    return false;
            }

            if (dt.Rows.Count != 1) // only one match allowed
                return false;

            DataRow dr = dt.Rows[0];

            string uploadedFile = null;
            string originalFileName = null;
            try
            {
                uploadedFile = Convert.ToString(dr["uploadName"]);
                originalFileName = Convert.ToString(dr["origName"]);
            }
            catch (System.Exception) { }

            if (uploadedFile == null)
                return false;

            string title = "";
            string desc = "";

            if (originalFileName != null)
            {
                originalFileName = System.IO.Path.GetFileName(originalFileName);
                title = System.IO.Path.GetFileNameWithoutExtension(originalFileName);
                desc = title;
            }

            int assetType, assetSubType;
            if (GetAssetTypeByUploadType(mediumUploadType, out assetType, out assetSubType))
            {
                AssetUtility.CreateAssetFromFile(userId, channelId, uploadedFile, originalFileName, title, "", desc, "", "", 0, 1, false, assetType == (int)Constants.eASSET_TYPE.PATTERN, userHostAddress, out assetId, out textureUrl);
            }

            return assetId != -1;
        }

        #region Database Access Functions

        /// <summary>
        /// InsertUploadRecord - insert a new record into ugc_uploads table
        /// </summary>
        private static Boolean InsertUploadRecord(int userId, UGCUpload.eType uploadType, int masterUploadId, Int64 subId, int bytes, UGCUpload.eState state, int requestId, int priceTier, float basePrice, string summary, int baseGlobalId, ref int uploadId)
        {

            string sqlInsert = "insert into " + ugcUploadTable + " " +
                               "(player_id, upload_type, master_upload_id, sub_id, bytes, hash, state, creationTime, updateTime, validation_request_id, tier_id, base_price, summary, base_global_id) " +
                               "values (@playerId, @uploadType, @masterUploadId, @subId, @bytes, '', @state, now(), creationTime, @reqId, @tierId, @basePrice, @summary, @baseGlobalId)";

            Hashtable parameters = new Hashtable();
            parameters.Add("@playerId", userId);
            parameters.Add("@uploadType", (int)uploadType);
            parameters.Add("@masterUploadId", masterUploadId);
            if (masterUploadId != -1)
                parameters.Add("@subId", subId);
            else
                parameters.Add("@subId", -1);
            parameters.Add("@bytes", bytes);
            parameters.Add("@state", (int)state);
            parameters.Add("@reqId", requestId);
            parameters.Add("@tierId", priceTier);
            parameters.Add("@basePrice", basePrice);
            parameters.Add("@summary", summary);
            parameters.Add("@baseGlobalId", baseGlobalId);

            int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteIdentityInsert(sqlInsert, parameters, ref uploadId);
            return rowsAffected == 1;
        }

        /// <summary>
        /// ValidateUGCUpload - validate upload request against pre-created upload records
        /// </summary>
        public static Boolean ValidateUGCUpload(int userId, UGCUpload.eType uploadType, int uploadId, int bytes)
        {
            string sqlSelect = "select state from " + ugcUploadTable + " where player_id=@playerId and upload_type=@uploadType and upload_id=@uploadId and (bytes=@bytes or bytes=-1) and state=@state";

            Hashtable parameters = new Hashtable();
            parameters.Add("@playerId", userId);
            parameters.Add("@uploadType", (int)uploadType);
            parameters.Add("@uploadId", uploadId);
            parameters.Add("@bytes", bytes);
            parameters.Add("@state", (int)UGCUpload.eState.WAITING);

            DataRow dr = KanevaGlobals.GetDatabaseUtilityKGP().GetDataRow(sqlSelect, parameters, false);
            if (dr == null)
                return false;

            return true;
        }

        /// <summary>
        /// SetUGCUploadGlobalId
        /// </summary>
        public static Boolean SetUGCUploadGlobalId(int userId, UGCUpload.eType uploadType, int uploadId, int globalId)
        {
            string sqlUpdate = "update " + ugcUploadTable + " set global_id=@globalId, updateTime=now() where player_id=@playerId and upload_type=@uploadType and upload_id=@uploadId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@playerId", userId);
            parameters.Add("@uploadType", (int)uploadType);
            parameters.Add("@uploadId", uploadId);
            parameters.Add("@globalId", globalId);

            int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sqlUpdate, parameters);
            return rowsAffected == 1;
        }

        /// <summary>
        /// SetUGCUploadState
        /// </summary>
        public static Boolean SetUGCUploadState(int userId, UGCUpload.eType uploadType, int uploadId, UGCUpload.eState currState, UGCUpload.eState finalState)
        {
            if (finalState == UGCUpload.eState.WAITING)  // Set to any state other than UGCUpload.State.WAITING
                return false;

            string sqlUpdate = "update " + ugcUploadTable + " set state=@finalState, updateTime=now() where player_id=@playerId and upload_type=@uploadType and upload_id=@uploadId";
            if (currState != UGCUpload.eState.DONTCARE)
                sqlUpdate += " and state=@currState";

            Hashtable parameters = new Hashtable();
            parameters.Add("@playerId", userId);
            parameters.Add("@uploadType", (int)uploadType);
            parameters.Add("@uploadId", uploadId);
            parameters.Add("@finalState", (int)finalState);
            if (currState != UGCUpload.eState.DONTCARE)
                parameters.Add("@currState", (int)currState);

            int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sqlUpdate, parameters);
            return rowsAffected == 1;
        }

        /// <summary>
        /// SetUGCUploadGlobalIdByMasterId
        /// </summary>
        public static Boolean SetUGCUploadGlobalIdByMasterId(int userId, UGCUpload.eType uploadType, int masterId, int globalId)
        {
            string sqlUpdate = "update " + ugcUploadTable + " set global_id=@globalId, updateTime=now() where player_id=@playerId and upload_type=@uploadType and master_upload_id=@masterId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@playerId", userId);
            parameters.Add("@uploadType", (int)uploadType);
            parameters.Add("@masterId", masterId);
            parameters.Add("@globalId", globalId);

            int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sqlUpdate, parameters);
            return rowsAffected > 0;
        }

        /// <summary>
        /// SetUGCUploadStateByMasterId
        /// </summary>
        public static Boolean SetUGCUploadStateByMasterId(int userId, UGCUpload.eType uploadType, int masterId, UGCUpload.eState currState, UGCUpload.eState finalState)
        {
            if (finalState == UGCUpload.eState.WAITING)  // Set to any state other than UGCUpload.State.WAITING
                return false;

            string sqlUpdate = "update " + ugcUploadTable + " set state=@finalState, updateTime=now() where player_id=@playerId and upload_type=@uploadType and master_upload_id=@masterId";
            if (currState != UGCUpload.eState.DONTCARE)
                sqlUpdate += " and state=@currState";

            Hashtable parameters = new Hashtable();
            parameters.Add("@playerId", userId);
            parameters.Add("@uploadType", (int)uploadType);
            parameters.Add("@masterId", masterId);
            parameters.Add("@finalState", (int)finalState);
            if (currState != UGCUpload.eState.DONTCARE)
                parameters.Add("@currState", (int)currState);

            int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sqlUpdate, parameters);
            return rowsAffected > 0;
        }

        /// <summary>
        /// SetUGCHashValue
        /// </summary>
        public static Boolean SetUGCFileMetadata(int userId, UGCUpload.eType uploadType, int uploadId, string originalHash, int originalSize)
        {
            string sqlUpdate = "update " + ugcUploadTable + " set hash=@originalHash, orig_size=@originalSize where player_id=@playerId and upload_type=@uploadType and upload_id=@uploadId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@playerId", userId);
            parameters.Add("@uploadType", (int)uploadType);
            parameters.Add("@uploadId", uploadId);
            parameters.Add("@originalHash", originalHash == null ? "" : originalHash);
            parameters.Add("@originalSize", originalSize);

            int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sqlUpdate, parameters);
            return rowsAffected == 1;
        }

        /// <summary>
        /// SetUGCUploadSize
        /// </summary>
        public static Boolean SetUGCUploadSize(int userId, UGCUpload.eType uploadType, int uploadId, int lengthInBytes)
        {
            string sqlUpdate = "update " + ugcUploadTable + " set bytes=@bytes where player_id=@playerId and upload_type=@uploadType and upload_id=@uploadId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@playerId", userId);
            parameters.Add("@uploadType", (int)uploadType);
            parameters.Add("@uploadId", uploadId);
            parameters.Add("@bytes", lengthInBytes);

            int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sqlUpdate, parameters);
            return rowsAffected == 1;
        }

        /// <summary>
        /// SetUGCOriginalName
        /// </summary>
        public static Boolean SetUGCOriginalName(int userId, UGCUpload.eType uploadType, int uploadId, string origName)
        {
            string sqlUpdate = "update " + ugcUploadTable + " set origName=@origName where player_id=@playerId and upload_type=@uploadType and upload_id=@uploadId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@playerId", userId);
            parameters.Add("@uploadType", (int)uploadType);
            parameters.Add("@uploadId", uploadId);
            parameters.Add("@origName", origName);

            int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sqlUpdate, parameters);
            return rowsAffected == 1;
        }

        /// <summary>
        /// SetUGCUploadName
        /// </summary>
        public static Boolean SetUGCUploadName(int userId, UGCUpload.eType uploadType, int uploadId, string newName)
        {
            string sqlUpdate = "update " + ugcUploadTable + " set uploadName=@uploadName where player_id=@playerId and upload_type=@uploadType and upload_id=@uploadId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@playerId", userId);
            parameters.Add("@uploadType", (int)uploadType);
            parameters.Add("@uploadId", uploadId);
            parameters.Add("@uploadName", newName);

            int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sqlUpdate, parameters);
            return rowsAffected == 1;
        }

        /// <summary>
        /// GetUGCUpload - return an UGC upload record
        /// </summary>
        public static DataRow GetUGCUpload(int upload_id)
        {
            string sqlSelect = "select " + ugcTblStdCols +
                               " from " + ugcUploadTable +
                               " where upload_id = @upload_id";

            Hashtable parameters = new Hashtable();
            parameters.Add("@upload_id", upload_id);

            DataRow dr = KanevaGlobals.GetDatabaseUtilityKGP().GetDataRow(sqlSelect, parameters, false);

            return dr;
        }


        /// <summary>
        /// GetUGCUpload - return an UGC upload record
        /// </summary>
        public static DataRow GetUGCUpload(int userId, UGCUpload.eType uploadType, int masterUploadId, Int64 subId)
        {
            string sqlSelect = "select " + ugcTblStdCols +
                               " from " + ugcUploadTable +
                               " where master_upload_id=@masterUploadId and sub_id=@subId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@masterUploadId", masterUploadId);
            parameters.Add("@subId", subId);

            DataRow dr = KanevaGlobals.GetDatabaseUtilityKGP().GetDataRow(sqlSelect, parameters, false);

            if (dr == null)
                return null;

            int drUserId = -1;
            int drUploadType = -1;

            try
            {
                drUserId = Convert.ToInt32(dr["player_id"]);
                drUploadType = Convert.ToInt32(dr["upload_type"]);
            }
            catch (System.Exception)
            {
                return null;
            }

            if (drUserId == userId && (uploadType == UGCUpload.eType.UNKNOWN || drUploadType == (int)uploadType))
                return dr;

            return null;
        }

        /// <summary>
        /// GetUGCUpload - return an UGC upload record
        /// </summary>
        public static DataRow GetUGCUpload(int userId, UGCUpload.eType uploadType, int uploadId)
        {
            string sqlSelect = "select " + ugcTblStdCols + " from " + ugcUploadTable + " where upload_id=@uploadId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@uploadId", uploadId);

            DataRow dr = KanevaGlobals.GetDatabaseUtilityKGP().GetDataRow(sqlSelect, parameters, false);

            if (dr == null)
                return null;

            int drUserId = -1;
            int drUploadType = -1;

            try
            {
                drUserId = Convert.ToInt32(dr["player_id"]);
                drUploadType = Convert.ToInt32(dr["upload_type"]);
            }
            catch (System.Exception)
            {
                return null;
            }

            if (drUserId == userId && (uploadType == UGCUpload.eType.UNKNOWN || drUploadType == (int)uploadType))
                return dr;

            return null;
        }

        /// <summary>
        /// GetUGCUploads - return all matching upload records
        /// </summary>
        public static DataTable GetUGCUploads(int userId, UGCUpload.eType uploadType)
        {
            List<UGCUpload.eType> uploadTypes = new List<UGCUpload.eType>();
            uploadTypes.Add(uploadType);
            return GetUGCUploads(userId, uploadTypes, -1, null, -1);
        }

        /// <summary>
        /// GetUGCUploads - return all matching upload records
        /// </summary>
        public static DataTable GetUGCUploads(int userId, UGCUpload.eType uploadType, int masterId)
        {
            List<UGCUpload.eType> uploadTypes = new List<UGCUpload.eType>();
            uploadTypes.Add(uploadType);
            return GetUGCUploads(userId, uploadTypes, masterId, null, -1);
        }

        /// <summary>
        /// GetUGCUploads - return all matching upload records
        /// </summary>
        public static DataTable GetUGCUploads(int userId, UGCUpload.eType uploadType, int masterId, IList<UGCUpload.eState> states, int uploadIdFilter)
        {
            List<UGCUpload.eType> uploadTypes = new List<UGCUpload.eType>();
            uploadTypes.Add(uploadType);
            return GetUGCUploads(userId, uploadTypes, masterId, states, uploadIdFilter);
        }

        /// <summary>
        /// GetUGCUploads - return all matching upload records
        /// </summary>
        public static DataTable GetUGCUploads(int userId, IList<UGCUpload.eType> uploadTypes, int masterId, IList<UGCUpload.eState> states, int uploadIdFilter)
        {
            string sqlSelect = "select " + ugcTblStdCols + " from " + ugcUploadTable + " where player_id=@playerId and master_upload_id=@masterId";
            if (uploadIdFilter >= 0)
                sqlSelect += " and upload_id=@uploadIdFilter";

            if (uploadTypes != null && uploadTypes.Count > 0)   // Use IN instead of find_in_set for performance
            {
                sqlSelect += " and upload_type IN (";
                for (int i = 0; i < uploadTypes.Count; i++)
                {
                    if (i > 0)
                    {
                        sqlSelect += ",";
                    }
                    sqlSelect += Convert.ToString((int)uploadTypes[i]);
                }
                sqlSelect += ")";
            }

            if (states != null && states.Count > 0)   // Use IN instead of find_in_set for performance
            {
                sqlSelect += " and state IN (";
                for (int i = 0; i < states.Count; i++)
                {
                    if (i > 0)
                    {
                        sqlSelect += ",";
                    }
                    sqlSelect += Convert.ToString((int)states[i]);
                }
                sqlSelect += ")";
            }

            Hashtable parameters = new Hashtable();
            parameters.Add("@playerId", userId);
            parameters.Add("@masterId", masterId);
            if (uploadIdFilter >= 0)
            {
                parameters.Add("@uploadIdFilter", uploadIdFilter);
            }

            return KanevaGlobals.GetDatabaseUtilityKGP().GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// PurgeUGCUpdateRecords - purge expired/completed UGC upload records
        /// </summary>
        public static int PurgeUGCUpdateRecords(int secondsSinceCreation, bool bPurgeWaiting, int userId, UGCUpload.eType uploadType)
        {
            string sqlDelete = "delete from " + ugcUploadTable + " where timestampdiff(SECOND, creationTime, now()) > @secondsSinceCreation";
            if (userId != -1)
                sqlDelete = sqlDelete + " and player_id=@playerId";
            if (!bPurgeWaiting)
                sqlDelete = sqlDelete + " and state<>@state";
            if (uploadType != UGCUpload.eType.UNKNOWN)
                sqlDelete = sqlDelete + " and upload_type=@uploadType";

            Hashtable parameters = new Hashtable();
            parameters.Add("@secondsSinceCreation", secondsSinceCreation);
            if (userId != -1)
                parameters.Add("@playerId", userId);
            if (!bPurgeWaiting)
                parameters.Add("@state", (int)UGCUpload.eState.WAITING);
            if (uploadType != UGCUpload.eType.UNKNOWN)
                parameters.Add("@uploadType", (int)uploadType);

            int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sqlDelete, parameters);
            return rowsAffected;
        }

        /// <summary>
        /// PurgeUGCUpdateRecords - purge expired/completed UGC upload records
        /// </summary>
        public static void PurgeUGCUpdateRecords(int secondsSinceCreation, UGCUpload.eType uploadType, bool bPurgeWaiting)
        {
            PurgeUGCUpdateRecords(secondsSinceCreation, bPurgeWaiting, -1, uploadType);   // Purge for all users
        }


        /// <summary>
        /// InsertValidationRequest
        /// </summary>
        private static Boolean DoInsertValidationRequest(int userId, UGCUpload.eUsage ugcUsage, Dictionary<int, string> properties, ref int requestId)
        {
            Hashtable parameters = new Hashtable();

            string sqlInsert = "INSERT INTO ugc_validation_requests(kaneva_user_id, ugc_usage_id, request_time) "
                             + "VALUES(@userId, @ugcUsage, now())";

            parameters.Clear();
            parameters.Add("@userId", userId);
            parameters.Add("@ugcUsage", (int)ugcUsage);

            int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteIdentityInsert(sqlInsert, parameters, ref requestId);
            if (requestId == -1)
            {
                return false;
            }

            string sqlInsertDetail = "INSERT INTO ugc_validation_request_details(request_id, property_level_id, property_id, property_path, property_value) " +
                                     "VALUES(@requestId, @propLevel, @propId, @propPath, @propValue)";

            foreach (KeyValuePair<int, string> kvp in properties)
            {
                int propId = kvp.Key;
                int propLevel = propId >> 8;
                propId &= 0xFF;

                string propValue = kvp.Value;

                parameters.Clear();
                parameters.Add("@requestId", requestId);
                parameters.Add("@propLevel", propLevel);
                parameters.Add("@propId", propId);
                parameters.Add("@propPath", "");    //@@temp
                parameters.Add("@propValue", propValue);

                rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sqlInsertDetail, parameters);
                if (rowsAffected != 1)
                {
                    //@@Todo: cleanup
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// ValidateUGCDynamicObjectProperties
        /// </summary>
        private static Boolean DoValidateUGCProperties(int userId, uint roleValue, int masterUploadId, UInt32 subId, UGCUpload.eUsage ugcUsage, Dictionary<int, string> properties, ref int requestId, ref string msg, ref string msgDetail, ref bool rulesFound, ref float basePrice)
        {
            if (!DoInsertValidationRequest(userId, ugcUsage, properties, ref requestId))
            {
                return false;
            }

            Hashtable parameters = new Hashtable();

            string sqlValidate = "CALL validate_ugc_request(@requestId, @roleValue, @returnCode, @returnMsg, @returnMsgDetail, @rulesFound, @price); SELECT CAST(@returnCode as UNSIGNED INT) as returnCode, @returnMsg, @returnMsgDetail, CAST(@rulesFound as UNSIGNED INT) as rulesFound, CAST(@price as UNSIGNED INT) as price; ";
            parameters.Clear();
            parameters.Add("@requestId", requestId);
            parameters.Add("@roleValue", roleValue);
            DataRow dr = KanevaGlobals.GetDatabaseUtilityKGP().GetDataRow(sqlValidate, parameters, false);
            if (dr == null)
            {
                //@@Todo: report error
                return false;
            }

            try
            {
                rulesFound = Convert.ToInt32(dr["rulesFound"]) != 0;
                if (!rulesFound)
                    return false;

                int returnCode = Convert.ToInt32(dr["returnCode"]);
                msg = Convert.ToString(dr["@returnMsg"]);
                msgDetail = Convert.ToString(dr["@returnMsgDetail"]);
                basePrice = (float)Convert.ToDouble(dr["price"]);
                return returnCode == 0;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public static IDictionary<int, string> GetValidationRequestDetails(int reqId)
        {
            string sqlSelect = "select property_level_id, property_id, property_value from ugc_validation_request_details where request_id=@reqId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@reqId", reqId);

            DataTable dt = KanevaGlobals.GetDatabaseUtilityKGP().GetDataTable(sqlSelect, parameters);
            if (dt == null || dt.Rows.Count == 0)
                return null;

            Dictionary<int, string> valuePairs = new Dictionary<int, string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                int propLevelId, propId;
                try
                {
                    propLevelId = Convert.ToInt32(dr["property_level_id"]);
                    propId = Convert.ToInt32(dr["property_id"]);
                    int fullPropId = (propLevelId << 8) + propId;
                    valuePairs.Add(fullPropId, Convert.ToString(dr["property_value"]));
                }
                catch (System.Exception) { }
            }

            return valuePairs;
        }

        /// <summary>
        /// IsItemDerivable - return true is an item is derivable
        /// </summary>
        public static Boolean IsItemDerivable(int globalId)
        {
            string sqlSelect = "select is_derivable from items where global_id=@global_id";

            Hashtable parameters = new Hashtable();
            parameters.Add("@global_id", globalId);

            DataRow dr = KanevaGlobals.GetDatabaseUtilityKGP().GetDataRow(sqlSelect, parameters, false);
            if (dr == null)
                return false;

            int isDerivable = Convert.ToInt32(dr["is_derivable"]);
            return isDerivable != 0;
        }

        /// <summary>
        /// GetDerivableTemplateInfo - return template info for derivation
        /// </summary>
        public static Boolean GetDerivableTemplateInfo(int globalId, ref string templateName, ref string creatorName, ref Int64[] subIds, ref float basePrice)
        {
            string sqlSelect = "select itm.name as templateName, itm.market_cost, tmp.sub_id, ifnull(usr.username, 'Kaneva') as creatorName " +
                               "from items itm " +
                                    "left outer join item_templates tmp on itm.global_id=tmp.global_id " +
                                    "left outer join kaneva.users usr on itm.item_creator_id=usr.user_id " +
                               "where itm.global_id=@globalId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@globalId", globalId);

            basePrice = 999999;
            subIds = null;

            DataTable dt = KanevaGlobals.GetDatabaseUtilityKGP().GetDataTable(sqlSelect, parameters);
            if (dt == null || dt.Rows.Count == 0)
                return false;

            if (dt.Rows[0]["sub_id"] == null)
                return false;

            try
            {
                basePrice = (float)Convert.ToDouble(dt.Rows[0]["market_cost"]);
                templateName = Convert.ToString(dt.Rows[0]["templateName"]);
                creatorName = Convert.ToString(dt.Rows[0]["creatorName"]);
            }
            catch (System.Exception) { }

            subIds = new Int64[dt.Rows.Count];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                try
                {
                    subIds[i] = Convert.ToInt64(dt.Rows[i]["sub_id"]);
                }
                catch (System.Exception)
                {
                    subIds[i] = -1;
                }
            }

            return true;
        }

        /// <summary>
        /// InsertItemTemplate
        /// </summary>
        public static Boolean InsertItemTemplate(int globalId, Int64 subId, string templatePath, int width, int height)
        {
            string sqlInsert = "insert into item_templates(global_id, sub_id, template_path, width, height) " +
                               "values( @globalId, @subId, @templatePath, @width, @height )";

            Hashtable parameters = new Hashtable();
            parameters.Add("@globalId", globalId);
            parameters.Add("@subId", subId);
            parameters.Add("@templatePath", templatePath);
            parameters.Add("@width", width);
            parameters.Add("@height", height);

            int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sqlInsert, parameters);
            return rowsAffected == 1;
        }

        /// <summary>
        /// InsertDynamicObjectHeader - insert a record into items_dynamic_objects table
        /// </summary>
        public static Boolean InsertDynamicObjectHeader(int globalId, bool isAttachable, bool isInteractive, bool canPlayFlash, bool hasCollision, float minX, float minY, float minZ, float maxX, float maxY, float maxZ)
        {
            string sqlInsert = "insert into " + dynObjHeaderTable
                + " (global_id, attachable, interactive, playFlash, collision, min_x, min_y, min_z, max_x, max_y, max_z)"
                + " values (@globalId, @attachable, @interactive, @playFlash, @collision, @minX, @minY, @minZ, @maxX, @maxY, @maxZ)";

            Hashtable parameters = new Hashtable();
            parameters.Add("@globalId", globalId);
            parameters.Add("@attachable", isAttachable ? 1 : 0);
            parameters.Add("@interactive", isInteractive ? 1 : 0);
            parameters.Add("@playFlash", canPlayFlash ? 1 : 0);
            parameters.Add("@collision", hasCollision ? 1 : 0);
            parameters.Add("@minX", minX);
            parameters.Add("@minY", minY);
            parameters.Add("@minZ", minZ);
            parameters.Add("@maxX", maxX);
            parameters.Add("@maxY", maxY);
            parameters.Add("@maxZ", maxZ);

            int rowsAffected = KanevaGlobals.GetDatabaseUtilityKGP().ExecuteNonQuery(sqlInsert, parameters);
            return rowsAffected == 1;
        }

        #endregion

        #region UploadMemberDesigns

        public static bool FinalizeUGCUploadToShop(User user, UGCUpload uploadItem, bool isDerivable,
            string itemName, string itemDesc, string itemKW, string userHostAddress,
            int category, int itemPassType, WOKItem.ItemActiveStates itemStatus, uint designerPrice,
            string userSelectedCurrency, bool blastUpload, out string errorMsg)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            errorMsg = string.Empty;
            UploadFeeCalculator uploadFeeCalculator = new UploadFeeCalculator(Configuration.UGC_UploadFee_BasePriceFactor,
                                                           Configuration.UGC_UploadFee_BaseCommissionFactor,
                                                           Configuration.UGC_UploadFeeBase,
                                                           Configuration.UGC_UploadFeeFactor,
                                                           Configuration.CatalogChargeFactor,
                                                           Configuration.UGC_UploadFeeMin,
                                                           Configuration.UGC_UploadFeeMax);

            #region Validation

            if (uploadItem == null)
            {
                errorMsg = "Unable to proceed: unrecognized upload ID. Please resubmit your design from Kaneva 3D Client.";
                return false;
            }

            if (string.IsNullOrWhiteSpace(itemName))
            {
                errorMsg = "Please enter a valid item name";
                return false;
            }

            if (string.IsNullOrWhiteSpace(itemDesc))
            {
                errorMsg = "Please enter a valid description";
                return false;
            }

            if (category <= 0 && !uploadItem.IsDOAnimation)
            {
                errorMsg = "Please select your category";
                return false;
            }
            if (category > 0)
            {
                List<ItemCategory> categoryList = GetValidCategoryListForUpload(uploadItem.ID);
                if (!categoryList.Exists(c => c.ItemCategoryId == category))
                {
                    errorMsg = "Invalid category.";
                    return false;
                }
            }

            // determine the pass type
            if (itemPassType < 0)
            {
                errorMsg = "Please select your pass type";
                return false;
            }

            // Cap designer price.
            if (designerPrice > 5000000)
            {
                designerPrice = 5000000;
            }
            #endregion

            // Calculate upload fee
            int uploadFee = uploadFeeCalculator.getUploadFee((int)uploadItem.Type, uploadItem.BasePrice, uploadItem.BaseCommission, uploadItem.IsDerivation);

            string txnCurrencySetting = GetUploadFeeCurrency((int)uploadItem.Type, uploadItem.IsDerivation);
            string txnCurrencyValue = GetUploadFeeCurrencyValue(txnCurrencySetting, userSelectedCurrency);

            // Check user balance
            if (!HasEnoughCredit(user.UserId, uploadFee, txnCurrencyValue))
            {
                errorMsg = "Not enough credits to create this item, required: " + uploadFee;
                return false;
            }

            #region Create WOK Item

            // If this is a Dyn. Obj. Animation then there is no category
            category = uploadItem.IsDOAnimation ? 0 : category;

            WOKItem item, baseItem;

            if (!UGCUtility.CreateAndDeployUGCItem(user.UserId, uploadItem,
                    itemName, itemDesc, itemKW, category, itemStatus, designerPrice, itemPassType, isDerivable,
                    out item, out baseItem))
            {
                errorMsg = "Item creation failed";
                return false;
            }

            #endregion


            // Handle textures

            // Collect texture metadata
            IDictionary<int, string> properties = UGCUtility.GetValidationRequestDetails(uploadItem.ValidationReqID);
            IDictionary<Int64, ItemPathTexture> textureMetadata = ParseTextureProperties(item.GlobalId, properties);

            // If item is not a derivative and is derivable, create template record
            bool writeItemTemplateRecord = !uploadItem.IsDerivation && baseItem.IsDerivable != 0;

            // Deploy textures
            UGCUpload.eType textureUploadType;
            if (UGCUtility.DeployUGCItemTextures(uploadItem.ID, uploadItem.Type, user.UserId, item.GlobalId, textureMetadata, writeItemTemplateRecord, baseItem.GlobalId, out textureUploadType))
            {
                UGCUtility.CompleteUGCUploadByMasterId(user.UserId, textureUploadType, uploadItem.ID, item.GlobalId, UGCUpload.eState.UPLOADED);
            }

            // Handle icons
            if (UGCUtility.DeployUGCItemIcons(uploadItem.ID, user.UserId, item.GlobalId, item.UseType))
            {
                UGCUtility.CompleteUGCUploadByMasterId(user.UserId, UGCUpload.eType.ICON, uploadItem.ID, item.GlobalId, UGCUpload.eState.UPLOADED);
            }

            UGCUtility.CompleteUGCUpload(user.UserId, uploadItem.Type, uploadItem.ID, item.GlobalId, uploadItem.IsDerivation ? UGCUpload.eState.DERIVATION : UGCUpload.eState.UPLOADED);

            // Award creator fame
            FameFacade famefaceade = new FameFacade();
            famefaceade.RedeemPacket(user.UserId, 5006, (int)FameTypes.Creator);

            // Item creation completed, deduct upload fee from user's account and add one copy of this item to user's inventory
            if (uploadItem.IsDerivation ? Configuration.UGC_AutoPurchaseUploadedDerivativeItem : Configuration.UGC_AutoPurchaseUploadedItem)
            {
                UInt32 commissionPayable = 0;
                if (baseItem != null)
                    commissionPayable = uploadFeeCalculator.getCommissionPayable(uploadItem.BasePrice, baseItem.DesignerPrice, uploadItem.IsDerivation);
                if (!PurchaseUploadedItem(user.UserId, item.GlobalId, uploadFee, uploadItem.IsDerivation, commissionPayable, (int)uploadItem.Type, userSelectedCurrency,
                    userHostAddress, out errorMsg))
                {
                    // Purchase failed, do not continue (Error message will be shown)
                    return false;
                }
            }

            // Blast if they wanted to Blast
            if (blastUpload)
            {
                BlastItemUpload(item.GlobalId, user);
            }

            return true;
        }

        private static void BlastItemUpload(int itemGlobalId, User user)
        {
            // getting a new instance of the item so all the fields are updated.
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            WOKItem item = shoppingFacade.GetItem(itemGlobalId);

            m_logger.Debug("Blasting Item");
            try
            {
                // Metrics tracking for accepts
                UserFacade userFacade = new UserFacade();
                string requestId = userFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_SHOP_MEMBER_DESIGN_UPLOAD, user.UserId, 0, 0);
                string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_BLAST, 0);

                string url = "http://" + KanevaGlobals.ShoppingSiteName + "/ItemDetails.aspx?gId=" + item.GlobalId + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentIdBlast;
                string itemThumbnail = StoreUtility.GetItemImageURL(item.ThumbnailLargePath, "la");

                BlastFacade blastFacade = new BlastFacade();

                blastFacade.SendShopUploadItemBlast(user.UserId, user.Username, user.NameNoSpaces,
                    item.DisplayName, item.Description, url, itemThumbnail, "", user.ThumbnailSmallPath);

            }
            catch (Exception ex)
            {
                m_logger.Error("Blasting Item upload Failed. Item:" + item.GlobalId + " UserId: " + user.UserId + " " + ex.ToString());
            }
        }

        private static bool PurchaseUploadedItem(int userId, int globalId, int price, bool isDerivative, UInt32 commissionPayableToTemplateDesigner,
            int uploadType, string userSelectedCurrency, string userHostAddress, out string errorMsg)
        {
            errorMsg = string.Empty;
            UserFacade userFacade = new UserFacade();
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            WOKItem item = shoppingFacade.GetItem(globalId);
            int successfulPayment = -1;
            int quantity = 1;
            int iOrderTotal = price;

            // Per animesh -2.      
            // Currently you can only use the item uploaded. You can?t have multiple copies of it.  
            // We want to change this so for an item uploaded, you can have multiple copies of it that is configurable. In other words copy = 1 is the behavior currently.
            quantity = Configuration.UGC_UploadQuantity.GetValueInt(uploadType, 1);

            string txnCurrencySetting = GetUploadFeeCurrency(uploadType, isDerivative);
            string txnCurrencyValue = GetUploadFeeCurrencyValue(txnCurrencySetting, userSelectedCurrency);

            // Check based on currency type
            // Does the user have enough credits?
            if (!HasEnoughCredit(userId, iOrderTotal, txnCurrencyValue))
            {
                errorMsg = "Not enough credits to create this item!";
                return false;
            }

            int inventoryType = GetAutoPurchaseInventoryType(uploadType, isDerivative);
            if (inventoryType == -1)
            {
                // Inventory type not provided in web options table -- determine it by currency type
                if (txnCurrencyValue == Constants.CURR_REWARDS)
                    inventoryType = 512;    // Purchase made by rewards
                else if (txnCurrencyValue == Constants.CURR_CREDITS)
                    inventoryType = 256;    // Purchase made by credits
                else // Log a warning for unknown currency type
                {
                    m_logger.Warn("Don't know how to handle currency: " + txnCurrencyValue + " for upload fee. Set inventory type to 256 anyway.");
                    inventoryType = 256;    // Purchase made by any other currencies
                }
            }

            try
            {
                successfulPayment = userFacade.AdjustUserBalanceWithDetails(userId, txnCurrencyValue, -Convert.ToDouble(iOrderTotal), Constants.CASH_TT_BOUGHT_ITEM_ON_WEB, globalId, quantity);
            }
            catch (Exception)
            {
                errorMsg = "Not enough credits to create this item.";
                return false;
            }

            if (successfulPayment == 0)  //0 is success according to AdjustUserBalance notes
            {
                // Give them the item
                userFacade.AddItemToPendingInventory(userId, "P", globalId, quantity, inventoryType);

                // Add it to history
                shoppingFacade.AddItemPurchase(globalId, (int)ItemPurchase.eItemTxnType.PURCHASE, userId, item.ItemCreatorId, iOrderTotal, 0, userHostAddress, txnCurrencyValue, quantity, 0);

                // Credit original designer if derivation
                UInt32 iTemplateCreatorId = 0;
                UInt32 iTemplateCreatorsComission;
                if (isDerivative && commissionPayableToTemplateDesigner > 0 && shoppingFacade.GetUGCTemplateInfo(item, out iTemplateCreatorId, out iTemplateCreatorsComission))
                {
                    // Clamp commission pay-out so it won't go above the designated commission amount by template designer
                    if (commissionPayableToTemplateDesigner > iTemplateCreatorsComission)
                        commissionPayableToTemplateDesigner = iTemplateCreatorsComission;

                    // credit original designer
                    // Per Jeff P, only do this for Credits
                    if (txnCurrencyValue == Constants.CURR_CREDITS)
                    {
                        userFacade.AdjustUserBalance(Convert.ToInt32(iTemplateCreatorId), txnCurrencyValue, Convert.ToDouble(commissionPayableToTemplateDesigner), Constants.CASH_TT_ROYALITY);
                    }

                    // Get base item global ID (the first derivative from the same template created by template designer)
                    int baseItemGlid = shoppingFacade.GetBaseItemGlobalId(globalId);

                    // Add template commission transaction to shopping history
                    shoppingFacade.AddItemPurchase(baseItemGlid, (int)ItemPurchase.eItemTxnType.COMMISSION, userId, iTemplateCreatorId, 0, commissionPayableToTemplateDesigner, userHostAddress, txnCurrencyValue, quantity, 0);
                }
                return true;
            }

            return false;
        }

        private static bool HasEnoughCredit(int userId, int price, string txnCurrency)
        {
            UserBalances userBalances = (new UserFacade()).GetUserBalances(userId);
            UInt64 iCredits = 0;

            // Does the user have enough credits?
            if (txnCurrency.Equals(Constants.CURR_REWARDS))
            {
                iCredits = Convert.ToUInt64(userBalances.Rewards);
            }
            else if (txnCurrency.Equals(Constants.CURR_CREDITS))
            {
                iCredits = Convert.ToUInt64(userBalances.Credits);
            }
            else
            {
                iCredits = Convert.ToUInt64(userBalances.Credits + userBalances.Rewards);
            }

            if ((iCredits < Convert.ToUInt64(price)))
                return false;

            return true;
        }

        public static string GetUploadFeeCurrency(int uploadType, bool bDerive)
        {
            if (bDerive)
            {
                return Configuration.UGC_DerivativeUploadFeeCurrency.GetValueStr(uploadType, null);
            }
            else
            {
                return Configuration.UGC_DerivativeUploadFeeCurrency.GetValueStr(uploadType, null);
            }
        }

        public static string GetUploadFeeCurrencyValue(string txnCurrency, string userSelectedCurrency)
        {
            if (txnCurrency == Constants.CURR_USERSELECTED)
            {
                // User selected is allowable, set based on thier choice
                if (userSelectedCurrency == Constants.CURR_REWARDS)
                {
                    return Constants.CURR_REWARDS;
                }
                else
                {
                    return Constants.CURR_CREDITS;
                }
            }

            return txnCurrency;
        }

        private static int GetAutoPurchaseInventoryType(int uploadType, bool bDerive)
        {
            if (bDerive)
                return Configuration.UGC_AutoPurchasedDerivativeItemInvType.GetValueInt(uploadType, -1);
            else
                return Configuration.UGC_AutoPurchasedDerivativeItemInvType.GetValueInt(uploadType, -1);
        }

        private static IDictionary<Int64, ItemPathTexture> ParseTextureProperties(int globalId, IDictionary<int, string> properties)
        {
            IDictionary<Int64, ItemPathTexture> result = new Dictionary<Int64, ItemPathTexture>();

            if (properties.ContainsKey(UGCProperty.PROP_TEXTURE_CNT))
            {
                try
                {
                    int numTex = Convert.ToInt32(properties[UGCProperty.PROP_TEXTURE_CNT]);
                    for (int i = 0; i < numTex; i++)
                    {
                        int propIndex = i << 16;
                        Int64 ordinal = -1;

                        if (properties.ContainsKey(propIndex + UGCProperty.PROP_IMAGE_ID))
                        {
                            try
                            {
                                ordinal = Convert.ToInt64(properties[propIndex + UGCProperty.PROP_IMAGE_ID]);
                            }
                            catch (Exception ex)
                            {
                                m_logger.Error("ParseTextureProperties(" + globalId + ") [" + i + "]: error parsing texture ID: " + ex);
                            }
                        }
                        if (ordinal >= 0)
                        {
                            UInt32 width = 0, height = 0, averageColor = 0;
                            ItemPathTexture.ImageOpacity opacity = ItemPathTexture.ImageOpacity.FULLY_OPAQUE;

                            if (properties.ContainsKey(propIndex + UGCProperty.PROP_IMAGE_WIDTH))
                            {
                                try
                                {
                                    width = Convert.ToUInt32(properties[propIndex + UGCProperty.PROP_IMAGE_WIDTH]);
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Warn("ParseTextureProperties(" + globalId + ") [" + i + "]: error parsing texture width: " + ex);
                                }
                            }
                            if (properties.ContainsKey(propIndex + UGCProperty.PROP_IMAGE_HEIGHT))
                            {
                                try
                                {
                                    height = Convert.ToUInt32(properties[propIndex + UGCProperty.PROP_IMAGE_HEIGHT]);
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Warn("ParseTextureProperties(" + globalId + ") [" + i + "]: error parsing texture height: " + ex);
                                }
                            }
                            if (properties.ContainsKey(propIndex + UGCProperty.PROP_IMAGE_OPACITY))
                            {
                                try
                                {
                                    opacity = (ItemPathTexture.ImageOpacity)Convert.ToInt32(properties[propIndex + UGCProperty.PROP_IMAGE_OPACITY]);
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Warn("ParseTextureProperties(" + globalId + ") [" + i + "]: error parsing texture opacity: " + ex);
                                }
                            }
                            if (properties.ContainsKey(propIndex + UGCProperty.PROP_IMAGE_AVG_COLOR))
                            {
                                try
                                {
                                    averageColor = Convert.ToUInt32(properties[propIndex + UGCProperty.PROP_IMAGE_AVG_COLOR]);
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Warn("ParseTextureProperties(" + globalId + ") [" + i + "]: error parsing texture color: " + ex);
                                }
                            }

                            result.Add(ordinal, new ItemPathTexture(globalId, ordinal, width, height, opacity, averageColor));
                        }
                    }
                }
                catch (Exception ex)
                {
                    m_logger.Error("ParseTextureProperties(" + globalId + "): error parsing texture count: " + ex);
                }
            }

            return result;
        }

        public static List<ItemCategory> GetValidCategoryListForUpload(int uploadId)
        {
            int actorType = 0;
            uint doGLID = 0;
            UGCUpload.eType uploadType = UGCUpload.eType.UNKNOWN;
            List<ItemCategory> lItemCategory = new List<ItemCategory>();

            try
            {
                DataRow dr = UGCUtility.GetUGCUpload(uploadId);
                uploadType = (UGCUpload.eType)Convert.ToUInt32(dr["upload_type"]);

                IDictionary<int, string> validationProps = UGCUtility.GetValidationRequestDetails(Convert.ToInt32(dr["validation_request_id"]));
                if (validationProps != null)
                {
                    // Get the actor type
                    if (validationProps.ContainsKey(UGCProperty.PROP_TARGET_ACTOR_TYPE))
                    {
                        string sActorType = validationProps[UGCProperty.PROP_TARGET_ACTOR_TYPE];

                        try
                        {
                            actorType = Convert.ToInt32(sActorType);
                        }
                        catch (System.Exception) { }

                        // actor type validation
                        if (actorType != 1 && actorType != 2)
                            actorType = 0;
                    }

                    // Check to see if upload is an animation
                    if (uploadType == UGCUpload.eType.ANM_GZ)
                    {
                        // Get the Target Actor GLID which is the GLID of the dynamic object the animation is asigned to
                        if (validationProps.ContainsKey(UGCProperty.PROP_TARGET_ACTOR_GLID))
                        {
                            string sDO_GLID = validationProps[UGCProperty.PROP_TARGET_ACTOR_GLID];

                            try
                            {
                                doGLID = Convert.ToUInt32(sDO_GLID);
                            }
                            catch (System.Exception) { }
                        }
                    }
                }
            }
            catch { }

            // What cats we show is based on the parent cat with the current upload type.
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            ItemCategory parentCategory;
            parentCategory = shoppingFacade.GetItemCategoryParentByUploadType(Convert.ToInt16(uploadType));

            if (uploadType == UGCUpload.eType.SND_OGG_KRX)
            {
                lItemCategory = shoppingFacade.GetItemCategoriesByParentCategoryId(parentCategory.ItemCategoryId);
            }
            else if (uploadType == UGCUpload.eType.ANM_GZ || uploadType == UGCUpload.eType.EQP_GZ)
            {
                lItemCategory = shoppingFacade.GetItemCatByUploadTypeActorGroup((uint)uploadType, actorType);
            }
            else
            {
                DataTable dtItemCategories = shoppingFacade.GetItemCatsByUploadType((uint)uploadType);
                if (dtItemCategories != null && dtItemCategories.Rows.Count > 0)
                {
                    lItemCategory = dtItemCategories.AsEnumerable().Select(row => new ItemCategory
                    {
                        Name = row["listName"].ToString(),
                        ItemCategoryId = Convert.ToUInt32(row["item_category_id"])
                    }).ToList();
                }
            }

            return lItemCategory;
        }

        public static uint GetTargetActorGlid(UGCUpload uploadItem)
        {
            uint actorGlid = 0;

            IDictionary<int, string> validationProps = UGCUtility.GetValidationRequestDetails(uploadItem.ValidationReqID);
            if (validationProps != null &&
                uploadItem.Type == UGCUpload.eType.ANM_GZ &&
                validationProps.ContainsKey(UGCProperty.PROP_TARGET_ACTOR_GLID))
            {
                uint.TryParse(validationProps[UGCProperty.PROP_TARGET_ACTOR_GLID] ?? "0", out actorGlid);
            }

            return actorGlid;
        }

        public class UploadFeeCalculator
        {
            private float _basePriceFactor;
            private float _baseCommissionFactor;
            private int _baseCharge;
            private float _finalFactor;
            private float _catalogChargeFactor;
            private OptionValueMap _min;
            private OptionValueMap _max;

            public UploadFeeCalculator(float bpFactor, float bcFactor, int baseCharge, float factor, float catalogChargeFactor, OptionValueMap min, OptionValueMap max)
            {
                _basePriceFactor = bpFactor;
                _baseCommissionFactor = bcFactor;
                _baseCharge = baseCharge;
                _finalFactor = factor;
                _catalogChargeFactor = catalogChargeFactor;
                _min = min;
                _max = max;
            }

            public int getUploadFee(int uploadType, int basePrice, UInt32 baseCommission, bool isDerivative)
            {
                if (isDerivative)
                    return Convert.ToInt32((basePrice + getCommissionPayable(basePrice, baseCommission, isDerivative)) * (1 + _catalogChargeFactor));    // Charge full item amount for derivatives per JP's request

                // For mesh uploads, upload fee is calculate by following formula (with possible discounts)
                return clampByMinMax(uploadType, Convert.ToInt32((basePrice * _basePriceFactor + _baseCharge) * _finalFactor + getCommissionPayable(basePrice, baseCommission, isDerivative)), isDerivative);
            }

            public UInt32 getCommissionPayable(int basePrice, UInt32 baseCommission, bool isDerivative)
            {
                if (isDerivative)
                    return baseCommission;  // Charge full template commission (and credit template designer the same amount) per JP's request

                return 0;   // No template commission pay-out if non-derivatives
            }

            protected int clampByMinMax(int uploadType, int fee, bool isDerivative)
            {
                int min = _min.GetValueInt(uploadType, 0);
                int max = _max.GetValueInt(uploadType, Int32.MaxValue);

                if (fee < min)
                    fee = min;
                if (fee > max)
                    fee = max;

                return fee;
            }
        }

        #endregion

        #region Helper Functions

        /// <summary>
        /// GetUsageTypeByUploadType
        /// </summary>
        public static UGCUpload.eUsage GetUsageTypeByUploadType(UGCUpload.eType uploadType)
        {
            if (UGCUsageMap.ContainsKey(uploadType))
            {
                return UGCUsageMap[uploadType];
            }

            return UGCUpload.eUsage.UNKNOWN;
        }

        /// <summary>
        /// GetDeployFilestore
        /// </summary>
        public static string GetDeployFilestore(ItemPathType pathTypeInfo)
        {
            string pathType = pathTypeInfo.Type;
            if (ItemPathType.IsJPGPathType(pathType))
            {
                pathType = ItemPathType.GetDDSPathTypeByJPGPathType(pathType);
            }

            if (DeployFilestoreMap.ContainsKey(pathType))
            {
                return DeployFilestoreMap[pathType];
            }

            return KanevaGlobals.TextureFilestore;
        }

        /// <summary>
        /// GetDeployPrefix
        /// </summary>
        public static string GetDeployPrefix(ItemPathType pathTypeInfo)
        {
            string pathType = pathTypeInfo.Type;
            if (DeployFileNamePrefixMap.ContainsKey(pathType))
            {
                return DeployFileNamePrefixMap[pathType];
            }

            return "";
        }

        /// <summary>
        /// GetDeployExtension
        /// </summary>
        public static string GetDeployExtension(ItemPathType pathTypeInfo)
        {
            string pathType = pathTypeInfo.Type;
            switch (pathType)
            {
                case ItemPathType.TYPE_MESH:
                case ItemPathType.TYPE_PARTICLE:
                case ItemPathType.TYPE_ANIM:
                case ItemPathType.TYPE_SOUND:
                case ItemPathType.TYPE_EQUIP:
                case ItemPathType.TYPE_ITEM_TEX_DDS:
                    if (!DeployExtensionMap.ContainsKey(pathTypeInfo.FileFormat))
                    {
                        throw new ArgumentException("Unable to determine file name extension for " + pathType + ", file format: '" + pathTypeInfo.FileFormat + "'", "pathTypeInfo");
                    }
                    return pathTypeInfo.AssetVersion.ToString(DeployExtensionMap[pathTypeInfo.FileFormat]);

                default:
                    return DEFAULT_EXT;
            }
        }

        public static bool IsDeployRootPathConfigured(string pathType)
        {
            if (ItemPathType.IsJPGPathType(pathType))
            {
                pathType = ItemPathType.GetDDSPathTypeByJPGPathType(pathType);
            }

            return DeployRootPathMap.ContainsKey(pathType) || DeployRootPathFuncs.ContainsKey(pathType);
        }

        public static string GetDeployRootPath(string pathType, string relPath)
        {
            if (ItemPathType.IsJPGPathType(pathType))
            {
                pathType = ItemPathType.GetDDSPathTypeByJPGPathType(pathType);
            }

            if (DeployRootPathMap.ContainsKey(pathType))
            {
                return DeployRootPathMap[pathType];
            }

            if (DeployRootPathFuncs.ContainsKey(pathType))
            {
                return DeployRootPathFuncs[pathType](relPath);
            }

            return null;
        }

        public static string GetItemPathTypeByUploadType(UGCUpload.eType uploadType)
        {
            if (DeployPathTypeMap.ContainsKey(uploadType))
            {
                return DeployPathTypeMap[uploadType];
            }

            return null;
        }

        private static string GetDeployRelativePathUnique(uint uniqueAssetId, ItemPathType pathTypeInfo)
        {
            // Path in unique asset store
            return pathTypeInfo.getAssetPathUnique(uniqueAssetId);
        }

        private static string GetDeployRelativePathByItem(int userId, int globalId, ItemPathType pathTypeInfo)
        {
            // Path in per-item asset store
            return userId.ToString() + "/" + GetDeployPrefix(pathTypeInfo) + globalId.ToString("0000000000") + GetDeployExtension(pathTypeInfo);
        }

        /// <summary>
        /// GetSubUploadType - return sub upload type based on master upload type (for derivation)
        /// </summary>
        private static UGCUpload.eType GetSubUploadType(UGCUpload.eType masterUploadType)
        {
            switch (masterUploadType)
            {
                case UGCUpload.eType.DOB_GZ:
                    return UGCUpload.eType.TEX_DDS_GZ;
            }

            return UGCUpload.eType.UNKNOWN;
        }

        /// <summary>
        /// GetDefaultPreviewImageUrl
        /// </summary>
        public static string GetDefaultPreviewImageUrl()
        {
            return KanevaGlobals.TextureServer + "/" + KanevaGlobals.TextureFilestore + defaultImageName;
        }

        /// <summary>
        /// GetDefaultPreviewSoundImageUrl
        /// </summary>
        public static string GetDefaultPreviewSoundImageUrl()
        {
            return KanevaGlobals.TextureServer + "/" + KanevaGlobals.TextureFilestore + defaultSoundImageName;
        }

        /// <summary>
        /// GetDefaultSoundPreviewImagePath
        /// </summary>
        public static string GetDefaultSoundPreviewImagePath()
        {
            return KanevaGlobals.TexturePath + "\\" + defaultSoundImageName;
        }

        /// </summary>
        public static string GetDefaultParticlePreviewImagePath()
        {
            return KanevaGlobals.TexturePath + "\\KanevaIconParticleEffect.jpg";
        }

        /// <summary>
        /// GetDefaultPreviewImageSize
        /// </summary>
        public static void GetDefaultPreviewImageSize(ref int width, ref int height)
        {
            if (defPreviewWidth == 0 || defPreviewHeight == 0)
            {
                string defaultImagePath = KanevaGlobals.TexturePath + "/" + defaultImageName;

                System.Drawing.Image defaultImg = null;
                try
                {
                    // Get image size information
                    if (Configuration.UseAmazonS3Storage)
                    {
                        string originalImage = (Configuration.ImageServer + "/" + defaultImagePath).Replace("\\", "/");

                        HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(originalImage);
                        HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                        System.IO.Stream receiveStream = httpWebResponse.GetResponseStream();
                        defaultImg = System.Drawing.Image.FromStream(receiveStream);
                    }
                    else
                    {
                        defaultImg = System.Drawing.Image.FromFile(defaultImagePath);
                    }

                    defPreviewWidth = defaultImg.Width;
                    defPreviewHeight = defaultImg.Height;
                }
                catch (System.Exception) { }
                finally
                {
                    if (defaultImg != null)
                        defaultImg.Dispose();
                }
            }

            width = defPreviewWidth;
            height = defPreviewHeight;
        }

        /// <summary>
        /// IsSupportedMasterUploadType
        /// </summary>
        public static Boolean IsSupportedMasterUploadType(UGCUpload.eType type)
        {
            foreach (UGCUpload.eType ult in SupportedMasterUploadTypes)
            {
                if (ult == type)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// IsSupportedWokItemUploadType
        /// </summary>
        public static Boolean IsSupportedWokItemUploadType(UGCUpload.eType type)
        {
            foreach (UGCUpload.eType ult in SupportedWokItemUploadTypes)
            {
                if (ult == type)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// IsSupportedDataUploadType
        /// </summary>
        public static Boolean IsSupportedDataUploadType(UGCUpload.eType type)
        {
            foreach (UGCUpload.eType ult in SupportedDataUploadTypes)
            {
                if (ult == type)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// SaveUGCUpload
        /// </summary>
        public static Boolean SaveUGCUpload(int userId, UGCUpload.eType uploadType, int uploadId, HttpPostedFile file, int fileIndex, out string fullPath)
        {
            bool res = false;
            fullPath = "";

            String newPath = Path.Combine(Configuration.UploadRepository, userId.ToString());
            String filename = uploadId + "." + UGCUpload.GetUploadTypeString(uploadType);
            if (fileIndex > 0)
                filename += "_" + fileIndex.ToString("000");


            if (!Configuration.UseAmazonS3Storage)
            {

                // Make sure the directory exists
                FileInfo fileInfo = new FileInfo(newPath + Path.DirectorySeparatorChar);
                if (!fileInfo.Directory.Exists)
                {
                    try
                    {
                        fileInfo.Directory.Create();
                    }
                    catch (System.Exception)
                    {
                        // directory creation failed
                        return false;
                    }
                }
            }


            if (Configuration.UseAmazonS3Storage)
            {
                try
                {
                    AmazonS3Client azS3client = GetAmazonS3Client();
                    TransferUtility fileTransferUtility = new TransferUtility(azS3client);

                    // Upload data from a type of System.IO.Stream.
                    fileTransferUtility.Upload(file.InputStream, Configuration.AmazonS3Bucket, newPath.Replace("\\", "/") + "/" + filename);

                    fullPath = newPath + Path.DirectorySeparatorChar + filename;
                    res = true;
                }
                catch (AmazonS3Exception s3Exception)
                {
                    m_logger.Error("S3 Error " + s3Exception.Message, s3Exception);
                }
            }
            else
            {

                // Store uploaded file
                file.InputStream.Position = 0;  // Reset seek position

                // Create a buffer
                int maximumBufferSize = 4096;
                byte[] transferBuffer;

                if (file.ContentLength > maximumBufferSize)
                {
                    transferBuffer = new byte[maximumBufferSize];
                }
                else
                {
                    transferBuffer = new byte[file.ContentLength];
                }

                fullPath = newPath + Path.DirectorySeparatorChar + filename;
                FileStream fsNewFile = new FileStream(fullPath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);

                try
                {
                    // Store the incoming data into a disk file
                    int bytesRead;
                    do
                    {
                        bytesRead = file.InputStream.Read(transferBuffer, 0, transferBuffer.Length);
                        fsNewFile.Write(transferBuffer, 0, bytesRead);
                    }
                    while (bytesRead > 0);
                    res = true;
                }
                catch (System.Exception ex)
                {
                    m_logger.Error("Writing file " + fullPath + " failed", ex);
                }
                finally
                {
                    fsNewFile.Close();
                }
            }

            return res;
        }

        /// <summary>
        /// CreateUGCItemInDB
        /// </summary>
        public static Boolean CreateAndDeployUGCItem(int userId, UGCUpload uploadItem, string name, string desc, string keywords, int category, WOKItem.ItemActiveStates itemStatus, UInt32 commission, int passType, bool isDerivable, out WOKItem item, out WOKItem baseItem)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            item = null;
            baseItem = null;
            UInt32 bundleCommision = commission;

            // check if uploadType is allowed for item creation
            if (!IsSupportedWokItemUploadType(uploadItem.Type))
            {
                // not a valid master upload type for item creation
                m_logger.Warn("CreateUGCItemInDB: unsupported upload type " + uploadItem.Type.ToString() + ". Cannot create item.");
                return false;
            }

            if (uploadItem.IsDerivation)
            {
                // isDerivation

                // is derivation supported by current upload type?
                if (!DoesUGCUploadTypeSupportDerivation(uploadItem.Type))
                {
                    m_logger.Warn("CreateUGCItemInDB: deriving with unsupported upload type " + uploadItem.Type.ToString() + ". Cannot create item.");
                    return false;   // derivation is not supported on this type
                }

                // verify base GLID
                if (uploadItem.BaseGlid == 0)
                {
                    m_logger.Warn("CreateUGCItemInDB: deriving without a base GLID. Cannot create item.");
                    return false;   // must specify base GLID
                }

                baseItem = shoppingFacade.GetItem(uploadItem.BaseGlid, true);
                if (baseItem == null)
                {
                    // GetItem failed
                    //ValidationFailed("Unrecoverable error while creating item: base item does not exist. [Err: NOBSE]");
                    m_logger.Error("CreateUGCItemInDB: Unrecoverable error while creating item: base item does not exist.");
                    return false;
                }

                // Get use value
                ItemParameter itemParameter = shoppingFacade.GetItemParameter(baseItem.GlobalId, baseItem.UseType);
                if (itemParameter != null)
                {
                    try
                    {
                        baseItem.UseValue = Convert.ToInt32(itemParameter.ParamValue);
                    }
                    catch (System.Exception) { }
                }
            }
            else // If not derivation, create base item
            {
                // UGC creation
                baseItem = new WOKItem();

                baseItem.GlobalId = 0;
                baseItem.ItemCreatorId = Convert.ToUInt32(userId);
                baseItem.Name = name;
                baseItem.DisplayName = name;
                baseItem.Description = desc;
                baseItem.Keywords = keywords;
                baseItem.PassTypeId = passType;
                baseItem.ItemActive = (int)itemStatus;


                baseItem.BaseGlobalId = 0;      // a root item
                baseItem.ArmAnywhere = 1;       // true
                baseItem.DestroyWhenUsed = 0;   // false
                baseItem.Disarmable = 1;        // true

                // Both of these Per Animesh 0 for scripts
                if (uploadItem.Type == UGCUpload.eType.ACTIONITEM)
                {
                    baseItem.DesignerPrice = 0;     // Commision is stored on the bundle below
                    commission = 0;

                    // New default thumbs
                    baseItem.ThumbnailPath = "KanevaIconScript.gif";
                    baseItem.ThumbnailSmallPath = "KanevaIconScript_sm.gif";
                    baseItem.ThumbnailMediumPath = "KanevaIconScript_me.gif";
                    baseItem.ThumbnailLargePath = "KanevaIconScript_la.gif";
                    baseItem.ThumbnailAssetdetailsPath = "KanevaIconScript_la.gif";
                }
                else
                {

                    baseItem.DesignerPrice = commission;     // Template designer's commission
                }

                baseItem.MarketCost = uploadItem.BasePrice;   // Price for user purchase
                baseItem.RequiredSkill = -1;
                baseItem.RequiredSkillLevel = 0;
                baseItem.Stackable = 1;
                baseItem.UseType = GetUseTypeForUploadItem(uploadItem);

                //#warning UseValue column is removed, now need to port this to item_parameters per Jim
                // used for creation
                baseItem.UseValue = GetUseValueForUploadItem(uploadItem);

                //baseItem.InventoryType = 256;   // Mark all these as credits only!!!!!!!!!!
                baseItem.InventoryType = Configuration.UGC_SellingCurrency;
                baseItem.SellingPrice = Convert.ToInt32(uploadItem.BasePrice * Configuration.UGC_SellingPriceFactor);      // Price when user wants to sell this item

                baseItem.IsDerivable = isDerivable ? 1 : 0;
                baseItem.DerivationLevel = WOKItem.DRL_TEMPLATE;

                // Process this base item
                // NOTE: for non-derivable types (e.g. sound, anim), baseItem should not be inserted into DB. It's only used consolidate code conveniently.
                if (!ProcessBaseUGCItem(uploadItem, baseItem))
                    return false;

                if (baseItem.GlobalId != 0)  // base item created?
                {
                    // Assign category to "template" item
                    shoppingFacade.AddCategoryToItem(baseItem.GlobalId, Convert.ToUInt32(category), 1);
                }
            }

            // Create item from base
            item = CreateAndDeployUGCItemFromBase(baseItem, userId, name, desc, keywords, passType, itemStatus, commission, category, uploadItem);


            // Add script and default GLID
            if (uploadItem.Type == UGCUpload.eType.ACTIONITEM)
            {
                m_logger.Debug("Type action item creating a new script GLID to assign to our bundle");

                // Create script and add to bundle
                WOKItem bundleItem = new WOKItem();

                bundleItem.GlobalId = 0;
                bundleItem.ItemCreatorId = Convert.ToUInt32(userId);
                bundleItem.Name = name;
                bundleItem.DisplayName = name;
                bundleItem.Description = desc;
                bundleItem.Keywords = keywords;
                bundleItem.PassTypeId = passType;
                bundleItem.ItemActive = (int)itemStatus;
                bundleItem.DesignerPrice = bundleCommision;     // Template designer's commission

                bundleItem.BaseGlobalId = 0;      // a root item
                bundleItem.ArmAnywhere = 1;       // true
                bundleItem.DestroyWhenUsed = 0;   // false
                bundleItem.Disarmable = 1;        // true
                bundleItem.MarketCost = 0;   // calculated later
                bundleItem.RequiredSkill = -1;
                bundleItem.RequiredSkillLevel = 0;
                bundleItem.Stackable = 1;
                bundleItem.UseType = WOKItem.USE_TYPE_BUNDLE;

                //#warning UseValue column is removed, now need to port this to item_parameters per Jim
                // used for creation
                bundleItem.UseValue = GetUseValueForUploadItem(uploadItem);

                //baseItem.InventoryType = 256;   // Mark all these as credits only!!!!!!!!!!
                bundleItem.InventoryType = Configuration.UGC_SellingCurrency;
                bundleItem.SellingPrice = 0;      // Price when user wants to sell this item

                bundleItem.IsDerivable = isDerivable ? 1 : 0;
                bundleItem.DerivationLevel = WOKItem.USE_TYPE_ACTION_ITEM;

                // Add default GLID to item_parameters
                if (uploadItem.DefaultGlid.Length > 0)
                {
                    try
                    {
                        // Copy the default glid icon if exists
                        WOKItem wiDefaultGlid = shoppingFacade.GetItem(Convert.ToInt32(uploadItem.DefaultGlid));

                        bundleItem.ThumbnailSmallPath = wiDefaultGlid.ThumbnailSmallPath;
                        bundleItem.ThumbnailMediumPath = wiDefaultGlid.ThumbnailMediumPath;
                        bundleItem.ThumbnailLargePath = wiDefaultGlid.ThumbnailLargePath;
                        bundleItem.ThumbnailAssetdetailsPath = wiDefaultGlid.ThumbnailAssetdetailsPath;
                        bundleItem.ThumbnailPath = wiDefaultGlid.ThumbnailPath;
                    }
                    catch (System.Exception) { }
                }

                int bundleGLID = shoppingFacade.AddCustomItem(bundleItem);

                // Add script to the bundle
                shoppingFacade.AddItemToBundle(bundleGLID, item.GlobalId, 1);

                // Per johnny The defaultGlid is the glid of the object to place when the action item is used. 
                // I think it gets added to the item_properties table.  But it would be a property of the script item, not the bundle.

                // Add default GLID to item_parameters
                if (uploadItem.DefaultGlid.Length > 0)
                {
                    shoppingFacade.UpdateItemParameter(item.GlobalId, 208, uploadItem.DefaultGlid);
                }
                else
                {
                    m_logger.Debug("No DefaultGlid defined for script");
                }

                // Set the item path on the script
                DeployUGCItem(uploadItem, item);

                bool IsAP = false;

                // Action items are a bundle, add all the bundle GLIDS
                if (uploadItem.ScriptBundleGLIDs.Length > 0)
                {
                    string[] arrGlids = uploadItem.ScriptBundleGLIDs.Split(',');
                    WOKItem wokItem;

                    foreach (string glid in arrGlids)
                    {
                        wokItem = shoppingFacade.GetItem(Convert.ToInt32(glid));

                        // http://bugz.kaneva.com/mantis/view.php?id=9432
                        // 0009432: Should not be able to include bundles or smart objects inside a smart object, the upload should fail

                        if (wokItem.UseType == WOKItem.USE_TYPE_BUNDLE)
                        {
                            // not a valid master upload type for item creation
                            m_logger.Warn("CreateUGCItemInDB: Bundles are not allowed. Cannot create item.");
                            return false;
                        }

                        // http://bugz.kaneva.com/mantis/view.php?id=9431
                        // 0009431: Upload of smart object including items that are AP, smart object bundle should be AP as well but isn't 
                        if (wokItem.PassTypeId == (int)Constants.ePASS_TYPE.ACCESS)
                        {
                            IsAP = true;
                        }

                        shoppingFacade.AddItemToBundle(bundleGLID, Convert.ToInt32(glid), 1);

                    }
                }
                else
                {
                    m_logger.Debug("No bundleGlids defined for script");
                }

                // Add categories to the bundle
                ItemCategory itemCategory;
                itemCategory = shoppingFacade.GetItemCategory(Convert.ToUInt16(category));
                shoppingFacade.AddCategoryToItem(bundleGLID, itemCategory.ItemCategoryId, 1);
                shoppingFacade.AddCategoryToItem(bundleGLID, itemCategory.ParentCategoryId, 2);

                // Category slot #3 designates this a 'Smart Object' belonging to the parent category.
                // This allows us to group smart objects within categories belonging to other upload types.
                List<ItemCategory> actionItemCatList = shoppingFacade.GetItemCategoriesByFilter("upload_type = " + UGCUpload.eType.ACTIONITEM + " and parent_category_id = " + itemCategory.ParentCategoryId);
                if (actionItemCatList.Count > 0)
                {
                    shoppingFacade.AddCategoryToItem(bundleGLID, actionItemCatList[0].ItemCategoryId, 3);
                }

                // Recalculate the bundle price here for search results purposes..
                WOKItem bundleToUpdatePrice = shoppingFacade.GetItem(bundleGLID);
                BundlePrice bundlePrice = shoppingFacade.CalculateBundlePrice(bundleToUpdatePrice.BundleItems, (int)bundleToUpdatePrice.DesignerPrice, (int)bundleToUpdatePrice.ItemCreatorId);
                // For Bundles Only: The stored market price is the actual web price
                bundleToUpdatePrice.MarketCost = bundlePrice.TotalBundle;

                if (IsAP)
                {
                    bundleToUpdatePrice.PassTypeId = (int)Constants.ePASS_TYPE.ACCESS;
                }

                shoppingFacade.UpdateCustomItem(bundleToUpdatePrice);
            }


            // New Item, tickle server with inventory update message
            // Find which server user is on and tickle that server for inventory update
            try
            {
                if (item != null && item.GlobalId > 0)
                {
                    // return server user is currently on
                    DataTable dt = new GameFacade().GetGameUserIn(userId);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        // Get the game id and server id
                        int gameId = Convert.ToInt32(dt.Rows[0]["game_id"]);
                        int serverId = Convert.ToInt32(dt.Rows[0]["server_id"]);

                        // Check to see if user is GM
                        CommunityFacade cf = new CommunityFacade();
                        if (cf.GetCommunityMember(cf.GetCommunityIdFromGameId(gameId), userId).HasEditRights)
                        {
                            (new RemoteEventSender()).broadcastItemChangeEventToServer(RemoteEventSender.ObjectType.PlayerInventory, RemoteEventSender.ChangeType.AddObject, item.GlobalId, serverId);
                        }
                    }
                }
            }
            catch { }


            return item != null;
        }


        protected static Boolean ProcessBaseUGCItem(UGCUpload uploadItem, WOKItem baseItem)
        {
            switch (uploadItem.Type)
            {
                case UGCUpload.eType.DOB_GZ:
                    if (!ProcessDynamicObjectBaseItem(uploadItem, baseItem))
                        return false;
                    break;
            }

            return true;
        }

        protected static bool PreProcessUGCItem(UGCUpload uploadItem, WOKItem item, out List<ItemParameter> itemParameters)
        {
            itemParameters = new List<ItemParameter>();
            switch (uploadItem.Type)
            {
                case UGCUpload.eType.ANM_GZ:
                    if (!PreProcessAnimationItem(uploadItem, item, itemParameters))
                        return false;
                    break;

                case UGCUpload.eType.EQP_GZ:
                    if (!PreProcessEquippableItem(uploadItem, item, itemParameters))
                        return false;
                    break;

                case UGCUpload.eType.ACTIONITEM:
                    if (!PreProcessActionItem(uploadItem, item, itemParameters))
                        return false;
                    break;

                case UGCUpload.eType.SND_OGG_KRX:
                    if (!PreProcessSoundItem(uploadItem, item, itemParameters))
                        return false;
                    break;
            }

            return true;
        }

        protected static bool PreProcessEffectItem(UGCUpload uploadItem, WOKItem item, List<ItemParameter> itemParameters, IDictionary<int, string> validationProps)
        {
            if (validationProps == null || !validationProps.ContainsKey(UGCProperty.PROP_EFFECT_DURATION))
                return false;   // properties are required to proceed

            string sDuration = validationProps[UGCProperty.PROP_EFFECT_DURATION];
            uint duration = 0;
            try
            {
                duration = Convert.ToUInt32(sDuration);
            }
            catch (System.Exception) { }

            itemParameters.Add(new ItemParameter(item.GlobalId, ItemParameter.PARAM_TYPE_EFFECT_DURATION, duration.ToString()));
            return true;
        }

        protected static bool PreProcessAnimationItem(UGCUpload uploadItem, WOKItem item, List<ItemParameter> itemParameters)
        {
            IDictionary<int, string> validationProps = GetValidationRequestDetails(uploadItem.ValidationReqID);
            if (validationProps == null || !validationProps.ContainsKey(UGCProperty.PROP_TARGET_ACTOR_TYPE))
                return false;   // properties are required to proceed

            if (!PreProcessEffectItem(uploadItem, item, itemParameters, validationProps))
            {
                return false;
            }

            string sActorType = validationProps[UGCProperty.PROP_TARGET_ACTOR_TYPE];
            int actorType = 0;
            try
            {
                actorType = Convert.ToInt32(sActorType);
            }
            catch (System.Exception) { }

            // actor type validation
            if (actorType != 1 && actorType != 2)
                actorType = 0;

            item.ActorGroup = actorType;
            return true;
        }

        protected static bool PreProcessSoundItem(UGCUpload uploadItem, WOKItem item, List<ItemParameter> itemParameters)
        {
            IDictionary<int, string> validationProps = GetValidationRequestDetails(uploadItem.ValidationReqID);
            if (validationProps == null)
                return false;   // properties are required to proceed

            if (!PreProcessEffectItem(uploadItem, item, itemParameters, validationProps))
            {
                return false;
            }

            return true;
        }

        protected static bool PreProcessEquippableItem(UGCUpload uploadItem, WOKItem item, List<ItemParameter> itemParameters)
        {
            IDictionary<int, string> validationProps = GetValidationRequestDetails(uploadItem.ValidationReqID);
            if (validationProps == null || !validationProps.ContainsKey(UGCProperty.PROP_EXCLUSION_GROUPS) || !validationProps.ContainsKey(UGCProperty.PROP_TARGET_ACTOR_TYPE))
                return false;   // properties are required to proceed

            string sActorType = validationProps[UGCProperty.PROP_TARGET_ACTOR_TYPE];
            int actorType = 0;
            try
            {
                actorType = Convert.ToInt32(sActorType);
            }
            catch (System.Exception) { }

            // actor type validation
            if (actorType != 1 && actorType != 2)
                actorType = 0;

            item.ActorGroup = actorType;

            string sExclusionGroups = validationProps[UGCProperty.PROP_EXCLUSION_GROUPS];

            // exclusion groups validation
            int exclusionGroup = 0;
            try
            {
                // NOTE: currently only one group per item (YC 03/2010)
                exclusionGroup = Convert.ToInt32(sExclusionGroups);
            }
            catch (System.Exception) { }

            foreach (int grp in BannedExclusionGroups)
            {
                if (exclusionGroup == grp)
                {
                    // Client-side tampering detected (or BUG)
                    // Reject this request
                    return false;
                }
            }

            // Note that global ID might not be available at this moment 
            // Set a dummy global ID value to item parameter, will be overridden by caller
            itemParameters.Add(new ItemParameter(0, ItemParameter.PARAM_TYPE_EXCLUSION_GROUPS, exclusionGroup.ToString()));
            return true;
        }

        protected static bool PreProcessActionItem(UGCUpload uploadItem, WOKItem item, List<ItemParameter> itemParameters)
        {
            IDictionary<int, string> validationProps = GetValidationRequestDetails(uploadItem.ValidationReqID);
            if (validationProps != null && validationProps.ContainsKey(UGCProperty.PROP_ACTIONITEM_PARENTSCRIPTGLID))
            {
                // Note that global ID might not be available at this moment 
                // Set a dummy global ID value to item parameter, will be overridden by caller
                string sParentScriptGlid = validationProps[UGCProperty.PROP_ACTIONITEM_PARENTSCRIPTGLID];
                int parentScriptGlid = -1;
                try
                {
                    parentScriptGlid = Convert.ToInt32(sParentScriptGlid);
                }
                catch (System.Exception) { }

                if (parentScriptGlid > 0)
                {
                    // Assign base global ID for DB insertion
                    item.BaseGlobalId = (UInt32)parentScriptGlid;
                }
            }
            return true;
        }

        protected static bool DeployDynObjItem(UGCUpload uploadItem, WOKItem item)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            // Copy mesh paths from base item (applicable to both derived and new DO item)
            List<ItemPath> filePaths = shoppingFacade.GetItemPaths(Convert.ToInt32(item.BaseGlobalId));
            HashSet<string> pathTypes = new HashSet<string>();
            bool bMeshPathFound = false;
            foreach (ItemPath filePath in filePaths)
            {
                pathTypes.Add(filePath.PathType);

                // update global ID to current item
                filePath.GlobalId = item.GlobalId;
                if (ItemPathType.IsMeshPathType(filePath.PathType))
                {
                    // only add mesh paths for now
                    shoppingFacade.AddItemPath(filePath);
                    bMeshPathFound = true;
                }
            }

            foreach (string pathType in pathTypes)
            {
                ItemPathType pathTypeInfo = shoppingFacade.GetItemPathType(pathType);
                if (pathTypeInfo == null)
                {
                    m_logger.Warn("PostProcessDynObjItem: no item_path_types record matching type [" + pathType + "]");
                }
                else
                {
                    string filestorePrefix = shoppingFacade.GetItemPathFileStore(Convert.ToInt32(item.BaseGlobalId), pathTypeInfo);
                    if (filestorePrefix != null)
                    {
                        shoppingFacade.AddItemPathFileStore(item.GlobalId, pathTypeInfo, filestorePrefix);
                    }
                }
            }

            if (!bMeshPathFound)
            {
                m_logger.Warn("PostProcessDynObjItem: base item does not have mesh paths, global ID=" +
                    item.GlobalId.ToString() + ", base global ID=" + item.BaseGlobalId.ToString());
            }

            return true;
        }

        protected static bool DeployUGCItem(UGCUpload uploadItem, WOKItem item)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            string itemPathType = GetItemPathTypeByUploadType(uploadItem.Type);
            if (itemPathType == null || !IsDeployRootPathConfigured(itemPathType))
            {
                m_logger.Warn("DeployUGCItem: DON'T know how to handle upload type " + uploadItem.Type.ToString());
                return false;
            }

            string deployRelativePath;
            int size, compressedSize;
            uint uniqueAssetId;
            if (!DeployUploadedFile(uploadItem.UserID, uploadItem.Type, uploadItem.ID, item.GlobalId, itemPathType, out deployRelativePath, out size, out compressedSize, out uniqueAssetId))
            {
                // Mark base item of an incomplete creation as "Deleted"
                item.ItemActive = (int)WOKItem.ItemActiveStates.Deleted;
                shoppingFacade.UpdateCustomItem(item);
                //ValidationFailed("Error creating item. Please try again later. [Err: DOBCP]");
                return false;
            }

            if (size != uploadItem.OriginalSize)
            {
                m_logger.Warn("DeployUGCItem: computed data size does not match record from ugc_uploads table. Expecting: " + uploadItem.OriginalSize + ", got " + size);
            }

            //insert path information into item_paths table
            if (deployRelativePath != null)
            {
                shoppingFacade.AddItemPath(new ItemPath(item.GlobalId, itemPathType, -1, deployRelativePath, uploadItem.OriginalSize, uploadItem.OriginalHash, compressedSize, uniqueAssetId));
            }
            else
            {
                m_logger.Error("DeployUGCItem: null item deployment path returned - globalID=" + item.GlobalId.ToString() + ", uploadType=" + uploadItem.Type.ToString());
            }
            return true;
        }

        public static bool DeployUGCItemTextures(int uploadId, UGCUpload.eType uploadType, int userId, int itemGlobalId, IDictionary<Int64, ItemPathTexture> textureMetadata, bool writeItemTemplateRecord, int baseGlobalId, out UGCUpload.eType textureUploadType)
        {
            textureUploadType = UGCUpload.eType.UNKNOWN;

            ShoppingFacade shoppingFacade = new ShoppingFacade();

            // Current JPG quality list -- hard-coded -- must match DB
            var JPGQualities = new int[] { 50, 25, 5 };

            IList<UGCUpload.eType> textureUploadTypes;
            UGCUtility.GetTextureUploadTypesFromMasterUploadType(uploadType, out textureUploadTypes);
            if (textureUploadTypes.Count == 0)
            {
                return false;
            }

            DataTable dt = UGCUtility.GetUGCUploads(userId, textureUploadTypes, uploadId, null, -1);
            if (dt == null || dt.Rows.Count == 0)
            {
                return false;
            }

            // Multiple texture uploads associated one item must use the same upload type.
            // Use first texture upload type
            UGCUpload.eType firstTextureUploadType = UGCUpload.eType.TEX_DDS_GZ;
            try
            {
                firstTextureUploadType = (UGCUpload.eType)Convert.ToInt32(dt.Rows[0]["upload_type"]);
                textureUploadType = firstTextureUploadType;
            }
            catch (Exception)
            {
                return false;
            }

            // Path types (wok.item_path_types.type)
            string strTexPathType = UGCUtility.GetItemPathTypeByUploadType(firstTextureUploadType);
            string strTemPathType = ItemPathType.GetTemplatePathTypeByTexturePathType(strTexPathType);
            var texPathTypeInfo = shoppingFacade.GetItemPathType(strTexPathType);
            if (texPathTypeInfo == null)
            {
                m_logger.Error("DeployUGCItemTextures: Error getting path type info for `" + strTexPathType + "'");
                return false;
            }

            bool uniqueTexFileStore = texPathTypeInfo.StoreType == ItemPathType.AssetStoreType.UNIQUE;

            string strJPGPathType = ItemPathType.GetJPGPathTypeByDDSPathType(strTexPathType, JPGQualities[0]);
            string temJPGPathType = ItemPathType.GetTemplatePathTypeByTexturePathType(strJPGPathType);

            var jpgPathTypeInfo = shoppingFacade.GetItemPathType(strJPGPathType);
            if (jpgPathTypeInfo == null)
            {
                m_logger.Error("DeployUGCItemTextures: Error getting path type info for `" + strJPGPathType + "'");
                return false;
            }

            Debug.Assert(!uniqueTexFileStore || jpgPathTypeInfo.StoreType == ItemPathType.AssetStoreType.AUX);

            // Texture deployment file store prefix (e.g. filestore11)
            string strTexFilestore = UGCUtility.GetDeployFilestore(texPathTypeInfo);

            // File extension
            string strTexExtension = UGCUtility.GetDeployExtension(texPathTypeInfo);
            string strJPGExtension = UGCUtility.TEXTURE_JPG_EXT;

            // Relative forward-slash path from file store root, with macros (PER_ITEM file store only)
            string strTexRelFSPathTemplate = null, strJPGRelFSPathTemplate = null;
            if (!uniqueTexFileStore)
            {
                // Path in per-item file store
                Debug.Assert(texPathTypeInfo.StoreType == ItemPathType.AssetStoreType.PER_ITEM);    // AUX type should not be here

                // Folder: UserId/GlobalId
                string strPathUserIdGlobalId = Path.Combine(userId.ToString(), itemGlobalId.ToString());    // 32145/3211123

                // File name with macros such as {subid}
                string uniqueName = KanevaGlobals.GenerateUniqueString(10);
                string strTexFilenameTemplate = uniqueName + TEXTURE_SUBID_TOKEN + strTexExtension;                             // A84FG823C4.{subid}.dds.gz or .krx.gz
                string strJPGFilenameTemplate = uniqueName + TEXTURE_SUBID_TOKEN + TEXTURE_QUALITY_TOKEN + strJPGExtension;     // A84FG823C4.{subid}.{q}.jpg

                // Relative forward-slash path from file store root, with macros
                strTexRelFSPathTemplate = Path.Combine(strPathUserIdGlobalId, strTexFilenameTemplate).Replace("\\", "/");    // 32145/3211123/A84FG823C4.{subid}.dds.gz or .krx.gz
                strJPGRelFSPathTemplate = Path.Combine(strPathUserIdGlobalId, strJPGFilenameTemplate).Replace("\\", "/");    // 32145/3211123/A84FG823C4.{subid}.{q}.jpg

                // Item path record: "template_gz" or "tem_dds_gz": ordinal=-1, no size/hash. One record per item.
                shoppingFacade.AddItemPath(new ItemPath(itemGlobalId, strTemPathType, -1, Path.Combine(strTexFilestore, strTexRelFSPathTemplate), 0, "", 0, 0));

                // Generate JPG paths if main texture is not encrypted
                if (firstTextureUploadType == UGCUpload.eType.TEX_DDS_GZ)
                {
                    // Item path record: "tem_jpg" for JPG files
                    shoppingFacade.AddItemPath(new ItemPath(itemGlobalId, temJPGPathType, -1, Path.Combine(strTexFilestore, strJPGRelFSPathTemplate), 0, "", 0, 0));
                }
            }
            else
            {
                // Unique texture store
                // Write per-item file store prefix if defined. This is particularly useful for preview environment where two separate file store prefixes are used.
                if (strTexFilestore != "")
                {
                    shoppingFacade.AddItemPathFileStore(itemGlobalId, texPathTypeInfo, strTexFilestore);
                }
            }

            // for all textures
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow drSubItem = dt.Rows[i];

                String sSubId = null;
                Int64 subId = -1;
                try
                {
                    sSubId = Convert.ToString(drSubItem["sub_id"]);
                }
                catch (System.Exception ex)
                {
                    m_logger.Error("Error getting texture sub ID for master upload: " + uploadId.ToString(), ex);
                }

                Debug.Assert(sSubId != null);
                if (sSubId == null)
                {
                    continue;
                }

                try
                {
                    subId = Convert.ToInt64(drSubItem["sub_id"]);
                }
                catch (System.Exception) { }

                // Insert record for UGC derivation
                if (writeItemTemplateRecord && subId != -1)
                {
                    UGCUtility.InsertItemTemplate(baseGlobalId, subId, "", 0, 0);
                }

                // Path to uploaded texture
                string uploadedTexturePath = null;
                try
                {
                    uploadedTexturePath = Convert.ToString(drSubItem["uploadName"]);
                }
                catch (System.Exception ex)
                {
                    m_logger.Error("Error getting uploaded texture path for master upload: " + uploadId.ToString() + ", subID: " + sSubId, ex);
                }

                Debug.Assert(uploadedTexturePath != null);
                if (uploadedTexturePath == null)
                {
                    continue;
                }

                string uploadedTexturePathForDeploy = uploadedTexturePath;

                // (Re-)compress uploaded texture if necessary
                string uploadFileFormat = UGCUpload.GetUploadFileFormat(firstTextureUploadType);
                byte[] hashSHA256;
                int size, compressedSize;
                if (!CheckAndConvertUploadedFile(ref uploadedTexturePathForDeploy, uploadFileFormat, texPathTypeInfo.FileFormat, uniqueTexFileStore ? true : false, out hashSHA256, out size, out compressedSize))
                {
                    m_logger.Warn("DeployUGCItemTextures: uploaded texture is in an incompatible format: expecting [" + texPathTypeInfo.FileFormat + "], got [" + uploadFileFormat + "], globalId = " + itemGlobalId + ", uploadId=" + uploadId);
                    continue;
                }

                ///////////////////////////////////////////////////////////
                // Deploy item texture file

                // Relative forward slash path from file store root, concrete, for item_paths table
                string strTexRelFSPath, strJPGRelFSPath;
                bool isNew = true;
                uint uniqueAssetId = 0;
                if (uniqueTexFileStore)
                {
                    // Check if asset already exists in file store
                    if (hashSHA256 == null)
                    {
                        m_logger.Error("DeployUGCItemTextures: error hashing asset file for unique file store: path type ID = " + texPathTypeInfo.TypeId + ", globalId = " + itemGlobalId + ", uploadId=" + uploadId);
                        continue;
                    }

                    if (texPathTypeInfo.SHA256Table == "")
                    {
                        m_logger.Error("DeployUGCItemTextures: missing unique asset table name: path type ID = " + texPathTypeInfo.TypeId + ", globalId = " + itemGlobalId + ", uploadId=" + uploadId);
                        return false;
                    }

                    uniqueAssetId = shoppingFacade.AllocUniqueAssetIdBySHA256(texPathTypeInfo, hashSHA256, size, compressedSize, out isNew);

                    strTexRelFSPath = GetDeployRelativePathUnique(uniqueAssetId, texPathTypeInfo);
                    strJPGRelFSPath = GetDeployRelativePathUnique(uniqueAssetId, jpgPathTypeInfo);  // item_path_type records of various JPG quality should share common path format
                }
                else
                {
                    strTexRelFSPath = strTexRelFSPathTemplate.Replace(ItemPathType.MACRO_SUB_ID, sSubId);			// 32145/3211123/A84FG823C4.215132154.dds.gz
                    strJPGRelFSPath = strJPGRelFSPathTemplate.Replace(ItemPathType.MACRO_SUB_ID, sSubId);			// 32145/3211123/A84FG823C4.215132154.{q}.jpg
                }

                string strTexRootPath = UGCUtility.GetDeployRootPath(strTexPathType, strTexRelFSPath);

                // Full asset path for file I/O
                string strTexFullPath = Path.Combine(strTexRootPath, strTexRelFSPath);   // \\...\Textures\32145/3211123/A84FG823C4.215132154.dds.gz or .krx.gz

                bool deployMainTexture = false;
                if (isNew)
                {
                    deployMainTexture = true;
                }
                else if (!AssetFileExists(strTexFullPath))  // Check if target file exists - sometimes we have records in unique_textures table without matching file on the disk
                {
                    m_logger.Info("DeployUGCItemTextures(" + itemGlobalId.ToString() + "): unique texture record exists but asset file not found - re-deploy to [" + strTexFullPath + "]");
                    deployMainTexture = true;
                }

                if (deployMainTexture)
                {
                    // Copy texture from upload store to content store
                    if (Configuration.UseAmazonS3Storage)
                    {
                        Stream originalImage = null;

                        try
                        {
                            AmazonS3Client azS3client = GetAmazonS3Client();

                            Amazon.S3.IO.S3FileInfo s3FileInfo = new Amazon.S3.IO.S3FileInfo(azS3client, Configuration.AmazonS3Bucket, uploadedTexturePathForDeploy.Replace("\\", "/"));
                            Amazon.S3.IO.S3FileInfo s3FileDeployInfo = new Amazon.S3.IO.S3FileInfo(azS3client, Configuration.AmazonS3Bucket, strTexFullPath.Replace("\\", "/"));

                            m_logger.Debug("DeployUGCItemTextures(" + itemGlobalId.ToString() + "): S3 copy from [" + s3FileInfo.FullName + "] to [" + s3FileDeployInfo.FullName + "], bucket: [" + Configuration.AmazonS3Bucket + "]");
                            s3FileInfo.CopyTo(s3FileDeployInfo);
                        }
                        catch (AmazonS3Exception s3Exception)
                        {
                            m_logger.Error("DeployUGCItemTextures(" + itemGlobalId.ToString() + "): S3 Error " + s3Exception.Message, s3Exception);
                        }
                        finally
                        {
                            if (originalImage != null)
                            {
                                originalImage.Dispose();
                            }
                        }
                    }
                    else
                    {
                        // Make sure destination folder exist
                        try
                        {
                            FileInfo fileInfo = new FileInfo(strTexFullPath);
                            if (!fileInfo.Directory.Exists)
                            {
                                fileInfo.Directory.Create();
                            }
                        }
                        catch (System.Exception ex)
                        {
                            m_logger.Error("Error creating destination folder for [" + strTexFullPath + "]", ex);
                            continue;
                        }

                        try
                        {
                            File.Copy(uploadedTexturePathForDeploy, strTexFullPath);
                            //@@Note: don't delete upload for now. Will purge periodically.
                            m_logger.Info("Deployed texture to [" + strTexFullPath + "]");
                        }
                        catch (System.Exception ex)
                        {
                            m_logger.Error("Error copying upload texture from [" + uploadedTexturePathForDeploy + "] to [" + strTexFullPath + "]", ex);
                            continue;
                        }
                    }
                }
                else
                {
                    m_logger.Info("Skipped deploying texture [" + strTexFullPath + "] - already exists");
                }

                // Update individual texture path
                int textureOriginalSize = 0;
                string textureOriginalHash = "";    // Hash of encrypted file, actually, but before compression

                try
                {
                    textureOriginalHash = Convert.ToString(drSubItem["hash"]);
                    textureOriginalSize = Convert.ToInt32(drSubItem["orig_size"]);
                }
                catch (Exception) { }

                if (textureOriginalSize != size)
                {
                    m_logger.Warn("DeployUGCItemTextures: computed data size does not match record from ugc_uploads table. Expecting: " + textureOriginalSize + ", got " + size);
                }

                // Individual texture path: ordinal=subId, size/hash provided. Once record per texture.
                shoppingFacade.AddItemPath(new ItemPath(itemGlobalId, strTexPathType, subId, Path.Combine(strTexFilestore, strTexRelFSPath), textureOriginalSize, textureOriginalHash, compressedSize, uniqueAssetId));

                // Texture metadata
                if (textureMetadata.ContainsKey(subId))
                {
                    var itemPathTex = textureMetadata[subId];
                    m_logger.Info("AddItemPathTexture: globalId=" + itemGlobalId + ", ordinal=" + subId + ", width=" + itemPathTex.Width + ", height=" + itemPathTex.Height + ", opacity=" + itemPathTex.Opacity + ", color=" + itemPathTex.AverageColor);
                    shoppingFacade.AddItemPathTexture(itemPathTex);
                }
                else
                {
                    m_logger.Warn("AddItemPathTexture: globalId=" + itemGlobalId + ", ordinal=" + subId + ": no texture metadata");
                }

                // Generate JPG multi-res textures if main texture is not encrypted
                if (firstTextureUploadType == UGCUpload.eType.TEX_DDS_GZ)
                {
                    // Decompress dds.gz
                    using (var decompressor = new TextureArchiveDecompressor(uploadedTexturePath, firstTextureUploadType))
                    {
                        if (!decompressor.Decompress())
                        {
                            m_logger.Error("Error decompressing uploaded texture GZ: " + uploadedTexturePath);
                        }
                        else
                        {
                            // JPG manifesting
                            var JPGFiles = new Dictionary<int, JPGInfo>();
                            var JPGFullPathsToDeploy = new Dictionary<int, string>();
                            foreach (int quality in JPGQualities)
                            {
                                string texJPGPathType = ItemPathType.GetJPGPathTypeByDDSPathType(strTexPathType, quality);
                                if (texJPGPathType == null)
                                {
                                    m_logger.Error("Manifesting: JPG path type for [" + firstTextureUploadType + "], quality:" + quality.ToString() + " is not defined");
                                    break;
                                }

                                string sQuality = string.Format("{0:000}", quality);
                                var JPG = new JPGInfo(strTexRootPath, strJPGRelFSPath.Replace(ItemPathType.MACRO_QUALITY, sQuality), texJPGPathType);
                                JPGFiles.Add(quality, JPG);
                                JPGFullPathsToDeploy.Add(quality, JPG.FullPath);
                            }

                            if (JPGFiles.Count < JPGQualities.Length)
                            {
                                // Error occurred
                                m_logger.Error("Cannot generate JPG images due to manifesting error");
                            }
                            else
                            {
                                // Convert DDS into JPG files
                                if (isNew || deployMainTexture)
                                {
                                    // Deploy all JPG files if texture is new or main texture is just deployed
                                }
                                else
                                {
                                    // Otherwise check if any of the JPG files exists. Remove them from deployment list if so.
                                    List<int> existingJPGs = new List<int>();
                                    foreach (var keyVal in JPGFullPathsToDeploy)
                                    {
                                        if (AssetFileExists(keyVal.Value))
                                        {
                                            existingJPGs.Add(keyVal.Key);
                                        }
                                    }

                                    foreach (var key in existingJPGs)
                                    {
                                        m_logger.Info("Skipped deploying texture [" + JPGFullPathsToDeploy[key] + "] - already exists");
                                        JPGFullPathsToDeploy.Remove(key);
                                    }
                                }

                                if (JPGFullPathsToDeploy.Count > 0 && !ImageHelper.GenerateJPGTexturesFromDDS(uploadedTexturePath, decompressor.DecompressedStream, JPGFullPathsToDeploy))
                                {
                                    m_logger.Error("Error occurred while converting DDS to multi-res JPG files. No JPG item_paths records generated.");
                                }
                                else
                                {
                                    foreach (var pair in JPGFiles)
                                    {
                                        var quality = pair.Key;
                                        var JPG = pair.Value;

                                        // Hash JPEG files
                                        JPG.computeSizeAndHash();
                                        if (JPG.Exists)
                                        {
                                            // Add item_paths concrete records for individual JPG files
                                            shoppingFacade.AddItemPath(new ItemPath(itemGlobalId, JPG.PathType, subId, Path.Combine(strTexFilestore, JPG.RelFSPath), (int)JPG.FileSize, JPG.FileHash, (int)JPG.FileSize, uniqueAssetId));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Debug.Assert(textureUploadType != UGCUpload.eType.UNKNOWN);
            return true;
        }

        public static bool DeployUGCItemIcons(int uploadId, int userId, int itemGlobalId, int itemUseType)
        {
            DataTable dtIcons = UGCUtility.GetUGCUploads(userId, UGCUpload.eType.ICON, uploadId);

            if (dtIcons == null || dtIcons.Rows.Count == 0)
            {
                return false;
            }

            // for all icons
            foreach (DataRow drIcon in dtIcons.Rows)
            {
                string iconFullPath = null;
                //icon
                try
                {
                    iconFullPath = Convert.ToString(drIcon["uploadName"]);
                }
                catch (System.Exception) { }

                if (iconFullPath == "")
                {
                    if (itemUseType == WOKItem.USE_TYPE_PARTICLE)
                    {
                        iconFullPath = UGCUtility.GetDefaultParticlePreviewImagePath();
                    }
                    else
                    {
                        iconFullPath = UGCUtility.GetDefaultSoundPreviewImagePath();
                    }
                }

                if (iconFullPath != null)
                {
                    string iconFileName = iconFullPath;
                    int slashIndex = iconFullPath.LastIndexOf("\\");
                    if (slashIndex != -1)
                        iconFileName = iconFullPath.Substring(slashIndex + 1);

                    // check icon count
                    int iconCount = 0;
                    int uscoreIndex = iconFileName.LastIndexOf("_");
                    if (uscoreIndex != -1)
                    {
                        try
                        {
                            iconCount = Convert.ToInt32(iconFileName.Substring(uscoreIndex + 1));
                        }
                        catch (System.Exception) { }
                        if (iconCount <= 0)
                            iconCount = 1;
                        else
                            iconCount++;

                        iconFileName = iconFileName.Substring(0, uscoreIndex);
                        iconFullPath = iconFullPath.Substring(0, uscoreIndex + slashIndex + 1);
                    }

                    if (iconFileName.Length > 0)
                    {
                        ImageHelper.GenerateWOKItemThumbs(iconFileName, iconFullPath, KanevaGlobals.TexturePath, userId, itemGlobalId, ".jpg");
                        if (iconCount > 1)
                        {
                            // multiple icon files
                            for (int fileLoop = 1; fileLoop < iconCount; fileLoop++)
                            {
                                string postFix = "_" + fileLoop.ToString("000");
                                string postFixJpg = "_" + fileLoop.ToString("000");
                                ImageHelper.GenerateWOKItemThumbs(iconFileName + postFix, iconFullPath + postFix, KanevaGlobals.TexturePath, userId, itemGlobalId, postFixJpg + ".jpg");
                            }
                        }
                    }
                }
            }

            return true;
        }

        public static bool DoesUGCUploadTypeSupportDerivation(UGCUpload.eType uploadType)
        {
            switch (uploadType)
            {
                case UGCUpload.eType.DOB_GZ:
                    return true;
            }

            // not support by default
            return false;
        }

        public static void GetTextureUploadTypesFromMasterUploadType(UGCUpload.eType uploadType, out IList<UGCUpload.eType> textureUploadTypes)
        {
            textureUploadTypes = new List<UGCUpload.eType>();
            switch (uploadType)
            {
                case UGCUpload.eType.DOB_GZ:
                case UGCUpload.eType.EQP_GZ:
                case UGCUpload.eType.PARTICLE:
                    textureUploadTypes.Add(UGCUpload.eType.TEX_DDS_GZ);
                    break;
                case UGCUpload.eType.MEDIA:
                    textureUploadTypes.Add(UGCUpload.eType.CUSTOMTEX);
                    break;
            }
        }

        protected static int GetUseTypeForUploadItem(UGCUpload uploadItem)
        {
            switch (uploadItem.Type)
            {
                case UGCUpload.eType.DOB_GZ:
                    return WOKItem.USE_TYPE_ADD_DYN_OBJ;

                case UGCUpload.eType.SND_OGG_KRX:
                    // sound behaves like an object to be placed so it has the same use type.
                    return WOKItem.USE_TYPE_SOUND;

                case UGCUpload.eType.ANM_GZ:
                    return WOKItem.USE_TYPE_ANIMATION;

                case UGCUpload.eType.EQP_GZ:
                    return WOKItem.USE_TYPE_EQUIP;

                case UGCUpload.eType.PARTICLE:
                    return WOKItem.USE_TYPE_PARTICLE;

                case UGCUpload.eType.ACTIONITEM:
                    return WOKItem.USE_TYPE_ACTION_ITEM;
            }

            return 0;
        }

        protected static int GetUseValueForUploadItem(UGCUpload uploadItem)
        {
            switch (uploadItem.Type)
            {
                case UGCUpload.eType.SND_OGG_KRX:
                    return 0 - uploadItem.UserID;
            }

            return 0;
        }

        protected static WOKItem CreateAndDeployUGCItemFromBase(WOKItem baseItem, int userId, string name, string desc, string keywords, int passType, WOKItem.ItemActiveStates itemStatus, UInt32 commission, int category, UGCUpload uploadItem)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            WOKItem item = new WOKItem();
            item.BaseGlobalId = Convert.ToUInt32(baseItem.GlobalId);
            item.ItemCreatorId = Convert.ToUInt32(userId);
            item.Name = name;
            item.DisplayName = name;
            item.Description = desc;
            item.Keywords = keywords;
            item.PassTypeId = passType;
            item.ItemActive = (int)itemStatus;
            if (item.BaseGlobalId == 0)
            {
                // UGC items without base item: set user-specified commission here.
                item.DesignerPrice = commission;
            }
            else
            {
                // UGC items with base item: if new creation, commission should go into base item (so 0 here). Set user-specified commission if deriving.
                item.DesignerPrice = uploadItem.IsDerivation ? commission : 0;  // If initial creation: commission is set in baseItem; just put 0 here to avoid double charge. Store commission here only if it's derivative.
            }
            //item.InventoryType = 256;  // Mark all these as credits only!!!!!!!!!!
            item.InventoryType = Configuration.UGC_SellingCurrency;
            item.ArmAnywhere = baseItem.ArmAnywhere;
            item.DestroyWhenUsed = baseItem.DestroyWhenUsed;
            item.Disarmable = baseItem.Disarmable;

            item.MarketCost = baseItem.MarketCost;
            item.RequiredSkill = baseItem.RequiredSkill;
            item.RequiredSkillLevel = baseItem.RequiredSkillLevel;
            item.SellingPrice = baseItem.SellingPrice;
            item.Stackable = baseItem.Stackable;
            item.UseType = baseItem.UseType;

            if (uploadItem.Type == UGCUpload.eType.ACTIONITEM)
            {
                item.ThumbnailPath = baseItem.ThumbnailPath;
                item.ThumbnailSmallPath = baseItem.ThumbnailSmallPath;
                item.ThumbnailMediumPath = baseItem.ThumbnailMediumPath;
                item.ThumbnailLargePath = baseItem.ThumbnailLargePath;
                item.ThumbnailAssetdetailsPath = baseItem.ThumbnailAssetdetailsPath;
            }

            //#warning UseValue column is removed, now need to port this to item_parameters per Jim
            // Needed for creation
            item.UseValue = baseItem.UseValue;      // Same as base global Id -- set by CreateItemTemplate

            if (!uploadItem.IsDerivation)   // not a derived item
                item.IsDerivable = baseItem.IsDerivable;    // copy derivable flag from base item
            else
                item.IsDerivable = 0;                       // otherwise, disable multi-level derivation

            item.DerivationLevel = uploadItem.IsDerivation ? WOKItem.DRL_UGCDO_DERIVATIVE_L1 : WOKItem.DRL_UGCDO_ORIGINAL;    // 1: derived UGC mesh items; 0: initial UGC mesh items

            // Process item and deploy files
            List<ItemParameter> itemParameters;
            if (!PreProcessUGCItem(uploadItem, item, out itemParameters))
            {
                return null;
            }

            // Add it to the db
            int globalId = shoppingFacade.AddCustomItem(item, uploadItem.AnimTargetActorGLID);
            if (globalId < 0)
            {
                return null;
            }

            // Set additional item parameters if provided
            foreach (ItemParameter ip in itemParameters)
            {
                // Use newly returned global ID - the one in ItemParameter is dummy
                shoppingFacade.UpdateItemParameter(globalId, ip.ParamTypeId, ip.ParamValue);
            }

            // Deploy asset file and add matching item_paths record
            if (uploadItem.Type == UGCUpload.eType.DOB_GZ)
            {
                // Special handling for dynamic object: copy from base item
                if (!DeployDynObjItem(uploadItem, item))
                {
                    return null;
                }
            }
            else if (!DeployUGCItem(uploadItem, item))
            {
                return null;
            }

            //Items can have up to three categories (Changes made 2009-11-19)
            //Start from lowest level and work upward 

            if (uploadItem.Type == UGCUpload.eType.ACTIONITEM)
            {
                // Categories are added to the bundle, not to the script
            }
            else
            {
                // Get passed in cat
                ItemCategory itemCategory;
                itemCategory = shoppingFacade.GetItemCategory(Convert.ToUInt16(category));


                // Assign category to new item
                shoppingFacade.AddCategoryToItem(item.GlobalId, itemCategory.ItemCategoryId, 1);

                // Middle tier
                // 			
                shoppingFacade.AddCategoryToItem(item.GlobalId, itemCategory.ParentCategoryId, 2);
            }

            #region Parent Cat
            // For third level cats and other instances where we need
            // to ensure a parent category is added.
            int parentCatUseType = 0;
            if (item.UseType == WOKItem.USE_TYPE_ANIMATION)
            {
                parentCatUseType = (int)UGCUpload.eType.ANM_GZ;
            }

            if (item.UseType == WOKItem.USE_TYPE_BUNDLE)
            {
                parentCatUseType = (int)UGCUpload.eType.ACTIONITEM;
            }


            //#warning UseValue column is removed, now need to port this to item_parameters per Jim
            // added back for item creation
            else if (item.UseValue < 0)
            {
                parentCatUseType = (int)UGCUpload.eType.SND_OGG_KRX;
            }
            else if (item.UseType == WOKItem.USE_TYPE_EQUIP)
            {
                parentCatUseType = (int)UGCUpload.eType.EQP_GZ;
            }

            // We do not want a 3rd level cat for DO animations
            if (parentCatUseType != 0 && !uploadItem.IsDOAnimation)
            {
                try
                {
                    if (uploadItem.Type == UGCUpload.eType.ACTIONITEM)
                    {
                        // Categories are added to the bundle, not to the script
                    }
                    else
                    {

                        uint parentCatId = shoppingFacade.GetItemCategoryParentByUploadType(parentCatUseType).ItemCategoryId;
                        shoppingFacade.AddCategoryToItem(item.GlobalId, Convert.ToUInt32(parentCatId), 3);
                    }
                }
                catch { }
            }
            #endregion Parent Cat


            return item;
        }

        protected static bool ProcessDynamicObjectBaseItem(UGCUpload uploadItem, WOKItem baseItem)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            // create base item first
            baseItem.GlobalId = shoppingFacade.CreateItemTemplate(baseItem);

            if (baseItem.GlobalId < 0)
            {
                // CreateItemTemplate failed
                //ValidationFailed("Error creating item. Please try again later. [Err: CRBSE]");
                return false;
            }

            // set use value to global ID of template ID (so all derivatives will copy it)
            baseItem.UseValue = baseItem.GlobalId;
            shoppingFacade.UpdateItemParameter(baseItem.GlobalId, baseItem.UseType, Convert.ToString(baseItem.UseValue));

            // Prepare dynamic object headers data for zone customization
            if (!SetupUGCDynamicObjectHeader(uploadItem.UserID, uploadItem.ID, baseItem.GlobalId, uploadItem.ValidationReqID))
            {
                // Mark base item of an incomplete creation as "Deleted"
                baseItem.ItemActive = (int)WOKItem.ItemActiveStates.Deleted;
                shoppingFacade.UpdateCustomItem(baseItem);
                //ValidationFailed("Error creating item. Please try again later. [Err: DOHDR]");
                return false;
            }

            return DeployUGCItem(uploadItem, baseItem);
        }

        private static Boolean GetAssetTypeByUploadType(UGCUpload.eType uploadType, out int assetType, out int assetSubType)
        {
            assetType = (int)Constants.eASSET_TYPE.ALL;
            assetSubType = (int)Constants.eASSET_SUBTYPE.ALL;

            switch (uploadType)
            {
                case UGCUpload.eType.PATTERN:
                    assetType = (int)Constants.eASSET_TYPE.PATTERN;
                    return true;

                case UGCUpload.eType.PICTURE:
                    assetType = (int)Constants.eASSET_TYPE.PICTURE;
                    return true;
            }

            return false;
        }

        private static bool IsSupportedCompression(string uploadFileCompressionFormat)
        {
            return uploadFileCompressionFormat == "gz";
        }

        public static Stream OpenAssetFileForRead(string inPath, string compressionFormat)
        {
            if (Configuration.UseAmazonS3Storage)
            {
                Stream originalImage = null;

                try
                {
                    AmazonS3Client azS3client = GetAmazonS3Client();
                    TransferUtility fileTransferUtility = new TransferUtility(azS3client);

                    originalImage = fileTransferUtility.OpenStream(Configuration.AmazonS3Bucket, inPath.Replace("\\", "/"));

                    if (compressionFormat == null)
                    {
                        // Plain file
                        return originalImage;
                    }

                    // Compressed file
                    try
                    {
                        MemoryStream outStream = new MemoryStream();
                        switch (compressionFormat)
                        {
                            case "gz":
                                DecompressGZStream(originalImage, outStream);
                                break;
                        }

                        outStream.Flush();
                        m_logger.Info("OpenAssetFileForRead: decompressing [" + inPath + "] " + outStream.Length.ToString() + " bytes written");
                        return outStream;
                    }
                    finally
                    {
                        if (originalImage != null)
                        {
                            originalImage.Dispose();
                        }
                    }
                }
                catch (AmazonS3Exception s3Exception)
                {
                    m_logger.Error("S3 Error " + s3Exception.Message, s3Exception);
                }
            }
            else
            {
                var fileInfo = new FileInfo(inPath);
                if (!fileInfo.Exists)
                {
                    m_logger.Error("OpenAssetFileForRead: path does not exist: " + inPath);
                    return null;
                }

                try
                {
                    if (compressionFormat == null)
                    {
                        // Plain file
                        return fileInfo.OpenRead();
                    }

                    // Compressed file
                    using (var inStream = fileInfo.OpenRead())
                    {
                        MemoryStream outStream = new MemoryStream();
                        switch (compressionFormat)
                        {
                            case "gz":
                                DecompressGZStream(inStream, outStream);
                                break;
                        }

                        outStream.Flush();
                        m_logger.Info("OpenAssetFileForRead: decompressing [" + inPath + "] " + outStream.Length.ToString() + " bytes written");
                        return outStream;
                    }
                }
                catch (Exception ex)
                {
                    m_logger.Error("OpenAssetFileForRead: " + ex.ToString() + ": " + inPath);
                }
            }

            return null;
        }

        private static void DecompressGZStream(Stream inStream, Stream outStream)
        {
            using (var gzStream = new GZipStream(inStream, CompressionMode.Decompress))
            {
                gzStream.CopyTo(outStream);
            }
        }

        private static void CompressStreamWithLZMA(Stream inStream, Stream outStream)
        {
            Int64 fileSize = inStream.Length;   // inStream must support Length. Otherwise NotSupportedException will be thown

            // See LzmaAlone.cs                                                                      
            SevenZip.Sdk.Compression.Lzma.Encoder encoder = new SevenZip.Sdk.Compression.Lzma.Encoder();
            /* Default compression settings as found in Encoder class:
             * DictionarySize      = 1 << 22  (less than C API?)
             * PosStateBits   (ps) = 2
             * LitContextBits (lc) = 3
             * LitPosBits     (pb) = 0
             * Algorithm           = 2  /// NOT USED?
             * NumFastBytes   (fb) = 32
             * MatchFinderType     = bt4
            */

            // Determine optimal dictionary size by file size
            int dictionarySize = 1 << 24;      // Default/max: 16MB
            while (dictionarySize > (1 << 16))
            {
                if ((dictionarySize >> 1) < fileSize)
                {
                    break;
                }
                dictionarySize >>= 1;   // Reduce dictionary size if file size is small
            }

            // Set compression properties
            encoder.SetCoderProperties(new CoderPropId[] { CoderPropId.DictionarySize }, new object[] { dictionarySize });

            // Write header (properties, dictionary size and uncompressed size)
            encoder.WriteCoderProperties(outStream);
            for (int i = 0; i < 8; i++)
            {
                outStream.WriteByte((Byte)(fileSize >> (8 * i)));
            }

            encoder.Code(inStream, outStream, inStream.Length, -1, null);
            outStream.Flush();
        }

        private static bool CheckAndConvertUploadedFile(ref string uploadFilePath, string uploadFileFormat, string deployFileFormat, bool generateHash, out byte[] hashSHA256, out int size, out int compressedSize)
        {
            hashSHA256 = null;
            size = 0;
            compressedSize = 0;

            string[] uploadFileFormatTokens = uploadFileFormat.Split('.');
            string[] deployFileFormatTokens = deployFileFormat.Split('.');
            string uploadFileCompressionMethod = uploadFileFormatTokens.Length == 2 ? uploadFileFormatTokens[1] : null;

            if (uploadFileFormat == deployFileFormat)
            {
                // No conversion needed
                if (Configuration.UseAmazonS3Storage)
                {
                    AmazonS3Client azS3client = GetAmazonS3Client();
                    Amazon.S3.IO.S3FileInfo s3FileInfo = new Amazon.S3.IO.S3FileInfo(azS3client, Configuration.AmazonS3Bucket, uploadFilePath.Replace("\\", "/"));

                    compressedSize = (int)s3FileInfo.Length;
                }
                else
                {
                    try
                    {
                        var fileInfo = new FileInfo(uploadFilePath);
                        compressedSize = (int)fileInfo.Length;
                    }
                    catch (Exception ex)
                    {
                        m_logger.Warn("CheckAndConvertUploadedFile: " + ex.ToString() + ", while checking size: " + uploadFilePath);
                    }
                }

                try
                {
                    using (Stream decompressedStream = OpenAssetFileForRead(uploadFilePath, uploadFileCompressionMethod))
                    {
                        if (generateHash)
                        {
                            SHA256 sha256 = SHA256.Create();
                            hashSHA256 = sha256.ComputeHash(decompressedStream);
                        }
                        size = (int)decompressedStream.Length;
                    }
                }
                catch (Exception ex)
                {
                    m_logger.Warn("CheckAndConvertUploadedFile: " + ex.ToString() + ", while hashing uploaded file: " + uploadFilePath);
                }
                return true;
            }

            // Compress/recompress with lzma if necessary
            if (deployFileFormatTokens.Length != 2 || deployFileFormatTokens[1] != "lzma")
            {
                return false;
            }

            // Deploy format is in *.lzma: check upload format
            if (uploadFileFormatTokens[0] != deployFileFormatTokens[0] || uploadFileFormatTokens.Length > 2)
            {
                return false;
            }

            // Same base format: check if upload is uncompressed or compressed with a supported format
            if (uploadFileCompressionMethod != null && !IsSupportedCompression(uploadFileCompressionMethod))
            {
                return false;
            }

            // Open uploaded file, decompress if necessary
            try
            {
                using (Stream decompressedStream = OpenAssetFileForRead(uploadFilePath, uploadFileCompressionMethod))
                {
                    if (decompressedStream == null)
                    {
                        m_logger.Warn("CheckAndConvertUploadedFile: error reading uploaded file: " + uploadFilePath);
                        return false;
                    }

                    // Generate hash if necessary
                    if (generateHash)
                    {
                        try
                        {
                            // Rewind decompressed stream
                            decompressedStream.Seek(0, SeekOrigin.Begin);

                            SHA256 sha256 = SHA256.Create();
                            hashSHA256 = sha256.ComputeHash(decompressedStream);
                        }
                        catch (Exception ex)
                        {
                            m_logger.Warn("CheckAndConvertUploadedFile: " + ex.ToString() + ", while hashing uploaded file: " + uploadFilePath);
                        }
                    }
                    size = (int)decompressedStream.Length;

                    // Rewind decompressed stream
                    decompressedStream.Seek(0, SeekOrigin.Begin);

                    // Compress with lzma
                    string outputName = uploadFilePath + ".lzma";

                    if (Configuration.UseAmazonS3Storage)
                    {
                        m_logger.Info("CheckAndConvertUploadedFile:re-compressed S3 Path: Start");

                        MemoryStream outStream = new MemoryStream();

                        try
                        {
                            CompressStreamWithLZMA(decompressedStream, outStream);
                            m_logger.Info("CheckAndConvertUploadedFile: re-compressed uploaded file: " + uploadFilePath + " to " + outputName);

                            // Save it
                            AmazonS3Client azS3client = GetAmazonS3Client();
                            TransferUtility fileTransferUtility = new TransferUtility(azS3client);

                            compressedSize = (int)outStream.Length;
                            uploadFilePath = outputName;

                            // Upload data from a type of System.IO.Stream.
                            fileTransferUtility.Upload(outStream, Configuration.AmazonS3Bucket, outputName.Replace("\\", "/"));

                            m_logger.Info("CheckAndConvertUploadedFile:re-compressed S3 Path Result: GOOD");
                        }
                        catch (Exception s3Exc)
                        {
                            m_logger.Info("CheckAndConvertUploadedFile:re-compressed S3 Path Result: Fail", s3Exc);
                        }
                        finally
                        {
                            if (outStream != null)
                            {
                                outStream.Close();
                            }
                        }

                        return true;

                    }
                    else
                    {
                        using (Stream lzmaStream = new FileStream(outputName, FileMode.Create, FileAccess.Write))
                        {
                            CompressStreamWithLZMA(decompressedStream, lzmaStream);
                            m_logger.Info("CheckAndConvertUploadedFile: re-compressed uploaded file: " + uploadFilePath + " to " + outputName);
                            compressedSize = (int)lzmaStream.Length;
                            uploadFilePath = outputName;
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                m_logger.Warn("CheckAndConvertUploadedFile: " + ex.ToString() + " while converting uploaded file: " + uploadFilePath);
                return false;
            }
        }

        #endregion

        #region Upload Item and Related Functions

        public static IList<UGCUpload> GetUploadedUGCList(int userId, int refUploadId)
        {
            return GetUploadedUGCList(userId, refUploadId, false);
        }

        public static IList<UGCUpload> GetUploadedUGCList(int userId, int refUploadId, bool bGetItemsOfSameType)
        {
            UGCUpload.eType refUploadType = UGCUpload.eType.UNKNOWN;

            DataRow refRow = GetUGCUpload(userId, UGCUpload.eType.UNKNOWN, refUploadId);
            if (refRow == null)
                return null;

            if (refRow["upload_type"] != null)
            {
                try
                {
                    refUploadType = (UGCUpload.eType)Convert.ToInt32(refRow["upload_type"]);
                }
                catch (System.Exception) { }
            }

            int uploadIdFilter = -1;
            if (!bGetItemsOfSameType)
                uploadIdFilter = refUploadId;

            IList<UGCUpload.eState> uploadedStates = new List<UGCUpload.eState>();
            uploadedStates.Add(UGCUpload.eState.UPLOADED);
            uploadedStates.Add(UGCUpload.eState.DERIVATION);
            DataTable dt = GetUGCUploads(userId, refUploadType, -1, uploadedStates, uploadIdFilter);
            if (dt == null)
                return null;

            IList<UGCUpload> ulList = new List<UGCUpload>();
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            for (int rowId = 0; rowId < dt.Rows.Count; rowId++)
            {
                int uploadId;
                UGCUpload.eType uploadType = UGCUpload.eType.UNKNOWN;
                string uploadName = null;
                DateTime uploadTime = DateTime.Now;
                string uploadSummary = null;
                int basePrice = -1;
                int baseGlid = 0;
                int validationReqId = 0;
                UGCUpload.eState uploadState = UGCUpload.eState.DONTCARE;
                int bytes = 0;
                string originalHash = "";
                int originalSize = 0;

                DataRow dr = dt.Rows[rowId];

                try
                {
                    uploadName = Convert.ToString(dr["origName"]);
                    uploadId = Convert.ToInt32(dr["upload_id"]);
                    uploadType = (UGCUpload.eType)Convert.ToInt32(dr["upload_type"]);
                    uploadTime = Convert.ToDateTime(dr["updateTime"]);
                    uploadSummary = Convert.ToString(dr["summary"]);
                    uploadState = (UGCUpload.eState)Convert.ToInt32(dr["state"]);
                    basePrice = Convert.ToInt32(dr["base_price"]);
                    baseGlid = Convert.ToInt32(dr["base_global_id"]);
                    validationReqId = Convert.ToInt32(dr["validation_request_id"]);
                    bytes = Convert.ToInt32(dr["bytes"]);
                    originalHash = Convert.ToString(dr["hash"]);
                    originalSize = Convert.ToInt32(dr["orig_size"]);
                }
                catch (System.Exception)
                {
                    uploadId = -1;
                }

                if (uploadId != -1)
                {
                    // calculate upload fee
                    UInt32 baseCommission = 0;
                    if (baseGlid != 0)
                    {
                        // Get base commission (for derivations only)
                        WOKItem wokItem = shoppingFacade.GetItem(baseGlid, true);
                        if (wokItem != null)
                            baseCommission = wokItem.DesignerPrice;
                    }

                    UGCUpload item = new UGCUpload(
                        uploadId, uploadType, userId, uploadName, uploadTime, uploadSummary, basePrice, uploadState == UGCUpload.eState.DERIVATION,
                        baseCommission, baseGlid, validationReqId, bytes, originalHash, originalSize);
                    ulList.Add(item);
                }
            }

            return ulList;
        }

        #endregion

        #region TextureArchiveDecompressor
        private class TextureArchiveDecompressor : IDisposable
        {
            public TextureArchiveDecompressor(string fullPath, UGCUpload.eType uploadType)
            {
                _fullPath = fullPath;
                _uploadType = uploadType;
            }

            public Stream DecompressedStream { get { return _decompressedStream; } }

            public bool Decompress()
            {
                if (_decompressedStream == null)
                {
                    switch (_uploadType)
                    {
                        case UGCUpload.eType.TEX_DDS_GZ:
                            _decompressedStream = UGCUtility.OpenAssetFileForRead(_fullPath, "gz");
                            break;
                    }
                }

                return _decompressedStream != null;
            }

            public void Dispose()
            {
                if (_decompressedStream != null)
                {
                    _decompressedStream.Close();
                    _decompressedStream = null;
                }
            }

            private string _fullPath = null;
            private UGCUpload.eType _uploadType = UGCUpload.eType.UNKNOWN;
            private Stream _decompressedStream = null;
        }
        #endregion

        #region JPGInfo
        private class JPGInfo
        {
            public JPGInfo(string texRootPath, string relFSPath, string pathType)
            {
                _relFSPath = relFSPath;
                _fullPath = Path.Combine(texRootPath, _relFSPath);
                _pathType = pathType;
            }

            public void computeSizeAndHash()
            {
                using (var md5 = System.Security.Cryptography.MD5.Create())
                {
                    if (Configuration.UseAmazonS3Storage)
                    {
                        AmazonS3Client azS3client = GetAmazonS3Client();
                        Amazon.S3.IO.S3FileInfo s3FileInfo = new Amazon.S3.IO.S3FileInfo(azS3client, Configuration.AmazonS3Bucket, _fullPath.Replace("\\", "/"));
                        _exists = s3FileInfo.Exists;
                        if (_exists)
                        {
                            _fileSize = (int)s3FileInfo.Length;

                            using (var stream = s3FileInfo.OpenRead())
                            {
                                byte[] md5sum = md5.ComputeHash(stream);
                                _fileHash = "";
                                foreach (byte b in md5sum)
                                {
                                    _fileHash += string.Format("{0:x2}", b);
                                }
                            }
                        }
                    }
                    else
                    {
                        var fileInfo = new FileInfo(_fullPath);
                        _exists = fileInfo.Exists;
                        if (_exists)
                        {
                            _fileSize = (int)fileInfo.Length;
                            using (var stream = File.OpenRead(_fullPath))
                            {
                                byte[] md5sum = md5.ComputeHash(stream);
                                _fileHash = "";
                                foreach (byte b in md5sum)
                                {
                                    _fileHash += string.Format("{0:x2}", b);
                                }
                            }
                        }
                    }
                }
            }

            public bool Exists { get { return _exists; } }
            public string RelFSPath { get { return _relFSPath; } }
            public string FullPath { get { return _fullPath; } }
            public int FileSize { get { return _fileSize; } }
            public string FileHash { get { return _fileHash; } }
            public string PathType { get { return _pathType; } }

            private bool _exists = false;
            private string _relFSPath = null;
            private string _fullPath = null;
            private int _fileSize = 0;
            private string _fileHash = null;
            private string _pathType = null;
        }
        #endregion

        private const string defaultImageName = "KanevaIconWOKItem_ad.jpg";
        private const string defaultSoundImageName = "KanevaIconAudio.gif";
        private static int defPreviewWidth = 0;
        private static int defPreviewHeight = 0;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }

    // to be moved into a separate file
    public class AssetUtility
    {


        public static string CreateAssetFromFile(int userId, int channelId, string filePath, string fileNameOverride, string title, string tags, string description, string instructions,
            string companyName, int categoryId, int permission, bool isRestricted, bool isPattern, string userHostAddress, out int assetId, out string textureUrl)
        {
            // set asset ID to invalid value
            assetId = -1;
            textureUrl = "";

            string res = null;
            Stream stream = null;
            long numBytes = 0;

            if (Configuration.UseAmazonS3Storage)
            {

                // don't even try to open we can just copy this later

                //try
                //{
                //    AmazonS3Client azS3client = new AmazonS3Client(Configuration.AmazonS3AccessKeyId,
                //           Configuration.AmazonS3SecretAccessKey,
                //           Amazon.RegionEndpoint.USEast1);

                //    TransferUtility fileTransferUtility = new TransferUtility(azS3client);

                //    stream = fileTransferUtility.OpenStream(Configuration.AmazonS3Bucket, filePath.Replace("\\", "/"));
                //}
                //catch (AmazonS3Exception s3Exception)
                //{
                //    m_logger.Error("S3 Error " + s3Exception.Message, s3Exception);
                //}
            }
            else
            {
                try
                {
                    stream = File.Open(filePath, FileMode.Open);
                    numBytes = stream.Length;
                }
                catch (Exception ex)
                {
                    // todo: log this error
                    res = "Error opening asset file (" + filePath + "): " + ex.Message;
                }
            }



            if (res == null)
            {
                try
                {
                    res = CreateAssetFromStream(userId, channelId, fileNameOverride == null ? filePath : fileNameOverride, stream, numBytes,
                                                 title, tags, description, instructions,
                                                 companyName, categoryId, permission, isRestricted, isPattern,
                                                 userHostAddress, out assetId, out textureUrl, filePath.Replace("\\", "/"));
                }
                catch (System.Exception ex)
                {
                    res = "Asset creation failed: " + ex.Message;
                }
                finally
                {
                    if (stream != null) // stream will be null if using Amazon S3
                    {
                        stream.Close();
                    }
                }
            }

            return res;
        }

        public static string CreateAssetFromPostedFile(int userId, int channelId, HttpPostedFile File, string title, string tags, string description, string instructions,
            string companyName, int categoryId, int permission, bool isRestricted, bool isPattern, string userHostAddress, out int assetId, out string textureUrl)
        {
            return CreateAssetFromStream(userId, channelId, File.FileName, File.InputStream, File.ContentLength,
                                         title, tags, description, instructions,
                                         companyName, categoryId, permission, isRestricted, isPattern,
                                         userHostAddress, out assetId, out textureUrl, "");
        }

        public static string CreateAssetFromStream(int userId, int channelId, string fileName, Stream stream, long numBytes, string title, string tags, string description, string instructions,
            string companyName, int categoryId, int permission, bool isRestricted, bool isPattern, string userHostAddress, out int assetId, out string textureUrl, string originalS3File)
        {
            // initialize asset ID to invalid value
            assetId = -1;
            textureUrl = "";

            string filename = System.IO.Path.GetFileName(fileName);
            filename = StoreUtility.CleanImageFilename(filename);

            int assetTypeId = (int)Constants.eASSET_TYPE.VIDEO;

            // Make sure it is a valid file type
            string strExtensionUpperCase = System.IO.Path.GetExtension(fileName).ToUpper();

            // Make sure the extensions are correct
            if (StoreUtility.IsFileVideo(strExtensionUpperCase))
            {
                assetTypeId = (int)Constants.eASSET_TYPE.VIDEO;
            }
            else if (StoreUtility.IsFilePicture(strExtensionUpperCase))
            {
                if (isPattern)
                {
                    assetTypeId = (int)Constants.eASSET_TYPE.PATTERN;
                }
                else
                {
                    assetTypeId = (int)Constants.eASSET_TYPE.PICTURE;
                }
            }
            else if (StoreUtility.IsFileGame(strExtensionUpperCase))
            {
                assetTypeId = (int)Constants.eASSET_TYPE.GAME;
            }
            else if (StoreUtility.IsFileMusic(strExtensionUpperCase))
            {
                assetTypeId = (int)Constants.eASSET_TYPE.MUSIC;
            }
            else
            {
                return ("File \\'" + filename + "\\' is not a supported file type. Please provide media in a Kaneva-supported format.\\n");
            }

            // Get the upload repository
            string uploadRepository = "";
            try
            {
                uploadRepository = Configuration.UploadRepository;
            }
            catch (Exception e)
            {
                m_logger.Error("UploadRepository is not defined in web.config", e);
                return "UploadRepository is not defined in web.config";
            }

            // Add the file
            try
            {
                string newPath = CreateNewRecord(out assetId, userId, channelId, title, filename, assetTypeId, tags, numBytes,
                    uploadRepository, categoryId, description, permission, isRestricted, instructions, companyName, userHostAddress);

                if (Configuration.UseAmazonS3Storage)
                {
                   
                    // Just do a copy here
                    try
                    {
                        AmazonS3Client azS3client = new AmazonS3Client(Configuration.AmazonS3AccessKeyId,
                            Configuration.AmazonS3SecretAccessKey,
                            Amazon.RegionEndpoint.USEast1);
                   
                        Amazon.S3.IO.S3FileInfo s3FileInfo = new Amazon.S3.IO.S3FileInfo(azS3client, Configuration.AmazonS3Bucket, originalS3File.Replace("\\", "/"));
                        Amazon.S3.IO.S3FileInfo s3FileDeployInfo = new Amazon.S3.IO.S3FileInfo(azS3client, Configuration.AmazonS3Bucket, newPath.Replace("\\", "/") + "/" + filename);

                        s3FileInfo.CopyTo(s3FileDeployInfo);
                    }
                    catch (AmazonS3Exception s3Exception)
                    {
                        m_logger.Error("S3 Error " + s3Exception.Message, s3Exception);
                    }
                }
                else
                {

                    // Make sure the directory exists
                    FileInfo fileInfo = new FileInfo(newPath + Path.DirectorySeparatorChar);
                    if (!fileInfo.Directory.Exists)
                    {
                        fileInfo.Directory.Create();
                    }

                    int maximumBufferSize = 4096;
                    byte[] transferBuffer;

                    if (numBytes > maximumBufferSize)
                    {
                        transferBuffer = new byte[maximumBufferSize];
                    }
                    else
                    {
                        transferBuffer = new byte[numBytes];
                    }

                    System.IO.FileStream fsNewFile = new System.IO.FileStream(newPath + Path.DirectorySeparatorChar + filename, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);

                    int totalBytes = 0;
                    int bytesRead;
                    do
                    {
                        bytesRead = stream.Read(transferBuffer, 0, transferBuffer.Length);
                        if (bytesRead > 0)
                        {
                            totalBytes += bytesRead;
                            fsNewFile.Write(transferBuffer, 0, bytesRead);
                        }
                    }
                    while (bytesRead > 0);

                    fsNewFile.Close();
                }

                // For pictures or patterns, generate the thumbs
                if (assetTypeId.Equals((int)Constants.eASSET_TYPE.PICTURE) || assetTypeId.Equals((int)Constants.eASSET_TYPE.PATTERN))
                {
                    CreatePhotoThumbs(assetId, userId, newPath + Path.DirectorySeparatorChar + filename, filename);

                    string textureUrlQuery = "SELECT file_size, image_full_path, thumbnail_xlarge_path FROM assets WHERE asset_id=@assetId";
                    Hashtable parameters = new Hashtable();
                    parameters.Add("@assetId", assetId);

                    DataRow dr = KanevaGlobals.GetDatabaseUtility().GetDataRow(textureUrlQuery, parameters, false);
                    if (dr != null)
                    {
                        if (!dr["file_size"].Equals(DBNull.Value))
                        {
                            if (Convert.ToInt32(dr["file_size"]) > 256000)
                            {
                                if (!dr["thumbnail_xlarge_path"].Equals(DBNull.Value))
                                    textureUrl = dr["thumbnail_xlarge_path"].ToString();
                            }
                            else
                            {
                                if (!dr["image_full_path"].Equals(DBNull.Value))
                                    textureUrl = dr["image_full_path"].ToString();
                            }
                        }
                        else
                        {
                            if (!dr["thumbnail_xlarge_path"].Equals(DBNull.Value))
                                textureUrl = dr["thumbnail_xlarge_path"].ToString();
                        }
                    }
                }

            }
            catch (UnauthorizedAccessException exc)
            {
                m_logger.Error("There was a fatal error posting the File \\'" + filename + "\\'. Please try again later.", exc);
                return ("There was a fatal error posting the File \\'" + filename + "\\'. Please try again later.\\n");
            }
            catch (Exception exc)
            {
                m_logger.Error("There was an error posting the File \\'" + filename + "\\'. Please try again.", exc);
                return ("There was an error posting the File \\'" + filename + "\\'. Please try again.\\n");
            }

            return "";
        }

        /// <summary>
        /// Create new asset_upload and asset record
        /// </summary>
        /// <param name="user_id"></param>
        private static string CreateNewRecord(out int assetId, int user_id, int channel_id, string itemName, string fileName, int assetTypeId,
            string tags, long size, string uploadRepository, int categoryId, string description, int permission, bool isRestricted,
            string instructions, string companyName, string userHostAddress)
        {
            //create a new asset
            string newItemName = itemName;
            string newPath = "";

            /* Code taken from assetEdit page  */
            int isMature = (int)Constants.eASSET_RATING.GENERAL;
            if (isRestricted)
            {
                isMature = (int)Constants.eASSET_RATING.MATURE;
            }

            // Categories
            int category1Id = categoryId;

            // Create a new asset record
            assetId = StoreUtility.InsertAsset(assetTypeId, 0, HttpUtility.HtmlEncode(newItemName), user_id, UsersUtility.GetUserNameFromId(user_id),
                (int)Constants.ePUBLISH_STATUS.UPLOADED, permission, isMature, category1Id, HttpUtility.HtmlEncode(description),
                HttpUtility.HtmlEncode(instructions), HttpUtility.HtmlEncode(companyName), "");

            if (assetId > 0)
            {
                // Non pictures must be processed, pictures don't
                if (assetTypeId.Equals((int)Constants.eASSET_TYPE.PICTURE) || assetTypeId.Equals((int)Constants.eASSET_TYPE.PATTERN))
                {
                    newPath = Path.Combine(Path.Combine(KanevaGlobals.ContentServerPath, user_id.ToString()), assetId.ToString());
                    StoreUtility.UpdateAssetFilePath(assetId, fileName, newPath, size);
                    StoreUtility.UpdateAssetStatus(assetId, (int)Constants.eASSET_STATUS.ACTIVE);
                    StoreUtility.UpdateAssetPublishStatus(assetId, (int)Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE, 0);
                }
                else
                {
                    // Add non-pictures to the asset  upload table to be processed
                    int assetUploadId = StoreUtility.InsertAssetUpload(user_id, fileName, uploadRepository, size, "", (int)Constants.eDS_INVENTORYTYPE.ASSETS, assetId);
                    newPath = Path.Combine(Path.Combine(uploadRepository, user_id.ToString()), assetId.ToString());
                    StoreUtility.UpdateAssetUploadPath(assetUploadId, newPath);
                    StoreUtility.UpdateAssetUploadStatus(assetId, (int)Constants.ePUBLISH_STATUS.UPLOADED, userHostAddress);

                    // Fix the media path
                    string mediaPath = "";
                    if (assetTypeId.Equals((int)Constants.eASSET_TYPE.MUSIC) || assetTypeId.Equals((int)Constants.eASSET_TYPE.GAME))
                    {
                        // Music
                        mediaPath = (Configuration.FileStoreHack + Path.Combine(user_id.ToString(), assetId.ToString()) + "/" + fileName);
                        mediaPath = mediaPath.Replace("\\", "/");
                        StoreUtility.UpdateAssetMediaPath(assetId, mediaPath);
                    }
                    else if (assetTypeId.Equals((int)Constants.eASSET_TYPE.VIDEO))
                    {
                        // Video
                        mediaPath = Configuration.FileStoreHack + Path.Combine(user_id.ToString(), assetId.ToString()) + "/" + System.IO.Path.GetFileNameWithoutExtension(fileName) + ".FLV";
                        mediaPath = mediaPath.Replace("\\", "/");
                        StoreUtility.UpdateAssetMediaPath(assetId, mediaPath);
                    }
                }

                // Insert into user's personal channel
                UserFacade userFacade = new UserFacade();
                int personalChannelId = userFacade.GetPersonalChannelId(user_id);
                StoreUtility.InsertAssetChannel(assetId, personalChannelId);

                if (channel_id != personalChannelId)
                {
                    //then insert into the broadcast channel
                    StoreUtility.InsertAssetChannel(assetId, channel_id);
                }

                // Update the tags
                if (tags.Length > 0)
                {
                    StoreUtility.UpdateAssetTags(assetId, HttpUtility.HtmlEncode(tags));
                }
            }
            else
            {
                m_logger.Error("Failed to insert asset record user " + user_id + " filename = " + fileName);
            }

            return newPath;
        }

        /// <summary>
        /// CreatePhotoThumbs
        /// </summary>
        private static void CreatePhotoThumbs(int assetId, int userId, string imagePath, string filename)
        {
            System.Drawing.Image imgOriginal = null;

            string contentRepository = "";
            if (KanevaGlobals.ContentServerPath != null)
            {
                contentRepository = KanevaGlobals.ContentServerPath;
            }
            else
            {
                m_logger.Error("CreatePhotoThumbs: ContentServerPath option not found");
                return;
            }

            string fileStoreHack = "";
            if (KanevaGlobals.FileStoreHack != "")
            {
                fileStoreHack = KanevaGlobals.FileStoreHack;
            }

            try
            {
                if (Configuration.UseAmazonS3Storage)
                {
                    string originalImage = (Configuration.ImageServer + "/" + imagePath).Replace("\\", "/");

                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(originalImage);
                    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    System.IO.Stream receiveStream = httpWebResponse.GetResponseStream();
                    imgOriginal = System.Drawing.Image.FromStream(receiveStream);
                }
                else
                {
                    //Create an image object from a file on disk
                    imgOriginal = System.Drawing.Image.FromFile(imagePath);
                }

                // Save the image at the specified sizes
                // Save the original path
                StoreUtility.UpdateAssetThumb(assetId, fileStoreHack + userId.ToString() + "/" + assetId.ToString() + "/" + filename, "image_full_path");

                // Small
                ImageHelper.SaveAssetThumbnail(imgOriginal, (int)Constants.PHOTO_THUMB_SMALL_HEIGHT, (int)Constants.PHOTO_THUMB_SMALL_WIDTH, "_sm", "thumbnail_small_path",
                    filename, imagePath, contentRepository, userId, assetId);

                // Medium
                ImageHelper.SaveAssetThumbnail(imgOriginal, (int)Constants.PHOTO_THUMB_MEDIUM_HEIGHT, (int)Constants.PHOTO_THUMB_MEDIUM_WIDTH, "_me", "thumbnail_medium_path",
                    filename, imagePath, contentRepository, userId, assetId);

                // Large
                ImageHelper.SaveAssetThumbnail(imgOriginal, (int)Constants.PHOTO_THUMB_LARGE_HEIGHT, (int)Constants.PHOTO_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
                    filename, imagePath, contentRepository, userId, assetId);

                // XLarge
                ImageHelper.SaveAssetThumbnail(imgOriginal, (int)Constants.PHOTO_THUMB_XLARGE_HEIGHT, (int)Constants.PHOTO_THUMB_XLARGE_WIDTH, "_xl", "thumbnail_xlarge_path",
                    filename, imagePath, contentRepository, userId, assetId);

                // Asset Details
                ImageHelper.SaveAssetThumbnail(imgOriginal, (int)Constants.PHOTO_THUMB_ASSETDETAILS_HEIGHT, (int)Constants.PHOTO_THUMB_ASSETDETAILS_WIDTH, "_ad", "thumbnail_assetdetails_path",
                    filename, imagePath, contentRepository, userId, assetId);
            }
            catch (Exception exc)
            {
                m_logger.Error("Error reading image path is " + imagePath, exc);

            }
            finally
            {
                if (imgOriginal != null)
                {
                    imgOriginal.Dispose();
                }
            }
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    }

}
