///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Text;

namespace KlausEnt.KEP.Kaneva.framework.widgets
{
	/// <summary>
	/// Summary description for PageUtility.
	/// </summary>
	public class PageUtility
	{
		/// <summary>
		/// set the last_update column in users table to be current date,
		/// note that <c>pageId</c> is the page_id column in layout_pages
		/// table
		/// </summary>
		/// <param name="pageId"></param>
		public static void PageUpdated(int pageId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			StringBuilder sb = new StringBuilder();
			sb.Append(" UPDATE communities ");
			sb.Append(" SET last_update = ").Append(dbUtility.GetCurrentDateFunction());
			sb.Append(" WHERE community_id = ");
			sb.Append(" ( ");
			sb.Append(" SELECT lp.channel_id ");
			sb.Append(" FROM layout_pages lp ");
			sb.Append(" WHERE lp.page_id = @page_id ");
			sb.Append(" ) ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@page_id", pageId);

			dbUtility.ExecuteNonQuery (sb.ToString(), parameters);
		}

		/// <summary>
		/// GetLayoutPageBodyAlignment
		/// </summary>
		/// <param name="page_id"></param>
		/// <returns></returns>
		public static double GetLayoutPageLeftPercentage ( int page_id )
		{
			string sqlSelect = "SELECT left_percentage " +
				" FROM layout_pages " +
				" WHERE page_id = @page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@page_id", page_id);

            DataRow drResult = KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);

			if (drResult != null)
			{
				return Convert.ToDouble( drResult ["left_percentage"]);
			}

			return 0;
		}

		// **********************************************************************************************
		// Page Management
		// **********************************************************************************************

		

		/// <summary>
		/// GetLayoutModuleGroups
		/// </summary>
		/// <param name="visibleFlag">bit mask, see Constants.eMODULE_GROUP_VISIBLE</param>
		/// <returns></returns>
		public static DataTable GetLayoutModuleGroupsNonCached (int visibleFlag, string filter)
		{
			string sqlSelect = "SELECT module_group_id, title, expanded " +
				" FROM layout_module_groups " +
				" WHERE visible & @visibleFlag > 0 " +
				filter +
				" ORDER BY sequence";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@visibleFlag", visibleFlag);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
		}

		/// <summary>
		/// return all the Layout Modules
		/// </summary>
		/// <returns></returns>
        public static DataTable GetLayoutModulesNonCached()
		{
			string sqlSelect = "SELECT module_id, title, description, icon_url " +
				" FROM layout_modules " +
				" WHERE visible > 0" + //at least one visible flag set
				" ORDER BY sequence";

			Hashtable parameters = new Hashtable ();
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
		}

		/// <summary>
		/// GetLayoutModules
		/// </summary>
		/// <param name="module_group_id"></param>
		/// <param name="visibleFlag">bit mask, see Constants.eMODULE_VISIBLE</param>
		/// <returns></returns>
		public static DataTable GetLayoutModulesNonCached ( int module_group_id , int visibleFlag)
		{
			string sqlSelect = "SELECT module_id, title, description, icon_url " +
				" FROM layout_modules " +
				" WHERE module_group_id = @module_group_id " + 
				" AND visible & @visibleFlag > 0 " + 
				" ORDER BY sequence";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_group_id", module_group_id);
			parameters.Add ("@visibleFlag", visibleFlag);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
		}

		/// <summary>
		/// GetLayoutZones
		/// </summary>
		/// <returns></returns>
		public static DataTable GetLayoutZonesNonCached ()
		{
			string sqlSelect = "SELECT zone_id, name, width, height " +
				" FROM layout_zones ";

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect);
		}

		/// <summary>
		/// GetLayoutPageModules
		/// </summary>
		/// <param name="page_id"></param>
		/// <param name="zone_id"></param>
		/// <returns></returns>
		public static DataTable GetLayoutPageModules ( int page_id, int zone_id )
		{
			string sqlSelect = "SELECT id, module_id " +
				" FROM layout_page_modules " +
				" WHERE page_id = @page_id AND zone_id = @zone_id " +
				" ORDER BY sequence";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@page_id", page_id);
			parameters.Add ("@zone_id", zone_id);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
		}

		/// <summary>
		/// return the module_id of a layout page module
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static int GetLayoutPageModule_ModuleID(int id)
		{
			string sqlSelect = " SELECT module_id " +
				" FROM layout_page_modules " +
				" WHERE id = @id";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);

            DataRow dr = KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);

			int retVal = -1;
			if(dr != null)
			{
				retVal = Int32.Parse(dr["module_id"].ToString());
			}

			return retVal;
		}

		/// <summary>
		/// UpdateLayoutPageModule
		/// </summary>
		/// <param name="page_module_id"></param>
		/// <param name="zone_id"></param>
		/// <param name="sequence"></param>
		/// <returns></returns>
		public static void UpdateLayoutPageModule ( int id, int zone_id, int sequence  )
		{
			string sqlSelect = "UPDATE layout_page_modules " +
				" SET zone_id = @zone_id, sequence = @sequence " +
				" WHERE id = @id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@zone_id", zone_id);
			parameters.Add ("@sequence", sequence);
			parameters.Add ("@id", id);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlSelect, parameters);
		}

		/// <summary>
		/// AddLayoutPageModule
		/// </summary>
		/// <param name="page_id"></param>
		/// <param name="module_id"></param>
		/// <param name="zone_id"></param>
		/// <param name="sequence"></param>
		/// <returns></returns>
		public static int AddLayoutPageModule ( int module_page_id, int page_id, int module_id, int zone_id, int sequence  )
		{
			string sqlSelect = "INSERT INTO layout_page_modules " +
				"(module_page_id, page_id, module_id, zone_id, sequence)" +
				" VALUES " +
				"(@module_page_id, @page_id, @module_id, @zone_id, @sequence)";

			Hashtable parameters = new Hashtable ();
			
			parameters.Add ("@module_page_id", module_page_id);
			parameters.Add ("@page_id", page_id);
			parameters.Add ("@module_id", module_id);
			parameters.Add ("@zone_id", zone_id);
			parameters.Add ("@sequence", sequence);

			int retVal = -1;

			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(sqlSelect, parameters, ref retVal);

			return retVal;
		}

		/// <summary>
		/// Delete a layout page module
		/// </summary>
		/// <param name="page_module_id"></param>
		/// <returns>int</returns>
		public static int DeleteLayoutPageModule (int id)
		{
			string sqlString = "DELETE FROM layout_page_modules where id = @id";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			return KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// UpdateLayoutPageLeftPercentage
		/// </summary>
		/// <param name="page_id"></param>
		/// <param name="left_percentage"></param>
		/// <returns></returns>
		public static void UpdateLayoutPageLeftPercentage ( int page_id, double left_percentage  )
		{
			string sqlSelect = "UPDATE layout_pages " +
				" SET left_percentage = @left_percentage " +
				" WHERE page_id = @page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@page_id", page_id);
			parameters.Add ("@left_percentage", left_percentage);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlSelect, parameters);

			PageUpdated(page_id);
		}

		/// <summary>
		/// return the page id of a user's default(home) page
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static int GetUserDefaultPageId(int userId)
		{
			string sqlSelect = " SELECT lp.page_id " +
				" FROM layout_pages lp " +
				" INNER JOIN communities_personal c ON c.community_id = lp.channel_id " +
				" WHERE lp.home_page = 1 AND c.creator_id = @user_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@user_id", userId);

            DataRow dr = KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);

			int retVal = -1;
			if(dr != null)
			{
				retVal = Int32.Parse(dr["page_id"].ToString());
			}

			return retVal;
		}

		/// <summary>
		/// return all layout modules for a give page 
		/// </summary>
		/// <param name="pageId"></param>
		/// <returns></returns>
		public static DataTable GetPageLayoutModules(int pageId)
		{
			string sqlSelect = " SELECT module_page_id, module_id, zone_id, id, sequence " +
				" FROM layout_page_modules " +
				" WHERE page_id = @page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@page_id", pageId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
			
		}

		/// <summary>
		/// GetLayoutPage
		/// </summary>
		/// <param name="page_id"></param>
		/// <returns></returns>
		public static DataRow GetLayoutPage ( int page_id )
		{
			string sqlSelect = "SELECT lp.home_page, name, template_id, channel_id, lp.access_id, pa.description AS access_desc, group_id, sequence, left_percentage " +
				" FROM layout_pages AS lp, layout_page_access AS pa " +
				" WHERE lp.page_id = @page_id AND lp.access_id = pa.access_id" +
				" ORDER BY sequence";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@page_id", page_id);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
		}

		/// <summary>
		/// GetLayoutPageAccesses
		/// </summary>
		/// <returns></returns>
		public static DataTable GetLayoutPageAccessesNonCached (  )
		{
			string sqlSelect = "SELECT access_id, description  " +
				" FROM layout_page_access ";

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect);
		}

		/// <summary>
		/// UpdateLayoutPageSequenceAndDefault
		/// </summary>
		/// <param name="page_id"></param>
		/// <param name="sequence"></param>
		/// <returns></returns>
		public static void UpdateLayoutPageSequenceAndDefault ( int page_id, int sequence, bool default_page )
		{
			string sqlSelect = "UPDATE layout_pages lp " +
				" SET sequence = @sequence, lp.home_page = @default " +
				" WHERE page_id = @page_id ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@page_id", page_id);
			parameters.Add ("@sequence", sequence);
			parameters.Add ("@default", default_page ? 1 : 0 );

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlSelect, parameters);

			PageUpdated(page_id);
		}

		/// <summary>
		/// UpdateLayoutPage
		/// </summary>
		/// <param name="page_id"></param>
		/// <param name="name"></param>
		/// <param name="group_id"></param>
		/// <param name="access_id"></param>
		/// <returns></returns>
		public static void UpdateLayoutPage ( int page_id, string name, int group_id, int access_id )
		{
			string sqlSelect = "UPDATE layout_pages lp " +
				" SET name = @name, group_id = @group_id, access_id = @access_id " +
				" WHERE page_id = @page_id ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@page_id", page_id);
			parameters.Add ("@name", name);

			if ( group_id != 0 )
				parameters.Add ("@group_id", group_id );
			else
				parameters.Add ("@group_id", DBNull.Value);

			parameters.Add ("@access_id", access_id );

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlSelect, parameters);

			PageUpdated(page_id);
		}

		/// <summary>
		/// Delete a layout page
		/// </summary>
		/// <param name="page_id"></param>
		/// <returns>int</returns>
		public static int DeleteLayoutPage (int page_id)
		{
			PageUpdated(page_id);

            string sqlString = "CALL delete_ProfilePage(@page_id);";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@page_id", page_id);
			return KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// Add a new layout page
		/// </summary>
		public static int AddLayoutPage (bool IsNewCommunity, int channel_id, string name, int group_id, 
			int access_id, int home_page)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			string sqlSelect = "";

            if (IsNewCommunity)
            {
                // Page number is 1
                sqlSelect = "INSERT INTO layout_pages " +
                "(name, group_id, access_id, home_page, channel_id, sequence) " +
                " VALUES " +
                " (@name, @group_id, @access_id, @home_page, @channel_id, 1) ";
            }
            else
            {
                sqlSelect = "INSERT INTO layout_pages " +
                "(name, group_id, access_id, home_page, channel_id, sequence) " +
                " SELECT " +
                "@name, @group_id, @access_id, @home_page, @channel_id, " +
                "IF ( MAX(sequence) IS NULL, 1, MAX(sequence) + 1) FROM layout_pages WHERE channel_id = @channel_id ";
            }
			
			
			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@name", name);
			
			if ( group_id != 0 )
				parameters.Add ("@group_id", group_id );
			else
				parameters.Add ("@group_id", DBNull.Value);

			if ( channel_id != 0 )
				parameters.Add ("@channel_id", channel_id );
			else
				parameters.Add ("@channel_id", DBNull.Value);

			parameters.Add ("@access_id", access_id);
			parameters.Add ("@home_page", home_page);

			int pageId = 0;
			dbUtility.ExecuteIdentityInsert (sqlSelect, parameters, ref pageId);

			PageUpdated(pageId);
			return pageId;
		}

		/// <summary>
		/// returns true if user is the page owner
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="pageId"></param>
		/// <returns></returns>
		public static bool IsUserPageOwner(int userId, int pageId)
		{
			string sqlSelect = "SELECT COUNT(*) " +
				" FROM layout_pages lp" +
				" INNER JOIN communities c ON c.community_id = lp.channel_id " +
				" WHERE lp.page_id = @page_id " +
				" AND c.creator_id = @user_id ";

			Hashtable parameters = new Hashtable ();            
			parameters.Add ("@page_id", pageId);
			parameters.Add ("@user_id", userId);
            int result = KanevaGlobals.GetDatabaseUtilityReadOnly2().ExecuteScalar(sqlSelect, parameters);
			return (result > 0);
		}

		/// <summary>
		/// returns true if user is the module owner
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="pageModuleId"></param>
		/// <returns></returns>
		public static bool IsUserModuleOwner(int userId, int pageModuleId)
		{
			string sqlSelect = " SELECT COUNT(*) " +
				" FROM layout_page_modules lpm " +
				" INNER JOIN layout_pages lp on lp.page_id = lpm.page_id " +
				" INNER JOIN communities c on c.community_id = lp.channel_id " +
				" WHERE lpm.id = @id AND c.creator_id = @user_id";

			Hashtable parameters = new Hashtable ();            
			parameters.Add ("@id", pageModuleId);
			parameters.Add ("@user_id", userId);
            int result = KanevaGlobals.GetDatabaseUtilityReadOnly2().ExecuteScalar(sqlSelect, parameters);
			return (result > 0);
		}
		/// <summary>
		/// GetLayoutPages for a channel
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static DataTable GetLayoutPages ( int channelId )
		{
			string sqlSelect = "SELECT page_id, lp.home_page, name, template_id, channel_id, " +
				" lp.access_id, pa.description AS access_desc, sequence, left_percentage, group_id " +
				" FROM layout_pages AS lp, layout_page_access AS pa " +
				" WHERE lp.channel_id = @channel_id AND lp.access_id = pa.access_id" +
				" ORDER BY sequence";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channel_id", channelId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
		}

		/// <summary>
		/// return the page id of a channel's default(home) page
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static int GetChannelDefaultPageId(int channelId)
		{
			string sqlSelect = " SELECT page_id " +
				" FROM layout_pages " +
				" WHERE home_page = 1 AND channel_id = @channel_id";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channel_id", channelId);

            DataRow dr = KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);

			int retVal = -1;
			if(dr != null)
			{
				retVal = Int32.Parse(dr["page_id"].ToString());
			}

			return retVal;
		}

		/// <summary>
		/// returns user id and name 
		/// </summary>
		/// <param name="pageId"></param>
		/// <returns></returns>
		public static DataRow GetChannelByPageId(int pageId)
		{
			string sqlSelect = " SELECT c.community_id, c.name " +
				" FROM communities c " +
				" INNER JOIN layout_pages lp ON lp.channel_id = c.community_id " +
				" WHERE lp.page_id = @page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@page_id", pageId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);

		}

		/// <summary>
		/// return true if the page is associated with the channel
		/// </summary>
		/// <param name="pageId"></param>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static bool IsPageInChannel(int pageId, int channelId)
		{
			string sqlSelect = "SELECT COUNT(*) " +
				" FROM layout_pages " +
				" WHERE channel_id = @channel_id " +
				" AND page_id = @page_id ";

			Hashtable parameters = new Hashtable ();            
			parameters.Add ("@page_id", pageId);
			parameters.Add ("@channel_id", channelId);
            int result = KanevaGlobals.GetDatabaseUtilityReadOnly2().ExecuteScalar(sqlSelect, parameters);
			return (result > 0);
		}

	}
}
