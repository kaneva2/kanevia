///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Collections;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for ReportUtility.
	/// </summary>
	public class ReportUtility
	{
		public ReportUtility()
		{
		}

      //  /// <summary>
      //  /// GetKPointEarnings
      //  /// </summary>
      //  public static Double GetKPointEarnings (DateTime dtStartDate, DateTime dtEndDate, string keiPointId)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = " SELECT COALESCE(SUM(ppta.amount),0) as amount " +
      //          " FROM purchase_point_transactions ppt, purchase_point_transaction_amounts ppta " +
      //          " WHERE ppt.point_transaction_id = ppta.point_transaction_id " +
      //          " AND ppt.transaction_status = @transactionStatus " +
      //          " AND ppta.kei_point_id = @keiPointId " +
      //          " AND ppt.transaction_date >= @startDate" +
      //          " AND ppt.transaction_date <= @endDate";
			
      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@transactionStatus", (int) Constants.eTRANSACTION_STATUS.VERIFIED);
      //      parameters.Add ("@keiPointId", keiPointId);
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      DataRow drResult = dbUtility.GetDataRow (sqlSelect, parameters, false);

      //      if (drResult != null)
      //      {
      //          return Convert.ToDouble (drResult ["amount"]);
      //      }
      //      return 0.0;
      //  }

      //  /// <summary>
      //  ///  call a sp to get WOK items purchase stats
      //  /// </summary>
      //  /// <param name="userId"></param>
      //  /// <returns></returns>
      //  public static DataTable GetWOKItemsPurchasedStats(DateTime startDate, DateTime endDate, string orderBy)
      //  {
      //      Hashtable parameters = new Hashtable();

      //      /*string sql = "CALL sp_WOK_items_purchase_stats( @startDate, @endDate);";
      //      parameters.Add("@startDate", startDate);
      //      parameters.Add("@endDate", endDate);*/

      //      DatabaseUtility dbUtility = GetDatabaseUtility();

      //      //attach primary key to columns and attach items table to columns to form SQL string
      //      string sql = "SELECT i.global_id, i.name, i.market_cost,SUM(wtld.quantity) as quantity, SUM(if (wtl.kei_point_id = 'KPOINT', wtld.quantity, 0)) as kpoint_quantity, " +
      //             "SUM(if (wtl.kei_point_id = 'GPOINT', wtld.quantity, 0)) as gpoint_quantity FROM kaneva.wok_transaction_log wtl " +
      //             "LEFT JOIN kaneva.wok_transaction_log_details wtld ON wtl.trans_id = wtld.trans_id INNER JOIN wok.items i " +
      //             "ON wtld.global_id = i.global_id WHERE market_cost > 0 AND wtl.created_date between @startDate AND @endDate " +
      //             "AND transaction_type IN (" + Constants.CASH_TT_BOUGHT_ITEM + ", " + Constants.CASH_TT_MISC + ", " + Constants.CASH_TT_BOUGHT_GIFT + ") " + 
      //             "GROUP BY wtld.global_id, i.market_cost";

      //      Hashtable theParams = new Hashtable();
      //      parameters.Add("@startDate", startDate);
      //      parameters.Add("@endDate", endDate);

      //      //limit by item id if one is provided
      //      if (orderBy != null && orderBy != "")
      //      {
      //          sql += " ORDER BY @orderBy ";
      //          theParams.Add("@orderBy", orderBy);
      //      }

      //      return GetDatabaseUtility().GetDataTable(sql, parameters);
      //  }

      //  /// <summary>
      //  /// GetCashEarnings
      //  /// </summary>
      //  public static DataRow GetCashEarnings (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = " SELECT COALESCE(SUM(ppt.amount_debited),0) as cash_total, COUNT(*) as count " +
      //          " FROM purchase_point_transactions ppt " +
      //          " WHERE ppt.transaction_status = @transactionStatus " +
      //          " AND ppt.transaction_date >= @startDate" +
      //          " AND ppt.transaction_date <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      parameters.Add ("@transactionStatus", (int) Constants.eTRANSACTION_STATUS.VERIFIED);
      //      return dbUtility.GetDataRow (sqlSelect, parameters, false);
      //  }

      //  /// <summary>
      //  /// GetKPointsSpent
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static DataRow GetKPointsSpent (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = "SELECT COALESCE(SUM(o.gross_point_amount),0) as amount, COUNT(*) as count, " +
      //          " COUNT(oi.featured_asset_id>0) as feat_count, COUNT(user_asset_subscription_id>0) as uas_count, " +
      //          " COUNT(oi.asset_id>0 AND user_asset_subscription_id IS NULL AND oi.featured_asset_id IS NULL) as a_count, COUNT(community_member_subscription_id) as cms_count " +
      //          " FROM orders o, order_items oi " +
      //          " WHERE o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED +
      //          " AND o.purchase_date >= @startDate" +
      //          " AND o.purchase_date <= @endDate" +
      //          " AND o.gross_point_amount > 0" +
      //          " AND o.order_id = oi.order_id ";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      DataRow drResult = dbUtility.GetDataRow (sqlSelect, parameters, false);

      //      // MySQL is haveing problems with this one (COUNT(oi.asset_id>0 AND user_asset_subscription_id IS NULL)) so do it separate.
      //      sqlSelect = "SELECT COUNT(*) as a_count " +
      //          " FROM orders o, order_items oi  " +
      //          " WHERE o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED +
      //          " AND o.purchase_date >= @startDate" +
      //          " AND o.purchase_date <= @endDate" +
      //          " AND o.gross_point_amount > 0 " +
      //          " AND o.order_id = oi.order_id " +
      //          " AND oi.asset_id > 0 " +
      //          " AND oi.user_asset_subscription_id IS NULL";
      //      DataRow drResult2 = dbUtility.GetDataRow (sqlSelect, parameters, false);

      //      drResult ["a_count"] = drResult2 ["a_count"];
      //      return drResult;
      //  }

      //  /// <summary>
      //  /// GetRoyalties
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static DataRow GetRoyalties (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = "SELECT COALESCE(SUM(oi.royalty_amount),0) as amount, COUNT(*) as count " +
      //          " FROM orders o, order_items oi " +
      //          " WHERE o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED +
      //          " AND oi.order_id = o.order_id" +
      //          " AND o.purchase_date >= @startDate" +
      //          " AND o.purchase_date <= @endDate" +
      //          " AND oi.royalty_amount > 0 ";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.GetDataRow (sqlSelect, parameters, false);
      //  }

      //  /// <summary>
      //  /// GetUserCreditTransactions
      //  /// </summary>
      //  /// <returns></returns>
      //  public static DataTable GetCreditTransactions (Double maxAmount,  string orderby, DateTime dtStartDate, DateTime dtEndDate)
      //  {		
      //      string sqlSelect = "SELECT SUM(ppta.amount) as kpoints, pm.name, pm.description, ppt.point_transaction_id, ppt.user_id, ppt.description as ppt_description, ppt.order_id, billing_information_id, " +
      //          " ppt.payment_method_id, ppt.amount_debited, ppt.amount_credited, ppt.transaction_status, ppt.transaction_date, " +
      //          " ppt.error_description, ppts.transaction_status_id ,ppts.description as status_description " +
      //          " FROM purchase_point_transactions ppt, purchase_point_transaction_amounts ppta, payment_methods pm, purchase_point_transactions_status ppts " +
      //          " WHERE ppt.point_transaction_id = ppta.point_transaction_id " +
      //          " AND pm.payment_method_id = ppt.payment_method_id " +
      //          " AND ppt.transaction_status = ppts.transaction_status_id " +
      //          " AND ppt.amount_debited > @maxAmount " +
      //          " AND ppt.transaction_date >= @startDate" +
      //          " AND ppt.transaction_date <= @endDate" +
      //          " GROUP BY pm.name, pm.description,ppt.point_transaction_id, ppt.user_id, ppt.description, ppt.order_id, billing_information_id, " +
      //          " ppt.payment_method_id, ppt.amount_debited, ppt.amount_credited, ppt.transaction_status, ppt.transaction_date, " +
      //          " ppt.error_description, ppts.transaction_status_id, ppts.description " +

      //          " order by " + orderby;

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@maxAmount", maxAmount);
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetTransactions
      //  /// </summary>
      //  /// <param name="numberOfTransactions"></param>
      //  /// <returns></returns>
      //  public static DataTable GetTransactions (int numberOfTransactions, DateTime dtStartDate, DateTime dtEndDate)
      //  {			
      //      string sqlSelect = "SELECT ppt.user_id, u.username, COUNT(*) as transactions, SUM(ppt.amount_debited) as total " +
      //              " FROM purchase_point_transactions ppt, users u " +
      //              " WHERE ppt.transaction_date >= @startDate" +
      //              " AND ppt.transaction_date <= @endDate" +
      //              " AND u.user_id = ppt.user_id " +
      //              " AND ppt.transaction_status = " + (int) Constants.eTRANSACTION_STATUS.VERIFIED + 
      //              " GROUP BY user_id " +
      //              " HAVING COUNT(*) > @numberOfTransactions";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@numberOfTransactions", numberOfTransactions);
      //      parameters.Add ("@startDate",  dtStartDate);
      //      parameters.Add ("@endDate",   dtEndDate);
      //      return GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
      //  }
		
      //  /// <summary>
      //  /// GetAssetActivity
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static DataRow GetAssetActivity (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      // Created
      //      string sqlSelect = "SELECT COUNT(*) as number_created, 0 as number_updated, 0 as number_deleted " +
      //          " FROM assets a " +
      //          " WHERE a.created_date >= @startDate" +
      //          " AND a.created_date <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      DataRow drResult = dbUtility.GetDataRow (sqlSelect, parameters, false);

      //      // Updated
      //      sqlSelect = "SELECT COUNT(*) as count " +
      //          " FROM assets a " +
      //          " WHERE a.last_updated_date >= @startDate" +
      //          " AND a.last_updated_date <= @endDate";

      //      int numberUpdated = dbUtility.ExecuteScalar (sqlSelect, parameters);

      //      // Deleted
      //      sqlSelect = "SELECT COUNT(*) as count " +
      //          " FROM assets a " +
      //          " WHERE a.last_updated_date >= @startDate" +
      //          " AND a.last_updated_date <= @endDate" +
      //          " AND a.status_id = " + (int) Constants.eASSET_STATUS.DELETED;

      //      int numberDeleted = dbUtility.ExecuteScalar (sqlSelect, parameters);

      //      drResult ["number_updated"] = numberUpdated;
      //      drResult ["number_deleted"] = numberDeleted;

      //      return drResult;
      //  }

      //  /// <summary>
      //  /// GetCommunityActivity
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static DataRow GetCommunityActivity (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      // Created
      //      string sqlSelect = "SELECT COUNT(*) as number_created, 0 as number_updated, 0 as number_deleted " +
      //          " FROM communities_public c " +
      //          " WHERE c.created_date >= @startDate" +
      //          " AND c.created_date <= @endDate" +
      //          " AND c.status_id != " + (int) Constants.eCOMMUNITY_STATUS.NEW;

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      DataRow drResult = dbUtility.GetDataRow (sqlSelect, parameters, false);

      //      // Updated
      //      sqlSelect = "SELECT COUNT(*) as count " +
      //          " FROM communities c " +
      //          " WHERE c.last_edit >= @startDate" +
      //          " AND c.last_edit <= @endDate" +
      //          " AND c.status_id != " + (int) Constants.eCOMMUNITY_STATUS.NEW;;

      //      int numberUpdated = dbUtility.ExecuteScalar (sqlSelect, parameters);

      //      // Deleted
      //      sqlSelect = "SELECT COUNT(*) as count " +
      //          " FROM communities c " +
      //          " WHERE c.last_edit >= @startDate" +
      //          " AND c.last_edit <= @endDate" +
      //          " AND c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.DELETED;

      //      int numberDeleted = dbUtility.ExecuteScalar (sqlSelect, parameters);

      //      drResult ["number_updated"] = numberUpdated;
      //      drResult ["number_deleted"] = numberDeleted;

      //      return drResult;
      //  }

      //  /// <summary>
      //  /// GetUserContributors
      //  /// </summary
      //  public static PagedDataTable GetUserContributors (DateTime dtStartDate, DateTime dtEndDate, string filter, string orderby, int pageNumber, int pageSize)
      //  {			
      //      Hashtable parameters = new Hashtable ();

      //      string selectList = " com.creator_id, com.creator_username ";

      //      string tableList = " communities_personal com ";

      //      string whereClause = " com.last_update >= @startDate" +
      //          " AND com.last_update <= @endDate ";

      //      // Filter it?
      //      if (filter.Length > 0)
      //      {
      //          whereClause += " AND " + filter;
      //      }

      //      parameters.Add ("@startDate", dtStartDate);
      //      parameters.Add ("@endDate", dtEndDate);
      //      return  GetDatabaseUtility ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
      //  }

      //  /// <summary>
      //  /// GetChannels
      //  /// </summary>
      //  public static PagedDataTable GetChannels (DateTime dtStartDate, DateTime dtEndDate, string orderby, int pageNumber, int pageSize)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = "c.name, c.name_no_spaces, c.community_id, c.created_date, c.last_edit, cs.number_of_members, " +
      //          " c.creator_id, c.creator_username, c.status_id, css.description";
				
      //      string tableList = "communities c " +
      //          " INNER JOIN channel_stats cs ON cs.channel_id = c.community_id " +
      //          " INNER JOIN community_status css ON c.status_id = css.status_id ";
				
      //      string whereClause = "c.status_id != " + (int) Constants.eCOMMUNITY_STATUS.NEW +
      //          " AND c.last_edit >= @startDate" +
      //          " AND c.last_edit <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate", dtStartDate);
      //      parameters.Add ("@endDate", dtEndDate);
      //      return dbUtility.GetPagedDataTable (sqlSelect, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
      //  }

      //  /// <summary>
      //  /// Get a list of items
      //  /// </summary>
      //  public static PagedDataTable GetItems (DateTime dtStartDate, DateTime dtEndDate, string orderby, int pageNumber, int pageSize)
      //  {
      //      Hashtable parameters = new Hashtable ();
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string selectList = "COALESCE(a.file_size,0) as file_size, a.publish_status_id, COALESCE(a.asset_id,0) as asset_id, COALESCE(a.asset_type_id,0) as asset_type_id, " +
      //          " a.name, a.created_date, a.last_updated_date, a.status_id, " +
      //          " u.user_id, u.username ";

      //      string tableList = "users u, assets a  ";

      //      string whereClaus = " a.owner_id = u.user_id " +
      //          StoreUtility.SQLCommon_GetOnlyActiveStatusAssetsSQL () +
      //          " AND a.last_updated_date >= @startDate" +
      //          " AND a.last_updated_date <= @endDate";

      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.GetPagedDataTable (selectList, tableList, whereClaus, orderby, parameters, pageNumber, pageSize);
      //  }

      //  /// <summary>
      //  /// GetNumberContentDownloaded
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static PagedDataTable GetContentDownloaded (DateTime dtStartDate, DateTime dtEndDate, string orderby, int pageNumber, int pageSize)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = "o.gross_point_amount, o.order_id, o.purchase_type, o.description, o.user_id, " +
      //          " o.purchase_date, o.download_end_date, " +
      //          " pts.transaction_status_id, pts.description as transaction_status_desc ";
				
      //      string tableList = " orders o, purchase_transaction_status pts ";
				
      //      string whereClause = "o.transaction_status_id = pts.transaction_status_id " + 
      //          " AND o.transaction_status_id IN (" + (int) Constants.eORDER_STATUS.COMPLETED + ")" +
      //          " AND (o.purchase_type = " + (int) Constants.ePURCHASE_TYPE.ASSET +
      //              " OR o.purchase_type = " + (int) Constants.ePURCHASE_TYPE.NOT_SET + 
      //              " OR o.purchase_type = " + (int) Constants.ePURCHASE_TYPE.GAME_SUBSCRIPTION + ")" +
      //          " AND o.purchase_date >= @startDate" +
      //          " AND o.purchase_date <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate", (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.GetPagedDataTable (sqlSelect, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
      //  }

      //  /// <summary>
      //  /// GetNumberContentPublished
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static PagedDataTable GetContentPublished (DateTime dtStartDate, DateTime dtEndDate, string filter, string orderby, int pageNumber, int pageSize)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = " a.owner_id as user_id, a.created_date, a.name ";
				
      //      string tableList = "assets a";
				
      //      string whereClause = " a.created_date >= @startDate" +
      //          " AND a.created_date <= @endDate" +
      //          StoreUtility.SQLCommon_GetOnlyActiveStatusAssetsSQL ();

      //      // Filter it?
      //      if (filter.Length > 0)
      //      {
      //          whereClause += " AND " + filter;
      //      }

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.GetPagedDataTable (sqlSelect, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
      //  }

      //  /// <summary>
      //  /// GetOutstandingPointBalance
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <returns></returns>
      //  public static Double GetOutstandingPointBalance (DateTime dtDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();
      //      Double pointsSold = 0.0;
      //      Double pointsSpent = 0.0;

      //      // Get the points Sold
      //      string sqlSelect = " SELECT COALESCE(SUM(ppta.amount),0) as amount " +
      //          " FROM purchase_point_transactions ppt, purchase_point_transaction_amounts ppta " +
      //          " WHERE ppt.point_transaction_id = ppta.point_transaction_id " +
      //          " AND ppt.transaction_status = @transactionStatus " +
      //          " AND (ppta.kei_point_id = @kPointId " +
      //              " OR ppta.kei_point_id = @mPointId) " +
      //          " AND ppt.transaction_date <= @dtDate";
			
      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@transactionStatus", (int) Constants.eTRANSACTION_STATUS.VERIFIED);
      //      parameters.Add ("@kPointId", Constants.CURR_KPOINT);
      //      parameters.Add ("@mPointId", Constants.CURR_MPOINT);
      //      parameters.Add ("@dtDate", dtDate);
      //      DataRow drResult = dbUtility.GetDataRow (sqlSelect, parameters, false);

      //      if (drResult != null)
      //      {
      //          pointsSold = Convert.ToDouble (drResult ["amount"]);
      //      }

      //      // Get the points spent
      //      sqlSelect = "SELECT COALESCE(SUM(o.gross_point_amount),0) as amount " +
      //          " FROM orders o, order_items oi " +
      //          " WHERE o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED +
      //          " AND o.purchase_date <= @dtDate" +
      //          " AND o.gross_point_amount > 0" +
      //          " AND o.order_id = oi.order_id ";

      //      parameters = new Hashtable ();			
      //      parameters.Add ("@dtDate", dtDate);
      //      drResult = dbUtility.GetDataRow (sqlSelect, parameters, false);

      //      if (drResult != null)
      //      {
      //          pointsSpent = Convert.ToDouble (drResult ["amount"]);
      //      }

      //      return pointsSold - pointsSpent;
      //  }

      //  /// <summary>
      //  /// GetCurrentUserBalances
      //  /// </summary>
      //  /// <returns></returns>
      //  public static DataRow GetCurrentUserBalances ()
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      // Created
      //      string sqlSelect = "SELECT COALESCE(SUM(balance),0) as balance, " +  dbUtility.GetCurrentDateFunction () + " as the_date " +
      //          " FROM user_balances " +
      //          " WHERE kei_point_id <> '" + Constants.CURR_DOLLAR + "'";

      //      return dbUtility.GetDataRow (sqlSelect, true);
      //  }

      //  /// <summary>
      //  /// GetNumberRaves
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static int GetNumberAssetRaves (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = sqlSelect = "SELECT COALESCE(COUNT(digg_id),0) as count " +
      //          " FROM asset_diggs " +
      //          " WHERE created_date >= @startDate" +
      //          " AND created_date <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetNumberChannelRaves
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static int GetNumberChannelRaves (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = sqlSelect = "SELECT COALESCE(SUM(rave_count),0) as count " +
      //          " FROM channel_diggs " +
      //          " WHERE created_date >= @startDate" +
      //          " AND created_date <= @endDate";

      //      Hashtable parameters = new Hashtable();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetNumberChannelRaves
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static int GetNumberPublicChannelRaves (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = sqlSelect = "SELECT COALESCE(SUM(rave_count),0) as count " +
      //          " FROM channel_diggs cd " +
      //          " INNER JOIN communities_public cp " +
      //          " ON cd.channel_id = cp.community_id " +
      //          " WHERE cp.created_date >= @startDate" +
      //          " AND cp.created_date <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetNumberChannelRaves
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static int GetNumberPersonalChannelRaves (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = sqlSelect = "SELECT COALESCE(SUM(rave_count),0) as count " +
      //          " FROM channel_diggs cd " +
      //          " INNER JOIN communities_personal cp " +
      //          " ON cd.channel_id = cp.community_id " +
      //          " WHERE cd.created_date >= @startDate" +
      //          " AND cd.created_date <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetWOKHangoutRaves
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static int GetWOKHangoutRaves (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();
      //      string dbNameForGame = KanevaGlobals.DbNameKaneva;

      //      string sqlSelect = sqlSelect = "SELECT COALESCE(SUM(rave_count),0) as count " +
      //          " FROM " + dbNameForGame + ".channel_diggs cd " +
      //          " INNER JOIN " + dbNameForGame + ".communities c " +
      //          " ON cd.channel_id = c.community_id AND c.community_type_id = 2 " +
      //          " WHERE cd.created_date >= @startDate" +
      //          " AND cd.created_date <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate", (dtStartDate));
      //      parameters.Add ("@endDate", (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetWOKApartmentRaves
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static int GetWOKApartmentRaves (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();
      //      string dbNameForGame = KanevaGlobals.DbNameKaneva;

      //      string sqlSelect = sqlSelect = "SELECT COALESCE(SUM(rave_count),0) as count " +
      //          " FROM " + dbNameForGame + ".channel_diggs cd " +
      //          " INNER JOIN " + dbNameForGame + ".communities c " +
      //          " ON cd.channel_id = c.community_id AND c.community_type_id = 5 " +
      //          " WHERE cd.created_date >= @startDate" +
      //          " AND cd.created_date <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }



      //  /// <summary>
      //  /// GetBlogActivity
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static int GetBlogActivity (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = sqlSelect = "SELECT COALESCE(COUNT(blog_id),0) as count " +
      //          " FROM blogs b " +
      //          " WHERE b.status_id = " + (int) Constants.eFORUM_STATUS.ACTIVE +
      //          " AND created_date >= @startDate" +
      //          " AND created_date <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetNumberComments
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static int GetNumberComments (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = sqlSelect = "SELECT COALESCE(COUNT(comment_id),0) as count " +
      //          " FROM comments c " +
      //          " WHERE created_date >= @startDate" +
      //          " AND created_date <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate", (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetFriendsRequested
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static int GetFriendsRequested (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = sqlSelect = "SELECT COALESCE(COUNT(friend_id),0) as count " +
      //          " FROM friend_requests f " +
      //          " WHERE request_date >= @startDate" +
      //          " AND request_date <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetFriendsAccepted
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static int GetFriendsAccepted (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = sqlSelect = "SELECT COALESCE(COUNT(friend_id),0) as count " +
      //          " FROM friends f " +
      //          " WHERE glued_date >= @startDate" +
      //          " AND glued_date <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate", (dtStartDate));
      //      parameters.Add ("@endDate", (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetConnectedMedia
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static int GetConnectedMedia (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = sqlSelect = "SELECT COALESCE(COUNT(asset_channel_id),0) as count " +
      //          " FROM asset_channels " +
      //          " WHERE created_date >= @startDate" +
      //          " AND created_date <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

		
      //  /// <summary>
      //  /// GetCommunityCount
      //  /// </summary>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static int GetCommunityCount (DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = sqlSelect = " SELECT COALESCE(community_count, 0) AS count " +
      //          " FROM report_kaneva.snapshot_communities " +
      //          " WHERE created_date=@endDate; ";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetCommunityCount
      //  /// </summary>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static int GetProfileCount (DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = sqlSelect = " SELECT COALESCE(profile_count, 0) AS count " +
      //          " FROM report_kaneva.snapshot_profiles " +
      //          " WHERE created_date=@endDate; ";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }


      //  /// <summary>
      //  /// GetNumberEditorDownloads
      //  /// </summary>
      //  /// <param name="dtStartDate"></param>
      //  /// <param name="dtEndDate"></param>
      //  /// <returns></returns>
      //  public static int GetNumberEditorDownloads (DateTime dtStartDate, string editorAssetIds, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = "SELECT COALESCE(COUNT(DISTINCT(user_id)),0) as count " +
      //              " FROM user_editor_downloads" +
      //              " WHERE downloaded_datetime >= @startDate" +
      //              " AND downloaded_datetime <= @endDate";

      //      Hashtable parameters = new Hashtable ();			
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetDownloads
      //  /// </summary>
      //  public static PagedDataTable GetDownloads (bool bIncludeFree, DateTime dtStartDate, DateTime dtEndDate, string orderBy, int pageNumber, int pageSize)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();
      //      Hashtable parameters = new Hashtable ();

      //      string sqlSelectList = "a.asset_id, a.name, o.purchase_date, o.gross_point_amount, COALESCE(u.username,'Anonymous') as username, " +
      //          " o.ip_address, COALESCE(u.user_id,0) as user_id, oi.number_of_streams, oi.number_of_downloads ";

      //      string sqlTableList = "orders o LEFT OUTER JOIN users u ON u.user_id = o.user_id, order_items oi, assets a";

      //      string sqlWhereClause = "o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED +
      //          " AND o.purchase_type IN (" + (int) Constants.ePURCHASE_TYPE.ASSET + "," + (int) Constants.ePURCHASE_TYPE.NOT_SET + "," + (int) Constants.ePURCHASE_TYPE.GAME_SUBSCRIPTION + ") " +
      //          " AND o.purchase_date >= @startDate" +
      //          " AND o.purchase_date <= @endDate" +
      //          " AND o.order_id = oi.order_id" +
      //          " AND oi.asset_id = a.asset_id";

      //      if (!bIncludeFree)
      //      {
      //          sqlWhereClause += " AND o.gross_point_amount > 0 ";
      //      }

      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderBy, parameters, pageNumber, pageSize);
      //  }

      //  /// <summary>
      //  /// GetGameLicenses
      //  /// </summary>
      //  public static PagedDataTable GetGameLicenses (string orderBy, int pageNumber, int pageSize)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();
      //      Hashtable parameters = new Hashtable ();

      //      string sqlSelectList = "ls.name, ls.max_server_users, a.name as asset_name, " +
      //          " gl.game_license_id, gl.asset_id, gl.license_type, gl.game_key, gl.server_name, " +
      //          " gl.purchase_date, gl.status_id, gl.patch_url, " +
      //          " gls.name as status_name, gls.description as status_description, "+ 
      //          " cge.last_ping_datetime, COALESCE(cge.engine_id,0) as engine_id ";

      //      string sqlTableList = "assets a, game_license gl LEFT OUTER JOIN current_game_engines cge ON gl.game_key  = cge.registration_key," +
      //          " license_subscription ls, game_license_status gls";

      //      string sqlWhereClause = "a.asset_id = gl.asset_id " +
      //          " AND gl.status_id = gls.status_id " +
      //          " AND gl.license_type = ls.license_subscription_id ";

      //      return dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderBy, parameters, pageNumber, pageSize);
      //  }

      //  /// <summary>
      //  /// GetPingHistory
      //  /// </summary>
      //  public static PagedDataTable GetPingHistory (int engineId, string orderBy, int pageNumber, int pageSize)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();
      //      Hashtable parameters = new Hashtable ();

      //      string sqlSelectList = "eh.player_count, eh.created_date, eh.ip_address ";

      //      string sqlTableList = "engine_history eh";

      //      string sqlWhereClause = "eh.engine_id = @engineId";

      //      parameters.Add ("@engineId", engineId);
      //      return dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderBy, parameters, pageNumber, pageSize);
      //  }

        /// <summary>
        /// GetAgeRange
        /// </summary>
        public static DataRow GetAgeRange(int startAge, int endAge)
        {
            DatabaseUtility dbUtility = GetDatabaseUtility();
            Hashtable parameters = new Hashtable();

            string sqlSelect = "SELECT COUNT(*) as count, SUM(IF(gender='M',1,0)) AS mCount, SUM(IF(gender='F',1,0)) AS fCount " +
                " FROM users " +
                " WHERE birth_date <= ADDDATE(NOW(), INTERVAL -" + startAge + " YEAR)" +
                " AND birth_date > ADDDATE(NOW(), INTERVAL -" + endAge + " YEAR)";

            return dbUtility.GetDataRow(sqlSelect, parameters, false);
        }

      //  /// <summary>
      //  /// GetSales
      //  /// </summary>
      //  public static PagedDataTable GetSales (DateTime dtStartDate, DateTime dtEndDate, string orderBy, int pageNumber, int pageSize)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();
      //      Hashtable parameters = new Hashtable ();

      //      string sqlQuery = "SELECT SUM(oi.number_of_downloads) as count, SUM(oi.number_of_streams) as number_of_streams, oi.asset_id, " +
      //          " SUM(oi.point_amount) as point_amount, SUM(oi.royalty_paid) as royalty_paid, SUM(oi.royalty_amount) as royalty_amount, " +
      //          " a.name, a.short_description, a.asset_type_id, a.amount, oi.point_amount" +
      //          " FROM order_items oi, orders o, assets a" +
      //          " WHERE oi.asset_id = a.asset_id " +
      //          " AND o.order_id = oi.order_id " +
      //          " AND o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED +
      //          " AND o.purchase_date >= @startDate" +
      //          " AND o.purchase_date <= @endDate" +
      //          " GROUP BY oi.asset_id ";

      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.GetPagedDataTableUnion (sqlQuery, orderBy, parameters, pageNumber, pageSize);     
      //  }

      //  /// <summary>
      //  /// GetPlatformFeedback
      //  /// </summary>
      //  public static PagedDataTable GetPlatformFeedback (string orderBy, int pageNumber, int pageSize)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();
      //      Hashtable parameters = new Hashtable ();

      //      string sqlSelectList = "feedback_id, subject, summary, IP, file_link, type, created_datetime ";

      //      string sqlTableList = "feedback";

      //      string sqlWhereClause = "feedback_id IS NOT NULL";

      //      return dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderBy, parameters, pageNumber, pageSize);
      //  }

        /// <summary>
        /// GetKPointSales
        /// </summary>
        /// <returns></returns>
        public static PagedDataTable GetKPointSales(DateTime dtStartDate, DateTime dtEndDate, string orderby, int pageNumber, int pageSize)
        {
            DatabaseUtility dbUtility = GetDatabaseUtility();
            Hashtable parameters = new Hashtable();

            string sqlSelect = "SELECT SUM(ppta.amount) as kpoints, pm.name, pm.description, ppt.point_transaction_id, ppt.user_id, ppt.description as ppt_description, ppt.order_id, billing_information_id, " +
                " ppt.payment_method_id, ppt.amount_debited, ppt.amount_credited, ppt.transaction_status, ppt.transaction_date, " +
                " ppt.error_description, ppts.transaction_status_id ,ppts.description as status_description, " +
                " com.name_no_spaces, com.name as com_name, com.thumbnail_small_path ";

            sqlSelect += " FROM purchase_point_transactions ppt INNER JOIN communities_personal com ON ppt.user_id = com.creator_id, " +
                " purchase_point_transaction_amounts ppta, payment_methods pm, purchase_point_transactions_status ppts";

            sqlSelect += " WHERE ppt.point_transaction_id = ppta.point_transaction_id " +
              " AND pm.payment_method_id = ppt.payment_method_id " +
              " AND ppt.transaction_status = ppts.transaction_status_id " +
              " AND ppt.transaction_date >= @startDate" +
              " AND ppt.transaction_date <= @endDate" +
                //Fixed to only show verified transactions with a positive amount_debited
                //" AND ppt.transaction_status <> " + (int) Constants.eTRANSACTION_STATUS.WAITING_VERIFICATION;
              " AND ppt.transaction_status = " + (int)Constants.eTRANSACTION_STATUS.VERIFIED +
              " AND ppt.amount_debited > 0 ";


            sqlSelect += " GROUP BY ppt.point_transaction_id ";

            parameters.Add("@startDate", (dtStartDate));
            parameters.Add("@endDate", (dtEndDate));

            return dbUtility.GetPagedDataTableUnion(sqlSelect, orderby, parameters, pageNumber, pageSize);
        }
		
      //  /// <summary>
      //  /// GetWokDownloads
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetWokDownloads (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = " SELECT COALESCE(COUNT(wok_download_id),0) as count " +
      //          " FROM wok_downloads " + 
      //          " WHERE created_date >= @startDate " +
      //          " AND created_date <= @endDate";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }
		
      //  /// <summary>
      //  /// GetWokNewPlaces
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetWokNewPlaces (int zoneType, DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(COUNT(channel_zone_id),0) as count " +
      //          " FROM " + dbNameForGame + ".channel_zones " + 
      //          " WHERE created_date >= @startDate " +
      //          " AND created_date <= @endDate " +
      //          " AND zone_type = @zoneType";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", (dtStartDate));
      //      parameters.Add ("@endDate", (dtEndDate));
      //      parameters.Add ("@zoneType", zoneType);
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetWokPeakConcurrentUsers
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetWokPeakConcurrentUsersCount (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;			  

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = " SELECT COALESCE(MAX(peak_count),0) AS count " +
      //          " FROM wok.peak_concurrent_players " + 
      //          " WHERE cur_date >= @startDate " +
      //          " AND cur_date <= @endDate " +
      //          " LIMIT 1";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", dbUtility.FormatDate (dtStartDate));
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }
		
      //  /// <summary>
      //  /// GetWokAvgHourlyConcurrentUsers
      //  /// </summary>
      //  /// <returns></returns>
      //  public static Double GetWokAvgHourlyConcurrentUsers (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;			  

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE((SELECT avg(max_logged_on_players) " +
      //          " FROM wok.summary_users_by_hour " + 
      //          " WHERE cur_date BETWEEN @startDate AND @endDate), 0) AS conc_users ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", dbUtility.FormatDate (dtStartDate));
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));

      //      DataRow drConcUsers = GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
      //      if (drConcUsers == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drConcUsers ["conc_users"]);
      //  }




      //  /// <summary>
      //  /// GetWokConcurrentUsersPerHour
      //  /// </summary>
      //  /// <returns></returns>
      //  public static DataTable GetWokAvgConcurrentUsersPerHour (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;			  

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT AVG(avg_logged_on_players) AS count, cur_hour " +
      //          " FROM report_kaneva.summary_users_by_hour " + 
      //          " WHERE cur_date >= @startDate " +
      //          " AND cur_date <= @endDate " +
      //          " GROUP BY cur_hour " +
      //          " ORDER BY cur_hour ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", dbUtility.FormatDate (dtStartDate));
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.GetDataTable (sqlSelect, parameters);
      //  }

        
      //  /// <summary>
      //  /// GetWokPeakConcurrentUsersPerHour
      //  /// </summary>
      //  /// <returns></returns>
      //  public static DataTable GetWokPeakConcurrentUsersPerHour(DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;			  

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT peak_count AS count, cur_hour " +
      //          " FROM wok.peak_concurrent_players " + 
      //          " WHERE cur_date >= @startDate " +
      //          " AND cur_date <= @endDate " +
      //          " ORDER BY cur_hour ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", dbUtility.FormatDate (dtStartDate));
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.GetDataTable (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetSpendingBreakdown
      //  /// </summary>
      //  /// <returns></returns>
      //  public static DataTable GetSpendingBreakdown (DateTime dtStartDate, DateTime dtEndDate, String creditType)
      //  {
      //      string sqlSelect = " SELECT SUM(wtl.transaction_amount) * -1 as debit_total,tt.trans_desc, wtl.kei_point_id " +
      //      " FROM wok_transaction_log wtl " +	
      //      " INNER JOIN transaction_type tt ON tt.transaction_type = wtl.transaction_type " +
      //      " WHERE created_date >= @startDate " +
      //      " AND created_date <= @endDate " +
      //      " and wtl.transaction_amount < 0 " +
      //      " and wtl.kei_point_id = @creditType AND wtl.transaction_type != 2 " +
      //      " group by wtl.kei_point_id, tt.transaction_type " + 
      //      " order by debit_total DESC " ;

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate",  dtStartDate);
      //      parameters.Add ("@endDate",  dtEndDate);
      //      parameters.Add ("@creditType", creditType);
      //      return GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetIncomeBreakdown
      //  /// </summary>
      //  /// <returns></returns>
      //  public static DataTable GetIncomeBreakdown (DateTime dtStartDate, DateTime dtEndDate, String creditType)
      //  {
      //      string sqlSelect = " SELECT SUM(wtl.transaction_amount) as debit_total,tt.trans_desc, wtl.kei_point_id " +
      //      " FROM wok_transaction_log wtl " +
      //      " INNER JOIN transaction_type tt ON tt.transaction_type = wtl.transaction_type " +
      //      " WHERE created_date >= @startDate " +
      //      " AND created_date <= @endDate " +
      //      " and wtl.transaction_amount >= 0 " +
      //      " and wtl.kei_point_id = @creditType AND wtl.transaction_type != 2 " +
      //      " group by wtl.kei_point_id, tt.transaction_type " +
      //      " order by debit_total DESC ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate",  dtStartDate);
      //      parameters.Add ("@endDate",  dtEndDate);
      //      parameters.Add ("@creditType", creditType);
      //      return GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
      //  }



      //  ///// <summary>
      //  ///// GetWokAvatarsCreated
      //  ///// </summary>
      //  ///// <returns></returns>
      //  //public static int GetWokAvatarsCreated (DateTime dtStartDate, DateTime dtEndDate)
      //  //{
      //  //    string dbNameForGame = KanevaGlobals.DbNameKGP;

      //  //    DatabaseUtility dbUtility = GetDatabaseUtility ();

      //  //    string sqlSelect = " SELECT COALESCE(sum(user_count),0) AS count " +
      //  //        " FROM report_kaneva.snapshot_user_avatar_gender " +
      //  //        " WHERE created_date between date(@startDate) and date(@endDate)";		

      //  //    Hashtable parameters = new Hashtable ();
      //  //    parameters.Add ("@startDate", dbUtility.FormatDate (dtStartDate));
      //  //    parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));			
      //  //    return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  //}

      //  /// <summary>
      //  /// GetWokAvatarsCreated
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetWokAvatarsCreated(DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility();

      //      string sqlSelect = "SELECT COUNT(player_id) as count FROM " + dbNameForGame + ".players WHERE created_date BETWEEN @startDate and @endDate";

      //      Hashtable parameters = new Hashtable();
      //      parameters.Add("@startDate", dtStartDate);
      //      parameters.Add("@endDate", dtEndDate);
      //      return dbUtility.ExecuteScalar(sqlSelect, parameters);
      //  }




      //  /// <summary>
      //  /// GetWokLoginCount
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetWokLoginCount (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(COUNT(game_login_id),0) as count " +
      //          " FROM developer.game_login_log " +
      //          " WHERE logon_time >= @startDate " +
      //          " AND logon_time <= @endDate AND game_id=3296";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate",  (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetWokUniqueLoginCount
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetWokUniqueLoginCount (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = "SELECT COALESCE(COUNT(DISTINCT(user_id)),0) AS count " +
      //          " FROM developer.game_login_log " +
      //          " WHERE logon_time >= @startDate " +
      //          " AND logon_time <= @endDate AND game_id=3296";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", (dtStartDate));
      //      parameters.Add ("@endDate", (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }
	
      //  public static int GetWokFirstTimeLoginCount (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();
		
      //      //commented out to see if greg's approach to queries works better
      //      //this greg query was optimized for speed and thus since his orginal one took 5.3s
      //      //string sqlSelect = " SELECT COALESCE(sum(user_count),0) AS count " +
      //      //    " FROM report_kaneva.snapshot_user_avatar_gender " +
      //      //    " WHERE created_date between date(@startDate) and date(@endDate)";

      //      //Hashtable parameters = new Hashtable ();
      //      //parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      //parameters.Add ("@startDate", dbUtility.FormatDate (dtStartDate));


      //      string sqlSelect = "SELECT COALESCE(COUNT(DISTINCT ll.user_id),0) AS count" +
      //      " FROM developer.game_login_log ll INNER JOIN " + dbNameForGame + ".game_users gu" +
      //      " ON ll.user_id = gu.kaneva_user_id WHERE (ll.logon_time >= @startDate AND ll.logon_time <= @endDate) AND" +
      //      " (gu.created_date >= @startDate and gu.created_date <= @endDate) AND game_id=3296";

      //      Hashtable parameters = new Hashtable();
      //      parameters.Add("@endDate", dtEndDate);
      //      parameters.Add("@startDate", dtStartDate);

      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }





		
      //  /// <summary>
      //  /// GetWokCreditsSpent
      //  /// </summary>
      //  /// <returns></returns>
      //  public static Double GetCreditsAdded (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      string sqlSelect = " SELECT COALESCE(SUM(transaction_amount),0) AS credits " +
      //          " FROM wok_transaction_log " +
      //          " WHERE transaction_amount > 0 " +
      //          " AND created_date BETWEEN @startDate and @endDate "+
      //          " AND kei_point_id = 'KPOINT' AND transaction_type != 2; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate",  dtStartDate);
      //      parameters.Add ("@endDate",  dtEndDate);
			
      //      DataRow drCA = GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
      //      if (drCA == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drCA ["credits"]);
      //  }

      //  /// <summary>
      //  /// GetWokCreditsSpent
      //  /// </summary>
      //  /// <returns></returns>
      //  public static Double GetRewardCreditsAdded (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      string sqlSelect = " SELECT COALESCE(SUM(transaction_amount),0) AS credits " +
      //          " FROM wok_transaction_log " +
      //          " WHERE transaction_amount > 0 " +
      //          " AND created_date BETWEEN @startDate and @endDate "+
      //          " AND kei_point_id = 'GPOINT'; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", dtStartDate);
      //      parameters.Add ("@endDate",  dtEndDate);
			
      //      DataRow drCA = GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
      //      if (drCA == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drCA ["credits"]);
      //  }
      //  /// <summary>
      //  /// GetWokCreditsSpent
      //  /// </summary>
      //  /// <returns></returns>
      //  public static Double GetCreditsSpent (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(SUM(spend_amount),0) AS count " +
      //          " FROM report_kaneva.summary_spent_credits_by_day_user " + 
      //          " WHERE record_date >= @startDate " +
      //          " AND record_date <= @endDate " +
      //          " AND kei_point_id='KPOINT'";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", dbUtility.FormatDate (dtStartDate));
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
			
      //      DataRow drCS = dbUtility.GetDataRow (sqlSelect, parameters, false);

      //      if (drCS == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drCS ["count"]);
      //  }

      //  /// <summary>
      //  /// GetWokCreditsSpent
      //  /// </summary>
      //  /// <returns></returns>
      //  public static Double GetRewardCreditsSpent (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(SUM(spend_amount),0) AS count " +
      //          " FROM report_kaneva.summary_spent_credits_by_day_user " + 
      //          " WHERE record_date >= @startDate " +
      //          " AND record_date <= @endDate " +
      //          " AND kei_point_id='GPOINT'";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", dbUtility.FormatDate (dtStartDate));
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
			
      //      DataRow drCS = dbUtility.GetDataRow (sqlSelect, parameters, false);

      //      if (drCS == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drCS ["count"]);
      //  }
		
		
      //  /// <summary>
      //  /// GetTotalWokEconomy
      //  /// </summary>
      //  /// <returns></returns>
      //  public static Double GetTotalWokEconomy (DateTime dtStartDate, DateTime dtEndDate, String creditType)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT balance AS total_wok_economy " +
      //          " FROM report_kaneva.snapshot_user_balances " +
      //          " WHERE created_date<=@endDate and kei_point_id=@creditType ORDER BY created_date DESC LIMIT 1; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      parameters.Add ("@creditType", creditType);
      //      DataRow drCS = dbUtility.GetDataRow (sqlSelect, parameters, false);
      //      if (drCS == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drCS ["total_wok_economy"]);
      //  }
		
		
      //  /// <summary>
      //  /// GetTotalRevenue			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static Double GetTotalRevenue (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      string sqlSelect = " SELECT COALESCE(SUM(ppt.amount_debited), 0) AS amount " +
      //          " FROM purchase_point_transactions ppt, purchase_point_transaction_amounts ppa " + 
      //          " WHERE ppt.point_transaction_id = ppa.point_transaction_id " +
      //          " AND ppt.transaction_status = 3 " +
      //          " AND ppa.kei_point_id = 'KPOINT' " +
      //          " AND transaction_date BETWEEN @startDate AND @endDate ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", dtStartDate);
      //      parameters.Add ("@endDate",  dtEndDate);
			
      //      DataRow drTotRev = GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
      //      if (drTotRev == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drTotRev ["amount"]);
      //  }

      //  /// <summary>
      //  /// GetRevenueBreakdown		   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static DataTable GetRevenueBreakdown(DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility();
            
      //      string sqlSelect = "SELECT sum(amount_debited) AS amount, pm.name " +
      //          " FROM purchase_point_transactions ppt " +
      //          " INNER JOIN kaneva.purchase_point_transaction_amounts ppta on ppta.point_transaction_id = ppt.point_transaction_id " +
      //          " INNER JOIN kaneva.payment_methods pm on ppt.payment_method_id = pm.payment_method_id " +
      //          " WHERE ppt.transaction_status = 3  " +
      //          " AND ppta.kei_point_id = 'KPOINT'  " +
      //          " AND amount_debited > 0 " +
      //          " AND transaction_date BETWEEN @startDate AND @endDate " +
      //          " GROUP BY ppt.payment_method_id; ";

      //      Hashtable parameters = new Hashtable();
      //      parameters.Add("@startDate", dbUtility.FormatDate (dtStartDate));
      //      parameters.Add("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return GetDatabaseUtility().GetDataTable(sqlSelect, parameters);
      //  }



      //  /// <summary>
      //  /// GetCreditPrice			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static Double GetCreditPrice (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      //modified during KPI hot fixes
      //      //string sqlSelect = " SELECT COALESCE(SUM(ppa.amount)/SUM(po.value_of_credits), 0)  AS value " +
      //      //    " FROM purchase_point_transactions ppt, purchase_point_transaction_amounts ppa, promotional_offers po " + 
      //      //    " WHERE ppt.point_transaction_id = ppa.point_transaction_id " +
      //      //    " AND ppt.point_bucket_id = po.promotion_id " +
      //      //    " AND ppt.transaction_status = 3 " +
      //      //    " AND ppa.kei_point_id = 'KPOINT' " +
      //      //    " AND transaction_date BETWEEN @startDate AND @endDate ";

      //      string sqlSelect = " SELECT COALESCE(SUM(ppa.amount)/SUM(ppt.amount_debited), 0) AS value FROM purchase_point_transactions ppt" +
      //          " INNER JOIN purchase_point_transaction_amounts ppa ON ppt.point_transaction_id = ppa.point_transaction_id" + 
      //          " WHERE ppt.transaction_status = 3 AND ppa.kei_point_id = 'KPOINT' AND ppa.amount > 0" + 
      //          " AND transaction_date BETWEEN @startDate AND @endDate ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", dtStartDate);
      //      parameters.Add ("@endDate",  dtEndDate);
			
      //      DataRow drTotRev = GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
      //      if (drTotRev == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drTotRev ["value"]);
      //  }



      //  /// <summary>
      //  /// GetNumberofPayers			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetNumberofPayers (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COUNT(distinct user_id) AS user_count " +
      //          " FROM report_kaneva.summary_ppt_by_day " +
      //          " WHERE dt_created BETWEEN @startDate AND @endDate ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", dbUtility.FormatDate (dtStartDate));
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetNumberofPayers			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetFirstTimePayerCount (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(COUNT(user_id), 0) AS count " +
      //          " FROM first_time_transactions " +
      //          " WHERE dt_created BETWEEN @startDate AND @endDate ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", (dtStartDate));
      //      parameters.Add ("@endDate",  (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetNumberofTransactions			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetNumberTransactions (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = " SELECT COALESCE(SUM(verified_transaction_count),0) AS count " +
      //          " FROM report_kaneva.summary_transactions_by_day " +
      //          " WHERE transaction_date BETWEEN @startDate AND @endDate ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add("@startDate", dbUtility.FormatDate(dtStartDate));
      //      parameters.Add("@endDate", dbUtility.FormatDate(dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }


      //  /// <summary>
      //  /// GetNumberTransactionsMav			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static double GetNumberTransactionsMav ( DateTime dtEndDate, int interval)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = " SELECT COALESCE(sum(verified_transaction_count)/ @interval, 0) as count " +
      //          " FROM report_kaneva.summary_transactions_by_day " +
      //          " WHERE transaction_date between DATE_SUB(@endDate, INTERVAL @interval DAY) and @endDate ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate(dtEndDate));
      //      parameters.Add ("@interval", interval);
      //      DataRow drTotRev = GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
      //      if (drTotRev == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drTotRev ["count"]);
      //  }

      //  /// <summary>
      //  /// GetValueTransactionMav			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static double GetValueTransactionMav ( DateTime dtEndDate, int interval)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = " SELECT COALESCE(sum(purchase_amount_debited)/sum(verified_transaction_count),0) AS count " +
      //          " FROM report_kaneva.summary_transactions_by_day " +
      //          " WHERE transaction_date between DATE_SUB(@endDate, INTERVAL @interval DAY) and @endDate ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate(dtEndDate));
      //      parameters.Add ("@interval", interval);
      //      DataRow drTotRev = GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
      //      if (drTotRev == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drTotRev ["count"]);
      //  }


      //  /// <summary>
      //  /// GetValueTrans   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static double GetValueTrans (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = "SELECT COALESCE(sum(purchase_amount_debited)/sum(verified_transaction_count),0) AS count " +
      //          " FROM report_kaneva.summary_transactions_by_day " +
      //          " WHERE transaction_date between @startDate and @endDate ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate(dtEndDate));
      //      parameters.Add ("@startDate", dbUtility.FormatDate (dtStartDate));
      //      DataRow drTotRev = GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
      //      if (drTotRev == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drTotRev ["count"]);
      //  }
		



		
      //  /// <summary>
      //  /// GetValuePayerMav			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static double GetValuePayerMav ( DateTime dtEndDate, int interval)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = " select COALESCE(sum(total)/count( distinct user_id),0) AS count " +
      //          " from report_kaneva.summary_ppt_by_day " +
      //          " WHERE dt_created between DATE_SUB(@endDate, INTERVAL @interval DAY) and @endDate ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate(dtEndDate));
      //      parameters.Add ("@interval", interval);
      //      DataRow drTotRev = GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
      //      if (drTotRev == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drTotRev ["count"]);
      //  }



      //  /// <summary>
      //  /// GetPayersByAmount
      //  /// </summary>
      //  /// <returns></returns>
      //  public static DataTable GetPayersByAmount (DateTime dtStartDate, DateTime dtEndDate)
      //  {			  
      //      string sqlSelect = "SELECT count(user_id) as numPayers, bucket FROM( " +
      //          " SELECT user_id, Truncate(sum(total), 0) as bucket FROM report_kaneva.summary_ppt_by_day " +
      //          " WHERE dt_created between date(@startDate) and date(@endDate) " +
      //          " GROUP BY user_id) as t1 " +
      //          " GROUP BY bucket " ; 

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate",  dtStartDate);
      //      parameters.Add ("@endDate",  dtEndDate);
      //      return GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
      //  }


      //  /// <summary>
      //  /// GetTransactionsByPayers
      //  /// </summary>
      //  /// <returns></returns>
      //  public static DataTable GetTransactionsByPayers (DateTime dtStartDate, DateTime dtEndDate)
      //  {			  
      //      string sqlSelect = "SELECT count(user_id) as numPayers, bucket FROM( " +
      //          " SELECT user_id, sum(transaction_count) as bucket FROM report_kaneva.summary_ppt_by_day " +
      //          " WHERE dt_created between date(@startDate) and date(@endDate) " +
      //          " GROUP BY user_id) as t1 " +
      //          " GROUP BY bucket " ;

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate",  dtStartDate);
      //      parameters.Add ("@endDate",  dtEndDate);
      //      return GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetGiftCardRedemptions
      //  /// </summary>
      //  /// <returns></returns>
      //  public static DataTable GetGiftCardRedemptions(DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string sqlSelect = "SELECT count(*) as count, description FROM purchase_point_transactions " +
      //          " WHERE gift_card_id != 0 and date(transaction_date) between date(@startDate) and date(@endDate) " +
      //          " GROUP BY gift_card_id ";                
      //      Hashtable parameters = new Hashtable();
      //      parameters.Add("@startDate", dtStartDate);
      //      parameters.Add("@endDate", dtEndDate);
      //      return GetDatabaseUtility().GetDataTable(sqlSelect, parameters);
      //  }


      //  /// <summary>
      //  /// GetIncommGCSold			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetIncommGCSold(DateTime dtStartDate, DateTime dtEndDate, int amount)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility();

      //      string sqlSelect = " SELECT count(denomination) as count, sum(gsr.transactionamt) value FROM report_kaneva.gc_sales_reports gsr " +
      //                         " WHERE gsr.denomination = @amount " +
      //                         " and gsr.transactionamt > 0 " +
      //                         " and date(gsr.sales_date) between date(@startDate) and date(@endDate) ";

      //      Hashtable parameters = new Hashtable();
      //      parameters.Add("@startDate", dbUtility.FormatDate(dtStartDate));
      //      parameters.Add("@endDate", dbUtility.FormatDate(dtEndDate));
      //      parameters.Add("@amount", amount);
      //      return dbUtility.ExecuteScalar(sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetIncommGCReturned			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetIncommGCReturned(DateTime dtStartDate, DateTime dtEndDate, int amount)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility();

      //      string sqlSelect = " SELECT count(denomination) as count, sum(gsr.transactionamt) value FROM report_kaneva.gc_sales_reports gsr " +
      //                         " WHERE gsr.denomination = @amount " +
      //                         " and gsr.transactionamt < 0 " +
      //                         " and date(gsr.sales_date) between date(@startDate) and date(@endDate) ";

      //      Hashtable parameters = new Hashtable();
      //      parameters.Add("@startDate", dbUtility.FormatDate(dtStartDate));
      //      parameters.Add("@endDate", dbUtility.FormatDate(dtEndDate));
      //      parameters.Add("@amount", amount);
      //      return dbUtility.ExecuteScalar(sqlSelect, parameters);
      //  }
        


      //  /// <summary>
      //  /// GetIncommGCredeemed			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetIncommGCredeemed(DateTime dtStartDate, DateTime dtEndDate, int amount)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility();

      //      string sqlSelect = " SELECT count(card_amount) as count, sum(grr.card_amount) value FROM report_kaneva.gc_redemption_reports grr " +
      //                         " WHERE grr.card_amount = @amount " +
      //                         " and date(grr.redemption_date) between date(@startDate) and date(@endDate) ";

      //      Hashtable parameters = new Hashtable();
      //      parameters.Add("@startDate", dbUtility.FormatDate(dtStartDate));
      //      parameters.Add("@endDate", dbUtility.FormatDate(dtEndDate));
      //      parameters.Add("@amount", amount);
      //      return dbUtility.ExecuteScalar(sqlSelect, parameters);
      //  }


        


      //  /// <summary>
      //  /// GetIncommGiftCardByState
      //  /// </summary>
      //  /// <returns></returns>
      //  public static DataTable GetIncommGiftCardByState(DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string sqlSelect = "Select *, num_sold-num_returned as net_sold from " +
      //              " ( SELECT gsr.state,count(gsr.state) as num_sold, IFNULL(num_returned,0) as num_returned FROM report_kaneva.gc_sales_reports gsr " +
      //              "  Left Join " +
      //              "   (SELECT state,count(state) as num_returned FROM report_kaneva.gc_sales_reports gsr WHERE gsr.transactionamt < 0 group by state) X " +
      //              "  ON gsr.state = X.state " +
      //              "  where transactionamt > 0 " +
      //              "  and date(sales_date)  between date(@startDate) and date(@endDate)  " +
      //              "  group by gsr.state " +
      //              " ) X order by net_sold DESC; " ;

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate",  dtStartDate);
      //      parameters.Add ("@endDate",  dtEndDate);
      //      return GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
      //  }





      //  /// <summary>
      //  /// GetTransPayerMav	   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static double GetTransPayerMav ( DateTime dtEndDate, int interval)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " select COALESCE(count(transaction_id)/count( distinct user_id),0) AS count " +
      //          " from purchase_point_transactions ppt " +
      //          " WHERE transaction_status="+ (int) Constants.eTRANSACTION_STATUS.VERIFIED + " and date(ppt.transaction_date) between DATE_SUB(@endDate, INTERVAL @interval DAY) and @endDate ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate(dtEndDate));
      //      parameters.Add ("@interval", interval);
      //      DataRow drTotRev = GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
      //      if (drTotRev == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drTotRev ["count"]);
      //  }

      //  /// <summary>
      //  /// GetTransPayer	   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static double GetTransPayer (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " select COALESCE(count(transaction_id)/ count( distinct user_id),0) AS count " +
      //          " from purchase_point_transactions ppt " +
      //          " WHERE transaction_status="+ (int) Constants.eTRANSACTION_STATUS.VERIFIED + " and date(ppt.transaction_date) between @startDate and @endDate ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate(dtEndDate));
      //      parameters.Add ("@startDate", dbUtility.FormatDate (dtStartDate));
      //      DataRow drTotRev = GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
      //      if (drTotRev == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drTotRev ["count"]);
      //  }


      //  /// <summary>
      //  /// Get Total WoK Users			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetTotalWokUsers (DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(SUM(user_count), 0) AS count " +
      //          " FROM report_kaneva.snapshot_user_gender " +
      //          " WHERE created_date=@endDate; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetTotalSiteUsers			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetTotalSiteUsers (DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(member_count, 0) AS count " +
      //          " FROM report_kaneva.snapshot_site_members " +
      //          " WHERE created_date=@endDate; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetWokFirstTimeLoginCount			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetPlayerCountByGender (bool isMale, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(user_count, 0) AS count " +
      //          " FROM report_kaneva.snapshot_user_gender " +
      //          " WHERE created_date=@endDate " +
      //          " AND gender='" + (isMale ? "M" : "F") + "'; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetWokCreditsSpent
      //  /// </summary>
      //  /// <returns></returns>
      //  public static Double GetAvgMinutesInWorldPerPlayer (DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		
								
      //      string sqlSelect = " SELECT COALESCE(play_time_avg, 0) AS avg_time " +
      //          " FROM report_kaneva.snapshot_wok_user_average " +
      //          " WHERE created_date=@endDate; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
			
      //      DataRow drAvgMins = dbUtility.GetDataRow (sqlSelect, parameters, false);
      //      if (drAvgMins == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drAvgMins ["avg_time"]);
      //  }

      //  /// <summary>
      //  /// GetWokCreditsSpent
      //  /// </summary>
      //  /// <returns></returns>
      //  public static Double GetAvgMinutesInWorldPerPlayer (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(AVG(play_time_avg), 0) AS avg_time " +
      //          " FROM report_kaneva.snapshot_wok_user_average " +
      //          " WHERE created_date between @startDate and @endDate; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", dbUtility.FormatDate (dtStartDate));
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
			
      //      DataRow drAvgMins = dbUtility.GetDataRow (sqlSelect, parameters, false);
      //      if (drAvgMins == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drAvgMins ["avg_time"]);
      //  }

		
      //  /// <summary>
      //  /// GetAvgDollarsSpentPerMember			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static Double GetAvgDollarsSpentPerMember (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      string sqlSelect = " SELECT COALESCE((SELECT SUM(ppt.amount_debited) " +
      //          " FROM purchase_point_transactions ppt, purchase_point_transaction_amounts ppa " + 
      //          " WHERE ppt.point_transaction_id = ppa.point_transaction_id " +
      //          " AND ppt.transaction_status = 3 " +
      //          " AND ppa.kei_point_id = 'KPOINT' " +															 
      //          " AND transaction_date BETWEEN @startDate AND @endDate)/ " +
      //          " (SELECT COUNT(DISTINCT user_id) FROM purchase_point_transactions ppt WHERE transaction_date BETWEEN @startDate AND @endDate " +
      //          " AND ppt.transaction_status = 3 ), 0) " +
      //          " AS avg_spent ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate",  dtStartDate);
      //      parameters.Add ("@endDate", dtEndDate);
      //      DataRow drAvgDolSpent = GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
      //      if (drAvgDolSpent == null)
      //      {
      //          return 0.0;
      //      }

      //      return Convert.ToDouble (drAvgDolSpent ["avg_spent"]);
      //  }

      //  /// <summary>
      //  /// GetActiveWokMemberCount			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetActiveWokMemberCount (DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(user_count, 0) AS count " +
      //          " FROM report_kaneva.snapshot_user_active " +
      //          " WHERE created_date=@endDate; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }


      //  /// <summary>
      //  /// GetActiveWokMemberCount			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetAvgActiveWokMemberCount (DateTime dtEndDate, int timeSpent)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		


      //      /* 
      //       * Old method
      //       * 
			
      //          string sqlSelect = " SELECT COALESCE(avg(user_count), 0) AS count " +
      //          " FROM report_kaneva.snapshot_user_active " +
      //          " WHERE created_date between date_sub(@endDate, interval 30 day) and @endDate; ";
      //       * 
      //       */

      //      string sqlSelect = " SELECT count(gu.user_id) AS player_count " + 
      //      " FROM wok.game_users gu " +
      //      " WHERE time_played >= @timeSpent " + 
      //      " AND last_logon >= date_sub(date(now()), interval 30 day); ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add("@endDate", dbUtility.FormatDate (dtEndDate));
      //      parameters.Add("@timeSpent", timeSpent);

      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetActiveWokSpenderCount			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetActiveWokSpenderCount (DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT active_spender_counts AS count " +
      //          " FROM report_kaneva.spender_activity_counts " +
      //          " WHERE created_date = @endDate ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add("@endDate", dbUtility.FormatDate(dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetActiveWokMemberCount			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetAvgActiveWokSpenders (DateTime dtEndDate)
      //  {			
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      /* 
      //       * Old method
      //       * 
             
      //          string sqlSelect = " SELECT COALESCE(avg(active_spender_counts), 0) AS count " +
      //          " FROM report_kaneva.spender_activity_counts " +
      //          " WHERE created_date between date_sub(@endDate, interval 30 day) and @endDate; ";
      //      */

      //      string sqlSelect = " SELECT COALESCE(count(distinct(user_id)), 0) as count " +
      //      " FROM purchase_point_transactions " +
      //      " WHERE date(transaction_date) BETWEEN date_sub(@endDate, interval 30 day) AND @endDate " +
      //      " AND transaction_status = 3 " +
      //      " AND amount_debited >= 9.99; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetActiveWokMemberCount			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetAvgInactiveWokSpenders (DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(avg(inactive_spender_counts), 0) AS count " +
      //          " FROM report_kaneva.spender_activity_counts " +
      //          " WHERE created_date between date_sub(@endDate, interval 30 day) and @endDate; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }
		
      //  /// <summary>
      //  /// GetInactiveWokMemberCount			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetInactiveWokMemberCount (DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(user_count, 0) AS count " +
      //          " FROM report_kaneva.snapshot_user_inactive " +
      //          " WHERE created_date=@endDate; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetActiveWokMemberCount			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetAvgInactiveWokMemberCount (DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(avg(user_count), 0) AS count " +
      //          " FROM report_kaneva.snapshot_user_inactive " +
      //          " WHERE created_date between date_sub(@endDate, interval 30 day) and @endDate; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }
      //  /// <summary>
      //  /// GetInactiveWokSpenderCount			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetInactiveWokSpenderCount (DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT inactive_spender_counts AS count " +
      //          " FROM report_kaneva.spender_activity_counts " +
      //          " WHERE created_date = @endDate ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetAccessPasses		   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetAccessPasses(DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility();

      //      string sqlSelect = "SELECT COALESCE(count(point_transaction_id),0) AS count FROM purchase_point_transactions ppt " + 
      //      " INNER JOIN promotional_offers po on ppt.point_bucket_id = po.promotion_id " +
      //      " WHERE (sku = 11001 or sku = 11002) " +
      //      " and transaction_status = '3' " +
      //      " and date(transaction_date) between @startDate and @endDate ";

      //      Hashtable parameters = new Hashtable();
      //      parameters.Add("@startDate", dbUtility.FormatDate(dtStartDate));
      //      parameters.Add("@endDate", dbUtility.FormatDate(dtEndDate));      
      //      return dbUtility.ExecuteScalar(sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetAccessPassOnly	   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetAccessPassOnly(DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility();

      //      string sqlSelect = "SELECT COALESCE(count(point_transaction_id),0) AS count FROM purchase_point_transactions ppt " +
      //      " INNER JOIN promotional_offers po on ppt.point_bucket_id = po.promotion_id " +
      //      " WHERE sku = 11002 " +
      //      " and transaction_status = '3' " +
      //      " and date(transaction_date) between @startDate and @endDate ";

      //      Hashtable parameters = new Hashtable();
      //      parameters.Add("@startDate", dbUtility.FormatDate(dtStartDate));
      //      parameters.Add("@endDate", dbUtility.FormatDate(dtEndDate));
      //      return dbUtility.ExecuteScalar(sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetKPointSales
      //  /// </summary>
      //  /// <returns></returns>
      //  public static PagedDataTable GetDeclinedInvites (DateTime dtStartDate, DateTime dtEndDate, string orderby, int pageNumber, int pageSize)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();
      //      Hashtable parameters = new Hashtable ();

      //      string sqlSelect = "SELECT i.invite_id, i.invited_date, id.decline_date, i.email, " +
      //          "idr.name, id.additional_information, idr.invite_decline_reason_id " +
      //          "FROM invite_declines id " +
      //          "INNER JOIN invites i ON i.invite_id = id.invite_id " +
      //          "INNER JOIN invite_decline_reasons idr ON idr.invite_decline_reason_id = id.invite_decline_reason_id " +
      //          "WHERE decline_date >= @startDate " +
      //          "AND decline_date <= @endDate";

      //      parameters.Add ("@startDate", (dtStartDate));
      //      parameters.Add ("@endDate", (dtEndDate));

      //      return dbUtility.GetPagedDataTableUnion (sqlSelect, orderby, parameters, pageNumber, pageSize);
      //  }

      //  public static int GetInvitesCount (DateTime dtStartDate, DateTime dtEndDate, string filter)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = "SELECT COALESCE(COUNT(invite_id), 0) AS count " +
      //          "FROM invites " +
      //          "WHERE invited_date >= @startDate " +
      //          "AND invited_date <= @endDate ";

      //      //append the where clause if a filter is provided
      //      if ((filter != null) && (filter != ""))
      //      {
      //          sqlSelect += "AND " + filter;
      //      }

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", (dtStartDate));
      //      parameters.Add ("@endDate", (dtEndDate));

      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  public static int GetInvitesAcceptedCount (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = "SELECT COALESCE(COUNT(i.invite_id), 0) AS count " + 
      //          "FROM invites i " + 
      //          "INNER JOIN users u on u.user_id = i.invited_user_id " +
      //          "WHERE (i.invite_status_id=3 OR i.invite_status_id=7) " +
      //          "AND u.signup_date >= @startDate " +
      //          "AND u.signup_date <= @endDate; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", (dtStartDate));
      //      parameters.Add ("@endDate", (dtEndDate));

      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  public static int GetInvitesOpenedCount (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = "SELECT COALESCE(COUNT(invite_id), 0) AS count " +
      //          "FROM invites " +
      //          "WHERE opened_invite=1 " +
      //          "AND invited_date >= @startDate " +
      //          "AND invited_date <= @endDate; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", (dtStartDate));
      //      parameters.Add ("@endDate", (dtEndDate));

      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  public static int GetInvitesJoinClickCount (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = "SELECT COALESCE(COUNT(invite_id), 0) AS count " +
      //          "FROM invites " +
      //          "WHERE clicked_accept_link=1 " +
      //          "AND invited_date >= @startDate " +
      //          "AND invited_date <= @endDate; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", (dtStartDate));
      //      parameters.Add ("@endDate", (dtEndDate));

      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  public static int GetNumberUsersInvitingFriends (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = "SELECT COALESCE(COUNT(DISTINCT(user_id)), 0) AS count " +
      //          "FROM invites " +
      //          "WHERE invited_date >= @startDate " +
      //          "AND invited_date <= @endDate ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@startDate", (dtStartDate));
      //      parameters.Add ("@endDate", (dtEndDate));

      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //      //DataRow dr = dbUtility.GetDataRow (sql, parameters, false);

      //      //if (dr != null && dr["count"] != null)
      //      //{
      //      //    return Convert.ToInt32 (dr["count"]);
      //      //}
      //      //else
      //      //{
      //      //    return 0;
      //      //}
      //  }

      //  public static int GetNewUserCount (DateTime dtStartDate, DateTime dtEndDate)
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();
      //      Hashtable parameters = new Hashtable ();

      //      string sqlSelect = "SELECT COALESCE(COUNT(user_id), 0) AS count " +
      //      " FROM users " +
      //      " WHERE signup_date >= @startDate " +
      //      " AND signup_date <= @endDate";

      //      parameters.Add ("@startDate", (dtStartDate));
      //      parameters.Add ("@endDate", (dtEndDate));
            
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }
        
      //  public static int GetActiveUserCount ()
      //  {
      //      DatabaseUtility dbUtility = GetDatabaseUtility ();

      //      string sqlSelect = "SELECT COALESCE(COUNT(u.user_id), 0) AS count " + 
      //      " FROM users u " +
      //      " INNER JOIN users_stats us ON us.user_id = u.user_id " +
      //      " WHERE u.last_login >= date_sub(date(now()), interval 180 day) " +
      //      " AND us.number_of_logins > 1 ";

      //      return dbUtility.ExecuteScalar (sqlSelect);
      // }

      //  /// <summary>
      //  /// GetTotalFurniturePlaced			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetTotalFurniturePlaced (DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(furniture_count, 0) AS count " +
      //          " FROM report_kaneva.snapshot_placed_furniture " +
      //          " WHERE created_date=@endDate; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

      //  /// <summary>
      //  /// GetTotalClothingPurchased			   
      //  /// </summary>
      //  /// <returns></returns>
      //  public static int GetTotalClothingPurchased (DateTime dtEndDate)
      //  {
      //      string dbNameForGame = KanevaGlobals.DbNameKGP;

      //      DatabaseUtility dbUtility = GetDatabaseUtility ();		

      //      string sqlSelect = " SELECT COALESCE(clothing_purchase_count, 0) AS count " +
      //          " FROM report_kaneva.snapshot_purchased_clothing " +
      //          " WHERE created_date=@endDate; ";

      //      Hashtable parameters = new Hashtable ();
      //      parameters.Add ("@endDate", dbUtility.FormatDate (dtEndDate));
      //      return dbUtility.ExecuteScalar (sqlSelect, parameters);
      //  }

        public static DataTable GetViralCoefficientData(int gameId, DateTime startDate, DateTime endDate)
        {
            Hashtable parameters = new Hashtable();

            DatabaseUtility dbUtility = GetDatabaseUtility();

            string sql =
                "SELECT  CONCAT(DATE_FORMAT(STR_TO_DATE(CONCAT(senders_count.week_id, ' Sunday'), '%X%V %W'),'%Y/%m/%d'),' - ',DATE_FORMAT(STR_TO_DATE(CONCAT(senders_count.week_id, ' Saturday'), '%X%V %W'),'%Y/%m/%d')) as date_range, " +
                "        senders_count.week_id, " +
                "        senders_count.game_id, " +
                "        senders_count.total_sent, " +
                "        (distinct_sender_count / distinct_visitor_count)*100 inviters_over_total_visitors, " +
                "        avg_invitees_by_inviter " +
                "FROM (SELECT YEARWEEK(date_created) week_id, game_id, COUNT(DISTINCT sender_name) distinct_sender_count, COUNT(*) total_sent " +
                "        FROM ( SELECT aba.date_created, game_id, sender_name FROM diary.active_blasts_archive aba, kaneva.communities_game cg " +
                "                WHERE aba.community_id = cg.community_id AND aba.blast_type = 23 " +
                "                  AND cg.game_id = @game_id " +
                "                  AND date_created > @startDate " +
                "                  AND date_created < @endDate " +
                "               UNION ALL " +
                "               SELECT ab.date_created, game_id, sender_name FROM diary.active_blasts ab, kaneva.communities_game cg " +
                "                WHERE ab.community_id = cg.community_id AND ab.blast_type = 23 " +
                "                  AND cg.game_id = @game_id " +
                "                  AND date_created > @startDate " +
                "                  AND date_created < @endDate ) AS aba " +
                "    GROUP BY YEARWEEK(date_created), game_id) AS senders_count, " +
                "     (SELECT YEARWEEK(created_date) week_id, game_id, COUNT(DISTINCT user_id) distinct_visitor_count " +
                "        FROM developer.login_log " +
                "       WHERE game_id = @game_id " +
                "         AND created_date > @startDate " +
                "         AND created_date < @endDate " +
                "    GROUP BY YEARWEEK(created_date), game_id) AS visitor_count, " +
                "     (SELECT week_id, game_id, AVG(distinct_recipient_count) avg_invitees_by_inviter " +
                "        FROM (SELECT * FROM (SELECT YEARWEEK(date_created) week_id, game_id, sender_name, COUNT(DISTINCT recipient_id) distinct_recipient_count " +
                "                               FROM (SELECT  ab.date_created, game_id, sender_name, recipient_id " +
                "                                       FROM  diary.active_blasts ab, " +
                "                                             kaneva.communities_game cg " +
                "                                      WHERE  ab.community_id = cg.community_id " +
                "                                        AND  ab.blast_type = 23 " +
                "                                        AND  cg.game_id = @game_id " +
                "                                        AND  date_created > @startDate " +
                "                                        AND  date_created < @endDate " +
                "                                    UNION ALL " +
                "                                      SELECT aba.date_created, game_id, sender_name, recipient_id " +
                "                                        FROM diary.active_blasts_archive aba, " +
                "                                             kaneva.communities_game cg " +
                "                                       WHERE aba.community_id = cg.community_id " +
                "                                        AND  aba.blast_type = 23 " +
                "                                        AND  cg.game_id = @game_id " +
                "                                        AND  date_created > @startDate " +
                "                                        AND  date_created < @endDate) AS aba " +
                "                              GROUP BY YEARWEEK(date_created), game_id, sender_name) AS X) AS receipient_counts_by_sender " +
                "    GROUP BY week_id, game_id) AS avg_invitee_count " +
                "WHERE senders_count.week_id = visitor_count.week_id " +
                "  AND senders_count.game_id = visitor_count.game_id " +
                "  AND visitor_count.week_id = avg_invitee_count.week_id " +
                "  AND visitor_count.game_id = avg_invitee_count.game_id  ORDER BY senders_count.week_id DESC ";

            Hashtable theParams = new Hashtable();
            parameters.Add("@game_id", gameId);
            parameters.Add("@startDate", startDate);
            parameters.Add("@endDate", endDate);

            return GetDatabaseUtility().GetDataTable(sql, parameters);
        }

        public static DataTable GetuserRetentionData(int gameId, DateTime startDate, DateTime endDate)
        {
            Hashtable parameters = new Hashtable();

            DatabaseUtility dbUtility = GetDatabaseUtility();

            string sql = "SELECT CONCAT(DATE_FORMAT(STR_TO_DATE(CONCAT(YEARWEEK(login_date), ' Sunday'), '%X%V %W'),'%Y/%m/%d'),' - ',DATE_FORMAT(STR_TO_DATE(CONCAT(YEARWEEK(login_date), ' Saturday'), '%X%V %W'),'%Y/%m/%d')) as date_range, " +
                            "       AVG(one_day)*100 AS one_day_retention, " +
                            "       AVG(seven_day)*100 AS seven_day_retention, " +
                            "       AVG(thirty_day)*100 AS thirty_day_retention " +
                            "FROM   " +
                            "(SELECT * FROM (SELECT  unique_logins.login_date, " +
                            "        COUNT( DISTINCT one_day_unique_logins.user_id ) / COUNT( DISTINCT unique_logins.user_id ) AS one_day, " +
                            "        COUNT( DISTINCT seven_day_unique_logins.user_id ) / COUNT( DISTINCT unique_logins.user_id ) AS seven_day, " +
                            "        COUNT( DISTINCT thirty_day_unique_logins.user_id ) / COUNT( DISTINCT unique_logins.user_id ) AS thirty_day " +
                            "FROM   (SELECT * FROM ( SELECT ll.user_id, " +
                            "                               DATE_FORMAT(ll.created_date, '%Y-%m-%d') login_date " +
                            "                          FROM developer.login_log ll " +
                            "                         WHERE game_id = @game_id " + "\r\n " +
                            "                           AND created_date >= @startDate " +
                            "                           AND created_date <  @endDate " +
                            "                      GROUP BY ll.user_id, " +
                            "                               DATE_FORMAT(ll.created_date, '%Y-%m-%d') ) AS x) AS unique_logins " +
                            "LEFT OUTER JOIN (SELECT * FROM ( SELECT ll.user_id, " +
                            "                               DATE_FORMAT(ll.created_date, '%Y-%m-%d') login_date " +
                            "                          FROM developer.login_log ll " +
                            "                         WHERE game_id = @game_id " +
                            "                           AND created_date >= @startDate " +
                            "                           AND created_date <  @endDate " +
                            "                      GROUP BY ll.user_id, " +
                            "                               DATE_FORMAT(ll.created_date, '%Y-%m-%d') ) AS x) AS one_day_unique_logins " +
                            "ON  unique_logins.user_id = one_day_unique_logins.user_id " +
                            "AND one_day_unique_logins.login_date = ( unique_logins.login_date + INTERVAL 1 DAY) " +
                            "LEFT OUTER JOIN (SELECT * FROM ( SELECT ll.user_id, " +
                            "                               DATE_FORMAT(ll.created_date, '%Y-%m-%d') login_date " +
                            "                          FROM developer.login_log ll " +
                            "                         WHERE game_id = @game_id " +
                            "                           AND created_date >= @startDate " +
                            "                           AND created_date <  @endDate " +
                            "                      GROUP BY ll.user_id, " +
                            "                               DATE_FORMAT(ll.created_date, '%Y-%m-%d') ) AS x) AS seven_day_unique_logins " +
                            "ON  unique_logins.user_id = seven_day_unique_logins.user_id " +
                            "AND seven_day_unique_logins.login_date = ( unique_logins.login_date + INTERVAL 7 DAY) " +
                            "LEFT OUTER JOIN (SELECT * FROM ( SELECT ll.user_id, " +
                            "                               DATE_FORMAT(ll.created_date, '%Y-%m-%d') login_date " +
                            "                          FROM developer.login_log ll " +
                            "                         WHERE game_id = @game_id " +
                            "                           AND created_date >= @startDate " +
                            "                           AND created_date <  @endDate " +
                            "                      GROUP BY ll.user_id, " +
                            "                               DATE_FORMAT(ll.created_date, '%Y-%m-%d') ) AS x) AS thirty_day_unique_logins " +
                            "ON  unique_logins.user_id = thirty_day_unique_logins.user_id " +
                            "AND thirty_day_unique_logins.login_date = ( unique_logins.login_date + INTERVAL 30 DAY) " +
                            "GROUP BY unique_logins.login_date) AS X) AS daily_retention " +
                            "GROUP BY YEARWEEK(login_date) ORDER BY login_date DESC ";

            Hashtable theParams = new Hashtable();
            parameters.Add("@game_id", gameId);
            parameters.Add("@startDate", startDate);
            parameters.Add("@endDate", endDate);

            return GetDatabaseUtility().GetDataTable(sql, parameters);
        }
        
		/// <summary>
		/// Get the database utility
		/// </summary>
		/// <returns></returns>
		private static DatabaseUtility GetDatabaseUtility ()
		{
			if (m_DatabaseUtilty == null)
			{
				switch (KanevaGlobals.DatabaseType)
				{
					case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
					{
						m_DatabaseUtilty = new SQLServerUtility (KanevaGlobals.ConnectionStringStats);
						break;
					}
					case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
					{
						m_DatabaseUtilty = new MySQLUtility (KanevaGlobals.ConnectionStringStats);
						break;
					}
					default:
					{
						throw new Exception ("This database type is not supported");
					}
				}
			}
			return m_DatabaseUtilty;
		}

		/// <summary>
		/// DataBase Utility
		/// </summary>
		private static DatabaseUtility m_DatabaseUtilty;

	}

}
