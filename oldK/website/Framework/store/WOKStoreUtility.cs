///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Linq;

// Add a changeset
// Merging test...

using System.Collections.Generic;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
    /// <summary>
    /// Summary description for WOKStoreUtility.
    /// </summary>
    /// Another comment
    public class WOKStoreUtility
    {
        private static string WOKdbName = KanevaGlobals.DbNameKGP;
        private static string COMMON_CATALOG_COLUMNS = " global_id, name, description, market_cost, selling_price, item_creator_id, inventory_type ";
        private static string COMMON_CATALOG_COLUMNS_PARAM = " @global_id, @name, @description, @market_cost, @selling_price, @item_creator_id ";
        private static string EDITOR_COLUMNS = " required_skill, required_skill_level, use_type, actor_group ";
        //private static string ALL_COLUMNS = CATALOG_COLUMNS + "," + EDITOR_COLUMNS;
        private static string ALL_COLUMNS = EDITOR_COLUMNS;

        /// <summary>
        /// get pass group info from table
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetPassGroups()
        {
            return GetPassGroups(null);
        }

        public static DataRow GetOwnerChannelZone (int userId)
        {
            string sql = "SELECT * " +
                        " FROM wok.channel_zones cz " +
                        " INNER JOIN wok.players p ON p.player_id = cz.zone_instance_id " +
                        " WHERE p.kaneva_user_id = @userId " +
                        " AND cz.zone_type = 3 limit 1 ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            return dbUtility.GetDataRow(sql, parameters, false);
        }

        public static DataRow GetHangoutChannelZone(int communityId, bool isHome)
        {
            string sql = "SELECT * " +
                        " FROM wok.channel_zones cz " +
                        " WHERE cz.zone_instance_id = @communityId " +
                        " AND cz.zone_type = @zoneType ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@communityId", communityId);
            parameters.Add ("@zoneType", isHome ? 3 : 6);
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            return dbUtility.GetDataRow(sql, parameters, false);
        }

        /// <summary>
        /// get pass group info from table
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetPassGroups(string passIdFilter)
        {
            string sql = "SELECT pass_group_id, name " +
                        "  FROM " + WOKdbName + ".pass_groups ";

            if ((passIdFilter != null) && (passIdFilter != ""))
            {
                sql += " WHERE pass_group_id IN (" + passIdFilter + ")";
            }

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            return dbUtility.GetDataTable(sql);
        }


        /// <summary>
        /// get list of stores
        /// </summary>
        /// <returns></returns>
        public static DataTable GetStores()
        {
            string sqlString = "SELECT s.name as storeName, concat( s.name, ' in the ', left( sw.name, INSTR(sw.name, '.exg')-1), ' (', cast(count(*) as char), ')' ) as name, s.store_id " +
                                "  FROM " + WOKdbName + ".stores s " +
                                "       INNER JOIN " +
                                       WOKdbName + ".supported_worlds sw ON (s.channel_id = sw.channel_id) " +
                                "     INNER JOIN " +
                                        WOKdbName + ".store_inventories si ON (s.store_id = si.store_id) " +
                                " INNER JOIN " +
                                      WOKdbName + ".items i ON (si.global_id = i.global_id) " +
                                " group by storeName ";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            return dbUtility.GetDataTable(sqlString);
        }

        /// <summary>
        /// get items sets from table
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetAllStores()
        {
            string sql = "SELECT s.store_id, s.name FROM " + WOKdbName + ".stores s ";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            return dbUtility.GetDataTable(sql);
        }

        /// <summary>
        /// get items stores from table
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetItemStores(int itemId)
        {
            Hashtable theParams = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sql = "SELECT s.store_id, s.name FROM " + WOKdbName + ".stores s " +
                "INNER JOIN " + WOKdbName + ".store_inventories si ON s.store_id = si.store_id WHERE si.global_id = @itemId ";

            theParams.Add("@itemId", itemId);

            return dbUtility.GetDataTable(sql, theParams);
        }

        /// <summary>
        /// get all the pass group an item is in
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetItemPassGroups(int itemId)
        {
            Hashtable theParams = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sql = "SELECT pgi.global_id, pgi.pass_group_id FROM " + WOKdbName + ".pass_group_items pgi WHERE pgi.global_id = @itemId ";

            theParams.Add("@itemId", itemId);

            return dbUtility.GetDataTable(sql, theParams);
        }

        /// <summary>
        /// get an item set by the item id and item set id
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetItemStoreByItemIDStoreID(int itemId, int storeId)
        {
            Hashtable theParams = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sql = "SELECT s.store_id, s.name FROM " + WOKdbName + ".stores s " +
                " INNER JOIN " + WOKdbName + ".store_inventories si ON s.store_id = si.store_id WHERE si.global_id = @itemId AND si.store_id = @storeId ";

            theParams.Add("@itemId", itemId);
            theParams.Add("@storeId", storeId);

            return dbUtility.GetDataTable(sql, theParams);
        }

        /// <summary>
        /// get currency types from table
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetCurrencyTypes()
        {
            string sql = "SELECT ct.currency_type_id, ct.currency_type FROM " + WOKdbName + ".currency_type ct ";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            return dbUtility.GetDataTable(sql);
        }

        /// <summary>
        /// get access types from table
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetAccessPassTypes()
        {
            string sql = "SELECT pg.pass_group_id, pg.name FROM " + WOKdbName + ".pass_groups pg";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            return dbUtility.GetDataTable(sql);
        }

        /// <summary>
        /// get sets that the item is in
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetItemsSets(int itemId)
        {
            Hashtable theParams = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sql = "SELECT isi.item_set_id, isi.item_id FROM " + WOKdbName + ".item_set_items isi WHERE isi.item_id = @itemId ";

            theParams.Add("@itemId", itemId);

            return dbUtility.GetDataTable(sql, theParams);
        }


        /// <summary>
        /// get an item set by the item id and item set id
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetItemSetByItemID(int itemId, int itemSetId)
        {
            Hashtable theParams = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sql = "SELECT isi.item_set_id, isi.item_id, iss.set_name FROM " + WOKdbName + ".item_set_items isi " +
                " INNER JOIN " + WOKdbName + ".item_sets iss ON isi.item_set_id = iss.item_set_id WHERE isi.item_id = @itemId AND isi.item_set_id = @itemSetID ";

            theParams.Add("@itemId", itemId);
            theParams.Add("@itemSetID", itemSetId);

            return dbUtility.GetDataTable(sql, theParams);
        }

        /// <summary>
        /// get an items pass group by its item Id and pass group Id
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetItemByPassGroupID(int itemId, int passGroupId)
        {
            Hashtable theParams = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sql = "SELECT pgi.global_id, pgi.pass_group_id FROM " + WOKdbName + ".pass_group_items pgi WHERE pgi.global_id = @itemId " +
            "AND pgi.pass_group_id = @passGroupId";

            theParams.Add("@itemId", itemId);
            theParams.Add("@passGroupId", passGroupId);

            return dbUtility.GetDataTable(sql, theParams);
        }

        /// <summary>
        /// get items sets from table
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetAllItemSets()
        {
            string sql = "SELECT iss.item_set_id, iss.set_name FROM " + WOKdbName + ".item_sets iss ";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            return dbUtility.GetDataTable(sql);
        }

        /// <summary>
        /// get product availability types from table
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetProductAvailability()
        {
            string sql = "SELECT pa.product_availability_id, pa.product_availability FROM " + WOKdbName + ".product_availability pa ";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            return dbUtility.GetDataTable(sql);
        }

        /// <summary>
        /// get product exclusivity types from table
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetProductExclusivity()
        {
            string sql = "SELECT pe.product_exclusivity_id, pe.product_exclusivity FROM " + WOKdbName + ".product_exclusivity pe ";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            return dbUtility.GetDataTable(sql);
        }

        /// <summary>
        /// get the contents of a WOK store
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static PagedDataTable GetStoreContents(int storeId, int pageNumber, int pageSize)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string selectList = " i.name, i.market_cost, i.global_id, i.selling_price, i.use_type, i.required_skill, i.required_skill_level, i.description, i.inventory_type, ";
            // stats not represented in database at this time.
            selectList += " 0 AS stat_count, 0 AS stat_id, 0 AS stat_bonus ";
            string tableList = WOKdbName + ".store_inventories si INNER JOIN " + WOKdbName + ".items i ON (si.global_id = i.global_id) ";
            string whereClause = "si.store_id = @storeId";
            string orderByList = "name ASC";
            Hashtable theParams = new Hashtable();
            theParams.Add("@storeId", storeId);

            return dbUtility.GetPagedDataTable(selectList, tableList, whereClause, orderByList, theParams, pageNumber, pageSize);

        }

        /// <summary>
        /// get the contents of a WOK store
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static DataTable GetStoreContents(int storeId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string sql= " SELECT i.name, i.market_cost, i.global_id, i.selling_price, i.use_type, ";
            sql += "i.required_skill, i.required_skill_level, i.description, i.inventory_type, ";
            // stats not represented in database at this time.
            sql += " 0 AS stat_count, 0 AS stat_id, 0 AS stat_bonus";
            sql += " FROM ";
            sql += WOKdbName + ".store_inventories si INNER JOIN ";
            sql += WOKdbName + ".items i ";
            sql += "ON (si.global_id = i.global_id) ";
            sql += "WHERE si.store_id = @storeId ORDER BY name ASC";

            Hashtable theParams = new Hashtable();
            theParams.Add("@storeId", storeId);

            return dbUtility.GetDataTable(sql, theParams);
        }


        /// <summary>
        /// get all the currently available in world items (1 parameters)
        /// catalog maintenance columns only
        /// </summary>
        /// <param name="filter"></param>
        public static PagedDataTable GetWOKItemsCatalog(string filter, int pageNumber, int pageSize)
        {
            return GetWOKItems("", -1, filter, pageNumber, pageSize);
        }

        /// <summary>
        /// get all the currently available in world items (1 parameters)
        /// catalog maintenance columns only
        /// </summary>
        /// <param name="filter"></param>
        public static PagedDataTable GetWOKItemsEditor(string filter, int pageNumber, int pageSize)
        {
            return GetWOKItems(EDITOR_COLUMNS, -1, filter, pageNumber, pageSize);
        }


        /// <summary>
        /// get all the currently available in world items (3 parameters)
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        public static PagedDataTable GetWOKItems(string filter, int pageNumber, int pageSize)
        {
            return GetWOKItems(ALL_COLUMNS, -1, filter, pageNumber, pageSize);
        }


        /// <summary>
        /// get all the currently available in world items (4 parameters)
        /// created to allow for singular item retrieval if so desired by specifying the items id number
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        public static PagedDataTable GetWOKItems(string Columns, int itemID, string filter, int pageNumber, int pageSize)
        {
            string whereClause = "";
            Hashtable theParams = new Hashtable();

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            if (Columns == null || Columns == "")
            {
                Columns = ALL_COLUMNS;
            }

            //attach primary key to columns and attach items table to columns to form SQL string
            string selectList = COMMON_CATALOG_COLUMNS;

            if (Columns.Length > 0)
            {
                selectList += "," + Columns;
            }

            string tableList = WOKdbName + ".items i ";

            //limit by item id if one is provided
            if (itemID > 0)
            {
                whereClause += "i.global_id = @itemId ";
                theParams.Add("@itemId", itemID);
            }

            //append the where clause if a filter is provided
            if ((filter != null) && (filter != ""))
            {
                if (whereClause.Length > 0)
                {
                    whereClause += "AND ";
                }
                whereClause += filter;
            }

            string orderByList = " name ASC";


            return dbUtility.GetPagedDataTable(selectList, tableList, whereClause, orderByList, theParams, pageNumber, pageSize);
        }

        /// <summary>
        /// get all the currently available in world items (1 parameters)
        /// all columns
        /// </summary>
        /// <param name="filter"></param>
        public static DataTable GetWOKItems(string filter)
        {
            return GetWOKItems(ALL_COLUMNS, -1, filter, false);
        }

        /// <summary>
        /// get all the currently available in world items (1 parameters)
        /// all columns
        /// </summary>
        /// <param name="filter"></param>
        public static DataTable GetWOKItems (string filter, bool isVIPPurchase)
        {
            return GetWOKItems (ALL_COLUMNS, -1, filter, isVIPPurchase);
        }

        /// <summary>
        /// get all the currently available in world items (1 parameters)
        /// catalog maintenance columns only
        /// </summary>
        /// <param name="filter"></param>
        public static DataTable GetWOKItemsCatalog(string filter)
        {
            return GetWOKItems ("", -1, filter, false);
        }

        /// <summary>
        /// get all the currently available in world items (1 parameters)
        /// catalog maintenance columns only
        /// </summary>
        /// <param name="filter"></param>
        public static DataTable GetWOKItemsEditor(string filter)
        {
            return GetWOKItems(EDITOR_COLUMNS, -1, filter, false);
        }

        /// <summary>
        /// get all the currently available in world items (3 parameters)
        /// based function for retrieving Editor and Catalog columns
        /// if no columns are specified it will pull back all columns
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        public static DataTable GetWOKItems(string Columns, int itemID, string filter, bool isVIPPurchase)
        {
            string sql = "";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();
            Hashtable theParams = new Hashtable();

            if (Columns == null || Columns == "")
            {
                Columns = ALL_COLUMNS;
            }

            //attach primary key to columns and attach items table to columns to form SQL string
            sql = "SELECT i.global_id, name, description, selling_price, item_creator_id, " +
                " IF(i.item_creator_id IN (0,4230696), 512, 256) AS inventory_type, ";

            if (isVIPPurchase)
            {
                sql += " IFNULL(iw.designer_price, 0) + IFNULL(iw2.designer_price, 0) AS market_cost ";
            }
            else
            {
                sql += " IF(i.item_creator_id > 0, CAST(((i.market_cost + iw.designer_price) * 1.1) as UNSIGNED) ,market_cost) AS market_cost ";
            }

            if (Columns.Length > 0)
            {
                sql += "," + Columns;
            }

            // Shop columns
            sql += " , iw.display_name, iw.date_added, iw.item_active, iw.thumbnail_medium_path ";

            sql += " FROM " + WOKdbName + ".items i " +
                " INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 ON iw.base_global_id = iw2.global_id ";

            //limit by item id if one is provided
            if (itemID > 0)
            {
                sql += " WHERE i.global_id = @itemId ";
                theParams.Add("@itemId", itemID);
            }

            //append the where clause if a filter is provided
            if ((filter != null) && (filter != ""))
            {
                if (itemID > 0)
                {
                    sql += " AND " + filter;
                }
                else
                {
                    sql += " WHERE " + filter;
                }
            }

            sql += " ORDER BY name ASC";


            return dbUtility.GetDataTable(sql, theParams);

        }

        /// <summary>
        /// get wok item by object placement id
        /// </summary>
        /// <param name="Cokumns"></param>
        /// <param name="objPlacementId"></param>
        public static WOKItem GetWOKItemsByObjPlacementId(int objPlacementId)
        {
            WOKItem returnItem = null;

            // Get the global id from the dynamic_objects table
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();
            Hashtable sqlParams = new Hashtable();

            string sql = "SELECT global_id FROM " + WOKdbName + ".dynamic_objects WHERE obj_placement_id = @objPlacementId";
            sqlParams.Add("@objPlacementId", objPlacementId);

            DataTable dt = dbUtility.GetDataTable(sql, sqlParams);
            if (dt != null)
            {
                DataRow row = dt.Rows[0];
                if (row != null)
                {
                    int globalId = (int)row["global_id"];

                    // Get the 
                    ShoppingFacade shopFacade = new ShoppingFacade();
                    returnItem = shopFacade.GetItem(globalId);
                }
            }

            return returnItem;
        }

        /// <summary>
        /// get wok item by object placement id
        /// </summary>
        /// <param name="Cokumns"></param>
        /// <param name="objectId"></param>
        public static DataTable GetWOKItemsByObjectId(string Columns, int objectId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();
            Hashtable theParams = new Hashtable();

            if (Columns == null || Columns == "")
            {
                Columns = ALL_COLUMNS;
            }

            //attach primary key to columns and attach items table to columns to form SQL string
            string sql = "SELECT " + COMMON_CATALOG_COLUMNS.Replace("global_id", "i.global_id") + "," + Columns + " FROM " + WOKdbName + ".items i INNER JOIN " +
                 WOKdbName + ".dynamic_objects d ON i.global_id = d.global_id WHERE d.object_id = @objectId";

            theParams.Add("@objectId", objectId);

            return dbUtility.GetDataTable(sql, theParams);

        }

        /// <summary>
        /// get a list of all WOK items. Limited data returned
        /// </summary>
        public static DataTable GetWOKItemsList()
        {
            string sql = "";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            //attach primary key to columns and attach items table to columns to form SQL string
            sql = "SELECT i.global_id, i.display_name, i.name, i.description FROM " + WOKdbName + ".items i " + 
            "ORDER BY display_name ASC";

            return dbUtility.GetDataTable(sql);

        }

        public static int GetTotalOrderCost(string itemList, ArrayList order, bool isVIPPurchase)
        {
            int total = 0;

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            // pull back the items and their individual cost
            //string sqlInsert = "select global_id, market_cost from wok.items where global_id IN (" + itemList +")";
            string sqlSelect = "SELECT i.global_id, market_cost, iw.designer_price, " +
                " IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice " +
                " FROM wok.items i " + 
                " INNER JOIN shopping.items_web iw ON iw.global_id = i.global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 ON iw2.global_id = iw.base_global_id " +
                " WHERE i.global_id IN (" + itemList + ")";

            DataTable orderItems = dbUtility.GetDataTable(sqlSelect);

            //multi-dimentioal array is expected to be 1 x 2
            foreach (int[,] orderItem in order)
            {
                //int cost = Convert.ToInt32((orderItems.Select("global_id = " + orderItem[0, 0]))[0]["market_cost"]);
                /*
                int cost = Convert.ToInt32 ((orderItems.Select ("global_id = " + orderItem[0, 0]))[0]["designer_price"]) +
                    Convert.ToInt32 ((orderItems.Select ("global_id = " + orderItem[0, 0]))[0]["TemplateDesignerPrice"]);
                if (!isVIPPurchase)
                {
                    cost += Convert.ToInt32 ((orderItems.Select ("global_id = " + orderItem[0, 0]))[0]["market_cost"]);
                }
                */
                int cost = GetItemCheckoutPrice (Convert.ToInt32 ((orderItems.Select ("global_id = " + orderItem[0, 0]))[0]["market_cost"]),
                    Convert.ToInt32 ((orderItems.Select ("global_id = " + orderItem[0, 0]))[0]["designer_price"]),
                    Convert.ToInt32 ((orderItems.Select ("global_id = " + orderItem[0, 0]))[0]["TemplateDesignerPrice"]), isVIPPurchase);

                total += cost * orderItem[0, 1];
            }

            return total;
        }

        public static int GetItemCheckoutPrice (int marketCost, int designerPrice, int templateDesignerPrice, bool isVIPPurchase)
        {
            int price = designerPrice + templateDesignerPrice;
            if (!isVIPPurchase)
            {
                price += marketCost;
            }

            return price;
        }

        /// <summary>
        /// InsertIntoItemSet - inserts an item into an itemset
        /// </summary>
        /// <returns></returns>
        public static int InsertIntoItemSet(int itemSetId, int itemId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            // Record User Asset Subscriptions
            string sqlInsert = "INSERT INTO "  + WOKdbName + ".item_set_items " +
                " (item_set_id, item_id) VALUES (@itemSetId, @itemId)";

            int insertItemId = 0;
            Hashtable parameters = new Hashtable();
            parameters.Add("@itemSetId", itemSetId);
            parameters.Add("@itemId", itemId);
            return dbUtility.ExecuteIdentityInsert(sqlInsert, parameters, ref insertItemId);
        }

        /// <summary>
        /// UpdateItemSet - updates item set
        /// </summary>
        /// <param name="featuredAssetId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        /*  public static int UpdateItemSet(int itemSetId, int itemId)
          {
              DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

              string sqlUpdate = "UPDATE item_set_items SET " +
                  "item_set_id = @itemSetId, " +
                  "item_id = @itemId " +
                  " WHERE featured_asset_id = @faId";

              Hashtable parameters = new Hashtable();
              parameters.Add("@itemSetId", itemSetId);
              parameters.Add("@itemId", itemId);
              return dbUtility.ExecuteNonQuery(sqlUpdate, parameters);
          }*/

        /// <summary>
        /// DeleteFromItemSet - removes an item from the item set
        /// </summary>
        /// <param name="id">the id of the asset</param>
        /// <returns></returns>
        public static int DeleteFromItemSet(int itemSetId, int itemId)
        {
            //delete from contest table
            string sqlString = "DELETE FROM " + WOKdbName + ".item_set_items WHERE item_set_id = @itemSetId AND item_id = @itemId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@itemSetId", itemSetId);
            parameters.Add("@itemId", itemId);

           return  KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// InsertIntoItemSet - inserts an item into an itemset
        /// this function throws errors to calling function
        /// </summary>
        /// <returns></returns>
        public static int InsertIntoPassGroup(int passGroupId, int itemId, int modifierId)
        {
            // Record User Asset Subscriptions
            string sqlInsert = "CALL " + WOKdbName + ".add_pass_group_items(@passGroupId,@itemId,@modifierId)";

            Hashtable parameters = new Hashtable();
            parameters.Add("@passGroupId", passGroupId);
            parameters.Add("@itemId", itemId);
            parameters.Add("@modifierId", modifierId);

            return KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sqlInsert, parameters);
        }

        /// <summary>
        /// DeleteFromItemSet - removes an item from the item set
        /// </summary>
        /// <param name="id">the id of the asset</param>
        /// <returns></returns>
        public static int DeleteFromPassGroup(int passGroupId, int itemId, int modifierId)
        {
            //delete from contest table
            string sqlString = "CALL " + WOKdbName + ".delete_pass_group_items(@passGroupId,@itemId,@modifierId)";

            Hashtable parameters = new Hashtable();
            parameters.Add("@passGroupId", passGroupId);
            parameters.Add("@itemId", itemId);
            parameters.Add("@modifierId", modifierId);

            return KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// InsertCatalogItem - inserts a bew catalog item
        /// </summary>
        /// <returns></returns>
        public static int InsertCatalogItem(ref int insertItemId, string name, string displayName, string description, DateTime dateAdded,
            int marketCost, int sellingPrice, int creatorId, int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            // Record User Asset Subscriptions
            string sqlInsert = "INSERT INTO " + WOKdbName + ".items " +
                " (" + COMMON_CATALOG_COLUMNS + ") VALUES (" + COMMON_CATALOG_COLUMNS_PARAM + ")";

            Hashtable parameters = new Hashtable();
            parameters.Add("@name", name);
            parameters.Add("@description", description);
            parameters.Add("@market_cost", marketCost);
            parameters.Add("@selling_price", sellingPrice);
            parameters.Add("@item_creator_id", creatorId);

            return dbUtility.ExecuteIdentityInsert(sqlInsert, parameters, ref insertItemId);
        }

        /// <summary>
        /// UpdateCatalogItem - updates WOK catalog item 
        /// </summary>
        /// <param name="featuredAssetId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
          public static int UpdateCatalogItem(int globalId, string name, string displayName, string description, DateTime dateAdded,
            int marketCost, int sellingPrice, int creatorId, int inventoryType, int itemActive, int userId)
          {
              DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

              string sqlUpdate = "UPDATE " + WOKdbName + ".items SET " +
                  "name = @name, " +
                  "description = @description, " +
                  "market_cost = @market_cost, " +
                  "selling_price = @selling_price, " +
                  "item_creator_id = @item_creator_id, " +
                  "inventory_type = @inventory_type " +
                  " WHERE global_id = @global_id ";

              Hashtable parameters = new Hashtable();
                parameters.Add("@name", name);
                parameters.Add("@description", description);
                parameters.Add("@market_cost", marketCost);
                parameters.Add("@selling_price", sellingPrice);
                parameters.Add("@item_creator_id", creatorId);
                parameters.Add("@inventory_type", inventoryType);
                parameters.Add("@global_id", globalId);
              dbUtility.ExecuteNonQuery(sqlUpdate, parameters);

              sqlUpdate = "UPDATE shopping.items_web SET " +
                  "display_name = @displayName, " +
                  "item_active = @itemActive " +
                  " WHERE global_id = @global_id ";
              parameters = new Hashtable();
              parameters.Add("@displayName", displayName);
              parameters.Add("@itemActive", itemActive);
              parameters.Add("@global_id", globalId);
              return dbUtility.ExecuteNonQuery(sqlUpdate, parameters);
          }

          /// <summary>
          /// UpdateCatalogItemControlledByWeb
          /// </summary>
        public static int UpdateCatalogItemControlledByWeb(int globalId, int webControlledEdit)
          {
              DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

              string sqlUpdate = "UPDATE " + WOKdbName + ".items SET " +
                  "web_controlled_edit = @web_controlled_edit " +
                  " WHERE global_id = @global_id ";

              Hashtable parameters = new Hashtable();
              parameters.Add("@web_controlled_edit", webControlledEdit);
              parameters.Add("@global_id", globalId);
              return dbUtility.ExecuteNonQuery(sqlUpdate, parameters);
          }

          /// <summary>
          /// UpdateCatalogItem - updates WOK catalog item 
          /// </summary>
          /// <param name="featuredAssetId"></param>
          /// <param name="status"></param>
          /// <returns></returns>
          public static int UpdateCatalogItemThumb(int globalId, string itemImagePath)
          {
              DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

              string sqlUpdate = "UPDATE " + WOKdbName + ".items SET " +
                  "item_image_path = @item_image_path " +
                  " WHERE global_id = @global_id ";

              Hashtable parameters = new Hashtable();
              parameters.Add("@item_image_path", itemImagePath);
              parameters.Add("@global_id", globalId);
              return dbUtility.ExecuteNonQuery(sqlUpdate, parameters);
          }

        /// <summary>
          /// UpdateWOKItemThumb
        /// </summary>
        public static int UpdateWOKItemThumb(int globalId, string itemImagePath, string dbColumn)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlUpdate = "UPDATE shopping.items_web SET " +
                dbColumn + " = @item_image_path " +
                " WHERE global_id = @global_id ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@item_image_path", itemImagePath);
            parameters.Add("@global_id", globalId);
            return dbUtility.ExecuteNonQuery(sqlUpdate, parameters);
        }

    }
}

// Merging test 2