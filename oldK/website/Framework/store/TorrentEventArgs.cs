///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Used to notify events about a torrent
	/// </summary>
	public class TorrentEventArgs : EventArgs
	{
		private string _infoHash;
		public TorrentEventArgs()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public TorrentEventArgs(string infoHash)
		{
			//
			// TODO: Add constructor logic here
			//
			InfoHash = infoHash;
		}

		public string InfoHash
		{
			get { return _infoHash; }
			set { _infoHash = value; }
		}
	}

	/// <summary>
	/// Event handler for the TorrentEventArgs event.
	/// </summary>
	public delegate void TorrentEventHandler( object sender, TorrentEventArgs e );
}
