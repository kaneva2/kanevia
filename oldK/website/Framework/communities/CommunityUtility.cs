///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using System.Web;
using System.Collections;
using System.Threading;
using log4net;
using KlausEnt.KEP.Kaneva.framework.widgets;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for CommunityUtility.
	/// </summary>
	public class CommunityUtility
	{
		static CommunityUtility()
		{
			m_DbNameKanevaNonCluster = KanevaGlobals.DbNameKanevaNonCluster;
		}

		/// <summary>
		/// GetBroadcastChannelImageURL
		/// </summary>
		/// <param name="imagePath">The relative path to the image on the NAS. Example - 4/howdown.gif</param>
		/// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la'</param>
		/// <returns></returns>
		public static string GetBroadcastChannelImageURL (string imagePath, string defaultSize)
		{
			if (imagePath.Length.Equals (0))
			{
                return KanevaGlobals.ImageServer + "/defaultworld_" + defaultSize + ".jpg";
			}
			else
			{
				return KanevaGlobals.ImageServer + "/" + imagePath;
			}
		}

		/// <summary>
		/// Get a communities
		/// </summary>
		/// <param name="orderby"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public static DataRow GetCommunity (int communityId)
		{
			return GetCommunity (communityId, Constants.eCOMMUNITY_STATUS.ACTIVE);
		}

		/// <summary>
		/// Get a communities
		/// </summary>
		/// <param name="orderby"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public static DataRow GetCommunity (int communityId, Constants.eCOMMUNITY_STATUS statusId)
		{
			string sqlSelect = "SELECT c.community_id, c.name, c.name_no_spaces, c.description, c.is_public, c.is_adult, c.over_21_required, c.created_date, " +
                " c.status_id, c.email, c.creator_id, c.background_image, " +
                " 0 AS category_id, c.place_type_id, c.background_rgb, " + 
				" c.thumbnail_path, c.is_personal, c.template_id, " +
                " c.creator_username as username, c.keywords, c.allow_publishing, c.allow_member_events, c.url, " +
				" c.thumbnail_small_path, c.thumbnail_medium_path, c.thumbnail_large_path," +
				" cs.number_of_views, cs.number_of_members, cs.number_of_diggs, cs.number_times_shared, cs.number_of_pending_members, " +
				" u.zip_code, u.age, u.gender, u.country, u.location, u.mature_profile, us.number_of_friends as friends" +
				" FROM communities c " +
				" INNER JOIN channel_stats cs ON c.community_id = cs.channel_id " +
				" INNER JOIN users u ON u.user_id = c.creator_id " +
				" INNER JOIN users_stats us ON us.user_id = c.creator_id " +
				" WHERE c.status_id = @statusId " +
				" AND c.community_id = @communityId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@statusId", (int) statusId);
			return  KanevaGlobals.GetDatabaseUtilityReadOnly2 ().GetDataRow (sqlSelect, parameters, true);
		}

		/// <summary>
		/// GetPublishChannels
		/// </summary>
		/// <returns></returns>
		public static PagedDataTable GetPublishChannels (int userId, string orderby, int pageNumber, int pageSize)
		{
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

			string selectList = " c.community_id, c.name, c.is_personal, c.creator_id, c.creator_username, " +
				" c.creator_username as username, c.is_adult, c.is_public, c.keywords, c.description, " +
				" c.name_no_spaces, c.thumbnail_small_path, " +
				" cs.number_of_members ";

			string tableList = " communities c " +
				" INNER JOIN community_members cm ON c.community_id = cm.community_id " +
				" INNER JOIN channel_stats cs ON c.community_id = cs.channel_id ";

			string sqlSelect = "SELECT " + selectList +
				" FROM " + tableList +
				" WHERE c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE +
				" AND cm.user_id = @userId " +
				//" AND c.category_id NOT IN (" + (int) Constants.eCOMMUNITY_CATEGORIES.MAIN_TAB + ")" + 
				" AND cm.active_member = 'Y' " +
				" AND cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE;

			sqlSelect += " UNION " +
				" SELECT " + selectList +
				" FROM " + tableList +
				" WHERE c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE +
				" AND cm.user_id = @userId " +
				//" AND c.category_id NOT IN (" + (int) Constants.eCOMMUNITY_CATEGORIES.MAIN_TAB + ")" + 
                " AND c.allow_publishing = 1 " +
				" AND cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE;

			if (orderby.Trim ().Length > 0)
			{
				sqlSelect += " ORDER BY " + orderby;
			}

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@userId", userId);
            return dbUtility.GetPagedDataTableUnion (sqlSelect, "", parameters, pageNumber, pageSize);
		}

        /// <summary>
        /// GetPublishChannels
        /// </summary>
        /// <returns></returns>
        public static DataTable GetPublishChannels(int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string selectList = " c.community_id, c.name, c.is_personal, c.creator_id, c.creator_username, " +
                " c.creator_username as username, c.is_adult, c.is_public, c.keywords, c.description, " +
                " c.name_no_spaces, c.thumbnail_small_path, " +
                " cs.number_of_members ";

            string tableList = " communities c " +
                " INNER JOIN community_members cm ON c.community_id = cm.community_id " +
                " INNER JOIN channel_stats cs ON c.community_id = cs.channel_id ";

            string sqlSelect = "SELECT " + selectList +
                " FROM " + tableList +
                " WHERE c.status_id = " + (int)Constants.eCOMMUNITY_STATUS.ACTIVE +
                " AND cm.user_id = @userId " +
                //" AND c.category_id NOT IN (" + (int) Constants.eCOMMUNITY_CATEGORIES.MAIN_TAB + ")" + 
                " AND cm.active_member = 'Y' " +
                " AND cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE;

            sqlSelect += " UNION " +
                " SELECT " + selectList +
                " FROM " + tableList +
                " WHERE c.status_id = " + (int)Constants.eCOMMUNITY_STATUS.ACTIVE +
                " AND cm.user_id = @userId " +
                //" AND c.category_id NOT IN (" + (int) Constants.eCOMMUNITY_CATEGORIES.MAIN_TAB + ")" + 
                " AND c.allow_publishing = 1 " +
                " AND cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE;

                sqlSelect += " ORDER BY c.name ";


            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);
            return KanevaGlobals.GetDatabaseUtility().GetDataTable(sqlSelect, parameters);
        }

		/// <summary>
		/// Get a community
		/// </summary>
		/// <param name="urlName"></param>
		/// <returns></returns>
		public static DataRow GetChannelFromURL (string urlName)
		{
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

			string sqlSelect = "SELECT c.community_id, c.name_no_spaces, is_personal " +
				" FROM communities c " +
				" WHERE c.url = @urlName";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@urlName", urlName);
			return dbUtility.GetDataRow (sqlSelect, parameters, false);
		}

		public static void UpdateCommunityNotifications( int communityId, int userId, int notifications)
		{
            string sqlSelect = "CALL update_community_notifications (@community_id, @user_id, @notifications)";

			Hashtable parameters = new Hashtable ();
			parameters.Add("@community_id", communityId);
			parameters.Add("@user_id", userId);
			parameters.Add("@notifications", notifications);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sqlSelect, parameters);
		}

		/// <summary>
		/// GetCommunityUsers
		/// </summary>
        public static PagedDataTable GetCommunityUsers(int communityId, UInt32 communityMemberStatusId, bool bIncludeSuspended, bool bIncludePending, bool bIncludeDeleted, bool bIncludeRejected, string filter, string orderby, int pageNumber, int pageSize)
		{
			Hashtable parameters = new Hashtable ();		

			string selectList = "cm.community_id, cm.account_type_id, cm.added_date, cm.status_id, " +
				" cm.notifications, cm.item_notify, cm.item_review_notify, cm.blog_notify, cm.blog_comment_notify, " +
				" cm.post_notify, cm.reply_notify, u.gender, u.birth_date, " +
				" u.username, u.email, u.show_email, u.user_id, u.email, u.show_online, " +
				" (u.online & u.show_online) as online, u.location, " +
				" com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, " +
				" cs.number_of_members, cs.number_of_views, cs.number_times_shared, cs.number_of_diggs ";

			string tableList = "users u, community_members cm,  communities_personal com " +
				" INNER JOIN channel_stats cs ON com.community_id = cs.channel_id ";
			
			string whereClause = "u.user_id = cm.user_id " +
				" AND cm.community_id = @communityId " +
				" AND com.creator_id = u.user_id ";

				// Did they want a specific status id?
				if (communityMemberStatusId > 0)
				{
                    if (bIncludeSuspended)
                    {
                        whereClause += "AND (cm.status_id = @statusId OR cm.status_id = @statusId2) ";
                        parameters.Add("@statusId", communityMemberStatusId);
                        parameters.Add("@statusId2", (UInt32)CommunityMember.CommunityMemberStatus.LOCKED);
                    }
                    else
                    {
                        whereClause += " AND cm.status_id = @statusId ";
                        parameters.Add("@statusId", communityMemberStatusId);
                    }
				}
				else
				{
					// All Status's, but maybe not deleted or pending
                    // Suspended?
                    if (!bIncludeSuspended)
                    {
                        whereClause += " AND cm.status_id <> " + (UInt32)CommunityMember.CommunityMemberStatus.LOCKED;
                    }
                    // Deleted?
                    if (!bIncludeDeleted)
                    {
                        whereClause += " AND cm.status_id <> " + (UInt32)CommunityMember.CommunityMemberStatus.DELETED;
                    }
                    // Pending?
					if (!bIncludePending)
					{
						whereClause += " AND cm.status_id <> " + (UInt32) CommunityMember.CommunityMemberStatus.PENDING;
					}
					// Rejected?
					if (!bIncludeRejected)
					{
						whereClause += " AND cm.status_id <> " + (UInt32) CommunityMember.CommunityMemberStatus.REJECTED;
					}
				}

			// Filter it?
			if (filter.Length > 0)
			{
				whereClause += " AND " + filter;
			}
			
			parameters.Add ("@communityId", communityId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderby, parameters, 
					pageNumber, pageSize);
		}

		/// <summary>
		/// Update a community
		/// </summary>
		public static int UpdateCommunity (int communityId, bool over21)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "CALL update_communities_over_21 (@communityId, @over21required)";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@over21required", over21 ? "Y" : "N");
			dbUtility.ExecuteNonQuery (sqlString, parameters);
			return 0;
		}

		/// <summary>
		/// Update a community
		/// </summary>
		public static int DeleteCommunityThumbs (int communityId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			
			string sqlString = "UPDATE communities " +
				" SET " +
				" thumbnail_path = @thumbnailPath," +
				" thumbnail_type = @thumbnailType, " +
				" thumbnail_small_path = @thumbnailSmallPath," +
				" thumbnail_medium_path = @thumbnailMediumPath," +
				" thumbnail_large_path = @thumbnailLargePath," +
				" thumbnail_xlarge_path = @thumbnailXLargePath, " +
				" has_thumbnail = 'N' " +
				" WHERE community_id = @communityId";

			Hashtable parameters = new Hashtable ();

			parameters.Add ("@thumbnailPath", DBNull.Value);
			parameters.Add ("@thumbnailSmallPath", "");
			parameters.Add ("@thumbnailMediumPath", "");
			parameters.Add ("@thumbnailLargePath", "");
			parameters.Add ("@thumbnailXLargePath", "");
			parameters.Add ("@thumbnailType", "");
			parameters.Add ("@communityId", communityId);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
			return 0;
		}

		/// <summary>
		/// Update a community
		/// </summary>
		public static int UpdateCommunityThumb (int communityId, string thumbnailPath, string dbColumn)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			
			string sqlString = "UPDATE communities " +
				" SET " +
				dbColumn + " = @thumbnailPath " +
				" WHERE community_id = @communityId";

			Hashtable parameters = new Hashtable ();

			if (thumbnailPath.Trim ().Length.Equals (0))
			{
				parameters.Add ("@thumbnailPath", DBNull.Value);
			}
			else
			{
				parameters.Add ("@thumbnailPath", thumbnailPath);
			}

			parameters.Add ("@communityId", communityId);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
			return 0;
		}

		/// <summary>
		/// Update a community
		/// </summary>
		public static int UpdateCommunity (int communityId, int categoryId, int placeId, string name, string description,
            string isPublic, string isAdult, string backgroundRGB, string backgroundImage, int allowPublishing, bool isPersonal)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();
			string nameNoSpaces = name.Replace (" ", "");

			parameters.Add ("@communityId", communityId);
			parameters.Add ("@nameNoSpaces", nameNoSpaces);

			// Make sure this community name does not already exist
			// check is_personal flag too, only checking uniqueness within personal channels or topic channels

			string sqlString = "";

			if (isPersonal)
			{
				sqlString = "SELECT COUNT(*) " +
					" FROM communities_personal " +
					" WHERE name_no_spaces = @nameNoSpaces " +
					" AND community_id <> @communityId "; 
			}
			else
			{
				sqlString = "SELECT COUNT(*) " +
					" FROM communities_public " +
					" WHERE name_no_spaces = @nameNoSpaces " +
					" AND community_id <> @communityId "; 
			}

			int count = dbUtility.ExecuteScalar (sqlString, parameters);
			if (count > 0)
			{
				return 1;
			}

            sqlString = "CALL update_communities (@communityId, @categoryId, @placeId, " +
                " @nameNoSpaces, @isPublic, @isAdult, @backgroundRGB, " +
                " @backgroundImage, @allowPublishing, " + (int)Constants.eCOMMUNITY_STATUS.ACTIVE + ")";

			parameters.Add ("@categoryId", categoryId);
            parameters.Add ("@placeId", placeId);
			parameters.Add ("@isPublic", isPublic);
			parameters.Add ("@isAdult", isAdult);
            parameters.Add("@backgroundRGB", backgroundRGB);
            parameters.Add("@backgroundImage", backgroundImage);
            parameters.Add("@allowPublishing", allowPublishing); 
            
            dbUtility.ExecuteNonQuery(sqlString, parameters);

			// Split out into two updates due to INNO db split and keeping Full Text Indexes
			UpdateCommunityFT (communityId, name, description);

			return 0;
		}

		/// <summary>
		/// UpdateCommunity
		/// </summary>
		public static int UpdateCommunityFT (int communityId, string name, string description)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

            string sqlString = "CALL update_communities_name (@communityId, @name, @description)";

			parameters.Add ("@communityId", communityId);
			parameters.Add ("@name", name);
			parameters.Add ("@description", description);

			return dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

		///<summary>
		///UpdateNewControlPanelTitle
		///</summary>
		public static int UpdateNewControlPanelTitle (string module_page_id, string title)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			string sqlString = "UPDATE layout_module_channel_control_panel " +
				" SET " +
				" title = @title" +
				" WHERE module_page_id = @module_page_id" +
				" AND title = 'new'";

			parameters.Add ("@module_page_id", module_page_id);
			parameters.Add ("@title", title);			

			return dbUtility.ExecuteNonQuery (sqlString, parameters);
		
		}

		/// <summary>
        /// Insert a new community member.
        /// (Deprecated - Consider using JoinCommunity(int communityId, int userId, bool isAdmin) in CommunityFacade)
		/// </summary>
		public static int InsertCommunityMember (int communityId, int userId, int accountTypeId, string newsletter,
			int invitedByUserId, string allowAssetUploads, string allowForumUse, UInt32 statusId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();	
			string sInvitedByUser = (invitedByUserId == 0)? "NULL": invitedByUserId.ToString ();

			// Make sure this community name does not already exist
			string sqlString = "SELECT count(*) FROM community_members WHERE " +
				" user_id = @userId " +
				" AND community_id = @communityId ";

			parameters.Add ("@communityId", communityId);
			parameters.Add ("@userId", userId);
			int count = dbUtility.ExecuteScalar (sqlString, parameters);
			if (count > 0)
			{
				return 1;
			}

            string sql = "CALL add_community_members (@communityId, @userId, @accountTypeId, @statusId, " +
                " @newsletter, " + sInvitedByUser + ", @allowAssetUploads, @allowForumUse," +
                " NULL, 0, 0, 0," +
                " 0, 0, 0, 0)";

            parameters.Add("@accountTypeId", accountTypeId);
            parameters.Add("@statusId", statusId);
            parameters.Add("@newsletter", newsletter);
            parameters.Add("@allowAssetUploads", allowAssetUploads);
            parameters.Add("@allowForumUse", allowForumUse);
            dbUtility.ExecuteNonQuery(sql, parameters);

            // Fame Packet -- Community members join
            DataRow drComm = GetCommunity (communityId);
            try
            {
                if (drComm != null)
                {
                    // Was it newly created?  Will return empty drComm for status new.
                    if (!drComm["creator_id"].Equals(DBNull.Value))
                    {
                        // Don't award fame when user is joined to his own community by the system.
                        // All users are made members of their personal channel by default
                        if (Convert.ToInt32(drComm["creator_id"]) != userId)
                        {
                            FameFacade fameFacade = new FameFacade();
                            fameFacade.RedeemPacketCommunityRelated(communityId, (int)PacketId.WEB_COMM_JOIN, (int)FameTypes.World);
                        }
                    }
                }
                else
                {
                    m_logger.Error ("Did not award Packet WEB_COMM_JOIN, GetCommunity(" + communityId.ToString () + ") returned a null data row.  Fame was NOT awarded.");
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error awarding Packet WEB_COMM_JOIN, ownerId=" + drComm["creator_id"].ToString () + ", communityId=" + communityId.ToString (), ex);
            }

            
            // Blast it
			if (statusId.Equals ((UInt32) CommunityMember.CommunityMemberStatus.ACTIVE))
			{
				DataRow drChannel = GetCommunity (communityId);

                // Was it newly created?  Will return empty drComm for status new.
                if (!drChannel["creator_id"].Equals(DBNull.Value))
                {
                    UserFacade userFacade = new UserFacade();
                    User user = userFacade.GetUser(userId);

                    if (!IsChannelPersonal(drChannel) && user.BlastPrivacyJoinCommunity)
                    {
                        BlastFacade blastFacade = new BlastFacade();
                        blastFacade.SendCommunityJoinBlast(userId, user.Username, user.NameNoSpaces, drChannel["name"].ToString(), drChannel["name_no_spaces"].ToString());
                    }
                }
			}

			return 0;
		}

		/// <summary>
		/// Get a list of the available broadband channels
		/// that are public and broad band (non-personal) intended for pulldown
		/// </summary>
		/// <returns></returns>
		public static DataTable GetAllPublicChannels ()
		{
			return GetAllPublicChannels("");
		}

        /// <summary>
        /// Get a list of the available broadband channels
        /// that are public and broad band (non-personal) intended for pulldown
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllPublicChannels(string filter)
        {
            string sqlSelect = "SELECT community_id, name_no_spaces, name, last_edit " +
                " FROM communities_public " +
                " WHERE is_public = @isPublic " +
                " AND status_id = " + (int)Constants.eCOMMUNITY_STATUS.ACTIVE;

            if ((filter != null) && (filter != ""))
            {
                sqlSelect += " AND " + filter;
            }

            Hashtable parameters = new Hashtable();
            parameters.Add("@isPublic", "Y");

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// AcceptAllPendingMembers
        /// </summary>
        public static int AcceptAllPendingMembers (int communityId)
        {
            int result = 0;
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlString = "CALL update_community_members_all_pending (@communityId)";

            Hashtable parameters = new Hashtable();
            parameters.Add("@communityId", communityId);
            parameters.Add("@statusId", (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE);
            parameters.Add("@oldStatusId", (UInt32) CommunityMember.CommunityMemberStatus.PENDING);
            result = dbUtility.ExecuteNonQuery(sqlString, parameters);
            return result;
        }

        /// <summary>
        /// UpdateCommunityMember status 
        /// (Deprecated - Use identical function in CommunityFacade.)
        /// </summary>
        public static int UpdateCommunityMember(int communityId, int userId, UInt32 statusId)
        {
            int result = 0;
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            Hashtable parameters = new Hashtable();

            string sqlString = "CALL update_community_members_status (@communityId, @userId, @statusId)";
            parameters.Add("@communityId", communityId);
            parameters.Add("@userId", userId);
            parameters.Add("@statusId", statusId);
            result = dbUtility.ExecuteNonQuery(sqlString, parameters);

            // Handle case if they are moderator
            if (statusId.Equals((UInt32)CommunityMember.CommunityMemberStatus.DELETED) || statusId.Equals((UInt32)CommunityMember.CommunityMemberStatus.LOCKED))
            {
                UpdateCommunityMember (communityId, userId, CommunityMember.CommunityMemberAccountType.SUBSCRIBER);
            }


            // Blast it
            if (statusId.Equals((UInt32)CommunityMember.CommunityMemberStatus.ACTIVE))
            {
                DataRow drChannel = GetCommunity(communityId);

                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(userId);
                if (!IsChannelPersonal(drChannel) && user.BlastPrivacyJoinCommunity)
                {
                    BlastFacade blastFacade = new BlastFacade();
                    blastFacade.SendCommunityJoinBlast(userId, user.Username, user.NameNoSpaces, drChannel["name"].ToString(), drChannel["name_no_spaces"].ToString());
                }
            }

            return result;
        }

		/// <summary>
        /// UpdateCommunityMember status 
        /// (Deprecated - Use identical function in CommunityFacade.)
		/// </summary>
		public static int UpdateCommunityMember (int communityId, int userId, int accountTypeId, string newsletter,
			string allowAssetUploads, string allowForumUse)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "CALL update_community_members_preferences (@communityId, @userId, " +
                    " @accountTypeId, @newsletter, @allowAssetUploads, " +
                    " @allowForumUse );";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@userId", userId);
			parameters.Add ("@accountTypeId", accountTypeId);
			parameters.Add ("@newsletter", newsletter);
			parameters.Add ("@allowAssetUploads", allowAssetUploads);
			parameters.Add ("@allowForumUse", allowForumUse);
			return dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// UpdateCommunityMember status
        /// (Deprecated - Use identical function in CommunityFacade.)
		/// </summary>
		public static void UpdateCommunityMember (int communityId, int userId, CommunityMember.CommunityMemberAccountType accountType)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "CALL update_community_members_account_type (@communityId, @userId, @accountTypeId)";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@userId", userId);
			parameters.Add ("@accountTypeId", (int) accountType);
			int result = dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// DeleteAllCommunityMembers status
		/// </summary>
		public static int DeleteAllCommunityMembers (int communityId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "CALL update_community_members_delete_all (@communityId)";
			
			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			dbUtility.ExecuteNonQuery (sqlString, parameters);

			return 0;
		}

		/// <summary>
		/// Get a community member
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
        [Obsolete("Use new Business Layer")]
		public static DataRow GetCommunityMember (int communityId, int userId)
		{
			string sqlString = "SELECT community_id, user_id, account_type_id, added_date, status_id, " +
				" newsletter, invited_by_user_id, allow_asset_uploads, allow_forum_use " +
				" FROM community_members WHERE " +
				" user_id = @userId " +
				" AND community_id = @communityId";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlString, parameters, false);
		}

		/// <summary>
		/// Delete a community
		/// </summary>
		/// <param name="assetId"></param>
		public static int DeleteCommunity (int communityId, int userId)
		{
			// Make sure they are an admin or owner of the community
			if (!IsCommunityOwner (communityId, userId))
			{
				return 1;
			}

			// Delete the members
			DeleteAllCommunityMembers (communityId);

			// Delete the assets
			DataTable dtAssets = StoreUtility.GetAssets (communityId);
			for (int i = 0; i < dtAssets.Rows.Count; i ++)
			{
				//StoreUtility.DeleteAsset (Convert.ToInt32 (dtAssets.Rows [i]["asset_id"]), userId);
				StoreUtility.RemoveAssetFromChannel (userId, Convert.ToInt32 (dtAssets.Rows [i]["asset_id"]), communityId);
			}

			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "CALL update_communities_delete (@communityId, " + (int)Constants.eCOMMUNITY_STATUS.DELETED + ", @mode);";
			Hashtable parameters = new Hashtable ();
			parameters.Add("@communityId", communityId);
			parameters.Add("@mode",        "deletegamerecords");
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			return 0;
		}

		/// <summary>
		/// get channels to migrate thumbnails
		/// </summary>
		/// <returns></returns>
		public static DataTable GetChannelImagesGenThumbs (int isPersonal)
		{
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

			string sb = "SELECT community_id, thumbnail_path, thumbnail_type, creator_id " +
				" FROM communities " +
				" WHERE thumbnail_path IS NOT NULL " +
				" AND is_personal = @isPersonal ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@isPersonal", isPersonal);
			return dbUtility.GetDataTable (sb, parameters);
		}

		/// <summary>
		/// get ContestProfileHeaders widgets to migrate Banners
		/// </summary>
		/// <returns></returns>
		public static DataTable GetContestProfileHeaderBanners ()
		{
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

			string sb = "SELECT lm.module_page_id, lm.banner, com.community_id " +
				" FROM layout_module_contest_profile_header lm " +
				" INNER JOIN layout_page_modules m ON m.module_page_id = lm.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" INNER JOIN communities com ON lp.channel_id = com.community_id " +
				" WHERE lm.banner IS NOT NULL " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CONTEST_PROFILE_HEADER);
			return dbUtility.GetDataTable (sb, parameters);
		}

		/// <summary>
		/// get title widgets to migrate Banners
		/// </summary>
		/// <returns></returns>
		public static DataTable GetTitleBanners ()
		{
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

			string sb = "SELECT lm.module_page_id, lm.banner, com.community_id " +
				" FROM layout_module_title_text lm " +
				" INNER JOIN layout_page_modules m ON m.module_page_id = lm.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" INNER JOIN communities com ON lp.channel_id = com.community_id " +
				" WHERE lm.banner IS NOT NULL " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.TITLE_TEXT);
			return dbUtility.GetDataTable (sb, parameters);
		}

		/// <summary>
		/// Verify this user has access to the community
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static bool HasCommunityAccess (int communityId, int userId, bool isAuthenticated, bool IsAdult)
		{
			// Administrator always has rights to access
			if (UsersUtility.IsUserAdministrator ())
			{
				return true;
			}

			// Get the community in question 
			DataRow drCommunity = GetCommunity (communityId);


			// If it adult content, and logged in, and not an adult.
            if (drCommunity["is_adult"].ToString().Equals(Constants.CONTENT_TYPE_ADULT) && isAuthenticated && !IsAdult)
			{
				return false;
			}

            //// If it is a pay community, see if they have a subscription
            //// If it is a pay community, make sure they have an active subscription
            //if (IsPayCommunity (communityId))
            //{
            //    return HasActiveCommunitySubscription (communityId, userId);
            //}

			// If private, make sure they have access
			if (!CommunityUtility.IsCommunityPublic (drCommunity))
			{
				// Make sure they are a member, owner, or moderator
				return (CommunityUtility.IsActiveCommunityMember (communityId, userId));
			}

			return true;
		}

		/// <summary>
		/// Is the specified community public?
		/// </summary>
		/// <param name="communityId"></param>
		/// <returns></returns>
		public static bool IsCommunityPublic (int communityId)
		{
			DataRow drCommunity = GetCommunity (communityId);
			return (IsCommunityPublic (drCommunity));
		}

		/// <summary>
		/// Is the specified community public?
		/// </summary>
		/// <param name="drCommunity"></param>
		/// <returns></returns>
		public static bool IsCommunityPublic (DataRow drCommunity)
		{
			return (IsCommunityPublic (drCommunity ["is_public"].ToString ()));
		}

		/// <summary>
		/// Is the specified community public?
		/// </summary>
		public static bool IsCommunityPublic (string IsPublic)
		{
			return (IsPublic.Equals ("Y"));
		}

		/// <summary>
		/// Is the user a communty member?
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static bool IsActiveCommunityMember (int communityId, int userId)
		{
			return IsActiveCommunityMember (communityId, userId, true);
		}

		/// <summary>
		/// Is the user a communty member?
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static bool IsActiveCommunityMember (int communityId, int userId, bool bAlwaysTrueIfAdmin)
		{
			// Administrator always has rights to access
			if (UsersUtility.IsUserAdministrator () && bAlwaysTrueIfAdmin)
			{
				return true;
			}

			// Everyone is active for watch/play/create/support
			if (IsKanevaMainCommunity (communityId))
			{
				return true;
			}

            //// If it is a pay community, make sure they have an active subscription
            //if (IsPayCommunity (communityId))
            //{
            //    return HasActiveCommunitySubscription (communityId, userId);
            //}

			string sqlSelect = "SELECT COUNT(*) " +
				" FROM community_members " +
				" WHERE user_id = @userId " +
				" AND community_id = @communityId " + 
				" AND status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE;

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@userId", userId);
			int result = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlSelect, parameters);
			return (result > 0);
		}

        /// <summary>
		/// Is the users membership pending
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static bool IsMembershipPending (int communityId, int userId)
		{
           bool pending = false;
           CommunityFacade communityFacade = new CommunityFacade();
           CommunityMember communityMember = communityFacade.GetCommunityMember(communityId, userId);

           if (communityMember.StatusId == (UInt32)CommunityMember.CommunityMemberStatus.PENDING)
           {
               pending = true;
           }

           return pending;
        }

		/// <summary>
		/// Is the user a communtiy moderator
		/// </summary>
		/// <param name="role"></param>
		/// <returns></returns>
        //[Obsolete("Use new Business Layer")]
		public static bool IsCommunityModerator (int communityId, int userId)
		{
			return IsCommunityModerator (communityId, userId, true);
		}

		/// <summary>
		/// Is the user a communtiy moderator
		/// </summary>
		/// <param name="role"></param>
		/// <returns></returns>
        //[Obsolete("Use new Business Layer")]
		public static bool IsCommunityModerator (int communityId, int userId, bool bAlwaysTrueIfAdmin)
		{
			// Administrator always has rights to access
			if (bAlwaysTrueIfAdmin && UsersUtility.IsUserAdministrator ())
			{
				return true;
			}

            CommunityFacade communityFacade = new CommunityFacade();
            CommunityMember communityMember = communityFacade.GetCommunityMember(communityId, userId);

            if (!communityMember.UserId.Equals (0))
			{
                int accountTypeID = communityMember.AccountTypeId;
                UInt32 statusId = communityMember.StatusId;

				// Owner is also always a moderator
                if (statusId.Equals((UInt32)CommunityMember.CommunityMemberStatus.ACTIVE) && (accountTypeID.Equals((int)CommunityMember.CommunityMemberAccountType.MODERATOR) || accountTypeID.Equals((int)CommunityMember.CommunityMemberAccountType.OWNER)))
				{
					return true;
				}
			}

			return (false);
		}

		/// <summary>
		/// Is the user a communtiy owner
		/// </summary>
		/// <param name="role"></param>
		/// <returns></returns>
		public static bool IsCommunityOwner (int communityId, int userId)
		{
			return IsCommunityOwner (communityId, userId, true);
		}

		/// <summary>
		/// Is the user a communtiy owner
		/// </summary>
		/// <param name="role"></param>
		/// <returns></returns>
		public static bool IsCommunityOwner (int communityId, int userId, bool bAlwaysTrueIfAdmin)
		{
			// Administrator always has rights to access
			if (bAlwaysTrueIfAdmin && UsersUtility.IsUserAdministrator ())
			{
				return true;
			}

            CommunityFacade communityFacade = new CommunityFacade();
            CommunityMember communityMember = communityFacade.GetCommunityMember(communityId, userId);

            if (!communityMember.UserId.Equals (0))
			{
                int accountTypeID = communityMember.AccountTypeId;
                UInt32 statusId = communityMember.StatusId;

                if (statusId.Equals((UInt32)CommunityMember.CommunityMemberStatus.ACTIVE) && accountTypeID.Equals((int)CommunityMember.CommunityMemberAccountType.OWNER))
				{
					return true;
				}
			}

			return (false);
		}

		/// <summary>
		/// TransferChannel
		/// </summary>
		public static bool TransferChannel (int channelId, int userId, int newOwnerId, int ownerId)
		{
			if (IsCommunityOwner (channelId, userId))
			{
				Hashtable parameters = new Hashtable ();
				DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

				// Make the old owner just a member
				UpdateCommunityMember (channelId, ownerId, CommunityMember.CommunityMemberAccountType.SUBSCRIBER);

                // Make the new owner owner
                UpdateCommunityMember(channelId, newOwnerId, CommunityMember.CommunityMemberAccountType.OWNER);

                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(newOwnerId);

                string sqlString = "CALL update_communities_owner (@communityId, @ownerId, @username)";
			
				parameters.Add ("@communityId", channelId);
				parameters.Add ("@ownerId", newOwnerId);
                parameters.Add("@username", user.Username);
				dbUtility.ExecuteNonQuery (sqlString, parameters);

				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Get the type of member from a given accountTypeId and status
		/// </summary>
		/// <param name="accountTypeId"></param>
		/// <returns></returns>
		public static string GetMemberTypeDescription (object accountTypeId, object cmStatusId)
		{
			if (accountTypeId != DBNull.Value)
			{
				// Make sure thier status is active
				if (cmStatusId != DBNull.Value)
				{
                    switch ((UInt32)Convert.ToUInt32(cmStatusId))
					{
						case ((UInt32) CommunityMember.CommunityMemberStatus.DELETED):
						{
							return "";
						}
						case ((UInt32) CommunityMember.CommunityMemberStatus.LOCKED):
						{
							return "*locked";
						}
						case ((UInt32) CommunityMember.CommunityMemberStatus.PENDING):
						{
							return "";
						}
					}
				}

				return GetMemberAccountTypeDescription (accountTypeId);
			}

			return "";
		}

		/// <summary>
		/// GetMemberAccountTypeDescription
		/// </summary>
		/// <param name="accountTypeId"></param>
		/// <returns></returns>
		public static string GetMemberAccountTypeDescription (object accountTypeId)
		{
			if (accountTypeId == DBNull.Value)
				return "";

			switch ((int) Convert.ToInt32 (accountTypeId))
			{
				case ((int) CommunityMember.CommunityMemberAccountType.OWNER):
				{
					return "*owner";
				}
				case ((int) CommunityMember.CommunityMemberAccountType.MODERATOR):
				{
					return "*moderator";
				}
				case ((int) CommunityMember.CommunityMemberAccountType.SUBSCRIBER):
				{
					return "*member";
				}
				default:
				{
					return "Unknown Account Type";
				}
			}
		}

		public static string GetMemberStatusIdDescription (object statusId)
		{
			// Make sure thier status is active
			if (statusId != DBNull.Value)
			{
				switch ((UInt32) Convert.ToUInt32 (statusId))
				{
					case ((UInt32) CommunityMember.CommunityMemberStatus.DELETED):
					{
						return "*deleted";
					}
					case ((UInt32) CommunityMember.CommunityMemberStatus.LOCKED):
					{
						return "*locked";
					}
					case ((UInt32) CommunityMember.CommunityMemberStatus.PENDING):
					{
						return "*pending";
					}
					case ((UInt32) CommunityMember.CommunityMemberStatus.ACTIVE):
					{
						return "*active";
					}
				}
			}
			return "Unknown";
		}

		/// <summary>
		/// Get the community status
		/// </summary>
		/// <param name="isPublic"></param>
		/// <returns></returns>
		public static string GetStatus (string isPublic)
		{
			if (Convert.ToString (isPublic).Equals ("Y"))
			{
				return "Public";
			}
			else
			{
				return "Private";
			}
		}

		/// <summary>
		/// GetPendingCommunityMembersForUser
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static int GetPendingCommunityMembersForUser (int userOwnderId)
		{
			string sqlSelect = "SELECT COALESCE(COUNT(*),0) " +
				" FROM community_members cm, communities c " +
				" WHERE c.creator_id = @userId " + 
				" AND c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE + 
				" AND cm.community_id = c.community_id " +
				" AND cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.PENDING;

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@userId", userOwnderId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().ExecuteScalar(sqlSelect, parameters);
		}

		/// <summary>
		/// GetPendingCommunitiesForUser
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static int GetPendingCommunitiesForUser (int userId)
		{
			string sqlSelect = "SELECT COALESCE(COUNT(*),0) " +
				" FROM community_members cm, communities c " +
				" WHERE cm.user_id = @userId " + 
				" AND c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE + 
				" AND cm.community_id = c.community_id " +
				" AND cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.PENDING;

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().ExecuteScalar(sqlSelect, parameters);
		}

		/// <summary>
		/// UpdateChannelTags that are stored in the community table
		/// </summary>
		/// <param name="channelId"></param>
		public static void UpdateChannelTags (int channelId, string strKeywords)
		{
            string sqlUpdate = "CALL update_communities_keywords (@channelId, @keywords)";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);
			parameters.Add ("@keywords", strKeywords.Trim ());
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlUpdate, parameters);
		}

		/// <summary>
		/// GetChannelTags
		/// </summary>
		/// <returns></returns>
		private static DataTable GetChannelTags (int channelId)
		{
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

			string sqlSelect = "SELECT c.keywords " +
				" FROM communities c " +
				" WHERE c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE +
				" AND c.community_id = @channelId";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);
			return dbUtility.GetDataTable (sqlSelect, parameters);
		}

        ///// <summary>
        ///// GetCommunityStats
        ///// </summary>
        ///// <param name="communityId"></param>
        ///// <returns></returns>
        //public static DataRow GetCommunityStats(int communityId)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

        //    string strSQL = " SELECT u.signup_date, u.last_login, u.second_to_last_login, " +
        //        " cs.number_of_views, cs.number_of_members, cs.number_times_shared, 0 as number_of_topics, " +
        //        " SUM(ac.count) as num_assets, " +
        //        " c.creator_id, c.creator_username, c.created_date, c.last_edit, c.last_update, c.is_personal,  " +
        //        " c.name, c.thumbnail_small_path, c.thumbnail_medium_path, c.thumbnail_large_path " +
        //        " FROM summary_assets_by_type_by_channel ac " +
        //        " INNER JOIN communities c ON c.community_id = ac.channel_id " +
        //        " INNER JOIN channel_stats cs ON cs.channel_id = c.community_id " +
        //        " INNER JOIN users u ON u.user_id = c.creator_id " +
        //        " WHERE c.community_id = @communityId " +
        //        " GROUP BY ac.channel_id ";


        //    Hashtable parameters = new Hashtable();
        //    parameters.Add("@communityId", communityId);
        //    return dbUtility.GetDataRow(strSQL, parameters, false);
        //}

		/// <summary>
		/// IsKanevaMainCommunity
		/// </summary>
		public static bool IsKanevaMainCommunity (int communityId)
		{
			// Everyone is active for watch/play/create/support
			if (communityId == (int) Constants.eCOMMUNITIES.PLAY || communityId == (int) Constants.eCOMMUNITIES.WATCH || communityId == (int) Constants.eCOMMUNITIES.CREATE || communityId == (int) Constants.eCOMMUNITIES.SUPPORT || communityId == (int) Constants.eCOMMUNITIES.FILMMAKERS)
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// returns true if community exists
		/// </summary>
		/// <param name="communityId"></param>
		/// <returns></returns>
		public static bool IsCommunityValid(int communityId)
		{
			string sqlSelect = " SELECT COUNT(*) " +
				" FROM communities " +
				" WHERE community_id = @community_id";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@community_id", communityId);

			return KanevaGlobals.GetDatabaseUtility().ExecuteScalar(sqlSelect, parameters) > 0;
		}

		/// <summary>
		/// GetChannelFilterType
		/// </summary>
		public static Constants.eCHANNEL_FILTER_TYPE GetChannelFilterType (int type)
		{
			switch (type)
			{
				case (int) Constants.eCHANNEL_FILTER_TYPE.OWNER:
					return Constants.eCHANNEL_FILTER_TYPE.OWNER;

				case (int) Constants.eCHANNEL_FILTER_TYPE.SUBSCRIBER:
					return Constants.eCHANNEL_FILTER_TYPE.SUBSCRIBER;

				case (int) Constants.eCHANNEL_FILTER_TYPE.PUBLIC:
					return Constants.eCHANNEL_FILTER_TYPE.PUBLIC;

				case (int) Constants.eCHANNEL_FILTER_TYPE.PRIVATE:
					return Constants.eCHANNEL_FILTER_TYPE.PRIVATE;

				case (int) Constants.eCHANNEL_FILTER_TYPE.ADULT:
					return Constants.eCHANNEL_FILTER_TYPE.ADULT;

				case (int) Constants.eCHANNEL_FILTER_TYPE.TEEN:
					return Constants.eCHANNEL_FILTER_TYPE.TEEN;

				case (int) Constants.eCHANNEL_FILTER_TYPE.GENERAL:
					return Constants.eCHANNEL_FILTER_TYPE.GENERAL;

				case (int) Constants.eCHANNEL_FILTER_TYPE.MODERATOR:
					return Constants.eCHANNEL_FILTER_TYPE.MODERATOR;
			
				default:
				{
					return Constants.eCHANNEL_FILTER_TYPE.DEFAULT;
				}
			}
		}

		/// <summary>
		/// GetUserCommunityMembershipSql
		/// </summary>
		public static string GetUserCommunityMembershipSql (int userId)
		{
			string sqlString = "SELECT c.community_id " +
				" FROM communities c LEFT OUTER JOIN community_members cm ON c.community_id = cm.community_id " +
			    " WHERE cm.user_id = " + userId;
    
			return sqlString;
		}

		// **********************************************************************************************
		// Searching
		// **********************************************************************************************
		#region Search Functions

		public static double GetChannelCount ()
		{
			string sqlSelect = "SELECT COUNT(*)" +
				" FROM communities_public c " +
				" WHERE c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE +
				" AND c.community_id NOT IN (" + (int) Constants.eCOMMUNITIES.WATCH + ", " + (int) Constants.eCOMMUNITIES.PLAY + ", " + (int) Constants.eCOMMUNITIES.CREATE + ", " + (int) Constants.eCOMMUNITIES.SUPPORT + ", " + (int) Constants.eCOMMUNITIES.FILMMAKERS + ")";

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().ExecuteScalarDouble(sqlSelect, new Hashtable());
		}

		/// <summary>
		/// Get Communities for notifications
		/// </summary>
		public static PagedDataTable GetCommunitiesForNotifications ( 
			string filter, string orderBy, int pageNumber, int pageSize)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly ();
			Hashtable parameters = new Hashtable ();

			string sqlSelectList = "DISTINCT c.community_id, " +
				" c.name, c.description, c.is_public, c.is_adult, c.keywords, c.over_21_required, " +
				" c.creator_id, c.creator_username as username, c.created_date, " +
				" c.name_no_spaces, c.thumbnail_small_path, " +
				" creator_c.name_no_spaces as creator_name_no_spaces, creator_c.thumbnail_small_path as creator_thumbnail_small_path, " +
				" cs.number_of_members, cs.number_of_views, cs.number_times_shared, cs.number_of_diggs ";

			string sqlTableList = "communities_personal creator_c, communities_public c " +
				" INNER JOIN channel_stats cs ON cs.channel_id = c.community_id ";

			string sqlWhereClause = " c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE +
				" AND c.community_id NOT IN (" + (int) Constants.eCOMMUNITIES.WATCH + ", " + (int) Constants.eCOMMUNITIES.PLAY + ", " + (int) Constants.eCOMMUNITIES.CREATE + ", " + (int) Constants.eCOMMUNITIES.SUPPORT + ", " + (int) Constants.eCOMMUNITIES.FILMMAKERS + ")" +
				" AND creator_c.creator_id = c.creator_id ";

			if (filter.Length > 0)
			{
				sqlWhereClause += " AND " + filter;
			}

			return dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderBy, parameters, pageNumber, pageSize);
		}

        ///// <summary>
        ///// Search 3dpps
        ///// </summary>
        //public static PagedDataTable Search3Dapps (bool onlyAccessPass, bool bGetMature, bool bGetOver21, string searchString, 
        //    string country, int pastDays,
        //    string filter, string orderBy, int pageNumber, int pageSize, int iPlaceTypeId, bool myCommunitiesOnly, int userId)
        //{

            //DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilitySearch ();
            //Hashtable parameters = new Hashtable ();

            //string sqlSelectList = " wok.concat_vw_url(c.name,'',0) as STPURL, g.game_id, scr.community_id, 10 as zoneType, " +
            //    " scr.name, scr.description, scr.is_public, scr.is_adult, scr.keywords, scr.over_21_required, " +
            //    " scr.creator_id, scr.username, scr.created_date, " +
            //    " scr.name_no_spaces, scr.thumbnail_small_path, " +
            //    " scr.creator_name_no_spaces, scr.creator_thumbnail_small_path, " +
            //    " scr.number_of_members, scr.number_of_views, scr.number_times_shared, scr.number_of_diggs, gs.server_status_id, last_ping_datetime ";

            //string sqlTableList = " search_kaneva.searchable_communities sc INNER JOIN search_kaneva.searchable_communities_results scr ON sc.community_id = scr.community_id " +
            //    " INNER JOIN kaneva.communities c ON c.community_id = sc.community_id " +
            //    " INNER JOIN kaneva.communities_game cg ON cg.community_id = c.community_id " +
            //    " INNER JOIN developer.games g ON cg.game_id = g.game_id " +
            //    " INNER JOIN developer.game_servers gs ON gs.game_id = g.game_id ";

            //string sqlWhereClause = "c.community_type_id = 3 " +
            //    " AND g.game_status_id = 1  " +
            //    " AND gs.server_status_id = 1   " +
            //    " AND timestampdiff(minute, last_ping_datetime, NOW()) < wok.deadServerInterval() " +
            //    " AND gs.visibility_id = 1 " +
            //    " AND g.game_access_id = 1 " +
            //    " AND gs.is_external = 1 ";

            //if (myCommunitiesOnly)
            //{
            //    sqlTableList += " INNER JOIN kaneva.community_members cm ON c.community_id = cm.community_id AND cm.status_id = " + (UInt32)CommunityMember.CommunityMemberStatus.ACTIVE +
            //        " AND cm.user_id = " + userId;

            //    orderBy = " cm.status_id ASC, cm.last_visited_at DESC ";
            //}

            //if (iPlaceTypeId != -1)
            //{
            //    sqlWhereClause += " AND sc.place_type_id = " + iPlaceTypeId.ToString() + " ";
            //}

            //if (searchString.Length > 0)
            //{
            //    // If no order by, do relevancy
            //    if (orderBy.Length.Equals (0))
            //    {
            //        parameters.Add ("@searchStringOrig", searchString);
            //        sqlSelectList += ", MATCH (sc.name,sc.description,sc.creator_username,sc.keywords) AGAINST (@searchStringOrig) as rel";
            //        orderBy = " rel DESC ";
            //    }

            //    char [] splitter  = {' '};
            //    string [] arKeywords = null;

            //    // Did they enter multiples?
            //    arKeywords = searchString.Split (splitter);

            //    if (arKeywords.Length > 1)
            //    {
            //        searchString = "";
            //        for (int j = 0; j < arKeywords.Length; j ++)
            //        {
            //            searchString += arKeywords [j].ToString () + "* ";
            //        }
            //    }
            //    else
            //    {
            //        searchString = searchString + "*";
            //    }

            //    sqlWhereClause += " AND MATCH (sc.name,sc.description,sc.creator_username,sc.keywords) AGAINST (@searchString IN BOOLEAN MODE)";
            //    parameters.Add ("@searchString", searchString);
            //}

            //// Filter
            //if (!bGetMature)
            //{
            //    sqlWhereClause += " AND scr.is_adult <> 'Y'";
            //}
            //if (bGetMature && onlyAccessPass)
            //{
            //    sqlWhereClause += " AND scr.is_adult = 'Y' ";
            //}

            //// Filter
            //if (!bGetOver21)
            //{
            //    sqlWhereClause += " AND sc.over_21_required = 'N'";
            //}

            //// Filter by country
            //if (country.Trim ().Length > 0)
            //{
            //    sqlWhereClause += " AND sc.creator_country = @country ";
            //    parameters.Add("@country", country);
            //}

            //// Only get last x number of days?
            //if (pastDays > 0)
            //{
            //    sqlWhereClause += " AND sc.created_date > " + dbUtility.GetDatePlusDays(-pastDays);
            //}


            //if (filter.Length > 0)
            //{
            //    sqlWhereClause += " AND " + filter;
            //}

            //return dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderBy, parameters, pageNumber, pageSize);
		//}


		#endregion

		#region member groups

		/// <summary>
		/// return number of members, pending applications and groups
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static DataRow GetMemberCounts(int channelId)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" SELECT COUNT(DISTINCT cm.user_id) AS num_members, ");
			sb.Append(" COUNT(DISTINCT cm_pending.user_id) AS num_pending, ");
			sb.Append(" COUNT(DISTINCT cmg.id) AS num_groups ");
			sb.Append(" FROM communities c ");
			sb.Append(" LEFT JOIN community_members cm ON cm.community_id = c.community_id AND ");
			sb.Append(" cm.status_id = ").Append((UInt32) CommunityMember.CommunityMemberStatus.ACTIVE);
			sb.Append(" LEFT JOIN community_members cm_pending ON cm_pending.community_id = c.community_id ");
			sb.Append(" AND cm_pending.status_id = ").Append((UInt32) CommunityMember.CommunityMemberStatus.PENDING);
			sb.Append(" LEFT JOIN community_member_groups cmg ON cmg.channel_id = c.community_id ");
			sb.Append(" WHERE c.community_id = @channelId");

			Hashtable param = new Hashtable();
			param.Add("@channelId",channelId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sb.ToString(), param, false);
		}

		
		public static DataTable GetMemberGroups(int channelId)
		{
			return GetMemberGroups(channelId, "", "", 1, Int32.MaxValue);
		}


		/// <summary>
		/// Get the member groups for a channel
		/// </summary>
		/// <param name="channelId"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public static PagedDataTable GetMemberGroups (int channelId, string filter, string orderby, 
			int pageNumber, int pageSize)
		{
			string selectList = " mg.id, mg.name, mg.created_datetime, " +
				" (SELECT COUNT(*) " +
				" FROM community_member_group_members mgs, users u " +
				" WHERE mgs.member_group_id = mg.id " +
				" AND mgs.member_id = u.user_id " +
				" AND u.active = 'Y' ) as member_count ";

			string tableList = " community_member_groups mg ";

			string whereClause = " mg.channel_id = @channelId ";		// Members invited by my I confirmed

			// Filter it?
			if (filter.Length > 0)
			{
				whereClause += " AND " + filter;
			}

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// Insert a member group
		/// </summary>
		/// <returns></returns>
		public static int InsertMemberGroup (int channelId, string groupName)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sql = "INSERT INTO community_member_groups " +
				"(channel_id, name, created_datetime " +
				") VALUES (" +
				"@channelId, @groupName, " + dbUtility.GetCurrentDateFunction () + ")";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);
			parameters.Add ("@groupName", groupName);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// Insert a member into a group
		/// </summary>
		/// <returns></returns>
		public static int InsertMemberInGroup (int channelId, int memberGroupId, int memberId)
		{
			// Make sure they are not already in the group
			if (IsMemberInGroup (memberGroupId, memberId))
			{
				return 0;
			}

			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sql = "INSERT INTO community_member_group_members " +
				"(member_group_id, member_id" +
				") VALUES (" +
				"@memberGroupId, @memberId)";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@memberId", memberId);
			parameters.Add ("@memberGroupId", memberGroupId);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// IsMemberInGroup
		/// </summary>
		/// <param name="memberGroupId"></param>
		/// <param name="memberId"></param>
		/// <returns></returns>
		public static bool IsMemberInGroup (int memberGroupId, int memberId)
		{
			string sqlSelect = "SELECT member_group_id " +
				" FROM community_member_group_members " +
				" WHERE member_group_id = @memberGroupId" +
				" AND member_id = @memberId ";

			Hashtable parameters = new Hashtable ();		
			parameters.Add ("@memberGroupId", memberGroupId);
			parameters.Add ("@memberId", memberId);
			DataRow drMemberGroup = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);

			return (drMemberGroup != null);
		}

		/// <summary>
		/// GetMemberGroup
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static DataRow GetMemberGroup (int id)
		{
			string sqlSelect = "SELECT c.channel_id, c.name, c.created_datetime " +
				" FROM community_member_groups c " +
				" WHERE c.id = @id";

			Hashtable parameters = new Hashtable ();		
			parameters.Add ("@id", id);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
		}

		/// <summary>
		/// Remove a member from a group
		/// </summary>
		/// <returns></returns>
		public static int RemoveMemberFromGroup (int memberGroupId, int memberId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sql = "DELETE FROM community_member_group_members " +
				" WHERE member_id = @memberId " +
				" AND member_group_id = @member_group_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@memberId", memberId);
			parameters.Add ("@member_group_id", memberGroupId);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// GetMemberGroup
		/// </summary>
		/// <param name="memberGroupId"></param>
		/// <returns></returns>
		public static PagedDataTable GetMembersInGroup (int memberGroupId, string orderby, int pageNumber, int pageSize)
		{	
			return GetMembersInGroup (memberGroupId, "", orderby, pageNumber, pageSize);
		}
		/// <summary>
		/// GetMemberGroup
		/// </summary>
		/// <param name="memberGroupId"></param>
		/// <returns></returns>
		public static PagedDataTable GetMembersInGroup (int memberGroupId, string filter, string orderby, int pageNumber, int pageSize)
		{
			string selectList = "fgf.member_id, u.username, u.user_id, u.gender, " +
				" (u.online & u.show_online) as online, u.location, cm.added_date, " +
				" cm.account_type_id, cm.community_id, uc.name_no_spaces, uc.thumbnail_small_path, uc.thumbnail_medium_path ";

			string tableList = "community_member_groups fg " +
				" INNER JOIN community_member_group_members fgf ON fg.id = fgf.member_group_id " +
				" INNER JOIN users u ON fgf.member_id = u.user_id " +
				" INNER JOIN communities_personal uc ON uc.creator_id = u.user_id " +
				" INNER JOIN community_members cm ON cm.user_id = u.user_id AND cm.community_id = fg.channel_id";

			string whereClause = "fgf.member_group_id = @memberGroupId" +
				" AND u.active = 'Y'";

			// Filter it?
			if (filter.Length > 0)
			{
				whereClause += " AND " + filter;
			}

			Hashtable parameters = new Hashtable ();		
			parameters.Add ("@memberGroupId", memberGroupId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, 
				orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// Delete a member group
		/// </summary>
		/// <returns></returns>
		public static int DeleteMemberGroup (int id)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Delete all members from group
			DeleteAllMembersFromGroup (id);

			// Delete the member group
			string sql = "DELETE FROM community_member_groups " +
				" WHERE id = @id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// DeleteAllMembersFromGroup
		/// </summary>
		/// <returns></returns>
		public static int DeleteAllMembersFromGroup (int id)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Delete any members in the group
			string sql = "DELETE FROM community_member_group_members " +
				" WHERE member_group_id = @id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		#endregion

        /// <summary>
        /// returns the userId of a given personal channelId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int GetUserIdFromPersonalChannelId(int channelId)
        {
            string sqlSelect = "SELECT creator_id " +
                " FROM communities_personal " +
                " WHERE community_id = @channelId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@channelId", channelId);

            return KanevaGlobals.GetDatabaseUtility().ExecuteScalar(sqlSelect, parameters);
        }

		/// <summary>
		/// returns true if the channel is personal
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static bool IsChannelPersonal (int channelId)
		{
			string sqlSelect = "SELECT is_personal " +
				" FROM communities " +
				" WHERE community_id = @channelId";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);

			DataRow rowChannel = KanevaGlobals.GetDatabaseUtility().GetDataRow(sqlSelect,parameters, false);
			return IsChannelPersonal (rowChannel);
		}

		/// <summary>
		/// returns true if the channel is personal
		/// </summary>
		public static bool IsChannelPersonal (DataRow drChannel)
		{
			return drChannel != null && drChannel ["is_personal"].ToString().Equals("1");
		}

		/// <summary>
		/// returns true if the channel is personal
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static bool IsChannelPublishable (int channelId)
		{
			string sqlSelect = "SELECT allow_publishing " +
				" FROM communities " +
				" WHERE community_id = @channelId";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);

			DataRow rowChannel = KanevaGlobals.GetDatabaseUtility().GetDataRow (sqlSelect,parameters, false);
			return IsChannelPublishable (rowChannel);
		}

		/// <summary>
		/// returns true if the channel is personal
		/// </summary>
		public static bool IsChannelPublishable (DataRow drChannel)
		{
			return drChannel != null && drChannel ["allow_publishing"].ToString().Equals("1");
		}

		// <summary>
		/// return number of times a channel is viewed
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static int GetChannelViewed (int channelId)
		{
			string sqlSelect = "SELECT number_of_views " +
				" FROM channel_stats " +
				" WHERE channel_id = @channelId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);

            return KanevaGlobals.GetDatabaseUtilityReadOnly2().ExecuteScalar(sqlSelect, parameters);
		}

		/// <summary>
		/// GetUserCommentCount
		/// </summary>
		public static int GetUniqueVistors (int channelId)
		{
			string sqlSelect = "SELECT DISTINCT(user_id) FROM community_views upv " +
				" WHERE community_id = @channelId " +
				" AND user_id > 0 ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters).Rows.Count;
		}

      
		/// <summary>
		/// GetTotalChannelStats
		/// </summary>
		public static DataRow GetTotalChannelStats (int user_id, int community_account_type_id )
		{
			string sqlSelect = "SELECT SUM(cs.number_of_members) as number_of_members, SUM(cs.number_of_views) as number_of_views, " +
				" SUM(cs.number_of_diggs) as number_of_diggs, SUM(cs.number_times_shared) as number_times_shared, " +
				" cm.account_type_id, COUNT(c.community_id) as number_of_channels " +
				" FROM users u " +
				" LEFT JOIN community_members cm ON cm.user_id = u.user_id " +
				" LEFT JOIN communities_personal c ON c.community_id = cm.community_id " +
				" INNER JOIN channel_stats cs ON cs.channel_id = c.community_id " +
				" WHERE c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE + 
				" AND cm.account_type_id = @community_account_type_id " +
				" AND cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE + 
				" AND u.user_id = @user_id " +
				" GROUP BY cm.account_type_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@user_id", user_id);
			parameters.Add ("@community_account_type_id", community_account_type_id);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
		}


		#region events
		public enum eEVENT_START_TIME_FILTER
		{
			PAST = 1,
			UPCOMING = 2,
			ALL = 3
		}	

		/// <summary>
		/// Get an event
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static DataRow GetEvent(int id)
		{
			string sqlSelect = "SELECT e.event_id, e.community_id, e.user_id, e.title, e.details, " +
				" e.location, e.start_time, e.end_time, e.type_id, e.time_zone_id FROM events e " +
				" WHERE event_id = @id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <param name="title"></param>
		/// <param name="details"></param>
		/// <param name="location"></param>
		/// <param name="startTime"></param>
		/// <param name="endTime"></param>
		/// <param name="typeId"></param>
		/// <param name="timeZoneId"></param>
		/// <returns></returns>
		public static int InsertEvent(
			int communityId, 
			int userId, 
			string title, 
			string details,
			string location, 
			DateTime startTime, 
			DateTime endTime, 
			int typeId, 
			int timeZoneId,
            string trackingRequestGUID
			)
		{
			string query = " INSERT INTO events " + 
				" ( community_id, user_id, title, details, " +
                " location, start_time, end_time, type_id, time_zone_id, tracking_request_GUID) " +
				" VALUES( @community_id, @user_id, @title, @details, " +
                " @location, @start_time, @end_time, @type_id, @time_zone_id, @tracking_request_GUID) ";

			Hashtable parameters = new Hashtable ();
			parameters.Add("@community_id", communityId);
			parameters.Add("@user_id", userId);
			parameters.Add("@title", title);
			parameters.Add("@details", details);
			parameters.Add("@location", location);
			parameters.Add("@start_time", startTime);
			parameters.Add("@end_time", endTime);
			parameters.Add("@type_id", typeId);
			parameters.Add("@time_zone_id", timeZoneId);
            parameters.Add("@tracking_request_GUID", trackingRequestGUID);

			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);
			return retVal;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="eventId"></param>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <param name="title"></param>
		/// <param name="details"></param>
		/// <param name="location"></param>
		/// <param name="startTime"></param>
		/// <param name="endTime"></param>
		/// <param name="typeId"></param>
		/// <param name="timeZoneId"></param>
		public static void SaveEvent(
			int eventId, 
			int communityId, 
			int userId, 
			string title, 
			string details,
			string location, 
			DateTime startTime, 
			DateTime endTime, 
			int typeId, 
			int timeZoneId
			)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE events ");
			sb.Append(" SET community_id = @community_id, ");
			sb.Append(" user_id= @user_id, ");
			sb.Append(" title= @title, ");
			sb.Append(" details = @details, ");
			sb.Append(" location = @location, ");
			sb.Append(" start_time = @start_time, ");
			sb.Append(" end_time = @end_time, ");
			sb.Append(" type_id = @type_id, ");
			sb.Append(" time_zone_id = @time_zone_id ");
			sb.Append(" WHERE event_id = @id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add("@id", eventId);
			parameters.Add("@community_id", communityId);
			parameters.Add("@user_id", userId);
			parameters.Add("@title", title);
			parameters.Add("@details", details);
			parameters.Add("@location", location);
			parameters.Add("@start_time", startTime);
			parameters.Add("@end_time", endTime);
			parameters.Add("@type_id", typeId);
			parameters.Add("@time_zone_id", timeZoneId);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		public static void DeleteEvent(int id)
		{
			//delete from layout_module_title_text table
			string sqlString = "DELETE FROM events " +
				" WHERE event_id = @id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}
		
		#endregion

		#region messages

		/// <summary>
		/// return number of pending members of channels the user owns or moderates
		/// </summary>
		public static double GetPendingMembersCount (int userId)
		{
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

			string strSQL = " SELECT SUM(cs.number_of_pending_members) " +
				" FROM channel_stats cs, community_members cm " +
				" WHERE cs.channel_id = cm.community_id " +
				" AND cm.has_edit_rights = 'Y' " +
				" AND cm.user_id = @userId " +
				" AND cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE;
			
			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@userId", userId);
			return dbUtility.ExecuteScalarDouble (strSQL, parameters);
		}

		/// <summary>
		/// return number of pending members of channels the user owns or moderates
		/// </summary>
		public static PagedDataTable GetPendingMembers (int userId, string filter, string orderby, int pageNumber, int pageSize)
		{
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();

            string select = " com.thumbnail_small_path, com.thumbnail_square_path, com.name_no_spaces, c.community_id, c.name AS community_name, " +
                " c.name_no_spaces as community_name_no_spaces, cm2.user_id, cm2.added_date, u.username, " +
                " u.gender, u.birth_date, u.age, u.location, NOW() as CurrentDate, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture ";
			
			StringBuilder tableList = new StringBuilder() ;
			tableList.Append(" communities c ");
			tableList.Append(" INNER JOIN community_members cm1 ON c.community_id = cm1.community_id ");
			tableList.Append(" INNER JOIN community_members cm2 ON c.community_id = cm2.community_id ");
			tableList.Append(" INNER JOIN users u ON u.user_id = cm2.user_id ");
            tableList.Append(" LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id ");
			tableList.Append(" INNER JOIN communities_personal com ON com.creator_id = u.user_id ");

			StringBuilder whereClause = new StringBuilder() ;
			whereClause.Append(" cm1.user_id = @userId AND cm1.status_id = ").Append(
				(UInt32) CommunityMember.CommunityMemberStatus.ACTIVE);
			whereClause.Append(" AND cm1.has_edit_rights = 'Y' ");
			whereClause.Append(" AND cm2.status_id = ").Append((UInt32) CommunityMember.CommunityMemberStatus.PENDING);
			if(filter!= null && filter.Trim().Length > 0)
			{
				whereClause.Append(" AND ").Append(filter);
			}
			
			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@userId", userId);

			return dbUtility.GetPagedDataTable(select,tableList.ToString(),whereClause.ToString(), orderby,
				parameters,pageNumber, pageSize);
		}
		#endregion

		#region KGP related
		/// <summary>
		///  return the number of users in a channel in the wok game
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static int GetPlayersInChannel( int channelId )
		{
			Int32 BROADBAND		= 6;	// CI_CHANNEL

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();
			string dbName = KanevaGlobals.DbNameKGP;

			string select = "select `count` from " + dbName + ".summary_active_population pp where pp.zone_type = " + BROADBAND + 
                            " and pp.zone_instance_id = " + channelId.ToString();

			return dbUtility.ExecuteScalar( select );
			
		}

		public static DataTable GetZonesPassIds(int zoneIndex, int instanceId )
		{
			string dbNameForGame = KanevaGlobals.DbNameKGP;
			string dbNameForKaneva = KanevaGlobals.DbNameKaneva;
			string sql;
			/*
			if could easily call SP w/result set, call this
			sql = "CALL " + dbNameForGame + ".getZonePassesDetails( @zoneIndex, @instanceId )";
			*/
			
			Hashtable parameters = new Hashtable ();
			parameters.Clear();
			// ack! ugly parsing of zone type 
			if ( (zoneIndex & 0xf0000000) == 0x40000000 )
			{
				sql = 
					"SELECT g.name, g.pass_group_id, 'N' AS set_by_admin "+
					"FROM " + dbNameForGame + ".pass_group_perm_channels cz "+
					"	INNER JOIN " + dbNameForGame + ".pass_groups g "+
					"	ON cz.pass_group_id = g.pass_group_id "+
					"WHERE cz.zone_index_plain = " + dbNameForGame + ".zoneIndex(@zoneIndex)  "+
					"ORDER BY name ";

				parameters.Add ("@zoneIndex", zoneIndex );

			}
			else
			{
				sql = 
					"SELECT g.name, g.pass_group_id, cz.set_by_admin "+
					"FROM " + dbNameForGame + ".pass_group_channel_zones cz "+
					"	INNER JOIN " + dbNameForGame + ".pass_groups g "+
					"	ON cz.pass_group_id = g.pass_group_id "+
					"WHERE cz.zone_instance_id = @instanceId AND cz.zone_type = " + dbNameForGame + ".zoneType(@zoneIndex)"+
					"ORDER BY name ";

				parameters.Add ("@instanceId", instanceId);
				parameters.Add ("@zoneIndex", zoneIndex );

			}


            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sql, parameters);
		}

		#endregion

        #region Join Template Functions
        /// <summary>
        /// Get the community settings for the join pages
        /// </summary>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public static DataRow GetJoinConfig(int communityId)
        {
            string sqlString = "SELECT community_id, join_id, css, page_join_copy, page_verify_copy, " +
                " page_complete_copy, user_profile_theme_male_id, user_profile_theme_female_id, " +
                " user_profile_video_id, user_profile_male_avatar_path, user_profile_female_avatar_path, " +
                " email_body_top, email_body_bottom, email_subject " +
                " FROM join_themes WHERE " +
                " community_id = @communityId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@communityId", communityId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlString, parameters, false);
        }

        /// <summary>
        /// Get the community settings for the join pages
        /// </summary>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public static DataTable GetJoinCommunities(int sourceCommunityId)
        {
            string sqlSelect = "SELECT source_community_id, join_community_id " +
                " FROM join_communities WHERE " +
                " source_community_id = @sourceCommunityId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@sourceCommunityId", sourceCommunityId);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        }

        #endregion


        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        /// <summary>
		/// Database name for tables that are no in the cluster
		/// </summary>
		private static string m_DbNameKanevaNonCluster = "my_kaneva";

	}
}
