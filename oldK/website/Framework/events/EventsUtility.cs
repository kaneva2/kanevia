///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using System.Web;
using System.Collections;
using System.Threading;
using log4net;
using KlausEnt.KEP.Kaneva.framework.widgets;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
    public class EventsUtility
    {
        private static readonly ILog log = LogManager.GetLogger (typeof (EventsUtility));

        static EventsUtility ()
        {

        }

        public enum eEVENT_START_TIME_FILTER
        {
            NOW = 0,
            PAST = 1,
            UPCOMING = 2,
            ALL = 3
        }

        public enum eEVENT_PRIORITY
        {
            NORMAL = 0,
            PREMIUM = 1,
            ADMINISTRATOR = 2
        }


        /// <summary>
        /// GetUsersEventsMyKaneva
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="nextHours"></param>
        /// <param name="IncludePremiumInvited"></param>
        /// <param name="orderby"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static PagedDataTable GetUsersEventsMyKaneva(int userId, int nextHours, bool IncludePremiumInvited, string orderby, int pageNumber, int pageSize)
        {
            DatabaseUtility utility = KanevaGlobals.GetDatabaseUtilityReadOnly2 ();
            string filter = "";

            if (nextHours > 0)
            {
                // Gets Events ending in next X OR events happening now OR events starting in next X hours
                filter = " AND (end_time < DATE_ADD(NOW(), INTERVAL " + nextHours + " HOUR) OR (start_time < NOW()) OR (start_time < DATE_ADD(NOW(), INTERVAL " + nextHours + " HOUR))) ";
            }

            // This gets all non premium events
            string sqlQuery = "SELECT e.event_id, e.user_id, e.community_id, e.title, e.title as Name, e.location, " +
                "e.start_time, e.end_time, e.premium, 0 AS invite_status_id, e.tracking_request_GUID, " +
                " IF(end_time > NOW() AND start_time < NOW(),'Y', 'N') as happening_now, IF(premium in (1,2),'Y', 'N') as is_premium,  " +
                " TIMESTAMPDIFF(MINUTE,NOW(),start_time) AS minutes_to_event, IF(e.user_id = @userId, 'Y', 'N') as mine, " +
                 " IF(cg.game_id IS NULL, wok.concat_vw_url(environment.getWOKGameId(),c.name,6), wok.concat_vw_url(c.name,'',0)) AS STPURL " +
                "FROM events e " +
                "INNER JOIN communities c ON e.community_id = c.community_id " +
                "LEFT JOIN communities_game cg ON e.community_id = cg.community_id " +
                "WHERE e.user_id = @userId " +
                "AND e.premium = 0 " +
                "AND e.end_time > NOW() " +
                "AND e.event_status_id = 1 " +
                filter +
                " UNION " +
                "SELECT e.event_id, e.user_id, e.community_id, e.title, e.title as Name, e.location, " +
                "e.start_time, e.end_time, e.premium, ei.invite_status_id, e.tracking_request_GUID, " +
                " IF(end_time > NOW() AND start_time < NOW(),'Y', 'N') as happening_now, IF(premium in (1,2),'Y', 'N') as is_premium, " +
                " TIMESTAMPDIFF(MINUTE,NOW(),start_time) AS minutes_to_event, IF(e.user_id = @userId, 'Y', 'N') as mine, " +
                 " IF(cg.game_id IS NULL, wok.concat_vw_url(environment.getWOKGameId(),c.name,6), wok.concat_vw_url(c.name,'',0)) AS STPURL " +
                "FROM events e " +
                "INNER JOIN communities c ON e.community_id = c.community_id " +
                "INNER JOIN event_invites ei ON ei.event_id = e.event_id " +
                "LEFT JOIN communities_game cg ON e.community_id = cg.community_id " +
                "WHERE ei.user_id = @userId " +
                "AND e.premium = 0 " +
                "AND e.event_status_id = 1 " +
                "AND e.end_time > NOW() " +
                "AND (ei.invite_status_id = 1 OR ei.invite_status_id = 2) " +
                filter;

            if (IncludePremiumInvited)
            {
                // This gets premium i was invited to
                sqlQuery += " UNION " +
                "SELECT e.event_id, e.user_id, e.community_id, " +
                "e.title, e.title as Name, e.location, e.start_time, e.end_time, e.premium, " +
                "IFNULL(ei.invite_status_id, 2) AS invite_status_id, e.tracking_request_GUID, " +
                " IF(end_time > NOW() AND start_time < NOW(),'Y', 'N') as happening_now, IF(premium in (1,2),'Y', 'N') as is_premium, " +
                " TIMESTAMPDIFF(MINUTE,NOW(),start_time) AS minutes_to_event, IF(e.user_id = @userId, 'Y', 'N') as mine, " +
                " IF(cg.game_id IS NULL, wok.concat_vw_url(environment.getWOKGameId(),c.name,6), wok.concat_vw_url(c.name,'',0)) AS STPURL " +
                "FROM events e " +
                "INNER JOIN communities c ON e.community_id = c.community_id " +
                "INNER JOIN event_invites ei ON ei.event_id = e.event_id " +
                "LEFT JOIN communities_game cg ON e.community_id = cg.community_id " +
                "WHERE premium = 1 " +
                "AND end_time > NOW() " +
                "AND e.event_status_id = 1 " +
                "AND ((ei.user_id = @userId AND invite_status_id IN (0,1,2)) OR e.user_id = @userId) " +
                filter +
                " GROUP BY e.event_id ";
            }

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return utility.GetPagedDataTableUnion (sqlQuery, orderby, parameters, pageNumber, pageSize);
        }

        public static PagedDataTable GetPremuimEventsMyKaneva (int userId, int nextHours, bool onlyNotInvited, string orderby, int pageNumber, int pageSize)
        {
            return GetPremuimEventsMyKaneva (userId, nextHours, onlyNotInvited, false, orderby, pageNumber, pageSize);
        }
        /// <summary>
        /// GetPremuimEventsMyKaneva
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="nextHours"></param>
        /// <param name="onlyNotInvited"></param>
        /// <param name="orderby"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static PagedDataTable GetPremuimEventsMyKaneva(int userId, int nextHours, bool onlyNotInvited, bool excludeOwnedEvents, string orderby, int pageNumber, int pageSize)
        {
            DatabaseUtility utility = KanevaGlobals.GetDatabaseUtilityReadOnly2 ();
            string filter = "";
            string filterCreator = "";
            if (nextHours >= 0)
            {
                // Gets Events ending in next X OR events happening now OR events starting in next X hours
                filter = " AND (end_time < DATE_ADD(NOW(), INTERVAL " + nextHours + " HOUR) OR (start_time < NOW()) OR (start_time < DATE_ADD(NOW(), INTERVAL " + nextHours + " HOUR))) ";
            }

            // This gets premium not invited
            string sqlQuery = "SELECT e.event_id, e.user_id, e.community_id, " +
                 "e.title, e.title as Name, e.location, e.start_time, e.end_time, e.premium, " +
                 "ei.invite_status_id, ei.user_id, e.tracking_request_GUID, " +
                 " IF(end_time > NOW() AND start_time < NOW(),'Y', 'N') as happening_now, IF(premium in (1,2),'Y', 'N') as is_premium, " +
                 " TIMESTAMPDIFF(MINUTE,NOW(),start_time) AS minutes_to_event, IF(e.user_id = @userId, 'Y', 'N') as mine, " +
                 " IF(cg.game_id IS NULL, wok.concat_vw_url(environment.getWOKGameId(),c.name,6), wok.concat_vw_url(c.name,'',0)) AS STPURL " +
                 "FROM events e " +
                 "INNER JOIN communities c ON e.community_id = c.community_id " +
                 "LEFT JOIN event_invites ei ON e.event_id = ei.event_id AND ei.user_id = @userId " +
                 "LEFT JOIN communities_game cg ON e.community_id = cg.community_id " +
                 "WHERE ei.user_id IS NULL " +
                 "AND e.premium = 1 " +
                 "AND e.end_time > NOW() " +
                 "AND e.event_status_id = 1 " +
                 filter;

            if (excludeOwnedEvents)
            {
                sqlQuery += " AND e.user_id <>  @userId ";
            }

            if (!onlyNotInvited)
            {
                // This gets premium i was invited to
                sqlQuery += " UNION " +
                    "SELECT e.event_id, e.user_id, e.community_id, " +
                     "e.title, e.title as Name, e.location, e.start_time, e.end_time, e.premium, " +
                     "ei.invite_status_id, ei.user_id, e.tracking_request_GUID, " +
                     " IF(end_time > NOW() AND start_time < NOW(),'Y', 'N') as happening_now, IF(premium in (1,2),'Y', 'N') as is_premium, " +
                     " TIMESTAMPDIFF(MINUTE,NOW(),start_time) AS minutes_to_event, IF(e.user_id = @userId, 'Y', 'N') as mine, " +
                     " IF(cg.game_id IS NULL, wok.concat_vw_url(environment.getWOKGameId(),c.name,6), wok.concat_vw_url(c.name,'',0)) AS STPURL " +
                     "FROM events e " +
                     "INNER JOIN communities c ON e.community_id = c.community_id " +
                     "INNER JOIN event_invites ei ON ei.event_id = e.event_id " +
                     "LEFT JOIN communities_game cg ON e.community_id = cg.community_id " +
                     "WHERE premium = 1 " +
                     "AND end_time > NOW() " +
                     "AND e.event_status_id = 1 " +
                     "AND ei.user_id = @userId AND invite_status_id IN (0,1,2) " +
                     filter;
            }

            sqlQuery += filterCreator;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return utility.GetPagedDataTableUnion (sqlQuery, orderby, parameters, pageNumber, pageSize);
        }

        public static PagedDataTable GetPremiumEvents(int userId, int nextHours, bool onlyNotInvited, bool excludeOwnedEvents, string orderby, int pageNumber, int pageSize)
        {
            DatabaseUtility utility = KanevaGlobals.GetDatabaseUtilityReadOnly2();
            string filter = "";
            string filterCreator = "";
            if (nextHours >= 0)
            {
                // Gets Events ending in next X OR events happening now OR events starting in next X hours
                filter = " AND (end_time < DATE_ADD(NOW(), INTERVAL " + nextHours + " HOUR) OR (start_time < NOW()) OR (start_time < DATE_ADD(NOW(), INTERVAL " + nextHours + " HOUR))) ";
            }

            // This gets premium not invited
            string sqlQuery = "SELECT e.event_id, e.user_id, e.community_id, " +
                 "e.title, e.title as Name, e.location, e.start_time, e.end_time, e.premium, " +
                 "ei.invite_status_id, ei.user_id, e.tracking_request_GUID, " +
                 "IF(end_time > NOW() AND start_time < NOW(),'Y', 'N') as happening_now, IF(premium in (1,2),'Y', 'N') as is_premium, " +
                 "TIMESTAMPDIFF(MINUTE,NOW(),start_time) AS minutes_to_event, IF(e.user_id = @userId, 'Y', 'N') as mine, " +
                 "IF(cg.game_id IS NULL, wok.concat_vw_url(environment.getWOKGameId(),c.name,6), wok.concat_vw_url(c.name,'',0)) AS STPURL, " +
                 "number_of_members, " +												
                 "  (SELECT count(f.user_id) " +
		         "      FROM event_invites ei " +
		         "      INNER JOIN friends f on ei.user_id = f.user_id " +
		         "      WHERE event_id = e.event_id " +
                 "      AND f.user_id = @userId) AS friends_invited " +
                 "FROM events e " +
                 "INNER JOIN communities c ON e.community_id = c.community_id " +
                 "INNER JOIN channel_stats cs ON cs.channel_id = c.community_id " +
                 "LEFT JOIN event_invites ei ON e.event_id = ei.event_id AND ei.user_id = @userId " +
                 "LEFT JOIN communities_game cg ON e.community_id = cg.community_id " +
                 "WHERE ei.user_id IS NULL " +
                 "AND e.premium = 1 " +
                 "AND e.end_time > NOW() " +
                 "AND e.event_status_id = 1 " +
                 filter;

            if (excludeOwnedEvents)
            {
                sqlQuery += " AND e.user_id <>  @userId ";
            }

            if (!onlyNotInvited)
            {
                // This gets premium i was invited to
                sqlQuery += " UNION " +
                    "SELECT e.event_id, e.user_id, e.community_id, " +
                     "e.title, e.title as Name, e.location, e.start_time, e.end_time, e.premium, " +
                     "ei.invite_status_id, ei.user_id, e.tracking_request_GUID, " +
                     "IF(end_time > NOW() AND start_time < NOW(),'Y', 'N') as happening_now, IF(premium in (1,2),'Y', 'N') as is_premium, " +
                     "TIMESTAMPDIFF(MINUTE,NOW(),start_time) AS minutes_to_event, IF(e.user_id = @userId, 'Y', 'N') as mine, " +
                     "IF(cg.game_id IS NULL, wok.concat_vw_url(environment.getWOKGameId(),c.name,6), wok.concat_vw_url(c.name,'',0)) AS STPURL, " +
                     "number_of_members, " +
                     "  (SELECT count(f.user_id) " +
                     "      FROM event_invites ei " +
                     "      INNER JOIN friends f on ei.user_id = f.user_id " +
                     "      WHERE event_id = e.event_id " +
                     "      AND f.user_id = @userId) AS friends_invited " +
                     "FROM events e " +
                     "INNER JOIN communities c ON e.community_id = c.community_id " +
                     "INNER JOIN event_invites ei ON ei.event_id = e.event_id " +
                     "INNER JOIN channel_stats cs ON cs.channel_id = c.community_id " +
                     "LEFT JOIN communities_game cg ON e.community_id = cg.community_id " +
                     "WHERE premium = 1 " +
                     "AND end_time > NOW() " +
                     "AND e.event_status_id = 1 " +
                     "AND ei.user_id = @userId AND invite_status_id IN (0,1,2) " +
                     filter;
            }

            sqlQuery += filterCreator;

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);
            return utility.GetPagedDataTableUnion(sqlQuery, orderby, parameters, pageNumber, pageSize);
        }

        public static PagedDataTable GetEventsForUser(int userId, int nextHours, bool IncludePremiumInvited, string orderby, int pageNumber, int pageSize)
        {
            DatabaseUtility utility = KanevaGlobals.GetDatabaseUtilityReadOnly2();
            string filter = "";

            if (nextHours > 0)
            {
                // Gets Events ending in next X OR events happening now OR events starting in next X hours
                filter = " AND (end_time < DATE_ADD(NOW(), INTERVAL " + nextHours + " HOUR) OR (start_time < NOW()) OR (start_time < DATE_ADD(NOW(), INTERVAL " + nextHours + " HOUR))) ";
            }

            // This gets all non premium events
            string sqlQuery = "SELECT e.event_id, e.user_id, e.community_id, e.title, e.title as Name, e.location, " +
                "e.start_time, e.end_time, e.premium, 0 AS invite_status_id, e.tracking_request_GUID, " +
                "IF(end_time > NOW() AND start_time < NOW(),'Y', 'N') as happening_now, IF(premium in (1,2),'Y', 'N') as is_premium,  " +
                "TIMESTAMPDIFF(MINUTE,NOW(),start_time) AS minutes_to_event, IF(e.user_id = @userId, 'Y', 'N') as mine, " +
                "COUNT(ei2.event_id) AS invite_count, " +
                "IF(cg.game_id IS NULL, wok.concat_vw_url(environment.getWOKGameId(),c.name,6), wok.concat_vw_url(c.name,'',0)) AS STPURL " +
                "FROM events e " +
                "INNER JOIN communities c ON e.community_id = c.community_id " +
                "INNER JOIN event_invites ei2 ON ei2.event_id = e.event_id " +
                "LEFT JOIN communities_game cg ON e.community_id = cg.community_id " +
                "WHERE e.user_id = @userId " +
                "AND e.premium = 0 " +
                "AND e.end_time > NOW() " +
                "AND e.event_status_id = 1 " +
                filter +
                "GROUP BY e.event_id " +
                " UNION " +
                "SELECT e.event_id, e.user_id, e.community_id, e.title, e.title as Name, e.location, " +
                "e.start_time, e.end_time, e.premium, ei.invite_status_id, e.tracking_request_GUID, " +
                "IF(end_time > NOW() AND start_time < NOW(),'Y', 'N') as happening_now, IF(premium in (1,2),'Y', 'N') as is_premium, " +
                "TIMESTAMPDIFF(MINUTE,NOW(),start_time) AS minutes_to_event, IF(e.user_id = @userId, 'Y', 'N') as mine, " +
                "COUNT(ei2.event_id) AS invite_count, " +
                "IF(cg.game_id IS NULL, wok.concat_vw_url(environment.getWOKGameId(),c.name,6), wok.concat_vw_url(c.name,'',0)) AS STPURL " +
                "FROM events e " +
                "INNER JOIN communities c ON e.community_id = c.community_id " +
                "INNER JOIN event_invites ei ON ei.event_id = e.event_id " +
                "INNER JOIN event_invites ei2 ON ei2.event_id = e.event_id " +
                "LEFT JOIN communities_game cg ON e.community_id = cg.community_id " +
                "WHERE ei.user_id = @userId " +
                "AND e.premium = 0 " +
                "AND e.event_status_id = 1 " +
                "AND e.end_time > NOW() " +
                "AND (ei.invite_status_id = 1 OR ei.invite_status_id = 2) " +
                filter +
                "GROUP BY e.event_id ";

            if (IncludePremiumInvited)
            {
                // This gets premium i was invited to
                sqlQuery += " UNION " +
                "SELECT e.event_id, e.user_id, e.community_id, " +
                "e.title, e.title as Name, e.location, e.start_time, e.end_time, e.premium, " +
                "IFNULL(ei.invite_status_id, 2) AS invite_status_id, e.tracking_request_GUID, " +
                "IF(end_time > NOW() AND start_time < NOW(),'Y', 'N') as happening_now, IF(premium in (1,2),'Y', 'N') as is_premium, " +
                "TIMESTAMPDIFF(MINUTE,NOW(),start_time) AS minutes_to_event, IF(e.user_id = @userId, 'Y', 'N') as mine, " +
                "COUNT(ei2.event_id) AS invite_count, " +
                "IF(cg.game_id IS NULL, wok.concat_vw_url(environment.getWOKGameId(),c.name,6), wok.concat_vw_url(c.name,'',0)) AS STPURL " +
                "FROM events e " +
                "INNER JOIN communities c ON e.community_id = c.community_id " +
                "INNER JOIN event_invites ei ON ei.event_id = e.event_id " +
                "INNER JOIN event_invites ei2 ON ei2.event_id = e.event_id " +
                "LEFT JOIN communities_game cg ON e.community_id = cg.community_id " +
                "WHERE premium = 1 " +
                "AND end_time > NOW() " +
                "AND e.event_status_id = 1 " +
                "AND ((ei.user_id = @userId AND ei.invite_status_id IN (0,1,2)) OR e.user_id = @userId) " +
                filter +
                "GROUP BY e.event_id ";
            }

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);
            return utility.GetPagedDataTableUnion(sqlQuery, orderby, parameters, pageNumber, pageSize);
        }

        public static PagedDataTable GetEventsMyKaneva(int userId, int typeId, string orderby, int pageNumber, int pageSize, bool future, bool past, string filter)
        {
            string selectList = "e.event_id, e.community_id, e.user_id, e.title as Name, e.details, e.location as Location, e.zone_index as ZoneIndex, e.zone_instance_id as ZoneInstanceId, " +
              " et.event_type as Type, u.username as Host, e.premium as Premium, e.start_time, privacy, e.tracking_request_GUID, " +
              " CASE WHEN e.user_id = @userId THEN 1 END AS `mine`, " +
              " CASE WHEN e.user_id IN (SELECT friend_id as user_id FROM friends WHERE user_id = @userId) THEN 1 END AS friend";
            return GetEvents (userId, true, false, typeId, orderby, pageNumber, pageSize, future, past, selectList, filter);
        }

        public static PagedDataTable GetEvents (int userId, bool bHasAPSubscription, bool bOnlyAPEvents, int typeId, string orderby, int pageNumber, int pageSize, bool future, bool past, string selectList, string filter)
        {
            DatabaseUtility utility = KanevaGlobals.GetDatabaseUtilityReadOnly2 ();

            //
            // Tables
            string tableList = "events e LEFT OUTER JOIN wok.summary_active_population p ON e.zone_index = p.zone_index AND e.zone_instance_id = p.zone_instance_id " +
                " , users u, event_types et ";

            //
            // Join Tables
            string whereClause = " e.user_id = u.user_id AND et.event_type_id = e.type_id AND e.event_status_id = 1 ";

            //
            // Add filter
            // Example: premium = 1, premium = 2
            whereClause += filter;

            //
            // Time Frame
            if (future)
            {
                whereClause += " AND end_time > @currentTime ";
            }
            if (past)
            {
                whereClause += " AND start_time < @currentTime ";
            }

            //
            // Filter By Type
            switch (typeId)
            {
                case 102:
                    {
                        // User's Communitites Only
                        whereClause += "AND e.community_id IN ( " +
                            " SELECT community_id FROM community_members " +
                            " WHERE user_id = @userId AND status_id = 1) ";
                        break;
                    }
                case 101:
                    {
                        // Friends Only
                        whereClause += " AND e.user_id  in ( SELECT friend_id as user_id FROM friends WHERE user_id = @userId )";
                        break;
                    }
                case 100:
                    {
                        // All events            
                        break;
                    }
                default:
                    {
                        // Default is to filter by a specific type            
                        whereClause += " AND et.event_type_id = " + typeId;
                        break;
                    }
            }

            // Apply AP to events
            if (bHasAPSubscription)
            {
                if (bOnlyAPEvents)
                {
                    whereClause += " AND e.is_AP_event = 'Y' ";
                }
            }
            else
            {
                whereClause += " AND e.is_AP_event = 'N' ";
            }

            // Do not show private events unless the user is a friend or they sent
            whereClause += " HAVING  ((mine = 1) OR (friend = 1) OR (premium in (1,2) AND privacy = 0)) ";

            if (orderby == "")
            {
                orderby = " e.premium DESC , mine DESC,  e.start_time ";
            }
            else
            {
                orderby = " e.premium DESC , mine DESC, " + orderby + ", e.start_time ";
            }



            Hashtable parameters = new Hashtable ();
            parameters.Add ("@currentTime", utility.GetCurrentDateTime ());
            parameters.Add ("@userId", userId);
            return utility.GetPagedDataTable (selectList, tableList, whereClause,
              orderby, parameters, pageNumber, pageSize);
        }

        public static PagedDataTable GetChannelEvents (int channelId, int startTime, string orderby, int pageNumber, int pageSize)
        {
            DatabaseUtility utility = KanevaGlobals.GetDatabaseUtilityReadOnly2 ();
            string selectList = "e.event_id, e.community_id, e.user_id, e.title, e.details, " +
              " e.location, e.start_time, e.end_time, e.type_id, e.time_zone_id ";

            string tableList = " events e ";

            string whereClause = " e.community_id = @channelId AND e.event_status_id = 1 ";
            Hashtable parameters = new Hashtable ();

            switch (startTime)
            {
                case (int) eEVENT_START_TIME_FILTER.PAST:
                    whereClause += " AND e.start_time < @start_time";
                    parameters.Add ("@start_time", utility.GetCurrentDateTime ());
                    break;
                case (int) eEVENT_START_TIME_FILTER.UPCOMING:
                    whereClause += " AND e.start_time >= @start_time";
                    parameters.Add ("@start_time", utility.GetCurrentDateTime ());
                    break;
                case (int) eEVENT_START_TIME_FILTER.ALL:
                    break;
                default:
                    break;
            }



            parameters.Add ("@channelId", channelId);

            return utility.GetPagedDataTable (selectList, tableList, whereClause,
              orderby, parameters, pageNumber, pageSize);
        }

        public static PagedDataTable GetUserRunningEvents (int userId, int pageNumber, int pageSize)
        {
            DatabaseUtility utility = KanevaGlobals.GetDatabaseUtilityReadOnly2 ();
            string selectList = "event_id ";

            string tableList = " events e ";

            string whereClause = " e.user_id = @userId ";
            Hashtable parameters = new Hashtable ();

            whereClause += " AND e.end_time > NOW() ";
            whereClause += " AND e.start_time <= NOW() ";
            whereClause += " AND e.event_status_id = 1 ";

            parameters.Add ("@userId", userId);
            m_logger.Debug (selectList + " " + tableList + " " + whereClause + " ");
            return utility.GetPagedDataTable (selectList, tableList, whereClause,
              "", parameters, pageNumber, pageSize);
        }

        public static DataRow GetEvent (int id)
        {
            string sqlSelect = "SELECT e.title, et.event_type, e.details, e.location, e.start_time, e.end_time, e.user_id, " +
              " e.premium, e.privacy, e.time_zone_id, e.created_date, e.last_update, u.username, e.community_id, e.tracking_request_GUID, " +
              " CASE WHEN e.community_id = 0 THEN '' ELSE c.name END AS community_name " +
              " FROM kaneva.events e " +
              " Inner Join kaneva.event_types et on e.type_id = et.event_type_id " +
              " INNER JOIN kaneva.users u on e.user_id = u.user_id " +
              " LEFT JOIN kaneva.communities c on e.community_id = c.community_id " +
              " WHERE event_id = @id";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@id", id);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2 ().GetDataRow (sqlSelect, parameters, false);
        }

        public static int UpdateEvent (int eventId, int communityId, string title, string details, string location,
              DateTime startTime, DateTime endTime, int typeId, int privacy, int premium, int timeZoneId, bool isAPEvent)
        {
            string sql = " UPDATE events " +
              " SET community_id = @community_id, " +
              " title= @title, " +
              " details = @details, " +
              " location = @location, " +
              " start_time = @start_time, " +
              " end_time = @end_time, " +
              " type_id = @type_id, " +
              " privacy = @privacy, " +
              " premium = @premium, " +
              " time_zone_id = @time_zone_id, " +
              " last_update = NOW(), " +
              " is_AP_event = @is_AP_event " +
              " WHERE event_id = @id ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@id", eventId);
            parameters.Add ("@community_id", communityId);
            parameters.Add ("@title", title);
            parameters.Add ("@details", details);
            parameters.Add ("@location", location);
            parameters.Add ("@start_time", startTime);
            parameters.Add ("@end_time", endTime);
            parameters.Add ("@type_id", typeId);
            parameters.Add ("@privacy", privacy);
            parameters.Add ("@premium", premium);
            parameters.Add ("@time_zone_id", timeZoneId);
            parameters.Add ("@is_AP_event", isAPEvent ? "Y" : "N");

            return KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sql, parameters);
        }

        public static void SaveEvent (int eventId, int communityId, int userId, string title, string details,
            string location, DateTime startTime, DateTime endTime, int typeId, int timeZoneId, bool isAPEvent)
        {
            StringBuilder sb = new StringBuilder ();
            sb.Append (" UPDATE events ");
            sb.Append (" SET community_id = @community_id, ");
            sb.Append (" user_id= @user_id, ");
            sb.Append (" title= @title, ");
            sb.Append (" details = @details, ");
            sb.Append (" location = @location, ");
            sb.Append (" start_time = @start_time, ");
            sb.Append (" end_time = @end_time, ");
            sb.Append (" type_id = @type_id, ");
            sb.Append (" time_zone_id = @time_zone_id, ");
            sb.Append (" is_AP_event = @is_AP_event ");
            sb.Append (" WHERE event_id = @id ");

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@id", eventId);
            parameters.Add ("@community_id", communityId);
            parameters.Add ("@user_id", userId);
            parameters.Add ("@title", title);
            parameters.Add ("@details", details);
            parameters.Add ("@location", location);
            parameters.Add ("@start_time", startTime);
            parameters.Add ("@end_time", endTime);
            parameters.Add ("@type_id", typeId);
            parameters.Add ("@time_zone_id", timeZoneId);
            parameters.Add ("@is_AP_event", isAPEvent ? "Y" : "N");

            KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sb.ToString (), parameters);
        }

        public static void DeleteEvent (int id)
        {
            //delete from layout_module_title_text table
            string sqlString = "DELETE FROM events " +
              " WHERE event_id = @id ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@id", id);
            KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
        }

        public static DataTable GetEventableCommunities (int userId)
        {
            //string sqlSelect = "SELECT channel_zone_id, `name` FROM wok.channel_zones WHERE kaneva_user_id = @userId ";
            string sqlSelect = "select  c.name,  c.community_id from  kaneva.communities c Where  c.creator_id = @userId AND  c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE;

            // Need to only use communities that are zoneable.
            sqlSelect += " AND community_id in ( SELECT community_id FROM wok.worlds_cache WHERE creator_id = @userId  ) ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
        }

        public static DataRow GetLocationDetail2 (int zone_index, int zone_instance_id, int channel_zone_id)
        {
            string strSqlSelect = "SELECT * FROM wok.channel_zones cz ";

            string strWhere = " WHERE cz.channel_zone_id > 0 ";

            strWhere += channel_zone_id.Equals (0) ? "" : " AND  cz.channel_zone_id = @channel_zone_id ";

            strWhere += zone_index.Equals (0) ? "" : " AND  cz.zone_index = @zone_index ";

            strWhere += zone_instance_id.Equals (0) ? "" : " AND  cz.zone_instance_id = @zone_instance_id ";

            strSqlSelect += strWhere;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@channel_zone_id", channel_zone_id);
            parameters.Add ("@zone_index", zone_index);
            parameters.Add ("@zone_instance_id", zone_instance_id);
            return KanevaGlobals.GetDatabaseUtilityReadOnly2 ().GetDataRow (strSqlSelect, parameters, false);
        }



        public static int ChargeForPremiumEvent (int userId, string currency)
        {
            // Premium Event?
            int successfulPayment = -1;

            // Do they have enough money?

            try
            {
                if (currency.Equals ("GPOINTS"))
                {
                    successfulPayment = (new UserFacade ()).AdjustUserBalance (userId, Constants.CURR_GPOINT, -200.0, Constants.CASH_TT_BOUGHT_ITEM);
                }
                if (currency.Equals ("KPOINTS"))
                {
                    successfulPayment = (new UserFacade ()).AdjustUserBalance (userId, Constants.CURR_KPOINT, -200.0, Constants.CASH_TT_BOUGHT_ITEM);
                }
            }
            catch (Exception)
            {
                // Not enough credits/rewards
                return -1;
            }

            if (successfulPayment == 0)//0 is success according to AdjustUserBalance notes
            {
                return 1;
            }
            else
            {
                //another error occured.
                return 0;
            }

        }


        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
    }
}
