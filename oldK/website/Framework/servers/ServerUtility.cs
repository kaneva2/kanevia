///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections;

namespace KlausEnt.KEP.Kaneva
{
  /// <summary>
  /// Summary description for ServerUtility.
  /// </summary>
  public class ServerUtility
  {
    public ServerUtility ()
    {
  
    }

        /// <summary>
        /// Get a list of Servers
        /// </summary>
        public static DataTable GetGameServers(int gameId, int patchVersion, string filter)
        {
            Hashtable parameters = new Hashtable();

            string sqlSelect = "SELECT COALESCE(cge.server_id, 0) as server_id, COALESCE(cge.server_name, '') as server_name, a.game_id, a.game_name," +
                                "    COALESCE(cge.server_started_date, now()) as server_started_date, COALESCE(cge.ip_address, '') as ip_address, COALESCE(cge.port, 0) as port, COALESCE(cge.server_status_id, 0) as server_status_id, COALESCE(cge.number_of_players, 0)  as number_of_players,  COALESCE(cge.max_players, 1000) as max_players, " +
                                "    gpu.patch_url, NULL as game_exe, " +
                                "    gpu.patch_type, gpu.client_path " +
                                "  FROM games a left outer join game_servers cge on a.game_id = cge.game_id" +
                                "         INNER JOIN game_licenses gl ON gl.game_id = a.game_id" +
                                "         INNER JOIN game_patch_urls gpu ON gpu.game_id = a.game_id" +
                                " WHERE a.game_id = @gameId" +
                                "  AND gpu.patch_id = @patchVersion " +
                                "  AND (a.incubator_hosted<>0 or !isnull(cge.last_ping_datetime) and cge.last_ping_datetime>" + KanevaGlobals.GetDatabaseUtility().GetDatePlusMinutes(-7) + ") ";

            if (filter.Length > 0)
            {
                sqlSelect += "AND " + filter;
            }

            sqlSelect += " ORDER BY gpu.patch_type DESC";     // Make sure old patch URLs come last

            parameters.Add("@gameId", gameId);
            parameters.Add("@patchVersion", patchVersion);

            return KanevaGlobals.GetDatabaseUtilityDeveloper().GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// Get a list of customization for a game's patcher.  Note we are returning
        /// gps.* to get all the customizations since we may be adding more and more in the future
        /// </summary>
        public static DataTable GetGamePatcherSettings(Int32 gameId)
        {
            Hashtable parameters = new Hashtable();

            /*
                SELECT g.name, g.game_status_id, gps.*
                  FROM games g LEFT OUTER JOIN game_patcher_settings gps
                          ON g.game_id = gps.game_id
                 WHERE g.game_id = 3296;
            */

            string sqlSelect =  "SELECT g.game_name, g.game_status_id, gps.*" +
                                "  FROM games g LEFT OUTER JOIN game_patcher_settings gps" +
                                "          ON g.game_id = gps.game_id" +
                                " WHERE g.game_Id = @gameId LIMIT 1";

            parameters.Add("@gameId", gameId);

            return KanevaGlobals.GetDatabaseUtilityDeveloper().GetDataTable(sqlSelect, parameters);
        }


        private static string WOKdbName = KanevaGlobals.DbNameKGP;

        /// <summary>
        /// get the list of Games
        /// </summary>
        /// <param name="page"></param>
        /// <param name="items_per_page"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderByDirection"></param>
        /// <returns></returns>
        public static PagedDataTable GetGames(int pageNumber, int pageSize, string orderBy)
        {
            string GameServerUrl = KanevaGlobals.GameServerUrl;

            Hashtable theParams = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            // XML conversion barfs on empty strings, so add IFs
            // TODO -- enforce valid values in DB
            string selectList = "IF(length(a.name)=0,'unknown',a.name) as name, a.asset_id as game_id, IF(length(a.body_text)=0,'unknown',a.body_text) AS description, " +
                                " (SELECT IF(length(patch_url)=0,'unknown',patch_url) as patch_url FROM game_license gl WHERE gl.asset_id = cge.game_asset_id LIMIT 1)  AS patch_url, " +
                                "			concat( '" + GameServerUrl + "', cast( a.asset_id as char ) ) as server_url," +
                                "			sum(cge.number_of_players) AS population, " +
                                "			sum(cge.max_players) AS max_population, " +
                                "			sad.number_of_diggs  AS raves";
            string tableList = "	assets a " +
                                "			INNER JOIN current_game_engines cge ON a.asset_id = cge.game_asset_id" +
                                "      INNER JOIN assets_stats sad ON a.asset_id = sad.asset_id";
            string whereClause = "cge.internally_hosted = 'Y'" +
                                "        AND timestampdiff(minute, cge.last_ping_datetime, NOW()) < " + WOKdbName + ".deadServerInterval()";

            whereClause += " GROUP BY 1, 2, 3";
            string orderByList = orderBy;
            /*
                SELECT IF(length(a.name) = 0, 'unknown', a.name)  AS name, 
                        a.asset_id  AS game_id,
                        IF(length(a.body_text) = 0, 'unknown', a.body_text)  AS description,
                        (SELECT IF(length(patch_url) = 0, 'unknown', patch_url) AS patch_url  FROM game_license gl WHERE gl.asset_id = cge.game_asset_id LIMIT 1)  AS patch_url, 
                        concat(' GameServerUrl + ', cast(a.asset_id AS char)) AS server_url, 
                        sum(cge.number_of_players) AS population, 
                        sum(cge.max_players) AS max_population, 
                        sad.number_of_diggs  AS raves
                  FROM  assets a 
                        INNER JOIN current_game_engines cge
                             ON a.asset_id = cge.game_asset_id
                        INNER JOINassets_stats sad
                            ON a.asset_id = sad.asset_id
                 WHERE cge.internally_hosted = 'Y'
                       AND timestampdiff(minute, cge.last_ping_datetime, NOW()) < wok.deadServerInterval()
                 GROUP BY 1, 2, 3;
             */

            return dbUtility.GetPagedDataTable(selectList, tableList, whereClause, orderByList, theParams, pageNumber, pageSize);
        }

        /// <summary>
        /// get the currently active server for this Game, least populated (load balancing)
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns></returns>
        public static DataRow GetGameServer(int gameId)
        {
            /*
              SELECT ip_address, port 
                FROM current_game_engines ge
               WHERE ge.status_id = 1 -- 1 is running
                 AND timestampdiff (minute, last_ping_datetime, NOW()) < wok.deadServerInterval() -- minute limit 
                 AND ge.internally_hosted = 'Y'
                 AND ge.number_of_players < (ge.max_players - 10 ) -- 10 is pad
                 AND ge.game_asset_id = 3296
               ORDER BY number_of_players LIMIT 1             */

            Hashtable theParams = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlSelect = "SELECT ip_address, port " +
                                "  FROM current_game_engines ge" +
                                " WHERE ge.status_id = " + ((int)Constants.eGAME_SERVER_STATUS.RUNNING).ToString() +
                                "   AND timestampdiff (minute, last_ping_datetime, NOW()) < wok.deadServerInterval() " +
                                "   AND ge.internally_hosted = 'Y'" +
                                "   AND ge.number_of_players < (ge.max_players - 10 )" + // 10 is buffer
                                "   AND ge.game_asset_id = @gameId" +
                                " ORDER BY number_of_players LIMIT 1";
            theParams.Add("@gameId", gameId);
            return dbUtility.GetDataRow(sqlSelect, theParams, false);
        }



				public static bool IsKnownServer(string serverIp, int gameId)
				{
					bool retval = true;
					Hashtable theParams = new Hashtable();
					DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

					string sqlSelect = "SELECT actual_ip_address FROM developer.game_servers WHERE game_id = @gameId";
					theParams.Add("@gameId", gameId);

					DataRow dbResult = dbUtility.GetDataRow(sqlSelect, theParams, false);
					if (dbResult == null) {
						retval = false;
					}
					else {
						if (dbResult["actual_ip_address"].ToString() != serverIp) {
							retval = false;
						}
					}
					return retval;
				}


        public static DataRow GetGameLicenseInfo(string licenseKey, string hostName)
        {

            Hashtable theParams = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlSelect = "SELECT sub.name," +
                                     " lic.license_status_id," +
                                     " lic.end_date," +
                                     " serv.server_id" +
                              " FROM developer.game_licenses lic LEFT JOIN (select * from developer.game_servers where server_name = @hostName) as serv on lic.game_id = serv.game_id" +
                                     " LEFT JOIN developer.game_license_subscriptions sub on lic.license_subscription_id = sub.license_subscription_id" +
                              " WHERE" +
                                     " lic.game_key = @licenseKey";

            theParams.Add("@licenseKey", licenseKey);
            theParams.Add("@hostName", hostName);
            return dbUtility.GetDataRow(sqlSelect, theParams, false);

        }

        

/*
    public static DataRow GetGameLicenseInfo(string licenseKey)
    {
      Hashtable theParams = new Hashtable();
      DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
      string sqlSelect = "SELECT game_license_subscriptions.name," +
                                     " game_licenses.license_status_id," +
                                     " game_licenses.end_date " +
                              " FROM developer.game_licenses, developer.game_license_subscriptions " +
                              " WHERE" +
                                     " game_licenses.license_subscription_id = game_licenses_subscriptions.license_subscription_id and " +
                                     " game_licenses.game_key = @licenseKey";
      
      theParams.Add("@licenseKey", licenseKey);
            return dbUtility.GetDataRow(sqlSelect, theParams, false);
      
      }
 */

  }
}
