///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for BaseAdaptor.
	/// </summary>
	public abstract class BaseAdaptor
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public BaseAdaptor()
		{
		}

		/// <summary>
		/// AuthorizePayment
		/// </summary>
		public abstract bool AuthorizePayment (DataRow drOrder, DataRow drPpt, string ccNumEncrypt, string userIpAddress, ref bool bSystemDown, ref string userErrorMessage);

		/// <summary>
		/// ProcessPayment
		/// </summary>
		public abstract bool ProcessPayment (DataRow drOrder, DataRow drPpt, string ccNumEncrypt, string ccSecCodeEncrypt, string userIpAddress, ref bool bSystemDown, ref string userErrorMessage);

        /// <summary>
        /// CreateSubscription
        /// </summary>
        public abstract bool CreateSubscription(DataRow drOrder, DataRow drPpt, string ccNumEncrypt, string ccSecCodeEncrypt, string userIpAddress, ref bool bSystemDown, ref string userErrorMessage, 
            ref string subscriptionIdReference, int subscriptionId, uint userSubscriptionId,
            global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm subTermFreePeriod,
            global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm subTermFrequency);

        public abstract bool CancelSubscription(ref bool bSystemDown, ref string userErrorMessage, GameSubscription gameSubscription, int orderId);
        public abstract bool CancelSubscription(ref bool bSystemDown, ref string userErrorMessage, UserSubscription userSubscription);

        public abstract bool UpdateSubscription(ref bool bSystemDown, ref string userErrorMessage, UserSubscription userSubscription);

        public abstract SubscriptionInfo GetSubscription(ref bool bSuccess, ref bool bSystemDown, ref string userErrorMessage, string subscriptionId, int orderId);

		/// <summary>
		/// ProcessRefund
		/// </summary>
		public abstract bool ProcessRefund (DataRow drOrder, DataRow drPpt, string userIpAddress, ref bool bSystemDown, ref string userErrorMessage);

		/// <summary>
		/// GetCurrency
		/// </summary>
		/// <returns></returns>
		public virtual string Currency ()
		{
			return "USD";
		}

		/// <summary>
		/// GetURL - URL (URL to api or site to redirect to)
		/// </summary>
		/// <returns></returns>
		public virtual string URL ()
		{
			return "https://www.sandbox.paypal.com";
		}

		/// <summary>
		/// Merchant Id or Account Id
		/// </summary>
		public virtual string MerchantId ()
		{
			return "test@test222.com";
		}

		/// <summary>
		/// Username
		/// </summary>
		public virtual string Username ()
		{
			return "";
		}

		/// <summary>
		/// Password
		/// </summary>
		public virtual string Password ()
		{
			return "";
		}

	}
}
