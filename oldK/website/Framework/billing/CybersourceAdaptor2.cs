using System;
using System.Configuration;
using System.Net;
using System.Data;
using log4net;

using CyberSource.Clients;
using CyberSource.Clients.SoapServiceReference;
using System.Security.Cryptography;

using Kaneva.BusinessLayer;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;

//using CyberSource.Soap;
//using CyberSource.Soap.CyberSourceWS;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for CybersourceAdaptor.
	/// </summary>
	public class CybersourceAdaptor : BaseAdaptor
	{
		public CybersourceAdaptor()
		{
		}

		/// <summary>
		/// ProcessRefund
		/// </summary>
		/// <returns></returns>
		public override bool ProcessRefund (DataRow drOrder, DataRow drPpt, string userIpAddress, ref bool bSystemDown, ref string userErrorMessage)
		{
			userErrorMessage = "Unable to process refunds at this time with Cybersource";
			return false;
		}

		/// <summary>
		/// ProcessPayment
		/// </summary>
		/// <returns></returns>
		public override bool AuthorizePayment (DataRow drOrder, DataRow drPpt, string ccNumEncrypt, string userIpAddress, ref bool bSystemDown, ref string userErrorMessage)
		{
			return ProcessPayment (drOrder, drPpt, ccNumEncrypt, string.Empty, userIpAddress, ref userErrorMessage, ref bSystemDown, false);
		}

		/// <summary>
		/// ProcessPayment
		/// </summary>
		/// <returns></returns>
		public override bool ProcessPayment (DataRow drOrder, DataRow drPpt, string ccNumEncrypt, string ccSecCodeEncrypt, string userIpAddress, ref bool bSystemDown, ref string userErrorMessage)
		{
			return ProcessPayment (drOrder, drPpt, ccNumEncrypt, ccSecCodeEncrypt, userIpAddress, ref userErrorMessage, ref bSystemDown, true);
		}

		/// <summary>
		/// ProcessPayment
		/// </summary>
		private bool ProcessPayment (DataRow drOrder, DataRow drPpt, string ccNumEncrypt, string ccSecCodeEncrypt, string userIpAddress, ref string userErrorMessage, ref bool bSystemDown, bool bCapture)
		{
			RequestMessage request = new RequestMessage ();
			bool bSuccess = false;
			bSystemDown = false;
			int orderId = Convert.ToInt32 (drOrder ["order_id"]);
			int pointTransactionId = Convert.ToInt32 (drPpt ["point_transaction_id"]);
			int userId = Convert.ToInt32 (drOrder ["user_id"]);
			int orderbillingInfoId = Convert.ToInt32 (drPpt ["order_billing_information_id"]);
			Double dAmountDebited = Convert.ToDouble (drPpt ["amount_debited"]);

            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

			// obtain merchantID from the config file
			request.merchantID = MerchantId ();

			// we want to do Credit Card Authorization in this sample
			request.ccAuthService = new CCAuthService();
			request.ccAuthService.run = "true";

			// Credit Card Capture
			if (bCapture)
			{
				request.ccCaptureService = new CCCaptureService();
				request.ccCaptureService.run = "true";
			}

			// add required fields
			request.merchantReferenceCode = "KEN-" + orderId;

			DataRow drOrderBillingInfo = StoreUtility.GetOrderBillingInfo (orderbillingInfoId);

			// Create the first and last name.
			string firstName = "", lastName = "";
			GetNames (drOrderBillingInfo ["address_name"].ToString (), ref firstName, ref lastName);

			BillTo billTo = new BillTo ();
			billTo.firstName = firstName;
			billTo.lastName = lastName;
			billTo.street1 = drOrderBillingInfo ["address1"].ToString ();
			billTo.street2 = drOrderBillingInfo ["address2"].ToString ();
			billTo.city = drOrderBillingInfo ["city"].ToString ();
			billTo.state = drOrderBillingInfo ["state_code"].ToString ();
			billTo.postalCode = drOrderBillingInfo ["zip_code"].ToString ();
			billTo.country = drOrderBillingInfo ["country_id"].ToString ();
            billTo.email = user.Email;
			billTo.ipAddress = userIpAddress;
			billTo.phoneNumber = drOrderBillingInfo ["phone_number"].ToString ();
            billTo.customerID = user.Username;
			request.billTo = billTo;

			Card card = new Card ();
			card.fullName = drOrderBillingInfo ["name_on_card"].ToString ();
			card.accountNumber = KanevaGlobals.Decrypt (ccNumEncrypt);
			
			if (ccSecCodeEncrypt != string.Empty)
			{
				card.cvNumber = KanevaGlobals.Decrypt (ccSecCodeEncrypt);
			}

			card.expirationMonth = drOrderBillingInfo ["exp_month"].ToString ();
			card.expirationYear = drOrderBillingInfo ["exp_year"].ToString ();
			request.card = card;

			PurchaseTotals purchaseTotals = new PurchaseTotals();
			purchaseTotals.currency = Currency ();
			
			
			// ****************************************************************
			// TODO REMOVE -- FOR TESTING ONLY			
		//	purchaseTotals.grandTotalAmount = card.fullName;
			// ****************************************************************
			
			request.purchaseTotals = purchaseTotals;
			   
			// There is just one item, the points they are buying
			request.item = new Item [1];
			Item item = new Item();
			item.id = pointTransactionId.ToString ();
			item.unitPrice = dAmountDebited.ToString ();
			item.productName = drPpt ["description"].ToString ();
			item.quantity = "1";
			if (!drPpt ["point_bucket_id"].Equals (DBNull.Value))
			{
				item.productSKU = "Point bucket id " + drPpt ["point_bucket_id"].ToString ();
			}
			request.item [0] = item;
		
			// Debugging purposes, the request
			m_logger.Debug ("Sending request to Cybersource: Order is " + request.merchantReferenceCode +
                    ", username = " + user.Username +
					", userId = " + Convert.ToInt32 (drOrder ["user_id"]) +
					", pointTransactionId = " + Convert.ToInt32 (drPpt ["point_transaction_id"]) +
					", orderbillingInfoId = " + Convert.ToInt32 (drPpt ["order_billing_information_id"]) +
					", dAmountDebited = " + Convert.ToDouble (drPpt ["amount_debited"])
				);

			try
			{
				bool retry;
			
				do
				{
					ReplyMessage reply = SoapClient.RunTransaction (request);

					// Was it succesfull?
					int reasonCode = int.Parse (reply.reasonCode);

					try
					{
						// Debugging purposes, the reply
						m_logger.Debug ("Reply from Cybersource: Order = " + reply.merchantReferenceCode +
							" Cybersource ref id = " + reply.requestID +
							" Decision = " + reply.decision + 
							", reasonCode = " +  reasonCode +										   
							", avsReasonCode = " + reply.ccAuthReply.avsCode + " - " + AVSCodeDescriptions (reply.ccAuthReply.avsCode) +
							", cvReasonCode = " + reply.ccAuthReply.cvCode  + " - " + CVCodeDescriptions (reply.ccAuthReply.cvCode) +						   
							", missing fields = " + EnumerateValues( reply.missingField ) +
							", invalid fields = " + EnumerateValues( reply.invalidField )
							);
					}
					catch (Exception)
					{
						// Some of the things we are logging could be null
						m_logger.Debug ("Reply from Cybersource Exc: reasonCode = " +  reasonCode + ", reply = " + reply.ToString ());
					}


					if ("ACCEPT".Equals ( reply.decision ) && reasonCode.Equals (100))
					{
						bSuccess = true;
						retry = false;

						int transactionStatus = (int) Constants.eTRANSACTION_STATUS.VERIFIED;
		
						if (bCapture)
						{
							// Mark it as verified
							transactionStatus = (int) Constants.eTRANSACTION_STATUS.VERIFIED;
						}
						else
						{
							// Only authorized
							transactionStatus = (int) Constants.eTRANSACTION_STATUS.AUTHORIZED;
						}

						// A succesfull order
						StoreUtility.UpdatePointTransaction (pointTransactionId, transactionStatus, "", reply.requestID);

						if (bCapture)
						{
							// Are they purchasing a point bucket?
							if (!drPpt ["point_bucket_id"].Equals (DBNull.Value))
							{
								int pointBucketId = Convert.ToInt32 (drPpt ["point_bucket_id"]);
								m_logger.Info ("Completing point bucket purchase, user = " + userId + ", pointTransactionId = " + pointTransactionId + ", pointBucketId = " + pointBucketId + ", orderId = " + orderId);
								CompletePointBucketPurchase (userId, pointTransactionId, pointBucketId, orderId);
							}
								// Are they purchasing custom k-point amounts?
							else
							{
								m_logger.Info ("Completing k-Point purchase, user = " + userId + ", pointTransactionId = " + pointTransactionId + ", orderId = " + orderId);
								CompleteCustomPointPurchase (userId, pointTransactionId, orderId);
							}
						}

					}
					else
					{
						string detailErrorMessage = string.Empty;

						// see comment inside ProcessReply() to see when it might return true.			  
						retry = ProcessReply (reply, ref userErrorMessage, ref detailErrorMessage);				  

						StoreUtility.UpdatePointTransaction (Convert.ToInt32 (drPpt ["point_transaction_id"]), (int) Constants.eTRANSACTION_STATUS.FAILED, "Reason Code=" + reply.reasonCode + ", UEM=" + userErrorMessage, reply.requestID);
					}
				}
				while (retry);
			}
            catch (CryptographicException se)
			{
				bSystemDown = true;
				HandleSignException (se, ref userErrorMessage);
			}
			catch (WebException we)
			{
				bSystemDown = true;
				HandleWebException (we, ref userErrorMessage);
			}
			catch (Exception exc)
			{
				bSystemDown = true;
				HandleGeneralException (exc, ref userErrorMessage);
			}

			return bSuccess;
		}

        /// <summary>
        /// ProcessPayment
        /// </summary>
        public bool ProcessPaymentUsingSubscription (DataRow drOrder, DataRow drPpt, string subscriptionId, string userIpAddress, ref string userErrorMessage, ref bool bSystemDown, bool bCapture)
        {
            RequestMessage request = new RequestMessage ();
            bool bSuccess = false;
            bSystemDown = false;
            int orderId = Convert.ToInt32 (drOrder["order_id"]);
            int pointTransactionId = Convert.ToInt32 (drPpt["point_transaction_id"]);
            int userId = Convert.ToInt32 (drOrder["user_id"]);
            Double dAmountDebited = Convert.ToDouble (drPpt["amount_debited"]);

            UserFacade userFacade = new UserFacade ();
            User user = userFacade.GetUser (userId);

            // obtain merchantID from the config file
            request.merchantID = MerchantId ();

            // we want to do Credit Card Authorization in this sample
            request.ccAuthService = new CCAuthService ();
            request.ccAuthService.run = "true";

            // Credit Card Capture
            if (bCapture)
            {
                request.ccCaptureService = new CCCaptureService ();
                request.ccCaptureService.run = "true";
            }

            // add required fields
            request.merchantReferenceCode = "KEN-" + orderId;  

            // Setup the purchaseTotals
            PurchaseTotals purchaseTotals = new PurchaseTotals ();
            purchaseTotals.currency = Currency ();
            purchaseTotals.grandTotalAmount = dAmountDebited.ToString ();
            request.purchaseTotals = purchaseTotals;
  
   
            // There is just one item, the points they are buying
            request.item = new Item[1];
            Item item = new Item ();
            item.id = pointTransactionId.ToString ();
            item.unitPrice = dAmountDebited.ToString ();
            item.productName = drPpt["description"].ToString ();
            item.quantity = "1";
            if (!drPpt["point_bucket_id"].Equals (DBNull.Value))
            {
                item.productSKU = "Point bucket id " + drPpt["point_bucket_id"].ToString ();
            }
            request.item[0] = item;
  
            // Need to add the subscription id so address & credit card info can be pulled
            RecurringSubscriptionInfo recurringSubscriptionInfo = new RecurringSubscriptionInfo ();
            recurringSubscriptionInfo.subscriptionID = subscriptionId;
            request.recurringSubscriptionInfo = recurringSubscriptionInfo;

            // Debugging purposes, the request
            m_logger.Debug ("Sending request to Cybersource: Order is " + request.merchantReferenceCode +
                    ", username = " + user.Username +
                    ", userId = " + Convert.ToInt32 (drOrder["user_id"]) +
                    ", pointTransactionId = " + Convert.ToInt32 (drPpt["point_transaction_id"]) +
                    ", dAmountDebited = " + Convert.ToDouble (drPpt["amount_debited"])
                );

            try
            {
                bool retry;

                do
                {
                    ReplyMessage reply = SoapClient.RunTransaction (request);

                    // Was it succesfull?
                    int reasonCode = int.Parse (reply.reasonCode);

                    try
                    {
                        // Debugging purposes, the reply
                        m_logger.Debug ("Reply from Cybersource: Order = " + reply.merchantReferenceCode +
                            " Cybersource ref id = " + reply.requestID +
                            " Decision = " + reply.decision +
                            ", reasonCode = " + reasonCode +
                            ", avsReasonCode = " + reply.ccAuthReply.avsCode + " - " + AVSCodeDescriptions (reply.ccAuthReply.avsCode) +
                            ", cvReasonCode = " + reply.ccAuthReply.cvCode + " - " + CVCodeDescriptions (reply.ccAuthReply.cvCode) +
                            ", missing fields = " + EnumerateValues (reply.missingField) +
                            ", invalid fields = " + EnumerateValues (reply.invalidField)
                            );
                    }
                    catch (Exception)
                    {
                        // Some of the things we are logging could be null
                        m_logger.Debug ("Reply from Cybersource Exc: reasonCode = " + reasonCode + ", reply = " + reply.ToString ());
                    }


                    if ("ACCEPT".Equals (reply.decision) && reasonCode.Equals (100))
                    {
                        bSuccess = true;
                        retry = false;

                        int transactionStatus = (int) Constants.eTRANSACTION_STATUS.VERIFIED;

                        if (bCapture)
                        {
                            // Mark it as verified
                            transactionStatus = (int) Constants.eTRANSACTION_STATUS.VERIFIED;
                        }
                        else
                        {
                            // Only authorized
                            transactionStatus = (int) Constants.eTRANSACTION_STATUS.AUTHORIZED;
                        }

                        // A succesfull order
                        StoreUtility.UpdatePointTransaction (pointTransactionId, transactionStatus, "", reply.requestID);

                        if (bCapture)
                        {
                            // Do nothing here.
                        }
                    }
                    else
                    {
                        string detailErrorMessage = string.Empty;

                        // see comment inside ProcessReply() to see when it might return true.			  
                        retry = ProcessReply (reply, ref userErrorMessage, ref detailErrorMessage);

                        StoreUtility.UpdatePointTransaction (Convert.ToInt32 (drPpt["point_transaction_id"]), (int) Constants.eTRANSACTION_STATUS.FAILED, "Reason Code=" + reply.reasonCode + ", UEM=" + userErrorMessage, reply.requestID);
                    }
                }
                while (retry);
            }
            catch (CryptographicException se)
            {
                bSystemDown = true;
                HandleSignException (se, ref userErrorMessage);
            }
            catch (WebException we)
            {
                bSystemDown = true;
                HandleWebException (we, ref userErrorMessage);
            }
            catch (Exception exc)
            {
                bSystemDown = true;
                HandleGeneralException (exc, ref userErrorMessage);
            }

            return bSuccess;
        }
        
        /// <summary>
        /// ProcessPayment
        /// </summary>
        public override bool CreateSubscription(DataRow drOrder, DataRow drPpt, string ccNumEncrypt, 
            string ccSecCodeEncrypt, string userIpAddress, ref bool bSystemDown, ref string userErrorMessage,
            ref string subscriptionIdReference, int subscriptionId, uint userSubscriptionId, global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm subTermFreePeriod, 
            global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm subTermFrequency)
        {
            RequestMessage request = new RequestMessage();
            bool bSuccess = false;
            bSystemDown = false;
            int orderId = Convert.ToInt32(drOrder["order_id"]);
            int pointTransactionId = Convert.ToInt32(drPpt["point_transaction_id"]);
            int userId = Convert.ToInt32(drOrder["user_id"]);
            int orderbillingInfoId = Convert.ToInt32(drPpt["order_billing_information_id"]);
            Double dAmountDebited = Convert.ToDouble(drPpt["amount_debited"]);

            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

            // obtain merchantID from the config file
            request.merchantID = MerchantId();

            // add required fields
            request.merchantReferenceCode = "KEN-S-" + userSubscriptionId.ToString ();

            DataRow drOrderBillingInfo = StoreUtility.GetOrderBillingInfo(orderbillingInfoId);

            // Create the first and last name.
            string firstName = "", lastName = "";
            GetNames(drOrderBillingInfo["address_name"].ToString(), ref firstName, ref lastName);

            BillTo billTo = new BillTo();
            billTo.firstName = firstName;
            billTo.lastName = lastName;
            billTo.street1 = drOrderBillingInfo["address1"].ToString();
            billTo.street2 = drOrderBillingInfo["address2"].ToString();
            billTo.city = drOrderBillingInfo["city"].ToString();
            billTo.state = drOrderBillingInfo["state_code"].ToString();
            billTo.postalCode = drOrderBillingInfo["zip_code"].ToString();
            billTo.country = drOrderBillingInfo["country_id"].ToString();
            billTo.email = user.Email;
            billTo.ipAddress = userIpAddress;
            billTo.phoneNumber = drOrderBillingInfo["phone_number"].ToString();
            billTo.customerID = user.Username;
            request.billTo = billTo;

            Card card = new Card();
            card.fullName = drOrderBillingInfo["name_on_card"].ToString();
            card.accountNumber = KanevaGlobals.Decrypt(ccNumEncrypt);

            if (ccSecCodeEncrypt != string.Empty)
            {
                card.cvNumber = KanevaGlobals.Decrypt(ccSecCodeEncrypt);
            }

            card.expirationMonth = drOrderBillingInfo["exp_month"].ToString();
            card.expirationYear = drOrderBillingInfo["exp_year"].ToString();
            card.cardType = GetCybersourceCardTypes (drOrderBillingInfo["card_type"].ToString());
            request.card = card;

            PurchaseTotals purchaseTotals = new PurchaseTotals();
            purchaseTotals.currency = Currency();
            request.purchaseTotals = purchaseTotals;

            // Set up the subscription
            request.paySubscriptionCreateService = new PaySubscriptionCreateService();
            request.paySubscriptionCreateService.run = "true";

            CyberSource.Clients.SoapServiceReference.Subscription sub = new CyberSource.Clients.SoapServiceReference.Subscription();
            sub.title = drPpt["description"].ToString();
            sub.paymentMethod = "credit card";
            request.subscription = sub;

            RecurringSubscriptionInfo rsi = new RecurringSubscriptionInfo();
            rsi.frequency = GetSubcriptionLengthFromKanevaConstants(subTermFrequency);
            rsi.amount = dAmountDebited.ToString();

            // Is there a free trial period?
            if (!subTermFreePeriod.Equals (0))
            {
                rsi.startDate = GetTrialLengthFromKanevaConstants(subTermFreePeriod).ToString("yyyyMMdd");
            }

            //rsi..numberOfPayments
            //rsi.automaticRenew
            //rsi.startDate = Default is tomorrow
            request.recurringSubscriptionInfo = rsi;

// Reply
//merchantReferenceCode=14344
//requestID=0622042292190008497821
//requestToken=AA4JUrWguaLLQxMUGwxSWVdPS1BIRk5IMUwA2yCv
//decision=ACCEPT
//reasonCode=100
//paySubscriptionCreateReply_reasonCode=100
//paySubscriptionCreateReply_subscriptionID=0622042292190167904150

            // Debugging purposes, the request
            m_logger.Debug("Sending create subscription request to Cybersource: Order is " + request.merchantReferenceCode);

            ReplyMessage reply = new ReplyMessage();
            bSystemDown = SendRequest(request, ref reply, ref userErrorMessage);

            if (!bSystemDown)
            {
                if ("ACCEPT".Equals(reply.decision) && reply.reasonCode.Equals("100"))
                {
                    bSuccess = true;

                    // A succesfull order
                    StoreUtility.UpdatePointTransaction(pointTransactionId, (int)Constants.eTRANSACTION_STATUS.VERIFIED, "", reply.requestID);
                    StoreUtility.UpdateOrderToComplete(orderId, userId);

                    // Record the subscription id
                    subscriptionIdReference = reply.paySubscriptionCreateReply.subscriptionID;
                    //reply.paySubscriptionCreateReply.reasonCode
                }
                else
                {
                    string detailErrorMessage = string.Empty;

                    // see comment inside ProcessReply() to see when it might return true.			  
                    ProcessReply(reply, ref userErrorMessage, ref detailErrorMessage);

                    StoreUtility.UpdatePointTransaction(Convert.ToInt32(drPpt["point_transaction_id"]), (int)Constants.eTRANSACTION_STATUS.FAILED, "Reason Code=" + reply.reasonCode + ", UEM=" + userErrorMessage, reply.requestID);
                }
            }

            return bSuccess;
        }

        /// <summary>
        /// GetSubcriptionLengthFromKanevaConstants
        /// </summary>
        /// <param name="subTermFrequency"></param>
        /// <returns></returns>
        public string GetSubcriptionLengthFromKanevaConstants (global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm subTermFrequency)
        {
            switch ((int) subTermFrequency)
            {
                case (int) global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.None:
                        return "";

                case (int) global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.BiMonthly:
                    return "semi-monthly";

                case (int) global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.BiWeekly:
                    return "bi-weekly";

                case (int) global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.Monthly:
                    return "monthly";

                case (int) global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.Weekly:
                    return "weekly";

                case (int) global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.Yearly:
                    return "annually";

                case (int) global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.Quarterly:
                    return "quarterly";

                case (int) global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.SemiAnnually:
                    return "semi-annually";
                    
                default:
                    return "monthly";
            }
        }

        /// <summary>
        /// GetSubcriptionLengthFromKanevaConstants
        /// </summary>
        /// <param name="subTermFrequency"></param>
        /// <returns></returns>
        public DateTime GetTrialLengthFromKanevaConstants(global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm subTermTrial)
        {
            switch ((int)subTermTrial)
            {
                case (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.None:
                    // This is in Cybersource adapter only 
                    // (Other billing adaptors may implement this function different i.e. Paypal adaptor might be DateTime.Now)
                    // For cybersource they may not be billed till sometime next day on initial creation,
                    // So don't expire yet since we don't extend till billing comes in.
                    DateTime dtTomorrow = DateTime.Now.AddDays(1);
                    return new DateTime ( dtTomorrow.Year, dtTomorrow.Month, dtTomorrow.Day, 23, 59, 59);
                case (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.BiMonthly:
                    return DateTime.Now.AddMonths (2);

                case (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.BiWeekly:
                    return DateTime.Now.AddDays (14);

                case (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.Monthly:
                    return DateTime.Now.AddMonths (1);

                case (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.Weekly:
                    return DateTime.Now.AddDays (7);

                case (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.Yearly:
                    return DateTime.Now.AddYears (1);

                case (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.Quarterly:
                    return DateTime.Now.AddMonths (3);

                case (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.SemiAnnually:
                    return DateTime.Now.AddDays (182);

                default:
                    return DateTime.Now;
            }
        }

        /// <summary>
        /// GetSubcriptionConstantsFromDays
        /// </summary>
        public int GetSubcriptionConstantsFromDays (int numberOfDays)
        {
            switch (numberOfDays)
            {
                case 0:
                    return (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.None;

                case 60:
                    return (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.BiMonthly;

                case 14:
                    return (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.BiWeekly;

                case 30:
                    return (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.Monthly;

                case 7:
                    return (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.Weekly;

                case 365:
                    return (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.Yearly;

                case 90:
                    return (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.Quarterly;

                case 182:
                    return (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.SemiAnnually;

                default:
                    return (int)global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionTerm.None;
            }
        }

        /// <summary>
        /// Cancels all of a user's subscriptions.
        /// </summary>
        public void CancelAllUserSubscriptions(int userId, string cancellationReason)
        {
            SubscriptionFacade subFacade = new SubscriptionFacade();

            List<UserSubscription> usersubscriptions = subFacade.GetUserSubscriptions(userId);
            if (usersubscriptions.Count > 0)
            {
                bool bSystemDown = false;
                string userErrorMessage = "";

                foreach (UserSubscription userSubscription in usersubscriptions)
                {
                    userSubscription.UserCancellationReason = cancellationReason;

                    bool succeed = false;

                    if (userSubscription.IsLegacy)
                    {
                        succeed = this.CancelSubscription(ref bSystemDown, ref userErrorMessage, userSubscription);
                    }
                    else
                    {
                        // Update DB
                        userSubscription.StatusId = global::Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionStatus.Cancelled;
                        subFacade.UpdateUserSubscription(userSubscription);

                        // No need to call cybersource
                        succeed = true;
                    }
                }

                // Tickle the wok servers
                (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.PlayerPassList, RemoteEventSender.ChangeType.UpdateObject, userId);
            }
        }

        /// <summary>
        /// CancelSubscription
        /// </summary>
        public override bool CancelSubscription(ref bool bSystemDown, ref string userErrorMessage, GameSubscription gameSubscription, int orderId)
        {
            RequestMessage request = new RequestMessage();
            bool bSuccess = false;
            bSystemDown = false;
            
            // obtain merchantID from the config file
            request.merchantID = MerchantId();

            // add required fields
            request.merchantReferenceCode = "KEN-" + orderId;

            request.paySubscriptionUpdateService = new PaySubscriptionUpdateService();
            request.paySubscriptionUpdateService.run = "true";

            RecurringSubscriptionInfo recurringSubscriptionInfo = new RecurringSubscriptionInfo();
            recurringSubscriptionInfo.subscriptionID = gameSubscription.SubscriptionIdentifier;
            recurringSubscriptionInfo.status = "cancel";
            request.recurringSubscriptionInfo = recurringSubscriptionInfo;

            // Debugging purposes, the request
            m_logger.Debug("Sending cancel subsrciption to Cybersource: Order is " + request.merchantReferenceCode);

            ReplyMessage reply = new ReplyMessage();
            bSystemDown = SendRequest(request, ref reply, ref userErrorMessage);

            if (!bSystemDown)
            {
                if ("ACCEPT".Equals(reply.decision) && reply.reasonCode.Equals("100"))
                {
                    bSuccess = true;

                    // A succesfull cancel
                    GameFacade gameFacade = new GameFacade();
                    gameSubscription.UserCancelled = true;
                    gameSubscription.GsStatusId = (int) GameSubscription.SubscriptionStatus.Cancelled;
                    gameFacade.UpdateGameSubscription(gameSubscription);
                }
                else
                {
                    string detailErrorMessage = string.Empty;

                    // see comment inside ProcessReply() to see when it might return true.			  
                    ProcessReply(reply, ref userErrorMessage, ref detailErrorMessage);
                }
            }

            return bSuccess;
        }

        /// <summary>
        /// UpdateSubscription
        /// </summary>
        public override bool UpdateSubscription(ref bool bSystemDown, ref string userErrorMessage, UserSubscription userSubscription)
        {
            RequestMessage request = new RequestMessage();
            bool bSuccess = false;
            bSystemDown = false;

            // obtain merchantID from the config file
            request.merchantID = MerchantId();

            // add required fields
            request.merchantReferenceCode = "KEN-S-" + userSubscription.UserSubscriptionId;

            request.paySubscriptionUpdateService = new PaySubscriptionUpdateService();
            request.paySubscriptionUpdateService.run = "true";

            RecurringSubscriptionInfo recurringSubscriptionInfo = new RecurringSubscriptionInfo();
            recurringSubscriptionInfo.subscriptionID = userSubscription.SubscriptionIdentifier;
            recurringSubscriptionInfo.amount = userSubscription.Price.ToString();
            recurringSubscriptionInfo.endDate = userSubscription.EndDate.ToString();
            //recurringSubscriptionInfo.status = userSubscription.StatusId.ToString();

            request.recurringSubscriptionInfo = recurringSubscriptionInfo;

            // Debugging purposes, the request
            m_logger.Debug("Sending update subsrciption to Cybersource: Order is " + request.merchantReferenceCode);

            ReplyMessage reply = new ReplyMessage();
            bSystemDown = SendRequest(request, ref reply, ref userErrorMessage);

            if (!bSystemDown)
            {
                if ("ACCEPT".Equals(reply.decision) && reply.reasonCode.Equals("100"))
                {
                    bSuccess = true;
                }
                else
                {
                    string detailErrorMessage = string.Empty;

                    // see comment inside ProcessReply() to see when it might return true.			  
                    ProcessReply(reply, ref userErrorMessage, ref detailErrorMessage);
                }
            }

            return bSuccess;
        }


        /// <summary>
        /// CancelSubscription
        /// </summary>
        public override bool CancelSubscription(ref bool bSystemDown, ref string userErrorMessage, UserSubscription userSubscription)
        {
            RequestMessage request = new RequestMessage();
            bool bSuccess = false;
            bSystemDown = false;

            // obtain merchantID from the config file
            request.merchantID = MerchantId();

            // add required fields
            request.merchantReferenceCode = "KEN-S-" + userSubscription.UserSubscriptionId;

            request.paySubscriptionUpdateService = new PaySubscriptionUpdateService();
            request.paySubscriptionUpdateService.run = "true";

            RecurringSubscriptionInfo recurringSubscriptionInfo = new RecurringSubscriptionInfo();
            recurringSubscriptionInfo.subscriptionID = userSubscription.SubscriptionIdentifier;
            recurringSubscriptionInfo.status = "cancel";
            request.recurringSubscriptionInfo = recurringSubscriptionInfo;

            // Debugging purposes, the request
            m_logger.Debug("Sending cancel subsrciption to Cybersource: Order is " + request.merchantReferenceCode);

            ReplyMessage reply = new ReplyMessage();
            bSystemDown = SendRequest(request, ref reply, ref userErrorMessage);

            if (!bSystemDown)
            {
                if ("ACCEPT".Equals(reply.decision) && reply.reasonCode.Equals("100"))
                {
                    bSuccess = true;

                    // A succesfull cancel
                    SubscriptionFacade subscriptionFacade = new SubscriptionFacade();
                    userSubscription.UserCancelled = true;
                    userSubscription.StatusId = global:: Kaneva.BusinessLayer.BusinessObjects.Subscription.SubscriptionStatus.Cancelled;
                    subscriptionFacade.UpdateUserSubscription(userSubscription);
                }
                else
                {
                    string detailErrorMessage = string.Empty;

                    // see comment inside ProcessReply() to see when it might return true.			  
                    ProcessReply(reply, ref userErrorMessage, ref detailErrorMessage);
                }
            }

            return bSuccess;
        }

        /// <summary>
        /// GetSubscription
        /// </summary>
        public override SubscriptionInfo GetSubscription(ref bool bSuccess, ref bool bSystemDown, ref string userErrorMessage, string subscriptionIdReference, int subscriptionId)
        {
            RequestMessage request = new RequestMessage();
            SubscriptionInfo subscriptionInfo = new SubscriptionInfo(subscriptionId, subscriptionIdReference, "");
            bSuccess = false;
            bSystemDown = false;

            // obtain merchantID from the config file
            request.merchantID = MerchantId();

            // add required fields
            request.merchantReferenceCode = "KEN-" + subscriptionId;

            request.paySubscriptionRetrieveService = new PaySubscriptionRetrieveService();
            request.paySubscriptionRetrieveService.run = "true";

            RecurringSubscriptionInfo recurringSubscriptionInfo = new RecurringSubscriptionInfo();
            recurringSubscriptionInfo.subscriptionID = subscriptionIdReference;
            request.recurringSubscriptionInfo = recurringSubscriptionInfo;

            // Debugging purposes, the request
            m_logger.Debug("Sending get subsrciption to Cybersource: Order is " + request.merchantReferenceCode);

            ReplyMessage reply = new ReplyMessage();
            bSystemDown = SendRequest(request, ref reply, ref userErrorMessage);

            if (!bSystemDown)
            {
                if ("ACCEPT".Equals(reply.decision) && reply.reasonCode.Equals("100"))
                {
                    bSuccess = true;

                    // A succesfull get
                    subscriptionInfo.Status = reply.paySubscriptionRetrieveReply.status;
                }
                else
                {
                    string detailErrorMessage = string.Empty;

                    // see comment inside ProcessReply() to see when it might return true.			  
                    ProcessReply(reply, ref userErrorMessage, ref detailErrorMessage);
                }
            }

            return subscriptionInfo;
        }

        /// <summary>
        /// SendRequest to Cybersource
        /// </summary>
        private bool SendRequest(RequestMessage request, ref ReplyMessage reply, ref string userErrorMessage)
        {
            bool bSystemDown = false;

            try
            {
                reply = SoapClient.RunTransaction(request);

                try
                {
                    // Debugging purposes, the reply
                    m_logger.Debug("Reply from Cybersource: Order = " + reply.merchantReferenceCode +
                        " Cybersource ref id = " + reply.requestID +
                        " Decision = " + reply.decision +
                        ", reasonCode = " + reply.reasonCode +
                        ", avsReasonCode = " + reply.ccAuthReply.avsCode + " - " + AVSCodeDescriptions(reply.ccAuthReply.avsCode) +
                        ", cvReasonCode = " + reply.ccAuthReply.cvCode + " - " + CVCodeDescriptions(reply.ccAuthReply.cvCode) +
                        ", missing fields = " + EnumerateValues(reply.missingField) +
                        ", invalid fields = " + EnumerateValues(reply.invalidField)
                        );
                }
                catch (Exception)
                {
                    // Some of the things we are logging could be null
                    m_logger.Debug("Reply from Cybersource Exc: reasonCode = " + reply.reasonCode + ", reply = " + reply.ToString());
                }
            }
            catch (CryptographicException se)
            {
                bSystemDown = true;
                HandleSignException(se, ref userErrorMessage);
            }
            catch (WebException we)
            {
                bSystemDown = true;
                HandleWebException(we, ref userErrorMessage);
            }
            catch (Exception exc)
            {
                bSystemDown = true;
                HandleGeneralException(exc, ref userErrorMessage);
            }

            return bSystemDown;
        }

        /// <summary>
        /// GetCybersourceCardTypes
        /// </summary>
        private string GetCybersourceCardTypes(string cardType)
        {
            cardType = cardType.ToUpper ();
            
            switch (cardType){
                case "VISA":
                    return "001";
                case "MASTERCARD":
                    return "002";
                case "AMERICAN EXPRESS":
                    return "003";
                case "DISCOVER":
                    return "004";
                case "DINERS CLUB":
                    return "005";
                case "JCB":
                    return "007";
                default:
                    return "";
            }
        }

        private string GetCardNameFromCardType (string cardType)
        {
            switch (cardType)
            {
                case "001":
                    return "Visa";
                case "002":
                    return "MasterCard";
                case "003":
                    return "American Express";
                case "004":
                    return "Discover";
                case "005":
                    return "Diners Club";
                case "007":
                    return "JCB";
                default:
                    return "";
            }
        }

		/// <summary>
		/// ProcessReply
		/// </summary>
		/// <param name="reply"></param>
		/// <returns></returns>
		private bool ProcessReply ( ReplyMessage reply, ref string userErrorMessage, ref string detailErrorMessage)
		{
			// Get the user message
			userErrorMessage = GetContent( reply, ref detailErrorMessage );
			return( false );
		}

		/// <summary>
		/// GetContent
		/// </summary>
		/// <param name="reply"></param>
		/// <returns></returns>
		private string GetContent ( ReplyMessage reply, ref string detailErrorMessage )
		{
			// This is where we retrieve the user error messages
			int reasonCode = int.Parse( reply.reasonCode );

			switch (reasonCode)
			{
				// Success
				case 100:
					return( 
						"Request ID: " + reply.requestID +
						"<BR>Authorized Amount: " +
						reply.ccAuthReply.amount +
						"<BR>Authorization Code: " +
						reply.ccAuthReply.authorizationCode +
						"<BR>Authorization Time: " + 
						reply.ccAuthReply.authorizedDateTime );

				// Missing field(s)
				case 101:																		  
					return(																		  
						"The following required field(s) are missing:<BR>" +
						EnumerateValues( reply.missingField ) );

				// Invalid field(s)
				case 102:
					return "The following field(s) are invalid:<BR>" +
						EnumerateValues( reply.invalidField );

				// Address Verification Failed
				case 200:
                case 520:
                    detailErrorMessage = AVSCodeDescriptions(reply.ccAuthReply.avsCode);

                    return "The address verification failed. " + AVSCodeDescriptions(reply.ccAuthReply.avsCode);

                // Expired card
                case 202:
                    return "Card is expired. Please use a different card.";

                // General decline of card. No other info provided
                case 203:
                    return "Card is declined. " + AVSCodeDescriptions(reply.ccAuthReply.avsCode);

				// Insufficient funds
				case 204:
					return "Insufficient funds in the account.  Please use a different card." ;

				case 211:  // Invalid Card Verification Number
					detailErrorMessage = CVCodeDescriptions (reply.ccAuthReply.cvCode);

					if (reply.ccAuthReply.cvCode == "N")  // N = card verification number not matched
					{
						return "The card security code did not match the card.  Please re-enter the security code.";
					}

					return "The card security code failed.  Please use a different card or re-enter the security code.";

                // Invalid account number
                case 231:
                    return "Invalid account number.";
                
                default:
					// For all other reason codes, return a general error message.
					return "There was a problem processing your payment. Please check your information and try again.";
			}	
		}

		/// <summary>
		/// HandleSignException
		/// </summary>
		/// <param name="se"></param>
        private void HandleSignException(CryptographicException se, ref string userErrorMessage)
		{
			// Technical error
			userErrorMessage = String.Format( 
				"Failed to sign the request with error code '{0}' and " +
				"message '{1}'.",se.HResult, se.Message );

			m_logger.Error ("Error running Cybersource credit transaction " + userErrorMessage, se);
            MailUtility.SendEmail(KanevaGlobals.FromEmail, "WebDevTeam@kaneva.com", "Billing System Possibly Down! (Sign Exception) on " + System.Net.Dns.GetHostName(), userErrorMessage, false, false, 1);

			// User displayable error
			userErrorMessage = cGENERAL_ERROR;
		}


		/// <summary>
		/// HandleWebException
		/// </summary>
		/// <param name="we"></param>
		private void HandleWebException ( WebException we, ref string userErrorMessage)
		{
			// Technical error
			userErrorMessage = String.Format( 
				"Failed to get a response with status '{0}' and " +
				"message '{1}'", we.Status, we.Message );

			m_logger.Error ("Error running Cybersource credit transaction " + userErrorMessage, we);

			if (IsCriticalError( we ))
			{
				/*
				 * The transaction may have been completed by CyberSource. If your request included a payment service, you should
				 * notify the appropriate department in your company (e.g. by sending an email) so that they can confirm if the request
				 * did in fact complete by searching the CyberSource Support Screens using the value of the merchantReferenceCode in
				 * your request.
				 */
				string messageBody = "The transaction may have been completed by CyberSource. " +
					" If your request included a payment service, you should notify the appropriate department in your company (e.g. by " +
					" sending an email) so that they can confirm if the request did in fact complete by searching the CyberSource Support " +
					" Screens using the value of the merchantReferenceCode in your Request.";

				m_logger.Warn (messageBody);
                MailUtility.SendEmail(KanevaGlobals.FromEmail, "WebDevTeam@kaneva.com", "Billing needs confirmation" + System.Net.Dns.GetHostName(), messageBody, false, false, 1);
			}
			else
			{
                MailUtility.SendEmail(KanevaGlobals.FromEmail, "WebDevTeam@kaneva.com", "Billing System Possibly Down! (Web Exception) on " + System.Net.Dns.GetHostName(), userErrorMessage, false, false, 1);
			}

			// User displayable error
			userErrorMessage = cGENERAL_ERROR;
		}

		/// <summary>
		/// HandleGeneralException
		/// </summary>
		/// <param name="she"></param>
		private void HandleGeneralException (Exception exc, ref string userErrorMessage)
		{
			// Technical error
			m_logger.Error ("Error running Cybersource credit transaction ", exc);
            MailUtility.SendEmail(KanevaGlobals.FromEmail, "WebDevTeam@kaneva.com", "Billing System Possibly Down! (General Error) on " + System.Net.Dns.GetHostName(), exc.ToString(), false, false, 1);

			// User displayable error
			userErrorMessage = cGENERAL_ERROR;
		}

		/// <summary>
		/// EnumerateValues
		/// </summary>
		/// <param name="array"></param>
		/// <returns></returns>
		private string EnumerateValues ( string[] array )
		{
			if (array == null)
			{
				return "";
			}

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			foreach (string val in array)
			{
				sb.Append( val + "<br>" );
			}

			return( sb.ToString() );
		}

		/// <summary>
		/// IsCriticalError
		/// </summary>
		/// <param name="we"></param>
		/// <returns></returns>
		private bool IsCriticalError ( WebException we )
		{
			switch (we.Status)
			{
				case WebExceptionStatus.ProtocolError:
					if (we.Response != null)
					{
						HttpWebResponse response = (HttpWebResponse) we.Response;

						// GatewayTimeout may be returned if you are connecting through a proxy server.
						return(	response.StatusCode == HttpStatusCode.GatewayTimeout );
					}

					// In case of ProtocolError, the Response property should always be present.  In the unlikely case 
					// that it is not, we assume something went wrong along the way and to be safe, treat it as a
					// critical error.
					return( true );

				case WebExceptionStatus.ConnectFailure:
				case WebExceptionStatus.NameResolutionFailure:
				case WebExceptionStatus.ProxyNameResolutionFailure:
				case WebExceptionStatus.SendFailure:
					return( false );

				default:
					return( true );
			}
		}

		/// <summary>
		/// CompleteCustomPointPurchase
		/// </summary>
		private void CompleteCustomPointPurchase (int userId, int purchasePointTransactionId, int orderId)
		{
			try
			{
				// Get the k-point amount to credit
				Double kpointAmount = StoreUtility.GetPurchasePointTransactionAmount (purchasePointTransactionId, Constants.CURR_KPOINT);

				// Custom amount
				if (kpointAmount.Equals (0.0))
				{
					// Mark it as item not found
					StoreUtility.UpdatePointTransaction (purchasePointTransactionId, (int) Constants.eTRANSACTION_STATUS.ITEM_NOT_FOUND, "No amount found", "");
					return;
				}

				// If credit card passes, credit k-point balance, write transaction record
				(new UserFacade()).AdjustUserBalance (userId, Constants.CURR_KPOINT, kpointAmount, Constants.CASH_TT_BOUGHT_CREDITS);

				// Where they purchasing assets? This would have meant they tried to purchase assets but need to buy K-points
				// to complete the transaction
				CompletePendingOrder (userId, orderId);
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error in CompleteCustomPointPurchase", exc);
			}
		}

		/// <summary>
		/// CompletePointBucketPurchase
		/// </summary>
		private void CompletePointBucketPurchase (int userId, int purchasePointTransactionId, int pointBucketId, int orderId)
		{
			try
			{
				// Update user balances with the K-Point amounts
				Double pointBucketAmount = 0;
				Double pointBucketFreeAmount = 0;
				string pointBucketKEIPoint = Constants.CURR_KPOINT;

                DataRow drPointBucket = StoreUtility.GetPromotion(pointBucketId);

				// No point bucket found here means something messed up somewhere
				if (drPointBucket == null)
				{
					// Mark it as item not found
					StoreUtility.UpdatePointTransaction (purchasePointTransactionId, (int) Constants.eTRANSACTION_STATUS.ITEM_NOT_FOUND, "No Matching Point Bucket Found", "");
				}
				else
				{
					pointBucketKEIPoint = drPointBucket ["kei_point_id"].ToString ();
					pointBucketAmount = Convert.ToDouble (drPointBucket ["kei_point_amount"]);
					pointBucketFreeAmount = Convert.ToDouble (drPointBucket ["free_points_awarded_amount"]);
				}

                if (pointBucketAmount > 0)
                {
                    // If credit card passes, credit k-point balance, write transaction record
                    (new UserFacade()).AdjustUserBalance(userId, pointBucketKEIPoint, pointBucketAmount, Constants.CASH_TT_BOUGHT_CREDITS);
                }

				// Any bonus points?
				if (pointBucketFreeAmount > 0)
				{
                    (new UserFacade()).AdjustUserBalance(userId, drPointBucket["free_kei_point_id"].ToString(), pointBucketFreeAmount, Constants.CASH_TT_BOUGHT_CREDITS);
				}

				// Where they purchasing assets? This would have meant they tried to purchase assets but need to buy K-points
				// to complete the transaction
				CompletePendingOrder (userId, orderId);
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error in CompletePointBucketPurchase", exc);
			}
		}

		/// <summary>
		/// CompletePendingOrder - this completes a pending order, a pending order is an order that attempted but
		/// they needed purchase some k-Points first.
		/// </summary>
		/// <param name="orderId"></param>
		private void CompletePendingOrder (int userId, int orderId)
		{
			try
			{
				// Mark it as completed, Add the subscriptions, Deduct points
				int result = StoreUtility.PurchaseOrderNow (userId, orderId, "");
					
				// Did it fail
				if (result == -1)
				{
					// Don't continue any further
					return;
				}
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error in CompletePendingOrder", exc);
			}
		}

		/// <summary>
		/// GetNames
		/// </summary>
		/// <param name="name"></param>
		/// <param name="firstName"></param>
		/// <param name="lastName"></param>
		private void GetNames (string name, ref string firstName, ref string lastName)
		{
			int iSpace = name.IndexOf (" ");

			if (iSpace > -1)
			{
				firstName = name.Substring (0, iSpace).Trim ();
				lastName = name.Substring (iSpace).Trim ();
			}
			else
			{
				firstName = name;
				lastName = name;
			}

		}

        /// <summary>
        /// CreateBillingProfile
        /// </summary>
        public bool CreateBillingSubscription(int userId, uint userSubId, BillingInfo billInfo, string userIpAddress, 
            ref bool bSystemDown, ref string userErrorMessage, ref string billingSubscriptionId) 
        {
            RequestMessage request = new RequestMessage ();
            bool bSuccess = false;
            bSystemDown = false;

            UserFacade userFacade = new UserFacade ();
            User user = userFacade.GetUser (userId);

            // obtain merchantID from the config file
            request.merchantID = MerchantId ();

            // add required fields
            string merchantReferenceCode = "KEN-UID-" + userId.ToString ();

            if (userSubId > 0)
            {
                merchantReferenceCode += "-US" + userSubId.ToString();
            }

            request.merchantReferenceCode = merchantReferenceCode;

            // Create the first and last name.
            string firstName = "", lastName = "";
            GetNames (billInfo.FullName, ref firstName, ref lastName);

            BillTo billTo = new BillTo ();
            billTo.firstName = firstName;
            billTo.lastName = lastName;
            billTo.street1 = billInfo.Address1;
            billTo.street2 = billInfo.Address2;
            billTo.city = billInfo.City;
            billTo.state = billInfo.State;
            billTo.postalCode = billInfo.PostalCode;
            billTo.country = billInfo.Country;
            billTo.email = user.Email;
            billTo.ipAddress = userIpAddress;
            billTo.phoneNumber = billInfo.PhoneNumber;
            billTo.customerID = user.Username;
            request.billTo = billTo;

            // Set the credit card information
            Card card = new Card ();
            card.fullName = billInfo.FullName;
            card.accountNumber = KanevaGlobals.Decrypt (billInfo.CardNumberEncrypt);

            if (billInfo.CardSecurityCodeEncrypt != string.Empty)
            {
                card.cvNumber = KanevaGlobals.Decrypt (billInfo.CardSecurityCodeEncrypt);
            }

            card.expirationMonth = billInfo.CardExpirationMonth;
            card.expirationYear = billInfo.CardExpirationYear;
            card.cardType = GetCybersourceCardTypes (billInfo.CardType);
            request.card = card;

            // Set the currency
            PurchaseTotals purchaseTotals = new PurchaseTotals ();
            purchaseTotals.currency = Currency ();
            request.purchaseTotals = purchaseTotals;

            // Set up the profile
            request.paySubscriptionCreateService = new PaySubscriptionCreateService ();
            request.paySubscriptionCreateService.run = "true";

            RecurringSubscriptionInfo rsi = new RecurringSubscriptionInfo ();
            rsi.frequency = "on-demand";
            request.recurringSubscriptionInfo = rsi;

            // Debugging purposes, the request
            m_logger.Debug ("Sending create Billing Subscription request to Cybersource: UserId = " + userId.ToString ());      

            ReplyMessage reply = new ReplyMessage ();
            bSystemDown = SendRequest (request, ref reply, ref userErrorMessage);

            if (!bSystemDown)
            {
                if ("ACCEPT".Equals (reply.decision) && reply.reasonCode.Equals ("100"))
                {
                    bSuccess = true;

                    // Record the subscription id
                    billingSubscriptionId = reply.paySubscriptionCreateReply.subscriptionID;
                }
                else
                {
                    string detailErrorMessage = string.Empty;

                    // see comment inside ProcessReply() to see when it might return true.			  
                    ProcessReply (reply, ref userErrorMessage, ref detailErrorMessage);
                }
            }

            return bSuccess;
        }

        /// <summary>
        /// UpdateBillingProfile
        /// </summary>
        public bool UpdateBillingProfile(int userId, uint userSubId, BillingInfo billInfo, string userIpAddress, ref bool bSystemDown, ref string userErrorMessage)
        {
            RequestMessage request = new RequestMessage ();
            bool bSuccess = false;
            bSystemDown = false;

            UserFacade userFacade = new UserFacade ();
            User user = userFacade.GetUser (userId);

            // obtain merchantID from the config file
            request.merchantID = MerchantId ();

            // add required fields
            // add required fields
            string merchantReferenceCode = "KEN-UID-" + userId.ToString();

            if (userSubId > 0)
            {
                merchantReferenceCode += "-US" + userSubId.ToString();
            }

            request.merchantReferenceCode = merchantReferenceCode;

            // Create the first and last name.
            string firstName = "", lastName = "";
            GetNames (billInfo.FullName, ref firstName, ref lastName);

            BillTo billTo = new BillTo ();
            billTo.firstName = firstName;
            billTo.lastName = lastName;
            billTo.street1 = billInfo.Address1;
            billTo.street2 = billInfo.Address2;
            billTo.city = billInfo.City;
            billTo.state = billInfo.State;
            billTo.postalCode = billInfo.PostalCode;
            billTo.country = billInfo.Country;
            billTo.ipAddress = userIpAddress;
            billTo.phoneNumber = billInfo.PhoneNumber;
            request.billTo = billTo;

            // only update card info it if is included
            if (billInfo.CardType != string.Empty)
            {
                // Set the credit card information
                Card card = new Card ();
                card.fullName = billInfo.FullName;
                if (billInfo.CardNumberEncrypt != string.Empty)
                {
                    card.accountNumber = KanevaGlobals.Decrypt (billInfo.CardNumberEncrypt);
                }

                //if (billInfo.CardSecurityCodeEncrypt != string.Empty)
                //{
                //    card.cvNumber = KanevaGlobals.Decrypt (billInfo.CardSecurityCodeEncrypt);
                //}

                card.expirationMonth = billInfo.CardExpirationMonth;
                card.expirationYear = billInfo.CardExpirationYear;
                card.cardType = GetCybersourceCardTypes (billInfo.CardType);
                request.card = card;
            }

            // Set up the profile
            request.paySubscriptionUpdateService = new PaySubscriptionUpdateService ();
            request.paySubscriptionUpdateService.run = "true";
            
            RecurringSubscriptionInfo rsi = new RecurringSubscriptionInfo ();
            rsi.subscriptionID = billInfo.SubscriptionId;
            request.recurringSubscriptionInfo = rsi;

            // Debugging purposes, the request
            m_logger.Debug ("Sending update subscription request to Cybersource: UserId is " + userId.ToString ());      

            ReplyMessage reply = new ReplyMessage ();
            bSystemDown = SendRequest (request, ref reply, ref userErrorMessage);

            if (!bSystemDown)
            {
                if ("ACCEPT".Equals (reply.decision) && reply.reasonCode.Equals ("100"))
                {
                    bSuccess = true;
                }
                else
                {
                    string detailErrorMessage = string.Empty;

                    // see comment inside ProcessReply() to see when it might return true.			  
                    ProcessReply (reply, ref userErrorMessage, ref detailErrorMessage);
                }
            }

            return bSuccess;
        }

        /// <summary>
        /// ClearBillingProfile
        /// </summary>
        public bool ClearBillingSubscription (int userId, BillingInfo billInfo, ref bool bSystemDown, ref string userErrorMessage)
        {
            RequestMessage request = new RequestMessage ();
            bool bSuccess = false;
            bSystemDown = false;

            // obtain merchantID from the config file
            request.merchantID = MerchantId ();

            // add required fields
            request.merchantReferenceCode = "KEN-UID-" + userId.ToString () + "-CANCEL-" + 
                DateTime.Now.ToString ("MM" + '_'+ "dd" + '_' + "yy" + '_' + "HH" + ':' + "mm");

            // Create the first and last name.
            string firstName = "", lastName = "";
            GetNames (billInfo.FullName, ref firstName, ref lastName);

            BillTo billTo = new BillTo ();
            billTo.firstName = firstName;
            billTo.lastName = lastName;
            billTo.street1 = billInfo.Address1;
            billTo.street2 = billInfo.Address2;
            billTo.city = billInfo.City;
            billTo.state = billInfo.State;
            billTo.postalCode = billInfo.PostalCode;
            billTo.country = billInfo.Country;
            billTo.ipAddress = "000.000.000.000";
            billTo.phoneNumber = billInfo.PhoneNumber;
            request.billTo = billTo;

            // Set the credit card information
            Card card = new Card ();
            card.fullName = billInfo.FullName;
            if (billInfo.CardNumberEncrypt != string.Empty)
            {
                card.accountNumber = KanevaGlobals.Decrypt (billInfo.CardNumberEncrypt);
            }

            //if (billInfo.CardSecurityCodeEncrypt != string.Empty)
            //{
            //    card.cvNumber = KanevaGlobals.Decrypt (billInfo.CardSecurityCodeEncrypt);
            //}

            card.expirationMonth = billInfo.CardExpirationMonth;
            card.expirationYear = billInfo.CardExpirationYear;
            card.cardType = GetCybersourceCardTypes (billInfo.CardType);
            request.card = card;

            // Set up the profile
            request.paySubscriptionUpdateService = new PaySubscriptionUpdateService ();
            request.paySubscriptionUpdateService.run = "true";

            // Set the subscription id
            RecurringSubscriptionInfo rsi = new RecurringSubscriptionInfo ();
            rsi.subscriptionID = billInfo.SubscriptionId;
            request.recurringSubscriptionInfo = rsi;

            // Debugging purposes, the request
            m_logger.Debug ("Sending clear profile request to Cybersource: UserId is " + userId.ToString ());      

            ReplyMessage reply = new ReplyMessage ();
            bSystemDown = SendRequest (request, ref reply, ref userErrorMessage);

            if (!bSystemDown)
            {
                if ("ACCEPT".Equals (reply.decision) && reply.reasonCode.Equals ("100"))
                {
                    bSuccess = true;
                }
                else
                {
                    string detailErrorMessage = string.Empty;

                    // see comment inside ProcessReply() to see when it might return true.			  
                    ProcessReply (reply, ref userErrorMessage, ref detailErrorMessage);
                }
            }

            return bSuccess;
        }

        /// <summary>
        /// GetBillingProfile
        /// </summary>
        public bool GetBillingSubscription (int userId, string billingSubscriptionId, ref bool bSystemDown, ref string userErrorMessage, ref BillingInfo billingInfo)
        {
            RequestMessage request = new RequestMessage ();
            bool bSuccess = false;
            bSystemDown = false;

            // obtain merchantID from the config file
            request.merchantID = MerchantId ();

            // add required fields
            request.merchantReferenceCode = "KEN-UID-" + userId.ToString ();

            request.paySubscriptionRetrieveService = new PaySubscriptionRetrieveService ();
            request.paySubscriptionRetrieveService.run = "true";

            RecurringSubscriptionInfo recurringSubscriptionInfo = new RecurringSubscriptionInfo ();
            recurringSubscriptionInfo.subscriptionID = billingSubscriptionId;
            request.recurringSubscriptionInfo = recurringSubscriptionInfo;

            // Debugging purposes, the request
            m_logger.Debug ("Sending get subscription (profile) to Cybersource: UserId is " + userId.ToString());   

            ReplyMessage reply = new ReplyMessage ();
            bSystemDown = SendRequest (request, ref reply, ref userErrorMessage);

            if (!bSystemDown)
            {
                if ("ACCEPT".Equals (reply.decision) && reply.reasonCode.Equals ("100"))
                {
                    bSuccess = true;

                    // A succesfull get - set the billing info properties so we can pass it back 
                    
                    // billing address info
                    billingInfo.Address1 = reply.paySubscriptionRetrieveReply.street1;
                    billingInfo.Address2 = reply.paySubscriptionRetrieveReply.street2 == null ? "" : reply.paySubscriptionRetrieveReply.street2;
                    billingInfo.City = reply.paySubscriptionRetrieveReply.city;
                    billingInfo.Country = reply.paySubscriptionRetrieveReply.country;
                    billingInfo.FullName = reply.paySubscriptionRetrieveReply.firstName + " " + reply.paySubscriptionRetrieveReply.lastName;
                    billingInfo.PhoneNumber = reply.paySubscriptionRetrieveReply.phoneNumber;
                    billingInfo.PostalCode = reply.paySubscriptionRetrieveReply.postalCode;
                    billingInfo.State = reply.paySubscriptionRetrieveReply.state == null ? "" : reply.paySubscriptionRetrieveReply.state;

                    // card info
                    billingInfo.CardExpirationMonth = reply.paySubscriptionRetrieveReply.cardExpirationMonth;
                    billingInfo.CardExpirationYear = reply.paySubscriptionRetrieveReply.cardExpirationYear;
                    billingInfo.CardNumber = reply.paySubscriptionRetrieveReply.cardAccountNumber;
                    billingInfo.CardType = GetCardNameFromCardType(reply.paySubscriptionRetrieveReply.cardType);
                    billingInfo.CybersourceCardTypeNumber = reply.paySubscriptionRetrieveReply.cardType;
                }
                else
                {
                    string detailErrorMessage = string.Empty;

                    // see comment inside ProcessReply() to see when it might return true.			  
                    ProcessReply (reply, ref userErrorMessage, ref detailErrorMessage);
                }
            }

            return bSuccess;
        }

		/// <summary>
		/// MerchantId
		/// </summary>
		/// <returns></returns>
		public override string MerchantId ()
		{
            return ConfigurationManager.AppSettings["merchantID"];
		}
		
		/// <summary>
		/// CVCodeDescriptions - returns descriptions for a Card Verification Code
		/// </summary>
		/// <returns></returns>
		public string CVCodeDescriptions (string cvCode)
		{
			switch (cvCode)
			{
				case "D":
					return "Transaction determined suspicious by issuing bank.";

				case "I":
					return "Card verification number failed processor's data validation check.";

				case "M":
					return "Card verification number matched.";

				case "N":
					return "Card verification number not matched.";

				case "P":
					return "Card verification number not processed by processor for unspecified reason.";

				case "S":
					return "Card verification number is on the card but was not included in the request.";

				case "U":
					return "Card verification is not supported by the issuing bank.";

				case "X":
					return "Card verification is not supported by the card association.";

				case "1":
					return "Card verification is not supported for this processor or card type.";

				case "2":
					return "Unrecognized result code returned by processor for card verification response.";

				case "3":
					return "No result code returned by processor.";

				default:
					return "No description available";
			}
		}
	
		/// <summary>
		/// AVSCodeDescriptions - returns descriptions for a Address Verification Code
		/// </summary>
		/// <returns></returns>
		public string AVSCodeDescriptions (string avsCode)
		{
			switch (avsCode)
			{
				case "A":								   
					return "Street address matches, but 5-digit and 9-digit postal code do not match.";							  
														   
				case "B":
					return "Street address matches, but postal code not verified. Returned only for non U.S.-issued Visa cards.";

				case "C":
					return "Street address and postal code do not match. Returned only for non U.S.-issued Visa cards.";

				case "D":
				case "M":
					return "Street address and postal code match. Returned only for non U.S.-issued Visa cards.";

				case "E":
					return "AVS data is invalid or AVS is not allowed for this card type.";

				case "F":
					return "Card member�s name does not match, but billing postal code matches. Returned only for the American Express card type.";

				case "G":
					return "Non-U.S. issuing bank does not support AVS.";

				case "H":
					return "Card member�s name does not match. Street address and postal code match. Returned only for the American Express card type.";
					
				case "I":
					return "Address not verified. Returned only for non U.S.-issued Visa cards.";
					
				case "J":
					return "Card member�s name, billing address, and postal code match. Shipping information verified and chargeback protection guaranteed through the Fraud Protection Program.";
					
				case "K":
					return "Card member�s name matches but billing address and billing postal code do not match.";
					
				case "L":
					return "Card member�s name and billing postal code match, but billing address does not match.";
					
				case "N":
					return "Street address and postal code do not match.";
					
				case "O":
					return "Card member�s name and billing address match, but billing postal code does not match.";
					
				case "P":
					return "Postal code matches, but street address not verified.";
					
				case "Q":
					return "Card member�s name, billing address, and postal code match.";
					
				case "R":
					return "System unavailable.";
					
				case "S":
					return "U.S.-issuing bank does not support AVS.";
					
				case "T":
					return "Card member�s name does not match, but street address matches.";
					
				case "U":
					return "Address information unavailable.";
					
				case "V":
					return "Card member�s name, billing address, and billing postal code match.";
					
				case "W":
					return "Street address does not match, but 9-digit postal code matches.";
					
				case "X":
					return "Street address and 9-digit postal code match.";
					
				case "Y":
					return "Street address and 5-digit postal code match.";
					
				case "Z":
					return "Street address does not match, but 5-digit postal code matches.";
					
				case "1":
					return "CyberSource AVS code. AVS is not supported for this processor or card type.";
					
				case "2":
					return "CyberSource AVS code. The processor returned an unrecognized value for the AVS response.";
					
				default:
					return "No description available.";
					
			}
		}
		
		
		private const string cGENERAL_ERROR = "There was a problem communicating with the billing provider. Please try again later.";
		/// <summary>
		/// Logger
		/// </summary>
		private static ILog m_logger = LogManager.GetLogger ("Billing");

	}
}
