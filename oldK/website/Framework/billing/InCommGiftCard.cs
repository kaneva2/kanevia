///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Net;
using System.Xml;
using System.IO;
using System.Data;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for InCommGiftCard.
	/// </summary>
	public class InCommGiftCard
	{
		public InCommGiftCard()
		{
		}

		public static bool RedeemGiftCard (int userId, string pinNumber, string userIpAddress, ref string userErrorMessage)
		{
			DateTime dtCurrentTime = DateTime.Now;

			// Create a new order
			int orderId = StoreUtility.CreateOrder (userId, userIpAddress, (int) Constants.eTRANSACTION_STATUS.CHECKOUT);

			// Set the purchase type
			StoreUtility.SetPurchaseType (orderId, Constants.ePURCHASE_TYPE.GIFT_CARD);

			string formPostData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<TransferredValueTxn xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" >" +
				"<TransferredValueTxnReq>" +
				"<ReqCat>FastCard</ReqCat>" +
				"<ReqAction>Redeem</ReqAction>" +
				"<Date>" + dtCurrentTime.ToString ("yyyymmdd") + "</Date>" +
				"<Time>" + dtCurrentTime.ToString ("hhmmss") + "</Time>" +
				"<PartnerName>Kaneva</PartnerName>" +
				"<CardActionInfo>" +
				"<PIN>" + pinNumber + "</PIN>" +
				"<AcctNum>" + KanevaGlobals.InCommAccountNumber + "</AcctNum>" +
				"<SrcRefNum>" + orderId + "</SrcRefNum>" +
				"</CardActionInfo>" +
				"</TransferredValueTxnReq>" +
				"</TransferredValueTxn>";

			m_logger.Debug ("Request to InComm '" + formPostData + "'");
			bool bSuccess = false;

			try
			{
				WebClient client = new WebClient ();
				client.Headers.Add ("Content-Type","application/x-www-form-urlencoded");
				byte[] postByteArray = System.Text.Encoding.ASCII.GetBytes (formPostData);
				byte[] responseArray = client.UploadData (KanevaGlobals.InCommURL, "POST", postByteArray);
								
				XmlDocument xmlResponse = new XmlDocument(); 

				// Load data   
				xmlResponse.Load (new StringReader (System.Text.Encoding.ASCII.GetString (responseArray)));   
	    
				m_logger.Debug ("The response from InComm '" + xmlResponse.InnerXml + "'");

				// Get result with XPath   
                string strResponseCode = "-1";
                string strRespRefNum = "";
                string strFaceValue = "";
                string strDenom = "";
                string strUPC = "";

                try
                {
                    strResponseCode = xmlResponse.SelectSingleNode("//TransferredValueTxn/TransferredValueTxnResp/RespCode").InnerText;
                    strRespRefNum = xmlResponse.SelectSingleNode("//TransferredValueTxn/TransferredValueTxnResp/RespRefNum").InnerText;
                    strFaceValue = xmlResponse.SelectSingleNode("//TransferredValueTxn/TransferredValueTxnResp/ProductResp/Product/FaceValue").InnerText;
                    strDenom = xmlResponse.SelectSingleNode("//TransferredValueTxn/TransferredValueTxnResp/ProductResp/Product/Denom").InnerText;
                    strUPC = xmlResponse.SelectSingleNode("//TransferredValueTxn/TransferredValueTxnResp/ProductResp/Product/UPC").InnerText;
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error reading InComm Response ", exc);
                }

				switch (strResponseCode)
				{
					case "0":	// Success
					{
						bSuccess = true;

						try
						{
							// We would like to use UPC instead of price, but UPC is unknown at this time
							DataRow drGiftCard = StoreUtility.GetGiftCard (Convert.ToDouble (strFaceValue));

							if (drGiftCard != null)
							{
								// Record the points transaction in the database, marked as checkout
								int transactionId = StoreUtility.PurchasePoints (userId, (int) Constants.eTRANSACTION_STATUS.CHECKOUT, drGiftCard ["description"].ToString (), 0, (int) Constants.ePAYMENT_METHODS.INCOMM_GIFT_CARD, 
									Convert.ToDouble (drGiftCard ["dollar_amount"]), Convert.ToDouble (drGiftCard ["kei_point_amount"]), 0, 0, userIpAddress);
			
								StoreUtility.UpdatePointTransaction (transactionId, (int) Constants.eTRANSACTION_STATUS.VERIFIED, "", strRespRefNum);

								// Record the gift card purchased
								StoreUtility.UpdatePointTransactionGiftCard (transactionId, Convert.ToInt32 (drGiftCard ["gift_card_id"]));
			
								// Set the order id on the point purchase transaction
								StoreUtility.UpdatePointTransaction (transactionId, orderId);
								StoreUtility.UpdateOrderPointTranasactionId (orderId, transactionId);

								// Credit k-point balance, write transaction record
                                (new UserFacade()).AdjustUserBalance(userId, Constants.CURR_KPOINT, Convert.ToDouble(drGiftCard["kei_point_amount"]), Constants.CASH_TT_BOUGHT_CREDITS);

								StoreUtility.UpdateOrderToComplete (orderId, userId);

								// Add any wok items
								AddWokItems (Convert.ToInt32 (drGiftCard ["gift_card_id"]), userId, orderId);
							}
							else
							{
								// Record the points transaction in the database, marked as failed
								int transactionId = StoreUtility.PurchasePoints (userId, (int) Constants.eTRANSACTION_STATUS.PRICE_MISMATCH, "Unknown Amount", 0, (int) Constants.ePAYMENT_METHODS.INCOMM_GIFT_CARD, 
									Convert.ToDouble (strFaceValue), 0, 0, 0, userIpAddress);
			
								StoreUtility.UpdatePointTransaction (transactionId, (int) Constants.eTRANSACTION_STATUS.PRICE_MISMATCH, "Gift Card with that amount was not found. But card was redeemed!", strRespRefNum);
			
								// Set the order id on the point purchase transaction
								StoreUtility.UpdatePointTransaction (transactionId, orderId);
								StoreUtility.UpdateOrderPointTranasactionId (orderId, transactionId);

								StoreUtility.FailOrder (orderId, userId, "Gift Card with that amount was not found. But card was redeemed!");
								bSuccess = false;
								userErrorMessage = "Gift Card with that amount was not found";
							}
						}
						catch (Exception exc)
						{
							m_logger.Error ("Error completing gift card redemption. orderId = " + orderId, exc);
						}

						break;
					}
					case "38":  // Card Redeemed
					{
						StoreUtility.FailOrder (orderId, userId, "This card was already redeemed. strRespRefNum is " + strRespRefNum);
						userErrorMessage = "This card was already redeemed.";
						break;
					}
					case "29":  //System Error
					case "32":  // Invalid Request
					case "43":  // Invalid Card
					case "44":  // Card Expired
					case "46":  // Card Deactivated
					case "50":  // Card Suspended
					case "52":  // Card Pending
					case "53":  // Card Activated
					{
						StoreUtility.FailOrder (orderId, userId, "Failure. Response code is " + strResponseCode + ", strRespRefNum is " + strRespRefNum);
						userErrorMessage = "Card is declined. Please check your pin number.";
						break;
					}
                    case "-1":
                    {
                        StoreUtility.FailOrder(orderId, userId, "Failure. Response code is " + strResponseCode + ", strRespRefNum is " + strRespRefNum);
                        userErrorMessage = "Error communicating to card processor.";
                        break;
                    }
					

				}

			}
			catch (WebException we)
			{
				m_logger.Error ("WebException from InComm", we);
				userErrorMessage = cGENERAL_ERROR;
			}
			catch (Exception exc)
			{
				m_logger.Error ("Exception from InComm", exc);
				userErrorMessage = cGENERAL_ERROR;
			}

			return bSuccess;
		}

		private static void AddWokItems (int giftCardId, int userId, int orderId)
		{
			int globalId = 0;
			int qty = 0;

			try
			{
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(userId);

				// get the free items
                DataTable dtFreeItems = StoreUtility.GetFreeWokItemsGiftCard(giftCardId, user.Gender);

				for (int i=0; i < dtFreeItems.Rows.Count; i++)
				{
                    globalId = Convert.ToInt32(dtFreeItems.Rows[i]["wok_item_id"]);
					qty = Convert.ToInt32(dtFreeItems.Rows[i] ["quantity"]);

					UsersUtility.AddItemToUserInventory (userId, Constants.WOK_INVENTORY_TYPE_PERSONAL, globalId, qty);
				}
			}																					   
			catch (Exception exc)
			{
				m_logger.Error ("Error adding item to users inventory: ", exc);
				m_logger.Error ("orderId=" + orderId.ToString () + " : userId=" + userId.ToString () + " : userId=" + userId.ToString () + " : globalId=" + globalId.ToString () + " : qty=" + qty.ToString ()); 
			}		

		}

		// Samples
		// Inquery Request
		//		<?xml version="1.0" encoding="UTF-8"?>
		//		< TransferredValueTxn xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
		//		<TransferredValueTxnReq>
		//		<ReqCat>FastCard</ReqCat>
		//		<ReqAction>StatInq</ReqAction>
		//		<Date>20040327</Date>
		//		<Time>221522</Time>
		//		<PartnerName>Partner</PartnerName>
		//		<CardActionInfo>
		//		<PIN>1234567</PIN>
		//		<AcctNum>jane22</AcctNum>
		//		<SrcRefNum>678901234</SrcRefNum>
		//		</CardActionInfo>
		//		</TransferredValueTxnReq>
		//		</TransferredValueTxn>

		// Redemption Request
		//		<?xml version="1.0" encoding="UTF-8"?>
		//		< TransferredValueTxn xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
		//		<TransferredValueTxnReq>
		//		<ReqCat>FastCard</ReqCat>
		//		<ReqAction>Redeem</ReqAction>
		//		<Date>20040327</Date>
		//		<Time>221522</Time>
		//		<PartnerName>Partner</PartnerName>
		//		<CardActionInfo>
		//		<PIN>1234567</PIN>
		//		<AcctNum>jane22</AcctNum>
		//		<SrcRefNum>678901234</SrcRefNum>
		//		</CardActionInfo>
		//		</TransferredValueTxnReq>
		//		</TransferredValueTxn>
		//	Response Code	Response Message
		//	0	Success
		//	29	System Error
		//	32	Invalid Request
		//	38	Card Redeemed
		//	43	Invalid Card
		//	44	Card Expired
		//	46	Card Deactivated
		//	50	Card Suspended
		//	52	Card Pending
		//	53	Card Activated


		//		Sample Reversal Request
		//		<?xml version="1.0" encoding="UTF-8"?>
		//		< TransferredValueTxn xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
		//		< TransferredValueTxnReq>
		//		<ReqCat>FastCard</ReqCat>
		//		<ReqAction>Reverse</ReqAction>
		//		<Date>20040327</Date>
		//		<Time>221522</Time>
		//		<PartnerName>Partner</ PartnerName >
		//		<CardActionInfo>
		//		<PIN>1234567890</ PIN>
		//		<AcctNum>Jane11234</AcctNum>
		//		<SrcRefNum>8907654</SrcRefNum>
		//		</CardActionInfo>
		//		</TransferredValueTxnReq>
		//		</ TransferredValueTxn >

		//		Sample History Request:
		//		<?xml version=\"1.0\" encoding=\"UTF-8\"?>
		//		<TransferredValueTxn xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
		//		<TransferredValueTxnReq>
		//		<ReqCat>FastCard</ReqCat>
		//		<ReqAction>History</ReqAction>
		//		<Date>20040416</Date>
		//		<Time>192120</Time>
		//		<PartnerName>Partner</PartnerName>
		//		<HistoryReq>
		//		<StartDate>2005-04-21</StartDate>
		//		<EndDate>2005-04-29</EndDate>
		//		<PIN>2017095062545</PIN>
		//		</HistoryReq>
		//		</TransferredValueTxnReq>
		//		</TransferredValueTxn>

		private const string cGENERAL_ERROR = "There was a problem communicating with the billing provider. Please try again later.";

		/// <summary>
		/// Logger
		/// </summary>
		private static ILog m_logger = LogManager.GetLogger ("Billing");
	}
}
