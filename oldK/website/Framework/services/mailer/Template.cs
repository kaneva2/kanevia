///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.Net.Mail;

namespace KlausEnt.KEP.Kaneva.Mailer
{
  [Serializable]
  public class Template
  {
    private int _TemplateId = 0;
    private string _Subject = "";
    private string _Body = "";
    private string _Header = "";
    private string _Footer = "";
    private string _EmailFrom = "";
    private string _EmailFromDisplayName = "";
    private string _UserMessage = "";


    public Template() { }

    public Template(int templateId, string subject, string body, string header, string footer, string emailFrom, 
      string emailFromDisplayName, string userMessage) 
    {
      this._TemplateId = templateId;
      this._Subject = subject;
      this._Body = body;
      this._Header = header;
      this._Footer = footer;
      this._EmailFrom = emailFrom;
      this._EmailFromDisplayName = emailFromDisplayName;
      this._UserMessage = userMessage;
    
    }


    public int TemplateId
    {
      get { return _TemplateId; }
      set { _TemplateId = value; }
    }

    public string Subject
    {
      get { return _Subject; }
      set { _Subject = value; }
    }

    public string Body
    {
      get { return _Body; }
      set { _Body = value; }
    }

    public string Header
    {
      get { return _Header; }
      set { _Header = value; }
    }

    public string Footer
    {
      get { return _Footer; }
      set { _Footer = value; }
    }

    public string EmailFrom
    {
      get { return _EmailFrom; }
      set { _EmailFrom = value; }
    }

    public string EmailFromDisplayName
    {
      get { return _EmailFromDisplayName; }
      set { _EmailFromDisplayName = value;}
    }

    public string UserMessage
    {
      get { return _UserMessage; }
      set { _UserMessage = value; }
    }

    public MailAddress MaFrom
    {
      get { return new MailAddress(_EmailFrom, _EmailFromDisplayName); }
    }

  }
}
