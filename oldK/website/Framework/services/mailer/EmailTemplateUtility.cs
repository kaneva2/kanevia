///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Text;
using System.Web.Security;
using System.Web;
using System.Web.Handlers;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Collections.Generic;


using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.Mailer
{
	public class EmailTemplateUtility
	{
		// Get Multiple Templates

		public static DataTable GetAllTemplates(int enabled)
		{
			string sqlSelect = "SELECT etm.email_template_id, ety.email_type_id, ety.type_name, etm.template_name, etm.template_version, etm.template_subversion,  " +
			" etm.reply_address, etm.reply_address_display_name, etm.template_text,	etm.template_html, etm.email_subject, etm.language_code, etm.enabled,  " +
			" etm.footer_template_id, etm.header_template_id, etm.rotation_weight, etm.date_created, etm.description, etm.comments  " +
			" FROM email_template etm  " +
			" LEFT JOIN email_type ety ON etm.email_type_id = ety.email_type_id " +
			" WHERE etm.enabled = @enabled AND etm.deleted = 0 " +
			" ORDER BY etm.email_type_id DESC, etm.template_version DESC;";
			Hashtable parameters = new Hashtable();
			parameters.Add("@enabled", enabled);

			DataTable dt = KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataTable(sqlSelect, parameters);

			DataColumn column;

			column = new DataColumn();
			column.DataType = System.Type.GetType("System.Int32");
			column.ColumnName = "send_count";
			dt.Columns.Add(column);

			return dt;

		}

		public static DataTable GetTemplatesByTypeId(int emailTypeId, int enabled)
		{
			string sqlSelect = "SELECT etm.email_template_id, ety.email_type_id, ety.type_name, etm.template_name, etm.template_version, etm.template_subversion,  " +
              " etm.reply_address, etm.reply_address_display_name, etm.template_text,	etm.template_html, etm.email_subject, etm.language_code, etm.enabled,  " +
			  " etm.footer_template_id, etm.header_template_id, etm.rotation_weight, etm.date_created, etm.description, etm.comments  " +
			  " FROM email_template etm  " +
			  " LEFT JOIN email_type ety ON etm.email_type_id = ety.email_type_id " +
			  " WHERE etm.email_type_id=@email_type_id AND etm.enabled = @enabled AND etm.deleted = 0 " +
			  " ORDER BY etm.email_type_id DESC, etm.template_version DESC";

			Hashtable parameters = new Hashtable();
			parameters.Add("@email_type_id", emailTypeId);
			parameters.Add("@enabled", enabled);

			return KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataTable(sqlSelect, parameters);
		}

		public static DataTable GetTemplatesByTypeName(string typeName, int enabled)
		{
			string sqlSelect = "SELECT * FROM email_template et JOIN email_type ety ON et.email_type_id = ety.email_type_id " +
			" WHERE ety.type_name=@type_name AND et.enabled = @enabled AND et.deleted = 0 ORDER BY et.email_type_id DESC, template_version DESC";

			Hashtable parameters = new Hashtable();
			parameters.Add("@type_name", typeName);
			parameters.Add("@enabled", enabled);

			return KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataTable(sqlSelect, parameters);
		}

		public static DataTable GetTemplates() // Get all templates 
		{
			string sqlSelect = "SELECT * FROM (SELECT * FROM email_template ORDER BY template_version DESC) t1 GROUP BY template_version ORDER BY template_version;";

			return KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataTable(sqlSelect);
		}

		// Get Single Template

		public static DataRow GetTemplateByName(string tmplName)
		{
			string sqlGetTemplate = "SELECT *	" +
			  " FROM email_template " +
			  " WHERE template_name = @tmplName AND enabled = 1 AND deleted = 0 ORDER BY template_version DESC LIMIT 1 ";

			Hashtable parameters = new Hashtable();
			parameters.Add("@tmplName", tmplName);

			DataRow drTemplate = KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataRow(sqlGetTemplate, parameters, false);

			return drTemplate;
		}

		public static int GetNextTemplateVersion(int typeId)
		{
			string sqlString = "SELECT MAX(template_version)FROM email_template WHERE email_type_id = @typeId";
			Hashtable parameters = new Hashtable();
			parameters.Add("@typeId", typeId);

			return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteScalar(sqlString, parameters) + 1;
		}

		public static int GetTemplateNextId(int typeId)
		{
			//Determine which template to send for this type

			DataTable dtTemplates = EmailTemplateUtility.GetTemplatesByTypeId(typeId, 1);

			Random random = new Random();

			int num = random.Next(1, dtTemplates.Rows.Count + 1);

			int templateIdToUse = 0;

			int i = 0;
			for (i = 0; i < dtTemplates.Rows.Count; i++)
			{
				if (i + 1 == num)
				{
					templateIdToUse = Convert.ToInt16(dtTemplates.Rows[i]["email_template_id"]);
				}
			}

			return templateIdToUse;
		}

		public static int GetTemplateNextRandomId(int typeId)
		{
			//	
			//
			//Logger.LogEmailServiceEvent("Info", "Looking for random for type:" + typeId);
			
			int templateToUse = 0;

			try
			{
				int weightTotal = 0;
				
				MailFacade mailFacade = new MailFacade();						
				IList<EmailTemplate> list = mailFacade.GetTemplatesByTypeId(typeId, 1);
				foreach (EmailTemplate i in list)
				{
					weightTotal += i.RotationWeight;
					
					//
					//
					//Logger.LogEmailServiceEvent("Info", "weight total =" + weightTotal);
				}				

				Random random = new Random();
				int num = random.Next(0, weightTotal);
				int thisWeight = 0;

				foreach (EmailTemplate i in list)
				{
					thisWeight += i.RotationWeight;
					if (thisWeight <= num)
					{
						// go fish!
						
						//
						//
						//Logger.LogEmailServiceEvent("Info", "go fish");
					}
					else
					{
						templateToUse = i.TemplateId;
						break;
					}
				}

			}
			catch (Exception exc)
			{


				Logger.LogEmailServiceEvent("Error", "Failed to get random template.", exc);
				//Logger.LogEmailServiceEvent("Error", "Failed to get random template.");
				return -1;
			}
			return templateToUse;
		}

		public static DataRow GetTemplateById(int templateId)
		{
			string sqlGetTemplate = "SELECT tp.email_template_id, tp.email_type_id, tp.template_name, tp.template_version, tp.template_subversion, " +
              " tp.reply_address, tp.reply_address_display_name, tp.template_text, tp.template_html, tp.email_subject, tp.language_code, tp.enabled, tp.footer_template_id,  " +
			  " tp.header_template_id, tp.rotation_weight, tp.date_created, tp.description, tp.comments, ty.tier,	" +
			  " tp2.template_html AS header_html, tp3.template_html AS footer_html" +
			  " FROM email_template tp LEFT JOIN email_type ty ON  tp.email_type_id = ty.email_type_id " +
			  " LEFT JOIN email_template tp2 ON tp.header_template_id = tp2.email_template_id " +
			  " LEFT JOIN email_template tp3 ON tp.footer_template_id = tp3.email_template_id " +
			  " WHERE tp.email_template_id = @templateId  LIMIT 1 ";

			Hashtable parameters = new Hashtable();
			parameters.Add("@templateId", templateId);


			DataRow drTemplate = KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataRow(sqlGetTemplate, parameters, false);

			return drTemplate;
		}

		private static DataRow GetTemplateDull(int typeId)
		{
            string sqlGetTemplate = "SELECT email_template_id, email_type_id, template_name, template_version, template_subversion, reply_address, reply_address_display_name, template_text, template_html, " +
			  " email_subject, language_code, enabled, footer_template_id, header_template_id, rotation_weight, date_created, description, comments	" +
			  " FROM email_template " +
			  " WHERE email_type_id = @typeId AND enabled = 1 AND deleted = 0 ORDER BY template_version DESC LIMIT 1 ";

			Hashtable parameters = new Hashtable();
			parameters.Add("@typeId", typeId);

			DataRow drTemplate = KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataRow(sqlGetTemplate, parameters, false);

			return drTemplate;
		}

		// Modify Templates
        public static int InsertEmailTemplate(int email_type_id, string template_name, int template_version, int template_subversion,
		  string reply_address, string reply_address_display_name, string template_text, string template_html, string email_subject, string language_code,
		  int enabled, int footer_template_id, int header_template_id, int rotation_weight, string description, string comments)
		{

			string sqlInsertEmailTemplate = " INSERT INTO email_template 	(email_type_id,	template_name, template_version, template_subversion, " +
              " reply_address, reply_address_display_name, template_text,  template_html, email_subject, language_code, enabled, footer_template_id, header_template_id, " +
			  " rotation_weight, date_created, description, comments) " +
              " VALUES (@email_type_id, @template_name, @template_version, @template_subversion, @reply_address, @reply_address_display_name, @template_text, @template_html, " +
			  " @email_subject, @language_code, @enabled, @footer_template_id, @header_template_id, @rotation_weight, " +
			  " @date_created, @description, @comments)";

			Hashtable parameters = new Hashtable();
			parameters.Add("@email_type_id", email_type_id);
			parameters.Add("@template_name", template_name);
			parameters.Add("@template_version", template_version);
			parameters.Add("@template_subversion", template_subversion);
			parameters.Add("@reply_address", reply_address);
            parameters.Add("@reply_address_display_name", reply_address_display_name);
			parameters.Add("@template_text", template_text);
			parameters.Add("@template_html", template_html);
			parameters.Add("@email_subject", email_subject);
			parameters.Add("@language_code", language_code);
			parameters.Add("@enabled", enabled);
			parameters.Add("@footer_template_id", footer_template_id);
			parameters.Add("@header_template_id", header_template_id);
			parameters.Add("@rotation_weight", rotation_weight);
			parameters.Add("@description", description);
			parameters.Add("@comments", comments);
			parameters.Add("@date_created", DateTime.Now);

			return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlInsertEmailTemplate, parameters);
		}

        public static int UpdateEmailTemplate(int email_template_id, int email_type_id, string template_name, int template_version, int template_subversion,
		  string reply_address, string reply_address_display_name, string template_text, string template_html, string email_subject, string language_code,
		  int enabled, int footer_template_id, int header_template_id, int rotation_weight, string description, string comments)
		{

			string sqlUpdateEmailTemplate = " UPDATE email_template SET	email_type_id = @email_type_id,	template_name= @template_name, " +
			  " template_version = @template_version, template_subversion = @template_subversion, " +
              " reply_address = @reply_address, reply_address_display_name = @reply_address_display_name, template_text = @template_text,  template_html = @template_html, " +
			  " email_subject = @email_subject, language_code = @language_code, enabled = @enabled, footer_template_id = @footer_template_id, " +
			  " header_template_id = @header_template_id, rotation_weight = @rotation_weight, description = @description, " +
			  " comments = @comments WHERE email_template_id = @email_template_id";

			Hashtable parameters = new Hashtable();
			parameters.Add("@email_template_id", email_template_id);
			parameters.Add("@email_type_id", email_type_id);
			parameters.Add("@template_name", template_name);
			parameters.Add("@template_version", template_version);
			parameters.Add("@template_subversion", template_subversion);
			parameters.Add("@reply_address", reply_address);
            parameters.Add("@reply_address_display_name", reply_address_display_name);
			parameters.Add("@template_text", template_text);
			parameters.Add("@template_html", template_html);
			parameters.Add("@email_subject", email_subject);
			parameters.Add("@language_code", language_code);
			parameters.Add("@enabled", enabled);
			parameters.Add("@footer_template_id", footer_template_id);
			parameters.Add("@header_template_id", header_template_id);
			parameters.Add("@rotation_weight", rotation_weight);
			parameters.Add("@description", description);
			parameters.Add("@comments", comments);


			return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlUpdateEmailTemplate, parameters);
		}

		public static void DeleteTemplate(int tmplId)
		{
			//delete from email_type
			//string sqlString = "DELETE FROM email_template WHERE email_template_id = @tmplId";
			string sqlString = "UPDATE email_template SET deleted = 1 WHERE email_template_id = @tmplId ";

			Hashtable parameters = new Hashtable();
			parameters.Add("@tmplId", tmplId);

			KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlString, parameters);
		}




	}
}
