///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Text;
using System.Web.Security;
using System.Web;
using System.Web.Handlers;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;


using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;


namespace KlausEnt.KEP.Kaneva.Mailer
{
  public class EmailTypeUtility
  {

    public static DataTable GetEmailTypes()
    {
	 string sqlSelect = "SELECT * FROM email_type WHERE deleted = 0";

      return KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataTable(sqlSelect);
    }

    public static DataRow GetTypeByName(string typeName)
    {
      string sqlGetType = "SELECT * FROM email_type WHERE type_name = @typeName AND enabled = 1 AND deleted = 0";

      Hashtable parameters = new Hashtable();
      parameters.Add("@typeName", typeName);

      DataRow drType = KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataRow(sqlGetType, parameters, false);

      return drType;
    }

    public static int GetTypeIdByName(string typeName)
    {
      int typeId = -1;

      DataRow drType = GetTypeByName(typeName);
      try
      {
        typeId = Convert.ToInt32(drType["email_type_id"].ToString());
      }
      catch
      {

      }

      return typeId;
    }

    public static int InsertEmailType(string name, string description, int enabled, int tier, int intervalQue, int intervalChew, string query)
    {

      string sqlInsertEmailType = " INSERT INTO email_type (type_name, enabled, date_created, description, tier, list_query, interval_que, interval_chew) " +
               " VALUES (@typeName, @enabled, @dateCreated, @description, @tier, @list_query, @interval_que, @interval_chew)";

      Hashtable parameters = new Hashtable();
      parameters.Add("@typeName", name);
      parameters.Add("@enabled", enabled);
      parameters.Add("@description", description);
      parameters.Add("@tier", tier);
      parameters.Add("@dateCreated", DateTime.Now);
      parameters.Add("@list_query", query);
      parameters.Add("@interval_que", intervalQue);
      parameters.Add("@interval_chew", intervalChew);
      
      return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlInsertEmailType, parameters);
    }

    public static int UpdateEmailType(int typeId ,string name, string description, int enabled, int tier, int intervalQue, int intervalChew, string query)
    {

      string sqlUpdateEmailType = " UPDATE email_type SET type_name=@typeName, enabled=@enabled, " +
        " description=@description, tier=@tier, list_query=@list_query, interval_que=@inverval_que, interval_chew=@interval_chew  " +
        " WHERE email_type_id = @typeId ";

      Hashtable parameters = new Hashtable();
      parameters.Add("@typeId", typeId);
      parameters.Add("@typeName", name);
      parameters.Add("@enabled", enabled);
      parameters.Add("@description", description);
      parameters.Add("@tier", tier);
      parameters.Add("@list_query", query);
      parameters.Add("@inverval_que", intervalQue);
      parameters.Add("@interval_chew", intervalChew);

      return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlUpdateEmailType, parameters);
    }

    public static DataRow GetEmailType(int typeId)
    {
      string sqlGetEmailType = "SELECT * FROM email_type WHERE email_type_id = @typeId ";
      
      Hashtable parameters = new Hashtable();
      parameters.Add("@typeId", typeId);
      return KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataRow(sqlGetEmailType, parameters, false);
    }

    public static void DeleteEmailType(int typeId)
    {
      //delete from email_type
      //string sqlString = "DELETE FROM email_type WHERE email_type_id = @typeId ";
	 string sqlString = "UPDATE email_type SET deleted = 1 WHERE email_type_id = @typeId ";

      Hashtable parameters = new Hashtable();
      parameters.Add("@typeId", typeId);

      KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlString, parameters);
    }

  }
}
