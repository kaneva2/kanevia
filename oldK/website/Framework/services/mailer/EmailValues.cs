///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Text;
using System.Web.Security;
using System.Web;
using System.Web.Handlers;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;


using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;


namespace KlausEnt.KEP.Kaneva.Mailer
{
	public class EmailValues
	{

		public static DataTable GetOverloadFields(int overloadId)
		{
			string sqlSelect = "SELECT * FROM email_overload_fields WHERE overload_id = @overloadId";
			
			Hashtable parameters = new Hashtable();
			parameters.Add("@overloadId", overloadId);

			return KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataTable(sqlSelect, parameters);
		}
		
	}
}
