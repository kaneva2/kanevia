///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Collections;
using System.Data;
using System.Text;
using System.Threading;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

// Import log4net classes.
using log4net;

namespace KlausEnt.KEP.Kaneva.Mailer
{
  public class Chewer
  {

    public Chewer()
    {
    }

    public static int Chew()
    {
      int ChewTotal = 0;
      try
      {
        DataTable dtTypes = EmailTypeUtility.GetEmailTypes();        
        int i = 0;
        for (i = 0; i < dtTypes.Rows.Count; i++)
        {

          if (Convert.ToInt16(dtTypes.Rows[i]["enabled"]) == 1 && Convert.ToInt16(dtTypes.Rows[i]["interval_chew"]) > 0) // For each enabled type
          {
            string typeName = dtTypes.Rows[i]["type_name"].ToString();

            int intInvRem = Mailer.InviteReminderChew.InviteReminderChewByType(1000, typeName);
              Logger.LogEmailServiceEvent("Info", typeName + " sent: " + intInvRem);
              ChewTotal += intInvRem;
          }
        }       
      }
      catch(Exception exc)
      {
        Logger.LogEmailServiceEvent("Error", "Error processing email types doing chew.", exc);
        return -1;
      }
      
      return ChewTotal;
    }

    private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
  }
}
