///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Text;
using System.Web.Security;
using System.Web;
using System.Web.Handlers;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Net.Mail;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.Mailer
{
  public class TemplateSherpa
  {
    
	public static int DressAndSendTemplate(DataTable dtMailingList)
	{
		int intSent = 0;

		for (int i = 0; i < dtMailingList.Rows.Count; i++)
		{
			if (intSent < 0)
			{
				// Error condition  
			    return intSent;
			}
			intSent += DressAndSendTemplate(dtMailingList.Rows[i]);
		}

		return intSent;
    }

      public static int DressAndSendTemplate(DataRow drMailing)
      {
          int emailTypeId = Convert.ToInt32(drMailing["email_type_id"]);
          int emailTemplateId = Convert.ToInt32(drMailing["email_template_id"]);
          int inviteId = Convert.ToInt32(drMailing["invite_id"]);
          string emailTo = drMailing["email_to"].ToString();
          int emailQueId = Convert.ToInt32(drMailing["email_queue_id"]);
		  int overloadId = Convert.ToInt32(drMailing["overload_id"]);


          int fromUserId = 0;
          int toUserId = 0;
          string keyValue = "";

          if (inviteId > 0)
          {
            // Get Invite info 
            DataRow drInvite = UsersUtility.GetInvite(inviteId);

            if (drInvite == null)
            {
              return 0;
            }
            else if (Convert.ToInt32(drInvite["user_id"]) == 0)
            {
              return 0;
            }
            else
            {
              fromUserId = Convert.ToInt32(drInvite["user_id"]);
              keyValue = drInvite["key_value"].ToString();
            }            
          }

          return DressAndSendTemplate(emailTypeId, emailTemplateId, emailTo, inviteId, fromUserId, toUserId, keyValue, emailQueId, "", "", 0, overloadId);
      }

    /// <summary>
    /// DressAndSendTemplate
    /// </summary>
	/// 

	  public static int DressAndSendTemplate(int emailTypeId, int emailTemplateId, string emailTo, int inviteId, int fromUserId,
		  int toUserId, string keyCode, int emailQueId, string userMessage, string addToSubject, int intExtra1)
	  {
		  return DressAndSendTemplate(emailTypeId, emailTemplateId, emailTo, inviteId, fromUserId, toUserId, keyCode, emailQueId, userMessage, addToSubject, intExtra1, 0);
	  }

      public static int DressAndSendTemplate(int emailTypeId, int emailTemplateId, string emailTo, int inviteId, int fromUserId,
        int toUserId, string keyCode, int emailQueId, string userMessage, string addToSubject, int intExtra1, int overloadId)
      {
          return DressAndSendTemplate(emailTypeId, emailTemplateId, emailTo, inviteId, fromUserId, toUserId, keyCode, emailQueId, userMessage, addToSubject, intExtra1, overloadId, string.Empty);
      }

    public static int DressAndSendTemplate(int emailTypeId, int emailTemplateId, string emailTo, int inviteId, int fromUserId,
        int toUserId, string keyCode, int emailQueId, string userMessage, string addToSubject, int intExtra1, int overloadId, string trackingMetric)
    {
      int intSent = 0;

      try
      {
        int send_status = 0;
        
        Template template = new Template();

        DataRow drTemplate = null;

        //Pick template. Priority is Que then by type. This is so a specific template can be queued without being overridden.
        
        if (emailTemplateId == 0) // if we aren't passed in a specific template
        {            
          try
          {
            drTemplate = EmailTemplateUtility.GetTemplateById(EmailTemplateUtility.GetTemplateNextRandomId(emailTypeId));
            template.TemplateId = Convert.ToInt32(drTemplate["email_template_id"]);
          }
          catch
          {
            Logger.LogEmailServiceEvent("info","Failed to get email template id. Type: " + emailTypeId);
			return -1;
          }
        }
        else
        {
          drTemplate = EmailTemplateUtility.GetTemplateById(emailTemplateId);
          template.TemplateId = Convert.ToInt32(drTemplate["email_template_id"]);
        }
        
        template.EmailFrom = drTemplate["reply_address"].ToString();
        // DisplayName may be null, so need to use Convert.ToString() instead of string.ToString()
        template.EmailFromDisplayName = Convert.ToString(drTemplate["reply_address_display_name"]);
        template.Subject = drTemplate["email_subject"].ToString();
        template.Body = drTemplate["template_html"].ToString();
        if (Convert.ToInt16(drTemplate["header_template_id"]) != 0)
        {
          template.Header = drTemplate["header_html"].ToString();
        }
        if (Convert.ToInt16(drTemplate["footer_template_id"]) != 0)
        {
          template.Footer = drTemplate["footer_html"].ToString();
        }
        template.UserMessage = userMessage;
       
        int emailId = Mailer.Logger.LogEmailSend(inviteId, emailTypeId, template.TemplateId, emailTo, template.EmailFrom); // we need the email_id to stype tracking in the template. 

        if (emailQueId != 0)
        {
          int intDeQueued = Mailer.Queuer.UnQueueEmail(emailQueId); //remove from que  
        }

        // We can rebuild it. 
        template.Subject = ReplaceBot(template, template.Subject, inviteId, fromUserId, toUserId, keyCode, emailTo, emailId, addToSubject, intExtra1, overloadId, trackingMetric);
        template.Body = ReplaceBot(template, template.Body, inviteId, fromUserId, toUserId, keyCode, emailTo, emailId, addToSubject, intExtra1, overloadId, trackingMetric);

        if (inviteId > 0)
        {       
          UserFacade userFacade = new UserFacade();
          User user = userFacade.GetUser(fromUserId);
          template.EmailFromDisplayName = user.FirstName + " " + user.LastName;
        }

        //send the email 
        bool ishtml = false;
        bool cc = false;
        int tier = Convert.ToInt32(drTemplate["tier"]);
        
        try
        {
          MailAddress maTo = new MailAddress(emailTo);
          send_status = MailUtility.SendEmail(template.MaFrom, maTo, template.Subject, template.Body, ishtml, cc, tier);
        }
        catch
        {
          send_status = -1;
        }

        Logger.LogEmailSendStatus(emailId, send_status);

        intSent++;
      }
      catch (Exception exc)
      {
        Logger.LogEmailServiceEvent("Error", "Error sending template.", exc);
      }

      return intSent; 
    }

    public static string ReplaceBot(Template template, string strTemplatePart, int inviteId, int fromUserId, int toUserId, string keyCode,
        string emailTo, int emailId, string addToSubject, int intExtra1, int overloadId, string trackingMetric)
    {              
      StringBuilder sb = new StringBuilder();      
      sb.Append(strTemplatePart);

      // Header and Footer 
      try
      {
        sb.Replace("#header#", template.Header);
        sb.Replace("#footer#", template.Footer);
      }
      catch(Exception exc)
      {
        throw new Exception(" Fail while replacing header and footer.", exc);       
      }

      // Recipient email
      sb.Replace("#toemail#", emailTo);

      // Subject
      sb.Replace("#addToSubject#", addToSubject);

      // Site names
      sb.Replace("#sitename#", KanevaGlobals.SiteName);
      sb.Replace("#shoppingsitename#", KanevaGlobals.ShoppingSiteName);

      // Message Id, this is used when the template is a private message 
      sb.Replace("#messageId#", template.UserMessage);

      // Request Type Sent Id for tracking
      sb.Replace("#" + KlausEnt.KEP.Kaneva.Constants.cREQUEST_TRACKING_URL_PARAM + "#", trackingMetric);
      
      // For Communities we use the addToSubject field for the community name, and the extra int for the id.
      bool isCommunityEmail = false;
      if (sb.ToString().Contains("#communityName#"))
      {
        isCommunityEmail = true;
        sb.Replace("#communityName#", addToSubject);
        sb.Replace("#communityId#", intExtra1.ToString());
        sb.Replace("#communityImage#", CommunityUtility.GetBroadcastChannelImageURL (CommunityUtility.GetCommunity (intExtra1)["thumbnail_medium_path"].ToString (), "me"));
      }

      // fromUserId 
      //
      // Get information about the user who sent the email (or if the email is about a user this is gets the info)
      //
      try
      {
        if (fromUserId > 0)
        {
          UserFacade userFacade = new UserFacade();
          User user = userFacade.GetUser(fromUserId);

          sb.Replace("#userfullname#", user.FirstName + " " + user.LastName);
          sb.Replace("#username#", user.Username);
          sb.Replace("#displayname#", user.DisplayName);
          sb.Replace("#displaythenusername#", (user.DisplayName + (string.IsNullOrEmpty(user.DisplayName) ? user.Username : " (" + user.Username + ")")));
          sb.Replace("#userthendisplayname#", (user.Username + (string.IsNullOrEmpty(user.DisplayName) ? "" : " (" + user.DisplayName + ")")));
          sb.Replace("#userlink#", KanevaGlobals.GetPersonalChannelUrl(user.NameNoSpaces));          
          sb.Replace("#userfirstname#", user.FirstName);
          sb.Replace("#additionalmessage#", template.UserMessage);
          sb.Replace("#userimage#", UsersUtility.GetProfileImageURL(user.ThumbnailSmallPath, "sm", user.Gender));
          sb.Replace("#meet3dlink#", KanevaGlobals.SiteName + "/kgp/playwok.aspx?goto=P" + user.UserId.ToString() + "&ILC=MM3D&link=mmnowin");

          // do the following if this is an invite.          
          if (inviteId > 0)
          {
            // is this an invite for a community?
            if (isCommunityEmail)
            {
              sb.Replace("#joinurl#", KanevaGlobals.SiteName + KanevaGlobals.JoinLocation.Substring(1) + "?i=" + inviteId + "&k=" + keyCode + "&cInv=" + intExtra1);
            }
            else
            {
            // Standard join url 
              sb.Replace("#joinurl#", KanevaGlobals.SiteName + KanevaGlobals.JoinLocation.Substring(1) + "?i=" + inviteId + "&k=" + keyCode);
            }
           
            sb.Replace("#declineurl#", KanevaGlobals.SiteName + "/declineInvite.aspx?i=" + inviteId + "&k=" + keyCode);
          }


        }
      }
      catch 
      {
        throw new Exception(" Fail when retrieving user info. Invite Id:" + inviteId + " User: " + fromUserId);
      }

      // toUserId 
      //
      // Get information about the user who sent the email (or if the email is about a user this is gets the info)
      //
      try
      {
          if (toUserId > 0)
          {
              UserFacade userFacade = new UserFacade ();
              User user = userFacade.GetUser (toUserId);
              sb.Replace ("#tousername#", user.Username);
              sb.Replace ("#todisplayname#", user.DisplayName);
              sb.Replace ("#todisplaythenusername#", (user.DisplayName + (string.IsNullOrEmpty(user.DisplayName) ? user.Username : " (" + user.Username + ")")));
              sb.Replace ("#touserthendisplayname#", (user.Username + (string.IsNullOrEmpty(user.DisplayName) ? "" : " (" + user.DisplayName + ")")));
              switch (addToSubject.ToLower ())
              {
                  case "blast":
                      sb.Replace ("#tolink#", KanevaGlobals.SiteName);
                      break;

                  case "profile":
                      sb.Replace ("#tolink#", KanevaGlobals.GetPersonalChannelUrl (user.NameNoSpaces));
                      break;

                  default:
                      sb.Replace ("#tolink#", KanevaGlobals.GetPersonalChannelUrl (user.NameNoSpaces));
                      break;
              }
          }
      }
      catch
      {
          throw new Exception (" Fail when retrieving user info. Invite Id:" + inviteId + " User: " + fromUserId);
      }    
		
      // get overload values

	  if (overloadId > 0)
	  {
		  DataTable dtOverloadFields = EmailValues.GetOverloadFields(overloadId);
		  foreach (DataRow drOverloadFields in dtOverloadFields.Rows)
		  {
			  sb.Replace(drOverloadFields["key"].ToString(),drOverloadFields["value"].ToString());
		  }
	  }

      // Tracking links
      //
      try
      {
        
        string inviteParam = "";
        if (inviteId > 0)
          inviteParam = "&invite=" + inviteId;        

        sb.Replace("#trackimgurl#", KanevaGlobals.SiteName + "/displayimagetracking.aspx?u=0&m=" + emailId + inviteParam);
        sb.Replace("#unsubcribeurl#", KanevaGlobals.SiteName + "/mykaneva/unsubscribe.aspx?email=" + emailTo );        
      }
      catch
      {
        throw new Exception(" Failing while replacing basic strings and links." );
      }

      // Turn urls into trackable links
      //
      try
      {
        strTemplatePart = sb.ToString();
        //Replace urls with redirect  
        string pattern = "(?<=href=\"http://)(.*)(?=(\"|'))";
        strTemplatePart = Regex.Replace(strTemplatePart, pattern, new MatchEvaluator(EncLink));
        strTemplatePart = strTemplatePart.Replace("#emailId#", emailId.ToString());
      }
      catch (Exception exc)
      {
        Logger.LogEmailServiceEvent("Error", "Fail while replacing tracking links." + exc.Message);
      }

      // Finish
      return strTemplatePart;
    }


    private static string EncLink(Match m)
    {
      string u = System.Web.HttpUtility.UrlEncode( KanevaGlobals.Encrypt(m.ToString()) );      
      return KanevaGlobals.SiteName + "/redir.aspx?u=" + u + "&e=#emailId#";
    }

    /// <summary>
    /// Get a table of active templates
    /// </summary>
    public static DataTable dtActiveTemplates = Mailer.EmailTemplateUtility.GetAllTemplates(1);

    private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
  }
}
