///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Text;
using System.Web.Security;
using System.Web;
using System.Web.Handlers;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;

using KlausEnt.KEP.Kaneva.framework.widgets;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.Mailer
{
  public class InviteReminderChew
  {
    
    public static int InviteReminderChewByType(int limit, string typeName)
    {
      // Process a template for everyone queued for the email specified.
      return TemplateSherpa.DressAndSendTemplate(GetQueueInviteReminder(limit, typeName));     
    }     
    /// <summary>
    /// Get Invite Reminders 10 days
    /// </summary>
    private static DataTable GetQueueInviteReminder(int limit, string typeName)
    {
      string sqlSelect = " SELECT email_queue_id, email_type_id, email_template_id, email_to, invite_id, overload_id " +
        " FROM email_queue WHERE email_type_id = @typeId " +
        " LIMIT @limit";

      Hashtable parameters = new Hashtable();
      parameters.Add("@typeId", EmailTypeUtility.GetTypeIdByName(typeName));
      parameters.Add("@limit", limit);

      return KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataTable(sqlSelect, parameters);
    }

    private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

  }
}
