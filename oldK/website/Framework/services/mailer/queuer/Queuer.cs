///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Collections;
using System.Data;
using System.Text;
using System.Threading;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

// Import log4net classes.
using log4net;

namespace KlausEnt.KEP.Kaneva.Mailer
{
  public class Queuer
  {

    public Queuer()
    {
    }

    public static int Queue()
    {
      m_logger.Debug("Filling the queue");

      
      // fill lists   
      int queCount = 0;
      int queTotal = 0;

      try
      {
        DataTable dtTypes = EmailTypeUtility.GetEmailTypes();

        int i = 0;
        for (i = 0; i < dtTypes.Rows.Count; i++)
        {

          if (Convert.ToInt16(dtTypes.Rows[i]["enabled"]) == 1 && Convert.ToInt16(dtTypes.Rows[i]["interval_que"]) > 0) // For each enabled type
          {
            string typeName = dtTypes.Rows[i]["type_name"].ToString();
            queCount = queList(typeName);
            Logger.LogEmailServiceEvent("Info", typeName + " Queued: " + queCount);
            m_logger.Info(typeName + "  Queued: " + queCount);
            queTotal += queCount;
          }

        }
      }
      catch (Exception exc)
      {
        Logger.LogEmailServiceEvent("Error", "Error processing email types doing queue.", exc);
      }
      

      return queTotal;
    }

    public static int queList(string typeName)
    {
      // get the type
      DataRow drType = EmailTypeUtility.GetTypeByName(typeName);
      int typeId = Convert.ToInt16(drType["email_type_id"]);

      // go get the list
      string strQuery = drType["list_query"].ToString();

      DataTable dtMailingList;
      dtMailingList = MailingLists.GetEmailList(strQuery);

      int n = 0; // counter for how many we send.

      if (strQuery.Contains("CALL"))
      {        
        // don't need to que
        try
        {
          n = Convert.ToInt32(dtMailingList.Rows[0]["number_queued"]);
        }
        catch
        {
          //probably a time out.
        }
      
      }
      else
      {
        // loop through the entries
        int i = 0;  
        n = 0;
        for (i = 0; i < dtMailingList.Rows.Count; i++)
        {
          try
          {
            // start que process
            string currEmail = dtMailingList.Rows[i]["email"].ToString();
            int inviteId = Convert.ToInt32(dtMailingList.Rows[i]["invite_id"]);
            // check against filters
            
            if (!Filters.IsEmailQueued(currEmail, typeId) && !Filters.IsEmailLogged(currEmail, typeId))
            {
              // Queue it!
              if (QueueEmail(typeId, currEmail, inviteId) == 1)
              {
                n++;
              }
            }

          }
          catch (Exception exc)
          {          
            Logger.LogEmailServiceEvent("Error", "Error filling queue.", exc);
          }
        }
      }
    

      return n;
    }

    public static int QueueEmail(int typeId, string emailTo, int inviteId)
    {
      return QueueEmail(typeId, 0, 0, emailTo, "", inviteId);
    }

    public static int QueueEmail(int typeId, int templateID, int sender_id, string emailTo, string emailFrom, int inviteId)
    {
		return QueueEmail(typeId, templateID, sender_id, emailTo, emailFrom, inviteId, 0);
    }

	public static int QueueEmail(int typeId, int templateID, int sender_id, string emailTo, string emailFrom, int inviteId, int overloadId)
	{
		string sqlQueueEmail = " INSERT INTO email_queue (date_queued, email_type_id, email_template_id, email_to,  invite_id, email_hash, overload_id ) " +
		   " VALUES (@dateQueued, @email_type_id, @template_id, @email_to, @inviteId, UNHEX(MD5(@email_to)), @overloadId)";

		Hashtable parameters = new Hashtable();
		parameters.Add("@dateQueued", DateTime.Now);
		parameters.Add("@email_type_id", typeId);
		parameters.Add("@template_id", templateID);
		parameters.Add("@email_to", emailTo);
		parameters.Add("@inviteId", inviteId);
		parameters.Add("@overloadId", overloadId);

		return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlQueueEmail, parameters);
	}

    public static int UnQueueEmail(int queueId)
    {
      string sqlUnQueueEmail = "DELETE FROM email_queue WHERE email_queue_id = @queueId";

      Hashtable parameters = new Hashtable();
      parameters.Add("@queueId", queueId);

      return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlUnQueueEmail, parameters);
    }

	public static int AddOverloadField(int overloadId, string key, string value)
	{
		int idToUse = overloadId;
		
		if (idToUse == 0)
		{
			try
			{
				//do magic to get value

				string getOverloadId = "SELECT shard_info.get_email_overload_fields_id() as overload_id";

				DataRow drOverload = KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataRow(getOverloadId, false);

				idToUse = Convert.ToInt32(drOverload["overload_id"]);
			}
			catch
			{
				return 0;
			}

		}

		try
		{
			string sqlAddOverloadField = " INSERT INTO email_overload_fields (`overload_id`, `key`, `value`)  " +
				" VALUES (@overloadId, @key, @value)";

			Hashtable parameters = new Hashtable();
			parameters.Add("@overloadId", idToUse);
			parameters.Add("@key", key);
			parameters.Add("@value", value);

			KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlAddOverloadField, parameters);
		}
		catch
		{
			return 0;
		}

		return idToUse;
	}
     
    private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

  }
}
