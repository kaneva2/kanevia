///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Text;
using System.Web.Security;
using System.Web;
using System.Web.Handlers;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;


namespace KlausEnt.KEP.Kaneva.Mailer
{
  public class MailingLists
  {
    /// <summary>
    /// Get Invite Reminders
    /// </summary>
    public static DataTable GetListInviteReminder(int limit, int daysElapsed)
    {
      DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityEmailViral();
      Hashtable parameters = new Hashtable();

      string sqlString = "CALL `get_queue_list`(50000, 30)";

      parameters.Add("@limit", limit); //TO DO: limit not supported currently, need work around.
      parameters.Add("@daysElapsed", daysElapsed);

      return dbUtility.GetDataTable(sqlString, parameters);
      
      
      
      /*
      string sqlSelect = " select invite_id, email, to_name " +
                      " from kaneva.invites " + 
                      " where invite_status_id = 1 " + 
                      " and clicked_accept_link = 0 " +
                      " and attempts > 0 " +
                      " and reinvite_date = invited_date " +
                      " and datediff(now(), invited_date) = @daysElapsed " +
                      " and email not in (select distinct email from kaneva.users where datediff(now(), signup_date) <= @daysElapsed) " +
                      " limit @limit";

      Hashtable parameters = new Hashtable();
      parameters.Add("@limit", limit);
      parameters.Add("@daysElapsed", daysElapsed);
      
      return KanevaGlobals.GetDatabaseUtility().GetDataTable(sqlSelect, parameters);
      */
    }

    /// <summary>
    /// Get Email List
    /// </summary>
    public static DataTable GetEmailList(string query)
    {
      DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityEmailViral();
      Hashtable parameters = new Hashtable();
      string sqlString = query;
      return dbUtility.GetDataTable(sqlString, parameters);
    }

  }
}
