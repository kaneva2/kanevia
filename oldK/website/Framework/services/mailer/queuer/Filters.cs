///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Text;
using System.Web.Security;
using System.Web;
using System.Web.Handlers;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;

using KlausEnt.KEP.Kaneva.framework.widgets;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.Mailer
{
  class Filters
  {
        
    public static bool IsEmailQueued(string email, int typeId)
    {
      // TODO: add filter!
      int iEmailId = 0;
      
      string strSql = "SELECT email_queue_id FROM email_queue WHERE email_to = @email AND email_type_id = @typeId";
      
      Hashtable parameters = new Hashtable();
      parameters.Add("@email", email);
      parameters.Add("@typeId", typeId);

      DataRow drMatch = KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataRow(strSql, parameters, false);
      if (drMatch != null)
      {
        iEmailId = Convert.ToInt32(drMatch["email_queue_id"]);

        if (iEmailId > 0)
        {
          return true;
        }
      }

      return false;
    }

    public static bool IsEmailLogged(string email, int typeId)
    {
      // TODO: add filter!
      // TODO: add filter!
      int iEmailId = 0;

      string strSql = "SELECT email_id FROM email_send_log WHERE email_to = @email_to AND email_type_id = @typeId ";

      Hashtable parameters = new Hashtable();
      parameters.Add("@email_to", email);
      parameters.Add("@typeId", typeId);

      DataRow drMatch = KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataRow(strSql, parameters, false);
      if (drMatch != null)
      {
        iEmailId = Convert.ToInt32(drMatch["email_id"]);

        if (iEmailId > 0)
        {
          return true;
        }
      }
      return false;
    }


  }
}
