///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Collections;
using System.Data;
using System.Text;
using System.Threading;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

// Import log4net classes.
using log4net;

namespace KlausEnt.KEP.Kaneva.Mailer
{
  public class Logger
  {
    public static int LogEmailSend(int inviteId, int typeId, int templateId, string emailTo, string emailFrom)
    {

      string sqlLogEmailInvite = " INSERT INTO email_send_log (invite_id, email_type_id, email_template_id, email_to, date_sent, email_hash ) " +
               " VALUES (@inviteId, @email_type_id, @template_id,  @email_to, @dateSent, UNHEX(MD5(@email_to)))";

      Hashtable parameters = new Hashtable();
      parameters.Add("@email_type_id", typeId);      
      parameters.Add("@inviteId", inviteId);
      parameters.Add("@template_id", templateId);
      parameters.Add("@email_to", emailTo);
      parameters.Add("@dateSent", DateTime.Now);

      //return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlLogEmailInvite, parameters);
      int retValue = 0;
      KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteIdentityInsert(sqlLogEmailInvite, parameters, ref retValue);
      return retValue;
    }

    public static int LogEmailOpen(int emailId)
    {

      string sqlLogEmailOpen = "UPDATE email_send_log SET date_opened=@date_opened WHERE email_id=@email_id AND date_opened ='0000-00-00 00:00:00'";    

      Hashtable parameters = new Hashtable();
      parameters.Add("@email_id", emailId);
      parameters.Add("@date_opened", DateTime.Now);

      return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlLogEmailOpen, parameters);
    }

    public static int LogEmailClick(int emailId)
    {

      string sqlLogEmailClick = "UPDATE email_send_log SET date_clicked=@date_clicked WHERE email_id=@email_id AND date_clicked ='0000-00-00 00:00:00'";

      Hashtable parameters = new Hashtable();
      parameters.Add("@email_id", emailId);
      parameters.Add("@date_clicked", DateTime.Now);

      return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlLogEmailClick, parameters);
    }

    public static int LogEmailSendStatus(int emailId, int status)
    {
      string sqlLogEmailStatus = "UPDATE email_send_log SET send_status=@send_status WHERE email_id=@email_id";

      Hashtable parameters = new Hashtable();
      parameters.Add("@email_id", emailId);
      parameters.Add("@send_status", status);

      return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlLogEmailStatus, parameters);
    }

    public static int LogEmailServiceEvent(string level, string message)
    {
      m_logger.Info(message);

      string sqlLogServiceEvent = " INSERT INTO kes_log (date_created, level, message) " +
               " VALUES (@date_created, @level, @message)";

      Hashtable parameters = new Hashtable();
      parameters.Add("@level", level);
      parameters.Add("@message", message);

      parameters.Add("@date_created", DateTime.Now);

      return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlLogServiceEvent, parameters);
    }

    public static int LogEmailServiceEvent(string level, string message, Exception exc)
    {
      m_logger.Error(message, exc);

      string sqlLogServiceEvent = " INSERT INTO kes_log (date_created, level, message, exception) " +
               " VALUES (@date_created, @level, @message, @exception)";

      Hashtable parameters = new Hashtable();
      parameters.Add("@level", level);
      parameters.Add("@message", message);
      parameters.Add("@exception", exc.ToString());
      parameters.Add("@date_created", DateTime.Now);

      return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlLogServiceEvent, parameters);             
    }

    public static int DeleteEmailSendLog(DateTime dateSent)
    {
      string sqlString = "DELETE FROM email_send_log WHERE date_sent > @dateSent";

      Hashtable parameters = new Hashtable();
      parameters.Add("@dateSent", dateSent);

      return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlString, parameters);
    }

    public static int DeleteQueue(DateTime dateQueued)
    {
      string sqlString = "DELETE FROM email_queue WHERE date_queued > @dateQueued";

      Hashtable parameters = new Hashtable();
      parameters.Add("@dateQueued", dateQueued);

      return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlString, parameters);
    }


    /// <summary>
    /// Logger
    /// </summary>
    private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
  }
}
