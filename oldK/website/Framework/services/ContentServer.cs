///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for ContentServer.
	/// </summary>
	public class ContentServer
	{
		public ContentServer(string IP, int Port, int homeId)
		{
			m_IP = IP;
			m_Port = Port;
			m_homeId = homeId;
		}

		public string IP
		{
			get 
			{
				return m_IP;
			}
		}

		public int Port
		{
			get 
			{
				return m_Port;
			}
		}

		public int HomeId
		{
			get 
			{
				return m_homeId;
			}
		}

		private string m_IP;
		private int m_Port;
		private int m_homeId;
	}
}
