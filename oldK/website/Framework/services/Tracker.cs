///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for Tracker.
	/// </summary>
	public class Tracker
	{
			
		/// <summary>
		/// Constructor
		/// </summary>
		public Tracker (string IP, int Port, int CacheUpdatePort, string Torrentpath, string User, string Password)
		{
			m_IP = IP;
			m_Port = Port;
			m_CacheUpdatePort = CacheUpdatePort;
			m_TorrentPath = Torrentpath;
			m_User = User;
			m_Password = Password;
		}

		public string IP
		{
			get 
			{
				return m_IP;
			}
		}

		public int Port
		{
			get 
			{
				return m_Port;
			}
		}

		public int CacheUpdatePort
		{
			get 
			{
				return m_CacheUpdatePort;
			}
		}

		public string Torrentpath
		{
			get 
			{
				return m_TorrentPath;
			}
		}

		public string User
		{
			get 
			{
				return m_User;
			}
		}

		public string Password
		{
			get 
			{
				return m_Password;
			}
		}
		private string m_IP;
		private int m_Port;
		private int m_CacheUpdatePort;
		private string m_TorrentPath;
		private string m_User;
		private string m_Password;
	}
}
