///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.Xml;

// Import log4net classes.
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for ContentServerSectionHandler.
	/// </summary>
	public class ContentServerSectionHandler : IConfigurationSectionHandler
	{
		public ContentServerSectionHandler()
		{
		}

		public object Create (object parent, object configContext, System.Xml.XmlNode section)
		{
			XmlNodeList contentServerSettings;
			//CardTypeCollection al = new CardTypeCollection();
			System.Collections.Hashtable htContentServers = new System.Collections.Hashtable ();

			contentServerSettings = section.SelectNodes ("contentServers//contentServer");

			foreach( XmlNode nodeTracker in contentServerSettings)
			{
				try
				{
					//Add them to the card types collection
					ContentServer contentServer = new ContentServer (nodeTracker.Attributes.GetNamedItem ("IP").Value, Convert.ToInt32 (nodeTracker.Attributes.GetNamedItem ("MessageHandlerPort").Value), Convert.ToInt32 (nodeTracker.Attributes.GetNamedItem ("HomeId").Value));

					htContentServers.Add (nodeTracker.Attributes.GetNamedItem ("IP").Value + nodeTracker.Attributes.GetNamedItem ("MessageHandlerPort").Value, contentServer);
				}
				catch (Exception exc)
				{
					m_logger.Error ("Error reading content server configuration", exc);
				}

			}

			return htContentServers;
		}

		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

	}

}
