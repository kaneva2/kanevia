///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.Xml;

// Import log4net classes.
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for Trackers.
	/// </summary>
	public class TrackerSectionHandler : IConfigurationSectionHandler
	{
		public TrackerSectionHandler ()
		{
		}

		public object Create (object parent, object configContext, System.Xml.XmlNode section)
		{
			XmlNodeList trackers;
			//CardTypeCollection al = new CardTypeCollection();
			System.Collections.Hashtable htTrackers = new System.Collections.Hashtable ();

			trackers = section.SelectNodes ("trackers//tracker");

			foreach( XmlNode nodeTracker in trackers)
			{
				try
				{
					//Add them to the card types collection
					Tracker tracker = new Tracker (nodeTracker.Attributes.GetNamedItem ("IP").Value, Convert.ToInt32 (nodeTracker.Attributes.GetNamedItem ("Port").Value),
						Convert.ToInt32 (nodeTracker.Attributes.GetNamedItem ("CacheUpdatePort").Value), nodeTracker.Attributes.GetNamedItem ("TorrentPath").Value,
						KanevaGlobals.Decrypt (nodeTracker.Attributes.GetNamedItem ("User").Value), KanevaGlobals.Decrypt (nodeTracker.Attributes.GetNamedItem ("Password").Value));

					htTrackers.Add (nodeTracker.Attributes.GetNamedItem ("IP").Value + nodeTracker.Attributes.GetNamedItem ("Port").Value, tracker);
				}
				catch (Exception exc)
				{
					m_logger.Error ("Error reading tracker configuration", exc);
				}

			}

			return htTrackers;
		}

		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

	}
}
