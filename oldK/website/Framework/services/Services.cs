///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Collections;
using System.Data;
using System.Text;
using System.Threading;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

// Import log4net classes.
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for Services.
	/// </summary>
	public class Services
	{
		static readonly ILog logger = LogManager.GetLogger("TorrentConsistencyCheck");
		static Services()
		{
            MAX_NUM_FILES_IN_TORRENT = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["maxNumFilesInTorrent"]);
            TCC_EMAIL_RECIPIENT = System.Configuration.ConfigurationManager.AppSettings["TCCEmailRecipient"];
		}

		public Services()
		{
		}

		// **********************************************************************************************
		// TCC Functions
		// **********************************************************************************************
		#region TCC Functions

		/// <summary>
		/// Periodically checks torrents for:
		///  - they haven't existed for DeleteAfterNDays without being published. Marks these torrents
		///    as deleted in the db if such based on PerformDeletion.
		///  - For torrents that have had their asset deleted and the current datetime is greater than
		///    any existing download_stop_date for that torrent, copy that torrent to the content server's
		///    import directory as torrentname.deleted. It will be picked up by the content server and deleted
		///    from the content server.
		/// </summary>
		/// <returns>None.</returns>
		public static void TorrentConsistencyCheck()
		{
			logger.Info("Checking Partial Asset Deletion Start");
			DeletePartiallyUploadedAssets();
			logger.Info("Checking Partial Asset Deletion End");

			logger.Info("Checking Expired Upload Start");
			DeleteExpiredAssetUploads();
			logger.Info("Checking Expired Upload End");

			logger.Info("Checking Publishing status Start");
			CheckOrphanedAssets();
			logger.Info("Checking Publishing status End");

			logger.Info("Checking Asset Truancy");
			CheckAssetTruancy();
			logger.Info("Checking Asset Truancy End");
			
			logger.Info("Torrent Consistency Check End");
		}

		/// <summary>
		/// remove partially uploaded files of assets that are marked for deletion
		/// </summary>
		private static void DeletePartiallyUploadedAssets()
		{
			ILog m_logger = LogManager.GetLogger("TorrentConsistencyCheck");
			DataTable dt = StoreUtility.GetDeletedPartialUploads();

			if (dt.Rows.Count == 0)
				return;

			StringBuilder deletionMsgBody = new StringBuilder() ;
			deletionMsgBody.Append("Following assets were marked for deletion before the uploading is completed.");
			deletionMsgBody.Append("They are now removed from the upload server.\r\n\r\n");

			foreach(DataRow row in dt.Rows)
			{
				int assetId = int.Parse(row["asset_id"].ToString());
				string path = row["path"].ToString();
				string filename = row["filename"].ToString();
				FileInfo fi = new FileInfo(Path.Combine(path, filename)) ;
				
				if(fi.Exists)
				{
					try
					{
						//every asset is put in an individual dir, so delete itself and the parent directory
						DirectoryInfo assetDir = fi.Directory;
						DirectoryInfo userDir = assetDir.Parent;
						//delete the file
						fi.Delete();
						
						//delete asset directory
						if(assetDir.GetFileSystemInfos().Length  == 0)
						{
							assetDir.Delete(); //not to do recursively deletion just in case the folder contains other data, although it shouldn't
							//delete user dir if it's empty
							if(userDir.GetFileSystemInfos().Length  == 0)
							{
								userDir.Delete();
							}
						}
						else
						{
							//this is not supposed to happen with current setup
							m_logger.Warn("Path " + path + " contains other files/folder after deletion of asset " + assetId);
						}
						//change asset status to deleted
						StoreUtility.UpdateAssetStatus(assetId, (int) Constants.eASSET_STATUS.DELETED);

						string deletion_msg = "Deleted partially uploaded file of asset " + assetId;
						m_logger.Info(deletion_msg);

						deletionMsgBody.Append("Asset Id <");
						deletionMsgBody.Append(assetId);
						deletionMsgBody.Append("> File Name <");
						deletionMsgBody.Append(filename);
						deletionMsgBody.Append(">.\r\n");
					}catch(Exception e)
					{
						m_logger.Error("Can not delete asset " + assetId, e );
					}
				}else
				{
					if(fi.Directory.Exists)
					{
						//direcotry exits but no file means nothing has been uploaded
						m_logger.Info("Nothing has been uploaded for asset " + assetId);
						StoreUtility.UpdateAssetStatus(assetId, (int) Constants.eASSET_STATUS.DELETED);
					}
					else
					{
						m_logger.Error("Can not delete asset " + assetId + " file " + fi.FullName + " not found." );
					}
				}
			}

			//send email
			MailUtility.SendEmail(KanevaGlobals.FromEmail, TCC_EMAIL_RECIPIENT, "Alert for partially uploaded content deletion", 
				deletionMsgBody.ToString(), false, false, 2);
		}

		/// <summary>
		/// remove partially uploaded files of assets that did not finish uploading within time limit
		/// </summary>
		private static void DeleteExpiredAssetUploads()
		{
			ILog m_logger = LogManager.GetLogger("TorrentConsistencyCheck");
			int uploadTimeoutHrs = StoreUtility.GetFileUploadTimeout();
			//give it an extra hour to avoid contention
			DateTime cutoffTime = DateTime.Now.AddHours(-(uploadTimeoutHrs+ 1));
			DataTable dt = StoreUtility.GetExpiredUploads(cutoffTime);

			if (dt.Rows.Count == 0)
				return;

			StringBuilder deletionMsgBody = new StringBuilder() ;
			deletionMsgBody.Append("Following assets were not uploaded after ").Append(uploadTimeoutHrs);
			deletionMsgBody.Append(" hour(s). They are now removed from the upload server.\r\n\r\n");

			foreach(DataRow row in dt.Rows)
			{
				int assetId = int.Parse(row["asset_id"].ToString());
				string path = row["path"].ToString();
				string filename = row["filename"].ToString();
				try
				{
					FileInfo fi = new FileInfo(Path.Combine(path, filename)) ;
					if(fi.Exists)
					{
						//every asset is put in an individual dir, so delete itself and the parent directory
						DirectoryInfo assetDir = fi.Directory;
						DirectoryInfo userDir = assetDir.Parent;
						//delete the file
						fi.Delete();
						
						//delete asset directory
						if(assetDir.GetFileSystemInfos().Length  == 0)
						{
							assetDir.Delete(); // Do not perform recursive deletion. Current design specifies that the
											   // target destination directory for content is "<specified content server>\UserId\AssetId\filename,
											   // thus, the assetDir and  userDir variables. But, old assets can be of the form
											   // <specified content server>\filename. The lack of userid and assetid on these old
											   // assets results in assetDir being the base directory for many different assets and
											   // userDir being the parent directory on the content server. So, don't do recursive, ever.

							if(userDir.GetFileSystemInfos().Length  == 0)
							{
								userDir.Delete();
							}
						}
						else
						{
							//this is not supposed to happen with current setup
							m_logger.Warn("Path " + path + " contains other files/folder after deletion of asset " + assetId);
						}
						StoreUtility.UpdateAssetStatus(assetId, (int) Constants.eASSET_STATUS.DELETED);

						string deletion_msg = "Deleted expired asset upload file, assetId= " + assetId;
						m_logger.Info(deletion_msg);

						deletionMsgBody.Append("Asset Id <");
						deletionMsgBody.Append(assetId);
						deletionMsgBody.Append("> File Name <");
						deletionMsgBody.Append(filename);
						deletionMsgBody.Append(">.\r\n");
					}
					else
					{
						if(fi.Directory.Exists)
						{
							//direcotry exits but no file means nothing has been uploaded
							m_logger.Info("Nothing has been uploaded for asset " + assetId);
							StoreUtility.UpdateAssetStatus(assetId, (int) Constants.eASSET_STATUS.DELETED);
						}
						else
						{
							m_logger.Error("Can not delete expired asset upload, assetId = " + assetId + " file " + fi.FullName + " not found." );
						}
					}
				}
				catch(Exception e)
				{
					m_logger.Error("Exception - Can not deleted expired asset upload file, assetId=" + assetId, e );
				}
			}

			//send email
			MailUtility.SendEmail(KanevaGlobals.FromEmail, TCC_EMAIL_RECIPIENT, "Alert for expired asset upload deletion", 
				deletionMsgBody.ToString(), false, false, 2);
		}

		/// <summary>
		/// Get the current set of assets that has been uploaded and processed but 
		/// second phase of publishing was not completed in Kaneva.
		/// </summary>
		private static void CheckOrphanedAssets()
		{
			ArrayList toDeleteAssets = new ArrayList();

			DataTable dt = StoreUtility.GetOrphanedAssets();

			bool performDeletion = Convert.ToInt32(
                System.Configuration.ConfigurationManager.AppSettings["DeleteIfUnPublishedDays"]) == 1;
            int DeleteAfterNDays = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["UnPublishedDays"]);
            int MailAlertAfterNDays = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MailAlertDays"]);


			DateTime now = DateTime.Now;
			//assets uploaded before this time are eligible for deletion
			DateTime deletionCutoffTime = now.AddDays(-DeleteAfterNDays); 
			//assets uploaded before this time are eligible mail alerts
			DateTime mailAlertCutoffTime = now.AddDays(-MailAlertAfterNDays); 

			if(dt.Rows.Count > 0)
			{
				string alertSubject = "Alert for asset truancy";
				string alertHeader = "Following assets have been uploaded and processed but " +
					"second phase of publishing of these assets was not completed after " + MailAlertAfterNDays +
					" day(s).\r\n\r\n";
				StringBuilder alertMsgBody = new StringBuilder() ;

				string deletionSubject = "Alert for asset deletion";
				string deletionHeader = "Following assets have been uploaded and processed but " +
					"second phase of publishing of these assets was not completed after " + DeleteAfterNDays +
					" day(s).\r\n" +
					"They are now marked for deletion.\r\n\r\n";
				StringBuilder deletionMsgBody = new StringBuilder() ;

				foreach(DataRow row in dt.Rows)
				{
					DateTime assetUploadedTime = (DateTime) row["completed_datetime"];

					if ( assetUploadedTime < deletionCutoffTime )
					{
						if ( performDeletion)
						{
							toDeleteAssets.Add(row["asset_id"]);
							StringBuilder sb = new StringBuilder();
							sb.Append("Asset id <");
							sb.Append(row["asset_id"]);
							sb.Append("> Name <");
							sb.Append(row["name"]);
							sb.Append("> unpublished, Uploaded date <");
							sb.Append(row["completed_datetime"]);
							sb.Append("> is older than ");
							sb.Append(DeleteAfterNDays);
							sb.Append(" day(s). It's now marked for deletion");
							logger.Info(sb.ToString());

							deletionMsgBody.Append("Asset id <");
							deletionMsgBody.Append(row["asset_id"]);
							deletionMsgBody.Append("> Name <");
							deletionMsgBody.Append(row["name"]);
							deletionMsgBody.Append("> Uploaded date <");
							deletionMsgBody.Append(row["completed_datetime"]);
							deletionMsgBody.Append(">.\r\n");
						}
						else
						{
							StringBuilder sb = new StringBuilder();
							sb.Append("Asset id <");
							sb.Append(row["asset_id"]);
							sb.Append("> Name <");
							sb.Append(row["name"]);
							sb.Append("> unpublished, Uploaded date <");
							sb.Append(row["completed_datetime"]);
							sb.Append("> is older than ");
							sb.Append(DeleteAfterNDays);
							sb.Append(" day(s).");
							logger.Info(sb.ToString());
						}
					}
					else if ( assetUploadedTime < mailAlertCutoffTime )
					{
						alertMsgBody.Append("Asset id <");
						alertMsgBody.Append(row["asset_id"]);
						alertMsgBody.Append("> Name <");
						alertMsgBody.Append(row["name"]);
						alertMsgBody.Append("> Uploaded date <");
						alertMsgBody.Append(row["completed_datetime"]);
						alertMsgBody.Append(">.\r\n");
					}
				}

				if ( (alertSubject != null) && (alertMsgBody.Length > 0) )
				{
					MailUtility.SendEmail(KanevaGlobals.FromEmail, TCC_EMAIL_RECIPIENT, alertSubject, 
						alertHeader + alertMsgBody.ToString(), 
						false, false, 2);
				}

				if ( toDeleteAssets.Count > 0)
				{
					MailUtility.SendEmail(KanevaGlobals.FromEmail, TCC_EMAIL_RECIPIENT, deletionSubject, 
						deletionHeader + deletionMsgBody.ToString(), 
						false, false, 2);
					StoreUtility.DeleteOrphanedAssets(toDeleteAssets);
				}
			}
			
			toDeleteAssets.Clear();
		}

		/// <summary>
		// Mail an alert for assets that are uploaded but not processed within time limit
		/// </summary>
		private static void CheckAssetTruancy()
		{
			int contentDownloadAlertDateTimeDays = Convert.ToInt32(
                System.Configuration.ConfigurationManager.AppSettings["AlertDaysSinceCreation"]);

			DataTable dtContentAlert = StoreUtility.GetContentNotProcessed(contentDownloadAlertDateTimeDays);

			string subject = "Alert for asset publishing";
			string header = "Following assets are uploaded but not processed after " + 
				contentDownloadAlertDateTimeDays + 
				" day(s)\r\n\r\n";
			StringBuilder msg = new StringBuilder() ;

			if(dtContentAlert.Rows.Count > 0)
			{
				foreach(DataRow row in dtContentAlert.Rows)
				{
					msg.Append("Asset id <");
					msg.Append(row["asset_id"]);
					msg.Append("> Asset Name <");
					msg.Append(row["name"]);
					msg.Append("> Publish Status <");
					msg.Append(row["publish_status_id"]);
					msg.Append("> Uploaded date <");
					msg.Append(row["completed_datetime"]);
					msg.Append(">.\r\n");
				}
				MailUtility.SendEmail(KanevaGlobals.FromEmail, TCC_EMAIL_RECIPIENT, 
					subject, header + msg.ToString(), false, false, 2);
			}
		}

//		/// <summary>
//		/// Post an asset file to the tracker. Asset file is attached to 
//		/// the SOAP request using DIME.
//		/// </summary>
//		/// <param name="user_id">user id.</param>
//		/// <param name="displayName">Kaneva name to show.</param>
//		/// <param name="infoHash">The hash of the torrent.</param>
//		/// <param name="contentSize">The size of the content that the torrent represents.</param>
//		/// <param name="contentFileName">The name of the file that the torrent represents.
//		/// Can be a folder name if the torrent represents multiples.</param>
//		/// <param name="description">A short description about the content to be used in Kaneva.</param>
//		/// <param name="isSecure">Free or charged for content. KEI will mirror content on our own content
//		/// servers for content that is bought. This will ensure it is available to other purchasers in the P2P
//		/// network.</param>
//		/// <returns>Zero if successful, error code otherwise.</returns>
//		/// 
//		[Obsolete("This is for backward compatibilities, not used for http-based publishing")]
//		public static int PostAssetFile (int user_id, string displayName, string infoHash, int contentSize, string contentFileName, string description, bool isSecure, bool isFolder, KlausEnt.KEP.Kaneva.Constants.eASSET_TYPE assetType, System.IO.Stream sTorrent, string fileName, string username)
//		{
//			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;
//
//			try
//			{
//				if ( user_id != 0)
//				{
//					bool isAdmin = false;
//					int diskQuota = 0;
//					double currentUsed = 0;
//					string membershipType = null;
//
//					ret = GetUserQuota( user_id, ref diskQuota, ref currentUsed, ref membershipType, ref isAdmin);
//
//					if ( ret == (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS)
//					{
//						if ( !isAdmin && isSecure && ( ((currentUsed + (double)contentSize) / 1048576) > (double)diskQuota) )
//						{
//							// This post will exceed the user's disk quota
//							ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_ASSET_QUOTA_EXCEEDED;
//						}
//						else if (sTorrent != null)
//						{
//							bool invalidTorrent = false;
//							bool hasAnnounceList = false;
//							string announceUrl = null;
//							bool hasToomanyFiles = false;
//
//							try
//							{
//								hasAnnounceList = TorrentUtility.HasAnnounceList (sTorrent);
//								announceUrl = TorrentUtility.GetAnnounceUrl (sTorrent);
//
//								//save it somewhere to make libtorrent work
//								sTorrent.Seek(0, SeekOrigin.Begin); // reset in case we've already read it for some othe purpose
//								
//
        //								//string tempTorrentPath = System.Configuration.ConfigurationManager.AppSettings ["TempTorrentDirectory"] + filename;
//								string tempTorrentPath = Path.GetTempFileName();
//
//								//Create byte Array with file len
//								Byte[] bTorrent = new Byte [sTorrent.Length];
//
//								//force the control to load data in array
//								sTorrent.Read (bTorrent, 0, bTorrent.Length);
//								
//								// Save it to a file so libtorrent can read it.
//
//								System.IO.FileStream fTempTorrent = new System.IO.FileStream (tempTorrentPath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
//								
//								fTempTorrent.Write (bTorrent, 0, bTorrent.Length);
//								fTempTorrent.Close ();
//								sTorrent.Seek(0, SeekOrigin.Begin);
//
//								TorrentInfoView torrentInfoView = TorrentInfoAdaptor.GetInstance ().GetInfo (tempTorrentPath);
//								hasToomanyFiles = torrentInfoView.NumFiles > MAX_NUM_FILES_IN_TORRENT;
//							}
//							catch(Exception e)
//							{
//								invalidTorrent = true;
//								m_Service_logger.Error ("IDS_EXCEPTION_POST_ASSET_FILE", e);
//								ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_FILE_INVALID_TORRENT;
//							}
//
//							if(!invalidTorrent)
//							{
//								if(announceUrl == null || ! announceUrl.ToUpper ().Equals(KanevaGlobals.AnnounceURL.ToUpper ()))
//								{
//									//incorrect announce url
//									ret = (int) Constants.eDS_RETURNCODES.ERROR_POST_FILE_INVALID_ANNOUNCE_URL;
//								}
//								else if(hasAnnounceList)
//								{
//									//has announce list
//									ret = (int) Constants.eDS_RETURNCODES.ERROR_POST_FILE_ANNOUNCE_LIST_FOUND;
//								}else if(hasToomanyFiles)
//								{
//									//reach num files limit in the torrent
//									ret = (int) Constants.eDS_RETURNCODES.ERROR_POST_FILE_EXCEEDED_NUM_FILES_LIMIT;
//								}
//								else
//								{
//									DataRow dr;
//
//									dr = KlausEnt.KEP.Kaneva.StoreUtility.GetTorrent( user_id, infoHash, contentFileName);
//
//									if (dr == null)
//									{
//										ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;
//
//										ret = AZUREUS.PostToAzureusTracker( fileName, user_id, false, sTorrent);
//
//										if ( ret == (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS)
//										{
//											string contentLocation = null;	// record the directory location where the torrent's content
//											// will be delivered. Status update will later use this to determine
//											// the status of the content download.
//
//											ret = AZUREUS.PostToAzureus( fileName, sTorrent, ref contentLocation);
//
//											if ( ret == (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS)
//											{
//												// Update status, on failure just orphan the tracker torrent
//												int torrentId = StoreUtility.InsertTorrent( user_id, fileName, displayName, infoHash, contentSize, contentFileName, description, isSecure, contentLocation, isFolder, (int) KlausEnt.KEP.Kaneva.Constants.eTORRENT_ENTRY_STATUS.ACTIVE, assetType);
//
//												if ( torrentId == 0)
//												{
//													m_Service_logger.Error ("IDS_ERROR_POST_DB_INSERT_FAILURE UserId = " + user_id + " File name = " + fileName);
//													ret = (int) Constants.eDS_RETURNCODES.ERROR_POST_DB_INSERT_FAILURE;
//												}
//											}
//										}
//									} 
//									else
//									{
//										// Entry already exists
//										m_Service_logger.Error ("IDS_ERROR_POST_DB_ENTRY_ALREADY_EXISTS UserId = " + user_id + " File name = " + fileName);
//										ret = (int) Constants.eDS_RETURNCODES.ERROR_POST_DB_ENTRY_ALREADY_EXISTS;
//									}
//								}
//							}
//						} 
//						else
//						{
//							if (sTorrent == null)
//							{
//								m_Service_logger.Error ("IDS_ERROR_POST_EMPTY_ASSET UserId = " + user_id);
//								ret = (int) Constants.eDS_RETURNCODES.ERROR_POST_EMPTY_ASSET;
//							} 
//							else
//							{
//								m_Service_logger.Error ("EXCEPTION_POSTING_ASSET_FILE UserId = " + user_id);
//								ret = (int) Constants.eDS_RETURNCODES.EXCEPTION_POSTING_ASSET_FILE;
//							}
//						}
//					}
//				} 
//				else
//				{
//					ret = (int) Constants.eDS_RETURNCODES.NOT_VALIDATED;
//				}
//			}
//			catch (Exception e)
//			{
//				m_Service_logger.Error ("IDS_EXCEPTION_POST_ASSET_FILE", e);
//				ret = (int) Constants.eDS_RETURNCODES.EXCEPTION_POSTING_ASSET_FILE;
//			}
//
//			return ret;
//		}


        ///// <summary>
        ///// Obtain a user's size quota, currently used space, and membership type.
        ///// </summary>
        ///// <param name="user_id">user id to determine quota</param>
        ///// <param name="diskQutoa">mega bytes of disk</param>
        ///// <param name="currentUsed">number of bytes currently used</param>
        ///// <param name="membershipType">textual information about the type of membership</param>
        ///// <returns>Zero if successful, error code otherwise.</returns>
        //private static int GetUserQuota( int user_id, ref int diskQuota, ref double currentUsed, ref string membershipType, ref bool isAdmin)
        //{
        //    int ret = (int) Constants.eDS_RETURNCODES.SUCCESS;

        //    try
        //    {
        //        if ( UsersUtility.IsUserAdministrator (user_id))
        //        {
        //            isAdmin = true;
        //        }

        //        diskQuota = 0;
        //        membershipType = null;
        //        currentUsed = 0;

        //        DataRow dr;

        //        dr = StoreUtility.GetActiveUserLicenseSubscription(user_id);

        //        if (dr == null)
        //        {
        //            // Didn't find pilot or commercial license, get the default base (formally called developer)
        //            dr = StoreUtility.GetLicenseSubscription ((int) KlausEnt.KEP.Kaneva.Constants.eLICENSE_SUBSCRIPTIONS.DEVELOPER);
        //        }

        //        if (dr != null)
        //        {
        //            if ( ! dr.IsNull("disk_quota") )
        //            {
        //                // In megabytes
        //                diskQuota = (int) dr["disk_quota"];
        //            }

        //            if ( ! dr.IsNull("name") )
        //            {
        //                membershipType = (string) dr["name"];
        //            }

        //            dr = StoreUtility.GetUserPublishedSize (user_id);

        //            if ( ! dr.IsNull("content_size") )
        //            {
        //                // In bytes
        //                currentUsed = (double) dr["content_size"];
        //            }
        //        }
        //        else
        //        {
        //            m_Service_logger.Error ("IDS_ERROR_OBTAINING_USER_QUOTA UserId=" + user_id);
        //            ret = (int) Constants.eDS_RETURNCODES.ERROR_OBTAINING_USER_QUOTA;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        m_Service_logger.Error ("IDS_EXCEPTION_GET_USER_QUOTA UserId=" + user_id, e);
        //        ret = (int) Constants.eDS_RETURNCODES.EXCEPTION_GET_USER_QUOTA;
        //    }

        //    return ret;
        //}

		#endregion

		// **********************************************************************************************
		// Most Popular and Ranking Functions
		// **********************************************************************************************
		#region Most Popular and Ranking Functions

//		/// <summary>
//		/// UpdateMostPopularTags
//		/// </summary>
//		public static void UpdateMostPopularTags ()
//		{
//			UpdateMostPopularTags ((int) Constants.eASSET_TYPE.GAME);
//			UpdateMostPopularTags ((int) Constants.eASSET_TYPE.MUSIC);
//			UpdateMostPopularTags ((int) Constants.eASSET_TYPE.VIDEO);
//			UpdateMostPopularTags ((int) Constants.eASSET_TYPE.PICTURE);
//
//			UpdateMostPopularTags (StoreUtility.cCOMMUNITY_POPULAR_KEYWORD_TYPE);
//		}
//
//		/// <summary>
//		/// UpdateMostPopularTags
//		/// </summary>
//		/// <param name="assetTypeId"></param>
//		private static void UpdateMostPopularTags (int assetTypeId)
//		{
//			char [] splitter  = {' '};
//			string [] arKeywords = null;  // key is the tag, value is the count
//			Hashtable htAllTags = new Hashtable ();
//
//			DataTable dtTaggedItems;
//			if (assetTypeId.Equals (StoreUtility.cCOMMUNITY_POPULAR_KEYWORD_TYPE))
//			{
//				dtTaggedItems = CommunityUtility.GetAllTaggedChannels ();
//			}
//			else
//			{
//				dtTaggedItems = StoreUtility.GetAllTaggedAssets (assetTypeId);
//			}
//
//			string strKeywords = "";
//			string strTag = "";
//
//			// Loop through all the tags, save to a hashtable, with the added up counts
//			for (int i = 0; i < dtTaggedItems.Rows.Count; i ++)
//			{
//				strKeywords = dtTaggedItems.Rows [i]["keywords"].ToString ();
//				arKeywords = strKeywords.Split (splitter);
//				
//				for (int j = 0; j < arKeywords.Length; j ++)
//				{
//					// Read a single keyword (tag)
//					strTag = arKeywords [j].ToString ().Trim ().ToLower ();
//
//					if (strTag.Length > 0)
//					{
//						// Does it already exist?
//						if (htAllTags.ContainsKey (strTag))
//						{
//							// Tag already found, increment the counter
//							htAllTags [strTag] = Convert.ToInt32 (htAllTags [strTag]) + 1;
//						}
//						else
//						{
//							// Tag not found yet
//							htAllTags.Add (strTag, 1);
//						}
//					}
//				}
//			}
//
//			// Didn't get any then get out
//			if (htAllTags == null)
//			{
//				return;
//			}
//
//			// We now have a hashtable of all tags with a count
//			// Update the counts in the database
//			// If it already exists, just update the count
//			IDictionaryEnumerator en = htAllTags.GetEnumerator ();
//			while (en.MoveNext ())
//			{
//				if (TagAlreadyExists (en.Key.ToString (), assetTypeId))
//				{
//					UpdateTag (en.Key.ToString (), Convert.ToInt32 (en.Value), assetTypeId);
//				}
//				else
//				{
//					InsertTag (en.Key.ToString (), Convert.ToInt32 (en.Value), assetTypeId);
//				}
//			}
//		}
//
//		/// <summary>
//		/// InsertTag
//		/// </summary>
//		/// <param name="tag"></param>
//		/// <param name="totalCount"></param>
//		/// <returns></returns>
//		private static int InsertTag (string tag, int totalCount, int assetTypeId)
//		{
//			DatabaseUtility dbUtility = GetDatabaseUtility ();
//
//			string sql = "INSERT INTO popular_keywords " +
//				"(keyword, total_count, asset_type_id " +
//				") VALUES (" +
//				"@tag, @totalCount, @assetTypeId)";
//
//			Hashtable parameters = new Hashtable ();			
//			parameters.Add ("@tag", tag);
//			parameters.Add ("@totalCount", totalCount);
//			parameters.Add ("@assetTypeId", assetTypeId);
//			return dbUtility.ExecuteNonQuery (sql, parameters);
//		}
//
//		/// <summary>
//		/// UpdateTag
//		/// </summary>
//		/// <returns></returns>
//		private static int UpdateTag (string tag, int totalCount, int assetTypeId)
//		{
//			DatabaseUtility dbUtility = GetDatabaseUtility ();
//
//			string sql = "UPDATE popular_keywords " +
//				" SET total_count = @totalCount " +
//				" WHERE keyword = @tag " + 
//				" AND asset_type_id = @assetTypeId ";
//
//			Hashtable parameters = new Hashtable ();			
//			parameters.Add ("@tag", tag);
//			parameters.Add ("@totalCount", totalCount);
//			parameters.Add ("@assetTypeId", assetTypeId);
//			return dbUtility.ExecuteNonQuery (sql, parameters);
//		}
//
//		/// <summary>
//		/// TagAlreadyExists
//		/// </summary>
//		/// <param name="messageId"></param>
//		/// <returns></returns>
//		private static bool TagAlreadyExists (string keyword, int assetTypeId)
//		{
//			string sqlSelect = "SELECT total_count " +
//				" FROM popular_keywords " +
//				" WHERE keyword = @keyword " +
//				" AND asset_type_id = @assetTypeId";
//
//			Hashtable parameters = new Hashtable ();			
//			parameters.Add ("@keyword", keyword);
//			parameters.Add ("@assetTypeId", assetTypeId);
//			DataRow drPopularTag = GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
//			return (drPopularTag != null);
//		}

//////		/// <summary>
//////		/// CalculateAssetRankings
//////		/// </summary>
//////		/// <returns></returns>
//////		public static int CalculateAssetRankings ()
//////		{
//////			// Get the datetime of the last time notifications where sent
//////			DataRow drConfig = GetConfiguration ();
//////
//////			if (drConfig == null)
//////			{
//////				m_Service_logger.Error ("No configuration data found");
//////				return -1;
//////			}
//////
//////			DateTime dtRankUpdated = Convert.ToDateTime (drConfig ["last_ranking_updated"]);
//////			DateTime dtNow = Convert.ToDateTime (drConfig ["current_dateTime"]);
//////	
//////			// Is it time to update?
//////			if (dtNow.AddDays (-1).CompareTo (dtRankUpdated) < 0)
//////			{
//////				return 0;
//////			}
//////
//////			try
//////			{
//////				string sqlSelect = "SELECT a.asset_id, a.number_of_streams, a.number_of_diggs, a.rank_overall, a.rank_yesterday, a.rank_difference, " +
//////				" COALESCE(digg_rating,0) as digg_rating " +
//////				" FROM assets a " +
//////				" Left Join( " +
//////				" SELECT COUNT(ad.digg_id) as digg_rating, asset_id from asset_diggs ad " + 
//////				" WHERE ad.created_date > ADDDATE(NOW(),INTERVAL -1 DAY) " + 
//////				" GROUP BY ad.asset_id)x on a.asset_id = x.asset_id " +
//////				" WHERE a.status_id IN (" + (int) Constants.eASSET_STATUS.ACTIVE + "," + (int) Constants.eASSET_STATUS.NEW + ")" +  
//////				" ORDER BY digg_rating DESC, a.number_of_diggs DESC, a.number_of_streams DESC ";
//////
//////				DatabaseUtility dbUtility = GetDatabaseUtility ();
//////				DataTable pdtAssets = dbUtility.GetDataTable (sqlSelect);
//////
//////				// Overall
//////				for (int i = 0; i < pdtAssets.Rows.Count; i ++)
//////				{
//////					dbUtility.ExecuteNonQuery ("UPDATE assets SET rank_overall = " + (i + 1) + "," +
//////						" rank_yesterday = " + Convert.ToInt32 (pdtAssets.Rows [i]["rank_overall"]) + "," +
//////						" rank_difference = " + (Convert.ToInt32 (pdtAssets.Rows [i]["rank_overall"]) - (i + 1)) +
//////						" WHERE asset_id = " + Convert.ToInt32 (pdtAssets.Rows [i]["asset_id"]));
//////				}
//////
//////	//			// Each Type
//////	//			DataView dvAssetTypes = new DataView (pdtAssets, "asset_type_id = " + (int) Constants.eASSET_TYPE.VIDEO, "popular_rating DESC, number_of_downloads DESC", DataViewRowState.CurrentRows);
//////	//			for (int j = 0; j < dvAssetTypes.Count; j ++)
//////	//			{
//////	//				dbUtility.ExecuteNonQuery ("UPDATE assets SET rank_by_type = " + (j + 1) + " WHERE asset_id = " + Convert.ToInt32 (dvAssetTypes [j]["asset_id"]));
//////	//			}
//////	//
//////	//			dvAssetTypes = new DataView (pdtAssets, "asset_type_id = " + (int) Constants.eASSET_TYPE.GAME, "popular_rating DESC, number_of_downloads DESC", DataViewRowState.CurrentRows);
//////	//			for (int k = 0; k < dvAssetTypes.Count; k ++)
//////	//			{
//////	//				dbUtility.ExecuteNonQuery ("UPDATE assets SET rank_by_type = " + (k + 1) + " WHERE asset_id = " + Convert.ToInt32 (dvAssetTypes [k]["asset_id"]));
//////	//			}
//////	//
//////	//			dvAssetTypes = new DataView (pdtAssets, "asset_type_id = " + (int) Constants.eASSET_TYPE.ASSET, "popular_rating DESC, number_of_downloads DESC", DataViewRowState.CurrentRows);
//////	//			for (int l = 0; l < dvAssetTypes.Count; l ++)
//////	//			{
//////	//				dbUtility.ExecuteNonQuery ("UPDATE assets SET rank_by_type = " + (l + 1) + " WHERE asset_id = " + Convert.ToInt32 (dvAssetTypes [l]["asset_id"]));
//////	//			}
//////
//////				// Update channel rankings
//////				sqlSelect = "SELECT c.community_id, c.number_of_views, c.number_of_diggs, c.rank, c.rank_yesterday, c.rank_difference, COALESCE(digg_rating, 0) " + 
//////					" FROM communities c " +  
//////					" Left Join (" + 
//////					" SELECT COUNT(cd.digg_id) as digg_rating, channel_id FROM channel_diggs cd " + 
//////					" WHERE cd.created_date > ADDDATE(NOW(),INTERVAL -1 DAY) " + 
//////					" GROUP BY cd.channel_id) " + 
//////					" x on x.channel_id = c.community_id " + 
//////					" WHERE c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE +
//////					" AND c.category_id NOT IN (" + (int) Constants.eCOMMUNITY_CATEGORIES.MAIN_TAB + ")";
//////		
//////				string orderBy = " ORDER BY digg_rating DESC, c.number_of_diggs DESC, c.number_of_views DESC ";
//////
//////				DataTable pdtComms = dbUtility.GetDataTable (sqlSelect + " AND is_personal = 0 " + orderBy);
//////				
//////				// Broadcast channels
//////				for (int i = 0; i < pdtComms.Rows.Count; i ++)
//////				{
//////					dbUtility.ExecuteNonQuery ("UPDATE communities SET rank = " + (i + 1) +  "," +
//////						" rank_yesterday = " + Convert.ToInt32 (pdtComms.Rows [i]["rank"]) + "," +
//////						" rank_difference = " + (Convert.ToInt32 (pdtComms.Rows [i]["rank"]) - (i + 1)) +
//////						" WHERE community_id = " + Convert.ToInt32 (pdtComms.Rows [i]["community_id"]));
//////				}
//////
//////				// Personal Channels
//////				pdtComms = dbUtility.GetDataTable (sqlSelect + " AND is_personal = 1 " + orderBy);
//////				for (int i = 0; i < pdtComms.Rows.Count; i ++)
//////				{
//////					dbUtility.ExecuteNonQuery ("UPDATE communities SET rank = " + (i + 1) + "," +
//////						" rank_yesterday = " + Convert.ToInt32 (pdtComms.Rows [i]["rank"]) + "," +
//////						" rank_difference = " + (Convert.ToInt32 (pdtComms.Rows [i]["rank"]) - (i + 1)) +
//////						" WHERE community_id = " + Convert.ToInt32 (pdtComms.Rows [i]["community_id"]));
//////				}
//////
//////			}
//////			catch (Exception exc)
//////			{
//////				m_Service_logger.Error ("Error in ranking caluclations", exc);
//////			}
//////			finally 
//////			{
//////				// Update the last notifications sent date
//////				string sqlUpdate = "UPDATE configuration SET " +
//////				" last_ranking_updated = @currentDateFromQuery";
//////
//////				Hashtable parameters = new Hashtable ();			
//////				parameters.Add ("@currentDateFromQuery", dtNow);
//////				GetDatabaseUtility ().ExecuteNonQuery (sqlUpdate, parameters); 
//////			}
//////
//////			return 0;
//////		}

		#endregion

		// **********************************************************************************************
		// Notification Functions
		// **********************************************************************************************
		#region Notification Functions

		/// <summary>
		/// SendNotifications
		/// </summary>
		/// <returns></returns>
		public static int SendNotifications ()
		{
			int communityId = 0;
			string channelName = "";
			string toEmail = "";
			int k = 0;
			bool bSentDaily = false;
			bool bSentWeekly = false;
            BlogFacade blogFacade = new BlogFacade();

			// Get the datetime of the last time notifications where sent
			DataRow drConfig = GetConfiguration ();

			if (drConfig == null)
			{
				m_Service_logger.Error ("No configuration data found");
				return -1;
			}

			DateTime dtLastNotification = Convert.ToDateTime (drConfig ["last_notifications_sent"]);
			DateTime dtLastDailyNotification = Convert.ToDateTime (drConfig ["last_daily_notifications_sent"]);
			DateTime dtLastWeeklyNotification = Convert.ToDateTime (drConfig ["last_weekly_notifications_sent"]);
			DateTime dtNow = Convert.ToDateTime (drConfig ["current_dateTime"]);

			try
			{
				// Go through all channels and send out the notifications for users that are subscribed
				DataTable dtCommunities = CommunityUtility.GetCommunitiesForNotifications ("", "", 1, Int32.MaxValue);
				PagedDataTable pdtCommunityUsers;
				int i = 0;

				// **************************************************************************
				// Individual Notifications
				// **************************************************************************
				for (i=0; i < dtCommunities.Rows.Count; i++)
				{
					communityId = Convert.ToInt32 (dtCommunities.Rows [i]["community_id"]);
					channelName = System.Web.HttpUtility.HtmlDecode (dtCommunities.Rows [i]["name"].ToString ());

					// Get all the community users who have notifications turned on
					pdtCommunityUsers = CommunityUtility.GetCommunityUsers (communityId, (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE, false, false, false, false, "cm.notifications = " + (int) Constants.eCOMMUNITY_NOTIFICATIONS.INDIVIDUAL_EMAIL + " AND u.status_id = " + (int) Constants.eUSER_STATUS.REGVALIDATED, "", 1, Int32.MaxValue);
					
                    //// 1) Now Playing Items
                    //PagedDataTable pdtAssets = StoreUtility.GetAssetsInChannel (communityId, true, false, 0, "last_updated_date > '" + GetDatabaseUtility ().FormatDateTime (dtLastNotification) + "'", "", 1, Int32.MaxValue);
                    //int assetId = 0;

                    //// 2) New Reviews
                    //PagedDataTable pdtAssetComments = StoreUtility.GetAssetCommentsByCommunityId (communityId, dtLastNotification, 1, Int32.MaxValue);

                    //// 3) New Topics
                    //PagedDataTable pdtForumTopics = ForumUtility.GetForumTopics (communityId, dtLastNotification, 1, Int32.MaxValue);

                    //// 4) Forum Replies
                    //PagedDataTable pdtThreads = ForumUtility.GetForumThreads (communityId, dtLastNotification, 1, Int32.MaxValue);

					// 5) Blogs
					//PagedDataTable dtBlogs = BlogUtility.GetBlogs (communityId, "b.last_updated_date > '" + GetDatabaseUtility ().FormatDateTime (dtLastNotification) + "'", "", "", 1, Int32.MaxValue);
                    PagedList<Blog> pgBlogs = blogFacade.GetBlogs(communityId, "b.last_updated_date > '" + GetDatabaseUtility().FormatDateTime(dtLastNotification) + "'", "", 1, Int32.MaxValue);

					// 6) Blog comments
                    PagedList<BlogComment> pdtBlogComments = blogFacade.GetBlogCommentsByCommunity(communityId, dtLastNotification, 1, Int32.MaxValue);

					// Loop through all the users
					for (int j=0; j < pdtCommunityUsers.Rows.Count; j++)
					{
						toEmail = pdtCommunityUsers.Rows [j]["email"].ToString ();

                        //// 1) Now Playing Items
                        //if (Convert.ToInt32 (pdtCommunityUsers.Rows [j]["item_notify"]).Equals (1))
                        //{
                        //    for (k=0; k < pdtAssets.Rows.Count; k++)
                        //    {
                        //        assetId = Convert.ToInt32 (pdtAssets.Rows [k]["asset_id"]);
                        //        MailUtility.SendNowPlayingNotification (toEmail, System.Web.HttpUtility.HtmlDecode (pdtAssets.Rows [k]["name"].ToString ()), assetId, channelName);
                        //    }
                        //}

                        //// 2) New Reviews
                        //if (Convert.ToInt32 (pdtCommunityUsers.Rows [j]["item_review_notify"]).Equals (1))
                        //{
                        //    for (k=0; k < pdtAssetComments.Rows.Count; k++)
                        //    {
                        //        assetId = Convert.ToInt32 (pdtAssetComments.Rows [k]["asset_id"]);
                        //        MailUtility.SendNowPlayingNotification (toEmail, System.Web.HttpUtility.HtmlDecode (pdtAssetComments.Rows [k]["name"].ToString ()), assetId, channelName);
                        //    }
                        //}
						
                        //// 3) Forums Posts
                        //if (Convert.ToInt32 (pdtCommunityUsers.Rows [j]["post_notify"]).Equals (1))
                        //{
                        //    for (k=0; k < pdtForumTopics.Rows.Count; k++)
                        //    {
                        //        MailUtility.SendForumTopicNotification (toEmail, System.Web.HttpUtility.HtmlDecode (pdtForumTopics.Rows [k]["forum_name"].ToString ()) + " - " + System.Web.HttpUtility.HtmlDecode (pdtForumTopics.Rows [k]["subject"].ToString ()), Convert.ToInt32 (pdtForumTopics.Rows [k]["topic_id"]), channelName, communityId);
                        //    }
                        //}

                        //// 4) Forum Replies
                        //if (Convert.ToInt32 (pdtCommunityUsers.Rows [j]["reply_notify"]).Equals (1))
                        //{
                        //    for (k=0; k < pdtThreads.Rows.Count; k++)
                        //    {
                        //        MailUtility.SendForumReplyNotification (toEmail, System.Web.HttpUtility.HtmlDecode (pdtThreads.Rows [k]["forum_name"].ToString ()) + " - " + System.Web.HttpUtility.HtmlDecode (pdtThreads.Rows [k]["subject"].ToString ()), Convert.ToInt32 (pdtThreads.Rows [k]["topic_id"]), channelName, communityId);
                        //    }
                        //}

						// 5) Blogs
						if (Convert.ToInt32 (pdtCommunityUsers.Rows [j]["blog_notify"]).Equals (1))
						{
                            foreach (Blog blog in pgBlogs)
                            {
                                MailUtility.SendBlogNotification(toEmail, System.Web.HttpUtility.HtmlDecode(blog.Subject), blog.BlogId, channelName, communityId);
                            }
						}

						// 6) Blog comments
						if (Convert.ToInt32 (pdtCommunityUsers.Rows [j]["blog_comment_notify"]).Equals (1))
						{
                            foreach (BlogComment blogComment in pdtBlogComments)
                            {
                                MailUtility.SendBlogCommentNotification(toEmail, System.Web.HttpUtility.HtmlDecode(blogComment.Subject), blogComment.BlogId, channelName, communityId);
                            }
						}
					}
				}

				// **************************************************************************
				// Daily Notifications
				// **************************************************************************
				// NOTE: Send out anytime we have not sent out within the past 1 day.
				if (dtNow.AddDays (-1).CompareTo (dtLastDailyNotification) > 0)
				{
					m_Service_logger.Info ("It is time to send daily notifications.");
					bSentDaily = true;

					for (i=0; i < dtCommunities.Rows.Count; i++)
					{
						communityId = Convert.ToInt32 (dtCommunities.Rows [i]["community_id"]);
						channelName = System.Web.HttpUtility.HtmlDecode (dtCommunities.Rows [i]["name"].ToString ());
						
                        //// 1) Daily - Now Playing Items
                        //PagedDataTable pdtDailyAssets = StoreUtility.GetAssetsInChannel (communityId, true, false, 0, "last_updated_date > '" + GetDatabaseUtility ().FormatDateTime (dtLastDailyNotification) + "'", "", 1, Int32.MaxValue);

                        //// 2 ) Daily - New Reviews
                        //PagedDataTable pdtAssetComments = StoreUtility.GetAssetCommentsByCommunityId (communityId, dtLastDailyNotification, 1, Int32.MaxValue);

                        //// 3) Daily Forum Topics
                        //PagedDataTable pdtForumTopics = ForumUtility.GetForumTopics (communityId, dtLastDailyNotification, 1, Int32.MaxValue);

                        //// 4) Forum Replies
                        //PagedDataTable pdtThreads = ForumUtility.GetForumThreads (communityId, dtLastDailyNotification, 1, Int32.MaxValue);

						// 5) Daily - Blogs
						//PagedDataTable dtBlogs = BlogUtility.GetBlogs (communityId, "b.last_updated_date > '" + GetDatabaseUtility ().FormatDateTime (dtLastDailyNotification) + "'", "", "", 1, Int32.MaxValue);
                        PagedList<Blog> pgBlogs = blogFacade.GetBlogs(communityId, "b.last_updated_date > '" + GetDatabaseUtility().FormatDateTime(dtLastDailyNotification) + "'", "", 1, Int32.MaxValue);

						// 6) Daily - Blog comments
                        PagedList<BlogComment> pdtBlogComments = blogFacade.GetBlogCommentsByCommunity(communityId, dtLastDailyNotification, 1, Int32.MaxValue);

                        //// Is there a need to loop through the users?
                        //if (
                        //    pdtForumTopics.Rows.Count > 0 || pdtThreads.Rows.Count > 0 ||
                        //    pgBlogs.Count > 0 || pdtBlogComments.Count > 0)
                        //{

                        //    // Get all the community users who have notifications turned on
                        //    pdtCommunityUsers = CommunityUtility.GetCommunityUsers (communityId, (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE, false, false, false, false, "cm.notifications = " + (int) Constants.eCOMMUNITY_NOTIFICATIONS.DAILY_EMAILS + " AND u.status_id = " + (int) Constants.eUSER_STATUS.REGVALIDATED, "", 1, Int32.MaxValue);

                        //    // Loop through all the users
                        //    for (int jj=0; jj < pdtCommunityUsers.Rows.Count; jj++)
                        //    {
                        //        toEmail = pdtCommunityUsers.Rows [jj]["email"].ToString ();
                        //        MailUtility.SendNotificationDailyDigest (toEmail, (DataRow) pdtCommunityUsers.Rows [jj],
                        //            pdtForumTopics, pdtThreads, pgBlogs, pdtBlogComments,  channelName, communityId);
                        //    }
                        //}
					}
				}

				// **************************************************************************
				// Weekly Notifications
				// **************************************************************************
				// NOTE: Send out anytime we have not sent out within the past 7 days.
				if (dtNow.AddDays (-7).CompareTo (dtLastDailyNotification) > 0)
				{
					m_Service_logger.Info ("It is time to send weekly notifications.");
					bSentWeekly = true;

					for (i=0; i < dtCommunities.Rows.Count; i++)
					{
						communityId = Convert.ToInt32 (dtCommunities.Rows [i]["community_id"]);
						channelName = System.Web.HttpUtility.HtmlDecode (dtCommunities.Rows [i]["name"].ToString ());
					
                        //// 1) Weekly - Now Playing Items
                        //PagedDataTable pdtWeeklyAssets = StoreUtility.GetAssetsInChannel (communityId, true, false, 0, "last_updated_date > '" + GetDatabaseUtility ().FormatDateTime (dtLastWeeklyNotification) + "'", "", 1, Int32.MaxValue);

                        //// 2) Weekly - New Reviews
                        //PagedDataTable pdtAssetComments = StoreUtility.GetAssetCommentsByCommunityId (communityId, dtLastWeeklyNotification, 1, Int32.MaxValue);

                        //// 3) Weekly Forum Topics
                        //PagedDataTable pdtForumTopics = ForumUtility.GetForumTopics (communityId, dtLastWeeklyNotification, 1, Int32.MaxValue);

                        //// 4) Forum Replies
                        //PagedDataTable pdtThreads = ForumUtility.GetForumThreads (communityId, dtLastWeeklyNotification, 1, Int32.MaxValue);

						// 5) Daily - Blogs
						//DataTable dtBlogs = BlogUtility.GetBlogs (communityId, "b.last_updated_date > '" + GetDatabaseUtility ().FormatDateTime (dtLastWeeklyNotification) + "'", "", "", 1, Int32.MaxValue);
                        PagedList<Blog> pgBlogs = blogFacade.GetBlogs(communityId, "b.last_updated_date > '" + GetDatabaseUtility().FormatDateTime(dtLastWeeklyNotification) + "'", "", 1, Int32.MaxValue);

						// 6) Daily - Blog comments
                        PagedList<BlogComment> pdtBlogComments = blogFacade.GetBlogCommentsByCommunity(communityId, dtLastWeeklyNotification, 1, Int32.MaxValue);

                        //// Is there a need to loop through the users?
                        //if (
                        //    pdtForumTopics.Rows.Count > 0 || pdtThreads.Rows.Count > 0 ||
                        //    pgBlogs.Count > 0 || pdtBlogComments.Count > 0)
                        //{

                        //    // Get all the community users who have notifications turned on
                        //    pdtCommunityUsers = CommunityUtility.GetCommunityUsers (communityId, (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE, false, false, false, false, "cm.notifications = " + (int) Constants.eCOMMUNITY_NOTIFICATIONS.WEEKLY_EMAILS + " AND u.status_id = " + (int) Constants.eUSER_STATUS.REGVALIDATED, "", 1, Int32.MaxValue);

                        //    // Loop through all the users
                        //    for (int jj=0; jj < pdtCommunityUsers.Rows.Count; jj++)
                        //    {
                        //        toEmail = pdtCommunityUsers.Rows [jj]["email"].ToString ();
                        //        MailUtility.SendNotificationDailyDigest (toEmail, (DataRow) pdtCommunityUsers.Rows [jj],
                        //            pdtForumTopics, pdtThreads, pgBlogs, pdtBlogComments,  channelName, communityId);
                        //    }
                        //}
					}

				}

				// 7) Private messages
				// These are sent out real time when the private message is sent.
				// See UsersUtility.InsertMessage () for implementation
			}
			catch (Exception exc)
			{
				m_Service_logger.Error ("Error sending email notifications", exc);
			}
			finally 
			{
				// Update the last notifications sent date
				string sqlUpdate = "UPDATE configuration SET " +
					" last_notifications_sent = @currentDateFromQuery";

				if (bSentDaily)
				{
					sqlUpdate += ",last_daily_notifications_sent = @currentDateFromQuery";
				}

				if (bSentWeekly)
				{
					sqlUpdate += ",last_weekly_notifications_sent = @currentDateFromQuery";
				}

				Hashtable parameters = new Hashtable ();			
				parameters.Add ("@currentDateFromQuery", dtNow);
				GetDatabaseUtility ().ExecuteNonQuery (sqlUpdate, parameters); 
			}


			return 0;
		}

		/// <summary>
		/// GetConfiguration
		/// </summary>
		/// <param name="messageId"></param>
		/// <returns></returns>
		private static DataRow GetConfiguration ()
		{
			string sqlSelect = "SELECT NOW() as current_dateTime, last_notifications_sent, last_daily_notifications_sent, last_weekly_notifications_sent, last_ranking_updated " +
				" FROM configuration ";

			return GetDatabaseUtility ().GetDataRow (sqlSelect, false);
		}

		#endregion


        // **********************************************************************************************
		// Update summary tables Functions
		// **********************************************************************************************
		#region Update summary tables Functions


		/// <summary>
		/// CallUpdateReportStats
		/// </summary>
		public static void CallUpdateReportStats ()
		{
			DatabaseUtility dbUtility = GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			// Update summary tables
			string sqlString = "CALL update_report_stats (CONCAT(DATE_SUB(DATE(NOW()),INTERVAL 1 DAY),' 00:00:00'),NOW())";
			dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// CallDatabaseCleanup
		/// </summary>
		public static void CallDatabaseCleanup ()
		{
			DatabaseUtility dbUtility = GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			// Clean summary tables
			string sqlString = "CALL _database_cleanup ()";
			dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

		#endregion

		// **********************************************************************************************
		// KDP Reward Functions
		// **********************************************************************************************
		#region KDP Reward Functions

        /// <summary>
        /// Give KDP rewards to daily winners and monthly winners.
        /// </summary>
        /// <param name="return_code_daily"> 0 success, otherwise error giving daily rewards</param>
        /// <param name="return_code_monthly"> 0 success, otherwise error giving monthly rewards</param>
        /// <returns></returns>
        public static void KDPRewards (ref int return_code_daily, ref int return_code_monthly)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "CALL wok.ddrRewards(@rc_daily, @rc_monthly); SELECT CAST(@rc_daily as UNSIGNED INT) as rc_daily, CAST(@rc_monthly as UNSIGNED INT) as rc_monthly;";
            Hashtable parameters = new Hashtable ();

            DataRow dr = dbUtility.GetDataRow (sqlSelect, parameters, false);

            return_code_daily = Convert.ToInt32 (dr["rc_daily"]);
			return_code_monthly = Convert.ToInt32 (dr["rc_monthly"]);
        }

        /// <summary>
        /// Send out private messages and blasts to KDP award winners.
        /// </summary>
        /// <returns></returns>
        public static void KDPRewardRecognition (int fromIdMsg, string subjectPrivateMsg, string bodyPrivateMsg,
												 string subjectFriendBlast, string bodyFriendBlast,
												 string subjectMonthlyBlast, string bodyMonthlyBlast, DateTime blastExpires)
        {
			KDPRewardRecognitionDaily( fromIdMsg, subjectPrivateMsg, bodyPrivateMsg, subjectFriendBlast, bodyFriendBlast);

			KDPRewardRecognitionMonthly( fromIdMsg, subjectMonthlyBlast, bodyMonthlyBlast, blastExpires);
        }

        /// <summary>
        /// Send a private message to each winner
		/// Send a blast to the each winner's friends
        /// </summary>
        /// <returns></returns>
		private static void KDPRewardRecognitionDaily (int fromIdMsg, string subjectPrivateMsg, string bodyPrivateMsg, string subjectFriendBlast, string bodyFriendBlast)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlSelect = "SELECT user_id, daily_reward, daily_place, player_name, daily_kei_point_id " +
							   "FROM wok.ddr_scores_archive " +
							   "WHERE created_date <= CONCAT(DATE(NOW()), ' 00:00:00') AND created_date >= CONCAT(DATE(DATE_SUB(NOW(), INTERVAL 1 DAY)), ' 00:00:01') " +
							   "AND daily_reward_result = 0";

			DataTable dt = dbUtility.GetDataTable (sqlSelect);

			int rowCount = dt.Rows.Count;

            string savedSubjectFriendBlast = subjectFriendBlast;
            string savedBodyPrivateMsg = bodyPrivateMsg;

			for ( int ii = 0; ii < rowCount; ii++)
			{
                subjectFriendBlast = savedSubjectFriendBlast;
                bodyPrivateMsg = savedBodyPrivateMsg;

				int userId = (int)dt.Rows[ii]["user_id"];
				int dailyReward = (int)dt.Rows[ii]["daily_reward"];
				int dailyPlace = (int)dt.Rows[ii]["daily_place"];
				string daily_kei_point_id = (string)dt.Rows[ii]["daily_kei_point_id"];
				string playerName = (string)dt.Rows[ii]["player_name"];


				string place = Convert.ToString(dailyPlace);

				switch (dailyPlace)
				{
				case 1:
					place += "st";
					break;
				case 2:
					place += "nd";
					break;
				case 3:
					place += "rd";
					break;
				default:
					place += "th";
					break;
				}

				bodyPrivateMsg = bodyPrivateMsg.Replace("[place]", place);
				bodyPrivateMsg = bodyPrivateMsg.Replace("[reward]", Convert.ToString(dailyReward));

				if ( daily_kei_point_id.Equals("GPOINT") )
				{
					bodyPrivateMsg = bodyPrivateMsg.Replace("[point_type]", "rewards");
				}
				else if ( daily_kei_point_id.Equals("KPOINT") )
				{
					bodyPrivateMsg = bodyPrivateMsg.Replace("[point_type]", "credits");
				}

                // Insert a private message
                BlastFacade blastFacade = new BlastFacade();
                UserFacade userFacade = new UserFacade();

                Message message = new Message(0, fromIdMsg, userId, subjectPrivateMsg,
                    bodyPrivateMsg, new DateTime(), 0, 0, 0, "U", "S");

                userFacade.InsertMessage(message);

				subjectFriendBlast = subjectFriendBlast.Replace("[username]", playerName);
				subjectFriendBlast = subjectFriendBlast.Replace("[place]", place);
				subjectFriendBlast = subjectFriendBlast.Replace("[reward]", Convert.ToString(dailyReward));

				if ( daily_kei_point_id.Equals("GPOINT") )
				{
					subjectFriendBlast = subjectFriendBlast.Replace("[point_type]", "rewards");
				}
				else if ( daily_kei_point_id.Equals("KPOINT") )
				{
					subjectFriendBlast = subjectFriendBlast.Replace("[point_type]", "credits");
				}

                blastFacade.SendFriendPassiveBlast (BlastTypes.AWARD, userId, playerName, subjectFriendBlast, bodyFriendBlast);
			}
		}

        /// <summary>
        /// Send a private message to each winner
		/// Send a blast to the each winner's friends
        /// </summary>
        /// <returns></returns>
		private static void KDPRewardRecognitionMonthly (int fromIdMsg, string subject, string body, DateTime blastExpires)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlSelect = "SELECT user_id, monthly_reward, monthly_place, player_name, monthly_kei_point_id, score " +
							   "FROM wok.ddr_scores_archive " +
							   "WHERE created_date <= CONCAT(DATE(DATE_SUB(NOW(), INTERVAL DAYOFMONTH(NOW())-1 DAY)), ' 00:00:00') AND " +
							   "created_date >= CONCAT(YEAR(DATE(DATE_SUB(NOW(), INTERVAL 1 MONTH))),'-',MONTH(DATE(DATE_SUB(NOW(), INTERVAL 1 MONTH))),'-','01 00:00:00') " +
							   "AND monthly_reward_result = 0 " +
                               "ORDER BY score DESC";

			DataTable dt = dbUtility.GetDataTable (sqlSelect);

			int rowCount = dt.Rows.Count;

			body += "\r\n";

			for ( int ii = 0; ii < rowCount; ii++)
			{
				int userId = (int)dt.Rows[ii]["user_id"];
				int monthlyReward = (int)dt.Rows[ii]["monthly_reward"];
				int monthlyPlace = (int)dt.Rows[ii]["monthly_place"];
				string monthly_kei_point_id = (string)dt.Rows[ii]["monthly_kei_point_id"];
				string playerName = (string)dt.Rows[ii]["player_name"];

				string place = Convert.ToString(monthlyPlace);

				switch (monthlyPlace)
				{
				case 1:
					place += "st";
					break;
				case 2:
					place += "nd";
					break;
				case 3:
					place += "rd";
					break;
				default:
					place += "th";
					break;
				}

				body += "\r\n";
				body += place;
				body += " place ";
				body += playerName;
				body += " with ";
				body += monthlyReward;

				if ( monthly_kei_point_id.Equals("GPOINT") )
				{
					body += " rewards";
				}
				else if ( monthly_kei_point_id.Equals("KPOINT") )
				{
					body += " credits";
				}
			}

			if (rowCount > 0)
			{
                BlastFacade blastFacade = new BlastFacade();
                blastFacade.SendAdminBlast(fromIdMsg, blastExpires, subject, body, "", "", false);
			}

		}

		
		#endregion


		/// <summary>
		/// Get the database utility
		/// </summary>
		/// <returns></returns>
		public static DatabaseUtility GetDatabaseUtility ()
		{
			if (m_DatabaseUtilty == null)
			{
				switch (KanevaGlobals.DatabaseType)
				{
					case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
					{
						m_DatabaseUtilty = new SQLServerUtility (KanevaGlobals.ConnectionString);
						break;
					}
					case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
					{
						m_DatabaseUtilty = new MySQLUtility (KanevaGlobals.ConnectionString);
						break;
					}
					default:
					{
						throw new Exception ("This database type is not supported");
					}
				}
			}
			return m_DatabaseUtilty;
		}

		/// <summary>
		/// DataBase Utility for Master Database
		/// </summary>
		private static DatabaseUtility m_DatabaseUtilty;

		public static readonly int MAX_NUM_FILES_IN_TORRENT;

		/// <summary>
		/// tcc email notifications are sent to this email address
		/// </summary>
		public static readonly string TCC_EMAIL_RECIPIENT;

		private static readonly ILog m_Service_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		//protected static AzureusUtility AZUREUS = new AzureusUtility ();
	}
}
