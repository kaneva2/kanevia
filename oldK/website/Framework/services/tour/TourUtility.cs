///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Text;
using System.Web.Security;
using System.Web;
using System.Web.Handlers;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Net;
using System.IO;
using System.Xml;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.Tour
{
    public class TourUtility
    {
        private static int nBlasted = 0;
        //private static DateTime estimatedTourEndTime;

        public static string OpenUrl(string url, CookieContainer myContainer, string myMethod)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            if (myContainer != null)
            {
                request.CookieContainer = myContainer;
            }
            request.Method = myMethod;
            HttpWebResponse response = (HttpWebResponse) request.GetResponse();

            string restring = response.StatusCode.ToString();
            response.Close();

            return restring;         
        }

        public static int SendInWorldBroadcast( string msg )
        {
            //auth to cookie
            CookieContainer myContainer = new CookieContainer();
            string wokweburl = System.Configuration.ConfigurationManager.AppSettings["WokWebServer"];
            string url = string.Format("{0}{1}", wokweburl, "/kgp/validateuser.aspx?user=kaneva&pw=wokandwoll989");
            string ret = OpenUrl(url, myContainer, "GET");
            
            //send the broadcast
            string urlmsg = HttpUtility.UrlEncode(msg);
            url = string.Format("{0}/kgp/broadcast.aspx?msg={1}", wokweburl, urlmsg); 
            ret = OpenUrl(url, myContainer, "GET");
           
            return 1;
        }

        public static int UpdateDailyWinners()
        {
            int user = 0;
            int packet = 0;
            int tourid = 0;
            
            //only do this past 11 and call the sp which will return the tours 
            //that will not run again until the next day.
            if (DateTime.Now.Hour < 23)
                return 0;
            
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            //Hashtable parameters = new Hashtable();
            
            FameFacade fFacade = new FameFacade();

            //get the list of people needing to be awarded in the tour_pending_awards, joining to the packet awards for the packet id
            string strQuery = "SELECT tpa.kaneva_user_id user, twr.fame_packet_id packet, tpa.running_tour_id tourid FROM tour.tour_pending_awards tpa, tour.tour_winner_rewards twr WHERE tpa.awarded = 0 AND tpa.place = twr.place_id";
            DataTable datatable = dbUtility.GetDataTable(strQuery);
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                DataRow row = datatable.Rows[i];
                user   = int.Parse(row["user"].ToString());
                packet = int.Parse(row["packet"].ToString());
                tourid = int.Parse(row["tourid"].ToString());

                //for each record call the fame facade
                int famereturn = fFacade.RedeemPacket(user, packet, 1);
                if (famereturn == 0)
                {
                    famereturn = 1;
                }

                strQuery = "UPDATE tour.tour_pending_awards SET awarded = @famereturn WHERE running_tour_id = @tourid AND kaneva_user_id = @userid";
                strQuery = strQuery.Replace("@famereturn", famereturn.ToString());
                strQuery = strQuery.Replace("@userid", user.ToString());
                strQuery = strQuery.Replace("@tourid", tourid.ToString());
                dbUtility.ExecuteNonQuery(strQuery);
            }

            strQuery = "CALL tour.tour_dailywinners()";                       
            DataRow data = dbUtility.GetDataRow(strQuery, false);
            if (data != null)
            {
                string field1 = data[0].ToString();
                return 1;
            }
            else
                return 0;
        }

        public static int UpdateTour()
        {
            //check to see if we need to reward the last tour of the day
            UpdateDailyWinners();
            
            // schedule the next tour
            // call the stored proc and check the return codes for the
            // status of the current tour.
            string strQuery = "CALL tour.tour_manager()";
            int minsleft = 0;
            int tourid = -1;

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            Hashtable parameters = new Hashtable();

            DataRow data = dbUtility.GetDataRow(strQuery, false);
            if (data != null)
            {
                tourid   = int.Parse(data[0].ToString());
                minsleft = int.Parse(data[1].ToString()) * -1;
                if (nBlasted == 0 && minsleft <= 10 && tourid > 0)
                {
                    BlastFacade bFacade = new BlastFacade();
                    DateTime expdate = DateTime.Now; expdate.AddMinutes(minsleft);
                    String msg = string.Format("Next tour starts in {0:D} Minutes, visit the Tour Guide in the City to sign up!", minsleft);

                    bFacade.SendAdminBlast(24, expdate, "Tour Guide blasts <a href=\"{0}\">{1}</a>", msg, "", "", true);

                    SendInWorldBroadcast( msg );
                    nBlasted = 1;
                }
                else if (minsleft > 10)
                    nBlasted = 0;

                return 1;
            }
            else
                return 0;
        }
    }
}
