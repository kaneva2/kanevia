///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for BreadCrumb.
	/// </summary>
	public class BreadCrumb
	{
		public BreadCrumb (string text, string hyperlink, string sort, int pageNumber)
		{
			m_Text = text;
			m_Hyperlink = hyperlink;
			m_Sort = sort;
			m_PageNumber = pageNumber;
		}

		public BreadCrumb (string text, string hyperlink, string sort, int pageNumber, string sortOrder, string filter)
			: this (text, hyperlink, sort, pageNumber)
		{
			m_SortOrder = sortOrder;
			m_Filter = filter;
		}

		public string Text
		{
			get 
			{
				return m_Text;
			}
		}

		public string Hyperlink
		{
			get 
			{
				return m_Hyperlink;
			}
		}
		public string Sort
		{
			get 
			{
				return m_Sort;
			}
		}

		public int PageNumber
		{
			get 
			{
				return m_PageNumber;
			}
		}

		public string SortOrder
		{
			get 
			{
				return m_SortOrder;
			}
		}

		public string Filter
		{
			get 
			{
				return m_Filter;
			}
		}

		private string m_Text;
		private string m_Hyperlink;
		private string m_Sort;
		private int m_PageNumber;
		private string m_SortOrder = "";
		private string m_Filter = "";
	}
}
