///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for AdvertisementDirectory.
	/// </summary>
	public class AdvertisementDirectory : AdvertisementItem
	{
		// Fields
		private bool recurse = false;

		/// <summary>
		/// Gets or sets a flag indicating whether or not to include all files in any sub-directories 
		/// when evaluating a request.
		/// </summary>
		public bool Recurse
		{
			get { return recurse; }
			set { recurse = value; }
		}

		/// <summary>
		/// Creates an instance with default values.
		/// </summary>
		public AdvertisementDirectory () : base()
		{
		}

		/// <summary>
		/// Creates an instance with initial values.
		/// </summary>
		/// <param name="path">The relative path to the directory or file.</param>
		/// <param name="ignore">A flag to ignore security for the directory or file.</param>
		public AdvertisementDirectory (string path, bool showAd, bool recurse) : base(path, showAd)
		{
			this.recurse = recurse;
		}

		/// <summary>
		/// Creates an instance with an initial path value.
		/// </summary>
		/// <param name="path">The relative path to the directory or file.</param>
		public AdvertisementDirectory (string path) : base(path)
		{
		}
	}
}
