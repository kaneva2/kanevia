///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for AdvertisementDirectoryCollection.
	/// </summary>
	public class AdvertisementDirectoryCollection : AdvertisementItemCollection
	{
		/// <summary>
		/// Initialize an instance of this collection.
		/// </summary>
		public AdvertisementDirectoryCollection () : base()
	{
	}

		/// <summary>
		/// Indexer for the collection.
		/// </summary>
		public AdvertisementDirectory this [int index]
		{
			get { return (AdvertisementDirectory) List[index]; }
		}

		/// <summary>
		/// Adds the item to the collection.
		/// </summary>
		/// <param name="item">The item to add.</param>
		public int Add (AdvertisementDirectory item)
		{
			return List.Add(item);
		}

		/// <summary>
		/// Inserts an item into the collection at the specified index.
		/// </summary>
		/// <param name="index">The index to insert the item at.</param>
		/// <param name="item">The item to insert.</param>
		public void Insert (int index, AdvertisementDirectory item)
		{
			List.Insert(index, item);
		}

		/// <summary>
		/// Removes an item from the collection.
		/// </summary>
		/// <param name="item">The item to remove.</param>
		public void Remove (AdvertisementDirectory item)
		{
			List.Remove(item);
		}
	}
}
