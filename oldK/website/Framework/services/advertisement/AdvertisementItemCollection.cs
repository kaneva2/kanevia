///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for AdvertisementItemCollection.
	/// </summary>
	public class AdvertisementItemCollection : CollectionBase
	{
		/// <summary>
		/// Initialize an instance of this collection.
		/// </summary>
		public AdvertisementItemCollection ()
		{
		}

		/// <summary>
		/// Returns the index of a specified item in the collection.
		/// </summary>
		/// <param name="item">The item to find.</param>
		/// <returns>Returns the index of the item.</returns>
		public int IndexOf(AdvertisementItem item)
		{
			return List.IndexOf(item);
		}

		/// <summary>
		/// Returns the index of an item with the specified path in the collection.
		/// </summary>
		/// <param name="Path">The path of the item to find.</param>
		/// <returns>Returns the index of the item with the path.</returns>
		public int IndexOf(string path)
		{
			// Create a comparer for sorting and searching
			AdvertisementItemComparer Comparer = new AdvertisementItemComparer ();
			InnerList.Sort(Comparer);
			return InnerList.BinarySearch(path, Comparer);
		}

	}
}
