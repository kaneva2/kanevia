///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Specialized;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// The different modes supported for the AdvertisingMode configuration section.
	/// </summary>
	public enum AdvertisingMode
	{
		/// <summary>
		/// Indicates that Advertising is off
		/// </summary>
		Off,

		/// <summary>
		///  Indicates that Advertising is Testing
		/// </summary>
		Testing,

		/// <summary>
		///  Indicates that Advertising is Live
		/// </summary>
		Live
	}

	/// <summary>
	/// Summary description for AdvertisementSettings.
	/// </summary>
	public class AdvertisementSettings
	{
		public AdvertisementSettings()
		{
			// Create the collections
			directories = new AdvertisementDirectoryCollection ();
			files = new AdvertisementFileCollection ();
		}

		/// <summary>
		/// Gets the collection of directories read from the configuration section.
		/// </summary>
		public AdvertisementDirectoryCollection Directories
		{
			get { return directories; }
		}

		/// <summary>
		/// Gets the collection of files read from the configuration section.
		/// </summary>
		public AdvertisementFileCollection Files
		{
			get { return files; }
		}

		/// <summary>
		/// Gets or sets the mode indicating how the advertising settings handled.
		/// </summary>
		public AdvertisingMode Mode
		{
			get { return mode; }
			set { mode = value; }
		}

		private AdvertisingMode mode = AdvertisingMode.Off;
		private AdvertisementDirectoryCollection directories;
		private AdvertisementFileCollection files;
	}


}
