///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for AdvertisementFile.
	/// </summary>
	public class AdvertisementFile : AdvertisementItem
	{
		/// <summary>
		/// Creates an instance with default values.
		/// </summary>
		public AdvertisementFile() : base()
		{
		}

		public AdvertisementFile (string path, bool showAd, string queryParam, string queryParamValue) : base(path, showAd, queryParam, queryParamValue)
		{
		}

		
		/// <summary>
		/// Creates an instance with initial values.
		/// </summary>
		/// <param name="path">The relative path to the directory or file.</param>
		/// <param name="showAd">A flag to showAd for the directory or file.</param>
		public AdvertisementFile (string path, bool showAd) : base(path, showAd)
		{
		}

		/// <summary>
		/// Creates an instance with an initial path value.
		/// </summary>
		/// <param name="path">The relative path to the directory or file.</param>
		public AdvertisementFile (string path) : base(path)
		{
		}
	}
}
