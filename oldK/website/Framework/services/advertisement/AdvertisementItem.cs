///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;

namespace KlausEnt.KEP.Kaneva
{

	/// <summary>
	/// Summary description for AdvertisementItem.
	/// </summary>
	public class AdvertisementItemComparer : IComparer
	{
		/// <summary>
		/// Initialize an instance of this class.
		/// </summary>
		public AdvertisementItemComparer ()
		{
		}

		/// <summary>
		/// Compares the two objects as string and AdvertisementItem or both AdvertisementItem 
		/// by the Path property.
		/// </summary>
		/// <param name="x">First object to compare.</param>
		/// <param name="y">Second object to compare.</param>
		/// <returns></returns>
		public int Compare(object x, object y)
		{
			// Check the type of the parameters
			if (!(x is AdvertisementItem) && !(x is string))
				// Throw an exception for the first argument
				throw new ArgumentException("Parameter must be a AdvertisementItem or a String.", "x");
			else if (!(y is AdvertisementItem) && !(y is string))
				// Throw an exception for the second argument
				throw new ArgumentException("Parameter must be a AdvertisementItem or a String.", "y");

			// Initialize the path variables
			string xPath = string.Empty;
			string yPath = string.Empty;

			// Get the path for x
			if (x is AdvertisementItem)
				xPath = ((AdvertisementItem) x).Path;
			else
				xPath = (string) x;

			// Get the path for y
			if (y is AdvertisementItem)
				yPath = ((AdvertisementItem) y).Path;
			else
				yPath = (string) y;

			// Compare the paths, ignoring case
			return string.Compare(xPath, yPath, true);
		}
	}


	/// <summary>
	/// The AdvertisementItemItem class is the base class that represents entries in the &lt;AdvertisementItemPages&gt;
	/// configuration section.
	/// </summary>
	public class AdvertisementItem
	{
		// Fields
		private bool showAd = false;
		private string path = string.Empty;
		private string queryParam = string.Empty;
		private string queryParamValue = string.Empty;

		/// <summary>
		/// Gets or sets the type of security for this directory or file.
		/// </summary>
		public bool ShowAd
		{
			get { return showAd; }
			set { showAd = value; }
		}

		/// <summary>
		/// Gets or sets the path of this directory or file.
		/// </summary>
		public string Path
		{
			get { return path; }
			set { path = value; }
		}

		/// <summary>
		/// Gets or sets the query param of this directory or file.
		/// </summary>
		public string QueryParam
		{
			get { return queryParam; }
			set { queryParam = value; }
		}

		/// <summary>
		/// Gets or sets the query param value of this directory or file.
		/// </summary>
		public string QueryParamValue
		{
			get { return queryParamValue; }
			set { queryParamValue = value; }
		}

		/// <summary>
		/// Creates an instance of this class.
		/// </summary>
		public AdvertisementItem ()
		{
		}

		public AdvertisementItem (string path, bool showAd, string queryParam, string queryParamValue)
		{
			// Initialize the path and secure properties
			this.path = Path;
			this.showAd = showAd;
			this.queryParam = queryParam;
			this.queryParamValue = queryParamValue;
		}

		/// <summary>
		/// Creates an instance with initial values.
		/// </summary>
		/// <param name="path">The relative path to the directory or file.</param>
		/// <param name="ignore">A flag to ignore security for the directory or file.</param>
		public AdvertisementItem (string path, bool showAd)
		{
			// Initialize the path and secure properties
			this.path = Path;
			this.showAd = showAd;
		}

		/// <summary>
		/// Creates an instance with an initial path value.
		/// </summary>
		/// <param name="path">The relative path to the directory or file.</param>
		public AdvertisementItem (string path) : this (path, false)
		{
		}

	}
}
