///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Xml;

// Import log4net classes.
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for AdvertisementSectionHandler.
	/// </summary>
	public class AdvertisementSectionHandler : IConfigurationSectionHandler
	{
		public AdvertisementSectionHandler()
		{
		}

		public object Create (object parent, object configContext, System.Xml.XmlNode section)
		{
			// Create a SecureWebPageSettings object for the settings in this section
			AdvertisementSettings Settings = new AdvertisementSettings ();
			
			// Read the general settings
			ReadGeneralSettings (section, Settings);

			// Traverse the child nodes
			foreach (XmlNode Node in section.ChildNodes)
			{
				if (Node.NodeType == System.Xml.XmlNodeType.Comment)
					continue;
				else if (Node.Name.ToLower() == "directory")
					// This is a directory path node
					Settings.Directories.Add (ReadDirectoryItem(Node));
				else if (Node.Name.ToLower () == "file")
					// This is a file path node
					Settings.Files.Add (ReadFileItem(Node));
				else
					// Invalid setting
					m_logger.Error (string.Format("'{0}' is not an acceptable setting.", Node.Name));
			}

			// Return the settings
			return Settings;
		}

		/// <summary>
		/// Reads general settings from the secureWebPages section into the given AdvertisementSettings instance.
		/// </summary>
		/// <param name="section">The XmlNode to read from.</param>
		/// <param name="settings">The AdvertisementSettings instance to set.</param>
		protected void ReadGeneralSettings(XmlNode section, AdvertisementSettings settings)
		{
			// Get the mode attribute
			if (section.Attributes["mode"] != null)
			{
				switch (section.Attributes["mode"].Value.ToLower())
				{
					case "off":
						settings.Mode = AdvertisingMode.Off;
						break;

					case "testing":
						settings.Mode = AdvertisingMode.Testing;
						break;

					case "live":
						settings.Mode = AdvertisingMode.Live;
						break;

					default:
						m_logger.Error ("Invalid value for the 'mode' attribute.");
						break;
				}
			}
		}
		/// <summary>
		/// Reads the typical attributes for a AdvertisementItem from the configuration node.
		/// </summary>
		/// <param name="node">The XmlNode to read from.</param>
		/// <param name="item">The AdvertisementItem to set values for.</param>
		protected void ReadChildItem(XmlNode node, AdvertisementItem item)
		{
				// Set the item only if the node has a valid path attribute value
			if (node.Attributes["path"] != null && node.Attributes["path"].Value.Trim().Length > 0)
			{
				// Get the value of the path attribute
				item.Path = node.Attributes["path"].Value.Trim().ToLower();

				// Add leading and trailing "/" characters where needed
				if (item.Path.Length > 1)
				{
					// Leading "/"
					if (!item.Path.StartsWith("/"))
						item.Path = "/" + item.Path;

					// Trailing "/" only if this is a directory item
					if (item is AdvertisementDirectory && !item.Path.EndsWith("/"))
						item.Path += "/";
				}

				// Check for a showAd attribute
				if (node.Attributes["showAd"] != null)
				{
					switch (node.Attributes["showAd"].Value.Trim().ToLower())
					{
						case "true":
							item.ShowAd = true;
							break;

						case "false":
							item.ShowAd = false;
							break;

						default:
							m_logger.Error ("Invalid value for the 'ShowAd' attribute.");
							break;
					}
				}

				if (node.Attributes["queryParam"] != null)
				{
					item.QueryParam = node.Attributes["queryParam"].Value.Trim().ToLower();
				}

				if (node.Attributes["queryParamValue"] != null)
				{
					item.QueryParamValue = node.Attributes["queryParamValue"].Value.Trim().ToLower();
				}
			}
			else
				// Throw an exception for the missing Path attribute
				m_logger.Error ("'path' attribute not found.");
		}

		/// <summary>
		/// Reads a directory item from the configuration node and returns a new instance of AdvertisementDirectory.
		/// </summary>
		/// <param name="node">The XmlNode to read from.</param>
		/// <returns>A AdvertisementDirectory initialized with values read from the node.</returns>
		protected AdvertisementDirectory ReadDirectoryItem (XmlNode node)
		{
			// Create a SecureWebPageDirectory instance
			AdvertisementDirectory Item = new AdvertisementDirectory ();

			// Read the typical attributes
			ReadChildItem (node, Item);

			// Check for a recurse attribute
			if (node.Attributes["recurse"] != null)
				Item.Recurse = (node.Attributes["recurse"].Value.ToLower() == "true");
			
			return Item;
		}

		/// <summary>
		/// Reads a file item from the configuration node and returns a new instance of AdvertisementFile.
		/// </summary>
		/// <param name="node">The XmlNode to read from.</param>
		/// <returns>A AdvertisementFile initialized with values read from the node.</returns>
		protected AdvertisementFile ReadFileItem(XmlNode node)
		{
			// Create a AdvertisementFile instance
			AdvertisementFile Item = new AdvertisementFile();

			// Read the typical attributes
			ReadChildItem(node, Item);

			return Item;
		}

		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

	}
}
