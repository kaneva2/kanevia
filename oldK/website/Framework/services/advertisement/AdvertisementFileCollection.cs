///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for AdvertisementFileCollection.
	/// </summary>
	public class AdvertisementFileCollection : AdvertisementItemCollection
	{
		/// <summary>
		/// Initialize an instance of this collection.
		/// </summary>
		public AdvertisementFileCollection () : base()
	{
	}

		/// <summary>
		/// Indexer for the collection.
		/// </summary>
		public AdvertisementFile this [int index]
		{
			get { return (AdvertisementFile) List[index]; }
		}

		/// <summary>
		/// Adds the item to the collection.
		/// </summary>
		/// <param name="item">The item to add.</param>
		public int Add (AdvertisementFile item)
		{
			return List.Add(item);
		}

		/// <summary>
		/// Inserts an item into the collection at the specified index.
		/// </summary>
		/// <param name="index">The index to insert the item at.</param>
		/// <param name="item">The item to insert.</param>
		public void Insert(int index, AdvertisementFile item)
		{
			List.Insert(index, item);
		}

		/// <summary>
		/// Removes an item from the collection.
		/// </summary>
		/// <param name="item">The item to remove.</param>
		public void Remove(AdvertisementFile item)
		{
			List.Remove(item);
		}
	}
}
