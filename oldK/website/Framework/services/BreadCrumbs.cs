///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for BreadCrumbs.
	/// </summary>
	public class BreadCrumbs : CollectionBase
	{
		public BreadCrumbs()
		{
		}

		// Method implementation from the CollectionBase class
		public void Add (BreadCrumb breadCrumb)
		{     
			List.Add (breadCrumb);
		}       

		// Method implementation from the CollectionBase class
		public void Remove (int index)
		{
			// Check to see if there is a Leaf at the supplied index.
			if (index > Count - 1 || index < 0) 
			{
				// Handle the error that occurs if the valid page index is       
				// not supplied.    
				// This exception will be written to the calling function             
				throw new Exception ("Index out of bounds");            
			}        
			List.RemoveAt (index);       
		} 

		// Method implementation from the CollectionBase class
		public BreadCrumb Item (int Index) 
		{     
			// The appropriate item is retrieved from the List object and     
			// explicitly cast to the Leaf type, then returned to the      
			// caller.     
			return (BreadCrumb) List[Index]; 
		}

        public BreadCrumb FindByText(string breadCrumbText)
        {
            int found = 0;
            BreadCrumb requestedCrumb = null;
            if ((breadCrumbText != null) && (breadCrumbText != ""))
            {
                while (found >= 0 && found < List.Count)
                {
                    //increment is previous to if to prevent teh reset of the break value
                    if (breadCrumbText.Equals(((BreadCrumb)List[found]).Text))
                    {
                        requestedCrumb = (BreadCrumb)List[found];
                        found = -1;
                    }
                    else
                    {
                        found++;
                    }
                }
            }

            return requestedCrumb;
        }
    }
}

