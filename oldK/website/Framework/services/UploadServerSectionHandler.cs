///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.Xml;

// Import log4net classes.
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for UploadServerSectionHandler.
	/// </summary>
	public class UploadServerSectionHandler : IConfigurationSectionHandler
	{
		public UploadServerSectionHandler()
		{
		}

		public object Create (object parent, object configContext, System.Xml.XmlNode section)
		{
			XmlNodeList contentServerSettings;
			
			System.Collections.Hashtable htUploadServers = new System.Collections.Hashtable ();

			contentServerSettings = section.SelectNodes ("uploadServers//uploadServer");

			foreach( XmlNode nodeTracker in contentServerSettings)
			{
				try
				{
					ContentServer uploadServer = new ContentServer (nodeTracker.Attributes.GetNamedItem ("IP").Value, 0, 0);

					htUploadServers.Add (nodeTracker.Attributes.GetNamedItem ("IP").Value, uploadServer);
				}
				catch (Exception exc)
				{
					m_logger.Error ("Error reading upload server configuration", exc);
				}

			}

			return htUploadServers;
		}

		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

	}

}