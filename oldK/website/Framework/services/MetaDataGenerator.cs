///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Text;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for MetaDataGenerator.
	/// </summary>
	public class MetaDataGenerator
	{
		public MetaDataGenerator()
		{
		}

		/// <summary>
		/// sets or gets meta data keyword html
		/// </summary>
		//generates the Title header
		public static string GenerateTitle (string name, string pageName, int pageType)
		{
			string title = "";

			//generate title based on page type
			switch(pageType)
			{
				case (int) Constants.eWEB_PAGE_TYPE.COMMUNITY:
				case (int) Constants.eWEB_PAGE_TYPE.PROFILE:
					
					//append current page
					if (pageName.Length > 0)
					{
						title = name + " - " + pageName;
					}
					else
					{
						title = name;
					}
					break;

				case (int) Constants.eWEB_PAGE_TYPE.MEDIA:
					title = name;
					break;
			}

			//truncate if necessary
			return KanevaGlobals.Truncate (title, 65);
		}

        public static string GenerateMetaKeywords(string name, NameValueCollection list, int pageType, bool stringOnly)
        {
            StringBuilder strKeyword;

            if(stringOnly)
            {
                strKeyword = new StringBuilder();
            }else
            {
                strKeyword = new StringBuilder("<meta name=\"keywords\" content=\"");
            } 

            switch (pageType)
            {
                case (int)Constants.eWEB_PAGE_TYPE.PROFILE:
                case (int)Constants.eWEB_PAGE_TYPE.COMMUNITY:

                    strKeyword.Append(name);
                    for (int i = 0; i < list.Count; i++)
                    {
                        strKeyword.Append(", " + list[i]);
                    }
                    break;

                case (int)Constants.eWEB_PAGE_TYPE.MEDIA:

                    for (int i = 0; i < list.Count; i++)
                    {
                        if (i > 0)
                        {
                            strKeyword.Append(", " + list[i]);
                        }
                        else
                        {
                            strKeyword.Append(list[i]);
                        }
                    }
                    break;
            }

            if (!stringOnly)
            {
                strKeyword.Append("\" />");
            }
            return strKeyword.ToString();
        }

		/// <summary>
		/// sets or gets meta data keyword html
		/// </summary>
		/// 
		//generates the Keywords meta data
		public static string GenerateMetaKeywords (string name, NameValueCollection list, int pageType)
		{
            return GenerateMetaKeywords(name, list, pageType, false);
		}

        public static string GenerateMetaDescription(string name, NameValueCollection list, int pageType, bool stringOnly)
        {
            StringBuilder strDescription = new StringBuilder();

            switch (pageType)
            {
                case (int)Constants.eWEB_PAGE_TYPE.COMMUNITY:
                    strDescription.Append("Kaneva's " + name + " Community includes ");

                    int itemCount = list.Count;
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (i == 0)
                        {
                            strDescription.Append(list[i]);
                        }
                        else if (i == itemCount - 1)
                        {
                            strDescription.Append(", and " + list[i] + ".");
                        }
                        else
                        {
                            strDescription.Append(", " + list[i]);
                        }
                    }

                    strDescription.Append(" Join now. More...");
                    break;

                case (int)Constants.eWEB_PAGE_TYPE.PROFILE:
                    strDescription.Append("Kaneva Profile of " + name + ". ");

                    string age = list["age"];
                    string gender = list["gender"];

                    if (KanevaGlobals.IsNumeric(age))
                    {
                        if (Convert.ToInt32(age) >= 18)
                        {
                            strDescription.Append(age + " year old ");
                        }
                    }

                    if (gender.ToUpper() == "M")
                    {
                        gender = "Male";
                    }
                    else
                    {
                        gender = "Female";
                    }

                    strDescription.Append(gender + " from " + list["location"] + ". ");

                    // This code is not even enabled at this time, so make it really fast and comment this out.
                    //					if (list ["community1"] != "")
                    //					{
                    //						strDescription.Append ("Interests include " + list ["community1"]);
                    //						if (list ["community2"] != "")
                    //						{
                    //							strDescription.Append (", " + list ["community2"]);
                    //						}
                    //						if (list ["community3"] != "")
                    //						{
                    //							strDescription.Append (", " + list ["community3"]);
                    //						}
                    //
                    //						strDescription.Append (". ");
                    //					}

                    strDescription.Append(list["friends"] + " Friends. " + list["raves"] + " Raves.");
                    strDescription.Append(" More about me... ");
                    break;

                case (int)Constants.eWEB_PAGE_TYPE.MEDIA:
                    strDescription.Append(name + " More Kaneva media...");
                    break;
            }

            if (stringOnly)
            {
                //truncate if necessary
                return KanevaGlobals.Truncate(strDescription.ToString(), 150);
            }
            else
            {
                //truncate if necessary
                return cDescriptionStart + KanevaGlobals.Truncate(strDescription.ToString(), 150) + cDescriptionEnd;
            }
        }

		/// <summary>
		/// sets or gets meta data keyword html
		/// </summary>
		//generates the Keywords meta data
		public static string GenerateMetaDescription (string name, NameValueCollection list, int pageType)
		{
            return GenerateMetaDescription(name, list, pageType, false);
		}

		private static string cDescriptionStart = "<meta name=\"description\" content=\"";
		private static string cDescriptionEnd = "\" />";
	}	
}

