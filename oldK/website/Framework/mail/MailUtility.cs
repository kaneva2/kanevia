///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data;
using log4net;
using System.IO;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for MailUtility.
	/// </summary>
	public class MailUtility
	{
        private static string Mail_Header = "";
        private static string Mail_Footer = "";
        private static string Mail_Username_Reset_Subject = "";
        private static string Mail_Username_Reset = "";
        private static string Mail_Registration_Email_Subject = "";
        private static string Mail_Registration_Email_Body = "";
        
    public MailUtility()
		{
    }


    #region Not Using Templates

        /// <summary>
		/// ShareAssetWithFriend
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void ShareAssetWithFriend (int userId, string recipient, int assetId, string sender, string message, DataRow drAsset, string assetType, System.Web.UI.Page page)
		{
			string senderURL = sender;
			string assetLink = "http://" + KanevaGlobals.SiteName + "/asset/" + assetId + ".storeItem";

			if (userId > 0)
			{
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(userId);

                sender = user.Username;
                senderURL = "member <a target='_resource' href='" + KanevaGlobals.GetPersonalChannelUrl(user.NameNoSpaces) + "' >" + sender + "</a>";
			}

			string subject = sender + " has sent you a Kaneva " + assetType;

			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br> Kaneva " + senderURL + " has shared the " + assetType + " <a target='_resource' href='" + assetLink + "'>" + drAsset ["name"].ToString () + "</a> with you.";

			strMessage += "<br><br>" + assetType + " link: ";
			strMessage += "<br> <a target='_resource' href='" + assetLink + "'>" + assetLink + "</a>";
		
			// Attach optional personal message?
			if (message.Trim ().Length > 0)
			{
				strMessage += "<br>\r\nPersonal message from " + sender + ":";
				strMessage += "<br><br>" + message;
			}

			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent by Kaneva, 270 Carpenter Drive, Sandy Springs, Georgia, 30328 USA.</span>";

			strMessage += "</body></html>";
			SendEmail (KanevaGlobals.FromEmail, recipient, subject, strMessage, true, true, 2);
		}

		/// <summary>
		/// SendGameSubPurchaseEmail
		/// </summary>
		public static void SendGameSubPurchaseEmail (int userId, int orderId, int assetId)
		{
			DataRow drOrder = StoreUtility.GetOrder (orderId);

			// Don't send emails for free items
			Double amount = Convert.ToDouble (drOrder ["gross_point_amount"]);
			if (amount.Equals (0.0))
			{
				return;
			}

            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

			DataRow drAsset = StoreUtility.GetAsset (assetId);

			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'><table border='0' width='100%'><tr><td width='20%'></td><td " + C_TD_STYLE + ">" +
				GetEmailHeader ();
			strMessage += "<br>Kaneva Game Subscription Purchase Confirmation. A payment has been made for a game which you have an <br>" +
				"active monthly subscription.";
			strMessage += "<br>";
			strMessage += "<br>Please keep this email for your records. Your order information is as follows:";
			strMessage += "<br>";
			strMessage += "<br><B>Item:</B> " + drAsset ["name"].ToString ();
			strMessage += "<br><B>Order Number:</B> " + Constants.ITEM_ORDER_ID_PREFIX + drOrder ["order_id"].ToString ();
			strMessage += "<br><B>Subscription Price:</B> " + KanevaGlobals.FormatKPoints (amount);
			strMessage += "<br><B>Puchase Date:</B> " + KanevaGlobals.FormatDateTime (Convert.ToDateTime (drOrder ["purchase_date"]));
			strMessage += "<br>";

			// Did they purchase additional K-Points?
			if (drOrder ["point_transaction_id"] != DBNull.Value)
			{
				DataRow drPurchasePoints = StoreUtility.GetPurchasePointTransaction (Convert.ToInt32 (drOrder ["point_transaction_id"]));

				strMessage += "<br>";
				strMessage += "Your k-point balance was insufficient to cover the subscription amount, K-Points were purchased on your behalf (" + KanevaGlobals.FormatCurrency (KanevaGlobals.MinimumCreditCardTransactionAmount) + " minimum).";
				strMessage += "<br>";
				strMessage += "<br><B>Order Number:</B> " + Constants.KPOINT_ORDER_ID_PREFIX + drPurchasePoints ["point_transaction_id"].ToString ();
				strMessage += "<br><B>Description:</B> " + drPurchasePoints ["description"].ToString ();
				strMessage += "<br><B>Amount Paid:</B> " + KanevaGlobals.FormatCurrency (Convert.ToDouble (drPurchasePoints ["amount_debited"]));
				strMessage += "<br><B>K-Points Received:</B> " + KanevaGlobals.FormatKPoints (Convert.ToDouble (drPurchasePoints ["totalPoints"]));
				strMessage += "<br><B>Puchase Date:</B> " + KanevaGlobals.FormatDateTime (Convert.ToDateTime (drPurchasePoints ["transaction_date"]));
				strMessage += "<br>";
			}
			
			strMessage += "<br>To review your order, you may go to your MyKaneva K-Point Purchases screen for this item.";
			strMessage += "<br>Help on game subscriptions can be found here <a style='color: green;' href=\"http://docs.kaneva.com/bin/view/Public/KanevaComPurchaseGame\">http://docs.kaneva.com/bin/view/Public/KanevaComPurchaseGame</a>.";
			strMessage += "<br>";
			strMessage += "<br>Thank you for your payment for this game subscription.";
			strMessage += "<br>";
            strMessage += "<br>This email was sent to " + user.Email + " by Kaneva, 270 Carpenter Drive, Sandy Springs, Georgia, 30328 USA.";
			strMessage += "<br>To cancel this subscription, go to your My Kaneva - My Profile - My Games screen <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/mykaneva/subscriptz.aspx\">http://" + KanevaGlobals.SiteName + "/mykaneva/subscriptz.aspx</a>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><br><br><br></td><td width='20%'></td></tr></table></body></html>";

			SendEmail (KanevaGlobals.FromEmail, user.Email, "Kaneva Game Subscription Payment Confirmation", strMessage, true, false, 1);
		}

		/// <summary>
		/// SendAuthorizeGameServer
		/// </summary>
		public static void SendAuthorizeGameServer (int userId, int orderId, double amount, int assetId)
		{
            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

			DataRow drOrder = StoreUtility.GetOrder (orderId);
			DataRow drAsset = StoreUtility.GetAsset (assetId);

			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'><table border='0' width='100%'><tr><td width='20%'></td><td " + C_TD_STYLE + ">" +
				GetEmailHeader ();
			strMessage += "<br>Kaneva Commercial Server License Authorization Confirmation";
			strMessage += "<br>";
			strMessage += "<br>Please keep this email for your records. Your item information is as follows:";
			strMessage += "<br>";
			strMessage += "<br><B>Item:</B> Authorize a Commercial Server License for " + drAsset ["name"].ToString ();
			strMessage += "<br><B>Order Number:</B> " + Constants.ITEM_ORDER_ID_PREFIX + drOrder ["order_id"].ToString ();
			strMessage += "<br><B>Authorization Amount:</B> " + KanevaGlobals.FormatCurrency (amount);
			strMessage += "<br><B>Authorization Date:</B> " + KanevaGlobals.FormatDateTime (Convert.ToDateTime (drOrder ["purchase_date"]));
			strMessage += "<br>";

			strMessage += "<br>This transaction has been posted your credit card as a temporary 'hold' (also called an 'authorization')";
			strMessage += "<br>This hold will be removed from your account over the next few business days, and the transaction will no";
			strMessage += "<br>longer be listed as a pending transaction.  At the end of the month this account will be billed for the amount";
			strMessage += "<br>owed based on end-user participation for your commercial game server license.";
			strMessage += "<br>";


			strMessage += "<br>To review and manage your game server licenses for this game, you may go to your Publish - My Published Games screen.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName +  "asset/publishedGames.aspx\">http://" + KanevaGlobals.SiteName +  "asset/publishedGames.aspx</a>";

			strMessage += "<br>";
			strMessage += "<br>Thank you for authorizing this server.";
            strMessage += "<br>This email was sent to " + user.Email + " by Kaneva, 270 Carpenter Drive, Sandy Springs, Georgia, 30328 USA.";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><br><br><br></td><td width='20%'></td></tr></table></body></html>";

            SendEmail(KanevaGlobals.FromEmail, user.Email, "Kaneva Commercial Server Authorization Confirmation", strMessage, true, false, 1);
		}

		/// <summary>
		/// SendOwnerEmail
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendOwnerEmail (int orderId, int assetId)
		{
			DataRow drOrder = StoreUtility.GetOrder (orderId);

			// Don't send emails for free items
			Double amount = Convert.ToDouble (drOrder ["gross_point_amount"]);
			if (amount.Equals (0.0))
			{
				return;
			}

			DataRow drAsset = StoreUtility.GetAsset (assetId);

            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(Convert.ToInt32(drAsset["owner_id"]));

			// Asset Owner
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'><table border='0' width='100%'><tr><td width='20%'></td><td " + C_TD_STYLE + ">" +
				GetEmailHeader ();
			strMessage += "<br>An Item you are the owner of has been purchased on Kaneva";
			strMessage += "<br>";
			strMessage += "<br>Please keep this email for your records. Your item information is as follows:";
			strMessage += "<br>";
			strMessage += "<br><B>Order Number:</B> " + Constants.ITEM_ORDER_ID_PREFIX + drOrder ["order_id"].ToString ();
			strMessage += "<br><B>Item:</B> " + drAsset ["name"].ToString ();
			strMessage += "<br><B>Puchase Amount:</B> " + KanevaGlobals.FormatKPoints (amount);
			strMessage += "<br><B>Puchase Date:</B> " + KanevaGlobals.FormatDateTime (Convert.ToDateTime (drOrder ["purchase_date"]));
			strMessage += "<br>";
			strMessage += "<br>To review the order, you may go to your MyKaneva Monthly Sales screens.";
			strMessage += "<br>";
			strMessage += "<br>You are receiving this email because you are the item owner.";
            strMessage += "<br>This email was sent to " + user.Email + " by Kaneva, 270 Carpenter Drive, Sandy Springs, Georgia, 30328 USA.";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><br><br><br></td><td width='20%'></td></tr></table></body></html>";

            SendEmail(KanevaGlobals.FromEmail, user.Email, "An Item you are the owner of has been purchased on Kaneva", strMessage, true, false, 1);
		}

		/// <summary>
		/// SendNowPlayingNotificationEmail
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendNowPlayingNotification (string email, string itemName, int assetId, string channelName)
		{
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>New <B>Now Playing Item</B> in the " + channelName + " community";
			strMessage += "<br>";
			strMessage += "<br>To view the new Now Playing item, please visit the following URL:";
			strMessage += "<br>";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/asset/" + assetId + ".StoreItem\">http://" + KanevaGlobals.SiteName + "/asset/" + assetId + ".StoreItem</a>";
			strMessage += "<hr>";
			strMessage += "\r\n";
			strMessage += "<br>If you'd like to change when you receive new Now Playing Item notifications:";
			strMessage += "<br>";
			strMessage += "<br>Sign in and adjust the 'I want updates' setting for this Community. The option can be found on your My Kaneva - My Communities page.<br>To change update settings for this community, click Change Setting, and then select 'As it happens', 'Daily Digest', 'Weekly Digest', or 'None'.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx\">http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx</a>";
			strMessage += "<br>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 270 Carpenter Drive, Sandy Springs, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "New Now Playing Item [" + itemName + "]", strMessage, true, false, 2);
		}

		/// <summary>
		/// SendReviewNotificationEmail
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendReviewNotification (string email, string itemName, int assetId, string channelName)
		{
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>New <B>Now Playing Item Review</B> in the " + channelName + " community";
			strMessage += "<br>";
			strMessage += "<br>To view the review, please visit the following URL:";
			strMessage += "<br>";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/asset/" + assetId + ".storeItem\">http://" + KanevaGlobals.SiteName + "/asset/assetComments?assetId=" + assetId + "</a>";
			strMessage += "<hr>";
			strMessage += "\r\n";
			strMessage += "<br>If you'd like to change when you receive new <B>Now Playing Item Review</B> notifications:";
			strMessage += "<br>";
			strMessage += "<br>Sign in and adjust the 'I want updates' setting for this Community. The option can be found on your My Kaneva - My Communities page.<br>To change update settings for this community, click Change Setting, and then select 'As it happens', 'Daily Digest', 'Weekly Digest', or 'None'.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx\">http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx</a>";
			strMessage += "<br>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 270 Carpenter Drive, Sandy Springs, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "New Review [" + itemName + "]", strMessage, true, false, 2);
		}

		/// <summary>
		/// SendForumTopicNotification
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendForumTopicNotification (string email, string itemName, int topicId, string channelName, int channelId)
		{
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>New <B>Forum Topic</B> in the " + channelName + " community";
			strMessage += "<br>";
			strMessage += "<br>To view the topic, please visit the following URL:";
			strMessage += "<br>";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + topicId + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + topicId + "&communityId=" + channelId + "</a>";
			strMessage += "<hr>";
			strMessage += "\r\n";
			strMessage += "<br>If you'd like to change when you receive new Forum Topic notifications:";
			strMessage += "<br>";
			strMessage += "<br>Sign in and adjust the 'I want updates' setting for this Community. The option can be found on your My Kaneva - My Communities page.<br>To change update settings for this community, click Change Setting, and then select 'As it happens', 'Daily Digest', 'Weekly Digest', or 'None'.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx\">http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx</a>";
			strMessage += "<br>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 270 Carpenter Drive, Sandy Springs, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "New Forum Topic [" + itemName + "]", strMessage, true, false, 2);
		}

		/// <summary>
		/// SendForumReplyNotification
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendForumReplyNotification (string email, string itemName, int topicId, string channelName, int channelId)
		{
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>New <B>Forum Reply</B> in the " + channelName + " community";
			strMessage += "<br>";
			strMessage += "<br>To view the reply, please visit the following URL:";
			strMessage += "<br>";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + topicId + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + topicId + "&communityId=" + channelId + "</a>";
			strMessage += "<hr>";
			strMessage += "\r\n";
			strMessage += "<br>If you'd like to change when you receive new Forum Reply notifications:";
			strMessage += "<br>";
			strMessage += "<br>Sign in and adjust the 'I want updates' setting for this Community. The option can be found on your My Kaneva - My Communities page.<br>To change update settings for this community, click Change Setting, and then select 'As it happens', 'Daily Digest', 'Weekly Digest', or 'None'.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx\">http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx</a>";
			strMessage += "<br>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 270 Carpenter Drive, Sandy Springs, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "New Forum Reply [" + itemName + "]", strMessage, true, false, 2);
		}

		/// <summary>
		/// SendBlogNotificationEmail
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendBlogNotification (string email, string itemName, int blogId, string channelName, int channelId)
		{
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>New <B>Blog Entry</B> in the " + channelName + " community";
			strMessage += "<br>";
			strMessage += "<br>To view the blog, please visit the following URL:";
			strMessage += "<br>";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/blog/blogComments.aspx?topicId=" + blogId + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/blogComments.aspx?topicId=" + blogId + "&communityId=" + channelId + "</a>";
			strMessage += "<hr>";
			strMessage += "\r\n";
			strMessage += "<br>If you'd like to change when you receive new Blog Entry notifications:";
			strMessage += "<br>";
			strMessage += "<br>Sign in and adjust the 'I want updates' setting for this Community. The option can be found on your My Kaneva - My Communities page.<br>To change update settings for this community, click Change Setting, and then select 'As it happens', 'Daily Digest', 'Weekly Digest', or 'None'.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx\">http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx</a>";
			strMessage += "<br>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 270 Carpenter Drive, Sandy Springs, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "New Blog Entry [" + itemName + "]", strMessage, true, false, 2);
		}

		/// <summary>
		/// SendBlogCommentNotificationEmail
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendBlogCommentNotification (string email, string itemName, int blogId, string channelName, int channelId)
		{
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>New <B>Blog Comment</B> in the " + channelName + " community";
			strMessage += "<br>";
			strMessage += "<br>To view the blog comment, please visit the following URL:";
			strMessage += "<br>";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/blog/blogComments.aspx?topicId=" + blogId + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/blogComments.aspx?topicId=" + blogId + "&communityId=" + channelId + "</a>";
			strMessage += "<hr>";
			strMessage += "<br>";
			strMessage += "\r\n";
			strMessage += "<br>If you'd like to change when you receive new <B>Blog Comment</B> notifications for this community:";
			strMessage += "<br>";
			strMessage += "<br>Sign in and adjust the 'I want updates' setting for this Community. The option can be found on your My Kaneva - My Communities page.<br>To change update settings for this community, click Change Setting, and then select 'As it happens', 'Daily Digest', 'Weekly Digest', or 'None'.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx\">http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx</a>";
			strMessage += "<br>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 270 Carpenter Drive, Sandy Springs, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "New Blog Comment [" + itemName + "]", strMessage, true, false, 2);
		}

		/// <summary>
		/// SendBlogCommentNotificationDaily
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
        public static void SendNotificationDailyDigest(string email, DataRow drCommunityUser, PagedDataTable pdtTopics, PagedDataTable pdtThreads, PagedList<Blog> plBlogs, PagedList<BlogComment> plBlogComments, string channelName, int channelId)
		{
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>Kaneva Digest for the <B>" + channelName + "</B> community";
			strMessage += "<br>";
			strMessage += "<br>";

            //// Now Playing Items
            //if ((pdtAssets.Rows.Count > 0) && Convert.ToInt32 (drCommunityUser ["item_notify"]).Equals (1))
            //{
            //    strMessage += "\r\nNew <B>Now Playing Items</B>";
            //    strMessage += "<br>";
            //    strMessage += "<br>To view the new Now Playing items, please visit the following URL(s):";
            //    strMessage += "<br>";
            //    for (int k=0; k < pdtAssets.Rows.Count; k++)
            //    {
            //        strMessage += "<br>" + (k + 1) + ". " + System.Web.HttpUtility.HtmlDecode (pdtAssets.Rows [k]["name"].ToString ()) + " <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/asset/" + Convert.ToInt32 (pdtAssets.Rows [k]["asset_id"]) + ".StoreItem\">http://" + KanevaGlobals.SiteName + "/asset/" + Convert.ToInt32 (pdtAssets.Rows [k]["asset_id"]) + ".StoreItem</a>";
            //    }
            //    strMessage += "<hr>";
            //}

            //// New Reviews
            //if ((pdtReviews.Rows.Count > 0) && Convert.ToInt32 (drCommunityUser ["item_review_notify"]).Equals (1))
            //{
            //    strMessage += "\r\nNew <B>Now Playing Item Reviews</B>";
            //    strMessage += "<br>";
            //    strMessage += "<br>To view the reviews, please visit the following URL(s):";
            //    strMessage += "<br>";
            //    for (int k=0; k < pdtReviews.Rows.Count; k++)
            //    {
            //        strMessage += "<br>" + (k + 1) + ". " + System.Web.HttpUtility.HtmlDecode (pdtReviews.Rows [k]["name"].ToString ()) + " <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/asset/" + Convert.ToInt32 (pdtReviews.Rows [k]["asset_id"]) + ".storeItem\">http://" + KanevaGlobals.SiteName + "/asset/assetComments?assetId=" + Convert.ToInt32 (pdtReviews.Rows [k]["asset_id"]) + "</a>";
            //    }
            //    strMessage += "<hr>";
            //}


			// Forum Topics
			if ((pdtTopics.Rows.Count > 0) && Convert.ToInt32 (drCommunityUser ["post_notify"]).Equals (1))
			{
				strMessage += "\r\nNew <B>Forum Topics</B>";
				strMessage += "<br>";
				strMessage += "<br>To view new forum topics, please visit the following URL(s):";
				strMessage += "<br>";
				for (int k=0; k < pdtTopics.Rows.Count; k++)
				{
					strMessage += "<br>" + (k + 1) + ". " + System.Web.HttpUtility.HtmlDecode (pdtTopics.Rows [k]["forum_name"].ToString ()) + " - " + System.Web.HttpUtility.HtmlDecode (pdtTopics.Rows [k]["subject"].ToString ()) + " <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + Convert.ToInt32 (pdtTopics.Rows [k]["topic_id"]) + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + Convert.ToInt32 (pdtTopics.Rows [k]["topic_id"]) + "&communityId=" + channelId + "</a>";
				}
				strMessage += "<hr>";
			}


			// Forum replies
			if ((pdtThreads.Rows.Count > 0) && Convert.ToInt32 (drCommunityUser ["reply_notify"]).Equals (1))
			{
				strMessage += "\r\nNew <B>Forum Replies</B>";
				strMessage += "<br>";
				strMessage += "<br>To view forum replies, please visit the following URL(s):";
				strMessage += "<br>";
				for (int k=0; k < pdtThreads.Rows.Count; k++)
				{
					strMessage += "<br>" + (k + 1) + ". " + System.Web.HttpUtility.HtmlDecode (pdtThreads.Rows [k]["forum_name"].ToString ()) + " - " + System.Web.HttpUtility.HtmlDecode (pdtThreads.Rows [k]["subject"].ToString ()) + " <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + Convert.ToInt32 (pdtThreads.Rows [k]["topic_id"]) + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + Convert.ToInt32 (pdtThreads.Rows [k]["topic_id"]) + "&communityId=" + channelId + "</a>";
				}
				strMessage += "<hr>";
			}


			// Blogs
			if ((plBlogs.Count > 0) && Convert.ToInt32 (drCommunityUser ["blog_notify"]).Equals (1))
			{
				strMessage += "\r\nNew <B>Blog Entries</B>";
				strMessage += "<br>";
				strMessage += "<br>To view the blog, please visit the following URL(s):";
				strMessage += "<br>";

                int k = 1;
                foreach (Blog blog in plBlogs)
                {
                    strMessage += "<br>" + k + ". " + System.Web.HttpUtility.HtmlDecode(blog.Subject) + " <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/blog/blogComments.aspx?topicId=" + blog.BlogId + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/blogComments.aspx?topicId=" + blog.BlogId + "&communityId=" + channelId + "</a>";
                    k++;
                }
				strMessage += "<hr>";
			}


			// Blog comments
            if ((plBlogComments.Count > 0) && Convert.ToInt32(drCommunityUser["blog_comment_notify"]).Equals(1))
			{
				strMessage += "\r\nNew <B>Blog Comments</B>";
				strMessage += "<br>";
				strMessage += "<br>To view the blog comments, please visit the following URL(s):";
				strMessage += "<br>";

                int k = 1;
                foreach (BlogComment blogComment in plBlogComments)
				{
                    strMessage += "<br>" + k + ". " + System.Web.HttpUtility.HtmlDecode(blogComment.Subject) + " <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/blog/blogComments.aspx?topicId=" + blogComment.BlogId + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/blogComments.aspx?topicId=" + blogComment.BlogId + "&communityId=" + channelId + "</a>";
                    k++;
				}
				strMessage += "<hr>";
			}

			strMessage += "<br><br>";
			strMessage += "\r\nIf you'd like to change when you receive new Digest notifications:";
			strMessage += "<br>";
			strMessage += "<br>Sign in and adjust the 'I want updates' setting for this Community. The option can be found on your My Kaneva - My Communities page.<br>To change update settings for this community, click Change Setting, and then select 'As it happens', 'Daily Digest', 'Weekly Digest', or 'None'.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx\">http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx</a>";
			strMessage += "<br>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 270 Carpenter Drive, Sandy Springs, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "Digest for the " + channelName + " community", strMessage, true, false, 2);
        }

        #endregion Not Using Templates

    #region Uses Templates

        #region Mail Functions Moved from Web to Framework

        private static string getStandardHeader()
        {
            if (Mail_Header == null || Mail_Header.Length.Equals(0))
            {
                DataRow dr = Mailer.EmailTemplateUtility.GetTemplateByName("Header");
                Mail_Header = dr["template_html"].ToString();
            }
            return Mail_Header;
        }

        private static string getStandardFooter()
        {
            if (Mail_Footer == null || Mail_Footer.Length.Equals(0))
            {
                DataRow dr = Mailer.EmailTemplateUtility.GetTemplateByName("Footer");
                Mail_Footer = dr["template_html"].ToString();
            }
            return Mail_Footer;
        }


        /// <summary>
        /// SendUsernameResetEmail
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        /// <param name="returnPage"></param>
        public static void SendUsernameResetEmail(string username, string password, string email)
        {
            try
            {
                string header = getStandardHeader();
                string footer = getStandardFooter();

                if (Mail_Username_Reset == null || Mail_Username_Reset.Length.Equals(0))
                {
                    using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "username-reset.txt"))
                    {
                        Mail_Username_Reset_Subject = sr.ReadLine();
                        Mail_Username_Reset_Subject = Mail_Username_Reset_Subject.Substring(Mail_Username_Reset_Subject.IndexOf(":") + 1).Trim();
                        sr.ReadLine();
                        Mail_Username_Reset = sr.ReadToEnd();
                        sr.Close();
                    }
                }

                Mail_Username_Reset = Mail_Username_Reset.Replace("#header#", header);
                Mail_Username_Reset = Mail_Username_Reset.Replace("#footer#", footer);
                Mail_Username_Reset = Mail_Username_Reset.Replace("#temp_username#", username);
                Mail_Username_Reset = Mail_Username_Reset.Replace("#temp_password#", password);
                Mail_Username_Reset = Mail_Username_Reset.Replace("#sitename#", KanevaGlobals.SiteName);

                SendEmail(KanevaGlobals.FromSupportEmail, email, Mail_Username_Reset_Subject, Mail_Username_Reset, true, true, 1);
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, username =" + username, exc);
            }
        }

        /// <summary>
        /// SendRegistrationEmail
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        public static void SendRegistrationEmail(string username, string email, string activationKey)
        {
            SendRegistrationEmail(username, email, activationKey, "signup");
        }

        /// <summary>
        /// SendRegistrationEmail
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        /// <param name="returnPage"></param>
        public static void SendRegistrationEmail(string username, string email, string activationKey, string returnPage)
        {
            try
            {
                string header = getStandardHeader();
                string footer = getStandardFooter();

                if (Mail_Registration_Email_Subject == null || Mail_Registration_Email_Subject.Length.Equals(0))
                {
                    using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "activate-account.txt"))
                    {
                        Mail_Registration_Email_Subject = sr.ReadLine();
                        Mail_Registration_Email_Subject = Mail_Registration_Email_Subject.Substring(Mail_Registration_Email_Subject.IndexOf(":") + 1).Trim();
                        sr.ReadLine();
                        Mail_Registration_Email_Body = sr.ReadToEnd();
                        sr.Close();
                    }
                }

                Mail_Registration_Email_Body = Mail_Registration_Email_Body.Replace("#header#", header);
                Mail_Registration_Email_Body = Mail_Registration_Email_Body.Replace("#footer#", footer);
                Mail_Registration_Email_Body = Mail_Registration_Email_Body.Replace("#username#", username);
                Mail_Registration_Email_Body = Mail_Registration_Email_Body.Replace("#sitename#", KanevaGlobals.SiteName);
                Mail_Registration_Email_Body = Mail_Registration_Email_Body.Replace("#activationkey#", activationKey);
                Mail_Registration_Email_Body = Mail_Registration_Email_Body.Replace("#returnpage#", returnPage);
                Mail_Registration_Email_Body = Mail_Registration_Email_Body.Replace("#toemail#", email);

                Mail_Registration_Email_Subject = Mail_Registration_Email_Subject.Replace("#username#", username);

                SendEmail("myaccount@kaneva.com", email, Mail_Registration_Email_Subject, Mail_Registration_Email_Body, true, true, 1);
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, SendRegistrationEmail, username =" + username, exc);
            }
        }

        #endregion

        #endregion Uses Templates

        private static bool MarketingEmailsDebugMode
        {
          get { return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["debugMode"]); }
        }

		public static string GetEmailHeader ()
		{
			return "<img src='http://" + KanevaGlobals.SiteName + "/images/head/logok.gif' border='0'><br><br><br>"; 
		}

		public static string GetEmailFooter ()
		{
			return "<hr>" +
				"<span style='font-size:11px;'>\r\nEveryone at Kaneva cares about your privacy. If you don't want to receive emails like this " +
				"in the future, you can <a href=\'http://" + KanevaGlobals.SiteName + "/mykaneva/settings.aspx' target='_resource'>update your account settings here</a>.</span>" +
				"<br><br><span style='font-size:11px;'>" +
				"(c) 2006-2007 Kaneva. All rights reserved worldwide.</span>";
		}

        /// <summary>
        /// Sends email. 
        /// </summary>
        /// OLD EMAIL FUNC
        public static int SendEmail(string from, string to, string subject, string body, bool isHtml, bool bSendBCCToKaneva, int tier)
        {
            string fromDisplayName = KanevaGlobals.FromDisplayName;
            if (fromDisplayName.Length == 0)
            {
                fromDisplayName = "";
            }

            MailAddress maFrom = new MailAddress(from, fromDisplayName);
            MailAddress maTo = new MailAddress(to);

            return SendEmail (maFrom, maTo, subject, body, isHtml, bSendBCCToKaneva, tier);
            
            
        }

        /// <summary>
        /// Sends email. 
        /// </summary>
        public static int SendEmail(MailAddress maFrom, MailAddress maTo, string subject, string body, bool isHtml, bool bSendBCCToKaneva, int tier)
        {

            // Viral Filter.
            if (tier.Equals(1))
            {
                // Check Traplist
                if (IsEmailTraplisted(maTo.Address))
                {
                  return (int)Constants.eEMAIL_SEND_STATUS.TRAPLIST;
                }
            }

            if (tier.Equals(3)  || tier.Equals(4))
            {
                // Check Filterlist
                if (IsEmailFiltered(maTo.Address))
                {
                  return (int)Constants.eEMAIL_SEND_STATUS.FILTERLIST;
                }
            }


            if (tier.Equals(2) || tier.Equals(3) || tier.Equals(4))
            {
                // Check Blacklist
                if (IsEmailBlacklisted(maTo.Address, true))
                {
                  return (int)Constants.eEMAIL_SEND_STATUS.BLACKLIST;
                }
            }

            MailMessage message = new MailMessage ();
            message.From = maFrom;
            message.To.Add (maTo);

            if (bSendBCCToKaneva)
            {
                message.Bcc.Add (new MailAddress ("maillogger@kaneva.com"));
            }

            message.Headers.Add("X-KELID", KanevaGlobals.ConvertStringToHex(maTo.Address));

            // Per Ryan R remove date - note not working yet
            // message.Headers.Remove("Date");

            message.Subject = subject;
                      
            // Multipart removed until further instructions
            
            // HTML Message
            AlternateView htmlPart = AlternateView.CreateAlternateViewFromString(body, Encoding.UTF8, "text/html");
            htmlPart.TransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;
            message.AlternateViews.Add(htmlPart);
            message.BodyEncoding = Encoding.UTF8;  
            message.IsBodyHtml = false;
           
            /*
            // Here we need to create text versions of all emails!!!
            AlternateView textPart = AlternateView.CreateAlternateViewFromString("Please enable HTML", Encoding.UTF8, "text/plain");
            textPart.TransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;
            message.AlternateViews.Add(textPart);
            */

            SmtpClient smtpClient = new SmtpClient ();
            smtpClient.Host = KanevaGlobals.SMTPServer;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
            smtpClient.PickupDirectoryLocation = GetPickupDirectory (tier);
            
            try
            {
                smtpClient.Send (message);
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error sending email", exc);
            }

            return (int)Constants.eEMAIL_SEND_STATUS.OK;
        }

        ///// <summary>
        ///// Returns false if the email matches  
        ///// </summary>
        ///// <param name="recipientEmail"></param>
        //public static bool AllowSend(string email)
        //{
        //  // This is where we check the email address against our sending policy
                
        //  // Does the address match something we've blacklisted?
        //  if(KanevaGlobals.EnableEmailBlacklist)
        //  {
        //    if(IsEmailBlacklisted(email, true))
        //    {        
        //      if(KanevaGlobals.EnableEmailDebug)
        //        m_logger.Warn("Email " + email + " blacklisted! Setting AllowSend to false.");

        //      return false;
        //    }
        //  }

        //  // Is this an email of someone who's unsubscribed
        //  if (KanevaGlobals.EnableEmailUnsubscribe)
        //  {
        //    if(IsEmailUnsubscribed(email))
        //    {
        //      if (KanevaGlobals.EnableEmailDebug)
        //        m_logger.Warn("Email " + email + " unsubscribed! Setting AllowSend to false ");
              
        //      return false;
        //    }
        //  }
               
        //  return true;
        //}

        /// <summary>
        /// IsEmailBlacklisted
        /// </summary>
        public static bool IsEmailBlacklisted(string email, bool bLogMatch)
        {
            // check the list 
            int iBlackListId = 0;
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityEmailViral();

            string strSql = "SELECT blacklist_id FROM blacklist WHERE email = @email";

            Hashtable parameters = new Hashtable();
            parameters.Add("@email", email);

            DataRow drMatch = dbUtility.GetDataRow(strSql, parameters, false);
            if (drMatch != null)
            {
                iBlackListId = Convert.ToInt32(drMatch["blacklist_id"]);

                if (iBlackListId > 0)
                {
                    // We don't want to log if they are simply on thier MyKaneva page
                    if (bLogMatch)
                    {
                        strSql = "INSERT INTO blacklist_match_log " +
                            "(blacklist_id, when_matched)" +
                            " VALUES " +
                            "(@iBlackListId, NOW())";

                        parameters = new Hashtable();
                        parameters.Add("@iBlackListId", iBlackListId);
                        dbUtility.ExecuteNonQuery(strSql, parameters);
                    }

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// IsEmailFiltered
        /// </summary>
        public static bool IsEmailFiltered(string email)
        {         
          // check the list 
          int iFilterListId = 0;
          DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityEmailViral();

          string strSql = "SELECT filterlist_id FROM filterlist WHERE INSTR(@email, pattern_text)";

          Hashtable parameters = new Hashtable();
          parameters.Add("@email", email);

          DataRow drMatch = dbUtility.GetDataRow(strSql, parameters, false);
          if (drMatch != null)
          {
              iFilterListId = Convert.ToInt32(drMatch["filterlist_id"]);

              if (iFilterListId > 0)
              {
                  strSql = "INSERT INTO filterlist_match_log " +
                      "(filterlist_id, email, when_matched)" +
                      " VALUES " +
                      "(@iFilterListId, @email, NOW())";

                  parameters = new Hashtable();
                  parameters.Add("@iFilterListId", iFilterListId);
                  parameters.Add("@email", email);
                  dbUtility.ExecuteNonQuery(strSql, parameters);
                  return true;
              }
          }

          return false;
        }

        /// <summary>
        /// IsEmailTraplisted
        /// </summary>
        public static bool IsEmailTraplisted(string email)
        {
            // check the list 
            int iTrapListId = 0;
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityEmailViral();

            string strSql = "SELECT traplist_id FROM traplist WHERE email = @email";

            Hashtable parameters = new Hashtable();
            parameters.Add("@email", email);

            DataRow drMatch = dbUtility.GetDataRow(strSql, parameters, false);
            if (drMatch != null)
            {
                iTrapListId = Convert.ToInt32(drMatch["traplist_id"]);

                if (iTrapListId > 0)
                {
                    strSql = "INSERT INTO traplist_match_log " +
                        "(traplist_id, when_matched)" +
                        " VALUES " +
                        "(@iTrapListId, NOW())";

                    parameters = new Hashtable();
                    parameters.Add("@iTrapListId", iTrapListId);
                    dbUtility.ExecuteNonQuery(strSql, parameters);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// RemoveFromBlackList
        /// </summary>
        /// <param name="email"></param>
        public static void RemoveFromBlackList(string email)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityEmailViral();

            string strSql = "CALL delete_blacklist (@email)";
            Hashtable parameters = new Hashtable();
            parameters.Add("@email", email);
            dbUtility.ExecuteNonQuery(strSql, parameters);
        }

        //public static bool IsEmailUnsubscribed(string email)
        //{     
        //  // check the list 
        //  DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

        //  string sqlSelectString = "SELECT u.unsubscribe_id FROM email_unsubscribe AS u WHERE @email = u.email";

        //  Hashtable parameters = new Hashtable();
        //  parameters.Add("@email", email);

        //  DataRow drMatch = dbUtility.GetDataRow(sqlSelectString, parameters, false);
        //  if (drMatch != null)
        //  {
        //    if ((int)drMatch["unsubscribe_id"] > 0)
        //    {
        //      return true;
        //    }
        //  }

        //  // REMOVE
        //  if (KanevaGlobals.EnableEmailDebug)
        //    m_logger.Warn("Not unsubscribed: " + email + " ");

        //  return false;
        //}

        //// GetEmailBlacklist
        ///// <summary>
        ///// Gets Email Blacklist
        ///// </summary>
        ///// <returns></returns>
        //public static PagedDataTable GetEmailBlacklist(string filter, string orderBy, int pageNumber, int pageSize)
        //{
        //  string sqlSelectList = "blacklist_id, pattern, note, created_date";

        //  string sqlTableList = "email_blacklist";

        //  string sqlWhereClause = "";

        //  if (filter.Trim().Length > 0)
        //  {
        //    sqlWhereClause = filter;
        //  }

        //  Hashtable parameters = new Hashtable();
        //  return KanevaGlobals.GetDatabaseUtilityEmailViral().GetPagedDataTable(sqlSelectList, sqlTableList, sqlWhereClause, orderBy, parameters, pageNumber, pageSize);
        //}


        //public static void DeleteBlacklistPattern(int id)
        //{
        //  //delete from email_blacklist table
        //  string sqlString = "DELETE FROM email_blacklist WHERE blacklist_id = @id ";

        //  Hashtable parameters = new Hashtable();
        //  parameters.Add("@id", id);

        //  KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlString, parameters);
        //}

        //public static int UpdateBlacklistPattern(int blacklist_id, string pattern, string note)
        //{
        //  DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
        //  Hashtable parameters = new Hashtable();

        //  string sqlString = "UPDATE email_blacklist" +
        //      " SET pattern = @pattern," +
        //      " note = @note" +
        //      " WHERE blacklist_id = @blacklist_id";

        //  parameters = new Hashtable();
        //  parameters.Add("@pattern", pattern);
        //  parameters.Add("@note", note);
        //  parameters.Add("@blacklist_id", blacklist_id);

        //  return KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlString, parameters);
        //}

        //public static int InsertBlacklistPattern(string pattern, string note)
        //{
        //  DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
        //  Hashtable parameters = new Hashtable();

        //  string sqlString = "INSERT INTO email_blacklist" +
        //      " (pattern,note,created_date)" +
        //      " VALUES (@pattern,@note,@created_date);";

        //  parameters = new Hashtable();
        //  parameters.Add("@pattern", pattern);
        //  parameters.Add("@note", note);
        //  parameters.Add("@created_date", DateTime.Now);

        //  int blacklist_id = 0;
        //  KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteIdentityInsert(sqlString, parameters, ref blacklist_id);

        //  return blacklist_id;
        //}

        public static string GetPickupDirectory(int tier)
        {
          // Determine the pickup directory
          switch (tier)
          {
            case 1:
              return KanevaGlobals.EmailTier1;
            case 2:
              return KanevaGlobals.EmailTier2;
            case 3:
              return KanevaGlobals.EmailTier3;
            case 4:
              return KanevaGlobals.EmailTier4;
            default:
              return KanevaGlobals.EmailTierDefault;
          }
        }

        // Tracking function
        public static int recordView(int user_id, int mailing_id, string ipaddress, string useragent)
            {
                try
                {

                    string sqlRecordView = "";
                    sqlRecordView = " INSERT INTO email_open_log (user_id, mailing_id, created_date, ipaddress, useragent)" +
                                " VALUES (@user_id, @mailing_id, @created_date, @ipaddress, @useragent)";
                    Hashtable parameters = new Hashtable();
                    parameters.Add("@user_id", user_id);
                    parameters.Add("@mailing_id", mailing_id);
                    parameters.Add("@created_date", DateTime.Now);
                    parameters.Add("@ipaddress", ipaddress);
                    parameters.Add("@useragent", useragent);
                    KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlRecordView, parameters);
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error Recording View: ", exc);
                }
                return 1;
            }

        public static int recordClick(int user_id, int mailing_id, string request, string ipaddress, string useragent)
        {
          try
          {

            string sqlRecordClick = "";
            sqlRecordClick = " INSERT INTO email_click_log  (user_id, mailing_id, created_date, request, ipaddress, useragent)" +
                        " VALUES (@user_id, @mailing_id, @created_date, @request, @ipaddress, @useragent)";
            Hashtable parameters = new Hashtable();
            parameters.Add("@user_id", user_id);
            parameters.Add("@mailing_id", mailing_id);
            parameters.Add("@created_date", DateTime.Now);
            parameters.Add("@request", request);
            parameters.Add("@ipaddress", ipaddress);
            parameters.Add("@useragent", useragent);
            KanevaGlobals.GetDatabaseUtilityEmailViral().ExecuteNonQuery(sqlRecordClick, parameters);
          }
          catch (Exception exc)
          {
            m_logger.Error("Error Recording Email Click: ", exc);
          }
          return 1;
        }

        public static int RecordViewInvite (int inviteId)
            {
                try
                {
                    string sql = "UPDATE invites " +
                        " SET opened_invite = 1 " +
                        " WHERE invite_id = @inviteId";
                    
                    Hashtable parameters = new Hashtable ();
                    parameters.Add ("@inviteId", inviteId);
                    GetDatabaseUtility ().ExecuteNonQuery (sql, parameters);
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error Recording View Invite: ", exc);
                }
                
                return 1;
            }

        /// <summary>
        /// Get the database utility
        /// </summary>
        /// <returns></returns>
        public static DatabaseUtility GetDatabaseUtility()
        {
            if (m_DatabaseUtilty == null)
            {
                switch (KanevaGlobals.DatabaseType)
                {
                    case (int)Constants.eSUPPORTED_DATABASES.SQLSERVER:
                        {
                            m_DatabaseUtilty = new SQLServerUtility(KanevaGlobals.ConnectionString);
                            break;
                        }
                    case (int)Constants.eSUPPORTED_DATABASES.MYSQL:
                        {
                            m_DatabaseUtilty = new MySQLUtility(KanevaGlobals.ConnectionString);
                            break;
                        }
                    default:
                        {
                            throw new Exception("This database type is not supported");
                        }
                }
            }
            return m_DatabaseUtilty;
        }

        /// <summary>
        /// DataBase Utility for Master Database
        /// </summary>
        private static DatabaseUtility m_DatabaseUtilty;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		private static string C_TD_STYLE = "style='font-family:arial; color:#666666; font-size:12px;' width='60%' align='left' bgcolor='#ffffff'";
   
	}
}
