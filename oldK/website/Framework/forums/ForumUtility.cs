///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for ForumUtility.
	/// </summary>
	public class ForumUtility
	{
		static ForumUtility()
		{
			m_DbNameKanevaNonCluster = KanevaGlobals.DbNameKanevaNonCluster;
		}

        ///// <summary>
        ///// GetForumStats
        ///// </summary>
        ///// <returns></returns>
        //public static string GetForumStats (int communityId)
        //{
        //    int forumCount, topicCount, postCount;

        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();
        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);

        //    string sqlSelect = "SELECT COUNT(*) FROM forums WHERE community_id = @communityId";
        //    forumCount = dbUtility.ExecuteScalar (sqlSelect, parameters);

        //    sqlSelect = "SELECT COALESCE(SUM(number_of_topics),0) FROM forums WHERE community_id = @communityId";
        //    topicCount = dbUtility.ExecuteScalar (sqlSelect, parameters);

        //    sqlSelect = "SELECT COALESCE(SUM(number_of_replies),0) FROM topics WHERE community_id = @communityId";
        //    postCount = dbUtility.ExecuteScalar (sqlSelect, parameters);

        //    return "Our users have posted " + postCount + " post(s) in " + topicCount + " Topic(s) in " + forumCount + " Forum(s)";
        //}

        ///// <summary>
        ///// GetForumReplies
        ///// </summary>
        ///// <returns></returns>
        //public static int GetForumReplies (int forumId)
        //{
        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@forumId", forumId);

        //    string sqlSelect = "SELECT COALESCE(SUM(number_of_replies),0) FROM topics WHERE forum_id = @forumId";
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().ExecuteScalar(sqlSelect, parameters);
        //}

        ///// <summary>
        ///// GetForumUserStats
        ///// </summary>
        ///// <returns></returns>
        //public static DataRow GetForumUserStats (int communityId)
        //{
        //    string sqlSelect = "SELECT th.created_date, th.user_id, th.topic_id, th.thread_id, u.username " + 
        //        " FROM topics t, threads th, forums f, users u " + 
        //        " WHERE f.community_id = @communityId " + 
        //        " AND f.last_topic_id = t.topic_id " + 
        //        " AND t.last_thread_id = th.thread_id " + 
        //        " AND th.user_id = u.user_id " + 
        //        " ORDER BY th.thread_id DESC LIMIT 1 ";

        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();
        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);
        //    return dbUtility.GetDataRow (sqlSelect, parameters, false);
        //}

        ///// <summary>
        ///// Get the Forum Categories
        ///// </summary>
        ///// <returns></returns>
        //public static DataTable GetForumCategories (int communityId)
        //{
        //    string sqlSelect = "SELECT forum_category_id, name, description, display_order " +
        //        " FROM forum_categories " +
        //        " WHERE community_id = @communityId " +
        //        " ORDER BY display_order ASC, name ASC";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        //}

        ///// <summary>
        ///// Get the Forum Categories
        ///// </summary>
        ///// <returns></returns>
        //public static DataRow GetForumCategory (int forumCategoryId)
        //{
        //    string sqlSelect = "SELECT status_id " +
        //        " FROM forum_categories " +
        //        " WHERE forum_category_id = @forumCategoryId";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@forumCategoryId", forumCategoryId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, true);
        //}

        ///// <summary>
        ///// Update forum category
        ///// </summary>
        ///// <param name="communityId"></param>
        ///// <param name="forumCategoryId"></param>
        ///// <param name="displayOrder"></param>
        ///// <returns></returns>
        //public static int UpdateForumCategoryOrder (int communityId, int forumCategoryId, string categoryName, int displayOrder)
        //{
        //    string sqlSelect = "UPDATE forum_categories " +
        //        " SET display_order = @displayOrder " +
        //        " , name = @categoryName " +
        //        " WHERE community_id = @communityId " +
        //        " AND forum_category_id = @forumCategoryId";

        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);
        //    parameters.Add ("@displayOrder", displayOrder);
        //    parameters.Add ("@categoryName", categoryName);
        //    parameters.Add ("@forumCategoryId", forumCategoryId);
        //    dbUtility.ExecuteNonQuery (sqlSelect, parameters);
        //    return 0;
        //}

        ///// <summary>
        ///// Get the Forums
        ///// </summary>
        ///// <returns></returns>
        //public static DataTable GetForums (int communityId, int forumCategoryId)
        //{
        //    string sqlSelect = "SELECT forum_id, forum_category_id, f.forum_name, f.description, " +
        //        " number_of_topics, last_post_date, display_order, f.status_id, f.last_topic_id, " +
        //        " com.name_no_spaces, com.thumbnail_small_path " +
        //        " FROM forums f LEFT OUTER JOIN communities_personal com ON f.last_user_id = com.creator_id " +
        //        " WHERE f.community_id = @communityId " +
        //        " AND f.forum_category_id = @forumCategoryId " +
        //        " ORDER BY f.display_order ASC";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);
        //    parameters.Add ("@forumCategoryId", forumCategoryId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        //}

        ///// <summary>
        ///// Get the Forums
        ///// </summary>
        ///// <returns></returns>
        //public static DataTable GetForums (int communityId)
        //{
        //    string sqlSelect = "SELECT forum_id, f.forum_category_id, forum_name, f.description, number_of_topics, last_post_date, " +
        //        " f.display_order, fc.display_order as fc_display_order, status_id, f.last_topic_id " +
        //        " FROM forums f " +
        //        " INNER JOIN forum_categories fc on f.forum_category_id = fc.forum_category_id " +
        //        " WHERE f.community_id = @communityId " +
        //        " ORDER BY fc.display_order ASC, f.display_order ASC";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        //}

        ///// <summary>
        ///// Update forums
        ///// </summary>
        //public static int UpdateForums (int communityId, int forumCategoryId, int forumId, string forumName, string description, int displayOrder)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
        //    Hashtable parameters = new Hashtable ();			

        //    string strUpdateForumName = "";

        //    // Only update the name if they pass something in.
        //    if (forumName.Length > 0)
        //    {
        //        parameters.Add ("@forumName", forumName);
        //        strUpdateForumName = " , forum_name = @forumName";
        //    }

        //    string strUpdatedescription = "";

        //    // Only update the name if they pass something in.
        //    if (description.Length > 0)
        //    {
        //        parameters.Add ("@description", description);
        //        strUpdatedescription = " , description = @description";
        //    }

        //    string sqlSelect = "UPDATE forums " +
        //        " SET display_order = @displayOrder " +
        //        " , forum_category_id = @forumCategoryId " +
        //        strUpdateForumName + strUpdatedescription +
        //        " WHERE community_id = @communityId " +
        //        " AND forum_id = @forumId ";

        //    parameters.Add ("@communityId", communityId);
        //    parameters.Add ("@displayOrder", displayOrder);
        //    parameters.Add ("@forumCategoryId", forumCategoryId);
        //    parameters.Add ("@forumId", forumId);
        //    dbUtility.ExecuteNonQuery (sqlSelect, parameters);
        //    return 0;
        //}

        ///// <summary>
        ///// Get a Forum
        ///// </summary>
        ///// <returns></returns>
        //public static DataRow GetForum (int forumId)
        //{
        //    string sqlSelect = "SELECT community_id, forum_id, forum_category_id, forum_name, description, number_of_topics, last_post_date, display_order, status_id " +
        //        " FROM forums " +
        //        " WHERE forum_id = @forumId";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@forumId", forumId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, true);
        //}

        ///// <summary>
        ///// Get the Forum Topics
        ///// </summary>
        ///// <returns></returns>
        //public static PagedDataTable GetForumTopics (int forumId, bool important, int pageNumber, int pageSize)
        //{
        //    string strQuery = "SELECT t.topic_id, t.subject, t.sticky, t.important, t.number_of_replies, t.number_of_views, t.created_date, " +
        //        " t.last_updated_date, t.last_reply_date, t.poll_id, t.status_id, t.last_updated_user_id, t.created_username AS username, t.last_thread_id, " +
        //        " com.name_no_spaces, com.thumbnail_small_path, comLastPost.name_no_spaces AS last_name_no_spaces, " +
        //        " comLastPost.name AS last_username, comLastPost.thumbnail_small_path AS last_thumbnail_small_path " +
        //        " FROM topics t " +
        //        " INNER JOIN communities_personal comLastPost ON comLastPost.creator_id = t.last_user_id " +
        //        " INNER JOIN communities_personal com ON com.creator_id = t.created_user_id " +
        //        " WHERE forum_id = @forumId " +
        //        " AND t.status_id <> " + (int) Constants.eFORUM_STATUS.DELETED +
        //        " ORDER BY t.important DESC , t.sticky DESC , t.last_reply_date DESC";

        //    string orderBy = "";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@forumId", forumId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTableUnion(strQuery, orderBy, parameters, pageNumber, pageSize);
        //}

        ///// <summary>
        ///// Get the Forum Topics
        ///// </summary>
        ///// <returns></returns>
        //public static PagedDataTable GetForumTopics (int communityId, DateTime sinceDate, int pageNumber, int pageSize)
        //{
        //    string selectList = "f.forum_name, t.topic_id, t.subject, t.sticky, t.important, t.number_of_replies, " +
        //        " t.number_of_views, t.created_date, t.last_updated_date, t.poll_id, t.status_id, t.last_updated_user_id, " +
        //        " t.created_username as username, t.last_thread_id";

        //    string tableList = " topics t, forums f ";

        //    string whereClause = "f.community_id = @communityId " +
        //        " AND f.forum_id = t.forum_id " +
        //        " AND t.status_id <> " + (int) Constants.eFORUM_STATUS.DELETED +
        //        " AND t.created_date > @sinceDate ";

        //    string orderBy = "t.sticky DESC, t.last_reply_date DESC";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);
        //    parameters.Add ("@sinceDate", sinceDate);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderBy, parameters, pageNumber, pageSize);
        //}

        ///// <summary>
        ///// Get the User Forum Topics
        ///// </summary>
        ///// <returns></returns>
        //public static PagedDataTable GetUserForumTopics (int userId, string orderBy, int pageNumber, int pageSize)
        //{
        //    string selectList = "f.community_id, t.topic_id, t.subject, t.sticky, t.important, t.number_of_replies, " +
        //        " t.number_of_views, t.created_date, t.last_updated_date, t.poll_id, t.body_text, t.status_id, t.last_updated_user_id, " +
        //        " u.username ";

        //    string tableList = " topics t, users u, forums f ";

        //    string whereClause = " f.forum_id = t.forum_id " +
        //        " AND t.created_user_id = u.user_id " +
        //        " AND t.status_id <> " + (int) Constants.eFORUM_STATUS.DELETED +
        //        " AND t.created_user_id = @userId ";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@userId", userId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly ().GetPagedDataTable (selectList, tableList, whereClause, orderBy, parameters, pageNumber, pageSize);
        //}

        ///// <summary>
        ///// Get the Forum Topics
        ///// </summary>
        ///// <returns></returns>
        //public static PagedDataTable GetLatestForumTopics (int communityId, int pageNumber, int pageSize)
        //{
        //    string strQuery = " SELECT t.topic_id, t.subject, t.created_date, t.last_updated_date, " +
        //        " COALESCE( t.last_updated_user_id, created_user_id ) AS last_updated_user_id, " +
        //        " t.last_reply_date, number_of_replies, created_user_id, created_username, t.last_user_id, " +
        //        " com.name_no_spaces as created_name_no_spaces, " +
        //        " com.thumbnail_small_path as created_thumbnail_small_path, " +
        //        " comLastPost.name_no_spaces AS last_post_name_no_spaces, " +
        //        " comLastPost.name AS last_username, " +
        //        " comLastPost.thumbnail_small_path as last_thumbnail_small_path " +
        //        " FROM topics t " +
        //        " INNER JOIN communities_personal comLastPost ON comLastPost.creator_id = t.last_user_id " +
        //        " INNER JOIN communities_personal com ON t.created_user_id = com.creator_id " +
        //        " WHERE t.community_id = @communityId " +
        //        " AND t.status_id <> " + (int) Constants.eFORUM_STATUS.DELETED +
        //        " GROUP BY t.topic_id " +
        //        " ORDER BY t.last_reply_date DESC";

        //    string orderBy = "";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTableUnion(strQuery, orderBy, parameters, pageNumber, pageSize);
        //}

        ///// <summary>
        ///// Get a topic
        ///// </summary>
        ///// <param name="topicId"></param>
        ///// <returns></returns>
        //public static DataRow GetForumTopic (int topicId)
        //{
        //    string sqlSelect = "SELECT t.topic_id, t.subject, t.allow_comments, t.allow_ratings, " +
        //        " t.body_text, t.created_date,  t.ip_address, t.created_user_id, t.important, t.sticky, t.number_of_replies,  " +
        //        " t.status_id, t.last_updated_date, t.last_updated_user_id, " +
        //        " u.username, u.signup_date, u.user_id, u.role, u2.username as updatedUsername, " +
        //        " f.community_id, f.forum_id, us.number_forum_posts " +
        //        " FROM users u INNER JOIN users_stats us ON u.user_id = us.user_id, " +
        //        " topics t LEFT OUTER JOIN users u2 ON t.last_updated_user_id = u2.user_id, forums f " + 
        //        " WHERE t.forum_id = f.forum_id " +
        //        " AND viewable = 'Y' " +
        //        " AND t.created_user_id = u.user_id" +
        //        " AND t.topic_id = @topicId";
			
        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@topicId", topicId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, true);
        //}

        ///// <summary>
        ///// Get a thread
        ///// </summary>
        ///// <param name="threadId"></param>
        ///// <returns></returns>
        //public static DataRow GetForumThread (int threadId)
        //{
        //    string sqlSelect = "SELECT t.thread_id, t.topic_id, t.user_id, t.parent_thread_id, COALESCE(t.subject,top.subject) as subject, t.body_text, t.ip_address, " +
        //        " t.created_date, t.last_updated_date, t.last_updated_user_id, " +
        //        " top.topic_id, f.forum_id, f.community_id " +
        //        " FROM threads t, topics top, forums f " +
        //        " WHERE top.forum_id = f.forum_id " +
        //        " AND t.topic_id = top.topic_id " +
        //        " AND thread_id = @threadId";
			
        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@threadId", threadId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataRow(sqlSelect, parameters, false);
        //}

        ///// <summary>
        ///// Get the threads for a topic
        ///// </summary>
        ///// <param name="topicId"></param>
        ///// <returns></returns>
        //public static PagedDataTable GetForumThreads (int topicId, int pageNumber, int pageSize)
        //{
        //    string selectList = "t.thread_id, t.user_id, t.parent_thread_id, COALESCE(t.subject,top.subject) as subject, t.body_text, t.ip_address, " +
        //        " t.created_date, t.last_updated_date, t.last_updated_user_id, top.topic_id, " +
        //        " u.username, u.signup_date, u.user_id, u.role, u2.username as updatedUsername, " +
        //        " f.community_id, com.name_no_spaces, com.thumbnail_small_path, us.number_forum_posts ";

        //    string tableList = " users u INNER JOIN users_stats us ON u.user_id = us.user_id, " + 
        //        " threads t LEFT OUTER JOIN users u2 ON t.last_updated_user_id = u2.user_id, " +
        //    " topics top, forums f, communities_personal com ";

        //    string whereClause = "f.forum_id = top.forum_id " +
        //        " AND top.topic_id = t.topic_id " +
        //        " AND t.topic_id = @topicId" +
        //        " AND t.user_id = u.user_id" +
        //        " AND t.status_id = " + (int) Constants.eFORUM_STATUS.ACTIVE +
        //        " AND u.user_id = com.creator_id ";

        //    string orderBy = "t.created_date ASC";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@topicId", topicId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderBy, parameters, pageNumber, pageSize);
        //}

        ///// <summary>
        ///// Get the threads for a topic
        ///// </summary>
        ///// <param name="topicId"></param>
        ///// <returns></returns>
        //public static PagedDataTable GetUserForumThreads (int userId, string orderBy, int pageNumber, int pageSize)
        //{
        //    string selectList = "t.thread_id, t.user_id, t.parent_thread_id, COALESCE(t.subject,top.subject) as subject, t.body_text, t.ip_address, " +
        //        " t.created_date, t.last_updated_date, t.last_updated_user_id, top.topic_id, f.community_id";

        //    string tableList = " threads t, topics top, forums f ";

        //    string whereClause = "f.forum_id = top.forum_id " +
        //        " AND top.topic_id = t.topic_id " +
        //        " AND t.user_id = @userId" +
        //        " AND t.status_id = " + (int) Constants.eFORUM_STATUS.ACTIVE;

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@userId", userId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly ().GetPagedDataTable (selectList, tableList, whereClause, orderBy, parameters, pageNumber, pageSize);
        //}

        ///// <summary>
        ///// Get the threads for a topic
        ///// </summary>
        ///// <param name="topicId"></param>
        ///// <returns></returns>
        //public static PagedDataTable GetForumThreads (int communityId, DateTime sinceDate, int pageNumber, int pageSize)
        //{
        //    string selectList = "f.forum_name, t.thread_id, t.user_id, t.parent_thread_id, COALESCE(t.subject,top.subject) as subject, t.body_text, t.ip_address, " +
        //        " t.created_date, t.last_updated_date, t.last_updated_user_id, top.topic_id, " +
        //        " u.username, u.signup_date, u.user_id, u.role, u2.username as updatedUsername, " +
        //        " f.community_id, us.number_forum_posts ";

        //    string tableList = " users u INNER JOIN users_stats us ON u.user_id = us.user_id, " + 
        //        " threads t LEFT OUTER JOIN users u2 ON t.last_updated_user_id = u2.user_id, " +
        //        " topics top, forums f ";

        //    string whereClause = "f.forum_id = top.forum_id " +
        //        " AND top.topic_id = t.topic_id " +
        //        " AND f.community_id = @communityId" +
        //        " AND t.user_id = u.user_id" +
        //        " AND t.status_id = " + (int) Constants.eFORUM_STATUS.ACTIVE +
        //        " AND t.last_updated_date > @sinceDate ";

        //    string orderBy = "t.created_date ASC";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);
        //    parameters.Add ("@sinceDate", sinceDate);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetPagedDataTable(selectList, tableList, whereClause, orderBy, parameters, pageNumber, pageSize);
        //}

        ///// <summary>
        ///// Get the threads for a forumId
        ///// </summary>
        ///// <param name="topicId"></param>
        ///// <returns></returns>
        //public static DataTable GetRssForumThreads (int communityId)
        //{
        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);

        //    // Get all the topcs
        //    string sqlSelect = "SELECT top.topic_id, u.user_id, top.subject, top.body_text, " +
        //        " top.created_date as pubDate, " +
        //        " f.community_id, u.username " +
        //        " FROM topics top, forums f, users u " +
        //        " WHERE f.forum_id = top.forum_id " +
        //        " AND f.community_id = @communityId" +
        //        " AND top.status_id = " + (int) Constants.eFORUM_STATUS.ACTIVE + 
        //        " AND top.created_user_id = u.user_id " +
        //        " ORDER BY top.created_date DESC";

        //    DataTable dtTopics = KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);

        //    // Get all the threads
        //     sqlSelect = "SELECT t.topic_id, t.user_id, top.subject, t.body_text, " +
        //        " t.last_updated_date as pubDate, " +
        //        " f.community_id, u.username " +
        //        " FROM threads t, topics top, forums f, users u " +
        //        " WHERE f.forum_id = top.forum_id " +
        //        " AND top.topic_id = t.topic_id " +
        //        " AND f.community_id = @communityId" +
        //        " AND t.status_id = " + (int) Constants.eFORUM_STATUS.ACTIVE + 
        //        " AND t.user_id = u.user_id " +
        //        " ORDER BY t.created_date DESC";

        //     DataTable dtThreads = KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);

        //    // Combine them
        //    if ( dtTopics.Rows.Count > 0)
        //    {
        //        for (int ii = 0; ii < dtTopics.Rows.Count; ii++)
        //        {
        //            DataRow theRow = dtTopics.Rows[ii];
        //            dtThreads.ImportRow (theRow);
        //        }
        //    }

        //    return dtThreads;
        //}

        ///// <summary>
        ///// Insert a Forum Thread Reply
        ///// </summary>
        //public static int InsertForumThread (int parentThreadId, int topicId, int forumId, int userId, string subject, string bodyText, string ipAddress)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
        //    string sParentThreadId = (parentThreadId == 0)? "NULL": parentThreadId.ToString ();

        //    string sqlString = "INSERT INTO threads ( " +
        //        " parent_thread_id, topic_id, " +
        //        " user_id, ip_address, " +
        //        " created_date, last_updated_date, status_id" +
        //        " ) VALUES (" +
        //        " @parentThreadId, @topicId," +
        //        " @userId, @ipAddress," +
        //        dbUtility.GetCurrentDateFunction () + "," + dbUtility.GetCurrentDateFunction () + ", " + (int) Constants.eFORUM_STATUS.ACTIVE + ")";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@topicId", topicId);
        //    parameters.Add ("@parentThreadId", sParentThreadId);
        //    parameters.Add ("@userId", userId);
        //    parameters.Add ("@ipAddress", ipAddress);
	
        //    int threadId = 0;
        //    dbUtility.ExecuteIdentityInsert (sqlString, parameters, ref threadId);

        //    if (threadId > 0)
        //    {
        //        sqlString = "INSERT INTO " + m_DbNameKanevaNonCluster + ".ft_threads ( " +
        //            " thread_id, subject, body_text " +
        //            " ) VALUES (" +
        //            " @threadId, @subject, @bodyText)";

        //        parameters = new Hashtable ();
        //        parameters.Add ("@threadId", threadId);
        //        parameters.Add ("@subject", subject);
        //        parameters.Add ("@bodyText", bodyText);

        //        dbUtility.ExecuteNonQuery (sqlString, parameters);
        //    }

        //    return 0;
        //}

        ///// <summary>
        ///// UpdateForumThread
        ///// </summary>
        //public static int UpdateForumThread (int threadId, string subject, string bodyText, string ipAddress, int userLastEdited)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

        //    string sqlString = "UPDATE threads SET " +
        //        " ip_address = @ipAddress," +
        //        " last_updated_date = " + dbUtility.GetCurrentDateFunction () +  "," +
        //        " last_updated_user_id = @userLastEdited " + 
        //        " WHERE thread_id = @threadId";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@threadId", threadId);
        //    parameters.Add ("@ipAddress", ipAddress);
        //    parameters.Add ("@userLastEdited", userLastEdited);

        //    dbUtility.ExecuteNonQuery (sqlString, parameters);

        //    sqlString = "UPDATE threads SET " +
        //        " subject = @subject," +
        //        " body_text = @bodyText " +
        //        " WHERE thread_id = @threadId";

        //    parameters = new Hashtable ();			
        //    parameters.Add ("@threadId", threadId);
        //    parameters.Add ("@subject", subject);
        //    parameters.Add ("@bodyText", bodyText);
        //    return dbUtility.ExecuteNonQuery (sqlString, parameters);
        //}

        ///// <summary>
        ///// UpdateForumTopicViewCount
        ///// </summary>
        //public static int UpdateForumTopicViewCount (int topicId)
        //{
        //    string sqlString = "UPDATE topics " +
        //        " SET number_of_views = number_of_views + 1 " +
        //        " WHERE topic_id = @topicId";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@topicId", topicId);
        //    return KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
        //}

        ///// <summary>
        ///// Insert a forum topic
        ///// </summary>
        //public static int InsertForumTopic (int userId, int forumId, int communityId, string subject,
        //    string bodyText, int createdUserId, string createdUsername, string sticky, string important, string ipAddress)
        //{
        //    int ret = 0;

        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

        //    string sqlString = "INSERT INTO topics ( " +
        //        " forum_id, community_id, " +
        //        " created_user_id, sticky, important, " +
        //        " number_of_replies, number_of_views, created_date, last_reply_date, " +
        //        " last_updated_date, ip_address, status_id, last_user_id " +
        //        " ) VALUES (" +
        //        " @forumId, @communityId, " +
        //        " @createdUserId, @sticky, @important," +
        //        "0,0," + dbUtility.GetCurrentDateFunction () + "," + dbUtility.GetCurrentDateFunction () + "," +
        //        dbUtility.GetCurrentDateFunction () + ", @ipAddress," + (int) Constants.eFORUM_STATUS.ACTIVE + ", @createdUserId" +
        //        ")";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@forumId", forumId);
        //    parameters.Add ("@communityId", communityId);
        //    parameters.Add ("@createdUserId", createdUserId);
        //    parameters.Add ("@sticky", sticky);
        //    parameters.Add ("@important", important);
        //    parameters.Add ("@ipAddress", ipAddress);
        //    dbUtility.ExecuteIdentityInsert (sqlString, parameters, ref ret);

        //    if (ret > 0)
        //    {
        //        // Split out into two inserts due to INNO db split and keeping Full Text Indexes
        //        parameters = new Hashtable ();
        //        sqlString = "INSERT INTO " + m_DbNameKanevaNonCluster + ".ft_topics ( " +
        //            " topic_id, subject, body_text, created_username" +
        //            " ) VALUES (" +
        //            " @topicId, @subject, @bodyText, @createdUsername)";

        //        parameters.Add ("@topicId", ret);
        //        parameters.Add ("@subject", subject);
        //        parameters.Add ("@bodyText", bodyText);
        //        parameters.Add ("@createdUsername", createdUsername);
        //        dbUtility.ExecuteNonQuery (sqlString, parameters);
        //    }

        //    return ret;
        //}

        ///// <summary>
        ///// UpdateForumTopic
        ///// </summary>
        ///// <returns></returns>
        //public static int UpdateForumTopic (int topicId, int statusId, int userLastEdited)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

        //    string sqlString = "UPDATE topics SET " +
        //        " status_id = @statusId," +
        //        " last_updated_date = " + dbUtility.GetCurrentDateFunction () +  "," +
        //        " last_updated_user_id = @userLastEdited " + 
        //        " WHERE topic_id = @topicId";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@topicId", topicId);
        //    parameters.Add ("@statusId", statusId);
        //    parameters.Add ("@userLastEdited", userLastEdited);
        //    return dbUtility.ExecuteNonQuery (sqlString, parameters);
        //}

        ///// <summary>
        ///// UpdateForumTopic
        ///// </summary>
        ///// <returns></returns>
        //public static int UpdateForumTopic (int topicId, string subject, string bodyText, int userLastEdited, string sticky, 
        //    string important, string ipAddress)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

        //    string sqlString = "UPDATE topics SET " +
        //        " ip_address = @ipAddress," +
        //        " sticky = @sticky," +
        //        " important = @important," +
        //        " last_updated_date = " + dbUtility.GetCurrentDateFunction () +  "," +
        //        " last_updated_user_id = @userLastEdited " + 
        //        " WHERE topic_id = @topicId";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@topicId", topicId);
        //    parameters.Add ("@userLastEdited", userLastEdited);
        //    parameters.Add ("@sticky", sticky);
        //    parameters.Add ("@important", important);
        //    parameters.Add ("@ipAddress", ipAddress);

        //    dbUtility.ExecuteNonQuery (sqlString, parameters);

        //    // Update the FT table
        //    return UpdateForumTopicFT (topicId, subject, bodyText);
        //}

        ///// <summary>
        ///// UpdateForumTopicFT
        ///// </summary>
        ///// <returns></returns>
        //public static int UpdateForumTopicFT (int topicId, string subject, string bodyText)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

        //    string sqlString = "UPDATE topics SET " +
        //        " subject = @subject," +
        //        " body_text = @bodyText " +
        //        " WHERE topic_id = @topicId";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@topicId", topicId);
        //    parameters.Add ("@subject", subject);
        //    parameters.Add ("@bodyText", bodyText);
        //    return dbUtility.ExecuteNonQuery (sqlString, parameters);
        //}


        ///// <summary>
        ///// Is the user allowed to edit the topic?
        ///// </summary>
        ///// <param name="topicId"></param>
        ///// <param name="userId"></param>
        ///// <returns></returns>
        //public static bool HasEditRights (int topicId, int communityId, int userId, int ownerId)
        //{
        //    bool isModerator = CommunityUtility.IsCommunityModerator (communityId, userId);
        //    bool isTopicLocked = IsTopicLocked (topicId);

        //    // Check permissions
        //    if (communityId == (int) Constants.eCOMMUNITIES.PLAY || communityId == (int) Constants.eCOMMUNITIES.WATCH || communityId == (int) Constants.eCOMMUNITIES.CREATE)
        //    {
        //        // Not editing a community post, make sure they have administrator access or they created the post.	
        //        if (isTopicLocked)
        //        {
        //            // Only an admin can edit a locked Thread
        //            return UsersUtility.IsUserAdministrator ();
        //        }
        //        else
        //        {
        //            return (UsersUtility.IsUserAdministrator () || (ownerId).Equals (userId));
        //        }
        //    }
        //    else
        //    {
        //        // Here we are in a community.
        //        // If it is locked, only the moderator should be able to edit.
        //        if (isTopicLocked)
        //        {
        //            // Only a moderator can edit a locked Thread
        //            return isModerator;
        //        }
        //        else
        //        {
        //            // Make sure they are at least a moderator of this community or topic owner.
        //            return (isModerator || (Convert.ToInt32 (ownerId).Equals (userId)));
        //        }
        //    }
        //}

        ///// <summary>
        ///// Insert Forum Category
        ///// </summary>
        //public static int InsertForumCategory (int communityId, string name, string description)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
        //    int forumCategoryId = 0;

        //    // MySQL having issues with subqueries, so do two here
        //    string sqlSelect = " SELECT COALESCE(MAX(display_order)+1,1) as display_order " +
        //        " FROM forum_categories " +
        //        " WHERE community_id = @communityId";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);
        //    int result = dbUtility.ExecuteScalar (sqlSelect, parameters);

        //    sqlSelect = "INSERT INTO forum_categories " +
        //        "(community_id, name, description," +
        //        "display_order) VALUES (" +
        //        "@communityId, @name, @description," +
        //        result + " )";
		
        //    parameters.Add ("@name", name);
        //    parameters.Add ("@description", description);
        //    dbUtility.ExecuteIdentityInsert (sqlSelect, parameters, ref forumCategoryId);

        //    return forumCategoryId;
        //}

        ///// <summary>
        ///// Insert a new forum
        ///// </summary>
        //public static int InsertForum (int communityId, string forumName, string description, int forumCategoryId)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
        //    int forumId = 0;

        //    // MySQL having issues with subqueries, so do two here
        //    string sqlSelect = " SELECT COALESCE(MAX(display_order)+1,1) as display_order " +
        //        " FROM forums " +
        //        " WHERE community_id = @communityId " + 
        //        " AND forum_category_id = @forumCategoryId";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);
        //    parameters.Add ("@forumCategoryId", forumCategoryId);
        //    int result = dbUtility.ExecuteScalar (sqlSelect, parameters);

        //    sqlSelect = "INSERT INTO forums " +
        //        "(forum_category_id, community_id, forum_name, description, " +
        //        " number_of_topics, created_date, last_post_date, display_order, " +
        //        " status_id) VALUES (" +
        //        "@forumCategoryId, @communityId, @forumName, @description," +  
        //        "0," + dbUtility.GetCurrentDateFunction () + "," + dbUtility.GetCurrentDateFunction () + "," + result + "," +
        //        (int) Constants.eFORUM_STATUS.ACTIVE + ")";

        //    parameters.Add ("@forumName", forumName);
        //    parameters.Add ("@description", description);
        //    dbUtility.ExecuteIdentityInsert (sqlSelect, parameters, ref forumId);

        //    return forumId;
        //}

        ///// <summary>
        ///// Get the number of topics for a given community grouped by forum id
        ///// </summary>
        ///// <param name="community_id"></param>
        ///// <returns></returns>
        //public static DataTable GetForumsPerCategoryCount (int communityId)
        //{
        //    string sqlSelect = "SELECT forum_category_id, COUNT(*) as count " +
        //        " FROM forums WHERE community_id = @communityId " +
        //        " GROUP BY forum_category_id";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().GetDataTable(sqlSelect, parameters);
        //}

        ///// <summary>
        ///// Get the number of forums for a given community
        ///// </summary>
        ///// <param name="community_id"></param>
        ///// <returns></returns>
        //public static int GetForumCount (int communityId)
        //{
        //    string sqlSelect = "SELECT COUNT(*) as count " + 
        //        "FROM forums WHERE community_id = @communityId";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().ExecuteScalar(sqlSelect, parameters);
        //}

        ///// <summary>
        ///// Get the number of forum categories for a given community
        ///// </summary>
        ///// <param name="community_id"></param>
        ///// <returns></returns>
        //public static int GetForumCategoryCount (int communityId)
        //{
        //    string sqlSelect = "SELECT COUNT(*) as count " + 
        //        "FROM forum_categories WHERE community_id = @communityId";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@communityId", communityId);
        //    return KanevaGlobals.GetDatabaseUtilityReadOnly2().ExecuteScalar(sqlSelect, parameters);
        //}

        ///// <summary>
        ///// Delete a forum
        ///// </summary>
        ///// <param name="assetId"></param>
        //public static void DeleteForum (int forumId)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

        //    // DO This since MySQL is having issues with Subqueries
        //    string sqlString = " SELECT topic_id from topics WHERE forum_id = @forumId";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@forumId", forumId);
        //    DataTable dtTopics = dbUtility.GetDataTable (sqlString, parameters);

        //    // Delete the Threads
        //    for (int i = dtTopics.Rows.Count - 1; i >= 0; i--)
        //    {
        //        sqlString = "DELETE FROM threads_base " +
        //            " WHERE topic_id = @topicId";
        //        dbUtility.ExecuteNonQuery (sqlString);
        //    }

        //    // Delete the topics
        //    sqlString = "DELETE FROM topics_base " +
        //        " WHERE forum_id = @forumId";
        //    dbUtility.ExecuteNonQuery (sqlString, parameters);


        //    // Delete the forum
        //    sqlString = "DELETE FROM forums WHERE forum_id = @forumId";
        //    dbUtility.ExecuteNonQuery (sqlString, parameters);
        //}

        ///// <summary>
        ///// DeleteTopic
        ///// </summary>
        //public static void DeleteTopic (int forumId, int topicId)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
        //    Hashtable parameters = new Hashtable ();			

        //    // Reset params
        //    parameters = new Hashtable ();			
        //    parameters.Add ("@topicId", topicId);

        //    // Delete the threads for this topic
        //    string sqlString = "DELETE FROM threads_base " +
        //        " WHERE topic_id = @topicId";
        //    dbUtility.ExecuteNonQuery (sqlString, parameters);

        //    // Delete the topics
        //    sqlString = "DELETE FROM topics_base " +
        //        " WHERE topic_id = @topicId";

        //    dbUtility.ExecuteNonQuery (sqlString, parameters);
        //}

        ///// <summary>
        ///// DeleteThread
        ///// </summary>
        //public static void DeleteThread (int topicId, int threadId)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
        //    Hashtable parameters = new Hashtable ();			

        //    string  sqlString = "DELETE FROM threads_base " +
        //        " WHERE thread_id = @threadId";
		
        //    parameters.Add ("@threadId", threadId);
        //    dbUtility.ExecuteNonQuery (sqlString, parameters);
        //}

        ///// <summary>
        ///// Delete a category
        ///// </summary>
        ///// <param name="assetId"></param>
        //public static void DeleteCategory (int forumCategoryId)
        //{
        //    // Delete all the forums
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

        //    // DO This since MySQL is having issues with Subqueries
        //    string sqlString = " SELECT forum_id from forums WHERE forum_category_id = @forumCategoryId";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@forumCategoryId", forumCategoryId);
        //    DataTable dtForums = dbUtility.GetDataTable (sqlString, parameters);

        //    // Delete the Threads
        //    for (int i = dtForums.Rows.Count - 1; i >= 0; i--)
        //    {
        //        DeleteForum (Convert.ToInt32 (dtForums.Rows [i]["forum_id"]));
        //    }

        //    // Delete the category

        //    sqlString = "DELETE FROM forum_categories WHERE forum_category_id = @forumCategoryId ";
        //    dbUtility.ExecuteNonQuery (sqlString, parameters);
        //}

        ///// <summary>
        ///// Lock a forum
        ///// </summary>
        ///// <param name="assetId"></param>
        //public static void LockForumFlip (int forumId)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

        //    int statusId = (int) Constants.eFORUM_STATUS.LOCKED;

        //    // If it is locked, set it to active
        //    DataRow drForum = GetForum (forumId);
        //    if (IsStatusLocked (Convert.ToInt32 (drForum ["status_id"])))
        //    {
        //        statusId = (int) Constants.eFORUM_STATUS.ACTIVE;
        //    }

        //    // Update the forum
        //    string sqlString = "UPDATE forums SET " +
        //        " status_id = @statusId " +
        //        " WHERE forum_id = @forumId";

        //    Hashtable parameters = new Hashtable ();			
        //    parameters.Add ("@statusId", statusId);
        //    parameters.Add ("@forumId", forumId);
        //    dbUtility.ExecuteNonQuery (sqlString, parameters);
        //}

        ///// <summary>
        ///// Is the forum locked?
        ///// </summary>
        ///// <param name="forumId"></param>
        ///// <returns></returns>
        //public static bool IsForumLocked (int forumId)
        //{
        //    // Is the forum locked
        //    DataRow drForum = GetForum (forumId);
        //    return IsStatusLocked (Convert.ToInt32 (drForum ["status_id"]));
        //}

        //public static bool IsStatusLocked (int statusId)
        //{
        //    return (statusId == (int) Constants.eFORUM_STATUS.LOCKED);
        //}

        ///// <summary>
        ///// Is the topic locked?
        ///// </summary>
        ///// <returns></returns>
        //public static bool IsTopicLocked (int topicId)
        //{
        //    DataRow drTopic = GetForumTopic (topicId);

        //    // Is the Topic locked? This topic is locked if the forum is locked
        //    return (IsForumLocked (Convert.ToInt32 (drTopic ["forum_id"])) || IsStatusLocked (Convert.ToInt32 (drTopic ["status_id"])));
        //}

        ///// <summary>
        ///// Is the topic locked?
        ///// </summary>
        ///// <returns></returns>
        //public static bool IsTopicLockedByStatusId (int forumStatusId, int topicStatusId)
        //{
        //    // Is the Topic locked? This topic is locked if the forum is locked
        //    return (IsStatusLocked (forumStatusId) || IsStatusLocked (topicStatusId));
        //}


        ///// <summary>
        ///// Search Forums
        ///// </summary>
        //public static PagedDataTable SearchForums (string searchString, int communityId, int userId, int pageNumber, int pageSize)
        //{
        //    DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly2();
        //    Hashtable parameters = new Hashtable ();

        //    // ************************************************
        //    // TOPICS
        //    // ************************************************
        //    string sqlSelectList = "0 as thread_id, t.community_id, t.topic_id, t.subject, t.status_id, t.number_of_views, " +
        //        " t.last_updated_date, t.created_date, t.last_reply_date, t.important, t.sticky, t.number_of_replies, " +
        //        " t.created_username as username, t.created_user_id, t.last_thread_id ";

        //    string sqlTableList = "topics t, communities c";

        //    string sqlWhereClause = "MATCH (t.created_username, t.subject, t.body_text) AGAINST (@searchString)" +
        //        " AND viewable = 'Y' " +
        //        " AND t.community_id = c.community_id ";

        //    // Do they want a specific community?
        //    if (communityId > 0)
        //    {
        //        if (CommunityUtility.IsKanevaMainCommunity (communityId) || CommunityUtility.IsCommunityPublic (communityId))
        //        {
        //            // It is a public community, let them search
        //            sqlWhereClause += " AND t.community_id = @communityId " +
        //                " AND c.is_public <> 'N'";

        //            parameters.Add ("@communityId", communityId);
        //        }
        //        else
        //        {
        //            // It is private, they must be an active member to search it
        //            sqlTableList += ", community_members cm";

        //            sqlWhereClause += " AND t.community_id = @communityId " +
        //                " AND cm.community_id = t.community_id " +
        //                " AND cm.user_id = @userId" +
        //                " AND cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE;

        //            parameters.Add ("@communityId", communityId);
        //            parameters.Add ("@userId", userId);
        //        }
        //    }
        //    else
        //    {
        //        // All forums in all channels
        //        // For private channels, make sure they are a member
        //        sqlTableList += " LEFT OUTER JOIN community_members cm ON " +
        //            " t.community_id = cm.community_id AND cm.community_id = t.community_id AND cm.user_id = @userId AND  cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE;

        //        sqlWhereClause += " AND ((c.is_public <> 'N') OR (c.is_public = 'N' AND cm.user_id IS NOT NULL))";
        //        parameters.Add ("@userId", userId);
        //    }

        //    // ************************************************
        //    // THREADS - Also search the Thread table
        //    // ************************************************
        //    string sqlThreadSelectList = "th2.thread_id, t2.community_id, t2.topic_id, t2.subject, t2.status_id, t2.number_of_views, " +
        //        " t2.last_updated_date, t2.created_date, t2.last_reply_date, t2.important, t2.sticky, t2.number_of_replies, " +
        //        " t2.created_username as username, t2.created_user_id, t2.last_thread_id";

        //    string sqlThreadTableList = "threads th2, topics t2, communities c2";

        //    string sqlThreadWhereClause = "MATCH (th2.subject, th2.body_text) AGAINST (@searchString)" +
        //        " AND th2.topic_id = t2.topic_id " +
        //        " AND t2.viewable = 'Y' " +
        //        " AND t2.community_id = c2.community_id";

        //    // Do they want a specific community?
        //    if (communityId > 0)
        //    {
        //        if (CommunityUtility.IsKanevaMainCommunity (communityId) || CommunityUtility.IsCommunityPublic (communityId))
        //        {
        //            // It is a public community, let them search
        //            sqlThreadWhereClause += " AND t2.community_id = @communityId2 " +
        //                " AND c2.is_public <> 'N'";
        //            parameters.Add ("@communityId2", communityId);
        //        }
        //        else
        //        {
        //            // It is private, they must be an active member to search it
        //            sqlThreadTableList += ", community_members cm2";

        //            sqlThreadWhereClause += " AND t2.community_id = @communityId2 " +
        //                " AND cm2.community_id = t2.community_id " +
        //                " AND cm2.user_id = @userId2" +
        //                " AND cm2.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE;

        //            parameters.Add ("@communityId2", communityId);
        //            parameters.Add ("@userId2", userId);
        //        }
        //    }
        //    else
        //    {
        //        // All forums in all channels
        //        // For private channels, make sure they are a member
        //        sqlThreadTableList += " LEFT OUTER JOIN community_members cm2 ON " +
        //            " t2.community_id = cm2.community_id AND cm2.community_id = t2.community_id AND cm2.user_id = @userId2 AND cm2.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE;

        //        sqlThreadWhereClause += " AND ((c2.is_public <> 'N') OR (c2.is_public = 'N' AND cm2.user_id IS NOT NULL))";
        //        parameters.Add ("@userId2", userId);
        //    }

        //    string sqlTopics = "SELECT " + sqlSelectList + 
        //        " FROM " + sqlTableList + 
        //        " WHERE " + sqlWhereClause;

        //    string sqlThread = " SELECT " + sqlThreadSelectList +
        //        " FROM " + sqlThreadTableList +
        //        " WHERE " + sqlThreadWhereClause;

        //    parameters.Add ("@searchString", searchString);
        //    return dbUtility.GetPagedDataTableUnion (sqlTopics + " UNION " + sqlThread, "", parameters, pageNumber, pageSize);
        //}

		/// <summary>
		/// Database name for tables that are no in the cluster
		/// </summary>
		private static string m_DbNameKanevaNonCluster = "my_kaneva";

	}
}
