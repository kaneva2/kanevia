///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Text;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for TorrentUtility.
	/// </summary>
	public class TorrentUtility
	{
		private const string ANNOUNCE = "announce";
		private const char COLON = ':';
		private const string ANNOUNCE_LIST = "13:announce-listl";
		public TorrentUtility()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		/// <summary>
		/// return true if torrent contains announce url list
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static bool HasAnnounceList(Stream stream)
		{
			//reset the stream
			stream.Seek(0,SeekOrigin.Begin);
			byte [] bBufferRead = new byte [stream.Length];
			stream.Read (bBufferRead, 0, (int) stream.Length);

			return IndexOf(bBufferRead, ANNOUNCE_LIST) != -1;
		}

		/// <summary>
		/// Returns the announce url in the torrent
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static string GetAnnounceUrl(Stream stream)
		{
			//reset the stream
			stream.Seek(0,SeekOrigin.Begin);

			byte [] bBufferRead = new byte [stream.Length];
			stream.Read (bBufferRead, 0, (int) stream.Length);
			
			string len = "";

			int start = IndexOf(bBufferRead, ANNOUNCE) + ANNOUNCE.Length;
			int end = 0;

			int i = start;
			while(bBufferRead[i] != COLON)
			{
				len += (char)bBufferRead[i];
				i++;
			}
			
			end = start + len.Length + 1 + int.Parse(len);
			StringBuilder sb = new StringBuilder();
			for(i = 0; i < bBufferRead.Length; i++)
			{
				int read = bBufferRead[i];
				if( i == end)
				{
					break;
				} 
				else if( i >= start)
				{
					char c = (char) read;
					sb.Append(c);
				}
			}

			string announceUrl = sb.ToString();

			if(announceUrl != null && announceUrl.IndexOf(COLON) != -1)
			{
				return announceUrl.Substring(announceUrl.IndexOf(COLON) + 1);
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="trackerUrl"></param>
		/// <returns>bytes represention of updated torrent dictionary</returns>
		public static byte[] UpdateAnnounceUrl(Stream stream, string trackerUrl)
		{
			//reset the stream
			stream.Seek(0,SeekOrigin.Begin);

			byte [] bBufferRead = new byte [stream.Length];
			stream.Read (bBufferRead, 0, (int) stream.Length);

			
			string len = "";

			int start = IndexOf(bBufferRead, ANNOUNCE) + ANNOUNCE.Length;
			int end = 0;

			int i = start;
			while(bBufferRead[i] != COLON)
			{
				len += (char)bBufferRead[i];
				i++;
			}
			
			end = start + len.Length + 1 + int.Parse(len);

			//remove announce-list
			string announceString = trackerUrl.Length.ToString() + COLON + trackerUrl;

			return Replace(bBufferRead, start, end, announceString);
		}

		/// <summary>
		/// returns -1 if not found
		/// </summary>
		/// <param name="bytes"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		private static int IndexOf(byte[] bytes, string key)
		{
			int retVal = -1;
			String matched = "";

			for(int i = 0; i < bytes.Length; i++)
			{
				if(key.Equals(matched))
				{
					retVal = i - key.Length;
					break;
				}
				else
				{
					int read = bytes[i];
					if(read == key[matched.Length])
					{
						char c = (char) read;
						matched += c;
					}
					else
					{
						matched = "";
					}
				}
			}
			return retVal;
		}

		private static byte[] Replace(byte[] bytes, int start, int end, string replacement)
		{
			byte[] retVal = new byte[bytes.Length - (end - start) + replacement.Length];
			int j = 0;
			for(int i = 0; i < start; i++)
			{
				retVal[j++] = bytes[i];
			}
			for(int i = 0; i < replacement.Length; i++)
			{
				retVal[j++] = (byte)replacement[i];
			}
			for(int i = end; i < bytes.Length; i++)
			{
				retVal[j++] = bytes[i];
			}
			return retVal;
		}

		/// <summary>
		/// GetNewAnnounceURL
		/// </summary>
		/// <param name="announceURL"></param>
		/// <param name="token"></param>
		/// <returns></returns>
		public static string GetNewAnnounceURL (string announceURL, string token)
		{
			// Examples http://tracker.kaneva.com becomes http://token.kaneva.com
			//			https://tracker.kaneva.com becomes https://token.kaneva.com

			const string cHTTP_SEP = "://";

			int iStartIndex = announceURL.IndexOf (cHTTP_SEP);

			// Everything after the HTTP(s)://
			string remanderAnnoundURL = announceURL.Substring (iStartIndex + cHTTP_SEP.Length);

			// The first part of the URL (i.e. tracker)
			int iStartURL = remanderAnnoundURL.IndexOf (".");
			if (iStartURL > 0)
			{
				remanderAnnoundURL = remanderAnnoundURL.Substring (iStartURL);
			}

			return announceURL.Substring (0, iStartIndex + cHTTP_SEP.Length) +	// http://
				token +															// token replaces first part (i.e. tracker1)			
				remanderAnnoundURL;												// The rest
		}
	}
}
