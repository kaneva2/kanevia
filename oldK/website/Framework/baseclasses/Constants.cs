///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Text;

namespace KlausEnt.KEP.Kaneva
{
  /// <summary>
  /// Summary description for Constants.
  /// </summary>
  public class Constants
  {
    static Constants()
    {
      
      COUNTER_FONTS = new SortedList();
      COUNTER_FONTS.Add(8,"8px");
      COUNTER_FONTS.Add(9,"9px");
      COUNTER_FONTS.Add(10,"10px");
      COUNTER_FONTS.Add(11,"11px");
      COUNTER_FONTS.Add(12,"12px");
      COUNTER_FONTS.Add(14,"14px");
      COUNTER_FONTS.Add(16,"16px");
      COUNTER_FONTS.Add(18,"18px");
      COUNTER_FONTS.Add(20,"20px");
      COUNTER_FONTS.Add(22,"22px");
      COUNTER_FONTS.Add(24,"24px");
      COUNTER_FONTS.Add(26,"26px");
      COUNTER_FONTS.Add(28,"28px");
      COUNTER_FONTS.Add(36,"36px");
      COUNTER_FONTS.Add(48,"48px");
      COUNTER_FONTS.Add(72,"72px");

      COUNTER_STYLES = new Hashtable();
      COUNTER_STYLES.Add("bold","bold");
      COUNTER_STYLES.Add("normal","normal");


      EVENT_TYPES = new SortedList();
      EVENT_TYPES.Add(1,"Grand Opening");
      EVENT_TYPES.Add(2,"Party");
      EVENT_TYPES.Add(3,"Competition");
      EVENT_TYPES.Add(4,"Discussion/Debate");
      EVENT_TYPES.Add(5,"Fashion Show");
      EVENT_TYPES.Add(6,"Tour");
      EVENT_TYPES.Add(7,"Game");
      EVENT_TYPES.Add(8,"Date");
      EVENT_TYPES.Add(9,"Speed Dating");
      EVENT_TYPES.Add(10,"Trade/Job");
      EVENT_TYPES.Add(11,"Live Streaming");
      EVENT_TYPES.Add(12,"Reunion");
      EVENT_TYPES.Add(13,"Birthday Party");
      EVENT_TYPES.Add(14,"Movie/TV Night");
      EVENT_TYPES.Add(15,"Dance Party");
      EVENT_TYPES.Add(16,"Art Gallery Showing");
      EVENT_TYPES.Add(17,"Meet Up");
      EVENT_TYPES.Add(18,"Professional Event");
      EVENT_TYPES.Add(19,"Club/Group");
      EVENT_TYPES.Add(20,"Teen Scene");
      EVENT_TYPES.Add(21,"College Event");
      EVENT_TYPES.Add(22,"Sports/League");
      EVENT_TYPES.Add(23,"Housewarming");
      EVENT_TYPES.Add(24, "Wedding/Engagement");


     

      OPT_MONTH = new SortedList();
      OPT_MONTH.Add(1,"January");
      OPT_MONTH.Add(2,"February");
      OPT_MONTH.Add(3,"March");
      OPT_MONTH.Add(4,"April");
      OPT_MONTH.Add(5,"May");
      OPT_MONTH.Add(6,"June");
      OPT_MONTH.Add(7,"July");
      OPT_MONTH.Add(8,"August");
      OPT_MONTH.Add(9,"September");
      OPT_MONTH.Add(10,"October");
      OPT_MONTH.Add(11,"November");
      OPT_MONTH.Add(12,"December");

      OPT_DAY = new SortedList();
      for(int i = 1; i < 31; i++)
      {
        OPT_DAY.Add(i, i);
      }
      OPT_YEAR = new SortedList();
      int yr = DateTime.Now.Year;
      for(int i = 0; i < 5; i++)
      {
        OPT_YEAR.Add(yr, yr);
        yr++;
      }
      OPT_HOUR = new SortedList();
      OPT_HOUR.Add(0,"12 am");
      OPT_HOUR.Add(1,"1 am");
      OPT_HOUR.Add(2,"2 am");
      OPT_HOUR.Add(3,"3 am");
      OPT_HOUR.Add(4,"4 am");
      OPT_HOUR.Add(5,"5 am");
      OPT_HOUR.Add(6,"6 am");
      OPT_HOUR.Add(7,"7 am");
      OPT_HOUR.Add(8,"8 am");
      OPT_HOUR.Add(9,"9 am");
      OPT_HOUR.Add(10,"10 am");
      OPT_HOUR.Add(11,"11 am");
      OPT_HOUR.Add(12,"12 pm");
      OPT_HOUR.Add(13,"1 pm");
      OPT_HOUR.Add(14,"2 pm");
      OPT_HOUR.Add(15,"3 pm");
      OPT_HOUR.Add(16,"4 pm");
      OPT_HOUR.Add(17,"5 pm");
      OPT_HOUR.Add(18,"6 pm");
      OPT_HOUR.Add(19,"7 pm");
      OPT_HOUR.Add(20,"8 pm");
      OPT_HOUR.Add(21,"9 pm");
      OPT_HOUR.Add(22,"10 pm");
      OPT_HOUR.Add(23,"11 pm");

      OPT_MINUTE = new SortedList();
      OPT_MINUTE.Add(0,":00");
      OPT_MINUTE.Add(30,":30");
      
      OPT_TIME_ZONE = new SortedList();


      FILE_TYPE_VIDEO = new ArrayList();
      FILE_TYPE_VIDEO.Add (".FLV") ; FILE_TYPE_VIDEO.Add (".MPG") ; FILE_TYPE_VIDEO.Add (".MPE") ;
      FILE_TYPE_VIDEO.Add (".MPA") ; FILE_TYPE_VIDEO.Add (".MPEG") ; FILE_TYPE_VIDEO.Add (".MOV") ;
      FILE_TYPE_VIDEO.Add (".AVI") ; FILE_TYPE_VIDEO.Add (".WMV") ; FILE_TYPE_VIDEO.Add (".QT") ;
      FILE_TYPE_VIDEO.Add (".MP4") ; FILE_TYPE_VIDEO.Add (".MP2") ; FILE_TYPE_VIDEO.Add (".ASF") ;
      FILE_TYPE_VIDEO.Add (".M4V") ; FILE_TYPE_VIDEO.Add (".DIVX");

      FILE_TYPE_MUSIC = new ArrayList();
      FILE_TYPE_MUSIC.Add (".MP3");

      FILE_TYPE_GAME = new ArrayList();
      FILE_TYPE_GAME.Add(".SWF");

      FILE_TYPE_PICTURE = new ArrayList();
      FILE_TYPE_PICTURE.Add (".JPG") ;
      FILE_TYPE_PICTURE.Add (".GIF");

      StringBuilder sb = new StringBuilder() ;
      sb.Append(@"^(?:[a-zA-Z]:)?(?:\\{1,2}|/)(?:.*?(?:\\|/))+.*?\.(?:");
      //@"[^\s]{1,}.jpg|[^\s]{1,}.gif|[^\s]{1,}.avi|[^\s]{0}";
      foreach (string entry in FILE_TYPE_VIDEO)
      {
        //sb.Append(@"[^\s]{1,}.");
        for(int i = 1; i < entry.Length; i++)
        {
          sb.Append("[").Append(entry.Substring(i,1).ToUpper()).Append(entry.Substring(i,1).ToLower()).Append("]");
        }
        sb.Append("|");
      }
      foreach (string entry in FILE_TYPE_MUSIC)
      {
        //sb.Append(@"[^\s]{1,}.");
        for(int i = 1; i < entry.Length; i++)
        {
          sb.Append("[").Append(entry.Substring(i,1).ToUpper()).Append(entry.Substring(i,1).ToLower()).Append("]");
        }
        sb.Append("|");
      }
      foreach (string entry in FILE_TYPE_GAME)
      {
        //sb.Append(@"[^\s]{1,}.");
        for(int i = 1; i < entry.Length; i++)
        {
          sb.Append("[").Append(entry.Substring(i,1).ToUpper()).Append(entry.Substring(i,1).ToLower()).Append("]");
        }
        sb.Append("|");
      }
      foreach (string entry in FILE_TYPE_PICTURE)
      {
        //sb.Append(@"[^\s]{1,}.");
        for(int i = 1; i < entry.Length; i++)
        {
          sb.Append("[").Append(entry.Substring(i,1).ToUpper()).Append(entry.Substring(i,1).ToLower()).Append("]");
        }
        sb.Append("|");
      }
      sb.Remove(sb.Length - 1, 1); //remove last |
      sb.Append(@")$");
      //sb.Append(@"[^\s]{0}");
      //			string fileTypes = sb.ToString();
      //			fileTypes = fileTypes.Substring(0,fileTypes.Length-1);//remove the last |
      //			fileTypes = fileTypes.Replace(".","\\.");
      //VALIDATION_REGEX_MEDIA_FILE_FORMAT = "^|" + fileTypes + "$";
      //VALIDATION_REGEX_MEDIA_FILE_FORMAT = @"/^$|\\.[Ff][Ll][Vv]$|\\.[mM][Pp][Gg]$|\\.[Mm][Pp][Ee]$|\\.[Mm][Pp][Aa]$|\\.[Mm][Pp][Ee][Gg]$|\\.[Mm][Oo][Vv]$|\\.[Aa][Vv][Ii]$|\\.[Ww][Mm][Vv]$|\\.[Qq][Tt]$|\\.[Mm][Pp]4$|\\.[Mm][Pp]2$|\\.[Aa][Ss][Ff]$|\\.[Mm]4[Vv]$|\\.[Dd][Ii][Vv][Xx]$|\\.[Mm][Pp]3$|\\.[Ss][Ww][Ff]$|\\.[Jj][Pp][Gg]$|\\.[Gg][Ii][Ff]$/s";
      VALIDATION_REGEX_MEDIA_FILE_FORMAT = sb.ToString();
    }
    public Constants()
    {
    }


        // Request Tracking Metrics
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_GAME_API_REQUEST = 1;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_SHOP_PURCHASE = 10;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_BUNDLE_UPLOAD = 11;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_DEED_UPLOAD = 12;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_SHOP_ITEM_UPLOAD = 13;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_SHOP_MEMBER_DESIGN_UPLOAD = 14;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_CONTEST_CREATED = 15;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_CONTEST_ENTERED = 16;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_CONTEST_VOTED = 17;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_CONTEST_FOLLOWING = 18;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_CONTEST_WINNER = 19;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_BLOG = 20;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_CREATION = 21; // Deprecated. Replaced by "Event Invite" + "Website Message Type" combo
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_RAVE_3DAPP = 22;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_RAVE_HOME = 23;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_RAVE_HANGOUT = 24;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_INVITE = 25;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_UPDATE = 26;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_REMINDER = 27;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_BLAST = 28;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_BLAST_COMMENT = 29;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_BIRTHDAY_EMAIL_VISIT_PROFILE = 30;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_BIRTHDAY_EMAIL_SEND_MESSAGE = 31;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_BIRTHDAY_EMAIL_PLAN_PARTY = 32;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_BIRTHDAY_EMAIL_CREATE_PARTY = 33;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_WEB_WORLD_CREATION = 34;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_WORLD_PROFILE_PLAY_NOW = 35;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_EXPLORE_WORLDS_PLAY_NOW = 36;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_CAMERA_BLAST = 37;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_RANDOM_EVENT_INVITE = 38;
        public const int cREQUEST_TRACKING_REQUEST_TYPE_ID_GAME_API_GIFT_REQUEST = 39;

        public const int cREQUEST_TRACKING_MESSAGE_TYPE_ID_PM = 1;
        public const int cREQUEST_TRACKING_MESSAGE_TYPE_ID_BLAST = 2;
        public const int cREQUEST_TRACKING_MESSAGE_TYPE_ID_EMAIL = 3;
        public const int cREQUEST_TRACKING_MESSAGE_TYPE_ID_IN_WORLD = 4;
        public const int cREQUEST_TRACKING_MESSAGE_TYPE_ID_WEBSITE = 5;
        public const int cREQUEST_TRACKING_MESSAGE_TYPE_ID_KIM = 6;

        public const string cREQUEST_TRACKING_URL_PARAM = "RTSID";
    public const string cREQUEST_TRACKING_URL_IE_PARAM = "IsIE";
    public const string cREQUEST_TRACKING_URL_USERAGENT_PARAM = "UserAgent";


    public const string CURR_KPOINT = "KPOINT";
    public const string CURR_MPOINT = "MPOINT";
    public const string CURR_DOLLAR = "DOLLAR";
    public const string CURR_GPOINT = "GPOINT";
        public const string CURR_CREDITS = CURR_KPOINT;
        public const string CURR_REWARDS = CURR_GPOINT;

        public const string CURR_USERSELECTED = "UserSelect";

    public const string CURR_KPOINT_SYMBOL_HTML = "&#1036;";
    public const string CURR_KPOINT_SYMBOL_NON_HTML = "K";

    public const string KPOINT_ORDER_ID_PREFIX = "KEN-KP-";
    public const string ITEM_ORDER_ID_PREFIX = "KEN-A-";

    public const string BLOG_NO_RATING = "N/A";
    public const double	BLOG_NO_RATING_INT = 0;

    public const string CONTENT_TYPE_ADULT = "Y";
    public const string CONTENT_TYPE_TEEN = "N";
    public const string CONTENT_TYPE_GENERAL = "C";

    public const string STR_TRUE = "Y";
    public const string STR_FALSE = "N";

    public const string COUNTRY_CODE_UNITED_STATES = "US";
    public const string COUNTRY_CODE_CANADA = "CA";

    public const string DROPDOWN_SEPERATOR = "----------------------------------------";

    public const int NOTHING_SELECTED = -1;

    // Online status
    public const string ONLINE_USTATE_ON = "On";
    public const string ONLINE_USTATE_OFF = "Off";
    public const string ONLINE_USTATE_INWORLD = "InWorld";
    public const string ONLINE_USTATE_ONINWORLD = "OnInWorld";

    // Featured Item
    public const Double C_FEATURED_ASSET_PRICE = 500;
    public const int C_FEATURED_ASSET_MONTHS = 1;

    // Buy a pilot game for one day
    public const Double C_PILOT_GAME_ONE_DAY_PRICE = 100;
    public const int C_PILOT_GAME_DAYS = 1;

    // Parameters
    public const string PARAM_SORT = "pSRT";
    public const string PARAM_SORT_ORDER = "pSRTORD";
    public const string PARAM_PAGE_NUMBER = "pPNU";
    public const string PARAM_FILTER = "pFIL";

    public enum eSUPPORTED_DATABASES
    {
      SQLSERVER = 1,
      MYSQL = 2
    }

    // ***********************************************
    // Application Contants
    // ***********************************************
    public const string CURRENT_USER_COUNT = "CUC";

    public const string CACHE_MEDIA_CATEGORIES = "medCatsTest";
    public const string CACHE_ASSET_CATEGORIES = "assCatsTest";
    public const string CACHE_GAME_CATEGORIES = "gamCatsTest";

    public const string CACHE_CONTEST_RANDOM = "contestRandom";

    public enum eWEB_PAGE_TYPE
    {
      COMMUNITY = 1,
      PROFILE = 2,
      MEDIA = 3,
      CORPORATE = 4
    }

        public enum ePROMOTION_TYPE
        {
            CREDITS = 0,
            ACCESS = 1,
            SPECIAL = 2,
            SPECIAL_ONE_TIME = 3,
            SPECIAL_FIRST_TIME = 4,
            REFERRAL = 5
        }

        public enum eRESTRICTION_TYPE
        {
            RESERVED = 1,
            POTTY_MOUTH = 2,
            ALL = 99
        }

        public enum eMATCH_TYPE
        {
            EXACT = 0,
            ANYWHERE = 1
        }

        public enum ePASS_TYPE
        {
            GENERAL = 0,
            ACCESS = 1,
            VIP = 2
        }

        public enum ePLAYER_NAME_COLORS
        {
            DEFAULT = 0,
            VIP = 1,
            GM = 3
        }

        public enum eERROR_TYPE
        {
            CHECKOUT_CLOSED = 0,
            PROFILE_NOT_FOUND = 1,
            COMMUNITY_NOT_FOUND = 2,
            THEEDAPP_NOT_FOUND = 3,
            WEBPAGE_NOT_FOUND = 4,
            ACCESS_DENIED = 5,
            GENERIC = 6,
            INVALID_ACCOUNT = 7
        }


        // ***********************************************
    // Image Size Contants
    // ***********************************************
    #region Image Size Contants

    // Channel
    public const int CHANNEL_THUMB_SMALL_HEIGHT = 56;
    public const int CHANNEL_THUMB_SMALL_WIDTH = 75;
    public const int CHANNEL_THUMB_MEDIUM_HEIGHT = 75;
    public const int CHANNEL_THUMB_MEDIUM_WIDTH = 100;
    public const int CHANNEL_THUMB_LARGE_HEIGHT = 98;
    public const int CHANNEL_THUMB_LARGE_WIDTH = 130;

    // Profile
    public const int PEOPLE_THUMB_SMALL_HEIGHT = 56;
    public const int PEOPLE_THUMB_SMALL_WIDTH = 75;
    public const int PEOPLE_THUMB_MEDIUM_HEIGHT = 75;
    public const int PEOPLE_THUMB_MEDIUM_WIDTH = 100;
    public const int PEOPLE_THUMB_LARGE_HEIGHT = 98;
    public const int PEOPLE_THUMB_LARGE_WIDTH = 130;
    public const int PEOPLE_THUMB_XLARGE_HEIGHT = 600;
    public const int PEOPLE_THUMB_XLARGE_WIDTH = 800;

        public const int PEOPLE_THUMB_SQ_HEIGHT = 50;
        public const int PEOPLE_THUMB_SQ_WIDTH = 50;

    // Media
    public const int MEDIA_THUMB_SMALL_HEIGHT = 56;
    public const int MEDIA_THUMB_SMALL_WIDTH = 75;
    public const int MEDIA_THUMB_MEDIUM_HEIGHT = 75;
    public const int MEDIA_THUMB_MEDIUM_WIDTH = 100;
    public const int MEDIA_THUMB_LARGE_HEIGHT = 98;
    public const int MEDIA_THUMB_LARGE_WIDTH = 130;

    // Photo
    public const int PHOTO_THUMB_SMALL_HEIGHT = 56;
    public const int PHOTO_THUMB_SMALL_WIDTH = 75;
    public const int PHOTO_THUMB_MEDIUM_HEIGHT = 75;
    public const int PHOTO_THUMB_MEDIUM_WIDTH = 100;

    public const int PHOTO_THUMB_LARGE_HEIGHT = 120;
    public const int PHOTO_THUMB_LARGE_WIDTH = 160;
    public const int PHOTO_THUMB_XLARGE_HEIGHT = 240;
    public const int PHOTO_THUMB_XLARGE_WIDTH = 320;

    public const int PHOTO_THUMB_ASSETDETAILS_HEIGHT = 768;
    public const int PHOTO_THUMB_ASSETDETAILS_WIDTH = 498;

        // Wok Item Shopping
        public const int WOK_ITEM_THUMB_SMALL_HEIGHT = 25;
        public const int WOK_ITEM_THUMB_SMALL_WIDTH = 25;
        public const int WOK_ITEM_THUMB_MEDIUM_HEIGHT = 40;
        public const int WOK_ITEM_THUMB_MEDIUM_WIDTH = 40;
        public const int WOK_ITEM_THUMB_LARGE_HEIGHT = 90;
        public const int WOK_ITEM_THUMB_LARGE_WIDTH = 90;
        public const int WOK_ITEM_THUMB_ASSETDETAILS_HEIGHT = 270;
        public const int WOK_ITEM_THUMB_ASSETDETAILS_WIDTH = 350;

        // Achievement
        public const int ACHIEVEMENT_THUMB_SMALL_HEIGHT = 32;
        public const int ACHIEVEMENT_THUMB_SMALL_WIDTH = 32;
        public const int ACHIEVEMENT_THUMB_MEDIUM_HEIGHT = 64;
        public const int ACHIEVEMENT_THUMB_MEDIUM_WIDTH = 64;
        public const int ACHIEVEMENT_THUMB_LARGE_HEIGHT = 128;
        public const int ACHIEVEMENT_THUMB_LARGE_WIDTH = 128;

    #endregion

    // ***********************************************
    // Channel Layout Constants
    // ***********************************************
    #region Channel Layout Constants

    public enum eZONE_ALIGNMENT
    {
      BODY_RIGHT = 1,
      BODY_LEFT = 0
    }

    public enum eMODULE_ZONE
    {
      HEADER = 1,
      BODY = 2,
            BODY_TOP = 6,
      COLUMN = 3,
      DELETE = 4,
      FOOTER = 5
    }

    public enum eZONE_WIDTHS
    {
      ZONE_HEADER = 990,
      ZONE_BODY = 605,
      ZONE_COLUMN = 385
    }

    #endregion

    // ***********************************************
    // Cache Contants
    // ***********************************************
    #region Cache Contants

    public const string CACHE_TOP_NEW_GAMES = "newGames";
    public const string CACHE_TOP_GAMES = "topGames";
    public const string CACHE_TOP_SELLING_GAMES = "topGames";

    public const string CACHE_TOP_NEW_ASSETS = "newAssets";
    public const string CACHE_TOP_ASSETS = "topAssets";
    public const string CACHE_TOP_SELLING_ASSETS = "topSellingAssets";
    public const string CACHE_TOP_DOWNLOADED_ASSETS = "topdlAssets";

    public const string CACHE_TOP_NEW_ITEMS = "newItems";
    public const string CACHE_TOP_ITEMS = "topItems";

    public const string CACHE_TOP_NEW_MEDIA = "newMedia";
    public const string CACHE_TOP_MEDIA = "topMedia";
    public const string CACHE_TOP_SELLING_MEDIA = "topSellMedia";
    public const string CACHE_TOP_DOWNLOADED_MEDIA = "topdlMedia";

    public const string CACHE_HOME_TOP_NEW_MEDIA = "hometnm";
    public const string CACHE_HOME_TOP_NEW_GAMES = "hometng";
    public const string CACHE_HOME_TOP_NEW_CHANNELS = "hometnc";

    public const string CACHE_TOP_COMMUNITIES = "topCommunities";
    public const string CACHE_TOP_NEW_COMMUNITIES = "newCommunities";

    #endregion

    // ***********************************************
    // Validation Contants (Regular expressions)
    // ***********************************************
    #region Validation Contants (Regular expressions)

    public const string VALIDATION_REGEX_EMAIL = "^[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
    public const string VALIDATION_REGEX_USERNAME = "[\\d_a-zA-Z0-9]{4,20}"; // letters, numbers, and mak, at least 4 characters
    public const string VALIDATION_REGEX_PASSWORD = "[\\d_a-zA-Z0-9]{4,20}"; // letters, numbers, and underscore, at least 4 characters
    public const string VALIDATION_REGEX_DAY = "[1-2][0-9]|[3][0-1]|[1-9]|[0][1-9]";
    public const string VALIDATION_REGEX_YEAR = "[1-2][0-9][0-9][0-9]";//"[1][9][1-9][0-9]|[2][0][0][0-9]";

    public const string VALIDATION_REGEX_CHANNEL_NAME = "[\\d\\s_\\-a-zA-Z0-9]{4,50}"; // letters, numbers, and underscore, at least 4 characters
    public const string VALIDATION_REGEX_CHANNEL_NAME_ERROR_MESSAGE = "Name should be at least four characters and can contain only letters, numbers, spaces, dashes or underscore.";
        public const string VALIDATION_REGEX_GAME_NAME_ERROR_MESSAGE = "3D App name should be 4-50 characters and can contain only letters, numbers, spaces or underscore.";
        
        // With leap year
    public const string VALIDATION_REGEX_DATE = "^(?:(?:(?:0?[13578]|1[02])(\\/|-|\\.)31)\\1|(?:(?:0?[1,3-9]|1[0-2])(\\/|-|\\.)(?:29|30)\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:0?2(\\/|-|\\.)29\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\\/|-|\\.)(?:0?[1-9]|1\\d|2[0-8])\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";
    public static readonly string VALIDATION_REGEX_MEDIA_FILE_FORMAT;

    public const string VALIDATION_REGEX_TIME_24 = "^(20|21|22|23|[01]\\d|\\d)(([:.][0-5]\\d){1,2})$";
    public const string VALIDATION_REGEX_TIME_AMPM = "(?<Time>^(?:0?[1-9]:[0-5]|1(?=[012])\\d:[0-5])\\d(?:[ap]m)?)";

    // Fails if the input contains /\:*?"<>|
    public const string VALIDATION_REGEX_FILENAME = "^[^\\\\\\/\\?\\*\\.\\&\\\"\\'\\>\\<\\:\\|]*$";
    public const string VALIDATION_REGEX_FILENAME_ERROR_MESSAGE = "cannot contain any of the following symbols /\\:.*?\"<>|&";

        //number field checks
        public const string VALIDATION_REGEX_POSITIVE_MONEY = "^\\d*\\.?\\d{0,2}$";
        public const string VALIDATION_REGEX_POSATIVE_INTEGERS = "^\\d+$";

    // Tags, min/max length 3/20, separated by spaces, only alphanumeric
    public const string VALIDATION_REGEX_TAG = "^([A-Za-z0-9]{2,20}[\\s]*)?([\\s]+([A-Za-z0-9]{2,20})[\\s]*)*$|^([A-Za-z0-9]{2,20}[,]*)?([,]+([A-Za-z0-9]{2,20})[,]*)*$|^([A-Za-z0-9]{2,20}[;]*)?([;]+([A-Za-z0-9]{2,20})[;]*)*$";
    public const string VALIDATION_REGEX_TAG_ERROR_MESSAGE = "Tags must be separated by spaces, commas or semicolons, be between 2 and 20 characters, and only contain letters and numbers.";

        // Restricted & Reserved words -- the Regular Expressions are created dynamically in the KanevaWebGlobals.isTextRestricted method
        public const string VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE = "Attention:\n\nYou\'ve entered a restricted word (one that is considered objectionable to other members). All materials published through the Kaneva site must comply with Kaneva\'s Terms of Service.\n\nTo continue, you\'ll need to change the word.";
    public const string VALIDATION_REGEX_RESERVED_WORD_MESSAGE = "Attention:\n\nThe name you?ve entered contains a word reserved by Kaneva.\n\n" +
            "To continue, you?ll need to change your name.\n\n";

        #endregion

    // ***********************************************
    // Contests Constants
    // ***********************************************
    #region Contest Constants

    public enum eTOP_VOTE_SORTBY
    {
      PRESENTER = 1,
      ENTRY = 0
    }

    public enum eASSET_INFO_POSITION
    {
      RIGHT = 1,
      BOTTOM = 0
    }

    public class ContestTableColumns
    {
      public const string ContestId = "contest_id";
      public const string ContestName = "name";
      public const string ContestTerms = "terms";
      public const string UploadStart = "upload_start_date";
      public const string UploadEnd = "upload_end_date";
      public const string VoteStart = "vote_start_date";
      public const string VoteEnd = "vote_end_date";
      public const string ChannelId = "channel_id";
      public const string ContestPicture = "contest_picture";
      public const string YesButton = "vote_yes_image";
      public const string NoButton = "vote_no_image";
    }

    // ***********************************************
    // Leader Board Constants
    // ***********************************************

    public enum eLEADER_BOARD_QUERIES
    {
      USERS_TO_USERS = 1,
      USERS_IN_CHANNEL = 2,
      CHANNEL_TO_CHANNEL = 3,
      MEDIA_IN_CHANNEL = 4
    }

    public enum eDEFAULTS
    {
      TIME_SPAN_MONTH = 3
    }

    public enum eURL_TYPE
    {
      PERSONAL = 1,
      BROADBAND = 2,
      MEDIA = 3
    }

    #endregion

    // ***********************************************
    // Contests Constants
    // ***********************************************
    #region Show Case

    public enum eTIME_FRAME
    {
      DAY = 1,
      WEEK = 7,
      MONTH = 30,
      YEAR = 365
    }

    #endregion


        // ***********************************************
        // WOK Item Constants
        // ***********************************************
        #region WOK Item Constants

        public enum eACCESS_TYPE
        {
            GENERAL = 1,
            ACCESS_PASS = 2
        }


        #endregion

        // ***********************************************
        // Game Constants
        // ***********************************************
        #region Game Constants

        public enum eGAME_SERVER_STATUS
        {
            STOPPED = 0,
            RUNNING = 1,
            STOPPING = 2,
            STARTING = 3,
            LOCKED = 4,
            FAILED = 5
        }

        public enum eGAME_SERVER_TYPE
        {
      ENGINE_SERVER = 1,
      ENGINE_CLIENT,
      ENGINE_AI,
      ENGINE_EDITOR,
      ENGINE_DISPATCHER,
      ENGINE_TELLSERVER
        }

        public enum eGAME_UPLOAD_PUBLISH_STATUS
        {
            ERROR = 0,
            UPLOADING = 1,
            UPLOADED = 2,
            /// <summary>
            /// when files on staging have been updated
            /// </summary>
            STAGING_UPDATED = 3
        }

        public const string WOK_INVENTORY_TYPE_ALL = "All";
        public const string WOK_INVENTORY_TYPE_PERSONAL = "P";
        public const string WOK_INVENTORY_TYPE_BANK = "B";
        #endregion

        // ***********************************************
    // Forum Constants
    // ***********************************************
    #region Forum Contants

    public enum eFORUM_STATUS
    {
      ACTIVE = 1,
      DELETED = 2,
      LOCKED = 3,
      NEW = 4
    }

    #endregion

    // ***********************************************
    // Blog Constants
    // ***********************************************
    #region Blog Contants

    public enum eBLOG_CATEGORY
    {
      ALL = 0,
      HOME = 1,
      WATCH = 2,
      PLAY = 3,
      CREATE = 4,
      COMMUNITY = 5
    }

    #endregion

    // ***********************************************
    // Blog Constants
    // ***********************************************
    #region Upload Configuration Contants

    public class UPLOAD_CONFIG
    {
      public const int YOUTUBE = 2;
      public const int UPLOAD = 1;
      public const int TV = 3;
      public const int FLASH = 4;
            public const int FLASH_GAME = 5;
    }


    #endregion

    // ***********************************************
    // User Constants
    // ***********************************************
    #region User Contants

    public enum eBILLING_INFO_TYPE
    {
      PROFILE = 1,
      ORDER = 2
    }

    public enum eUSER_COMMENT_SECURITY
    {
      ALLOWED = 0,
      UPON_APPROVAL = 1,
      NEVER = 2,
    }

    public enum eUSER_STATUS
    {
      REGVALIDATED = 1,
      REGNOTVALIDATED = 2,
      DELETEDBYUS = 3,
      DELETED = 4,
      LOCKED = 5,
      LOCKEDVALIDATED = 6,
      DELETEDVALIDATED = 7
    }

    public enum eUSER_BLOCK_SOURCE
    {
        UNKNOWN = 0,
        WEB = 1,
        WOK = 2,
        KIM = 3,
    }

    public enum eEMAIL_STATUS
    {
        OK = 0,
        BOUNCED = 1
    }

    public enum eEMAIL_SEND_STATUS
    {     
      OK = 0,
      TRAPLIST = 1,
      FILTERLIST = 2,
      BLACKLIST = 3
    }


    public enum eLOGIN_RESULTS
    {
      FAILED = 0,				      // 0 not authenticated
      SUCCESS = 1,			      // 1 successuful authentication
      USER_NOT_FOUND = 2,		  // 2a user not retrieved from database
      WORLD_NOT_FOUND = 2,    // 2b 3D app not found in developer database (used by gamelogin.aspx / get_game_info SP)
      INVALID_PASSWORD = 3,   // 3 invalid password
      NO_GAME_ACCESS = 4,     // 4a user was authenticated, but not to game supplied
      NO_WORLD_SERVERS = 4,   // 4b 3D app server not running (used by gamelogin.aspx / get_game_info SP)
      NOT_VALIDATED = 5,      // 5 user has not validated the account
      ACCOUNT_DELETED = 6,    // 6 user account was deleted
      ACCOUNT_LOCKED = 7,     // 7 user account was locked
      ALREADY_LOGGED_IN = 8,  // 8 user alreay logged in, performed by DS web service
      GAME_FULL = 9,		      // 9 Max user limit hit for the game
      NOT_AUTHORIZED = 10,    // 10 mature, age, access pass, etc.
      WRONG_SERVER = 11,      // 11 mismatched server info serverId doesn't match serverId for actualIp
      ADMIN_ONLY = 12,        // 12 the server is admin_only and you're not an admin
      PROMPT_ALLOW = 13,      // 13 the player must be prompted before going on to patch 3dApp
      GAME_BANNED = 14,       // 14 the game (3DApp) has been banned
	  GAME_DELETED = 15,      // 15 the game (3DApp)  was deleted by owner
      MEMBERS_ONLY = 16       // 16 the game is members only and you're not a member of the world
    }

        // Result code from "can xxx spawn to xxx" SPs
        public enum eCANSPAWN_RESULTS
        {
            FAILED = -1,            // Invalid parameter or etc.
            SUCCESS = 0,
            NOT_IN_GAME = 1,        // User not in game
            DEST_NOT_FOUND = 2,     // Destination not found
            NO_PERMISSION = 3,      // Private community
            BLOCKED = 4,            // Blocked or member only. NOTE: 3 and 4 are not clearly distinguished, especially for community.
            ZONE_FULL = 5,          // Zone is full (not used?)
            SERVER_FULL = 6,        // Server is full
            NO_PASS = 7,            // Access pass or VIP required
            CRITICAL_ERROR = 10,    // Bug in SPs
        }

    public enum eBLAST_PREFERENCES
    {
      
      BLOG_COMMENT = 0,	
      PROFILES = 1,		
      COMMENTS = 2,		
      FORUMS = 3,			
      ADD_FRIEND = 4,		
      JOIN_COMMUNITY = 5,	
      RAVE_HOME = 6,		
      RAVE_HANGOUT = 7,	
      SEND_GIFT = 8	
    }

    public enum eMESSAGE_TYPE
    {
      PRIVATE_MESSAGE = 0,
      ALERT = 1,
      //FRIEND_REQUEST = 2,
      //MEMBER_REQUEST = 3
      GIFT = 4,
            APP_REQUEST = 5,
            COMMUNITY_INVITES = 6
    }

    public const string GIFT_2D = "2D";
    public const string GIFT_3D = "3D";

        // generated by select concat( "public const UInt16 CASH_", trans_text, " = ", transaction_type, ";" )
        // from `kaneva`.`transaction_type` 
        // order by transaction_type;
        public const UInt16 CASH_TT_BOUGHT_CREDITS = 1;
        public const UInt16 CASH_TT_TRADE = 2;
        public const UInt16 CASH_TT_BOUGHT_ITEM = 3;
        public const UInt16 CASH_TT_SOLD_ITEM = 4;
        public const UInt16 CASH_TT_LOOT = 5;
        public const UInt16 CASH_TT_MISC = 6;
        public const UInt16 CASH_TT_REFUND = 7;
        public const UInt16 CASH_TT_OPENING_BALANCE = 8;
        public const UInt16 CASH_TT_BOUGHT_HOUSE_ITEM = 9;
        public const UInt16 CASH_TT_QUEST = 10;
        public const UInt16 CASH_TT_BANKING = 11;
        public const UInt16 CASH_TT_HOUSE_BANKING = 12;
        public const UInt16 CASH_TT_BOUGHT_GIFT = 13;
        public const UInt16 CASH_TT_ROYALITY = 14;
        public const UInt16 CASH_TT_SPECIAL = 15;
        public const UInt16 CASH_TT_COVER_CHARGE = 16;
        public const UInt16 CASH_TT_COMMISSION = 17;
        public const UInt16 CASH_TT_ITEM_PICKUP = 18;
        public const UInt16 CASH_TT_LEVEL_REWARD = 19;
        public const UInt16 CASH_TT_NAME_CHANGE = 20;
        public const UInt16 CASH_TT_BOUGHT_ITEM_ON_WEB = 21;
        public const UInt16 CASH_TT_CONTEST = 22;
        public const UInt16 CASH_TT_EVENT = 23;
        public const UInt16 CASH_TT_SURVEY = 24;
        public const UInt16 CASH_TT_INSTANT_BUY = 25;
        public const UInt16 CASH_TT_SUPER_REWARDS = 26;
        public const UInt16 CASH_TT_SUBSCRIPTIONS = 27;
        public const UInt16 CASH_TT_RAVE = 28;
        public const UInt16 CASH_TT_MEGARAVE = 29;
        public const UInt16 CASH_TT_RECEIVED_GIFT = 30;
        public const UInt16 CASH_TT_TOUR = 31;
        public const UInt16 TT_WORLD_FAME_ACHIEVEMENT = 32;
        public const UInt16 TT_EMP_AGENCY_LEVELUP = 33;
        public const UInt16 TT_TOUR_PARTICIPATION = 34;
        public const UInt16 TT_WORLD_VISITORS_BONUS = 35;
        public const UInt16 CASH_TT_PREMIUM_ITEM_CREDIT_REDEEM = 36;
        public const UInt16 CASH_TT_PREMIUM_ITEM = 37;
        public const UInt16 TT_GAMING = 38;
        public const UInt16 TT_GAME_ITEM = 39;
        public const UInt16 TT_GAME_ITEM_COMMISION = 40;

        public const string GENDER_MALE = "M";
        public const string GENDER_FEMALE = "F";
        public const string GENDER_BOTH = "U";

    public enum eMESSEGE_NOTIFICATION
    {
      NONE = 0,
      ALL = 1,
      ONLY_ALERTS = 2
    }

    public const int ERR_USER_EXCEEDED_NON_FRIEND_MESSAGE_COUNT = -98;
    public const int ERR_USER_BLOCKED_FROM_SENDING_MESSAGE = -99;

    #endregion

    // ***********************************************
    // Asset Constants
    // ***********************************************
    #region Asset Contants

        //correlates to 2nd normal form table asset_status
    public enum eASSET_STATUS
    {
      ACTIVE = 1,
      DELETED = 2,
      NEW = 3,
      MARKED_FOR_DELETION = 4
    }

    // Asset publishing statuses
    public enum ePUBLISH_STATUS
    {
      ERROR = 0,
      UPLOADING = 1,
      UPLOADED = 2,
      CONVERSION_PROCESSING = 3,
      /// <summary>
      /// when kaneva has created a torrent for uploaded content
      /// </summary>
      TORRENT_CREATED = 4,
      /// <summary>
      /// when torrent created by kaneva has been imported to the content server
      /// </summary>
      TORRENT_IMPORTED = 5,

      //other statuses, virus_checking_complete?

      /// <summary>
      /// when secure content is uploaded and processed
      /// </summary>
      PUBLISH_COMPLETE = 10
    }

    public enum eASSET_PERMISSION
    {
      PUBLIC = 1,
      PRIVATE = 2,
      FRIENDS = 3,
      KANEVA_MEMBERS = 4
    }

    public enum eASSET_TYPE
    {
      ALL = 0,
      GAME = 1,
      VIDEO = 2,
      ASSET = 3,
      MUSIC = 4,
      PICTURE = 5,
      PATTERN = 6,
      TV = 7,
      WIDGET = 8
  }

    public enum eASSET_SUBTYPE
    {
      ALL = 0,
      YOUTUBE = 1
    }

    public enum eFEATURED_ASSET_STATUS
    {
      PENDING = 1,
      ACTIVE = 2
    }

    public enum eASSET_RATING_TYPE
    {
      RATING = 1,
      COMMENT = 2,
      REVIEW = 3
    }

    public enum eASSET_RATING
    {
      GENERAL = 1,
      TEEN = 2,
      MATURE = 3
    }

    public enum eSEARCH_TYPE
    {
      WATCH = 2,
      PLAY = 1,
      CREATE = 3,
      CHANNELS = 4,
      USERS = 5,
      BLOGS = 6,
      FORUMS = 9,
      WORLD = 10
    }

    public enum eASSET_DOWNLOAD_TYPE
    {
      P2P = 1,
      STREAMING = 2,
      HTTP = 3
    }

        // Error Codes
    public enum eCoordinateErrorCode
    {
      NoError = 0,
      GeneralError = 1,
      MaxDistanceExceeded = 2
    }

    #endregion

    // ***********************************************
    // Channel Constants
    // ***********************************************
    #region Channel Contants

    public class ChannelCommunityTableColumns
    {
      public const string ChannelId = "community_id";
      public const string ChannelName = "name_no_spaces";
    }

    public enum eCOMMUNITIES
    {
      WATCH = 101,
      PLAY = 103,
      CREATE = 102,
      SUPPORT = 100,
      FILMMAKERS = 99
    }

    public enum eCOMMUNITY_STATUS
    {
      ACTIVE = 1,
      LOCKED = 2,
      DELETED = 3,
      NEW = 4,
            IMPORT = 5
    }

    public enum eCOMMUNITY_NOTIFICATIONS
    {
      OFF = 0,
      INDIVIDUAL_EMAIL = 1,
      DAILY_EMAILS = 2,
      WEEKLY_EMAILS = 3
    }

    public enum eCHANNEL_FILTER_TYPE
    {
      OWNER = 1,
      SUBSCRIBER = 2,
      PUBLIC = 3,
      PRIVATE = 4,
      ADULT = 5,
      TEEN = 6,
      GENERAL = 7,		// Was children
      MODERATOR = 8,
      DEFAULT = 9
    }

    public enum eCHANNEL_SEARCH_TYPE
    {
      PERSONAL = -1,
      NON_PERSONAL = -2
    }

        public class TabType
        {
            public const string ABOUT = "About";
            public const string EVENT = "Events";
            public const string BLOG = "Blogs";
            public const string FORUM = "Forums";
            public const string MEMBER = "Members";
            public const string FRIEND = "Friends";
            public const string GAME = "Games";
            public const string VIDEO = "Videos";
            public const string MUSIC = "Music";
            public const string PICTURE = "Photos";
            public const string BLAST = "Blasts";
            public const string COMMUNITIES = "Communities";
            public const string PERSONAL_INFO = "Info";
            public const string APP_3D = "3D Apps";
            public const string PREMIUM_ITEMS = "Premium Items";
            public const string LEADERBOARD = "Leaders";
            public const string BADGE = "Badges";
            public const string FAME = "Fame";
            public const string NEWS = "News";
            public const string WORLDS = "Worlds";
        }

        public enum eTab
        {
            About = 1,
            Blast = 2,
            Premium_Items = 3,
            Members = 4,
            Photos = 5,
            Videos = 6,
            Music = 7,
            Games = 8,
            Blog = 9,
            Forums = 10,
            Events = 11,
            Badges = 12,
            Leaders = 13,
            Friends = 14,
            Info = 15,
            Communities = 16,
            Fame = 17
        }

    #endregion

    // ***********************************************
    // Transaction Constants
    // ***********************************************
    #region Transaction Constants

    public enum ePAYMENT_METHODS
    {
      TESTER = 1,
      NOCHARGE = 2,
      PAYPAL = 3,
      INVITE_USER = 4,
      KANEVA_CYBERSOURCE = 5,
      INCOMM_GIFT_CARD = 6
    }

    public enum eTRANSACTION_STATUS
    {
      CHECKOUT = 0,
      WAITING_VERIFICATION = 1,
      DECLINED = 2,
      VERIFIED = 3,
      PRICE_MISMATCH = 4,
      ITEM_NOT_FOUND = 5,
      POINT_BUCKET_NOT_FOUND = 6,
      USER_NOT_VALID = 7,
      SELLER_EMAIL_MISMATCH = 8,
      NO_PAYMENT_STATUS = 9,
      PP_CANCELED_REVERSAL = 10,
      DENIED = 11,
      FAILED = 12,
      PENDING = 13,
      REFUNDED = 14,
      PP_REVERSED = 15,
      UNKNOWN = 16,
      EXPIRED = 17,
      AUTHORIZED = 18
    }

    public enum eORDER_STATUS
    {
      CART = 0,				// Used for bookmarks
      PENDING_PURCHASE = 1,
      COMPLETED = 2,
      FAILED = 3,
      CART_TAGS = 4,
      CHECKOUT = 5,
      REFUNDED = 6
    }

    public enum ePURCHASE_TYPE
    {
      NOT_SET = 1,
      ASSET = 2,
      FEATURED_ITEM = 3,
      GAME_SUBSCRIPTION = 4,
      DAY_PILOT_LICENSE = 5,
      COMMERCIAL_SERVER_AUTH = 6,
      KPOINT_ONLY = 7,
      GIFT_CARD = 8,
            ACCESS_PASS = 9, 
            INVITE_AWARD = 10,
            NAME_CHANGE = 11,
            STAR_LICENSE = 12,
            SUBSCRIPTION = 13
    }

    public const string ROYALTY_NOT_PAID = "N";
    public const string ROYALTY_CREDITED = "C";
    public const string ROYALTY_PAID = "P";

    #endregion

    // ***********************************************
    // Friend Constants
    // ***********************************************
    #region Friend Constants

    public enum eFRIEND_STATUS
    {
      FRIENDS = 0,
      NEED_CONFIRM = 1,
      REFRUSED = 2,
      FRIENDSHIP_ENDED = 3,
      FRIENDSHIP_ENDED_BY_US = 4
    }

    public enum eINVITE_STATUS
    {
      UNREGISTER = 1,
      DELETE_BEFORE_RESPOND = 2,
      REGISTERED = 3,
      DELETED = 4,
      DELETED_BY_US = 5,
            MAX_IP = 6,
            WOK_LOGIN = 7,
            DECLINED = 8
    }

        public enum eINVITE_POINT_AWARD_TYPE
        {
            REGISTRATION = 2,
            WOK_LOGIN = 3
        }

    #endregion

    // ***********************************************
    // Torrents
    // ***********************************************
    #region Torrents

    public enum eTORRENT_STATUS
    {
      NOSCRAPE = 0,
      /// <summary>
      /// when publisher is in the process of uploading to the content server
      /// </summary>
      UPLOADING = 1,
      /// <summary>
      /// when a torrent is posted to kaneva
      /// </summary>
      TORRENT_POSTED = 2,
      SEEDING = 3,
      SEEDING_COMPLETE = 4,
      /// <summary>
      /// when secure content is uploaded to the content server
      /// </summary>
      UPLOADED = 5,
      ERROR = 6,
      /// <summary>
      /// when kaneva has recreated a torrent for uploaded content
      /// </summary>
      TORRENT_RECREATED = 7,
      /// <summary>
      /// when torrent created by kaneva has been imported to the content server
      /// </summary>
      TORRENT_REPLACED = 8,
      /// <summary>
      /// when secure content is uploaded and processed
      /// </summary>
      PUBLISH_COMPLETE = 9
    }

    public enum eTORRENT_ENTRY_STATUS
    {
      ACTIVE = 1,
      DELETED = 2
    }

    #endregion

    // ***********************************************
    // RSS
    // ***********************************************
    #region RSS

    public enum eRSS_FEEDS
    {
      KANEVA_NEWS = 1,
      VIDEO_NEWS = 2,
      GAME_NEWS = 3,
      DEV_NEWS = 4,
      COMMUNITY_NEWS = 5,
      FORUM_POSTS = 6,
      STORE_ITEMS = 7,
      WHOLE_CHANNEL = 8
    }

    #endregion

    // ***********************************************
    // Search Constants
    // ***********************************************
    #region Search Constants

    public const string QUERY_STRING_KEYWORD = "kwd";
    public const string QUERY_STRING_SEARCH_TYPE = "type";
    public const string QUERY_STRING_ORDERBY = "ob";
    public const string QUERY_STRING_WITHIN = "wi";
    public const string QUERY_STRING_PHOTO = "ph";
        
        // Params for personal attributes, interests, etc
        public const string QUERY_STRING_RELATIONSHIP = "relate";
        public const string QUERY_STRING_ORIENTATION = "orient";
        public const string QUERY_STRING_RELIGION = "relgn";
        public const string QUERY_STRING_ETHNICITY = "eth";
        public const string QUERY_STRING_CHILDREN = "child";
        public const string QUERY_STRING_EDUCATION = "edu";
        public const string QUERY_STRING_INCOME = "income";
        public const string QUERY_STRING_HEIGHT = "height";
        public const string QUERY_STRING_SMOKING = "smoke";
        public const string QUERY_STRING_DRINKING = "drink";
        public const string QUERY_STRING_INTEREST = "int";
        public const string QUERY_STRING_SCHOOL = "sch";
        public const string QUERY_STRING_HOMETOWN = "htwn";
        public const string QUERY_STRING_COUNTRY = "country";
        public const string QUERY_STRING_ZIP_CODE = "zip";


    public const string SEARCH_ORDERBY_RELEVANCE = "relevance";
    public const string SEARCH_ORDERBY_RAVES = "raves";
        public const string SEARCH_ORDERBY_VIEWS = "views";
        public const string SEARCH_ORDERBY_VISITS = "visits";
        public const string SEARCH_ORDERBY_REQUESTS = "requests";
        public const string SEARCH_ORDERBY_CURRENT_POPULATION = "current_population";
        public const string SEARCH_ORDERBY_NEWEST = "newest";
    public const string SEARCH_ORDERBY_MEMBERS = "members";
    public const string SEARCH_ORDERBY_ADDS = "adds";
    public const string SEARCH_ORDERBY_MOST_ACTIVE = "active";
    public const string SEARCH_ORDERBY_RECENTLY_UPDATED = "recent_update";
    public const string SEARCH_ORDERBY_LAST_LOGGED_IN = "last_login";

    public enum eSEARCH_TIME_FRAME
    {
      TODAY = 1,
      WEEK = 7,
      MONTH = 30,
      ALL_TIME = 0
    }

        public enum eHANGOUT_PLACE_TYPE
        {
            OTHER = 0,
            ARCADE = 1,
            ART_GALLERY = 2,
            CASINO = 3,
            DANCE_CLUB = 4,
            FESTIVAL = 5,
            GAME_LOUNGE = 6,
            MUSUEM = 7,
            MUSIC_VENUE = 8,
            PLACE_OF_WORSHIP = 9,
            SPORTS_BAR = 10,
            STORE = 11,
            THEATER = 12,
            THEME_PARK = 13,
            WEDDING_CHAPEL = 14
        }
    
    public enum eCLOUD_TYPE
    {
      ALL_MEDIA = 0,
      GAME = 1,
      VIDEO = 2,
      MUSIC = 4,
      PHOTO = 5,
      PATTERN = 6,
      COMMUNITY = -1
    }

        #endregion

    // ***********************************************
    // Feedback
    // ***********************************************
    #region Feedback
    public enum eFeedback
    {
      ERROR = 1,
      FEEDBACK = 2,
            LAUNCHER_FEEDBACK = 3
    }
    #endregion

    // ***********************************************
    // Return codes used by the Error reporting web service
    // ***********************************************
    #region Return codes used by the Error reporting web service

    public enum eERROR_REPORTING_RETURNCODES
    {
      SUCCESS = 0,
      ERROR_RECORDING_ISSUE_IN_DB = 1,
      ERROR_SAVING_REPORT_FILE = 2,
      ERROR_SENDING_EMAIL_NOTIFICATION = 3
    }

    #endregion


        // ***********************************************
        // Marketing email types
        // ***********************************************
        #region Marketing email types

        public enum eMARKETING_EMAIL_TYPES
        {            
            NO_WOK = 1,
            NOT_SPENT = 2,
            PEOPLE_NEARBY = 3,
            NEWSLETTER = 4,
            TESTING = 86
        }

        #endregion

    // ***********************************************
    // Return codes used by the DS web service
    // ***********************************************
    #region Return codes used by the DS web service

    public enum eDS_RETURNCODES
    {
      SUCCESS = 0,
      BIT_TORRENT_LINK_FAIL = 1,
      EXCEPTION_OBTAINING_ASSET_SERVERS = 2,
      EXCEPTION_OBTAINING_INVENTORY = 3,
      EXCEPTION_OBTAINING_ASSET_FILE = 4,
      EXCEPTION_POSTING_ASSET_FILE = 5,
      ERROR_POST_MULTIPLE_ASSET = 6,
      ERROR_POST_EMPTY_ASSET = 7,
      ERROR_POST_TRACKER = 8,
      EXCEPTION_POSTING_TO_TRACKER = 9,
      ERROR_POST_FILE_CORRUPT_OR_INVALID = 10,
      ERROR_POST_FILE_NOT_TORRENT = 11,
      ERROR_POST_FILE_CATEGORY_TAG_INVALID = 12,
      ERROR_POST_FILE_ALREADY_EXISTS = 13,
      ERROR_POST_FILE_HASH_CONFLICT = 14,
      EXCEPTION_DELETING_FROM_TRACKER = 15,
      ERROR_DELETE_FILE_FAILURE = 16,
      ERROR_POST_DB_ENTRY_ALREADY_EXISTS = 17,
      ERROR_POST_DB_INSERT_FAILURE = 18,
      EXCEPTION_GET_DEFAULT_ASSET_IMAGE = 19,
      ERROR_INVALID_INVENTORY_TYPE = 20,
      NOT_VALIDATED = 21,
      ALREADY_LOGGED_IN = 22,
      VALID_LOGIN = 23,
      EXCEPTION_POSTING_TO_ABC = 24,
      ERROR_POST_FILE_ABC_DUPLICATE = 25,
      ERROR_POST_FILE_ABC_URL_BAD = 26,
      ERROR_POST_FILE_ABC_UNKNOWN_ERROR = 27,
      ERROR_POST_TO_AZUREUS = 28,
      EXCEPTION_OBTAINING_FREE_ASSET = 29,
      ERROR_OBTAINING_FREE_ASSET = 30,
      EXCEPTION_GET_CONTENT_ACCESS = 31,
      EXCEPTION_GET_USER_QUOTA = 32,
      ERROR_OBTAINING_USER_QUOTA = 33,
      ERROR_POST_ASSET_QUOTA_EXCEEDED = 34,
      EXCEPTION_OBTAINING_ASSET_STATUS = 35,
      EXCEPTION_OBTAINING_CONTENT_ASSETS = 36,
      ERROR_RECORDING_ISSUE_IN_MANTIS = 37,
      ERROR_GETTING_PUBLISH_COMMUNITIES = 38,
      ERROR_POST_FILE_ANNOUNCE_LIST_FOUND = 39,
      ERROR_POST_FILE_INVALID_ANNOUNCE_URL = 40,
      ERROR_POST_FILE_INVALID_TORRENT = 41,
      EXCEPTION_PROCESSING_UPLOADED_CONTENT = 42,
      ERROR_POST_FILE_EXCEEDED_NUM_FILES_LIMIT = 43,
      ERROR_UPDATE_FREE_ASSETS = 44,
      EXCEPTION_GETTING_IRIS_BUILT_DATE = 45,
      ERROR_OBTAINING_INVENTORY_ASSET = 46,
      EXCEPTION_OBTAINING_INVENTORY_ASSET = 47
    }

    // ***********************************************
    // Type of inventory items that can be returned by the
    // DS web service
    // ***********************************************
    public enum eDS_INVENTORYTYPE
    {
      GAMES = 1,
      ASSETS = 2
    }

    #endregion



    #region publishing processor constants
    // ***********************************************
    // Return codes used by asset processor
    // ***********************************************
    public enum eASSET_PROCESSOR_RETURNCODES
    {
      SUCCESS = 0,
      ERROR_MOVING_FILES,
      ERROR_POSTING_TORRENT,
      ERROR_DELETEING_UPLOADED_FILE
    }

    // ***********************************************
    // Return codes used by game upload processor
    // ***********************************************
    public enum eGAME_UPLOAD_PROCESSOR_RETURNCODES
    {
      SUCCESS = 0,
      ERROR_MOVING_FILES
    }
    #endregion

    #region publishing server constants
    public const String PARAM_LOGIN_USERNAME = "username";
    public const String PARAM_LOGIN_PASSWORD = "password";

    public const String PARAM_UPLOAD_INIT_TICKET = "ticket";
    public const String PARAM_UPLOAD_INIT_FILE_NAME = "fileName";
    public const String PARAM_UPLOAD_INIT_HASH = "hash";
    public const String PARAM_UPLOAD_INIT_SIZE = "size";
    public const String PARAM_UPLOAD_INIT_TYPE = "type";

    public const String	PARAM_UPLOAD_INIT_DEST_PATH = "destinationPath";
    public const String	PARAM_UPLOAD_INIT_GAME_ID = "gameId";
    public const String	PARAM_UPLOAD_INIT_GAME_UPLOAD_ID = "gameUploadId";

    public const String PARAM_UPLOAD_PROCESS_TICKET = "ticket";
    public const String PARAM_UPLOAD_PROCESS_ASSET_ID = "assetId";
    public const String PARAM_UPLOAD_PROCESS_START_BYTE = "startByte";
    public const String PARAM_UPLOAD_PROCESS_END_BYTE = "endByte";
    public const String PARAM_UPLOAD_PROCESS_CRC = "crc";
    public const String PARAM_UPLOAD_PROCESS_DATA = "data";
    public const String PARAM_UPLOAD_PROCESS_GAME_UPLOAD_ID = "gameUploadId";
    public const String PARAM_UPLOAD_PROCESS_GAME_FILE_NAME = "fileName";
    public const String PARAM_UPLOAD_PROCESS_GAME_RELATIVE_PATH = "destinationPath";

    public const String PARAM_GET_GAMES_TICKET = "ticket";

    /// <summary>
    /// path of a file to be uploaded is sent over in format of RELATIVE_PATH\FILENAME
    /// </summary>
    public const String GAME_UPLOAD_DIR_SEPARATOR = "\\";

    public const String PARAM_NOTIFY_DELETION_ASSET_ID = "assetId";

    public const string GAME_FILE_MANIFEST = "manifest.xml";
    public const string GAME_FILE_CHANGE_LOG = "change_log.txt";

    public const string UPLOAD_FOLDER_SERVER = "ServerFiles";
    public const string UPLOAD_FOLDER_PATCH = "PatchFiles";

    public enum ePS_RETURNCODES
    {
      SUCCESS = 0,
      ERROR_INVALID_REQUEST = 1,
      LOGIN_NOT_VALIDATED = 2,
      ERROR_LOGIN = 3,
      EXCEPTION_INIT_UPLOAD = 4,
      ERROR_TICKET_NOT_AUTHORIZED = 5,
      ERROR_FILE_EXISTS = 6,
      ERROR_CRC_ERROR = 7,
      ERROR_INVALID_START_BYTE = 8,
      EXCEPTION_UPLOAD = 9,
      ERROR_MD5_ERROR = 10,
      SERVER_ERROR = 11,
      ERROR_MAX_FILE_SIZE_EXCEEDED = 12,
      ERROR_UPLOAD_EXPIRED = 13,
      ERROR_FILE_DELETED = 14,
      /// <summary>
      /// user is not the owner of the asset trying to upload
      /// </summary>
      ERROR_USER_NOT_OWNER = 15,
      /// <summary>
      /// file requested to upload was not in the initial manifest file
      /// </summary>
      ERROR_FILE_NOT_IN_LIST = 16,
      /// <summary>
      /// this is for game file only, denotes that the file on staging has the same hash
      /// so user doesn't need to reupload
      /// </summary>
      FILE_NOT_CHANGED = 17,
      /// <summary>
      /// the whole game is now uploaded
      /// </summary>
      GAME_UPLOADED = 18,
      /// <summary>
      /// a previous game upload is not yet processed. user can not upload a new version
      /// </summary>
      ERROR_GAME_UPLOAD_BEING_PROCESSED = 19,
      ERROR_GET_GAMES = 20

    }

    public enum eGAME_FILE_UPLOAD_RESULT
    {
      /// <summary>
      /// The file is not yet uploaded
      /// </summary>
      NOT_UPLOADED = 0,
      /// <summary>
      /// same file on server is updated
      /// </summary>
      UPLOADED = 1,
      /// <summary>
      /// new file is created and saved on server
      /// </summary>
      //			UPDATED = 2,
      /// <summary>
      /// the file on server is identical to the file being uploaded
      /// </summary>
      NO_CHANGE = 3,
      /// <summary>
      /// upload failed
      /// </summary>
      ERROR = 4
    }
    #endregion

    #region Page Management constants

    // see layout_zones table
    public const int HEADER_ZONE_ID	= 1;
    public const int LEFT_ZONE_ID	= 2;
    public const int RIGHT_ZONE_ID	= 3;
    public const int DELETE_ZONE_ID	= 4;
    public const int FOOTER_ZONE_ID	= 5;
        public const int LEFT_TOP_ZONE_ID = 6;

    #endregion

    #region layout

    //user must be this age or older to view mature content
    public const int MINIMAL_MATURE_AGE = 18;

    public enum eMODULE_TYPE
    {
      TITLE_TEXT = 1,
      BLOGS = 2,
      PROFILE = 3,
      FRIENDS = 4,
      COMMENTS = 5,
      TEXT = 6,
      HTML_CONTENT = 7,
      MENU = 8,
      MY_PICTURE = 9,
      MULTIPLE_PICTURES = 10,
      GAMES_PLAYER = 11,
      CHANNELS = 12,
      SINGLE_PICTURE = 13,
      PHOTOSTACK = 14,
      PHOTO_ALBUM = 15,
      SLIDE_SHOW = 16,
      MUSIC_PLAYER = 17,
      VIDEO_PLAYER = 18,
      MY_COUNTER = 19,
      MAP = 20,
      SYSTEM_STATS = 21,
      NEW_PEOPLE = 22,
      CONTROL_PANEL = 23,
      CHANNEL_OWNER = 24,
      CHANNEL_MEMBERS = 25,
      CHANNEL_EVENTS = 26,
      CHANNEL_FORUM = 27,
      CHANNEL_CONTROL_PANEL = 28,
      CHANNEL_DESCRIPTION = 29,
      MIXED_MEDIA = 30,
      OMM_VIDEO_PLAYER = 32,
      OMM_VIDEO_PLAYLIST = 33,
      OMM_MUSIC_PLAYER = 34,
      OMM_MUSIC_PLAYLIST = 35,
      TOP_CONTRIBUTORS = 36,
      CONTEST_UPLOAD = 37,
      CONTEST_SUBMISSIONS = 38,
      CONTEST_GET_RAVED = 39,
      USER_GIFTS = 40,
      CONTEST_PROFILE_HEADER = 41,
      CONTEST_TOP_RESULTS = 42,
      PROFILE_PORTAL	= 43,
      CHANNEL_PORTAL = 44,
      LEADER_BOARD = 45,
      HOT_NEW_STUFF = 46,
      NEWEST_MEDIA = 47,
      BILLBOARD = 48,
      MOST_VIEWED = 49,
      USER_UPLOAD = 50,
      KANEVA_LEADER_BOARD = 51,
      COMMUNITY_TOP_BANNER = 52,
            INTERESTS = 53,
            PERSONAL = 54,
            CHANNEL_INFO = 55,
            FAME_PANEL = 56,
            HTML_CONTENT_BASIC = 57

      }

    public enum eCHANNEL_MODULE_FILTER
    {
      SHOW_OWNER = 1, //show owned channel
      SHOW_MODERATOR = 2, //show channels that the user moderates
      SHOW_MEMBER = 4 //show channels that the user is a member of
    }

    // BLOG
    public const int	DEFAULT_BLOG_ENTRIES_PER_PAGE = 20;
    public const bool	DEFAULT_BLOG_ENTRIES_SHOW_DATE = true;

    // FRIEND
    public const int	DEFAULT_FRIEND_ENTRIES_PER_PAGE = 10;

    // COMMENTS
    public const int	DEFAULT_COMMENT_MAX_PER_PAGE = 100;
    public const bool	DEFAULT_COMMENT_SHOW_DATE_TIME = true;
    public const bool	DEFAULT_COMMENT_SHOW_PROFILE_PIC = true;
    public const char	DEFAULT_COMMENT_AUTHOR_ALIGNMENT = 'L';
    public const int	DEFAULT_COMMENT_PERMISSION_FRIENDS = 1;
    public const int	DEFAULT_COMMENT_PERMISSION_EVERYONE = 1;

    // COUNTER
    public const int DEFAULT_COUNTER_FONT_SIZE = 22;
    public const string DEFAULT_COUNTER_FONT_STYLE = "bold";
    public const string DEFAULT_COUNTER_FONT_COLOR = "#FFFFFF";
    public const string DEFAULT_COUNTER_BG_COLOR = "#000000";

    //EVENTS
    public const int	DEFAULT_EVENTS_ENTRIES_PER_PAGE = 10;

    public const int EVENTS_ALL = 100;
    public const int EVENTS_FRIENDS = 101;
    public const int COMMUNITIES = 102;
    
    public static readonly IDictionary COUNTER_FONTS;
    public static readonly IDictionary COUNTER_STYLES;
    public static readonly IDictionary EVENT_TYPES;
    public static readonly IDictionary OPT_MONTH;
    public static readonly IDictionary OPT_DAY;
    public static readonly IDictionary OPT_YEAR;
    public static readonly IDictionary OPT_HOUR;
    public static readonly IDictionary OPT_MINUTE;
    public static readonly IDictionary OPT_TIME_ZONE;

    public static readonly IList FILE_TYPE_VIDEO;
    public static readonly IList FILE_TYPE_MUSIC;
    public static readonly IList FILE_TYPE_GAME;
    public static readonly IList FILE_TYPE_PICTURE;

    // MY PICTURE
    public const bool	DEFAULT_MY_PICTURE_SHOW_GENDER		= true;
    public const bool	DEFAULT_MY_PICTURE_SHOW_LOCATION	= true;
    public const bool	DEFAULT_MY_PICTURE_SHOW_AGE			= true;

    // New People
    public const int	DEFAULT_NEW_PEOPLE_NUM_SHOWING		= 5;
    public const bool	DEFAULT_NEW_PEOPLE_WITH_PIC_ONLY	= true;

    // MULTIPLE PICTURES
    public const int	DEFAULT_MULTIPLE_PICTURES_PICTURES_PER_PAGE		= 10;
    public const bool	DEFAULT_MULTIPLE_PICTURES_SHOW_IMAGE_TITLE		= true;

    // MULTIPLE PICTURES
    public const int	DEFAULT_CHANNELS_ENTRIES_PER_PAGE		= 10;
    public const int	DEFAULT_CHANNELS_FILTER					= (int) eCHANNEL_MODULE_FILTER.SHOW_MEMBER |
      (int) eCHANNEL_MODULE_FILTER.SHOW_MODERATOR | (int) eCHANNEL_MODULE_FILTER.SHOW_OWNER;

    // SLIDE SHOW
    public const bool	DEFAULT_SLIDE_SHOW_HIDE_CAPTION		= false;
    public const bool	DEFAULT_SLIDE_SHOW_AUTOMATED		= true;

    // CHANNEL MEMBERS
    public const int	DEFAULT_CHANNELS_MEMBERS_ENTRIES_PER_PAGE		= 10;
    public const int	DEFAULT_CHANNEL_FORUM_ENTRIES_PER_PAGE			= 5;
    public const bool	DEFAULT_CHANNEL_MEMBER_SHOW_JOIN_DATE			= true;
    public const bool	DEFAULT_CHANNEL_MEMBER_SHOW_DIGS				= true;
    public const bool	DEFAULT_CHANNEL_MEMBER_SHOW_ROLE				= true;

    // CHANNEL DESC
    public const int	DEFAULT_CHANNEL_NUM_MODERATORS_TO_SHOW = 5;
    public const bool	DEFAULT_CHANNEL_SHOW_MODERATORS = true;


    // SINGLE PICTURE

    public enum eSINGLE_PICTURE_ALIGNMENT
    {
      LEFT	= 0,
      CENTER	= 1,
      RIGHT	= 2
    }
    public const int	DEFAULT_SINGLE_PICTURE_ALIGNMENT	= (int)eSINGLE_PICTURE_ALIGNMENT.CENTER;
    public const int	DEFAULT_SINGLE_PICTURE_ASSET_ID		= 0;

    public enum ePICTURE_SIZE
    {
      ACTUAL		= 0,
      THUMBNAIL	= 1,
      SMALL		= 2,
      MEDIUM		= 3,
      LARGE		= 4,
      RELATIVE	= 5
    }

    // profile portal
    public const bool DEFAULT_PROFILE_PORTAL_SHOW_STATS = true;

    // pixels (same value used for height and width)
    public const int	PICTURE_DIMENSIONS_THUMBNAIL		= 50;
    public const int	PICTURE_DIMENSIONS_SMALL			= 100;
    public const int	PICTURE_DIMENSIONS_MEDIUM			= 240;
    public const int	PICTURE_DIMENSIONS_LARGE			= 500;


    /// <summary>
    /// bit flag that indicates where a module is appliable
    /// </summary>
    public enum eMODULE_VISIBLE
    {
      PERSON = 0x01,
      CHANNEL = 0x02,
      SYSTEM = 0x04
    }

    /// <summary>
    /// bit flag that indicates where a module group is appliable
    /// </summary>
    public enum eMODULE_GROUP_VISIBLE
    {
      PERSON = 0x01,
      CHANNEL = 0x02,
      SYSTEM = 0x04
    }

    // Control Panel
    public const bool	DEFAULT_CONTROL_PANEL_SHOW_GENDER	= true;
    public const bool	DEFAULT_CONTROL_PANEL_SHOW_LOCATION	= true;
    public const bool	DEFAULT_CONTROL_PANEL_SHOW_AGE		= true;

    //mixed media
    public enum eMODULE_MIXED_MEDIA_VIEW
    {
      LEFT = 1,
      RIGHT = 2,
      TABLE = 3
    }

    public enum eMODULE_MIXED_MEDIA_THUMBNAIL_SIZE
    {
      SMALL = 1,
      LARGE = 2
    }

    public const int	DEFAULT_MIXED_MEDIA_VIEW_TYPE_ID =
      (int) eMODULE_MIXED_MEDIA_VIEW.LEFT;
    public const int	DEFAULT_MIXED_MEDIA_THUMBNAIL_SIZE =
      (int) eMODULE_MIXED_MEDIA_THUMBNAIL_SIZE.LARGE;
    public const int	DEFAULT_MIXED_MEDIA_NUM_SHOWING = 5;
    public const int	DEFAULT_MIXED_MEDIA_NUM_ROWS = 0;
    public const int	DEFAULT_MIXED_MEDIA_NUM_COLUMNS = 0;
    public const bool	DEFAULT_MIXED_MEDIA_SHOW_NAME = true;
    public const bool	DEFAULT_MIXED_MEDIA_SHOW_TEASER = true;
    public const bool	DEFAULT_MIXED_MEDIA_SHOW_DIGGS = false;
    public const bool	DEFAULT_MIXED_MEDIA_SHOW_NUM_VIEWS = false;
    public const bool	DEFAULT_MIXED_MEDIA_SHOW_NUM_SHARES = false;
    public const bool	DEFAULT_MIXED_MEDIA_SHOW_PRICE = false;
    public const bool	DEFAULT_MIXED_MEDIA_SHOW_TYPE = false;

    public const int    MIXED_MEDIA_SMALL_THUMBNAIL_WIDTH = 61;
    public const int    MIXED_MEDIA_SMALL_THUMBNAIL_HEIGHT = 41;
    public const int    MIXED_MEDIA_LARGE_THUMBNAIL_WIDTH = 173;
    public const int    MIXED_MEDIA_LARGE_THUMBNAIL_HEIGHT = 120;

    public const int    DEFAULT_MEMBERS_MAP_NUM_TOP_MEMBERS = 10;
    public const int    DEFAULT_TOP_CONTRIBUTORS_NUM_TOP_CONTRIBUTORS = 5;

    public enum ePAGE_ACCESS
    {
      PUBLIC = 0,
      FRIENDS = 1,
      PRIVATE = 2,
      MEMBERS = 3
    }

    #endregion

    #region OMM request
    // request types
    public const Int32 GET_FULL_METADATA	= 1;
    public const Int32 GET_STATIC_METADATA	= 2;
    public const Int32 GET_DYNAMIC_METADATA	= 3;
    public const Int32 GET_PLAYLIST_LIST	= 4;
    public const Int32 GET_PLAYLIST			= 5;
    public const Int32 ADD_RAVE				= 6;
    public const Int32 ADD_TO_FAVORITES		= 7;
    public const Int32 REPORT_ITEM			= 8;
    public const Int32 GET_SHARE_DATA		= 9;
    public const Int32 VIEW_ITEM			= 10;

    // constant playlist ids
    public const Int32 COOL_ITEM_PLAYLIST_ID = -1;
    public const Int32 USER_ALL_VIDEO_PLAYLIST_ID  = -2;
    public const Int32 USER_ALL_MUSIC_PLAYLIST_ID  = -3;
    public const Int32 COOL_ITEM_PLAYLIST_ID_HOME = -4;
    public const Int32 COOL_ITEM_PLAYLIST_ID_MEMBER_CHANNEL = -5;
    public const Int32 COOL_ITEM_PLAYLIST_ID_BROADBAND_CHANNEL = -6;
    public const Int32 COOL_ITEM_PLAYLIST_ID_MEDIA = -7;

    // query string values
    public const string QS_TYPE				= "type";
    public const string QS_ASSETID			= "assetId";
    public const string QS_PLAYLISTID		= "plId";
    public const string QS_USERID			= "userId";
    public const string QS_REPORT_EMAIL		= "email";
    public const string QS_REPORT_TYPE		= "reportType";
    public const string QS_REPORT_COMMENT	= "comment";
    public const string QS_FROM_GAME		= "game";

    #endregion

    #region KGP
    public const string TEXTURE_EXTENSION = ".JPG";
        public const Int32 GROUP_ZONE = 1;
        public const Int32 GUILD_ZONE = 2;
        public const Int32 APARTMENT_ZONE = 3;
        public const Int32 PERMANENT_ZONE = 4;
        public const Int32 ARENA_ZONE = 5;
        public const Int32 BROADBAND_ZONE = 6;
        public const Int32 ZONE_TO_PERSON = 99;
        public const Int32 SHOW_PERM_ZONE = 100; // travelzonelist.aspx

    #endregion

    #region gifts
    public const string GIFT_STATUS_UNACCEPTED = "U";
    public const string GIFT_STATUS_ACCEPTED = "A";
    public const string GIFT_STATUS_REJECTED = "R";
    #endregion

    #region Item External Add constants
    public enum eITEM_EXTERNAL_ADD_SOURCE
    {
      NONE = 0,
      OWNER = 1,
      MODERATOR = 2
    }
    #endregion

        #region User Acquisition constants
        
        public enum eACQUISITION_SOURCE_TYPE
        {
            OTHER_SITE = 1,
            GOOGLE_PAID_KEYWORD = 2,
            GOOGLE_SEARCH = 3,
            REFERRAL = 4
        }

        public const string ACQUISITION_COOKIE_NAME = "firstref";
        public const string ACQUISITION_COOKIE_VALUE_HOST = "host";
        public const string ACQUISITION_COOKIE_VALUE_ABSOLUTEURI = "absoluteuri";

        public const string ACQUISITION_QUERYSTRING_REFERRAL = "refuser";
        public const string ACQUISITION_QUERYSTRING_GOOGLE_PAID_KEYWORD = "HBX_PK";

        #endregion User Acquisition constants
    }
}
